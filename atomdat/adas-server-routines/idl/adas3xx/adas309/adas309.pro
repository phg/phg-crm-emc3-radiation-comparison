; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas309/adas309.pro,v 1.16 2004/07/06 10:33:39 whitefor Exp $ Date $Date: 2004/07/06 10:33:39 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS309
;
; PURPOSE:
;	The highest level routine for the ADAS 309 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 309 application.  Associated with adas309.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The actual FORTRAN code which is being communicated with depends
;	on the point in the program. Here the data set is interrogated
;	by adas309.out whilst the actual calculation and output is
;	performed by adas309b.out.  THis latter can be run either
;       interactively in which case the IDL communicates with it
;	by pipe all the way through the calculation, or as a batch
;	job where the IDL writes a UNIX script which is run at a later
;	time using the UNIX 'at' command.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run. In the case of the batch job, the error messages
;	will appear as standard cron job output which is usually
;	emailed to the user. If the job has completed succesfully then
;	the user will get a message telling them so.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID. This checking does not occur for the 
;	batch cases.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas309.pro is called to start the
;	ADAS 309 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas309,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       C9SPF0          Pipe comms with FORTRAN C9SPF0 routine.
;       CXSETP          Pipe comms with FORTRAN CXSETP routine.
;       C9ISPF          Pipe comms with FORTRAN C9ISPF routine.
;       C9SPF1          Pipe comms with FORTRAN C9SPF1 routine.
;	C9SPF2		Pipe comms with FORTRAN ADAS309B and RUN309
;			routines - i.e. communicates with the actual
;			routine that does the calculation when the
;			job is run 'interactively'.
;	C9SPF3		Updates the batch files and variables for
;			the batch runs depending on the user selections.
;
; SIDE EFFECTS:
;	This routine spawns FORTRAN executables.  Note the pipe 
;	communications routines listed above. 
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 22/06/1995
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First release
;	1.2	Tim Hammond	
;		Tidied code up
;	1.3	Tim Hammond
;		Added -s flag to spawn command for 'at'
;               to ensure that tmp files are removed
;               regardless of whether the user has
;               rm aliased to rm -i in their csh (this
;               flag runs at in sh).
;	1.4	Tim Hammond			
;		Applied above change to correct at command
;               and increased version number to 1.2
;               Also ensured that appending is turned off
;               after a batch job is submitted.
;	1.5	Tim Hammond			
;		Altered program so it enquires what sort of machine
;               it is on and alters the syntax of the at command
;               and the accompanying message as appropriate.
;               Currently works where command uname returns:
;               'OSF1', 'ULTRIX', and 'SunOS' (5.3 or 4.1.3)
;		Updated version number to 1.3
;	1.6	Tim Hammond
;		Increased version number to 1.4
;	1.7	William Osborn
;		Added menu buttons
;	1.8	William Osborn
;		Updated version number to 1.5
;	1.9	William Osborn
;		Minor syntax error - replaced " by ' in batch execution string
;       1.10    William Osborn
;               Changed SunOS batch execution code
;       1.11    William Osborn
;               Increased version number to 1.6
;	1.12	William Osborn	
;		Added call to batch.pro rather than having code for
;		each UNIX flavour present in this routine.
;       1.13    William Osborn
;               Increased version number to 1.7
;	 1.14     Richard Martin
;		    Increased version number to 1.9
;	1.15	Richard Martin
;		Added check for 'gzip'- it is used if it is in the
;		users path.
;		Increased version no to 1.10
;	1.16	Richard Martin
;		Increased version no to 1.11
;
; VERSION:
;       1.1	22-06-95
;	1.2	10-07-95
;	1.3	17-07-95
;	1.4	17-07-95
;	1.5	15-11-95
;	1.6	13-05-96
;	1.7	05-07-96
;	1.8	11-07-96
;	1.9	20-09-96
;	1.10	11-10-96
;       1.11	14-10-96
;	1.12	18-10-96
;       1.13	25-11-96
;	1.14	01-11-97
;	1.15	22-09-99
;	1.16	15-11-00
;			
;-----------------------------------------------------------------------------


PRO ADAS309,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS309 V1.11'
    lpend = 0
    gomenu = 0
    ipset = 0
    ipbatset = 0
    l2batch = 0				;Marker for whether a batch file
					;has been created.
    dsbatch = ''			;batch-file name
    deffile = userroot+'/defaults/adas309_defaults.dat'
    device = ''
    bitfile = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf01/'
        inval.userroot = userroot+'/adf01/'

    end else begin

        inval = { 							$
		ROOTPATH:userroot+'/adf01/', 				$
		FILE:'', 						$
		CENTROOT:centroot+'/adf01/', 				$
		USERROOT:userroot+'/adf01/' }

    procval = {NEW:-1}

    outval = { 								$
                TEXOUT:0, TEXAPP:0, 					$
                TEXREP:0, TEXDSN:'', 					$
                TEXDEF:'paper.txt',TEXMES:'',				$
		DATOUT:0, DATAPP:0,					$
		DATREP:0, DATDSN:'',					$
		DATDEF:userroot+'/pass/data.pass', DATMES:''		$
             }

    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas309.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c9spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    c9spf0, pipe, inval, dsfull, rep, FONT=font_large

    ipset = 0
    ipbatset = 0

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from cxsetp ****
		;*************************************

    cxsetp, pipe, mxtab, mxgrf, symbd, idz0, symbr, irz0, irz1, irz2, $
            ngrnd, ntot



LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c9ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    c9ispf, pipe, lpend, procval, dsfull,          			$
            symbd, idz0, symbr, irz0, irz1, irz2,			$
	    ntot, ngrnd, gomenu, bitfile,                              	$
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

 
                ;******************************************
		;**** If cancel selected then goto 100 ****
		;**** First check whether a batch job  ****
		;**** has been set up If it has then   ****
                ;**** start it off.		       ****
		;******************************************

LABELBATCH:

    if lpend eq 1 or gomenu eq 1 then begin
        if (l2batch ne 0) then begin 		;Kick the batch job off
            dscontrol = 'control'+strmid(dsbatch, 5, 4)+'.tmp'
            dsinfo = 'info'+strmid(dsbatch, 5, 4)+'.tmp'
            openw, conunit, dscontrol, /GET_LUN
            openw, infunit, dsinfo, /GET_LUN
            printf, conunit, fortdir+'/adas309b.out < '+dsinfo 
            printf, conunit, 'rm -f '+dsinfo 
            printf, conunit, 'rm -f '+dscontrol
            printf, conunit, 'rm -f '+dsbatch
            yesbatch = 1
            iunt07 = 30
            iunt14 = 31
            printf, infunit, yesbatch
            printf, infunit, dsbatch
            printf, infunit, iunt07  
            printf, infunit, iunt14
            close, infunit, conunit
            spawn, 'chmod u+x '+dscontrol 
    		spawn,'which gzip', result
    		result2=findfile(result(0))
    		if result2(0) ne '' then begin
	  		spawn, result2(0)+' -d ./'+dsbatch
    		endif else begin
	  		  spawn, 'uncompress ./'+dsbatch
    		endelse


            batch, dscontrol, title='ADAS309: INFORMATION', $
              jobname='NO. '+strmid(dsbatch, 5, 4)

            dsbatch = ''
            l2batch = 0

        endif
        outval.datapp = 0
    endif

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse
     
    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c9spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    if (l2batch ne 0) then begin
        outval.datapp = 1
        outval.datrep = 0
    endif
    c9spf1, pipe, lpend, outval, dsfull, header, bitfile, gomenu,	$
		FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;**** If menu button clicked, erase output message ****
		;**** and goto LABELBATCH in order to set off batch****
		;**** job if required before exiting		   ****

    if gomenu eq 1 then begin
	outval.texmes=''
	outval.datmes=''
	goto, LABELBATCH
    endif else begin
        printf, pipe, 0
    endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.datmes = ''
        goto, LABEL200
    end

		;***********************************************
		;**** Now begin the second part of the      ****
		;**** calculation depending on what action  ****
		;**** has been requested.		    ****
                ;***********************************************

    if lpend eq 0 then begin			;'Run Now' selected
        ipset = ipset + 1			; Increase 'run number'
        if (outval.datrep eq 1) then ipset = 1
        c9spf2, pipe, outval, dsfull, header, procval, 			$
        idz0, irz0, irz1, irz2, symbr, symbd, ipset, date,    		$
        fortdir, FONT=font_large
    endif else if lpend eq 2 then begin		;'Run in batch' selected
        ipbatset = ipbatset + 1			;Increase run number
        if (l2batch eq 0) then l2batch = 1
	c9spf3, pipe, outval, dsfull, header, procval,  		$
        idz0, irz0, irz1, irz2, symbr, symbd, ipbatset, date, 		$
	dsbatch, fortdir, l2batch, font = font_large
    endif



		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:

		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
