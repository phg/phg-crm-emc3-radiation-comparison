; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas309/c9ispf.pro,v 1.4 2004/07/06 11:57:08 whitefor Exp $ Date $Date: 2004/07/06 11:57:08 $ 
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C9ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS309 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routines simply pops up the processing screen
;	to determine how the user wishes to process the input 
;	dataset.  When the user's interactions are complete 
;	the 'cancel/done' signal is piped to the FORTRAN, but unlike
;	in other adas programs the actual data is held for later transfer.
;	This is due to the fact that for adas309 there are two FORTRAN
;	programs to consider.
;	Communications are to the FORTRAN subroutine C9ISPF.
;
; USE:
;	The use of this routine is specific to ADAS309, see adas309.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS309 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS309 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas309.pro.  If adas309.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
; 		                      	new   : 0,              $
;              		         	title : '',             $
;					ramsno: 0.0,		$
;					tiev  : 0.0 ,      	$
;					tev   : 0.0 ,      	$
;					densz : 0.0 ,      	$
;					dens  : 0.0 ,      	$
;					zeff  : 0.0 ,		$
;					bmag  : 0.0 ,		$
;					bmeng : 0.0 ,		$
;					ntu   : 0,		$
;					ntl   : 0,		$
;					nbmeng: 0,		$
;					ndensz: 0,		$
;					ntiev : 0,		$
;					nzeff : 0,		$
;					nbmag : 0,		$
;					bmenga: fltarr(24),	$		
;					densza: fltarr(24),	$
;					tieva : fltarr(12),	$
;					zeffa : fltarr(12),	$
;					bmaga : fltarr(12),	$
;					itable: 0,		$
;					ibstat: 0,		$
;					iemms : 0,		$
;					itheor: 0		$
;             			  }
;
;		  See cw_adas309_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;       ngrnd   - (Int) Minimum allowed N quantum number.
;
;       ntot    - (Int) Maximum allowed N quantum number.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;	
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS309_PROC	Invoke the IDL interface for ADAS309 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	One way communication with ADAS309 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 22-Jun-1995
;
; MODIFIED:
;	1.1             Tim Hammond                     22-06-95
;	1.2		Tim Hammond			10-07-95
;       1.3     	Tim Hammond                     10-07-95
;	1.4		William Osborn			05-07-96
;
; VERSION:
;	1.1		First release		
;	1.2		Tidied up code and comments.
;       1.3     	Included error trapping
;	1.4		Added menu button code
;
;-
;-----------------------------------------------------------------------------


PRO c9ispf, pipe, lpend, procval, dsfull,	 			$
            symbd, idz0, symbr, irz0, irz1, irz2,			$
	    ntot, ngrnd, gomenu, bitfile,				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}


    lpend = 0				;Cancel/done flag


		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        procval = {		          $
		  new   : 0 ,             $
		  title : '',		  $
                  ramsno: 0.0,            $
                  tiev  : 0.0 ,           $
                  tev   : 0.0 ,           $
                  densz : 0.0 ,           $
                  dens  : 0.0 ,           $
                  zeff  : 0.0 ,           $
                  bmag  : 0.0 ,           $
                  bmeng : 0.0 ,           $
                  ntu   : 0,              $
                  ntl   : 0,              $
                  nbmeng: 0,              $
                  ndensz: 0,              $
                  ntiev : 0,              $
                  nzeff : 0,              $
                  nbmag : 0,              $
                  bmenga: fltarr(24),     $
                  densza: fltarr(24),     $
                  tieva : fltarr(12),     $
                  zeffa : fltarr(12),     $
                  bmaga : fltarr(12),     $
		  itable: 0,		  $
		  ibstat: 0,		  $
		  iemms : 0,		  $
		  itheor: 0		  $
	        }
    end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas309_proc, procval, dsfull, action, 				$
                  symbd, idz0, symbr, irz0, irz1, irz2,                 $
        	  ntot, ngrnd, bitfile,					$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are only three possible events ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************

    gomenu = 0
    if action eq 'Done' then begin
        lpend = 0
    end else if action eq 'Menu' then begin
	lpend = 0
	gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend


END
