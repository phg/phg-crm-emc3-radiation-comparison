;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  c9cxee
;
; PURPOSE    :  Calculates the L-resolved effective emissivity rate
;               coefficient for the given transition.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                c9cxee
;
;               NAME      I/O    TYPE    DETAILS
;
;               mxneng     I     int     Maximum no. of energies in data set.
;               mxnshl     I     int     Maximum number of n shells.
;               ngrnd      I     int     Principal quantum number of ground state.
;               ntot       I     int     Principal quantum number of highest bound state.
;               nbot       I     int     Minimum principal quantum number for rate tables.
;               ntop       I     int     Maximum principal quantum number for rate tables.
;               irz0       I     int     Receiver nuclear charge.
;               irz1       I     int     Receiver ion initial charge.
;               ramsno     I     float   Receiver atomic mass.
;               tev        I     float   Electron temperature. units: ev
;               tiev       I     float   Ion temperature. units: ev
;               dens       I     float   Electron density. units: cm-3
;               densz      I     float   Plasma ion density. units: cm-3
;               zeff       I     float   Effective ion charge.
;               bmag       I     float   Plasma magnetic induction. units: tesla
;               bmeng      I     float   Beam energy. units: ev/amu
;               itheor     I     int     Charge exchange model option.
;                                          1 => use input data set.
;                                          2 => use eikonal model.
;               ibstat     I     int     Donor state for eikonal model.
;                                          1 => h(1s)    
;                                          2 => h(2s)    
;                                          3 => h(2p)    
;                                          4 => he(1s2)  
;                                          5 => he(1s2s) 
;               iemms      I     int     Emission measure model option.
;                                          1 => charge exchange.
;                                          2 => electron impact excitation.
;               ntl        I     int     Lower principal quantum number of transition.
;               ntu        I     int     Upper principal quantum number of transition.
;               nminf      I     int     Lowest n-shell for which data read.
;               nmaxf      I     int     Highest n-shell for which data read.
;               nenrgy     I     int     Number of energies read from data set.
;               enrgya()   I     float   Collision energies read from input data set.
;                                        Units: ev/amu
;                                          dimension: energy index
;               alphaa()   I     float   Extrapolation parameter alpha read from
;                                        Input data set.
;                                          dimension: energy index
;               xsecna(,)  I     float   N-resolved charge exchange cross-sections
;                                        Read from input data set. units: cm2
;                                          1st dimension: energy index
;                                          2nd dimension: n-shell
;               fracla(,)  I     float   L-resolved charge exchange cross-sections.
;                                        Fraction of n-resolved  data.
;                                          1st dimension: energy index
;                                          2nd dimension: indexed by i4idfl(n,l)
;
;               erate      O     float   Effective emissivity rate coefficient for
;                                        Requested transition spectrum line. units: cm3 sec-1
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  08/12/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    08/12/12
;
;-
;----------------------------------------------------------------------

PRO c9cxee, mxneng , mxnshl , ngrnd  , ntot    , $
            nbot   , ntop   , irz0   , irz1    , $
            ramsno , tev    , tiev   , dens    , $
            densz  , zeff   , bmag   , bmeng   , $
            itheor , ibstat , iemms  , ntu     , $
            ntl    , nminf  , nmaxf  , nenrgy  , $
            enrgya , alphaa , xsecna , fracla  , $
            erate

fortdir = getenv('ADASFORT')

ibstat  = long(ibstat)
iemms   = long(iemms)
irz0    = long(irz0)
irz1    = long(irz1)
itheor  = long(itheor)
mxneng  = long(mxneng)
mxnshl  = long(mxnshl)
nbot    = long(nbot)
nenrgy  = long(nenrgy)
ngrnd   = long(ngrnd)
nmaxf   = long(nmaxf)
nminf   = long(nminf)
ntl     = long(ntl)
ntop    = long(ntop)
ntot    = long(ntot)
ntu     = long(ntu)
alphaa  = double(alphaa)
bmag    = double(bmag)
bmeng   = double(bmeng)
dens    = double(dens)
densz   = double(densz)
enrgya  = double(enrgya)
fracla  = double(fracla)
ramsno  = double(ramsno)
tev     = double(tev)
tiev    = double(tiev)
xsecna  = double(xsecna)
zeff    = double(zeff)

erate   = 0.0D0

dummy = CALL_EXTERNAL(fortdir+'/c9cxee_if.so','c9cxee_if', $
                      mxneng , mxnshl , ngrnd  , ntot    , $
                      nbot   , ntop   , irz0   , irz1    , $
                      ramsno , tev    , tiev   , dens    , $
                      densz  , zeff   , bmag   , bmeng   , $
                      itheor , ibstat , iemms  , ntu     , $
                      ntl    , nminf  , nmaxf  , nenrgy  , $
                      enrgya , alphaa , xsecna , fracla  , $
                      erate  )


END
