;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas309_qeff
;
; PURPOSE    :  Returns the effective emissivity for CX capture for a
;               set of conditions. A simpler interface to c9cxee.pro
;               which does not require multiple file opening and closing.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                adas309_qeff
;
;               NAME      I/O    TYPE    DETAILS
;
;               adf01      I     str     adf01 CX cross section file
;               energy     I     float   Beam energies (eV/amu)
;               iz0        I     float   Receiver nuclear charge
;               mass       I     float   Receiver mass (amu)
;               upper      I     int     Upper n of required transition
;               lower      I     int     Lower n of required transition
;               te         I     float   Electron temperature. units: ev
;               tion       I     float   Ion temperature. units: ev
;               dens       I     float   Electron density. units: cm-3
;               dion       I     float   Plasma ion density. units: cm-3
;               zeff       I     float   Plasma ion density. units: cm-3
;               qeff()     O     float   Effective emissivity rate coefficient
;                                        units: cm3 sec-1
;
; KEYWORDS      help              -     displays help entry
;
; NOTES      :  One of ENERGY, TE, TION, DENS, DION or ZEFF may be a vector.
;
;               Calls fortran code.
;
;               More control is provided by calling c9cxee.pro but this is not vectorized
;               and opens/closes the adf01 file at each invocation.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  12-12-12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
;
; VERSION:
;       1.1    12-12-12
;
;-
;----------------------------------------------------------------------



PRO adas309_qeff, adf01  = adf01,   $
                  energy = energy,  $
                  iz0    = iz0,     $
                  mass   = mass,    $
                  upper  = upper,   $
                  lower  = lower,   $
                  te     = te,      $
                  tion   = tion,    $
                  dens   = dens,    $
                  dion   = dion,    $
                  zeff   = zeff,    $
                  qeff   = qeff

; If asked for help

if keyword_set(help) then begin
   doc_library, 'adas309_qeff'
   return
endif


; Check input - allow just on of the inputs to be a vector or insist that
;               there are all the same length

mess = 'Only one of ENERGY, TE, TION, DENS, DION or ZEFF can have more than 1 element ' + $
       'or they must all have the same length'

num_energy = n_elements(energy)
num_te     = n_elements(te)
num_tion   = n_elements(tion)
num_dens   = n_elements(dens)
num_dion   = n_elements(dion)
num_zeff   = n_elements(zeff)

if (num_energy + num_te + num_tion + num_dens + num_dion + num_zeff) EQ (6 * num_energy) then begin

   num       = num_energy
   energy_in = energy
   te_in     = te
   tion_in   = tion
   dens_in   = dens
   dion_in   = dion
   zeff_in   = zeff

endif else begin   

   if num_energy GT 1 then begin
      num = num_energy
      if (num_te + num_tion + num_dens + num_dion + num_zeff) GT 5 then message, mess
      energy_in = energy
      te_in     = dblarr(num) + te
      tion_in   = dblarr(num) + tion
      dens_in   = dblarr(num) + dens
      dion_in   = dblarr(num) + dion
      zeff_in   = dblarr(num) + zeff
   endif

   if num_te GT 1 then begin
      num = num_te
      if (num_energy + num_tion + num_dens + num_dion + num_zeff) GT 5 then message, mess
      energy_in = dblarr(num) + energy
      te_in     = te
      tion_in   = dblarr(num) + tion
      dens_in   = dblarr(num) + dens
      dion_in   = dblarr(num) + dion
      zeff_in   = dblarr(num) + zeff
   endif

   if num_tion GT 1 then begin
      num = num_tion
      if (num_energy + num_te + num_dens + num_dion + num_zeff) GT 5 then message, mess
      energy_in = dblarr(num) + energy
      te_in     = dblarr(num) + te
      tion_in   = tion
      dens_in   = dblarr(num) + dens
      dion_in   = dblarr(num) + dion
      zeff_in   = dblarr(num) + zeff
   endif

   if num_dens GT 1 then begin
      num = num_dens
      if (num_energy + num_te + num_tion + num_dion + num_zeff) GT 5 then message, mess
      energy_in = dblarr(num) + energy
      te_in     = dblarr(num) + te
      tion_in   = dblarr(num) + tion
      dens_in   = dens
      dion_in   = dblarr(num) + dion
      zeff_in   = dblarr(num) + zeff
   endif

   if num_dion GT 1 then begin
      num = num_dion
      if (num_energy + num_te + num_tion + num_dens + num_zeff) GT 5 then message, mess
      energy_in = dblarr(num) + energy
      te_in     = dblarr(num) + te
      tion_in   = dblarr(num) + tion
      dens_in   = dblarr(num) + dens
      dion_in   = dion
      zeff_in   = dblarr(num) + zeff
   endif

   if num_zeff GT 1 then begin
      num = num_zeff
      if (num_energy + num_te + num_tion + num_dens + num_dion) GT 5 then message, mess
      energy_in = dblarr(num) + energy
      te_in     = dblarr(num) + te
      tion_in   = dblarr(num) + tion
      dens_in   = dblarr(num) + dens
      dion_in   = dblarr(num) + dion
      zeff_in   = zeff
   endif

endelse

qeff = dblarr(num)



; Set dimensions following adas309

MXNENG = 40L
MXNSHL = 20L

read_adf01, file=adf01, fulldata=all

enrgya = dblarr(MXNENG)
alphaa = dblarr(MXNENG)
xsecna = dblarr(MXNENG,MXNSHL)
fracla = dblarr(MXNENG,(MXNSHL*(MXNSHL+1))/2)
lforma = lonarr(MXNENG)
xlcuta = dblarr(MXNENG)
pl2a   = dblarr(MXNENG)
pl3a   = dblarr(MXNENG)

enrgya[0:all.nenrgy-1] = all.enrgya
alphaa[0:all.nenrgy-1] = all.alphaa
xsecna[0:all.nenrgy-1, 0:all.nmax-1] = all.signa
fracla[0:all.nenrgy-1, 0:(all.nmax * (all.nmax+1)/2)-1] = all.sigla
lforma[0:all.nenrgy-1] = all.lforma
xlcuta[0:all.nenrgy-1] = all.xlcuta
pl2a[0:all.nenrgy-1] = all.pl2a
pl3a[0:all.nenrgy-1] = all.pl3a


cxfrac, mxneng, mxnshl, all.nenrgy, all.nmin, all.nmax, all.lsetl, $
        xsecna, fracla

cxextr, mxneng, mxnshl, all.nmin, all.nmax, all.nenrgy,  $
        all.lparms, alphaa, lforma, xlcuta, pl2a,        $
        pl3a, xsecna, fracla


; User input

irz0   = long(iz0)
irz1   = long(iz0)
ramsno = double(mass)

ntu    = long(upper)
ntl    = long(lower)

; Ignore Eikonal model ans set emissioon model to CX

itheor = 1L
ibstat = 1L
iemms  = 1L

; Follow adas309 for these values

ntot   = MXNSHL
ntop   = MXNSHL
ngrnd  = 1L
nbot   = 2L
nminf  = all.nmin
nmaxf  = all.nmax
nenrgy = all.nenrgy

nenrgy = all.nenrgy

; Ignore bmag

bfield = 3.0D0

; Loop over whichever input varies

for j = 0, num-1 do begin

   bmeng  = energy_in[j]
   tev    = te_in[j]
   tiev   = tion_in[j]
   dense  = dens_in[j]
   densz  = dion_in[j]
   zeffe  = zeff_in[j]

   c9cxee, mxneng , mxnshl , ngrnd  , ntot    , $
           nbot   , ntop   , irz0   , irz1    , $
           ramsno , tev    , tiev   , dense   , $
           densz  , zeffe  , bfield , bmeng   , $
           itheor , ibstat , iemms  , ntu     , $
           ntl    , nminf  , nmaxf  , nenrgy  , $
           enrgya , alphaa , xsecna , fracla  , $
           rate

   qeff[j] = rate

endfor

if num EQ 1 then qeff = qeff[0]

END
