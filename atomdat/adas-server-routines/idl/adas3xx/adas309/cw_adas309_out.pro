; Copyright (c) 1995, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas309/cw_adas309_out.pro,v 1.5 2004/07/06 12:40:09 whitefor Exp $ Date $Date: 2004/07/06 12:40:09 $ 
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS309_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS309 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of two
;	output file widgets cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output. The data output file produces passing file data in the 
;	format adf12.  This widget also includes a button for browsing the 
;	comments from the input dataset, a 'Cancel' button and 'Run Now' 
;	and 'Run in batch' buttons.
;	The compound widgets cw_adas_outfile.pro included in this file 
;	are self managing.  This widget only handles events from the 
;	'Cancel' and 'Run...' buttons.
;
;	This widget only generates events for the 'Run..', 'Cancel' and
;	'Menu' buttons.
;
; USE:
;	This routine is unique to adas309.                              
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See 
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { 						$
;			TEXOUT:0, TEXAPP:0 , 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;			DATOUT:0, DATAPP:0,  				$
;			DATREP:0, DATDSN:'', 				$
;			DATDEF:userroot+'/pass/data.pass', DATMES:''  	$
;		      }
;
;	  For CW_ADAS_OUTFILE tabular output (paper.txt);
;		TEXOUT	Integer; Activation button 1 on, 0 off
;		TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;		TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;		TEXDSN	String; Output file name
;		TEXDEF	String; Default file name
;		TEXMES	String; file name error message
;	  For CW_ADAS_OUTFILE data output (data.pass)
;		DATOUT  Integer; Activation button 1 on, 0 off
;		DATAPP  Integer; Append button 1 on, 0 off, -1 no button
;		DATREP  Integer; Replace button 1 on, 0 off, -1 no butn
;		DATDSN  String; Output file name
;		DATDEF  String; Default file name
;		DATMES  String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT309_GET_VAL()
;	OUT309_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 29th June 1995
;
; MODIFIED:
;	1.1	Tim Hammond		29/06/95
;	1.2	TIm Hammond		10/07/95
;	1.3	Tim Hammond		10/07/95
;	1.4	William Osborn		05-07-96
;	1.5	William Osborn		25-07-96
; VERSION:
;	1.1	First Release	
;	1.2	Tidied up code and comments
;	1.3	Corrected minor syntax errors
;	1.4	Added menu button
;	1.5	Removed xsize=40 keyword to widget_label
;-
;-----------------------------------------------------------------------------

FUNCTION out309_get_val, id

                ;**** Return to caller on error ****

  ON_ERROR, 2

                ;**** Retrieve the state ****

  parent=widget_info(id, /parent)
  widget_control, parent, get_uvalue=state, /no_copy

		;**** Get text output settings ****

  widget_control, state.paperid, get_value=papos
  widget_control, state.passid, get_value=pspos

  os = {   out309_set, 							$
	   TEXOUT:papos.outbut,  TEXAPP:papos.appbut, 		 	$
	   TEXREP:papos.repbut,  TEXDSN:papos.filename, 	 	$
	   TEXDEF:papos.defname, TEXMES:papos.message,		 	$
	   DATOUT:pspos.outbut,  DATAPP:pspos.appbut,		 	$
	   DATREP:pspos.repbut,	 DATDSN:pspos.filename,		 	$
	   DATDEF:pspos.defname, DATMES:pspos.message		 	$
        }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out309_event, event


                ;**** Base ID of compound widget ****

  parent=event.top

                ;**** Retrieve the state ****

  widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Menu button ****
		;*********************

    state.outid: begin
        new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
    end

		;*****************************
		;**** Run In Batch button ****
		;*****************************

    state.batid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	  if (os.texout eq 1 and strtrim(os.texmes) ne '')		$
	  or (os.datout eq 1 and strtrim(os.datmes) ne '') then error=1

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      widget_control,state.messid,set_value = 			$
	      '**** Error in output settings ****'
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
	      ACTION:'Run In Batch'}
	  endelse

        end

		;************************
		;**** Run Now button ****
		;************************

    state.runid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	  if (os.texout eq 1 and strtrim(os.texmes) ne '')		$
	  or (os.datout eq 1 and strtrim(os.datmes) ne '') then error=1

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      widget_control, state.messid, set_value= 			$
	      '**** Error in output settings ****'
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
	      ACTION:'Run Now'}
	  endelse

        end

    ELSE: new_event = 0L

  ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas309_out, parent, dsfull, bitfile, VALUE=value, 		$
		UVALUE=uvalue, FONT=font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas309_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out309_set, 						$
			TEXOUT:0, TEXAPP:0, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'paper.txt', TEXMES:'', 			$
			DATOUT:0, DATAPP:0,				$
			DATREP:0, DATDSN:'',				$
			DATDEF:'data.pass', DATMES:''			$
              }
  END ELSE BEGIN
	os = {out309_set, 						$
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes, 	$
			DATOUT:value.datout, DATAPP:value.datapp,       $
                        DATREP:value.datrep, DATDSN:value.datdsn,       $
                        DATDEF:value.datdef, DATMES:value.datmes	$
	      }
  END

		;**********************************************
		;**** Create the 309 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 				$
		      EVENT_FUNC = "out309_event", 			$
		      FUNC_GET_VALUE = "out309_get_val", 		$
		      /COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font)

                ;**** Widget for data output ****

  outfval = { OUTBUT:os.datout, APPBUT:os.datapp, REPBUT:os.datrep, 	$
              FILENAME:os.datdsn, DEFNAME:os.datdef, MESSAGE:os.datmes }
  base1 = widget_base(cwid, /row, /frame)
  passid = cw_adas_outfile(base1, OUTPUT='Data Output', 		$
                           VALUE=outfval, FONT=font)

		;**** Widget for text output ****

  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, 	$
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(cwid, /row, /frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                            VALUE=outfval, FONT=font)

		;**** Error message ****

  messid = widget_label(cwid, value=' ', font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid, /row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base, value='Cancel', font=font)
  runid = widget_button(base, value='Run Now', font=font)
  batid = widget_button(base, value='Run in Batch', font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

  new_state = { PAPERID:paperid,  PASSID:passid, BATID:batid, 		$
		CANCELID:cancelid, RUNID:runid, MESSID:messid, 		$
		OUTID:outid }

                ;**** Save initial state structure ****

  widget_control, parent, set_uvalue=new_state, /no_copy
  RETURN, cwid

END

