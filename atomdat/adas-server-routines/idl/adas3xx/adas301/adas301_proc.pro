; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas301/adas301_proc.pro,v 1.3 2004/07/06 10:27:37 whitefor Exp $ Date $Date: 2004/07/06 10:27:37 $                                 
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS301_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS301
;	processing.
;
; USE:
;	This routine is ADAS301 specific, see c1ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas301_proc.pro.
;
;		  See cw_adas301_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NDEIN	- Integer : Maximum number of Energies allowed.
;
;	NENRGY  - Integer : Maximum number of energies from data file.
;
;	NENER   - Integer : Number of energies from input data file
;
;	EVALS(,)- Double array : Electron temperatures from input data file
;				first dimension - ndenrgy
;				 Second dimension - 3
;				                  - 1 => Kelvin
;						    2 => eV
;						    3 => Reduced
;				 
;       LSETL   - integer - indicating whether data is l resolved
;       LSETM   - integer - indicating whether data is m resolved
;       NMIN    - integer - lowest n-shell for which data read
;       NMAX    - integer - highest n-shell for which data read
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;
; CALLS:
;	CW_ADAS301_PROC	Declares the processing options widget.
;	ADAS301_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC301_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 01-May-1995 
;
; MODIFIED:
;	1.1     Tim Hammond
;		First Release
;	1.2     Tim Hammond
;		Tidied code up
;	1.3	William Osborn
;		Added menu button code and dynlabel
;
; VERSION:
;	1.1	01-05-95
;	1.2	01-05-95
;	1.3	05-06-96
;
;-
;-----------------------------------------------------------------------------


PRO adas301_proc_ev, event


  COMMON proc301_blk,action,value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****
		      widget_control,event.id,get_value=value 
		      widget_control,event.top,/destroy

	           end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas301_proc, procval, dsfull, $
		  ndein, nenrgy, nener, $
		  evals, act, $
                  lsetl, lsetm, nmin, nmax, bitfile,  $
		  FONT_LARGE=font_large, FONT_SMALL=font_small, $
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

  COMMON proc301_blk,action,value

		;**** Copy "procval" to common ****

  value = procval

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

  procid = widget_base(TITLE='ADAS301 PROCESSING OPTIONS', $
					XOFFSET=100,YOFFSET=100)

		;**** Declare processing widget ****

  cwid = cw_adas301_proc(procid, dsfull,  $
                         ndein, nenrgy, nener, $
                  	 evals, act, $
                         lsetl, lsetm, nmin, nmax, bitfile,  $
			 PROCVAL=value, $
		 	 FONT_LARGE=font_large, FONT_SMALL=font_small, $
			 EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****

  xmanager,'adas301_proc',procid,event_handler='adas301_proc_ev', $
					/modal,/just_reg

		;*** copy value back to procval for return to c1ispf ***

  act = action
  procval = value
 
END

