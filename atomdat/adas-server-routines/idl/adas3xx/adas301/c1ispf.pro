; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas301/c1ispf.pro,v 1.3 2004/07/06 11:47:31 whitefor Exp $ Date $Date: 2004/07/06 11:47:31 $                           
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C1ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS301 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS301
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS301
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	C1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS301, see adas301.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS301 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS301 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas301.pro.  If adas301.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			new   : 0 ,             $
;                			title : '',             $
;                			ifout : 1,              $
;					ldfit : 0,              $
;					maxe  : 0, 		$
;                			tine  : temp_arr,       $
;                			lfsel : 0,              $
;                			tolval: 5,              $
;					losel : 0,		$
;                                       qnsel : 0,              $
;					qnnval : nmin,          $
;					qnlval : 0,		$
;					qnmval : 0		$
;              			}
;
;
;		  See cw_adas301_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS301_PROC	Invoke the IDL interface for ADAS301 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS301 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 01-May-1995
;
; MODIFIED:
;	1.1     Tim Hammond
;		First Release
;	1.2     Tim Hammond
;		Tidied code up
;	1.3	William Osborn
;		Added menu button code
;
; VERSION:
;	1.1	01-05-95
;	1.2	01-05-95
;	1.3	05-06-96
;
;-
;-----------------------------------------------------------------------------


PRO c1ispf, pipe, lpend, procval, dsfull, bitfile, gomenu, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************
  lpend = 0
  ndein = 0
  nblock = 0
  nenrgy = 0
  nener = 0
  lsetl = 0
  lsetm = 0
  nmin = 1
  nmax = 2

		;********************************
		;**** Read data from fortran ****
		;********************************

  readf, pipe, ndein
  readf, pipe, nblock
  readf, pipe, nenrgy
  readf, pipe, nener
	

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

  evals = dblarr(3,nenrgy)

		;****************************************************
		;*** Define some variables used to read arrays in ***
		;****************************************************

  temp = 0.0D
  itemp = 0
  
		;********************************
                ;**** Read data from fortran ****
                ;********************************

  for i = 0, 2 do begin 
     for j = 0, nener-1 do begin
        readf, pipe, temp
        evals(i,j) = temp
     endfor
  endfor	

  readf, pipe, lsetl
  readf, pipe, lsetm
  readf, pipe, nmin
  readf, pipe, nmax


		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
  if (procval.new lt 0) then begin
     procval = {			$
		new   : 0 ,             $
		title : '',		$
		maxe  : nenrgy,         $
                ifout : 1, 		$
		eine  : evals(0,*),     $
		lfsel : 0,		$
		tolval: 5,              $
		losel : 0,		$
                qnsel : 0,		$
		qnnval : nmin,          $
		qnlval : 0,		$
		qnmval : 0		$
	      }
  end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

  adas301_proc, procval, dsfull,  $
		ndein, nenrgy, nener, $ 
		evals, action, $
                lsetl, lsetm, nmin, nmax, bitfile, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

  if action eq 'Done' then begin
      lpend = 0
  endif else if action eq 'Menu' then begin
      lpend = 0
      gomenu = 1
  endif else begin
      lpend = 1
  endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

  printf, pipe, lpend
  printf,pipe,procval.title,format='(a40)'
  printf,pipe,procval.ifout
  printf,pipe,procval.maxe
  for i = 0, procval.maxe-1 do begin
     printf, pipe, procval.eine(i)
  endfor
  printf,pipe,procval.lfsel
  if (procval.lfsel eq 1) then begin
     printf,pipe,procval.tolval
  endif
  if (procval.qnsel) then begin
      printf, pipe, procval.qnnval
      if (lsetl) then begin
          printf, pipe, procval.qnlval
      endif else begin
          printf, pipe, -1
      endelse
      if (lsetm) then begin
          printf, pipe, procval.qnmval
      endif else begin
          printf, pipe, -1
      endelse
  endif else begin
      printf, pipe, 0
      printf, pipe, -1
      printf, pipe, -1
  endelse
  printf, pipe, procval.losel

END
