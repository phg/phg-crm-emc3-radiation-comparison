; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas301/adas301_plot.pro,v 1.3 2004/07/06 10:27:28 whitefor Exp $ Date $Date: 2004/07/06 10:27:28 $                                    
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS301_PLOT
;
; PURPOSE:
;	Generates ADAS301 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOT301 actually plots the
;	graph.
;
; USE:
;	This routine is very similar to that used for ADAS201.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user added to graph title.
;
;	TEMP	- Double array; Energies from input data file.
;	
;	RATE    - Double array; Cross-section from input data file
;
;	TOSA	- Double array; Selected energies for spline fit
;
;	ROSA	- Double array; Cross-sections for spline fit
;
;	TOMA	- Double array; Selected energies for minimax fit
;
;	DATE	- String; Date of graph production
;
;	LDEF1	- Integer; 1 - use user entered graph scales
;			   0 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
;	NMX	- Integer; Number of energies used for minimax fit
;
;	NENER   - Integer; Number of user entered energy values
;
;	NPSPL	- Integer; Number of interpolated values for spline.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT301		Make one plot to an output device for 301.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT301_BLK.
;
;	One other routine is included in this file;
;	ADAS301_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,   1-May-1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First Release
;	1.2	Tim Hammond
;		Tidied code up
;	1.3	William Osborn
;		Added menu button
;
; VERSION:
;	1.1	01-05-95
;	1.2	01-05-95
;	1.3	05-06-96
;-
;----------------------------------------------------------------------------

PRO adas301_plot_ev, event

  COMMON plot301_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenu

  newplot = 0
  print = 0
  done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		     end

	'bitbutton'   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			gomenu = 1
			done = 1
		     end
  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

  if done eq 0 then begin
		;**** Set graphics device ****
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

		;**** Draw graphics ****

    plot301, data.x , data.y, data.nener, data.nmx, data.npspl, $
	   data.nplots, data.title, data.xtitle, data.ytitle, $
	   data.ldef1, data.xmin, data.xmax, data.ymin, data.ymax 


	if print eq 1 then begin
	  message = 'Plot  written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end


  end

END

;----------------------------------------------------------------------------

PRO adas301_plot,  dsfull, $
		    titlx, titlm, utitle, date, $
		    temp, rate, tosa, rosa, toma, roma, $
                    ldef1, xmin, xmax, ymin, ymax, $
                    lfsel, losel, nmx, nener, npspl, $
                    hrdout, hardname, device, header, $
		    bitfile, gomenu, FONT=font

  COMMON plot301_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
  plotdev = device
  plotfile = hardname
  fileopen = 0
  gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

  title = strarr(5)

  title(0) = "CHARGE-EXCHANGE CROSS-SECTION VERSUS ENERGY "
  if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
     title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
  endif
  title(1) =  'ADAS    :' + strupcase(header)
  title(2) =  'FILE     :' + strcompress((titlx))
  if (lfsel eq 1) then begin
     title(3)  = "MINIMAX : " + strupcase(titlm)
     title(4)  = 'KEY     : (CROSSES - INPUT DATA) ' + $ 
		'(FULL LINE - SPLINE FIT)' + '   (DASH LINE - MINIMAX) '
  endif else begin
     title(3) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
     title(4) = ' ' 
  endelse
  
		;*************************************
		;***   Set up Y data for plots     ***
		;*** dimension of y axis depends   ***
		;*** upon selected fitting options ***
		;*************************************

  if ((lfsel eq 1) and (losel eq 1)) then begin
     ydim = nmx > npspl
  endif else begin
      if (lfsel eq 1) then ydim= nmx  else $
          if (losel eq 1) then ydim= npspl else $ 
             ydim = nener
  endelse

  y = make_array(3, ydim, /float)
  valid_data = where((rate gt 1e-37) and (rate lt 1e+37))
  if (valid_data(0) ge 0) then begin
     y(0, valid_data) = rate(valid_data)
     if (losel eq 1) then begin
        valid_data = where((rosa gt 1e-37) and (rosa lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(1, valid_data) = rosa(valid_data)
        endif else begin
           print, "ADAS301 : unable to plot spline fit data"
        endelse
     endif
     if (lfsel eq 1) then begin
        valid_data = where((roma gt 1e-37) and (roma lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(2, valid_data) = roma(valid_data)
        endif else begin
           print, "ADAS301 : unable to plot polynomial fit data"
        endelse
     endif
  endif else begin
           print, "ADAS301 : unable to plot data"
  endelse

  ytitle = "CROSS-SECTION (cm!E2!N)"

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

  if ((lfsel eq 1) and (losel eq 1)) then begin
     xdim = nmx > npspl
  endif else begin
      if (lfsel eq 1) then xdim= nmx  else $
          if (losel eq 1) then xdim= npspl else $ 
             xdim = nener
  endelse
  x = fltarr(3, xdim)

		;*** write x axis array ***

  x(0,0:nener-1) = temp
  nplots = 1

  if (losel eq 1) then begin
     x(1,0:npspl-1) = tosa
     nplots = nplots +2
  endif 
  if (lfsel eq 1) then begin
     x(2,0:nmx-1)  = toma
     nplots = nplots + 4
  endif
  xtitle = " ENERGY (eV/amu) "

  		;******************************************
		;*** if desired set up axis scales      ***
		;******************************************

  if (ldef1 eq 0) then begin
      xmin = min(x(*,1)) 	; data has to be monotonically increasing.
      xmin = xmin * 0.9
      xmax = max(x) * 1.1

      ymin = min(y(*,1))	; data has to be monotonically increasing.
      ymin = ymin * 0.9
      ymax = max(y) * 1.1
  endif

		;*************************************
		;**** Create graph display widget ****
		;*************************************

  graphid = widget_base(TITLE='ADAS301 GRAPHICAL OUTPUT', $
					XOFFSET=50,YOFFSET=0)
  bitval = bitfile + '/menu.bmp'
  cwid = cw_adas_graph(graphid,print=hrdout,FONT=font, bitbutton = bitval)

                ;**** Realize the new widget ****

  widget_control,graphid,/realize

		;**** Get the id of the graphics area ****

  widget_control,cwid,get_value=grval
  win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

  data = {    	y:y,	x:x,   nener:nener,    nmx:nmx,  npspl:npspl, $
	        nplots:nplots, title:title,  xtitle:xtitle, ytitle:ytitle, $
                ldef1:ldef1, $
		xmin:xmin,	xmax:xmax,	ymin:ymin,	ymax:ymax $
          }
 
 wset,win


  plot301, x , y, nener, nmx, npspl, nplots, title, xtitle, ytitle, $
	      ldef1, xmin, xmax, ymin, ymax 


		;***************************
                ;**** make widget modal ****
		;***************************

  xmanager,'adas301_plot',graphid,event_handler='adas301_plot_ev', $
                                        /modal,/just_reg

  gomenu = gomenucom

END
