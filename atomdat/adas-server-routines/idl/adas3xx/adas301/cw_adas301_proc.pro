; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas301/cw_adas301_proc.pro,v 1.8 2007/05/24 15:27:43 mog Exp $ Date $Date: 2007/05/24 15:27:43 $                              
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS301_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS301 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget ,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for energy/velocity data ,
;	   a button to enter default values into the table, 
;          a widget for selecting the quantum numbers to be used,
;	   a message widget, a 'Cancel', a 'Done' and a 'Menu' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button, the 'Done' button and the 'Menu' button.
;
;	This widget only generates events for the 'Done', 'Cancel' and 'Menu'
;	buttons.
;
; USE:
;	This widget is specific to ADAS301, see adas301_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	NDEIN   - Integer; number of energies selected
;
;	NENRGY  - Integer; maximum number of input data file temps.
;
;	NV	- Integer; number of energies from input data file.
;	
;	EVALS	- Double array ; Temperature values from input data file.
;	
;	The inputs map exactly onto variables of the same
;	name in the ADAS301 FORTRAN program.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc301_set, $
;			new   : 0,		$
;                	title : '',             $
;			maxe  : 0, 		$
;			ifout : 1 ,             $
;                	eine  : temp_arr,       $
;                	lfsel : 0,              $
;                	tolval: '5', 		$
;			losel : 0 ,		$
;                       qnsel : 0,              $
;                       qnnval : nmin,          $
;			qnlval : 0,             $
;			qnmval : 0		$
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		MAXE    Number of temperature values selected
;		IFOUT   Index indicating which units are being used
;		EINE    User supplied temperature values for fit.
;		LFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;		LOSEL   Flag whether or not interpolated values for spline
;			fit have been used.
;               QNSEL   Flag whether or not quantum number selection is
;                       required.
;               QNNVAL  Value of the principal quantum number N
;               QNLVAL  Value of the orbital quantum number L
;		QNMVAL  Value of the azimuthal quantum number M
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS301 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS301_TABLE Adas301 data table widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC301_GET_VAL()	Returns the current PROCVAL structure.
;	PROC301_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 11-Apr-1995 
;
; MODIFIED:
;	1.1     Tim Hammond
;		First Release
;	1.2     Tim Hammond
;		Tidied code up
;	1.3     Tim Hammond
;		Corrected label of energy table
;	1.4	William Osborn
;		Added menu button
;	1.5	Martin O'Mullane and Richard Martin
;		Added count to 'blanks = where(tabledata eq 0.0,count)' 
;               statement for check to see if blanks are required for table. 
;	1.6	Richard Martin
;		IDL 5.5 fixes.
;	1.7	Martin O'Mullane
;               Set default fonts to '' rather than one which may 
;               not be present.
;	1.8	Martin O'Mullane
;               Change definitions of m quantum number labels to remove
;               this option from view. Data is not passed from xxdata_01
;               anymore.
;
; VERSION:
;	1.1	21-04-95
;	1.2	01-05-95
;	1.3	29-06-95
;	1.4	05-06-96
;	1.5     20-11-98
;	1.6	08-03-02
;	1.7	13-05-2005
;	1.8	22-05-2007
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc301_get_val, id

  COMMON cw_proc301_blk, lsetl, lsetm, qnnval, nmin, nmax, $
                         qnlval, qnmval


                ;**** Return to caller on error ****
; ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

  widget_control,state.runid,get_value=title
  title = strcompress(title(0))
  title_len = strlen(title) 
  if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
  endif
  pad = (40 - title_len)/2 
  spaces = '                                         '
  title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  

		;****************************************************
		;**** Get new temperature data from table widget ****
		;**** If losel is selected                       ****
		;****************************************************

  if (state.losel eq 1)  then begin
     widget_control,state.tempid,get_value=tempval
     tabledata = tempval.value
     ifout = tempval.units+1

		;**** Copy out temperature values ****

     eine = dblarr(state.ndein)
     blanks = where(strtrim(tabledata(0,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

     if blanks(0) ge 0 then maxe=blanks(0) else maxe=state.ndein
     eine(0:maxe-1) = double(tabledata(0,0:maxe-1))

		;**** Fill in the rest with zeroes ****

     if maxe lt state.ndein then begin
       eine(maxe:state.ndein-1) = double(0.0)
     endif
  endif else begin	; *** use existing values
     ifout = 1
     eine = state.evals(0,*)
     maxe = state.maxe
  endelse
   

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

  widget_control,state.optid,get_uvalue=polyset,/no_copy
  lfsel = polyset.optionset.option[0]
  if (num_chk(polyset.optionset.value[0]) eq 0) then begin
     tolval = (polyset.optionset.value[0])
  endif else begin
     tolval = -999
  endelse
  widget_control,state.optid,set_uvalue=polyset,/no_copy

                ;*************************************************
                ;**** Get quantum number information          ****
                ;*************************************************

  if (state.qnsel eq 1) then begin
      widget_control, state.qnbut, get_value=get_qnnval, /no_copy
      qnncheck = byte(get_qnnval[0])
      if (max(qnncheck) lt 58 ) then begin
          qnnval = fix(get_qnnval[0])
      endif else begin
          qnnval = -99
      endelse
      widget_control, state.qnbut, set_value=get_qnnval, /no_copy
      if (lsetl) then begin
          widget_control, state.qlbut, get_value=get_qnlval, /no_copy
          qnlcheck = byte(get_qnlval[0])
          if (max(qnlcheck) lt 58 ) then begin
              qnlval = fix(get_qnlval[0])
          endif else begin
              qnlval = -99
          endelse
          widget_control, state.qlbut, set_value=get_qnlval, /no_copy
          if (lsetm) then begin
              widget_control, state.qmbut, get_value=get_qnmval,/no_copy
              qnmcheck = byte(get_qnmval[0])
              if (max(qnmcheck) lt 58 ) then begin
                  qnmval = fix(get_qnmval[0])
              endif else begin
                  qnmval = -99
              endelse
              widget_control, state.qmbut, set_value=get_qnmval,/no_copy
          endif
      endif
  endif 

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 		$
	        new   : 0, 			$
                title : title,          	$
		ifout : ifout,			$
		maxe  : maxe,			$
                eine  : eine,       		$
                lfsel : lfsel,              	$
                tolval: tolval,			$
		losel : state.losel,		$
                qnsel : state.qnsel,            $
                qnnval : qnnval,                $
		qnlval : qnlval,		$
		qnmval : qnmval			$
              }

   
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc301_event, event

  COMMON cw_proc301_blk, lsetl, lsetm, qnnval, nmin, nmax,    $
                         qnlval, qnmval

                ;**** Base ID of compound widget ****

  parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

                ;****************************************
                ;*** quantum nos. selected for output ***
                ;****************************************

    state.qbut: begin
                   if (state.qnsel eq 0) then state.qnsel = 1 else $
                       state.qnsel = 0
                   widget_control, state.qnbut, sensitive=state.qnsel
                   widget_control, state.qnblab, sensitive=state.qnsel
                   widget_control, state.qntot, sensitive=state.qnsel
                   if (lsetm) then begin
                      widget_control, state.qmbut, sensitive=state.qnsel
                      widget_control, state.qmblab, sensitive=state.qnsel
                      widget_control, state.qmtot, sensitive=state.qnsel
                   endif else begin
                      widget_control, state.qmbut, sensitive=0
                      widget_control, state.qmblab, sensitive=0
                      widget_control, state.qmtot, sensitive=0
                   endelse
                   if (lsetl) then begin
                      widget_control, state.qlbut, sensitive=state.qnsel
                      widget_control, state.qlblab, sensitive=state.qnsel
                      widget_control, state.qltot, sensitive=state.qnsel
                   endif else begin
                      widget_control, state.qlbut, sensitive=0          
                      widget_control, state.qlblab, sensitive=0          
                      widget_control, state.qltot, sensitive=0          
                   endelse
                   widget_control, state.qheadlabel, sensitive=state.qnsel
                 end

		;*********************************
		;*** ener. selected for output ***
		;*********************************

    state.lbut : begin
		   if (state.losel eq 0) then state.losel = 1 else $
			state.losel = 0 
		   widget_control, state.deftid, sensitive= state.losel
		   widget_control, state.tempid, sensitive = state.losel
		 end

		;*************************************
		;**** Default Energies button     ****
		;*************************************

    state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	action= popup(message='Confirm Overwrite values with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
		;**** Get current table widget value ****
 	   widget_control,state.tempid,get_value=tempval

		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;***********************************************

             units = tempval.units+1
	     nenrgy = state.nenrgy
   	     nv = state.nv 
 	     if nv gt 0 then begin
   	       tempval.value(0,0:nv-1) = $
		   strtrim(string(tempval.value(units,0:nv-1), $
		                               format=state.num_form),2)
 	  end

		;**** Fill in the rest with blanks ****

 	  if nv lt state.ndein then begin
   	    tempval.value(0,nv:state.ndein-1) = ''
 	  end

		;**** Copy new data to table widget ****

 	  widget_control,state.tempid, set_value=tempval

	end

      end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc301_get_val so it can be used ***
		;*** there.				  ***
		;********************************************

	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

		;**** check temp values entered ****

	if (ps.losel eq 1) then begin
	   if error eq 0 and ps.maxe eq 0 then begin
	      error = 1
	      message='Error: No velocities/energies entered.'
            endif
	endif

		;*** Check to see if sensible tolerance is selected.

	if (error eq 0 and ps.lfsel eq 1) then begin $
	    if (float(ps.tolval) lt 0)  or $
		          (float(ps.tolval) gt 100) then begin
	       error = 1
	       message='Error: Tolerance for polyfit must be 0-100% '
	    endif
	endif

                ;**** Check whether value of "N" is reasonable ****

        if (error eq 0 and ps.qnsel eq 1) then begin 
            qnntest = fix(ps.qnnval)
            if (fix(ps.qnnval) gt nmax) or $
            ((fix(ps.qnnval) lt nmin and fix(ps.qnnval) ne 0)) then begin
                    error = 1
                    message="Error: Principal quantum number N is"+     $
                            " out of range"
            endif
        endif

                ;**** Check whether value of "L" is reasonable ****

        if (error eq 0 and ps.qnsel eq 1 and lsetl) then begin
            qnltest = fix(ps.qnlval)
            if (qnltest gt (qnntest-1) or qnltest eq -99) then begin
                error = 1
                message="Error: Orbital quantum number L is"+         $
                        " out of range"
            endif
        endif

                ;**** Check whether value of "M" is reasonable ****

        if (error eq 0 and ps.qnsel eq 1 and lsetm) then begin
            qnmtest = fix(ps.qnmval)
            if (qnmtest gt (qnltest) or qnmtest eq -99) then begin
                error = 1
                message="Error: Azimuthal quantum number M is"+       $
                        " out of range"
            endif
        endif
             

		;**** return value or flag error ****

	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end
	

      end

		;*********************
                ;**** Menu button ****
		;*********************

      state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                       ACTION:'Menu'}
      end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state,/no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

function cw_adas301_proc, topparent, dsfull, $
 		ndein, nenrgy, nv,  $
		evals,  act, $
                lsetl, lsetm, nmin, nmax, bitfile, $
		procval=procval, uvalue=uvalue, $
		font_large=font_large, font_small=font_small, $
		edit_fonts=edit_fonts, num_form=num_form

  COMMON cw_proc301_blk, lsetlcom, lsetmcom, qnnvalcom, nmincom, $
                         nmaxcom, qnlvalcom, qnmvalcom

  lsetlcom = lsetl
  lsetmcom = lsetm
  qnnvalcom = procval.qnnval
  qnlvalcom = procval.qnlval
  qnmvalcom = procval.qnmval
  nmincom = nmin
  nmaxcom = nmax

		;**** Set defaults for keywords ****

  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then $
				edit_fonts = {font_norm:'',font_input:''}
  if not (keyword_set(num_form)) then num_form = '(E10.3)'

  if not (keyword_set(procval)) then begin
     ps = { $ 
		new   : 0, 		$
		title : '',             $
		maxe  : ndein,  	$
		ifout : 1,		$
                eine  : evals(1,*),     $
                lfsel : 0,              $
                tolval: '5',		$
		losel : 0 ,		$
                qnsel : 0,              $
                qnnval : nmin,          $
     		qnlval : 0,		$
		qnmval : 0		$
              }
  endif else begin
	ps = { $ 
		new   : procval.new,    $
                title : procval.title,  $
                maxe  : procval.maxe,   $
                ifout : procval.ifout,  $
                eine  : procval.eine,   $
                lfsel : procval.lfsel,  $
                tolval: procval.tolval, $
		losel : procval.losel,  $
                qnsel : procval.qnsel,  $
                qnnval : procval.qnnval,$
		qnlval : procval.qnlval,$
		qnmval : procval.qnmval $
	     }
  endelse

		;****************************************************
		;**** Assemble energy & velocity table data      ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare energies  table array             *****
		;**** col 1 has user energy values              *****
		;**** col 2-4 have energy values from files,    *****
		;**** which can have one of three possible units*****
		;****************************************************

  maxe = ps.maxe
  tabledata = strarr(4,ndein)

                ;*************************************
		;**** Copy out energy      values ****
		;**** number of energy            ****
		;**** values for this data block  ****
		;*************************************

  if (maxe gt 0) then begin
    
    tabledata(0,0:nv-1) = $
		strtrim(string(ps.eine(0:nv-1),format=num_form),2)
    tabledata(1,0:nv-1 ) = $
	        strtrim(string(evals(0,0:nv-1),format=num_form),2)
    tabledata(2,0:nv-1) = $
		strtrim(string(evals(1,0:nv-1),format=num_form),2)
    tabledata(3,0:nv-1) = $
                strtrim(string(evals(2,0:nv-1),format=num_form),2)

		;**** fill rest of table with blanks ****

    blanks = where(tabledata eq 0.0,count) 

   if count ne 0 then tabledata(blanks) = ' ' 

  end

		;********************************************************
		;**** Create the 301 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

  parent = widget_base(topparent, UVALUE = uvalue, $
			title = 'ADAS301 PROCESSING OPTIONS', $
			EVENT_FUNC = "proc301_event", $
			FUNC_GET_VALUE = "proc301_get_val", $
			/COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

  base = widget_base(topbase,/row)
  rc = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=38,font=font_large,/edit)

		;**** add dataset name and browse button ****

  rc = cw_adas_dsbr(topbase,dsfull,font=font_large)


		;**********************
		;**** Another base ****
		;**********************

  tablebase = widget_base(parent,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;**** and whether table used for text output ****
		;************************************************

  base = widget_base(tablebase,/column,/frame)

		;***************************************
		;*** "LOSEL" parameter switch **********
		;***************************************

  lbase = widget_base(base, /row)
  llabel = widget_label(lbase,                    $
           value="Select Velocities/Energies for output file")
  lbbase = widget_base(lbase, /row, /nonexclusive)
  lbut = widget_button(lbbase, value=' ')
  widget_control, lbut, set_button=ps.losel

		;********************************
		;**** Energy data table      ****
		;********************************

  colhead = [['Output','Input'], $
	     ['','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

  units = ps.ifout-1 
  unitname = ['Atomic units','cm/sec',$
              'eV/amu']

		;************************************************************
		;**** Two  columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three         ****
		;**** units but column 2 switches between sets 2,3 & 4   ****
		;**** in the input array tabledata as the units change.  ****
		;************************************************************

  unitind = [[0,0,0],[1,2,3]]
  table_title =  [ [" "],["Output Collision Velocities/Energies "]] 

		;****************************
		;**** table of data   *******
		;****************************
 
  tempid = cw_adas_table(base, tabledata, units, unitname, unitind, $
			UNITSTITLE = 'Velocity/Energy units', $
			COLEDIT = [1,0], COLHEAD = colhead, $
			TITLE = table_title, $
			ORDER = [1,0], $ 
			LIMITS = [1,1], CELLSIZE = 12, $
			/SCROLL, ytexsize = 7, NUM_FORM = num_form, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

  tbase = widget_base(base)
  deftid = widget_button(tbase,value=' Default Velocity/Energy Values  ',$
		font=font_large)

		;*** Make table non/sensitive as ps.losel ***

  widget_control, tempid, sensitive = ps.losel
  widget_control, deftid, sensitive = ps.losel

                ;**************************************
                ;**** A base for the quantum nos.  ****
                ;**************************************

  qnbase = widget_base(tablebase, /column, /frame)

                ;**************************************
                ;**** "QNSEL" parameter switch     ****
                ;**************************************

  qnpbase = widget_base(qnbase, /row)
  qnplabel = widget_label(qnpbase,                      $
             value = "Select quantum numbers for processing")
  qbutbase = widget_base(qnpbase, /row, /nonexclusive)
  qbut = widget_button(qbutbase, value=' ')
  widget_control, qbut, set_button=ps.qnsel

                ;**************************************
                ;**** Column headings              ****
                ;**************************************

  qgap = widget_label(qnbase, fon=font_small, value=" ")
  qheadlabel = widget_label(qnbase, font=font_small,        $
               value = "                                 Range:"+   $
               "      Total:")

                ;**************************************
                ;**** "N" Button  *********************
                ;**************************************

  qnbutbase = widget_base(qnbase, /row, /frame)
  n_title = "Principal quantum no.  N             "
  n_range = "("+strcompress(nmin, /remove_all)+    $
            " - "+strcompress(nmax, /remove_all)+")"
  n_titlen = strlen(n_title)
  n_rangelen = strlen(n_range)
  n_title= strmid(n_title, 0, (n_titlen - 1 - n_rangelen))
  n_title = n_title+n_range
  qnblab = widget_label(qnbutbase, value = n_title, font=font_large)
  if ( ((ps.qnnval lt nmin) and (ps.qnnval ne 0))      $
      or (ps.qnnval gt nmax)) then ps.qnnval=nmin
  qnstring=" "+strcompress(ps.qnnval, /remove_all)
  qnbut = widget_text(qnbutbase,  xsize = 3,/all_events,  $
          /editable, value=qnstring)
  qntot = widget_label(qnbutbase, value = "  [ 0]", font=font_small)

                ;**************************************
                ;**** "L" Button  *********************
                ;**************************************

  qlbutbase = widget_base(qnbase, /row, /frame)
  l_title = "  Orbital quantum no.  L   (0 - N-1)"
  qlblab = widget_label(qlbutbase, value = l_title, font=font_large)
  if ( ps.qnlval gt (ps.qnnval-1)) then ps.qnlval=0
  if ((ps.qnlval lt 0) and (ps.qnlval ne -99)) then ps.qnlval = -1
  qlstring=" "+strcompress(ps.qnlval, /remove_all)
  qlbut = widget_text(qlbutbase,  xsize = 3,/all_events,  $
          /editable, value=qlstring)
  qltot = widget_label(qlbutbase, value = "  [-1]", font=font_small)

                ;**************************************
                ;**** "M" Button  *********************
                ;**************************************

;   qmbutbase = widget_base(qnbase, /row, /frame)
;   m_title = "Azimuthal quantum no.  M     (0 - L)"
;   qmblab = widget_label(qmbutbase, value = m_title, font=font_large)
;   if ( ps.qnmval gt (ps.qnlval)) then ps.qnmval=0
;   if ((ps.qnmval lt 0) and (ps.qnmval ne -99)) then ps.qnmval = -1
;   qmstring=" "+strcompress(ps.qnmval, /remove_all)
;   qmbut = widget_text(qmbutbase,  xsize = 3,/all_events,  $
;           /editable, value=qmstring)
;   qmtot = widget_label(qmbutbase, value = "  [-1]", font=font_small)


; Minimal changes to remove m option from view

   qmbutbase = widget_base(qnbase, /row)
   qmblab    = widget_label(qmbutbase, value = "", font=font_small)
   qmbut     = widget_label(qmbutbase, value = "", font=font_small)
   qmtot     = widget_label(qmbutbase, value = "", font=font_small)

                ;**************************************
                ;**** Resolution of data label     ****
                ;**************************************

  res_title = "Data is N "
  if (lsetl) then begin
       if (not(lsetm)) then begin
           res_title=res_title+"and L"
       endif else begin
           res_title = res_title + ", L and M"
       endelse
  endif

  res_title = res_title+" resolved."
  qngap3 = widget_label(qnbase,font=font_small, value=" ")
  qtitlabel = widget_label(qnbase, font=font_small,        $
              value = res_title)

                ;**************************************
                ;**** Initially blank error message ***
                ;**************************************

  qngap2 = widget_label(qnbase,font=font_small, value=" ")
  qnerr = widget_label(qnbase,font=font_small, value= " ")

                ;**************************************
                ;**** Make quantum numbers sensitive **
                ;**************************************

  widget_control, qnbut, sensitive=ps.qnsel
  widget_control, qnblab, sensitive=ps.qnsel
  widget_control, qntot, sensitive=ps.qnsel
  if (lsetm) then begin
      widget_control, qmbut, sensitive=ps.qnsel
      widget_control, qmblab, sensitive=ps.qnsel
      widget_control, qmtot, sensitive=ps.qnsel
  endif else begin
      widget_control, qmbut, sensitive=0
      widget_control, qmblab, sensitive=0
      widget_control, qmtot, sensitive=0
  endelse
  if (lsetl) then begin
      widget_control, qlbut, sensitive=ps.qnsel
      widget_control, qlblab, sensitive=ps.qnsel
      widget_control, qltot, sensitive=ps.qnsel
  endif else begin
      widget_control, qlbut, sensitive=0         
      widget_control, qlblab, sensitive=0         
      widget_control, qltot, sensitive=0         
  endelse
  widget_control, qheadlabel, sensitive=ps.qnsel

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

  polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						; cw_opt_value.pro
                                                ; expects arrays
  polyset.option = [ps.lfsel]
  polyset.val = [ps.tolval]
  options = ['Fit Polynomial']
  optbase = widget_base(topbase,/frame)
  optid   = cw_opt_value(optbase, options, $
			       title='Polynomial Fitting',	$
			       limits = [0,100], 		$
			       value=polyset, font=font_large 	$
			     )

		;**** Error message ****

  messid = widget_label(parent,font=font_large, $
	value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid = widget_button(base,value='Done',font=font_large)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

  new_state = { runid:runid,  		$
		outid:outid,		$
		messid:messid, 		$
		deftid:deftid,		$
		tempid:tempid, 		$
		losel : ps.losel, 	$
                qnsel : ps.qnsel,       $
                qbut : qbut,            $
                qnbut : qnbut,          $
                qlbut : qlbut,          $
                qmbut : qmbut,          $
                qnblab : qnblab,        $
                qlblab : qlblab,        $
                qmblab : qmblab,        $
                qntot  : qntot,         $
		qmtot  : qmtot,		$
		qltot  : qltot,		$
		qheadlabel:qheadlabel,	$
                lbut  : lbut,		$
		optid:optid,		$
		cancelid:cancelid,	$
		doneid:doneid, 		$
		dsfull:dsfull,		$
		ndein:ndein,            $
		nenrgy:nenrgy,		$
 		nv:nv,			$
		maxe:maxe, 		$
		evals:evals,		$
		font:font_large,	$
		num_form:num_form 	$
	      }

                 ;**** Save initial state structure ****

   widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, parent

END

