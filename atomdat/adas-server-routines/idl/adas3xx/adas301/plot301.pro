; Copyright (c) 1995, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas301/plot301.pro,v 1.3 2004/07/06 14:30:58 whitefor Exp $ Date $Date: 2004/07/06 14:30:58 $                             
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT301
;
; PURPOSE:
;	Plot one graph for ADAS301.
;
; EXPLANATION:
;	This routine plots ADAS301 output for a single graph.
;
; USE:
;	This routine is very similar to that for adas201.    
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	NENER   - Integer : Number of data points and spline fit points
;
;	NMX     - Integer : Number of points used in polynomial fit.
;
;	NPSPL	- Integer : Number of intepolated points for spline fit.
;
;	NPLOTS  - Integer : type of plots  1: Data from file only
;					   3: Data and spline fit
;					   5: Data and minmax fit
;					   7: Data, spline, & minimax fits.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  1-May-1995 
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First version
;	1.2	Tim Hammond
;               Added new COMMON block Global_lw_data which contains the
;               values of left, right, top, bottom, grtop, grright
;	1.3     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	01-05-95
;	1.2	27-02-96
;	1.3	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot301, x , y, nener, nmx, npspl, nplots, title, xtitle, ytitle, $
             ldef1, xmin, xmax, ymin, ymax

  COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

  makeplot = 1

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

  !p.font=-1
  if small_check eq 'YES' then begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2) + "!C!C" + $
        title(3) + "!C!C" + title(4)
  endif else begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
        title(3) + "!C" + title(4)
  endelse
		;**** Find x and y ranges for auto scaling ****

  if ldef1 eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
    xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    yvals = where(y gt 1.0e-37 and y lt 1.0e37)

    if xvals(0) gt -1 then begin
      makeplot = 1
      xmax = max(x(xvals))
      xmin = min(x(xvals))
    end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      makeplot = 1
      ymax = max(y(yvals))
      ymin = min(y(yvals))
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin
		;Check that at least some data in in axes range ***
    xvals = where(x ge xmin and x le xmax)
    yvals = where(y ge ymin and y le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    end
    style = 1
  end


		;**** Set up log-log plotting axes ****

    plot_oo,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

  if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************

     case nplots of 

	1 : oplot, x(0,0:nener-1), y(0,0:nener-1), psym=1
	3 :  begin
		oplot, x(0,0:nener-1), y(0,0:nener-1), psym=1
		oplot, x(1,0:npspl-1), y(1,0:npspl-1)
             end
        5 :  begin
                oplot, x(0,0:nener-1), y(0,0:nener-1), psym=1
                oplot, x(2,0:nmx-1), y(2,0:nmx-1),linestyle=5
	     end
        7 :  begin
                oplot, x(0,0:nener-1), y(0,0:nener-1), psym=1
		oplot, x(1,0:npspl-1), y(1,0:npspl-1)
                oplot, x(2,0:nmx-1), y(2,0:nmx-1), linestyle=5
	     end
        else : print, "ADAS301 : NO PLOTS"

      endcase

  endif else begin
     print, "ADAS301 : No data found in these axes ranges"
  endelse

		;**** plot title ****

      xyouts, left, top, gtitle, /normal, alignment=0.0, $
			charsize=charsize*0.95

END
