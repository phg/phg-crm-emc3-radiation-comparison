; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/cw_adas311_out.pro,v 1.2 2005/02/24 14:55:55 mog Exp $ Date $Date: 2005/02/24 14:55:55 $
;
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS311_OUT()
;
; PURPOSE:
;       Produces a widget for ADAS311 output options.
;
; EXPLANATION:
;       This function declares a compound widget consisting of:
;        - a typein text widget for a user-defined title for any calculations
;        - 2 output file widgets (cw_adas_outfile) for text and
;          passing file output. The widget requires that at least one 
;          of these be selected for any output to be produced
;        - buttons for 'Run Now', 'Run in Batch','Cancel' and 'Return to
;          series menu' (this latter is represented by a bitmapped button).
;
; USE:
;       This routine is specific to adas311.
;
; INPUTS:
;       PARENT  - Long integer; ID of parent widget.
;
;       INVAL   - A structure containing the settings of the input
;                 options widget.
;
;       PROCVAL - A structure containing the settings of the processing
;                 options widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       OUTVAL  - A structure which determines the initial settings of
;                 the output options widget.  
;
;       UVALUE  - A user value for this widget.
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;       CW_ADAS_OUTFILE Output file name entry widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Two other routines are included in this file
;       OUT311_GET_VAL()
;       OUT311_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support 
;       Services plc, 10th January 1996. Modified by Harvey
;       Anderson for use with adas311.
;
; MODIFIED:
;       1.1     Harvey Anderson
;                - First version
;       1.2     Martin O'Mullane
;                - Give more descriptive name for passing files.
;                - Force user to choose at least one output file.
;
;
; VERSION:
;       1.1     02-02-99
;       1.1     23-02-2005
;
;-
;-----------------------------------------------------------------------------

FUNCTION out311_get_val, id

    COMMON outblock311, font, font_small

                ;***********************************
                ;**** Return to caller on error ****
                ;***********************************

    ON_ERROR, 2

                ;****************************
                ;**** Retrieve the state ****
                ;****************************

    parent = widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

                ;***********************************
                ;**** Get run title from widget ****
                ;**** Then centre in in string  ****
                ;**** of 40 characters          ****
                ;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title)
    if (title_len gt 40 ) then begin
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
        wait, 1
    endif
    pad = (40 - title_len)/2
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

                ;*********************************
                ;**** Get the output settings ****
                ;*********************************

    widget_control, state.summaryid, get_value=summaryval
    widget_control, state.pass1id, get_value=pass1val
    widget_control, state.pass2id, get_value=pass2val

    os = {      TITLE           :       title(0),                       $
                TEXOUT          :       summaryval.outbut,              $
                TEXOUT1         :       pass1val.outbut,                $
                TEXOUT2         :       pass2val.outbut,                $
                TEXAPP          :       summaryval.appbut,              $       
                TEXAPP1         :       pass1val.appbut,                $
                TEXAPP2         :       pass2val.appbut,                $
                TEXREP          :       summaryval.repbut,              $
                TEXREP1         :       pass1val.repbut,                $
                TEXREP2         :       pass2val.repbut,                $
                TEXDSN          :       summaryval.filename,            $       
                TEXDSN1         :       pass1val.filename,              $
                TEXDSN2         :       pass2val.filename,              $
                TEXDEF          :       summaryval.defname,             $
                TEXDEF1         :       pass1val.defname,               $
                TEXDEF2         :       pass2val.defname,               $
                TEXMES          :       summaryval.message,             $
                TEXMES1         :       pass1val.message,               $
                TEXMES2         :       pass2val.message                }

                ;**************************
                ;**** Return the state ****
                ;**************************

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os

END

;-----------------------------------------------------------------------------

FUNCTION out311_event, event

    COMMON outblock311, font, font_small

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {       ID      :       parent,                 $
                                TOP     :       event.top,              $
                                HANDLER :       0L,                     $
                                ACTION  :       'Cancel'                }
        end

                ;***********************
                ;**** Menu button ****
                ;***********************

        state.outid: begin
            new_event = {       ID      :       parent,                 $
                                TOP     :       event.top,              $
                                HANDLER :       0L,                     $
                                ACTION  :       'Menu'                  }
        end

                ;************************
                ;**** Run Now button ****
                ;************************

        state.nowid: begin

                ;***************************************
                ;**** Check for errors in the input ****
                ;***************************************

            error = 0
            mess = 'Error in output settings' 
            widget_control, state.summaryid, get_value=summaryval
            widget_control, state.pass1id, get_value=pass1val
            widget_control, state.pass2id, get_value=pass2val
            if (summaryval.outbut eq 1) and                             $
            (strtrim(summaryval.message) ne '') then error=1
            if (error eq 0) and (pass1val.outbut eq 1) and              $
            (strtrim(pass1val.message) ne '') then error=1
            if (error eq 0) and (pass2val.outbut eq 1) and              $
            (strtrim(pass2val.message) ne '') then error=1
            
            if error eq 0           and $
               pass1val.outbut eq 0 and $
               pass2val.outbut eq 0 then begin
               error = 1
               mess = 'Choose at least one output file'
            endif
            
                ;*******************************
                ;**** Return error or event ****
                ;*******************************

            if error eq 1 then begin
                widget_control, state.messid, set_value=mess
                new_event = 0L
            endif else begin
                new_event = {   ID      :       parent,                 $
                                TOP     :       event.top,              $
                                HANDLER :       0L,                     $
                                ACTION  :       'Run Now'               }
            endelse
        end

        ELSE: begin
            new_event = 0L
        end

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas311_out, parent, inval, procval, bitfile,               $
                         VALUE=outval, FONT_LARGE=font_large,           $
                         FONT_SMALL=font_small

    COMMON outblock311, fontlargecom, fontsmallcom

                ;***********************************
                ;**** Set defaults for keywords ****
                ;***********************************
                          
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    fontlargecom = font_large
    fontsmallcom = font_small

                ;**********************************************
                ;**** Create the 311 Output options widget ****
                ;**********************************************
                                    
    cwid = widget_base( parent, UVALUE = uvalue,                        $
                        EVENT_FUNC = "out311_event",                    $
                        FUNC_GET_VALUE = "out311_get_val",              $
                        /COLUMN)
                ;***********************
                ;**** add run title ****
                ;***********************

    base = widget_base(cwid, /row)
    rc = widget_label(base, value='Title for run: ', font=font_small)
    runid = widget_text(base, value=outval.title, xsize=38,             $
                        font=font_small, /editable)

                ;*****************************
                ;**** Text output widgets ****
                ;*****************************

    outfval =   {       OUTBUT          :       outval.texout,          $
                        APPBUT          :       outval.texapp,          $
                        REPBUT          :       outval.texrep,          $
                        FILENAME        :       outval.texdsn,          $
                        DEFNAME         :       outval.texdef,          $
                        MESSAGE         :       outval.texmes           }
    base = widget_base(cwid, /row, /frame)
    summaryid = cw_adas_outfile(base, output='Run Summary Output  ',    $
                                value=outfval, font=font_small)
    outfval1 =  {       OUTBUT          :       outval.texout1,         $
                        APPBUT          :       outval.texapp1,         $
                        REPBUT          :       outval.texrep1,         $
                        FILENAME        :       outval.texdsn1,         $
                        DEFNAME         :       outval.texdef1,         $
                        MESSAGE         :       outval.texmes1          }
    base = widget_base(cwid, /row, /frame)
    pass1id = cw_adas_outfile(base, output='adf26 bundle-n population :',      $
                              value=outfval1, font=font_small)
    outfval2 =  {       OUTBUT          :       outval.texout2,         $
                        APPBUT          :       outval.texapp2,         $
                        REPBUT          :       outval.texrep2,         $
                        FILENAME        :       outval.texdsn2,         $
                        DEFNAME         :       outval.texdef2,         $
                        MESSAGE         :       outval.texmes2          }
    base = widget_base(cwid, /row, /frame)
    pass2id = cw_adas_outfile(base, output='Projection matrices : ',      $
                              value=outfval2, font=font_small)

                ;***********************
                ;**** Error message ****
                ;***********************

    messid = widget_label(cwid, value=' ', font=font_large)

                ;******************************
                ;**** Add the exit buttons ****
                ;******************************

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    nowid = widget_button(base, value='Run Now', font=font_large)

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;**** window.                                 ****
                ;*************************************************

    new_state = {       MESSID          :       messid,                 $
                        RUNID           :       runid,                  $
                        SUMMARYID       :       summaryid,              $
                        PASS1ID         :       pass1id,                $
                        PASS2ID         :       pass2id,                $
                        OUTID           :       outid,                  $
                        NOWID           :       nowid,                  $
                        CANCELID        :       cancelid                }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END
