; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/adas311.pro,v 1.6 2005/02/24 14:56:55 mog Exp $ Date $Date: 2005/02/24 14:56:55 $
;
; PROJECT:
;       UNIX ADAS 
;
; NAME:
;       ADAS311
;
; PURPOSE:
;       The highest level routine for the ADAS 311 program.
;
; NOTE:
;       The comment blocks for adas 311 refer to two methods of using the
;       code. The batch method will not initially be implemented as the
;       interactive code runs reasonably quickly for even large 
;       calculations. It is possible that the batch capability
;       will be added at a later date. The comments will be left for
;       reference.
;
; EXPLANATION:
;       This routine is called from the main adas system routine, adas.pro,
;       to start the ADAS 311 application.  Associated with adas311.pro
;       is a FORTRAN executable. The IDL code provides the user
;       interface and output graphics whilst the FORTRAN code reads
;       in data files, performs numerical processing and creates the
;       output files. The IDL code communicates with the FORTRAN
;       executable via a bi-directional UNIX pipe.  The unit number
;       used by the IDL for writing to and reading from this pipe is
;       allocated when the FORTRAN executable is 'spawned' (see code
;       below).  Pipe communications in the FORTRAN process are to
;       stdin and stdout, i.e streams 5 and 6.
;
;       ADAS 311 is similar to ADAS 310 in that no actual
;       pipe communications are performed before the processing stage and
;       indeed the FORTRAN executable is not actually spawned until this
;       point.
;
;       The FORTRAN code is an independent process under the UNIX system.
;       The IDL process can only exert control over the FORTRAN in the
;       data which it communicates via the pipe.  The communications
;       between the IDL and FORTRAN must be exactly matched to avoid
;       input conversion errors.  The correct ammounts of data must be
;       passed so that neither process 'hangs' waiting for communications
;       which will never occur.
;
;       The FORTRAN code performs some error checking which is
;       independent of IDL.  In cases of error the FORTRAN may write
;       error messages.  To prevent these error messages from conflicting
;       with the pipe communications all FORTRAN errors are written to
;       output stream 0, which is stderr for UNIX.  These error messages
;       will appear in the window from which the ADAS session/IDL session
;       is being run. In the case of the batch job, the error messages
;       will appear as standard cron job output which is usually
;       emailed to the user. If the job has completed successfully then
;       the user will get a message telling them so.
;
;       In the case of severe errors the FORTRAN code may terminate
;       itself prematurely.  In order to detect this, and prevent the
;       IDL program from 'hanging' or crashing, the IDL checks to see
;       if the FORTRAN executable is still an active process before
;       each group of pipe communications.  The process identifier
;       for the FORTRAN process, PID, is recorded when the process is
;       first 'spawned'.  The system is then checked for the presence
;       of the FORTRAN PID. This checking does not occur for the
;       batch cases.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas311.pro is called to start the
;       ADAS 311 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot,             $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas311,   adasrel, fortdir, userroot, centroot, devlist,       $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL -  A string indicating the ADAS system version,
;                  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                  character should be a space.
;
;       FORTDIR -  A string holding the path to the directory where the
;                  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST -  A string array of hardcopy device names, used for
;                  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                  This array must mirror DEVCODE.  DEVCODE holds the
;                  actual device names used in a SET_PLOT statement.
;
;       DEVCODE -  A string array of hardcopy device code names used in
;                  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                  This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14',                 $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;       CBSPF0          Controls the input options screen.
;       CBISPF          Controls the processing options screen.
;       CBSPF1          Controls the output options screen.
;       CBSPF2          Controls the interactive calculations.
;       XXDATE          Get date and time from operating system.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support Services plc, 
;       18th December 1995 for use with adas310. Modified by Harvey
;       Anderson for use with adas 311.
;
; MODIFIED:
;       1.1 Harvey Anderson
;               First version
;       1.2 Richard Martin
;           Increased version no. to 1.2
;       1.3 Richard Martin
;           Increased version no. to 1.3
;       1.4 Richard Martin
;           Increased version no. to 1.4
;       1.5 Richard Martin
;           Increased version no. to 1.5
;       1.6 Martin O'Mullane
;           Increased version no. to 1.6 because projection data is
;           now an output.
;
; VERSION:
;       1.1   01-28-99
;       1.2   15-10-99
;       1.3   21-03-00
;       1.4   10-11-00
;       1.5   18-03-03
;       1.6   22-02-2005
;
;-
;-----------------------------------------------------------------------------

PRO adas311, adasrel, fortdir, userroot, centroot, devlist, devcode,    $
             font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS311 V1.6'
    lpend = 0
    gomenu = 0
    ipset = 0                           ;'Run number' counter for fg jobs
    ipbatset = 0                        ;'Run number' counter for bg jobs
    lbatch = 0                          ;Marker for whether a batch file
                                        ;has been created.
    dsbatch = ''                        ;batch-file name
    deffile = userroot + '/defaults/adas311_defaults.dat'
    bitfile = centroot + '/bitmaps'
    device = ''

                ;**********************************************
                ;**** Parameters previously set in FORTRAN ****
                ;**********************************************

    mxcor = 20                          ;unknown
    mxeps = 10                          ;unknown
    mxdef = 10                          ;max. no. of quantum defects
    mxeng = 25                          ;max. no. of beam energies in scans
    mxden = 25                          ;max. no. of elec/proton densities
    mxtmp = 25                          ;max. no. of elec/proton temperatures
    mximp = 10                          ;max. no. of impurities
    mxnrp = 30                          ;max. no. of representative 
                                        ;N-shell levels
       
                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.ecentroot = centroot + '/adf18/'
        inval.euserroot = userroot + '/adf18/'
        inval.ccentroot = centroot + '/adf01/'
        inval.cuserroot = userroot + '/adf01/'
    endif else begin
        inval   = {     EROOTPATH       :       userroot + '/adf18/',   $
                        EFILE           :       '',                     $
                        ECENTROOT       :       centroot + '/adf18/',   $
                        EUSERROOT       :       userroot + '/adf18/',   $
                        CROOTPATH       :       userroot + '/adf01/',   $
                        CFILE           :       '',                     $
                        CCENTROOT       :       centroot + '/adf01/',   $
                        CUSERROOT       :       userroot + '/adf01/',   $
                        Z               :       0.0,                    $
                        IZ0             :       2,                      $
                        DSNEX           :       '',                     $
                        DSNCX           :       ''                      }
        procval = {     NEW             :       -1                      }
        outval  = {     TITLE           :       '',                     $
                        TEXOUT          :       0,                      $
                        TEXOUT1         :       0,                      $
                        TEXOUT2         :       0,                      $
                        TEXAPP          :       -1,                     $
                        TEXAPP1         :       -1,                     $
                        TEXAPP2         :       -1,                     $
                        TEXREP          :       0,                      $
                        TEXREP1         :       0,                      $
                        TEXREP2         :       0,                      $
                        TEXDSN          :       '',                     $
                        TEXDSN1         :       '',                     $
                        TEXDSN2         :       '',                     $
                        TEXDEF          :       'paper.txt',            $
                        TEXDEF1         :       'adas311_adf26.pass',   $
                        TEXDEF2         :       'adas311_proj.pass',    $
                        TEXMES          :       '',                     $
                        TEXMES1         :       '',                     $
                        TEXMES2         :       ''                      }

;------------------------------------------------------------------------
;
;       The structure conval contains all the input parameters which 
;       for the present implementation are constant.
;
;------------------------------------------------------------------------


       conval = {       ZP              :       1.0,                    $
                        EMP             :       1.8361E3,               $
                        MN              :       2,                      $
                        RX3             :       12.0,                   $
                        DPT             :       16.0,                   $
                        EHCT            :       1.0,                    $
                        NHCT            :       7,                      $
                        LHCT            :       -1,                     $
                        NIP             :       0.0,                    $
                        NEX             :       5,                      $
                        IPRT            :       10,                     $
                        NDEL            :       5,                      $
                        ZEFF            :       1.0,                    $
                        DEDEG           :       0.0,                    $
                        LMAX            :       2,                      $
                        NTAIL           :       1,                      $
                        LSWCH           :       0,                      $
                        IGHNL           :       0,                      $
                        IRNDGV          :       1,                      $
                        IDIEL           :       1,                      $
                        ILREP           :       1,                      $
                        ILTREP          :       1,                      $
                        Z0              :       2.0,                    $
                        Z1              :       1.0,                    $
                        ALF             :       1.25992,                $
                        AMSZ0           :       4.0,                    $
                        AMSHYD          :       1.0,                    $
                        LP              :       0,                      $
                        ISP             :       2,                      $
                        EIJA            :       dblarr(10),             $
                        FIJA            :       dblarr(10),             $
                        CORA            :       dblarr(10,6),           $
                        EIJSA           :       dblarr(10),             $
                        FIJSA           :       dblarr(10),             $
                        CORSA           :       dblarr(10,6),           $
                        MJ              :       2,                      $
                        IREF            :       intarr(10),             $
                        JREF            :       intarr(10),             $
                        SREF            :       dblarr(10),             $
                        WREF            :       dblarr(10),             $
                        CREF            :       dblarr(10),             $
                        MG              :       3,                      $
                        IRA             :       intarr(10),             $
                        NGA             :       intarr(2),              $
                        LGA             :       intarr(2),              $
                        ISGA            :       intarr(2),              $
                        NGLA            :       intarr(2),              $
                        INAL            :       intarr(2,10),           $
                        INALTG          :       intarr(2,10),           $
                        ANAEGY          :       dblarr(2,10),           $
                        ANAFPG          :       dblarr(2,10),           $
                        NSERA           :       intarr(2),              $
                        LAA             :       intarr(2),              $
                        LTAA            :       intarr(2),              $
                        INBL            :       intarr(2,10),           $
                        INBLT           :       intarr(2,10),           $
                        INBIN0          :       intarr(2,10),           $
                        INBN1           :       intarr(2,10),           $
                        ANBE1           :       dblarr(2,10),           $
                        INBN2           :       intarr(2,10),           $
                        ANBE2           :       dblarr(2,10),           $
                        INBN3           :       intarr(2,10),           $
                        ANBE3           :       dblarr(2,10),           $
                        INLREP1         :       indgen(10),             $
                        INLMP           :       indgen(10)              }



    endelse


;------------------------------------------------------------------------
;
;       Initialise values which are contained in the structure conval
;
;------------------------------------------------------------------------

        conval.eija(*)=0.75063
        conval.fija(*)=0.4162
        conval.cora(*)=0.0
        conval.cora(0,0) = 0.05
        conval.cora(0,1) = 0.3
        conval.cora(0,2) = 0.7
        conval.cora(0,3) = 0.9
        conval.eijsa(*)  = 1.0
        conval.fijsa(*)  = 0.0
        conval.corsa(*)  = 0.0
        conval.iref(*)   = 2    
        conval.jref(*)   = 0
        conval.jref(0)   = 1
        conval.jref(1)   = 3     ; value depends on user input. 
        conval.sref(*)   = 0.0
        conval.wref(*)   = 0.0
        conval.cref(*)   = 0.0
        conval.ira(*)    = 0.0  
        conval.ira(0)    = 1
        conval.ira(1)    = 2
        conval.ira(2)    = conval.jref(1)

        conval.nga(0)    = 1
        conval.nga(1)    = 2
        conval.lga(*)    = 0
        conval.isga(0)   = 1
        conval.isga(1)   = 3
        conval.ngla(*)   = 0
        conval.ngla(0)   = 1
        conval.ngla(1)   = 1

        conval.inal(*)     = 0
        conval.inaltg(*)   = 0
        conval.anaegy(*)   = 0.0
        conval.anaegy(0,0) = 1.80708838
        conval.anaegy(1,0) = 0.350424896
        conval.anafpg(*)   = 1.0
        
        conval.nsera(*)    = 2
        conval.laa(*)      = 1
        conval.ltaa(*)     = 1

        conval.inbl(*)     = 1  
        conval.inbl(0,0)   = 0
        conval.inbl(1,0)   = 0  
        conval.inblt(*)    = 1
        conval.inblt(0,0)  = 0
        conval.inblt(1,0)  = 0          
        conval.inbin0(*)   = 3          
        conval.inbn1(*)    = 2          
        conval.anbe1(*)    = 0.0
        conval.anbe1(0,0)  = 0.291908949
        conval.anbe1(1,0)  = 0.350424896
        conval.anbe1(0,1)  = 0.24764462
        conval.anbe1(1,1)  = 0.266307709        
        conval.inbn2(*)    = 3          
        conval.anbe2(*)    = 0.0
        conval.anbe2(0,0)  = 0.122528438
        conval.anbe2(1,0)  = 0.137363685
        conval.anbe2(0,1)  = 0.110275904
        conval.anbe2(1,1)  = 0.116150843                        
        conval.inbn3(*)    = 4  
        conval.anbe3(*)    = 0.0
        conval.anbe3(0,0)  = 0.0671649
        conval.anbe3(1,0)  = 0.0730156
        conval.anbe3(0,1)  = 0.0621307
        conval.anbe3(1,1)  = 0.0646415          
        

;------------------------------------------------------------------------
;
;                     End of Initialising values 
;
;------------------------------------------------------------------------


                ;******************
                ;**** Get date ****
                ;******************

    date = xxdate()

LABEL100:

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

    cbspf0, inval, rep, FONT_LARGE=font_large, FONT_SMALL=font_small


    ipset = 0
    ipbatset = 0

                ;*********************************************
                ;**** If cancel selected then end program ****
                ;*********************************************

    if rep eq 'YES' then goto, LABELEND

LABEL200:

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** processing options selection           ****
                ;************************************************


    cbispf, inval, procval, bitfile, mximp, mxnrp, mxden, mxtmp, mxeng, $
            mxcor, mxeps, mxdef, lpend, gomenu, FONT_LARGE=font_large,  $
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts 

                ;*************************************************
                ;**** If menu button clicked, end the program ****
                ;*************************************************

    if gomenu eq 1 then begin
        goto, LABELEND
    endif


                ;******************************************
                ;**** If cancel selected then goto 100 ****
                ;******************************************

    if lpend eq 1 then goto, LABEL100

LABEL300:
                ;******************************************
                ;**** Invoke user interface widget for ****
                ;**** output options screen            ****
                ;******************************************

    cbspf1, inval, procval, outval, bitfile, lpend, gomenu,             $
            FONT_LARGE=font_large, FONT_SMALL=font_small

                ;*************************************************
                ;**** If menu button clicked erase any output ****
                ;**** and then end the program                ****
                ;*************************************************

    if gomenu eq 1 then begin
        outval.texmes = ''
        outval.texmes1 = ''
        outval.texmes2 = ''
        goto, LABELEND
    endif

                ;***************************************
                ;**** If cancel selected then erase ****
                ;**** output messages and goto 200  ****
                ;***************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.texmes1 = ''
        outval.texmes2 = ''
        goto, LABEL200
    endif

                ;***************************************************
                ;**** If user has selected 'Run Now' then begin ****
                ;**** an interactive calculation                ****
                ;***************************************************

    if lpend eq 0 then begin
        cbspf2, inval, procval, outval,conval, date, fortdir,    $
                centroot, mxcor, mxeps, mxdef, font=font_large
    endif else begin

                ;******************************************
                ;**** User has selected 'Run in Batch' ****
                ;**** NOT CURRENTLY IMPLEMENTED        ****
                ;******************************************

    endelse

                ;**************************************
                ;**** Back for more output options ****
                ;**************************************

    GOTO, LABEL300

LABELEND:

                ;****************************
                ;**** Save user defaults ****
                ;****************************

    save, inval, procval, outval, conval,  filename=deffile

END
