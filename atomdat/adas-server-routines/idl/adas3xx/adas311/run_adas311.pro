;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas311
;
; PURPOSE    :  Runs ADAS311 beam stopping code as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf18      I     str    expansion file name.
;               adf01      I     str    charge exchange file name.
;               iz0        I     int    Nuclear charge of beam species
;               iz1        I     int    Recombining ion charge
;               ts         I     real   Radiation field temperature (ev)
;               wdil       I     real   External radiation field dilution
;                                       factor
;               cion       I     real   Multiplier of ground level e-impact
;                                       ionisation rate
;               cpy        I     real   Multiplier of ground level e-impact
;                                       excitation rate
;               wdpi       I     real   External radiation field dilution
;                                       factor photo-ionisation
;               bsim       I     real   Beam species isotope mass
;               nip        I     int    Range of delta n for impact parameter
;                                       cross sections
;               intd       I     int    Order of maxwell quadrature for
;                                       cross sections
;               iprs       I     int    Beyond nip - use 0 van regemorter,
;                                                        1 Percival-Richards
;               ilow       I     int    =1 Access special low level data
;                                       =0 Do not
;               ionip      I     int    =1 Include ion impact collisions
;                                       =0 Do not
;               nionip     I     int    Delta n for ion impact excitation
;                                       cross sections
;               ilprs      I     int    =1 Lodge-Percival-Richards or
;                                       =0 Vainshtein default
;               ivdisp     I     int    =1 Use beam energy in calculation
;                                          of cross section
;                                       =0 Do not
;               izimp      I     int()  Impurity atomic number
;               amimp      I     real() Impurity mass
;               frimp      I     real() Impurity fraction - total LE 1.0
;               nmin       I     int    Minimum representative shell
;               nmax       I     int    Maximunm representative shell
;               nmid       I     int    Middle representative shell
;               nrep       I     int()  Representative shells (max 25)
;               te         I     real() Electron temperatures requested
;               tp         I     real() Ion temperatures requested
;               dens       I     real() Electron densities requested
;               denp       I     real() Ion densities requested
;               bmeng      I     real() Beam energies requested
;               tref       I     real   Reference electron temperatures
;               dref       I     real   Reference density (electron)
;               bref       I     real   Reference beam energy
;               bdens      I     real   Beam density
;               num_met    I     int    Use up to this number of metastables:
;                                         1 : Use 1s2  1S
;                                         2 : Add 1s2s 1S
;                                         3 : Add 1s2s 3S  - default value
;                                         4 : Add 1s2p 3P
;                                         5 : Add 1s2p 1P
;
;               log        O     str    Output logfile name.
;               adf26      O     str    Output adf26 population dataset name.
;               projf      O     str    Output projection dataset name.
;
;
; KEYWORDS      pbar               -    Display a progress bar when running.
;               help               -    If specified this comment section
;                                       is written to screen.
;
;
; OUTPUTS    :  An adf26 file is the output.
;
;
; NOTES      :  run_adas311 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-11-2006
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;
; DATE
;       1.1     07-11-2006
;
;-
;----------------------------------------------------------------------


PRO run_adas311, iz0      = iz0,      $
                 iz1      = iz1,      $
                 adf18    = adf18,    $
                 adf01    = adf01,    $
                 ts       = ts,       $
                 wdil     = wdil,     $
                 cion     = cion,     $
                 cpy      = cpy,      $
                 wdpi     = wdpi,     $
                 bsim     = bsim,     $
                 nip      = nip,      $
                 intd     = intd,     $
                 iprs     = iprs,     $
                 ilow     = ilow,     $
                 ionip    = ionip,    $
                 nionip   = nionip,   $
                 ilprs    = ilprs,    $
                 ivdisp   = ivdisp,   $
                 izimp    = izimp,    $
                 amimp    = amimp,    $
                 frimp    = frimp,    $
                 nmin     = nmin,     $
                 nmax     = nmax,     $
                 nmid     = nmid,     $
                 nrep     = nrep,     $
                 te       = te,       $
                 tp       = tp,       $
                 dens     = dens,     $
                 denp     = denp,     $
                 bmeng    = bmeng,    $
                 tref     = tref,     $
                 dref     = dref,     $
                 bref     = bref,     $
                 bdens    = bdens,    $
                 log      = log,      $
                 projf    = projf,    $
                 adf26    = adf26,    $
                 num_met  = num_met,  $
                 pbar     = pbar,     $
                 help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas311'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2
close, /all

; Input screen

if n_elements(iz0) EQ 0 then begin
    a311_iz0 = 2.0D0
    print, 'Using a default helium beam'
endif else a311_iz0 = double(iz0)

if n_elements(iz1) EQ 0 then begin
    a311_iz1 = 1.0D0
    print, 'Using a default helium beam charge'
endif else a311_iz1 = double(iz1+1)

if n_elements(adf18) EQ 0 then message, 'An expansion file is necessary'
a311_adf18 = adf18

if n_elements(adf01) EQ 0 then begin
    a311_adf01 = '/home/adas/adas/adf01/qcx#h0/qcx#h0_old#h1.dat'
    print, 'Using a default adf01 file : ' + a311_adf01
endif else a311_adf01 = adf01


; Processing screen
;
; Switches

if n_elements(ts) EQ 0 then begin
    a311_ts = 8617.0D0
    print, 'Using a default radiation field temperature : ' + string(a311_ts)
endif else a311_ts = double(ts)

if n_elements(wdil) EQ 0 then begin
    a311_wdil = 0.0D0
    print, 'Using a default radiation dilution factor : ' + string(a311_wdil)
endif else a311_wdil = double(wdil)

if n_elements(cion) EQ 0 then begin
    a311_cion = 1.0D0
    print, 'Using a  default multiplier of ground level ' + $
           'e-impact ionisation rate: ' + string(a311_cion)
endif else a311_cion = double(cion)

if n_elements(cpy) EQ 0 then begin
    a311_cpy = 1.0D0
    print, 'Using a  default multiplier of ground level ' + $
           'e-impact excitation rate: ' + string(a311_cpy)
endif else a311_cpy = double(cpy)

if n_elements(wdpi) EQ 0 then begin
    a311_wdpi = 0.0D0
    print, 'Using a default photo-ionisation radiation ' + $
           'field dilution factor : ' + string(a311_wdpi)
endif else a311_wdpi = double(wdpi)

if n_elements(bsim) EQ 0 then begin
    a311_bsim = 1.0D0
    print, 'Using a default beam species isotope mass : ' + string(a311_bsim)
endif else a311_bsim = double(bsim)

if n_elements(nip) EQ 0 then begin
    a311_nip = 2L
    print, 'Using a default delta n for impact parameter cross sections : ' + string(a311_nip)
endif else a311_nip = long(nip)

if n_elements(intd) EQ 0 then begin
    a311_intd = 3L
    print, 'Using a default order of maxwell quadrature : ' + string(a311_intd)
endif else a311_intd = long(intd)

if keyword_set(iprs) then a311_iprs = 1 else a311_iprs = 0
if keyword_set(ilow) then a311_ilow = 1 else a311_ilow = 0
if keyword_set(ionip) then a311_ionip = 1 else a311_ionip = 0

if n_elements(nionip) EQ 0 then begin
    a311_nionip = 2L
    print, 'Using a default delta n for ion impact excitation : ' + string(a311_nionip)
endif else a311_nionip = long(nionip)

if keyword_set(ilprs) then a311_ilprs = 1 else a311_ilprs = 0
if keyword_set(ivdisp) then a311_ivdisp = 1 else a311_ivdisp = 0


; Impurities

if n_elements(izimp) EQ 0 then message, 'Impurity atomic numbers are required'
a311_izimp = long(izimp)
if n_elements(amimp) EQ 0 then message, 'Impurity masses are required'
a311_amimp = double(amimp)
if n_elements(frimp) EQ 0 then message, 'Impurity fraction are required'
a311_frimp = double(frimp)

len_izimp = n_elements(a311_izimp)
len_amimp = n_elements(a311_amimp)
len_frimp = n_elements(a311_frimp)

if (len_amimp ne len_izimp) or (len_izimp ne len_frimp) then $
   print, 'Impurity Z/M/fraction size mismatch - smallest  used'

nimp = min([len_izimp,len_amimp,len_frimp])



; Representative n-shells

if n_elements(nmin) EQ 0 then begin
    a311_nmin = 1L
    print, 'Using a default minimum n : ' + string(a311_nmin)
endif else a311_nmin = long(nmin)

if n_elements(nmax) EQ 0 then begin
    a311_nmax = 100L
    print, 'Using a default maximum n : ' + string(a311_nmax)
endif else a311_nmax = long(nmax)

if n_elements(nmid) EQ 0 then begin
    a311_nmid = 3L
    print, 'Using a default middle n : ' + string(a311_nmid)
endif else a311_nmid = long(nmid)

if n_elements(nrep) EQ 0 then message, 'A set of representative n-shells is required'
a311_nrep = long(nrep)
imax = n_elements(nrep)


; Plasma and beam parameters

if n_elements(te) EQ 0 then message, 'Electron temperatures are required'
a311_te = double(te)

if n_elements(tp) EQ 0 then begin
    a311_nmax = a311_te
    print, 'Setting ion temperatures to equal electron temperatures'
endif else a311_tp = double(tp)

len_te = n_elements(a311_te)
len_tp = n_elements(a311_tp)

if (len_te ne len_tp) then print, 'Te/Tion size mismatch - smallest  used'
ntemp = min([len_te,len_tp])

if n_elements(dens) EQ 0 then message, 'Electron densmperatures are required'
a311_dens = double(dens)

if n_elements(denp) EQ 0 then begin
    a311_nmax = a311_dens
    print, 'Setting ion densities to equal electron densities'
endif else a311_denp = double(denp)

len_dens = n_elements(a311_dens)
len_denp = n_elements(a311_denp)

if (len_dens ne len_denp) then print, 'dens/nion size mismatch - smallest  used'
ndens = min([len_dens,len_denp])


if n_elements(bmeng) EQ 0 then message, 'Beam energies are required'
a311_bmeng = double(bmeng)
nbmeng     = n_elements(bmeng)

if n_elements(tref) EQ 0 then begin
    a311_tref = a311_te[ntemp / 2]
    print, 'Using a generated reference temperature : ' + string(a311_tref)
endif else a311_tref = double(tref)
ind       = i4indf(a311_te, a311_tref)
a311_tref = a311_te[ind]
a311_tref_ind = ind + 1

if n_elements(dref) EQ 0 then begin
    a311_dref = a311_dens[ntemp / 2]
    print, 'Using a generated reference density : ' + string(a311_dref)
endif else a311_dref = double(dref)
ind       = i4indf(a311_dens, a311_dref)
a311_dref  = a311_dens[ind]
a311_dpref = a311_denp[ind]
a311_dref_ind = ind + 1

if n_elements(bref) EQ 0 then begin
    a311_bref = a311_bmeng[ntemp / 2]
    print, 'Using a generated reference beam energy : ' + string(a311_bref)
endif else a311_bref = double(bref)
ind       = i4indf(a311_bmeng, a311_bref)
a311_bref = a311_bmeng[ind]
a311_bref_ind = ind + 1

if n_elements(bdens) EQ 0 then begin
    a311_bdens = 1.0D7
    print, 'Using a default beam density : ' + string(a311_bdens)
endif else a311_bdens = double(bdens)



; Output - restrict to paper.txt, adf26 and projection files

if n_elements(log) NE 0 then begin
    a311_log = log
    texout   = 1L
endif else texout = 0L

if n_elements(projf) EQ 0 AND n_elements(adf26) EQ 0 then $
   message, 'An output file is required'

if n_elements(adf26) NE 0 then begin
   a311_adf26 = adf26
   a311_out26 = 1L
endif else begin
   a311_adf26 = 'NULL'
   a311_out26 = 0L
endelse

if n_elements(projf) NE 0 then begin
   a311_projf = projf
   a311_outpr = 1L
endif else begin
   a311_projf = 'NULL'
   a311_outpr = 0L
endelse


; Metastables

if n_elements(num_met) EQ -1 then begin
   n_metastables = 3
endif else begin
   if num_met LT 0 OR num_met GT 5 then message, 'Only 1-5 metastable are allowed'
   n_metastables = num_met
endelse
              

;------------------
; Now run the code
;------------------

fortdir  = getenv('ADASFORT')
centroot = getenv('ADASCENT')

spawn, fortdir+'/adas311.out', unit=pipe, /noshell, PID=pid


date = xxdate()
printf, pipe, date[0]
printf, pipe, centroot


MN   = 2L
RX3  =  12.0
DPT  =  16.0
EHCT =  1.0
NHCT =  7L
LHCT =  -1L

printf, pipe, MN
printf, pipe, RX3
printf, pipe, DPT
printf, pipe, EHCT
printf, pipe, NHCT
printf, pipe, LHCT

printf, pipe, a311_nip

NEX   = 5L
IPRT  = 10L
NDEL  = 5L

printf, pipe, NEX
printf, pipe, IPRT
printf, pipe, NDEL

printf, pipe, a311_intd
printf, pipe, a311_iprs
printf, pipe, a311_ilow
printf, pipe, a311_ionip
printf, pipe, a311_nionip
printf, pipe, a311_ilprs
printf, pipe, a311_ivdisp

ZEFF  = 1.0
DEDEG = 0.0

printf, pipe, ZEFF
printf, pipe, DEDEG


printf, pipe, a311_nmin
printf, pipe, a311_nmid
printf, pipe, a311_nmax

printf, pipe, a311_iz0
printf, pipe, a311_iz1


ALF     =  1.25992
AMSZ0   =  4.0
AMSHYD  =  1.0
LP      =  0L
ISP     =  2L

printf, pipe, ALF
printf, pipe, AMSZ0
printf, pipe, AMSHYD
printf, pipe, LP
printf, pipe, ISP

LMAX   =  2L
NTAIL  =  1L
LSWCH  =  0L
IGHNL  =  0L
IRNDGV =  1L
IDIEL  =  1L

printf, pipe, LMAX
printf, pipe, NTAIL
printf, pipe, LSWCH
printf, pipe, IGHNL
printf, pipe, IRNDGV

printf, pipe, IDIEL

EIJA = 0.75063
FIJA = 0.4162
printf, pipe, EIJA
printf, pipe, FIJA

cora = [0.05, 0.3, 0.7, 0.9, 0.0, 0.0]
for j = 0, 5 do printf, pipe, cora[j]

EIJSA = 1.0
FIJSA = 0.0
printf, pipe, EIJSA
printf, pipe, FIJSA

corsa = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
for j = 0, 5 do printf, pipe, corsa[j]



bdn  = imax - a311_nmid
bdnl = fix(total(nrep[0:a311_nmid-1]))   ; total turns this into a float


case n_metastables of
   3 : begin
          IREF = [2L, 2L]
          JREF = [1L, bdn + bdnl + 1]
          SREF = [0.0, 0.0]
          WREF = [0.0, 0.0]
          CREF = [0.0, 0.0]
       end
   4 : begin
          IREF = [2L, 2L, 2L]
          JREF = [1L, bdn + bdnl + 1, bdn + bdnl + 2]
          SREF = [0.0, 0.0, 0.0]
          WREF = [0.0, 0.0, 0.0]
          CREF = [0.0, 0.0, 0.0]
       end
   5 : begin
          IREF = [2L, 2L, 2L, 2L]
;           JREF = [1L, bdn + bdnl + 1, bdn + bdnl + 2, 3L]
;          JREF = [1L, 3L, bdn + bdnl + 1, bdn + bdnl + 2]
          JREF = [1L, 3L, bdn + bdnl + 1, bdn + bdnl + 2]
          SREF = [0.0, 0.0, 0.0, 0.0]
          WREF = [0.0, 0.0, 0.0, 0.0]
          CREF = [0.0, 0.0, 0.0, 0.0]       
       end
endcase

MJ = n_elements(iref)
printf, pipe, MJ

W1 = 1.0e30
for j = 0, MJ-1 do printf, pipe, iref[j], jref[j], sref[j], wref[j], cref[j], w1, w1


MG = long(n_metastables)
case n_metastables of

  1 : IRA = [1L]
  2 : IRA = [1L, 2L]
  3 : IRA = [1L, 2L, bdn + bdnl + 1]
  4 : IRA = [1L, 2L, bdn + bdnl + 2, bdn + bdnl + 1]
;  5 : IRA = [1L, 2L, bdn + bdnl + 1, bdn + bdnl + 2, 3L]
;  5 : IRA = [1L, 2L, 3L, bdn + bdnl + 1, bdn + bdnl + 2]
  5 : IRA = [1L, 2L, 3L, bdn + bdnl + 1, bdn + bdnl + 2]

endcase

printf, pipe, MG
for j = 0, MG-1 do printf, pipe, ira[j]


intemp = a311_ts                         ; Convert temperature to K
xxtcon, 2, 1, 1, 1, intemp, outtemp
printf, pipe, outtemp
printf, pipe, a311_wdil
printf, pipe, a311_bdens

printf, pipe, ntemp
xxtcon, 2, 1, 1, ntemp, a311_te[*], etemps_kelvin
xxtcon, 2, 1, 1, ntemp, a311_tp[*], ptemps_kelvin
for i = 0, ntemp-1 do printf, pipe, etemps_kelvin[i]
for i = 0, ntemp-1 do printf, pipe, ptemps_kelvin[i]

printf, pipe, a311_tref_ind

printf, pipe, ndens
for i = 0, ndens-1 do printf, pipe, a311_dens[i]
for i = 0, ndens-1 do printf, pipe, a311_denp[i]

printf, pipe, a311_dref_ind

printf, pipe, 1L
printf, pipe, nimp
for i = 0, nimp-1 do printf, pipe, a311_izimp[i], a311_amimp[i], a311_frimp[i]


printf, pipe, nbmeng
for i = 0, nbmeng-1 do printf, pipe, a311_bmeng[i]
printf, pipe, a311_bref_ind


ZP  = 1.0
EMP = 1.8361E3

printf, pipe, ZP
printf, pipe, EMP

NGA  = [1L, 2]
LGA  = [0L,0]
ISGA = [1L, 3]
NGLA = [1L,1]

printf, pipe, NGA[0]
printf, pipe, LGA[0]
printf, pipe, ISGA[0]
printf, pipe, NGLA[0]

INAL    = intarr(2,10)
INALTG  = intarr(2,10)
ANAEGY  = dblarr(2,10)
ANAFPG  = dblarr(2,10)
NSERA   = intarr(2)
LAA     = intarr(2)
LTAA    = intarr(2)
INBL    = intarr(2,10)
INBLT   = intarr(2,10)
INBIN0  = intarr(2,10)
INBN1   = intarr(2,10)
ANBE1   = dblarr(2,10)
INBN2   = intarr(2,10)
ANBE2   = dblarr(2,10)
INBN3   = intarr(2,10)
ANBE3   = dblarr(2,10)
INLREP1 = indgen(10)
INLMP   = indgen(10)


inal[*]     = 0
inaltg[*]   = 0
anaegy[*]   = 0.0
anaegy[0,0] = 1.80708838
anaegy[1,0] = 0.350424896
anafpg[*]   = 1.0

nsera[*]    = 2
laa[*]      = 1
ltaa[*]     = 1

inbl[*]     = 1
inbl[0,0]   = 0
inbl[1,0]   = 0
inblt[*]    = 1
inblt[0,0]  = 0
inblt[1,0]  = 0
inbin0[*]   = 3
inbn1[*]    = 2
anbe1[*]    = 0.0
anbe1[0,0]  = 0.291908949
anbe1[1,0]  = 0.350424896
anbe1[0,1]  = 0.24764462
anbe1[1,1]  = 0.266307709
inbn2[*]    = 3
anbe2[*]    = 0.0
anbe2[0,0]  = 0.122528438
anbe2[1,0]  = 0.137363685
anbe2[0,1]  = 0.110275904
anbe2[1,1]  = 0.116150843
inbn3[*]    = 4
anbe3[*]    = 0.0
anbe3[0,0]  = 0.0671649
anbe3[1,0]  = 0.0730156
anbe3[0,1]  = 0.0621307
anbe3[1,1]  = 0.0646415

for i = 0, ngla[0]-1 do printf, pipe, inal[0,i], inaltg[0,i], anaegy[0,i], anafpg[0,i]

printf, pipe, nsera[0]
printf, pipe, laa[0]
printf, pipe, ltaa[0]

for i = 0, nsera[0]-1 do printf, pipe, inbl[0,i], inblt[0,i], inbin0[0,i], inbn1[0,i], $
                                       anbe1[0,i], inbn2[0,i], anbe2[0,i], inbn3[0,i], $
                                       anbe3[0,i]

ISP = 2

if isp gt 1  then begin
        printf, pipe, nga[1]
        printf, pipe, lga[1]
        printf, pipe, isga[1]
        printf, pipe, ngla[1]

     for i = 0, ngla[1]-1 do begin
        printf, pipe, inal[1,i]
        printf, pipe, inaltg[1,i]
        printf, pipe, anaegy[1,i]
        printf, pipe, anafpg[1,i]
     endfor

     printf, pipe, nsera[1]
     printf, pipe, laa[1]
     printf, pipe, ltaa[1]

     for i = 0, nsera[1]-1 do begin
        printf, pipe, inbl[1,i]
        printf, pipe, inblt[1,i]
        printf, pipe, inbin0[1,i]
        printf, pipe, inbn1[1,i]
        printf, pipe, anbe1[1,i]
        printf, pipe, inbn2[1,i]
        printf, pipe, anbe2[1,i]
        printf, pipe, inbn3[1,i]
        printf, pipe, anbe3[1,i]
     endfor

endif

printf, pipe, imax

ILREP  = 1L
ILTREP = 1L

printf, pipe, ILREP
printf, pipe, ILTREP

for  i = 0, imax-1 do printf, pipe, a311_nrep[i]

INLREP1 = indgen(10)
INLMP   = indgen(10)

for i = 0, a311_nmid-1 do begin
   printf, pipe, inlrep1[i+1]
   for j = 0, inlrep1[i] do printf, pipe, inlmp[j]
endfor




printf, pipe, 'Generated by run_adas311'
printf, pipe, texout
printf, pipe, a311_out26
printf, pipe, a311_outpr


; select a random extension for output files

rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)

if texout EQ 1 then printf, pipe, a311_log else printf, pipe, 'temp1.0' + rand
printf, pipe, 'temp1.1' + rand
printf, pipe, 'temp1.2' + rand

idum = 0L
readf, pipe, idum
if idum EQ 1 then message, 'A problem has occurred with file opening'

idum = 0
readf, pipe, idum
i1 = idum
readf, pipe, idum
i2 = idum
readf, pipe, idum
i3 = idum

if a311_out26 EQ 1 then begin

    itotal = ( i1 * i3 ) + i2
    adas_progressbar, pipe     = pipe,     $
                      n_call   = itotal,   $
                      adasprog = 'ADAS311 adf26 bundle-n : '
endif


if a311_outpr EQ 1 then begin

    itotal = ( i1 * i2 ) + i3
    adas_progressbar, pipe     = pipe,     $
                      n_call   = itotal,   $
                      adasprog = 'ADAS311 projection matrix :'

endif



; Final signal

idum = 0L
readf, pipe, idum


; Add comments to the bottom of the output files

date = xxdate()
name = xxuser()

com = 'C--------------------------------------------------------------------'
com = [com,'C']
com = [com,'C  Code      : run_adas311']
com = [com,'C  Producer  : ' + name(0)]
com = [com,'C  Date      : ' + date(0)]
com = [com,'C']
com = [com,'C--------------------------------------------------------------------']


if a311_adf26 NE 'NULL' then begin

   adas_readfile, file='temp1.1' + rand, all=all, nlines=nlines
   all = [all, ' ', com]
   openw, lun, a311_adf26, /get_lun
   for j = 0L, nlines+8-1 do printf, lun, all[j]
   free_lun, lun

endif

if a311_projf NE 'NULL' then begin

   adas_readfile, file='temp1.2' + rand, all=all, nlines=nlines
   all = [all, ' ', com]
   openw, lun, a311_projf, /get_lun
   for j = 0L, nlines+8-1 do printf, lun, all[j]
   free_lun, lun

endif


; Remove unwanted files

cmd = 'rm -f temp1.1' + rand + ' ' + $
            'temp1.2' + rand

spawn, cmd, /sh

END
