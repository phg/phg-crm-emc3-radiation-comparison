; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/cbspf1.pro,v 1.1 2004/07/06 12:00:09 whitefor Exp $ Date $Date: 2004/07/06 12:00:09 $
;
; PROJECT:
;       ADAS 
;
; NAME:
;       CBSPF1
;
; PURPOSE:
;       IDL user interface and communications with ADAS311 FORTRAN
;       process via pipe which this routine also begins if the user
;	requests it.
;
;
; USE:
;       The use of this routine is specific to ADAS311.
;
; INPUTS:
;       
;       INVAL   - A structure containing the final settings of the input
;                 options screen.
;
;       PROCVAL - A structure containing the final settings of the processing
;                 options screen.
;
;	OUTVAL	- A structure which determines the initial settings of
;                 the output options widget. The initial value is set in
;		  adas311.pro. OUTVAL is passed unmodified through to
;		  cw_adas311_out.pro: see that routine for a full
;		  description.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                  for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       LPEND   - Integer; indicates user action on leaving output
;                 selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if they exited with 
;		  'Run Now', or the 'Menu' button and 2 if they exited
;		  with 'Run in Batch'.
;
;       OUTVAL  - On output the structure records the final settings of
;                 the output selection widget if the user pressed either
;		  the 'Run Now' or 'Run in Batch' buttons otherwise it is 
;		  not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;
;       FONT_LARGE	- The large font to be used for the interface widgets.
;
;	FONT_SMALL	- The small font to be used for the interface widgets.
;
;
; CALLS:
;       ADAS311_OUT     User interface - output options.
;
; SIDE EFFECTS:
;       Depending on user actions this routine may spawn a FORTRAN executable
;	which it then communicates with via a bi-directional UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support 
;	Services plc, 10th January 1996.Modified by Harvey
;	Anderson for use in adas311.
;
;
; MODIFIED:
;       1.1     Harvey Anderson
;		First version
;
; VERSION:
;	1.1	02-02-99
;
;-
;-----------------------------------------------------------------------------

PRO cbspf1, inval, procval, outval, bitfile, lpend, gomenu, 		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''


                ;**************************************
                ;**** Pop-up output options widget ****
                ;**************************************

    adas311_out, inval, procval, outval, action, bitfile, 		$
		 FONT_LARGE=font_large, FONT_SMALL=font_small


                ;*********************************************
                ;**** Act on the output from the widget   ****
                ;**** There are four   possible actions   ****
                ;**** 'Menu', 'Cancel', 'Run Now' and     ****
		;**** 'Run in Batch'.			  ****
                ;*********************************************

    if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else if action eq 'Run Now' then begin
        lpend = 0
    endif else if action eq 'Run in Batch' then begin
        lpend = 2
    endif else begin
        lpend = 1
    endelse

                ;**************************************************
                ;**** Set messages and settings ready for when ****
                ;**** output window is re-displayed.           ****
                ;**************************************************

    if (lpend eq 0 and gomenu eq 0) or (lpend eq 2) then begin
        if outval.texout eq 1 then begin
            outval.texdef = outval.texdsn
            if outval.texrep ge 0 then outval.texrep = 0
            outval.texmes = 'Output written to file.'
        endif
        if outval.texout1 eq 1 then begin
            outval.texdef1 = outval.texdsn1
            if outval.texrep1 ge 0 then outval.texrep1 = 0
            outval.texmes1 = 'Output written to file.'
        endif
        if outval.texout2 eq 1 then begin
            outval.texdef2 = outval.texdsn2
            if outval.texrep2 ge 0 then outval.texrep2 = 0
            outval.texmes2 = 'Output written to file.'
        endif
   endif
                                                   
END

