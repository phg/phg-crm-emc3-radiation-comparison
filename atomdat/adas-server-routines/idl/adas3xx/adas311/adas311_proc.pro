; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/adas311_proc.pro,v 1.1 2004/07/06 10:35:31 whitefor Exp $ Date $Date: 2004/07/06 10:35:31 $
;
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS311_PROC
;
; PURPOSE:
;       IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select options and input data to control ADAS311
;       processing.
;
; USE:
;       This routine is ADAS311 specific, see cbispf.pro for how it
;       is used.
;
; INPUTS:
;       INVAL   - A structure containing the final settings of the input
;                 options screen.
;
;       PROCVAL - A structure which determines the initial settings of
;                 the processing options widget.  The value is passed
;                 unmodified into cw_adas311_proc.pro.
;
;                 See cbispf.pro for a full description of this structure.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       MXIMP   - Integer; Maximum number of impurities
;
;       MXNRP   - Integer; Maximum number of representative N-shell levels
;
;       MXDEN   - Integer; Maximum number of electron/proton densities
;
;       MXTMP   - Integer; Maximum number of electron/proton temperatures
;
;       MXENG   - Integer; Maximum number of beam energies in scan
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       PROCVAL - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       ACTION  - String; 'Done', 'Menu' or 'Cancel' for the button the
;                 user pressed to terminate the processing options
;                 window.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;       CW_ADAS311_PROC Declares the processing options widget.
;       ADAS311_PROC_EV Called indirectly during widget management,
;                       routine included in this file.
;       XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC311_BLK, to pass the
;       variables VALUE and ACTION between the two routines.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support Services plc, 
;       20th December 1995, for use with adas 311. Modified by Harvey
;	Anderson.
;
; MODIFIED:
;       1.1     Harvey Anderson
;               First version, based on the structure of adas310_proc.pro
;
; VERSION:
;       1.1     01-28-99
;
;-
;-----------------------------------------------------------------------------

PRO adas311_proc_ev, event

    COMMON proc311_blk, action, value

    action = event.action

    CASE event.action OF

                ;**** 'Done' button ****
                                            
        'Done'  : begin

                ;**** Get the output widget value ****

            widget_control, event.id, get_value=value
	    widget_control, event.top, /destroy
	
	end

                ;**** 'Cancel' button ****


        'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

 	ELSE:		;Do nothing

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas311_proc, inval, procval, bitfile, mximp, mxnrp, mxden,         $
                  mxtmp, mxeng, action, FONT_LARGE=font_large,          $
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Declare common variables ****

    COMMON proc311_blk, actioncom, value

                ;**** Copy "procval" to common ****

    value = procval

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN                               $
    edit_fonts = { font_norm:'', font_input:''}

                ;**** Create base widget ****

    procid = widget_base(TITLE='ADAS311 PROCESSING OPTIONS',            $
                         XOFFSET=50, YOFFSET=0)

                ;**** Declare processing widget ****

    cwid = cw_adas311_proc(procid, bitfile, inval, PROCVAL=value,	$
                           mxnrp, mxden, mxtmp, mxeng, mximp,		$
                           FONT_LARGE=font_large, FONT_SMALL=font_small,$
                           EDIT_FONTS=edit_fonts)

                ;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

                ;**** Make widget modal ****

    xmanager, 'adas311_proc', procid, event_handler='adas311_proc_ev',  $
              /modal, /just_reg

                ;*** Copy value back to procval for return to caispf ***

    action = actioncom
    procval = value

END
