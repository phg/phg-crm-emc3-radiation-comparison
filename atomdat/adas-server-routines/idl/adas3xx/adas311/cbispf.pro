; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/cbispf.pro,v 1.1 2004/07/06 11:59:56 whitefor Exp $ Date $Date: 2004/07/06 11:59:56 $
;
; PROJECT:
;       ADAS 
;
; NAME:
;       CBISPF
;
; PURPOSE:
;       IDL user interface for 311 processing options
;
; EXPLANATION:
;       This routine invokes the part of the user interface
;       used to specify the processing options for ADAS311.
;
; USE:
;       The use of this routine is specific to ADAS311, see adas311.pro.
;
; INPUTS:
;	INVAL	- A structure containing the final settings of the input
;		  options screen.
;
;	PROCVAL	- A structure which determines the initial settings of
;                 the processing options widget.  The initial value is
;                 set in adas311.pro.  If adas311.pro passes a blank
;                 dummy structure of the form {NEW:-1} into PROCVAL then
;                 PROCVAL is reset to a default structure.
;
;                 The PROCVAL structure is;
;
;           procval = {	NEW		:	0,			$
;			ITOGGLE1	:	0,			$
;			ITOGGLE2	:	0,			$
;			ITOGGLE3	:	0,			$
;			TS		:	8.61668e3,		$
;			W		:	0.0,			$
;			W1		:	1.0e30,			$
;			BSIM		:	bsimdef,		$
;			INTD		:	-1,			$
;			IPRS		:	0,			$
;			ILOW		:	0,			$
;			IONIP		:	0,			$
;                        NIP		:	1,			$
;			NIONIP		:	-1,			$
;			ILPRS		:	0,			$
;			IVDISP		:	0,			$
;			NOSCAN		:	0,			$
;			IZEFF		:	0,			$
;			NIMP		:	0,			$
;			IZIMPA		:	intarr(mximp),		$
;			AMIMPA		:	fltarr(mximp),		$
;			FRIMPA		:	fltarr(mximp),		$
;			NMIN		:	0,			$
;			NMID		:	0,			$
;			NMAX		:	0,			$
;			IMAX		:	0,			$
;			NREP		:	intarr(mxnrp),		$
;			LREP		:	indgen(5),		$
;			IDREF		:	-1,			$
;			DEREF		:	0.0,			$
;			DPREF		:	0.0,			$
;			ITREF		:	-1,			$
;			TEREF		:	0.0,			$
;			NDENS		:	0,			$
;			DENSA		:	fltarr(mxden),		$
;			DENPA		:	fltarr(mxden),		$
;			NTEMP		:	0,			$
;			TEA		:	fltarr(mxtmp),		$
;			TPA		:	fltarr(mxtmp),		$
;			IBREF		:	-1,			$
;			BEREF		:	0.0,			$
;			NBENG		:	0,			$
;			BMENGA		:	fltarr(mxeng),		$
;			DENSH		:	1E7			}
;
;			Where the elements represent the following:
;
;		NEW	- Flag which defines whether or not default values
;                         exist or not. (< 0 if no values exist)
;
;		ITOGGLE1- Flag indicating which of two parts of the processing
;			  widget are displayed:
;				0 => Display mode of operation section
;				1 => Display representative N-shell section
;
;		ITOGGLE2- Flag indicating which of three tables are displayed
;			  on the processing widget:
;				0 => Display the electron/proton density table
;				1 => Display the electron/proton temp. table
;				2 => Display the beam energy table
;
;		ITOGGLE3- Flag indicating which of two sets of parameters
;			  are displayed on the processing widget:
;				0 => Display the actual parameters section
;				1 => Display the parameter switches section
;
;		TS	- The external radiation field temperature (in eV)
;			  [Default value = 8.61668e3 eV = 1.0e8 K]
;
;		W	- The external radiation field dilution factor
;			  [Default value = 0.0]
;
;		W1	- The external ionising radiation field dilution
;			  factor for photo-ionisation from the ground level
;			  [Default = 1.0e8]
;
;		BSIM	- The Beam Species Isotope Mass - an added feature
;			  in the idl_adas version of adas310. Currently
;			  the value of BSIM will simly be passed to the
;			  processing code and not used. The default value
;			  will be determined from the value of inval.iz0
;
;		INTD	- Integer; the order of Maxwell quadrature for the
;			  impact parameter cross-sections [range 0-3]
;
;		IPRS	- Integer; flag which controls cross-sections
;			  beyond NIP range:
;				0 => Default to van Regemorter cross-sections
;				1 => Use Percival-Richards cross-sections
;
;		ILOW	- Integer; flag which controls access of special,
;			  low-level data:
;				0 => No special, low-level cross-section
;				     data accessed
;				1 => Special, low-level cross-section data
;				     is accessed
;
;		IONIP	- Integer; Controls inclusion of ion impact collisions
;				0 => No ion impact collisions included
;				1 => Ion impact excitation and ionisation
;				     included
;		NIP	- Integer; the range of delta N for the impact
;			  parameter cross-sections [range 0-4]
;
;		NIONIP	- Integer; Range of delta N for ion impact cross-
;			  sections [range 0-999]
;
;		ILPRS	- Integer; Controls use of Lodge-Percival-Richards
;			  ion impact cross-sections:
;				0 => Default to Vainshtein cross-sections
;				1 => Use Lodge-Percival-Richards cross-sections
;
;		IVDISP	- Integer; Controls use of beam energy in calculation
;			  of ion cross-sections:
;				0 => Ion impact at thermal Maxwellian energies
;				1 => Ion impact at displaced thermal energies
;				     according to the neutral beam energy
;				     parameter
;
;		NOSCAN	- Integer; Controls mode of operation:
;				0 => Single impurity
;				1 => Multiple impurities
;
;		IZEFF	- Integer; Nuclear charge of impurity [only set if
;			  NOSCAN=0]
;
;		NIMP	- Integer; Number of impurity species [only set if
;			  NOSCAN=1] (Maximum = mximp)
;
;		IZIMPA	- Integer array; The nuclear charges of the impurities
;			  [Entered by the user if NOSCAN=1]
;			  
;		AMIMPA	- Float array; Atomic mass numbers of the impurities
;			  [Entered by the user if NOSCAN=1]
;
;		FRIMPA	- Float array; Impurity fractions
;			  [Entered by the user if NOSCAN=1]
;
;		NMIN	- Integer; The lowest representative N-shell
;
;		NMID	- Integer; Cut off between nl and n resolved treatment.
;
;		NMAX	- Integer; the highest representative N-shell
;
;		IMAX	- Integer; the number of representative N-shell levels
;			  Minimum currently set to 3
;
;		NREP	- Integer array; the set of representative N-shell
;			  levels (maximum of mxnrp)
;
;		IDREF	- Integer; Index of density reference values
;
;		DEREF	- Float; Reference electron density (cm-3)
;
;		DPREF	- Float; Reference proton density (cm-3)
;
;		ITREF	- Integer; Index of temperature reference value
;
;		TEREF	- Float; Reference electron temperature (eV)
;
;		NDENS	- Integer; number of electron/proton densities
;			  [Maximum of mxden]
;
;		DENSA	- Float array; Electron densities (cm-3)
;
;		DENPA	- Float array; Proton densities (cm-3)
;
;		NTEMP	- Integer; Number of electron/proton temperatures
;			  [Maximum of mxtmp]
;
;		TEA	- Float array; Electron temperatures (eV)
;
;		TPA	- Float array; Proton temperatures (eV)
;
;		IBREF	- Integer; Index of the reference beam energy
;
;		BEREF	- Float; Reference beam energy (eV/amu)
;
;		NBENG	- Integer; Number of beam energies in the scan
;			  [Maximum of mxeng]
;
;		BMENGA	- Float array; Beam energies in scan (eV/amu)
;
;		DENSH	- Float; Beam density (cm-3)
;
;		End of definition of PROCVAL
;
;	BITFILE - String; the path to the directory containing bitmaps
;                  for the 'escape to series menu' button.
;
;	MXIMP	- Integer; Maximum number of impurities
;
;	MXNRP	- Integer; Maximum number of representative N-shell levels
;
;	MXDEN	- Integer; Maximum number of electron/proton densities
;
;	MXTMP	- Integer; Maximum number of electron/proton temperatures
;
;	MXENG	- Integer; Maximum number of beam energies in scan
;
;	MXCOR	- Integer; Unknown
;
;	MXEPS   - Integer; Unknown
;
;       MXDEF   - Integer; Maximum number of quantum defects
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'. 
;
;       PROCVAL - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	I4Z0IE		Used to convert the atomic number inval.iz0 to the
;			element symbol
;	XXEIAM		Used to convert the above symbol to default atomic
;			mass for the beam species isotope
;       ADAS310_PROC    Invoke the IDL interface for ADAS310 data
;                       processing options/input
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support Services plc, 
;	20th December 1995 for use with adas 310. Modified by Harvey 
;	Anderson for use with adas 311.
;
; MODIFIED:
;       1.1             Harvey Anderson
;                       First version. Based on the structure of caispf.pro
;
; VERSION:
;       1.1             20-12-95
;
;
;-
;-----------------------------------------------------------------------------

PRO cbispf, inval, procval, bitfile, mximp, mxnrp, mxden, mxtmp, mxeng, $
            mxcor, mxeps, mxdef, lpend, gomenu, FONT_LARGE=font_large,  $
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN                               $
    edit_fonts = {font_norm:'',font_input:''}
   
                ;*******************************************
                ;**** Set default value if non provided ****
                ;*******************************************

    if (procval.new lt 0) then begin
	i4z0ie, inval.iz0, element
	xxeiam, element, bsimdef
        procval = {	NEW		:	0,			$
			ITOGGLE1	:	0,			$
			ITOGGLE2	:	0,			$
			ITOGGLE3	:	0,			$
			TS		:	8.61668e3,		$
			W		:	0.0,			$
			W1		:	1.0e30,			$
			BSIM		:	bsimdef,		$
			INTD		:	-1,			$
			IPRS		:	0,			$
			ILOW		:	0,			$
			IONIP		:	0,			$
                        NIP		:	1,			$
			NIONIP		:	-1,			$
			ILPRS		:	0,			$
			IVDISP		:	0,			$
			NOSCAN		:	0,			$
			IZEFF		:	0,			$
			NIMP		:	0,			$
			IZIMPA		:	intarr(mximp),		$
			AMIMPA		:	fltarr(mximp),		$
			FRIMPA		:	fltarr(mximp),		$
			NMIN		:	0,			$
			NMID		:	0,			$
			NMAX		:	0,			$
			IMAX		:	0,			$
			NREP		:	intarr(mxnrp),		$
			LREP		:	indgen(5),		$
			IDREF		:	-1,			$
			DEREF		:	0.0,			$
			DPREF		:	0.0,			$
			ITREF		:	-1,			$
			TEREF		:	0.0,			$
			NDENS		:	0,			$
			DENSA		:	fltarr(mxden),		$
			DENPA		:	fltarr(mxden),		$
			NTEMP		:	0,			$
			TEA		:	fltarr(mxtmp),		$
			TPA		:	fltarr(mxtmp),		$
			IBREF		:	-1,			$
			BEREF		:	0.0,			$
			NBENG		:	0,			$
			BMENGA		:	fltarr(mxeng),		$
			DENSH		:	1E7			}
    endif

 

                ;****************************************
                ;**** Pop-up processing input widget ****
                ;****************************************

    adas311_proc, inval, procval, bitfile, mximp, mxnrp, mxden, 	$
                  mxtmp, mxeng, action,	FONT_LARGE=font_large, 		$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;********************************************
                ;****  Act on the event from the widget  ****
                ;****  There are 3 possible events:      ****
		;**** 'Done', 'Cancel' or 'Menu'         ****
                ;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

END
