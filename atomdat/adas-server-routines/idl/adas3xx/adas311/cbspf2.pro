; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/cbspf2.pro,v 1.4 2005/02/24 14:54:38 mog Exp $ Date $Date: 2005/02/24 14:54:38 $
;
; PROJECT:
;       ADAS 
;
; NAME:
;       CBSPF2
;
; PURPOSE:
;       IDL control program for the interactive version of the adas 311
;       calculations. 
;
; EXPLANATION:
;       This routine kicks off an interactive version of the 311 FORTRAN 
;       - adas311.out - and communicates with it via a bi-directional 
;       UNIX pipe. 
;       The progress of the calculations is displayed on an information
;       widget for the user. Once the calculations are finished the FORTRAN
;       process terminates and the routine returns to the purely IDL
;       output options screen.
;       All calculations and file operations are handled by the FORTRAN so
;       the IDL continuously monitors to see whether the FORTRAN is still
;       running and if not it will terminate as well.
;       Communications are with the FORTRAN subroutine ADAS311 and main
;       program ADAS311.
;
; USE:
;       The use of this routine is specific to ADAS311.
;
; INPUTS:
;       INVAL   - Structure containing the final settings of the input
;                 options widget.
;
;       PROCVAL - Structure containing the final settings of the processing
;                 options widget.
;
;       OUTVAL  - Structure containing the final settings of the output
;                 options widget.
;
;       DATE    - String; the date.
;
;       FORTDIR - String; the full pathname of the directory containing
;                 the FORTRAN executables.
;
;       CENTROOT- String; Path pointing to the central data area on each
;                 machine. Used to construct variable dslpath to pass to
;                 FORTRAN.
;
;       MXCOR   - Integer;Parameter - use unknown except as size of other
;                         unknown variables.
;
;       MXEPS   - Integer;Parameter - use unknown except as size of other
;                         unknown variables.
;
;       MXDEF   - Integer;Parameter - use unknown except as size of other
;                         unknown variables.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;     
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;
;       FONT    - Supplies the font to be used for the information widgets.
;
; CALLS:
;       XXTCON  - General temperature conversion routine
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS310a FORTRAN process
;       via UNIX pipes.
;       It also pops up an information widget which keeps the user
;       updated as the calculation progresses.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support 
;       Services plc, for use with adas 310. Modified by
;       Harvey Anderson for use with adas 311.
;
; MODIFIED:
;       1.1     Harvey Anderson   
;       1.2     Richard Martin  
;               Added font keyword to widget_label statements.
;       1.3     Richard Martin
;               Initialised idum   
;       1.4     Martin O'Mullane
;               Projection matrix as output.   
;
; VERSION:
;       1.1     11-02-99
;       1.2     20-09-99
;       1.3     21-03-00
;       1.4     23-02-2005
;
;-
;-----------------------------------------------------------------------------

PRO cbspf2, inval, procval, outval, conval, date, fortdir, centroot, $
            mxcor, mxeps, mxdef, font=font

                ;***********************************
                ;**** Return to caller on error ****
                ;***********************************

    ON_ERROR, 2

                ;**********************************
                ;**** Set defaults for keyword ****
                ;**********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''


                ;*************************************************
                ;**** Spawn the FORTRAN routine 'adas311.out' ****
                ;*************************************************

    spawn, fortdir+'/adas311.out', unit=pipe, /noshell, PID=pid


                ;*********************************************
                ;**** Write all necessary data to FORTRAN ****
                ;**** Communications are with RUN311.FOR  ****
                ;*********************************************

                ;***********************
                ;**** Send the date ****
                ;***********************

     printf, pipe, date(0)


                ;****************************
                ;**** Input options data ****
                ;****************************

     dslpath = centroot 
     printf, pipe, dslpath
     

                ;*********************************
                ;**** Processing options data ****
                ;*********************************



     printf, pipe, conval.mn
     printf, pipe, conval.rx3
     printf, pipe, conval.dpt
     printf, pipe, conval.ehct
     printf, pipe, conval.nhct
     printf, pipe, conval.lhct
     printf, pipe, procval.nip
     printf, pipe, conval.nex
     printf, pipe, conval.iprt
     printf, pipe, conval.ndel

     printf, pipe, procval.intd
     printf, pipe, procval.iprs
     printf, pipe, procval.ilow
     printf, pipe, procval.ionip
     printf, pipe, procval.nionip
     printf, pipe, procval.ilprs
     printf, pipe, procval.ivdisp
     printf, pipe, conval.zeff
     printf, pipe, conval.dedeg

     printf, pipe, procval.nmin
     printf, pipe, procval.nmid
     printf, pipe, procval.nmax

     printf, pipe, inval.iz0
     printf, pipe, (inval.z+1)                  ;as defined in FORTRAN
     printf, pipe, conval.alf
     printf, pipe, conval.amsz0
     printf, pipe, conval.amshyd

     printf, pipe, conval.lp
     printf, pipe, conval.isp

     printf, pipe, conval.lmax     
     printf, pipe, conval.ntail
     printf, pipe, conval.lswch
     printf, pipe, conval.ighnl
     printf, pipe, conval.irndgv

     printf, pipe, conval.idiel

     for i=0, conval.idiel-1 do begin
        printf,pipe, conval.eija(i)
        printf,pipe, conval.fija(i)
        for j = 0, 5 do begin
           printf,pipe, conval.cora(i,j)
        endfor
     endfor

     for i=0, conval.idiel-1 do begin
        printf,pipe, conval.eijsa(i)
        printf,pipe, conval.fijsa(i)
        for j = 0, 5 do begin
           printf,pipe, conval.corsa(i,j)
        endfor
     endfor
 
;****************************************************     
;   Calculating index for population modification 
;**************************************************** 

     bdn  = procval.imax-procval.nmid
     bdnl = total(procval.nrep(0:procval.nmid-1))
    
     conval.jref(1) = bdn + bdnl + 1
     
     conval.ira(2)    = conval.jref(1)

     printf, pipe, conval.mj
     for i=0, conval.mj-1 do begin
             printf, pipe, conval.iref(i)
             printf, pipe, conval.jref(i)
             printf, pipe, conval.sref(i)
             printf, pipe, conval.wref(i)
             printf, pipe, conval.cref(i)
             printf, pipe, procval.w1
             printf, pipe, procval.w1
     endfor

     printf, pipe, conval.mg
     for i=0, conval.mg-1 do begin
            printf, pipe, conval.ira(i)
     endfor

     xxtcon, 2, 1, 1, 1, procval.ts, ts_kelvin
     printf, pipe, ts_kelvin
     printf, pipe, procval.w
     printf, pipe, procval.densh
     printf, pipe, procval.ntemp
     xxtcon, 2, 1, 1, procval.ntemp, procval.tea(*), etemps_kelvin
     xxtcon, 2, 1, 1, procval.ntemp, procval.tea(*), ptemps_kelvin
     for i=0, (procval.ntemp-1) do begin
        printf, pipe, etemps_kelvin(i)
       endfor
     for i=0, (procval.ntemp-1) do begin
         printf, pipe, ptemps_kelvin(i)
     endfor

     printf, pipe, procval.teref

     printf, pipe, procval.ndens
     for i=0, (procval.ndens-1) do begin
        printf, pipe, procval.densa(i)
       endfor
     for i=0, (procval.ndens-1) do begin
        printf, pipe, procval.denpa(i)
     endfor
     printf, pipe, procval.deref

     printf, pipe, procval.noscan
     if procval.noscan eq 1 then begin
        printf, pipe, procval.nimp
        for i=0,(procval.nimp-1) do begin
            printf, pipe, procval.izimpa(i)
            printf, pipe, procval.amimpa(i)
            printf, pipe, procval.frimpa(i)
        endfor
     endif

     printf, pipe, procval.nbeng
     for i=0, (procval.nbeng-1) do begin
         printf, pipe, procval.bmenga(i)
     endfor
     printf, pipe, procval.beref

     printf,pipe, conval.zp
     printf,pipe, conval.emp

     printf,pipe, conval.nga(0)
     printf,pipe, conval.lga(0)
     printf,pipe, conval.isga(0)
     printf,pipe, conval.ngla(0) 

     for i=0,( conval.ngla(0)-1 ) do begin
        printf,pipe, conval.inal(0,i)
        printf,pipe, conval.inaltg(0,i)
        printf,pipe, conval.anaegy(0,i)
        printf,pipe, conval.anafpg(0,i)
     endfor

     printf,pipe, conval.nsera(0)
     printf,pipe, conval.laa(0)
     printf,pipe, conval.ltaa(0)

     for i=0,(conval.nsera(0)-1) do begin
        printf,pipe, conval.inbl(0,i)
        printf,pipe, conval.inblt(0,i)
        printf,pipe, conval.inbin0(0,i)
        printf,pipe, conval.inbn1(0,i)
        printf,pipe, conval.anbe1(0,i)
        printf,pipe, conval.inbn2(0,i)
        printf,pipe, conval.anbe2(0,i)
        printf,pipe, conval.inbn3(0,i)
        printf,pipe, conval.anbe3(0,i)
     endfor

     if ( conval.isp gt 1 ) then begin
        printf,pipe, conval.nga(1)
        printf,pipe, conval.lga(1)
        printf,pipe, conval.isga(1)
        printf,pipe, conval.ngla(1)

     for i=0,( conval.ngla(1)-1 ) do begin
        printf,pipe, conval.inal(1,i)
        printf,pipe, conval.inaltg(1,i)
        printf,pipe, conval.anaegy(1,i)
        printf,pipe, conval.anafpg(1,i)
     endfor

     printf,pipe, conval.nsera(1)
     printf,pipe, conval.laa(1)
     printf,pipe, conval.ltaa(1)

     for i=0,(conval.nsera(1)-1) do begin
        printf,pipe, conval.inbl(1,i)
        printf,pipe, conval.inblt(1,i)
        printf,pipe, conval.inbin0(1,i)
        printf,pipe, conval.inbn1(1,i)
        printf,pipe, conval.anbe1(1,i)
        printf,pipe, conval.inbn2(1,i)
        printf,pipe, conval.anbe2(1,i)
        printf,pipe, conval.inbn3(1,i)
        printf,pipe, conval.anbe3(1,i)
     endfor

     endif

     printf,pipe, procval.imax
     printf,pipe, conval.ilrep
     printf,pipe, conval.iltrep

     for i=0, (procval.imax-1) do begin
         printf, pipe, procval.nrep(i)
     endfor

     for i=0,(procval.nmid-1) do begin
          printf,pipe, conval.inlrep1(i+1)
          for j=0, conval.inlrep1(i) do begin
             printf,pipe, conval.inlmp(j)
          endfor
     endfor


                ;*****************************
                ;**** Output options data ****
                ;*****************************

     printf, pipe, strmid(outval.title,0,80)
     printf, pipe, outval.texout
     printf, pipe, outval.texout1
     printf, pipe, outval.texout2

     if outval.texout eq 1 and                                           $
     strcompress(outval.texdsn, /remove_all) ne '' then begin
         filename = outval.texdsn
     endif else begin
         filename = 'temp1.0'
     endelse
     printf, pipe, filename
     
     if outval.texout1 eq 1 and                                           $
     strcompress(outval.texdsn1, /remove_all) ne '' then begin
         filename = outval.texdsn1
     endif else begin
         filename = 'temp1.1'
     endelse
     printf, pipe, filename
     
     if outval.texout2 eq 1 and                                           $
     strcompress(outval.texdsn2, /remove_all) ne '' then begin
         filename = outval.texdsn2
     endif else begin
         filename = 'temp2.2'
     endelse
     printf, pipe, filename

                ;*****************************************************
                ;**** Wait for a continue signal from the FORTRAN ****
                ;*****************************************************
    idum = -1

    readf, pipe, idum
    if idum eq 1 then begin                     ;Error opening files
        print, '**** CBSPF2.PRO: File opening error in FORTRAN ****'
        goto, LABELEND
    endif



; ADAS311 will output either adf26, projection matrix or both
;
; Size of calculation determined by density, temperature, energy

    idum = 0
    readf, pipe, idum  
    i1 = idum
    readf, pipe, idum   
    i2 = idum
    readf, pipe, idum       
    i3 = idum
   
    if outval.texout1 EQ 1 then begin
    
        itotal = ( i1 * i3 ) + i2
        adas_progressbar, pipe     = pipe,     $
                          n_call   = itotal,   $
                          adasprog = 'ADAS311 adf26 bundle-n : ', $
                          font     = font                          
    endif
       
        
    if outval.texout2 EQ 1 then begin
    
        itotal = ( i1 * i2 ) + i3
        adas_progressbar, pipe     = pipe,     $
                          n_call   = itotal,   $
                          adasprog = 'ADAS311 projection matrix :', $
                          font     = font
                          
    endif


                ;************************************************
                ;**** Wait for the final signal from FORTRAN ****
                ;************************************************

    readf, pipe, idum

                ;****************************************
                ;**** Destroy the information widget ****
                ;****************************************

LABELEND:


END
