; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas311/cw_adas311_in.pro,v 1.1 2004/07/06 12:41:26 whitefor Exp $ Date $Date: 2004/07/06 12:41:26 $
;
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS311_IN()
;
; PURPOSE:
;       Data file selection for adas 311.
;
; EXPLANATION:
;       This function creates a compound widget consisting of text typeins
;	for the beam species element symbol (default h) and the beam 
;	species ion charge (default 0).
;	The compound widget is completed with 'Cancel' and 'Done' buttons.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas311_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ 	EROOTPATH	:	'',		$
;				EFILE		:	'',		$
;				ECENTROOT	:	'',		$
;				EUSERROOT	:	'',		$	
;				CROOTPATH	:	'',		$
;				CFILE		:	'',		$
;			  	CCENTROOT	:	'',		$
;			  	CUSERROOT	:	'',		$
;				Z		:	0.0,		$
;				IZ0		:	1,		$
;				DSNEX		:	'',		$
;				DSNCX		:	''		}
;
;		  Where the elements of the structure are as follows:
;
;                 EROOTPATH   - Current data directory for expansion files
;
;		  EFILE	      - Current expansion file
;
;                 ECENTROOT   - Default central data store for expansion files
;
;                 EUSERROOT   - Default user data store for expansion files
;
;                 CROOTPATH   - Current data directory for ch-exchange files
;
;		  CFILE	      - Current charge exchange data file
;
;                 CCENTROOT   - Default central data store for ch-exchange files
;
;                 CUSERROOT   - Default user data store for ch-exchange files
;
;		  Z	      - Recombining ion charge
;
;		  IZ0	      - Nuclear charge of beam species
;
;		  DSNEX       - Full path name of expansion file
;
;		  DSNCX       - Full path name of charge exchange file
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output. This does not apply to DSNEX and DSNCX.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
; CALLS:
;	CW_ADAS_INFILE	Data file selection widget.
;	I4Z0IE		Converts atomic number into element symbol.
;	I4EIZ0		Converts element symbol into atomic number.
;	FILE_ACC	Checks availability of files.
;	NUM_CHK		Checks validity of numerical data.
;
; SIDE EFFECTS:
;       IN311_GET_VAL() Widget management routine in this file.
;       IN311_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Originally written by Tim Hammond, Tessella Support Services plc, 
;	18th December 1995 for use with adas 310. Modified by Harvey
;	Anderson for use with adas 311.
;
; MODIFIED:
;       1.1     Harvey Anderson 
;		First release. Created from skeleton of cw_adas310_in.
;
; VERSION:
;       1.1     01-28-99
;
;-
;-----------------------------------------------------------------------------

FUNCTION in311_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.expid, get_value=fileval
    widget_control, state.chxid, get_value=fileval2
    widget_control, state.iz0id, get_value=iz0val
    widget_control, state.zid, get_value=zval
    iz0string = strupcase(strcompress(iz0val(0), /remove_all))
    i4eiz0, iz0string, iz0out
    zout = float(zval(0))
    dsnex = fileval.rootpath + fileval.file
    dsncx = fileval2.rootpath + fileval2.file

    ps = {	EROOTPATH	:	fileval.rootpath,		$
		EFILE		:	fileval.file,			$
		ECENTROOT	:	fileval.centroot,		$
		EUSERROOT	:	fileval.userroot,		$
		CROOTPATH	:	fileval2.rootpath,		$
		CFILE		:	fileval2.file,			$
		CCENTROOT	:	fileval2.centroot,		$
		CUSERROOT	:	fileval2.userroot,		$
                Z               :       zout,                      	$
                IZ0             :       iz0out,                      	$
                DSNEX           :       dsnex,                     	$
                DSNCX           :       dsncx                  		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in311_event, event

    COMMON adas311_inblock, userdir, centdir

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

		;**** Clear any messages away ****

    widget_control, state.messid, set_value= '   '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**** Event from expansion file selection ****

	state.expid: begin
	    widget_control, state.chxid, get_value=chxval
            if event.action eq 'newfile' then begin
                if strcompress(chxval.file, /remove_all) ne '' then begin
		    filename = chxval.rootpath + chxval.file
                    file_acc, filename, fileexist, read, write, 	$
                              execute, filetype
		    if filetype eq '-' then begin
                        widget_control, state.doneid, /sensitive
		    endif
		endif
                widget_control, state.browseid, /sensitive
            endif else begin
                widget_control, state.doneid, sensitive=0
                if strcompress(chxval.file, /remove_all) eq '' then begin
                    widget_control, state.browseid, sensitive=0
		endif
            endelse
        end

		;**** Event from charge exchange file selection ****

	state.chxid: begin
	    widget_control, state.expid, get_value=expval
            if event.action eq 'newfile' then begin
		if strcompress(expval.file, /remove_all) ne '' then begin
		    filename = expval.rootpath + expval.file
                    file_acc, filename, fileexist, read, write, 	$
                              execute, filetype
		    if filetype eq '-' then begin
                        widget_control, state.doneid, /sensitive
		    endif
		endif
                widget_control, state.browseid, /sensitive
            endif else begin
                widget_control, state.doneid, sensitive=0
		if strcompress(expval.file, /remove_all) eq '' then begin
                    widget_control, state.browseid, sensitive=0
		endif
            endelse
        end

		;**** Text typeins ****

	state.zid: widget_control, state.iz0id, /input_focus
	state.iz0id: widget_control, state.zid, /input_focus

                ;***********************
                ;**** Browse button ****
                ;***********************

        state.browseid: begin

                ;**** Invoke comments browsing ****

	    widget_control, state.expid, get_value=expval
	    if strcompress(expval.file, /remove_all) ne '' then begin
		filename = expval.rootpath + expval.file
                file_acc, filename, fileexist, read, write, execute, filetype
		if filetype eq '-' then begin
		    xxtext, filename, font=state.font_large
		endif
	    endif
	    widget_control, state.chxid, get_value=chxval
	    if strcompress(chxval.file, /remove_all) ne '' then begin
		filename = chxval.rootpath + chxval.file
                file_acc, filename, fileexist, read, write, execute, filetype
		if filetype eq '-' then begin
		    xxtext, filename, font=state.font_large
		endif
	    endif
        end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

	state.doneid: begin
	    error = 0

		;************************************************
		;**** Check that the element symbol is valid ****
		;************************************************

	    widget_control, state.iz0id, get_value=iz0val
	    iz0testval = strcompress(iz0val(0), /remove_all)
	    if iz0testval ne '' then begin
		i4eiz0, strupcase(iz0testval), iz0flag
		if iz0flag eq 0 then begin
		    widget_control, state.messid, set_value='**** Error:'  $
		    +' The beam species element symbol is invalid ****'
		    error = 1
		endif
	    endif else begin
		widget_control, state.messid, set_value='**** Error:'  	   $
                +' You must enter a beam species element symbol ****'
		error = 1
	    endelse

		;********************************************
		;**** Check that the ion charge is valid ****
		;********************************************

	    widget_control, state.zid, get_value=zval
	    num_error = num_chk(zval(0), sign=1)
	    if num_error ne 0 then begin
		widget_control, state.messid, set_value='**** Error:'  	$
                +' The beam species ion charge is invalid ****'
		error = 1
	    endif

		;************************************
                ;**** return value or flag error ****
		;************************************

            if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
                new_event = 0L
            endelse
	end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas311_in, parent, VALUE=value, FONT_LARGE=font_large, 	$
                        FONT_SMALL=font_small

    COMMON adas311_inblock, userdir, centdir

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = {	EROOTPATH	:	'./',			$
                        EFILE           :       '',                     $
			ECENTROOT	:	'',			$
			EUSERROOT	:	'',			$
                        CROOTPATH       :       userroot + '/adf01/',   $
                        CFILE           :       '',                     $
                        CCENTROOT       :       centroot + '/adf01/',   $
                        CUSERROOT       :       userroot + '/adf01/',   $
                        Z               :       0.0,                    $
                        IZ0             :       1,                      $
                        DSNEX           :       '',                     $
                        DSNCX           :       ''                     	}
    ENDIF ELSE BEGIN
	inset = {	EROOTPATH       :       value.erootpath,	$
                        EFILE           :       value.efile,            $
			ECENTROOT       :       value.ecentroot,	$
			EUSERROOT       :       value.euserroot,	$
                        CROOTPATH       :       value.crootpath,	$
                        CFILE           :       value.cfile,		$
                        CCENTROOT       :       value.ccentroot,	$
                        CUSERROOT       :       value.cuserroot,	$
                        Z               :       value.z,		$
                        IZ0             :       value.iz0,		$
                        DSNEX           :       value.dsnex,		$
                        DSNCX           :       value.dsncx 		}
        if strtrim(inset.erootpath) eq '' then begin
            inset.erootpath = './'
        endif else if                                                   $
        strmid(inset.erootpath, strlen(inset.erootpath)-1,1) ne '/' then begin
            inset.erootpath = inset.erootpath+'/'
        endif
        if strtrim(inset.crootpath) eq '' then begin
            inset.crootpath = './'
        endif else if                                                   $
        strmid(inset.crootpath, strlen(inset.crootpath)-1,1) ne '/' then begin
            inset.crootpath = inset.crootpath+'/'
        endif
    ENDELSE
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in311_event",		$
                          FUNC_GET_VALUE = "in311_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

                ;*************************************
		;**** Beam species details widget ****
                ;*************************************

    detbase = widget_base(cwid, /column, /frame)
    detlabel = widget_label(detbase, font=font_small,			$
			    value='Please enter beam species details:-')
    detbase2 = widget_base(detbase, /row)
    detlab2 = widget_label(detbase2, font=font_small,			$
			   value='Beam species element symbol : ')
    i4z0ie, inset.iz0, iz0name
    iz0val = ' ' + strcompress(iz0name, /remove_all)
    iz0id = widget_text(detbase2, font=font_small, value=iz0val, 	$
                        xsize=4, /editable)
    detlab3 = widget_label(detbase2, font=font_small,                   $
                           value='Beam species ion charge : ')
    zval = strcompress(string(inset.z, format='(f5.1)'), /remove_all)
    zid = widget_text(detbase2, font=font_small, value=zval, xsize=4,	$
                      /editable)

                ;*****************************************
                ;**** Expansion file selection widget ****
                ;*****************************************

    expbase = widget_base(cwid, /column, /frame)
    expval = {	ROOTPATH	:	inset.erootpath,		$
		FILE		:	inset.efile,			$
		CENTROOT	:	inset.ecentroot,		$
		USERROOT	:	inset.euserroot			}
    exptitle = widget_label(expbase, font=font_small,			$
	                    value='Expansion File Details:-')
    expid = cw_adas_infile(expbase, value=expval, font=font_small,	$
                           ysize=5)

                ;***********************************************
                ;**** Charge exchange file selection widget ****
                ;***********************************************

    chxbase = widget_base(cwid, /column, /frame)
    chxval = {	ROOTPATH	:	inset.crootpath,		$
		FILE		:	inset.cfile,			$
		CENTROOT	:	inset.ccentroot,		$
		USERROOT	:	inset.cuserroot			}
    chxtitle = widget_label(chxbase, font=font_small,			$
	                    value='Charge Exchange File Details:-')
    chxid = cw_adas_infile(chxbase, value=chxval, font=font_small,	$
                           ysize=5)

		;**** Error message ****

    messid = widget_label(cwid, font=font_large,			$
    value='Edit the processing options data and press Done to proceed')

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse Comments',		$
                             font=font_large)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=font_large)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=font_large)

                ;*************************************************
                ;**** Check filenames and desensitise buttons ****
                ;**** if it is a directory or it is a file    ****
                ;**** without read access.                    ****
                ;*************************************************

    browseallow = 0
    filename = inset.erootpath + inset.efile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif else begin
	    browseallow = 1
	endelse
    endelse
    filename = inset.crootpath + inset.cfile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        if browseallow eq 0 then widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            if browseallow eq 0 then widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	font_large,		$
			font_small	:	font_small,		$
			inval		:	inset,			$
			doneid		:	doneid,			$
			browseid	:	browseid,		$
			expid		:	expid,			$
			chxid		:	chxid,			$
			iz0id		:	iz0id,			$
			zid		:	zid,			$
			messid		:	messid,			$
			cancelid	:	cancelid		}

               ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
