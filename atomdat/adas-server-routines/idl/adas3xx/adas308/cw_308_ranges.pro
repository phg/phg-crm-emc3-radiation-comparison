; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/cw_308_ranges.pro,v 1.4 2004/07/06 12:25:20 whitefor Exp $ Date $Date: 2004/07/06 12:25:20 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_308_RANGES()
;
; PURPOSE:
;	Produces a widget for specifying axis scaling in graphical output.
;
; EXPLANATION:
;	This function declares a compound widget to be used for
;	specifying x and y axis ranges to be used in graphical output
;	plotting.  The widget consists of a toggle button, used to
;	activate the remainder of the widget, four text widgets used
;	for numeric data entry and an error message.  When the toggle
;	button is on all text widgets will receive input, when the
;	toggle is off the text widgets are desensitized.
;
;	The eight text widgets are used for XMIN, XMAX, YMIN, YMAX,
;       XMINB, XMAXB, YMINB and YMAXB.
;	Click on a text widget to direct keyboard input to that input.
;	When the user presses the return key the keyboard input is
;	automatically transfered to the next input.  Also the values
;	entered so far are checked to see if they are legal numeric
;	values.
;
;	When the GET_VALUE call is used, the widget makes a more
;	complete check on the entered numeric values.  It checks that
;	all four values have been entered as legal numbers and that
;	minimum values are less than maximum values.  In the event of 
;	an error an error message is issued.  The caller can test to
;	see if the widget input is in error by testing to see if the
;	message returned in the widget value structure is not the
;	null string.
;
;	This widget does not generate any events.
;
; USE:
;	An example of usage;
;
;	base = widget_base(/column)
;	rngid = cw_308_ranges(base)
;	rc = widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc = widget_event()
;	widget_control,rngid,get_value=ranges
;	if ranges.scalbut eq 1 then begin
;	  if strtrim(ranges.grprmess) eq '' then begin
;	    print,'XMINA:',ranges.xmina,'  XMAXA:',ranges.xmaxa
;	    print,'YMINA:',ranges.ymina,'  YMAXA:',ranges.ymaxa
;	    print,'XMINB:',ranges.xminb,'  XMAXB:',ranges.xmaxb
;	    print,'YMINB:',ranges.yminb,'  YMINB:',ranges.yminb
;	  end else begin
;	    print,'Errors in ranges input:',ranges.grprmess
;	  end
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure describing the value of the widget;
;		   {SCALBUT:0, XMINA:'', XMAXA:'', YMINA:'', YMAXA:'', $
;		    XMINB:'', XMAXB:'', YMINB:'', YMAXB:'', GRPRMESS:''}
;
;		   SCALBUT  State of the activation button 0-off 1-on.
;		   XMINA     Minimum on x-axis, as a string.
;		   XMAXA     Maximum on x-axis, as a string.
;		   YMINA     Minimum on y-axis, as a string.
;		   YMAXA     Maximum on y-axis, as a string.
;		   XMINB     Minimum on x-axis (plot B), as a string.
;		   XMAXB     Maximum on x-axis (plot B), as a string.
;		   YMINB     Minimum on y-axis (plot B), as a string.
;                  YMAXB     Maximum on y-axis (plot B), as a string.
;		   GRPRMESS Error message for ranges input.
;
;	SIGN  	 - Defines if values need to be positive or negative
;		   if sign < 0 then values must be negative
;		   if sign > 0 then values must be positive
;		   if sign = 0 then values can be either pos. or neg.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
; CALLS:
;	NUM_CHK		Called fron r308_GET_VAL, check numeric values.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	r308_SET_VAL
;	r308_GET_VAL()
;	r308_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-May-1993
;
; MODIFIED:
;	1.1	Tim Hammond				20-06-95
;	1.2	Tim Hammond				26-06-95
;	1.3	Tim Hammond				27-06-95
;	1.4	William Osborn				01-08-96
;
; VERSION:
;	1.1	Created cw_308_ranges from cw_adas_ranges
;		The only difference is that there are now two
;		sets of minimum and maximum x and y values.
;	1.2	Renamed 'rngval' to 'valrng' in order to prevent
;		conflicts with the same named, but differently
;		defined structure in the original cw_adas_ranges.pro
;	1.3     Renamed all subroutines for same reason as above
;	1.4	Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;-
;-----------------------------------------------------------------------------

PRO r308_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


		;**** Sensitise or desensitise with output button setting ****

  if value.scalbut eq 1 then begin
    widget_control,state.exscid,set_button=1
    widget_control,state.rngbasid,/sensitive
    widget_control,state.xminid,set_value=value.xmina
    widget_control,state.xmaxid,set_value=value.xmaxa
    widget_control,state.yminid,set_value=value.ymina
    widget_control,state.ymaxid,set_value=value.ymaxa
    widget_control,state.xminbid,set_value=value.xminb
    widget_control,state.xmaxbid,set_value=value.xmaxb
    widget_control,state.yminbid,set_value=value.yminb
    widget_control,state.ymaxbid,set_value=value.ymaxb
  end else if value.scalbut eq 0 then begin
    widget_control,state.exscid,set_button=0
    widget_control,state.rngbasid,sensitive=0
    widget_control,state.xminid,set_value=''
    widget_control,state.xmaxid,set_value=''
    widget_control,state.yminid,set_value=''
    widget_control,state.ymaxid,set_value=''
    widget_control,state.xminbid,set_value=''
    widget_control,state.xmaxbid,set_value=''
    widget_control,state.yminbid,set_value=''
    widget_control,state.ymaxbid,set_value=''
  end


		;**** Copy message to widget ****

  if strtrim(value.grprmess) ne '' then begin
    message = value.grprmess
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message


		;**** Copy the new value to state structure ****

  state.valrng.scalbut = value.scalbut
  state.valrng.xmina = value.xmina
  state.valrng.xmaxa = value.xmaxa
  state.valrng.ymina = value.ymina
  state.valrng.ymaxa = value.ymaxa
  state.valrng.xminb = value.xminb
  state.valrng.xmaxb = value.xmaxb
  state.valrng.yminb = value.yminb
  state.valrng.ymaxb = value.ymaxb
  state.valrng.grprmess = value.grprmess

		;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION r308_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Clear existing error message ****

  widget_control,state.messid,set_value=' '
  state.valrng.grprmess = ''

		;**** Get scaling ranges and copy to value ****

  if state.valrng.scalbut eq 1 then begin

    widget_control,state.xminid,get_value=xmina
    widget_control,state.xmaxid,get_value=xmaxa
    widget_control,state.yminid,get_value=ymina
    widget_control,state.ymaxid,get_value=ymaxa
    widget_control,state.xminbid,get_value=xminb
    widget_control,state.xmaxbid,get_value=xmaxb
    widget_control,state.yminbid,get_value=yminb
    widget_control,state.ymaxbid,get_value=ymaxb

		;**** Update state ****

    state.valrng.xmina = xmina(0)
    state.valrng.xmaxa = xmaxa(0)
    state.valrng.ymina = ymina(0)
    state.valrng.ymaxa = ymaxa(0)
    state.valrng.xminb = xminb(0)
    state.valrng.xmaxb = xmaxb(0)
    state.valrng.yminb = yminb(0)
    state.valrng.ymaxb = ymaxb(0)

    error = 0

		;**** Check for missing values ****

    if error eq 0 then begin
      if strtrim(state.valrng.xmina) eq '' then error = 1
      if strtrim(state.valrng.xmaxa) eq '' then error = 1
      if strtrim(state.valrng.ymina) eq '' then error = 1
      if strtrim(state.valrng.ymaxa) eq '' then error = 1
      if strtrim(state.valrng.xminb) eq '' then error = 1
      if strtrim(state.valrng.xmaxb) eq '' then error = 1
      if strtrim(state.valrng.yminb) eq '' then error = 1
      if strtrim(state.valrng.ymaxb) eq '' then error = 1
      if error eq 1 then begin
	state.valrng.grprmess = '**** Missing Value ****'
      end
    end

		;**** Check for illegal values ****

    if error eq 0 then begin
      if num_chk(state.valrng.xmina,/sign) eq 1 then error = 1
      if num_chk(state.valrng.xmaxa,/sign) eq 1 then error = 1
      if num_chk(state.valrng.ymina,/sign) eq 1 then error = 1
      if num_chk(state.valrng.ymaxa,/sign) eq 1 then error = 1
      if num_chk(state.valrng.xminb,/sign) eq 1 then error = 1
      if num_chk(state.valrng.xmaxb,/sign) eq 1 then error = 1
      if num_chk(state.valrng.yminb,/sign) eq 1 then error = 1
      if num_chk(state.valrng.ymaxb,/sign) eq 1 then error = 1
      if error eq 1 then begin
	state.valrng.grprmess = '**** Illegal Numeric Value ****'
      end
    end

		;**** Check x range ****

    if error eq 0 then begin
      if float(state.valrng.xmina) ge float(state.valrng.xmaxa) then error=1
      if error eq 1 then begin
	state.valrng.grprmess = '**** X-MIN must be less than X-MAX ****'
      end
    end

		;**** Check y range ****

    if error eq 0 then begin
      if float(state.valrng.ymina) ge float(state.valrng.ymaxa) then error=1
      if error eq 1 then begin
	state.valrng.grprmess = '**** Y-MIN must be less than Y-MAX ****'
      end
    end

    if error eq 1 then $
	widget_control,state.messid,set_value=state.valrng.grprmess

  end



  RETURN, state.valrng

END

;-----------------------------------------------------------------------------

FUNCTION r308_event, event


		;**** Base ID of compound widget ****

  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

		;**** Clear existing error message ****

  widget_control,state.messid,set_value=' '
  state.valrng.grprmess = ''

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.exscid: begin
	if state.valrng.scalbut eq 0 then begin
	  state.valrng.scalbut = 1
	  widget_control,state.exscid,set_button=1
	  widget_control,state.rngbasid,/sensitive
	  widget_control,state.xminid,set_value=state.valrng.xmina
	  widget_control,state.xmaxid,set_value=state.valrng.xmaxa
	  widget_control,state.yminid,set_value=state.valrng.ymina
	  widget_control,state.ymaxid,set_value=state.valrng.ymaxa
          widget_control,state.xminbid,set_value=state.valrng.xminb
          widget_control,state.xmaxbid,set_value=state.valrng.xmaxb
          widget_control,state.yminbid,set_value=state.valrng.yminb
          widget_control,state.ymaxbid,set_value=state.valrng.ymaxb
	end else begin
	  state.valrng.scalbut = 0
	  widget_control,state.exscid,set_button=0
	  widget_control,state.rngbasid,sensitive=0
	  widget_control,state.xminid,get_value=xmina
	  widget_control,state.xmaxid,get_value=xmaxa
	  widget_control,state.yminid,get_value=ymina
	  widget_control,state.ymaxid,get_value=ymaxa
          widget_control,state.xminbid,get_value=xminb
          widget_control,state.xmaxbid,get_value=xmaxb
          widget_control,state.yminbid,get_value=yminb
          widget_control,state.ymaxbid,get_value=ymaxb
	  state.valrng.xmina = xmina(0)
	  state.valrng.xmaxa = xmaxa(0)
	  state.valrng.ymina = ymina(0)
	  state.valrng.ymaxa = ymaxa(0)
          state.valrng.xminb = xminb(0)
          state.valrng.xmaxb = xmaxb(0)
          state.valrng.yminb = yminb(0)
          state.valrng.ymaxb = ymaxb(0)
	  widget_control,state.xminid,set_value=''
	  widget_control,state.xmaxid,set_value=''
	  widget_control,state.yminid,set_value=''
	  widget_control,state.ymaxid,set_value=''
          widget_control,state.xminbid,set_value=''
          widget_control,state.xmaxbid,set_value=''
          widget_control,state.yminbid,set_value=''
          widget_control,state.ymaxbid,set_value=''
	end
    end

    state.xminid: widget_control,state.xmaxid,/input_focus

    state.xmaxid: widget_control,state.yminid,/input_focus

    state.yminid: widget_control,state.ymaxid,/input_focus

    state.ymaxid: widget_control,state.xminbid,/input_focus

    state.xminbid: widget_control,state.xmaxbid,/input_focus

    state.xmaxbid: widget_control,state.yminbid,/input_focus

    state.yminbid: widget_control,state.ymaxbid,/input_focus

    state.ymaxbid: widget_control,state.xminid,/input_focus

    ELSE:

  ENDCASE

		;**** Check numeric values ****

  if event.id ne state.exscid then begin
    error = 0
    i = 0
    id = [state.xminid, state.xmaxid, state.yminid, state.ymaxid,	$
          state.xminbid, state.xmaxbid, state.yminbid, state.ymaxbid]
    while (error eq 0 and i lt 4) do begin

      valueid = id(i)
      widget_control,valueid,get_value=num
      if strtrim(num(0)) ne '' then begin
        error = num_chk(num(0),/sign)
        if error eq 1 then begin
	  state.valrng.grprmess = '**** Illegal Numeric Value ****'
          widget_control,state.messid,set_value=state.valrng.grprmess
          widget_control,valueid,/input_focus
        end
      end
      i = i + 1

    end
  end


		;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_308_ranges, parent, SIGN=sign, 				$
		VALUE=value, FONT=font, UVALUE=uvalue



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_308_ranges'

  IF (NOT KEYWORD_SET(sign)) then sign=0

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(value)) THEN begin
	  valrng = {valrng, 						$
			SCALBUT:0, 					$
			XMINA:'', XMAXA:'', 				$
			YMINA:'', YMAXA:'', 				$
			XMINB:'', XMAXB:'', 				$
			YMINB:'', YMAXB:'', 				$
			GRPRMESS:'', SIGN:sign }
	end else begin
	  valrng = {valrng, 						$
			SCALBUT:value.scalbut, 				$
			XMINA:value.xmina, XMAXA:value.xmaxa, 		$
			YMINA:value.ymina, YMAXA:value.ymaxa, 		$
			XMINB:value.xminb, XMAXB:value.xmaxb, 		$
			YMINB:value.yminb, YMAXB:value.ymaxb, 		$
			GRPRMESS:value.grprmess , SIGN:sign}
	end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****

  main    = WIDGET_BASE(parent, UVALUE = uvalue, 			$
		EVENT_FUNC = "r308_event", 				$
		FUNC_GET_VALUE = "r308_get_val", 			$
		PRO_SET_VALUE = "r308_set_val", 			$
		/COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(main)

    scbasid = widget_base(first_child,/column)

		;**** Scaling and ranges widget ****

  inbase = widget_base(scbasid,/row)

  base2 = widget_base(inbase,/column)
  base = widget_base(base2,/column,/nonexclusive)
  exscid = widget_button(base,value='Explicit Scaling',font=font)
  labaid = widget_label(base2, value='Plot A:', font=font)
  ladumid = widget_label(base2, value=" ")
  ladumid2 = widget_label(base2, value=" ")
  ladumid3 = widget_label(base2, value=" ")
  labbid = widget_label(base2, value='Plot B:', font=font)

  rngbasid = widget_base(inbase,/row)

  minbase = widget_base(rngbasid,/column)
  maxbase = widget_base(rngbasid,/column)

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='X-min :',font=font)
  xminid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='X-max :',font=font)
  xmaxid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='Y-min :',font=font)
  yminid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='Y-max :',font=font)
  ymaxid = widget_text(base,/editable,xsize=8,font=font)

  mingap = widget_label(minbase, value=" ")

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='X-min :',font=font)
  xminbid = widget_text(base,/editable,xsize=8,font=font)

  maxgap = widget_label(maxbase, value=" ")

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='X-max :',font=font)
  xmaxbid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='Y-min :',font=font)
  yminbid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='Y-max :',font=font)
  ymaxbid = widget_text(base,/editable,xsize=8,font=font)

		;**** Message ****

  if strtrim(valrng.grprmess) eq '' then begin
    message = ' '
  end else begin
    message = valrng.grprmess
  end
  messid = widget_label(scbasid,value=message,font=font)


		;**** Set initial state according to value ****

  if valrng.scalbut eq 1 then begin
    widget_control,exscid,set_button=1
    widget_control,xminid,set_value=valrng.xmina
    widget_control,xmaxid,set_value=valrng.xmaxa
    widget_control,yminid,set_value=valrng.ymina
    widget_control,ymaxid,set_value=valrng.ymaxa
    widget_control,xminbid,set_value=valrng.xminb
    widget_control,xmaxbid,set_value=valrng.xmaxb
    widget_control,yminbid,set_value=valrng.yminb
    widget_control,ymaxbid,set_value=valrng.ymaxb
  end else begin
    widget_control,rngbasid,sensitive=0
  end

		;**** Create state structure ****

  new_state = { EXSCID:exscid, RNGBASID:rngbasid, MESSID:messid, 	$
		XMINID:xminid, XMAXID:xmaxid, YMINID:yminid, YMAXID:ymaxid, $
		XMINBID:xminbid, XMAXBID:xmaxbid, YMINBID:yminbid, 	$
		YMAXBID:ymaxbid, VALRNG:valrng, 			$
		FONT:font }

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main    

END
