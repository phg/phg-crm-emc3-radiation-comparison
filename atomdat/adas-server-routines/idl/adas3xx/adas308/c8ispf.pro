; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/c8ispf.pro,v 1.4 2004/07/06 11:55:22 whitefor Exp $ Date $Date: 2004/07/06 11:55:22 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C8ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS308 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS308
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS308
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	C8ISPF.
;
; USE:
;	The use of this routine is specific to ADAS308, see adas308.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS308 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS308 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas308.pro.  If adas308.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
; 		                      	new   : 0,              $
;              		         	title : '',             $
;					ramso : 0.0,		$
;					tiev  : 0.0 ,      	$
;					tev   : 0.0 ,      	$
;					densz : 0.0 ,      	$
;					dens  : 0.0 ,      	$
;					zeff  : 0.0 ,		$
;					bmag  : 0.0 ,		$
;					nbeam : 0,		$
;					bmfra : fltarr(6),	$
;					bmena : fltarr(6),	$
;					noline: 0,		$
;					nu    : intarr(10),	$
;					nl    : intarr(10),	$
;					emisa : fltarr(10),	$
;					npl   : intarr(20),	$
;					npu   : intarr(20),	$
;					npkey : intarr(20),	$
;					npline: 0,		$
;					lrttb : 0,		$
;					itable: 0,		$
;					ibstat: 0,		$
;					iemms : 0,		$
;					itheor: 0		$
;             			  }
;
;		  See cw_adas308_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       mxtab   - (Int) Maximum number of emissivity tables allowed.
;
;       mxgrf   - (Int) Maximum number of emissivity graphs allowed.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;       ngrnd   - (Int) Minimum allowed N quantum number.
;
;       ntot    - (Int) Maximum allowed N quantum number.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;	
;	graphdex - (Intarr) indices of the requested predictions for
;		    which graphs are required.
;
;	ritit    - First part of the title placed to the right of all graphs.
;
;	ritit2   - 2nd part of the title placed to the right of all graphs.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS308_PROC	Invoke the IDL interface for ADAS308 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS308 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 5-Jun-1995
;
; MODIFIED:
;	1.1     Tim Hammond 
;		First release.
;	1.2	Tim Hammond
;		Tidied code up.
;	1.3	Tim Hammond
;		Added code for "return to menu" button.
;	1.4	Tim Hammond
;		Added "intelligent" spacing of the labels for the plots.
;
; VERSION:
;	1.1	05-06-95
;	1.2	20-06-95
;	1.3	21-06-96
;	1.4	08-06-96
;
;-
;-----------------------------------------------------------------------------


PRO c8ispf, pipe, lpend, procval, dsfull,	 			$
            symbd, idz0, symbr, irz0, irz1, irz2,			$
	    ntot, ngrnd, mxtab, mxgrf, ngrf, graphdex, ritit, rtit2,	$
	    gomenu, bitfile,                                            $
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
        edit_fonts = {font_norm:'',font_input:''}
    small_check = GETENV('VERY_SMALL')

    lpend = 0				;Cancel/done flag


		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        bmarr = fltarr(6)
        osarr = intarr(10)
        os2arr = fltarr(10)
        procval = {		          $
		  new   : 0 ,             $
		  title : '',		  $
                  ramsno: 0.0,            $
                  tiev  : 0.0 ,           $
                  tev   : 0.0 ,           $
                  densz : 0.0 ,           $
                  dens  : 0.0 ,           $
                  zeff  : 0.0 ,           $
                  bmag  : 0.0 ,           $
		  nbeam : 0,		  $
		  bmfra : bmarr,	  $
		  bmena : bmarr,	  $
		  noline: 0,		  $
		  nu    : osarr,	  $
		  nl    : osarr,	  $
 		  emisa : os2arr,	  $
		  npl   : intarr(20),	  $
		  npu   : intarr(20),	  $
		  npkey : intarr(20),	  $
		  npline: 0,		  $
		  lrttb : 0,		  $
		  itable: 0,		  $
		  ibstat: 0,		  $
		  iemms : 0,		  $
		  itheor: 0		  $
	        }
    end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas308_proc, procval, dsfull, action, 				$
                  symbd, idz0, symbr, irz0, irz1, irz2,                 $
        	  ntot, ngrnd,mxtab, mxgrf, bitfile,			$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are three possible events    ****
		;**** 'Menu', 'Done' and 'Cancel'.       ****
		;********************************************

    gomenu=0
    if action eq 'Done' then begin
        lpend = 0
    end else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    printf, pipe, procval.title,format='(a40)'
    printf, pipe, procval.ramsno
    printf, pipe, procval.tiev
    printf, pipe, procval.tev
    printf, pipe, procval.densz
    printf, pipe, procval.dens
    printf, pipe, procval.zeff
    printf, pipe, procval.bmag
    printf, pipe, (procval.itheor+1)
    printf, pipe, (procval.ibstat+1)
    printf, pipe, (procval.iemms+1)
    printf, pipe, procval.nbeam
    for i = 0, (procval.nbeam-1) do begin
        printf, pipe, procval.bmfra(i)
        printf, pipe, procval.bmena(i)
    endfor
    printf, pipe, procval.noline
    for i = 0, (procval.noline-1) do begin
        printf, pipe, procval.nu(i)
        printf, pipe, procval.nl(i)
        printf, pipe, procval.emisa(i)
    endfor
    printf, pipe, procval.npline
    for i = 0, (procval.npline -1) do begin
        printf, pipe, procval.npu(i)
        printf, pipe, procval.npl(i)
    endfor
    printf, pipe, procval.lrttb

                ;********************************************
                ;**** Calculate indices of tables/graphs ****
                ;**** for output/display.                ****
                ;********************************************

    graphdex = where(procval.npkey eq 1)
    if (graphdex(0) eq -1) then begin		;No graphs
        ngrf = 0
    endif else begin
        graphsize = size(graphdex)		;Graphs requested
        ngrf = graphsize(1)
        idgrf = graphdex + 1
    endelse
    printf, pipe, ngrf
    if (ngrf ne 0) then begin
        for i = 0, (ngrf-1) do begin
            printf, pipe, idgrf(i)
        endfor
    endif

    tabledex = where(procval.npkey ne 0)
    if (tabledex(0) eq -1) then begin		;No tables or graphs
        ntab = 0
    endif else begin
        tablesize = size(tabledex)
        ntab = tablesize(1)
        idtab = tabledex + 1
    endelse
    printf, pipe, ntab
    if (ntab ne 0) then begin
        for i = 0, (ntab-1) do begin
            printf, pipe, idtab(i)
        endfor
    endif

		;************************************************
		;**** Construct the labels for the right hand ***
		;**** side of the graphical output	      ***
                ;************************************************

    ritit = '------- USER INPUT DATA -------!C!C'
    ritit = ritit+'------ DONOR INFORMATION ------!C!C'
    ritit = ritit+'ELEMENT SYMBOL!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'NUCLEAR CHARGE!C!C'
    ritit = ritit+'----- RECEIVER INFORMATION -----!C!C'
    ritit = ritit+'ELEMENT SYMBOL!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'NUCLEAR CHARGE!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'INITIAL CHARGE!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'FINAL CHARGE!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'ATOMIC MASS NO!C!C'
    ritit = ritit+'------ PLASMA PARAMETERS ------!C!C'
    ritit = ritit+'ION TEMP.      !C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'ELECTRON TEMP.  !C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'ION DENS.      !C
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'ELECTRON DENS.!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'EFFECTIVE Z!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'MAG. INDUCTION !C!C'
    ritit = ritit+'------- BEAM PARAMETERS -------!C!C'
    ritit = ritit+'INDEX   FRACTION      ENERGY (eV)!C!C'
    for i = 0, (procval.nbeam-1) do begin
        ritit = ritit+'  '+strtrim(string(i+1),2)+'      '
        ritit = ritit+strtrim(string(procval.bmfra(i), format='(f5.3)'),2)
        ritit = ritit+'         '
        ritit = ritit+strtrim(string(procval.bmena(i), format='(E10.2)'),2)
        ritit = ritit+'!C'
        if small_check eq 'YES' then ritit=ritit+'!C'
    endfor
    ritit = ritit+'!C'
    if small_check eq 'YES' then ritit=ritit+'!C'
    ritit = ritit+'--- OBSERVED SPECTRUM LINES ---!C!C'
    ritit = ritit+'INDEX  NU  NL    COL. EMIS.(Ph cm!E-2!N s!E-1!N)!C!C'
    for i =0, (procval.noline - 1) do begin
        ritit = ritit+'  '+strtrim(string(i+1), 2)+'     '
        ritit = ritit+strtrim(string(procval.nu(i)), 2)+'   '
        ritit = ritit+strtrim(string(procval.nl(i)), 2)+'          '
        ritit = ritit+strtrim(string(procval.emisa(i), format='(E10.2)'),2)
        ritit = ritit+'!C'
        if small_check eq 'YES' then ritit=ritit+'!C'
    endfor
    ritit = ritit+'!C'
    ritit = ritit+'------ MODEL INFORMATION ------!C!C'
    ritit = ritit+'CHARGE EXCHANGE MODEL : '
    if (procval.itheor eq 0) then begin
        ritit = ritit+'INPUT DATA SET!C'
        if small_check eq 'YES' then ritit=ritit+'!C'
    endif else begin
        ritit = ritit+'EIKONAL MODEL!C'
        if small_check eq 'YES' then ritit=ritit+'!C'
    endelse
    ritit = ritit+'EMISSION MEASURE MODEL : '
    if (procval.iemms eq 0) then begin
        ritit = ritit+'CHARGE EXCHANGE!C'
    endif else begin
        ritit = ritit+'ELECTRON IMPACT !C'
        ritit = ritit+'                             EXCITATION'
    endelse
    rtit2 = '!C!C!C!C'
    rtit2 = rtit2+'=  '+strtrim(symbd, 2)+'!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(idz0, 2)+'!C!C!C!C'
    rtit2 = rtit2+'=  '+strtrim(symbr, 2)+'!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(irz0, 2)+'!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(irz1, 2)+'!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(irz2, 2)+'!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strcompress(string(procval.ramsno,              $
    format='(f5.2)'))+'!C!C!C!C'
    rtit2 = rtit2+'=  '+strtrim(string(procval.tiev, format='(E10.2)'),2)+'eV!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(string(procval.tev, format='(E10.2)'),2)+'eV!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(string(procval.densz, 			$
    format='(E10.2)'),2)+'cm!E-3!N!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(string(procval.dens, 			$
    format='(E10.2)'),2)+'cm!E-3!N!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(string(procval.zeff, format='(f7.2)'),2)+'!C'
    if small_check eq 'YES' then rtit2=rtit2+'!C'
    rtit2 = rtit2+'=  '+strtrim(string(procval.bmag,format='(f7.2)'),2)+'T!C!C'


END
