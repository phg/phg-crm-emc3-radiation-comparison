; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/c8outg.pro,v 1.3 2004/07/06 11:55:49 whitefor Exp $ Date @(#)$Header: /home/adascvs/idl/adas3xx/adas308/c8outg.pro,v 1.3 2004/07/06 11:55:49 whitefor Exp $                  
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C8OUTG
;
; PURPOSE:
;	Graphics output.
;
; EXPLANATION:
;	The routine invokes the IDL graphical output routine for 
;	ADAS308. It also reads in the output data from c8outg.for.
;
; USE:
;	The use of this routine is specific to ADAS308 see adas308.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS308 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;       Plot A values:-
;
;	XMINA	 - String; User specified x-axis minimum, number as string.
;
;	XMAXA	 - String; User specified x-axis maximum, number as string.
;
;	YMINA	 - String; User specified y-axis minimum, number as string.
;
;	YMAXA	 - String; User specified y-axis maximum, number as string.
;
;	Plot B values:-
;
;       XMINB    - String; User specified x-axis minimum, number as string.
;
;       XMAXB    - String; User specified x-axis maximum, number as string.
;
;       YMINB    - String; User specified y-axis minimum, number as string.
;
;       YMAXB    - String; User specified y-axis maximum, number as string.
;
;	NPU	 - Indices of upper principal quantum numbers for 
;		   requested emissivity predictions.
;
;	NPL     - Indices of lower principal quantum numbers for
;                  requested emissivity predictions.
;
;	GRAPHDEX - Indices of the graphs which have been requested.
;
;	RITIT	 - First part of the title to place at the right.
;
;	RITIT2   - 2nd part of the title to place at the right of the plots.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;		  to series menu'.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS308_PLOT	ADAS308 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 15-Jun-1995
;
; MODIFIED:
;	1.1	Tim Hammond			
;		Tidied up code and comments
;	1.2	Tim Hammond		
;		First release
;	1.3	Tim Hammond
;		Added code for 'return to menu' button.
;
; VERSION:
;	1.1	15-06-95
;	1.2	20-06-95
;
;-
;-----------------------------------------------------------------------------

PRO c8outg, dsfull, pipe, utitle, grpscal, xmina, xmaxa, ymina, ymaxa,  $
		  xminb, xmaxb, yminb, ymaxb, 				$
		  npu, npl, graphdex, ritit, rtit2,   			$
		  hrdout, hardname, device, header, gomenu, bitfile,    $
                  FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;********************************
                ;**** Read data from FORTRAN ****
                ;********************************

    itemp = 0
    ftemp = 0.0

    readf, pipe, itemp
    ngrf = itemp
    if (ngrf gt 0) then begin
        readf, pipe, itemp
        mxa = itemp
        readf, pipe, itemp
        mxb = itemp
    
        xa = fltarr(ngrf, mxa)
        ya = fltarr(ngrf, mxa)
        xb = fltarr(ngrf, mxb)
        yb = fltarr(ngrf, mxb)

        qeff = fltarr(ngrf)

        for j = 0, (ngrf-1) do begin
            readf, pipe, itemp
            npts = itemp
            readf, pipe, ftemp
            qeff(j) = ftemp
            for i = 0, (mxa-1) do begin
                readf, pipe, ftemp
                xa(j,i) = ftemp
                readf, pipe, ftemp
                ya(j,i) = ftemp
            endfor
            if (npts lt mxa) then begin
                xa(j,npts:*) = 0.0
                ya(j,npts:*) = 0.0
            endif
            for i = 0, (mxb-1) do begin
                readf, pipe, ftemp
                xb(j,i) = ftemp
                readf, pipe, ftemp
                if (ftemp lt 1.0e-37) then ftemp =0.0
                yb(j,i) = ftemp
            endfor
        endfor
    endif


		;***********************
		;**** Plot the data ****
		;***********************

    adas308_plot, dsfull, ngrf, grpscal, utitle,			$
                  xmina, xmaxa, ymina, ymaxa, 				$
		  xminb, xmaxb, yminb, ymaxb,				$
		  xa, ya, xb, yb, 					$
		  npu,npl, graphdex, qeff, ritit, rtit2, 		$
		  hrdout, hardname, device, header, bitfile, gomenu,    $
                  FONT=font


END
