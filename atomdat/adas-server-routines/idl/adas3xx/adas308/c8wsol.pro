; Copyright (c) 1995, Strathclyde University
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/c8wsol.pro,v 1.2 2019/04/09 14:03:20 mog Exp $ Date $Date: 2019/04/09 14:03:20 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       C8WSOL
;
; PURPOSE:
;       Communicates with FORTRAN C8WSOL.FOR via pipe and solves
;       series of real tridiagonal linear equations with a single right
;       hand side
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS308
;       FORTRAN process via a UNIX pipe.  The set of linear equations
;       are then solved and the result written back to the FORTRAN.
;
; USE:
;       The use of this routine is specific to ADAS308.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS308 FORTRAN process.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       Uses the IDL mathematics library routine NR_TRIDAG to arrive at
;       the solution for the linear equations.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 12-Jun-1995
;
; MODIFIED:
;       1.1             Tim Hammond                     12-06-95
;
; VERSION:
;       1.1             First release
;
;-
;-----------------------------------------------------------------------------


PRO c8wsol, pipe

    ;**** Read inputs from FORTRAN routine ****

    ntemp = 1
    itemp = 0
    ftemp = 0.0

    readf, pipe, ntemp

    while (ntemp eq 1) do begin

        readf, pipe, itemp
        n = itemp
    
        vd = fltarr(n)
        vds = fltarr(n)
        vdi = fltarr(n)
        rhs = fltarr(n)

        for i=0,(n-1) do begin
            readf, pipe, ftemp
            vd(i) = ftemp
        endfor
        for i=0,(n-1) do begin
            readf, pipe, ftemp
            vds(i) = ftemp
        endfor
        for i=0,(n-1) do begin
            readf, pipe, ftemp
            vdi(i) = ftemp
        endfor
        for i=0,(n-1) do begin
            readf, pipe, ftemp
            rhs(i) = ftemp
        endfor

	;**** Swap first and last elements of vds ****

        swap = vds(1:*)
        vds = [swap,0.0]

        ;**** Now calculate the solution 'remq' ****

        answer = nr_tridag(vdi, vd, vds, rhs, /double)
        answer = float(answer)

        ;**** Write the solution back to the FORTRAN ****

        for i = 0, (n-1) do begin
            printf, pipe, answer(i)
        endfor
    
        readf, pipe, ntemp

    endwhile

END
            
