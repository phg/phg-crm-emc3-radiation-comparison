; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/plot308.pro,v 1.6 2004/07/06 14:31:50 whitefor Exp $ Date $Date: 2004/07/06 14:31:50 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	PLOT308
;
; PURPOSE:
;	Plot one graph for ADAS308.
;
; EXPLANATION:
;	This routine plots ADAS308 output for up to 2 graphs.
;	Each display contains 2 plots.
;
; USE:
;	Use is specific to ADAS308.  See adas308_plot.pro for
;	example.
;
; INPUTS:
;
;	DSNINC	- String; Input COPASE data set name.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;       XMINA   - Double; Minimum value for X axis (plot A).
;
;       XMAXA   - Double; Maximum value for X axis (plot A).
;
;       YMINA   - Double; Minimum value for Y axis (plot A).
;
;       YMAXA   - Double; Maximum value for Y axis (plot A).
;
;       XMINB   - Double; Minimum value for X axis (plot B).
;
;       XMAXB   - Double; Maximum value for X axis (plot B).
;
;       YMINB   - Double; Minimum value for Y axis (plot B).
;
;       YMAXB   - Double; Maximum value for Y axis (plot B).
;
;       X       - Fltarr; X values for plots A
;
;       Y       - Fltarr; Y values for plots A
;
;       XB      - Fltarr; X values for plots B
;
;       YB      - Fltarr; Y values for plots B
;
;       GRPSCAL - Integer; 1 if user specified axis limits to be used,
;                 0 if default scaling to be used.
;
;       NPU     - Intarr; Upper principal quantum numbers for graphs
;
;       NPL     - Intarr; Lower principal quantum numbers for graphs
;
;       GRAPHDEX  Intarr; indices of the graphs to be plotted.
;
;       QEFF    - Effective rate coefficients (used in labels)
;
;       RITIT   - String; First part of right hand label of graphs
;
;       RITIT2  - String; Second part of right hand label of graphs
;
;       GTIT1   - String; Title for graph.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of font passed to graphical output
;		  widget.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  18-Jun-1995
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First version
;	1.2	Tim Hammond			
;		Tidied up code and comments
;	1.3	Tim Hammond				
;		Added initial erase and changed labels
;       1.4     Tim Hammond
;               Added new COMMON block Global_lw_data which contains the
;               values of left, right, top, bottom, grtop, grright
;	1.5	William Osborn
;		Changed J-resolved to L-resolved in header
;	1.6 	Tim Hammond
;		Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_begin file) then the font sized used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;       1.1	18-06-95
;	1.2	20-06-95
;	1.3	26-06-95
;       1.4     26-02-96
;	1.5	11-07-96
;	1.6	06-08-96
;
;-
;----------------------------------------------------------------------------

PRO plot308, dsninc, header, xmina, xmaxa, ymina, ymaxa, 		$
             xminb, xmaxb, yminb, ymaxb, x, y, xb, yb, grpscal, 	$
	     npu, npl, graphdex, qeff,  ritit, rtit2, gtit1

    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;**** Extra Window area positions ****

    leftb = 0.41
    rightb = 0.70
    grbleft = 0.41
    grbright = 0.70
    erase

		;**** Initialise titles ****
		;**** '!E' shifts for exponent, !N returns to normal ****

    ytitle = 'EMISSIVITY (phot cm!E-2!N s!E-1!N)'
    ybtitle = '(Phot cm!E-2!N sec!E-1!N '+string("305b)+'!E-1!N)'
    xtitle = 'WAVELENGTH ('+string("305b)+')'

		;**** Construct graph title ****
		;**** '!C' is the new line control. ****

    if small_check eq 'YES' then begin
        gtitle='L-RESOLVED CHARGE EXCHANGE EMISSIVITIES: '+gtit1+'!C!C!C'
        gtitle = gtitle+'ADAS   :'+strupcase(header)+'!C!C'
        gtitle = gtitle+'FILE    :'+strcompress(dsninc)+'!C!C'
        gtitle = gtitle+'TRANSITION : N = '+strcompress(string(npu(graphdex)))
        gtitle = gtitle+' -> N = '+strcompress(string(npl(graphdex)))+'!C!C'
        gtitle = gtitle+'EFF. RATE COEFF. (cm!E3!N s!E-1!N) = '
        gtitle = gtitle+strcompress(string(qeff, format='(E10.4)'))
    endif else begin
        gtitle='L-RESOLVED CHARGE EXCHANGE EMISSIVITIES: '+gtit1+'!C!C'
        gtitle = gtitle+'ADAS   :'+strupcase(header)+'!C'
        gtitle = gtitle+'FILE    :'+strcompress(dsninc)+'!C!C'
        gtitle = gtitle+'TRANSITION : N = '+strcompress(string(npu(graphdex)))
        gtitle = gtitle+' -> N = '+strcompress(string(npl(graphdex)))+'!C'
        gtitle = gtitle+'EFF. RATE COEFF. (cm!E3!N s!E-1!N) = '
        gtitle = gtitle+strcompress(string(qeff, format='(E10.4)'))
    endelse


  		;******************************************************
		;****			PLOT A			   ****
		;****       (x vs y, xmina,ymina,xmaxa,ymaxa)	   ****
		;******************************************************

                ;******************************************************
		;**** Find x and y ranges for auto scaling,	   ****
		;**** check x and y in range for explicit scaling. ****
                ;******************************************************

    !p.multi = [0, 2, 1]
    makeplot = 1

    if grpscal eq 0 then begin

		;********************************************
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	         ****
		;********************************************

        xvals = where(x gt 1.6e-36 and x lt 1.0e37)
        yvals = where(y gt 1.6e-36 and y lt 1.0e37)

        if xvals(0) gt -1 then begin
            xmaxa = max(x(xvals))
            xmina = min(x(xvals))
            xmid = (xmina + xmaxa) / 2.0
            if (xmina gt (xmid - 20.0)) then xmina = xmid - 20.0
            if (xmaxa lt (xmid + 20.0)) then xmaxa = xmid + 20.0
        end else begin
            makeplot = 0
        end

        if yvals(0) gt -1 then begin
            ymaxa = max(y(yvals))
            ymina = min(y(yvals))
            if (ymaxa eq ymina) then ymina = 0.0
        end else begin
            makeplot = 0
        end

        style = 1

    end else begin
        xvals = where(x ge xmina and x le xmaxa)
        yvals = where(y ge ymina and y le ymaxa)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
            makeplot = 0
        end
        style = 1
    end

    if makeplot eq 1 then begin

		;**** Set up plotting axes ****

        plot,[xmina,xmaxa],[ymina,ymaxa],/nodata, 			$
		position=[left,bottom,grright,grtop], 			$
		xtitle=xtitle, xstyle=style, ystyle=4, 			$
		xrange=[xmina,xmaxa],yrange=[ymina,ymaxa],		$
		xminor=1, xticklen=0.01, 				$
		charsize=charsize*1.0

		;**** Set up a clearer y axis ****

        ymid = (ymina + ymaxa) / 2.0
        yfactor = float(string(ymid,format='(e10.0)'))
        ylog = fix(alog10(yfactor))
        yfactor = (10.0)^ylog
        axis, yaxis=0, yrange=(!y.crange/yfactor), ystyle=1,		$
                ytitle=ytitle, yminor=2, charsize=charsize*1.0
        blanks = strarr(30)
        blanks(*) = ' '
        axis, yaxis=1, yminor=2, ytickname=blanks 
        ylabel = "x10!E"+string(ylog, format='(i2)')+"!N"
        xyouts, 0.01,0.7,ylabel,/normal, charsize=charsize

		;***********************
		;**** Make  plot  A ****
		;***********************

        npoints = size(where(x gt 0))		;no. of lines to plot
        croppoints = where(y gt ymaxa)		;plot goes outside axes
        if (croppoints(0) ne -1) then y(croppoints) = ymaxa
        ybottom = !y.crange(0)
        for ipoint = 0, (npoints(1) - 1) do begin
            plots, [x(ipoint), x(ipoint)],[ybottom, y(ipoint)], thick = 1.8
        endfor


    end 


                ;******************************************************
                ;****                   PLOT B                     ****
                ;****    (xb vs yb, xminb,yminb,xmaxb,ymaxb)       ****
                ;******************************************************

    makebplot = 1

    if grpscal eq 0 then begin

                ;********************************************
                ;**** identify values in the valid range ****
                ;**** plot routines only work within     ****
                ;**** single precision limits.           ****
		;********************************************

        xvals = where(xb gt 1.6e-36 and xb lt 1.0e37)
        yvals = where(yb gt 1.6e-36 and yb lt 1.0e37)

        if xvals(0) gt -1 then begin
            xmaxb = xmaxa		;Same x-axis as plot A is the default
            xminb = xmina
        end else begin
            makebplot = 0
        end

        if yvals(0) gt -1 then begin
            ymaxb = max(yb(yvals))
            yminb = min(yb(yvals))
        end else begin
            makebplot = 0
        end

        style = 1

    end else begin
        xvals = where(xb ge xminb and xb le xmaxb)
        yvals = where(yb ge yminb and yb le ymaxb)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
            makebplot = 0
        end
        style = 1
    end

    if makebplot eq 1 then begin

                ;**** Set up plotting axes ****

        plot,   xb, yb, 						$
                position=[grbleft,bottom,grbright,grtop], 		$
                xtitle=xtitle, xstyle=style, ystyle=4, 			$
                xrange=[xminb,xmaxb],yrange=[yminb,ymaxb],		$
                xminor=2,               				$
                charsize=charsize*1.0

                ;**** Set up a clearer y axis ****

        ymid = (yminb + ymaxb) / 2.0
        yfactor = float(string(ymid,format='(e10.0)'))
        ylog = fix(alog10(yfactor))
        yfactor = (10.0)^ylog
        axis, yaxis=0, yrange=(!y.crange/yfactor), ystyle=1,                $
                ytitle=ybtitle, yminor=2, charsize=charsize*1.0
        blanks = strarr(30)
        blanks(*) = ' '
        axis, yaxis=1, yminor=2, ytickname=blanks
        ylabel = "x10!E"+string(ylog, format='(i2)')+"!N"
        xyouts, (grbleft-0.04),0.7,ylabel,/normal, charsize=charsize

    end 


                ;**** Print a message to the screen (if necessary) ****

    if makeplot eq 0 then begin
        xyouts,(left*0.5),grtop*0.7,ytitle+'   V   '+xtitle+'!C!C'+         $
        '  ---NO DATA IN SPECIFIED RANGE---',/normal,charsize=charsize*1.0
    endif
    if makebplot eq 0 then begin
        xyouts,(grbleft),(grtop*0.7),ybtitle+'  V  '+xtitle+'!C!C'+$
        '  ---NO DATA IN SPECIFIED RANGE---',/normal,charsize=charsize*1.0
    endif

                ;**** Output title above graphs ****

    xyouts,left,(top+0.05),gtitle,/normal,charsize=charsize*1.0

                ;**** Output titles to right of graphs ****

    if small_check eq 'YES' then begin
        xyouts, 0.75, top, ritit, /normal, charsize=charsize*0.7
        xyouts, 0.87, top, rtit2, /normal, charsize=charsize*0.7
    endif else begin
        xyouts, 0.75, top, ritit, /normal, charsize=charsize*0.9
        xyouts, 0.87, top, rtit2, /normal, charsize=charsize*0.9
    endelse

END
