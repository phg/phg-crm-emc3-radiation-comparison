; Copyright (c) 1995, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/c8spf1.pro,v 1.3 2004/07/06 11:56:15 whitefor Exp $ Date $Date: 2004/07/06 11:56:15 $      
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C8SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS308 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS308 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine C8SPF1.
;
; USE:
;	The use of this routine is specific to ADAS308, See adas308.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS308 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas308.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	NGRF	- The number of output graphs requested by the user.
;
;	HEADER  - Header information used for text output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS308_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS308 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 12-jun-1995
;
; MODIFIED:
;	1.1	Tim Hammond  				
;		First release.
;	1.2	Tim Hammond			
;		Tidied up code and comments
;	1.3	Tim Hammond
;		Added code for 'return to menu' button.
;
; VERSION:
;	1.1	12-06-95
;	1.2	20-06-95
;	1.3	21-06-96
;
;-
;-----------------------------------------------------------------------------

PRO   c8spf1, pipe, lpend, value, dsfull, header, ngrf,			$
              gomenu, bitfile, DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Define some variables before read from pipe ****

    lfsel = 0
    ibsel = 0

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    outtitle = "ADAS308 OUTPUT OPTIONS"
    adas308_out, value, dsfull, action, outtitle, ngrf, bitfile,	$
                  DEVLIST=devlist, FONT=font

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are three possible actions    ****
		;**** 'Menu', 'Done' and 'Cancel'.        ****
		;*********************************************

    gomenu = 0
    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf,pipe,lpend

    if lpend eq 0 then begin

       printf, pipe, value.grpout
       if (value.grpout eq 1) then begin
          printf, pipe, value.grpscal
       endif

		;*** If text output requested tell FORTRAN ****
     
       printf, pipe, value.texout
       if (value.texout eq 1) then begin
	  printf, pipe, value.texdsn
       endif

    endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if value.texout eq 1 then begin
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
            value.texapp=0
            value.texdef = value.texdsn
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
    endif

END
