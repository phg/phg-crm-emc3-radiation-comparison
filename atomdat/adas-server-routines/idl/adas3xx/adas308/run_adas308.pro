;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas308
;
; PURPOSE    :  Runs ADAS308 CX coefficient generation code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf01      I     str    charge exchange cross section file
;               te         I     real() electron temperatures requested
;               tion       I     real() ion temperatures requested 
;               dens       I     real() electron densities requested
;               dion       I     real() ion densities requested
;               energy     I     real() beam energy components (eV/amu)
;               frac       I     real() fraction in each beam component (normalized to 1.0)
;               n1req      I     int    lower level of requested coefficient
;               n2req      I     int    upper level of requested coefficient
; 
; OPTIONAL   :  mass       I     real    atomic mass of the receiver
;               n1in       I     int     lower level of observed line
;               n2in       I     int     upper level of observed line
;               emiss      I     real    emission measure of observed line
;               zeff       I     real    for re-scaling ion densities, defaults to 1.0
;               bmag       I     real    magnetic field (not used in model)
;
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; KEYWORDS   :  elec       I      -     = 1 return e-impact values
;                                       = 0 do not (default)
;               kelvin            -     requested temperature in K (default eV)
;               help       I      -     Returns help and exits if set
;
; OUTPUTS    :  data       O     struc  data.qeff  - effective rate coefficient
;                                       data.ncomp - number in nl-n'l' complex
;                                       data.wave  - nl-n'l' air wavelengths
;                                       data.emiss - nl-n'l' column emissivities
;                                       data.pop   - nl-n'l' column populations
;                                       data.nu    - nl-n'l' complex : n
;                                       data.nl    - nl-n'l' complex : n'
;                                       data.lu    - nl-n'l' complex : l
;                                       data.ll    - nl-n'l' complex : l'
;                                       data.aval  - nl-n'l' A-values
;                                       
;
; NOTES      :  - run_adas308 used the spawn command. Therefore IDL v5.3 
;                 cannot be used. Any other version will work.
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  10-05-2001
; 
; UPDATE     :  
;
; MODIFIED:
;       1.1     Richard Martin
;                 - Put under SCCS control.
;       1.2     Martin O'Mullane
;                 - Terminate the program properly to ensure the 
;                   logfile is written.
;                 - Set key to output nl-n'l' components in logfile.
;       1.3     Martin O'Mullane
;                 - Alter the termination sequence when a log file
;                   is not requested.
;                 - Change the parameter name eng to energy.
;                 - Set defaults for n1in, n2in, emiss, zeff and bmag
;                   so that they are not required parameters.
;                 - Update the comments. 
;
; VERSION:
;       1.1     20-07-2001
;       1.2     03-04-2019
;-
;----------------------------------------------------------------------
PRO run_adas308_calc, file, mass, te, tion, dens, dion, zeff, bmag, $
                      frac, eng, n1in, n2in, emiss, n1req, n2req, $
                      is_cx, is_elec, is_log, outfile, state

; Now run the ADAS308 code - the input has been checked already.

    fortdir = getenv('ADASFORT')
    spawn, fortdir+'/adas308.out', unit=pipe, /noshell, PID=pid
    
    date = xxdate()
    printf, pipe, date[0]
   

    printf, pipe, 'NO'
    printf, pipe, file

    cxsetp, pipe, mxtab, mxgrf, symbd, idz0, symbr, irz0, irz1, irz2, $
            ngrnd, ntot
            
    idum = 0
    readf, pipe, idum


    if is_cx then iemms = 1
    if is_elec then iemms = 2
    
    nbeam  = n_elements(eng)
    noline = n_elements(n1in)
    npline = n_elements(n1req)
    
    title = 'run_adas308 IDL command version'

    printf, pipe, 0
    printf, pipe, title,format='(a40)'
    printf, pipe, mass
    printf, pipe, tion
    printf, pipe, te
    printf, pipe, dion
    printf, pipe, dens
    printf, pipe, zeff
    printf, pipe, bmag
    printf, pipe, 1
    printf, pipe, 1
    printf, pipe, iemms
    printf, pipe, nbeam
    for i = 0, nbeam-1 do begin
        printf, pipe, frac[i]
        printf, pipe, eng[i]
    endfor
    printf, pipe, noline
    for i = 0, noline-1 do begin
        printf, pipe, n1in[i]
        printf, pipe, n2in[i]
        printf, pipe, emiss[i]
    endfor
    printf, pipe, npline
    for i = 0, npline-1 do begin
        printf, pipe, n1req[i]
        printf, pipe, n2req[i]
    endfor
    
    lrttb = 0
    printf, pipe, lrttb


    printf, pipe, npline
    printf, pipe, 1
    ntab = 1
    printf, pipe, ntab            ; set to 1 for nl-n'l'
    printf, pipe, 1
    
    printf, pipe, 0

    c8wsol, pipe
    c8emqx, pipe

    idum = 0
    readf, pipe, idum


    lpend = 0
    printf,pipe,lpend

    printf, pipe, 1
    printf, pipe, 0
    
    if is_log then texout = 1 else texout = 0 
    printf, pipe, texout
    if texout EQ 1 then printf, pipe, outfile

    printf, pipe, 0 
    
    
    itemp = 0
    ftemp = 0.0

    readf, pipe, itemp
    ngrf = itemp
    if (ngrf gt 0) then begin
        readf, pipe, itemp
        mxa = itemp
        readf, pipe, itemp
        mxb = itemp
    
        xa = fltarr(ngrf, mxa)
        ya = fltarr(ngrf, mxa)
        xb = fltarr(ngrf, mxb)
        yb = fltarr(ngrf, mxb)

        qeff = fltarr(ngrf)

        for j = 0, (ngrf-1) do begin
            readf, pipe, itemp
            npts = itemp
            readf, pipe, ftemp
            qeff(j) = ftemp
            for i = 0, (mxa-1) do begin
                readf, pipe, ftemp
                xa(j,i) = ftemp
                readf, pipe, ftemp
                ya(j,i) = ftemp
            endfor
            if (npts lt mxa) then begin
                xa(j,npts:*) = 0.0
                ya(j,npts:*) = 0.0
            endif
            for i = 0, (mxb-1) do begin
                readf, pipe, ftemp
                xb(j,i) = ftemp
                readf, pipe, ftemp
                if (ftemp lt 1.0e-37) then ftemp = 0.0
                yb(j,i) = ftemp
            endfor
        endfor
    endif

 
   state = { qeff   : qeff[0],                $
             ncomp  : npts,                   $
             wave   : reform(xa[0,0:npts-1]), $                   
             emiss  : reform(ya[0,0:npts-1])  }                  
                      
printf, pipe, 0
if is_log EQ 1 then printf, pipe, 'ADAS'
printf, pipe, 1
printf, pipe, 1


END



;----------------------------------------------------------------------------

PRO run_adas308, adf01  = adf01,  $
                 mass   = mass,   $
                 te     = te,     $
                 tion   = tion,   $
                 dens   = dens,   $
                 dion   = dion,   $
                 zeff   = zeff,   $
                 bmag   = bmag,   $
                 frac   = frac,   $
                 energy = energy, $
                 eng    = eng,    $
                 n1in   = n1in,   $
                 n2in   = n2in,   $
                 emiss  = emiss,  $
                 n1req  = n1req,  $
                 n2req  = n2req,  $
                 elec   = elec,   $
                 log    = log,    $
                 data   = data
                 

; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other v5 or above version'
   return
endif


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas308'
   return
endif


; Warn about parameter name change

if n_elements(eng) GT 0 AND n_elements(energy) GT 0 then begin
   message, 'eng is deprecated in favour of energy. Do not set both eng and energy'
endif

if n_elements(eng) NE 0 then begin
   message, 'The eng parameter is renamed to energy.', /continue
   message, 'Change calling program to stop this message.', /continue
   energy = eng
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2


; Check the file name and extract the atomic number and mass from line 1

if n_elements(adf01) EQ 0 then begin
    message,'A CX cross section ion file (adf01) is required'
endif else file = adf01

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf01 file does not exist '+file
if read ne 1 then message, 'adf01 file cannot be read from this userid '+file


xxdata_01, file=adf01, fulldata=all

xxeiam, all.symbr, mass_file
izr = all.izr

if n_elements(mass) EQ 0 then mass = mass_file

; Suitable defaults

if n_elements(zeff) EQ 0 then zeff = 1.0
if n_elements(bmag) EQ 0 then bmag = 3.0


; Set suitable defaults for the keywords

if keyword_set(elec) then begin
   is_cx   = 0
   is_elec = 1
endif else begin
   is_cx   = 1
   is_elec = 0
endelse

if n_elements(n1in) EQ 0 then n1in = 2
if n_elements(n2in) EQ 0 then n2in = 1
if n_elements(emiss) EQ 0 then emiss = 1.0D12

; Necessary inputs

; Limit n1req and n2req to one even though ADAS308 can tolerate 2.

if n_elements(n1req) NE 1 OR n_elements(n2req) NE 1 then $
                             message, 'Only one n-n prediction allowed'

nl = min([n1req,n2req], max=nu)
 
; Order emissivities 

test = n1in > n2in
if test NE n1in AND test NE n2in then message, 'Error in input n-n shells'
if n1in[0] GT n2in[0] then begin
   nlin = n2in
   nuin = n1in
endif else begin
   nlin = n1in
   nuin = n2in
endelse

if n_elements(frac) eq 0 then message, 'User requested fractions are missing'
if n_elements(energy) eq 0 then message, 'User requested beam energies are missing'

if n_elements(frac) NE n_elements(energy) then $
         message, 'Discrepency between number of beam fractions and energies'

partype=size(frac, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Beam fractions must be numeric'
endif  else fraction = DOUBLE(frac)

partype=size(energy, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Beam energies must be numeric'
endif  else energy = DOUBLE(energy)



; Temperature and density

if n_elements(te)   NE 1 then message, 'Error in requested electron temperature'
if n_elements(tion) NE 1 then message, 'Error in requested ion temperature'
if n_elements(dens) NE 1 then message, 'Error in requested electron density'
if n_elements(dion) NE 1 then message, 'Error in requested ion density'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Electron temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Electron density must be numeric'
endif else dens = DOUBLE(dens)

partype=size(tion, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Ion temperature must be numeric'
endif else tion = DOUBLE(tion)

partype=size(dion, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Ion density must be numeric'
endif else dion = DOUBLE(dion)

if keyword_set(kelvin) then begin
   te_in   = te/11605.0D0
   tion_in = tion/11605.0D0
endif else begin
   te_in   = te
   tion_in = tion
endelse

; Output file

if n_elements(log) EQ 0 then begin
   output_file = '' 
   is_log = 0
endif else begin
   output_file = log
   is_log = 1
endelse


; Run ADAS308

run_adas308_calc, file, mass, te_in, tion_in, dens, dion, zeff, bmag, $
                  fraction, energy, nuin, nlin, emiss, nu, nl, $
                  is_cx, is_elec, is_log, output_file, state
   


; Return the result in a structure

; Construct the nl-n'l' complex

n  = n1req
n1 = n2req

nu_out = fltarr(state.ncomp)
nl_out = fltarr(state.ncomp)
lu_out = fltarr(state.ncomp)
ll_out = fltarr(state.ncomp)
aval   = fltarr(state.ncomp)

k  = 0
for L = 0, n1 do begin
   for L1 = L+1, L-1, -2 do begin
      if L1 GE 0 AND L1 LT N1 then begin
      
         nu_out[k] = n
         nl_out[k] = n1
         lu_out[k] = l
         ll_out[k] = l1
         
         aval[k] = r8ah(n,l,n1,l1)
         k = k + 1
         
      endif
   endfor
endfor


data = { qeff  : state.qeff,                 $
         ncomp : state.ncomp,                $
         wave  : state.wave,                 $                 
         emiss : state.emiss,                $
         pop   : state.emiss / (aval*izr^4), $
         nu    : nu_out,                     $
         lu    : lu_out,                     $
         nl    : nl_out,                     $
         ll    : ll_out,                     $
         aval  : aval                        }

; Free up allocated units

close,/all
                       

END
