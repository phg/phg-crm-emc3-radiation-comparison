; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/cw_308_gr_sel.pro,v 1.4 2004/07/06 12:25:06 whitefor Exp $ Date $Date: 2004/07/06 12:25:06 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_308_GR_SEL()
;
; PURPOSE:
;	Generates a widget used for graphical output selection.
;
; EXPLANATION:
;	This widget consists of a titled activation button, a text
;	widget for entering a graph title, a scaling ranges input
;	widget cw_308_ranges.pro, an output file widget
;	cw_adas_outfile.pro, a single selection widget cw_single_sel.pro
;	with a list of available hardcopy devices.  A general error
;	message is also included in the widget.
;
;	cw_308_ranges.pro and cw_adas_outfile.pro handle their own
;	events and perform their own error checking.  The general
;	activation button desensitises all other component widgets when
;	it is set off.  The only other events processed by this widget
;	are from the two single selection lists for graphs and devices.
;
;	Many of the elements of this widget are optional and controlled
;	by the caller.
;
;	This widget generates an event when the activation button is
;	pressed.  The event structure is;
;		{ID:0L, TOP:0L, HANDLER:0L, OUTBUT:0}
;	where OUTBUT is 0 if the activation button is off and 1 if
;	the activaion button is on.
;
; USE:
;	An example of use is given below.  Also see cw_adas205_out.pro
;	for another example.
;
;	grplist = ['Graph 1','Graph2','Graph3']
;	devlist = ['Device 1','Device 2','Device 3']
;	value = {$
;		OUTBUT:0 , GTIT1:'', $
;		SCALBUT:0, $
;		XMINA:'', XMAXA:'', $
;		YMINA:'', YMAXA:'', $
;		XMINB:'', XMAXB:'', $
;		YMINB:'', YMAXB:'', $
;		HRDOUT:0, HARDNAME:'', $
;		GRPDEF:'', GRPFMESS:'', $
;		GRPSEL:-1, GRPRMESS:'', $
;		DEVSEL:-1, GRSELMESS:'' }
;
;	base=widget_base(/column)
;	grpid=cw_308_gr_sel(base,value=value,grplist=grplist,devlist=devlist)
;	rc=widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc=widget_event()
;	widget_control,grpid,get_value=value
;
;	if strtrim(value.grselmess) eq '' and $
;	   strtrim(value.grpfmess) eq '' and $
;	   strtrim(value.grprmess) eq '' then begin
;
;	  if value.outbut eq 1 then begin
;	    print,'Graph selected: ',grplist(value.grpsel)
;
;	    if value.hrdout eq 1 then begin
;	      print,'Device selected: ',devlist(value.devsel)
;	      print,'Hard copy file: ',value.hardname
;	    end
;
;	    if value.scalbut eq 1 then begin
;	      print,'Scaling ranges:'
;	      print,'XMINA: ',value.xmina,'  XMAXA: ',value.xmaxa
;	      print,'YMINA: ',value.ymina,'  YMAXA: ',value.ymaxa
;	      print,'XMINB: ',value.xminb,'  XMAXB: ',value.xmaxb
;	      print,'YMINB: ',value.yminb,'  YMAXB: ',value.ymaxb
;	    end
;	  end else begin
;	    print,'No graphical output requested.'
;	  end
;
;	end else begin
;	  print,'Graphical selection error;'
;	  print,value.grselmess,value.grpfmess,value.grprmess
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	SIGN	  - Integer: < 0  => Graph ranges must be negative
;			     0	  => Graph ranges can be positive or negative
;			     > 0  => Graph ranges must be positive
;	OUTPUT	  - A string; Label to use for activate button.  Default
;		    'Graphical Output'.
;
;	GRPLIST	  - A string array; A list of graphs one array element per
;		    graph.  If the list is empty i.e grplist='' then the
;		    selection list widget will not appear.  Default value=''.
;
;	LISTTITLE - A string; title for the graph selection list.
;		    Default 'Select Graph'.
;
;	DEVLIST	  - A string array; A list of devices one array element per
;		    device.  If the list is empty i.e devlist='' then the
;		    selection list widget will not appear.  Default value=''.
;
;	VALUE	  - A structure which defines the value of this widget.
;		    The structure is made up of a number of parts each
;		    pertaining to one of the widgets in this compound
;		    widget.  Default value;
;
;		      { OUTBUT:0 , GTIT1:'', $
;			SCALBUT:0, $
;			XMINA:'', XMAXA:'', $
;			YMINA:'', YMAXA:'', $
;			XMINB:'', XMAXB:'', $
;			YMINB:'', YMAXB:'', $
;			HRDOUT:0, HARDNAME:'', $
;			GRPDEF:'', GRPFMESS:'', $
;			GRPSEL:-1, GRPRMESS:'', $
;			DEVSEL:-1, GRSELMESS:'' }
;
;		    Note these do not appear in the same order as in the
;		    value structure.
;		    General;
;			OUTBUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRSELMESS  String; General error message
;
;		    For CW_308_RANGES;
;			SCALBUT	   Integer; Scaling activation 1 on, 0 off,
;				   -1 no widget.
;			XMINA	   String; x-axis minimum, string of number
;			XMAXA	   String; x-axis maximum, string of number
;			YMINA	   String; y-axis minimum, string of number
;			YMAXA	   String; y-axis maximum, string of number
;			GRPRMESS   String; Scaling ranges error message
;
;		    For CW_ADAS_OUTFILE hard copy output file;
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off,
;				   -1 no widget.
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			(Replace and append values are fixed)
;
;		    For CW_SINGLE_SEL graph selection;
;			DEVSEL	   Integer; index of selected device in DEVLIST
;				   -1 indicates no selection made.
;
;       FONT      - String; Name of a font to use for all text in this widget.
;
;	UVALUE	  - A user value for this widget.
;
; CALLS:
;	CW_308_RANGES	Specify explict x and y axis ranges.
;	CW_ADAS_OUTFILE	Specify output file name.
;	CW_SINGLE_SEL	A single selection list widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	GR_308_SET_VAL
;	GR_308_GET_VAL()
;	GR_308_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-May-1993
;
; MODIFIED:
;	1.1	Tim Hammond				15-06-95
;	1.2	Tim Hammond				26-06-95
;	1.3	Tim Hammond				27-06-95
;	1.4	William Osborn				01-08-96
;
; VERSION:
;	1.1	Created cw_308_gr_sel.pro from cw_adas_gr_sel.pro
;		The only difference being that this one can handle
;		two sets of minimum and maximum x and y values.
;	1.2	Renamed grselval to grvalsel and rngval to valrng
;		to avoid clashes in definitions when structures of
;		the same names are referenced by the original
;		cw_adas_gr_sel.pro called by other adas programs.
;	1.3	Renamed all procedures in file for same reason as above.
;	1.4	Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;-
;-----------------------------------------------------------------------------

PRO gr_308_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Value for the file widget ****

  outfval = { OUTBUT:value.hrdout, APPBUT:-1, REPBUT:0, 		$
              FILENAME:value.hardname, DEFNAME:value.grpdef, 		$
	      MESSAGE:value.grpfmess }

		;**** Value for the ranges widget ****

  if state.scbasid gt 0 then begin
    valrng = {  SCALBUT:value.scalbut, 					$
		XMINA:value.xmina, XMAXA:value.xmaxa, 			$
		YMINA:value.ymina, YMAXA:value.ymaxa, 			$
		XMINB:value.xminb, XMAXB:value.xmaxb, 			$
                YMINB:value.yminb, YMAXB:value.ymaxb, 			$
		GRPRMESS:value.grprmess, SIGN:value.sign }
  end

		;**** Sensitise or desensitise with output button setting ****

  if value.outbut eq 0 then begin

    widget_control,state.actid,set_button=0

		;**** desensitise graph title ****

    widget_control,state.tibasid,sensitive=0

		;**** desensitise graph selection list ****

    if state.listid gt 0 then widget_control,state.lsbasid,sensitive=0

		;**** desensitise device selection list ****

    if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

		;**** desensitise filename and hide settings ****

    widget_control,state.fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,state.fileid,set_value=outfval

		;**** desensitise ranges and hide settings ****

    if state.scbasid gt 0 then begin
      widget_control,state.scbasid,sensitive=0
      valrng.scalbut = 0
      widget_control,state.rangeid,set_value=valrng
    end

  end else begin

    widget_control,state.actid,set_button=1

    widget_control,state.tibasid,/sensitive

    if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

    if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive

    widget_control,state.fbaseid,/sensitive
    widget_control,state.fileid,set_value=outfval

    if state.scbasid gt 0 then begin
      widget_control,state.scbasid,/sensitive
      widget_control,state.rangeid,set_value=valrng
    end

  end

		;**** Update value of graph selection ****

  if state.listid gt 0 then widget_control,state.listid,set_value=value.grpsel

		;**** Update value of device selection ****

  if state.devid gt 0 then widget_control,state.devid,set_value=value.devsel

		;**** Update message ****

  if strtrim(value.grselmess) ne '' then begin
    message = value.grselmess
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message

		;**** Copy the new value to state structure ****

  state.grvalsel.outbut = value.outbut
  state.grvalsel.gtit1 = value.gtit1
  state.grvalsel.scalbut = value.scalbut
  state.grvalsel.xmina = value.xmina
  state.grvalsel.xmaxa = value.xmaxa
  state.grvalsel.ymina = value.ymina
  state.grvalsel.ymaxa = value.ymaxa
  state.grvalsel.xminb = value.xminb
  state.grvalsel.xmaxb = value.xmaxb
  state.grvalsel.yminb = value.yminb
  state.grvalsel.ymaxb = value.ymaxb
  state.grvalsel.hrdout = value.hrdout
  state.grvalsel.hardname = value.hardname
  state.grvalsel.grpdef = value.grpdef
  state.grvalsel.grpfmess = value.grpfmess
  state.grvalsel.grpsel = value.grpsel
  state.grvalsel.grprmess = value.grprmess
  state.grvalsel.devsel = value.devsel
  state.grvalsel.grselmess = value.grselmess


		;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION gr_308_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Get the graph title ****
		;*** truncate to 30 characters ***

  widget_control,state.titleid,get_value=gtit1
  if (strlen(gtit1(0)) gt 30 ) then begin
	gtit1(0) = strmid(gtit1(0),0,30)
        message = 'Warning : Title truncated to 30 characters'
        widget_control,state.messid,set_value=message
        wait, 1.0
	message = ' '
  endif
  state.grvalsel.gtit1 = gtit1(0)

		;**** Get the latest filename ****

  widget_control,state.fileid,get_value=outfval
  state.grvalsel.hardname = outfval.filename
  state.grvalsel.grpfmess = outfval.message
  if state.grvalsel.outbut eq 1 then state.grvalsel.hrdout = outfval.outbut

		;**** Get scaling ranges and copy to value ****

  if state.scbasid gt 0 then begin
    widget_control,state.rangeid,get_value=valrng
    state.grvalsel.xmina = valrng.xmina
    state.grvalsel.xmaxa = valrng.xmaxa
    state.grvalsel.ymina = valrng.ymina
    state.grvalsel.ymaxa = valrng.ymaxa
    state.grvalsel.xminb = valrng.xminb
    state.grvalsel.xmaxb = valrng.xmaxb
    state.grvalsel.yminb = valrng.yminb
    state.grvalsel.ymaxb = valrng.ymaxb
    state.grvalsel.grprmess = valrng.grprmess
    if state.grvalsel.outbut eq 1 then state.grvalsel.scalbut = valrng.scalbut
  end

		;**** Clear any error message ****

  if state.grvalsel.hrdout ge 0 then begin
    state.grvalsel.grselmess = ''
    widget_control,state.messid,set_value=' '
  end

		;**** Check that a graph has been selected if appropriate ****

  if state.listid gt 0L then begin
    if state.grvalsel.grpsel lt 0 then begin
      state.grvalsel.grselmess = '**** You must select a graph ****'
      widget_control,state.messid,set_value=state.grvalsel.grselmess
    end
  end

		;**** Check that a device has been selected if appropriate ****

  if state.grvalsel.hrdout eq 1 and state.devid gt 0L then begin
    if state.grvalsel.devsel lt 0 then begin
      state.grvalsel.grselmess = '**** You must select a device ****'
      widget_control,state.messid,set_value=state.grvalsel.grselmess
    end
  end


  RETURN, state.grvalsel

END

;-----------------------------------------------------------------------------

FUNCTION gr_308_event, event


		;**** Base ID of compound widget ****

  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

  new_event = 0L
		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.actid: begin
			;**** Sensitise and desensitise with toggle ****
			;**** of output button.			    ****

	if state.grvalsel.outbut eq 1 then begin

	  state.grvalsel.outbut = 0
	  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:0}

			;**** Desensitise graph title ****

	  widget_control,state.tibasid,sensitive=0

			;**** Desensitise graph selection list ****

	  if state.listid gt 0 then $
				widget_control,state.lsbasid,sensitive=0

			;**** Desensitise device selection list ****

	  if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

			;**** Desensitise filename and hide settings ****

	  outfval = {	OUTBUT:0, APPBUT:-1, REPBUT:0, 			$
                	FILENAME:state.grvalsel.hardname, 		$
			DEFNAME:state.grvalsel.grpdef, 			$
	        	MESSAGE:'' }
	  widget_control,state.fbaseid,sensitive=0
	  widget_control,state.fileid,set_value=outfval

			;**** Desensitise scaling and hide settings ****
	  if state.scbasid gt 0 then begin
	  valrng = {	SCALBUT:0, 					$
			XMINA:state.grvalsel.xmina, XMAXA:state.grvalsel.xmaxa, $
			YMINA:state.grvalsel.ymina, YMAXA:state.grvalsel.ymaxa, $
			XMINB:state.grvalsel.xminb, XMAXB:state.grvalsel.xmaxb,	$
                        YMINB:state.grvalsel.yminb, YMAXB:state.grvalsel.ymaxb, $
			GRPRMESS:'' , SIGN:state.grvalsel.sign}
	    widget_control,state.scbasid,sensitive=0
	    widget_control,state.rangeid,set_value=valrng
	  end

	end else begin

	  state.grvalsel.outbut = 1
	  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:1}

			;**** Sensitise graph title ****

	  widget_control,state.tibasid,/sensitive

			;**** Sensitise selection list ****

	  if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

			;**** Sensitise device selection list ****

	  if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive

			;**** Sensitise filename and restore settings ****

	  widget_control,state.fbaseid,/sensitive
	  widget_control,state.fileid,get_value=outfval
	  outfval.outbut = state.grvalsel.hrdout
	  widget_control,state.fileid,set_value=outfval

			;**** Sensitise scaling and restore settings ****

	  if state.scbasid gt 0 then begin
	    widget_control,state.scbasid,/sensitive
	    widget_control,state.rangeid,get_value=valrng
	    valrng.scalbut = state.grvalsel.scalbut
	    widget_control,state.rangeid,set_value=valrng
	  end

	end
    end

    state.listid: state.grvalsel.grpsel = event.index

    state.devid: state.grvalsel.devsel = event.index

    ELSE:

  ENDCASE

		;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_308_gr_sel, parent, SIGN=sign, OUTPUT=output, GRPLIST=grplist, $
			LISTTITLE=listtitle, DEVLIST=devlist, 		$
			VALUE=value, FONT=font, UVALUE=uvalue



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_308_gr_sel'

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(sign)) THEN sign = 0
  IF NOT (KEYWORD_SET(output)) THEN output = 'Graphical Output'
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(listtitle)) THEN listtitle = 'Select Graph'
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	  grvalsel = {grvalsel, 		$
			OUTBUT:0 , GTIT1:'', 	$
			SCALBUT:0, 		$
			XMINA:'', XMAXA:'', 	$
			YMINA:'', YMAXA:'', 	$
			XMINB:'', XMAXB:'',	$
                        YMINB:'', YMAXB:'',     $
			SIGN:sign, 	  	$	
			HRDOUT:0, HARDNAME:'', 	$
			GRPDEF:'', GRPFMESS:'', $
			GRPSEL:-1, GRPRMESS:'', $
			DEVSEL:-1, GRSELMESS:'' $
		     }
	end else begin
	  grvalsel = {grvalsel, 					$
			OUTBUT:value.outbut, GTIT1:value.gtit1, 	$
			SCALBUT:value.scalbut, 				$
			XMINA:value.xmina, XMAXA:value.xmaxa, 		$
			YMINA:value.ymina, YMAXA:value.ymaxa, 		$
			XMINB:value.xminb, XMAXB:value.xmaxb,		$
                        YMINB:value.yminb, YMAXB:value.ymaxb, 		$
			SIGN:sign,		  			$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess }
	end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****
  main = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		EVENT_FUNC = "gr_308_event", 				$
		FUNC_GET_VALUE = "gr_308_get_val", 			$
		PRO_SET_VALUE = "gr_308_set_val", 			$
		/ROW)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

  first_child = widget_base(main)

  cwid = widget_base(first_child,/row)

		;**** Left hand base ****

  lfid = widget_base(cwid,/column)

		;**** Right hand base ****

  rgid = widget_base(cwid,/column)

		;**** Create button to activate graphing ****

  base = widget_base(lfid,/column,/nonexclusive)
  actid = widget_button(base,value=output,font=font)

		;**** Graph title ****

  tibasid = widget_base(lfid,/row)
  rc = widget_label(tibasid,value='Graph Title',font=font)
  titleid = widget_text(tibasid,value=grvalsel.gtit1, 			$
			/editable,xsize=30,font=font)

		;**** Scaling and ranges widget ****

  if grvalsel.scalbut ge 0 then begin
    scbasid = widget_base(lfid,/column,/frame)
    valrng = {  SCALBUT:grvalsel.scalbut, 				$
		XMINA:grvalsel.xmina, XMAXA:grvalsel.xmaxa, 		$
		YMINA:grvalsel.ymina, YMAXA:grvalsel.ymaxa, 		$
		XMINB:grvalsel.xminb, XMAXB:grvalsel.xmaxb, 		$
                YMINB:grvalsel.yminb, YMAXB:grvalsel.ymaxb, 		$
		GRPRMESS:grvalsel.grprmess , SIGN:sign}
    rangeid = cw_308_ranges(scbasid, SIGN=sign, VALUE=valrng, FONT=font) 
  end else begin
    scbasid = 0L
  end


		;**** Output file name widget ****

  if grvalsel.hrdout ge 0 then begin
    outfval = { OUTBUT:grvalsel.hrdout, APPBUT:-1, REPBUT:0, 		$
                FILENAME:grvalsel.hardname, DEFNAME:grvalsel.grpdef, 	$
	        MESSAGE:grvalsel.grpfmess }
    fbaseid = widget_base(lfid,/row,/frame)
    fileid = cw_adas_outfile(fbaseid, OUTPUT='Enable Hard Copy', 	$
                              VALUE=outfval, FONT=font)
  end else begin
    fbaseid = 0L
    fileid = 0L
  end


		;**** Add selection list if required ****

  if strtrim(grplist(0)) ne '' then begin
    lsbasid = widget_base(rgid,/column)
    listid = cw_single_sel(lsbasid,grplist,title=listtitle, 		$
				value=grvalsel.grpsel, font=font)
  end else begin
    grvalsel.grpsel = -1
    lsbasid = 0L
    listid = 0L
  end

		;**** Add hardcopy device selection list ****

  if grvalsel.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
    devbasid = widget_base(rgid,/column)
    devid = cw_single_sel(devbasid,devlist,title='Select Device', 	$
				value=grvalsel.devsel, font=font)
  end else begin
    grvalsel.devsel = -1
    devbasid = 0L
    devid = 0L
  end

		;**** Add message widget ****

  if strtrim(grvalsel.grselmess) ne '' then begin
    message = grvalsel.grselmess
  end else begin
    message = ' '
  end
  messid = widget_label(lfid,value=message,font=font)

		;**** Set initial state according to value ****

  if grvalsel.outbut eq 1 then begin

    widget_control,actid,set_button=1

  end else begin

    widget_control,tibasid,sensitive=0

    if scbasid gt 0 then begin
      widget_control,scbasid,sensitive=0
      valrng.scalbut = 0
      widget_control,rangeid,set_value=valrng
    end

    widget_control,fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,fileid,set_value=outfval

    if listid gt 0 then widget_control,lsbasid,sensitive=0

    if devbasid gt 0 then widget_control,devbasid,sensitive=0

  end


		;**** Create state structure ****

  new_state = { ACTID:actid, 						$
		TIBASID:tibasid, TITLEID:titleid, 			$
		SCBASID:scbasid, RANGEID:rangeid, 			$
		FBASEID:fbaseid, FILEID:fileid, 			$
		LSBASID:lsbasid, LISTID:listid, 			$
		DEVBASID:devbasid, DEVID:devid, MESSID:messid, 		$
		GRVALSEL:grvalsel, FONT:font }

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main

END
