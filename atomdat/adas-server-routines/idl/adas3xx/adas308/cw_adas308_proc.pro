; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/cw_adas308_proc.pro,v 1.8 2004/07/06 12:39:53 whitefor Exp $ Date $Date: 2004/07/06 12:39:53 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS308_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS308 processing options/input.
;
; EXPLANATION:
;	This function creates a complicated compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget ,
;	   an information widget detailing the receiver and donor ions,
;	   a text widget holding atomic mass number of receiver,
;	   text widgets holding the plasma parameter values,
;	   three table widgets:
;		- beam parameter information,
;		- observed spectrum lines,
;		- required emissivity predictions,
;	   a selector widget for the charge exchange theory,
;	   a selector widget for the donor state(if eikonal model 
;				selected for above),
;	   a selector widget for the emission measure model,
;	   a message widget, and a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS308, see adas308_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;       ntot    - (Int) Maximum allowed N quantum number.
;
;       ngrnd   - (Int) Minimum allowed N quantum number.
;
;       mxtab   - (Int) Maximum number of emissivity tables allowed.
;
;       mxgrf   - (Int) Maximum number of emissivity graphs allowed.
;	
;	Most of the inputs map exactly onto variables of the same
;	name in the ADAS308 FORTRAN program.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'menu','done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;		bmarr = fltarr(6)
;		osarr = intarr(10)
;		osfarr = fltarr(10)
;		rparr = intarr(20)
;     		ps = {proc308_set, $
;			new   : 0,		$
;                	title : '',             $
;                       ramsno: 0.0,            $
;                       tiev  : 0.0 ,           $
;                       tev   : 0.0 ,           $
;                       densz : 0.0 ,           $
;                       dens  : 0.0 ,           $
;                       zeff  : 0.0 ,           $
;                       bmag  : 0.0 ,           $
;			itable: 0,		$
;			nbeam:  0,		$
;		  	bmfra: bmarr,		$
;			bmena: bmarr,		$
;			noline: 0,		$
;			nu: osarr,		$
;			nl: osarr,		$
;			emisa: osfarr,		$
;			npu: rparr,		$
;			npl: rparr,		$
;			npkey: rparr,		$
;			npline:0,		$
;			lrttb:0,		$
;			ibstat: 0,		$
;			iemms:  0,		$
;			itheor: 0		$
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;
;		RAMSNO  (Real) atomic mass number of receiver ion
;
;		TIEV    (Real) Plasma ion temp. in eV
;
;		TEV	(Real) Plasma electron temp. in eV
;
;		DENSZ	(Real) Plasma ion density in cm-3
;
;		DENS    (Real) Plasma electron density in cm-3
;
;		ZEFF	(Real) Effective Z
;
;		BMAG	(Real) Magnetic field B (T)
;
;		ITABLE	(Int) Flag indicating which table is active:
;			0 - Beam parameter information
;			1 - Observed spectrum lines
;			2 - Required emissivity predictions
;
;		NBEAM	(Int) Number of beam energies entered in table 0
;
;		BMFRA	(Fltarr) Fractions of total beam from table 0
;			Dimension = NBEAM
;
;		BMENA	(Fltarr) Beam energies corresponding to above fractions
; 			Dimension = NBEAM
;
;               NOLINE	(Int) Number of lines entered in table 1
;
;		NU	(Intarr) Upper principal quantum numbers of lines
;			Dimension = NOLINE
;
;		NL	(Intarr) Lower principal quantum numbers of lines
;                       Dimension = NOLINE
;
;               EMISA 	(Fltarr) Column emissivities of lines
;			Dimension = NOLINE
;
;		NPLINE	(Int) Number of required predictions in table 2
;
;               NPU	(Intarr) Upper principal quantum numbers of 
;			required predictions. Dimension = NPLINE
;
;		NPL	(Intarr) Lower principal quantum numbers of
;                       required predictions. Dimension = NPLINE
;
;               NPKEY	(Intarr) Flags showing what sort of input is required
;			for each predicted emissivity.
;			Dimension = NPLINE
;			0 = Summary only
;			1 = Graph and table
;			2 = Table only
;
;		LRTTB	(Int) Flag indicating whether rate table printing
;			is required (1) or not (0).
;
;		ITHEOR	(Int) Flag indicating charge exchange theory to 
;			be used.
;			0 = Use input data set
;			1 = Use Eikonal model
;
;		IBSTAT	(Int) Flag indicating which donor state is required
;			for Eikonal model (see above).
;			0 = H(1S)
;			1 = H(2S)
;			2 = H(2P)
;			3 = He(1s2)
;			4 = He(1s2s)
;
;		IEMMS	(Int) Flag indicating which emission model is req'd.
;			0 = Charge exchange
;			1 = Electron impact excitation
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS308 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(e10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE   Adas data table widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC308_GET_VAL()	Returns the current PROCVAL structure.
;	PROC308_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 06-Jun-1995 
;
; MODIFIED:
;	1.1	Tim Hammond  	 		
;		First version
;	1.2	Tim Hammond	
;		Tidied code up and removed calls to 'adas308' tables
;               and replaced with calls to the updated normal table
;               procedures.
;	1.3	Tim Hammond			
;		Put in extra error checking for when 308 is run for
;               the first time.
;	1.4	Tim Hammond
;               Modified use of fonts and sizes of tables for use on
;               different platforms.
;	1.5	Tim Hammond
;		Added code for 'return to menu' button
;	1.6	Tim Hammond
;		Ensured that a default font is always given to prevent
;		very different fonts appearing on machines with very
;		small screens.
;	1.7	Richard Martin
;		Added 'strtim' command when obtaining prediction data from
;		table.
;	1.8	Martin O'Mullane
;		Added check for lines not in increaseing order.
;
; VERSION:
;	1.1 	06-06-95
;	1.2	20-06-95
;	1.3	14-07-95
;	1.4	27-02-96
;	1.5	21-06-96
;	1.6	06-08-96
;	1.7	14-04-98
;	1.8	04-05-2000
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc308_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control,state.runid,get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

		;***********************************************
		;**** Get value of atomic mass of receiver   ***
		;***********************************************

    widget_control, state.amtext, get_value=massno
    if (num_chk(massno(0)) eq 0) then begin
        ramsno = float(massno(0))
    endif else begin
        ramsno = -99.0
    endelse
    widget_control, state.amtext, set_value=massno

                ;***********************************************
                ;**** Get value of ion temperature           ***
                ;***********************************************

    widget_control, state.ittext, get_value=iontemp
    if (num_chk(iontemp(0)) eq 0) then begin
        tiev = float(iontemp(0))
    endif else begin
        tiev = -99.0
    endelse
    widget_control, state.ittext, set_value=iontemp

                ;***********************************************
                ;**** Get value of electron temperature      ***
                ;***********************************************

    widget_control, state.ettext, get_value=eletemp
    if (num_chk(eletemp(0)) eq 0) then begin
        tev = float(eletemp(0))
    endif else begin
        tev = -99.0
    endelse
    widget_control, state.ettext, set_value=eletemp
    
                ;***********************************************
                ;**** Get value of ion density               ***
                ;***********************************************

    widget_control, state.idtext, get_value=iondens
    if (num_chk(iondens(0)) eq 0) then begin
        densz = float(iondens(0))
    endif else begin
        densz = -99.0
    endelse
    widget_control, state.idtext, set_value=iondens

                ;***********************************************
                ;**** Get value of electron density          ***
                ;***********************************************

    widget_control, state.edtext, get_value=eledens
    if (num_chk(eledens(0)) eq 0) then begin
        dens = float(eledens(0))
    endif else begin
        dens = -99.0
    endelse
    widget_control, state.edtext, set_value=eledens

                ;***********************************************
                ;**** Get value of Z effective               ***
                ;***********************************************

    widget_control, state.zftext, get_value=zefval 
    if (num_chk(zefval(0)) eq 0) then begin
        zeff = float(zefval(0))
    endif else begin
        zeff = -99.0
    endelse
    widget_control, state.zftext, set_value=zefval 

                ;***********************************************
                ;**** Get value of B magnetic                ***
                ;***********************************************

    widget_control, state.bmtext, get_value=bmaval 
    if (num_chk(bmaval(0)) eq 0) then begin
        bmag = float(bmaval(0))
    endif else begin
        bmag = -99.0
    endelse
    widget_control, state.bmtext, set_value=bmaval 

                ;***********************************************
                ;**** Get values of cex theory/emis model    ***
                ;***********************************************

    widget_control, state.cexther, get_value=theory
    itheor = theory
    widget_control, state.cexstat, get_value=donor
    ibstat = donor
    widget_control, state.emimod, get_value=model
    iemms = model

    widget_control, state.ratestat, get_value=lrttb

                ;***********************************************
                ;**** Get values related to tables           ***
                ;***********************************************

    widget_control, state.tabmenu, get_value = tableval
    itable = tableval

                ;***********************************************
                ;**** Get beam parameter values              ***
                ;***********************************************

    widget_control, state.bptabid, get_value = bpvalues
    list = where( strlen( bpvalues.value(0,*)), nbeam)
    nbeam = fix(nbeam)
    bmfra = fltarr(6)
    bmena = fltarr(6)
    if (nbeam ne 0) then begin
        bmfra(0:nbeam-1) = float( bpvalues.value(0,0:nbeam-1))
        bmena(0:nbeam-1) = float( bpvalues.value(1,0:nbeam-1))
    endif

                ;***********************************************
                ;**** Get observed spectrum line values      ***
                ;***********************************************

    widget_control, state.ostabid, get_value = osvalues
    list = where( strlen( osvalues.value(0,*)), noline)
    noline = fix(noline)
    nu = intarr(10)
    nl = intarr(10)
    emisa = fltarr(10)
    if (noline ne 0) then begin
        nu (0:noline-1) = fix( osvalues.value(0,0:noline-1))
        nl (0:noline-1) = fix( osvalues.value(1,0:noline-1))
        emisa (0:noline-1) = float( osvalues.value(2,0:noline-1))
    endif

                ;***********************************************
                ;**** Get required predictions information   ***
                ;***********************************************
  
    widget_control, state.rptabid, get_value = rpvalues
    list = where( strlen( strtrim(rpvalues.value(0,*),2)), npline)
    npline = fix(npline)
    npu = intarr(20)
    npl = intarr(20)
    npkey = intarr(20)
    if (npline ne 0) then begin
        npuwhere = where(strtrim(rpvalues.value(0,0:npline-1),2) ne '')
        npu (npuwhere) = fix( rpvalues.value(0, npuwhere))
        nplwhere = where(strtrim(rpvalues.value(1,0:npline-1),2) ne '')
        npl(nplwhere) = fix ( rpvalues.value(1,nplwhere))
        npkey (0:npline-1) = 0
        npkeywhere = where(strtrim(rpvalues.value(2,0:npline-1),2) ne '')
        npkey (npkeywhere) = fix( rpvalues.value(2,npkeywhere))
    endif

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 			$
	        new   : 0, 				$
                title : title,          		$
                ramsno: ramsno, 	         	$
                tiev  : tiev  ,        		 	$
                tev   : tev   ,         		$
                densz : densz ,         		$
                dens  : dens  ,         		$
                zeff  : zeff  ,         		$
                bmag  : bmag  ,         		$
		nbeam : nbeam ,				$
		bmfra : bmfra,				$
		bmena : bmena,				$
		noline:noline,				$
		nu    : nu,				$
		nl    : nl,				$
		emisa : emisa,				$
		npu   : npu,  				$
		npl   : npl,  				$
		npkey : npkey,				$
		npline: npline,				$
		lrttb : lrttb,				$
		itable: itable,				$
		ibstat: ibstat,				$
		iemms : iemms,				$
		itheor: itheor		 		$
              }

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc308_event, event

    COMMON blk_308, ntot, ngrnd, mxtab, mxgrf

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

    state.tabmenu: begin
        widget_control, state.tabmenu, get_value=itable
        CASE itable OF
            0: begin
                widget_control, state.bpbase, map=1
                widget_control, state.osbase, map=0
                widget_control, state.rpbase, map=0
            end
	    1: begin
                widget_control, state.bpbase, map=0
                widget_control, state.osbase, map=1
                widget_control, state.rpbase, map=0
	    end
	    2: begin
                widget_control, state.bpbase, map=0
                widget_control, state.osbase, map=0
                widget_control, state.rpbase, map=1
	    end
        ENDCASE
    end

    state.cexther: begin
        widget_control, state.cexther, get_value=sensi
        widget_control, state.cexstat, sensitive=sensi
        widget_control, state.cexlab2, sensitive=sensi
    end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

    state.amtext: widget_control, state.ittext,/input_focus

    state.ittext: widget_control, state.ettext,/input_focus

    state.ettext: widget_control, state.idtext,/input_focus

    state.idtext: widget_control, state.edtext,/input_focus

    state.edtext: widget_control, state.zftext,/input_focus

    state.zftext: widget_control, state.bmtext,/input_focus

    state.bmtext: widget_control, state.amtext,/input_focus

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc308_get_val so it can be used ***
		;*** there.				  ***
		;********************************************

	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

                ;***********************************************
		;**** Check to see if sensible mass entered ****
                ;***********************************************

	if (error eq 0 and (ps.ramsno le 0.0 or 		$
        ps.ramsno gt 300.0)) then begin
            error = 1
            message='Error: Invalid atomic mass number for receiver'
        endif

                ;***********************************************
		;**** Check to see if sensible ion temp     ****
                ;***********************************************

        if (error eq 0 and (ps.tiev le 0.0) ) then begin
            error = 1
            message='Error: Invalid ion temperature'
        endif

                ;***********************************************
                ;**** Check to see if sensible ion density  ****
                ;***********************************************

        if (error eq 0 and (ps.densz le 0.0) ) then begin
            error = 1
            message='Error: Invalid ion density'
        endif

                ;***********************************************
                ;**** Check to see if sensible elec temp    ****
                ;***********************************************

        if (error eq 0 and (ps.tev le 0.0) ) then begin
            error = 1
            message='Error: Invalid electron temperature'
        endif

                ;***********************************************
                ;**** Check to see if sensible elec density ****
                ;***********************************************

        if (error eq 0 and (ps.dens le 0.0) ) then begin
            error = 1
            message='Error: Invalid electron density'
        endif

                ;***********************************************
                ;**** Check to see if sensible  Z effective ****
                ;***********************************************

        if (error eq 0 and (ps.zeff le 0.0) ) then begin
            error = 1
            message='Error: Invalid Z effective'
        endif

                ;***********************************************
                ;**** Check to see if sensible B Magnetic   ****
                ;***********************************************

        if (error eq 0 and (ps.bmag le 0.0) ) then begin
            error = 1
            message='Error: Invalid B magnetic'
        endif

                ;***********************************************
                ;**** Check for sensible beam parameters    ****
                ;***********************************************

	if (error eq 0 and (ps.nbeam eq 0)) then begin
	    error =1 
	    message='Error: No beam parameter information entered'
        endif
        beamtest = where (ps.bmfra gt 1.0)
        if (error eq 0 and (beamtest(0) ne -1)) then begin
	    error = 1
	    message='Error: Beam component fraction greater than 1'
        endif

                ;***********************************************
                ;**** Check for sensible spectrum lines     ****
                ;***********************************************

        if (error eq 0 and (ps.noline eq 0)) then begin
            error = 1
	    message='Error: No data for the observed spectrum lines'
	endif
        if (ps.noline ne 0) then begin
            linetest1 = where (ps.nu(0:ps.noline-1) gt ntot)
            linetest2 = where (ps.nl(0:ps.noline-1) lt ngrnd)
            linetest3 = where (ps.nl gt ps.nu) 
            linetest4 = where (ps.nl(0:ps.noline-1) eq ps.nu(0:ps.noline-1))
            if (ps.noline gt 1) then begin
                linetest5 = where (ps.nu(0:ps.noline-2) gt ps.nu(1:ps.noline-1))
            endif else linetest5 = -1
        endif
        if (error eq 0) then begin
	    if (error eq 0 and (linetest1(0) ne -1)) then begin
	        error = 1
	        message='Error: Spectrum lines: upper level N greater than maximum'
            endif
            if (error eq 0 and (linetest2(0) ne -1)) then begin
	        error = 1
 	        message='Error: Spectrum lines: lower level N less than minimum'
	    endif
            if (error eq 0 and (linetest3(0) ne -1)) then begin
                error = 1
                message='Error: Spectrum lines: upper level N below lower level N'
            endif
            if (error eq 0 and (linetest4(0) ne -1)) then begin
                error = 1
                message = 'Error: Spectrum lines: upper level N and lower level N are the same'
            endif
            if (error eq 0 and (ps.noline gt 1) and (linetest5(0) ne -1)) then begin
                error = 1
                message = 'Error: Observed spectrum lines: upper levels N  are not in increasing order'
            endif
	endif

                ;***********************************************
                ;**** Check for sensible reqd. predictions  ****
                ;***********************************************

	if (error eq 0 and (ps.npline eq 0)) then begin
            error = 1
            message='Error: No emissivity predictions specified'
	endif
	if (ps.npline ne 0) then begin
            linetest1 = where (ps.npu(0:ps.npline-1) gt ntot)
            linetest2 = where (ps.npl(0:ps.npline-1) lt ngrnd)
            linetest3 = where (ps.npl gt ps.npu)
            linetest4 = where (ps.npl(0:ps.npline-1) eq ps.npu(0:ps.npline-1))
            linetest5 = where (ps.npkey(0:ps.npline-1) ne 0)
            linetest6 = where (ps.npkey(0:ps.npline-1) eq 1)
        endif
	if error eq 0 then begin
            if (error eq 0 and (linetest1(0) ne -1)) then begin
                error = 1
                message='Error: Emissivity predictions: upper level N greater than maximum'
	    endif
            if (error eq 0 and (linetest2(0) ne -1)) then begin
                error = 1
                message='Error: Emissivity predictions: lower level N less than minimum'
	    endif
            if (error eq 0 and (linetest3(0) ne -1)) then begin
                error = 1
                message='Error: Emissivity predictions: upper level N below lower level N'
	    endif
            if (error eq 0 and (linetest4(0) ne -1)) then begin
                error = 1
                message = 'Error: Emissivity predictions:upper level N and'+$
                ' lower level N are the same'
	    endif
            if (error eq 0 and linetest5(0) ne -1) then begin
                linesize = size(linetest5)
                linesize2 = size(linetest6)
                if (linesize(3) gt 5 and error eq 0) then begin
                    error = 1
                    message='Error: Emissivity predictions: '+$
                    'too many graphs/tables requested'
                endif
                if(error eq 0 and (linesize2(n_elements(linesize2)-1) gt 2)) then begin
	    	    error = 1
		    message='Error: Emissivity predictions: '+$
                    'too many graphs requested'
	        endif
	    endif
        endif
	if (error eq 0 and max(ps.npkey) gt 2) then begin
            error = 1
            message = 'Error: Emissivity predictions: '+$
	    'Invalid key selection'
	endif
   

		;**** return value or flag error ****

	if error eq 0 then begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        endif else begin
	    widget_control, state.messid, set_value=message
	    new_event = 0L
        endelse
	

      end
                ;*********************
                ;**** Menu button ****
                ;*********************

      state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Menu'}
      end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas308_proc, topparent, dsfull, act, 		$
                symbd, idz0, symbr, irz0, irz1, irz2,           $
		ntot, ngrnd,mxtab, mxgrf, bitfile,		$
		procval=procval, uvalue=uvalue, 		$
		font_large=font_large, font_small=font_small,   $
		edit_fonts=edit_fonts, num_form=num_form

    COMMON blk_308, ntotcom, ngrndcom, mxtabcom, mxgrfcom
    ntotcom=ntot
    ngrndcom=ngrnd
    mxtabcom=mxtab
    mxgrfcom=mxgrf

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(e10.3)'
    if not (keyword_set(procval)) then begin
        bmarr = fltarr(6)
        osarr = intarr(10)
        os2arr=fltarr(10)
        rparr=intarr(20)
        ps = { 				$			 
		new   : 0, 		$
		title : '',             $
                ramsno: 0.0,            $
                tiev  : 0.0 ,           $
                tev   : 0.0 ,           $
                densz : 0.0 ,           $
                dens  : 0.0 ,           $
                zeff  : 0.0 ,           $
                bmag  : 0.0 ,           $
		nbeam : 0,		$
		bmfra : bmarr,		$
		bmena : bmarr,		$
		noline: 0,		$
		nu    : osarr,		$
		nl    : osarr,          $
                emisa : os2arr,		$
		npu   : rparr,		$
		npl   : rparr,		$
		npkey : rparr,		$
		npline: 0,		$
		lrttb : 0,		$
		itable: 0,		$
		ibstat: 0,		$
		iemms : 0,		$
		itheor: 0		$
              }
    endif else begin
	ps = { 				$ 
		new   : procval.new,    $
                title : procval.title,  $
		ramsno: procval.ramsno, $
		tiev  :	procval.tiev,	$
		tev   : procval.tev,	$
		densz : procval.densz,	$
		dens  : procval.dens,	$
		zeff  : procval.zeff,	$
		bmag  : procval.bmag,	$
		nbeam : procval.nbeam,	$
		bmfra : procval.bmfra,	$
		bmena : procval.bmena,	$
		noline: procval.noline,	$
		nu    : procval.nu,	$
		nl    : procval.nl,	$
		emisa : procval.emisa,	$
		npu   : procval.npu,	$
		npl   : procval.npl,	$
		npkey : procval.npkey,	$
		npline: procval.npline, $
		lrttb : procval.lrttb,	$
		itable: procval.itable,	$
		ibstat: procval.ibstat,	$
		iemms : procval.iemms,	$
		itheor: procval.itheor  $
	     }
    endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;********************************************************
		;**** Create the 308 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			title = 'ADAS308 PROCESSING OPTIONS',		$
			EVENT_FUNC = "proc308_event", 			$
			FUNC_GET_VALUE = "proc308_get_val", 		$
			/COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase,dsfull,font=large_font,/row)


 		;************************************************
 		;**** base for the 'Information' widgets     ****
	 	;************************************************

    infobase = widget_base(topbase, /row, /frame)
    infobase1 = widget_base(infobase, /column, /frame)
    label1   = widget_label(infobase1, font=font_small,			$
    value = "------------- Receiver --------------       - Neutral donor -   ")
    label2   = widget_label(infobase1, font=font_small,			$
    value = "        Nuclear   Initial      Final                  Nuclear   ")
    label3   = widget_label(infobase1, font=font_small,			$
    value = "Symbol  charge   ion charge  ion charge     Symbol    charge    ")
    label4   = widget_label(infobase1, font=font_small,			$
    value = "-------------------------------------------------------------   ")
    label5val = "                                                               "
    strput, label5val, symbr, 1
    strput, label5val, (strtrim(string(irz0),2)),10
    strput, label5val, (strtrim(string(irz1),2)),20
    strput, label5val, (strtrim(string(irz2),2)),32
    strput, label5val, (strtrim(string(idz0),2)),56
    strput, label5val, symbd, 46
    label5 = widget_label(infobase1, font=font_small, value=label5val)

                ;************************************************
                ;**** base for atomic mass no.   widgets     ****
                ;************************************************

    ambase = widget_base(infobase, /column)
    amdum = widget_label(ambase, font=large_font, value=" ")
    amlabel1 = widget_label(ambase, font=large_font,			$
    value=" Please input following receiver information:-")
    ambase2 = widget_base(ambase, /row)
    amlabel2 = widget_label(ambase2, font=large_font,                   $
    value = "    Atomic mass number of receiver ")
    if (procval.ramsno ne 0.0) then begin
        amval = string (procval.ramsno, format='(f5.1)')
    endif else begin
        amval = "  "
    endelse
    amtext = widget_text(ambase2, xsize=5, font=large_font,		$
    value = amval, /editable)

                ;************************************************
                ;**** Another base to hold the next two      ****
                ;************************************************

    bigbase = widget_base(topbase, /row)

                ;************************************************
                ;**** Base for the 3 input tables            ****
                ;************************************************

    tabbase = widget_base(bigbase, /frame, /column)

                ;************************************************
                ;**** Base for table headings - always there ****
                ;************************************************

    headbase = widget_base(tabbase, /frame, /column)
    heading = widget_label(headbase, font=large_font,			$
    value = "Input beam and spectrum line information:- ")
    tabmenu = cw_bgroup(headbase, /column, /exclusive, font=font_small, $
    set_value = procval.itable, ['Beam parameter information',		$
    'Observed spectrum lines','Required emissivity predictions'])

                ;************************************************
                ;**** Bases for the 3 input tables - these   ****
		;**** are mapped and unmapped according to   ****
		;**** the value of procval.itable            ****
                ;************************************************

    tabbase2 = widget_base(tabbase, /frame)

                ;************************************************
                ;**** Base for the beam parameter information****
                ;************************************************

    if (procval.itable eq 0) then begin
        bpmap = 1
    endif else begin
        bpmap = 0
    endelse
    bpbase = widget_base(tabbase2, /column, map=bpmap)
    tabbp = strarr(2,6)
    if (procval.nbeam ne 0) then begin
        format_test = where(procval.bmfra(0:procval.nbeam-1) eq 1.0)
        tabbp(0,0:procval.nbeam-1)=strtrim(string(procval.bmfra(0:procval.nbeam-1),$
        format='(f5.3)'),2)
        if(format_test(0) ne -1) then begin
            tabbp(0,format_test)="   "+strtrim(string(procval.bmfra(format_test),$
            format='(i3)'),2)
        endif
        tabbp(1,0:procval.nbeam-1)=strtrim(string(procval.bmena(0:procval.nbeam-1),$
        format=num_form),2)
    endif

    bptabid = cw_adas_table( bpbase, tabbp, coledit=[1,1],		$
              colhead=['Fraction','Energy (eV/amu)'], /scroll,		$
              title='Beam parameter information',limits=[2,2],		$
              /rowskip, font_small=font_small, 				$
              fltint=['(f5.3)','(e10.3)'], ytexsize=y_size ,		$
	      font_large=large_font)

    bptablab = widget_label(bpbase, font=font_small,			$
    value="Note: It is left to the user to ensure the ")
    bptablab2 = widget_label(bpbase, font=font_small,                   $
    value="      sum of the fractions is correct.     ")
    bptabspace = widget_label(bpbase, value = ' ')
    

                ;************************************************
                ;**** Base for the observed spectrum lines   ****
                ;************************************************

    if (procval.itable eq 1) then begin
        osmap = 1
    endif else begin
        osmap = 0
    endelse
    osbase = widget_base(tabbase2, /column, map=osmap)
    tabos = strarr(3,10)
    if (procval.noline ne 0) then begin
    tabos(0,0:procval.noline-1)="   "+strtrim(string(procval.nu(0:procval.noline-1),$
        format='(i3)'),2)
    tabos(1,0:procval.noline-1)="   "+strtrim(string(procval.nl(0:procval.noline-1),$
        format='(i3)'),2)
    tabos(2,0:procval.noline-1)=strtrim(string(procval.emisa(0:procval.noline-1),$
        format=num_form),2)
    endif

    ostabid = cw_adas_table( osbase, tabos, coledit=[1,1,1],		$
              colhead=[['Upper','Lower','Column emissivity'],		$
              ['level N','level N','(ph. cm-2 s-1)']], /scroll,		$
              title='Observed spectrum lines',limits=[2,2,2],		$
	      fltint=['(i4)','(i4)','(e10.3)'],				$
              /rowskip, font_small=font_small, ytexsize=y_size,		$
	      font_large=large_font)

    ostablab = widget_label(osbase, font=font_small,			$
    value="Note: maximum allowed N quantum no. :"+string(ntot))
    ostablab2 = widget_label(osbase, font=font_small,                   $
    value= "      minimum allowed N quantum no. :"+string(ngrnd))
    

                ;************************************************
                ;**** Base for the required predictions      ****
                ;************************************************

    if (procval.itable eq 2) then begin
        rpmap = 1
    endif else begin
        rpmap = 0
    endelse
    rpbase = widget_base(tabbase2, /column, map=rpmap)
    tabrp = strarr(3,20)
    if (procval.npline ne 0) then begin
    tabrp(0,0:procval.npline-1)="   "+strtrim(string(procval.npu(0:procval.npline-1),$
        format='(i3)'),2)
    tabrp(1,0:procval.npline-1)="   "+strtrim(string(procval.npl(0:procval.npline-1),$
        format='(i3)'),2)

                ;****************************************************
		;**** Tidy up alignment for columns where values ****
		;**** are greater than 9 			 ****
		;****************************************************

    npltidy = where(procval.npl(0:procval.npline-1) gt 9)
    nputidy = where(procval.npu(0:procval.npline-1) gt 9)
    if (npltidy(0) ne -1) then begin
        tabrp(1,npltidy)=strmid(tabrp(1,npltidy),1,4)
    endif
    if (nputidy(0) ne -1) then begin
        tabrp(0,nputidy)=strmid(tabrp(0,nputidy),1,4)
    endif
    tabrp(2,0:procval.npline-1)=" "+strtrim(string(procval.npkey(0:procval.npline-1),$
        format='(i3)'),2)
    zerochk = where(procval.npkey(0:procval.npline-1) eq 0)
    if (zerochk(0) ne -1) then begin
        tabrp(2,zerochk) = ' '
    endif
    endif

    rptabid = cw_adas_table( rpbase, tabrp, coledit=[1,1,1],		$
              colhead=[['Upper','Lower','   '],				$
              ['level N','level N','Key']],/scroll,			$
	      title='Required emissivity predictions',limits=[2,2,1],	$
	      fltint=['(i4)','(i4)','(i2)'],  				$
              /rowskip, font_small=font_small, /difflen, /gaps,		$
              ytexsize=y_size, font_large=large_font)

    rptablab = widget_label(rpbase, font=font_small,                    $
    value="Note: maximum allowed N quantum no. :"+string(ntot))
    rptablab2 = widget_label(rpbase, font=font_small,                   $
    value= "      minimum allowed N quantum no. :"+string(ngrnd))
    rptablab6 = widget_label(rpbase, font=font_small,                   $
    value="      ")
    rptablab3 = widget_label(rpbase, font=font_small,			$
    value="Key: 1 = Graphical and tabular output (max. "+strtrim(mxgrf,2)+$
    ")         ")
    rptablab4 = widget_label(rpbase, font=font_small,                   $
    value="     2 = Tabular output only (max. "+strtrim(mxtab,2)+	$
    " non-blank entries)")
    rptablab5 = widget_label(rpbase, font=font_small,                   $
    value=" Blank = Summary only                                  ")



                ;************************************************
                ;**** Base for last two bases                ****
                ;************************************************

    rightbase = widget_base(bigbase, /column)

                ;************************************************
                ;**** Base for plasma parameters             ****
                ;************************************************

    plasbase = widget_base(rightbase, /column, /frame)
    plastit = widget_label(plasbase, font=large_font,                    $
    value = "Input plasma parameter information:-")

                ;************************************************
                ;**** Base for ion/electron temperatures     ****
                ;************************************************

    plasbase1 = widget_base(plasbase, /row)
    itlab = widget_label(plasbase1, font=font_small,                     $
    value = "Ion temp. (eV)   :")
    if (procval.tiev ne 0.0) then begin
        itval = string (procval.tiev, format='(e7.1)')
    endif else begin
        itval = "       "
    endelse
    ittext = widget_text(plasbase1, xsize=7, /editable, value = itval,	$
	                 font=large_font)
    etlab = widget_label(plasbase1, font=font_small,			$
    value = "Elec temp. (eV)  :")
    if (procval.tev ne 0.0) then begin
        etval = string (procval.tev, format='(e7.1)')
    endif else begin
        etval = "       "
    endelse
    ettext = widget_text(plasbase1, xsize=7, /editable, value = etval,	$
	                 font=large_font)

                ;************************************************
                ;**** Base for ion/electron densities        ****
                ;************************************************

    plasbase2 = widget_base(plasbase, /row)
    idlab = widget_label(plasbase2, font=font_small,                     $
    value = "Ion dens. (cm-3) :")
    if (procval.densz ne 0.0) then begin
        idval = string (procval.densz, format='(e7.1)')
    endif else begin
        idval = "       "
    endelse
    idtext = widget_text(plasbase2, xsize=7, /editable, value = idval,	$
	                 font=large_font)
    edlab = widget_label(plasbase2, font=font_small,                    $
    value = "Elec dens. (cm-3):")
    if (procval.dens ne 0.0) then begin
        edval = string (procval.dens, format='(e7.1)')
    endif else begin
        edval = "       "
    endelse
    edtext = widget_text(plasbase2, xsize=7, /editable, value = edval,	$
	                 font=large_font)

                ;************************************************
                ;**** Base for Zeff/B Magn                   ****
                ;************************************************

    plasbase3 = widget_base(plasbase, /row)
    zflab = widget_label(plasbase3, font=font_small,                     $
    value = "Z effective      :")
    if (procval.zeff ne 0.0) then begin
        zfval = string (procval.zeff, format='(d7.2)')
    endif else begin
        zfval = "       "
    endelse
    zftext = widget_text(plasbase3, xsize=7, /editable, value = zfval,	$
	                 font=large_font)
    bmlab = widget_label(plasbase3, font=font_small,                    $
    value = "B Magn. (T)      :")
    if (procval.bmag ne 0.0) then begin
        bmval = string (procval.bmag, format='(d7.2)')
    endif else begin
        bmval = "       "
    endelse
    bmtext = widget_text(plasbase3, xsize=7, /editable, value = bmval,	$
	                 font=large_font)

                ;************************************************
                ;**** Base for charge exchange theory        ****
                ;************************************************

    cexbase = widget_base(rightbase, /column, /frame)
    cexbase1 = widget_base(cexbase, /row)
    cexlab1 = widget_label(cexbase1, font=large_font,			$
    value = "Select charge exchange theory : ")
    cexther = cw_bselector(cexbase1, ['Use input data set',		$
    'Use Eikonal model'], set_value = procval.itheor, font=large_fonT)
    cexbase2 = widget_base(cexbase, /row)
    cexlab2 = widget_label(cexbase2, font=large_font,			$
    value = "Select donor state            : ")
    cexstat = cw_bselector(cexbase2, [' H  (1S)',' H  (2S)',		$
    ' H  (2P)',' HE  (1S2)',' HE  (1S2S)'], set_value = procval.ibstat)
    emibase = widget_base(cexbase, /row)
    emilab = widget_label(emibase, font=large_font,			$
    value = "Select emission measure model : ")
    emimod = cw_bselector(emibase, ['Charge exchange             ',	$
    'Electron impact excitation'], set_value = procval.iemms, 		$
    font=large_font)
    widget_control, cexstat, sensitive=procval.itheor
    widget_control, cexlab2, sensitive=procval.itheor

                ;************************************************
                ;**** Base for charge exchange theory        ****
                ;************************************************
    ratebase = widget_base(rightbase, /row,/frame)
    ratelabel = widget_label(ratebase, font=large_font,			$
    value = "Is rate table printing required? ")
    ratestat = cw_bselector(ratebase, ['No','Yes'],			$
    set_value = procval.lrttb, font=large_font)


		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base,value='Cancel',font=large_font)
    doneid = widget_button(base,value='Done',font=large_font)
    base = widget_base(parent,/row)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid:runid,  		$
		  messid:messid, 		$
		  outid:outid,			$
		  cancelid:cancelid,		$
		  doneid:doneid, 		$
		  dsfull:dsfull,		$
		  font:font_large,		$
		  amtext:amtext,		$
                  ittext:ittext,		$
		  ettext:ettext,		$
		  idtext:idtext,		$
		  edtext:edtext,		$
                  zftext:zftext,		$
		  bmtext:bmtext,		$
  		  cexther:cexther,		$
                  cexstat:cexstat,		$
		  cexlab2:cexlab2,		$
		  emimod:emimod,		$
		  tabmenu:tabmenu,		$
		  bpbase:bpbase,		$
		  osbase:osbase,		$
		  rpbase:rpbase,		$
                  bptabid:bptabid,		$
  		  ostabid:ostabid,		$
		  rptabid:rptabid,		$
		  ratestat:ratestat,		$
		  num_form:num_form 		$
	        }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END

