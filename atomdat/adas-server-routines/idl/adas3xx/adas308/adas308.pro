; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/adas308.pro,v 1.12 2004/07/06 10:32:06 whitefor Exp $ Date $Date: 2004/07/06 10:32:06 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS308
;
; PURPOSE:
;	The highest level routine for the ADAS 308 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 308 application.  Associated with adas308.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas308.pro is called to start the
;	ADAS 308 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas308,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       C8SPF0          Pipe comms with FORTRAN C8SPF0 routine.
;       CXSETP          Pipe comms with FORTRAN C8SETP routine.
;       C8ISPF          Pipe comms with FORTRAN C8ISPF routine.
;       C8SPF1          Pipe comms with FORTRAN C8SPF1 routine.
;	C8WSOL		Pipe comms with FORTRAN C8WSOL routine
;			Also solves simultaneous equations.
;	C8EMQX          Pipe comms with FORTRAN C8EMQX routine
;                       Also solves simultaneous equations.
;       C8OUTG          Pipe comms with FORTRAN C8OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 05/06/1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First version
;	1.2	Tim Hammond	
;		Tidied code up
;	1.3	Tim Hammond		
;		Increased version number to 1.3
;	1.4	Tim Hammond
;		Added flexible font use for output options widget.
;	1.5	Tim Hammond
;		Increased version number to 1.4
;	1.6	Tim Hammond
;		Added sections for 'return to menu' button.
;	1.7	William Osborn
;		Updated version number to 1.5
;	1.8	Tim Hammond + William Osborn
;		Added information widgets between screens.
;               Increased to v1.6
;	1.9	Richard Martin
;           Increased to v1.7
;	1.10	Richard Martin
;           Increased to v1.8
;	1.11	Richard Martin
;           Increased to v1.9
;	1.12	Richard Martin
;           Increased to v1.10
;
; VERSION:
;       1.1	05-06-95
;	1.2	20-06-95
;	1.3	14-07-95
;	1.4	27-02-96
;	1.5	13-05-96
;	1.6	21-06-96
;	1.7	11-07-96
;	1.8	06-08-96 / 14-10-96
;	1.9   23-06-98
;	1.10  04-12-98
;	1.11	18-10-99
;	1.12	14-10-00
;
;-
;-----------------------------------------------------------------------------


PRO ADAS308,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS308 V1.10'
    lpend = 0
    deffile = userroot+'/defaults/adas308_defaults.dat'
    device = ''
    bitfile = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf01/'
        inval.userroot = userroot+'/adf01/'

    end else begin

        inval = { 							$
		ROOTPATH:userroot+'/adf01/', 				$
		FILE:'', 						$
		CENTROOT:centroot+'/adf01/', 				$
		USERROOT:userroot+'/adf01/' }

    procval = {NEW:-1}

    outval = { 								$
               GRPOUT:0, GTIT1:'', GRPSCAL:0, 				$
                XMINA:'',  XMAXA:'',   					$
                YMINA:'',  YMAXA:'',   					$
		XMINB:'',  XMAXB:'',					$
                YMINB:'',  YMAXB:'',                                    $
                HRDOUT:0, HARDNAME:'', 					$
                GRPDEF:'', GRPFMESS:'',					$ 
                GRPSEL:-1, GRPRMESS:'', 				$
                DEVSEL:-1, GRSELMESS:'', 				$
                TEXOUT:0, TEXAPP:-1, 					$
                TEXREP:0, TEXDSN:'', 					$
                TEXDEF:'paper.txt',TEXMES:'' 				$
             }

    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas308.out',unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c8spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    c8spf0, pipe, inval, dsfull, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;**** Create an information widget ****

    widget_control, /hourglass
    widgetbase = widget_base(/column,xoffset=300,yoffset=200,     $
                             title = "ADAS308 INFORMATION")
    lab0 = widget_label(widgetbase, value='')
    lab1 = widget_label(widgetbase, value='')
    lab2 = widget_label(widgetbase, font=font_large,              $
                        value="       Accessing data - please wait       ")
    lab3 = widget_label(widgetbase, value='')
    lab4 = widget_label(widgetbase, value='')
    widget_control, widgetbase, /realize

		;*************************************
		;**** Read pipe comms from cxsetp ****
		;*************************************

    cxsetp, pipe, mxtab, mxgrf, symbd, idz0, symbr, irz0, irz1, irz2, $
            ngrnd, ntot


		;**************************************
                ;**** wait for signal from FORTRAN ****
                ;**** before continuing            ****
		;**************************************

    idum = 0
    readf, pipe, idum
    widget_control, widgetbase, /destroy


LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c8ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    c8ispf, pipe, lpend, procval, dsfull,          			$
            symbd, idz0, symbr, irz0, irz1, irz2,			$
	    ntot, ngrnd, mxtab, mxgrf, ngrf, graphdex, ritit, rtit2, 	$
            gomenu, bitfile, 						$
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

                ;**** Create an information widget ****

    widget_control, /hourglass
    widgetbase = widget_base(/column,xoffset=300,yoffset=200,     $
                             title = "ADAS308 INFORMATION")
    lab0 = widget_label(widgetbase, value='')
    lab1 = widget_label(widgetbase, value='')
    lab2 = widget_label(widgetbase, font=font_large,              $
                        value="       Processing - please wait       ")
    lab3 = widget_label(widgetbase, value='')
    lab4 = widget_label(widgetbase, value='')
    widget_control, widgetbase, /realize

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Call relevant routines for solving the ****
		;**** simultaneous equations for the FORTRAN ****
                ;************************************************

    c8wsol, pipe
    c8emqx, pipe

		;**************************************
                ;**** wait for signal from FORTRAN ****
                ;**** before continuing            ****
		;**************************************

    idum = 0
    readf, pipe, idum
    widget_control, widgetbase, /destroy


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c8spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
                ;**** Choose font according to platform      ****	
		;************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine ne 'HPUX' then begin
        outfont = font_small
    endif else begin
        outfont = font_large
    endelse

    c8spf1, pipe, lpend, outval, dsfull, header, ngrf, gomenu, bitfile, $
            DEVLIST=devlist, FONT=outfont

                ;**** If menu button clicked, tell FORTRAN to stop **** 

    if gomenu eq 1 then begin 
	printf, pipe, 1 
	outval.texmes = '' 
	goto, LABELEND 
    endif else begin 
	printf, pipe, 0 
    endelse 

		;**********************************************
		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
		;**********************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    endif


		;************************************************
                ;**** If graphical output requested          ****
		;**** Communicate with c8outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

               ;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmina = outval.xmina
        xmaxa = outval.xmaxa
        ymina = outval.ymina
        ymaxa = outval.ymaxa
	xminb = outval.xminb
        xmaxb = outval.xmaxb
        yminb = outval.yminb
        ymaxb = outval.ymaxb

		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1
  
        c8outg, dsfull, pipe, utitle, grpscal, 				$
	        xmina, xmaxa, ymina, ymaxa, 				$
	        xminb, xmaxb, yminb, ymaxb,				$
	        procval.npu, procval.npl, graphdex, ritit, rtit2, 	$
                hrdout, hardname, device, header, gomenu, bitfile,      $
 		FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop **** 

        if gomenu eq 1 then begin
            printf, pipe, 1
            outval.texmes=''
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

    if lpend eq 0 and outval.texout eq 1 then begin
        printf, pipe, header
    endif

		;**** Back for more output options ****

GOTO, LABEL300

LABELEND:

		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
