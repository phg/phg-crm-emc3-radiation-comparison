; Copyright (c) 1995, Strathclyde University
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas308/c8emqx.pro,v 1.2 2019/04/09 14:03:20 mog Exp $ Date $Date: 2019/04/09 14:03:20 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       C8EMQX
;
; PURPOSE:
;       Communicates with FORTRAN C8EMQX.FOR via pipe and solves
;       series of linear equations using the simplest idl algorithm.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS308
;       FORTRAN process via a UNIX pipe.  The set of linear equations
;       are then solved and the result written back to the FORTRAN.
;
; USE:
;       The use of this routine is specific to ADAS308.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS308 FORTRAN process.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       Uses the IDL mathematics library routine CRAMER to arrive at
;       an approximate solution for the linear equations.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 12-Jun-1995
;
; MODIFIED:
;       1.1             Tim Hammond                     12-06-95
;
; VERSION:
;       1.1             First release
;
;-
;-----------------------------------------------------------------------------


PRO c8emqx, pipe

    ;**** Read inputs from FORTRAN routine ****

    itemp = 0
    ftemp = 0.0d0

    readf, pipe, itemp
    nrep = itemp

    ared = fltarr(nrep, nrep)
    remisa = fltarr(nrep)
    remq = fltarr(nrep)

    for i=0,(nrep-1) do begin
        for j =0,(nrep-1) do begin
            readf, pipe, ftemp
            ared(i,j) = ftemp
        endfor
        readf, pipe, ftemp
        remisa(i) = ftemp
    endfor

    ;*******************************************************
    ;**** Now calculate the solution 'remq'             ****
    ;**** Note that if there was only one observed line ****
    ;**** (nrep=1) then we have a simple linear equation****
    ;**** and the CRAMER routine will crash if it is    ****
    ;**** asked to solve it.                            ****
    ;*******************************************************

    if (nrep eq 1) then begin
        remq(0) = remisa(0)/ared(0)
    endif else begin
        remq = cramer (ared, remisa, /double)
        remq = float(remq)
    endelse

    ;**** Write the solution back to the FORTRAN ****

    for i = 0, (nrep-1) do begin
        printf, pipe, remq(i)
    endfor

END
            
