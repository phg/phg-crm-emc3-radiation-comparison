; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas303/c3ispf.pro,v 1.4 2004/07/06 11:50:00 whitefor Exp $ Date @(#)$Header: /home/adascvs/idl/adas3xx/adas303/c3ispf.pro,v 1.4 2004/07/06 11:50:00 whitefor Exp $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C3ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS303 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS303
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS303
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	C3ISPF.
;
; USE:
;	The use of this routine is specific to ADAS303, see adas303.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS303 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS303 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas303.pro.  If adas303.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
; 		                      	new   : 0,              $
;              		         	title : '',             $
;                      		 	nbsel : 0 ,             $
;              		         	ibsel : 0 ,             $
;	                       		ifout : 1 ,             $
;                      		 	maxe  : 0,              $
;	                       		eine  : enarr,          $
;	                       		lfsel : 0,              $
;                       		ppsel : 0,              $
;                       		bmval : 0.0,            $
;                       		zfval : 0.0,            $
;                       		tival : 0.0,            $
;                       		dival : 0.0,            $
;                       		tolval: '5'             $
;             			  }
;
;		  See cw_adas303_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       NBSEL   _ Number of data blocks in the input file.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS303_PROC	Invoke the IDL interface for ADAS303 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS303 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 16-May-1995
;
; MODIFIED:
;	1.1             Tim Hammond                     16-05-95
;	1.2		Tim Hammond			16-05-95
;	1.3		Tim Hammond			30-05-95
;	1.4		William Osborn			05-06-96
;
; VERSION:
;	1.1		First release			16-05-95
;	1.2		Tidied code up			16-05-95
;	1.3		Corrected definitions of reference values
;							30-05-95
;	1.4		Added menu button code		05-06-96
;
;-
;-----------------------------------------------------------------------------


PRO c3ispf, pipe, lpend, procval, dsfull, nbsel, bitfile, gomenu, 	$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
        edit_fonts = {font_norm:'',font_input:''}

		;*********************************************
		;**** Declare variables for input, some   ****
		;**** arrays will be declared after sizes ****
		;**** have been read.                     ****
                ;*********************************************

    lpend = 0				;Cancel/done flag
    teref = dblarr (nbsel)		;Ion temperature ref values
    deref = dblarr (nbsel)		;Ion density ref values
    zeref = dblarr (nbsel) 		;Z effective ref values
    bmref = dblarr (nbsel)		;Mag field ref values
    nenera = intarr (nbsel)		;Nos. of energies in each block
    ntempa = intarr (nbsel) 		;Nos. of temperatures in blocks
    ndensa = intarr (nbsel) 		;Nos. of densities in blocks
    nzeffa = intarr (nbsel)		;Nos. of Z effectives in blocks
    nbmaga = intarr (nbsel)		;Nos. of mag fields in blocks
    evals = dblarr (24, 3, nbsel)	;Energies (in different units)
    tempa = dblarr (12, nbsel)		;Temperatures
    densa = dblarr (24, nbsel)		;Densities
    zeffa = dblarr (12, nbsel)		;Z. effectives
    bmaga = dblarr (12, nbsel)		;Mag. fields
    cpcode = strarr (nbsel)		;Generating code name
    cdonor = strarr (nbsel)		;Name of donor
    crecvr = strarr (nbsel)		;Name of receiver
    cwavel = strarr (nbsel)		;Transitions
    cfile = strarr (nbsel)		;Data filenames
    cindm = strarr (nbsel)		;Emission types
    ndein = 0				;Max no. of input temps allowed
    temp = 0.0d0			;Dummy variable used for reading
    itemp =0				;Dummy int var used for reading
    stemp = ' '				;Dummy string var used for reading


		;********************************
		;**** Read data from fortran ****
		;********************************

    for i = 0, (nbsel-1) do begin
        readf, pipe, temp    
        teref(i) = temp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, temp
        deref(i) = temp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, temp
        zeref(i) = temp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, temp    
        bmref(i) = temp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, itemp
        nenera(i) = itemp
        readf, pipe, itemp
        ntempa(i) = itemp
        readf, pipe, itemp
        ndensa(i) = itemp
        readf, pipe, itemp
        nzeffa(i) = itemp
        readf, pipe, itemp
        nbmaga(i) = itemp
    endfor
    for j = 0, 23 do begin
        for i = 0, (nbsel-1) do begin
            readf, pipe, temp
            densa(j,i) = temp
        endfor
    endfor
    for j = 0, 23 do begin
        for k = 0,2 do begin
            for i = 0, (nbsel-1) do begin
                readf, pipe, temp       
                evals(j,k,i) = temp       
            endfor
        endfor
    endfor
    for j = 0, 11 do begin
        for i = 0, (nbsel-1) do begin
            readf, pipe, temp         
            tempa(j,i) = temp
            readf, pipe, temp         
            zeffa(j,i) = temp
            readf, pipe, temp       
            bmaga(j,i) = temp
        endfor
    endfor	
    for i = 0, (nbsel-1) do begin
        readf, pipe, stemp
        cpcode(i) = stemp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, stemp      
        cdonor(i) = stemp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, stemp      
        crecvr(i) = stemp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, stemp     
        cwavel(i) = stemp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, stemp   
        cfile(i) = stemp
    endfor
    for i = 0, (nbsel-1) do begin
        readf, pipe, stemp    
        cindm(i) = stemp
    endfor
    readf, pipe, ndein

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        enarr = fltarr(ndein)
        procval = {		        $
		  new   : 0 ,           $
		  title : '',		$
                  nbsel : 0 ,           $
                  ibsel : 0 ,           $
                  ifout : 1, 		$
                  maxe  : 0,            $
		  eine  : enarr,        $
		  lfsel : 0,		$
                  ppsel : 0,		$
                  bmval : 0.0,		$
                  zfval : 0.0,          $
                  tival : 0.0,          $
                  dival : 0.0,          $
		  tolval: 5             $
	        }
    end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas303_proc, procval, dsfull, ndein, nenera, nbsel, evals, action, $
                  nenera, deref, densa, ndensa, teref, tempa, ntempa,   $
                  bmref, bmaga, nbmaga, zeref, zeffa, nzeffa,           $
                  cwavel, cpcode, cdonor, crecvr, cfile, cindm, bitfile,$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;***********************************************
		;**** Act on the output from the widget     ****
		;**** There are only three possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'           ****
		;***********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 1
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    printf,pipe,procval.title,format='(a40)'
    printf, pipe, procval.ibsel	
    printf,pipe,procval.ifout
    printf,pipe,procval.maxe
    printf, pipe, procval.eine
    printf,pipe,procval.lfsel
    printf,pipe,procval.tolval
    if (procval.ppsel) then begin
        printf,pipe,procval.dival
        printf,pipe,procval.tival
        printf,pipe,procval.zfval
        printf,pipe,procval.bmval
    endif else begin
        printf,pipe,deref(procval.ibsel)
        printf,pipe,teref(procval.ibsel)
        printf,pipe,zeref(procval.ibsel)
        printf,pipe,bmref(procval.ibsel)
    endelse

END
