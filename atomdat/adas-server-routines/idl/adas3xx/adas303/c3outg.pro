; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas303/c3outg.pro,v 1.2 2004/07/06 11:50:15 whitefor Exp $ Date $Date: 2004/07/06 11:50:15 $              
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C3OUTG
;
; PURPOSE:
;	Communication with ADAS303 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS303
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS303 is invoked.  Communications are to
;	the FORTRAN subroutine C3OUTG.
;
; USE:
;	The use of this routine is specific to ADAS303 see adas303.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS303 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS303_PLOT	ADAS303 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 15-May-1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First release
;	1.2	William Osborn
;		Added menu button code
; VERSION:
;	1.1	15-05-95
;	1.2	05-06-96
;
;-
;-----------------------------------------------------------------------------

PRO c3outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, device, header, bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    title = " "
    titlx = " "
    titlm = " "
    date = " "
    cwavel = " "
    cfile = " "
    cpcode  = " " 
    cdonor = " "
    crecvr = " "
    cindm = " "
    ieval = 0
    ldef1 = 0
    xmin = 0.0
    xmax = 0.0
    ymin = 0.0
    ymax = 0.0
    lfsel = 0
    nmx = 0
    head1 = " "
    head2a = " "
    head2b = " "
    dummy = 0.0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , title 
    readf, pipe, format = '(a120)', titlx 			 
    readf, pipe, format = '(a80)' , titlm 
    readf, pipe, format = '(a8)' , date 
    readf, pipe, format = '(a10)' , cwavel
    readf, pipe, format = '(a8)' , cfile
    readf, pipe, format = '(a8)' , cpcode
    readf, pipe, format = '(a8)' , cdonor
    readf, pipe, format = '(a5)' , crecvr
    readf, pipe, format = '(a6)' , cindm 
    readf, pipe, ieval 
		;**** now declare array dimensions ****
    eamu = dblarr(ieval) 
    qefa = dblarr(ieval) 
    for i=0, ieval-1 do begin
       readf, pipe, dummy
       eamu(i) = dummy 
    endfor
    for i=0, ieval-1 do begin
       readf, pipe, dummy
       qefa(i) = dummy 
    endfor

    readf, pipe, ldef1 
    if (ldef1 eq 1) then begin
       readf, pipe, xmin
       readf, pipe, xmax
       readf, pipe, ymin
       readf, pipe, ymax
    endif 
    readf, pipe, lfsel 
    if (lfsel eq 1) then begin
       readf, pipe, nmx 
 		;**** declare arrays ****
       qefm = fltarr(nmx)
       efitm = fltarr(nmx)

       for i = 0, nmx-1 do begin
	  readf, pipe, dummy
          qefm(i) = dummy 
       endfor

       for i = 0, nmx-1 do begin
	  readf, pipe, dummy 
          efitm(i) = dummy
       endfor
    endif

    readf, pipe, head1,  format = '(a32)'
    readf, pipe, head2a, format = '(a45)'
    readf, pipe, head2b, format = '(a45)'
    readf, pipe, bmval
    readf, pipe, zfval
    readf, pipe, tival
    readf, pipe, dival
    readf, pipe, bmref
    readf, pipe, zeref
    readf, pipe, teref
    readf, pipe, deref
  
		;***********************
		;**** Plot the data ****
		;***********************
    adas303_plot, dsfull, $
  		  title , titlx, titlm, utitle,date,  $
  		  cwavel, cfile, cpcode, cdonor, crecvr, cindm, ieval,  $
		  eamu, qefa, qefm, efitm, $
                  ldef1, xmin, xmax, ymin, ymax, $
                  bmval, zfval, tival, dival,    $
		  bmref, zeref, teref, deref,    $
   		  lfsel, nmx, $
   		  head1, head2a, head2b, $
		  hrdout, hardname, device, header,	$
		  bitfile, gomenu, FONT=font


END
