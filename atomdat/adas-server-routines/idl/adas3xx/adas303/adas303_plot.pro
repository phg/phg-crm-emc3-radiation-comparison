; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas303/adas303_plot.pro,v 1.4 2004/07/06 10:28:52 whitefor Exp $ Date $Date: 2004/07/06 10:28:52 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS303_PLOT
;
; PURPOSE:
;	Generates ADAS303 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine PLOT303 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS303, see c3outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	IZ0	- Integer; Nuclear charge of emitting ion
;
;	IZ	- Integer; Charge of emitting ion
;
;	CWAVEL 	- String; Wavelength of data block used
;
;	CFILE	- String; Specific ion file source
;
;	CTYPE  	- String; processing code from input data file
;
;	CINDM	- String; metastable index
;
;	IEVAL	- Integer; number of user entered energy pairs
;
;	EAMU	- Double array; User entered electron energies, amu
;
;	QEFA	- Double array; Spline interpolated or extrapolated 
;				effective emiss. coefficients for user 
;                               entered beam energies.
;
;	QEFM	- Double array; Minimax fit values of eff. emission coeffs.  
;				at 'efitm()'
;
;	EFITM	- Double array; Selected energy values for minmiax fit.
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	NMX	- Integer; Number of temp/density pairs used for minimax fit
;
;	HEAD1	- String; header information for data source
;
;	HEAD2A	- String; Header information for temperature/density 
;
;	HEAD2B  - String; column titles for temp/density pair information
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT303		Make one plot to an output device for 303.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT303_BLK.
;
;	One other routine is included in this file;
;	ADAS303_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  15-May-1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First release
;	1.2	Tim Hammond
;		Tidied up code
;	1.3	William Osborn
;		Added menu button code
;	1.4	Tim Hammond
;		Added "intelligent" resizing of the graphical output
;               window.
;
; VERSION:
;	1.1	15-05-95
;	1.2	15-05-95
;	1.3	05-06-96
;	1.4	06-08-96
;
;-
;----------------------------------------------------------------------------

PRO adas303_plot_ev, event

    COMMON plot303_blk, data, action, win, plotdev, 			$
		        plotfile, fileopen, gomenu


    newplot = 0
    print = 0
    done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		     end
	'bitbutton'   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
			gomenu = 1
		     end
  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin
		;**** Set graphics device ****
        if print eq 1 then begin

            set_plot,plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device,filename=plotfile
	        device,/landscape
            end

        end else begin

            set_plot,'X'
            wset,win
  
        end

		;**** Draw graphics ****
        plot303, data.x , data.y, data.ieval, data.nplots, data.nmx, 	$
	         data.title, data.xtitle, data.ytitle, 			$
	         data.head1, data.head2a,data.head2b, 			$
	         data.eamu, data.bmval, data.zfval, data.tival,   	$
                 data.dival, data.bmref, data.zeref, data.teref, 	$
                 data.deref,         					$
                 data.cwavel, data.cfile, data.cpcode, data.cdonor, 	$
                 data.crecvr, data.cindm,   				$
                 data.ldef1, 						$
	         data.xmin, data.xmax, data.ymin, data.ymax 


	if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control,event.id,set_value=grval
	end


    end

END

;----------------------------------------------------------------------------

PRO adas303_plot,   dsfull, 						$
		    title , titlx, titlm, utitle, date,  		$
                    cwavel, cfile, cpcode, cdonor, crecvr, cindm, ieval,$
                    eamu, qefa, qefm, efitm, 				$
                    ldef1, xmin, xmax, ymin, ymax, 			$
                    bmval, zfval, tival, dival,    			$
                    bmref, zeref, teref, deref,    			$
                    lfsel, nmx, 					$
                    head1, head2a, head2b, 				$
                    hrdout, hardname, device, header, bitfile, gomenu,	$
		    FONT=font

    COMMON plot303_blk, data, action, win, plotdev, 			$
               	        plotfile, fileopen, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(5)
    type = 'ELECTRON DENSITY'

    title(0) = "EFF. EMISSION COEFFS. VS NEUTRAL BEAM ENERGY "
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + strupcase(header)
    title(2) = 'FILE     : ' + strcompress((titlx))
    if (lfsel eq 1) then begin
        title(3)  = 'MINIMAX : ' + strupcase(titlm)
    endif
    title(4) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    if (lfsel eq 1) then  title(4) = title(4) + "  (DASH LINE - MINIMAX) "

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    if (lfsel eq 1) then ydim = nmx else ydim = ieval
    y = make_array(2, ydim, /float)
    valid_data = where((qefa gt 1e-37) and (qefa lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(0, valid_data) = qefa(valid_data)
        if (lfsel eq 1) then begin
            valid_data = where((qefm gt 1e-37) and (qefm lt 1e+37))
	    if (valid_data(0) ge 0) then begin
                y(1, valid_data) = qefm(valid_data)
            endif else begin
                print, "ADAS303 : unable to plot polynomial fit data"
            endelse
        endif
    endif else begin
        print, "ADAS303 : unable to plot spline fit data"
    endelse

    ytitle = "EFF. EMISSION COEFFICIENT (cm!e3!n s!e-1!n)"

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

    if (lfsel eq 1) then xdim = nmx else xdim = ieval
    x = fltarr(2, xdim)

		;*** write x axis array depending on whether temp ***
		;*** or density values used 			  ***

    if (lfsel eq 0) then begin
        x(0,0:ieval-1) = eamu
        nplots = 1
    endif else begin
        x(0,0:ieval-1) = eamu
        x(1,*)  = efitm
        nplots = 2
    endelse
        xtitle = "BEAM ENERGY (eV/amu)"

  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
        xmin = min(x, max = xmax)
        xmin = xmin * 0.9
        xmax = xmax * 1.1
        ymin = min( y, max = ymax)
        ymin = ymin * 0.9
        ymax = ymax *1.1
    endif



		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS303 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font, 		$
                         xsize=xwidth, ysize=yheight, bitbutton = bitval)

                ;**** Realize the new widget ****

    widget_control,graphid,/realize

		;**** Get the id of the graphics area ****

    widget_control,cwid,get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {    y:y,	x:x,   ieval:ieval,    nplots:nplots,  nmx:nmx, $
                title:title,   xtitle:xtitle,  ytitle:ytitle,           $
		head1:head1,    head2a:head2a,           		$
                head2b:head2b, eamu:eamu,      ldef1:ldef1, 		$
                bmval:bmval, zfval:zfval, tival:tival, dival:dival, 	$
                cwavel:cwavel, cfile:cfile, cpcode:cpcode, cdonor:cdonor,$
                crecvr:crecvr, cindm:cindm,                        	$
		bmref:bmref, zeref:zeref, teref:teref, deref:deref, 	$
		xmin:xmin,	xmax:xmax,	ymin:ymin,	ymax:ymax $
            }
 
    wset,win

    plot303, x , y, ieval, nplots, nmx, title, xtitle, ytitle, 		$
	      head1, head2a,head2b, eamu, 				$
	      bmval, zfval, tival, dival, 				$
              bmref, zeref, teref, deref,    				$
              cwavel, cfile, cpcode, cdonor, crecvr, cindm,  		$
	      ldef1, xmin, xmax, ymin, ymax 


		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager,'adas303_plot',graphid,event_handler='adas303_plot_ev', 	$
                                        /modal,/just_reg

    gomenu = gomenucom

END
