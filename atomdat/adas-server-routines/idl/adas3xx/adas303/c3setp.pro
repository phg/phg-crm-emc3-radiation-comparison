; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas303/c3setp.pro,v 1.1 2004/07/06 11:50:22 whitefor Exp $ Date $Date: 2004/07/06 11:50:22 $                         
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C3SETP
;
; PURPOSE:
;	IDL communications with ADAS303 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS303 FORTRAN subroutine
;	C3SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine C3SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS303.  See
;	adas303.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS303 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       NBLKS        - Number of data sub-blocks read in.
;
;       NBLOCK       - Max number of input sub-blocks.
;
;       TITLF(NBLKS) - Titles for each sub-block.
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 04/05/1995
;
; MODIFIED:
;
; VERSION:
;	1.1		First release			04-05-95
;
;-----------------------------------------------------------------------------



PRO c3setp, pipe, nbsel

		;**********************************
		;**** Initialise new variables ****
		;**********************************

  nbsel  = 0


		;********************************
		;**** Read data from fortran ****
		;********************************

  readf,pipe,nbsel

END
