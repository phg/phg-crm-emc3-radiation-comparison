; Copyright (c) 1995, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas303/plot303.pro,v 1.5 2004/07/06 14:31:13 whitefor Exp $ Date $Date: 2004/07/06 14:31:13 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT303
;
; PURPOSE:
;	Plot one graph for ADAS303.
;
; EXPLANATION:
;	This routine plots ADAS303 output for a single graph.
;
; USE:
;	Use is specific to ADAS303.  See adas303_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	IEVAL   - Integer : Number of data points and spline fit points
;
;	NPLOTS  - Integer : Number of plots  1: no minimax fit, 2 : minimax fit
;
;	NMX     - Integer : Number of points used in polynomial fit.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	HEAD1   - String : header information for data source.
;
;	HEAD2A  - String : header information for temperature/density 
;
;	HEAD2B  - String : column titles for temperature/density info.
;
;	TEVA    - fltarr : Energy values - for display only.
;
;	BMVAL   - Float  : the value used for B. Magnetic (in T)
;
;	ZFVAL   - Float  : the value used for Z effective
;
;	TIVAL   - Float  : the ion temperature used in the processing
;
;	DIVAL   - Float : the ion density used in the processing
;
;	BMREF	- Float : the B. Magnetic reference value
;
;	ZEREF	- Float : The reference value of Z effective
;
;	TEREF	- Float : the ion temperature reference value
;
;	DEREF	- Float : the ion density reference value
;
;	CWAVEL	- String : the transition being analysed
;
;	CFILE	- String : the data file from which the data comes
;
;	CPCODE	- String : the processing code
;
;	CDONOR	- String : the donor neutral atom	
;
;	CRECVR	- String : the receiver nucleus
;
;	CINDM	- String : the emission type
;
;	LDEF1	- Integer; 1 if user specified axis limits to be used, 
;		  0 if default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  15-May-1995
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First version
; 	1.2	Tim Hammond
;		Tidied code and comments up 
;	1.3	Tim Hammond
;		Improved min/max checking for x and y data  
;       1.4     Tim Hammond
;               Added new COMMON block Global_lw_data which contains the
;               values of left, right, top, bottom, grtop, grright
;	1.5	Tim Hammond
;		Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_begin file) then the font sized used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	15-05-95
;	1.2   	15-05-95
;	1.3 	22-05-95
;	1.4	27-02-96
;	1.5	06-08-96
;
;-
;----------------------------------------------------------------------------

PRO plot303, x , y, ieval, nplots, nmx, title, xtitle, ytitle,  $
             head1, head2a, head2b, teva, 			$
             bmval, zfval, tival, dival,  			$
	     bmref, zeref, teref, deref,  			$
             cwavel, cfile, cpcode, cdonor, crecvr, cindm,      $
             ldef1, xmin, xmax, ymin, ymax


    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8


		;**** set makeplot counter ****

    makeplot = 1

		;**** Construct graph title ****
		;**** "!C" is the new line control. ****

    !p.font=-1
    if small_check eq 'YES' then begin
        gtitle =  title(0) + "!C!C!C" + title(1) + "!C!C" + title(2) + 	$
                  "!C!C" + title(3) + "!C!C" + title(4)
    endif else begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
	          title(3) + "!C" + title(4)
    endelse

		;**** Find x and y ranges for auto scaling ****

    if ldef1 eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
        xvals = where(x gt 1.6e-36 and x lt 1.0e37)
        yvals = where(y gt 1.6e-36 and y lt 1.0e37)

        if xvals(0) gt -1 then begin
            xmax = max(x(xvals))
            xmin = min(x(xvals))
            makeplot = 1
        end else begin
            makeplot = 0
        end

        if yvals(0) gt -1 then begin
            ymax = max(y(yvals))
            ymin = min(y(yvals))
            makeplot = 1
        end else begin
            makeplot = 0
        end

        style = 0

    end else begin

		;Check that at least some data in in axes range ***

        xvals = where(x ge xmin and x le xmax)
        yvals = where(y ge ymin and y le ymax)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
            makeplot = 0
        end
            style = 1
    end

		;**** Set up log-log plotting axes ****

    plot_oo,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, 		  $
		position=[left,bottom,grright,grtop], 			  $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

    if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************

        oplot, x(0,0:ieval-1), y(0,0:ieval-1), psym=-1
        if (nplots eq 2) then begin
            oplot, x(1,*), y(1,*), linestyle=5
        endif 
 
    endif else begin
        print, "ADAS303 : No data found in these axes ranges"
    endelse

		;**** Annotation to plot ****

    rhs = grright + 0.02

		;**** plot title ****

    if small_check eq 'YES' then begin
        xyouts, (left-0.05), top, gtitle, /normal, alignment=0.0,	$
                charsize = charsize*0.9
    endif else begin
        xyouts, (left-0.05), top, gtitle, /normal, alignment=0.0,	$
                charsize = charsize*0.95
    endelse

		;**** right hand side labels ****

    if small_check eq 'YES' then charsize=charsize*0.95
    cpp = grtop - 0.05
    xyouts, rhs,  cpp, head1, /normal, alignment=0.0,charsize=charsize*0.9
    cpp = cpp -0.04
    trantit = "TRANSITION (N-N')"
    trantit2 = "= "+strcompress(cwavel,/remove_all)
    xyouts, rhs,  cpp, trantit, /normal, alignment=0.0,      		$
    charsize=charsize*0.9
    xyouts, rhs+0.22, cpp, trantit2, /normal, alignment=0.0,      	$
    charsize=charsize*0.9
    cpp = cpp -0.02
    neutit = "NEUTRAL ATOM DONOR"
    neutit2 = "= "+strcompress(cdonor)
    xyouts, rhs,  cpp, neutit, /normal, alignment=0.0,      		$
    charsize=charsize*0.9
    xyouts, rhs+0.22, cpp, neutit2, /normal, alignment=0.0,    		$
    charsize=charsize*0.9
    cpp = cpp -0.02
    bartit = "BARE NUCLEUS RECEIVER"
    bartit2 = "= "+strcompress(crecvr, /remove_all)
    xyouts, rhs,  cpp, bartit, /normal, alignment=0.0,      		$
    charsize=charsize*0.9
    xyouts, rhs+0.22, cpp, bartit2, /normal, alignment=0.0,    		$
    charsize=charsize*0.9
    cpp = cpp -0.02
    qcxtit = "QCX SOURCE FILE MEMBER"
    qcxtit2 = "= "+strcompress(cfile)
    xyouts, rhs,  cpp, qcxtit, /normal, alignment=0.0,      		$
    charsize=charsize*0.9
    xyouts, rhs+0.22, cpp, qcxtit2, /normal, alignment=0.0,    		$
    charsize=charsize*0.9
    cpp = cpp -0.02
    protit = "PROCESSING CODE"
    protit2 ="= "+strcompress(cpcode)
    xyouts, rhs,  cpp, protit, /normal, alignment=0.0,      		$
    charsize=charsize*0.9
    xyouts, rhs+0.22, cpp, protit2, /normal, alignment=0.0,      	$
    charsize=charsize*0.9
    cpp = cpp -0.02
    emistit = "EMISSION TYPE"
    emistit2 = "= "+strcompress(cindm)
    xyouts, rhs,  cpp,  emistit, /normal, alignment=0.0,      		$
    charsize=charsize*0.9
    xyouts, rhs+0.22, cpp, emistit2, /normal, alignment=0.0,      	$
    charsize=charsize*0.9

    cpp = cpp - 0.05	
    xyouts, rhs, cpp, head2a, /normal, charsize=charsize*0.9
    cpp = cpp - 0.04
    xyouts, rhs, cpp, head2b, /normal, charsize=charsize*0.9
    cpp = cpp - 0.04
    detit="ION DENS. (cm!e-3!n)  "+strcompress(string(dival, 		$
    format='(e10.3)'))+"    "+strcompress(string(deref, 		$
    format='(e10.3)'))
    xyouts, rhs, cpp, detit, /normal, 					$
    charsize=charsize*0.9
    tetit = "ION TEMP. (eV)    "+strcompress(string(tival, 		$
    format='(e10.3)'))+"    "+strcompress(string(teref, 		$
    format='(e10.3)'))
    xyouts, rhs, cpp-0.02, tetit, /normal, 				$
    charsize=charsize*0.9
    zftit="Z EFFECTIVE       "+strcompress(string(zfval, 		$
    format='(e10.3)'))+"    "+strcompress(string(zeref, 		$
    format='(e10.3)'))
    xyouts, rhs, cpp-0.04, zftit, /normal, 				$
    charsize=charsize*0.9
    bmtit="B MAGN. (T)       "+strcompress(string(bmval, 		$
    format='(e10.3)'))+"    "+strcompress(string(bmref, 		$
    format='(e10.3)'))
    xyouts, rhs, cpp-0.06, bmtit, /normal, 				$
    charsize=charsize*0.9


END
