; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module   @(#)$Header: /home/adascvs/idl/adas3xx/adas303/adas303_proc.pro,v 1.3 2004/07/06 10:29:00 whitefor Exp $ Date $Date: 2004/07/06 10:29:00 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS303_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS303
;	processing.
;
; USE:
;	This routine is ADAS303 specific, see c3ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas303_proc.pro.
;
;		  See cw_adas303_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NDEIN	- Integer : number of energies selected.
;
;	NENERA	- Intarr: number of beam energies in each data block
;                               dimension - NBSEL
;
;	NBSEL   - Integer : number of data blocks accepted and read-in
;
;       EVALS   - Float array; energy values from data file
;                               1st dimension - energy index
;				2nd dimension - units flag
;				3rd dimension - NBSEL
;
;       NEIN    - Integer array; Number of beam energies per block
;                               dimension - NBSEL
;
;       DEREF   - dblarr: reference ion densities
;                               dimension - NBSEL
;
;       DENSA   - 2D dblarr: ion densities
;                               dimensions - 24, nbsel
;
;       NDENSA  - intarr: nos. of ion densities in each data block
;                               dimension - NBSEL
;       TEREF   - dblarr: reference ion temperatures
;                               dimension - NBSEL
;
;       TEMPA   - 2D dblarr: temperatures
;                               dimensions - 12, nbsel
;
;       NTEMPA  - intarr: nos. of ion temperatures in each data block
;                               dimension - NBSEL
;
;       BMREF   - dblarr: reference magnetic field values
;                               dimension - NBSEL
;
;       BMAGA   - 2D dblarr: Magnetic fields
;                               dimensions - 12, nbsel
;
;       NBMAGA  - intarr: nos. of magnetic fields in each data block
;                               dimension - NBSEL
;
;       ZEREF   - dblarr: reference effective Z's
;                               dimension - NBSEL
;
;       ZEFFA   - 2d dblarr: Z effectives
;                               dimensions - 12, nbsel
;
;       NZEFFA  - intarr: nos. of Z effectives in the data blocks
;                               dimension - nbsel
;	
;	CWAVEL  - String array : Wavelength (Angstroms) of data blocks
;				 dimension data block index (NBSEL)  
;	CFILE   - String array : Specific Ion file source.
;				 dimension data block index (NBSEL)  
;	CTYPE   - String array : processing code for each data block 
;				 dimension data block index (NBSEL)  
;	CINDM   - String array : Metastable index for each data block
;				 dimension data block index (NBSEL)  
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS303_PROC	Declares the processing options widget.
;	ADAS303_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;
; SIDE EFFECTS:
;       This widget uses a common block PROC303_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 16-May-1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First release
;	1.2	Tim Hammond
;		Tidied up code
;	1.3	William Osborn
;		Added menu button code and dynlabel procedure
; VERSION:
;	1.1	16-05-95
;	1.2	16-05-95
;	1.3	05-06-96
;-
;-----------------------------------------------------------------------------


PRO adas303_proc_ev, event


    COMMON proc303_blk,action,value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control,event.id,get_value=value 
	    widget_control,event.top,/destroy

	end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas303_proc, procval, dsfull, ndein, nenera, nbsel, evals, act,    $
                  nein, deref, densa, ndensa, teref, tempa, ntempa,     $
                  bmref, bmaga, nbmaga, zeref, zeffa, nzeffa,           $
                  cwavel, cpcode, cdonor, crecvr, cfile, cindm, bitfile,$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc303_blk,action,value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
	edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS303 PROCESSING OPTIONS', 		$
			 XOFFSET=50,YOFFSET=0)

		;**** Declare processing widget ****

    cwid = cw_adas303_proc(procid, dsfull, ndein, nenera, nbsel, 	$
                           evals, nein, deref, densa, ndensa, 		$
                           teref, tempa, ntempa,     			$
                           bmref, bmaga, nbmaga,     			$
                           zeref, zeffa, nzeffa,     			$
                           cwavel, cpcode, cdonor, crecvr, cfile, cindm,$
		           bitfile, PROCVAL=value, 			$
		           FONT_LARGE=font_large, FONT_SMALL=font_small,$
		           EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control,procid,/realize

		;**** make widget modal ****

    xmanager,'adas303_proc',procid,event_handler='adas303_proc_ev', 	$
	/modal,/just_reg

		;*** copy value back to procval for return to c3ispf ***
    act = action
    procval = value
 
END

