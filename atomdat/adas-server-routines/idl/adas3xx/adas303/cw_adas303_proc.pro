; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas303/cw_adas303_proc.pro,v 1.8 2004/07/06 12:37:24 whitefor Exp $ Date $Date: 2004/07/06 12:37:24 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS303_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS303 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a widget to select the data block for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for temperature/density data cw_adas_table,
;	   buttons to enter default values into the table, 
;          a widget to enter parameter values.
;	   a message widget, a 'Cancel', a 'Done' and a 'Menu' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done', 'Cancel' and
;	'Menu' buttons.
;
; USE:
;	This widget is specific to ADAS303, see adas303_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	NDEIN	- Integer; number of energies selected
;
;	NENERA  - Intarr: number of beam energies in each data block
;				dimension - NBSEL
;
;	NBSEL   - Integer; number of data blocks included in this file
;
;       EVALS   - Float array; energy values from data file
;                               dimension - NBSEL
;
;       NEIN    - Integer array; Number of beam energies per block
;                               dimension - NBSEL
;
;	DEREF	- dblarr: reference ion densities
;                               dimension - NBSEL
;
;	DENSA	- 2D dblarr: ion densities
;				dimensions - 24, nbsel
;
;	NDENSA	- intarr: nos. of ion densities in each data block
;				dimension - NBSEL
;
;       TEREF	- dblarr: reference ion temperatures
;                               dimension - NBSEL
;
;       TEMPA	- 2D dblarr: temperatures
;                               dimensions - 12, nbsel
;
;       NTEMPA  - intarr: nos. of ion temperatures in each data block
;                               dimension - NBSEL
;
;       BMREF	- dblarr: reference magnetic field values
;                               dimension - NBSEL
;
;       BMAGA	- 2D dblarr: Magnetic fields
;                               dimensions - 12, nbsel
;
;       NBMAGA	- intarr: nos. of magnetic fields in each data block
;                               dimension - NBSEL
;
;       ZEREF	- dblarr: reference effective Z's
;                               dimension - NBSEL
;
;       ZEFFA   - 2d dblarr: Z effectives
;                               dimensions - 12, nbsel
;
;       NZEFFA	- intarr: nos. of Z effectives in the data blocks
;                               dimension - nbsel
;
;	CWAVEL  - String array; wavelength values from file
;				dimension - NBSEL
;
;	CFILE   - String array; specific ion source file
;				dimension - NBSEL
;
;	CTYPE  - String array; Data type code
;				dimension - NBSEL
;
;	CINDM   - String array; Metastable index
;				dimension - NBSEL
;
;	The inputs map exactly onto variables of the same
;	name in the ADAS303 FORTRAN program.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;		enarr = fltarr(ndein)
;     		ps = {proc303_set, $
;			new   : 0,		$
;                	title : '',             $
;			nbsel : 0 ,		$
;                	ibsel : 0 ,             $
;			ifout : 1 ,             $
;			maxe  : 0, 		$
;			eine  : enarr,		$
;                	lfsel : 0,              $
;			ppsel : 0,		$
;			bmval : 0.0,		$
;			zfval : 0.0,		$
;			tival : 0.0,		$
;			dival : 0.0,		$
;                	tolval: '5'		$
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IBSEL   Selected data block
;		NBSEL   Number of blocks in data file
;		IFOUT   Index indicating which units are being used
;		MAXE    Number of energy values selected
;		EINE	User supplied energy values for fit.
;		LFSEL   Flag as to whether polynomial fit is chosen
;		PPSEL   Flag as to whether plasma parameters are to
;			be specified by user or not.
;		BMVAL   User specified value of Magnetic field
;		ZFVAL   User specified value of Z effective
;		TIVAL   User specified ion temperature
;		DIVAL   User specified ion density
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS303 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC303_GET_VAL()	Returns the current PROCVAL structure.
;	PROC303_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 15-May-1995
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First version
;	1.2	Tim Hammond			
;		Tidied up code      
;	1.3	Tim Hammond				
;		Corrected default defn of reference values
;       1.4     Tim Hammond
;               Modified use of fonts and sizes of tables for use on
;               different platforms.
;       1.5     William Osborn
;               Added menu button
;	1.6	Tim Hammond
;		Ensured a default font name is always given and
;		made it so you can step through the plasma parameters
;		by pressing enter.
;	1.7	Richard Martin
;		Added '/remove_all' to strcompress statements in Plasma 
;		Parameters sub-widget so that full number is viewed.
;	1.8	Richard Martin
;		IDL 5.5 updates.
;
; VERSION:
;	1.1	15-05-95	
;	1.2	16-05-95
;	1.3	30-05-95
;	1.4	27-02-96
;	1.5	05-06-96
;	1.6	06-08-96
;	1.7	24-11-98
;	1.8	08-03-02
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc303_get_val, id

    COMMON cw_proc303_blk, dival, tival, zfval, bmval, denmin, denmax, $
                           temmin, temmax, zefmin, zefmax, bmamin,     $
                           bmamax


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control,state.runid,get_value=title
    title_len = strlen(title(0)) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))
  

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

    widget_control,state.tempid,get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units+1

		;**** Copy out temperature values ****

    eine = dblarr(state.ndein)
    blanks = where(strtrim(tabledata(0,*),2) eq '')
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
    if blanks(0) ge 0 then maxe=blanks(0) else maxe=state.ndein
                ;**** Only perform following if there is 1 or ****
                ;**** more energy present in the table        ****

    if (maxe ge 1) then begin
        eine(0:maxe-1) = double(tabledata(0,0:maxe-1))
    endif

		;**** Fill out the rest with zero ****

    if maxe lt state.ndein then begin
        eine(maxe:state.ndein-1) = double(0.0)
    endif

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control,state.optid,get_uvalue=polyset,/no_copy
    lfsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
        tolval = (polyset.optionset.value[0])
    endif else begin
        tolval = -999
    endelse
    widget_control,state.optid,set_uvalue=polyset,/no_copy

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.indexid, get_value=select_block
    ibsel = select_block[0] 

                ;***********************************************
                ;**** get parameter values                  ****
                ;***********************************************

    if (state.ppsel eq 1) then begin

        widget_control, state.pdenbut, get_value=get_pdenbut,/no_copy
        pdencheck = byte(strcompress(string(get_pdenbut[0]), /remove_all))
        maxk = max(pdencheck)
        mink = min(pdencheck)
        if (((maxk gt 58) and (maxk ne 69) and (maxk ne 101)    $
        and (maxk ne 68) and (maxk ne 100) ) or                 $
        ((mink lt 48) and (mink ne 46) and (mink ne 45) and     $
        (mink ne 43) ) ) then begin
            dival = -99
        endif else begin
            dival = double(get_pdenbut[0])
        endelse
        widget_control, state.pdenbut, set_value=get_pdenbut, /no_copy

        widget_control, state.ptembut, get_value=get_ptembut,/no_copy
        ptemcheck = byte(strcompress(string(get_ptembut[0]), /remove_all))
        maxk = max(ptemcheck)
        mink = min(ptemcheck)
        if (((maxk gt 58) and (maxk ne 69) and (maxk ne 101)    $
        and (maxk ne 68) and (maxk ne 100) ) or                 $
        ((mink lt 48) and (mink ne 46) and (mink ne 45) and     $
        (mink ne 43) ) ) then begin
            tival = -99
        endif else begin
            tival = double(get_ptembut[0])
        endelse
        widget_control, state.ptembut, set_value=get_ptembut, /no_copy
                                                                      
        widget_control, state.pbmabut, get_value=get_pbmabut,/no_copy
        pbmacheck = byte(strcompress(string(get_pbmabut[0]), /remove_all))
        maxk = max(pbmacheck)
        mink = min(pbmacheck)
        if (((maxk gt 58) and (maxk ne 69) and (maxk ne 101)    $
        and (maxk ne 68) and (maxk ne 100) ) or                 $
        ((mink lt 48) and (mink ne 46) and (mink ne 45) and     $
        (mink ne 43) ) ) then begin
            bmval = -99
        endif else begin
            bmval = double(get_pbmabut[0])
        endelse
        widget_control, state.pbmabut, set_value=get_pbmabut, /no_copy
                                                                       
        widget_control, state.pzefbut, get_value=get_pzefbut,/no_copy
        pzefcheck = byte(strcompress(string(get_pzefbut[0]), /remove_all))
        maxk = max(pzefcheck)
        mink = min(pzefcheck)
        if (((maxk gt 58) and (maxk ne 69) and (maxk ne 101)    $
        and (maxk ne 68) and (maxk ne 100) ) or                 $
        ((mink lt 48) and (mink ne 46) and (mink ne 45) and     $
        (mink ne 43) ) ) then begin
            zfval = -99
        endif else begin
            zfval = double(get_pzefbut[0])
        endelse
        widget_control, state.pzefbut, set_value=get_pzefbut, /no_copy

    endif else begin

        bmval = state.bmval
        zfval = state.zfval
        tival = state.tival
        dival = state.dival

    endelse
  

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 				$
	        new   : 0, 					$
                title : title,          			$
		nbsel : state.nbsel, 				$
                ibsel : ibsel ,             			$
		ifout : ifout,					$
		maxe  : maxe,					$
                eine  : eine,       				$
                lfsel : lfsel,              			$
                ppsel : state.ppsel,            		$
                bmval : bmval,					$
                zfval : zfval,                  		$
                tival : tival,                  		$
                dival : dival,                  		$
                tolval: tolval					$
              }
   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc303_event, event

    COMMON cw_proc303_blk, dival, tival, zfval, bmval, denmin, denmax,  $
                           temmin, temmax, zefmin, zefmax, bmamin,      $
                           bmamax
    COMMON cw_proc303_blk2, deref, densa,  ndensa,    			$
                            teref, tempa, ntempa,     			$
                            bmref, bmaga, nbmaga,     			$
                            zeref, zeffa, nzeffa

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy
  
		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;************************
                ;**** New index selected*
                ;************************

        state.indexid: begin

            widget_control, state.indexid, get_value=index

                ;*** Reset all reference values and ranges***

            dentext = "   "+strcompress(string(deref(index),format='(E10.3)'))
            denmin = min(densa(0:(ndensa(index)-1), index))
            dentext=dentext+"     "+strcompress(string(denmin,format='(E10.3)'))
            denmax = max(densa(0:(ndensa(index)-1), index))
            dentext=dentext+"  "+strcompress(string(denmax,format='(E10.3)'))
            widget_control, state.pdenval, set_value=dentext

            temtext = "   "+strcompress(string(teref(index),format='(E10.3)'))
            temmin = min(tempa(0:(ntempa(index)-1), index))
            temtext=temtext+"     "+strcompress(string(temmin,format='(E10.3)'))
            temmax = max(tempa(0:(ntempa(index)-1), index))
            temtext=temtext+"  "+strcompress(string(temmax,format='(E10.3)'))
            widget_control, state.ptemval, set_value=temtext

            zeftext = "   "+strcompress(string(zeref(index),format='(E10.3)'))
            zefmin = min(zeffa(0:(nzeffa(index)-1), index))
            zeftext=zeftext+"     "+strcompress(string(zefmin,format='(E10.3)'))
            zefmax = max(zeffa(0:(nzeffa(index)-1), index))
            zeftext=zeftext+"  "+strcompress(string(zefmax,format='(E10.3)'))
            widget_control, state.pzefval, set_value=zeftext

            bmatext = "   "+strcompress(string(bmref(index),format='(E10.3)'))
            bmamin = min(bmaga(0:(nbmaga(index)-1), index))
            bmatext=bmatext+"     "+strcompress(string(bmamin,format='(E10.3)'))
            bmamax = max(bmaga(0:(nbmaga(index)-1), index))
            bmatext=bmatext+"  "+strcompress(string(bmamax,format='(E10.3)'))
            widget_control, state.pbmaval, set_value=bmatext

        end

		;***************************
		;**** Parameter typeins ****
		;***************************

	state.pbmabut: widget_control, state.pdenbut, /input_focus
	state.ptembut: widget_control, state.pzefbut, /input_focus
	state.pdenbut: widget_control, state.ptembut, /input_focus
	state.pzefbut: widget_control, state.pbmabut, /input_focus

                ;************************
                ;**** Parameter button **
                ;************************

        state.pbut:  begin

            if (state.ppsel eq 0) then state.ppsel =1 else  		$
            state.ppsel = 0
            widget_control, state.ppheadlabel, sensitive=state.ppsel
            widget_control, state.ppheadlabel2,sensitive=state.ppsel
            widget_control, state.pdenbut,sensitive=state.ppsel
            widget_control, state.pdenlab,sensitive=state.ppsel
            widget_control, state.pdenval,sensitive=state.ppsel
            widget_control, state.ptembut,sensitive=state.ppsel
            widget_control, state.ptemlab,sensitive=state.ppsel
            widget_control, state.ptemval,sensitive=state.ppsel
            widget_control, state.pzefbut,sensitive=state.ppsel
            widget_control, state.pzeflab,sensitive=state.ppsel
            widget_control, state.pzefval,sensitive=state.ppsel
            widget_control, state.pbmabut,sensitive=state.ppsel
            widget_control, state.pbmalab,sensitive=state.ppsel
            widget_control, state.pbmaval,sensitive=state.ppsel

        end

		;*************************************
		;**** Default temperature button ****
		;*************************************

    state.deftid: begin

		;**** popup window to confirm overwriting current values ****

        action= popup(message='Confirm Overwrite values with Defaults', $
	buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	     widget_control,state.tempid,get_value=tempval

		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all density values to same   ****
		;**** value  				    ****

             deft = tempval
             units = tempval.units+1
	     maxe = state.maxe
 	     if maxe gt 0 then begin
   	         tempval.value(0,0:maxe-1) = 				$
                 strtrim(string(deft.value(units,0:maxe-1), 		$
	         format=state.num_form),2)
 	     end

		;**** Fill in the rest with blanks ****

 	     if maxe lt state.ndein then begin
   	         tempval.value(0,maxe:state.ndein-1) = ''
 	     end

		;**** Copy new data to table widget ****

             widget_control,state.tempid, set_value=tempval

        end

    end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc303_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

		;**** check temp/density values entered ****

	if error eq 0 and ps.maxe eq 0 then begin
	    error = 1
	    message='Error: No energies entered.'
        end

		;*** Check to see if index has been selected ***

	if error eq 0 and ps.ibsel lt 0 then begin
	    error = 1
	    message='Error: Invalid block selected'
	end

		;*** Check to see if sensible tolerance is selected.

	if (error eq 0 and ps.lfsel eq 1) then begin 			$
	    if (float(ps.tolval) lt 0)  or 				$
            (float(ps.tolval) gt 100) then begin
	        error = 1
	        message='Error: Tolerance for polyfit must be 0-100% '
	    endif
	endif

                ;*** Check to see if sensible density selected ****

         if (error eq 0 and ps.ppsel eq 1) then begin 			$
             if ( (ps.dival lt 0) or 					$
             (ps.dival lt denmin) or (ps.dival gt denmax) ) then begin
                 error = 1
                 message="Error: Ion density must be in range indicated "
            endif
        endif

                ;*** Check to see if sensible temperature selected ****

         if (error eq 0 and ps.ppsel eq 1) then begin 			$
            if ( (ps.tival lt 0) or 					$
            (ps.tival lt temmin) or (ps.tival gt temmax) ) then begin
                 error = 1
                 message="Error: Ion temperature must be in range indicated "
            endif
        endif

                ;*** Check to see if sensible Z eff. selected ****

         if (error eq 0 and ps.ppsel eq 1) then begin 			$
            if ( (ps.zfval lt 0) or 					$
            (ps.zfval lt zefmin) or (ps.zfval gt zefmax) ) then begin
                error = 1
                message="Error: Z effective must be in range indicated "
            endif
        endif

                ;*** Check to see if sensible B mag. selected ****

         if (error eq 0 and ps.ppsel eq 1) then begin 			$
            if ( (ps.bmval lt 0) or 					$
            (ps.bmval lt bmamin) or (ps.bmval gt bmamax) ) then begin
                error = 1
                message="Error: B magnetic must be in range indicated "
            endif
        endif

		;**** return value or flag error ****

	if error eq 0 then begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	    widget_control,state.messid,set_value=message
	    new_event = 0L
        end
	
    end
		;*********************
                ;**** Menu button ****
		;*********************

    state.outid: begin
        new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                     ACTION:'Menu'}
    end

    ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas303_proc, topparent, dsfull, ndein, nenera, nbsel, 	$
                	  evals, nein, deref, densa,  ndensa,		$
                	  teref, tempa, ntempa,     			$
                	  bmref, bmaga, nbmaga,   			$ 
                	  zeref, zeffa, nzeffa,     			$
                	  cwavel, cpcode, cdonor, crecvr, cfile, cindm, $
			  bitfile, procval=procval, uvalue=uvalue, 	$
			  font_large=font_large, font_small=font_small, $
			  edit_fonts=edit_fonts, num_form=num_form

    COMMON cw_proc303_blk,  dival, tival, zfval, bmval, denmin, denmax, $
                            temmin, temmax, zefmin, zefmax, bmamin,    	$
                            bmamax

    COMMON cw_proc303_blk2, derefcom, densacom, ndensacom,     		$
                            terefcom, tempacom, ntempacom,     		$
                            bmrefcom, bmagacom, nbmagacom,     		$
                            zerefcom, zeffacom, nzeffacom

    derefcom = deref
    densacom = densa
    ndensacom = ndensa
    terefcom = teref
    tempacom = tempa
    ntempacom = ntempa
    bmrefcom = bmref
    bmagacom = bmaga
    nbmagacom = nbmaga
    zerefcom = zeref
    zeffacom = zeffa
    nzeffacom = nzeffa

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''	
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
	edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
        enarr = fltarr(ndein)
        ps = { 								$
		new   : 0, 						$
		title : '',             				$
		nbsel : 0 , 						$
                ibsel : 0 ,             				$
		ifout : 1,						$
		maxe  : 0,						$
                eine  : enarr,          				$
                lfsel : 0,              				$
                ppsel : 0,              				$
		bmval : 0.0,						$
                zfval : 0.0,              				$
                tival : 0.0,              				$
                dival : 0.0,              				$
                tolval: '5'						$
              }
    endif else begin
        ps = { 								$
		new   : procval.new,       				$
		title : procval.title,     				$
		nbsel : procval.nbsel,     				$
                ibsel : procval.ibsel,     				$
		ifout : procval.ifout,	   				$
		maxe  : procval.maxe,	   				$
                eine  : procval.eine,      				$
                lfsel : procval.lfsel,     				$
                ppsel : procval.ppsel,     				$
		bmval : procval.bmval,	   				$
                zfval : procval.zfval,     				$
                tival : procval.tival,     				$
                dival : procval.dival,     				$
                tolval: procval.tolval    				$
	     }
    endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
    y_size = 4
    large_font = font_small
  endif else begin
    y_size = 6
    large_font = font_large
  endelse

		;****************************************************
		;**** Assemble temperature  & density table data ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temp. & dens.table array          *****
		;**** col 1 has user temperature values         *****
		;**** col 2 has temperature values from files,  *****
		;**** which can have one of three possible units*****
		;**** col 5 has user density values             *****
		;**** col 6 has density values from file.       *****
		;****************************************************

    tabledata = strarr(4,ndein)

		;**** Copy out temperature & density values ****
		;**** number of temperature and density     ****
		;**** values for this data block            ****

    if (ps.ibsel gt nbsel) then ps.ibsel = 0
    maxe = nein(ps.ibsel) 

    if (maxe gt 0) then begin
        tabledata(0,*) = 						$
            strtrim(string(ps.eine(*),format=num_form),2)
        tabledata(1,0:maxe-1) = 					$
	    strtrim(string(evals(0:maxe-1,0,ps.ibsel),format=num_form),2)
        tabledata(2,0:maxe-1) = 					$
	    strtrim(string(evals(0:maxe-1,1,ps.ibsel),format=num_form),2)
        tabledata(3,0:maxe-1) = 					$
            strtrim(string(evals(0:maxe-1,2,ps.ibsel),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(ps.eine eq 0.0) 
        tabledata(0,blanks) = ' ' 
        tabledata(1:3,maxe:ndein-1) = ' '
    end

		;********************************************************
		;**** Create the 303 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS303 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc303_event", 			$
			 FUNC_GET_VALUE = "proc303_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase,dsfull,font=large_font)

		;***************************************************
		;**** add a window to display and select index   ***
		;**** first create data array for table          ***
		;**** Then convert to 1D string array(text_table)***
		;**** call cw_single_sel.pro for options choice  ***
		;***************************************************

    block_info = strarr(6, nbsel)
    block_info(0,*) = cwavel
    block_info(1,*) = cdonor
    block_info(2,*) = crecvr
    block_info(3,*) = cfile
    block_info(4,*) = cpcode
    block_info(5,*) = cindm

    titles = [['Transition', 'Donor', 'Receiver', 'QCX File', 'Processing',$
               'Emission'], 						   $
   	      ['N - N''','','', 'Source', 'Code', 'Type']]

    select_data = text_table(block_info, colhead=titles)

    coltitles = select_data(0:1)
    select_data = select_data(2:nbsel+1)
    indexid = cw_single_sel( parent, select_data, value=ps.ibsel, 	$
			     title='Select data Block', 		$
			     coltitles = coltitles, 			$
			     ysize = (y_size-2),			$
			     font=font_small, 				$
			     big_font=large_font 			$
			   )

		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    base = widget_base(tablebase,/column,/frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input'], 					$
	       ['','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['eV/amu','Atomic units','cm/sec']

		;**** Two columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three ****
		;**** units but column 2 switches between sets 2,3 & 4 ****
		;**** in the input array tabledata as the units change.  ****

    unitind = [[0,0,0],[1,2,3]]
    table_title = [ [" "],["Neutral Beam Donor Energy Values"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind,   $
  			   UNITSTITLE = 'Energy/Velocity Units', 	$
			   COLEDIT = [1,0], COLHEAD = colhead, 		$
			   TITLE = table_title, 			$
			   ORDER = [1,0], 				$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase,value=' Default Energy/Velocity Values  ',$
		           font=large_font)

                ;******************************************
                ;**** A base for the plasma parameters ****
                ;******************************************

    ppbase = widget_base(tablebase, /column, /frame)

                ;******************************************
                ;**** 'PPSEL' parameter switch.        ****
                ;******************************************

    pppbase = widget_base(ppbase, /row)
    ppplabel = widget_label(pppbase, font=large_font,  			$
               value = "Select supplementary plasma parameters")
    pbutbase = widget_base(pppbase, /row, /nonexclusive)
    pbut = widget_button(pbutbase, value=' ')
    widget_control, pbut, set_button=ps.ppsel

                ;******************************************
                ;**** Column headings                  ****
                ;******************************************

    ppheadlabel = widget_label(ppbase, font=font_small,        		 $
                  value = "                       Output      Reference"+$
                          "   --- Scan Range ---  ")
    ppheadlabel2= widget_label(ppbase, font=font_small,        	 $
                  value = "                       Value:      Value:   "+$
                          "   Minimum   Maximum   ")
  
                ;******************************************
                ;**** Density "button"                 ****
                ;******************************************
                                                                    
    pdenbutbase = widget_base(ppbase, /row, /frame)
    den_title = "Ion Density (cm-3)   "
    pdenlab = widget_label(pdenbutbase, value = den_title, font=font_small)
    denmin = min(densa(0:(ndensa(ps.ibsel)-1), ps.ibsel))
    denmax = max(densa(0:(ndensa(ps.ibsel)-1), ps.ibsel))
    if (ps.dival lt denmin) or (ps.dival gt denmax) then begin
        ps.dival = deref(ps.ibsel)
    endif
    pdenstring = strcompress(string(ps.dival, format='(E10.3)'),/remove_all)
    pdenbut = widget_text(pdenbutbase, xsize=9,             		  $
              /editable, value=pdenstring, font=large_font)
    dentext = "   "+strcompress(string(deref(ps.ibsel),format='(E10.3)'))
    dentext=dentext+"     "+strcompress(string(denmin,format='(E10.3)'))
    dentext=dentext+"  "+strcompress(string(denmax,format='(E10.3)'))+"   "
    pdenval = widget_label(pdenbutbase, value = dentext, font=font_small)          
                ;******************************************
                ;**** Temperature "button"             ****
                ;******************************************

    ptembutbase = widget_base(ppbase, /row, /frame)
    tem_title = "Ion Temperature (eV) "
    ptemlab = widget_label(ptembutbase, value = tem_title, font=font_small)
    temmin = min(tempa(0:(ntempa(ps.ibsel)-1), ps.ibsel))
    temmax = max(tempa(0:(ntempa(ps.ibsel)-1), ps.ibsel))
    if (ps.tival lt temmin) or (ps.tival gt temmax) then begin
        ps.tival = teref(ps.ibsel)
    endif
    ptemstring = strcompress(string(ps.tival, format='(E10.3)'),/remove_all)
    ptembut = widget_text(ptembutbase, xsize=9,             		$ 
              /editable, value=ptemstring, font=large_font)
    temtext = "   "+strcompress(string(teref(ps.ibsel),format='(E10.3)'))
    temtext = temtext + "     "+strcompress(string(temmin,format='(E10.3)'))
    temtext = temtext + "  "+strcompress(string(temmax,format='(E10.3)'))
    ptemval = widget_label(ptembutbase, value=temtext, font=font_small)

                ;******************************************
                ;**** Z Effective "button"             ****
                ;******************************************

    pzefbutbase = widget_base(ppbase, /row, /frame)
    zef_title = "Z Effective          "
    pzeflab = widget_label(pzefbutbase, value = zef_title, font=font_small)
    zefmin = min(zeffa(0:(nzeffa(ps.ibsel)-1), ps.ibsel))
    zefmax = max(zeffa(0:(nzeffa(ps.ibsel)-1), ps.ibsel))
    if (ps.zfval lt zefmin) or (ps.zfval gt zefmax) then begin
        ps.zfval = zeref(ps.ibsel)
    endif
    pzefstring = strcompress(string(ps.zfval, format='(E10.3)'),/remove_all)
    pzefbut = widget_text(pzefbutbase, xsize=9,             		$
              /editable, value=pzefstring, font=large_font)
    zeftext = "   "+strcompress(string(zeref(ps.ibsel),format='(E10.3)'))
    zeftext = zeftext + "     "+strcompress(string(zefmin,format='(E10.3)'))
    zeftext = zeftext + "  "+strcompress(string(zefmax,format='(E10.3)'))
    pzefval = widget_label(pzefbutbase, value=zeftext, font=font_small)

                ;******************************************
                ;**** B Magn.     "button"             ****
                ;******************************************

    pbmabutbase = widget_base(ppbase, /row, /frame)
    bma_title = "B Magnetic (T)       "
    pbmalab = widget_label(pbmabutbase, value = bma_title, font=font_small)
    bmamin = min(bmaga(0:(nbmaga(ps.ibsel)-1), ps.ibsel))
    bmamax = max(bmaga(0:(nbmaga(ps.ibsel)-1), ps.ibsel))
    if (ps.bmval lt bmamin) or (ps.bmval gt bmamax) then begin
        ps.bmval = bmref(ps.ibsel)
    endif
    pbmastring = strcompress(string(ps.bmval, format='(E10.3)'),/remove_all)
    pbmabut = widget_text(pbmabutbase, xsize=9,             		$
              /editable, value=pbmastring, font=large_font)
    bmatext = "   "+strcompress(string(bmref(ps.ibsel),format='(E10.3)'))
    bmatext = bmatext + "     "+strcompress(string(bmamin,format='(E10.3)'))
    bmatext = bmatext + "  "+strcompress(string(bmamax,format='(E10.3)'))
    pbmaval = widget_label(pbmabutbase, value=bmatext, font=font_small)

                ;**************************************
                ;**** Make parameters non/sensitive ***
                ;**************************************

    widget_control, ppheadlabel, sensitive=ps.ppsel
    widget_control, ppheadlabel2, sensitive=ps.ppsel
    widget_control, pdenbut, sensitive=ps.ppsel
    widget_control, pdenlab, sensitive=ps.ppsel
    widget_control, pdenval, sensitive=ps.ppsel
    widget_control, ptembut, sensitive=ps.ppsel
    widget_control, ptemlab, sensitive=ps.ppsel
    widget_control, ptemval, sensitive=ps.ppsel
    widget_control, pzefbut, sensitive=ps.ppsel
    widget_control, pzeflab, sensitive=ps.ppsel
    widget_control, pzefval, sensitive=ps.ppsel
    widget_control, pbmabut, sensitive=ps.ppsel
    widget_control, pbmalab, sensitive=ps.ppsel
    widget_control, pbmaval, sensitive=ps.ppsel

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.lfsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(topbase,/frame)
    optid   = cw_opt_value(optbase, options, 				$
			   title='Polynomial Fitting',			$
			   limits = [0,100], 				$
			   value=polyset, font=large_font 		$
			   )

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
	value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base,value='Cancel',font=large_font)
    doneid = widget_button(base,value='Done',font=large_font)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****

    new_state = { runid:runid,  					$
		  outid:outid,						$
		  messid:messid, 					$
		  deftid:deftid,					$
		  tempid:tempid, 					$
		  optid:optid,						$
		  indexid:indexid,					$
		  cancelid:cancelid,					$
		  doneid:doneid, 					$
		  dsfull:dsfull,					$
		  nbsel :nbsel, 					$
                  ibsel : ps.ibsel,					$
                  ppsel : ps.ppsel,					$
		  pbut  : pbut,						$
                  ppbase: ppbase, 					$
		  ppheadlabel:ppheadlabel,				$
		  ppheadlabel2:ppheadlabel2,				$
		  pdenbut:pdenbut,					$
		  pdenlab:pdenlab,					$
		  pdenval:pdenval,					$
		  ptembut:ptembut,					$
		  ptemlab:ptemlab,					$
		  ptemval:ptemval,					$
		  pzefbut:pzefbut,					$
		  pzeflab:pzeflab,					$
		  pzefval:pzefval,					$
		  pbmabut:pbmabut,					$
		  pbmalab:pbmalab,					$
		  pbmaval:pbmaval,					$
		  ndein:ndein,						$
		  maxe:maxe, 						$
		  evals:evals,						$
		  bmval:ps.bmval,					$
                  zfval:ps.zfval,         				$
                  tival:ps.tival,         				$
                  dival:ps.dival,         				$
		  font:font_large,					$
		  num_form:num_form					$
	        }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END
