;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas314
;
; PURPOSE    :  Generate effective CX cross sections from adf01 data.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O   TYPE     DETAILS
; INPUTS     :  adf01      I     str     original adf01 file.
;               amdon      I     double  donor mass number
;               amrec      I     double  receiver mass number
;               catyp      I     str     type of evaluation
;                                          = 'tt' thermal/thermal
;                                                 (equal temperatures for
;                                                  donor and receiver only)
;                                          = 'tr' thermal receiver,
;                                                 monoenergetic donor
;                                          = 'td' thermal donor,
;                                                 monoenergetic receiver
;                                          = 'me' special monoenergetic case
;               trd        I     double   donor energy    ( 'tr' case )
;                                         receiver energy ( 'td' case )
;               etout()    I     double   temperatures (ev) for output data set
;                                         in 'tt', 'td', 'tr' cases.
;                                         energy/amu for output data set for
;                                         'me' case.
;               log        I    str       log file (paper.txt)
;               outfile    I    str       outpur RR adf08 file
;
; KEYWORDS   :  help       I      -       Display header as help.
;               flat       I      -       if set extrapolate lower energies
;                                         by setting these to first energy.
;               zero       I      -       set energies bleow lowest in adf01
;                                         file to zero (default if none set).
;
; OUTPUTS    :  all information is returned in ADAS datasets.
;
;
; NOTES      :  run_adas314 used the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version will work.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  21-09-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1     21-09-2009
;-
;----------------------------------------------------------------------



PRO run_adas314, adf01   = adf01,   $
                 etout   = etout,   $
                 flat    = flat,    $
                 zero    = zero,    $
                 amdon   = amdon,   $
                 amrec   = amrec,   $
                 catyp   = catyp,   $
                 trd     = trd,     $
                 outfile = outfile, $
                 log     = log,     $
                 help    = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas314'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4 or v6 and above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(outfile) EQ 0 then message,'An output file is required'

; Check the input driver file

if n_elements(adf01) EQ 0 then begin

    message,'An input adf01 file is required'

endif else begin

   a01 = adf01
   file_acc, a01, exist, read, write, execute, filetype
   if exist ne 1 then message, 'adf01 file does not exist '+ a01
   if read ne 1 then message, 'adf01 file cannot be read from this userid '+ a01

endelse

if n_elements(amdon) EQ 0 then message, 'Set donor mass' else amdon = double(amdon)
if n_elements(amrec) EQ 0 then message, 'Set receiver mass' else amrec = double(amrec)

catyp = strupcase(strcompress(catyp, /remove_all))

case catyp of
   'ME' : begin
            iopt  = 0L
            ittyp = 3L
          end
   'TT' : begin
            iopt  = 1L
            ittyp = 1L
          end
   'TD' : begin
            iopt  = 2L
            ittyp = 1L
          end
   'TR' : begin
            iopt  = 3L
            ittyp = 2L
          end
   else : message, 'Set an appropriate type for processing [TT, TR, TD, ME]'
endcase

if catyp EQ 'TR' OR catyp EQ 'TD' then begin
   if n_elements(trd) EQ 0 then message, 'Set donor or receiver energy' $
                           else trd  = double(trd)
endif else trd = 0.0D0

if keyword_set(flat) EQ 1 then is_flat = 1L else is_flat = 0L
if keyword_set(zero) EQ 1 then is_zero = 1L else is_zero = 0L

if (is_flat + is_zero) EQ 2 then message, 'Set either /flat or /zero'
if (is_flat + is_zero) EQ 0 then begin
   is_zero = 1L
   message, 'Low energy extrapolation set off', /continue
endif

if is_flat EQ 1 then iextyp = 0L
if is_zero EQ 1 then iextyp = 1L

if n_elements(etout) EQ 0 then message, 'Set output energies/temperatures' $
                          else etout = double(etout)
itval = n_elements(etout)

if n_elements(log) NE 0 then begin
   log = log
   is_log = 1L
endif else begin
   log = '/dev/null'
   is_log = 0L
endelse


; Run the fortran code

date = xxdate()

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas314.out', unit=pipe, /noshell, PID=pid

; Preliminaries

printf, pipe, date[0]


; Input screen

printf, pipe, 'NO'
printf, pipe, a01


; Processing screen

nedim = -1L
ndtin = -1L

readf, pipe, nedim
readf, pipe, ndtin

evals  = dblarr(nedim)

readf, pipe, evals

printf, pipe, 0L


printf, pipe, 'run_adas314 processing', format='(a40)'
printf, pipe, amdon
printf, pipe, amrec

printf, pipe, iopt
printf, pipe, trd
printf, pipe, trd

printf, pipe, iextyp + 1L
printf, pipe, ittyp

if itval GT ndtin then message, 'Maximum number of energies/temperatures is:' +$
                                 string(ndtin, format='(i2)')

printf, pipe, itval

tin            = dblarr(ndtin)
tin[0:itval-1] = etout

for j = 0, itval-1 do printf, pipe, tin[j]

printf, pipe, 0L


; Calculation and output

printf, pipe, 0L

printf, pipe, outfile
printf, pipe, '/dev/null'
printf, pipe, log
printf, pipe, 1L
printf, pipe, 0L
printf, pipe, is_log

printf, pipe, 0L

n_nlev = -1L
idum = -1L

readf, pipe, n_nlev
for j = 0, n_nlev-1 do readf, pipe, idum
readf, pipe, idum


printf, pipe, 1L


; Finish

wait, 0.2
close, /all


END
