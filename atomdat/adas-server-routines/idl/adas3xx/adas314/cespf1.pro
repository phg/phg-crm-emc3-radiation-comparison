; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas314/cespf1.pro,v 1.1 2004/07/06 12:04:03 whitefor Exp $ Date $Date: 2004/07/06 12:04:03 $
;+
; PROJECT:
;       ADAS  
;
; NAME:
;	CESPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS314 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS314 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine CESPF1.
;
; USE:
;	The use of this routine is specific to ADAS314, See adas314.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS314 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas314.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS314_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS314 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Richard Martin        
;               Put through SCCS.
; VERSION:
;	1.1	04-12-98
;
;-
;-----------------------------------------------------------------------------
PRO   cespf1, pipe, lpend, value, header, bitfile, gomenu,           $
              FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Define some variables before read from pipe ****


                ;**************************************
                ;**** Pop-up output options widget ****
                ;**************************************

    outtitle = "ADAS314 OUTPUT OPTIONS"
    adas314_out, value, action, bitfile, FONT=font

                ;*************************************************
                ;**** Act on the output from the widget       ****
                ;**** There are three    possible actions     ****
                ;**** 'Done', 'Cancel' and 'Menu'.            ****
                ;*************************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************

    printf, pipe, lpend

    if (lpend eq 0) then begin

        printf, pipe, value.qcxdsn
        printf, pipe, value.scxdsn
        printf, pipe, value.texdsn
        printf, pipe, value.qcxout
        printf, pipe, value.scxout
        printf, pipe, value.texout

                ;**************************************************
                ;**** Set messages and settings ready for when ****
                ;**** output window is re-displayed.           ****
                ;**************************************************

        if value.qcxout eq 1 then begin
            value.qcxdef = value.qcxdsn
            if value.qcxrep ge 0 then value.qcxrep = 0
            value.qcxmes = 'Output written to file.'
        endif

        if value.scxout eq 1 then begin
            value.scxdef = value.scxdsn
            if value.scxrep ge 0 then value.scxrep = 0
            value.scxmes = 'Output written to file.'
        endif
        
        if value.texout eq 1 then begin
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
    endif



END
