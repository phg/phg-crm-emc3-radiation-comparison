; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas314/cw_adas314_proc.pro,v 1.3 2004/07/06 12:42:40 whitefor Exp $ Date $Date: 2004/07/06 12:42:40 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	CW_ADAS314_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS314 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an edieable 'Run title', 
;	   the dataset name/browse widget ,
;	   a widget to set the reactant masses,
;	   a button group widget to select type of thermal averaging required,
;	   a table widget for energies or temperature data ,
;	   a button to enter default values into the table, 
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;       Depending on the averaging choice the title of the table entry 
;       widget changes and an entry box for donor or recv temperature
;       is shown or hidden.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the 'Defaults' button,
;	the 'Cancel' button and the 'Done' button and the 
;       'escape to series menu' button.
;
;
; USE:
;	This widget is specific to ADAS314, see adas314_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NEDIM   - Integer : Maximum number of energies from data file.
;
;	NDEIN	- Integer : Maximum number of user energies allowed.
;
;
;	EVALS	- Double array : Electron energies for each block
;				 1st dimension - energy index
;				 2nd dimension - units 1 => cm s-1
;						       2 => at. units
;						       3 => eV/amu
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;	
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;        		ps = { 	nmet   	: 	0, 			$
;				title 	: 	'',            		$
;				amd 	: 	2.0,			$
;				amr 	: 	2.0,			$
;				iopt 	: 	0,			$
;				t_don   :	3.0,			$
;				t_rec   :	3.0,			$
;				itval 	: 	ndtin,			$
;				ittyp 	: 	3,			$
;                		tin  	:	temp_arr		}
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		AMD     Donor atomic mass number
;		AMR     Receiver atomic mass number
;		IOPT    Type of averaging
;                         0 : monoenergetic    (to regrid adf01 data)
;                         1 : Thermal-Thermal  (both reactants same temp)
;                         2 : Thermal donor    (with single temp receiver)
;                         3 : Thermal receiver (with single temp donor)
;		T_DON   Temp of donor for iopt=3
;		T_REC   Temp of donor for iopt=2
;		TIN     User supplied energy/temp values.
;		ITVAL   Number of user temperature values entered
;		ITTYP   Index indicating which temp. units are being used
;		TIN     User supplied temperature values for fit.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS314 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_BGROUP	IDL library widget for button groups.
;
;	See side effects for other related widget management roueins.
;
; SIDE EFFECTS:
;
;	The following widget management roueins are included in this file;
;	PROC314_GET_VAL()	Returns the current PROCVAL structure.
;	PROC314_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Richard Martin        
;               Put through SCCS.
;	  1.2	    Martin O'Mullane
;	  1.3	    ichard Martin
;		Changed widget name switch to switchb for IDL 5.4 compatibility.
;
; VERSION:
;	1.1	04-12-98
;	1.2	24-03-99
;	1.3	10-11-00
;
;-----------------------------------------------------------------------------

FUNCTION proc314_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre it in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  
		;******************************************
		;**** Get reactant mass parameters     ****
		;******************************************

    widget_control, state.amdid, get_value=amdval
    if strcompress(amdval(0), /remove_all) eq '' then begin
  	amd =2.0
    endif else begin
	amd = float(amdval(0))
    endelse

    widget_control, state.amrid, get_value=amrval
    if strcompress(amrval(0), /remove_all) eq '' then begin
  	amr =2.0
    endif else begin
	amr = float(amrval(0))
    endelse

		;*************************************
		;**** Get type of thermal average ****
		;*************************************

    widget_control, state.ioptid, get_value=iopt
   

		;***********************************
		;**** Get type of extrapolation ****
		;***********************************

    widget_control, state.exid, get_value=iextyp
               
                ;**********************************
                ;**** Get new donor/rec temp.  ****
                ;**********************************

; NB: get both to ensure that t_don and t_rec are defined as well as
;     to keep both values between sessions.
 
    widget_control, state.donorid, get_value=a_temp_value
    if (num_chk(a_temp_value(0)) eq 0) then begin
        t_rec = float(a_temp_value(0))
    endif else begin
        t_rec = -99.0
    endelse
    widget_control, state.donorid, set_value=a_temp_value

    widget_control, state.recvid, get_value=a_temp_value
    if (num_chk(a_temp_value(0)) eq 0) then begin
        t_don = float(a_temp_value(0))
    endif else begin
        t_don = -99.0
    endelse
    widget_control, state.recvid, set_value=a_temp_value
		
                
                ;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    tempdata = tempval.value
    ittyp = tempval.units+1

	    ;**** Copy out temperature values ****

    tin = dblarr(state.ndtin)
    blanks = where(strtrim(tempdata(0,*),2) eq '')

            ;***********************************************
	    ;**** next line assumes that all blanks are ****
	    ;**** at the end of the columns             ****
            ;***********************************************

    if blanks(0) ge 0 then itval=blanks(0) else itval=state.ndtin

            ;*************************************************
            ;**** Only perform following if there is 1 or ****
            ;**** more value present in the table         ****
            ;*************************************************

    if itval ge 1 then begin
        tin(0:itval-1) = double(tempdata(0,0:itval-1))
    endif

	    ;**** Fill out the rest with zero ****

    if itval lt state.ndtin then begin
        tin(itval:state.ndtin-1) = double(0.0)
    endif

  
		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = {	nmet   	:  	0, 					$
                title 	: 	title,          			$
		amd 	: 	amd,					$
		amr 	: 	amr,					$
		iopt 	: 	iopt,					$
		t_don   :	t_don,					$
		t_rec   :	t_rec,					$
		itval 	: 	itval,					$
		iextyp 	: 	iextyp,					$
		ittyp 	: 	ittyp,					$
                tin  	: 	tin       				}
   
     widget_control, first_child, set_uvalue=state, /no_copy

     RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc314_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;*************************************
		;****   select donor mass number  ****
		;*************************************

        state.amdid: begin
            widget_control, state.amdid, get_value=amdval
            if strcompress(amdval(0), /remove_all) eq '' then begin
  	        amd =2.0
            endif else begin
	        amd = float(amdval(0))
            endelse

            neval = state.neval

		;**** Get current temperature table widget value ****

 	    widget_control, state.tempid, get_value=tempval

		;**** Copy new  values into structure  ****
 
	    if (neval gt 0) then begin    
    	        tempval.value(1,0:neval-1 ) = 				$
	        strtrim(string(amd*state.evals(0:neval-1), 	  	$
	        format=state.num_form),2)

		;**** Copy new data to table widget ****
   
 	        widget_control, state.tempid, set_value=tempval

  	    endif

	  	;**** reset state.amd             ****

	    state.amd = amd
	end

		;****************************************
		;****   select receiver mass number  ****
		;****************************************

        state.amrid: begin
            widget_control, state.amrid, get_value=amrval
            if strcompress(amrval(0), /remove_all) eq '' then begin
  	        amr =2.0
            endif else begin
	        amr = float(amrval(0))
            endelse

            neval = state.neval

		;**** Get current temperature table widget value ****

 	    widget_control, state.tempid, get_value=tempval

		;**** Copy new  values into structure  ****
 
	    if (neval gt 0) then begin    
                tempval.value(2,0:neval-1) = 				$
                strtrim(string(amr*state.evals(0:neval-1),   		$
                format=state.num_form),2)

		;**** Copy new data to table widget ****
   
 	        widget_control, state.tempid, set_value=tempval

  	    endif

	  	;**** reset state.amr ****

	    state.amr = amr
	end

		;****************************************
		;**** change of thermal average type ****
		;****************************************

        state.ioptid: begin
 
            widget_control, state.ioptid, get_value=iopt
            widget_control, state.typeid, set_value=state.iopt_type(iopt)
		
                ;******************************************
                ;**** map donor/rec temp if necessary  ****
		;******************************************
             
            case iopt of
                 2: begin
                       widget_control, state.donorid, map=1
                       widget_control, state.recvid,  map=0
                    end
                 3: begin
                       widget_control, state.donorid, map=0
                       widget_control, state.recvid,  map=1
                    end
              else: begin
                       widget_control, state.donorid, map=0
                       widget_control, state.recvid,  map=0
                    end
            endcase            
 
	end

		;*****************************************
		;**** Default temperatures button     ****
		;*****************************************

        state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults', $
			  buttons=['Confirm','Cancel'], font=state.font)

	    if action eq 'Confirm' then begin

		;**** Get current temp. widget value ****

 	   	widget_control,state.tempid,get_value=tempval

		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****

           	units = tempval.units+1
	   	neval = state.neval
 	   	if neval gt 0 then begin
   	            tempval.value(0,0:neval-1) =   			$
	            strtrim(string(tempval.value(units,0:neval-1), 	$
	            format=state.num_form),2)
 	   	endif

		;**** Fill out the rest with blanks ****

 	    	if neval lt state.ndtin then begin
   	            tempval.value(0,neval:state.ndtin-1) = ''
 	    	endif

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval
	    endif
        end
		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc314_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy

	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy


		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
			     HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

                ;**** Menu button ****

      	state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
      	end


      	ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas314_proc, topparent, dsfull, act,  			$
			  nedim, evals, ndtin, 				$
                	  bitfile, procval=procval, 			$
			  uvalue=uvalue, font_large=font_large, 	$
			  font_small=font_small, edit_fonts=edit_fonts, $
  			  num_form=num_form


		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
           edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
   
    if not (keyword_set(procval)) then begin
   
        temp_arr = fltarr(ndtin)
        ps = { 	nmet   	: 	0, 					$
		title 	: 	'',             			$
		amd 	: 	2.0,					$
		amr 	: 	2.0,					$
		iopt 	: 	0,					$
		t_don   :	3.0,					$
		t_rec   :	3.0,					$
		itval 	: 	ndtin,					$
		iextyp 	: 	1,					$
		ittyp 	: 	3,					$
                tin  	:	temp_arr				}
                
	if (nedim le ndtin) then begin
	    ps.tin(0:nedim-1)=evals(*,2,0)
	endif else begin
	    ps.tin = evals(0:ndtin-1,2,0)
	    print,'cw_adas314_proc.pro : truncating energy list for '+	$
		  'temperature values, too many values'
	endelse

    endif else begin
 
 	 ps = {  nmet   : 	procval.nmet,   			$
                 title 	: 	procval.title,  			$
		 amd 	: 	procval.amd,				$
		 amr 	: 	procval.amr,				$
                 iopt 	: 	procval.iopt,	  			$
		 t_don  :	procval.t_don,				$
		 t_rec  :	procval.t_rec,				$
                 itval 	: 	procval.itval,  			$
                 iextyp : 	procval.iextyp,  			$
                 ittyp 	: 	procval.ittyp,  			$
                 tin  	: 	procval.tin   				}
    endelse

    
    iopt_names = ['Mono-energetic','Thermal-Thermal ',   $
                  'Thermal Donor ','Thermal Receiver']
                
    iopt_type = ['Enter Output Particle Energies', $
                 'Enter Temperatures            ', $
                 'Enter Donor Temperatures      ', $
                 'Enter Receiver Temperatures   '  ]
      
    ex_names = ['Set to first value in adf01 file ', 'Set to 0.0']
                


		;****************************************************
		;****       Assemble temperature table data      ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;****     Declare temp.table array              *****
		;**** col 1 has user temperature values         *****
		;**** col 2-4 have temp. values from files,     *****
		;**** which can have one of three possible units*****
		;****************************************************

    neval = n_elements(evals)
    itval = neval
    tvals = dblarr(nedim,3)
    tempdata = strarr(4,ndtin)

		;**** Copy out temperature values      ****
		;**** number of temperature            ****
		;**** values for this data block       ****

    tvals(*,0) = ps.amd*evals(*)	
    tvals(*,1) = ps.amr*evals(*)	
    tvals(*,2) = evals(*)	
 

    if (neval gt 0) then begin
        tempdata(0,*) = strtrim(string(ps.tin,format=num_form),2)
    	tempdata(1,0:neval-1 ) = 					$
	strtrim(string(tvals(0:neval-1,0),format=num_form),2)
        tempdata(2,0:neval-1) = 					$
	strtrim(string(tvals(0:neval-1,1),format=num_form),2)
        tempdata(3,0:neval-1) = 					$
        strtrim(string(tvals(0:neval-1,2),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(tempdata eq 0.0)
	if (blanks(0) ge 0) then begin
	    tempdata(blanks) = ' '
	endif
    endif

		;********************************************************
		;**** Create the 314 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS314 PROCESSING OPTIONS',		$
			 EVENT_FUNC = "proc314_event", 			$
			 FUNC_GET_VALUE = "proc314_get_val", 		$
			 /COLUMN, XOFFSET=1, YOFFSET=1)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=font_large)
    runid = widget_text(base, value=ps.title, xsize=38, 		$
		        font=font_large, /edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=font_large)


		;**********************
		;**** Another base ****
		;**********************

    parambase = widget_base(parent, /row, /frame)

		;********************************
		;**** add parameter typeins ****
		;********************************

    paramhead = widget_label(parambase, font=font_large,		$
		value="Enter the reactant mass numbers    ")
    amdlab    = widget_label(parambase, font=font_small, value="Donor: ")
    amdval    = strtrim(string(ps.amd, format='(f5.1)'),2)
    amdid     = widget_text(parambase, font=font_small, /editable,	$
		value=amdval, xsize=7)		
    amrlab    = widget_label(parambase, font=font_small, value="Receiver: ")
    amrval    = strtrim(string(ps.amr, format='(f5.1)'),2)
    amrid     = widget_text(parambase, font=font_small, /editable,	$
		value=amrval, xsize=7)

		
		;************************************
		;**** Add the run type selector ****
		;************************************

    ioptid = cw_bgroup(parent,iopt_names,exclusive=1,row=2,			$
                       label_top='Select form of thermal average :',	$
                       font=font_large)


		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent, /row, /frame)

           


		;****************************
		;**** table of data   *******
		;****************************

    base = widget_base(tablebase, /column, /frame)
 
		;*******************************************************
		;**** base for the temp.  table and defaults button ****
		;**** and whether table used for text output        ****
		;*******************************************************

    lbase = widget_base(base, /row)
    typeid = widget_label(lbase, value=iopt_type(ps.iopt), $
			  font = font_large)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input'], ['','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ittyp-1 
    unitname = ['eV(don.)','eV(rec.)','eV/amu']

		;************************************************************
		;**** Two  columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three         ****
		;**** units but column 2 switches between sets 2,3 & 4   ****
		;**** in the input array enerdata as the units change.  ****
		;************************************************************

    unitind = [[0,0,0],[1,2,3]]
    table_title =  ['Output Values'] 

		;****************************
		;**** table of data   *******
		;****************************
                
    tempid = cw_adas_table(base, tempdata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0], COLHEAD = colhead, 		$
			   TITLE = table_title, ORDER = [1,0], 		$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, YTABSIZE = 7, NUM_FORM = num_form,	$
			   FONTS = edit_fonts, FONT_LARGE = font_large, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default button  ****
		;*************************

    deftid = widget_button(base,value=' Default Values ', font=font_large)


		;************************************************
                ; **** Base for iextyp (extrapolation type). **** 
		;************************************************
                
     exbase  = widget_base(tablebase,/column)
     
     
     exid = cw_bgroup(exbase,ex_names,exclusive=1,row=2,			$
                      label_top='Extrapolation to low energies:',	$
                      font=font_large,/frame)




		;***********************************************************
                ; **** Base for iopt choice inputs - map/unmap these as **** 
                ; **** appropriate and not the individual widgets held  ****
                ; **** within them                                      ****
		;***********************************************************
        
     switchb  = widget_base(exbase)
     donbase = widget_base(switchb,/row)
     recbase = widget_base(switchb,/row)

; Thermal Donor => monoenergetic receiver
     if (ps.t_rec ne 0.0) then begin
         fval = string (ps.t_rec, format='(d10.3)')
     endif else begin
         fval = "          "
     endelse
     donlab  = widget_label(donbase, value = 'Enter Receiver Temp. (eV) :', $
                            font=font_small)
     donorid = widget_text(donbase, xsize=10,/editable, $
                        value = fval, font=font_small)

; Thermal Receiver => monoenergetic donor
     if (ps.t_don ne 0.0) then begin
         fval = string (ps.t_don, format='(d10.3)')
     endif else begin
         fval = "          "
     endelse
     reclab  = widget_label(recbase, value = 'Enter Donor Temp. (eV) :', $
                            font=font_small)
     recvid = widget_text(recbase, xsize=10,/editable, $
                        value = fval, font=font_small)



		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(parent,font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)
  

		;*****************************************
                ;**** set extrapolation to first type ****
		;*****************************************
         
    widget_control, exid, set_value = ps.iextyp

		;********************************************
                ;**** set buttons and options depending  ****
                ;**** on the type of average selected    ****
		;********************************************
         
    widget_control, ioptid, set_value = ps.iopt 
	
        
		;******************************************
                ;**** map donor/rec temp if necessary  ****
		;******************************************
             
    case ps.iopt of
         2: begin
               widget_control, donorid, map=1
               widget_control, recvid,  map=0
            end
         3: begin
               widget_control, donorid, map=0
               widget_control, recvid,  map=1
            end
      else: begin
               widget_control, donorid, map=0
               widget_control, recvid,  map=0
            end
    endcase
            
            
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************


    new_state = { 	runid		:	runid,  		$
			messid		:	messid, 		$
			amdid		:	amdid,			$
			amrid		:	amrid,			$
			deftid		:	deftid,			$
			tempid		:	tempid, 		$
			cancelid	:	cancelid,		$
			doneid		:	doneid, 		$
	        	outid		:	outid,			$
			dsfull		:	dsfull,			$
			amd 		:	ps.amd,			$
			amr 		:	ps.amr, 		$
		  	ioptid		: 	ioptid,			$
			iopt		:	ps.iopt,		$
		  	exid		: 	exid,			$
			iextyp		:	ps.iextyp,		$
		  	typeid		: 	typeid,			$
    			iopt_type	:	iopt_type,		$	
		  	donorid		: 	donorid,		$
		  	recvid		: 	recvid,			$
    			neval		:	neval,			$
			evals		:	evals,			$
			itval		:	itval, 			$
			tvals		:	tvals,			$
			font		:	font_large,		$
			nedim		:	nedim,			$
			ndtin		:	ndtin,			$
			num_form	:	num_form		}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END
