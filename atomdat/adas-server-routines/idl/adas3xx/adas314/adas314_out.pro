; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas314/adas314_out.pro,v 1.1 2004/07/06 10:37:01 whitefor Exp $ Date $Date: 2004/07/06 10:37:01 $
;+
; PROJECT:
;       ADAS  
;
; NAME:
;       ADAS314_OUT
;
; PURPOSE:
;       IDL ADAS user interface, output options.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select options and input data to control ADAS314
;       file output.
;
; USE:
;       This routine is ADAS314 specific, see cespf1.pro for how it
;       is used.
;
; INPUTS:
;       VAL     - A structure which determines the initial settings of
;                 the output options widget.  The value is passed
;                 unmodified into cw_adas314_out.pro.
;
;                 See cw_adas314_out.pro for a full description of this
;                 structure.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       ACT     - String; 'Done', 'Cancel' or 'Menu' for the button the
;                 user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS
;       FONT    - Supplies the font to be used for the interface widgets.
;
; CALLS:
;       CW_ADAS314_OUT  Creates the output options widget.
;       DYNLABEL        Recursively adds dynamic_resize keyword to label
;                       widgets for version 4.0.1 and higher of IDL
;       XMANAGER
;       See side effects for widget management routine.
;
; SIDE EFFECTS:
;       This routine uses a common block OUT314_BLK to maintain its state.
;       ADAS314_OUT_EV  is included in this file and is called
;                       indirectly from XMANAGER during widget
;                       management.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Richard Martin        
;               Put through SCCS.
; VERSION:
;	1.1	04-12-98
;
;-
;-----------------------------------------------------------------------------

PRO adas314_out_ev, event

    COMMON out314_blk, action, value
        

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action of

                ;**** 'Done' button ****

        'Done'  : begin

                ;**** Get the output widget value ****

            child = widget_info(event.id,/child)
            widget_control, child, get_value=value 

                ;*****************************************
                ;**** Kill the widget to allow IDL to ****
                ;**** continue and interface with     ****
                ;**** FORTRAN only if there is work   ****
                ;**** for the FORTRAN to do.          ****
                ;*****************************************

            if (value.qcxout eq 1) or (value.scxout eq 1) then begin
                widget_control, event.top, /destroy
            endif 
        end

                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

        ELSE:   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas314_out, val, act, bitfile, font=font

    COMMON out314_blk, action, value

                ;**** Copy value to common ****

    value = val

                ;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''

                ;***************************************
                ;**** Pop-up a new widget           ****
                ;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS314 OUTPUT OPTIONS', xoffset=100,    $
                        yoffset=1)

                ;**** Declare output options widget ****

    cwid = cw_adas314_out(outid,  bitfile, value=value,          $
                          font=font )

                ;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

                ;**** make widget modal ****

    xmanager, 'adas314_out', outid, event_handler='adas314_out_ev',     $
              /modal, /just_reg
 
                ;**** Return the output value from common ****

    act = action
    val = value

END
