; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas314/ceprogbar.pro,v 1.1 2004/07/06 12:03:47 whitefor Exp $ Date $Date: 2004/07/06 12:03:47 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CEPROGBAR
;
; PURPOSE:
;	IDL user interface and communications with ADAS314 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine creates a progress indicator which is updated as
;	the FORTRAN progresses. The FORTRAN communicates via a pipe
;	the total number of stages and the stage which has been reached.
;
; USE:
;	The use of this routine is specific to ADAS314. See adas314.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS314 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS314 FORTRAN processes
;	via UNIX pipes.
;	It also pops up an information widget which keeps the user 
;	updated as the calculation progresses.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Richard Martin        
;               Put through SCCS.
; VERSION:
;	1.1	04-12-98
;
;-
;-----------------------------------------------------------------------------

PRO ceprogbar, pipe, FONT=font


                ;**** If there is an io error caused ****
		;**** by the Fortran crashing handle it ****

    ON_IOERROR, PIPE_ERR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create an information widget ****

    widget_control, /hourglass
    base  = widget_base(/column, xoffset=300, yoffset=200,           $
                        title = "ADAS314: INFORMATION")
    lab0  = widget_label(base, value='')
    lab1  = widget_label(base,  font=font,                                         $
              value='   ADAS314 Computation Underway - Please Wait   ')
    
                ;**** Size of the bar is hardwired ****
                ;**** Centre it in its own base    ****
   
    barbase = widget_base(base,/align_center)
    
    grap  = widget_draw(barbase, ysize=20, xsize=480) 
    
    lab2  = widget_label(base, value = ' ')
    
    
    dynlabel, base

    widget_control, base, /realize

    num = 0
    readf, pipe, num

   
                ;*********************************************
                ;**** The time scales linearly in this    ****
		;**** calculation. Update the information ****
                ;**** widget in stages.                   ****
                ;*********************************************
                
    next = 0
    step = 1000.0/num
             

    for i=0,num-1 do begin
       
       readf, pipe, next
       p = float(i)/num*1000   
       q = p+step              
       
       for j=fix(p),fix(q) do begin
           x = (float(j)+1)/1000.0
           plots, [x, x],[0.0,1.0], /normal
       endfor
       widget_control, lab2, set_value='PROCESSING '+                  $
                string(fix(q/10),format='(I3)')+'% COMPLETED'
    endfor

    readf, pipe, next


    goto, DONE


PIPE_ERR:
    mess = ['ADAS314 has crashed.',$
            '  ',$
            'Check that all inputs are correct.']
    action = popup(message=mess, title = '*** ADAS314 Error ***',$
		 buttons=['OK'])

DONE:
		;**** Destroy the information widget ****

    widget_control, base, /destroy

END
