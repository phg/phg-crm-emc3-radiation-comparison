; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas314/ceispf.pro,v 1.2 2004/07/06 12:03:38 whitefor Exp $ Date $Date: 2004/07/06 12:03:38 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CEISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS314 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS314
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS314
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	CDISPF.
;
; USE:
;	The use of this routine is specific to ADAS314, see adas314.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS314 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS314 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas314.pro.  If adas314.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  See cw_adas314_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS314 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS314_PROC	Invoke the IDL interface for ADAS314 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS314 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Richard Martin        
;               Put through SCCS.
;	  1.2	    Martin O'Mullane
; VERSION:
;	1.1	04-12-98
;	1.2	24-03-99
;-
;-----------------------------------------------------------------------------

PRO ceispf, pipe, lpend, procval, dsfull,  gomenu, bitfile,		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays will be declared after sizes****
		;**** have been read.                    ****
                ;********************************************

    lpend = 0
    nedim = 0
    ndtin = 0
    
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    nedim = input
    readf, pipe, input
    ndtin = input

                ;******************************************
                ;**** Now can define other array sizes ****
                ;******************************************

    tvals  = dblarr(nedim)
    evals  = dblarr(nedim)

		;********************************
                ;**** Read data from fortran ****
                ;********************************

    next_item = 0.0


    for i = 0, nedim-1 do begin
        readf, pipe, next_item
        evals(i) = next_item
    endfor

		;********************************************
		;**** Set default value if none provided ****
		;********************************************

    if (procval.nmet lt 0) then begin
         temp_arr = fltarr(ndtin)
         procval = {	nmet 	: 	0 ,             		$
			title 	: 	'',				$
       			amd     : 	2.0,		                $
			amr     :	2.0, 				$
			iopt  	: 	0,              		$
			t_don   :	3.0,				$
			t_rec   :	3.0,				$
			itval  	: 	0,              		$
			iextyp  : 	1,              		$
			ittyp  	: 	1,              		$
			tin   	: 	temp_arr			}
   endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas314_proc, procval, dsfull, nedim, evals,  ndtin,  		$
		  action, bitfile,					$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;********************************************
		;****  Act on the event from the widget  ****
                ;**** There are three  possible  actions ****
                ;**** 'Done', 'Cancel'and 'Menu'.        ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.amd
    printf, pipe, procval.amr

    printf, pipe, procval.iopt
    printf, pipe, procval.t_don
    printf, pipe, procval.t_rec

    printf, pipe, procval.iextyp + 1      ; the old fortran/idl starting index
    printf, pipe, procval.ittyp
    printf, pipe, procval.itval

    for j = 0, procval.itval-1 do begin
        printf, pipe, procval.tin(j)
    endfor


END
