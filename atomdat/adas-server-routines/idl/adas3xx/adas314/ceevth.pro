;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  ceevth
;
; PURPOSE    : Generates rate coefficients for donor/receiver charge
;              exchange collisions for cases of
;              monoenergetic donor/thermal receiver, thermal
;              donor/monoenergetic receiver, thermal donor/thermal
;              receiver (same temperature) from cross-section tabulations.
;              A mono-energetic case is allowed which converts input
;              cross-sections tabulated at a set of energies/amu to
;              output cross-sections tabulated a different set of
;              energies/amu.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                ceevth, ndenr   = ndenr,   $
;                        amdon   = amdon,   $
;                        amrec   = amrec,   $
;                        catyp   = catyp,   $
;                        dren    = dren,    $
;                        iextyp  = iextyp,  $
;                        enin    = enin ,   $
;                        enout   = enout,   $
;                        sgin    = sgin,    $
;                        rcout   = rcout,   $
;                        help    = help
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  z0         I     double  nuclear charge
;               ndenr      I     long    max. number of energies/temperatures
;                                        in input/output energy/temperature
;                                        vectors  
;                                        (defaults to max of ein or eout elements)
;               amdon      I     double  donor mass number
;               amrec      I     double  receiver mass number
;               catyp      I     str     type of evaluation
;                                          = 'tt' thermal/thermal
;                                                 (equal temperatures for
;                                                  donor and receiver only)
;                                          = 'tr' thermal receiver,
;                                                 monoenergetic donor
;                                          = 'td' thermal donor,
;                                                 monoenergetic receiver
;                                          = 'me' special monoenergetic case
;               dren       I     double   donor energy    ( 'tr' case )
;                                         receiver energy ( 'td' case )
;               iextyp     I     long     extrapolation method
;                                            = 1 set lower energies to first point
;                                                in data
;                                            = 2 set lower energies to 0.0
;               enin()     I     double   energies (ev/amu) in input data set
;               enout()    I     double   temperatures (ev) for output data set
;                                         in 'tt', 'td', 'tr' cases.
;                                         energy/amu for output data set for
;                                         'me' case.
;               sgin()     I     double   input x-sections (cm2) from input
;                                         data set
;               rcout()    O     double   rate coefficient (cm3 s-1)
;
;
; KEYWORDS      help : Get these comments as help.
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  27/03/06
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Make sure ndenr is large enough for internal
;                 splining routine in fortran.
;
;
; VERSION:
;       1.1    27/03/2006
;       1.2    03/05/2007
;
;-
;----------------------------------------------------------------------

PRO ceevth, ndenr   = ndenr,   $
            amdon   = amdon,   $
            amrec   = amrec,   $
            catyp   = catyp,   $
            dren    = dren,    $
            iextyp  = iextyp,  $
            enin    = enin ,   $
            enout   = enout,   $
            sgin    = sgin,    $
            rcout   = rcout,   $
            help    = help

; If we ask for help

if keyword_set(help) then begin
   doc_library, 'ceevth'
   return
endif

fortdir = getenv('ADASFORT')

; Check inputs and set defaults

if n_elements(amdon) EQ 0 then message, 'Set donor mass' else amdon = double(amdon)
if n_elements(amrec) EQ 0 then message, 'Set receiver mass' else amrec = double(amrec)

if n_elements(catyp) EQ 0 then begin
   message, 'Set processing to thermal-thermal', /continue
   catyp = 'TT'
endif

catyp = strupcase(strcompress(catyp, /remove_all))

if catyp NE 'TT' AND catyp NE 'TR' AND catyp NE 'TD'  AND catyp NE 'ME' then begin
   message, 'Set an appropriate type for processing [TT, TR, TD, ME]'
endif

if catyp EQ 'TR' OR catyp EQ 'TD' then begin
   if n_elements(dren) EQ 0 then message, 'Set donor or receiver energy' $
                            else dren  = double(dren)
endif else dren = 0.0D0

if n_elements(iextyp) EQ 0 then begin
   message, 'Assume energies lower than minumum in adf are set to first point', /continue
   iextyp = 1L
endif else iextyp = long(iextyp)

if n_elements(enin) EQ 0 then message, 'Set input energies' else enin = double(enin)
if n_elements(sgin) EQ 0 then message, 'Set input cross sections' else sgin = double(sgin)
if n_elements(enout) EQ 0 then message, 'Set output energies' else enout = double(enout)

nenin   = long(n_elements(enin))
nenout  = long(n_elements(enout))

; Now we can set ndenr

if n_elements(ndenr) EQ 0 then begin
   ndenr = max([nenin, nenout])
endif else begin
   ndenr = long(ndenr)
   if ndenr LT nenin OR ndenr LT nenout then message, 'Set ndenr higher'
endelse


il_lsetx = 1L
il_lpass = 0L
iltyp    = 0L
rate     = dblarr(nenout)

dummy = CALL_EXTERNAL(fortdir+'/ceevth_if.so','ceevth_if',       $
                      ndenr, il_lsetx, il_lpass, amdon, amrec,   $
                      dren, iltyp, iextyp, nenin, enin,          $
                      nenout, enout, sgin, rate, catyp)

if arg_present(rcout) then rcout = rate

END
