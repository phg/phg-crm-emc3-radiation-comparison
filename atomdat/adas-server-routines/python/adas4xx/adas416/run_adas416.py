def run_adas416(file=None, logfile=None):

    """

      PURPOSE    :  Runs the adas416 adf11-bundling code as a python function

      frac_p, frac_c = run_adas416(file='/home/adas/adas/scripts416/partition_02_argon_89.dat')

                    NAME       TYPE     DETAILS
      REQUIRED   :  script     str      script file to drive the program
                    logfile    str      text log file, defaults to no output file

      RETURNS    :  frac_p     dict     abundances from original adf11 data
                    frac_c     dict     abundances of bundled adf11 data

                    The frac dictionaries are:

                    frac       dict    { 'te'    - temperature array from adf11 file (eV)
                                         'dens'  - density arry from adf11 file (cm-3)
                                         'stage' - array of ion/metastable/bundle descriptors
                                         'ion'   - fractional abundances of stages
                                                      1st dim: te index
                                                      2nd dim: density
                                                      3rd dim: stages/bundle index

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    03-04-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    import os
    import subprocess
    from datetime import datetime
    from numpy import asarray, zeros, chararray
    from adaslib import adas_write_pipe, xxscript_416


    # Initialisation and setup

    d     = datetime.now()
    today = d.strftime("%d/%m/%y")

    fortran_code = os.getenv('ADASFORT') + '/adas416.out'


    # Examine inputs

    if file == None:
        raise Exception('A valid script/driver file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
        raise Exception('A valid script/driver file is required')


    # Launch fortran and interact with it

    pipe = subprocess.Popen(fortran_code,
           shell=False, bufsize=0,
           stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)


    # First (input) panel

    adas_write_pipe(pipe, today)

    adas_write_pipe(pipe, 1)    # we want the child fractional abundances
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, "NO")
    adas_write_pipe(pipe, file)


    # Calculation underway

    print("Calculation underway")

    line = pipe.stdout.readline()
    nloops = int(line)

    for j in range(nloops):
        line = pipe.stdout.readline()


    # Graphical confrmation screen

    line = pipe.stdout.readline()
    itmax_p = int(line)
    line = pipe.stdout.readline()
    idmax_p = int(line)
    line = pipe.stdout.readline()
    num_p = int(line)
    line = pipe.stdout.readline()
    num_c = int(line)

    te      = asarray(zeros(itmax_p), float)
    dens    = asarray(zeros(idmax_p), float)
    fabun_p = asarray(zeros((itmax_p, idmax_p, num_p), float))
    fabun_c = asarray(zeros((itmax_p, idmax_p, num_c), float))

    for j in range(itmax_p):
        line = pipe.stdout.readline()
        te[j] = float(line)

    for j in range(idmax_p):
        line = pipe.stdout.readline()
        dens[j] = float(line)

    for it in range(itmax_p):
        for id in range(idmax_p):
            for istg in range(num_p):
                line = pipe.stdout.readline()
                fabun_p[it,id,istg] = float(line)
            for istg in range(num_c):
                line = pipe.stdout.readline()
                fabun_c[it,id,istg] = float(line)

    line = pipe.stdout.readline()
    n_lab = int(line)

    label = chararray(n_lab,80)
    #label[:] = ' '
    for j in range(n_lab):
        line = pipe.stdout.readline()
        label[j] = line

    adas_write_pipe(pipe, "CONTINUE")



    # Output screen

    line = pipe.stdout.readline()
    n_class = int(line)

    a11_files = chararray(n_class, 120)
    for j in range(n_class):
        line = pipe.stdout.readline()
        a11_files[j] = line



    # Finish interaction with fortran

    adas_write_pipe(pipe, "YES")
    adas_write_pipe(pipe, "CANCEL")

    line = pipe.stdout.readline()



    # Construct the stage information from partition description

    all = xxscript_416(file=file)

    res_p = all['iptnla'][0:all['nptnl']].max()
    res_c = all['iptnla'][0:all['nptnl']].min()

    ind_p   = res_p-1
    num_p   = all['nptn'][ind_p]
    label_p = []

    for j in range(num_p):
        str = ''
        for k in range(all['nptnc'][ind_p,j]): str = str + "{:02d}".format(all['iptnca'][ind_p, j, k]) + ' '
        label_p.append(str.strip())

    ind_c   = res_c-1
    num_c   = all['nptn'][ind_c]
    label_c = []

    for j in range(num_c):
        str = ''
        for k in range(all['nptnc'][ind_c,j]): str = str + "{:02d}".format(all['iptnca'][ind_c, j, k]) + ' '
        label_c.append(str.strip())


    # Construct, and return, the output abundance dictionaries

    frac_p = { 'te'    : te,
               'dens'  : dens,
               'stage' : label_p,
               'ion'   : fabun_p  }

    frac_c = { 'te'    : te,
               'dens'  : dens,
               'stage' : label_c,
               'ion'   : fabun_c  }


    return frac_p, frac_c
