def run_adas405(uid='adas', year=None, defyear=89, filter=None, elem=None,
                files=None, partial=False, mh=1.0, mz=1.0,
                te=[0.0], dens=[0.0], unit_te='ev', all=False,
                tion=[0.0], denh=[0.0], cx=False, logfile=None):

    """

      PURPOSE    :  Runs the adas405 equilibrium balance code as a python function

      frac, power = run_adas405(elem=elem, uid=uid, year=year, dens=dens, te=te, all=True, ....)

                    NAME       TYPE     DETAILS
      REQUIRED   :  uid        str      username of adf11 location. Defaults
                                        to 'adas' for central ADAS data.
                    year       int      year of adf11 data.
                    defyear    int      default year of adf11 data
                                        (defaults to 89 if not set).
                    filter     str      filter (if appropriate).
                    elem       str      element
                    te         float()  temperatures requested
                    dens       float()  densities requested
                    tion       float()  ion temperatures requested
                                        (if cx=True is used).
                    denh       float()  ion densities requested
                                        (if cx=True is used).
                    mh         float    Hydrogen isotope mass
                                        (if cx=True is used).
                    mz         float    Element isotope mass
                                        (if cx=True is used).
                    log        str      name of output text file.
                                        (defaults to no output).
                    files      dict     Optional dictionary with replacement
                                        names for individula files.
                                               {'acd'  : '',  $
                                                'scd'  : '',  $
                                                'ccd'  : '',  $
                                                'prb'  : '',  $
                                                'prc'  : '',  $
                                                'qcd'  : '',  $
                                                'xcd'  : '',  $
                                                'plt'  : ''   }
                                            Not all file names are required - just
                                            those for replacement.

      OPTIONAL   :  all        bool   return 2D coeff(te, dens): default is false
                    cx         bool   if True include thermal CX as a process

      RETURNS    :  frac       dict   { 'stage' :   list of ion/metastable descriptors
                                        'ion'   :   Equilibrium abundances
                    power      dict   { 'stage' :   list of ion/metastable descriptors
                                        'ion'   :   Stage contribution to power
                                        'plt'   :   Line power
                                        'prb'   :   Continuum power (recombination + bremsstrahlung)
                                        'prc'   :   CX power
                                        'total' :   total line power

                    Dimensions are (nte, nstage) where nte is number of Te/dens pairs
                                   (ndens, nte, nstage) if all=True

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Dimension ion quantities from the values in the acd
                       file since this is not related to Z for superstage
                       adf11 datasets.
           1.3     Martin O'Mullane
                     - For python3 compatibility, change print statement to
                       a function and use adas_write_pipe to send the appropriate
                       stream to the bi-directional pipe.
           1.4     Martin O'Mullane
                     - The line, continuum and CX powers were mis-labelled in the
                       power output dictionary.
           1.5     Martin O'Mullane
                     - Use datetime.now() rather than datetime.utcnow()
           1.6     Martin O'Mullane
                     - Change the default uid to 'adas' from None.
                     - Fix failure to work with a single input temperature
                       or density.
                     - Return stage labels as strings, not byte arrays.
           1.7     Martin O'Mullane
                     - Correct method for forming resolved names from uid and year.
                     - Call adf11_nmeta.py to calculate the number of stages.
                       This works for resolved and un-resolved data.
           1.8     Martin O'Mullane
                     - Move construction and checking of filenames to a separate
                       routine.
                     - Fix reshaping of fractional abundances for 2D (all=True).
                     
      VERSION    :
            1.1    20-08-2012
            1.2    24-04-2013
            1.3    16-01-2014
            1.4    19-12-2015
            1.5    29-06-2017
            1.6    06-08-2018
            1.7    07-11-2018
            1.8    08-11-2019

    """

#------------------------------------------------------------------------------


    def _run405_calc(fortran_code, today, yr, defyr, esym, flt,
                     filenames, fileavailable, defavailable, indices, is_partial,
                     te_int, dens_int, tion_int, dion_int, mz, mh,
                     logfile):
        """
           run fortran adas405 (within limits of Te/dens dimensions)
        """

        import subprocess
        from adaslib import adas_write_pipe
        from numpy import arange

        # Launch fortran and interact with it

        pipe = subprocess.Popen(fortran_code,
               shell=True, bufsize=-1,
               stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)

        # First (input) panel
        
        adas_write_pipe(pipe, today)

        rep = 'NO'
        adas_write_pipe(pipe, rep)

        adas_write_pipe(pipe, yr)
        adas_write_pipe(pipe, defyr)
        adas_write_pipe(pipe, esym)

        scriptname = 'NULL'
        adas_write_pipe(pipe, scriptname)

        adas_write_pipe(pipe, flt)

        for f in filenames:
           adas_write_pipe(pipe, f)
        for i in fileavailable:
           adas_write_pipe(pipe, i)
        for i in defavailable:
           adas_write_pipe(pipe, i)
        for i in indices:
           adas_write_pipe(pipe, i)

        adas_write_pipe(pipe, is_partial)

        line = pipe.stdout.readline()


        # Second (processing) panel

        line = pipe.stdout.readline()
        ntdmax = int(line)
        line = pipe.stdout.readline()
        nline = int(line)

        if nline > 0:
           ciion  = [''] * nline
           cmpts  = [''] * nline
           ctitle = [''] * nline
           for j in arange(nline):
              line = pipe.stdout.readline()
              ciion[j] = line
              line = pipe.stdout.readline()
              cmpts[j] = line
              line = pipe.stdout.readline()
              ctitle[j] = line

        adas_write_pipe(pipe, 0)

        adas_write_pipe(pipe, 'Test')
        adas_write_pipe(pipe, 0)
        adas_write_pipe(pipe, 2)         # Te in eV
        adas_write_pipe(pipe, te_int.size)
        adas_write_pipe(pipe, mz)
        adas_write_pipe(pipe, mh)


        for j in arange(te_int.size):
           adas_write_pipe(pipe, te_int[j])
           adas_write_pipe(pipe, dens_int[j])
           adas_write_pipe(pipe, tion_int[j])
           adas_write_pipe(pipe, dion_int[j])

        adas_write_pipe(pipe, 0)


        # Final (output) panel

        adas_write_pipe(pipe, 0)

        adas_write_pipe(pipe, 1)       # we want fractional abundance
        adas_write_pipe(pipe, 1)

        if logfile == None:        
           adas_write_pipe(pipe, 0)
        else:
           adas_write_pipe(pipe, 1)
           adas_write_pipe(pipe, logfile)

        if goftfile == None:
           adas_write_pipe(pipe, 0)
        else:
           adas_write_pipe(pipe, 1)
           adas_write_pipe(pipe, goftfile)

        if logfile != None:
           adas_write_pipe(pipe, 'Made by run_adas405 ' + today)

        adas_write_pipe(pipe, 0)


        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)
        for j in arange(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        fvals = zeros((itmax, nmsum), float)
        poptit = [''] * nmsum

        for i in arange(itmax):
           for j in arange(nmsum):
              line = pipe.stdout.readline()
              fvals[i,j] = float(line)

        for j in arange(nmsum):
           line = pipe.stdout.readline()
           poptit[j] = line

        line = pipe.stdout.readline()


        adas_write_pipe(pipe, 0)        # and the power
        adas_write_pipe(pipe, 0)
        adas_write_pipe(pipe, 1)
        adas_write_pipe(pipe, 2)

        if logfile == None:
           adas_write_pipe(pipe, 0)
        else:
           adas_write_pipe(pipe, 1)
           adas_write_pipe(pipe, logfile)

        adas_write_pipe(pipe, 0)

        if logfile != None:
           adas_write_pipe(pipe, 'Made by run_adas405 ' + today)

        adas_write_pipe(pipe, 0)        # and the power

        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)
        tvals = zeros((itmax,4), float)

        for j in arange(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        pvals = zeros((itmax, nmsum), float)
        poptit = [''] * nmsum

        for i in arange(itmax):
           for j in arange(nmsum):
              line = pipe.stdout.readline()
              pvals[i,j] = float(line)

        for j in arange(nmsum):
           line = pipe.stdout.readline()
           poptit[j] = line

        line = pipe.stdout.readline()

        for j in arange(4):
           for i in arange(itmax):
              line = pipe.stdout.readline()
              tvals[i,j] = float(line)


        adas_write_pipe(pipe, 1)


        # Close pipe and return fractional abundances and powers as dictionaries

        line = pipe.stdout.readline()
        if line == 1:
           pipe.stdin.close()

        return poptit, fvals, pvals, tvals

#------------------------------------------------------------------------------



    ### MAIN ###

    # External routines required

    import os
    from datetime import datetime
    from numpy import asarray, zeros, arange, atleast_1d
    from adaslib import numlines
    from adas4xx import adas4xx_ionbal_files

    # Initialisation and setup

    goftfile = None

    d     = datetime.now()
    today = d.strftime("%d/%m/%y")

    fortran_code = os.getenv('ADASFORT') + '/adas405.out'

    # Examine inputs

    if not elem:
        raise Exception("An element must be supplied")

    if year == None:
        year = defyear
        print("The default year will be used")

    # check and conversions

    te_int   = atleast_1d(asarray(te, float))
    dens_int = atleast_1d(asarray(dens, float))

    if not cx:
        tion_int = atleast_1d(zeros(te_int.size))
        dion_int = atleast_1d(zeros(dens_int.size))
    else:
        tion_int = atleast_1d(asarray(tion, float))
        dion_int = atleast_1d(asarray(denh, float))

    if unit_te.lower() == "kelvin":
        te_int = te_int / 11605.0

    if partial == False:
        indices = [1, 1, 0, 1, 0, 0, 0, 1]
        is_partial = 0
    else:
        indices = [1, 1, 0, 1, 0, 1, 1, 1]
        is_partial = 1


    filenames, fileavailable, defavailable, info = \
       adas4xx_ionbal_files(elem, year, uid=uid, defyear=defyear, 
                            filter=filter, partial=partial,
                            files=files)
    
    numz = info['num_meta']

    # Call fortran in chunks of 30 Te/dens pairs - fortran dimension constraint

    if filter == None:
        flt = ''
    else:
        flt = str(filter)
    flt = "".join(flt.split())

    len_te   = te_int.size
    len_dens = dens_int.size

    if all == True:

        itval = len_te * len_dens

        te_int   = asarray([te_int] * len_dens).ravel()
        dens_int = asarray([dens_int] * len_te).transpose().ravel()
        tion_int = asarray([tion_int] * len_dens).ravel()
        dion_int = asarray([dion_int] * len_te).transpose().ravel()

    else:

        itval = min(len_dens, len_te)
        te_int   = te_int[0:itval]
        dens_int = dens_int[0:itval]
        tion_int = tion_int[0:itval]
        dion_int = dion_int[0:itval]


    MAXVAL = 30
    n_call = numlines(itval, MAXVAL)

    frac_coeff = zeros((itval, numz), float)
    pion_coeff = zeros((itval, numz), float)
    plt_coeff  = zeros(itval)
    prb_coeff  = zeros(itval)
    prc_coeff  = zeros(itval)
    ptot_coeff = zeros(itval)


    for j in arange(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, itval)

       te_ca   = te_int[ist:ifn]
       dens_ca = dens_int[ist:ifn]
       tion_ca = tion_int[ist:ifn]
       dion_ca = dion_int[ist:ifn]
       
       poptit, fvals, pvals, tvals = _run405_calc(fortran_code, today, 
                                                  info['year'], info['defyear'], info['elem'], flt,
                                                  filenames, fileavailable, defavailable,
                                                  indices, is_partial,
                                                  te_ca, dens_ca, tion_ca, dion_ca,
                                                  mz, mh, logfile)

       frac_coeff[ist:ifn,:] = fvals
       pion_coeff[ist:ifn,:] = pvals
       plt_coeff[ist:ifn]    = tvals[:,2]
       prb_coeff[ist:ifn]    = tvals[:,0]
       prc_coeff[ist:ifn]    = tvals[:,1]
       ptot_coeff[ist:ifn]   = tvals[:,3]

    # swith to 2D (dens,Te,stage) shape if all is true

    if all == True:
       frac_coeff = frac_coeff.reshape(len_dens, len_te, numz)
       pion_coeff = pion_coeff.reshape(len_dens, len_te, numz)
       plt_coeff  = plt_coeff.reshape(len_dens, len_te)
       prb_coeff  = prb_coeff.reshape(len_dens, len_te)
       prc_coeff  = prc_coeff.reshape(len_dens, len_te)
       ptot_coeff = ptot_coeff.reshape(len_dens, len_te)


    # prepare output and return
    
    c_stg = asarray([row.strip().decode() for row in poptit])    # bytes to string and remove \n
    
    frac = { 'stage' : c_stg,
             'ion'   : frac_coeff }

    power = { 'stage' : c_stg,
              'ion'   : pion_coeff,
              'plt'   : plt_coeff,
              'prb'   : prb_coeff,
              'prc'   : prc_coeff,
              'total' : ptot_coeff
            }

    return frac, power
