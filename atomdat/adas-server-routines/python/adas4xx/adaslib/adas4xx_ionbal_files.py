def adas4xx_ionbal_files(elem, year, uid='adas', defyear=None, filter=None, partial=False, files=None):
    """

      PURPOSE    : Construct a list of filenames required for ionisation balance
                   calculations, eg for adas405 and adas406. A mixture of uid,
                   element, year, power filter and file names are supported.
                   Partial adf11 data for metastable-resolved coefficients, are
                   requested via partial switch.

                   A list indicating whether the files are available or whether
                   a default files is present is returned.

                   A minimum input of element symbol and year is required.
                   
                   If the acd file exists information on the number of of metastables 
                   and a stage/metastable identified (as in adas405 ouptut) is determined.
                   The fall back value is -1 for the number of metastables ans "n/a" for
                   their identifier.

                   The info dictionary and the way that the names are returned,
                   ie as an in-order list, is because adas405 and adas406 expects this.

      filenames, available, defavailable, info = adas4xx_ionbal_files('n', 96)

                    NAME         TYPE     DETAILS
      ARGUMENTS  :
                    elem         str      element
                    year         int      year of adf11 data.
                    uid          str      username for the adf11 location. Defaults
                                          to 'adas' for central ADAS data.
                    defyear      int      default year of adf11 data (defaults to 89 if not set).
                    filter       str      filter if appropriate. Defaults to none.
                    partial      bool     if True construct list of metastable
                                          resolved adf11 files

                    files        dict     Optional dictionary with replacement
                                          names for individula files.
                                                 {'acd'  : '',  $
                                                  'scd'  : '',  $
                                                  'ccd'  : '',  $
                                                  'prb'  : '',  $
                                                  'prc'  : '',  $
                                                  'qcd'  : '',  $
                                                  'xcd'  : '',  $
                                                  'plt'  : ''   }
                                              Not all file names are required - just
                                              those for replacement.

      RETURNS    :  filenames    dict   list of adf11 filenames ordered by
                                          acd, scd, ccd, prb, prc, qcd, xcd
                                          and plt.
                    available    list   list indicating whether the file exists
                                        1 if it exists, 0 if it is not present
                    defavailable list   list indicating whether the default file exists
                                        1 if it exists, 0 if it is not present
                    info         dict   Fully checked data for 
                                        {'elem'    : element symbol
                                         'iz0'     : atomic number
                                         'num_meta': the number of metastable present
                                         'stage'   : list identifting the satge/metastable
                                         'year'    : year of adf11 data
                                         'defyear' : default year }
                                         


      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    07-11-2019

    """

    import os
    from numpy import arange
    from adas import xxdata_11, i4eiz0


    # Clean up the inputs and defaults

    esym  = elem.lower()
    esym  = "".join(esym.split())
    
    nz = i4eiz0(esym)
    if nz == -1: raise Exception("Invalid element")

    if isinstance(year, str):
        yr = year
    else:
        yr = "{:02d}".format(year)

    if defyear is None:
        defyr = '89'
    else:
        if isinstance(defyear, str):
            defyr = defyear
        else:
            defyr = "{:02d}".format(defyear)


    # construct filenames

    log_name  = os.getenv("LOGNAME")
    adas_user = os.getenv("ADASUSER")
    adas_cent = os.getenv("ADASCENT")

    dnames       = ['acd', 'scd', 'ccd', 'prb', 'prc', 'qcd', 'xcd', 'plt']
    tpl_dirnames = dnames
    num_adf11    = 8

    if uid != 'adas':
        i1 = adas_user.find(log_name)
        i2 = len(log_name)
        p1 = adas_user[:i1]
        p2 = "".join(uid.split())
        p3 = adas_user[i1+i2:]

        fileroot = p1 + p2 + p3 + '/adf11/'
    else:
        fileroot = adas_cent + '/adf11/'
 
    defdirnames = [s + defyr for s in tpl_dirnames]
    dirnames    = [s + yr for s in tpl_dirnames]

    if partial is True:
        defdirnames = [s + defyr + 'r' for s in tpl_dirnames]
        dirnames    = [s + yr + 'r' for s in tpl_dirnames]

    if filter is None:
        filenames = [fileroot + s + '/' + s + '_' + esym + '.dat' for s in dirnames]
        defnames  = [fileroot + s + '/' + s + '_' + esym + '.dat' for s in defdirnames]
    else:
        filenames = [fileroot + s + '/' + s + '_' + esym + '.' + filter + '.dat'
                     if s[0] == 'p'
                     else fileroot + s + '/' + s + '_' + esym + '.dat'
                     for s in dirnames]
        defnames  = [fileroot + s + '/' + s + '_' + esym + '.' + filter + '.dat'
                     if s[0] == 'p'
                     else fileroot + s + '/' + s + '_' + esym + '.dat'
                     for s in defdirnames]


    # Overwrite filenames which are missing with default ones

    available = [0] * num_adf11
    defavailable  = [0] * num_adf11

    for j in arange(num_adf11):

       f = defnames[j]
       if (os.path.isfile(f) and os.access(f, os.R_OK)) == True:
          defavailable[j] = 1

       f = filenames[j]
       if (os.path.isfile(f) and os.access(f, os.R_OK)) == True:
          available[j] = 1
       else:
          available[j] = 0
          filenames[j] = defnames[j]


    # Replace with filenames from files list

    if files:
       for f in files.keys():
          if f in dnames:
            i = dnames.index(f)
            filenames[i] = files[f]
            available[i] = 1

    # Remove qcd and xcd for non-partial

    if partial is False:
        filenames[5] = ''
        filenames[6] = ''


    # From acd file, get number of metastables and construct stage labels

    if (os.path.isfile(filenames[0]) and os.access(filenames[0], os.R_OK)) == True:
        acd = xxdata_11(file=filenames[0], adf11type='acd')
        num_meta = acd['icnctv'].sum(axis=0)
        poptit = list()
        for iz in arange(nz):
            for ig in arange(acd['icnctv'][iz]):
                poptit.append(elem.ljust(2).title() + '+' + "{:2d}".format(iz) + ' (' + "{:2d}".format(ig+1) + ')')
    else:
        num_meta = -1
        poptit   = "n/a"


    # Retrun
    
    info = {'elem'     : esym,
            'iz0'      : nz,
            'num_meta' : num_meta,
            'stage'    : poptit,
            'year'     : yr,
            'defyear'  : defyr }

    return filenames, available, defavailable, info
