def adas4xx_ionbal_tau(uid='adas', year=None, defyear=89, filter=None, elem=None,
                       files=None, partial=False, mh=1.0, mz=1.0,
                       meta=None, tconf=1.0e8, time=[1.0],
                       te=[0.0], dens=[0.0], unit_te='ev', all=False,
                       tion=[0.0], denh=[0.0], cx=False, skipzero=False):

    """

      PURPOSE    :  A time-dependent ionization balance code with a confinement time
                    as an additional loss term. The inputs and outputs are similar to
                    run_adas406 with outputs arrays having an extra time dimension.

                    metastable-resolved (partial=True) and thermal CX (cx=True) are
                    not supported yet.

      frac, energy = run_adas406(elem=elem, uid=uid, year=year, dens=dens, te=te, all=True, ....)

                    NAME       TYPE     DETAILS
      REQUIRED   :  uid        str      username of adf11 location. Defaults
                                        to 'adas' for central ADAS data.
                    year       int      year of adf11 data.
                    defyear    int      default year of adf11 data
                                        (defaults to 89 if not set).
                    filter     str      filter (if appropriate).
                    elem       str      element
                    te         float()  temperatures requested
                    dens       float()  densities requested
                    tion       float()  ion temperatures requested
                                        (if cx=True is set).
                    denh       float()  ion densities requested
                                        (if cx=True is set).
                    mh         float    Hydrogen isotope mass
                                        (if cx=True is set).
                    mz         float    Element isotope mass
                                        (if cx=True is set).
                    meta       float()  initial fractional abundances
                                        at t=0sec. Defaults to all in
                                        the ground stage/metastable.
                    time       float()  times required (sec)
                                        (must be supplied).
                    tconf      float    confinement time (sec),
                                        defaults to infinity (1e8 sec in practice).
                    files      dict     Optional dictionary with replacement
                                        names for individula files.
                                               {'acd'  : '',  $
                                                'scd'  : '',  $
                                                'ccd'  : '',  $
                                                'prb'  : '',  $
                                                'prc'  : '',  $
                                                'qcd'  : '',  $
                                                'xcd'  : '',  $
                                                'plt'  : ''   }
                                            Not all file names are required - just
                                            those for replacement.

      OPTIONAL   :  all        bool     True => return 2D coeff(te, dens)
                                        default is False
                    cx         bool     if True include thermal CX as a process
                    skipzero   bool     do not use effective zero in interpolating
                                        to avoid ringing: default is False

      RETURNS    :  pop        float()  relative population of stages

                    prad       float()
                    pdiff






                    Dimensions are (nte, nstage, ntime) where nte is number of Te/dens pairs
                                   (ndens, nte, nstage, ntime) if all=True

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    08-11-2019

    """


    # External routines required

    import numpy as np
    import scipy.linalg as la

    from adaslib import read_adf11
    from adas4xx import adas4xx_ionbal_files


    # Temporary restrictions: Stop if partial is requested or if Te/dens are 2D

    if partial is True:
        raise Exception("The metastable-resolved model is not ready")

    if cx is True:
        raise Exception("The thermal CX model is not ready")


    # Examine inputs

    if not elem:
        raise Exception("An element must be supplied")

    if year == None:
        year = defyear
        print("The default year will be used")


    # check and conversions

    t_int = np.atleast_1d(np.asarray(time, np.float))

    te_int   = np.atleast_1d(np.asarray(te, np.float))
    dens_int = np.atleast_1d(np.asarray(dens, np.float))

    if not cx:
        tion_int = np.atleast_1d(np.zeros(te_int.size))
        dion_int = np.atleast_1d(np.zeros(dens_int.size))
    else:
        tion_int = np.atleast_1d(np.asarray(tion, float))
        dion_int = np.atleast_1d(np.asarray(denh, float))

    if unit_te.lower() == "kelvin":
        te_int = te_int / 11605.0

    filenames, fileavailable, defavailable, info = \
       adas4xx_ionbal_files(elem, year, uid=uid, defyear=defyear,
                            filter=filter, partial=partial,
                            files=files)

    n_lev = info['num_meta']

    if meta is None:
        init_meta = np.zeros(n_lev)
        init_meta[0] = 1.0
    else:
        init_meta = np.asarray(meta) / np.asarray(meta).sum(axis=0)

    iz0 = info['iz0']

    if tconf > 1.0e4:
        aconf = 0.0
    else:
        aconf = 1.0 / tconf


    # Construct internal Te/dens vectors and extract rate coefficients

    len_te   = te_int.size
    len_dens = dens_int.size
    n_times  = t_int.size

    if all == True:

        itval    = len_te * len_dens
        te_int   = np.asarray([te_int] * len_dens).ravel()
        dens_int = np.asarray([dens_int] * len_te).transpose().ravel()
        tion_int = np.asarray([tion_int] * len_dens).ravel()
        dion_int = np.asarray([dion_int] * len_te).transpose().ravel()

    else:

        itval    = min(len_dens, len_te)
        te_int   = te_int[0:itval]
        dens_int = dens_int[0:itval]
        tion_int = tion_int[0:itval]
        dion_int = dion_int[0:itval]

    acd = np.zeros((n_lev, itval), np.float)
    scd = np.zeros((n_lev, itval), np.float)
    plt = np.zeros((n_lev, itval), np.float)
    prb = np.zeros((n_lev, itval), np.float)

    for j in np.arange(n_lev-1):

        iz1 = j + 1

        acd[j,:] = read_adf11(file=filenames[0], adf11type='acd', is1=iz1, te=te_int, dens=dens_int, skipzero=skipzero)
        scd[j,:] = read_adf11(file=filenames[1], adf11type='scd', is1=iz1, te=te_int, dens=dens_int, skipzero=skipzero)
        plt[j,:] = read_adf11(file=filenames[7], adf11type='plt', is1=iz1, te=te_int, dens=dens_int, skipzero=skipzero)
        prb[j,:] = read_adf11(file=filenames[3], adf11type='plt', is1=iz1, te=te_int, dens=dens_int, skipzero=skipzero)


    # Evaluate ionization balance at each time

    pop  = np.zeros((n_lev, itval, n_times), np.float)

    pr_ion = np.zeros((n_lev, itval, n_times), np.float)
    pr_plt = np.zeros((itval, n_times), np.float)
    pr_prb = np.zeros((itval, n_times), np.float)
    pr_tot = np.zeros((itval, n_times), np.float)

    for ite in np.arange(itval):

        # fill matrix

        a = np.zeros((n_lev, n_lev), np.float)

        a[1,0] = dens_int[ite]*scd[0,ite]
        a[0,1] = dens_int[ite]*acd[0,ite]
        a[0,0] = -a[1,0] - aconf

        for i in np.arange(1, n_lev-1):
            a[i+1,i] = dens_int[ite]*scd[i,ite]
            a[i,i+1] = dens_int[ite]*acd[i,ite]
            a[i,i]   = -a[i+1,i] - a[i-1,i] - aconf

        a[n_lev-1, n_lev-1] = -a[n_lev-2,n_lev-1] - aconf

        for i in np.arange(n_lev):
            a[0,i] = a[0,i] + aconf


        # solve for Eigen vectors and values

        eigvals, eigvecs = la.eig(a)

        val = eigvals.real

        c = la.solve(eigvecs, init_meta)

        for it in np.arange(n_times):
            tm = time[it]
            for j in np.arange(n_lev):
                pop[:, ite, it] = pop[:, ite, it] + c[j] * eigvecs[:,j] * np.exp(val[j]*tm)

        pop = pop.clip(min=1.0e-36)

        for it in np.arange(n_times):
            pr_ion[:, ite, it] = pop[:,ite,it] * plt[:, ite]

        for j in np.arange(n_lev-1):
            pr_tot[ite,:] = pr_tot[ite,:] + pop[j,ite,:] * plt[j, ite] + pop[j+1, ite,:] * prb[j,ite]
            pr_plt[ite,:] = pr_plt[ite,:] + pop[j,ite,:] * plt[j, ite]
            pr_prb[ite,:] = pr_prb[ite,:] + pop[j+1, ite,:] * prb[j,ite]


    # Reformat to 2D if required

    if all is False:

        pr_tot_out = pr_tot
        pr_plt_out = pr_plt
        pr_prb_out = pr_prb

        pop_out = np.zeros((itval, n_lev, n_times), np.float)
        pow_out = np.zeros((itval, n_lev, n_times), np.float)
        for it in np.arange(n_times):
            pop_out[:,:,it] = pop[:,:,it].transpose()
            pow_out[:,:,it] = pr_ion[:,:,it].transpose()

    else:

        pr_tot_out = np.zeros((len_dens, len_te, n_times), np.float)
        pr_plt_out = np.zeros((len_dens, len_te, n_times), np.float)
        pr_prb_out = np.zeros((len_dens, len_te, n_times), np.float)

        pop_out = np.zeros((len_dens, len_te, n_lev, n_times), np.float)
        pow_out = np.zeros((len_dens, len_te, n_lev, n_times), np.float)

        for it in np.arange(n_times):
            pr_tot_out[:,:,it] = pr_tot[:,it].reshape(len_dens, len_te)
            pr_plt_out[:,:,it] = pr_plt[:,it].reshape(len_dens, len_te)
            pr_prb_out[:,:,it] = pr_prb[:,it].reshape(len_dens, len_te)
            for ist in np.arange(n_lev):
                pop_out[:,:,ist,it] = pop[:,ist,it].reshape(len_dens, len_te)
                pow_out[:,:,ist,it] = pr_ion[:,ist,it].reshape(len_dens, len_te)


    # Return populations and powers

    frac = {'stage' : info['stage'],
            'time'  : time,
            'tau'   : tconf,
            'ion'   : pop_out }

    power = {'stage' : info['stage'],
            'time'  : time,
            'tau'   : tconf,
            'ion'   : pow_out,
            'plt'   : pr_plt_out,
            'prb'   : pr_prb_out,
            'total' : pr_tot_out }

    return frac, power
