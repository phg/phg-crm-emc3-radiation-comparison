def run_adas406(uid='adas', year=None, defyear=89, filter=None, elem=None,
                files=None, partial=False, mh=1.0, mz=1.0,
                meta=None, tint=None,
                te=[0.0], dens=[0.0], unit_te='ev', all=False,
                tion=[0.0], denh=[0.0], cx=False, logfile=None):

    """

      PURPOSE    :  Runs the adas406 equilibrium balance code as a python function

      frac, energy = run_adas406(elem=elem, uid=uid, year=year, dens=dens, te=te, all=True, ....)

                    NAME       TYPE     DETAILS
      REQUIRED   :  uid        str      username of adf11 location. Defaults
                                        to 'adas' for central ADAS data.
                    year       int      year of adf11 data.
                    defyear    int      default year of adf11 data
                                        (defaults to 89 if not set).
                    filter     str      filter (if appropriate).
                    elem       str      element
                    te         float()  temperatures requested
                    dens       float()  densities requested
                    tion       float()  ion temperatures requested
                                        (if cx=True is set).
                    denh       float()  ion densities requested
                                        (if cx=True is set).
                    mh         float    Hydrogen isotope mass
                                        (if cx=True is set).
                    mz         float    Element isotope mass
                                        (if cx=True is set).
                    log        str      name of output text file.
                    meta       float()  initial fractional abundances
                                        at t=0sec. Defaults to all in
                                        the ground stage/metastable.
                    tint       float    integration time (sec)
                                        (must be supplied).
                    files      dict     Optional dictionary with replacement
                                        names for individula files.
                                               {'acd'  : '',  $
                                                'scd'  : '',  $
                                                'ccd'  : '',  $
                                                'prb'  : '',  $
                                                'prc'  : '',  $
                                                'qcd'  : '',  $
                                                'xcd'  : '',  $
                                                'plt'  : ''   }
                                            Not all file names are required - just
                                            those for replacement.

      OPTIONAL   :  all        bool   True => return 2D coeff(te, dens)
                                      default is False
                    cx         bool   if True include thermal CX as a process

      RETURNS    :  frac       dict   { 'stage' :   list of ion/metastable descriptors
                                        'ion'   :   Equilibrium abundances
                    energy     dict   { 'stage' :   list of ion/metastable descriptors
                                        'ion'   :   Stage contribution to energy surfeit/defecit
                                        'elt'   :   Line energy surfeit/defecit
                                        'erb'   :   Continuum energy surfeit/defecit (recombination + bremsstrahlung)
                                        'erc'   :   CX energy surfeit/defecit
                                        'total' :   total line energy surfeit/defecit

                    Dimensions are (nte, nstage) where nte is number of Te/dens pairs
                                   (ndens, nte, nstage) if all=True

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Fix the check for meta because there was an attribute 
                       problem when the (optional) array was not supplied. 
           1.3     Martin O'Mullane
                     - Move construction and checking of filenames to a separate
                       routine.
                     - Fix reshaping of fractional abundances for 2D (all=True).

      VERSION    :
            1.1    08-09-2019
            1.2    15-09-2019
            1.3    08-11-2019

    """

#------------------------------------------------------------------------------


    def _run406_calc(fortran_code, today, yr, defyr, esym, flt,
                     filenames, fileavailable, defavailable, indices, is_partial,
                     te_int, dens_int, tion_int, dion_int, mz, mh,
                     init_meta, timef,
                     logfile):
        """
           run fortran adas406 (within limits of Te/dens dimensions)
        """

        import subprocess
        from adaslib import adas_write_pipe
        from numpy import arange

        # Launch fortran and interact with it

        pipe = subprocess.Popen(fortran_code,
               shell=True, bufsize=-1,
               stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)

        # First (input) panel

        adas_write_pipe(pipe, today)

        rep = 'NO'
        adas_write_pipe(pipe, rep)

        adas_write_pipe(pipe, yr)
        adas_write_pipe(pipe, defyr)
        adas_write_pipe(pipe, esym)

        scriptname = 'NULL'
        adas_write_pipe(pipe, scriptname)

        adas_write_pipe(pipe, flt)

        for f in filenames:
           adas_write_pipe(pipe, f)
        for i in fileavailable:
           adas_write_pipe(pipe, i)
        for i in defavailable:
           adas_write_pipe(pipe, i)
        for i in indices:
           adas_write_pipe(pipe, i)

        adas_write_pipe(pipe, is_partial)

        line = pipe.stdout.readline()


        # Second (processing) panel

        line = pipe.stdout.readline()
        ntdmax = int(line)

        line = pipe.stdout.readline()
        imdimd = int(line)
        line = pipe.stdout.readline()
        nmsum = int(line)

        chpop = [''] * nmsum
        for j in arange(nmsum):
           line = pipe.stdout.readline()
           chpop[j] = line

        line = pipe.stdout.readline()
        nline = int(line)

        if nline > 0:
           ciion  = [''] * nline
           cmpts  = [''] * nline
           ctitle = [''] * nline
           for j in arange(nline):
              line = pipe.stdout.readline()
              ciion[j] = line
              line = pipe.stdout.readline()
              cmpts[j] = line
              line = pipe.stdout.readline()
              ctitle[j] = line

        adas_write_pipe(pipe, 0)

        adas_write_pipe(pipe, 'Test')
        adas_write_pipe(pipe, 0)

        adas_write_pipe(pipe, timef)
        for j in arange(init_meta.size):
           adas_write_pipe(pipe, init_meta[j])


        adas_write_pipe(pipe, 2)            # Te in eV
        adas_write_pipe(pipe, te_int.size)
        adas_write_pipe(pipe, mz)
        adas_write_pipe(pipe, mh)


        for j in arange(te_int.size):
           adas_write_pipe(pipe, te_int[j])
           adas_write_pipe(pipe, dens_int[j])
           adas_write_pipe(pipe, tion_int[j])
           adas_write_pipe(pipe, dion_int[j])

        adas_write_pipe(pipe, 0)


        # Final (output) panel

        adas_write_pipe(pipe, 0)

        adas_write_pipe(pipe, 1)       # we want fractional abundance
        adas_write_pipe(pipe, 1)

        if logfile == None:
           adas_write_pipe(pipe, 0)
        else:
           adas_write_pipe(pipe, 1)
           adas_write_pipe(pipe, logfile)

        if goftfile == None:
           adas_write_pipe(pipe, 0)
        else:
           adas_write_pipe(pipe, 1)
           adas_write_pipe(pipe, goftfile)

        if logfile != None:
           adas_write_pipe(pipe, 'Made by run_adas406 ' + today)

        adas_write_pipe(pipe, 0)


        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)
        for j in arange(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        fvals = zeros((itmax, nmsum), float)
        poptit = [''] * nmsum

        for i in arange(itmax):
           for j in arange(nmsum):
              line = pipe.stdout.readline()
              fvals[i,j] = float(line)

        for j in arange(nmsum):
           line = pipe.stdout.readline()
           poptit[j] = line

        line = pipe.stdout.readline()

        line = pipe.stdout.readline()
        timef_ret = float(line)

        sy0 = zeros(nmsum)
        for j in arange(nmsum):
           line = pipe.stdout.readline()
           sy0[j] = line


        adas_write_pipe(pipe, 0)        # and the power
        adas_write_pipe(pipe, 0)
        adas_write_pipe(pipe, 1)
        adas_write_pipe(pipe, 2)

        if logfile == None:
           adas_write_pipe(pipe, 0)
        else:
           adas_write_pipe(pipe, 1)
           adas_write_pipe(pipe, logfile)

        adas_write_pipe(pipe, 0)

        if logfile != None:
           adas_write_pipe(pipe, 'Made by run_adas406 ' + today)

        adas_write_pipe(pipe, 0)        # and the power

        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)

        for j in arange(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        pvals = zeros((itmax, nmsum), float)
        poptit = [''] * nmsum

        for i in arange(itmax):
           for j in arange(nmsum):
              line = pipe.stdout.readline()
              pvals[i,j] = float(line)

        for j in arange(nmsum):
           line = pipe.stdout.readline()
           poptit[j] = line

        line = pipe.stdout.readline()

        line = pipe.stdout.readline()
        timef_ret = float(line)

        sy0 = zeros(nmsum)
        for j in arange(nmsum):
           line = pipe.stdout.readline()
           sy0[j] = line

        adas_write_pipe(pipe, 0)
        adas_write_pipe(pipe, 0)
        adas_write_pipe(pipe, 1)
        adas_write_pipe(pipe, 4)
        adas_write_pipe(pipe, 0)
        adas_write_pipe(pipe, 0)

        if logfile != None:
           adas_write_pipe(pipe, 'Made by run_adas406 ' + today)

        adas_write_pipe(pipe, 0)

        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)

        for j in arange(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        tvals = zeros((itmax,4), float)

        for i in arange(itmax):
           for j in arange(4):
              line = pipe.stdout.readline()
              tvals[i,j] = float(line)

        line = pipe.stdout.readline()
        timef_ret = float(line)


        adas_write_pipe(pipe, 1)


        # Close pipe and return fractional abundances and energy surfeits/deficits as dictionaries

        pipe.stdin.close()

        return poptit, fvals, pvals, tvals

#------------------------------------------------------------------------------



    ### MAIN ###

    # External routines required

    import os
    from datetime import datetime
    from numpy import asarray, zeros, arange, atleast_1d
    from adaslib import numlines
    from adas4xx import adas4xx_ionbal_files

    # Initialisation and setup

    goftfile = None

    d     = datetime.now()
    today = d.strftime("%d/%m/%y")

    fortran_code = os.getenv('ADASFORT') + '/adas406.out'


    # Examine inputs

    if not elem:
        raise Exception("An element must be supplied")

    if year == None:
        year = defyear
        print("The default year will be used")


    # check and conversions

    te_int   = atleast_1d(asarray(te, float))
    dens_int = atleast_1d(asarray(dens, float))

    if not cx:
        tion_int = atleast_1d(zeros(te_int.size))
        dion_int = atleast_1d(zeros(dens_int.size))
    else:
        tion_int = atleast_1d(asarray(tion, float))
        dion_int = atleast_1d(asarray(denh, float))

    if unit_te.lower() == "kelvin":
        te_int = te_int / 11605.0

    if partial == False:
        indices = [1, 1, 0, 1, 0, 0, 0, 1]
        is_partial = 0
    else:
        indices = [1, 1, 0, 1, 0, 1, 1, 1]
        is_partial = 1


    filenames, fileavailable, defavailable, info = \
       adas4xx_ionbal_files(elem, year, uid=uid, defyear=defyear, 
                            filter=filter, partial=partial,
                            files=files)
    
    num_meta = info['num_meta']
    
    if meta is None:
        init_meta = zeros(num_meta)
        init_meta[0] = 1.0
    else:
        init_meta = asarray(meta) / asarray(meta).sum(axis=0)


    if tint == None:
        raise Exception("An integration time must be supplied")


    # Call fortran in chunks of 30 Te/dens pairs - fortran dimension constraint

    if filter == None:
        flt = ''
    else:
        flt = str(filter)
    flt = "".join(flt.split())

    len_te   = te_int.size
    len_dens = dens_int.size

    if all == True:

        itval = len_te * len_dens

        te_int   = asarray([te_int] * len_dens).ravel()
        dens_int = asarray([dens_int] * len_te).transpose().ravel()
        tion_int = asarray([tion_int] * len_dens).ravel()
        dion_int = asarray([dion_int] * len_te).transpose().ravel()

    else:

        itval = min(len_dens, len_te)
        te_int   = te_int[0:itval]
        dens_int = dens_int[0:itval]
        tion_int = tion_int[0:itval]
        dion_int = dion_int[0:itval]


    MAXVAL = 30
    n_call = numlines(itval, MAXVAL)

    frac_coeff = zeros((itval, num_meta), float)
    pion_coeff = zeros((itval, num_meta), float)
    plt_coeff  = zeros(itval)
    prb_coeff  = zeros(itval)
    prc_coeff  = zeros(itval)
    ptot_coeff = zeros(itval)


    for j in arange(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, itval)

       te_ca   = te_int[ist:ifn]
       dens_ca = dens_int[ist:ifn]
       tion_ca = tion_int[ist:ifn]
       dion_ca = dion_int[ist:ifn]
       
       poptit, fvals, pvals, tvals = _run406_calc(fortran_code, today,
                                                  info['year'], info['defyear'], info['elem'], flt,
                                                  filenames, fileavailable, defavailable,
                                                  indices, is_partial,
                                                  te_ca, dens_ca, tion_ca, dion_ca,
                                                  mz, mh, init_meta, tint, logfile)

       frac_coeff[ist:ifn,:] = fvals
       pion_coeff[ist:ifn,:] = pvals
       plt_coeff[ist:ifn]    = tvals[:,2]
       prb_coeff[ist:ifn]    = tvals[:,0]
       prc_coeff[ist:ifn]    = tvals[:,1]
       ptot_coeff[ist:ifn]   = tvals[:,3]

    # switch to 2D (dens,Te,stage) shape if all is true

    if all == True:
       frac_coeff = frac_coeff.reshape(len_dens, len_te, num_meta)
       pion_coeff = pion_coeff.reshape(len_dens, len_te, num_meta)
       plt_coeff  = plt_coeff.reshape(len_dens, len_te)
       prb_coeff  = prb_coeff.reshape(len_dens, len_te)
       prc_coeff  = prc_coeff.reshape(len_dens, len_te)
       ptot_coeff = ptot_coeff.reshape(len_dens, len_te)


    # prepare output and return

    c_stg = asarray([row.strip().decode() for row in poptit])    # bytes to string and remove \n

    frac = { 'stage' : c_stg,
             'ion'   : frac_coeff }

    energy = { 'stage' : c_stg,
               'ion'   : pion_coeff,
               'elt'   : plt_coeff,
               'erb'   : prb_coeff,
               'erc'   : prc_coeff,
               'total' : ptot_coeff
             }

    return frac, energy
