def xxelem(iz0):

    """

      PURPOSE    : convert atomic number to element name(s)

                   name = xxesym(6)     returns 'carbon'
                   s = xxesym([4, 74])           ['beryllium, 'tungsten']


                    NAME       TYPE     DETAILS
      REQUIRED   :  iz0        int()    atomic number

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    14-11-2019

    """

#------------------------------------------------------------------------------

    from numpy import asarray, atleast_1d

    names = ['hydrogen',   'helium',     'lithium',      'beryllium',    'boron',      'carbon',     'nitrogen',   'oxygen',
             'fluorine',   'neon',       'sodium',       'magnesium',    'aluminium',  'silicon',    'phosphorus', 'sulphur',
             'chlorine',   'argon',      'potassium',    'calcium',      'scandium',   'titanium',   'vanadium',   'chromium',
             'manganese',  'iron',       'cobalt',       'nickel',       'copper',     'zinc',       'gallium',    'germanium',
             'arsenic',    'selenium',   'bromine',      'krypton',      'rubidium',   'strontium',  'yttrium',    'zirconium',
             'niobium',    'molybdenum', 'technetium',   'ruthenium',    'rhodium',    'palladium',  'silver',     'cadmium',
             'indium',     'tin',        'antimony',     'tellurium',    'iodine',     'xenon',      'cesium',     'barium',
             'lanthanum',  'cerium ',    'praseodymium', 'neodymium',    'promethium', 'samarium',   'europium',   'gadolinium',
             'terbium',    'dysprosium', 'holmium',      'erbium',       'thulium',    'ytterbium',  'lutetium',   'hafnium',
             'tantalum',   'tungsten',   'rhenium',      'osmium',       'iridium',    'platinum',   'gold',       'mercury',
             'thallium',   'lead',       'bismuth',      'polonium',     'astatine',   'radon',      'francium',   'radium',
             'actinium',   'thorium',    'protactinium', 'uranium']

    names = asarray(names)

    iz0 = atleast_1d(asarray(iz0, int))

    for iz in iz0:
        if iz < 1 or iz > 93: raise Exception('iz0 must be in range 1-92 (H to U)')

    result = names[iz0-1]

    if len(iz0) == 1:
        result = result[0]

    return result
