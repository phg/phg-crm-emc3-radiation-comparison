def continuo(wave, tev, iz0, iz1):
    """
      PURPOSE    : calculates continuum emission at a requested wavelength
                   and temperature for an element ionisation stage.

      contff, contin = continuo(wave, tev, iz0, iz1)

                    NAME         TYPE     DETAILS
      REQUIRED   :  wave()       float    wavelength required (A)
                    tev()        float    electron temperature (eV)
                    iz0          int      atomic number
                    iz1          int      ion stage + 1

      RETURNS       contff(,)    float    free-free emissivity (ph cm3 s-1 A-1)
                    contin(,)    float    total continuum emissivity
                                          (free-free + free-bound) (ph cm3 s-1 A-1)
                                              dimensions: wave, te (dropped if just 1).

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    16-06-2016

    """

    # External routines required

    from .continuo_if import continuo_if
    from numpy import asarray, zeros

    # Make internal variables from input

    iz0_int = int(iz0)
    iz1_int = int(iz1)

    wv_int = asarray(wave, float)
    te_int = asarray(tev, float)
    len_wv = wv_int.size
    len_te = te_int.size

    if len_wv == 1:
        wv_int = asarray([wave], float)
    if len_te == 1:
        te_int = asarray([tev], float)

    contin = zeros([len_wv, len_te])
    contff = zeros([len_wv, len_te])
    
    # continuo returns a value for just one wave/te pair
     
    for it in range(len_te):
        for iw in range(len_wv):
            val_ff, val_all = continuo_if(wv_int[iw], te_int[it], iz0_int, iz1_int)
            contin[iw,it] = val_all
            contff[iw,it] = val_ff
    
    # remove redundant dimensions

    contff = contff.squeeze()
    contin = contin.squeeze()

    return (contff, contin)
