def xxdtes(config):
    """
      PURPOSE    : Detects if the configuration string from a specific ion
                   level list line is of Eissner or standard form.

      is_eissner, is_bundle, is_parent, info = xxdtes(config)

                    NAME         TYPE     DETAILS
      REQUIRED   :  config       string   input configuration

      RETURNS       is_eissner   bool     True if config is in Eissner format
                    is_bundle    bool     True if config is in bundled form ('*' found)
                    is_parent    bool     True if parent form ('[...]' found)
                    info         dict      'cfg_top' : leading part of config. string in Eissner
                                                       format (no leading blank, trailing blanks)
                                           'cfg_tail': trailing part of config. string in Eissner
                                                       format (no leading blank, trailing blanks)
                                           'nvlce'   : outer electron n-shell if recognisable
                                                        -1 otherwise
                                           'lvlce'   : outer electron l-shell if recognisable
                                                        -1 otherwise
      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Set the return values to boolean rather than rely on
                       interpreting the int value returned from fortran.

      VERSION    :
            1.1    06-07-2018
            1.2    10-07-2018

    """

    # External routines required

    from .xxdtes_if import xxdtes_if

    # Internal variables - incoming config must be padded to 99 characters
    #                      and have a leading space.

    leiss = False
    lstan = False
    lbndl = False
    lprnt = False
    
    cfg      = ' ' + "{:<98}".format(config)
    cfg_top  = ' ' * 19
    cfg_tail = ' ' * 99
    
    (leiss, lstan, lbndl, lprnt, nvlce, lvlce) = xxdtes_if(cfg, cfg_top, cfg_tail)

    info = { 'cfg_top'  : cfg_top,
             'cfg_tail' : cfg_tail,
             'nvlce'    : nvlce,
             'lvlce'    : lvlce }

    return (bool(leiss), bool(lbndl), bool(lprnt), info)
