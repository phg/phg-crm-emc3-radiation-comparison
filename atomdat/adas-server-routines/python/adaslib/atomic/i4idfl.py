def i4idfl(n,l):
    """"
    PURPOSE: Returns a unique index number based on the value of the
             n and l quantum numbers passed to it. The index is used to
             reference arrays containing data dependent on the n and l
             quantum numbers.

    INPUTS: n,l - principal and orbital quantum numbers

    EXAMPLE: print i4idfl(4, 2)
                9

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    02-07-2018

    """

    index = ((n * (n - 1)) // 2) + l + 1

    return index
