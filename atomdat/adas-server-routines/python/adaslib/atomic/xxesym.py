def xxesym(iz0):

    """

      PURPOSE    : convert atomic number to element symbol(s)

                   s = xxesym(6)             returns 'C'
                   s = xxesym([4, 74])               ['Be', 'W']


                    NAME       TYPE     DETAILS
      REQUIRED   :  iz0        int()    atomic number

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    17-09-2019

    """

#------------------------------------------------------------------------------

    from numpy import asarray, atleast_1d, arange

    symbols = ['H',  'He', 'Li', 'Be', 'B',  'C',  'N',  'O',  'F',  'Ne', 'Na',
               'Mg', 'Al', 'Si', 'P',  'S',  'Cl', 'Ar', 'K',  'Ca', 'Sc', 'Ti',
               'V',  'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge',
               'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y',  'Zr', 'Nb', 'Mo',
               'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te',
               'I',  'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm',
               'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf',
               'Ta', 'W',  'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb',
               'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U' ]

    symbols = asarray(symbols)

    iz0 = atleast_1d(asarray(iz0, int))

    for iz in iz0:
        if iz < 1 or iz > 93: raise Exception('iz0 must be in range 1-92 (H to U)')

    result = symbols[iz0-1]

    if len(iz0) == 1:
        result = result[0]

    return result
