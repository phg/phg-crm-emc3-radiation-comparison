def i4eiz0(esym):
    """"
    PURPOSE: Returns the nuclear charge of esym, the element symbol.

    INPUTS: esym - atomic symbol, eg Ar for argon.

    EXAMPLE: print i4eiz0('Ni')
                28
             print i4eiz0('Xs')
                -1
    """

    from numpy import minimum, mod

    symbol = ['H','HE','LI','BE','B','C','N','O','F','NE','NA','MG',
              'AL','SI','P','S','CL','AR','K','CA','SC','TI','V','CR',
              'MN','FE','CO','NI','CU','ZN','GA','GE','AS','SE','BR',
              'KR','RB','SR','Y','ZR','NB','MO','TC','RU','RH','PD',
              'AG','CD','IN','SN','SB','TE','I','XE','CS','BA','LA',
              'CE','PR','ND','PM','SM','EU','GD','TB','DY','HO','ER',
              'TM','YB','LU','HF','TA','W','RE','OS','IR','PT','AU',
              'HG','TL','PB','BI','PO','AT','RN','FR','RA','AC','TH','P']

    etest = esym.upper()
    etest = "".join(etest.split())

    try:
        ind = symbol.index(etest) + 1
    except:
        ind = -1

    return ind
