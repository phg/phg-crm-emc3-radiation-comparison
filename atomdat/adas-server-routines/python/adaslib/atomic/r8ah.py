def r8ah(nu , lu , nl , ll):

    """

      PURPOSE    : Calculates the A-value for hydrogen transitions

                   a = r8ah(8, 5, 7, 4, nn=nn)
                   returns 209295.33/s for the (n=7,l=7) to (n=7,l=4)
                   transition A-value.

                    NAME       TYPE     DETAILS
      REQUIRED   :  nu         int      upper value of N quantum number
                    lu         int      upper value of L quantum number
                    nl         int      lower value of N quantum number
                    ll         int      lower value of L quantum number

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    03-04-2019

    """

#------------------------------------------------------------------------------

    from adaslib import r8rd2b

    p1 = 2.67744e+09

    # Check for valid nl - n'l' transition

    ltest = ( (nu < 2) or (lu < 0) or (lu >= nu) or
              (nl < 1) or (ll < 0) or (ll >= nl) or
              (nl >= nu)
            )

    if (ltest):

        r8ah = 0.0

    else:

        xnu = float(nu)
        xlu = float(lu)
        xnl = float(nl)
        xll = float(ll)

        t1   = p1 / (2.0 * xlu + 1.0)
        de   = (1.0 / xnl**2) - (1.0 / xnu**2)

        r8ah = t1 * max([xlu , xll]) * de**3 * r8rd2b(nu , lu , nl ,ll)

    return r8ah
