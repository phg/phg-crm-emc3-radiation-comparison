def xxcftr(config, transform=1):
    """
      PURPOSE    : Converts a configuration character string, such as occurs
                   in a specific ion file level list, between Eissner and
                   standard forms.

      config_new = xxcftr(config, transform=1)

                    NAME         TYPE     DETAILS
      REQUIRED   :  config       string   input configuration


      RETURNS       config_new   string   transformed configuration
                    transform    int      type of conversion
                                           = 1 => standard form out, standard form in
                                             2 => Eissner  form out, standard form in
                                             3 => standard form out, Eissner  form in
                                             4 => Eissner  form out, Eissner  form in

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    06-07-2018

    """

    # External routines required

    from .xxcftr_if import xxcftr_if

    # check for valid transform value

    icfsel = int(transform)
    if icfsel < 1 or icfsel > 4:
       raise Exception('Conversion flag must be an integer between 1 and 4')

    # input string length is arbitrary - use 64 to transfer to fortran
    # ans convert result to string for return

    config  = "{:<64}".format(config)
    cfg_out = ' ' * 64
    res = ' ' * 64

    res = xxcftr_if(icfsel, config, cfg_out)
    res = res.decode()

    return res
