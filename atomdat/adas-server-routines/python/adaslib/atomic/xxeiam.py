def xxeiam(esym):

    """

      PURPOSE    : returns the atomic mass number for an element(s)

                   mass = xxeiam('C')          returns double : 12.001
                   mass = xxeiam(['He', 'Be'])         array  : [4.0026 , 9.01218]

                   Returns 0.0 if element symbol is not recognized.

                    NAME       TYPE     DETAILS
      REQUIRED   :  esym       str()    standard element symbol

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    29-03-2019

    """

#------------------------------------------------------------------------------

    from numpy import asarray

    s2m = {'H'  :    1.00794,
           'D'  :    2.0,
           'T'  :    3.0,
           'HE' :    4.0026,
           'LI' :    6.941,
           'BE' :    9.01218,
           'B'  :   10.81,
           'C'  :   12.011,
           'N'  :   14.0067,
           'O'  :   15.9994,
           'F'  :   18.998403,
           'NE' :   20.179,
           'NA' :   22.98977,
           'MG' :   24.305,
           'AL' :   26.98154,
           'SI' :   28.0855,
           'P'  :   30.97376,
           'S'  :   32.06,
           'CL' :   35.453,
           'AR' :   39.948,
           'K'  :   39.0983,
           'CA' :   40.08,
           'SC' :   44.9559,
           'TI' :   47.88,
           'V'  :   50.9415,
           'CR' :   51.996,
           'MN' :   54.938,
           'FE' :   55.847,
           'CO' :   58.9332,
           'NI' :   58.69,
           'CU' :   63.546,
           'ZN' :   65.38,
           'GA' :   69.72,
           'GE' :   72.59,
           'AS' :   74.9216,
           'SE' :   78.96,
           'BR' :   79.904,
           'KR' :   83.8,
           'RB' :   85.4678,
           'SR' :   87.62,
           'Y'  :   88.9059,
           'ZR' :   91.22,
           'NB' :   92.9064,
           'MO' :   95.94,
           'TC' :   98.0,
           'RU' :  101.07,
           'RH' :  102.9055,
           'PD' :  106.42,
           'AG' :  107.8682,
           'CD' :  112.41,
           'IN' :  114.82,
           'SN' :  118.69,
           'SB' :  121.75,
           'TE' :  127.6,
           'I'  :  126.9045,
           'XE' :  131.3,
           'CS' :  132.9054,
           'BA' :  137.33,
           'LA' :  138.9055,
           'CE' :  140.12,
           'PR' :  140.9077,
           'ND' :  144.24,
           'PM' :  145.0,
           'SM' :  150.4,
           'EU' :  151.96,
           'GD' :  157.25,
           'TB' :  158.9254,
           'DY' :  162.5,
           'HO' :  164.9304,
           'ER' :  167.26,
           'TM' :  168.9342,
           'YB' :  173.04,
           'LU' :  174.97,
           'HF' :  178.49,
           'TA' :  180.9479,
           'W'  :  183.85,
           'RE' :  186.2,
           'OS' :  190.2,
           'IR' :  192.22,
           'PT' :  195.09,
           'AU' :  196.9665,
           'HG' :  200.59,
           'TL' :  204.37,
           'PB' :  207.2,
           'BI' :  208.9808,
           'PO' :  210.0,
           'AT' :  210.0,
           'RN' :  222.0,
           'FR' :  223.0,
           'RA' :  226.0254,
           'AC' :  227.0,
           'TH' :  232.0381,
           'PA' :  231.0359,
           'U'  :  238.029 }

    if not isinstance(esym, (list, tuple)):
        esym_l = [esym]
        one    = True
    else:
        esym_l = esym
        one    = False

    e_search    = [x.upper() for x in esym_l]
    default_val = 0.0
    result      = asarray([s2m.get(k, default_val) for k in e_search])

    if one:
        result = result[0]

    return result
