def xxidtl(index):
    """"
    PURPOSE: Inverse function of i4idfl which returns the n and l
             quantum numbers of a unique index.

    INPUT  : index
    OUTPUTS: n,l - principal and orbital quantum numbers

    EXAMPLE: print xxidtl(9)
                (4, 2)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    02-07-2018

    """

    from adaslib import i4idfl

    n = 1
    l = 0

    while i4idfl(n+1,l) <= index:
        n = n + 1

    while i4idfl(n,l+1) <= index:
        l = l + 1

    return n, l
