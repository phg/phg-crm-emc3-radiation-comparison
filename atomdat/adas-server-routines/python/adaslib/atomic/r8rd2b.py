def r8rd2b(nu , lu , nl , ll):

    """

      PURPOSE    : Calculates hydrogenic bound-bound radial integrals using
                   recurrence relations.

                   a = r8rd2b(8, 5, 7, 4)
                   returns 1571.5089

                    NAME       TYPE     DETAILS
      REQUIRED   :  nu         int      upper value of N quantum number
                    lu         int      upper value of L quantum number
                    nl         int      lower value of N quantum number
                    ll         int      lower value of L quantum number

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    03-04-2019

    """

#------------------------------------------------------------------------------

    from numpy import sqrt

    p1  = 64.0
    p12 = p1**2
    p2  = 1.0 / p1
    p22 = p2**2

    # Check for valid nl - n'l' transition

    ltest = ( (nu < 2) or (lu < 0) or (lu >= nu) or
              (nl < 1) or (ll < 0) or (ll >= nl) or
              (nl >= nu)
            )

    if (ltest):

        r8rd2b = 0.0

        return r8rd2b

    else:

        xnu = float(nu)
        xnl = float(nl)

        xnl2 = xnl**2
        xnu2 = xnu**2

        v  = 1.0 - (xnl2 / xnu2)
        w  = (1.0 + (xnl / xnu) )**2
        u  = 8.0 * (xnl2 / w**2)
        p  = 1.0
        js = 0

        for i in range(1, nl+1):
            xi = float(i)
            p  = p * u * (1.0 - (xi**2 / xnu2)) / (xi * (2.0 * xi - 1.0))
            if (abs(p) < p22):
                js = js - 1
                p  = p * p12


        p = 2.0 * p * xnl / xnu
        p = 4.0 * sqrt( p ) / ( xnu * v**2 )

        for i in range(nl+1, nu+1):
            p  = p * v / w
            if (abs( p ) < p2):
                js = js - 1
                p  = p * p1


        r8rd2b = 0.0

        # delta_l = 1 for lu > ll

        if (lu - ll) == 1:

            t2 = p

            if (nl - ll) < 2:

                t3 = t2

            else:

                u  = sqrt( ( 2.0 * xnl - 1.0 ) * v )
                t3 = u * t2 / 2.0
                for i in range(nl-2, ll, -1):
                     xi   = float(i)
                     xi1  = float(i + 1)
                     xi12 = xi1**2
                     t1   = t2
                     t2   = t3
                     t3   = ( t2 * ( 4.0 * ( xnl2 - xi12 ) +
                              xi1 * ( 2.0 * xi1 - 1.0 ) * v ) -
                             (2.0 * xnl * u * t1) )
                     u    = sqrt( (xnl2 - xi**2) * ( 1.0 - xi12 / xnu2 ) )
                     t3   = t3 / ( 2.0 * xnl * u )
                     if abs(t3) > p1:
                         js = js + 1
                         t3 = t3 * p2
                         t2 = t2 * p2

            r8rd2b = xnl2**2 * t3**2 * p12**js


        # delta_l = 1 for lu < ll

        if (ll - lu) == 1:

            t2   = p
            xnl1 = float( nl - 1 )
            u    = sqrt( v / ( 1.0 - xnl1**2 / xnu2 ) )
            t2   = t2 * u / ( 2.0 * xnl )

            if (nl - ll) < 2:

              t3 = t2

            else:

                u  =  ( sqrt( (2.0 * xnl - 1.0) *
                           (1.0 - ( xnl - 2.0 )**2 / xnu2 ) ) )
                t3 = ( ( 4.0 + xnl1 * v ) * ( 2.0 * xnl - 1.0 ) *
                       t2 / ( 2.0 * xnl * u ))

                for i in range(nl-3, ll-1, -1):
                     xi   = float(i)
                     xi1  = float(i + 1)
                     xi12 = xi1**2
                     t1   = t2
                     t2   = t3
                     t3   = ( t2 * ( 4.0 * ( xnl2 - xi12 ) +
                              xi1 * ( 2.0 * xi1 + 1.0 ) * v ) -
                              2.0 * xnl * u * t1 )
                     u    = sqrt( (xnl2 - xi12 ) * ( 1.0 - xi**2 / xnu2 ) )
                     t3   = t3 / ( 2.0 * xnl * u )

                     if abs(t3) > p1:
                         js = js + 1
                         t3 = t3 * p2
                         t2 = t2 * p2


            r8rd2b = xnl2**2 * t3**2 * p12**js


    return r8rd2b
