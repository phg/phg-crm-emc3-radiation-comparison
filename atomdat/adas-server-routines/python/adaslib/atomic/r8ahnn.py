def r8ahnn(nu,  nl):

    """

      PURPOSE    : Calculates the A-value for hydrogen n-n' transitions

                   a, f = r8ahnn(8, 7)
                   returns 227322.94/s for the n=8-7 multiplet A-value
                   and 1.6156667 for the oscillator strength.

                    NAME       TYPE     DETAILS
      REQUIRED   :  nu         int      upper value of N quantum number
                    nl         int      lower value of N quantum number

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    03-04-2019

    """

#------------------------------------------------------------------------------

    from adaslib import r8rd2b, r8ah

    # Check for valid n-n' transition

    ltest = (nu < 2) or (nl < 1) or (nl >= nu)

    if (ltest):

        r8ahnn = 0.0

    else:

        a_nn = 0.0
        wt   = 0.0

        for lu in range(nu):
            wt = wt + 2.0 * (2.0*lu + 1)

        for lu in range(nl+1):
           for ll in [lu-1, lu+1]:
              if (ll >= 0) and (ll < nl):
                  a_nn = a_nn + 2.0 * (2.0*lu + 1)  * r8ah(nu,lu,nl,ll)

        nn = a_nn / wt

        e   = 1.0 / nl**2.0
        wi  = 2.0*nl**2
        e11 = 1.0 / nu**2.0
        wj  = 2.0*nu**2
        lam = 1.0e8 / ((e - e11)*109737.0)
        fn  = nn * lam**2 * wj / (wi * 6.6702e15)

    return nn, fn
