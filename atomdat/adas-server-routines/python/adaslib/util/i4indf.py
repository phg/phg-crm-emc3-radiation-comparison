def i4indf(array, value):
    """
    PURPOSE    :  Find the index of the array closest to the requested value
    
    res = i4indf(array, 12.3)
    
    INPUTS:
          ARRAY  -  vector of floats.
          VALUE  -  find the index of array closest to this value.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    20-10-2017

    """
    
    from numpy import asarray, abs
    
    array = asarray(array)
    index = abs(array-value).argmin()
    
    return index
