def array2strlist(array, fmt=None, num_col=8):
    """
      PURPOSE    :  Convert a numpy array to a list of strings according
                    to a format string

      lines = array2strlist([2.3,5.5,8.5,12.0,4], fmt='{:9.2e}', num_col=3)

      for l in lines: print(l) # returns

        2.30e+00  5.50e+00  8.50e+00
        1.20e+01  4.00e+00

                    NAME         TYPE        DETAILS
      REQUIRED   :  array        int/fooat   numpy array
                    fmt          str         a valid numeric format (d,f,e,E)
                    num_col      int         required number of colums in the
                                             output, defaults to 8

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - For dtypes which are not float or integer set array_kind
                       to None to fall back to default formatting.
           1.3     Martin O'Mullane
                     - numpy.array2string has a default limit of 1000 array elements
                       where it summarizes rather than outputs all the elements. For
                       numpy versions up to v1.13, printoptions must be changed to remove
                       this limit. For v1.14 numpy.array2string has extra keyword options.
           1.4     Martin O'Mullane
                     - set legacy='1.13' in array2string rather than True which is
                       not a valid option.

      VERSION    :
            1.1    25-07-2018
            1.2    26-07-2018
            1.3    27-07-2018
            1.4    12-11-2019

    """

    # External routines

    import numpy as np
    from distutils.version import LooseVersion

    new_options = LooseVersion(np.__version__) >= LooseVersion('1.14')

    # Make sure the incoming array is a numpy array and flatten it
    # to deal with more than one dimension

    array = np.asarray(array).flatten()

    # Find length of column from first element of the array

    length = len(fmt.format(array[0]))

    # Calculate max_line_width from number of columns and account
    # for the space inserted between elements by the numpy
    # array2string function

    max_lw = (length + 1) * num_col

    # Determine the python type from the numpy type for the formatter

    np_kind = str(array.dtype)
    array_kind = None
    if 'float' in np_kind:
        array_kind = 'float'
    elif 'int' in np_kind:
        array_kind = 'int'

    fmtr = {array_kind:lambda x: fmt.format(x)}


    # Convert the array into a string

    if new_options:
        a_str = np.array2string(array, max_line_width=max_lw, formatter=fmtr, threshold=np.inf, legacy='1.13')
    else:
        thresh_val = np.get_printoptions()['threshold']
        np.set_printoptions(threshold=np.inf)

        a_str = np.array2string(array, max_line_width=max_lw, formatter=fmtr)

        np.set_printoptions(threshold=thresh_val)


    # remove the '[' and ']' brackets, add a space at the start and finally split into a list

    all = (' ' + a_str[1:-1]).splitlines()

    return all
