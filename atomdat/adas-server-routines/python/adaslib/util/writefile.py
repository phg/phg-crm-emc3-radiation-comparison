def writefile(file=None, content=None, comments=None, trim=True, append=False):
    """"
      PURPOSE: Write (or append) a list of strings to a named file.

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         name of file.
                    content      list        a list of strings to be
                                             written to file.
                    comments     list        another list of strings added at
                                             the end as comments.
      OPTIONAL   :  trim         bool        remove whitespace at end of line
                                             (defaults to True)
                    append       bool        Add content to the end of an existing
                                             file (default is False)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    13-11-2019

    """

    # Test that content is a non-empty list of (unicode) strings

    if not (isinstance(content, list) and all(isinstance(line, str) for line in content)):
        raise TypeError('content must be a list of one or more strings.')

    if len(content) == 0:
        raise Exception('At least one entry in content is required')


    # Add comments if supplied

    if comments:
        if not (isinstance(comments, list) and all(isinstance(line, str) for line in comments)):
            raise TypeError('comments must be a list of one or more strings.')
        content = content + comments


    # Write the file

    if append:
        try:
            out_lun = open(file, 'a')
        except IOError:
            raise Exception('Cannot append to this file: ', file)
    else:
        try:
            out_lun = open(file, 'w+')
        except IOError:
            raise Exception('Cannot write to this file: ', file)

    for line in content:
        if trim: line = line.rstrip()
        out_lun.write(line + '\n')

    out_lun.close()
