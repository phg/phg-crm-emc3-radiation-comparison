def readfile(file=None):
    """"
      PURPOSE: Reads a text file into a list of strings and
               returns this and the number of lines.

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         name of file.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    23-11-2019

    """
    
    import os

    # Examine inputs

    if not file:
        raise Exception('A valid file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the requested file is invalid')

    # Read in file and parse resulting array of string

    f = open(file, 'r')
    lines = f.read().splitlines()
    f.close()

    nlines = len(lines)

    return lines, nlines
