def strlist2array(inlist):
    """
      PURPOSE    :  Convert a list of strings containing numbers, typically
                    from an adf file to a numpy array. Inverse routine
                    of array2strlist.

                    data = ['2.30e+00  5.50e+00  8.50e+00', '1.20e+01  4.00e+00']

                    print(strlist2array(data))

                    [2.30e+00, 5.50e+00, 8.50e+00, 1.20e+01, 4.00e+00]

                    It returns a numpy array of floats irrespective
                    of the representation of the data in the strings.

                    NAME         TYPE        DETAILS
      REQUIRED   :  inlist       list        list of strings

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    16-11-2019

    """

    # External routines

    from numpy import asarray
    

    # Flatten into list of strings - if a single string is input make
    # it a one element list.
    
    if isinstance(inlist, str):
        inlist = [inlist]

    # Replace D and d in scientific notation with e
    
    inlist = [w.replace('D', 'E') for w in inlist]

    long_list = []
    for line in inlist:
        for s in line.split():
            long_list.append(s)

    # Convert to numpy float array

    res = asarray(long_list, dtype=float)

    return res
