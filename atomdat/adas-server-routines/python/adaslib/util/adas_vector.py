def adas_vector(low=0.0, high=100.0, num=10, linear=False):
    """
    PURPOSE    : Generate a log spaced vector of values given the
                 minimum, maximum and number of points required.

    res = adas_vector(low=1, high=900, num=30)

    INPUTS:
          LOW    -  minimum value.
          HIGH   -  maximum value.
          NUM    -  number of points.
          LINEAR -  True for a linear distribution, default is False

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    20-08-2012

    """

    from numpy import arange, math, exp

    if num == 1:
        coeff = -1
        return coeff

    if not linear:
        coeff = math.log(low) + arange(num, dtype=float)/float(num-1) * (math.log(high)-math.log(low))
        coeff = exp(coeff)
    else:
        coeff = low + arange(0,num) * (high-low) / (num-1)

    return coeff
