def list_index_str(inlist, substr, pos=None):
    """
      PURPOSE    :  Given a list of strings return a list of
                    indices which contain substr. This is
                    an implementation of the IDL patterns
                      index = where(strpos(lines, substr) NE -1, count)
                      index = where(strpos(lines, substr) EQ pos, count)

                    index, count = list_index_str(lines, substr)

                    NAME         TYPE        DETAILS
      REQUIRED   :  inlist       list        list of strings
      OPTIONAL   :  pos          int         match if the position of
                                             substr is pos.

      RETURNS    :  index        list        indices of lines which contain
                                             substr.
                                              -1 : if no match

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    16-11-2019

    """

    count = 0
    k = 0
    ind = []

    if pos is None:
        for line in inlist:
            if substr in line:
                ind.append(k)
                count = count + 1
            k = k + 1
    else:
        for line in inlist:
            if line.find(substr) == pos:
                ind.append(k)
                count = count + 1
            k = k + 1

    if count == 0:
        ind = -1

    return ind, count
