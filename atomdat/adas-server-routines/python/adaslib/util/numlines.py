def numlines(ndim, llength):
    """"
    PURPOSE: Calculates how many lines in a text file are occupied by a 
             vector given a fixed number of enteries per line.
    
    INPUTS: ndim    - size of vector.
            llenght - number of numbers permitted in a line.

    EXAMPLE: Say a vector of 25 elements and 7 numbers permitted per line
                     1 2 3 4 5 6 7 
                     7 6 5 4 3 2 1
                     1 2 3 4 5 6 7
                     1 2 3 4
    
    print (numlines(25,7)) gives 4
    
    MODIFIED   :
         1.1     Martin O'Mullane
                   - First version
         1.2     Martin O'Mullane
                   - Use floor division since python 3 give float for /

    VERSION    :
          1.1    20-08-2012
          1.2    16-01-2014

    """

    from numpy import minimum, mod
    
    res = (ndim // llength) + minimum(1, mod(ndim,llength))
    
    return res
