def xxtrisol(a, b, c, rhs):
    """
      PURPOSE    : solves the tridiagonal systems of linear equations

                   [b0 c0 0 ...           ] [u0]   [r0]
                   [a0 b1 c1 0 ...        ] [ :]   [ :]
                   [ 0 a1 b2 c2 0 ...     ] [  ] = [  ]
                   [                      ] [  ]   [  ]
                   [ ...  0 an-1 bn-1 cn-1] [ :]   [ :]
                   [        ... 0 an-1 bn ] [un]   [rn]

      result = xxtrisol(a, b, c, rhs)

                    NAME       TYPE     DETAILS
      REQUIRED   :  a          float()  vector of sub-diagonal elements
                    b          float()  vector of diagonal elements
                    c          float()  vector of super-diagonal elements
                    rhs        float()  vector of right hand side elements

      Based on the IDL (trisol) and Numerical Recipes (tridag) way of
      specifying the tri-diagonal matrix.

      Coding hints from:
         http://jean-pierre.moreau.pagesperso-orange.fr/Fortran/tridiag_f90.txt
         http://userpages.irap.omp.eu/~bdintrans/docs/tridag.py

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    29-03-2019

    """

    # External routines required

    import numpy as np

    # Condition input

    a   = np.asarray(a, float)
    b   = np.asarray(b, float)
    c   = np.asarray(c, float)
    rhs = np.asarray(rhs, float)

    # Setup conditions

    beta = b[0]
    if b[0] == 0.0:
        raise Exception('The matrix is not invertable')

    u   = np.zeros_like(rhs)
    n   = len(b)
    tmp = np.asarray(np.zeros(n), float)

    u[0] = rhs[0] / beta

    # De-composition and forward substitution

    for j in range(1, n):

       tmp[j] = c[j-1] / beta
       beta   = b[j] - a[j] * tmp[j]
       if beta == 0.0:
           raise Exception('Solution method has failed')
       u[j] = (rhs[j] - a[j] * u[j-1]) / beta

    # Back substitute

    for j in range(n-2, -1, -1):
        u[j] -= tmp[j+1] * u[j+1]

    return u
