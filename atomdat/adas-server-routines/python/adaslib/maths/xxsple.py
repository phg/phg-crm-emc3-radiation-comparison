def xxsple(xin, yin, xout, opt=0, log=False):
    """
      PURPOSE    : Interpolates and extrapolates with cubic splines. This
                   is a direct translation from the fortran code.

      yout, info = xxsple(xin, yin, xout, opt=4, log=True)

                    NAME         TYPE     DETAILS
      REQUIRED   :  xin()        float    independent variable
                    yin()        float    dependent variable
                    xout()       float    requested points

      OPTIONAL   :  log          bool     if True convert spline in
                                          log space.
                    opt          int      extrapolation behaviour. The
                                          default is 1 which sets a
                                          constant slope at each end.
      RETURNS    :  yout()       float    splined points for xin()
                    info         dict     dy()     : derivatives at input knots
                                          extrap() : flag if point extrapolated
                                          interp() : flag if point interpolated

                 -------------------------------------------------------
                 | IOPT  |  DY(1)  DDY(1)  |  DY(N)   DDY(N)  |EXTRAP'N|
                 |-------|-----------------|------------------|--------|
                 | < 0   |    -     0.0    |    -      0.0    |  NO    |
                 |   0   |    -     0.0    |    -      0.0    |  YES   |
                 |   1   |    -     0.0    |  -1.5      -     |  YES   |
                 |   2   |   0.0     -     |   1.0      -     |  YES   |
                 |   3   |  -0.5     -     |  -1.5      -     |  YES   |
                 |   4   |   0.0     -     |    -      0.0    |  YES   |
                 |   5   |  -4.5     -     |  -1.5      -     |  YES   |
                 |   6   |  +0.5     -     |    -      0.0    |  YES   |
                 |   7   |  -3.5     -     |    -      0.0    |  YES   |
                 -------------------------------------------------------


      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Handle case when xout is a scalar.

      VERSION    :
            1.1    14-07-2018
            1.2    14-08-2019

    """

    # External routines required

    import numpy as np

    # Internal variables

    iopt     = np.int(opt)

    xx_xout  = np.atleast_1d(np.asarray(xout, float))
    xx_xin   = np.asarray(xin, float)
    xx_yin   = np.asarray(yin, float)

    nout     = xx_xout.size
    xx_yout  = np.atleast_1d(np.asarray(np.zeros(nout), float))
    interp   = np.atleast_1d(np.asarray(np.zeros(nout), int))

    # log or linear

    if log:

       xx_xout = np.log(xx_xout)
       xx_xin  = np.log(xx_xin)
       xx_yin  = np.log(xx_yin)


    # Set up internal parameters

    qval  = np.asarray([ -0.5, -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0], float)
    d2val = np.asarray([  1.5,  1.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0], float)
    d3val = np.asarray([  0.0,  0.0,  0.0, -0.5,  0.0, -4.5,  0.5, -3.5], float)
    uval  = np.asarray([  0.0, -1.5,  1.0, -1.5,  0.0, -1.5,  0.0,  0.0], float)
    luval = np.asarray([    1,    0,    0,    0,    1,    0,    1,    1], float)

    agrl = np.asarray(np.zeros(4), float)

    # Set up arrays for splining

    nin    = xx_xin.size
    dy     = np.asarray(np.zeros(nin), float)

    nknots = nin + 1
    x      = np.asarray(np.zeros(nknots), float)
    dely   = np.asarray(np.zeros(nknots), float)
    h      = np.asarray(np.zeros(nknots), float)
    q      = np.asarray(np.zeros(nknots), float)
    u      = np.asarray(np.zeros(nknots), float)
    d1     = np.asarray(np.zeros(nknots), float)
    d2     = np.asarray(np.zeros(nknots), float)
    d3     = np.asarray(np.zeros(nknots), float)

    nopt   = max(0, iopt)

    x      = xx_xin
    h[1]   = x[1] - x[0]
    q[0]   = qval[nopt]
    d2[0]  = d2val[nopt] / h[1]
    d3[0]  = d3val[nopt]

    nin0   = nin - 1

    for i in np.arange(1, nin0):
        h[i+1] = x[i+1] - x[i]
        t1     = 1.0 / ( h[i+1] + h[i] )
        t2     = h[i+1] * t1
        t3     = 1.0 - t2
        t4     = 1.0 / ( ( t2 * q[i-1] ) + 2.0 )
        q[i]   = -t3 * t4
        d1[i]  = ( 3.0 * t4 * t2 ) / h[i]
        d2[i]  = ( 3.0 * t4 * t3 ) / h[i+1]
        d3[i]  =  t2 * t4


    t4        = 1.0 / ( q[nin0-1] + 2.0 )
    d1[nin-1] = ( 3.0 * t4 ) / h[nin-1]
    d3[nin-1] = t4


    # Setup cubic spline derivatives

    dely[1] = xx_yin[1] - xx_yin[0]
    u[0]    = ( d2[0] * dely[1] ) + d3[0]

    for i in np.arange(1, nin0):
        dely[i+1] = xx_yin[i+1] - xx_yin[i]
        u[i]      = ( d1[i] * dely[i] ) + ( d2[i] * dely[i+1] ) - ( d3[i] * u[i-1] )


    if luval[nopt]:
        u[nin-1] = ( d1[nin-1] * dely[nin-1] ) - ( d3[nin-1] * u[nin0-1] )
    else:
        u[nin-1] = uval[nopt]

    dy[nin-1] = u[nin-1]

    for i in np.arange(nin0-1, -1, -1):
        dy[i] = ( q[i] * dy[i+1] ) + u[i]


    # Set up parameters relating to the requested 'xx_xout' array values

    for k in np.arange(0, nout):

        xk  = xx_xout[k]
        xkk = xk

        # interpolation test

        if  xk >= xx_xin[nin-1]:

            inter     = nin - 1
            interp[k] = 0
            agrl[0]   = 0.0
            agrl[2]   = 0.0
            if xk == xx_xin[nin-1]:
                interp[k] = 1
                agrl[1]   = 0.0
                agrl[3]   = 1.0
            else:
                if iopt >= 0:
                    agrl[1] = xkk - x[nin-1]
                    agrl[3] = 1.0
                else:
                    agrl[1] = 0.0
                    agrl[3] = 0.0


        if xk <= xx_xin[0]:

            inter     = 1
            interp[k] = 0

            agrl[1]   = 0.0
            agrl[3]   = 0.0
            if xk == xx_xin[0]:
                interp[k] = 1
                agrl[0]   = 0.0
                agrl[2]   = 1.0
            else:
                if iopt >= 0:
                    agrl[0]   = xkk-x[0]
                    agrl[2]   = 1.0
                else:
                    agrl[0]   = 0.0
                    agrl[2]   = 0.0


        if (xk > xx_xin[0] and xk < xx_xin[nin-1]):

             # interpolate

             for i in np.arange(nin-1, -1, -1):
                 if xk < xx_xin[i]: inter = i

             interp[k] =  1
             dl1       =  ( x[inter] - xkk    ) / h[inter]
             dl2       =  1.0 - dl1
             dl3       =  h[inter] * dl1 * dl2
             agrl[0]   =  dl1 * dl3
             agrl[1]   = -dl2 * dl3
             agrl[2]   =  dl1 * dl1 * ( 1.0 + dl2 + dl2 )
             agrl[3]   =  1.0 - agrl[2]


        # evaluate 'xx_yout'-value for requested 'xx_xout'-value

        xx_yout[k] = ( ( agrl[0] * dy[inter-1] )  + ( agrl[1] * dy[inter] ) +
                       ( agrl[2] * xx_yin[inter-1] ) + ( agrl[3] * xx_yin[inter] ) )



    # Return yout in correct form

    if log:
        yout = np.exp(xx_yout)
    else:
        yout = xx_yout

    # flags for intepolated/extrapolated points

    interp = np.asarray(interp, bool)
    extrap = np.logical_not(interp)

    # return results

    info = { 'dy'     : dy,
             'interp' : interp,
             'extrap' : extrap }

    return yout, info
