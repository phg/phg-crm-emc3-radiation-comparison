def adf11_nmeta(file):

    """

      PURPOSE    : Returns the number of metastables (including fully stripped)
                   from the first line in unresolved and bundled files and 
                   from the partition line(s) in a resolved adf11 file.

      num = adf11_nmeta(file)

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      fully qualified name of adf11 file

      OPTIONAL   :

      RETURNS    :  num        int      number of metastables

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Increase the number of dashes when searching
                       for the separation line in the partition.
           1.3     Martin O'Mullane
                     - Check whether it is a bundled adf11 file.
           1.4     Martin O'Mullane
                     - Account for files where the comments start
                       with a dashed line and not 'C. These are mostly
                       from the 93 GCR production and v2bndl/tcmprss3
                       code output.

      VERSION    :
            1.1    07-11-2018
            1.2    08-04-2019
            1.3    10-04-2019
            1.4    23-09-2019

    """

    # The partition vector is stored in a block between two sets of dashes.

    cdash = '-----------------------------------------------------------'
    k = 0
    with open(file, "r") as lun:
        array = []
        for line in lun:
            if cdash in line and line[0].lower() != 'c':  k = k + 1
            if k == 1: array.append(line.rstrip('\n'))
            if 's1' in line.lower(): break
            if 'z1' in line.lower(): break
            if k > 2: break
    
    # For bundled adf11 file use same method as unresolved datasets so
    # ignore the more elaborate partition vector and reset k to 1.
     
    if k > 1:
        if '//#' in array[1]: k = 1


    # Extract the number of stages including fully stripped
             
    if k == 1:
        
        # unresolved adf11
    
        with open(file, 'r') as f:
            first_line = f.readline()

        val  = [int(e) for e in first_line.split(' ') if e.isdigit()]
        num = val[4] - val[3] + 2
    
    else:

        # Drop first element of list (dashes) and combine the rest into
        # a single string, iterate over this to extract values and sum.

        p_line = "".join(array[1:])

        num = 0
        for x in p_line.split(): num = num + int(x)

    return num
