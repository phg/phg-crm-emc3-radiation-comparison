def write_adf37(file=None, fulldata=None, comments=None):
    """
      PURPOSE    :  Write an adf37 electron distribution file from a user
                    supplied fulldata dictionary.

      write_adf37(fulldata=my_eedf, file='my_eedf.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of the output adf37 file
                    comments     list        write this list with first character 
                                             'C' for each entry at the end of the
                                             file as a comment.
                    fulldata     dict        all data in the file data

                    fulldata = { 'title'      :  header for file
                                 'icateg'     :  category of file
                                                   1 => superposition
                                                   2 => numerical
                                 'ieunit'     :  energy units of distribution function
                                                   1 => kelvin
                                                   2 => eV
                                 'nenerg'     :  type 1 => number of distribution families
                                                      2 => number of energy points
                                 'nblock'     :  type 1 => number of members in output family
                                                      2 => number of effective temperatures
                                 'nform1'     :  type of threshold behaviour
                                                   1 => cutoff
                                                   2 => energy^param1
                                 'nform2'     :  type of high-energy behaviour
                                                   1 => cutoff
                                                   2 => energy^(-param2[0])
                                                   3 => exp(-param2[0]*energy)
                                                   4 => exp(-param2[0]*energy^param2[1])
                                 'param1'     :  parameter of threshold form
                                 'param2'     :  parameter of high-energy form
                                 'ea'         :  energy points of tabulation
                                                   array(nblock,nenergy)
                                 'fa'         :  distribution function tabulation
                                                   array(nblock,nenergy)
                                 'mode_eng'   :  most probable energy (eV), array(nblock)
                                 'median_eng' :  median energy (eV), array(nblock)
                                 'teff'       :  effective temperature (eV), array(nblock)

      NOTES      :  'ea' and 'fa' should be numpy arrays of floats. If only one nblock is present
                    the shallow dimension need not be given. A 1D array of nenerg is sufficient.
                    'mode_eng', 'median_eng' and 'teff' are not required to write an adf37 file.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Allow ea and fa to be 1D vectors.
           1.3     Martin O'Mullane
                     - Use adas writefile function to write the file.

      VERSION    :
            1.1    23-07-2018
            1.2    26-07-2018
            1.3    13-11-2019

    """

    # External routines required
    
    from numpy import arange
    from adaslib import array2strlist, writefile


    # Put contents of fulldata into a list of string
    
    all = [fulldata['title'],
           "{:8}".format(fulldata['icateg']) + '          ; format of file 1 => superposition; 2 => numerical',
           "{:8}".format(fulldata['ieunit']) + '          ; energy units 1 => K; 2 => eV',
           "{:8}".format(fulldata['nenerg']) + '          ; number of tabulated values',
           "{:8}".format(fulldata['nblock']) + '          ; number of tabulated temperatures']
    
    if fulldata['nform1'] == 1:
        all = all + ['    1             ; cutoff threshold']
    elif fulldata['nform1'] == 2:
        all = all + ["{:5} {:10.2f}".format(fulldata['nform1'], fulldata['param1']) + '   ; cutoff threshold']
    
    if fulldata['nform2'] == 1:
        all = all + ['    1             ; high energy behaviour']
    elif fulldata['nform2'] == 2:
        all = all + ["{:5}{:10.2e}".format(fulldata['nform2'], fulldata['param2']) + '   ; high energy behaviour']
    elif fulldata['nform2'] == 3:
        all = all + ["{:5}{:10.2e}".format(fulldata['nform2'], fulldata['param2']) + '   ; high energy behaviour']
    elif fulldata['nform2'] == 4:
        all = all + ["{:5}{:10.2e}{:10.2e}".format(fulldata['nform2'], fulldata['param2']) + '   ; high energy behaviour']
    
    nblock  = fulldata['nblock']
    ea      = fulldata['ea']
    fa      = fulldata['fa']
    
    if ea.ndim == 1:
        for j in arange(nblock):
            lines = array2strlist(ea, fmt='{:9.2e}', num_col=7)
            all = all + lines
            lines = array2strlist(fa, fmt='{:9.2e}', num_col=7)
            all = all + lines
    else:
        for j in arange(nblock):
            lines = array2strlist(ea[j,:], fmt='{:9.2e}', num_col=7)
            all = all + lines
            lines = array2strlist(fa[j,:], fmt='{:9.2e}', num_col=7)
            all = all + lines

    
    # Write the file
    
    writefile(file=file, content=all, comments=comments)
