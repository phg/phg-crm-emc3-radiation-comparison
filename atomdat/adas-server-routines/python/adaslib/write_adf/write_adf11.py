def write_adf11(file=None, fulldata=None, comments=None, project='Unknown'):
    """
      PURPOSE    :  Write an adf11 source/power coefficient files from a user
                    supplied fulldata dictionary.

                    write_adf11(fulldata=my_acd, file='new_acd.dat', project='Re-scaled')
                    
                    Writing super-stage files is not enabled yet so the partition 
                    information is not required in fulldata.

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of the output adf11 file
                    comments     list        Add this list of string at the end. Make
                                             sure each string begins with 'C'.
                    project      str         Identified for top line
                                             (defaults to 'Unknown').
                    fulldata     dict        data in the adf11 file

                    fulldata = { 'iz0'       : nuclear charge
                                 'class'     : One of 'acd','scd','ccd','prb','prc',
                                                      'qcd','xcd','plt','pls','zcd',
                                                      'ycd','ecd'
                                 'is1min'    : minimum ion charge + 1
                                               (generalised to connection vector index)
                                 'is1max'    : maximum ion charge + 1
                                               (note excludes the bare nucleus)
                                               (generalised to connection vector index
                                                and excludes last one which always remains
                                                the bare nucleus)
                                'icnctv'     : connection vector of number of partitions
                                               of each superstage in resolved case
                                               including the bare nucleus
                                               1st dim: connection vector index
                                'dnr_ams'    : donor element mass (for ccd and prc)
                                'isppr'      : 1st (parent) index for each partition block
                                               1st dim: index of (sstage, parent, base)
                                                        block in isonuclear master file
                                'ispbr'      : 2nd (base) index for each partition block
                                               1st dim: index of (sstage, parent, base)
                                                        block in isonuclear master file
                                'isstgr'     : s1 for each resolved data block
                                               (generalises to connection vector index)
                                               1st dim: index of (sstage, parent, base)
                                                        block in isonuclear master file

                                'idmax'      : number of dens values in
                                               isonuclear master files
                                'itmax'      : number of temp values in
                                               isonuclear master files
                                'ddens'      : log10(electron density(cm-3)) from adf11
                                'dtev'       : log10(electron temperature (eV) from adf11
                                'drcof'      : if(iclass <=9):
                                                 log10(coll.-rad. coefft.) from
                                                 isonuclear master file
                                               if(iclass >=10):
                                                 coll.-rad. coefft. from
                                                 isonuclear master file
                                               1st dim: index of (sstage, parent, base)
                                                         block in isonuclear master file
                                               2nd dim: electron temperature index
                                               3rd dim: electron density index

                                'lptn'       : True  => partition block present
                                               False => partition block not present

                                'lres'       : True  => partial file
                                               False => not partial file


      NOTES      : Not all entries in the adf11 fulldata dictionary are required.
                   Above are listed what is used by this routine depending on 
                   whether it is a standard or metastable-resolved adf1. lptn and
                   lres are queried and suitable defaults are set if not present.
                   dnr_ams is only required for ccd and prc files. 

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    13-11-2019

    """

    def block_line(adf11type, is1, index_1, index_2, today='00/00/00', mass=None):
        """
           routine to return the header line of each block depending of adf11 class
        """

        lines = { 'acd'  : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'scd'  : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'ccd'  : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'plt'  : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'prb'  : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'prc'  : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'ecd'  : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'ycd'  : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'zcd'  : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'acdr' : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'scdr' : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'qcdr' : ('---------------------/ IGRD=', '  / JGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'xcdr' : ('---------------------/ IPTR=', '  / JPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'ccdr' : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'pltr' : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'prbr' : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'prcr' : ('---------------------/ IPRT=', '  / IGRD=', '  /', '/ Z1=', '   / DATE= '),
                  'ecdr' : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'ycdr' : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= '),
                  'zcdr' : ('---------------------/ IGRD=', '  / IPRT=', '  /', '/ Z1=', '   / DATE= ')
                }

        p1,p2,p3,p4,p5 = lines.get(adf11type)

        if mass is None:
            mstr = '--------'
        else:
            mstr = " MH={:4.2f}".format(mass)

        line = p1 + "{:2}".format(index_1) + \
               p2 + "{:2}".format(index_2) + \
               p3 + mstr +\
               p4 + "{:2}".format(is1) + \
               p5 + today

        return line



    # External routines required

    from datetime import datetime
    from numpy import arange
    from adaslib import array2strlist, writefile, xxelem, xxdate


    # Cannot process super-stage file yet
    
    if 'lptn' in fulldata.keys():
        if fulldata['lptn']:
            raise Exception('Writing super-stage files is not enabled yet')
    
    
    # Data required from the fulldata dictionary

    iz0     = fulldata['iz0']
    a11type = (fulldata['class']).lower()
    idmax   = fulldata['idmax']
    itmax   = fulldata['itmax']
    is1min  = fulldata['is1min']
    is1max  = fulldata['is1max']

    isstgr  = fulldata['isstgr']
    isppr   = fulldata['isppr']
    ispbr   = fulldata['ispbr']

    if 'dnr_ams' in fulldata.keys():
        mass = fulldata['dnr_ams']
    else:
        mass = None

    nblock = len(isstgr)


    # check whether partial or unresolved and set mass if ccd or prc

    is_partial = False
    if 'lres' in fulldata.keys():
        is_partial = fulldata['lres']
    if is_partial:
        a11type = a11type + 'r'

    a11str = (fulldata['class']).upper()


    # Put contents of fulldata into a list of strings

    elname  = xxelem(iz0).upper()
    project = project.ljust(21)
    today   = xxdate()

    dashes = '-' * 79
    fmt    = "{:9.5f}"


    # First line

    all = ["   " + "{:2}".format(iz0) +
           "".join([ "{:5}".format(x) for x in [idmax, itmax, is1min, is1max]]) +
           "     /" + elname.ljust(14) +
           "/" + a11str.ljust(4) +
           "/" + project[0:21] +
           "   ADF11"]

    if is_partial:
        all = all + ['-' + dashes] + \
                     array2strlist(fulldata['icnctv'], fmt="{:4}", num_col=16) + \
                     ['-' + dashes] + \
                     array2strlist(fulldata['ddens'], fmt=fmt, num_col=8) + \
                     array2strlist(fulldata['dtev'], fmt=fmt, num_col=8)
    else:
        all = all + ['-' + dashes] + \
                     array2strlist(fulldata['ddens'], fmt=fmt, num_col=8) + \
                     array2strlist(fulldata['dtev'], fmt=fmt, num_col=8)

    if a11type in ['ecd', 'ycd', 'zcd']:
        fmt = "{:9.3e}"

    for is1 in arange(nblock):
        line = block_line(a11type, isstgr[is1], isppr[is1], ispbr[is1], today=today, mass=mass)
        all  = all + [line]

        for it in arange(itmax):
            all = all + array2strlist(fulldata['drcof'][is1,it,:], fmt=fmt, num_col=8)


    # Add a comment termintor (necessary for proper working of xxdata_11)

    all = all + ['C' + dashes]


    # Write the file

    writefile(file=file, content=all, comments=comments)
