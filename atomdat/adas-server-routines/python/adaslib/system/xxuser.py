def xxuser():
    """

      PURPOSE    :  Returns the full name of the user.
      
      iam = xxuser()

      NOTES      : Queries the password file.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
           1.1     16-04-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    import os
    import pwd

    uid = os.getlogin()
    
    user_info=pwd.getpwnam(uid)
    
    name = user_info.pw_gecos
    
    return name
