def xxdate():
    """

      PURPOSE    :  Returns the current date formatted as DD-MM-YY.
      
                    today = xxdate()

      NOTES      :

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
           1.1     19-11-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    from datetime import datetime

    d     = datetime.utcnow()
    today = d.strftime("%d-%m-%y")
    
    return today
