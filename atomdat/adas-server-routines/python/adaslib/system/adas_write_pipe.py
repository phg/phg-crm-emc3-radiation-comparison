def adas_write_pipe(pipe, value):
    """
    PURPOSE    : Write value to a bi-directional pipe setup between
                 python code and fortran.

                 Required since python 2 and 3 have different
                 behaviours.

                 Type of value determines the conversion to a byte stream.

    res = adas_vector(pipe, value)

    INPUTS:
          pipe   -  previously established pipe.
          value  -  quantity to be written.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Report an error, rather than hanging, if the type 
                       of variable cannot be determined.
                     - Import numpy for its types. The type of a single 
                       element of a numpy defined int array is numpy.int64
                       and not int. Testing the type with isinstance requires
                       the type and not just the name.

      VERSION    :
            1.1    19-01-2014
            1.2    05-10-2018

    """

    import sys
    import numpy as np

    if sys.version_info[0] == 2:
        if isinstance(value, str):
            pipe.stdin.write('%s\n' % value)
        elif isinstance(value, int):
            pipe.stdin.write('%d\n' % value)
        elif isinstance(value, float):
            pipe.stdin.write('%g\n' % value)
        else:
            raise Exception('Unknown type encountered')
    else:
        if isinstance(value, str):
            pipe.stdin.write(bytes(value + '\n', 'utf-8'))
        elif isinstance(value, int):
            pipe.stdin.write(bytes('%d\n' % value, 'ascii'))
        elif isinstance(value, np.int64):
            pipe.stdin.write(bytes('%d\n' % value, 'ascii'))
        elif isinstance(value, float):
            pipe.stdin.write(bytes('%g\n' % value, 'ascii'))
        else:
            raise Exception('Unknown type encountered')

    pipe.stdin.flush()

