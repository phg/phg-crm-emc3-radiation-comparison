def xxdata_00(file=None, iz0=None, ls=None, ic=None):
    """
      PURPOSE    :  Reads adf00 fixed atomic files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_00(file='/home/adas/adas/adf00/au.dat')
         or
      fulldata = xxdata_00(iz0=79)

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf00 file
      or            iz0          int         atomic number

      OPTIONAL      ls           bool        True => read

      RETURNS       fulldata    dictionary      contains
                                 'filename'     : adf00 filename
                                 'esym'         : element symbol.
                                 'iz0'          : nuclear charge read
                                 'wf'           : work function for ls coupled data (eV)
                                 'bwnoa'        : ionisation potential (cm-1) of each stage
                                                    int array(ion charge +1)
                                 'eeva'         : ionisation potential (eV) of each stage
                                                    int array(ion charge +1)
                                 'iorba'        : number of orbital shells in configuration
                                                    int array(ion charge +1)
                                 'na'           : principal quantum number of shell
                                                    int array(ion charge +1, shell index)
                                 'la'           : orbital ang. momentum qu. no. of shell
                                                    int array(ion charge +1, shell index)
                                 'iqa'          : occupancy. of shell
                                                    int array(ion charge +1, shell index)
                                 'cstr_std'     : configuration string in standard form
                                                    str array(nuclear charge +1)
                                 'isa'          : multiplicity of ground level
                                                    int array(ion charge +1)
                                 'ila'          : total orbital ang. mom. of  of ground level
                                                    int array(ion charge +1)
                                 'xja'          : total ang. mom. (or [stat. wt.-1/2])
                                                    int array(ion charge +1)
                                 'imeta'        : theor. no. of metastables for each ionis. stage
                                                    int array(ion charge +1)
                                 'imeta_actual' : actual. no. of metastables found for each ionis. stage
                                                    int array(ion charge +1)
                                 'eevma'        : excitation energy (eV) of each metastable
                                                     float array(ion charge +1, metastable index)
                                 'iorbma'       : number of orbital shells in metas. config.
                                                     int array(ion charge +1, metastable index)
                                 'nma'          : principal quantum number of metas.shell
                                                     int array(ion charge +1, shell index, metastable index)
                                 'lma'          : orbital ang. mom. qu. no. of metas. shell
                                                     int array(ion charge +1, shell index, metastable index)
                                 'iqma'         : occupancy. of metas. shell
                                                     int array(ion charge +1, shell index, metastable index)
                                 'cstrm_std'    : meta. config. string in standard form
                                                     str array(ion charge +1, metastable index)
                                 'isma'         : multiplicity of metastable level
                                                     int array(ion charge +1, metastable index)
                                 'ilma'         : total orbital ang. mom. of metastable level
                                                     int array(ion charge +1, metastable index)
                                 'xjma'         : total ang. mom. (or [stat. wt.-1/2]) of metastable level
                                                     int array(ion charge +1, metastable index)
                                 'lexist'       : True   => ionisation potential present
                                                  False  => not present
                                 'lresol'       : True   => metastable resolved adf00 file
                                                  False  => not metastable resolved adf00
                                 'lquno'        : True   => outer quantum nos. present in adf00 file
                                                  False  => outer quantum nos. not present
                                 'coupling'     : ''  : base form,  unresolved,
                                                  'ls':  '_ls' detected in dataset name,
                                                  'ic':  '_ic' detected in dataset name,
                                                  'ns': unspecified resolved

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - dsnin should be padded with spaces for sending
                       to fortran but not when reading the first line
                       to count the number of metastables.

      VERSION    :
            1.1    17-09-2019
            1.2    26-09-2019

    """

    # External routines required

    import os
    import re
    from numpy import asarray, zeros, empty, arange
    from adaslib import xxesym
    from .xxdata00 import xxdata_00_calc

    # Examine inputs

    if iz0 is None:
        if not file:
            raise Exception('A valid adf00 file is required')
        else:
            if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
                raise Exception('the adf00 file selected is invalid')
            dsnin = file

    if not file:
        if iz0 is None:
            raise Exception('supply an atomic number')
        else:
            if iz0 < 1 or iz0 > 86:
                raise Exception('invalid atomic number')
            else:
                extra = '.dat'
                if ls: extra = '_ls.dat'
                if ic: extra = '_ic.dat'
                dsnin = os.getenv('ADASCENT') + '/adf00/' + xxesym(iz0).lower() + extra
    
    # Read first line to check dimensions are appropriate
    
    with open(dsnin, "r") as f:
        line = f.readline()

    i2 = line.find('/')
    if i2 != -1:
        i1  = 22
        res = [int(s) for s in re.findall(r'\b\d+\b', line[i1:i2-i1+1])]
        max_num_meta = max(res)
    else:
        max_num_meta = 1
    
    # Define variables for fortran routine

    izdimd_ = 86
    iodimd_ = 128
    imdimd_ = 13
    
    dsnin        = dsnin.ljust(132)  # pad for passing to fortran

    iz0_r        = -1
    esym         = '--'
    wf           = 0.0
    bwnoa        = asarray(zeros(izdimd_), float)
    eeva         = asarray(zeros(izdimd_), float)
    iorba        = asarray(zeros(izdimd_), int)
    na           = asarray(zeros((izdimd_,iodimd_)), int)
    la           = asarray(zeros((izdimd_,iodimd_)), int)
    iqa          = asarray(zeros((izdimd_,iodimd_)), int)
    cstr_std     = empty(izdimd_, dtype='U100')
    isa          = asarray(zeros(izdimd_), int)
    ila          = asarray(zeros(izdimd_), int)
    xja          = asarray(zeros(izdimd_), float)
    imeta        = asarray(zeros(izdimd_), int)
    imeta_actual = asarray(zeros(izdimd_), int)
    eevma        = asarray(zeros((izdimd_,iodimd_)), float)
    iorbma       = asarray(zeros((izdimd_,imdimd_)), int)
    nma          = asarray(zeros((izdimd_,iodimd_,imdimd_)), int)
    lma          = asarray(zeros((izdimd_,iodimd_,imdimd_)), int)
    iqma         = asarray(zeros((izdimd_,iodimd_,imdimd_)), int)
    cstrm1d      = empty((izdimd_*imdimd_), dtype='U100')
    isma         = asarray(zeros((izdimd_,imdimd_)), int)
    ilma         = asarray(zeros((izdimd_,imdimd_)), int)
    xjma         = asarray(zeros((izdimd_,imdimd_)), float)
    lexist       = False
    lresol       = False
    lquno        = False
    coupling     = '--'

    if max_num_meta > imdimd_:
        raise Exception('Increase number of allowed metastables in ')    
    
    (esym,iz0_r,wf,bwnoa,eeva,
     iorba,na,la,iqa,cstr_std,isa,ila,xja,
     imeta,imeta_actual,
     eevma,iorbma,nma,lma,iqma,
     cstrm1d,isma,ilma,xjma,
     lexist,lresol,lquno,coupling) = xxdata_00_calc(dsnin, izdimd_, iodimd_, imdimd_)

    c1 = asarray([row.tostring().strip().decode() for row in cstr_std[0:iz0]])
    c2 = asarray([row.tostring().strip().decode() for row in cstrm1d[0:izdimd_*imdimd_]])
    
    max_meta = imeta_actual.max()
    max_sh   = iorbma.max()

    fulldata = { 'esym'         :  esym.strip().decode(),
                 'iz0'          :  iz0_r,
                 'wf'           :  wf,
                 'bwnoa'        :  bwnoa[0:iz0_r],
                 'eeva'         :  eeva[0:iz0_r],
                 'iorba'        :  iorba[0:iz0_r],
                 'na'           :  na[0:iz0_r,0:max_sh],
                 'la'           :  la[0:iz0_r,0:max_sh],
                 'iqa'          :  iqa[0:iz0_r,0:max_sh],
                 'cstr_std'     :  c1[0:iz0_r],
                 'isa'          :  isa[0:iz0_r],
                 'ila'          :  ila[0:iz0_r],
                 'xja'          :  xja[0:iz0_r],
                 'imeta'        :  imeta[0:iz0_r],
                 'imeta_actual' :  imeta_actual[0:iz0_r],
                 'eevma'        :  eevma[0:iz0_r,0:max_sh],
                 'iorbma'       :  iorbma[0:iz0_r,0:max_meta],
                 'nma'          :  nma[0:iz0_r,0:max_sh,0:max_meta],
                 'lma'          :  lma[0:iz0_r,0:max_sh,0:max_meta],
                 'iqma'         :  iqma[0:iz0_r,0:max_sh,0:max_meta],
                 'cstrm_std'    :  c2.reshape(izdimd_,-1)[0:iz0_r,0:max_meta],
                 'isma'         :  isma[0:iz0_r,0:max_meta],
                 'ilma'         :  ilma[0:iz0_r,0:max_meta],
                 'xjma'         :  xjma[0:iz0_r,0:max_meta],
                 'lexist'       :  bool(lexist),
                 'lresol'       :  bool(lresol ),
                 'lquno'        :  bool(lquno),
                 'coupling'     :  coupling.strip().decode(),
                 'filename'     :  dsnin.strip() }

    return fulldata
