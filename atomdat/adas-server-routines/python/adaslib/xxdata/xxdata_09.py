def xxdata_09(file=None):
    """
      PURPOSE    :  Reads adf09 dielectronic recombination files (all variants)
                    and return its contents in a dictionary.

      fulldata = xxdata_09(file='/home/adas/adas/adf09/nrb00#ne/nrb00#ne_ar8ic23.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf07 file

      RETURNS       fulldata    dictionary   
                                  
                                  key               description
                                  ---               -----------
                                  iz             :  recombined ion charge
                                  iz0            :  nuclear charge
                                  iz1            :  recombining ion charge
                                  sym            :  recombined ion seq
                                  ctype          :  resolution type (ls/ic/ca)
                                  iform          :  data set organisational form
                                                        1: LS 1993 Badnell data form
                                                        2: LS 2000 Badnell data form
                                                        3: IC 2000 Badnell data form
                                                        4: CA 2000 Badnell data form
                                                        5: hybrid LS 2015 Badnell data form
                                                        6: hybrid IC 2015 Badnell data form
                                                        7: hybrid CA 2015 Badnell data form
                                                        8: unassigned
                                                        9: LS 2016 ADAS standard data form
                                                       10: IC 2016 ADAS standard data form
                                                       11: CA 2016 ADAS standard data form
                                  nprnt          :  total number of parents
                                  nprnti         :  number of parents which are initial parents
                                  nprntf         :  number of parents which are final parents
                                  bwnp           :  binding wave no. of ground parent (cm-1)
                                  ipa            :  parent energy level index number
                                                      dim : parent lvl/trm/cfg index
                                  cstrpa()       :  nomencl./config. for parent level 'ipa()'
                                                      dim: parent lvl/trm/cfg index
                                  ispa()         :  multiplicity for parent level 'ipa()'
                                                    note: (ispa-1)/2 = quantum number (sp)
                                                      dim: parent lvl/trm/cfg index
                                  ilpa()         :  quantum number (lp) for parent level 'ipa()'
                                                      dim: parent lvl/trm/cfg index
                                  xjpa()         :  quantum number (jp) for parent level 'ipa()'
                                                    note: (2*xjpa)+1 = statistical weight
                                                      dim: parent lvl/trm/cfg index
                                  wpa()          :  energy relative to parent level 1 (cm-1)
                                                    for parent level 'ipa()'
                                                      dim: parent lvl/trm/cfg index
                                  nlvl           :  number of energy levels (lvl/trm/cfg) of
                                                    recombined ion - replaced ncfg for hybrid forms
                                  bwnr           :  ionisation potential (cm-1) of lowest level
                                                    of recombined ion
                                  ia()           :  recombined ion energy level index number
                                                     dim: lvl/trm/cfg index
                                  ip()           :  associated parent of recombined level
                                                     dim: lvl/trm/cfg index
                                  cstrga()       :  nomencl./config. for recombined ion level 'ia()'
                                                     dim: lvl/trm/cfg index
                                  isa()          :  multiplicity for recombined level 'ia()'
                                                    note: (isa-1)/2 = quantum number (s)
                                                     dim: lvl/trm/cfg index
                                  ila()          :  quantum number (l) for recombined level 'ia()'
                                                     dim: lvl/trm/cfg index
                                  xja()          :  quantum number (j) for recombined level 'ia()'
                                                    note: (2*xja)+1 = statistical weight
                                                     dim: lvl/trm/cfg index
                                  wa()           :  energy relative to recombined level 1 (cm-1)
                                                    for recombined level 'ia()'
                                                     dim: lvl/trm/cfg index
                                  auga_res(,)    :  specific auger rates for resolved levels
                                                      dim: lvl/trm/cfg index
                                                      dim: parent pair index
                                  lauga_res(,)   :  1 (true)  => auger rate present for lvl/trm/cfg
                                                    0 (false) => auger rate not present
                                                     dim: lvl/trm/cfg index
                                                     dim: parent pair index
                                  nlrep          :  number of representative nl-shells
                                  iaprs_nl       :  number of nl-shell auger rate initial
                                                    and final parent pairs
                                  caprs_nl()     :  nl-shell auger rate parent pair string
                                                     dim: parent pair index
                                  ipaug_nl(,)    :  initial and final parents index
                                                    for nl-shell auger breakups
                                                     dim: parent pair index
                                                     dim: 2 (for initial and final parent)
                                  irepa_nl()     :  representative nl-shell index number
                                                      dim: representative nl-shell index
                                  irepa_nl()     :  representative nl-shell index number
                                                      dim: representative nl-shell index
                                  nlrepa_n()     :  n value for representative nl-shell
                                                      dim: representative nl-shell index
                                  nlrepa_l()     :  l value for representative nl-shell
                                                      dim: representative nl-shell index
                                  auga_nl(,)     :  auger rates (sec-1)
                                                     dim: representative nl-shell index
                                                     dim: parent pair index
                                  lauga_nl(,)    :  1 (true)  => auger rate present for nl-shell
                                                    0 (false) => auger rate not present
                                                      dim: representative n-shell index
                                                      dim: parent pair index
                                  nrep           :  number of representative n-shells
                                  iaprs_n        :  number of n-shell auger rate initial
                                                    and final parent pairs
                                  caprs_n()      :  auger rate parent pair string
                                                     dim: parent pair index
                                  ipaug_n(,)     :  initial and final parents index
                                                    for n-shell auger breakups
                                                     dim: parent pair index
                                                     dim: 2 (for initial and final parent)
                                  irepa_n()      :  representative n-shell index number
                                                      dim: representative n-shell index
                                  nrepa()        :  n value for representative n-shell
                                                      dim: representative n-shell index
                                  auga_n(,)      :  auger rates (sec-1)
                                                     dim: representative n-shell index
                                                     dim: parent pair index
                                  lauga_n(,)     :  1 (true)  => auger rate present for n-shell
                                                    0 (false) => auger rate not present
                                                     dim: representative n-shell index
                                                     dim: parent pair index
                                  iprti()        :  initial parent index from block header
                                                      dim: initial parent index number
                                  diel_res(,,)   :  lvl/trm/cfg resol. diel. coeffts.(cm3 s-1)
                                                     dim: level index
                                                     dim: initial parent index
                                                     dim: temperature index
                                  ldiel_res(,)   :  1 (true)  => diel. present for lvl/trm/cfg index
                                                    0 (false) => diel. not present for
                                                                 lvl/trm/cfg index
                                                     dim: level index
                                                     dim: initial parent index
                                  iprtf(,)       :  final parent index from block header
                                                         dim: initial parent index number
                                                         dim: final parent index number
                                  nsysf(,)       :  no. of spin systems built on final parent
                                                    defaults to 1 for /ic/ca/
                                                      dim: initial parent index number
                                                      dim: final parent index number
                                  isys(,,)       :  n-shell spin system index for final parent
                                                    defaults to 1 for /ic/ca/
                                                      dim: initial parent index number
                                                      dim: final parent index number
                                                      dim: spin system index number
                                  ispsys(,,)     :  n-shell spin system for final parent
                                                    applicable to /ls/ only
                                                      dim: initial parent index number
                                                      dim: final parent index number
                                                      dim: spin system index number
                                  cspsys(,,)     :  characterising string for final parent
                                                    system - universal for /ls/ic/ca/
                                                      dim: initial parent index number
                                                      dim: final parent index number
                                                      dim: spin system index number
                                  diel_nl(,,,,)  :  nl-shell dielec. coeffts.(cm3 s-1)
                                                      dim: repr. nl-shell index
                                                      dim: initial parent index
                                                      dim: final parent index
                                                      dim: spin system index
                                                      dim: temperature index
                                  ldiel_nl(,,,)  :  1 (true)  => diel. present for repr. nl-shell
                                                    0 (false) => diel. not present for  nl-shell
                                                     dim: repr. nl-shell index
                                                     dim: initial parent index
                                                     dim: final parent index
                                                     dim: spin system index
                                  diel_n(,,,,)   :  n-shell dielec. coeffts.(cm3 s-1)
                                                     dim: repr. n-shell index
                                                     dim: initial parent index
                                                     dim: final parent index
                                                     dim: spin system index
                                                     dim: temperature index
                                  ldiel_n(,,,,)  :  1 (true)  => diel. present for repr. n-shell
                                                    0 (false) => diel. not present for  n-shell
                                                     dim: repr. n-shell index
                                                     dim: initial parent index
                                                     dim: final parent index
                                                     dim: spin system index
                                  diel_sum(,,,)  :  sum of n-shell dielec. coeffts.(cm3 s-1)
                                                      dim: initial parent index
                                                      dim: final parent index
                                                      dim: spin system index
                                                      dim: temperature index
                                  ldiel_sum(,,)  :  1 (true)  => diel. sum present
                                                    0 (false) => diel. sum not present
                                                     dim: initial parent index
                                                     dim: final parent index
                                                     dim: spin system index
                                  diel_tot(,)    :  total diel coefficients (cm3 s-1) tabulated
                                                    in dataset
                                                     dim: initial parent index
                                                     dim: electron temperature index
                                  ldiel_tot()    :  1 (true)  => diel. total present in dataset
                                                    0 (false) => diel. total not present
                                                     dim: initial parent index
                                  nte            :  no. of electron temperatures
                                  tea()          :  electron temperatures (k)
                                                     dim: electron temperature index


                    Additional entries for hybrid forms of adf09 files:

                                  ncfg           : number of averaged energy levels (lvl/trm/cfg)
                                                   of recombined ion for hybrid adf09
                                  iscp()         : parent of IP straddling nl-level
                                                   1st.dim: cfg index
                                  iscn()         : n quantum number of IP straddling  nl-level
                                                   1st.dim: cfg index
                                  iscl()         : l quantum number of IP straddling  nl-level
                                                   1st.dim: cfg index
                                  fsc(,)         : fractional statistical weight of the levels
                                                   of a config which lie below and above the IP
                                                   1st.dim: index
                                                   2nd.dim: 1 -> below, 2-> above
                                  wsc(,)         : the statistical average energies of the levels
                                                   of a config which lie below and above the IP
                                                   1st.dim: index
                                                   2nd.dim: 1 -> below, 2-> above
                                  ipca()         : parent config averaged index number          
                                                   1st dim: parent cfg index                    
                                  cstrpca()      : nomencl./config. for parent config 'ipca()'
                                                   1st dim: parent cfg index                    
                                  xjpca()        : quantum number (jp) for parent level 'ipca()'
                                                   note: (2*xjpa)+1 = statistical weight        
                                                   1st dim: parent cfg index                    
                                  wpca()         : energy relative to parent level 1 (cm-1)     
                                                   for parent level 'ipca()'     
                                                   1st dim: parent cfg index     

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    19-10-2018

    """

    # External routines required

    import os
    from numpy import asarray, zeros, array, ndarray, arange
    from .xxdata09 import xxdata_09_calc

    # Examine inputs

    if not file:
        raise Exception('A valid adf09 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf09 file selected is invalid')

    dnsin   = file.ljust(120)
    dnssize = len(dnsin.rstrip())


    # Define variables for fortran routine

    # Dimensions

    ndprt  = 50
    ndrep  = 250
    ndlrep = 500
    ndlev  = 16000
    ndaug  = 80
    ndt    = 20
    ndsc   = 1000

    # variables

    iz           =    0
    iz0          =    0
    iz1          =    0
    iform        =    0
    nprnt        =    0
    nprnti       =    0
    nprntf       =    0
    bwnp         =    0.0
    ipa          =    asarray(zeros(ndprt), int)
    ipca         =    asarray(zeros(ndprt), int)
    ispa         =    asarray(zeros(ndprt), int)
    ilpa         =    asarray(zeros(ndprt), int)
    xjpa         =    asarray(zeros(ndprt), float)
    xjpca        =    asarray(zeros(ndprt), float)
    wpa          =    asarray(zeros(ndprt), float)
    wpca         =    asarray(zeros(ndprt), float)
    nlvl         =    0
    ncfg         =    0
    nippy        =    0
    bwnr         =    0.0
    ia           =    asarray(zeros(ndlev), int)
    ip           =    asarray(zeros(ndlev), int)
    isa          =    asarray(zeros(ndlev), int)
    ila          =    asarray(zeros(ndlev), int)
    xja          =    asarray(zeros(ndlev), float)
    wa           =    asarray(zeros(ndlev), float)
    iscp         =    asarray(zeros(ndsc), int)
    iscn         =    asarray(zeros(ndsc), int)
    iscl         =    asarray(zeros(ndsc), int)
    fsc          =    asarray(zeros((ndsc,2)), float)
    wnsc         =    asarray(zeros((ndsc,2)), float)
    auga_res     =    asarray(zeros((ndlev,ndprt)), float)
    ilauga_res   =    asarray(zeros((ndlev,ndprt)), int)
    nlrep        =    0
    iaprs_nl     =    0
    ipaug_nl     =    asarray(zeros((ndaug,2)), int)
    irepa_nl     =    asarray(zeros(ndlrep), int)
    nlrepa_n     =    asarray(zeros(ndlrep), int)
    nlrepa_l     =    asarray(zeros(ndlrep), int)
    auga_nl      =    asarray(zeros((ndlrep,ndaug)), float)
    ilauga_nl    =    asarray(zeros((ndlrep,ndaug)), int)
    nrep         =    0
    iaprs_n      =    0
    ipaug_n      =    asarray(zeros((ndaug,2)), int)
    irepa_n      =    asarray(zeros(ndrep), int)
    nrepa        =    asarray(zeros(ndrep), int)
    auga_n       =    asarray(zeros((ndrep,ndaug)), float)
    ilauga_n     =    asarray(zeros((ndrep,ndaug)), int)
    iprti        =    asarray(zeros(ndprt), int)
    diel_res     =    asarray(zeros((ndlev,ndprt,ndt)), float)
    ildiel_res   =    asarray(zeros((ndlev,ndprt)), int)
    iprtf        =    asarray(zeros((ndprt,ndprt)), int)
    nsysf        =    asarray(zeros((ndprt,ndprt)), int)
    isys         =    asarray(zeros((ndprt,ndprt,2)), int)
    ispsys       =    asarray(zeros((ndprt,ndprt,2)), int)
    diel_nl      =    asarray(zeros((ndlrep,ndprt,ndprt,2,ndt)), float)
    ildiel_nl    =    asarray(zeros((ndlrep,ndprt,ndprt,2)), int)
    diel_n       =    asarray(zeros((ndrep,ndprt,ndprt,2,ndt)), float)
    ildiel_n     =    asarray(zeros((ndrep,ndprt,ndprt,2)), int)
    diel_sum     =    asarray(zeros((ndprt,ndprt,2,ndt)), float)
    ildiel_sum   =    asarray(zeros((ndprt,ndprt,2)), int)
    diel_tot     =    asarray(zeros((ndprt,ndt)), float)
    ildiel_tot   =    asarray(zeros(ndprt), int)
    nte          =    0
    tea          =    asarray(zeros(ndt), float)

    # integer arrays to transfer the character variables -- xxdata_09_calc.for expects these

    ictype    =    asarray(zeros(2), int)
    iseqsym   =    asarray(zeros(2), int)
    icstrga   =    asarray(zeros((20,ndlev)), int)
    icstrpa   =    asarray(zeros((20,ndprt)), int)
    icstrpca  =    asarray(zeros((20,ndprt)), int)
    icspsys   =    asarray(zeros((30,ndprt,ndprt,2)), int)
    icaprs_n  =    asarray(zeros((10,ndaug)), int)
    icaprs_nl =    asarray(zeros((10,ndaug)), int)


    # call fortran routine

    (iseqsym,iz,iz0,iz1,ictype,iform,nprnt,nprnti,nprntf,
     bwnp,ipa,icstrpa,ispa,ilpa,xjpa,wpa,
     ipca,icstrpca,xjpca,wpca,nlvl,ncfg,nippy,
     bwnr,ia,ip,icstrga,isa,ila,xja,wa,iscp,iscn,
     iscl,fsc,wnsc,auga_res,ilauga_res,nlrep,iaprs_nl,
     icaprs_nl,ipaug_nl,irepa_nl,nlrepa_n,nlrepa_l,auga_nl,
     ilauga_nl,nrep,iaprs_n,icaprs_n,ipaug_n,irepa_n,nrepa,
     auga_n,ilauga_n,iprti,diel_res,ildiel_res,iprtf,nsysf,
     isys,ispsys,icspsys,diel_nl,ildiel_nl,diel_n,ildiel_n,
     diel_sum,ildiel_sum,diel_tot,ildiel_tot,nte,tea) = xxdata_09_calc(ndprt,ndrep,ndlrep,ndlev,ndaug,ndt,ndsc,dnsin,dnssize)


    # convert integer arrays into character arrays

    ctype  = "".join(map(chr, ictype))
    seqsym = "".join(map(chr, iseqsym))

    cstrga = array(['                    '] * ndlev, dtype=object)
    for j in arange(ndlev): cstrga[j] = "".join(map(chr, icstrga[:,j]))

    cstrpa  = array(['                    '] * ndprt, dtype=object)
    cstrpca = array(['                    '] * ndprt, dtype=object)
    for j in arange(ndprt):
        cstrpa[j]  = "".join(map(chr, icstrpa[:,j]))
        cstrpca[j] = "".join(map(chr, icstrpca[:,j]))

    cspsys = ndarray((ndprt,ndprt,2), dtype=object)

    for j in arange(ndprt):
        for i in arange(ndprt):
            cspsys[j,i,0]  = "".join(map(chr, icspsys[:,j,i,0]))
            cspsys[j,i,1]  = "".join(map(chr, icspsys[:,j,i,1]))

    caprs_n  = array(['          '] * ndaug, dtype=object)
    caprs_nl = array(['          '] * ndaug, dtype=object)
    for j in arange(ndaug):
        caprs_n[j]  = "".join(map(chr, icaprs_n[:,j]))
        caprs_nl[j] = "".join(map(chr, icaprs_nl[:,j]))


    # Bring together the data to return in the fulldata dictionary.
    # Throw away unncessary data - note hybrid forms have fewer fields than
    # the others but also hold extra information on the IP straddling
    # configurations.

    if (iform==5 or iform==6 or iform==7):

        ipa          =    ipa[0:nprnti]
        ispa         =    ispa[0:nprnti]
        ilpa         =    ilpa[0:nprnti]
        xjpa         =    xjpa[0:nprnti]
        wpa          =    wpa[0:nprnti]
        ia           =    ia[0:ncfg]
        ip           =    ip[0:ncfg]
        isa          =    isa[0:ncfg]
        ila          =    ila[0:ncfg]
        xja          =    xja[0:ncfg]
        wa           =    wa[0:ncfg]
        auga_res     =    auga_res[0:ncfg,0:nprnt]
        ilauga_res   =    ilauga_res[0:ncfg,0:nprnt]

        if nlrep > 0:
            irepa_nl     =    irepa_nl[0:nlrep]
            nlrepa_n     =    nlrepa_n[0:nlrep]
            nlrepa_l     =    nlrepa_l[0:nlrep]
            diel_nl      =    diel_nl[0:nlrep,0:nprnti,0:nprntf,:,0:nte]
            ildiel_nl    =    ildiel_nl[0:nlrep,0:nprnti,0:nprntf,:]
        else:
            irepa_nl     =    0
            nlrepa_n     =    0
            nlrepa_l     =    0
            diel_nl      =    0.0
            ildiel_nl    =    0

        if iaprs_nl >= 1:
            ipaug_nl     =  ipaug_nl[0:iaprs_nl,:]
            auga_nl      =  auga_nl[0:nlrep,0:iaprs_nl]
            ilauga_nl    =  ilauga_nl[0:nlrep,0:iaprs_nl]
            caprs_nl     =  caprs_nl[0:iaprs_n]
        else:
            ipaug_nl     =  0
            auga_nl      =  0.0
            ilauga_nl    =  0
            caprs_nl     =  " "

        if iaprs_n >= 1:
            ipaug_n      =  ipaug_n[0:iaprs_n,:]
            auga_n       =  auga_n[0:nrep,0:iaprs_n]
            ilauga_n     =  ilauga_n[0:nrep,0:iaprs_n]
            caprs_n      =  caprs_n[0:iaprs_n]
        else:
            ipaug_n      =  0
            auga_n       =  0.0
            ilauga_n     =  0
            caprs_n      =  " "

        irepa_n    = irepa_n[0:nrep]
        nrepa      = nrepa[0:nrep]
        iprti      = iprti[0:nprnt]
        diel_res   = diel_res[0:ncfg,0:nprnti,0:nte]
        ildiel_res = ildiel_res[0:ncfg,0:nprnt]
        iprtf      = iprtf[0:nprnti,0:nprntf]
        nsysf      = nsysf[0:nprnti,0:nprntf]
        isys       = isys[0:nprnti,0:nprntf,:]
        ispsys     = ispsys[0:nprnti,0:nprntf,:]
        diel_n     = diel_n[0:nrep,0:nprnti,0:nprntf,:,0:nte]
        ildiel_n   = ildiel_n[0:nrep,0:nprnti,0:nprntf,:]
        diel_sum   = diel_sum[0:nprnti,0:nprntf,:,0:nte]
        ildiel_sum = ildiel_sum[0:nprnti,0:nprntf,:]
        diel_tot   = diel_tot[0:nprnti,0:nte]
        ildiel_tot = ildiel_tot[0:nprnti]
        tea        = tea[0:nte]
        cstrpa     = cstrpa[0:nprnt]
        cstrga     = cstrga[0:ncfg]
        cspsys     = cspsys[0:nprnti,0:nprntf,:]

        # specific hybrid data

        if nippy > 0:

            ipca    = ipca[0:nprntf]
            cstrpca = cstrpca[0:nprntf]
            xjpca   = xjpca[0:nprntf]
            wpca    = wpca[0:nprntf]
            iscp    = iscp[0:nippy]
            iscn    = iscn[0:nippy]
            iscl    = iscl[0:nippy]
            fsc     = fsc[0:nippy,:]
            wnsc    = wnsc[0:nippy, :]

        else:

            ipca    = -1
            cstrpca = ""
            xjpca   = 0.0
            wpca    = 0.0
            iscp    = -1
            iscn    = -1
            iscl    = -1
            fsc     = 0.0
            wnsc    = 0.0

        # Assemble fulldata structure

        fulldata  = { 'iz'          :   iz,
                      'iz0'         :   iz0,
                      'iz1'         :   iz1,
                      'sym'         :   seqsym,
                      'ctype'       :   ctype,
                      'iform'       :   iform,
                      'nprnt'       :   nprnt,
                      'nprnti'      :   nprnti,
                      'nprntf'      :   nprntf,
                      'bwnp'        :   bwnp,
                      'ipa'         :   ipa,
                      'cstrpa'      :   cstrpa,
                      'ispa'        :   ispa,
                      'ilpa'        :   ilpa,
                      'xjpa'        :   xjpa,
                      'wpa'         :   wpa,
                      'ipca'        :   ipca,
                      'cstrpca'     :   cstrpca,
                      'xjpca'       :   xjpca,
                      'wpca'        :   wpca,
                      'ncfg'        :   ncfg,
                      'bwnr'        :   bwnr,
                      'ia'          :   ia,
                      'ip'          :   ip,
                      'cstrga'      :   cstrga,
                      'isa'         :   isa,
                      'ila'         :   ila,
                      'xja'         :   xja,
                      'wa'          :   wa,
                      'nippy'       :   nippy,
                      'iscp'        :   iscp,
                      'iscn'        :   iscn,
                      'iscl'        :   iscl,
                      'fsc'         :   fsc,
                      'wnsc'        :   wnsc,
                      'auga_res'    :   auga_res,
                      'lauga_res'   :   ilauga_res,
                      'nlrep'       :   nlrep,
                      'iaprs_nl'    :   iaprs_nl,
                      'caprs_nl'    :   caprs_nl,
                      'ipaug_nl'    :   ipaug_nl,
                      'irepa_nl'    :   irepa_nl,
                      'nlrepa_n'    :   nlrepa_n,
                      'nlrepa_l'    :   nlrepa_l,
                      'auga_nl'     :   auga_nl,
                      'lauga_nl'    :   ilauga_nl,
                      'nrep'        :   nrep,
                      'iaprs_n'     :   iaprs_n,
                      'caprs_n'     :   caprs_n,
                      'ipaug_n'     :   ipaug_n,
                      'irepa_n'     :   irepa_n,
                      'nrepa'       :   nrepa,
                      'auga_n'      :   auga_n,
                      'lauga_n'     :   ilauga_n,
                      'iprti'       :   iprti,
                      'diel_res'    :   diel_res,
                      'ldiel_res'   :   ildiel_res,
                      'iprtf'       :   iprtf,
                      'nsysf'       :   nsysf,
                      'isys'        :   isys,
                      'ispsys'      :   ispsys,
                      'cspsys'      :   cspsys,
                      'diel_nl'     :   diel_nl,
                      'ldiel_nl'    :   ildiel_nl,
                      'diel_n'      :   diel_n,
                      'ldiel_n'     :   ildiel_n,
                      'diel_sum'    :   diel_sum,
                      'ldiel_sum'   :   ildiel_sum,
                      'diel_tot'    :   diel_tot,
                      'ldiel_tot'   :   ildiel_tot,
                      'nte'         :   nte,
                      'tea'         :   tea              }

    else:

        ipa          =    ipa[0:nprnt]
        ispa         =    ispa[0:nprnt]
        ilpa         =    ilpa[0:nprnt]
        xjpa         =    xjpa[0:nprnt]
        wpa          =    wpa[0:nprnt]
        ia           =    ia[0:nlvl]
        ip           =    ip[0:nlvl]
        isa          =    isa[0:nlvl]
        ila          =    ila[0:nlvl]
        xja          =    xja[0:nlvl]
        wa           =    wa[0:nlvl]
        auga_res     =    auga_res[0:nlvl,0:nprnt]
        ilauga_res   =    ilauga_res[0:nlvl,0:nprnt]

        if nlrep > 0:
            irepa_nl     =    irepa_nl[0:nlrep]
            nlrepa_n     =    nlrepa_n[0:nlrep]
            nlrepa_l     =    nlrepa_l[0:nlrep]
            diel_nl      =    diel_nl[0:nlrep,0:nprnti,0:nprntf,:,0:nte]
            ildiel_nl    =    ildiel_nl[0:nlrep,0:nprnti,0:nprntf,:]
        else:
            irepa_nl     =    0
            nlrepa_n     =    0
            nlrepa_l     =    0
            diel_nl      =    0.0
            ildiel_nl    =    0

        if iaprs_nl >= 1:
            ipaug_nl     =  ipaug_nl[0:iaprs_nl,:]
            auga_nl      =  auga_nl[0:nlrep,0:iaprs_nl]
            ilauga_nl    =  ilauga_nl[0:nlrep,0:iaprs_nl]
            caprs_nl     =  caprs_nl[0:iaprs_n]
        else:
            ipaug_nl     =  0
            auga_nl      =  0.0
            ilauga_nl    =  0
            caprs_nl     =  " "

        if iaprs_n >= 1:
            ipaug_n      =  ipaug_n[0:iaprs_n,:]
            auga_n       =  auga_n[0:nrep,0:iaprs_n]
            ilauga_n     =  ilauga_n[0:nrep,0:iaprs_n]
            caprs_n      =  caprs_n[0:iaprs_n]
        else:
            ipaug_n      =  0
            auga_n       =  0.0
            ilauga_n     =  0
            caprs_n      =  " "

        irepa_n    = irepa_n[0:nrep]
        nrepa      = nrepa[0:nrep]
        iprti      = iprti[0:nprnt]
        diel_res   = diel_res[0:nlvl,0:nprnt,0:nte]
        ildiel_res = ildiel_res[0:nlvl,0:nprnt]
        iprtf      = iprtf[0:nprnti,0:nprntf]
        nsysf      = nsysf[0:nprnti,0:nprntf]
        isys       = isys[0:nprnti,0:nprntf,:]
        ispsys     = ispsys[0:nprnti,0:nprntf,:]
        diel_n     = diel_n[0:nrep,0:nprnti,0:nprntf,:,0:nte]
        ildiel_n   = ildiel_n[0:nrep,0:nprnti,0:nprntf,:]
        diel_sum   = diel_sum[0:nprnti,0:nprntf,:,0:nte]
        ildiel_sum = ildiel_sum[0:nprnti,0:nprntf,:]
        diel_tot   = diel_tot[0:nprnti,0:nte]
        ildiel_tot = ildiel_tot[0:nprnti]
        tea        = tea[0:nte]
        cstrpa     = cstrpa[0:nprnt]
        cstrga     = cstrga[0:nlvl]
        cspsys     = cspsys[0:nprnti,0:nprntf,:]

        # Assemble fulldata structure

        fulldata  = { 'iz'          :   iz,
                      'iz0'         :   iz0,
                      'iz1'         :   iz1,
                      'sym'         :   seqsym,
                      'ctype'       :   ctype,
                      'iform'       :   iform,
                      'nprnt'       :   nprnt,
                      'nprnti'      :   nprnti,
                      'nprntf'      :   nprntf,
                      'bwnp'        :   bwnp,
                      'ipa'         :   ipa,
                      'cstrpa'      :   cstrpa,
                      'ispa'        :   ispa,
                      'ilpa'        :   ilpa,
                      'xjpa'        :   xjpa,
                      'wpa'         :   wpa,
                      'nlvl'        :   nlvl,
                      'bwnr'        :   bwnr,
                      'ia'          :   ia,
                      'ip'          :   ip,
                      'cstrga'      :   cstrga,
                      'isa'         :   isa,
                      'ila'         :   ila,
                      'xja'         :   xja,
                      'wa'          :   wa,
                      'auga_res'    :   auga_res,
                      'lauga_res'   :   ilauga_res,
                      'nlrep'       :   nlrep,
                      'iaprs_nl'    :   iaprs_nl,
                      'caprs_nl'    :   caprs_nl,
                      'ipaug_nl'    :   ipaug_nl,
                      'irepa_nl'    :   irepa_nl,
                      'nlrepa_n'    :   nlrepa_n,
                      'nlrepa_l'    :   nlrepa_l,
                      'auga_nl'     :   auga_nl,
                      'lauga_nl'    :   ilauga_nl,
                      'nrep'        :   nrep,
                      'iaprs_n'     :   iaprs_n,
                      'caprs_n'     :   caprs_n,
                      'ipaug_n'     :   ipaug_n,
                      'irepa_n'     :   irepa_n,
                      'nrepa'       :   nrepa,
                      'auga_n'      :   auga_n,
                      'lauga_n'     :   ilauga_n,
                      'iprti'       :   iprti,
                      'diel_res'    :   diel_res,
                      'ldiel_res'   :   ildiel_res,
                      'iprtf'       :   iprtf,
                      'nsysf'       :   nsysf,
                      'isys'        :   isys,
                      'ispsys'      :   ispsys,
                      'cspsys'      :   cspsys,
                      'diel_nl'     :   diel_nl,
                      'ldiel_nl'    :   ildiel_nl,
                      'diel_n'      :   diel_n,
                      'ldiel_n'     :   ildiel_n,
                      'diel_sum'    :   diel_sum,
                      'ldiel_sum'   :   ildiel_sum,
                      'diel_tot'    :   diel_tot,
                      'ldiel_tot'   :   ildiel_tot,
                      'nte'         :   nte,
                      'tea'         :   tea              }


    return fulldata
