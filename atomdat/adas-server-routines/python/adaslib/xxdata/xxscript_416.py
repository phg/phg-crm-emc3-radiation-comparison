def xxscript_416(file=None):
    """
      PURPOSE    :  Reads and ADAS416 script file and returns parent emissivity
                    files, partition specification data lines and child output
                    emissivity file names in a dictionary.

      fulldata = xxscript_416(file='/home/adas/adas/scripts416/partition_02_argon_89.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf01 file

      RETURNS       fulldata     dictionary       contains
                                 'filename'    :  adas416 script file name
                                 'dsngcr_p'    :  parent gcr template file name from script
                                 'dsna_p'      :  parent emissivity source file template
                                                     1st dim: emissivity type
                                 'lfilea_p'    :  True => parent emissivity type exists
                                                  False => file does not exist
                                                     1st dim: emissivity type
                                 'ipl_p'       :  position of parent partition in iptnla
                                 'dsngcr_c'    :  child gcr template file name from script
                                 'dsna_c'      :  child emissivity source file template
                                                     1st dim: emissivity type
                                 'lfilea_c'    :  True => child emissivity type required
                                                  False => file not required
                                                     1st dim: emissivity type
                                 'ipl_c'       :  position of child partition in iptnla
                                 'nptnl'       :  number of partition levels in block
                                 'nptn'        :  number of partitions in partition level
                                                     1st dim: number of partitions
                                 'nptnc'       :  number of components in partition
                                                     1st dim: number of partitions
                                                     2nd dim: number of partitions in level
                                 'iptnla'      :  partition level index
                                                     1st dim: number of partitions
                                 'iptna'       :  partition index
                                                     1st dim: number of partitions
                                                     2nd dim: number of partitions in level
                                 'iptnca'      :  component index
                                                     1st dim: number of partitions
                                                     2nd dim: number of partitions in level
                                                     3rd dim: number of components in a partition
                                 'ncptn_stack' :  number of text lines in partition stack
                                 'cptn_stack'  :  stack of partition text lines
                                 'lres'        :  True => script file resolved
                                                  False => script file unresolved

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    06-04-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros, full
    from numpy import byte as np_byte
    from .xxscript416 import xxscript_416_calc

    # Examine inputs

    if not file:
        raise Exception('A valid adas416 script file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adas416 script file selected is invalid')

    dsnin = file.ljust(132)

    # Define variables for fortran routine

    ifdimd  = 4
    ndptn   = 128
    ndptnl  = 4
    ndptnc  = 256
    ndstack = 40

    ipl_c       = -1
    ipl_p       = -1
    iptna       = asarray(zeros((ndptnl,ndptn)), int)
    iptnca      = asarray(zeros((ndptnl,ndptn,ndptnc)), int)
    iptnla      = asarray(zeros(ndptnl), int)
    ircode      = -1
    ncptn_stack = -1
    nptn        = asarray(zeros(ndptnl), int)
    nptnc       = asarray(zeros((ndptnl,ndptn)), int)
    nptnl       = -1
    il_lfilea_c = asarray(zeros(ifdimd), int)
    il_lfilea_p = asarray(zeros(ifdimd), int)
    lres        = 0

    by_file_in = bytearray(dsnin.encode())

    (by_dsngcr_p, by_dsna_p, il_lfilea_p, ipl_p,
     by_dsngcr_c, by_dsna_c, il_lfilea_c, ipl_c,
     nptnl, nptn, nptnc,
     iptnla, iptna, iptnca,
     ncptn_stack, by_cptn_stack,
     il_lres, by_elem, by_year) = xxscript_416_calc(by_file_in, ifdimd, ndstack, ndptnl, ndptn, ndptnc)


    # Convert outputs back into booleans and strings

    if il_lres == 1:
        lres = True
    else:
        lres = False

    lfilea_p = full(ifdimd, False)
    lfilea_c = full(ifdimd, False)

    for j in range(ifdimd):
        if il_lfilea_p[j] == 1: lfilea_p[j] = True
        if il_lfilea_c[j] == 1: lfilea_c[j] = True

    dsngcr_p = by_dsngcr_p.astype(np_byte).tostring().decode().strip()
    dsngcr_c = by_dsngcr_c.astype(np_byte).tostring().decode().strip()

    c1 = asarray([row.astype(np_byte).tostring().decode().strip() for row in by_cptn_stack[0:ndstack,:]])
    c2 = asarray([row.astype(np_byte).tostring().decode().strip() for row in by_dsna_p[0:ifdimd,:]])
    c3 = asarray([row.astype(np_byte).tostring().decode().strip() for row in by_dsna_c[0:ifdimd,:]])

    fulldata = { 'filename'    :   file.strip(),
                 'dsngcr_p'    :   dsngcr_p,
                 'dsna_p'      :   c2,
                 'lfilea_p'    :   lfilea_p,
                 'ipl_p'       :   ipl_p ,
                 'dsngcr_c'    :   dsngcr_c,
                 'dsna_c'      :   c3,
                 'lfilea_c'    :   lfilea_c,
                 'ipl_c'       :   ipl_c,
                 'nptnl'       :   nptnl,
                 'nptn'        :   nptn,
                 'nptnc'       :   nptnc,
                 'iptnla'      :   iptnla,
                 'iptna'       :   iptna,
                 'iptnca'      :   iptnca,
                 'ncptn_stack' :   ncptn_stack,
                 'cptn_stack'  :   c1,
                 'lres'        :   lres }

    return fulldata
