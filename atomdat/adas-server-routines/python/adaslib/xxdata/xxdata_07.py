def xxdata_07(file=None):
    """
      PURPOSE    :  Reads adf07 ionization rate files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_07(file='/home/adas/adas/adf07/szd02#li/szd02#li_li2.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf07 file

      RETURNS       fulldata    dictionary   contains
                                 'filename' :  adf07 filename
                                 'nblock'   :  number of ionisation rates in file
                                 'esym'     :  element Symbol
                                 'iz0'      :  nuclear charge
                                 'iz'       :  number of ionisation rates
                                 'iz1'      :  initial charge
                                                 int array(nblock)
                                 'cicode'   :  initial state metastable index
                                                 string array(nblock)
                                 'cfcode'   :  final state metastable index
                                                 string array(nblock)
                                 'ciion'    :  initial ion (as <esym>+(iz[nblock]> )
                                                  string array(nblock)
                                 'cfion'    :  final ion (as <esym>+(iz[nblock]> )
                                                  string array(nblock)
                                 'bwno'     :  effective ionization potential (cm-1)
                                                  float array(nblock)
                                 'nte'      :  number of temperatures in each block
                                                  float array(nblock)
                                 'te'       :  temperatures (eV)
                                                  float array(max(nte), nblock)
                                 'szd'      :  ionization rates (cm3 s-1)
                                                  float array(max(nte), nblock)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    10-08-2018

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata07 import xxdata_07_calc

    # Examine inputs

    if not file:
        raise Exception('A valid adf07 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf07 file selected is invalid')

    dsnin = file.ljust(132)

    # Define variables for fortran routine

    nstore_  = 500
    ntdim_   = 24

    iz0    = -1
    esym   = '--'
    nbsel  = 0
    isela  = asarray(zeros(nstore_), int)
    iz     = asarray(zeros(nstore_), int)
    iz1    = asarray(zeros(nstore_), int)
    cicode = asarray(['--'] * nstore_)
    cfcode = asarray(['--'] * nstore_)
    ciion  = asarray(['-----'] * nstore_)
    cfion  = asarray(['-----'] * nstore_)
    bwno   = asarray(zeros(nstore_), float)
    ita    = asarray(zeros(nstore_), int)
    teta   = asarray(zeros((ntdim_,nstore_)), float)
    szd    = asarray(zeros((ntdim_,nstore_)), float)

    (esym,iz0,nbsel,isela,iz,iz1,
     cicode,cfcode,ciion,cfion,bwno,ita,teta,szd) = xxdata_07_calc(dsnin, nstore_, ntdim_)

    c1 = asarray([row.tostring().strip().decode() for row in cicode[0:nbsel]])
    c2 = asarray([row.tostring().strip().decode() for row in cfcode[0:nbsel]])
    c3 = asarray([row.tostring().strip().decode() for row in ciion[0:nbsel]])
    c4 = asarray([row.tostring().strip().decode() for row in cfion[0:nbsel]])

    fulldata = { 'esym'     :  esym.strip().decode(),
                 'iz0'      :  iz0,
                 'iz'       :  iz[0:nbsel],
                 'iz1'      :  iz1[0:nbsel],
                 'nblock'   :  nbsel,
                 'cicode'   :  c1,
                 'cfcode'   :  c2,
                 'ciion'    :  c3,
                 'cfion'    :  c4,
                 'bwno'     :  bwno[0:nbsel],
                 'nte'      :  ita[0:nbsel],
                 'te'       :  teta[0:ita.max(), 0:nbsel],
                 'szd'      :  szd[0:ita.max(), 0:nbsel],
                 'filename' :  file.strip() }

    return fulldata
