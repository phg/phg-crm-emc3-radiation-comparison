def xxdata_10_p208(file=None):
    """

      PURPOSE    :  Reads the data in a partial adf10 file adf10 files and return 
                    all its contents in a dictionary. It is ddesigned to be
                    used with the gcr files output by adas208, normally
                    named acd208.pass, plt208.pass etc. 

                    fulldata = xxdata_10_p208(file='scd208.pass')

      REQUIRED   :  file         str         full name of partial adf10 file

      RETURNS       fulldata     dict        data held in file

                    fulldata = { 'iz0'       :  nuclear charge
                                 'iz1'       :  ion charge
                                 'class'     :  type of adf10 file
                                 'nmet'      :  number of metastables
                                 'name_ind1' :  names of first index
                                 'index_1'   :  values of first index
                                 'name_ind2' :      "
                                 'index_2'   :      " for second index
                                 'te'        :  temperature (eV)
                                 'dens'      :  density (cm**-3)
                                 'data'      :  coefficient
                                                 1st dim: metastable
                                                 2nd    : temperature
                                                 3rd    : density
                                 'comments'  :  comments in adf10 file
                                 'file'      :  name of adf10 file
                    
                    The name of the index depends on the type of adf10 file:
                                 
                    class    (indices 1 and 2)  units
                    ----       ----     ----    -----
                    acd        iprt     igrd    cm3/s
                    scd        iprt     igrd    cm3/s
                    ccd        iprt     igrd    cm3/s
                    qcd        igrd     jgrd    cm3/s
                    xcd        iprt     jprt    cm3/s
                    prb        iprt             W cm3
                    prc        iprt             W cm3
                    plt        igrd             W cm3
                    met        igrd               -

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
                     
      VERSION    :
            1.1    19-11-2019

    """

    def _type_a(lines, itval, idval):
        """ extract data from list of lines"""

        len_blk = numlines(idval, 8) * itval

        i1, c1 = list_index_str(lines, 'Z1=')

        index_1 = asarray(zeros(c1), int)
        index_2 = asarray(zeros(c1), int)
        data    = asarray(zeros((c1, itval, idval)), float)

        for j in arange(c1):
            ind = i1[j]

            index_1[j]  = int(lines[ind][38:40])
            index_2[j]  = int(lines[ind][49:51])
            data[j,:,:] = strlist2array(lines[ind+1:ind+len_blk+1]).reshape(itval, idval)


        nmet = asarray([index_1, index_2]).max()

        return index_1, index_2, nmet, data


    def _type_b(lines, itval, idval):
        """ extract data from list of lines"""

        len_blk = numlines(idval, 8) * itval

        i0, c0 = list_index_str(lines, 'NMET         =')
        nmet = int(lines[i0[0]][14:16])

        i1, c1 = list_index_str(lines, '/', pos=0)

        index_1 = asarray(zeros(nmet), int)
        index_2 = -1
        data    = asarray(zeros((nmet, itval, idval)), float)

        for j in arange(nmet):
            ind = i1[j]
            index_1[j]  = j+1
            data[j,:,:] = strlist2array(lines[ind+1:ind+len_blk+1]).reshape(itval, idval)

        return index_1, index_2, nmet, data


    # External routines required

    import os
    from numpy import asarray, zeros, arange
    from adas import numlines, readfile, list_index_str, strlist2array, xxdata_04


    # Examine inputs

    if not file:
        raise Exception('A valid partial adf10 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('A valid partial adf10 file is required')


    # Read in file and parse resulting array of strings
    
    lines, nlines = readfile(file=file)

    if nlines <= 3:
        print('No data in file', file)
        fulldata = { 'class' : 'EMPTY'}
        return fulldata


    # Determine class from the second line of header comments

    possible = { 'C  IONISATION DATA' :  'scd',
                 'C  RECOMBINATION D' :  'acd',
                 'C  CX RECOMBINATIO' :  'ccd',
                 'C  METASTABLE CROS' :  'qcd',
                 'C  CX RECOMBINATIO' :  'ccd',
                 'C  TOTAL LINE POWE' :  'plt',
                 'C  PARENT CROSS CO' :  'xcd',
                 'C  METASTABLE FRAC' :  'met',
                 'C  RECOM/CASACDE/B' :  'prb'}

    a10type = possible.get(lines[1][0:18])


    # From comment section extract the nuclear charge and the adf04 filename

    i1, c1 = list_index_str(lines, "NUCL")
    iz0 = int(lines[i1[0]][19:])

    i1, c1 = list_index_str(lines, "SPECIFIC ION FILE")
    adf04 = lines[i1[0]][24:]

    # Extract block data for te and dens. i<X> is the index
    # in the lines list of the quantity of interest.

    i0, c0 = list_index_str(lines, "--------")
    i1 = i0[0]
    i3 = i0[2]

    _, itval, idval, nion  = strlist2array(lines[i1+1]).astype(int)
    iz1 = int(float(lines[i1+2]))

    i4 = i3 + 1
    i5 = i4 + numlines(idval, 8) - 1
    dens_10 = strlist2array(lines[i4:i5+1])
    i6 = i5 + 1
    i7 = i6 + numlines(itval, 8) - 1
    te_10 = strlist2array(lines[i6:i7+1])

    i8 = i0[c0-2]
    i9 = i0[c0-1]

    comments = lines[i8+1:i9]

    # Convert temperature and density to real units

    te_10   =  (te_10 * iz1**2) / 1.16045221E+04
    dens_10 =  dens_10 * iz1**7


    # Data is arranged differently according to adf10 type

    if a10type == 'scd':
        name_ind1 = 'iprt'
        name_ind2 = 'igrd'
        index_1, index_2, nmet, data = _type_a(lines, itval, idval)
        
        all_04 = xxdata_04(file=adf04)
        eia = asarray(zeros((index_1.max(), index_2.max()), float))
        
        for j in arange(len(index_1)):
            in1 = index_1[j]-1
            in2 = index_2[j]-1
            eia[in1, in2] = all_04['bwnoa'][in1] - all_04['wa'][in2]

    if a10type == 'acd' or a10type == 'ccd':
        name_ind1 = 'iprt'
        name_ind2 = 'igrd'
        index_1, index_2, nmet, data = _type_a(lines, itval, idval)

    if a10type == 'qcd':
        name_ind1 = 'igrd'
        name_ind2 = 'jgrd'
        index_1, index_2, nmet, data = _type_a(lines, itval, idval)

    if a10type == 'xcd':
        name_ind1 = 'iprt'
        name_ind2 = 'jprt'
        index_1, index_2, nmet, data = _type_a(lines, itval, idval)

    if a10type == 'prb' or a10type == 'prc':
        name_ind1 = 'iprt'
        name_ind2 = ''
        index_1, index_2, nmet, data = _type_b(lines, itval, idval)
        data = data * 1.0e-7

    if a10type == 'plt':
        name_ind1 = 'imet'
        name_ind2 = ''
        index_1, index_2, nmet, data = _type_b(lines, itval, idval)
        data = data * 1.0e-7

    if a10type == 'met':
        name_ind1 = 'imet'
        name_ind2 = ''
        index_1, index_2, nmet, data = _type_b(lines, itval, idval)



    # return all data in a dictionary

    fulldata = { 'iz0'       : iz0,
                 'iz1'       : iz1,
                 'class'     : a10type,
                 'nmet'      : nmet,
                 'name_ind1' : name_ind1,
                 'index_1'   : index_1,
                 'name_ind2' : name_ind2,
                 'index_2'   : index_2,
                 'te'        : te_10,
                 'dens'      : dens_10,
                 'data'      : data,
                 'comments'  : comments,
                 'file'      : file  }
    
    if a10type == 'scd':
        fulldata['eia'] = eia
    
    return fulldata
