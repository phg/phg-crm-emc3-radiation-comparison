def xxdata_02(file=None):
    """
      PURPOSE    :  Read adf02, ion driven cross section, files
                    and return all its contents in a dictionary.

      fulldata = xxdata_02(file='/home/adas/adas/adf02/sia#h/sia#h_j99#h.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf02 file

      RETURNS       fulldata    dictionary   contains
                                 'filename' :  adf02 filename
                                 'nblock'   :  number of blocks present
                                 'nedim'    :  max number of collision energies allowed
                                 'cprimy'   :  primary species identification
                                                 string array(nblock)
                                 'csecdy'   :  secondary species identification
                                                 string array(nblock)
                                 'ctype'    :  cross-section type
                                                 string array(nblock)
                                 'ampa'     :  primary species atomic mass number
                                                  float array(nblock)
                                 'amsa'     :  secondary species atomic mass number
                                                  float array(nblock)
                                 'alpha'    :  high energy extrapolation parmameter
                                                  float array(nblock)
                                 'ethra'    :  energy threshold (eV)
                                                  float array(nblock)
                                 'iea'      :  number of collision energies in each block
                                                  float array(nblock)
                                 'teea'     :  collision energies (units: eV/amu)
                                                  float array(max(iea), nblock)
                                 'sia'      :  collision cross-section values (cm**2)
                                                  float array(max(iea), nblock)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    10-07-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata02 import xxdata_02_calc

    # Examine inputs

    if not file:
        raise Exception('A valid adf02 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf02 file selected is invalid')

    dsnin = file.ljust(132)

    # Define variables for fortran routine

    nstore_  = 200
    nedim_   = 24

    nbsel  = 0
    isela  = asarray(zeros(nstore_), int)
    cprimy = asarray(['-----'] * nstore_)
    csecdy = asarray(['-----'] * nstore_)
    ctype  = asarray(['---'] * nstore_)
    ampa   = asarray(zeros(nstore_), float)
    amsa   = asarray(zeros(nstore_), float)
    alpha  = asarray(zeros(nstore_), float)
    ethra  = asarray(zeros(nstore_), float)
    iea    = asarray(zeros(nstore_), int)
    teea   = asarray(zeros((nedim_,nstore_)), float)
    sia    = asarray(zeros((nedim_,nstore_)), float)

    (nbsel,isela,
     cprimy,csecdy,ctype,
     ampa,amsa,alpha,ethra,
     iea,teea,sia) = xxdata_02_calc(dsnin, nstore_, nedim_)

    c1 = asarray([row.tostring().strip().decode() for row in cprimy[0:nbsel]])
    c2 = asarray([row.tostring().strip().decode() for row in csecdy[0:nbsel]])
    c3 = asarray([row.tostring().strip().decode() for row in ctype[0:nbsel]])

    fulldata = { 'nblock'   :  nbsel,
                 'cprimy'   :  c1,
                 'csecdy'   :  c2,
                 'ctype'    :  c3,
                 'ampa'     :  ampa[0:nbsel],
                 'amsa'     :  amsa[0:nbsel],
                 'alpha'    :  alpha[0:nbsel],
                 'ethra'    :  ethra[0:nbsel],
                 'iea'      :  iea[0:nbsel],
                 'teea'     :  teea[0:iea.max(), 0:nbsel],
                 'sia'      :  sia[0:iea.max(), 0:nbsel],
                 'filename' :  file.strip() }

    return fulldata
