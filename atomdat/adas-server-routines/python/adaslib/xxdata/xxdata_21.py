def xxdata_21(file=None):
    """
      PURPOSE    :  Reads adf21 (BMS) files from the python command line
                    and return all its contents in a dictionary.
                    Note that adf22 (BME and BMP) have the same format.

      fulldata = xxdata_21(file='/home/adas/adas/adf21/bms97#h/bms97#h_h1.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf21 file

      RETURNS       fulldata     dict        BMS/BME/BMP data (cm3 s-1 / ph cm3 s-1 / na)

                    fulldata = { 'file'    :  filename
                                 'itz'     :  target ion charge.
                                 'tsym'    :  target ion element symbol.
                                 'beref'   :  reference beam energy (eV/amu).
                                 'tdref'   :  reference target density (cm-3).
                                 'ttref'   :  reference target temperature (eV).
                                 'svref'   :  stopping coefft. at reference
                                              beam energy, target density and
                                              temperature (cm3 s-1).
                                 'be'      :  beam energies(eV/amu).
                                 'tdens'   :  target densities(cm-3).
                                 'ttemp'   :  target temperatures (eV).
                                 'svt'     :  stopping coefft. at reference beam
                                              energy and target density (cm3 s-1)
                                 'sved'    :  stopping coefft. at reference target
                                              temperature (cm3 s-1).
                                                1st dimension : beam energy
                                                2nd dimension : density

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Do not cut off the last element of each
                       array in fulldata.

      VERSION    :
            1.1    20-11-2012
            1.1    17-04-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata21 import xxdata_21

    # Examine inputs

    if not file:
        raise Exception('A valid adf21 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('adf21 file is invalid')

    mxbe  = 25
    mxtd  = 25
    mxtt  = 25
    iunit = 67

    be    = asarray(zeros(mxbe, float))
    tdens = asarray(zeros(mxtd, float))
    ttemp = asarray(zeros(mxtt, float))

    (itz, tsym, beref, tdref, ttref, svref,
     nbe, be, ntdens, tdens, nttemp, ttemp,
     svt, sved,  ) = xxdata_21(iunit, be, tdens, ttemp, file)

    fulldata = { 'file'    :  file,
                 'itz'     :  itz,
                 'tsym'    :  tsym,
                 'beref'   :  beref,
                 'tdref'   :  tdref,
                 'ttref'   :  ttref,
                 'svref'   :  svref,
                 'be'      :  be[0:nbe],
                 'tdens'   :  tdens[0:ntdens],
                 'ttemp'   :  ttemp[0:nttemp],
                 'svt'     :  svt[0:nttemp],
                 'sved'    :  sved[0:nbe, 0:ntdens] }

    return fulldata
