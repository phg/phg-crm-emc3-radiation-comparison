def xxdata_13(file=None):
    """
      PURPOSE    :  Reads an adf13 ionizations per photon (SXB) file from
                    the python command line and return all its contents
                    in a dictionary.

      fulldata = xxdata_13(file='/home/adas/adas/adf13/sxb96#li/sxb96#li_pju#li1.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf13 file

      RETURNS       fulldata    dictionary   contains
                                 'filename' :  adf13 filename
                                 'nblock'   :  number of ionisation rates in file
                                 'esym'     :  element Symbol
                                 'iz0'      :  nuclear charge
                                 'iz'       :  number of ionisation rates
                                 'iz1'      :  initial charge
                                                 int array(nblock)
                                 'cwavel'   :  wavelength
                                                 string array(nblock)
                                 'wave'     :  wavelength
                                                 float array(nblock)
                                 'cindm'    :  driving metastable, T for unresolved
                                                 string array(nblock)
                                 'nte'      :  number of temperatures in each block
                                                  float array(nblock)
                                 'te'       :  temperatures (eV)
                                                  float array(max(nte), nblock)
                                 'ndens'    :  number of temperatures in each block
                                                  float array(nblock)
                                 'dens'     :  density (cm-3)
                                                  float array(max(ndens), nblock)
                                 'sxb'      :  ionizations per photon (number) 
                                                  float array(max(nte), max(ndens), nblock)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Fix documentation of fulldata entries.

      VERSION    :
            1.1    22-08-2018
            1.2    07-05-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata13 import xxdata_13_calcpy

    # Examine inputs

    if not file:
        raise Exception('A valid adf13 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf13 file selected is invalid')

    dsnin = file.ljust(132)

    # Define variables for fortran routine

    nstore = 500
    ntdim  = 35
    nddim  = 24

    iunit     = 67
    esym      = '--'
    iz        = -1
    iz0       = -1
    iz1       = -1
    nbsel     = -1
    isela     = asarray(zeros(nstore), int)
    ita       = asarray(zeros(nstore), int)
    ida       = asarray(zeros(nstore), int)
    teta      = asarray(zeros((ntdim, nstore)), float)
    teda      = asarray(zeros((ntdim, nstore)), float)
    sxb       = asarray(zeros((ntdim, nddim, nstore)), float)
    cindm     = asarray(['--'] * nstore)
    cfile     = asarray(['--------'] * nstore)
    cpcode    = asarray(['--------'] * nstore)
    cwavel    = asarray(['----------'] * nstore)

    (iz0, iz, iz1, esym, nbsel, isela, cwavel, cfile, ctype, cindm,
     ita, ida, teta, teda, sxb) = xxdata_13_calcpy(iunit, dsnin, nstore, ntdim, nddim)

    c1 = asarray([row.tostring().strip().decode() for row in cwavel[0:nbsel]])
    c2 = asarray([row.tostring().strip().decode() for row in cfile[0:nbsel]])
    c3 = asarray([row.tostring().strip().decode() for row in ctype[0:nbsel]])
    c4 = asarray([row.tostring().strip().decode() for row in cindm[0:nbsel]])

    wave = asarray([res.upper().replace('A','') for res in c1]).astype(float)

    fulldata = { 'nblock'   :  nbsel,
                 'esym'     :  esym.strip().decode(),
                 'iz0'      :  iz0,
                 'iz'       :  iz,
                 'iz1'      :  iz1,
                 'cwavel'   :  c1,
                 'cindm'    :  c4,
                 'wave'     :  wave,
                 'nte'      :  ita[0:nbsel],
                 'te'       :  teta[0:ita.max(), 0:nbsel],
                 'ndens'    :  ida[0:nbsel],
                 'dens'     :  teda[0:ita.max(), 0:nbsel],
                 'sxb'      :  sxb[0:ita.max(), 0:ida.max(), 0:nbsel],
                 'filename' :  file.strip() }

    return fulldata
