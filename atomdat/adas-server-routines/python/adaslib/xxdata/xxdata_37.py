def xxdata_37(file=None):
    """
      PURPOSE    :  Read an adf37 electron distribution file and returns
                    all its contents in a dictionary.

      fulldata = xxdata_37(file='/home/adas/adas/adf37/JET_SOL_example.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf37 file

      RETURNS       fulldata     dict        all data in the file data

                    fulldata = { 'file'       :  adf37 filename
                                 'title'      :  header for file
                                 'icateg'     :  category of file
                                                   1 => superposition
                                                   2 => numerical
                                 'ieunit'     :  energy units of distribution function
                                                   1 => kelvin
                                                   2 => eV
                                 'nenerg'     :  type 1 => number of distribution families
                                                      2 => number of energy points
                                 'nblock'     :  type 1 => number of members in output family
                                                      2 => number of effective temperatures
                                 'nform1'     :  type of threshold behaviour
                                                   1 => cutoff
                                                   2 => energy^param1
                                 'nform2'     :  type of high-energy behaviour
                                                   1 => cutoff
                                                   2 => energy^(-param2[0])
                                                   3 => exp(-param2[0]*energy)
                                                   4 => exp(-param2[0]*energy^param2[1])
                                 'param1'     :  parameter of threshold form
                                 'param2'     :  parameter of high-energy form
                                 'ea'         :  energy points of tabulation
                                                   array(nblock,nenergy)
                                 'fa'         :  distribution function tabulation
                                                   array(nblock,nenergy)
                                 'mode_eng'   :  most probable energy (eV), array(nblock)
                                 'median_eng' :  median energy (eV), array(nblock)
                                 'teff'       :  effective temperature (eV), array(nblock)


      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Use adas readfile routine to read in file.

      VERSION    :
            1.1    17-07-2018
            1.2    23-11-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros, arange, where
    from adas import numlines, readfile

    # Examine inputs

    if not file:
        raise Exception('A valid adf37 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('adf37 file is invalid')

    
    # Read in file and parse resulting array of string

    lines, n_lines = readfile(file=file)
    
    title = lines[0]

    i1 = lines[1].find(';')
    if i1 == -1:
        icateg = int(lines[1])
    else:
        icateg = int(lines[1][0:i1])

    
    # cannot process type 1 files yet

    if icateg == 1:
        raise Exception('Type 1 adf37 file cannot be read by this routine')

    i1 = lines[2].find(';')
    if i1 == -1:
        ieunit = int(lines[2])
    else:
        ieunit = int(lines[2][0:i1])

    i1 = lines[3].find(';')
    if i1 == -1:
        nenerg = int(lines[3])
    else:
        nenerg = int(lines[3][0:i1])

    i1 = lines[4].find(';')
    if i1 == -1:
        nblock = int(lines[4])
    else:
        nblock = int(lines[4][0:i1])

    i1 = lines[5].find(';')
    if i1 == -1:
        val = lines[5].split()
        if len(val) == 2:
            nform1 = int(val[0])
            param1 = float(val[1])
    else:
        val = lines[5][0:i1].split()
        if len(val) == 2:
            nform1 = int(val[0])
            param1 = float(val[1])
        else:
            nform1 = int(val[0])
            param1 = None

    i1 = lines[6].find(';')
    if i1 == -1:
        val = lines[6].split()
        nform2 = int(val[0])
        param2 = float(val[1])
    else:
        val = lines[6][0:i1].split()
        nform2 = int(val[0])
        param2 = float(val[1])

    ea = asarray(zeros((nblock, nenerg)), float)
    fa = asarray(zeros((nblock, nenerg)), float)

    nl = numlines(nenerg, 7)

    flatten = lambda l: [item for sublist in l for item in sublist]
    i0 = 7
    i2 = 0
    for j in arange(nblock):
        i1 = i0 + j*nl*2
        i2 = i1 + nl
        ea[j,:] = asarray(flatten([row.split() for row in lines[i1:i2]]), float)
        fa[j,:] = asarray(flatten([row.split() for row in lines[i2:i2+nl]]), float)


    # Calculate effective temperature and mode and meadian energies

    mode_eng   = asarray(zeros(nblock), float)
    median_eng = asarray(zeros(nblock), float)
    teff       = asarray(zeros(nblock), float)

    for j in arange(nblock):

        contrib = asarray(zeros(nenerg), float)

        sum = 0.0
        mode_eng[j] = fa[j, 0]
        if nform1 == 2:
            sum = fa[j,0] * ea[j,0]**2 / (param1 + 2.0)
        contrib[0] = sum

        for ie in arange(nenerg-1):        # to penultimate point
            de  = ea[j, ie+1] - ea[j, ie]
            sum = sum + de * ( (fa[j,ie] * ea[j,ie]) + (fa[j,ie+1] * ea[j,ie+1]) ) / 2.0
            contrib[ie+1] = sum
            if fa[j, ie+1] > mode_eng[j]:
                mode_eng[j] = fa[j, ie+1]

        if nform2 == 2:
            sum = sum + fa[j, nenerg-1] * ea[j, nenerg-1]**2 / (param2 + 2.0)
        elif nform2 == 3:
            sum = sum + fa[j, nenerg-1] * (ea[j, nenerg-1] + 1.0/param2) / param2

        teff[j] = 2.0 * sum / 3.0

        ind = where(contrib >= sum/2.0)[0][0]
        median_eng[j] = ( (sum /2.0 - contrib[ind-1]) /
                          (contrib[ind] - contrib[ind-1]) * (ea[j,ind] - ea[j,ind-1]) +
                          ea[j,ind-1]
                        )

    # Return the results

    fulldata = { 'file'       :  file,
                 'title'      :  title,
                 'icateg'     :  icateg,
                 'ieunit'     :  ieunit,
                 'nenerg'     :  nenerg,
                 'nblock'     :  nblock,
                 'nform1'     :  nform1,
                 'nform2'     :  nform2,
                 'param1'     :  param1,
                 'param2'     :  param2,
                 'ea'         :  ea,
                 'fa'         :  fa,
                 'mode_eng'   :  mode_eng,
                 'median_eng' :  median_eng,
                 'teff'       :  teff
                }

    return fulldata
