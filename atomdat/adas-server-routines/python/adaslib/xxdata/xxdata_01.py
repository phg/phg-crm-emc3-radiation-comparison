def xxdata_01(file=None):
    """
      PURPOSE    :  Reads an adf01 charge exchange cross -section (QCX) file
                    from the python command line and return all its contents
                    in a dictionary.

      fulldata = xxdata_01(file='/home/adas/adas/adf01/qcx#h0/qcx#h0_old#c6.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf01 file

      RETURNS       fulldata     dictionary    contains
                                 'filename' :  adf01 filename
                                 'symbr'    :  receiver ion element symbol.
                                 'symbd'    :  donor ion element symbol.
                                 'izr'      :  ion charge of receiver.
                                 'izd'      :  ion charge of donor.
                                 'indd'     :  donor state index.
                                 'nenrgy'   :  number of energies in file.
                                 'nmin'     :  lowest n-shell for which data read.
                                 'nmax'     :  highest n-shell for which data read.
                                 'lparms'   :  flag if l-splitting parameters present.
                                                   True  => l-splitting parameters present.
                                                   False => l-splitting parameters absent.
                                 'lsetl'    :  flag if l-resolved data present.
                                                   True  => l-resolved data present.
                                                   False => l-resolved data absent.
                                 'enrgya'   :  collision energies (eV/amu).
                                                   dimension: energy index
                                 'alphaa'   :  extrapolation parameter alpha.
                                                   dimension: energy index
                                 'lforma'   :  parameters for calculating l-res x-sec.
                                                   dimension: energy index
                                 'xlcuta'   :  parameters for calculating l-res x-sec.
                                                   dimension: energy index
                                 'pl2a'     :  parameters for calculating l-res x-sec.
                                                   dimension: energy index
                                 'pl3a'     :  parameters for calculating l-res x-sec.
                                                   dimension: energy index
                                 'sigta'    :  total charge exchange cross-section (cm2).
                                                   dimension: energy index
                                 'signa'    :  n-resolved charge exchange cross-sections (cm2).
                                                   1st dimension: energy index
                                                   2nd dimension: n-shell
                                 'sigla'    :  l-resolved charge exchange cross-sections (cm2).
                                                   1st dimension: energy index
                                                   2nd dimension: indexed by i4idfl(n,l)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    01-04-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from adaslib import i4idfl
    from .xxdata01 import xxdata_01_calcpy

    # Examine inputs

    if not file:
        raise Exception('A valid adf01 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf01 file selected is invalid')

    dsnin = file.ljust(132)

    # Define variables for fortran routine

    mxneng = 35
    mxnshl = 100
    iunit  = 67

    symbd     = 'DD'
    symbr     = 'RR'
    izr       = -1
    izd       = -1
    indd      = -1
    nenrgy    = -1
    nmin      = -1
    nmax      = -1
    il_lparms = 0
    il_lsetl  = 0
    enrgya    = asarray(zeros(mxneng), float)
    alphaa    = asarray(zeros(mxneng), float)
    lforma    = asarray(zeros(mxneng), int)
    xlcuta    = asarray(zeros(mxneng), float)
    pl2a      = asarray(zeros(mxneng), float)
    pl3a      = asarray(zeros(mxneng), float)
    sigta     = asarray(zeros(mxneng), float)
    signa     = asarray(zeros((mxneng,mxnshl)), float)
    sigla     = asarray(zeros((mxneng,(mxnshl*(mxnshl+1))//2)), float)

    (symbd,symbr,izr,izd,indd,nenrgy,nmin,nmax,
     il_lparms,il_lsetl,enrgya,alphaa,lforma,xlcuta,pl2a,pl3a,
     sigta,signa,sigla) = xxdata_01_calcpy(iunit, dsnin, mxneng, mxnshl)

    last_l = i4idfl(nmax, nmax-1)

    if il_lparms == 1:
        lparms = True
    else:
        lparms = False

    if il_lsetl == 1:
        lsetl = True
    else:
        lsetl = False

    fulldata = { 'filename' :  file.strip(),
                 'symbr'    :  symbr.strip().decode(),
                 'symbd'    :  symbd.strip().decode(),
                 'izr'      :  izr,
                 'izd'      :  izd,
                 'indd'     :  indd,
                 'nenrgy'   :  nenrgy,
                 'nmin'     :  nmin,
                 'nmax'     :  nmax,
                 'lparms'   :  lparms,
                 'lsetl'    :  lsetl,
                 'enrgya'   :  enrgya[0:nenrgy],
                 'alphaa'   :  alphaa[0:nenrgy],
                 'lforma'   :  lforma[0:nenrgy],
                 'xlcuta'   :  xlcuta[0:nenrgy],
                 'pl2a'     :  pl2a[0:nenrgy],
                 'pl3a'     :  pl3a[0:nenrgy],
                 'sigta'    :  sigta[0:nenrgy],
                 'signa'    :  signa[0:nenrgy, 0:nmax],
                 'sigla'    :  sigla[0:nenrgy, 0:last_l] }

    return fulldata
