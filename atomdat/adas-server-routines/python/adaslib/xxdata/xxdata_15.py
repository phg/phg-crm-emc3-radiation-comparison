def xxdata_15(file=None):
    """
      PURPOSE    :  Reads adf15 (PEC) files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_15(file='/home/adas/adas/adf15/pec96#be/pec96#be_pju#be1.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf15 file

      RETURNS       fulldata     dict        PEC data (ph cm3 s-1)

                    fulldata = { 'a15file'     : adf15 filename
                                 'a04file'     : adf04 file name used to calculate adf15 data if available
                                 'a18file'     : adf18 file name used to calculate adf15 data if projection
                                                 was used.
                                 'esym'        : emitting ion - element symbol (string)
                                 'iz0'         : emitting ion - nuclear charge
                                 'iz'          : emitting ion - charge
                                                 (generalised to superstage label)
                                 'iz1'         : emitting ion - charge + 1
                                                 (generalised to superstage index= is + 1)
                                 'nptn'        : number of partitions in partition level
                                                 1st dim: partition level
                                 'nptnc'       : number of components in partition
                                                 1st dim: partition level
                                                 2nd dim: member partition in partition level
                                 'iptnla'      : partition level label (0=resolved root,1=unresolved root)
                                                 1st dim: partition level index
                                 'iptna'       : partition member label (labelling starts at 0)
                                                 1st dim: partition level index
                                                 2nd dim: member partition index in partition level
                                 'iptnca'      : component label (labelling starts at 0)
                                                 1st dim: partition level index
                                                 2nd dim: member partition index in partition level
                                                 3rd dim: component index of member partition
                                 'icnctv'      : connection vector of number of partitions
                                                 of each superstage in resolved case
                                                 including the bare nucleus
                                                 1st dim: connection vector index
                                 'cptn'        : text lines in partition block
                                                  1st dim: text line index
                                 'lres'        : True  => partial file
                                               : False => not partial file
                                 'lptn'        : True  => partition block present
                                               : False => partition block not present
                                 'lsup'        : True  => ss use of filmem field
                                               : False => old use of filmem field
                                 'cwavel'      : wavelength string (angstroms)
                                                 1st dim: data-block index
                                 'ctype'       : data type string
                                                 1st dim: data-block index
                                 'cindm'       : metastable index string
                                                 1st dim: data-block index
                                 'wavel'       : wavelength (angstroms)
                                                 1st dimension: data-block index
                                 'isppr'       : parent index for each line block
                                                 1st dim: index of block in adf15 file
                                 'ispbr'       : base index for each line block
                                                 1st dim: index of block in adf15 file
                                 'isstgr'      : s1 for each resolved data block
                                                 1st dim: index of block in adf15 file
                                 'iszr'        : ion charge relating to each line
                                                 1st dim: index of block in adf15 file
                                 'nte'         : number of electron temperatures
                                                 dimension: data-block index
                                 'ndens'       : number of electron densities
                                                 1st dim: data-block index
                                 'te'          : electron temperatures (units: ev)
                                                 1st dim: electron temperature index
                                                 2nd dim: data-block index
                                 'dens'        : electron densities (units: cm-3)
                                                  1st dim: electron density index
                                                  2nd dim: data-block index
                                 'pec'         : photon emissivity coeffts
                                                 1st dim: electron temperature index
                                                 2nd dim: electron density index
                                                 3rd dim: data-block index
                                 'pec_max'     : photon emissivity coefft. maximum
                                                 as a function of Te at first Ne value
                                                 1st dim: data-block index
                                 'tran_index'  : transition index within adf04 file
                                 'power_rank'  : the rank by its contribution to 
                                                 the total power

                   As different dimensions per block are possible, the multi-dimension arrays
                   use the max value for pixels, number of temperatures and number of densities.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Add afd18 expansion file name to fulldata
                     - Document all entries in fulldata.
           1.3     Martin O'Mullane
                     - Some files do not document the adf04 or adf18 inputs. Return
                       None for these.

      VERSION    :
            1.1    22-08-2018
            1.2    27-09-2018

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata15 import xxdata_15_calc

    # Examine inputs

    if not file:
        raise Exception('A valid adf15 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('adf15 file is invalid')

    dsnin = file.ljust(132)

    # Dimensions

    nstore_  = 500
    ntdim_   = 48
    nddim_   = 30
    ndptnl_  = 4
    ndptn_   = 128
    ndptnc_  = 256
    ndcnct_  = 100
    ndstack_ = 40

    # Define variables

    str         = '--------------------------------------------------------------------------------'

    iunit       = 67
    iz0         = -1
    iz          = 0
    iz1         = 0
    esym        = '--'
    nptnl       = 0
    nptn        = asarray(zeros(ndptnl_), int)
    nptnc       = asarray(zeros((ndptnl_,ndptn_), int))
    iptnla      = asarray(zeros(ndptnl_), int)
    iptna       = asarray(zeros((ndptnl_,ndptn_), int))
    iptnca      = asarray(zeros((ndptnl_,ndptn_,ndptnc_), int))
    ncnct       = 0
    icnctv      = asarray(zeros(ndcnct_), int)
    ncptn_stack = 0
    cptn_stack  = asarray([str] * ndstack_)
    lres        = False
    lptn        = False
    lsup        = False
    nbsel       = 0
    isela       = asarray(zeros(nstore_), int)
    cwavel      = asarray(['----------'] * nstore_)
    cfile       = asarray(['--------'] * nstore_)
    ctype       = asarray(['--------'] * nstore_)
    cindm       = asarray(['--------'] * nstore_)
    wavel       = asarray(zeros(nstore_), float)
    ispbr       = asarray(zeros(nstore_), int)
    isppr       = asarray(zeros(nstore_), int)
    isstgr      = asarray(zeros(nstore_), int)
    iszr        = asarray(zeros(nstore_), int)
    ita         = asarray(zeros(nstore_), int)
    ida         = asarray(zeros(nstore_), int)
    teta        = asarray(zeros((ntdim_,nstore_), float))
    teda        = asarray(zeros((nddim_,nstore_), float))
    pec         = asarray(zeros((ntdim_,nddim_,nstore_), float))
    pec_max     = asarray(zeros(nstore_), float)

    (iz0, iz, iz1, esym, nptnl, nptn, nptnc,
     iptnla, iptna, iptnca, ncnct, icnctv,
     ncptn_stack, cptn_stack, lres, lptn, lsup,
     nbsel, isela, cwavel, cfile, ctype, cindm, wavel,
     ispbr, isppr, isstgr, iszr,
     ita, ida, teta, teda, pec, pec_max) = xxdata_15_calc(iunit, dsnin, nstore_, ntdim_, nddim_, ndptnl_ , ndptn_, ndptnc_, ndcnct_, ndstack_)

    c1 = asarray([row.tostring().strip().decode() for row in ctype[0:nbsel]])
    c2 = asarray([row.tostring().strip().decode() for row in cindm[0:nbsel]])


    # check for connection vector

    if ncnct > 0:

        icnctv     =  icnctv[0:ncnct]
        cptn_stack =  asarray([row.tostring().strip().decode() for row in cptn_stack[0:ncptn_stack]])
        nptn       =  nptn[0:nptnl]
        iptna      =  iptna[0:nptnl, 0:nptn.max()]
        nptnc      =  nptnc[0:nptnl, 0:nptn.max()]
        iptnca     =  iptnca[0:nptnl, 0:nptn.max(), 0:nptnc.max()]
        iptnla     =  iptnla[0:nptnl]

    else:

        icnctv     =  -1
        cptn_stack = 'Not available'
        nptn       =  0
        iptna      =  -1
        nptnc      =  0
        iptnca     =  -1
        iptnla     =  -1


    # extract information from comments of new style adf15 files

    a04file    = 'unknown'
    tran_index = -1
    power_rank = -1

    with open(file, 'r') as f:
        all = f.read().splitlines()
    
    a04file = None
    a18file = None
    ind = -1
    with open(file) as f:
        for i, line in enumerate(f, 0):
            if 'ispp nspp sz    tg pr wr' in line:
                ind = i
            if 'SPECIFIC ION FILE  :' in line.upper():
                a04file = line.split(':')[1].strip()
            if 'adf04 source file    :' in line.lower():
                a04file = line.split(':')[1].strip()
            if 'EXPANSION FILE     :' in line.upper():
                a18file = line.split(':')[1].strip()
            if 'adf18/a17_p208 file  :' in line.lower():
                a18file = line.split(':')[1].strip()

    if ind > 0:

        tran_index = []
        power_rank = []

        i1  = all[ind].index('tg')
        all = all[ind+2:ind+2+nbsel]

        for line in all:
             tran_index.append(int(line[i1-2:i1+2]))
             power_rank.append(int(line[i1+2:i1+5]))

        tran_index = asarray(tran_index)
        power_rank = asarray(power_rank)

    fulldata = { 'a15file'    :  file.strip(),
                 'a04file'    :  a04file,
                 'a18file'    :  a18file,
                 'esym'       :  esym.strip().decode(),
                 'iz0'        :  iz0,
                 'iz'         :  iz,
                 'iz1'        :  iz1,
                 'nptn'       :  nptn,
                 'iptna'      :  iptna,
                 'nptnc'      :  nptnc,
                 'iptnca'     :  iptnca,
                 'iptnla'     :  iptnla,
                 'icnctv'     :  icnctv,
                 'ndens'      :  ida[0:nbsel],
                 'nte'        :  ita[0:nbsel],
                 'dens'       :  teda[0:ida.max(), 0:nbsel],
                 'te'         :  teta[0:ita.max(), 0:nbsel],
                 'ctype'      :  c1,
                 'cindm'      :  c2,
                 'cptn'       :  cptn_stack,
                 'wave'       :  wavel[0:nbsel],
                 'isppr'      :  isppr[0:nbsel],
                 'ispbr'      :  ispbr[0:nbsel],
                 'isstgr'     :  isstgr[0:nbsel],
                 'iszr'       :  iszr[0:nbsel],
                 'lres'       :  bool(lres),
                 'lptn'       :  bool(lptn),
                 'lsup'       :  bool(lsup),
                 'pec'        :  pec[0:ita.max(),0:ida.max(),0:nbsel],
                 'pec_max'    :  pec_max[0:nbsel],
                 'tran_index' :  tran_index,
                 'power_rank' :  power_rank  }

    return fulldata
