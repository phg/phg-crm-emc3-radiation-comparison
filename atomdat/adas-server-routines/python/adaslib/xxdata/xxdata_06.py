def xxdata_06(file=None, ndlev=450, ndtrn=15000, ndmet=13, ndqdn=8, ndpro=20, nvmax=50, itieactn=True):
    """
      PURPOSE    :  Reads adf06 specific ion files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_06(file='/home/adas/adas/adf06/lilike/lilike_aug99#li0_t1.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf06 file

      OPTIONAL   :  ndlev        int         max. no. levels (450)
                    ndtrn        int         max. no. transitions (15000)
                    ndmet        int         max. no. metastables (13)
                    ndqdn        int         max. principal quantum number of orbitals (8)
                    nvmax        int         max. no. temperatures/energies (100)

                    itieactn     bool        set to False to terminate if any levels
                                             are untied.

      RETURNS       fulldata    dictionary   contains
                                 'file'       :  adf06 filename
                                 'adf06_type' :  variant of the adf06 file
                                                   1 = collision strengths vs E/delta_E
                                                   2 = unused
                                                   3 = effective collision strengths
                                                   4 = non-Maxwellian
                                                   5 = collision strengths vs E-delta_E (Ryd)
                                                   6 = specific for BBGP
                                 'iz0'        :  nuclear charge
                                 'iz'         :  recombined ion charge
                                 'ia'         :  energy level index number (array)
                                 'cstrga'     :  nomenclature/configuration for level (array)
                                 'isa'        :  multiplicity for level (array)
                                                    note: (isa-1)/2 = quantum number (s)
                                 'ila'        :  quantum number (l) for level (array)
                                 'xja'        :  quantum number (j-value) for level (array)
                                                    note: (2*xja)+1 = statistical weight
                                 'wa'         :  energy relative to level 1 (cm-1) for level (array)
                                 'npla'       :  number of parent/zeta contributions to ionisation
                                                 of level (array)
                                 'ipla'       :  parent index of contributions to ionisation
                                                 of level (array[parent index, level index])
                                 'zpla'       :  effective zeta parameter of contributions to ionisation
                                                 of level (array[parent index, level index])
                                 'ltied'      :  indicates whether a level is tied (True )
                                                 or not (False). (array)
                                 'bwno'       :  ionisation potential (cm-1) of lowest parent
                                 'bwnoa'      :  ionisation potential (cm-1) of parents (array)
                                 'cprta'      :  parent name in brackets (array)
                                 'prtwta'     :  parent weight for parents in bwnoa array
                                 'eorb'       :  orbital energies (Rydberg) of occupied orbitals
                                                 in the configuration present (array)
                                 'qdn'        :  quantum defect for n-shells (array)
                                 'qdorb'      :  quantum defects for nl-orbitals (array) indexed by
                                                 ADAS i4idfl routine.
                                 'lqdorb'     :  indication whether source data available for quantum
                                                 defect (True) (array)
                                 'te'         :  electron temperatures (K) or energy parameter (array)
                                 'lower'      :  lower energy level index for excitation (array)
                                 'upper'      :  upper energy level index for excitation (array)
                                 'aval'       :  transition probability (s-1) (array)
                                 'gamma'      :  gamma (array [transition, te, projectile]
                                                   - effective collision strength for type 3 adf06
                                                   - collision strength for type 1
                                 'projectile' :  name of projectile (array [projectile]), eg '7^Li'
                                 'charge_prj' :  charge of projectile (array [projectile])
                                 'mass_prj'   :  mass of projectile (array [projectile]) in atomic number
                                 'mass_target':  mass of target (array [projectile]) in atomic number
                                 'bt_type'    :  Burgess Tully classification of transition (1-4)
                                 'beth'       :  Bethe coefficient
                                 'level_ion'  :  ionising level index for ionisation (array)
                                 'parent_ion' :  parent level index for ionisation (array)
                                 'ion'        :  ionisation rate (cm3 s-1) (array[te, transition])


      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    06-09-2018

    """

    # External routines required

    import os
    from numpy import asarray, zeros, int8, where, nditer, transpose, atleast_1d, squeeze
    from adaslib import xxidtl, xxdtes, xxcftr
    from .xxdata06 import xxdata_06_calcpy

    # Examine inputs

    if not file:
        raise Exception('A valid adf06 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf06 file selected is invalid')


    # Settings

    iunit = 67
    if itieactn == True:
        itieactn_int = 1
    else:
        itieactn_int = 0

    # Define variables for fortran routine, read the data and
    # classify the type with a call to bxttyp

    str      = '------------------------------------------------------------------------------------------'

    titled   = '   '
    iz       = -1
    iz0      = -1
    iz1      = -1
    bwno     = -1.0
    npl      = -1
    bwnoa    = asarray(zeros(ndmet), float)
    lbseta   = asarray(zeros(ndmet), int8)
    prtwta   = asarray(zeros(ndmet), float)
    cprta    = asarray(['---------'] * ndmet)
    il       = -1
    qdorb    = asarray(zeros((ndqdn*(ndqdn+1))//2), float)
    lqdorb   = asarray(zeros((ndqdn*(ndqdn+1))//2), int8)
    qdn      = asarray(zeros(ndqdn), float)
    iorb     = -1
    ia       = asarray(zeros(ndlev), int)
    cstrga   = asarray([str] * ndlev)
    isa      = asarray(zeros(ndlev), int)
    ila      = asarray(zeros(ndlev), int)
    xja      = asarray(zeros(ndlev), float)
    wa       = asarray(zeros(ndlev), float)
    cpla     = asarray(['-'] * ndlev)
    npla     = asarray(zeros(ndlev), int)
    ipla     = asarray(zeros((ndmet,ndlev)), int)
    zpla     = asarray(zeros((ndmet,ndlev)), float)
    np       = -1
    proj     = asarray(zeros((3,ndpro)), float)
    cpro     = asarray(['-----'] * ndpro)
    nv       = -1
    scef     = asarray(zeros(nvmax), float)
    itran    = -1
    maxlev   = 0
    tcode    = asarray(['-'] * ndtrn)
    i1a      = asarray(zeros(ndtrn), int)
    i2a      = asarray(zeros(ndtrn), int)
    aval     = asarray(zeros(ndtrn), float)
    scom     = asarray(zeros((nvmax,ndtrn,ndpro)), float)
    beth     = asarray(zeros((ndtrn,ndpro)), float)
    iadftyp  = 0
    lprn     = False
    lcpl     = False
    lorb     = False
    lbeth    = False
    lityp    = False
    lstyp    = False
    ltied    = asarray(zeros(ndlev), bool)
    itieactn = itieactn_int


    (titled, iz, iz0, iz1, bwno, npl, bwnoa, lbseta, prtwta, cprta,
     il, qdorb, lqdorb, qdn, iorb, ia, cstrga, isa, ila, xja, wa, cpla,
     npla, ipla, zpla, nv, scef, itran, maxlev, tcode, i1a, i2a, aval, scom,
     np, proj, cpro, beth, iadftyp, lprn, lcpl, lorb, lbeth, lityp, lstyp,
     itieactn, ltied) = xxdata_06_calcpy(iunit,file,itieactn_int,ndlev, ndtrn, ndmet,ndqdn, nvmax, ndpro)

    # Convert output arrays to only include relevant data

    c1 = asarray([row.tostring().strip().decode() for row in cprta[0:npl]])
    c2 = asarray([row.tostring().strip().decode() for row in cstrga[0:il]])
    c3 = [row.tostring().strip().decode() for row in cpla[0:il]]
    c4 = asarray([row.tostring().strip().decode() for row in cpro[0:np]])
    tc = atleast_1d(asarray([row.tostring().decode().upper() for row in tcode[0:itran]]))

    # Projectile data

    mass_t   = asarray(proj[0,0:np])
    charge_p = asarray(proj[1,0:np])
    mass_p   = asarray(proj[2,0:np])

    # Calculate orbital energies from orbital quantum defects

    eorb = -1
    if lorb == True:
        eorb = asarray(zeros(qdorb.size), float)
        ind = where(lqdorb[0:iorb] == 1)
        for i in nditer(ind):
            n,l = xxidtl(i+1)
            eorb[i] = (float(iz1) / (float(n)-qdorb[i]) )**2
        eorb = eorb[0:len(ind[0])]

    # Generate standard configurations if adf06 is Eissner (c2 holds trimmed set)

    cstrga_std = []
    for cfg in c2:
       is_eissner,_,_,_ = xxdtes(cfg)
       if is_eissner:
           res = xxcftr(cfg, transform=3)
           cstrga_std.append(res.strip())
       else:
           cstrga_std.append(cfg)
    cstrga_std = asarray(cstrga_std)

    # Temperatures/energies

    if nv > 0:
       scef = scef[0:nv]
    else:
       scef       = -1
       gamma      = -1


    # Indices lists for excitation and ionisation (tc holds tcode in uppercase)

    ind_exc = where(tc == ' ')
    ind_sl  = where(tc == 'S')
    n_exc   = len(ind_exc[0])
    n_sl    = len(ind_sl[0])

    # electron impact excitation

    if n_exc > 0:

        lower = i1a[ind_exc]
        upper = i2a[ind_exc]

        aval  = aval[ind_exc]
        beth  = beth[ind_exc,0:np]
        if np == 1:
            gamma = transpose(squeeze(scom[0:nv, ind_exc, 0]))
        else:
            gamma = scom[0:nv, ind_exc[0], 0:np].transpose((1,0,2))

    else:

        lower = -1
        upper = -1
        aval  = -1
        beth  = -1


    # Ionisation (S-lines)

    if n_sl > 0:

        level_ion  = i1a[ind_sl]
        parent_ion = i2a[ind_sl]

        if np == 1:
            ion = transpose(squeeze(scom[0:nv, ind_sl, 0]))
        else:
            ion = transpose(scom[0:nv, ind_sl[0], 0:np], (1,0,2))

    else:

        level_ion  = -1
        parent_ion = -1
        ion        = -1


    # Construct the dictionary of results

    fulldata = { 'file'       :  file,
                 'adf06_type' :  iadftyp,
                 'iz0'        :  iz0,
                 'iz'         :  iz,
                 'ia'         :  ia[0:il],
                 'cstrga'     :  c2,
                 'cstrga_std' :  cstrga_std,
                 'isa'        :  isa[0:il],
                 'ila'        :  ila[0:il],
                 'xja'        :  xja[0:il],
                 'wa'         :  wa[0:il],
                 'npla'       :  npla[0:il],
                 'ipla'       :  ipla[0:npl, 0:il],
                 'zpla'       :  zpla[0:npl, 0:il],
                 'ltied'      :  (ltied[0:il]).astype(bool),
                 'bwno'       :  bwno,
                 'bwnoa'      :  bwnoa[0:npl],
                 'cprta'      :  c1,
                 'prtwta'     :  prtwta[0:npl],
                 'eorb'       :  eorb,
                 'qdn'        :  qdn,
                 'qdorb'      :  qdorb,
                 'lqdorb'     :  lqdorb.astype(bool),
                 'te'         :  scef,
                 'lower'      :  lower,
                 'upper'      :  upper,
                 'aval'       :  aval,
                 'gamma'      :  gamma,
                 'beth'       :  beth,
                 'projectile' :  c4,
                 'charge_prj' :  charge_p,
                 'mass_prj'   :  mass_p,
                 'mass_target':  mass_t,
                 'level_ion'  :  level_ion,
                 'parent_ion' :  parent_ion,
                 'ion'        :  ion,
               }

    return fulldata
