def xxdata_04(file=None, ndlev=4500, ndtrn=150000, ndmet=4, ndqdn=8, nvmax=50, itieactn=True):
    """
      PURPOSE    :  Reads adf04 specific ion files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_04(file='/home/adas/adas/adf04/belike/belike_cpb03#be0.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf04 file

      OPTIONAL   :  ndlev        int         max. no. levels (4500)
                    ndtrn        int         max. no. transitions (150000)
                    ndmet        int         max. no. metastables (4)
                    ndqdn        int         max. principal quantum number of orbitals (8)
                    nvmax        int         max. no. temperatures/energies

                    itieactn     bool        set to False to terminate if any levels
                                             are untied.

      RETURNS       fulldata    dictionary   contains
                                 'file'       :  adf04 filename
                                 'adf04_type' :  variant of the adf04 file
                                                   1 = collision strengths vs E/delta_E
                                                   2 = unused
                                                   3 = effective collision strengths
                                                   4 = non-Maxwellian
                                                   5 = collision strengths vs E-delta_E (Ryd)
                                                   6 = specific for BBGP
                                 'iz0'        :  nuclear charge
                                 'iz'         :  recombined ion charge
                                 'ia'         :  energy level index number (array)
                                 'cstrga'     :  nomenclature/configuration for level (array)
                                 'isa'        :  multiplicity for level (array)
                                                    note: (isa-1)/2 = quantum number (s)
                                 'ila'        :  quantum number (l) for level (array)
                                 'xja'        :  quantum number (j-value) for level (array)
                                                    note: (2*xja)+1 = statistical weight
                                 'wa'         :  energy relative to level 1 (cm-1) for level (array)
                                 'npla'       :  number of parent/zeta contributions to ionisation
                                                 of level (array)
                                 'ipla'       :  parent index of contributions to ionisation
                                                 of level (array[parent index, level index])
                                 'zpla'       :  effective zeta parameter of contributions to ionisation
                                                 of level (array[parent index, level index])
                                 'ltied'      :  indicates whether a level is tied (True )
                                                 or not (False). (array)
                                 'bwno'       :  ionisation potential (cm-1) of lowest parent
                                 'bwnoa'      :  ionisation potential (cm-1) of parents (array)
                                 'cprta'      :  parent name in brackets (array)
                                 'prtwta'     :  parent weight for parents in bwnoa array
                                 'eorb'       :  orbital energies (Rydberg) of occupied orbitals
                                                 in the configuration present (array)
                                 'qdn'        :  quantum defect for n-shells (array)
                                 'qdorb'      :  quantum defects for nl-orbitals (array) indexed by
                                                 ADAS i4idfl routine.
                                 'lqdorb'     :  indication whether source data available for quantum
                                                 defect (True) (array)
                                 'te'         :  electron temperatures (K) or energy parameter (array)
                                 'lower'      :  lower energy level index for excitation (array)
                                 'upper'      :  upper energy level index for excitation (array)
                                 'aval'       :  transition probability (s-1) (array)
                                 'gamma'      :  gamma (array [transition, te]
                                                   - effective collision strength for type 3 adf04
                                                   - collision strength for type 1 and 5
                                 'bt_type'    :  Burgess Tully classification of transition (1-4)
                                 'beth'       :  Bethe coefficient
                                 'level_rec'  :  capturing level index for recombination (array)
                                 'parent_rec' :  parent level index for recombination (array)
                                 'rec'        :  recombination rate (cm3 s-1) (array[te, transition])
                                 'level_ion'  :  ionising level index for ionisation (array)
                                 'parent_ion' :  parent level index for ionisation (array)
                                 'ion'        :  ionisation rate (cm3 s-1) (array[te, transition])
                                 'level_cx'   :  capturing level index for charge exchange (array)
                                 'parent_cx'  :  parent level index for cx (array)
                                 'cx'         :  cx rate (cm3 s-1) (array[te, transition])


      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Add standard configurations, Bethe coefficient, Burgess-Tully types
                       and adf04 file type to the output dictionary.
           1.3     Martin O'Mullane
                     - Return boolean values for ltied and lqdorb rather than rely on
                       interpreting the int value returned from fortran.

      VERSION    :
            1.1    01-07-2018
            1.2    06-07-2018
            1.3    10-07-2018

    """

    # External routines required

    import os
    from numpy import asarray, zeros, int8, where, nditer, transpose
    from adas2xx import bxttyp
    from adaslib import xxidtl, xxdtes, xxcftr
    from .xxdata04 import xxdata_04_calcpy

    # Examine inputs

    if not file:
        raise Exception('A valid adf04 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('the adf04 file selected is invalid')


    # Settings

    iunit = 67
    if itieactn == True:
        itieactn_int = 1
    else:
        itieactn_int = 0

    # Define variables for fortran routine, read the data and
    # classify the type with a call to bxttyp

    str      = '------------------------------------------------------------------------------------------'

    titled   = '   '
    iz       = -1
    iz0      = -1
    iz1      = -1
    bwno     = -1.0
    npl      = -1
    bwnoa    = asarray(zeros(ndmet), float)
    lbseta   = asarray(zeros(ndmet), int8)
    prtwta   = asarray(zeros(ndmet), float)
    cprta    = asarray(['---------'] * ndmet)
    il       = -1
    qdorb    = asarray(zeros((ndqdn*(ndqdn+1))//2), float)
    lqdorb   = asarray(zeros((ndqdn*(ndqdn+1))//2), int8)
    qdn      = asarray(zeros(ndqdn), float)
    iorb     = -1
    ia       = asarray(zeros(ndlev), int)
    cstrga   = asarray([str] * ndlev)
    isa      = asarray(zeros(ndlev), int)
    ila      = asarray(zeros(ndlev), int)
    xja      = asarray(zeros(ndlev), float)
    wa       = asarray(zeros(ndlev), float)
    cpla     = asarray(['-'] * ndlev)
    npla     = asarray(zeros(ndlev), int)
    ipla     = asarray(zeros((ndmet,ndlev)), int)
    zpla     = asarray(zeros((ndmet,ndlev)), float)
    nv       = -1
    scef     = asarray(zeros(nvmax), float)
    itran    = -1
    maxlev   = 0
    tcode    = asarray(['-'] * ndtrn)
    i1a      = asarray(zeros(ndtrn), int)
    i2a      = asarray(zeros(ndtrn), int)
    aval     = asarray(zeros(ndtrn), float)
    scom     = asarray(zeros((nvmax,ndtrn)), float)
    beth     = asarray(zeros(ndtrn), float)
    iadftyp  = 0
    lprn     = False
    lcpl     = False
    lorb     = False
    lbeth    = False
    letyp    = False
    lptyp    = False
    lrtyp    = False
    lhtyp    = False
    lityp    = False
    lstyp    = False
    lltyp    = False
    ltied    = asarray(zeros(ndlev), bool)
    itieactn = itieactn_int


    (titled, iz, iz0, iz1, bwno, npl, bwnoa, lbseta, prtwta, cprta,
     il, qdorb, lqdorb, qdn, iorb, ia, cstrga, isa, ila, xja, wa, cpla,
     npla, ipla, zpla, nv, scef, itran, maxlev, tcode, i1a, i2a, aval, scom,
     beth, iadftyp, lprn, lcpl, lorb, lbeth, letyp, lptyp, lrtyp, lhtyp, lityp, lstyp,
     lltyp, itieactn, ltied) = xxdata_04_calcpy(iunit,file,itieactn_int,ndlev, ndtrn, ndmet,ndqdn, nvmax)

    a04typ = bxttyp(ndlev, ndmet, ndtrn, itran, tcode, i1a, i2a , aval)


    # Convert output arrays to only include relevant data

    c1 = asarray([row.tostring().strip().decode() for row in cprta[0:npl]])
    c2 = asarray([row.tostring().strip().decode() for row in cstrga[0:il]])
    c3 = [row.tostring().strip().decode() for row in cpla[0:il]]
    c4 = [row.tostring().strip().decode() for row in tcode[0:itran]]

    if nv > 0:
        scef = scef[0:nv]
    else:
        scef = -1

    # Calculate orbital energies from orbital quantum defects

    eorb = -1
    if lorb == True:
        eorb = asarray(zeros(qdorb.size), float)
        ind = where(lqdorb[0:iorb] == 1)
        for i in nditer(ind):
            n,l = xxidtl(i+1)
            eorb[i] = (float(iz1) / (float(n)-qdorb[i]) )**2
        eorb = eorb[0:len(ind[0])]
    
    # Generate standard configurations if adf04 is Eissner (c2 holds trimmed set)
    
    cstrga_std = []
    for cfg in c2:
       is_eissner,_,_,_ = xxdtes(cfg)
       if is_eissner:
           res = xxcftr(cfg, transform=3)
           cstrga_std.append(res.strip())
       else:
           cstrga_std.append(cfg)
    cstrga_std = asarray(cstrga_std)

    
    # electron impact excitation

    icnte = a04typ['icnte']
    if icnte > 0:

        ind   = (a04typ['ietrn'])[0:icnte]-1
        lower = (a04typ['ie1a'])[0:icnte]
        upper = (a04typ['ie2a'])[0:icnte]
        tcode = tcode[0:icnte]

        aval  = (a04typ['aa'])[ind]
        gamma = transpose(scom[0:nv, ind])
        beth  = beth[ind]
        
        # determine Burgess-Tully type
        
        fzero = 1.0e-4
        fbig  = 0.01
        
        bt_type = asarray(zeros(icnte), int)
        for j in range(icnte):
            
            wvnou = wa[upper[j]-1]
            wvnol = wa[lower[j]-1]
            elu   = abs(wvnou - wvnol) / 109737.26
            wtu   = 2.0 * xja[upper[j]-1] + 1.0
            wtl   = 2.0 * xja[lower[j]-1] + 1.0
            ain   = aval[j]
            s     = 3.73491e-10 * wtu * ain / (elu**3.0)       
            fin   = 3.333333e-1 * elu * s / wtl               
            
            if isa[upper[j]-1] == isa[lower[j]-1]:
                if (abs(ila[upper[j]-1]-ila[lower[j]-1]) <= 1) and (fin > fbig):
                    bt_type[j] = 1
                else:
                    bt_type[j] = 2
            else:
                if (fin > fzero) and (fin < fbig):
                    bt_type[j] = 4
                else:
                    bt_type[j] = 3
    
    else:

         lower = -1
         upper = -1
         aval  = -1
         gamma = -1
         beth  = -1
         bt_type = -1


    # Recombination (R-lines)

    icntr = a04typ['icntr']
    if icntr > 0:

        ind        = (a04typ['irtrn'])[0:icntr]-1
        level_rec  = (a04typ['ia2a'])[0:icntr]
        parent_rec = (a04typ['ia1a'])[0:icntr]

        rec        = transpose(scom[0:nv, ind])

    else:

        level_rec  = -1
        parent_rec = -1
        rec        = -1


    # Ionisation (S-lines)

    icnts = a04typ['icnts']
    if icnts > 0:

        ind        = (a04typ['istrn'])[0:icnts]-1
        level_ion  = (a04typ['is2a'])[0:icnts]
        parent_ion = (a04typ['is1a'])[0:icnts]

        ion        = transpose(scom[0:nv, ind])

    else:

        level_ion  = -1
        parent_ion = -1
        ion        = -1


    # Charge exchange (H-lines)

    icnth = a04typ['icnth']
    if icnth > 0:

        ind       = (a04typ['ihtrn'])[0:icnth]-1
        level_cx  = i2a[ind]
        parent_cx = i1a[ind]

        cx        = transpose(scom[0:nv, ind])

    else:

        level_cx  = -1
        parent_cx = -1
        cx        = -1

    # Construct the dictionary of results

    fulldata = { 'file'       :  file,
                 'adf04_type' :  iadftyp,
                 'iz0'        :  iz0,
                 'iz'         :  iz,
                 'ia'         :  ia[0:il],
                 'cstrga'     :  c2,
                 'cstrga_std' :  cstrga_std,
                 'isa'        :  isa[0:il],
                 'ila'        :  ila[0:il],
                 'xja'        :  xja[0:il],
                 'wa'         :  wa[0:il],
                 'npla'       :  npla[0:il],
                 'ipla'       :  ipla[0:npl, 0:il],
                 'zpla'       :  zpla[0:npl, 0:il],
                 'ltied'      :  (ltied[0:il]).astype(bool),
                 'bwno'       :  bwno,
                 'bwnoa'      :  bwnoa[0:npl],
                 'cprta'      :  c1,
                 'prtwta'     :  prtwta[0:npl],
                 'eorb'       :  eorb,
                 'qdn'        :  qdn,
                 'qdorb'      :  qdorb,
                 'lqdorb'     :  lqdorb.astype(bool),
                 'te'         :  scef,
                 'lower'      :  lower,
                 'upper'      :  upper,
                 'aval'       :  aval,
                 'gamma'      :  gamma,
                 'beth'       :  beth,
                 'bt_type'    :  bt_type,
                 'level_rec'  :  level_rec,
                 'parent_rec' :  parent_rec,
                 'rec'        :  rec,
                 'level_ion'  :  level_ion,
                 'parent_ion' :  parent_ion,
                 'ion'        :  ion,
                 'level_cx'   :  level_cx,
                 'parent_cx'  :  parent_cx,
                 'cx'         :  cx
               }

    return fulldata
