def xxdata_11(file=None, adf11type='xxx'):
    """
      PURPOSE    :  Reads adf11 files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_11(file='/home/adas/adas/adf11/acd96/acd96_c.dat', adf11type='acd')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf11 file
                    adf11type    str         type of adf11 file,

                                             acd  recombination coeffts
                                             scd  ionisation coeffts
                                             ccd  CX recombination coeffts
                                             prb  recomb/brems power coeffts
                                             prc  CX power coeffts
                                             qcd  base meta. coupl. coeffts
                                             xcd  parent meta. coupl. coeffts
                                             plt  low level line power coeffts
                                             pls  represent. line power coefft
                                             zcd  effective charge
                                             ycd  effective squared charge
                                             ecd  effective ionisation potential

                                             if not supplied the code attempts to detect
                                             it from the first line of the file.

      RETURNS       fulldata     dict        adf11 data

                    fulldata={  'iz0'       : nuclear charge
                                'class'     : One of 'acd','scd','ccd','prb','prc',
                                                     'qcd','xcd','plt','pls','zcd',
                                                     'ycd','ecd'
                                'iclass'    : 1..12 corresponding to above list.
                                'is1min'    : minimum ion charge + 1
                                              (generalised to connection vector index)
                                'is1max'    : maximum ion charge + 1
                                              (note excludes the bare nucleus)
                                              (generalised to connection vector index
                                               and excludes last one which always remains
                                               the bare nucleus)
                                'nptnl'     : number of partition levels in block
                                'nptn'      : number of partitions in partition level
                                              1st dim: partition level
                                'nptnc'     : number of components in partition
                                              1st dim: partition level
                                              2nd dim: member partition in partition level
                                'iptnla'    : partition level label (0=resolved root,1=
                                                                       unresolved root)
                                              1st dim: partition level index
                                'iptna'     : partition member label (labelling starts at 0)
                                              1st dim: partition level index
                                              2nd dim: member partition index in partition
                                              level
                                'iptnca'    : component label (labelling starts at 0)
                                              1st dim: partition level index
                                              2nd dim: member partition index in partition
                                              level
                                              3rd dim: component index of member partition
                                'ncnct'     : number of elements in connection vector
                                'icnctv'    : connection vector of number of partitions
                                              of each superstage in resolved case
                                              including the bare nucleus
                                              1st dim: connection vector index

                                'iblmx'     : number of (sstage, parent, base)
                                              blocks in isonuclear master file
                                'ismax'     : number of charge states
                                              in isonuclear master file
                                              (generalises to number of elements in
                                               connection vector)
                                'dnr_elem'  : donor element name (for ccd and prc)
                                'dnr_ams'   : donor element mass (for ccd and prc)
                                'isppr'     : 1st (parent) index for each partition block
                                              1st dim: index of (sstage, parent, base)
                                                       block in isonuclear master file
                                'ispbr'     : 2nd (base) index for each partition block
                                              1st dim: index of (sstage, parent, base)
                                                       block in isonuclear master file
                                'isstgr'    : s1 for each resolved data block
                                              (generalises to connection vector index)
                                              1st dim: index of (sstage, parent, base)
                                                       block in isonuclear master file

                                'idmax'     : number of dens values in
                                              isonuclear master files
                                'itmax'     : number of temp values in
                                              isonuclear master files
                                'ddens'     : log10(electron density(cm-3)) from adf11
                                'dtev'      : log10(electron temperature (eV) from adf11
                                'drcof'     : if(iclass <=9):
                                                log10(coll.-rad. coefft.) from
                                                isonuclear master file
                                              if(iclass >=10):
                                                coll.-rad. coefft. from
                                                isonuclear master file
                                              1st dim: index of (sstage, parent, base)
                                                        block in isonuclear master file
                                              2nd dim: electron temperature index
                                              3rd dim: electron density index

                                'lres'      : True  => partial file
                                              False => not partial file
                                'lstan'     : True  => standard file
                                              False => not standard file
                                'lptn'      : True  => partition block present
                                              False => partition block not present


      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Arrays in fulldata were too short by one element.
           1.3     Martin O'Mullane
                     - Ensure that boolean entries in fulldata are cast
                       to bool after return from call to fortran code.

      VERSION    :
            1.1    21-10-2013
            1.2    25-10-2019
            1.3    15-11-2019

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata11 import xxdata_11_calcpy

    if file == None:
        raise Exception('A valid adf11 file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
        raise Exception('A valid adf11 file is required')

    # type of adf11 file

    iclass    = -1
    class_set = ['acd', 'scd', 'ccd', 'prb', 'prc', 'qcd', 'xcd', 
                 'plt', 'pls', 'zcd', 'ycd', 'ecd']

    if adf11type == 'xxx':

        # see if type can be detected from 1st line of file

        with open(file, 'r') as fl:
            first_line = fl.readline()

        for tstr in class_set:
            if first_line.lower().find(tstr) != -1:
                iclass = class_set.index(tstr) + 1

        if iclass == -1:
            raise Exception('cannot determine adf11 type from file - supply a valid adf11 type')

    else:

        for tstr in class_set:
            if adf11type.lower().find(tstr) != -1:
                iclass = class_set.index(tstr) + 1

        if iclass == -1:
            raise Exception('cannot determine adf11 type from file - supply a valid adf11 type')


    # Dimensions

    isdimd = 200
    itdimd = 51
    iddimd = 40
    ndptnl = 4
    ndptn  = 128
    ndptnc = 256
    ndcnct = 100
    iunit  = 67

    # Define variables

    iz0     = 0
    is1min  = 0
    is1max  = 0
    iblmx   = 0
    ismax   = 0
    itmax   = 0
    idmax   = 0
    nptnl   = 0
    ncnct   = 0
    dnr_ams = 0.0

    nptn   = asarray(zeros(ndptnl), int)
    nptnc  = asarray(zeros((ndptnl, ndptn)), int)
    iptnla = asarray(zeros(ndptnl), int)
    iptna  = asarray(zeros((ndptnl, ndptn)), int)
    iptnca = asarray(zeros((ndptnl, ndptn, ndptnc)), int)
    icnctv = asarray(zeros(ndcnct), int)
    isppr  = asarray(zeros(isdimd), int)
    ispbr  = asarray(zeros(isdimd), int)
    isstgr = asarray(zeros(isdimd), int)

    ddens  = asarray(zeros(iddimd), float)
    dtev   = asarray(zeros(itdimd), float)
    drcof  = asarray(zeros((isdimd, itdimd, iddimd)), float)

    lres   = False
    lstan  = False
    lptn   = False

    dnr_ele = '            '

    (iz0,is1min,is1max,nptnl,nptn,nptnc,
     iptnla,iptna,iptnca,ncnct,icnctv,iblmx,ismax,
     dnr_ele,dnr_ams,isppr,ispbr,isstgr,idmax,itmax,
     ddens,dtev,drcof,lres,lstan,lptn) = xxdata_11_calcpy(iunit,file,iclass,isdimd,iddimd,itdimd,ndptnl,ndptn,ndptnc,ndcnct)

    fulldata = { 'iz0'      :    iz0,
                 'class'    :    class_set[iclass-1],
                 'iclass'   :    iclass,
                 'is1min'   :    is1min,
                 'is1max'   :    is1max,
                 'nptnl'    :    nptnl,
                 'nptn'     :    nptn[0:nptnl],
                 'nptnc'    :    nptnc[0:nptnl, 0:nptn.max()],
                 'iptnla'   :    iptnla[0:nptnl],
                 'iptna'    :    iptna[0:nptnl, 0:nptn.max()],
                 'iptnca'   :    iptnca[0:nptnl, 0:nptn.max(), 0:nptnc.max()],
                 'ncnct'    :    ncnct,
                 'icnctv'   :    icnctv[0:ncnct],
                 'iblmx'    :    iblmx,
                 'ismax'    :    ismax,
                 'dnr_elem' :    dnr_ele,
                 'dnr_ams'  :    dnr_ams,
                 'isppr'    :    isppr[0:iblmx],
                 'ispbr'    :    ispbr[0:iblmx],
                 'isstgr'   :    isstgr[0:iblmx],
                 'idmax'    :    idmax,
                 'itmax'    :    itmax,
                 'ddens'    :    ddens[0:idmax],
                 'dtev'     :    dtev[0:itmax],
                 'drcof'    :    drcof[0:iblmx, 0:itmax, 0:idmax],
                 'lres'     :    bool(lres),
                 'lstan'    :    bool(lstan),
                 'lptn'     :    bool(lptn),
                 'file'     :    file  }

    return fulldata
