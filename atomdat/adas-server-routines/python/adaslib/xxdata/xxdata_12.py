def xxdata_12(file=None):
    """
      PURPOSE    :  Reads adf12 (QEF) files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_12(file='/home/adas/adas/adf12/qef93#h/qef93#h_c6.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf21 file

      RETURNS       fulldata     dict        QEF data (ph cm3 s-1)

                    fulldata = { 'file'     :  adf12 filename
                                 'nbsel'    :  number of transitions in file
                                 'csymb'    : element symbol
                                 'czion'    : emitting ion charge
                                 'cwavel'   : wavelength (A)
                                 'cdonor'   : donor neutral atom
                                 'crecvr'   : receiver nucleus
                                 'ctrans'   : transition
                                 'cfile'    : specific ion file source
                                 'ctype'    : type of emissivity
                                 'cindm'    : emissivity index
                                 'qefref'   : reference value of rate coefficient
                                 'enref'    :     "       "   "  energy
                                 'teref'    :     "       "   "  temperature
                                 'deref'    :     "       "   "  density
                                 'zeref'    :     "       "   "  effective z
                                 'bmref'    :     "       "   "  magnetic field
                                 'nenera'   : number of energies
                                 'ntempa'   :     "     temperatures
                                 'ndensa'   :     "     densities
                                 'nzeffa'   :     "     effective z's
                                 'nbmaga'   :     "     magnetic field values
                                 'enera'    :
                                 'tempa'    : energies
                                 'densa'    : temperatures
                                 'zeffa'    : densities
                                 'bmaga'    : effective z
                                 'qenera'   : rate coefficients for energy value
                                 'qtempa'   : rate coefficients for temperatures
                                 'qdensa'   : rate coefficients for densities
                                 'qzeffa'   : rate coefficients for effective z
                                 'qbmaga'   : rate coefficients for magnetic fields

                   Arrays are dimensioned with the number of blocks in the
                   adf12 file. 2-D arrays have a leading dimension of 12 for
                   temperature, effective Z and Bmag quantities, 24 for beam
                   energy and density.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    16-12-2012

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata12 import xxdata_12_calcpy

    # Examine inputs

    if not file:
        raise Exception('A valid adf12 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('adf12 file is invalid')

    # Dimensions

    ndtem  = 12
    ndden  = 24
    ndein  = 24
    ndzef  = 12
    ndmag  = 12
    nstore = 150
    iunit  = 67

    # Define variables

    nbsel   = -1
    csymb   = asarray(['--'] * nstore)
    czion   = asarray(['--'] * nstore)
    cwavel  = asarray(['--------'] * nstore)
    cdonor  = asarray(['------'] * nstore)
    crecvr  = asarray(['-----'] * nstore)
    ctrans  = asarray(['-------'] * nstore)
    cfile   = asarray(['----------'] * nstore)
    ctype   = asarray(['--'] * nstore)
    cindm   = asarray(['---'] * nstore)
    qefref  = asarray(zeros(nstore), float)
    enref   = asarray(zeros(nstore), float)
    teref   = asarray(zeros(nstore), float)
    deref   = asarray(zeros(nstore), float)
    zeref   = asarray(zeros(nstore), float)
    bmref   = asarray(zeros(nstore), float)
    nenera  = asarray(zeros(nstore), int)
    ntempa  = asarray(zeros(nstore), int)
    ndensa  = asarray(zeros(nstore), int)
    nzeffa  = asarray(zeros(nstore), int)
    nbmaga  = asarray(zeros(nstore), int)
    enera   = asarray(zeros((ndein,nstore)), float)
    tempa   = asarray(zeros((ndtem,nstore)), float)
    densa   = asarray(zeros((ndden,nstore)), float)
    zeffa   = asarray(zeros((ndzef,nstore)), float)
    bmaga   = asarray(zeros((ndmag,nstore)), float)
    qenera  = asarray(zeros((ndein,nstore)), float)
    qtempa  = asarray(zeros((ndtem,nstore)), float)
    qdensa  = asarray(zeros((ndden,nstore)), float)
    qzeffa  = asarray(zeros((ndzef,nstore)), float)
    qbmaga  = asarray(zeros((ndmag,nstore)), float)


    (nbsel,csymb, czion,cwavel,cdonor,crecvr,ctrans,cfile,ctype,cindm,
     qefref,enref,teref,deref,zeref,bmref,
     nenera,ntempa,ndensa,nzeffa,nbmaga,
     enera,tempa,densa,zeffa,bmaga,qenera,
     qtempa,qdensa,qzeffa,qbmaga)  = xxdata_12_calcpy(iunit,file,ndtem,ndden,ndein,ndzef,ndmag,nstore)

    c1 = [row.tostring() for row in csymb[0:nbsel-1]]
    c2 = [row.tostring() for row in czion[0:nbsel-1]]
    c3 = [row.tostring() for row in cwavel[0:nbsel-1]]
    c4 = [row.tostring() for row in cdonor[0:nbsel-1]]
    c5 = [row.tostring() for row in crecvr[0:nbsel-1]]
    c6 = [row.tostring() for row in ctrans[0:nbsel-1]]
    c7 = [row.tostring() for row in cfile[0:nbsel-1]]
    c8 = [row.tostring() for row in ctype[0:nbsel-1]]
    c9 = [row.tostring() for row in cindm[0:nbsel-1]]

    fulldata = { 'file'     :  file,
                 'nbsel'    :  nbsel ,
                 'csymb'    :  c1,
                 'czion'    :  c2,
                 'cwavel'   :  c3,
                 'cdonor'   :  c4,
                 'crecvr'   :  c5,
                 'ctrans'   :  c6,
                 'cfile'    :  c7,
                 'ctype'    :  c8,
                 'cindm'    :  c9,
                 'qefref'   :  qefref[0:nbsel-1],
                 'enref'    :  enref[0:nbsel-1],
                 'teref'    :  teref[0:nbsel-1],
                 'deref'    :  deref[0:nbsel-1],
                 'zeref'    :  zeref[0:nbsel-1],
                 'bmref'    :  bmref[0:nbsel-1],
                 'nenera'   :  nenera[0:nbsel-1],
                 'ntempa'   :  ntempa[0:nbsel-1],
                 'ndensa'   :  ndensa[0:nbsel-1],
                 'nzeffa'   :  nzeffa[0:nbsel-1],
                 'nbmaga'   :  nbmaga[0:nbsel-1],
                 'enera'    :  enera[:,0:nbsel-1],
                 'tempa'    :  tempa[:,0:nbsel-1],
                 'densa'    :  densa[:,0:nbsel-1],
                 'zeffa'    :  zeffa[:,0:nbsel-1],
                 'bmaga'    :  bmaga[:,0:nbsel-1],
                 'qenera'   :  qenera[:,0:nbsel-1],
                 'qtempa'   :  qtempa[:,0:nbsel-1],
                 'qdensa'   :  qdensa[:,0:nbsel-1],
                 'qzeffa'   :  qzeffa[:,0:nbsel-1],
                 'qbmaga'   :  qbmaga[:,0:nbsel-1]  }

    return fulldata
