def xxdata_40(file=None):
    """
      PURPOSE    :  Reads adf40 (fPEC) files from the python command line
                    and return all its contents in a dictionary.

      fulldata = xxdata_40(file='/home/adas/adas/adf40/fpec40#w/fpec40#w_ic#w45.dat')

                    NAME         TYPE        DETAILS
      REQUIRED   :  file         str         full name of ADAS adf40 file

      RETURNS       fulldata     dict        fPEC data (ph cm3 s-1)

                    fulldata = { 'esym'        : element (string)
                                 'iz0'         : atomic number
                                 'iz'          : ion charge
                                 'iz1'         : recombined ion charge
                                 'nblock'      : number of blocks, or fpecs
                                 'npix'        : number of pixels for each block (array)
                                 'wave_min'    : minimum wavelength (A) for each block (array)
                                 'wave_max'    : maximum wavelength (A) for each block (array)
                                 'nte'         : number of temperatures for each block (array)
                                 'te'          : electron temperatures (eV)
                                                   array(max no. te, no. blocks)
                                 'ndens'       : number of densities for each block (array)
                                 'dens'        : electron density (cm**-3)
                                                   array(max no. dens, no. blocks)
                                 'fpec'        : feature photon emissivities (ph cm**3 s**-1)
                                                   array(pixel, te, dens, blocks)
                                 'type'        : excitation type
                                 'comments'    :
                                 'filename'    : name of adf40 file

                   As different dimensions per block are possible, the multi-dimension arrays
                   use the max value for pixels, number of temperatures and number of densities.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Allow up to 132 characters for filename.
                     - Correct the comment array.
           1.3     Martin O'Mullane
                     - nte and ndens in fulldata were short by 1.

      VERSION    :
            1.1    30-07-2018
            1.2    01-08-2018
            1.3    10-08-2018

    """

    # External routines required

    import os
    from numpy import asarray, zeros
    from .xxdata40 import xxdata_40_calc

    # Examine inputs

    if not file:
        raise Exception('A valid adf40 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('adf40 file is invalid')

    dsnin = file.ljust(132)

    # Dimensions

    nstore_  = 10
    ndpix_   = 1024
    ntdim_   = 40
    nddim_   = 50
    ndptnl_  = 4
    ndptn_   = 128
    ndptnc_  = 256
    ndcnct_  = 100
    ndstack_ = 40
    ndcmt_   = 2000

    # Define variables

    str         = '--------------------------------------------------------------------------------'
    iz0         = -1
    iz          = 0
    iz1         = 0
    esym        = '--'
    nptnl       = 0
    nptn        = asarray(zeros(ndptnl_), int)
    nptnc       = asarray(zeros((ndptnl_,ndptn_), int))
    iptnla      = asarray(zeros(ndptnl_), int)
    iptna       = asarray(zeros((ndptnl_,ndptn_), int))
    iptnca      = asarray(zeros((ndptnl_,ndptn_,ndptnc_), int))
    ncnct       = 0
    icnctv      = asarray(zeros(ndcnct_), int)
    ncptn_stack = 0
    cptn_stack  = asarray([str] * ndstack_)
    lres        = False
    lptn        = False
    lcmt        = False
    lsup        = False
    nbsel       = 0
    isela       = asarray(zeros(nstore_), int)
    npixa       = asarray(zeros(nstore_), int)
    cfile       = asarray(['--------'] * nstore_)
    ctype       = asarray(['--------'] * nstore_)
    cindm       = asarray(['--------'] * nstore_)
    ispbr       = asarray(zeros(nstore_), int)
    isppr       = asarray(zeros(nstore_), int)
    isstgr      = asarray(zeros(nstore_), int)
    ilzr        = asarray(zeros(nstore_), int)
    ihzr        = asarray(zeros(nstore_), int)
    wvmina      = asarray(zeros(nstore_), float)
    wvmaxa      = asarray(zeros(nstore_), float)
    ita         = asarray(zeros(nstore_), int)
    ida         = asarray(zeros(nstore_), int)
    teta        = asarray(zeros((ntdim_,nstore_), float))
    teda        = asarray(zeros((nddim_,nstore_), float))
    fpec        = asarray(zeros((ndpix_,ntdim_,nddim_,nstore_), float))
    fpec_max    = asarray(zeros(nstore_), float)
    ncmt_stack  = 0
    cmt_stack   = asarray([str] * ndcmt_)

    (iz0, iz, iz1, esym, nptnl, nptn, nptnc,
     iptnla, iptna, iptnca, ncnct, icnctv,
     ncptn_stack, cptn_stack, lres, lptn, lcmt, lsup,
     nbsel, isela, npixa, cfile, ctype, cindm,
     ispbr, isppr, isstgr, ilzr, ihzr, wvmina, wvmaxa,
     ita, ida, teta, teda, fpec, fpec_max,
     ncmt_stack, cmt_stack) = xxdata_40_calc(dsnin, nstore_, ndpix_, ntdim_, nddim_, ndptnl_ , ndptn_, ndptnc_, ndcnct_, ndstack_, ndcmt_)

    c1 = asarray([row.tostring().strip().decode() for row in ctype[0:nbsel]])
    c2 = asarray([row.tostring().strip().decode() for row in cmt_stack[0:ncmt_stack]])

    fulldata = { 'esym'     :  esym.strip().decode(),
                 'iz0'      :  iz0,
                 'iz'       :  iz,
                 'iz1'      :  iz1,
                 'nblock'   :  nbsel,
                 'npix'     :  npixa[0:nbsel],
                 'wave_min' :  wvmina[0:nbsel],
                 'wave_max' :  wvmaxa[0:nbsel],
                 'nte'      :  ita[0:nbsel],
                 'te'       :  teta[0:ita.max(), 0:nbsel],
                 'ndens'    :  ida[0:nbsel],
                 'dens'     :  teda[0:ida.max(), 0:nbsel],
                 'fpec'     :  fpec[0:npixa.max(),0:ita.max(),0:ida.max(),0:nbsel],
                 'type'     :  c1,
                 'comments' :  c2,
                 'filename' :  file.strip() }

    return fulldata
