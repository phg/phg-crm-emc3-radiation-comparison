def read_adf12(file=None, block=1, ein=[0.0], tion=[0.0], dion=[0.0],
               zeff=[0.0], bmag=[0.0]):
    """
      PURPOSE    :  Reads adf12 (QEF) files from the python command line.

      coeff, info = read_adf12(file=file, block=block, ein=ein, tion=tion, dion=dion, zeff=zeff, bmag=bmag)

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      full name of ADAS adf12 file
                    block      int      selected block
                    ein()      float    energies requested
                    tion()     float    ion temperatures requested (eV)
                    dion()     float    ion densities requested (cm-3)
                    zeff()     float    Z-effective requested
                    bmag()     float    B field requested (T)

                    Either all have the same length or just one is allowed
                    to be a vector.

      RETURNS    :  coeff()    float    emissivity coefficient (cm3 s-1)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    20-08-2012

    """

    # External routines required

    import os
    from adas3xx import cxsqef
    from numpy import asarray, zeros

    # Examine inputs

    if file == None:
       raise Exception('A valid adf12 file is required')
       if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
          raise Exception('A valid adf12 file is required')

    if block == 0:
       raise Exception('A valid block in adf12 is required')

    ein_int  = asarray(ein, float)
    tion_int = asarray(tion, float)
    dion_int = asarray(dion, float)
    zeff_int = asarray(zeff, float)
    bmag_int = asarray(bmag, float)

    if ein_int.size == 1: ein_int = asarray([ein], float)
    if tion_int.size == 1: tion_int = asarray([tion], float)
    if dion_int.size == 1: dion_int = asarray([dion], float)
    if zeff_int.size == 1: zeff_int = asarray([zeff], float)
    if bmag_int.size == 1: bmag_int = asarray([bmag], float)

    if ein_int.sum() == 0.0 : raise Exception('ein must not be 0.0')
    if tion_int.sum() == 0.0 : raise Exception('tion must not be 0.0')
    if dion_int.sum() == 0.0 : raise Exception('dion must not be 0.0')
    if zeff_int.sum() == 0.0 : raise Exception('zeff must not be 0.0')
    if bmag_int.sum() == 0.0 : raise Exception('bmag must not be 0.0')

    n_e = ein_int.size
    n_t = tion_int.size
    n_d = dion_int.size
    n_z = zeff_int.size
    n_b = bmag_int.size

    # Either all user parameters are the same size or just one varies

    mess = 'Only one of EIN, TION, DION, ZEFF or BMAG can have more than 1 element or they must all have the same length'

    if (n_e + n_t + n_d + n_z + n_b) == 5 * n_e :

        num = n_e
        p_e = ein_int
        p_t = tion_int
        p_d = dion_int
        p_z = zeff_int
        p_b = bmag_int

    else:

        if n_e > 1:
            num = n_e
            if (n_t+n_d+n_z+n_b) > 4 : raise Exception(mess)
            p_e = ein_int
            p_t = asarray(zeros(num), float) + tion_int
            p_d = asarray(zeros(num), float) + dion_int
            p_z = asarray(zeros(num), float) + zeff_int
            p_b = asarray(zeros(num), float) + bmag_int

        if n_t > 1:
           num = n_t
           if (n_e+n_d+n_z+n_b) > 4 : raise Exception(mess)
           p_e = asarray(zeros(num), float) + ein_int
           p_t = tion_int
           p_d = asarray(zeros(num), float) + dion_int
           p_z = asarray(zeros(num), float) + zeff_int
           p_b = asarray(zeros(num), float) + bmag_int

        if n_d > 1:
           num = n_d
           if (n_e+n_t+n_z+n_b) > 4 : raise Exception(mess)
           p_e = asarray(zeros(num), float) + ein_int
           p_t = asarray(zeros(num), float) + tion_int
           p_d = dion_int
           p_z = asarray(zeros(num), float) + zeff_int
           p_b = asarray(zeros(num), float) + bmag_int

        if n_z > 1:
           num = n_z
           if (n_e+n_t+n_d+n_b) > 4 : raise Exception(mess)
           p_e = asarray(zeros(num), float) + ein_int
           p_t = asarray(zeros(num), float) + tion_int
           p_d = asarray(zeros(num), float) + dion_int
           p_z = zeff_int
           p_b = asarray(zeros(num), float) + bmag_int

        if n_b > 1:
           num = n_b
           if (n_e+n_t+n_d+n_z) > 4 : raise Exception(mess)
           p_e = asarray(zeros(num), float) + ein_int
           p_t = asarray(zeros(num), float) + tion_int
           p_d = asarray(zeros(num), float) + dion_int
           p_z = asarray(zeros(num), float) + zeff_int
           p_b = asarray(zeros(num), float) + bmag_int


    # set ttar to 0.0 to bypass Maxwellian averaging branch of cxsqef

    ttar = asarray(zeros(num), float)

    ener, qener, qeff = cxsqef(file=file, ibsel=block, epro=p_e, ttar=ttar,
                               ti=p_t, densi=p_d, zeff=p_z, bmag=p_b)

    return qeff
