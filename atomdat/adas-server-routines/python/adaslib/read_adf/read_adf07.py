def read_adf07(file=None, block=1, te=[0.0], unit_te='ev'):
    """
      PURPOSE    :  Reads adf07 ionization rate (SZD) files from the 
                    python command line.

      coeff, info = read_adf07(file=file, block=block, te=te)

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      full name of ADAS adf07 file
                    block      int      selected block
                    te         float()  temperatures requested (eV)

      OPTIONAL   :  unit_te    str      eV or Kelvin: default is eV

      RETURNS    :  coeff      float()  1D or 2D array of SZDs (cm3 s-1)
                    info       dict     'i_ion'  = initial ion <el>+z
                                        'f_ion'  = final ion <el>+z
                                        'i_met'  = initial metastable
                                        'f_met'  = final metastable
                                        'ip'     = ionisation potential

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    10-08-2018

    """

    # External routines required

    import os
    from .readadf07 import readadf07
    from adaslib import numlines, adas_vector
    from numpy import asarray, zeros, arange, atleast_1d

    # Examine inputs

    if file == None:
       raise Exception('A valid adf07 file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
       raise Exception('A valid adf07 file is required')

    dsnin = file.ljust(132)

    te_int   = atleast_1d(asarray(te, float))
    len_te   = te_int.size

    if unit_te.lower() == 'kelvin':
       te_int = te_int / 11605.0


    coeff = zeros(len_te, float)
    ci_ion = 'ABCDE'
    cf_ion = 'abcde'
    i_met  = 0
    f_met  = 0
    bwno   = 0.0

    # Get the data in chunks of 100 elements because of limit in e2spln

    MAXVAL = 100
    
    n_call = numlines(len_te, MAXVAL)

    for j in range(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, len_te)

       t_ca = te_int[ist:ifn]
       
       c_ca, i_met, f_met, bwno, ci_ion, cf_ion = readadf07(block, t_ca, ci_ion, cf_ion, dsnin)
       
       coeff[ist:ifn] = c_ca
    
    info = { 'i_ion' : ci_ion.strip().decode(), 
             'f_ion' : cf_ion.strip().decode(), 
             'i_met' : i_met, 
             'f_met' : f_met, 
             'ip'    : bwno }   
    
    # Return results
    
    return coeff, info
