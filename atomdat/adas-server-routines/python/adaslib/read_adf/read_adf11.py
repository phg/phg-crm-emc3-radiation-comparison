def read_adf11(file=None, adf11type='xxx', is1=0, index_1=-1, index_2=-1,
               te=[0.0], dens=[0.0], all=False, skipzero=False, unit_te='ev'):
    """
      PURPOSE    :  Reads adf11 files from the python command line.

      coeff = read_adf11(file=file, block=block, te=te, all=False, unit_te=['ev'])

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      full name of ADAS adf11 file
                    adf11type  str      type of adf11 file (see notes below for list)
                                        if not supplied the code attempts to detect
                                        it from the first line of the file.
                    is1        int      stage (or superstage) requested
                    index_1    int      partition identifier (see notes below)
                    index_2    int      partition identifier (see notes below)
                    te         float()  temperatures requested
                    dens       float()  densities requested

      OPTIONAL   :  all        bool     return 2D coeff(te, dens): default is false
                    unit_te    str      ev or kelvin: default is ev
                    skipzero   bool     do not use effective zero in interpolating
                                        to avoid ringing: default is False

      RETURNS    :  coeff      float()  1D or 2D array of adf11 data (cm3 s-1 or W cm3)

      NOTE       : To specify the required partition the input names are
                   those in the datasets and both the old or new variants
                   can be used.

                           adf11     old names              new names
                           type
                                    (index_1 index_2)      (index_1 index_2
                           ----          ----     ----         ----     ----
                           acd        iprt     igrd         ispp     ispb
                           scd        iprt     igrd         ispp     ispb
                           ccd        iprt     igrd         ispp     ispb
                           prb        iprt                  ispp
                           prc        iprt                  ispp
                           qcd        igrd     jgrd         ispb     jspb
                           xcd        iprt     jprt         ispp     jspp
                           plt        igrd                  ispb
                           pls        igrd                  ispb
                           zcd        igrd                  ispb
                           ycd        igrd                  ispb
                           ecd        igrd                  ispb

                   In partitioned nomenclature: s=superstage; p=partition;
                   b=base (current superstage), p=parent (next up super-
                   stage), c=child (next down superstage). Thus arrays
                   'iprtr' and 'igrd' in old notation are now substituted
                   by 'isppr' and 'ispbr' respectively internally.

                   Note that index_1 (and index_2) only need to be specified when
                   the adf11 dataset is resolved by metastable, also referred
                   to as partial adf11 data.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Add skipzero option

      VERSION    :
            1.1    31-10-2013
            1.2    06-10-2017

    """

    # External routines required

    import os
    from .readadf11 import readadf11
    from adaslib import numlines, xxdata_11
    from numpy import asarray, zeros

    # Examine and check inputs

    # file

    if file == None:
        raise Exception('A valid adf11 file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
        raise Exception('A valid adf11 file is required')

    # type of adf11 file

    iclass    = -1
    class_set = ['acd', 'scd', 'ccd', 'prb', 'prc', 'qcd', 'xcd', 'plt', 'pls', 'zcd', 'ycd', 'ecd']

    if adf11type == 'xxx':

        # see if type can be detected from 1st line of file

        with open(file, 'r') as fl:
            first_line = fl.readline()

        for tstr in class_set:
            if first_line.lower().find(tstr) != -1:
                iclass = class_set.index(tstr) + 1

        if iclass == -1:
            raise Exception('cannot determine adf11 type from file - supply a valid adf11 type')

    else:

        for tstr in class_set:
            if adf11type.lower().find(tstr) != -1:
                iclass = class_set.index(tstr) + 1

        if iclass == -1:
            raise Exception('cannot determine adf11 type from file - supply a valid adf11 type')


    # Check stage and partition selection indices

    if is1 == 0:
        raise Exception('A valid stage/superstage in adf11 is required')

    fulldata = xxdata_11(file, adf11type=class_set[iclass-1])
    lres     = fulldata['lres']

    if lres == True:

        if iclass in [1, 2, 3, 6, 7]:
            if index_1 == -1 or index_2 == -1:
                raise Exception('index_1 and index_2 must be given')
            else:
                ind1 = index_1
                ind2 = index_2
        else:
            ind2 = 1
            if index_1 == -1:
                raise Exception('index_1 must be given')
            else:
                ind1 = index_1
    else:
        ind1   = 1
        ind2   = 1

    # user supplied conditions

    te_int   = asarray(te, float)
    dens_int = asarray(dens, float)
    len_te   = te_int.size
    len_dens = dens_int.size

    if len_te == 1:
        te_int = asarray([te], float)

    if len_dens == 1:
        dens_int = asarray([dens], float)

    if all == True:

        itval    = len_te * len_dens
        te_int   = asarray([te_int] * len_dens).ravel()
        dens_int = asarray([dens_int] * len_te).transpose().ravel()

    else:

        itval    = min(len_dens, len_te)
        te_int   = te_int[0:itval]
        dens_int = dens_int[0:itval]

    if unit_te.lower() == 'kelvin':
        te_int = te_int / 11605.0


    if skipzero:
        igzero = 1
    else:
        igzero = 0

    # Get the data in chunks of 2000 elements because of limit in e5spln

    coeff  = zeros(itval)

    MAXVAL = 2000
    n_call = numlines(itval, MAXVAL)

    for j in range(n_call):

        ist = j*MAXVAL
        ifn = min((j+1)*MAXVAL, itval)

        t_ca = te_int[ist:ifn]
        d_ca = dens_int[ist:ifn]
        c_ca = readadf11(is1, iclass, ind1, ind2, igzero, t_ca, d_ca, file)

        coeff[ist:ifn] = c_ca

    if all == True:
        coeff = coeff.reshape((len_dens, len_te)).transpose()

    # finish by returning the interpolated data

    return coeff
