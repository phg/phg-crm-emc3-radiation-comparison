def read_adf02(file=None, block=1, energy=[0.0]):
    """
      PURPOSE    :  Interpolate/extrapolate ion driven cross section data
                    from adf02 files.

      coeff, info = read_adf02(file=file, block=block, energy=energy)

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      full name of ADAS adf02 file
                    block      int      selected block
                    energy     float()  energies requested (eV/amu)

      RETURNS    :  coeff      float()  vector of cross section (cm2)
                    info       dict     'primary'    = primary species
                                        'secondary'  = secondary species
                                        'type'       = type of process
                                        'mass_p'     = primary species atomic mass number
                                        'mass_s'     = secondary species atomic mass number
                                        'alpha'      = high energy extrapolation parmameter
                                        'ethresh     = energy threshold (ev)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    10-02-2019

    """

    # External routines required

    import os
    from .readadf02 import readadf02
    from adaslib import numlines
    from numpy import asarray, zeros, atleast_1d

    # Examine inputs

    if file == None:
       raise Exception('A valid adf02 file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
       raise Exception('A valid adf02 file is required')

    dsnin = file.ljust(132)

    eng_int  = atleast_1d(asarray(energy, float))
    len_eng  = eng_int.size

    coeff = zeros(len_eng, float)
    sdonor = 'ABCDE'
    srecvr = 'abcde'
    stype  = 'FGH'
    massd  = 0.0
    massr  = 0.0
    alpha  = 0.0
    eth    = 0.0

    # Get the data in chunks of 24 elements because of limit in e2spln

    MAXVAL = 24

    n_call = numlines(len_eng, MAXVAL)

    for j in range(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, len_eng)

       e_ca = eng_int[ist:ifn]

       s_ca, alpha, eth, massd, massr, sdonor, srecvr, stype = readadf02(block, e_ca, dsnin, sdonor, srecvr, stype)

       coeff[ist:ifn] = s_ca

    info = { 'primary'   : sdonor.strip().decode(),
             'secondary' : srecvr.strip().decode(),
             'type'      : stype.strip().decode(),
             'mass_p'    : massd,
             'mass_s'    : massr,
             'alpha'     : alpha,
             'ethresh'   : eth }

    # Return results

    return coeff, info
