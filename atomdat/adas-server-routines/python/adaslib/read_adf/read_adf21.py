def read_adf21(files=None, energy=[0.0], te=[0.0], dens=[0.0], fraction=1.0):
    """
      PURPOSE    :  Reads adf21 (BMS) files from the python command line.
                    Note that adf22 (BME and BMP) have the same format.

      coeff = read_adf21(files=files, energy=energy, te=te, dens=dens, fraction=fraction)

                    NAME         TYPE        DETAILS
      REQUIRED   :  files        list str()  full name of ADAS adf21 files
                    fraction     float()     target ion fractions; same as number of
                                             adf21 files
                                                1st Dimension: requested parameters
                                                2nd Dimension: numfiles
                                             If only one dimension is present
                                             read_adf21 assumes that the target
                                             impurity fraction is the same for all
                                             requested parameters.
                    te           float()     temperatures requested (eV)
                    dens         float()     electron densities requested (cm-3)
                    energy       float()     beam energies requested (eV/amu)

      RETURNS       coeff        float()     BMS/BME/BMP data (cm3 s-1 , ph cm3 s-1 , na)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - 2D fraction inputs were not processed correctly.
           1.3     Martin O'Mullane
                     - Improve file checking when just one file in supplied.

      VERSION    :
            1.1    20-11-2012
            1.2    09-01-2016
            1.3    24-08-2018

    """

    # External routines required

    import os
    from .readadf21 import readadf21
    from adaslib import numlines
    from numpy import asarray, zeros

    # Examine inputs

    if not files:
        raise Exception('Valid adf21 file(s) required')
    else:
        if isinstance(files, list):
            nfiles   = len(files)
        else:
            nfiles = 1
            files = [files]

        str_vars = zeros((132, nfiles), int) + 32
        
        k = 0
        for f in files:
            if (os.path.isfile(f) and os.access(f, os.R_OK)) == False:
                raise Exception('adf21 file is invalid')
            else:
                for i in range(len(f)): str_vars[i,k] = ord(f[i])
            k = k + 1


    energy_int   = asarray(energy, float)
    te_int       = asarray(te, float)
    dens_int     = asarray(dens, float)
    fraction_int = asarray(fraction, float)

    len_energy = energy_int.size
    len_te     = te_int.size
    len_dens   = dens_int.size

    if len_energy == 1:
        energy_int = asarray([energy], float)

    if len_te == 1:
        te_int = asarray([te], float)

    if len_dens == 1:
        dens_int = asarray([dens], float)

    itval = min(len_te,len_dens,len_energy)

    # fraction can either be a numeric vector or a 2D array to allow varying
    # ion fractions along the request vector. If it is a vector expand to a
    # 2D array keeping the fraction constant.

    if len(fraction_int.shape)==1:
        fraction_int = asarray([fraction] * itval)

    if nfiles != fraction_int.shape[1] :
        raise Exception('Fractions must match number of files')

    len_frac = fraction_int.shape[0]
    
    # Return data for the smallest dimension
    
    itval = min(itval, len_frac)
    
    # Get the data in chunks of 1024 elements, the limit of c4spln

    coeff = asarray(zeros(itval, float))

    MAXVAL = 1024
    n_call = numlines(itval, MAXVAL)

    for j in range(n_call):

        ist = j*MAXVAL
        ifn = min((j+1)*MAXVAL, itval)

        itval_ca = ifn-ist
        t_ca     = te_int[ist:ifn]
        d_ca     = dens_int[ist:ifn]
        e_ca     = energy_int[ist:ifn]
        frac_ca  = fraction_int[ist:ifn, :]

        c_ca = readadf21(t_ca, d_ca, e_ca, str_vars, frac_ca)

        coeff[ist:ifn] = c_ca

    return coeff
