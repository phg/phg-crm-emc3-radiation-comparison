def read_adf13(file=None, block=1, te=[0.0], dens=[0.0], all=False, unit_te='ev'):
    """
      PURPOSE    :  Reads adf13 ionization per photon (SXB) files from 
                    the python command line.

      coeff, info = read_adf13(file=file, block=block, te=te, all=False, unit_te=['ev'])

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      full name of ADAS adf13 file
                    block      int      selected block
                    te         float()  temperatures requested
                    dens       float()  densities requested
      OPTIONAL   :  all        bool     return 2D coeff(te, dens): default is False
                    unit_te    str      eV or kelvin: default is ev

      RETURNS    :  coeff      float()  1D or 2D array of SXBs (cm3 s-1)
                    info       dict    {'wavelength' : ,  in Angstrom
                                        'iz0'        : ,  nuclear charge
                                        'iss'        : }  ion charge

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    21-08-2018

    """

    # External routines required

    import os
    from .readadf13 import readadf13
    from adaslib import numlines
    from numpy import asarray, zeros, atleast_1d
    
    # Examine inputs

    if file == None:
       raise Exception('A valid adf13 file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
       raise Exception('A valid adf13 file is required')
    
    dsnin = file.ljust(132)
    
    te_int   = atleast_1d(asarray(te, float))
    dens_int = atleast_1d(asarray(dens, float))
    len_te   = te_int.size
    len_dens = dens_int.size

    if all == True:

       itval = len_te * len_dens

       te_int   = asarray([te_int] * len_dens).ravel()
       dens_int = asarray([dens_int] * len_te).transpose().ravel()

    else:

       itval    = min(len_dens, len_te)
       te_int   = te_int[0:itval]
       dens_int = dens_int[0:itval]

    if unit_te.lower() == 'kelvin':
       te_int = te_int / 11605.0

    # Get the data in chunks of 99 elements because of limit in e5spln

    coeff = zeros(itval)

    MAXVAL = 99
    n_call = numlines(itval, MAXVAL)

    for j in range(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, itval)

       t_ca = te_int[ist:ifn]
       d_ca = dens_int[ist:ifn]
       c_ca, iz0_int, iss_int, iss1_int, wave = readadf13(block, t_ca, d_ca, dsnin)

       coeff[ist:ifn] = c_ca

    if all == True:
       coeff = coeff.reshape((len_dens, len_te)).transpose()

    info = {'wavelength' : wave,
            'iz0'        : iz0_int,
            'iss'        : iss_int}

    return coeff, info
