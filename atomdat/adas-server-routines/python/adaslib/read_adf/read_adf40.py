def read_adf40(file=None, block=1, te=[0.0], dens=[0.0], all=False, unit_te='ev'):
    """
      PURPOSE    :  Reads adf40 (fPEC) files from the python command line.

      coeff, wave = read_adf40(file=file, block=block, te=te, all=False, unit_te=['ev'])

                    NAME       TYPE     DETAILS
      REQUIRED   :  file       str      full name of ADAS adf40 file
                    block      int      selected block
                    te         float()  temperatures requested
                    dens       float()  densities requested
      OPTIONAL   :  all        bool     return 2D coeff(te, dens): default is false
                    unit_te    str      eV or Kelvin: default is eV

      RETURNS    :  coeff      float()  1D or 2D array of fPECs (cm3 s-1)
                    wave       float()  1D vector of wavelengths (A)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    31-07-2018

    """

    # External routines required

    import os
    from .readadf40 import readadf40
    from adaslib import numlines, adas_vector
    from numpy import asarray, zeros, arange

    # Examine inputs

    if file == None:
       raise Exception('A valid adf40 file is required')
    if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
       raise Exception('A valid adf40 file is required')

    dsnin = file.ljust(132)

    te_int   = asarray(te, float)
    dens_int = asarray(dens, float)
    len_te   = te_int.size
    len_dens = dens_int.size

    if len_te == 1:
        te_int = asarray([te], float)

    if len_dens == 1:
        dens_int = asarray([dens], float)

    if all == True:

       itval = len_te * len_dens

       te_int   = asarray([te_int] * len_dens).ravel()
       dens_int = asarray([dens_int] * len_te).transpose().ravel()

    else:

       itval    = min(len_dens, len_te)
       te_int   = te_int[0:itval]
       dens_int = dens_int[0:itval]

    if unit_te.lower() == 'kelvin':
       te_int = te_int / 11605.0

    # Get the data in chunks of 99 elements because of limit in e3spln

    MAXVAL = 100
    
    nstore  = 10
    ndpix   = 1024
    ntdim   = 40
    nddim   = 50
    ndptnl  = 4
    ndptn   = 128
    ndptnc  = 256
    ndcnct  = 100
    ndstack = 40
    ndcmt   = 2000
    ntmax   = MAXVAL
    
    coeff = zeros((ndpix,itval), float)
    nvpix = 0
    wvmin = 0.0
    wvmax = 0.0

    n_call = numlines(itval, MAXVAL)

    for j in range(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, itval)

       t_ca = te_int[ist:ifn]
       d_ca = dens_int[ist:ifn]
       c_ca, nvpix, wvmin, wvmax = readadf40(nstore, ndpix, ntdim, nddim, ndptnl, ndptn, 
                                             ndptnc, ndcnct, ndstack, ndcmt, ntmax, 
                                             block, t_ca, d_ca, dsnin)
       coeff[:,ist:ifn] = c_ca

    # Trim coeff to number of pixels present in adf40
    
    coeff = coeff[0:nvpix,:]
    
    # Re-shape over Te/dens if needed
     
    if all == True:
       ctemp = zeros((nvpix, len_te, len_dens), float)
       k = 0
       for i in arange(len_te):
           for j in arange(len_dens):
               ctemp[:,i,j] = coeff[:,k]
               k = k + 1
       coeff = ctemp

    # Generate wavelength vector
    
    wave = adas_vector(low=wvmin, high=wvmax, num=nvpix, linear=True)
    
    # Return results
    
    return coeff, wave
