def run_adas208(adf04=None, adf18=None,
                te=[0.0], tion=[0.0], th=[0.0], unit_te='ev',
                dens=[0.0], dion=[0.0], denh=[0.0],  unit_dens='cm-3', zeff=0.0,
                meta=[1], nonorm=False,
                wmin=[0.01], wmax=[9000.0], amin=[1.0e2], 
                tr_upper=[0], tr_lower=[0],
                pass_dir='./', logfile=None,
                ion=False, rec=False, cx=False,
                gcr=False, pec=False, sxb=False):

    """

      PURPOSE    :  Runs the adas208 collisional-radiative population code as a python function

      pop = run_adas208(adf04='hlike_c5.dat', dens=dens, te=te, pec=True, ....)

                    NAME       TYPE     DETAILS
      REQUIRED   :  adf04      str      specific ion file
                    adf18      str      cross reference file for projection
                    te         float()  electron temperatures requested
                    tion       float()  ion temperatures requested
                    th         float()  hydrogen temperatures requested
                    unit_te    str      'ev', 'kelvin', 'red', default to eV
                    dens       float()  electron densities requested
                    dion       float()  ion densities requested
                    denh       float()  hydrogen densities requested
                    unit_dens  str      'cm-3', 'red', defaults to cm-3
                    zeff       float    for scaling ion collisions if dion set
                    meta       int()    metastable indices (start at 1), default is [1]
                    nonorm     boolean  do not normalise pop to ground for single
                                        metastable case, default is to normalize
                    wmin       float()  lower wavelength(A) for PEC/SXB, defaults to 0.01A
                    wmax       float()  upper wavelength(A) for PEC/SXB, defaults to 9000.0.
                    amin       float()  lower A value for PEC/SXB, defaults to 100.0 s-1).
                    tr_upper   int()    upper indices for transitions in PEC file.
                    tr_lower   int()    lower indices for transitions in PEC file.
                    pass_dir   str      passing file directory for gcr etc. default
                                        is the current directory ('./')
                    logfile    str      text log file, defaults to no output file
                    ion        boolean  include ionisation from excited level (S-lines),
                                        defaults to not including process
                    rec        boolean  use recombination data (R-lines),
                                        defaults to not including process
                    cx         boolean  use cx data (H-lines),
                                        defaults to not including process
                    gcr        boolean  output gcr set of file to pass_dir,
                                        default is not to output files
                    pec        boolean  output pec data to pass_dir as pec.pass,
                                        default is not to output file
                    sxb        boolean  output sxb data to pass_dir as sxb.pass,
                                        default is not to output file

      RETURNS    :  pop        dict     { 'numlev'      : number of ordinary levels
                                          'nummet'      : number of metastables
                                          'levels'      : configuration + quantum numbers of ordinary levels
                                          'metastables' :               "                    metastables
                                          'dependence'  : population dependence on metastables

                                          dependence[nord, nmet, nte, ndens]
      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Zeff was passed to fortran incorrectly when it was 0.0.
                     - Return levels and metastables as strings.
           1.3     Martin O'Mullane
                     - Allow up to 13 metastables.
           1.4     Martin O'Mullane
                     - Allow user to directly specify which transitions are
                       to be included in adf15 file.
                     - Set imetr back to int (problem fixed in adas_write_pipe).
           1.5     Martin O'Mullane
                     - nonorm logic was inverted.

      VERSION    :
            1.1    23-06-2017
            1.2    16-07-2018
            1.3    03-08-2018
            1.4    27-09-2018
            1.5    19-11-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    import os
    import subprocess
    from datetime import datetime
    from numpy import asarray, zeros, chararray, atleast_1d
    from adaslib import adas_write_pipe
    
    # Initialisation and setup

    d     = datetime.now()
    today = d.strftime("%d/%m/%y")
    now   = d.strftime("%H:%M:%S")
    uid   = os.getenv('USER')

    fortran_code = os.getenv('ADASFORT') + '/adas208.out'


    # Examine inputs

    if adf04 == None:
       raise Exception('A valid specific ion file (adf04) is required')
    if (os.path.isfile(adf04) and os.access(adf04, os.R_OK)) == False:
       raise Exception('A valid specific ion file (adf04) is required')

    print('Processing....' + adf04)

    if adf18 == None:
       proj = False
       adf18_int = 'NULL'
    else:
       proj = True
       adf18_int = adf18
       if (os.path.isfile(adf18) and os.access(adf18, os.R_OK)) == False:
          raise Exception('A valid projection file (adf18) is required')

    # check and conversions

    te_int   = atleast_1d(asarray(te, float))
    dens_int = atleast_1d(asarray(dens, float))

    if zeff == 0.0:
       tion_int = zeros(te_int.size, float)
       dion_int = zeros(dens_int.size, float)
    else:
       tion_int = atleast_1d(asarray(tion, float))
       dion_int = atleast_1d(asarray(dion, float))

    if not cx:
       th_int   = zeros(te_int.size, float)
       denh_int = zeros(dens_int.size, float)
    else:
       th_int   = atleast_1d(asarray(th, float))
       denh_int = atleast_1d(asarray(denh, float))

    choices = {'k' : 1, 'ev' : 2, 'red' : 3}
    ifout   = choices.get(unit_te.lower(), None)

    if ifout == None: raise Exception('Incorrect units for temperature')

    choices = {'cm-3' : 1, 'red' : 2}
    idout   = choices.get(unit_dens.lower(), None)

    if idout == None: raise Exception('Incorrect units for density')

    meta = atleast_1d(asarray(meta, int))
    if meta.size > 13 : raise Exception('Maximum of 13 metastables are allowed')
    if te_int.size > 35 : raise Exception('Maximum of 35 temperatures are allowed')
    if dens_int.size > 24 : raise Exception('Maximum of 24 densities are allowed')

    # PEC selection
    
    wmin = atleast_1d(asarray(wmin, float))
    wmax = atleast_1d(asarray(wmax, float))
    amin = atleast_1d(asarray(amin, float))

    itrup  = atleast_1d(asarray(tr_upper, int))
    itrlow = atleast_1d(asarray(tr_lower, int))
    
    ntrsel = 0
    if itrup.size >= 1 and itrup[0] > 0:
        print('transition selection overrides wmin/wmax/amin method')
        ntrsel = itrup.size
       
        if itrup.size != itrlow.size: 
            raise Exception('Number of upper and lower transition indicies must be the same')
   
    # Launch fortran and interact with it

    pipe = subprocess.Popen(fortran_code,
           shell=False, bufsize=0,
           stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)

    # First (input) panel

    adas_write_pipe(pipe, today)
    adas_write_pipe(pipe, uid)

    rep = 'NO'
    adas_write_pipe(pipe, rep)
    adas_write_pipe(pipe, adf04)
    adas_write_pipe(pipe, adf18_int)

    # b8setp

    line = pipe.stdout.readline()  # not used sz0
    line = pipe.stdout.readline()  # not used sz
    line = pipe.stdout.readline()  # not used scnte
    line = pipe.stdout.readline()  # not used sil
    line = pipe.stdout.readline()  # lfpool
    lfpool = int(line)

    for j in range(2*lfpool):
       line = pipe.stdout.readline()

    line = pipe.stdout.readline()
    ndmet = int(line)

    line = pipe.stdout.readline()  # not used ndwvl
    line = pipe.stdout.readline()  # not used ndsel
    line = pipe.stdout.readline()  # not used npl

    for j in range(ndmet):
       line = pipe.stdout.readline()

    line = pipe.stdout.readline()
    ndlev = int(line)

    for j in range(ndmet*ndlev):
       line = pipe.stdout.readline()


    # Processing panel

    tscef = zeros((14,3))          # imposed by fortran-IDL design

    line = pipe.stdout.readline()  # lpend
    line = pipe.stdout.readline()  # selem
    line = pipe.stdout.readline()
    ndtem = int(line)
    line = pipe.stdout.readline()
    ndden = int(line)
    line = pipe.stdout.readline()
    il = int(line)
    line = pipe.stdout.readline()
    nv = int(line)

    for i in range(3):
       for j in range(14):
          line = pipe.stdout.readline()
          tscef[j,i]=float(line)

    adas_write_pipe(pipe, 0)


    # fill arrays to size as dimensioned in fortran code ([0:2] means fill index 0 and 1)

    imetr_in = zeros(ndmet, dtype=int)
    imetr_in[0:meta.size] = meta            
                                            
    te_in = zeros(ndtem, dtype=float)
    te_in[0:te_int.size] = te_int
    tion_in = zeros(ndtem, dtype=float)
    tion_in[0:tion_int.size] = tion_int
    th_in = zeros(ndtem, dtype=float)
    th_in[0:th_int.size] = th_int

    dens_in = zeros(ndden, dtype=float)
    dens_in[0:dens_int.size] = dens_int
    dion_in = zeros(ndden, dtype=float)
    dion_in[0:dion_int.size] = dion_int
    rat_in = zeros(ndden, dtype=float)
    rat_in[0:dens_int.size] = denh_int / dens_int

    adas_write_pipe(pipe, 'Produced by run_adas208')
    adas_write_pipe(pipe, meta.size)

    for j in range(ndmet):
       adas_write_pipe(pipe, imetr_in[j])

    adas_write_pipe(pipe, ifout)
    adas_write_pipe(pipe, te_int.size)       # note _int not _in

    for j in range(ndtem):
       adas_write_pipe(pipe, te_in[j])
    for j in range(ndtem):
       adas_write_pipe(pipe, tion_in[j])
    for j in range(ndtem):
       adas_write_pipe(pipe, th_in[j])

    adas_write_pipe(pipe, idout)
    adas_write_pipe(pipe, dens_int.size)

    for j in range(ndden):
       adas_write_pipe(pipe, dens_in[j])
    for j in range(ndden):
       adas_write_pipe(pipe, dion_in[j])
    for j in range(ndden):
       adas_write_pipe(pipe, rat_in[j])

    ratio = zeros((ndden, ndmet), float)
    ratio[:,0] = 1.0

    for i in range(ndmet):
       for j in range(ndden):
          adas_write_pipe(pipe, ratio[j,i])

    for i in range(ndmet):
       for j in range(ndden):
          adas_write_pipe(pipe, ratio[j,i])

    adas_write_pipe(pipe, zeff)

    if zeff > 0.0:
       adas_write_pipe(pipe, 1)
       adas_write_pipe(pipe, 1)
    else:
       adas_write_pipe(pipe, 0)
       adas_write_pipe(pipe, 0)

    is_cx   = 1 if cx else 0
    is_rec  = 1 if rec else 0
    is_ion  = 1 if ion else 0
    is_proj = 1 if proj else 0
    norm    = 0 if nonorm else 1

    adas_write_pipe(pipe, 0)
    adas_write_pipe(pipe, is_cx)
    adas_write_pipe(pipe, is_rec)
    adas_write_pipe(pipe, is_ion)
    adas_write_pipe(pipe, is_proj)
    adas_write_pipe(pipe, norm)

    for i in range(ndmet):
       for j in range(ndmet):
          adas_write_pipe(pipe, 0)

    for i in range(ndmet):
       for j in range(ndmet):
          adas_write_pipe(pipe, 0)

    for i in range(ndtem):
       for j in range(ndmet):
          for k in range(ndmet):
             adas_write_pipe(pipe, 0.0)

    adas_write_pipe(pipe, ' ')

    adas_write_pipe(pipe, wmin.size)
    for j in range(wmin.size):
       adas_write_pipe(pipe, wmin[j])
       adas_write_pipe(pipe, wmax[j])
       adas_write_pipe(pipe, amin[j])
    
    adas_write_pipe(pipe, ntrsel)
    for j in range(ntrsel):
        adas_write_pipe(pipe, itrlow[j])
        adas_write_pipe(pipe, itrup[j])

    adas_write_pipe(pipe, 0)


    # Output screen

    header = ' run_adas208 : DATE: '+ today + ' TIME: ' + now

    line = pipe.stdout.readline()

    maxt = int(line)
    tine = zeros(maxt, float)
    for j in range(maxt):
       line = pipe.stdout.readline()
       tine[j] = float(line)
    line = pipe.stdout.readline()

    adas_write_pipe(pipe, 0)


    adas_write_pipe(pipe, 1)   # choose pop production always for python version
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 'Arbitrary graph title!')


    if logfile == None:
       adas_write_pipe(pipe, 0)
    else:
       adas_write_pipe(pipe, 1)
       adas_write_pipe(pipe, 0)
       adas_write_pipe(pipe, logfile)

    if pec:
       adas_write_pipe(pipe, 1)
    else:
       adas_write_pipe(pipe, 0)

    if sxb:
       adas_write_pipe(pipe, 1)
    else:
       adas_write_pipe(pipe, 0)

    if gcr:
       adas_write_pipe(pipe, 1)
    else:
       adas_write_pipe(pipe, 0)


    adas_write_pipe(pipe, pass_dir + '/.pass')

    if logfile != None:
       adas_write_pipe(pipe, header)

    adas_write_pipe(pipe, 0)


    # get the population data

    line = pipe.stdout.readline()
    ndlev = int(line)
    line = pipe.stdout.readline()
    ndtem = int(line)
    line = pipe.stdout.readline()
    ndden = int(line)
    line = pipe.stdout.readline()
    ndmet = int(line)
    line = pipe.stdout.readline()
    titled = line
    line = pipe.stdout.readline()
    title = line
    line = pipe.stdout.readline()
    gtit1 = line
    line = pipe.stdout.readline()
    dsninc = line
    line = pipe.stdout.readline()
    iz = int(line)
    line = pipe.stdout.readline()
    itsel = int(line)
    line = pipe.stdout.readline()
    tev = float(line)
    line = pipe.stdout.readline()
    il = int(line)
    line = pipe.stdout.readline()
    nmet = int(line)
    line = pipe.stdout.readline()
    nord = int(line)
    line = pipe.stdout.readline()
    maxd = int(line)

    lmetr = zeros(nmet, int)
    imetr = zeros(nmet, int)
    iordr = zeros(nord, int)
    densa = zeros(maxd, float)
    strga = chararray(il, itemsize=30)
    stack = zeros((il,nmet,ndtem,maxd), float)

    for j in range(nmet):
       line = pipe.stdout.readline()
       lmetr[j] = int(line)
    for j in range(nmet):
       line = pipe.stdout.readline()
       imetr[j] = int(line)
    for j in range(nord):
       line = pipe.stdout.readline()
       iordr[j] = int(line)
    for j in range(maxd):
       line = pipe.stdout.readline()
       densa[j] = float(line)
    for j in range(il):
       line = pipe.stdout.readline()
       strga[j] = line

    for l in range(maxd):
       for k in range(ndtem):
          for j in range(nmet):
             for i in range(il):
                line = pipe.stdout.readline()
                stack[i,j,k,l] = float(line)


    # Terminate adas208

    adas_write_pipe(pipe, 1)


    # Return the population dictionary as the output of this fuction

    dependence = stack[0:il-nmet, 0:nmet, 0:maxt, 0:maxd]

    pop  = { 'numlev'      : nord,
             'nummet'      : nmet,
             'levels'      : strga[iordr-1].astype(str),
             'metastables' : strga[imetr-1].astype(str),
             'dependence'  : dependence    }

    return pop
