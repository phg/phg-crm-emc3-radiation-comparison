def bxttyp(ndlev, ndmet, ndtrn, itran, tcode, i1a, i2a, aval):
    """
      PURPOSE    : Sorts transition arrays into transition/recomb types

      result = bxttyp(ndlev, ndmet, ndtrn, nplr, npli, itran, tcode, i1a, i2a , aval)

                    NAME         TYPE     DETAILS
      REQUIRED   :  wave()       float    wavelength required (A)
                    tev()        float    electron temperature (eV)
                    iz0          int      atomic number
                    iz1          int      ion stage + 1

      RETURNS       result       dict

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    03-07-2018

    """

    # External routines required

    from .bxttyp_if import bxttyp_if
    from numpy import asarray, zeros, int8

    nplr   = 0
    npli   = 0
    icnte  = 0
    icntp  = 0
    icntr  = 0
    icnth  = 0
    icnti  = 0
    icntl  = 0
    icnts  = 0
    ietrn  = asarray(zeros(ndtrn), int)
    iptrn  = asarray(zeros(ndtrn), int)
    irtrn  = asarray(zeros(ndtrn), int)
    ihtrn  = asarray(zeros(ndtrn), int)
    iitrn  = asarray(zeros(ndtrn), int)
    iltrn  = asarray(zeros(ndtrn), int)
    istrn  = asarray(zeros(ndtrn), int)
    ie1a   = asarray(zeros(ndtrn), int)
    ie2a   = asarray(zeros(ndtrn), int)
    aa     = asarray(zeros(ndtrn), float)
    ip1a   = asarray(zeros(ndtrn), int)
    ip2a   = asarray(zeros(ndtrn), int)
    ia1a   = asarray(zeros(ndtrn), int)
    ia2a   = asarray(zeros(ndtrn), int)
    auga   = asarray(zeros(ndtrn), float)
    il1a   = asarray(zeros(ndlev), int)
    il2a   = asarray(zeros(ndlev), int)
    wvla   = asarray(zeros(ndlev), float)
    is1a   = asarray(zeros(ndlev), int)
    is2a   = asarray(zeros(ndlev), int)
    lss04a = asarray(zeros((ndlev, ndmet)), int8)

    (nplr   , npli  ,
     icnte  , icntp , icntr  , icnth , icnti ,
     icntl  , icnts ,
     ietrn  , iptrn , irtrn  , ihtrn , iitrn ,
     iltrn  , istrn ,
     ie1a   , ie2a  , aa    ,
     ip1a   , ip2a  ,
     ia1a   , ia2a  , auga  ,
     il1a   , il2a  , wvla  ,
     is1a   , is2a  , lss04a) = bxttyp_if(ndlev, ndmet, ndtrn,
                                          itran, tcode, i1a, i2a, aval)

    result = { 'nplr'   :  nplr,
               'npli'   :  npli,
               'icnte'  :  icnte,
               'icntp'  :  icntp,
               'icntr'  :  icntr,
               'icnth'  :  icnth,
               'icnti'  :  icnti,
               'icntl'  :  icntl,
               'icnts'  :  icnts,
               'ietrn'  :  ietrn,
               'iptrn'  :  iptrn,
               'irtrn'  :  irtrn,
               'ihtrn'  :  ihtrn,
               'iitrn'  :  iitrn,
               'iltrn'  :  iltrn,
               'istrn'  :  istrn,
               'ie1a'   :  ie1a,
               'ie2a'   :  ie2a,
               'aa'     :  aa,
               'ip1a'   :  ip1a,
               'ip2a'   :  ip2a,
               'ia1a'   :  ia1a,
               'ia2a'   :  ia2a,
               'auga'   :  auga,
               'il1a'   :  il1a,
               'il2a'   :  il2a,
               'wvla'   :  wvla,
               'is1a'   :  is1a,
               'is2a'   :  is2a,
               'lss04a' :  lss04a}

    return result
