def ngffmh(gam2):
    """
      PURPOSE    : Evaluates electron temperature and frequency averaged
                   hydrogenic free free Gaunt factor.

      gff = ngffmg(iz**2*157894.0/te) for te in K
          or
      gff = ngffmg(iz**2*13.6/te) for te in eV

                   NAME         TYPE     DETAILS
      REQUIRED   : gam2         float    array of z**2 *IH / Te

      RETURNS      ngffmh       float    free-free Gaunt factor array.
      
      
      NOTES      :

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    22-09-2019

    """

    # External routines required

    from .ngffmh_calc import ngffmh_calc
    from numpy import asarray, zeros,atleast_1d

    # Make internal variables from input

    g2_int = atleast_1d(asarray(gam2, float))
    num    = g2_int.size
    res    = atleast_1d(asarray(zeros(num, float)))
    
    res = ngffmh_calc(g2_int)
    
    # remove redundant dimensions

    if num == 1:
        res = res[0]
        
    return res
