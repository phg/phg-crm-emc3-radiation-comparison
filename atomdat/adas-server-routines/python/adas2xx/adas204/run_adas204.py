def run_adas204(adf25=None, adf17=None, logfile=None,
                popfile=None, projfile=None,
                acdfile=None, scdfile=None, xcdfile=None,
                prbfile=None, pcasfile=None):
    """

      PURPOSE    :  Runs the adas204 bundle-n population code.

                    run_adas310(adf01='bns96#h_si13.dat', adf17='proj_matrix.dat')

                    NAME       TYPE     DETAILS
      INPUTS     :  adf25      str      driver file for adas204
                    adf17      str      output file of projection matrices

                    logfile    str      log file
                    popfile    str      file with population details
                    projfile   str      file with projection details
                    acdfile    str      file of recombination coefficients
                    scdfile    str      file of ionization coefficients
                    xcdfile    str      file of cross-coupling coefficients
                    prbfile    str      file of recombination powers
                    pcasfile   str      file of wavelength binned recombination power


      NOTES      :  The output of the routine is a set of files.
                    adf17 is required but outputing all other files
                    is optional and the default is not to write them.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
           1.1     20-11-2019

    """

    # External routines required

    import os
    import subprocess
    from numpy import arange
    from adaslib import adas_write_pipe, xxdate


    # Initialisation and setup

    today = xxdate()
    adas_c = os.getenv('ADASCENT')
    adas_u = os.getenv('ADASUSER')
    fortran_code = os.getenv('ADASFORT') + '/adas204.out'


    # Examine inputs - ADAS files

    if adf25 is None:
        raise Exception('A valid driver file (adf25) is required')
    if (os.path.isfile(adf25) and os.access(adf25, os.R_OK)) == False:
        raise Exception('A valid driver file (adf25) is required')

    if adf17 is None:
        raise Exception('A projection file (adf17) is required')


    # Launch fortran and interact with it

    pipe = subprocess.Popen(fortran_code, shell=False, bufsize=0,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                            close_fds=True)

    # First (input) panel

    adas_write_pipe(pipe, 0)                  # not batch mode
    adas_write_pipe(pipe, today)

    adas_write_pipe(pipe, 'NO')
    adas_write_pipe(pipe, adf25)
    adas_write_pipe(pipe, adas_c)
    adas_write_pipe(pipe, adas_u)


    # Run calculation and output results

    file_names = [logfile, adf17, projfile, acdfile, scdfile, xcdfile, prbfile, pcasfile, popfile]

    adas_write_pipe(pipe, 0)

    for f in file_names:
        if f is None:
            adas_write_pipe(pipe, ' ')
            adas_write_pipe(pipe, 0)
        else:
            adas_write_pipe(pipe, f)
            adas_write_pipe(pipe, 1)

    line = pipe.stdout.readline()
    num = int(line)

    for j in arange(num):
        line = pipe.stdout.readline()

    line = pipe.stdout.readline()
    num = int(line)
    
    if num == -1:
        adas_write_pipe(pipe, 'YES')
    else:
        raise Exception('Something went wrong in adas204') 

    return
