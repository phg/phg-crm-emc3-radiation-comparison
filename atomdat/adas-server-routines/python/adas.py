"""
Atomic Data and Analysis Structure (ADAS)

This library provides a number of ADAS routines in python.

Many of these call on compiled fortran routines.

They should be compatible with python 2 and 3.
"""
__version_info__ = ('4', '1', '0')
__version__ = '.'.join(__version_info__)

from adas2xx import(ngffmh, run_adas204, run_adas208, run_adas218, bxttyp)

from adas3xx import(stark, c5dplr, run_adas308, run_adas310, run_adas312,
                    cxsqef)

from adas4xx import(run_adas405, run_adas406, run_adas416,
                    adas4xx_ionbal_files, adas4xx_ionbal_tau)

from adaslib import(continuo, i4eiz0, i4idfl, xxcftr, xxdtes, xxeiam,
                    xxelem, xxesym, xxidtl,
                    r8ah, r8ahnn, r8rd2b,
                    xxsple, xxtrisol,
                    adas_vector, array2strlist, i4indf, list_index_str,
                    numlines, readfile, strlist2array, writefile,
                    read_adf02, read_adf07, read_adf11, read_adf12, read_adf13,
                    read_adf15, read_adf21, read_adf40,
                    write_adf11, write_adf37,
                    adf11_nmeta,
                    adas_write_pipe, xxdate, xxuser,
                    xxdata_00, xxdata_01, xxdata_02, xxdata_04, xxdata_06, 
                    xxdata_07, xxdata_09, xxdata_10_p208,
                    xxdata_11, xxdata_12, xxdata_13,
                    xxdata_15, xxdata_21, xxdata_37, xxdata_40,
                    xxscript_416)
