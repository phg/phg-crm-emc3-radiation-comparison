def cxsqef(file=None, ibsel=0, epro=[0], ttar=[0], em1=0.0, em2=0.0, iord=1,
           ti=[0], densi=[0], zeff=[0], bmag=[0]):
    """
      PURPOSE    :  Return the CX effective emission coefficient from an
                    adf12 file.

                    If iord, em1, em2 and ttar are set the Maxwell averaged
                    effective rate for a mono-energetic donor and thermal
                    receiver is returned.

      REQUIRED   :  file       str      Full name of ADAS adf12 file
                    ibsel      int      Selected block in adf12
                    epro()     double   Incident particle energy (ev/amu)
                    ttar()     double   Maxwell temperature of target particles (ev)
                                        If (ttar.le.0) then rates for t=0 are
                                        returned
                    em1        double   Atomic mass number of first particle
                    em2        double   Atomic mass number of second particle
                    iord       long        1 for 1st particle incident and monoenergetic
                                           2 for 2nd particle incident and monoenergetic
                    ti()       double   Plasma ion temperature (ev)
                    densi()    double   Plasma ion density (cm-3)
                    zeff()     double   Plasma z effective
                    bmag()     double   Plasma magnetic field (tesla)

      OPTIONAL   :

      RETURNS    :  qeff()     float    Effective rate coefficient (cm3 sec-1)
                    ener()     float    Set of energies (ev/amu) for selected source data
                    qener(,)   float    Rate coeffts.(cm**3 sec-1) for ibsel
                                           1st dimension: energy index adf12
                                           2nd dimension: energy index in epro

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    15-12-2012

    """

    import os
    from numpy import asarray, zeros
    from .cxsqef_ import cxsqef

    # make sure something is set for all inputs

    if not file:
        raise Exception('A valid adf12 file is required')
    else:
        if (os.path.isfile(file) and os.access(file, os.R_OK)) == False:
            raise Exception('adf12 file is invalid')

    if ibsel == 0:
        raise Exception('A valid block in adf12 is required')

    epro = asarray(epro, float)
    if epro.size == 1: epro = asarray([epro], float)

    ttar = asarray(ttar, float)
    if ttar.size == 1: ttar = asarray([ttar], float)

    ti = asarray(ti, float)
    if ti.size == 1: ti = asarray([ti], float)

    densi = asarray(densi, float)
    if densi.size == 1: densi = asarray([densi], float)

    zeff = asarray(zeff, float)
    if zeff.size == 1: zeff = asarray([zeff], float)

    bmag = asarray(bmag, float)
    if bmag.size == 1: bmag = asarray([bmag], float)

    nqeff = len(epro)

    if iord not in [1,2]:
        raise Exception('iopt must be 1 or 2')

    # Internal settings - nener matched to cxsqef.for and cxsqef_.pyf

    iunit = 67
    nener = 24

    # Define output parameters from cxsqef.for - not passed back from this routine

    ener   = asarray(zeros(nener, float))
    qener  = asarray(zeros((nener,nqeff), float))
    ircode = 0

    csymb  = 'A2'
    czion  = 'B2'
    cwavel = 'C2345678'
    cdonor = 'D23456'
    crecvr = 'E2345'
    ctrans = 'F234567'
    cfile  = 'G23456789a'
    ctype  = 'H2'
    cindm  = 'I23'

    ener, qener, qeff = cxsqef(iunit, file, ibsel, 
                               epro, ttar, em1, em2, iord, 
                               ti, densi, zeff, bmag, 
                               nener,
                               csymb, czion, cwavel, cdonor, crecvr, ctrans,
                               cfile, ctype, cindm, ircode, nqeff)

    return ener, qener, qeff
