def run_adas308(adf01 = None   , mass = 0.0,
                te = [0.0]     , tion = [0.0],
                dens = [0.0]   , dion = [0.0],
                zeff = 1.0     , bmag = 3.0,
                energy = [0.0] , frac = [0.0],
                n1in = 1       , n2in = 2,
                n1req = 8      , n2req = 7,
                emiss = 1.0e12 ,
                elec = False   , logfile = None):

    """

      PURPOSE    :  Runs the adas308 CX coefficient generation code as a python function

      data = run_adas308(adf01='qcx#h0_old#c6.dat', energy=[1e4, 5e4], frac=[0.7,0.3], ....)

                    NAME       TYPE     DETAILS
      REQUIRED   :  adf01      str      charge exchange cross section file
                    te         float()  electron temperatures requested
                    tion       float()  ion temperatures requested
                    dens       float()  electron densities requested
                    dion       float()  ion densities requested
                    energy     float()  beam energy components (eV/amu)
                    frac       float()  fraction in each beam component (normalized to 1.0)
                    n1req      int      lower level of requested coefficient
                    n2req      int      upper level of requested coefficient
                    elec       boolean  True => use electron impact rather than
                                                CX to evaluate coefficient
                    logfile    str      text log file, defaults to no output file

      OPTIONAL   :  mass       float    atomic mass of the receiver
                    n1in       int      lower level of observed line
                    n2in       int      upper level of observed line
                    emiss      float    emission measure of observed line
                    zeff       float    for re-scaling ion densities, defaults to 1.0
                    bmag       float    magnetic field (not used in model)

      RETURNS    :  data       dict    { 'qeff'  - effective rate coefficient (ph cm3/s)
                                         'ncomp' - number in nl-n'l' complex
                                         'wave'  - nl-n'l' air wavelengths (A)
                                         'emiss' - nl-n'l' column emissivities
                                         'pop'   - nl-n'l' column populations
                                         'nu'    - nl-n'l' complex : n
                                         'nl'    - nl-n'l' complex : n'
                                         'lu'    - nl-n'l' complex : l
                                         'll'    - nl-n'l' complex : l'
                                         'aval'  - nl-n'l' A-values (/s)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version
           1.2     Martin O'Mullane
                     - Alter the termination sequence when a log file is 
                       not requested.

      VERSION    :
            1.1    29-03-2019
            1.2    09-04-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    import os
    import subprocess
    from datetime import datetime
    from numpy import asarray, zeros, atleast_1d, roll
    from numpy.linalg import solve
    from adaslib import adas_write_pipe, xxdata_01, xxeiam, xxtrisol, r8ah

    # Initialisation and setup

    d     = datetime.now()
    today = d.strftime("%d/%m/%y")

    fortran_code = os.getenv('ADASFORT') + '/adas308.out'


    # Examine inputs

    if adf01 == None:
        raise Exception('A valid CX cross section file (adf01) is required')
    if (os.path.isfile(adf01) and os.access(adf01, os.R_OK)) == False:
        raise Exception('A valid CX cross section file (adf01) is required')

    print('Processing....' + adf01)


    # Checks and conversions

    te_int   = float(te)
    tion_int = float(tion)
    dens_int = float(dens)
    dion_int = float(dion)

    eng_int = atleast_1d(asarray(energy, float))
    frac_int = atleast_1d(asarray(frac, float))

    if eng_int.size != frac_int.size:
        raise Exception('Number of energies must match number of fractions')

    # Get mass of receiver if not supplied

    res = xxdata_01(file=adf01)
    mass_file = xxeiam(res['symbr'])
    izr = res['izr']

    if mass == 0.0:
        mass = mass_file


    # Check transition selections

    if n1in == n2in:
        raise Exception("Input n and n' cannot be the same")

    if n1in > n2in:
        nlin = n2in
        nuin = n1in
    else:
        nlin = n1in
        nuin = n2in

    nl = min([n1req, n2req])
    nu = max([n1req, n2req])

    if nl == nu:
        raise Exception("Requested n and n' cannot be the same")

    # Switch for CX/e-impact

    if elec:
        iemms = 2
    else:
        iemms = 1


    # Launch fortran and interact with it

    pipe = subprocess.Popen(fortran_code,
           shell=False, bufsize=0,
           stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)


    # First panel (input)

    adas_write_pipe(pipe, today)

    rep = 'NO'
    adas_write_pipe(pipe, rep)
    adas_write_pipe(pipe, adf01)


    # cxsetp

    line = pipe.stdout.readline()
    mxtab = int(line)
    line = pipe.stdout.readline()
    mxgrf = int(line)
    line = pipe.stdout.readline()
    symbd = line
    line = pipe.stdout.readline()
    idz0 = int(line)
    line = pipe.stdout.readline()
    symbr = line
    line = pipe.stdout.readline()
    irz0 = int(line)
    line = pipe.stdout.readline()
    irz1 = int(line)
    line = pipe.stdout.readline()
    irz2 = int(line)
    line = pipe.stdout.readline()
    ngrnd = int(line)
    line = pipe.stdout.readline()
    ntot = int(line)

    line = pipe.stdout.readline()


    # Second panel (processing)

    nbeam  = eng_int.size
    noline = 1
    npline = 1
    title  = 'run_adas308.py version'

    adas_write_pipe(pipe, 0)
    adas_write_pipe(pipe, title)
    adas_write_pipe(pipe, mass)
    adas_write_pipe(pipe, tion_int)
    adas_write_pipe(pipe, te_int)
    adas_write_pipe(pipe, dion_int)
    adas_write_pipe(pipe, dens_int)
    adas_write_pipe(pipe, zeff)
    adas_write_pipe(pipe, bmag)
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, iemms)
    adas_write_pipe(pipe, nbeam)

    for j in range(nbeam):
        adas_write_pipe(pipe, frac_int[j])
        adas_write_pipe(pipe, eng_int[j])

    adas_write_pipe(pipe, noline)                # force to 1 for run_adas308.py
    adas_write_pipe(pipe, nuin)
    adas_write_pipe(pipe, nlin)
    adas_write_pipe(pipe, emiss)

    adas_write_pipe(pipe, npline)                # force to 1 for run_adas308.py
    adas_write_pipe(pipe, nu)
    adas_write_pipe(pipe, nl)

    adas_write_pipe(pipe, 0)                     # lrttb

    adas_write_pipe(pipe, npline)
    adas_write_pipe(pipe, 1)

    adas_write_pipe(pipe, 1)                     # ntab set to 1 for nl - n'l'
    adas_write_pipe(pipe, 1)

    adas_write_pipe(pipe, 0)


    # c8wsol

    line = pipe.stdout.readline()
    ntemp = int(line)

    while ntemp == 1:

        line = pipe.stdout.readline()
        n = int(line)

        vd = zeros(n, float)
        vds = zeros(n, float)
        vdi = zeros(n, float)
        rhs = zeros(n, float)

        for j in range(n):
            line = pipe.stdout.readline()
            vd[j] = float(line)
        for j in range(n):
            line = pipe.stdout.readline()
            vds[j] = float(line)
        for j in range(n):
            line = pipe.stdout.readline()
            vdi[j] = float(line)
        for j in range(n):
            line = pipe.stdout.readline()
            rhs[j] = float(line)

        vds = roll(vds, -1)
        ans = xxtrisol(vdi, vd, vds, rhs)

        for j in range(n):
            adas_write_pipe(pipe, ans[j])

        line = pipe.stdout.readline()
        ntemp = int(line)

    # c8emqx

    line = pipe.stdout.readline()
    nrep = int(line)

    ared   = asarray(zeros((nrep, nrep)), float)
    remisa = asarray(zeros(nrep), float)
    remq   = asarray(zeros(nrep), float)

    for i in range(nrep):
        for j in range(nrep):
            line = pipe.stdout.readline()
            ared[i,j] = float(line)
        line = pipe.stdout.readline()
        remisa[i] = float(line)

    if nrep == 1:
        remq[0] = remisa[0] / ared[0,0]
    else:
        remq = solve(ared, remisa)

    for j in range(nrep):
        adas_write_pipe(pipe, remq[j])

    line = pipe.stdout.readline()

    adas_write_pipe(pipe, 0)  # lpend


    # Third panel (output)

    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 0)

    if logfile == None:
        adas_write_pipe(pipe, 0)
    else:
        adas_write_pipe(pipe, 1)
        adas_write_pipe(pipe, logfile)

    adas_write_pipe(pipe, 0)

    line = pipe.stdout.readline()
    ngrf = int(line)

    if ngrf > 0:

        line = pipe.stdout.readline()
        mxa = int(line)
        line = pipe.stdout.readline()
        mxb = int(line)

        xa = asarray(zeros((ngrf, mxa)), float)
        ya = asarray(zeros((ngrf, mxa)), float)
        xb = asarray(zeros((ngrf, mxb)), float)
        yb = asarray(zeros((ngrf, mxb)), float)

        qeff = asarray(zeros(ngrf), float)

        for j in range(ngrf):

            line = pipe.stdout.readline()
            npts = int(line)

            line = pipe.stdout.readline()
            qeff[j] = float(line)

            for i in range(mxa):
                line = pipe.stdout.readline()
                xa[j,i] = float(line)
                line = pipe.stdout.readline()
                ya[j,i] = float(line)

            if npts < mxa:
                xa[j, npts:] = 0.0
                ya[j, npts:] = 0.0

            for i in range(mxb):
                line = pipe.stdout.readline()
                xb[j,i] = float(line)
                line = pipe.stdout.readline()
                yb[j,i] = float(line)

    adas_write_pipe(pipe, 0)

    # End the process

    if logfile != None:
        adas_write_pipe(pipe, " ADAS")
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 1)


    # Assemble data for output

    ncomp  = npts
    nu_out = asarray(zeros(ncomp), float)
    nl_out = asarray(zeros(ncomp), float)
    lu_out = asarray(zeros(ncomp), float)
    ll_out = asarray(zeros(ncomp), float)
    aa_out = asarray(zeros(ncomp), float)

    n  = n1req
    n1 = n2req

    k  = 0
    for l in range(n1+1):
        for l1 in [l+1, l-1]:
            if (l1 >= 0) and (l1 < n1):

                nu_out[k] = n
                nl_out[k] = n1
                lu_out[k] = l
                ll_out[k] = l1

                aa_out[k] = r8ah(n,l,n1,l1)
                k = k + 1


    result = {'qeff'  : qeff[0],
              'ncomp' : ncomp,
              'wave'  : xa[0,0:ncomp],
              'emiss' : xa[0,0:ncomp],
              'pop'   : xa[0,0:ncomp] / (aa_out*izr**4),
              'nu'    : nu_out,
              'lu'    : lu_out,
              'nl'    : nl_out,
              'll'    : ll_out,
              'aval'  : aa_out }

    return result
