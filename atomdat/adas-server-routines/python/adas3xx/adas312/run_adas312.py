def run_adas312(adf26   = None,
                nlow    = None,
                nup     = None,
                outfile = None):

    """

      PURPOSE    :  Runs the adas312 adf21/adf22 extraction code as
                    a python function

      res = run_adas312(adf26='bdn10#h_h1.dat', nlow=2, nup=4, file='adf22.pass')

                    NAME       TYPE     DETAILS
      REQUIRED   :  adf26      str      charge exchange cross section file
                    outfile    str      beam stopping, beam emission or
;                                       excited population level filename

      OPTIONAL   :  nlow       int      lower n-level for beam emission
                    nup        int      upper n-level for beam emission
                                        or n value for the excited n-level
                                        population

      RETURNS    :  ircode     int      success code
                                          0 => successful
                                          1 => there was a problem

      NOTES      : If nlow and nup are specified a beam emission file is produced.
                   If nup only is specified a beam density file is produced.
                   If neither is specified a beam stopping file is produced.

                   The output of the routine is a file.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
           1.1     16-04-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    import os
    import subprocess
    from datetime import datetime
    from adaslib import adas_write_pipe

    # Initialisation and setup

    ircode = 0
    d      = datetime.now()
    today  = d.strftime("%d/%m/%y")

    fortran_code = os.getenv('ADASFORT') + '/adas312.out'


    # Examine inputs - adf26 and outfile

    if adf26 == None:
        raise Exception('A valid population file (adf26) is required')
    if (os.path.isfile(adf26) and os.access(adf26, os.R_OK)) == False:
        raise Exception('A valid population file (adf26) is required')
    a312_adf26 = adf26

    if outfile == None:
        raise Exception('An output filename is required')
    a312_outfile = outfile


    # Examine inputs - nup and nlow determines type of output

    a312_nup  = -1
    a312_nlow = -1
    option    = -1

    if nup != None:
        a312_nup = nup
    if nlow != None:
        a312_nlow = nlow

    if a312_nlow == -1 and a312_nup == -1:
        option = 0

    if a312_nup > 0 and a312_nlow == -1:
        option = 2

    if a312_nup > 0 and a312_nlow > 0:
        option = 1

    if option == -1:
        raise Exception('Problem in specifying nlow and nup')



    # Launch fortran and interact with it

    pipe = subprocess.Popen(fortran_code,
           shell=False, bufsize=0,
           stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)


    # First panel (input)

    adas_write_pipe(pipe, today)
    adas_write_pipe(pipe, 'NO')

    adas_write_pipe(pipe, a312_adf26)


    # Data from adas312 - not needed

    for j in range(9):
        line = pipe.stdout.readline()

    line = pipe.stdout.readline()
    ntemp = int(line)

    for j in range(ntemp):
        line = pipe.stdout.readline()


    # Processing

    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 0)
    adas_write_pipe(pipe, 0)

    adas_write_pipe(pipe, option+1)

    if option == 1:
        adas_write_pipe(pipe, a312_nlow)
        adas_write_pipe(pipe, a312_nup)
        line = pipe.stdout.readline()
        dflag = int(line)

    if option == 2:
        adas_write_pipe(pipe, a312_nup)
        line = pipe.stdout.readline()
        dflag = int(line)

    if option == 1 and dflag == 0:
        raise Exception('Data for selected transition is unavaliable')


    # Skip past the graphics

    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 0)
    adas_write_pipe(pipe, 0)

    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, a312_outfile)
    adas_write_pipe(pipe, 0)

    adas_write_pipe(pipe, 0)
    adas_write_pipe(pipe, 0)
    adas_write_pipe(pipe, 1)

    return ircode
