def c5dplr(npix=100, wvmin=1.0, wvmax=1.0, wvcomp=[0.0], emcomp=[0.0],
           tev=50.0, amss=2.0):
    """
      PURPOSE    :  Adds Doppler broadening to a set of discrete emissivity components
                    over a linear pixel space.

      emiss = c5dplr(npix=1024, wvmin=100.0, wvmax=500.0, wvcomp=wc, emcomp=ec, tev=1.0e3, amss=58.9)

                    NAME         TYPE     DETAILS
      REQUIRED   :  npix         int      Number of pixels in range
                    wvmin        float    Minimum wavelength (A)
                    wvmax        float    Maximum wavelength (A)
                    wvcomp()     float    Wavelength of component
                    emcomp()     float    Emissivity of component
                    tev          float    Plasma ion temperature (eV)
                    amss         float    Atomic mass of element in plasma

      RETURNS       doppler()    float    Doppler broadened feature
                                            1st dimension : npix

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    20-11-2012

    """

    # External routines required

    from numpy import asarray, zeros
    from .c5dplr_ import c5dplr

    # Examine inputs

    if wvmin == wvmax:
        raise Exception('A wavelength range is required')

    wvcomp = asarray(wvcomp)
    emcomp = asarray(emcomp)

    if len(wvcomp) == 0 or len(emcomp) == 0 or len(wvcomp) != len(emcomp):
        raise Exception('Problem in specification of components')

    ncomp = len(wvcomp)

    doppler = asarray(zeros(npix, float))

    doppler = c5dplr(npix, wvmin, wvmax, ncomp, wvcomp, emcomp, tev, amss, doppler)

    return doppler
