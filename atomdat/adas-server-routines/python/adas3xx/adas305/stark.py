def stark(beam=None, plasma=None, bfield=None, efield=None, obs=None,
          nlower=2, nupper=3, pop_mul=1.0):
    """
      PURPOSE    :  Calculates the components of the Hydrogen Stark feature.
                    If n-n' is not supplied Balmer-alpha (3-2) is assumed.
    
      REQUIRED   :  beam{}      dictionary    with the beam details:
                                              mass    : H beam mass 
                                              energy  : in eV/amu
                                              te      : in eV
                                              density : in cm-3
                                              dc_x    : direction cosine for velocity x-component
                                              dc_y    : y
                                              dc_y    : z
                    plasma      dictionary    with the plasma details:
                                              mass    : H mass in plasma
                                              te      : in eV
                                              density : in cm-3
                                              Zeff    : effective charge
                    bfield      dictionary    with the magnetic field details:
                                              value   : in Tesla        
                                              dc_x    : direction cosine for x-component
                                              dc_y    : y
                                              dc_y    : z
                    efield      dictionary    with the magnetic field details:
                                              value   : in Volts       
                                              dc_x    : direction cosine for x-component
                                              dc_y    : y
                                              dc_y    : z
                    obs         dictionary    with the line of sight details:
                                              dc_x    : direction cosine for x-component
                                              dc_y    : y
                                              dc_y    : z
                                              sigma   : specific sigma polarisation 
                                                        intensity multiplier
                                              pi      : specific pi polarisation 
                                                        intensity multiplier
                    n_lower     integer      lower principal quantum number (1,2 or 3)
                    n_upper     integer      upper principal quantum number (4,3 or 2)
      OPTIONAL   :  pop_mul     float        re-normalization factor of upper level,
                                             usually from an adf22/bmp file; defaults to 1.0

      RETURNS    :  wave_comp   float()     Wavelength components of Stark feature (A)
                    emiss_comp  float()     Associated emission components (ph cm3 s-1)

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    20-11-2012

    """

    from numpy import asarray, zeros
    from .stark_ import stark

    # make sure something is set for all inputs

    if beam is None:
        raise Exception('Beam parameters are missing')
    if plasma is None:
        raise Exception('Plasma parameters are missing')
    if bfield is None:
        raise Exception('Magnetic field parameters are missing')
    if efield is None:
        raise Exception('Electric field parameters are missing')
    if obs is None:
        raise Exception('Observation parameters are missing')

    amdeut = beam['mass']
    bener  = beam['energy']
    dv1    = beam['dc_x']
    dv2    = beam['dc_y']
    dv3    = beam['dc_z']
    densb  = beam['density']

    amss   = plasma['mass']
    te     = plasma['te']
    zeff   = plasma['zeff']
    dens   = plasma['dens']

    bmag   = bfield['value']
    db1    = bfield['dc_x']
    db2    = bfield['dc_y']
    db3    = bfield['dc_z']

    emag   = efield['value']
    de1    = efield['dc_x']
    de2    = efield['dc_y']
    de3    = efield['dc_z']

    do1    = obs['dc_x']
    do2    = obs['dc_y']
    do3    = obs['dc_z']
    polo   = obs['sigma']
    polp   = obs['pi']

    # Balmer-alpha default

    nl     = nlower
    nu     = nupper

    popu   = pop_mul


    # set max of 300 components

    ndcomp = 300

    wvcomp = asarray(zeros(ndcomp, float))
    emcomp = asarray(zeros(ndcomp, float))

    ncomp, wvcomp,emcomp = stark(amdeut,amss,
                                 bener,dv1,dv2,dv3,densb,
                                 bmag,db1,db2,db3,
                                 emag,de1,de2,de3,
                                 do1,do2,do3,polo,polp,
                                 dens,te,zeff,
                                 nu,nl,popu, wvcomp, emcomp)

    return wvcomp[0:ncomp], emcomp[0:ncomp]
