def run_adas310(adf01  = None,
                adf18  = None,
                izimp  = [0],
                amimp  = [0.0],
                frimp  = [0.0],
                te     = [0.0],
                tp     = [0.0],
                dens   = [0.0],
                denp   = [0.0],
                bmeng  = [0.0],
                files  = None,
                tref   = 0.0,
                dref   = 0.0,
                bref   = 0.0,
                iz0    = 1,
                iz1    = 1,
                bsim   = 1.0,
                ts     = 8617.0,
                wdil   = 0.0,
                cion   = 1.0,
                cpy    = 1.0,
                wdpi   = 1.0e-8,
                nip    = 2,
                intd   = 3,
                iprs   = 1,
                ilow   = True,
                ionip  = True,
                nionip = 2,
                ilprs  = 1,
                ivdisp = True,
                nmin   = 1,
                nmax   = 110,
                nrep   = [ 1,  2,  3,  4,  5,  6, 7,   8,  9,  10, 12,
                          15, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110],
                bdens  = 1.0e10,
                no_ion = False,
                no_cx  = False):

    """

      PURPOSE    :  Runs the adas10 beam stopping generation code as a python function

      res = run_adas310(adf01='qcx#h0_old#c6.dat', iz0, .... , files=files)

                    NAME       TYPE     DETAILS
      REQUIRED   :  adf01      str      charge exchange cross section file
                    adf18      str      expansion file name
                    izimp      int()    impurity atomic number
                    amimp      float()  impurity mass
                    frimp      float()  impurity fraction - values between 0-1.0 but
                                        the fractions are to the total value
                    te         float()  electron temperatures requested (eV)
                    tp         float()  ion temperatures requested (eV)
                    dens       float()  electron densities requested (cm-3)
                    denp       float()  ion densities requested (cm-3)
                    bmeng      float()  beam energies requested (eV/amu)
                    files      dict     {'adf21'   - stopping coefficient
                                         'adf26'   - population dataset
                                         'crmat'   - collisional-radiative matrix
                                         'repmap'  - mapping between n-representations
                                         'logfile' - summary of the run

      OPTIONAL   :  iz0        int      nuclear charge of beam species (default =1)
                    iz1        int      beam species recombining ion charge
                                        ie beam charge + 1 (default = 1)
                    bsim       float    beam species isotope mass (default = 1.0)
                    ts         float    radiation field temperature (default = 8617eV)
                    wdil       float    external radiation field dilution factor
                                        (default = 0.0)
                    cion       float    multiplier of ground level e-impact
                                        ionisation rate (default = 1.0)
                    cpy        float    multiplier of ground level e-impact
                                        excitation rate (default = 1.0)
                    wdpi       float    external radiation field dilution
                                        factor for photo-ionisation
                                        (default = 1.0e-8)
                    nip        int      range of delta n for impact parameter
                                        cross sections (default = 2)
                    intd       int      order of Maxwell quadrature for
                                        cross sections (default = 3)
                    iprs       int      above nip => 0 use van Regemorter,
                                                  => 1 use Percival-Richards
                                        (default = 1)
                    ilow       boolean  True  => access special low level data
                                        False => do not use low level data
                                        (default = True)
                    ionip      boolean  True  => include ion impact collisions
                                        False => do not
                                        (default = True)
                    nionip     int      Delta n for ion impact excitation
                                        cross sections (default = 2)
                    ilprs      int      0 => Vainshtein for ion impacr (default)
                                        1 => use Lodge-Percival-Richards
                                        (default = 1)
                    ivdisp     boolean  True  => use beam energy in calculation
                                                 of cross section
                                        False => do not
                                        (default = True)
                    nmin       int      minimum representative shell  (default = 1)
                    nmax       int      maximunm representative shell (default = 110)
                    nrep       int ()   representative n-shells (default as above)
                    bdens      float    beam density (cm-3)
                    tref       float    reference electron temperatures (eV)
                                        if not given use middle value of te
                    dref       float    reference density (electron) (cm-3)
                                        if not given use middle value of dens
                    bref       float    reference beam energy (eV/amu)
                                        if not given use middle value of bref
                    no_ion     boolean  True  => switch OFF the ion impact contribution.
                                        False => use ion impact (default is True)
                    no_cx      boolean  True  => switch off CX contribution.
                                        False => use cx in calculation (default is True)

      RETURNS    :  ircode     int      success code
                                          0 => successful
                                          1 => there was a problem

      NOTES      : There are a lot of inputs, with most of the control parameters
                   having sensible defaults. The choice of target elements and
                   plasma conditions are set to 0.0 and must be supplied for
                   the routine to work properly.

                   The output of the routine is the set of files.

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
           1.1     16-04-2019

    """

#------------------------------------------------------------------------------

    # External routines required

    import os
    import subprocess
    import random
    import string
    from datetime import datetime
    from numpy import asarray, zeros, atleast_1d
    from adaslib import adas_write_pipe, i4indf, xxuser

    # Initialisation and setup

    EV_TO_K = 11605.4

    ircode = 0
    d      = datetime.now()
    today  = d.strftime("%d/%m/%y")
    name   = xxuser()
    rand   = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(8)])

    dslpath = os.getenv('ADASCENT')

    fortran_code = os.getenv('ADASFORT') + '/adas310.out'


    # Examine inputs - control parameters (optional) inputs

    a310_iz0   = int(iz0)
    a310_iz1   = int(iz1)
    a310_bsim  = float(bsim)
    a310_ts    = float(ts)
    a310_wdil  = float(wdil)
    a310_cion  = float(cion)
    a310_cpy   = float(cpy)
    a310_wdpi  = float(wdpi)
    a310_nip   = int(nip)
    a310_intd  = int(intd)
    a310_iprs  = int(iprs)

    if ilow:
        a310_ilow = 1
    else:
        a310_ilow = 0
    if ionip:
        a310_ionip = 1
    else:
        a310_ionip = 0

    a310_nionip = int(nionip)
    a310_ilprs  = int(ilprs)

    if ivdisp:
        a310_ivdisp = 1
    else:
        a310_ivdisp = 0

    a310_nmin = int(nmin)
    a310_nmax = int(nmax)
    a310_nrep = atleast_1d(asarray(nrep, int))

    a310_bdens = float(bdens)

    imax = a310_nrep.size

    if no_ion:
        lnoion = 1
    else:
        lnoion = 0

    if no_cx:
        lnocx = 1
    else:
        lnocx = 0

    if a310_iz1 == 0.0:
        raise Exception('Recombining charge cannot be zero')


    # Examine inputs - ADAS files

    if adf01 == None:
        raise Exception('A valid CX cross section file (adf01) is required')
    if (os.path.isfile(adf01) and os.access(adf01, os.R_OK)) == False:
        raise Exception('A valid CX cross section file (adf01) is required')
    a310_adf01 = adf01

    if adf18 == None:
        raise Exception('A valid expansion file (adf18) is required')
    if (os.path.isfile(adf18) and os.access(adf01, os.R_OK)) == False:
        raise Exception('A valid expansion file (adf18) is required')
    a310_adf18 = adf18


    # Examine inputs - plasma parameters

    a310_te    = atleast_1d(asarray(te, float))
    a310_tp    = atleast_1d(asarray(tp, float))
    a310_dens  = atleast_1d(asarray(dens, float))
    a310_denp  = atleast_1d(asarray(denp, float))
    a310_bmeng = atleast_1d(asarray(bmeng, float))

    if a310_te.sum() == 0.0:
        raise Exception('Non-zero electron temperatures are required input')
    if a310_tp.sum() == 0.0:
        raise Exception('Non-zero ion temperatures are required input')
    if a310_dens.sum() == 0.0:
        raise Exception('Non-zero electron densities are required input')
    if a310_denp.sum() == 0.0:
        raise Exception('Non-zero ion densities are required input')
    if a310_bmeng.sum() == 0.0:
        raise Exception('Non-zero beam energies are required input')

    len_te   = a310_te.size
    len_tp   = a310_tp.size
    len_dens = a310_dens.size
    len_denp = a310_denp.size

    ntemp  = min([len_te, len_tp])
    ndens  = min([len_dens, len_denp])
    nbmeng = a310_bmeng.size


    # Examine inputs - reference values. Use value in te/dens/bmeng closest to requested
    #                                  value or the middle one.

    if tref == 0.0:
        a310_tref = a310_te[ntemp // 2]
        print('Using the middle temperature as reference')
    else:
        ind = i4indf(a310_te, tref)
        a310_tref = a310_te[ind]

    if dref == 0.0:
        a310_dref = a310_dens[ndens // 2]
        print('Using the middle density as reference')
    else:
        ind = i4indf(a310_dens, dref)
        a310_dref = a310_dens[ind]
        a310_dpref = a310_denp[ind]

    if bref == 0.0:
        a310_bref = a310_bmeng[nbmeng // 2]
        print('Using the middle beam energy as reference')
    else:
        ind = i4indf(a310_bmeng, bref)
        a310_bref = a310_bmeng[ind]


    # Examine inputs - impurity parameters

    a310_izimp  = atleast_1d(asarray(izimp, int))
    a310_amimp  = atleast_1d(asarray(amimp, float))
    a310_frimp  = atleast_1d(asarray(frimp, float))

    if a310_izimp.sum() == 0:
        raise Exception('Fuel/impurity atomic number is a required input')
    if a310_amimp.sum() == 0.0:
        raise Exception('Fuel/impurity atomic mass is a required input')
    if a310_frimp.sum() == 0.0:
        raise Exception('Fuel/impurity fraction is a required input')

    nimp = a310_izimp.size


    # Launch fortran and interact with it

    pipe = subprocess.Popen(fortran_code,
           shell=False, bufsize=0,
           stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)


    # First panel (input)

    adas_write_pipe(pipe, 0)                  # not batch mode
    adas_write_pipe(pipe, today)

    adas_write_pipe(pipe, a310_iz0)
    adas_write_pipe(pipe, a310_iz1)
    adas_write_pipe(pipe, a310_adf18)
    adas_write_pipe(pipe, a310_adf01)

    adas_write_pipe(pipe, dslpath)

    adas_write_pipe(pipe, a310_ts*EV_TO_K)    # convert to K
    adas_write_pipe(pipe, a310_wdil)
    adas_write_pipe(pipe, a310_cion)
    adas_write_pipe(pipe, a310_cpy)
    adas_write_pipe(pipe, a310_wdpi)
    adas_write_pipe(pipe, a310_bsim)
    adas_write_pipe(pipe, a310_nip)
    adas_write_pipe(pipe, a310_intd)
    adas_write_pipe(pipe, a310_iprs)
    adas_write_pipe(pipe, a310_ilow)
    adas_write_pipe(pipe, a310_ionip)
    adas_write_pipe(pipe, a310_nionip)
    adas_write_pipe(pipe, a310_ilprs)
    adas_write_pipe(pipe, a310_ivdisp)

    adas_write_pipe(pipe, 1)                  # noscan = 1
    adas_write_pipe(pipe, nimp)
    for j in range(nimp):
        adas_write_pipe(pipe, a310_izimp[j])
        adas_write_pipe(pipe, a310_amimp[j])
        adas_write_pipe(pipe, a310_frimp[j])

    adas_write_pipe(pipe, a310_nmin)
    adas_write_pipe(pipe, a310_nmax)
    adas_write_pipe(pipe, imax)
    for j in range(imax):
        adas_write_pipe(pipe, a310_nrep[j])
        adas_write_pipe(pipe, 0.0)

    adas_write_pipe(pipe, a310_dref)
    adas_write_pipe(pipe, a310_dpref)
    adas_write_pipe(pipe, a310_tref * EV_TO_K)

    adas_write_pipe(pipe, ndens)
    for j in range(ndens):
        adas_write_pipe(pipe, a310_dens[j])
        adas_write_pipe(pipe, a310_denp[j])

    adas_write_pipe(pipe, ntemp)
    for j in range(ntemp):
        adas_write_pipe(pipe, a310_te[j] * EV_TO_K)
        adas_write_pipe(pipe, a310_tp[j] * EV_TO_K)

    adas_write_pipe(pipe, a310_bref)
    adas_write_pipe(pipe, nbmeng)
    for j in range(nbmeng):
        adas_write_pipe(pipe, a310_bmeng[j])

    adas_write_pipe(pipe, a310_bdens)


    # Set up values not exposed to the user

    mxcor     = 20
    mxeps     = 10
    mxdef     = 10
    cor       = asarray(zeros(mxcor), float)
    epsil     = asarray(zeros(mxeps), float)
    fij       = asarray(zeros(mxeps), float)
    wij       = asarray(zeros(mxeps), float)
    defect    = asarray(zeros(mxdef), float)
    jcor      = 1
    cor[0]    = 0.0
    jmax      = 1
    epsil[0]  = 0.75
    fij[0]    = 0.0
    wij[0]    = 0.0
    jdef      = 1
    defect[0] = 0.0

    adas_write_pipe(pipe, jcor)
    for j in range(mxcor):
        adas_write_pipe(pipe, cor[j])

    adas_write_pipe(pipe, jmax)
    for j in range(mxeps):
        adas_write_pipe(pipe, epsil[j])
        adas_write_pipe(pipe, fij[j])
        adas_write_pipe(pipe, wij[j])

    adas_write_pipe(pipe, jdef)
    for j in range(mxdef):
        adas_write_pipe(pipe, defect[j])


    # Control options

    adas_write_pipe(pipe, lnoion)
    adas_write_pipe(pipe, lnocx)


    # Output options - produce all possible files and adjust at end

    adas_write_pipe(pipe, 'Generated by run_adas310')
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 1)
    adas_write_pipe(pipe, 1)

    adas_write_pipe(pipe, 'temp1.0'+rand)
    adas_write_pipe(pipe, 'temp1.1'+rand)
    adas_write_pipe(pipe, 'temp1.2'+rand)
    adas_write_pipe(pipe, 'temp1.3'+rand)
    adas_write_pipe(pipe, 'temp1.4'+rand)

    line = pipe.stdout.readline()
    res = int(line)
    if line == 1:
        raise Exception('A problem has occurred with file opening')

    line = pipe.stdout.readline()
    nloops = int(line)

    for k in range(nloops):
        line = pipe.stdout.readline()
        i1 = int(line)
        line = pipe.stdout.readline()
        i2 = int(line)
        itotal = i1 * i2 * 4
        info = 'run_adas310 (loop ' + "{:2d}".format(k) + ' of ' + "{:2d}".format(nloops) + ')'
        for i in range(itotal):
            line = pipe.stdout.readline()
            i3 = int(line)


    # final signal from fortran

    line = pipe.stdout.readline()
    res = int(line)
    print('end ', res)


    # Append comments to output files (just 1, 2 and 4)

    comments = ['C--------------------------------------------------------------------',
                'C',
                'C  Code      : run_adas310',
                'C  Producer  : ' + name,
                'C  Date      : ' + today,
                'C',
                'C--------------------------------------------------------------------']

    with open('temp1.1'+rand, 'a') as f:
        for line in comments:
            f.write(line + '\n')
    with open('temp1.2'+rand, 'a') as f:
        for line in comments:
            f.write(line + '\n')
    with open('temp1.3'+rand, 'a') as f:
        for line in comments:
            f.write(line + '\n')
    with open('temp1.4'+rand, 'a') as f:
        for line in comments:
            f.write(line + '\n')


    # Assign to user supplied output files or delete

    if 'adf21' in files:
        os.rename('temp1.4' + rand, files['adf21'])
    else:
        os.remove('temp1.4' + rand)

    if 'adf26' in files:
        os.rename('temp1.1' + rand, files['adf26'])
    else:
        os.remove('temp1.1' + rand)

    if 'crmat' in files:
        os.rename('temp1.2' + rand, files['crmat'])
    else:
        os.remove('temp1.2' + rand)

    if 'repmap' in files:
        os.rename('temp1.3' + rand, files['repmap'])
    else:
        os.remove('temp1.3' + rand)

    if 'logfile' in files:
        os.rename('temp1.0' + rand, files['logfile'])
    else:
        os.remove('temp1.0' + rand)


    return ircode
