       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS809 **********************
C
C  VERSION:  1.1
C
C  PURPOSE:  TO  GRAPH  AND INTERPOLATE  SELECTED  DATA  FROM  MASTER
C            CONDENSED FILES  CONTAINING ELECTRON  IMPACT  RATE  DATA
C            (GAMMA VALUES) AS A FUNCTION OF TEMPERATURE.
C
C            IT ALLOWS THE EXAMINATION AND DISPLAY OF THE DATA FOR  A
C            SINGLE SELECTED ELECTRON TRANSITION AS FOLLOWS:
C
C            TABULATED VALUES FOR THE SELECTED TRANSITION ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE, FROM WHICH INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF REQUESTED A MINIMAX
C            POLYNOMIAL  IS FITTED THROUGH THE  DATA  AND  GRAHICALLY
C            DISPLAYED. THE MINIMAX COEFFICIENTS AND THE ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE ARE OUTPUT
C            TO HARDCOPY.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.<SE>LIKE.DATA(<MEMBER>)'
C
C            WHERE, <SE> = ELEMENT SYMBOL FOR ISO-ELECTRON SERIES
C                          (ONE OR TWO CHARACTERS)
C
C            THE <MEMBER> NAME IS CONSTRUCTED AS FOLLOWS:
C
C               <INITIALS OF DATA SOURCE><YEAR><ELEMENT SYMBOL><*>
C
C               E.G. HPS89BE,  BKT1988C
C
C               <INITIALS OF DATA SOURCE> - E.G. HPS = HP SUMMERS
C
C               <YEAR> - THIS MAY OR MAY NOT BE ABBREVIATED
C
C               IF <ELEMENT SYMBOL> IS PRESENT =>
C                                       SPECIFIC  ION  FILE.  ONE
C                                       ELEMENT ONLY.
C                                           E.G. BE = BERYLIUM
C                                                C  = CARBON
C               IF <ELEMENT SYMBOL> IS MISSING =>
C                                       GENERAL Z EXCITATION FILES
C                                       CONTAINING BETWEEN 2 AND 7
C                                       ELEMENTS (INCLUSIVE). THIS
C                                       PROGRAM WILL  NOT  ANALYSE
C                                       THESE  FILES,  THEY   MUST
C                                       PASS THROUGH 'SPFMODM2' OR
C                                       AN    EQUIVALENT   PROGRAM
C                                       FIRST.
C
C               <*> - FURTHER EXTENSIONS TO MEMBER NAME MAY BE PRESENT,
C                    THESE MAY REFER TO DATA SET GENERATION STATISTICS.
C
C           EACH VALID MEMBER HAS ITS DATA REPRESENTED IN AN ABBREVIATED
C           FORM WHICH OMITS THE "D" OR "E" EXPONENT SPECIFIER.
C           E.G. 1.23D-06 OR 1.23E-06 IS REPRESENTED AS 1.23-06
C                6.75D+07 OR 6.75E+07 IS REPRESENTED AS 6.75+07
C
C           THEREFORE THE FORM OF EACH REAL NUMBER IN THE DATA SET IS:
C                          N.NN+NN OR N.NN-NN
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C
C
C  PROGRAM:
C
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NEDIM   = PARAMETER = MAX. OF INPUT DATA FILE ENERGIES
C          (I*4)  NDTEM   = PARAMETER = MAXIMUM OF DATA CONVERSION TEMPS
C          (I*4)  NDTIN   = PARAMETER = MAX.NUMBER OF ISPF ENTERED TEMPS
C          (I*4)  NFDIM   = PARAMETER = MAX.NO. OF ENERGIES IN NUM. DIST.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NPSPL   = PARAMETER =
C                           NUMBER  OF SPLINE INTERPOLATED GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED   GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  ICSTMX  = PARAMETER = 11 =
C                           MAXIMUM SIZE OF NOMENCLATURE STRING OUTPUT
C                           FOR PANELS AND PASSING DATASETS.(SEE CSTRGB)
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ      =  RECOMBINED ION CHARGE READ FROM INPUT FILE
C          (I*4)  IZ0     =         NUCLEAR CHARGE READ FROM INPUT FILE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE READ FROM INPUT FILE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY INDEX
C                                            LEVELS.
C          (I*4)  ITRAN   = INPUT DATA FILE: TOTAL NUMBER OF TRANSITIONS.
C          (I*4)  ITRN    = INPUT DATA FILE: SELECTED ELECTRON IMPACT
C                                     TRANSITION INDEX FOR ANALYSIS.
C                                     (FOR USE WITH 'IETRN()', 'IE1A()'
C                                      'IE2A()' AND 'AVALE()' ARRAYS)
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                                     PAIRS FOR THE SELECTED TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C          (I*4)  IFOUT   = 1 => 'TINE(ARRAY)' UNITS: KELVIN
C                         = 2 => 'TINE(ARRAY)' UNITS: EV
C                         = 3 => 'TINE(ARRAY)' UNITS:REDUCED TEMPERATURE
C          (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C          (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C          (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C          (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C          (R*8)  R8TCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  EUPPER  = SELECTED TRANSITION: UPPER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  ELOWER  = SELECTED TRANSITION: LOWER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C          (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  AA      = SELECTED TRANSITION A-VALUE (SEC-1)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMINK   = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  XMAXK   = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE COEFF. (CM**3/S)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE COEFF. (CM**3/S)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
CA         (L*4)  L2GRAPH = .TRUE.  => DISPLAY GRAPH
CA                          .FALSE. => DO NOT DISPLAY GRAPH
C
CA         (L*4)  L2FILE  = .TRUE.  => WRITE DATA TO FILE
CA                          .FALSE. => DO NOT WRITE DATA TO FILE
C
CA         (C*80) SAVFIL  = NAME OF FILE TO WHICH DATA IS WRITTEN.
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*3)  TITLED  = ELEMENT SYMBOL.(INCORPORATED INTO 'TITLX')
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CX         (C*44) DSFULL1  = FULL INPUT DATA SET NAME (READ FROM ISPF)
CA         (C*80) DSFULL1  = FULL INPUT DATA SET NAME (READ FROM IDL)
C                           (INCORPORATED INTO 'TITLX')
CX         (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CA         (C*40) TITLE   = TITLE OF RUN (READ FROM IDL PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
CX         (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
CA         (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE AND  TRANSITION
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C          (I*4)  IPLA(,) = PARENT INDEX FOR CONTRIBUTION TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (R*8)  ZPLA(,) = EFF. ZETA PARAM. FOR CONTRIBUTION TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (R*8)  AVALE() = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  SCX()   = INPUT DATA FILE: X-PARAMETER SET
C          (R*8)  SCEF()  = PROGRAM: INPUT FILE CONVERSION TEMPS, OR 2/3
C                           AVERAGE ENERGY OF NUMERICAL DISTRIBUTION IF 
C                           ADF37 COMPARISON FILE IS SELECTED (KELVIN)
C          (R*8)  SCEF2() = MOST COMMON ENERGY OF DISTRIBUTION FUNC. IF 
C                           ADF37 COMPARISON FILE IS SELECTED (KELVIN)
C          (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GAMMA VALUE AT 'SCEF()'
C          (R*8)  RATE()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           EXCITATION RATE COEFF. (CM**3/S) AT 'SCEF()'
C          (R*8)  DRATE() = INPUT DATA FILE: SELECTED TRANSITION -
C                           DEEXCITATION RATE COEF.(CM**3/S) AT 'SCEF()'
C
C          (R*8)  TINE()  = ISPF ENTERED ELECTRON TEMPERATURE VALUES.
C                           (NOTE: UNITS ARE GIVEN BY 'IFOUT')
C          (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (KELVIN)
C
C          (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (KELVIN)
C          (R*8)  GAMOSA()= SPLINE INTEROPLATED GAMMA VALUE AT 'TOSA()'
C          (R*8)  ROSA()  = EXCITATION RATE COEFF.(CM**3/S) AT 'TOSA()'
C          (R*8)  DROSA() = DEEXCITATION RATE COEF.(CM**3/S) AT 'TOSA()'
C
C          (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (KELVIN)
C          (R*8)  GAMOMA()= MINIMAX GENERATED GAMMA VALUE AT 'TOMA()'
C          (R*8)  ROMA()  = EXCITATION RATE COEFF.(CM**3/S) AT 'TOMA()'
C          (R*8)  DROMA() = DEEXCITATION RATE COEF.(CM**3/S) AT 'TOMA()'
C
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPS OR 2/3
C                            AVERAGE ENERGY IF ADF37 COMPARISON FILE IS 
C                            SELECTED
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => ELECTRON IMPACT   TRANSITION
C                           'P' => PROTON   IMPACT   TRANSITION
C                           'H' => CHARGE   EXCHANGE RECOMBINATION
C                           'R' => FREE     ELECTRON RECOMBINATION
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES.
C          (C*22) STRGA() = NOMENCLATURE FOR LEVEL 'IA()' INCLUDES:
C                           'CSTRGA()' AND QUANTUM NUMBERS.
C	   (I*4)  IFIRST  = FIRST NON-BLANK CHARCTER IN FILENAME
C
C	   (I*4)  ILAST   = LAST  NON-BLANK CHARCTER IN FILENAME
C
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
C
C NOTE:    INPUT TEMPERATURES 'TINE()' ARE CONVERTED TO KELVIN 'TOA()'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          H9SPF0     ADAS      GATHERS INPUT FILE NAMES VIA IDL SCREENS
C          BIISPF     ADAS      GATHERS USERS VALUES VIA IDL SCREENS
C 	   H9ISPF     ADAS      OUTPUT/DISPLAY OPTIONS SELECTION SCREEN
C          H9TRAN     ADAS      GATHERS SELECTED TANSITION PARAMETERS
C          H9SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          H9OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING IDL
C          H9OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          H9RATE     ADAS      CALCULATES RATE COEFFICIENTS
C          BXSETP     ADAS      PASSES VARIABLES BACK TO MAIN IDL ROUTINES
C          BXCSTR     ADAS      EXTRACT NON-BLANK CHARS. FROM STRING
C          HXTTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO KELVIN
C	   XXSLEN     ADAS      FINDS POSITIONS OF SPACES IN STRING
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION: CONVERT TEMP. TO KELVIN
C          xxdata_04  ADAS      gathers data from adf04 input file
C          xxdata_37  ADAS      gathers data from adf37 input file
C
C  AUTHOR:  HUGH SUMMERS (UNIV. OF STRATHCLYDE) EXT.4196
C
C  DATE:    30 NOVEMBER 2001
C
C  MODIFICATION HISTORY:
C
C  DATE:   30/11/01			VERSION: 1.1
C  MODIFIED: HUGH SUMMERS
C		- FIRST RELEASE
C
C  DATE:   22/11/04
C  MODIFIED: PAUL BRYANS
C		- ALTERED TO ACCOMODATE NON-MAXWELLIAN DISTRIBUTIONS
C  DATE:   26/11/04
C  MODIFIED: ALLAN WHITEFORD
C		- TEMPERATURE GRID FOR NEUTRLS ADDED
C  DATE:   04/04/05
C  MODIFIED: ALLAN WHITEFORD
C		- CHANGED LSS04A FROM A SCALAR TO AN ARRAY OF
C                 SIZE (NDLEV,NDMET)
C		- SENT THE STRING 'ONE' BACK TO IDL.
C
C-----------------------------------------------------------------------
      INTEGER     NDLEV        , NDTRN        , NDTEM     , NDTIN      ,
     &            MAXDEG       , NEDIM        , NFDIM     , IUNT37
      INTEGER     IUNT07       , IUNT10       , IUNT11    , PIPEIN
      INTEGER     NPSPL        , NMX          , PIPEOU
      INTEGER     L1           , ICSTMX	      , IFIRST    , ILAST
      INTEGER     NDMET        , NDQDN        , NVMAX     , 
     &		  IORB         , IADFTYP      , ITIEACTN
C-----------------------------------------------------------------------
      LOGICAL     LMLOG
C-----------------------------------------------------------------------
      PARAMETER ( NDLEV = 5000 , NDTRN  = 1000000, NEDIM = 50 ,
     &            NDTEM = 50   , NDTIN  = 50   , MAXDEG = 19 ,
     &            NFDIM = 200  , NDMET  = 4    , NDQDN  = 6  , 
     &            NVMAX = ndtem )
      PARAMETER ( IUNT07 =   7 , IUNT10 =  10 , IUNT11 = 11 )
      PARAMETER ( IUNT37 =  37 )
      PARAMETER ( NPSPL  = 100 , NMX    = 100 )
      PARAMETER ( L1     =   1 , ICSTMX =  11 )
      PARAMETER ( PIPEIN = 5   , PIPEOU = 6 )
C-----------------------------------------------------------------------
      PARAMETER ( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER     I4UNIT       , ISTOP        , I         , IT        
      INTEGER     IZ           , IZ0          , IZ1       ,
     &            IL           , ITRAN        , ITRN      , MAXLEV     ,
     &            ICNTE        , ICNTP        , ICNTR     , ICNTH      ,
     &            ICNTI        , ICNTL        , ICNTS     , 
     &            NV           , NVT          , MAXT      , KPLUS1     ,
     &            IFOUT        , IFORM        ,
     &            IUPPER       , ILOWER       , LUPPER    , LLOWER
      INTEGER     IFINT        , ITYPE        , ITYPT     , ILINR      ,
     &            IESCL        , IC  
      INTEGER     DTYPE        , N_EN         , NTEMP
      INTEGER	  NPL          , NPLA(NDLEV)  , IPLA(NDMET,NDLEV)      ,
     &            icateg       , nenerg       , nblock    , nform1     ,
     &            nform2
C-----------------------------------------------------------------------
      REAL*8      R8TCON
      REAL*8      EUPPER       , ELOWER       , WUPPER    , WLOWER     ,
     &            BWNO         , AA           , EVT
      REAL*8      XMIN         , XMAX         , XMINK     , XMAXK      ,
     &            YMIN         , YMAX
      REAL*8      TOLVAL
      REAL*8      KAP_VAL      , DRU_VAL      , LIMIT
      REAL*8      QDORB((NDQDN*(NDQDN+1))/2)  , QDN(NDQDN)   ,
     &		  BETH(NDTRN)  , BWNOA(NDMET) , PRTWTA(NDMET),
     &            ZPLA(NDMET,NDLEV)           ,
     &            param1       , param2(2)    , ea(nvmax,nfdim)        ,
     &            teff(ndtem)  , mode(ndtem)  , fa(nvmax,nfdim)        ,
     &            median(ndtem), AUGA(NDTRN)  , WVLA(NDLEV)            ,
     &            rtions(ndtem), rtrecm(ndtem)
C-----------------------------------------------------------------------
      LOGICAL     OPEN10       , OPEN11       , LPEND     , 
     &            LOSEL        , LFSEL        , AREP      , A2FILE
      LOGICAL     LGRAPH       , L2FILE       , LREP      , OPEN07
      LOGICAL     LDEF1        , LEXIST
      LOGICAL     OPEN37
      LOGICAL     LQDORB((NDQDN*(NDQDN+1))/2)
      LOGICAL     LPRN        , LCPL           , LORB       , LBETH
      LOGICAL     LETYP       , LPTYP          , LRTYP      , LHTYP   ,
     &            LITYP       , LSTYP          , LLTYP
      LOGICAL     LTIED(NDLEV), LBSETA(NDMET)
C-----------------------------------------------------------------------
      CHARACTER   REP*3        , DATE*8       , DSFULL1*80, DSFULL2*80 ,
     &            TITLED*3  , TITLE*40   ,
     &            F1TYPE*6     , F2TYPE*6    , header*80 ,
     &            TITLM*80     , TITLX*120    , ADF37*80
      CHARACTER   SAVFIL*80    , ADFFIL*80    , TCHR*1
      CHARACTER   CPLA(NDLEV)*1 , CPRTA(NDMET)*9
      character   filnam(nfdim)*120 , filout*120 , calgeb(ndtem,4)*25  ,
     &            ealgeb(ndtem)*25
C-----------------------------------------------------------------------
      INTEGER     IA(NDLEV)    , ISA(NDLEV)   , ILA(NDLEV)  ,
     &            I1A(NDTRN)   , I2A(NDTRN)
      INTEGER     IETRN(NDTRN) , IPTRN(NDTRN) ,
     &            IRTRN(NDTRN) , IHTRN(NDTRN) ,
     &            IiTRN(NDTRN) , IlTRN(NDTRN) ,
     &            IsTRN(NDTRN) , 
     &            IE1A(NDTRN)  , IE2A(NDTRN)  ,
     &            IP1A(NDTRN)  , IP2A(NDTRN)  ,
     &            Ia1A(NDTRN)  , Ia2A(NDTRN)  ,
     &            Il1A(NDTRN)  , Il2A(NDTRN)  ,
     &            Is1A(NDTRN)  , Is2A(NDTRN)  ,
     &            nplr         , npli
C-----------------------------------------------------------------------
      REAL*8      XJA(NDLEV)   , WA(NDLEV)    ,
     &            AVAL(NDTRN)  , AVALE(NDTRN)
      REAL*8      COEF(MAXDEG+1)              ,
     &            SCX(NEDIM)   , OMA(NEDIM)   , SCEF(NDTEM)  , 
     &            GAMMA(NDTEM) , RATE(NDTEM)  , DRATE(NDTEM) ,
     &            PRATE(4,NDTEM) , TVA(NDTEM) ,
     &            TVA2(NDTEM)
      REAL*8      TINE(NDTIN)  , SCEF2(NDTEM) , SCEFS(14) , NCEFS(14) ,
     &            TOA(NDTIN)   , TOSA(NPSPL)  , TOMA(NMX)   ,
     &            GAMOSA(NPSPL), GAMOMA(NMX)  ,
     &            ROSA(NPSPL)  , ROMA(NMX)    ,
     &            DROSA(NPSPL) , DROMA(NMX)   ,
     &            PROSA(4,NPSPL) , PROMA(4,NMX),
     &            GAMDNOSA(NPSPL) , GAMDNOMA(NPSPL)
      REAL*8      TSCEF(NDTEM,3)      , OMGA(NEDIM,NDTRN)
      REAL*8      GAMMAUP(NDTEM)      , GAMMADN(NDTEM), TSCEF2(NDTEM,3),
     &            SCOMUP(NDTEM,NDTRN ), SCOMDN(NDTEM,NDTRN),
     &            EN(NDTEM,NFDIM)     , F(NDTEM,NFDIM)     ,
     &            USCEF(NDTEM)        , 
     &            UTVA(NDTEM)         , UTVA2(NDTEM)
      real*8      xsec(nedim), zeta, ip, ip_ev
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(npspl)  , LSS04A(NDLEV,NDMET)
C-----------------------------------------------------------------------
      CHARACTER   TCODE(NDTRN)*1 , CSTRGA(NDLEV)*18 , CSTRGB(NDLEV)*18 ,
     &            STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      DATA MAXT   /0/
      DATA OPEN10 /.FALSE./ , OPEN07 /.FALSE./,
     &     OPEN37 /.FALSE./
      DATA XMIN   /0/       , XMAX   /0/
C-----------------------------------------------------------------------
c      DATA TVAS   /1.00D+00,2.00D+00,5.00D+00,1.00D+01,2.00D+01,5.00D+01
c     &            ,1.00D+02,2.00D+02,5.00D+02,1.00D+03,2.00D+03,5.00D+03
c     &            ,1.00D+04,2.00D+04/
      DATA scefs /1.00D+02,2.00D+02,5.00D+02,1.00D+03,2.00D+03,5.00D+03
     &           ,1.00D+04,2.00D+04,5.00D+04,1.00D+05,2.00D+05,5.00D+05
     &           ,1.00D+06,2.00D+06/
      DATA ncefs /1.70D+03,3.10D+03,5.60D+03,1.00D+04,1.70D+04,3.10D+04
     &           ,5.60D+04,1.00D+05,1.70D+05,3.10D+05,5.60D+05,1.00D+06
     &           ,1.70D+06,3.10D+06/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
	TITLE='NULL'
	TITLM='NULL'
	TITLX='NULL'
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
 100  CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
        CLOSE(10)
        OPEN10=.FALSE.
      ENDIF
C         
      DSFULL2 = ' '
C
C-----------------------------------------------------------------------
C FILENAME NOW GATHERED FROM IDL INTERFACE
C-----------------------------------------------------------------------
C
      CALL H9SPF0( REP , DSFULL1 , DSFULL2 , F1TYPE , F2TYPE )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED:  END RUN
C-----------------------------------------------------------------------
C
      IF (REP.EQ.'YES') THEN
        GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL1
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSFULL1 , STATUS='UNKNOWN')
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET - ADF04 TYPE 1 or adf37 type 1
C-----------------------------------------------------------------------
C
C      CALL H9DATA( IUNT10 , NDLEV  , NDTRN ,
C     &             TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
C     &             IL     ,
C     &             IA     , CSTRGA , ISA   , ILA   , XJA   , WA    ,
C     &             NV     , SCX    ,
C     &             ITRAN  , MAXLEV ,
C     &             TCODE  , I1A    , I2A    , AVAL , OMGA
C     &           )
C     
      if (f1type.eq.'adf04') then
      
        CALL XXDATA_04( IUNT10 , 
     &  	        NDLEV  , NDTRN  , NDMET   , NDQDN , NVMAX ,
     &  	        TITLED , IZ     , IZ0	  , IZ1	  , BWNO  ,
     &  	        NPL    , BWNOA  , LBSETA  , PRTWTA, CPRTA ,
     &  	        IL     , QDORB  , LQDORB  , QDN	  , IORB  ,
     &  	        IA     , CSTRGA , ISA	  , ILA	  , XJA	  ,
     &  	        WA     ,
     &  	        CPLA   , NPLA   , IPLA	  , ZPLA  ,
     &  	        NV     , SCX    ,
     &  	        ITRAN  , MAXLEV ,
     &  	        TCODE  , I1A    , I2A	  , AVAL  , OMGA  ,
     &  	        BETH   ,	  
     &  	        IADFTYP, LPRN   , LCPL    , LORB  , LBETH ,
     &  	        LETYP  , LPTYP  , LRTYP   , LHTYP , LITYP ,
     &  	        LSTYP  , LLTYP  , ITIEACTN, LTIED
     &  	      )

      elseif (f1type.eq.'adf37') then
      
        call xxdata_37( iunt10 ,
     &	                nfdim  , nvmax  ,
     &			header , icateg , nenerg , nblock ,
     &			nform1 , param1 , nform2 , param2 ,
     &			ea     , fa     , teff   , mode   ,
     &			median , filnam , filout , calgeb ,
     &			ealgeb
     &		      )
      
      endif
C
C-----------------------------------------------------------------------
C REDUCED LEVEL NOMENCLATURE STRINGS TO LAST 'ICSTMX' NON-BLANK BYTES
C-----------------------------------------------------------------------
C
      CALL BXCSTR( CSTRGA , IL     , ICSTMX ,
     &             CSTRGB
     &           )
C
C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPES.
C-----------------------------------------------------------------------
C
c      CALL HXTTYP( NDTRN  ,
c     &             ITRAN  , TCODE  , I1A    , I2A   , AVAL  ,
c     &             ICNTE  , ICNTP  , ICNTR  , ICNTH ,
c     &             IETRN  , IPTRN  , IRTRN  , IHTRN ,
c     &                               IE1A   , IE2A  , AVALE ,
c     &                               IP1A   , IP2A
c     &                  )
cC     
c      IF (ITRAN.NE.ICNTE) THEN
c          WRITE(I4UNIT(-1),1002) 'ELECTRON TRANSITION COUNT',ICNTE,
c     &                           ' .NE. TOTAL TRANSITION COUNT'
c          WRITE(I4UNIT(-1),1003)
c          STOP
c      ENDIF
c
      call h9ttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &             itran  , tcode  , i1a    , i2a   , aval  ,
     &             iadftyp,
     &             icnte  , icntp  , icntr  , icnth , icnti ,
     &             icntl  , icnts  ,
     &             ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &             iltrn  , istrn  ,
     &                               ie1a   , ie2a  , avale ,
     &                               ip1a   , ip2a  ,
     &                               ia1a   , ia2a  , auga  ,
     &                               il1a   , il2a  , wvla  ,
     &                               is1a   , is2a  , lss04a 
     &           )




C
C-----------------------------------------------------------------------
C VERIFY IF COMPARATIVE DATA FILE EXISTS. IF SO OBTAIN TEMPERATURES FROM
C FILE, OTHERWISE USE THE STANDARD SET
C SET EXTENDED TEMPERATURE GRID FOR INPUT FILE CONVERSION IN KELVIN & EV
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
        CLOSE(10)
        OPEN10=.FALSE.
      ENDIF
C       
      INQUIRE ( FILE=DSFULL2, EXIST=LEXIST)
C
      IF (LEXIST) THEN
        OPEN( UNIT=IUNT10 , FILE=DSFULL2 , STATUS='UNKNOWN')
        OPEN10=.TRUE.    
        if (f2type.eq.'adf043'.or.f2type.eq.'adf044')
     &    call h9gett( iunt10 , ndlev , nvt , tva )
        if (f2type.eq.'adf37')
     &    call xxdata_37( iunt10 ,
     &	                  nfdim  , nvmax  ,
     &			  header , icateg , nenerg , nvt    ,
     &			  nform1 , param1 , nform2 , param2 ,
     &			  ea     , fa     , tva    , tva2   ,
     &			  median , filnam , filout , calgeb ,
     &			  ealgeb
     &		        )
        do 10 i=1,nvt
          scef(i)  = tva(i)*11605.4d0
	  scef2(i) = tva2(i)*11605.4d0
 10     enddo            
      else
        nvt=14
        do 20 i=1,nvt
          if (iz1.eq.1) then
            scef(i) = ncefs(i)
	  else
	    scef(i) = scefs(i)*iz1**2.d0
	  endif
	  tva(i)  = scef(i)/11605.4d0
 20     enddo
      endif
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------
C
      DO 30 IFORM=1,3
        CALL XXTCON( L1 , IFORM , IZ1 , NVT , SCEF , TSCEF(1,IFORM) )
	CALL XXTCON( L1 , IFORM , IZ1 , NVT , SCEF2, TSCEF2(1,IFORM))
 30   CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
      CALL BXSETP( IZ0    , IZ    ,
     &             NDLEV  , IL    , ICNTE ,
     &             CSTRGB , ISA   , ILA   , XJA  ,
     &             STRGA
     &           )
C
 200  CALL H9ISPF( LPEND   ,
     &             NDTIN   , IL      , NDTEM   ,
     &             NVT     , TSCEF   , TSCEF2  ,
     &             ICNTE   , IE1A    , IE2A    ,
     &             F2TYPE  , TITLE   , 
     &             ITRN    , IFOUT   ,
     &             MAXT    , TINE    ,
     &             LFSEL   , LOSEL   ,
     &             TOLVAL  ,
     &             DTYPE   , KAP_VAL , DRU_VAL ,
     &             ADF37
     &           )
C
        
      if (dtype.eq.2) then
        
	if (open37) then
          close(37)
          open37=.false.
        endif
c       
        inquire ( file=adf37, exist=lexist)	
      
        open( unit=iunt37 , file=adf37 , status='unknown')
	open37=.true.
	
        call xxdata_37( iunt37 ,
     &	                nfdim  , nvmax  ,
     &			header , icateg , n_en   , ntemp  ,
     &			nform1 , param1 , nform2 , param2 ,
     &			en     , f      , utva   , utva2  ,
     &			median , filnam , filout , calgeb ,
     &			ealgeb
     &		      )

        if (icateg.ne.2) then
	  write(i4unit(-1),1002) 'icateg =',icateg,', must equal 2'
	  write(i4unit(-1),1003)
	  stop
	endif
	
	do i=1, ntemp
	  uscef(i)  = utva(i)*11605.4d0
	enddo
        nvt = ntemp
      else
        nvt = maxt
      endif
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TINE -> TOA)
C-----------------------------------------------------------------------
C  
      CALL XXTCON( IFOUT , L1 , IZ1 , MAXT , TINE , TOA)
      DO IFORM=1,NDTIN
        TINE(IFORM) = TOA(IFORM)/11605.4d0
      ENDDO
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C  SET STANDARD PARAMETERS FOR THE MAXWELL AVERAGING                    
C-----------------------------------------------------------------------
C
      IFINT= 1                                                         
      ILINR= 1                                                         
      IESCL= 1
        
      IF (IZ1.EQ.1) THEN 
        ITYPT = 2
      ELSE
        ITYPT = 1
      ENDIF
C
C-----------------------------------------------------------------------
C  CYCLE THROUGH EACH TRANSITION IN TURN                                
C-----------------------------------------------------------------------
C
      DO 40 IC = 1, ICNTE
        TCHR = TCODE(IETRN(IC))
        READ(TCHR,'(I1)')ITYPE
	LIMIT=BETH(IC)
      
        CALL H9TRAN(   NDLEV  , NDTRN     , NEDIM  ,
     &                 IL     , IETRN(IC) , NV     ,
     &                 IA     , WA        , XJA    ,
     &                 I1A    , I2A       , AVAL   , 
     &                 OMGA   ,
     &                 IUPPER , ILOWER    , 
     &                 LUPPER , LLOWER    ,
     &                 WUPPER , WLOWER    ,
     &                 EUPPER , ELOWER    ,
     &                 AA     , OMA       
     &               )
C     
        EVT=13.6048d0*(EUPPER-ELOWER)/109737.26d0
C     
        CALL H9NTQD( NEDIM   , NDTEM   , NFDIM     ,            
     &               IFINT   , ITYPE   , ITYPT     , ILINR , IESCL  , 
     &               IETRN(IC) ,              
     &               NV      , MAXT    ,                      
     &               EVT     , SCX     , 
     &               OMA     , TINE    , 
     &               GAMMAUP , GAMMADN ,
     &               KAP_VAL , dru_val , DTYPE     ,
     &               N_EN    , EN      , F         ,
     &               NTEMP   , UTVA    ,
     &               LBETH   , LIMIT   ,
     &               nform1  , param1  , nform2    , param2 
     &             )
C
	DO 50 IT=1,NVT
          SCOMUP(IT,IETRN(IC)) = GAMMAUP(IT)
          SCOMDN(IT,IETRN(IC)) = GAMMADN(IT) 
 50     ENDDO
 40   ENDDO   

C----------------------------------------------------------------------
C  Ionisation / recombination
C----------------------------------------------------------------------

      do ic = 1,icnts
        tchr = tcode(istrn(ic))
	
	call h9trni( NDLEV  , NDTRN     , NEDIM  , ndmet ,
     &               IL     , IsTRN(IC) , NV     ,
     &               IA     , WA        , XJA    ,
     &               I1A    , I2A       , AVAL   , 
     &               OMGA   , zpla      , bwnoa  , ipla ,
     &               IUPPER , ILOWER    , 
     &               LUPPER , LLOWER    ,
     &               WUPPER , WLOWER    ,
     &               EUPPER , ELOWER    ,
     &               AA     , xsec      ,
     &               zeta   , ip
     &             )
	
	ip_ev = 13.6048d0*ip/109737.26d0
			
	call h9qd3b( ndtem , nedim   , nfdim   ,
     &	             tine  , utva    ,
     &               nv    , maxt    , wupper  , wlower  ,
     &               scx   , dtype   , kap_val , dru_val ,
     &               zeta  , ip_ev   , xsec    ,
     &               n_en  , en      , f       , ntemp   ,
     &               nform1, param1  , nform2  , param2  ,
     &               rtions, rtrecm
     &             )

	do it=1,nvt
          scomup(it,istrn(ic)) = rtions(it)
          scomdn(it,istrn(ic)) = rtrecm(it) 
        enddo

      enddo
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C IDENTIFY REQUESTED TRANSITION PARAMETERS
C-----------------------------------------------------------------------
C
      CALL H9TRAN( NDLEV  , NDTRN        , NDTEM  ,
     &             IL     , IETRN(ITRN)  , NVT    ,
     &             IA     , WA           , XJA    ,
     &             I1A    , I2A          , AVAL   ,
     &             SCOMUP , 
     &             IUPPER , ILOWER       ,
     &             LUPPER , LLOWER       ,
     &             WUPPER , WLOWER       ,
     &             EUPPER , ELOWER       ,
     &             AA     , GAMMA
     &           )

      CALL H9TRAN( NDLEV  , NDTRN        , NDTEM  ,
     &             IL     , IETRN(ITRN)  , NVT    ,
     &             IA     , WA           , XJA    ,
     &             I1A    , I2A          , AVAL   ,
     &             SCOMDN , 
     &             IUPPER , ILOWER       ,
     &             LUPPER , LLOWER       ,
     &             WUPPER , WLOWER       ,
     &             EUPPER , ELOWER       ,
     &             AA     , GAMMADN
     &           )      
C
C  MOVED XXTCON FOR CONVERTING TINE TO TOA TO JUST AFTER H9ISPF
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO KELVIN.
C-----------------------------------------------------------------------
C
      XMINK = R8TCON( IFOUT , L1 , IZ1 , XMIN )
      XMAXK = R8TCON( IFOUT , L1 , IZ1 , XMAX )
C
C-----------------------------------------------------------------------
C FIT CUBIC SPLINE AND INTERPOLATE GAMMAS FOR GRAPHICAL & TABULAR OUTPUT
C-----------------------------------------------------------------------
C
      CALL H9SPLN( NDTRN   , NDTEM  ,
     &             NDTIN   , nvt     , MAXT   , NPSPL  ,
     &             IETRN   , icnte   , DTYPE  ,
     &             ISTRN   , ICNTS   ,
     &             USCEF   , TOA     , TOSA   ,
     &             SCOMUP  ,
     &             GAMMA   , GAMOSA  ,
     &             LTRNG   , ITRN
     &           )   
    
      CALL H9SPLN( NDTRN   , NDTEM  ,
     &             NDTIN   , nvt     , MAXT   , NPSPL  ,
     &             IETRN   , icnte   , DTYPE  ,
     &             ISTRN   , ICNTS   ,
     &             USCEF   , TOA     , TOSA   ,
     &             SCOMDN  ,
     &             GAMMADN , GAMDNOSA,
     &             LTRNG   , ITRN
     &           )   
           
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED
C-----------------------------------------------------------------------
C
C
      
      IF (LFSEL) THEN
        CALL XXMNMX( LMLOG  , MAXDEG , TOLVAL ,
     &               MAXT   , toa    , GAMMA  ,
     &               NMX    , TOMA   , GAMOMA ,
     &               KPLUS1 , COEF   ,
     &               TITLM
     &             )
        CALL XXMNMX( LMLOG  , MAXDEG , TOLVAL   ,
     &               MAXT   , toa    , GAMMADN  ,
     &               NMX    , TOMA   , GAMDNOMA ,
     &               KPLUS1 , COEF   ,
     &               TITLM
     &             )     
      ENDIF
C
C-----------------------------------------------------------------------
C CONVERT GAMMA VALUES INTO (DE-)EXCITATION RATE COEFFICIENTS
C-----------------------------------------------------------------------
C
C 1) ORIGINAL <SE>LINE.DATA VALUES
C
      CALL H9RATE(  NDTEM  , MAXT   , 
     &              TOA    , GAMMA  , gammadn ,
     &              EUPPER , ELOWER ,
     &              WUPPER , WLOWER ,
     &              RATE   , DRATE
     &           )
         
C
C 2) SPLINE INTERPOLATED VALUES
C
      CALL H9RATE( NPSPL  , NPSPL  ,
     &             TOSA   , GAMOSA , gamdnosa ,
     &             EUPPER , ELOWER ,
     &             WUPPER , WLOWER ,
     &             ROSA   , DROSA
     &           )

C
C 3) MINIMAX VALUES (IF REQUESTED)
C
      IF (LFSEL) THEN
        CALL H9RATE( NMX    , NMX    , 
     &               TOMA   , GAMOMA , gamdnoma ,
     &               EUPPER , ELOWER ,
     &               WUPPER , WLOWER ,
     &               ROMA   , DROMA
     &             )
      ENDIF      
C
C-----------------------------------------------------------------------
C  CONSTRUCT DESCRIPTIVE TITLE FOR OUTPUT
C-----------------------------------------------------------------------
C
      CALL XXSLEN(DSFULL1, IFIRST, ILAST)
      WRITE (TITLX,1001) DSFULL1(IFIRST:ILAST),TITLED,IZ,LLOWER,LUPPER
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
C-----------------------------------------------------------------------
300   CONTINUE

      CALL H9SPF1( LDEF1   ,
     &             LGRAPH  , L2FILE     , A2FILE   , SAVFIL ,
     &             ADFFIL  , XMINK      , XMAXK    , YMIN   ,
     &             YMAX    , LPEND      , LREP     , AREP
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C  IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
C OUTPUT RESULTS - MOVED BEFORE GRAPHING SO THAT PAPER OUTPUT IS
C ACHIEVED EVEN WHEN MENU BUTTON IS PRESSED ON GRAPH SCREEN
C----------------------------------------------------------------------
C      
      IF (L2FILE) THEN
        IF(LREP.AND.OPEN07)THEN
          CLOSE(IUNT07)
          OPEN07 = .FALSE.
        ENDIF
        IF(.NOT.OPEN07)THEN
          OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
          OPEN07 = .TRUE.
        ENDIF
C
        CALL H9OUT0( IUNT07  , IL     , LFSEL  , LOSEL  ,
     &               TITLE   , TITLX  , TITLM  , DATE   ,
     &               IZ0     , IZ1    , IZ     , BWNO   ,
     &               MAXT    , LTRNG  , ADF37  ,
     &               TOA     , GAMMA  , DRATE  , RATE   ,
     &               AA      , LUPPER , LLOWER , EUPPER , ELOWER ,
     &               IUPPER  , ILOWER ,
     &               CSTRGB  , ISA    , ILA    , XJA    ,
     &               KPLUS1  , COEF   , DTYPE  , KAP_VAL
     &              )
      ENDIF
C 
      IF (A2FILE) THEN
        IF(AREP.AND.OPEN11)THEN
          CLOSE(IUNT11)
          OPEN11 = .FALSE.
        ENDIF
        IF(.NOT.OPEN11)THEN
          OPEN(UNIT=IUNT11, FILE=ADFFIL, STATUS='UNKNOWN')
          OPEN11 = .TRUE.
        ENDIF
C
        CALL H9WR11( IUNT11 , DSFULL1 ,
     &               NDLEV  , NDTRN   , NDTEM   , NDMET , 
     &               DATE   , 
     &               TITLED , IZ      , IZ0     , IZ1   , BWNOA  ,
     &               IL     ,
     &               CSTRGA , ISA     , ILA   , XJA    , WA ,
     &               IPLA   , ZPLA    ,
     &               MAXT   , TOA     , 
     &               icnte  , icnts   , ietrn   , istrn ,
     &               TCODE  , I1A     , I2A     , AVAL  , SCOMUP ,
     &               SCOMDN , DTYPE   , KAP_VAL , ADF37 , dru_val,
     &               lbeth  , beth    , npl     , cprta
     &              )
      ENDIF
C
C-----------------------------------------------------------------------
C  IF SELECTED - GENERATE GRAPH; H9OUTG COMMUNICATES WITH IDL VIA PIPE.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN
C       
	  DO 60 I=1, maxt
            PRATE(1,I) = GAMMA(I)
            PRATE(2,I) = GAMMADN(I)
	    PRATE(3,I) = RATE(I)
            PRATE(4,I) = DRATE(I)
 60       CONTINUE	  
	  DO 70 I=1, 100
            PROSA(1,I) = GAMOSA(I)
            PROSA(2,I) = GAMDNOSA(I)
            PROMA(1,I) = GAMOMA(I)
            PROMA(2,I) = GAMDNOMA(I)
	    PROMA(3,I) = ROMA(I)
	    PROSA(3,I) = ROSA(I)
            PROMA(4,I) = DROMA(I)
            PROSA(4,I) = DROSA(I)
 70	  CONTINUE

        CALL H9OUTG(  TITLX   , TITLM , DATE  ,
     &                TOA     , PRATE , MAXT  ,
     &                TOMA    , PROMA , NMX   ,
     &                TOSA    , PROSA , NPSPL ,
     &                LDEF1   , LFSEL ,
     &                XMINK   , XMAXK , YMIN  , YMAX
     &              )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
        READ(PIPEIN,*) ISTOP
        IF(ISTOP.EQ.1) GOTO 9999
C
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO ISPF PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
 1001 FORMAT((1A),'  ELEMENT: ',A3,'(Z=',I2,')',
     &       '  TRANSITION: Q (',I2.2,'->',I2.2,')')
 1002 FORMAT(1X,32('*'),' ADAS809 ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I4,A)
 1003 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1010 FORMAT(2X,5(1E12.4))  
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE
C
      IF(OPEN07) CLOSE(IUNT07)
      WRITE(PIPEOU,'(A3)') 'ONE'
      CALL XXFLSH(PIPEOU)
C
      END
