      SUBROUTINE H9SPF0( REP  , DSFULL1 , DSFULL2 , FILE_TYPE1 , 
     &                   FILE_TYPE2 ) 

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS809
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL1 = MAIN INPUT DATA SET NAME , INCLUDING PATH
C  OUTPUT: (C*80)  DSFULL2 = COMPARATIVE DATA SET NAME , INCLUDING PATH
C  OUTPUT: (C*6)   FILE_TYPE1 = TYPE OF ADF FILE, EITHER ADF04 OR 37
C  OUTPUT: (C*6)   FILE_TYPE2 = TYPE OF ADF FILE, EITHER ADF04 OR 37
C
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  HUGH SUMMERS (UNIV. OF STRATHCLYDE)
C
C DATE:    30/11/01
C
C UPDATE: 18/12/02 - MARTIN TORNEY: ADDED FILE_TYPE TO ALLOW DISTINCTION
C                                   BETWEEN ADF04 AND ADF37 FILES
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       CHARACTER    REP*3        , DSFULL1*80   , DSFULL2*80,
     &              FILE_TYPE1*6 , FILE_TYPE2*6
C-----------------------------------------------------------------------
       INTEGER    PIPEIN    , PIPEOU
       PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
       READ(PIPEIN,'(A)') REP
       READ(PIPEIN,'(A)') DSFULL1
       READ(PIPEIN,'(A)') DSFULL2
       READ(PIPEIN,'(A)') FILE_TYPE1
       READ(PIPEIN,'(A)') FILE_TYPE2
C
C-----------------------------------------------------------------------
C
       RETURN
       END
