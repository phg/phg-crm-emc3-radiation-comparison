       SUBROUTINE H9OUT0( IWRITE   , IL     , LFSEL , LOSEL ,
     &                    TITLE    , TITLX  , TITLM , DATE  ,
     &                    IZ0      , IZ1    , IZ    , BWNO  ,
     &                    MAXT     , LTRNG  , ADF37 ,
     &                    TOA      , GAMOA  , DROA  , ROA   ,
     &                    AA       , LUPPER , LLOWER, EUPPER, ELOWER ,
     &                    IUPPER   , ILOWER ,
     &                    CSTRGA   , ISA    , ILA   , XJA   ,
     &                    KPLUS1   , COEF   , DTYPE , KAP_VAL
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED TRANSITION UNDER
C            ANALYSIS.
C
C  CALLING PROGRAM: ADAS809
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (I*4)  IL      = NUMBER OF INDEX ENERGY LEVELS
C  INPUT : (L*4)  LOSEL   = .TRUE.  => INTERPOLATED   VALUES  FOR   ISPF
C                                      PANEL INPUT VALUES CALCULATED
C                           .FALSE  => INTERPOLATED   VALUES  FOR   ISPF
C                                      PANEL INPUT VALUES NOT CALCULATED
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  INPUT : (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C  INPUT : (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C  INPUT : (C*20) ADF37   = NAME OF THE ADF37 FILE USED FOR AVERAGING
C
C  INPUT : (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (kelvin)
C  INPUT : (R*8)  GAMOA() = SPLINE INTEROPLATED GAMMA VALUE AT 'TOA()'
C  INPUT : (R*8)  ROA()   = EXCITATION RATE COEFF.(cm**3/s) AT 'TOA()'
C  INPUT : (R*8)  DROA()  = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOA()'
C
C  INPUT : (R*8)  AA      = SELECTED TRANSITION A-VALUE (SEC-1)
C  INPUT : (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C  INPUT : (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C  INPUT : (R*8)  EUPPER  = SELECTED TRANSITION: UPPER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C  INPUT : (R*8)  ELOWER  = SELECTED TRANSITION: LOWER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C
C  INPUT : (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C  INPUT : (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C
C  INPUT : (C*18) CSTRGA()= LEVEL CONFIGURATION FOR INDEX LEVELS
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR INDEX LEVELS
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR INDEX LEVELS
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR INDEX LEVELS
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (R*8)  WN2RYD  = WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C          (R*8)  TK2EV   = KELVINS TO ELECTRON VOLTS CONVERSION
C          (R*8)  TK2RT   = KELVINS TO REDUCED TEMPERATURE CONVERSION
C          (R*8)  EDIF    = TRANSITION ENERGY IN WAVE NUMBERS (CM-1)
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR  : HUGH SUMMERS (UNIVERSITY OF STRATHCLYDE)
C           JA7.08
C           EXT. 4196
C
C DATE:    14/06/01
C
C UPDATE:  
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 14/06/01
C MODIFIED: HUGH SUMMERS 
C              - FIRST VERSION
C
C----------------------------------------------------------------------
      REAL*8     WN2RYD             , TK2EV
C----------------------------------------------------------------------
      PARAMETER( WN2RYD=9.11269D-06 , TK2EV=8.6167D-05 )
C----------------------------------------------------------------------
      INTEGER    I           , IWRITE     , IL          ,
     &           IZ0         , IZ1        , IZ          ,
     &           MAXT        , KPLUS1     ,
     &           LUPPER      , LLOWER     ,
     &           IUPPER      , ILOWER
C----------------------------------------------------------------------
      REAL*8     EUPPER      , ELOWER     ,
     &           TK2RT       , EDIF       ,
     &           BWNO        , AA         ,
     &           KAP_VAL
C----------------------------------------------------------------------
      LOGICAL    LFSEL       , LOSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)   , TITLX*(*)  , TITLM*(*)   , DATE*8    ,
     &           TEXTU*5     , TEXTL*5    , CADAS*80    , ADF37*20
C----------------------------------------------------------------------
      INTEGER    ISA(IL)     , ILA(IL)    , DTYPE
C----------------------------------------------------------------------
      REAL*8     XJA(IL)     ,
     &           TOA(MAXT)   , GAMOA(MAXT) , DROA(MAXT) , ROA(MAXT) ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRNG(MAXT)
C----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18, TYPEARR(5)*15
C----------------------------------------------------------------------
      SAVE       CADAS
C----------------------------------------------------------------------
      DATA       TEXTU       , TEXTL       , CADAS
     &         / 'UPPER'     , 'LOWER'     , ' '        /

C----------------------------------------------------------------------
C
C**********************************************************************
C
      TK2RT=1.d0/DBLE(IZ1*IZ1)
      EDIF=EUPPER-ELOWER
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001) ' GAMMA GENERATION FROM ADF04 TYPE 1' ,
     &                   'ADAS809' , DATE
      WRITE(IWRITE,1002) TITLE
      WRITE(IWRITE,1003) TITLX
      WRITE(IWRITE,1004) IZ0    , IZ1    , IZ    , BWNO   ,
     &                   (WN2RYD*BWNO)
      WRITE(IWRITE,1005)
      WRITE(IWRITE,1006) LUPPER      , TEXTU  , CSTRGA(IUPPER)(1:12) ,
     &                   ISA(IUPPER) , ILA(IUPPER) , XJA(IUPPER)   ,
     &                   EUPPER      , (WN2RYD*EUPPER)
      WRITE(IWRITE,1006) LLOWER      , TEXTL  , CSTRGA(ILOWER)(1:12) ,
     &                   ISA(ILOWER) , ILA(ILOWER) , XJA(ILOWER)   ,
     &                   ELOWER      , (WN2RYD*ELOWER)
C
C---------------------------------------------------------------------
C OUTPUT SELECTED TRANSITION A-VALUE AND ENERGY.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1007) AA   , EDIF , (WN2RYD*EDIF)
C
C---------------------------------------------------------------------
C OUTPUT DISTRIBUTION TYPE AND AND DISTRIBUTION PARAMETERS
C---------------------------------------------------------------------
C
      TYPEARR(1) = 'MAXWELLIAN'
      TYPEARR(2) = 'KAPPA'
      TYPEARR(3) = 'NUMERICAL'
C      
      IF (DTYPE.EQ.0) WRITE(IWRITE,1016) TYPEARR(1)
      IF (DTYPE.EQ.1) WRITE(IWRITE,1017) TYPEARR(2), KAP_VAL
      IF (DTYPE.EQ.2) WRITE(IWRITE,1018) TYPEARR(3), ADF37
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES, GAMMAS, AND RATE COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LOSEL) THEN
            WRITE (IWRITE,1008)
               DO 1 I=1,MAXT
                  IF (LTRNG(I)) THEN
                     WRITE(IWRITE,1009) TOA(I)        , (TK2EV*TOA(I)) ,
     &                                 (TK2RT*TOA(I)) , GAMOA(I)       ,
     &                                  DROA(I)       , ROA(I)
                  ELSE
                     WRITE(IWRITE,1010) TOA(I)        , (TK2EV*TOA(I)) ,
     &                                 (TK2RT*TOA(I))
                  ENDIF
    1          CONTINUE
            WRITE (IWRITE,1011)
         ENDIF
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
            WRITE (IWRITE , 1012 )
              DO 2 I=1,KPLUS1,2
                 IF (I.EQ.KPLUS1) THEN
                    WRITE (IWRITE , 1013 ) I , COEF(I)
                 ELSE
                    WRITE (IWRITE , 1014 ) I , COEF(I) , I+1 , COEF(I+1)
                 ENDIF
    2         CONTINUE
            WRITE (IWRITE , 1015 ) TITLM
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,7('*'),' TABULAR OUTPUT FROM ',A35,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,7('*')/)
 1002 FORMAT(1H ,19('-'),1X,A40,1X,19('-')//11X,
     & 'EXCITATION RATE COEFFICIENTS AS A FUNCTION OF TEMPERATURE'/
     & 11X,57('-')/21X,
     & 'DATA GENERATED USING PROGRAM: ADAS809'/21X,37('-')/)
CA  UNIX PORT - ALLOW OUTPUT TO BE OF VARIABLE LENGTH
CX  1003 FORMAT(1X,A80/)
 1003 FORMAT(1X,'FILE :  ', (1A)/)
 1004 FORMAT(' NUCLEAR CHARGE         (Z0) = ', I3 /
     &       ' RECOMBINING ION CHARGE (Z1) = ', I3 /
     &       ' RECOMBINED ION CHARGE  (Z)  = ', I3 //
     &       ' IONISATION POTENTIAL        = ', 1P,D12.5,
     &       ' (cm-1)  = ',D12.5,' (rydberg)'/)
 1005 FORMAT(/
     & '    LEVEL      CONFIGURATION      QUANTUM NUMBERS ',
     & '   ENERGY RELATIVE TO LEVEL 1 '/
     & 33X,'2S+1  L  (WI-1)/2',5X,'(cm-1)',9X,'(rydberg)'/1X,79('-')
     &      )
 1006 FORMAT(1X,I2,' - ',A5,4X,A12,6X,I3,2X,I3,3X,F4.1,2(4X,1P,D12.5))
 1007 FORMAT(//' TRANSITION A-VALUE = ',1P,D9.2,' (sec-1)'//
     &         ' TRANSITION ENERGY  = ',1P,D12.5,' (cm-1)  = ',D12.5,
     &         ' (rydberg)'/)
 1008 FORMAT(/1X,13('-'),' TEMPERATURE ',13('-'),'   GAMMA   ',
     &'RATE COEFFICIENTS  (cm**3/s)'/5X,'kelvin',9X,'eV',6X,
     &'kelvin/Z1**2',12X,'DE-EXCITATION   EXCITATION'/1X,79('-'))
 1009 FORMAT(1P,2X,D9.2,3(4X,D9.2),2(4X,D10.3))
 1010 FORMAT(1P,2X,D9.2,2(4X,D9.2),
     &          4X,'-- TEMPERATURE VALUE OUT OF RANGE  --')
 1011 FORMAT(1X,79('-'))
C
 1012 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL - TAYLOR COEFFICIENTS:',
     &          ' LOG10(GAMMA) vs. LOG10(TEMP<kelvin>)'/1X,79('-'))
 1013 FORMAT ( 12X,'A(',I2,') = ',1P,D17.9)
 1014 FORMAT ( 2(12X,'A(',I2,') = ',1P,D17.9))
 1015 FORMAT (/1X,A79/1X,79('-'))
 1016 FORMAT (/1X,'DISTRIBUTION TYPE: ',A15)
 1017 FORMAT (/1X,'DISTRIBUTION TYPE: ',A15,' KAPPA = ',D12.5)
 1018 FORMAT (/1X,'DISTRIBUTION TYPE: ',A15,' ADF37 = ',A20)
C
C---------------------------------------------------------------------
C
      RETURN
      END
