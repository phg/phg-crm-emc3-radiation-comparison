       subroutine h9gett( iunit  , ndlev  , 
     &                    nv     , tva   
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: h9gett *********************
c
c  purpose:  to fetch temperature set from input adf04 type 3 data set.
c
c  calling program: adas809
c
c  data:
c           the 'real' data in the file is represented in an abbreviated
c           form which omits the "d" or "e" exponent specifier.
c           e.g. 1.23d-06 or 1.23e-06 is represented as 1.23-06
c                6.75d+07 or 6.75e+07 is represented as 6.75+07
c
c           therefore the form of each 'real' number in the data set is:
c                          n.nn+nn or n.nn-nn
c
c           the units used in the data file are taken as follows:
c
c           ionisation potential: wave number (cm-1)
c           index level energies: wave number (cm-1)
c           temperatures        : kelvin
c           a-values            : sec-1
c           gamma-values        :
c           rate coefft.        : cm3 sec-1
c
c
c  subroutine:
c
c  input : (i*4)  iunit   = unit to which input file is allocated
c  input : (i*4)  ndlev   = maximum number of levels that can be read
c
c  output: (i*4)  nv      = input data file: number of gamma/temperature
c                           pairs for a given transition.
c  output: (r*8)  scef()  = input data file: electron temperatures (k)
c
c          (i*4)  nvmax   = parameter = max. number of temperatures
c                                       that can be read in.
c
c          (i*4)  i4unit  = function (see routine section below)
c          (i*4)  iqs     = x-sect data format selector
c                           note: iqs=3 or 4 only allowed in this program
c          (i*4)  i       = general use.
c          (i*4)  j       = general use.
c          (i*4)  iline   = energy level index for current line
c          (i*4)  itpow() = temperatures - exponent
c                           note: mantissa initially kept in 'scef()'
c
c          (r*4)  zf      = should be equivalent to 'iz1'
c
c          (c*80) cline   = current energy level index parameter line
c          (c*500)buffer  = general string buffer storage
c          (c*3)  citpow()= used to parse values to itpow()
c          (c*5)  cscef() = used to parse values to scef()
c
c          (l*4)  ldata   = identifies  whether  the end of an  input
c                           section in the data set has been located.
c                           (.true. => end of section reached)
c          (l*4)  ltied() = .true.  => specified level tied
c                         = .false. => specified level is untied
c                           dimension => level index
c
c routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c
c routines: none
c
c author:  Hugh Summers, University of strathclyde
c          ja7.08
c          ext. 4196
c
c date:    13/08/01
c
c update:  
c
c unix-idl port:
c
c version: 1.1                          date: 14-06-013
c modified: Hugh Summers
c               - first version
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   nvmax       , mtied
c-----------------------------------------------------------------------
      real*8    zf 
c-----------------------------------------------------------------------
      parameter( nvmax = 14 , mtied = 5000 )
c-----------------------------------------------------------------------
      integer   i4unit
      integer   iunit       , ndlev          , nv
      integer   iqs         , i              , j      , iline    
c-----------------------------------------------------------------------
      real*8    scef(nvmax) , tva(nvmax)
c-----------------------------------------------------------------------
      integer   itpow(nvmax)
      character cline*80        , buffer*500
      character citpow(nvmax)*3              , cscef(nvmax)*5
c-----------------------------------------------------------------------
      logical   ldata 
c-----------------------------------------------------------------------
c
      if (mtied.lt.ndlev)
     &             stop ' h9gett error: mtied < ndlev - increase mtied'
     
      read (iunit,'(a80)') cline
c
c
c***********************************************************************
c read in energy level specifications
c***********************************************************************
c
      ldata=.true.
c
         do 1 i=1,ndlev
c
            if (ldata) then
               read (iunit,'(a80)') cline
               read (cline,'(i5)')  iline
c
c  energy level input information is terminated by iline=-1.
c
                  if (iline.le.0) then
                     ldata=.false.
                  endif
            endif
    1    continue
c
c***********************************************************************
c read in temperatures (kelvin) and data format selector
c***********************************************************************
c
      buffer = ' '
      read(iunit,'(a)') buffer
      read(buffer,1005) zf, iqs, (cscef(i),citpow(i),i=1,nvmax)
c
c check data format selector is valid
c
         if(iqs.ne.3.and.iqs.ne.4) then
            write(i4unit(-1),1001) '(iqs.ne.3 or 4) -'
            write(i4unit(-1),1006)
     &      'file contains invalid format selector (must equal 3 or 4)'
            write(i4unit(-1),1002)
            stop
         endif
c
c identify the number of temperatures values input
c
         nv = 0
c
         do 2 j=1,nvmax
            if (cscef(j).eq.' ') then
               scef(j)  = 0.0d0
               itpow(j) = 0
            else
               read(cscef(j) ,'(f5.2)') scef(j)
               read(citpow(j), '(i3)')  itpow(j)
               if (scef(j).gt.0.0d0) nv = j
            endif
    2    continue
c
c check that at least one valid temperature exists.
c
    3    if (nv.le.0) then
            write(i4unit(-1),1001) '(nv.le.0) -'
            write(i4unit(-1),1006)
     &      'no valid temperature values found (none > 0.0)'
            write(i4unit(-1),1002)
            stop
         endif
c
c-----------------------------------------------------------------------
c combine input mantissa and exponent for temperatures
c-----------------------------------------------------------------------
c
         do 4 i=1,nv
            scef(i)  = scef(i)  * ( 10.d0**dble(itpow(i)) )
	    tva(i)   = scef(i) / 11605.4d0
    4    continue
c
c-----------------------------------------------------------------------
c check temperatures are in a strictly increasing monotonic sequence
c (required for the later use of the minimax and spline nag routines)
c-----------------------------------------------------------------------
c
         do 5 i=2,nv
            if (scef(i).le.scef(i-1)) then
               write(i4unit(-1),1007) '(scef(',i,').le.scef(',i-1,')) -'
               write(i4unit(-1),1006)
     &      'temperatures are not in a strict monotonic ascending order'
               write(i4unit(-1),1002)
               stop
             endif
    5    continue
c
c***********************************************************************
c
 1001 format(1x,32('*'),' h9gett error ',32('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1005 format(f5.2,4x,i1,6x,50(a5,a3))
 1006 format(1x,a)
 1007 format(1x,32('*'),' h9gett error ',32('*')//
     &       1x,'fault in input data file: ',a,2(i1,a))
c
c-----------------------------------------------------------------------
c
      return
      end
