      subroutine h9int( itran  , stcode  , i1a    , i2a   , aval  ,
     &                  iadftyp, DTYPE  , ADF37  , iz1   , MAXT  ,
     &                  beth   , il     , nv     , ia    , wa    ,
     &                  xja    , omga   , scx    , ilbeth, DPARAM,
     &                  zpla   , bwnoa  , ipla   , TINE  , IFOUT,
     &                  upsilon , dnsilon   )
      implicit none
C----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9INT *********************
C
C  PURPOSE: GENERATES MAXWELLIAN AND NON-MAXWELLIAN UPSILONS AND
C           DOWNSILONS FROM GIVEN COLLISIONAL DATA. THIS PROGRAM
C           IS A SUBROUTINE VERSION OF ADAS809.
C
C  CALLING PROGRAM: CALLED FROM IDL VIA A C INTERFACE
C
C  SUBROUTINE:
C
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NEDIM   = PARAMETER = MAX. OF INPUT DATA FILE ENERGIES
C          (I*4)  NDTEM   = PARAMETER = MAXIMUM OF DATA CONVERSION TEMPS
C          (I*4)  NDTIN   = PARAMETER = MAX.NUMBER OF ISPF ENTERED TEMPS
C          (I*4)  NFDIM   = PARAMETER = MAX.NO. OF ENERGIES IN NUM. DIST.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ1     = RECOMBINING ION CHARGE READ FROM INPUT FILE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY INDEX
C                                            LEVELS.
C          (I*4)  ITRAN   = INPUT DATA FILE: TOTAL NUMBER OF TRANSITIONS.
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                                     PAIRS FOR THE SELECTED TRANSITION.
C          (I*4)  MAXT    = NUMBER OF TEMPERATURE VALUES.
C          (I*4)  IFOUT   = 1 => 'TINE(ARRAY)' UNITS: KELVIN
C                         = 2 => 'TINE(ARRAY)' UNITS: EV
C                         = 3 => 'TINE(ARRAY)' UNITS:REDUCED TEMPERATURE
C          (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C          (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C          (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C          (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C          (R*8)  R8TCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  EUPPER  = SELECTED TRANSITION: UPPER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  ELOWER  = SELECTED TRANSITION: LOWER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C          (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  AA      = SELECTED TRANSITION A-VALUE (SEC-1)
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C          (I*4)  IPLA(,) = PARENT INDEX FOR CONTRIBUTION TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (R*8)  ZPLA(,) = EFF. ZETA PARAM. FOR CONTRIBUTION TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (R*8)  AVALE() = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')C
C          (R*8)  SCX()   = INPUT DATA FILE: X-PARAMETER SET
C          (R*8)  SCEF()  = PROGRAM: INPUT FILE CONVERSION TEMPS, OR 2/3
C                           AVERAGE ENERGY OF NUMERICAL DISTRIBUTION IF 
C                           ADF37 COMPARISON FILE IS SELECTED (KELVIN)
C          (R*8)  SCEF2() = MOST COMMON ENERGY OF DISTRIBUTION FUNC. IF 
C                           ADF37 COMPARISON FILE IS SELECTED (KELVIN)
C          (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GAMMA VALUE AT 'SCEF()'
C          (R*8)  RATE()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           EXCITATION RATE COEFF. (CM**3/S) AT 'SCEF()'
C          (R*8)  DRATE() = INPUT DATA FILE: SELECTED TRANSITION -
C                           DEEXCITATION RATE COEF.(CM**3/S) AT 'SCEF()'
C
C          (R*8)  TINE()  = ISPF ENTERED ELECTRON TEMPERATURE VALUES.
C                           (NOTE: UNITS ARE GIVEN BY 'IFOUT')
C          (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (KELVIN)
C
C          (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (KELVIN)
C          (R*8)  GAMOSA()= SPLINE INTEROPLATED GAMMA VALUE AT 'TOSA()'
C          (R*8)  ROSA()  = EXCITATION RATE COEFF.(CM**3/S) AT 'TOSA()'
C          (R*8)  DROSA() = DEEXCITATION RATE COEF.(CM**3/S) AT 'TOSA()'
C
C          (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (KELVIN)
C          (R*8)  GAMOMA()= MINIMAX GENERATED GAMMA VALUE AT 'TOMA()'
C          (R*8)  ROMA()  = EXCITATION RATE COEFF.(CM**3/S) AT 'TOMA()'
C          (R*8)  DROMA() = DEEXCITATION RATE COEF.(CM**3/S) AT 'TOMA()'
C
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPS OR 2/3
C                            AVERAGE ENERGY IF ADF37 COMPARISON FILE IS 
C                            SELECTED
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => ELECTRON IMPACT   TRANSITION
C                           'P' => PROTON   IMPACT   TRANSITION
C                           'H' => CHARGE   EXCHANGE RECOMBINATION
C                           'R' => FREE     ELECTRON RECOMBINATION
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES.
C          (C*22) STRGA() = NOMENCLATURE FOR LEVEL 'IA()' INCLUDES:
C                           'CSTRGA()' AND QUANTUM NUMBERS.
C	   (I*4)  IFIRST  = FIRST NON-BLANK CHARCTER IN FILENAME
C
C	   (I*4)  ILAST   = LAST  NON-BLANK CHARCTER IN FILENAME
C
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
C
C NOTE:    INPUT TEMPERATURES 'TINE()' ARE CONVERTED TO KELVIN 'TOA()'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C  AUTHOR:  PAUL BRYANS
C
C  DATE:    02 MARCH 2005
C
C  MODIFICATION HISTORY:
C
C  DATE:   02/03/05			VERSION: 1.1
C  MODIFIED: PAUL BRYANS
C		- EXAMPLE CODE
C     
C  DATE:   17/03/05			VERSION: 1.2
C  MODIFIED: ALLAN WHITEFORD
C		- MODIFIED TO INTERFACE WITH C/IDL.
C  DATE:   04/04/05
C  MODIFIED: ALLAN WHITEFORD
C		- CHANGED LSS04A FROM A SCALAR TO AN ARRAY OF
C                 SIZE (NDLEV,NDMET)
C
C-----------------------------------------------------------------------
      INTEGER     NDLEV        , NDTRN        , NDTEM     , NDTIN      ,
     &            NEDIM        , NFDIM     , IUNT37
      INTEGER     NPSPL      
      INTEGER     L1                
      INTEGER     NDMET        , NVMAX     , 
     &		  IADFTYP      , ILBETH
C-----------------------------------------------------------------------
      PARAMETER ( NDLEV = 110  , NDTRN  = 2100 , NEDIM  = 50 ,
     &            NDTEM = 50   , NDTIN  = 50   ,
     &            NFDIM = 200  , NDMET  = 4    ,
     &            NVMAX = ndtem )
      PARAMETER ( IUNT37 =  37 )
      PARAMETER ( NPSPL  = 100 )
      PARAMETER ( L1     =   1 )
C-----------------------------------------------------------------------
      INTEGER     I4UNIT       , I         , IT , J       
      INTEGER     IZ1       ,
     &            IL           , ITRAN        , ITRN      , 
     &            ICNTE        , ICNTP        , ICNTR     , ICNTH      ,
     &            ICNTI        , ICNTL        , ICNTS     , 
     &            NV           , NVT          , MAXT      , 
     &            IFOUT        , IFORM        ,
     &            IUPPER       , ILOWER       , LUPPER    , LLOWER
      INTEGER     IFINT        , ITYPE        , ITYPT     , ILINR      ,
     &            IESCL        , IC  
      INTEGER     DTYPE        , N_EN         , NTEMP
      INTEGER	  IPLA(NDMET,NDLEV)      ,
     &            icateg       , nform1     ,
     &            nform2
C-----------------------------------------------------------------------
      REAL*8      dparam
      REAL*8      EUPPER       , ELOWER       , WUPPER    , WLOWER     ,
     &            AA           , EVT
      REAL*8      KAP_VAL      , DRU_VAL      , LIMIT
      REAL*8      BETH(NDTRN)  , BWNOA(NDMET) , 
     &            ZPLA(NDMET,NDLEV)           ,
     &            param1       , param2(2)       ,
     &            median(ndtem), AUGA(NDTRN)  , WVLA(NDLEV)            ,
     &            rtions(ndtem), rtrecm(ndtem)
C-----------------------------------------------------------------------
      LOGICAL     LEXIST
      LOGICAL     OPEN37
      LOGICAL     LBETH       
C-----------------------------------------------------------------------
      CHARACTER   header*80 ,ADF37*80
      CHARACTER   TCHR*1
      character   filnam(nfdim)*120 , filout*120 , calgeb(ndtem,4)*25  ,
     &            ealgeb(ndtem)*25
C-----------------------------------------------------------------------
      INTEGER     IA(NDLEV)    ,
     &            I1A(NDTRN)   , I2A(NDTRN)
      INTEGER     IETRN(NDTRN) , IPTRN(NDTRN) ,
     &            IRTRN(NDTRN) , IHTRN(NDTRN) ,
     &            IiTRN(NDTRN) , IlTRN(NDTRN) ,
     &            IsTRN(NDTRN) , 
     &            IE1A(NDTRN)  , IE2A(NDTRN)  ,
     &            IP1A(NDTRN)  , IP2A(NDTRN)  ,
     &            Ia1A(NDTRN)  , Ia2A(NDTRN)  ,
     &            Il1A(NDTRN)  , Il2A(NDTRN)  ,
     &            Is1A(NDTRN)  , Is2A(NDTRN)  ,
     &            nplr         , npli
C-----------------------------------------------------------------------
      REAL*8      XJA(NDLEV)   , WA(NDLEV)    ,
     &            AVAL(NDTRN)  , AVALE(NDTRN)
      REAL*8      SCX(NEDIM)   , OMA(NEDIM)   , 
     &            GAMMA(NDTEM)
      REAL*8      TINE(NDTIN)  , SCEFS(14) , 
     &            TOA(NDTIN)   , TOSA(NPSPL)  ,
     &            GAMOSA(NPSPL),
     &            GAMDNOSA(NPSPL) 
      REAL*8      OMGA(NEDIM,NDTRN)
      REAL*8      GAMMAUP(NDTEM)      , GAMMADN(NDTEM),
     &            SCOMUP(NDTEM,NDTRN ), SCOMDN(NDTEM,NDTRN),
     &            upsilon(NDTEM,NDTRN ), dnsilon(NDTEM,NDTRN),
     &            EN(NDTEM,NFDIM)     , F(NDTEM,NFDIM)     ,
     &            USCEF(NDTEM)        , 
     &            UTVA(NDTEM)         , UTVA2(NDTEM)
      real*8      xsec(nedim), zeta, ip, ip_ev
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(npspl)  , LSS04A(NDLEV,NDMET)  
C-----------------------------------------------------------------------
      CHARACTER   TCODE(NDTRN)*1
      INTEGER     STCODE(NDTRN)
C-----------------------------------------------------------------------
      DATA OPEN37 /.FALSE./
C-----------------------------------------------------------------------
      DATA scefs /1.00D+02,2.00D+02,5.00D+02,1.00D+03,2.00D+03,5.00D+03
     &           ,1.00D+04,2.00D+04,5.00D+04,1.00D+05,2.00D+05,5.00D+05
     &           ,1.00D+06,2.00D+06/
C-----------------------------------------------------------------------     
        
	 if (ilbeth.eq.0) lbeth=.false.
	 if (ilbeth.eq.1) lbeth=.false.

      do i = 1, itran
        tcode(i) = char(stcode(i))
      end do 
 
      if (dtype.eq.1) then
        kap_val = dparam
	dru_val = 0d0
      elseif (dtype.eq.3) then
        kap_val = 0d0
	dru_val = dparam
      else
        kap_val = 0d0
	dru_val = 0d0
      endif
      
      call h9ttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &             itran  , tcode  , i1a    , i2a   , aval  ,
     &             iadftyp,
     &             icnte  , icntp  , icntr  , icnth , icnti ,
     &             icntl  , icnts  ,
     &             ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &             iltrn  , istrn  ,
     &                               ie1a   , ie2a  , avale ,
     &                               ip1a   , ip2a  ,
     &                               ia1a   , ia2a  , auga  ,
     &                               il1a   , il2a  , wvla  ,
     &                               is1a   , is2a  , lss04a 
     &           )

      if (dtype.eq.2) then
        
	if (open37) then
          close(37)
          open37=.false.
        endif
c       
        inquire ( file=adf37, exist=lexist)	
      
        open( unit=iunt37 , file=adf37 , status='unknown')
	open37=.true.
	
        call xxdata_37( iunt37 ,
     &	                nfdim  , nvmax  ,
     &			header , icateg , n_en   , ntemp  ,
     &			nform1 , param1 , nform2 , param2 ,
     &			en     , f      , utva   , utva2  ,
     &			median , filnam , filout , calgeb ,
     &			ealgeb
     &		      )

        if (icateg.ne.2) then
	  write(i4unit(-1),1002) 'icateg =',icateg,', must equal 2'
	  write(i4unit(-1),1003)
	  stop
	endif
	
	do i=1, ntemp
	  uscef(i)  = utva(i)*11605.4d0
	enddo
        nvt = ntemp
      else
        nvt = maxt
      endif
      
      CALL XXTCON( IFOUT , L1 , IZ1 , MAXT , TINE , TOA)

      DO IFORM=1,NDTIN
        TINE(IFORM) = TOA(IFORM)/11605.4d0
      ENDDO
      
      IFINT= 1                                                         
      ILINR= 1                                                         
      IESCL= 1
        
      IF (IZ1.EQ.1) THEN 
        ITYPT = 2
      ELSE
        ITYPT = 1
      ENDIF
      
      DO 40 IC = 1, ICNTE
        TCHR = TCODE(IETRN(IC))
       READ(TCHR,'(I1)')ITYPE
	LIMIT=BETH(IC)      
            
        CALL H9TRAN(   NDLEV  , NDTRN     , NEDIM  ,
     &                 IL     , IETRN(IC) , NV     ,
     &                 IA     , WA        , XJA    ,
     &                 I1A    , I2A       , AVAL   , 
     &                 OMGA   ,
     &                 IUPPER , ILOWER    , 
     &                 LUPPER , LLOWER    ,
     &                 WUPPER , WLOWER    ,
     &                 EUPPER , ELOWER    ,
     &                 AA     , OMA       
     &               )
C     
        EVT=13.6048d0*(EUPPER-ELOWER)/109737.26d0
C     
        CALL H9NTQD( NEDIM   , NDTEM   , NFDIM     ,            
     &               IFINT   , ITYPE   , ITYPT     , ILINR , IESCL  , 
     &               IETRN(IC) ,              
     &               NV      , MAXT    ,                      
     &               EVT     , SCX     , 
     &               OMA     , TINE    , 
     &               GAMMAUP , GAMMADN ,
     &               KAP_VAL , dru_val , DTYPE     ,
     &               N_EN    , EN      , F         ,
     &               NTEMP   , UTVA    ,
     &               LBETH   , LIMIT   ,
     &               nform1  , param1  , nform2    , param2 
     &             )
C
	DO 50 IT=1,NVT
          SCOMUP(IT,IETRN(IC)) = GAMMAUP(IT)
          SCOMDN(IT,IETRN(IC)) = GAMMADN(IT) 
 50     ENDDO
 40   ENDDO
        
      do ic = 1,icnts
        tchr = tcode(istrn(ic))
	
	call h9trni( NDLEV  , NDTRN     , NEDIM  , ndmet ,
     &               IL     , IsTRN(IC) , NV     ,
     &               IA     , WA        , XJA    ,
     &               I1A    , I2A       , AVAL   , 
     &               OMGA   , zpla      , bwnoa  , ipla ,
     &               IUPPER , ILOWER    , 
     &               LUPPER , LLOWER    ,
     &               WUPPER , WLOWER    ,
     &               EUPPER , ELOWER    ,
     &               AA     , xsec      ,
     &               zeta   , ip
     &             )
	
	ip_ev = 13.6048d0*ip/109737.26d0
			
	call h9qd3b( ndtem , nedim   , nfdim   ,
     &	             tine  , utva    ,
     &               nv    , maxt    , wupper  , wlower  ,
     &               scx   , dtype   , kap_val , dru_val ,
     &               zeta  , ip_ev   , xsec    ,
     &               n_en  , en      , f       , ntemp   ,
     &               nform1, param1  , nform2  , param2  ,
     &               rtions, rtrecm
     &             )

	do it=1,nvt
          scomup(it,istrn(ic)) = rtions(it)
          scomdn(it,istrn(ic)) = rtrecm(it) 
        enddo

      enddo
      
      CALL H9SPLN( NDTRN   , NDTEM  ,
     &             NDTIN   , nvt     , MAXT   , NPSPL  ,
     &             IETRN   , icnte   , DTYPE  ,
     &             ISTRN   , ICNTS   ,
     &             USCEF   , TOA     , TOSA   ,
     &             SCOMUP  ,
     &             GAMMA   , GAMOSA  ,
     &             LTRNG   , ITRN
     &           )   
    
      CALL H9SPLN( NDTRN   , NDTEM  ,
     &             NDTIN   , nvt     , MAXT   , NPSPL  ,
     &             IETRN   , icnte   , DTYPE  ,
     &             ISTRN   , ICNTS   ,
     &             USCEF   , TOA     , TOSA   ,
     &             SCOMDN  ,
     &             GAMMADN , GAMDNOSA,
     &             LTRNG   , ITRN
     &           )

    
      do i=1,NDTEM
        do j=1,NDTRN 
          upsilon(i,j) = scomup(i,j)
          dnsilon(i,j) = scomdn(i,j)
        enddo
      enddo

C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
 1001 FORMAT((1A),'  ELEMENT: ',A3,'(Z=',I2,')',
     &       '  TRANSITION: Q (',I2.2,'->',I2.2,')')
 1002 FORMAT(1X,32('*'),' ADAS809 ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I4,A)
 1003 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1010 FORMAT(2X,5(1E12.4))  
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE      
      end
