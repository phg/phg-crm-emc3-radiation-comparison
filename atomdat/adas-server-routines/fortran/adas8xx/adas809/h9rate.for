      SUBROUTINE H9RATE( nmax   ,  NARR  , 
     &                   TEMP   , GAMAUP , gamadn ,  
     &                   EUPPER , ELOWER ,
     &                   WUPPER , WLOWER ,
     &                   RATE   , DRATE
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9RATE *********************
C
C  PURPOSE: TO CALCULATE THE EXCITATION AND DE-EXCIATATION RATE COEFFI-
C           CIENTS FOR A SET OF INPUT TEMPERATURE(kelvin)/ GAMMA PAIRS.
C
C  CALLING PROGRAM:  ADAS809
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NMAX    = MAX NUMBER OF INPUT TEMPERATURE/GAMMA PAIRS
C  INPUT :  (I*4)  NARR    = NUMBER OF INPUT TEMPERATURE/GAMMA PAIRS
C  INPUT :  (R*8)  TEMP()  = TEMPERATURE VALUES (kelvin)
C  INPUT :  (R*8)  GAMUP() = GAMMA VALUES FOR EXCITATION
C  INPUT :  (R*8)  GAMDN() = GAMMA VALUES FOR DE-EXCITATION
C
C  INPUT :  (R*8)  EUPPER  = SELECTED TRANSITION - UPPER ENERGY LEVEL
C                            RELATIVE TO INDEX LEVEL 1 (CM-1).
C  INPUT :  (R*8)  ELOWER  = SELECTED TRANSITION - LOWER ENERGY LEVEL
C                            RELATIVE TO INDEX LEVEL 1 (CM-1).
C
C  INPUT :  (R*8)  WUPPER  = SELECTED TRANSITION - UPPER ENERGY LEVEL
C                            STATISTICAL WEIGHT.
C  INPUT :  (R*8)  WLOWER  = SELECTED TRANSITION - LOWER ENERGY LEVEL
C                            STATISTICAL WEIGHT.
C
C  OUTPUT:  (R*8)  RATE    = EXCITATION RATE COEFFS (cm**3/s)
C  OUTPUT:  (R*8)  DRATE   = DEEXCITATION RATE COEFS (cm**3/s)
C
C           (R*8)  TK2ATE  = PARAMETER = EQUATION CONSTANT = 1.5789D+05
C           (R*8)  R2GAM   = PARAMETER = EQUATION CONSTANT = 2.17161D-08
C           (R*8)  WN2RYD  = PARAMETER =
C                            WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C           (I*4)  I       = GENERAL ARRAY INDEX
C
C           (R*8)  SUPPER  = 1/(UPPER LEVEL STATISTICAL WEIGHT)
C           (R*8)  SLOWER  = 1/(LOWER LEVEL STATISTICAL WEIGHT)
C           (R*8)  RYDDIF  = NEGATIVE TRANSITION ENERGY IN RYDBERGS
C                            ( NOTE: 1 Rydberg = 1.09737E5 cm-1)
C           (R*8)  ATE     = EQUATION PARAMETER
C           (R*8)  GVAL    = EQUATION PARAMETER
C
C ROUTINES:  NONE
C
C NOTES:
C            EQUATIONS USED -
C
C                      2.17161E-8 x GAMMA x SQRT(157890 / TEMP)
C            RATE = ---------------------------------------------
C                   WLOWER x EXP(1.4388 x (EUPPER-ELOWER) / TEMP)
C
C                      2.17161E-8 x GAMMA x SQRT(157890 / TEMP)
C            DRATE = ---------------------------------------------
C                                     WUPPER
C
C AUTHOR:  HUGH SUMMERS (UNIVERSITY OF STRATHCLYDE)
C          JA7.08
C          EXT. 4196
C
C DATE:    30/11/01
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     TK2ATE          , R2GAM           , WN2RYD
C-----------------------------------------------------------------------
      PARAMETER( TK2ATE=1.5789D+5, R2GAM=2.17161D-8, WN2RYD=9.11269D-6 )
C-----------------------------------------------------------------------
      INTEGER    NMAX            , NARR            , I
C-----------------------------------------------------------------------
      REAL*8     ATE             , GVALUP          , GVALDN
      REAL*8     EUPPER          , ELOWER          ,
     &           WUPPER          , WLOWER          ,
     &           SUPPER          , SLOWER          , RYDDIF
C-----------------------------------------------------------------------
      REAL*8     TEMP(NMAX)      , GAMAUP(NMAX)    , GAMADN(NMAX) ,
     &           RATE(NMAX)      , DRATE(NMAX)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       RYDDIF=WN2RYD*(ELOWER-EUPPER)
       SUPPER=1.d0/WUPPER
       SLOWER=1.d0/WLOWER
C-----------------------------------------------------------------------
       DO 1 I=1,NARR
         ATE     = TK2ATE/TEMP(I)
         GVALUP  = R2GAM*GAMAUP(I)*DSQRT(ATE)
	 GVALDN  = R2GAM*GAMADN(I)*DSQRT(ATE)
         RATE(I) = GVALUP*SLOWER*(DEXP(RYDDIF*ATE))
         DRATE(I)= GVALDN*SUPPER
    1  CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
