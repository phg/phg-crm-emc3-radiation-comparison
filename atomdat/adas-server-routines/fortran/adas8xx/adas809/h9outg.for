       SUBROUTINE H9OUTG( TITLX  , TITLM , DATE  ,
     &                    TEMP   , RATE  , NENER ,
     &                    TOMA   , ROMA  , NMX   ,
     &                    TOSA   , ROSA  , NPSPL ,
     &                    LDEF1  , LFSEL ,
     &                    XMIN   , XMAX  , YMIN  , YMAX
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED TRANSITION USING IDL.
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                ORIGINAL <SE>LIKE.DATA    (CROSSES    )
C                                SPLINE INTERPOLATED DATA  (FULL CURVE)
C                                MINIMAX FIT TO DATA       (DASH CURVE )
C
C            PLOT IS LOG10(RATE(CM**3/SEC))  VERSUS LOG10(TEMP(KELVIN))
C
C  CALLING PROGRAM: ADAS809
C
C  SUBROUTINE:
C
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE AND  TRANSITION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  TEMP()  = INPUT DATA FILE: TEMPERATURES (KELVIN)
C  INPUT : (R*8)  RATE(,) = INPUT DATA FILE: SELECTED TRANSITION -
C                           1: UPSILON AT 'TEMP()'
C                           2: DOWNSILON AT 'TEMP()'
C                           3: EXC. RATE COEFF. (CM**3/S) AT 'TEMP()'
C                           4: DE-EXC. RATE COEFF. (CM**3/S) AT 'TEMP()'
C  INPUT : (I*4)  NENER   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C  INPUT : (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (KELVIN)
C  INPUT : (R*8)  ROMA(,) = 1: UPSILON AT 'TEMP()'
C                           2: DOWNSILON AT 'TEMP()'
C                           3: EXC. RATE COEFF. (CM**3/S) AT 'TEMP()'
C                           4: DE-EXC. RATE COEFF. (CM**3/S) AT 'TEMP()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX  GENERATED   GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (KELVIN)
C  INPUT : (R*8)  ROSA(,) = 1: UPSILON AT 'TEMP()'
C                           2: DOWNSILON AT 'TEMP()'
C                           3: EXC. RATE COEFF. (CM**3/S) AT 'TEMP()'
C                           4: DE-EXC. RATE COEFF. (CM**3/S) AT 'TEMP()'
C  INPUT : (I*4)  NPSPL   = NUMBER  OF SPLINE INTERPOLATED GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (K)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (K)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE COEFF. (CM**3/S)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE COEFF. (CM**3/S)
C
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C	   (I*4)  PIPEOU  = PARAMETER : UNIT NUMBER OF PIPE
C	   (I*4)  ONE	  = PARAMETER : VARIABLE USED AS LOGICAL FOR IDL
C	   (I*4)  ZERO    = PARAMETER : VARIABLE USED AS LOGICAL FOR IDL
C	   (I*4)  IFIRST  = POSITION OF FIRST NO-BLNK CHARACTER IN STRING
C	   (I*4)  ILAST   = POSITION OF LAST  NO-BLNK CHARACTER IN STRING
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C	   XXSLEN      ADAS      GET POSITION OF NON-BLANK CHARACTERS.
C
C AUTHOR:  HUGH SUMMERS (UNIVERSITY OF STRATHCLYDE)
C
C DATE:    30/11/01
C
C UPDATE:  28/01/04   M TORNEY - ADDED DIMENSION TO RATE, ROMA AND ROSA
C                                TO PASS DATA FOR UPSILON, DOWNSILON,
C                                AND RATE COEFFICIENTS 
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
        INTEGER NENER     , NMX         , NPSPL    , I   , J     ,
     &			    IFIRST      , ILAST    ,
     &                      PIPEOU      , ONE      ,  ZERO
	PARAMETER (PIPEOU = 6, ONE = 1, ZERO = 0)
C-----------------------------------------------------------------------
       REAL*8 TEMP(NENER) , RATE(4,NENER) ,
     &        TOMA(NMX)   , ROMA(4,NMX)   ,
     &        TOSA(NPSPL) , ROSA(4,NPSPL) ,
     &        XMIN        , XMAX        ,
     &        YMIN        , YMAX
C-----------------------------------------------------------------------
       CHARACTER TITLX*120   , TITLM*80  , DATE*8
C-----------------------------------------------------------------------
       LOGICAL   LDEF1     , LFSEL
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C WRITE OUT TITLES AND DATE INFORMATION
      CALL XXSLEN(TITLX, IFIRST, ILAST)
      WRITE(PIPEOU,'(A120)') TITLX(IFIRST:ILAST)
      WRITE(PIPEOU,'(A80)') TITLM
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
C
C WRITE OUT ARRAY SIZES
      WRITE(PIPEOU,*) NENER
      WRITE(PIPEOU,*) NPSPL
      CALL XXFLSH(PIPEOU)
C
C DATA FROM DATA FILE
      DO 1 I = 1, NENER 
         WRITE(PIPEOU,*) TEMP(I)
 1    CONTINUE
      CALL XXFLSH(PIPEOU)
      DO 2 J = 1, 4
        DO 3 I = 1, NENER
           WRITE(PIPEOU,*) RATE(J,I)
 3      CONTINUE
 2    CONTINUE
      CALL XXFLSH(PIPEOU)
C      
C WRITE OUT INTERPOLATED SPLINE VALUES
	 WRITE(PIPEOU,*) ONE
         DO 6 I = 1, NPSPL
            WRITE(PIPEOU,*) TOSA(I)
 6       CONTINUE
         CALL XXFLSH(PIPEOU)
         DO 7 J = 1, 4
           DO 8 I = 1, NPSPL
              WRITE(PIPEOU,*) ROSA(J,I)
 8         CONTINUE
 7       CONTINUE
         CALL XXFLSH(PIPEOU)
C        
C WRITE OUT USER SELECTED AXES RANGES.
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
      ELSE
         WRITE(PIPEOU,*) ONE
	 WRITE(PIPEOU,*) XMIN
	 WRITE(PIPEOU,*) XMAX
	 WRITE(PIPEOU,*) YMIN
	 WRITE(PIPEOU,*) YMAX
      ENDIF
      CALL XXFLSH(PIPEOU)
C
C IF SELECTED WRITE OUT MINMAX FIT VALUES
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 11 I = 1, NMX
	    WRITE(PIPEOU,*) TOMA(I)
 11       CONTINUE
	 CALL XXFLSH(PIPEOU)
         DO 12 J = 1, 4
           DO 13 I = 1, NMX
	     WRITE(PIPEOU,*) ROMA(J,I)
 13        CONTINUE
 12       CONTINUE
	 CALL XXFLSH(PIPEOU)
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
      END
