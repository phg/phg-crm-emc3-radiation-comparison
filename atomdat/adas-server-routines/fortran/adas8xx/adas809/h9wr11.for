       SUBROUTINE H9WR11( IUNT11 , DSNINP , 
     &                    NDLEV  , NDTRN , NVMAX , NDMET ,
     &                    DATE   , 
     &                    TITLED , IZ     , IZ0   , IZ1   , BWNOA ,
     &                    IL     ,
     &                    CSTRGA , ISA   , ILA   , XJA   , WA ,
     &                    IPLA   , ZPLA   ,
     &                    NVN    , SCEFN  ,
     &                    icnte  , icnts  , ietrn , istrn ,
     &                    TCODE  , I1A    , I2A   , AVAL  , SCOMUP ,
     &                    SCOMDN , DTYPE  , KAPPA , ADF37 , druval ,
     &                    lbeth  , beth   , npl   , cprta
     &                  )
       IMPLICIT NONE
C----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9WR11 *********************
C
C  PURPOSE:  PRODUCES AN ADF04 TYPE 3 FILE, WHERE THE CONTENTS IS
C            CONSIDERED AS THE OUTPUT DATA SET FROM ADAS809.
C
C  CALLING PROGRAM: ADAS809
C
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNT11  = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (C*80) DSNINP  = NAME OF INPUT ADF04 FILE
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS 
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS 
C  INPUT : (I*4)  NVMAX   = MAX. NUMBER OF TEMPERATURES 
C  INPUT : (I*4)  NDMET   = MAX. NUMBER OF METASTABLES
C  INPUT : (C*8)  DATE    = DATE (AS DD/MM/YY).
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C  INPUT : (I*4)  IPLA(,) = PARENT INDEX FOR CONTRIBUTION TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C  INPUT : (R*8)  ZPLA(,) = EFF. ZETA PARAM. FOR CONTRIBUTION TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C
C  INPUT : (I*4)  NVN     = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  SCEFN() = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C                           (INITIALLY JUST THE MANTISSA. SEE 'ITPOW()')
C                           (NOTE: TE=TP=TH IS ASSUMED)
C
C  INPUT : (I*4)  ITRNE   = INPUT DATA FILE: NO OF EXCITATION TRANSITIONS
C  INPUT : (I*4)  ITRNS   = INPUT DATA FILE: NO OF IONISATION TRANSITIONS
C
C  INPUT : (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C  INPUT : (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            SIGNED PARENT      INDEX (CASE 'H' & 'R')
C  INPUT : (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C  INPUT : (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C  INPUT : (R*8)  SCOMN(,)= TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXNAME     ADAS      FINDS REAL NAME OF USER
C          XXWSTR     ADAS      WRITES STRING TO A UNIT WITH TRAILING
C                               BLANKS REMOVED
C
C
C  AUTHOR : H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL.  0141-553-4196
C
C  DATE:    30/11/01
C
C  UPDATE: 
CC
C----------------------------------------------------------------------
       INTEGER  NDLEV       , NDTRN          , NVMAX       , NDMET
       INTEGER  IUNT11      , npl
C----------------------------------------------------------------------
       logical  lbeth
C----------------------------------------------------------------------
       INTEGER   IZ          , IZ0            , IZ1        ,
     &           IL          , NVN            ,
     &           icnte       , icnts          ,
     &           IT          ,
     &           I           , IPOS           , IQS        ,
     &           L1          , L2
       INTEGER   NDECB       , NDECA          , DTYPE
       integer   IETRN(NDTRN), ISTRN(NDTRN)   , prtmax
C-----------------------------------------------------------------------
       REAL*8    bwnoa(ndmet), WNOMAX         , Z1    ,
     &           NULLA       , beth(ndtrn)
C----------------------------------------------------------------------
       INTEGER   ISA(NDLEV)     , ILA(NDLEV) ,
     &           I1A(NDTRN)  , I2A(NDTRN)     , IPLA(NDMET,NDLEV)
     
C----------------------------------------------------------------------
       REAL*8   SCEFN(NVMAX)        , KAPPA  , druval
       REAL*8   XJA(NDLEV)          , WA(NDLEV)           ,
     &          AVAL(NDTRN)         , SCOMUP(NVMAX,NDTRN)  ,
     &          SCOMDN(NVMAX,NDTRN) , ZPLA(NDMET,NDLEV)
C-----------------------------------------------------------------------
       CHARACTER DSNINP*80     , ADF37*80
       CHARACTER TITLED*3      , TCODE(NDTRN)*1 , CSTRGA(NDLEV)*18
       CHARACTER C9*9
       CHARACTER STRING*136    , BLANKS*133
       CHARACTER STRG2*44
C-----------------------------------------------------------------------
       CHARACTER DATE*8        , REALNAME*30
       CHARACTER CFORM7*7      , CFORM19*25     , CFORM20*20
       character cprta(ndmet)*9
C-----------------------------------------------------------------------
       parameter( nulla = 1.d-30 )
C-----------------------------------------------------------------------
       DATA  CFORM7/'(F15.?)'/
       DATA  CFORM19/'(1A3,I2,2I10,?(F15.?,A?))'/
       DATA  CFORM20/'(7X,"{",I1,"}",F4.2)'/
       DATA BLANKS/' '/
C-----------------------------------------------------------------------
       CALL XXNAME(REALNAME)
C       REALNAME = 'H. P. Summers'
       CALL XXSLEN(DSNINP,L1,L2)
C       
C-----------------------------------------------------------------------
C  ASSESS MAXIMUM DECIMAL PLACES FOR WAVE NUMBER PRINT OUT
C-----------------------------------------------------------------------
C
       wnomax=bwnoa(1)
       prtmax=len(cprta(1))
       do i=2,npl
         if(bwnoa(i).gt.wnomax) wnomax=bwnoa(i)
	 if(len(cprta(i)).gt.prtmax) prtmax=len(cprta(i))
       enddo
       do i=1,il
         if(wa(i).gt.wnomax) wnomax=wa(i)
       enddo
       ndecb=int(log10(float(int(wnomax))))
       ndeca=min(15-ndecb-2,4)
       write(cform7(6:6),'(i1)')ndeca
       write(cform19(14:14),'(i1)')npl
       write(cform19(20:20),'(i1)')ndeca       
       write(cform19(23:23),'(i1)')prtmax
C
C-----------------------------------------------------------------------
C  WRITE OUT FIRST LINE
C-----------------------------------------------------------------------
C       
       STRING = BLANKS
       WRITE(STRING,CFORM19)TITLED,IZ,IZ0,IZ1,(bwnoa(i),cprta(i),
     &   i=1,npl)
       CALL XXWSTR(IUNT11,STRING)
C
C-----------------------------------------------------------------------
C  WRITE OUT LEVEL INFORMATION
C-----------------------------------------------------------------------
C
       DO 80 I = 1,IL
         STRING = BLANKS
         STRG2  = BLANKS(1:44)
         WRITE(STRG2(1:16),CFORM7) WA(I)
         WRITE(STRG2(17:33),CFORM20) IPLA(1,I), ZPLA(1,I)
         WRITE(STRING,1000)I,CSTRGA(I),ISA(I),ILA(I),XJA(I),
     &                     STRG2
         CALL XXWSTR(IUNT11,STRING)
   80  CONTINUE
       CALL XXWSTR(IUNT11,'   -1')
C
C----------------------------------------------------------------------
C  WRITE OUT ZEFF, IQS AND TEMPERATURE LINE
C----------------------------------------------------------------------
C
       STRING = BLANKS
       Z1  = IZ1
       IF (DTYPE.EQ.0) THEN
         IQS = 3
       ELSE 
         IQS = 4
       ENDIF
       WRITE(STRING,'(F5.2,I5)')Z1,IQS
       IPOS = 0
       DO 90 IT = 1,NVN
         WRITE(C9,'(1PD9.2)') SCEFN(IT)
         STRING(17+IPOS:21+IPOS) = C9(1:5)
         STRING(22+IPOS:24+IPOS) = C9(7:9)
         IPOS =IPOS+8
   90  CONTINUE
       CALL XXWSTR(IUNT11,STRING)
C
C----------------------------------------------------------------------
C  WRITE OUT TRANSITION LINES
C----------------------------------------------------------------------
C
       DO 300 I = 1,icnte
         STRING = BLANKS
c         WRITE(STRING,'(A1,I3,I4)')tcode(ietrn(i)), I2A(ietrn(I)),
c     &                             I1A(ietrn(I))
         WRITE(STRING,'(1X,I3,I4)')I2A(ietrn(I)),I1A(ietrn(I))
	 WRITE(C9,'(1PD9.2)') AVAL(ietrn(I))
         STRING(9:13) = C9(1:5)
         STRING(14:16) = C9(7:9)
         IPOS = 0
         DO 210 IT = 1,NVN
           WRITE(C9,'(1PD9.2)') SCOMUP(IT,ietrn(I))
           STRING(17+IPOS:21+IPOS) = C9(1:5)
           STRING(22+IPOS:24+IPOS) = C9(7:9)
           IPOS =IPOS+8
  210    CONTINUE

         if (lbeth.and.iqs.eq.3) then
	   write(c9,'(1pd9.2)') beth(i)
	   string(17+ipos:21+ipos) = c9(1:5)
	   string(22+ipos:24+ipos) = c9(7:9)
	 endif
	 
         CALL XXWSTR(IUNT11,STRING)
  300  CONTINUE
C
       IF (IQS.EQ.4) THEN
         DO 400 I = 1,icnte
           STRING = BLANKS
c	   WRITE(STRING,'(A1,I3,I4)')tcode(ietrn(i)), I1A(ietrn(I)),
c     &                               I2A(ietrn(I))	   
	   WRITE(STRING,'(1X,I3,I4)')I1A(ietrn(I)),I2A(ietrn(I))	 
	   WRITE(C9,'(1PD9.2)') nulla
	   STRING(9:13) = C9(1:5)
	   STRING(14:16) = C9(7:9)
	   IPOS = 0
	   DO 310 IT = 1,NVN
	     WRITE(C9,'(1PD9.2)') SCOMDN(IT,ietrn(I))
	     STRING(17+IPOS:21+IPOS) = C9(1:5)
	     STRING(22+IPOS:24+IPOS) = C9(7:9)
	     IPOS = IPOS+8
C
  310      CONTINUE
           CALL XXWSTR(IUNT11,STRING)
  400    CONTINUE
       ENDIF
       
       
       
       
       DO 500 I = 1,icnts
         STRING = BLANKS
	 if (i1a(istrn(i)).le.9) WRITE(STRING,'(A1,I3,2X,A1,I1)')
     &     tcode(istrn(i)), I2A(istrn(I)),'+',I1A(istrn(I))
         if (i1a(istrn(i)).gt.9) WRITE(STRING,'(A1,I3,1X,A1,I2)')
     &     tcode(istrn(i)), I2A(istrn(I)),'+',I1A(istrn(I))
         WRITE(C9,'(1PD9.2)') nulla
         STRING(9:13) = C9(1:5)
         STRING(14:16) = C9(7:9)
         IPOS = 0
         DO 510 IT = 1,NVN
           WRITE(C9,'(1PD9.2)') SCOMUP(IT,istrn(I))
           STRING(17+IPOS:21+IPOS) = C9(1:5)
           STRING(22+IPOS:24+IPOS) = C9(7:9)
           IPOS =IPOS+8
  510    CONTINUE
C
         CALL XXWSTR(IUNT11,STRING)
  500  CONTINUE
C
       IF (IQS.EQ.4) THEN
         DO 600 I = 1,icnts
           STRING = BLANKS
	   if (i1a(istrn(i)).le.9) WRITE(STRING,'(A1,1X,A1,I1,I4)')
     &       tcode(istrn(i)),'+',I1A(istrn(I)),I2A(istrn(I))
           if (i1a(istrn(i)).gt.9) WRITE(STRING,'(A1,A1,I2,I4)')
     &       tcode(istrn(i)),'+',I1A(istrn(I)),I2A(istrn(I))
	      
	   WRITE(C9,'(1PD9.2)') nulla
	   STRING(9:13) = C9(1:5)
	   STRING(14:16) = C9(7:9)
	   IPOS = 0
	   DO 610 IT = 1,NVN
	     WRITE(C9,'(1PD9.2)') SCOMDN(IT,istrn(I))
	     STRING(17+IPOS:21+IPOS) = C9(1:5)
	     STRING(22+IPOS:24+IPOS) = C9(7:9)
	     IPOS = IPOS+8
C
  610      CONTINUE
           CALL XXWSTR(IUNT11,STRING)
  600    CONTINUE
       ENDIF
       
       
       WRITE(IUNT11,'(1A4)')'  -1'
       WRITE(IUNT11,'(1A8)')'  -1  -1'
C
       WRITE(IUNT11,1003) IQS,'ADAS809',DSNINP(1:L2)
       IF (DTYPE.EQ.0) WRITE(IUNT11,1004) 'Maxwellian'
       IF (DTYPE.EQ.1) THEN
         WRITE(IUNT11,1004) 'Kappa'
         WRITE(IUNT11,1005)  KAPPA
       ENDIF
       IF (DTYPE.EQ.2) THEN
         WRITE(IUNT11,1004) 'Numerical'
         WRITE(IUNT11,1006) ADF37
       ENDIF
       IF (DTYPE.EQ.3) THEN
         WRITE(IUNT11,1004) 'Druyvesteyn'
         WRITE(IUNT11,1008) druval
       ENDIF
       WRITE(IUNT11,1007) REALNAME,DATE
C
       CALL FLUSH(IUNT11)
C       
       RETURN
C
C----------------------------------------------------------------------
 1000  FORMAT(I5,1X,1A18,'(',I1,')',I1,'(',F5.2,')',1A44)
 1001  FORMAT(1A3,I2,2I10,F15.1)
 1003  FORMAT('C',79('-')/
     &        'C'/
     &        'C  File generated by converting an ADF04 dataset ',
     &        'from type 1 to type ',1I1/
     &        'C'/
     &        'C  Program       : ',1A7/
     &        'C'/
     &        'C  Source file   : ',A,/
     &        'C')
 1004  FORMAT('C  Distribution  : ',A)
 1005  FORMAT('C',18(' '),'K = ',F8.5,/'C')
 1006  FORMAT('C',8(' '),'From Adf37: ',A,/'C')
 1007  FORMAT('C  Producer      : ',A,/
     &        'C'/
     &        'C  Date          : ',1A8/
     &        'C',79('-'))
 1008  FORMAT('C',18(' '),'x = ',F8.5,/'C')
C----------------------------------------------------------------------
C
      END
