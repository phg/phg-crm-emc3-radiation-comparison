      SUBROUTINE H9ISPF( LPEND    , NDTIN    , 
     &                   IL       , NDTEM    ,
     &                   NV       , TSCEF    , TSCEF2   ,
     &                   ITRAN    , I1A      , I2A      ,
     &                   FTYPE    , TITLE    ,
     &                   ISTRN    , IFOUT    ,
     &                   MAXT     , TINE     ,
     &                   LFSEL    , LOSEL    ,
     &                   TOLVAL   ,
     &                   DTYPE    , KAP_VAL  , DRU_VAL  ,
     &                   ADF37
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL AND TO RETURN USER SELECTED 
C           OPTIONS AND VALUES.
C
C  CALLING PROGRAM: ADAS809
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF TEMPERATURES ALLOWED
C
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NDTEM    = INPUT DATA FILE: MAX. NO. OF TEMPERATURES
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER   OF TEMPERATURES
C
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES, OR
C                             2/3 AVERAGE ENERGY OF A NUMERICAL 
C                             DISTRIBUTION
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  INPUT : (R*8)   TSCEF2(,)= INPUT DATA FILE: MOST COMMON ELECTRON
C                            'TEMPERATURE' FOR A NUMERICAL DISTRIBUTION,
C                             OR A ZERO ARRAY IN ANALYTIC CASE
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  INPUT : (I*4)   ITRAN    = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (I*4)   I1A()    = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C
C  INPUT : (I*4)   I2A()    = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C
C  INPUT : (C*6)   FTYPE    = TYPE OF FILE SELECTED AS COMPARISON
C
C  OUTPUT: (C*40)  TITLE    = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   ISTRN    = SELECTED ELECTRON IMPACT TRANSITION INDEX
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT.
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C
C  OUTPUT: (I*8)   DTYPE    = DISTRIBUTION TYPE SUCH THAT:
C                             0 => MAXWELLIAN
C                             1 => KAPPA
C                             2 => NUMERICAL
C  OUTPUT: (R*8)   KAP_VAL  = VALUE OF KAPPA
C
C
C
C 	   (I*4)   ILOGIC   = RETURN VALUE FROM IDL WHICH IS USED TO 
C			      REPRESENT A LOGICAL VARIABLE SINCE IDL
C                             DOES HAVE SUCH DATA TYPES.
C	   
C	   (I*4)   I	    = GENERAL PURPOSE COUNTER
C
C	   (I*4)   J	    = GENERAL PURPOSE COUNTER
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS 'FLUSH' TO CLEAR PIPE
C
C AUTHOR:  HUGH SUMMERS (UNIV. OF STRATHCLYDE)
C
C DATE:    30/11/01
C
C DATE:   07/04/05
C MODIFIED: ALLAN WHITEFORD
C		- ADDED FORMAT STATEMENT TO READING FROM PIPE
C-----------------------------------------------------------------------
      INTEGER    NDTIN      , IL         , NDTEM     , NV    ,
     &           ITRAN      , ISTRN      , IFOUT     , MAXT
C-----------------------------------------------------------------------
      REAL*8     TOLVAL
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40   , FTYPE*6    , ADF37*80
C-----------------------------------------------------------------------
      LOGICAL    LPEND
      LOGICAL    LFSEL      , LOSEL 
C-----------------------------------------------------------------------
      INTEGER    I1A(ITRAN) , I2A(ITRAN), DTYPE  
C-----------------------------------------------------------------------
      REAL*8     TSCEF(NDTEM,3)         , TINE(NDTIN) ,
     &           TSCEF2(NDTEM,3)
C-----------------------------------------------------------------------
      REAL*8     KAP_VAL   , DRU_VAL
C----------------------------------------------------------------------
      INTEGER    PIPEOU     , PIPEIN
      PARAMETER  (PIPEOU=6  , PIPEIN=5)
C-----------------------------------------------------------------------
      INTEGER    I          , J         , ILOGIC
C-----------------------------------------------------------------------
C  WRITE VARIABLES OUT TO IDL VIA PIPE
C-----------------------------------------------------------------------
C
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) NDTIN
      WRITE(PIPEOU, *) IL
      WRITE(PIPEOU, *) NDTEM
      WRITE(PIPEOU, *) NV
      WRITE(PIPEOU, *) ITRAN
      WRITE(PIPEOU, *) FTYPE
C
      CALL XXFLSH(PIPEOU)
C
      DO J = 1, 3
	 DO I = 1, NDTEM
            WRITE(PIPEOU, *) TSCEF(I,J)
         ENDDO
      ENDDO
      CALL XXFLSH(PIPEOU)
C      
      DO J = 1, 3
        DO I = 1, NDTEM
	  WRITE(PIPEOU, *) TSCEF2(I,J)
	ENDDO
      ENDDO
C
      CALL XXFLSH(PIPEOU)
C
      DO I = 1, ITRAN
         WRITE(PIPEOU, *) I1A(I)
      ENDDO
C
      CALL XXFLSH(PIPEOU)
C
      DO I = 1, ITRAN
         WRITE(PIPEOU, *) I2A(I)
      ENDDO
C
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C     READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN, '(I4)') ILOGIC
C
      CALL XXFLSH(PIPEOU)
      IF (ILOGIC .EQ. 1 ) THEN
         LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      END IF
C
      READ(PIPEIN,'(A)') TITLE
      READ(PIPEIN,*) ISTRN
C
C CHANGE IDL INDEX TO FORTRAN INDEX 
C
      ISTRN = ISTRN + 1
      READ(PIPEIN,*) IFOUT
      READ(PIPEIN,*) MAXT
      READ(PIPEIN,*) (TINE(I), I=1, MAXT)
      READ(PIPEIN, *) ILOGIC
C
      IF (ILOGIC .EQ. 1) THEN 
	 LFSEL = .TRUE.
         READ(PIPEIN,*) TOLVAL
         TOLVAL = TOLVAL /100.d0
      ELSE
         LFSEL = .FALSE.
      ENDIF
C
      READ(PIPEIN, *) ILOGIC
C
      IF (ILOGIC .EQ. 1) THEN 
         LOSEL = .TRUE.
      ELSE 
         LOSEL = .FALSE.
      ENDIF
C
C READ DISTRIBUTION TYPE
C
      READ(PIPEIN,*) DTYPE
C
C SET DISTRIBUTION PARAMETERS TO NEUTRAL VALUES
C      
      KAP_VAL = 0.d0
C
C READ DISTRIBUTION PARAMETER(S)
C
      IF (DTYPE.EQ.1) THEN
        READ(PIPEIN,*) KAP_VAL
C
      elseIF (DTYPE.EQ.2) THEN
        READ(PIPEIN,'(A)') ADF37
C
      elseIF (DTYPE.EQ.3) THEN
        READ(PIPEIN,*) DRU_VAL
      ENDIF
C
C READ QUADRATURE OPTIONS
C
c      READ(PIPEIN,*) ILINR
c      READ(PIPEIN,*) IFINT
c      READ(PIPEIN,*) IESCL
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
C
      END
