      SUBROUTINE H9SPF1( LDEF1      ,  
     &                   LGRAPH     , L2FILE     , A2FILE   , SAVFIL ,
     &                   ADFFIL     , XMIN       , XMAX     , YMIN   , 
     &                   YMAX       , LPEND      , LREP     , AREP
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS809
C
C  SUBROUTINE:
C
C  OUTPUT:   (L*4)   LDEF1    = .TRUE.  => USER SLECTED AXES LIMITS
C				.FALSE. => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (C*80)  ADFFIL   = FILENAME FOR OUTPUT ADF04 FILE
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR TEMP OR DENSITY UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR TEMP OR DENSITY UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS IONIZATIONS/PHOTON
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT IONIZATIONS/PHOTON
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (L*4)   LREP     = .TRUE.  => REPLACE PAPER.TXT
C  				.FALSE. => DON'T
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  HUGH SUMMERS (UNIVERSITY OF STRATHLCYDE)
C          JA7.08
C          EXT. 4196
C
C DATE:    14/06/01 
C
C MODIFICATION HISTORY:
C
C DATE:   08/08/03			VERSION: 1.2
C MODIFIED: MARTIN TORNEY
C	         - ADDED OPTION TO CREATE NAMED ADF04 FILE
C DATE:   07/04/05			VERSION: 1.3
C MODIFIED: ALLAN WHITEFORD
C	         - ADDED FORMAT STATEMENT TO READING FROM PIPE
C
C-----------------------------------------------------------------------
      REAL*8       XMIN        , XMAX        , YMIN        , YMAX
C-----------------------------------------------------------------------
      CHARACTER    SAVFIL*80   , ADFFIL*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      ,
     &             LDEF1       , LREP        ,
     &             AREP        , A2FILE
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      ,
     &             PIPEIN      , PIPEOU      , ONE
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1 )
C
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LGRAPH = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               LDEF1  = .TRUE.
	       READ(PIPEIN,*) XMIN
	       READ(PIPEIN,*) XMAX
	       READ(PIPEIN,*) YMIN
	       READ(PIPEIN,*) YMAX
	    ELSE
	       LDEF1  = .FALSE.
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF
C
C  CREATE PAPER FILE
C
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
            READ(PIPEIN,*) ILOGIC
            IF (ILOGIC .EQ. ONE) THEN
               LREP = .TRUE.
            ELSE
               LREP = .FALSE.
            ENDIF
	    L2FILE = .TRUE.
            READ(PIPEIN, *) SAVFIL
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
C  CREATE ADF04 FILE
C	 
	 READ(PIPEIN,*) ILOGIC
	 IF (ILOGIC .EQ. ONE) THEN
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN
	       AREP = .TRUE.
	    ELSE
	       AREP = .FALSE.
	    ENDIF
	    A2FILE = .TRUE.
	    READ(PIPEIN,'(A80)') ADFFIL
	 ELSE
	    A2FILE = .FALSE.
	 ENDIF   
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
