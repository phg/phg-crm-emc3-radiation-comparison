      SUBROUTINE H9SPLN( NDTRN   , NDTEM  ,
     &                   NDTIN   , 
     &                   NV      , MAXT    , NPSPL  ,
     &                   IETRN   , ICNTE   , DTYPE  ,
     &                   ISTRN   , ICNTS   ,
     &                   USCEF   , TOA     , TOSA   ,
     &                   SCOM    ,
     &                   GAMMA   , GAMOSA  ,
     &                   LTRNG   , ITRN
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: H9SPLN *********************
C
C  PURPOSE:
C         1) PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE) VERSUS LOG(GAMMA)
C            INPUT DATA. ('SCEF' VERSUS 'GAMMA' , NV DATA PAIRS)
C
C         2) INTERPOLATES 'MAXT'  GAMMA VALUES USING ABOVE SPLINES AT
C            TEMPERATURES READ IN FROM ISPF PANELS FOR TABULAR OUTPUT.
C            (ANY TEMPERATURE VALUES WHICH REQUIRED EXTRAPOLATION TO
C             TAKE PLACE ARE SET TO ZERO).
C
C         3) INTERPOLATES 'NPSPL' GAMMA VALUES USING ABOVE SPLINES AT
C            TEMPERATURES EQUI-DISTANCE ON RANGE OF LOG(TEMPERATURES)
C            STORED IN INPUT 'SCEF' ARRAY.
C
C  CALLING PROGRAM: ADAS809
C
C
C  SUBROUTINE:
C
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS READ FOR THE TRANSITION BEING ASSESSED
C  INPUT : (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES AT
C                           WHICH INTERPOLATED GAMMA VALUES ARE REQUIRED
C                           FOR TABULAR OUTPUT.
C  INPUT : (I*4)  NPSPL   = NUMBER OF  SPLINE  INTERPOLATED  GAMMA/TEMP.
C                           REQUIRED FOR GRAPHICAL DISPLAY.
C
C  INPUT : (I*4)  SCEF()  = INPUT DATA FILE: TEMPERATURES (KELVIN)
C  INPUT : (I*4)  TOA()   = ISPF PANEL ENTERED TEMPERATURES (KELVIN)
C  OUTPUT: (I*4)  TOSA()  = 'NPSPL' TEMPERATURES FOR GRAPHICAL OUTPUT
C                           (KELVIN).
C
C  INPUT : (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                           GAMMA VALUES AT 'SCEF()'.
C  OUTPUT: (R*8)  GAMOSA()= SPLINE INTERPOLATED GAMMA VALUES AT 'TOSA()'
C
C  OUTPUT: (L*4)  LTRNG() = .TRUE.  => OUTPUT   SPLINE    VALUE    WAS
C                                      INTERPOLATED FOR 'DLOG(TOA())'.
C                           .FALSE. => OUTPUT   SPLINE    VALUE    WAS
C                                      EXTRAPOLATED FOR 'DLOG(TOA())'.
C                                      (NOTE: 'YOUT()=0' AS 'IOPT < 0').
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF  INPUT  TEMP/GAMMA
C                                      PAIRS MUST BE >= 'NV'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF 'OUTPUT TEMP/GAMMA
C                                      PAIRS MUST BE >= 'MAXT' & 'NPSPL'
C
C          (I*4)  IARR    = ARRAY SUBSCRIPT USED FOR TEMP/GAMMA PAIRS
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (R*8)  TSTEP   = THE SIZE OF STEP BETWEEN 'XOUT()' VALUES FOR
C                           GRAPHICAL  OUTPUT  TEMP/GAMMA  PAIRS  TO  BE
C                           CALCULATED USING SPLINES.
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  XIN()   = LOG( 'SCEF()' )
C          (R*8)  YIN()   = LOG( 'GAMMA()' )
C          (R*8)  XOUT()  = LOG(TEMPERATURES AT WHICH SPLINES REQUIRED)
C          (R*8)  YOUT()  = LOG(OUTPUT SPLINE INTERPOLATED GAMMA VALUES)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C          (L*4)  LDUMP() = .TRUE.  => OUTPUT SPLINE VALUE INTRPOLATED
C                                      FOR 'YOUT()'.
C                           .FALSE. => OUTPUT SPLINE VALUE EXTRAPOLATED
C                                      FOR 'YOUT()'.
C                                      (NOTE: USED AS A DUMMY ARGUMENT.
C                                             ALL VALUES WILL BE TRUE.)
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  PAUL BRYANS (UNIVERSITY OF STRATHCLYDE)
C
C DATE:    09/09/03
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN          , NOUT          , NDTIN  ,
     &           NDTRN        , NDTEM         , ITRN   ,
     &           IETRN(NDTRN) , ICNTE         , DTYPE  ,
     &           ISTRN(NDTRN) , ICNTS
C-----------------------------------------------------------------------
      PARAMETER( NIN = 51     , NOUT = 100    )
C-----------------------------------------------------------------------
      INTEGER    NV           , MAXT          , NPSPL
      INTEGER    IOPT         , IARR          , IC
C-----------------------------------------------------------------------
      REAL*8     R8FUN1       , TSTEP
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     SCOM(NDTEM,NDTRN)
      REAL*8     USCEF(NDTEM)    , GAMMA(NDTEM)     ,
     &           TOA(ndtin)    ,
     &           TOSA(NPSPL)  , GAMOSA(NPSPL)
      REAL*8     XIN(NIN)     , YIN(NIN)      , YINUP(NIN)          ,
     &           XOUT(NOUT)   , YOUT(NOUT)
      REAL*8     DF(NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(nout)  , LDUMP(NOUT)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
      IF (NIN.LT.NV)
     &             STOP ' B1SPLN ERROR: NIN < NV - INCREASE NIN'
      IF (NOUT.LT.MAXT)
     &             STOP ' B1SPLN ERROR: NOUT < MAXT - INCREASE NTOUT'
      IF (NOUT.LT.NPSPL)
     &             STOP ' B1SPLN ERROR: NOUT < NPSPL - INCREASE NGOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      IOPT  = 0
C
C-----------------------------------------------------------------------
C  INTERPOLATE SPLINED TEMP/GAMMA PAIRS FOR EACH TRANSITION IN THE CASE
C  OF NUMERICAL DISTRIBUTION FILE. CONVERTS FROM ADF37 TEMPERATURES TO
C  ISPF PANEL TEMPERATURES.
C-----------------------------------------------------------------------
C
      IF (DTYPE.EQ.2) THEN
        DO 1 IC=1,ICNTE
	  DO 2 IARR=1,NV
	    XIN(IARR) = DLOG(USCEF(IARR))
    2	    YINUP(IARR) = DLOG(SCOM(IARR,IETRN(IC)))

	  DO 3 IARR=1,MAXT
    3	    XOUT(IARR) = DLOG(TOA(IARR))

	  LSETX = .TRUE.
	  CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &  	       NV    , XIN    , YINUP  ,
     &  	       NPSPL , XOUT   , YOUT   ,
     &  	       DF    , LTRNG
     &  	     )

	  DO 4 IARR=1,MAXT
    4	    SCOM(IARR,IETRN(IC)) = DEXP(YOUT(IARR))

    1	CONTINUE
        
	DO 11 IC=1,ICNTS
	  DO 12 IARR=1,NV
	    XIN(IARR) = DLOG(USCEF(IARR))
   12	    YINUP(IARR) = DLOG(SCOM(IARR,ISTRN(IC)))

	  DO 13 IARR=1,MAXT
   13	    XOUT(IARR) = DLOG(TOA(IARR))

	  LSETX = .TRUE.
	  CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &  	       NV    , XIN    , YINUP  ,
     &  	       NPSPL , XOUT   , YOUT   ,
     &  	       DF    , LTRNG
     &  	     )

	  DO 14 IARR=1,MAXT
   14	    SCOM(IARR,ISTRN(IC)) = DEXP(YOUT(IARR))

   11	CONTINUE
	
	
	DO 5 IARR=1,MAXT
    5     GAMMA(IARR) = SCOM(IARR,IETRN(ITRN))
      ENDIF      
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH OFF EXTRAPOLATION.
C-----------------------------------------------------------------------
C             
      LSETX = .TRUE.
      IOPT  = -1
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT GAMMA/TEMP PAIRS
C-----------------------------------------------------------------------
C
      DO 6 IARR=1,maxt
        XIN(IARR) = DLOG( TOA(IARR)   )
        YIN(IARR) = DLOG( GAMMA(IARR) )
    6 CONTINUE
C	 
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED TEMP/GAMMA PAIRS FOR GRAPH OUTPUT- 'NPSPL' VALUES
C-----------------------------------------------------------------------
C
      TSTEP = ( XIN(NV)-XIN(1) ) / DBLE( NPSPL-1 )
C
      DO 7 IARR=1,NPSPL
	XOUT(IARR) = ( DBLE(IARR-1)*TSTEP ) + XIN(1)
    7 CONTINUE


C
      CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &             NV    , XIN    , YIN    ,
     &  	   NPSPL , XOUT   , YOUT   ,
     &  	   DF    , LDUMP
     &  	 )
C

      DO 8 IARR=1,NPSPL
	TOSA(IARR)	= DEXP( XOUT(IARR) )
	GAMOSA(IARR) = DEXP( YOUT(IARR) )
    8 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
