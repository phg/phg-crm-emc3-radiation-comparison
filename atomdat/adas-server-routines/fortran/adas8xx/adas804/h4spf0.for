C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4spf0.for,v 1.1 2004/07/06 14:00:56 whitefor Exp $ Date $Date: 2004/07/06 14:00:56 $
C
       subroutine h4spf0( rep     , dsfull  ,
     &                    iasel   , indxref
     &                  )

       implicit none

c-----------------------------------------------------------------------
c
c  ******************* fortran77 subroutine: h4spf0 ********************
c
c  purpose: pipe communications with idl. reads archiving option
c           and index/archive name if requested.
c
c  calling program: adas804
c
c  input:
c          none
c  output:
c          (c*3)  rep    = 'yes' - finish program run
c                          'no ' - continue
c          (c*80) dsfull = the selected archive name.
c          (i*4)  iasel  = the archiving option
c                          0 = ignore archiving.
c                          1 = examine old archive and possibly
c                              add new data.
c                          2 = refresh transition data from 
c                              old archive (requires index no.)
c                          3 = create a new archive.
c          (i*4)  indxref= the index number to refresh data from.
c
c  routines:
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    8 June 2002
c
c
c  version: 1.1   Hugh Summers  08/06/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       character rep*3   , dsfull*80
c-----------------------------------------------------------------------
       integer iasel   , indxref
       integer pipein  , pipeou
c-----------------------------------------------------------------------
       parameter( pipein=5, pipeou=6 )
c-----------------------------------------------------------------------
c  read outputs from pipe
c-----------------------------------------------------------------------
c
       dsfull = ' '
       
       read(pipein,'(1a3)') rep
       read(pipein,'(i5)') iasel
       if (iasel.ne.0)then
           read(pipein,'(a)') dsfull
           if (iasel.eq.2)then
               read(pipein,'(i5)') indxref
           endif
       endif
c
c-----------------------------------------------------------------------
c
       return
c
       end
