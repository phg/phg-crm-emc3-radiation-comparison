C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4lnft.for,v 1.1 2004/07/06 14:00:33 whitefor Exp $ Date $Date: 2004/07/06 14:00:33 $
C
       subroutine h4lnft( istdim ,
     &                    x      , xsa   , y     , ysa   , ict
     &                  )
       implicit none                                         
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4lnft.for *********************
c
c  purpose:  perform linear interpolation
c
c  
c  input : (r*8)  x     = required x-value
c  input : (r*8)  xsa(i)= x-values
c  input : (r*8)  ysa(i)= y-values
c  input : (i*4)  ict   = number of values
c   
c  input : (r*8)  y     = returned y-value
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c-----------------------------------------------------------------------
       integer  nstdim
c-----------------------------------------------------------------------
       parameter ( nstdim = 40)
c-----------------------------------------------------------------------
       integer  istdim , ict   , i
       integer  i4unit 
c-----------------------------------------------------------------------
       real*8   x      , y   
c-----------------------------------------------------------------------
       real*8   xsa(istdim)    , ysa(istdim)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  check internal dimensions are compatible
c-----------------------------------------------------------------------

       if(nstdim.lt.istdim) then
           write(i4unit(-1),2002)' ','nstdim=',nstdim,
     &                           ' < istdim=',istdim
	   write(i4unit(-1),2003)
	   stop
       endif
       
       if(ict.gt.1)go to 10                                             
       y=ysa(1)                                                         
       return                                                           
   10  if(x.gt.xsa(1))go to 20                                          
       y=ysa(1)                                                         
       return                                                           
   20  if(x.lt.xsa(ict))go to 30                                        
       y=ysa(ict)                                                       
       return                                                           
   30  i=0                                                              
   40  i=i+1                                                            
       if(x.gt.xsa(i))go to 40                                          
       y=((x-xsa(i))*ysa(i-1)-(x-xsa(i-1))*ysa(i))/(xsa(i-1)            
     &-xsa(i))                                                          
       
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4lnft warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4lnft error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
                                    
       return                                                           
      end                                                               
