      FUNCTION VPOTL(Z0,ZC,ALF,X,V,Z1,Z2,Z3,H,X1)
      IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C PURPOSE: Unknown.
C
C-----------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 08-04-2010
C  MODIFIED : Martin O'Mullane
C              - Remove listing information from colums 72+.
C              - Change from "real function vpotl*8" to 
C                "function vpotl".
C              - Add comment.
C-----------------------------------------------------------------------
      DIMENSION V(1000)
      IF (X-X1) 2,2,1
1     T = ALF*X
      T1 = 1.0D0/X
      T2 = T1*T1
      VPOTL = 2.0D0*(Z1+T2*(Z2+T2*Z3)+(Z0-ZC)*DEXP(-0.2075D0*T)
     1       /(1.0D0+1.19D0*T))*T1
      RETURN
2     J = X/H
      T = J
      T = T*H
      J1 = X1/H + 0.5D0
      IF (J-J1+1) 4,3,1
3     J = J - 1
      T = T - H
4     IF (J-1) 5,6,7
5     J = 1
      T = H
6     T1 = Z0 + Z0
      GO TO 8
7     T1 = (T-H)*V(J-1)
8     T2 = T*V(J)
      T3 = (T+H)*V(J+1)
      T4 = (T+H+H)*V(J+2)
      P = (X-T)/H
      VPOTL = (P*(P-1.0D0)*((P+1.0D0)*T4-(P-2.0D0)*T1)/6.0D0
     1       +(P+1.0D0)*(P-2.0D0)*((P-1.0D0)*T2-P*T3)*0.5D0)/X
      RETURN
      END
