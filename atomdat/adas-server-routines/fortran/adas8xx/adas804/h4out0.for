C UNIX-IDL - SCCS info: Module @(#)h4out0.for	1.1 Date 03/18/03
C
       subroutine h4out0( ixdim   , itdim   , isdim   ,
     &                    dsfull  , indxref , title   , cameth ,
     &                    z0      , z       , zeff    ,
     &                    n1      , l1      , eb1     ,
     &                    n2      , l2      , eb2     ,
     &                    isp     , lp      , xjp     ,
     &                    ist1    , lt1     , xj1     , xjt1   ,
     &                    ist2    , lt2     , xj2     , xjt2   ,
     &                    neqv1   , fpc1    , neqv2   , fpc2   ,
     &                    aval    ,
     &                    xmax    , iextwf  , ijucys  , isrch  ,
     &                    nshell  , ns      , ls      , iqs    , alfaa ,
     &                    nx      , xa      , omga    ,
     &                    nt      , tea     , upsa    ,
     &                    date    , ianopt  , iasel   ,
     &                    cheader , texdsn  , itexout , larch
     &                )
       implicit none
C-----------------------------------------------------------------------
C
C  ********************* fortran77 subroutine h4out0*******************
C
C  Purpose:
C        To write data to an old/new archive in the format decided
C        by the analysis option.
C
C  Calling program:
C        adas804.for
C
C  Data:
C        ciarr(500)*80- 500 is the current limit on indexes
C
C  input:
C            (i*4)  ixdim    = maximum dimension for X  array
C            (i*4)  itdim    = maximum dimension for Te array
C            (i*4)  isdim    = maximum dimension for shell vectors
C
C            (c*80) dsfull   - the users' chosen archive file name.
C            (i*4)  indxref  - the index number to refresh from.
C            (c*40) title    - the information line in the archive
C                              file.
C            (c*1)  cameth   = the tag distinguishing the type of
C                              analysis: a - Born, b- IP
C            (r*8)  z0       = nuclear charge
C            (r*8)  z        = ion charge
C            (r*8)  zeff     = ion charge +1
C            (i*4)  n1       = lower n-shell of transition
C            (i*4)  l1       = lower l-shell of transition
C            (r*8)  eb1      = binding energy (Ryd) in lower level
C            (i*4)  n2       = upper n-shell of transition
C            (i*4)  l2       = upper l-shell of transition
C            (r*8)  eb2      = binding energy (Ryd) in upper level
C            (i*4)  isp      = 2*Sp+1 for parent
C            (i*4)  lp       = Lp for parent
C            (r*8)  xjp      = Jp for parent (if 'ic' coupling)
C            (i*4)  ist1     = 2*S+1 for lower state
C            (i*4)  lt1      = L for lower state
C            (r*8)  xj1      = j for lower state
C            (r*8)  xjt1     = J for lower state
C            (i*4)  ist2     = 2*S'+1 for upper state
C            (i*4)  lt2      = L' for upper state
C            (r*8)  xj2      = j' for upper state
C            (r*8)  xjt2     = J' for upper state
C            (i*4)  neqv1    = no. of equiv. electrons for lower shell.
C            (r*8)  fpc1     = fract. parentage for lower state
C            (i*4)  neqv2    = no. of equiv. electrons for upper shell.
C            (r*8)  fpc2     = fract. parentage for upper state
C            (i*4)  aval     = A-value (sec-1) if dipole; else -ve
C            (i*4)  xmax     = range of numerical wave functions
C            (i*4)  iextwf   = 0 => calculate radial wave functions
C                            = 1 => read in radial wave functions
C            (i*4)  ijucys   = -1 => Jucys potential form adopted
C                            = 0  => Slater potential form adopted
C            (i*4)  isrch    = 0 => fcf6 search for energy eigenvalue
C                            = 1 => fcf6 search for scaling parameters
C            (i*4)  nshell   = number of screening shells
C            (i*4)  n        =  n for each screening shell
C                               1st dim: screening shell index
C            (i*4)  ls       = l for each screening shell
C                               1st dim: screening shell index
C            (i*4)  iqs      = iq for each screening shell
C                               1st dim: screening shell index
C            (i*4)  alfaa    = scaling factor for each screening shell
C                               1st dim: index for lower & upper state
C                               2nd dim: index over screening shells
C            (i*4)  nx       = number of incident electron energies
C            (r*8)  xa()     = threshold parameter values
C            (r*8)  omga()   = omegas at input values of x-parameter
C            (i*4)  nt       = number of electron temperatures
C            (r*8)  tea()    = electron temperatures (K)
C            (r*8)  upsa()   = upsilon at onput values of Te
C            (c*8)  date   = the current data
C            (i*4)  ianopt = analysis option
C                            1 - adas option
C                            2 - burgess option
C            (i*4)  iasel  = the archiving choice (see h4spf0.f)
C            (c*80) cheader= the adas header for the paper.txt file
C            (c*80) texdsn = the default hard copy file name
C            (i*4)  itexout= the hard copy of file output flag
C                            0 - no, 1 - yes
C            (l)    larch  = archiving selection option
C
C
C  Routines:
C        none
C
C
C  Author:  H. P. Summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   24/02/03
C
C  Update: HP Summers  21/05/04  correct errors, separate archive and
C                                standard output handling so that
C                                h4spf0.for handles archiving only
C
C
C
C-----------------------------------------------------------------------
       integer ixdim   , itdim  , isdim
       integer nx      , nt
       integer indxref , ichk1  , indl  , indu   , ict   , itout
       integer indx    , ianopt , i     , icnt
       integer iasel   , itexout, ihflag
       integer n1      , l1     , n2    , l2     ,
     &         isp     , lp     ,
     &         ist1    , lt1    , ist2  , lt2    ,
     &         neqv1   , neqv2  ,
     &         iextwf  , ijucys , isrch ,
     &         nshell
C-----------------------------------------------------------------------
       integer   ns(isdim)       , ls(isdim)      , iqs(isdim)
C-----------------------------------------------------------------------
       character dsfull*80 , cstring*40 , cstrng*80
       character title*40  , cameth*1   , date*8       , code*1
       character dcode(2)*1, cindx*3    , ciarr(500)*80
       character cheader*80, texdsn*80  , cmethod*12
C-----------------------------------------------------------------------
       integer iwrite      , pipein     , pipeou
C-----------------------------------------------------------------------
       parameter( iwrite = 9 , pipein = 5 , pipeou = 6 )
C-----------------------------------------------------------------------
       integer i4unit , iaselcopy
C-----------------------------------------------------------------------
       logical larch
C-----------------------------------------------------------------------
       real*8 z    , z0   , zeff ,
     &        eb1  , eb2  ,
     &        xjp  , xj1  , xjt1      , xj2     , xjt2    ,
     &        fpc1 , fpc2 ,
     &        aval , xmax
C-----------------------------------------------------------------------
       real*8    alfaa(2,isdim)
       real*8    xa(ixdim)    , omga(ixdim)
       real*8    tea(itdim)   , upsa(itdim)
C-----------------------------------------------------------------------
       data dcode/'A','B'/
C-----------------------------------------------------------------------

       open( unit=iwrite , file = dsfull , status = 'unknown' )

       if (iasel.ne.3) then
C
C-----------------------------------------------------------------------
C  check for highest index
C-----------------------------------------------------------------------
C
 1         read(iwrite,1015) cstrng
           if (cstrng(1:1).ne.' '.and.cstrng(1:1).ne.'-') then
               if ((cstrng(1:1).ne.'c').and.
     &             (cstrng(1:1).ne.'C')) then
                   cindx = cstrng(2:4)
                   go to 1
               else
                   cindx = cindx
               endif
           else
               go to 1
           endif
C
C-----------------------------------------------------------------------
C  copy index comments to temporary array
C-----------------------------------------------------------------------
C
           ciarr(1) = cstrng
           icnt = 2
 2         read(iwrite,1015) cstrng
           if ((cstrng(1:2).eq.'c ').or.
     &         (cstrng(1:2).eq.'C ')) then
               ciarr(icnt) = cstrng
               icnt = icnt+1
               go to 2
           else
               ciarr(icnt) = cstrng
               icnt = icnt+1
           endif
C
C-----------------------------------------------------------------------
C  close file off to re-open for position finding
C-----------------------------------------------------------------------
C
           close( unit=iwrite , status = 'keep')
           open( unit=iwrite , file = dsfull , status = 'unknown' )
 3         read(iwrite,1015) cstrng
           if ((cstrng(1:1).eq.'i').or.
     &         (cstrng(1:1).eq.'I'))  then
               if (cstrng(2:4).ne.cindx) then
                   go to 3
               endif
           else
               go to 3
           endif
C
 4         read(iwrite,1015) cstrng
           if (cstrng(1:1).ne.'-') then
               go to 4
           endif
C
C  clock index up one
C
           read(cindx,1016) indx
           indx = indx+1
       else
               indx = 1
       endif
C
C-----------------------------------------------------------------------
C  now write out data
C-----------------------------------------------------------------------
C
         if ((cameth.eq.'a').or.(cameth.eq.'A')) then
             code = dcode(1)
             cmethod = 'EFFPOT-BORN  '
         elseif ((cameth.eq.'b').or.(cameth.eq.'B')) then
             code = dcode(2)
             cmethod = 'IMPACT-PARM  '
         endif
 100     if ((cameth.eq.'a').or.(cameth.eq.'A')) then

             write(cstring,1000) title
             write(iwrite,1001) indx, cstring, date, code
             write(iwrite,1002)z0   , z   , zeff  ,
     &                         n1   , l1  , eb1   ,
     &                         n2   , l2  , eb2
             if ( xjp.ge.0.0d0 ) then
                 write(iwrite,1018)isp,lp,xjp
                 if( xjt1.ge.0.0d0 ) then
                     write(iwrite,1024)xj1,xjt1,xj2,xjt2
                 else
                     write(iwrite,1025)xj1,xj2
                 endif
             else
                 write(iwrite,1019)isp,lp
                 if( xjt1.ge.0.0d0 ) then
                     write(iwrite,1022)ist1,lt1,xjt1,ist2,lt2,xjt2
                 else
                     write(iwrite,1023)ist1,lt1,ist2,lt2
                 endif
             endif
             write(iwrite,1020) neqv1 , fpc1  , neqv2 , fpc2  ,
     &                          aval  ,
     &                          xmax  , iextwf, ijucys, isrch
            if(nshell.gt.0) then
                do i=1,nshell
                  write(iwrite,1021)1000*ns(i)+100*ls(i)+iqs(i),
     &                              alfaa(1,i),alfaa(2,i)
                enddo
            endif
            write(iwrite,1026)
            do i=1,nx
              write(iwrite,1027)xa(i),omga(i)
            enddo
            write(iwrite,1028)
            do i=1,nt
              write(iwrite,1027)tea(i),upsa(i)
            enddo
         else
C             write(cstring,1000) title
C             write(iwrite,1001) indx, cstring, date, code
C             write(iwrite,1003) z0, z, zeff
         endif

         if (iasel.eq.3) then
             write(iwrite,1013)
         else
             write(iwrite,1017)
             do 30 i = 1, icnt-2
                write(iwrite,1015) ciarr(i)
 30           continue
         endif
C
         write(iwrite,1014) indx, code, cstring(1:6),
     &                      cstring(7:40), cmethod,
     &                      date
         close( unit=iwrite , status = 'keep')
C
C
C
C-----------------------------------------------------------------------
C
 1000  format(1a40)
 1001  format('I',i3,2x,1a40,10x,1a8,9x,1a1)
 1002        format(' Z0    = ',f5.2,2x,' Z     = ',f5.2,2x,
     &              ' ZEFF  = ',F5.2/
     &              ' N1    = ',i2,5x,' L1    = ',i2,5x,
     &              ' EB1   = ',f10.6/
     &              ' N2    = ',i2,5x,' L2    = ',i2,5x,
     &              ' EB2   = ',f10.6)
 1013  format('-1  '/
     & 'C-----------------------------------------------------------',
     & '--------------------'/
     & 'c index  code  information                                  ',
     & 'date       ')
 1014  format('C ',i3,5x,1a1,3x,1a6,2x,1a34,2x,1a12,2x,1a8/
     & 'C-----------------------------------------------------------',
     & '--------------------')
 1015  format(1a80)
 1016  format(i3)
 1017  format('-1  ')
 1018  format('  '/
     &        ' ISP   = ',i2,5x,' LP    = ',i2,5x,
     &        ' JP    = ',f5.1)
 1019  format('  '/
     &        ' ISP   = ',i2,5x,' LP    = ',i2)
 1020  format(' NEQV1 = ',i2,5x,' FPC1  = ',f5.2, 2x,
     &        ' NEQV2 = ',i2,5x,' FPC2  = ',f5.2, 2x,
     &        ' AVAL  = ',1P,d10.2,/,/,
     &        ' XMAX  = ',0p,f5.1,4x,' IEXTWF = ',i2,2x,
     &        ' IJUCYS = ',i2,2x,'ISRCH = ',i1,/,/,
     &        ' SCREENING SHELLS & INITIAL SCALING',/,
     &        ' NLIQ        ALFA1       ALFA2')
 1021  format(' ',i4,5x,f8.5,4x,f8.5)
 1022  format(' IST1  = ',i2,5x,' LT1   = ',i2,5x,' JT1   = ',f5.1,2x,
     &        ' IST2  = ',i2,5x,' LT2   = ',i2,5x,' JT2   = ',f5.1)
 1023  format(' IST1  = ',i2,5x,' LT1   = ',i2,5x,
     &        ' IST2  = ',i2,5x,' LT2   = ',i2)
 1024  format(' J1    = ',f5.1,2x,' JT1   = ',f5.1,2x,
     &        ' J2    = ',f5.1,2x,' JT2   = ',f5.1)
 1025  format(' J1    = ',f5.1,2x,' J2    = ',f5.1)
 1026  format('  ',/,
     &        '    X                      OMEGA')
 1027  format(1p,d11.4,11x,1p,d11.4)
 1028  format('  ',/,
     &        '   TE(K)                 UPSILON')
C-----------------------------------------------------------------------

       return
       end
