C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4gspc.for,v 1.1 2004/07/06 14:00:25 whitefor Exp $ Date $Date: 2004/07/06 14:00:25 $
C
       subroutine h4gspc( istdim ,
     &                    xa     , n      , 
     &                    c1     , c2     , c3     , c4
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4gspc.for *********************
c
c  purpose: generate precursors of spline coefficients suitable
c           for both forward and backward interpolation
c
c  calling program: various
c
c  
c  input : (i*4)  istdim  = dimensionality for splining arrays
c
c  input : (r*8)  xa(i)   = set of knots
c  input : (i*4)  n       = number of knots  (n.le.istdim)
c  
c  output: (r*8)  c1(i,j) = 1st spline coefficient precursor
c  output: (r*8)  c2(i,j) = 2nd spline coefficient precursor
c  output: (r*8)  c3(i,j) = 3rd spline coefficient precursor
c  output: (r*8)  c4(i,j) = 4th spline coefficient precursor
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          h4spl3     adas      
c          i4unit     adas      fetch unit number for output of messages
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer  nstdim
c-----------------------------------------------------------------------
       parameter ( nstdim = 40)
c-----------------------------------------------------------------------
       integer   istdim , i4unit , n
       integer   i      , j      , j1
c-----------------------------------------------------------------------
       real*8    x
       real*8    h      , h1     , h2     , h3
       real*8    u1     , u2
c-----------------------------------------------------------------------
       real*8    xa(istdim)
       real*8    c1(istdim,istdim-1) , c2(istdim,istdim-1),
     &           c3(istdim,istdim-1) , c4(istdim,istdim-1)
       real*8    ha(nstdim)          , w(nstdim,nstdim)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  check internal dimensions are compatible
c-----------------------------------------------------------------------

       if(nstdim.lt.istdim) then
           write(i4unit(-1),2002)' ','nstdim=',nstdim,
     &                           ' < istdim=',istdim
	   write(i4unit(-1),2003)
	   stop
       endif
       
       do j = 2,n                                                       
         j1=j-1                                                           
         ha(j1)=xa(j)-xa(j1)
       enddo
                                                      
       if(n.lt.2) then                                                     
           c1(1,1)=1.0                                                      
           c2(1,1)=0.0                                                      
           c3(1,1)=0.0                                                      
           c4(1,1)=0.0
	   return                                                      
       else                                                         
           call h4spl3( istdim , n     , ha    ,w    )
           do j = 2,n                                                      
             j1=j-1                                                           
             h=ha(j1)                                                         
             h1=1.0/h                                                         
             h2=h1*h1                                                         
             h3=h1*h2                                                         
             do i = 1,n                                                       
               u1=h*(w(j1,i)+w(j,i))                                            
               u2=h*(w(j1,i)-w(j,i))                                            
               x=0.0                                                            
               if(j1.eq.i)x=1.0                                                 
               if(j.eq.i)x=-1.0                                                 
               c1(i,j1)=0.5*x*x+0.125*u2                                        
               c2(i,j1)=-h1*(1.5*x+0.25*u1)                                     
               c3(i,j1)=-0.5*h2*u2                                              
               c4(i,j1)=h3*(2.0*x+u1)
             enddo                                           
           enddo
	   return                                                         
       endif                                                         
       
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4gspc warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4gspc error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
                                                                 
       end                                                              
