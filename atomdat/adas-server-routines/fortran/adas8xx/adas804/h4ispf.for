C UNIX-IDL - SCCS info: Module @(#)h4ispf.for	1.1 Date 03/18/03
C
       subroutine h4ispf( ixdim  , itdim   , isdim  ,
     &                    dsfull , indxref , title  , cameth , 
     &                    z0     , z       , zeff   , 
     &                    n1     , l1      , eb1    ,
     &                    n2     , l2      , eb2    ,
     &                    isp    , lp      , xjp    ,
     &                    ist1   , lt1     , xj1    , xjt1   , 
     &                    ist2   , lt2     , xj2    , xjt2   ,
     &                    neqv1  , fpc1    , neqv2  , fpc2   ,
     &                    aval   ,
     &                    xmax   , iextwf  , ijucys , isrch  ,
     &                    nshell , ns      , ls     , iqs    , alfaa  ,
     &                    nx     , xa      , 
     &                    nt     , tea     ,
     &                    iasel  , ntabcol , lpend  
     &                )
       implicit none
C-----------------------------------------------------------------------
C
C  ********************* fortran77 subroutine h4ispf *******************
C
C  Purpose: To read user alterations/specifications from idl and to
C           communicate old archive data to idl if requested.
C
C  Calling program:
C            adas804.for
C
C  Subroutine:
C  
C  input : (i*4)  ixdim    = maximum dimension for X  array
C  input : (i*4)  itdim    = maximum dimension for Te array
C  input : (i*4)  isdim    = maximum dimension for shell vectors
C
C  input : (c*80) dsfull   = the users' chosen archive file name.
C  input : (i*4)  indxref  = the index number to refresh from.
C
C  output: (c*40) title    = transition title in the archive file.
C  output: (c*1)  cameth   = the tag distinguishing the type of 
C                            analysis: a - Born, b- IP
C  output: (r*8)  z0       = nuclear charge
C  output: (r*8)  z        = ion charge
C  output: (r*8)  zeff     = ion charge +1
C  output: (i*4)  n1       = lower n-shell of transition
C  output: (i*4)  l1       = lower l-shell of transition
C  output: (r*8)  eb1      = binding energy (Ryd) in lower level
C  output: (i*4)  n2       = upper n-shell of transition
C  output: (i*4)  l2       = upper l-shell of transition
C  output: (r*8)  eb2      = binding energy (Ryd) in upper level
C  output: (i*4)  isp      = 2*Sp+1 for parent
C  output: (i*4)  lp       = Lp for parent
C  output: (r*8)  xjp      = Jp for parent (if 'ic' coupling)
C  output: (i*4)  ist1     = 2*S+1 for lower state
C  output: (i*4)  lt1      = L for lower state
C  output: (r*8)  xj1      = j for lower state
C  output: (r*8)  xjt1     = J for lower state
C  output: (i*4)  ist2     = 2*S'+1 for upper state
C  output: (i*4)  lt2      = L' for upper state
C  output: (r*8)  xj2      = j' for upper state
C  output: (r*8)  xjt2     = J' for upper state
C  output: (i*4)  neqv1    = no. of equiv. electrons for lower shell. 
C  output: (r*8)  fpc1     = fract. parentage for lower state
C  output: (i*4)  neqv2    = no. of equiv. electrons for upper shell. 
C  output: (r*8)  fpc2     = fract. parentage for upper state
C  output: (i*4)  aval     = A-value (sec-1) if dipole; else -ve
C  output: (i*4)  xmax     = range of numerical wave functions
C  output: (i*4)  iextwf   = 0 => calculate radial wave functions 
C                          = 1 => read in radial wave functions
C  output: (i*4)  ijucys   = -1 => Jucys potential form adopted
C                          = 0  => Slater potential form adopted
C  output: (i*4)  ijucys   = -1 => Jucys potential form adopted
C                          = 0  => Slater potential form adopted
C  output: (i*4)  isrch    = 0 => fcf6 search for energy eigenvalue 
C                          = 1 => fcf6 search for scaling parameters
C  output: (i*4)  nshell   = number of screening shells
C  output: (i*4)  n        =  n for each screening shell
C                              1st dim: screening shell index
C  output: (i*4)  ls       = l for each screening shell
C                              1st dim: screening shell index
C  output: (i*4)  iqs      = iq for each screening shell
C                              1st dim: screening shell index
C  output: (i*4)  alfaa    = scaling factor for each screening shell
C                              1st dim: index for lower & upper state                            
C                              2nd dim: index over screening shells
C  output: (i*4)  nx       = number of incident electron energies
C  output: (i*4)  xa()     = threshold parameter values 
C  output: (i*4)  nt       = number of electron temperatures
C  output: (i*4)  tea()    = electron temperatures (K)
C  output: (i*4)  iasel    = the archiving choice.
C                             0 - no archive
C                             1 - examine old archive
C                             2 - refresh from archive
C                             3 - new archive
C  output: (l)    lpend    = 'cancel' or 'done' decision.
C  output: (i*4)  ntabcol  = number of columns in the editable idl table
C
C  Routines:
C          routine    source    brief description
C          -------------------------------------------------------------
C          xxflsh     adas      clears pipe buffer
C          xxslen     adas      removes spaces from a string
C
C  Author:  Hugh P. Summers, University of Strathclyde
C           JA7.08
C           Tel.: +44 (0)141-548-4196
C
C  Date:    26 June 2002
C
C
C  Version: 1.1   Hugh Summers  21/02/03
C  Modified:      first release
C
C  Version: 1.2   Hugh Summers  25/05/2004
C  Modified:      send/get title to IDL
C
C-----------------------------------------------------------------------
       integer   ixdim     , itdim     , isdim  
C-----------------------------------------------------------------------
       integer   i4unit
       integer   indxref  , 
     &           n1       , l1        , n2        , l2       ,           
     &           isp      , lp        , 
     &           ist1     , lt1       , ist2      , lt2      , 
     &           neqv1    , neqv2     , 
     &           iextwf   , ijucys    , isrch     ,
     &           nshell   , nx        , nt
       integer   ntabcol  , iatyp     , iwhc
       integer   ilogic   , pipein    , pipeou    , iasel
       integer   i        , j         , ifirst    , ilast   
C-----------------------------------------------------------------------
       real*8    z0       , z         , zeff      , 
     &           eb1      , eb2       , 
     &           xjp      , xj1       , xjt1      , xj2      , xjt2    ,
     &           fpc1     , fpc2      ,    
     &           aval     , xmax    
C-----------------------------------------------------------------------
       character dsfull*80      , title*40        , c40*40   ,
     &           cameth*1       
C-----------------------------------------------------------------------
       integer   ns(isdim)       , ls(isdim)      , iqs(isdim)       
C-----------------------------------------------------------------------
       real*8    alfaa(2,isdim) , 
     &           xa(ixdim)      , tea(itdim) 
C-----------------------------------------------------------------------
       logical lpend
C-----------------------------------------------------------------------
       parameter( pipein = 5 , pipeou = 6 )
C-----------------------------------------------------------------------
       integer ifout(ntabcol)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

       write(pipeou,*)ixdim
       call xxflsh(pipeou)
       write(pipeou,*)itdim
       call xxflsh(pipeou)
       write(pipeou,*)isdim
       call xxflsh(pipeou)
       
       if(cameth.eq.'a')then
           iwhc = 1
       else
           iwhc = 2
       endif
       
       write(pipeou,*)iwhc
       call xxflsh(pipeou)
       
       if(iasel.eq.2)then
           if(cameth.eq.'a')then
               write(pipeou,'(1x,1a40)')title
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')z
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')z0
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')zeff
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')n1
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')l1
               call xxflsh(pipeou)
               write(pipeou,'(1x,1f15.8)')eb1
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')n2
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')l2
               call xxflsh(pipeou)
               write(pipeou,'(1x,1f15.8)')eb2
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')isp
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')lp
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')xjp
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')ist1
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')lt1
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')xj1
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')xjt1
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')ist2
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')lt2
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')xj2
               call xxflsh(pipeou)
               write(pipeou,'(1x,f6.2)')xjt2
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')neqv1
               call xxflsh(pipeou)
               write(pipeou,'(1x,f8.4)')fpc1
               call xxflsh(pipeou)
               write(pipeou,'(1x,1i3)')neqv2
               call xxflsh(pipeou)
               write(pipeou,'(1x,f8.4)')fpc2
               call xxflsh(pipeou)
               write(pipeou,'(1x,1pe12.4)')aval
               call xxflsh(pipeou)
               write(pipeou,'(1x,f10.5)')xmax
               call xxflsh(pipeou)
               write(pipeou,'(1x,i1)')iextwf
               call xxflsh(pipeou)
               write(pipeou,'(1x,i1)')ijucys
               call xxflsh(pipeou)
               write(pipeou,'(1x,i1)')isrch
               call xxflsh(pipeou)
               
               write(pipeou,'(1x,i3)')nshell
               call xxflsh(pipeou)
               do i = 1,nshell
                   write(pipeou,'(1x,i4,1x,i4,1x,i4,1x,f8.4,1x,f8.4)')
     &                        ns(i),ls(i),iqs(i),alfaa(1,i),alfaa(2,i)
                   call xxflsh(pipeou)
               enddo
               write(pipeou,'(1x,i3)')nx
               call xxflsh(pipeou)
               do i = 1,nx
                   write(pipeou,'(1x,1pe12.4)')xa(i)
                   call xxflsh(pipeou)
               enddo
               write(pipeou,'(1x,i3)')nt
               do j = 1,nt
                   write(pipeou,'(1x,1pe12.4)')tea(j)
                   call xxflsh(pipeou)
               enddo
               
           else if(cameth.eq.'b')then
           
               continue
               
           endif
               
       endif
C
C now read alterations from idl pipe for all possible selections
C
       read(pipein,*)ilogic
       if(ilogic.eq.1)then
           lpend = .true.
       else
           lpend = .false.
       endif
       
       read(pipein,'(1x,1a40)') c40
       call xxslen(c40,ifirst,ilast)
       title=' '
       title(1:ilast-ifirst+1)=c40(ifirst:ilast)
       
       read(pipein,*) z
       read(pipein,*) z0
       read(pipein,*) zeff
       read(pipein,*) n1
       read(pipein,*) l1
       read(pipein,*) eb1
       read(pipein,*) n2
       read(pipein,*) l2
       read(pipein,*) eb2
       read(pipein,*) isp
       read(pipein,*) lp
       read(pipein,*) xjp
       read(pipein,*) ist1
       read(pipein,*) lt1
       read(pipein,*) xj1
       read(pipein,*) xjt1
       read(pipein,*) ist2
       read(pipein,*) lt2
       read(pipein,*) xj2
       read(pipein,*) xjt2
       read(pipein,*) neqv1
       read(pipein,*) fpc1
       read(pipein,*) neqv2
       read(pipein,*) fpc2
       read(pipein,*) aval
       read(pipein,*) xmax
       read(pipein,*) iextwf
       read(pipein,*) ijucys
       read(pipein,*) isrch
              
       read(pipein,*) nshell

       do i = 1, nshell
          read(pipein,'(1x,i4,1x,i4,1x,i4,1x,f8.4,1x,f8.4)') 
     &              ns(i),ls(i),iqs(i),alfaa(1,i),alfaa(2,i)
       enddo
       
       read(pipein,*) nx
       
       do i = 1, nx
          read(pipein,'(1x,1e12.4)') xa(i)
       enddo
       
       read(pipein,*) nt

       do i = 1, nt
          read(pipein,'(1x,1e12.4)') tea(i)
       enddo
       
       do i = 1, ntabcol
           read(pipein,'(1x,i3)')ifout(i)
           ifout(i) = ifout(i)+1
       enddo
         
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       return
       end
