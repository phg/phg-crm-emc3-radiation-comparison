C UNIX-IDL - SCCS info: Module @(#)fcf4.for	1.1 Date 03/18/03
C
       subroutine fcf4(f,c,x0,e,z,el,x1,h)
       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 program: fcf4.for **********************
C
C  Purpose:  Evaluates free regular Coulomb real function
C
C            Puts result in
C            f(j),j=1,2,...,x1/h.
C            f satisfies ((d/dx)(d/dx)-el(el+1)-2z/x+e)f=0
C            f=c*x**(el+1.0)*(1.0+...)   for small x
C            f=k**(-0.5)*dsin(kx-0.5*el*pi-(z/k)log(2kx)+
C            arggamma(el+1+i*z/k))  for large x
C            where k=dsqrt(e)
C            n.b. z is positive for repulsive field
C
C  Subroutine:
C
C  input : (r*8)  e        = energy (Ryd)
C  input : (r*8)  z        = effective charge seen by electron
C  input : (r*8)  el       = orbital angular momentum
C  input : (r*8)  x1       = outer limit for tabulation
C  input : (r*8)  h        = tabulation step length
C
C  output: (r*8)  f()      = resulting Coulomb function
C  output: (r*8)  c        = normalisation constant
C  output: (r*8)  x0       = the (approx) first point of inflexion in f
C
C          (r*8) wilf      = fortran function
C
C
C  Routines:
C          none
C
C  Author:  H. P. Summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   24/02/03
C
C  Update: HP Summers   24/05/04 Restructure and addded standard warning
C
C  Update: AD Whiteford 20/07/07 Modified comments slightly to allow
C                                for automatic generation of
C                                documentation.
C
C-----------------------------------------------------------------------
       integer  i4unit
       integer  i1    , l     , i     , i0    , j     , j0    , l1
C-----------------------------------------------------------------------
       real*8   c     , x0    , e     , z     , el    , x1    , h
       real*8   wilf  , w1    , w2    , w3    , zz    , ek    ,
     &          t1    , t2    , c2    , c3    , cj    , cj2   , x     ,
     &          z2    , h0    , h1    , c0    , c1
C-----------------------------------------------------------------------
       real*8   f(1000),a(100)
C-----------------------------------------------------------------------
       wilf(x)=e+w1/x+w2/(x*x)

       w1=-2.0d0*z
       w2=-el*(el+1.0d0)
       i1=0.5d0+x1/h
       l=el+0.1d0
       w3=(el+1.0d0)*(el+2.0d0)
       zz=z*z
       if(e-1.0d-40)1,1,6
    1  if(dabs(z)-1.0d-15)2,2,3
    2  write(i4unit(-1),2000)
     &       'Failed in fcf4 because e and z are both zero',
     &       'output set to zero'
       write(i4unit(-1),2001)
   38  do 39 i=1,i1
   39  f(i)=0.0d0
       return
    3  if(z)5,4,4
    4  write(i4unit(-1),2000)'fcf4 output set to zero because',
     &         'e is less than 1.0d-40 and z exceeds 1.0d-15'
       write(i4unit(-1),2001)
       go to 38
    5  c=-6.2831853d0*z
       go to 11
    6  ek=dsqrt(e)
       t1=3.14159265d0*z/ek
       t2=dabs(t1)
       if(t2-0.01d0)7,7,8
    7  c=3.0d0*ek/(3.0d0+t1*(3.0d0+t1*(2.0d0+t1)))
       go to 11
    8  if(t2-80.0d0)10,9,9
    9  if(z)5,4,4
   10  c=1.0d0-dexp(t1+t1)
       c=-6.2831853d0*z/c
   11  c2=1.0d0
       if(l)14,14,12
   12  continue
       do 13 j=1,l
       cj=j
       cj2=j+j
       c2=c2*cj*(cj2+1.0d0)
       c=c*(zz+e*cj*cj)
   30  if(c+c2-1.0d70)13,31,31
   31  c2=1.0d-5*c2
       c=1.0d-10*c
       go to 30
   13  continue
   14  c=dsqrt(c)/c2
       x0=w3/(dsqrt(zz+w3*e)-z)+h+h
       if(x1-x0)15,16,16
   15  x0=x1
   16  i0=0.5d0+x0/h
       x0=i0
       x0=x0*h
       l1=l+1
       a(1)=1.0d0
       t1=l1
       a(2)=x0*z/t1
       j=2
       z2=z+z
   17  j=j+1
       c2=j-1
       c3=j+l+l
       a(j)=x0*(z2*a(j-1)-x0*e*a(j-2))/(c2*c3)
       if(j-6)17,17,18
   18  c2=dabs(a(j))+dabs(a(j-1))
       if(c2-1.0d-10)19,19,17
   19  if(i0-i1)21,21,20
   20  i0=i1
   21  j0=j
       do 23 i=1,i0
       x=i
       x=x*h/x0
       j=j0
       t1=a(j)
   22  j=j-1
       t1=a(j)+x*t1
       if(j-1)23,23,22
   23  f(i)=c*t1*(x*x0)**l1
       if(i0-i1)24,26,26
   24  h0=h*h
       h1=0.0833333333d0*h0
       c0=f(i0)*(1.0d0+(h1-h0)*wilf(x0))-f(i0-1)*(1.0d0+h1*wilf(x0-h))
       c1=f(i0)*(1.0d0+h1*wilf(x0))
       i=i0
   25  i=i+1
       x=i
       x=x*h
       c1=c1+c0
       c2=wilf(x)
       f(i)=c1/(1.0d0+h1*c2)
       c0=c0-h0*c2*f(i)
       if(i-i1)25,26,26
   26  return
C-----------------------------------------------------------------------
 2000 format(1x,32('*'),'  fcf4 warning  ',32('*')//
     &       2x,a,2x,a )
 2001 format(/1x,29('*'),' program continues ',29('*'))
C-----------------------------------------------------------------------
      end
