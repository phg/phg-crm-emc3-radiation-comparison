C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4sort.for,v 1.1 2004/07/06 14:00:53 whitefor Exp $ Date $Date: 2004/07/06 14:00:53 $
C
       subroutine h4sort(xa,ya,n)
       implicit none
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4sort.for *********************
c
c  purpose: to sort paired vectors xa and ya so that xa is in increasing 
c           order
c
c  calling program: various
c
c  
c  i/o   : (i*4)  xa() = x-values
c  i/o   : (i*4)  ya() = y-values
c  input : (i*4)  n     = number of values
c
c  routines:
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer   n       , n1       , i      , i1    , j
c-----------------------------------------------------------------------
       real*8    swap
c-----------------------------------------------------------------------
       real*8    xa(*)   , ya(*)
c-----------------------------------------------------------------------
       n1=n-1                                                           
       do 10 i=1,n1                                                     
         i1=i+1                                                           
         do 5 j=i1,n                                                      
           if(xa(i).le.xa(j))go to 5                                        
           swap=xa(i)                                                       
           xa(i)=xa(j)                                                      
           xa(j)=swap                                                       
           swap=ya(i)                                                       
           ya(i)=ya(j)                                                      
           ya(j)=swap                                                       
    5    continue                                                         
   10  continue                                                         
       return                                                           
      end                                                               
