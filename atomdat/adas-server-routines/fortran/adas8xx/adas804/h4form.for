C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4form.for,v 1.1 2004/07/06 14:00:15 whitefor Exp $ Date $Date: 2004/07/06 14:00:15 $
C
       function h4form(i,x)                                              
       implicit none                                         
c-----------------------------------------------------------------------
c
c  **************** fortran77 function: h4form.for *******************
c
c  purpose: specifies an independent variable transforma for splining
c
c  calling program: various
c
c  input : (i*4)  i       = selection parameter for transform
c  input : (r*8)  x       = independent variable
c
c  output: (r*8)  h4form  = transformed independent variable

c
c  routines:
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer  i
c-----------------------------------------------------------------------
       real*8   x     , h4form
c-----------------------------------------------------------------------
       go to (1,2,1,2,1,2,1,2,1,2,1,2),i                                
    1  h4form = 1.0d0                                                      
       go to 3                                                          
    2  h4form = x                                                          
    3  return                                                           
      end                                                               
