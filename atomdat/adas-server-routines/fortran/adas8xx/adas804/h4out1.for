       subroutine h4out1( ixdim   , itdim   , isdim   , ndinfo ,
     &                    dsfull  , indxref , title   , cameth ,
     &                    z0      , z       , zeff    ,
     &                    n1      , l1      , eb1     ,
     &                    n2      , l2      , eb2     ,
     &                    isp     , lp      , xjp     ,
     &                    ist1    , lt1     , xj1     , xjt1   ,
     &                    ist2    , lt2     , xj2     , xjt2   ,
     &                    neqv1   , fpc1    , neqv2   , fpc2   ,
     &                    aval    ,
     &                    xmax    , iextwf  , ijucys  , isrch  ,
     &                    nshell  , ns      , ls      , iqs    , alfaa ,
     &                    nx      , xa      , omga    ,
     &                    nt      , tea     , upsa    ,
     &                    date    , ianopt  , iasel   ,
     &                    cheader , texdsn  , itexout , larch  ,
     &                    ninfo   , cinfoa
     &                )
       implicit none
C-----------------------------------------------------------------------
C
C  ********************* fortran77 subroutine h4out1*******************
C
C  purpose:
C        To print results to the standard output file.
C
C  calling program:
C        adas804.for
C
C  data:
C        ciarr(500)*80- 500 is the current limit on indexes
C
C  input:
C            (i*4)  ixdim    = maximum dimension for X  array
C            (i*4)  itdim    = maximum dimension for Te array
C            (i*4)  isdim    = maximum dimension for shell vectors
C            (i*4)  ndinfo   = maximum number of information strings
C
C            (c*80) dsfull   - the users' chosen archive file name.
C            (i*4)  indxref  - the index number to refresh from.
C            (c*40) title    - the information line in the archive
C                              file.
C            (c*1)  cameth   = the tag distinguishing the type of
C                              analysis: a - Born, b- IP
C            (r*8)  z0       = nuclear charge
C            (r*8)  z        = ion charge
C            (r*8)  zeff     = ion charge +1
C            (i*4)  n1       = lower n-shell of transition
C            (i*4)  l1       = lower l-shell of transition
C            (r*8)  eb1      = binding energy (Ryd) in lower level
C            (i*4)  n2       = upper n-shell of transition
C            (i*4)  l2       = upper l-shell of transition
C            (r*8)  eb2      = binding energy (Ryd) in upper level
C            (i*4)  isp      = 2*Sp+1 for parent
C            (i*4)  lp       = Lp for parent
C            (r*8)  xjp      = Jp for parent (if 'ic' coupling)
C            (i*4)  ist1     = 2*S+1 for lower state
C            (i*4)  lt1      = L for lower state
C            (r*8)  xj1      = j for lower state
C            (r*8)  xjt1     = J for lower state
C            (i*4)  ist2     = 2*S'+1 for upper state
C            (i*4)  lt2      = L' for upper state
C            (r*8)  xj2      = j' for upper state
C            (r*8)  xjt2     = J' for upper state
C            (i*4)  neqv1    = no. of equiv. electrons for lower shell.
C            (r*8)  fpc1     = fract. parentage for lower state
C            (i*4)  neqv2    = no. of equiv. electrons for upper shell.
C            (r*8)  fpc2     = fract. parentage for upper state
C            (i*4)  aval     = A-value (sec-1) if dipole; else -ve
C            (i*4)  xmax     = range of numerical wave functions
C            (i*4)  iextwf   = 0 => calculate radial wave functions
C                            = 1 => read in radial wave functions
C            (i*4)  ijucys   = -1 => Jucys potential form adopted
C                            = 0  => Slater potential form adopted
C            (i*4)  isrch    = 0 => fcf6 search for energy eigenvalue
C                            = 1 => fcf6 search for scaling parameters
C            (i*4)  nshell   = number of screening shells
C            (i*4)  n        =  n for each screening shell
C                               1st dim: screening shell index
C            (i*4)  ls       = l for each screening shell
C                               1st dim: screening shell index
C            (i*4)  iqs      = iq for each screening shell
C                               1st dim: screening shell index
C            (i*4)  alfaa    = scaling factor for each screening shell
C                               1st dim: index for lower & upper state
C                               2nd dim: index over screening shells
C            (i*4)  nx       = number of incident electron energies
C            (r*8)  xa()     = threshold parameter values
C            (r*8)  omga()   = omegas at input values of x-parameter
C            (i*4)  nt       = number of electron temperatures
C            (r*8)  tea()    = electron temperatures (K)
C            (r*8)  upsa()   = upsilon at onput values of Te
C            (c*8)  date   = the current data
C            (i*4)  ianopt = analysis option
C                            1 - adas option
C                            2 - burgess option
C            (i*4)  iasel  = the archiving choice (see h4spf0.f)
C            (c*80) cheader= the adas header for the paper.txt file
C            (c*80) texdsn = the default hard copy file name
C            (i*4)  itexout= the hard copy of file output flag
C                            0 - no, 1 - yes
C            (l)    larch  = archiving selection option
C            (i*4)  ninfo    = number of information strings
C            (c*90) cinfoa() = information strings
C                            1st dim: index number of strings
C
C
C  Routines:
C        none
C
C
C  Author:  H. P. Summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   21/05/04
C
C  Update:
C
C
C
C-----------------------------------------------------------------------
       integer ixdim   , itdim  , isdim , ndinfo
       integer nx      , nt
       integer indxref , ichk1  , indl  , indu   , ict   , itout
       integer indx    , ianopt , i     , icnt
       integer iasel   , itexout, ihflag
       integer n1      , l1     , n2    , l2     ,
     &         isp     , lp     ,
     &         ist1    , lt1    , ist2  , lt2    ,
     &         neqv1   , neqv2  ,
     &         iextwf  , ijucys , isrch ,
     &         nshell
       integer ninfo
C-----------------------------------------------------------------------
       integer   ns(isdim)       , ls(isdim)      , iqs(isdim)
C-----------------------------------------------------------------------
       character dsfull*80 , cstring*40 , cstrng*80
       character title*40  , cameth*1   , date*8       , code*1
       character dcode(2)*1, cindx*3    , ciarr(500)*80
       character cheader*80, texdsn*80  , cmethod*12
C-----------------------------------------------------------------------
       integer iwrite      , pipein     , pipeou
C-----------------------------------------------------------------------
       parameter( iwrite = 9 , pipein = 5 , pipeou = 6 )
C-----------------------------------------------------------------------
       integer i4unit , iaselcopy
C-----------------------------------------------------------------------
       logical larch
C-----------------------------------------------------------------------
       real*8 z    , z0   , zeff ,
     &        eb1  , eb2  ,
     &        xjp  , xj1  , xjt1      , xj2     , xjt2    ,
     &        fpc1 , fpc2 ,
     &        aval , xmax
C-----------------------------------------------------------------------
       real*8    alfaa(2,isdim)
       real*8    xa(ixdim)    , omga(ixdim)
       real*8    tea(itdim)   , upsa(itdim)
C-----------------------------------------------------------------------
       character cinfoa(ndinfo)*90
C-----------------------------------------------------------------------
       data dcode/'A','B'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  write out data
C-----------------------------------------------------------------------
C
         if ((cameth.eq.'a').or.(cameth.eq.'A')) then
             code = dcode(1)
             cmethod = 'Effpot-Born:'
         else
             code = dcode(2)
             cmethod = 'Impact-Parm:'
         endif

         open(unit = iwrite,file = texdsn, status = 'unknown')


         if ((cameth.eq.'a').or.(cameth.eq.'A')) then
             write(iwrite,*) cheader
             write(iwrite,'(/)')
             cstring = ' '
             write(cstring,1000) title
             write(iwrite,1001) cmethod,cstring, date, code

             write(iwrite,*)
             write(iwrite,*)'Wave function generation information:'
             write(iwrite,1029) xmax  , iextwf, ijucys, isrch
             if(nshell.gt.0) then
                 do i=1,nshell
                   write(iwrite,1021)1000*ns(i)+100*ls(i)+iqs(i),
     &                               alfaa(1,i),alfaa(2,i)
                enddo
             endif
             do i=1,ninfo
               write(iwrite,'(1a90)') cinfoa(i)
             enddo

             write(iwrite,*)
             write(iwrite,*)
             write(iwrite,*)'Transition information:'

             write(iwrite,1002)z0   , z   , zeff  ,
     &                         n1   , l1  , eb1   ,
     &                         n2   , l2  , eb2
             if ( xjp.ge.0.0d0 ) then
                 write(iwrite,1018)isp,lp,xjp
                 if( xjt1.ge.0.0d0 ) then
                     write(iwrite,1024)xj1,xjt1,xj2,xjt2
                 else
                     write(iwrite,1025)xj1,xj2
                 endif
             else
                 write(iwrite,1019)isp,lp
                 if( xjt1.ge.0.0d0 ) then
                     write(iwrite,1022)ist1,lt1,xjt1,ist2,lt2,xjt2
                 else
                     write(iwrite,1023)ist1,lt1,ist2,lt2
                 endif
             endif
             write(iwrite,1020) neqv1 , fpc1  , neqv2 , fpc2  ,
     &                          aval
            write(iwrite,1026)
            do i=1,nx
              write(iwrite,1027)xa(i),omga(i)
            enddo
            write(iwrite,1028)
            do i=1,nt
              write(iwrite,1027)tea(i),upsa(i)
            enddo

         endif

         close( unit=iwrite , status = 'keep')
C
C-----------------------------------------------------------------------
C
 1000  format(1a40)
 1001  format(1x,1a12,2x,1a40,10x,1a8,9x,1a1)
 1002        format(' z0    = ',f5.2,2x,' z     = ',f5.2,2x,
     &              ' zeff  = ',f5.2/
     &              ' n1    = ',i2,5x,' l1    = ',i2,5x,
     &              ' eb1   = ',f10.6/
     &              ' n2    = ',i2,5x,' l2    = ',i2,5x,
     &              ' eb2   = ',f10.6)
 1003  format(' nuclear charge =',f6.2,'   ion charge =',f6.2,
     &        '   effective ion charge =',f6.2)
 1004  format(' lower  index = ',i5,19x,' upper  index = ',i5)
 1005  format(' lower energy = ',f12.6,12x,' upper energy = ',f12.6)
 1006  format(' lower weight = ',f5.1,19x,' upper weight = ',f5.1)
 1007  format(' acoeff = ',1pe12.4,'     s = ',1pe11.4,
     &        '      fabs = ',1pe12.4,'     energy difference =',
     &        1a13)
 1008  format(' xsect type=',i3,'   xsect opt. factors, fxc2 = ',
     &        1pe11.4,'   fxc3 = ',1pe11.4,'   ixops =',i2,
     &        '   ibpts =',i2,'   ifpts = ',i2,'   idiff =',i2)
 1009  format('    x',20x,'  omega',10x,'approx. omega ',10x,
     &        ' difference ')
 1010  format(1pe11.4,10x,1pe12.4,10x,1pe12.4,10x,1pe12.4)
 1011  format(' te(k)',20x,' gamma ',9x,' approx. gam',
     &        12x,' ex. rate ',11x,' dexc. rate',13x,' gbarf ')
 1012  format(1pe11.4,10x,1pe12.4,10x,1pe12.4,10x,1pe12.4,10x,
     &        1pe12.4,10x,1pe12.4)
 1013  format('-1  '/
     & 'C-----------------------------------------------------------',
     & '--------------------'/
     & 'c index  code  information                                  ',
     & 'date       ')
 1014  format('C ',i3,5x,1a1,3x,1a6,2x,1a34,2x,1a12,2x,1a8/
     & 'C-----------------------------------------------------------',
     & '--------------------')
 1015  format(1a80)
 1016  format(i3)
 1017  format('-1  ')
 1018  format('  '/
     &        ' ISp   = ',i2,5x,' Lp    = ',i2,5x,
     &        ' Jp    = ',f5.1)
 1019  format('  '/
     &        ' ISp   = ',i2,5x,' Lp    = ',i2)
 1020  format(' neqv1 = ',i2,5x,' fpc1  = ',f5.2, 2x,
     &        ' neqv2 = ',i2,5x,' fpc2  = ',f5.2, 2x,
     &        ' aval  = ',1p,d10.2,/)
 1021  format(' ',i4,5x,f8.5,4x,f8.5)
 1022  format(' ISt1  = ',i2,5x,' Lt1   = ',i2,5x,' Jt1   = ',f5.1,2x,
     &        ' ISt2  = ',i2,5x,' Lt2   = ',i2,5x,' Jt2   = ',f5.1)
 1023  format(' ISt1  = ',i2,5x,' Lt1   = ',i2,5x,
     &        ' ISt2  = ',i2,5x,' Lt2   = ',i2)
 1024  format(' j1    = ',f5.1,2x,' Jt1   = ',f5.1,2x,
     &        ' j2    = ',f5.1,2x,' Jt2   = ',f5.1)
 1025  format(' j1    = ',f5.1,2x,' j2    = ',f5.1)
 1026  format('  ',/,
     &        '    X                      Omega')
 1027  format(1p,d11.4,11x,1p,d11.4)
 1028  format('  ',/,
     &        '   Te(K)                 Upsilon')
 1029  format(' xmax  = ',0p,f5.1,4x,' iextwf = ',i2,2x,
     &        ' ijucys = ',i2,2x,'isrch = ',i1,/,/,
     &        ' screening shells & initial scaling',/,
     &        ' nliq        alfa1       alfa2')
C-----------------------------------------------------------------------

       return
       end
