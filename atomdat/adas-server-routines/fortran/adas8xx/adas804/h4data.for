C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4data.for,v 1.1 2004/07/06 14:00:09 whitefor Exp $ Date $Date: 2004/07/06 14:00:09 $
C
       subroutine h4data( iunit  , ixdim  , itdim   , isdim  ,
     &                    dsfull , indxref , title  , cameth , 
     &                    z0     , z       , zeff   , 
     &                    n1     , l1      , eb1    ,
     &                    n2     , l2      , eb2    ,
     &                    isp    , lp      , xjp    ,
     &                    ist1   , lt1     , xj1    , xjt1   , 
     &                    ist2   , lt2     , xj2    , xjt2   ,
     &                    neqv1  , fpc1    , neqv2  , fpc2   ,
     &                    aval   ,
     &                    xmax   , iextwf  , ijucys , isrch  ,
     &                    nshell , nlqs    , alfaa  ,
     &                    nx     , xa      , 
     &                    nt     , tea     ,
     &                    cresol  
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran77 subroutine h4data *******************
c
c  purpose: to refresh a data index from an adas804 archive. reads
c           in the index code a-effective potential Born, b-impact 
c           parameter and the the rest of the data as appropriate.
c
c  calling program: adas804
c
c  subroutine:
c
c  input : (i*4)  iunit    = unit to be used for reading file
c  input : (i*4)  ixdim    = maximum dimension for X  array
c  input : (i*4)  itdim    = maximum dimension for Te array
c  input : (i*4)  isdim    = maximum dimension for shell vectors
c
c  input : (c*80) dsfull   = the users' chosen archive file name.
c  input : (i*4)  indxref  = the index number to refresh from.
c
c  output: (c*40) title    = transition title in the archive file.
c  output: (c*1)  cameth   = the tag distinguishing the type of 
c                            analysis: a - Born, b- IP
c  output: (r*8)  z0       = nuclear charge
c  output: (r*8)  z        = ion charge
c  output: (r*8)  zeff     = ion charge +1
c  output: (i*4)  n1       = lower n-shell of transition
c  output: (i*4)  l1       = lower l-shell of transition
c  output: (r*8)  eb1      = binding energy (Ryd) in lower level
c  output: (i*4)  n2       = upper n-shell of transition
c  output: (i*4)  l2       = upper l-shell of transition
c  output: (r*8)  eb2      = binding energy (Ryd) in upper level
c  output: (i*4)  isp      = 2*Sp+1 for parent
c  output: (i*4)  lp       = Lp for parent
c  output: (r*8)  xjp      = Jp for parent (if 'ic' coupling)
c  output: (i*4)  ist1     = 2*S+1 for lower state
c  output: (i*4)  lt1      = L for lower state
c  output: (r*8)  xj1      = j for lower state
c  output: (r*8)  xjt1     = J for lower state
c  output: (i*4)  ist2     = 2*S'+1 for upper state
c  output: (i*4)  lt2      = L' for upper state
c  output: (r*8)  xj2      = j' for upper state
c  output: (r*8)  xjt2     = J' for upper state
c  output: (i*4)  neqv1    = no. of equiv. electrons for lower shell. 
c  output: (r*8)  fpc1     = fract. parentage for lower state
c  output: (i*4)  neqv2    = no. of equiv. electrons for upper shell. 
c  output: (r*8)  fpc2     = fract. parentage for upper state
c  output: (i*4)  aval     = A-value (sec-1) if dipole; else -ve
c  output: (i*4)  xmax     = range of numerical wave functions
c  output: (i*4)  iextwf   = 0 => calculate radial wave functions 
c                          = 1 => read in radial wave functions
c  output: (i*4)  ijucys   = -1 => Jucys potential form adopted
c                          = 0  => Slater potential form adopted
c  output: (i*4)  isrch    = 0 => fcf6 search for energy eigenvalue 
c                          = 1 => fcf6 search for scaling parameters
c  output: (i*4)  nshell   = number of screening shells
c  output: (i*4)  nlqs     = 1000*n+100*l+iq for each screening shell
c                              1st dim: screening shell index
c  output: (i*4)  alfaa    = scaling factor for each screening shell
c                              1st dim: index for lower & upper state                            
c                              2nd dim: index over screening shells
c  output: (i*4)  nx       = number of incident electron energies
c  output: (i*4)  xa()     = threshold parameter values 
c  output: (i*4)  nt       = number of electron temperatures
c  output: (i*4)  tea()    = electron temperatures (K)
c  output: (i*4)  cresol   = 'ic' => transition between J-levels
c                          = 'ls' => transition between terms
c 
c
c  routines: 
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          xxword     adas      extract position of number in buffer
c          xxcase     adas      change string to all upper or lower case
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 February 2003
c
c
c  version: 1.1   Hugh Summers  24/02/03
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer   iunit    , ixdim     , itdim     , isdim 
       integer   i4unit   
       integer   indxref  , 
     &           n1       , l1        , n2        , l2       ,            
     &           isp      , lp        , 
     &           ist1     , lt1       , ist2      , lt2      , 
     &           neqv1    , neqv2     , 
     &           iextwf   , ijucys    , isrch     ,
     &           nshell   , nx        , nt
       integer   ichk1    ,
     &           nfirst   , iword     , nwords       
c-----------------------------------------------------------------------
       real*8    z0       , z         , zeff      , 
     &           eb1      , eb2       , 
     &           xjp      , xj1       , xjt1      , xj2      , xjt2    ,
     &           fpc1     , fpc2      ,    
     &           aval     , xmax    
c-----------------------------------------------------------------------
       character dsfull*80      , title*40        ,
     &           cameth*1       , cresol*2
       character cstring*120    , cstrgin*120 
c-----------------------------------------------------------------------
       integer   nlqs(isdim)
       integer   ifirsta(18)    , ilasta(18)
c-----------------------------------------------------------------------
       real*8    alfaa(2,isdim) , 
     &           xa(ixdim)      , tea(itdim) 
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  open archive and search for index number
c-----------------------------------------------------------------------

       open(unit=iunit,file=dsfull)
       
       cstring=' '
       read(iunit,'(a)')cstring
 1     if((cstring(1:1).eq.'i').or.(cstring(1:1).eq.'I'))then
         read(cstring(2:4),'(i3)')ichk1
           if(ichk1.ne.indxref) then
             read(iunit,'(a)')cstring
             goto 1
           endif
       else
          if((cstring(1:1).eq.'c').or.(cstring(1:1).eq.'C'))then
              write(i4unit(-1),2000)' ','missing archive no.',indxref,
     &                             ' using last archive no.',ichk1
              write(i4unit(-1),2001)
              indxref = ichk1
              rewind(iunit)
          endif
          cstring=' '
          read(iunit,'(a)')cstring
          go to 1
       endif

       read(cstring,'(1x,1i3,2x,1a40,27x,1a1)')ichk1,title,cameth
       
       if((cameth.eq.'a').or.(cameth.eq.'A'))then
           cameth = 'a'
       
c-----------------------------------------------------------------------
c  Born archive detected
c-----------------------------------------------------------------------
        
           cstrgin =' '
	   nfirst  = 1
	   iword   = 9
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
           if(nwords.eq.9) then
	       if(cstring(ifirsta(1):ilasta(1)).eq.'z0')
     &              read(cstring(ifirsta(3):ilasta(3)),*)z0
	       if(cstring(ifirsta(4):ilasta(4)).eq.'z')
     &              read(cstring(ifirsta(6):ilasta(6)),*)z
	       if(cstring(ifirsta(7):ilasta(7)).eq.'zeff')
     &              read(cstring(ifirsta(9):ilasta(9)),*)zeff
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 2'
               write(i4unit(-1),2003)
	       stop
           endif
        
           cstrgin =' '
	   nfirst  = 1
	   iword   = 9
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
           if(nwords.eq.9) then
	       if(cstring(ifirsta(1):ilasta(1)).eq.'n1')
     &              read(cstring(ifirsta(3):ilasta(3)),*)n1
	       if(cstring(ifirsta(4):ilasta(4)).eq.'l1')
     &              read(cstring(ifirsta(6):ilasta(6)),*)l1
	       if(cstring(ifirsta(7):ilasta(7)).eq.'eb1')
     &              read(cstring(ifirsta(9):ilasta(9)),*)eb1
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 3'
               write(i4unit(-1),2003)
	       stop
           endif
        
           cstrgin =' '
	   nfirst  = 1
	   iword   = 9
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
           if(nwords.eq.9) then
	       if(cstring(ifirsta(1):ilasta(1)).eq.'n2')
     &              read(cstring(ifirsta(3):ilasta(3)),*)n2
	       if(cstring(ifirsta(4):ilasta(4)).eq.'l2')
     &              read(cstring(ifirsta(6):ilasta(6)),*)l2
	       if(cstring(ifirsta(7):ilasta(7)).eq.'eb2')
     &              read(cstring(ifirsta(9):ilasta(9)),*)eb2
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 4'
               write(i4unit(-1),2003)
	       stop
           endif
	 
           read(iunit,'(a)')cstring
        
           cstrgin =' '
	   nfirst  = 1
	   iword   = 9
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
           if(nwords.eq.6) then
	       xjp = -99.0
	       xj1 = -99.0
	       xj2 = -99.0
	       if(cstring(ifirsta(1):ilasta(1)).eq.'isp')
     &              read(cstring(ifirsta(3):ilasta(3)),*)isp
	       if(cstring(ifirsta(4):ilasta(4)).eq.'lp')
     &              read(cstring(ifirsta(6):ilasta(6)),*)lp
           elseif(nwords.eq.9) then
	       xjp=-99.0
	       if(cstring(ifirsta(1):ilasta(1)).eq.'isp')
     &              read(cstring(ifirsta(3):ilasta(3)),*)isp
	       if(cstring(ifirsta(4):ilasta(4)).eq.'lp')
     &              read(cstring(ifirsta(6):ilasta(6)),*)lp
	       if(cstring(ifirsta(7):ilasta(7)).eq.'jp')
     &              read(cstring(ifirsta(9):ilasta(9)),*)xjp
                    cresol='jl'
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 6'
               write(i4unit(-1),2003)
	       stop
           endif
        
           cstrgin = ' '
	   nfirst  = 1
	   iword   = 18
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
     
           if((xjp.lt.0.0).and.(nwords.eq.12)) then
	       xjt1 = -99.0
	       xjt2 = -99.0
	       cresol = 'ls'
	       if(cstring(ifirsta(1):ilasta(1)).eq.'ist1')
     &              read(cstring(ifirsta(3):ilasta(3)),*)ist1
	       if(cstring(ifirsta(4):ilasta(4)).eq.'lt1')
     &              read(cstring(ifirsta(6):ilasta(6)),*)lt1
	       if(cstring(ifirsta(7):ilasta(7)).eq.'ist2')
     &              read(cstring(ifirsta(9):ilasta(9)),*)ist2
	       if(cstring(ifirsta(10):ilasta(10)).eq.'lt2')
     &              read(cstring(ifirsta(12):ilasta(12)),*)lt2
           elseif((xjp.lt.0.0).and.(nwords.eq.18)) then
	       cresol = 'ic'
	       if(cstring(ifirsta(1):ilasta(1)).eq.'ist1')
     &              read(cstring(ifirsta(3):ilasta(3)),*)ist1
	       if(cstring(ifirsta(4):ilasta(4)).eq.'lt1')
     &              read(cstring(ifirsta(6):ilasta(6)),*)lt1
	       if(cstring(ifirsta(7):ilasta(7)).eq.'jt1')
     &              read(cstring(ifirsta(9):ilasta(9)),*)xjt1
	       if(cstring(ifirsta(10):ilasta(10)).eq.'ist2')
     &              read(cstring(ifirsta(12):ilasta(12)),*)ist2
	       if(cstring(ifirsta(13):ilasta(13)).eq.'lt2')
     &              read(cstring(ifirsta(15):ilasta(15)),*)lt2
	       if(cstring(ifirsta(16):ilasta(16)).eq.'jt2')
     &              read(cstring(ifirsta(18):ilasta(18)),*)xjt2
           elseif((xjp.ge.0.0).and.(nwords.eq.12)) then
	       cresol = 'jl'
	       ist1 = 0
	       ist2 = 0
	       lt1 = 0
	       lt2 = 0
	       if(cstring(ifirsta(1):ilasta(1)).eq.'j1')
     &              read(cstring(ifirsta(3):ilasta(3)),*)xj1
	       if(cstring(ifirsta(4):ilasta(4)).eq.'jt1')
     &              read(cstring(ifirsta(6):ilasta(6)),*)xjt1
	       if(cstring(ifirsta(7):ilasta(7)).eq.'j2')
     &              read(cstring(ifirsta(9):ilasta(9)),*)xj2
	       if(cstring(ifirsta(10):ilasta(10)).eq.'jt2')
     &              read(cstring(ifirsta(12):ilasta(12)),*)xjt2
           elseif((xjp.ge.0.0).and.(nwords.eq.6)) then
	       cresol = 'jl'
	       ist1 = 0
	       ist2 = 0
	       lt1 = 0
	       lt2 = 0
	       xjt1=-99.0
	       xjt2=-99.0
	       if(cstring(ifirsta(1):ilasta(1)).eq.'j1')
     &              read(cstring(ifirsta(3):ilasta(3)),*)xj1
	       if(cstring(ifirsta(4):ilasta(4)).eq.'j2')
     &              read(cstring(ifirsta(6):ilasta(6)),*)xj2
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 7'
               write(i4unit(-1),2003)
	       stop
           endif
        
           cstrgin = ' '
	   nfirst  = 1
	   iword   = 15
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
           if(nwords.eq.15) then
	       if(cstring(ifirsta(1):ilasta(1)).eq.'neqv1')
     &              read(cstring(ifirsta(3):ilasta(3)),*)neqv1
	       if(cstring(ifirsta(4):ilasta(4)).eq.'fpc1')
     &              read(cstring(ifirsta(6):ilasta(6)),*)fpc1
	       if(cstring(ifirsta(7):ilasta(7)).eq.'neqv2')
     &              read(cstring(ifirsta(9):ilasta(9)),*)neqv2
	       if(cstring(ifirsta(10):ilasta(10)).eq.'fpc2')
     &              read(cstring(ifirsta(12):ilasta(12)),*)fpc2
	       if(cstring(ifirsta(13):ilasta(13)).eq.'aval')
     &              read(cstring(ifirsta(15):ilasta(15)),*)aval
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 8'
               write(i4unit(-1),2003)
	       stop
           endif
	 
           read(iunit,'(a)')cstring
        
           cstrgin = ' '
	   nfirst  = 1
	   iword   = 12
           read(iunit,'(a)')cstrgin
	   call xxcase(cstrgin,cstring,'lc')
           call xxword( cstring  , ' '    , nfirst ,
     &                  iword    ,
     &                  ifirsta  , ilasta , nwords
     &                   )
           if(nwords.eq.12) then
	       if(cstring(ifirsta(1):ilasta(1)).eq.'xmax')
     &              read(cstring(ifirsta(3):ilasta(3)),*)xmax
	       if(cstring(ifirsta(4):ilasta(4)).eq.'iextwf')
     &              read(cstring(ifirsta(6):ilasta(6)),*)iextwf
	       if(cstring(ifirsta(7):ilasta(7)).eq.'ijucys')
     &              read(cstring(ifirsta(9):ilasta(9)),*)ijucys
	       if(cstring(ifirsta(10):ilasta(10)).eq.'isrch')
     &              read(cstring(ifirsta(12):ilasta(12)),*)isrch
           else
               write(i4unit(-1),2002)'archive structure fault: ',
     &                               'line 10'
               write(i4unit(-1),2003)
	       stop
           endif
	 
   10      read(iunit,'(a)')cstrgin
           call xxcase(cstrgin,cstring,'lc')
           if(index(cstring,'nliq').le.0)go to 10
	   
	   nshell = 0
	   
   20	   read(iunit,'(a)')cstrgin
           call xxcase(cstrgin,cstring,'lc')
           if((cstring(1:10).ne.'          ') .and.
     &        (index(cstring,'-').le.0))then
	       nfirst  = 1
	       iword   = 3
               call xxword( cstring  , ' '    , nfirst ,
     &                      iword    ,
     &                      ifirsta  , ilasta , nwords
     &                     )
               nshell=nshell+1
               read(cstring(ifirsta(1):ilasta(1)),*)nlqs(nshell)
               read(cstring(ifirsta(2):ilasta(2)),*)alfaa(1,nshell)
               read(cstring(ifirsta(3):ilasta(3)),*)alfaa(2,nshell)
	       go to 20
	   endif
	   
	 
   30      read(iunit,'(a)')cstrgin
           call xxcase(cstrgin,cstring,'lc')
           if(index(cstring,'x').le.0)go to 30
	   
	   nx = 0
	   
   40	   read(iunit,'(a)')cstrgin
           call xxcase(cstrgin,cstring,'lc')
           if((cstring(1:10).ne.'          ') .and.
     &        (index(cstring(1:3),'-').le.0))then
	       nfirst  = 1
	       iword   = 1
               call xxword( cstring  , ' '    , nfirst ,
     &                      iword    ,
     &                      ifirsta  , ilasta , nwords
     &                     )
               nx=nx+1
               read(cstring(ifirsta(1):ilasta(1)),*)xa(nx)
 	       go to 40
	   endif
	 
   50      read(iunit,'(a)')cstrgin
           call xxcase(cstrgin,cstring,'lc')  
           if(index(cstring,'te(k)').le.0)go to 50
	   
	   nt = 0
	   
   60	   read(iunit,'(a)')cstrgin
           call xxcase(cstrgin,cstring,'lc')
           if((cstring(1:10).ne.'          ') .and.
     &        (index(cstring(1:3),'-').le.0))then
	       nfirst  = 1
	       iword   = 1
               call xxword( cstring  , ' '    , nfirst ,
     &                      iword    ,
     &                      ifirsta  , ilasta , nwords
     &                     )
               nt=nt+1
               read(cstring(ifirsta(1):ilasta(1)),*)tea(nt)
 	       go to 60
	   endif
	   
       
	 
	 
       else if(cameth.eq.'b')then
       
c-----------------------------------------------------------------------
c  Impact parameter archive detected
c-----------------------------------------------------------------------
           continue
        
       endif
       
       close( unit=iunit , status = 'keep')
       
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4data warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4data error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       return
       end
