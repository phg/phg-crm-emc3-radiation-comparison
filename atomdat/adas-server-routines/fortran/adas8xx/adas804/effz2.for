       SUBROUTINE EFFZ2(N,L,E,QD,Z0,Z1,ALF,X0,X1,X2,D,M0)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C PURPOSE: Unknown.
C
C-----------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 08-04-2010
C  MODIFIED : Martin O'Mullane
C              - Remove listing information from colums 72+.
C              - Add comment.
C-----------------------------------------------------------------------
       DIMENSION F(1000),V(1000),VS(100),AMP(20),VS1(100)
       JMAX=20
       I1=256
       H=X1/256.0
       S1=1.2
       S2=1.0/S1
       M=-1
       M1=1
       ZC=Z1
       Z2=0.0D0
       Z3=0.0D0
       CALL VSER(Z0,Z1,VS1)
       VS(1)=VS1(1)
       J=0
    1  J=J+1
       IF(J-JMAX)3,3,2
    2  WRITE(0,100)
       RETURN
    3  CONTINUE
       X=0.0
       DO 4 I=1,I1
       X=X+H
       V(I)=VPOT(Z0,Z1,ALF,X)
    4  CONTINUE
       T=1.0
       DO 5 I=2,100
       IF(T-1.0D60)40,41,41
   40  T=T*ALF
   41  CONTINUE
       VS(I)=VS1(I)*T
    5  CONTINUE
       CALL FCF6(F,C,A2,AMP,PHI,QD1,X0,N,L,E,Z0,ZC,ALF,V,Z1,Z2,Z3,
     1 VS,X1,H,X2)
       IF(E+1.0D-40)30,31,31
   30  DEL=QD1
       GO TO 32
   31  DEL=QD-QD1
   32  CONTINUE
       IF(M)6,9,16
    6  XA=ALF
       YA=DEL
       IF(DEL)7,20,8
    7  ALF=S1*ALF
       M=0
       GO TO 1
    8  ALF=S2*ALF
       M=0
       GO TO 1
    9  XB=ALF
       YB=DEL
       IF(YA*YB)13,20,10
   10  IF(YB)11,20,12
   11  ALF=S1*ALF
       YA=YB
       XA=XB
       GO TO 1
   12  ALF=S2*ALF
       YA=YB
       XA=XB
       GO TO 1
   13  IF(YB)14,14,15
   14  T=XA
       XA=XB
       XB=T
       T=YA
       YA=YB
       YB=T
   15  IF(YA+YB)25,26,26
   25  YMIN=YB
       GO TO 27
   26  YMIN=-YA
   27  YMIN=D*YMIN
       ALF=0.5*(XA+XB)
       M=1
       GO TO 1
   16  IF(DEL)17,20,18
   17  T=XA
       XA=ALF
       YA=DEL
       GO TO 19
   18  T=XB
       XB=ALF
       YB=DEL
   19  IF(DABS(ALF-T)-D*ALF)23,23,24
   23  IF(DABS(DEL)-YMIN)28,28,24
   28  ALF=(XA*YB-XB*YA)/(YB-YA)
       GO TO 20
   24  M1=-M1
       IF(M1)22,22,21
   21  ALF=(XA*YB-XB*YA)/(YB-YA)
       GO TO 1
   22  ALF=0.5*(XA+XB)
       GO TO 1
   20  M0=PHI+0.5
       RETURN
  100  FORMAT(18H   FAILED IN EFFZ2)
      END
