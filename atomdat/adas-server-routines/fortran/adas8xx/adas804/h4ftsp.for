C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4ftsp.for,v 1.1 2004/07/06 14:00:18 whitefor Exp $ Date $Date: 2004/07/06 14:00:18 $
C
       subroutine h4ftsp( istdim  ,
     &                    x       , xa   , n    , yaa  , y    , dy    ,
     &                    i0      , c1   , c2   , c3   , c4   , isw
     &                  )            
       implicit none                                         
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4ftsp.for *********************
c
c  purpose: obtain the value from a spline interpolation
c
c  calling program: various
c
c  
c  input : (i*4)  istdim  = dimensionality for splining arrays
c
c  input : (r*8)  x       = required x-value
c  input : (r*8)  xa(i)   = x-values
c  input : (i*4)  n       = number of values
c  input : (r*8)  yaa(i)  = y-values (possibly stored as multiple sets)
c  input : (i*4)  i0      = starting index(-1) in yaa array of required input set
c  input : (r*8)  c1(i,j) = 1st spline coefficient precursor
c  input : (r*8)  c2(i,j) = 2nd spline coefficient precursor
c  input : (r*8)  c3(i,j) = 3rd spline coefficient precursor
c  input : (r*8)  c4(i,j) = 4th spline coefficient precursor
c  input : (i*4)  isw     = .le.0  ordinary     spline interpolation
c                         = .gt.0  logarithmic  spline interpolation
c
c  output: (r*8)  y       = returned y-value
c  output: (r*8)  dy      = returned derivative
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer  nstdim
c-----------------------------------------------------------------------
       parameter ( nstdim = 40)
c-----------------------------------------------------------------------
       integer  istdim , i4unit
       integer  n      , i0     , isw    , i       , j      , j1
c-----------------------------------------------------------------------
       real*8   t      , xb     , x      , y       , dy
       real*8   ct1    , ct2    , ct3    , ct4
c-----------------------------------------------------------------------
       real*8   yaa(*)               , xa(istdim)
       real*8   ta(nstdim)                      
       real*8   c1(istdim,istdim-1)  , c2(istdim,istdim-1)  ,
     &          c3(istdim,istdim-1)  , c4(istdim,istdim-1)                  
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  check internal dimensions are compatible
c-----------------------------------------------------------------------

       if(nstdim.lt.istdim) then
           write(i4unit(-1),2002)' ','nstdim=',nstdim,
     &                           ' < istdim=',istdim
	   write(i4unit(-1),2003)
	   stop
       endif
       
       do i = 1,n                                                       
         t = yaa(i+i0)                                                      
         if(isw.gt.0)t=dlog(t)                                            
         ta(i)=t
       enddo
       
       do j=2,n                                                      
         if(x.le.xa(j))then                                           
             xb = 0.5d0*(xa(j-1)+xa(j))                                         
             j1 = j-1                                                           
             go to 25
         endif                                                         
       enddo
                                                                
       xb=0.5*(xa(n-1)+xa(n))                                           
       j1=n-1                                                           
   25  xb=x-xb
       
       ct1 = 0.0d0
       ct2 = 0.0d0
       ct3 = 0.0d0
       ct4 = 0.0d0
                                                                 
       do i = 1,n                                                      
         ct1 = ct1+c1(i,j1)*ta(i)                                   
         ct2 = ct2+c2(i,j1)*ta(i)                                   
         ct3 = ct3+c3(i,j1)*ta(i)                                   
         ct4 = ct4+c4(i,j1)*ta(i)
       enddo                                   
                                                                
       y  = ct1+xb*(ct2+xb*(ct3+xb*ct4))                   
       dy = ct2+xb*(2.0d0*ct3+xb*3.0d0*ct4)
                          
       if(isw.gt.0)then
           y=dexp(y)                                                        
           dy=dy*y
       endif
       
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4ftsp warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4ftsp error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
                                                                 
       return                                                           
       end                                                              
