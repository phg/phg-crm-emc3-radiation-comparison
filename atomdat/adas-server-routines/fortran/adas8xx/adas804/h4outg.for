C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4outg.for,v 1.1 2004/07/06 14:00:46 whitefor Exp $ Date $Date: 2004/07/06 14:00:46 $
C
       subroutine h4outg ( ixdim  , itdim , 
     &                     nx     , xa    , omga   , 
     &                     nt     , tea   , upsa   ,
     &                     ixtyp  , s12   , e12    ,
     &                     ianopt , bcval , 
     &                     iasel  ,
     &                     lpend  , larch , dsarch 
     &                   )
       implicit none
c-------------------------------------------------------------------------
c
c  **************** fortran 77 subroutine h4outg.for  ********************
c
c
c  purpose: to send output from h4born subroutine to idl
c           graph editor via the unix pipe. 
c
c  calling program: adas804.for
c
c  subroutine:
c
c  input : (i*4)  ixdim    = maximum dimension for X  array
c  input : (i*4)  itdim    = maximum dimension for Te array
c  input : (i*4)  nx       = number of incident electron energies
c  input : (r*8)  xa()     = threshold parameter values 
c  output: (r*8)  omga()   = omegas at input values of x-parameter
c  input : (i*4)  nt       = number of electron temperatures
c  input : (r*8)  tea()    = electron temperatures (K)
c  input : (r*8)  upsa()   = upsilon at onput values of Te
c  input : (i*4)  ixtyp    = transition type 1=dipole;2=non-dipole;
c                                            3=spin-change
c  input : (r*8)  s12      = line strength (dipole) otherwise zero
c  input : (r*8)  e12      = level energy difference (Ryd)
c  input : (i*4)  ianopt   = analysis option
c                            1 - adas
c                            2 - burgess
c  input : (i*4)  iasel    = the original archiving option (see h4spf0)
c  input : (r*8)  bcval    = Burgess c-value scaling parameter
c
c  output: (l*4)  larch    = archiving selection option
c  output: (l*4)  lpend    = .false. => done
c                            .true.  => cancel
c  output: (c*80) dsarch   = the archive file name
c
c  routines:
c          xxslen - removes the spaces in a string
c          xxflsh - clears the pipe buffer
c
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c  date:   19/07/02 
c
c  update:  
c
c
c-------------------------------------------------------------------------
       integer ixdim  , itdim  
       integer i4unit , iasel , nx     , nt
       integer ianopt , ixtyp 
       integer i      , logic , iarch  , ifirst , ilast  
c-------------------------------------------------------------------------
       real*8  e12    , s12   , bcval
c-------------------------------------------------------------------------
       logical lpend  , larch
c-------------------------------------------------------------------------
       character dsarch*80 , cstring*80
c-------------------------------------------------------------------------
c-------------------------------------------------------------------------
       real*8 xa(ixdim)  , omga(ixdim)  
       real*8 tea(itdim) , upsa(itdim)
c-------------------------------------------------------------------------
       integer pipein , pipeou 
c-------------------------------------------------------------------------
       parameter( pipein = 5 , pipeou = 6 )
c-------------------------------------------------------------------------
c-------------------------------------------------------------------------

       write(pipeou,1000) nx
       call xxflsh(pipeou)

       do 10 i = 1, nx
           write(pipeou,1001) xa(i), omga(i)
	   call xxflsh(pipeou)
 10    continue

       write(pipeou,1000) nt
       call xxflsh(pipeou)
       
       do 20 i = 1, nt
           write(pipeou,1001) tea(i), upsa(i)
	   call xxflsh(pipeou)
 20    continue
  
       write(pipeou,1000)ixtyp
       call xxflsh(pipeou)
       write(pipeou,1002)s12
       call xxflsh(pipeou)
       write(pipeou,1002)e12
       call xxflsh(pipeou)
       
       write(pipeou,1000)ianopt
       call xxflsh(pipeou)
       write(pipeou,1002)bcval
       call xxflsh(pipeou)
c
c-------------------------------------------------------------------------
c read alterations from idl
c-------------------------------------------------------------------------
c
       lpend = .false.
       read(pipein,*) logic
       if (logic.eq.1) then
           lpend = .true.
       else
           lpend = .false.
           read(pipein,*) iarch
       endif
       if (iarch.eq.1) then
           larch = .true.
       else
           larch = .false.
       endif
       if (lpend) go to 999
c
c-----------------------------------------------------------------------
c if archiving is selected after initially specifying no archive
c then the default is to create a file called archive.dat in the 
c arch804 directory
c-----------------------------------------------------------------------
c
       if (larch) then
           if (iasel.eq.0) then
               read(pipein,1004) cstring
           endif
       endif
       call xxslen( cstring , ifirst , ilast )
       read(cstring(ifirst:ilast),1004) dsarch

c-------------------------------------------------------------------------
 1000  format(1x,i3)
 1001  format(1x,1pe12.4,1x,1pe12.4)
 1002  format(1x,1pe12.4)
 1004  format(1a80)
c-------------------------------------------------------------------------

  999  return
       end
