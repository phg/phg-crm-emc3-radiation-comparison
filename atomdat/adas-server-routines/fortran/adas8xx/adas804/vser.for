       SUBROUTINE VSER(Z0,Z1,VS)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C PURPOSE: Unknown.
C
C-----------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 08-04-2010
C  MODIFIED : Martin O'Mullane
C              - Remove listing information from colums 72+.
C              - Add comment.
C-----------------------------------------------------------------------
       DIMENSION VS(100),A(100),B(100)
       T=0.0
       A(1)=2.0D60*(Z0-Z1)
       B(1)=1.0
       DO 1 I=2,60
       T=T+1.0
       A(I)=-0.2075*A(I-1)/T
       B(I)=-1.19*B(I-1)
    1  CONTINUE
       DO 4 I=61,100
       T=T+1.0
       A(I)=0.0
       B(I)=-1.19*B(I-1)
    4  CONTINUE
       VS(1)=Z0+Z0
       DO 3 I=2,100
       S=0.0
       DO 2 J=1,I
       J1=I-J+1
       S=S+A(J)*B(J1)
    2  CONTINUE
       VS(I)=S*1.0D-60
    3  CONTINUE
       RETURN
      END
