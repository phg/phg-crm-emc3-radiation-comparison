C UNIX-IDL - SCCS info: Module @(#)adas804.for	1.1 Date 03/18/03
C
       program adas804
       implicit none
C-----------------------------------------------------------------------
C
C  ******************* fortran 77 program: adas804 *********************
C
C  Purpose: calculation of electron impact excitation cross-sections 
C           by effective potentail Born or impact parameter methods. 
C
C  Data:
C           data is entered via the idl interface, but can also be 
C           recovered from the users' own archive files. these 
C           typically reside in '/.../<uid>/adas/arch804'.
C           particular transition datasets have their own index
C           number and this is entered on the first panel after
C           selecting 'refresh from old archive'. there is no
C           central adas archive.
C  
C  Program:
C          (i*4)  indxref= the index number to refresh data from.
C          (i*4)  itout  = number of temperatures
C          (i*4)  ichk1  = checks for the right index number
C          (i*4)  ixtyp  = 1  dipole transition                                    
C                        = 2  non-dipole transition                                
C                        = 3  spin change transition                               
C                        = 4  other     
C          (i*4)  iasel  = the archiving choice.
C                          0 - no archive
C                          1 - examine old archive
C                          2 - refresh from archive
C                          3 - new archive
C          (i*4)  iunt07 = a free unit number
C          (i*4)  ntabcol= the number of columns in the editable 
C                          idl table
C          (i*4)  igrpout= graph display selection switch
C          (i*4)  ianopt = analysis option
C                          1 - adas
C                          2 - burgess
C          (i*4)  igrpscal1 = default(0) or user scaling(1) switch for  
C                             ist graph.
C          (i*4)  igrpscal2 = default(0) or user scaling(1) switch for  
C                             2nt graph.
C          (i*4)  itexout= hard copy of text output request switch.
C                          0 - no
C                          1 - yes
C          (c*80) dsfull = the selected archive name.
C          (c*3)  rep    = 'yes' - finish program run
C                          'no ' - continue
C          (c*40) title  = the information line in the archive
C                          file.
C          (c*1)  cameth   = the tag distinguishing the type of 
C                            analysis: a - Born, b- IP
C          (c*8)  date   = the current data 
C          (c*80) cheader= the adas header for the paper.txt file
C          (c*80) texdsn = the default hard copy file name
C          (c*80) dsarch = the archive file name
C          (c*40) cgtit  = the graph title (information for any
C                          new archive)
C          (r*8)  z0     = nuclear charge of ion                 
C          (r*8)  z      = ion charge                             
C          (r*8)  zeff   = ion charge + 1 
C          (r*8)  bcval  = burgess scalable parameter
C          (r*8)  xmin1  = user selected 1st graph x-axis minimum.
C          (r*8)  xmax1  = user selected 1st graph x-axis maximum.
C          (r*8)  ymin1  = user selected 1st graph y-axis minimum.
C          (r*8)  ymax1  = user selected 1st graph y-axis maximum.
C          (r*8)  xmin2  = user selected 2nd graph x-axis minimum.
C          (r*8)  xmax2  = user selected 2nd graph x-axis maximum.
C          (r*8)  ymin2  = user selected 2nd graph y-axis minimum.
C          (r*8)  ymax2  = user selected 2nd graph y-axis maximum.
C          (l)    lpend  = cancel or done flag
C          (l)    larch  = archiving selection option
C          (r*8)  xa     = energy (parameter x)
C          (r*8)  ya     = omega (collision strength)
C          (r*8)  z1     = z+1                   
C          (r*8)  x      = temp. value (energy) 
C          (r*8)  y      = temp. value (xsect) 
C          (r*8)  te     = temp. value (temperature)a
C          (i*4)  istop  = flag used by idl to signal an immediate end to
C                          the program
C  Routines:  
C          routine    source    brief description
C          -------------------------------------------------------------
C          h4born  adas    calculates Born approximation xsects & rates
C          h4spf0   adas   gathers input archive name via idl pipe
C          h4data   adas   reads selected data from archive
C          h4ispf   adas   pipes data to idl reads user processing edits 
C          h4spf1   adas   gathers user selected output options
C          h4outg   adas   sends graphic data to idl g
C          h4out0   adas   writes data to archive/text output files
C          xx0000   adas   sets machine dependent adas configuration
C          xxdate   adas   provides current date
C          xxslen   adas   removes spaces from a string
C          i4unit   adas   fetch unit number for output of messages
C          xxflsh   adas   clears idl pipe buffer
C
C  Author:  Hugh P. Summers, University of Strathclyde
C           JA7.08
C           Tel.: +44 (0)141-548-4196
C
C  Date:    24 February 2003
C
C  Update: HP Summers  21/05/04  correct errors, separate archive and
C                                standard output handling. Add calc- 
C                                ulation information strings to 
C                                parameter output
C
C
C  Version: 1.1   Hugh Summers  24/02/03
C  Modified:      first release
C
C  Version: 1.2   Hugh Summers  21/04/04
C  Modified:       - corrections and extension as above
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       integer   iunt12   , ixdim     , itdim     , isdim 
       integer   iunt07   , ntabcol   , pipein
       integer   ndinfo 
C-----------------------------------------------------------------------
       parameter ( iunt12 = 12 , ixdim   = 40 , itdim = 14, isdim = 10 )
       parameter ( iunt07 = 7  , ntabcol =  2 , pipein = 5 )
       parameter ( ndinfo = 100 )
C-----------------------------------------------------------------------
       integer   i4unit
       integer   indxref  , 
     &           n1       , l1        , n2        , l2       ,           
     &           isp      , lp        , 
     &           ist1     , lt1       , ist2      , lt2      , 
     &           neqv1    , neqv2     , 
     &           iextwf   , ijucys    , isrch     ,
     &           nshell   , nx        , nt
       integer   iasel    , igrpout   , itexout   , istop
       integer   ianopt   , igrpscal1 , igrpscal2 
       integer   ixtyp 
       integer   i        , j
       integer   ninfo  
C-----------------------------------------------------------------------
       real*8    z0       , z         , zeff      , 
     &           eb1      , eb2       , 
     &           xjp      , xj1       , xjt1      , xj2     , xjt2    ,
     &           fpc1     , fpc2      ,    
     &           aval     , xmax      , e12       , s12
       real*8    bcval       
       real*8    xmin1    , xmax1     , ymin1     , ymax1
       real*8    xmin2    , xmax2     , ymin2     , ymax2
C-----------------------------------------------------------------------
       character dsfull*80      , dsarch*80       , title*40        ,
     &           ixtit*40       , dsnout*80       , 
     &           cameth*1       , cresol*2
       character cgtit*40       , cheader*80      , texdsn*80  
       character date*8         , rep*3
C-----------------------------------------------------------------------
       integer   nlqs(isdim)
       integer   ns(isdim)      , ls(isdim)       , iqs(isdim)
C-----------------------------------------------------------------------
       real*8    alfaa(2,isdim) , 
     &           xa(ixdim)      , omga(ixdim)
       real*8    tea(itdim)     , upsa(itdim) 
C-----------------------------------------------------------------------
       character cinfoa(ndinfo)*90
C-----------------------------------------------------------------------
       logical open7 , larch , lpend
C-----------------------------------------------------------------------
       data open7/.false./
C-----------------------------------------------------------------------
C *************************** main program ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C set machine dependent adas configuration values.
C-----------------------------------------------------------------------
C
       call xx0000
C
C-----------------------------------------------------------------------
C get current date.
C-----------------------------------------------------------------------
C
       call xxdate( date )
C
C-----------------------------------------------------------------------
C  initialise information strings
C-----------------------------------------------------------------------
       do i=1,ndinfo
         cinfoa(i)=' '
       enddo
       ninfo=0   

C-----------------------------------------------------------------------
  100  continue
C-----------------------------------------------------------------------
C  if files are active on unit 7 - close it
C-----------------------------------------------------------------------
       if (open7) close(7)
       open7=.false.
C
C-----------------------------------------------------------------------
C get archive name from idl
C-----------------------------------------------------------------------
C
       call h4spf0( rep     , dsfull  ,
     &              iasel   , indxref )
     
C
C-----------------------------------------------------------------------
C  if program run completed end  
C-----------------------------------------------------------------------
C
       if (rep.eq.'yes') then
           goto 999
       endif
C
C-----------------------------------------------------------------------
C open archive and read if refreshing
C-----------------------------------------------------------------------
C
        if (iasel.eq.2) then

             call h4data( iunt12 , ixdim  , itdim   , isdim  ,
     &                    dsfull , indxref , title  , cameth , 
     &                    z0     , z       , zeff   , 
     &                    n1     , l1      , eb1    ,
     &                    n2     , l2      , eb2    ,
     &                    isp    , lp      , xjp    ,
     &                    ist1   , lt1     , xj1    , xjt1   , 
     &                    ist2   , lt2     , xj2    , xjt2   ,
     &                    neqv1  , fpc1    , neqv2  , fpc2   ,
     &                    aval   ,
     &                    xmax   , iextwf  , ijucys , isrch  ,
     &                    nshell , nlqs    , alfaa  ,
     &                    nx     , xa      , 
     &                    nt     , tea     ,
     &                    cresol  
     &                  )
     
       else
            nshell = 0
            nlqs(1)= 0
C ****** temporary setting
            cameth = 'a'
            
 
       endif
       
C
C-----------------------------------------------------------------------
C
  200  continue
C
C-----------------------------------------------------------------------
C read user processing options from idl via unix pipe
C-----------------------------------------------------------------------
C

       do i = 1,nshell
          j  = nlqs(i)                                                        
          ns(i) = j/1000
          ls(i) = (j-1000*ns(i))/100
          iqs(i) = j-1000*ns(i)-100*ls(i)
       enddo
       

       call h4ispf( ixdim  , itdim   , isdim  ,
     &              dsfull , indxref , title  , cameth , 
     &              z0     , z       , zeff   , 
     &              n1     , l1      , eb1    ,
     &              n2     , l2      , eb2    ,
     &              isp    , lp      , xjp    ,
     &              ist1   , lt1     , xj1    , xjt1   , 
     &              ist2   , lt2     , xj2    , xjt2   ,
     &              neqv1  , fpc1    , neqv2  , fpc2   ,
     &              aval   ,
     &              xmax   , iextwf  , ijucys , isrch  ,
     &              nshell , ns      , ls     , iqs    , alfaa  ,
     &              nx     , xa      , 
     &              nt     , tea     ,
     &              iasel  , ntabcol , lpend  
     &            )
     

C
C-----------------------------------------------------------------------
C read signal from idl for immediate termination (1) or continue (0)
C-----------------------------------------------------------------------
C
      read(pipein,*) istop
      if(istop.eq.1) goto 999
C
C-----------------------------------------------------------------------
C
       if (lpend) goto 100
C
C-----------------------------------------------------------------------
C read user output options from idl via unix pipe  
C-----------------------------------------------------------------------
C
 300   continue
C
C-----------------------------------------------------------------------

       call h4spf1( lpend     , igrpout   , ianopt    ,
     &              igrpscal1 , igrpscal2 , itexout   ,
     &              cgtit     , cheader   , texdsn    , bcval     ,
     &              xmin1     , xmax1     , ymin1     , ymax1     ,
     &              xmin2     , xmax2     , ymin2     , ymax2      
     &             )
     

C-----------------------------------------------------------------------
C read signal from idl for immediate termination (1) or continue (0)
C-----------------------------------------------------------------------
C
      read(pipein,*) istop
      if(istop.eq.1) goto 999
C
C-----------------------------------------------------------------------
C
       if (lpend) goto 200
C
C-----------------------------------------------------------------------
C process analysis option 
C-----------------------------------------------------------------------

       call h4born( ixdim  , itdim   , isdim  , ndinfo ,
     &              cameth , 
     &              z0     , z       , zeff   , 
     &              n1     , l1      , eb1    ,
     &              n2     , l2      , eb2    ,
     &              isp    , lp      , xjp    ,
     &              ist1   , lt1     , xj1    , xjt1   , 
     &              ist2   , lt2     , xj2    , xjt2   ,
     &              neqv1  , fpc1    , neqv2  , fpc2   ,
     &              aval   , 
     &              xmax   , iextwf  , ijucys , isrch  ,
     &              nshell , ns      , ls     , iqs    , alfaa  ,
     &              nx     , xa      , omga   , 
     &              nt     , tea     , upsa   ,
     &              cresol , ixtyp   , s12    , e12    ,
     &              ninfo  , cinfoa         
     &            )
     
         if (igrpout.eq.1)then
             call h4outg ( ixdim  , itdim , 
     &                     nx     , xa    , omga   , 
     &                     nt     , tea   , upsa   ,
     &                     ixtyp  , s12   , e12    ,
     &                     ianopt , bcval ,
     &                     iasel  ,
     &                     lpend  , larch , dsarch 
     &                   )
C
C-----------------------------------------------------------------------
C read signal from idl for immediate termination (1) or continue (0)
C-----------------------------------------------------------------------
C     
           read(pipein,*) istop
           if(istop.eq.1) goto 999
        endif 
C
C-----------------------------------------------------------------------
C
       if (lpend) goto 300
C
C-----------------------------------------------------------------------
C if no archiving end complete program run
C-----------------------------------------------------------------------
C
       if (.not.larch.and.itexout.ne.1)then
          goto 999
       endif
C
C-----------------------------------------------------------------------
C copy dsarch to dsfull if archiving selected after picking no archive
C-----------------------------------------------------------------------
C
       if (larch) then
           if (iasel.eq.0)then
             dsfull = dsarch
             iasel = 3
           endif
       endif
       if (iasel.ne.0)then
           if (iasel.eq.2)then
              ixtit = title
           else
              ixtit = cgtit
           endif
           
           call xxrmve(dsfull,dsnout,' ')

           call h4out0( ixdim    , itdim   , isdim   , 
     &                  dsnout   , indxref , ixtit   , cameth , 
     &                  z0       , z       , zeff    ,
     &                  n1       , l1      , eb1     ,
     &                  n2       , l2      , eb2     ,
     &                  isp      , lp      , xjp     ,
     &                  ist1     , lt1     , xj1     , xjt1   , 
     &                  ist2     , lt2     , xj2     , xjt2   ,
     &                  neqv1    , fpc1    , neqv2   , fpc2   ,
     &                  aval     , 
     &                  xmax     , iextwf  , ijucys  , isrch  ,
     &                  nshell   , ns      , ls      , iqs    , alfaa  ,
     &                  nx       , xa      , omga    , 
     &                  nt       , tea     , upsa    ,
     &                  date     , ianopt  , iasel   ,
     &                  cheader  , texdsn  , itexout , larch    
     &                  )
       endif

C
C-----------------------------------------------------------------------
C  send results to standard output file if required
C-----------------------------------------------------------------------
C
       if (itexout.eq.1) then
       
           ixtit = title
           call xxrmve(texdsn,dsnout,' ')

           call h4out1( ixdim    , itdim   , isdim   , ndinfo , 
     &                  dsnout   , indxref , ixtit   , cameth , 
     &                  z0       , z       , zeff    ,
     &                  n1       , l1      , eb1     ,
     &                  n2       , l2      , eb2     ,
     &                  isp      , lp      , xjp     ,
     &                  ist1     , lt1     , xj1     , xjt1   , 
     &                  ist2     , lt2     , xj2     , xjt2   ,
     &                  neqv1    , fpc1    , neqv2   , fpc2   ,
     &                  aval     , 
     &                  xmax     , iextwf  , ijucys  , isrch  ,
     &                  nshell   , ns      , ls      , iqs    , alfaa  ,
     &                  nx       , xa      , omga    , 
     &                  nt       , tea     , upsa    ,
     &                  date     , ianopt  , iasel   ,
     &                  cheader  , texdsn  , itexout , larch  ,
     &                  ninfo    , cinfoa    
     &                  )
       endif


C
C-----------------------------------------------------------------------
C
 999   stop
       end
