C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4angf.for,v 1.1 2004/07/06 14:00:02 whitefor Exp $ Date $Date: 2004/07/06 14:00:02 $
C
       subroutine h4angf( xsp   , xlp   , xl0   , xst0  , xlt0  ,
     &                    xl1   , xst1  , xlt1  , xnq   , fpc   ,
     &                    xjp   , xj0   , xjt0  , xj1   , xjt1  ,
     &                    anga  , lama  , nlam  
     &                )                                                        
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: h4angf *********************
c
c  purpose:  calculates angular factors for born approximation
c
c
c  subroutine:
c
c  input : (r*8)  xsp       = 2*Sp+1 parent spin angular momentum 
c  input : (r*8)  xlp       = Lp parent orbital angular momentum
c  input : (r*8)  xl0       = l initial valence electron orbital 
c  input : (r*8)  xst0      = 2*S+1  total initial spin anular momentum 
c  input : (r*8)  xlt0      = L  total initial orbital angular momentum
c  input : (r*8)  xl1       = l' final valence electron orbital    
c  input : (r*8)  xst1      = 2*S'+1  total initial spin anular momentum
c  input : (r*8)  xlt1      = L'  total initial orbital angular momentum
c  input : (r*8)  xnq       = number of equivalent electrons in initial
c                             state 
c  input : (r*8)  fpc       = fractional parentage coefficient
c  input : (r*8)  xjp       = Jp  parent total angular momentum
c  input : (r*8)  xj0       = j  initial valence electron angular mom.
c  input : (r*8)  xjt0      = J  total initial angular momentum
c  input : (r*8)  xj1       = j'  final valence electron angular mom.
c  input : (r*8)  xjt1      = J'  total initial angular momentum
c
c  output: (r*8)  anga()   = angular factor for each multipole
c  output: (i*4)  lama()   = multipole
c  output: (i*4)  nlam     = number of multipoles in anga vector
c
c                      
c  output: (c*80) cstrg     = string marking end of partition block
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          wig3j      adas      evaluates the wigner 3-j symbol
c          wig6j      adas      evaluates the wigner 6-j symbol
c          i4unit     adas      fetch unit number for output of messages
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c  date:    24/02/03
c
c  update:  
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
       integer    i4unit
       integer    l0     , l1     , isp    , j0    , j1 
       integer    nlam   , lam    
c-----------------------------------------------------------------------
       real*8     xsp    , xlp    ,
     &            xl0    , xst0   , xlt0   , xl1   , xst1   , xlt1  ,  
     &            xjp    , xj0    , xjt0   , xj1   , xjt1 
       real*8     xnq    , fpc
       real*8     xlam   , w1     , w2     , w3    , t1    , t2     , 
     &            t3     , ang    , st0
       real*8     wig3j  , wig6j 
c-----------------------------------------------------------------------
       integer    lama(20)     
c-----------------------------------------------------------------------
       real*8     anga(20)                                      
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
       l0=xl0+0.01d0                                                    
       l1=xl1+0.01d0                                                    
       isp=xsp+0.01d0                                                   
       j0=iabs(l0-l1)                                                   
       j1=l0+l1 
                                                               
       nlam=0 
                                                                 
       do lam=j0,j1
                                                         
         xlam=lam                                                         
         w2=wig3j(xl0,xl1,xlam,0.0d0,0.0d0,0.0d0)
         t2=(2.0d0*xlam+1.0d0)*(2.0d0*xl1+1.0d0)*w2*w2
	                     
	 if(xjp.lt.0.0d0) then                         
             w1=wig6j(xl0,xlt0,xlp,xlt1,xl1,xlam)                             
             t1=(2.0d0*xl0+1.0d0)*(2.0d0*xlt1+1.0d0)*
     &           xnq*fpc*fpc*w1*w1
	 
	     if ((xjt0.ge.0.0d0).and.(xjt1.ge.0.0d0))     then
	         st0=(xst1-1.0d0)/2.0d0
c		 write(i4unit(-1),*)
c     &                'h4angf:xlam,xjt0,xjt1,st0,xlt1,xlt0=',
c     &                        xlam,xjt0,xjt1,st0,xlt1,xlt0
	         w3=wig6j(xlam,xjt0,xjt1,st0,xlt1,xlt0)
	         t3=(2.0d0*xjt1+1.0d0)*(2.0d0*xlt0+1.0d0)*w3*w3
	     elseif ((xjt0.ge.0.0d0).and.(xjt1.lt.0.0d0)) then
	         t3= 1.0d0
	     elseif ((xjt0.lt.0.0d0).and.(xjt1.ge.0.0d0)) then
	         t3= (2.0d0*xjt1+1.0d0)/(xst1*(2.0d0*xlt1+1.0d0))
	     elseif ((xjt0.lt.0.0d0).and.(xjt1.lt.0.0d0)) then
	         t3=1.0d0
	     else
	         t3=0.0d0
	     endif
	     
             ang=t1*t2*t3 
	     
c	     write(i4unit(-1),*)'h4angf:lam,t1,t2,t3=',lam,t1,t2,t3
	                                                        
             if(isp.eq.1.and.dabs(xst0-xst1).ge.1.0d-3)ang=0.0d0              
             if(dabs(xst0-xst1).ge.1.0d-3)ang=0.5d0*xst1*ang/xsp
	     
	 else
             w1=wig6j(xlam,xj1,xj0,0.5d0,xl0,xl1)                             
             t1=(2.0d0*xl0+1.0d0)*(2.0d0*xj1+1.0d0)*
     &           xnq*fpc*fpc*w1*w1
	 
	 
	     if ((xjt0.ge.0.0d0).and.(xjt1.ge.0.0d0))     then
	         w3=wig6j(xlam,xjt0,xjt1,xjp,xj1,xj0)
	         t3=(2.0d0*xjt1+1.0d0)*(2.0d0*xj0+1.0d0)*w3*w3
	     elseif ((xjt0.ge.0.0d0).and.(xjt1.lt.0.0d0)) then
	         t3= 1.0d0
	     elseif ((xjt0.lt.0.0d0).and.(xjt1.ge.0.0d0)) then
	         t3= (2.0d0*xjt1+1.0d0)/
     &               ((2.0d0*xjp+1)*(2.0d0*xj1+1.0d0))
	     elseif ((xjt0.lt.0.0d0).and.(xjt1.lt.0.0d0)) then
	         t3=1.0d0
	     else
	         t3=0.0d0
	     endif
	     
             ang=t1*t2*t3 
	                                                        
	 
	 endif
	               
         if(ang.gt.1.0d-6)then                                        
             nlam=nlam+1                                                      
             lama(nlam)=lam                                                   
             anga(nlam)=ang 
	 endif
	                                                   
       enddo
                                                            
       return
                                                                  
      end                                                               
