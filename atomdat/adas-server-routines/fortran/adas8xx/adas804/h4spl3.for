C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4spl3.for,v 1.1 2004/07/06 14:01:08 whitefor Exp $ Date $Date: 2004/07/06 14:01:08 $
C
       subroutine h4spl3( istdim ,
     &                    n      , h      , w     
     &                  )
       implicit none                                         
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4spl3.for *******************
c
c  purpose: calculate splines with various end conditions
c
c  calling program: h4gspc
c
c
c  notes: (1) uses labelled common  /espl3/
c         (2) conditions at 1st & nth nodes controlled by iend1 & iendn    
c           iend = 1 : specified d log(y) ie. dy/y at node stored in 
c                    appropriate vector
c                = 2 : zero curvature                                          
c                = 3 : constant curvature                                      
c                = 4 : matched to specified functional form in terms of        
c                      two parameters a and b such that                        
c                        funct = p(1)*a+q(1)*b                          
c                        1st deriv. = p(2)*a+q(2)*b                          
c                        2nd deriv. = p(3)*a+q(3)*b                          
c                        where a1,b1,p1,q1 are used for 1st node and             
c                        an,bn,pn,qn for nth node                                
c
c  input : (i*4)  istdim  = dimensionality for splining arrays
c
c  input : (i*4)  n    = number of knots
c  input : (r*8)  h()  = intervals between knots
c  
c  output: (r*8)  w(,) = spline matrix
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          h4spl3     adas      
c          i4unit     adas      fetch unit number for output of messages
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer  nstdim
c-----------------------------------------------------------------------
       parameter ( nstdim = 40)
c-----------------------------------------------------------------------
       integer  istdim , i4unit , n
       integer  iend1  , iendn
       integer  i      , j      , k      
c-----------------------------------------------------------------------
       real*8   g1     , gn    ,     
     &          a1     , b1    , an    , bn
       real*8   x      , x3    , y     , y3    ,
     &          dt1    , dtn   , t1    , t2 
c-----------------------------------------------------------------------
       real*8 h(istdim)    , w(istdim,istdim)                                       
       real*8 a(nstdim)    , b(nstdim)     , c(nstdim)
       real*8 a1ry(nstdim) , b1ry(nstdim)  , anry(nstdim)  ,
     &        bnry(nstdim)
       real*8 p1(3)        , q1(3)         , pn(3)         , qn(3)
c-----------------------------------------------------------------------
       common /espl3/iend1 , iendn , g1    , gn    , 
     &               a1    , b1    , an    , bn    ,
     &               p1    , q1    , pn    , qn    ,
     &               a1ry  , b1ry  , anry  , bnry                                                     
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  check internal dimensions are compatible
c-----------------------------------------------------------------------

       if(nstdim.lt.istdim) then
           write(i4unit(-1),2002)' ','nstdim=',nstdim,
     &                           ' < istdim=',istdim
	   write(i4unit(-1),2003)
	   stop
       endif
       
       x=0.0                                                            
       x3=0.0                                                           
       h(n)=1.0d72
                                                             
       do i=1,n                                                      
         do j=1,n                                                      
           w(i,j)=0.0
         enddo                                                       
         y=1.0/h(i)                                                       
         y3=3.0*y*y                                                       
         a(i)=x                                                           
         b(i)=2.0*(x+y)                                                   
         c(i)=y                                                           
         if(i.eq.1)go to 13                                               
         w(i,i-1)=-x3                                                     
   13    w(i,i)=x3-y3                                                     
         if(i.eq.n)go to 14                                               
         w(i,i+1)=y3                                                      
   14    x=y                                                              
         x3=y3
       enddo
                                                                   
       if(iend1.eq.1) then                                        
           c(1)=0.0d0                                                       
           w(1,1)=b(1)*g1                                                   
           w(1,2)=0.0d0                                                     
       elseif(iend1.eq.3) then                                                         
           c(1)=b(1)                                                        
           w(1,1)=-b(1)*b(1)                                                
           w(1,2)=-w(1,1)                                                   
       elseif(iend1.eq.4) then                                                         
           dt1=1.0d0/(p1(1)*q1(2)-p1(2)*q1(1))                              
           t1=0.5d0*dt1*(p1(1)*q1(3)-p1(3)*q1(1))                           
           t2=0.5d0*dt1*(p1(3)*q1(2)-p1(2)*q1(3))                           
           b(1)=b(1)+t1                                                     
           w(1,1)=w(1,1)-t2
       endif
                                                      
       if(iendn.eq.1) then                                        
           a(n)=0.0d0                                                       
           w(n,n)=b(n)*gn                                                   
           w(n,n-1)=0.0d0                                                   
       elseif(iendn.eq.3) then                                                         
           a(n)=b(n)                                                        
           w(n,n)=b(n)*b(n)                                                 
           w(n,n-1)=-w(n,n)                                                 
       elseif(iendn.eq.4) then                                                         
           dtn=1.0d0/(pn(1)*qn(2)-pn(2)*qn(1))                              
           t1=0.5d0*dtn*(pn(1)*qn(3)-pn(3)*qn(1))                           
           t2=0.5d0*dtn*(pn(3)*qn(2)-pn(2)*qn(3))                           
           b(n)=b(n)-t1                                                     
           w(n,n)=w(n,n)+t2 
       endif                                                
       h(n)=0.0 
                                                           
       do i=2,n                                                      
         x=a(i)/b(i-1)                                                    
         b(i)=b(i)-x*c(i-1)                                               
         do j=1,i                                                      
           w(i,j)=w(i,j)-x*w(i-1,j)
         enddo                                         
       enddo
                                                            
       x=1.0/b(n)
                                                              
       do i=1,n                                                      
         w(n,i)=x*w(n,i)
       enddo
                                                         
       do i=2,n                                                      
         k=n-i+1                                                          
         x=1.0/b(k)                                                       
         do j=1,n                                                      
           w(k,j)=x*(w(k,j)-c(k)*w(k+1,j))
         enddo                                  
       enddo 
                                                               
       if(iend1.eq.4)then                                           
           do i=1,n                                                      
             a1ry(i)=-dt1*q1(1)*w(1,i)                                        
             b1ry(i)=dt1*p1(1)*w(1,i)
           enddo                                         
           a1ry(1)=a1ry(1)+dt1*q1(2)                                        
           b1ry(1)=b1ry(1)-dt1*p1(2)
       endif
                                               
       if(iendn.eq.4)then
           do i=1,n                                                      
             anry(i)=-dtn*qn(1)*w(n,i)                                        
             bnry(i)=dtn*pn(1)*w(n,i)
           enddo                                         
           anry(n)=anry(n)+dtn*qn(2)                                        
           bnry(n)=bnry(n)-dtn*pn(2)
       endif                                        
       
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4spl3 warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4spl3 error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
                                                                 
       return                                                           
       end                                                              
