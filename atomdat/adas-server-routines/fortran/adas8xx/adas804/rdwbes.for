C UNIX-IDL - SCCS info: Module @(#)rdwbes.for	1.1 Date 03/18/03
C
       subroutine rdwbes( ndinfo,
     &                    z0    , nlqs  , nshell , na    , la    ,
     &                    ea    , qda   , alfaa  ,
     &                    jsn   , jealfa, acc    , xmax  , h     ,
     &                    lam   , einc  , irept  , iext  ,iochk  ,
     &                    res   ,
     &                    ninfo , cinfoa
     &                  )
       implicit none
C-----------------------------------------------------------------------
C
C  **************** fortran77 program: rdwbes.for *********************
C
C  Purpose:  Evaluates Born multipole integrals using distorted bound
C            waves.  The distorted waves are in a Jucys or Slater type
C            potential.
C
C
C  Subroutine:
C
C  Input : (i*4)  ndinfo   = maximum number of information strings
C  Input : (r*8)  z0       = nuclear charge (+ve)
C  Input : (i*4)  nlqs()   = 1000*n+100*l+iqfor each screening shell
C                            1st dim: screening shell index
C  Input : (i*4)  nshell   = number of screening shells
C  Input : (i*4)  na()     = initial (1) and final (2) state principal
C                            quantum numbers.
C  Input : (i*4)  la()     = initial (1) and final (2) orbital quantum
C                            numbers.
C  Input : (r*8)  ea()     = energies(ryd) of initial (1) and final (2)
C                            states - set <0 for bound states.
C  Input : (r*8)  qda()    = quantum defects for initial (1) and
C                            final (2) states.
C  Input : (r*8)  alfaa(,) = screening parameters
C                            1st dim: initial (1) and final (2) states
C                            2nd dim: screening shell index.
C  Input : (i*4)  jsn      = -1 => Jucys potential
C                          =  0 => Slater potential
C  Input : (i*4)  jealfa   =  0 => search for energies given potential
C                          =  1 => search for alfaa parameters for
C                                  potential given energies and quantum
C                                  defects.
C  Input : (r*8)  acc      = search accuracy setting
C  Input : (r*8)  xmax     = range for numerical wave function generation
C                            and storage
C  Input : (r*8)  h        = step interval for numerical wave function
C                            storage
C  Input : (i*4)  lam      = Born multipole
C  Input : (r*8)  einc     = incident electron energy(ryd)
C  Input : (i*4)  irept    = 0 =>full wave function determination
C                          = 1 => repeat with same wave functions as in
C                                 previous case with irept=0
C  Input : (i*4)  iext     = 0 normal operation with internally generated
C                              wave functions
C                          = 1 use external wave functions supplied in
C                              function gext(x,n,l) with n and l
C                              specifying orbital.
C  Input : (i*4)  iochk    = 1 => Born multipole integral evaluated
C                          = 2 => Ochkur multipole integral evaluated
C
C  Output: (r*8)  res      = multipole integral (at.unit)
C  Output: (i*4)  ninfo    = number of information strings
C  Output  (c*90) cinfoa() = information strings
C                            1st dim: index number of strings
C
C
C  Routines:
C          routine    source   brief description
C          -------------------------------------------------------------
C          zeff      adas      effectrive charge (+ve)
C          effz3     adas      evaluates effective potential
C          gext      adas      access external radial wave functions
C          zser      adas      power series expansion of z(r)
C          bdcf7     adas      generate bound radial distorted wave fn.
C          fcf6      adas      generate free radial distorted wave fn.
C          rbesf     adas      evalluate bessel function
C          ass2      adas      asymptotic integral contribution
C          dnamp     adas      asymp. wave fn. amplitude Taylor coeffts
C          phase     adas      asymp. wave fn. phase Taylor coeffts
C          ass       adas      asymptotic integral contribution
C
C  Author:  H. P. Summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   22/04/85
C
C  Update: HP Summers  16/06/95  alter definition of nlqs as
C                                1000*n+100*l+iq to avoid problem when
C                                number of equivalent electrons is 10
C  Update: HP Summers  21/05/04  restructure and add calculation
C                                information strings to parameter output
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       integer   i4unit
       integer   ndinfo
       integer   i         , iext     , iq       , iqs      , irept
       integer   j         , j1       , jalf1    , jalf2    , js
       integer   ls        , n        , ns       , nshell
       integer   icase     , icheck   , iochk    , is       , jb      ,
     &           jbmax     , jealfa   , jmax     , jsn
       integer   l         , lai      , laj      , lam      , m       ,
     &           ni        , nj       , nstep
       integer   ninfo
C-----------------------------------------------------------------------
       real*8    gext      , phase    , rbesf
       real*8    z0        , zz0      , zef      , xmax     , h   ,
     &           q         , x1       , en       , qs       , xmax1
       real*8    t         , ee       , z2       , ak2      , einc,
     &           ak        , ei       , ej       , tkij
       real*8    zeff      , phi1     , qdi      , phi2     , qdj
       real*8    rem       , tdw      , z1       , acc      , ell ,
     &           el        , e        , qd       , x0       , x2
       real*8    zz1       , zz2      , zz3      , h1       , x   ,
     &           x0i       , x1i      , x2i      , x0j      , x1j ,
     &           x2j       , e12      , qborn    , hb       , hb1 ,
     &           cs        , b3       , b2       , b1
       real*8    phii      , phij     , rem2     , f1       , g0  ,
     &           g1        , eli      , elj      , ellj     , elli
       real*8    zed       , f0       , tb       , qb       , tbref
       real*8    res
       real*8    a0i       , a0j      , a10      , a20      ,
     &           ci        , cj       , h2
C-----------------------------------------------------------------------
       character str1*10   , str2*10
C-----------------------------------------------------------------------
       integer   nlqs(10)    , numel(10)  , nc(10)
       integer   na(2)       , la(2)
C-----------------------------------------------------------------------
       real*8    alfa(10)
       real*8    alfaa(2,10) , ea(2)      , qda(2)
       real*8    x0a(2)      , x1a(2)     , x2a(2)
       real*8    gi(1000)    , gj(1000)   , zl(1000)   , zs(100)
       real*8    ampi(20)    , ampj(20)   , amp1(20)   , amp2(20)
       real*8    ta(5)
C-----------------------------------------------------------------------
       character cinfoa(ndinfo)*90
C-----------------------------------------------------------------------

       if(irept.eq.1.or.iext.eq.1)go to 73
C-----------------------------------------------------------------------
C  enter wave function generation
C-----------------------------------------------------------------------
       z1=z0
       zz0=-z0

       if(nshell.gt.0)then
           do i=1,nshell
             j=nlqs(i)
             j1=j/100
             iq=j-100*j1
             q=iq
             z1=z1-q
             n=j/1000
             numel(i)=iq
             nc(i)=n
           enddo
       endif

       jalf1=1
       jalf2=nshell

       do i=1,2
         if(na(i).gt.0)then
             zef=z0
             if(nshell.gt.0)then
                 do  is=1,nshell
                   alfa(is)=alfaa(i,is)
                   js=nlqs(is)
                   ns=js/1000
                   ls=js/100-10*ns
                   iqs=js-100*ls-1000*ns
                   qs=iqs
                   if(na(i).gt.ns)then
                       zef=zef-qs
                   elseif (na(i).eq.ns) then
                       if(la(i).eq.ls) then
                           zef=zef-0.5d0*qs-0.5d0
                       elseif(la(i).gt.ls) then
                           zef=zef-0.5d0*qs
                       endif
                   endif
                 enddo
             endif

             en=na(i)
             t=zef/en

             if(jealfa.le.0)then
                 qda(i)=en-z1/t
             endif

             ee=-t*t
             el=la(i)
             ell=el*(el+1.0d0)
             t=z1*z1+ell*ee

             if(t.le.0.0d0)then
                 x1=-z1/ee
             else
                 x1=(-z1-dsqrt(t))/ee
             endif

             z2=-zeff(jsn,-z0,nshell,nc,numel,alfa,x1)
             t=z2*z2+ell*ee

             if(t.le.0.0d0)then
                 x1=-z2/ee
             else
                 x1=(-z2-dsqrt(t))/ee
             endif
         else
             if(nshell.le.0)then
                 x1=6.28d0/dsqrt(ea(i))
             else
                 en=nc(nshell)
                 x1=-0.5d0*en*dlog(z1*acc/(z0-z1))/z1
             endif
         endif

         n=na(i)
         l=la(i)
         e=ea(i)
         qd=qda(i)
         if(nshell.gt.0)then
             do is=1,nshell
               alfa(is)=alfaa(i,is)
             enddo
             if((n.gt.0).or.(jealfa.gt.0))then
                 call effz3( jealfa, n     , l     , e     , qd    ,
     &                       jsn   , zz0   , nshell, nc    , numel ,
     &                       alfa  , jalf1 , jalf2 ,
     &                       x0    , x1    , x2    , acc   , m
     &                      )
             endif
         endif
C-----------------------------------------------------------------------
C  write wavefunction generator effective potential info to strings
C-----------------------------------------------------------------------
         if(n.gt.0)then
             if(jealfa.le.0)then
                 en=n
                 qd=en-z1/dsqrt(-e)
                 ninfo = ninfo +2
                 cinfoa(ninfo)(1:90)=
     &               '  jsn    n    l         qd              e'//
     &               '        alfa       x0        x1        x2       m'
                 ninfo = ninfo +1
                 cinfoa(ninfo)(1:36)=
     &               '                 estimate calculated'
                 ninfo = ninfo +1
                 write(cinfoa(ninfo)(1:90),'(3i5,7f10.5,i5)')
     &                 jsn,n,l,qda(i),qd,e,alfa(1),x0,x1,x2,m
                 if(nshell.gt.1)then
                     do is = 2, nshell
                       ninfo=ninfo+1
                       write(cinfoa(ninfo)(1:55),'(45x,f10.5)')
     &                       (alfa(is))
                     end do
                 endif
             else
                 ninfo = ninfo +2
                 cinfoa(ninfo)(1:90)=
     &               '  jsn    n    l     qd        e'//
     &               '             alfa            x0        x1'//
     &               '        x2       m'
                 ninfo = ninfo +1
                 cinfoa(ninfo)(1:56)=
     &                '                                     '//
     &                'estimate calculated'
                 ninfo = ninfo +1
                 write(cinfoa(ninfo)(1:90),'(3i5,7f10.5,i5)')
     &                 jsn,n,l,qd,e,alfaa(i,1),alfa(1),x0,x1,x2,m
                 if(nshell.gt.1)then
                     do is = 2, nshell
                       ninfo=ninfo+1
		       write(str1, '(f10.5)')alfaa(i,is)
		       write(str2, '(f10.5)')alfa(is)
                       cinfoa(ninfo)(36:45) = str1
                       cinfoa(ninfo)(46:55) = str2
                     end do
                 endif
             endif
         else
                 ninfo = ninfo +2
                 cinfoa(ninfo)(1:90)=
     &               '  jsn     e        l     qd            alfa'//
     &               '            x0        x1        x2'
                 ninfo = ninfo +1
                 cinfoa(ninfo)(1:51)=
     &                '                                '//
     &                'estimate calculated'
                 ninfo = ninfo +1
             write(cinfoa(ninfo)(1:80),'(i5,f10.5,i5,6f10.5)')
     &             jsn,e,l,qd,alfaa(i,1),alfa(1),x0,x1,x2
             if(nshell.gt.1)then
                 do is = 2, nshell
                   ninfo=ninfo+1
		   write(str1, '(f10.5)')alfaa(i,is)
		   write(str2, '(f10.5)')alfa(is)
                   cinfoa(ninfo)(31:40) = str1
                   cinfoa(ninfo)(41:50) = str2
                 end do
             endif
         endif

         qda(i)=qd
         x0a(i)=x0
         x1a(i)=x1
         x2a(i)=x2

         if(nshell.gt.0)then
             do is=1,nshell
               alfaa(i,is)=alfa(is)
             enddo
         endif

       enddo

       xmax1=dmax1(x1a(1),x1a(2))
       zz1=0.0d0
       zz2=0.0d0
       zz3=0.0d0

       if(xmax.lt.xmax1)then
           write(i4unit(-1),2000)'warning xmax too low'
           write(i4unit(-1),2001)
       endif

   73  jmax=xmax/h+0.5d0
       h1=0.33333333d0*h
       icase=0
       if(irept.eq.1)go to 69
       if(iext.gt.0)then
           x=0.0d0
           do j=1,jmax
             x=x+h
             gi(j)=gext(x,na(1),la(1))
             gj(j)=gext(x,na(2),la(2))
           enddo
           go to 69
       endif
       if(nshell.gt.0)then
           do is=1,nshell
             alfa(is)=alfaa(1,is)
           enddo
       endif
       x0i=x0a(1)
       x1i=x1a(1)
       x2i=x2a(1)
       ni=na(1)
       lai=la(1)
       eli=lai
       ei=ea(1)
       qdi=qda(1)
       call zser( jsn   , zz0   , nshell, nc    , numel ,
     &            alfa  , zs
     &          )
       x=0.0d0
       do j=1,jmax
         x=x+h
         zl(j)=zeff( jsn   , zz0   , nshell, nc    , numel ,
     &               alfa  , x
     &              )
       enddo

       if(ni.gt.0)then
           call bdcf7( ni    , lai   , qdi   , jsn   ,
     &                 zz0   , nshell, nc    , numel , alfa   ,
     &                 zl    , zz1   , zz2   , zz3   , zs     ,
     &                 x0i   , x1i   , x2i   , xmax  , h      ,
     &                 gi    , ci
     &                )
       else
           icase=icase+2
           call fcf6(  gi    , ci    ,
     &                 a0i   , ampi  , phii  , qdi   ,
     &                 x0    , ni    , lai   , ei    , jsn    ,
     &                 zz0   , nshell, nc    , numel , alfa   ,
     &                 zl    , zz1   , zz2   , zz3   , zs     ,
     &                 xmax  , h     , x2i   , h2
     &                 )
       endif

       if(nshell.gt.0)then
           do is=1,nshell
             alfa(is)=alfaa(2,is)
           enddo
       endif

       x0j=x0a(2)
       x1j=x1a(2)
       x2j=x2a(2)
       nj=na(2)
       laj=la(2)
       elj=laj
       ej=ea(2)
       qdj=qda(2)

       call zser( jsn   , zz0   , nshell, nc    , numel ,
     &            alfa  , zs
     &          )
       x=0.0d0
       do j=1,jmax
         x=x+h
         zl(j)=zeff(jsn,zz0,nshell,nc,numel,alfa,x)
       enddo

       if(nj.gt.0)then
           call bdcf7( nj    , laj   , qdj   , jsn   ,
     &                 zz0   , nshell, nc    , numel , alfa   ,
     &                 zl    , zz1   , zz2   , zz3   , zs     ,
     &                 x0j   , x1j   , x2j   , xmax  , h      ,
     &                 gj    , cj
     &               )
       else
           icase=icase+1
           call fcf6(  gj    , cj    ,
     &                 a0j   , ampj  , phij  , qdj   ,
     &                 x0    , nj    , laj   , ej    , jsn    ,
     &                 zz0   , nshell, nc    , numel , alfa   ,
     &                 zl    , zz1   , zz2   , zz3   , zs     ,
     &                 xmax  , h     , x2j   , h2
     &               )
       endif

   69  icheck=1
       e12=ea(2)-ea(1)
       qborn=0.0d0
       nstep=2
       if(iochk.ge.2)nstep=4
       hb=nstep*4
       hb=2.0d0/hb
       hb1=0.04444444d0*hb
       if(icheck.ne.1)go to 251
       cs=1.0d0-hb
       jbmax=1
       go to 258
  251  jbmax=5
       cs=-1.0d0
  255  cs=cs-hb
  258  do jb=1,jbmax
         cs=cs+hb
         ak2=einc+einc-e12-2.0d0*dsqrt(einc*(einc-e12))*cs
         ak=dsqrt(ak2)
         j=0
         x=0.0d0
         tdw=0.0d0
         b3=0.0d0

   83    j=j+2
         x=x+h+h
         b1=b3
         b2=gi(j-1)*gj(j-1)*rbesf(lam,ak,x-h)
         b3=gi(j)*gj(j)*rbesf(lam,ak,x)
         tdw=tdw+b1+4.0d0*b2+b3
         if(j.lt.jmax)go to 83

         tdw=tdw*h1
         if(icase.ge.3)then
             tkij=dabs(dsqrt(ei)-dsqrt(ej))
             zed=-z1
             a10=a0i
             a20=a0j
             phi1=phii
             phi2=phij
             do j=1,10
               amp1(j)=ampi(j)
               amp2(j)=ampj(j)
             enddo
             x=xmax
             rem2=0.0d0
             if((tkij*xmax.lt.10.0d0).or.
     &          ((x2i+x2j-xmax-xmax).gt.1.0d-40))then
                 f0=gi(jmax-1)
                 f1=gi(jmax)
                 g0=gj(jmax-1)
                 g1=gj(jmax)
                 call ass2( xmax  , h     , x     ,
     &                      f0    , f1    , g0    , g1     ,
     &                      ei    , ej    , tkij  , lai    , laj   ,
     &                      zed   , 1     , rem2
     &                     )
                 elli=eli*(eli+1.0d0)
                 ellj=elj*(elj+1.0d0)
                 call dnamp(a10,amp1,ei,elli,zed,x,10,5)
                 call dnamp(a20,amp2,ej,ellj,zed,x,10,5)
                 phi1=phase(ei,eli,zed,x)
                 phi2=phase(ej,elj,zed,x)
             endif
             phi1=phi1+3.14159265d0*qdi
             phi2=phi2+3.14159265d0*qdj
             call ass( a10    , amp1   , a20    , amp2     ,
     &                 phi1   , phi2   , x      , 1        ,
     &                 ei     , ej     , 10     , rem
     &               )
             tdw=tdw+rem+rem2
         endif
         tb=tdw*tdw
         if(iochk.ge.2)tb=tb*ak2*ak2/(einc*einc)
         ta(jb)=tb
       enddo

       if(icheck.gt.1)go to 265
       icheck=icheck+1
       tbref=tb
       go to 251
  265  qb=hb1*(7.0d0*(ta(1)+ta(5))+32.0d0*(ta(2)+ta(4))+12.0d0*ta(3))
       if(dabs(ta(5)/ta(1)).lt.4.0d0.or.dabs(ta(5)).lt.1.0d-3*dabs(
     &tbref))go to 266
       cs=cs-4.0d0*hb
       hb=0.5d0*hb
       hb1=0.5d0*hb1
       go to 255
  266  qborn=qborn+qb
       if(cs.lt.0.99999d0)go to 255
       res=qborn

       return
C-----------------------------------------------------------------------
 2000 format(1x,32('*'),' rdwbes warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' rdwbes error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
C-----------------------------------------------------------------------
      end
