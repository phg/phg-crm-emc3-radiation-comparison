C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4spf1.for,v 1.1 2004/07/06 14:01:03 whitefor Exp $ Date $Date: 2004/07/06 14:01:03 $
C
       subroutine h4spf1( lpend     , igrpout   , ianopt    , 
     &                    igrpscal1 , igrpscal2 , itexout   ,
     &                    cgtit     , cheader   , texdsn    , bcval    ,
     &                    xmin1     , xmax1     , ymin1     , ymax1    ,
     &                    xmin2     , xmax2     , ymin2     , ymax2    
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran 77 subroutine: h4spf1  *******************
c
c  calling program: 
c          adas804.for
c
c  purpose:
c          to communicate with idl output selection interface 
c          h4spf1.pro via the unix pipe.
c
c  input:
c          none
c  output:
c          (l)    lpend     = cancel or done.
c          (i*4)  igrpout   = graph display selection switch
c          (i*4)  ianopt    = analysis option
c                             1 - standard plots
c                             2 - burgess c plots
c          (i*4)  igrpscal1 = default(0) or user scaling(1) switch for  
c                             ist graph.
c          (i*4)  igrpscal2 = default(0) or user scaling(1) switch for  
c                             2nt graph.
c          (i*4)  itexout   = hard copy of text output request switch.
c                             0 - no, 1 - yes
c          (c*40) cgtit     = the graph title (information for any
c                             new archive).
c          (c*80) cheader   = the adas header for the default file.
c          (c*80) texdsn    = the default archive file name.
c          (r*8)  bcval     = Burgess C-parameter value.
c          (r*8)  xmin1     = user selected 1st graph x-axis minimum.
c          (r*8)  xmax1     = user selected 1st graph x-axis maximum.
c          (r*8)  ymin1     = user selected 1st graph y-axis minimum.
c          (r*8)  ymax1     = user selected 1st graph y-axis maximum.
c          (r*8)  xmin2     = user selected 2nd graph x-axis minimum.
c          (r*8)  xmax2     = user selected 2nd graph x-axis maximum.
c          (r*8)  ymin2     = user selected 2nd graph y-axis minimum.
c          (r*8)  ymax2     = user selected 2nd graph y-axis maximum.
c
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          xxflsh     adas      clears pipe buffer
c          xxslen     adas      removes spaces from a string
c
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    18 July 2002
c
c
c  version: 1.1   Hugh Summers  18/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer  igrpout  , ianopt   , ifirst  , ilast 
       integer  igrpscal1, igrpscal2, itexout , pipein  , pipeou
       integer  ipend
       integer  i4unit          
c-----------------------------------------------------------------------
       character cgtit*40 , cheader*80 , texdsn*80 , cgtemp*80
       character ctexdsn*80
c-----------------------------------------------------------------------
       logical lpend
c-----------------------------------------------------------------------
       parameter( pipein = 5 , pipeou = 6 )
c-----------------------------------------------------------------------
       real*8  bcval 
       real*8  xmin1   , xmax1   , ymin1   , ymax1
       real*8  xmin2   , xmax2   , ymin2   , ymax2
c-----------------------------------------------------------------------
       read(pipein,1000)ipend
       if (ipend.eq.1)then
          lpend = .true.
       else
          lpend = .false.
       endif
c
       if (ipend.eq.0) then
           read(pipein,1000)ianopt
           read(pipein,1001)bcval
c
         read(pipein,1000)igrpout
         if (igrpout.eq.1)then
          read(pipein,1003)cgtemp
            call xxslen( cgtemp , ifirst , ilast )
               read(cgtemp(ifirst:ifirst+39),1004)cgtit
           read(pipein,1000)igrpscal1
             if (igrpscal1.eq.1)then
               read(pipein,1001)xmin1
               read(pipein,1001)xmax1
               read(pipein,1001)ymin1
               read(pipein,1001)ymax1
             endif
c
           read(pipein,1000)igrpscal2
             if (igrpscal2.eq.1)then
               read(pipein,1001)xmin2
               read(pipein,1001)xmax2
               read(pipein,1001)ymin2
               read(pipein,1001)ymax2
             endif
         endif
c-----------------------------------------------------------------------
         read(pipein,1000)itexout
           if (itexout.eq.1)then
             read(pipein,1003)ctexdsn
               call xxslen( ctexdsn , ifirst , ilast )
               read(ctexdsn(ifirst:ilast),'(a)') texdsn
c              texdsn = ctexdsn
           endif
       endif
c
       if (ipend.eq.0.and.itexout.eq.1)then
         read(pipein,1003)cheader
       endif
c-----------------------------------------------------------------------
 1000  format(1x,i3)
 1001  format(1x,e8.2)
 1002  format(1x,1a40)
 1003  format(1x,1a80)
 1004  format(1a45)
c-----------------------------------------------------------------------
       return
       end
