C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/h4fasy.for,v 1.1 2004/07/06 14:00:12 whitefor Exp $ Date $Date: 2004/07/06 14:00:12 $
C
      subroutine h4fasy( istdim ,
     &                   x      , xa    , n    , ya    , y     , dy    ,
     &                   c1     , c2    , c3   , c4    ,
     &                   form   , iforms
     &                 )
      implicit none
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4spl3.for *******************
c
c  purpose: provide a spline interpolate making use of specified
c           asymptotic behaviour
c
c  calling program: various
c
c  notes: (1) uses labelled common  /espl3/
c
c  input : (i*4)  istdim  = dimensionality for splining arrays
c
c  input : (r*8)  x       = required x-value
c  input : (i*4)  x(i)    = knots
c  input : (r*8)  n       = number of knots
c  
c  input : (r*8)  c1(i,j) = 1st spline coefficient precursor
c  input : (r*8)  c2(i,j) = 2nd spline coefficient precursor
c  input : (r*8)  c3(i,j) = 3rd spline coefficient precursor
c  input : (r*8)  c4(i,j) = 4th spline coefficient precursor
c  input : (r*8)  form    = external function specifying asymptotic forms
c  input : (i*4)  iforms  = index of required form
c
c  output: (r*8)  y=returned y-value
c  output: (r*8)  dy=returned derivative
c
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          h4fspl     adas      
c          i4unit     adas      fetch unit number for output of messages
c
c  author:  Hugh P. Summers, University of Strathclyde
c           JA7.08
c           Tel.: +44 (0)141-548-4196
c
c  date:    24 July 2002
c
c
c  version: 1.1   Hugh Summers  24/07/02
c  modified:      first release
c
c-----------------------------------------------------------------------
       integer  nstdim
c-----------------------------------------------------------------------
       parameter ( nstdim = 40)
c-----------------------------------------------------------------------
       integer  istdim  , i4unit  , n       , iforms
       integer  iends   , in      , i       , j      , k
       integer  i0      , isw
c-----------------------------------------------------------------------
       real*8   form
       real*8   x       , dx      , y       , dy      , ddy
       real*8   a       , b       , xx      , h
c-----------------------------------------------------------------------
       integer  iend(2)
c-----------------------------------------------------------------------
       real*8   xa(istdim)           , ya(istdim)          ,
     &          c1(istdim,istdim-1)  , c2(istdim,istdim-1) ,
     &          c3(istdim,istdim-1)  , c4(istdim,istdim-1)
       real*8   g(2)    , ab(4)   , pq(12)     , 
     &          abry(4*nstdim)
c-----------------------------------------------------------------------
       common /espl3/iend  , g    ,     
     &               ab    , 
     &               pq    ,
     &               abry           
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  check internal dimensions are compatible
c-----------------------------------------------------------------------

       if(nstdim.lt.istdim) then
           write(i4unit(-1),2002)' ','nstdim=',nstdim,
     &                           ' < istdim=',istdim
	   write(i4unit(-1),2003)
	   stop
       endif
       
       dy=0.0d0
                                                                
       if(x.lt.xa(1))then                                            
           iends=1                                                          
           in=1                                                             
       elseif(x.gt.xa(n)) then             
           iends=2                                                          
           in=n                                                             
       else                                                         
           i0  = 0
           isw = 0                                                        
           call h4ftsp( istdim  ,
     &              x       , xa      , n      , 
     &              ya      , y       , dy     , i0      , 
     &              c1      , c2      , c3     , c4      , isw
     &            )                      
           return
       endif 
                                                                 
       k=iend(iends)
                                                           
       if(k.eq.1) then                                             
           dy=g(iends)*ya(in)                                               
           y=ya(in)+(x-xa(in))*dy                                           
           return 
       elseif(k.eq.2.or.k.eq.3) then                     
           xx=xa(in)
           i0  = 0
           isw = 0                                                        
           call h4ftsp( istdim  ,
     &                  xx      , xa     , n       ,
     &                  ya      , y      , dy      , i0      ,
     &                  c1      , c2     , c3      , c4      , isw
     &            )
                          
           if(iend(iends).eq.2) then                                     
               y=ya(in)+(x-xa(in))*dy           
               return
	   endif
	    
           if(iends.ne.2) then        
               h=xa(2)-xa(1)          
               ddy=2.0d0*(-ya(1)+ya(2)-h*dy)/(h*h)        
               dx=x-xa(in)      
               y=ya(in)+dx*dy+0.5d0*dx*dx*ddy
	       return      
           else                                                         
               h=xa(n)-xa(n-1)       
               ddy=2.0d0*(ya(n-1)-ya(n)+h*dy)/(h*h)
               dx=x-xa(in)      
               y=ya(in)+dx*dy+0.5d0*dx*dx*ddy
	       return      
	   endif                             
       elseif(k.eq.4) then               
           a=0.0d0           
           b=0.0d0
           do i=1,n                                                      
             a=a+abry(2*istdim*iends-2*istdim+i)*ya(i)
             b=b+abry(2*istdim*iends-istdim+i)*ya(i)
           enddo
           j=4*iforms+2*iends-5  
           y=a*form(j,x)+b*form(j+1,x)
	   return
       endif
       
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4fasy warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4fasy error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
                                        
      end                                                               
