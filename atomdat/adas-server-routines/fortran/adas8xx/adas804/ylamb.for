      SUBROUTINE YLAMB(LAM,M,C0,C,A,B,Y0,Y1,H,XMAX)
      IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C PURPOSE: CALCULATES Y(X1)=  INTEGRAL,FROM 0 TO X1,
C                             OF (F(X2)*X2**LAM/X1**(LAM+1))DX2
C                             +INTEGRAL,FROM X1 TO INFINITY,
C                             OF (F(X2)*X1**LAM/X2**(LAM+1))DX2.
C INPUT  REQUIRED IS
C        LAM,H,XMAX,
C        A(N)=F(X2=N*H),FOR N=1,2...,NMAX,WITH NMAX*H=XMAX,
C        C0 AND M,WHERE F(X)=C0*X**M IN THE LIMIT OF SMALL X.
C IT IS ASSUMED THAT F(X) IS SMALL AND DECREASING IN MAGNITUDE
C FOR X GREATER THAN XMAX.
C OUTPUT IS
C        B(N)=Y(X1=N*H),FOR N=1,2...NMAX,
C        C0 AND C(J),WHERE Y(X)=C0+C(1)*X+...+C(10)*X**10,
C        Y0=INTEGRAL,FROM 0 TO INFINITY,OF (F(X2)/X2**(LAM+1))DX2,
C        Y1=INTEGRAL,FROM 0 TO INFINITY,OF (F(X2)X*LAM)DX2.
C
C-----------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 08-04-2010
C  MODIFIED : Martin O'Mullane
C              - Remove listing information from colums 72+.
C-----------------------------------------------------------------------
      DIMENSION A(400),B(400),C(10),D(6)
      NMAX=XMAX/H+0.1D0
      X=0.0
      DO 2 I=1,5
      X=X+H
2     C(I)=A(I)/X**M-C0
      D(1)=C0
      Z=120.0*H
      D(2)=(600.0*C(1)-600.0*C(2)+400.0*C(3)-150.0*C(4)+24.0*C(5))/Z
      Z=H*Z
      D(3)=(-770.0*C(1)+1070.0*C(2)-780.0*C(3)+305.0*C(4)-50.0*C(5))/Z
      Z=H*Z
      D(4)=(355.0*C(1)-590.0*C(2)+490.0*C(3)-205.0*C(4)+35.0*C(5))/Z
      Z=H*Z
      D(5)=(-70.0*C(1)+130.0*C(2)-120.0*C(3)+55.0*C(4)-10.0*C(5))/Z
      Z=H*Z
      D(6)=(5.0*C(1)-10.0*C(2)+10.0*C(3)-5.0*C(4)+C(5))/Z
      X=0.0
      DO 4 N=1,4
      X=X+H
      T=0.0
      DO 3 I1=1,6
      I2=7-I1
      Z=LAM+M+I2
3     T=D(I2)/Z+T*X
      C(N)=A(N+1)*(X+H)**LAM
4     B(N)=T*X**M
      J=LAM+1
      T1=B(3)*(X-H)**J
      T2=B(4)*X**J
      J=-J
      H1=H/90.0
      H2=H/3.0
      N=4
5     N=N+1
      X=X+H
      IF(N-NMAX)7,6,9
6     T=T1+H2*(C(2)+4.0*C(3)+C(4))
      GO TO 8
7     C(5)=A(N+1)*(X+H)**LAM
      T=T1+H1*(34.0*(C(2)+C(4))+114.0*C(3)-(C(1)+C(5)))
      T1=T2
      T2=T
8     B(N)=T*X**J
      C(1)=C(2)
      C(2)=C(3)
      C(3)=C(4)
      C(4)=C(5)
      GO TO 5
9     Y1=(T+T2)/2.0D0
      T1=C(2)-C(3)
      IF(DABS(T1)-1.0D-60)31,30,30
30    Y1=Y1+H*C(3)*C(3)/T1
31    DO 10 I=1,4
      N=N-1
      X=X-H
10    C(I)=A(N)*X**J
      T1=C(2)-C(1)
      T=0.0D0
      IF(DABS(T1)-1.0D-60)33,32,32
32    T=H*C(1)*C(1)/T1
33    T1=T+(9.0*C(1)+19.0*C(2)-5.0*C(3)+C(4))*H/24.0
      T2=T+H2*(C(1)+4.0*C(2)+C(3))
      B(NMAX)=B(NMAX)+T*XMAX**LAM
      B(N+2)=B(N+2)+T1*(XMAX-H)**LAM
      B(N+1)=B(N+1)+T2*(X+H)**LAM
11    C(5)=A(N-1)*(X-H)**J
      T=T1+H1*(34.0*(C(2)+C(4))+114.0*C(3)-(C(1)+C(5)))
      T1=T2
      T2=T
      B(N)=B(N)+T*X**LAM
      C(1)=C(2)
      C(2)=C(3)
      C(3)=C(4)
      C(4)=C(5)
      N=N-1
      X=X-H
      IF(N-3)12,12,11
12    C(5)=A(N-1)*(X-H)**J
      T3=T1+H1*(34.0*(C(2)+C(4))+114.0*C(3)-(C(1)+C(5)))
      X=0.0
      DO 14 N=1,4
      X=X+H
      T=0.0
      DO 13 I1=1,6
      I2=7-I1
      Z=M+I2-LAM-1
13    T=D(I2)/Z+T*X
14    C(N)=T*X**(M-LAM)
      B(3)=B(3)+(T2+C(4)-C(3))*(3.0*H)**LAM
      B(2)=B(2)+(T2+C(4)-C(2))*(H+H)**LAM
      B(1)=B(1)+(T2+C(4)-C(1))*H**LAM
      Y0=(T3+C(3)+T2+C(4))/2.0
      T=-2*LAM-1
      T1=M-LAM-1
      T2=M+LAM
      DO 15 I=1,6
      T1=T1+1.0
      T2=T2+1.0
      D(I)=T*D(I)/(T1*T2)
15    CONTINUE
      C0=0.0
      DO 19 I=1,10
19    C(I)=0.0
      IF(LAM)20,20,21
20    C0=Y0
      GO TO 22
21    C(LAM)=Y0
22    DO 24 I=1,6
      J=M+I-1
      IF(J-10)23,23,24
23    C(J)=D(I)
24    CONTINUE
      RETURN
      END
