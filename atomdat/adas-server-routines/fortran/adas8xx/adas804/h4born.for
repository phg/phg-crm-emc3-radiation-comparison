C UNIX-IDL - SCCS info: Module @(#)h4born.for	1.1 Date 03/18/03
C
       subroutine h4born( ixdim  , itdim   , isdim  , ndinfo ,
     &                    cameth ,
     &                    z0     , z       , zeff   ,
     &                    n1     , l1      , eb1    ,
     &                    n2     , l2      , eb2    ,
     &                    isp    , lp      , xjp    ,
     &                    ist1   , lt1     , xj1    , xjt1   ,
     &                    ist2   , lt2     , xj2    , xjt2   ,
     &                    neqv1  , fpc1    , neqv2  , fpc2   ,
     &                    aval   ,
     &                    xmax   , iext    , ijucys , jealfa ,
     &                    nshell , nc      , lc     , numel  , alfaa  ,
     &                    nx     , xa      , omga   ,
     &                    nt     , tea     , upsa   ,
     &                    cresol , ixtyp   , s12    , e12    ,
     &                    ninfo  , cinfoa
     &                  )
       implicit none
C-----------------------------------------------------------------------
C
C  **************** fortran77 program: h4born.for *********************
C
C  Purpose:  Calculation of Born cross-sections using numerical wave
C            functions.
C
C
C  Subroutine:
C
C  input : (i*4)  ixdim    = maximum dimension for X  array
C  input : (i*4)  itdim    = maximum dimension for Te array
C  input : (i*4)  isdim    = maximum dimension for shell vectors
C  input : (i*4)  ndinfo   = maximum number of information strings
C
C  input : (c*1)  cameth   = the tag distinguishing the type of
C                            analysis: a - Born, b- IP
C  input : (r*8)  z0       = nuclear charge
C  input : (r*8)  z        = ion charge
C  input : (r*8)  zeff     = ion charge +1
C  input : (i*4)  n1       = lower n-shell of transition
C  input : (i*4)  l1       = lower l-shell of transition
C  input : (r*8)  eb1      = binding energy (Ryd) in lower level
C  input : (i*4)  n2       = upper n-shell of transition
C  input : (i*4)  l2       = upper l-shell of transition
C  input : (r*8)  eb2      = binding energy (Ryd) in upper level
C  input : (i*4)  isp      = 2*Sp+1 for parent
C  input : (i*4)  lp       = Lp for parent
C  input : (r*8)  xjp      = Jp for parent (if 'ic' coupling)
C  input : (i*4)  ist1     = 2*S+1 for lower state
C  input : (i*4)  lt1      = L for lower state
C  input : (r*8)  xj1      = j for lower state
C  input : (r*8)  xjt1     = J for lower state
C  input : (i*4)  ist2     = 2*S'+1 for upper state
C  input : (i*4)  lt2      = L' for upper state
C  input : (r*8)  xj2      = j' for lower state
C  input : (r*8)  xjt2     = J' for upper state
C  input : (i*4)  neqv1    = no. of equiv. electrons for lower shell.
C  input : (r*8)  fpc1     = fract. parentage for lower state
C  input : (i*4)  neqv2    = no. of equiv. electrons for upper shell.
C  input : (r*8)  fpc2     = fract. parentage for upper state
C  input : (i*4)  aval     = A-value (sec-1) if dipole; else -ve
C  input : (i*4)  xmax     = range of numerical wave functions
C  input : (i*4)  iext     = 0 => calculate radial wave functions
C                          = 1 => read in radial wave functions
C  input : (i*4)  ijucys   = 0  => Slater potential form adopted
C                          = 1 => Jucys potential form adopted
C  input : (i*4)  jealfa   = 0 => fcf6 search for energy eigenvalue
C                          = 1 => fcf6 search for scaling parameters
C  input : (i*4)  nshell   = number of screening shells
C  input : (i*4)  nc()     =  n for each screening shell
C                              1st dim: screening shell index
C  input : (i*4)  lc()     = l for each screening shell
C                              1st dim: screening shell index
C  input : (i*4)  numel()  = iq for each screening shell
C                              1st dim: screening shell index
C  input : (r*8)  alfaa    = scaling factor for each screening shell
C                              1st dim: index for lower & upper state
C                              2nd dim: index over screening shells
C  input : (i*4)  nx       = number of incident electron energies
C  input : (r*8)  xa()     = threshold parameter values
C  input : (i*4)  nt       = number of electron temperatures
C  input : (r*8)  tea()    = electron temperatures (K)
C  input : (c*2)  cresol   = resolution level 'ls' or 'ic'

C  output: (r*8)  omga()   = omegas at input values of x-parameter
C  output: (r*8)  upsa()   = upsilon at onput values of Te
C  output: (i*4)  ixtyp    = transition type 1=dipole;2=non-dipole;
C                                            3=spin-change
C  output: (r*8)  s12      = line strength (dipole) otherwise zero
C  output: (r*8)  e12      = level energy difference (Ryd)
C  output: (i*4)  ninfo    = number of information strings
C  output  (c*90) cinfoa() = information strings
C                            1st dim: index number of strings
C
C        : (i*4)  icount    = general counter index
C        : (i*4)  iq        = general num. of equiv. electrons
C        : (i*4)  irept     = 0 => full wave function determination
C                           = 1 => use wave functions from previous case
C        : (i*4)  iswit     = 1 => no spin change
C                           = 2 => spin change
C        : (i*4)  j         = general integer variable
C        : (i*4)  j1        = general integer variable
C        : (i*4)  jalf1     = lower range of scaling parameters
C        : (i*4)  jalf2     = upper range of scaling parameters
C        : (i*4)  jh        = radial wave function tabulation size
C        : (i*4)  jsn       = -1 => Jucys potential form adopted
C                           = 0  => Slater potential form adopted
C        : (i*4)  lam       = multipole number
C        : (i*4)  n         = general principal quanum number
C        : (i*4)  nlam      = number of multipoles in anga vector
C        : (i*4)  nq        = number of equivalent electrons in initial
C                             state
C        : (r*8)  acc       = precision for eigenvalue search
C        : (r*8)  de        = transition energy (ryd)
C        : (r*8)  fpc       = fractional parentage coefficient
C        : (r*8)  ei        = incid. electron energy (ryd)
C        : (r*8)  eiu       = adj. incid. engy. for Cowan threshold (ryd)
C        : (r*8)  h         = step length for radial wave fn. tabulation
C        : (r*8)  omegb     = Born cross-section
C        : (r*8)  q         = number of equivalent electrons in initial
C                             state
C        : (r*8)  res       = general result variable
C        : (r*8)  t         = general real variable
C        : (r*8)  xlp       = Lp parent orbital angular momentum
C        : (r*8)  xl1       = l initial valence electron orbital
C        : (r*8)  xlt1      = L  total initial orbital angular momentum
C        : (r*8)  xl2       = l' final valence electron orbital
C        : (r*8)  xlt2      = L'  total initial final angular momentum
C        : (r*8)  xn1       = principal quantum number of initial shell
C        : (r*8)  xn2       = principal quantum number of final shell
C        : (r*8)  xnq       = number of equivalent electrons in initial
C        : (r*8)  xsp       = 2*Sp+1 parent spin angular momentum
C        : (r*8)  xst1      = 2*S+1  total initial spin anular momentum
C        : (r*8)  xst2      = 2*S'+1  total final spin anular momentum
C                             state
C        : (r*8)  z0        = nuclear charge
C        : (r*8)  z1        = ion charge + 1
C        : (r*8)  z         = ion charge
C        : (r*8)  zeff      = effective ion charge
C        : (r*8)  zz0       = -z0
C
C        : (i*4)  nlqs()    = 1000*n+100*l+iq for each screening shell
C                              1st dim: screening shell index
C        : (i*4)  na()      = princ. qu. no. for val. elec.(initial & final)
C        : (i*4)  la()      = l qu. no. for val. elec.(initial & final)
C        : (i*4)  lama()    = multipole value
C
C        : (r*8)  alfa()    = scaling factor for each screening shell
C                              1st dim: index over screening shells
C        : (r*8)  anga()    = angular factor for each multipole
C                              1st dim: index over included multipoles
C        : (r*8)  ea()      = energy (ryd) for active electron.  NB -ve for
C                             a bound state
C                              1st dim: index for initial & final state
C        : (r*8)  omega()   = collision strength for each multipole
C                              1st dim: index over included multipoles
C        : (r*8)  qda()     = quantum defect for valence electron.
C                              1st dim: index for initial & final state
C        : (r*8)  x0a()     = inner turning pt. for val. elec. wave fn.
C                              1st dim: index for initial & final state
C        : (r*8)  x1a()     = outer turning pt. for val. elec. wave fn.
C                              1st dim: index for initial & final state
C        : (r*8)  x2a()     = range for active elec. wave fn.
C                              1st dim: index for initial & final state
C
C
C  Routines:
C          routine    source    brief description
C          -------------------------------------------------------------
C          gamaf      adas      tabulates factorials
C          h4angf     adas      evaluates the Born angular parts
C          rdwbes     adas      evaluates the Born radial parts
C          i4unit     adas      fetch unit number for output of messages
C
C  Author:  H. P. Summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   25/02/03
C
C  Update: HP Summers  21/05/04  Restructure and add calculation
C                                information strings to parameter output.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       integer   jh
C-----------------------------------------------------------------------
       real*8    acc
C-----------------------------------------------------------------------
       parameter ( jh = 512   , acc = 0.01d0 )
C       parameter ( jh = 1000   , acc = 0.01d0 )
C-----------------------------------------------------------------------
       integer   i4unit    , ixdim     , itdim    , isdim    , ndinfo
       integer   nx        , nt
       integer   n1        , l1        , n2        , l2
       integer   irept     , iext      , ijucys
       integer   i         , j         , j1        , iq      , n
       integer   nshell    , jalf1     , jalf2     , jsn     , jealfa
       integer   isp       , lp
       integer   ist1      , lt1       , neqv1
       integer   ist2      , lt2       , neqv2
       integer   nq        , lam       , nlam      , iswit
       integer   icount    , nx_stan   , ixtyp
       integer   ninfo
C-----------------------------------------------------------------------
       real*8    z0        , z1        , z         , zeff
       real*8    eb1       , eb2       , e1        , e2       , e12
       real*8    xn1       , xl1       , xn2       , xl2
       real*8    xmax      , zz0       , q
       real*8    xsp       , xlp       , xjp       , fpc1
       real*8    xst1      , xlt1      , xj1       , xjt1
       real*8    xst2      , xlt2      , xj2       , xjt2      , fpc2
       real*8    xnq       , fpc       , t
       real*8    h         , de        , ei        , eiu
       real*8    omegb     , res       , aval      , s12
       real*8    w1        , w2
C-----------------------------------------------------------------------
       character cameth*1  , cresol*2
       character dsnin*80
C-----------------------------------------------------------------------
       logical   lxa_stan
C-----------------------------------------------------------------------
       integer   nlqs(isdim)  , numel(isdim)  , nc(isdim)  , lc(isdim)
       integer   lama(20)
       integer   na(2)        , la(2)
C-----------------------------------------------------------------------
       real*8    xa(ixdim)    , omga(ixdim)
       real*8    tea(itdim)   , upsa(itdim)
       real*8    alfa(isdim)  , alfaa(2,isdim)
       real*8    anga(20)     , omega(20)
       real*8    ea(2)        , qda(2)
       real*8    x0a(2)       , x1a(2)        , x2a(2)
       real*8    xa_stan(31)  , omga_stan(31)
C-----------------------------------------------------------------------
       character cinfoa(ndinfo)*90
C-----------------------------------------------------------------------
       data  nx_stan/31/
       data  xa_stan/1.00100,1.00200,1.00300,1.00500,1.00700,
     &               1.01000,1.02000,1.03000,1.05000,1.07000,
     &               1.10000,1.20000,1.30000,1.50000,1.70000,
     &               2.00000,2.50000,3.00000,5.00000,7.00000,
     &               1.0e+01,1.5e+01,2.0e+01,5.0e+01,7.0e+01,
     &               1.0e+02,1.5e+02,2.0e+02,5.0e+02,7.0e+02,
     &               1.0e+03/
C-----------------------------------------------------------------------
C  check if standard set of x-parameters on input
C-----------------------------------------------------------------------
      lxa_stan=.false.
      if(nx.eq.nx_stan)then
             do i=1,nx
                if(xa(i).ne.xa_stan(i)) goto 10
             enddo
             lxa_stan = .true.
      endif
C-----------------------------------------------------------------------
C  set step length for wave functions
C-----------------------------------------------------------------------

   10  h=jh
       h=xmax/h

C-----------------------------------------------------------------------
C  exit if attempt to use external wave functions
C-----------------------------------------------------------------------

       if(iext.eq.1) then

           write(i4unit(-1),2000)'No implementation of ',
     &                           'external wave functions'
           write(i4unit(-1),2001)
           return
       endif

C-----------------------------------------------------------------------
C  evaluate angular parts
C-----------------------------------------------------------------------

C--------------------------------------------------------------
C  set statistical weight of initial and final states (w1 & w2)
C--------------------------------------------------------------

          if(xjt1.ge.0.0d0) then
              w1 = 2.0d0*xjt1+1.0d0
          elseif((xjt1.lt.0.0d0).and.(xjp.lt.0.0d0)) then
              w1 = dfloat(ist1)*(2.0d0*dfloat(lt1)+1.0d0)
          elseif((xjt1.lt.0.0d0).and.(xjp.ge.0.0d0)) then
              w1 = (2.0d0*xjp+1.0d0)*(2.0d0*xj1+1.0d0)
          endif

           if(xjt2.ge.0.0d0) then
              w2 = 2.0d0*xjt2+1.0d0
          elseif((xjt2.lt.0.0d0).and.(xjp.lt.0.0d0)) then
              w2 = dfloat(ist2)*(2.0d0*dfloat(lt2)+1.0d0)
          elseif((xjt2.lt.0.0d0).and.(xjp.ge.0.0d0)) then
              w2 = (2.0d0*xjp+1.0d0)*(2.0d0*xj2+1.0d0)
          endif


C---------------------------------------------------
C  set table of factorials in labelled common /fac/
C---------------------------------------------------

       call gamaf(200)
       xsp=isp
       xlp=lp

       xl1   = l1
       xst1=ist1
       xlt1=lt1

       xl2   = l2
       xst2=ist2
       xlt2=lt2

       nq  = neqv1
       fpc = fpc1
       xnq = nq


       call h4angf( xsp    , xlp    ,
     &              xl1    , xst1   , xlt1  ,
     &              xl2    , xst2   , xlt2  ,
     &              xnq    , fpc    ,
     &              xjp    , xj1    , xjt1  , xj2   , xjt2  ,
     &              anga   , lama   , nlam
     &             )

C-----------------------------------------------------------------------
C  note transition type - dipole or non-dipole
C-----------------------------------------------------------------------
       if(nlam.eq.0)then
           write(i4unit(-1),2000)'No allowed Born multipoles'
           write(i4unit(-1),2001)
           return
       else
           if(lama(1).eq.1) then
               ixtyp = 1
           else
               ixtyp = 2
           endif
       endif


C-----------------------------------------------------------------------
C  evaluate radial part
C-----------------------------------------------------------------------

       do i = 1, nshell
         nlqs(i)    = 1000*nc(i)+100*lc(i)+numel(i)
       enddo

       z1    = z+1.0d0
       xn1   = n1
       na(1) = n1
       la(1) = l1
       ea(1) = -eb1
       qda(1)= xn1-z1/dsqrt(eb1)

       xn2   = n2
       na(2) = n2
       la(2) = l2
       ea(2) = -eb2
       qda(2)= xn2-z1/dsqrt(eb2)


       de=eb1-eb2

       z1=z0
       zz0=-z0
       jalf1=1
       jalf2=nshell


C---------------------------------------------------
C  set switch for Ochkur if spin change transitions
C---------------------------------------------------
       iswit=1
       if(ist1.ne.ist2)then
           iswit = 2
           ixtyp = 3
       endif

C---------------------------------------------------
C  set Slater or Jucys as required by rdwbes
C---------------------------------------------------

       if(ijucys.eq.0) then
           jsn = 0
       else
           jsn = -1
       endif

       irept=0

C-----------------------------------------------------------------------
C  evaluate omegas for input set of x-parameters
C-----------------------------------------------------------------------

       do icount = 1,nx
         omga(icount)= 0.0d0
         ei=xa(icount)*de
C---------------------------------------------------
C  use cowan finite threshold method if positive ion
C---------------------------------------------------
         eiu=ei
         if(z.gt.0.0d0)then
             eiu=ei+3.0d0*de*de/(ei+de)
         endif

         t=8.0d0*dsqrt(eiu*(eiu-eb1+eb2))*w1
         if(iswit.eq.2)t=t/(eiu*eiu)

         do j=1,nlam
           lam=lama(j)
           call rdwbes( ndinfo,
     &                  z0    , nlqs  , nshell , na    , la    ,
     &                  ea    , qda   , alfaa  ,
     &                  jsn   , jealfa, acc    , xmax  , h     ,
     &                  lam   , eiu   , irept  , iext  , iswit ,
     &                  res   ,
     &                  ninfo , cinfoa
     &                 )
           irept=1
           omega(j)=t*anga(j)*res
           omga(icount)=omga(icount)+omega(j)
         enddo

       enddo

C-----------------------------------------------------------------------
C  evaluate omegas for standard set of x-parameters if necessary
C-----------------------------------------------------------------------

       if(.not.lxa_stan) then

           do icount = 1,nx_stan
             omga_stan(icount)= 0.0d0
             ei=xa_stan(icount)*de
             eiu=ei
             if(z.gt.0.0d0)then
                 eiu=ei+3.0d0*de*de/(ei+de)
             endif

             t=8.0d0*dsqrt(eiu*(eiu-eb1+eb2))*w1
             if(iswit.eq.2)t=t/(eiu*eiu)

             do j=1,nlam
               lam=lama(j)
               call rdwbes( ndinfo,
     &                      z0    , nlqs  , nshell , na    , la    ,
     &                      ea    , qda   , alfaa  ,
     &                      jsn   , jealfa, acc    , xmax  , h     ,
     &                      lam   , eiu   , irept  , iext  , iswit ,
     &                      res   ,
     &                      ninfo , cinfoa
     &                     )
               irept=1
               omega(j)=t*anga(j)*res
               omga_stan(icount)=omga_stan(icount)+omega(j)
             enddo

           enddo

       else

           do i=1,nx_stan
               omga_stan(i)=omga(i)
           enddo

       endif

C-----------------------------------------------------------------------
C  evaluate upsilons using standard set of omegas according to ixtyp
C-----------------------------------------------------------------------

      if(nt.gt.0) then

          if(eb1.gt.eb2) then
              e1  = 0.0d0
              e2  = eb1-eb2
              e12 = e2
          else
              write(i4unit(-1),2002)'lower level b.e.',
     &                              ' > upper level b.e.'
              write(i4unit(-1),2003)
          endif

          call h4mxwl( z0       , z         , zeff       ,
     &                 ixtyp    , aval      , s12        ,
     &                 w1       , w2        , e1         , e2      ,
     &                 nx_stan  , xa_stan   , omga_stan  ,
     &                 nt       , tea       , upsa
     &                )

       endif

      return

C-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4born warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4born error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
C
C-----------------------------------------------------------------------
      end
