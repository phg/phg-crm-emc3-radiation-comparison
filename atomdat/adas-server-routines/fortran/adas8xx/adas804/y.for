       SUBROUTINE Y(L,M,A,B,Y1,H,X1)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C PURPOSE: Unknown.
C
C-----------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 08-04-2010
C  MODIFIED : Martin O'Mullane
C              - Remove listing information from colums 72+.
C              - Add comment.
C-----------------------------------------------------------------------
       DIMENSION A(1000),B(1000),C(5),D(5),E(5)
       H1=0.044444444*H
       J=M-1
       DO 2 I=1,4
       X=I
       X=X*H
       Z=1.0
       DO 1 I1=1,J
    1  Z=Z*X
    2  C(I)=A(I)/Z
       Z=H
       D(1)=0.083333333*(48.0*C(1)-36.0*C(2)+16.0*C(3)-3.0*C(4))/Z
       Z=H*Z
       D(2)=0.041666667*(-104.*C(1)+114.*C(2)-56.*C(3)+11.*C(4))/Z
       Z=H*Z
       D(3)=0.083333333*(18.*C(1)-24.*C(2)+14.*C(3)-3.*C(4))/Z
       Z=H*Z
       D(4)=0.041666667*(-4.0*C(1)+6.0*C(2)-4.0*C(3)+C(4))/Z
       J=L+1
       X=0.0
       DO 8 N=1,4
       X=X+H
       C(N)=0.0
       DO 3 I1=1,4
       I2=5-I1
       Z=L+M+I2
    3  C(N)=D(I2)/Z+C(N)*X
       Z=1.0
       DO 4 I1=1,M
    4  Z=X*Z
       B(N)=Z*C(N)
       DO 5 I1=1,J
    5  Z=X*Z
       C(N)=Z*C(N)
       E(N)=A(N)
       IF(L)8,8,6
    6  DO 7 I1=1,L
    7  E(N)=X*E(N)
    8  CONTINUE
       N=4
    9  N=N+1
       X=X+H
       Z=1.0
       IF(L)12,12,10
   10  DO 11 I1=1,L
   11  Z=X*Z
   12  E(5)=Z*A(N)
       C(5)=C(1)+H1*(7.*E(1)+32.*E(2)+12.*E(3)+32.*E(4)+7.*E(5))
       B(N)=C(5)/(X*Z)
       DO 13 I1=1,4
       C(I1)=C(I1+1)
   13  E(I1)=E(I1+1)
       IF(X-X1+0.5*H)9,14,14
   14  Y1=C(5)
       DO 15 I1=1,5
       C(I1)=0.0
   15  E(I1)=0.0
   16  Z=1.0
       IF(L)19,19,17
   17  DO 18 I1=1,L
   18  Z=X*Z
   19  E(5)=A(N)/(X*Z)
       C(5)=C(1)+H1*(7.*E(1)+32.*E(2)+12.*E(3)+32.*E(4)+7.*E(5))
       B(N)=B(N)+C(5)*Z
       IF(L)22,22,23
   22  B(N)=B(N)-Y1/X
   23  DO 20 I1=1,4
       C(I1)=C(I1+1)
   20  E(I1)=E(I1+1)
       X=X-H
       N=N-1
       IF(X-0.5*H)21,21,16
   21  RETURN
      END
