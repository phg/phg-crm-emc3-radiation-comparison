       SUBROUTINE IPRATE(IZ,WI,EI,WJ,EJ,M,AJI,EPS,OMEG,N,T,RAT,QI,QJ)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C  ***************** fortran77 program: iprate.for *********************
C
C  Purpose:
C   CALCULATES ELECTRON COLLISIONAL EXCITATION AND DEEXCITATION RATE
C   COEFFICIENTS FOR DIPOLE TRANSITIONS IN THE IMPACT PARAMETER
C   APPROXIMATION (BURGESS AND SUMMERS,1976,MON.NOT.R.AST.SOC.,174,345)
C   OPTIONALLY A SET OF INCIDENT ELECTRON ENERGIES AND COLLISION STRENGT
C   MAY BE PROVIDED, IN WHICH CASE THE IMPACT PARAMETER THEORY IS USED T
C   CALCULATE THE COLLISION STRENGTHS AT HIGH ENERGY WITH VALUES SCALED
C   THE HIGHEST ENERGY INPUT COLLISION STRENGTH.
C   EITHER THE ABSORPTION OSCILLATOR STRENGTH OR THE EINSTEIN COEFFICIEN
C   MUST BE PROVIDED, THE OTHER BEING COMPUTED.
C
C  ARGUMENTS
C   IZ,WI,EI,WJ,EJ,M,AJI,EPS(20),OMEG(20),N,T(50),RAT,QI(50),QJ(50)
C
C  SUBROUTINES
C   EIQIP,XIP,YIP,ZERO1
C
C  INPUT
C       IZ=ION CHARGE
C       TRANSITION NAME TAKES THE FORM
C          ELECTRON TRANS.(COLS 11-15), ANGULAR TRANS.(COLS 21-40)
C       WI=STATISTICAL WEIGHT OF STATE I
C       EI=BINDING ENERGY OF STATE I (RYDBERGS)
C       WJ=STATISTICAL WEIGHT OF STATE J
C       EJ=BINDING ENERGY OF STATE J (RYDBERGS)
C       M=NUMBER OF TABULAR VALUES OF COLLISION STRENGTH
C       FIJ=ABSORPTION OSCILLATOR STRENGTH FOR TRANSITION
C       AJI=EINSTEIN COEFFICIENT FOR TRANSITION
C       EPS(K)=INCIDENT ELECTRON ENERGIES (RYDBERGS)
C       OMEG(K)=COLLISION STRENGTHS
C       N=NUMBER OF ELECTRON TEMPERATURES
C       T(I)=ELECTRON TEMPERATURES (DEGS. K)
C
C  OUTPUT
C   RAT=RATIO OF OMEG(M) TO I.P. OMEGA.
C   QI(I)=COLLISIONAL EXCITATION RATE COEFFICIENTS
C   QJ(I)=COLLISIONAL DEEXCITATION RATE COEFFICIENTS.
C
C  AUTHOR
C   HUGH SUMMERS     1977/5/20
C
C  COMMENTS
C   I IS THE LOWER LEVEL OF THE TRANSITION.
C   M MAY BE ZERO, IN WHICH CASE NO EPS AND OMEG VALUES ARE REQUIRED.
C   UNDERFLOW IS NOT TRAPPED. THIS MAY BE ACHEIVED IN IBM FORTRAN WITH T
C
C----------------------------------------------------------------------
C
C  ADAS804 version - original version H P Summers 20-05-1977.
C
C  VERSION  : 1.1
C  DATE     : 10-03-2010
C  MODIFIED : Martin O'Mullane
C              - First ADAS version in CVS.
C
C----------------------------------------------------------------------

       DIMENSION A(50),EPS(20),OMEG(20),T(50)
       DIMENSION X(4),H(4)
       DIMENSION QI(50),QJ(50)

       X(1)=0.322547689619D0
       X(2)=1.745761101158D0
       X(3)=4.536620296921D0
       X(4)=9.395070912301D0
       H(1)=6.03154104342D-1
       H(2)=3.57418692438D-1
       H(3)=3.88879085150D-2
       H(4)=5.39294705561D-4
       Z1=IZ+1
       EIJ=EI-EJ
       ELAM=9.11763D2/EIJ
       ZCOL=Z1-1.0D0
   13  FIJ=1.2449705D-10*WJ*AJI/(EIJ*EIJ*WI)
       PHI=FIJ/EIJ
       SC=1.0D0
       T2=1.0D0
       EPSM=EIJ
       R=1.25D0*Z1/EI+0.25/Z1
       EM=1.0D0
       IF(M)4,4,3
    3  CONTINUE
       M1=M-1
       EPSM=EPS(M)
       CALL EIQIP(EPSM,EIJ,EM,ZCOL,PHI,SC,WI,WJ,R,EIQ,FLAG)
       T2=WI*EIQ
       IF(M1)7,7,8
    7  EPSM=EIJ
       GO TO 9
    8  CONTINUE
    9  RAT=OMEG(M)/T2
       T2=WI*OMEG(M)/T2
    4  CONTINUE
       DO 20 I=1,N
       ATE=157890.0D0/T(I)
       T1=T2*DEXP(-ATE*(EPSM-EIJ))
       S=0.0D0
       M1=M-2
       IF(M1)10,1,1
    1  S=OMEG(1)
       U1=ATE*(EPS(1)-EIJ)
       G1=1.0D0-U1
       U2=ATE*(EPS(2)-EIJ)
       G2=DEXP(-U2)
       B=(OMEG(2)-OMEG(1))/(U2-U1)
       IF(M1)6,6,2
    2  DO 5 K=1,M1
       S=S+B*(G1-G2)
       U1=U2
       G1=G2
       U2=ATE*(EPS(K+2)-EIJ)
       G2=DEXP(-U2)
    5  B=(OMEG(K+2)-OMEG(K+1))/(U2-U1)
    6  S=S+B*(G1-G2)-OMEG(M)*G2
   10  S1=0.0D0
       DO 15 K=1,4
       E=X(K)/ATE+EPSM
       CALL EIQIP(E,EIJ,EM,ZCOL,PHI,SC,WI,WJ,R,EIQ,FLAG)
   15  S1=S1+H(K)*EIQ
       S=S+T1*S1
       P=6.8916111D-2*S/(WI*PHI)
       QJ(I)=2.171609D-8*DSQRT(ATE)*S/WJ
       QI(I)=QJ(I)*WJ*DEXP(-ATE*EIJ)/WI
   20  CONTINUE
       RETURN
      END
