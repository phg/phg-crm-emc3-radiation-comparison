C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/diag.for,v 1.2 2007/05/16 12:42:37 allan Exp $ Date $Date: 2007/05/16 12:42:37 $
C
      SUBROUTINE DIAG(N,Z,D)                                            
      IMPLICIT REAL*8(A-H,O-Z)                                          
C                                                                       
C PURPOSE: DIAGONALIZATION OF REAL SYMMETRIC N-BY-N MATRIX Z.           
C
C INPUT REQUIRED. N AND Z. ONLY LOWER TRIANGLE OF Z NEED BE SUPPLIED.   
C                          MATRIX Z OVERWRITTEN BY EIGENVECTORS OF Z.   
C OUTPUT.         Z AND D, WHERE Z CONSISTS OF COLUMN EIGENVECTORS      
C                          AND D CONSISTS OF CORRESPONDING EIGENVALUES. 
C N.B. THE VALUE OF N MUST NOT EXCEED THE DIMENSION OF THE VARIABLE E,  
C SPECIFIED IN THE FOLLOWING LINE.                                      
C                                                                       
C-------------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 16-05-2007
C  MODIFIED : Allan Whiteford
C              - Remove listing information from colums 72+. 
C              - Updated comments as part of subroutine documentation
C	         procedure.
C-------------------------------------------------------------------------
      DIMENSION D(10),E(10),Z(10,10)                                    
      TOL = 2.5D-63                                                     
      IF(N-1)20,20,3                                                    
3     DO 19 II = 2,N                                                    
      I = N - II + 2                                                    
      L = I - 2                                                         
      F = Z(I,I-1)                                                      
      G = 0.0D0                                                         
      IF(L)6,6,4                                                        
4     DO 5 K = 1,L                                                      
      G = G + Z(I,K)*Z(I,K)                                             
5     CONTINUE                                                          
6     H = G + F*F                                                       
      IF(G-TOL)7,7,8                                                    
7     E(I) = F                                                          
      H = 0.0D0                                                         
      GO TO 18                                                          
8     L = L + 1                                                         
      G = DSQRT(H)                                                      
      IF(F)10,9,9                                                       
9     G = -G                                                            
10    E(I) = G                                                          
      H = H - F*G                                                       
      Z(I,I-1) = F - G                                                  
      F = 0.0D0                                                         
      DO 15 J = 1,L                                                     
      Z(J,I) = Z(I,J)/H                                                 
      G = 0.0D0                                                         
      DO 11 K = 1,J                                                     
      G = G + Z(J,K)*Z(I,K)                                             
11    CONTINUE                                                          
      J1 = J + 1                                                        
      IF(J1-L)12,12,14                                                  
12    DO 13 K = J1,L                                                    
      G = G + Z(K,J)*Z(I,K)                                             
13    CONTINUE                                                          
14    E(J) = G/H                                                        
      F = F + G*Z(J,I)                                                  
15    CONTINUE                                                          
      HH = F/(H+H)                                                      
      DO 17 J = 1,L                                                     
      F = Z(I,J)                                                        
      G = E(J) - HH*F                                                   
      E(J) = G                                                          
      DO 16 K = 1,J                                                     
      Z(J,K) = Z(J,K) - F*E(K) - G*Z(I,K)                               
16    CONTINUE                                                          
17    CONTINUE                                                          
18    D(I) = H                                                          
19    CONTINUE                                                          
20    E(1) = 0.0D0                                                      
      D(1) = 0.0D0                                                      
      DO 28 I = 1,N                                                     
      L = I - 1                                                         
      IF(D(I)-0.0D0)21,25,21                                            
21    DO 24 J = 1,L                                                     
      G = 0.0D0                                                         
      DO 22 K = 1,L                                                     
      G = G + Z(I,K)*Z(K,J)                                             
22    CONTINUE                                                          
      DO 23 K = 1,L                                                     
      Z(K,J) = Z(K,J) - G*Z(K,I)                                        
23    CONTINUE                                                          
24    CONTINUE                                                          
25    D(I) = Z(I,I)                                                     
      Z(I,I) = 1.0D0                                                    
      IF(L)28,28,26                                                     
26    DO 27 J = 1,L                                                     
      Z(I,J) = 0.0D0                                                    
      Z(J,I) = 0.0D0                                                    
27    CONTINUE                                                          
28    CONTINUE                                                          
      EPS = 2.3D-16                                                     
      IF(N-1)33,33,31                                                   
31    DO 32 I = 2,N                                                     
      E(I-1) = E(I)                                                     
32    CONTINUE                                                          
33    E(N) = 0.0D0                                                      
      B = 0.0D0                                                         
      F = 0.0D0                                                         
      DO 54 L = 1,N                                                     
      J = 0                                                             
      H = EPS*(DABS(D(L))+DABS(E(L)))                                   
      IF (B-H)34,35,35                                                  
34    B = H                                                             
35    DO 36 M = L,N                                                     
      IF(DABS(E(M))-B)37,37,36                                          
36    CONTINUE                                                          
37    IF(M-L)38,53,38                                                   
38    IF(J-30)39,62,62                                                  
39    J = J+ 1                                                          
      P = E(L) + E(L)                                                   
      G = D(L)                                                          
      H = D(L+1) - G                                                    
      IF(DABS(H)-DABS(E(L)))40,43,43                                    
40    P = H/P                                                           
      R = DSQRT(P*P+1.0D0)                                              
      H = P + R                                                         
      IF(P-0.0D0)41,42,42                                               
41    H = P - R                                                         
42    D(L) = E(L)/H                                                     
      GO TO 44                                                          
43    P = P/H                                                           
      R = DSQRT(P*P+1.0D0)                                              
      D(L) = E(L)*P/(R+1.0D0)                                           
44    H = G - D(L)                                                      
      I1 = L + 1                                                        
      IF(I1-N)45,45,47                                                  
45    DO 46 I = I1,N                                                    
      D(I) = D(I) - H                                                   
46    CONTINUE                                                          
47    F = F + H                                                         
      P = D(M)                                                          
      C = 1.0D0                                                         
      S = 0.0D0                                                         
      M1 = M - 1                                                        
      DO 52 II = L,M1                                                   
      I = M1 - II + L                                                   
      G = C*E(I)                                                        
      H = C*P                                                           
      IF(DABS(P)-DABS(E(I)))49,48,48                                    
48    C = E(I)/P                                                        
      R = DSQRT(C*C+1.0D0)                                              
      E(I+1) = S*P*R                                                    
      S = C/R                                                           
      C = 1.0D0/R                                                       
      GO TO 50                                                          
49    C = P/E(I)                                                        
      R = DSQRT(C*C+1.0D0)                                              
      E(I+1) = S*E(I)*R                                                 
      S = 1.0D0/R                                                       
      C = C/R                                                           
50    P = C*D(I) - S*G                                                  
      D(I+1) = H + S*(C*G+S*D(I))                                       
      DO 51 K = 1,N                                                     
      H = Z(K,I+1)                                                      
      Z(K,I+1) = S*Z(K,I) + C*H                                         
      Z(K,I)   = C*Z(K,I) - S*H                                         
51    CONTINUE                                                          
52    CONTINUE                                                          
      E(L) = S*P                                                        
      D(L) = C*P                                                        
      IF(DABS(E(L))-B)53,53,38                                          
53    D(L) = D(L) + F                                                   
54    CONTINUE                                                          
      DO 61 I = 1,N                                                     
      K = I                                                             
      P = D(I)                                                          
      I1 = I + 1                                                        
      IF(I1-N)55,55,58                                                  
55    DO 57 J = I1,N                                                    
      IF(D(J)-P)56,57,57                                                
56    K = J                                                             
      P = D(J)                                                          
57    CONTINUE                                                          
58    IF(K-I)59,61,59                                                   
59    D(K) = D(I)                                                       
      D(I) = P                                                          
      DO 60 J = 1,N                                                     
      P = Z(J,I)                                                        
      Z(J,I) = Z(J,K)                                                   
      Z(J,K) = P                                                        
60    CONTINUE                                                          
61    CONTINUE                                                          
      RETURN                                                            
62    WRITE(6,100)                                                      
      RETURN                                                            
100   FORMAT(' FAILED IN DIAG')                                         
      END                                                               
