C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas804/dipol.for,v 1.3 2007/05/16 12:42:08 allan Exp $
C
       SUBROUTINE DIPOL(JSW,N1,N2,E2,LMAX,CP,CM,JC)

       IMPLICIT REAL*8(A-H,O-Z)
C-------------------------------------------------------------------------
C  CALCULATES SQUARES OF HYDROGENIC DIPOLE LENGTH RADIAL MATRIX ELEMENTS
C  FOR BOUND-BOUND OR BOUND-FREE TRANSITIONS.
C
C  BOUND STATES ARE NORMALISED TO UNITY.
C  FREE STATES ARE NORMALISED TO ASYMPTOTIC AMPLITUDE K**(-0.5).
C
C  INPUT
C   FOR BOUND-BOUND,SET JSW=NEGATIVE
C                     N1,N2=PRINCIPAL QUANTUM NUMBERS OF STATES
C                      LMAX=RANGE OF ANGULAR MOMENTUM QUANTUM NUMBERS
C   FOR FREE-FREE, SET JSW=POSITIVE
C                       N1=BOUND STATE PRINCIPAL QUANTUM NUMBER
C                       E2=FREE STATE ENERGY IN RYDBERGS (=K**2)
C
C  OUTPUT
C   VECTOR CP(L),L=1,LMAX,CONTAINS SQUARED MATRIX ELEMENTS FOR ANGULAR
C                         MOMENTUM TRANSITIONS FROM L-1 TO L,
C   VECTOR CM(L),L=1,LMAX,CONTAINS SQUARED MATRIX ELEMENTS FOR ANGULAR
C                         MOMENTUM TRANSITIONS FROM L TO L-1,
C               IN BOTH CASES THE ENERGY CHANGE IS FROM LOWER TO HIGHER,
C               INDEPENDANT OF THE SIGN OF N1-N2 FOR BOUND-BOUND CASES.
C               IF N1??? THEN CP(L)=CM(L).
C   VECTOR JC(L),L=1,LMAX WILL USUALLY BE ZERO AND MAY THEN BE IGNORED,
C               BUT FOR EXTREME INPUT VALUES THERE IS POSSIBILITY OF
C               OVER OR UNDERFLOW OF CP(L) OR CM(L),IN WHICH CASE THE
C               OUTPUT VALUES OF CP(L) AND CM(L) SHOULD BE MULTIPLIED
C               BY (1.0D10)**JC(L) TO OBTAIN TRUE VALUES.
C  FOR SINGLE PRECISION OPERATION,CHANGES ARE REQUIRED AT LINES NUMBER
C  31 33 34 35 36 37 38 39 40 41 42 43 44 117 119 130 133 149 150 153
C
C
C-------------------------------------------------------------------------
C
C  VERSION  : 1.1                          
C  DATE     : ?
C  MODIFIED : H P Summers 
C              - Initial version. 
C
C  VERSION  : 1.2                          
C  DATE     : 20-02-2005
C  MODIFIED : Martin O'Mullane  
C              - Remove listing information from colums 72+. 
C
C  VERSION  : 1.3                          
C  DATE     : 16-05-2007
C  MODIFIED : Allan Whiteford
C              - Updated comments as part of subroutine documentation
C	         procedure.
C
C-------------------------------------------------------------------------
       DIMENSION CP(LMAX),CM(LMAX),JC(LMAX)
       ZERO=0.0D0
       ONE=1.0D0
       PI=3.14159265359D0
       S1=1.0D10
       S2=1.0D-10
       TEST1=1.0D-20
       TEST2=1.0D20
       TEST3=0.044D0
       TEST4=0.1D0
       TEST5=300.0D0
       TEST6=1.0D-30
       TEST7=1.0D30
       N=N1
       IF(JSW)1,1,4
    1  EN2=N2
       N3=N2
       IF(N2-N1)2,59,3
    2  N=N2
       EN2=N1
       N3=N1
    3  E2=-ONE/(EN2*EN2)
    4  EN=N
       ENN=EN*EN
       E1=-ONE/ENN
       JMAX=LMAX
       IF(N-LMAX)5,7,8
    5  L1=N+1
       DO 6 L=L1,LMAX
       CP(L)=ZERO
       CM(L)=ZERO
       JC(L)=0
    6  CONTINUE
    7  CP(N)=ONE
       CM(N)=ZERO
       JC(N)=0
       JMAX=N-1
    8  C1=ONE
       C2=ZERO
       JS=0
       L=N+1
    9  L=L-1
       IF(L-1)15,15,10
   10  EL=L
       ELL=EL*EL
       T1=ONE+ELL*E1
       T2=ONE+ELL*E2
       T3=L+L-1
       T4=ONE/(T3+ONE)
       T5=(T3*T1*C2+T2*C1)*T4
       C1=(T1*C2+T3*T2*C1)*T4
       C2=T5
   11  IF(C1*C1-TEST2)13,13,12
   12  C1=S2*C1
       C2=S2*C2
       JS=JS+1
       GO TO 11
   13  IF(L-LMAX-1)14,14,9
   14  CP(L-1)=C1
       CM(L-1)=C2
       JC(L-1)=JS
       GO TO 9
   15  CONTINUE
       JS=0
       T=4
       T=ONE/(T*EN*ENN)
       IF(JSW)16,16,23
   16  ENN2=EN2*EN2
       T1=4
       T1=T1*ENN*ENN2/(ENN2-ENN)
       T1=T1*T1
       T=T*T1*T1/(EN2*ENN2)
       IF(N3-30)17,17,18
   17  T=T*((EN2-EN)/(EN2+EN))**(N3+N3)
       GO TO 34
   18  E21=E2/E1
       IF(E21-TEST4)19,19,21
   19  T2=ZERO
       DO 20 J=1,11
       T3=2*(11-J)+1
       T2=ONE/T3+T2*E21
   20  CONTINUE
       T2=T2+T2
       GO TO 22
   21  T3=EN/EN2
       T2=DLOG((ONE+T3)/(ONE-T3))/T3
   22  T2=T2+T2
       T1=T1*DEXP(-T2)
       GO TO 34
   23  T1=4
       T1=T1*ENN/(ONE+ENN*E2)
       T1=T1*T1
       T=T*T1*T1
       IF(E2-TEST3)24,25,25
   24  T3=2
       T=T*(PI/T3)
       GO TO 29
   25  CONTINUE
       T4=DSQRT(E2)
       IF(T4-TEST5)26,26,27
   26  T3=(PI+PI)/T4
       T3=ONE-DEXP(-T3)
       T3=ONE/T3
       GO TO 28
   27  T4=PI/T4
       T3=3
       T3=(ONE+T4+T4*T4/T3)/(T4+T4)
   28  T2=2
       T=T*(PI*T3/T2)
   29  T4=ENN*E2
       IF(T4-TEST4)30,30,32
   30  T2=ZERO
       DO 31 J=1,11
       T3=2*(11-J)+1
       T2=ONE/T3-T2*T4
   31  CONTINUE
       GO TO 33
   32  T3=DSQRT(T4)
       T2=DATAN(T3)/T3
   33  T2=T2+T2
       T2=T2+T2
       T1=T1*DEXP(-T2)
   34  DO 39 J=1,N
       TJ=J+J
       T2=TJ*(TJ-ONE)
       T2=T2*T2
       T=T*T1/T2
   35  IF(T-TEST1)36,36,37
   36  T=T*S1
       JS=JS-1
       GO TO 35
   37  IF(T-TEST2)39,38,38
   38  T=T*S2
       JS=JS+1
       GO TO 37
   39  CONTINUE
       J=0
   40  J=J+1
       IF(J-JMAX)41,41,50
   41  TJ=J
       TJ=TJ*TJ
       T1=ONE+TJ*E1
       T2=ONE+TJ*E2
       T3=CP(J)
       T3=T2*T*T3*T3
       T4=CM(J)
       T4=T1*T*T4*T4
       L1=JC(J)+JC(J)+JS
   42  IF(L1)43,47,45
   43  IF(T4-TEST6)47,47,44
   44  L1=L1+1
       T3=T3*S2
       T4=T4*S2
       GO TO 42
   45  IF(T3-TEST7)46,47,47
   46  L1=L1-1
       T3=T3*S1
       T4=T4*S1
       GO TO 42
   47  CP(J)=T3
       CM(J)=T4
       JC(J)=L1
       T=T*T1*T2
   48  IF(T-TEST2)40,40,49
   49  T=T*S2
       JS=JS+1
       GO TO 48
   50  IF(N-LMAX)51,51,58
   51  T2=ONE+ENN*E2
       T3=CP(N)
       T3=T2*T*T3*T3
       L1=JC(N)+JC(N)+JS
   52  IF(L1)53,57,55
   53  IF(T3-TEST6)57,57,54
   54  L1=L1+1
       T3=T3*S2
       GO TO 52
   55  IF(T3-TEST7)56,57,57
   56  L1=L1-1
       T3=T3*S1
       GO TO 52
   57  CP(N)=T3
       JC(N)=L1
   58  RETURN
   59  JMAX=LMAX
       IF(N-LMAX)60,60,62
   60  DO 61 L=N,LMAX
       CP(L)=ZERO
       CM(L)=ZERO
       JC(L)=0
   61  CONTINUE
       JMAX=N-1
   62  T1=9
       T2=4
       T3=(T1/T2)
       T1=EN2*EN2
       T2=T1*T3
       DO 63 J=1,JMAX
       TJ=J
       JC(J)=0
       T=T2*(T1-TJ*TJ)
       CP(J)=T
       CM(J)=T
   63  CONTINUE
       RETURN
      END
