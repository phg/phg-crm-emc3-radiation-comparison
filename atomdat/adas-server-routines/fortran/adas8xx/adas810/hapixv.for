       subroutine hapixv( ndwvl  , ndpix  , fcrit  ,
     &                    nwvl   , npix   , wvmin  , wvmax  ,
     &                    iwvrg  , cpixmx ,
     &                    wvl    , tev    , amssno , pec    ,
     &                    cpixa  , ind1   , ind2
     &                  )

       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: hapixv *********************
c
c  purpose:  distribute Doppler broadened line emission into pixel range
c
c  calling program: hapecf
c
c
c  subroutine:
c
c  input : (i*4)  ndwvl   = maximum number of wavelength intervals
c  input : (i*4)  ndpix   = maximum number of pixels per wvln. interval
c  input : (r*8)  fcrit   = pixel counts for the selected line below
c                           this fraction of the largest pixel count are
c                           discounted.
c
c  input : (i*4)  nwvl    = number of wavelength intervals
c  input : (i*4)  npix()  = number of pixels assigned to wavelength interval
c  input : (r*8)  wvmin() = lower limit of wavelength interval (ang)
c  input : (r*8)  wvmax() = upper limit of wavelength interval (ang)
c
c  input : (i*4)  iwvrg   = index of wavelength range in which line lies
c  input : (r*8)  cpixmx  = largest pixel count currently found
c                           for the wavelength range
c
c  input : (r*8)  wvl     = input line wavelength for test(ang)
c  input : (r*8)  tev     = electron temperature (eV)
c  input : (r*8)  amssno  = atomic mass number
c  input : (r*8)  pec     = photon emissivity coefficient for line
c
c  output: (r*8)  cpixa() = counts in each pixel for the line
c  output: (r*8)  ind1    = first pixel with non-negligible count
c  output: (r*8)  ind2    = last pixel with non-negligible count
c
c routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          r8erfc     adas      returns erfc(x) function value
c
c author:  Hugh Summers, University of Strathclyde
c          JA7.08
c          tel. 0141-548-4196
c
c date:    15/01/02
c
c version  : 1.1
c date     : 15-01-2002
c modified : H P Summers
c             - first version.
c
c version  : 1.2
c date     : 18-06-2007
c modified : H P Summers
c             - corrected error in ind1 & ind2 return.
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   i4unit
      integer   ndwvl       , ndpix
      integer   nwvl        , iwvrg
      integer   ind1        , ind2
      integer   i
      integer   i0
c-----------------------------------------------------------------------
      real*8    wvl         , fcrit     , cpixmx   , cmax
      real*8    tev         , amssno    , pec
      real*8    sigma       , h         , dwvl     ,
     &          xp          , xm        , erfcp    , erfcm    ,
     &          erfcp1      , erfcm1    , cnt
      real*8    r8erfc
c-----------------------------------------------------------------------
      integer   npix(ndwvl)
c-----------------------------------------------------------------------
      real*8    wvmin(ndwvl), wvmax(ndwvl) , cpixa(ndpix)
c-----------------------------------------------------------------------
c----------------------------------------------------------------------
c  establish sigma and the pixel in which the line lies
c-----------------------------------------------------------------------

      sigma = 4.6344d-5*wvl*dsqrt(tev/amssno)
      h     = (wvmax(iwvrg)-wvmin(iwvrg))/npix(iwvrg)
      i0    = int((wvl-wvmin(iwvrg))/h)+1
      dwvl  = wvl-h*dfloat(i0-1)-wvmin(iwvrg)


c-----------------------------------------------------------------------
c fill the pixel bins working outwards from i0
c-----------------------------------------------------------------------
       xm=dwvl
       xp=h-dwvl
       erfcm = r8erfc(xm/sigma)
       erfcp = r8erfc(xp/sigma)

       cpixa(i0)=pec*(1.0d0-0.5d0*(erfcm+erfcp))
       cmax=dmax1(cpixmx,cpixa(i0))

       ind1=i0
       ind2=i0

       if(i0.gt.1) then
           do i=i0-1,1,-1
             xm=xm+h
             erfcm1 = r8erfc(xm/sigma)
             cnt = 0.5d0*pec*(erfcm-erfcm1)
             if(cnt.gt.(fcrit*cmax))then
                 cpixa(i)=cnt
                 erfcm=erfcm1
             else
                 ind1 = i+1
                 go to 10
             endif
           enddo
           ind1 = 1
       endif

   10  if(i0.lt.npix(iwvrg)) then
           do i=i0+1,npix(iwvrg)
             xp=xp+h
             erfcp1 = r8erfc(xp/sigma)
             cnt = 0.5d0*pec*(erfcp-erfcp1)
             if(cnt.gt.(fcrit*cmax))then
                 cpixa(i)=cnt
                 erfcp=erfcp1
             else
                 ind2 = i-1
                 go to 20
             endif
           enddo
           ind2 = npix(iwvrg)
        endif

   20   return

c
c-----------------------------------------------------------------------
c
 1001 format(1x,32('*'),' hapixv error ',32('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
