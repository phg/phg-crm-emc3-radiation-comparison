C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas810/haadas2.for,v 1.1 2004/07/06 14:04:29 whitefor Exp $ Date $Date: 2004/07/06 14:04:29 $
C
       subroutine haadas2(ichan   , date   ,
     +                    ntdim   , nnedim ,
     +                    data    ,
     +                    itmax   , idmax  ,
     +                    dense   , tempe  ,
     +                    iz0     , iz1    ,
     +                    name    , method ,
     +                    cstrg1  , cstrg2 , 
     +                    infplt  , comments , ncomments )
c 
        implicit none
c	 
c-----------------------------------------------------------------------
c
c  ******************** fortran77 subroutine: haadas2 ******************
c
c  purpose : to write plt standard adas density dependent data 
c            The data is in the form :-
c                        data(it,id,iz)
c            where,
c                  it     :  temperature index ( 1 - itmax )
c                  id     :  density     index ( 1 - idmax )
c
c            with electron temperatures ---- tempe(1 - itmax)
c                electron densities     ---- dense(1 - idmax)
c
c  calling program: adas810
c
c  input :  (i*4)  ichan     =  stream number (previously allocated)
c  input :  (i*4)  ntdim     =  maximum number of temperatures
c  input :  (i*4)  nnedim    =  maximum number of densities
c  input :  (i*4)  itmax     =  number of temperatures
c  input :  (i*4)  idmax     =  number of densities
c  input :  (i*4)  iz0       =  nuclear charge of species
c  input :  (i*4)  iz1       =  ion charge +1
c  input :  (i*4)  ncomments =  number of comment strings
c  input :  (r*8)  data       =  profile array (see above)
c  input :  (r*8)  dense      =  electron densities
c  input :  (r*8)  tempe      =  electron temperatures
c  input :  (c*13) name       =  name of element
c  input :  (c*25) method     =  method used in the calculations
c  input :  (c*10) cstrg1     =  ground information string
c  input :  (c*10) cstrg2     =  parent information string
c  input :  (c*8)  infplt     =  total line power stringssection
c  input :  (c*80) comments() =  comment strings
c
c
c author:  h. p. summers, university of strathlcyde
c
c date:    24 April 2002
c
c update:
c
c-----------------------------------------------------------------------
       integer    ichan    , ntdim   , nnedim
       integer    itmax    , idmax
       integer    iz0      , iz1
       integer    i        , it      , id
       integer    ncomments
c-----------------------------------------------------------------------
       character  adfcode*5   , date*8    , infplt*8 
       character  cstrg1*10   , cstrg2*10 , name*13   , method*24
c-----------------------------------------------------------------------
       real*8     data(ntdim,nnedim)
       real*8     tempe(ntdim),dense(nnedim)
c-----------------------------------------------------------------------
       character  comments(ncomments)*80
c-----------------------------------------------------------------------
       data adfcode/'ADF11'/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
 
       date(3:3) = '.'
       date(6:6) = '.'
 
       write(ichan,1000)iz0,idmax,itmax,iz1,iz1,name,method,adfcode
       write(ichan,1100)
       write(ichan,1200)(dense(id),id = 1,idmax)
       write(ichan,1200)(tempe(it),it = 1,itmax)
 
       write(ichan,1300)cstrg1,cstrg2,infplt,iz1,date
       do it = 1,itmax
          write(ichan,1400)(data(it,id),id = 1,idmax)
       end do
       
       do i=1,ncomments
         write(ichan,1500)comments(i)
       end do
 
 1000  format(5i5,5x,'/',a13,6x,'/',a24,a5)
 1100  format(' -----------------------------------------------------',
     +        '--------------------------')
 1200  format(8(1x,f9.5))
 1300  format(' --------------------/', 1a10 ,'/', 1a10 ,'/',1a8,
     +        '/ Z1=',i2,'   / DATE= ',a8)
 1400  format(8f10.5)
 1500  format(1a80)
 
 
       return
       end
