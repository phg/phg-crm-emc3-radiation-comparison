       program adas810

       implicit none
C-----------------------------------------------------------------------
C
C  ********************** FORTRAN77 PROGRAM: ADAS810 *******************
C
C  Version:  1.1
C
C  Purpose:  to calculate excited populations for a complex ion and
C            generate principal adf15 pec coefficients, envelope feature
C            adf40 f-pec coefficients, low level line power (unfiltered
C            and filtered) adf11 plt and adf11 plt_filter coefficients.
C
C  Program:
C
C          (I*4)  iunt09  = input unit for adf43 and adf04 file
C          (I*4)  iunt11  = output unit for adf11-plt results
C          (I*4)  iunt12  = output unit for adf11-plt results
C          (I*4)  iunt13  = output unit for adf40-f_pec results
C          (I*4)  iunt14  = output unit for adf40-f_pec results
C
C          (I*4)  ndlev   = maximum number of levels allowed
C          (I*4)  ndtrn   = maximum number of transitions allowed
C          (I*4)  ndtem   = maximum number of temperatures allowed
C          (I*4)  ndden   = maximum number of densities allowed
C          (I*4)  ndmet   = maximum number of metastables allowed
C          (I*4)  ndwvl   = maximum number of wavelength intervals
C          (I*4)  ndpix   = maximum no. of pixels in a wvl. interval
C          (I*4)  nvmax   = maximum no. of electron temps. in adf04 file
C
C          (I*4)  i       = general integer variable
C          (I*4)  j       = general integer variable
C          (I*4)  iunt27  = output unit for results from expansion routine
C          (I*4)  ii       = general integer variabl
C          (I*4)  iz0      = nuclear charge
C          (I*4)  iz       = recombined ion charge
C          (I*4)  iz1      = recombining ion charge
C          (I*4)  npl      = no. of metastables of(z+1) ion accessed
C                              by excited state ionisation in copase
C                              file with ionisation potentials given
C                              on the first data line
C          (I*4)  nplr     = no. of active metastables of (z+1) ion
C          (I*4)  npli     = no. of active metastables of (z-1) ion
C          (I*4)  npl3     = no. of active metastables of (z+1) ion with
C                            three-body recombination on.
C          (I*4)  il      = number of energy levels
C          (I*4)  nv      = no. of electron temps. in input adf04 file
C          (I*4)  itran   = no. of transitions in input adf04 file
C          (I*4)  maxlev  = highest index level in read transitions
C          (I*4)  nmet    = number of metastables levels: 1<=nmet<=ndmet
C          (I*4)  nord    = number of ordinary levels ('il' - 'nmet')
C          (I*4)  ifout   = temperature form passed to xxtcon subroutine
C                             1 => input temperatures in kelvin
C                             2 => input temperatures in ev
C                             3 => input temperatures in reduced form
C          (I*4)  idout   = density form passed to xxdcon subroutine
C                             1 => input densities in cm-3
C                             2 => input densities in reduced form
C          (I*4)  maxt    = number of input temperatures ( 1 -> 'ndtem')
C          (I*4)  maxd    = number of input densities ( 1 -> 'ndden')
C          (I*4)  icnte   = number of electron impact transitions input
C          (I*4)  icntp   = number of proton impact transitions input
C          (I*4)  icntr   = number of free electron recombinations input
C          (I*4)  icnth   = no. of charge exchange recombinations input
C          (I*4)  icnti   = number of lower stage ionisations      input
C          (I*4)  icntl   = number of 'l' line unresolved DR lines input
C          (I*4)  icnts   = number of 's' line elec.impact ionis. input
C          (I*4)  nwvl    = number of wavelength intervals
C
C          (R*8)  bwno    = ionisation potential (cm-1) of lowest parent
C          (R*8)  zeff    = plasma z effective ( if 'lzsel' = .true.)
C                            (if 'lzsel' = .false. => 'zeff=1.0')
C          (R*8)  z2scl   = multilpier used for temperature z-scaling
C          (R*8)  z7scl   = multilpier used for density z-scaling
C          (R*8)  bwno1   = ionisation potential from  adf43 driver
C          (R*8)  bwno2   = ionisation potential from  adf04 input file
C                           (bwno1 & bwno2 are checked for consistency)
C
C          (C*132)dsnin   = input adf04 specific ion file
C          (C*132)dsnexp  = input expansion file
C          (C*132)dsnflt  = input adf35 filter file
C          (C*132)dsnplt  = output unfiltered adf11 plt file
C          (C*132)dsnpec  = output adf15 pec photon emissivity file
C          (C*132)dsnfet  = output adf43 f-pec env. feature emiss. file
C          (C*132)dsnpltf = output filtered adf11 plt file
C
C          (C*3)  celem*3 = element symbol in form '<symbol>+'
C          (C*132)dsn     = input adf42 driver data file
C          (C*8)  date    = current date
C          (C*30) user    = full name of author
C
C          (L*4)  open27  = .TRUE.   => file allocated to unit 7.
C                         = .FALSE.  => no file allocated to unit 7.
C          (L*4)  lpsel   = .TRUE.   => include proton collisions
C                         = .FALSE.  => do not include proton collisions
C          (L*4)  lzsel   = .TRUE.   => scale proton collisions with
C                                       plasma z effective'zeff'.
C                         = .FALSE.  => do not scale proton collisions
C                                       with plasma z effective 'zeff'.
C          (L*4)  liosel  = .TRUE.   => include ionisation rates
C                         = .FALSE.  => do not include ionisation rates
C                                       for recom and 3-body
C          (L*4)  lhsel   = .TRUE.   => include charge transfer from
C                                       neutral hydrogren.
C                         = .FALSE.  => do not include charge transfer
C                                       from neutral hydrogren.
C          (L*4)  lrsel   = .TRUE.   => include free electron
C                                       recombination.
C                         = .FALSE.  => do not include free electron
C                                       recombination.
C          (L*4)  lisel   = .TRUE.   => include electron impact
C                                       ionisation.
C                         = .FALSE.  => do not include free electron
C                                       recombination.
C          (L*4)  lnsel    = .TRUE.  => include projected bundle-n data
C                                       from datafile if available
C                          = .FALSE. => do not include projected
C                                       bundle-n data
C          (L*4)  lftlsel  = .TRUE.  => adf35 filter file selected
C                          = .FALSE. => Do not filter output
C          (L*4)  lmetr    = .TRUE.  => resolve metastables in
C                                       population calculation
C                          = .FALSE. => do not resolve metastables in
C                                       population calculation
C          (L*4)  ltscl    = .TRUE.  => electron temperatures in driver
C                                       adf43 file are z-scaled
C                          = .FALSE. => electron temperatures in driver
C                                       adf43 file are not z-scaled
C          (L*4)  ldscl    = .TRUE.  => electron densities in driver
C                                       adf43 file are z-scaled
C                          = .FALSE. => electron densities in driver
C                                       adf43 file are not z-scaled
C          (L*4)  lbrdi    = .TRUE.  => lines are to be ion temp.
C                                       broadened at elec. temp.
C                          = .FALSE. => lines are not to be ion temp.
C                                       broadened at elec. temp.
C          (L*4)  lnorm    = .TRUE.  => if nmet=1 then various
C                                       emissivity output files
C                                       normalised to stage tot.populatn.
C                                       (** norm type = t)
C                          = .FALSE. => otherwise normalise to identified
C                                       metastable populations.
C                                       (** norm type = m)
C          (I*4)  imetr() = index of metastable in complete level list
C          (I*4)  iordr() = index of ordinary levels in complete level
C                           list.
C          (I*4)  ia()    = energy level index number
C          (I*4)  isa()   = multiplicity for level 'ia()'
C                           note: (isa-1)/2 = quantum number (s)
C          (I*4)  ila()   = quantum number (l) for level 'ia()'
C          (I*4)  i1a()   = transition:
C                            lower energy level index (case ' ' & 'p')
C                            signed parent ndex (case 'h','r' & 'i')
C          (I*4)  i2a()   = transition:
C                            upper energy level index (case ' ' & 'p')
C                            capturing level index (case 'h','r' & 'i')
C          (I*4)  npla()  = no. of parent/zeta contributions to ionis.
C                            of level
C          (I*4)  ipla(,) = parent index for contributions to ionis.
C                            of level
C                            1st dimension: parent index
C                            2nd dimension: level index
C          (I*4)  ietrn()  ietrn() = electron impact transition:
C                            index values in main transition arrays which
C                            represent electron impact transitions.
C          (I*4)  iptrn() = proton impact transition:
C                            index values in main transition arrays which
C                            represent proton impact transitions.
C          (I*4)  irtrn() = free electron recombination:
C                            index values in main transition arrays which
C                            represent free electron recombinations.
C          (I*4)  ihtrn() = charge exchange recombination:
C                            index values in main transition arrays which
C                            represent charge exchange recombinations.
C          (I*4)  iitrn() = electron impact ionisation:
C                            index values in main transition arrays which
C                            represent ionisations from the lower stage
C          (I*4)  iltrn() = satellite dr recombination:
C                            index values in main transition arrays which
C                            represent satellite dr recombinations.
C          (I*4)  istrn() = electron impact ionisation:
C                            index values in main transition arrays which
C                            represent ionisations to upper stage ion.
C          (I*4)  ie1a()  = electron impact transition:
C                             lower energy level index
C          (I*4)  ie2a()  = electron impact transition:
C                             upper energy level index
C          (I*4)  ip1a()  = proton impact transition:
C                             lower energy level index
C          (I*4)  ip2a()  = proton impact transition:
C                             upper energy level index
C          (I*4)  ia1a()  = auger transition:
C                             parent energy level index
C          (I*4)  ia2a()  = auger transition:
C                             recombined ion energy level index
C          (I*4)  il1a()  = satellite dr transition:
C                             recomnining ion  index
C          (I*4)  il2a()  = satellite dr transition:
C                             recombined ion index
C          (I*4)  is1a()  = ionising transition:
C                             ionised ion  index
C          (I*4)  is2a()  = ionising transition:
C                             ionising ion index
C          (I*4)  npix()  = number of pixels in each wvln. interval
C
C          (R*8)  scef()  =  electron temps (K) from adf04 input file
C          (R*8)  bwnoa() = ionisation potential (cm-1) of parents
C          (R*8)  prtwta()= weight for parent associated with bwnoa()
C          (R*8)  tine()  = electron temperatures (k) from adf43 driver
C          (R*8)  tinp()  = ion temperatures (k) - set equal to tine()
C          (R*8)  tinh()  = neutral H temps (k) - set equal to tine()
C          (R*8)  dine()  = electron densities (cm-3) from adf43 driver
C          (R*8)  dinp()  = proton densities - set to zero
C          (R*8)  aa()    = electron impact transition: a-value (sec-1)
C          (R*8)  aval()  = transition:
C                            a-value (sec-1)          (case ' ')
C                            neutral beam energy      (case 'h')
C                            not used             (case 'p','r' & 'i')
C          (R*8)  auga()  = auger transition: aug-value (sec-1)
C                             recombined ion energy level index
C          (R*8)  xja()   = quantum number (j-value) for level 'ia()'
C                           note: (2*xja)+1 = statistical weight
C          (R*8)  wa()    = energy relative to level 1 (cm-1)
C                           dimension: level index
C          (R*8)  er()    = energy relative to level 1 (rydbergs)
C                            dimension: level index
C          (R*8)  xia()   = energy relative to ion. pot. (rydbergs)
C                            dimension: level index
C          (R*8)  zpla(,) = eff. zeta param. for contributions to ionis.
C                            of level
C                            1st dimension: parent index
C                            2nd dimension: level index
C          (R*8)  wvla()  = wavelength associated with unresolved
C                           DR satellite 'l' lines
C          (R*8)  scom(,) = transition data from input adf04 file:
C                             upsilon values  input :  (case ' ' & 'p')
C                             rate coefft. (cm3 sec-1) (case 'h' & 'r')
C                            1st dimension - temperature 'scef()'
C                            2nd dimension - transition number
C          (R*8)  popun(,,)= level populations returned from hacoef
C                              1st dimension: level index
C                              2nd dimension: temperature index
C                              3rd dimension: density index
C          (R*8)  wvmin()   = minimum  wvln. (A) for each interval
C          (R*8)  wvmax()   = maximum  wvln. (A) for each interval
C
C          (R*8)  ratpia(,)  = ratio ( n(z+1)/n(z)  stage abundancies )
C                             1st dimension: temp/dens index
C                             2nd dimension: parent index
C          (R*8)  ratmia(,)  = ratio ( n(z-1)/n(z)  stage abundancies )
C                             1st dimension: temp/dens index
C                             2nd dimension: parent index
C          (R*8)  stckm(,,) = metastable populations stack
C                             1st dimension: metastable index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C          (R*8)  stvrm(,,,)= metastable free electron recombination
C                            coefficients.
C                             1st dimension: metastable index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C                             4th dimension: parent index
C          (R*8)  stvhm(,,,)= metastable charge exchange coefficients
C                             1st dimension: metastable index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C          (R*8)  stvim(,,,)= metastable electron impact ionisation
C                            coefficients.
C                             1st dimension: metastable index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C                             4th dimension: parent index
C
C          (R*4)  stvr(,,,) = free electron recombination coefficients
C                             1st dimension: ordinary level index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C                             4th dimension: parent index
C          (R*4)  stvh(,,,) =  charge exchange coefficients
C                             1st dimension: ordinary level index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C          (R*4)  stvi(,,,) = electron impact ionisation  coefficients
C                             1st dimension: ordinary level index
C                             2nd dimension: temperature index
C                             3rd dimension: density index
C                             4th dimension: parent index
C          (R*4)  stack(,,,)= population dependence
C                             1st dimension: ordinary level index
C                             2nd dimension: metastable index
C                             3rd dimension: temperature index
C                             4th dimension: density index
C
C          (C*18) cstrga() = nomenclature/configuration for level 'ia()'
C          (C*1)  tcode()  = transition: data type pointer:
C                            ' ' => electron impact   transition
C                            'p' => proton   impact   transition
C                            'h' => charge   exchange recombination
C                            'r' => free     electron recombination
C                            'i' => coll. ionisation from lower stage ion
C          (C*1)  cpla()   = char. specifying 1st parent for level 'ia()'
C                                integer - parent in bwnoa() list
C                                'blank' - parent bwnoa(1)
C                                  'x'   - do not assign a parent
C          (C*9)  cprta()  = parent term enclosed in brackets
C
C          (L*4)  lbseta() = .true.  - parent weight set for bwnoa()
C                            .false. - parent weight not set for bwnoa()
C          (L*4)  lss04a(,) = .true. => ionis. rate set in adf04 file:
C                            .false.=> not set in adf04 file
C                             1st dim: level index
C                             2nd dim: parent metastable index
C
C
C Routines:
C          routine    source    brief description
C          ------------------------------------------------------------
C          haddat     ADAS      Fetch data from an adas810 driver set
C          hadetm     ADAS      Analyse adf04 file to find metastables
C          hacoef     ADAS      Calculate collisional-radiative popns.
C          hapecf     ADAS      Calc. pec, f-pec, plt & plt-filter files
C          badata     ADAS      Read data from a specific ion adf04 file
C          b8ttyp     ADAS      Sort transition types in an adf04
C          xxname     ADAS      Obtain user name from unix system
C          xxeryd     ADAS      Obtain ionisation energies of level set
C
C
C  AUTHOR:  Hugh Summers (University of Strathclyde)
C
C  DATE  :  25-04-2002
C
C  UPDATE:
C
C  VERSION  : 1.1
C  DATE     : 17-01-2003
C  MODIFIED : Martin O'Mullane
C              - This is the interactive ADAS version.
C
C  VERSION  : 1.2
C  DATE     : 29-05-2003
C  MODIFIED : Martin O'Mullane
C             - Use xxdata_04 to read adf04 file.
C             - Move haddat to xxdata_42 to keep consistency
C               in reading adf formatted data.
C             - Increase NDLEV to 1200.
C
C  VERSION  : 1.3
C  DATE     : 27-02-2004
C  MODIFIED : Martin O'Mullane
C             - Pass the adf35 filter dataset to hapecf.
C
C  VERSION  : 1.4
C  DATE     : 29-09-2009
C  MODIFIED : Martin O'Mullane
C             - Increase number of levels to 3500
C             - Increase number of transitions to 380000
C             - Increased ndqdn from 6 to 8
C
C  VERSION  : 1.5
C  DATE     : 01-08-2011
C  MODIFIED : Martin O'Mullane
C              - Add ion temperature (tinp) to hapecf call.
C
C  VERSION  : 1.6
C  DATE     : 24-04-2019
C  MODIFIED : Martin O'Mullane
C              - Increase the length of all filenames to 132 characters.
C              - Read a terminating signal from IDL before finishing.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       integer    pipein       , pipeou    , one
       integer    iunt09
       integer    ndlev        , ndtrn     , ndtem    , ndden   ,
     &            ndmet        , ndqdn     , ndwvl    , ndpix   , nvmax
       integer    iunt07       , iunt15    , iunt40   , iunt11  ,
     &            iunt11f      , iunt35
C-----------------------------------------------------------------------
       parameter( pipein = 5   , pipeou = 6     , one     = 1   )
       parameter( iunt07 = 07  , iunt09 = 09    , iunt15  = 10  ,
     &            iunt40 = 11  , iunt11 = 12    , iunt11f = 13  ,
     &            iunt35 = 14  )
       parameter( ndlev  = 3500, ndtrn  = 380000 , ndtem   = 24  ,
     &            ndden  = 24  , ndmet  = 4      , ndqdn   = 8   )
       parameter( ndwvl  = 5   , ndpix  = 1024  )
       parameter( nvmax  = 14  )
C-----------------------------------------------------------------------
       integer    i4unit
       integer    i         , j           , iunt27
       integer    iz0       , iz          , iz1     ,
     &            npl       , nplr        , npli    , npl3
       integer    il        , nv          , itran   ,
     &            maxlev    ,
     &            nmet      , nord        ,
     &            ifout     , idout       ,
     &            maxt      , maxd
       integer    icnte     , icntp       , icntr   , icnth   ,
     &            icnti     , icntl       , icnts   ,
     &            iadftyp   , itieactn    , iorb
       integer    nwvl
       integer    ilogic
C-----------------------------------------------------------------------
       character  dsnin*132 , dsnexp*132  , dsnflt*132 ,
     &            dsnplt*132, dsnpec*132  , dsnfet*132 , dsnpltf*132,
     &            dsnpap*132
       character  celem*3   , dsn42*132   , title*80   , method*8   ,
     &            date*8    , user*30     , reply*8    , cadas*80
C-----------------------------------------------------------------------
       real*8     bwno      , zeff
       real*8     bwno1     , bwno2       , fmul       , amin
C-----------------------------------------------------------------------
       logical    open27    , ltick
       logical    lpsel     , lzsel       , liosel     , lhsel     ,
     &            lrsel     , lisel       , lnsel      , lfltsel
       logical    lmetr     , ltscl       , ldscl      , lbrdi     ,
     &            lnorm
       logical    open07    , open15      , open40     , open11    ,
     &            open11f   , open35
       logical    lprn      , lcpl        , lorb       , lbeth     ,
     &            letyp     , lptyp       , lrtyp      , lhtyp     ,
     &            lityp     , lstyp       , lltyp
C-----------------------------------------------------------------------
       integer    imetr(ndmet)    , iordr(ndlev)     ,
     &            ia(ndlev)       ,
     &            isa(ndlev)      , ila(ndlev)       ,
     &            i1a(ndtrn)      , i2a(ndtrn)       ,
     &            npla(ndlev)     , ipla(ndmet,ndlev)
       integer    ietrn(ndtrn)    , iptrn(ndtrn)     ,
     &            irtrn(ndtrn)    , ihtrn(ndtrn)     ,
     &            iitrn(ndtrn)    , iltrn(ndtrn)     , istrn(ndtrn) ,
     &            ie1a(ndtrn)     , ie2a(ndtrn)      ,
     &            ip1a(ndtrn)     , ip2a(ndtrn)      ,
     &            ia1a(ndtrn)     , ia2a(ndtrn)      ,
     &            il1a(ndlev)     , il2a(ndlev)      ,
     &            is1a(ndlev)     , is2a(ndlev)
       integer    npix(ndwvl)
C-----------------------------------------------------------------------
       real*8    scef(nvmax)     , bwnoa(ndmet)     , prtwta(ndmet) ,
     &           tine(ndtem)     , tinp(ndtem)      , tinh(ndtem)   ,
     &           dine(ndden)     , dinp(ndden)      ,
     &           aa(ndtrn)       , aval(ndtrn)      , auga(ndtrn)   ,
     &           xja(ndlev)      , wa(ndlev)        , er(ndlev)     ,
     &           xia(ndlev)      , zpla(ndmet,ndlev), wvla(ndlev)
       real*8    scom(nvmax,ndtrn)
       real*8    beth(ndtrn)     , qdn(ndqdn)       ,
     &           qdorb((ndqdn*(ndqdn+1))/2)
       real*8    wvmin(ndwvl)     , wvmax(ndwvl)
C-----------------------------------------------------------------------
       real*8    ratpia(ndden,ndmet)     , ratmia(ndden,ndmet)
       real*8    stckm(ndmet,ndtem,ndden)      ,
     &           stvrm(ndmet,ndtem,ndden,ndmet) ,
     &           stvhm(ndmet,ndtem,ndden,ndmet) ,
     &           stvim(ndmet,ndtem,ndden,ndmet)
C-----------------------------------------------------------------------
       real*4    stvr(ndlev,ndtem,ndden,ndmet) ,
     &           stvh(ndlev,ndtem,ndden,ndmet) ,
     &           stvi(ndlev,ndtem,ndden,ndmet)
       real*4    stack(ndlev,ndmet,ndtem,ndden)
C-----------------------------------------------------------------------
       character cstrga(ndlev)*18 , tcode(ndtrn)*1
       character cpla(ndlev)*1    , cprta(ndmet)*9
C-----------------------------------------------------------------------
       logical   lbseta(ndmet)
       logical   lsseta(ndmet,ndmet)   , lss04a(ndlev,ndmet)
       logical   ltied(ndlev)          , lqdorb((ndqdn*(ndqdn+1))/2)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Set machine dependant ADAS configuration values
C-----------------------------------------------------------------------

      call xx0000
      call xxname(user)
      call xxdate(date)
      call xxadas(cadas)

      ltick = .TRUE.

      iunt27 = 7
      open27 = .FALSE.

C-----------------------------------------------------------------------
C  Input screen
C-----------------------------------------------------------------------

 100  continue

      read(pipein,'(A)')reply

      method = reply

      if (method(1:6).EQ.'DRIVER') then

C-----------------------------------------------------------------------
C  Open driver and fetch in control data
C-----------------------------------------------------------------------

         read(pipein,'(A)')dsn42

         call xxdata_42(dsn42   ,
     &                  ndtem   , ndden   , ndwvl   , ndpix   , ndmet ,
     &                  dsnin   , dsnexp  , dsnflt  ,
     &                  dsnpec  , dsnfet  , dsnplt  , dsnpltf ,
     &                  celem   , iz0     , iz1     , bwno1   ,
     &                  lnorm   , nmet    , imetr   ,
     &                  liosel  , lhsel   , lrsel   , lisel   ,
     &                  lnsel   , lpsel   , zeff    ,
     &                  lmetr   , ltscl   , ldscl   , lbrdi   ,
     &                  maxt    , maxd    , nwvl    ,
     &                  tine    , tinp    , tinh    , dine    , dinp  ,
     &                  npix    , wvmin   , wvmax   , amin
     &                 )

C        filenames - input and output

         write(pipeou,'(A80)')dsnin
         call xxflsh(pipeou)
         write(pipeou,'(A80)')dsnexp
         call xxflsh(pipeou)
         write(pipeou,'(A80)')dsnflt
         call xxflsh(pipeou)

         write(pipeou,'(A80)')dsnpec
         call xxflsh(pipeou)
         write(pipeou,'(A80)')dsnfet
         call xxflsh(pipeou)
         write(pipeou,'(A80)')dsnplt
         call xxflsh(pipeou)
         write(pipeou,'(A80)')dsnpltf
         call xxflsh(pipeou)

C        temperatures

         write(pipeou,*)maxt
         call xxflsh(pipeou)
         if (ltscl) then
            fmul = iz1*iz1 / 1.16054D4
         else
            fmul = 1.0D0
         endif
         do i=1, maxt
           write(pipeou,*)fmul*tine(i)
           call xxflsh(pipeou)
         end do
         do i=1, maxt
           write(pipeou,*)fmul*tinp(i)
           call xxflsh(pipeou)
         end do
         do i=1, maxt
           write(pipeou,*)fmul*tinh(i)
           call xxflsh(pipeou)
         end do

C        densities

         write(pipeou,*)maxd
         call xxflsh(pipeou)
         if (ldscl) then
            fmul = iz1**7
         else
            fmul = 1.0D0
         endif
         do i=1, maxd
           write(pipeou,*)fmul*dine(i)
           call xxflsh(pipeou)
         end do
         do i=1, maxd
           write(pipeou,*)fmul*dinp(i)
           call xxflsh(pipeou)
         end do

C        metastables

         call xxwlgc(lnorm)
         write(pipeou,*)nmet
         call xxflsh(pipeou)

         do i=1, ndmet
           write(pipeou,*)imetr(i)
           call xxflsh(pipeou)
         end do

C        reactions

         call xxwlgc(liosel)
         call xxwlgc(lhsel)
         call xxwlgc(lrsel)
         call xxwlgc(lisel)
         call xxwlgc(lnsel)
         call xxwlgc(lpsel)

         write(pipeou,*)zeff
         call xxflsh(pipeou)

C        wavelength intervals

         write(pipeou,*)nwvl
         call xxflsh(pipeou)
         do i=1, nwvl
           write(pipeou,*)npix(i)
           call xxflsh(pipeou)
           write(pipeou,*)wvmin(i)
           call xxflsh(pipeou)
           write(pipeou,*)wvmax(i)
           call xxflsh(pipeou)
         end do
         write(pipeou,*)amin
         call xxflsh(pipeou)

      elseif (method(1:4).EQ.'FILE') then

C-----------------------------------------------------------------------
C  More like 208 approach
C-----------------------------------------------------------------------

         dsn42 = ' '

         read(pipein,'(a)') dsnin
         read(pipein,'(a)') dsnexp
         read(pipein,*) ilogic

         if (ilogic.EQ.1) then
            lnsel = .TRUE.
         else
            lnsel = .FALSE.
         endif

         read(pipein,'(a)') dsnflt
         read(pipein,'(a)') ilogic

         if (ilogic.EQ.1) then
            lfltsel = .TRUE.
         else
            lfltsel = .FALSE.
         endif

      else

         write(i4unit(-1),*)'Error in type of input'
         stop

      endif


C-----------------------------------------------------------------------
C  Processing Screen
C-----------------------------------------------------------------------

 200  continue

      read(pipein,'(A)')reply

      if (reply(1:4).EQ.'MENU') goto 999
      if (reply(1:6).EQ.'CANCEL') goto 100

      if (reply(1:8).EQ.'CONTINUE') then

         read(pipein,'(A)') title

         read(pipein,*) ifout
         read(pipein,*) maxt
         read(pipein,*) (tine(i),i=1,ndtem)
         read(pipein,*) (tinp(i),i=1,ndtem)
         read(pipein,*) (tinh(i),i=1,ndtem)

         read(pipein,*) idout
         read(pipein,*) maxd
         read(pipein,*) (dine(i),i=1,ndden)
         read(pipein,*) (dinp(i),i=1,ndden)

         read(pipein,*) nmet
         read(pipein,*) (imetr(i),i=1,ndmet)

         liosel = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) liosel = .TRUE.

         lhsel = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) lhsel = .TRUE.

         lrsel = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) lrsel = .TRUE.

         lisel = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) lisel = .TRUE.

         lnsel = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) lnsel = .TRUE.

         lpsel = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) lpsel = .TRUE.

         read(pipein,*) zeff

         lnorm = .FALSE.
         read(pipein,*) ilogic
         if (ilogic.EQ.one) lnorm = .TRUE.

         read(pipein,*) nwvl
         read(pipein,*) (npix(i),i=1,ndwvl)
         read(pipein,*) (wvmin(i),i=1,ndwvl)
         read(pipein,*) (wvmax(i),i=1,ndwvl)
         read(pipein,*) amin

      endif


C-----------------------------------------------------------------------
C Population calculation
C
C  - read adf04 data
C  - sort transitions into transition/recombination types
C  - calculate level energies relative to level 1 & ionisation pot. in ryds
C  - calculate level populations
C
C-----------------------------------------------------------------------

       open( unit=iunt09 , file=dsnin  , status='old' )

       itieactn = 0

       call xxdata_04( iunt09 ,
     &                 ndlev  , ndtrn , ndmet   , ndqdn , nvmax ,
     &                 celem  , iz    , iz0     , iz1   , bwno2 ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )

       close(iunt09)

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti ,
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a
     &            )


      if (bwno1 .ne. bwno2) then
         write(i4unit(-1),*) 'Mismatch of Ion. Pot. between adf04 and',
     &              ' adf42 driver file - using driver file Ion. Pot.'
         bwnoa(1) = bwno1
      endif
      bwno = bwno1

      call xxeryd( bwno, il, wa, er, xia )

      if (zeff.GT.0.0) then
         lzsel = .TRUE.
      else
         lzsel = .FALSE.
         zeff  = 1.0D0
      endif

      call bxcoef(ndlev  , ndtrn  , ndtem  , ndden  , ndmet ,
     &            nmet   , imetr  , nord   , iordr  ,
     &            maxt   , tine   , tinp   , tinh   , ifout ,
     &            maxd   , dine   , dinp   , idout  ,
     &            lpsel  , lzsel  , liosel ,
     &            lhsel  , lrsel  , lisel  , lnsel  ,
     &            iz     , iz0    , iz1    ,
     &            npl    , bwno   , bwnoa  , prtwta ,
     &            npla   , ipla   , zpla   , nplr   , npli  ,
     &            il     , maxlev , xja    , wa     , zeff  ,
     &            xia    , er     ,
     &            icnte  , icntp  , icntr  , icnth  , icnti ,
     &            icntl  , icnts  ,
     &            ietrn  , iptrn  , irtrn  , ihtrn  , iitrn ,
     &            iltrn  , istrn  ,
     &            ie1a   , ie2a   , ip1a   , ip2a   , aa    ,
     &            ia1a   , ia2a   , auga   ,
     &            il1a   , il2a   , wvla   ,
     &            is1a   , is2a   , lss04a ,
     &            i1a    , i2a    ,
     &            nv     , scef   , scom   ,
     &            dsnexp , dsnin  , iunt27 , open27 ,
     &            stckm  , stvr   , stvi   , stvh   ,
     &            stvrm  , stvim  , stvhm  , stack  ,
     &            ltick
     &           )


C-----------------------------------------------------------------------
C Output screen
C-----------------------------------------------------------------------

  300  continue

      read(pipein,'(A)')reply

      if (reply(1:4).EQ.'MENU') goto 999
      if (reply(1:6).EQ.'CANCEL') goto 200

      if (reply(1:8).EQ.'CONTINUE') then

         open07 = .FALSE.
         open15 = .FALSE.
         open40 = .FALSE.
         open11 = .FALSE.
         open11f = .FALSE.

         read(pipein,*)ilogic
         if (ilogic.eq.1) open07  = .TRUE.
         read(pipein,*)ilogic
         if (ilogic.eq.1) open15  = .TRUE.
         read(pipein,*)ilogic
         if (ilogic.eq.1) open40  = .TRUE.
         read(pipein,*)ilogic
         if (ilogic.eq.1) open11  = .TRUE.
         read(pipein,*)ilogic
         if (ilogic.eq.1) open11f = .TRUE.

         read(pipein,'(a)')dsnpap
         read(pipein,'(a)')dsnpec
         read(pipein,'(a)')dsnfet
         read(pipein,'(a)')dsnplt
         read(pipein,'(a)')dsnpltf

         if (open07) open(unit=iunt07, file=dsnpap)
         if (open15) open(unit=iunt15, file=dsnpec)
         if (open40) open(unit=iunt40, file=dsnfet)
         if (open11) open(unit=iunt11, file=dsnplt)
         if (open11f) open(unit=iunt11f, file=dsnpltf)

      endif

C Set inputs - ratpia and ratmia should not be necessary but
C set first metastable to 1 as in 208 run for GCR/PEC/SXB production.
C
C Open adf35 filter file if present

      open35 = .FALSE.
      if (dsnflt.NE.'NULL') then
         open(unit=iunt35 , file=dsnflt, status='UNKNOWN')
         open35 = .TRUE.
      endif

      if (liosel) then
          npl3 = max0(npl,nplr)
      endif

      do i = 1, maxd
         ratpia(i,1) = 1.0D0
         ratmia(i,1) = 1.0D0
         do j = 2, ndmet
           ratpia(i,j) = 0.0D0
           ratmia(i,j) = 0.0D0
         end do
      end do

      call hapecf( iunt15 , iunt40 , iunt11 , iunt11f, iunt35  ,
     &             open15 , open40 , open11 , open11f, open35  ,
     &             dsnflt ,
     &             ndlev  , ndtrn  , ndtem  , ndden  , ndmet   ,
     &             ndwvl  , ndpix  ,
     &             nmet   , imetr  , nord   , iordr  ,
     &             maxt   , tine   , tinp   ,
     &             maxd   , dine   ,
     &             lpsel  , lzsel  , liosel ,
     &             lhsel  , lrsel  , lisel  , lnsel  ,
     &             iz     , iz0    , iz1    ,
     &             npl    , bwno   ,
     &             nplr   , npli   , npl3   ,
     &             dsnin  , dsnexp ,
     &             celem  , date   , user   ,
     &             il     ,
     &             ia     , cstrga , isa    , ila   , xja , wa  ,
     &             icnte  , icntr  , icnth  , icnti ,
     &             ietrn  ,
     &             ie1a   , ie2a   , aa     ,
     &             lnorm  ,
     &             stckm  , stvr   , stvi   , stvh  ,
     &             stvrm  , stvim  , stvhm  ,
     &             ratpia , ratmia , stack  ,
     &             lsseta , lss04a ,
     &             nwvl   , npix   , wvmin  , wvmax , amin
     &           )


C Must put something out to paper.txt

      if (open07) then

         call haout0( iunt07   , cadas   , method  , dsn42   , title ,
     &                ndtem    , ndden   , ndwvl   , ndmet   ,
     &                dsnin    , dsnexp  , dsnflt  ,
     &                dsnpec   , dsnfet  , dsnplt  , dsnpltf ,
     &                celem    , iz0     , iz1     , bwno1   ,
     &                lnorm    , nmet    , imetr   ,
     &                liosel   , lhsel   , lrsel   , lisel   ,
     &                lnsel    , lpsel   , zeff    ,
     &                maxt     , maxd    , nwvl    ,
     &                tine     , tinp    , tinh    , dine    , dinp  ,
     &                npix     , wvmin   , wvmax   , amin
     &              )

      endif

C Close the I/O files

      if (open07)  close(iunt07)
      if (open15)  close(iunt15)
      if (open40)  close(iunt40)
      if (open11)  close(iunt11)
      if (open11f) close(iunt11f)
      if (open35)  close(iunt35)

C Go back to output screen when finished

      goto 300

C-----------------------------------------------------------------------
C Finish up by waiting for a final signal from IDL
C-----------------------------------------------------------------------

 999  continue
      read(pipein,*)ilogic

C-----------------------------------------------------------------------
      end
