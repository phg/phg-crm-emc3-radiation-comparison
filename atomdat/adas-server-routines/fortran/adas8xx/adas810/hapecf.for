      subroutine hapecf( iunt15 , iunt40 , iunt11 , iunt11f , iunt35  ,
     &                   open15 , open40 , open11 , open11f , open35  ,
     &                   dsn35  ,
     &                   ndlev  , ndtrn  , ndtem  , ndden   , ndmet   ,
     &                   ndwvl  , ndpix  ,
     &                   nmet   , imetr  , nord   , iordr   ,
     &                   maxt   , teva   , tpva   ,
     &                   maxd   , densa  ,
     &                   lpsel  , lzsel  , liosel ,
     &                   lhsel  , lrsel  , lisel  , lnsel   ,
     &                   iz     , iz0    , iz1    ,
     &                   npl    , bwno   ,
     &                   nplr   , npli   , npl3   ,
     &                   dsninc , dsnexp ,
     &                   titled , date   , user   ,
     &                   il     ,
     &                   ia     , cstrga , isa    , ila     , xja , wa ,
     &                   icnte  , icntr  , icnth  , icnti   ,
     &                   ietrn  ,
     &                   ie1a   , ie2a   , aa     ,
     &                   lnorm  ,
     &                   stckm  , stvr   , stvi   , stvh    ,
     &                   stvrm  , stvim  , stvhm  ,
     &                   ratpia , ratmia , stack  ,
     &                   lsseta , lss04a ,
     &                   nwvl   , npix   , wvmin  , wvmax   , avlt
     &                 )
      implicit none
c-----------------------------------------------------------------------
c
c  ******************** fortran77 subroutine: hapecf *******************
c
c  purpose:  to prepare pec, envelope feature f-pec, plt and plt-filter
c            passing files for diagnostic use.
c
c  calling program: adas810
c
c
c  subroutine:
c
c  input : (i*4)  iunt15  = output unit for adf15 pec results
c  input : (i*4)  iunt40  = output unit for adf40 fpec results
c  input : (i*4)  iunt11  = output unit for adf11 plt results
c  input : (i*4)  iunt11f = output unit for adf41 filtered plt results
c  input : (i*4)  iunt35  = input  unit for adf35 filter file
c
c  input : (i*4)  ndlev   = maximum number of levels allowed
c  input : (i*4)  ndtrn   = maximum number of transitions allowed
c  input : (i*4)  ndtem   = maximum number of temperatures allowed
c  input : (i*4)  ndden   = maximum number of densities allowed
c  input : (i*4)  ndmet   = maximum number of metastables allowed
c  input : (i*4)  ndwvl   = maximum number of wavelength intervals
c  input : (i*4)  ndpix   = maximum no. of pixels in a wvl. interval
c
c  input : (i*4)  nmet    = number of metastables levels: 1<=nmet<=ndmet
c  input : (i*4)  imetr() = index of metastable in complete level list
c  input : (i*4)  nord    = number of ordinary levels ('il' - 'nmet')
c  input : (i*4)  iordr() = index of ordinary levels in complete level
c                           list.
c
c  input : (i*4)  maxt    = number of input temperatures ( 1 -> 'ndtem')
c  input : (i*4)  maxd    = number of input densities ( 1 -> 'ndden')
c  input : (r*8)  teva()  = electron temperatures (units: ev)
c  input : (r*8)  tpva()  = ion temperatures (units: ev)
c  input : (r*8)  densa() = electron densities  (units: cm-3)
c
c  input : (l*4)  lpsel    = .true.  => include proton collisions
c                          = .false. =>do not include proton collisions
c  input : (l*4)  lzsel    = .true.  => scale proton collisions with
c                                       plasma z effective'zeff'.
c                          = .false. => do not scale proton collisions
c                                       with plasma z effective 'zeff'.
c                          (only used if 'lpsel=.true.')
c  input   (l*4)  liosel  = .true.  => include ionisation rates
c                         = .false. => do not include ionisation rates
c                            for recom and  3-body
c  input   (l*4)  lhsel   = .true.  => include charge transfer from
c                                      neutral hydrogren.
c                         = .false. => do not include charge transfer
c                                      from neutral hydrogren.
c  input   (l*4)  lrsel   = .true.  => include free electron
c                                      recombination.
c                         = .false. => do not include free electron
c                                      recombination.
c  input   (l*4)  lisel   = .true.  => include electron impact
c                                      ionisation.
c                         = .false. => do not include free electron
c                                      recombination.
c  input : (l*4)  lnsel    = .true.  => include projected bundle-n data
c                                        from datafile if available
c                          = .false. => do not include projected
c                                       bundle-n data
c
c  input : (i*4)  iz      =  recombined ion charge read
c  input : (i*4)  iz0     =         nuclear charge read
c  input : (i*4)  iz1     = recombining ion charge read
c                           (note: iz1 should equal iz+1)
c
c  input : (i*4)  npl      = no. of metastables of(z+1) ion accessed
c                              by excited state ionisation in copase
c                              file with ionisation potentials given
c                              on the first data line
c  input : (r*8)  bwno    = ionisation potential (cm-1)
c
c  input : (i*4)  nplr     = no. of active metastables of (z+1) ion
c  input : (i*4)  npli     = no. of active metastables of (z-1) ion
c  input : (i*4)  npl3     = no. of active metastables of (z+1) ion with
c                            three-body recombination on.
c
c  input : (c*44) dsninc  = input copase data set name
c  input : (c*80) dsnexp  = input expansion file
c
c  input : (c*3)  titled  = element symbol.
c  input : (c*8)  date    = current date.
c  input : (c*30) user    = full name of author.
c
c  input : (i*4)  il      = number of energy levels
c
c  input : (i*4)  ia()    = energy level index number
c  input : (c*18) cstrga()= nomenclature/configuration for level 'ia()'
c  input : (i*4)  isa()   = multiplicity for level 'ia()'
c                           note: (isa-1)/2 = quantum number (s)
c  input : (i*4)  ila()   = quantum number (l) for level 'ia()'
c  input : (r*8)  xja()   = quantum number (j-value) for level 'ia()'
c                           note: (2*xja)+1 = statistical weight
c  input : (r*8)  wa()    = energy relative to level 1 (cm-1)
c                           dimension: level index
c
c
c  input : (i*4)  icnte   = number of electron impact transitions input
c  input : (i*4)  icntr   = number of free electron recombinations input
c  input : (i*4)  icnth   = no. of charge exchange recombinations input
c  input : (i*4)  icnti   = number of lower stage ionisations      input
c
c  input : (i*4)  ietrn() = electron impact transition:
c                           index values in main transition arrays which
c  input : (i*4)  ie1a()  = electron impact transition:
c                            lower energy level index
c  input : (i*4)  ie2a()  = electron impact transition:
c                            upper energy level index
c  input : (r*8)  aa()    = electron impact transition: a-value (sec-1)
c
c  input : (l*4)  lnorm   =.true.  => if nmet=1 then various
c                                     emissivity output files
c                                     normalised to stage tot.populatn.
c                                     (** norm type = t)
c                         =.false. => otherwise normalise to identified
c                                     metastable populations.
c                                      (** norm type = m)
c
c
c  input : (r*8) stckm(,,) = metastable populations stack
c                             1st dimension: metastable index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c  input : (r*4) stvr(,,,) = free electron recombination coefficients
c                             1st dimension: ordinary level index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c                             4th dimension: parent index
c  input : (r*4) stvi(,,,) = electron impact ionisation  coefficients
c                             1st dimension: ordinary level index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c                             4th dimension: parent index
c  input : (r*4) stvh(,,,) =  charge exchange coefficients
c                             1st dimension: ordinary level index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c  input : (r*8) stvrm(,,,)= metastable free electron recombination
c                            coefficients.
c                             1st dimension: metastable index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c                             4th dimension: parent index
c  input : (r*8) stvim(,,,)= metastable electron impact ionisation
c                            coefficients.
c                             1st dimension: metastable index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c                             4th dimension: parent index
c  input : (r*8) stvhm(,,,)= metastable charge exchange coefficients
c                             1st dimension: metastable index
c                             2nd dimension: temperature index
c                             3rd dimension: density index
c  input : (r*8) ratpia(,)   = ratio ( n(z+1)/n(z)  stage abundancies )
c                             1st dimension: temp/dens index
c                             2nd dimension: parent index
c  input : (r*8) ratmia(,)   = ratio ( n(z-1)/n(z)  stage abundancies )
c                             1st dimension: temp/dens index
c                             2nd dimension: parent index
c  input : (r*4) stack(,,,)= population dependence
c                             1st dimension: ordinary level index
c                             2nd dimension: metastable index
c                             3rd dimension: temperature index
c                             4th dimension: density index
c  input : (l*4) lsseta(,) = .true.  - met. ionis rate set in b8gets
c                            .false.- met. ionis rate not set in b8gets
c                             1st dimension: (z) ion metastable index
c                             2nd dimension: (z+1) ion metastable index
c  input : (l*4) lss04a(,) = .true. => ionis. rate set in adf04 file:
c                            .false.=> not set in adf04 file
c                             1st dim: level index
c                             2nd dim: parent metastable index
c
c  input : (i*4) nwvl      = number of wavelength intervals
c  input : (i*4) npix()    = number of pixels in each wvln. interval
c  input : (r*8) wvmin()   = minimum  wvln. (a) for each interval
c  input : (r*8) wvmax()   = maximum  wvln. (a) for each interval
c
c          (r*8) avlt      = lower limit of a-values for pec & f-pec
c
c          (i*4) notrn     = parameter = maximum number of transitions
c          (i*4) ndpec     = parameter = maximum number of pecs per
c                                        metastable for output
c          (i*4) metcnt    = counter of pecs for each metastable
c
c          (i*4) i4unit    = function (see routine selection below)
c
c          (i*4) i         = general use
c          (i*4) j         = general use
c          (i*4) k         = general use
c          (i*4) l         = general use
c
c          (r*8) dum1      = general use- dummy
c          (r*8) dum2      = general use- dummy
c          (r*8) dum3      = general use- dummy
c          (r*8) pec()     = renormalised pec
c                            1st dimension: temperature index
c
c routines:
c          -------------------------------------------------------------
c          hawvrg     adas      check for spectrum line in wvln.interval
c          hapixv     adas      doppler broaden line over pixel range
c          haout1     adas      writes plt and plt-filter output to files
c          b8norm     adas      perform  stage population normalisation
c          b8corp     adas      'fixes' low te problem in rec. data of pecs
c          i4unit     adas      fetch unit number for output of messages
c          xxordr     adas      sorts a real*8 array and its index array
c          xxeiam     adas      return the atomic mass of an element
c          xxmkrc     adas      make root connection vector
c          xxmkrp     adas      make root partition text lines for output
c          xxwcmt_15  adas      writes structured comments to adf15 dataset
c          xxwcmt_40  adas      writes structured comments to adf40 dataset
c
c author:  h. p. summers, university of strathclyde
c          tel: 0141-548-4196
c
c date:    24/04/02
c
c
c  version  : 1.1
c  date     : 24-02-2003
c  modified : H P Summers
c              - first version.
c
c  version  : 1.2
c  date     : 12-11-2003
c  modified : Martin O'Mullane
c              - trap plt and pltnfl for values below machine precision.
c              - increased number of transitions in line with 801/ifgpp.
c
c  version  : 1.3
c  date     : 05-12-2003
c  modified : Thomas Puetterich
c              - did not write f-pec file as per specification.
c
c  version  : 1.4
c  date     : 25-02-2004
c  modified : Martin O'Mullane
c              - increased number of transitions in line with 801/ifgpp.
c              - change behaviour of plt and filtered plt. no
c                limitations of wavelength or a-value to iunt11. an
c                adf35 filter is now an input and write plt filtered
c                by this to iunt11f.
c
c  version  : 1.5
c  date     : 26-05-2006
c  modified : Hugh Summers
c              - altered output on header lines for superstage
c                 compatibility.
c              - altered strategy for power ranking of emissivities
c              - altered comment lines for superstage compatibility
c                and field key reading of comments.
c
c  version  : 1.6
c  date     : 20-02-2007
c  modified : Martin O'Mullane
c              - Do not write comments to non-open units.
c              - Bring interactive version into line with
c                latest version of adf15 defintion (superstages).
c
c  version  : 1.7                          
c  date     : 05-12-2008
c  modified : Adam Foster
c              - change ndpec 1500 -> 380000 
c                (required for heavy species calculations)
c              - change ndpotrn 20000 -> 380000 
c              - removed reference to unused subroutine hawinf in 
c                comments
c
c  version  : 1.8
c  date     : 26-03-2009
c  modified : Adam Foster
c              - increased length of ctrans string to 35 characters
c
c  version  : 1.9                         
c  date     : 01-08-2011
c  modified : Martin O'Mullane  
c              - Add tpva as separate ion temperature input.
c              - Set tev to this input ion temperature in the call to hapixv.
c              - Write correct number of blocks since no recombination
c                fpecs are calculated yet.
c 
c  version  : 1.10                         
c  date     : 24-04-2019
c  modified : Martin O'Mullane  
c              - Filename lengths are defined by argument length.
c              - Remove redundant dsnin argument to haout1.
c
c-----------------------------------------------------------------------
      integer   notrn    , ndpec
      integer   ntdim    , nddim   ,  npxdim  , nwgdim  , nmdim
      integer   ncomments
      integer   max_pecout
      integer   ndedge   , ndeng
      integer   idcmt    , idfld   , idsyn    , idion   , idopt
      integer   ndcnct   , ndstack , ndstore
C-----------------------------------------------------------------------
      real*8    fcrit
C-----------------------------------------------------------------------
      parameter ( notrn = 380000  , ndpec = 380000  )
      parameter ( ntdim = 24      , nddim = 24     , npxdim = 1024  )
      parameter ( nwgdim = 5      , nmdim = 4      , ncomments = 21 )
      parameter ( fcrit  = 1.0D-6 , max_pecout = 50   )
      parameter ( ndedge = 6      , ndeng      = 2500 )
      parameter ( idcmt = 500     , idfld  = 30  , idsyn = 1 )
      parameter ( idion = 25      , idopt  = 10 )
      parameter ( ndcnct = 100     , ndstack = 40 )
      parameter ( ndstore = 3*nmdim*ndpec )
C-----------------------------------------------------------------------
      integer   i4unit
      integer   ndlev     , ndtem      , ndden   , ndmet   , ndtrn  ,
     &          ndwvl     , ndpix
      integer   iunt11    , iunt15     , iunt40  , iunt11f , iunt35 ,
     &          npl       , nplr       , npli    , npl3    ,
     &          iz        , iz0        , iz1     ,
     &          il        , nmet       , nord    ,
     &          maxt      , maxd       , icnte   , icntr   , icnti  ,
     &          icnth
      integer   iw        , id         , ndpec_new         , ifail
      integer   i         , j          , ip      , k       , l      ,
     &          ic        , ii         , it      , ine     , ipix   ,
     &          iwg       , ind1       , ind2    ,
     &          isel      , iulev      , kstrn   , io      ,
     &          isel4     ,
     &          isel1_out , len_cterm  ,
     &          index1    , index2     , metcnt  , iedge   , ieng
      integer   nwvl      , iwvrg      , nblocks , isel_f
      integer   ncmt      , nfld_15    , nfld_40 , nion    , nopt
      integer   iptnl     , is         , nspb    , nspp
      integer   ita_max   , isa_max    , ila_max
      integer   ncnct     , ncptn_stack
C-----------------------------------------------------------------------
      real*8    avlt
      real*8    bwno      , dum1       , dum2    , dum3
      real*8    sum1
      real*8    wvl
      real*8    amssno    , cpixmx     , tev     , de
      real*8    val       , valnfl     , frac    , tmenergy
      real*8    pecpow    , xja_max
C-----------------------------------------------------------------------
      character titled*3         , dsninc*(*)
      character date*8           , dsnexp*(*)    , trans*29
      character user*30
      character name*13          , xfelem*12     , dsn35*(*)
      character dsnpt*120
      character popcode*7        , esym*2
      character tabul_15*60      , units_15*60
      character tabul_40*60      , units_40*60
      character code*7           , producer*30
      character cdash*80
      character film15_exc*23    , film15_rec*23
      character film40_exc*29    , film40_rec*29
      character fmt01*9          , fmt02*63      , fmt03*31
C-----------------------------------------------------------------------
      logical   lnorm
      logical   lrsel  , lisel  , lhsel  , liosel
      logical   lpsel  , lzsel  , lnsel
      logical   lwvrg  , lup
      logical   open15 , open40 , open11 , open11f  , open35
      logical   lroot  , lsuper
C-----------------------------------------------------------------------
      integer   ia(ndlev)        , isa(ndlev)       , ila(ndlev)
      integer   imetr(ndmet)     , iordr(ndlev)
      integer   ietrn(ndtrn)     , ie1a(ndtrn)      , ie2a(ndtrn)
      integer   ima(notrn)
      integer   npix(ndwvl)
      integer   indx_peca(notrn) , inde_peca(notrn) , invdx_peca(notrn)
      integer   isyn_15(idfld)   , isyn_40(idfld)
      integer   itg(ndstore)     , ipr(ndstore)     , iwr(ndstore)
      integer   icnctv(ndcnct)
      integer   ispbr(ndstore)   , isppr(ndstore)   , iszr(ndstore)
      integer   ispbr_f(ndwvl)   , isppr_f(ndwvl)
      integer   ilzr_f(ndwvl)    , ihzr_f(ndwvl)
      integer   irg_f(ndwvl)     , ipr_f(ndwvl)     , iwr_f(ndwvl)
C-----------------------------------------------------------------------
      real*8    pec(50)           , sum(50)          , peca(nddim,ntdim)
      real*8    xja(ndlev)        , teva(ndtem)      , tpva(ndtem)   ,
     &          densa(ndden)
      real*8    wa(ndlev)         , aa(ndtrn)        , wvla(notrn)
      real*8    peca_mx(notrn)
      real*8    wvl_sort(max_pecout)          , wtrans(ndstore)
      real*8    stckm(ndmet,ndtem,ndden)
      real*8    stvrm(ndmet,ndtem,ndden,ndmet) ,
     &          stvim(ndmet,ndtem,ndden,ndmet)
      real*8    stvhm(ndmet,ndtem,ndden,ndmet)
      real*8    ratpia(ndden,ndmet)  , ratmia(ndden,ndmet)
      real*8    wvmin(ndwvl)         , wvmax(ndwvl)
      real*8    cpixa(npxdim)        , cpixmxa(nwgdim,ntdim)
      real*8    plt(ntdim,nddim)     , pltnfl(ntdim,nddim)
      real*8    tel(ndtem)           , fnel(ndden)
      real*8    edge(ndedge)         , energy(ndeng) , fraction(ndeng)
      real*8    pow_f(ndwvl)
      real*8    wvrg_sort(ndwvl)     ,powf_sort(ndwvl)
C-----------------------------------------------------------------------
      real*4    stvr(ndlev,ndtem,ndden,ndmet) ,
     &          stvi(ndlev,ndtem,ndden,ndmet) ,
     &          stvh(ndlev,ndtem,ndden,ndmet)
      real*4    stack(ndlev,ndmet,ndtem,ndden)
      real*4    f_cpixa(npxdim,ntdim,nddim,nwgdim)
C-----------------------------------------------------------------------
      character comments(ncomments)*80
      character cstrga(ndlev)*18     , cterm(ndlev)*14   ,
     &          config(ndlev)*19     ,
     &          type(4)*5
      character fldk_15(idfld,idsyn)*40
      character fldk_40(idfld,idsyn)*40
      character cion(idion)*5        , copt(idopt)*6
      character cwvla(ndpec)*10      , ctrans(max_pecout)*35
      character cptn_stack(ndstack)*80
      character ctype(ndstore)*5
      character ctype_f(ndwvl)*5
C-----------------------------------------------------------------------
      logical   lsseta(ndmet,ndmet)  , lss04a(ndlev,ndmet)
      logical   lfld_15(idfld)       , lfld_40(idfld)
      logical   lion(idion)          , lopt(idopt)
C-----------------------------------------------------------------------
      data      type/'excit','recom','chexc','ionis'/
      data      film15_exc/'pl=0*:ss=**:pb=**:sz=**'/
      data      film15_rec/'pl=0*:ss=**:pp=**:sz=**'/
      data      film40_exc/'pl=0*:ss=**:pb=**:lz=**:hz=**'/
      data      film40_rec/'pl=0*:ss=**:pp=**:lz=**:hz=**'/
      data        cdash/'C----------------------------------------------
     &---------------------------------'/
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C Check external dimensions against internal dimension parameters
C-----------------------------------------------------------------------

      if((ndtem.ne.ntdim).or.(ndden.ne.nddim).or.(ndpix.ne.npxdim).or.
     &   (ndwvl.ne.nwgdim) .or. (ndtrn.ne.notrn) .or.
     &   (ndmet.ne.nmdim)) then
         write(i4unit(-1),2011)
         write(i4unit(-1),2013)
         write(i4unit(-1),*)ndtem,ndden,ndpix,ndwvl,ndtrn,ndmet
         write(i4unit(-1),*)ntdim,nddim,npxdim,nwgdim,notrn,nmdim
         stop
      endif

c-----------------------------------------------------------------------
c  field key declarations
c-----------------------------------------------------------------------

       nfld_15 = 23

       do i=1,nfld_15
         isyn_15(i) = 1
         lfld_15(i) = .true.
       enddo

       fldk_15(1,1)  = 'root partition information              '
       fldk_15(2,1)  = 'nuclear charge                          '
       fldk_15(3,1)  = 'ion charge+1                            '
       fldk_15(4,1)  = 'population calculation information      '
       fldk_15(5,1)  = 'population processing code              '
       fldk_15(6,1)  = 'adf04 source file                       '
       fldk_15(7,1)  = 'adf18/a17_p208 file                     '
       fldk_15(8,1)  =  'meta. ion. coeff. selector             '
       fldk_15(9,1)  = 'options                                 '
       fldk_15(10,1) = 'energy levels                           '
       fldk_15(11,1) = 'lv      configuration                   '
       fldk_15(12,1) = 'superstage partition information        '
       fldk_15(13,1) = 'element symbol                          '
       fldk_15(14,1) = 'parent template                         '
       fldk_15(15,1) = 'partition level                         '
       fldk_15(16,1) = 'superstage label                        '
       fldk_15(17,1) = 'tabulation                              '
       fldk_15(18,1) = 'units                                   '
       fldk_15(19,1) = 'photon emissivity atomic transitions    '
       fldk_15(20,1) = 'isel  wvle                              '
       fldk_15(21,1) = 'code                                    '
       fldk_15(22,1) = 'producer                                '
       fldk_15(23,1) = 'date                                    '

       nfld_40 = 23

       do i=1,nfld_40
         isyn_40(i) = 1
         lfld_40(i) = .true.
       enddo

       fldk_40(1,1)  = 'root partition information              '
       fldk_40(2,1)  = 'nuclear charge                          '
       fldk_40(3,1)  = 'ion charge+1                            '
       fldk_40(4,1)  = 'population calculation information      '
       fldk_40(5,1)  = 'population processing code              '
       fldk_40(6,1)  = 'adf04 source file                       '
       fldk_40(7,1)  = 'adf18/a17_p208 file                     '
       fldk_40(8,1)  =  'meta. ion. coeff. selector             '
       fldk_40(9,1)  = 'options                                 '
       fldk_40(10,1) = 'energy levels                           '
       fldk_40(11,1) = 'lv      configuration                   '
       fldk_40(12,1) = 'superstage partition information        '
       fldk_40(13,1) = 'element symbol                          '
       fldk_40(14,1) = 'parent template                         '
       fldk_40(15,1) = 'partition level                         '
       fldk_40(16,1) = 'superstage label                        '
       fldk_40(17,1) = 'tabulation                              '
       fldk_40(18,1) = 'units                                   '
       fldk_40(19,1) = 'feature photon emissivity wvln. ranges  '
       fldk_40(20,1) = 'isel  wvle                              '
       fldk_40(21,1) = 'code                                    '
       fldk_40(22,1) = 'producer                                '
       fldk_40(23,1) = 'date                                    '


       lroot  = .true.
       lsuper = .true.

       nopt = 8

       copt(1) = 'lnorm '
       copt(2) = 'lpsel '
       copt(3) = 'lzsel '
       copt(4) = 'liosel'
       copt(5) = 'lhsel '
       copt(6) = 'lrsel '
       copt(7) = 'lisel '
       copt(8) = 'lnsel '

       lopt(1) = lnorm
       lopt(2) = lpsel
       lopt(3) = lzsel
       lopt(4) = liosel
       lopt(5) = lhsel
       lopt(6) = lrsel
       lopt(7) = lisel
       lopt(8) = lnsel

       if(nmet.gt.1) then
           iptnl = 0
           nspb  = nmet
           nspp  = nplr
       else
           iptnl = 1
           nspb  = 1
           nspp  = 0
       endif

       write(film15_exc(5:5),'(i1)')iptnl
       write(film15_exc(10:11),'(i2)')iz
       write(film15_exc(22:23),'(i2)')iz
       write(film15_rec(5:5),'(i1)')iptnl
       write(film15_rec(10:11),'(i2)')iz
       write(film15_rec(22:23),'(i2)')iz

       write(film40_exc(5:5),'(i1)')iptnl
       write(film40_exc(10:11),'(i2)')iz
       write(film40_exc(22:23),'(i2)')iz
       write(film40_exc(28:29),'(i2)')iz
       write(film40_rec(5:5),'(i1)')iptnl
       write(film40_rec(10:11),'(i2)')iz
       write(film40_rec(22:23),'(i2)')iz
       write(film40_rec(28:29),'(i2)')iz


       nion = 0
       do i=1,nmet
         do ip =1,npl
           nion=nion+1
           write(cion(nion),'(a1,i1,a1,i1,a1)')'(',i,',',ip,')'
           lion(nion)=lss04a(i,ip)
         enddo
       enddo

       esym=titled(1:2)
       popcode  = 'adas810'
       producer = user(1:20)
       code     = 'adas810'
       dsnpt=' '
       is = iz

       tabul_15 = ' '
       tabul_15(1:32) = 'photon emissivity coefft (te,ne)'
       units_15=' '
       units_15(1:45) = 'ph. emis coef(cm^3 s^-1); te (ev); ne (cm^-3)'

       tabul_40 = ' '
       tabul_40(1:40) = 'feature photon emissivity coefft (te,ne)'
       units_40=' '
       units_40(1:54) = 'ph. emis coef(cm^3 s^-1 pixel^-1)'//
     &                  '; te (ev); ne (cm^-3)'

C-----------------------------------------------------------------------
C Read data from adf35 filter
C-----------------------------------------------------------------------


      if (open35) then

         call xxdata_35( iunt35  ,
     &                   ndedge  , ndeng  ,
     &                   iedge   , ieng   ,
     &                   edge    , energy , fraction
     &                 )

      else

         iedge = 0
         ieng  = 0

      endif


C-----------------------------------------------------------------------
C Get element mass number & setup adf04 file member name for header
C-----------------------------------------------------------------------

      call xxeiam(titled(1:2),amssno)

      call xxslen(dsninc,index1,index2)
      if((index2-index1).gt.4) then
          if(dsninc(index2-3:index2).eq.'.dat') then
              index1=max(index2-11,1)
              index2=index2-4
          else
              index1=max(index2-7,1)
          endif
          index1=index1+index(dsninc(index1:index2),'/')
      endif

c------ initialise term parameter maxima

      isa_max=0
      ila_max=0
      xja_max=0.0d0

c-------and identify maximum values for term parameters

      do i = 1, il
        isa_max=max0(isa_max,isa(i))
        ila_max=max0(ila_max,ila(i))
        xja_max=dmax1(xja_max,xja(i))
        config(i)=cstrga(i)//' '
      enddo

c------make the term string format to fit

       call xxfrmt_trm(isa_max,ila_max,xja_max,iw,fmt03)
       len_cterm=len(cterm(1))
       if(iw.gt.len_cterm)then
           fmt03="('  (',i1,')',i1,'(',f5.1,') ')"
           write(i4unit(-1),3002)iw
       endif

c       write(i4unit(-1),'(1a31)')fmt03

       do i=1,il
         cterm(i)=' '
         write(cterm(i)(len_cterm-iw+1:len_cterm),fmt03)
     &                  isa(i),ila(i),xja(i)
       enddo

C-----------------------------------------------------------------------
C Put feature pec output to iunt40 and initialize
C-----------------------------------------------------------------------

       nblocks =  nwvl*nmet

C For future use as we don't write recombination contributions yet
C 
C        if(lrsel) then
C            nblocks=nblocks+nwvl*nplr
C        endif
C        	
C        if(lrsel.and.lhsel) then
C            nblocks=nblocks+nwvl*nplr
C        endif

       if (open40) then

           write(iunt40,2002) nblocks, esym , is

c-----output the root connection vector text lines

           call  xxmkrc( ndcnct   ,
     &                   iz0      , iptnl    ,
     &                   ncnct    , icnctv
     &                 )

           write(iunt40,'(1a80)')'-'//cdash(2:80)
           write(iunt40,'(14i5)')(icnctv(ii),ii=1,ncnct)


c-----output the root partition level text lines

           call xxmkrp( ndstack       ,
     &                  iz0           , iptnl       ,
     &                  ncptn_stack   , cptn_stack
     &                )

           write(iunt40,'(1a80)')'-'//cdash(2:80)
           do ii=1,ncptn_stack
             write(iunt40,'(1a80)') cptn_stack(ii)
           enddo
           write(iunt40,'(1a80)')'-'//cdash(2:80)

       endif

C-----------------------------------------------------------------------
C Start processing the pecs
C-----------------------------------------------------------------------
      isel   = 0
      isel_f = 0

      do iwvrg=1,nwvl
        iwr_f(iwvrg)=iwvrg
        ipr_f(iwvrg)=iwvrg
        pow_f(iwvrg)=0.0d0
        powf_sort(iwvrg)=wvmin(iwvrg)
        wvrg_sort(iwvrg)=wvmin(iwvrg)
      enddo

      do 19 i = 1 , nmet

c      write(i4unit(-1),*)'hapecf: do 19 loop: i=',i


C-----------------------------------------------------------------------
C Zero the feature pixel count accumulation vectors
C-----------------------------------------------------------------------

        do ipix = 1,npxdim
          cpixa(ipix) = 0.0D0
        enddo

        do iwg = 1,nwgdim
          do it=1,ntdim
            cpixmxa(iwg,it) = 0.0D0
            do ine = 1,nddim
              do ipix = 1,npxdim
                f_cpixa(ipix,it,ine,iwg) = 0.0e0
              enddo
            enddo
          enddo
        enddo

        metcnt = 0
          do 18 j = 1 , icnte

C-----------------------------------------------------------------------
C  Limit pecs by A-value and wavelength - check only for 1st metastable
C-----------------------------------------------------------------------
            if(i.eq.1) then

              if ( aa(j) .gt. avlt ) then
                 iulev = ie2a(j)
                  do 16 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        de    = wa(ie2a(j))-wa(ie1a(j))

                        wvl   = 1.0D8/de
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 17

                        metcnt = metcnt+1
                        peca_mx(metcnt)=0.0D0
                        inde_peca(metcnt)=j
                        wvla(metcnt)=wvl

                        do 14 k = 1 , maxd
                          do 13 l=1,maxt
                            pec(l) = aa(j)*stack(kstrn,i,l,k)/densa(k)
                            if(lnorm.and.(nmet.eq.1))then
                               call b8norm( ndlev , ndmet ,
     &                                      nord  ,
     &                                      stack(1,1,l,k),
     &                                      pec(l), dum1  ,
     &                                      dum2  , dum3
     &                                    )
                            endif
                            if(pec(l).le.1.0D-74) pec(l)=1.0D-74
c-----------------------------------------------------------------------
c  revise pec power definition to use sum of metastable contributions
c-----------------------------------------------------------------------
                            if(k.eq.1) then
                              pecpow=0.0d0
                              do ii=1,nmet
                                pecpow=pecpow+stack(kstrn,ii,l,k)
                              enddo
                              pecpow=pecpow*aa(j)/densa(k)
                              peca_mx(metcnt)=dmax1(peca_mx(metcnt),
     &                                              pecpow)
                            endif
                            peca(k,l)=pec(l)
   13                       continue
   14                     continue
                        go to 17
                    endif
   16             continue
              endif
            else
              if(inde_peca(metcnt+1).eq.j)then
                  iulev = ie2a(j)
                  do io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                    endif
                  enddo
                  metcnt=metcnt+1
                  do k=1,maxd
                    do l=1,maxt
                      peca(k,l)=aa(j)*stack(kstrn,i,l,k)/densa(k)
                    enddo
                  enddo
              endif
            endif
c------------limited allocation to pec stack but warn if exceeded
   17     if(metcnt.ge.ndpec) then
             ndpec_new = ndpec*(icnte/metcnt)
             write(i4unit(-1),3001)ndpec, ndpec_new
             go to 60
          endif
C-----------------------------------------------------------------------
C  Accumulate line in feature pixel count vectors
C-----------------------------------------------------------------------
          if (.not.lwvrg) goto 18

          if(i.eq.1) then
            pow_f(iwvrg)=pow_f(iwvrg)+pecpow
          endif


          do it=1,maxt
            do ipix = 1,npxdim
              cpixa(ipix) = 0.0D0
            enddo
            tev = tpva(it)
            cpixmx = cpixmxa(iwvrg,it)
            call hapixv( nwgdim , npxdim , fcrit  ,
     &                   nwvl   , npix   , wvmin  , wvmax     ,
     &                   iwvrg  , cpixmx ,
     &                   wvl    , tev    , amssno , peca(1,it),
     &                   cpixa  , ind1   , ind2
     &                  )

            do ine=1,maxd
              do ipix=ind1,ind2
                f_cpixa(ipix,it,ine,iwvrg) = f_cpixa(ipix,it,ine,iwvrg)+
     &                             peca(ine,it)*cpixa(ipix)/peca(1,it)
                cpixmxa(iwvrg,it) = dmax1(cpixmxa(iwvrg,it),
     &                             dble(f_cpixa(ipix,it,ine,iwvrg)))
              enddo
            enddo
            do ipix = ind1,ind2
              cpixa(ipix)=0.0D0
            enddo
          enddo
   18     continue
C-----------------------------------------------------------------------
C  Output f_pec for wavelength intervals and metastable to iunt40
C-----------------------------------------------------------------------
   60     continue

          if (open40) then
             do iwg = 1,nwvl
               isel_f = isel_f+1

               ctype_f(isel_f)=type(1)
               ispbr_f(isel_f)=i
               irg_f(isel_f)=iwg
               ilzr_f(isel_f)=iz
               ihzr_f(isel_f)=iz


               if(lnorm.and.(nmet.eq.1))then
c--- put '1' here rather than 't'; ispb field holds the 't'.  comment!!!
                   write(film40_exc(16:17),'(1x,1a1)')'1'
                   write(iunt40,2001) npix(iwg) , maxd , maxt ,
     &                    film40_exc , type(1) ,'ispb','t', isel_f
               else
                   write(film40_exc(16:17),'(i2)')i
                   write(iunt40,2000) npix(iwg) , maxd , maxt ,
     &                    film40_exc , type(1) , 'ispb',i , isel_f
               endif
               write(iunt40,2005) wvmin(iwg), wvmax(iwg)
               write(iunt40,1002) (densa(k),k=1,maxd)
               write(iunt40,1002) (teva(k),k=1,maxt)
               do ine = 1,maxd
                 do it = 1,maxt
                   write(iunt40,1002)(f_cpixa(ipix,it,ine,iwg),ipix=1,
     &                                npix(iwg))
                 enddo
               enddo
             enddo
          endif
C-----------------------------------------------------------------------
C Based on the first metastable - order the pec list, strongest first,
C restrict number per metastable for output to max_pecout
C
C We may have no valid data. Jump to the plt writing in this case.
C-----------------------------------------------------------------------

        if (i.eq.1) then

            lup = .false.
            if (metcnt.GT.0) then

c               write(i4unit(-1),*)'hapecf: before xxordr'
c               do ii=1,metcnt
c                 write(i4unit(-1),*)
c     &              'ii,indx_peca(ii),wvla(ii),peca_mx(ii)=',
c     &               ii,indx_peca(ii),wvla(ii),peca_mx(ii)
c               enddo

               call xxordr(metcnt, lup, peca_mx, indx_peca, invdx_peca)
               isel1_out=min(metcnt,max_pecout)

c               write(i4unit(-1),*)'hapecf: after xxordr'
c               do ii=1,isel1_out
c                 write(i4unit(-1),*)
c     &            'ii,indx_peca(ii),wvla(indx_peca(ii)),peca_mx(ii)=',
c     &             ii,indx_peca(ii),wvla(indx_peca(ii)),peca_mx(ii)
c               enddo

            else
               write(i4unit(-1),3000)
               goto 19
            endif

c------ now sort restricted list by wavelength

            do ii=1,isel1_out
              ipr(ii)=ii
              wvl_sort(ii)=wvla(indx_peca(ii))
            enddo

            lup=.true.

c               write(i4unit(-1),*)'hapecf: before xxr8sort'
c               do ii=1,isel1_out
c                 write(i4unit(-1),*)
c     &              'ii,ipr(ii),wvl_sort(ii),peca_mx(ii)=',
c     &               ii,ipr(ii),wvl_sort(ii),peca_mx(ii)
c               enddo

            call xxr8sort(isel1_out,lup,wvl_sort,ipr)

c               write(i4unit(-1),*)'hapecf: after xxr8sort'
c               do ii=1,isel1_out
c                 write(i4unit(-1),*)
c     &              'ii,ipr(ii),wvl_sort(ii),peca_mx(ipr(ii))=',
c     &               ii,ipr(ii),wvl_sort(ii),peca_mx(ipr(ii))
c               enddo

c------ set pointers and initialise transition parameter maxima

            ita_max=0
            isa_max=0
            ila_max=0
            xja_max=0.0d0

            do ii=1,isel1_out

c-------and make the wavelength string for the output set

              wvl=wvla(indx_peca(ipr(ii)))
              wtrans(ii)=wvl
              iw=8
              call xxdeci(wvl,iw,id,ifail)
              if((ifail.eq.0).and.(id.lt.10)) then
                 fmt01='(f9.*,1x)'
                 write(fmt01(5:5),'(i1)')id
                 write(cwvla(ii),fmt01)wvl
c                 write(i4unit(-1),*)'ii,ipr,indx_peca,cwvla(ii)=',
c     &  ii,ipr(ii),indx_peca(ipr(ii)),wvl,cwvla(ii)
              else
                 cwvla(ii)=' ******** '
              endif

c-------and identify maximum values for transition parameters

              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do io = 1, nord
                if ( iulev .eq. iordr(io) ) then
                   kstrn = io
                   ita_max=max0(ita_max,ie2a(j))
                   ita_max=max0(ita_max,ie1a(j))
                   isa_max=max0(isa_max,isa(ie2a(j)))
                   isa_max=max0(isa_max,isa(ie1a(j)))
                   ila_max=max0(ila_max,ila(ie2a(j)))
                   ila_max=max0(ila_max,ila(ie1a(j)))
                   xja_max=dmax1(xja_max,xja(ie2a(j)))
                   xja_max=dmax1(xja_max,xja(ie1a(j)))
                endif
              enddo
            enddo

c------make the transition string format to fit

            call xxfrmt_t(ita_max,isa_max,ila_max,xja_max,iw,fmt02)
            if(iw.gt.len(ctrans(1)))then
                fmt02="(i3,'(',i1,')',i1,'(',f5.1,')-',i1,'(',i1,')',i1,
     &'(',f5.1,')')"
                write(i4unit(-1),3002)iw
            endif

c            write(i4unit(-1),'(1a63)')fmt02

            do ii=1,isel1_out

c-------and make the transition string for the output set

              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do io = 1, nord
                if ( iulev .eq. iordr(io) ) then
                    write(ctrans(ii),fmt02)ie2a(j),isa(ie2a(j)),
     &                                     ila(ie2a(j)),xja(ie2a(j)),
     &                                     ie1a(j),isa(ie1a(j)),
     &                                     ila(ie1a(j)),xja(ie1a(j))
c                    write(i4unit(-1),*)'ii,j,ctrans(ii)=',
c     &                                  ii,j,ctrans(ii)
                endif
              enddo
            enddo


        endif

c-----------------------------------------------------------------------
c initialise iunt15 for pec output and set headers when i=1
c-----------------------------------------------------------------------

      if (i.eq.1) then
         nblocks =  isel1_out*nmet
         if(lrsel) then
             nblocks=nblocks+isel1_out*nplr
         endif
         if(lrsel.and.lhsel) then
             nblocks=nblocks+isel1_out*nplr
         endif
         if (open15) then

             write(iunt15,2003) nblocks, esym , is

c-----output the root connection vector text lines

             call  xxmkrc( ndcnct   ,
     &                     iz0      , iptnl    ,
     &                     ncnct    , icnctv
     &                   )

             write(iunt15,'(1a80)')'-'//cdash(2:80)
             write(iunt15,'(14i5)')(icnctv(ii),ii=1,ncnct)


c-----output the root partition level text lines

             call xxmkrp( ndstack       ,
     &                    iz0           , iptnl       ,
     &                    ncptn_stack   , cptn_stack
     &                  )

             write(iunt15,'(1a80)')'-'//cdash(2:80)
             do ii=1,ncptn_stack
               write(iunt15,'(1a80)') cptn_stack(ii)
             enddo
             write(iunt15,'(1a80)')'-'//cdash(2:80)

         endif

      endif

c-----------------------------------------------------------------------
c output strongest pec set directly to file for each metastable
c-----------------------------------------------------------------------


      do ii=1,isel1_out
        j=inde_peca(indx_peca(ipr(ii)))

        iulev = ie2a(j)
        do io = 1, nord
          if ( iulev .eq. iordr(io) ) then
              kstrn = io
          endif
        enddo

        isel = isel + 1
        ima(isel) = i

        ispbr(isel) = i
        iszr(isel) = iz
        itg(isel)=j
        iwr(isel)=ii
        ipr(isel)=ipr(ii)
        ctype(isel)=type(1)
        wtrans(isel)=wvl_sort(ii)


        if(lnorm.and.(nmet.eq.1))then
c--- put '1' here rather than 't'; ispb field holds the 't'.  comment!!!
            write(film15_exc(16:17),'(1x,1a1)')'1'
            if (open15) write(iunt15,1001) cwvla(ii) , maxd , maxt ,
     &                         film15_exc , type(1) ,'ispb','t', isel
        else
            write(film15_exc(16:17),'(i2)')i
            if (open15) write(iunt15,1000) cwvla(ii) , maxd , maxt ,
     &                         film15_exc , type(1) ,'ispb', i , isel
        endif
        if (open15) write(iunt15,1002) (densa(k),k=1,maxd)
        if (open15) write(iunt15,1002) (teva(k),k=1,maxt)
        do k = 1 , maxd
           do l=1,maxt
              pec(l) = aa(j)*stack(kstrn,i,l,k)/densa(k)
              if(lnorm.and.(nmet.eq.1))then
                  call b8norm( ndlev , ndmet ,
     &                         nord  ,
     &                         stack(1,1,l,k),
     &                         pec(l), dum1  ,
     &                         dum2  , dum3
     &                        )
              endif
              if(pec(l).le.1.0D-74) pec(l)=1.0D-74
           enddo
           if (open15) write(iunt15,1002)(pec(l),l=1,maxt)
        enddo
      enddo
   19 continue


C-----------------------------------------------------------------------
C     NOTE: Hugh and I were unsure about the addition of .and.lrsel - ADW
C-----------------------------------------------------------------------

      if (liosel .and. lrsel) then


        call b8corp(il, maxt,  maxd,  nmet,  teva, stvr )

        if(lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 25 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 24 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 25
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = 0
                       do 23 k = 1 , maxd
                          sum1=0.0
                          do  i=1,npl3
                            sum1=sum1+ratpia(k,i)
                          end do
                          if(sum1.le.0.0D0) then
                              write(i4unit(-1),2012)
     &                        'Parent population sum .le. 0'
                              write(i4unit(-1),2013)
                              stop
                          endif
                          do l=1,maxt
                            sum(l)=0.0D0
                            do  i=1,npl3
                              sum(l)=sum(l)+aa(j)*stvr(kstrn,l,k,i)*
     &                                      ratpia(k,i)
                            end do
                            sum(l)=sum(l)/sum1
                          end do
   23                   continue
                        go to 25
                    endif
   24             continue
              endif
          if (metcnt.ge.ndpec) go to 130
   25     continue

        else

            do 29 i = 1 , npl3
             metcnt=0
             do 28 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 27 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 28
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = i
                    endif
   27             continue
              endif
              if(metcnt.ge.ndpec) go to 29
   28        continue
   29       continue
        endif
 130    continue

      elseif((.not.liosel).and.lrsel.and.(icntr.gt.0))then

        if(lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 35 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 34 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   = 1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 35
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = 0
                        do 33 k = 1 , maxd
                         sum1=0.0d0
                         do 30 i=1,nplr
                          sum1=sum1+ratpia(k,i)
   30                    continue
                         if(sum1.le.0.0D0) then
                             write(i4unit(-1),2012)
     &                             'Parent population sum .le. 0'
                             write(i4unit(-1),2013)
                             stop
                         endif
                         do 32 l=1,maxt
                          sum(l)=0.0d0
                          do 31 i=1,nplr
                           sum(l)=sum(l)+aa(j)*stvr(kstrn,l,k,i)*
     &                                   ratpia(k,i)
   31                     continue
                          sum(l)=sum(l)/sum1
   32                    continue
   33                   continue
                        go to 35
                    endif
   34             continue
              endif
              if (metcnt.ge.ndpec) go to 140
   35     continue

        else

            do 39 i = 1 , nplr
             metcnt = 0
             do 38 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 37 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 38
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = i
                        go to 38
                    endif
   37             continue
              endif
              if(metcnt.ge.ndpec) go to 39
   38        continue
   39       continue
        endif

      endif
 140  continue

C-----------------------------------------------------------------------

      if(lhsel.and.(icnth.gt.0))then

        if(lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 45 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 44 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 45
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = 0
                        do 43 k = 1 , maxd
                         sum1=0.0d0
                         do 40 i=1,nplr
                          sum1=sum1+ratpia(k,i)
   40                    continue
                         if(sum1.le.0.0D0) then
                             write(i4unit(-1),2012)
     &                             'Parent population sum .le.0'
                             write(i4unit(-1),2013)
                             stop
                         endif
                         do 42 l=1,maxt
                          sum(l)=0.0D0
                          do 41 i=1,nplr
                           sum(l)=sum(l)+aa(j)*stvh(kstrn,l,k,i)*
     &                                   ratpia(k,i)
   41                     continue
                           sum(l)=sum(l)/sum1
   42                    continue
   43                   continue
                        go to 45
                    endif
   44             continue
              endif
              if (metcnt.ge.ndpec) go to 150
   45     continue

        else

           do 49 i = 1 , nplr
             metcnt = 0
             do 48 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 47 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 48
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = i
                go to 48
                    endif
   47             continue
              endif
              if (metcnt.ge.ndpec) go to 49
   48        continue
   49       continue
        endif

      endif
 150  continue

C-----------------------------------------------------------------------
      if(lisel.and.(icnti.gt.0))then

        if(lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 55 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 54 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 55
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = 0
                        do 53 k = 1 , maxd
                         sum1=0.0d0
                         do 50 i=1,npli
                          sum1=sum1+ratmia(k,i)
   50                    continue
                         if(sum1.le.0.0D0) then
                             write(i4unit(-1),2012)
     &                             'Parent population sum .le.0'
                             write(i4unit(-1),2013)
                             stop
                         endif
                         do 52 l=1,maxt
                          sum(l)=0.0D0
                          do 51 i=1,npli
                           sum(l)=sum(l)+aa(j)*stvi(kstrn,l,k,i)*
     &                                   ratmia(k,i)
   51                     continue
                           sum(l)=sum(l)/sum1
   52                    continue
   53                   continue
                        go to 55
                    endif
   54             continue
              endif
              if (metcnt.ge.ndpec) go to 160
   55     continue

        else

            do 59 i = 1 , npli
             metcnt = 0
             do 58 j = 1 , icnte
              if ( aa(j) .gt. avlt ) then
                  iulev = ie2a(j)
                  do 57 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                        call hawvrg( ndwvl  , ndpix  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               wvl    ,
     &                               lwvrg  , iwvrg
     &                              )
                        if(.not.lwvrg) go to 58
                        metcnt = metcnt+1
                        isel = isel + 1
                        wvla(isel)=wvl
                        ima(isel) = i
                        go to 58
                    endif
   57             continue
              endif
              if (metcnt.ge.ndpec) go to 59
   58        continue
   59       continue
        endif

      endif

 160  isel4=isel

      if (open15) then
         call xxwcmt_15( iunt15     ,
     &                   ndstore    , idcmt    , idfld   , idsyn   ,
     &                   idion      , idopt    , ndlev   ,
     &                   lroot      , lsuper   ,
     &                   nfld_15    , isyn_15  , fldk_15 ,
     &                   lfld_15    ,
     &                   iz0        , iz1      , popcode ,
     &                   dsninc     , dsnexp    ,
     &                   nion       , cion     , lion    ,
     &                   nopt       , copt     , lopt    ,
     &                   il         , config   , cterm   , wa      ,
     &                   esym       , dsnpt    ,
     &                   iptnl      , is       , tabul_15, units_15,
     &                   isel4      , nspb     , nspp    ,
     &                   isel1_out  , ctrans   , wtrans  ,
     &                   ctype      , ispbr    , isppr   , iszr    ,
     &                   itg        , ipr      , iwr     ,
     &                   code       , producer , date
     &                  )
      endif


C-----------------------------------------------------------------------
C  sort feature pecs by minimum wavelength of range and power in range
C-----------------------------------------------------------------------
      lup=.true.

      call xxr8sort(nwvl,lup,wvrg_sort,iwr_f)

      lup=.false.
      do i=1,nwvl
        powf_sort(i)=pow_f(i)
      enddo
      call xxr8sort(nwvl,lup,powf_sort,ipr_f)

      if(nmet.gt.1) then
        do i=2,nmet
          do iwvrg=1,nwvl
            iwr_f(iwvrg+(i-1)*nwvl)=iwr_f(iwvrg)
            ipr_f(iwvrg+(i-1)*nwvl)=ipr_f(iwvrg)
          enddo
        enddo
      endif



      if (open40) then
         call xxwcmt_40( iunt40     ,
     &                   ndstore    , idcmt    , idfld   , idsyn   ,
     &                   idion      , idopt    , ndlev   ,
     &                   lroot      , lsuper   ,
     &                   nfld_40    , isyn_40  , fldk_40 ,
     &                   lfld_40    ,
     &                   iz0        , iz1      , popcode ,
     &                   dsninc     , dsnexp    ,
     &                   nion       , cion     , lion    ,
     &                   nopt       , copt     , lopt    ,
     &                   il         , config   , cterm   , wa      ,
     &                   esym       , dsnpt    ,
     &                   iptnl      , is       , tabul_40, units_40,
     &                   isel_f     , nspb     , nspp    ,
     &                   nwvl       , npix     , wvmin   , wvmax   ,
     &                   ilzr_f     , ihzr_f   ,
     &                   ctype_f    , ispbr_f  , isppr_f ,
     &                   irg_f      , ipr_f    , iwr_f   ,
     &                   code       , producer , date
     &                  )
      endif


C-----------------------------------------------------------------------
C  Now the radiated power
C
C  Fill the plt and pltnfl arrays - only for nmet=1
C-----------------------------------------------------------------------

      do it = 1,ntdim
        do ine = 1,nddim
          plt(it,ine)    = 0.0D0
          pltnfl(it,ine) = 0.0D0
        end do
      end do


      do j = 1 , icnte

         iulev = ie2a(j)
         de    = wa(ie2a(j))-wa(ie1a(j))

         do io = 1 , nord

            if ( iulev .eq. iordr(io) ) then

              kstrn    = io
              tmenergy = de / 8065.541D0

              call d8tran(ndeng    , ndedge  ,
     &                    ieng     , iedge   ,
     &                    edge     , energy  , fraction ,
     &                    tmenergy , frac
     &                    )

              do k = 1 , maxd
                do l = 1 , maxt
                  pltnfl(l,k) = pltnfl(l,k) + 1.98638D-23 *
     &                            aa(j) * de *
     &                            stack(kstrn,1,l,k) / densa(k)
                  plt(l,k) = plt(l,k) + 1.98638D-23 *
     &                        aa(j) * de * frac *
     &                        stack(kstrn,1,l,k) / densa(k)
                end do
              end do

            endif

         end do

      end do


C-----------------------------------------------------------------------
C Write out standard adf11 plt and plt-filter to  iunt11 and iunt11f
C-----------------------------------------------------------------------

      do ic = 1,ncomments
         comments(ic)=' '
      end do

      write(comments(1)(1:80),'(1a80)')
     &      'C-------------------------------------------'//
     &      '------------------------------------'
      write(comments(2)(1:1),'(1a1)')'C'
      write(comments(3)(1:36),'(1a36)')
     &     'C  Low-level line power coefficients'
      write(comments(4),'(a5,a)')'C    ',dsn35(1:75)
      write(comments(5)(1:1),'(1a1)')'C'

      ic = 6

      write(comments(ic)(1:1),'(1a1)')'C'
      write(comments(ic+1)(1:21),'(1a14,1a7)')
     &      'C  CODE     : ', 'ADAS810'
      write(comments(ic+2)(1:44),'(1a14,1a30)')
     &      'C  PRODUCER : ', user
      write(comments(ic+3)(1:44),'(1a14,1a8)')
     &      'C  DATE     : ', date
      write(comments(ic+4)(1:1),'(1a1)')'C'
      write(comments(ic+5)(1:80),'(1a80)')
     &      'C-------------------------------------------'//
     &      '------------------------------------'

      ic = ic+5

C prepare output as log10 and normalise

      do k=1,maxd
         fnel(k)=dlog10(densa(k))
      end do

      do l=1,maxt
         tel(l)=dlog10(teva(l))
      end do

      do k=1,maxd
        do l=1,maxt

          val    = plt(l,k)
          valnfl = pltnfl(l,k)
          if(lnorm) then
              call b8norm( ndlev , ndmet ,
     &                     nord  ,
     &                     stack(1,1,l,k),
     &                     val   , dum1  ,
     &                     dum2  , dum3
     &                   )
              call b8norm( ndlev , ndmet ,
     &                     nord  ,
     &                     stack(1,1,l,k),
     &                     valnfl , dum1 ,
     &                     dum2   , dum3
     &                  )
           endif

           val          = max(val,1.0D-74)
           valnfl       = max(valnfl,1.0D-74)
           plt(l,k)     = dlog10(val)
           pltnfl(l,k)  = dlog10(valnfl)

        end do
      end do

      name = xfelem(iz0)//' '

      call haout1( iunt11   , iunt11f  ,
     &             open11   , open11f  ,
     &             date     ,
     &             ntdim    , nddim    ,
     &             maxt     , maxd     ,
     &             iz0      , iz1      ,
     &             tel      , fnel     , name   ,
     &             plt      , pltnfl   ,
     &             comments , ic
     &             )




C-----------------------------------------------------------------------
 1000 format(1a10,2i4,' /',1a23,'/type=',1a5,
     &       '/',1a4,' = ',i1,'/isel = ',i4)
 1001 format(1a10,2i4,' /',1a23,'/type=',1a5,
     &       '/',1a4,' = ',1a1,'/isel = ',i4)
 1002 format(1p,8e9.2)
C-----------------------------------------------------------------------
 2000 format(2x,i4,'  ',2i4,' /',1a29,'/type=',1a5,
     &       '/',1a4,' = ',i1,'/isel = ',i4)
 2001 format(2x,i4,'  ',2i4,' /',1a29,'/type=',1a5,
     &       '/',1a4,' = ',1a1,'/isel = ',i4)
 2002 format(i5,'    /',1a2,':',i2,' feature photon emissivity coeffic',
     &       'ients/')
 2003 format(i5,'    /',1a2,':',i2,' photon emissivity coefficients/')
 2005 format(2f12.5)
 2011 format(1x,32('*'),' hapecf error ',32('*')//
     &       1x,'Mismatch of internal dimension parameters')
 2012 format(1x,32('*'),' hapecf error ',32('*')//
     &       1x,'Fault in pec output: ',a,i3,a)
 2013 format(/1x,29('*'),' Program terminated ',29('*'))
C-----------------------------------------------------------------------
 3000 format(1x,30('*'),' hapecf warning ',30('*')//
     &       1x,'No valid data in wavelength region -',
     &       1x,'No fPEC file will be written', /,
     &       1x,76('*'))
 3001 format(1x,30('*'),' hapecf warning ',30('*')//
     &       1x,'truncation of lines applied - ',
     &       1x,'increase ndpec =', i6,' to at least ',i6 /,
     &       1x,76('*'))
 3002 format(1x,30('*'),' hapecf warning ',30('*')//
     &       1x,'field transition string too short - ',
     &       1x,'increase ctrans() strings to *',i2/,
     &       1x,76('*'))
C-----------------------------------------------------------------------

      return
      end
