      subroutine haout0( iunit  , cadas   , method , dsn42   , title ,
     &                   ndtem  , ndden   , ndwvl  , ndmet   ,
     &                   dsnin  , dsnexp  , dsnflt ,
     &                   dsnpec , dsnfet  , dsnplt , dsnpltf ,
     &                   celem  , iz0     , iz1    , bwno    ,
     &                   lnorm  , nmet    , imetr  ,
     &                   liosel , lhsel   , lrsel  , lisel   ,
     &                   lnsel  , lpsel   , zeff   ,
     &                   maxt   , maxd    , nwvl   ,
     &                   tine   , tinp    , tinh   , dine    , dinp  ,
     &                   npix   , wvmin   , wvmax  , amin
     &              )

      implicit none

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 subroutine: haout0 *********************
C
C  Purpose: writes information to 'paper.txt' as a record of the run.
C
C  Calling program: adas810
C
C  Input : (I*4)  iunit       = output unit for results
C  Input : (C*(*))dsnin       = full input data set name.
C  Input : (I*4)  itdimd      = maximum number of temperatures
C  Input : (I*4)  iddimd      = maximum number of densities
C  Input : (I*4)  numt        = number of temperatures to be used in scan
C  Input : (I*4)  numd        = number of densities
C  Input : (I*4)  iz0         =  nuclear charge
C  Input : (I*4)  iz1         =  ion charge +1
C  Input : (R*8)  tel()       = temperature set of tables - log mesh
C  Input : (R*8)  fnel()      = density set of tables - log mesh
C  Input : (C*13) name        = element name
C
C
C
C
C Routines:
C          Routine    Source    Brief description
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    03-03-2003
C
C UPDATE:
C
C  VERSION : 1.2                           DATE: 29-05-2003
C  MODIFIED: Martin O'Mullane
C             - Add title to output file.
C
C  VERSION : 1.3                           
C  DATE    : 01-08-2011
C  MODIFIED: Martin O'Mullane
C             - Trap for case where TITLE is blank.
C
C  VERSION : 1.4                           
C  DATE    : 24-04-2019
C  MODIFIED: Martin O'Mullane
C             - Input filename lengths are defined by argument length.
C
C  VERSION : 1.5                           
C  DATE    : 23-05-2019
C  MODIFIED: Martin O'Mullane
C             - Use g12.4 in format statement 6010 to accommodate
C               large wavelengths.
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       integer   ndtem     , ndden       , ndwvl      ,       
     &           ndmet     , iz0         , iz1        , iunit      ,
     &           maxt      , maxd        , nmet       , nwvl              
       integer   it        , id          , l1         , l2
C-----------------------------------------------------------------------
       character dsnin*(*) , dsnexp*(*)  , dsnflt*(*) , dsnplt*(*) ,  
     &           dsnpec*(*), dsnfet*(*)  , dsnpltf*(*),
     &           celem*3   , dsn42*(*)   , method*8   , str*80     ,
     &           cadas*80  , title*80
C-----------------------------------------------------------------------
       real*8    bwno      , zeff        , amin
C-----------------------------------------------------------------------
       logical   lpsel     , liosel      , lhsel     , 
     &           lrsel     , lisel       , lnsel      , lnorm 
C-----------------------------------------------------------------------
       integer   imetr(ndmet)    , npix(ndwvl) 
C-----------------------------------------------------------------------
       real*8    tine(ndtem)     , tinp(ndtem)      , tinh(ndtem)   ,
     &           dine(ndden)     , dinp(ndden)      ,
     &           wvmin(ndwvl)    , wvmax(ndwvl)
C-----------------------------------------------------------------------

C Banner

      call xxslen(cadas, L1, L2)
      write(iunit, '(A)')cadas(L1:L2)
      write(iunit, 100)
      
      call xxslen(title, L1, L2)
      if (L1.GT.0) then
         write(iunit, '(A)')title(L1:L2)
      else
         write(iunit, '(A)')' '
      endif
      write(iunit, 100)

C Calculation method
      
      if (method(1:1).EQ.'D') then
         call xxslen(dsn42, L1, L2)
         write(iunit, 1000)dsn42(L1:L2)
      else
         write(iunit, 1010)
      endif
      write(iunit,100)
      

C Filenames

      call xxslen(dsnin, L1, L2)
      write(iunit,2000)dsnin(L1:L2)
      
      if (dsnexp(1:4).EQ.'NULL') then
         str = 'Not used'
      else
         str = dsnexp
      endif
      call xxslen(str, L1, L2)
      write(iunit,2010)str(L1:L2)
      
      if (dsnflt(1:4).EQ.'NULL') then
         str = 'Not used'
      else
         str = dsnflt
      endif
      call xxslen(str, L1, L2)
      write(iunit,2020)str(L1:L2)
      
      write(iunit,100)

C Ion information

      write(iunit,2500)celem(1:2), iz0, iz1, bwno     
      write(iunit,100)


C Plasma parameters

      write(iunit,3000)maxt, maxd
      write(iunit,3010)
      write(iunit,3020)
     
      do it = 1,maxt

         if (it.le.maxd) then
            write(iunit,3030) it, tine(it), tinp(it), tinh(it),
     &                        it, dine(it), dinp(it)
         else
            write(iunit,3040) it, tine(it), tinp(it), tinh(it)
         endif
      
      end do

      if (maxd.gt.maxt) then
         do id = maxt+1,maxd
            write(iunit,3050) id, dine(id), dinp(id)
         end do
      endif

      write(iunit,3020)
      write(iunit,100)


C Metastable information
 
      write(iunit,4000)
      write(iunit,4010)nmet, lnorm
      write(iunit,4020)(imetr(it), it=1, nmet)
      write(iunit,100)
       
C Reactions

      write(iunit,4030)liosel, lhsel, lrsel, lisel, lnsel, lpsel
      write(iunit,4040)zeff
      write(iunit,100)

C Outputs

      write(iunit,5000)
      if (dsnpec(1:4).EQ.'NULL') then
         str = 'Not used'
      else
         str = dsnpec
      endif
      call xxslen(str, L1, L2)
      write(iunit,5010)str(L1:L2)
 
      if (dsnfet(1:4).EQ.'NULL') then
         str = 'Not used'
      else
         str = dsnfet
      endif
      call xxslen(str, L1, L2)
      write(iunit,5020)str(L1:L2)

      if (dsnplt(1:4).EQ.'NULL') then
         str = 'Not used'
      else
         str = dsnplt
      endif
      call xxslen(str, L1, L2)
      write(iunit,5030)str(L1:L2)
      
      if (dsnpltf(1:4).EQ.'NULL') then
         str = 'Not used'
      else
         str = dsnpltf
      endif
      call xxslen(str, L1, L2)
      write(iunit,5040)str(L1:L2)
      write(iunit,100)


C Wavelength ranges

      write(iunit,6000)
      do it = 1, nwvl
        write(iunit, 6010)npix(it), wvmin(it), wvmax(it)
      end do
      write(iunit,100)
      write(iunit,6020)amin



C-----------------------------------------------------------------------
  100 format(' ')
 
 1000 format('Method : adf42 driver file = ', a, /)
 1010 format('Method : Standard input files', /)

 2000 format('Specific ion file : ', a)
 2010 format('Expansion file    : ', a)
 2020 format('Filter file       : ', a)
 
 2500 format('Element              : ', a3, /
     &       'Nuclear charge       : ', i2, /
     &       'Recombining charge   : ', i2, /
     &       'Ionisation Potential : ', f12.2)

 3000 format('Output plasma parameters :-'/,
     &       '---------------------------',/,
     &       'Number of temperatures : ',I2, /,
     &       'Number of densities    : ',I2, /)
 3010 format( / ,'Index',3X,9('-'),' Temperatures (units: eV) ',9('-'),
     &       10X,'Index',4X,'Densities (units: cm-3)',6X/
     &       11X,'ElectroN',8X,'Proton',4X,'Neutral hydrogen',20X,
     &           'ElectroN',7X,'Proton',11X/
     &       13X,'<TE>',11X,'<TP>',11X,'<TH>',28X,'<NE>',10X,'<NP>')
 3020 FORMAT(52('-'),10X,38('-'))
 3030 FORMAT( 2X,I2,5X,1P,3(D11.3,4X),
     &       10X,I2,2X,   2(3X,D11.3))
 3040 FORMAT( 2X,I2,5X,1P,3(D11.3,4X))
 3050 FORMAT(64X,I2,2X,1P,2(3X,D11.3))
     
 4000 format('Metastables information:- ',/
     &       '-------------------------')
 4010 format('Number of metastables : ', i2, /,
     &       'Normalised            : ', L2 )
 4020 format('Indices               : ', 4(I2,2x,:))

 4030 format('Include following reactions :-',/
     &       '------------------------------',/,
     &       'Ionisation Rates         : ', L2, / 
     &       'Charge Exchange          : ', L2, /        
     &       'Recombination            : ', L2, /        
     &       'Inner Shell Ionisation   : ', L2, /  
     &       'Include Projection Data  : ', L2, /  
     &       'Proton Impact Collisions : ', L2, /)
 4040 format('Zeff for ion scaling     : ', f5.2)

 5000 format('Output files selected :- ',/,
     &       '-------------------------')
 5010 format('Photon emissivities                 : ', a)
 5020 format('Feature PEC                         : ', a)
 5030 format('Total power coefficients            : ', a)
 5040 format('Total power coefficients - filtered : ', a)

 6000 format('Wavelength ranges selected :-', /,
     &       '-----------------------------', /,/,
     &       'IWVRG     wavelength range (Ang)' ,/
     &       '-----     -----------------------')
 6010 format(1x,i4, 1x, 1p, g12.4,' - ', g12.4)
 6020 format('Minimum A-value : ', 1p,e10.2)

C-----------------------------------------------------------------------


      return

      end
