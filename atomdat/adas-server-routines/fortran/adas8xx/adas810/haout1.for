C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas810/haout1.for,v 1.3 2019/04/25 10:56:08 mog Exp $ Date $Date: 2019/04/25 10:56:08 $
C
      subroutine haout1( iunt11   , iunt14   ,
     &                   open11   , open14   ,
     &                   date     ,
     &                   itdimd   , iddimd   ,
     &                   numt     , numd     ,
     &                   iz0      , iz1      ,
     &                   tel      , fnel     , name   ,
     &                   pltl     , pltlnfl  ,
     &                   comments , ncomments
     &                 )
C
      implicit none
C
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: haout1 *********************
C
C  purpose: writes plt and plt-filter output to adf11 baseline 
C           unresolved format files.
C
C  calling program: adas810
C
C  input : (i*4)  iunt11      = output unit for adf11-plt results
C  input : (i*4)  iunt14      = output unit for adf11-plt-filter results
C  input : (c*8)  date        = date string.
C  input : (i*4)  itdimd      = maximum number of temperatures
C  input : (i*4)  iddimd      = maximum number of densities
C  input : (i*4)  numt        = number of temperatures to be used in scan
C  input : (i*4)  numd        = number of densities
C  input : (i*4)  iz0         =  nuclear charge
C  input : (i*4)  iz1         =  ion charge +1
C  input : (r*8)  tel()       = temperature set of tables - log mesh
C  input : (r*8)  fnel()      = density set of tables - log mesh
C  input : (c*13) name        = element name
C
C  input : (r*8)  pltl(,)     = total line radiated power (with filter)
C  input : (r*8)  pltlnfl(,)  = line power (no filter)
C  input : (c*80) comments()  = comment section for output datasets
C
C          (c*8)  infplt    = total line power info string
C          (c*8)  infpltn   = total line power (no filter) info
C
C          (c*24) metplt      = total line power method string
C          (c*24) metpltn     = total line power (no filter) method
C
C          (i*4)  i           = array index.
C          (i*4)  i1          = array index.
C          (i*4)  i2          = array index.
C          (c*10) cstrg1      = ground information string
C          (c*10) cstrg2      = parent information string
C
C
C routines:
C          routine    source    brief description
C          ------------------------------------------------------------
C          haadas2    adas      output standard master data
C
C AUTHOR:  H. P. Summers, University of Strathlcyde
C
C DATE:    24 April 2002
C
C UPDATE:
C
C
C VERSION  : 1.2                        
C DATE     : 28-02-2004
C MODIFIED : Martin O'Mullane  
C             - Alter comments in filtered output to reflect change
C               from wavelength filtering to adf35 filtering.
C
C  VERSION : 1.3                           
C  DATE    : 24-04-2019
C  MODIFIED: Martin O'Mullane
C             - Remove redundant dsnin argument.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      integer    itdimd     , iddimd  
      integer    iunt11     , iunt14  
      integer    numt       , numd    
      integer    iz0        , iz1     
      integer    ncomments
C-----------------------------------------------------------------------
      logical    open11     , open14
C-----------------------------------------------------------------------
      character  date*8     , name*13
      character  metplt*24  , metpltn*24 
      character  infplt*8   , infpltn*8
      character  cstrg1*10  , cstrg2*10
C-----------------------------------------------------------------------
      real*8     pltl     (itdimd,iddimd)
      real*8     pltlnfl  (itdimd,iddimd)
      real*8     tel(itdimd),fnel(iddimd)
C-----------------------------------------------------------------------
      character comments(ncomments)*80
C-----------------------------------------------------------------------

      cstrg1 = ' IGRD= 1  '
      cstrg2 = ' IPRT= 0  '
      
      infplt  = 'PLT     '
      infpltn = 'PLT     '

      metplt      = 'ADAS810-filtered        '
      metpltn     = 'ADAS810-total           '


C-----------------------------------------------------------------------
C   total line radiated power - including filter
C-----------------------------------------------------------------------


      comments(3)(38:77)=' - filtered through adf35 file'
 
      if (open14) call haadas2( iunt14 , date     ,
     &                          itdimd , iddimd   ,
     &                          pltl   ,           
     &                          numt   , numd     ,
     &                          fnel   , tel      ,
     &                          iz0    , iz1      ,
     &                          name   , metplt   ,
     &                          cstrg1 , cstrg2   ,
     &                          infplt , comments , ncomments ) 
     

C-----------------------------------------------------------------------
C   total line radiated power - no filter applied
C-----------------------------------------------------------------------


      comments(3)(38:77)= ' - unfiltered                           '
      comments(4)       = 'C'
 
      if (open11) call haadas2( iunt11    , date     ,
     &                          itdimd    , iddimd   ,
     &                          pltlnfl   ,
     &                          numt      , numd     ,
     &                          fnel      , tel      ,
     &                          iz0       , iz1      ,
     &                          name      , metpltn  ,
     &                          cstrg1    , cstrg2   ,
     &                          infpltn   , comments , ncomments )
 

      return

      end
