       subroutine hawvrg( ndwvl  , ndpix  , 
     &                    nwvl   , npix   , wvmin  , wvmax  ,
     &                    wvl    ,
     &                    lwvrg  , iwvrg   
     &                  )
c
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: hawvrg *********************
c
c  purpose:  to check if a line wavelength is in one of the selected 
c            wavelength intervals  
c
c  calling program: hapecf
c
c
c  subroutine:
c
c  input : (i*4)  ndwvl   = maximum number of wavelength intervals
c  input : (i*4)  ndpix   = maximum number of pixels per wvln. interval
c
c  input : (i*4)  nwvl    = wvaelength intervals 
c  input : (i*4)  npix()  = number of pixels assigned to wavelength interval
c  input : (r*8)  wvmin() = lower limit of wavelength interval (ang)
c  input : (r*8)  wvmax() = upper limit of wavelength interval (ang)
c
c  input : (r*8)  wvl     = input line wavelength for test(ang)
c
c  output: (l*4)  lwvrg   = .true.  => spectrum line in selected range
c                         = .false. => spectrum line in selected range
c  output: (i*4)  iwvrg   = index of wavelength range in which lin lies
c                           if lwvrg = .true. otherwise set to zero
c
c routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c author:  Hugh Summers, University of Strathclyde
c          JA7.08
c          tel. 0141-548-4196
c
c date:    15/01/02
c
c update:
c
c  VERSION  : 1.1                          
c  DATE     : 04-03-2009
c  MODIFIED : Adam Foster
c              - Changed upper bound of "in wavelength range" check from .le.
c                to .lt. upper wavelength to avoid indexing an array 
c                element out of bounds
c
c
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   ndwvl       , ndpix 
      integer   nwvl        , iwvrg         
      integer   i  
c-----------------------------------------------------------------------
      real*8    wvl 
c-----------------------------------------------------------------------
      integer   npix(ndwvl) 
c-----------------------------------------------------------------------
      real*8    wvmin(ndwvl), wvmax(ndwvl)
c-----------------------------------------------------------------------
      logical   lwvrg 
c-----------------------------------------------------------------------
c---------------------------------------------------------------------- 
c  set lwvrg to .false.  and iwvrg  to zero initially
c-----------------------------------------------------------------------
          lwvrg = .false.
          
          iwvrg = 0
c-----------------------------------------------------------------------
c test spectrum line
c-----------------------------------------------------------------------
c
       i=0
      
   10  i=i+1
       if(i.le.nwvl) then 
           if((wvl.ge.wvmin(i)).and.(wvl.lt.wvmax(i))) then
               lwvrg = .true.
               iwvrg = i
               go to 20
           else
               go to 10
           endif
       endif
       
   20  return
c
c-----------------------------------------------------------------------
c
 1001 format(1x,32('*'),' hawvrg error ',32('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
