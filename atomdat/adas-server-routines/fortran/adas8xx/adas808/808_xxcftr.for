CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/808_xxcftr.for,v 1.1 2004/07/06 09:57:45 whitefor Exp $ Date $Date: 2004/07/06 09:57:45 $
CX
      SUBROUTINE XXCFTR( ICFSEL , CSTRGI , CSTRGO , NSHEL)
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXCFTR *********************
C
C  PURPOSE: CONVERTS A CONFIGURATION CHARACTER STRING, SUCH AS OCCURS
C           IN A SPECIFIC ION FILE LEVEL LIST, BETWEEN EISSNER AND
C           STANDARD FORMS
C
C  CALLING PROGRAMS: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   ICFSEL  = 1 => STANDARD FORM OUT, STANDARD FORM IN
C                            2 => EISSNER  FORM OUT, STANDARD FORM IN
C                            3 => STANDARD FORM OUT, EISSNER  FORM IN
C                            4 => EISSNER  FORM OUT, EISSNER  FORM IN
C  INPUT : (C*(*)) CSTRGI  = CONFIGURATION STRING IN INPUT FORM
C  OUTPUT: (C*(*)) CSTRGO  = CONFIGURATION STRING IN OUTPUT FORM
C
C          (I*4)   I       = GENERAL USE
C          (I*4)   ISHEL   = SHELL COUNTER
C          (I*4)   IP      = PARITY OF CONFIGURATION
C          (I*4)   MAXN    = N_SHELL SUM FOR CONFIGURATION
C          (I*4)   NSHEL   = NUMBER OF SHELLS IDENTIFIED fFROM STRING
C          (I*4)   NELA()  = NUMBER OF ELECTRONS IN EACH SHELL
C
C          (C*19)  STRG    = STANDARD FORM CONFIGURATION STRING
C          (C*19)  STRGE   = EISSNER FORM CONFIGURATION STRING
C          (C*1)   CHEISA()= EISSNER CHARACTER FOR ORBITALS
C          (C*2)   CHSTDA()= STANDARD ORBITAL SPEC. FOR EACH SHELL
C                            (EISSNER FORM CASE)
C          (C*1)   CHQA()  = INDEX TO HEXADECIMAL CONVERSIONS
C          (C*1)   CHRA()  = CHAR. FOR NO. OF. EQUIV. ELEC. IN SHELL
C                            (STANDARD FORM CASE)
C
C          (L*4)   LEISS   = .TRUE.  => EISSNER FORM
C                            .FALSE. => NOT EISSNER FORM
C
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS     FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4NGRP     ADAS     RETURNS N QUANTUM NUMBER IN THE
C                              EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C          I4PGRP     ADAS     RETURNS PARITY OF ORBITAL GIVEN THE
C                              EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C          I4SCHR     ADAS     RETURNS NUMERICAL VALUE FOR NUMBER OF
C                              EQUIVALENT ELECTRONS GIVEN AS HEX> CHAR.
C          CSTGRP     ADAS     RETURNS TERM OF ORBITAL GIVEN IN THE
C                              EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C          CEIGRP     ADAS     RETURNS EISSNER CODE FOR ORBITAL
C
C
C NOTE:    THE ROUTINE IS USED TO CONVERT THE CONFIGURATION CHARACTER
C          STRING OCCURRING IN ADF04 FILE LEVEL LISTS.  THE STRING
C          LENGTH ALLOCATED TO THIS IS *18 FOLLOWING 1 BLANK SPACE
C          AFTER THE LEVEL INDEX.  A PROBLEM ARISES WHEN THE FIRST
C          SHELL CONTAINS MORE THAN 9 EQUIVALENT ELECTRONS.  IN THIS
C          CASE, OVERSPILL IS ALLOWED INTO THE BLANK CHARACTER SPACE.
C          THE ROUTINE WILL ANALYSE A *19 STRING INCLUDING THE USUALLY
C          BLANK LOCATION OR A *18 STRING EXCLUDING IT.  IN THE LATTER
C          CASE AN INTELLIGENT GUESS IS MADE AS TO WHETHER THE OMITTED
C          BLANK SHOULD IN FACT BE A '1'.  THIS SITUATION OCCURS FOR A
C          LEADING CLOSED D-SHELL.
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    25/10/95
C
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 11-03-03
C MODIFIED: ALLAN WHITEFOR
C               - Based on xxcftr.for v 1.1
C
C-----------------------------------------------------------------------
      INTEGER   NDSHL
C-----------------------------------------------------------------------
      PARAMETER (NDSHL=21)
C-----------------------------------------------------------------------
      INTEGER   ICFSEL    , I
      INTEGER   I4UNIT    , I4NGRP    , I4PGRP  , I4FCTN
      INTEGER   ISHEL     , NSHEL     , IP      , MAXN
      INTEGER   IABT
C-----------------------------------------------------------------------
      CHARACTER CSTRGI*(*), CSTRGO*(*) , CSTR19*100
      CHARACTER STRG*100   , STRGE*100
      CHARACTER CSTGRP*2  , CEIGRP*1
C-----------------------------------------------------------------------
      LOGICAL*4 LEISS
C-----------------------------------------------------------------------
      INTEGER   NELA(NDSHL)
C-----------------------------------------------------------------------
      CHARACTER CHEISA(NDSHL)*1 , CHSTDA(NDSHL)*2
      CHARACTER CHQA(NDSHL)*2  , CHRA(NDSHL)*2
C-----------------------------------------------------------------------
      DATA  CHQA    /'1 ','2 ','3 ','4 ','5 ','6 ','7 ','8 ','9 ',
     &               '10','11','12','13','14','15', '16', '17', 
     &		     '18','19','20','21'/
C-----------------------------------------------------------------------
C
C---------------------------------------------------------------------
C---------------------------------------------------------------------
       IF(ICFSEL.LT.1.OR.ICFSEL.GT.4) THEN
           WRITE(I4UNIT(-1),*)
     &     'XXCFTR ERROR: IMPROPER CHOICE FOR ICFSEL'
           STOP
       ENDIF
       CSTR19=CSTRGI
C
C---------------------------------------------------------------------
C  SCAN LEVEL, CHECK IF EISSNER INPUT FORM OR NOT, PARSE INTO
C  SHELLS AND SET UP STRG, STRGE STRING ARRAYS.
C---------------------------------------------------------------------
C
      IF(ICFSEL.EQ.3.OR.ICFSEL.EQ.4)THEN
C-------- CHECK CONSISTENT WITH EISSNER
C         LEISS = .TRUE.
            IF(INDEX(CSTR19,'s').GT.0.OR.
     &         INDEX(CSTR19,'p').GT.0.OR.
     &         INDEX(CSTR19,'d').GT.0.OR.
     &         INDEX(CSTR19,'f').GT.0.OR.
     &         INDEX(CSTR19,'g').GT.0.OR.
     &         INDEX(CSTR19,'h').GT.0) THEN
               WRITE(I4UNIT(-1),*)
     &           'XXCFTR ERROR: CONFIGURATION STRING OF INCORRECT FORM'
               STOP
            ELSE
                 STRGE = CSTR19
                 READ(CSTR19,1045)(NELA(ISHEL),CHEISA(ISHEL),
     &                               ISHEL=1,NDSHL)
                 NSHEL = 0
                 IP = 0
                 MAXN = 0
   81            CONTINUE
                 IF(NSHEL.LT.NDSHL) THEN
                     IF(NELA(NSHEL+1).GT.0)THEN
                         NSHEL=NSHEL+1
                         NELA(NSHEL)=MOD(NELA(NSHEL),50)
                         CHSTDA(NSHEL)=CSTGRP(CHEISA(NSHEL))
                         IP = IP + NELA(NSHEL)*I4PGRP(CHEISA(NSHEL))
                         MAXN = MAX0(MAXN,I4NGRP(CHEISA(NSHEL)))
                         GO TO 81
                     ENDIF
                 ENDIF
                 IP = MOD(IP,2)
                 IF(NSHEL.GT.1) THEN
                     WRITE(STRG,1046)CHSTDA(1),CHQA(NELA(1)),
     &                    (CHSTDA(ISHEL),CHQA(NELA(ISHEL)),
     &                    ISHEL=2,NSHEL)
                 ELSE
                     WRITE(STRG,1046)CHSTDA(1),CHQA(NELA(1))
                 ENDIF
            ENDIF
      ELSEIF(ICFSEL.EQ.1.OR.ICFSEL.EQ.2)THEN
C-------- CHECK CONSISTENT WITH STANDARD
C         LEISS = .FALSE.
            IF(INDEX(CSTR19,'s').EQ.0.AND.
     &         INDEX(CSTR19,'p').EQ.0.AND.
     &         INDEX(CSTR19,'d').EQ.0.AND.
     &         INDEX(CSTR19,'f').EQ.0.AND.
     &         INDEX(CSTR19,'g').EQ.0.AND.
     &         INDEX(CSTR19,'h').EQ.0) THEN
               WRITE(I4UNIT(-1),*)
     &         'XXCFTR ERROR: CONFIGURATION STRING OF INCORRECT FORM'
               STOP
            ELSE
                 STRG = CSTR19
                 READ(CSTR19,1046)(CHSTDA(ISHEL),CHRA(ISHEL),
     &                             ISHEL=1,NDSHL)
                 NSHEL = 0
                 IP = 0
                 MAXN = 0
   83            CONTINUE
                 IF(NSHEL.LT.NDSHL) THEN
                     IF(CHRA(NSHEL+1).NE.' ') THEN
                         NELA(NSHEL+1)=I4fctn(CHRA(NSHEL+1),IABT)
                         NSHEL=NSHEL+1
                         CHEISA(NSHEL)=CEIGRP(CHSTDA(NSHEL))
                         IP = IP + NELA(NSHEL)*I4PGRP(CHEISA(NSHEL))
                         MAXN = MAX0(MAXN,I4NGRP(CHEISA(NSHEL)))
                         GO TO 83
                     ENDIF
                 ENDIF
                 IP = MOD(IP,2)
                 IF(NSHEL.GT.1) THEN
                     WRITE(STRGE,1045)NELA(1),CHEISA(1),
     &                    (NELA(ISHEL)+50,CHEISA(ISHEL),ISHEL=2,NSHEL)
                 ELSE
                     WRITE(STRGE,1045)NELA(1),CHEISA(1)
                 ENDIF
            ENDIF
      ENDIF
C
      IF (ICFSEL.EQ.1.OR.ICFSEL.EQ.3) THEN
         CSTRGO=STRG
      ELSEIF (ICFSEL.EQ.2.OR.ICFSEL.EQ.4) THEN
         CSTRGO=STRGE
      ENDIF
C
      RETURN
C
C-----------------------------------------------------------------------
 1045 FORMAT(21(I2,1A1))
 1046 FORMAT(1X,21(1A2,A2,1X))
C-----------------------------------------------------------------------
      END
