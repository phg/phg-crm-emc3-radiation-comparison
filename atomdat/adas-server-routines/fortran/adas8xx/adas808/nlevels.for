CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/nlevels.for,v 1.1 2004/07/06 14:23:46 whitefor Exp $ Date $Date: 2004/07/06 14:23:46 $
CX
       function nlevels(ioccup)
       implicit none
c-----------------------------------------------------------------------
       integer  ndshell
c-----------------------------------------------------------------------
       parameter (ndshell=21)
c-----------------------------------------------------------------------
       integer  ioccup(ndshell), shmax(ndshell), njvals(14, 4), 
     &		lvals(ndshell)
       integer  nlevels, i
c-----------------------------------------------------------------------
       data shmax/2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22/
       data lvals/0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5/
       data njvals/1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     &		   2, 5,  5,  5,  2,  1, -1, -1, -1, -1, -1, -1, -1, -1,
     &		   2, 9, 19, 29, 36, 29, 19,  9,  2,  1, -1, -1, -1, -1,
     &		   2,13, 41,106,158,295,326,295,158,106, 41, 13,  2,  1/
c-----------------------------------------------------------------------

       nlevels=1
       do 10 i=1,ndshell
          if ((ioccup(i).lt.shmax(i)).and.(ioccup(i).gt.0)) then
	     nlevels=nlevels*njvals(ioccup(i),lvals(i)+1)
	  endif
10     continue       
c       print *,'no of levels=',nlevels
       
       return 
       end
