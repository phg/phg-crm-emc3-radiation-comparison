CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/promotion.for,v 1.1 2004/07/06 14:37:38 whitefor Exp $ Date $Date: 2004/07/06 14:37:38 $
CX
       PROGRAM PROMOTION
c=======================================================================
c  ****************** fortran77 program: promotion ********************
c
c  purpose: This program takes in a ground configuration and produces
c           a set of promoted configurations.
c-----------------------------------------------------------------------
c
c subroutine:
c   
c          (c*100) stndo    = promoted configuration in standard form
c          (c*1)   lastlarr = array containg the last l values of the 
c                             promoted configuraions
c          (c*1)   grndl    = last value of l in the ground string
c          (c*100) confarr  = configuration array
c          (c*1)   cvalch   = valence shell character
c          
c
c          (i*4)   chksum   = a check for the correct number of 
c                             electrons in the promoted state.
c          (i*4)   i	    = general use
c          (i*4)   icut     = the first mentioned quantum shell or the 
c                  	      last full shell plus 1 
c          (i*4)   igrndpar = parity of ground configuration.
c          (i*4)   ihigh    = no. of electrons in the highest shell	 
c          (i*4)   ilow     = no. of electrons in the lowest shell
c          (i*4)   imin     = minimum value of occupation array  
c          (i*4)   indxval  = index no. for valence shell
c          (i*4)   ioccup   = no. of electrons in the shell
c          (i*4)   ipar     = parity of the non ground shells
c          (i*4)   iprom    = switch value to activate inner shell
c  			      promotions
c          (i*4)   ivaln    = n quantum number of shell
c          (i*4)   iz0      = nuclear charge
c          (i*4)   iz1      = ion charge + 1
c          (i*4)   j	    = general use
c          (i*4)   k	    = general use
c          (i*4)   l	    = general use
c          (i*4)   larr     = array of l values for 
c          (i*4)   length   = length of character string stndo
c          (i*4)   len2     = length of string stndo
c          (i*4)   lhighest = highest value of l
c          (i*15)  limar    = limit array
c          (i*4)   llowest  = lowest value of l
c          (i*4)   nconf    = no. of configurations
c          (i*4)   ncount   = no. of valence shells
c          (i*4)   nhighest = highest n-shell promoted to
c          (i*4)   nlowest  = lowest n=shell promoted to
c          (i*4)   nmaxdn   = maximum change in n-shell
c          (i*4)   nmindn   = minimum change in n-shell
c          (i*4)   nopen    = no. of open shells in the configuration
c          (i*4)   nval     = storage number for valence shells
c          (i*4)   shmax    = maximum shell capacity
c          (i*4)   val      = 
c          (i*4)   iprom_arr1 = allowed low inner shell promotions
c          (i*4)   iprom_arr2 = allowed high inner shell promotions
c          (i*4)   iprom_low  = lowest inner shell to be promoted from
c          (i*4)   iprom_high = highest inner shell to be promoted from
c          (i*4)   iprom_low_indx  = index no. of lowest inner shell to
c                                    be promoted from
c          (i*4)   iprom_high_indx = index no.of highest inner shell to
c                                    be promoted from
c
c
c routines:
c          routine     source     brief description
c          -------------------------------------------------------------
c          xxcftr      adas       converts a configuration character
c                                 string between eissner and standard 
c       			  form.
c          lenstr      adas       returns effective length of a string
c                                 (ignoring trailing blanks).
c          i4lgrp      adas       returns the angular momemtum quantum 
c                                 number of orbital given the 
c                                 eissner single hexadecimal character
c                                 form.
c          i4ngrp      adas       returns n quantum number given in
c                                 the eissner single hexadecimal
c                                 character form.
c			  				  				  
c  Author: Susan Turnbull
c  Date:  16th August 2001
c
c-----------------------------------------------------------------------			  
C-----------------------------------------------------------------------
       IMPLICIT NONE
C-----------------------------------------------------------------------
       INTEGER  NDCONF, NDSHELL, PIPEIN, PIPEOUT
C-----------------------------------------------------------------------
       PARAMETER (NDCONF=100, NDSHELL=21, PIPEIN=5, PIPEOUT=6)
C-----------------------------------------------------------------------
       INTEGER	CHKSUM		,I		,	  
     &		ICUT		,IFLG		,   
     &		IGRNDPAR	,IHIGH		,
     &		ILOW		,IMIN		,    
     &		INDXVAL		,IPROM		,IPROM_HIGH	,
     &		IPROM_HIGH_INDX	,IPROM_LOW	,IPROM_LOW_INDX	,
     &		IVALN		,IZ0		,IZ1		,
     &		I4NGRP		, 
     &		J		,K		,L		,  
     &		LEN2		,LENGTH		,
     &		LENSTR		,LHIGHEST	,LLOWEST	,
     &		NCONF		,NCOUNT		,
     &		NHIGHEST	, 
     &		NLOWEST		,NMAXDN		,	     
     &		NMINDN		,NOPEN(10)	,		      
     &		NVAL		,ISTART		,
     &		OPEN_VAL	,NMAXDN_1	,NMINDN_1	, 
     &		LHIGHEST_1	,LLOWEST_1	,NMAXDN_2	,
     &		NMINDN_2	,LHIGHEST_2	,LLOWEST_2	,
     &		ICNT

       INTEGER  IOCCUP(NDSHELL), IOCCUP_STORE(NDCONF+1,NDSHELL),     
     &		IPAR(NDCONF),    IPROM_ARR1(6),
     &          IPROM_ARR2(6),       LARR(NDSHELL),   
     &          SHMAX(NDSHELL),      NARR(NDSHELL)
C-----------------------------------------------------------------------
      CHARACTER GRNDL*1,         STNDO*100 ,
     &		CHR_TMP*5, GRND_CONFIG*100
               
      CHARACTER CONFARR(NDCONF)*100, CVALCH*1,        
     &		LASTLARR(NDCONF)*1,  CHSTD(NDSHELL)*1,
     &		SHL(NDSHELL)*2
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       DATA SHMAX/2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22/
       DATA LARR/0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5/
       DATA IPROM_ARR1/1,2,4,7,11,16/
       DATA IPROM_ARR2/1,3,6,10,15,21/
       DATA CHSTD/'1','2','3','4','5','6','7','8','9','A','B','C','D',
     &		'E','F','G','H','I','K','L','M'/
       DATA NARR/1,2,2,3,3,3,4,4,4,4,5,5,5,5,5,6,6,6,6,6,6/
       DATA SHL/'1s','2s','2p','3s','3p','3d','4s','4p','4d',
     &		'4f','5s','5p','5d','5f','5g','6s','6p','6d',
     &		'6f','6g','6h'/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  ZERO ARRAYS
C-----------------------------------------------------------------------
 
       DO 1 I=1,NDSHELL
          IOCCUP(I)=0
1      CONTINUE       
       DO 2 I=1,NDCONF
          IPAR(I)=0
	  CONFARR(I)=' '
	  LASTLARR(I)=' '
2      CONTINUE       
       DO 4 I=1,NDCONF+1
          DO 3 J=1,NDSHELL
             IOCCUP_STORE(I,J)=0
3         CONTINUE       
4      CONTINUE       

C-----------------------------------------------------------------------
C  READ IN GROUND CONFIGURATION AND PROMOTION RULES.
C-----------------------------------------------------------------------

       do 5 j=1,ndshell
          READ (PIPEIN,'(i2)')IOCCUP(J)
5      continue
       READ (PIPEIN,'(i2)')ICUT
       READ (PIPEIN,'(I6)')NVAL
       READ (PIPEIN,'(I3,I3)')IZ0, IZ1
       READ(PIPEIN,'(I3)')OPEN_VAL
       IF (OPEN_VAL.EQ.0) THEN
          READ (PIPEIN,'(I3,I3)')NMAXDN, NMINDN
          READ (PIPEIN,'(I3,I3)')LHIGHEST, LLOWEST
       ENDIF
       IF (OPEN_VAL.EQ.1) THEN
          READ (PIPEIN,'(I3,I3)')NMAXDN_1, NMINDN_1
          READ (PIPEIN,'(I3,I3)')LHIGHEST_1, LLOWEST_1
          READ (PIPEIN,'(I3,I3)')NMAXDN_2, NMINDN_2
          READ (PIPEIN,'(I3,I3)')LHIGHEST_2, LLOWEST_2
       ENDIF
       READ (PIPEIN,'(I3)')IPROM
       IF (IPROM.EQ.0) THEN
	  READ (PIPEIN,'(I3,I3)')IPROM_LOW,IPROM_HIGH
       ENDIF
       istart=icut+1       
C-----------------------------------------------------------------------
C  CONVERT GROUND CONFIGURATION TO VECTOR FORM.
C-----------------------------------------------------------------------


       DO 27 I=1,NDSHELL
          IOCCUP_STORE(1,I)=IOCCUP(I)
27     CONTINUE	  
    
       IF (NVAL.GE.100) THEN
          NCOUNT=2
	  NOPEN(1)=MOD(NVAL,100)
	  NOPEN(2)=INT(NVAL/100)
       ELSE
          NCOUNT=1
	  NOPEN(1)=MOD(NVAL,100)
       ENDIF
       
       CHKSUM=0	  
       DO 30 K=1,NDSHELL
	     CHKSUM = CHKSUM+IOCCUP(K)
30     CONTINUE
       IF (CHKSUM.NE.(IZ0-IZ1+1)) THEN
          WRITE(0,*)'WARNING: CHECK NUMBER OF ELECTRONS'
	  STOP
       END IF
       
C-----------------------------------------------------------------------
C  WRITE GROUND CONFIG IN FORM SUITABLE FOR ADF34 FILE
C-----------------------------------------------------------------------
       ICNT=1
       DO 36 J=ISTART,NDSHELL
          IF (IOCCUP(J).NE.0) THEN
             IF (J.EQ.ISTART) THEN
        	IF (IOCCUP(J).LT.10) THEN
       		   WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I1,1X)')
     &			SHL(J),IOCCUP(J)
        	ELSE
        	   WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I2)')
     &			SHL(J),IOCCUP(J)
        	ENDIF
		ICNT=ICNT+5
        	GOTO 36
             ENDIF
             IF (IOCCUP(J).LT.10) THEN
        WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I1,1X)')SHL(J),IOCCUP(J)
		ICNT=ICNT+5
             ELSE
        WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I2)')SHL(J),IOCCUP(J)
		ICNT=ICNT+5
             ENDIF
          ENDIF
36     CONTINUE
       GRND_CONFIG=STNDO
       WRITE(0,*)'ground=',STNDO
       
C-----------------------------------------------------------------------
C  GENERATE AN ADF34 DATAFILE.     
C-----------------------------------------------------------------------

       IGRNDPAR=0
       DO 45 I=1,NDSHELL
          IF ((IOCCUP(I).NE.0).AND.(IOCCUP(I).NE.SHMAX(I))) THEN
             IGRNDPAR = IGRNDPAR+(LARR(I)*IOCCUP(I))
	  END IF
45     CONTINUE
       IGRNDPAR = MOD(IGRNDPAR,2)

C-----------------------------------------------------------------------
C  EVALUATE LIMITS FOR CONFIGURATION PROMOTIONS
C-----------------------------------------------------------------------

       NCONF=1
       IFLG=0
33     CONTINUE       
       IF (NCOUNT.EQ.1) THEN 
          INDXVAL = NOPEN(1)
       ENDIF
       IF ((NCOUNT.EQ.2).AND.(IFLG.EQ.0)) THEN
          INDXVAL = NOPEN(2)
       ENDIF
       IF ((NCOUNT.EQ.2).AND.(IFLG.EQ.1)) THEN
          INDXVAL = NOPEN(1)
       ENDIF
       IOCCUP(INDXVAL) = IOCCUP(INDXVAL)-1
       
       WRITE(CVALCH, '(A1)') CHSTD(INDXVAL)
       IVALN = I4NGRP(CVALCH)
       IF (OPEN_VAL.EQ.0) THEN
          NLOWEST = IVALN+NMINDN
          NHIGHEST = IVALN+NMAXDN
       ENDIF
       IF (OPEN_VAL.EQ.1) THEN
          IF ((NCOUNT.EQ.2).AND.(IFLG.EQ.0)) THEN
             NLOWEST = IVALN+NMINDN_2
             NHIGHEST = IVALN+NMAXDN_2
	     LLOWEST=LLOWEST_2
	     LHIGHEST=LHIGHEST_2
C	     WRITE(0,*)NMINDN_2,NMAXDN_2,LLOWEST_2,LHIGHEST_2
	  ENDIF
          IF ((NCOUNT.EQ.2).AND.(IFLG.EQ.1)) THEN
             NLOWEST = IVALN+NMINDN_1
             NHIGHEST = IVALN+NMAXDN_1
	     LLOWEST=LLOWEST_1
	     LHIGHEST=LHIGHEST_1
C	     WRITE(0,*)NMINDN_1,NMAXDN_1,LLOWEST_1,LHIGHEST_1
	  ENDIF
       ENDIF
       IF (NHIGHEST.GT.6) NHIGHEST=6
       IF (NLOWEST.LE.0) NLOWEST=1
       IF (LHIGHEST.GT.5) LHIGHEST=5
       IF (LLOWEST.LT.0) LLOWEST=0
       ILOW=IPROM_ARR1(NLOWEST)
       IHIGH=IPROM_ARR2(NHIGHEST)
c       WRITE(0,*)ILOW,IHIGH, IFLG, NLOWEST,NHIGHEST
C       WRITE(0,*)NOPEN, NCOUNT
            
C-----------------------------------------------------------------------
C  ASSEMBLE THE PROMOTED VECTORS FOR VALENCE SHELL PROMOTIONS AND
C  CONVERT VECTORS BACK TO STANDARD FORM.
C-----------------------------------------------------------------------
       DO 50 I = ILOW,IHIGH
          IF (NARR(INDXVAL).GT.NARR(I)) THEN
	     IF (LARR(I).GT.LLOWEST) GOTO 50
	  ELSE
	     IF (LARR(I).GT.LHIGHEST) GOTO 50
	  ENDIF
	  IF (I.EQ.INDXVAL) GO TO 50
	  IF (IOCCUP(I).GE.SHMAX(I)) GO TO 50
          IOCCUP(I) = IOCCUP(I)+1
          NCONF = NCONF+1
          DO 55 J=1,NDSHELL
             IOCCUP_STORE(NCONF,J)=IOCCUP(J)
55        CONTINUE	  
	  IPAR(NCONF)=0
          LASTLARR(NCONF)=' '

          CHKSUM=0	  
	  DO 60 K=1,NDSHELL
	        CHKSUM = CHKSUM+IOCCUP(K)
60        CONTINUE
          IF (CHKSUM.NE.(IZ0-IZ1+1)) THEN
	     WRITE(0,*) 'WARNING CHECKSUM ERROR'
	     STOP
	  END IF

          DO 65 J=1,NDSHELL
             IF ((IOCCUP(J).NE.0).AND.(IOCCUP(J).NE.SHMAX(J))) THEN
                IPAR(NCONF) = IPAR(NCONF)+(LARR(J)*IOCCUP(J))
	     END IF
65        CONTINUE
          IPAR(NCONF) = MOD(IPAR(NCONF),2)
          	  
       ICNT=1
       DO 76 J=ISTART,NDSHELL
          IF (IOCCUP(J).NE.0) THEN
             IF (J.EQ.ISTART) THEN
        	IF (IOCCUP(J).LT.10) THEN
       		   WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I1,1X)')
     &			SHL(J),IOCCUP(J)
        	ELSE
        	   WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I2)')
     &			SHL(J),IOCCUP(J)
        	ENDIF
		ICNT=ICNT+5
        	GOTO 76
             ENDIF
             IF (IOCCUP(J).LT.10) THEN
        WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I1,1X)')SHL(J),IOCCUP(J)
		ICNT=ICNT+5
             ELSE
        WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I2)')SHL(J),IOCCUP(J)
		ICNT=ICNT+5
             ENDIF
          ENDIF
76     CONTINUE

	  
	  LEN2 = LENSTR(STNDO)
	  LASTLARR(NCONF) = STNDO(LEN2 - 1:LEN2 - 1)
          CONFARR(NCONF) = STNDO	  
	  IOCCUP(I) = IOCCUP(I)-1
          WRITE(0,*)'STNDO prom=',STNDO
50     CONTINUE  
       IF ((NCOUNT.EQ.2).AND.(IFLG.EQ.0)) THEN
          IFLG=1
          IOCCUP(INDXVAL) = IOCCUP(INDXVAL)+1
          GOTO 33
       ENDIF

C-----------------------------------------------------------------------
C  ASSEMBLE THE PROMOTED VECTORS FOR INNER SHELL PROMOTIONS AND
C  CONVERT VECTORS BACK TO STANDARD FORM.
C-----------------------------------------------------------------------

       
       IF (IPROM_LOW.LE.0) IPROM_LOW=1
       IF (IPROM_HIGH.GT.6) IPROM_HIGH=6
       IOCCUP(INDXVAL) = IOCCUP(INDXVAL)+1
       IF (IPROM.EQ.0) THEN
       WRITE(0,*)'INNER SHELL',IPROM_LOW,IPROM_HIGH
          IPROM_LOW_INDX=IPROM_ARR1(IPROM_LOW)
          IPROM_HIGH_INDX=IPROM_ARR2(IPROM_HIGH)
          DO 99, L=IPROM_LOW_INDX,NOPEN(NCOUNT)-1 
	     IF((L.EQ.NOPEN(1)).OR.(L.EQ.NOPEN(2))) GOTO 99
             IF (IOCCUP(L).GT.0) THEN
	        IOCCUP(L) = IOCCUP(L)-1
    	     ELSE
	        GOTO 99
	     ENDIF
	     DO 150 I = IPROM_LOW_INDX,IPROM_HIGH_INDX
                IMIN=1
		IOCCUP(I) = IOCCUP(I)+1
	        IF (I.EQ.L) GO TO 155
	        IF (IOCCUP(I).GT.SHMAX(I)) GO TO 155
                NCONF = NCONF+1
                DO 154 J=1,NDSHELL
                   IOCCUP_STORE(NCONF,J)=IOCCUP(J)
154             CONTINUE

                CHKSUM=0
	        DO 160 K=1,NDSHELL
	           CHKSUM = CHKSUM+IOCCUP(K)
160             CONTINUE
164             CONTINUE
                IF (IOCCUP(IMIN).EQ.SHMAX(IMIN)) THEN
	           IMIN=IMIN+1
	           GOTO 164
	        ENDIF
161             CONTINUE
		IF ((IOCCUP(IMIN).GE.10).OR.(IOCCUP(IMIN).EQ.0)) THEN
		   IMIN=IMIN-1
		   GOTO 161
		ENDIF

      	        IF (IMIN.LE.0) IMIN=1

                IF (CHKSUM.NE.(IZ0-IZ1+1)) THEN
	           WRITE(0,*)'WARNING: CHECK NUMBER OF ELECTRONS'
	           STOP
	        END IF

	        IPAR(NCONF)=0
                LASTLARR(NCONF)=' '
             DO 165 J=1,NDSHELL
                IF ((IOCCUP(J).NE.0).AND.(IOCCUP(J).NE.SHMAX(J))) THEN
                      IPAR(NCONF) = IPAR(NCONF)+(LARR(J)*IOCCUP(J))
	        END IF
165          CONTINUE
                IPAR(NCONF) = MOD(IPAR(NCONF),2)
          
       ICNT=1
       DO 176 J=ISTART,NDSHELL
          IF (IOCCUP(J).NE.0) THEN
             IF (J.EQ.ISTART) THEN
        	IF (IOCCUP(J).LT.10) THEN
       		   WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I1,1X)')
     &			SHL(J),IOCCUP(J)
        	ELSE
        	   WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I2)')
     &			SHL(J),IOCCUP(J)
        	ENDIF
		ICNT=ICNT+5
        	GOTO 176
             ENDIF
             IF (IOCCUP(J).LT.10) THEN
        WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I1,1X)')SHL(J),IOCCUP(J)
		ICNT=ICNT+5
             ELSE
        	WRITE(STNDO(ICNT:ICNT+4),'(1X,A2,I2)')SHL(J),IOCCUP(J)
		ICNT=ICNT+5
             ENDIF
          ENDIF
176     CONTINUE
          WRITE(0,*)'STNDO=',STNDO
	  
	        LEN2 = LENSTR(STNDO)
	        LASTLARR(NCONF) = STNDO(LEN2 - 1:LEN2 - 1)
                CONFARR(NCONF) = STNDO	  
155	        CONTINUE
	        IOCCUP(I) = IOCCUP(I)-1
150          CONTINUE
             IOCCUP(L) = IOCCUP(L)+1
99        CONTINUE  
        ENDIF       

c-----------------------------------------------------------------------
c  output results to the screen in standard form.
c-----------------------------------------------------------------------
       
       WRITE(PIPEOUT,*)NCONF
       CALL XXFLSH(PIPEOUT)
       WRITE(PIPEOUT,'(a100)')grnd_config
       CALL XXFLSH(PIPEOUT)
       WRITE(PIPEOUT,'(I2)')NDSHELL
       CALL XXFLSH(PIPEOUT)
       DO 144 J=1,NDSHELL
          WRITE(PIPEOUT,'(I2)')IOCCUP_STORE(1, J)
          CALL XXFLSH(PIPEOUT)
144    CONTINUE	    
       DO 145 I=2, NCONF
         WRITE(PIPEOUT,'(a100)')CONFARR(I)
         CALL XXFLSH(PIPEOUT)
	 WRITE(PIPEOUT,'(i1)') IPAR(I)
         CALL XXFLSH(PIPEOUT)
	 
c	 ,LASTLARR(I)
c         CALL XXFLSH(PIPEOUT)
	 DO 146 J=1,NDSHELL
	    WRITE(PIPEOUT,'(I2)')IOCCUP_STORE(I, J)
            CALL XXFLSH(PIPEOUT)
146      CONTINUE	    
145    CONTINUE

       
c-----------------------------------------------------------------------       
  10   FORMAT (a100)
  35   FORMAT (a70)
  40   FORMAT (3x, i2, 3x, i2, 3x, a2, 1x, a16, a100)
  70   FORMAT (i1, a1)
  80   FORMAT (i2, a1)
 180   FORMAT (1x,i1, a1, '1')
 190   FORMAT (i3, i3, a2)
 191   FORMAT (i3, i3)
 192   FORMAT (i3)
 193   FORMAT (a100,i3)
C-----------------------------------------------------------------------
      END
c=======================================================================
