CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/valence.for,v 1.1 2004/07/06 15:27:57 whitefor Exp $ Date $Date: 2004/07/06 15:27:57 $
CX
       subroutine valence(ioccup,nval, ncount, nopen, ilast)
c-----------------------------------------------------------------------
c ******************fortran77 subroutine: valences*********************
c-----------------------------------------------------------------------
c    purpose: To evaluate the valence shell(s) of a given configuration
c		stored as occupation numbers in ioccup.
c-----------------------------------------------------------------------
       implicit none  
c-----------------------------------------------------------------------
       integer  ndconf, ndshell
c-----------------------------------------------------------------------
       parameter (ndconf=100, ndshell=21)
c-----------------------------------------------------------------------
       integer  i, ncount, nval, ilast
       integer  shmax(ndshell), ioccup(ndshell), nopen(10)
c-----------------------------------------------------------------------            
      data shmax/2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22/
c-----------------------------------------------------------------------
      ncount=0
      nopen(1)=0
      nopen(2)=0
      do 10 i=1, ndshell
          if (((ioccup(i).gt.0).and.(ioccup(i).lt.shmax(i))).or.
     &       ((i.eq.ilast).and.(ioccup(i).eq.shmax(i)))) then  
	     ncount = ncount+1
	     nopen(ncount)=i
	  end if
10     continue

       nval=nopen(1)+100*nopen(2)

       return
       end
