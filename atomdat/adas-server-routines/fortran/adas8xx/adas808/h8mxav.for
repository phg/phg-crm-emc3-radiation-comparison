CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/h8mxav.for,v 1.1 2004/07/06 14:02:19 whitefor Exp $ Date $Date: 2004/07/06 14:02:19 $
CX
        program h8mxav
        
        implicit none
        
        real*8 z0,z,zeff
        integer ixtyp
        real*8  acoeff,s,wi,wj,ei,ej
        
        integer nx,nt

        real*8  xa(100)        , omga(100)    
        real*8  tea(100)       , upsa(100)             

        integer i
        real*8 a,b

        open(10,file='input.txt',status='old')
        
        read(10,*) z0,z,zeff,ixtyp
        read(10,*) acoeff,wi,wj,ei,ej
        read(10,*) nx,nt
        do 10 i=1,nx
                read(10,*) a,b
                xa(i)=a
10              omga(i)=b
        do 20 i=1,nt
                read(10,*) a
20              tea(i)=a
        
        close(10)
        
c        write(0,*) nx,nt
                                         
        call h4mxwl(z0,z,zeff,ixtyp,acoeff,s,wi,wj,ei,ej,
     &                  nx,xa,omga,nt,tea,upsa)
        
        open(10,file='output.txt',status='unknown')
        do 30 i=1,nt
30              write(10,*) upsa(i)
        close(20)     
        
c        write(0,*) s
        
        end
