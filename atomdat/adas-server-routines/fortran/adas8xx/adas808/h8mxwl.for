CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/h8mxwl.for,v 1.1 2004/07/06 14:02:29 whitefor Exp $ Date $Date: 2004/07/06 14:02:29 $
CX
       subroutine h4mxwl( z0    , z      , zeff   ,  
     &                    ixtyp , acoeff , s      ,   
     &                    wi    , wj     , ei     , ej     , 
     &                    nx    , xa     , omga   , 
     &                    nt    , tea    , upsa   
     &                   )
       implicit none
c-----------------------------------------------------------------------
c
c  **************** fortran77 subroutine: h4mxwl.for *********************
c
c  purpose:  analyse electron impact collision strength data and convert 
c  	     to Maxwell averaged collision strengths.
c
c  notes:    data is fitted with approximate forms to aid interpolation 
c            depending on the transition type. these are
c                 1. dipole                                             
c                 2. non-dipole                                         
c                 3. spin change                                        
c                 4. other                                              
c
c  subroutine:
c  
c  input : (r*8)  z0       = nuclear charge
c  input : (r*8)  z        = ion charge
c  input : (r*8)  zeff     = ion charge +1
c  input : (i*4)  ixtyp    = 1  dipole transition   
c                          = 2  non-dipole transition    
c                          = 3  spin change transition       
c                          = 4  other         
c  input : (r*8)  wi       = lower level statistical weight    
c  input : (r*8)  wj       = upper level statistical weight     
c  input : (r*8)  ei       = lower level energy (in selected units)   
c  input : (r*8)  ej       = upper level energy        
c  input : (r*8)  acoeff   = A-value (s-1) (dipole case only)
c  input : (i*4)  nx       = number of X-parm/omega pairs         
c  input : (i*4)  nt       = number of output temperatures          
c  input : (r*8)  xa(i)    = input X-parm. values                          
c  input : (r*8)  omga(i)  = input omega values                         
c  input : (r*8)  tea(i)   = output electron temps.(K)
c              
c  output: (r*8)  upsa(i)  = output upsilons
c  output: (r*8)  s        = line strength (dipole case) otherwise zero

c          (i*4)  iibts  = 0  bad point option off            
c                        = 1  bad point option on            
c          (i*4)  iifpt  = 1  select one point optimising          
c                        = 2  select two point optimising       
c          (i*4)  iixop  = 0  optimising off           
c                        = 1  optimising on  (if allowed)      
c          (i*4)  iidif  = 0  ratio fitting for dipole x-sect
c                            (only with optimising)
c                        = 1  difference fitting for dipole x-sect   
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          egasm      adas      
c          h4sort     adas     sorts a vector pair by the first vector     
c          h4ftsp     adas     evaluate from a spline interpolant      
c          h4gspc     adas     generate spline precursors      
c          h4lnft     adas     perform linear interpolation    
c          h4fasy     adas     evaluate  from spline. of specified asympts. 
c          h4gasy     adas     create  specified asympts. for spline 
c          h4form     adas     form for spline independent variable      
c          h4spl3     adas     calculate spline of various end conditions      
c          eei        adas     evaluates exp(x)*E1(x)           
c          ee2        adas     evaluates exp(x)E2(x)           
c          i4unit     adas     fetch unit number for output of messages
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c  date:   19/07/02 
c
c  update:  
c
c-----------------------------------------------------------------------
       integer   ngaus   , istdim
c-----------------------------------------------------------------------
       parameter ( ngaus = 12 , istdim = 40 )
c-----------------------------------------------------------------------
       integer   i4unit
       integer   ixtyp     , nx        , nt
       integer   iibts     , iifpt     , iixop     , iidif
       integer   ibpts     , ifpts     , ixopt     , idiff
       integer   i         , it        , iter      , k
       integer   iends     , iend1     , iendn     , iforms                       
c-----------------------------------------------------------------------
       real*8    z0        , z1        , z         , zeff 
       real*8    eij       , ei        , ej
       real*8    wi        , wj
       real*8    acoeff    , s         , fij       , te
       real*8    fxc2      , fxc3      
       real*8    g1        , gn        ,
     &           a1        , b1        , an        , bn                
       real*8    x1        , xn        , dx1       , dxn
       real*8    x         , xe        , ate       , sum
       real*8    y         , dy
       real*8    h4form    
       real*8    eei       , ee2
c-----------------------------------------------------------------------
       real*8   xa(nx)          , omga(nx)    
       real*8   tea(nt)         , upsa(nt)             
       real*8   wg(12)          , xg(12)
       real*8   apupsa(istdim)  ,
     &          xoa(istdim)     , yoa(istdim)                
       real*8   xsa(istdim)     , ysa(istdim)    ,
     &          xos(istdim)     , yos(istdim)             
       real*8   apout(istdim)   , apoma(istdim)  , difoma(istdim)
       real*8   p1(3)           , q1(3)          , 
     &          pn(3)           , qn(3)          ,
     &          a1ry(istdim)    , b1ry(istdim)   ,
     &          anry(istdim)    , bnry(istdim)
       real*8   c1(istdim,istdim-1)  , c2(istdim,istdim-1)         ,
     &          c3(istdim,istdim-1)  , c4(istdim,istdim-1)                
c-----------------------------------------------------------------------
       external h4form                                                   
c-----------------------------------------------------------------------
       common /espl3/iend1 , iendn , g1    , gn   ,
     &               a1    , b1    , an    , bn   ,
     &               p1    , q1    , pn    , qn   ,
     &               a1ry  , b1ry  , anry  , bnry 
c-----------------------------------------------------------------------
c  gauss-laguerre quadrature coefficients (12 point)                               
c-----------------------------------------------------------------------
       data wg/2.64731371055d-1 ,3.77759275873d-1 ,                     
     &         2.44082011320d-1 ,9.04492222117d-2 ,                     
     &         2.01023811546d-2 ,2.66397354187d-3 ,                     
     &         2.03231592663d-4 ,8.36505585682d-6 ,                     
     &         1.66849387654d-7 ,1.34239103052d-9 ,                     
     &         3.06160163504d-12,8.14807746743d-16/                     
       data xg/0.115722117358d0 ,0.611757484515d0 ,                     
     &         1.512610269776d0 ,2.833751337744d0 ,                     
     &         4.599227639418d0 ,6.844525453115d0 ,                     
     &         9.621316842457d0 ,1.3006054993306d1,                     
     &         1.7116855187462d1,2.2151090379397d1,                     
     &         2.8487967250984d1,3.7099121044467d1/                  
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initial control settings        
c-----------------------------------------------------------------------


c        write(0,*) z0    , z      , zeff   ,  
c     &                    ixtyp , acoeff , s      ,   
c     &                    wi    , wj     , ei     , ej     , 
c     &                    nx    ,  
c     &                    nt   
       
c      write(0,*) (xa(i),omga (i),i=1,nx)
c      write(0,*) (tea(i),i=1,nt)


       iibts = 0
       iifpt = 2
       iixop = 1
       iidif = 0
       
       do 1 i =1,nt
         upsa(i) = 0.0d0
 1     continue
c-----------------------------------------------------------------------
c  obtain line strength and oscillator strength if type 1 transition        
c-----------------------------------------------------------------------
       z1 = z+1
       eij=ej-ei 
                                                              
       if(ixtyp.eq.1)then                                               
           s   = 3.73491e-10*wj*acoeff/(eij*eij*eij)                        
           fij = 3.333333d-1*eij*s/wi
       else
           s   = 0.0d0
	   fij = 0.0d0                                     
       endif                                                            
       
c-----------------------------------------------------------------------
c  convert to best variables for splining depending upon transition type
c-----------------------------------------------------------------------
       if(ixtyp.eq.1)then                                               
           ifpts=iifpt                        
   72      if(nx.le.1.or.omga(1).ge.omga(nx).or.ifpts.eq.1)then           
               ifpts=1                                                  
               fxc3=1.333333d0                                          
               fxc2=dexp(omga(1)/(fxc3*s))-xa(1)                         
               if(fxc2.gt.0.0d0)then                                    
                   ixopt=iixop                                          
               else                                                     
                   ixopt=0                                              
               endif                                                    
           elseif(ifpts.eq.2)then                                       
               fxc2=0.0d0                                               
               do 73 iter=1,20  
c               write(0,*) fxc2,omga(1),omga(nx),xa(1),xa(nx)                                         
   73          fxc2=-xa(1)+(xa(nx)+fxc2)**(omga(1)/omga(nx))              
               fxc3=omga(1)/(s*dlog(xa(1)+fxc2))                          
               if(fxc2.gt.0.0d0)then                                    
                   ixopt=iixop                                          
               else                                                     
                   ifpts=1                                              
                   go to 72                                             
               endif                                                    
           endif                                                        
           if(ixopt.gt.0)then                                           
               idiff=iidif                                              
           else                                                         
               idiff=1                                                  
               fxc2=0.0d0                                               
               fxc3=1.333333d0                                          
           endif                                                        
       elseif(ixtyp.eq.2.and.iixop.eq.1)then                            
           ifpts=iifpt                                                  
   74      if(nx.le.1.or.ifpts.eq.1)then                               
               fxc2=0.0d0                                               
               fxc3=omga(1)                                               
               ixopt=iixop                                              
           elseif(ifpts.eq.2)then                                       
               fxc2=xa(1)*xa(nx)*(omga(1)-omga(nx))/(xa(nx)-xa(1))       
               fxc3=omga(1)-fxc2/xa(1)                                    
               if(fxc2+fxc3.gt.0.0d0.and.fxc3.gt.0.0d0)then              
                   ixopt=iixop                                          
               else                                                    
                   ifpts=1                                              
                   go to 74                                             
               endif                                                    
           endif                                                        
           idiff=0                                                      
       elseif(ixtyp.eq.3.and.iixop.eq.1)then                            
           ifpts=iifpt                                                  
   76      if(nx.le.1.or.omga(1).le.omga(nx).or.ifpts.eq.1)then           
               ifpts=1                                                  
               fxc2=100.0                                               
               fxc3=omga(1)*(xa(1)+fxc2)*(xa(1)+fxc2)                     
               ixopt=iixop                                              
           elseif(ifpts.eq.2)then                                       
               fxc2=(xa(nx)*dsqrt(omga(nx)/omga(1))-xa(1))/               
     &         (1.0d0 - dsqrt(omga(nx)/omga(1)))                           
               if(fxc2.le.0.1d0)  then
	           write(i4unit(-1),2000)
     &                   'type 3: fxc2 too small-limited to 0.1'
	       endif                            
               fxc2=dmax1(fxc2,0.1d0)                                   
               fxc3=omga(1)*(xa(1)+fxc2)*(xa(1)+fxc2)                     
               if(fxc2.gt.0.0d0)then                                    
                   ixopt=iixop                                          
               else                                                     
                   ifpts=1                                              
                   go to 76                                            
               endif                                                    
           endif                                                        
           idiff=0                                                      
       elseif(ixtyp.eq.4)then                                           
           idiff=1                                                      
       elseif(iixop.eq.0)then                                           
           fxc2=0.0d0                                                   
           fxc3=0.0d0                                                   
           ixopt=iixop                                                  
           ifpts=0                                                      
           idiff=1                                                      
       endif
       
       do 75 k=1,nx                                                   
        if(ixtyp.eq.1)then                                              
            apoma(k)=fxc3*s*dlog(xa(k)+fxc2)                            
            if(ixopt.eq.1.and.idiff.eq.0)then                           
                ysa(k)=omga(k)/apoma(k)                                   
            else                                                        
                ysa(k)=omga(k)-apoma(k)                                   
            endif                                                       
            xsa(k)=1.0d0/xa(k)
        elseif(ixtyp.eq.2.and.ixopt.eq.1)then                           
            apoma(k)=fxc3+fxc2/xa(k)                                    
            ysa(k)=omga(k)/apoma(k)                                       
            xsa(k)=1.0d0/xa(k)                                          
        elseif(ixtyp.eq.3.and.ixopt.eq.1)then                           
            apoma(k)=fxc3/(xa(k)+fxc2)**2                               
            ysa(k)=omga(k)/apoma(k)                                       
            xsa(k)=1.0/xa(k)                                            
        elseif(ixtyp.eq.4)then                                          
            apoma(k)=0.0d0                                              
            ysa(k)=dlog10(omga(k))                                        
            xsa(k)=1.0/xa(k)                                            
        elseif(ixopt.eq.0)then                                          
            apoma(k)=0.0d0                                              
            ysa(k)=omga(k)                                                
            xsa(k)=1.0d0/xa(k)                                          
        endif                                                           
        difoma(k)=omga(k)-apoma(k)                                        
   75  continue                              
       call h4sort(xsa,ysa,nx)                                          
       ibpts=iibts                                                      
       if(ibpts.le.0)then                                               
           iend1=1                                                      
           g1=0.0d0                                                     
           iendn=1                                                     
           gn=0.0d0                                                     
           iforms=ixtyp                                                 
           iends=1                                                      
           x1=xsa(1)                                                    
           dx1=0.01*x1                         
           call h4gasy( istdim ,
     &                  x1     , dx1    ,
     &                  h4form , iforms , iends
     &                 )                       
           iends=2                                                      
           xn=xsa(nx)                                                  
           dxn=0.01*xn                                                  
           call h4gasy( istdim ,
     &                  xn     , dxn    ,
     &                  h4form , iforms , iends
     &                )                    
           call h4gspc( istdim ,
     &                  xsa    , nx     ,
     &                  c1     , c2     , c3     , c4
     &                )                              
       endif                                                            
       do 200 it=1,nt                                              
        te=tea(it)                                                      
        ate=1.57890d5*eij/te                                           
        sum=0.0d0                                                      
        do 85 k=1,ngaus                                                 
         apout(k)=0.0d0                                                 
         xoa(k)=xg(k)/ate+1.0d0                                   
         x=1.0d0/xoa(k)                                                 
         xos(k)=x                                                      
         if(ibpts.gt.0)then                                             
             call h4lnft( istdim ,
     &                    x      , xsa   , y     , ysa   , nx
     &                  )                               
         else                                                           
             call h4fasy( istdim ,
     &                    x      , xsa   , nx    , ysa   , y    , dy   ,
     &                    c1     , c2    , c3    , c4    ,
     &                    h4form , iforms
     &                  )  
         endif                                                        
         yos(k)=y                                                      
         if(ixtyp.eq.1)then                                             
             apout(k)=fxc3*s*dlog(xoa(k)+fxc2)                          
             if(idiff.eq.0)then                                         
                yoa(k)=(y-1.0d0)*apout(k)                               
             else                                                      
                 yoa(k)=y                                              
             endif                                                      
         elseif(ixtyp.eq.2.and.ixopt.eq.1)then                          
             apout(k)=fxc3+fxc2/xoa(k)                                  
             yoa(k)=(y-1.0d0)*apout(k)                                  
         elseif(ixtyp.eq.3.and.ixopt.eq.1)then                          
             apout(k)=fxc3/(xoa(k)+fxc2)**2                             
             yoa(k)=(y-1.0d0)*apout(k)                                  
         elseif(ixtyp.eq.4)then                                         
             apout(k)=0.0d0                                             
             yoa(k)=10.0d0**y                                   
         elseif(ixopt.eq.0)then                                         
             apout(k)=0.0d0                                             
             yoa(k)=y                                                   
         endif                                                          
   85   sum=sum+wg(k)*yoa(k)                                      
        upsa(it)=sum                                                     
        if(ixtyp.eq.1)then                                              
            xe=(1.0d0+fxc2)*ate                                         
            apupsa(it)=fxc3*s*(dlog(1.0d0+fxc2)+eei(xe))                 
            upsa(it)=upsa(it)+apupsa(it)                                   
        elseif(ixtyp.eq.2.and.ixopt.eq.1)then                           
            xe=ate                                                      
            apupsa(it)=fxc3+fxc2*ate*eei(ate)                            
            upsa(it)=upsa(it)+apupsa(it)                                   
        elseif(ixtyp.eq.3.and.ixopt.eq.1)then                   
            xe=(1.0d0+fxc2)*ate                                 
            apupsa(it)=fxc3*ate*ate*ee2(xe)/xe                   
            upsa(it)=upsa(it)+apupsa(it)                           
        elseif(ixtyp.eq.4)then                                  
            apupsa(it)=0.0d0                                             
            upsa(it)=upsa(it)                                             
        elseif(ixopt.eq.0)then                                          
            apupsa(it)=0.0d0                                             
            upsa(it)=upsa(it)                                             
        endif 
	                                                          
  200  continue 
                                                     
c-----------------------------------------------------------------------
 2000 format(1x,32('*'),' h4mxwl warning ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
 2002 format(1x,32('*'),' h4mxwl error ',32('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------

      return
      end                                                               
