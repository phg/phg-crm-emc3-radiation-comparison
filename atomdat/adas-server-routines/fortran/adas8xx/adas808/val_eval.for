CX UNIX  - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas808/val_eval.for,v 1.1 2004/07/06 15:27:50 whitefor Exp $ Date $Date: 2004/07/06 15:27:50 $
CX
       program val_eval
       implicit none
c-----------------------------------------------------------------------      
       integer pipein, pipeout, ndshell
c-----------------------------------------------------------------------      
       parameter(pipein=5, pipeout=6, ndshell=21)
c-----------------------------------------------------------------------      
       integer ion_min, ion_max, icfsel, nshel, len, icut, iabt,
     &		nelec, ilast, ncount, nopen(10), i, lenstr, 
     &		i4jgrp, i4fctn, j, i4unit, iflag, icount, 
     &          test_nval, igrndpar
c-----------------------------------------------------------------------      
       integer	ioccup(ndshell), shmax(ndshell), nval(ndshell), 
     &		larr(ndshell)
c-----------------------------------------------------------------------      
       character config(100)*100, cstrgo*100, car*3, cval(21)*2, 
     &           strout*5
c-----------------------------------------------------------------------      
       data shmax/2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22/
       data cval/'1s','2s','2p','3s','3p','3d','4s','4p','4d','4f',
     &   '5s','5p','5d','5f','5g','6s','6p','6d','6f','6g','6h'/
       data larr/0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5/
c-----------------------------------------------------------------------      

       do 5 i=1,ndshell
          nval(i)=0
5      continue	  
       read(pipein,*)ion_min, ion_max
       
       icfsel=2
       icount=1
       do 12 i=ion_min,ion_max
          ilast=0
          ncount=0
          nopen(1)=0
          nopen(2)=0
	  iflag=0
          do 6 j=1, ndshell
	     ioccup(j)=0
6         continue

          read(pipein,14)config(i+1)
	  config(i+1)=' '//config(i+1)
          call xxcftr( icfsel, config(i+1), cstrgo , nshel)
          len = lenstr (cstrgo) 
          icut = i4jgrp(cstrgo(3:3))
          if (icut.eq.1) go to 15
          do 20 j=1,icut-1
             ioccup(j) = shmax(j)
20        continue            
15        continue
          do 25 j=1,nshel
             iabt=0
             car = cstrgo(3*j-2:3*j-1)
	     nelec=mod(i4fctn(car,iabt),50)
             ioccup(i4jgrp(cstrgo(3*j:3*j))) = nelec
             ilast = i4jgrp(cstrgo(3*j:3*j))  
25        continue 

	  do 10 j=1, ndshell
              if (((ioccup(j).gt.0).and.(ioccup(j).lt.shmax(j))).or.
     &          ((j.eq.ilast).and.(ioccup(j).eq.shmax(j)))) then  
	        ncount = ncount+1
	        nopen(ncount)=j
	     end if
10        continue
          if (ncount.eq.1) strout=cval(nopen(1)) 
	  if (ncount.eq.2) strout=cval(nopen(1))//' '//cval(nopen(2))
	  if (ncount.gt.2) then
	     write(0,*)'ERROR : GREATER THAN 2 VALENCE SHELLS'
	     stop
	  endif
	  
          test_nval=nopen(1)+100*nopen(2)
	  write(pipeout,*)test_nval
	  call xxflsh(pipeout)

          igrndpar=0
          do 45 j=1,ndshell
             if ((ioccup(j).ne.0).and.(ioccup(j).ne.shmax(j))) then
                igrndpar = igrndpar+(larr(j)*ioccup(j))
	     end if
45        continue
          igrndpar = mod(igrndpar,2)
	  write(pipeout,'(i1)')igrndpar
	  call xxflsh(pipeout)
	  
	  do 17 j=1, ndshell
	     if (test_nval.eq.nval(j)) iflag=1
17        continue
          write(pipeout,*)iflag
	  call xxflsh(pipeout)
	  if (iflag.eq.0) then
	     nval(icount)=nopen(1)+100*nopen(2)
	     write(pipeout,11) strout, ncount
	     call xxflsh(pipeout)
	     icount=icount+1
	  end if
12     continue	 
	     	     

11     format (a5, i3)
13     format (i3,i3)
14     format (a100)
       
       end
