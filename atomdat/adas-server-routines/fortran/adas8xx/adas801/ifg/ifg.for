C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/ifg.for,v 1.6 2004/07/06 14:08:31 whitefor Exp $ Date $Date: 2004/07/06 14:08:31 $
      Program IFG

      implicit none
C-----------------------------------------------------------------------
C
C This program generates the intermediate data file from the Cowan
C Code output. A full run must be performed. The required files and
C their unit numbers are
C
C     unit 20 : RCN.DAT
C     unit 19 : RCN.OUT
C     unit 21 : D11.DAT
C     unit 22 : RCG.OUT
C     unit 23 : SPECTRUM file of non-Born run
C     unit 24 : COLLISION.OUT
C     unit 25 : OUTPUT FILE
C
C 13-3-92 : 1 - Initial version
C 17-3-92 : 2 - Change ordering of terms from energy to position thus
C               maintaining reproducability and avoiding unassigned
C               energy designations influencing the ordering.
C  1-4-92   3 - Added number of open shells information
C               Indexing for levels and terms
C  6-5-92 : 4 - Sorting routines changed - they are now based on the
C               shellsort routines from "Numerical Recipes" book.
C               The contents of the label information have been changed
C               in subroutine produce after sorting to only store open
C               shell data. This was previously extracted when printing
C               the level information.
C 11-5-92 : 5 - Indexing by (parent)-S. The parents are identified in
C               three ways
C                  i) (parent)+single-electron
C                 ii) one open shell with equivalent electrons. No
C                     parents are identified.
C                iii) more than one group of equivalent electrons.
C                     the 'parent' is taken as the LS value of each
C                     equivalent electron group
C               All can be identified similarly by constructing a
C               parent from the open shells.
C 14-5-92 : 5 - All indexing information is now taken from the collision
C               file. The spectrum file is no longer required.
C 18-5-92 : 6 - Second parity forbidden transitions allowed.
C  8-6-92 : 7 - Cowan has been modified to provide a unique label for
C               each level. Change level reading routine and mark
C               re-labeled levels on output.
C  9-6-92 : 8 - Slater integral parameters and collision orders used
C               extracted from D11 file to provide all information
C               necessary to duplicate a run.
C 26-6-92 : 9 - Default energy criterion removed.
C 17-9-92 :10 - Transition probabilities for E2 and M1 transitions
C               taken from a seperate run.
C 27-11-92:11 - Replaced subroutine findconfigs. New output of
C               Eissner type for configurations.
C 18-5-93 :12 - Add orbital energies
C 27-8-93 :13 - If a collision rate calculation is not performed will
C               find the bundled A values
C 13-7-94 :14 - Restyle collision file to write impact energies and
C               temperatures at end of file and not for every entry
C 13-7-94 :15 - Change orbital information string to rank orbitals by
C               n and l AND output only those used.
C 12-10-94:16 - Change format of eav from f11.2 1pd11.6 - must make
C               in if2 code to read into a string followed by a
C               free-format read
C  4-10-96:17 - Change number of orbitals to 36 (from 21) to accommodate
C               up to n=8 energy levels
C   7-3-97:18 - In forbtrans read number of M1 and E2 transitions from
C               start of file for each partity. This caused problems
C               for the second parity forbidden transitions.
C  14-3-97:19 - Changes in produce and findformat
C                i) dimensions of nc are now nTerms from ncfg. This
C                   permits more than 8 configurations per parity to
C                   be properly sorted. Cases where there were more
C                   than 8 configs need to be re-assessed (particularly
C                   in the second parity).
C               ii) added test for I and K l-values
C  18-3-97:20 -  i) minor change to orbital. set string=' ' before
C                   filling it to write to output.
C               ii) additional check in levelfill to only add lpos to
C                   lab(,) if lr(numopen) is a valid L value.
C 30-10-97:21 - Changed orbital to use e-e correlated relativistic
C               orbital energies (EPS FGR) rather than EREL which
C               gives negative quantum defects (sometimes!)
C   8-2-00:22 - Major restructuring in order to incorporate it into
C               ADAS801. Implicit none is used. Reading of RCN input file
C               is removed as this information is passed in along with the
C               file names. The number of open shells is split by parity.
C               New versioning system (1.1) from now.
C
C
C---------------------------------------------------------------------
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 08-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          
C  DATE     : 01-11-2000
C
C  MODIFIED : Martin O'Mullane  
C             Removed data statement to initialise variables as it
C             caused trouble to the g77 compiler. Also removed a
C             statement which did not have a path to it.
C
C  VERSION  : 1.3                          
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane  
C             Account for case where only one parity is present.
C
C  VERSION  : 1.4                          
C  DATE     : 04-12-2002
C
C  MODIFIED : Martin O'Mullane  
C              - Add purity of IC level to IFG file.
C
C  VERSION  : 1.5
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C---------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h'
C---------------------------------------------------------------------
      integer     iutrcn  ,  iutd11   , iutrcg    ,  iutspec  , 
     &            iutcoll ,  iout 
      character   blank*132
      real*8      default
C-----------------------------------------------------------------------
      parameter  ( iutrcn  = 19,   iutd11  =  21,  iutrcg   = 22 ,
     &             iutspec = 23,   iutcoll =  24,  iout     = 25 )
      parameter  (blank    =  ' ')
      parameter  (default  = -99.99D0)
C-----------------------------------------------------------------------
      integer    iz0      , iz1      , nump1    , nump2  , is_batch ,    
     &           i        , j        , ierr     ,
     &           nshellp1 , nshellp2 ,
     &           jsp1     , jsp2     , nenrgs   , numA   ,
     &           numE2p1  , numE2p2  , numF1    , numF2  ,
     &           numM1p1  , numM1p2
C-----------------------------------------------------------------------
      logical    lpar1    , lpar2    , ratecalc , isfirst
C-----------------------------------------------------------------------
      character  inf*80   , str1*4   , str2*15
C-----------------------------------------------------------------------
      integer    p1format(nconfigs,ncfg) , p2format(nconfigs,ncfg) ,
     &           relabp1(nJ,nLevels)     , relabp2(nJ,nLevels)     ,
     &           ipurep1(nJ,nLevels)     , ipurep2(nJ,nLevels)     ,
     &           mulp1(nJ,nLevels)       , mulp2(nJ,nLevels)       ,
     &           ktermp1(nJ,nLevels)     , ktermp2(nJ,nLevels)     , 
     &           levsp1(nJ),levsp2(nJ)                             ,
     &           indlevp1(nJ,nLevels)    , indlevp2(nJ,nLevels)    ,
     &           indsgp1(nJ,nLevels)     , indsgp2(nJ,nLevels)     ,
     &           indlsp1(nJ,nLevels)     , indlsp2(nJ,nLevels)
C-----------------------------------------------------------------------
      real*8     xs(ndenrgs)             , tev(ndenrgs)            ,
     &           elevp1(nJ,nLevels)      , elevp2(nJ,nLevels)      ,
     &           jvalp1(nJ)              , jvalp2(nJ)
C-----------------------------------------------------------------------
      character  p1configs(nconfigs)*40  , p2configs(nconfigs)*40  ,
     &           p1sscfgs(nconfigs)*24   , p2sscfgs(nconfigs)*24   ,
     &           labp1(nJ,nLevels)*132   , labp2(nJ,nLevels)*132
C-----------------------------------------------------------------------
C       data ((p1format(i,j),i=1,nconfigs),j=1,ncfg) /ncc*0/
C       data ((p2format(i,j),i=1,nconfigs),j=1,ncfg) /ncc*0/
C       data ((labp1(i,j),i=1,nJ),j=1,nLevels)       /nmx*blank/
C       data ((labp2(i,j),i=1,nJ),j=1,nLevels)       /nmx*blank/
C       data ((elevp1(i,j),i=1,nJ),j=1,nLevels)      /nmx*default/
C       data ((elevp2(i,j),i=1,nJ),j=1,nLevels)      /nmx*default/
C       data ((relabp1(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((relabp2(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((mulp1(i,j),i=1,nJ),j=1,nLevels)       /nmx*0/
C       data ((mulp2(i,j),i=1,nJ),j=1,nLevels)       /nmx*0/
C       data ((ktermp1(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((ktermp2(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((indlevp1(i,j),i=1,nJ),j=1,nLevels)    /nmx*0/
C       data ((indlevp2(i,j),i=1,nJ),j=1,nLevels)    /nmx*0/
C       data ((indsgp1(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((indsgp2(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((indlsp1(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C       data ((indlsp2(i,j),i=1,nJ),j=1,nLevels)     /nmx*0/
C-------------------------------------------------------------------------

      do j = 1, ncfg
        do i = 1, nconfigs
          p1format(i,j) = 0
          p2format(i,j) = 0
        end do
      end do
      
      do j = 1, nLevels
        do i = 1, nJ
          labp1(i,j)    = blank
          labp2(i,j)    = blank
          elevp1(i,j)   = default
          elevp2(i,j)   = default
          relabp1(i,j)  = 0
          relabp2(i,j)  = 0
          ipurep1(i,j)  = -1
          ipurep2(i,j)  = -1
          mulp1(i,j)    = 0
          mulp2(i,j)    = 0
          ktermp1(i,j)  = 0
          ktermp2(i,j)  = 0
          indlevp1(i,j) = 0
          indlevp2(i,j) = 0
          indsgp1(i,j)  = 0
          indsgp2(i,j)  = 0
          indlsp1(i,j)  = 0
          indlsp2(i,j)  = 0
        end do
      end do
      

C-------------------------------------------------------------------------
C Read instruction file and open required files.

      read(5,*)is_batch
      
      read(5,*)iz0, iz1
      read(5,*)nump1, nump2

      if (nump1.eq.0) goto 999

      do i = 1, nump1
        read(5,'(a)')p1configs(i)
      end do 
      if (nump2.gt.0) then
         do i = 1, nump2
           read(5,'(a)')p2configs(i)
         end do
      endif

      read(5,'(A)')inf
      open(iutrcn, file=inf, status='old')
      read(5,'(A)')inf
      open(iutd11, file=inf, status='old')
      read(5,'(A)')inf
      open(iutrcg, file=inf, status='old')
      read(5,'(A)')inf
      open(iutspec, file=inf, status='old')
      read(5,'(A)')inf
      open(iutcoll, file=inf, status='old')
      read(5,'(A)')inf
      open(iout, file=inf, status='unknown')


C-------------------------------------------------------------------------
C Extract information on the configurations and temperatures.
      
      if (is_batch.le.0) write(6,*)is_batch
      
      write(iout,'(1x,2i4)')iz0,iz1
      
C Get the configurations in Eissner form
      
      lpar1 = .TRUE.
      lpar2 = (nump2.GT.0) 

      call eissner(nump1, p1configs, p1sscfgs)
      if (lpar2) call eissner(nump2, p2configs, p2sscfgs)

C Get the temperatures and energies from the collision file. Write
C these along with the orbital energies to iout.

      ierr=1
      call fndstr(iutcoll,1,8,'ENG/TEMP',ierr)
      if (ierr.eq.-99) goto 999
      call advance(iutcoll,1)
      read(iutcoll,200,end=250)nenrgs
      if (nenrgs.eq.0) then
         ratecalc=.FALSE.
      else
         ratecalc=.TRUE.
         read(iutcoll,204,end=250)(xs(j),j=1,nenrgs)
         read(iutcoll,204,end=250)(tev(j),j=1,nenrgs)
      endif
 200  format(1x,i4)
 204  format(1p,7d10.3)
      rewind iutcoll
 250  continue
 
      do i=1,nenrgs
        tev(i)=tev(i)*1.1604428D4
      end do

 
      write(iout,200)nenrgs
      write(iout,204)(xs(j),j=1,nenrgs)
      write(iout,*)' '
      write(iout,204)(tev(j),j=1,nenrgs)
      write(iout,*)' '
 
      call orbital(iutrcn, iout)

      if (is_batch.le.0) write(6,*)is_batch
 
 
 
C-------------------------------------------------------------------------
C Get the labels and energies of the IC levels from interrogation RCG 
C (iutrcg) output file

 
      call getnshell(iutd11, nshellp1, nshellp2)
      call findformat(iutd11, p1format, nshellp1, nump1)
      if (lpar2) call findformat(iutd11, p2format, nshellp2, nump2)

 300  read(iutrcg,350,end=400)str1,str2
 350  format(23x,a4,a15)
 
      if (str2.eq.'parity number 1') then
        call labelfill(iutrcg, nshellp1, labp1, jvalp1, levsp1, jsp1)
      elseif (str2.eq.'parity number 2') then
        call labelfill(iutrcg, nshellp2, labp2, jvalp2, levsp2, jsp2)
      endif
 
      if (str1.eq.'Para') then
        if (lpar1) call elevfill(iutrcg , elevp1  , relabp1 , levsp1 , 
     &                           jsp1   , ipurep1 )
        if (lpar2) call elevfill(iutrcg , elevp2  , relabp2 , levsp2 , 
     &                           jsp2   , ipurep2 )
      endif
 
      goto 300
 
 
 400  continue

 
 
C-------------------------------------------------------------------------
C Write the energy levels and level information to output file.

 
      write(iout,510)jvalp1(1),jvalp1(1)+jsp1-1
      do i=1,nump1
        write(iout,520)i,p1configs(i),p1sscfgs(i)
      end do
      write(iout,530)
  
      if (lpar1) call produce(iout    , labp1   , jvalp1   , 
     &                       levsp1   , jsp1    , elevp1   , nshellp1 , 
     &                       relabp1  , mulp1   , ktermp1  , p1format ,
     &                       indlevp1 , indsgp1 , indlsp1  , ipurep1  )
      if (is_batch.le.0) write(6,*)is_batch
 
      if (lpar2) then

        write(iout,540)
        write(iout,550)jvalp2(1),jvalp2(1)+jsp1-1
        do i=1,nump2
          write(iout,520)i,p2configs(i),p2sscfgs(i)
        end do
        write(iout,530)
        call produce(iout    , labp2    , jvalp2  , levsp2   , jsp2 ,
     &               elevp2  , nshellp2 ,
     &               relabp2 , mulp2    , ktermp2 , p2format ,
     &               indlevp2, indsgp2  , indlsp2 , ipurep2  )
      if (is_batch.le.0) write(6,*)is_batch
      
      endif
      
      write(iout,560)
      write(iout,540)
 
 510  format(1x,'PARITY 1',2f6.2,/,1x,'     ')
 520  format(1x,i3,3x,a40,3x,a24)
 530  format(1x,'-----------------------------------------------')
 540  format(1x,' '/1x,' ')
 550  format(1x,'PARITY 2',2f6.2,/,1x,'     ')
 560  format(1x,'END TERMS')
 

C-------------------------------------------------------------------------
C Write the energy levels and level information to output file.

      numF1 = 0
      numF2 = 0
      numA  = 0
 
      numM1p1 = 0
      numE2p1 = 0
      numM1p2 = 0
      numE2p2 = 0
 
      call colltrans(iutcoll, ratecalc, numF1, numF2, numA)
      if (is_batch.le.0) write(6,*)is_batch
 
      write(iout,620)numF1,numF2,numA
 620  format(1x,3i6,/,/)
 
      call forbtrans(iutspec, numM1p1, numE2p1)
      if (lpar2) call forbtrans(iutspec, numM1p2, numE2p2)
      
      
      if (numF1.gt.0) then
        isfirst=.true.
        call readmeta(iutspec  , iutcoll  , iout    ,
     &                isfirst  , ratecalc ,
     &                numF1    , numM1p1  , numE2p1 ,
     &                ktermp1  , jvalp1   ,
     &                indlevp1 , indsgp1  ,indlsp1  )
        if (is_batch.le.0) write(6,*)is_batch
      endif
 
      if (numF2.gt.0) then
        isfirst=.false.
        call readmeta(iutspec  , iutcoll  , iout    ,
     &                isfirst  , ratecalc ,
     &                numF2    , numM1p2  , numE2p2 ,
     &                ktermp2  , jvalp2,
     &                indlevp2 , indsgp2  ,indlsp2  )
        if (is_batch.le.0) write(6,*)is_batch
      endif
 
      if (numA.gt.0) then
         call readtrans(iutcoll  , iout     , ratecalc ,
     &                  numA     , ktermp1  , ktermp2  ,
     &                  jvalp1   , jvalp2   ,
     &                  indlevp1 , indlevp2 ,
     &                  indsgp1  , indsgp2  ,
     &                  indlsp1  , indlsp2  )
         if (is_batch.le.0) write(6,*)is_batch
      endif
 

C-------------------------------------------------------------------------
 999  continue
     
      end
