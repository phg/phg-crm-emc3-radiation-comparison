C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/eissner.for,v 1.4 2004/07/06 13:48:45 whitefor Exp $ Date $Date: 2004/07/06 13:48:45 $
C
      subroutine eissner(numcfg, configs , ssconfigs)
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Produces Eissner/SuperStructure configuration label.
C             The input configurations are of the form 
C             (shell,occupnacy) a3,a2 eg '10D10' or ' 3P3 '. 
C
C  INPUT    : (I*4)  numcfg    =  Number of configuration strings.
C
C  INPUT    : (C)    configs   =  Configurations in Cowan standard form.
C
C  OUTPUT   : (C)    ssconfigs =  Eissner configuration label
C
C  COMMENT  : The commented-out if statement removes labelling of
C             full shells.
C  
C ROUTINES  :
C              ROUTINE   SOURCE   BRIEF DESCRIPTION
C              ---------------------------------------------------------
C              XXSTUC    ADAS     Ensures all characters are upper case.
C              XXSLEN    ADAS     Finds the length of a string excluding
C                                 leading and trailing blanks.
C
C
C  AUTHOR   : Martin O'Mullane
C
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  MODIFIED : 1.2 Hugh Summers		18 Feb. 2003  
C             Amended incorrect specification of Eissner string for
C             shell occupancies greater than 9.  Extended shell range.
C             If the 1st. shell has occupancy > 9, then Eissner string
C             is one character longer to include the non-blank leading
C             character.    
C
C  VERSION  : 
C		1.1 	02-02-2000
C		1.2	18-03-2003
C
C
C  VERSION  : 1.3
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h' 
C-----------------------------------------------------------------------
      integer     numcfg  ,  i     , j ,  m ,  mm, ms, ntot, i1, i2, j1, 
     &            nshell  , jelec  , ielec
      integer     ifirst  , ilast  
C-----------------------------------------------------------------------
      character   cfg*40
C-----------------------------------------------------------------------
      character   configs(numcfg)*(*), ssconfigs(numcfg)*(*)
C-----------------------------------------------------------------------
      character   sval(21)*1     ,  number(35)*1  , lshell*1
      character   shell(ncfg)*3  ,  elec(ncfg)*2
C----------------------------------------------------------------------- 
      data (number(i),i=1,35) /'1','2','3','4','5',
     &                         '6','7','8','9',
     &                         'A','B','C','D','E',
     &                         'F','G','H','I','J',
     &                         'K','L','M','N','O',
     &                         'P','Q','R','S','T',
     &                         'U','V','W','X','Y','Z'/
 
      data (sval(i),i=1,21) /'S','P','D','F','G','H','I','K','L','M',
     &                       'N','O','Q','R','T','U','V','W','X','Y',
     &                       'Z'/
C-----------------------------------------------------------------------
 

 
      do i=1,numcfg
        
        cfg = configs(i) 
        call xxstuc(cfg)
        read(cfg ,110)(shell(mm),elec(mm),mm=1,ncfg)
        
        ssconfigs(i) = '                       '
        
        i1=1
        i2=3
        j1=1
        do j=1,ncfg

          if (shell(j).ne.'   ') then
             read(shell(j),120)nshell,lshell
             ms=1
             do while (lshell.ne.sval(ms))
               ms=ms+1
             end do
             ntot=0
             do m=1,nshell-1
               ntot=ntot+m
             end do
             ntot=ntot+ms
             if (elec(j).eq.' ') then
                ielec=1
             else
                read(elec(j),*)ielec
             endif
             jelec=2*(2*(ms-1)+1)
                if (j1.eq.1) then
                   write(ssconfigs(i)(i1:i2),130)
     &                   ielec+50,number(ntot)
                   i1=4
                   i2=6
                else
                   write(ssconfigs(i)(i1:i2),130)
     &                   ielec+50,number(ntot)
                   i1=i2+1
                   i2=i2+3
                endif
                j1=j1+1
          endif
          
        end do
        
	if(ssconfigs(i)(1:1).eq.'5') then
	    call xxslen(ssconfigs(i),ifirst,ilast)
	    ssconfigs(i)(ifirst:ilast-1)=ssconfigs(i)(ifirst+1:ilast)
	    ssconfigs(i)(ilast:ilast)=' '
	endif
	
      end do
 
  10  format(1x,a40)
  20  format(1x,i4,3x,i2,22x,a40)
 110  format(8(a3,a2))
 120  format(i2,a1)
 130  format(i2,a1)
 131  format(a1,a1)
 
      end
