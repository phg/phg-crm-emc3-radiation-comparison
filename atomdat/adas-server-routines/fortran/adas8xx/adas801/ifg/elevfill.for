C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/elevfill.for,v 1.4 2004/07/06 13:49:00 whitefor Exp $ Date $Date: 2004/07/06 13:49:00 $
      subroutine elevfill(iunit, elev, relab, levs, js, ipure)
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Makes up a 132 character label for each IC level. 
C
C  INPUT    : (I)    iunit     =  unit number of D11 file.
C
C  OUTPUT   : (R*8)  jval      =  Array of J-values.
C  OUTPUT   : (I)    levs      =  Array of levels.
C  OUTPUT   : (I)    js        =  Number of J-values in parity.
C
C  
C ROUTINES  : None.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          
C  DATE     : 04-12-2002
C
C  MODIFIED : Martin O'Mullane  
C              - Read purity from RCG file.
C
C  VERSION  : 1.4
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------
      integer    iunit   , js           , j          , l     , nserial
      integer    itmp
C-----------------------------------------------------------------------
      real*8     energy
C-----------------------------------------------------------------------
      character  str*10
C-----------------------------------------------------------------------
      integer    relab(nJ,nLevels)      ,  levs(nJ)  , ipure(nJ,nLevels)
C-----------------------------------------------------------------------
      real*8     elev(nJ,nLevels)
C-----------------------------------------------------------------------


      do j=1,js
        
        if (levs(j).eq.1)then
        
   5       continue
           read(iunit,480,end=999)str
           if (str.ne.'ues      (') goto 5
           read(iunit,480,end=999)str
           read(iunit,490,end=999)energy
           elev(j,1)  = energy*1000.0D0
           ipure(j,1) = 100
 480       format(11x,a10)
 490       format(28x,f9.3)
 
        else
        
           do l=1,levs(j)
  10         read(iunit,500,err=900,end=999)str
 500         format(52x,a10)
             if (str.ne.'Identified') goto 10
 
             backspace iunit
             read(iunit,505,err=905,end=999)energy,nserial,str
 505         format(32x,f16.2,39x,i3,12x,a10)
             if (str(1:6).eq.'re-lab') relab(j,nserial)=1
             read(str,'(6x,i4)')itmp
             read(iunit,500,err=910,end=999)str
             elev(j,nserial)  = energy
             ipure(j,nserial) = itmp
 
           end do
        
        endif
 
        if (levs(j).ne.1)then
        
           do l=1,levs(j)
 
  60         continue
             read(iunit,500,err=900,end=999)str
             if (str.ne.'Identified') goto 60
             backspace iunit
             read(iunit,505,err=905,end=999)energy
             read(iunit,500,err=910,end=999)str
 
           end do
       
        endif
      
      end do
      goto 999
 
 900  write(0,*)'ERROR: reading Eigenvalue'
      goto 999
 905  write(0,*)'ERROR: reading energy'
      goto 999
 910  write(0,*)'ERROR: skiping line'
      goto 999
 915  write(0,*)'ERROR: reading serial number'
 
 999  continue
      end
