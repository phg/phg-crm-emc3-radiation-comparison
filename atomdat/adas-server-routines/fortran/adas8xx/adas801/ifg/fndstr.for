C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/fndstr.for,v 1.4 2010/06/30 15:37:11 mog Exp $ Date $Date: 2010/06/30 15:37:11 $
C
      subroutine fndstr(nunit,skip,length,teststring,fromstart)
 
C-----------------------------------------------------------------------
C  PURPOSE  : find a string in a fixed block text fileand moves the
C             file pointer to that line
C
C  INPUT    : nunit      - unit number of opened file
C             skip       - skip this many characters before string
C             length     - length of search string
C             teststring - string to be searched
C             fromstart  - .ge.1 always search from start
C                          .lt.1 search from current position
C                          abs=2 diagnostic print of each line
C
C  OUTPUT   : none
C
C
C  AUTHOR   : M O'Mullane, UCC
C  DATE     : 16-8-93
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
c version : 1.3
c date    : 30-06-2010
c modified: Martin O'Mullane
c            - Remove comma before first variable in debug write 
c              statement.
c
C-----------------------------------------------------------------------
      include    'common.h'
C-----------------------------------------------------------------------
      integer    fromstart,skip
C-----------------------------------------------------------------------
      logical    dbpr
C-----------------------------------------------------------------------
      character  teststring*(*)
      character  str*50          , fstate*7
C-----------------------------------------------------------------------

      dbpr=.FALSE.
 
      if (fromstart.ge.1) rewind (nunit)
 
      if (skip.le.9) then
         write(fstate,50)skip
      elseif (skip.gt.9.and.skip.le.99) then
         write(fstate,52)skip
      else
        fromstart=-99
        return
      endif
 
      if (abs(fromstart).eq.2) dbpr=.TRUE.
 
  10  read(nunit,fstate,end=800)str
      if (dbpr) write(0,*)'>>>',str(1:length),'<<<'
      if (str(1:length).ne.teststring) goto 10
      backspace nunit
      goto 999
 
  50  format('(',i1,'X,a',')')
  52  format('(',i2,'X,a',')')
 
 800  fromstart=-99
 
 999  continue
      end
