C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/getnshell.for,v 1.3 2004/07/06 13:58:53 whitefor Exp $ Date $Date: 2004/07/06 13:58:53 $
C
      subroutine getnshell(iunit, nshellp1, nshellp2)

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  :  Get the number of open subshells for both parities 
C              from the RCN2 output file. 
C
C  CALLING PROGRAM: ADAS801 - IFG part.
C
C  INPUT    : (I)    iunit     =  unit number of D11 file.
C
C  OUTPUT   : (I)    nshellp1  =  number of open subshells in parity 1
C  OUTPUT   : (I)    nshellp2  =  number of open subshells in parity 2
C
C  ROUTINES : None
C
C  
C  AUTHOR   : Martin O'Mullane,
C
C  VERSION  : 1.1                          
C  DATE     : 02-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------      
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------      
      integer iunit  , nshellp1  , nshellp2
C-----------------------------------------------------------------------      

      read(iunit,30)nshellp1, nshellp2

 30   format(10x, i1, 4x, i1)
 
      end
