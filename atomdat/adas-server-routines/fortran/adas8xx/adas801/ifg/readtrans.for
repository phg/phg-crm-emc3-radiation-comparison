C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/readtrans.for,v 1.3 2004/07/06 14:49:38 whitefor Exp $ Date $Date: 2004/07/06 14:49:38 $
C
      subroutine readtrans(iutcoll  , iout     , ratecalc ,
     &                     numA     , ktp1     , ktp2     ,
     &                     jp1      , jp2      ,
     &                     indlevp1 , indlevp2 , 
     &                     indsgp1  , indsgp2  ,
     &                     indlsp1  , indlsp2  )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Gathers collision strengths and A-values for allowed
C             transitions. 
C
C  INPUT    : (I)    iunit     =  unit number of spectrum file.
C
C  OUTPUT   : (I)    numM1     =  number of magnetic dipole transitions.
C  OUTPUT   : (I)    numE2     =  number of electric quadrupole transitions.
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              findstr    ADAS    Finds a 10 character search string.  
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane  
C              - Add include statements.
C
C  VERSION  : 1.3                          
C  DATE     : 20-12-2003
C
C  MODIFIED : Martin O'Mullane  
C              - Extend size of level field from i3 to i4.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------
      integer     iutcoll  , iout  , numA 
      integer     ierr     , ncser , ncserp    , i            , jjj   ,
     &            kil      , kir   , kjl       , kjr          , nenrgs
C-----------------------------------------------------------------------
      real*8      t        , fjt   , tp        , fjtp         , fnu   ,
     &            flam     , dum1  , gf        , gflog        , ga
C-----------------------------------------------------------------------
      logical     ratecalc , fromstart
C-----------------------------------------------------------------------
      character   lldes*8  , lldesp*8
C-----------------------------------------------------------------------
      integer     indlevp1(nJ,nLevels)  , indlevp2(nJ,nLevels) ,
     &            indsgp1(nJ,nLevels)   , indsgp2(nJ,nLevels)  ,
     &            indlsp1(nJ,nLevels)   , indlsp2(nJ,nLevels)  ,
     &            ktp1(nJ,nLevels)      , ktp2(nJ,nLevels)
C-----------------------------------------------------------------------
      real*8      omega(ndenrgs)        , gamma(ndenrgs)       ,
     &            jp1(nJ)               , jp2(nJ)
C-----------------------------------------------------------------------

 
      write(iout,45)
   45 format(1x,/,/,' ALLOWED')
 
      fromstart = .TRUE.
      call findstr(iutcoll,' elec dip ', fromstart, ierr)
      if (ierr.eq.-99) goto 999
      call advance(iutcoll,1)
 
      do i=1,numA
        read(iutcoll,100,end=200,err=200)ncser,ncserp,lldes,lldesp,
     &       t,fjt,tp,fjtp,fnu,flam,dum1,gf,gflog,ga
        kjl=idint(fjt-jp1(1)+1)
        kjr=idint(fjtp-jp2(1)+1)
        read(lldes,55)kil
        read(lldesp,55)kir
   55   format(i3)
 
        read(iutcoll,300,end=350)nenrgs
        if (ratecalc) then
           read(iutcoll,305,end=350)(omega(jjj),jjj=1,nenrgs)
           read(iutcoll,305,end=350)(gamma(jjj),jjj=1,nenrgs)
        endif
 
  300   format(1x,i4)
  305   format(7(1pd10.3))
  350   continue
 
        
        write(iout,60)i,
     &        ncser,ktp1(kjl,kil),
     &        indsgp1(kjl,kil),indlsp1(kjl,kil),indlevp1(kjl,kil),fjt,
     &        ncserp,ktp2(kjr,kir),
     &        indsgp2(kjr,kir),indlsp2(kjr,kir),indlevp2(kjr,kir),fjtp,
     &        flam,gf,gflog,ga
        if (ratecalc) then
           write(iout,305)(omega(jjj),jjj=1,nenrgs)
           write(iout,305)(gamma(jjj),jjj=1,nenrgs)
        endif
        write(iout,65)
   60   format(1x,i4,2x,2(5i4,f6.1,2x),f12.4,f9.4,f8.3,1pd10.3)
   65   format(1x,'-----------------------------------------')
 
 
      end do
  200 continue
 
 
   40 format(2x,a8)
   20 format(i3,1x,a8,i3,1x,a8)
  100 format(6x,2i2,2(1x,a8,1x),f13.3,f6.1,f12.3,f6.1,f10.2,f12.3,
     &        3f9.4,1pe12.3)
 
  999 continue
 
      end
