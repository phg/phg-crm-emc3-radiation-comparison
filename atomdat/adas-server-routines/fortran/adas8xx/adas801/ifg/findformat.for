C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/findformat.for,v 1.3 2004/07/06 13:54:18 whitefor Exp $ Date $Date: 2004/07/06 13:54:18 $
C
      subroutine findformat(iunit, form, numopen, num)

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  :  . 
C
C  CALLING PROGRAM: ADAS801 - IFG part.
C
C  INPUT    : (I)    iunit   =  unit number of D11 file.
C  INPUT    : (I)    form(,) =  
C  INPUT    : (I)    numopen =  number of open subshells
C  INPUT    : (I)    num     =  number of configurations
C
C
C  ROUTINES : None
C
C  
C  AUTHOR   : Martin O'Mullane,
C
C  VERSION  : 1.1                          
C  DATE     : 02-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------      
      include 'common.h'
      include 'ifg.h' 
C-----------------------------------------------------------------------      
      integer    iunit ,   num , numopen   ,
     &           i     ,   j   , k
C-----------------------------------------------------------------------      
      character  lwchar*3
C-----------------------------------------------------------------------      
      integer    form(nconfigs,ncfg)  , w(ncfg),    nlw(nLSterms)
C-----------------------------------------------------------------------      
      logical    nolabel
C-----------------------------------------------------------------------      
      character  clw(nLSterms)*3      , flw(nfull)*3
      character  l(ncfg)*1
C-----------------------------------------------------------------------      
      data nlw /1,1,1,3,3,3,1,1,
     &          1,5,8,16,16,16,8,5,1,1,
     &          1,7,17,27,73,119,119,119,73,27,17,7,1,1,
     &          1,
     &          1,
     &          1,
     &          1/
 
      data clw /'s 1','s 2','p 1','p 2','p 3','p 4','p 5','p 6',
     &          'd 1','d 2','d 3','d 4','d 5','d 6','d 7','d 8',
     &          'd 9','d10',
     &          'f 1','f 2','f 3','f 4','f 5','f 6','f 7','f 8',
     &          'f 9','f10','f11','f12','f13','f14',
     &          'g 1',
     &          'h 1',
     &          'i 1',
     &          'k 1'/
 
      data flw /'s 2','p 6','d10','f14'/
C-----------------------------------------------------------------------      
 
  
      do i=1,num

        read(iunit,10)(l(k),w(k),k=1,numopen)
        do j=1,numopen
          write(lwchar,20)l(j),w(j)
          do k=1,nLSterms
             if (lwchar.eq.clw(k)) then
               form(i,j)=nlw(k)
               goto 5
             endif
          end do
   5      continue
        end do

        do j=1,numopen
          write(lwchar,20)l(j),w(j)
          do k=1,nfull
            if (lwchar.eq.flw(k)) then
              form(i,j)=0
              goto 6
            endif
          end do
   6      continue
        end do
        
        nolabel=.true.
        do j=1,numopen
          nolabel=nolabel.and.(form(i,j).eq.0)
        end do
        if (nolabel) form(i,1)=1

      end do
 
  10  format(8(a1,i2,2x))
  20  format(a1,i2)
      end
