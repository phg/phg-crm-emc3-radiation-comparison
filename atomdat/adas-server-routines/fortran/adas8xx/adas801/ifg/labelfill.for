C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/labelfill.for,v 1.3 2004/07/06 14:10:27 whitefor Exp $ Date $Date: 2004/07/06 14:10:27 $
C
      subroutine labelfill(iunit, numopen, lab, jval, levs, js)
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Makes up a 132 character label for each IC level. 
C
C  INPUT    : (I)    iunit     =  unit number of D11 file.
C
C  OUTPUT   : (C)    lab       =  IC level label.
C  OUTPUT   : (R*8)  jval      =  Array of J-values.
C  OUTPUT   : (I)    levs      =  Array of .
C  OUTPUT   : (I)    js        =  Number of J values in parity.
C
C  COMMENT  : The commented-out if statement removes labelling of
C             full shells.
C  
C ROUTINES  : None.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.3                        
C  DATE     : 20-11-2003
C
C  MODIFIED : Martin O'Mullane  
C              - lfinal(13) and (14) had incorrect L values.
C              - extended lfinal to l=U (15) for 4d 4f configs.
C              - swap sizes of nq() and s() in labelfill - now i3 and i2.
C                nq can exceed 99 but s is unlikely to.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h' 
C-----------------------------------------------------------------------
      integer    iunit   , numopen       , js            , 
     &           i       , k             , m             ,  
     &           iconfig , index
C-----------------------------------------------------------------------
      character  str0*4  ,  str1*4       , str2*4        , bracket*1
C-----------------------------------------------------------------------
      integer    levs(nJ),  nq(ncfg)     , nr(ncfg)      , s(ncfg)
C-----------------------------------------------------------------------
      real*8     jval(nJ)
C-----------------------------------------------------------------------
      character  l(ncfg)*1, lr(ncfg)*1   ,lfinal(16)*1   , lpos(16)*1
      character  lab(nJ,nLevels)*132
C----------------------------------------------------------------------- 
      data lfinal /'S','P','D','F','G','H','I',
     &             'K','L','M','N','O','Q','R',
     &             'T','U'/
      data lpos   /'a','b','c','d','e','f','g',
     &             'h','i','j','k','l','m','n',
     &             'o','p'/
C----------------------------------------------------------------------- 
 
      js=1
  10  read(iunit,500,end=999)str0,str1,str2
 500  format(1x,a4,18x,a4,a4,7x,a4)
      
      if (str1.eq.'urat') then
 
        backspace iunit
        read(iunit,505,err=905,end=999)jval(js)
 505    format(3x,f4.1)
c       print *,'The J value is...',jval(js)
 
  20    if (numopen.gt.3) then
          read(iunit,510,err=910,end=999)bracket
        else
          read(iunit,511,end=999)bracket
        endif
 510    format(9x,a1)
 511    format(7x,a1)
        if (bracket.ne.'(') goto 20
 
        i=1
        backspace iunit
  30    if (numopen.gt.3) then
  
          read(iunit,515,err=915,end=999)index,iconfig,bracket,
     &         nq(1),s(1),l(1),nr(1),lr(1),
     &         (nq(m),s(m),l(m),nr(m),lr(m),m=2,numopen)
 515      format(1x,2i3,2x,a1,i3,i2,a1,1x,i2,a1,
     &           7(3x,i2,i2,a1,1x,i2,a1))
        else
        
          read(iunit,516,err=915,end=999)iconfig,bracket,
     &         nq(1),s(1),l(1),nr(1),lr(1),
     &         (nq(m),s(m),l(m),nr(m),lr(m),m=2,numopen)
 516      format(1x,i2,4x,a1,i3,i3,a1,1x,i3,a1,
     &           2(5x,i3,i3,a1,1x,i3,a1))
     
        endif
        
        write(lab(js,i),517)
 517    format(132(' '))
        write(lab(js,i),520)iconfig,nq(1),s(1),l(1),nr(1),lr(1),
     &       (nq(m),s(m),l(m),nr(m),lr(m),m=2,numopen)
 520    format(i3,i3,i2,a1,i2,a1,7(i3,i2,a1,i2,a1))

        if (lr(numopen).ne.' ') then
           k=1
           do while (lr(numopen).ne.lfinal(k))
             k=k+1
           end do
c          print *,'k used is ',k
c          write(lab(js,i)(94:96),525)(10-nr(numopen)),lpos(k)
           write(lab(js,i)(119:121),525)nr(numopen),lpos(k)
 525       format(i2,a1)
c          print *,'lev and js are',i,js
c          print *,'label string is:',lab(js,i),':'
        endif
 
        if (bracket.ne.'(') goto 40
        if (numopen.le.4) read (iunit,510)bracket
        i=i+1
        goto 30
  40    continue
 
      elseif (str2.eq.'pari') then
        backspace iunit
        goto 999
      elseif (str0.eq.'mup ') then
        goto 999
      else
        goto 10
      endif
 
      levs(js)=i-1
      js=js+1
      
      goto 10
 
 900  write(0,*)'ERROR: reading URAT identification string'
      goto 999
 905  write(0,*)'ERROR: reading J value'
      goto 999
 910  write(0,*)'ERROR: reading bracket'
      goto 999
 915  write(0,*)'ERROR: reading quantum numbers'
 999  continue
      js=js-1
      
      
      end
