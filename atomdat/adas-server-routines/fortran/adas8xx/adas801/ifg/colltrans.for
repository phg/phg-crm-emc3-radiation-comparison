C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/colltrans.for,v 1.3 2004/07/06 12:08:03 whitefor Exp $ Date $Date: 2004/07/06 12:08:03 $
C
      subroutine colltrans(iunit, ratecalc, numF1, numF2, numA)
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Makes up a 132 character label for each IC level. 
C
C  INPUT    : (I)    iunit     =  unit number of collision file.
C  INPUT    : (L)    ratecalc  =  rates or cross-sections.
C
C  OUTPUT   : (I)    numF1     =  number of parity 1 forbidden transitions.
C  OUTPUT   : (I)    numF2     =  number of parity 2 forbidden transitions.
C  OUTPUT   : (I)    numA      =  number of allowed transitions.
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              findstr    ADAS    Finds a 10 character search string.  
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.2                          
C  DATE     : 23-11-2003
C
C  MODIFIED : Martin O'Mullane  
C               - For complex species the number of levels may exceed
C                 9999 and the index field in the spectrum file is
C                 replaced by ****. When counting transitions accept
C                 * as a valid number.
C
C-----------------------------------------------------------------------
      integer     iunit    , numF1    , numF2  , numA , ierr ,   i2    
C-----------------------------------------------------------------------
      character   string*1
C-----------------------------------------------------------------------
      logical     ratecalc , fromstart
C-----------------------------------------------------------------------
      include 'common.h'
C-----------------------------------------------------------------------

      numF1 = 0
      numF2 = 0
      numA  = 0
 
      fromstart = .TRUE.
      call findstr(iunit,' elec qud ', fromstart, ierr)
      if (ierr.eq.-99) then
        numF1=0
      else
        call advance(iunit,1)
        numF1=1
  10    read(iunit,90,err=999,end=400)string
        if (string.eq.'0'.or.string.eq.'1'.or.string.eq.'2'.or.
     &      string.eq.'3'.or.string.eq.'4'.or.string.eq.'5'.or.
     &      string.eq.'6'.or.string.eq.'7'.or.string.eq.'8'.or.
     &      string.eq.'9'.or.string.eq.'*') then
          read(iunit,100,err=999,end=999)i2
          if (ratecalc) then
             i2=int(float(i2)/7.0)*2
             call advance(iunit,i2)
          endif
          numF1=numF1+1
          goto 10
        else
          numF1=numF1-1
        endif
      endif
 
      call advance(iunit,-3)

      fromstart = .FALSE.
      call findstr(iunit,' elec qud ', fromstart, ierr)
      if (ierr.eq.-99) then
        numF2=0
      else
        call advance(iunit,1)
        numF2=1
  15    read(iunit,90,err=999,end=400)string
        if (string.eq.'0'.or.string.eq.'1'.or.string.eq.'2'.or.
     &      string.eq.'3'.or.string.eq.'4'.or.string.eq.'5'.or.
     &      string.eq.'6'.or.string.eq.'7'.or.string.eq.'8'.or.
     &      string.eq.'9'.or.string.eq.'*') then
          read(iunit,100,err=999,end=999)i2
          if (ratecalc) then
             i2=int(float(i2)/7.0)*2
             call advance(iunit,i2)
          endif
          numF2=numF2+1
          goto 15
        else
          numF2=numF2-1
        endif
      endif
 
 
 400  continue

      fromstart = .TRUE.
      call findstr(iunit,' elec dip ', fromstart, ierr)
      if (ierr.eq.-99) then
        numA=0
      else
        call advance(iunit,1)
        numA=1
  20    read(iunit,90,err=999,end=999)string
        if (string.eq.'0'.or.string.eq.'1'.or.string.eq.'2'.or.
     &      string.eq.'3'.or.string.eq.'4'.or.string.eq.'5'.or.
     &      string.eq.'6'.or.string.eq.'7'.or.string.eq.'8'.or.
     &      string.eq.'9'.or.string.eq.'*') then
          read(iunit,100,err=999,end=999)i2
          if (ratecalc) then
             i2=int(float(i2)/7.0)*2
             call advance(iunit,i2)
          endif
          numA=numA+1
          goto 20
        endif
      endif
 
 
  90  format(5x,a1)
 100  format(1x,i4)
 
 999  continue
      if (numA.gt.0) numA=numA-1
 
      end
