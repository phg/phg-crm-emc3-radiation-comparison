C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/readmeta.for,v 1.4 2017/08/12 16:47:25 mog Exp $ Date $Date: 2017/08/12 16:47:25 $
C
      subroutine readmeta(iutspec , iutcoll  , iout  ,
     &                    first   , ratecalc ,
     &                    numF    , numM1    , numE2 ,
     &                    kt      , jval     ,
     &                    indlev  , indsg    , indls )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Gathers collision strengths and A-values for forbidden
C             transitions.
C
C  INPUT    : (I)    iunit     =  unit number of spectrum file.
C
C  OUTPUT   : (I)    numM1     =  number of magnetic dipole transitions.
C  OUTPUT   : (I)    numE2     =  number of electric quadrupole transitions.
C
C
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              findstr    ADAS    Finds a 10 character search string.
C              advance    ADAS    Advances the file pointer.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.3
C  DATE     : 20-12-2003
C
C  MODIFIED : Martin O'Mullane
C              - Extend size of level field from i3 to i4.
C
C  VERSION  : 1.4
C  DATE     : 12-08-2017
C
C  MODIFIED : Martin O'Mullane
C              - Reset indtrans array at each call.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------
      integer     iutspec , iutcoll  , iout  , numF  , numM1  , numE2 ,
     &            ierr
      integer     i       , jjj      , kir   , kil   , kjl    , kjr   ,
     &            indpos  , nenrgs   , numft , ncser , ncserp
C------------------------------------------------------------------------
      real*8      t       , fjt      , tp    , fjtp  , fnu    , wave  ,
     &            d1      , tgf      , d2    , tga   , d3     , flam  ,
     &            dum1    , gf       , gflog , ga
C------------------------------------------------------------------------
      logical     first   , ratecalc , fromstart
C------------------------------------------------------------------------
      character   lldes*8 , lldesp*8 , str*10
C-----------------------------------------------------------------------
      integer     indtrans(nJ,nLevels,nJ,nLevels)   ,
     &            indlev(nJ,nLevels)                , indsg(nJ,nLevels),
     &            indls(nJ,nLevels)                 , kt(nJ,nLevels)
C-----------------------------------------------------------------------
      real*8      transitions(nmtp,2), jval(nJ)     ,
     &            omega(ndenrgs),gamma(ndenrgs)
C-----------------------------------------------------------------------

C Reset storage arrays

      do i=1,nmtp
        transitions(i,1) = 0.0D0
        transitions(i,2) = 0.0D0
      end do

      do kir=1,nlevels
         do kjr=1,nj
           do kil=1,nlevels
             do kjl=1,nj
                indtrans(kjl,kil,kjr,kir) = 0
             end do
           end do
         end do
      end do

C Indicate which parity

      if (first) then
        write(iout,45)
      else
        write(iout,46)
      endif
   45 format(1x,'PAR 1 FORBIDDEN')
   46 format(1x,/,/,1x,'PAR 2 FORBIDDEN')


C Locate file pointer

      if (first) then
        str='  mag dip '
        if (numM1.eq.0) str=' elec qud '
        fromstart = .TRUE.
        call findstr(iutspec, str, fromstart, ierr)
        if (ierr.eq.-99) goto 999
        call advance(iutspec,1)
      else
        str='  mag dip '
        if (numM1.eq.0) str=' elec qud '
        fromstart = .FALSE.
        call findstr(iutspec, str, fromstart, ierr)
        if (ierr.eq.-99) goto 999
        call advance(iutspec,1)
      endif


C Get A-values - M1 followed by E2

      numft=1
      do i=1,numM1
        read(iutspec,400,end=200)t,fjt,ncser,lldes,tp,
     &                           fjtp,ncserp,lldesp,
     &                           fnu,wave,d1,tgf,d2,tga,d3
        kjl=idint(fjt-jval(1)+1)
        kjr=idint(fjtp-jval(1)+1)
        read(lldes,55)kil
        read(lldesp,55)kir

        indtrans(kjl,kil,kjr,kir)=numft
        transitions(numft,1)=tga
        numft=numft+1

      end do

      call advance(iutspec,1)

      do i=1,numE2
        read(iutspec,400,end=200)t,fjt,ncser,lldes,tp,
     &                           fjtp,ncserp,lldesp,
     &                           fnu,wave,d1,tgf,d2,tga,d3
        kjl=idint(fjt-jval(1)+1)
        kjr=idint(fjtp-jval(1)+1)
        read(lldes,55)kil
        read(lldesp,55)kir

        if (indtrans(kjl,kil,kjr,kir).ne.0) then
           indpos=indtrans(kjl,kil,kjr,kir)
           transitions(indpos,2)=tga
        else
           indtrans(kjl,kil,kjr,kir)=numft
           transitions(numft,2)=tga
           numft=numft+1
        endif
      end do

 400  format(5x,f11.4,f5.1,i3,1x,a8,f13.4,f5.1,i3,1x,a8,f13.4,
     &       f12.4,f8.3,f9.4,f8.3,1pe10.3,0pf8.4)


C Now read the collision strengths

      if (first) then
        fromstart = .TRUE.
        call findstr(iutcoll, ' elec qud ', fromstart, ierr)
        if (ierr.eq.-99) goto 999
        call advance(iutcoll,1)
      else
        fromstart = .FALSE.
        call findstr(iutcoll, ' elec qud ', fromstart, ierr)
        if (ierr.eq.-99) goto 999
        call advance(iutcoll,1)
      endif


      do i=1,numF
        read(iutcoll,100,end=200,err=200)ncser,ncserp,lldes,lldesp,
     &       t,fjt,tp,fjtp,fnu,flam,dum1,gf,gflog,ga
        kjl=idint(fjt-jval(1)+1)
        kjr=idint(fjtp-jval(1)+1)
        read(lldes,55)kil
        read(lldesp,55)kir
   55   format(i3)

        read(iutcoll,300,end=350)nenrgs
        if (ratecalc) then
           read(iutcoll,305,end=350)(omega(jjj),jjj=1,nenrgs)
           read(iutcoll,305,end=350)(gamma(jjj),jjj=1,nenrgs)
        endif
  300   format(1x,i4)
  305   format(7(1pd10.3))
  350   continue

        if (indtrans(kjl,kil,kjr,kir).ne.0) then
           indpos=indtrans(kjl,kil,kjr,kir)
           ga=transitions(indpos,1)+transitions(indpos,2)
        else
           ga=0.0D0
        endif

        write(iout,60)i,ncser,kt(kjl,kil),
     &              indsg(kjl,kil),indls(kjl,kil),indlev(kjl,kil),fjt,
     &              ncserp,kt(kjr,kir),
     &              indsg(kjr,kir),indls(kjr,kir),indlev(kjr,kir),fjtp,
     &              flam,gf,gflog,ga
        if (ratecalc) then
           write(iout,305)(omega(jjj),jjj=1,nenrgs)
           write(iout,305)(gamma(jjj),jjj=1,nenrgs)
        endif

        write(iout,65)
   60   format(1x,i4,2x,2(5i4,f6.1,2x),f12.4,f9.4,f8.3,1pd10.3)
   65   format(1x,'-----------------------------------------')


      end do
  200 continue


   40 format(2x,a8)
   20 format(i3,1x,a8,i3,1x,a8)
  100 format (6x,2i2,2(1x,a8,1x),f13.3,f6.1,f12.3,f6.1,f10.2,f12.3,
     &        3f9.4,1pe12.3)


  999 continue
      end
