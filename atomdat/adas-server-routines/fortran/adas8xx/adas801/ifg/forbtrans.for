C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/forbtrans.for,v 1.4 2004/07/06 13:55:37 whitefor Exp $ Date $Date: 2004/07/06 13:55:37 $
C
      subroutine forbtrans(iunit, numM1, numE2)

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Find the number of M1 and E2 transitions. 
C
C  INPUT    : (I)    iunit     =  unit number of spectrum file.
C
C  OUTPUT   : (I)    numM1     =  number of magnetic dipole transitions.
C  OUTPUT   : (I)    numE2     =  number of electric quadrupole transitions.
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              findstr    ADAS    Finds a 10 character search string.  
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane  
C             Correct for (rare) cases where numE2 was incorrectly 
C             calculated.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.3                          
C  DATE     : 06-11-2003
C
C  MODIFIED : Martin O'Mullane  
C               - For complex species the number of levels may exceed
C                 9999 and the index field in the spectrum file is
C                 replaced by ****. When counting transitions accept
C                 * as a valid number.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------
      integer     iunit    , numM1    , numE2  , ierr
C-----------------------------------------------------------------------
      character   string*1
C-----------------------------------------------------------------------
      logical     fromstart
C-----------------------------------------------------------------------

      numM1 = 0
      numE2 = 0
 
      fromstart = .FALSE.
      call findstr(iunit,'  mag dip ', fromstart, ierr)
      if (ierr.eq.-99) then
        numM1=0
      else
        call advance(iunit,1)
        numM1=1
  10    read(iunit,90,err=999,end=300)string
        if (string.eq.'0'.or.string.eq.'1'.or.string.eq.'2'.or.
     &      string.eq.'3'.or.string.eq.'4'.or.string.eq.'5'.or.
     &      string.eq.'6'.or.string.eq.'7'.or.string.eq.'8'.or.
     &      string.eq.'9'.or.string.eq.'*') then
          numM1=numM1+1
          goto 10
        else
          numM1=numM1-1
        endif
      endif
 
 300  continue
 
      call advance(iunit,-2)
      fromstart = .FALSE.
      call findstr(iunit,' elec qud ', fromstart, ierr)
      if (ierr.eq.-99) then
        numE2=0
      else
        call advance(iunit,1)
        numE2=1
  15    read(iunit,90,err=999,end=400)string
        if (string.eq.'0'.or.string.eq.'1'.or.string.eq.'2'.or.
     &      string.eq.'3'.or.string.eq.'4'.or.string.eq.'5'.or.
     &      string.eq.'6'.or.string.eq.'7'.or.string.eq.'8'.or.
     &      string.eq.'9'.or.string.eq.'*') then
          numE2=numE2+1
          goto 15
C         else
C           numE2=numE2-1
        endif
      endif
 
      call advance(iunit,-2)
 
 400  continue
      numE2=numE2-1
 
  90  format(4x,a1)
 
 999  continue
 
      end
