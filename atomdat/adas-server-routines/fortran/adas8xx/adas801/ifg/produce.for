C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/produce.for,v 1.5 2004/07/06 14:37:35 whitefor Exp $ Date $Date: 2004/07/06 14:37:35 $
C 
      subroutine produce(iout   , lab     , jval  , levs  , js   , 
     &                   elev   , numopen , 
     &                   relab  , mul     , kterm , form  , 
     &                   indlev , indsg   , indls , ipure )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Makes up a 132 character label for each IC level. 
C
C  INPUT    : (I)    iout      =  unit number of output file.
C
C  OUTPUT   : (C)    lab       =  IC level label.
C  OUTPUT   : (R*8)  jval      =  Array of J-values.
C  OUTPUT   : (I)    levs      =  Array of levels.
C  OUTPUT   : (I)    js        =  Number of J-values in parity.
C
C  
C ROUTINES  : 
C              ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              printit    ADAS     
C              xxi4ss     ADAS     Integer shell sort.                   
C              xxchss     ADAS     Character shell sort.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                         
C  DATE     : 01-11-2000
C
C  MODIFIED : Martin O'Mullane  
C             Problem with using undefined variable (ijk) - only showed 
C             up in certain cases under Linux.
C
C  VERSION  : 1.3                          
C  DATE     : 04-12-2002
C
C  MODIFIED : Martin O'Mullane  
C              - Pass purity of level through to printit subroutine.
C              - swap sizes of nq() and s() in labelfill - now i3 and i2.
C                nq can exceed 99 but s is unlikely to.
C              - write iconfig and ll-1 as i2 and i4 rather than 2i3.
C
C  VERSION  : 1.4
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------
      integer    iout      , js            , numopen   
      integer    i         , j             , k             , m        ,
     &           ijk       , ll            , kj            , ki       , 
     &           lp        , mtp           , jtot          , 
     &           iconfig   , nconfs        , nstart
      integer    index     , indexsg       , indexsgtot    , indexls  , 
     &           icfg1     , icfg          , iopen         , iterm    , 
     &           kk        , mterm         ,  numterm       
C-----------------------------------------------------------------------
      real*8     eav
C-----------------------------------------------------------------------
      logical    lfirst    , lup
C-----------------------------------------------------------------------
      character  term*110  , temp*13     , part*8
C-----------------------------------------------------------------------
      integer    form(nconfigs,ncfg)     , relab(nJ,nLevels)  ,
     &           mul(nJ,nLevels)         , kterm(nJ,nLevels)  , 
     &           levs(nJ)                , 
     &           nconfig(nTerms)         , nc(nTerms)         ,
     &           itag(nTerms)            , itag2(nTerms)      ,
     &           ntag(nTerms)            , 
     &           indlev(nJ,nLevels)      , indsg(nJ,nLevels)  ,
     &           indls(nJ,nLevels)       , s(ncfg)            ,
     &           nq(ncfg)                , nr(ncfg)           ,
     &           ipure(nJ,nLevels)
C-----------------------------------------------------------------------
      real*8     elev(nJ,nLevels)        , jval(nJ)           ,
     &           menergy(nTerms)
C-----------------------------------------------------------------------
      character  parent(nJ,nLevels)*66   , prevparent(nSG)*66 ,
     &           lab(nJ,nLevels)*132     ,
     &           fterms(nTerms)*3        , fterms2(nTerms)*3  ,
     &           l(ncfg)*1               , lr(ncfg)*1
C-----------------------------------------------------------------------

 
      do i=1,nSG
        write(prevparent(i), 50)
      end do
  50  format(66(' '))    
 
      mtp=1
      do j=1,js
        do i=1,levs(j)
          eav=1.0D-10
          jtot=0
          if (lab(j,i)(109:117).ne.'allocated') then
             read(lab(j,i),200)iconfig,fterms(mtp)
 200         format(i3,115x,a3)
             eav=eav+elev(j,i)*(jval(j)*2+1)
             jtot=jtot+(jval(j)*2+1)
             mul(j,i)=mtp
             do kj=j+1,js
               do ki=1,levs(kj)
                 if (lab(j,i).eq.lab(kj,ki)) then
                    eav=eav+elev(kj,ki)*(jval(kj)*2 +1)
                    jtot=jtot+(jval(kj)*2+1)
                    mul(kj,ki)=mtp
                    lab(kj,ki)(109:117)='allocated'
                    goto 10
                 endif
               end do
 10          continue
             end do
             lab(j,i)(109:117)='allocated'
             nconfig(mtp)=iconfig
             itag(mtp)=mtp
             if (eav.ne.1.0D-10) menergy(mtp)=eav/jtot
          mtp=mtp+1
          endif
        end do
      end do
      mtp=mtp-1
 
      do i=1,nTerms
        nc(i)=0
      end do
 
      do i=1,mtp
        nc(nconfig(i)) = nc(nconfig(i))+1
      end do
      i=1
      do while (nc(i).ne.0)
        nconfs=i
        i=i+1
      end do
 
      lup = .TRUE.
      call xxi4ss(mtp, lup, nconfig, itag)
 
      nstart=1
      do i=1,nconfs
        if (i.gt.1) nstart=nstart+nc(i-1)
        do j=1,nc(i)
          itag2(j)=itag(nstart+j-1)
          fterms2(j)=fterms(itag(nstart+j-1))
        end do
        lup = .FALSE.
        call xxchss(nc(i), lup, fterms2, itag2)
        do j=1,nc(i)
          itag(nstart+j-1)=itag2(j)
        end do
      end do
 
      nstart=1
      do i=1,nconfs
        if (i.gt.1) nstart=nstart+nc(i-1)
        do j=nstart,nstart+nc(i)-1
          ntag(itag(j))=j-nstart+1
        end do
      end do
  
 
 
      do j=1,js
        do i=1,levs(j)
          read(lab(j,i),400)iconfig,nq(1),s(1),l(1),nr(1),lr(1),
     &                      (nq(m),s(m),l(m),nr(m),lr(m),m=2,numopen)
 400      format(i3,i3,i2,a1,i2,a1,7(i3,i2,a1,i2,a1))
 
          term='       '
          ll=1
          ijk=0
          do k=1,numopen
            if (form(iconfig,k).ne.0) then
              temp='       '
              if (iconfig.gt.1) then
                do m=1,iconfig-1
                   if (form(m,k).eq.0) ijk=1
                   nq(k)=(nq(k)-form(m,k))-ijk
                   ijk=0
                end do
              endif
              write(temp,410)nq(k),s(k),l(k),nr(k),lr(k)
              term((7+(ll-1)*14):6+(ll*14))=temp
              ll=ll+1
            endif
          end do
 410      format('<(',i2,')',i2,a1,'>',i3,a1,1x)
          write(term(1:2),'(i2)')iconfig
          write(term(3:6),'(i4)')(ll-1)
 
          write(parent(j,i), 50)
          lfirst=.TRUE.
          lp=1
          do k=numopen,1,-1
            if (form(iconfig,k).ne.0) then
              if (lfirst) then
                write(part,430)nq(k),s(k),l(k)
                lfirst=.FALSE.
                write(parent(j,i)(65:66),425)nr(k)
              else
                write(part,435)nq(k),s(k),l(k),nr(k),lr(k)
              endif
              parent(j,i)((1+(lp-1)*8):(lp*14))=part
              lp=lp+1
            endif
          end do
 425      format(i2)
 430      format(2i2,a1,'   ')
 435      format(2i2,a1,i2,a1)
 
 
          lab(j,i)(1:110)=term
 
        end do
      end do
 
 
 
      index      = 1
      indexsg    = 1
      indexsgtot = 1
      indexls    = 1
      icfg1      = 0
 
      do k=1,mtp
        numterm=itag(k)
        do j=1,js
          do i=1,levs(j)
            kterm(j,i)=ntag(mul(j,i))
            if (lab(j,i)(109:118).ne.'secondpass'
     &          .and.mul(j,i).eq.numterm) then
 
               read(lab(j,i),55)icfg,iopen
 55            format(2i3)
               if (icfg.ne.icfg1) then
                 icfg1=icfg
                 index=1
                 indexsg=1
                 indexls=1
                 indexsgtot=1
                 prevparent(1)=parent(j,i)
               else
                 do kk=1,indexsgtot
                   if (parent(j,i).eq.prevparent(kk)) then
                     indexsg=kk
                     goto 57
                   endif
                 end do
                 indexsgtot=indexsgtot+1
                 indexsg=indexsgtot
                 prevparent(indexsg)=parent(j,i)
 57              continue
                 index=index+1
                 indexls=1
               endif
               indlev(j,i)=index
               indsg(j,i)=indexsg
               indls(j,i)=indexls
 
               mterm=mul(j,i)
               eav=menergy(mterm)
               iterm=ntag(mterm)
               call printit(iout, lab(j,i),elev(j,i),relab(j,i),
     &                      ipure(j,i),jval(j),eav,
     &                      iterm,index,indexsg,indexls,1)
               do kj=j+1,js
                 do ki=1,levs(kj)
                   if (lab(j,i).eq.lab(kj,ki)) then
                      index=index+1
                      call printit(iout, lab(kj,ki),elev(kj,ki),
     &                             relab(j,i),ipure(kj,ki),
     &                             jval(kj),eav,
     &                             iterm,index,
     &                             indexsg,indexls,2)
                      lab(kj,ki)(109:118)='secondpass'
                      indlev(kj,ki)=index
                      indsg(kj,ki)=indexsg
                      indls(kj,ki)=indexls
                      goto 60
                   endif
                 end do
 60            continue
               end do
               lab(j,i)(109:118)='secondpass'
            write(25,44)
            endif
          end do
        end do
      end do
 
      do j=1,js
        do i=1,levs(j)
          mul(j,i)=ntag(mul(j,i))
        end do
      end do
  44  format(1x,'-----------------------------------------------')
 
      end
