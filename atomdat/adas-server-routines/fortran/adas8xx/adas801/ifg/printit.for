C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifg/printit.for,v 1.4 2004/07/06 14:37:12 whitefor Exp $ Date $Date: 2004/07/06 14:37:12 $
C
      subroutine printit(iunit , label , elev  , relab , ipure , jval , 
     &                   eav   , iterm , indl  , indsg , indls , itype)
     
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Write the level information and energy to iunit. 
C
C  INPUT    : (I)    iunit     =  unit number of output file.
C
C
C  
C ROUTINES  : None.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          
C  DATE     : 04-12-2002
C
C  MODIFIED : Martin O'Mullane  
C              - Write purity of level to IFG file.
C
C
C  VERSION  : 1.3                          
C  DATE     : 04-12-2002
C
C  MODIFIED : Martin O'Mullane  
C              - Write iconfig and numLS as i2 and i4 rather 
C                than 1x, i2, i3.
C
C  VERSION  : 1.4
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
C-----------------------------------------------------------------------
      integer     iunit     , itype    , relab  , iconfig  , numLs   ,
     &            indsg     , indls    , indl   , iterm    , ipure
      integer     ifmt1     , ifmt2
C-----------------------------------------------------------------------
      real*8      jval      , eav      , elev
C-----------------------------------------------------------------------
      character   label*132 , term*104
C-----------------------------------------------------------------------
     
      if (relab.eq.0) then
         assign 500 to ifmt1
         assign 520 to ifmt2
      else
         assign 505 to ifmt1
         assign 525 to ifmt2
      endif

      read(label,200)iconfig,numLS,term
 200  format(i2,i4,a104)
 
      if (itype.eq.1) then
        write(iunit,ifmt1)iconfig,iterm,indsg,indls,eav,numLS,
     &               term,jval,elev,indl,ipure
      else
        write(iunit,ifmt2)jval,elev,indl,ipure
      endif
 
 500  format(i2,i4,i3,i2,1x,1pd11.4,0p,1x,i1,2x,a104,/,
     &       1x,'J= ',f4.1, ' --- ',f16.2,2x,i4,9x,i4)
 505  format(i2,i4,i3,i2,1x,1pd11.4,0p,1x,i1,2x,a104,/,
     &       1x,'J= ',f4.1, ' --- ',f16.2,2x,i4,3x,'re-lab',i4)
 
 520  format(1x,'J= ',f4.1,' --- ',f16.2,2x,i4,9x,i4)
 525  format(1x,'J= ',f4.1,' --- ',f16.2,2x,i4,3x,'re-lab',i4)
 
      end
