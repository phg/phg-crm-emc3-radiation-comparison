      subroutine orbital(iunit, iout)

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  :  Reads the orbital energies from the RCN output file and
C              write them to iout. 
C
C  CALLING PROGRAM: ADAS801 - IFG part.
C
C  INPUT    : (I)    iunit  =  unit number of RCN output file.
C  INPUT    : (I)    iout   =  unit number of IFG output file.
C
C
C  ROUTINES : None
C
C  
C  AUTHOR   : Martin O'Mullane,
C
C  VERSION  : 1.1                          
C  DATE     : 02-02-2000
C
C  MODIFIED : Martin O'Mullane  
C              - First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C
C  VERSION  : 1.3                         
C  DATE     : 22-11-2003
C
C  MODIFIED : Martin O'Mullane  
C              - A longer string is need to hold the orbital energies.
C
C  VERSION  : 1.4                          
C  DATE     : 25-02-2004
C
C  MODIFIED : Martin O'Mullane  
C              - Vary the format of the orbital energies in order
C                to maximise the number of significant digits.
C
C  VERSION  : 1.5                          
C  DATE     : 05-12-2008
C
C  MODIFIED : Adam Foster       
C              - Increase length of string to (ndorbitals+1)*8 so that
C                correct number of energies are output
C
C  VERSION  : 1.6                          
C  DATE     : 02-08-2011
C
C  MODIFIED : Martin O'Mullane  
C              - Do not skip missing orbitals on output line but 
C                set value to 0.0.
C              - Chop spaces off end of orbital line. 
C
C-----------------------------------------------------------------------      
      include 'common.h'
      include 'ifg.h'
C-----------------------------------------------------------------------
      integer     ndorbitals
C-----------------------------------------------------------------------
      parameter  (ndorbitals=36)
C-----------------------------------------------------------------------
      integer     iunit       , iout    , i       ,  is     , if    ,
     &            degree
C-----------------------------------------------------------------------
      real*8      erel
C-----------------------------------------------------------------------
      character   string*296  ,  str*2  , str1*12 , orbfmt*9
C-----------------------------------------------------------------------
      real*8      energy(ndorbitals) ,  num(ndorbitals)
C-----------------------------------------------------------------------
      character   label(ndorbitals)*2
C-----------------------------------------------------------------------
      data label /'1s','2s','2p','3s','3p','3d',
     &            '4s','4p','4d','4f','5s','5p','5d','5f','5g',
     &            '6s','6p','6d','6f','6g','6h',
     &            '7s','7p','7d','7f','7g','7h','7i',
     &            '8s','8p','8d','8f','8g','8h','8i','8k'/
C-----------------------------------------------------------------------
  
      do i=1,ndorbitals
        energy(i) = 0.0D0
        num(i)    = 0
      end do
 

C Read energies from the 'eps fgr' column. Average over the different 
C configurations.

 10   continue
      read(iunit,'(A)',end=400,err=999)string
      if (string(18:29).eq.'eps fg      ') then
         call advance(iunit,1)
 20      read(iunit,100)str,str1
         if (str.ne.'  ') then
            read(str1,'(f12.5)')erel
            do i=1,ndorbitals
              if (str.eq.label(i)) then
                 energy(i)=energy(i)+erel
                 num(i)=num(i)+1.0D0
              endif
            end do
            goto 20
         endif
      endif
      goto 10
 	
400   continue

      string=' '
      write(string(1:8),110)
      is=9
      do i=1,ndorbitals
        if (num(i).ne.0) then
           energy(i)=abs(energy(i)/num(i))
           
           degree = int(log10(energy(i)))
           if (degree.GE.3) then
              orbfmt = '(1x,f7.2)'
           elseif (degree.GE.2) then
              orbfmt = '(1x,f7.3)'
           elseif (degree.GE.1) then
              orbfmt = '(1x,f7.4)'
           else 
              orbfmt = '(1x,f7.5)'
           endif
           
           if=is+7
           write(string(is:if),orbfmt)energy(i)
           is=if+1
        else
           if=is+7
           write(string(is:if),'(f7.1)')0.0
           is=if+1           
        endif
      end do
      
      if=len(string)
      do i=ndorbitals,1,-1
         if (string(if-7:if).eq.'    0.0') if=if-8
      end do
      
      write(iout,'(1x,A)')string(1:if)
      write(iout,*)' '
 
 100  format(2x,a2,54x,a12)
 110  format('   -1   ')
 
 
 999  continue
 
      end
