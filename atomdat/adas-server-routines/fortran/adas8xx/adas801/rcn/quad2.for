C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/quad2.for,v 1.2 2004/07/06 14:39:31 whitefor Exp $ Date $Date: 2004/07/06 14:39:31 $
C
      subroutine quad2(m)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
c
c     calculate atomic coulomb potential  (integrate by simpsons rule)
c           xi=integral(rsatom),  xj=integral(rsatom/r)
c
  510 xi(1)=0.0
      xj(1)=0.0
      b2=0.0
      im=min0(mesh,ib+6)
c
      ho3=r(2)/3.0
      i=1
      do 515 j=1,nblock
      do 513 k=1,20
      i=i+2
      if (i.gt.ib) go to 520
      xi(i)=xi(i-2)+ho3*(rsatom(i-2)+4.0*rsatom(i-1)+rsatom(i))
      b0=b2
      b2=rsatom(i)/r(i)
  513 xj(i)=xj(i-2)+ho3*(b0+4.0*rsatom(i-1)/r(i-1)+b2)
      if (i.gt.idb) go to 515
      ho3=ho3+ho3
  515 continue
  520 ic=1
      if (i.eq.ib) go to 525
      i=i-1
      if (i.ne.ib) go to 525
      ic=2
      xi(i)=xi(i-1)+1.5*ho3*(rsatom(i-1)+rsatom(i))
      b0=b2
      b2=rsatom(i)/r(i)
      xj(i)=xj(i-1)+1.5*ho3*(b0+b2)
  525 if (ib.eq.mesh) go to 530
      do 527 i=ib,im
      xi(i)=xi(ib)
  527 xj(i)=xj(ib)
  530 rsint1=xi(ib)
      if (m.eq.2) rsint=rsint1
c
c           ruee=r*(electron-electron energy), xj=r*(total energy)
c
      do 535 i=1,im,2
  535 xi(i)=2.0*(xi(i)+r(i)*(xj(ib)-xj(i)))
      if (ic.eq.2) xi(ib)=2.0*xi(ib)
      do 540 i=3,ib,4
      b0=(xi(i)-xi(i-2)+xi(i)-xi(i+2))/8.0
      xi(i-1)=0.5*(xi(i)+xi(i-2))+b0
  540 xi(i+1)=0.5*(xi(i)+xi(i+2))+b0
      i1=mod(ib-1,4)
      if (i1.ne.1) go to 545
      i=ib-i1
      b0=(xi(i)-xi(i-2)+xi(i)-xi(i+2))/8.0
      xi(i+1)=0.5*(xi(i)+xi(i+2))+b0
  545 if (m.eq.1) return
      do 550 i=1,ib
      ruee(i)=xi(i)
  550 xj(i)=-twoz+ruee(i)+exf10*ruexch(i)
      do 560 i=ib,mesh
      ruee(i)=ruee(ib)
  560 xj(i)=xj(ib)
      return
      end
