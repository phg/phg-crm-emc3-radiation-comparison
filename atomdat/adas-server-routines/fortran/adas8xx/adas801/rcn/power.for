C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/power.for,v 1.2 2004/07/06 14:36:25 whitefor Exp $ Date $Date: 2004/07/06 14:36:25 $
C
      subroutine power
c
c          evaluate expectation values of r to n
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
      dimension s1(10,ko),s2(10,ko)
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
      dimension psq(kmsh),p(kmsh,10)
c     equivalence (xj,psq,p)
c
      integer fg
c
      mm=ncspvs
      if (ee(mm).gt.0.0) mm=mm-1
      if (mm.le.0) return
  350 if (itpow.ge.2. and.end.eq.1.0) write (9,35)
   35 format (///2x,'nl   wnl  ',7x,2hee,10x,2haz,
     1    6x,5h(r-3),6x,5h(r-2),6x,5h(r-1),6x,5h(r+1),6x,5h(r+2),
     2    6x,5h(r+3),6x,5h(r+4),6x,5h(r+6)/)
c
  400 kkk=0
      do 410 m=1,mm
      if (nkkk(m).gt.kkk) kkk=nkkk(m)
  410 continue
c
c          integrate
c
      do 420 k=1,kkk
      r1=r(k)
      r2=r1*r1
      r3=r1*r2
      p(k,2)=1.0/r3
      p(k,3)=1.0/r2
      p(k,4)=1.0/r1
      p(k,5)=r1
      p(k,6)=r2
      p(k,7)=r3
      p(k,8)=r2*r2
      p(k,9)=r2*r3
  420 p(k,10)=r3*r3
      do 599 m=1,mm
      kkk=nkkk(m)
      do 510 k=1,kkk
  510 psq(k)=pnl(k,m)**2
      do 530 n=2,10
      s2(n,m)=0.0
      do 520 k=1,kkk
  520 xi(k)=psq(k)*p(k,n)
      if (n.ne.3.or.l(m).ne.0) go to 530
      xi(1)=4.0*(xi(2)+xi(4))-6.0*xi(3)-xi(5)
  530 call quad5(xi,1,1,kkk,s1(n,m))
      rm3(m)=s1(2,m)
  599 continue
c               print <r> as a diagnostic (npr.ne.0.and.niter.ge.np)
      if (end.eq.1.0) go to 700
      n1=max(1,ncspvs-6)
      write (6,70) (nlbcd(n),s1(5,n), n=n1, ncspvs)
   70 format (' <r>=',7(4x,a3,f7.3))
      go to 900
c
  700 if (itpow.lt.2) go to 900
c
      fn=1.0e-20
      do 721 m=1,mm
  710 do 711 n=2,10
  711 s2(n,1)=s2(n,1)+wwnl(m)*s1(n,m)
      fn=fn+wwnl(m)
      s1(9,m)=s1(10,m)
      if (l(m).le.0) s1(2,m)=0.0
      if (ifrac.eq.0) then
        write (9,71) nlbcd(m),wwnl(m),ee(m),az(m),(s1(n,m),n=2,9)
   71 format (1x,a3,f6.0,f14.5,f11.3,1p,8e11.3)
      else
        write (9,72) nlbcd(m),wwnl(m),ee(m),az(m),(s1(n,m),n=2,9)
   72 format (1x,a3,f7.3,f13.5,f11.3,1p,8e11.3)
      end if
  721 continue
      if (mm.eq.1) return
      do 730 n=3,10
  730 s2(n,2)=s2(n,1)/fn
      r1=0.1
      s2(9,1)=s2(10,1)
      s2(9,2)=s2(10,2)
      if (ifrac.eq.0) then
        write (9,73)fn,(s2(n,1), n=3,9), r1,(s2(n,2),n=3,9)
   73 format (/4x,f6.0,36x,1p,7e11.3)
      else
        write (9,74)fn,(s2(n,1), n=3,9), r1,(s2(n,2),n=3,9)
   74 format (/4x,f7.3,35x,1p,7e11.3)
      end if
c
  900 return
      end
