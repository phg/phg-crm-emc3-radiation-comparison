C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/subcor.for,v 1.2 2004/07/06 15:23:41 whitefor Exp $ Date $Date: 2004/07/06 15:23:41 $
C
      subroutine subcor
c
c          calculate correlation energy density
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
c
  100 if (eras.gt.1.0e-20) go to 120
      b0=9999.9999
      b1=0.0
      b2=0.0
      b3=0.0
      go to 200
  120 b0=(3.0*eras1*eras1/eras)**.333333333e+00
      a1=sqrt(b0+9.0)
      a2=4.0*a1+1.142*b0
      ec=-1.0/a2
      b1=eras1*ec
      a2=ec*ec*(2.0/a1+1.142)
      b2=eras1*(ec-b0*a2/3.0)
      b3=-1.22177412*eras1/b0
      ec=b2/eras1
      b1=b2
      ec1=-1.0/(4.0*a1+0.75*1.142*b0)
      b2=eras1*ec1
      b1=b2
  200 return
      end
