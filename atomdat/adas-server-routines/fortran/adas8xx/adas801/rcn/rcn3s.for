C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/rcn3s.for,v 1.2 2004/07/06 14:44:21 whitefor Exp $ Date $Date: 2004/07/06 14:44:21 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Change arguments to mod function from
C                      mod(wwnl(m),1.0)
C               to     mod(int(wwnl(m)),1)
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine rcn3s
c
c          calculate ionization potentials and total energies
c           from values of slater coulomb integrals
c
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/brt/emag,eret,fmi(10,3),fni(10,4),fnorm(ko)
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
      common/sli/ xa(kmsh),xb(kmsh),f1(kmsh),f2(kmsh),rkp1(kmsh),
     1  vpar(7,20,ko),emn(ko,ko),rryd(10,2),rkay(10,2),k1(10,2),
     2  frac(10,2)
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      integer fg
c     equivalence (v,rkp1),(rscore,vpar)
c
      rdb=r(idb)
      if (iptvu.lt.3) go to 200
c
c          print overlap integrals
c
  100 write (9,30) conf,nconf,iz,ncores,nvales,ion,c,ca1
      write (9,10)
   10 format (18h0overlap integrals)
      n2=min0(ncspvs,12)
      write (9,33) (nlbcd(n), n=1,n2)
      write (9,35)
      do 120 m=1,ncspvs
      n2=min0(m,12)
  120 write (9,12) nlbcd(m), (vpar(7,m,n), n=1,n2)
   12 format (1h ,a3,f12.5,11f9.5)
      if (ncspvs.le.12) go to 200
      write (9,33) (nlbcd(n), n=13,ncspvs)
      write (9,35)
      do 140 m=13,ncspvs
  140 write (9,12) nlbcd(m), (vpar(7,m,n), n=13,m)
c
c          calc coefs of slater parameters, and interaction energies
c
  200 do 250 m=1,ncspvs
      b(m)=0.0
      flm=l(m)
      if (wwnl(m).gt.1.0) go to 202
      emn(m,m)=0.0
      go to 220
  202 emn(m,m)=vpar(1,m,m)
      if (l(m).le.0) go to 220
      im=l(m)+1
      fk=0.0
      do 206 i=2,im
      fk=fk+2.0
      coef=-s3j0sq(flm,fk,flm)*(2.0*flm+1.0)/(4.0*flm+1.0)
  206 emn(m,m)=emn(m,m)+coef*vpar(i,m,m)
c
  220 n1=m+1
      if (n1.gt.ncspvs) go to 250
      do 240 n=n1,ncspvs
      fln=l(n)
      k=iabs(l(m)-l(n))-2
      fk=k
      emn(m,n)=vpar(1,m,n)
      im=(l(m)+l(n)-k)/2+1
      do 225 i=2,im
      fk=fk+2.0
      coef=-s3j0sq(flm,fk,fln)/2.0
  225 emn(m,n)=emn(m,n)+coef*vpar(i,m,n)
      emn(n,m)=emn(m,n)
  240 continue
  250 continue
c
      n=ncspvs
      if (ee(n).lt.0.0) go to 300
      do 265 m=1,n
      emn(m,n)=0.0
  265 emn(n,m)=0.0
      evel(n)=0.0
      edar(n)=0.0
      ecorr(n)=0.0
      ucorr(n)=0.0
      epss(n)=0.0
c
  300 if (iptvu.eq.0) go to 400
      write (9,30) conf,nconf,iz,ncores,nvales,ion,c,ca1
   30 format (3h1  ,3a6,2x,10h    nconf=,i3,6h    z=,i3,11h    ncores=,
     1  i2,11h    nvales=,i2, 8h    ion=,i2, 14h    r(i)/x(i)=,f10.8,
     2  6x,4hca1=,f5.3)
      write (9,31) tolstb,tolkm2,tolend,thresh,kut, exf,corrf,ca0,ihf1,
     1  mesh,idb,rdb,emx,r(mesh),irel
   31 format (/8h0tolstb=,f5.3,3x,7htolk-2=,f7.5,3x,7htolend=,e8.1,
     1  3x,7hthresh=,e8.1,3x,4hkut=,i2,4x,4hexf=,f5.3,9h   corrf=,f5.3,
     2  6x,4hca0=,f6.3//18h0rcn mod 36  ihf1=,i2,
     3  10x,5hmesh=,i4,3x,4hidb=,i4,3x,4hrdb=,f8.3,3x,4hemx=,f7.2,
     4  3x,8hr(mesh)=,f8.3,5x,5hirel=,i2)
      if (iptvu.eq.1) go to 400
      write (9,32)
   32 format (21h0interaction energies)
      n2=min0(ncspvs,12)
      write (9,33) (nlbcd(n), n=1,n2)
   33 format (///5h     ,12(6x,a3))
      write (9,35)
      do 320 m=1,ncspvs
      n2=min0(m,12)
  320 write (9,34) nlbcd(m), (emn(m,n), n=1,n2)
   34 format (1h ,a3,f12.4,11f9.4)
      if (ncspvs.le.12) go to 400
      write (9,33) (nlbcd(n), n=13,ncspvs)
      write (9,35)
   35 format (1h )
      do 340 m=13,ncspvs
  340 write (9,34) nlbcd(m), (emn(m,n), n=13,m)
      if (ncspvs.le.12) go to 400
      write (9,30) conf,nconf,iz,ncores,nvales,ion,c,ca1
      write (9,31) tolstb,tolkm2,tolend,thresh,kut, exf,corrf,ca0,ihf1,
     1  mesh,idb,rdb,emx,r(mesh),irel
c
c          calc ionization potentials, eps(m), and total correlation ene
c
  400 write (9,40)
   40 format (  //1h0,15x,19hwithout correlation,16x,16hwith correlation
     1  /14x,23h-----------------------,11x,23h-----------------------/
     2  '  nl   wnl ',6x,6heps fg,6x,7heps fgr,4x,4hcorr,7x,6heps fg,6x,
     3  7heps fgr,5x,3hn*r,5x,4hn*rc,5x,3hect,5x,19hec-----------------,
     4  /)
      ect=0.0
      xi(1)=0.0
      fion=ion+1
      do 405 i=1,mesh
  405 xj(i)=0.0
      do 460 m=1,ncspvs
  409 a0=0.0
      do 412 np=1,ncspvs
      n=ncspvs+1-np
      eras=wwnl(n)
      if (m.ne.n) go to 411
      eras=eras-1.0
  411 a0=a0+eras*emn(m,n)
  412 a(n,1)=a0
      eps2=ei(m)+a0
      evd=0.0
      if (irel.ne.0) go to 415
      evd=evel(m)
      if (l(m).eq.0) evd=evd+edar(m)
  415 epscor=ecorr(m)
      eps1c=epss(m)
      eps1=eps1c-epscor
      eps2c=eps2+epscor
      eps2r=eps2+evd
      eps2cr=eps2c+evd
      ehf(m)=abs(eps2)
      fnstr=0.0
      fnstrc=0.0
      if (eps2.ge.0.0) go to 420
      fnstr=fion/sqrt(-eps2r)
      fnstrc=fion/sqrt(-eps2cr)
  420 k=wwnl(m)+0.999999
      kkk=nkkk(m)
      do 426 i=1,kkk
      xj(i)=0.0
      do 424 mm=1,ncspvs
      if (mm.eq.m) go to 424
      if (ee(mm).ge.ee(m)) go to 424
      xj(i)=xj(i)+wwnl(mm)*(pnl(i,mm)**2)
  424 continue
  426 continue
      do 440 lp=1,k
      ax=0.0
      if (ee(m).ge.0.0) go to 439
      do 435 i=2,kkk
      eras2=pnl(i,m)**2
      eras1=r(i)
      eras=xj(i)
      call subcor
      xi(i)=b1*eras2/r(i)
  435 xj(i)=xj(i)+eras2
      call quad5(xi,1,1,kkk,ax)
  439 kk=min0(lp,4)
      ecm(kk)=ax
      occ=1.0
      if (lp.eq.k) occ=mod(int(wwnl(m)),1)
      if (occ.lt.0.000001) occ=1.0
      ect=ect+occ*ax
  440 continue
      if (ifrac.eq.0) then
        write (9,44) nlbcd(m),wwnl(m),eps2,eps2r,epscor,eps2c,eps2cr,
     1  fnstr,fnstrc, ect, (ecm(k), k=1,kk)
   44 format (1x,a3,f6.0,1x,2f13.5,f8.5,2f13.5,2f8.4,2f10.5,4f7.4)
      else
        write (9,45) nlbcd(m),wwnl(m),eps2,eps2r,epscor,eps2c,eps2cr,
     1  fnstr,fnstrc, ect, (ecm(k), k=1,kk)
   45 format (1x,a3,f7.3,2f13.5,f8.5,2f13.5,2f8.4,2f10.5,4f7.4)
      end if
      etvd=etotrl
      do 450 mp=1,ncspvs
      eras=a(mp,1)*wwnl(m)
      if (m.lt.mp) go to 450
      eras=0.5*eras
  450 b(mp)=b(mp)+eras
      eeo(m)=ee(m)
      if (m.ge.n1sc) ee(m)=eps2cr
  460 continue
      etvd=etotrl
      emag=0.0
      eret=0.0
      if (irel.eq.2) call ebreit
c
c          calc total binding energy
c
  500 a0=0.0
      b0=0.0
      eras=0.0
      do 501 mp=1,ncspvs
      m=ncspvs+1-mp
      temp=1.0
      if (irel.ne.0) temp=0.0
      eras=eras+temp*(evel(m)+edar(m))*wwnl(m)
      erel(m)=eras
      a0=a0+ei(m)*wwnl(m)
      a(m,1)=a0+b(m)
      b0=b0+ecorr(m)*wwnl(m)
  501 b(m)=b0
      et2=a(1,1)+eterm
      et1c=etot+eterm
      et1=et1c-ecorrt
      et2c=et2+ecorrt
      et3c=et2+ect
      et2r=et2+etvd
      et2cr=et2c+etvd
      et3cr=et3c+etvd
      et4cr=et3cr+emag+eret
      write (9,49) ecorrt,ect,etotrl, emag,eret
   49 format (/1h0,27x,7hecorrt=,f10.5,6h (old),14x,4hect=,f10.5,
     1  6h (new),5x,7hetotrl=,f13.5,2h +,f9.5,2h +,f9.5)
      write (9,50)    et2,et2r,et2c,et2cr,et3c,et3cr,et4cr
   50 format( /'      etot=',2f13.5,f21.5,4f13.5)
c          store eav for rcg input
c
      vprm(1)=et4cr-eterm
c
c
c          print rcg input parameter values
c
  550 if (nzi.le.0) go to 560
      do 552 i=1,nzi
      npar=npar+1
      kpar(npar)=2
  552 vprm(npar)=zi(i)
  560 if (nfij.le.0) go to 570
      do 562 i=1,nfij
      npar=npar+1
      kpar(npar)=3
  562 vprm(npar)=fij(i)
  570 if (ngij.le.0) go to 580
      do 572 i=1,ngij
      npar=npar+1
      kpar(npar)=4
  572 vprm(npar)=gij(i)
c
  580 write (9,57) conf,npar, (vprm(i),kpar(i), i=1,npar)
   57 format (//1h ,3a6,i6,f12.4,i2,4(f10.4,i2)/1x,7(f10.4,i2)/
     1  1x,7(f10.4,i2)/1x,7(f10.4,i2)/1x,7(f10.4,i2))
      if (iw6.lt.0) write (6,58) npar, (vprm(i),kpar(i), i=1,npar)
   58 format (' Eav & pars',i6,f12.4,i2,4(f8.4,i2)/1x,7(f8.4,i2)/
     1  1x,7(f8.4,i2)/1x,7(f8.4,i2)/1x,7(f8.4,i2))
c
  600 return
      end
