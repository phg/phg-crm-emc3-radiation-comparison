C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/zk.for,v 1.2 2004/07/06 15:41:43 whitefor Exp $ Date $Date: 2004/07/06 15:41:43 $
C
      subroutine zk(m1,m2,m3,m4)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
      common/lc2/ra(kmsh),rb(kmsh)
c
      do 120 i=1,kkk
  120 xj(i)=pnl(i,m2)*pnl(i,m4)
      call quadk(m1,3)
      kp3=k+3
      do 160 i=3,kkk,2
  160 xj(i)=pnl(i,m1)*pnl(i,m3)*x1(i)/(r(i)**kp3)
      xj(1)=0.0
      do 170 i=3,kkk,4
      b0=(xj(i)-xj(i-2)+xj(i)-xj(i+2))*0.125
      xj(i-1)=0.5*(xj(i)+xj(i-2))+b0
  170 xj(i+1)=0.5*(xj(i)+xj(i+2))+b0
      xj(2)=0.0
      if (xj(5).ne.0.0) xj(2)=(xj(3)**2)/xj(5)
      do 180 i=4,14,2
      xm=xj(i-1)
      x0=xj(i)
      xp=xj(i+1)
      if (x0.lt.xm.and.x0.lt.xp) go to 175
      if (x0.gt.xm.and.x0.gt.xp) go to 175
      if (xm.eq.0.0) go to 180
      if (xp/xm.lt.6.0) go to 180
  175 xj(i)=sqrt(xm*xp)
      if (xp.lt.0.0) xj(i)=-xj(i)
  180 continue
      return
      end
