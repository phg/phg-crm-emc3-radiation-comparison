C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/sli1.for,v 1.3 2010/04/09 16:45:16 mog Exp $ Date $Date: 2010/04/09 16:45:16 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.2
C  DATE    : 09-04-2010
C  MODIFIED: Martin O'Mullane
C             - Literal constants in quotes, to be assigned to an
C               integer variable, are not allowed in data statements
C               (IBM allowed extension to f66) so replace with 
C               Hollerith constants (f77 standard).
C
C-----------------------------------------------------------------------
      subroutine sli1
c
c          evaluate slater radial coulomb integrals fk and gk
c               and radial overlap integrals
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
      common/sli/ xa(kmsh),xb(kmsh),f1(kmsh),f2(kmsh),rkp1(kmsh),
     1  vpar(7,ko,ko),emn(ko,ko),rryd(10,2),rkay(10,2),k1(10,2),
     2  frac(10,2)
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      integer fg
c     equivalence (v,rkp1),(rscore,vpar)
      data jhlf,jhlg/1hf,1hg/
      data jhuF,jhuG/1hF,1hG/
c
      npr1=0
      kdmin=0
      eterm=0.0
  120 do 969 ma=1,ncspvs
      la=l(ma)
      i1max=nkkk(ma)
      do 959 mb=ma,ncspvs
      lb=l(mb)
      i2max=nkkk(mb)
      imax=max0(i1max,i2max)
      kemin=iabs(la-lb)
      nokmax=min0(la,lb)+1
      ne=nokmax
      nd=nokmax
      if (ma.ne.mb) go to 400
      ne=0
      if (wwnl(ma).le.1.0) nd=0
c
c          calc pnl-products for direct or exchange case
c
  400 do 700 n1=1,2
      if (n1.le.1) go to 420
      if (ne.le.0) go to 500
      do 415 i=1,imax
      f1(i)=pnl(i,ma)*pnl(i,mb)
  415 f2(i)=f1(i)
      k=kemin-2
      go to 500
  420 do 425 i=1,imax
      f1(i)=pnl(i,ma)**2
  425 f2(i)=pnl(i,mb)**2
      k=kdmin-2
      if (nd.le.0) go to 800
c
c          calc for each value of k
c
  500 do 700 n=1,nokmax
      a1=0.0
      b1=1.0
      k=iabs(k+2)
      k1(n,n1)=k
      if (ee(ma).ge.0.0.or.ee(mb).ge.0.0) go to 650
      if (n1.lt.ne+2) go to 520
      k1(n,n1)=0
      go to 650
  520 continue
c
      rkp1(1)=0.001
      do 530 i=2,imax
      if (n.gt.1) go to 529
      rkp1(i)=r(i)
      if (k.le.0) go to 530
      do 527 m=1,k
  527 rkp1(i)=rkp1(i)*r(i)
      go to 530
  529 rkp1(i)=rkp1(i)*r(i)*r(i)
  530 continue
c
c          calc r1 integral
c
      xi(1)=0.0
      xj(1)=0.0
      xa(1)=0.0
      xb(1)=0.0
      a2=0.0
      b2=0.0
      ho12=r(2)/12.0
      ni=40
      do 550 i=3,i1max,2
      a0=a2
      b0=b2
      eras=8.0*f1(i-1)
      a1=eras*rkp1(i-1)/r(i-1)
      b1=eras/rkp1(i-1)
      a2=f1(i)*rkp1(i)/r(i)
      b2=f1(i)/rkp1(i)
      eras=ho12*(5.0*a0+a1-a2)
      xi(i-1)=xi(i-2)+eras
      xa(i-1)=xa(i-2)+abs(eras)
      eras=ho12*(5.0*b0+b1-b2)
      xj(i-1)=xj(i-2)+eras
      xb(i-1)=xb(i-2)+abs(eras)
      eras=ho12*(-a0+a1+5.0*a2)
      xi(i)=xi(i-1)+eras
      xa(i)=xa(i-1)+abs(eras)
      eras=ho12*(-b0+b1+5.0*b2)
      xj(i)=xj(i-1)+eras
      xb(i)=xb(i-1)+abs(eras)
      ni=ni-2
      if (ni) 540,540,550
  540 ho12=ho12+ho12
      ni=40
      if (i.eq.idb) ni=10000
  550 continue
c
      do 560 i=2,i1max
      eras=rkp1(i)/r(i)
      xi(i)=xi(i)/rkp1(i)+(xj(i1max)-xj(i))*eras
  560 xa(i)=xa(i)/rkp1(i)+(xb(i1max)-xb(i))*eras
      if (i2max-i1max) 600,600,569
  569 a0=xi(i1max)*rkp1(i1max)
      b0=xa(i1max)*rkp1(i1max)
      do 570 i=i1max,i2max
      xi(i)=a0/rkp1(i)
  570 xa(i)=b0/rkp1(i)
c
c          calc r2 integral
c
  600 a0=0.0
      b0=0.0
      a1=0.0
      b1=0.0
      ho3=r(2)/1.5
      ni=40
      do 620 i=3,i2max,2
      a2=f2(i)*xi(i)
      b2=abs(f2(i))*xa(i)
      eras=4.0*f2(i-1)
      a1=a1+ho3*(a0+eras*xi(i-1)+a2)
      b1=b1+ho3*(b0+abs(eras)*xa(i-1)+b2)
      a0=a2
      b0=b2
      ni=ni-2
      if (ni.gt.0) go to 620
      ho3=ho3+ho3
      ni=40
      if (i.eq.idb) ni=10000
  620 continue
c
  650 frac(n,n1)=a1/b1
      rryd(n,n1)=a1
      rkay(n,n1)=a1*109737.31
      if (nfg.eq.0) go to 700
c          calc ls-term contribution to e(av)
      nla=nlbcd(ma)
      nlb=nlbcd(mb)
      do 670 j=1,nfg
      if ((n1.eq.1.and.fg(j,ii).ne.jhlf.and.fg(j,ii).ne.jhuF).or.
     1    (n1.eq.2.and.fg(j,ii).ne.jhlg.and.fg(j,ii).ne.jhuG))
     1  go to 670
      if (k1(n,n1).ne.kfg(j,ii)) go to 670
      if ((ifg(j,ii).ne.nla.or.jfg(j,ii).ne.nlb).and.
     1    (jfg(j,ii).ne.nla.or.ifg(j,ii).ne.nlb)) go to 670
      eterm=eterm+cfg(j,ii)*a1
  670 continue
  700 continue
      if (iptvu.lt.3) go to 840
c
c          calc overlap integrals
c
  800 i=1
      xif=(r(imax)-r(imax-1))*5.0/288.0
      eras=0.0
      if (ee(ma).ge.0.0.or.ee(mb).ge.0.0) go to 830
  810 if (i.le.idb) eras=0.5*eras
      do 820 n=1,8
      i=i+5
  820 eras=eras+19.0*(f1(i-5)+f1(i))+75.0*(f1(i-4)+f1(i-1))
     1         +50.0*(f1(i-3)+f1(i-2))
      if (i-imax) 810,830,830
  830 vpar(7,mb,ma)=eras*xif
      if (nd.le.0) go to 959
  840 if (iptvu.eq.0) go to 915
      if (ma.lt.ncspvs-iabs(norbpt)+1) go to 915
c
c          output
c
  899 if (npr1) 900,900,905
  900 write (9,90) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
   90 format (17h1slater integrals///1x,3a6,6x,6hnconf=,i3,6x,2hz=,i3,
     1  5x,4hion=,i2,5x,4hkut=,i2,6x,4hexf=,f5.3,5x,6hcorrf=,f5.3,3x,
     2  4hca1=,f5.3,7h   ca0=,f5.3//)
  901 write (9,91)
   91 format (1h0,16x,1hk,19x,2hfk,21x,4hfrac,9x,1hk,19x,2hgk,21x,
     1  4hfrac)
      write (9,92)
   92 format (1h ,16x,39h-   -----------------------------------,
     1  8h   -----,9x,39h-   -----------------------------------,
     2  8h   -----//)
  905 npr1=npr1+nokmax+1
      if (npr1-50) 910,910,906
  906 npr1=0
      go to 900
  910 do 911 n=1,nokmax
  911 write (9,93) nlbcd(ma),nlbcd(mb), (k1(n,n1),rryd(n,n1),rkay(n,n1),
     1  frac(n,n1), n1=1,2)
   93 format (2h (,a3,1h,,a3,1h),i8,f13.7,7h ryd  =,f13.3,5h cm-1,f8.3,
     1  i10,f13.7,7h ryd  =,f13.3,5h cm-1,f8.3)
      write (9,94)
   94 format (1h )
c
c          store slater integrals for rcg input
c
  915 a1=4*l(ma)+1
      a2=4*l(mb)+1
      if (wwnl(ma).gt.a1) go to 950
      if (ma.ne.mb) go to 930
      if (wwnl(ma).le.1.0) go to 950
      if (wwnl(ma).ge.a1) go to 950
      do 924 i=2,nokmax
      npar=npar+1
      kpar(npar)=1
  924 vprm(npar)=rkay(i,1)*0.001
      go to 950
  930 if (wwnl(mb).gt.a2) go to 950
      if (wwnl(ma).eq.0.0.or.wwnl(mb).eq.0.0) go to 950
      do 934 i=1,nokmax
      if (k1(i,1).le.0) go to 933
      nfij=nfij+1
      fij(nfij)=rkay(i,1)*0.001
  933 ngij=ngij+1
  934 gij(ngij)=rkay(i,2)*0.001
c
  950 if (ma.ne.mb) go to 955
      do 953 i=1,nokmax
  953 vpar(i,ma,mb)=rryd(i,1)
      go to 959
  955 vpar(1,ma,mb)=rryd(1,1)
      do 956 i=1,nokmax
  956 vpar(i+1,ma,mb)=rryd(i,2)
  959 continue
c
  969 continue
c
      call rcn3s
  980 return
      end
