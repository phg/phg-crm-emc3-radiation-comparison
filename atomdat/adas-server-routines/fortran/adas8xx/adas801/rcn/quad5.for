C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/quad5.for,v 1.2 2004/07/06 14:39:36 whitefor Exp $ Date $Date: 2004/07/06 14:39:36 $
C
      subroutine quad5(fi,ipow,i1,i2,a1)
c
c     integrate fi (ipow=1) or fi**2 (ipow=2) from mesh point i1 to i2.
c          integration is by 8 applications of newton-cotes closed
c            quadrature for five intervals on each block.
c if i2-1 not a multiple of 5, fill out remainder
c   of mesh by simpsons and/or trapezoidal rule.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      dimension fi(kmsh)
c
      mmsave=mm
      xif=(r(i1+1)-r(i1))*2.5/288.0
      a1=0.0
      j=i1
      k=j-40*(j/40)
      mm=8-k/5
      i3=5*((i2-1)/5)+1
      if (ipow.eq.2) go to 200
      go to 106
c
  105 mm=8
  106 mm1=(i2-j)/5
      mm=min0(mm,mm1)
      if (j.eq.idb) mm=mm1
      xif=xif+xif
      a2=0.0
  110 a2=a2+19.0*(fi(j)+fi(j+5))+75.0*(fi(j+1)+fi(j+4))
     1   +50.0*(fi(j+2)+fi(j+3))
      j=j+5
      mm=mm-1
      if (ib.ge.0) go to 115
      a3=a1+xif*a2
      write (9,10) ib,j,r(j),fi(j),xif,a2,a1,a3
   10 format (2i6,f12.5,1p,5e16.6)
  115 if (mm.gt.0) go to 110
      a1=a1+xif*a2
      if (j.lt.i3) go to 105
      if (j.eq.i2) go to 150
      xif=(r(i2)-r(i2-1))/3.0
  120 if (j.eq.i2-1) go to 130
      a1=a1+xif*(fi(j)+4.0*fi(j+1)+fi(j+2))
      j=j+2
      if (j.eq.i2) go to 150
      go to 120
  130 a1=a1+1.5*xif*(fi(j)+fi(j+1))
  150 mm=mmsave
      return
c
  200 fj5=fi(j)**2
      go to 206
  205 mm=8
  206 mm1=(i2-j)/5
      mm=min0(mm,mm1)
      if (j.eq.idb) mm=mm1
      xif=xif+xif
      a2=0.0
  210 fj=fj5
      fj5=fi(j+5)**2
      a2=a2+19.0*(fj+fj5)+75.0*(fi(j+1)**2+fi(j+4)**2)
     1  +50.0*(fi(j+2)**2+fi(j+3)**2)
      j=j+5
      mm=mm-1
      if (mm.gt.0) go to 210
      a1=a1+xif*a2
      if (j.lt.i3) go to 205
      if (j.eq.i2) go to 250
      xif=(r(i2)-r(i2-1))/3.0
  220 if (j.eq.i2-1) go to 230
      fj=fj5
      fj5=fi(j+2)**2
      a1=a1+xif*(fj+4.0*fi(j+1)**2+fj5)
      j=j+2
      if (j.eq.i2) go to 250
      go to 220
  230 a1=a1+1.5*xif*(fj5+fi(j+1)**2)
  250 mm=mmsave
      return
      end
