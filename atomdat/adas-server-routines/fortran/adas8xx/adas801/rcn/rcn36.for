C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/rcn36.for,v 1.2 2004/07/06 14:44:15 whitefor Exp $ Date $Date: 2004/07/06 14:44:15 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase inf to 80 characters.
C               Change opening of files.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      program rcn36
      implicit real*8 (a-h,o-z)	
      include 'common.h'
c     program rcn36(in36,tape10=in36,out36,tape9=out36,tape11,
c    1  indiel,tape12=indiel,tape4,tape2n,tape2=tape2n,tape7)
c
c          This program copyright by los alamos national laboratory
c               and the u.s. government.  the program may be freely
c               distributed gratis anywhere in the world, but
c               may not be sold.
c
c
c     hartree-fock-slater self-consistent atomic field program(kut=1,0)
c          with options for hartree (kut=-1, exfm1=0.0) or for
c               hartree-plus-statistical-exchange (kut=-1, exfm1.gt.0)
c               or for hartree-slater (kut=-2)
c               or for hartree-fock (ihf=2)
c
c      originally  written by sherwood skillman,
c           rca laboratories, princeton, new jersey, spring 1961
c      modified by frank herman, summer 1961
c      further modified by richard kortum,  lockheed research
c           laboratories, palo alto, california,  summer 1962
c      further mod by r. d. cowan, lasl, jan-mar1964, oct 65-mar 66,
c           may-june 1966, nov-dec 1966.
c      further mod by k. l. andrew, lasl, may 1968, to simplify
c           configuration input
c      method of calculating correlation-energy correction
c           modified january, 1971.
c      calculation of continuum functions modified june, 1972
c           to handle kinetic energies up to 500 rydbergs
c           (no mesh-size doubling beyond point idb at which
c                deltar=0.25/sqrt(emx)   )
c      finite-boundary and relativistic options added spring,1973
c      hartree-fock options (ihf=2) added april and august 1981
c      more flexible configuration-card input introduced (mod 33)
c           at purdue, july-august 1987
c      Vinti-integral option added at Royal Holloway Bedford New College,
c           (England) october 1987
c      option to delete writing of output on tape2 when the configuration
c           card contains an asterisk anywhere in columns 21 to 26
c           added at the Zeeman Laboratory (Amsterdam), december 1987.
c
c      rcn mod 34, adapted to the vax 8200 at Lund University (Sweden),
c           made march 1988.  further modified to use generic functions,
c           etc., at LANL, july-december, 1988
c      rcn mod 35, incorporating subroutines
c           diel and analyz1 to set up input for dielectronic-
c           recombination runs, made at LANL january 1989
c      This version (mod 36) with commons of the same name having the
c           same length in all routines, and with timing routine
c           SECONDS coded for CRAY, VAX and MACINTOSH, SUN, or
c           IBM System/6000 RISC, made at LANL February-April 1992
c               (uncomment appropriate section of this routine).
c      Modifications for Macintosh Centris (using Language Systems
c           Fortran), and feature to write some output to the monitor
c           screen if IW6<0, added May 1993.
c      Vers. 36.0.1 (August 1996): Minor change at rcn3s 930+ and 
c           a(5,5) -> a(ko,5) in blank common, to remove unimportant
c           problems when compiled with bounds checking.
c      Vers. 36.0.2 (September 28, 1996): Additions before statement
c           200 of subroutine scheq to cure a bug for irel=1, by
c           smoothing out a singularity in the relativistic potential
c           from the term  (dP/dr)/P -1/r in Eq. (7.60) of my book.
c      Vers. 36.0.3 (March 1997): For neutral and singly ionized 
c           lanthanide and actinide systems, change default values of 
c           alfmin and alfmax from 0.2 and 1.0 to 0.15 and 0.75;  
c           at rcn 540, change way in which alfmin and alfmax are 
c           modified.  Also, modify code in region 710-730.
c      Vers. 36.1.3 (June 1998).  All equivalence statements removed.
c      Vers. 36.1.4 (Dec 1998).  Blank commons changed to labeled.
c      Vers. 36.1.5 (May 1999).  Removed numerous unused variables that
c           give warning diagnostics with some compilers.
c      Vers. 36.2.0 (Aug 1999).  Added the possibility of using fractional
c           occupation numbers (maximum of three decimal places; i.e.,
c           maximum of 4 to 6 characters, according as the integral part
c           of the occupation number is 0, 1, or 2 digits, respectively).
c                Also changed the action of the diagnostic variable npr,
c           improved the algorithm for normalizing continuum functions,
c           and added warning prints to the monitor if the value of emx
c           is inappropriate for highly excited or continuum functions.
c         NOTE: If any occupation number is non-integral, nothing will
c           be written on the file tape2n for that "configuration", 
c           as fractional-occupation-number results are not appropriate 
c           for either rcn2 or rcg.
c               -----------------------------------------------
c
c
c     binary tape 2  contains self consistent atomic potential,
c      energy eigenvalues, and radial wave functions
c      successive solutions separated by end of logical record.
c     z = atomic number.  ion = ionicity.  zzz = ion+1.
c     rho=total electron density
c     rhom=electron density of min(wwnl(m),2.0) electrons in orbital m
c     ruee=total classical electron-electron potential
c     rueem=same for one electron of orbital m
c     ruexch=-2.0*(3.0*rho/pi)**(1.0/3.0)
c     ru=-2.0*z+ruee+exf10*ruexch
c     kut= 1, rv=min1f(ru,-2.0*ion) ---hfs (no tail cutoff)
c     kut= 0, rv=min1f(ru,-2.0*zzz) ---hfs (with latter tail cutoff)
c     kut=-1, ---hx or hartree
c       rv=-2*z+ruee-rueem+exfm1*ruexch*(1.0-rhom/rho)*f1*f2
c          f1=(rho-rhom)/(rho-rhom+ca0/(n-l))
c          f2=1.0 except for l(m) greater than 1 and either
c            l(m-1)=l(m), or l(m-2)=l(m) and wwnl(m-1)=1,
c                 in which cases
c          f2=1.0+ca1*(r(j)-r(i))/r(j)  for i less than j,
c            where r(j) is the position of the kth node of pnl(m)
c               (k=no. of orbitals with l=l(m) and n less than n(m).)
c     ihf=1, write output on tape7 as input to program hf8
c     ihf=2, make self-contained hartree-fock calculation within rcn
c
c     maxit = maximum no. of iterations
c     exf=factor multiplying slater exchange term
c     corrf=factor multiplying correlation term
c
c
      character*80 inf
c
c          ----------------------------------------------------------
c          comment out IMPLICIT REAL*8 cards for 64-bit machines,
c               uncomment them for VAX, SUN, etc.
c          ----------------------------------------------------------
c          open files, if not done via program card
c               (use iii>1 and program card at start
c                of program for cray and cdc computers)
c               (otherwise, comment out program card that defines files and
c                set iii=1 to use standard file names, or
c                set iii=0 to name files interactively, e.g. for vax)
c           ---------------------------------------------------------
c

C------Start ADAS conversion-------
     
     
      common/c7/t0,t1,t2, ibatch

      
      iii=2
      
      read(5,*)ibatch
      
      read(5,'(A)')inf
      open(10,file=inf,status='old')
      read(5,'(A)')inf
      open(9,file=inf,status='unknown')
      read(5,'(A)')inf
      open(2,file=inf,status='unknown',form='unformatted')
      
      open(7,status='scratch',form='unformatted')
      open(4,status='scratch',form='unformatted')
      
C------End ADAS conversion-------
      
      
      if (iii.eq.1) go to 150
      if (iii.gt.1) go to 160
c
      write(6,*) ' input file name  '
      read(5,11) inf
   11 format(a20)
      open(10,file=inf,status='old')
      write(6,*) ' output file name  '
      read(5,12) inf
   12 format (a)
      open(9,file=inf,status='unknown')
      write(6,*) ' give name of data file for the rcn2-program '
      read(5,12) inf
      if(inf.ne.'   ') go to 120
      open(2,status='scratch',form='unformatted')
      go to 155
  120 open(2,file=inf,status='unknown',form='unformatted')
      go to 155
c
c          use these open statements for standard file names
c               (set iii=1 in statement above)
c
  150 open(10,file='in36',status='old')
      open(9,file='out36',status='unknown')
      open(2,file='tape2n',status='unknown',form='unformatted')
  155 continue
      open(7,status='scratch')
      open(4,status='scratch',form='unformatted')
c
c
  160 rewind 10
      read (10,10,end=200) inf(1:5)
   10 format (a5)
      if (inf(1:5).ne.'   -1') go to 300
c
c          if tape10 is empty or the first record is an exit card, 
c               call diel to set up rcn input for dielectronic recombination runs
c
  200 if (iii.lt.2) open(12,file='indiel',status='old')
      if (iii.lt.2) open(11,status='scratch')
      call diel_rcn
c
c          call rcn for (final) calculation of radial wavefunctions
c
  300 rewind 10
      call rcn
      
      
      end
