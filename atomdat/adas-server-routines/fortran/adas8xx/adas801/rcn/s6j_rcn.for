C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/s6j_rcn.for,v 1.2 2004/07/06 15:18:41 whitefor Exp $ Date $Date: 2004/07/06 15:18:41 $
C 
      function s6j(fj1,fj2,fj3,fl1,fl2,fl3)
c
c          calculate 6-j symbol
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      a6j=0.0
      f1=delsq(fj1, fj2, fj3)*delsq(fj1, fl2, fl3)
     1*delsq(fl1, fj2, fl3)*delsq(fl1, fl2, fj3)
      if (f1.le.0.0) go to 214
  210 a=fj1+fj2+fj3
      b=fj1+fl2+fl3
      c=fl1+fj2+fl3
      d=fl1+fl2+fj3
      e=fj1+fj2+fl1+fl2
      f=fj2+fj3+fl2+fl3
      g=fj3+fj1+fl3+fl1
      fi=max(a,b,c,d)
      i1=fi+0.001
      i2=min(e,f,g)+0.001
      if (i2.lt.i1) go to 214
  211 do 212 i=i1,i2
      f2=fctrl(fi-a)*fctrl(fi-b)*fctrl(fi-c)*fctrl(fi-d)
      f3=fctrl(e-fi)*fctrl(f-fi)*fctrl(g-fi)
      a6j=a6j+((-1.0)**mod(i,2))*fctrl(fi+1.0)/(f2*f3)
  212 fi=fi+1.0
      s6j=a6j*sqrt(f1)
      return
  214 s6j=a6j
  215 return
      end
