C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/outpt.for,v 1.2 2004/07/06 14:26:55 whitefor Exp $ Date $Date: 2004/07/06 14:26:55 $
C
      subroutine outpt
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      integer fg
      data hdg1,hdg2,hdg3/6hrc(a0),6hr (a0),6h  pnl(/
c
  800 rewind 4
      read (4) ru,ruee,ruexch,rsatom,recorr,rucorr
c
c     compute first term of series. (pnl(r)/r**(lam+1) at r=0)
c
      do 807 m=1,ncspvs
      lp=l(m)+1
      do 806 i=1,4
      a(i,1)=1.0
      a(i,2)=r(i+1)
      a(i,3)=r(i+1)*r(i+1)
      a(i,4)=r(i+1)*a(i,3)
  806 a(i,5)=pnl(i+1,m)/(r(i+1)**lp)
      call crosym (4)
      az(m)=a(1,5)
  807 continue
      etotrl=0.0
      if (tolend.gt.1.0e-4) go to 987
      if (ihf.eq.0.or.ncspvs.le.1) go to 808
      if (irel.ne.0) go to 990
      go to 982
c
c          calculate one-electron energy integrals
c
  808 twooalf=2.0*137.0360
      froasq=twooalf**2
      do 850 m=1,ncspvs
      read (4) (v(i), i=1,mesh)
      if (m.lt.n1sc) go to 850
  810 kkk=nkkk(m)
      qnl(1,m)=0.0
      xi(1)=0.0
      do 812 i=2,kkk
      xj(i)=pnl(i,m)**2
      eras1=r(i)
      eras=rscore(i)-xj(i)
      if (ee(ncspvs).lt.0.0) eras=eras+rsvale(i)
      call subcor
      recorr(i)=b1
      rucorr(i)=b2
      if (irel.ne.2.or.ee(m).ge.0.0) go to 812
      eras=twooalf/(froasq-v(i)+ee(m))
      qnl(i,m)=eras*((pnl(i+1,m)-pnl(i-1,m))/(r(i+1)-r(i-1))
     1  -pnl(i,m)/r(i))
  812 pnlo(i)=xj(i)/r(i)
      if (irel.eq.2.and.ee(m).lt.0.0) qnl(kkk,m)=0.0
      a1=0.0
  815 do 840 n=1,6
      if (ee(m)) 820,820,840
  820 do 835 i=2,kkk
      go to (821,822,823,824,825,826), n
  821 xi(i)=xj(i)*twoz/r(i)
      go to 835
  822 xi(i)=xj(i)*v(i)
      go to 835
  823 xi(i)=pnlo(i)*ruee(i)
      go to 835
  824 xi(i)=pnlo(i)*ruexch(i)
      go to 835
  825 xi(i)=pnlo(i)*rucorr(i)
      go to 835
  826 xi(i)=pnlo(i)*recorr(i)
  835 continue
      call quad5(xi,1,1,kkk,a1)
  840 b(n)=a1
      ekin(m)=ee(m)-b(2)
      een(m)=-b(1)
      ei(m)=ekin(m)+een(m)
      uee(m)=b(3)
      uex(m)=b(4)*exf10
      ucorr(m)=b(5)
      ecorr(m)=b(6)
      epss(m)=ei(m)+uee(m)+1.5*b(4)+ecorr(m)
  850 continue
      do 870 i=1,mesh
      if (ee(ncspvs).le.0.0) go to 869
      rsatom(i)=rscore(i)
      go to 870
  869 rsatom(i)=rscore(i)+rsvale(i)
  870 continue
      rewind 4
c
c       output
c
  900 eitot=0.0
      ekint=0.0
      eentot=0.0
      eeetot=0.0
      eextot=0.0
      ucorrt=0.0
      ecorrt=0.0
      do 907 m=1,ncspvs
      if (ee(m).ge.0.0) go to 907
      eitot=eitot+ei(m)*wwnl(m)
      ekint=ekint+ekin(m)*wwnl(m)
      eentot=eentot+een(m)*wwnl(m)
      eeetot=eeetot+uee(m)*wwnl(m)
      eextot=eextot+uex(m)*wwnl(m)
      ucorrt=ucorrt+ucorr(m)*wwnl(m)
      ecorrt=ecorrt+ecorr(m)*wwnl(m)
  907 continue
c
      eeetot=0.5*eeetot
      eextot=0.75*eextot/exf10
      etot=eitot+eeetot+eextot+ecorrt
      ecorrk=-ecorrt+3.0*(ucorrt-ecorrt)
      ecorrp=ecorrt-ecorrk
      etotk=ekint+ecorrk
      etotp=etot-etotk
c
      if (ipteb-1) 917,912,908
  908 write (9,84) ncelec,nvelec,noelec,rsint
   84 format (8h1ncelec=,i3,11h    nvelec=,i2,11h    noelec=,i3,
     1  10h    rsint=,f10.6///
     2  '0 m   nl    w  imt kkk    e (ryd)', 6x,5he kin,7x,5he e-n,
     3  6x,5hu e-e,5x,6hu exch,4x,6hu corr,6x,5heps s,
     4  5x,6he corr,18h   no scheq cycles//)
      i1=min0(niter,10)
      do 910 m=1,ncspvs
      if (ifrac.eq.0) then
        write (9,85) m,nlbcd(m),wwnl(m),imat(m),nkkk(m),ee(m),ekin(m),
     1 een(m),uee(m),uex(m),ucorr(m),epss(m),ecorr(m),(nsch(i,m),i=1,i1)
   85 format (i3,1x,a3,f6.0,1x,2i4,3f12.5,f11.5,2f10.5,f12.5,f10.5,2i3,
     1  8i2)
      else
        write (9,86) m,nlbcd(m),wwnl(m),imat(m),nkkk(m),ee(m),ekin(m),
     1 een(m),uee(m),uex(m),ucorr(m),epss(m),ecorr(m),(nsch(i,m),i=1,i1)
   86 format (i3,1x,a3,f7.3,2i4,3f12.5,f11.5,2f10.5,f12.5,f10.5,2i3,8i2)
      end if
  910 continue
      write (9,87) etot,ekint,eentot,eeetot,eextot,ecorrt,etotk,etotp,
     1  ecorrk,ecorrp
   87 format (/1h0,15x,5hetot=,f13.5,1h=,f11.5,f12.5,1h+,2f10.5,f32.5///
     1    22x,10he tot k,p=,f11.5,f12.5,27x,11he corr k,p=,2f10.5)
c
  912 write (9,77)
   77 format (//1h2/'   nl   w',10x,2hee,9x,1hj,8x,4hr(j),9x,2haz/)
      do 915 m=1,ncspvs
      j=jjj(m)
      if (ifrac.eq.0) then
        write (9,78) nlbcd(m),wwnl(m),ee(m),jjj(m),r(j),az(m)
   78 format (1x,a3,f6.0,f15.5,i8,f12.5,f12.3)
      else
        write (9,79) nlbcd(m),wwnl(m),ee(m),jjj(m),r(j),az(m)
   79 format (1x,a3,f7.3,f14.5,i8,f12.5,f12.3)
      end if
  915 continue
  917 read (4) ru,ruee,ruexch,rsatom,recorr,rucorr
      kkk=0
      do 920 m=1,ncspvs
      if (nkkk(m)-kkk) 920,920,919
  919 kkk=nkkk(m)
  920 continue
      do 930 m=1,ncspvs
      k=nkkk(m)+5
      if (k-kkk) 925,925,930
  925 do 926 i=k,kkk,5
  926 pnl(i,m)=0.0
  930 continue
      kkk=min(kkk,mesh-5)
c
      if (iptvu.lt.4) go to 945
      write (9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
   89 format (1h1,3a6,4x,6hnconf=,i3,6x,2hz=,i3,5x,4hion=,i2,5x,4hkut=,
     1  i2,6x,4hexf=,f5.3,5x,6hcorrf=,f5.3,7h   ca1=,f5.3,7h   ca0=,
     2  f5.3///)
  933 write (9,90)
   90 format (25x,12houtput ru(i),10x,9hru e-e(i),11x,9hruexch(i),
     1  11x,9hrucorr(i),11x,9hrecorr(i))
      write (9,91) hdg1
   91 format (4x,a6,4x,2hnc,1x,5(9x,1h1,9x,1h6)//)
      nc=-1
      do 940 i=1,kkk,10
      nc=nc+1
  940 write (9,29) rc(i),nc,ru(i),ru(i+5),ruee(i),ruee(i+5), ruexch(i),
     1  ruexch(i+5),rucorr(i),rucorr(i+5),recorr(i),recorr(i+5)
   29 format (1h ,f9.4,i6,4x,10f10.4)
c
  945 if (norbpt.le.0) go to 980
      if (irel.ne.2) go to 948
      do 947 m=1,ncspvs
      do 947 i=6,mesh,10
  947 qnl(i-5,m)=pnl(i,m)
  948 write (9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
      k=min0(ncspvs,2)
      write (9,92) (hdg3,nlbcd(m), m=1,k)
   92 format (27x,9hrscore(i),11x,9hrsvale(i),11x,9hrsatom(i),
     1  2(9x,a6,a3,2h) ))
      write (9,91) hdg2
      nc=-1
      do 950 i=1,kkk,10
      nc=nc+1
  950 write (9,93) r(i),nc,rscore(i),rscore(i+5),rsvale(i),rsvale(i+5),
     1  rsatom(i),rsatom(i+5), (pnl(i,m),pnl(i+5,m), m=1,k)
c    1  rsatom(i),rsatom(i+5), (pnl(i,m),qnl(i,m), m=1,k)
   93 format (1h ,f9.4,i6,4x,6f10.5,4f10.6)
c
  955 if (k.ge.ncspvs) go to 961
      k=max0(k,ncspvs-norbpt)
      j=k+1
      k=min0(k+5,ncspvs)
      write (9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
      write (9,94) (hdg3,nlbcd(m), m=j,k)
   94 format (16x,5(9x,a6,a3,2h) ))
      write (9,91) hdg2
      nc=-1
      do 960 i=1,kkk,10
      nc=nc+1
  960 write (9,95) r(i),nc, (pnl(i,m),pnl(i+5,m), m=j,k)
c 960 write (9,95) r(i),nc, (pnl(i,m),qnl(i,m), m=j,k)
   95 format (1h ,f9.4,i6,4x,10f10.6)
      go to 955
c
  961 if (ee(ncspvs).le.0.0) go to 980
      if (norbpt.ge.6) go to 963
      if (nconft.gt.1.and.mod(nconft,5).ne.0) go to 980
  963 write(9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
      write (9,94) hdg3,nlbcd(ncspvs)
      write (9,28)
   28 format (16h    r (a0)    nc,10x,1h1,9x,1h2,9x,1h3,9x,1h4,9x,1h5,
     1  9x,1h6,9x,1h7,9x,1h8,9x,1h9,8x,2h10//)
      nc=-1
      do 970 imin=1,kkk,10
      nc=nc+1
      imax=imin+9
  970 write (9,95) r(imin),nc, (pnl(i,ncspvs), i=imin,imax)
c
c
c          calc expectation values of r**n, spin-orbit pars,
c            relativistic corrs, and slater integrals
c
  980 lines=54
      if (itpow.eq.1.or.itpow.ge.3) lines=lines-3-niter
      if (ipteb.gt.0) lines=60-13-ncspvs
      if (ipteb.gt.1) lines=30-2-ncspvs
      if (iptvu.ge.4.or.norbpt.gt.0) lines=0
      if (itpow.lt.2) go to 983
      if (lines.gt.ncspvs+9) go to 981
      write (9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
      lines=60
  981 lines=lines-9-ncspvs
  983 call power
      if (lines.gt.ncspvs+6) go to 982
      write (9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
      lines=60
  982 call zeta1
      if (ihf.eq.1.and.ncspvs.gt.1) go to 987
      lines=lines-6-ncspvs
      if (iptvu.gt.0) go to 985
      if (lines.gt.ncspvs+19) go to 985
      write (9,89) conf,nconf,iz,ion,kut,exf,corrf,ca1,ca0
  985 call sli1
  987 rewind 4
      read (4) ru,ruee,ruexch,rsatom
      rewind 4
c
      do 988 i=1,ncspvs
  988 nnlz(i)=100*nnn(i)+l(i)
      norb=1+ncspvs-ns
      if (nnn(ncspvs).gt.98) ion=ion-1
      if (iz.eq.izo.and.ion.eq.iono) go to 989
      izo=iz
      iono=ion
      ieras=iabs(norbpt)
      norb=min0(ncspvs,max0(2,ieras))
      if (ieras.eq.9) norb=ncspvs
      ns=1+ncspvs-norb
  989 if (ihf.ne.0.and.ncspvs.gt.1.and.(ncspvs.gt.2.or.nnn(2).le.90))
     1  go to 990
      if (nconf.eq.0.or.ifrac.eq.1) go to 995
      write (2) nnn,l,wwnl,ncspvs,vprm,conf,nconf,iz,z,ion,irel,hxid,
     1  mesh,c,idb,exf,corrf,kut,npar,nlbcd,nnlz,nkkk,ee,ee8,norb,
     2  r,ru,((pnl(i,m),i=1,kmsh),m=ns,ncspvs),iw6
      go to 995
  990 call hfwrtp
c
  995 return
      end
