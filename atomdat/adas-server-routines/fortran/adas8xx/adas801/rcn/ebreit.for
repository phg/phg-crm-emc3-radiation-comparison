C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/ebreit.for,v 1.2 2004/07/06 13:47:52 whitefor Exp $ Date $Date: 2004/07/06 13:47:52 $
C
      subroutine ebreit
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
      common/brt/emag,eret,fmi(10,3),fni(10,4),fnorm(ko)
      dimension uab(kmsh),vab(kmsh)
c     equivalence (uab,rscore),(vab,rsvale)
c
c          calculate normalization constants
c
      mmax=ncspvs
      if (nnn(ncspvs).gt.98) mmax=ncspvs-1
      do 110 m=1,mmax
      kkk=nkkk(m)
      do 105 i=1,kkk
      xi(i)=pnl(i,m)**2+qnl(i,m)**2
      call quad5(xi,1,1,kkk,a1)
  105 fnorm(m)=a1
  110 continue
c
      emag=0.0
      eret=0.0
c
c          sum over all pairs of orbitals
c
      do 690 mb=1,mmax
      if (l(mb).gt.4) go to 690
      do 680 ma=1,mb
      if (l(ma).gt.4) go to 680
      if (ma.eq.mb.and.wwnl(ma).eq.1.0) go to 680
      kkk=min0(nkkk(ma),nkkk(mb))
      do 120 i=1,kkk
      eras=pnl(i,ma)*qnl(i,mb)
      eras1=pnl(i,mb)*qnl(i,ma)
      uab(i)=eras-eras1
  120 vab(i)=eras+eras1
c
c          calculate mi and ni integrals
c
      kp1x=l(ma)+l(mb)+2
      do 150 kp1=1,kp1x
      do 125 i=1,4
      if (i.eq.4) go to 125
      fmi(kp1,i)=0.0
  125 fni(kp1,i)=0.0
      k=kp1-1
      if (ma.eq.mb) go to 135
      do 130 i=1,kkk
  130 xj(i)=uab(i)
      call quadk(31,1)
      call brtint(1,ma,mb)
  135 do 140 i=1,kkk
  140 xj(i)=vab(i)
      call quadk(32,1)
  150 call brtint(2,ma,mb)
c
c          sum over both kappas, each orbital
c
      fkapb=l(mb)
      if (fkapb.eq.0.0) fkapb=-1.0
  200 eras=2*l(mb)+1
      qb=wwnl(mb)*abs(fkapb)/eras
      fjb=abs(fkapb)-0.5
      fkapa=l(ma)
      if (fkapa.eq.0.0) fkapa=-1.0
  210 eras=2*l(ma)+1
      qa=wwnl(ma)*abs(fkapa)/eras
      fja=abs(fkapa)-0.5
      kp1n0=abs(fja-fjb)+1.0
      kp1x=fja+fjb+1.0
      flmag=0.0
      flret=0.0
c
c          calculate lambda(kapa,kapb) terms
c
  300 kp1n=kp1n0
      if (mod(l(ma)+kp1n0+l(mb),2).eq.0) kp1n=kp1n0+1
      if (ma.eq.mb.and.fkapa.eq.fkapb) go to 400
      eras=fkapb-fkapa
      do 320 kp1=kp1n,kp1x,2
      k=kp1-1
      fk=k
      fm1=0.5
      fm2=0.0
      flambda=s3j(fja,fk,fjb,fm1,fm2)**2
      t1=fk*fmi(k,1)
      if (eras.ne.0.0) t1=t1+eras*(2.0*fmi(k,2)+eras*fmi(k,3)/fk)
      t1=t1/(2.0*fk-1.0)
      fkp1=fk+1.0
      t2=fkp1*fmi(k+2,1)
      if (eras.ne.0.0) t2=t2+eras*(-2.0*fmi(k+2,2)+eras*fmi(k+2,3)/fkp1)
      t2=t2/(2.0*fk+3.0)
      x=fk*fkp1*(fni(k+2,1)-fni(k,1))
      if (eras.eq.0.0) go to 310
      x=x+eras*(fkp1*(fni(k+2,2)-fni(k,2))+fk*(fni(k,3)-fni(k+2,3)))
      x=x+(eras**2)*(fni(k,4)-fni(k+2,4))
  310 flmag=flmag+flambda*(t1+t2)
      flret=flret+flambda*(fk*t1+fkp1*t2+x)/(fk+fkp1)
      if (itpow.eq.4)
     1  write (9,3100) ma,mb,fkapa,fkapb,k,flambda,t1,t2,x,flmag,flret
 3100 format (2i4,2f5.1,i4,6f12.7)
  320 continue
c
c          calculate lambda(-kapa,kapb) terms
c
  400 eras=fkapa+fkapb
      if (eras.eq.0.0) go to 500
      kp1n=kp1n-1
      if (kp1n.lt.kp1n0) kp1n=kp1n+2
      do 420 kp1=kp1n,kp1x,2
      k=kp1-1
      fk=k
      half=0.5
      zero=0.0
      flambda=s3j(fja,fk,fjb,half,zero)**2
      flmag=flmag+flambda*(eras**2)*fmi(k+1,3)/(fk*(fk+1.0))
      if (itpow.eq.4)
     1  write (9,3100) ma,mb,fkapa,fkapb,k,flambda,flmag
  420 continue
c
c
c
  500 eras=2.0/(fnorm(ma)*fnorm(mb))
      flmag=eras*flmag
      flret=-eras*flret
      if (ma.eq.mb.and.fkapa.eq.fkapb) go to 520
      eras=qa*qb
      emag=emag+eras*flmag
      eret=eret+eras*flret
      go to 530
  520 emag=emag+qa*(qa-1.0)*flmag*(fja+0.5)/(2.0*fja)
  530 continue
      if (itpow.eq.4)
     1 write (9,5300) ma,mb,fkapa,fkapb,qa,qb,eras,flmag,flret,emag,eret
 5300 format (2i4,2f5.1,2f8.4,5f12.7)
      if (fkapa.lt.0.0) go to 600
      fkapa=-fkapa-1.0
      go to 210
c
  600 if (fkapb.lt.0.0) go to 680
      fkapb=-fkapb-1.0
      go to 200
  680 continue
  690 continue
c
  700 return
      end
