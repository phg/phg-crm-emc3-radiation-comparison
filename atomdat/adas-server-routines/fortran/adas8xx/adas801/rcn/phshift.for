C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/phshift.for,v 1.2 2004/07/06 14:28:11 whitefor Exp $ Date $Date: 2004/07/06 14:28:11 $
C
      subroutine phshift
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      dimension fmz(5,2),rz(5,2),phase(5,2),shift(5)
c
      pi=3.141592653590
      twopi=2.0*pi
      twozz=twozzz-2.0
      mm=ncspvs
      eras=l(mm)
      aa=0.5*pi*eras
      fk=sqrt(ee(mm))
      n1=5
      i1=1
      do 120 i=1000,mesh
  120 pnlo(i)=pnl(i,mm)
      go to 300
c
c          calculate (non-relativistic) coulomb function
c
  200 do 210 i=2,mesh
      v(i)=-twozz/r(i)
  210 x2(i)=v(i)
      nn=nnn(mm)
      lam=l(mm)
      e=ee(mm)
      irel1=irel
      irel=0
      call scheq
      irel=irel1
c
c          calc distorted-wave or coulomb phase shift
c
  300 m=mesh-5
      do 330 n=1,n1
  310 m=m-1
      if (pnlo(m).lt.0.0) go to 310
      if (pnlo(m-1).gt.0.0) go to 310
      m=m-1
      ll=m
      if (abs(pnlo(m)).gt.pnlo(m+1)) ll=m+1
c          find node via r=c1+c2*p+c3*(p**3)
      a1=pnlo(ll-1)
      a2=pnlo(ll)
      a3=pnlo(ll+1)
      a13=a1**3
      a23=a2**3
      a33=a3**3
      b1=r(ll-1)
      b2=r(ll)
      b3=r(ll+1)
      b0=b1*(a2*a33-a3*a23)+b2*(a3*a13-a1*a33)+b3*(a1*a23-a2*a13)
      a0=a1*(a23-a33)+a2*(a33-a13)+a3*(a13-a23)
      rz(n,i1)=b0/a0
      f1=m
      fmz(n,i1)=f1+(rz(n,i1)-r(m))/(r(m+1)-r(m))
      eras=fk*rz(n,i1)+0.5*twozz*log(2.0*fk*rz(n,i1))/fk-aa
      eras=mod(eras,twopi)
      if (eras.le.0.0) eras=eras+twopi
      phase(n,i1)=eras
      if (i1.ne.2) go to 330
      shift(n)=phase(n,1)-phase(n,2)
      if (shift(n).lt.0.0) shift(n)=shift(n)+twopi
  330 continue
      if (i1.eq.2) go to 400
      i1=2
      go to 200
c
  400 write (9,40) (fmz(n,1),rz(n,1),phase(n,1),fmz(n,2),rz(n,2),
     1  phase(n,2),shift(n), n=1,n1)
   40 format (//2x,7hmesh pt,5x,1hr,5x,8hdw phase,7x,7hmesh pt,5x,
     1  1hr,6x,7hcoul ph,4x,8hph shift/(f9.2,f9.3,f10.5,f14.2,f9.3,
     2  f10.5,f12.5))
      return
      end
