C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/blkdtarcn.for,v 1.2 2004/07/06 11:41:49 whitefor Exp $ Date $Date: 2004/07/06 11:41:49 $
C
      block data
 
      implicit real*8 (a-h,o-z)
      character*6 wwwnl(25),wwnl8(8)
      common/charww/wwwnl,wwnl8
      include 'common.h'
      include 'rcn.h'
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
c
      data iblank /2h  /
      data (morb(i),i=1,7)   /1,3,5,8,11,15,19/
      data (lbcd(i),i=1,21)/1hs,1hp,1hd,1hf,1hg,1hh,1hi,1hk,1hl,1hm,
     1                      1hn,1ho,1hq,1hr,1ht,1hu,1hv,1hw,1hx,1hy,1hz/
      data (wwwnl(m),m=1,25) /'0 ','1 ','2 ','3 ','4 ','5 ','6 ','7 ',
     1           '8 ','9 ','10','11','12','13','14','00','01','02','03',
     2           '04','05','06','07','08','09'/
c
      end
