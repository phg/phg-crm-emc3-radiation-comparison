C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/brtint.for,v 1.2 2004/07/06 11:42:59 whitefor Exp $ Date $Date: 2004/07/06 11:42:59 $
C
      subroutine brtint(n,ma,mb)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/brt/emag,eret,fmi(10,3),fni(10,4),fnorm(ko)
c     dimension uab(kmsh),vab(kmsh)
c     equivalence (uab,rscore),(vab,rsvale)
c
c
      kp1=k+1
  120 xi(1)=0.0
      xj(1)=0.0
      do 130 i=2,kkk
  130 xi(i)=x1(i)/(r(i)**kp1)
      if (ma.eq.mb) go to 300
c
      do 210 i=1,kkk
  210 v(i)=rscore(i)*xi(i)
      call quad5(v,1,1,kkk,a1)
      fni(kp1,n)=a1
c
  300 do 310 i=1,kkk
  310 v(i)=rsvale(i)*xi(i)
      call quad5(v,1,1,kkk,a1)
c     write (9,3100) n,a1,(xi(i),i=1,50),(xj(i),i=1,50),(v(i),i=1,240)
c3100 format (//i5,f14.6/(10f12.5))
      fni(kp1,n+2)=a1
      if (n.eq.1) go to 400
      fmi(kp1,1)=2.0*fni(kp1,1)
      fmi(kp1,2)=fni(kp1,2)+fni(kp1,3)
      fmi(kp1,3)=2.0*fni(kp1,4)
c     write (9,3100) n,a1,(xi(i),i=1,50),(xj(i),i=1,50),(v(i),i=1,240)
  400 continue
      if (itpow.eq.4)
     1  write (9,40) k,(fmi(kp1,i),i=1,3), (fni(kp1,i),i=1,4)
   40 format (/i5,3f10.6,5x,4f10.6)
      return
      end
