C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/delsq.for,v 1.2 2004/07/06 13:30:15 whitefor Exp $ Date $Date: 2004/07/06 13:30:15 $
C
      function delsq(a,b,c)
c
c          short (but slower) version calling factorial function
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      d=a+b
      e=d+c
c     if (aint(e).ne.e) go to 120
      tenm6=1.0e-6
      if (abs(aint(e)-e).gt.tenm6) go to 120
      f=a-b
      delsq=fctrl(d-c)*fctrl(c+f)*fctrl(c-f)/fctrl(e+1.0)
      return
  120 delsq=0.0
      return
      end
