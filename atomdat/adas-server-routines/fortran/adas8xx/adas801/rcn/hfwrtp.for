C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/hfwrtp.for,v 1.2 2004/07/06 14:05:45 whitefor Exp $ Date $Date: 2004/07/06 14:05:45 $
C
      subroutine hfwrtp
c
c     subroutine written by d.c.griffin to convert hx output to hf input
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      integer fg
      dimension maxi(ko),rhf(200),phf(ko,200)
c     equivalence (ruexch,maxi),(xi,rhf),(v,phf)
c
c
c
  200 rhohf=-3.0
      h=0.0625
      no=200
      rhoi=rhohf
      do 250 i=1,no
      rhf(i)=exp(rhoi)/z
  250 rhoi=rhoi+h
      notemp=no
      if (rhf(200).gt.r(mesh)) notemp=log(r(mesh)/rhf(1))/h
      nufsh=0
      do 270 i=1,ncspvs
      if(wwnl(i).eq.(4.*float(l(i))+2.)) go to 270
      nufsh=nufsh+1
  270 continue
      write (7) conf,nconf,iz,z,ncspvs,rhohf,h,no,nhftrm,irel,etotrl,
     1  nufsh,izhxbw,ns,iphfwf
c
      if (nhftrm.eq.0) go to 502
      do 501 i=1,nhftrm
      nfg=nf(i)+ng(i)
  501 write (7) t2hf(i),nf(i),ng(i),nfg,
     1  (cfg(j,i),fg(j,i),kfg(j,i),ifg(j,i),jfg(j,i),j=1,nfg)
c
  502 do 503 i=1,ncspvs
      j=imat(i)
      rhfmtc(i)=r(j)
      msend=nkkk(i)
      rend=r(msend)
      max=log(rend/rhf(1))/h
      if(max.gt.notemp) max=notemp
      maxi(i)=max
  503 write (7) nlbcd(i),wwnl(i),az(i),ehf(i),rhfmtc(i),max
c
      jmin=2
      do 510 i=1,no
      do 504 m=1,ncspvs
  504 phf(m,i)=0.0
      do 508 j=jmin,mesh
      if (rhf(i).gt.r(j)) go to 508
      jm2=j-2
      jm1=j-1
      if (mod(jm2,40).eq.0.and.jm2.lt.idb) jm2=j-3
      u=(rhf(i)-r(jm2))/(r(jm1)-r(jm2))
      u2=0.5*u*(u-1.0)
      rhm1=1.0/sqrt(rhf(i))
      do 506 m=1,ncspvs
      if (i.gt.maxi(m).or.nnn(m).gt.90) go to 506
      del1=pnl(jm1,m)-pnl(jm2,m)
      del2=pnl(j,m)-pnl(jm1,m)
      phf(m,i)=(pnl(jm2,m)+del1*u+(del2-del1)*u2)*rhm1
  506 continue
      go to 509
  508 continue
  509 jmin=j-1
  510 continue
  520 write (7) (rhf(i),i=1,no), ((phf(i,j),j=1,no), i=1,ncspvs)
c
      meshi=min0(mesh,1001)
      if (nnn(ncspvs).gt.10) write (7) meshi,idb,
     1  (r(j),pnl(j,ncspvs), j=1,meshi)
c
      if (ihf.lt.3) go to 700
      idel=1
      if (iphfwf.lt.0) idel=4
      write(9,103)
  103 format (1h1,50x,22hhf input wavefunctions)
      npwftp=ncspvs-10
      if (npwftp.lt.1) npwftp=1
      write(9,104) (nlbcd(i),i=npwftp,ncspvs)
  104 format (13x,3hrhf,6x,a3,10(7x,a3))
      do 14 i=1,no,idel
      eras=sqrt(rhf(i))
      do 15 j=npwftp,ncspvs
      phf(j,i)=phf(j,i)*eras
   15 continue
   14 continue
      do 13 i=1,no,idel
      write (9,102) i,rhf(i),(phf(j,i),j=npwftp,ncspvs)
  102 format (i5,f12.5,11f10.5)
   13 continue
  700 return
      end
