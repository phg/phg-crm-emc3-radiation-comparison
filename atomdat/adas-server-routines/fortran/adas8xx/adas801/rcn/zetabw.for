C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/zetabw.for,v 1.2 2004/07/06 15:41:37 whitefor Exp $ Date $Date: 2004/07/06 15:41:37 $
C
      function zetabw(m,rm3)
c
c      calculate spin-orbit parameter via blume-watson method
c       proc. roy. soc. (london) a270, 127 (1962); a271, 565 (1963)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,dummy(ko),iw6,nconft,ifrac
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      dimension snkk(ko),vkk(ko)
c
      sp=0.0
      if (l(m).eq.0) go to 400
      flm=l(m)
      e2=-12*l(m)-6
      e3=3.0*sqrt((2.0*flm+1.0)/(flm*(flm+1.0)))
      sp=z*rm3*5.843660
      if (ihf1.gt.5) write (9,8810) m,flm,e2,e3,sp
 8810 format (/i10,4f15.6)
c
      do 290 mp=1,ncspvs
      if (ee(mp).gt.0.0) go to 290
      if (mp.eq.m) go to 290
      lp=l(mp)
      flp=lp
      wp=wwnl(mp)
      k=0
      sp=sp-2.0*wp*sm(m,mp)
c
      kn=iabs(l(m)-lp)-2
      kx=l(m)+lp
      inv=-1
      do 130 k=kn,kx
      snkk(k+3)=0.0
      vkk(k+3)=0.0
      inv=-inv
      if (inv.lt.0) go to 120
      if (k.lt.-1) go to 130
      if (k.eq.kx.and.l(m).eq.lp) go to 130
      snkk(k+3)=sn(m,mp)
      go to 130
  120 if (k.lt.0) go to 130
      vkk(k+3)=vk(m,mp)
  130 continue
      if (ihf1.gt.5) write (9,8830) m,mp,nlbcd(m),nlbcd(mp),flp,wp,sp,
     1  (k,snkk(k),vkk(k), k=1,9)
 8830 format (/2i10,3x,a4,3x,a4,3f15.6,/(i10,2f30.6))
      kn=kn+2
      kxm2=kx-2
      if (kxm2.lt.kn) go to 200
      sum2=0.0
      do 160 k=kn,kxm2
      fk=k
      fkp1=fk+1.0
      eras=s6j(fk,1.0d0,fkp1,flm,flp,flm)**2
      eras=eras*s3j0sq(flm,fk,flp)
      eras=eras*(2.0*fk+1.0)*(2.0*fk+3.0)/(fk+2.0)
      sum2=sum2+eras*snkk(k+3)
      if (ihf1.gt.5) write (9,8840) k,fk,eras,snkk(k+3),sum2
 8840 format (/i10,5f15.6)
  160 continue
      sp=sp-e2*wp*sum2
c
  200 e13=flp*(flp+1.0)-flm*(flm+1.0)
      if (ihf1.gt.5) write (9,8850) sp,e13
 8850 format (/4f15.6)
      if (kn.eq.0) kn=2
      sum13=0.0
      do 220 k=kn,kx
      fk=k
      eras=sqrt(fk*(fk+1.0)*(2.0*fk+1.0))
      eras=eras*s3j0sq(flm,fk,flp)
      eras=eras*s6j(fk,1.0d0,fk,flm,flp,flm)
      eras1=e13*(snkk(k+1)/fk-snkk(k+3)/(fk+1.0))
      sum13=sum13+eras*(vkk(k+2)+eras1)
      if (ihf1.gt.5) write (9,8840) k,fk,eras,eras1,sum13,vkk(k+2)
  220 continue
      sp=sp-e3*wp*sum13
      if (ihf1.gt.5) write (9,8850) sp,e3,wp,sum13
  290 continue
c
  300 w=wwnl(m)
      if (w.lt.2.0) go to 400
      k=0
      sm0=sm(m,m)
      sp=sp-(2.0*w-3.0)*sm0
      if (ihf1.gt.5) write (9,8850) sp,sm0
      if (flm.lt.2.0) go to 400
      e=6.0*((2.0*flm+1.0)**2)
      kn=2
      kx=2*l(m)-2
      do 330 k=kn,kx
      fk=k
      fkp1=fk+1.0
      sum=0.0
      fkp=fk-2.0
c---------------------
c ADAS Conversion Changes 
c Changed "do 320 kp=1,2" to "do 320 kkp=1,2"
c (kp is a parameter in the other stages)
c ADW - 12/12/01
c---------------------
      do 320 kkp=1,2
      fkp=fkp+2.0
      eras=s6j(fkp1,1.0d0,fkp,flm,flm,flm)**2
      eras=eras*s3j0sq(flm,fkp,flm)
  320 sum=sum+(fkp1-fkp)*(2.0*fkp+1.0)*eras
      smk=sm(m,m)
      sum1=sum*(2.0*fk+3.0)*smk
      sp=sp+e*sum1
      if (ihf1.gt.5) write (9,8840) k,eras,sum,smk,sum1,sp
  330 continue
c
  400 zetabw=sp
      return
      end
