C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/rcn.for,v 1.3 2010/04/09 16:45:16 mog Exp $ Date $Date: 2010/04/09 16:45:16 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Terminate with return rather than a stop.
C
C  Martin O'Mullane 
C  
C  VERSION : 1.2
C  DATE    : 09-04-2010
C  MODIFIED: Martin O'Mullane
C             - Literal constants in quotes, to be assigned to an
C               integer variable, are not allowed in data statements
C               (IBM allowed extension to f66) so replace with 
C               Hollerith constants (f77 standard).
C
C-----------------------------------------------------------------------
      subroutine rcn
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
      common/lc2/ra(kmsh),rb(kmsh)
c
      common/c9/ qq(kmsh),delp(ko),alfm(ko)
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      character*6 wwwnl(25),wwnl8(8)
      common/charww/wwwnl,wwnl8
      integer fg
c
      dimension ru0(91),deltn(200)
      character*8 chxid
      data hxid1,hxid2,hfid1,hfid2 /2hxb,2hxr,2hhf,2hhr/
      data ru0/1.0,0.99094,0.98164,0.97218,0.96266,0.95311,0.94359,
     1  0.93413,0.92475,0.91549,0.90634,0.88844,0.87109,0.85429,
     2  0.83802,0.82227,0.80700,0.79220,0.77785,0.76393,0.75044,
     3  0.72473,0.70066,0.67816,0.65710,0.63732,0.61866,0.60097,
     4  0.58416,0.56814,0.55285,0.52427,0.49813,0.47416,0.45214,
     5  0.43184,0.41304,0.39553,0.37916,0.36377,0.34927,0.32262,
     6  0.29887,0.27789,0.25953,0.24348,0.22926,0.21645,0.20468,
     7  0.19376,0.18354,0.16493,0.14852,0.13412,0.12152,0.11053,
     8  0.10094,0.09255,0.08517,0.07866,0.07289,0.06311,0.05556,
     9  0.048,0.042,0.037,0.033,0.030,0.025,0.021,0.017,0.014,0.011,
     a  0.009,0.007,0.006,0.005,0.004,0.003,0.002,0.001,0.0,0.0,0.0,
     b  0.0,0.0,0.0,0.0,0.0,0.0,0.0/
      data jhlf/1hf/
      data jhuF/1hF/
c
c      kmsh=1801
c      ko=20
c
c
c     position tapes 2, 4, and 7 for next run
c
  160 rewind 2
      rewind 4
      rewind 7
      meshx=kmsh
      mesh=meshx
      izo=-7
      iono=-7
      do 165 m=1,20
      eeo(m)=-7.0
      wwnlo(m)=0.0
  165 nlbcdo(m)=' '
      ncspvo=0
      ns=1
      nconf=0
      nconft=0
      nconft2=0
      rsint1=0.0
      do 170 i=1,mesh
      ruexch(i)=0.0
  170 ruee(i)=0.0
c
c          expand atomic potential from data block
c
  195 m=0
      do 196 i=1,361,4
      m=m+1
  196 ru(i)=ru0(m)
      call seconds(t0)
      t0=t0
      t1=t0
c
c          read control card.
c
  200 read (10,20) itpow,iptvu,ipteb,norbpt,izhxbw,iphfwf,ihf,ibb,
     1  tolstb,tolkm2,tolend,thresh,
     2  kutd,kut1,ivinti,irel,maxit,npr,exf10,exfm1,emx,corrf,iw6
   20 format (3i1,i2,i1,2i2,i3,f2.1,e5.1,2e10.1,i2,i2,2i1,2i2,4f5.5,i5)
      ca0=0.50
      ca1=0.70
      ihf1=ihf
      if (ihf.gt.1) ihf=0
      thres1=thresh
      itemp1=kmshq-kmsh
      itemp2=koq-ko
      if ((itemp1.lt.0.or.itemp2.lt.0).and.irel.eq.2) irel=1
      hxid=hxid1
      if (irel.ne.0) hxid=hxid2
      if (ihf1.gt.1) hxid=hfid1
      if (ihf1.gt.1.and.irel.ne.0) hxid=hfid2
      chxid='        '
      if (ihf1.gt.1) chxid='hf      '
      if (ihf1.gt.1.and.irel.ne.0) chxid='hfr     '
      if (kutd.eq.0) kutd=-2
      maxit0=maxit
      if (emx.gt.0.0) go to 204
c          set emx = largest continuum energy (if any)
      iread=0
c 201 read (10,19) n,iz,ee8
c  19 format (i1,i4,69x,0pf6.5)
  201 read (10,21) n,iz,ibb1,nspect,inpcard
      call analyz
      iread=iread+1
      if (iz.le.0) go to 202
      emx=max(emx,ee8)
      if (n.eq.0) go to 201
      do 1201 i=1,n
      read (10,18) nfg
   18 format (10x,i5)
      if (nfg.gt.3) read (10,18)
      if (nfg.gt.7) read (10,18)
 1201 iread=iread+1+nfg/4
      go to 201
c
  202 if (iread.eq.0) go to 204
      do 203 i=1,iread
  203 backspace 10
c
  204 if (emx.le.0.0) emx=1.0e-20
      mesh=meshx
      if (emx.le.1.0e-20) mesh=min0(mesh,641)
      ib=ibb
      if (ibb.eq.0.and.mesh.lt.meshx) ib=mesh
      if (ibb.eq.0.and.ib.eq.0) ib=mesh-1
      icut=mesh
      if (irel.eq.0) go to 210
      do 205 i=1,mesh
  205 vr(i)=0.0
c
c          read z, stage of ionization, and configuration.
c
c 210 read (10,21) nhftrm,iz,ibb1,nspect,conf,ialfmin,ialfmax,
c    1  (nlbcd8(m),wwnl8(m), m=1,8), kut,ee8
c  21 format (i1,i4,i3,i2,3a6,2i2,8(a3,a2),i2,0pf6.5)
c     alfmin=0.01*ialfmin
c     alfmax=0.01*ialfmax
  210 read (10,21) nhftrm,iz,ibb1,nspect,inpcard
   21 format (i1,i4,i3,i2,a70)
      n1scf=1
      if (ibb1.le.ko.and.nconft.gt.0) n1scf=max0(1,ibb1+1)
      if (ibb1.le.ko) ibb1=0
      if (iz.eq.-999) return
      if (iz) 212,200,214
  212 end file 2
      rewind 2
      end file 9
      rewind 9
      if (ihf.ne.0) go to 213
C------Start ADAS conversion-------
     
C      stop '(normal exit)'
      return
      
C------End ADAS conversion-------
     
  213 ncspvs=0
      if (rhohf.eq.0.0) rhohf=-3.0
      if (h.eq.0.0) h=0.065
      if (no.eq.0) no=200
      if (nufsh.eq.0) nufsh=0
      write (7) conf,nconf,iz,z,ncspvs,rhohf,h,no,nhftrm,irel,etotrl,
     1  nufsh,izhxbw,ns,iphfwf
      end file 7
      rewind 7
      stop '(cff hf exit)'
  214 if (inpcard(1:6).ne.'      ') go to 216
c          skip over iz completed runs on tape 2
      do 215 n=1,iz
      read (2) nnn,l,wwnl,ncspvs,vprm,conf,nconf,iz1,z,ion,irel,hxid,
     1  mesh,c,idb,exf,corrf,ca1,ca0,kut,npar,nlbcd,nnlz,nkkk,ee,norb,
     2  r,ru
  215 write (9,22) nconf, (conf(i), i=1,3)
   22 format (36h0on tape 2, skip over configuration ,i3,1h=,3x,3a6)
      nconft=nconf
      nconft2=nconf
      go to 210
  216 if (ibb1.gt.0) ib=ibb1
      if (nspect.eq.0)  nspect = 1
      noelec = iz + 1 - nspect
c          analyze inpcard
      call analyz
c
      nf(1)=0
      ng(1)=0
      if (ihf.eq.0) go to 217
      if (irel.gt.0.and.noelec.gt.2) go to 217
      if (tolend.gt.5.e-5) tolend=5.e-8
      if (thresh.gt.1.e-8) thresh=1.e-11
      thres1=thresh
  217 if (nhftrm.eq.0) go to 230
      do 220 i=1,nhftrm
      read (10,23) t1hf(i),t2hf(i),nfg,
     1  (cfg(j,i),fg(j,i),kfg(j,i),ifg(j,i),jfg(j,i), j=1,3)
   23 format (a7,a3,i5,5x,3(f9.8,a1,i1,1x,a3,1x,a3,1x))
      if (nfg.gt.3) read (10,24)
     1  (cfg(j,i),fg(j,i),kfg(j,i),ifg(j,i),jfg(j,i), j=4,nfg)
   24 format (4(f9.8,a1,i1,1x,a3,1x,a3,1x))
      nf(i)=0
      do 218 j=1,nfg
      if (fg(j,i).eq.jhlf.or.fg(j,i).eq.jhuF) nf(i)=nf(i)+1
  218 continue
  220 ng(i)=nfg-nf(i)
  230 if (alfmin.eq.0.)  alfmin=0.20
      if (alfmax.eq.0.)  alfmax=1.0
      alfmax0=alfmax
      alffix=0.4*alfmax
      if (kut.eq.-1) alffix=alfmax
      if (kut.eq.0)  kut = kut1
      if (iz.ne.izo.and.kut.lt.0) kut=0
      maxit=maxit0
      z=iz
      twoz=z+z
      nvales=1
      c=0.88534138/z**(1.0/3.0)
      nblock=(mesh)/40
c
      if (itpow.lt.8) go to 240
      write (9,26) iz,nspect,conf,alfmin,alfmax, (nlbcd8(m),wwnl8(m),
     1  m=1,8), ee8,alffix
   26 format (29h1card image except for alphas,2(2x,i3),3a6, f5.3,1x,
     1  f5.3, 8(a3,a6),f8.5, 8halffix =,f5.3 //  )
c
c           set up orbitals and estimate eigenvalues
c
  240 call setcfg
      if (ierror.ne.0) go to 210
      if (ion.gt.1) go to 250
      if (iz.lt.55.or.iz.gt.102) go to 250
      if (iz.gt.70.and.iz.lt.87) go to 250
      temp=0.75
      alfmax=min(alfmax,temp)
      alfmin=0.2*alfmax
      alffix=0.4*alfmax
      alfmax0=alfmax
      if (kut.eq.-1) alffix=alfmax
  250 if (ihf.eq.0.or.nnn(ncspvs).le.10) go to 260
      if (tolend.gt.5.e-5) tolend=5.e-8
      if (thresh.gt.1.e-8) thresh=1.e-11
      thres1=thresh
  260 if (n1sc.gt.1.or.n1scf.gt.1) go to 300
c
c          construct atomic potential
c
  270 if (nconft.gt.0) go to 280
      do 272 i=1,357,4
  272 ru(i)=-ru(i)*twoz
      ru(361)=ru(357)
      ru(365)=ru(357)
      m=9
      do 275 i=1,357,4
      m=m-1
      if (m.ge.0) go to 274
      ru(i+1)=(22.0*ru(i)+11.0*ru(i+4)-ru(i+8))/32.0
      ru(i+2)=(10.0*ru(i)+15.0*ru(i+4)-ru(i+8))/24.0
      ru(i+3)=( 6.0*ru(i)+27.0*ru(i+4)-ru(i+8))/32.0
      m=9
      go to 275
  274 ru(i+1)=(21.0*ru(i)+14.0*ru(i+4)-3.0*ru(i+8))/32.0
      ru(i+2)=( 3.0*ru(i)+ 6.0*ru(i+4)-    ru(i+8))/ 8.0
      ru(i+3)=( 5.0*ru(i)+30.0*ru(i+4)-3.0*ru(i+8))/32.0
  275 continue
      do 276 i=362,mesh
  276 ru(i)=ru(361)
      go to 285
c
  280 if (abs(twoz+ru(1)).le.0.001) go to 285
      eras=(-twoz-ru(mesh))/(ru(1)-ru(mesh))
      do 282 i=1,mesh
  282 ru(i)=ru(mesh)+eras*(ru(i)-ru(mesh))
  285 rum=-twoion
      if (ru(mesh).le.rum) go to 290
      eras=(rum-ru(1))/(ru(mesh)-ru(1))
      do 289 i=2,mesh
  289 ru(i)=ru(1)+eras*(ru(i)-ru(1))
c
c          construct r mesh
c
  290 i=1
      r(1)=0.0
      idb=1
      rdb=0.0
      deltar=0.0025*c
      drx=0.40/sqrt(emx)
      do 295 j=1,nblock
      do 294 k=1,40
      i=i+1
      r(i)=r(i-1)+deltar
  294 continue
      if (deltar.ge.drx) go to 295
      idb=i
      rdb=r(i)
      deltar=deltar+deltar
  295 continue
      r(1)=1.0e-10
      v(1)=-twoz/r(1)
c
c
  300 ihftrm=max0(1,nhftrm)
      if (ihf.eq.1) ihftrm=1
      write (nla,29) conf(1)
   29 format (a6)
      ndel=1
      do 305 k=1,6
      if (nla(k:k).eq.'*') ndel=0
  305 continue
      do 850 ii=1,ihftrm
      nfg=nf(ii)+ng(ii)
      nconft=nconft+1
      nconft2=nconft2+ndel
      nconf=0
      if (ndel.eq.1) nconf=nconft2
      ion=iz-noelec
      delta=2000.0
      delt=2000.0
      exf=exf10
      if (kut.eq.-1) exf=exfm1
  310 niter=0
      end=0.0
c
  330 if (tolend.lt.1.0e-04) write (9,30)
   30 format (1h1)
      write (9,31) conf,nconf,iz,ncores,nvales,ion,c,ca1
   31 format (3h   ,3a6,2x,10h    nconf=,i3,6h    z=,i3,11h    ncores=,
     1  i2,11h    nvales=,i2, 8h    ion=,i2, 14h    r(i)/x(i)=,f10.8,
     2  6x,4hca1=,f5.3)
      n1scfm1=n1scf-1
      n1sc=n1scf
      if (itpow.eq.2) go to 390
      write (9,32)tolstb,tolkm2,tolend,thresh,kut, exf,corrf,ca0,ihf1,
     1  mesh,idb,rdb,emx,r(mesh),irel,ib
   32 format (/8h tolstb=,f5.3,3x,7htolk-2=,f7.5,3x,7htolend=,e8.1,
     1  3x,7hthresh=,e8.1,3x,4hkut=,i2,4x,4hexf=,f5.3,9h   corrf=,f5.3,
     2  6x,4hca0=,f6.3//18h0rcn mod 36  ihf1=,i2,
     3  8x,5hmesh=,i4,3x,4hidb=,i4,3x,4hrdb=,f8.3,3x,4hemx=,f7.2,
     4  3x,8hr(mesh)=,f8.3,5x,5hirel=,i2,5x,3hib=,i4)
      if (ihf1.ne.2) go to 360
      if (nhftrm.gt.0) go to 345
      write (9,33) chxid
   33 format (1h0,a8,15h energy = e(av))
      go to 360
  345 write (9,34) t1hf(ii),t2hf(ii),(cfg(j,ii),fg(j,ii),kfg(j,ii),
     1  ifg(j,ii),jfg(j,ii), j=1,nfg)
   34 format (12h0energy = e(,a7,a3,9h) = e(av),4(3h + ,f9.6,1h*,a1,i1,
     1  1h(,a3,1h,,a3,1h))
     2  /31x,4(3h + ,f9.6,1h*,a1,i1,1h(,a3,1h,,a3,1h)))
  360 if (n1sc.gt.1) write (9,36)
   36 format (50x,45h calculate only outermost (continuum) orbital)
      if (n1scf.le.1) go to 370
      write (9,27) n1scfm1
   27 format ('0*****first',i3,' orbital(s) held fixed')
c
  370 write (9,37) (nlbcd(m),m=1,ncspvs)
   37 format (//31h it  time   delta  idel kut itp,1x,15(a3,3x)/
     1  34x,15(a3,3x))
      if (ifrac.eq.0) then
        write (9,38) (wwnl(m),m=1,ncspvs)
      else
        write (9,39) (wwnl(m),m=1,ncspvs)
      end if
  390 if (iw6.ge.0) go to 400
      write (6,37) (nlbcd(m),m=1,ncspvs)
      if (ifrac.eq.0) then
        write (6,38) (wwnl(m),m=1,ncspvs)
      else
        write (6,39) (wwnl(m),m=1,ncspvs)
      end if
   38 format (30x,15f6.0/32x,15f6.0)
   39 format (31x,15f6.3/33x,15f6.3)
c
c          start scf iteration
c
  400 niterp=0
      if (kut.eq.-1) niterp=1
  401 niterp=niterp+1
  402 niter=niter+1
      thresh=thres1
      if (alffix.ge.0.7) go to 406
c     if (delta/tolend.lt.100.0) go to 406
c     thresh=sqrt(thres1*delta)
c 406 if (npr.le.0) go to 409
  406 go to 409
c             This diagnostic removed 22 Aug 1999
c     write (9,40) niter, (ru(i), i=1,400,20), (ruee(i), i=1,400,20),
c    1  (ruexch(i), i=1,400,20)
c  40 format (1h1,i2,5x,14hru,ruee,ruexch/(10f12.6/10f12.6//))
  409 do 410 i=1,mesh
      rscore(i)=0.0
  410 rsvale(i)=0.0
      nod=0
      nof=0
c
c          calc v for kut greater than -1
c
  411 do 413 i=1,mesh
  413 rc(i)=rsatom(i)
      eras=-twozzz
      if (kut) 430,418,417
  417 eras=-twoion
  418 do 425 i=2,mesh
      a0=min(ru(i),eras)
      v(i)=a0/r(i)
  425 x2(i)=v(i)
c
c     solve schroedinger equation for each orbital in turn.
c            also, calculate core and valence charge densities.
c
  430 do 499 m=1,ncspvs
      if (end.gt.0.0.and.m.gt.ncspvs) go to 800
      kkk=nkkk(m)
      if (end.eq.0.0.and.n1sc.gt.1.and.m.lt.n1sc) go to 491
      e=ee(m)
      nn=nnn(m)
      lam=l(m)
      nodf=0
      if (lam-2) 433,431,432
  431 nod=nod+1
      nodf=nod
      go to 433
  432 if (lam.ne.3) go to 433
      nof=nof+1
      nodf=nof
  433 if (e.le.0.0) go to 434
      nn=99999
  434 if (kut.lt.0) go to 435
      if (end) 450,450,449
  435 if (m.gt.ncspvo) go to 450
      do 437 i=1,mesh
      if (e.le.0.0) go to 436
      rsatom(i)=0.0
      go to 437
  436 rsatom(i)=pnl(i,m)**2
  437 continue
c
c     calc xi(i)=coulomb energy of one electron in orbital m
c
      call quad2(1)
c
c          if (ihf1.gt.1) calc hf potential
      if (ihf1.gt.1) call hfpot(m)
      two=2.0
      a4=min(wwnl(m),two)
      j=jjj(m)
      do 447 i=2,mesh
      if (ihf1.gt.1.and.irel.eq.0) go to 446
      if (kut.eq.-2) go to 442
c          calc hx potential
      eras=rc(i)-a4*rsatom(i)
      a0=exfm1*eras/(eras+a0m(m))
      if (i.ge.j) go to 441
      a0=a0*(1.0+ca1*(r(j)-r(i))/r(j))
  441 continue
      a2=0.0
      if (rc(i).ne.0.0) a2=eras/rc(i)
      x1(i)=ru(i)-xi(i)+(a0*a2-exf10)*ruexch(i)
      go to 444
c          calc hs potential
  442 eras1=r(i)
      eras=a4*rsatom(i)
      eras=2.0*rsatom(i)
      call subcor
      x1(i)=ru(i)-xi(i)-exf10*b3
      if (wwnl(m).gt.1.0) go to 444
      eras=rc(i)+rsatom(i)
      call subcor
      x1(i)=x1(i)+exf10*(b3-ruexch(i))
  444 continue
      x2(i)=x1(i)/r(i)
      if (ihf1.le.1) xj(i)=x1(i)
  446 v(i)=xj(i)/r(i)
c          add correlation contribution to potential
      if (corrf.le.0.0) go to 447
      eras1=r(i)
      eras=rc(i)-rsatom(i)
      call subcor
      v(i)=v(i)+corrf*b2/eras1
  447 continue
      if (end.le.0.0) go to 450
      kkk=nkkk(m)
      if (ihf.eq.1.and.irel.eq.0.and.ncspvs.gt.1) go to 449
      if (iptvu.lt.5) go to 449
      eras=delp(m)/pnl(kkk-47,m)
      eras1=l(m)*(l(m)+1)
      do 448 i=1,400,20
  448 rsatom(i)=xj(i)+eras1/r(i)
      write (9,49) niter,nlbcd(m),rsint1,eras, (xi(i), i=1,400,20),
     1  (xj(i), i=1,400,20), (rsatom(i),i=1,400,20)
      if (iptvu.lt.6.or.m.lt.ncspvs) go to 449
      write (9,1570) conf,nlbcd(m),ee(m)
 1570 format (1h1,3a6,1h(,a3,10h function,,3x,2he=,f11.6,1h)///5x,1hi,
     1  10x,1hr,13x,3hr*v,10x,6hr*veff,12x,1hp,9x,10h(veff-e)*p,3x,
     2  14hd2p/dr2 (5 pt),1x,14hd2p/dr2 (3 pt),5x,6hveff-e/)
      ix=min0(nkkk(m),500)
      do 1571 i=2,ix
      rsatom(i)=xj(i)+eras1/r(i)
      a4=rsatom(i)/r(i)-ee(m)
      b0=a4*pnl(i,m)
      if (i.eq.2.or.mod(i,40).eq.0) go to 1569
      j=i+2
      jj=40*(i/40)+1
      a0=1.0/(r(j)-r(j-1))**2
      do 1568 i1=1,5
      pnlo(553-i1)=pnl(j,m)
      j=j-1
      if (j.lt.jj) j=j-1
 1568 continue
      b2=a0*(16.0*(pnlo(551)+pnlo(549))-30.0*pnlo(550)-pnlo(552)
     1  -pnlo(548))/12.0
      b1=a0*(pnlo(551)-2.0*pnlo(550)+pnlo(549))
      go to 1571
 1569 b2=0.0
      b1=4.0*a0*(pnl(i+1,m)-2.0*pnl(i,m)+pnl(i-1,m))
 1571 write (9,1572) i,r(i),xj(i),rsatom(i),pnl(i,m),b0,b2,b1,a4
 1572 format (i6,7f15.6,1p,e19.6)
  449 write (4) (v(i), i=1,mesh)
      go to 499
c
c     integrate differential equation to obtain radial wavefunction pnl
c
  450 mm=m
      if (m.lt.n1scf) go to 491
      call scheq
      jjj(m)=j
      if (m.lt.3) go to 451
      if (l(m).eq.l(m-1)) go to 452
      if (l(m).ne.l(m-2)) go to 451
      if (wwnl(m-1).eq.1.0) go to 452
  451 jjj(m)=1
c
  452 ee(m)=e
      ehf(m)=-e
      nkkk(m)=kkk
      kkk=nkkk(m)
      imat(m)=imatch
      if (niter.gt.10) go to 454
      nsch(niter,m)=nprint
c
c          stabilize pnl if e neg and niterp gr than 1
c
  454 eras=0.0
      eras=pnlo(kkk-47)-pnl(kkk-47,m)
      if (niterp.eq.1) go to 460
      if (e.gt.0.0) go to 459
      if (eras.eq.0.0) go to 461
      if (niterp-2) 460,470,480
  459 alfm(m)=1.0
      go to 461
  460 alfm(1)=alffix
      alfm(m)=0.0
  461 do 462 i=1,kkk
  462 pnl(i,m)=pnlo(i)
      go to 487
  470 alfm(m)=alffix
      go to 485
  480 if (delp(m).eq.0.0) go to 484
      b0=eras/delp(m)
      if (b0.gt.-0.20) go to 482
      alfm(m)=max(alfmin,alfm(m)*0.66667)
      go to 485
  482 if (b0.lt.0.20) go to 484
      alfm(m)=min(alfmax,alfm(m)*1.5)
  484 if (alfm(m).ge.1.0) go to 461
  485 a0=1.0-alfm(m)
      do 486 i=1,kkk
  486 xj(i)=alfm(m)*pnlo(i)+a0*pnl(i,m)
      if (ee(m).gt.0.0) go to 487
      call quad5(xj,2,1,kkk,pnorm)
      pnorm=1.0/sqrt(pnorm)
      do 1486 i=1,kkk
 1486 pnl(i,m)=pnorm*xj(i)
  487 if (kkk.ge.mesh) go to 490
      k1=kkk+1
      do 489 i=k1,mesh
  489 pnl(i,m)=0.0
  490 delp(m)=eras
c
  491 if (m.gt.ncores) go to 493
      if (ee(m).gt.0.0) go to 495
      do 492 i=1,kkk
  492 rscore(i)=rscore(i)+wwnl(m)*pnl(i,m)**2
      go to 495
  493 do 494 i=1,kkk
  494 rsvale(i)=rsvale(i)+wwnl(m)*pnl(i,m)**2
c 495 if (npr.le.0) go to 499
  495 go to 499
c              This diagnostic removed 22 Aug 1999
c     do 497 i=1,400
c 497 xj(i)=v(i)*r(i)
c     write (9,49) niter,nlbcd(m),rsint1,e,(xi(i),i=1,400,20), (xj(i),
c    1  i=1,400,20), (pnlo(i), i=1,400,20),(pnl(i,m),i=1,400,20)
   49 format (/1h0,i2,3x,a3,f15.7,f15.6,3x,8hr*vself,,2x,9hr*vtotal,,2x,
     1  7hr*veff,,3x,59h(if diagnostic print out-r*vs, r*vt, calc. wvfn,
     2 mod. wvfn)  / (10f12.6/10f12.6 // ))
  499 continue
      ncspvo=ncspvs
      if (end.gt.0.0) go to 800
c
c     calc total charge density and exchange and correlation potentials
c
  500 do 509 i=1,mesh
      if (ee(ncspvs).le.0.0) go to 502
      rsatom(i)=rscore(i)
      go to 503
  502 rsatom(i)=rscore(i)+rsvale(i)
  503 eras1=r(i)
      eras=rsatom(i)
      call subcor
      rc(i)=b0
      recorr(i)=b1
      rucorr(i)=b2
      ruexch(i)=b3
  509 continue
      rc(1)=rc(2)+rc(2)-rc(3)
c
c       calc ruee=r*(electron-electron energy), xj=r*(total energy)
c
      call quad2(2)
c
c
c          calculate delta
c
  530 delta=0.0
      delto=delt
      idelto=idelta
      do 540 i=2,mesh
      a0=xj(i)-ru(i)
      eras=abs(a0)
      if (eras.lt.delta) go to 540
  535 delta=eras
      delt=a0
      idelta=i
  540 continue
      deltn(niter)=delt
      if (ion.gt.1) go to 545
      if (iz.lt.55.or.iz.gt.102) go to 545
      if (iz.gt.70.and.iz.lt.87) go to 545
      if (niter.gt.2) then
           temp=delt/delto
           if (temp.lt.-0.3) alfmax=0.8*alfmax
      end if
  541 if (niter.lt.8) go to 543
      do 542 n=niter-2,niter-1
  542 if (deltn(n)*deltn(niter).lt.0.0) go to 543
      if (abs(deltn(niter)).gt.0.3*abs(deltn(niter-1)))
     1  alfmax=min(alfmax0,1.2*alfmax)
  543 alfmin=0.2*alfmax
      go to 548
  545 if(niterp.gt.2.and.abs(delt).gt.abs(delto).and.alfmax.gt.0.5) then
           half=0.5
           alfmax=max(0.8*alfmax,half)
           alfmin=min(0.2*alfmax,alfmin)
      end if
  548 call seconds(t2)
      time=t2-t1
      if (iw6.lt.0) write (6,55) niter,time,delt,idelta,kut,niterp,
     1  (alfm(m), m=1,ncspvs)
c            New npr diagnostic, 22 Aug 1999
      if (npr.ne.0.and.niter.ge.npr) call power
  550 if (itpow.ne.1.and.itpow.lt.3) go to 600
      write (9,55) niter,time,delt,idelta,kut,niterp,
     1  (alfm(m), m=1,ncspvs)
   55 format (1x,i2,f6.2,f10.6,i4,i3,i4,1x,15f6.3/f39.3,15f6.3)
c
c          calculate next trial potential.
c             (stabilize ru if niterp=1)
c
  600 a1=1.0
      if (niterp.gt.1) go to 650
      eras=delt/delto
      if (eras.gt.-0.3) go to 603
      alffix=alffix*0.8
      go to 610
  603 if (eras.lt.0.3) go to 610
      if (iabs(idelta-idelto).gt.50) go to 610
      alffix=alffix*1.25
  610 a1=alffix
  650 a0=1.0-a1
      do 660 i=1,mesh
  660 ru(i)=a1*xj(i)+a0*ru(i)
c
c     test self-consistency of atomic potential,
c          and (if thresh=thres1) of the wavefunctions.
c
  700 if (niter.le.1) go to 402
      if (delta.ge.tolend) go to 710
      if (thresh.gt.thres1) go to 710
      if (ihf.eq.1.and.ncspvs.gt.1) go to 780
      if (tolkm2.ge.0.1.and.kut.gt.-1) go to 710
      do 708 m=n1sc,ncspvs
      kkk=nkkk(m)
      if (abs(delp(m)/pnl(kkk-47,m)).gt.5.0e-06) go to 710
  708 continue
      if (ee(ncspvs).gt.0.0.and.abs(delp(ncspvs)).gt.1.0e-7) go to 710
      go to 780
  710 if (niter.lt.maxit) go to 730
      if (maxit.eq.maxit0) go to 725
      write (9,71) maxit0,(i,deltn(i),i=niter-2,niter),tolend
   71 format (//' ***************************************************'/
     1          ' *     POOR SCF CONVERGENCE:                       *'/
     2   ' *      maxit0=',i3,'   deltn(',i3,')=',f11.8,'        *'/
     3   ' *                   deltn(',i3,')=',f11.8,'        *'/
     4   ' *                   deltn(',i3,')=',f11.8,'        *'/
     5   ' *                       tolend=',f11.8,'        *'/
     6          ' ***************************************************'/)
      if (iw6.lt.0) write (6,71) maxit0,(i,deltn(i),i=niter-2,niter),
     1  tolend
      go to 780
  725 if (delta.le.10.0*tolend) go to 780
      maxit=maxit+10
      if (delta.lt.100.0*tolend) maxit=maxit+20
  730 if (kut.lt.0) go to 740
      if (kut.ge.0.and.tolkm2.gt.tolend.and.delta.lt.tolkm2) go to 735
      if (delta.lt.tolend) go to 740
      if (delta.ge.tolkm2) go to 740
  735 kut=-1
      exf=exfm1
      if (kutd.ne.-1) go to 400
      kut=-2
      exf=exf10
      go to 400
  740 if (niterp.ge.2) go to 401
      if (delta-tolstb) 401,402,402
c
  780 end=1.0
c     if (npr.eq.9999) npr=npr0
      write (4) ru,ruee,ruexch,rsatom,recorr,rucorr
      if (ihf.eq.0) go to 411
      if (tolend.gt.1.0e-4) go to 800
      if (ihf.eq.1.and.irel.ne.0.and.ncspvs.gt.1) go to 800
      go to 411
c
  800 if (itpow.ne.2) go to 810
      write (9,32)tolstb,tolkm2,tolend,thresh,kut, exf,corrf,ca0,ihf1,
     1  mesh,idb,rdb,emx,r(mesh),irel,ib
      if (ihf1.ne.2) go to 806
      if (nhftrm.gt.0) go to 803
      write (9,33) chxid
      go to 806
  803 write (9,34) t1hf(ii),t2hf(ii),(cfg(j,ii),fg(j,ii),kfg(j,ii),
     1  ifg(j,ii),jfg(j,ii), j=1,nfg)
  806 if (n1sc.gt.1) write (9,36)
      if (n1scf.le.1) go to 810
      write (9,27) n1scfm1
c
  810 if (ihf.eq.0) call clockrcn
      call outpt
      if (nnn(ncspvs).gt.98) call phshift
      if (ihf.ne.0.or.nhftrm.eq.0) go to 840
      if (ii.eq.1) write (7,55)
      do 820 i=2,npar
  820 vprm(i)=vprm(i)/109.73731
      write (7,82) conf,t1hf(ii),t2hf(ii),(vprm(i),kpar(i),i=1,npar)
   82 format (2a6,a2,a7,a3,f12.4,i1,8(f9.5,i1)/28x,9(f9.5,i1))
c
  840 if (ivinti.gt.0) call vinti(ivinti)
      call clockrcn
      if (iw6.ge.0) go to 845
      tr=t2-t1
      tj=t2-t0
      write (6,85) conf,tr,tj
   85 format (' finished  ',2a6,a2,'  at   t(run)=',f6.2,',',5x,
     1  't(job)=',f7.2,' seconds')
  845 t1=t2
  850 continue
c
  900 if (tolkm2.ne.tolend.and.kut.gt.-1) go to 210
  991 kut=kut-1
      if (kut.eq.kutd) go to 991
      if (kut.lt.-2) go to 210
      go to 300
c
      end
