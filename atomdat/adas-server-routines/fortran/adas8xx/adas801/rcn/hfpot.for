C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/hfpot.for,v 1.3 2010/04/09 16:45:16 mog Exp $ Date $Date: 2010/04/09 16:45:16 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.2
C  DATE    : 09-04-2010
C  MODIFIED: Martin O'Mullane
C             - Literal constants in quotes, to be assigned to an
C               integer variable, are not allowed in data statements
C               (IBM allowed extension to f66) so replace with 
C               Hollerith constants (f77 standard).
C
C-----------------------------------------------------------------------
      subroutine hfpot(m)
c
c          calculate hartree-fock potential terms over and above
c               the classical potential
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
      common/lc2/ra(kmsh),rb(kmsh)
c
      integer fg
      data jhlf,jhlg/1hf,1hg/
      data jhuF,jhuG/1hF,1hG/
c
      kkk1=min0(nkkk(m)+40,mesh)
      kkk=kkk1
      x1(1)=0.0
      x2(1)=0.0
      do 110 i=1,mesh
      ra(i)=0.0
  110 rb(i)=0.0
      if (niter.eq.1.and.ee(m).gt.0.0) go to 455
      if ((wwnl(m).lt.2.0.and.nhftrm.eq.0).or.l(m).eq.0) go to 300
c          calc direct terms
      fl=l(m)
      c1=(wwnl(m)-1.0)*(4.0*fl+2.0)/(4.0*fl+1.0)
      do 149 m1=1,ncspvs
      if (ee(m1).gt.0.0) go to 149
      kx=2*l(m1)
      if (kx.lt.2) go to 149
      full=2*kx+2
      kkk=nkkk(m1)
      do 120 i=1,kkk
  120 xj(i)=pnl(i,m1)**2
      do 140 k=2,kx,2
      fk=k
      c2=0.0
      if (nhftrm.eq.0.or.wwnl(m1).eq.full) go to 125
      if (nfg.le.0) go to 125
c               ls-dependent terms
      do 123 j=1,nfg
      if ((fg(j,ii).ne.jhlf.and.fg(j,ii).ne.jhuF).or.kfg(j,ii).ne.k)
     1  go to 123
      if ((ifg(j,ii).eq.nlbcd(m1).and.jfg(j,ii).eq.nlbcd(m)).or
     1   .(jfg(j,ii).eq.nlbcd(m1).and.ifg(j,ii).eq.nlbcd(m)))
     2    c2=-2.0*cfg(j,ii)/wwnl(m)
  123 continue
c
  125 if (m1.ne.m) go to 128
      c2=2.0*c2
      c2=c2+c1*s3j0sq(fl,fk,fl)
  128 continue
      if (c2.eq.0.0) go to 140
      call quadk(m,1)
      do 130 i=3,kkk,2
      eras=r(i)**k
      eras=x1(i)/eras+eras*r(i)*(x2(kkk)-x2(i))
  130 ra(i)=ra(i)+c2*eras
  140 continue
  149 continue
      kkk=kkk1
      do 150 i=3,kkk,4
      b0=(ra(i)-ra(i-2)+ra(i)-ra(i+2))*0.125
      ra(i-1)=0.5*(ra(i)+ra(i-2))+b0
  150 ra(i+1)=0.5*(ra(i)+ra(i+2))+b0
      if (ra(5).eq.0.0.or.ra(9).eq.0.0) go to 300
      ra(2)=(ra(3)**2)/ra(5)
      ra(4)=ra(5)*ra(7)/ra(9)
c          calc exchange terms
  300 if (ncspvs.eq.1) go to 455
      fl2=l(m)
      do 400 m1=1,ncspvs
      if (m1.eq.m) go to 400
      if (ee(m1).gt.0.0) go to 400
      full=4*l(m1)+2
      kkk=min0(nkkk(m1),nkkk(m))
      fl1=l(m1)
      do 320 i=1,kkk
  320 xj(i)=pnl(i,m1)*pnl(i,m)
      kn=iabs(l(m1)-l(m))
      kx=l(m1)+l(m)
      do 340 k=kn,kx,2
      fk=k
      c2=0.0
      if (nhftrm.eq.0.or.wwnl(m1).eq.full) go to 325
      if (nfg.le.0) go to 325
c               ls-dependent terms
      do 323 j=1,nfg
      if ((fg(j,ii).ne.jhlg.and.fg(j,ii).ne.jhuG).or.kfg(j,ii).ne.k) 
     1  go to 323
      if ((ifg(j,ii).eq.nlbcd(m1).and.jfg(j,ii).eq.nlbcd(m)).or
     1   .(jfg(j,ii).eq.nlbcd(m1).and.ifg(j,ii).eq.nlbcd(m)))
     2    c2=-2.0*cfg(j,ii)/wwnl(m)
  323 continue
c
  325 c2=c2+wwnl(m1)*s3j0sq(fl1,fk,fl2)
      call quadk(m,1)
      do 330 i=3,kkk,2
      eras=r(i)**k
      eras=x1(i)/eras+eras*r(i)*(x2(kkk)-x2(i))
  330 rb(i)=rb(i)+c2*eras*pnl(i,m1)
  340 continue
  400 continue
      kkk=kkk1
      do 450 i=3,kkk,4
      b0=(rb(i)-rb(i-2)+rb(i)-rb(i+2))*0.125
      rb(i-1)=0.5*(rb(i)+rb(i-2))+b0
  450 rb(i+1)=0.5*(rb(i)+rb(i+2))+b0
      if (rb(5).eq.0.0.or.rb(9).eq.0.0) go to 455
      rb(2)=(rb(3)**2)/rb(5)
      rb(4)=rb(5)*rb(7)/rb(9)
c
  455 frac=0.01
      pmax=0.0
      do 458 i=2,kkk
      eras=pnl(i,m)
      pmax=max(pmax,abs(frac*eras))
      if (eras.ge.0.0.and.eras.lt.pmax) eras=pmax
      if (eras.lt.0.0.and.eras.gt.-pmax) eras=-pmax
  458 rb(i)=rb(i)/eras
      idel=1
      idel2=idel*2
      km10=kkk-10
      do 465 i=10,km10
      if (pnl(i,m)*pnl(i+1,m).gt.0.0) go to 465
      eras=(rb(i+1+idel)-rb(i-idel))/(r(i+1+idel)-r(i-idel))
      do 462 j=1,idel2
  462 rb(i-idel+j)=rb(i-idel)+eras*(r(i-idel+j)-r(i-idel))
  465 continue
      do 468 i=2,kkk
  468 xj(i)=ru(i)-xi(i)-exf10*ruexch(i)-ra(i)-rb(i)
      if (kkk.ge.mesh) go to 480
      do 470 i=kkk,mesh
  470 xj(i)=xj(kkk)
  480 continue
      if (end.gt.0.0.and.iptvu.ge.5) write (9,46) m,k,kkk,
     1  (ra(i),i=2,kkk),(rb(i),i=2,kkk),(xj(i),i=2,kkk)
   46 format (1h0/3i10/(10f12.4))
      return
      end
