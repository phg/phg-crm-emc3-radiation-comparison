C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/vinti.for,v 1.2 2004/07/06 15:28:03 whitefor Exp $ Date $Date: 2004/07/06 15:28:03 $
C
      subroutine vinti(ivinti)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
      common/lc2/ra(kmsh),rb(kmsh)
c
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
      integer fg
c
      if (ncspvs.eq.1) return
      write (9,10) conf
   10 format ('1   vinti integrals for  ',3a6////
     1  10x,'vinti integral',23x,'j**2',17x,'wt*(j**2)'/)
      sum=0.0
c
      do 390 n=1,ncspvs
      fl=l(n)
      lm1=l(n)-1
      flm1=lm1
      do 380 np=1,ncspvs
      if (l(np).ne.lm1) go to 380
      kmax=min0(nkkk(n),nkkk(np))
      kmxm2=kmax-2
      h12=1.0/(12.0*r(2))
c
      do 290 i=1,kmxm2
      if (i.ge.kmax-1) go to 130
      if (i.ge.idb+2) go to 150
      i1=i-40*((i-2)/40)
      if (i1.gt.2) go to 120
      if (i1.eq.2) go to 110
      der=-25.0*pnl(i,np)+48.0*pnl(i+1,np)-36.0*pnl(i+2,np)
     1  +16.0*pnl(i+3,np)-3.0*pnl(i+4,np)
      go to 200
  110 der=-3.0*pnl(i-1,np)-10.0*pnl(i,np)+18.0*pnl(i+1,np)
     1  -6.0*pnl(i+2,np)+pnl(i+3,np)
      go to 200
  120 if (i1.lt.40) go to 150
      if (i1.eq.41) go to 130
      der=-pnl(i-3,np)+6.0*pnl(i-2,np)-18.0*pnl(i-1,np)
     1  +10.0*pnl(i,np)+3.0*pnl(i+1,np)
      go to 200
  130 der=3.0*pnl(i-4,np)-16.0*pnl(i-3,np)+36.0*pnl(i-2,np)
     1  -48.0*pnl(i-1,np)+25.0*pnl(i,np)
      go to 200
  150 der=pnl(i-2,np)-8.0*pnl(i-1,np)+8.0*pnl(i+1,np)-pnl(i+2,np)
  200 der=h12*der
      xi(i)=pnl(i,n)*(der-fl*pnl(i,np)/r(i))
      if (ivinti.lt.2) go to 280
      if (n.eq.3.and.np.eq.2) write (9,9920) i,r(i),pnl(i,np),
     1  der,i1,h12,pnl(i,n),xi(i)
 9920 format (i5,3f14.8,i5,3f14.8)
  280 if (i1.eq.41) h12=0.5*h12
  290 continue
      xi(kmax-1)=xi(kmxm2)
      xi(kmax)=xi(kmxm2)
c
      call quad5(xi,1,1,kmax,fj)
      fjsq=fj**2
      aa=(4.0*fl+2.0)*(4.0*flm1+2.0)
      wt=2.0*fl*wwnl(n)*wwnl(np)/aa
      bb=wt*fjsq
      sum=sum+bb
      write (9,30) nlbcd(n),nlbcd(np),fj,fjsq,bb
   30 format (5x,'j(',a3,',',a3,')=',f14.8,8x,f15.8,8x,f15.8)
  380 continue
  390 continue
c
      write (9,40) sum
   40 format (/10x,'total k-factor=',f16.8)
      return
      end
