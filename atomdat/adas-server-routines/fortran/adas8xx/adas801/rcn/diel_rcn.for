C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/diel_rcn.for,v 1.2 2004/07/06 13:32:11 whitefor Exp $ Date $Date: 2004/07/06 13:32:11 $
C
      subroutine diel_rcn
c
c     subroutine to construct continuum configuration cards for
c          rcn/rcg autoionization-problem runs
c
c          to use this option, make tape10=in36 an empty file, 
c               or a file containing a single record with "-1" in columns 4-5;
c
c          input (in file called tape12=indiel) consists of:
c     (1) rcn36 control card (with appropriate value of emx, or zero)
c     (2) ion config. card(s) to which a continuum electron is to be added
c          (1 to 5 in number)
c     (3) first-parity (radiative-decay) configurations
c     (4) second-parity (doubly excited) configuration(s)
c          (1 to 10 in number)
c     (5) card with -1 in columns 4-5 (rcn exit card)
c          further sets of cards (2)-(5) may be added as desired
c
c          action of subroutine:
c     (a) first portion of program writes control card (1) on
c          tape10 (input file for preliminary rcn run) and on
c          tape11=in34 (input file for final rcn/rcg run)
c     (b) statements 100-300 copy cards (2) and (4)-(5) on tape 10
c     (c) rcn is called to provide data for calculation of
c          kinetic energy of free electron
c     (d) statements 410-799 copy cards (3)-(4) onto tape11 and
c          add continuum configuration cards
c     (e) parts (b)-(d) are repeated for each additional
c          set of input data cards (2)-(5)
c     (f) statement 800 adds the rcn exit card to tape11
c
c          final tape11 (copied to tape10) is ready to be used directly as
c            input file for final rcn/rcn2/rcg run, except for the following
c          (i) it may be necessary to revise the value of emx on the
c            control card if it was not adequately estimated on input card (1)
c          (ii) if the parities of cards (3)--or the parities of cards (4)--
c               were not the same in contiguous sets but the values of z
c               were the same, then it is necessary to break tape11
c               into two pieces, one for each parity, because of the requirements
c               of rcn2
c               (this can be done automatically by including at the appropriate
c               place on tape12 a single ion card of different z,
c               plus an rcn exit card (5). )
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      character*8 ia(10),ib(3),inpcard*70,iba,icard*80,form*42,hibo*5
      character*2 ioccch,iwwnl(26),iochion(8,10)
      character*1 lbcd(21),lh,lhion(8,10)
      common/ch/ia,ib,inpcard,iba
      common/d0/confg(3),vpar(45)
      dimension nion(8,10),
     1  lion(8,10),ioccion(8,10),mionk(10),eavion(10),alf(10)
      common/d3/natom(8,10),latom(8,10),ioccatm(8,10),matmk(10),la(2)
c
      data lbcd/'s','p','d','f','g','h','i','k','l','m',
     1          'n','o','q','r','t','u','v','w','x','y','z'/
      data (iwwnl(m),m=1,26) /'0 ','1 ','2 ','3 ','4 ','5 ','6 ','7 ',
     1         '8 ','9 ','10','11','12','13','14','00','01','02','03',
     2         '04','05','06','07','08','09','  '/
      data alf/1ha,1hb,1hc,1hd,1he,1hf,1hg,1hh,1hi,1hj/
      data fff1,fff2,fff3/5hf6.4,,5hf6.2,,5hf6.1,/
c
      rewind 11
      rewind 10
c          read rcn control card
      read (12,10) ia
   10 format (10a8)
      write (11,10) ia
      write (10,10) ia
      write (9,11) ia
   11 format (//1h ,10a8)
c          read ion configuration card(s)
  100 rewind 10
      read (10,10) ia
      ird=0
      iparty0=-7
      i=0
  150 i=i+1
      read (12,22,end=800) iz,ispect,inpcard
      ird=ird+1
      if (i.eq.1) isp1=ispect
      if (i.eq.1) izo=iz
      if (i.gt.2.or.iz.gt.0) go to 160
      ispect=izo-1
      hibo='dummy'
      write (11,22) izo,ispect,hibo
      go to 100
  160 if (isp1.ne.ispect) go to 230
      write (10,22) iz,ispect,inpcard
      write (9,30) iz,ispect,inpcard
      go to 150
c          select second-parity configuration cards
  220 read (12,22) iz,ispect,inpcard
   22 format (2i5,a70)
      ird=ird+1
  230 if (iz.le.0) go to 300
  240 iparity=0
      call analyz1
      do 280 i=1,8
c     decode (5,24,ia(i)) n,lh,ioccch
      read (ia(i),24) n,lh,ioccch
   24 format (i2,a1,a2)
      if (n.le.0) go to 290
      do 245 j=1,21
      if (lh.eq.lbcd(j)) go to 250
  245 continue
  250 l=j-1
      do 255 m=1,26
      if (ioccch.eq.iwwnl(m)) go to 260
  255 continue
      m=1
  260 iocc=m-1
      if (m.gt.15) iocc=m-16
      if (m.eq.26) iocc=1
      iparity=iparity+l*iocc
  280 continue
  290 iparity=mod(iparity,2)
      if (iparty0.eq.-7) iparty0=iparity
      if (iparity.eq.iparty0) go to 220
c
  300 if (iz.lt.0) iz=-999
      write (10,22) iz,ispect,inpcard
      write (9,30) iz,ispect,inpcard
   30 format (1h ,2i5,a70)
      if (iz.gt.0) go to 220
c
      rewind 10
c          run rcn to calculate free-electron energies
      call rcn
      rewind 2
c
      do 410 i=1,ird
  410 backspace 12
c          read ion configuration(s)
      k=0
  420 k=k+1
      read (12,52) iz,ispion,inpcard
      if (k.eq.1) isp=ispion
      if (ispion.eq.isp) go to 430
      kion=k-1
      backspace 12
      go to 500
  430 iparity=0
      call analyz1
      do 480 i=1,8
c     decode (5,54,ia(i)) nion(i,k),lhion(i,k),iochion(i,k)
      read (ia(i),54) nion(i,k),lhion(i,k),iochion(i,k)
      if (nion(i,k).gt.0) mionk(k)=i
      do 435 j=1,21
      if (lhion(i,k).eq.lbcd(j)) go to 450
  435 continue
      j=1
  450 lion(i,k)=j-1
      do 455 m=1,26
      if (iochion(i,k).eq.iwwnl(m)) go to 460
  455 continue
      m=1
  460 iocc=m-1
      if (m.gt.15) iocc=m-16
      if (m.eq.26) iocc=1
      if (nion(i,k).eq.0) iocc=0
      iparity=iparity+lion(i,k)*iocc
      ioccion(i,k)=iocc
  480 continue
      i=1
  490 read (2) 
      eavion(k)=vpar(1)
      go to 420
c          copy bound-state configurations
  500 iparty0=-7
      k=1
  520 read (12,52) iz,ispect,inpcard
   52 format (2i5,a70)
      if (iz.le.0) go to 600
      write (11,52) iz,ispect,inpcard
      write (9,52) iz,ispect,inpcard
  540 iparity=0
      call analyz1
      do 580 i=1,8
c     decode (5,54,ia(i)) n,lh,ioccch
      read (ia(i),54) n,lh,ioccch
   54 format (i2,a1,a2)
      if (n.le.0) go to 590
      natom(i,k)=n
      do 545 j=1,21
      if (lh.eq.lbcd(j)) go to 550
  545 continue
      j=1
  550 latom(i,k)=j-1
      do 555 m=1,26
      if (ioccch.eq.iwwnl(m)) go to 560
  555 continue
      m=1
  560 iocc=m-1
      if (m.gt.15) iocc=m-16
      if (m.eq.26) iocc=1
      iparity=iparity+latom(i,k)*iocc
      ioccatm(i,k)=iocc
  580 continue
  590 iparity=mod(iparity,2)
      if (iparty0.eq.-7) iparty0=iparity
      katm=k
      matmk(k)=i-1
      iza=iz
      ispa=ispect
      iba=ib(1)
      if (iparity.ne.iparty0) k=k+1
      go to 520
c          set up continuum cards
  600 do 799 ka=1,katm
      matom=matmk(ka)
      read (2) 
      do 798 k=1,kion
      mion=mionk(k)
      eps=vpar(1)-eavion(k)
      if (eps.gt.0.0) go to 605
      write (9,60) ka,k,eps
   60 format (/' for atom config.',i3,' and ion config.',
     1  i3,' eps=',e14.4)
      go to 798
  605 ji=0
      do 649 mi=1,mion
      do 609 ma=1,matom
      if (nion(mi,k).ne.natom(ma,ka).or.lion(mi,k).ne.latom(ma,ka))
     1  go to 609
      if (ioccion(mi,k).le.ioccatm(ma,ka)) go to 649
  609 continue
  610 ji=ji+1
      if (ji.eq.1) go to 620
      j=1
      ma=8
      write (9,61) j,mi,ma
   61 format (11h0*****error,i1,2i5/)
      go to 798
  620 li1=lion(mi,k)
  649 continue
      ja=0
      do 699 ma=1,matom
      iocca=ioccatm(ma,ka)
      do 659 mi=1,mion
      if (nion(mi,k).ne.natom(ma,ka).or.lion(mi,k).ne.latom(ma,ka))
     1  go to 659
      if (ioccion(mi,k).ge.iocca) go to 699
      go to 680
  659 continue
      mi=8
  680 ja=ja+1
      if (ja.le.2) go to 685
      j=2
      mi=8
      write (9,61) j,mi,ma
      go to 798
  685 la(ja)=latom(ma,ka)
      iocca=iocca-1
      write (9,9685) ma,mi,k,ja,la(ja),iocca,ioccion(mi,k)
 9685 format (10i5)
      if (iocca.gt.ioccion(mi,k)) go to 680
  699 continue
c
  700 k1=iabs(la(1)-la(2))
      k2=la(1)+la(2)
      lcontx=li1+k2
      lcontn=mod(lcontx,2)
  702 if (iabs(li1-lcontn).le.k2.and.li1+lcontn.ge.k1) go to 703
      lcontn=lcontn+2
      go to 702
  703 continue
      write (9,9705) k,la,li1,k1,k2,lcontn,lcontx
 9705 format (10i5)
      fff=fff2
      if (eps.gt.999.0) fff=fff3
      if (eps.lt.9.99) fff=fff1
      mp1=mion+1
c     encode (42,71,form) fff,mp1,fff
      write (form,71) fff,mp1,fff
   71 format (11h(2i5,a5,a1,,a5,6ha1,9x,,i1,11h(i2,a1,a2),,3h3x,,a4,1h))
      kk=99
      if (lcontx.gt.9) lcontx=9
      if (lcontx.lt.lcontn) go to 798
      do 720 l=lcontn,lcontx,2
      lh=lbcd(l+1)
      write (11,form) iza,ispa,iba,alf(k),eps,lh, (nion(i,k),
     1  lhion(i,k),iochion(i,k), i=1,mion), kk,lh,iwwnl(26),eps
  720 write (9,form) iza,ispa,iba,alf(k),eps,lh, (nion(i,k),
     1  lhion(i,k),iochion(i,k), i=1,mion), kk,lh,iwwnl(26),eps
  798 continue
  799 continue
      go to 100
c
  800 write (11,80)
   80 format (3x,2h-1,10x)
      rewind 11
      rewind 10
  850 read (11,81,end=900) icard
   81 format (a)
      write (10,82) icard
   82 format (a80)
      go to 850
  900 rewind 10
      rewind 9
      return
      end
