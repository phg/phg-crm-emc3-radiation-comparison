C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/quadk.for,v 1.2 2004/07/06 14:39:40 whitefor Exp $ Date $Date: 2004/07/06 14:39:40 $
C
      subroutine quadk(m,kk)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/lc2/ra(kmsh),rb(kmsh)
c
c       calculate  const*yk function   (integrate by simpsons rule)
c
  510 do 511 i=2,kkk
      eras=r(i)**k
      x1(i)=eras*xj(i)
  511 x2(i)=xj(i)/(eras*(r(i)**kk))
  512 x1(1)=0.0
      x2(1)=2.0*x2(2)-x2(3)
      if (abs(x2(1)).lt.0.2*abs(x2(2))) x2(1)=0.0
c
      ho3=r(2)/3.0
      i=1
      b12=x1(1)
      b22=x2(1)
      x2(1)=0.0
      do 515 j=1,nblock
      do 513 k1=1,20
      i=i+2
      if (i.gt.kkk) go to 520
      b10=b12
      b12=x1(i)
      x1(i)=x1(i-2)+ho3*(b10+4.0*x1(i-1)+b12)
      b20=b22
      b22=x2(i)
  513 x2(i)=x2(i-2)+ho3*(b20+4.0*x2(i-1)+b22)
      if (i.gt.idb) go to 515
      ho3=ho3+ho3
  515 continue
  520 kkk1=kkk
      if (m.le.25) kkk=min0(nkkk(m)+40,mesh)
      if (m.gt.25) kkk=min0(kkk+40,mesh)
      do 527 i=kkk1,kkk,2
      x1(i)=x1(kkk1)
  527 x2(i)=x2(kkk1)
      kkk=kkk1
  530 n=2
      if (m.lt.31) go to 600
c
      do 550 i=3,kkk,4
      b0=(x1(i)-x1(i-2)+x1(i)-x1(i+2))*0.125
      x1(i-1)=0.5*(x1(i)+x1(i-2))+b0
      x1(i+1)=0.5*(x1(i)+x1(i+2))+b0
      b0=(x2(i)-x2(i-2)+x2(i)-x2(i+2))*0.125
      x2(i-1)=0.5*(x2(i)+x2(i-2))+b0
  550 x2(i+1)=0.5*(x2(i)+x2(i+2))+b0
      n=1
  600 if (end.gt.0.0.and.iptvu.ge.7) write (9,60) m,k,kkk1,kkk,
     1  (xj(i),i=1,kkk),(x1(i),i=1,kkk,n),(x2(i),i=1,kkk,n)
   60 format (1h0/4i10/(10f12.4))
      return
      end
