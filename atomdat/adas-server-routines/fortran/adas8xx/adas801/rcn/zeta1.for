C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/zeta1.for,v 1.1 2004/07/06 15:41:26 whitefor Exp $ Date $Date: 2004/07/06 15:41:26 $
C
      subroutine zeta1
c
c          calculate spin-orbit parameters (via both central-field
c               formula and blume-watson method)
c                     and relativistic energy corrections
c
      implicit real*8 (a-h,o-z)
      parameter (kmsh=1801,ko=20,kmshq=1801,koq=20)
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/c7/t0,t1,t2
      common/c8/nhftrm,nfg,etotrl,ehf(ko),t1hf(9),t2hf(9),eterm,
     1  nf(9),ng(9),cfg(10,9),fg(10,9),kfg(10,9),rhfmtc(ko),ii
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
      dimension rsq(kmsh),zeta(ko,3)
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      integer fg
      dimension fi(kmsh,5),d(kmsh,3),drudr(kmsh),p(kmsh)
c     equivalence (rscore,fi),(pnlo,d),(ruee,p)
c
  100 npar=1
      nzi=0
      nfij=0
      ngij=0
      do 101 i=1,45
      vprm(i)=0.0
  101 kpar(i)=0
c
      if (ihf.eq.1) go to 360
  350 write (9,35)
   35 format(///1h ,11x,'--------------------zeta---------------------'
     1/'  nl   wnl ',2x,21h ----blume-watson----,5x,16h-----(r*vi)-----,
     2  5x,7hi (ryd),6x,5heps s,7x,4hevel,7x,24hedar   evel+edar    erel
     3  /16x,6h (ryd),6x,6h(cm-1),5x,5h(ryd) ,6x,6h(cm-1))
  360 do 380 i=1,mesh
      rsq(i)=r(i)*r(i)
      d(i,1)=-z-z+ruee(i)
  380 d(i,2)=ru(i)
c
c           calculate evel, edar, and zeta for each orbital
c
  400 etotrl=0.0
      do 899 m=1,ncspvs
      if (ee(m)) 405,405,401
  401 evel(m)=0.0
      edar(m)=0.0
      zeta(m,1)=0.0
      zeta(m,2)=0.0
      zeta(m,3)=0.0
      go to 700
c
c          set up potentials for individual orbitals and total atom
c
  405 read (4)(v(i), i=1,mesh)
      do 406 i=1,mesh
  406 d(i,3)=v(i)*r(i)
c
c          calculate derivatives of potentials
c
      n1=3
      nblock=mesh/40
      do 460 n=n1,3
      dr=3.0*r(2)
      i=1
      do 420 m123=1,nblock
      do 410 j=1,10
      i=i+4
      b0=d(i-3,n)-d(i-2,n)-d(i-2,n)+d(i-1,n)
      b1=d(i-2,n)-d(i-1,n)-d(i-1,n)+d(i,n)
      b2=d(i-3,n)-d(i,n)
      drudr(i-3)=(-4.5*b0-b2)/dr
      drudr(i-2)=(-1.5*b1-b2)/dr
      drudr(i-1)=(1.5*b0-b2)/dr
  410 drudr(i)=(4.5*b1-b2)/dr
      if (i.gt.idb) go to 420
      dr=dr+dr
  420 continue
      do 450 i=2,mesh
      v(i)=d(i,n)/r(i)
  450 d(i,n)=(drudr(i)-v(i))/rsq(i)
  460 continue
c
c          calc p=dpnl/dr and all integrands
c
      dr=3.0*r(2)
      kkk=nkkk(m)
      nblokk=kkk/40
      i=1
      p(1)=0.0
      do 515 nb=1,nblokk
      do 514 j=1,10
      i=i+4
      b0=pnl(i-3,m)-2.0*pnl(i-2,m)+pnl(i-1,m)
      b1=pnl(i-2,m)-2.0*pnl(i-1,m)+pnl(i,m)
      b2=pnl(i-3,m)-pnl(i,m)
      p(i-3)=(-4.5*b0-b2)/dr
      p(i-2)=(-1.5*b1-b2)/dr
      p(i-1)=(1.5*b0-b2)/dr
  514 p(i)=(4.5*b1-b2)/dr
      if (i.gt.idb) go to 515
      dr=dr+dr
  515 continue
c
      eras=(0.5/137.036)**2
      do 520 i=1,kkk
      psq=pnl(i,m)*pnl(i,m)
      fi(i,1)=psq*d(i,1)
      fi(i,2)=psq*d(i,2)
      fi(i,3)=psq*d(i,3)
      fi(i,4)=psq*((ee(m)-v(i))**2)
      fi(i,5)=pnl(i,m)*(r(i)*p(i)-pnl(i,m))*d(i,3)
      eras1=1.0+eras*(ee(m)-v(i))
      fi(i,2)=fi(i,3)/eras1
      if (irel.ne.0) fi(i,5)=fi(i,5)/eras1
  520 continue
      fi(1,4)=4.0*(fi(2,4)-1.5*fi(3,4)+fi(4,4))-fi(5,4)
      fi(1,5)=4.0*(fi(2,5)-1.5*fi(3,5)+fi(4,5))-fi(5,5)
c
c          integrate
c
      p(1)=0.0
      p(2)=0.0
      p(3)=0.0
      ibo=ib
      n1=3
      if (l(m).le.0) n1=4
      do 530 n=n1,5
      if (itpow.gt.5.and.n.ge.4) ib=-m
  530 call quad5(fi(1,n),1,1,kkk,p(n))
      ib=ibo
  550 zeta(m,1)=2.66246e-5*p(1)
      zeta(m,2)=2.66246e-5*p(2)
      zeta(m,3)=2.66246e-5*p(3)
      if (m.lt.n1sc) go to 700
      evel(m)=-1.33123e-5*p(4)
      edar(m)=-1.33123e-5*p(5)
c
  700 zetak1=zeta(m,1)*109737.31
      zetak1=zeta(m,2)
      zetak2=zeta(m,2)*109737.31
      zetak3=zeta(m,3)*109737.31
      ec=evel(m)
      if (l(m).eq.0) ec=ec+edar(m)
      etotrl=etotrl+wwnl(m)*ec
      if (ihf.eq.1) go to 705
      erll=epss(m)+ec
c          call zetabw to calc spin-orbit integral by the blume-watson m
  701 if (ee(m).lt.0.0) zetak2=zetabw(m,rm3(m))
  702 zetak1=zetak2/109737.3
      if (ifrac.eq.0) then
        write (9,70) nlbcd(m),wwnl(m),zetak1,zetak2,zeta(m,3),zetak3,
     1  ei(m),epss(m),evel(m),edar(m),ec,erll
   70 format (1x,a3,f6.0,f13.5,f12.3,f10.5,f12.3,f11.5,f12.5,2f11.5,
     1  f9.3,f12.5)
      else
        write (9,71) nlbcd(m),wwnl(m),zetak1,zetak2,zeta(m,3),zetak3,
     1  ei(m),epss(m),evel(m),edar(m),ec,erll
   71 format (1x,a3,f7.3,f12.5,f12.3,f10.5,f12.3,f11.5,f12.5,2f11.5,
     1  f9.3,f12.5)
      end if
c
c          store zeta for rcg input
c
  705 if (l(m).le.0) go to 899
      if (wwnl(m).eq.0.0) go to 899
      eras=4*l(m)+2
      if (wwnl(m)-eras) 720,899,899
  720 nzi=nzi+1
      zi(nzi)=zetak3*0.001
  721 zi(nzi)=zetak2*0.001
c
  899 continue
      if (irel.ne.0) etotrl=0.0
      return
      end
