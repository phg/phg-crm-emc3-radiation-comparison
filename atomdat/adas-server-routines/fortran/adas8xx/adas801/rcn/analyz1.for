C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/analyz1.for,v 1.2 2004/07/06 11:16:10 whitefor Exp $ Date $Date: 2004/07/06 11:16:10 $
C
      subroutine analyz1
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      character*8 ia(10),ib(3),inpcard*70,iba
      common/ch/ia,ib,inpcard,iba
      character chtemp*70,lbcd(21)*1
c
      data lbcd/'s','p','d','f','g','h','i','k','l','m',
     1          'n','o','q','r','t','u','v','w','x','y','z'/
c
      i=6
  110 nbl=0
  112 i=i+1
      if (inpcard(i:i).ne.' ') go to 110
      nbl=nbl+1
      if (nbl.ge.3.or.i.ge.18) go to 120
      go to 112
c          configuration identification
  120 chtemp=' '
      i=min0(i,18)
      chtemp(1:i)=inpcard(1:i)
      read (chtemp,12) ib
   12 format (3a8)
c
      i3=i
      kut=0
      ee8=0.0
      nalf=0
      alfmin=0.0
      alfmax=0.0
      norb=0
      do 125 j=1,10
  125 ia(j)='        '
c
c
  130 i=i3
      i1=0
      i2=0
      i3=0
      idec=0
      ineg=0
      chtemp=' '
c
  132 i=i+1
      if (i.gt.70) go to 900
      if (inpcard(i:i).ne.' ') go to 134
      if (i3.ne.0) go to 150
      go to 132
  134 if (i1.eq.0) i1=i
      if (i2.gt.0) go to 138
      do 136 j=1,20
      if (inpcard(i:i).eq.lbcd(j)) i2=i
  136 continue
      if (inpcard(i:i).eq.'.') idec=i
      if (inpcard(i:i).eq.'-') ineg=i
  138 i3=i
      if (i.lt.70.or.i1.eq.0) go to 132
c
  150 chtemp='     '
      if (i2.ne.0.and.i1.ge.i2-2) go to 300
      if (norb.eq.0.and.nalf.eq.0.and.i3.gt.i1.and.ineg.eq.0) go to 200
      if (idec.gt.0) go to 500
      if (ineg.gt.0.or.i1.eq.i3) go to 400
      go to 500
c          alfmax and (if i3-i1.gt.1) alfmin
  200 if (i2.gt.0) i3=i2-3
      chtemp(i1-i3+4:4)=inpcard(i1:i3)
      read (chtemp,20) ialfmin,ialfmax
   20 format (2i2)
      alfmin=0.01*ialfmin
      alfmax=0.01*ialfmax
      nalf=1
      go to 130
c          orbital
  300 i3=min0(i3+1,i2+6)
      chtemp(3+i1-i2:9)=inpcard(i1:i3)
      norb=norb+1
      read (chtemp,30) ia(norb)
   30 format (a9)
      go to 130
c          kut
  400 chtemp(1:2)=inpcard(i3-1:i3)
      read (chtemp,40) kut
   40 format (i2)
      go to 130
c          ee8
  500 chtemp=inpcard(i1:i3)
      read (chtemp,50) ee8
   50 format (f70.3)
      go to 130
c
  900 continue
      write (9,90) inpcard
   90 format (/3x,a70)
      write (9,91) ib,alfmin,alfmax,(ia(m),m=1,8),kut,ee8
   91 format (1x,2a8,a2,2f3.1,8a9,i2,f6.2)
      return
      end
