C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/s3j.for,v 1.2 2004/07/06 15:18:02 whitefor Exp $ Date $Date: 2004/07/06 15:18:02 $
C
      function s3j (fj1, fj2, fj3, fm1, fm2)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      s3j=0.0
      fm3=-fm1-fm2
      if (abs(fm3).gt.fj3) return
      a=fj1+fj2-fj3
      b=fj1-fj2+fj3
      c=-fj1+fj2+fj3
      d=fj1-fm1
      e=fj2+fm2
      x=fctrl(a)*fctrl(b)*fctrl(c)
      if (x.le.0.0) return
      x=x/fctrl(fj1+fj2+fj3+1.0)
      x=x*fctrl(d)*fctrl(fj1+fm1)*fctrl(fj2-fm2)*fctrl(e)
     1  *fctrl(fj3-fm3)*fctrl(fj3+fm3)
      if (x.le.0.0) return
      x=sqrt(x)
      b=d-b
      c=e-c
      zero=0.0
      fk=max(zero,b,c)
      k1=fk
      k2=min(a,d,e)
      if (k2.lt.k1) return
      f=(-1.0)**k1
      y=0.0
      do 150 k=k1,k2
      y=y+f/(fctrl(fk)*fctrl(fk-b)*fctrl(fk-c)*fctrl(a-fk)
     1  *fctrl(d-fk)*fctrl(e-fk))
      fk=fk+1.0
  150 f=-f
      s3j=x*y
      k=fj1-fj2-fm3
      s3j=s3j*((-1.0)**k)
      return
      end
