C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/analyz.for,v 1.2 2004/07/06 11:16:04 whitefor Exp $ Date $Date: 2004/07/06 11:16:04 $
C
      subroutine analyz
c
      implicit real*8 (a-h,o-z)
      include 'rcn.h'
      include 'common.h'
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70,chtemp*70,lbcd(21)*1
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c5/izo,iono,idum(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      character*6 wwwnl(25),wwnl8(8)
      common/charww/wwwnl,wwnl8
c
      data lbcd/'s','p','d','f','g','h','i','k','l','m',
     1          'n','o','q','r','t','u','v','w','x','y','z'/
c
      i=6
  110 nbl=0
  112 i=i+1
      if (inpcard(i:i).ne.' ') go to 110
      nbl=nbl+1
      if (nbl.ge.3.or.i.ge.18) go to 120
      go to 112
c          configuration identification
  120 chtemp=' '
      i=min0(i,18)
      chtemp(1:i)=inpcard(1:i)
      read (chtemp,12) conf
   12 format (3a6)
c
      i3=i
      do 125 j=1,8
      nlbcd8(j)='   '
  125 wwnl8(j)='  ' 
      kut=0
      ee8=0.0
      nalf=0
      alfmin=0.0
      alfmax=0.0
      norb=0
c
c
  130 i=i3
      i1=0
      i2=0
      i3=0
      idec=0
      ineg=0
      chtemp=' '
c
  132 i=i+1
      if (i.gt.70) go to 900
      if (inpcard(i:i).ne.' ') go to 134
      if (i3.ne.0) go to 150
      go to 132
  134 if (i1.eq.0) i1=i
      if (i2.gt.0) go to 138
      do 136 j=1,20
      if (inpcard(i:i).eq.lbcd(j)) i2=i
  136 continue
      if (inpcard(i:i).eq.'.') idec=i
      if (inpcard(i:i).eq.'-') ineg=i
  138 i3=i
      if (i.lt.70.or.i1.eq.0) go to 132
c
  150 chtemp='     '
      if (i2.ne.0.and.i1.ge.i2-2) go to 300
      if (norb.eq.0.and.nalf.eq.0.and.i3.gt.i1.and.ineg.eq.0) go to 200
      if (idec.gt.0) go to 500
      if (ineg.gt.0.or.i1.eq.i3) go to 400
      go to 500
c          alfmax and (if i3-i1.gt.1) alfmin
  200 if (i2.gt.0) i3=i2-3
      chtemp(i1-i3+4:4)=inpcard(i1:i3)
      read (chtemp,20) ialfmin,ialfmax
   20 format (2i2)
      alfmin=0.01*ialfmin
      alfmax=0.01*ialfmax
      nalf=1
      go to 130
c          orbital
  300 i3=min0(i3+1,i2+6)
      chtemp(3+i1-i2:9)=inpcard(i1:i3)
      norb=norb+1
      read (chtemp,30) nlbcd8(norb),wwnl8(norb)
   30 format (a3,a6)
      go to 130
c          kut
  400 chtemp(1:2)=inpcard(i3-1:i3)
      read (chtemp,40) kut
   40 format (i2)
      go to 130
c          ee8
  500 chtemp=inpcard(i1:i3)
      read (chtemp,50) ee8
   50 format (f70.3)
      go to 130
c
  900 continue
c     write (6,90) inpcard
c  90 format (/3x,a70)
c     write (6,91) conf,alfmin,alfmax,(nlbcd8(m),wwnl8(m),m=1,8),
c    1  kut,ee8
c  91 format (1x,3a6,2f3.1,8(a3,a6),i2,f6.2)
      return
      end
