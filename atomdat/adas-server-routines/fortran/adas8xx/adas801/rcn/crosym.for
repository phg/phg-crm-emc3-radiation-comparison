C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/crosym.for,v 1.2 2004/07/06 12:23:47 whitefor Exp $ Date $Date: 2004/07/06 12:23:47 $
C
      subroutine crosym(m)
c
c     simultaneous equation solver
c     written by i.c. hanson, scientific computation department,
c     lockheed missles and space company, sunnyvale, california
c     solve m simultaneous equations by the method of crout
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
c
      n=m+1
      i1=1
    1 i3=i1
      sum=abs(a(i1,i1))
      do 3 i=i1,m
      if (sum-abs(a(i,i1))) 2,3,3
    2 i3=i
      sum=abs(a(i,i1))
    3 continue
      if (i3-i1) 4,6,4
    4 do 5 j=1,n
      sum=-a(i1,j)
      a(i1,j)=a(i3,j)
    5 a(i3,j)=sum
    6 i3=i1+1
      do 7 i=i3,m
    7 a(i,i1)=a(i,i1)/a(i1,i1)
   14 j2=i1-1
      i3=i1+1
      if (j2) 8,11,8
    8 do 9 j=i3,n
      do 9 i=1,j2
    9 a(i1,j)=a(i1,j)-a(i1,i)*a(i,j)
      if (i1-m) 11,15,11
   11 j2=i1
      i1=i1+1
      do 12 i=i1,m
      do 12 j=1,j2
   12 a(i,i1)=a(i,i1)-a(i,j)*a(j,i1)
      if (i1-m) 1,14,1
   15 do 17 i=1,m
      j2=m-i
      i3=j2+1
      a(i3,n)=a(i3,n)/a(i3,i3)
      if (j2) 16,18,16
   16 do 17 j=1,j2
   17 a(j,n)=a(j,n)-a(i3,n)*a(j,i3)
   18 return
      end
