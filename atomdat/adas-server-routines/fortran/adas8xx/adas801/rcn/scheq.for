C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/scheq.for,v 1.3 2004/07/06 15:18:59 whitefor Exp $ Date $Date: 2004/07/06 15:18:59 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  01-03-2004 : Fix format statement 20.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine scheq
c
c     compute energy eigenvalue and wave function
c      originally  written by sherwood skillman
c      rca laboratories, princeton, new jersey, spring 1961
c      modified by frank herman, summer 1961
c      further modified by richard kortum,  lockheed research
c       laboratories, palo alto, california,  summer 1962
c      further modified by r. d. cowan, lasl, jan-feb, 1964
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
      common/c9/ qq(kmsh),delp(ko),alfm(ko)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
c
      dimension p(5),d(5),q(5),t(5)
      save
c
c
c
c     set up constants and initialize
c
      erasx=0.0
  100 many=99
      emore=-1.0e-10
      eless=-1.0e+12
      eg=0.0
      de=0.0
      nprint=0
      imatch=0
      lamp=lam+1
      xlp=lamp
      ndcr=nn-lamp
      if (nn.gt.98) ndcr=9999
      b0=lam*lamp
      h=r(2)
      h1=h*h
      b3=(v(3)-v(2))/h-z/h1
      y=h+h
      flps=4*lam+6
      slpt=6*lam+12
      elpt=8*lam+20
      a1=-z/xlp
      b1=-z-z
      ab1=a1*b1
      ab3=a1*b3
c
c     raise h and y to lam+1
c
      htl=h
      ytl=y
      if (lam) 210,140,120
  120 do 130 i=1,lam
      htl=htl*h
  130 ytl=ytl*y
  140 bohs=b0/h1
      boh=b1/h
      bth=b3*h
      bq3=bohs+boh+bth
      bq4=bohs/4.0+boh/2.0+bth+bth
  150 if ((niter.eq.1.and.n1sc.eq.1).or.irel.eq.0) go to 200
      if (lam.ne.0) go to 200
c          calc darwin relativistic term
      kkk=nkkk(mm)
      nblk=kkk/40
      i=2
      k1=2
      k2=40
      do 160 j=1,nblk
      if (j.eq.nblk) k2=39
      eras=0.5/(r(i+1)-r(i))
      do 155 k=k1,k2
      i=i+1
      eras1=eras*(pnl(i+1,mm)-pnl(i-1,mm))
      eras1=eras1/pnl(i,mm)-1.0/r(i)
  155 vr(i)=eras1*eras*(x2(i+1)-x2(i-1))
      k1=1
      if (i.gt.idb.or.j.eq.nblk) go to 160
      eras=0.5*eras
      eras1=eras*(pnl(i+1,mm)-pnl(i-2,mm))
      eras1=eras1/pnl(i,mm)-1.0/r(i)
      vr(i)=eras1*eras*(x2(i+1)-x2(i-2))
  160 continue
      vr(2)=4.0*vr(3)
      vr(mesh)=vr(mesh-1)
      if (iptvu.ge.6.and.mod(niter-2,5).eq.0)
     1  write (9,7788) nn,lam,eras, (vr(k), k=2,47)
 7788 format (2i10/(1p,8e15.5))
      if (irel.ne.3) go to 180
      eras=1.0/(12.0*(r(2)-r(1)))
      do 165 i=4,39
      eras1=eras*(8.0*(pnl(i+1,mm)-pnl(i-1,mm))-pnl(i+2,mm)+pnl(i-2,mm))
      eras1=eras1/pnl(i,mm)-1.0/r(i)
  165 vr(i)=eras1*eras*(8.0*(x2(i+1)-x2(i-1))-x2(i+2)+x2(i-2))
      eras1=log(vr(4)/vr(5))/log(r(5)/r(4))
      vr(3)=vr(4)*((r(4)/r(3))**eras1)
      vr(2)=vr(4)*((r(4)/r(2))**eras1)
      vr(mesh)=vr(mesh-1)
      if (iptvu.ge.5.and.mod(niter-2,5).eq.0)
     1  write (9,7788) nn,lam,eras, (vr(k), k=2,47)
  180 do 185 i=20,mesh-2
      if (vr(i+1)*vr(i).gt.0.0) go to 185
      j=i
      if (abs(vr(i+1)).gt.abs(vr(i))) j=i+1
      vr(j)=0.5*(vr(j+1)+vr(j-1))
  185 continue
c
c     start outward integration
c
  200 nprint=nprint+1
      j=iabs(npr)
      if (j.eq.0.or.j.gt.9) go to 880
      if (mod(nprint,j).ne.0) go to 880
c     if (mm.lt.ncspvs-2) go to 880
      write (9,17) niter,nprint,nn,lam,ik,imatch,kkk,ncross,
     1  de,eg,emore,eless
   17 format (//3i3,i1,4i5,4f10.6)
      write (9,18) (qq(j),j=201,kkk,10)
   18 format (1x,10f12.4)
      write (9,19) (pnlo(j),j=201,kkk,10)
   19 format (1x,10f12.7)
  880 continue
      eps =e-eg
      eg =e
C      write(0,'11HEnergies...,1p,4e12.3,i4,1x,a8')
C     &      eless,emore,e, de,nprint,nlbcd(mm)
      if (nprint-many) 220,220,201
  201 write (9,20) nlbcd(mm),nprint,eless,emore,e, de,ncross,imatch,i,ik,
     1  ikmax, (pnlo(j), j=1,mesh,2)
   20 format (' *****No convergence on ',a8,i4,4e17.8,4i4/
     1  /' EMX may need to be non-zero, but small'//(3x,1p,10e11.3))
      write (6,20) nlbcd(mm),nprint,eless,emore,e, de,ncross,imatch,i,ik,
     1  ikmax
      write (6,1020) r(mesh),emx,nlbcd(mm)
 1020 format (' Possibly r(mesh)=', f10.5,' is too small because',
     1  ' EMX=0.0 (mesh=641 instead of 1801),'/
     2  '         or because EMX is too large (idb too small--increase',
     3  ' idb by 40 by decreasing EMX=',f7.2,' by a factor 4):'/
     4  '     r(mesh) needs to be 5 or 10 times as large as <r> for ',
     5  'P(',a3,').')
      if (abs(de/e)-8.0*thresh) 900,205,205
  205 end=1.0
      go to 990
  210 nstop=210
  211 write (9,21)nstop
   21 format(5h0stop,i4,9h in scheq)
      stop 210
  220 do 221 i=1,mesh
  221 pnlo(i)=0.0
c
c     calc qq.    if e neg, make certain that qq(mesh) is positive,
c         and that qq(i) is negative for some i less than ikmax
c
      n2bg=nprint+nn
      if (nprint-1) 210,230,235
  230 do 231 i=1,mesh
  231 qq(i) = v(i)+b0/(r(i)*r(i))-e
      if ((niter.eq.1.and.n1sc.eq.1).or.irel.eq.0) go to 233
c          calc mass-velocity relativistic term
      eras=(0.5/137.036)**2
      do 232 i=2,kkk
      eras1=e-v(i)
      eras2=eras*eras1
      qq0(i)=qq(i)
      eras3=eras1*eras2
      if (lam.eq.0) eras3=eras3+eras*vr(i)/(1.0+eras2)
  232 qq(i)=qq(i)-eras3
      if (niter.ne.8.or.irel.lt.4) go to 233
      write(9,22) niter,nprint,mm,b0,e
   22 format (1h1,3i5,2f17.7//)
      write(9,23)(i,r(i),v(i),vr(i),qq0(i),qq(i),pnl(i,mm), i=11,kkk,10)
   23 format (i5,6f18.7)
  233 ik=mesh+1
      if (e) 240,240,250
  235 do 236 i=1,mesh
  236 qq(i) = qq(i)-eps
      if (nprint-20) 240,240,237
  237 if (abs(eps/e)-0.00001) 250,250,240
  240 m=mesh
      ik=m+1
      do 246 i=10,mesh
      if (qq(m)) 245,246,246
  245 ik=m+20
      go to 247
  246 m=m-1
      eps=-0.3*e
      go to 248
  247 ikmax=mesh-min0( 60, 6000/(nprint+nn)**2 )
      if (ik.le.ikmax+5) go to 250
      mesh=mesh
      eps=min(qq(ikmax-10),qq(mesh)-0.000001)
  248 e=e+eps
      eg=e
      n2bg=n2bg+1
      if (n2bg.le.22) go to 235
      if (nprint.lt.many) go to 250
      write (9,24) nn,lam, (qq(i), i=201,mesh)
   24 format (//2i3//(5x,10f11.5))
c
c          calc starting values for irel=0
c     b0=lam*(lam+1)
c     b1= -2.*z
c     b3=(v(3)-v(2))/h -z/hsq
c     a1= -z/(lam+1)
c     a2=(a1*b1+b2)/(4*lam+6)
c     a3=(a2*b1+a1*b2+b3)/(6*lam+12)
c     a4=(a3*b1+a2*b2+a1*b3)/(8*lam+20)
c     p(3)=(1.0+a1*h+a2*h**2+a3*h**3+a4*h**4)*h**(xl+1.0)
c     p(4)=(1.0+a1*y+a2*y**2+a3*y**3+a4*y**4)*y**(xl+1.0)
c     q(3)=(b+b1*h+b2*h**2+b3*h**3)/h**2
c     q(4)=(b+b1*y+b2*y**2+b3*y**3)/y**2
c
  250 h=r(2)
      y=h+h
      b2=3.0*z/h-e+2.0*v(2)-v(3)
      a2=(ab1+b2)/flps
      a3=(a2*b1+a1*b2+b3)/slpt
      a4=(a3*b1+a2*b2+ab3)/elpt
      p(3)=(1.0+h*(a1+h*(a2+h*(a3+h*a4))))*htl
      p(4)=(1.0+y*(a1+y*(a2+y*(a3+y*a4))))*ytl
      p(5)=0.0
      q(3)=bq3+b2
      q(4)=bq4+b2
      i=3
      dx=h
      h1=h**2
      h2=h1/12.0
      if (irel.eq.0) go to 255
c          calc starting values for irel.ne.0 (relativistic terms includ
      q(3)=qq(2)
      q(4)=qq(3)
      eras1=(z/137.036)**2
      d2=1.0/(1.0+(274.072**2+e)*r(3)*0.5/z)
      if (lam.ne.0) d2=0.0
      eras=0.5*(1.0-d2)+sqrt(b0+0.25*((1.0+d2)**2)-eras1)
      p(3)=r(2)**eras
      p(4)=r(3)**eras
      eras2=eras+1.0+d2
      a1=-(2.0*z+eras1*e/z)/(eras2*eras-b0+eras1)
      p(3)=p(3)*(1.0+a1*r(2))
      p(4)=p(4)*(1.0+a1*r(3))
c
  255 pnlo(2)=p(3)
      pnlo(3)=p(4)
      t(3)=p(3)*(1.0-h2*qq(2))
      t(4)=p(4)*(1.0-h2*qq(3))
      d(4)=t(4)-t(3)
      ncount=2
      nint=2
      if (nprint-40) 280,260,260
  260 write (9,26) nn,lam,niter,nprint,ikmax,ik,imatch,ncross,e,eps,
     1  eless,emore,alfm(mm),delta
   26 format (1h ,i2,i1,6i6,4f19.11,f6.3,f10.6)
  280 ncross=0
      ib1=ib+1
      ptthree=0.3
c
c          integrate outward
c
  300 i=i+1
      if (i.lt.ib1) go to 310
      if (e.ge.0.0) go to 960
      if (ncross-ndcr) 370,303,360
  303 if (ik-ib1) 370,850,850
c
  310 q(5)=qq(i)
      if (abs(p(5)).gt.1.0e30) go to 312
      if (i.lt.ik) go to 314
  312 if (ncount.gt.1) go to 314
      if (nint.gt.4) go to 350
  314 d(5)=d(4)+h1*q(4)*p(4)
      t(5)=d(5)+t(4)
      eras=h2*q(5)
      if (abs(eras).gt.abs(erasx)) erasx=eras
      if (abs(eras).gt.ptthree) eras=sign(ptthree,eras)
  315 p(5)=t(5)/(1.0-eras)
      pnlo(i)=p(5)
      if (p(5).gt.0.0.and.p(4).gt.0.0) go to 331
      if (p(5).lt.0.0.and.p(4).lt.0.0) go to 331
      if (p(5).eq.0.0) go to 331
c          count changes in sign
  330 ncross=ncross+1
  331 ncount=ncount+1
      if (ncount.eq.6) ncount=1
      nint=nint+1
      if (i.gt.idb) go to 341
      if (nint.lt.40) go to 341
  340 dx=dx+dx
      h=dx
      h1=h**2
      h2=h1/12.0
      nint=0
      t(5)=p(5)*(1.0-h2*q(5))
      t(3)=p(3)*(1.0-h2*q(3))
      d(5)=t(5)-t(3)
  341 do 342 k=1,4
      p(k)=p(k+1)
      t(k)=t(k+1)
      d(k)=d(k+1)
  342 q(k)=q(k+1)
      go to 300
c
c     matching radius has been reached going out
c
  350 if (ncross-ndcr) 370,380,360
c          too many crossings, increase absf(e)
  360 emore=min(emore,e)
      e=max(2.00*e,0.5*(eless+emore))
      go to 200
c          too few crossings, decrease absf(e)
  370 eless=max(eless,e)
      de=emore-eless
      if (de.lt.0.0001) emore=emore+0.1*de
      if (emore.ge.0.0) emore=-1.0e-8
      e=min(0.50*e,0.5*(eless+emore))
      go to 200
c
c     number of crossings is correct
c     check to see that wave is in the damped region (absolute value
c       decreasing and signs alike)
c
  380 if (abs(pnlo(i-1))-abs(pnlo(i-2))) 382,381,381
c     large absolute value of p in what should be the damped region
c       indicates too few peaks, decrease absf(e)
  381 if (abs(p(5))-1.0e05) 314,370,370
c
  382 if (p(5)) 383,314,384
  383 if (pnlo(i-2)) 400,314,314
  384 if (pnlo(i-2)) 314,314,400
c
c     now ndcr = ncross and matching radius lies in damped region
c         calculate logarithmic derivative
c
  400 imatch=i-2
      xmatch=r(i-2)
      ppout=(t(4)-t(2)-0.5*(p(4)-p(2)))/h
      s6=ppout/p(3)
c
c  integrate pnlo**2
c
      call quad5(pnlo,2,1,imatch,sum1)
c
      s5=sum1/p(3)**2
      pmatch=p(3)
c
c          start inward integration
c
c       choose outermost mesh point, kkk
  500 xinw=10.0*xmatch
  510 do 511 i=41,mesh,40
      if (r(i).ge.xinw) go to 512
  511 continue
      kkk=mesh
      go to 550
  512 kkk=i
c       calc starting values
  550 i=kkk
      dx=r(i)-r(i-1)
      h=-dx
      h1=h*h
      h2=h1/12.0
      q(3)=qq(i)
      eras1=sqrt(q(3))
      eras2=r(i)*eras1
      p(3)=exp(-eras2)
      i=i-1
      q(4)=qq(i)
      eras3=r(i)*sqrt(q(4))
      p(4)=exp(-eras3)
      if (abs(p(4)).gt.1.0e-35) go to 580
      kkk=kkk-40
      if (kkk.gt.imatch) go to 550
  570 write (9,57)z,nn,lam,kkk,imatch
   57 format (6h0at z=,f6.0,6h   nl=,i3,i1,10h,     kkk=,i3,
     1    22h  is less than imatch=,i3,
     2    48h.    inward integration will be tried at kkk+40.)
      kkk=kkk+40
      p(3)=1.0e-35
      p(4)=p(3)*exp(eras2-eras3)
  580 if (pmatch.gt.0.0) go to 582
      p(3)=-p(3)
      p(4)=-p(4)
  582 pnlo(i+1)=p(3)
      pnlo(i)=p(4)
      t(3)=p(3)*(1.0-h2*q(3))
      t(4)=p(4)*(1.0-h2*q(4))
      d(4)=t(4)-t(3)
      sum3=p(3)*p(3)/2.0/eras1
c
c          integrate inward
c
      m2=kkk-idb
      if (m2.lt.40) m2=40
  600 do 620 m=2,m2
      i=i-1
      q(5)=qq(i)
      d(5)=h1*q(4)*p(4)+d(4)
      t(5)=d(5)+t(4)
      p(5)=t(5)/(1.0-h2*q(5))
      if (i.eq.imatch-1) go to 700
      pnlo(i)=p(5)
      do 620 k=1,4
      p(k)=p(k+1)
      t(k)=t(k+1)
      d(k)=d(k+1)
  620 q(k)=q(k+1)
      m2=40
      q(5)=qq(i-2)
      d(5)=h1*q(4)*p(4)+d(4)
      t(5)=d(5)+t(4)
      p(5)=t(5)/(1.0-h2*q(5))
      p(5)=1.09375*p(4)+0.2734375*p(5)-0.546875*p(3)+0.21875*p(2)
     1     -0.0390625*p(1)
      i=i-1
      dx=dx/2.0
      q(5)=qq(i)
      h=-dx
      h1=h*h
      h2=h1/12.0
      t(5)=p(5)*(1.0-h2*q(5))
      t(4)=p(4)*(1.0-h2*q(4))
      d(5)=t(5)-t(4)
      pnlo(i)=p(5)
      do 630 l1=1,4
      p(l1)=p(l1+1)
      t(l1)=t(l1+1)
      d(l1)=d(l1+1)
  630 q(l1)=q(l1+1)
      go to 600
c
c     matching radius has been reached coming in
c            integrate pnlo**2 and calc logarithmic derivative
c
  700 k=kkk
      call quad5(pnlo,2,imatch,kkk,sum4)
      sum3=sum3+sum4
  760 s3=sum3/p(4)**2
      ppin=(t(5)-t(3)-0.5*(p(5)-p(3)))/h
      s4=ppin/p(4)
c
c     improve trial eigenvalue by perturbation theory if necessary
c
  800 de=(s6-s4)/(s5+s3)
      if (de) 801,900,802
  801 emore=min(emore,e)
      de=max(de,0.8*(eless-e))
      go to 805
  802 eless=max(eless,e)
      de=min(de,0.8*(emore-e))
  805 e=e+de
      if (abs(de/e).gt.thresh) go to 200
      if (kkk.eq.ib) go to 912
      go to 900
c
c     improve trial eigenvalue for finite-boundary case
c
  850 kkk=ib
      ppout=(pnlo(kkk+1)-pnlo(kkk-1))/(r(kkk+1)-r(kkk-1))
      if (mod(kkk-1,40).ne.0) go to 860
      ppout=0.5*(ppout+(pnlo(kkk)-pnlo(kkk-1))/(r(kkk)-r(kkk-1)))
  860 call quad5(pnlo,2,1,kkk,sum1)
      s6=ppout/pnlo(kkk)
      s5=sum1/pnlo(kkk)**2
      s4=1.0/r(kkk)
      s3=0.0
      go to 800
c
c     determine normalization constant for bound wavefunction
c
  900 pop=pmatch/p(4)
      kkk=min0(kkk,ib)
      do 910 j=imatch,kkk
  910 pnlo(j)=pnlo(j)*pop
  912 call quad5(pnlo,2,1,kkk,sum1)
  950 c1=sqrt(sum1)
      if (pnlo(3).gt.0.0) go to 980
      c1=-c1
      go to 980
c
c     determine normalization constant for unbound wavefunction
c         ( pmax=1.0/sqrt(pi*sqrt(e)) )
c
  960 i=i-1
      nmin=i
  962 i=i-1
      inmin=i
      if (pnlo(i+1).eq.0.0) go to 962
      if (pnlo(i)/pnlo(i+1).gt.0.0) go to 962
  964 i=i-1
      if (pnlo(i)/pnlo(i+1).gt.1.0) go to 964
  972 a1=(pnlo(i)-pnlo(i+1))/(r(i)-r(i+1))
      b1=(pnlo(i+1)-pnlo(i+2))/(r(i+1)-r(i+2))
      a1=(a1-b1)/(r(i)-r(i+2))
      b1=b1-a1*(r(i+1)+r(i+2))
      c1=pnlo(i+1)-r(i+1)*(a1*r(i+1)+b1)
      c1=abs(c1-b1*b1/4.0/a1)
      zst=zzz-1.0+1.0e-7
      a1=0.5*zst/(e*r(i+1))
      b0=lam*lamp
      b1=1.0-b0/(2.0*zst*r(i+1))-2.5*a1
      f=1.0-a1*b1
      if ((kut.eq.-1.and.delta.lt.(20.0*tolend)).and.abs(delp(mm)).
     1  lt.4.0e-6) then
         write (9,97) zst,e,r(i+1),a1,b1,f,i,inmin,nmin
   97 format (1h0,9x,23h*****zst,e,r,a1,b1,f,i=,3f13.6,3f13.8,3i7)
      if (nconf.ne.nconf6.and.inmin-i.lt.2) 
     1     write (6,98) nlbcd(ncspvs),i,inmin,
     2     nlbcd(ncspvs),mesh-10,mesh-1,(pnlo(nn),nn=mesh-10,mesh-1)
   98 format (/' *****EMX too small for accurate calc. of continuum ',
     1  'function  P(',a3,'):  i,inmin=',2i6,' must differ by two',
     2   ' or more.' /' P/',a3,'\(',i4,'-',i4,')=   ',10(f9.5)/)
      nconf6=nconf
      end if
      c1=c1/f
      c1=c1*sqrt(3.141592654*sqrt(e))
      kkk=mesh
      imatch=i+1
c
c     calculate the normalized wave function
c
  980 j=1
      if (mod(niter,5).eq.0.and.irel.gt.3) write (9,9872) m,lam,erasx
 9872 format (10x,2i6,6herasx=,f10.6)
      k=2
      c1=1.0/c1
      do 985 i=1,kkk-1
      if (pnlo(j)*pnlo(j+1).ge.0.0) go to 984
      if (k.ge.nodf) go to 985
      k=k+1
  984 j=j+1
  985 pnlo(i)=pnlo(i)*c1
      if (niter.ne.17.or.nn.gt.2) go to 8790
      if (irel.ne.3) go to 8790
      qq0(1)=0.0
      qq(1)=0.0
      do 8787 i=2,360
      eras=pnlo(i-1)
      if (mod(i-1,40).eq.0) eras=pnlo(i-2)
      eras=pnlo(i+1)-2.0*pnlo(i)+eras
      qq0(i)=eras/(r(i+1)-r(i))**2
 8787 qq(i)=qq(i)*pnlo(i)
      write (9,8788)
 8788 format (1h1)
      write (9,8786) (i,pnlo(i),qq0(i),qq(i), i=1,51)
 8786 format (3(i4,f11.7,2f14.2))
 8790 continue
      if (nodf.gt.1) go to 990
      j=1
  990 return
      end
