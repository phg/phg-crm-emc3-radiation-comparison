C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/setcfg.for,v 1.2 2004/07/06 15:19:17 whitefor Exp $ Date $Date: 2004/07/06 15:19:17 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Make ww8 an integer and change all tests and mods 
C               involving it to integers also.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine setcfg
c
c     set up heaviest noble gas configuration with no. of electrons
c          .le.iz-nspect+1, and modify as indicated on config. input car
c     estimate eigenvalue for each orbital
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/c1/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),rsatom(kmsh),
     1     nnlz(ko),nnn(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),l(ko),nkkk(ko),ee(ko),
     3  ei(ko),ekin(ko),een(ko),uee(ko),uex(ko),ecorr(ko),ucorr(ko),
     4  epss(ko),evel(ko),edar(ko),erel(ko),a(ko,5),b(ko),nsch(10,ko),
     5  az(ko),imat(ko),ecm(10)
      character*8 nlbcd(ko),nlbcdo(ko),nlbcd8(ko),ifg(10,9),jfg(10,9),
     1  nla,nlb
      character conf(3)*6,inpcard*70
      common/char/conf,nlbcd,nlbcdo,nlbcd8,ifg,jfg,nla,nlb,inpcard
      common/xij/xi(kmsh),xj(kmsh),v(kmsh),rscore(kmsh),rsvale(kmsh),
     1  recorr(kmsh),rucorr(kmsh),rc(kmsh),pnlo(kmsh),x1(kmsh),x2(kmsh)
c
      common/c2/z,iz,nconf,ncores,nvales,ncspvs,ion,mesh,kut,icut,idb,
     1  rdb,corrf,c,twozzz,nblock,kkk,tolstb,tolkm2,tolend,thresh,
     2  etot,ecorrt,ca0,ca1,emx,ekint,eentot,etotk,etotp,zzz,
     3  alfmin,alfmax,twoion,twoz,ncelec,nvelec,itpow,iptvu,xif,
     4  exf,exf10,exfm1,ipteb,ns,ib,nconft2,rm3(ko),iw6,nconft,ifrac
      common/c3/npar,nzi,nfij,ngij,vprm(50),kpar(50),zi(8),fij(30),
     1  gij(35),norb,norbpt,dh(5),delta,npr
      common/c4/vr(kmsh),qq0(kmsh),irel,niter,nn,lam,e,end,nodf,i,j,k,
     1  mm,imatch,rsint,rsint1,hxid,izhxbw,iphfwf,ihf,ihf1,nprint
      common/c5/izo,iono,lbcd(21),morb(7),eeo(ko),
     1  ee8,noelec,ierror,wwnlo(ko),n1sc,n1scf,iblank
      common/c6/a0,a1,a2,a3,a4,ab1,ab3,b0,b1,b2,b3,eras,eras1,eras2
      common/pq/pnl(kmsh,ko),qnl(kmshq,koq)
c
      dimension ne(7)
c     equivalence (ru,      f1), (ruee,  f2), (ruexch,rsq,xa,emn),
c    1 (rsatom,qq,drudr,xb), (a,delp,rryd), (b,alfm,rkay)
c     equivalence (rsatom,s1),(rsatom(251),s2),(rsatom(501),zeta)
      character*6 wwwnl(25),wwnl8(8)
      common/charww/wwwnl,wwnl8
c
      data (ne(i),i=1,7)     /2,10,18,36,54,86,118/
      
      integer ww8
c
c     set up noble gas core with atomic number less than or equal to noe
c
c     first find number of orbitals in this core from table
c
      ierror=0
  218 mcore=0
      necore=0
      ifrac=0
      do 220 n=1,7
      if (ne(n).gt.noelec) go to 221
      necore=ne(n)
  220 mcore=morb(n)
  221 m=0
      do 225 n=1,7
      do 225 lp1=1,n
      if (mcore.eq.m) go to 230
      m=m+1
      nnn(m)=n
      l(m)=lp1-1
      i=l(m)+1
c     encode (3,22,nlbcd(m))  nnn(m),lbcd(i)
      write (nlbcd(m),22)  nnn(m),lbcd(i)
   22 format (i2,a1)
      wwnl(m)=4*lp1-2
      if (necore.eq.54.and.n.eq.4.and.l(m).eq.3)  m=m-1
      if (necore.eq.86.and.n.eq.5.and.l(m).eq.3)  m=m-1
      if (necore.eq.86.and.n.eq.5.and.l(m).eq.4)  m=m-1
      if (necore.eq.118.and.n.eq.5.and.l(m).eq.4)  m=m-1
      if (necore.eq.118.and.n.eq.6.and.l(m).eq.3)  m=m-1
      if (necore.eq.118.and.n.eq.6.and.l(m).eq.4)  m=m-1
      if (necore.eq.118.and.n.eq.6.and.l(m).eq.5)  m=m-1
  225 continue
c
c     decode and store n, l, and occupation nos. for subshells
c          outside of or which modify the core
c
  230 do 241 i=1,8
      if (nlbcd8(i)(1:3).eq.'   ') go to 241
      do 233 j=1,25
      if (wwnl8(i).ne.wwwnl(j)) go to 233
      ww8=j-1
      if (j.lt.16) go to 234
      ww8=j-16
      go to 234
  233 continue
      ww8=1
      if (ww8.eq.1.and.(wwnl8(i)(3:3).ne.' '.or.wwnl8(i)(2:2).eq.'.'
     1  .or.wwnl8(i)(1:1).eq.'.')) read (wwnl8(i),1023) ww8
 1023 format (f6.3)
      if (mod(ww8,1).ne.0) ifrac=1
  234 if (m.eq.0) go to 237
      m1=m
      do 236 j=1,m1
      if (nlbcd8(i).ne.nlbcd(j)) go to 236
      if (ww8.ne.0.or.j.lt.n1scf) go to 240
      m=m-1
      if (j.eq.m+1) go to 241
      do 235 mp=j,m
      nnn(mp)=nnn(mp+1)
      l(mp)=l(mp+1)
      nlbcd(mp)=nlbcd(mp+1)
  235 wwnl(mp)=wwnl(mp+1)
      go to 241
  236 continue
  237 if (ww8.eq.0.and.m+1.ge.n1scf) go to 241
      m=m+1
      j=m
      nlbcd(m)=nlbcd8(i)
c     decode (3,22,nlbcd(m))  nnn(m),li
      read (nlbcd(m),22)  nnn(m),li
      do 238 iii=1,21
      if (li.eq.lbcd(iii)) go to 239
  238 continue
      write (9,23)  m,nlbcd(m)
   23 format (7h0nlbcd(,i2, 4h) = ,a3,
     1  43h was not found in table.  next case please.  ///)
      go to 310
  239 l(m)=iii-1
      if (iii.gt.nnn(m)) go to 1241
  240 wwnl(j)=ww8
  241 continue
      ncspvs=m
      ncores=m-1
      if (m.gt.0) go to 242
 1241 write (9,24) iz,noelec,conf,alfmin,alfmax, (nlbcd8(m),wwnl8(m),
     1  m=1,8), ee8
   24 format (55h0ncspvs = 0  you have troubles with the following case-
     1  //2(2x,i3),3a6,2f5.2,8(a3,a6),0pf8.5//22h try the next element
     2  //1h1  )
      go to 310
c
c     accumulate no. of electrons, estimate eigenvalue for each orbital
c
  242 www=0.
      do 243 m=1,ncspvs
      www=www+wwnl(m)
      if (m.eq.ncores) ncelec=www
      eras=nnn(m)-l(m)
  243 a0m(m)=ca0/eras
      if (ee8.gt.0.0) www=ncelec
      noelec=www
      nvelec=noelec-ncelec
      ion=iz-noelec
      twoion=ion+ion
      zzz=ion+1
      twozzz=zzz+zzz
      n1sc=1
      if (eeo(ncspvs).lt.0.0.or.ee(ncspvs).lt.0.0.or.ee8.le.0.0)
     1  go to 265
      if (iz.ne.izo.or.ion.ne.iono+1) go to 265
      do 262 m=1,ncspvs
      if (m.eq.ncspvs) go to 262
      if (nlbcd(m).ne.nlbcdo(m).or.wwnl(m).ne.wwnlo(m)) go to 265
  262 continue
      n1sc=ncspvs
      kut=-1
  265 do 250 m=1,ncspvs
      if (n1sc.gt.1) go to 246
      smsig=0.
      if (iz.ne.izo) go to 244
      if (ion.ne.iono.and.m.ge.ncspvs-2) go to 244
      if (m.ge.ncspvs) go to 244
      if (nlbcd(m).eq.nlbcdo(m)) go to 246
  244 eras=l(m)
      sig1=0.6+0.05*eras
      do 245  mp=1,ncspvs
      eras=nnn(m)-nnn(mp)
      sig=sig1+0.2*eras
      if (sig.lt.0.0) go to 245
      if (sig.gt.1.0) sig=1.0
      wwnlx=wwnl(mp)
      if (nnn(m).eq.nnn(mp).and.l(m).eq.l(mp)) wwnlx=wwnlx-1.0
      smsig=smsig+sig*wwnlx
  245 continue
      if (nnn(m)+l(m).eq.1.and.wwnl(m).eq.2.0) smsig=0.5*smsig+0.3
      ee(m)=-((z-smsig)/float(nnn(m)))**2
  246 if (n1sc+n1scf.gt.2.and.m.lt.ncspvs) ee(m)=eeo(m)
      if (m.eq.ncspvs.and.ee8.gt.0.0) ee(m)=ee8
      if (itpow.lt.8.or.n1sc.gt.1) go to 250
      write (9,25) m,nnn(m),l(m),wwnl(m),nlbcd(m),wwnl(m),ee(m), smsig
   25 format (4h m =,i2, i3,i2,f7.3,3x,a3,f6.3,f14.5, 9h rydbergs, 5x,
     1 38hscreening of electron in mth orbital =  ,f9.5  )
  250 continue
      if (itpow.lt.8) go to 260
      write (9,26) mcore, necore, www
   26 format (8h0mcore =,i3, 3x,8hnecore =,i3, 8h   www =,f3.0  //  )
  260 do 268 m=1,ncspvs
      wwnlo(m)=wwnl(m)
  268 nlbcdo(m)=nlbcd(m)
      m=ncspvs
      if (ee(m).ne.0.0) go to 300
      fn=nnn(m)
      ee(m)=-(zzz/fn)**2
  300 return
  310 ierror=7
      return
      end
