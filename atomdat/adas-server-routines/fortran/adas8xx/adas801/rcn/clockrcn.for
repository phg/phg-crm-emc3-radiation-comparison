C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn/clockrcn.for,v 1.2 2004/07/06 12:06:53 whitefor Exp $ Date $Date: 2004/07/06 12:06:53 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Write out a tickmark following call to clock in 
C               order to update the progress bar during interactive
C               use. Add ibatch to /c7/
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine clockrcn
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      common/c7/t0,t1,t2, ibatch
c
      call seconds(t2)
      tr=t2-t1
      tj=t2-t0
      write (9,99) tr,tj
   99 format (//12h0    t(run)=,f6.2,1h,,5x,7ht(job)=,f7.2,8h seconds)
      
C------Start ADAS conversion-------

      if (ibatch.eq.0) write(6,*)ibatch

C------End ADAS conversion-------


      return
      end
