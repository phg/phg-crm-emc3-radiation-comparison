C-----------------------------------------------------------------------
C  Parameters for rcg of the cowan code.
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 17-06-2003
C  MODIFIED: Martin O'Mullane
C             - First version.
C  
C  VERSION : 1.2
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - Brought into line with offline v1.2
C               - Neutral Mo kpc 1500->2000.
C               - 4f kpc 2000->25000.
C               - 4f kmx 1000->1500.
C               - 4f klam 250000->300000.
C             - kiser 10000->20000
C             - added ksjk, dimension of nsjk. Was hardcoded to 110 in
C               rcg. Set to 200
C             - klam 300000->800000 to allow some complicated tin calcs
C             - kls4 9 -> 18
C  
C-----------------------------------------------------------------------
      parameter (ks=8)          ! who knows
      parameter (kc=300)        ! No of configs in each parity 
      parameter (klsp=3500)     ! who knows
      parameter (kjk=110)       ! who knows
      parameter (kjp=1000)      ! who knows
      parameter (kmx=1500)      ! matrix size (stop calcfc 226)
      parameter (kiser=20000)   ! who knows
      parameter (kpr=2100)      ! who knows
      parameter (kjjt=50)       ! who knows
      parameter (kpc=25000)     ! who knows (related to a stop prk70)
      parameter (kls1=319)      ! Complexity of subshell 1
      parameter (kls2=319)      ! who knows
      parameter (kls3=319)      ! who knows
      parameter (kls4=18)       ! who knows
      parameter (kls5=9)        ! who knows
      parameter (kls6=9)        ! Complexity of subshell i=7,8
      parameter (klam=800000)  ! number of spectral lines printed
      parameter (kw=104)        ! who knows
      parameter (kbgks=41)      ! who knows
      parameter (kenrgs=41)     ! number of energies to tabulate omega at
      parameter (kmult=100)     ! who knows 
      parameter (ksjk=200)      ! who knows
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C     derived parameters, change these at your own risk
C-----------------------------------------------------------------------
      parameter (klc42=kmx**2)
      parameter (kp=2*kw+2)
      parameter (kcc=kiser/4)
      parameter (klc4=2*kiser+1)
      parameter (kexc=kmx)
      parameter (klsp1=kls1+1)
      parameter (klc1=kmx**2-(kls1**2)*9)
      parameter (klsc=kls1+kls2+kls3+26)
      parameter (klc2=kmx**2-(kls1**2)*9-kpc)
      parameter (ktran=klam)    ! number of generalised f-values
                                ! need not be same but easier if they are. 
C-----------------------------------------------------------------------
