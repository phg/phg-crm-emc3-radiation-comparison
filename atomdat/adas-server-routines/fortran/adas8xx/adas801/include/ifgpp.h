C-----------------------------------------------------------------------
C  Parameters for rcg of the cowan code.
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 17-06-2003
C  MODIFIED: Martin O'Mullane
C             - First version.
C  
C  VERSION : 1.2
C  DATE    : 06-11-2003
C  MODIFIED: Martin O'Mullane
C             - Heavy Z calculations demand 250000 transitions.
C  
C  VERSION : 1.3
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - Even more transitions.
C               nterms 2800 -> 5000
C               ndlev  3000 -> 3500
C               ndqdn  6    -> 8
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C     Variable declaration
C-----------------------------------------------------------------------
      integer     ncfg    , nterms   ,  nrates  ,  ntemp  ,
     &            ndlev   , ndtrn    ,  ndqdn   ,  ndmet
      integer     nenergy , ndcom
C-----------------------------------------------------------------------
C     Set the parameters 
C-----------------------------------------------------------------------
      parameter (ncfg   = 20 )     ! configurations
      parameter (nterms = 5000 )   ! LS terms
      parameter (nrates = 21 )     ! collision strengths
      parameter (ntemp  = 14 )     ! output temperatures
      parameter (ndlev  = 3500 )   ! levels
      parameter (ndtrn  = 380000 ) ! transitions
      parameter (ndqdn  = 8 )      ! orbital energies
      parameter (ndmet  = 5 )      ! metastables
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C     Derived parameters, change these at your own risk
C-----------------------------------------------------------------------
      parameter ( nenergy = nterms*2  )
      parameter ( ndcom   = 5 + ndtrn/1000 )  ! number of untied levels 
                                              ! comment lines
C-----------------------------------------------------------------------
