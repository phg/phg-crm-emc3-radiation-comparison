C-----------------------------------------------------------------------
C     Parameters for rcn/rcn2 of the cowan code.
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - kcase 500 -> 2000
C
C-----------------------------------------------------------------------
      parameter (kmsh=1801)     ! number of radial mesh points
      parameter (ko=20)         ! maximum number of orbitals
      parameter (kmshq=1801)    ! number of radial mesh points
      parameter (koq=20)        ! maximum number of orbitals
      parameter (kbgks=41)      ! who knows
      parameter (kcase=2000)    ! who knows
      parameter (kco=100)       ! who knows
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C     derived parameters, change these at your own risk
C-----------------------------------------------------------------------
      parameter (km=kmsh)
      parameter (kp=kco+1)
C-----------------------------------------------------------------------
