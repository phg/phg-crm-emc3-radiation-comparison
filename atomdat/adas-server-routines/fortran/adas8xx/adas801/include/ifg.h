C-----------------------------------------------------------------------
C  Parameters for ifg  post processing of the Cowan code.
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 10-12-2008
C  MODIFIED: Adam Foster
C             - First commented version.
C
C  VERSION : 1.5
C  DATE    : 17-09-2008
C  MODIFIED: Adam Foster
C             - brought into line with version 1.4 of offline code.
C             - nLevels 450->1000
C  
C-----------------------------------------------------------------------
C     Variable declaration
C-----------------------------------------------------------------------
      integer nj,nlevels,ndenrgs,nmtp,nconfigs,ncfg,
     &          nSG,nTerms,nLSterms,nfull
C-----------------------------------------------------------------------
C     Parameters for ifg part of the cowan code.
C-----------------------------------------------------------------------
      parameter (nJ=16)         ! unique J values / parity
      parameter (nLevels=1000)   ! levels
      parameter (ndenrgs=25)    ! max energies fro RCG
      parameter (nmtp=300000)   ! who knows
      parameter (nconfigs=20)   ! who knows
      parameter (ncfg=8)        ! max number of groups in a configuration
      parameter (nSG=300)       ! spin groups
      parameter (nTerms=1200)   ! terms
      parameter (nLSterms=36)   ! don't touch this!
      parameter (nfull=4)       ! who knows
C-----------------------------------------------------------------------
       
C-----------------------------------------------------------------------
C     Notes:
C-----------------------------------------------------------------------
C     also look in orbital.for for info on expanding the maximum
C     handleable orbital
C-----------------------------------------------------------------------
