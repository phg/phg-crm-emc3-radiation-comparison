C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/zeta2.for,v 1.2 2004/07/06 15:41:32 whitefor Exp $ Date $Date: 2004/07/06 15:41:32 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase kcase to 500 (from 50).
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine zeta2
c
c          calculate spin-orbit configuration-interaction integrals
c
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/pi/ pa(km),pb(km),pc(km),pd(km)
c
      common/c0/nlz(4),ncfg(2),mmin,ncdes,nkk(4),l(4),
     1  m,iexch,iz1,idum0,overlp,x(km)
      dimension drudr(km),deru(km),v(km),zei(km)
c
      common/c1/fact(5),ifact(5),idummy2,dummy2(20)
      common/c3/icon,isli,idip,ic,icn(2),nconfn(2),nc0,icase,
     1  nsconf(3,2),del(kco),idipp,iovfact,ovfact
      common/cp/slitest,wnl(2),rm1(km),rmkp1(km)
      common/lcm1/nt2(ko),lt2(ko),wt2(ko),ncspvs,idum2,vpar(50),
     1  nconf,iz,z,ion,irel,hxid,c,mesh,idb,exf,corrf,kut,npar
      common/lcm1a/nlbcd
      common/lcm1b/nnlz(ko),nkkk(ko),ee(ko),ee8,r(km),ru(km),
     1  pnl(km,ko),npnl
      character dh*80,option*28,nlh(4)*3,ha*3,hb*3,hc*3,hd*3,ha1*3,
     1  hb1*3,he*3,nlbcd(ko)*8,nlht(8)*3,nlb(8,kco)*3,kst(kcase,2)*3
      character*6 conf(3),cst(kcase,6),conf1(3),conf2(3),
     1  cf1(kp),cf2(kp),cf3(kp),subr0*3,subr*3,calsq(5)*3,rj*1
      common/cdh/dh,option,nlh,ha,hb,hc,hd,ha1,hb1,he,nlht,nlb,kst,
     1  conf,cst,conf1,conf2,cf1,cf2,cf3,subr0,subr,calsq,rj
c     equivalence (p,pa),(p(1,2),pb),(p(1,3),pc),(p(1,4),pd),
c    1  (l(1),la),(l(2),lb),(l(3),lc),(l(4),ld),
c    2  (nlh(1),ha),(nlh(2),hb),(nlh(3),hc),(nlh(4),hd)
c
      nlh(1)=ha
      nlh(2)=hb
  111 if (subr.ne.subr0) go to 114
      if (ncfg(2).eq.nconf) go to 150
  114 subr0=subr
      write (9,10)
   10 format (11h1rcn mod 35///)
c
c
c          read wavefunctions from tape ic and select those desired
c
  150 n=1
      k=1
  205 ic=icn(n)
      if (ic.gt.30) k=ic-30
      if (ncfg(n).eq.nconf) go to 220
      if (ncfg(n)-nconfn(k)) 210,210,211
c     if (ncfg(n)-nconfn(k)) 210,206,211
c 206 backspace ic
c     go to 211
  210 rewind ic
  211 read (ic) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,ee,ee8,npnl,r,ru, ((pnl(i,m),i=1,km),m=1,npnl)
      if (nconf.ne.ncfg(n)) go to 211
      nconfn(k)=nconf
c
  220 do 221 i=1,3
      conf1(i)=conf2(i)
  221 conf2(i)=conf(i)
c
      do 222 m=1,ncspvs
      if (nlbcd(m)(1:3).eq.nlh(n)) go to 225
  222 continue
      go to 1000
  225 nkk(n)=nkkk(m)
      nlz(n)=nnlz(m)
c
      jj=m+npnl-ncspvs
      if (jj.le.0) go to 1000
      go to (1230,2230), n
 1230 do 1231 i=1,mesh
 1231 pa(i)=pnl(i,jj)
      go to 250
 2230 do 2231 i=1,mesh
 2231 pb(i)=pnl(i,jj)
  250 n=n+1
      if (n.ge.2) go to 350
      go to 205
c
  350 write (9,35) conf1,conf2,iz,kut
   35 format ( 1h ,3a6,5x,3a6,6x,2hz=,i3,6x,4hkut=,i2///
     1  6x,32hnl   nl  zeta(ryd)    zeta(cm-1) )
c
c          calculate v and derivative of ru
c
  400 dr3=3.0*(r(2)-r(1))
      kkk=min0(nkk(1),nkk(2))
      nblock=kkk/40
      i=1
      do 420 nb=1,nblock
      do 410 j=1,10
      i=i+4
      drudr(i-3)=(-5.5*ru(i-3)+9.0*ru(i-2)-4.5*ru(i-1)+ru(i))/dr3
      drudr(i-2)=(-ru(i-3)-1.5*ru(i-2)+3.0*ru(i-1)-0.5*ru(i))/dr3
      drudr(i-1)=(0.5*ru(i-3)-3.0*ru(i-2)+1.5*ru(i-1)+ru(i))/dr3
  410 drudr(i)=(-ru(i-3)+4.5*ru(i-2)-9.0*ru(i-1)+5.5*ru(i))/dr3
      if (i.gt.idb) go to 420
      dr3=dr3+dr3
  420 continue
c
      do 450 i=2,kkk
      v(i)=ru(i)/r(i)
  450 deru(i)=(drudr(i)-v(i))/(r(i)**2)
c
      do 520 i=2,kkk
      psq=pa(i)*pnl(i,jj)
  520 zei(i)=psq*deru(i)
c
c          integrate
c
      zei(1)=0.0
      xif=0.00125*c*5.0/288.0
      j=1
      s1z=0.0
  530 mm=8
      if (i.gt.idb) go to 531
      xif=xif+xif
  531 continue
      s2z=0.0
  532 s2z=s2z+19.0*(zei(j)+zei(j+5))+75.0*(zei(j+1)+zei(j+4))
     1       +50.0*(zei(j+2)+zei(j+3))
  533 j=j+5
      mm=mm-1
      if (mm) 534,534,532
  534 s1z=s1z+xif*s2z
      if (j-kkk) 530,535,535
  535 zeta=2.66246e-5*s1z
      zetak=zeta*109737.31
c
      write (9,70) ha,hb,zeta,zetak
   70 format (5x,a3,2x,a3,f11.5,f14.3////)
c
c
  900 return
c
 1000 write (9,1001) nlh(n),conf
 1001 format (//9h orbital ,a3, 23h not found for config  ,3a6)
       go to 900
      end
