C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/s3j_rcn2.for,v 1.2 2004/07/06 15:18:26 whitefor Exp $ Date $Date: 2004/07/06 15:18:26 $
C
      function s3j (fj1, fj2, fj3, fm1, fm2)
c
c          calculate 3-j symbol
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      s3j=0.0
      fm3=fm1+fm2
	  a=fj2+fj3+fm1
      b=fj1-fm1
      c=-fj1+fj2+fj3
      d=fj3+fm3
      e=fj1-fj2-fm3
      f=fj1-fj2+fj3
      g=fj1+fj2-fj3
      h=fj1+fj2+fj3+1.0
      e2=fctrl(b)*fctrl(fj1+fm1)*fctrl(fj2-fm2)*fctrl(fj2+fm2)
      e1=(fctrl(c)*fctrl(f)/fctrl(h))*fctrl(g)*fctrl(d)*fctrl(fj3-fm3)
      if (e1.le.0.0) go to 153
      e1=sqrt(e2)/sqrt(e1)
      if (e1) 153,153,150
  150 zero=0.0
      i1=max(zero,-e)
      i2=min(a,c,d)
      if (i2-i1) 153,151,151
  151 do 152 i=i1,i2
      fi=i
      e2=fctrl(fi)*fctrl(c-fi)*fctrl(d-fi)/fctrl(a-fi)
  152 s3j=s3j+(((-1.0)** mod (i,2))/e2)*fctrl(b+fi)/fctrl(e+fi)
      ieras=fj1+fm2+fm3
      s3j=s3j*((-1.0)**mod(ieras,2))/e1
  153 return
      end
