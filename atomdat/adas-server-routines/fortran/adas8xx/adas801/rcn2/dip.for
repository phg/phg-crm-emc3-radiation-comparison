C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/dip.for,v 1.2 2004/07/06 13:32:23 whitefor Exp $ Date $Date: 2004/07/06 13:32:23 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase kcase to 500 (from 50).
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine dip
c
c          calculate electric dipole or quadrupole radial integrals
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/pi/ pa(km),pb(km),pc(km),pd(km)
      save
c
      common/c0/nlz(4),ncfg(2),mmin,ncdes,nkk(4),l(4),
     1  m,iexch,iz1,idum0,overlp,x(km)
c
      common/c2/ex(kp),stn(kp),wt(kp),gamma(kp),q(kp),
     1  q2(kp),pdipsc(kp),rrsc(kp,12),ncd,ncpd,nfirst,ncont1,ipwr
      common/c3/icon,isli,idip,ic,icn(2),nconfn(2),nc0,icase,
     1  nsconf(3,2),del(kco),idipp,iovfact,ovfact
      common/cp/slitest,wnl(2),rm1(km),rmkp1(km)
      common/lcm1/nt2(ko),lt2(ko),wt2(ko),ncspvs,idum2,vpar(50),
     1  nconf,iz,z,ion,irel,hxid,c,mesh,idb,exf,corrf,kut,npar
      common/lcm1a/nlbcd
      common/lcm1b/nnlz(ko),nkkk(ko),ee(ko),ee8,r(km),ru(km),
     1  pnl(km,ko),npnl
      common/lcm2/rdips(kbgks,kcase),st(kcase,5)
      common/genosc/igen,nbigks,bigks(kbgks),ibk
      character dh*80,option*28,nlh(4)*3,ha*3,hb*3,hc*3,hd*3,ha1*3,
     1  hb1*3,he*3,nlbcd(ko)*8,nlht(8)*3,nlb(8,kco)*3,kst(kcase,2)*3
      character*6 conf(3),cst(kcase,6),conf1(3),conf2(3),
     1  cf1(kp),cf2(kp),cf3(kp),subr0*3,subr*3,calsq(5)*3,rj*1
      common/cdh/dh,option,nlh,ha,hb,hc,hd,ha1,hb1,he,nlht,nlb,kst,
     1  conf,cst,conf1,conf2,cf1,cf2,cf3,subr0,subr,calsq,rj
c     equivalence (p,pa),(p(1,2),pb),(p(1,3),pc),(p(1,4),pd),
c    1  (l(1),la),(l(2),lb),(l(3),lc),(l(4),ld),
c    2  (nlh(1),ha),(nlh(2),hb),(nlh(3),hc),(nlh(4),hd)
c
      nlh(1)=ha
      nlh(2)=hb
      ha1=ha
      itest=1
  100 if (subr.eq.subr0) go to 200
  101 subr0=subr
      if (igen.le.8) write (9,10)
   10 format (1h1//19h  z  kut           ,7x,5hconf1,15x,5hconf2,8x,
     1  10hr integral,6x,22hreduced mupole element,7x,4hfrac,4x,
     2  8h ovfact ,12h  over   fr1/
     3  19h --- --- ---- ---- ,20h------------------- ,
     4  20h------------------- ,10h----------,3x,14h--------------,
     5  14h--------------,3x,6h------,2x, 9h---------,12h ----- -----/)
c     go to 200
c
c 150 if (ncfg(1).ne.ncfg1.or.ha.ne.ha1) go to 200
c     n=2
c     go to 251
c
c          read wavefunctions from tape ic and select those desired
c
  200 n=1
      ic=icn(1)
      ha1=ha
      k=1
      write (9,20)
   20 format (1h )
  205 if (ic.gt.30) k=ic-30
      if (ncfg(n).eq.nconf) go to 220
      if (ncfg(n)-nconfn(k)) 210,210,211
c     if (ncfg(n)-nconfn(k)) 210,206,211
c 206 backspace ic
c     go to 211
  210 rewind ic
  211 read (ic) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,ee,ee8,npnl,r,ru, ((pnl(i,m),i=1,km),m=1,npnl)
      if (nconf.ne.ncfg(n)) go to 211
      nconfn(k)=nconf
c
  220 if (n.gt.1) go to 223
  221 do 222 i=1,3
  222 conf1(i)=conf(i)
      go to 225
  223 do 224 i=1,3
  224 conf2(i)=conf(i)
  225 if (mmin.lt.0) go to 250
      do 226 m=1,ncspvs
      if (nlbcd(m)(1:3).eq.nlh(n)) go to 230
  226 continue
      go to 1000
c
  230 nkk(n)=nkkk(m)
      nlz(n)=nnlz(m)
      wnl(n)=wt2(m)
      j=m+npnl-ncspvs
      if (j.le.0) go to 1000
      go to (1230,2230), n
 1230 la=nnlz(m)-100*(nnlz(m)/100)
      do 1231 i=1,mesh
 1231 pa(i)=pnl(i,j)
      go to 250
 2230 lb=nnlz(m)-100*(nnlz(m)/100)
      do 2231 i=1,mesh
 2231 pb(i)=pnl(i,j)
c
  250 n=n+1
  251 ic=icn(2)
      if (n-2) 200,205,300
c
c          begin integration loop
c
  300 pwrt=2*ipwr+1
      pwrt=sqrt(pwrt)
      do 450 ibk=1,nbigks
      if (mmin.ge.0) go to 305
c
  500 ha='   '
      hb='   '
      ha1='   '
      dipole=0.0
      reddip=0.0
      frac=0.0
      overlp=0.0
      frac1=0.0
      go to 410
c	
  305 dipole=0.0
      absdip=0.0
      rpwr=0.
      nf=-1
      overlp=0.0
      absovr=0.0
      imax=min0(nkk(1),nkk(2))
      do 399 i=1,imax
      rip=r(i)
      if (i.eq.1) go to 320
      rpwr=rip**ipwr
      if (igen.lt.5) go to 320
c          calculate sperical Bessel function
      bigk=bigks(ibk)
      bigk2=bigk**2
      call sbess(rip,itest,ipwr,bigk2,sz,szp)
      rpwr=pwrt*sz/(rip*bigk2)
c
  320 continue
      if (nf) 340,350,360
  340 eras=1.5
      if (i-1) 342,341,342
  341 eras=1.0
      go to 344
  342 if (i-imax) 344,343,344
  343 eras=0.5
  344 eras1=pa(i)*pb(i)*eras
      eras=rpwr*eras1
      dipole=(dipole+eras)*0.5
      absdip=(absdip+abs(eras))*0.5
      overlp=0.5*(overlp+eras1)
      absovr=0.5*(absovr+abs(eras1))
      nf=0
      nocyc=40
      if (i.ge.idb) nocyc=imax-i
      go to 399
c
  350 eras1=2.0*pa(i)*pb(i)
      eras=rpwr*eras1
      dipole=dipole+eras
      absdip=absdip+abs(eras)
      overlp=overlp+eras1
      absovr=absovr+abs(eras1)
      nf=1
      nocyc=nocyc-2
      if (nocyc) 399,351,399
  351 nf=-1
      go to 399
c
  360 eras1=pa(i)*pb(i)
      eras=rpwr*eras1
      dipole=dipole+eras
      absdip=absdip+abs(eras)
      overlp=overlp+eras1
      absovr=absovr+abs(eras1)
      nf=0
c
  399 continue
c
c          complete calculation of integral
c
  400 frac=dipole/absdip
      frac1=overlp/absovr
      dipole=dipole*(r(imax)-r(imax-1))/0.75
      overlp=overlp*(r(imax)-r(imax-1))/0.75
      if (ipwr.eq.0) dipole=dipole-overlp/bigk
      dipole=dipole*ovfact
      a=la
      b=lb
      c=ipwr
      zero=0.0
      reddip=((-1.0)**la)*sqrt((2.0*a+1.0)*(2.0*b+1.0))
     1  *s3j(a,b,c,zero,zero)*dipole
      pdipsc(ncpd)=reddip
      lgr=max0(la,lb)
      flgr=lgr
  410 if (igen.lt.5) go to 420
      if ((igen.le.5.and.reddip.ne.0.0).or.(igen.le.8.and.ibk.eq.1))
     1  go to 420
      go to 430
  420 write (9,41) iz,kut,conf1,conf2,
     1  dipole,  ha,rj,ipwr,hb, reddip , frac, ovfact,overlp,frac1
   41 format (1x,2i3,11x,    2(1x,3a6,1x), f11.6, 3x, 1h(,a3,
     1  2h//,a1,i1,2h//,a3, 2h)=, f10.5, 3h a0, f9.4, f11.6,2f6.3)
  430 if (ncfg(2).ge.ncont1+nc0) reddip=0.0
      iover=100.4*overlp
      if1=100.4*frac1
      if (igen.ge.5) go to 440
      if (idip.ne.8) go to 432
      frac=1.0
      go to 435
  432 if (idip.lt.7) go to 435
      i=ncfg(2)-nc0
      reddip=del(i)*reddip
      if (idip.lt.9) frac=del(i)
      write (9,42) del(i),reddip
   42 format (30x,9h sqrtdel=,f10.4,38x,f10.5)
  435 if (ncont1.eq.10000) go to 450
      if (frac.ge.0.0.and.frac.lt.9.99) go to 437
      write (11,43) conf1,conf2,reddip,ha,ipwr,hb,frac,hxid,iover,if1
   43 format (3a6,2x,2a6,a4,f14.7,1h(,a3,3h//r,i1,2h//,a3,1h),
     1  f6.3,a2,2i4)
      go to 450
  437 write (11,44) conf1,conf2,reddip,ha,ipwr,hb,frac,hxid,iover,if1
   44 format (3a6,2x,2a6,a4,f14.7,1h(,a3,3h//r,i1,2h//,a3,1h),
     1  f6.4,a2,2i4)
      go to 450
c          store results in Bessel-function case
  440 if (ibk.gt.1) go to 449
      icase=icase+1
      if (icase.gt.kcase) stop 44
      do 447 icf=1,3
      cst(icase,icf)=conf1(icf)
  447 cst(icase,icf+3)=conf2(icf)
      st(icase,1)=ipwr
      st(icase,2)=frac
      st(icase,3)=hxid
      st(icase,4)=iover
      st(icase,5)=if1
      kst(icase,1)=ha
      kst(icase,2)=hb
c
  449 rdips(ibk,icase)=reddip
  450 continue
      return
c
 1000 write (9,1001) nlh(n),conf
 1001 format (//9h orbital ,a3, 23h not found for config  ,3a6)
      return
c
      end
