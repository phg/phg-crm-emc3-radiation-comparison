C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/rcn2.for,v 1.2 2004/07/06 14:44:08 whitefor Exp $ Date $Date: 2004/07/06 14:44:08 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase inf to 80 characters.
C               Change opening of files.
C               Increase kcase to 500 (from 50).
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      program rcn2
c     program rcn2(in2,tape10=in2,out2,tape9=out2,
c    1  tape2n,tape2=tape2n,out2ing,tape11=out2ing,tape31,tape32)
c
c     this program copyright 1980 by los alamos national laboratory
c          and the u.s. government.  the program may be freely
c          distributed gratis anywhere in the world, but may not be sold.
c
c
c          This program reads radial wavefunctions from tape (or disk)
c               unit number 2 prepared by program rcn or hf, and (using
c               input option g5inp) calculates configuration-interaction
c               parameters (rk) and radial electric multipole integrals,
c               scales the energy-level parameter values (fk, gk, zeta,
c               and rk), and punches complete input card decks for
c               program rcg mod 5, 6, 7, 8, or 9.
c          option to include overlap integrals of spectator electrons
c               in computed configuration-interaction and electric-
c               multipole integrals added at the zeeman laboratory,
c               december 1987.
c
c          this version, easily adaptable to the vax 8200,
c               made at lund university march 1988,
c                    modified at lanl august-december 1988.
c          Version 36.0.1 November 1997.  Changed subr, subr0, calsq,
c               and rj from Hollarith to character strings, with
c               associated minor coding changes.  Also changed "save no"
c               in sli2 to simply "save" and made changes near the
c               "do 425" in g5inp to correct bugs in calculation of
c               CI integrals and dipole integrals, respectively.
c          Version 36.1.2 (June 1998).  All equivalence statements
c               removed to be consistent with FORTRAN 90.
c          Version 36.1.4 (Dec 1998).  Blank commons changed to labeled.
c               Also, multipole-integral formats changed to give
c               more significant figures.
c          Version 36.1.5 (Jan 1999).  Further corrections regarding the
c               CI problem of November 1997.
c          Version 36.1.6 (May 1999).  Removed numerous unused variables.
c          Version 36.1.7 (September 1999).  In subroutine g5inp, define
c               dmin and iprint to be character*5.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
c
      common/c0/nlz(4),ncfg(2),mmin,ncdes,nkk(4),l(4),
     1  m,iexch,iz1,idum0,overlp,x(km)
c
      common/c1/fact(5),ifact(5),idummy2,dummy2(20)
      common/c2/ex(kp),stn(kp),wt(kp),gamma(kp),q(kp),
     1  q2(kp),pdipsc(kp),rrsc(kp,12),ncd,ncpd,nfirst,ncont1,ipwr
      common/c3/icon,isli,idip,ic,icn(2),nconfn(2),nc0,icase,
     1  nsconf(3,2),del(kco),idipp,iovfact,ovfact
      common/c4/irho,isig,irhop,isigp,n12,idum4,t0
      common/cp/slitest,wnl(2),rm1(km),rmkp1(km)
      common/lcm1/nt2(ko),lt2(ko),wt2(ko),ncspvs,idum2,vpar(50),
     1  nconf,iz,z,ion,irel,hxid,c,mesh,idb,exf,corrf,kut,npar
      common/lcm1a/nlbcd
      common/lcm1b/nnlz(ko),nkkk(ko),ee(ko),ee8,r(km),ru(km),
     1  pnl(km,ko),npnl
      common/genosc/igen,nbigks,bigks(kbgks),ibk
      character dh*80,option*28,nlh(4)*3,ha*3,hb*3,hc*3,hd*3,ha1*3,
     1  hb1*3,he*3,nlbcd(ko)*8,nlht(8)*3,nlb(8,kco)*3,kst(kcase,2)*3
      character*6 conf(3),cst(kcase,6),conf1(3),conf2(3),
     1  cf1(kp),cf2(kp),cf3(kp),subr0*3,subr*3,calsq(5)*3,rj*1
      common/cdh/dh,option,nlh,ha,hb,hc,hd,ha1,hb1,he,nlht,nlb,kst,
     1  conf,cst,conf1,conf2,cf1,cf2,cf3,subr0,subr,calsq,rj
c     equivalence (p,pa),(p(1,2),pb),(p(1,3),pc),(p(1,4),pd),
c    1  (l(1),la),(l(2),lb),(l(3),lc),(l(4),ld),
c    2  (nlh(1),ha),(nlh(2),hb),(nlh(3),hc),(nlh(4),hd)	
      character*80 inf
c
c
c          open files
c               (set iii=0 to name files interactively, for vax)
c               (set iii=1 to use standard file names, for vax)
c               (set iii>1 and uncomment program card at
c                     start of program for cray and cdc computers)
c

C------Start ADAS conversion-------
     
     
      common/c7/is_batch

      
      iii=2
      
      read(5,*)is_batch
      
      read(5,'(A)')inf
      open(10,file=inf,status='old')
      read(5,'(A)')inf
      open(2,file=inf,status='old',form='unformatted')
      
      read(5,'(A)')inf
      open(9,file=inf,status='unknown')
      read(5,'(A)')inf
      open(11,file=inf,status='unknown')
      
      
      open(31,status='scratch',form='unformatted')
      open(32,status='scratch',form='unformatted')
      
      iii=2


C------End ADAS conversion-------

      if (iii.eq.1) go to 50
      if (iii.gt.1) go to 100
c
      write(6,*) '  input file name  '
      read(5,11) inf
   11 format(a)
      open(10,file=inf,status='old')
      write(6,*) '  output file name  '
      read(5,11) inf
      open(9,file=inf,status='unknown')
      write(6,*) '  give file name for wavefunc. file '
      read(5,11) inf
      open(2,file=inf,status='old',form='unformatted')
      write(6,*) '  give file name for rcg input file '
      read(5,11) inf
      open(11,file=inf,status='unknown')
      go to 55
c
c          use these open statements for standard file names
c               (set iii=1 on statement above)
c
   50 open(10,file='in2',status='old')
      open(9,file='out2',status='unknown')
      open(2,file='tape2n',status='old',form='unformatted')
      open(11,file='out2ing',status='unknown')
   55 open(31,status='scratch',form='unformatted')
      open(32,status='scratch',form='unformatted')
c
  100 rewind 2
      ic=2
      icn(1)=2
      icn(2)=2
      nconf=99
      nconfn(1)=99
      nconfn(2)=99
      subr0='   '
      slitest=-99.0
      nbigks=1
      igen=0
      n12=1
      ncont1=10000
      nc0=1
      do 120 i=1,kco
  120 del(i)=1.0
c
  200 read (10,19) dh
   19 format (a)
c     decode (3,20,dh) subr
      read (dh,20) subr
   20 format (a3)
      if (subr.eq.calsq(5)) go to 800
c     decode(45,21,dh) subr,iz1,ncfg(1),ncfg(2),ha,hb,hc,hd,mmin,iovfact
      read (dh,21) subr,iz1,ncfg(1),ncfg(2),ha,hb,hc,hd,mmin,iovfact
   21 format (a3,4x,i3,2i5, 4(2x,a3), 2i5)
      ipwr=1
      if (iovfact.lt.0) iovfact=0
      if (iovfact.eq.0) ovfact=1.0
      if (iz1.lt.0) write (11,22)
   22 format (5h   -1)
      if (iz1.ge.0) go to 250
      do 240 i=9,11
      if (i.eq.10) go to 240
      end file i
      rewind i
  240 continue

C------Start ADAS conversion-------
     
C      stop '(normal exit)'
c      return
	 stop
        
C------End ADAS conversion-------

  250 do 300 i=1,5
      if(subr.eq.calsq(i)) go to (500,550,600,700,800) i
  300 continue
      go to 200
  500 call dip
      go to 200
  550 call over
      go to 200
  600 call sli2
      go to 200
  700 call zeta2
      go to 200
  800 call g5inp
      write (9,80)
   80 format (//15h0finished g5inp/)
      go to 200
      end
