C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/over.for,v 1.2 2004/07/06 14:27:06 whitefor Exp $ Date $Date: 2004/07/06 14:27:06 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase kcase to 500 (from 50).
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine over
c
c          evaluate overlap integrals
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/pi/ pa(km),pb(km),pc(km),pd(km)
c
      common/c0/nlz(4),ncfg(2),mmin,ncdes,nkk(4),l(4),
     1  m,iexch,iz1,idum0,overlp,x(km)
c
      common/c3/icon,isli,idip,ic,icn(2),nconfn(2),nc0,icase,
     1  nsconf(3,2),del(kco),idipp,iovfact,ovfact
      common/cp/slitest,wnl(2),rm1(km),rmkp1(km)
      common/lcm1/nt2(ko),lt2(ko),wt2(ko),ncspvs,idum2,vpar(50),
     1  nconf,iz,z,ion,irel,hxid,c,mesh,idb,exf,corrf,kut,npar
      common/lcm1a/nlbcd
      common/lcm1b/nnlz(ko),nkkk(ko),ee(ko),ee8,r(km),ru(km),
     1  pnl(km,ko),npnl
      character dh*80,option*28,nlh(4)*3,ha*3,hb*3,hc*3,hd*3,ha1*3,
     1  hb1*3,he*3,nlbcd(ko)*8,nlht(8)*3,nlb(8,kco)*3,kst(kcase,2)*3
      character*6 conf(3),cst(kcase,6),conf1(3),conf2(3),
     1  cf1(kp),cf2(kp),cf3(kp),subr0*3,subr*3,calsq(5)*3,rj*1
      common/cdh/dh,option,nlh,ha,hb,hc,hd,ha1,hb1,he,nlht,nlb,kst,
     1  conf,cst,conf1,conf2,cf1,cf2,cf3,subr0,subr,calsq,rj
c     equivalence (p,pa),(p(1,2),pb),(p(1,3),pc),(p(1,4),pd),
c    1  (l(1),la),(l(2),lb),(l(3),lc),(l(4),ld),
c    2  (nlh(1),ha),(nlh(2),hb),(nlh(3),hc),(nlh(4),hd)
c
      nlh(1)=ha
      nlh(2)=hb
      k=1
      if (subr.eq.'   ') go to 105
  100 if (subr.eq.subr0) go to 150
  101 write (9,10)
   10 format (1h1)
  105 subr0=subr
      if (isli.gt.0) go to 200
      write (9,11)
   11 format (1h0//15h  z  kut       ,9x,5hconf1,17x,5hconf2,15x,
     1  16hoverlap integral,11x,8hfraction/17h --- ---         ,
     2  22h-------------------   ,23h-------------------    ,
     3  24h------------------------,7x,8h--------/)
      go to 200
c
  150 if (ncfg(1).ne.nconfn(k).or.ha.ne.ha1) go to 200
      if (iovfact.gt.2) go to 200
      n=2
      go to 251
c
c          read wavefunctions from tape ic and select those desired
c
  200 n=1
      ha1=ha
      if (isli.gt.0) go to 205
      write (9,20)
   20 format (1h )
  205 ic=icn(n)
      if (ic.gt.30) k=ic-30
      if (ncfg(n).eq.nconf) go to 220
      if (ncfg(n)-nconfn(k)) 210,210,211
c     if (ncfg(n)-nconfn(k)) 210,206,211
c 206 backspace ic
c     go to 211
  210 rewind ic
  211 read (ic) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,ee,ee8,npnl,r,ru, ((pnl(i,m),i=1,km),m=1,npnl)
      if (nconf.ne.ncfg(n)) go to 211
      nconfn(k)=nconf
c
  220 if (n.gt.1) go to 223
  221 do 222 i=1,3
  222 conf1(i)=conf(i)
      go to 225
  223 do 224 i=1,3
  224 conf2(i)=conf(i)
  225 do 226 m=1,ncspvs
      if (nlbcd(m)(1:3).eq.nlh(n)) go to 230
  226 continue
      go to 1000
c
  230 nkk(n)=nkkk(m)
      nlz(n)=nnlz(m)
      wnl(n)=wt2(m)
      j=m+npnl-ncspvs
      if (j.le.0) go to 1000
      if (ha.eq.hb.and.nnlz(m).ge.9900) go to 260
      go to (1230,2230), n
 1230 do 1231 i=1,mesh
 1231 pa(i)=pnl(i,j)
      go to 250
 2230 do 2231 i=1,mesh
 2231 pb(i)=pnl(i,j)
c
  250 n=n+1
  251 if (n-2) 200,205,300
c          set overlp=1.0 for continuum electrons
  260 overlp=1.0
      absovr=1.0
      go to 400
c
c          begin integration loop
c
  300 nf=-1
      overlp=0.0
      absovr=0.0
      imax=min0(nkk(1),nkk(2))
      do 399 i=1,imax
      if (nf) 340,350,360
  340 eras=1.5
      if (i-1) 342,341,342
  341 eras=1.0
      go to 344
  342 if (i-imax) 344,343,344
  343 eras=0.5
  344 eras=pa(i)*pb(i)*eras
      overlp=(overlp+eras)*0.5
      absovr=(absovr+abs(eras))*0.5
      nf=0
      nocyc=40
      if (i.ge.idb) nocyc=imax-i
      go to 399
c
  350 eras=pa(i)*pb(i)
      eras=eras+eras
      overlp=overlp+eras
      absovr=absovr+abs(eras)
      nf=1
      nocyc=nocyc-2
      if (nocyc) 399,351,399
  351 nf=-1
      go to 399
c
  360 eras=pa(i)*pb(i)
      overlp=overlp+eras
      absovr=absovr+abs(eras)
      nf=0
c
  399 continue
c
c          complete calculation of integral
c
  400 frac=overlp/absovr
      overlp=overlp*(r(imax)-r(imax-1))/0.75
      if (isli.gt.0) go to 450
  410 write (9,41) iz,kut,conf1,conf2,ha,hb,overlp,frac
   41 format (2i4,6x, 2(3x,3a6,1x), 5h    (,a3,1h/,a3,2h)=,f14.9,f15.6)
  450 return
c
 1000 write (9,1001) nlh(n),conf
 1001 format (//9h orbital ,a3, 23h not found for config  ,3a6)
      go to 450
c
      end
