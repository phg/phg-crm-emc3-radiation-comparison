C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/sli2.for,v 1.2 2004/07/06 15:20:09 whitefor Exp $ Date $Date: 2004/07/06 15:20:09 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase kcase to 500 (from 50).
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine sli2
c
c          calculate configuration-interaction coulomb integrals rk
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      common/pi/ pa(km),pb(km),pc(km),pd(km)
      save
c
      common/c0/nlz(4),ncfg(2),mmin,ncdes,nkk(4),l(4),
     1  m,iexch,iz1,idum0,overlp,x(km)
      dimension xi(km),xj(km),xa(km),xb(km),f1(km),f2(km),rkp1(km)
      common/c1/fact(5),ifact(5),nrk,rkaysc(20)
      common/c2/ex(kp),stn(kp),wt(kp),gamma(kp),q(kp),
     1  q2(kp),pdipsc(kp),rrsc(kp,12),ncd,ncpd,nfirst,ncont1,ipwr
      common/c3/icon,isli,idip,ic,icn(2),nconfn(2),nc0,icase,
     1  nsconf(3,2),del(kco),idipp,iovfact,ovfact
      common/c4/irho,isig,irhop,isigp,n12,idum4,t0
      common/cp/slitest,wnl(2),rm1(km),rmkp1(km)
      common/lcm1/nt2(ko),lt2(ko),wt2(ko),ncspvs,idum2,vpar(50),
     1  nconf,iz,z,ion,irel,hxid,c,mesh,idb,exf,corrf,kut,npar
      common/lcm1a/nlbcd
      common/lcm1b/nnlz(ko),nkkk(ko),ee(ko),ee8,r(km),ru(km),
     1  pnl(km,ko),npnl
      common/lcm3/pe(km),wa1,wb1
      character dh*80,option*28,nlh(4)*3,ha*3,hb*3,hc*3,hd*3,ha1*3,
     1  hb1*3,he*3,nlbcd(ko)*8,nlht(8)*3,nlb(8,kco)*3,kst(kcase,2)*3
      character*6 conf(3),cst(kcase,6),conf1(3),conf2(3),
     1  cf1(kp),cf2(kp),cf3(kp),subr0*3,subr*3,calsq(5)*3,rj*1
      common/cdh/dh,option,nlh,ha,hb,hc,hd,ha1,hb1,he,nlht,nlb,kst,
     1  conf,cst,conf1,conf2,cf1,cf2,cf3,subr0,subr,calsq,rj
c     equivalence (p,pa),(p(1,2),pb),(p(1,3),pc),(p(1,4),pd),
c    1  (l(1),la),(l(2),lb),(l(3),lc),(l(4),ld),
c    2  (nlh(1),ha),(nlh(2),hb),(nlh(3),hc),(nlh(4),hd)
c
      dimension k1(10,2),rryd(10,2),rkay(10,2),frac(10,2),eav(2),eps(2)
      dimension rr1(10,2,3),rk1(10,2,3),kk(4),ww(4),ncfgo(2),nn(4)
c     equivalence (kk(1),ka),(kk(2),kb),(kk(3),kc),(kk(4),kd),
c    1            (ww(1),wa),(ww(2),wb),(ww(3),wc),(ww(4),wd)
c
c
      if (slitest.eq.(-99.0)) ncfgo(1)=-7
      if (slitest.eq.(-99.0)) ncso=-7
      if (ncfg(1).ne.ncfgo(1).or.ncfg(2).ne.ncfgo(2)) itest=0
      ncs=1000*ncfg(1)+ncfg(2)
      if (isli.eq.0) go to 110
      write (9,95)
      go to 120
  110 write (9,10)
   10 format (11h1rcn mod 36//)
c          determine whether interaction is class 1b,7, or 8
c
  120 iclass=0
      la1=la
      lb1=lb
c
      do 125 i=1,4
      if (i.eq.1) read (ha,12) la
      if (i.eq.2) read (hb,12) lb
      if (i.eq.3) read (hc,12) lc
      if (i.eq.4) read (hd,12) ld
  125 continue
   12 format (2x,a1)
      if (ha.eq.hc) go to 150
      if (hb.eq.hd) go to 130
      if (hb.eq.hc.or.ha.eq.hd) go to 140
      go to 150
  130 if (la.ne.lc.or.hb(1:2).eq.'99') go to 150
      he=hb
      hb=ha
      ha=he
      he=hd
      hd=hc
      hc=he
      iclass=8
      if (ha.eq.hb.or.hc.eq.hd) iclass=1
      go to 148
  140 if (la.ne.ld.or.lb.ne.lc) go to 150
      iclass=7
      if (ha.eq.hd) go to 145
      he=hb
      hb=ha
      ha=he
      go to 148
  145 he=hd
      hd=hc
      hc=he
  148 n12=1
c
c          read wavefunctions from tape ic and select those desired
c
  150 nlh(1)=ha
      nlh(2)=hb
      nlh(3)=hc
      nlh(4)=hd
      if (ncfg(2).gt.ncfg(1)+1.and.iclaso.eq.7) n12=1
      n=n12
      npos=0
      if (n12.eq.2) npos=npos1
      la=la1
      lb=lb1
      wa=wa1
      wb=wb1
      eee=0.0
      if (n12.eq.2) eee=ee1
      call seconds(t1)
      iread=0
  151 if (ncfg(n12)-nconf) 160,220,170
  160 rewind ic
  170 read (ic) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,ee,ee8,npnl,r,ru, ((pnl(i,m),i=1,km),m=1,npnl)
      eav(n)=vpar(1)
      eps(n)=ee(ncspvs)
      iread=iread+1
      if (nconf.ne.ncfg(n)) go to 170
      k=1
      if (ic.gt.30) k=ic-30
      nconfn(k)=nconf
c
  220 do 221 i=1,3
      if (n12.eq.1) conf1(i)=conf2(i)
  221 conf2(i)=conf(i)
c
      do 229 j=1,2
      k=2*n-2+j
      do 222 m=1,ncspvs
      if (nlbcd(m)(1:3).eq.nlh(k)) go to 225
  222 continue
      go to 1000
  225 nkk(k)=nkkk(m)
      nlz(k)=nnlz(m)
      nn(k)=nt2(m)
      kk(k)=m
      ip=mesh
      jj=m+npnl-ncspvs
      if (jj.le.0) go to 1000
      go to (1225,2225,3225,4225), k
 1225 la=nnlz(m)-100*(nnlz(m)/100)
      ww(k)=wt2(m)
      wa=ww(k)
      do 1226 i=1,ip
 1226 pa(i)=pnl(i,jj)
      go to 229
 2225 lb=nnlz(m)-100*(nnlz(m)/100)
      ww(k)=wt2(m)
      wb=ww(k)
      do 2226 i=1,ip
 2226 pb(i)=pnl(i,jj)
      go to 229
 3225 lc=nnlz(m)-100*(nnlz(m)/100)
      ww(k)=wt2(m)
      do 3226 i=1,ip
 3226 pc(i)=pnl(i,jj)
      go to 229
 4225 ld=nnlz(m)-100*(nnlz(m)/100)
      ww(k)=wt2(m)
      wd=ww(k)
      do 4226 i=1,ip
 4226 pd(i)=pnl(i,jj)
  229 continue
      m=kk(2*n)
      if (nt2(m).ge.99) npos=npos+1
      if (n.eq.1) npos1=npos
      eee=eee+0.5*ee(m)
      if (isli.le.0) write (9,22) ee(m),eee
   22 format (8h ee,eee=,2f10.5)
c
      n=n+1
      wa1=wa
      wb1=wb
      if (n.gt.2) go to 250
      ee1=eee
  230 if (ncfg(2)-nconf) 160,231,170
  231 hc=ha
      hd=hb
      go to 220
c
c          determine whether interaction is class 1a,6, or 11
c
  250 if (iclass.ne.0) go to 300
      if (ha.ne.hc) go to 300
      if (lb.ne.ld) go to 300
      if ((wb+wd).eq.2.0) iclass=11
      if (isig.ne.isigp) iclass=6
      if (ha.eq.hb.or.hc.eq.hd) iclass=1
c
c          set up k tables for direct (d) and exchange (e) integrals
c
  300 kdmin=max0(iabs(la-lc),iabs(lb-ld))
      kemin=max0(iabs(la-ld),iabs(lb-lc))
      kdmax=min0(la+lc, lb+ld)
      kemax=min0(la+ld, lb+lc)
      nokd=(kdmax-kdmin)/2+1
      noke=(kemax-kemin)/2+1
      nokmax=max0(nokd,noke)
      call seconds(t2)
c
c          calculate integration limits
c
      i1max=max0( min0(nkk(1),nkk(3)), min0(nkk(1),nkk(4)) )
      i2max=max0( min0(nkk(2),nkk(4)), min0(nkk(2),nkk(3)) )
      imax=max0(i1max,i2max)
c
c          calc pnl-products for direct or exchange case
c
  400 do 700 n1=1,2
      if (n1-1) 420,420,412
  412 do 415 i=1,imax
      f1(i)=pa(i)*pd(i)
  415 f2(i)=pb(i)*pc(i)
      k=kemin-2
      go to 500
  420 do 425 i=1,imax
      f1(i)=pa(i)*pc(i)
  425 f2(i)=pb(i)*pd(i)
      k=kdmin-2
c
c          calc for each value of k
c
  500 do 700 n=1,nokmax
      k1(n,n1)=0
  520 k=iabs(k+2)
      k1(n,n1)=k
c
      if (mmin.ge.0) go to 522
      a1=0.0
      b1=1.0
      iclass=0
      go to 650
  522 rkp1(1)=0.001
      rm1(1)=1.0/r(1)
      do 530 i=2,imax
      rm1(i)=1.0/r(i)
      if (n-1) 525,525,529
  525 rkp1(i)=r(i)
      if (k.eq.0) go to 530
  526 do 527 m=1,k
  527 rkp1(i)=rkp1(i)*r(i)
      go to 530
  529 rkp1(i)=rkp1(i)*(r(i)**2)
  530 rmkp1(i)=1.0/rkp1(i)
c
c          calc r1 integral
c
      xi(1)=0.0
      xj(1)=0.0
      xa(1)=0.0
      xb(1)=0.0
      a2=0.0
      b2=0.0
      ho12=r(2)/12.0
      ni=40
      do 550 i=3,i1max,2
      a0=a2
      b0=b2
      eras=8.0*f1(i-1)
      a1=eras*rkp1(i-1)*rm1(i-1)
      b1=eras*rmkp1(i-1)
      a2=f1(i)*rkp1(i)*rm1(i)
      b2=f1(i)*rmkp1(i)
      eras=ho12*(5.0*a0+a1-a2)
      xi(i-1)=xi(i-2)+eras
      xa(i-1)=xa(i-2)+abs(eras)
      eras=ho12*(5.0*b0+b1-b2)
      xj(i-1)=xj(i-2)+eras
      xb(i-1)=xb(i-2)+abs(eras)
      eras=ho12*(-a0+a1+5.0*a2)
      xi(i)=xi(i-1)+eras
      xa(i)=xa(i-1)+abs(eras)
      eras=ho12*(-b0+b1+5.0*b2)
      xj(i)=xj(i-1)+eras
      xb(i)=xb(i-1)+abs(eras)
      ni=ni-2
      if (ni) 540,540,550
  540 ho12=ho12+ho12
      ni=40
      if (i.ge.idb) ni=10000
  550 continue
c
      do 560 i=2,i1max
      eras=rkp1(i)*rm1(i)
      xi(i)=xi(i)*rmkp1(i)+(xj(i1max)-xj(i))*eras
  560 xa(i)=xa(i)*rmkp1(i)+(xb(i1max)-xb(i))*eras
      if (i2max-i1max) 600,600,569
  569 a0=xi(i1max)*rkp1(i1max)
      b0=xa(i1max)*rkp1(i1max)
      do 570 i=i1max,i2max
      xi(i)=a0*rmkp1(i)
  570 xa(i)=b0*rmkp1(i)
c
c          calc r2 integral
c
  600 a0=0.0
      b0=0.0
      a1=0.0
      b1=0.0
      ho3=r(2)/1.5
      ni=40
      do 620 i=3,i2max,2
      a2=f2(i)*xi(i)
      b2=abs(f2(i))*xa(i)
      eras=4.0*f2(i-1)
      a1=a1+ho3*(a0+eras*xi(i-1)+a2)
      b1=b1+ho3*(b0+abs(eras)*xa(i-1)+b2)
      xj(i)=a1
      a0=a2
      b0=b2
      ni=ni-2
      if (ni) 610,610,620
  610 ho3=ho3+ho3
      ni=40
      if (i.ge.idb) ni=10000
  620 continue
      if (npos.ne.2) go to 650
      amin=1.0e30
      amax=-1.0e30
      jn=0
      jx=0
      i=i2max+2
      ix=i2max/4
      do 630 j=1,ix
      i=i-2
      if (xj(i).le.amax) go to 622
      amax=xj(i)
      jx=jx+1
  622 if (xj(i).ge.amin) go to 624
      amin=xj(i)
      jn=jn+1
  624 if (xj(i).lt.amax.and.xj(i).gt.amin.and.jn.gt.3.and.jx.gt.3)
     1  go to 640
  630 continue
  640 a1=0.5*(amin+amax)
      if (isli.gt.0) go to 650
      write (9,8764) amin,amax,a1
 8764 format ( 3f15.6)
c
  650 frac(n,n1)=a1/b1
      rryd(n,n1)=a1
  700 rkay(n,n1)=a1*109737.31
c
c          output
c
      mminp1=max(mmin+1,1)
      do 890 n=1,nokmax
      do 890 n1=1,2
      rr1(n,n1,mminp1)=rryd(n,n1)
  890 rk1(n,n1,mminp1)=rkay(n,n1)
      if (isli.gt.0) go to 935
      if (mmin.gt.0) go to 930
  900 write (9,90) conf1,ncfg(1),iz,ion,exf,irel,conf2,ncfg(2),kut,
     1  corrf,iclass
   90 format (/23h0 slater integrals for   ,3a6,6x,6hnconf=,i2,
     1      14x,2hz=,i3,3x,4hion=,i2,5x,4hexf=,f5.3,11x,5hirel=,i2/
     2    23x,3a6,6x,6hnconf=,i2,22x,4hkut=,i2,3x,6hcorrf=,f5.3,
     3  10x,6hclass ,i2)
  905 if (ncfg(1)-ncfg(2)) 920,910,920
  910 write (9,91) ha,hb,ha,hb
   91 format (/1h0,8x,1hk,14x,3hfk(,a3,1h,,a3,1h),21x,4hfrac,
     1 9x,1hk,14x,3hgk(,a3,1h,,a3,1h),21x,4hfrac)
      go to 930
  920 write (9,92) ha,hb,hc,hd,ha,hb,hc,hd
   92 format (
     1  /1h0,8x,1hk,10x,4hrdk(,a3,1h,,a3,1h,,a3,1h,,a3,1h),16x,4hfrac,
     2       9x,1hk,10x,4hrek(,a3,1h,,a3,1h,,a3,1h,,a3,1h),16x,4hfrac)
  930 write (9,93)
   93 format (1h ,8x,43h-     -------------------------------------,
     1 8h   -----,9x,43h-     -------------------------------------,
     2 8h   -----//)
  935 if (isli.gt.1) go to 945
      if (isli.eq.1.and.iclass.ne.0.and.mmin.ge.0.and.mmin.lt.3)
     1  go to 945
      do 940 n=1,nokmax
  940 write (9,94) (k1(n,n1),rryd(n,n1),rkay(n,n1),
     1      frac(n,n1), n1=1,2)
   94 format (i10,f16.8,7h ryd  =,f14.4,5h cm-1,f8.3,
     1        i10,f16.8,7h ryd  =,f14.4,5h cm-1,f8.3)
      write (9,95)
   95 format (1h0)
c
  945 aa=0.0
      a=0.0
      if (iclass.eq.0) go to 980
      mmin=mmin+1
      go to (950,960,965,970), mmin
  950 do 951 i=1,ip
      pe(i)=pc(i)
  951 pc(i)=pa(i)
      go to 400
  960 do 962 i=1,ip
      pc(i)=pe(i)
      pe(i)=pa(i)
  962 pa(i)=pc(i)
      go to 400
  965 do 966 n=1,nokmax
      do 966 n1=1,2
      rryd(n,n1)=(rryd(n,n1)+rr1(n,n1,2))/2.0
  966 rkay(n,n1)=(rkay(n,n1)+rk1(n,n1,2))/2.0
      if (isli.gt.0) go to 935
      go to 930
  970 he=ha
      ha=hb
      hb=hd
      fla=la
      flb=lb
      hb=ha
      ha=he
      do 972 i=1,ip
  972 pa(i)=pe(i)
c
      b=(2.0*flb+1.0)/(4.0*flb+1.0)
      if (itest.ne.0) go to 975
      itest=7
      aa=0.0
      a=aa
  975 fk=kemin-2
      do 979 n=1,noke
      fk=fk+2.0
      if (iclass.eq.1) go to 976
      d=ww(1)
      c=d*rryd(n,2)
      if (iclass.eq.11) go to 1978
      go to 978
  976 n2=2
      n3=3
      if (ha.eq.hb) go to 977
      n2=3
      n3=2
  977 d=ww(n2)-1.0
      c=0.5*rr1(n,2,n3)
      if (n.gt.1) c=c+b*rr1(n,1,n2)
      c=c*d
  978 a=a+0.5*c*s3j0sq(fla,fk,flb)
 1978 continue
      if (isli.gt.0) go to 979
      write (9,96) d,c,fla,fk,flb,a,eee,overlp
   96 format (/8f10.5)
  979 continue
      rr1(1,1,1)=a/d
      aa=109.73731*aa/d
c
  980 m=0
      do 981 n1=1,2
      ncfgo(n1)=ncfg(n1)
      n2=n1
      if (iclass.eq.7) n2=3-n1
      do 981 n=1,noke
      m=m+1
  981 rkay(m,1)=109.73731*rr1(n,n2,1)
      n3=(2-n2)*noke+1
      m=1
      n=2*nokd
      if (ha.eq.hb.or.hc.eq.hd) n=nokd
      n1=n-m+1
      n2=5
      if (isli.lt.3) write (9,97) conf1(1),conf1(2),
     1      conf1(3),conf2(2),conf2(3),n1, (rkay(i,1),n2, i=m,n),ovfact
   97 format (/1h0,2a6,a4,1h-,a6,a4,4x,i2,5(f11.4,1x,i1)/7(f11.4,1x,i1))
      if (ncs.ne.ncso) no=0
c
c         this write statement included to cure an apparent compiler bug
            if (isli.gt.5) write (9,2356) ncso,ncs,no,m,n
 2356 format (' ncso,ncs,no,m,n=',5i5)
c
      do 982 i=m,n
      j=no+i
      rkaysc(j)=fact(5)*rkay(i,1)*ovfact
      if (ncd.ne.nfirst) go to 982
      rrsc(ncpd,j)=rkaysc(j)/109.73731
  982 continue
      n=n+no
      np1=n+1
      if (idipp.eq.5.and.eps(2).gt.0.0.and.abs(eav(1)-eav(2)).
     1  gt.0.01) np1=1
      if (idip.ne.6.or.(nn(2).lt.99.or.nn(4).lt.99)) go to 983
      np1=1
      if (eps(1).eq.eps(2).and.l(2).eq.l(4)) np1=no+1
  983 do 985 i=np1,20
  985 rkaysc(i)=0.0
      nrk=n
  990 no=n
      ncso=ncs
c
      call seconds(t3)
      t4=t2-t1
      t5=t3-t2
      t3=t3-t0
      write (9,9988) ncfg,iread,t4,t5,t3
 9988 format (3i5,2f10.3,f10.3,4h sec)
      iclaso=iclass
      if (iclass.eq.0.or.iclass.eq.6.or.iclass.eq.11) return
      nconf=1000
      ha='   '
      return
c
 1000 write (9,1001) nlh(k),conf
 1001 format (//9h orbital ,a3, 23h not found for config  ,3a6)
      ha='   '
      go to 990
c
      end
