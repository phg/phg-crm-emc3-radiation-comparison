C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/g5inp.for,v 1.3 2004/07/06 13:57:23 whitefor Exp $ Date $Date: 2004/07/06 13:57:23 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Add /c7/ for batch and interactive indicators.
C               Add code to write out status for interactive monitoring.
C               Increase kcase to 500 (from 50).
C  25-02-2004 : Add explanation to stop 140.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine g5inp
c
c          punch input for program rcg, mods 5 to 11 (but not 12)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcn.h'
      dimension norb(kp),n(25,kp),l(25,kp),iww(8),
     1  w(25,kp),nn(8,kco),ww(8,kco),lsymb(22),
     2  lb(8,kco),ll(8,kco),wwo(8,kco),lbo(8,kco),etot(kco),ee8nc(kco)
c
      common/c0/nlz(4),ncfg(2),mmin,ncdes,jdum(8),
     1  idum(4),overlp,xdum(km)
      dimension ix(8),fk(8,5),ze(8),fgk(8,8,5),kpar(50),ivpar(50)
      common/c1/fact(5),ifact(5),nrk,rkaysc(20)
      common/c2/ee(kp),stn(kp),wt(kp),gamma(kp),q(kp),
     1  q2(kp),pdipsc(kp),rrsc(kp,12),ncd,ncpd,nfirst,ncont1,ipwr
      common/c3/icon,isli,idip,ic,icn(2),nconfn(2),nc0,icase,
     1  nsconf(3,2),del(kco),idipp,iovfact,ovfact
      common/c4/irho,isig,irhop,isigp,n12,idum4,t0
      common/cp/slitest,wnl(2),rm1(km),rmkp1(km)
      common/lcm1/nt2(ko),lt2(ko),wt2(ko),ncspvs,idum2,vpar(50),
     1  nconf,iz,z,ion,irel,hxid,c,mesh,idb,exf,corrf,kut,npar
      common/lcm1a/nlbcd
      common/lcm1b/nnlz(ko),nkkk(ko),e(ko),ee8,r(km),ru(km),
     1  pnl(km,ko),npnl
      common/lcm2/rdips(kbgks,kcase),st(kcase,5)
      common/genosc/igen,nbigks,bigks(kbgks),ibk
      character dh*80,option*28,nlh(4)*3,ha*3,hb*3,hc*3,hd*3,ha1*3,
     1  hb1*3,he*3,nlbcd(ko)*8,nlht(8)*3,nlb(8,kco)*3,kst(kcase,2)*3
      character ha1t*3
      character*6 conf(3),cst(kcase,6),conf1(3),conf2(3),
     1  cf1(kp),cf2(kp),cf3(kp),subr0*3,subr*3,calsq(5)*3,rj*1
      common/cdh/dh,option,nlh,ha,hb,hc,hd,ha1,hb1,he,nlht,nlb,kst,
     1  conf,cst,conf1,conf2,cf1,cf2,cf3,subr0,subr,calsq,rj
c     equivalence (nlh(1),ha),(nlh(2),hb),(nlh(3),hc),(nlh(4),hd),
c    1  (pnl,wwo),(pnl(1,4),lbo),(rdips,etot),(rdips(1,10),ee8nc)
c
      dimension eav(kp)
      data (lsymb(i),i=1,22)/1hs,1hp,1hd,1hf,1hg,1hh,1hi,1hk,1hl,1hm,
     1  1hn,1ho,1hq,1hr,1ht,1hu,1hv,1hw,1hx,1hy,1hz,1ha/
      character bcdr*1,bcdj*1,dmin*5,iprint*5
      data bcdr,bcdj/'r','j'/
     
C------Start ADAS conversion-------
     
      common/c7/is_batch

C------End ADAS conversion-------
c
c
c     kco=100
c     kp=kco+1
      call seconds(t0)
c 100 decode (80,10,dh) nck,iovfact,nocset,nsconf(3,1),nsconf(3,2),
c    1  eav11,iabg,
c    2  option,iquad,ifact,dmin,iprint,iengyd,ispecc,icon,isli,idip,alf
  100 read(dh,10) nck,iovfact,nocset,nsconf(3,1),nsconf(3,2),eav11,iabg,
     1  option,iquad,ifact,dmin,iprint,iengyd,ispecc,icon,isli,idip,alf
   10 format (5x,a2,i1,i2,i1,i2,f7.4,i1,a28,i1,5i2,2a5,2a1,3i1,f5.0)
c
c               check for generalised oscillator strength option
c                    and compute momentum transfer values
c
c     decode (18,11,option) nbigks,nenrgs,igen,irnq,irxq,irnd,irxd
      read (option,11) nbigks,nenrgs,igen,irnq,irxq,irnd,irxd
   11 format (1x,i3,i2,5x,5i1)
      if (igen.lt.5.and.iquad.gt.0) irnq=2
      irxq=max0(irxq,irnq)
      irnd=max0(irnd,1)
      irxd=max0(irxd,irnd)
      rj=bcdr
      if (igen.lt.5) nbigks=1
      if (igen.lt.5) go to 80
      if (nbigks.le.10) nbigks=kbgks
      if (nenrgs.le.0) nenrgs=21
      rj=bcdj
      option(1:8)='        '
      xmin=1.01
      if (icon.eq.1) xmin=1.1
      if (icon.gt.2) xmin=icon
      if (icon.eq.9) xmin=10.0
      icon=0
      xmax=100.0
      if (idip.eq.1) xmax=20.0
      if (idip.gt.1) xmax=100*idip
      if (idip.eq.9) xmax=1000.0
      idip=0
   80 nc0=0
      idipp=0
      if (idip.ne.5) go to 82
      idipp=5
      idip=9
   82 ncmax=kco
      nocset=-iabs(nocset)
      iabg=iabs(iabg)
      iquad=iabs(iquad)
      do 101 i=1,5
      if (ifact(i).eq.0.and.i.eq.2) ifact(i)=95
      if (ifact(i).eq.0) ifact(i)=85
      eras=ifact(i)
      if (eras.lt.1.5) eras=0.1
      fact(i)=0.01*eras
  101 if (ifact(i).eq.99) fact(i)=1.0
      if (icon.eq.0) icon=8
      nscf31=iabs(nsconf(3,1))
      ns32=nsconf(3,2)
      if (nsconf(3,2).eq.0.and.idip.eq.8) nsconf(3,2)=-8
      nscf32=iabs(nsconf(3,2))
      nosubc=0
      nc3=0
      rewind 2
      if (nc0.eq.0) go to 102
      do 105 nc=1,nc0
  105 read (2)
c
  102 do 90 i=1,kp
      stn(i)=0.0
      wt(i)=0.0
      gamma(i)=0.0
      q(i)=0.0
      q2(i)=0.0
      pdipsc(i)=0.0
      eav(i)=0.0
      do 90 j=1,12
   90 rrsc(i,j)=0.0
      ncont1=1000
      ncont2=1000
      idele=1000
      emax=0.0
c
c          determine number of configs of each parity
c
      rewind 31
      rewind 32
      ic=31
      icn(1)=31
      icn(2)=31
      nc=0
      nmax=0
      lmax=0
      norbx=0
      ipct=0
      vpar1=-99999999.0
      nc2=0
  110 nc=nc+1
      if (nc+nc0.gt.ncmax) go to 130
      read (2,end=112) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,e,ee8,npnl,r,ru, ((pnl(i,m),i=1,km),m=1,npnl),
     3  iw6
      go to 115
  112 ncmax=nc0+nc-1
      go to 130
  115 cf1(nc)=conf(1)
      cf2(nc)=conf(2)
      cf3(nc)=conf(3)
      etot(nc)=109.73731*vpar(1)
      ee(nc)=e(ncspvs)
      ee8nc(nc)=ee8
      norb(nc)=ncspvs
      if (ic.ne.32.or.ns32.ne.-7.or.ipct.ne.0) go to 118
      if (nc.gt.nc2+7.and.vpar(1).lt.vpar1) ipct=nc-nc2-1
  118 vpar1=vpar(1)
      if (idip.lt.9.or.nt2(ncspvs).lt.99) go to 120
      if (ncont1.eq.1000) ncont1=nc
      if (ee(nc).eq.ee(ncont1)) ncont2=nc
      ieras=ncont2-ncont1
      if (ieras.gt.0) idele=1000/ieras
      ieras=nc-ncont2-1
      if (ieras.gt.0) idele=min0(idele,3000/ieras)
  120 if (nc.eq.1) iont=ion
      if (nc.eq.1) izt=iz
      if (iont.eq.ion.and.izt.eq.iz) go to 132
  130 nc3=nc-1
      if (nc3.eq.0) go to 900
      backspace 2
      go to 160
  132 if (ncspvs.gt.norbx) norbx=ncspvs
      prty=0.0
      do 135 i=1,ncspvs
      n(i,nc)=nt2(i)
      l(i,nc)=lt2(i)
      w(i,nc)=wt2(i)
      if (i.eq.ncspvs.and.w(i,nc).le.1.0) go to 134
      nmax=max0(nmax,n(i,nc))
      lmax=max0(lmax,l(i,nc))
  134 eras=l(i,nc)
  135 prty=prty+w(i,nc)*eras
      two=2.0
      prty=mod(prty,two)
      if (nc.eq.1) prty1=prty
      if (prty.eq.prty1) go to 150
      if (ic.eq.32) go to 130
      prty1=prty
      nc2=nc-1
      ic=32
      icn(2)=32
  150 write (ic) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,e,ee8,npnl,r,ru, ((pnl(i,m),i=1,km),m=1,npnl)
      go to 110
c
  160 if (nc3.eq.1.and.cf1(1).eq.'dummy ') go to 700

C------Start ADAS conversion-------
      
      if (is_batch.eq.0) then
         write(6,*)is_batch
         call flush(6)
      endif
      
C------End ADAS conversion-------
     
      write (9,13)
   13 format (34h1rcn2---input for rcg mod 10 or 11///)
      if (nc2.eq.0) nc2=nc3
      nsconf(2,1)=nc2
      nsconf(2,2)=nc3-nc2
      if (nscf31.eq.0) nsconf(3,1)=nc2
      if (nscf32.eq.0) nsconf(3,2)=nsconf(2,2)
      do 162 i=1,8
      iww(i)=0
      do 162 nc=1,kco
      lbo(i,nc)=lb(i,nc)
      lb(i,nc)=lsymb(1)
      ll(i,nc)=0
      nn(i,nc)=0
      wwo(i,nc)=ww(i,nc)
  162 ww(i,nc)=0.0
c
c          set up partially-filled or interacting shells
c
c     first, find orbitals not always full, but with more
c        than one electron in some configuration,
c          or find singly-occupied orbital which is not the outermost orbital
      j1=0
      if (nmax.eq.0) go to 170
      nmin=1
      do 169 nnn=nmin,nmax
      lmaxp1=min0(nnn,lmax+1)
      do 169 lp1=1,lmaxp1
      lll=lp1-1
      wmax=1.0
      a=4*lll+2
      wmin=a
      do 166 nc=1,nc3
      ncspvs=norb(nc)
      do 165 i=1,ncspvs
      if (n(i,nc).ne.nnn.or.l(i,nc).ne.lll) go to 165
      wmin=min(wmin,w(i,nc))
      wmax=max(wmax,w(i,nc))
      if (w(i,nc).eq.1.0.and.i.ne.ncspvs) wmax=2.0
      go to 166
  165 continue
      wmin=0.0
  166 continue
      if (wmin.eq.a.or.wmax.eq.1.0) go to 169
      j1=j1+1
      do 168 nc=1,nc3
      nn(j1,nc)=nnn
  168 ll(j1,nc)=lll
  169 continue
c
c     second, find remaining (singly-occupied) orbitals
c        (for one parity at a time)
  170 nsubco=nosubc
      nosubc=0
      k=1
      ncn=1
      ncx=nc2
      if (icon.ne.1) icon=max0(icon,j1+1)
  171 j3=j1
      do 180 nc=ncn,ncx
      nc1=max0(nc-1,ncn)
      do 172 j=1,8
  172 nlb(j,nc)='   '
      ncspvs=norb(nc)
      do 179 i=1,ncspvs
      if (j3.eq.0.and.i.eq.ncspvs.and.nc.eq.ncx) go to 177
      if(j3.eq.0) go to 174
      do 173 j=1,j3
      if (n(i,nc).eq.nn(j,nc1).and.l(i,nc).eq.ll(j,nc1)) go to 178
  173 continue
  174 if (j3.eq.j1.or.nc.eq.ncn) go to 176
      j=j3
      if (l(i,nc).eq.ll(j,nc1).and.i.eq.ncspvs) go to 178
  176 a=4*l(i,nc)+2
      if (w(i,nc).eq.a) go to 179
  177 j3=j3+1
      if (icon.gt.1) j3=min0(j3,icon)
      do 1177 nc1=nc,ncx
 1177 ll(j3,nc1)=l(i,nc)
      j=j3
  178 ww(j,nc)=w(i,nc)
      nn(j,nc)=n(i,nc)
      if (nn(j,ncn).eq.0) nn(j,ncn)=n(i,nc)
      jj=ll(j,nc)
      lb(j,nc)=lsymb(jj+1)
c     encode (3,16,nlb(j,nc)) nn(j,nc),lb(j,nc)
      write (nlb(j,nc),16) nn(j,nc),lb(j,nc)
   16 format (i2,a1)
  179 continue
  180 continue
      nosubc=max0(nosubc,j3)
      if (nosubc.le.8) go to 181
      write (9,17)
   17 format (/'*****more than 8 subshells!')
      go to 700
  181 if (nc2.eq.nc3.or.k.eq.2) go to 182
      k=2
      ncn=nc2+1
      ncx=nc3
      go to 171
c
c          if configurations same as for previous case, set ictc=1
c
  182 ictc=1
      if (nosubc.ne.nsubco) ictc=0
      if (ictc.eq.0) go to 185
      do 184 i=1,nosubc
      do 183 nc=1,nc3
      if (ww(i,nc).ne.wwo(i,nc)) ictc=0
      if (ww(i,nc).eq.0.0) go to 183
      if (lb(i,nc).ne.lbo(i,nc)) ictc=0
  183 continue
  184 continue
      if (ictc.eq.0) go to 185
      backspace 11
      go to 200
c          punch control card
  185 nsconf(1,1)=nosubc
      nsconf(1,2)=nosubc
      if (nc2.eq.nc3) nsconf(1,2)=0
      if (iw6.ge.0) iw6=6
      if (iw6.lt.0) iw6=-6
      if (idip.eq.6) iw6=8*iw6/abs(iw6)
      write (9,18) nck,nocset,nsconf,iabg,option,iquad,dmin,iprint,
     1  iengyd,ispecc,iw6,icon,isli,idip,ipct,ictc,irel
   18 format (4x,1h1,a2,i3,i1,2i2,i1,2i2,i1,a28,i1,1x,9h1000.0000,
     1  2a5,2a1,6i2,i8)
      if (icon.ne.1) write(11,18) nck,nocset,nsconf,iabg,option,iquad,
     1  dmin,iprint,iengyd,ispecc,iw6,ipct,ictc
      if (nocset.lt.0) nocset=nocset-1
c          punch configuration cards
  191 k=1
      ncn=1
      ncx=nc2
  192 if (ncn.eq.ncx) go to 196
      do 195 i=1,nosubc
      do 194 nc=ncn,ncx
      if (ww(i,nc).eq.0.0) go to 194
      do 193 m=ncn,ncx
      if (ww(i,m).ne.0.0) go to 193
      ll(i,m)=ll(i,nc)
      lb(i,m)=lb(i,nc)
  193 continue
      go to 195
  194 continue
  195 continue
  196 do 198 nc=ncn,ncx
      do 197 i=1,nosubc
  197 iww(i)=ww(i,nc)
      write  (9,19) (lb(i,nc),iww(i), i=1,8),cf1(nc),cf2(nc),cf3(nc),
     1  etot(nc),ee8nc(nc)
   19 format (1x,8(a1,i2,4x),5x,3a6,5x,4heav=,f14.3,3h kk,
     1  5x,5hekin=,f9.4,3h ry)
      if (icon.ne.1) write (11,20) (lb(i,nc),iww(i), i=1,8),
     1  cf1(nc),cf2(nc),cf3(nc),etot(nc),ee8nc(nc)
   20 format (8(a1,i2,2x),3a6,f13.3,f9.4)
  198 continue
      if (nc2.eq.nc3.or.k.eq.2) go to 200
      k=2
      ncn=nc2+1
      ncx=nc3
      go to 192
c
c
c          rearrange, scale, and punch single-conf parameters
c
  200 rewind 31
      rewind 32
      iflag=0
      kk=1
      nfirst=1
      nlast=nc2
      ic=31
      do 495 nc=1,nc3
      icn(1)=ic
      icn(2)=ic
      read (ic) nt2,lt2,wt2,ncspvs,vpar,conf,nconf,iz,z,
     1  ion,irel,hxid,mesh,c,idb,exf,corrf,kut,npar,
     2  nlbcd,nnlz,nkkk,e,ee8
      i1=ncspvs+1
      do 201 i=i1,25
  201 l(i,nc)=77
      nconf=99
      nconfn(kk)=99
      if (nc.eq.1) e1=vpar(1)
      kpar(1)=0
      vpar(1)=eav11+109.73731*(vpar(1)-e1)
      eav(nc)=vpar(1)
c          determine energy widths for pseudo-discrete configurations
      del(nc)=1.0
      if (nc.ge.ncont1) del(nc)=0.0
      if (idip.eq.8.and.nt2(ncspvs).ge.99) del(nc)=0.0
      if (nc.eq.nfirst.and.nt2(ncspvs).lt.99) go to 202
      if (idip.ne.7.or.w(ncspvs,nc).ne.1.0) go to 206
      if (l(ncspvs,nc).ne.l(ncspvs,nc-1)) emax=eav(nc)-109.737*e(ncspvs)
  202 zc=ion+1
      if (zc.le.0.0) zc=1.0
      fn=99.0
      if (e(ncspvs).lt.0.0) fn=zc/sqrt(-e(ncspvs))
      del0=109.73731
      if (fn.eq.99.0) go to 203
      del0=2.0*del0*(zc**2)/(fn**3)
      if (nc.eq.nfirst) go to 1202
      if (l(ncspvs,nc).ne.l(ncspvs,nc-1)) go to 206
      if (n(ncspvs,nc).gt.n(ncspvs,nc-1)+1) go to 203
 1202 emax=eav(nc)+0.5*del0
      if (nc.eq.nfirst.or.nc.eq.nc2.or.nc.eq.nc3) go to 206
      ncs1=ncspvs
      vpar1=vpar(1)
      read (ic) nt2,lt2,wt2,ncspvs,vpar(1)
      backspace ic
      eavp1=eav11+109.73731*(vpar(1)-e1)
      ncspvs=ncs1
      vpar(1)=vpar1
      if (eavp1.le.eav(nc)) go to 206
      x=(eavp1-eav(nc))/(eavp1-eav(nc-1))
      emax=min(emax,x*eav(nc)+(1.0-x)*eavp1)
      go to 206
  203 emin=emax
      emax=2.0*eav(nc)-emin
      if (nc.eq.nc2.or.nc.eq.nc3) go to 205
      ncs1=ncspvs
      vpar1=vpar(1)
      read (ic) nt2,lt2,wt2,ncspvs,vpar(1)
      backspace ic
      ib=lt2(ncspvs)
      eavp1=eav11+109.73731*(vpar(1)-e1)
      ncs2=ncspvs
      ncspvs=ncs1
      vpar(1)=vpar1
      if (nc.eq.nfirst.or.eav(nc).lt.eav(nc-1)) go to 1203
      if (ncspvs.ne.ncs2) go to 205
      if (ee(nc).lt.ee(nc-1)) go to 1203
      if (nc.gt.nfirst.and.ib.ne.l(ncspvs,nc)) go to 205
      if (nc.gt.nfirst.and.ib.eq.l(ncspvs,nc).and.ib.eq.l(ncspvs,nc-1))
     1  go to 204
 1203 emax=0.5*(eav(nc)+eavp1)
      emin=emax+eav(nc)-eavp1
      go to 205
  204 if (eavp1.le.eav(nc)) go to 205
      x=(eavp1-eav(nc))/(eavp1-min(eav(nc-1),emin))
      half=0.5
      x=max(x,half)
      emax=min(emax,x*eav(nc)+(1.0-x)*eavp1)
  205 if (emax.le.emin) go to 206
      del(nc)=sqrt((emax-emin)/del0)
c
  206 do 210 i=1,8
      ix(i)=0
      nlht(i)='   '
      ze(i)=-1.0
      do 210 k=1,5
      fk(i,k)=-1.0
      do 210 j=1,8
  210 fgk(i,j,k)=-1.0
c
      npar=1
      jx=0
      do 220 i=1,ncspvs
      a=4*l(i,nc)+1
      if (w(i,nc).lt.1.0.or.w(i,nc).gt.a) go to 220
      jx=jx+1
      nlht(jx)=nlbcd(i)(1:3)
      l(jx,nc)=l(i,nc)
      w(jx,nc)=w(i,nc)
      if (w(i,nc).eq.1.0.or.w(i,nc).eq.a) go to 220
      nk=l(i,nc)
      do 216 k=1,nk
      npar=npar+1
  216 fk(jx,k)=vpar(npar)
  220 continue
c
      if (jx.eq.0) go to 291
      do 230 i=1,jx
      if (l(i,nc).eq.0) go to 230
      npar=npar+1
      ze(i)=vpar(npar)
  230 continue
c
      if (jx.eq.1) go to 251
      jm1=jx-1
      do 240 i=1,jm1
      j1=i+1
      do 240 j=j1,jx
      nk=min0(l(i,nc),l(j,nc))
      if (nk.eq.0) go to 240
      do 235 k=1,nk
      npar=npar+1
  235 fgk(i,j,k)=vpar(npar)
  240 continue
c
      do 250 i=1,jm1
      j1=i+1
      do 250 j=j1,jx
      nk=min0(l(i,nc),l(j,nc))+1
      do 245 k=1,nk
      npar=npar+1
  245 fgk(j,i,k)=vpar(npar)
  250 continue
c
c
  251 npar=1
      do 260 i=1,nosubc
      do 256 j=1,jx
      if (nlht(j).ne.nlb(i,nc)) go to 256
      ix(i)=j
      do 252 k=1,5
      if (fk(j,k).lt.0.0) go to 253
      npar=npar+1
      kpar(npar)=1
  252 vpar(npar)=fk(j,k)*fact(1)
  253 if (fk(j,1).lt.0.0) go to 260
      if (iabg.eq.0) go to 260
      nk=ll(i,nc)
      if (nk.ne.2) go to 254
      nk=nk+1
      if (ww(i,nc).gt.2.and.ww(i,nc).lt.8) nk=nk+2
  254 do 255 k=1,nk
      npar=npar+1
      kpar(npar)=1
      vpar(npar)=0.0
  255 if (k.eq.1) vpar(npar)=alf
      go to 260
  256 continue
  260 continue
c
      jj=0
      do 270 i=1,nosubc
      ii=ix(i)
      if (ii.eq.0) go to 270
      if (ii.le.jj) iflag=1
      jj=ii
      if (ze(ii).lt.0.0) go to 270
      npar=npar+1
      kpar(npar)=2
      vpar(npar)=ze(ii)*fact(2)
  270 continue
c
      if (jx.eq.1) go to 291
      jm1=nosubc-1
      do 280 i=1,jm1
      if (ix(i).eq.0) go to 280
      j1=i+1
      do 279 j=j1,nosubc
      ii=ix(i)
      jj=ix(j)
      if (jj.eq.0) go to 279
      if (ii.lt.jj) go to 272
      jjj=ii
      ii=jj
      jj=jjj
  272 do 275 k=1,5
      if (fgk(ii,jj,k).lt.0.0) go to 279
      npar=npar+1
      kpar(npar)=3
  275 vpar(npar)=fgk(ii,jj,k)*fact(3)
  279 continue
  280 continue
c
      do 290 i=1,jm1
      if (ix(i).eq.0) go to 290
      j1=i+1
      do 289 j=j1,nosubc
      ii=ix(i)
      jj=ix(j)
      if (jj.eq.0) go to 289
      if (ii.lt.jj) go to 282
      jjj=ii
      ii=jj
      jj=jjj
  282 do 285 k=1,5
      if (fgk(jj,ii,k).lt.0.0) go to 289
      npar=npar+1
      kpar(npar)=4
  285 vpar(npar)=fgk(jj,ii,k)*fact(4)
  289 continue
  290 continue
c
  291 if (npar.ge.5) go to 294
      n1=npar+1
      do 292 i=n1,5
      kpar(i)=0
      ivpar(i)=0.0
  292 vpar(i)=0.0
  294 write (9,28) conf, (vpar(i), i=1,5), hxid,fact
   28 format (//1h0,3a6,6x,5f12.5,5x,a2,5x,5hfact=,5f6.2)
      if (nc.ge.ncont1) vpar(1)=-9500+idele*(nc-ncont1)
      if (nc.gt.ncont2) vpar(1)=-7500+idele*(nc-ncont2-1)
      do 295 i=1,npar
  295 ivpar(i)=10000.0*vpar(i)
      if (vpar(1).gt.99999.99) go to 296
      if (vpar(1).lt.-9999.99) go to 296
      write (11,29) conf,npar, (ivpar(i),kpar(i), i=1,5), hxid,
     1  (ifact(i), i=1,4)
   29 format (3a6,i2,5(i9,i1),a2,4i2)
      go to 298
  296 write (11,27) conf,npar, (vpar(i),kpar(i), i=1,5), hxid,
     1  (ifact(i), i=1,4)
   27 format (3a6,i2,f9.1,i1,4(f9.2,i1),a2,4i2)
  298 if (npar.le.5) go to 300
      write ( 9,30) (vpar(i), i=6,npar)
   30 format (7f12.5)
      write (11,31) (ivpar(i),kpar(i), i=6,npar)
   31 format (7(i9,i1))
c
c          calc configuration interaction parameters
c
  300 if (nc.lt.nlast) go to 495
      if (nfirst.eq.nlast) go to 350
      if (isli.gt.4) go to 350
      subr0='   '
      subr=calsq(3)
      nlm1=nlast-1
      neras=nsconf(3,kk)
      if (neras.ge.0.or.neras.le.-8) go to 301
      nlm1=min0(nlm1,nfirst-1+max0(-neras,ipct))
      ncd1=0
      ha1='   '
      hb1='   '
  301 do 329 ncd=nfirst,nlm1
      ncfg(1)=ncd+nc0
      ncpdf=ncd+1
      ncpdl=nlast
      if (ncd.ne.nfirst.and.neras.eq.-9) ncpdl=ncpdf
      irho1=-7
      isig1=-7
      do 329 ncpd=ncpdf,ncpdl
      ncfg(2)=ncpd+nc0
      nrk=0
      slitest=-99.0
      do 328 irho=1,nosubc
      do 328 isig=irho,nosubc
      do 328 irhop=1,nosubc
      do 327 isigp=irhop,nosubc
      mmin=0
      if (irho.ne.irhop.or.isig.ne.isigp) go to 303
      www=0
      do 302 i=isig,nosubc
  302 www=www+ww(i,ncd)+ww(i,ncpd)
      if (www.gt.2.0) go to 327
  303 do 304 i=1,nosubc
      ww1=ww(i,ncd)
      ww2=ww(i,ncpd)
      if (i.eq.irho) ww1=ww1-1.0
      if (i.eq.isig) ww1=ww1-1.0
      if (i.eq.irhop) ww2=ww2-1.0
      if (i.eq.isigp) ww2=ww2-1.0
      if (ww1.ne.ww2) go to 327
      if (ww1.lt.0.0) go to 327
      if (i.le.isig.or.i.le.isigp.or.ww1.le.0.0) go to 304
      if (ll(i,ncd).ne.ll(i,ncpd)) go to 327
      if (nn(i,ncd).ne.nn(i,ncpd)) mmin=-1
      if (nn(i,ncd)+nn(i,ncpd).eq.198.and.ee(ncd).ne.ee(ncpd)) mmin=-1
  304 continue
      a=4*ll(irho,ncd)+2
      if (ww(irho,ncd).eq.a.and.ww(irho,ncpd).eq.a) go to 327
      a=4*ll(isig,ncd)+2
      if (ww(isig,ncd).eq.a.and.ww(isig,ncpd).eq.a) go to 327
      kdmin=max0(iabs(ll(irho,ncd)-ll(irhop,ncpd)),iabs(ll(isig,
     1  ncd)-ll(isigp,ncpd)))
      kdmax=min0(ll(irho,ncd)+ll(irhop,ncpd),ll(isig,ncd)
     1  +ll(isigp,ncpd))
      if(kdmin.gt.kdmax) go to 327
      ovfact=1.0
      if (mmin.lt.0) go to 320
      if (iovfact.le.0.or.iovfact.eq.2) go to 320
c          calculate overlap factor for spectator electrons
      subr='   '
      ha1t=ha1
      do 310 i=1,nosubc
      ww1=ww(i,ncd)
      if (i.eq.irho) ww1=ww1-1.0
      if (i.eq.isig) ww1=ww1-1.0
      if (ww1.le.0.0) go to 310
      ha=nlb(i,ncd)
      hb=ha
      call over
      ovfact=ovfact*(overlp**ww1)
      if (iovfact.gt.3) write (9,1031) ncfg,i,ha,ww1,overlp,ovfact
 1031 format (3i3,2x,a3,f5.1,2f10.6)
  310 continue
      ha1=ha1t
      ncd1=-7
      subr='   '
  320 continue
      ha=nlb(irho,ncd)
      hb=nlb(isig,ncd)
      hc=nlb(irhop,ncpd)
      hd=nlb(isigp,ncpd)
      n12=2
      if (ncd.ne.ncd1) n12=1
      if (ha.ne.ha1.or.hb.ne.hb1) n12=1
      if (irho.ne.irho1.or.isig.ne.isig1) n12=1
      call sli2     
      ncd1=ncd
      ha1=ha
      hb1=hb
      irho1=irho
      isig1=isig
      slitest=0.0
  327 continue
  328 continue
      if (nrk.eq.0) go to 329
      if (idip.eq.7.or.idip.eq.8) go to 324
      if (ncd.lt.ncont1) go to 326
  324 do 325 i=1,nrk
  325 rkaysc(i)=del(ncd)*del(ncpd)*rkaysc(i)
  326 write (9,32) del(ncd),del(ncpd),(rkaysc(i),i=1,nrk)
   32 format (9h sqrtdel=,2f10.4,5x,5(f11.4,2x)/7(f11.4,2x))
      n2=5
      write (11,33) conf1(2),conf1(3),conf2(2),conf2(3),nrk,
     1  (rkaysc(i),n2,i=1,5),hxid,(ifact(i), i=1,3),ifact(5)
   33 format (a6,a3,1h-,a6,a3,i1,5(f9.4,i1),a2,4i2)
      if (nrk.le.5) go to 329
      write (11,34) (rkaysc(i),n2, i=6,nrk)
   34 format (7(f9.4,i1))
  329 continue
c
  350 if (iquad.lt.3.and.iquad.ne.kk) go to 390
      n1=nfirst
      n2=nlast
      n3=n1
      n4=n2
      irmn1=irnq+1
      irmx1=irxq+1
      icn(1)=ic
      icn(2)=ic
      go to 402
  390 if (kk.eq.2) go to 400
      if (iw6.lt.0) write (6,38) nfirst,nlast,kk
   38 format (' finish g5inp for configurations',i4,'  to',i4,
     1  ',   parity=',i3)
      if (nc2.eq.nc3) go to 496
      kk=2
      nfirst=nc2+1
      nlast=nc3
      ic=32
      write (9,39)
   39 format (1h1)
      go to 495
c
c          calc quadrupole or dipole reduced matrix elements
c
  400 n1=1
      n2=nc2
      n3=nc2+1
      n4=nc3
      irmn1=irnd+1
      irmx1=irxd+1
      icn(1)=31
      icn(2)=32
  402 subr0='   '
      subr=calsq(1)
      ipwr=irmn1-1
      if (idip.eq.1) go to 460
      j1=icn(1)-30
      if (igen.lt.5) go to 3402
      elo=eav(1)
      ehi=elo
      do 1402 i=1,nc3
      ea=eav(i)
      if (ea.lt.elo) elo=ea
      if (ea.gt.ehi) ehi=ea
 1402 continue
      if (ehi.le.elo) ehi=elo+vpar(2)
      enmax=(ehi-elo)/109.73731
      enmax=enmax*1.1*xmax
      if (enmax.lt.0.001) then
         write(0,*)'ENMAX is below 0.001 ' ,enmax
         stop 140
      endif
      ekmax=2.0*sqrt(enmax)
      ekmin=1.0e-3
      ekmaxl=log(ekmax)
      ekminl=log(ekmin)
      delekl=(ekmaxl-ekminl)/float(nbigks-1)
      write (11,42) nbigks,ekminl,ekmaxl,delekl,nenrgs,xmin,xmax
   42 format (i5,3f15.10,i10,2f10.2)
      do 2402 i=1,nbigks
      ekl=ekminl+delekl*(i-1)
 2402 bigks(i)=exp(ekl)
      icase=0
      write (9,43) enmax,nbigks
   43 format (/7h enmax=,f14.7,8h nbigks=,i4,//9h k-values/)
      write (9,44) (bigks(i),i=1,nbigks)
   44 format (8f14.7)
 3402 continue
      do 450 ipwr1=irmn1,irmx1,2
      ipwr=ipwr1-1
      do 450 ncd=n1,n2
      ncfg(1)=ncd+nc0
      do 450 ncpd=n3,n4
      ncfg(2)=ncpd+nc0
      do 439 irho=1,nosubc
      do 438 irhop=1,nosubc
      if (iabs(ll(irho,ncd)-ll(irhop,ncpd)).gt.ipwr) go to 438
      lsum=ll(irho,ncd)+ll(irhop,ncpd)
      if (lsum.lt.ipwr) go to 438
      if (mod(lsum+ipwr,2).ne.0) go to 438
      if (irho.ne.irhop) go to 410
      if (ncd.ne.ncpd) go to 404
      do 403 i=irho,nosubc
      eras=4*ll(i,ncd)+2
      www=ww(i,ncd)
      if ((www.eq.0.0.or.www.eq.eras).and.i.eq.irho) go to 438
      if (i.eq.irho) go to 403
      if (www.eq.0.0.or.www.eq.eras) go to 403
      if (ll(i,ncd).eq.0) go to 403
      go to 438
  403 continue
      go to 410
  404 www=0
      do 405 i=irho,nosubc
  405 www=www+ww(i,ncd)+ww(i,ncpd)
      if (www.ne.2.0) go to 438
  410 do 420 i=1,nosubc
      ww1=ww(i,ncd)
      ww2=ww(i,ncpd)
      if (i.ne.irho.and.i.ne.irhop) go to 412
      if (i.eq.irho) ww1=ww1-1.0
      if (i.eq.irhop) ww2=ww2-1.0
      if (ww1.lt.0.0) go to 438
      go to 414
  412 if (ww1+ww2.eq.0.0) go to 420
      if (nlb(i,ncd).ne.nlb(i,ncpd)) go to 438
  414 if (ww1.ne.ww2) go to 438
  420 continue
      ovfact=1.0
      if (iovfact.lt.2) go to 430
c          calculate overlap factor for spectator electrons
      do 425 i=1,nosubc
      ww1=ww(i,ncd)
      if (i.eq.irho) ww1=ww1-1.0
      if (ww1.le.0.0) go to 425
      ha=nlb(i,ncd)
      hb=ha
      call over
      ovfact=ovfact*(overlp**ww1)
      if (iovfact.gt.3) write (9,1031) ncfg,i,ha,ww1,overlp,ovfact
  425 continue
      ha1='   '
  430 continue
      ha=nlb(irho,ncd)
      hb=nlb(irhop,ncpd)
      mmin=0
      call dip

C------Start ADAS conversion-------
      
      if (is_batch.eq.0) then
         write(6,*)is_batch
         call flush(6)
      endif
      
      
C------End ADAS conversion-------
     
      go to 450
  438 continue
  439 continue
      ovfact=1.0
      mmin=-7
      call dip

C------Start ADAS conversion-------
      
      if (is_batch.eq.0) then
         write(6,*)is_batch
         call flush(6)
      endif
      
      
C------End ADAS conversion-------
     
  450 continue
      if (igen.lt.5) go to 460
      do 455 ibk=1,nbigks
      do 454 ic=1,icase
      do 452 icc=1,3
      conf1(icc)=cst(ic,icc)
  452 conf2(icc)=cst(ic,icc+3)
      ipwr=st(ic,1)
      frac=st(ic,2)
      hxid=st(ic,3)
      iover=st(ic,4)
      if1=st(ic,5)
      ha=kst(ic,1)
      hb=kst(ic,2)
      reddip=rdips(ibk,ic)
      write (11,46) conf1,conf2,reddip,ha,rj,ipwr,hb,frac,hxid,iover,if1
   46 format(3a6,2x,2a6,a4,f14.7,1h(,a3,2h//,a1,i1,2h//,a3,1h),
     1  f6.3,a2,2i4)
  454 continue
  455 continue
      if (iw6.lt.0) write (6,47)
   47 format (' finish calculation of Bessel-function matrix elements')
  460 if (mod(ipwr,2).eq.0) go to 390
c
  495 continue
      if (iw6.lt.0.and.kk.eq.2) write (6,38) nfirst,nlast,kk
c
  496 if (iflag.eq.0) go to 500
      do 497 i=1,5
  497 write (9,40)
   40 format (11h **********)
      write (9,41)
   41 format (34h0*warning --- orbitals rearranged*)
c
c
c
  500 call seconds(t)
      t=t-t0
      write (9,50) t
   50 format (//20x,10h-99999999.,10x,2ht=,f7.3,4h sec)
      if (nocset.lt.0) write (11,51)
   51 format (20x,10h-55555555.)
      write (11,52)
   52 format (20x,10h-99999999.)
c
c
  700 nc0=nc0+nc3
      if (nc0.eq.ncmax) go to 900
      go to 102
c
  900 return
      end
