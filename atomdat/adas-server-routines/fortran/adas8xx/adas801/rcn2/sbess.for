C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/sbess.for,v 1.2 2004/07/06 15:18:50 whitefor Exp $ Date $Date: 2004/07/06 15:18:50 $
C
      subroutine sbess(rb,itest,ls,eksq,sz,szp)
c
c     routine for the evaluation of spherical bessel functions
c           using recurrence relations
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      common/blk2/ibug1,ibug2,ibug3,ibug4,ibug5,ibug6,lswt,ldel,lmin
      common/inform/ iread,iwrite,ipunch
      dimension sb(200), s(200)
c
      if (itest.eq.3) eksq=-eksq
      ak=sqrt(eksq)
      r=ak*rb
      if (ls-r) 10,20,20
   10 go to (30,60,90), itest
   20 go to (180,60,90), itest
   30 sb(1)=sin(r)
      sb(2)=sin(r)/r-cos(r)
      if (ls-1) 40,50,140
   40 sz=sb(1)
      szp=ak*cos(r)
      go to 210
   50 sz=sb(2)
      szp=ak*(cos(r)/r-sin(r)/r**2+sin(r))
      go to 210
   60 sb(1)=-cos(r)
      sb(2)=-cos(r)/r-sin(r)
      if (ls-1) 70,80,140
   70 sz=sb(1)
      szp=ak*sin(r)
      go to 210
   80 sz=sb(2)
      szp=ak*(sin(r)/r+cos(r)/r**2-cos(r))
      go to 210
   90 sb(1)=1.0
      sb(2)=-(1.0+1.0/r)
      if (ls-1) 100,110,120
  100 sz=exp(-r)
      szp=-ak*exp(-r)
      go to 210
  110 sz=-(1.0+1.0/r)*exp(-r)
      szp=(1.0+1.0/r+1.0/r**2)*exp(-r)
      go to 210
  120 ls1=ls+1
      eras=-1.0
      do 130 l=2,ls1
      eras=-eras
  130 sb(l+1)=(2*l-1)*eras*sb(l)/r-sb(l-1)
      l=ls1
      sz=sb(l+1)*exp(-r)
      szp=ak*((l+1)*(-1)**l*sb(l+1)/r+sb(l))*exp(-r)
      go to 210
  140 ls1=ls+1
      do 150 l=2,ls1
      sb(l+1)=(2*l-1)*sb(l)/r-sb(l-1)
  150 continue
      if (itest-2) 200,160,200
  160 if (ls-ls/2*2) 170,200,170
  170 sz=-sz
      szp=-szp
      go to 200
  180 m=ls+12
      if (r.lt.0.05) m=ls+6
      s(m+1)=0.0
      s(m)=1.0e-20
      mm1=m-1
      do 190 i=1,mm1
      j=m+1-i
      s(j-1)=(2*j-1)/r*s(j)-s(j+1)
  190 continue
      sb(m)=sin(r)/s(1)
      sb(ls)=s(ls)*sb(m)
      sb(ls+1)=s(ls+1)*sb(m)
      sb(ls+2)=s(ls+2)*sb(m)
  200 sz=sb(ls+1)
      szp=ak*(sb(ls+1)/r+(ls*sb(ls)-(ls+1)*sb(ls+2))/(2*ls+1))
  210 if (ibug5-1) 240,220,240
  220 write(iwrite,230)sz,szp
  230 format (//11h sz,szp  = ,(2e16.8)//)
  240 continue
      return
      end
