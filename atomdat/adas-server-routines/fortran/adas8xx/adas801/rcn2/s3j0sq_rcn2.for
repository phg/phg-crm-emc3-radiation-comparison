C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcn2/s3j0sq_rcn2.for,v 1.2 2004/07/06 15:18:20 whitefor Exp $ Date $Date: 2004/07/06 15:18:20 $
C
      function s3j0sq(fj1,fj2,fj3)
c
c          calculate square of 3-j symbol with zero magnetic quantum nos.
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      fj=fj1+fj2+fj3
      a=fj-fj1-fj1
      b=fj-fj2-fj2
      c=fj-fj3-fj3
      s3j0sq=fctrl(a)*fctrl(b)*fctrl(c)/fctrl(fj+1.0)
      a=fctrl(a/2.0)*fctrl(b/2.0)*fctrl(c/2.0)/fctrl(fj/2.0)
      s3j0sq=s3j0sq/a/a
      return
      end
