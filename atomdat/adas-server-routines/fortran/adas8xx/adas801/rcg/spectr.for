C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/spectr.for,v 1.3 2004/07/06 15:21:05 whitefor Exp $ Date $Date: 2004/07/06 15:21:05 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase ktran to 30000.
C               Extend /pwborn/ common block.
C               Write spid ('elec dip' etc) to units 52 and 77.
C               Write transition info to unit 52.
C               Add /batch/ common bloack and write number of
C               lines to unit 6 in interactive use.
C
C  17-06-2003 : Replace stop 57 with a stop 'Too many transitions'
C               message. Add some new variables to allow for a 
C               future separation between spectrum lines and
C               maximum number of collisions. The logic is
C               still too convolved to do this easily.
C 
C               Transfer spectal identification with b_ variables
C               to facilitate future separation between spectral
C               lines and collision strength calculations.
C
C  17-11-2003 : For very highly ionised species, eg W66+, the admissable
C               eigenvalue range of (GT -4000) was not sufficient.
C               Relax to -20000. 
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
c     overlay(g9,10,4)
      subroutine spectr
c
c
c        Calc. spectrum for magnetic dipole(ii=0), electric dipole(ii=1),
c             or electric quadrupole(ii=2) transitions
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2b/ncfg(kmx),ncfgp(kmx),pmup(kc,kc),eigp(kmx),lcserp(kmx)
      common/c9/l1,l2,cflag(kc,2),cgengy(kc,2),swt(kc,2),imax
      common/c3b/t(klam),tp(klam),s2(klam),soabss(klam),
     1  aa(kmx),aap(kmx),gaa(klam),gaap(klam),brnch(klam),
     2  icut(66),sgfcut(66),sfcut(66),didgf(66),dsgfdgf(66),title(20),
     3  ficut(66),fnucut(66),fnuplt(82),sgf(66),fiplt(82),sgfplt(82),
     4  ncser(klam),aat(kmx),aapt(kmx)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c6/emin,emax,sumwt,flbdn,flbdx,fnunp,fnuxp,nlev,iewr
      common/c3lc1/d(kmx,kmx)
      common/c3lc2/v(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/cta(kmx,kmx)
      common/c3lc5/goss(ktran,kbgks)
      common/c3lc6/gaaxc(kmx,kexc),brnchx(kexc,kexc)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),xx(kmx)
      common/spec/fjt(klam),fjtp(klam),ncserp(klam),gaat(klam),
     1  gaapt(klam)
      common/ctkev/nptkev,ntkev,tkev(6),tunrgy(6),brnchl(6),brncht(6),
     1  eionry
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      dimension vv(kmx*(kmx-24))
      dimension x(klam),fnu(klam),flam(klam),sumfj(kc)
      equivalence (cta,x,fnu),(ct4,flam),(vv,v(1,23))
      dimension spid(3)
      common/c7/g1,sumgaa,sumgar,sumgaat
      common/c8/exc(kexc),jexc(kexc),mexc(kexc)
      common/energies/etot(kc,2),ee8(kc,2),e0kin(kc),ekin(kmx)
      common/pwborn/nbigks,nenrgs,ekminl,ekmaxl,delekl,bigks(kbgks),
     1  xmin,xmax,tev(25),corr(kenrgs),ibk,ipwr,
     2  i11,ijkion,ijkiz,newstyle,temps(25),xsec(kenrgs)

      dimension iser(klam),exc1(kexc),jexc1(kexc),delm1(kc)
      integer h18
      character*10 parnam(kpr,2)
      character br1*9,idfg(15)*11
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      character*8 lldes(klam),lldesp(klam)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 elem(3,kc,2),elem1(3),elemj(kexc),g,h,gold,hold
      character*18 abc,def,pid
      common/char2/elem,elem1
      data h1/1h /,h18/1h(/
      data (spid(i),i=1,3) /8h mag dip,8helec dip,8helec qud/
      character*1 cflag

C-------Start ADAS conversion-------

      common /batch/is_batch, i_tot, lldes,lldesp
 
      character*8 b_lldes(ktran), b_lldesp(ktran)
      common /bornextra/b_t(ktran), b_tp(ktran), b_fjt(ktran),
     &                  b_fjtp(ktran), b_lldes, b_lldesp,
     &                  b_ncser(ktran), b_ncserp(ktran), b_s2(ktran)
     
C-------Start ADAS conversion-------
c
c
c     kmx=500
c     kexc=kmx
c     klam=10000
c     ktran=1600
c
c          For special Lund Univ. print formats (on level-sort
c               line lists), set ilund=1; otherwise, set ilund=0.
c
      ilund=0
c
c          For paper with 66 lines per page, headings on every page,
c               set jlines=52.
c          For paper with 51 lines per page, headings every second
c               page, set jlines=84.
c
      jlines=52
c
c
cc                                87 06 02  joe
c          If ilund.eq.1.and.dmin.lt.0, delete upward transitions for
c                level sorts.
c          If ilund.eq.0.and.dmin.lt.0, delete all lines except those
c               arising from the lowest level of each parity.
c          If abs(dmin) gt 1.0 and le 50.0, delete weak lines on the
c               basis of ga<10**abs(dmin) rather than gf<abs(dmin).
      negtp=0
      if (ilund.eq.1.and.dmin.lt.0.0) negtp=1
      if (ilund.eq.0.and.dmin.lt.0.0) negtp=2
      dminga=abs(dmin)
      if (dminga.gt.1.0.and.dminga.le.50.0) then
        iminga=dminga
        dminga=10**iminga
        dmin=0.0
      else
        dminga=0.0
      endif
cc                                 slut
      if (iarg1.eq.0) go to 92
      if (igen.lt.5) go to 92
      read (ir,8) nbigks,ekminl,ekmaxl,delekl,nenrgs,xmin,xmax
    8 format (i5,3f15.10,i10,2f10.2)
      write (iw,9) nbigks,ekminl,ekmaxl,delekl,nenrgs,xmin,xmax
    9 format (//i5,3f15.10,i10,2f10.2)
      do 90 i=1,ktran
      do 90 j=1,nbigks
   90 goss(i,j)=0.0
   92 ii=iarg1
      ie1=iarg2
      ie2=iarg3
      tunrgy(1)=9.9e10
      do 94 i=2,6
   94 tunrgy(i)=tkev(i)*8065479/uenrgy
      uenrev=uenrgy/8065.479
      gold=" "
      hold=" "
      ipwr=1
      ibk=0
      
C------Start ADAS conversion-------

      write(52,'(1x,a9)')spid(ii+1)                                          
      write(77,'(1x,a9)')spid(ii+1) 

C------End ADAS conversion-------

c
c          Read reduced mupole matrix elements
c
      iread=0
      if (ii.eq.0) go to 104
  100 read (ir,12) ieras,n
   12 format (50x,a1,6x,i1)
      backspace ir
      if (ieras.ne.h18) go to 999
      if (igen.lt.5.and.n.ne.ii) go to 999
      ipwr=n
      iptg=0
      if (ibk.eq.0.or.(ipwr.gt.irmn.and.ibk.eq.1)) iptg=1
      if (ii.ne.1) iptg=0
      if (iptg.eq.1) write (iw,13) spid(ii+1)
   13 format (1h1,59x,a9,9h spectrum////)
      iback=iread
      if (igen.ge.5.and.n.ne.irmn) iback=0
      if (iback.eq.0) go to 104
      do 102 i=1,iread
  102 backspace ic
      iread=0
  104 l1=ie1-ila+1
      l2=ie2-ila+1
      jnx1=nsconf(2,l1)
      jnx2=nsconf(2,l2)
      jnx=max0(jnx1,jnx2)
      do 110 i=1,jnx1
      do 110 j=1,jnx2
      pmup(i,j)=1.0
      if (ii.eq.0) go to 110
  105 read (ir,14) abc,def, pmup(i,j), g,h, frac, pid
   14 format (a18,2x,a16,f14.5, a6,a8, f6.4, a2,2e4.1)
  106 if (iptg.eq.1) write(iw,15) i,abc,j,def,g,h,pmup(i,j),frac,pid
   15 format (i5,5x,a18,i10,5x,a18,10x,a6,a8,1h=,f15.7,6x,5hfrac=,f7.4,
     1  5x,a2)
      if (frac.eq.0.0) frac=1.0e-5
      if (pmup(i,j).ne.0.0) delm1(j)=1.0/frac**2
      csigma=0.0
      if (g.eq.gold.and.h.eq.hold) csigma=8.06662e-18
      gold=g
      hold=h
  110 continue
c          csigma.gt.0.0 for photoionization cross-section calc (sigma)
      if (nsconf(3,2).ge.0.or.l1.eq.l2.or.texcit.gt.0.0) csigma=0.0
c
      fnux=1.0e8/(flbdn*uenrgy)
      fnun=1.0e8/(flbdx*uenrgy)
      assign 57 to ifmt1
      assign 58 to ifmt2
      if (csigma.gt.0.0) go to 150
      assign 56 to ifmt1
      if (texcit.eq.0.0) go to 150
      assign 51 to ifmt1
      assign 53 to ifmt2
c
c
c
  150 rewind ie1
      rewind ie2
      rewind 20
      fnunp=1.0e20
      fnuxp=0.0
      sj31=100.0
      sj32=100.0
      pmax=sqrt(2.0/3.0)
      pmax=0.0001
      do 160 i=1,jnx1
      do 160 j=1,jnx2
      if (abs(pmup(i,j)).gt.abs(pmax)) pmax=pmup(i,j)
  160 continue
      a=1.0/pmax
      psq=3.0375e-6*(pmax**2)
      if (ii.eq.0) psq=psq*(0.5/137.04)**2
      if (igen.ge.5) go to 162
      if (ii.eq.2) psq=psq*((3.14159*0.529)**2)/5.0
      go to 163
  162 psq=psq*3.0
  163 sums2=0.0
      sums2n=0.0
      sums2m=0.0
      sumgf=0.0
      sumf=0.0
      sumgar=0.0
      do 170 j=1,66
      icut(j)=0
      sgfcut(j)=0.0
  170 sfcut(j)=0.0
      i=0
      i0=0
      i0n=0
      i0x=0
      ijjp=0
      ilostt=0
      icdlt=0
      ibrnch=0
      d0max=0.0
      do 175 l=1,kmx
      do 175 j=1,kexc
  175 gaaxc(l,j)=0.0
  180 if (iptg.eq.1) write (iw,18)
   18 format (////18x,12hNo. of lines/
     1  5x,1hJ,5x,2hJp,5x,4hJ-Jp,3x,5hTotal,3x,4hklam,4x,5hilost,
     2  8x,4hdmin,6x,3hi0 ,5x,3hi0n,5x,3hi0x,3x,6hitrunc,3x,5hnTkeV/)
c
c          Read dipole matrix for basis vectors
c
  200 read (ic) scrj8,scrj8p,nls,nlsp, ((d(l,lp),l=1,nls), lp=1,nlsp),
     1  ncfg,ncfgp
      iread=iread+1
      if (scrj8.lt.0.0) go to 560
      do 220 l=1,nls
      do 220 lp=1,nlsp
      in=ncfg(l)
      jn=ncfgp(lp)
  220 d(l,lp)=d(l,lp)*a*pmup(in,jn)
  250 itrunc=i
      i00=i0
      i0n0=i0n
      i0x0=i0x
      sums20=sums2
      sumsn0=sums2n
      sumsm0=sums2m
      ilost=0
c
c          Read eigenvalues and vectors,
c               and calc mupole matrix for actual eigenvectors
c
  300 if (scrj8-sj31) 301,302,305
  301 continue
  302 rewind ie1
c 301 backspace ie1
c     backspace ie1
c 302 backspace ie1
c     backspace ie1
  305 read (ie1) sj31,nls, (eig(lp), (v(l,lp),l=1,nls), lp=1,nls),
     1  lcser,ldes
      read (ie1) lx, (aa(lp),aat(lp), lp=1,nls)
      if (scrj8-sj31) 301,308,305
  308 if (ie1.eq.ie2) sj32=sj31
      if (lx.ne.0.and.lx.ne.nls) ibrnch=7
      do 360 l=1,nls
      do 360 lp=1,nlsp
      cta(l,lp)=0.0
      ct4(l,lp)=0.0
      do 355 k=1,nls
      eras=v(k,l)*d(k,lp)
      cta(l,lp)=cta(l,lp)+abs(eras)
  355 ct4(l,lp)=ct4(l,lp)+eras
  360 continue
  400 if (scrj8p-sj32) 401,402,405
  401 continue
  402 rewind ie2
c 401 backspace ie2
c     backspace ie2
c 402 backspace ie2
c     backspace ie2
  405 read (ie2) sj32,nlsp, (eigp(lp), (v(l,lp),l=1,nlsp), lp=1,nlsp),
     1  lcserp,ldesp
      read (ie2) lxp, (aap(lp),aapt(lp), lp=1,nlsp),
     1  nlst,ixc,ixc1, (exc(j), (gaaxc(lp+nlst,j),lp=1,lxp), j=1,ixc1)
      if (scrj8p-sj32) 401,408,405
  408 if (ie1.eq.ie2) sj31=sj32
      if (lxp.ne.0.and.lxp.ne.nlsp) ibrnch=7
c
c          Calculate spectrum
c
      ijjp=0
      deldmin=0.1
      icontin=0
      do 520 l=1,nls
      do 520 lp=1,nlsp
      
C------Start ADAS conversion-------

C      if (eig(l).gt.-4000.0.and.eigp(lp).gt.-4000.0) go to 450
      if (eig(l).gt.-20000.0.and.eigp(lp).gt.-20000.0) go to 450
      
C------Start ADAS conversion-------

      icontin=7
      go to 520
  450 if (ie1.eq.ie2.and.scrj8.eq.scrj8p.and.l.gt.lp) go to 520
      erasa=0.0
      eras=0.0
      do 455 k=1,nlsp
      erasa=erasa+cta(l,k)*abs(v(k,lp))
  455 eras=eras+ct4(l,k)*v(k,lp)
      erass=eras*abs(eras)
      eras=eras*eras
      sums2=sums2+eras
      if (ie1.ne.ie2) go to 490
      if (scrj8.ne.scrj8p) go to 480
      if (l.eq.lp) go to 520
  480 sums2=sums2+eras
  490 continue
      d0=eras
      eras1=abs(eig(l)-eigp(lp))
      if (lcser(l).le.nck(1).and.lcserp(lp).le.nck(2)) go to 494
      if (l1.ne.l2) go to 492
      if (lcserp(lp).le.nck(1).and.lcser(l).le.nck(2)) go to 494
  492 icdlt=icdlt+1
      go to 520
  494 sums2n=sums2n+eras
      if (l1.eq.l2.and.scrj8.eq.scrj8p) sums2n=sums2n+eras
      if (eras1.ge.fnun) go to 498
      i0n=i0n+1
      go to 520
  498 if (eras1.le.fnux) go to 499
      i0x=i0x+1
      go to 520
  499 if (texcit.le.0.0) go to 501
      temp=eras1*uenrgy
      ga=0.6698*psq*eras*(temp**3)
      d0=ga*exp(-(max(eig(l),eigp(lp))-emean)/texcit)
  501 if (d0.ge.abs(dmin)) go to 510
      i0=i0+1
      if (d0.gt.d0max) d0max=d0
      go to 520
  510 if (i.lt.klam) go to 515
      if (itrunc.gt.0) go to 511
      if (itrunc.eq.0) go to 513
  511 i=itrunc
      i0=i00
      i0n=i0n0
      i0x=i0x0
      sums2=sums20
      sums2n=sumsn0
      sums2m=sumsm0
      if (dmin.le.0.0.and.ntkev.eq.0) go to 560
      if (dmin.le.0.0.and.texcit.gt.0.0) dmin=1.0e4
      if (dmin.le.0.0) dmin=0.00001
      do 512 i=1,iread
  512 backspace ic
      iread=0
      dmin=dmin+min(dmin,deldmin)
      if (dmin.gt.4.0) dmin=2.0*dmin
      go to 150
  513 ilost=ilost+1
      go to 520
  515 i=i+1
      ijjp=ijjp+1
      eras1=1.e-6*(scrj8+0.01*l)
      t(i)=eig(l)+eras1
      fjt(i)=scrj8
      eras2=1.e-6*(scrj8p+0.01*lp)
      tp(i)=eigp(lp)+eras2
      fjtp(i)=scrj8p
      if (igdlev.eq.0.or.i.eq.1) go to 518
      if (t(i).eq.t(1)) go to 518
      if (t(i).lt.t(1)) go to 517
      i=i-1
      ijjp=max0(ijjp-1,0)
      go to 520
  517 t(1)=t(i)
      fjt(1)=fjt(i)
      tp(1)=tp(i)
      fjtp(1)=fjtp(i)
      ijjp=max0(ijjp-i+1,0)
      i=1
      sums2m=0.0
  518 gaa(i)=(2.0*scrj8+1.0)*aa(l)
      gaap(i)=(2.0*scrj8p+1.0)*aap(lp)
      gaat(i)=(2.0*scrj8+1.0)*aat(l)
      gaapt(i)=(2.0*scrj8p+1.0)*aapt(lp)
      s2(i)=eras
      sums2m=sums2m+eras
      soabss(i)=(erass+1.0e-30)/(erasa*erasa+1.0e-30)
      iser(i)=lp-(nlsp-lxp)+nlst
      ncser(i)=lcser(l)
      ncserp(i)=lcserp(lp)
      lldes(i)=ldes(l)
      lldesp(i)=ldesp(lp)
  519 if (ie1.ne.ie2.or.t(i).lt.tp(i)) go to 520
      t(i)=eigp(lp)+eras2
      fjt(i)=scrj8p
      tp(i)=eig(l)+eras1
      fjtp(i)=scrj8
      eras=gaa(i)
      gaa(i)=gaap(i)
      gaap(i)=eras
      eras=gaat(i)
      gaat(i)=gaapt(i)
      gaapt(i)=eras
      ncser(i)=lcserp(lp)
      ncserp(i)=lcser(l)
      lldes(i)=ldesp(lp)
      lldesp(i)=ldes(l)
  520 continue
  550 if (iptg.eq.1.and.dmin.le.9999.0) write (iw,52) scrj8,scrj8p,ijjp,
     1  i,klam,ilost,dmin,i0,i0n,i0x,itrunc,ntkev
   52 format (1h ,2f6.1,4i8,f15.6,i6,4i8)
      if (iptg.eq.1.and.dmin.gt.9999.0) write (iw,49) scrj8,scrj8p,ijjp,
     1  i,klam,ilost,dmin,i0,i0n,i0x,itrunc,ntkev
   49 format (1h ,2f6.1,4i8,1p,e15.3,i6,4i8)
      if (i.eq.klam.and.ilost.gt.0) go to 560
      go to 200
c
c          Sort lines by levels of first parity
c
C-------Start ADAS conversion-------

  560 continue
      
       if (is_batch.eq.-1) write(6,*)i
 
C-------Start ADAS conversion-------

  
      imax=i
      if (imax.eq.0) go to 980
      ilostt=ilostt+ilost
      ipt=0
      if (mod(ispecc,2).ne.0) ipt=1
      if (ispecc.eq.9) ipt=0
      if (igen.ge.5) ipt=iptg
      if (ipt.eq.0.and.igen.lt.5) go to 567
      do 562 i=1,imax
  562 x(i)=1.0e8*t(i)+tp(i)
      call sort2(imax,12,x,iser,t,ncser,ncserp,tp,fjt,fjtp,s2,soabss,
     1  gaa,gaap,gaat)	
      if (imax.le.1) go to 563
      call order2(imax,gaapt)	
      call orderch(imax,lldes)	
      call orderch(imax,lldesp)
  563 if (igen.lt.5) go to 567
      if (imax.le.ktran) go to 565
      write (iw,54) ktran,imax
   54 format (//26h0*****WARNING-- Only first,i5,3h of,i6,
     1  42h lines retained, for lack of storage space///)
      imax=ktran
  565 if (ipwr.ne.irmn) go to 567
      ek=ekminl+delekl*ibk
      ibk=ibk+1

C-------Start ADAS conversion-------

c      if (ibk.gt.kbgks) stop 57
      if (ibk.gt.kbgks) then
        write(0,'(a,1x,a9)')'Too many transitions for ', spid(ii+1)
        stop 'Increase ktran'
      endif
 
C-------Start ADAS conversion-------

      bigks(ibk)=ek
  567 sumfiu=0.0
      sumfid=-0.0
      sumgfph=0.0
      jcx=min0(nsconf(2,2),nck(2))
      do 570 jc=1,jcx
  570 sumfj(jc)=0.0
      sumga=0.0
      s2max=0.0
      gfmax=0.0
      i1=1
      j=0
      jnxx=jnx
      ijoe=1

      do 599 i=1,imax
      if (j.gt.0) go to 575
      if (ipt.eq.0) go to 575
      if (ilund.ne.0.and.i.gt.1) go to 575
      write (iw,55) spid(ii+1),uenrgy,uenrev,
     1  (jn, (elem(l,jn,l1),l=1,3), (elem(l,jn,l2),l=1,3), jn=1,jnxx)
   55 format (1h1,9x,a8,9h spectrum,5x,21h(Energies in units of,
     1  f8.1,7h cm-1 =,f8.2,4h eV)//(i5,6x,3a6,12h   ---      ,3a6))
      if (ilund.eq.0) go to 572
      write (iw,1056)
 1056 format (/'            E      J     Level        delta E ',
     1   ' lambda(a)  log gf ga(sec-1) cf/br'/)
      go to 575
  572 if (texcit.eq.0.0) write (iw,ifmt1)
      if (texcit.gt.0.0) write (iw,ifmt1) texcit
   51 format (/1h0,8x,7h   E   , 3x,8hJ   Conf,11x,8h   Ep   , 2x,
     1  4h Jp ,6h Confp,10x,
     2  8h delta E,3x,9hlambda(a),10h s/pmax**2,3x,2hgf,10h ga*bltzmn,
     3  10h ga(sec-1),9h cf,brnch/103x,6h(texc=,f7.1,1h))
   56 format (/1h0,8x,7h   E   , 3x,8hJ   Conf,11x,8h   Ep   , 2x,
     1  4h Jp ,6h Confp,10x,
     2  8h delta E,3x,9hlambda(a),1x,9hs/pmax**2,3x,2hgf,4x,6hlog gf,1x,
     3  9hga(sec-1),9h cf,brnch/)
   57 format (/1h0,8x,7h   E   , 3x,8hJ   Conf,11x,8h   Ep   , 2x,
     1  4h Jp ,6h Confp,10x,
     2  8h delta E,3x,9hlambda(a),1x,9hs/pmax**2,3x,2hgf,4x,6hlog gf,1x,
     3  10hsigma(cm2),5h   cf/)
      j=jlines-jnxx
      jnxx=min0(5,jnx)
  575 fnu(i)=abs(t(i)-tp(i))
      eras1=fnu(i)*uenrgy
      flam(i)=1.0e8/eras1
      if (s2(i).gt.s2max) s2max=s2(i)
      gf=psq*s2(i)*eras1

C-------Start ADAS conversion-------
C
C      if (igen.ge.5) goss(i,ibk)=goss(i,ibk)+gf

      if (igen.ge.5.and.ibk.gt.0) then
      
        goss(i,ibk) =  goss(i,ibk)+gf
        b_t(i)      =  t(i)     
        b_tp(i)     =  tp(i)    
        b_fjt(i)    =  fjt(i)   
        b_fjtp(i)   =  fjtp(i)  
        b_lldes(i)  =  lldes(i) 
        b_lldesp(i) =  lldesp(i)
        b_ncser(i)  =  ncser(i) 
        b_ncserp(i) =  ncserp(i)
        b_s2(i)     =  s2(i)    

        if (i.GT.ktran) then
          stop 'Too many transitions - increase ktran'
        endif
      
      endif
                 
C-------End ADAS conversion-------
      
      if (igen.lt.5.and.ii.eq.2) gf=gf/(flam(i)**2)
      alggf=-99.999
      if (gf.gt.0.0) alggf=log10(gf)
      ga=0.66698*gf*eras1*eras1
      if (texcit.gt.0.0) alggf=ga*exp(-(max(t(i),tp(i))-emean)/texcit)
      jc=ncserp(i)
      if (csigma.gt.0.0) ga=csigma*gf*delm1(jc)/(2.0*fjt(i)+1.0)
      if (gf.gt.gfmax) gfmax=gf
      sumgf=sumgf+gf
      sumf=sumf+gf/(2.0*fjt(i)+1.0)
      if (ipt.eq.0) go to 599
      if (t(i).ne.t(1).and.negtp.eq.2) go to 578
      if (ilund.eq.0) go to 577
      nccc=ncser(i)
      if (ijoe.gt.0) write (iw,1057) t(i),fjt(i),nccc,lldes(i),
     1  (elem(l,nccc,l1),l=1,3)
 1057 format (5x,'* * *  ',f10.3,f5.1,i4,1x,a8,5x,3a6,' * * *',/)
      ijoe=0
      if (t(i).lt.tp(i).and.negtp.eq.1) go to 578
      if (ga.lt.dminga) go to 578
      write (iw,1058) i,tp(i),fjtp(i),
     1  ncserp(i),lldesp(i),fnu(i),flam(i),alggf,ga,soabss(i)
 1058 format (1h ,i4,f11.3,f5.1,i3,1x,a8,f12.3,
     1  f11.3,f8.3,1pe10.3,0pf6.2)
      go to 578
  577 write (iw,ifmt2) i,t(i),fjt(i),ncser(i),lldes(i),tp(i),fjtp(i),
     1  ncserp(i),lldesp(i),fnu(i),flam(i),s2(i),gf,alggf,ga,soabss(i)
   53 format (1h ,i4,f11.4,f5.1,i3,1x,a8,f13.4,f5.1,i3,1x,a8,f13.4,
     1  f12.4,f8.3,f9.5,1p,2e9.2,0p,f8.4)
   58 format (1h ,i4,f11.4,f5.1,i3,1x,a8,f13.4,f5.1,i3,1x,a8,f13.4,
     1  f12.4,f8.3,f9.5,f8.3,1pe10.3,0pf8.4)

C------Start ADAS conversion-------

        write(52,ifmt2)I,T(I),FJT(I),NCSER(I),LLDES(I),TP(I),FJTP(I),   
     &  NCSERP(I),LLDESP(I),FNU(I),FLAM(I),S2(I),GF,ALGGF,GA,SOABSS(I)  
                                                                        
      if (igen.lt.9) then
 
         write (77,32) i,ncser(i),ncserp(i),lldes(i),lldesp(i),
     &                 t(i),fjt(i),tp(i),fjtp(i),
     &                 fnu(i),flam(i),s2(i),gf,alggf,ga
         write (77,*)'    0'
 
  32     format (1h ,i4,2i2,2(1x,a8,1x),f13.3, f6.1,f12.3,
     &           f6.1,f10.2,f12.3,f9.4,f9.4,f9.4,1pe12.3)
 
      endif
 

C------End ADAS conversion-------




      j=j-1
  578 if (t(i).gt.tp(i)) go to 580
      sumfiu=sumfiu+gf
      sumfj(jc)=sumfj(jc)+gf
      go to 584
  580 sumga=sumga+ga
      sumfid=sumfid-gf
  584 continue
      if (abs(iw6).le.6) go to 585
      if (tp(i).le.t(i)) go to 1584
      sumgfph=sumgfph+gf
 1584 if (i.eq.imax) go to 2584
      if (abs(tp(i+1)-tp(i)).lt.1.0e-6*tp(i)) go to 585
 2584 jjj=ncser(i)
      jjjp=ncserp(i)
      ephoton=etot(jjjp,2)-etot(jjj,1)
      ebd=tp(i)-ee8(jjjp,2)*109737.31/uenrgy
      delebdbd=ebd-t(i)
      ek=ephoton-delebdbd
      write (iw,4584) ephoton,ebd,delebdbd,sumgfph,ek
 4584 format (' Ephoton(av)=',f9.4,5x,'   Ebound=',f9.4,8x,
     1  '   dele-bdbd=',f9.4,12x,'  sumgf=',f9.4,6x,'   Ekin=',f9.4)
      sumgfph=0.0
  585 if (i.eq.imax) go to 586
      if (t(i+1).eq.t(i)) go to 599
  586 eras=2.0*fjt(i)+1.0
      flife=9.9946e33
      if (sumga.gt.0.0) flife=eras/sumga
      eras=1.0/eras
      sumfiu=sumfiu*eras
      sumfid=sumfid*eras
      sumfi=sumfiu+sumfid
      if (ilund.eq.0) go to 588
      ijoe=1
      write (iw,1059) sumfiu,sumfid,sumfi,flife,sumga
 1059 format (/1x,6hsumfi=,f7.4,f8.4,2h =,f8.4,2x,
     1  9hrad.life=,1pe10.3,4h sec,2x,6hsumga=,e10.3/)
      go to 589
  588 write (iw,59) t(i),fjt(i),sumfiu,sumfid,sumfi
     1,flife,sumga
   59 format (5x,f13.4,f5.1,17x,6hsumfi=,f7.4,f8.4,2h =,f8.4,5x,
     1  9hlifetime=,1pe10.3,4h sec,5x,6hsumga=,e10.3)
  589 do 590 jc=1,jcx
  590 sumfj(jc)=sumfj(jc)*eras
      if (jcx.gt.1) write (iw,60) (sumfj(jc),jc, jc=1,jcx)
   60 format (6x,13hsumf by conf=,10(f8.4,i3)/19x,10(f8.4,i3))
      do 592 l=i1,i
      brnch(l)=0.0
      if (t(l).lt.tp(l)) go to 592
      brnch(l)=gaat(l)+sumga
      if (brnch(l).le.0.0) go to 592
      eras1=fnu(l)*uenrgy
      brnch(l)=gaa(l)*0.66698*psq*s2(l)*(eras1**3)/brnch(l)
  592 continue
      i1=i+1
      sumfiu=0.0
      sumfid=-0.0
      do 595 jc=1,jcx
  595 sumfj(jc)=0.0
      sumga=0.0
      write (iw,61)
   61 format (1h )
      if (ilund.eq.0) go to 598
      ijoe=1
      write (iw,2061)
 2061 format ('      =  =  =  =  =  =  =  =  =  =  =  =  =  =  ='/)
      go to 599
  598 j=j-2
      if (jcx.gt.1) j=j-(jcx+9)/10
  599 continue
      call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,2059) delt
 2059 format (' Finished first-parity sort at time=',f8.3,' min')
c
c          Sort lines by levels of second parity
c
      if (csigma.gt.0.0) assign 56 to ifmt1
      ipt=0
      if (ispecc.ge.8.or.icontin.ne.0) ipt=1
      if (mod(ispecc/2,2).ne.0) ipt=2
      if (ipt.eq.0) go to 700
      brnchr=0.0
      do 602 i=1,6
  602 brncht(i)=0.0
      brncha=0.0
      do 605 j=1,kexc
      do 604 i=1,kexc
  604 brnchx(i,j)=0.0
      mexc(j)=j
      eras=10000*jexc(j)
  605 vv(j)=eras+exc(j)
      call sort2(ixc1,3,vv,mexc,exc,jexc,idum,dum,dum,dum,dum,
     1  dum,dum,dum,dum)
      do 608 i=1,imax
  608 vv(i)=t(i)+1.0e9*tp(i)
      call sort2(imax,12,vv,iser,t,ncser,ncserp,tp,fjt,fjtp,
     1  s2,soabss,gaa,gaap,gaat)
      if (imax.le.1) go to 609
      call order2(imax,gaapt)
      call order2(imax,brnch)
      call orderch(imax,lldes)
      call orderch(imax,lldesp)
  609 sumfiu=0.0
      sumfid=-0.0
      sumga=0.0
      i1=1
      j=0
      jnxx=jnx
      ijoe=1
      do 640 i=1,imax
      fnu(i)=abs(t(i)-tp(i))
      eras1=fnu(i)*uenrgy
      flam(i)=1.0e8/eras1
      if (j.gt.0) go to 610
      if (ipt.lt.2) go to 610
      if (ilund.ne.0.and.i.gt.1) go to 610
      write (iw,55) spid(ii+1),uenrgy,uenrev,
     1  (jn, (elem(l,jn,l1),l=1,3), (elem(l,jn,l2),l=1,3), jn=1,jnxx)
      if (ilund.eq.0) go to 607
      write (iw,1056)
      go to 610
  607 if (texcit.eq.0.0) write (iw,ifmt1)
      if (texcit.gt.0.0) write (iw,ifmt1) texcit
      j=jlines-jnxx
      jnxx=min0(5,jnx)
  610 gf=psq*s2(i)*eras1
      if (igen.lt.5.and.ii.eq.2) gf=gf/(flam(i)**2)
      alggf=-99.999
      if (gf.gt.0.0) alggf=log10(gf)
      ga=0.66698*gf*eras1*eras1
      if (texcit.gt.0.0) alggf=ga*exp(-(max(t(i),tp(i))-emean)/texcit)
      if (ipt.lt.2) go to 612
      if (tp(i).ne.tp(1).and.negtp.eq.2) go to 612
      if (ilund.eq.0) go to 611
      nccc=ncserp(i)
      if (ijoe.gt.0) write (iw,1057) tp(i),fjtp(i),nccc,lldesp(i),
     1  (elem(l,nccc,l2),l=1,3)
      ijoe=0
cc
      if (t(i).gt.tp(i).and.negtp.eq.1) go to 612
      if (ga.lt.dminga) go to 612
      write (iw,1058) i,t(i),fjt(i),ncser(i),lldes(i),
     1  fnu(i),flam(i),alggf,ga,soabss(i)
      go to 612
  611 write (iw,ifmt2) i,t(i),fjt(i),ncser(i),lldes(i),tp(i),fjtp(i),
     1  ncserp(i),lldesp(i),fnu(i),flam(i),s2(i),gf,alggf,ga,soabss(i)

C------Start ADAS conversion-------

        write(52,ifmt2)I,T(I),FJT(I),NCSER(I),LLDES(I),TP(I),FJTP(I),   
     &  NCSERP(I),LLDESP(I),FNU(I),FLAM(I),S2(I),GF,ALGGF,GA,SOABSS(I)  

C------End ADAS conversion-------
     
     
      j=j-1
  612 if (t(i).lt.tp(i)) go to 615
      sumfiu=sumfiu+gf
      go to 617
  615 sumga=sumga+ga
      sumgar=sumgar+ga
      sumfid=sumfid-gf
  617 continue
      if (i.eq.imax) go to 620
      if (tp(i+1).eq.tp(i)) go to 640
  620 eras=2.0*fjtp(i)+1.0
      flife=9.9946e33
      if (sumga.gt.0.0) flife=eras/sumga
      eras=1.0/eras
      sumfiu=sumfiu*eras
      sumfid=sumfid*eras
      sumfi=sumfiu+sumfid
      if (ilund.eq.0) go to 621
      ijoe=1
      if (ipt.ge.1)
     1  write (iw,1059) sumfiu,sumfid,sumfi,flife,sumga
      go to 1621
  621 if (ipt.ge.1)
     1  write (iw,62) tp(i),fjtp(i),sumfiu,sumfid,sumfi,flife,sumga
   62 format (12x,f13.3,f6.1,9x,6hsumfi=,f7.4,f8.4,2h =,f8.4,5x,
     1  14hrad. lifetime=,1pe10.3,4h sec,5x,6hsumga=,e10.3)
 1621 brnch(i)=gaapt(i)+sumga
      if (brnch(i).le.0.0) go to 630
      brncha=brncha+gaap(i)*(gaapt(i)-gaap(i))/brnch(i)
      brion=gaapt(i)/brnch(i)
      if (ipt.lt.1.or.brion.le.0.0) go to 622
      flife=1.0/(eras*(sumga+gaapt(i)))
      write (iw,63) brion,flife,gaapt(i)
   63 format (55x,6hbrion=,1pe10.3,5x,14htot. lifetime=,e10.3,
     1  4h sec,4x,7hgaatot=,e10.3)
      j=j-1
  622 if (ixc.eq.0) go to 625
      j2=iser(i)
      do 624 jj=1,ixc
      m1=mexc(jj)
      jexc1(jj)=jexc(jj)
      exc1(jj)=exc(jj)
      do 624 jk=1,ixc
      m2=mexc(jk)
      brnchx(jj,jk)=brnchx(jj,jk)+gaaxc(j2,m1)*gaaxc(j2,m2)/brnch(i)
  624 continue
  625 eras=0.0
      do 627 l=i1,i
      ga=0.0
      if (t(l).gt.tp(l)) brnch(l)=0.0
      if (t(l).gt.tp(l)) go to 627
      ga=0.66698*psq*s2(l)*((fnu(l)*uenrgy)**3)
      brnch(l)=gaap(i)*ga/brnch(i)
      brnchr=brnchr+brnch(l)
      eras=eras+brnch(l)
  627 continue
c          Print t-dependence of brnch, summed over both
c               upper and lower levels
      if (ntkev.eq.0) go to 630
      nc=ncserp(i)
      ll=npav(nc)
      eras1=vpar(ll,l2)-tp(i)
      np1=ntkev+1
      do 629 l=1,np1
      brnchl(l)=eras*exp(eras1/tunrgy(l))
  629 brncht(l)=brncht(l)+brnchl(l)
      if (nptkev.gt.1) write (iw,64) (tkev(l),brnchl(l), l=1,np1)
   64 format (18h T(keV), brnch=   ,6(0pf6.1,1pe11.4))
      j=j-1
  630 continue
      i1=i+1
      sumfiu=0.0
      sumfid=-0.0
      sumga=0.0
      if (ilund.eq.0) go to 639
      ijoe=1
      write (iw,2061)
      go to 640
  639 write (iw,61)
      j=j-2
  640 continue
      jnxx=jnx
      if (nptkev.gt.0.and.brnchr.gt.0.0)
     1  write (iw,55) spid(ii+1),uenrgy,uenrev,
     2  (jn, (elem(l,jn,l1),l=1,3), (elem(l,jn,l2),l=1,3), jn=1,jnxx)
c
      write (iw,65) brnchr,brncha
   65 format (//14x,10h gm*frbar=,1pe11.4,10x,9hgm*fabar=,e11.4)
      np1=ntkev+1
      if (ntkev.gt.0) write (iw,64) (tkev(l),brncht(l), l=1,np1)
      if (nptkev.eq.0.or.nptkev.eq.2) go to 670
      jnxx=min0(jnxx,3)
      write (13,71) (elem(l,1,1),l=1,3),((elem(l,jn,2),l=1,3),jn=1,jnxx)
   71 format (4(3a6,2x))
      write (13,72) (tkev(l), l=2,np1)
   72 format (37x,11hT(keV)=inf.,2x,5f6.2)
      gm=1.0
      nn=nsconf(2,2)
      ll=0
      do 642 i=1,nosubc
      mm=nosubc-i+1
      eras=nijk(mm,nn,2)
      if (eras.gt.0.0) ll=ll+1
      if (ll.eq.1) go to 642
      eras1=4.0*fllijk(mm,nn,2)+2.0
      gm=gm*fctrl(eras1)/(fctrl(eras)*fctrl(eras1-eras))
  642 continue
c     decode (6,73,elem(2,nn,2)) ek
      write (11,173) elem(2,nn,2)
  173 format (a6)
      backspace 11
      read (11,73) ek
      backspace 11
   73 format (f6.2)
      ek=ek*0.0136058
      do 644 l=2,np1
  644 brncht(l)=brncht(l)/brncht(1)
      brncht(1)=brncht(1)/gm
c     encode (10,74,idfg) brncht(1)
      write (idfg,74) brncht(1)
   74 format (1pe10.3)
c     decode (10,75,idfg) a1,a2
      read (idfg,75) a1,a2
   75 format (a6,1x,a3)
c     encode (9,76,a3) a1,a2
      write (br1,76) a1,a2
   76 format (a6,a3)
      nc=ncserp(imax)
      ll=npav(nc)
      ekev=vpar(ll,l2)*uenrgy/8065479.0
      dekev=0.0
      if (eionry.ne.0.0) dekev=eionry*0.0136058+ek-ekev
      ekev=ekev+dekev
      if (brncht(np1).ge.1.0) write (13,77) (elem(l,1,1),l=1,2),
     1  ekev,gm,(elem(l,1,2),l=2,3),ek,br1,(brncht(l),l=2,np1)
   77 format (2a6,f8.4,f4.1,1x,a6,a3,f7.3,a9,5f6.3)
      if (brncht(np1).lt.1.0) write (13,78) (elem(l,1,1),l=1,2),
     1  ekev,gm,(elem(l,1,2),l=2,3),ek,br1,(brncht(l),l=2,np1)
   78 format (2a6,f8.4,f4.1,1x,a6,a3,f7.3,a9,5f6.4)
c          Sum brnch over upper levels
c               (for various T, and for each lower level)
      do 645 i=1,imax
  645 v(i,23)=t(i)
      call sort2(imax,12,v(1,23),iser,t,ncser,ncserp,tp,fjt,fjtp,
     1  s2,soabss,gaa,gaap,brnch)
      if (imax.le.1) go to 646
      call orderch(imax,lldes)
      call orderch(imax,lldesp)
  646 write (iw,66) (tkev(l),l=2,6)
   66 format (/8x,11hLower level, 13h  T(keV)=inf.,5f17.2)
      do 650 l=1,6
  650 brnchl(l)=1.0e-20
      do 665 i=1,imax
      fnu(i)=abs(t(i)-tp(i))
      nc=ncserp(i)
      ll=npav(nc)
      eras1=vpar(ll,l2)-tp(i)
      if (t(i).ge.tp(i)) go to 656
      do 654 l=1,np1
  654 brnchl(l)=brnchl(l)+brnch(i)*exp(eras1/tunrgy(l))
  656 if (i.eq.imax) go to 658
      if (t(i+1).eq.t(i)) go to 665
  658 write (iw,67) ncser(i),t(i),fjt(i), (brnchl(l), l=1,np1)
   67 format (i5,f13.4,f5.1,1pe12.4,5e17.4)
      do 660 l=2,np1
  660 brnchl(l)=brnchl(l)/brnchl(1)
      brnchl(1)=brnchl(1)/gm
c     encode (10,74,idfg) brnchl(1)
      write (idfg,74) brnchl(1)
c     decode (10,75,idfg) a1,a2
      read (idfg,75) a1,a2
c     encode (9,76,a3) a1,a2
      write (br1,76) a1,a2
      ekev=t(i)*uenrgy/8065479.0+dekev
      nc=ncser(i)
      if (brnchl(np1).ge.1.0) write (13,77) (elem(l,nc,1),l=1,2),
     1  ekev,fjt(i),(elem(l,1,2),l=2,3),ek,br1,(brnchl(l),l=2,np1)
      if (brnchl(np1).lt.1.0) write (13,78) (elem(l,nc,1),l=1,2),
     1  ekev,fjt(i),(elem(l,1,2),l=2,3),ek,br1,(brnchl(l),l=2,np1)
      do 663 l=1,6
  663 brnchl(l)=1.0e-20
  665 continue
      eras=-1.0
      write (13,77) h1,h1,eras
c          Write branching factors for autoionization
c               contributions to collisional excitation
  670 if (ixc.eq.0) go to 699
      write (iw,81) (jexc(j),exc(j), j=1,ixc)
   81 format (//25x,3hexc,6x,6hbrnchx//(32x,10(i3,f7.2)))
      write (iw,82)
      do 672 i=1,ixc
      j=jexc(i)
  672 write (iw,82) j, (elem(m,j,2),m=1,2), exc(i),(brnchx(i,l),l=1,ixc)
   82 format (i3,2x,2a6,f12.4,3x,1p,10e10.2/(32x,1p,10e10.2))
      if (ixc.le.1) go to 699
      ixc2=0
      if (nsconf(2,2).le.2) go to 690
c          Sum over l of continuum electron
      do 673 j=1,ixc
      n=jexc(j)
  673 elemj(j)=elem(2,n,2)
      netot=0
      do 675 n=1,nosubc
  675 netot=netot+nijk(n,1,2)
      nc=jexc(ixc)-jexc(1)
      if (nc.le.0) go to 690
      ixc1=ixc
      mixc=jexc(ixc)
      i=0
  676 i=i+1
      if (i.ge.ixc1) go to 684
      mi=jexc1(i)
      if (mi.eq.mixc) go to 684
      j=i
  678 j=j+1
      if (j.gt.ixc1) go to 676
      mj=jexc1(j)
      if (mj.eq.mi) go to 678
      if (abs(exc1(j)-exc1(i)).gt.0.01) go to 678
      ne=0
      do 679 n=1,nosubc
      ne=ne+max0(nijk(n,mi,2),nijk(n,mj,2))
      if (ne.ge.netot) go to 681
      if (nijk(n,mi,2).ne.nijk(n,mj,2)) go to 678
  679 continue
  681 do 682 n1=1,ixc
  682 brnchx(n1,i)=brnchx(n1,i)+brnchx(n1,j)
      ixc1=ixc1-1
      do 683 n=j,ixc1
      jexc1(n)=jexc1(n+1)
      exc1(n)=exc1(n+1)
      elemj(n)=elemj(n+1)
      do 683 n1=1,ixc
  683 brnchx(n1,n)=brnchx(n1,n+1)
      j=j-1
      go to 678
  684 if (ixc1.eq.ixc) go to 690
  685 write (iw,81) (jexc1(j),exc1(j), j=1,ixc1)
  686 write (iw,82)
      do 687 i=1,ixc
      j=jexc(i)
  687 write (iw,82) j,(elem(m,j,2),m=1,2), exc(i),(brnchx(i,l),l=1,ixc1)
      if (ixc2.gt.0) go to 699
c          Sum over levels within the core
  690 i=0
      ixc2=ixc1
  691 i=i+1
      if (i.eq.ixc2) go to 695
      mi=jexc1(i)
      j=i+1
  692 mj=jexc1(j)
      if (mj.ne.mi) go to 691
      do 693 n=1,ixc
  693 brnchx(n,i)=brnchx(n,i)+brnchx(n,j)
      ixc2=ixc2-1
      if (i.eq.ixc2) go to 695
      do 694 j1=j,ixc2
      jexc1(j1)=jexc1(j1+1)
      exc1(j1)=exc1(j1+1)
      do 694 n=1,ixc
  694 brnchx(n,j1)=brnchx(n,j1+1)
      go to 692
  695 if (ixc2.eq.ixc1) go to 699
      do 697 j=1,ixc2
      n=jexc1(j)
  697 elemj(j)=elem(2,n,2)
      write (iw,87) (jexc1(j),elemj(j), j=1,ixc2)
   87 format(//25x,3hexc,6x,6hbrnchx//(32x,10(i3,2x,a5)))
      ixc1=ixc2
      go to 686
  699 call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,2069) delt
 2069 format (' Finished second-parity sort at time=',f8.3,' min')
c
c          Sort lines by wavenumber
c
  700 ipt=0
      if (ispecc.ge.4) ipt=2
      if (ispecc.eq.9) ipt=1
      if (ipt.eq.0) go to 980
      call sort2(imax,12,fnu,iser,t,ncser,ncserp,tp,fjt,fjtp,
     1  s2,soabss,gaa,gaap,brnch)
      if (imax.gt.1) call orderch(imax,lldes)
      if (imax.gt.1) call orderch(imax,lldesp)
      fnunp=min(fnunp,fnu(1))
      fnuxp=max(fnuxp,fnu(imax))
      if (ispecc.ge.8) write (20) imax,psq, (fnu(i),s2(i),
     1  brnch(i), i=1,imax)
c
  708 j=0
      jnxx=jnx
      do 720 i=1,imax
      eras1=fnu(i)*uenrgy
      flam(i)=1.0e8/eras1
      gf=psq*s2(i)*eras1
      if (igen.lt.5.and.ii.eq.2) gf=gf/(flam(i)**2)
      alggf=-99.999
      if (gf.gt.0.0) alggf=log10(gf)
      ga=0.66698*gf*eras1*eras1
      if (texcit.gt.0.0) alggf=ga*exp(-(max(t(i),tp(i))-emean)/texcit)
      if (ilund.gt.0.and.i.gt.1) go to 710
      if (j.gt.0) go to 710
      if (ipt.eq.1) go to 709
      write (iw,55) spid(ii+1),uenrgy,uenrev,
     1  (jn, (elem(l,jn,l1),l=1,3), (elem(l,jn,l2),l=1,3), jn=1,jnxx)
      if (texcit.eq.0.0) write (iw,ifmt1)
      if (texcit.gt.0.0) write (iw,ifmt1) texcit
  709 j=jlines-jnxx
      jnxx=min0(5,jnx)
  710 j=j-1
      if (ipt.lt.2) go to 720
      if (ibrnch.eq.0) go to 715
      write (iw,88) i,t(i),fjt(i),ncser(i),lldes(i),tp(i),fjtp(i),
     1  ncserp(i),lldesp(i),fnu(i),flam(i),s2(i),gf,alggf,ga,brnch(i)
   88 format (1h ,i4,f11.4,f5.1,i3,1x,a8,f13.4,f5.1,i3,1x,a8,f13.4,
     1  f12.4,f8.3,f9.5,f8.3,1p,e10.3,e9.2)
      go to 720
  715 write (iw,ifmt2) i,t(i),fjt(i),ncser(i),lldes(i),tp(i),fjtp(i),
     1  ncserp(i),lldesp(i),fnu(i),flam(i),s2(i),gf,alggf,ga,soabss(i)
  720 continue
c
c
      call clock(time)
      delt=time-time0
      if (iptg.eq.1) write (iw,95) delt
   95 format (//' Finished wavelength sort at time=',f7.3,4h min//)
      if (iw6.lt.0) write (6,2095) delt
 2095 format (' Finished wavelength sort at time=',f9.3,4h min//)
  980 if (scrj8.lt.0.0) go to 985
      i=0
      if (imax.ne.itrunc) go to 180
      iread=iread-1
      backspace ic
      go to 200
c
  985 read (ic) sopi2
      iread=iread+1
      ssopx2=0.0
      do 986 i=1,jnx1
      do 986 j=1,jnx2
  986 ssopx2=ssopx2+sopi2(i,j)*(pmup(i,j)**2)
      ssopx2=ssopx2*(a**2)
      if (ispecc.lt.8) go to 988
      if (imax.eq.0) go to 988
      iras=5
      eras=-1.0
      write (20) iras,eras, (eras,eras,eras, i=1,iras)
      call wndist(ii)
  988 continue
c
      if (iptg.eq.0) go to 991
      call clock(time)
      delt=time-time0
      write (iw,96) delt,time,pmax,ssopx2,sums2n,sums2,sums2m,sumgf,
     1  sumf,sumgar,dmin,i0,d0max,i0n,flbdx,i0x,flbdn,ilostt,icdlt,nck
   96 format (//6h time=,f7.3,4h min,2x,10h(abs time=,f8.3,1h),5x,
     1   9hfor pmax=,f9.5,1h,,f17.4,f11.5/57x,12hsums2,sumgf=,f10.4,
     2  f11.5,f9.4/64x,5hsumf=,f30.4,9x,7hsumgar=,1pe11.4/57x,6hs2min=,
     3  0pf27.5//29x,i5,28h lines omitted, with max s2=,f28.5//29x,i5,
     4  31h lines omitted, with lambda.gt.,f14.4/29x,i5,
     5  31h lines omitted, with lambda.lt.,f14.4/29x,i5,
     6  31h lines omitted, insuff storage /29x,i5,
     7  36h lines omitted, conf serial nos .gt.,i4,1h,,i4)
      if (iw6.lt.0) write (6,96) delt,time,pmax,ssopx2,sums2n,sums2,
     1  sums2m,sumgf,sumf,sumgar,dmin,i0,d0max,i0n,flbdx,i0x,flbdn,
     2  ilostt,icdlt,nck
      write (iw,97) (coupl(i), i=1,7)
   97 format (///13h ave purities,29x,9(a6,2x)/)
      do 990 k1=l1,l2
      j1x=nsconf(2,k1)
      if (j1x.lt.2) j1x=2
      j1=j1x-1
      write(iw,98) ((elem(i,jn,k1), i=1,3), jn=1,j1x,j1),
     1  (avp(i,k1), i=1,7), avcg(k1),aveig(k1)
   98 format (1x,3a6,3h---,3a6,7f8.3,5x,6h  Eav=,f10.4/101x,6havEig=,
     1  f10.4)
  990 continue
c
  991 if (ii.eq.0) return
      if (igen.lt.5) go to 100
      if (igen.gt.7.or.mod(ibk-1,5).ne.0) go to 100
      imax=min0(imax,30)
  992 write (iw,99) ibk,bigks(ibk),ipwr,imax, (goss(i,ibk), i=1,imax)
   99 format (/46h0Weighted generalized osc. strengths at bigks(,i3,
     1  2h)=,f12.7,8h   ipwr=,i1,12h   for first,i3,6h lines//(10f12.8))
      go to 100
c

  999 return
      end
