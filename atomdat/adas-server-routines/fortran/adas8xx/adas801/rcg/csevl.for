C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/csevl.for,v 1.2 2004/07/06 12:23:53 whitefor Exp $ Date $Date: 2004/07/06 12:23:53 $
C
      function csevl (x,cs,n)
c          April 1977 version. W. Fullerton, C-3, Los Alamos Scientific Lab.
c
c          evaluate the n-term chebyshev series cs at x. adapted from
c          R. Broucke, Algorithm 446, C.A.C.M., 16, 254 (1973).  Also see Fox
c          and Parker, Chebyshev Polys in Numer. Analysis, Oxford Press, p.56.
c
c          input arguments --
c               x    value at which the series is to be evaluated.
c               cs   array of n terms of a chebyshev series. in eval-
c                      uating cs, only half the first coef is summed.
c               n    number of terms in array cs.
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      dimension cs(1)
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
c
      if (n.lt.1.or.n.gt.1000) write (iw,1) n
    1 format (24h *****error in csevl, n=,i7)
      if (x.lt.-1.0.or.x.gt.1.0) write (iw,2) x
    2 format (24h *****error in csevl, x=,f10.5)
c
       b1=0.
       b0=0.
       twox=2.*x
       do 10 i=1,n
       b2=b1
       b1=b0
       ni=n+1-i
       b0=twox*b1-b2+cs(ni)
 10    continue
c
       csevl = 0.5 * (b0-b2)
c
       return
      end
