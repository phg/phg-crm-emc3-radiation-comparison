C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/aknint.for,v 1.2 2004/07/06 11:15:58 whitefor Exp $ Date $Date: 2004/07/06 11:15:58 $
C
      subroutine aknint(x,y)
c
c          Aitken repeated interpolation
c
c   xbar = abscissa at which interpolation is desired
c   iabs(in) = no. of values in table
c   im   = degree of approximating polynomial
c   x    = vector of iabs(in) values of abscissa
c   y    = vector of iabs(in) values of ordinate
c   t    = temporary storage vector of 4*(m+1) locations)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
c
      dimension x(kbgks),y(kbgks),t(80)
      common/akn/xbar,n,m,yb
c
      do 10 j=1,n
      if (xbar.le.x(j)) go to 11
   10 continue
      j=n
   11 k=m+1
      j=j-k/2
      j=min0(j,n-m)
      j=max0(j,1)
      mend=j+m
      do 12 i=j,mend
      kk=i-j+1
      t(kk) = y(i)
   12 t(kk+k)=x(i)-xbar
      do 13 i=1,m
      kk=i+1
      do 13 jj=kk,k
      t(jj)=(t(i)*t(jj+k) -t(jj)*t(i+k))/(x(jj+j-1) - x(i+j-1))
   13 continue
      yb=t(k)
      return
      end
