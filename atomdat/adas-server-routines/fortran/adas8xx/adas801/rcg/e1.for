C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/e1.for,v 1.2 2004/07/06 13:34:19 whitefor Exp $ Date $Date: 2004/07/06 13:34:19 $
C
      function e1(x)
c
c          Computes the exponential integral e1(x).
c
c      July 1977 edition.  W. Fullerton, C-3, Los Alamos Scientific Lab.
c           Modified March 1982 by R.D. Cowan for x.gt.-1 only
c                and with abbreviated series for only 5-figure accuracy
c
c
c
c          series for e12        on the interval -1.le.x.le.+1
c
c          series for ae13       on the interval  +1.le.x.le.4
c
c          series for ae14       on the interval  4.le.x.le.100
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      dimension e12cs(7),ae13cs(7),ae14cs(7)
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
c
      data e12 cs( 1) /  -0.0373902147 /
      data e12 cs( 2) /   0.0427239860 /
      data e12 cs( 3) /   -.1303182079 /
      data e12 cs( 4) /    .0144191240 /
      data e12 cs( 5) /   -.0013461707 /
      data e12 cs( 6) /    .0001073102 /
      data e12 cs( 7) /   -.0000074299 /
      data ae13cs( 1) /   -.6057732466 /
      data ae13cs( 2) /   -.1125352434 /
      data ae13cs( 3) /    .0134322662 /
      data ae13cs( 4) /   -.0019268451 /
      data ae13cs( 5) /    .0003091183 /
      data ae13cs( 6) /   -.0000535641 /
      data ae13cs( 7) /    .0000098278 /
      data ae14cs( 1) /   -.1892918000 /
      data ae14cs( 2) /   -.0864811785 /
      data ae14cs( 3) /    .0072241015 /
      data ae14cs( 4) /   -.0008097559 /
      data ae14cs( 5) /    .0001099913 /
      data ae14cs( 6) /   -.0000171733 /
      data ae14cs( 7) /    .0000029856 /
      data xmax,nte12,ntae13,ntae14 /100.0,7,7,7/
c
c     if (xmax.ne.0) go to 40
c     nte12=7
c     ntae13=7
c     ntae14=7
c     xmax=100.0
c
 40   if (x.gt.1.) go to 50
      if (x.eq.0.0) write (iw,1)
    1 format (24h *****error in e1, x=0.0)
c
c          e1(x) = -ei(-x) for -1. .lt. x .le. 1.,  x .ne. 0.
c
      e1=(-log(abs(x))-0.6875+x)+csevl(x,e12cs,nte12)
      return
c
 50   if (x.gt.4.) go to 60
c
c          e1(x) = -ei(-x) for 1. .lt. x .le. 4.
c
      e1 = exp(-x)/x * (1.+csevl ((8./x-5.)/3., ae13cs, ntae13))
      return
c
 60   if (x.gt.xmax) go to 70
c
c          e1(x) = -ei(-x) for 4. .lt. x .le. xmax
c
      e1 = exp(-x)/x * (1. + csevl (8./x-1., ae14cs, ntae14))
      return
c
c          e1(x) = -ei(-x) for x .gt. xmax
c
   70 e1=0.0
      return
      end
