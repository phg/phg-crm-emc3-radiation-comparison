C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/s9j.for,v 1.2 2004/07/06 15:18:46 whitefor Exp $ Date $Date: 2004/07/06 15:18:46 $
C
      function s9j(a1,a2,a3,a4,a5,a6,a7,a8,a9)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      a9j=0.0
      a=max(abs(a1-a9), abs(a2-a6), abs(a4-a8))
      c=min(a1+a9, a2+a6, a4+a8)
      fj=2.0*a+1.0
      j1=fj
      j2=2.0*c+1.0
      if (j2.lt.j1) go to 302
      do 301 j=j1,j2,2
      a9j=a9j-((-1.0)**j)*fj*s6j(a1,a2,a3,a6,a9,a)
     1  *s6j(a4,a5,a6,a2,a,a8)*s6j(a7,a8,a9,a,a1,a4)
      a=a+1.0
  301 fj=fj+2.0
  302 s9j=a9j
      return
      end
