C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/calcfct.for,v 1.2 2004/07/06 11:59:10 whitefor Exp $ Date $Date: 2004/07/06 11:59:10 $
C
      subroutine calcfct
c
c          Note: If your compiler will not accept a dimension of the
c                form (0:200), this can be changed to (200) and the
c                statement  fct(0)=1.0  removed, provided the program
c                is compiled without bounds checking (and the compiler
c                places fctz immediately in front of fct).
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      common/cfactrl/scale,scalem1,fctn(200),fctz,fct(0:200)
c
      scale=8.0
      scalem1=1.0/scale
      fctz=1.0
      fctn(1)=0.0
      fct(0)=1.0
      fct(1)=1.0/scale
      do 100 i=2,70
      fctn(i)=0.0
  100 fct(i)=fct(i-1)*i/scale
      return
      end
