C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/blkdatarcg.for,v 1.2 2004/07/06 11:41:43 whitefor Exp $ Date $Date: 2004/07/06 11:41:43 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : ktran increased to 30000.
C               kcase increased to 500.
C               Extend pwborn common block definition.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      block data

      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      common/pwborn/nbigks,nenrgs,ekminl,ekmaxl,delekl,bigks(kbgks),
     1  xmin,xmax,tev(25),corr(kenrgs),ibk,ipwr,
     2  i11,ijkion,ijkiz,newstyle,temps(25),xsec(kenrgs)

c
      data parnam(1,1),parnam(1,2)/'Eav       ','Eav       '/
      data (coupl(i),i=1,12)/6h    LS,6h    JJ,6h  JJJK,6h  LSLK,
     1  6h  LSJK,6hLSJLKS,6hLSJLSJ,6hJ1-LSJ,1h ,1h ,1h ,6h    SL/
      data (lsymb(i),i=1,48)/1hs,1hp,1hd,1hf,1hg,1hh,1hi,
     1  1hk,1hl,1hm,1hn,1ho,1hq,1hr,1ht,1hu,1hv,1hw,
     2  1hx,1hy,1hz,1ha,1hb,1hc,1hS,1hP,1hD,1hF,1hG,1hH,1hI,
     3  1hK,1hL,1hM,1hN,1hO,1hQ,1hR,1hT,1hU,1hV,1hW,
     4  1hX,1hY,1hZ,1hA,1hB,1hC/
      data tev/1.0,1.5,2.0,3.0,5.0,7.0,10.0,15.0,20.0,30.0,
     1  50.0,70.0,100.0,150.0,200.0,300.0,500.0,700.0,1000.0,
     2  1500.0,2000.0,3000.0,5000.0,7000.0,10000.0/
      end
