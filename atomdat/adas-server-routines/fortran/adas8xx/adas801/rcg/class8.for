C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/class8.for,v 1.2 2004/07/06 12:06:21 whitefor Exp $ Date $Date: 2004/07/06 12:06:21 $
C
      subroutine class8
c
c          Calculate for irho.lt.irhop.lt.isig=isigp
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
      i=ix
      j=jx
      n1=nalsp(l1,i)
      n2=nalsp(l2,i)
      a1=pscrl(l1,i-1)
      a2=pscrl(l2,i-1)
      a3=pscrs(l1,i-1)
      a4=pscrs(l2,i-1)
      a5=pscrl(l1,i)
      a6=pscrl(l2,i)
      a7=pscrs(l1,i)
      a8=pscrs(l2,i)
      b1=fl(n1,i)
      b2=fl(n2,i)
      b3=s(n1,i)
      b4=s(n2,i)
      tfln=max(abs(fll(in)-a5),abs(a2-b1),abs(fll(i)-a6))
      tfsn=max(abs(0.5-a7),abs(a4-b3),abs(0.5-a8))
      tflx=min(fll(in)+a5,a2+b1,fll(i)+a6)
      tfsx=min(0.5+a7,a4+b3,0.5+a8)
      if (tfln.gt.tflx) go to 900
      if (tfsn.gt.tfsx) go to 900
c
      nii=1
      kj=nalsp(l1,j)-notsj1(j,j11)
      kpj=nalsp(l2,j)-notsj1(j,j12)
      do 120 iii=1,2
      l3=l1
      if (iii.eq.2) l3=l2
      do 120 ip=i,j
      ii=nalsp(l3,ip)
      tl(iii,ip)=fl(ii,ip)
      ts(iii,ip)=s(ii,ip)
      tscl(iii,ip)=pscrl(l3,ip)
  120 tscs(iii,ip)=pscrs(l3,ip)
      tl(1,i)=fll(in)
      tl(2,i)=fll(i)
      ts(1,i)=0.5
      ts(2,i)=0.5
c
      e1=sqrt((2.0*a1+1.0)*(2.0*a3+1.0)*(2.0*b2+1.0)*(2.0*b4+1.0))
      xx1=fll(i)-fll(in)+a2+a4-a1-a3+a6+a8
      tfl=tfln-1.0
      nlx=tflx-tfl
      nsx=tfsx-tfsn+1.0
      do 200 nl=1,nlx
      tfl=tfl+1.0
      tscl(1,i-1)=tfl
      tscl(2,i-1)=tfl
      e2=e1*(2.0*tfl+1.0)*s6j(fll(in),a2,a1, b1,a5,tfl)
     1  *s6j(a2,b1,tfl, fll(i),a6,b2)
      tfs=tfsn-1.0
      do 200 ns=1,nsx
      tfs=tfs+1.0
      tscs(1,i-1)=tfs
      tscs(2,i-1)=tfs
      e3=e2*(2.0*tfs+1.0)*s6j(half,a4,a3, b3,a7,tfs)
     1  *s6j(a4,b3,tfs, half,a8,b4)
      if (mod(xx1-tfl-tfs,two).ne.zero) e3=-e3
      tk=kdmin-2
      do 150 nn=1,nkd
      tk=tk+2.0
      call rdij(i,j)
      ccc(nn)=ccc(nn)+e3*a
  150 continue
      n1=nkd+1
      tk=kemin-2
      do 160 nn=n1,nktot
      tk=tk+2.0
      call reij(i,j)
      ccc(nn)=ccc(nn)+e3*rsum
  160 continue
  200 continue
c
  900 return
      end
