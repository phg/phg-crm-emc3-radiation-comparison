C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/ckdim.for,v 1.2 2004/07/06 12:05:18 whitefor Exp $ Date $Date: 2004/07/06 12:05:18 $
C
      subroutine ckdim
c
c        subroutine (called from lncuv) to check adequacy of dimensions
c          kls1, kls2,...kls6 for the variables u1,v1,u2,v2,...u8,v8,
c                                           and cfp1,cfp2,cfgp1,cfgp2
c        other dimensional checks are made at the end of subroutine plev
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      dimension kdim(8),nltn(14,2),nlpn(14,2),nlgpn(14,2),icfgp(2)
c
      kdim(1)=kls1
      kdim(2)=kls2
      kdim(3)=kls3
      kdim(4)=kls4
      kdim(5)=kls5
      kdim(6)=kls6
      kdim(7)=kls6
      kdim(8)=kls6
      ierror=0
      icfp=1
      icfgp(1)=1
      icfgp(2)=1
c
      nosubc=nsconf(1,1)
      do 699 i=1,nosubc
      do 680 k=1,2
      jmax=nsconf(2,k)
      if (jmax.le.0) go to 680
      nmax=0
      nltmax=0
      do 110 n=1,14
      nltn(n,k)=0
      nlpn(n,k)=0
  110 nlgpn(n,k)=0
c
      do 180 j=1,jmax
      n=nijk(i,j,k)
      if (n.le.0) go to 180
      if (nltn(n,k).ne.0) go to 180
      fll(i)=fllijk(i,j,k)
      call locdsk(id2,fll(i),n)
      read (id2) (ia,ia,ia,a,a,m=1,nlt),nlp,nlgp
      ia=ia
      nmax=max(n,nmax)
      nltmax=max(nlt,nltmax)
      if (nltmax.eq.nlt) nt=n
      nltn(n,k)=nlt
      nlpn(n,k)=nlp
      nlgpn(n,k)=nlgp
  180 continue
      l=fll(i)
      lsy=lsymb(l+1)
c
      if (nmax.le.1) go to 680
c          check dimensions for uk and vk
  205 if (nltmax.le.kdim(i)) go to 210
      write (iw,20) k,i,i,lsy,nt,nltmax,i,kdim(i)
      if (iw6.lt.0) write (6,20) k,i,i,lsy,nt,nltmax,i,kdim(i)
   20 format (' ***** parity=',i1,'  u',i1,' and v',i1,' for ',a1,i2,
     1  ' require dim. of ',i5,
     2  ':',5x,'dimension kls',i1,' is only ',i3)
      ierror=7
c
  210 nlpmax=0
      nlgpmax=0
      nt1=0
      nt2=0
      np=0
      ngp=0
      do 215 n=2,14
      if (nltn(n-1,k).eq.0.or.nltn(n,k).eq.0) go to 212
      if (nlpmax.ge.nlpn(n,k)) go to 212
      nlpmax=nlpn(n,k)
      nt1=nltn(n,k)
      np=n
  212 if (n.eq.2) go to 215
      if (nltn(n-2,k).eq.0.or.nltn(n,k).eq.0) go to 215
      if (nlgpmax.ge.nlgpn(n,k)) go to 215
      nlgpmax=nlgpn(n,k)
      nt2=nltn(n,k)
      ngp=n
  215 continue
c          Check dimensions for cfp within one parity
  220 if (np.eq.0) go to 230
      m=1
      if (i.gt.1) m=2
      if (nt1.le.kdim(m).and.nlpmax.le.kdim(m)) go to 230
      write (iw,22) k,m,lsy,np,nt1,nlpmax,m,kdim(m)
      if (iw6.lt.0) write (6,22) k,m,lsy,np,nt1,nlpmax,m,kdim(m)
   22 format (' ***** parity=',i1,'  cfp',i1,' for ',a1,i2,
     1  ' requires dims. of ',2i4,
     2  ':',5x,'dimension kls',i1,' is only ',i3)
      ierror=7
c          Check dimensions for cfgp
  230 if (ngp.eq.0) go to 240
      m=1
      if (icfgp(k).ge.2) m=2
      icfgp(k)=icfgp(k)+1
      if (nt2.le.kdim(m).and.nlgpmax.le.kdim(m)) go to 240
      write (iw,23) k,m,lsy,ngp,nt2,nlgpmax,m,kdim(m)
      if (iw6.lt.0) write (6,23) k,m,lsy,ngp,nt2,nlgpmax,m,kdim(m)
   23 format (' ***** parity=',i1,'  cfgp',i1,' for ',a1,i2,
     1  ' requires dims. of',2i4,
     2  ':',5x,'dimension kls',i1,' is only ',i3)
      ierror=7
c          Check dimensions for g,h,i... electrons
  240 if (l.le.3) go to 299
      write (iw,24) i,lsy,nmax,i,2*l+1,i,2*l-1
      if (iw6.lt.0) write (6,24) i,lsy,nmax,i,2*l+1,i,2*l-1
   24 format (' ***** WARNING:  For subshell ',i1,' (',a1,i2,'),'/20x,
     1  ' third dimension of u',i1,' and cfp or cfgp must be at least',
     2  i3/20x,' and that of v',i1,' must be at least',i3)
  299 continue
c
c          Check dimensions of inter-parity cfp
c
  610 if (k.eq.1) go to 680
      nlpmax=0
      nt1=0
      np=0
      do 615 n=2,14
      if (nltn(n-1,1).eq.0.or.nltn(n,2).eq.0) go to 612
      if (nlpmax.ge.nlpn(n,2)) go to 612
      nlpmax=nlpn(n,2)
      nt1=nltn(n,2)
      np=n
      go to 615
  612 if (nltn(n-1,2).eq.0.or.nltn(n,1).eq.0) go to 615
      if (nlpmax.ge.nlpn(n,1)) go to 615
      nlpmax=nlpn(n,1)
      nt1=nltn(n,1)
      np=n
  615 continue
c
  620 if (np.eq.0) go to 680
      m=1
      if (icfp.gt.1) m=2
      icfp=icfp+1
      if (nt1.le.kdim(m).and.nlpmax.le.kdim(m)) go to 680
      write (iw,62) m,lsy,np,nt1,nlpmax,m,kdim(m)
      if (iw6.lt.0) write (6,62) m,lsy,np,nt1,nlpmax,m,kdim(m)
   62 format (' ***** pars.1,2  cfp',i1,' for ',a1,i2,
     1  ' requires dims. of ',2i4,
     2  ':',5x,'dimension kls',i1,' is only ',i3)
      ierror=7
  680 continue
  699 continue
c
      if (ierror.eq.0) go to 799
      write (iw,59)
      if (iw6.lt.0) write (6,59)
   59 format (' ***** dimensions exceeded')
      stop ' (ckdim 59)'
  799 return
      end
