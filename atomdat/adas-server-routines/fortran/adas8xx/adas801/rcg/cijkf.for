C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/cijkf.for,v 1.2 2004/07/06 12:05:12 whitefor Exp $ Date $Date: 2004/07/06 12:05:12 $
C
      function cijkf(a,b,c)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      d=s3j0sq(a,b,c)
      d=sqrt((2.0*a+1.0)*(2.0*b+1.0)*d)
      two=2.0
      if (mod(0.5*(b+c-a),two).ne.0.0) d=-d
      cijkf=d
      return
      end
