C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/class5.for,v 1.2 2004/07/06 12:06:03 whitefor Exp $ Date $Date: 2004/07/06 12:06:03 $
C
      subroutine class5
c
c          Calculate for irhop.lt.isigp.lt.irho=isig
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
      i=ix
      j=jx
      n1=nalsp(l1,j)
      n2=nalsp(l2,j)
      b1=fl(n1,j)
      b2=fl(n2,j)
      b3=s(n1,j)
      b4=s(n2,j)
      tfln=max(abs(fll(i)-fll(in)),abs(b1-b2))
      tfsn=abs(b3-b4)
      tflx=min(fll(i)+fll(in),2.0*fllsig,b1+b2)
      tfsx=min(b3+b4,one)
      do 120 m=i,j
      a1=pscrl(l1,m)
      a2=pscrl(l2,m)
      a3=pscrs(l1,m)
      a4=pscrs(l2,m)
      if (m.eq.j) go to 120
      tfln=max(abs(a1-a2),tfln)
      tfsn=max(abs(a3-a4),tfsn)
      tflx=min(a1+a2,tflx)
      tfsx=min(a3+a4,tfsx)
  120 continue
      if (tfln.gt.tflx) go to 900
      if (tfsn.gt.tfsx) go to 900
c
      n3=nalsp(l1,i)
      n4=nalsp(l2,i)
      a5=pscrl(l1,j-1)
      a6=pscrl(l2,j-1)
      a7=pscrs(l1,j-1)
      a8=pscrs(l2,j-1)
      a9=pscrl(l1,i-1)
      a10=pscrl(l2,i-1)
      a11=pscrs(l1,i-1)
      a12=pscrs(l2,i-1)
      a13=pscrl(l1,i)
      a14=pscrl(l2,i)
      a15=pscrs(l1,i)
      a16=pscrs(l2,i)
      b5=fl(n3,i)
      b6=fl(n4,i)
      b7=s(n3,i)
      b8=s(n4,i)
c
      e1=sqrt((2.0*b6+1.0)*(2.0*b8+1.0)*(2.0*a10+1.0)*(2.0*a12+1.0)
     1  *(2.0*a13+1.0)*(2.0*a15+1.0)*(2.0*b1+1.0)*(2.0*b3+1.0)
     2  *(2.0*a6+1.0)*(2.0*a8+1.0))
      xx=fll(in)+fllsig+a5+a7+b1+b3+a1+a3
      if (mod(xx,two).gt.zero) e1=-e1
      tfl=tfln-1.0
      nmin=tfln+1.0
      nmax=tflx+1.0
      i1=n1-notsj1(j,j11)
      i2=n2-notsj1(j,j12)
      do 200 n=nmin,nmax
      tfl=tfl+1.0
      tfs=(1+(-1)**n)/2
      if (tfs.lt.tfsn.or.tfs.gt.tfsx) go to 200
      e2=e1
      if (nijk(j,j12,kk).eq.0) go to 130
      e2=e1*cfgp2(i1,i2,n)
      if (e2.eq.0.0) go to 200
  130 e3=e2*s6j(b2,tfl,b1, a5,a1,a6)*s6j(b4,tfs,b3, a7,a3,a8)
      mn=i+1
      mx=j-1
      if (mn.gt.mx) go to 160
      do 150 m=mn,mx
      n5=nalsp(l1,m)
      a17=pscrl(l2,m-1)
      a18=pscrs(l2,m-1)
      a19=pscrl(l1,m)
      a20=pscrl(l2,m)
      a21=pscrs(l1,m)
      a22=pscrs(l2,m)
      b9=fl(n5,m)
      b10=s(n5,m)
      xx=b9+b10+a19+a21+a17+a18
      if (mod(xx,two).gt.zero) e3=-e3
      e3=e3*sqrt((2.0*a19+1.0)*(2.0*a21+1.0)*(2.0*a17+1.0)
     1  *(2.0*a18+1.0))
  150 e3=e3*s6j(b9,pscrl(l1,m-1),a19, tfl,a20,a17)
     1    *s6j(b10,pscrs(l1,m-1),a21, tfs,a22,a18)
  160 e4=e3*sqrt((2.0*tfl+1.0)*(2.0*tfs+1.0))
      if (tfs.eq.1.0) e4=-e4
      e4=e4*s9j(a9,fll(in),a10, b5,fll(i),b6, a13,tfl,a14)
     1     *s9j(a11,half,a12, b7,half,b8, a15,tfs,a16)
      fk=kdmin-2
      do 170 nn=1,nkd
      fk=fk+2.0
  170 ccc(nn)=ccc(nn)+e4*s6j(fllsig,fllsig,tfl, fll(i),fll(in),fk)
  200 continue
c
  900 return
      end
