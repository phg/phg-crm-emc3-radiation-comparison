C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/fctrl.for,v 1.2 2004/07/06 13:52:09 whitefor Exp $ Date $Date: 2004/07/06 13:52:09 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Replaced a by ain. Expressions cannot be modified as
C               was attempted here.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      function fctrl(ain)
c
c          calculate factorial of a
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      a = ain
      fctrl=1.0
      if (abs(a)-0.1) 10, 10, 11
   10 a=0.0
   11 if (a) 12, 15, 13
   12 fctrl=0.0
      go to 15
   13 imax=a+0.1
      do 14 i=1,imax
   14 fctrl=fctrl*i
   15 return
      end
