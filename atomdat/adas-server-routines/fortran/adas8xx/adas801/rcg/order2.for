C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/order2.for,v 1.2 2004/07/06 14:26:00 whitefor Exp $ Date $Date: 2004/07/06 14:26:00 $
C
      subroutine order2(n,y)
c
c         A table of n numbers starting at y is ordered according to a
c         sequence table in the first n words of itmp, which must be integer
c         and 2n in length.
c
c          The variable y is real.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      dimension y(klam),yt(klam)
      integer t1,t2
c
      do 1 i = 1,n
      t1 = itmp(i)
c     t2 = n + i
      t2=i
    1 yt(t2) = y(t1)
      do 2 i = 1,n
c     t2 = n + i
      t2=i
    2 y(i) = yt(t2)
      return
      end
