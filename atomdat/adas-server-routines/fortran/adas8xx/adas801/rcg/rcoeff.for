C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/rcoeff.for,v 1.2 2004/07/06 14:44:27 whitefor Exp $ Date $Date: 2004/07/06 14:44:27 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : New version.
C               This subroutine has been modified to integrate cross-sections
C               to rate coefficients analytically because of incorrect behaviour
C               of neutral cross-sections at low temperatures. The numerical
C               results of Simpson's rules were not reliable. It now returns
C               the effective collision strength (gamma) rather than the
C               collision rate. 
C               Increase ktran to 30000.
C               Increase kcase to 500.
C               Extend /pwborn/ common block.
C
C               H.Summers JET 29-5-92
C
C
C  24-04-2003 : Initialise z() to prevent NANs.
C
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
      subroutine rcoeff(x,y,z,deltae,swt0,uenrgy)

      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),xx(kmx)
      common/pwborn/nbigks,nenrgs,ekminl,ekmaxl,delekl,bigks(kbgks),
     1  xmin,xmax,tev(25),corr(kenrgs),ibk,ipwr,
     2  i11,ijkion,ijkiz,newstyle,temps(25),xsec(kenrgs)

      dimension x(kenrgs),y(kenrgs),z(kenrgs)

      DIMENSION qqtev(25),yy(kenrgs)

      DATA qqTEV/1.0d+3,1.5D+3,2.0d+3,3.0d+3,5.0d+3,7.0d+3,
     +           1.0d+4,1.5D+4,2.0d+4,3.0d+4,5.0d+4,7.0d+4,
     +           1.0d+5,1.5D+5,2.0d+5,3.0d+5,5.0d+5,7.0d+5,
     +           1.0d+6,1.5D+6,2.0d+6,3.0d+6,5.0d+6,7.0d+6,1.0d+7/


      i1=0

      fion=float(ijkion+1)

      do i=1,25
        tev(i)=temps(i)*fion*fion/11604.428
      end do
      

      do i=1,kenrgs
        yy(i)=y(i)
        if (yy(i).lt.0.0D0) yy(i)=0.0D0
        z(i)=0.0D0
      end do

      ind=0
   10 ind=ind+1
      tryd=tev(ind)/13.605
      alpha=deltae/tryd
      c1=alpha
c     c2=2.1717d-8/(swt0*dsqrt(tryd))
c     c3=c1*c2
      if(ind.gt.20) then
        goto 900
      else
        if (ijkion.eq.0) then
           sumyl=(1.0-dexp(-alpha*(x(1)-1.0))*
     &           (alpha*(x(1)-1.0)+1.0))*yy(1)
     &           /(alpha*alpha*(x(1)-1.0))
        else
          sumyl=yy(1)*(1.0-dexp(-alpha*(x(1)-1.0)))/alpha
        endif
        sum0=c1*sumyl
        do i=1,nenrgs-1
           anyl=(dexp(-alpha*(x(i)-1.0))*
     &          (alpha*(x(i+1)-x(i))*yy(i)+yy(i+1)-yy(i))
     &          -dexp(-alpha*(x(i+1)-1.0))*
     &          (alpha*(x(i+1)-x(i))*yy(i+1)+yy(i+1)-yy(i)))
     &          /(alpha*alpha*(x(i+1)-x(i)))
           sumyl=sumyl+anyl
        end do
        sum=sumyl*c1

c          add integral from xmax to infinity

        aln1=log(c1*x(nenrgs-2))
        x0=c1*(x(nenrgs)-1.0)
        aln2=log(x0)
        a=(yy(nenrgs)-yy(nenrgs-2))/(aln2-aln1)
        if (a.lt.0.0) a=0.0
        b=yy(nenrgs)-a*aln2
        eras=yy(nenrgs)*exp(-x0)+a*e1(x0)
        z(ind)=sum+eras
        sum2=eras
        corr(i)=100.0*(sum0+sum2)/(z(ind)+1.0e-30)

        go to 10
      endif

 900  continue

      return
      end
