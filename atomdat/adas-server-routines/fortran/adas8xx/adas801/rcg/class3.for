C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/class3.for,v 1.2 2004/07/06 12:05:50 whitefor Exp $ Date $Date: 2004/07/06 12:05:50 $
C
      subroutine class3
c
c          Calculate for irho=isig.lt.irhop.lt.isigp
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
      i=in
      j=jn
      n1=nalsp(l1,i)
      n2=nalsp(l2,i)
      tfln=max(abs(fll(j)-fllsigp),abs(fl(n1,i)-fl(n2,i)))
      tfsn=abs(s(n1,i)-s(n2,i))
      tflx=min(fll(j)+fllsigp,2.0*fll(i),fl(n1,i)+fl(n2,i))
      tfsx=min(s(n1,i)+s(n2,i),one)
      jm1=j-1
      do 120 m=i,jm1
      tfln=max(abs(pscrl(l1,m)-pscrl(l2,m)),tfln)
      tfsn=max(abs(pscrs(l1,m)-pscrs(l2,m)),tfsn)
      tflx=min(pscrl(l1,m)+pscrl(l2,m),tflx)
  120 tfsx=min(pscrs(l1,m)+pscrs(l2,m),tfsx)
      if (tfln.gt.tflx) go to 900
      if (tfsn.gt.tfsx) go to 900
c
      n3=nalsp(l1,j)
      n4=nalsp(l2,j)
      a1=pscrl(l1,j-1)
      a2=pscrl(l2,j-1)
      a3=pscrs(l1,j-1)
      a4=pscrs(l2,j-1)
      a5=pscrl(l1,j)
      a6=pscrl(l2,j)
      a7=pscrs(l1,j)
      a8=pscrs(l2,j)
      b1=fl(n3,j)
      b2=fl(n4,j)
      b3=s(n3,j)
      b4=s(n4,j)
c
      e1=sqrt((2.0*b2+1.0)*(2.0*b4+1.0)*(2.0*a1+1.0)*(2.0*a3+1.0)
     1  *(2.0*a6+1.0)*(2.0*a8+1.0))
      xx=fll(i)+fll(j)+fllsigp+0.5+a1+a3-a2-a4+a5+a7-a6-a8
      if (mod(xx,two).ne.zero) e1=-e1
      tfl=tfln-1.0
      nmin=tfln+1.0
      nmax=tflx+1.0
      i1=n1-notsj1(i,j11)
      i2=n2-notsj1(i,j12)
      do 200 n=nmin,nmax
      tfl=tfl+1.0
      tfs=(1+(-1)**n)/2
      if (tfs.lt.tfsn.or.tfs.gt.tfsx) go to 200
      e2=e1
      if (nijk(i,j12,kk).eq.0) go to 130
      e2=e1*cfgp1(i1,i2,n)
      if (e2.eq.0.0) go to 200
  130 if (i.eq.1) go to 140
      a9=pscrl(l1,i-1)
      a10=pscrs(l1,i-1)
      a11=pscrl(l1,i)
      a12=pscrl(l2,i)
      a13=pscrs(l1,i)
      a14=pscrs(l2,i)
      b5=fl(n1,i)
      b6=fl(n2,i)
      b7=s(n1,i)
      b8=s(n2,i)
      xx=a9+a10+b6+b8+a11+a13
      if (mod(xx,two).gt.zero) e2=-e2
      e2=e2*sqrt((2.0*a12+1.0)*(2.0*a14+1.0)*(2.0*b5+1.0)*(2.0*b7+1.0))
      e2=e2*s6j(a9,b6,a12, tfl,a11,b5)*s6j(a10,b8,a14, tfs,a13,b7)
  140 mn=i+1
      mx=j-1
      if (mn.gt.mx) go to 160
      do 150 m=mn,mx
      n5=nalsp(l1,m)
      a9=pscrl(l1,m-1)
      a10=pscrs(l1,m-1)
      a11=pscrl(l2,m)
      a12=pscrs(l2,m)
      b5=fl(n5,m)
      b6=s(n5,m)
      xx=b5+b6+a9+a10+a11+a12
      if (mod(xx,two).gt.zero) e2=-e2
      e2=e2*sqrt((2.0*a9+1.0)*(2.0*a10+1.0)*(2.0*a11+1.0)*(2.0*a12+1.0))
  150 e2=e2*s6j(tfl,pscrl(l2,m-1),a9, b5,pscrl(l1,m),a11)
     1     *s6j(tfs,pscrs(l2,m-1),a10, b6,pscrs(l1,m),a12)
  160 e3=e2*sqrt((2.0*tfl+1.0)*(2.0*tfs+1.0))
      if (tfs.eq.1.0) e3=-e3
      e3=e3*s9j(tfl,fll(j),fllsigp, a2,b2,a6, a1,b1,a5)
     1     *s9j(tfs,half,half, a4,b4,a8, a3,b3,a7)
      fk=kdmin-2
      do 170 nn=1,nkd
      fk=fk+2.0
  170 ccc(nn)=ccc(nn)+e3*s6j(fll(i),fll(i),tfl, fllsigp,fll(j),fk)
  200 continue
c
  900 return
      end
