C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/cuvfd.for,v 1.2 2004/07/06 12:24:10 whitefor Exp $ Date $Date: 2004/07/06 12:24:10 $
C
c     overlay(g9,1,0)
      subroutine cuvfd
c
c          Read cfp from cards or tape,
c               and set up tables for U, V, Fk(i,i), d(i).
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3c/cfp(klsp1,klsp1),
     1  multbr(kls1,2),lbrbcd(kls1,2),ialfbr(kls1,2),flbr(kls1,2),
     2  sbr(kls1,2),itrm(kls1),ipnt(kls1),abg(kls1,3),cavei(3),
     3  nalsji(kjp),pji(kjp),njki(25)
      common/c3lc1/cfpm1(klsp1,klsp1),cfp2(klsp1,klsp1),
     1  cdum(kmx**2-2*(klsp1**2))
      common/cuv/pc(klam)
      common/sortdum/dum(2),idum(2),itmp(2*klam)
c
      dimension cfgp(klsp1,klsp1),ua(klsp1,klsp1),va(klsp1,klsp1),
     1  uv(klsp1,klsp1),cfgpt(klsp1,klsp1)
      dimension cavedn(7),tdn(21,7),t1dn(21,3),t2dn(21,3)
      equivalence (cfp2,cfgp,ua,va),(cfpm1,cfgpt,uv)
      character*10 zetanm,abgnam(3),abgnm1,abgnm2,aa
c
      data zetanm/'Zeta'/
      data (abgnam(i),i=1,3)/'alpha','beta','gamma'/
      data abgnm1,abgnm2/'alpha','1000 alpha'/
      data sortys,sortno/1h ,7hno sort/
      data cavedn/-108.88888887,-122.5,-116.66666667,-97.22222222,
     1 -70.0,-40.83333333,-15.55555555/
      data (tdn(i,1),i=1,5)/60.0,210.0,100.0,170.0,0.0/
      data (tdn(i,2),i=1,9)/60.0,210.0,30.0,130.0,210.0,52.5,
     1  34.36931771,322.5,135.0/
      data (tdn(i,3),i=1,21)/70.0,10.0,135.0,40.0,30.0,175.0,
     1  310.0,140.0,37.41657387,150.0,10.0,66.66666667,
     2  -55.27707984,248.33333333,235.0,160.0,127.2792206,
     3  190.0,0.0,0.0,640.0/
      data (tdn(i,4),i=1,21)/0.0,50.0,30.0,155.0,105.0,5.0,15.0,
     1  90.0,0.0,170.0,150.0,134.1640786,230.0,35.0,39.68626967,
     2  195.0,0.0,148.492424,275.0,225.0,320.0/
      data (tdn(i,5),i=1,21)/0.0,20.0,45.0,20.0,-30.0,125.0,
     1  80.0,70.0,-37.41657387,300.0,0.0,33.33333333,
     2  55.27707984,91.66666667,225.0,150.0,-127.2792206,
     3  360.0,0.0,0.0,0.0/
      data (tdn(i,6),i=1,9)/0.0,0.0,0.0,50.0,90.0,17.5,
     1  -34.36931771,67.5,315.0/
      data (tdn(i,7),i=1,5)/0.0,0.0,0.0,140.0,0.0/
      data (t1dn(i,1),i=1,9)/-9.0,21.0,-15.0,35.0,-21.0,0.0,
     1  -13.74772708,84.0,-141.0/
      data (t1dn(i,2),i=1,21)/0.0,-24.0,26.0,-12.0,12.0,-6.0,96.0,28.0,
     1  14.96662955,-136.0,0.0,20.0,-92.86549413,66.0,-30.0,-36.0,
     2  118.7939392,-144.0,0.0,-54.99090834,336.0/
      data (t1dn(i,3),i=1,21)/0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     1  0.0,160.9968944,0.0,0.0,47.6235236,0.0,0.0,178.1909089,0.0,0.0,
     2  0.0/
      data (t2dn(i,1),i=1,9)/-9.0,21.0,6.136363636,-3.75,2.25,0.0,
     1  -13.74772708,-9.0,-1.5/
      data (t2dn(i,2),i=1,21)/0.0,-2.863636364,-12.75,-12.0,12.0,17.25,
     1  3.0,28.0,14.96662955,3.5,12.681818182,20.0,0.603022689,
     2  -6.568181818,-6.75,-36.0,-12.72792206,-4.5,0.0,-54.99090834,
     3  -36.0/
      data (t2dn(i,3),i=1,21)/0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     1  27.15015068,0.0,0.0,5.031152949,0.0,0.0,47.6235236,0.0,0.0,
     2  -19.09188309,0.0,0.0,0.0/
c
c
c          Set unit numbers onto which cfp, U, V, and coeffs are
c               to be written
c
      id3=id2+1
      id4=id3+1
      write (iw,8) id2,id3,id4,ilncuv
    8 format (//' cuvfd---write units',3i5,10x,'ilncuv=',i2)
      rewind id2
      rewind id3
      rewind id4
      llm1=0
      llm2=0
      fllm1=0.0
      nim1=-7
      nim2=-7
      ntm1=1
      ntm2=0
      nltm1=0
      nltm2=0
      icpc=0
      if (ilncuv.gt.2) icpc=ilncuv
      abgnam(1)=abgnm1
      if (ictbcd.ne.0) abgnam(1)=abgnm2
      go to 120
c
c          Set back cfp tables
c
  110 llm2=llm1
      nim2=nim1
      ntm2=ntm1
      nltm2=nltm1
      do 112 m=1,ntm1
      mult(m,3)=mult(m,2)
      lbcd(m,3)=lbcd(m,2)
      s(m,3)=s(m,2)
      fl(m,3)=fl(m,2)
  112 ialf(m,3)=ialf(m,2)
      llm1=ll(1)
      fllm1=fll(1)
      nim1=ni(1)
      ntm1=not
      nltm1=nlt
      do 115 m=1,not
      mult(m,2)=mult(m,1)
      lbcd(m,2)=lbcd(m,1)
      s(m,2)=s(m,1)
      fl(m,2)=fl(m,1)
      ialf(m,2)=ialf(m,1)
      do 115 n=1,nop
  115 cfpm1(m,n)=cfp(m,n)
c
c          Read sub-shell data, and terms to be included
c
  120 read (ir,9) ll(1),ni(1),not,nop,icuv,klast,nlt,
     1  (mult(m,1),lbcd(m,1),ialf(m,1), m=1,12), nosort
    9 format (3x,a1,3i4,2x,a1,i1,i4,4x,12(i1,a1,a2), i4)
      if (klast.eq.0) klast=0
      if (not.le.0) go to 900
      eras=sortys
      if (nosort.ne.0) eras=sortno
      if (ilncuv.eq.0) go to 122
      write (iw,10)
   10 format (1h1)
  122 if (nlt.le.12) go to 124
      read (ir,11) (mult(m,1),lbcd(m,1),ialf(m,1), m=13,nlt)
   11 format (20(i1,a1,a2))
  124 if (nlt.gt.0) go to 126
      write (iw,12) ll(1),ni(1),not,nop,icuv,klast,nlt,eras
   12 format (/1h ,a1,i2,2i5,a1,i1,i5,5x,9hall terms,10x,a7)
      if (ilncuv.gt.0) write (iw,9)
      go to 127
  126 write (iw,13) ll(1),ni(1),not,nop,icuv,klast,nlt,
     1  (mult(m,1),lbcd(m,1),ialf(m,1), m=1,nlt)
   13 format (/1h ,a1,i2,2i5,a1,i1,i5,5x,20(i1,a1,a2,1x)/
     1  29x,20(i1,a1,a2,1x))
  127 do 128 j=1,24
      if (lsymb(j).eq.ll(1)) go to 129
  128 continue
  129 fll(1)=j-1
c
c          Read cfp table from cards
c
  140 read (ir,16) (multbr(n,2),lbrbcd(n,2),ialfbr(n,2), n=1,nop)
   16 format (20(i1,a1,a2))
      do 141 n=1,nop
      do 141 m=1,not
  141 cfp2(m,n)=0.0
      m=0
  142 m=m+1
      it=0
  143 ito=it
      read (ir,17) mult(m,4),lbcd(m,4),ialf(m,4),it,
     1  (ipnt(j),pc(j), j=1,4)
   17 format (i1,a1,a2,i4, 4(i3,f13.10))
      if (ito.gt.0) go to 144
      m1=mult(m,4)
      l1=lbcd(m,4)
      ia1=ialf(m,4)
  144 do 145 j=1,4
      n=ipnt(j)
      if (n.eq.0) go to 145
      cfp2(m,n)=pc(j)
  145 continue
c          check card ordering
      if (m1.ne.mult(m,4)) go to 146
      if (l1.ne.lbcd(m,4)) go to 146
      if (ia1.ne.ialf(m,4)) go to 146
      if (iabs(it).eq.(ito+1)) go to 147
  146 write (iw,18) ll(1),ni(1),m1,l1,ia1,ito,mult(m,4),lbcd(m,4),
     1  ialf(m,4),it
   18 format (//35h0cuvfd 18--cfd cards out of order  ,a1,i2,i4,a1,a2,i6
     1  /34x,i4,a1,a2,i6///)
  147 if (it.gt.0) go to 143
      if (m.lt.not) go to 142
c
c          Reorder parents to correspond to terms of preceeding l(n),
c               and reorder terms with specified nlt terms first
c
  150 if (ll(1).eq.llm1.and.ni(1).eq.nim1+1) go to 155
      do 151 i=1,nop
  151 ipnt(i)=i
      go to 170
  155 do 160 n=1,nop
      do 159 m=1,ntm1
      if (multbr(n,2).ne.mult(m,2)) go to 159
      if (lbrbcd(n,2).ne.lbcd(m,2)) go to 159
      if (ialfbr(n,2).eq.ialf(m,2)) go to 160
  159 continue
      write (iw,19) multbr(n,2),lbrbcd(n,2),ialfbr(n,2),ll(1),ni(1),
     1  llm1,nim1
   19 format (//18h0cuvfd 19--parent ,i1,a1,a2,4h of ,a1,i2,
     1  27h  not found among terms of ,a1,i2)
  160 ipnt(m)=n
c
  170 if (nlt.gt.0) go to 175
      do 171 i=1,not
  171 itrm(i)=i
      nlt=not
      go to 185
  175 m=nlt
      do 180 i=1,not
      do 178 j=1,nlt
      if (mult(i,4).ne.mult(j,1)) go to 178
      if (lbcd(i,4).ne.lbcd(j,1)) go to 178
      if (ialf(i,4).eq.ialf(j,1)) go to 179
  178 continue
      m=m+1
      itrm(m)=i
      go to 180
  179 itrm(j)=i
  180 continue
      if (m.eq.not) go to 185
      write (iw,20) m,not
   20 format (//17h0cuvfd 20--m,not=,2i5)
c          Calc values of S and L
  185 do 190 m=1,not
      do 188 j=1,24
      if (lsymb(j).eq.lbcd(m,4)) go to 189
  188 continue
  189 fl(m,4)=j-1
      a=mult(m,4)-1
  190 s(m,4)=0.5*a
      do 195 n=1,nop
      do 192 j=1,24
      if (lsymb(j).eq.lbrbcd(n,2)) go to 193
  192 continue
  193 flbr(n,2)=j-1
      a=multbr(n,2)-1
  195 sbr(n,2)=0.5*a
      if (fll(1)-fllm1) 198,196,200
  196 if (ni(1).gt.nim1) go to 200
  198 write (iw,21)
   21 format (//37h0cuvfd 21--cfp decks out of order****//)
c
  200 do 202 m=1,not
      i=itrm(m)
c     decode (1,22,ialf(i,4)) n
      write (11,1021) ialf(i,4)
 1021 format (a2)
      backspace 11
      read (11,22) n
      backspace 11
   22 format (i1)
      a=n
      if (a.eq.0.0) a=9.5
  202 pc(m)=-100.0*(s(i,4)+1.0)-fl(i,4)+0.05*a
c
c          If all terms included and nosort=0, sort by s and l
c
      if (nlt.lt.not.or.nosort.ne.0) go to 203
      call sort2(nlt,1,pc(1),itrm,dum,idum,idum,dum,dum,
     1  dum,dum,dum,dum,dum,dum)
c
  203 do 205 n=1,nop
      j=ipnt(n)
      multbr(n,1)=multbr(j,2)
      lbrbcd(n,1)=lbrbcd(j,2)
      ialfbr(n,1)=ialfbr(j,2)
      sbr(n,1)=sbr(j,2)
      flbr(n,1)=flbr(j,2)
      do 205 m=1,not
      i=itrm(m)
  205 cfp(m,n)=cfp2(i,j)
      do 210 m=1,not
      i=itrm(m)
      mult(m,1)=mult(i,4)
      lbcd(m,1)=lbcd(i,4)
      ialf(m,1)=ialf(i,4)
      s(m,1)=s(i,4)
  210 fl(m,1)=fl(i,4)
      if (ilncuv.ne.1.and.ilncuv.ne.4) go to 220
      write (iw,23) (multbr(n,1),lbrbcd(n,1),ialfbr(n,1), n=1,nop)
   23 format (//10h0cfp table//5x,10(i9,a1,a2)/(5x,10(i9,a1,a2)))
      do 218 m=1,not
  218 write (iw,24) mult(m,1),lbcd(m,1),ialf(m,1), (cfp(m,n), n=1,nop)
   24 format (1h ,i1,a1,a4,10f12.8/(2x,10f12.8))
c          Check orthonormality of cfp rows
  220 do 230 m=1,not
      do 229 m1=m,not
      sum=-1.0
      if (m.eq.m1) go to 222
      sum=0.0
      if (mult(m,1).ne.mult(m1,1)) go to 229
      if (lbcd(m,1).ne.lbcd(m1,1)) go to 229
  222 do 224 n=1,nop
  224 sum=sum+cfp(m,n)*cfp(m1,n)
      if (abs(sum).lt.1.0e-6) go to 229
      write (iw,25) mult(m,1),lbcd(m,1),ialf(m,1),
     1             mult(m1,1),lbcd(m1,1),ialf(m1,1)
   25 format (//36h0cuvfd 25--cfp not orthonormal for  ,2(i4,a1,a2)//)
  229 continue
  230 continue
c          Check orthonormality of cfp columns
      if (ni(1).eq.0) go to 250
      a=ni(1)
      b=(4.0*fll(1)+3.0-a)/a
      do 239 n=1,nop
      sum=-b*(2.0*flbr(n,1)+1.0)*(2.0*sbr(n,1)+1.0)
      do 238 n1=n,nop
      if (n.eq.n1) go to 232
      sum=0.0
      if (multbr(n,1).ne.multbr(n1,1)) go to 238
      if (lbrbcd(n,1).ne.lbrbcd(n1,1)) go to 238
  232 do 234 m=1,not
      a=(2.0*fl(m,1)+1.0)*(2.0*s(m,1)+1.0)
  234 sum=sum+a*cfp(m,n)*cfp(m,n1)
      if (abs(sum).lt.1.0e-6) go to 238
      write (iw,25) multbr(n,1),lbrbcd(n,1),ialfbr(n,1),
     1             multbr(n1,1),lbrbcd(n1,1),ialfbr(n1,1)
  238 continue
  239 continue
c
c          Read or calc cfgp (if needed) and write disk id2
c
  250 nlp=1
      nocfp=0
      if (ll(1).ne.llm1.or.ni(1).ne.(nim1+1)) go to 252
      nlp=nltm1
      nocfp=1
  252 nlgp=0
      nocfgp=0
      if (ll(1).ne.llm2.or.ni(1).ne.(nim2+2)) go to 254
      nlgp=nltm2
      nocfgp=2.0*fll(1)+1.0
  254 norcd=1+nocfp+nocfgp
      if (nocfp.gt.0) norcd=norcd+1
      if (nocfgp.gt.0) norcd=norcd+1
      write (id2) ni(1),fll(1),not,nlt,norcd
      write (id2) (mult(m,1),lbcd(m,1),ialf(m,1),fl(m,1),s(m,1),
     1  m=1,nlt), nlp,nlgp,nocfp,nocfgp
      if (nocfp.eq.0) go to 257
      write (id2) ((cfp(m,n),m=1,nlt), n=1,nlp)
      write (id2) (mult(m,2),lbcd(m,2),ialf(m,2),fl(m,2),s(m,2),m=1,nlp)
  257 if (nocfgp.eq.0) go to 300
c
  260 write (id2) (mult(m,3),lbcd(m,3),ialf(m,3),fl(m,3),s(m,3),
     1  m=1,ntm2)
      flp=-1.0
      do 285 k=1,nocfgp
      flp=flp+1.0
      sp=0.5*(1.0+(-1.0)**k)
      mulp=2.0*sp+1.0
      iflp=flp
      lll=lsymb(iflp+1)
      a=sqrt((2.0*flp+1.0)*(2.0*sp+1.0))
      do 273 m=1,nlt
      x0=fl(m,1)+s(m,1)+1.0
      do 273 n=1,nlgp
      sum=0.0
      do 272 n1=1,nop
      b=s6j(fll(1),fll(1),flp, fl(m,1),fl(n,3),fl(n1,2))
      if (b.eq.0.0) go to 272
      b=b*s6j(half,half,sp, s(m,1),s(n,3),s(n1,2))
      if (b.eq.0.0) go to 272
      b=b*sqrt((2.0*fl(n1,2)+1.0)*(2.0*s(n1,2)+1.0))
      b=b*cfp(m,n1)*cfpm1(n1,n)
      sum=sum+b
  272 continue
      sum=sum*a
      x=x0+fl(n,3)+s(n,3)
      if (mod(x,two).gt.zero) sum=-sum
  273 cfgp(m,n)=sum
      if (ilncuv.ne.1.and.ilncuv.ne.4) go to 276
      write (iw,29) ll(1),mulp,lll, (mult(n,3),lbcd(n,3),ialf(n,3),
     1   n=1,nlgp)
   29 format (///11h0cfgp for  ,a1,2h2(,i1,a1,1h)//5x,10(i9,a1,a2)/
     1  (5x,10(i9,a1,a2)))
      do 275 m=1,nlt
  275 write (iw,24) mult(m,1),lbcd(m,1),ialf(m,1), (cfgp(m,n), n=1,nlgp)
  276 write (id2) ((cfgp(m,n),m=1,nlt), n=1,nlgp)
c          check orthonormality of cfgp rows
      if (nlgp.ne.nltm2) go to 295
      l=200
      do 280 m=1,nlt
      do 279 m1=m,nlt
      if (mult(m,1).ne.mult(m1,1)) go to 279
      if (lbcd(m,1).ne.lbcd(m1,1)) go to 279
      l=l+1
      if (k.eq.1) pc(l)=0.0
  277 do 278 n=1,nlgp
  278 pc(l)=pc(l)+cfgp(m,n)*cfgp(m1,n)
  279 continue
  280 continue
c
  285 continue
c
      l=200
      do 290 m=1,nlt
      do 289 m1=m,nlt
      if (mult(m,1).ne.mult(m1,1)) go to 289
      if (lbcd(m,1).ne.lbcd(m1,1)) go to 289
      l=l+1
      if (m.eq.m1) pc(l)=pc(l)-1.0
      if (abs(pc(l)).lt.1.0e-6) go to 289
      write (iw,30) mult(m,1),lbcd(m,1),ialf(m,1),
     1             mult(m1,1),lbcd(m1,1),ialf(m1,1)
   30 format (/37h cuvfd 30--cfgp not orthonormal for  ,2(i4,a1,a2)/)
  289 continue
  290 continue
  295 continue
c
c          Calculate number of terms of each kind
c
  300 k=0
      n2=-100
      do 310 m=1,nlt
      n1=n2
      n2=-100.0*s(m,1)-fl(m,1)
      if (n2.ne.n1) go to 309
      ntrmk(k)=ntrmk(k)+1
      go to 310
  309 k=k+1
      ntrmk(k)=1
  310 continue
      ndifft=k
c
c          Calculate number of levels of each j
c
      m=0
      do 315 j=1,nlt
      eji=abs(fl(j,1)-s(j,1))-1.0
      nji=fl(j,1)+s(j,1)-eji
      do 315 ij=1,nji
      m=m+1
      eji=eji+1.0
      nalsji(m)=j
      a=j
      pc(m)=-200.0*eji-25.0*s(j,1)-fl(j,1)+0.001*a
  315 pji(m)=eji
      ntotji=m
      call sort2(m,2,pc(1),nalsji,pji,idum,idum,dum,dum,dum,
     1  dum,dum,dum,dum,dum)
c
      k=1
      njki(k)=1
      if (m.le.1) go to 320
      do 319 j=2,m
      if (pji(j).ne.pji(j-1)) go to 317
      njki(k)=njki(k)+1
      go to 319
  317 k=k+1
      njki(k)=1
  319 continue
  320 ndifji=k
c
c        Write U,V on disk id3, and coeffs for l(n) on disk id4
c
  400 fn=ni(1)
      nmxm1=4.0*fll(1)+1.0
      npar=0
      if (ni(1).eq.0.or.ni(1).eq.(nmxm1+1).or.fll(1).eq.0.0) go to 410
      npar=1
      if (ni(1).eq.1.or.ni(1).eq.nmxm1) go to 410
      npar=1.0+fll(1)
      if (iabg.eq.0) go to 410
      three=3.0
      npar=1.0+fll(1)+min(three,fll(1))
      if (fll(1).ne.two) go to 410
      npar=npar+1
      if (ni(1).gt.2.and.ni(1).lt.8) npar=npar+2
  410 norcd=1+npar
      write (id4) ni(1),fll(1),not,nlt,norcd
      write (id4) ndifft, (ntrmk(k), k=1,ndifft), ntotji,
     1  (nalsji(m),pji(m), m=1,ntotji), ndifji, (njki(m), m=1,ndifji)
      if (npar.eq.0) go to 800
      if (ni(1).eq.1) go to 500
      norcd=4.0*fll(1)+2.0
      write (id3) ni(1),fll(1),not,nlt,norcd
c
c          Calc. U matrices
c
      nor=norcd/2
      do 485 irp1=1,nor
      k=irp1-1
      r=k
      do 435 m=1,not
      mpx=min0(m,nlt)
      do 435 mp=1,mpx
      a=0.0
      b=0.0
      if (s(m,1).ne.s(mp,1)) go to 434
      if (r.gt.(fl(m,1)+fl(mp,1))) go to 434
      if (r.lt.abs(fl(m,1)-fl(mp,1))) go to 434
      do 432 n=1,nop
      b=cfp(m,n)*cfp(mp,n)
      if (b.eq.0.0) go to 432
      b=b*s6j(fll(1),fll(1),r, fl(m,1),fl(mp,1),flbr(n,1))
      x=mod(flbr(n,1),two)
      if (x.gt.0.0) b=-b
      a=a+b
  432 continue
      a=a*sqrt((2.0*fl(m,1)+1.0)*(2.0*fl(mp,1)+1.0))*fn
      x=mod(fll(1)+fl(m,1)+r,two)
      if (x.gt.0.0) a=-a
      b=a
      x=mod(fl(m,1)-fl(mp,1),two)
      if (x.ne.0.0) b=-b
  434 ua(m,mp)=a
  435 ua(mp,m)=b
      write (id3) k, ((ua(m,mp),m=1,nlt), mp=1,nlt)
c
      if (r.eq.0.0.or.npar.le.1) go to 485
      x=mod(r,two)
      if (x.gt.0.0) go to 460
c          calc Fk
  440 a=2.0*fll(1)+1.0
      cij=0.5*a*s3j0sq(fll(1),r,fll(1))
      cave=cij*fn*(fn-1.0)/(a+fll(1)+fll(1))
      cavep=cave-fn*cij
      cij=cij*a
      if (ilncuv.gt.1) write (iw,41) k,cave,ll(1),ni(1)
   41 format (//2h0F,i1,8x,5hcave=,f12.7,10x,a1,i2//)
c
      nopc=0
      mmax=0
      do 450 l=1,ndifft
      mmin=mmax+1
      mmax=mmax+ntrmk(l)
      do 450 m=mmin,mmax
      cij1=cij/(2.0*fl(m,1)+1.0)
      do 448 mp=mmin,m
      nopc=nopc+1
      a=0.0
      do 445 mpp=1,not
  445 a=a+ua(mpp,m)*ua(mpp,mp)
      pc(nopc)=cij1*a
      if (m.ne.mp) go to 448
      pc(nopc)=pc(nopc)+cavep
  448 continue
      if (icpc.eq.0) go to 450
      n2=nopc-m+mmin
      write (iw,42) nopc,mult(m,1),lbcd(m,1), (pc(n1), n1=n2,nopc)
   42 format (2i5,a1,10f11.6/(13x,10f11.6))
  450 continue
      kpar=-1
      write (aa,43) k,ll(1),ni(1)
   43 format (1hF,i1,1h(,a1,i2,4h)   )
      write (id4) kpar,k,aa,cave,nopc, (pc(n1), n1=1,nopc)
      call clock(time)
      delt=time-time0
      if (ilncuv.gt.1) write (iw,45) aa,nopc,cave,delt
   45 format (1h ,a7,7h    pc(,i4,1h),7x,5hcave=,f12.7,8x,5htime=,f6.3,
     1  4h min)
      go to 485
c          Calc. beta or gamma
  460 if (k.gt.5) go to 485
      i=(k+1)/2
      do 480 m=1,nlt
      a=0.0
      do 465 mp=1,not
  465 a=a+ua(mp,m)**2
      a=a/(2.0*fl(m,1)+1.0)
      go to (471,473,475) i
  471 abg(m,2)=3.0*a
      abg(m,3)=0.0
      go to 480
  473 abg(m,3)=abg(m,2)+7.0*a
      go to 480
  475 abg(m,2)=abg(m,2)+11.0*a
      abg(m,3)=abg(m,3)+11.0*a
  480 continue
c
  485 continue
c          Finish calculation of alpha, beta, gamma, T, T1, and T2
      if (npar.le.1.or.iabg.eq.0) go to 500
      a=4.0*fll(1)+1.0
      b=fn*(fn-a-1.0)
      cavei(2)=b/26.0
      b=b*fll(1)/a
      afact=1.0
      if (ictbcd.ne.0) afact=0.001
      cavei(1)=b*(fll(1)+1.0)*afact
      a=1.0/(2.0*fll(1)-1.0)
      cavei(3)=a*b
      do 487 m=1,nlt
      abg(m,1)=fl(m,1)*(fl(m,1)+1.0)*afact+cavei(1)
      abg(m,2)=0.25*abg(m,2)+cavei(2)
  487 abg(m,3)=a*abg(m,3)+cavei(3)
      do 488 i=1,nopc
  488 pc(i)=0.0
      k=0
      imax=min(fll(1),three)
      do 492 i=1,imax
      j=i
      aa=abgnam(j)
      if (ll(1).eq.lsymb(3).and.i.eq.2) j=3
      if (ilncuv.gt.1) write (iw,48) aa,cavei(j),ll(1),ni(1)
   48 format (//1h0,a10,5hcave=,f12.7,10x,a1,i2//)
      m=0
      nopc=0
      do 490 l=1,ndifft
      nk=ntrmk(l)
      do 490 n=1,nk
      m=m+1
      n2=nopc+1
      nopc=nopc+n
      pc(nopc)=abg(m,j)
      if (icpc.eq.0) go to 490
      write (iw,42) nopc, mult(m,1),lbcd(m,1), (pc(n1), n1=n2,nopc)
  490 continue
      write (id4) kpar,k,aa,cavei(j),nopc, (pc(n1), n1=1,nopc)
      call clock(time)
      delt=time-time0
      if (ilncuv.gt.1) write (iw,45) aa,nopc,cavei(j),delt
  492 continue
      if (fll(1).ne.2.0) go to 500
      if (ni(1).le.1.or.ni(1).ge.9) go to 500
      j=ni(1)-1
      k=0
      m=0
      nopc=0
      do 495 l=1,ndifft
      nk=ntrmk(l)
      do 495 n=1,nk
      n2=nopc+1
      do 494 n1=1,n
      m=m+1
      nopc=nopc+1
      pc(nopc)=tdn(m,j)
      if (n1.eq.n) pc(nopc)=pc(nopc)+cavedn(j)
  494 continue
      if (icpc.eq.0) go to 495
      write (iw,42) nopc,mult(m,1),lbcd(m,1), (pc(n1), n1=n2,nopc)
  495 continue
      write (aa,49) ll(1),ni(1)
   49 format(2hT(,a1,i2,5h)    )
      write (id4) kpar,k,aa,cavedn(j),nopc, (pc(n1), n1=1,nopc)
      if (ni(1).le.2.or.ni(1).ge.8) go to 500
      j=min0(ni(1)-2,8-ni(1))
      eras=0.0
      do 499 i=1,2
      m=0
      nopc=0
      do 498 l=1,ndifft
      nk=ntrmk(l)
      do 498 n=1,nk
      n2=nopc+1
      do 497 n1=1,n
      m=m+1
      nopc=nopc+1
      pc(nopc)=t1dn(m,j)
      if (i.eq.2) pc(nopc)=t2dn(m,j)
      if (ni(1).gt.5) pc(nopc)=-pc(nopc)
  497 continue
      if (icpc.eq.0) go to 498
      write (iw,42) nopc,mult(m,1),lbcd(m,1), (pc(n1), n1=n2,nopc)
  498 continue
      write (aa,50) i,ll(1),ni(1)
   50 format(1hT,i1,1h(,a1,i2,4h)   )
  499 write (id4) kpar,k,aa,eras,nopc, (pc(n1), n1=1,nopc)
c
c          Calc. V matrices
c
  500 nov=nor
      if (ni(1).eq.1) nov=1
      do 585 irp1=1,nov
      k=irp1-1
      if (nov.eq.1) k=1
      r=k
      do 535 m=1,nlt
      do 535 mp=1,nlt
      a=0.0
      b=0.0
      if (s(m,1)+s(mp,1).lt.1.0) go to 534
      if (abs(s(m,1)-s(mp,1)).gt.1.0) go to 534
      if (fl(m,1)+fl(mp,1).lt.r) go to 534
      if (abs(fl(m,1)-fl(mp,1)).gt.r) go to 534
      do 532 n=1,nop
      b=cfp(m,n)*cfp(mp,n)
      if (b.eq.0.0) go to 532
      b=b*s6j(half,half,one, s(m,1),s(mp,1),sbr(n,1))
     1   *s6j(fll(1),fll(1),r, fl(m,1),fl(mp,1),flbr(n,1))
      x=mod(s(m,1)+sbr(n,1)+1.5+flbr(n,1),two)
      if (x.gt.0.0) b=-b
      a=a+b
  532 continue
      a=a*fn*sqrt(1.5*(2.0*s(m,1)+1.0)*(2.0*s(mp,1)+1.0)
     1  *(2.0*fl(m,1)+1.0)*(2.0*fl(mp,1)+1.0))
      x=mod(fll(1)+fl(m,1)+r,two)
      if (x.gt.0.0) a=-a
      b=a
      x=mod(fl(m,1)-fl(mp,1)+s(m,1)-s(mp,1),two)
      if (x.ne.0.0) b=-b
  534 va(m,mp)=a
  535 va(mp,m)=b
      if (nov.eq.1) go to 539
      write (id3) k, ((va(m,mp),m=1,nlt), mp=1,nlt)
  539 if (k.ne.1) go to 585
c          Calc. coeffs of Zeta
  540 if (ilncuv.gt.1) write (iw,52) ll(1),ni(1)
   52 format (//5h0Zeta,33x,a1,i2//)
c
      b=sqrt(fll(1)*(fll(1)+1.0)*(2.0*fll(1)+1.0))
      nopc=0
      mmax=0
      do 560 l=1,ndifji
      mmin=mmax+1
      mmax=mmax+njki(l)
      do 560 m=mmin,mmax
      j=nalsji(m)
      do 550 mp=mmin,m
      jp=nalsji(mp)
      nopc=nopc+1
      pc(nopc)=0.0
      a=va(j,jp)
      if (a.eq.0.0) go to 550
      x=mod(fl(jp,1)+s(j,1)+pji(m),two)
      if (x.gt.0.0) a=-a
      pc(nopc)=a*b*s6j(s(j,1),fl(j,1),pji(m), fl(jp,1),s(jp,1),one)
  550 continue
      if (icpc.eq.0) go to 560
      n2=nopc-m+mmin
      write (iw,54) nopc,mult(j,1),lbcd(j,1),pji(m),
     1  (pc(n1), n1=n2,nopc)
   54 format (2i5,a1,f4.1,10f11.6/(17x,10f11.6))
  560 continue
      kpar=1
      cave=0.0
      aa=zetanm
      write (id4) kpar,k,aa,cave,nopc, (pc(n1), n1=1,nopc)
      call clock(time)
      delt=time-time0
      if (ilncuv.gt.1) write (iw,45) aa,nopc,cave,delt
c
  585 continue
c
  800 call clock(time)
      delt=time-time0
      write (iw,60) ll(1),ni(1),delt
   60 format (26h Finished cuvfd calc for  ,a1,i2,11h   at time=,f7.3,
     1  4h min)
      go to 110
c
c          Write dummy records required by system bug on ibm7030
c
  900 write (id2) (pc(j), j=1,20)
      write (id3) (pc(j), j=1,20)
      write (id4) (pc(j), j=1,20)
      rewind id2
      rewind id3
      rewind id4
      return
      end
