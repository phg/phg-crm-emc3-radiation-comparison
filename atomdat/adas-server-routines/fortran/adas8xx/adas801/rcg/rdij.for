C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/rdij.for,v 1.2 2004/07/06 14:45:02 whitefor Exp $ Date $Date: 2004/07/06 14:45:02 $
C
      subroutine rdij(i,j)
c
c          Calculate Coulomb direct coeffs. rd(i,j)
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
      n=tk+1.0
      a=zero
      do 200 ii=i,j
      if (ts(1,ii).ne.ts(2,ii)) go to 399
      if (tscs(1,ii).ne.tscs(2,ii)) go to 399
  200 continue
  300 ixx=tl(1,j)+tscl(2,j-1)+tscl(1,j)
      a=(-one)**ixx
      a=a*s6j(tscl(1,j-1),tscl(2,j-1),tk, tl(2,j),tl(1,j),tscl(1,j))
      mn=i+1
      mx=j-1
      if (mn-mx) 350,350,352
  350 do 351 m1=mn,mx
  351 a=a*uncpla(tscl(1,m1-1),tl(1,m1),tscl(1,m1),
     1    tk,tscl(2,m1-1),tscl(2,m1))
  352 if (i-1) 366,366,355
  355 a=a*uncplb(tscl(1,i-1),tl(1,i),tscl(1,i), tk,tl(2,i),tscl(2,i))
  366 if (nii.eq.1) go to 380
      if (irho.ne.0) go to 371
      go  to (371,372,373,374,375,376,377,378) i
  371 a=a*u1(ki,kpi,n)
      go to 380
  372 a=a*u2(ki,kpi,n)
      go to 380
  373 a=a*u3(ki,kpi,n)
      go to 380
  374 a=a*u4(ki,kpi,n)
      go to 380
  375 a=a*u5(ki,kpi,n)
      go to 380
  376 a=a*u6(ki,kpi,n)
      go to 380
  377 a=a*u7(ki,kpi,n)
      go to 380
  378 a=a*u8(ki,kpi,n)
  380 if (nij.eq.1) go to 399
      if (irho.ne.0) go to 381
      go to (381,382,383,384,385,386,387,388) j
  381 a=a*u1(kj,kpj,n)
      go to 399
  382 a=a*u2(kj,kpj,n)
      go to 399
  383 a=a*u3(kj,kpj,n)
      go to 399
  384 a=a*u4(kj,kpj,n)
      go to 399
  385 a=a*u5(kj,kpj,n)
      go to 399
  386 a=a*u6(kj,kpj,n)
      go to 399
  387 a=a*u7(kj,kpj,n)
      go to 399
  388 a=a*u8(kj,kpj,n)
  399 continue
c
      return
      end
