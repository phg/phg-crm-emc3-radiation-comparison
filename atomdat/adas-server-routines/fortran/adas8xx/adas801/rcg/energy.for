C UNIX-IDL - SCCS info: Module @(#)energy.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase ktran to 30000.
C               Increase kcase to 500.
C               Extend /pwborn/ common block.
C               Add a call to sprnadd.
C
C  Martin O'Mullane 
C
C
C 01-02-2002  : Added call to Cowan's new elecden routine.
C
C Allan Whiteford
C  
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
c     overlay(g9,10,0)
      subroutine energy
c
c          Read parameter values, calc. energy matrices, and diagonalize;
c               with continuum electrons present, also calculate kinetic
c               energies, and autoionization probabilities and rates
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/magee/nmult,ndum,nexp(kmx),sums(kmult),
     1  ials(kmult,ks,2),sl(kmult,ks,2),ss(kmult,ks,2),nexped(kmult,2),
     2  etrm(kmult,2)
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/dumlc4(klc42)
      common/c3lc5/ci(kmx,kmx),dum5(ktran*kbgks-kmx**2)
      common/c3lc6/gaaxc(kmx,kexc),brnchx(kexc,kexc)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c6/emin,emax,sumwt,flbdn,flbdx,fnunp,fnuxp,nlev,iewr
      dimension vect(kmx,kmx),nparj(kc,kc),dele(kmx),enk(2),exk(2)
      equivalence (dumlc4(klc4),nparj),(dele,ct4)
      dimension cpurty(kmx,kc),nlcfgj(kc),nscfgj(kc)
      dimension aa(kmx,kmx),cii(kmx,kmx),jpar(kpr)
      equivalence (ct4,aa,cpurty),(dumlc4,vect,cii,jpar)
      equivalence (dumlc4,nlcfgj),(dumlc4(500),nscfgj)
      common/c7/g1,sumgaa,sumgar,sumgaat
      common/c8/exc(kexc),jexc(kexc),mexc(kexc)
      common/c9/l1,l2,cflag(kc,2),cgengy(kc,2),swt(kc,2),imax
      common/pwborn/nbigks,nenrgs,ekminl,ekmaxl,delekl,bigks(kbgks),
     1  xmin,xmax,tev(25),corr(kenrgs),ibk,ipwr,
     2  i11,ijkion,ijkiz,newstyle,temps(25),xsec(kenrgs)
      common/energies/etot(kc,2),ee8(kc,2),e0kin(kc),ekin(kmx)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      character*8 ldesn(kmx),fidmult(kmult,2)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 elem(3,kc,2),elem1(3),blank
      common/char2/elem,elem1
      common/char1/fidmult
      dimension illp(kmx**2),coeff(kmx**2)
      equivalence (dumlc4,illp),(ct4,coeff)
      character*1 blnk,star,cflag
      data blank,blnk,star/'        ',' ','*'/


C-------Start ADAS conversion-------

      dimension qqtev(25)                                               
      DATA qqTEV/1.0d+3,1.5D+3,2.0d+3,3.0d+3,5.0d+3,7.0d+3,             
     &           1.0d+4,1.5D+4,2.0d+4,3.0d+4,5.0d+4,7.0d+4,             
     &           1.0d+5,1.5D+5,2.0d+5,3.0d+5,5.0d+5,7.0d+5,             
     &           1.0d+6,1.5D+6,2.0d+6,3.0d+6,5.0d+6,7.0d+6,1.0d+7/      

      fion=float(ijkion+1)
 
 
      do i=1,25                                                      
        tev(i)=temps(i)*fion*fion/11604.428                          
      end do
                                                               
C-------End ADAS conversion-------




c
c     ks=8
c     kmx=500
c     kexc=kmx
      ikkk=kcpld(8)
      if (nlsmax.ne.0) ikkk=0
      iprint=kcpld(9)
      m2x=max(nsconf(2,1),nsconf(2,2))
      do 99 m3=1,2
      do 99 m2=1,m2x
      do 99 m1=1,3
   99 elem(m1,m2,m3)=blank
      iewr=0
c
c          Read quantum numbers
c
  100 il=ila
      ie=ila
      nlsx=0
      k=1
      g1aa=0.0
      sumgaa=0.0
      sumgaat=0.0
      rewind ic
  101 rewind ie
      read (ic) notsj1,nscrj8,sj8mn,sj8mx,npar,nparj,parnam
      icrd=1
      kk=k
c
c          Read parameter values
c
      if (iprint.ge.7) go to 102
      write (iw,12)
   12 format (1h1)
  102 ixc=0
      ixc1=1
      nlst=0
      do 103 i=1,kexc
      jexc(i)=kexc+1
  103 exc(i)=0.0
c
      j1x=nsconf(2,k)
      npar0=1
      do 131 i=1,j1x
      npav(i)=npar0
      npar1=npar0+1
      npar4=npar0+4
      nparsn=nparj(i,i)+npar0-1
  105 read (ir,10) (elem1(n),n=1,3), x(npar0),
     1  (x(n),jpar(n),n=npar1,npar4),type,ifact
   10 format (2a6,a7,1x,f10.5,4(f9.4,i1),a2,4i2)
c
  109 if (x(npar0).lt.-90000000.0) go to 900
      if (x(npar0).lt.-50000000.0) go to 800
      if (x(npar0).lt.-30000000.0) go to 100
      do 112 n=1,3
  112 elem(n,i,k)=elem1(n)
      do 114 j=1,5
  114 vpar(npar0+j-1,k)=x(npar0+j-1)
      if (nparj(i,i).le.5) go to 120
      npar5=npar4+1
      read (ir,11) (vpar(n,k),jpar(n),n=npar5,nparsn)
   11 format (7(f9.4,i1))
  120 do 121 j=1,4
      if (ifact(j).eq.99.or.ifact(j).eq.0) ifact(j)=100
      eras=ifact(j)
  121 fact(j)=0.01*eras
c          Shift Eav and/or rescale single-configuration parameters
      n1=npar1
      eras=vpar(npar0,k)
      if (eras.le.-4000.0) go to 123
      vpar(npar0,k)=eras+deleav
      n1=npar0
  123 do 125 n=n1,nparsn
  125 vpar(n,k)=scale*vpar(n,k)
      if (npar1.gt.nparsn) go to 127
      do 126 n=npar1,nparsn
      j=jpar(n)
      if (j.lt.1.or.j.gt.4) go to 126
      if (fact0(j).eq.0.0) go to 126
      vpar(n,k)=vpar(n,k)*fact0(j)/fact(j)
  126 continue
  127 do 128 j=1,4
      if (fact0(j).gt.0.0) fact(j)=fact0(j)
  128 continue
      e0kin(i)=0.0
      if (vpar(npar0,k).gt.-4000.0) go to 130
      eras=ee8(i,k)
      eras=eras*109737.0/uenrgy
      e0kin(i)=etot(1,1)-etot(i,k)+eras+vpar(npar0,k)-vpar(1,1)
c
  130 write (iw,13) (elem(n,i,k),n=1,3),uenrgy,type,fact,
     1  i, (llijk(l,i,k),nijk(l,i,k), l=1,nosubc)
   13 format (//1x,3a6,4x,20hParameter values in ,f7.1,7h cm-1 (,
     1  a2,6h times,4f5.2,1h), 3x,i3,8(2x,a1,i2))
      write (iw,14) (parnam(n,k),n=npar0,nparsn)
   14 format (/39x,a10,4(5x,a10)/4x,7(5x,a10)/4x,7(5x,a10)/4x,7(5x,a10))
      write(iw,15) (vpar(n,k),n=npar0,nparsn)
   15 format (/ 30x,5f15.3/(7f15.3))
      npar0=nparsn+1
      cflag(i,k)=blnk
      cgengy(i,k)=0.0
      swt(i,k)=0.0
  131 continue
      not=npar0
      njj=nparj(j1x,j1x)
      if (j1x.le.1) go to 139
      j1xm1=j1x-1
      do 138 j1a=1,j1xm1
      j1ap1=j1a+1
      do 138 j1b=j1ap1,j1x
      jab=max0(j1b-j1a,1)
      if (nparj(j1a,j1b).eq.0) go to 138
      npar4=npar0+4
      read(ir,16) (elem1(n),n=1,3),(vpar(n,k),jpar(n),n=npar0,npar4),
     1  type,ifact
   16 format (2a6,a7,1x,5(f9.4,i1),a2,4i2)
      if (ifact(4).eq.99.or.ifact(4).eq.0) ifact(4)=100
      eras=ifact(4)
      fact5=0.01*eras
      nparsn=nparj(j1a,j1b)+npar0-1
      if (nparj(j1a,j1b).le.5) go to 132
      npar5=npar4+1
      read (ir,11) (vpar(n,k),jpar(n),n=npar5,nparsn)
c          Rescale configuration-interaction parameters
  132 scale1=scale
      j=jpar(npar0)
      if (j.lt.1.or.j.gt.5) go to 133
      if (fact0(j).eq.0.0) go to 133
      scale1=scale*fact0(j)/fact5
      fact5=fact0(j)
  133 do 134 n=npar0,nparsn
  134 vpar(n,k)=scale1*vpar(n,k)
c
  136 write (iw,17) (elem1(n),n=1,3),uenrgy,type,fact5,
     1  (jn, (llijk(l,jn,k),nijk(l,jn,k), l=1,ks), jn=j1a,j1b,jab)
   17 format (//1x,2a6,a7,3x,20hParameter values in ,f7.1,7h cm-1 (,
     1  a2,6h times, f5.2,1h),18x,i3,8(2x,a1,i2)/89x,i3,8(2x,a1,i2))
      write (iw,18) (parnam(n,k),n=npar0,nparsn)
   18 format (/37x,a10,4(5x,a10)/2x,7(5x,a10)/2x,7(5x,a10)/2x,7(5x,a10))
      write (iw,15) (vpar(n,k),n=npar0,nparsn)
      npar0=nparsn+1
  138 continue
  139 do 140 i=1,9
      avp(i,k)=0.0
  140 fnt(i,k)=0.0
      avcg(k)=0.0
      aveig(k)=0.0
      sumwt=0.0
      emin=1.0e20
      emax=-1.0e20
      nlev=0
      npar=nparsn
      npark(k)=npar
      do 150 l=1,kmx
  150 lcser(l)=0
c
c          Calculate energy matrices
c
  200 do 399 nn=1,nscrj8
      read (ic) scrj8,nls,nls1, ((tmx(l,lp),l=1,nls1), lp=1,nls1),
     1  ((nals(l,i),scrl(l,i),scrs(l,i),
     2  i=1,nosubc), ncfg(l),fidls(l,1),fidjj(l,1), l=1,nls1)
           if (nls.le.0) go to 399
      icrd=icrd+1
      nlsx=max0(nlsx,nls)
      noicrd=0
      wt=2.0*scrj8+1.0
      eras=0.0
      if (nls.le.0) nls=1
      do 212 l=1,nls
      do 211 lp=1,l
  211 c(l,lp)=0.0
      j=ncfg(l)
      i=npav(j)
      eras=eras+vpar(i,k)
  212 c(l,l)=vpar(i,k)
      avcg(k)=avcg(k)+eras*wt
      if (npar.le.1) go to 225
      do 222 ipar=2,npar
      do 218 j=1,j1x
      if (ipar.eq.npav(j)) go to 222
  218 continue
c
c          Read coefficient matrix elements and calculate energy matrix
c
c     read (ic) ((ct4(l,lp),l=lp,nls), lp=1,nls)
c     do 220 l=1,nls
c     do 220 lp=1,l
c 220 c(l,lp)=c(l,lp)+vpar(ipar,k)*ct4(l,lp)
      icrd=icrd+1
      noicrd=noicrd+1
      read (ic) ncoeff,(illp(i),coeff(i),i=1,ncoeff)
      do 220 i=1,ncoeff
      l=illp(i)/10000
      lp=illp(i)-10000*l
  220 c(l,lp)=c(l,lp)+vpar(ipar,k)*coeff(i)
c
  222 continue
  225 do 230 l=1,nls
      do 230 lp=1,l
  230 c(lp,l)=c(l,lp)
      if (iprint.ge.7) go to 240
      kpar=3
      call sprin
  240 lx=0
      do 250 l=1,nls
      j=ncfg(l)
      i=npav(j)
      if (vpar(i,k).gt.-4000.0) lx=l
  250 continue
      lxp1=lx+1
      jx=nls-lx+1
      jxp1=jx+1
      nlsp1=nls+1
      l=nosubc
      j=1
      kkk=1
      if (lx.eq.0.or.lx.eq.nls) go to 271
      i=0
      do 260 lp=1,nls
      if (lp.le.lx) go to 260
      i=i+1
      do 255 l=1,lx
      ci(l,i)=c(l,lp)
      c(l,lp)=0.0
  255 c(lp,l)=0.0
  260 continue
c          Calculate statistical weight of first configuration or
c               (if continuum configurations present) of ion core
      do 265 l=lxp1,nls
      j=ncfg(l)
      i=npav(j)
      if (vpar(i,k).lt.-9000.0) go to 266
  265 continue
  266 kkk=k
      m=0
      do 270 i=1,nosubc
      l=nosubc-i+1
      if (nijk(l,j,kkk).gt.0) m=m+1
      if (m.eq.2) go to 271
  270 continue
  271 g1=1.0
      do 280 i=1,l
      e1=4.0*fllijk(i,j,kkk)+2.0
      e2=nijk(i,j,kkk)
  280 g1=g1*fctrl(e1)/(fctrl(e2)*fctrl(e1-e2))
      if (lx.gt.0.and.lx.lt.nls) g1aa=g1
      if (g1aa.gt.0.0) g1=g1aa
c
c          Calculate eigenvalues and vectors
c
  300 i=0
      call clock(time1)
c          c contains matrix to be diagonalized.
c          mlew returns eigenvalues and vectors in eig and c, respectively
c     call overlay(2hg9,8,3)
      call mlew
      eras=0.0
      do 320 lp=1,nls
  320 eras=eras+eig(lp)
      call clock(time)
      tdiag=time-time1
      aveig(k)=aveig(k)+eras*wt
      eras=nls
      sumwt=sumwt+eras*wt
      emin=min(emin,eig(1))
      emax=max(emax,eig(nls))
      nlev=nlev+nls
c          Check orthonormality of eigenvectors
      do 329 l=1,nls
      do 329 lp=l,nls
      eras=0.0
      if (l.eq.lp) eras=-1.0
      eras1=eras
      do 328 j=1,nls
      eras=eras+c(j,l)*c(j,lp)
  328 eras1=eras1+c(l,j)*c(lp,j)
      if (abs(eras).gt.1.0e-10.or.abs(eras1).gt.1.0e-10)
     1  write (iw,29) l,lp,eras,eras1
   29 format (35h *****Poor orthonormality for l,lp=,2i4,3x,1p,2e10.2)
  329 continue
c
c      Calculate configuration serial number for level lp
c
      do 330 j=1,j1x
      nlcfgj(j)=0
      nscfgj(j)=0
      do 330 lp=1,nls
  330 cpurty(lp,j)=0.0
      do 331 l=1,nls
      lcser(l)=0
      j=ncfg(l)
      nscfgj(j)=nscfgj(j)+1
      do 331 lp=1,nls
  331 cpurty(lp,j)=cpurty(lp,j)+c(l,lp)**2
c
      do 333 lp=1,nls
      eras=0.0
      do 332 j=1,j1x
      if (cpurty(lp,j).lt.eras) go to 332
      eras=cpurty(lp,j)
      lcser(lp)=j
      m=j
  332 continue
  333 nlcfgj(m)=nlcfgj(m)+1
c
      do 334 j=1,j1x
  334 if (nlcfgj(j).ne.nscfgj(j)) cflag(j,k)=star
      icount=0
  335 ieras=0
      do 337 j=1,j1x
      if (nlcfgj(j).ge.nscfgj(j)) go to 337
      ieras=7
      eras=0.0
      do 336 lp=1,nls
      j1=lcser(lp)
      if (j1.eq.j.or.nlcfgj(j1).le.nscfgj(j1)) go to 336
      if (cpurty(lp,j).lt.eras) go to 336
      eras=cpurty(lp,j)
      m=lp
  336 continue
      j1=lcser(m)
      lcser(m)=j
      nlcfgj(j1)=nlcfgj(j1)-1
      nlcfgj(j)=nlcfgj(j)+1
  337 continue
      icount=icount+1
      if (ieras.ne.0.and.icount.le.nls) go to 335
      eras=2.0*scrj8+1.0
      do 338 lp=1,nls
      j=lcser(lp)
      cgengy(j,k)=cgengy(j,k)+eras*eig(lp)
  338 swt(j,k)=swt(j,k)+eras
      nntrm=0
c          Calculate level designations
      do 340 lp=1,nls
      c2x=0.0
      do 339 l=1,nls
      if (ncfg(l).ne.lcser(lp)) go to 339
      c2=c(l,lp)**2
      if (c2.lt.c2x) go to 339
      ld=l
      c2x=c2
  339 continue
      ldes(lp)=fidls(ld,1)
      if (kcpl.eq.2) ldes(lp)=fidjj(ld,1)
      if (c(ld,lp).gt.0.0) go to 2339
      do 1339 l=1,nls
 1339 c(l,lp)=-c(l,lp)
c          Determine expediency number (special for Norm Magee)
 2339 if (ielpun.lt.2) go to 340
      if (scrj8.gt.abs(scrl(ld,nosubc)-scrs(ld,nosubc))) go to 340
      if (nntrm.eq.0) go to 4339
      do 3339 n1=1,nntrm
      if (ldes(lp).eq.ldesn(n1)) go to 5339
 3339 continue
 4339 nntrm=nntrm+1
      n1=nntrm
      ldesn(n1)=ldes(lp)
      nexp(n1)=0
 5339 nexp(n1)=nexp(n1)+1
      do 7339 l1=1,nmult
      if (ldes(lp).ne.fidmult(l1,k)) go to 7339
      do 6339 k1=1,nosubc
      if (nals(ld,k1).ne.ials(l1,k1,k)) go to 7339
      if (scrl(ld,k1).ne.sl(l1,k1,k)) go to 7339
      if (scrs(ld,k1).ne.ss(l1,k1,k)) go to 7339
 6339 continue
      nexped(l1,k)=nexp(n1)
      etrm(l1,k)=eig(lp)
 7339 continue
c
  340 continue
c
c          Print eigenvalues and vectors
c
      if (iprint.ge.9) go to 350
      if (iprint.ge.7) go to 342
      if (iengyd.lt.2) go to 342
      if (nls.le.11) go to 342
      if (eig(nls).gt.9999.0) go to 341
      write (iw,30) scrj8,(eig(l),l=1,nls)
   30 format (15h1  Eigenvalues ,5x,3h(J=,f4.1,1h)/(/28x,11f9.3))
      go to 345
  341 write (iw,31) scrj8,(eig(l),l=1,nls)
   31 format (15h1  Eigenvalues ,5x,3h(J=,f4.1,1h)/(/28x,11f9.2))
      go to 345
  342 if (eig(nls).gt.9999.0) go to 343
      write (iw,32) scrj8,(eig(l),l=1,nls)
   32 format (15h0  Eigenvalues ,5x,3h(J=,f4.1,1h)/(/28x,11f9.3))
      go to 345
  343 write (iw,33) scrj8,(eig(l),l=1,nls)
   33 format (15h0  Eigenvalues ,5x,3h(J=,f4.1,1h)/(/28x,11f9.2))
  345 write (iw,34) (lcser(l), l=1,nls)
   34 format (15h   Config. no. /(25x,11i9))
c          Print free-electron kinetic energies
      if (lx.eq.0.or.lx.eq.nls) go to 350
      llx=nls-lx
      lmin=2+11*((llx-11)/11)
      do 349 lp=1,llx
      do 347 l=lmin,nls
      ekin(l)=0.0
      if (l.le.llx) go to 347
      ijj=lcser(lp)
      ekin(l)=eig(l)+e0kin(ijj)-eig(lp)
  347 continue
        do l=1,nls/11
        if (lmin+10.lt.nls.and.ekin(lmin+10).eq.0.0) lmin=lmin+11
        end do
      write (iw,28) (elem(l1,ijj,k), l1=1,3),
     1  ldes(lp),(ekin(l),l=lmin,nls)
   28 format (' KE for core  ',2a6,a2,1x,a8,10f9.3
     1  /(28x,11f9.3))
  349 continue
  350 kpar=4
 
C------Start ADAS conversion-------

      call sprnadd(0)                                                      

C------End ADAS conversion-------
 
  355 write (ie) scrj8,nls, (eig(lp), (c(l,lp),l=1,nls), lp=1,nls),
     1  lcser,ldes
      iewr=iewr+1
  358 if (iprint.ge.8) go to 359
      call sprin
  359 if (ielpun.eq.1) write (11,35) (elem(l,1,k), l=1,3),
     1  scrj8,nls, (eig(l), l=1,nls)
   35 format (3a6,2x,f5.1,i5/(6f12.3))
c
c          Calculate autoionization transition probabilities
c
      do 360 lp=1,nls
      aa(lp,jx)=0.0
      aa(lp,jxp1)=0.0
      do 360 i=1,kexc
  360 gaaxc(lp,i)=0.0
      if (lx.ne.0.and.lx.ne.nls) go to 1360
      nlst=1
      ixc=0
      ixc1=1
      go to 380
 1360 if (nls.gt.kmx) write (iw,36) nls,kmx
   36 format (//10h *****nls=,i3,29h .gt. max allowable value of ,
     1  i3,18h for aa calcn*****//)
      backspace ie
      read (ie) scrj8,nls, (eig(lp), (c(l,lp),l=1,nls), lp=1,nls)
       aamx=0.0
      do 362 lp=1,nls
      do 362 j=lxp1,nls
      i=j-lx
      sum=0.0
      do 361 l=1,lx
  361 sum=sum+c(l,lp)*ci(l,i)
  362 cii(lp,j)=sum
      do 365 lp=1,nls
      do 364 i=1,nls
      if (eig(i).gt.-4000.0) go to 364
      aa(lp,i)=0.0
      if (eig(i).lt.-8000.0.or.emina.eq.0.0) go to 2362
      if (eig(lp).lt.emina) go to 364
      j=lcser(i)
      jj=npav(j)
      etemp=eig(i)-vpar(jj,k)
c          Include following statement only in special cases
c     if (etemp.gt.0.0) go to 364
c
 2362 sum=0.0
      do 363 l=lxp1,nls
  363 sum=sum+cii(lp,l)*c(l,i)
      aa(lp,i)=((sum*uenrgy/109735.3)**2)*6.28318*2066.0
      if (eig(i).lt.-8000.0) aa(lp,jx)=aa(lp,jx)+aa(lp,i)
      aa(lp,jxp1)=aa(lp,jxp1)+aa(lp,i)
  364 continue
  365 aamx=max(aa(lp,jxp1),aamx)
      eras=1.0
      l=13
      if (aamx.lt.0.1) l=10
      if (aamx.gt.999.9) l=16
      l1=l
      if (l.eq.13) go to 367
      eras=10.0**(13-l)
      do 366 lp=1,nls
      do 366 i=1,jxp1
  366 aa(lp,i)=eras*aa(lp,i)
  367 if (iprint.ge.9) go to 371
      write (iw,37) l
   37 format (17h aa(units of 10**,i2,5h/sec))
      lmin=1+11*((llx-11)/11)
      i=0
      do 370 j=lx,nlsp1
      i=i+1
      if (nls.eq.lxp1.and.i.ge.2) go to 370
      if (kcpld(1)+kcpld(2).lt.3) go to 368
      if (i.lt.jx.and.(nls-lx).gt.2.and.nls.gt.11) go to 370
  368 if (j.ge.nls) go to 369
      n1=lcser(i)
        do jjj=1,nls/11
        if (lmin+10.lt.nls.and.aa(lmin+10,i).eq.0.0) lmin=lmin+11
        end do
      write (iw,38) (elem(i1,n1,k),i1=2,3), ldes(i),
     1     (aa(lp,i),lp=lmin,nls)
   38 format (8x,2a6,a8,11f9.4/(28x,11f9.4))
      go to 370
  369 itemp=-8000
      if (j.eq.nlsp1) itemp=-4000
      write (iw,39) itemp, (aa(lp,i),lp=lmin,nls)
   39 format (11x,12hTotal, E.lt.,i5,11f9.4/(28x,11f9.4))
  370 continue
  371 eras=1.0e13/eras

      gg=2.0*scrj8+1.0
      do 372 lp=1,nls
      aa(lp,jx)=eras*aa(lp,jx)
      aa(lp,jxp1)=eras*aa(lp,jxp1)
      sumgaa=sumgaa+gg*aa(lp,jx)
  372 sumgaat=sumgaat+gg*aa(lp,jxp1)
      eras=gg*eras
      jxm1=jx-1
      do 378 l=1,jxm1
      if (eig(l).gt.-4000.0) go to 378
      j=lcser(l)
      jj=npav(j)
      etemp=eig(l)-vpar(jj,k)
      if (ixc.eq.0) go to 375
      do 374 i=1,ixc
      if (abs(etemp-exc(i)).lt.0.01.and.j.eq.jexc(i)) go to 376
  374 continue
  375 ixc=ixc+1
      i=ixc
      exc(i)=etemp
      jexc(i)=j
  376 do 377 lp=1,nls
  377 gaaxc(lp,i)=gaaxc(lp,i)+eras*aa(lp,l)
  378 continue
      ixc1=max0(ixc,1)
      if (iprint.ge.9) go to 380
      write (iw,40) sumgaa,sumgaat
   40 format (/20x,7hsumgaa=,1pe13.5,10x,8hsumgaat=,e13.5//
     1  10x,11hexc   gaaxc/)
  380 write (ie) lx, (aa(lp,jx),aa(lp,jxp1), lp=1,nls),
     1  nlst,ixc,ixc1, (exc(i), (gaaxc(lp,i),lp=jx,nls), i=1,ixc1)
      nlst=nlst+lx
      if (iprint.ge.9) go to 399
      if (nls.le.1) go to 397
      if (ixc.eq.0) go to 388
      if (lx.eq.0.or.lx.eq.nls) go to 388
      eras=10.0**(-l1)
      do 386 i=1,ixc
      do 384 lp=1,nls
  384 gaaxc(lp,i)=eras*gaaxc(lp,i)
  386 write (iw,41) jexc(i),exc(i), (gaaxc(lp,i), lp=lmin,nls)
   41 format (1x,i15,f12.4,11f9.4/(28x,11f9.4))
  388 continue
c
c          Calculate level distribution
c
      fnls=nls
      sum=0.0
      do 391 l=1,nls
  391 sum=sum+eig(l)
      aveeig=sum/(fnls+1.0e-10)
      sum=0.0
      sum3=0.0
      do 392 l=1,nls
      delta=eig(l)-aveeig
      delta2=delta**2
      sum=sum+delta2
  392 sum3=sum3+delta2*delta
      rms1=sqrt(sum/fnls)
      sum3=sum3/fnls
      alf3=sum3/(rms1**3+1.0e-10)
      nlsm1=nls-1
      fnlsm1=nlsm1
      do 393 l=1,nlsm1
  393 dele(l)=eig(l+1)-eig(l)
      sum=0.0
      do 394 l=1,nlsm1
  394 sum=sum+dele(l)
      avedel=sum/fnlsm1
      sum=0.0
      do 395 l=1,nlsm1
  395 sum=sum+(dele(l)-avedel)**2
      rms2=sqrt(sum/fnlsm1)
      call clock(time)
      delt=time-time0
      write (iw,42) aveeig,rms1,alf3,avedel,rms2,delt,tdiag
   42 format (//17h0aveEig,rms,alf3=,2f12.3,f9.4,5x,11havedel,rms=,
     1  2f12.3,7x,15htime, diagtime=,f6.3,f8.5,4h min)
c
c 397 if (iv.ne.0) call overlay(2hg9,8,2)
  397 if (iv.ne.0) call calcv
  398 if (igen.eq.3.or.igen.eq.4) call elecden(kk,scrj8,igen)
c
  399 continue
c           Write dummy record, required by system bug on IBM Stretch
      write (ie) (eig(l), l=1,20)
c
      do 400 j=1,j1x
  400 cgengy(j,k)=cgengy(j,k)*uenrgy/((swt(j,k)+0.00000001)*109737.31)
      avcg(k)=avcg(k)/sumwt
      aveig(k)=aveig(k)/sumwt
      do 410 i=1,9
      if (fnt(i,k).eq.0.0) go to 410
      avp(i,k)=avp(i,k)/fnt(i,k)
  410 continue
      if (avp(1,k).gt.0) go to 415
      do 412 i=1,9
  412 avp(i,k)=1.0
  415 if (k.eq.1.and.nsconf(1,2).gt.0.and.iprint.ge.7) go to 430
      write  (iw,43) (coupl(i), i=1,7)
   43 format (///13h ave purities,29x,9(a6,2x)/)
      do 420 k1=1,k
      j1x=nsconf(2,k1)
      if (j1x.lt.2) j1x=2
      j1=j1x-1
  420 write (iw,44) ((elem(i,jn,k1), i=1,3), jn=1,j1x,j1),
     1  (avp(i,k1), i=1,7), avcg(k1),aveig(k1)
   44 format (1x,3a6,3h---,3a6,7f8.3,5x,6h  Eav=,f10.4/101x,6havEig=,
     1  f10.4)
      enk(k)=emin
      exk(k)=emax
c
c 430 if (ispecc.ge.8) call overlay(2hg9,8,1)
  430 if (ispecc.ge.8) call lvdist
c
      rewind ie
      if (imag.ne.k.and.imag.lt.3) go to 450
c     call overlay(2hg9,8,4)
      iarg1=0
      iarg2=ie
      iarg3=ie
      call spectr
  450 if (iquad.ne.k.and.iquad.lt.3) go to 470
c     call overlay(2hg9,8,4)
      iarg1=2
      iarg2=ie
      iarg3=ie
      irmn=irnq
      call spectr
c     if (igen.ge.5) call overlay(2hg9,8,5)
      if (igen.ge.5) call born
  470 if (nsconf(1,2).le.0) go to 100
      if (ie.eq.ilb) go to 500
  480 il=ilb
      ie=ilb
      k=2
      go to 101
c
c 500 call overlay(2hg9,8,4)
  500 iarg1=iiprty
      iarg2=ila
      iarg3=ilb
      irmn=irnd
      emean=0.5*(max(enk(1),enk(2))+max(exk(1),exk(2)))
      call spectr
      
c          Write multiplet line strength on tape 19 (special for Norm Magee)
      if (ielpun.lt.2) go to 515
      stot=0.0
      do 505 nn=1,nmult
  505 stot=stot+sums(nn)
      write (19,50) ((llijk(i,1,k),nijk(i,1,k),i=1,3), k=1,2),
     1  nmult,stot,ssopi2
   50 format (2x,3(a1,i2,2x),' - ',3(a1,i2,2x),'     nmult=',i3,
     1  '     Stot=',f9.5,'     Theory=',f9.5)
      do 510 nn=1,nmult
  510 write (19,51) (fidmult(nn,k),nexped(nn,k),etrm(nn,k), k=1,2),
     1  sums(nn)
   51 format (2x,a8,i5,f12.5,5x,a8,i5,f12.5,f15.5)
c
c     if (igen.ge.5) call overlay(2hg9,8,5)
  515 if (igen.ge.5) call born
      if (ikkk.le.8) go to 100
      do 520 i=1,icrd
  520 backspace ic
      go to 480
c
c 800 call overlay(2hg9,8,6)
  800 call rceinp
      go to 100
c
  900 return
      end
