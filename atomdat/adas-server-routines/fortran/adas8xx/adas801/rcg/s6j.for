C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/s6j.for,v 1.2 2004/07/06 15:18:33 whitefor Exp $ Date $Date: 2004/07/06 15:18:33 $
C
      function s6j(fj1,fj2,fj3,fl1,fl2,fl3)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      common/cfactrl/scale,scalem1,fctn(200),fctz,fct(0:200)
c
      delsq(ia,ib,ic,id)=fct(id-ia)*fct(id-ib)*fct(id-ic)/fct(id+1)
      s6j=0.0
      s1=fj1+fj2+fj3
      s2=fj1+fl2+fl3
      s3=fl1+fj2+fl3
      is1=s1
      is2=s2
      is3=s3
      if ((s1-is1).gt.0.01) go to 200
      if ((s2-is2).gt.0.01) go to 200
      if ((s3-is3).gt.0.01) go to 200
      is4=fl1+fl2+fj3
      is5=fj1+fj2+fl1+fl2
      is6=fj2+fj3+fl2+fl3
      is7=fj3+fj1+fl3+fl1
      kn=max0(is1,is2,is3,is4)
      kx=min0(is5,is6,is7)
      if (kx.lt.kn) go to 200
      it1=2.000001*fj1
      it2=2.000001*fj2
      it3=2.000001*fj3
      it4=2.000001*fl1
      it5=2.000001*fl2
      it6=2.000001*fl3
      coef=sqrt(delsq(it1,it2,it3,is1)*delsq(it1,it5,it6,is2)
     1         *delsq(it4,it2,it6,is3)*delsq(it4,it5,it3,is4))*scalem1
      sgn=-1+2*mod(kn,2)
      sum=0.0
      do 100 k=kn,kx
      sgn=-sgn
  100 sum=sum+sgn*fct(k+1)/(fct(k-is1)*fct(k-is2)*fct(k-is3)*fct(k-is4)
     1  *fct(is5-k)*fct(is6-k)*fct(is7-k))
      s6j=coef*sum
  200 return
      end
