C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/clas10.for,v 1.2 2004/07/06 12:05:24 whitefor Exp $ Date $Date: 2004/07/06 12:05:24 $
C
      subroutine clas10
c
c          Calculate for irho.lt.irhop.lt.isig.lt.isigp
c                 or for irho.lt.irhop.lt.isigp.lt.isig
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
      flljn=fllsig
      flljx=fllsigp
      if (irho.lt.irhop.and.isig.le.isigp) go to 110
      if (irho.ge.irhop.and.isig.ge.isigp) go to 110
      flljn=fllsigp
      flljx=fllsig
  110 la=l1
      lb=l2
c      ja=j11   ! lines seem unncessary 
c      jb=j12   ! ADW 01/02/02
      tl(1,jn)=flljn
      tl(2,jn)=flljx
      ts(1,jn)=0.5
      ts(2,jn)=0.5
      if (l1.eq.lci.and.isig.le.isigp) go to 120
      if (l1.eq.lcip.and.isigp.le.isig) go to 120
      la=l2
      lb=l1
      tl(1,jn)=flljx
      tl(2,jn)=flljn
c
  120 i=ix
      j=jn
      nii=1
      nij=1
      n1=nalsp(l1,i)
      n2=nalsp(l2,i)
      n3=nalsp(la,j)
      n4=nalsp(lb,j)
      a1=pscrl(l1,i-1)
      a2=pscrl(l2,i-1)
      a3=pscrs(l1,i-1)
      a4=pscrs(l2,i-1)
      a5=pscrl(l1,i)
      a6=pscrl(l2,i)
      a7=pscrs(l1,i)
      a8=pscrs(l2,i)
      a9=pscrl(la,j-1)
      a10=pscrl(lb,j-1)
      a11=pscrs(la,j-1)
      a12=pscrs(lb,j-1)
      a13=pscrl(la,j)
      a14=pscrl(lb,j)
      a15=pscrs(la,j)
      a16=pscrs(lb,j)
      b1=fl(n1,i)
      b2=fl(n2,i)
      b3=s(n1,i)
      b4=s(n2,i)
      b5=fl(n3,j)
      b6=fl(n4,j)
      b7=s(n3,j)
      b8=s(n4,j)
      if (jn.lt.nosubc.or.jx.lt.nosubc) go to 130
      a14=a10
      a16=a12
      b5=flljn
      b6=0.0
      b7=0.5
      b8=0.0
c
  130 tli=max(abs(fll(in)-a5),abs(a2-b1),abs(fll(i)-a6))-1.0
      ntli=min(fll(in)+a5,a2+b1,fll(i)+a6)-tli
      if (ntli.le.0) go to 900
      tljn=max(abs(flljn-a9),abs(a13-b6),abs(flljx-a10))-1.0
      ntlj=min(flljn+a9,a13+b6,flljx+a10)-tljn
      if (ntlj.le.0) go to 900
      tsin=max(abs(0.5-a7),abs(a4-b3),abs(0.5-a8))-1.0
      ntsi=min(0.5+a7,a4+b3,0.5+a8)-tsin
      if (ntsi.le.0) go to 900
      tsjn=max(abs(0.5-a11),abs(a15-b8),abs(0.5-a12))-1.0
      ntsj=min(0.5+a11,a15+b8,0.5+a12)-tsjn
      if (ntsj.le.0) go to 900
c
      jm1=j-1
      do 150 iii=1,2
      m=l1
      if (iii.eq.2) m=l2
      do 150 ip=i,jm1
      ii=nalsp(m,ip)
      tl(iii,ip)=fl(ii,ip)
      ts(iii,ip)=s(ii,ip)
      tscl(iii,ip)=pscrl(m,ip)
  150 tscs(iii,ip)=pscrs(m,ip)
      tl(1,i)=fll(in)
      tl(2,i)=fll(ix)
      ts(1,i)=0.5
      ts(2,i)=0.5
c
      e1=sqrt((2.0*a1+1.0)*(2.0*a3+1.0)*(2.0*b2+1.0)*(2.0*b4+1.0)
     1  *(2.0*b5+1.0)*(2.0*b7+1.0)*(2.0*a14+1.0)*(2.0*a16+1.0))
      xx1=fll(in)+fll(i)+flljx+b1+b1+a1+a2+a6+b5+b6+a9+a13+a14
     1  +1.5+b3+b3+a3+a4+a8+b7+b8+a11+a15+a16
      do 200 nli=1,ntli
      tli=tli+1.0
      e2=e1*(2.0*tli+1.0)*s6j(fll(in),a2,a1, b1,a5,tli)
      e2=e2*s6j(a2,b1,tli, fll(i),a6,b2)
      tscl(1,i-1)=tli
      tscl(2,i-1)=tli
      tlj=tljn
      do 200 nlj=1,ntlj
      tlj=tlj+1.0
      e3=e2*(2.0*tlj+1.0)*s6j(b6,flljn,b5, a9,a13,tlj)
      e3=e3*s6j(b6,a10,a14, flljx,a13,tlj)
      tscl(1,j)=tlj
      tscl(2,j)=tlj
      tsi=tsin
      do 200 nsi=1,ntsi
      tsi=tsi+1.0
      e4=e3*(2.0*tsi+1.0)*s6j(half,a4,a3, b3,a7,tsi)
      e4=e4*s6j(a4,b3,tsi, half,a8,b4)
      tscs(1,i-1)=tsi
      tscs(2,i-1)=tsi
      tsj=tsjn
      do 200 nsj=1,ntsj
      tsj=tsj+1.0
      e5=e4*(2.0*tsj+1.0)*s6j(b8,half,b7, a11,a15,tsj)
      e5=e5*s6j(b8,a12,a16, half,a15,tsj)
      xx=xx1+tli+tlj+tsi+tsj
      if (mod(xx,two).gt.zero) e5=-e5
      tscs(1,j)=tsj
      tscs(2,j)=tsj
      tk=kdmin-2
      do 160 nn=1,nkd
      tk=tk+2.0
      call rdij(i,j)
      ccc(nn)=ccc(nn)+e5*a
  160 continue
      n1=nkd+1
      tk=kemin-2
      do 170 nn=n1,nktot
      tk=tk+2.0
      call reij(i,j)
      ccc(nn)=ccc(nn)+e5*rsum
  170 continue
  200 continue
c
  900 return
      end
