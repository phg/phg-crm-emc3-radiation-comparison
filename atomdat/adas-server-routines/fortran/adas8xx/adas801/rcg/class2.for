C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/class2.for,v 1.2 2004/07/06 12:05:43 whitefor Exp $ Date $Date: 2004/07/06 12:05:43 $
C
      subroutine class2
c
c          Calculate for irho=isig.lt.irhop=isigp
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
      n1=nalsp(l1,jx)
      n2=nalsp(l2,jx)
      n3=nalsp(l1,in)
      n4=nalsp(l2,in)
      a1=pscrl(l1,jx)
      a2=pscrs(l1,jx)
      a3=pscrl(l1,jx-1)
      a4=pscrl(l2,jx-1)
      a5=pscrs(l1,jx-1)
      a6=pscrs(l2,jx-1)
      b1=fl(n1,jx)
      b2=fl(n2,jx)
      b3=s(n1,jx)
      b4=s(n2,jx)
      b5=fl(n3,in)
      b6=fl(n4,in)
      b7=s(n3,in)
      b8=s(n4,in)
c
      tfl=abs(a3-a4)-1.0
      nmin=tfl+2.0
      nmax=min(2.0*fll(in),2.0*fllsigp,a3+a4)+1.0
      if (nmin.gt.nmax) go to 900
      tfsn=abs(a5-a6)
      tfsx=a5+a6
c
      e1=sqrt((2.0*b2+1.0)*(2.0*b4+1.0)*(2.0*a3+1.0)*(2.0*a5+1.0))
      xx=a4+a6+b2+b4+a1+a2+fll(in)+fllsigp
      if (mod(xx,two).gt.zero) e1=-e1
      mn=in+1
      mx=jx-1
      if (mn.gt.mx) go to 170
      do 150 m=mn,mx
      e1=e1*sqrt((2.0*pscrl(l2,m)+1.0)*(2.0*pscrs(l2,m)+1.0)
     1  *(2.0*pscrl(l1,m-1)+1.0)*(2.0*pscrs(l1,m-1)+1.0))
      i=nalsp(l2,m)
      xx=fl(i,m)+s(i,m)+pscrl(l2,m)+pscrs(l2,m)+pscrl(l1,m-1)
     1  +pscrs(l1,m-1)
      if (mod(xx,two).gt.zero) e1=-e1
  150 continue
  170 if (in.eq.1) go to 200
      a7=pscrl(l1,in)
      a8=pscrl(l2,in)
      a9=pscrs(l1,in)
      a10=pscrs(l2,in)
      a11=pscrl(l1,in-1)
      a12=pscrs(l1,in-1)
      e1=e1*sqrt((2.0*a8+1.0)*(2.0*a10+1.0)*(2.0*b5+1.0)*(2.0*b7+1.0))
      xx=a11+a12+b6+b8+a7+a9
      if (mod(xx,two).gt.zero) e1=-e1
c
  200 i1=n1-notsj1(jx,j11)
      i2=n2-notsj1(jx,j12)
      i3=n3-notsj1(in,j11)
      i4=n4-notsj1(in,j12)
      do 300 n=nmin,nmax
      tfl=tfl+1.0
      tfs=(1+(-1)**n)/2
      if (tfs.lt.tfsn.or.tfs.gt.tfsx) go to 300
      e2=cfgp1(i3,i4,n)*cfgp2(i2,i1,n)
      if (e2.eq.0.0) go to 300
      if (tfs.eq.1.0) e2=-e2
      e2=e1*e2*s6j(b1,tfl,b2, a4,a1,a3)*s6j(b3,tfs,b4, a6,a2,a5)
      if (mn.gt.mx) go to 270
      do 250 m=mn,mx
      i=nalsp(l2,m)
      e2=e2*s6j(fl(i,m),pscrl(l2,m-1),pscrl(l2,m),
     1          tfl,pscrl(l1,m),pscrl(l1,m-1))
  250 e2=e2*s6j(s(i,m),pscrs(l2,m-1),pscrs(l2,m),
     1          tfs,pscrs(l1,m),pscrs(l1,m-1))
  270 if (in.eq.1) go to 280
      e2=e2*s6j(a11,b6,a8, tfl,a7,b5)
      e2=e2*s6j(a12,b8,a10, tfs,a9,b7)
  280 fk=kdmin-2
      do 290 nn=1,nkd
      fk=fk+2.0
  290 ccc(nn)=ccc(nn)
     1  +e2*s6j(fll(in),fll(in),tfl, fllsigp,fllsigp,fk)
  300 continue
c
  900 return
      end
