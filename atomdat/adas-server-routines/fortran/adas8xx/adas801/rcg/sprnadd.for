C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/sprnadd.for,v 1.5 2009/02/24 11:39:26 afoster Exp $ Date $Date: 2009/02/24 11:39:26 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  1.1 07-01-2000 : This is a new subroutine to output the purity of
C      	            the energy levels.
C  Martin O'Mullane
C
C  Modified:
C       1.2   Martin O'Mullane
C	        - Rewrite with a better algorithm to reassign same
C                 labeled levels with unmatched ones.
C  	        - Add purity value to Eigenvalue line.
C       1.3   Martin O'Mullane
C	        - Replace parameter statements by include definitions.
C       1.4   Martin O'Mullane
C	        - Update the configuration when re-asigning 
C                 unlabeled levels.
C	
C
C  Date:
C	1.2   03-12-2002
C	1.3   15-04-2003
C	1.4   25-02-2004
C
C-----------------------------------------------------------------------
C
C  VERSION : 1.5
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
      subroutine sprnadd(nprint)
      implicit real*8 (a-h,o-z)

      include 'common.h'
      include 'rcg.h'

      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
       common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc1/c(kmx,kmx)
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 elem(3,kc,2),elem1(3),d,e
      common/char2/elem,elem1
      character*3 fidlsad(kmx)
      common/charad/fidlsad

      logical match
      character*12 fidout
      character*60 compeig(kmx,7)
      dimension aa(kmx),laa(kmx),inddom(kmx),
     &          indem(kmx), irelab(kmx), eigall(kmx,kmx)



C Initialise variables

      do ii=1,kmx
        do jj=1,7
          compeig(ii,jj)= ' '
        end do
        inddom(ii) = -1
        indem(ii)  = -1
        irelab(ii) = -1
        do jj=1,kmx
          eigall(jj,ii)= -1.0D0
        end do
      end do



C Find dominant basis function of each level

      do lpvar=1,nls

        do index=1,nls
          aa(index)  = c(index,lpvar)
          laa(index) = index
        end do

        do lvar=1,nls-1
          mm  = lvar
          do index=lvar+1,nls
            if (dabs(aa(index)).gt.dabs(aa(mm))) mm = index
          end do
          temp      = aa(mm)
          aa(mm)    = aa(lvar)
          aa(lvar)  = temp
          ltemp     = laa(mm)
          laa(mm)   = laa(lvar)
          laa(lvar) = ltemp
        end do

        write(compeig(lpvar,1),899)eig(lpvar),(eig(lpvar)*1000)

        inddom(lpvar)=laa(1)


C Assume for now that they are unique and copy info into compeig

        if (kcpl1.le.1) then
          n=ncfg(laa(1))
          d=elem(2,n,kk)
          e=elem(3,n,kk)
          write(fidout,802)fidlsad(laa(1)),
     &                     fidls(laa(1),1)(4:5),
     &                     fidls(laa(1),1)(6:8)
          write(compeig(lpvar,2),810)n,d,e,fidout,
     &                               int(0.5 + (aa(1)**2)*100.0)
        endif

        do index=1,5
          if (index.gt.nls) goto 895
          n=ncfg(laa(index))
          d=elem(2,n,kk)
          e=elem(3,n,kk)
          if (kcpl1.le.1) then
            write(fidout,802)fidlsad(laa(index)),
     &                       fidls(laa(index),1)(4:5),
     &                       fidls(laa(index),1)(6:8)
            write(compeig(lpvar,(index+2)),800)n,d,e,fidout,
     &                    laa(index),aa(index),(aa(index)**2)*100
          else
            write(compeig(lpvar,(index+2)),803)n,d,e,
     &                    fidjj(laa(index),1),laa(index),aa(index),
     &                   (aa(index)**2)*100
          endif
        end do
  895   continue

      end do





C Find the duplicated dominant levels

      do lpvar=1,nls
        irelab(lpvar) = 0
        do index=1,nls
          if (index.NE.lpvar) then
             if (inddom(lpvar).EQ.inddom(index)) irelab(lpvar)=1
          endif
        end do
      end do


C Set up 2D array of eigenvalue**2

      do lpvar=1,nls
         eigall(lpvar,inddom(lpvar)) = c(inddom(lpvar),lpvar)**2
      end do


C Go through each index identifying the dominant one and determine
C whether inddom should be relabeled - eigall set to a default of 
C -1.0 to avoid case where all are zero.

      do index=1,nls
        idom = -1
        emax = 0.0D0
        do lpvar=1,nls
           if (eigall(lpvar,index).GE.emax) then
              emax = eigall(lpvar,index)
              idom = lpvar
           endif
        end do
        if (idom.GT.-1) irelab(idom) = 0
      end do


C Find basis functions which have no match

      do lpvar=1,nls
        match = .FALSE.
        do index=1,nls
          if (inddom(index).eq.lpvar) then
            match = .TRUE.
            goto 755
          endif
        end do
  755   continue
        if (.not.match) indem(lpvar) = lpvar
      end do


C Now we should have the same number of re-labeled as unmatched levels

      nrlab = 0
      nunmt = 0
      do lpvar = 1,nls
        if (irelab(lpvar).GT.0) nrlab = nrlab+1
        if (indem(lpvar).GT.0)  nunmt = nunmt+1
      end do

      if (nrlab.NE.nunmt) then
         write(0,'(a,f4.1,2i5)')'Problem in labeling : J=', scrj8,
     &              nrlab, nunmt
      endif


C Assign the unmatched levels to the relabeled ones according to
C their magnitude within the level
C
C Set up 2D array of eigenvalue**2 - reuse eigall. Only set the following
C - first dimension  : levels needing reassignment
C - second dimension : unmatched set

      do ii=1,nls
        do jj=1,nls
          eigall(ii,jj)= 0.0D0
        end do
      end do

      do lpvar=1,nls
         if (irelab(lpvar).EQ.1) then
            index = lpvar
            do jj=1, nls
               if (indem(jj).NE.-1) then
                  eigall(index,indem(jj)) = c(indem(jj),index)**2
               endif
            end do
         endif
      end do


C Cycle through and assign dominant eigenvalues from unmatched list
C
C Find largest eigenvalue of entire set and assign it to lpvar, then
C remove the rleabeled lpvar and the now matched index eigall values.
C Continue until all relabeled levels are reassigned.


       do while (nrlab.GT.0)

          emax = 0.0D0
          do lpvar=1,nls
            do index=1,nls
              if (eigall(lpvar,index).GE.emax) then
                 emax   = eigall(lpvar,index)
                 ileft  = lpvar
                 iright = index
              endif
           end do
         end do
         do index = 1,nls
           eigall(ileft,index)  = 0.0D0
           eigall(index,iright) = 0.0D0
         end do

         nrlab         = nrlab-1
         irelab(ileft) = 2
         inddom(ileft) = iright
         indem(iright) = -1

      end do



C Write in the final identified level and update ldes array to
C propagate changes through to spectrum and collision calculation.
C
C Also update lcser which holds the configuration information.

        do lpvar = 1, nls

          if (irelab(lpvar).EQ.2) then

             if (kcpl1.le.1) then
               index = inddom(lpvar)
               n     = ncfg(index)
               d     = elem(2,n,kk)
               e     = elem(3,n,kk)
               write(fidout,802)fidlsad(index),
     &                          fidls(index,1)(4:5),
     &                          fidls(index,1)(6:8)
               write(compeig(lpvar,2),820)n,d,e,fidout,
     &               int(0.5 + (c(index,lpvar)**2)*100.0)
               
               ldes(lpvar)  = fidls(index,1)
               lcser(lpvar) = n
               
             endif

          endif

        end do

C Write the reuslts to the RCG output file


      write(iw,898)
 	
      if (nprint.eq.1) then
        do lpvar=1,nls
          write(iw,801)
          write(iw,861)compeig(lpvar,1)(1:32),compeig(lpvar,2)
          write(iw,862)(compeig(lpvar,kkk),kkk=3,7)
          write(iw,863)
        end do
      endif



  800 format(3x,i3,2a6,2x,a12,i3,3x,f9.5,4x,f6.2)
  801 format(1x,' ')
  802 format(a3,1x,1h(,a2,1h),1x,a3)
  803 format(3x,i3,2a6,2x,a8,i3,3x,f9.5,4x,f6.2)

  810 format(3x,i3,2a6,2x,a12,9x,i4)
  820 format(3x,i3,2a6,2x,a12,3x,'re-lab',i4)

  899 format(2f16.2)

  861 format(3x,'Eigenvalue  ',a32,5x,'Identified as  ',a60,/)
  862 format(5(a60,/):)
  863 format(1h0)
  898 format(1h0,2x,'Composition of levels')

      end
