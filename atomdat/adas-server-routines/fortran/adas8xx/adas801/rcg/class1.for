C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/class1.for,v 1.2 2004/07/06 12:05:35 whitefor Exp $ Date $Date: 2004/07/06 12:05:35 $
C
      subroutine class1
c
c          Calculate for irho=isig=irhop.lt.isigp
c                 or for irhop.lt.irho=isig=isigp
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
      n1=nalsp(l1,ix)
      n2=nalsp(l2,ix)
      if (ix.eq.1) go to 120
      a1=pscrl(l1,ix-1)
      a2=pscrl(l2,ix-1)
      a3=pscrs(l1,ix-1)
      a4=pscrs(l2,ix-1)
  120 a5=pscrl(l1,ix)
      a6=pscrl(l2,ix)
      a7=pscrs(l1,ix)
      a8=pscrs(l2,ix)
      b1=fl(n1,ix)
      b2=fl(n2,ix)
      b3=s(n1,ix)
      b4=s(n2,ix)
c
      e1=1.0
      if (in.lt.ix) go to 130
      flli=fllsigp
      if (irho.eq.1) go to 141
      e1=recpsh(a1,b2,a6,flli,a5,b1)*recpsh(a3,b4,a8,half,a7,b3)
      go to 140
  130 flli=fll(in)
      e1=-recpjp(a1,flli,a2,b2,a5,b1)*recpjp(a3,half,a4,b4,a7,b3)
  140 if (e1.eq.0.0) go to 900
c
  141 i=1
      j=2
      nii=min0(nijk(ix,j11,kk),nijk(ix,j12,kk))
      nij=1
      tl(2,1)=b2
      ts(2,1)=b4
      tscl(2,1)=b2
      tscs(2,1)=b4
      tl(1,2)=fll(ix)
      ts(1,2)=0.5
      tl(2,2)=flli
      ts(2,2)=0.5
      tscl(1,2)=b1
      tscl(2,2)=b1
      tscs(1,2)=b3
      tscs(2,2)=b3
c
      if (nii-1) 900,144,143
  143 nlp=notsj1(ix,j12+1)-notsj1(ix,j12)
      kpi=n2-notsj1(ix,j12)
      go to 145
  144 nlp=1
      tl(1,1)=fll(ix)
      ts(1,1)=0.5
      tscl(1,1)=fll(ix)
      tscs(1,1)=0.5
      e2=e1
  145 do 200 m=1,nlp
      if (nii.le.1) go to 152
      ki=m
      n=n1-notsj1(ix,j11)
      if (ix.gt.1) go to 150
      e2=e1*cfp1(n,m)
      go to 151
  150 e2=e1*cfp2(n,m,ix-1)
  151 if (e2.eq.0.0) go to 200
      n=m+notsj1(ix,j12)
      tl(1,1)=fl(n,ix)
      ts(1,1)=s(n,ix)
      tscl(1,1)=fl(n,ix)
      tscs(1,1)=s(n,ix)
  152 tk=kdmin-2
      do 160 nn=1,nkd
      tk=tk+2.0
      call rdij(i,j)
      ccc(nn)=ccc(nn)+e2*a
  160 continue
  200 continue
c
  900 return
      end
