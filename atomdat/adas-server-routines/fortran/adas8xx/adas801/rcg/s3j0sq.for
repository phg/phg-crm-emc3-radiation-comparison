C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/s3j0sq.for,v 1.2 2004/07/06 15:18:08 whitefor Exp $ Date $Date: 2004/07/06 15:18:08 $
C
      function s3j0sq(fj1,fj2,fj3)
c
c          Calculate square of 3-j symbol with zero magnetic quantum nos.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      common/cfactrl/scale,scalem1,fctn(200),fctz,fct(0:200)
c
      s3j0sq=0.0
      fj=fj1+fj2+fj3
      ifj=fj
      if (mod(ifj,2).ne.0) go to 100
      ia=fj-2.0*fj1
      ib=fj-2.0*fj2
      ic=fj-2.0*fj3
      if (ia.lt.0.or.ib.lt.0.or.ic.lt.0) go to 100
      b=fct(ia)*fct(ib)*fct(ic)/fct(ifj+1)
      a=fct(ifj/2)/(fct(ia/2)*fct(ib/2)*fct(ic/2))
      s3j0sq=scalem1*b*(a**2)
  100 return
      end
