C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/lvdist.for,v 1.2 2004/07/06 14:13:53 whitefor Exp $ Date $Date: 2004/07/06 14:13:53 $
C
c     overlay(g9,10,1)
      subroutine lvdist
c
c          Plot level distribution on microfilm (obsolete)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c3d/ecut(42),eplot(82),icut(42),gcut(42),title(20),gplt(82)
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      common/c6/emin,emax,sumwt,flbdn,flbdx,fnunp,fnuxp,nlev,iewr
      common/cje07/ixl,ixr,iyt,iyb,xmn,xmx,ymx,ymn
      character*8 elem(3,kc,2),elem1(3)
      common/char2/elem,elem1
c
  100 if (nlev.lt.10) go to 900
      k=kk
      fn=40.0
      del=0.0005*(emax-emin)
      emin=emin-del
      emax=emax+del
      dele=(emax-emin)/fn
      ecut(1)=emin
      eplot(1)=emin-0.5*dele
      np2=fn+2.0
      do 150 j=1,np2
      if (j.eq.1) go to 140
      ecut(j)=ecut(j-1)+dele
      eplot(j)=eplot(j-1)+dele
  140 icut(j)=0
  150 gcut(j)=0.0
c
      rewind ie
      disp=0.0
      third=0.0
      do 300 nn=1,nscrj8
      read (ie) scrj8,nls, (eig(l), (b, lp=1,nls), l=1,nls)
      b=b
      read (ie)
      wt=2.0*scrj8+1.0
      jcut=2
      do 230 n=1,nls
  210 if (eig(n).lt.ecut(jcut)) go to 220
      jcut=jcut+1
      go to 210
  220 icut(jcut)=icut(jcut)+1
      gcut(jcut)=gcut(jcut)+wt
      eras1=eig(n)-aveig(k)
      eras2=wt*eras1*eras1
      disp=disp+eras2
  230 third=third+eras2*eras1
  300 continue
      disp=disp/sumwt
      rms=sqrt(disp)
      third=third/sumwt
      alf3=third/(rms**3)
      write (iw,30)
   30 format (//1h0,8x,1hj,7x,4hecut,8x,5heplot,7x,4hicut,6x,4hgcut,8x,
     1  5hsumwt,7x,4hdisp,9x,3hrms,8x,4half3/)
      do 310 j=1,np2
      gcut(j)=gcut(j)/sumwt
      write(iw,31)j,ecut(j),eplot(j),icut(j),gcut(j),sumwt,disp,rms,alf3
   31 format (i10,2f12.3,i10,f12.6,f12.0,2f12.3,f12.4)
  310 continue
c     np1=np2-1
c     l=0
c     do 320 j=1,np1
c     l1=j-1
c     do 320 j1=1,2
c     l=l+1
c     l1=l1+1
c     eplot(l)=ecut(j)
c 320 gplt(l)=gcut(l1)
c     np1t2=2*np1
c
c     j1x=max0(nsconf(2,k),2)
c     j1=j1x-1
c     encode (44,40,title) ((elem(i,jn,k),i=1,3), jn=1,j1x,j1)
c  40 format (6x,3a6,2h--,3a6)
c     encode (111,41,title(6)) nlev,sumwt,rms,alf3
c  41 format (50x,5hnlev=,i4,4x,6hsumwt=,f7.0,4x,4hrms=,f10.3,4x,
c    1  5half3=,f8.4)
c     call plojb(eplot,gplt,np1t2,1,0,28,0.0,1.0,0.93,title,161,
c    1  13he (1000 cm-1),13,29hfractional statistical weight,29)
c     do 420 i=1,3
c     fi=i
c     eplot(i)=avcg(k)+(fi-2.0)*rms
c 420 gcut(i)=0.0
c     call plojb(eplot,gcut,3,1,1,-24,0.0,1.0,0.93,title,0,
c    1  b,0,b,0)
c     call lincnt(59)
c     write (12,42) (coupl(i), i=1,7)
c  42 format (10x,13h ave purities,29x,9(a6,2x)/)
c     write (12,44) ((elem(i,jn,k), i=1,3), jn=1,j1x,j1),
c    1  (avp(i,k), i=1,7), avcg(k)
c  44 format (11x,3a6,3h---,3a6,7f8.3,5x,6h  eav=,f10.4)
c
c          Plot skewed-gaussian curve
c
c     de=(xmx-xmn)/81.0
c     eplot(1)=xmn
c     do 505 j=2,82
c 505 eplot(j)=eplot(j-1)+de
c     c=(ecut(2)-ecut(1))/(rms*sqrt(2.0*3.14159))
c     do 510 j=1,82
c     x=(eplot(j)-aveig(k))/rms
c     x2=x*x
c     y=1.0-0.5*alf3*(x-x*x2/3.0)
c 510 gplt(j)=c*y*exp(-0.5*x2)
c     call plojb(eplot,gplt,82,1,0,-28,0.0,1.0,0.95,title,0,b,0,b,0)
  900 return
      end
