C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/reij.for,v 1.2 2004/07/06 14:50:17 whitefor Exp $ Date $Date: 2004/07/06 14:50:17 $
C
      subroutine reij(i,j)
c
c          Calculate Coulomb exchange coeffs. re(i,j)
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
      data sqrt3h/1.224744871/
c
  400 ixx=tl(1,j)+tscl(2,j-1)+tscl(1,j)
      ar=(-one)**ixx
      ixx=ts(1,j)+tscs(2,j-1)+tscs(1,j)
      a1r=(-one)**ixx
      a1r=a1r*s6j(tscs(1,j-1),ts(1,j),tscs(1,j),ts(2,j),tscs(2,j-1),
     1  one)
      mn=i+1
      mx=j-1
      if (mn-mx) 420,420,422
  420 do 421 m1=mn,mx
      ixx=tscl(1,m1-1)+tl(1,m1)+tscl(2,m1)
      if (mod(ixx,2).gt.0) ar=-ar
      ar=ar*sqrt((2.0*tscl(1,m1)+1.0)*(2.0*tscl(2,m1)+1.0))
  421 a1r=a1r*uncpla(tscs(1,m1-1),ts(1,m1),tscs(1,m1),
     1      one,tscs(2,m1-1),tscs(2,m1))
  422 if (i-1) 430,430,425
  425 ixx=tscl(1,i-1)+tscl(1,i)+tl(2,i)
      if (mod(ixx,2).gt.0) ar=-ar
      ar=ar*sqrt((2.0*tscl(1,i)+1.0)*(2.0*tscl(2,i)+1.0))
      a1r=a1r*uncplb(tscs(1,i-1),ts(1,i),tscs(1,i),
     1      one,ts(2,i),tscs(2,i))
  430 a1r=a1r*ar
      do 431 m1=i,mx
      if (tscs(1,m1)-tscs(2,m1)) 433,431,433
  431 continue
      if (ts(1,i)-ts(2,i)) 433,432,433
  432 if (ts(1,j)-ts(2,j)) 433,440,433
  433 ar=zero
c
  440 rsum=zero
      r=kdmin-1
      do 469 irp1=1,irp1mx
      r=r+1.0
      n=r+1.0
      pr=s6j(tscl(1,j-1),tscl(2,j-1),r, tl(2,j),tl(1,j),tscl(1,j))
      p1r=4.0
      if (mn-mx) 443,443,445
  443 do 444 m1=mn,mx
      if (mod(r,two).gt.zero) pr=-pr
  444 pr=pr*s6j(tscl(1,m1-1),tscl(2,m1-1),r,
     1    tscl(2,m1),tscl(1,m1),tl(1,m1))
  445 if (i.le.1) go to 447
      if (mod(r,two).gt.zero) pr=-pr
      pr=pr*s6j(tl(1,i),tl(2,i),r,
     1          tscl(2,i),tscl(1,i),tscl(1,i-1))
  447 p1r=p1r*pr*a1r
      pr=pr*ar
      if (nii.gt.1) go to 450
      p1r=p1r*sqrt3h
      go to 459
  450 if (irho.ne.0) go to 451
      go to(451,452,453,454,455,456,457,458) i
  451 pr=pr*u1(ki,kpi,n)
      p1r=p1r*v1(ki,kpi,n)
      go to 459
  452 pr=pr*u2(ki,kpi,n)
      p1r=p1r*v2(ki,kpi,n)
      go to 459
  453 pr=pr*u3(ki,kpi,n)
      p1r=p1r*v3(ki,kpi,n)
      go to 459
  454 pr=pr*u4(ki,kpi,n)
      p1r=p1r*v4(ki,kpi,n)
      go to 459
  455 pr=pr*u5(ki,kpi,n)
      p1r=p1r*v5(ki,kpi,n)
      go to 459
  456 pr=pr*u6(ki,kpi,n)
      p1r=p1r*v6(ki,kpi,n)
      go to 459
  457 pr=pr*u7(ki,kpi,n)
      p1r=p1r*v7(ki,kpi,n)
      go to 459
  458 pr=pr*u8(ki,kpi,n)
      p1r=p1r*v8(ki,kpi,n)
  459 if (nij.gt.1) go to 460
      p1r=p1r*sqrt3h
      go to 1468
  460 if (irho.ne.0) go to 461
      go to (461,462,463,464,465,466,467,468) j
  461 pr=pr*u1(kj,kpj,n)
      p1r=p1r*v1(kj,kpj,n)
      go to 1468
  462 pr=pr*u2(kj,kpj,n)
      p1r=p1r*v2(kj,kpj,n)
      go to 1468
  463 pr=pr*u3(kj,kpj,n)
      p1r=p1r*v3(kj,kpj,n)
      go to 1468
  464 pr=pr*u4(kj,kpj,n)
      p1r=p1r*v4(kj,kpj,n)
      go to 1468
  465 pr=pr*u5(kj,kpj,n)
      p1r=p1r*v5(kj,kpj,n)
      go to 1468
  466 pr=pr*u6(kj,kpj,n)
      p1r=p1r*v6(kj,kpj,n)
      go to 1468
  467 pr=pr*u7(kj,kpj,n)
      p1r=p1r*v7(kj,kpj,n)
      go to 1468
  468 pr=pr*u8(kj,kpj,n)
      p1r=p1r*v8(kj,kpj,n)
 1468 b=(2.0*r+1.0)*s6j(fllrho,fllrhop,r, fllsig,fllsigp,tk)
      if (mod(r,two).gt.zero) b=-b
      rsum=rsum+b*(pr+p1r)
  469 continue
      rsum=-0.5*rsum
c
      return
      end
