C UNIX-IDL - SCCS info: Module @(#)cafcdolp.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C-----------------------------------------------------------------------
C
C  VERSION : 1.5
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------

      subroutine cafcdolp(l,k)
c
c          8-fold do-loop,
c               moved from calcfc because of VAX-compiler limitations
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks),mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/dumlc4(kmx**2-kcc*8),cc(kcc,8)
      dimension nparj(kc,kc)
      equivalence (dumlc4,nparj)
      common/c3lcm/mn1,mx1,mn2,mx2,mn3,mx3,mn4,mx4,mn5,mx5,
     1  mn6,mx6,mn7,mx7,mn8,mx8,sj2,sj3,sj4,sj5,sj6,sj7,sj8
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      dimension pc(kiser),v1(kls1,kls1,9),p5(kiser),ip5(kiser)
      equivalence (ct4,pc,v1),(cc,p5,ip5)
c
c
c     ks=8
c     kc=50
c
      do 325 m1=mn1,mx1
      do 325 m2=mn2,mx2
      do 325 m3=mn3,mx3
      do 325 m4=mn4,mx4
      do 325 m5=mn5,mx5
      do 325 m6=mn6,mx6
      do 325 m7=mn7,mx7
      do 325 m8=mn8,mx8
      l=l+1
      if (l.gt.kmx) go to 325
      nsjk(k)=nsjk(k)+1
      go to (324,323,322,321,318,317,316,315) nosubc
  315 scrj(l,8)=sj8
      fj(l,8)=pj(m8,8)
      nalsj(l,8)=nalsjp(m8,8)
  316 scrj(l,7)=sj7
      fj(l,7)=pj(m7,7)
      nalsj(l,7)=nalsjp(m7,7)
  317 scrj(l,6)=sj6
      fj(l,6)=pj(m6,6)
      nalsj(l,6)=nalsjp(m6,6)
  318 scrj(l,5)=sj5
      fj(l,5)=pj(m5,5)
      nalsj(l,5)=nalsjp(m5,5)
  321 scrj(l,4)=sj4
      fj(l,4)=pj(m4,4)
      nalsj(l,4)=nalsjp(m4,4)
  322 scrj(l,3)=sj3
      fj(l,3)=pj(m3,3)
      nalsj(l,3)=nalsjp(m3,3)
  323 scrj(l,2)=sj2
      fj(l,2)=pj(m2,2)
      nalsj(l,2)=nalsjp(m2,2)
  324 scrj(l,1)=pj(m1,1)
      fj(l,1)=pj(m1,1)
      nalsj(l,1)=nalsjp(m1,1)
  325 continue
      return
      end
