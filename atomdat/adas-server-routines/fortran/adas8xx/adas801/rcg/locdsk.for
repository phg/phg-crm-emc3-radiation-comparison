C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/locdsk.for,v 1.2 2004/07/06 14:13:21 whitefor Exp $ Date $Date: 2004/07/06 14:13:21 $
C
      subroutine locdsk(idsk,flldes,nides)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
c
      rewind idsk
  100 read (idsk,end=250) nid,flld,not,nlt,norcd
      if (flld.eq.flldes.and.nid.eq.nides) go to 300
      do 200 nn=1,norcd
  200 read (idsk)
      go to 100
  250 l=flldes
      write (iw,25) lsymb(l+1),nides,idsk
      if (iw6.lt.0) write (6,25) lsymb(l+1),nides,idsk
   25 format (' *****data for  ',a1,i2,'  not present on disk unit ',i2)
      stop 'Required cfp deck not present in calculation of disks 72-74'
  300 return
      end
