C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/wndist.for,v 1.2 2004/07/06 15:28:59 whitefor Exp $ Date $Date: 2004/07/06 15:28:59 $
C
      subroutine wndist(ii)
c
c          Plot wavenumber distribution on microfilm (obsolete)
c               (special version for gaussian smear)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c3b/t(klam),tp(klam),s2(klam),soabss(klam),
     1  aa(kmx),aap(kmx),gaa(klam),gaap(klam),brnch(klam),
     2  icut(66),sgfcut(66),sfcut(66),didgf(66),dsgfdgf(66),title(20),
     3  ficut(66),fnucut(66),fnuplt(82),dum(66),fiplt(82),sgfplt(82),
     4  ncser(klam),aadum(kmx),aapt(kmx)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c6/emin,emax,sumwt,flbdn,flbdx,fnunp,fnuxp,nlev,iewr
      common/c3lc1/d(kmx,kmx)
      common/c3lc2/v(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/cta(kmx,kmx)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),xx(kmx)
      common/spec/fjt(klam),fjtp(klam),ncserp(klam),gaat(klam),
     1  gaapt(klam)
c
      dimension x(klam),fnu(klam),flam(klam), fnut(klam)
      equivalence (cta,x,fnu),(ct4,flam), (d,fnut)
      dimension fnus(100),gamma(7)
      equivalence (t,fnus)
      dimension eplt(klam),sgf(klam)
      equivalence (tp,eplt),(soabss,sgf)
      common/c7/g1,sumgaa,sumgar,sumgaat
      character*10 parnam(kpr,2)
      character iel*8
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 elem(3,kc,2),elem1(3)
      common/char2/elem,elem1
      data igf,idi/2hgf,2hdi/
c
c          dele in keV,  fnun and fnunp in cm-1
c
      dele=delekev
      if (dele.le.0.0) dele=0.0005
  100 del=0.0005*(1.002*fnuxp-fnunp)
      fnun=fnunp-del
      fnux=fnuxp+del
  105 delnu=8065470.0*dele/uenrgy
      ieras=(fnux-fnun)/delnu
      np2=ieras+3
      if (np2.le.klam/2) go to 110
      if (ieras.gt.klam-5) write (2,10) ((elem(i,1,j),i=1,3),j=1,2),
     1  fnuxp,fnunp,dele
   10 format (///5h*****,3a6,3h - ,3a6,2f15.1,6h dele=,f7.5,
     1  10h too small///)
      dele=2.0*dele
      go to 105
  110 imin=fnunp/delnu
      fimin=imin
      eplt(1)=fimin*dele
      do 150 j=1,np2
      eplt(j+1)=eplt(j)+dele
  150 sgf(j)=0.0
c
      rewind 20
      itot=0
      gftot=0.0
      sbr=0.0
  200 read (20) imax,psq, (fnu(i),s2(i), brnch(i), i=1,imax)
      if (psq.lt.0.0) go to 300
      do 205 i=1,imax
  205 sbr=sbr+brnch(i)
      do 230 i=1,imax
      eras1=fnu(i)*uenrgy
      gf=psq*s2(i)*eras1
      if (ii.eq.2.and.igen.lt.5) gf=gf*((1.0e-8*eras1)**2)
      if (sbr.gt.0.0) gf=brnch(i)
      j=fnu(i)/delnu+1.5
      j=j-imin
      if (j.lt.1.or.j.gt.np2) write (2,20) np2,j, fnu(i),delnu
   20 format (10x,2i10,2f15.1)
      sgf(j)=sgf(j)+gf
      itot=itot+1
  230 gftot=gftot+gf
      go to 200
c
  300 do 310 j=1,np2
      sgf(j)=sgf(j)/(gftot+1.0e-10)
  310 continue
      iii=1
      gamma(1)=0.0
      imx=np2
      id=igf
      if (sbr.gt.0.0) id=idi
c     encode (8,91,iel) elem(1,1,1),id
      write (iel,91) elem(1,1,1),id
   91 format (a6,a2)
      brfac=0.0
      if (sbr.gt.0.0) brfac=gftot/sumgaa
      write (iw,92) (elem(j,1,1),j=1,3), id,gamma(iii),g1,gftot,
     1  eplt(1),dele,imx,brfac
   92 format (//1h0,3a6,2x,a2,2f5.0,10x,1p,e10.3,0p,2f10.6,i10,10x,
     1  6hbrfac=,f10.6)
      write (11,94) (elem(j,1,1),j=1,3), id,gamma(iii),g1,gftot,
     1  eplt(1),dele,imx
   94 format (3a6,a2,2f5.0,10x,1p,e10.3,0p,2f10.6,i10)
      jx=0
      i=0
  865 jn=jx+1
      jx=min0(jn+6,imx)
      i=i+1
  866 write (iw,93) iel,i, (sgf(j), j=jn,jx)
   93 format (1x,a8,i4,7f10.7)
      write (11,95) iel,i, (sgf(j), j=jn,jx)
   95 format (a8,i2,7f10.7)
      if (jx.lt.imx) go to 865
  868 continue
c
c
      if (sbr.eq.0.0) go to 900
      nosubc=nsconf(1,2)
      gj=1.0
      do 880 i=1,nosubc
      w=nijk(i,1,2)
      fll=fllijk(i,1,2)
      wf=4.0*fll+2.0
  880 gj=gj*fctrl(wf)/(fctrl(w)*fctrl(wf-w))
      aat=sumgaa/gj
      art=sumgar/gj
      papp=aat*art/(aat+art)
      pdet=gftot/gj
      bapp=aat/(aat+art)
      bdet=gftot/(sumgar+1.0e-10)
      rr=bdet/bapp
      aateff=art/(1.0/bdet-1.0)
      write (iw,88) ((elem(i,1,j),i=1,3), j=1,2), gj,aat,art,papp,pdet,
     1  bapp,bdet,rr,aateff
      write (3,88) ((elem(i,1,j),i=1,3), j=1,2), gj,aat,art,papp,pdet,
     1  bapp,bdet,rr,aateff
   88 format (1h0,44x,2hgj,5x,3haat,8x,3hart,5x,9haa*ar/sum,4x,
     1  6hp(det),4x,6hb(app),4x,6hb(det),4x,7hdet/app,3x,8haat(eff)/
     2  1x,3a6,3h---,2x,3a6,f5.0,1p,4e11.3,0p,3f10.7,1p,e11.3)
      if (sumgaa.ge.sumgaat) go to 883
      aatt=sumgaat/gj
      papp=aat*art/(aatt+art)
      bapp=papp/art
      rr=bdet/bapp
      write (iw,88) ((elem(i,1,j),i=1,3), j=1,2), gj,aatt,art,papp,pdet,
     1  bapp,bdet,rr,aateff
      write (3,88) ((elem(i,1,j),i=1,3), j=1,2), gj,aatt,art,papp,pdet,
     1  bapp,bdet,rr,aateff
  883 jx=max0(nsconf(2,1),nsconf(2,2))
      if (jx.lt.2) go to 900
      do 885 jj=2,jx
      write (iw,89) ((elem(i,jj,j),i=1,3), j=1,2)
      write (3,89) ((elem(i,jj,j),i=1,3), j=1,2)
   89 format (1x,3a6,3h---,2x,3a6)
  885 continue
  900 return
      end
