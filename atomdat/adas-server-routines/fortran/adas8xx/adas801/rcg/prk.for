C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/prk.for,v 1.2 2004/07/06 14:37:22 whitefor Exp $ Date $Date: 2004/07/06 14:37:22 $
C
c     overlay(g9,5,0)
      subroutine prk
c
c          Calc. preliminary configuration-interaction matrix elements
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kcc*8),cc(kcc,8)
      dimension nparj(kc,kc)
      equivalence (dumlc4,nparj)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      character*10 parnam(kpr,2),aa
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      dimension cfp1(klsp1,klsp1),nopccc(klc1)
      equivalence (cfp1,cfgp1),(dumlc3,nopccc)
      dimension intest(80)
c
c
c     ks=8
c     kcc=2500
c     klc1=78**2-(17**2)*9
c     kpc=2000
c     nktotx=locf(ccc(1))-locf(cijr(1))
c          nktotx=dimension of cijr and ccc
c
      nktotx=20
      npcx=kpc
      nccx=klc1
c
      nparjo=npar
      j1x=nsconf(2,kk)
      if (j1x.le.1) go to 900
      do 400 i=1,80
  400 intest(i)=0
      if (nsconf(3,kk).ne.0) go to 500
      read (ir,50) (intest(i),i=1,80)
   50 format (80i1)
c
c
c          Calculate rk(i,j)
c
  500 j1xm1=j1x-1
      noccx=0
      jtest=0
      do 799 j1a=1,j1xm1
      j1ap1=j1a+1
      do 799 j1b=j1ap1,j1x
      jtest=jtest+1
      nparj(j1a,j1b)=0
      if (nsconf(3,kk).eq.0.and.intest(jtest).eq.0) go to 799
      if (nsconf(3,kk).gt.0.and.j1b.gt.j1a+nsconf(3,kk)) go to 799
      if (nsconf(3,kk).eq.-9.and.(j1a.eq.1.or.j1b.eq.j1a+1)) go to 501
      if (nsconf(3,kk).eq.-9) go to 799
      if (nsconf(3,kk).eq.-8) go to 501
      if (nsconf(3,kk).lt.0.and.j1a.gt.max0(-nsconf(3,kk),ipct))
     1  go to 799
  501 do 798 irho=1,nosubc
      irs(1)=irho
      do 798 isig=irho,nosubc
      irs(2)=isig
      do 798 irhop=1,nosubc
      irs(3)=irhop
      do 797 isigp=irhop,nosubc
      irs(4)=isigp
      fllsig=fllijk(isig,j1a,kk)
      fllsigp=fllijk(isigp,j1b,kk)
c              Check whether irho...isigp is a possible set
c                      of interacting electrons
      if (isig.eq.nosubc.or.isigp.eq.nosubc) go to 1501
      if (nijk(nosubc,j1a,kk).eq.1.and.nijk(nosubc,j1b,kk).eq.1.and.
     1  fllijk(nosubc,j1a,kk).ne.fllijk(nosubc,j1b,kk)) go to 797
 1501 if (irho.ne.irhop.or.isig.ne.isigp) go to 503
      nn=0
      do 502 i=isig,nosubc
  502 nn=nn+nijk(i,j1a,kk)+nijk(i,j1b,kk)
      if (nn.gt.2) go to 797
  503 do 504 i=1,nosubc
      nijkp(i,1)=nijk(i,j1a,kk)
  504 nijkp(i,2)=nijk(i,j1b,kk)
      nijkp(irho,1)=nijkp(irho,1)-1
      nijkp(isig,1)=nijkp(isig,1)-1
      nijkp(irhop,2)=nijkp(irhop,2)-1
      nijkp(isigp,2)=nijkp(isigp,2)-1
      do 505 i=1,nosubc
      if (nijkp(i,1).ne.nijkp(i,2)) go to 797
  505 if (nijkp(i,1).lt.0) go to 797
c
      kdmin=max(abs(fll(irho)-fll(irhop)),abs(fllsig-fllsigp))
      kdmax=min(abs(fll(irho)+fll(irhop)),abs(fllsig+fllsigp))
      nkd=(kdmax-kdmin)/2+1
      kemin=max(abs(fll(irho)-fllsigp),abs(fllsig-fll(irhop)))
      nke=nkd
      if (irho.eq.isig.or.irhop.eq.isigp) nke=0
      if (nkd.le.0) go to 797
      irp1mx=kdmax-kdmin+1
      nktot=nkd+nke
      if (nktot.le.nktotx) go to 506
      write (iw,51) nktot,nktotx
   51 format (///12h0*****nktot=,i5,29h is greater than dim of cijr=,i5,
     1  6h *****///)
      stop '(prk 51)'
  506 fk=kdmin-2
      do 507 nn=1,nkd
      fk=fk+2.0
  507 cijr(nn)=cijkf(fll(irho),fll(irhop),fk)
     1  *cijkf(fllsig,fllsigp,fk)
      if (nke.le.0) go to 509
      fk=kemin-2
      n1=nkd+1
      do 508 nn=n1,nktot
      fk=fk+2.0
  508 cijr(nn)=cijkf(fll(irho),fllsigp,fk)
     1  *cijkf(fll(irhop),fllsig,fk)
  509 continue
c
      do 511 i=1,4
  511 pc(i)=irs(i)
      call sort2(4,0,pc,idum,dum,idum,idum,dum,dum,dum,dum,dum,
     1  dum,dum,dum)
      in=pc(1)
      ix=pc(2)
      jn=pc(3)
      jx=pc(4)
c
c          Determine class of configuration interaction
c
      i=1
      j=3
      if (irho.le.irhop) go to 521
      i=3
      j=1
  521 neqp1=1
      do 522 k=1,3
      kp1=k+1
      do 522 l=kp1,4
  522 if (irs(k).eq.irs(l)) neqp1=neqp1+1
      iclass=0
      go to (526,525,524,523), neqp1
  523 iclass=1
      go to 527
  524 iclass=2
      if (irho.eq.irhop) iclass=11
      if (iclass.eq.11.and.fllsig.ne.fllsigp) iclass=6
      go to 527
  525 if (irs(i).eq.irs(i+1).and.irs(i).lt.irs(j)) iclass=3
      if (irs(j).eq.irs(j+1).and.irs(j).lt.irs(i+1)) iclass=4
      if (irs(j).eq.irs(j+1).and.irs(j).gt.irs(i+1)) iclass=5
      if (irs(i).eq.irs(j)) iclass=6
      if (irs(i+1).eq.irs(j)) iclass=7
      if (irs(i+1).eq.irs(j+1)) iclass=8
      if (iclass.eq.8.and.fllsig.ne.fllsigp) iclass=10
      go to 527
  526 if (irs(i+1).lt.irs(j)) iclass=9
      if (irs(j).lt.irs(i+1)) iclass=10
c
c          Determine whether configs are to be transposed (itrnsp=2)
c
  527 itrnsp=1
      if (iclass.eq.2.and.irho.lt.irhop) go to 530
      if (irhop.eq.isigp) go to 528
      if (irho.eq.isig) go to 530
      if (irho.lt.irhop) go to 530
      if (irho.eq.irhop.and.isig.le.isigp) go to 530
  528 itrnsp=2
c
c          Read reduced matrix elements U,V from disk 73
c
  530 j=0
      if (isig.eq.irhop.or.isig.eq.isigp) j=isig
      if (irho.eq.irhop.or.irho.eq.isigp) j=irho
      if (j.eq.0) go to 540
      nj=min0(nijk(j,j1a,kk),nijk(j,j1b,kk))
      nii=nj
      nij=nj
      if (nj.le.1) go to 540
      nfull=4.0*fll(j)+2.0
      if (nj.eq.nfull) go to 797
      call locdsk(id3,fll(j),nj)
      nor=norcd/2
      do 533 k=1,nor
  533 read (id3) n, ((u1(ii,jj,k),ii=1,nlt), jj=1,nlt)
      do 534 k=1,nor
  534 read (id3) n, ((v1(ii,jj,k),ii=1,nlt), jj=1,nlt)
c
c          Read cfp or cfgp from disk 72
c
  540 do 565 i=1,nosubc
      ndel(i)=iabs(nijk(i,j1a,kk)-nijk(i,j1b,kk))
      if (ndel(i).eq.0.or.ndel(i).gt.2) go to 565
      ifl(i)=max0(nijk(i,j1a,kk),nijk(i,j1b,kk))
      if (ndel(i).eq.1.and.ifl(i).le.2) go to 565
      call locdsk(id2,fll(i),ifl(i))
      read (id2) (ia,ia,ia,a,a, m=1,nlt), nlp,nlgp,nocfp
      ia=ia
      if (nocfp.eq.0) go to 555
      if (ifl(i).gt.2.and.ndel(i).eq.1) go to 545
      read (id2)
      go to 554
  545 if (i.gt.1) go to 550
      read (id2) ((cfp1(m,n), m=1,nlt), n=1,nlp)
      go to 554
  550 read (id2) ((cfp2(m,n,i-1), m=1,nlt), n=1,nlp)
  554 read (id2)
  555 if (ndel(i).ne.2) go to 565
      read (id2)
      n=1.0+min(fll(irho)+fllsig, fll(irhop)+fllsigp)
      if (i.gt.in) go to 561
      do 560 k=1,n
  560 read (id2) ((cfgp1(m,l,k), m=1,nlt), l=1,nlgp)
      go to 565
  561 do 562 k=1,n
  562 read (id2) ((cfgp2(m,l,k), m=1,nlt), l=1,nlgp)
  565 continue
c
      tc0=1.0
      ixx=0
      do 570 i=irho,isig
  570 if (i.gt.irho) ixx=ixx+nijk(i,j1a,kk)
      do 571 i=irhop,isigp
  571 if (i.gt.irhop) ixx=ixx+nijk(i,j1b,kk)
      if (irho.eq.isig) ixx=ixx+1
      if (irhop.eq.isigp) ixx=ixx+1
      ixx=mod(ixx,2)
      if (ixx.gt.0) tc0=-tc0
      n=nijk(irho,j1a,kk)
      n1=n*nijk(isig,j1a,kk)
      if (irho.eq.isig) n1=(n1*(n-1))/n
      n=nijk(irhop,j1b,kk)
      n1=n1*n*nijk(isigp,j1b,kk)
      if (irhop.eq.isigp) n1=(n1*(n-1))/n
      if (j.gt.0) n1=n1/(nj*nj)
      eras=n1
      tc0=tc0*sqrt(eras)
      if (irho.eq.isig.and.irhop.eq.isigp) tc0=0.5*tc0
c
c          Calculate matrix elements
c
  600 ln=ntottj(j1a)+1
      lx=ntottj(j1a+1)
      lpn=ntottj(j1b)+1
      lpx=ntottj(j1b+1)
      nopc=0
      do 700 lp=lpn,lpx
      do 700 l=ln,lx
      if (pscrl(l,nosubc).ne.pscrl(lp,nosubc)) go to 700
      if (pscrs(l,nosubc).ne.pscrs(lp,nosubc)) go to 700
      nopc=nopc+1
      tc=0.0
      do 610 i=1,nosubc
      if (i.lt.in) go to 605
      if (i.lt.jx) go to 606
  605 if (pscrl(l,i).ne.pscrl(lp,i)) go to 690
      if (pscrs(l,i).ne.pscrs(lp,i)) go to 690
  606 do 607 j=1,4
  607 if (i.eq.irs(j)) go to 610
      if (nalsp(l,i)-notsj1(i,j1a).ne.nalsp(lp,i)-notsj1(i,j1b))goto 690
  610 continue
c          Multiply by cfp
      tc=tc0
      do 630 i=1,nosubc
      if (ndel(i).ne.1) go to 630
      if (ifl(i).le.2) go to 630
      if (iclass.eq.1.and.i.eq.ix) go to 630
      l1=l
      l2=lp
      j11=j1a
      j12=j1b
      if (nijk(i,j1a,kk).gt.nijk(i,j1b,kk)) go to 622
      l1=lp
      l2=l
      j11=j1b
      j12=j1a
  622 n1=nalsp(l1,i)-notsj1(i,j11)
      n2=nalsp(l2,i)-notsj1(i,j12)
      if (i.gt.1) go to 627
      tc=tc*cfp1(n1,n2)
      go to 630
  627 tc=tc*cfp2(n1,n2,i-1)
  630 continue
c
      if (tc.eq.0.0) go to 690
c
      l1=l
      l2=lp
      j11=j1a
      j12=j1b
      if (irho.lt.irhop) go to 635
      l1=lp
      l2=l
      j11=j1b
      j12=j1a
  635 if (in.eq.1.or.in.eq.ix) go to 640
      if (nijk(in,j11,kk).le.1) go to 640
      m=in
      n1=nalsp(l1,m)
      n2=nalsp(l2,m)
      tc=tc*recpsh(pscrl(l1,m-1),fl(n2,m),pscrl(l2,m),fll(m),
     1    pscrl(l1,m),fl(n1,m)) *recpsh(pscrs(l1,m-1),s(n2,m),
     2  pscrs(l2,m),half,pscrs(l1,m),s(n1,m))
      if (tc.eq.0.0) go to 690
c
  640 mn=in+1
      mx=ix-1
      if (mn.gt.mx) go to 650
      do 645 m=mn,mx
      n1=nalsp(l1,m)
      n2=nalsp(l2,m)
      tc=tc*recpex(pscrl(l2,m-1),fll(in),pscrl(l1,m-1),fl(n1,m),
     1  pscrl(l1,m),pscrl(l2,m)) *recpex(pscrs(l2,m-1),half,
     2  pscrs(l1,m-1),s(n1,m),pscrs(l1,m),pscrs(l2,m))
  645 continue
      if (tc.eq.0.0) go to 690
c
  650 l1=l
      l2=lp
      j11=j1a
      j12=j1b
      flljx=fllsig
      if (isig.gt.isigp) go to 655
      l1=lp
      l2=l
      j11=j1b
      j12=j1a
      flljx=fllsigp
  655 if (jn.ge.jx) go to 660
      if (nijk(jx,j11,kk).le.1) go to 660
      m=jx
      n1=nalsp(l1,m)
      n2=nalsp(l2,m)
      tc=tc*recpjp(pscrl(l1,m-1),flljx,pscrl(l2,m-1),fl(n2,m),
     1  pscrl(l1,m),fl(n1,m)) *recpjp(pscrs(l1,m-1),half,
     2  pscrs(l2,m-1),s(n2,m),pscrs(l1,m),s(n1,m))
      if (tc.eq.0.0) go to 690
c
  660 mn=jn+1
      mx=jx-1
      if (mn.gt.mx) go to 670
      flljx=fllijk(jx,j11,kk)
      do 665 m=mn,mx
      n1=nalsp(l1,m)
      n2=nalsp(l2,m)
      tc=tc*recpex(pscrl(l1,m-1),fl(n1,m),pscrl(l1,m),flljx,
     1  pscrl(l2,m),pscrl(l2,m-1)) *recpex(pscrs(l1,m-1),s(n1,m),
     2  pscrs(l1,m),half,pscrs(l2,m),pscrs(l2,m-1))
  665 continue
  670 continue
c
  690 pc(nopc)=tc
  700 continue
      if (nopc.le.npcx) go to 701
      write (iw,70) nopc,npcx
   70 format (///11h0*****nopc=,i5,27h is greater than dim of pc=,i5,
     1  6h *****///)
      stop '(prk 70)'
c
  701 fllrho=fll(irho)
      fllrhop=fll(irhop)
      if (itrnsp.ne.2) go to 702
      eras=fllrho
      fllrho=fllrhop
      fllrhop=eras
      eras=fllsig
      fllsig=fllsigp
      fllsigp=eras
  702 if (iclass.ne.7) go to 705
      kkk=kdmin
      kdmin=kemin
      kemin=kkk
      eras=fllrho
      fllrho=fllsig
      fllsig=eras
      if (mod(fll(irhop)-fll(irho),two).eq.zero) go to 705
      do 703 nn=1,nkd
  703 cijr(nn)=-cijr(nn)
  705 nopc=0
      nocc=0
      do 780 lp=lpn,lpx
      do 780 l=ln,lx
      if (pscrl(l,nosubc).ne.pscrl(lp,nosubc)) go to 780
      if (pscrs(l,nosubc).ne.pscrs(lp,nosubc)) go to 780
      nopc=nopc+1
      if (pc(nopc).eq.0.0) go to 780
      do 710 nn=1,nktot
  710 ccc(nn)=0.0
      lci=l
      lcip=lp
      if (itrnsp.eq.2) go to 722
      l1=l
      l2=lp
      j11=j1a
      j12=j1b
      go to 730
  722 l1=lp
      l2=l
      j11=j1b
      j12=j1a
  730 go to (731,732,733,734,735,736,737,738,739,740,741), iclass
  731 call class1
      go to 750
  732 call class2
      go to 750
  733 call class3
      go to 750
  734 call class4
      go to 750
  735 call class5
      go to 750
  736 call class6
      go to 750
  737 call class7
      go to 750
  738 call class8
      go to 750
  739 call class9
      go to 750
  740 call clas10
      go to 750
  741 call clas11
  750 continue
c
      nocc=nocc+1
      if (nocc.le.nccx) go to 755
      write (iw,75) nocc,nccx,j1a,j1b,irho,isig,irhop,isigp,ln,l,lx,
     1  lpn,lp,lpx
   75 format (///11h0*****nocc=,i5,31h is greater than dim of nopccc=,
     1  i5,6h *****,10x,2i1,i2,3i1,5x,3i5,5x,3i5///)
      stop '(prk 75)'
  755 nopccc(nocc)=nopc
      do 760 nn=1,nktot
  760 cc(nocc,nn)=pc(nopc)*cijr(nn)*ccc(nn)
  780 continue
      noccx=max(noccx,nocc)
c
      if (iclass.ne.7) go to 781
      kkk=kdmin
      kdmin=kemin
      kemin=kkk
  781 do 790 nn=1,nktot
      cave=0.0
      if (iclass.ne.11) go to 782
      if (nn.le.nkd) go to 782
      a=nijk(irho,j1a,kk)
      cave=0.5*a*cijr(nn)/((2.0*fll(irho)+1.0)*(2.0*fllsig+1.0))
  782 nopc=0
      m=1
      do 784 lp=lpn,lpx
      do 783 l=ln,lx
      if (pscrl(l,nosubc).ne.pscrl(lp,nosubc)) go to 783
      if (pscrs(l,nosubc).ne.pscrs(lp,nosubc)) go to 783
      nopc=nopc+1
      pc(nopc)=0.0
      if (m.gt.nocc) go to 783
      if (nopccc(m).ne.nopc) go to 783
      pc(nopc)=cc(m,nn)
      if ((l-ln).eq.(lp-lpn)) pc(nopc)=pc(nopc)+cave
      m=m+1
  783 continue
  784 continue
      npar=npar+1
      if (nn-nkd) 785,785,786
  785 k=kdmin+2*(nn-1)
      write (aa,77) j1a,j1b,k,irho,isig,irhop,isigp
   77 format (2i2,i1,1hd,4i1)
      go to 787
  786 k=kemin+2*(nn-nkd-1)
      write (aa,78) j1a,j1b,k,irho,isig,irhop,isigp
   78 format (2i2,i1,1he,4i1)
  787 parnam(npar,kk)=aa
      if (icpc.le.2) go to 789
      write (iw,80) aa,cave,iclass
   80 format (1h0,a10,5x,5hcave=,f12.7,15x,16hconf inter class,i3/)
      write (iw,81) (muls1(lp),lhs1(lp),muls4(lp),lhs4(lp),
     1  lp=lpn,lpx)
   81 format (10x,12(3x,1h(,i1,a1,1h),i2,a1)/
     1        12x,12(3x,1h(,i1,a1,1h),i2,a1)/
     2        12x,12(3x,1h(,i1,a1,1h),i2,a1)/
     3        12x,12(3x,1h(,i1,a1,1h),i2,a1))
      nt=0
      k1=1
      do 1788 k=1,ndifft
      nt=nt+ntrmk(k)
      if (nt.eq.ln-1) k1=k+1
      if (nt.eq.lx) k2=k
      if (nt.eq.lpn-1) k1p=k+1
      if (nt.eq.lpx) k2p=k
 1788 continue
      npc2=0
      l2=ln-1
      do 4788 k=k1,k2
      lxn=ntrmk(k)
      l1=l2+1
      l2=l2+lxn
      l2p=lpn-1
c---------------------
c ADAS Conversion Changes 
c Changed "do 320 kp=1,2" to "do 320 kkp=1,2"
c (kp is a parameter in the order stages)
c ADW - 12/12/01
c---------------------
      do 2788 kkp=k1p,k2p
      l2p=l2p+ntrmk(kkp)
      if (pscrl(l2,nosubc).ne.pscrl(l2p,nosubc)) go to 2788
      if (pscrs(l2,nosubc).ne.pscrs(l2p,nosubc)) go to 2788
      go to 3788
 2788 continue
      go to 4788
 3788 npc1=npc2
      npc2=npc2+ntrmk(k)*ntrmk(kkp)
      do 788 l=l1,l2
      npc1=npc1+1
  788 write (iw,82) muls1(l),lhs1(l),muls4(l),lhs4(l),
     1  (pc(i), i=npc1,npc2,lxn)
   82 format (2h (,i1,a1,1h),i2,a1,2x,12f10.5/(12x,12f10.5))
 4788 continue
      if (npc2.ne.nopc) write (iw,83) npc2,nopc
   83 format (///16h0*****npc2,nopc=,2i8,6h *****///)
  789 if (icpc.eq.0) go to 1789
      call clock(time)
      delt=time-time0
      write(iw,84) aa,nopc,cave,iclass,delt,(llijk(l,j1a,kk),
     1  nijk(l,j1a,kk),l=1,ks), (llijk(l,j1b,kk),nijk(l,j1b,kk),l=1,ks)
   84 format (/1h ,a10,4x,3hpc(,i5,1h),4x,5hcave=,f12.7,5x,7hiclass=,i2,
     1  6x,5hdelt=,f6.3,4h min,7x, 8(a1,i2,3x)/87x,8(a1,i2,3x))
 1789 kpar=-2
      write (20) npar,kpar,k,j1a,j1b,aa,cave,j,nopc, (pc(i), i=1,nopc)
  790 continue
c
  797 continue
  798 continue
      nparj(j1a,j1b)=npar-nparjo
      nparjo=npar
  799 continue
c
      write (iw,86) noccx
   86 format (//7h0noccx=,i5)
      write (iw,87) npar
   87 format (///1h0,8x,5hnpar=,i4//9x,16hnparj-----------/)
      do 890 i=1,j1x
  890 write (iw,89) i, (nparj(i,j), j=i,j1x)
   89 format (4h j1=,i2,2x,40i3/(8x,40i3))
c
  900 continue
      return
      end
