C UNIX-IDL - SCCS info: Module @(#)mupole.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
c     overlay(g9,7,0)
      subroutine mupole
c
c          Compute magnetic-dipole (ii=0), electric-dipole (ii=1),
c               or electric-quadrupole (ii=2) matrices
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/magee/nmult,ndum,nexp(kmx),sums(kmult),
     1  ials(kmult,ks,2),sl(kmult,ks,2),ss(kmult,ks,2),nexped(kmult,2),
     2  etrm(kmult,2)
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2a/sj8k(2),notsjp(ks,100),ncfgp(kmx),nalsp(kmx,ks)
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks,2),
     2  scrs(kmx,ks,2), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/dumlc4(klc42)
      common/c3lcd/dumlc5
      common/mup/fl(kmx,ks,2),s(kmx,ks,2),cfp2(kls2,kls2),
     1  cfp1(klsp1,klsp1)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),xxx(kmx)
      dimension tmxp(kmx,kmx),vect(kmx,kmx)
      equivalence (dumlc4,tmxp)
      dimension u1(klsp1,klsp1),nparj(kc,kc)
      equivalence (cfp1,u1),(dumlc4,vect),(dumlc4(klc4),nparj)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      character*8 fidmult(kmult,2)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      common/char1/fidmult
c
c
      itrans=0
      irmn1=irmn+1
      irmx1=irmx+1
      rmx=irmx
      ii=iarg1
      il1=iarg2
      il2=iarg3
      ix=ii
      i1=il1-ila+1
      i2=il2-ila+1
      do 990 ir1=irmn1,irmx1,2
      r=ir1-1
      sums20=0.0
      sums21=0.0
      rewind il1
      read (il1) notsj1,nscrj8,sj8mn,sj8mx,npar,nparj,parnam,
     1  notsx, ((fl(l,i,1),s(l,i,1), l=1,notsx), i=1,nosubc)
      sj8k(i1)=sj8mn-1.0
      rewind il2
      read (il2) notsjp,nscrjp,sj8pmn,sj8pmx,npar,nparj,parnam,
     1  notsx, ((fl(l,i,2),s(l,i,2), l=1,notsx), i=1,nosubc)
      nscrjp=nscrjp
      sj8k(i2)=sj8pmn-1.0
      j1a=nsconf(2,i1)
      j1b=nsconf(2,i2)
      do 105 i=1,j1a
      do 105 j=1,j1b
  105 sopi2(i,j)=0.0
      nmult=0
      do 106 i=1,kmult
  106 sums(i)=0.0
c
      nsc=nosubc
      sjd=sj8k(i1)
  110 sjd=sjd+1.0
      if (sjd.gt.sj8mx) go to 950
  120 n=sj8k(i1)-sjd
      if (n.lt.0) go to 126
  122 n=n+1
      do 124 i=1,n
  124 backspace il1
  126 read (il1) sj8k(i1),nls,nls1, ((tmx(l,lp),l=1,nls1), lp=1,nls1),
     1  ((nals(l,i),scrl(l,i,1),scrs(l,i,1),i=1,nsc),
     2  ncfg(l),fidls(l,1),fidjj(l,1), l=1,nls1)
      if (sj8k(i1).ne.sjd) go to 120
      y=100.0
      do 127 i=irmn1,irmx1,2
      r1=i-1
      x=abs(sjd-r1)
      if (x.le.y) y=x
  127 continue
      if (ii.eq.0) y=abs(sjd-1.0)
  128 x=0.0
      if (i1.eq.i2) x=sjd
      sjpd=max(sj8pmn,y,x)
      go to 135
  130 sjpd=sjpd+1.0
      if (sjd.eq.0.0) sjpd=sjpd+1.0
  135 if (sjpd.gt.min(sjd+rmx,sj8pmx)) go to 110
  140 n=sj8k(i2)-sjpd
      if (n.lt.0) go to 146
  142 n=n+1
      do 144 i=1,n
  144 backspace il2
  146 read (il2) sj8k(i2),nlsp,nls1, ((tmxp(l,lp),l=1,nls1), lp=1,nls1),
     1  ((nalsp(l,i),scrl(l,i,2),scrs(l,i,2),i=1,nsc),
     2  ncfgp(l),fidls(l,2),fidjj(l,2), l=1,nls1)
      if (sj8k(i2).ne.sjpd) go to 140
c
c
  150 nco=0
      ncpo=0
      if (nls.le.0.or.nlsp.le.0) go to 130
      do 879 l=1,nls
      nc=ncfg(l)
      do 878 lp=1,nlsp
      ncp=ncfgp(lp)
      c(l,lp)=0.0
      if (ii.gt.0) go to 300
c
c          Calculate magnetic dipole element
c
  200 if (nc.ne.ncp) go to 878
      do 210 i=1,nosubc
      if (nals(l,i).ne.nalsp(lp,i)) go to 878
      if (scrl(l,i,1).ne.scrl(lp,i,2)) go to 878
  210 if (scrs(l,i,1).ne.scrs(lp,i,2)) go to 878
      a1=scrl(l,nosubc,1)
      a2=scrs(l,nosubc,1)
      tc=1.00232*uncplb(a1,a2,sjd,one,a2,sjpd)
     1  *sqrt(a2*(a2+1.0)*(2.0*a2+1.0))
      if (sjd.eq.sjpd) tc=tc+sqrt(sjd*(sjd+1.0)*(2.0*sjd+1.0))
      go to 700
c
c          Calculate electric multipole element
c
  300 if (nc.eq.nco.and.ncp.eq.ncpo) go to 400
c
c          Determine whether a transition is possible (itrans.ne.0)
c
      itrant=itrans
      it=i
      jt=j
      itrans=0
      do 340 irho=1,nosubc
      do 340 irhop=1,nosubc
      do 302 i=1,nosubc
      if (i.eq.irho.or.i.eq.irhop) go to 302
      if (nijk(i,nc,i1)+nijk(i,ncp,i2).eq.0) go to 302
      if (fllijk(i,nc,i1).ne.fllijk(i,ncp,i2)) go to 340
      if (nijk(i,nc,i1).ne.nijk(i,ncp,i2)) go to 340
  302 continue
      if (irho.ne.irhop) go to 320
      if (il1.ne.il2.or.nc.ne.ncp) go to 305
      do 304 i=irho,nosubc
      ieras=4.0*fllijk(i,nc,i1)+2.0
      nn=nijk(i,nc,i1)
      if ((nn.eq.0.or.nn.eq.ieras).and.i.eq.irho) go to 340
      if (i.eq.irho) go to 304
      if (nn.eq.0.or.nn.eq.ieras) go to 304
      if (fllijk(i,nc,i1).eq.0.0) go to 304
      go to 340
  304 continue
      go to 320
  305 nn=0
      do 310 i=irho,nosubc
  310 nn=nn+nijk(i,nc,i1)+nijk(i,ncp,i2)
      if (nn.gt.2) go to 340
  320 a=fllijk(irho,nc,i1)+fllijk(irhop,ncp,i2)
      if (mod(a+r,two).ne.zero) go to 340
      if (a.lt.r) go to 340
      if (abs(fllijk(irho,nc,i1)-fllijk(irhop,ncp,i2)).gt.r) go to 340
      do 325 i=1,nosubc
      n=nijk(i,nc,i1)
      if (i.eq.irho) n=n-1
      m=nijk(i,ncp,i2)
      if (i.eq.irhop) m=m-1
      if (n.ne.m) go to 340
      if (n.lt.0) go to 340
  325 continue
      go to 350
  340 continue
      itrans=itrant
      i=it
      j=jt
      go to 878
c
c          Calculate theoretical S/P2
c
  350 nco=nc
      ncpo=ncp
      if (sopi2(nc,ncp).ne.0.0) go to 355
      eras=2.0
      do 354 i=1,nosubc
      b=nijk(i,nc,i1)
      b1=nijk(i,ncp,i2)
      if (b.ge.b1) go to 351
      a=4.0*fllijk(i,ncp,i2)+1.0
      go to 354
  351 a=4.0*fllijk(i,nc,i1)+2.0
      if (i.ne.irho) go to 354
      if (i1.ne.i2.or.nc.ne.ncp) go to 353
      eras=eras*b*(a-b)/(a*(a-1.0))
      go to 354
  353 a=a-1.0
      b=b-1.0
  354 eras=eras*fctrl(a)/(fctrl(b)*fctrl(a-b))
      sopi2(nc,ncp)=eras
      if (i1.eq.i2) sopi2(ncp,nc)=eras
c
c          Determine whether configs. are to be interchanged (itrans=-1)
c
  355 i=min0(irho,irhop)
      j=max0(irho,irhop)
      m=nijk(irho,nc,i1)
      n=nijk(irhop,ncp,i2)
      itrans=1
      tc0=1.0
      if (il1.eq.il2.and.nc.eq.ncp) go to 370
      a=m*n
      tc0=sqrt(a)
      if (irho.gt.irhop) n=m
      m=0
      do 360 k=i,j
      if (k.ne.i.and.k.ne.j) m=m+nijk(k,nc,i1)
  360 continue
      if (mod(m+n,2).eq.0) tc0=-tc0
      if (i.ne.irho) itrans=-1
      if (itrans.gt.0) go to 370
      if (mod(abs(sjpd-sjd+r),two).ne.zero) tc0=-tc0
c
c          Read cfp from disk 72, or u2 from disk 73 (for e2 transitions),
c               or read U0, U2, U4,... (for plane-wave-Born calculations)
c
  370 n=nijk(irho,nc,i1)
      all=fllijk(irho,nc,i1)
      if (i.eq.j) go to 390
      iii=1
  375 if (n.le.2) go to 385
      call locdsk(id2,all,n)
      read (id2) (ia,ia,ia,a,a, m=1,nlt), nlp
      ia=ia
      if (iii.eq.1.and.itrans.eq.(-1)) go to 380
      if (iii.eq.2.and.itrans.eq.1) go to 380
      read (id2) ((cfp1(k,m), k=1,nlt), m=1,nlp)
      go to 385
  380 read (id2) ((cfp2(k,m), k=1,nlt), m=1,nlp)
  385 if (iii.eq.2) go to 400
      n=nijk(irhop,ncp,i2)
      all=fllijk(irhop,ncp,i2)
      iii=2
      go to 375
c
  390 if (n.lt.2) go to 400
      call locdsk(id3,all,n)
      if (ir1.le.1) go to 396
      do 394 k=2,ir1
  394 read (id3)
  396 read (id3) k73, ((u1(k,m), k=1,nlt), m=1,nlt)
      k73=k73
c
c
c
  400 if (itrans) 410,878,405
  405 l1=l
      l2=lp
      nc1=nc
      nc2=ncp
      j1=i1
      j2=i2
      k1=1
      k2=2
      sj1=sjd
      sj2=sjpd
      go to 420
  410 l1=lp
      l2=l
      nc1=ncp
      nc2=nc
      j1=i2
      j2=i1
      k1=2
      k2=1
      sj1=sjpd
      sj2=sjd
c          Test selection rules
  420 do 430 m=1,nosubc
      a=abs(scrl(l,m,1)-scrl(lp,m,2))
      b=scrl(l,m,1)+scrl(lp,m,2)
      if (m.ge.j) go to 426
      if (m.lt.i) go to 424
      if (a.gt.fllijk(i,nc1,j1)) go to 878
      if (b.lt.fllijk(i,nc1,j1)) go to 878
      if (m.eq.i) go to 430
      go to 428
  424 if (a.ne.0.0) go to 878
      go to 427
  426 if (a.gt.r) go to 878
      if (b.lt.r) go to 878
  427 if (scrs(l,m,1).ne.scrs(lp,m,2)) go to 878
      if (m.eq.j) go to 430
  428 if (nals(l,m)-notsj1(m,nc).ne.nalsp(lp,m)-notsjp(m,ncp)) go to 878
  430 continue
c
  450 tc=tc0
      a1=scrl(l1,nosubc,k1)
      a2=scrl(l2,nosubc,k2)
      a3=scrs(l1,nosubc,k1)
      tc=tc*uncpla(a1,a3,sj1, r,a2,sj2)
c
      mn=j+1
      if (mn.gt.nosubc) go to 460
      do 455 m=mn,nosubc
      a1=scrl(l1,m-1,k1)
      a2=scrl(l1,m,k1)
      a3=scrl(l2,m,k2)
      n1=nals(l,m)
      a4=fl(n1,m,1)
  455 tc=tc*uncpla(a1,a4,a2, r,scrl(l2,m-1,k2),a3)
c
  460 if (il1.eq.il2.and.nc.eq.ncp) go to 600
c
c          Complete calculation for non configuration-diagonal case
c
  500 if (nijk(i,nc1,j1).le.2) go to 510
      n1=nals(l,i)-notsj1(i,nc)
      n2=nalsp(lp,i)-notsjp(i,ncp)
      a=cfp1(n1,n2)
      if (itrans.lt.0) a=cfp1(n2,n1)
      tc=tc*a
  510 if (nijk(j,nc2,j2).le.2) go to 520
      n1=nals(l,j)-notsj1(j,nc)
      n2=nalsp(lp,j)-notsjp(j,ncp)
      a=cfp2(n1,n2)
      if (itrans.gt.0) a=cfp2(n2,n1)
      tc=tc*a
c
  520 fli=fllijk(i,nc1,j1)
      if (i.eq.1) go to 530
      if (nijk(i,nc1,j1).eq.1) go to 530
      a1=scrl(l1,i-1,k1)
      a2=scrl(l1,i,k1)
      a3=scrl(l2,i,k2)
      n1=nals(l,i)
      n2=nalsp(lp,i)
      if (itrans.gt.0) go to 522
      n1=n2
      n2=nals(l,i)
  522 a4=fl(n1,i,j1)
      a5=fl(n2,i,j2)
      b1=scrs(l1,i-1,k1)
      b2=scrs(l1,i,k1)
      b3=scrs(l2,i,k2)
      b4=s(n1,i,j1)
      b5=s(n2,i,j2)
      tc=tc*recpsh(a1,a5,a3,fli,a2,a4)*recpsh(b1,b5,b3,half,b2,b4)
c
  530 mn=i+1
      mx=j-1
      if (mn.gt.mx) go to 540
      do 539 m=mn,mx
      a1=scrl(l1,m-1,k1)
      a2=scrl(l2,m-1,k2)
      a3=scrl(l1,m,k1)
      a4=scrl(l2,m,k2)
      n1=nals(l,m)
      a5=fl(n1,m,1)
      b1=scrs(l1,m-1,k1)
      b2=scrs(l2,m-1,k2)
      b3=scrs(l1,m,k1)
      b4=scrs(l2,m,k2)
      b5=s(n1,m,1)
  539 tc=tc*recpex(a2,fli,a1,a5,a3,a4)*recpex(b2,half,b1,b5,b3,b4)
c
  540 flj=fllijk(j,nc2,j2)
      n1=nals(l,j)
      n2=nalsp(lp,j)
      if (itrans.gt.0) go to 542
      n1=n2
      n2=nals(l,j)
  542 if (j.eq.1) go to 700   ! rcg 11.4
      a1=scrl(l1,j-1,k1)
      a2=scrl(l2,j-1,k2)
      a3=scrl(l1,j,k1)
      a4=scrl(l2,j,k2)
      if (i.eq.j) go to 560
      a5=fl(n1,j,j1)
      a6=fl(n2,j,j2)
      b5=s(n1,j,j1)
      if (b5.eq.0.0) go to 550
      b1=scrs(l1,j-1,k1)
      b2=scrs(l2,j-1,k2)
      b3=scrs(l1,j,k1)
      b6=s(n2,j,j2)
      tc=tc*recpjp(b2,half,b1,b5,b3,b6)
  550 if (a5.eq.0.0) go to 560
      if (mod(flj+a5-a6,two).ne.zero) tc=-tc
      tc=tc*sqrt((2.0*a1+1.0)*(2.0*a6+1.0)*(2.0*a3+1.0)*(2.0*a4+1.0))
      tc=tc*s9j(a1,a2,fli, a5,a6,flj, a3,a4,r)
      go to 700
  560 if (a2.eq.0.0) go to 700 ! rcg 11.4
      tc=tc*uncplb(a2,fli,a3, r,flj,a4)
      go to 700
c
c          Complete calculation for configuration-diagonal case
c
  600 if (i.eq.1) go to 620
      n1=nals(l,i)
      n2=nalsp(lp,i)
  610 a1=scrl(l1,i-1,k1)
      a3=scrl(l1,i,k1)
      a4=scrl(l2,i,k2)
      a5=fl(n1,i,1)
      a6=fl(n2,i,2)
      tc=tc*uncplb(a1,a5,a3, r,a6,a4)
  620 if (nijk(i,nc1,j1).eq.1) go to 700
      n1=nals(l,i)-notsj1(i,nc)
      n2=nalsp(lp,i)-notsjp(i,ncp)
      tc=tc*u1(n1,n2)
c
  700 a1=tc*tc
      sums21=sums21+a1
      if (i1.ne.i2) go to 720
      if (sjd.ne.sjpd) go to 710
      if (l.le.lp) go to 725
      go to 720
  710 sums21=sums21+a1
  720 sums20=sums20+a1
  725 if (iabg.lt.3) go to 730
      m=nosubc
      ipp=scrl(l,m,1)+scrs(l,m,1)-sjd+scrl(lp,m,2)+scrs(lp,m,2)-sjpd
      if (mod(ipp,2).ne.0) tc=-tc
  730 c(l,lp)=tc
c
c          Sum line strength for each multiplet (special for Norm Magee)
c
      if (ielpun.lt.2) go to 878
      if (a1.le.0.0) go to 878
      if (nmult.eq.0) go to 830
      do 820 nn=1,nmult
      do 810 jj=1,ks
      if (nals(l,jj).ne.ials(nn,jj,1).or.nalsp(lp,jj).ne.ials(nn,jj,2))
     1  go to 820
      if (scrl(l,jj,1).ne.sl(nn,jj,1).or.scrl(lp,jj,2).ne.sl(nn,jj,2))
     1  go to 820
      if (scrs(l,jj,1).ne.ss(nn,jj,1).or.scrs(lp,jj,2).ne.ss(nn,jj,2))
     1  go to 820
  810 continue
      sums(nn)=sums(nn)+a1
      go to 878
  820 continue
  830 nmult=nmult+1
      if (nmult.le.kmult) go to 832
      write (iw,83) nmult,kmult
   83 format ('0*****nmult=',i3,'  greater than dimension kmult=',i3)
      stop '(mupole 83)'
  832 nn=nmult
      do 840 jj=1,ks
      ials(nn,jj,1)=nals(l,jj)
      ials(nn,jj,2)=nalsp(lp,jj)
      sl(nn,jj,1)=scrl(l,jj,1)
      sl(nn,jj,2)=scrl(lp,jj,2)
      ss(nn,jj,1)=scrs(l,jj,1)
      ss(nn,jj,2)=scrs(lp,jj,2)
      sums(nn)=a1
      fidmult(nn,1)=fidls(l,1)
      fidmult(nn,2)=fidls(lp,2)
      nexped(nn,1)=0.0
      nexped(nn,2)=0.0
  840 continue
c
  878 continue
  879 continue
c
c          Output (transform to JJ coupling if necessary)
c
  900 kpar=2
      scrj8=sjd
      scrj8p=sjpd
      call sprin
c
  925 go to 130
c
c          Write dummy record
c
  950 scrj8=-7.0
      nls=1
      write (ic) scrj8,scrj8,nls,nls, c(1,1), ncfg,ncfgp
      noicwr=noicwr+1
      noicmup(kk)=noicmup(kk)+1
      rewind il1
      rewind il2
      j1x=nsconf(2,i1)
      j2x=nsconf(2,i2)
      ssopi2=0.0
      do 958 nc=1,j1x
      do 958 ncp=1,j2x
  958 ssopi2=ssopi2+sopi2(nc,ncp)
      write (iw,92) sums21,sums20,ssopi2
   92 format (///15h0sums21,sums20=,2f12.5/8x,7hssopi2=,f12.5//)
      do 960 i=1,j1x
  960 write (iw,93) (sopi2(i,j), j=1,j2x)
   93 format (45x,9f8.3)
      if (ii.eq.0) sopi2(1,1)=sums21
      write (ic) sopi2
      noicwr=noicwr+1
      noicmup(kk)=noicmup(kk)+1
  990 continue
      return
      end
