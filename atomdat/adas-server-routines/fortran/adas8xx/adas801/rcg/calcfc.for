C UNIX-IDL - SCCS info: Module @(#)calcfc.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Add /charad/ common block.
C               Alter format statements 41 and 42.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
c     overlay(g9,6,0)
      subroutine calcfc
c
c          Set up final coef. matrices, and calc. LS-JJ transf. matrix
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks),mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/dumlc4(kmx**2-kcc*8),cc(kcc,8)
      dimension nparj(kc,kc)
      equivalence (dumlc4,nparj)
      common/c3lcm/mn1,mx1,mn2,mx2,mn3,mx3,mn4,mx4,mn5,mx5,
     1  mn6,mx6,mn7,mx7,mn8,mx8,sj2,sj3,sj4,sj5,sj6,sj7,sj8
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      character*10 parnam(kpr,2),aa,eav
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      character*8 fieras
      common/char/parnam,fidls,fidjj,ldes,ldesp
      dimension pc(kiser),v1(kls1,kls1,9),p5(kiser),ip5(kiser)
      equivalence (ct4,pc,v1),(cc,p5,ip5)
      dimension illp(kmx**2),coeff(kmx**2)
      equivalence (dumlc4,illp),(ct4,coeff)
      data eav/'Eav       '/
    
      character*3 fidlsad(kmx)
      common/charad/fidlsad                                             
c
c
c          Calculate final levels and coefficient matrices
c
c     ks=8
c     kc=50
      j1x=nsconf(2,kk)
      rewind il
      read (il) notsj1,nscrj8,sj8mn,sj8mx,npar,nparj,parnam,
     1  notsx, ((fl(l,i),s(l,i), l=1,notsx), i=1,nosubc),nalsjp,pj
c          write on tape5 input for least-squares program konfit
      if (ictbcd.gt.0) write (19,14)
   14 format (5x,1h )
c
c
  200 scrj8=sj8mn-1.0
      nj8non0=0
      do 900 n=1,nscrj8
      scrj8=scrj8+1.0
      rewind 20
c
c          Set up LS levels
c
      l=0
      mx=0
      do 229 k=1,ndifft
      lf(k)=0
      mn=mx+1
      mx=mx+ntrmk(k)
      sjmn=abs(pscrl(mn,nosubc)-pscrs(mn,nosubc))
      sjmx=pscrl(mn,nosubc)+pscrs(mn,nosubc)
      if (scrj8.lt.sjmn) go to 229
      if (scrj8.gt.sjmx) go to 229
      do 219 m=mn,mx
      l=l+1
      if (l.gt.kmx) go to 219
      ncfg(l)=ncfgp(m)
      do 215 i=1,nosubc
      nals(l,i)=nalsp(m,i)
      scrl(l,i)=pscrl(m,i)
  215 scrs(l,i)=pscrs(m,i)
  219 continue
      lf(k)=1
  229 continue
      nls=l
      if (nls.le.kmx) go to 300
      write (iw,22) scrj8,nls,kmx
   22 format (' For J=',f4.1,',  matrix size',i5,'  is greater than ',
     1  'dimension kmx=',i5)
      if (iw6.lt.0) write (6,22) scrj8,nls,kmx
      stop '(calcfc 229)'
c
c          Set up JJ levels
c
  300 if (nolskp.ne.0) go to 350
      l=0
      k=0
      j1=1
      mx1=0
      k1mx=ndiffj(1)
      do 349 k1=1,k1mx
      mn1=mx1+1
      mx1=mx1+njk(k1,1)
      if (nalsjp(mx1,1).gt.notsj1(1,j1+1)) j1=j1+1
      mx2=0
      k2mx=ndiffj(2)
      do 348 k2=1,k2mx
      mn2=mx2+1
      mx2=mx2+njk(k2,2)
      j=nalsjp(mx2,2)
      if (j.le.notsj1(2,j1).or.j.gt.notsj1(2,j1+1)) go to 348
      mx3=0
      k3mx=ndiffj(3)
      do 347 k3=1,k3mx
      mn3=mx3+1
      mx3=mx3+njk(k3,3)
      j=nalsjp(mx3,3)
      if (j.le.notsj1(3,j1).or.j.gt.notsj1(3,j1+1)) go to 347
      mx4=0
      k4mx=ndiffj(4)
      do 346 k4=1,k4mx
      mn4=mx4+1
      mx4=mx4+njk(k4,4)
      j=nalsjp(mx4,4)
      if (j.le.notsj1(4,j1).or.j.gt.notsj1(4,j1+1)) go to 346
      mx5=0
      k5mx=ndiffj(5)
      do 345 k5=1,k5mx
      mn5=mx5+1
      mx5=mx5+njk(k5,5)
      j=nalsjp(mx5,5)
      if (j.le.notsj1(5,j1).or.j.gt.notsj1(5,j1+1)) go to 345
      mx6=0
      k6mx=ndiffj(6)
      do 344 k6=1,k6mx
      mn6=mx6+1
      mx6=mx6+njk(k6,6)
      j=nalsjp(mx6,6)
      if (j.le.notsj1(6,j1).or.j.gt.notsj1(6,j1+1)) go to 344
      mx7=0
      k7mx=ndiffj(7)
      do 343 k7=1,k7mx
      mn7=mx7+1
      mx7=mx7+njk(k7,7)
      j=nalsjp(mx7,7)
      if (j.le.notsj1(7,j1).or.j.gt.notsj1(7,j1+1)) go to 343
      mx8=0
      k8mx=ndiffj(8)
      do 342 k8=1,k8mx
      mn8=mx8+1
      mx8=mx8+njk(k8,8)
      j=nalsjp(mx8,8)
      if (j.le.notsj1(8,j1).or.j.gt.notsj1(8,j1+1)) go to 342
      sj2=abs(pj(mn1,1)-pj(mn2,2))-1.0
      nj2=pj(mn1,1)+pj(mn2,2)-sj2
      do 339 ij2=1,nj2
      sj2=sj2+1.0
      sj3=abs(sj2-pj(mn3,3))-1.0
      nj3=sj2+pj(mn3,3)-sj3
      do 339 ij3=1,nj3
      sj3=sj3+1.0
      sj4=abs(sj3-pj(mn4,4))-1.0
      nj4=sj3+pj(mn4,4)-sj4
      do 339 ij4=1,nj4
      sj4=sj4+1.0
      sj5=abs(sj4-pj(mn5,5))-1.0
      nj5=sj4+pj(mn5,5)-sj5
      do 339 ij5=1,nj5
      sj5=sj5+1.0
      sj6=abs(sj5-pj(mn6,6))-1.0
      nj6=sj5+pj(mn6,6)-sj6
      do 339 ij6=1,nj6
      sj6=sj6+1.0
      sj7=abs(sj6-pj(mn7,7))-1.0
      nj7=sj6+pj(mn7,7)-sj7
      do 339 ij7=1,nj7
      sj7=sj7+1.0
      sj8=abs(sj7-pj(mn8,8))-1.0
      nj8=sj7+pj(mn8,8)-sj8
      do 339 ij8=1,nj8
      sj8=sj8+1.0
      if (sj8.ne.scrj8) go to 339
c
      k=k+1
      nsjk(k)=0
c
c          8-fold do loop (37 lines) moved to subroutine cafcdolp
c               because of VAX-compiler limitations
c
      call cafcdolp(l,k)
c
  339 continue
  342 continue
  343 continue
  344 continue
  345 continue
  346 continue
  347 continue
  348 continue
  349 continue
      ndifsj=k
      njj=l
      if (njj.ne.nls) write (iw,35) nls,njj,scrj8
   35 format (///5h0nls=,i3,5x,5h njj=,i3,5x,11h for scrj8=,f4.1)
      if (nls.le.kmx) go to 350
      write (iw,36) nls,kmx,scrj8
   36 format (///18h0*****matrix size=,i4,
     1  25h  greater than dimension=,i4,'     for scrj8=',f4.1)
      nls=kmx
c
c          Calculate subshell to be used in basis-state labels
c
  350 if (nls.le.0) go to 600
      nj8non0=nj8non0+1
      do 390 j=1,j1x
      isubj(j)=0
      isub=0
      do 380 i=1,nosubc
      i1=nosubc-i+1
      w=nijk(i1,j,kk)
      wfm1=4.0*fllijk(i1,j,kk)+1.0
      if (w.eq.0.0.or.w.gt.wfm1) go to 380
      if (w.eq.1.0.or.w.eq.wfm1) go to 370
      isubj(j)=-i1
      go to 380
  370 if (isubj(j).eq.0.and.i1.lt.isub) isubj(j)=i1
      isub=i1
  380 continue
      if (isubj(j).eq.0) isubj(j)=1
  390 continue
c
c          Write complete quantum numbers,
c               and form basis-state labels
c
      ipt=0
      if (icfc.eq.0) go to 405
      ipt=1
      if (kcpld(1)+kcpld(2).gt.1) ipt=0
      if (kcpld(9).ge.9.or.ispecc.ge.9) ipt=0
      jjj=min0(j1x,9)
      if (ipt.eq.0) go to 400
      if (jjj+nls*((ks+3)/4).gt.55.or.icfc.gt.2) write (iw,37)
   37 format (1h1)
  400 write(iw,38) scrj8,(j,(llijk(i,j,kk),nijk(i,j,kk),i=1,ks),j=1,jjj)
   38 format (3h0j=,f4.1,10x,13hconfiguration,i10,4x,8(a1,i2,3x)/
     1  (30x,i10,4x,8(a1,i2,3x)))
      if (j1x.gt.9) write (iw,39)  j1x, (llijk(i,j1x,kk),nijk(i,j1x,kk),
     1  i=1,ks)
   39 format (30x,i10,4x,8(a1,i2,3x))
      write (iw,40)
   40 format (7h ------/)
  405 do 470 l=1,nls
      do 410 i=1,nosubc
      m=nals(l,i)
      mul(i)=mult(m,i)
      lh(i)=lbcd(m,i)
      muls(i)=2.0*scrs(l,i)+1.0
      lp1=scrl(l,i)+1.0
  410 lhs(i)=lsymb(lp1+24)
      j=ncfg(l)
      i1=iabs(isubj(j))
      mi=mul(i1)
      li=lh(i1)
      if (isubj(j).lt.0) go to 420 ! 420 --> 415 for latest cowan code
      mi=muls(i1)
      li=lhs(i1)

      goto 420 ! bypass new cowan code stuff  

c
c     start upgrade to new version of cowan code, ADW
c
  415 nocc=0
      do 417 i=1,nosubc
      if (nijk(i,j,kk).gt.0.and.nijk(i,j,kk).lt.4*llijk(i,j,kk)+2)
     1  nocc=nocc+1
  417 continue
      write(0,*) nocc
      if (nocc.gt.1) go to 420
      mi=1
      li=lsymb(25)
c
c     end upgrade to new version of cowan code, ADW
c
c 420 encode (8,41,fidls(l,1)) mi,li,muls(nosubc),lhs(nosubc)

C------START ADAS conversion-------

C  420 write (fidls(l,1),41) mi,li,muls(nosubc),lhs(nosubc)
C   41 format (1h(,i1,a1,1h),i2,a1,1x)
C      if (ipt.eq.0) go to 440
C      if (nosubc.le.3) go to 430
C      write (iw,42) l, ncfg(l), (nals(l,i),mul(i),lh(i),
C     1    muls(i),lhs(i), i=1,nosubc)
C   42 format(1h ,i3,i3,2x,1h(,i3,i2,a1,1h),i2,a1,
C     1  7(3h  (,i1,i3,a1,1h),i2,a1))

 
  420 WRITE (FIDLS(L,1),41) L,MI,LI,MULS(NOSUBC),LHS(NOSUBC)            
   41 FORMAT (i3,I1,A1,I2,A1)                                           
 
      write (fidlsad(l),'(i3)')L
      
      IF (IPT.EQ.0) GO TO 440                                           
      IF (NOSUBC.LE.3) GO TO 430                                        
      WRITE (IW,42) L, NCFG(L), (NALS(L,I),MUL(I),LH(I),                
     1    MULS(I),LHS(I), I=1,NOSUBC)                                   
 
   42 format(1h ,i3,i3,2x,1h(,i3,i2,a1,1h),i2,a1,                       
     1  7(3h  (,i2,i2,a1,1h),i2,a1))                                    
 
C------End ADAS conversion-------

      go to 440

  430 write (iw,43) ncfg(l), (nals(l,i),mul(i),lh(i),
     1    muls(i),lhs(i), i=1,nosubc)
   43 format (1h ,i2,4x,3(1h(,i3,i3,a1,1h),i3,a1,4x))
  440 if (nolskp.ne.0) go to 470
      do 445 i=1,nosubc
      m=nalsj(l,i)
      mul(i)=mult(m,i)
      lh(i)=lbcd(m,i)
  445 continue
      fji=fj(l,i1)
      if (isubj(j).gt.0) fji=scrj(l,i1)
c     encode (8,44,fidjj(l,1)) mul(i1),lh(i1),fji
      write (fidjj(l,1),44) mul(i1),lh(i1),fji
   44 format (1h(,i1,a1,f4.1,1h))
      if (ipt.eq.0) go to 470
      if (nosubc.gt.4) go to 470
      if (nosubc.le.2) go to 460
      write (iw,45) l,
     1 (nalsj(l,i),mul(i),lh(i),fj(l,i),scrj(l,i), i=1,nosubc)
   45 format (1h+,i60,4x,1h(,i3,i2,a1,f4.1,1h),f4.1,
     1  3(3h  (,i2,i2,a1,f4.1,1h),f4.1))
      go to 470
  460 write (iw,46) l,
     1 (nalsj(l,i),mul(i),lh(i),fj(l,i),scrj(l,i), i=1,nosubc)
   46 format (1h+,i60,3x,3(4x,1h(,i3,i3,a1,f5.1,1h),f5.1))
  470 continue
      if (nolskp.ne.0) go to 595
      if (ipt.eq.0) go to 505
      write (iw,43)
      if (nosubc.le.4) go to 505
      do 472 l=1,nls
      do 471 i=1,nosubc
      m=nalsj(l,i)
      mul(i)=mult(m,i)
  471 lh(i)=lbcd(m,i)
      write (iw,47) l,
     1 (nalsj(l,i),mul(i),lh(i),fj(l,i),scrj(l,i), i=1,nosubc)
   47 format (i5,1x,8(2h (,i2,i2,a1,f4.1,1h),f4.1))
  472 continue
      write (iw,43)
c
c          Calculate transformation matrix
c
  505 do 530 ls=1,nls
      ilssl=scrl(ls,nosubc)+scrs(ls,nosubc)-scrj8
      ilssl=mod(ilssl,2)
      do 530 jj=1,njj
      tmx(ls,jj)=0.0
      do 510 i=1,nosubc
      if (nals(ls,i).ne.nalsj(jj,i)) go to 530
  510 continue
      tmx(ls,jj)=1.0
      if (iabg.ge.3.and.ilssl.ne.0) tmx(ls,jj)=-1.0
      if (nosubc.le.1) go to 530
      do 521 i=2,nosubc
      ml=nals(ls,i)
      if (fl(ml,i).eq.0.0.and.s(ml,i).eq.0.0) go to 521
      if (scrl(ls,i-1).eq.0.0.and.scrs(ls,i-1).eq.0.0) go to 521
      a=sqrt((2.0*scrl(ls,i)+1.0)*(2.0*scrs(ls,i)+1.0)
     1  *(2.0*fj(jj,i)+1.0)*(2.0*scrj(jj,i-1)+1.0))
      b=s9j(scrl(ls,i-1),fl(ml,i),scrl(ls,i), scrs(ls,i-1),
     1  s(ml,i),scrs(ls,i), scrj(jj,i-1),fj(jj,i),scrj(jj,i))
      tmx(ls,jj)=a*b*tmx(ls,jj)
  521 continue
  530 continue
c          Check orthonormality
      if (icfc.le.1) go to 540
      write (iw,53) coupl(1)
   53 format (  27h0Transformation matrix from,a6,15h to jj coupling)
  540 do 550 l=1,nls
      do 550 lp=l,nls
      sum=0.0
      if (l.eq.lp) sum=-1.0
      do 543 m=1,njj
  543 sum=sum+tmx(l,m)*tmx(lp,m)
      if (abs(sum).ge.1.0e-7) write (iw,54) l,lp
   54 format (6h rows ,i3, 5h and ,i3, 16h not orthonormal)
  550 continue
c
      if (icfc.le.1) go to 600
      lpx=0
  560 lpn=lpx+1
      lpx=lpx+11
      if (lpx.gt.njj) lpx=njj
      write (iw,55) (fidjj(lp,1), lp=lpn,lpx)
   55 format (//28x, 11(1x,a8))
      write (iw,56) (lp, lp=lpn,lpx)
   56 format (26x,11i9)
      write (iw,57)
   57 format (1h )
      do 580 l=1,nls
  580 write (iw,58) ncfg(l),fidls(l,1),l, (tmx(l,lp), lp=lpn,lpx)
   58 format (i11,2x,1x,a8,i4,2x,11f9.5)
      if (lpx.lt.njj) go to 560
  595 call clock(time)
      delt=time-time0
      write (iw,59) scrj8,delt
   59 format (///' calcfc, j=',f4.1,5x,5htime=,f6.3,4h min)
      if (iw6.lt.0) write (6,59) scrj8,delt
c
c          Form final coefficient matrices
c
  600 ilc=il
      nls1=max(nls,1)
      do 601 nn=1,2
      write (ilc) scrj8,nls,nls1, ((tmx(l,lp),l=1,nls1), lp=1,nls1),
     1  ((nals(l,i),scrl(l,i),scrs(l,i),
     2  i=1,nosubc), ncfg(l),fidls(l,1),fidjj(l,1), l=1,nls1)
  601 ilc=ic
      if (nls.le.0) go to 900
      noicwr=noicwr+1
      npar=0
      nc=0
      if (ictbcd.le.0) go to 604
      nx=min0(n,j1x)
      write (19,61) n,nls,scrj8, coupl(kcpl), (llijk(i,nx,kk),
     1  nijk(i,nx,kk), i=1,6)
   61 format (2i6,6h     1,6x,f4.1, 3x,a6,5h cplg,6x, 2(2x,a1,i2),
     1  1x, 4(1x,a1,i1))
c
  604 nparo=npar
      read (20) npar,kpar,k20,i,j,aa,cave,j1,nopc, (pc(n1), n1=1,nopc)
      k20=k20
      if (npar.eq.-7) go to 890
      if (ictbcd.eq.0) go to 609
      nparx=npar
      if (npar.eq.nparo+1) go to 609
      nn=-n
      npar20=npar
      npar=nparo+1
  605 nc=nc+1
      parnam(npar,kk)=eav
      do 607 lp=1,nls
      do 606 l=lp,nls
  606 c(l,lp)=0.0
      c(lp,lp)=0.0
      if (ncfg(lp).eq.nc) c(lp,lp)=1.0
  607 continue
      go to 805
  609 parnam(npar,kk)=aa
      j1a=j1
      j1b=j1
  610 do 611 l=1,nls
      do 611 lp=1,l
  611 c(l,lp)=0.0
      if (kpar.gt.0) go to 630
      if (kpar.eq.-2) go to 680
c
c          fk,gk matrix (lower triangle)
c
  620 lx=0
      lmx=0
      m=0
      do 629 k=1,ndifft
      lmx=lmx+ntrmk(k)
      if (ncfgp(lmx).eq.j1) go to 621
      if (lf(k).gt.0) lx=lx+ntrmk(k)
      go to 629
  621 if (lf(k).gt.0) go to 622
      m=m+(ntrmk(k)*(ntrmk(k)+1))/2
      go to 629
  622 ln=lx+1
      lx=lx+ntrmk(k)
      do 625 l=ln,lx
      do 625 lp=ln,l
      m=m+1
  625 c(l,lp)=pc(m)
  629 continue
      go to 690
c
c          zeta matrix (lower triangle)
c
  630 if (kcpl.eq.2) go to 640
c          ls coupling
      j1v=0
      thrhaf=1.5
      sqrt3h=sqrt(thrhaf)
      do 639 l=1,nls
      j=ncfg(l)
      if (j.ne.j1) go to 639
      if (j1v.eq.j) go to 632
      j1v=j
      lij=notsj1(i,j)
      v1(1,1,2)=sqrt3h
      el=fllijk(i,j,kk)
      if (nijk(i,j,kk).eq.1) go to 632
      call locdsk(id3,el,nijk(i,j,kk))
      nor=(norcd/2)+2
      do 631 m=1,nor
  631 read (id3) k, ((v1(ii,jj,2),ii=1,nlt), jj=1,nlt)
  632 do 638 lp=1,l
      if (ncfg(lp).ne.j) go to 638
      k=nosubc
      do 634 m=1,k
      if (m.eq.i) go to 633
      if (nals(l,m).ne.nals(lp,m)) go to 638
      if (m.gt.i) go to 633
      if (scrl(l,m).ne.scrl(lp,m)) go to 638
      if (scrs(l,m).ne.scrs(lp,m)) go to 638
      go to 634
  633 if (abs(scrl(l,m)-scrl(lp,m)).gt.1.0) go to 638
      if (abs(scrs(l,m)-scrs(lp,m)).gt.1.0) go to 638
  634 continue
      eras=1.0
      ii=scrl(lp,k)+scrs(l,k)+scrj8
      if (iabg.ge.3) ii=scrl(l,k)+scrs(lp,k)+scrj8
      if (mod(ii,2).ne.0) eras=-1.0
      eras=eras*s6j(scrl(l,k),scrs(l,k),scrj8,scrs(lp,k),scrl(lp,k),
     1  one)
      if (i.eq.k) go to 636
      ip1=i+1
      do 635 m=ip1,k
      lt=nals(l,m)
  635 eras=eras*uncpla(scrl(l,m-1),fl(lt,m),scrl(l,m),1.d0,scrl(lp,m-1),
     1  scrl(lp,m))*uncpla(scrs(l,m-1),s(lt,m),scrs(l,m),one,
     2  scrs(lp,m-1),scrs(lp,m))
  636 lt=nals(l,i)
      lr=nals(lp,i)
      if (i.gt.1) eras=eras*uncplb(scrl(l,i-1),fl(lt,i),scrl(l,i),
     1  one,fl(lr,i),scrl(lp,i))*uncplb(scrs(l,i-1),s(lt,i),scrs(l,i),
     2  one,s(lr,i),scrs(lp,i))
      c(l,lp)=eras*sqrt(el*(el+1.0)*(2.0*el+1.0))*v1(lt-lij,lr-lij,2)
  638 continue
  639 continue
      go to 690
c          JJ coupling
  640 kix=ndiffj(i)
      lx=0
      do 679 k=1,ndifsj
      ln=lx+1
      lx=lx+nsjk(k)
      j=nalsj(lx,i)
      if (ncfg(ln).ne.j1) go to 679
      ml=1
      mc=1
      do 643 ki=1,kix
      if (ncfgjp(ml,i)-j1) 643,641,644
  641 if (pj(ml,i)-fj(ln,i)) 644,645,642
  642 mc=mc+(njk(ki,i)*(njk(ki,i)+1))/2
  643 ml=ml+njk(ki,i)
  644 write (iw,64) ln,i,fj(ln,i),ml,mc
   64 format (//19h0cannot find pj=fj(,i2,1h,,i1,2h)=,f4.1,
     1  5x,3hml=,i4,5x,3hmc=,i4//)
  645 m1=njk(ki,i)-1
      if (m1.gt.0) go to 655
c            (1 x 1  submatrix)
      do 653 lp=ln,lx
      c(lp,lp)=pc(mc)
  653 continue
      go to 679
c            (n x n  submatrix)
  655 do 656 l=ln,lx
      if (nalsj(l,i).eq.nalsjp(ml+1,i)) go to 657
  656 continue
  657 ldel=l-ln
  660 do 669 l0=ln,lx
      if (nalsj(l0,i).ne.nalsjp(ml,i)) go to 669
      m=mc
      l1=l0+ldel*m1
      do 665 l=l0,l1,ldel
      do 665 lp=l0,l,ldel
      c(l,lp)=pc(m)
      m=m+1
  665 continue
  669 continue
  679 continue
      go to 690
c
c          rk matrix (lower triangle)
c
  680 j1a=i
      j1b=j
      lamn=0
      lamx=0
      lbmx=0
      do 682 l=1,nls
      if (ncfg(l).lt.j1a) lamn=l
      if (ncfg(l).eq.j1a) lamx=l
      if (ncfg(l).lt.j1b) lbmn=l
  682 if (ncfg(l).eq.j1b) lbmx=l
      if (lamx.eq.0.or.lbmx.eq.0) go to 690
      lj=lamn
      lamn=lamn+1
      lbmn=lbmn+1
      lpj=lbmn
c
      ln=ntottj(j1a)+1
      lx=ntottj(j1a+1)
      lpn=ntottj(j1b)+1
      lpx=ntottj(j1b+1)
      nopc=0
      do 688 lp=lpn,lpx
      do 688 l=ln,lx
      itest=0
      if (pscrl(l,nosubc).ne.pscrl(lp,nosubc)) itest=1
      if (pscrs(l,nosubc).ne.pscrs(lp,nosubc)) itest=1
      if (itest.eq.0) nopc=nopc+1
      if (abs(pscrl(l,nosubc)-pscrs(l,nosubc)).gt.scrj8) go to 688
      if (abs(pscrl(lp,nosubc)-pscrs(lp,nosubc)).gt.scrj8) go to 688
      if (pscrl(l,nosubc)+pscrs(l,nosubc).lt.scrj8) go to 688
      if (pscrl(lp,nosubc)+pscrs(lp,nosubc).lt.scrj8) go to 688
      lj=lj+1
      if (lj.le.lamx) go to 686
      lj=lamn
      lpj=lpj+1
      if (lpj.le.lbmx) go to 686
      write (iw,68) j1a,j1b,l,lp,lj,lpj,lamn,lamx,lbmn,lbmx,nopc,
     1  pc(nopc),ln,lx,lpn,lpx
   68 format (//17h0***lpj.gt.lbmx  ,2i5,3x,4i5,3x,4i5,i10,f15.7,5x,4i5)
  686 if (itest.eq.0) c(lpj,lj)=pc(nopc)
  688 continue
c
c
c          Complete matrix (upper triangle)
c
  690 do 691 l=1,nls
      do 691 lp=1,l
  691 c(lp,l)=c(l,lp)
c
c          output
c
  700 k=kc
      nnn=n
      call sprin
c
c          Write matrix coefficients on unit ic
c               (non-zero coefficients only)
c
c 800 write (ic) ((c(l,lp),l=lp,nls), lp=1,nls)
  800 ncoeff=0
      do 801 lp=1,nls
      do 801 l=lp,nls
      if (c(l,lp).eq.0.0) go to 801
      ncoeff=ncoeff+1
      illp(ncoeff)=10000*l+lp
      coeff(ncoeff)=c(l,lp)
  801 continue
      if (ncoeff.gt.0) go to 802
      ncoeff=1
      illp(1)=10001
      coeff(1)=0.0
  802 write (ic) ncoeff,(illp(i),coeff(i),i=1,ncoeff)
      noicwr=noicwr+1
      if (ictbcd.eq.0) go to 604
      nn=n
  805 if (ictbcd.gt.0) go to 870
c
c          Write matrix elements for Argonne NL least-squares program
c
  870 n5=0
      do 880 lp=1,nls
      do 879 l=lp,nls
      if (abs(c(l,lp)).le.1.0e-9) go to 879
      if (ictbcd.gt.0) go to 874
      n5=n5+1
      ip5(n5)=((lp*i2to18)+l)*i2to18+npar
      p5(n5+1000)=c(l,lp)
      go to 879
  874 if (kcpl.le.1) go to 875
      write (19,87) n,lp,l,npar,c(l,lp), nalsj(l,1),fidjj(l,1),
     1  parnam(npar,1)
   87 format (4i6,f12.7, 2h (,i3,1x,a8,2x,a8,3x,3(a1,i2,2x) )
      go to 879
  875 write(19,88) n,lp,l,npar,c(l,lp), nals(l,1),fidls(l,1),
     1  parnam(npar,1)
   88 format (4i6,f12.7, 2h (,i3,1x,a8,2x,a8,3x,3(a1,i2,2x) )
  879 continue
  880 continue
  888 if (nn.gt.0) go to 604
      nparo=npar
      npar=npar+1
      if (npar.eq.npar20) go to 609
      go to 605
  890 if (ictbcd.ge.0) go to 895
  895 if (ictbcd.gt.0)  write (19,14)
c
      call cpl37
c
c
  900 continue
      nj8k(kk)=nj8non0
c           Write dummy record,
c                (required by system bug on Stretch, upon backspace)
      write (il) (pc(i), i=1,20)
c
      rewind 20
      rewind il
      if (ictbcd.le.0) go to 950
      m=nosubc
      if (kcpl.ne.1) go to 920
      read (il)
      do 915 n=1,nscrj8
      read (il) scrj8,nls,nls1, ((a,l=1,nls1),lp=1,nls1),
     1  ((iii,scrl(l,i),scrs(l,i), i=1,m), iii,fieras,fieras, l=1,nls1)
      iii=iii
      do 910 l=1,nls
      pc(l)=0.0
      if  (scrj8.eq.0.0) go to 910
      pc(l)=0.50116*(scrs(l,m)*(scrs(l,m)+1.0)-scrl(l,m)*(scrl(l,m)+1.))
     1  /(scrj8*(scrj8+1.0))+1.50116
  910 continue
  915 write (19,91) (pc(l),l=1,nls)
   91 format (6f12.6)
      rewind il
  920 read (il)
      do 930 n=1,nscrj8
      read (il) scrj8,nls,nls1, ((a,l=1,nls1), lp=1,nls1),
     1  ((iii,a,a, i=1,m), iii,fidls(l,1),fidjj(l,1), l=1,nls1)
      if (kcpl.ne.1) go to 925
      write (19,92) (fidls(l,1), l=1,nls)
   92 format (6(1x,a8,1x,:,2x))
      go to 930
  925 write (19,92) (fidjj(l,1), l=1,nls)
  930 continue
      rewind il
      write (19,93) (parnam(i,1), i=1,nparx)
   93 format (6(2x,a10))
      end file 19
      write (iw,96) coupl(kcpl)
   96 format (/44h0coefficient matrices written on tape 19 in ,
     1  a6,9h coupling)
  950 return
      end
