C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/plev.for,v 1.3 2010/04/09 16:45:16 mog Exp $ Date $Date: 2010/04/09 16:45:16 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.2
C  DATE    : 09-04-2010
C  MODIFIED: Martin O'Mullane
C             - Literal constants in quotes, to be assigned to an
C               integer variable, are not allowed in data statements
C               (IBM allowed extension to f66) so replace with 
C               Hollerith constants (f77 standard).
C
C-----------------------------------------------------------------------
c     overlay(g9,3,0)
      subroutine plev
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/c3lc7/m,i,j,k,l,i1,j2,k1,l8,j1
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
      data iblank/1h /
c
c
c     ks=8
c     kjp=500
c     klsp=210
c     kjk=110
      ntottj(1)=0
      ndifft=0
      sumx=0.0
      sj8mn=1000.0
      sj8mx=0.0
      sj8mx1=0.0
      do 50 i=1,ks
      nalsjp(1,i)=0
      notsj1(i,1)=0
      ntotjj(i,1)=0
   50 ndiffj(i)=0
c
      notsx=0
      jx=nsconf(2,kk)
      do 229 j1=1,jx
      ll(nosubc)=llijk(nosubc,j1,kk)
      fll(nosubc)=fllijk(nosubc,j1,kk)
      do 79 i=1,ks
      nots=notsj1(i,j1)
c             Read terms from disk 72
      ni(i)=nijk(i,j1,kk)
      nn=ni(i)
      nfull=4.0*fll(i)+2.0
      if (nn.gt.1.and.nn.lt.nfull-1) go to 70
      not=1
      nlt=1
      nlastt(i)=nlt
      not1=nots+1
      nots=nots+nlt
      mult(not1,i)=1
      lbcd(not1,i)=lsymb(25)
      ialf(not1,i)=iblank
      fl(not1,i)=0.0
      s(not1,i)=0.0
      if (nn.eq.0.or.nn.eq.nfull) go to 75
      mult(not1,i)=2
c     lbcd(not1,i)=ll(i)
        do j=1,24
        if (ll(i).eq.lsymb(j)) lbcd(not1,i)=lsymb(j+24)
        end do
      ialf(not1,i)=iblank
      fl(not1,i)=fll(i)
      s(not1,i)=0.5
      go to 75
   70 call locdsk(id2,fll(i),ni(i))
      if (i.eq.1.and.j1.lt.jx) nlt=min0(nlt,nlt11)
      if (i.eq.1.and.jx.eq.1) nlt=min0(nlt,nlt11)
      nlastt(i)=nlt
      not1=nots+1
      nots=nots+nlt
      read (id2) (mult(m,i),lbcd(m,i),ialf(m,i),fl(m,i),s(m,i),
     1  m=not1,nots)
   75 notsj1(i,j1+1)=nots
      notsx=max0(nots,notsx)
        do 77 m=not1,nots
        do j=1,24
        if (lbcd(m,i).eq.lsymb(j)) lbcd(m,i)=lsymb(j+24)
        end do
   77   continue
   79 continue
c
c          Calc. preliminary ls terms
c
      m=ntottj(j1)
      n1=notsj1(1,j1)+1
      n2=notsj1(2,j1)+1
      n3=notsj1(3,j1)+1
      n4=notsj1(4,j1)+1
      n5=notsj1(5,j1)+1
      n6=notsj1(6,j1)+1
      n7=notsj1(7,j1)+1
      n8=notsj1(8,j1)+1
      n11=notsj1(1,j1+1)
      n22=notsj1(2,j1+1)
      n33=notsj1(3,j1+1)
      n44=notsj1(4,j1+1)
      n55=notsj1(5,j1+1)
      n66=notsj1(6,j1+1)
      n77=notsj1(7,j1+1)
      n88=notsj1(8,j1+1)
      do 120 i=n1,n11
      do 120 j=n2,n22
      do 120 k=n3,n33
      do 120 l=n4,n44
      do 120 i1=n5,n55
      do 120 j2=n6,n66
      do 120 k1=n7,n77
      do 120 l8=n8,n88
c
c          15-fold do loop (101 lines) moved to subroutine plevdolp
c               because of VAX-compiler limitations
c
      call plevdolp
c
  120 continue
      notott=m
      l=ntottj(j1)+1
      ntottj(j1+1)=m
      if (iplev) 125,125,121
  121 write (iw,12)
   12 format (9h1LS terms)
      do 122 m=l,notott
  122 write (iw,13) (ncfgp(m),
     1              nalsp(m,i),pscrs(m,i),pscrl(m,i), i=1,nosubc)
   13 format (1h , 8(i5,i3,f5.1,f3.0))
c
c        Sort on decreasing   25*scrs4+scrl4
c
  125 m1=ntottj(j1+1)-ntottj(j1)
      if (m1.le.klsp) go to 126
      write (6,1013) m1,klsp
 1013 format (' m1=',i4,' greater than klsp=',i4)
      stop '(m1.gt.klsp at PLEV 125)'
  126 if (m1.le.1) go to 129
      call sort2(m1,11,pc(l),nalsp(l,1),pscrs(l,1),nalsp(l,2),
     1   nalsp(l,3),pscrl(l,1),pscrs(l,2),pscrl(l,2),pscrs(l,3),
     2   pscrl(l,3),pscrs(l,4),pscrl(l,4),dum)
      call orderi(m1,nalsp(l,4))
      if (nosubc.le.4) go to 129
      if (nosubc.lt.7) go to 127
      call order2(m1,pscrl(l,8))
      call order2(m1,pscrs(l,8))
      call orderi(m1,nalsp(l,8))
      call order2(m1,pscrl(l,7))
      call order2(m1,pscrs(l,7))
      call orderi(m1,nalsp(l,7))
  127 call order2(m1,pscrl(l,6))
      call order2(m1,pscrs(l,6))
      call orderi(m1,nalsp(l,6))
      call order2(m1,pscrl(l,5))
      call order2(m1,pscrs(l,5))
      call orderi(m1,nalsp(l,5))
c
  129 if (iplev) 135,135,130
  130 write (iw,14)
   14 format (///18h0LS terms (sorted)/)
      do 131 m=l,notott
  131 write (iw,13) (ncfgp(m),nalsp(m,i),pscrs(m,i),
     1                        pscrl(m,i), i=1,nosubc)
      write (iw,15)
   15 format (1h1)
c
c        Calc. number of terms of each kind
c
  135 k=ndifft
      n2=-100
  140 do 143 m=l,notott
      n1=n2
      n2=25.0*pscrs(m,nosubc)+pscrl(m,nosubc)
      if (n2-n1) 142,141,1942
  141 ntrmk(k)=ntrmk(k)+1
      go to 143
 1942 if (m.eq.l) go to 142
      stop '(terms not correctly sorted at PLEV 141)'
  142 k=k+1
      ntrmk(k)=1
      sj8mn=min(sj8mn,abs(pscrs(m,nosubc)-pscrl(m,nosubc)))
      sj8mx=max(sj8mx,pscrs(m,nosubc)+pscrl(m,nosubc))
      sj8mx1=max(sj8mx1,pscrs(m,nosubc)+pscrl(m,nosubc))
  143 continue
      ndifft=k
  150 if (sj8mn.eq.sj8mx) go to 170
      if (sj8mn.ge.sjn) go to 160
      sj8mn=sj8mn+1.0
      go to 150
  160 if (sj8mx.le.sjx) go to 170
      sj8mx=sj8mx-1.0
      go to 160
  170 nscrj8=sj8mx-sj8mn+1.0
c
c          Calc. preliminary levels for each subshell
c
  200 if (nolskp.ne.0) go to 229
      sum=0.0
      do 225 i=1,nosubc
      m=ntotjj(i,j1)
      n1=notsj1(i,j1)+1
      n11=notsj1(i,j1+1)
      do 205 j=n1,n11
      eji=abs(fl(j,i)-s(j,i))-1.0
      nji=fl(j,i)+s(j,i)-eji
      do 205 ij=1,nji
      m=m+1
      eji=eji+1.0
      nalsjp(m,i)=j
      a=j
      ncfgjp(m,i)=j1
      pc(m)=-200.0*eji-25.0*s(j,i)-fl(j,i)+0.001*a
  205 pj(m,i)=eji
      nototj(i)=m
      ntotjj(i,j1+1)=m
      l=ntotjj(i,j1)+1
      if (iplev) 215,215,206
  206 write (iw,20) i
   20 format (/20h0Levels of subshell ,i1)
      do 211 n=l,m
      j=nalsjp(n,i)
  211 write (iw,21)  ncfgjp(n,i),nalsjp(n,i),mult(j,i),lbcd(j,i),pj(n,i)
   21 format (i13,i4,i5,a1,f5.1)
c
c        Sort on decreasing pj
c
  215 m1=ntotjj(i,j1+1)-ntotjj(i,j1)
      call sort2(m1,2,pc(l),nalsjp(l,i),pj(l,i),idum,idum,dum,dum,
     1  dum,dum,dum,dum,dum,dum)
c
      if (iplev) 218,218,216
  216 write (iw,22) i
   22 format (/20h0Levels of subshell ,i1,9h (sorted))
      do 217 n=l,m
      j=nalsjp(n,i)
  217 write (iw,21)  ncfgjp(n,i),nalsjp(n,i),mult(j,i),lbcd(j,i),pj(n,i)
c
c        Calc. number of levels of each pj
c
  218 k=ndiffj(i)+1
      njk(k,i)=1
      l=l+1
      if (m-l) 224,220,220
  220 do 223 j=l,m
      if (pj(j,i)-pj(j-1,i)) 222,221,222
  221 njk(k,i)=njk(k,i)+1
      go to 223
  222 k=k+1
      njk(k,i)=1
  223 continue
  224 ndiffj(i)=k
  225 sum=sum+pj(l-1,i)
      if (sum.gt.sumx) sumx=sum
  229 continue
c
      if (sumx.eq.sj8mx1) go to 256
      write (iw,23) sj8mx1,sumx
   23 format (///15h0maximum scrj8=,f5.1,10h  from ls ,
     1  10hcalc, and=,f5.1, 14h  from jj calc
     2  /52h   (ls.lt.jj is ok if j values have been restricted)
     3  /52h    (jj.eq.0 is ok if ls terms have been restricted))
  256 do 270 i=1,ks
      if (i.le.nosubc.and.nolskp.eq.0) go to 270
      do 260 j=1,jx
      if (i.le.nosubc) go to 258
      notsj1(i,j+1)=j
  258 ntotjj(i,j)=j
      nototj(i)=j
      ndiffj(i)=j
      njk(j,i)=1
      nalsjp(j,i)=j
  260 pj(j,i)=0.0
  270 continue
c
  300 ierror=0
      maxm=0
      maxk=0
      do 310 i=1,ks
      maxm=max0(maxm,nototj(i))
  310 maxk=max0(maxk,ndiffj(i))
      if (notott.gt.klsp.or.ndifft.gt.klsp) ierror=7
      if (maxm.gt.kjp.or.maxk.gt.kjk) ierror=7
      if (notsx.gt.klsc) ierror=7
      write (iw,30) kk,klsp,notott,klsp,ndifft,kjp,(nototj(i),i=1,ks),
     1  kjk,(ndiffj(i),i=1,ks),klsc,notsx
   30 format (///25h maximum subscript values,', parity number',i2//
     1  32h nalsp(m,i), pscrl(m,i),   klsp=,i4,15h   m=notott=   ,i5/
     2  17h ntrmk(k), lf(k),, 10x,5hklsp=,i4,15h   k=ndifft=   ,i5/
     3  32h nalsjp(m,i), pj(m,i),     kjp= ,i4,15h   m=nototj(i)=,8i5/
     4  10h njk(k,i),,17x,5hkjk= ,i4,3x,12hk=ndiffj(i)=,8i5/
     5  16h fl(m,i),s(m,i),,11x,5hklsc=,i4,14h   mmax=notsx=,i6)
      jx=jx+1
      write (iw,31) (ntottj(j), j=1,jx)
   31 format (/8h0ntottj=,20i6/(8x,20i6))
      write (iw,32) (ntrmk(k), k=1,ndifft)
   32 format (/8h0 ntrmk=,20i6/(8x,20i6))
      if (ierror.eq.0) go to 400
      write (iw,33)
   33 format (//25h***** dimensions exceeded)
      if (iw6.ge.0) go to 399
      write (6,30) kk,klsp,notott,klsp,ndifft,kjp,(nototj(i),i=1,ks),
     1  kjk,(ndiffj(i),i=1,ks),klsc,notsx
      write (6,33)
  399 stop '(plev 33)'
  400 return
      end
