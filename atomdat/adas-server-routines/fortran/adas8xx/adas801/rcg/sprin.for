C UNIX-IDL - SCCS info: Module @(#)sprin.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Added a call to sprnadd.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
      subroutine sprin
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2a/sj8k(2),notsjp(ks,100),ncfgp(kmx),nalsp(kmx,ks)
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/vect(kmx,kmx)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      dimension tmxp(kmx,kmx)
      equivalence (vect,tmxp)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      character*8 idelem(11)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 elem(3,kc,2),elem1(3),d,e,blank
      common/char2/elem,elem1
      data blank/'        '/
c
c
c          Print coef, mupole, energy, or eigenvector matrix.
c               If necessary, transform and print again.
c               For mupole elements, also print squares.
c          kpar=-1,0,+1,+2 for Fk,Gk,Zeta,mupole,respectively.
c          kpar=-2 for rk
c          kpar=+3,+4 for energy or eigenvector matrix, respectively.
c
c
c     ks=8
      k=kk
      j1x=nsconf(2,k)
      jab=max0(j1b-j1a,1)
      itest=0
  100 if (kpar-2) 101,108,108
  101 if (icfc.eq.3.and.kpar.ne.-2) itest=1
      if (icfc.eq.4.and.kpar.eq.-2) itest=1
      if (icfc.gt.4) itest=1
      if (itest.gt.0) write (iw,10)
   10 format (1h2)
  108 ip=1
      kcpl1=1
      if (kpar-1) 121,115,170
  115 kcpl1=kcpl
      go to 121
c
  120 if (kpar-1) 121,121,170
  121 nlsp=nls
      if (itest.eq.0) go to 200
      write (iw,14) parnam(npar,k),cave,coupl(kcpl1),scrj8,
     1  (jn, (llijk(l,jn,k),nijk(l,jn,k), l=1,ks), jn=j1a,j1b,jab)
   14 format (1h0,a10,6x,5hcave=,f9.5,10x,a6,9h coupling,8x,2hj=,f4.1,
     1  10x,6hConfig,i3,8(2x,a1,i2)/84x,i3,8(2x,a1,i2))
      write (iw,15)
   15 format (1h0)
      go to 200
  170 if (kpar-3) 171,180,190
  171 ip=2
      i12=1
      jx=max0(nsconf(2,i1),nsconf(2,i2))
      jxm1=min0(jx-1,8)
      if (idip.eq.0) go to 200
      if (idip.gt.1) write (iw,10)
  172 do 174 j=1,jx
      if (j.gt.jxm1.and.j.lt.jx) go to 174
      write (iw,16) i12,ix, j,(llijk(l,j,i1),nijk(l,j,i1),l=1,ks),scrj8,
     1  j,(llijk(l,j,i2),nijk(l,j,i2), l=1,ks),scrj8p,coupl(kcpl1)
  174 continue
   16 format (7h mup mx,i3,4h ii=,i1,i3,8(2x,a1,i2),3x,2hJ=,
     1  f4.1,4h ---,i3,8(2x,a1,i2),3x,3hJp=,f4.1,2x,a6,4h cpl)
      write (iw,15)
      go to 200
c
  180 kcpl1=kcpl
      nlst=nls
      if (iengyd.gt.2) nls=min0(nls,11*iengyd)
      nlsp=nls
      jxj=j1x
      i=5
      if (iengyd.lt.200) i=1
      if (scrj8.ne.sj8mn) jxj=min0(jxj,i)
      write (iw,18)  coupl(kcpl1),scrj8,   ((elem(i,jn,k), i=1,3),
     1  jn, (llijk(l,jn,k),nijk(l,jn,k), l=1,ks), jn=1,jxj)
   18 format (15h1 Energy matrix,4h   (,a6,9h coupling,1h),7x,
     1  2hJ=,f4.1,7x,3a6,5x,6hConfig,i3,8(2x,a1,i2)/
     2  (55x,3a6,11x,i3,8(2x,a1,i2)))
      if (jxj.lt.j1x) write (iw,13)
     1  (elem(i,j1x,k),i=1,3), j1x,(llijk(l,j1x,k),nijk(l,j1x,k),l=1,ks)
   13 format (55x,3a6,11x,i3,8(2x,a1,i2))
      write (iw,15)
      if (iengyd-2) 500,200,200
c
  190 kcpl1=kcpl
      nlsp=nls
  191 if (kcpl1-1) 196,192,196
c                Calculate g-factors
  192 m=nosubc
      if (scrj8.le.0.0) go to 196
      do 193 l=1,nls
  193 ct4(l,1)=0.50116*(scrs(l,m)**2+scrs(l,m)-scrl(l,m)**2-scrl(l,m))/
     1     (scrj8**2+scrj8)+1.50116
      do 194 n=1,nls
      ct4(n,2)=0.0
      do 194 l=1,nls
  194 ct4(n,2)=ct4(n,2)+ct4(l,1)*(c(l,n)**2)
      write  (iw,20) (ct4(n,2), n=1,nls)
   20 format (//11h   g-values/(28x,11f9.3))
  196 if (kcpld(kcpl1).gt.0) go to 281
      write (iw,19) coupl(kcpl1)
   19 format (//15h   Eigenvectors,4h   (,a6,9h coupling,1h)/)
c
  200 if (kpar-2) 201,202,205
  201 if (itest) 282,282,205
  202 if (idip-1) 282,282,205
  205 lx=0
  210 ln=lx+1
      lx=lx+11
      if (lx-nlsp) 212,212,211
  211 lx=nlsp
  212 if (kpar-4) 213,245,245
  213 if (kpar.eq.2) go to 215
      write (iw,24) (ncfg(l), l=ln,lx)
      go to 216
  215 write (iw,24) (ncfgp(l), l=ln,lx)
  216 if (kcpl1-1) 230,220,230
  220 write (iw,22) (fidls(l,ip), l=ln,lx)
   22 format (28x, 11(2x,a7,:))
      go to 240
  230 write (iw,23) (fidjj(l,ip), l=ln,lx)
   23 format (28x, 11(1x,a8,:))
  240 write (iw,24) (l, l=ln,lx)
   24 format (26x,11i9)
      if (kpar-3) 250,290,245
  245 write (iw,24)
      if (kcpl1.ne.kcpl) go to 248
      do 246 l=ln,lx
      n=lcser(l)
c 246 encode (8,28,idelem(l-ln+1)) elem(2,n,k),elem(3,n,k)
  246 write (idelem(l-ln+1),28) elem(2,n,k),elem(3,n,k)
   28 format (a6,a2)
      n=(ln+11)/11
      write (iw,25) n, (idelem(l-ln+1),l=ln,lx)
   25 format (i21,9x,11(a8,1x))
      write (iw,29) (ldes(l),l=ln,lx)
   29 format (30x,11(a8,1x,:))
  248 if (ln.gt.nevmax) go to 281
c
c          Print coef, dipole, or eigenvector matrix
c
  250 do 280 l=1,nls
      n=ncfg(l)
      d=elem(2,n,k)
      e=elem(3,n,k)
      if (kpar.gt.2) go to 255
      d=blank
      e=d
  255 if (kcpl1-1) 260,260,270
  260 write (iw,26) n,d,e, fidls(l,1), l, (c(l,lp), lp=ln,lx)
   26 format (i3,1h:,2a6, 1x,a7, i3,1x,11f9.5)
      go to 280
  270 write (iw,27) n,d,e, fidjj(l,1), l, (c(l,lp), lp=ln,lx)
   27 format (i3,1h:,2a6, 1x,a8, i3,11f9.5)
  280 continue
      if (lx-nlsp) 210,281,281
  281 write (iw,27)
      if (kpar-4) 282,283,283
  282 if (kcpl1-kcpl) 300,400,300
  283 if (nls-1) 288,288,1283
 1283 pur=0.0
      do 286 n=1,nls
      ct4(n,1)=0.0
      do 285 l=1,nls
      eras=c(l,n)**2
      if (eras-ct4(n,1)) 285,285,284
  284 ct4(n,1)=eras
  285 continue
  286 pur=pur+ct4(n,1)
      fnls=nls
      avp(kcpl1,k)=avp(kcpl1,k)+pur
      fnt(kcpl1,k)=fnt(kcpl1,k)+fnls
      pur=pur/fnls
      nnn=min0(nls,nevmax)
      write (iw,30) pur, (ct4(n,1), n=1,nnn)
   30 format (13x,8h Purity=,f5.3,2x,11f9.5/(28x,11f9.5))
   


C------Start ADAS conversion-------
       
       call sprnadd(1)

C------End ADAS conversion-------
   
  288 if (nolskp.ne.0) go to 398
      if (kcpl1-kcpl) 400,289,400
  289 if (nls-1) 400,400,300
c
c          Print energy matrix
c
  290 eras=c(1,1)
      do 298 l=1,nls
      n=ncfg(l)
      d=elem(2,n,k)
      e=elem(3,n,k)
      if (kcpl1-1) 291,291,295
  291 if (eras.gt.9999.0) go to 292
      write (iw,31) n,d,e, fidls(l,1), l, (c(l,lp), lp=ln,lx)
   31 format (i3,1h:,2a6, 1x,a7, i3,1x,11f9.3)
      go to 298
  292 write (iw,32) n,d,e, fidls(l,1), l, (c(l,lp), lp=ln,lx)
   32 format (i3,1h:,2a6, 1x,a7, i3,1x,11f9.2)
      go to 298
  295 if (eras.gt.9999.0) go to 296
      write (iw,33) n,d,e, fidjj(l,1), l, (c(l,lp), lp=ln,lx)
   33 format (i3,1h:,2a6,  1x,a8, i3,11f9.3)
      go to 298
  296 write (iw,34) n,d,e, fidjj(l,1), l, (c(l,lp), lp=ln,lx)
   34 format (i3,1h:,2a6,  1x,a8, i3,11f9.2)
  298 continue
      if (lx.ge.11*iengyd) go to 299
      if (lx-nlsp) 210,299,299
  299 write (iw,34)
      nls=nlst
      go to 500
c
c          Transform matrix
c
  300 if (kcpl1-1) 305,305,355
  305 do 320 l=1,nls
      do 320 lp=1,nlsp
      ct4(l,lp)=0.0
      do 310 l0=1,nls
  310 ct4(l,lp)=ct4(l,lp)+tmx(l0,l)*c(l0,lp)
  320 continue
      if (kpar-2) 331,321,396
  321 do 330 l=1,nls
      do 330 lp=1,nlsp
      c(l,lp)=0.0
      do 325 l0=1,nlsp
  325 c(l,lp)=c(l,lp)+ct4(l,l0)*tmxp(l0,lp)
  330 continue
      go to 395
  331 do 340 l=1,nls
      do 340 lp=1,l
      c(l,lp)=0.0
      do 335 l0=1,nls
  335 c(l,lp)=c(l,lp)+ct4(l,l0)*tmx(l0,lp)
  340 c(lp,l)=c(l,lp)
      go to 395
c
  355 do 370 l=1,nls
      do 370 lp=1,nlsp
      temp=0.0
      if (iabs(kpar).gt.1) go to 356
      if (ncfg(l).ne.ncfg(lp)) go to 365
  356 do 360 l0=1,nls
      eras=tmx(l,l0)
      if (eras.eq.0.0) go to 360
      temp=temp+eras*c(l0,lp)
  360 continue
  365 ct4(l,lp)=temp
  370 continue
      if (kpar-2) 381,371,397
  371 do 380 l=1,nls
      do 380 lp=1,nlsp
      c(l,lp)=0.0
      do 375 l0=1,nlsp
  375 c(l,lp)=c(l,lp)+ct4(l,l0)*tmxp(lp,l0)
  380 continue
      go to 395
  381 do 390 l=1,nls
      do 389 lp=1,l
      temp=0.0
      if (iabs(kpar).gt.1) go to 383
      if (ncfg(l).ne.ncfg(lp)) go to 386
  383 do 385 l0=1,nls
      eras=tmx(lp,l0)
      if (eras.eq.0.0) go to 385
      temp=temp+ct4(l,l0)*eras
  385 continue
  386 c(l,lp)=temp
  389 c(lp,l)=temp
  390 continue
c
  395 if (kpar.lt.2.and.itest.gt.0) write (iw,35)
      if (kpar.eq.2.and.idip.gt.1) write (iw,35)
   35 format (1h0)
      kcpl1=kcpl
      go to 120
  396 kcpl1=2
      go to 398
  397 kcpl1=1
c               Store eigenvectors in coupling kcpl
  398 do 399 l=1,nls
      do 399 lp=1,nls
      vect(l,lp)=c(l,lp)
  399 c(l,lp)=ct4(l,lp)
      if (nolskp.eq.0) go to 191
c
  400 if (kpar-2) 500,405,500
  405 if (i12-2) 410,500,500
  410 write (ic) scrj8,scrj8p,nls,nlsp, ((c(l,lp),l=1,nls), lp=1,nlsp),
     1  ncfg,ncfgp
      noicwr=noicwr+1
      noicmup(kk)=noicmup(kk)+1
      if (idip.eq.0) go to 500
      do 430 l=1,nls
      do 430 lp=1,nlsp
  430 c(l,lp)=c(l,lp)**2
      write (iw,35)
      i12=2
      go to 172
c
  500 if (kpar.lt.2.and.itest.eq.0) go to 600
      call clock(time)
      delt=time-time0
      write (iw,50) delt
   50 format (11x,'time=',f6.3,4h min)
      if (kpar.eq.2.and.iw6.lt.0) write (6,51) scrj8,scrj8p,delt,
     1  nls,nlsp
   51 format (' Finished mupole matrix for  J=',f3.1,'  and Jp=',
     1   f3.1,'  at time=',f6.3,4h min,',     Matrix size=',i4,
     2   ' by ',i4)
      if (kpar.eq.4.and.iw6.lt.0) write (6,52) scrj8,delt,nls
   52 format (' Finished energy for  J=',f3.1,
     1   '  at time=',f6.3,4h min,',     Matrix size=',i4)
      if (kpar-4) 600,501,501
  501 continue
      call sprn37
  600 return
      end
