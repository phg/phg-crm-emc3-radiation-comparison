C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  11-06-2003 : lcser in /ener/ had hardwired dimension. 
C               Replace with kmx.
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------

      subroutine elecden(kkk,scrj1,igen)
c
c     Program to compute radial electron-density distribution for
c          each energy-level eigenvector (for diagonal contributions
c          only if igen=3, and including cross terms as well if igen=4).
c          Diagonal terms are computed for all configurations.  Cross terms
c          are computed only for certain pairs of configurations (see
c          comment cards following statements 470 and 530 below).
c     One-electron, single configuration radial wavefunctions Pnl are obtained
c          from the disk file "tape2n" written by program RCN, as are the 
c          orbital quantum numbers nnm and llm, the occupation numbers wwnl,
c          the number of orbitals nscpvs=norb, and the number of radial 
c          mesh points nkkk(m) used for orbital number m.
c     Subroutine elecden is called for each J and parity kkk from statement
c          398 of subroutine energy.  From disk unit ie written in energy are
c          read the value sj=scrj1 of J, the matrix size nls, the eigenvalues,
c          and for each eigenvector lp the serial number lcser and LS
c          designation ldes corresponding to the dominant eigenvector
c          component l.  The eigenvector matrix v(l,lp) and the basis-function
c          LS labels fidls(l,kkk) are in common blocks.
c     The variable rsatom(r)=sum over all electrons of (Pnl(r)**2) 
c          [in the nomenclature of RCN] when multiplied by dr is the number 
c          of electrons in a shell of radius r and thickness dr.  
c          Thus the true spherically averaged electron density at radius r
c          is  rsatom(r)/(4*pi*(r**2)).
c     rsatom(r,c) is the pure-configuration density at radius r (in Bohr units)
c          for configuration c, and rsatomlp(r,lp) is the density for eigenstate
c          (multi-configuration, intermediate- coupling eigenvector) lp.
c
c     Code corrected December 12, 2001 to include at statement 543 a factor
c          sqrt(occupancy of subshell from which electron is being excited),
c          a weighting factor arising from antisymmetrization of the ket
c          function.  [See Eqs. (9.75) and (13.53) of TASS.]        
c          
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc2/v(250,250)
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 elem(3,kc,2),elem1(3)
      common/char2/elem,elem1
      dimension cfp(kls1,kls1,kc)
c
      parameter (kmsh=1801,ko=20)
      dimension rsatom(kmsh,kc),rsatomlp(kmsh,kmx),frac(kc,kmx),
     1  pnlm(kmsh,kc),nm(kc),lm(kc)
      dimension noccss(kc),ifirst(kc),ilast(kc),nterms(kc),
     1  nparents(kc),ncfp(kc),iinclude(kc),nshells(kc)
      common/c1n/r(kmsh),ru(kmsh),ruee(kmsh),ruexch(kmsh),
     1     nnlz(ko),nnm(ko),
     2  a0m(ko),jjj(ko),wwnl(ko),llm(ko),nkkk(ko),ee(ko)
      character*8 nlbcd(ko),ldeslm(20,20,kc),ldesj
      character conf(3)*6
      common/charn/conf,nlbcd
      common/c3n/vprm(50)
      common/pq/pnl(kmsh,ko)
c
      open(3,file='tape2n',status='old',form='unformatted')
      open(4,file='elecden',status='new')
c
      rewind 3
c          calculate total "electron density" for each pure configuration j
  100 nc=nsconf(2,kkk)
      nkkkx=0
      do 150 j=1,nc
      read (3) nnm,llm,wwnl,ncspvs,vprm,conf,nconf,iz,z,ion,irel,hxid,
     1  mesh,c,idb,exf,corrf,kut,npardum,nlbcd,nnlz,nkkk,ee,ee8,norb,
     2  r,ru,((pnl(i,m),i=1,kmsh),m=1,norb),iw6
      if (norb.ne.ncspvs) then
      write (6,10)
   10 format (//' Not all Pnl were written on tape2n;'/
     1   '       column 5 of the RCN control card must be 9.')
      stop '( error at statement 10 of elecden)'
      end if
      do 109 i=1,nkkk(ncspvs)
      rsatom(i,j)=0.0
      do 105 m=1,ncspvs
  105 rsatom(i,j)=rsatom(i,j)+wwnl(m)*pnl(i,m)**2
  109 pnlm(i,j)=pnl(i,ncspvs)
      nm(j)=nnm(ncspvs)
      lm(j)=llm(ncspvs)
      nkkkx=max(nkkkx,nkkk(ncspvs))
  150 continue
c
c          for each eigenvector lp, calculate total weighting factor 
c               frac(j,lp) for each configuration j and eigenvector lp,
c               and calculate weighted rsatom (diagonal terms only)
      rewind ie
      do 204 n=1,50
      read (ie,end=400) sj,nls, (eig(lp),(v(l,lp),l=1,nls), lp=1,nls),
     1  lcser,ldes
      read (ie)
      if (sj.eq.scrj1) go to 205
  204 continue
  205 do 250 lp=1,nls
      do 210 j=1,nc
  210 frac(j,lp)=0.0
      do 220 l=1,nls
      j=ncfg(l)
      frac(j,lp)=frac(j,lp)+v(l,lp)**2
  220 continue
      do 230 i=1,nkkkx
      rsatomlp(i,lp)=0.0
      do 225 j=1,nc
  225 rsatomlp(i,lp)=rsatomlp(i,lp)+frac(j,lp)*rsatom(i,j)
  230 continue
  250 continue
c          Write configuration fractions for each eigenvector lp
      write (6,24) sj,nls
   24 format (/' J=',f5.1,'   nls=',i4,
     1    '    config frac for each eigenvector')
      write (6,25) (ldes(lp),lp=1,nls)
   25 format (13x,10(a8,4x)/(16x,10(a8,4x)))
      do 270 j=1,nc
      write (6,28) elem(2,j,kkk),(frac(j,lp), lp=1,nls)
   28 format (1x,a8,10f12.6/(12x,10f12.6))
  270 continue
c          write rsatom for each pure configuration
  400 write (iw,40) (j, j=1,nc)
   40 format (/'  rsatom(i),  pure configurations'/
     1  ' conf. no.=     ',10(i2,8x)/(17x,10(i2,8x)))
      write (iw,41) (elem(2,j,kkk), j=1,nc)
   41 format ('   r (ao)    ',10a10/(18x,10a10))
      do 420 i=1,nkkkx,40
  420 write (iw,42) r(i),(rsatom(i,j), j=1,nc)
   42 format (f9.4,2x,10f10.5/(14x,10f10.5))
c          write rsatomlp for each eigenvector lp (diag. contributions only)
      write (iw,44) sj,(eig(i),i=1,nls)
   44 format (/'  rsatomlp(i), level dependent,  diagonal terms only'/
     1   ' J=',f4.1,'  E=',10f9.4/(14x,10f9.4))
      write (iw,45) (elem(2,lcser(i),kkk),i=1,nls)
   45 format (13x,10a9/(16x,10a9))
      write (iw,46) (ldes(i),i=1,nls)
   46 format ('   r (ao)   ',10a9/(15x,10a9))
      do 470 i=1,nkkkx,40
  470 write (iw,47) r(i),(rsatomlp(i,lp),lp=1,nls)
   47 format (f9.4,2x,10f9.5/14x,10f9.5)
c
c          If igen=4, add on contributions to rsatomlp from cross terms
c
      if (igen.eq.3) go to 600
      nxterms=0
c          For 1x1 matrix, there is no CI and hence there are no cross terms.
c          For only one partially filled subshell in the problem, 
c             it is either singly occupied or there is only one configuration;
c             in either case there is no CI and so no cross terms.
      if (nls.eq.1.or.nsconf(1,kkk).eq.1) go to 600
c          Calculate no. of partially filled subshells for conf. j,
c          and store occupancies nshells(j) for all but the outermost
c          electron.  Do not include cross terms [iinclude(j)=0] involving a
c          configuration j having more than two occupied subshells other than s2 
c          [noccss(j).gt.2], nor a configuration with d or f subshell 
c          having more than two electrons or holes, nor one with a multiply 
c          occupied subshell outside the first non-filled one.
      do 530 j=1,nc
      noccss(j)=0
      ifirst(j)=0
      ilast(j)=0
      iinclude(j)=1
      do 505 i=1,nsconf(1,kkk)
      nnn=nijk(i,j,kkk)
      lll=fllijk(i,j,kkk)
      if (lll.eq.2.and.(nnn.gt.2.and.nnn.lt.8)) then
          iinclude(j)=0
          go to 506
      end if
      if (lll.eq.3.and.(nnn.gt.2.and.nnn.lt.12)) then
          iinclude(j)=0
          go to 506
      end if
      if (nnn.eq.0) go to 505
      ilast(j)=i
      if (nnn.eq.2.and.lll.eq.0) go to 505
      noccss(j)=noccss(j)+1
         if (noccss(j).eq.1.and.ifirst(j).eq.0) then
            ifirst(j)=i
         end if
      if (i.gt.ifirst(j).and.nnn.gt.1) iinclude(j)=0
  505 continue
      if (noccss(j).gt.2) iinclude(j)=0
  506 continue
      nshells(j)=0
      do 507 i=1,ilast(j)
      nnn=nijk(i,j,kkk)
      if (i.eq.ilast(j)) nnn=nnn-1
  507 nshells(j)=nshells(j)+nnn*(100**(i-1))
c
c          Form basis-state designations ldeslm(m,n,j) 
c                for term m (m.le.nlt) and parent n (n.le.nlp)  
c                of configuration j with a multiply occupied subshell.
c
      ncfp(j)=1
c          If the only occupied subshell (other than s2) 
c               is multiply occupied, make a cfp expansion.
c          [Note: nlt is found by subroutine locdsk.]
      if (noccss(j).ne.1.or.nijk(ifirst(j),j,kkk).le.1) go to 530
      call locdsk(id2,fllijk(ifirst(j),j,kkk),nijk(ifirst(j),j,kkk))
      read (id2) (mult(m,1),lbcd(m,1),ialf(m,1),fl(m,1),s(m,1),
     1  m=1,nlt), nlp,nlgp,nocfp,nocfgp
      ncfp(j)=nlt*nlp
      nterms(j)=nlt
      nparents(j)=nlp
      read (id2) ((cfp(m,n,j),m=1,nlt), n=1,nlp)
      read (id2) (mult(n,2),lbcd(n,2),ialf(n,2),fl(n,2),s(n,2),n=1,nlp)
      do 510 m=1,nlt
      i=fl(m,1)
      lbcd(m,1)=lsymb(i+25)
  510 continue
      do 520 n=1,nlp
      i=fl(n,2)
      lbcd(n,2)=lsymb(i+25)
  520 continue
      do m=1,nlt
      do n=1,nlp
      write (ldeslm(m,n,j),51) mult(n,2),lbcd(n,2),mult(m,1),lbcd(m,1)
   51 format ('(',i1,a1,') ',i1,a1)
      end do
      end do
  530 continue
c
      if (nls.eq.1) go to 600
c
c          In this section, l and m are two components of eigenvector lp,
c                and ncfg(l) is the configuration serial number
c                for component l.
c          Cross terms exist only between different configurations,
c                and then only if the outer electron in each has the
c                same orbital angular momentum lm.  Also, there must be
c                agreement for the LS term labels fidls(l,kkk)(6:7) 
c                and for the parent term labels fidls(l,kkk)(2:3) 
c                [or ldeslm(it,ip,j)(2:3) for a cfp expansion]
c                
      do 550 lp=1,nls
      do 548 l=1,nls-1
      jl=ncfg(l)
      if (iinclude(jl).eq.0) go to 548
      do 545 m=l+1,nls
      jm=ncfg(m)
      if (iinclude(jm).eq.0) go to 545
      if (nshells(jl).ne.nshells(jm)) go to 545
      if (jl.eq.jm) go to 545
      if (lm(jl).ne.lm(jm)) go to 545
      if (fidls(l,kkk)(6:7).ne.fidls(m,kkk)(6:7)) go to 545
      it=1
      ip=1
      jlm=1
      cfp(1,1,1)=1.0
      if (ncfp(jl)*ncfp(jm).eq.1) go to 543
      jlm=jl
      ldesj=fidls(m,kkk)
      if (ncfp(jl).eq.1) then
         jlm=jm
         ldesj=fidls(l,kkk)
      end if 
      do 542 it=1,nterms(jlm)
      do 542 ip=1,nparents(jlm)
      if (ldeslm(it,ip,jlm).eq.ldesj) go to 543
  542 continue
      go to 545
  543 temp=nijk(ifirst(j),j,kkk)
      temp=sqrt(temp)*2.0*cfp(it,ip,jlm)*v(l,lp)*v(m,lp)
          if (lp.eq.1) write (6,1538) l,m,lp,cfp(it,ip,jlm),
     1       v(l,lp),v(m,lp),temp  
 1538 format (' l,m,lp,cfp(it,ip,jlm),v(l,lp),v(m,lp),temp=',
     1            3i5,4f12.5)
      do 544 i=1,nkkkx
      rsatomlp(i,lp)=rsatomlp(i,lp)+temp*pnlm(i,jl)*pnlm(i,jm)
          if (lp.eq.1.and.i.eq.281) write (6,1544) 
     1      lp,jl,jm,i,temp,pnlm(i,jl),pnlm(i,jm),rsatomlp(i,lp)
 1544 format (' lp,jl,jm,i,temp,pnlm(i,jl),pnlm(i,jm),rsatomlp(i,lp)=',
     1  3i2,i4,4f10.6)
  544 continue
      nxterms=nxterms+1
  545 continue
  548 continue
  550 continue
      if (nxterms.eq.0) go to 600
c          write rsatomlp for each eigenvector lp (including cross terms)
      write (iw,55) sj,(eig(i),i=1,nls)
   55 format (/'  rsatomlp(i), level dependent,  including cross terms'/
     1   ' J=',f4.1,'  E=',10f9.4/(14x,10f9.4))
      write (iw,45) (elem(2,lcser(i),kkk),i=1,nls)     
      write (iw,46) (ldes(i),i=1,nls)
      do 570 i=1,nkkkx,40
  570 write (iw,47) r(i),(rsatomlp(i,lp),lp=1,nls)
c
  600 return
      end
