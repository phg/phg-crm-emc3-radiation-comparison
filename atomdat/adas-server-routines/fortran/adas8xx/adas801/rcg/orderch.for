C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/orderch.for,v 1.2 2004/07/06 14:26:05 whitefor Exp $ Date $Date: 2004/07/06 14:26:05 $
C
      subroutine orderch(n,y)
c
c          A table of n 8-character strings starting at y is ordered
c          according to a sequence table in the first n words of itmp,
c          which must be integer and 2n in length.
c
c          The variable y is character*8.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      dimension y(klam),yt(klam)
      character*8 y,yt
      integer t1,t2
c
      do 1 i = 1,n
      t1 = itmp(i)
      t2=i
    1 yt(t2) = y(t1)
      do 2 i = 1,n
      t2=i
    2 y(i) = yt(t2)
      return
      end
