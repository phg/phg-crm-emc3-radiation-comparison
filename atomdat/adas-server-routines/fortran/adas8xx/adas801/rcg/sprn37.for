C UNIX-IDL - SCCS info: Module @(#)sprn37.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
      subroutine sprn37
c
c          Print eigenvectors in representations 3-7
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/vect(kmx,kmx)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      character*8 elem(3,kc,2),elem1(3),d,e
      common/char2/elem,elem1
c
      k=kk
  100 if (nosubc-1) 940,940,101
  101 if (nls-1) 940,940,102
  102 if (nls-nlsmax) 103,103,940
  103 kcpl1=3
      iq=nosubc
      iqm1=iq-1
      iqm2=iq-2
c
  110 if (kcpld(kcpl1)) 150,150,900
  150 read (ic) ((tmx(l,lp), lp=1,nls),  fk(l),fkj(l),scrj(l,iqm1),
     1  scrj(l,iqm2),scrl6(l),fk6(l),scrs6(l),lhqq(l),multqq(l),l=1,nls)
      noicrd=noicrd+1
c
c          Transform eigenvectors
c
  200 do 210 l=1,nls
      do 210 lp=1,nls
      c(l,lp)=0.0
      do 205 i=1,nls
  205 c(l,lp)=c(l,lp)+tmx(i,l)*vect(i,lp)
  210 continue
c
c          Print eigenvectors in new coupling
c
      write (iw,22) coupl(kcpl1)
   22 format (//15h   Eigenvectors,4h   (,a6,9h coupling,1h)/)
c
      lx=0
  240 ln=lx+1
      lx=lx+11
      if (lx-nls) 242,242,241
  241 lx=nls
  242 do 250 l=1,nls
      n=ncfg(l)
      d=elem(2,n,k)
      e=elem(3,n,k)
      go to (243,243,243,244,245,246,247) kcpl1
  243 write (iw,23) n,d,e,scrj(l,iqm1),fkj(l),l,(c(l,lp),lp=ln,lx)
   23 format (i3,1h=,a6,a5,f4.1,1hk,f4.1,i3,1x,11f9.5)
      go to 250
  244 lp1=scrl(l,nosubc)+1.0
      lhs4=lsymb(lp1+24)
      write (iw,24) n,d,e,lhs4,fk(l),l,(c(l,lp),lp=ln,lx)
   24 format (i3,1h=,2a6,2x,a1,f4.1,i4,1x,11f9.5)
      go to 250
  245 write (iw,23) n,d,e,scrj(l,iqm1),fk(l),l,(c(l,lp),lp=ln,lx)
      go to 250
  246 write (iw,26) n,d,e,scrj(l,iqm2),lhqq(l),fk6(l),multqq(l),l,
     1  (c(l,lp), lp=ln,lx)
   26 format (i3,1h=,a6,a5,f4.1,a1,f3.1,i2,i3,11f9.5)
      go to 250
  247 write (iw,27) n,d,e,scrj(l,iqm2),multqq(l),lhqq(l),scrj(l,iqm1),
     1  l, (c(l,lp), lp=ln,lx)
   27 format (i3,1h=,a6,a5,f4.1,i2,a1,f3.1,i3,11f9.5)
  250 continue
      write (iw,23)
      if (lx.ge.nevmax) go to 300
      if (lx-nls) 240,300,300
c
c          Calculate purities
c
  300 if (nls.le.1) go to 900
      pur=0.0
      do 330 n=1,nls
      ct4(n,1)=0.0
      do 320 l=1,nls
      eras=c(l,n)**2
      if (eras-ct4(n,1)) 320,320,310
  310 ct4(n,1)=eras
  320 continue
  330 pur=pur+ct4(n,1)
      fnls=nls
      avp(kcpl1,k)=avp(kcpl1,k)+pur
      fnt(kcpl1,k)=fnt(kcpl1,k)+fnls
      pur=pur/fnls
      nnn=min0(nls,nevmax)
      write  (9,30) pur, (ct4(n,1), n=1,nnn)
   30 format (13x,8h Purity=,f5.3,2x,11f9.5/(15x,11f9.5))
c
c
  900 kcpl1=kcpl1+1
      if (kcpl1-4) 110,110,910
  910 if (kcpl1-6) 920,110,921
  920 if (iq-3) 930,110,110
  921 if (kcpl1-7) 110,110,930
  930 call clock(time)
      delt=time-time0
      write (iw,93) delt
   93 format (11x,5htime=,f6.3,4h min)
  940 return
      end
