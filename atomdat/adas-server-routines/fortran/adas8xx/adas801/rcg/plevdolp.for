C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/plevdolp.for,v 1.2 2004/07/06 14:28:23 whitefor Exp $ Date $Date: 2004/07/06 14:28:23 $
C
      subroutine plevdolp
c
c          15-fold do loop (101 lines) moved from subroutine plev
c               because of VAX-compiler limitations

      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/c3lc7/m,i,j,k,l,i1,j2,k1,l8,j1
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
c
c
c     ks=8
c     kjp=500
c     klsp=210
c     kjk=110
      es2=abs(s(i,1)-s(j,2))-1.0
      ns2=s(i,1)+s(j,2)-es2
      do 110 is2=1,ns2
      es2=es2+1.0
      el2=abs(fl(i,1)-fl(j,2))-1.0
      nl2=fl(i,1)+fl(j,2)-el2
      do 110 il2=1,nl2
      el2=el2+1.0
      es3=abs(es2-s(k,3))-1.0
      ns3=es2+s(k,3)-es3
      do 110 is3=1,ns3
      es3=es3+1.0
      el3=abs(el2-fl(k,3))-1.0
      nl3=el2+fl(k,3)-el3
      do 110 il3=1,nl3
      el3=el3+1.0
      es4=abs(es3-s(l,4))-1.0
      ns4=es3+s(l,4)-es4
      do 110 is4=1,ns4
      es4=es4+1.0
      el4=abs(el3-fl(l,4))-1.0
      nl4=el3+fl(l,4)-el4
      do 110 il4=1,nl4
      el4=el4+1.0
      es5=abs(es4-s(i1,5))-1.0
      ns5=es4+s(i1,5)-es5
      do 110 is5=1,ns5
      es5=es5+1.0
      el5=abs(el4-fl(i1,5))-1.0
      nl5=el4+fl(i1,5)-el5
      do 110 il5=1,nl5
      el5=el5+1.0
      es6=abs(es5-s(j2,6))-1.0
      ns6=es5+s(j2,6)-es6
      do 110 is6=1,ns6
      es6=es6+1.0
      el6=abs(el5-fl(j2,6))-1.0
      nl6=el5+fl(j2,6)-el6
      do 110 il6=1,nl6
      el6=el6+1.0
      es7=abs(es6-s(k1,7))-1.0
      ns7=es6+s(k1,7)-es7
      do 110 is7=1,ns7
      es7=es7+1.0
      el7=abs(el6-fl(k1,7))-1.0
      nl7=el6+fl(k1,7)-el7
      do 110 il7=1,nl7
      el7=el7+1.0
      es8=abs(es7-s(l8,8))-1.0
      ns8=es7+s(l8,8)-es8
      do 110 is8=1,ns8
      es8=es8+1.0
      el8=abs(el7-fl(l8,8))-1.0
      nl8=el7+fl(l8,8)-el8
      do 110 il8=1,nl8
      el8=el8+1.0
      if (abs(el8-es8).gt.sjx) go to 110
      if (el8+es8.lt.sjn) go to 110
      if (nolskp.le.0) go to 100
      if (j1.lt.nclskp(kk)) go to 100
      do 95 ii=1,nolskp
      if (el8.eq.scrlkp(ii,kk).and.es8.eq.scrskp(ii,kk)) go to 100
   95 continue
      go to 110
  100 m=m+1
      go to (108,107,106,105,104,103,102,101) nosubc
  101 nalsp(m,8)=l8
      pscrs(m,8)=es8
      pscrl(m,8)=el8
  102 nalsp(m,7)=k1
      pscrs(m,7)=es7
      pscrl(m,7)=el7
  103 nalsp(m,6)=j2
      pscrs(m,6)=es6
      pscrl(m,6)=el6
  104 nalsp(m,5)=i1
      pscrs(m,5)=es5
      pscrl(m,5)=el5
  105 nalsp(m,4)=l
      pscrs(m,4)=es4
      pscrl(m,4)=el4
  106 nalsp(m,3)=k
      pscrs(m,3)=es3
      pscrl(m,3)=el3
  107 nalsp(m,2)=j
      pscrs(m,2)=es2
      pscrl(m,2)=el2
  108 nalsp(m,1)=i
      pscrs(m,1)=s(i,1)
      pscrl(m,1)=fl(i,1)
      ncfgp(m)=j1
      a=0.0
      prod=1.0
      eras=0.02
      do 109 ii=1,nosubc
      b=nalsp(m,ii)
      a=a+prod*b
      if (ii.gt.4) eras=0.1
  109 prod=prod*eras
      pc(m)=-(25.0*pscrs(m,nosubc)+pscrl(m,nosubc))+0.002*a
  110 continue
      return
      end
