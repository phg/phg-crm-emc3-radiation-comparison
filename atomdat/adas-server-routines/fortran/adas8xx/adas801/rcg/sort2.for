C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/sort2.for,v 1.2 2004/07/06 15:20:31 whitefor Exp $ Date $Date: 2004/07/06 15:20:31 $
C
      subroutine sort2(n,l,x,y1,y2,y3,y4,y5,y6,y7,y8,y9,y10,y11,y12)
c
c         n numbers in table x are sorted numerically increasing in
c         value. Then l tables of y are reordered to correspond to their
c         associated x. Temporary table t must be 2n words long.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      dimension x(klam),y1(klam),y2(klam),y3(klam),y4(klam),
     1  y5(klam),y6(klam),y7(klam),y8(klam),y9(klam),y10(klam),
     2  y11(klam),y12(klam)
      integer y1,y3,y4
c
      if ( n.le.1 ) return
      do 5 i = 1,n
    5 itmp(i) = i
      j = 1
    6 if (x(j).le.x(j+1)) go to 8
      m = j
      eras   = x(m+1)
    7 x(m+1) = x(m)
      itmp(m+1) = itmp(m)
      m = m - 1
      if ( m.eq.0 ) go to 9
      if ( x(m).gt.eras   ) go to 7
    9 x(m+1) = eras
      itmp(m+1) = j + 1
    8 j = j + 1
      if ( j.lt.n ) go to 6
c          Commence ordering of associated tables of y.
      if (l.eq.0) go to 100
      go to (101,102,103,104,105,106,107,108,109,110,111,112), l
  112 call order2(n,y12)
  111 call order2(n,y11)
  110 call order2(n,y10)
  109 call order2(n,y9)
  108 call order2(n,y8)
  107 call order2(n,y7)
  106 call order2(n,y6)
  105 call order2(n,y5)
  104 call orderi(n,y4)
  103 call orderi(n,y3)
  102 call order2(n,y2)
  101 call orderi(n,y1)
  100 return
      end
