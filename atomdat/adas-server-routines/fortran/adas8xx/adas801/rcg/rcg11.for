C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/rcg11.for,v 1.3 2004/07/06 14:43:53 whitefor Exp $ Date $Date: 2004/07/06 14:43:53 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase ktran to 30000. 
C               Extend /pwborn/ common block.
C               Add in code for reading in user supplied temperatures.
C  13-06-2000 : Remove spurious i1 in writing tev to collision file. 
C
C  Martin O'Mullane 
C
C  VERSION : 1.2
C  DATE    : 05-10-2000
C  MODIFIED: Martin O'Mullane
C             -  Removed spurious i1 in writing tev to collision file.
C  
C  VERSION : 1.3
C  DATE    : 17-06-2003
C  MODIFIED: Martin O'Mullane
C             - Output ktran at start of run to allow for dimension
C               checking in interactive ADAS801.
C  
C-----------------------------------------------------------------------
c     overlay(g9,0,0)
      program rcg11
c     program rcg11(ing11,tape10=ing11,outg11,tape9=outg11,
c    1  tape3,tape20,tape31,tape32,tape41,
c    2  tape72,tape73,tape74,tape2e,tape2=tape2e,tape19,
c    3  outgine,tape11=outgine,tape13,tape5)
c
c     Program copyright 1980, Los Alamos National Laboratory and
c          U.S. Government.  This program may be freely distributed
c          gratis any place in the world, but may not be sold.
c
c
c
c          RCG-Vers. 11.1.6 (December 1998), main program
c
c
c          RCG is a program to compute coefficient matrices for
c               atomic energy-level parameters (fk, gk, zeta, alpha,
c               beta, gamma, t, rk), read parameter values,
c               and compute and diagonalize the energy matrices
c               to obtain energy levels and eigenvectors.
c          Also, if desired, to compute angular coefficient matrices
c               for magnetic dipole and
c               for electric dipole and quadrupole transitions,
c               read radial multipole integrals, and calculate
c               oscillator strengths and transition probabilities.
c
c          First version (RCG Mod 0) coded by B. Fagan and R. D. Cowan,
c               Los Alamos Scientific Laboratory, summer and fall 1964.
c
c          RCG Mod 5 (including configuration interaction)
c               coded fall, 1968 by R. D. Cowan for the IBM 7030 (Stretch).
c               --- modified for the CDC 6500
c               at Purdue University, March 1971.  Converted July-November
c               1971 to LASL CDC 7600, using large core memory.
c               Dimensions increased to 8 subshells, February 1975.
c               Autoionization trans.-prob. calcs. added spring 1975.
c               plane-wave-born collision-strength options added
c               Jan-Feb 1980 by W. D. Robb and R. D. Cowan.
c          modified to allow different l for final subshell, July 1981,
c               and to accept rescaling cards, October 1981.
c          Mod 10:
c               Faster 3nj routines incorporated July 21, 1983.
c               Modified for CRAY 1 computers, December 1983.
c               Modified for Zeeman-Lab. (Amsterdam) CYBER 170/750, Feb. 1984.
c               Option to truncate to given set of LS values added June 1984.
c               Write on tape11 of RCE input added at NBS (now NIST) May 1986.
c               Changes in action of print-control variables ipct
c                    and ictc made at Zeeman Lab., December 1987.
c
c               A version for the VAX 8200 made at Lund University (Sweden),
c                    March-April 1988, incorporating numerous minor
c                    additions and improvements.
c               Reconverted to the CRAY
c                    at LANL fall 1988, leaving comment cards for easy
c                    reconversion to the VAX.  Encode and decode
c                    statements removed by changing many variables to
c                    character strings, January 1989.
c               This version (Mod 11, 1992) has commons modified so that a
c                    common of given name has the same length in all
c                    routines.  New timing subroutine seconds coded
c                    with alternative sections available for CRAY, VAX
c                    or Macintosh, SUN, and IBM System/6000 RISC.
c               August 1995, code changed to write on unit 41 only non-
c                    zero coefficient matrix elements instead of entire
c                    upper triangle of coefficient matrix--ditto on tape2e.
c                    (With algorithm used for illp, kmx is limited to 9999.)
c                    Also, in calcv and rceinp, fixed bugs when a 1 x 1
c                    matrix is present.
c               -------------------------------------------------------
c               NOTE: BACKSPACE commands in this program may cause
c                    difficulties on the IBM RISC (backspaces of long
c                    unformatted records not effective) if FORTRAN
c                    compiler versions earlier than 2.02 are used.
c                         Also, DECfortran version 3.0 is inadequate,
c                    and version 3.2 is required.
c                         Very time-consuming backspaces in subroutine
c                    RCEINP replaced May 1993 by rewind and
c                    reads forward.
c               -------------------------------------------------------
c               Vers. 11.1 May 1996, previously-used fixed numerical dimensions
c                    for U2, ... U6, V2, ...V6, have been replaced by variables
c                    defined in parameter statements, and subroutine ckdim
c                    added to dynamically check in each run the adequacy of
c                    dimensions of these variables and those of CFP1, CFP2,
c                    CFGP1, and CFGP2.
c               Vers. 11.1.1 August 1996, coding quirks in main (following 102),
c                    calcfct (dimension of fct), class1 (near start), and
c                    energy (do loop to 99), modified to allow compilation
c                    with bounds checking.
c               Vers. 11.1.2 October 1997, change made in subroutine energy
c                    to correct calculation of kinetic energies when Eav for
c                    the first first-parity configuration is not zero.
c               Vers. 11.1.3 January 1998, correct errors in calcfc and energy
c                    for cases in which there are no levels of some total J.
c                    (Bugs when both parities present corrected in Vers. 11.1.5)
c               Vers. 11.1.4 June 5, 1998, in formats 53 and 58 in spectr,
c                    change format for gf from f9.4 to f9.5.  Also, increase
c                    dimensions kmx and ktran from 150 and 600 to 250 and 1600.
c                    Dimensions of kls4, kls5, and kls6 and third subscripts
c                    of u2-u6, v2-v6, cfp, and cfgp increased from 7 to 9,
c                    for g-electron subshells; and fixed a bug in cuvfd for
c                    multply occupied subshells with l.gt.3.
c               Vers. 11.1.5 November 21, 1998, numerous unused local
c                    variables deleted, and variable-type inconsistances in
c                    sort2 and order removed.  Also, bugs corrected that
c                    caused trouble in rceinp, and in spectr when some
c                    intermediate J had no levels (as in 3s + 5g).
c               Vers. 11.1.6 December 3, 1998, elem changed to character variable,
c                    blank commons changed to labeled commons,
c                    and multipole-integral formats changed to give
c                    more significant figures.
c               Vers. 11.1.7 May 28, 1999, numerous unused variables deleted,
c                    including lld in the first tape72-74 write and read
c                    records.  [Note: This requires recomputing the tapes.]
c                    Also, variables u7,v7,u8,v8 added so that all eight
c                    subshells can be multiply occupied.  July 2, changed from
c                    kexc=40 to kexc=kmx.
c               Vers. 11.1.8 August 21, 1999, missing common/ener/ added to
c                    subroutine mupole.  New feature added such that if dmin<0.0,
c                    the print is deleted for all spectrum lines that do not
c                    involve the lowest-energy level of the parity being sorted.
c               Vers. 11.1.9 September 29, 1999, corrected some unimportant
c                    uninitialized variables that produced warning messages
c                    on some compilers; also corrected some write statements
c                    in pfgd re option icpc>4.
c               Vers. 11.2 October 24, 1999, made numerous format changes to
c                    use capital instead of lower-case letters, especially for
c                    LS-term letters S, P, D, F ... and for parameter names Eav,
c                    Fk, Gk, Zeta.  Also deleted print of rows of zero values
c                    of KE of autoionized free electrons and of aa and gaaxc,
c                    and deleted the KONFIT option.
c               Vers. 11.3 February 28, 2001, subroutine elecden added (called
c                    from energy 398) to calculate radial electron-density 
c                    distribution for each level eigenvector lp, using one-
c                    electron radial wavefunctions from disk file "tape2n"
c                    computed by program RCN.  July 14: In subroutine elecden,
c                    remove unneeded () in the write (6,28) statement 
c                    and add x(kmx) to common/ener/.  August 22: In calcfc add
c                    code before 420 to make parent label (1S) if there is only
c                    one occupied and non-filled subshell.
c               Vers. 11.4 December 19, 2001, correction made in subroutine 
c                    elecden.  (See comments therein.)  
c                    Also add for each J in energy, time prints for energy-matrix
c                    calculation, diagonalization, and sprin times for E and V.
c
c               The values of the dimension variables used in this version
c                    of RCG are too small to handle cfp decks for f5, f6,
c                    f7, f8, f9, and f10.  To make possible use of these
c                    subshells (in subshell 1), make the following changes
c                    in parameter statements throughout the program:
c                         kls1=119
c                         kjp=350
c                         kmx=360        (or greater)
c                         ktran=3200     (or greater)
c                    For two-open subshell configurations, even larger
c                    dimensions may be required, for example:
c                         kjp=500
c                         klsp=1500
c                         kmx=500
c                         ktran=6100
c                                [In general, ktran.ge.(kmx**2)/kbgks]
c                    If the f subshell is the second rather than first
c                    subshell--e.g., for d9 f7--then it is kls2 instead
c                    of kls1 that must be changed to 119, etc.
c               -------------------------------------------------------
c

      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lc5/goss(ktran,kbgks)
      common/c3lcd/dumlc5
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c6/emin,emax,sumwt,flbdn,flbdx,fnunp,fnuxp,nlev,iewr
      common/ctkev/nptkev,ntkev,tkev(6),tunrgy(6),brnchl(6),brncht(6),
     1  eionry
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
      dimension sjnk(2),sjxk(2),notkp(2)
      character*16 fil
      character*10 parnam(kpr,2),aa
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp


C-------Start ADAS conversion-------
c
      character*80 inf
      common/pwborn/nbigks,nenrgs,ekminl,ekmaxl,delekl,bigks(kbgks),
     1  xmin,xmax,tev(25),corr(kenrgs),ibk,ipwr,
     2  i11,ijkion,ijkiz,newstyle,temps(25),xsec(kenrgs)
      character*1 borncalc
      
      character*8 lldes(klam),lldesp(klam)
      common /batch/is_batch, i_tot, lldes,lldesp
 
c      dimension qqcoupl(12),mqqlsymb(24)                                
c      DATA (qqCOUPL(I),I=1,12)/6H    LS,6H    JJ,6H  JJJK,6H  LSLK,     
c     1  6H  LSJK,6HLSJLKS,6HLSJLSJ,6HJ1-LSJ,1H ,1H ,1H ,6H    SL/       
c      DATA (mqqLSYMB(I),I=1,24)/1HS,1HP,1HD,1HF,1HG,1HH,1HI,            
c     1  1HK,1HL,1HM,1HN,1HO,1HQ,1HR,1HT,1HU,1HV,1HW,                    
c     2  1HX,1HY,1HZ,1HA,1HB,1HC/                                        
 
 
 
      optread=1
                                                                        
c      do i=1,12                                                    
c        coupl(i)=qqcoupl(i)
c      end do                                             
c      do i=1,24                                                    
c        lsymb(i)=mqqlsymb(i)                                            
c      end do                                             
                                                                        
c      PARNAM(1,1)='EAV       '                                          
c      PARNAM(1,2)='EAV       '                                          


      read(5,*)is_batch, i_part

      read(5,'(A)')inf
      open(10,file=inf,status='old')
      
      read(5,'(A)')inf
      open(9,file=inf,status='unknown')
      
      read(5,'(A)')inf
      if (i_part.eq.1) then
         open(52,file=inf,status='unknown')
         open(77,status='scratch')
         do kk=1, 25
           temps(kk)=0.0
         end do
      elseif (i_part.eq.2) then
         open(77,file=inf,status='unknown')
         open(52,status='scratch')
         read(5,*)ijkiz,ijkion
         read(5,*)(temps(kk),kk=1,25)
      endif
      
      
      open(20,status='scratch',form='unformatted')
      open(31,status='scratch',form='unformatted')
      open(32,status='scratch',form='unformatted')
      open(41,status='scratch',form='unformatted')
      open(42,status='scratch',form='unformatted')
      open(43,status='scratch',form='unformatted')
      open(44,status='scratch',form='unformatted')
      
      open(19,status='scratch',form='unformatted')
      open(72,status='scratch',form='unformatted')
      open(73,status='scratch',form='unformatted')
      open(74,status='scratch',form='unformatted')
      
      
      open(2,status='scratch')
      open(3,status='scratch')
      open(13,status='scratch')
      open(11,status='scratch')


      i_tot = 0

      iii=2



C-------End ADAS conversion-------


c
c
c      open files
c
c          If iii=0, choose external names of input and
c               output files interactively (for VAX).
c          If iii=1, use standard file names (for VAX).
c          For CRAY and CDC computers, set iii>1 and
c               uncomment program card at start of program.
c
      if (iii.eq.1) go to 50
      if (iii.gt.1) go to 60
c
      write(6,*) ' input file name '
      read(5,3) fil
    3 format(a)
      open(10,file=fil,status='old')
      write(6,*) ' output file name '
      read(5,3) fil
      open(9,file=fil,status='unknown')
      write(6,*) ' file name for least-sqs. coeff. (blank, no file)'
      read(5,3) fil
      open(2,file=fil,status='unknown',form='unformatted')
      write (6,*) ' file name for least-sqs. input (blank, no file) '
      read(5,3) fil
      open(11,file=fil,status='unknown')
      go to 55
c
   50 open(10,file='ing11',status='old')
      open(9,file='outg11',status='unknown')
      open(2,file='tape2e',status='unknown',form='unformatted')
      open(11,file='outgine',status='unknown')
   55 open(20,status='scratch',form='unformatted')
      open(31,status='scratch',form='unformatted')
      open(32,status='scratch',form='unformatted')
c     open(41,status='scratch',form='unformatted')
      open(41,status='scratch',form='unformatted')
      open(72,file='tape72',status='unknown',form='unformatted')
      open(73,file='tape73',status='unknown',form='unformatted')
      open(74,file='tape74',status='unknown',form='unformatted')
      open(3,status='scratch')
      open(19,file='tape19',status='unknown')
      open(13,status='scratch')
      open(5,status='scratch')
c
c          set input and output disk unit numbers
c
   60 ir=10
      iw=9
      id2def=72
      ila=31
      ilb=32
      ic=41
c
c          calculate table of scaled factorials
c
      call calcfct

C-------Start ADAS conversion-------

      if (is_batch.LE.0) write(6,*)ktran
      if (is_batch.LE.0) write(6,*)is_batch
 
C-------Start ADAS conversion-------

      ntkev=0
      nptkev=0
      delekev=0.0
      iabg=1
      iv=0
      emina=0.0
      flbdn=0.0
      flbdx=0.0
      nolskp=0
      notkp(1)=0
      notkp(2)=0
      zero=0.0
      half=0.5
      one=1.0
      thrhal=1.5
      two=2.0
      call clock(time0)
      write (iw,4)
    4 format (1h1/)
      ires=0
    
    
C-------Start ADAS conversion-------


   80 read (ir,5) kcpl
C    5 format (i5)
    5 format (1x,i2)

C-------End ADAS conversion-------

 
c
      backspace ir
      if (kcpl.le.2) go to 94
      if (kcpl.gt.10) go to 90
      if (kcpl.eq.3) go to 85
      write (iw,6) kcpl
    6 format (//30h *****control card value kcpl=,i3,11h is illegal//)
      read (ir,5)
      go to 80
c
c          Read optional first control card (kcpl.eq.3)
c               (id2.gt.0--e.g. id2=42 or 72-- if cuvfd to be called)
c

C------Start ADAS conversion-------

   85 read (ir,7) kcpl,ilncuv,id2t,flbdn,flbdx,delekev,eionry,nptkev,
     1  (tkev(i),i=2,6),emina
C    7 format (i5,i2,i3,2e5.1,2f10.5,i5,5f5.3,f10.5)
    7 format (1x,i2,2x,i2,i3,2e5.1,2f10.5,i5,5f5.3,f10.5)

C------End ADAS conversion-------

      do 87 i=1,5
   87 if (tkev(i+1).gt.0.0) ntkev=i
      tkev(1)=9999.9
      write (iw,11) eionry,iabg,iv,nptkev,(tkev(i),i=2,6),time0
   11 format (11h0rcg mod 11,5x,7heionry=,f10.5,
     1  7x,5hiabg=,i1,3x,3hiv=,i1,4x,6hntkev=,i1,3x,5htkev=,
     2  5f6.2,6x,5htime=,f8.2)
      if (id2t.eq.0) go to 80
      id2=id2t
c
c     call overlay(2hg9,1,0)
      call cuvfd

C-------Start ADAS conversion-------

      if (is_batch.LE.0) write(6,*)is_batch
 
C-------Start ADAS conversion-------

      ires=7
      go to 80
c
c          Read optional second control card (kcpl.gt.3)
c               (for configurations with serial no..ge.nclskp(k);
c          retain only those basis states having
c               one of the notkp(k) values of total L and S)
c
   90 read (ir,8) j,k,nolskp,(muls1(i),lhs1(i),i=1,nolskp)
    8 format (i4,i1,i5,14(i4,a1))
      nclskp(k)=j
      notkp(k)=nolskp
      if (ires.eq.7) write (iw,4)
      ires=0
      if (nolskp.le.0) go to 80
      write (iw,9) k,j,(muls1(i),lhs1(i),i=1,nolskp)
    9 format (11h For parity,i2,19h and configs. with ,
     1  14hserial no..ge.,i3,18h, keep only terms ,14(i3,a1))
      do 92 i=1,nolskp
      eras=muls1(i)
      scrskp(i,k)=0.5*(eras-1.0)
      do 91 j=1,24
      if (lhs1(i).ne.lsymb(j)) go to 91
      scrlkp(i,k)=j-1
      go to 92
   91 continue
   92 continue
      go to 80
c
   94 if (id4.gt.0) go to 95
      id2=id2def
      id3=id2+1
      id4=id3+1
   95 if (flbdn.le.0.0) flbdn=0.001
      if (flbdx.le.flbdn) flbdx=500000.0
      if (delekev.le.0.0) delekev=0.00025
c
c          Read final control card (or rescale card if kcpl=0).
c
c          Read kind of coupling (1=ls, or 2=jj),
c               number of sub-shells in each configuration, and
c               number of configurations (for each parity).
c          If iabg.ge.1, parameters alpha, beta, gamma are included
c               for equivalent electrons (also t for d2-d8, and
c               t1 and t2 for d3-d7).  If iabg=2 or 4, then illegal-k
c               parameters are included for non-equivalent electrons.
c               If iabg.ge.3, then SL coupling is used instead of LS.
c          If eav=-55555555., input for least-squares energy-level-fitting
c               program RCE is written on tape 2.
c          If ictbcd.gt.0, input for Argonne least-squares program
c               is written on tape 19.  (Tapes 2 and 19 cannot
c               both be written in the same run.)
c          If ictbcd.lt.0, input for Zeeman Laboratory least-squares
c               program (KONFIT) is written on tape 5 (=tape 19).
c                  (Option deleted October 1999; ictbcd<0 set to 0.)
c          If igen=kcpld(3).ge.5, plane-wave-Born collision
c               strengths will be calculated (igen=9 gives minimum print).
c               Values of kcpl(4)-(5) and (6)-(7) must be set appropriately
c               for minimum and maximum values of the multipole index,
c               for parity conserving and changing excitations, respectively.
c
      coupl1=coupl(1)
c     ks=8
c     kc=50
      irw19=0
      rewind 2
      iscale=0
      deleav=0.0
      uenrgy=0.0
      fact0(1)=0.0
      fact0(2)=0.0
      fact0(3)=0.0
      fact0(4)=0.0
      fact0(5)=0.0
      texcit=0.0
      if (kcpl.eq.0) go to 101
c


C-------Start ADAS conversion-------
C
C  100 read (ir,10) kcpl,nck,nocset,nsconf,iabg,iv,nlsmax,nlt11,nevmax,
C     1  kcpld,ielpun,sjnk(1),sjxk(1),sjnk(2),sjxk(2),imag,iquad,ueninp,
C     2  dmin,ilncuv,iplev,icpc,icfc,idip, iengyd,ispecc,
C     3  iw6,ipct,ictc,ictbcd
C
C   10 format (i5,i1,2i2,i1,2i2,i1,2i2,2i1,i3,i2,i3,
C     1  9i1,i1,4f2.1,2i1,f10.5,   e5.1,5i1, 2i1,   4i2)
C      if (kcpl.lt.0) then
c          write EOF on units 9 and 11
C        do 99 i=9,11
C        if (i.eq.10) go to 99
C        endfile i
C        rewind i
C   99   continue
C        stop '(normal exit)'
C      end if
c
      read (ir,9905) borncalc                                           
 9905 format (a1)                                                       
 
      if (borncalc.eq.'N'.or.borncalc.eq.'n') then
         newstyle=1
         assign 10 to ifm
      elseif (borncalc.eq.'B'.or.borncalc.eq.'b') then
         newstyle=2
         assign 10 to ifm
      else
         newstyle=0
         assign 9910 to ifm
      endif
      backspace ir                                                      
 
  100 read (ir,ifm) kcpl,nck,nocset,nsconf,iabg,iv,nlsmax,nlt11,nevmax, 
     1  kcpld,ielpun,sjnk(1),sjxk(1),sjnk(2),sjxk(2),imag,iquad,ueninp, 
     2  dmin,ilncuv,iplev,icpc,icfc,idip, iengyd,ispecc,                
     3  ins,ipct,ictc,ictbcd                                            
 
   10 format (1x,i2,2i2,1x,i2,i1,2i2,i1,2i2,2i1,i3,i2,i3,               
     +        9i1,i1,4f2.1,2i1,f10.5,   e5.1,5i1, 2i1,   4i2)           
 9910 format (i5,i1,2i2,i1,2i2,i1,2i2,2i1,i3,i2,i3,                     
     1        9i1,i1,4f2.1,2i1,f10.5,   e5.1,5i1, 2i1,   4i2)           
 
 
      IF (KCPL.LT.0) then
 
         write(77,*)'ENG/TEMP'
         write(77,4889)nenrgs
         if (nenrgs.ne.0) then
            write(77,4888)(xsec(jjj),jjj=1,nenrgs)
c            write(77,4888)(tev(jjj+i1),jjj=1,nenrgs)
            write(77,4888)(tev(jjj),jjj=1,nenrgs)
         endif
 
C         STOP '(NORMAL EXIT)'
c         return
 	   stop
      endif
 4888 format(7(1pd10.3))
 4889 format(1x,i4)

C------End ADAS conversion-------

      if (ueninp.le.0.0) ueninp=1000.0
      if (ictbcd.lt.0) ictbcd=0
      if (kcpl.gt.0) go to 104
c          If (kcpl.eq.0) interpret as a rescale card.
      backspace ir
  101 read (ir,12) deleav,kcpld,ielpun,ueninp,texcit
   12 format (20x,f10.5,10i1,10x,f10.5,f5.2)
      iscale=0
      if (ueninp.le.0.0) go to 102
      iscale=7
      uenrgy=ueninp
  102 i=0
      do 103 j=1,5
      i=i+2
      if (j.lt.5) eras=10*kcpld(i-1)+kcpld(i)
      if (j.eq.5) eras=10*kcpld(9)+ielpun
      if (eras.ge.98.5) eras=100.0
      if (eras.gt.0.0.and.eras.lt.1.5) eras=0.1
  103 fact0(j)=eras/100.0
      go to 100
c
  104 if (nsconf(2,1).le.kc.and.nsconf(2,2).le.kc) go to 105
      write (iw,13) nsconf(2,1),nsconf(2,2),kc
   13 format (' no. of configurations=',2i4,' greater than dim. kc=',i2)
      stop '(too many configurations)'
  105 if (iscale.eq.0) uenrgy=ueninp
      scale=ueninp/uenrgy
      if (nlt11.eq.0) nlt11=119
      if (nevmax.eq.0) nevmax=500
      if (nocset.ne.0.and.nlsmax.ne.0) nlsmax=500
      if (ielpun.eq.2) ictbcd=0
      if (sjxk(1).le.0.0) sjxk(1)=99.0
      if (sjxk(2).le.0.0) sjxk(2)=99.0
      if (nolskp.eq.0.and.sjxk(1)+sjxk(2).ne.198.0) nolskp=-7
      if (nolskp.eq.0) go to 106
c     kcpl=1
c     kcpld(2)=1
c     nlsmax=0
  106 if (iengyd.eq.0) iengyd=500
      if (iengyd.eq.2) iengyd=(nevmax+10)/11
      igdlev=0
      if (ispecc.eq.0) igdlev=7
      if (igdlev.ne.0) ispecc=8
      iplotc=0
      if (ispecc.eq.0) iplotc=1
      igen=kcpld(3)
      if (igen.ge.5) go to 110
      irnq=2
      irxq=2
      irnd=1
      irxd=1
      go to 120
  110 irnq=kcpld(4)
      irxq=max0(irnq,kcpld(5))
      irnd=max0(1,kcpld(6))
      irxd=max0(irnd,kcpld(7))
      if (nsconf(2,2).le.0) iquad=1
      dmin=0.0
      ispecc=1
      ntkev=0
      nptkev=0
      if (nck(1).le.0) nck(1)=1
  120 if (nck(1).eq.0) nck(1)=kc
      if (nck(2).eq.0) nck(2)=kc
      call clock(time0)
      coupl(1)=coupl1
      if (iabg.ge.3) coupl(1)=coupl(12)
c        NOTE---The SL-coupling option concerns only (scrs4,scrl4)scrj8.
c               The JJ, JK, etc. representations still use (Li,Si)Ji.
      if (ires.eq.7) write (iw,4)
      ires=7
      write (iw,15) coupl(kcpl),nsconf,iabg,iv,nlt11,kcpld,sjxk,
     1  ilncuv,iplev,icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd
   15 format (11h0rcg mod 11,a6,9h coupling,4x,7hnsconf=,3i2,3x,
     1  2i2,i3,4x,5hiabg=,i1,3x,3hiv=,i1,2x,i4,1x,9i1,2f5.1,2x,
     2  6hprint=,5i1, i4,4i2)
      if (iw6.lt.0)
     1  write (6,15) coupl(kcpl),nsconf,iabg,iv,nlt11,kcpld,sjxk,
     2  ilncuv,iplev,icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd
      if (ictbcd.lt.0) rewind 5
      if (ictbcd.le.0.and.ielpun.lt.2) go to 180
      if (irw19.ne.0) go to 180
      rewind 19
      irw19=7
  180 if (nocset.eq.0) go to 200
      ictbcd=0
      if (nocset.lt.0) go to 200
  182 read (2) nocdes,nscrj8,npar,j1x
      npar=npar-j1x+1
      do 183 n=1,nscrj8
      read (2) l
      do 183 m=1,npar
  183 read (2) a
      if (nocdes.ne.nocset) go to 182
      go to 100
c
c          Calc. level designations and coefficient matrices
c
c 200 call overlay(2hg9,2,0)
  200 call lncuv

C-------Start ADAS conversion-------

      if (is_batch.LE.0) write(6,*)is_batch
 
C-------Start ADAS conversion-------

      call clock(time)
      delt=time-time0
      write (iw,25) delt
   25 format (/19h0Finished lncuv at ,f6.3,4h min)
      if (iw6.lt.0) write (6,1025) delt
 1025 format (/19h Finished lncuv at ,f6.3,4h min)
c
      il=ila
      rewind ic
      noicwr=0
      k=1
c
  400 kk=k
      noic(k)=noicwr
      noicmup(k)=0
      nolskp=notkp(k)
      do 410 i=1,ks
      ll(i)=llijk(i,1,k)
  410 fll(i)=fllijk(i,1,k)
      nosubc=nsconf(1,k)
      sjn=sjnk(k)
      sjx=sjxk(k)
      if (ictc.ne.0) go to 450
c     call overlay(2hg9,3,0)
      call plev
      call clock(time)

C-------Start ADAS conversion-------

      if (is_batch.LE.0) write(6,*)is_batch
 
C-------Start ADAS conversion-------

      delt=time-time0
      write (iw,26) delt
   26 format (/19h0Finished plev at  ,f6.3,4h min/1h1)
      if (iw6.lt.0) write (6,1026) delt
 1026 format (19h Finished plev at  ,f6.3,4h min)
c     call overlay(2hg9,4,0)
      call pfgd
      call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,1027) delt
 1027 format (19h Finished pfgd at  ,f6.3,4h min)
      if (nsconf(1,2).eq.0.and.icpc.eq.9) go to 100
      if (nsconf(2,k).le.1) go to 430
c     call overlay(2hg9,5,0)
      call prk
      call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,1028) delt
 1028 format (18h Finished prk at  ,f7.3,4h min)	
  430 nopc=25
      npar1=-7
      j1=1
      aa='dummy'
      write (20) npar1,kpar,k,i,j,aa,cave,j1,nopc, (pc(n1), n1=1,nopc)
      rewind 20
      j1x=nsconf(2,k)
      notsx=0
      do 440 i=1,nosubc
  440 notsx=max0(notsj1(i,j1x+1),notsx)
      rewind il
      ilc=il
      do 442 nn=1,2
      write (ilc) notsj1,nscrj8,sj8mn,sj8mx,npar,nparj,parnam,
     1  notsx, ((fl(l,i),s(l,i), l=1,notsx), i=1,nosubc),nalsjp,pj
  442 ilc=ic
      noicwr=noicwr+1
c     call overlay(2hg9,6,0)
c          write (6,2468) noicwr,noicmup
c2468 format (' noicwr,noicmup=',i5,2x,2i5)
      call calcfc
      call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,1029) delt
 1029 format (19h Finished calcfc at,f6.3,4h min)	
c          Write (6,2468) noicwr,noicmup
      iarg1=0
      iarg2=il
      iarg3=il
      irmn=1
      irmx=1
c     if (imag.eq.k.or.imag.gt.2) call overlay(2hg9,7,0)
      if (imag.eq.k.or.imag.gt.2) call mupole
c          Write (6,2468) noicwr,noicmup
      iarg1=2
      irmn=irnq
      irmx=irxq
c     if (iquad.eq.k.or.iquad.gt.2) call overlay(2hg9,7,0)
      if (iquad.eq.k.or.iquad.gt.2) call mupole
c          Write (6,2468) noicwr,noicmup
  450 rewind il
      if (il.eq.ilb) go to 500
      if (nsconf(1,2).le.0) go to 600
      il=ilb
      k=2
      go to 400
c
  500 iarg1=iiprty
      iarg2=ila
      iarg3=ilb
      irmn=irnd
      irmx=irxd
      if (ictc.ne.0) go to 600
c     call overlay(2hg9,7,0)
      call mupole
c          Write (6,2468) noicwr,noicmup
c
c          Calculate energy levels and spectra
c
c 600 call overlay(2hg9,8,0)

C-------Start ADAS conversion-------

      if (is_batch.LE.0) write(6,*)is_batch
 
C-------Start ADAS conversion-------

  600 call energy
      rewind ila
      rewind ilb
      go to 100
c
      end
