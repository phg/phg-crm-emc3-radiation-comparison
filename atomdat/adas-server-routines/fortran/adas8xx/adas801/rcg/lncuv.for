C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/lncuv.for,v 1.3 2010/04/09 16:45:16 mog Exp $ Date $Date: 2010/04/09 16:45:16 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.2
C  DATE    : 09-04-2010
C  MODIFIED: Martin O'Mullane
C             - Literal constants in quotes, to be assigned to an
C               integer variable, are not allowed in data statements
C               (IBM allowed extension to f66) so replace with 
C               Hollerith constants (f77 standard).
C
C-----------------------------------------------------------------------
c     overlay(g9,2,0)
      subroutine lncuv
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/energies/etot(kc,2),ee8(kc,2),e0kin(kc),ekin(kmx)
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
      data iblank/1h /
c
c
c
c          Read and check input configurations
c
c     ks=8
  200 ierror=0
      write (iw,10) uenrgy
   10 format (/8h0  k   j,11x,13hconfiguration,35x,
     1  35hn   par   i   terms of li(ni) -----,7x,
     2  9h(e unit =,f8.1,6h cm-1))
      ii=0
      do 259 k=1,2
      if (nsconf(1,k).le.0) go to 259
      write (iw,11)
   11 format (1h )
      nosubc=nsconf(1,k)
      jmax=nsconf(2,k)
      do 202 i=1,ks
      ll(i)=lsymb(1)
      fll(i)=0.0
      nlastt(i)=1
      mult(1,i)=1
      lbcd(1,i)=lsymb(25)
      jjj=max0(nsconf(2,1),nsconf(2,2))
      do 202 j=1,jjj
      llijk(i,j,k)=lsymb(1)
      fllijk(i,j,k)=0.0
  202 nijk(i,j,k)=0
      do 249 j=1,jmax
      n=0
      iprity=1
      read (ir,20) (ll(i),ni(i), i=1,8),(pci(i),i=1,3),a1,a2
   20 format (8(a1,i2,2x),3a6,f13.3,f9.4)
      do 229 i=1,nosubc
      nijk(i,j,k)=ni(i)
      do 212 l=1,24
      if (lsymb(l).ne.ll(i)) go to 212
      ill=l-1
      fll(i)=l-1
      fllijk(i,j,k)=fll(i)
      go to 213
  212 continue
  213 llijk(i,j,k)=ll(i)
      if (ll(i).eq.llijk(i,1,k)) go to 220
      if (i.eq.nosubc.and.ni(i).eq.1) go to 220
      do 217 m=1,j
      if (nijk(i,m,k).ne.0) go to 218
  217 continue
      go to 220
  218 if (ll(i).eq.llijk(i,m,k)) go to 220
      ierror=7
  220 n=n+ni(i)
      if (mod(ni(i)*ill,2).ne.0) iprity=-iprity
  229 continue
c
      npt=0
      do 232 i=1,nosubc
      nn=nijk(i,j,k)
      nfull=4.0*fll(i)+2.0
      if (nn.gt.1.and.nn.lt.nfull-1) go to 230
      not=1
      nlt=1
      mult(1,i)=1
      lbcd(1,i)=lsymb(25)
      ialf(1,i)=iblank
      fl(1,i)=0.0
      s(1,i)=0.0
      m=1
      if (nn.eq.0.or.nn.eq.nfull) go to 1230
      mult(1,i)=2
c     lbcd(1,i)=ll(i)
        do jj=1,24
        if (ll(i).eq.lsymb(jj)) lbcd(1,i)=lsymb(jj+24)
        end do
      fl(1,i)=fll(i)
      s(1,i)=0.5
      m=1
      go to 1230
  230 call locdsk(id2,fll(i),nijk(i,j,k))
      read (id2) (mult(m,i),lbcd(m,i),ialf(m,i),fl(m,i),s(m,i), m=1,nlt)
 1230 if (i.eq.1.and.j.lt.jmax) nlt=min0(nlt,nlt11)
      if (i.eq.1.and.jmax.eq.1) nlt=min0(nlt,nlt11)
      do jj=1,24
      if (lbcd(m,i).eq.lsymb(jj)) lbcd(m,i)=lsymb(jj+24)
      end do
      if (nlt.eq.not) go to 232
      npt=npt+1
      if (npt.gt.1) go to 231
      write (iw,21) k,j, (ll(ip),nijk(ip,j,k),ip=1,ks), n,iprity,i,
     1  (mult(l,i),lbcd(l,i),ialf(l,i), l=1,nlt)
   21 format (i4,i4,7x,8(a1,i2,3x),i5,i5,i5,3x,13(i1,a1,a2)/
     1  81x,13(i1,a1,i2))
      go to 232
  231 write (iw,22) i, (mult(l,i),lbcd(l,i),ialf(l,i), l=1,nlt)
   22 format (73x,i5,3x,13(i1,a1,a2)/81x,13(i1,a1,a2))
  232 continue
      if (npt.gt.0) go to 240
      a1=a1*1000.0/uenrgy
      write (iw,23) k,j, (ll(i),nijk(i,j,k),i=1,ks), n,iprity,
     1  (pci(i),i=1,3),a1,a2
   23 format (i4,i4,7x,8(a1,i2,3x),i5,i5,8x,3hall,8x,3a6,f13.3,f9.4)
      etot(j,k)=a1
      ee8(j,k)=a2
  240 if (j.gt.1) go to 245
      if (k-1) 241,241,242
  241 no=n
  242 iprtyo=iprity
      go to 249
  245 if (n.ne.no) go to 247
      if (iprity.eq.iprtyo) go to 249
  247 ierror=7
  249 continue
  259 ii=ii+iprity
      iiprty=1
      if (ii.ne.0) iiprty=2
c
      if (ierror.eq.0) go to 281
      write (iw,25)
   25 format (//' lncuv 25--deck setup error: ',
     1  'occ. nos. or parities inconsistent'/)
      if (iw6.lt.0) write (6,25)
      stop '(lncuv 25)'
c
c          Calc (li//Ck//lj) table
c
  281 write (iw,28)
   28 format (///)
      if (ilncuv.le.0) go to 900
      n=nosubc
      do 299 i1=1,nosubc
      do 299 i2=i1,nosubc
      if (i2-i1) 283,283,284
  283 i=i1
      go to 285
  284 n=n+1
      i=n
  285 do 286 nk=1,6
  286 cijk(i,nk)=0.0
      fk0=abs(fll(i1)-fll(i2))-2.0
      kmin=fk0+2.0
      nok=(fll(i1)+fll(i2)-fk0)/2.0
      do 295 nk=1,nok
      fk0=fk0+2.0
  295 cijk(i,nk)=cijkf(fll(i1),fll(i2),fk0)
  297 write (iw,29) kmin,i1, ll(i1),i2, ll(i2), (cijk(i,nk), nk=1,nok)
   29 format (6h kmin=,i2,5x,1h(,i1,a1,6h//Ck//,i1,a1,2h)=,
     1  3x,6f15.8)
  299 continue
c          Check dimensions of Ui, Vi, cfp, and cfgp
  900 call ckdim
      return
      end
