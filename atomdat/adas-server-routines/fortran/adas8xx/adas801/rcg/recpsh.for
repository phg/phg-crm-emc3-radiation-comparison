C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/recpsh.for,v 1.2 2004/07/06 14:50:00 whitefor Exp $ Date $Date: 2004/07/06 14:50:00 $
C
      function recpsh(fj1,fj2,fjp,fj3,fj,fjpp)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      a=sqrt((2.0*fjp+1.0)*(2.0*fjpp+1.0))
      two=2.0
      if (mod(fj1+fj2+fj3+fj,two).gt.0.0) a=-a
      recpsh=a*s6j(fj1,fj2,fjp, fj3,fj,fjpp)
      return
      end
