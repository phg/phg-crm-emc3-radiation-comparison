C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/recpex.for,v 1.2 2004/07/06 14:49:49 whitefor Exp $ Date $Date: 2004/07/06 14:49:49 $
C
      function recpex(fj1,fj2,fjp,fj3,fj,fjpp)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      a=sqrt((2.0*fjp+1.0)*(2.0*fjpp+1.0))
      two=2.0
      if (mod(fj2+fj3+fjp+fjpp,two).gt.0.0) a=-a
      recpex=a*s6j(fj2,fj1,fjp, fj3,fj,fjpp)
      return
      end
