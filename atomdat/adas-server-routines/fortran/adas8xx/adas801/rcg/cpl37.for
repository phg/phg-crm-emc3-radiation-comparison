C UNIX-IDL - SCCS info: Module @(#)cpl37.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------

      subroutine cpl37
c
c          Calc. transf. matrices from LS or JJ to representations 3-7
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/dumlc4(kmx**2-kcc*8),cc(kcc,8)
      dimension nparj(kc,kc)
      equivalence (dumlc4,nparj)
      common/c3lcd/dumlc5
      dimension iql(kmx),iqm1l(kmx),iqm2l(kmx),scrjm1(kmx),scrjm2(kmx)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
c
c
c        Calc. transformation matrices from LS (kcpl=1) or JJ (kcpl=2) to
c               five additional coupling representations (kcpl1=3,4,5,6,7)
c
  100 if (nosubc.le.1.or.nls.le.1) go to 980
      if (nls.gt.nlsmax) go to 980
      kcpl1=3
      if (kcpl.eq.1) go to 101
      rewind 2
      write (2) ((tmx(l,lp),l=1,nls), lp=1,nls)
  101 nmax=nsconf(2,kk)
      do 107 nc=1,nmax
      iq=0
      iqm1=0
      iqm2=0
      do 104 i=1,nosubc
      j=nosubc-i+1
      ieras=4.0*fllijk(j,nc,kk)+2.0
      if (nijk(j,nc,kk).eq.0.or.nijk(j,nc,kk).eq.ieras) go to 104
      if (iq.ne.0) go to 102
      iq=j
      go to 104
  102 if (iqm1.ne.0) go to 103
      iqm1=j
      go to 104
  103 if (iqm2.ne.0) go to 105
      iqm2=j
      go to 105
  104 continue
  105 iqm1=max0(1,iqm1)
      iqm2=max0(1,iqm2)
      do 106 l=1,nls
      if (ncfg(l).ne.nc) go to 106
      iql(l)=iq
      iqm1l(l)=iqm1
      iqm2l(l)=iqm2
  106 continue
  107 continue
      if (iabg.lt.3) go to 110
      do 109 l=1,nls
      m=nosubc
      i=scrl(l,m)+scrs(l,m)-scrj8
      if (mod(i,2).ne.0) go to 109
      do 108 lp=1,nls
  108 tmx(l,lp)=-tmx(l,lp)
  109 continue
c
  110 do 111 l=1,nls
  111 lf(l)=0
      if (kcpld(kcpl1).le.0) go to 200
      if (kcpl1.ne.4) go to 120
      if (kcpld(5)) 200,200,970
  120 if (kcpl1.ne.6) go to 970
  121 if (kcpld(7).gt.0) go to 970
  200 go to (100,100,300,400,500,600,700) kcpl1
c
c          jjJK coupling (kcpl1=3)
c
  300 do 349 l=1,nls
      iqm1=iqm1l(l)
      iq=max0(iql(l),iqm1+1)
      scrjm1(l)=scrj(l,iqm1)
      if (lf(l).ne.0) go to 349
      lf(l)=l
      nq=nalsj(l,iq)
      fkk=min(scrj(l,iqm1)+fl(nq,iq), scrj8+s(nq,iq))
      fkj(l)=fkk
      if (l.ge.nls) go to 349
      l1=l+1
      do 339 lp=l1,nls
      do 325 i=1,iq
      if (nalsj(l,i).ne.nalsj(lp,i)) go to 339
  325 continue
      do 330 i=1,iqm1
      if (fj(l,i).ne.fj(lp,i)) go to 339
      if (scrj(l,i).ne.scrj(lp,i)) go to 339
  330 continue
      lf(lp)=l
      fkk=fkk-1.0
      fkj(lp)=fkk
  339 continue
  349 continue
c
      do 369 l=1,nls
      iqm1=iqm1l(l)
      iq=max0(iql(l),iqm1+1)
      nq=nalsj(l,iq)
      do 368 lp=1,nls
      ct4(l,lp)=tmx(l,lp)
      c(l,lp)=0.0
      if(ncfg(l).ne.ncfg(lp)) go to 368
      if (lf(l).ne.lf(lp)) go to 368
      i=scrj(l,iqm1)+fl(nq,iq)+s(nq,iq)+scrj8+0.001
      a=sqrt((2.0*fkj(lp)+1.0)*(2.0*fj(l,iq)+1.0))
      if (mod(i,2).ne.0) a=-a
      b=s6j(fkj(lp),s(nq,iq),scrj8, fj(l,iq),scrj(l,iqm1),fl(nq,iq))
      c(l,lp)=a*b
  368 continue
  369 continue
      go to 900
c
c          LSLK coupling (kcpl1=4)
c
  400 do 449 l=1,nls
      iqm1=iqm1l(l)
      iq=max0(iql(l),iqm1+1)
      if (lf(l).ne.0) go to 449
      lf(l)=l
      nq=nals(l,iq)
      fkk=min(scrl(l,iq)+scrs(l,iqm1), scrj8+s(nq,iq))
      fk(l)=fkk
      if (l.ge.nls) go to 449
      l1=l+1
      do 439 lp=l1,nls
      do 425 i=1,iq
      if (nals(l,i).ne.nals(lp,i)) go to 439
  425 continue
      if (scrl(l,iq).ne.scrl(lp,iq)) go to 439
      do 430 i=1,iqm1
      if (scrl(l,i).ne.scrl(lp,i)) go to 439
      if (scrs(l,i).ne.scrs(lp,i)) go to 439
  430 continue
      lf(lp)=l
      fkk=fkk-1.0
      fk(lp)=fkk
  439 continue
  449 continue
c
      do 469 l=1,nls
      iqm1=iqm1l(l)
      iq=max0(iql(l),iqm1+1)
      do 468 lp=1,nls
      tmx(l,lp)=0.0
      if(ncfg(l).ne.ncfg(lp)) go to 468
      if (lf(l).ne.lf(lp)) go to 468
      nq=nals(l,iq)
      i=scrl(l,iq)+scrs(l,iqm1)+s(nq,iq)+scrj8+0.001
      a=sqrt((2.0*fk(lp)+1.0)*(2.0*scrs(l,iq)+1.0))
      if (mod(i,2).ne.0) a=-a
      b=s6j(fk(lp),s(nq,iq),scrj8, scrs(l,iq),scrl(l,iq),scrs(l,iqm1))
      tmx(l,lp)=a*b
  468 continue
  469 continue
      go to 930
c
c          LSJK coupling (kcpl1=5)
c
  500 do 549 l=1,nls
      iqm1=iqm1l(l)
      iq=max0(iql(l),iqm1+1)
      if (lf(l).ne.0) go to 549
      lf(l)=l
      nq=nals(l,iq)
      fjj=min(scrl(l,iqm1)+scrs(l,iqm1), fl(nq,iq)+fk(l))
      scrj(l,iqm1)=fjj
      scrjm1(l)=fjj
      if (l.ge.nls) go to 549
      l1=l+1
      do 539 lp=l1,nls
      do 525 i=1,iq
      if (nals(l,i).ne.nals(lp,i)) go to 539
  525 continue
      if (fk(l).ne.fk(lp)) go to 539
      do 530 i=1,iqm1
      if (scrl(l,i).ne.scrl(lp,i)) go to 539
      if (scrs(l,i).ne.scrs(lp,i)) go to 539
  530 continue
      lf(lp)=l
      fjj=fjj-1.0
      scrj(lp,iqm1)=fjj
      scrjm1(lp)=fjj
  539 continue
  549 continue
c
      do 569 l=1,nls
      iqm1=iqm1l(l)
      iq=max0(iql(l),iqm1+1)
      do 568 lp=1,nls
      ct4(l,lp)=tmx(l,lp)
      c(l,lp)=0.0
      if(ncfg(l).ne.ncfg(lp)) go to 568
      if (lf(l).ne.lf(lp)) go to 568
      nq=nals(lp,iq)
      i=scrl(l,iq)+scrj(lp,iqm1)+fl(nq,iq)+scrs(l,iqm1)+0.001
      a=sqrt((2.0*scrl(l,iq)+1.0)*(2.0*scrj(lp,iqm1)+1.0))
      if (mod(i,2).ne.0) a=-a
      b=s6j(scrl(l,iq),scrs(l,iqm1),fk(l), scrj(lp,iqm1),fl(nq,iq),
     1  scrl(l,iqm1))
      c(l,lp)=a*b
  568 continue
  569 continue
      go to 900
c
c          lsjLKS coupling (kcpl1=6)
c
c      calc qu. nos. for kcpl1=6 and 7 both
  600 do 646 l=1,nls
      iqm2=iqm2l(l)
      iqm1=max0(iqm1l(l),iqm2+1)
      iq=max0(iql(l),iqm1+1)
      if (lf(l).ne.0) go to 646
      l1=l
      nqm1=nals(l,iqm1)
      nq=nals(l,iq)
      njq=2.0*min(scrl(l,iqm2),scrs(l,iqm2))+1.0
      nlq=2.0*min(fl(nqm1,iqm1),fl(nq,iq))+1.0
      nsq=2.0*min(s(nqm1,iqm1),s(nq,iq))+1.0
      sjq=scrl(l,iqm2)+scrs(l,iqm2)
      do 645 ijq=1,njq
      slq=fl(nqm1,iqm1)+fl(nq,iq)
      do 644 ilq=1,nlq
      ssq=s(nqm1,iqm1)+s(nq,iq)
      do 643 isq=1,nsq
      fkk=sjq+slq+1.0
      fkmn=abs(sjq-slq)
      fjj=slq+ssq+1.0
      fjmn=abs(slq-ssq)
  602 fkk=fkk-1.0
      if (fkk.lt.fkmn) go to 643
      if (fkk+ssq-scrj8) 643,608,606
  606 if (abs(fkk-ssq).gt.scrj8) go to 602
  608 fjj=fjj-1.0
      if (fjj.lt.fjmn) go to 643
      if (fjj+sjq-scrj8) 643,615,612
  612 if (abs(fjj-sjq).gt.scrj8) go to 608
  615 lf(l1)=l
      scrj(l1,iqm2)=sjq
      scrjm2(l1)=sjq
      scrl6(l1)=slq
      fk6(l1)=fkk
      scrs6(l1)=ssq
      scrj(l1,iqm1)=fjj
      scrjm1(l1)=fjj
      i=slq+1.0
      lhqq(l1)=lsymb(i+24)
      multqq(l1)=2.0*ssq+1.0
      if (l1.ge.nls) go to 646
      l11=l1+1
      do 630 lp=l11,nls
      do 622 i=1,iq
      if (nals(lp,i).ne.nals(l,i)) go to 630
  622 continue
      do 625 i=1,iqm2
      if (scrl(lp,i).ne.scrl(l,i)) go to 630
      if (scrs(lp,i).ne.scrs(l,i)) go to 630
  625 continue
      l1=lp
      go to 602
  630 continue
      go to 646
  643 ssq=ssq-1.0
  644 slq=slq-1.0
  645 sjq=sjq-1.0
  646 continue
c
      do 669 l=1,nls
      iqm2=iqm2l(l)
      iqm1=max0(iqm1l(l),iqm2+1)
      iq=max0(iql(l),iqm1+1)
      nqm1=nals(l,iqm1)
      nq=nals(l,iq)
      a1=scrl(l,iqm2)
      a2=scrs(l,iqm2)
      a4=scrl(l,iqm1)
      a5=scrs(l,iqm1)
      a6=scrl(l,iq)
      a7=scrs(l,iq)
      a8=fl(nqm1,iqm1)
      a9= s(nqm1,iqm1)
      a10=fl(nq,iq)
      a11= s(nq,iq)
      do 668 lp=1,nls
      tmx(l,lp)=0.0
      if(ncfg(l).ne.ncfg(lp)) go to 668
      if (lf(l).ne.lf(lp)) go to 668
      a3=scrj(lp,iqm2)
      a12=fk6(lp)
      a13=scrl6(lp)
      a14=scrs6(lp)
      i=a1+a2+a3+a6+a7-scrj8+a8+a9+a10+a11+a13-a14
      a=sqrt((2.0*a4+1.0)*(2.0*a5+1.0)*(2.0*a6+1.0)*(2.0*a7+1.0)
     1      *(2.0*a13+1.0)*(2.0*a14+1.0)*(2.0*a12+1.0)*(2.0*a3+1.0))
      if (mod(i,2).ne.0) a=-a
      b=s6j(a1,a8,a4, a10,a6,a13) * s6j(a2,a9,a5, a11,a7,a14)
     1  *s6j(a12,a14,scrj8, a7,a6,a2) * s6j(a6,a2,a12, a3,a13,a1)
      tmx(l,lp)=a*b
  668 continue
  669 continue
      go to 930
c
c          lsj(LSJ) coupling (kcpl1=7)
c
  700 do 769 l=1,nls
      iqm2=iqm2l(l)
      iqm1=max0(iqm1l(l),iqm2+1)
      iq=max0(iql(l),iqm1+1)
      a1=scrj(l,iqm2)
      a2=scrl6(l)
      a3=fk6(l)
      a4=scrs6(l)
      do 768 lp=1,nls
      ct4(l,lp)=tmx(l,lp)
      c(l,lp)=0.0
      if(ncfg(l).ne.ncfg(lp)) go to 768
      if (lf(l).ne.lf(lp)) go to 768
      if (a1.ne.scrj(lp,iqm2)) go to 768
      if (a2.ne.scrl6(lp)) go to 768
      if (a4.ne.scrs6(lp)) go to 768
      a6=scrj(lp,iqm1)
      i=a1+a2+a4+scrj8
      a=sqrt((2.0*a3+1.0)*(2.0*a6+1.0))
      if (mod(i,2).ne.0) a=-a
      b=s6j(a1,a2,a3, a4,scrj8,a6)
      c(l,lp)=a*b
  768 continue
  769 continue
      go to 900
c
c          Output
c
  900 do 910 l=1,nls
      do 910 lp=1,nls
      tmx(l,lp)=0.0
      do 905 k=1,nls
  905 tmx(l,lp)=tmx(l,lp)+ct4(l,k)*c(k,lp)
  910 continue
c
  930 if (iabg.lt.3) go to 935
      do 934 l=1,nls
      m=nosubc
      i=scrl(l,m)+scrs(l,m)-scrj8
      if (mod(i,2).ne.0) go to 934
      do 933 lp=1,nls
  933 tmx(l,lp)=-tmx(l,lp)
  934 continue
  935 if (kcpl.eq.1) go to 939
      if (kcpl1.eq.5.or.kcpl1.eq.7) go to 939
c          transform from ls to jj representation
      rewind 2
      read (2) ((c(l,lp),l=1,nls), lp=1,nls)
      do 936 l=1,nls
      do 936 lp=1,nls
      ct4(l,lp)=tmx(l,lp)
  936 continue
      do 938 l=1,nls
      do 938 lp=1,nls
      tmx(l,lp)=0.0
      do 937 lpp=1,nls
  937 tmx(l,lp)=tmx(l,lp)+c(lpp,l)*ct4(lpp,lp)
  938 continue
  939 if (kcpld(kcpl1).ne.0) go to 970
      write (ic) ((tmx(l,lp), lp=1,nls),  fk(l),fkj(l),scrjm1(l),
     1  scrjm2(l),scrl6(l),fk6(l),scrs6(l),lhqq(l),multqq(l),l=1,nls)
      noicwr=noicwr+1
c
      if (icfc.le.0) go to 970
      write (iw,91) coupl(kcpl),coupl(kcpl1)
   91 format (9h0tmx from, a6, 5h  to ,a6,9h coupling//)
      lx=0
  940 ln=lx+1
      lx=lx+11
      if (lx.gt.nls) lx=nls
      go to (943,943,943,944,945,946,947) kcpl1
  943 write (iw,93) (scrjm1(l),fkj(l), l=ln,lx)
   93 format (28x,11(f4.1,1hk,f4.1))
      go to 950
  944 lp1=scrl(l,nosubc)+1.0
      lhs4=lsymb(lp1+24)
      write (iw,94) (lhs4,fk(l), l=ln,lx)
   94 format (28x,11(2x,a1,1h(,f4.1,1h)))
      go to 950
  945 write (iw,93) (scrjm1(l),fk(l), l=ln,lx)
      go to 950
  946 write (iw,96) (scrjm2(l),lhqq(l),fk6(l),multqq(l), l=ln,lx)
   96 format (28x,11(f4.1,a1,f3.1,i1))
      go to 950
  947 write (iw,97) (scrjm2(l),multqq(l),lhqq(l),scrjm1(l),
     1    l=ln,lx)
   97 format (28x,11(f4.1,i1,a1,f3.1))
  950 do 955 l=1,nls
      if (kcpl.eq.1) go to 953
      write (iw,98) ncfg(l),fidjj(l,1), l, (tmx(l,lp), lp=ln,lx)
   98 format (i11,2x,1x,a8,i4,2x,11f9.5)
      go to 955
  953 write (iw,98) ncfg(l),fidls(l,1), l, (tmx(l,lp), lp=ln,lx)
  955 continue
      if (lx.lt.nls) go to 940
c
  970 kcpl1=kcpl1+1
      if (kcpl1.le.4) go to 110
      if (kcpl1-6) 972,110,973
  972 if (nosubc-3) 980,110,110
  973 if (kcpl1-7) 110,121,980
  980 call clock(time)
      delt=time-time0
      write (iw,99) delt
   99 format (11x,5htime=,f6.3,4h min)
      return
      end
