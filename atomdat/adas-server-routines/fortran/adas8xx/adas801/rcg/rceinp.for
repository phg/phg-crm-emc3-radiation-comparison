C UNIX-IDL - SCCS info: Module @(#)rceinp.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------
c     overlay(g9,10,6)
      subroutine rceinp
c
c          Set up tape2e for binary input to RCE,
c               and file outgine for bcd input to RCE
c
c          If nlsmax.gt.0, nls.le.nlsmax, kcpld(i)=0, and all other
c               kcpld=1, then coef matrices will be written on tape2e in
c               coupling i and transformation matrix will be tmx(i,LS).
c          Otherwise, if kcpl=1, coef matrices will be in LS, tmx(LS,JJ),
c          if kcpl=2, coef matrices will be in JJ, tmx(JJ,LS).
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c6/emin,emax,sumwt,flbdn,flbdx,fnunp,fnuxp,nlev,iewr
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/dumlc4(klc42)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      dimension nparj(kc,kc)
      equivalence (dumlc4(klc4),nparj)
      dimension illp(klc42),coeff(klc42)
      equivalence (ct4,coeff)
      character*8 sj8
      character*10 parnam(kpr,2),parnm(kpr)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      character*8 yiden(kmx,2)
      character*8 elem(3,kc,2),elem1(3)
      common/char2/elem,elem1
      common/char/parnam,fidls,fidjj,ldes,ldesp
c
c
      call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,95) delt
   95 format (//' Start rceinp at time=',f7.3,4h min/)
      rewind 2
      nocset = iabs(nocset)
      if (nocset.eq.0) nocset=1
      k=1
      il=ila
  100 j1x = nsconf(2,k)
      nj8non0=nj8k(k)
      npar = 0
      do 140 mzq=1,j1x
      npar = npar+nparj(mzq,mzq)
      do 140 nzq=mzq,j1x
      if (nzq.eq.mzq) go to 140
      npar = npar+nparj(mzq,nzq)
  140 continue
      write (2) nocset,nj8non0,npar,j1x,((nparj(l,lp),lp=l,j1x),
     1 l=1,j1x)
      npar = npar-j1x
      nord=noic(k)+1
      ispace=0
      itrans=0
      if (nosubc.eq.1) go to 151
      ii=7
      if (nosubc.lt.3) ii=4
      do 150 i=3,ii
      if (kcpld(i).ne.0) go to 150
      ispace=ispace+1
      jjj=i
  150 continue
      if (ispace.ne.1) go to 151
      itrans=1
      kcprce=jjj
c
c
  151 do 400 mzq=1,nscrj8
c               Read tmx(ls,jj)
      read (ic) scrj8,nls,nls1, ((tmx(l,lp),l=1,nls1), lp=1,nls1),
     1  ((nals(l,i),scrl(l,i),scrs(l,i),
     2  i=1,nosubc), ncfg(l),fidls(l,1),fidjj(l,1), l=1,nls1)
      if (nls.le.0) go to 400
      nord=nord+1
      i1=1
      i2=2
      if (kcpl.eq.1) go to 152
      i1=2
      i2=1
  152 do 160 l=1,nls
      yiden(l,i1)=fidls(l,1)
      yiden(l,i2)=fidjj(l,1)
c     encode (8,18,sj8) scrj8
      write (sj8,18) scrj8
   18 format (2x,2hJ=,f4.1)
c               calc g-matrix, and transform if kcpl=2
      do 154 lp=1,nls
  154 c(l,lp)=0.0
      if (scrj8.eq.0.0) go to 160
      c(l,l)=0.50116*(scrs(l,nosubc)**2+scrs(l,nosubc)-scrl(l,nosubc)**2
     1  -scrl(l,nosubc))/(scrj8**2+scrj8)+1.50116
  160 continue
      if (kcpl.eq.1) go to 165
      do 162 l=1,nls
      do 162 lp=1,nls
      ct4(l,lp)=0.0
      do 161 lpp=1,nls
  161 ct4(l,lp)=ct4(l,lp)+c(l,lpp)*tmx(lpp,lp)
  162 continue
c               Transpose from tmx(ls,jj) to tmx(jj,ls)
      do 163 l=1,nls
      do 163 lp=l,nls
      a=tmx(l,lp)
      tmx(l,lp)=tmx(lp,l)
  163 tmx(lp,l)=a
      do 164 l=1,nls
      do 164 lp=1,nls
      c(l,lp)=0.0
      do 164 lpp=1,nls
      c(l,lp)=c(l,lp)+tmx(l,lpp)*ct4(lpp,lp)
  164 continue
c               Write g-matrix on 19
  165 close (19,status='delete')
      open (19,status='scratch',form='unformatted')
      rewind 19
      write (19) ((c(l,lp),l=lp,nls), lp=1,nls)
      if (itrans.eq.0) go to 188
      if (nls.gt.nlsmax) go to 188
      do 170 l=1,nls
      yiden(l,2)=yiden(l,1)
  170 continue
      i2=i1
      i1=kcprce
      if (nls.eq.1) go to 188
  171 do 175 i=1,npar
  175 read (ic)
c               Read tmx(i,kcpl)
      read (ic) ((tmx(lp,l),lp=1,nls),fk(l),fkj(l),scrj(l,1),scrj(l,2),
     1  scrl6(l),fk6(l),scrs6(l),lhqq(l),multqq(l),l=1,nls)
      go to (176,176,176,178,180,182,184) kcprce
  176 do 177 l=1,nls
      write (yiden(l,1),21) scrj(l,1),fkj(l)
   21 format(f3.1,1hk,f4.1)
  177 continue
      go to 186
  178 do 179 l=1,nls
      lp1=scrl(l,nosubc)+1.0
      lhs4=lsymb(lp1+24)
      write (yiden(l,1),22) lhs4,fk(l)
   22 format(1x,a1,1h(,f4.1,1h))
  179 continue
      go to 186
  180 do 181 l=1,nls
      write (yiden(l,1),21) scrj(l,1),fk(l)
  181 continue
      go to 186
  182 do 183 l=1,nls
      write (yiden(l,1),23) scrj(l,2),lhqq(l),fk6(l),multqq(l)
   23 format(f3.1,a1,f3.1,i1)
  183 continue
      go to 186
  184 do 185 l=1,nls
      write (yiden(l,1),24) scrj(l,2),multqq(l),lhqq(l),scrj(l,1)
   24 format(f3.1,i1,a1,f3.1)
  185 continue
  186 npars=npar+1
c     do 187 i=1,npars
c 187 backspace ic
      rewind ic
c     nord=noic(k)+2+(mzq-1)*(npar+2)
      do 187 i=1,nord
  187 read (ic) dumdcg
c               Write tmx(i,kcpl), or (if itrans=0 or nls.gt.nlsmax)
c                 tmx(LS,JJ) if kcpl=1 or tmx(JJ,LS) if kcpl=2.
  188 write (2) sj8,nls, ((tmx(l,lp),l=1,nls), lp=1,nls),
     1  (ncfg(l),l=1,nls),((yiden(l,nzq),l=1,nls),nzq=1,2)
c     if (nls.eq.1) nonlseq1=nonlseq1+1
      npars=npar+1
      do 250 nzq=1,npars
      i=ic
c     if (nzq.ne.npars) go to 204
      if (nzq.ne.npars) go to 205
      i=19
      rewind 19
c               Read coeff (nzq.lt.npars) or g (nzq.eq.npars) matrix
  204 read (i) ((c(l,lp),l=lp,nls), lp=1,nls)
      go to 210
  205 read (i) ncoeff,(illp(n),coeff(n),n=1,ncoeff)
      nord=nord+1
      do 206 lp=1,nls
      do 206 l=lp,nls
  206 c(l,lp)=0.0
      do 207 n=1,ncoeff
      l=illp(n)/10000
      lp=illp(n)-10000*l
  207 c(l,lp)=coeff(n)
  210 if(itrans.eq.0) go to 240
      if (nls.eq.1) go to 240
      if (nls.gt.nlsmax) go to 240
c               If itrans.gt.0 and nls.le.nlsmax, transf matrix to i.
      do 220 l=2,nls
      ls=l-1
      do 220 lp=1,ls
  220 c(lp,l)=c(l,lp)
      do 230 l=1,nls
      do 230 lp=1,nls
      ct4(l,lp)=0.0
      do 229 lpp=1,nls
  229 ct4(l,lp)=ct4(l,lp)+tmx(l,lpp)*c(lpp,lp)
  230 continue
      do 235 l=1,nls
      do 235 lp=1,l
      c(l,lp)=0.0
      do 234 lpp=1,nls
  234 c(l,lp)=c(l,lp)+ct4(l,lpp)*tmx(lp,lpp)
  235 continue
c 240 write (2) ((c(l,lp),l=lp,nls), lp=1,nls)
  240 ncoeff=0
      do 245 lp=1,nls
      do 245 l=lp,nls
      if (c(l,lp).eq.0.0) go to 245
      ncoeff=ncoeff+1
      illp(ncoeff)=10000*l+lp
      coeff(ncoeff)=c(l,lp)
  245 continue
      if (ncoeff.gt.0) go to 247
      ncoeff=1
      illp(1)=10001
      coeff(1)=0.0
  247 write (2) ncoeff,(illp(i),coeff(i),i=1,ncoeff)
  250 continue
      call clock(time)
      delt=time-time0
      if (iw6.lt.0) write (6,15) mzq,nj8non0,delt,nls,npars
   15 format (' End calc of matrix',i3,' of',i3,';   time=',
     1  f8.3,',   size=',i3,',   npars=',i4)
      if (ispace.eq.0) go to 400
      if (nls.eq.1) go to 400
      if (nls.gt.nlsmax) go to 400
      do 399 i=1,ispace
  399 read (ic) dumdcg
      nord=nord+ispace
  400 continue
      call clock(time)
      delt=time-time0
      write (iw,40) nocset,delt,coupl(i1),coupl(i2)
   40 format (12h0cset number,i4,27h written on tape 2 at time=,f7.3,
     1  17h  for couplings  ,a8,7h  and  ,a8//)
      if (iw6.lt.0) write (6,40) nocset,delt,coupl(i1),coupl(i2)
c
c          Write input for RCE on disk 11, using (if a diagonalization
c               has been made) computed instead of experimental levels,
c               and using the theoretical parameter values
c               as preliminary estimates; if no diagonalization,
c               then use zeroes for the above quantities.
c
  500 nocypr=1
      nocycr=30
      nocyce=5
      write (11,51) nocypr,nocycr,nocyce,iw6
   51 format (3i5,5x,i5)
      ncdes=nocset
      idenom=0
      iprnv=1
      iprna=1
      wmax=0.0
      iprnx=1
      iprnsq=1
      scale=1.0
      write (11,52) ncdes,idenom,iprnv,iprna,wmax,iprnx,iprnsq,scale
   52 format (4i5,f10.3,2i5,f10.4)
      write (11,53) elem(1,1,k),(elem(2,i,k),elem(3,i,k), i=1,j1x)
   53 format (a8,2x,6(a6,a4)/7(a6,a4))
      npar=npar+j1x
      j=0
      do 530 i=1,npar
      write (parnm(i),523) parnam(i,k)
  523 format (a10)
      if (parnm(i)(1:3).ne.'eav') go to 530
      j=j+1
      write (parnm(i),524) elem(2,j,k)
  524 format (4heav ,a6)
  530 continue
      write (11,54) (parnm(i), i=1,npar)
   54 format (7a10)
      rewind il
      if (iewr.gt.0) go to 550
      do 540 lp=1,kmx
  540 eig(lp)=0.0
      do 545 i=1,kpr
  545 vpar(i,k)=0.0
      read (il)
  550 gre=0.0
      do 552 i=1,npar
  552 gre=max(vpar(i,k),gre)
      do 560 n=1,nj8non0
      if (iewr.gt.0) go to 558
      read (il) sj,nls
      go to 559
  558 read (il) sj,nls, (eig(lp), (eras,l=1,nls), lp=1,nls)
      read (il)
  559 if (gre.le.7.0e3) write (11,55) (eig(lp), lp=1,nls)
   55 format (7f10.4)
      if (gre.gt.7.0e3.and.gre.le.7.0e4) write(11,56) (eig(lp),lp=1,nls)
   56 format (7f10.3)
      if (gre.gt.7.0e4) write (11,57) (eig(lp), lp=1,nls)
   57 format (7f10.2)
  560 write (11,58) (lp, lp=1,nls)
   58 format (7i10)
      npar=0
      do 565 mzq=1,j1x
  565 npar=npar+nparj(mzq,mzq)
      j=-50
      do 570 i=1,npar
      multqq(i)=0
      if (iewr.eq.0.or.i.eq.1.or.vpar(i,k).gt.0.0) go to 570
      multqq(i)=-99
  570 continue
      if (j1x.eq.1) go to 600
      j1x1=j1x-1
      do 590 mzq=1,j1x1
      mzq1=mzq+1
      npar2=0
      do 585 mp=mzq1,j1x
  585 npar2=npar2+nparj(mzq,mp)
      npar2=npar+npar2
      npar=npar+1
      j=j-1
      do 588 i=npar,npar2
  588 multqq(i)=j
  590 npar=npar2
  600 write (11,58) (multqq(i), i=1,npar)
      xmax=0.0
      write (11,55) (xmax, i=1,npar)
      if (gre.le.7.0e3) write (11,55) (vpar(i,k), i=1,npar)
      if (gre.gt.7.0e3.and.gre.le.7.0e4)
     1  write (11,56) (vpar(i,k), i=1,npar)
      if (gre.gt.7.0e4) write (11,57) (vpar(i,k), i=1,npar)
      f0=1.0
      fm=0.0
      f1=-1.0
      delf=0.0
      nofmax=1000
      gmax=1.0
      dxmax=10000.0
      crit1=0.3
      crit=0.00
      icrit2=-1
      write (11,60) f0,fm,f1,delf,nofmax,gmax,dxmax,crit1,crit,icrit2
   60 format (3f10.1,f5.2,i5,f10.3,f10.2,2f5.2,i2)
      ncdes=-1
      write (11,52) ncdes
c
      if (k.eq.2) go to 900
      k=2
      il=ilb
      if (nsconf(1,k).eq.0) go to 900
      if (noicmup(1).eq.0) go to 710
      ispace=noicmup(1)
      do 700 i=1,ispace
  700 read (ic) dumdcg
  710 nocset=nocset+1
      read (ic) notsj1,nscrj8,sj8mn,sj8mx,npar,nparj,parnam
      go to 100
c
  900 return
      end
