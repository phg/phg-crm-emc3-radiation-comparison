C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/pfgd.for,v 1.2 2004/07/06 14:27:38 whitefor Exp $ Date $Date: 2004/07/06 14:27:38 $
C
c     overlay(g9,4,0)
      subroutine pfgd
c
c          Calc. preliminary single-configuration matrix elements
c
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3/    cfp2(kls2,kls2,9),cijk(10,6),
     1         u2(kls2,kls2,9),u3(kls3,kls3,9),u4(kls4,kls4,9),
     2         v2(kls2,kls2,9),v3(kls3,kls3,9),v4(kls4,kls4,9),
     2  u5(kls5,kls5,9),u6(kls6,kls6,9),v5(kls5,kls5,9),v6(kls6,kls6,9),
     3  u7(kls6,kls6,9),u8(kls6,kls6,9),v7(kls6,kls6,9),v8(kls6,kls6,9),
     3  nti(41),irp1mx,nalsji(kjp),tfln,tflx,tfsn,tfsx,
     4  pji(kjp),mulsi(kls1),lbcdi(kls1),irho,irhop,isig,isigp,fllrho,
     5  fllsig,in,jn,jx,nii,nij,ki,kj,kpi,kpj,kdmin,kemin,nkd,nke,nktot,
     6  tr,tk,tc,rsum,  j11,j12,l1,l2,tl(2,ks),ts(2,ks),
     7  tscl(2,ks),tscs(2,ks),nijkp(ks,2),irs(ks),ifl(ks),ndel(ks),
     8  cijr(20),ccc(20),n1,n2,n3,n4,e1,e2,e3,
     9  e4,e5,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,
     a  cfgp2(kls2,kls2,9),nalsjp(kjp,ks),pj(kjp,ks)
      common/c3lc1/u1(kls1,kls1,9),dumlc1(klc1)
      common/c3lc2/cfgp1(kls1,kls1,9),pc(kpc),dumlc2(klc2)
      common/c3lc3/v1(kls1,kls1,9),dumlc3(klc1)
      common/c3lc4/dumlc4(kmx**2-kiser),pci(kiser)
      dimension nparj(kc,kc),iser(kiser)
      equivalence (dumlc4,nparj),(dumlc4(kc**2+1),iser)
      common/c3lcd/dumlcd
      common/p/nalsp(klsp,ks),pscrl(klsp,ks),pscrs(klsp,ks),ncfgp(klsp),
     1  njk(kjk,ks),ncfgjp(kjp,ks),muls1(klsp),muls4(klsp),
     2  lhs1(klsp),lhs4(klsp)
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      dimension cfp1(klsp1,klsp1)
      equivalence (cfp1,cfgp1)
      dimension vfg(20,15)
      character*10 parnam(kpr,2)
      character idfg(15)*11
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
c
      character*10 ze(8),aa
      data (ze(i),i=1,8) /'Zeta 1','Zeta 2','Zeta 3','Zeta 4',
     1  'Zeta 5','Zeta 6','Zeta 7','Zeta 8'/
c
c
c     ks=8
c
      j1x=nsconf(2,kk)
      lx=ntottj(j1x+1)
      do  97 l=1,lx
      muls1(l)=2.0*pscrs(l,1)+1.0
      muls4(l)=2.0*pscrs(l,nosubc)+1.0
      i=pscrl(l,1)+1.0
      lhs1(l)=lsymb(i+24)
      i=pscrl(l,nosubc)+1.0
   97 lhs4(l)=lsymb(i+24)
c
      npar=0
      nfg=0
      nparjo=0
      rewind 20
c
      do 599 j1=1,j1x
      ll(nosubc)=llijk(nosubc,j1,kk)
      fll(nosubc)=fllijk(nosubc,j1,kk)
      npar=npar+1
      parnam(npar,kk)=parnam(1,1)
      do 99 i=1,ks
   99 ni(i)=nijk(i,j1,kk)
c
c          Obtain equivalent-electron Coulomb coefficients fk(i,i)
c               from disk 74, and terms of li(ni) from disk 72
c
  100 do 199 i=1,nosubc
      if (ni(i).le.1) go to 199
      n=4.0*fll(i)
      if (ni(i).gt.n) go to 199
      call locdsk(id2,fll(i),ni(i))
      read (id2) (mulsi(m),lbcdi(m),ia,a,a, m=1,nlt)
        do 110 m=1,nlt
        do 110 j=1,24
        if (lbcdi(m).eq.lsymb(j)) lbcdi(m)=lsymb(j+24)
  110   continue
      ia=ia
      call locdsk(id4,fll(i),ni(i))
      if (nsconf(1,kk).gt.1) go to 115
      if (sjn.gt.0.0.or.sjx.lt.99.0) go to 115
      read (id4)
      go to 170
c
  115 read (id4) ndiffi,(nti(k),k=1,ndiffi)
c
c          Find correlation between terms of conf. j1 and terms of li(ni)
c
      nopc=0
      mx=0
      do 169 k=1,ndifft
      mn=mx+1
      mx=mx+ntrmk(k)
      if (mx.le.ntottj(j1)) go to 169
      if (mx.gt.ntottj(j1+1)) go to 170
      do 159 m=mn,mx
      j=nalsp(m,i)
      npi0=0
      mxi=0
      do 125 k1=1,ndiffi
      mni=mxi+1
      mxi=mxi+nti(k1)
      if (mult(j,i).ne.mulsi(mni)) go to 125
      if (lbcd(j,i).eq.lbcdi(mni)) go to 130
  125 npi0=npi0+(nti(k1)*(nti(k1)+1))/2
  130 do 149 mp=mn,m
      jp=nalsp(mp,i)
      nopc=nopc+1
           if (nopc.gt.kpc-5) write (6,1130) nopc
 1130 format (' nopc=',i7)
      pc(nopc)=0.0
      iser(nopc)=-7
      if (mult(j,i).ne.mult(jp,i)) go to 149
      if (lbcd(j,i).ne.lbcd(jp,i)) go to 149
      do 133 l=1,nosubc
      if (pscrl(m,l).ne.pscrl(mp,l)) go to 149
      if (pscrs(m,l).ne.pscrs(mp,l)) go to 149
      if (l.eq.i) go to 133
      if (nalsp(m,l).ne.nalsp(mp,l)) go to 149
  133 continue
      npi=npi0
      do 139 mi=mni,mxi
      do 135 mpi=mni,mi
      npi=npi+1
      if (j-notsj1(i,j1).ne.mi) go to 135
      if (jp-notsj1(i,j1).ne.mpi) go to 135
      iser(nopc)=npi
      go to 149
  135 continue
  139 continue
  149 continue
  159 continue
  169 continue
c
c          Write preliminary coefficients pc on disk 20
c
  170 npari=norcd-2
      if (iabg.eq.0) npari=fll(i)
      do 198 n=1,npari
      if (nsconf(1,kk).gt.1) go to 175
      if (sjn.gt.0.0.or.sjx.lt.99.0) go to 175
      read (id4) kpar,k,aa,cave,nopc, (pc(j),j=1,nopc)
      if (kpar.eq.1) go to 198
      go to 185
  175 read (id4) kpar,k,aa,cave,npi, (pci(j),j=1,npi)
      if (kpar.eq.1) go to 198
      do 180 j=1,nopc
      if (iser(j).le.0) go to 180
      l=iser(j)
      pc(j)=pci(l)
  180 continue
c
  185 npar=npar+1
      nfg=nfg+1
      if (k.ne.0) go to 186
      if (iabg) 198,198,188
  186 write (aa,14) k,i,i
   14 format (1hF,i1,1h(,2i1,5h)    )
  188 parnam(npar,kk)=aa
      write (20) npar,kpar,k,i,i,aa,cave,j1,nopc, (pc(n1), n1=1,nopc)
      if (icpc.le.1.or.icpc.eq.3) go to 197
      write (iw,16) aa,cave
   16 format (1h2,a10,5x,5hcave=,f12.7//)
      nopc=0
      lfg=0
      mx=0
      do 191 l=1,ndifft
      mn=mx+1
      mx=mx+ntrmk(l)
      if (mx.le.ntottj(j1)) go to 191
      if (mx.gt.ntottj(j1+1)) go to 197
      do 190 m=mn,mx
      j=nalsp(m,i)
      n2=nopc+1
      nopc=n2+m-mn
      write (iw,17) ncfgp(j),nopc,mult(j,i),lbcd(j,i),muls4(m),
     1  lhs4(m), (pc(n1), n1=n2,nopc)
   17 format (2i5,5h    (,i1,a1,1h),i4,a1, 10f10.5/(25x, 10f10.5))
      if (icpc.lt.5) go to 190
      lfg=lfg+1
      vfg(lfg,nfg)=pc(nopc)
      write (idfg(nfg),19) k,i,ll(i),i,ll(i)
   19 format (1hF,i1,1h(,i2,a1,1h,,i2,a1,1h))
  190 continue
  191 continue
  197 if (icpc.eq.0) go to 198
      call clock(time)
      delt=time-time0
      write (iw,18) aa,nopc,cave,delt, (ll(l),ni(l), l=1,nosubc)
   18 format (/1h ,a10,5x,3hpc(,i5,1h),5x,5hcave=,f12.7,17x,5hdelt=,
     1  f6.3,4h min,7x, 8(a1,i2,3x))
  198 continue
  199 continue
c
c          Obtain spin-orbit coefficients zeta(i) from disk 74
c
  200 do 299 i=1,nosubc
      if (ni(i).le.0) go to 299
      if (fll(i).le.0.0) go to 299
      nn=4.0*fll(i)+2.0
      if (ni(i).ge.nn) go to 299
      if (ni(i).gt.1.and.ni(i).lt.nn-1) go to 205
      norcd=2
      m=1
      nti(1)=1
      n1=2
      nalsji(1)=1
      nalsji(2)=1
      pji(1)=fll(i)+0.5
      pji(2)=fll(i)-0.5
      ndifji=2
      nti(1)=1
      nti(2)=1
      go to 220
  205 call locdsk(id4,fll(i),ni(i))
      if (nsconf(1,kk).gt.1) go to 215
      read (id4)
      go to 270
c
  215 read (id4) m,(nti(k),k=1,m), n1,(nalsji(k),pji(k),k=1,n1),
     2  ndifji,(nti(k),k=1,ndifji)
c
c          Find correl. between levels in conf j1 and levels of li(ni)
c
  220 nopc=0
      mx=0
      kx=ndiffj(i)
      do 269 k=1,kx
      mn=mx+1
      mx=mx+njk(k,i)
      if (mx.le.ntotjj(i,j1)) go to 269
      if (mx.gt.ntotjj(i,j1+1)) go to 270
      do 259 m=mn,mx
      j=nalsjp(m,i)-notsj1(i,j1)
      npi0=0
      mxi=0
      do 225 ki=1,ndifji
      mni=mxi+1
      mxi=mxi+nti(ki)
      if (pj(m,i).eq.pji(mni)) go to 230
  225 npi0=npi0+(nti(ki)*(nti(ki)+1))/2
  230 do 249 mp=mn,m
      jp=nalsjp(mp,i)-notsj1(i,j1)
      nopc=nopc+1
      pc(nopc)=0.0
      iser(nopc)=-7
      if (pj(m,i).ne.pj(mp,i)) go to 249
      npi=npi0
      do 239 mi=mni,mxi
      do 235 mpi=mni,mi
      npi=npi+1
      if (j.ne.nalsji(mi)) go to 235
      if (jp.ne.nalsji(mpi)) go to 235
      iser(nopc)=npi
      go to 249
  235 continue
  239 continue
  249 continue
  259 continue
  269 continue
c
c          Write pc on disk 20
c
  270 npari=norcd-1
      do 289 n=1,npari
      if (ni(i).gt.1.and.ni(i).lt.nn-1) go to 271
      a=-0.5
      if (ni(i).eq.1) a=0.5
      eras1=a*fll(i)
      eras2=-a*(fll(i)+1.0)
      kpar=1
      k=0
      cave=0.0
      if (nsconf(1,kk).gt.1) go to 1270
      pc(1)=eras1
      pc(2)=eras2
      go to 285
 1270 pci(1)=eras1
      pci(2)=eras2
      go to 277
  271 if (n.eq.npari) go to 272
      read (id4)
      go to 289
  272 if (nsconf(1,kk).gt.1) go to 275
      read (id4) kpar,k,aa,cave,nopc, (pc(j),j=1,nopc)
      go to 285
  275 read (id4) kpar,k,aa,cave,npi, (pci(j),j=1,npi)
  277 do 280 j=1,nopc
      if (iser(j).le.0) go to 280
      l=iser(j)
      pc(j)=pci(l)
  280 continue
  285 npar=npar+1
      aa=ze(i)
      parnam(npar,kk)=aa
      write (20) npar,kpar,k,i,i,aa,cave,j1,nopc, (pc(n1), n1=1,nopc)
      if (icpc.le.1.or.icpc.eq.3) go to 288
      write (iw,16) aa,cave
      nopc=0
      mx=0
      kx=ndiffj(i)
      do 287 k=1,kx
      mn=mx+1
      mx=mx+njk(k,i)
      if (mx.le.ntotjj(i,j1)) go to 287
      if (mx.gt.ntotjj(i,j1+1)) go to 288
      do 286 m=mn,mx
      j=nalsjp(m,i)
      n2=nopc+1
      nopc=n2+m-mn
  286 write (iw,28) ncfgjp(m,i),nopc,mult(j,i),lbcd(j,i),pj(m,i),
     1  (pc(n1),n1=n2,nopc)
   28 format (3i5,a1,f4.1,10f11.6/(22x,10f11.6))
  287 continue
  288 if (icpc.eq.0) go to 289
      call clock(time)
      delt=time-time0
      write (iw,18) aa,nopc,cave,delt, (ll(l),ni(l), l=1,nosubc)
  289 continue
  299 continue
c
c          Calculate Coulomb direct coefficients fk(i,j)
c
      noscm1=nosubc-1
      if (noscm1.le.0) go to 498
      irho=0
c          Read reduced matrix elements U,V from disk 73
      do 9304 i=1,nosubc
      if (ni(i).le.1) go to 9304
      n=4.0*fll(i)+2.0
      if (ni(i).eq.n) go to 9304
      call locdsk(id3,fll(i),ni(i))
      nor=norcd/2
      do 303 k=1,nor
      go to (1303,2303,3303,4303,5303,6303,7303,8303), i
 1303 read (id3) n, ((u1(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 2303 read (id3) n, ((u2(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 3303 read (id3) n, ((u3(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 4303 read (id3) n, ((u4(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 5303 read (id3) n, ((u5(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 6303 read (id3) n, ((u6(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 7303 read (id3) n, ((u7(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 303
 8303 read (id3) n, ((u8(ii,jj,k),ii=1,nlt),jj=1,nlt)
  303 continue
      do 304 k=1,nor
      go to (1304,2304,3304,4304,5304,6304,7304,8304), i
 1304 read (id3) n, ((v1(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 2304 read (id3) n, ((v2(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 3304 read (id3) n, ((v3(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 4304 read (id3) n, ((v4(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 5304 read (id3) n, ((v5(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 6304 read (id3) n, ((v6(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 7304 read (id3) n, ((v7(ii,jj,k),ii=1,nlt),jj=1,nlt)
      go to 304
 8304 read (id3) n, ((v8(ii,jj,k),ii=1,nlt),jj=1,nlt)
  304 continue
 9304 continue
c
      do 399 i=1,noscm1
      nii=ni(i)
      if (ni(i)) 399,399,306
  306 if (fll(i)) 399,399,307
  307 n=4.0*fll(i)+2.0
      if (ni(i)-n) 308,399,399
  308 i1=i+1
      do 389 j=i1,nosubc
      nij=ni(j)
      if (ni(j)) 389,389,310
  310 if (fll(j)) 389,389,311
  311 n=4.0*fll(j)+2.0
      if (ni(j)-n) 312,389,389
  312 if (fll(i)-fll(j)) 313,314,314
  313 kmax=2.0*fll(i)
      go to 315
  314 kmax=2.0*fll(j)
  315 do 385 k=1,kmax
      if (iabg.ne.2.and.iabg.ne.4.and.mod(k,2).ne.0) go to 385
      if (icpc.le.1.or.icpc.eq.3) go to 320
      write (iw,31)
   31 format (1h2)
      write (iw,32) k,i,j
   32 format (2h F,i1,1h(,i1,1h,,i1,1h)//)
  320 fk=k
      cij=cijkf(fll(i),fll(i),fk)*cijkf(fll(j),fll(j),fk)
      if (mod(k,2).ne.0) cij=1.0
c
  330 nopc=0
      nfg=nfg+1
      lfg=0
      mmax=0
      do 383 l=1,ndifft
      mmin=mmax+1
      mmax=mmax+ntrmk(l)
      if (ncfgp(mmax).ne.j1) go to 383
      notsi=notsj1(i,j1)
      notsj=notsj1(j,j1)
      do 382 m=mmin,mmax
      ji=nalsp(m,i)
      jj=nalsp(m,j)
      ki=ji-notsi
      kj=jj-notsj
      do 381 mp=mmin,m
      jpi=nalsp(mp,i)
      jpj=nalsp(mp,j)
      kpi=jpi-notsi
      kpj=jpj-notsj
      nopc=nopc+1
      pc(nopc)=0.0
      do 341 ip=1,nosubc
      if (pscrs(m,ip)-pscrs(mp,ip)) 381,331,381
  331 if (ip-i) 339,332,335
  332 if (s(ji,i)-s(jpi,i)) 381,341,381
  335 if (ip-j) 340,336,339
  336 if (pscrl(m,ip)-pscrl(mp,ip)) 381,337,381
  337 if (s(jj,j)-s(jpj,j)) 381,341,381
  339 if (pscrl(m,ip)-pscrl(mp,ip)) 381,340,381
  340 if (nalsp(m,ip)-nalsp(mp,ip)) 381,341,381
  341 continue
      do 350 iii=1,2
      mm=m
      if (iii.eq.2) mm=mp
      do 350 ip=1,nosubc
      ii=nalsp(mm,ip)
      tl(iii,ip)=fl(ii,ip)
      ts(iii,ip)=s(ii,ip)
      tscl(iii,ip)=pscrl(mm,ip)
  350 tscs(iii,ip)=pscrs(mm,ip)
      tk=fk
      call rdij(i,j)
c
      pc(nopc)=a*cij
  381 continue
      if (icpc.le.1.or.icpc.eq.3) go to 382
      n2=nopc-m+mmin
      write (iw,17) ncfgp(m),nopc,muls1(m),lhs1(m),muls4(m),
     1  lhs4(m), (pc(n1), n1=n2,nopc)
      if (icpc.lt.5) go to 382
      lfg=lfg+1
      vfg(lfg,nfg)=pc(nopc)
      write (idfg(nfg),19) k,i,ll(i),j,ll(j)
  382 continue
  383 continue
      npar=npar+1
      kpar=-1
      cave=0.0
      write (aa,14) k,i,j
      parnam(npar,kk)=aa
      write (20) npar,kpar,k,i,j,aa,cave,j1,nopc, (pc(n1), n1=1,nopc)
      if (icpc.eq.0) go to 385
      call clock(time)
      delt=time-time0
      write (iw,18) aa,nopc,cave,delt, (ll(l),ni(l), l=1,nosubc)
  385 continue
  389 continue
  399 continue
c
c          Calculate Coulomb exchange coefficients gk(i,j)
c
  400 do 497 i=1,noscm1
      nii=ni(i)
      if (ni(i)) 497,497,401
  401 n=4.0*fll(i)+2.0
      if (ni(i)-n) 403,497,497
  403 i1=i+1
      do 489 j=i1,nosubc
      nij=ni(j)
      if (ni(j)) 489,489,405
  405 n=4.0*fll(j)+2.0
      if (ni(j)-n) 406,489,489
  406 kmax=fll(i)+fll(j)
      kmin=abs(fll(i)-fll(j))
      kdmin=0
      irp1mx=kmax-kmin+1
      mn=i+1
      mx=j-1
      do 485 k=kmin,kmax
      if (iabg.ne.2.and.iabg.ne.4.and.mod(k-kmin,2).ne.0) go to 485
      fk=k
      cij=(cijkf(fll(i),fll(j),fk))**2
      a=ni(i)*ni(j)
      cave=a*s3j0sq(fll(i),fk,fll(j))/2.0
      lpl=fll(i)+fll(j)+fk
      if (mod(lpl,2).eq.0) go to 407
      cij=0.5*((-1.0)**k)
      cave=-cij*a/((2.0*fll(i)+1.0)*(2.0*fll(j)+1.0))
      cij=2.0*cij
  407 if (icpc.le.1.or.icpc.eq.3) go to 410
      write (iw,31)
      write (iw,40) k,i,j,cave
   40 format (2h G,i1,1h(,i1,1h,,i1,1h),6x,5hcave=,f12.7//)
c
  410 nopc=0
      nfg=nfg+1
      lfg=0
      mmax=0
      do 483 l=1,ndifft
      mmin=mmax+1
      mmax=mmax+ntrmk(l)
      if (ncfgp(mmax).ne.j1) go to 483
      notsi=notsj1(i,j1)
      notsj=notsj1(j,j1)
      do 482 m=mmin,mmax
      ji=nalsp(m,i)
      jj=nalsp(m,j)
      ki=ji-notsi
      kj=jj-notsj
      do 481 mp=mmin,m
      jpi=nalsp(mp,i)
      jpj=nalsp(mp,j)
      kpi=jpi-notsi
      kpj=jpj-notsj
      nopc=nopc+1
      pc(nopc)=0.0
      do 417 ip=1,nosubc
      if (ip-i) 414,417,413
  412 if (nalsp(m,ip)-nalsp(mp,ip)) 481,417,481
  413 if (ip-j) 412,415,414
  414 if (nalsp(m,ip)-nalsp(mp,ip)) 481,415,481
  415 if (pscrl(m,ip)-pscrl(mp,ip)) 481,416,481
  416 if (pscrs(m,ip)-pscrs(mp,ip)) 481,417,481
  417 continue
c
      do 450 iii=1,2
      mm=m
      if (iii.eq.2) mm=mp
      do 450 ip=1,nosubc
      ii=nalsp(mm,ip)
      tl(iii,ip)=fl(ii,ip)
      ts(iii,ip)=s(ii,ip)
      tscl(iii,ip)=pscrl(mm,ip)
  450 tscs(iii,ip)=pscrs(mm,ip)
      fllrho=fll(i)
      fllrhop=fll(i)
      fllsig=fll(j)
      fllsigp=fll(j)
      tk=fk
      call reij(i,j)
c
      pc(nopc)=cij*rsum
      if (m.ne.mp) go to 481
      pc(nopc)=pc(nopc)+cave
  481 continue
c
      if (icpc.le.1.or.icpc.eq.3) go to 482
      n2=nopc-m+mmin
      write (iw,17) ncfgp(m),nopc,muls1(m),lhs1(m),muls4(m),
     1  lhs4(m), (pc(n1), n1=n2,nopc)
      if (icpc.lt.5) go to 482
      lfg=lfg+1
      vfg(lfg,nfg)=pc(nopc)
      write (idfg(nfg),47) k,i,ll(i),j,ll(j)
   47 format (1hG,i1,1h(,i2,a1,1h,,i2,a1,1h))
  482 continue
  483 continue
      npar=npar+1
      kpar=0
      write (aa,48) k,i,j
   48 format (1hG,i1,1h(,2i1,5h)    )
      parnam(npar,kk)=aa
      write (20) npar,kpar,k,i,j,aa,cave,j1,nopc, (pc(n1), n1=1,nopc)
      if (icpc.eq.0) go to 485
      call clock(time)
      delt=time-time0
      write (iw,18) aa,nopc,cave,delt, (ll(l),ni(l), l=1,nosubc)
  485 continue
  489 continue
  497 continue
c
  498 nparj(j1,j1)=npar-nparjo
      nparjo=npar
  499 continue
      if (icpc.lt.5) go to 599
c
c          Write RCN input coefficients for LS-dependent H-F calcns.
c
      do 510 k=1,lfg
c      m1=2.0*pscrs(k,1)+1.0 !line seems unnecessary - ADW 01/02/02
      l1=pscrl(k,1)
      l1=lsymb(l1+25)
      m=2.0*pscrs(k,nosubc)+1.0
      l=pscrl(k,nosubc)
      l=lsymb(l+25)
  510 write (11,51) (llijk(i,j1,kk),nijk(i,j1,kk),i=1,3),
     1  m,l,nfg,(vfg(k,j),idfg(j),j=1,nfg)
   51 format (3(a1,i1),i2,a1,i5,5x,3(f9.5,a11)/
     1  (4(f9.5,a11)))
      write (11,28)
c
  599 continue
  900 return
      end
