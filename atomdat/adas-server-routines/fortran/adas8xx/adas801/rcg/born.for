C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/born.for,v 1.2 2004/07/06 11:42:55 whitefor Exp $ Date $Date: 2004/07/06 11:42:55 $
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  07-01-2000 : Increase ktran to 30000.
C               Extend /pwborn/ common block.
C               Add /char/ common block and t_ldsp, t_ldesp variables.
C               Remove writing output from this subroutine to IW.
C               Send all output to unit 77.
C               Do not modify neutral cross-sections.
C
C  12-06-2003 : Interpolating goss failed occasionally when using
C               aknint() and a negative value was returned. In these
C               cases try xxspln and if this fails set the value
C               to zero. 
C
C               Change dimensions of internal only variables from
C               kcase to klam and remove kcase.
C
C               Transfer spectal identification with b_ variables
C               to facilitate future separation between spectral
C               lines and collision strength calculations.
C
C
C  Martin O'Mullane 
C  
C-----------------------------------------------------------------------
c     overlay(g9,10,5)
      subroutine born
c
c          A routine to integrate generalised oscillator strengths
c               to give plane-wave Born collision strengths
c                    (W. D. Robb and R. D. Cowan, 1980)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/c3lc1/d(kmx,kmx)
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      common/spec/fjt(klam),fjtp(klam),ncserp(klam),gaat(klam),
     1  gaapt(klam)
      common/c3lc5/goss(ktran,kbgks)
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,intdum,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c3b/t(klam),tp(klam),s2(klam),soabss(klam),
     1  aa(kmx),aap(kmx),gaa(klam),gaap(klam),brnch(klam),
     2  icut(66),sgfcut(66),sfcut(66),didgf(66),dsgfdgf(66),title(20),
     3  ficut(66),fnucut(66),fnuplt(82),sgf(66),fiplt(82),sgfplt(82),
     4  ncser(klam),aat(kmx),aapt(kmx)
      common/akn/xbar,n,m,ybar
      common/c5/iarg1,iarg2,iarg3,igen,irnq,irxq,irnd,irxd,irmn,irmx,
     1  delekev,deleav,scale,fact0(5),emina,scrlkp(14,2),
     2  scrskp(14,2),nclskp(2),nolskp
      common/c9/l1,l2,cflag(kc,2),cgengy(kc,2),swt(kc,2),imax
      common/pwborn/nbigks,nenrgs,ekminl,ekmaxl,delekl,bigks(kbgks),
     1  xmin,xmax,tev(25),corr(kenrgs),ibk,ipwr,
     2  i11,ijkion,ijkiz,newstyle,temps(25),xsec(kenrgs)

      dimension fst(kbgks),en(kenrgs),ncs(klam,2)
      dimension xs(kenrgs),cs(kenrgs,klam),csm(kenrgs,klam),
     1  gfst(klam),csm2(kenrgs),decswt(klam),cswt(klam),c1(kenrgs),
     2  c2(kenrgs),fk(51),yk(51),fmxel(51),sumi(51),ratem2(kenrgs)
c      equivalence (cs,gaa),(csm,gaap),(decswt,aap)
      character*8 elem(3,kc,2),elem1(3)
      common/char2/elem,elem1
      character*1 cflag
      data t1,t2,t3,t4,t5/6h cowan,6h mpwb ,6h total,6h mod2 ,6h  pwb /

C------Start ADAS conversion-------

      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      character*8 t_ldes, t_ldesp

      character*8 lldes(klam),lldesp(klam)
      common /batch/is_batch, i_tot, lldes,lldesp
      
      character*8 b_lldes(ktran), b_lldesp(ktran)
      common /bornextra/b_t(ktran), b_tp(ktran), b_fjt(ktran),
     &                  b_fjtp(ktran), b_lldes, b_lldesp,
     &                  b_ncser(ktran), b_ncserp(ktran), b_s2(ktran)

      logical  lsetx
      integer  iopt, nin, nout
      real*8   r8fun1, xout(1), yout(1), dy(kenrgs), yin(kenrgs)
      external r8fun1
      
      lsetx = .TRUE.
      iopt  = 0
      nin   = nbigks
      nout  = 1
     
C------End ADAS conversion-------
c
c          Set punch quantities
c
c     kcase=50
      m1=-nenrgs
      m2=30
      m3=0
      m4=1
      m5=-m1
      an=nenrgs-1
      m=3
      npts=nbigks-1
      nk=nenrgs+1
      nk2=nk/2
      
c
c          Initialize arrays
c
      ics=1
      ncs(ics,1)=b_ncser(1)
      ncs(ics,2)=b_ncserp(1)
      do 110 k=1,klam
      gfst(k)=0.0
      decswt(k)=0.0
      cswt(k)=0.0
      do 110 i=1,nenrgs
      csm(i,k)=0.0
  110 cs(i,k)=0.0
      enmax=exp(2.0*ekmaxl)
      do 120 i=1,nbigks
  120 fst(i)=exp(bigks(i))
C      write (iw,12) nbigks,enmax, (fst(i),i=1,nbigks)
   12 format (1h1//20h k-values,   nbigks=,i3,8h  enmax=,f14.7//
     1  (10f12.7))
c
c          Loop over the transitions
c
      ipwr=mod(ipwr,2)
      swt0=0.0
      tr0=-9999999.0
      fjtr0=-1.0
      
C------Start ADAS conversion-------

      if (is_batch.eq.-2) write(6,*)imax
      
C------End ADAS conversion-------

      do 490 it=1,imax
      tr=b_t(it)
      tpr=b_tp(it)
      fjtr=b_fjt(it)
      fjtpr=b_fjtp(it)

C------Start ADAS conversion-------

      t_ldes=b_lldes(it)
      t_ldesp=b_lldesp(it)
            
      i_tot = i_tot+1
      if (is_batch.eq.-2.and.(mod(i_tot,10).eq.0)) write(6,*)is_batch

      if (tr.ne.tr0.or.fjtr.ne.fjtr0) swt0=swt0+2.0*fjtr+1.0
      tr0=tr
      fjtr0=fjtr
      do 200 i=1,ibk
        fst(i)=goss(it,i)
        yin(i)=log10(fst(i)) 
  200 continue
  
C------End ADAS conversion-------
      
      fnu=abs(tpr-tr)
      eras1=fnu*uenrgy
      deltae=eras1/109737.31
      c=3.0/deltae
      if (ipwr.eq.0) c=15.0*c
      flam=1.0e8/eras1
      nc1=b_ncser(it)
      nc2=b_ncserp(it)
      ict=0
      do 220 id=1,ics
      ni1=ncs(id,1)
      ni2=ncs(id,2)
      if (nc1.eq.ni1.and.nc2.eq.ni2) ict=id
  220 continue
      if (ict.gt.0) go to 230
      ics=ics+1
      ncs(ics,1)=nc1
      ncs(ics,2)=nc2
      ict=ics
  230 continue
c
c          Integrate the gen.osc.str. over a logarithmic mesh for
c               various incident electron energies
c
      gf=goss(it,1)
      gfst(ict)=gfst(ict)+gf
      e1=xmin*deltae
      e2=xmax*deltae
      e1l=log(e1)
      e2l=log(e2)
      ei=(e2l-e1l)/an
      do 300 k=1,nenrgs
      el=e1l+(k-1)*ei
  300 en(k)=exp(el)
C      write (iw,30)
   30 format (/1h0,14x,7h   e   , 4x,2hj ,10x,8h   ep   , 3x,3h jp,12x,
     1  8h delta e,3x,9hlambda(a),2x,9hs/pmax**2,4x,2hgf,5x,6hlog gf,3x,
     2  9hga(sec-1)/)
      alggf=-99.999
      if (gf.gt.0.0) alggf=log10(gf)
      ga=0.66698*gf*eras1*eras1

C------Start ADAS conversion-------

C      write (iw,32) it,nc1,nc2,tr,fjtr,tpr,fjtpr,fnu,flam,
C     1  s2(it),gf,alggf,ga
C   32 format (1h ,i4,2i2,f13.3, f6.1,f18.3, f6.1,f20.2,f11.3,f11.5,
C     1  f9.4,f9.4,1pe12.3)
C      if (igen.lt.9) write (iw,33) (goss(it,k), k=1,nbigks)
C   33 format (36h0Weighted generalized osc. strengths/(10f12.8))
C      write (iw,34)
C   34 format (//16x,2h x,11x,5he(ry),10x,5homega,7x,8homega*fx,
C     1  7x,7homegam2,10x,5ht(ev),3x,15hratem2(cm3/sec),2x,9hcorr(pct)/)

      WRITE (77,32) IT,NC1,NC2,t_ldes,t_ldesp,TR,FJTR,TPR,FJTPR,
     &              FNU,FLAM,b_S2(IT),GF,ALGGF,GA

   32 format (1h ,i5,2i2,2(1x,a8,1x),f13.3, f6.1,f12.3,
     &        f6.1,f10.2,f12.3,f9.4,f9.4,f9.4,1pe12.3)
     
     

C------End ADAS conversion-------

      n=nbigks
      ek0=deltae*3.0
      do 480 ken=1,nk
      ken1=ken-1
      if (ken.gt.1) ek0=en(ken1)
      ekn=ek0-deltae
      ak0=sqrt(ek0)
      akn=sqrt(ekn)
      akmax=ak0+akn
      akmin=ak0-akn
      akmaxl=log(akmax)
      akminl=log(akmin)
      akintl=(akmaxl-akminl)/npts
      xbar=akminl-akintl
      akxx=1.000001*akmaxl
      am=1.0
      sum=0.0
      ii=0
  350 continue
      xbar=xbar+akintl
      if (xbar.gt.akxx) go to 390
      call aknint(bigks,fst)

C------Start ADAS conversion-------

      if (ybar.LT.0.0D0) then
           xout(1) = xbar
           yout(1) = 0.0D0
           call  xxspln( lsetx , iopt  , r8fun1 ,
     &                   nin   , bigks , yin    ,
     &                   nout  , xout  , yout   ,
     &                   dy
     &                    )
           ybar = max(10.0D0**yout(1), 0.0D0)
      endif

C------End ADAS conversion-------

      sum=sum+ybar*am
      if (am.gt.3.0) go to 370
      am=4.0
      if (ken.ne.nk) go to 350
      ii=ii+1
      fk(ii)=exp(xbar)
      yk(ii)=ybar
      eras=c*ybar
      if (ipwr.eq.0) eras=eras/(fk(ii)**2)
      fmxel(ii)=0.0
      if (eras.gt.0.0) fmxel(ii)=sqrt(eras)
      if (eras.lt.0.0) fmxel(ii)=-sqrt(-eras)
      sumi(ii)=sum-ybar
      go to 350
  370 am=2.0
      go to 350
  390 sum=sum-ybar
      sum=sum*akintl/3.0
c
c          Evaluate, store, and modify the pwB collision strength
c
      colls=8.0*sum/deltae
      xe=ek0/deltae
      if (ken.eq.1) collm=colls
      xe1=(1.0-xe)*0.07702
      fx=1.0-0.2*exp(xe1)
      collsm=colls*fx
      if (xe.lt.3.0) collsm=collm*fx
      if (ken.eq.1) go to 480
      xs(ken1)=xe
      
C------Start ADAS conversion-------

      xsec(ken1)=xe

C------End ADAS conversion-------

      c1(ken1)=colls
      c2(ken1)=collsm
      cs(ken1,ict)=cs(ken1,ict)+colls
      csm(ken1,ict)=csm(ken1,ict)+collsm
  480 continue
      decswt(ict)=decswt(ict)+deltae*c1(nk2)
      cswt(ict)=cswt(ict)+c1(nk2)
      n=nenrgs
      do 485 i=1,nenrgs
      csm2(i)=0.0
      if ((c1(i)+c2(i)).eq.0.0) go to 485
      xbar=xs(i)+3.0/(1.0+xs(i))
      call aknint(xs,c1)
      csm2(i)=ybar
  485 continue
      eras=2.0*fjtr+1.0

C------Start ADAS conversion-------

C      call rcoeff(xs,csm2,ratem2,deltae,eras,uenrgy)
C      do 486 i=1,nenrgs
C      i2=i+i1
C  486 write (iw,48) xs(i),en(i),c1(i),c2(i),csm2(i),tev(i2),ratem2(i),
C     1  corr(i)
C   48 format (8x,5f14.7,f15.1,1pe15.4,0pf11.1)
   
      if (ijkion.eq.0) then
        call rcoeff(xs,c1,ratem2,deltae,eras,uenrgy)                    
      else
        call rcoeff(xs,csm2,ratem2,deltae,eras,uenrgy)                  
      endif
      
      write(77,4889)nenrgs
      if (ijkion.eq.0) then
        write(77,4888)(c1(jjj),jjj=1,nenrgs)
      else
        write(77,4888)(csm2(jjj),jjj=1,nenrgs)
      endif
      write(77,4888)(ratem2(jjj),jjj=1,nenrgs)
 
 4888 format(7(1pd10.3))
 4889 format(1x,i4)
      
C------End ADAS conversion-------
      
      if (igen.gt.5) go to 490
      iii=1
      if (ii.gt.ila) iii=2
      do 488 i=1,ii,iii
  488 sumi(i)=sumi(i)/(sumi(ii)+1.0e-15)
C      write (iw,49) akmin,akmax,(fk(i),yk(i),fmxel(i),sumi(i),i=1,ii)
   49 format (//2f12.6,16x,1hk,12x,2hgf,10x,6hmxelem,9x,3hint/
     1  (30x,4f14.8))
  490 continue
c
c          Punch the total collision strength
c
      n=nenrgs
      do 890 jtn=1,ics
      nc1=ncs(jtn,1)
      nc2=ncs(jtn,2)
      if (nc1.eq.0) goto 890
      fprt=0.0
      if (l1.ne.l2) fprt=gfst(jtn)/swt(nc1,1)
      deav=abs(cgengy(nc2,l2)-cgengy(nc1,l1))
      de1=abs(cgengy(nc2,l2)-t(1)*uenrgy/109737.31)
      de2=0.0
      if (cswt(jtn).gt.0.0) de2=decswt(jtn)/cswt(jtn)
      do 810 i=1,nenrgs
      xbar=xs(i)+3.0/(1.0+xs(i))
      call aknint(xs,cs(1,jtn))
  810 csm2(i)=ybar
      deltae=max(deav,de2)
      if (deltae.eq.0.0) deltae=de1

C------Start ADAS conversion-------

C      call rcoeff(xs,csm2,ratem2,deltae,swt0,uenrgy)
C      write (iw,60)
C   60 format (1h1,//20x,44h ****     Total collision strengths     ****
C     1  ////)
C      write (iw,81) m1,m2,m3,m4,t1,t2,(elem(l,nc1,l1),l=1,3),
C     1  cflag(nc1,l1),cflag(nc2,l2),(elem(l,nc2,l2),l=1,3),t3
   81 format (4i5,5a6,a1,4h -- ,a1,4a6)
C      write (iw,84) deav,fprt,de1,de2,deltae
C   84 format (/6h deav=,f15.8,12h ry,    fav=,f14.10,3f15.8///
C     1  13x,1hx,13x,2hcs,13x,3hcsm,6x,14hxeff=x+3/(1+x),
C     2  10x,5hT(eV),3x,15hratem2(cm3/sec),2x,9hcorr(pct)/)
C      do 850 i=1,nenrgs
C      i2=i+i1
C  850 write (iw,86) xs(i),cs(i,jtn),csm(i,jtn),csm2(i),
C     1  tev(i2),ratem2(i),corr(i)
C   86 format (3x,4f15.9,f17.1,1pe15.4,0pf11.1)

      if (ijkion.eq.0) then
        call rcoeff(xs,c1,ratem2,deltae,eras,uenrgy)                    
      else
        call rcoeff(xs,csm2,ratem2,deltae,eras,uenrgy)                  
      endif
 
C------End ADAS conversion-------
   
   
      write (11,81) m5,m2,m3,m3,t1,t5,(elem(l,nc1,l1),l=1,3),
     1  cflag(nc1,l1),cflag(nc2,l2),(elem(l,nc2,l2),l=1,3),t3
      write (11,88) (xs(i),i=1,nenrgs)
      write (11,88) (cs(i,jtn),i=1,nenrgs)
   88 format (5e16.8)
      write (11,81) m5,m2,m3,m3,t1,t2,(elem(l,nc1,l1),l=1,3),
     1  cflag(nc1,l1),cflag(nc2,l2),(elem(l,nc2,l2),l=1,3),t3
      write (11,88) (xs(i),i=1,nenrgs)
      write (11,88) (csm(i,jtn),i=1,nenrgs)
      write (11,81) m1,m2,m3,m4,t1,t4,(elem(l,nc1,l1),l=1,3),
     1  cflag(nc1,l1),cflag(nc2,l2),(elem(l,nc2,l2),l=1,3),t3
      write (11,88) deav,fprt,de1,de2
      write (11,88) (xs(i),i=1,nenrgs)
      write (11,88) (csm2(i),i=1,nenrgs)
  890 continue
      call clock(time)
      delt=time-time0
      write (iw,89) delt
   89 format (//6h time=,f6.3,4h min//)
   
      return
      end
