C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/orderi.for,v 1.2 2004/07/06 14:26:09 whitefor Exp $ Date $Date: 2004/07/06 14:26:09 $
C
      subroutine orderi(n,y)
c
c         A table of n numbers starting at y is ordered according to a
c         sequence table in the first n words of itmp, which must be integer
c         and 2n in length.
c
c          The variable y is type integer.
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/sortdum/dum(2),idum(2),itmp(2*klam)
      dimension y(klam)
      integer t1,t2,y
c
      do 1 i = 1,n
      t1 = itmp(i)
      t2 = n + i
    1 itmp(t2) = y(t1)
      do 2 i = 1,n
      t2 = n + i
    2 y(i) = itmp(t2)
      return
      end
