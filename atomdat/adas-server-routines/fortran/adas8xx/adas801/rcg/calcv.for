C UNIX-IDL - SCCS info: Module @(#)calcv.for	1.1 Date 03/22/00
C
C-----------------------------------------------------------------------
C  ADAS conversion changes
C  -----------------------
C
C  VERSION : 1.1
C  DATE    : 04-12-2008
C  MODIFIED: Adam Foster
C             - First commented version
C             - Changed dimension of nsjk from 110 to ksjk (see rcg.h)
C
C-----------------------------------------------------------------------

c     subroutine plojb(x,y,npts,inc,lnn,nsym,c,xaa,yaa,labelz,nzl,label
c    1  x,nxl,labely,nyl)
c     implicit real*8 (a-h,o-z)
c     eplot=7
c     a=eplot
c     return
c     end
c     overlay(g9,10,2)
      subroutine calcv
c
c          Calc. v matrix (derivatives of eigenvalues wrt parameters)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
      include 'rcg.h'
      common/idskno/ir,iw,id2,id3,id4,ila,ilb,il,ic,ie
      common/c1/kcpl,nocset,nsconf(3,2),iabg,iv,nlsmax,nlt11,nevmax,
     1  kcpld(9),sjn,sjx,imag,iquad,uenrgy,dmin,ielpun,ilncuv,iplev,
     2  icpc,icfc,idip,iengyd,ispecc,ipct,ictc,ictbcd,kcpl1,coupl(12),
     3  lsymb(48),nosubc,kk,time0,time,delt,ndifft,notott,sj8mn,
     4  sj8mx,igdlev,nscrj8,nj8k(2),scrj8,scrj8p,nls,nlsp,njj,
     5  nnn,ndifsj,kpar,npar,ix,cave,sums20,nopc,j1a,j1b,
     6  nijk(ks,kc,2),notsj1(ks,100),not,nlt,norcd,a,lci,lcip,j1x,
     7  icnvrt(2),nlastt(ks),ndiffj(ks),nototj(ks),ntottj(100),
     8  ntotjj(ks,100),llijk(ks,kc,2),iiprty,fllijk(ks,kc,2),
     9  ssopi2,sopi2(kc,kc),nck(2),texcit,emean,
     *  zero,half,one,thrhal,two,iplotc,iw6,noicwr,noic(2),noicmup(2)
      common/c2/ll(ks),ni(ks),fll(ks),mult(klsc,ks),lbcd(klsc,ks),
     1  ialf(klsc,ks),fl(klsc,ks),s(klsc,ks),ntrmk(klsp),fllrhop,fllsigp
      common/c3a/isubj(kc),muls(ks),lhs(ks),
     1  nals(kmx,ks),ncfg(kmx),nalsj(kmx,ks),scrl(kmx,ks),scrs(kmx,ks),
     2  fj(kmx,ks),scrj(kmx,ks), mul(ks),lh(ks),lf(klsp),nsjk(ksjk),
     3  fk(kmx),fkj(kmx),scrl6(kmx),scrs6(kmx),fk6(kmx),lhqq(kmx),
     4  multqq(kmx),nalsjp(kjp,ks),pj(kjp,ks),noicrd
      common/c3lc1/c(kmx,kmx)
      common/c3lc2/tmx(kmx,kmx)
      common/c3lc3/ct4(kmx,kmx)
      common/c3lc4/vect(kmx,kmx)
      common/c3lcd/dumlc5
      common/ener/vpar(kpr,2),avp(9,2),aveig(2),avcg(2),
     1  npark(2),npav(kc),    eig(kmx),fnt(9,2),i1,i2,
     2  ifact(4),fact(4),lcser(kmx),x(kmx)
      character*10 parnam(kpr,2)
      character*8 fidls(kmx,2),fidjj(kmx,2),ldes(kmx),ldesp(kmx)
      common/char/parnam,fidls,fidjj,ldes,ldesp
      dimension illp(kmx**2),coeff(kmx**2)
      equivalence (tmx,illp),(c,coeff)
c
      dimension v(100,64)
c     equivalence (c,v)
c          NOTE---v(kpr,kmx) spills over into /c3lc2/
c
      if (nls.eq.1) vect(1,1)=1.0
      m=noicrd
      if (m.eq.0) go to 900
      k=kk
      do 100 jj=1,m
  100 backspace ic
      nlst=min0(nls,nevmax)
c
c          calc v matrix
c
      do 229 ipar=1,npar
      do 205 j=1,j1x
      if (ipar.ne.npav(j)) go to 205
      do 202 l=1,nls
      do 201 lp=1,nls
  201 ct4(l,lp)=0.0
      if (ncfg(l).eq.j) ct4(l,l)=1.0
  202 continue
      go to 212
  205 continue
c
c     read (ic) ((ct4(l,lp),l=lp,nls), lp=1,nls)
c     do 210 l=1,nls
c     do 210 lp=1,l
c 210 ct4(lp,l)=ct4(l,lp)
      do 207 l=1,nls
      do 207 lp=1,nls
  207 ct4(l,lp)=0.0
      read (ic) ncoeff,(illp(i),coeff(i),i=1,ncoeff)
      do 210 i=1,ncoeff
      l=illp(i)/10000
      lp=illp(i)-10000*l
      ct4(l,lp)=coeff(i)
  210 ct4(lp,l)=ct4(l,lp)
c
  212 do 220 m=1,nlst
      v(ipar,m)=0.0
      do 220 l=1,nls
      a=0.0
      do 215 lp=1,nls
  215 a=a+ct4(l,lp)*vect(lp,m)
  220 v(ipar,m)=v(ipar,m)+vect(l,m)*a
  229 continue
c
c
c
      write (iw,35)
   35 format (////9h V matrix)
      lx=0
  310 write (iw,36)
   36 format (1h )
      ln=lx+1
      lx=lx+11
      if (lx.gt.nlst) lx=nlst
      do 320 i=1,npar
  320 write (iw,37) parnam(i,k), (v(i,l), l=ln,lx)
   37 format (1x,a10,4x,11f9.5)
      if (lx.lt.nlst) go to 310
c
      m=noicrd-npar+j1x
      if (m.le.0) go to 900
      do 400 i=1,m
  400 read (ic) a
c
  900 call clock(time)
      delt=time-time0
      write (iw,93) delt
   93 format (11x,5htime=,f6.3,4h min)
      return
      end
