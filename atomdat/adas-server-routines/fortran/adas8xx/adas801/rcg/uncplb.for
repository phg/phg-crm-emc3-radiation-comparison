C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/rcg/uncplb.for,v 1.2 2004/07/06 15:27:09 whitefor Exp $ Date $Date: 2004/07/06 15:27:09 $
C
      function uncplb(fj1,fj2,fj, fk,fj2p,fjp)
c
      implicit real*8 (a-h,o-z)
      include 'common.h'
c
      a=sqrt((2.0*fj+1.0)*(2.0*fjp+1.0))
      two=2.0
      if (mod(fj1+fj+fj2p+fk,two).gt.0.0) a=-a
      uncplb=a*s6j(fj2,fj,fj1, fjp,fj2p,fk)
      return
      end
