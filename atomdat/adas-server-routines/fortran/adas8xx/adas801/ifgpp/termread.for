C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifgpp/termread.for,v 1.5 2009/02/24 11:29:37 afoster Exp $ Date $Date: 2009/02/24 11:29:37 $
C
      subroutine termread(iunit   , ipar     , index    , energies ,
     7                    labels  , itag     ,
     &                    iref    , iconmax  , indlmax  , ierror   ,
     &                    ieng    , ienglev  )
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Reads LS term information from IFG file and creates the 
C             labels string which is structured:
C               1: 12 - no longer used
C              13: 15 - ' * ' if re-labeled, blank otherwise
C              17: 26 - S, L , XJ
C              28: 42 - reserved for energy (not written to here)
C              44:122 - parentage string
C             123:146 - Eissner form of configuration
C               4:  6 - Lval
C               7:  9 - Sval
C
C
C  INPUT    : (I)    iunit     =  unit number of IFG file.
C
C  OUTPUT   : (I)    numF1     =  number of parity 1 forbidden transitions.
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              findstr    ADAS    Finds a 10 character search string.  
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.3                          
C  DATE     : 06-12-2003
C
C  MODIFIED : Martin O'Mullane
C	       - Changed string5.eq.'re-labeled' to
C	         string5(1:6).eq.'re-lab'
C
C  VERSION  : 1.4                          
C  DATE     : 14-11-2003
C
C  MODIFIED : Martin O'Mullane  
C              - Carry Lval of level in unused part of labels.
C              - Lfinal(13) and (14) had incorrect L values.
C              - Extended lfinal to l=U (15) for 4d 4f configs.
C
C  VERSION  : 1.5                          
C  DATE     : 10-12-2008
C
C  MODIFIED : Adam Foster
C              - Brought into line with offline v1.5:
C                - Carry Sval of level in unused part of labels.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifgpp.h'
C-----------------------------------------------------------------------
      integer     iunit       , Sval        , num1        , ierr       , 
     &            ierror      , iref        , j           , iconfig    ,
     &            iconmax     , il          , indlev      , ipar       ,
     &            nopen       , ieng
C-----------------------------------------------------------------------
      real*8      energy      , valL        , wt
C-----------------------------------------------------------------------
      character   string1*130 , string2*130 , string3*104 , string4*10 ,
     &            string5*10  , engchar*11
      character   Lval*1
C-----------------------------------------------------------------------
      integer     index(ncfg,nterms)    , itag(nenergy)   , 
     &            indlmax(ncfg)         , ienglev(nenergy)
C-----------------------------------------------------------------------
      real*8      energies(nenergy)
C-----------------------------------------------------------------------
      character   c1(8)*2                , c2(8)*3        , c3(8)*4
      character   configuration(ncfg)*24 , Lfinal(16)*1   , 
     &            labels(nenergy)*146
C-----------------------------------------------------------------------
      data Lfinal /'S','P','D','F','G','H','I',
     &             'K','L','M','N','O','Q','R',
     &             'T','U'/
C-----------------------------------------------------------------------

      num1=iref
 
      ierr=0
      call getconfigs(iunit,ipar,configuration,ierr)
      if (ierr.eq.-99) goto 999
 
      iconmax=0
 
  10  read(iunit,100,end=999)string1
 
      if (string1(1:5).eq.'-----') then
        read(iunit,90,end=999)string2
        if (string2(1:10).eq.' END TERMS') then
          goto 999
        elseif (string2(1:9).eq.'         ') then
          goto 10
        else
          read(string2,110)iconfig,indlev,engchar,nopen,string3
          read(engchar,*)energy
          indlmax(iconfig)=indlmax(iconfig)+1
          iref=iref+1
          index(iconfig,indlev)=iref
          energies(iref)=energy
          itag(iref)=iref
          labels(iref)(1:12)='zzzzzzzzzzzz'
          labels(iref)(13:15)='   '
          labels(iref)(123:146)=configuration(iconfig)
          read(iunit,120)string4,string5
          if (string4(1:5).eq.'-----') then
            call advance(iunit,-1)
          elseif (string4(1:2).eq.'J=') then
            if (string5(1:6).eq.'re-lab') labels(iref)(13:15)=' * '
            call advance(iunit,-1)
          endif
          read(string3,130)(c1(j),c2(j),c3(j),j=1,nopen)
          write(labels(iref)(44:122),150)
     &         (c1(j),c2(j),c3(j),j=1,nopen)
          read(c3(nopen),160)Sval,Lval
          il=1
          do while (Lval.ne.Lfinal(il))
            il=il+1
          end do
          ValL=dfloat(il-1)
          wt=(((2.0D0*ValL+1.0D0)*Sval)-1.0D0)/2.0D0
          write(labels(iref)(17:26),170)Sval,(il-1),wt
          write(labels(iref)(4:6),'(i3)')il-1
          write(labels(iref)(7:9),'(i3)')Sval
        endif
      elseif (string1(1:2).eq.'J=') then
        ieng=ieng+1
        ienglev(ieng)=iref
      elseif (string1(1:8).eq.'PARITY 2') then
        goto 999
      endif
 
      if ((iref-num1).gt.nterms) then
        ierror=-99
        goto 999
      endif
 
      if (iconfig.gt.iconmax) iconmax=iconfig
 
      goto 10
 
  90  format(a130)
 100  format(1x,a130)
C110  format(i2,i3,6x,f11.2,1x,i1,2x,a104)
 110  format(i2,i4,6x,a11,1x,i1,2x,a104)
 120  format(1x,a10,27x,a10)
 130  format(8(2x,a2,1x,a3,1x,a4,1x,:))
 150  format(8(a2,a3,a4,'/',:))
 160  format(i3,a1)
 170  format('(',i1,')',i1,'(',f4.1,')')
 
 999  continue
 
      end
