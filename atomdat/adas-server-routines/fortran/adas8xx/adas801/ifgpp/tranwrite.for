C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifgpp/tranwrite.for,v 1.6 2005/04/19 14:16:01 mog Exp $ Date $Date: 2005/04/19 14:16:01 $
C
      subroutine tranwrite(iunit    , iout    , nfmt      ,
     &                     itype    , jtype   , quantity  ,
     &                     lnorates , lwt     , itran     , 
     &                     num      , num2    ,
     &                     indexp1  , indexp2 , energies  , weights ,
     &                     indtemp  , ktot    , numred    )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  :  Reads the collision strengths and transition probabilities,
C              determines the resolved A and upsilons and writes them to
C              file.
C
C              transitions  - holds the A and upsilons
C                             1st dimension : monotonic index
C                             2nd dimension : 1 - A value
C                                             2..ktot - upsilon
C
C              energies     - energy of level, depending on coupling
C              weights      - statistical weight of energy level
C
C              labeltran    - 2D master index of transitions
C                             1st dimension : lhs index of transition
C                             2nd dimension : rhs     "
C                             it's value is the monotonic index
C
C              indexp1, indexp2 - 2D indices of the parity
C
C              num, num2 - number of transitions in call type and the
C                          number of levels.
C
C              nfmt - space for indices
C
C              With labeltran it is easy to determine whether we have a
C              new multiplet or a component of the multiplet.
C
C
C              In LS coupling set LWEIGHT='NO' to get same results as ADAS209.
C                
C
C
C  INPUT    : (I)    iunit     =  unit number of IFG file.
C
C  OUTPUT   : (I)    numF1     =  number of parity 1 forbidden transitions.
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C              - First version.
C
C  VERSION  : 1.2
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane
C              - Added extra 'quantity' option to enable output of
C                effective collision strengths (default) or the
C                collision strength (options RATES or GAMMA).
C
C  VERSION  : 1.3
C  DATE     : 03-12-2002
C
C  MODIFIED : Martin O'Mullane
C              - Add in the width of the transition indices for flexible 
C                adf04 transition labelling.
C
C  VERSION  : 1.4                                               
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Added include statements.
C
C  VERSION  : 1.5
C  DATE     : 24-11-2003
C
C  MODIFIED : Martin O'Mullane
C              - Move transitions() from real*8 to real*4 to accommodate
C                the larger sizes without seg faulting.
C
C  VERSION  : 1.6
C  DATE     : 28-02-2004
C
C  MODIFIED : Martin O'Mullane
C              - Re-work the transitions 2D array as a direct access
C                file. This moves the burden from memory to I/O but
C                is far more scalable.
C
C  VERSION  : 1.7
C  DATE     : 19-04-2005
C
C  MODIFIED : Martin O'Mullane
C              - Close the temporary file at the end of the routine. The
C                sun compilers complain otherwise.
C
C-----------------------------------------------------------------------
      include     'common.h'
      include     'ifgpp.h'
C-----------------------------------------------------------------------
      integer     nmtp    ,  nval     , itmp
C-----------------------------------------------------------------------
      parameter  (nmtp    = nterms*nterms)
      parameter  (nval    = ntemp+1)
      parameter  (itmp    = 67)
C-----------------------------------------------------------------------
      integer    iunit    , iout      , ktot      , itype  , jtype    ,
     &           ierror   , itran     , numtran   , nlcfg  , nrcfg    ,
     &           nlind    , nrind     , num       , num2   ,
     &           numred   , lessk     , i         , j      , k        ,
     &           jj       , kk        , nl        , nr     , nt       ,
     &           itp      , ml        , mr        , mtemp  , nfmt
C-----------------------------------------------------------------------
      real*8     wmul     , aeff      , gA        , eav    , tot2j    ,
     &           fj1      , fj2       , flam      , aa
C-----------------------------------------------------------------------
      logical    lnorates , lwt       , ltop
C-----------------------------------------------------------------------
      character  fstr*10  , quantity*5
C-----------------------------------------------------------------------
      integer    labeltran(nenergy,nenergy)  , indexp1(ncfg,nterms)   ,
     &           indexp2(ncfg,nterms)        , indtemp(ntemp)
C-----------------------------------------------------------------------
      real*4     transitions(nval)
C-----------------------------------------------------------------------
      real*8     rate(nrates)                ,
     &           energies(nenergy)           , weights(nenergy)       ,
     &           coll(ntemp)
C-----------------------------------------------------------------------

C Store the transition information in a direct access file

      open(itmp, access='direct', recl=nval*4, status='scratch')
 
C Zero the storage arrays

      do i=1,nval
        transitions(i)=0.0D0
      end do
  
      do i=1,nterms
        do k=1,nterms
          labeltran(i,k)=0
        end do
      end do


C Find the requested transition type
 
      if (itran.eq.1) then
        fstr='PAR 1 FORB'
      elseif (itran.eq.2) then
        fstr='PAR 2 FORB'
      else
        fstr='ALLOWED   '
      endif
  
      ierror=1
      ltop = .TRUE.
      call findstr(iunit,fstr,ltop, ierror)
      if (ierror.eq.-99) goto 999
      call advance(iunit,1)
      
      
C Now loop over the transitions in the IFG file
      
      numtran=1
      do kk=1,num
      
        call tranread(iunit , itype , jtype  , lnorates , quantity ,
     &                nlcfg , nlind , nrcfg  , nrind    , 
     &                fj1   , fj2   , flam   , gA       , rate )
        if (itype.eq.-99) goto 220
      
        if (itran.eq.1) then
          nl=indexp1(nlcfg,nlind)
          nr=indexp1(nrcfg,nrind)
        elseif (itran.eq.2) then
          nl=indexp2(nlcfg,nlind)
          nr=indexp2(nrcfg,nrind)
        else
          nl=indexp1(nlcfg,nlind)
          nr=indexp2(nrcfg,nrind)
        endif
 
C Check if indices are swapped

        if (nl.gt.nr) then
           itp = nl
           nl = nr
           nr = itp
        endif
 
C Find the numerator of the A value (of f if jtype=1) sum.
C Note that if delta E = 0 we cannot form proper term average.

        if (lwt) then 
           wmul = flam
        else
           wmul = 1.0D0
        endif    
        
        if (energies(nl).eq.energies(nr)) wmul = 1.0D0
          
        if (jtype.eq.1) then
           aeff = gA*(wmul**3)
        else
           if (itype.ne.4) then
              aeff = gA*wmul
           else
              aeff = gA
           endif
        endif
                                   

C       First instance - assign record number and write to file
C       subsequent times - read from file and update
 
        if (labeltran(nl,nr).eq.0) then
        
          labeltran(nl,nr) = numtran
          transitions(1)   = aeff

          do i = 2, ktot+1
            transitions(i) = rate(indtemp(i-1))
          end do
          write(itmp, rec=numtran)transitions
          
          numtran = numtran + 1
          
        else
        
          nt = labeltran(nl,nr)
          
          read(itmp, rec=nt)transitions
          
          transitions(1) = transitions(1) + aeff

          do i = 2, ktot+1
            transitions(i) = transitions(i) + rate(indtemp(i-1))
          end do
          write(itmp, rec=nt)transitions
          
        endif
        
      end do
 220  continue
      numtran = numtran - 1
 
 
      lessk=0
      do k=1,numtran
        do i=1,num2
          do j=1,num2
            if (labeltran(i,j).eq.k) then
               ml=i
               mr=j
               if (ml.lt.mr) then
                  mtemp=ml
                  ml=mr
                  mr=mtemp
               endif
               if (ml.eq.mr) then
                 lessk=lessk+1
                 goto 240
               endif
               eav=abs(energies(mr)-energies(ml))
               
C If Delta E=0 we cannot form proper term average
C If weighting is off then eav is not used.

               if (eav.eq.0.0D0) then
                  eav = 1.0D0
               else
                  if (jtype.eq.1) then
                     eav=((1/eav)*1.0D+8)**3
                  else
                     eav=((1/eav)*1.0D+8)
                  endif
               endif
               
               if (itype.eq.4) eav=1.0D0
               if (.not.lwt) eav=1.0D0
               
               if ( energies(ml).gt.energies(mr) ) then
                  tot2j=weights(ml)
               else
                  tot2j=weights(mr)
               endif
               
               read(itmp, rec=k)transitions
               transitions(1)=transitions(1)/(eav*tot2j)
               
               aa = transitions(1)
               do jj = 2, ktot+1
                 coll(jj-1) =  transitions(jj)
               end do
               call rate_write(iout, nfmt, ml, mr, ktot, aa, coll)
               goto 240
               
            endif
          end do
        end do
 240    continue
      end do
 
 
      numred=numtran-lessk
      

 
 999  continue
 
      close(itmp)
 
      end
