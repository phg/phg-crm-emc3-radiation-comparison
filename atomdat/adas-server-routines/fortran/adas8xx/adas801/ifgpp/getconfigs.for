C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifgpp/getconfigs.for,v 1.3 2004/07/06 13:58:40 whitefor Exp $ Date $Date: 2004/07/06 13:58:40 $
C
      subroutine getconfigs(iunit, ipar , configuration, ierr)
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Get the configurations from IFG file. Only the Eissner
C             form is  extracted and is converted to standard form with
C             ADAS routine  XXCFTR.
C
C 
C  INPUT    : (I)    iunit          =  Unit number of IFG file.
C  INPUT    : (I)    ipar           =  Requested parity.
C
C  OUTPUT   : (C)    configuration  =  Eissner cofiguration.
C  OUTPUT   : (I)    ierr           =  error code
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              findstr    ADAS    Finds a 10 character search string.  
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifgpp.h'
C-----------------------------------------------------------------------
      integer     iunit     , ipar       , ierr     , iconfig
C-----------------------------------------------------------------------
      logical     ltop
C-----------------------------------------------------------------------
      character   string*80 , sscfg*24   , parity*10
C-----------------------------------------------------------------------
      character   configuration(ncfg)*24
C-----------------------------------------------------------------------
 
  
      parity='PARITY 1  '
      if (ipar.eq.2) parity='PARITY 2  '
 
      ltop = .TRUE.
      call findstr(iunit, parity, ltop, ierr)
      if (ierr.eq.-99)  goto 999
 
      call advance(iunit,2)
      
   5  read(iunit,100,end=999)string
        if (string(1:5).eq.'-----') goto 7
        read(string,6)iconfig,sscfg
        configuration(iconfig)=sscfg
      goto 5
      
   6  format(i4,45x,a24)
 
   7  continue
   
      call advance(iunit,-1)
 
 100  format(1x,a80)
 
 999  continue
 
      end
