      Program IFGpp
 
      implicit none
 
C---------------------------------------------------------------------
C
C  This program post processes an IFG intermediate data file to produce
C  an ADAS adf04 specific ion file. Two levels of resolution, viz LS
C  and intermediate coupling, are possible
C
C      unit 10 : instruction file
C      unit 20 : IFG input file
C      unit 21 : OUTPUT file
C
C  First attempt limited to nterms in each parity.
C
C  AUTHOR : Martin O'Mullane
C  DATE   : 2-4-92
C
C  UPDATES
C
C   28-8-97 : Problem with LS bundled A values where the Sum(gA*lamda**3)
C             was divided by the incorrect Sum(2J_k+1). The k should be
C             the upper level (in energy) not just the rhs value.
C
C  14-10-97 : increased number of temperatures from 8 to 14
C
C  27-10-97 : add name of IFG file to the comments section
C             find correct A value if energy levels are the same
C
C  20-04-98 : for J resolved do not reset nlmaxp1 and nlmaxp2. Put
C             the itype =2,3,4 inside an if statement.
C
C  27-01-99 : Renamed to ifgpp. Extensive restructuring including
C              - restrict to IC and LS coupling.
C              - namelist input for instructions.  
C              - LS A values are now correct, the term statistical 
C                weight is used in the denominator.  
C              - program is slightly better commented
C              - central utility subroutines and ADAS subroutines
C                are used where appropriate. 
C              - output information on adf04 file improved.
C
C  14-04-99 : Incorrect temperature array was passed to temp_write.
C
C  28-07-99 : In certain complex, same parity, transitions the LS 
C             indices had nl.gt.nr so check for this possibility in
C             subroutine tranwrite. 
C
C  8-2-00:22: Even more extensive restructuring in order to incorporate
C             it into ADAS801. Implicit none is used. New versioning 
C             system (1.1) from now.
C
C
C---------------------------------------------------------------------
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1
C  DATE     : 08-02-2000
C
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C  VERSION  : 1.2
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane
C              - Added extra 'quantity' option to enable output of
C                effective collision strengths (default) or the
C                collision strength (options RATES or GAMMA).
C
C  VERSION  : 1.3
C  DATE     : 04-12-2002
C
C  MODIFIED : Martin O'Mullane
C              - Pass numE (number of energies) through to rate_write
C                to accommodate large indices with the new flexible
C                definition.
C              - Make supplement comment stronger.
C              - Add purity information to comment section (=1 part)
C                for IC run.
C
C  VERSION  : 1.3
C  DATE     : 28-04-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add a new subroutine, a04filter, to tie untied levels
C                to ground and to sort the transition list into 
C                ascending order.
C              - Change format of IC to LS map from i4 to i6.
C
C  VERSION  : 1.4
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.5
C  DATE     : 14-11-2003
C
C  MODIFIED : Martin O'Mullane
C              - When converting Eissner strings with a full d10 or f14
C                initial shell the string to xxcftr must be modified.
C              - Adjust level line format is L is greater than 10 (n). 
C              - Make the orbital string longer to accomodate 6g. 
C
C  VERSION  : 1.6
C  DATE     : 08-12-2008
C
C  MODIFIED : Adam Foster
C              - Implemented changes in offline version 1.6:
C                - Adjust adf04 level string formats according to sizes
C                  of 2S+1, L and Eissner configuration string length
C              - Increase orb to 296 characters to allow for 36 
C                configurations
C             
C
C---------------------------------------------------------------------
      include    'common.h'
      include    'ifgpp.h'
C-----------------------------------------------------------------------
      integer     iunit   , ifile    , iraw ,  iout 
      character   blank*132
C-----------------------------------------------------------------------
      parameter  (iunit   = 5   ,  ifile  =  20 ,  iout   =  21 , 
     &            iraw    = 22  ) 
C-----------------------------------------------------------------------
      parameter  (blank   = ' ' )
C-----------------------------------------------------------------------
      integer    iz0       , iz1       , itype    , jtype      ,
     &           isofile   , itran     , ktot     , ipar
      integer    i         , j         , k        , itm        , numE ,
     &           num1      , num2      , numcom   , numrates   , itmp ,
     &           ncp1      , ncp2      , ierror   , numJ       ,
     &           numF1     , numF2     , numA     , ls1        , ls2  ,
     &           numF1red  , numF2red  , numAred  , i4unit     , ieng ,
     &           is_batch  , nfmt      , ia04     
      integer    ncount
      integer    l_max     , s_max     , l_val    , s_val
      integer    len_l     , len_s     , len_cfg  , len_cfgs
C-----------------------------------------------------------------------
      real*8     fip       , ground    , xja
C-----------------------------------------------------------------------
      logical    lnorates  , lwt       , ltop     , lup        , luntied
C-----------------------------------------------------------------------
      character  str*80    , symbl*2   , xfesym*2 , cin*18     ,
     &           cin2*25   , cout*24   , orb*296  , producer*30, 
     &           date*10   , fstr*10
      character  fmt45*45  , fmt46*45  , couts*40
C-----------------------------------------------------------------------
      integer    indexp1(ncfg,nterms)  , indexp2(ncfg,nterms)  , 
     &           indtemp(ntemp)        ,
     &           nlmaxp1(ncfg)         , nlmaxp2(ncfg)         , 
     &           itag(nenergy)         , ktag(nenergy)         , 
     &           ienglev(nenergy)      , istag(nenergy)          
C-----------------------------------------------------------------------
      real*8     energies(nenergy)     , weights(nenergy)      , 
     &           tmp(25)               , temp(nrates)          , 
     &           teout(ntemp)          , engJ(nenergy)           
C-----------------------------------------------------------------------
      character  info(30)*80           , labels(nenergy)*146   ,
     &           f_line*21             , com_untied(ndcom)*80
C-----------------------------------------------------------------------
      data tmp/1.0D+3,1.5D+3,2.0D+3,3.0D+3,5.0D+3,7.0D+3,
     &         1.0D+4,1.5D+4,2.0D+4,3.0D+4,5.0D+4,7.0D+4,
     &         1.0D+5,1.5D+5,2.0D+5,3.0D+5,5.0D+5,7.0D+5,
     &         1.0D+6,1.5D+6,2.0D+6,3.0D+6,5.0D+6,7.0D+6,1.0D+7/

C---------------------------------------------------------------------



C---------------------------------------------------------------------
C   User input in form of namelists
C---------------------------------------------------------------------

      character ifgfile*80 , outfile*80
      character coupling*2 , aval*3   , isonuclear*3, lweight*3 ,
     &          quantity*5
      integer   numtemp    , comments 
      real*8    ip
         
      namelist/files/   ifgfile    , outfile
      namelist/options/ ip         , coupling   , aval     , numtemp  , 
     &                  lweight    , isonuclear , comments , quantity 


C  Set the default values of the namelists
      ifgfile    = ' '           ! reject this though
      outfile    = 'ifgpp.list'  ! reject this though
                                 !
      ip         = 0.0D0         ! in cm**-1
      coupling   = 'LS'          ! or IC
      aval       = 'YES'         ! if NO give oscillator strengths
      numtemp    = 8             !
      lweight    = 'YES'         ! default to weighting averages by
                                 ! wavelength
      isonuclear = 'NO'          ! or YES
      comments   = 0             ! Level of comments
                                 ! 0 : no additional comments
                                 ! 1 : give Eissner and parentage
                                 ! 2 : give IC->LS map also
      quantity   = 'RATES'       ! 'RATES' : effective collision strengths 
                                 !           (adf04 standard)
                                 ! 'GAMMA' : collision strengths

 
C-----------------------------------------------------------------------
C Begin program
C-----------------------------------------------------------------------
 
       
C Initialize some variables
      
      call xx0000
      
      do i=1,nenergy
        labels(i)=blank
      end do
      
      do i=1,ncfg
        nlmaxp1(i)=0
        nlmaxp2(i)=0
      end do
 
      lnorates = .FALSE.
      
      luntied  = .FALSE.

      open(iraw, status='scratch' )


C Read instruction file and open required files.
C
C Get date and producer
      
      read(5,*)is_batch

      read(iunit,'(a)')producer
      read(iunit,'(a)')date


C Get comments

      read(iunit,*)numcom
      do i =1, numcom
        read(iunit,'(a)')info(i)
      end do


C Get instructions and check their validity
 
      read(iunit,files)

      open(ifile, file=ifgfile, status='old' )
      open(iout,  file=outfile, status='unknown' )
      
      
      
      read(iunit,options)
      
      call xxstuc(coupling)
      call xxstuc(isonuclear)
      call xxstuc(lweight)
      call xxstuc(aval)
      call xxstuc(quantity)
      
      fip = ip
      if (coupling.eq.'LS') then 
         itype = 2
      elseif (coupling.eq.'IC') then 
         itype = 1
      else
         write(i4unit(-1),*)'Unknown coupling'
         goto 999
      endif
      ktot = numtemp
      if (ktot.lt.0) then
         write(i4unit(-1),*)'How many temperatures???'
         goto 999
      endif
      if (isonuclear(1:1).eq.'Y') then 
         isofile = 2
      else  
         isofile = 1
      endif
      if (lweight(1:1).eq.'Y') then 
         lwt = .TRUE. 
      else  
         lwt = .FALSE. 
      endif
      if (aval(1:1).eq.'Y') then 
         jtype = 1 
      else  
         jtype = 0 
      endif
      
      read(iunit,*,end=5) (indtemp(i),i=1,ktot)
      goto 6
   5  write(i4unit(-1),*)'No temperature indices given'
      goto 999   
      
   6  continue  
     
      close(iunit)
 

C-----------------------------------------------------------------------
C Now start processing
C-----------------------------------------------------------------------

      if (is_batch.le.0) write(6,*)is_batch
 

C Get collision energies, temperatures and orbital energies
 
      read(ifile,'(i5,i4)',end=999)iz0,iz1
      read(ifile,'(i5)',end=999)numrates
      
      if (quantity.EQ.'RATES') then
         call advance(ifile,4)
         if (numrates.ne.0) then
            read(ifile,25,end=999)(temp(j),j=1,nrates)
            call advance(ifile,1)
         else
            lnorates=.TRUE.
            itm=1
            do k=1,8
              indtemp(k)=itm
              itm=itm+2
            end do
            do k=1,nrates
              temp(k)=tmp(k)*(dfloat(iz1+1))**2
            end do
         endif
      else
         read(ifile,25,end=999)(temp(j),j=1,nrates)
         call advance(ifile,5)
      endif
  25  format(1p,7(d10.3,:))
 
 
      read(ifile,'(1x,A)')orb
      if (orb(4:5).ne.'-1') orb='   -1'
 

C Read in IC energy levels and indexing 
  
      numE = 0
      ipar = 1
      call levelread(ifile , ipar , indexp1 , energies , labels , itag,
     &               numE  , ncp1 , nlmaxp1 , ierror   )
      num1 = numE
      ipar = 2
      call levelread(ifile , ipar , indexp2 , energies , labels , itag,
     &               numE  , ncp2 , nlmaxp2 , ierror   )
      num2=numE
      do i=1,numE
        engJ(i)=energies(i)
        ktag(i)=itag(i)
      end do
      numJ=numE

      if (is_batch.le.0) write(6,*)is_batch
 
C Read in energy levels and indexing of other coupling schemes
  
      if (itype.ne.1) then
         do i=1,ncfg
           nlmaxp1(i)=0
           nlmaxp2(i)=0
         end do
         if (itype.eq.2) then
            numE = 0
            ieng = 0
            ipar = 1
            call termread(ifile   , ipar   , indexp1 , energies ,
     &                    labels  , itag   , numE    , ncp1     ,
     &                    nlmaxp1 , ierror , ieng    , ienglev  )
            num1 = numE
            ipar = 2
            call termread(ifile   , ipar   , indexp2 , energies ,
     &                    labels  , itag   , numE    , ncp2     ,
     &                    nlmaxp2 , ierror , ieng    , ienglev  )
            num2=numE
         else
            write(i4unit(-1),*)'Will introduce other couplings later'
         endif
      endif

      if (is_batch.le.0) write(6,*)is_batch
        


C Write top line of adf04 file
 
      symbl=xfesym(iz0)
      call xxstuc(symbl)
      write(iraw,27)symbl,(iz1-1),iz0,iz1,fip
  27  format(a2,'+',i2,8x,i2,8x,i2,4x,f12.1)
 
 
C Calculate maximum 2S+1 value, maximum L value and the statistical weights,  
C sort energy levels and write to adf04 file.
C Adjust format for level lines in adf04 file according to maximum field
C lengths for Eissner configuration, 2S+1 and L.
 
      lup = .TRUE.
      call xxr8ss(numE, lup, energies, itag)
      call xxr8ss(numJ, lup, engJ,     ktag)


      l_max   = 0
      s_max   = 0
      len_l   = 1
      len_s   = 1
      len_cfg = 18
      
      do i=1,numE
        itmp=itag(i)
        read(labels(itmp)(4:6),*)l_val
        read(labels(itmp)(7:9),*)s_val
        l_max = max(l_max, l_val)
        s_max = max(s_max, s_val)
	
	call xxslen(labels(itmp)(123:146),ls1,ls2)
	
	len_l    = max(int(dlog10(dfloat(l_val)))+1,len_l)
	len_s    = max(int(dlog10(dfloat(s_val)))+1,len_s)
	len_cfg  = max(ls2-ls1+1,len_cfg)
	len_cfgs = 4*int((len_cfg+1)/3)-1
	
      end do
      
      fmt45="(i5,1x,a??,1x,'(',i?,')',i?,'(',f4.1,')',a15)"
      
      write(fmt45(9:10),'(i2)')len_cfg
      write(fmt45(20:20),'(i1)')len_s
      write(fmt45(27:27),'(i1)')len_l
      
      fmt46=fmt45
      write(fmt46(9:10),'(i2)')len_cfgs
      
      
c      write(6,*)'fmt45=',fmt45
c      write(6,*)'fmt46=',fmt46

 
      ground=energies(1)
      do i=1,numE
      
        itmp=itag(i)
        
        energies(i) = energies(i)-ground
        write(labels(itmp)(28:42),50)energies(i)
        
        read(labels(itmp)(22:25),*)xja
        read(labels(itmp)(4:6),*)l_val
        read(labels(itmp)(7:9),*)s_val
        weights(i) = 2.0D0*xja+1.0D0

        cout = labels(itmp)(123:146)
        if (isofile.ne.2) then
           cin = '                  '
           call xxslen(cout, ls1, ls2)
           write(cin(1:ls2),'(a)')cout(1:ls2)
           if (cin(1:2).EQ.'60'.or.cin(1:2).EQ.'64') then
              cin2 = '                   '
              cin2(1:ls2) = cin(1:ls2)
              call xxcftr(3,cin2,couts)
           else
              call xxcftr(3,cin,couts)
           endif
           call xxstuc(couts)
	   
	   write(iraw,fmt46)i,couts(1:len_cfgs),s_val,l_val,
     &	                      xja,labels(itmp)(28:42)
        else
        
	   write(iraw,fmt45)i,cout(1:len_cfg),s_val,l_val,xja,
     &	                      labels(itmp)(28:42)
        endif
        
      end do

  40  format(i5,1x,a18,a26)
  45  format(i5,1x,a18,a3,a3,a22)
  50  format(f15.1)
 
      
C write orbital energies to adf04 file
       call xxwstr(iraw,orb)


C Determine the size of the space for the indices in the transition line.
C This depends on the number of levels; 3 for values to 99, then 4 etc.

       nfmt = max(3, 2+int(dlog10(dfloat(numE))))
       nfmt = min(9, nfmt)

 
C write temperatures to adf04 file
      
      do i=1,ktot
        teout(i) = temp(indtemp(i))
      end do
      if (quantity.EQ.'RATES') then
         ia04 = 3
      else
         ia04 = 1
      endif
      call temp_write(iraw,nfmt,ktot,teout,iz1,ia04)
 
 
C Process transitions 

      do i=1,ncp1
        do j=1,nlmaxp1(i)
          k=1
          do while (indexp1(i,j).ne.itag(k).and.k.le.numE)
            k=k+1
          end do
          indexp1(i,j)=k
        end do
      end do
 
      do i=1,ncp2
        do j=1,nlmaxp2(i)
          k=1
          do while (indexp2(i,j).ne.itag(k).and.k.le.numE)
            k=k+1
          end do
          indexp2(i,j)=k
        end do
      end do


      ierror=1
      ltop = .TRUE.
      fstr = 'END TERMS '
      call findstr(ifile,fstr,ltop,ierror)
      if (ierror.eq.-99) goto 99
      call advance(ifile,3)
      read(ifile,80,err=99,end=99)numF1,numF2,numA
  80  format(1x,3i6)

      if (is_batch.le.0) write(6,*)is_batch
 
      if (numF1.ne.0) then
         itran = 1
         call tranwrite(ifile    , iraw    , nfmt     ,
     &                  itype    , jtype   , quantity , 
     &                  lnorates , lwt     , itran    , numF1   , num2 , 
     &                  indexp1  , indexp2 , energies , weights ,
     &                  indtemp  , ktot    , numF1red )
         if (is_batch.le.0) write(6,*)is_batch
      endif
      
      if (numF2.ne.0) then
         itran = 2
         call tranwrite(ifile    , iraw    , nfmt     ,
     &                  itype    , jtype   , quantity , 
     &                  lnorates , lwt     , itran    , numF2   , num2 , 
     &                  indexp1  , indexp2 , energies , weights ,
     &                  indtemp  , ktot    , numF2red )
         if (is_batch.le.0) write(6,*)is_batch
      endif
      
      
      if (numA.ne.0) then
         itran = 3
         call tranwrite(ifile    , iraw    , nfmt     ,
     &                  itype    , jtype   , quantity , 
     &                  lnorates , lwt     , itran    , numA    , num2 , 
     &                  indexp1  , indexp2 , energies , weights ,
     &                  indtemp  , ktot    , numAred  )
         if (is_batch.le.0) write(6,*)is_batch
      endif


 
 99   continue
 
C write terminators - position depends on nfmt
      
      write(f_line,'(5H(1X,I,i1,7H,/,1X,I,i1,5H,1X,I,i1,1H))')
     &               nfmt,nfmt,nfmt
      
      write(iraw,f_line)-1,-1,-1
      
      call xxflsh(iraw)
 

C filter the file to fix untied levels and sort transition list

      rewind(iraw)
      
      call a04filter(iraw, iout, nfmt, ia04, luntied, com_untied)

      close(iraw)

 
C----------------------------------------------------------------------
C Time to write the comments
 
C Comment level = 1 => write Eissner, parentage, relabeling indicator
C and purity if IC.
 
      if (comments.ge.1) then
       
         write(iout,500)
         write(iout,'(1HC)')
         if (itype.EQ.1) then
            write(iout,520)
         else
            write(iout,530)
         endif
 
         do i=1,numE
           
           itmp=itag(i)
           cin = labels(itmp)(123:140)
           call xxslen(cin, ls1, ls2)
           if (cin(1:2).EQ.'60'.or.cin(1:2).EQ.'64') then
              cin2 = '                   '
              cin2(1:ls2) = cin(1:ls2)
             call xxcftr(3,cin2,cout)
           else
              call xxcftr(3,cin,cout)
           endif
           cin = labels(itmp)(123:140)
           call xxstuc(cout)
 
           str = labels(itmp)(44:122)
           call xxslen(str,ls1,ls2)
           
           if (itype.EQ.1) then
              write(iout,540)i, cin, cout,labels(itmp)(14:14),
     &                       labels(itmp)(1:3),str(ls1:ls2)
           else
              write(iout,550)i, cin, cout,labels(itmp)(14:14),
     &                       str(ls1:ls2)
           endif
         
         end do
         
         write(iout,560)
         
      endif
 
 500  format('C',80('-'))
 520  format('C',3x,'          Configuration',/,
     &       'C',7x,'Eissner',11x,' == Standard',11x,'R  % ',2x,
     &       'Parentage',/,'C')
 530  format('C',3x,'          Configuration',/,
     &       'C',7x,'Eissner',11x,' == Standard',11x,'R',1x,
     &       'Parentage',/,'C')
 540  format('C',i4,3x,a18,' == ',a18,1x,a1,1x,a3,2x,a)
 550  format('C',i4,3x,a18,' == ',a18,1x,a1,1x,a)
 560  format('C',/,'C (R) - Levels (or levels within a term) have ',
     &       'been reassigned',/,'C       from their principal ',
     &       'component.',/, 'C')
 


C Comment level = 2 => write LS to IC map
C
C tag indices are : ienglev - sequential list of coupled energy level
C                             components; parity 1 followed by parity 2.
C                             eg (odd) 1 2 3 3 3 4 5 (even) 6 7 8 8 8 9 
C                   itag    - coupled energies sorted
C                   ktag    - IC energies sorted
C                   istag   - inverse of itag

      if (comments.ge.2.and.itype.gt.1) then
          
         write(iout,600)
         write(iout,610)

         do i=1,numE
           istag(itag(i))=i
         end do
          
         write(iout,'(a)')'C IC Level list :'
         write(iout,'(a)')'C'
         write(iout,650)(j,j=1,numJ)
         write(iout,'(a)')'C'
         write(iout,'(a)')'C Map to LS levels :'
         write(iout,'(a)')'C'
         write(iout,650)(istag(ienglev(ktag(j))),j=1,numJ)
     
         write(iout,610)
 
      endif
      
 600  format('C',80('-'))
 610  format('C')
 650  format('C   ',10i6)
 

C If there are untied levels add comment block here

       if (luntied) then
       
          write(iout,600)
          do i=1, ndcom
            if (com_untied(i)(1:1).EQ.'C') then
               call xxwstr(iout,com_untied(i))
            endif
          end do
         write(iout,'(a)')'C'
          
       endif

C Standard comments at the bottom of adf04 file.
  
      write(iout,760)
      write(iout,761)
      write(iout,762)
      call xxslen(ifgfile,ls1,ls2)
      write(iout,763)ifgfile(ls1:ls2)
      write(iout,761)
      write(iout,780)coupling,aval,numtemp,lweight,isonuclear,comments
      write(iout,761)
      write(iout,761)
      write(iout,782)
      do i=1,numcom
        write(iout,766)info(i)(1:1),info(i)(2:72)
      end do
      write(iout,764)numF1,numF2,numA
      write(iout,765)numF1red,numF2red,numAred
      write(iout,761)
      write(iout,785)
      write(iout,761)
      write(iout,760)

C A warning block if we write a type 1 file
      
      if (quantity.EQ.'GAMMA') write(iout,787)

C Producer and date
      
      write(iout,761)
      write(iout,774)
      call xxslen(producer,ls1,ls2)
      write(iout,776)producer(1:ls2)
      call xxslen(date,ls1,ls2)
      write(iout,775)date(1:ls2)
      write(iout,761)
      write(iout,760)

      if (is_batch.le.0) write(6,*)is_batch
 
 
 760  format('C',80('-'))
 761  format('C')
 762  format('C',3x,'Generated from Cowan Atomic Structure Program',
     &       /,'C')
 763  format('C   From IFG file : ',A)
 764  format('C',3x,'Parity 1    Parity 2    Allowed',/,
     &       'C',3x,i8,4x,i8,4x,i7,5x,'initially')
 765  format('C',3x,i8,4x,i8,4x,i7,5x,'reduced')
 
 766  format(a1,4x,a70)
 
 774  format('C',3x,'Code     : ADAS801')
 775  format('C',3x,'Date     : ',a)
 776  format('C',3x,'Producer : ',a)
 
 780  format('C   Options in effect',/,
     &       'C',/,
     &       'C   Coupling    Avalue   numtemps   Lweight   ',
     &       'Isonuclear   Comment Level',/
     &       'C',3x,a2,10x,a3,6x,i2,9x,a3,7x,a3,10x,i1) 

 782  format('C   Cowan code options',/,
     &       'C   ------------------')
 785  format('C   Note: The Born method does NOT calculate spin ',
     &       'changing transitions',/,'C   correctly. You should ',
     &       'supplement for important transitions of this type.') 

 787  format('C',/,
     &       'C   ***** Type 1 adf04 file ***** ',/,
     &       'C',/,
     &       'C   Temperatures replaced by X=(impact energy)/',
     &       'Delta E.',/,
     &       'C   Effective collision strengths replaced by ',
     &       'collision strengths.',/,'C',/,'C',80('-'))
C-----------------------------------------------------------------------
 
 999  continue 

      close(ifile)
      close(iout)

C-----------------------------------------------------------------------

      end
