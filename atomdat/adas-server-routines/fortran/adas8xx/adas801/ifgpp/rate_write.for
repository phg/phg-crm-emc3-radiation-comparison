C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifgpp/rate_write.for,v 1.4 2004/07/06 14:43:29 whitefor Exp $ Date $Date: 2004/07/06 14:43:29 $
C
       subroutine rate_write(iout, nfmt, nl, nr, ktot, aval, coll)
 
       implicit none
C-----------------------------------------------------------------------
C
C PURPOSE   : Writes adf04 transition line, lower index, upper index,
C             A value and collision strengths. The 'D' or 'E' in the
C             exponent is skipped.
C
C INPUT     : iout       - unit number of opened file
C             nfmt       - space for indices
C             nl         - first index
C             nr         - second index
C             ktot       - number of collision strengths
C             aval       - transition probability
C             coll       - collision strengths
C
C OUTPUT    : to iout opened file
C
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              xxslen     ADAS     Finds length of a string.  
C
C  AUTHOR   : Martin O'Mullane
C  DATE     : 27-1-99
C
C  UPDATES  :
C
C  VERSION  : 1.2
C  DATE     : 03-12-2002
C
C  MODIFIED : Martin O'Mullane
C              - Pass in the width of the transition indices for flexible 
C                adf04 transition labelling.
C
C  VERSION  : 1.3
C  DATE     : 28-04-2003
C
C  MODIFIED : Martin O'Mullane
C              - Dimension coll with ntemp rather than ktot.
C
C  VERSION  : 1.4
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifgpp.h'
C-----------------------------------------------------------------------
      real*8      dzero     
      character   blank*132
C-----------------------------------------------------------------------
      parameter  (dzero  = 1.0D-30)
      parameter  (blank  = ' ' )
C-----------------------------------------------------------------------
      integer    iout      , nfmt     , i       , j      , ktot    ,   
     &           nl        , nr       , il      , ic     , ls      , 
     &           lf        , nst
C-----------------------------------------------------------------------
      real*8     aval
C-----------------------------------------------------------------------
      character  line*156  , cline*144 , f_line*26
C-----------------------------------------------------------------------
      real*8     coll(ntemp)
C-----------------------------------------------------------------------

C Format statement for writing transitions

      write(f_line,'(5H(1X,I,i1,5H,1X,I,i1,14H,2x,1p,15d9.2))')nfmt,nfmt

      do i=1,ktot
        coll(i)=max(dzero,coll(i))
      end do
      aval=max(aval,dzero)
              
      write(line,f_line)nl,nr,aval,(coll(j),j=1,ktot)
 
      cline=blank

C Get starting point in output line
 
      nst = 2 + 2*nfmt
      cline(1:nst)=line(1:nst)
      il = nst + 3
      ic = nst + 1
      do i=1,ktot+1
        cline(ic:ic+nst)=line(il:il+4)//line(il+6:il+8)
        il=il+9
        ic=ic+8
      end do
 
      call xxslen(cline,ls,lf)
      write(iout,'(A)')cline(1:lf)
 
      end
