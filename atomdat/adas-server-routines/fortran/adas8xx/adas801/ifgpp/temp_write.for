C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifgpp/temp_write.for,v 1.4 2004/07/06 15:24:05 whitefor Exp $ Date $Date: 2004/07/06 15:24:05 $
C       
	 subroutine temp_write(iout, nfmt, ktot, tev, iz1,itype)
 
       implicit none
C-----------------------------------------------------------------------
C
C PURPOSE   : Writes adf04 temperature line. The 'D' or 'E' in the
C             exponent is skipped.
C
C INPUT     : iout       - unit number of opened file
C             nfmt       - space for indices
C             ktot       - number of temperatures
C             tev        - temperatures
C             iz1        - ion stage
C             itype      - type of adf04 file (1 or 3)
C
C OUTPUT    : to iout opened file
C
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              xxslen     ADAS     Finds length of a string.  
C
C  AUTHOR   : Martin O'Mullane
C  DATE     : 27-1-99
C
C  UPDATES  :
C
C  VERSION  : 1.2
C  DATE     : 03-12-2002
C
C  MODIFIED : Martin O'Mullane
C              - Pass in the width of the transition indices for flexible 
C                adf04 transition labelling.
C
C  VERSION  : 1.3
C  DATE     : 28-04-2003
C
C  MODIFIED : Martin O'Mullane
C              - Dimension tev with ntemp rather than ktot.
C
C  VERSION  : 1.2
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifgpp.h'
C-----------------------------------------------------------------------
      character   blank*142
C-----------------------------------------------------------------------
      parameter  (blank  = ' ' )
C-----------------------------------------------------------------------
      integer    iout      , i         , j        , iz1       , ktot   ,  
     &           il        , ic        , ls       , lf        , nfmt   ,
     &           itype
C-----------------------------------------------------------------------
      character  line*142  , cline*142 , f_line*18
C-----------------------------------------------------------------------
      real*8     tev(ntemp)
C-----------------------------------------------------------------------

C How wide is the transition line?
      
       write(f_line,'(12H(f5.1,4x,I1,i2,4Hx,a))')nfmt*2

       cline=blank
 
       write(line,50)(tev(j),j=1,ktot)
  50   format(1p,14(D9.2))
 
       cline(1:5)=line(1:5)
       il=1
       ic=1
       do i=1,ktot
         cline(ic:ic+7)=line(il:il+4)//line(il+6:il+8)
         il=il+9
         ic=ic+8
       end do
 
       call xxslen(cline,ls,lf)
       write(iout,f_line)float(iz1),itype,cline(1:lf)
 
       end
