      subroutine a04filter( itmp     , iout    , nfmt , itype ,
     &                      lcomment , comments )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Filters the adf04 file
C                i. orders the transitions
C               ii. identifies untied levels and ties these to ground
C              iii. send comments to be written to final adf04 file.
C
C
C  INPUT    : (I)    itmp      =  unit number of raw adf04 file.
C                    iout      =  unit number of final adf04 file.
C                    nfmt      =  number of spaces for transition indices.
C                    itype     =  adf04 file type.
C
C  OUTPUT   : (I)    lcomments =  .TRUE. if there are untied levels.
C                    comments  =  comment array.
C
C
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              xxdata_04  ADAS    Reads adf04 file.
C              advance    ADAS    Advances the file pointer.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1
C  DATE     : 25-04-2003
C
C  MODIFIED : Martin O'Mullane
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 30-10-2003
C
C  MODIFIED : Martin O'Mullane & Allan Whiteford
C              - Incorrectly placed initialisation of k when
C                checking the untied levels.
C
C  VERSION  : 1.2
C  DATE     : 14-11-2003
C
C  MODIFIED : Martin O'Mullane
C              - Alter output depending on size of L.
C
C  VERSION  : 1.3
C  DATE     : 25-02-2004
C
C  MODIFIED : Martin O'Mullane
C              - Vary the format of the orbital energies in order
C                to maximise the number of significant digits.
C
C  VERSION  : 1.4
C  DATE     : 03-12-2007
C
C  MODIFIED : Adam Foster
C              - Change length of string to allow for levels > 7d
C
C  VERSION  : 1.5
C  DATE     : 03-08-2009
C
C  MODIFIED : Martin O'Mullane
C              - Permit cstrga to have a maximum length of 90
C                characters but only make the width in the output
C                dataset that of the largest non-blank configuration
C                or 18 if shorter.
C
C-----------------------------------------------------------------------
      include   'common.h'
      include   'ifgpp.h'
C-----------------------------------------------------------------------
      integer   itmp          , iout        , nfmt     , itype
      integer   iorb          , il          , iz       , iz0        ,
     &          iz1           , nv          , itran    , maxlev     ,
     &          npl           , iadftyp     , itieactn
      integer   iuntied       , ind         , i        , j          ,
     &          k             , ileft       , iright   , i4unit     ,
     &          l             , n           , l_max    , degree     ,
     &          ilen          , igap        , ilgap
C----------------------------------------------------------------------
      real*8    bwno
C----------------------------------------------------------------------
      logical   lprn          , lcpl        , lorb     , lbeth      ,
     &          letyp         , lptyp       , lrtyp    , lhtyp      ,
     &          lityp         , lstyp       , lltyp
      logical   lcomment      , lup
C----------------------------------------------------------------------
      character titled*3      , f_line*21   , orbfmt*9 , string*296 ,
     &          f_220*53
C----------------------------------------------------------------------
      integer   i1a(ndtrn)    , i2a(ndtrn)
      integer   ia(ndlev)     , isa(ndlev)  , ila(ndlev)
      integer   ipla(ndmet,ndlev)           , npla(ndlev)
      integer   isort(ndtrn)  , itag(ndtrn)
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet)
      real*8    zpla(ndmet,ndlev)
      real*8    scef(ntemp)
      real*8    xja(ndlev)    , wa(ndlev)
      real*8    aval(ndtrn)   , scom(ntemp,ndtrn)      , beth(ndtrn)
      real*8    qdorb((ndqdn*(ndqdn+1))/2)             , qdn(ndqdn)
      real*8    eorb((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------
      logical   ltied(ndlev)  , lbseta(ndmet)          ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------
      character cpla(ndlev)*1 , cprta(ndmet)*9         ,
     &          tcode(ndtrn)*1, cstrga(ndlev)*90
      character comments(ndcom)*80
C----------------------------------------------------------------------

C Initialise comment section

       lcomment = .FALSE.
       do i = 1, ndcom
         comments(i) = 'X'
       end do


C Read in raw adf04 file

       itieactn = 1

       call xxdata_04( itmp   ,
     &                 ndlev  , ndtrn  , ndmet   , ndqdn , ntemp ,
     &                 titled , iz     , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa  , lbseta  , prtwta, cprta ,
     &                 il     , qdorb  , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga , isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla   , ipla    , zpla  ,
     &                 nv     , scef   ,
     &                 itran  , maxlev ,
     &                 tcode  , i1a    , i2a     , aval  , scom  ,
     &                 beth   ,
     &                 iadftyp, lprn   , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp  , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp  , itieactn, ltied
     &                )

C Set-up output format for configuration length and L quantum number size

       call xxslna(il, cstrga, ilen)
       if (ilen.LT.18) then
          igap = 18 - ilen + 1
       else
          igap = 1
       endif

       l_max = 0
       do ind = 1, il
          l_max = max(l_max, ila(ind))
       end do
       if (l_max.GT. 10) then
          ilgap = 2
       else
          ilgap = 1
       endif

       f_220 = '(i5,1x,1a??,??x,1H(,i1,1H),i??,1H(,f4.1,1H),1x,f15.1)'
       write(f_220(10:11), '(i2.2)')ilen
       write(f_220(13:14), '(i2.2)')igap
       write(f_220(29:30), '(i2.2)')ilgap

C Sort the transition list - sort on i1*(IL+1) + i2

       do i=1, itran

         itag(i)  = i
         isort(i) = (il+1)*i1a(i)+i2a(i)

       end do

       lup = .TRUE.
       call xxi4ss(itran, lup, isort, itag)


C  Tie any untied levels to ground

       k = 1
       if (itieactn.eq.1) then

          lcomment = .TRUE.

          do ind = 1, il

             if (.NOT.ltied(ind)) then

                i1a(itran+k) = 1
                i2a(itran+k) = ind
                do j=1,nv
                   scom(j,itran+k) = 1.00D-30
                end do
                k = k + 1
                if (k.GT.ndtrn) then
                   write(i4unit(-1),*)'Make ndtrn larger'
                   stop
                endif

             endif

          end do

       endif
       iuntied = k-1


C Calculate orbital energies from orbital quantum defects

       do ind = 1, iorb
          call xxidtl(ind, n, l)
          eorb(ind) = (dfloat(iz1) / (dfloat(n)-qdorb(ind)))**2
       end do


C Write the new file

       write(iout,200)titled, iz, iz0, iz1, bwno

       do ind = 1, il
          write(iout,f_220)ind, cstrga(ind), isa(ind), ila(ind),
     &                     xja(ind), wa(ind)
       end do


C The -1 level terminator and orbital energies

       string = ' '
       write(string(1:8),'(a8)')'   -1   '

       ileft = 9
       do ind = 1, iorb

          degree = int(log10(eorb(ind)))
          if (degree.GE.3) then
             orbfmt = '(1x,f7.2)'
          elseif (degree.GE.2) then
             orbfmt = '(1x,f7.3)'
          elseif (degree.GE.1) then
             orbfmt = '(1x,f7.4)'
          else
             orbfmt = '(1x,f7.5)'
          endif

          iright = ileft + 7
          write(string(ileft:iright),orbfmt)eorb(ind)
          ileft = iright + 1

       end do

       call xxwstr(iout, string)


C Temperatures and transitions

       call temp_write(iout,nfmt,nv,scef,iz1,itype)


       do i = 1, itran

         ind    = itag(i)
         ileft  = i2a(ind)
         iright = i1a(ind)
         call rate_write(iout, nfmt, ileft, iright, nv, aval(ind),
     &                   scom(1,ind))

       end do


       do i = 1, iuntied

         ind    = itran + i
         ileft  = i2a(ind)
         iright = i1a(ind)
         call rate_write(iout, nfmt, ileft, iright, nv, aval(ind),
     &                   scom(1,ind))

       end do



       write(f_line,'(5H(1X,I,i1,7H,/,1X,I,i1,5H,1X,I,i1,1H))')
     &                nfmt,nfmt,nfmt
       write(iout,f_line)-1,-1,-1



C Write comment block for untied levels

       if (lcomment) then

          comments(1) = 'C'
          comments(2) = 'C  The following levels tied to ground ' //
     &                  'with gamma=0, A=0'
          comments(3) = 'C'

          do k = 1, 1+iuntied/10
            ileft  = 1 + (k-1)*10
            iright = min(ileft+10, iuntied)
            write(comments(k+3),210)(i2a(j+itran), j=ileft, iright)
          end do

       endif


C----------------------------------------------------------------------

  200  format(a2,'+',i2,7x,i3,7x,i3,5x,f11.1)
  210  format('C   ', 10(i5, :, 1H,))

C----------------------------------------------------------------------

       end

