C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/ifgpp/tranread.for,v 1.4 2004/07/06 15:26:14 whitefor Exp $ Date $Date: 2004/07/06 15:26:14 $
C
      subroutine tranread(iunit , itype , jtype , lnorates , quantity ,
     &                    nlcfg , nlind , nrcfg , nrind    ,
     &                    fj1   , fj2   , flam  , A        , rate )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Reads in the transition data from the IFG file. The correct
C             indexing depending on coupling, and A or f value depending on
C             settings, are returned.
C
C
C  INPUT    : (I)    iunit     =  unit number of IFG file.
C
C  OUTPUT   : (I)    numF1     =  number of parity 1 forbidden transitions.
C
C  
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              advance    ADAS    Advances the file pointer.  
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane
C             Added extra 'quantity' option to enable output of
C             effective collision strengths (default) or the
C             collision strength (options RATES or GAMMA).
C
C  VERSION  : 1.3
C  DATE     : 09-09-2003
C
C  MODIFIED : Martin O'Mullane
C              - Add include statements.
C
C  VERSION  : 1.4                          
C  DATE     : 20-12-2003
C
C  MODIFIED : Martin O'Mullane  
C              - Extend size of level field from i3 to i4.
C
C-----------------------------------------------------------------------
      include 'common.h'
      include 'ifgpp.h'
C-----------------------------------------------------------------------
      integer     iunit  , itype  , jtype , ifmt   , nlcfg  , nlind  ,
     &            nrcfg  , nrind  , j
C-----------------------------------------------------------------------
      real*8      A      , fj1    , fj2   , fjp    , fjpt   ,
     &            flam   , gflog  , gA
C-----------------------------------------------------------------------
      character   quantity*5
C-----------------------------------------------------------------------
      logical     lnorates
C-----------------------------------------------------------------------
      real*8      rate(nrates)
C-----------------------------------------------------------------------
  
      if (itype.eq.1) then
        assign 501 to ifmt
      elseif (itype.eq.2) then
        assign 502 to ifmt
      elseif (itype.eq.3) then
        assign 503 to ifmt
      elseif (itype.eq.4) then
        assign 503 to ifmt
      else
        itype=-99
        return
      endif
 
      read(iunit,ifmt,end=800,err=999)nlcfg,nlind,fjp,
     &     nrcfg,nrind,fjpt,flam,gflog,gA
          
 
      if (jtype.eq.1) then
         A=gA
         fj1=fjp
         fj2=fjpt
      else
         A=10.0D0**gflog
         fj1=fjp
         fj2=fjpt
      endif
 
      if (.not.lnorates) then
         if (quantity.EQ.'RATES') then
            call advance(iunit,3)
            read(iunit,100,end=999,err=999)(rate(j),j=1,nrates)
         else
            read(iunit,100,end=999,err=999)(rate(j),j=1,nrates)
            call advance(iunit,3)
         endif
      endif
      call advance(iunit,1)
      return
 
 100  format(7(1pd10.3))


 
 501  format(7x,2(i4,12x,i4,f6.1,2x),f12.4,10x,f7.3,1pd10.3)
 502  format(7x,2(2i4,12x,f6.1,2x),f12.4,10x,f7.3,1pd10.3)
 503  format(7x,2(i4,4x,i4,8x,f6.1,2x),f12.4,10x,f7.3,1pd10.3)
 
 800  itype=-99
 
 999  continue
      end
