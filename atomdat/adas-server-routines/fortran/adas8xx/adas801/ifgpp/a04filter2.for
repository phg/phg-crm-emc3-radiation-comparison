      subroutine a04filter2( itmp      , iout     , nfmt     , itype ,
     &                       lionpot   , lreverse , lreorder ,
     &                       lcomment  , comments , ncomm
     &                     )

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Filters the adf04 file
C                i. Removes levels above ionisation potential
C               ii. Re-orders energy levels
C              iii. Removes unphysical A-values due to energy level
C                   re-adjustment.
C
C
C  INPUT    : (I)    itmp      =  unit number of raw adf04 file.
C                    iout      =  unit number of final adf04 file.
C                    lionpot   =  if .TRUE. remove levels
C                    lreverse  =  if .TRUE. zero A-values
C                    lreorder  =  if .TRUE. re-order energy levels
C
C  OUTPUT   : (I)    lcomments =  .TRUE. if action was taken
C                    comments  =  comment array.
C
C
C ROUTINES  :  ROUTINE    SOURCE   BRIEF DESCRIPTION
C              -------------------------------------------------------
C              xxdata_04  ADAS    Reads adf04 file.
C              advance    ADAS    Advances the file pointer.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1
C  DATE     : 14-04-2005
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C  VERSION  : 1.2
C  DATE     : 20-04-2005
C  MODIFIED : Martin O'Mullane
C              - Set i1a_new and i2a_new to i1a and i2a when levels are
C                in order.
C
C  VERSION  : 1.3
C  DATE     : 03-08-2009
C
C  MODIFIED : Martin O'Mullane
C              - Change length of string to allow for levels > 7d
C              - Permit cstrga to have a maximum length of 90
C                characters but only make the width in the output
C                dataset that of the largest non-blank configuration
C                or 18 if shorter.
C
C-----------------------------------------------------------------------
      include   'common.h'
      include   'ifgpp.h'
C-----------------------------------------------------------------------
      integer   itmp          , iout        , nfmt     , itype
      integer   iorb          , il          , iz       , iz0        ,
     &          iz1           , nv          , itran    , maxlev     ,
     &          npl           , iadftyp     , itieactn
      integer   ind           , i           , j        ,
     &          k             , ileft       , iright   , iup        ,
     &          ilow          , l           , n        , l_max      ,
     &          degree        , ncomm       , ncom_r   , ncom_i     ,
     &          nlines
      integer   i4indfi4      , ilen        , igap     , ilgap
C----------------------------------------------------------------------
      real*8    bwno          , diff
C----------------------------------------------------------------------
      logical   lprn          , lcpl        , lorb     , lbeth      ,
     &          letyp         , lptyp       , lrtyp    , lhtyp      ,
     &          lityp         , lstyp       , lltyp
      logical   lcomment
      logical   lionpot       , lreverse    , lreorder , linorder
C----------------------------------------------------------------------
      character titled*3      , f_line*21   , orbfmt*9 , string*296 ,
     &          f_220*53      , f_1002*59
C----------------------------------------------------------------------
      integer   i1a(ndtrn)    , i2a(ndtrn)
      integer   ia(ndlev)     , isa(ndlev)  , ila(ndlev)
      integer   ipla(ndmet,ndlev)           , npla(ndlev)
      integer   iabove(ndlev) , ia_new(ndlev)
      integer   i1a_new(ndtrn), i2a_new(ndtrn)
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet)
      real*8    zpla(ndmet,ndlev)
      real*8    scef(ntemp)
      real*8    xja(ndlev)    , wa(ndlev)
      real*8    aval(ndtrn)   , scom(ntemp,ndtrn)      , beth(ndtrn)
      real*8    qdorb((ndqdn*(ndqdn+1))/2)             , qdn(ndqdn)
      real*8    eorb((ndqdn*(ndqdn+1))/2)
      real*8    wa_new(ndlev)
C----------------------------------------------------------------------
      logical   ltied(ndlev)  , lbseta(ndmet)          ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------
      character cpla(ndlev)*1 , cprta(ndmet)*9         ,
     &          tcode(ndtrn)*1, cstrga(ndlev)*90
      character comments(100+ndtrn/50)*80 , str_r(ndtrn/100)*80  ,
     &          str_i(ndtrn/100)*80
C----------------------------------------------------------------------
      external  i4indfi4
C----------------------------------------------------------------------

C Initialise comment section

       lcomment = .FALSE.
       do i = 1, 40+ndtrn/50
         comments(i) = 'X'
       end do

       do i = 1, ndlev
         iabove(i) = 0
       end do


C Read in raw adf04 file

       itieactn = 1

       call xxdata_04( itmp   ,
     &                 ndlev  , ndtrn  , ndmet   , ndqdn , ntemp ,
     &                 titled , iz     , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa  , lbseta  , prtwta, cprta ,
     &                 il     , qdorb  , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga , isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla   , ipla    , zpla  ,
     &                 nv     , scef   ,
     &                 itran  , maxlev ,
     &                 tcode  , i1a    , i2a     , aval  , scom  ,
     &                 beth   ,
     &                 iadftyp, lprn   , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp  , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp  , itieactn, ltied
     &                )


C Set-up output format for configuration length and L quantum number size

       call xxslna(il, cstrga, ilen)
       if (ilen.LT.18) then
          igap = 18 - ilen + 1
       else
          igap = 1
       endif

       l_max = 0
       do ind = 1, il
          l_max = max(l_max, ila(ind))
       end do
       if (l_max.GT. 10) then
          ilgap = 2
       else
          ilgap = 1
       endif

       f_220 = '(i5,1x,1a??,??x,1H(,i1,1H),i??,1H(,f4.1,1H),1x,f15.1)'
       write(f_220(10:11), '(i2.2)')ilen
       write(f_220(13:14), '(i2.2)')igap
       write(f_220(29:30), '(i2.2)')ilgap

       f_1002 = '(3HC  ,i5,1x,1a??,??x,1H(,i1,1H),' //
     &          'i??,1H(,f4.1,1H),1x,f15.1)'
       write(f_1002(16:17), '(i2)')ilen
       write(f_1002(19:20), '(i2.2)')igap
       write(f_1002(35:36), '(i2.2)')ilgap

C Calculate orbital energies from orbital quantum defects

       do ind = 1, iorb
          call xxidtl(ind, n, l)
          eorb(ind) = (dfloat(iz1) / (dfloat(n)-qdorb(ind)))**2
       end do

C Re-order energy list and re-index transition list

       do i=1,il
         ia_new(i) = ia(i)
         wa_new(i) = wa(i)
       end do

       if (lreorder) call xxr8ss(il, .TRUE., wa_new, ia_new)

       linorder = .TRUE.
       i=1
       do while (i.le.il)
         linorder = linorder.and.(ia(i).eq.ia_new(i))
         i=i+1
       end do

       if (.NOT.linorder) then
          do i = 1, itran
            i1a_new(i) = i4indfi4(il, ia_new, i1a(i))
            i2a_new(i) = i4indfi4(il, ia_new, i2a(i))
          end do
       else
          do i = 1, itran
            i1a_new(i) = i1a(i)
            i2a_new(i) = i2a(i)
          end do
       endif


C  Check for finite energy reversal A values

       ncom_r = 1
       if (lreverse) then
          do i = 1, itran

            if (tcode(i).EQ.' ') then
               iup  = i2a_new(i)
               ilow = i1a_new(i)

               diff = wa(iup) - wa(ilow)
               if (diff.LT.0.0.AND.aval(i).GT.1.0D-30) then
                  write(str_r(ncom_r),1001)ilow, iup, aval(i)
                  aval(i) = 1.0D-30
                  ncom_r = ncom_r + 1
               endif

            endif

          end do
       endif
       ncom_r = ncom_r - 1


C  Identify any levels above the ionisation potential

       ncom_i = 1
       if (lionpot) then
          do i = 1, il
             ind = ia_new(i)
             if (wa(ind).GT.bwno) then
                write(str_i(ncom_i), f_1002)i,cstrga(ind),isa(ind),
     &                       ila(ind),xja(ind),wa(ind)
                iabove(ind) = 1
                ncom_i = ncom_i + 1
             endif
          end do
       endif
       ncom_i = ncom_i - 1




C Write the new file

       write(iout,200)titled, iz, iz0, iz1, bwno

       do i = 1, il
          ind = ia_new(i)
          if (iabove(ind).EQ.0) then
            write(iout,f_220)i, cstrga(ind), isa(ind), ila(ind),
     &                       xja(ind), wa(ind)
          endif
       end do


C The -1 level terminator and orbital energies

       string = ' '
       write(string(1:8),'(a8)')'   -1   '

       ileft = 9
       do ind = 1, iorb

          degree = int(log10(eorb(ind)))
          if (degree.GE.3) then
             orbfmt = '(1x,f7.2)'
          elseif (degree.GE.2) then
             orbfmt = '(1x,f7.3)'
          elseif (degree.GE.1) then
             orbfmt = '(1x,f7.4)'
          else
             orbfmt = '(1x,f7.5)'
          endif

          iright = ileft + 7
          write(string(ileft:iright),orbfmt)eorb(ind)
          ileft = iright + 1

       end do

       call xxwstr(iout, string)


C Temperatures and transitions

       call temp_write(iout,nfmt,nv,scef,iz1,itype)

       do i = 1, itran
         ileft  = i2a_new(i)
         iright = i1a_new(i)
         if (iabove(ileft).EQ.0.AND.iabove(iright).EQ.0) then
            call rate_write(iout, nfmt, ileft, iright, nv, aval(i),
     &                      scom(1,i))
         endif

       end do

       write(f_line,'(5H(1X,I,i1,7H,/,1X,I,i1,5H,1X,I,i1,1H))')
     &                nfmt,nfmt,nfmt
       write(iout,f_line)-1,-1,-1



C Generate comment block for return

       if (ncom_r.GT.0.OR.ncom_i.GT.0) lcomment = .TRUE.

       k = 1
       if (ncom_r.GT.0) then

          comments(k) = 'C'
          k = k + 1
          comments(k) = 'C  Unphysical A values. Set A=1.0E-30'//
     &                          ' for following transitions :'
          k = k + 1
          comments(k) = 'C'
          do i = 1, ncom_r
            comments(k) = str_r(i)
            k = k + 1
          end do
          comments(k) = 'C'
          k = k + 1

       endif


       if (ncom_i.GT.0) then

          if (ncom_r.GT.0) then
             write(comments(k), 500)
             k = k + 1
          endif
          comments(k) = 'C'
          k = k + 1
          write(comments(k), 1003)bwno
          k = k + 1
          comments(k) = 'C'
          k = k + 1
          do i = 1, ncom_i
            comments(k) = str_i(i)
            k = k + 1
          end do
          comments(k) = 'C'
          k = k + 1

       endif

       if (lreorder.AND..NOT.linorder) then

          lcomment = .TRUE.
          if (ncom_r.GT.0.OR.ncom_i.GT.0) then
             write(comments(k), 500)
             k = k + 1
          endif
          comments(k) = 'C'
          k = k + 1
          comments(k) = 'C Level list :'
          k = k + 1
          comments(k) = 'C'
          k = k + 1

          nlines = il / 10 + min(1, mod(il, 10))

          do i = 1, nlines
            write(comments(k), '(3HC  ,10i4)')
     &           (ia(j), j=1+(i-1)*10, min(i*10,il))
            k = k + 1
          end do
          comments(k) = 'C'
          k = k + 1
          comments(k) = 'C Original level order :'
          k = k + 1
          comments(k) = 'C'
          k = k + 1
          do i = 1, nlines
            write(comments(k), '(3HC  ,10i4)')
     &           (ia_new(j), j=1+(i-1)*10, min(i*10,il))
            k = k + 1
          end do
          comments(k) = 'C'
          k = k + 1

       endif


       ncomm = k - 1


C----------------------------------------------------------------------

  200  format(a2,'+',i2,7x,i3,7x,i3,5x,f11.1)
  500  format('C', 79('-'))
 1001  format('C   ',i4,2x,i4,4x,'A = ',1pd10.2)
 1003  format('C The following levels were above the ionisation ',
     &        'potential : ',f12.1)

C----------------------------------------------------------------------

       end

