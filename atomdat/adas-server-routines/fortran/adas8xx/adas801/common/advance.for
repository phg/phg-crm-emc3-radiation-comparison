C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/common/advance.for,v 1.1 2004/07/06 11:15:31 whitefor Exp $ Date $Date: 2004/07/06 11:15:31 $
C
      subroutine advance(nunit, num)

      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  :  Advance num records along the open file nunit. 
C
C  CALLING PROGRAM: ADAS801 - IFG part.
C
C  INPUT    : (I)    nunit   =  unit number of D11 file.
C  INPUT    : (I)    num     =  number of lines to advance or go back.
C
C
C  ROUTINES : None
C
C  
C  AUTHOR   : Martin O'Mullane,
C
C  VERSION  : 1.1                          
C  DATE     : 13-3-1992
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------      
      integer    nunit , num , i
C-----------------------------------------------------------------------      
      character  str*1
C-----------------------------------------------------------------------      
 
      if (num.ge.0) then
        
        do i=1,num
          read(nunit,'(a)',end=999)str
        end do
      
      else
       
        do i=1,abs(num)
          backspace(nunit)
        end do
      
      endif
 
 
 999  continue
      end
