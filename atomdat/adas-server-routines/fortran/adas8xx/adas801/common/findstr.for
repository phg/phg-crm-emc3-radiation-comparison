C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/common/findstr.for,v 1.1 2004/07/06 13:54:21 whitefor Exp $ Date $Date: 2004/07/06 13:54:21 $
C
      subroutine findstr(iunit, searchstr, fromstart, ierr)
 
      implicit none
C-----------------------------------------------------------------------
C
C  PURPOSE  : Searches for the first instance of a 10 character searchstring 
C             in a file and positions the file pointer there.    
C
C  INPUT    : (I)    iunit     =  unit number of search file.
C  INPUT    : (C)    searchstr =  string to seek.            
C  INPUT    : (L)    fromstart = .TRUE. start from the beginning
C                                .FALSE. continue from last position.
C  
C  OUTPUT   : (I)    ierr      =  -99 if the end of file is reached
C                                 without finding the search string.
C
C  ROUTINES  : None.
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 07-02-2000
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
      integer     iunit  , ierr
C-----------------------------------------------------------------------
      character   str*10 , searchstr*10
C-----------------------------------------------------------------------
      logical     fromstart
C-----------------------------------------------------------------------
 
      ierr = 0
      if (fromstart) rewind (iunit)
 
  10  read(iunit,50,end=800)str
      if (str.ne.searchstr) goto 10
      backspace iunit
      goto 999
 
  50  format(1x,a10)
 
 800  ierr = -99
 
 999  continue
      end
