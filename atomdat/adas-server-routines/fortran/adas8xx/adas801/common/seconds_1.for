C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas8xx/adas801/common/seconds_1.for,v 1.1 2004/07/06 15:19:07 whitefor Exp $ Date $Date: 2004/07/06 15:19:07 $
C
      subroutine seconds(t)
c
c          Timing routine (results for information only)
c               For other machines, set t=0.0 or add
c               appropriate code.
c
      real*8 t
c
c------------------------------------
c          Use this code for a Linux PC
      real*4 t1,second
c      t1=second()
c      t=dble(t1)
      t=0.0
c------------------------------------
c          Use this code for a SUN or OSF1 (DIGITAL UNIX)
c      real etime,t2
c      dimension t2(2)
c      t=etime(t2)
c-------------------------------------
c          Use this code for IBM RISC
c      it=mclock()
c      t=it
c      t=t/100.0
c-------------------------------------

      return
      end
