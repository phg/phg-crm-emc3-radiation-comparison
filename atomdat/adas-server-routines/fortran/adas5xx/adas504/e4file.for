CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4file.for,v 1.1 2004/07/06 13:41:38 whitefor Exp $ Date $Date: 2004/07/06 13:41:38 $
CX
      SUBROUTINE E4FILE( IUNIT , IZ0 , IRCODE , DSNAME )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4FILE *********************
C
C  PURPOSE: TO OPEN AN RADIATED POWER COEFFT 'IONELEC' DATA SET
C           BY DEFAULT, OR AN ALTERNATIVE DATA SET IF REQUIRED, FOR
C           RADIATING ELEMENT GIVEN BY IT NUCLEAR CHARGE 'IZ0'.
C           THIS WILL BE CONNECTED TO UNIT 'IUNIT'.
C
C  DATA SET OPENED: '$ADASUSER/DEFADF/<GROUP>/<TYPE>/
C			<GROUP>_<EXT>#<ELEMENT SYMBOL>'
C
C  CALLING PROGRAM: SPZD
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNIT   = UNIT TO WHICH DATA SET WILL BE CONNECTED
C  INPUT : (I*4)   IZ0     = NUCLEAR CHARGE OF ELEMENT REQUESTED
C
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => DATA SET SUCCESSFULLY CONNECTED
C                            1 => REQUESTED DATA  SET  MEMBER  DOES  NOT
C                                 EXISTS - DATA SET NOT CONNECTED.
C                            9 => REQUESTED DATA  SET  EXISTS BUT CANNOT
C                                 BE OPENED.
C  OUTPUT: (C*80)  DSNAME  = NAME OF OPENED DATA SET UNDER UNIX
C
C          (I*4)   IDLEN   = LENGTH, IN BYTES, OF FIXED 'DSNAME' PREFIX
C          (I*4)   LENF1   = FIRST NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF2   = LAST  NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF3   = FIRST NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF4   = LAST  NON-BLANK CHR OF 'DSNAME' TYPE PART
C	   (I*4)   LENF5   = FIRST NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF6   = LAST  NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF7   = FIRST NON-BLANK CHR OF 'DSNAME' USREXT PART
C          (I*4)   LENF8   = LAST  NON-BLANK CHR OF 'DSNAME' USREXT PART
CA         (C*1)   HASH    = '#'   IF NON-BLANK EXT, ELSE ' '.
C          (C*2)   XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)   ESYM    = ELEMENT SYMBOL FOR NUCLEAR CHARGE 'IZ0'
C          (C*3)   USREXT  = ADAS SOURCE DATA FILE EXTENSION
C          (C*80)  USERID  = ADAS SOURCE DATA USER ID
C          (C*8)   USRGRP  = ADAS SOURCE DATA GROUPNAME
CA         (C*80)  USRTYP  = SUB-DIRECTORY NAME (OPTIONAL)
CA         (C*5)   DEFADF  = DEFAULT ADF DIRECTORY FOR THIS ROUTINE,I.E.
CA                           ADF19
C
C          (L*4)   LEXIST  = .TRUE.  => REQUESTED  DATA  SET  EXISTS.
C                            .FALSE. => REQUESTED  DATA  SET  DOES  NOT
C                                       EXIST.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSPZD     ADAS      FETCHES/SETS ADAS SOURCE DATA FILENAME
C                               AND FILE EXTENSION
C          XFESYM     ADAS      CHARACTER*2 FUNCTION -
C                               GATHERS ELEMENT SYMBOL FOR NUC. CHARGE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    06/06/91
C UPDATE:  31/07/91 - PE BRIDEN: ADDED "ACTION='READ'" TO OPEN STATEMENT
C UPDATE:  08/08/91 - PE BRIDEN: ADDED "IRCODE=9" => OPEN ERROR
C UPDATE:  10/03/93 - PE BRIDEN: ADDED CALL TO XXUID AND USERID VARIABLE
C UPDATE:   2/09/93 - HPS      : ADDED CALL TO XXSPZD AND USRGRP, USRTYP
C                                  AND USREXT NAMES
C                                - NOW ALLOWS ANY INPUT DATASET FILENAME
C                                  AND EXTENSION
C                                - NOW ALLOWS ANY INPUT DATASET USER ID.
C UPDATE:  23/11/93 - PEB      : CORRECT ERROR - A '.' HAD MISTAKENLY
C                                BEEN PLACED BEFORE THE MEMBER NAME IN
C                                VARIABLE DSNAME.
C UPDATE:   4/11/94 - L.JALOTA : MODIFIED FOR UNIX,INCREASED NAME LENGTHS
C UPDATE:   7/11/94 - L.JALOTA : ADDED DEFADF.
C UPDATE:  21/11/94 - L.JALOTA : CORRECTED SIZE OF USRTYP.
C
C UPDATE:  24/03/95 - HPS      : INTRODUCED HASH TO ELIMINATE # IN FILE IF
C                                THERE IS NO EXTENSION PART OF THE FILE NAME
C                                ALTER LOGIC TO ALLOW USRTYP, USREXT TO BE A
C                                SINGLE CHARACTER.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IUNIT         , IZ0        , IRCODE      , IDLEN     ,
     &            LENF1         , LENF2      ,
     &            LENF3         , LENF4      ,
     &            LENF5         , LENF6      ,
     &            LENF7         , LENF8
C
      
C-----------------------------------------------------------------------
      CHARACTER   XFESYM*2      , ESYM*2     , USERID*80   , DSNAME*80 ,
     &            USRGRP*8      , USRTYP*80  , USREXT*3    , DEFADF*5  ,
     &            HASH*1
C-----------------------------------------------------------------------
      LOGICAL     LEXIST
C-----------------------------------------------------------------------
      PARAMETER (DEFADF='adf19')
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C IDENTIFY ADAS SOURCE DATA USER ID, GROUPNAME , TYPENAME
C AND FILE EXTENSION
C-----------------------------------------------------------------------
C
      USERID = '?'
      CALL XXUID(USERID)
C
      USRGRP = '?'
      USRTYP = '?'
      USREXT = '?'
      CALL XXSPZD(USRGRP , USRTYP , USREXT)

C
C-----------------------------------------------------------------------
C
      CALL XXSLEN( USRGRP , LENF1 , LENF2 )
      CALL XXSLEN( USRTYP , LENF3 , LENF4 )
      CALL XXSLEN( USERID , LENF5 , LENF6 )
      CALL XXSLEN( USREXT , LENF7 , LENF8 )
C
CA CONSTRUCT FILE NAME DEPENDING ON WHETHER OR NOT A SUB-DIRECTORY WAS
CA SPECIFIED.
C
      DSNAME = USERID(LENF5:LENF6)//'/'//DEFADF//'/'//
     &             USRGRP(LENF1:LENF2)//'/'
      IDLEN  = INDEX(DSNAME,' ') - 1
C
      IF (LENF7.EQ.0) THEN
          HASH = ' '
      ELSE
          HASH = '#'
      ENDIF
C
      IF (LENF3 .EQ. 0) THEN 
	 DSNAME = DSNAME(1:IDLEN)//USRGRP(LENF1:LENF2)//'_'
     &               //USREXT(LENF7:LENF8)//HASH
      ELSE 
	 DSNAME = DSNAME(1:IDLEN)//USRTYP(LENF3:LENF4)//'/'//
     &               USRGRP(LENF1:LENF2)//'_'//USREXT(LENF7:LENF8)//HASH
      END IF
C
      IDLEN  = INDEX(DSNAME,' ') - 1
      ESYM   = XFESYM( IZ0 )
C
         IF (IRCODE.EQ.0) THEN
C
            IF ( ESYM(2:2) .EQ. ' ' ) THEN
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:1)//'.dat'
            ELSE 
	       DSNAME = DSNAME(1:IDLEN)//ESYM(1:2)//'.dat'
            ENDIF
C
          INQUIRE( FILE=DSNAME , EXIST=LEXIST )
C
            IF (.NOT.LEXIST) THEN
               IRCODE = 1
            ELSE
	       OPEN( UNIT=IUNIT , FILE=DSNAME , STATUS='OLD'  , ERR=9999 )
               IRCODE = 0
            ENDIF
C
         ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C OPEN STATEMENT ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999 IRCODE = 9
      RETURN
C
C-----------------------------------------------------------------------
C
      END
