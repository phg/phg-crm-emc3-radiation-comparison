CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4titl.for,v 1.2 2004/07/06 13:42:32 whitefor Exp $ Date $Date: 2004/07/06 13:42:32 $
CX
      SUBROUTINE E4TITL( IBSEL  , DSNAME ,
     &                   CIION  , CITYPE , CIINFO ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS504/SPZD
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*80) DSNAME   = DATA FILE NAME UNDER UNIX INCLUDING PATH
C
C  INPUT : (C*5)  CIION    = SELECTED DATA-BLOCK: RADIATING ION
C  INPUT : (C*5)  CITYPE   = SELECTED DATA-BLOCK: RADIATION TYPE
C  INPUT : (C*20) CIINFO   = SELECTED DATA-BLOCK: INFORMATION STRING
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C	   (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C	   (I*4)  LEN_NAME = LENGTH OF FILENAME
C          (I*4)  IFIRST   = POSITION OF FIRST CHARACTER IN FILENAME
C          (I*4)  ILAST    = POSITION OF LAST CHARACTER IN FILENAME
C
C
C ROUTINES: 
CA         XXSLEN          = UTILITY ROUTINE WHICH FINDS FIRST AND LAST
CA                           NON-BLANK CHARACTERS IN A STRING.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2620
C
C DATE:    07/06/91
C
C UNIX-IDL PORT:
C
C UPDATE:  04/11/94 - L. JALOTA: MODIFIED FOR USE UNDER UNIX
C
C UPDATE:  26/03/95 - HPS      : CORRECTED CIION AND CITYP TO C*5
C
C VERSION: 1.1                          DATE: 25-5-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C UPDATE:  HP SUMMERS  20/12/95  - UNIX-IDL PORT
C
C VERSION: 1.2                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - MADE CHANGES IN LINE WITH THOSE MADE BY HUGH SUMMERS
C                 AT STRATHCLYDE - INCLUDING TIDYING UP COMMENTS AND
C                 HOW THE TITLE IS CONSTRUCTED
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL          
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*80       ,
     &           CIION*5         , CITYPE*5      , CIINFO*20  ,
     &           TITLX*120       ,
     &           C2*2
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
         CALL XXSLEN(DSNAME,IFIRST,ILAST)
	 LEN_NAME = ILAST-IFIRST+1
         IF (LEN_NAME.GT.80) IFIRST = ILAST - 80
         LEN_NAME = ILAST-IFIRST+1
         TITLX(1:LEN_NAME+1) = DSNAME(IFIRST:ILAST)
         POS_NOW = LEN_NAME+1         
	 WRITE(C2,1000) IBSEL
         TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
         POS_NOW = POS_NOW+9
         TITLX(POS_NOW:120) = ' <' // CIION // '> ' //
     &                  '- <' // CIINFO // '>    '

C
C-----------------------------------------------------------------------
1000  FORMAT(I2)
C-----------------------------------------------------------------------
      RETURN
      END
