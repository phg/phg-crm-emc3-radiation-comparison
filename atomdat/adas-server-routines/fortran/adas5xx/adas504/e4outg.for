CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4outg.for,v 1.3 2004/07/06 13:41:58 whitefor Exp $ Date $Date: 2004/07/06 13:41:58 $
CX
      SUBROUTINE E4OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   ESYM   , IZ0    , IZ    , IZ1   ,
     &                   CIION  , CITYPE , CIINFO        ,
     &                   TEVA   , PZDA   , ITVAL ,
     &                   TFITM  , PZDM   , NMX   ,
     &                   LGRD1  , LDEF1  , LFSEL ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4OUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL TO PLOT GRAPH
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE INTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(ZERO-DENSITY RADIATED POWER COEFFT. W cm**3)
C                                       VERSUS
C                             LOG10(ELECTRON TEMPERATURE (eV))
C
C  CALLING PROGRAM: ADAS504
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED   | NOT USED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C  INPUT : (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK, ION INFORMATION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: RADIATING ION - ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: RADIATING ION - NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION - CHARGE
C  INPUT : (I*4)  IZ1     = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION - CHARGE + 1
C
C  INPUT : (C*5)  CIION() = INPUT DATA FILE: RADIATING ION
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*5)  CITYPE()= INPUT DATA FILE: RADIATION TYPE
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*20) CIINFO()= INPUT DATA FILE: INFORMATION STRING
C                           DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (eV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  PZDA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED ZERO-
C                           DENSITY RADIATED POWER COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE VALUES
C
C  INPUT : (R*8)  TFITM() = MINIMAX: SELECTED TEMPERATURES (eV)
C  INPUT : (R*8)  PZDM()  = RADIATED POWER COEFFICIENTS AT 'TFITM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX GENERATED RADIATED POWER
C                           COEFFT./TEMP. VALUES FOR GRAPHICAL DISPLAY
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR COEFFICIENT.
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR COEFFICIENT.
C
C
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*28) STRG1   =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG2   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG3   =  DESCRIPTIVE STRING FOR RADIATOR SPECIES.
C          (C*28) STRG4   =  DESCRIPTIVE STRING FOR RADIATION TYPE
C          (C*28) STRG5   =  DESCRIPTIVE STRING FOR ADDITIONAL INFO
C          (C*28) STRG6   =  DESCRIPTIVE STRING FOR TEMP. LIST
C          (C*32) HEAD1   =  HEADING FOR RADIATION INFORMATION
C          (C*16) HEAD2   =  HEADING FOR RADIATOR INFORMATION
C          (C*24) HEAD3   =  HEADING FOR ELECTRON TEMPERATURES

C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C	   XXFLSH      IDL-ADAS  CALLS FLUSH COMMAND TO CLEAR PIPE.
C
C AUTHOR: H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    20/12/95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED DUPLICATE DEFINITION OF LOOP COUNTER I
C
C
C VERSION: 1.3                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS READING OF XMIN, XMAX ETC FROM IDL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    IZ0             , IZ               , IZ1       ,
     &           ITVAL           , NMX
      INTEGER    I                        
C-----------------------------------------------------------------------
      REAL*8     XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST          ,
     &           LGRD1           , LDEF1            , LFSEL
C-----------------------------------------------------------------------
CX    CHARACTER  TITLE*40       , TITLX*80          , TITLM*80  ,
      CHARACTER  TITLE*40       , TITLX*120         , TITLM*80  ,
     &           DATE*8         , ESYM*2            ,
     &           CIION*5        , CITYPE*5          , CIINFO*20
      CHARACTER  STRG1*28       , STRG2*28          ,
     &           STRG3*28       , STRG4*28          ,
     &           STRG5*28       , STRG6*28          ,
     &           HEAD1*32       , HEAD2*16          , 
     &           HEAD3*24      
C-----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)    , PZDA(ITVAL)      ,
     &           TFITM(NMX)     , PZDM(NMX)
C-----------------------------------------------------------------------
      DATA HEAD1 /'---- RADIATION INFORMATION  ----'/ ,
     &     HEAD2 /' RADIATOR     : '/                 ,
     &     HEAD3 /' ELECTRON TEMPERATURES: '/                 
      DATA STRG1 /' ELEMENT SYMBOL           = '/ ,
     &     STRG2 /' NUCLEAR CHARGE           = '/ ,
     &     STRG3 /'    SPECIES               = '/ ,
     &     STRG4 /'    RADIATION TYPE        = '/ ,
     &     STRG5 /'    ADDITIONAL INFO       = '/ ,
     &     STRG6 /'    INDEX     TE(eV)        '/
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') ESYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)')  CIION
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)')  CITYPE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A20)') CIINFO
      CALL XXFLSH(PIPEOU)
C
      WRITE(PIPEOU,*) ITVAL
      CALL XXFLSH(PIPEOU)
      DO  1 I = 1,ITVAL
         WRITE(PIPEOU,*) TEVA(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
C
      DO 3 I = 1,ITVAL
         WRITE(PIPEOU,*) PZDA(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
	    WRITE(PIPEOU,*) PZDM(I)
	    CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO 5 I = 1, NMX
            WRITE(PIPEOU,*) TFITM(I) 
	    CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      WRITE(PIPEOU,'(A28)') STRG1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG3
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG4
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG5
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG6
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A16)') HEAD2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A24)') HEAD3
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
