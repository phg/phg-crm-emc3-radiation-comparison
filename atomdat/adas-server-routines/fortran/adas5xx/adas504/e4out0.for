CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4out0.for,v 1.1 2004/07/06 13:41:50 whitefor Exp $ Date $Date: 2004/07/06 13:41:50 $
CX
       SUBROUTINE E4OUT0( IWRITE   , LFSEL  ,
     &                    TITLE    , TITLX  , TITLM , DATE   ,
     &                    IBSEL    , ITVAL  ,
     &                    ESYM     , IZ0    , IZ    , IZ1    ,
     &                    CIION    , CITYPE , CIINFO,
     &                    LTRNG    ,
     &                    TKEL     , TEVA   , TRED  ,
     &                    PZDA     ,
     &                    KPLUS1   , COEF   , CADAS
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED DATA-BLOCK
C            UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS504
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
CX                          SET NAME IN BYTES 1->35.
CA INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IBSEL   = DATA-BLOCK INDEX SELECTED FROM INPUT DATASET
C                           FOR ANALYSIS.
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED ELECTRON TEMPERATURES
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: RADIATING ION- ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: RADIATING ION- NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION CHARGE
C                                 ( SET TO -1 IF WHOLE ELEMENT)
C  INPUT : (I*4)  IZ1     = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION CHARGE +1
C                                 ( SET TO 1 IF WHOLE ELEMENT)
C
C  INPUT : (C*5)  CIION   = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION
C  INPUT : (C*5)  CITYPE  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATION TYPE
C  INPUT : (C*20) CIINFO  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           INFORMATION STRING
C
C  INPUT : (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'PZDA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'PZDA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (eV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TKEL()  = USER ENTERED: ELECTRON TEMPERATURES (kelvin)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TRED()  = USER ENTERED: ELECTRON TEMPERATURES (reduced
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  INPUT : (R*8)  PZDA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED ZERO-
C                           DENSITY RADIATED POWER COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1T     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT ELECTRON TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CELEM   = IONIZING ION  ELEMENT NAME
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C	   (C*80) CSTRNG  = MARKER IN TITLX WHICH IS USED TO GET FILENAME
C	   (I*4)  IFIRST  = MARKS END OF FILENAME IN TITLX
C	   (I*4)  ILAST   = MARKS END OF FILENAME IN TITLX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C	   XXFCHR     IDL-ADAS  GETS POSITION OF ONE STRING IN ANOTHER
C
C AUTHOR  : PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/37
C           JET EXT. 2520
C
C DATE:    07/06/91
C
C
C UNIX-IDL PORT:
C
C UPDATE:  20/12/95  HP SUMMERS - UNIX-IDL PORT
C                                 REMOVED HOLLERITH CONSTANT ZEROES
C                                 AND ONES FROM THE PRINTED OUTPUT
C
C VERSION: 1.1                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IBSEL         , ITVAL      ,
     &           IZ0           , IZ            , IZ1        , KPLUS1
      INTEGER    I             , IFIRST        , ILAST
C----------------------------------------------------------------------
      LOGICAL    LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)     , TITLM*(*)  , ESYM*2  ,
     &           CIION*5       , CITYPE*5      , CIINFO*20  , 
     &           DATE*8
      CHARACTER  XFELEM*12     ,
     &           C1T*1         , CIZ*2    ,
     &           CELEM*12      , CADAS*80
      CHARACTER  CSTRNG*80
C----------------------------------------------------------------------
      REAL*8     TKEL(ITVAL)   , TEVA(ITVAL)   , TRED(ITVAL) ,
     &           PZDA(ITVAL)   ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)
C----------------------------------------------------------------------
      DATA       CSTRNG/'BLK'/
C----------------------------------------------------------------------
C
C**********************************************************************
C
      CELEM = XFELEM( IZ0 )
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
CX    CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &     'ZERO DENSITY RADIATED POWER COEFFICIENT INTERROGATION',
     &     'ADAS504' , DATE
      WRITE(IWRITE,1002) TITLE
CX    WRITE(IWRITE,1003) TITLX(1:35) , IBSEL
CA UNIX PORT - FIND LENGTH OF FILENAME IN TITLX
CA
      CALL XXFCHR(TITLX(1:80), CSTRNG, IFIRST, ILAST)
      WRITE(IWRITE,1003) TITLX(1:IFIRST-1), IBSEL
      IF(IZ.EQ.-1)THEN
          CIZ='  '
      ELSE
          WRITE(CIZ,'(1A2)')IZ
      ENDIF
      WRITE(IWRITE,1004) CELEM  , ESYM   ,
     &                   IZ0    ,
     &                   CIION  , CIZ    , CITYPE ,
     &                   IZ1    , CIINFO
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES, DENSITIES AND IONIZATIONS PER PHOTON
C---------------------------------------------------------------------
C
      WRITE (IWRITE,1005)
C
         DO 1 I=1,ITVAL
            C1T = '*'
            IF (LTRNG(I)) C1T = ' '
            WRITE(IWRITE,1006) TKEL(I) , C1T , TEVA(I) , C1T , TRED(I) ,
     &                         PZDA(I)
    1    CONTINUE
C
      WRITE (IWRITE,1007)
      WRITE (IWRITE,1008)
      WRITE (IWRITE,1007)
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
            WRITE(IWRITE,1009)
C
            WRITE (IWRITE,1007)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1010) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1011) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1012) TITLM
            WRITE(IWRITE,1007)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,11('*'),' TABULAR OUTPUT FROM ',A54,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,11('*')/)
 1002 FORMAT(1H ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'ZERO-DENSITY RADIATED POWER COEFFICIENT AS A ',
     & 'FUNCTION OF ELECTRON TEMPERATURE'/
     & 2X,78('-')/
     & 23X,'DATA GENERATED USING PROGRAM: ADAS504'/23X,37('-'))
 1003 FORMAT(1H ,A80,' - DATA-BLOCK: ',I2)
 1004 FORMAT(1H ,'RADIATION INFORMATION:'/
     &       1H ,25('-')/
     &       1H ,'ELEMENT NAME                   =', 2X,A12 /
     &       1H ,'ELEMENT SYMBOL                 =', 2X,A2  /
     &       1H ,'NUCLEAR CHARGE            (Z0) =', 2X,I3  //
     &       1H ,'RADIATING ION:'/
     &       1H ,'   ION                         =', 2X,A5  /
     &       1H ,'   ION CHARGE             (Z)  =', 3X,A2  /
     &       1H ,'        NOTES:'/
     &       1H ,'   RADIATION TYPE              =', 2X,A5  /
     &       1H ,'   SCALING CHARGE FOR TE  (Z1) =', 2X,I3  /
     &       1H ,'   ADDITIONAL INFO             =', 2X,A20 )
 1005 FORMAT(1H /1H ,8X,'-------- ELECTRON TEMPERATURE --------',
     &               9X,'RADIATED POWER COEFFT.'/
     &           1H ,13X,'kelvin',8X,'eV',7X,'K/Z1**2',
     &               18X,'  W cm**3 ')
 1006 FORMAT(1H ,1P,10X,D10.3,2(1X,A,D10.3),17X,D10.3)
 1007 FORMAT(1H ,79('-'))
 1008 FORMAT(1H ,'NOTE: * => RADIATED POWER COEFFTS. EXTRAPOLATED ',
     &                     'FOR ELECTRON TEMPERATURE VALUE')
C
 1009 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL FIT - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(RATE-COEFFICIENT<W CM**3>) VERSUS ',
     &                'LOG10(ELECTRON TEMPERATURE<eV>) -')
 1010 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1011 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1012 FORMAT (1H ,A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
