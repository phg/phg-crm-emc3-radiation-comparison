CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4setp.for,v 1.1 2004/07/06 13:42:09 whitefor Exp $ Date $Date: 2004/07/06 13:42:09 $
CX
       SUBROUTINE E4SETP( NBSEL )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4SETP *********************
C
C  PURPOSE:  WRITES THE VALUE OF NBSEL OUT TO IDL
C
C  CALLING PROGRAM: ADAS504
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'E4DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBSEL   = NUMBER OF IONIZING ION COMBINATIONS READ IN.
C                           I.E. NUMBER OF DATA-BLOCKS.
C
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TP PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    05/06/91
C
C UNIX-IDL PORT:
C
C UPDATE:  20/05/95  HP SUMMERS - UNIX-IDL PORT
C
C VERSION: 1.1                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBSEL            
      INTEGER    PIPEOU
      PARAMETER ( PIPEOU = 6 )
C-----------------------------------------------------------------------
C
C **********************************************************************
C-----------------------------------------------------------------------
C WRITE NBSEL OUT TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, * ) NBSEL
      CALL XXFLSH( PIPEOU )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
