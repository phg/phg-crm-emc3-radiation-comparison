CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/spzd.for,v 1.2 2007/07/20 10:45:49 allan Exp $ Date $Date: 2007/07/20 10:45:49 $
CX
      SUBROUTINE SPZD( IBSEL  , IZ0IN  ,
     &                 ITVAL  , TVAL   ,
     &                 IZ     , IZ1  ,
     &                 PZDA   , LTRNG  ,
     &                 TITLX  , IRCODE
     &               )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: SPZD **********************
C
C  PURPOSE: TO EXTRACT AND INTERPOLATE  DENSITY INDEPENDENT  RADIATED
C           POWER COEFFICIENTS FOR GIVEN ELEMENT NUCLEAR CHARGE AND
C           DATA-BLOCK FOR A SET OF ELECTRON TEMPERATURES.
C
C           USES THE SAME ROUTINES AS ADAS504, EXCEPT FOR:
C
C           'E4FILE' - WHICH OPENS THE REQUESTED FILE.
C           'E4CHKB' - WHICH CHECKS INPUT  VALUES  ARE  CONSISTENT  WITH
C                      THE SELECTED DATA-BLOCK 'IBSEL' AND   'IBSEL'  IS
C                      IN RANGE.
C
C            THE FIRST OF THESE FUNCTIONS IS CARRIED  OUT  IN  'ADAS604'
C            VIA ISPF PANELS USING THE ROUTINE 'E4SPF0'  -  ADAS604 DOES
C            NOT REQUIRE THE ROUTINE 'E4CHKB' AS THE USER CANNOT  SELECT
C            AN INVALID VALUE FOR 'IBSEL' OR 'IBSEL'/ELEMENT COMBINATION
C
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)  IZ0IN   = NUCLEAR CHARGE OF REQUIRED ELEMENT
C
C  INPUT : (I*4)  ITVAL   = NUMBER OF ELECTRON TEMPERATURE VALUES
C  INPUT : (R*8)  TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  OUTPUT: (I*4)  IZ      = INPUT FILE - SELECTED DATA BLOCK:
C                           RADIATING ION CHARGE
C                                (SET TO -1 WHEN WHOLE ELEMENT)
C  OUTPUT: (I*4)  IZ1     = INPUT FILE - SELECTED DATA BLOCK:
C                           RADIATING ION CHARGE +1
C                                ( SET TO 1 WHEN WHOLE ELEMENT)
C
C  OUTPUT: (R*8)  PZDA()  = ZERO-DENSITY RADIATED POWER COEFFICIENTS
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  OUTPUT: (L*4)  LTRNG() =.TRUE.  => OUTPUT 'PZDA()'  VALUE WAS INTER-
C                                     POLATED  FOR  THE  USER  ENTERED
C                                     ELECTRON TEMPERATURE 'TVAL()'.
C                          .FALSE. => OUTPUT 'PZDA()'  VALUE WAS EXTRA-
C                                     POLATED  FOR  THE  USER  ENTERED
C                                     ELECTRON TEMPERATURE 'TVAL()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  OUTPUT: (C*80) TITLX   = INFORMATION STRING (DSN ETC.)
C  OUTPUT: (I*4)  IRCODE  = RETURN CODE FROM SUBROUTINE:
C                           0 => NORMAL COMPLETION - NO ERROR DETECTED
C                           1 => DATA SET MEMBER FOR IONIZING ION WITH
C                                NUCLEAR CHARGE  'IZ0IN'  CAN  NOT  BE
C                                FOUND/DOES NOT EXIST.
C                           2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                AND THOSE IN INPUT FILE.
C                           3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                OF RANGE OR DOES NOT EXIST.
C                           4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                           9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                INPUT DATA-SET.
C
C          (I*4)  NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)  NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)  IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)  IZ0LST  = LAST VALUE OF 'IZ0IN' FOR  WHICH  INPUT
C                           DATA WAS READ.
C          (I*4)  IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)  NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                           DATA SET.
C          (I*4)  IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C
C          (L*4)  LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                           .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)  ESYM    = INPUT FILE - IONIZING ION - ELEMENT SYMBOL
C          (C*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C          (C*3)  EXTLST  = ADAS SOURCE DATA FILE EXT. USED LAST TIME
C                           DATA WAS READ.
C          (C*80) UIDIN   = CURRENT ADAS SOURCE DATA USER ID.
C          (C*80) UIDLST  = ADAS SOURCE DATA USER ID USED LAST TIME
C                           DATA WAS READ.
C          (C*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C          (C*8)  GRPLST  = ADAS SOURCE DATA GROUPNAME USED LAST TIME
C                           DATA WAS READ.
CA 	   (C*80) TYPIN   = CURRENT SUB-DIRECTORY (OPTIONAL)
CA         (C*80) TYPLST  = SUB-DIRECTORY USED LAST TIME DATA WAS READ
C
C
C          (C*80) DSNREQ  = NAME OF DATA SET REQUESTED
C                           (MAY OR MAY NOT EXIST)
C          (C*80) DSNAME  = FULL NAME OF DATA SET INTERROGATED
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZOUT() = INPUT DATA FILE: IONIZING ION INITIAL CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ1OUT()= INPUT DATA FILE: IONIZING ION FINAL   CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  PZD(,)    =INPUT DATA SET -
C                            FULL SET OF IONIZATIONS RATE-COEFFICIENTS
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (C*5)  CIION() = INPUT DATA FILE - RADIATING ION
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*5)  CITYPE()= INPUT DATA FILE - RADIATION TYPE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*20) CIINFO()= INPUT DATA FILE - INFORMATION STRING
C                           DIMENSION: DATA-BLOCK INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E4FILE     ADAS      OPEN DATA SET FOR SELECTED ELEMENT
C          E4DATA     ADAS      FETCH INPUT DATA FROM SELECTED DATA SET
C          E4CHKB     ADAS      CHECK VALIDITY OF ELEMENT AND 'IBSEL'
C          E4SPLN     ADAS      INTERPOLATE DATA WITH ONE-WAY SPLINES
C          E4TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSPZD     ADAS      FETCHES/SETS ADAS SOURCE DATA FILE NAME+
C
C AUTHOR:  H.P. SUMMERS
C          K1/1/47
C          JET EXT. 4941
C
C DATE:    28/08/92
C
C UPDATE:  10/03/93 - PE BRIDEN: INTRODUCED CALL TO XXUID TO ESTABLISH
C                                IF USERID OF INPUT DATASET CHANGES
C                                BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES UIDIN,UIDLST,DSNREQ)
C
C UPDATE:   2/09/93 - HPS      : INTRODUCED CALL TO XXSPZD TO ESTABLISH
C                                IF USRGRP, USRTYP AND USREXT OF INPUT
C                                DATASET CHANGES BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES GRPIN,GRPLST,TYPIN,
C                                 TYPLST, EXTIN, EXTLST)
C
C UPDATE:   4/11/94 - L. JALOTA: MODIFIED TO RUN UNDER UNIX.
C				  CHANGED DIMENSIONS OF DSNAME,DSNREQ
C
C UPDATE:  26/03/95 - HPS      : CORRECTED CIION and CITYP TO C*5
C-----------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 25-05-95
C MODIFIED: UNKNOWN (SOMEONE FROM TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 20-07-07
C MODIFIED: Allan Whiteford
C	    - Small modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDIM
      INTEGER     IZ0MIN         , IZ0MAX
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 160   , NTDIM  = 24       )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 60       )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          , IZ0IN             , ITVAL       ,
     &            IZ             , IZ1               ,
     &            IRCODE
      INTEGER     IZ0LST         , IUNIT             , NBSEL       ,
     &            IZ0
C-----------------------------------------------------------------------
      LOGICAL     LOPEN
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , EXTIN*3           , EXTLST*3     ,
     &                             UIDIN*80          , UIDLST*80    ,
     &                             GRPIN*8           , GRPLST*8     ,
     &                             TYPIN*80          , TYPLST*80    ,
     &            DSNREQ*80      , DSNAME*80         , TITLX*80
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             , ITA(NSTORE)          ,
     &            IZOUT(NSTORE)             , IZ1OUT(NSTORE)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , PZDA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CIION(NSTORE)*5           , CITYPE(NSTORE)*5     ,
     &            CIINFO(NSTORE)*20
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDIM,NSTORE)        , PZD(NTDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE        DSNAME      , UIDLST     , GRPLST     , TYPLST   ,
     &            EXTLST      , IZ0LST     ,
     &            IUNIT
C-----------------------------------------------------------------------
      DATA        DSNAME /' '/   , UIDLST /' '/ , GRPLST /' '/     ,
     &            TYPLST /' '/   ,
     &            EXTLST /' '/   , IZ0LST /0/   ,
     &            IUNIT  /16/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IRCODE = 0
      LOPEN  = .FALSE.
C
C-----------------------------------------------------------------------
C ESTABLISH CURRENT ADAS SOURCE DATA UID, FILE AND FILE EXTENSION
C-----------------------------------------------------------------------
C
      UIDIN  = '?'
      CALL XXUID(UIDIN)
C
      GRPIN  = '?'
      TYPIN  = '?'
      EXTIN  = '?'
      CALL XXSPZD(GRPIN , TYPIN , EXTIN)
C
C-----------------------------------------------------------------------
C
         IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN
C
C-----------------------------------------------------------------------
C IF NEW ELEMENT ION CHARGE ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------
C
               IF ( (IZ0LST.NE.IZ0IN) .OR.
     &              (UIDLST.NE.UIDIN) .OR.
     &              (GRPLST.NE.GRPIN) .OR.
     &              (TYPLST.NE.TYPIN) .OR.
     &              (EXTLST.NE.EXTIN)      )  THEN
C
                  CALL E4FILE( IUNIT , IZ0IN , IRCODE , DSNREQ )
                     IF (IRCODE.EQ.0) THEN
                        LOPEN  = .TRUE.
                        IZ0LST = IZ0IN
                        UIDLST = UIDIN
                        GRPLST = GRPIN
                        TYPLST = TYPIN
                        EXTLST = EXTIN
                        DSNAME = DSNREQ
                        CALL E4DATA( IUNIT  , DSNAME ,
     &                               NSTORE , NTDIM  ,
     &                               ESYM   , IZ0    ,
     &                               NBSEL  , ISELA  ,
     &                               IZOUT  , IZ1OUT ,
     &                               CIION  , CITYPE , CIINFO ,
     &                               ITA    ,
     &                               TETA   , PZD
     &                             )
                     ENDIF
               ENDIF
C
C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED ELEMENT NUCLEAR CHARGE VALUE.
C-----------------------------------------------------------------------
C
               IF ( IRCODE.EQ.0 ) THEN
                  CALL E4CHKB( IUNIT , NBSEL  , IBSEL ,
     &                         IZ0IN , IZ0    ,
     &                         LOPEN , IRCODE
     &                       )
               ENDIF
C
C-----------------------------------------------------------------------
C 1) INTERPOLATE/EXTRAPOLATE WITH ONE DIMENSIONAL SPLINE.
C 2) CREATE TITLE FOR OUTPUT.
C-----------------------------------------------------------------------
C
               IF ( IRCODE.EQ.0 ) THEN
                  CALL E4SPLN( ITA(IBSEL)        , ITVAL         ,
     &                         TETA(1,IBSEL)     , TVAL          ,
     &                         PZD(1,IBSEL)      , PZDA          ,
     &                         LTRNG
     &                       )
                  CALL E4TITL( IBSEL         , DSNAME        ,
     &                         CIION(IBSEL)  , CITYPE(IBSEL) ,
     &                         CIINFO(IBSEL) , TITLX
     &                       )
C
                  IZ   = IZOUT(IBSEL)
                  IZ1  = IZ1OUT(IBSEL)
C
               ENDIF
C
         ELSE
            IRCODE = 4
         ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
