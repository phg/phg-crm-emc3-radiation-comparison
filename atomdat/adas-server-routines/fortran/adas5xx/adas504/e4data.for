CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4data.for,v 1.2 2004/07/06 13:41:36 whitefor Exp $ Date $Date: 2004/07/06 13:41:36 $
CX
       SUBROUTINE E4DATA( IUNIT  , DSNAME ,
     &                    NSTORE , NTDIM  ,
     &                    ESYM   , IZ0    ,
     &                    NBSEL  , ISELA  ,
     &                    IZ     , IZ1    ,
     &                    CIION  , CITYPE , CIINFO ,
     &                    ITA    ,
     &                    TETA   , PZD
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4DATA *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT RADIATED POWER COEFFICIENTS
C             OF AN ELEMENT AND ITS IONS.
C            (MEMBER STORED IN IONELEC.DATA - MEMBER PREFIX 'PZD#').
C
C  CALLING PROGRAM: ADAS504/SPZD
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH BLOCK FORMING A COMPLETE SET OF  RADIATED
C           POWER COEFFICIENT VALUES FOR GIVEN TEMPERATURES.
C           EACH DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER
C           DATA-BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           TEMPERATURES        : EV
C           RATE COEFFT         : W CM**3
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*80) DSNAME   = NAME OF DATA SET BEING READ
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NTDIM    = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C
C  OUTPUT: (C*2)  ESYM     = READ - IONISING ION - ELEMENT SYMBOL
C  OUTPUT: (I*4)  IZ0      = READ - IONISING ION - NUCLEAR CHARGE
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IZ()     = READ - RADIATING ION CHARGE
C                                   ( SET TO -1 IF WHOLE ELEMENT)
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (I*4)  IZ1()    = READ - RADIATING ION CHARGE +1
C                                   ( SET TO 1 IF WHOLE ELEMENT)
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*5)  CIION()  = READ - RADIATING ION (AS <ESYM>+(IZ()> )
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CITYPE() = READ - RADIATION TYPE
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*20) CIINFO() = READ - INFORMATION STRING
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  ITA()    = READ - NUMBER OF ELECTRON TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TETA(,)  = READ - ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  PZD(,)    =READ - FULL SET OF ZERO DENSITY RADIATED
C                                   POWER COEFFTS. (W CM**3)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: ELECTRON TEMPERATURE INDEX
C          (I*4)  NTNUM    = NUMBER OF ELECTRON TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*4)  CKEY1    = '****'  - INPUT BLOCK HEADER KEY
C          (C*5)  CKEY2    = 'TYPE'  - INPUT BLOCK HEADER KEY
C          (C*5)  CKEY3    = 'INFO'  - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL'  - INPUT BLOCK HEADER KEY
C          (C*10) C10      = GENERAL USE TEN BYTE CHARACTER STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    07/06/91
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:   4/11/94 - L. JALOTA - INCREASED SIZE OF DSNAME TO RUN UNDER
C 			        	UNIX
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)/HUGH SUMMERS
C               - MINOR MODIFICATIONS TO FORMAT STATEMENTS
C
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0                , I4FCTN              , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NTDIM                 , IZ0                 ,
     &           NBSEL
      INTEGER    IBLK                  , ITT                 ,
     &           NTNUM                 , IABT                ,
     &           IPOS1                 , IPOS2
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*80             , ESYM*2
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*4               , CKEY2*4             ,
     &           CKEY3*4               , CKEY4*4             ,
     &           C10*10                , C80*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         , ITA(NSTORE)         ,
     &           IZ(NSTORE)            , IZ1(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CIION(NSTORE)*5       , CITYPE(NSTORE)*5    ,
     &           CIINFO(NSTORE)*20
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)    , PZD(NTDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4
C
C-----------------------------------------------------------------------
C
      DATA       CSLASH / '/' /
      DATA       CKEY1  / '****'   /   , CKEY2  / 'TYPE'   /  ,
     &           CKEY3  / 'INFO'   /   , CKEY4  / 'ISEL'   /
C
C **********************************************************************
C
      LBEND = .FALSE.
C
C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) C80
      READ(C80  ,*   ) NBSEL
C
      IPOS1 = INDEX(C80,'/') + 1
      IPOS2 = IPOS1          + 1
      READ(C80(IPOS1:IPOS2),1001) ESYM
C
      IZ0   = I4EIZ0( ESYM )
C
         IF (NBSEL.GT.NSTORE) THEN
            WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
            NBSEL = NSTORE
         ENDIF
C
C***********************************************************************
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C***********************************************************************
C
         DO 1 IBLK=1,NBSEL
C
C-----------------------------------------------------------------------
C INPUT TITLE, ION, TYPE AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------
C
            IF (.NOT.LBEND) THEN
               READ(IUNIT,1000)   C80
C
                  IF ( C80(1:2).NE.'C-') THEN
C
                     CIION(IBLK) = C80(1:5)
                     C2          = C80(4:5)
                     IF(C2.EQ.'  ') C2='-1'
                     IZ(IBLK)    = I4FCTN( C2 , IABT )
                     IF(IZ(IBLK).EQ.-1)THEN
                      IZ1(IBLK)=1
                     ELSE
                      IZ1(IBLK)=IZ(IBLK)+1
                     ENDIF
C
                     IPOS2       = INDEX(C80(13:80),'/') + 12
C
                     READ(C80(13:IPOS2),*) ITA(IBLK)
C
                     CALL XXHKEY( C80 , CKEY1 , CSLASH , C10          )
                     CALL XXHKEY( C80 , CKEY2 , CSLASH , CITYPE(IBLK) )
                     CALL XXHKEY( C80 , CKEY3 , CSLASH , CIINFO(IBLK) )
                     CALL XXHKEY( C80 , CKEY4 , CSLASH , C2           )
C
                     ISELA(IBLK) = I4FCTN( C2  , IABT )
                     NTNUM       = ITA(IBLK)
C
                         IF (NTNUM.GT.NTDIM) THEN
                            WRITE(I4UNIT(-1),2001) DSNAME , IBLK  ,
     &                                            NTDIM   , NTNUM
                            STOP
                         ENDIF
C
C-----------------------------------------------------------------------
C INPUT TEMPERATURE AND RADIATED POWER COEFFTS. FOR BLOCK
C-----------------------------------------------------------------------
C
                     READ(IUNIT,1002) ( TETA(ITT,IBLK) , ITT=1,NTNUM )
C
                     READ(IUNIT,1002) ( PZD(ITT,IBLK)  , ITT=1,NTNUM )
C
                  ELSE
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1
                  ENDIF
C
            ENDIF
C
    1    CONTINUE
C
C***********************************************************************
C
 1000 FORMAT(1A80)
 1001 FORMAT(A2)
 1002 FORMAT(6D10.2)
C
 2000 FORMAT(/1X,31('*'),' E4DATA MESSAGE ',31('*')/
     &        2X,'INPUT RATE COEFFT. DATA SET NAME: ',A80/
     &        2X,'NUMBER OF ELEMENT ION DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' E4DATA ERROR ',32('*')/
     &        2X,'INPUT RATE COEEFT. DATA SET NAME: ',A80/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT ELECTRON TEMPERATURES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' E4DATA MESSAGE ',31('*')/
     &        2X,'INPUT RATE COEFFT. DATA SET NAME: ',A80/
     &        2X,'INCONSISTENCY IN THE NUMBER OF DATA-BLOCKS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
