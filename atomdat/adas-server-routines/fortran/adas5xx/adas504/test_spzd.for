CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/test_spzd.for,v 1.1 2004/07/06 15:24:26 whitefor Exp $ Date $Date: 2004/07/06 15:24:26 $
CX
      PROGRAM TEST_SPZD	
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: TEST_SPZD *******************
C
C  PURPOSE: TO TEST THE INTERROGATION ROUTINE SPZD AND ALL IMBEDDED
C  CALLS TO SUBROUTINES. SPZD EXTRACTS AND INTERPOLATES ZERO DENSITY
C  RADIATIVE POWER LOSS COEFFICIENTS FOR IONS OF AN ELEMENT
C
C
C  INPUTS:
C
C         (CHAR*80) UIDIN   = CURRENT ADAS SOURCE DATA USER ID  PATHWAY.
C         (CHAR*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C         (CHAR*80) TYPIN   = CURRENT ADAS SOURCE DATA TYPENAME
C         (CHAR*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C
C         (I*4)     IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C         (I*4)     IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C         (I*4)     ITVAL   = NO. OF ELECTRON TEMPERATURES
C         (R*8)     TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                             DIMENSION: TEMPERATURE INDEX
C
C  OUTPUTS : 
C         (R*8)     PZDA()  = RADIATED POWER COEFFICIENT COEFFICIENT.
C                             DIMENSION: TEMPERATURE INDEX
C         (L*4)     LTRNG() =.TRUE.  => OUTPUT 'PZDA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PZDA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE INDEX
C
C         (C*120)   TITLX   = INFORMATION STRING (DSN ETC.)
C         (I*4)     IRCODE  = RETURN CODE FROM SUBROUTINE SSZD:
C
C
C  AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C	    31/10/94
C
C  UPDATE:  24/03/95 - HPS      : INTRODUCED MORE COMPLETE TEST, DIAGNOSTIC
C                                 PRINT AND NEW E4FILE SUBROUTINE.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     ITVAL
C-----------------------------------------------------------------------
      PARAMETER ( ITVAL = 10)
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , IRCODE           ,
     &            IZ             , IZ1                
C-----------------------------------------------------------------------
      CHARACTER   UIDIN*80      , GRPIN*8           ,      
     &            TYPIN*80      , EXTIN*3           ,
     &            TITLX*120             
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , 
     &            PZDA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)
C-----------------------------------------------------------------------
      DATA TVAL/ 5.0D0 , 1.0D1 , 1.5D1 , 2.0D1 , 3.0D1 ,
     &           4.0D1 , 5.0D1 , 6.0D1 , 8.0D1 , 1.0D2  /
C-----------------------------------------------------------------------
c------------------------------------------------------------------------
C  EXAMPLE  SETTING THE ADAS CENTRAL DEFAULT DATABASE PATHWAY
C-----------------------------------------------------------------------
C
        IRCODE = 0
C
C	UIDIN = '/export/home/adas/adas'
        UIDIN = '*'
C
 	CALL XXUID(UIDIN)
 
C	GRPIN = 'ionelec'
        GRPIN = '*'
 	TYPIN = '*'
C	EXTIN = 'pzd'
        EXTIN = '*'

	CALL XXSPZD(GRPIN,TYPIN,EXTIN)	
C
        IBSEL = 2
	IZ0IN = 6
C	

	CALL SPZD( IBSEL  , IZ0IN  ,
     &             ITVAL  , TVAL   ,
     &             IZ     , IZ1    , 
     &             PZDA   , LTRNG  , 
     &             TITLX  , IRCODE
     &            )
C 
	WRITE(6,*) "TITLX      : ", TITLX
        WRITE(6,*) "IRCODE     : ", IRCODE
        WRITE(6,*) "IZ         : ", IZ
        WRITE(6,*) "IZ1        : ", IZ1
        WRITE(6,*) "PZDA(1)    : ", PZDA(1)
C
C-----------------------------------------------------------------------
      END
