CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas504/e4spln.for,v 1.2 2004/07/06 13:42:27 whitefor Exp $ Date $Date: 2004/07/06 13:42:27 $
CX
      SUBROUTINE E4SPLN( ITA   , ITVAL   ,
     &                   TETA   , TEVA    ,
     &                   PZD    , PZDA    ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E4SPLN *********************
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE <EV> ) VERSUS
C          LOG(SCALED RADIATED POWER COEFFICIENTS).
C          INPUT DATA FOR A GIVEN IONZING ION COMBINATION DATA-BLOCK.
C
C          USING  ONE-WAY SPLINES  IT  CALCULATES  THE  RADIATED POWER
C          COEFFICIENT  FOR  'ITVAL'  ELECTRON  TEMPERATURE VALUES  FROM
C          THE LIST OF ELECTRON TEMPERATURES READ IN FROM THE INPUT FILE
C
C          IF A  VALUE  CANNOT  BE  INTERPOLATED  USING  SPLINES  IT  IS
C          EXTRAPOLATED VIA 'XXSPLE'. (SEE NOTES BELOW).
C
C  CALLING PROGRAM: ADAS504/SPZD
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ITA     = INPUT DATA FILE: NUMBER OF ELECTRON TEMPERA-
C                           TURES READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  ITVAL   = NUMBER OF ISPF ENTERED ELECTRON TEMPERATURE
C                           VALUES FOR  WHICH  IOINIZATION RATE COEFFTS
C                           ARE REQUIRED FOR TABULAR/GRAPHICAL OUTPUT.
C
C  INPUT : (R*8)  TETA()  = INPUT DATA FILE: ELECTRON TEMPERATURES (EV)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (EV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  INPUT : (R*8)  PZD()    =INPUT DATA FILE: FULL SET OF ZERO DENSITY
C                           RADIATED POWER COEFFTS FOR THE DATA-BLOCK
C                           BEING ANALYSED.
C                           1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C  OUTPUT: (R*8)  PZDA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  ZERO
C                           DENSITY RADIATED POWER COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  OUTPUT: (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'PZDA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'PZDA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF INPUT  TEMPERATURE
C                                       VALUES. MUST BE >= 'ITA'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF OUTPUT TEMPERATURE
C                                       PAIRS.  MUST BE >= 'ITVAL'
C          (I*4)  L1      = PARAMETER = 1
C
C          (R*8)  BCONST  = PARAMETER = 1/(SCALED BOLTZMANN'S CONSTANT)
C
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           TEMPERATURES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           TEMPERATURE VALUES.
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (R*8)  XIN()   = LOG( DATA FILE ELECTRON TEMPERATURES )
C          (R*8)  YIN()   = LOG( DATA FILE SCALED ION. RATE COEFFTS.)
C          (R*8)  XOUT()  = LOG( USER ENTERED ELECTRON TEMPS.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED SCALED ION. RATE COEF)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C          ONE-DIMENSIONAL SPLINE CARRIED OUT BY THIS SUBROUTINE:
C
C           LOG( Pzd )  VS. LOG( Te )
C
C           Te      = electron temperature (units: eV)
C           Pzd     = zero density radiated power coefficient
C                                          (units: W cm**3)
C
C           Extrapolation criteria:
C
C           Low  Te: zero gradient  extrapolation (i.e. DY(1)  = 0.0)
C           High Te: zero curvature extrapolation (i.e. DDY(N) = 0.0)
C
C           (These criteria are met by calling XXSPLE with IOPT=4)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2620
C
C DATE:    07/06/91
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES AND TIDIED COMMENTS
C                 SLIGHTLY
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN                   , NOUT            , L1
C-----------------------------------------------------------------------
      REAL*8     BCONST
C-----------------------------------------------------------------------
      PARAMETER( NIN    = 24           , NOUT = 24       , L1 = 1     )
C-----------------------------------------------------------------------
      PARAMETER( BCONST = 1.23977D-04  )
C-----------------------------------------------------------------------
      INTEGER    ITA                   , ITVAL
      INTEGER    IET                   , IT              , IOPT
C-----------------------------------------------------------------------
      REAL*8     R8FUN1            
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TETA(ITA)             , TEVA(ITVAL)     ,
     &           PZD(ITA)              , PZDA(ITVAL)
      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.ITA)
     &             STOP ' E4SPLN ERROR: NIN < ITA - INCREASE NIN'
      IF (NOUT.LT.ITVAL)
     &             STOP ' E4SPLN ERROR: NOUT < ITVAL - INCREASE NTOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX  = .TRUE.
      IOPT   = 4
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------
C
         DO 1 IET=1,ITA
            XIN(IET) = DLOG( TETA(IET) )
    1    CONTINUE
C
         DO 2 IT=1,ITVAL
            XOUT(IT) = DLOG( TEVA(IT)  )
    2    CONTINUE
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL USER ELECTRON TEMPERATURES
C-----------------------------------------------------------------------
C
         DO 3 IET=1,ITA
            YIN(IET) = DLOG( PZD(IET) )
    3    CONTINUE
C
      CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &             ITA   , XIN     , YIN    ,
     &             ITVAL , XOUT    , YOUT   ,
     &             DF    , LTRNG
     &           )
C
C-----------------------------------------------------------------------
C SET UP OUTPUT IONIZATION RATE COEFFICIENT ARRAY.
C-----------------------------------------------------------------------
C
         DO 4 IT=1,ITVAL
            PZDA(IT) = DEXP( YOUT(IT) )
    4    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
