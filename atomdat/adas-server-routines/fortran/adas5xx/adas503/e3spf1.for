CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas503/e3spf1.for,v 1.6 2004/07/06 13:40:30 whitefor Exp $ Date $Date: 2004/07/06 13:40:30 $
CX
      SUBROUTINE E3SPF1( DSFULL     , 
     &                   IBSEL      , LFSEL      , LDEF1    ,
     &                   LGRAPH     , L2FILE     , SAVFIL   ,
     &                   XMIN       , XMAX       , YMIN     , 
     &                   YMAX       , LPEND      , CADAS    ,
     &                   LREP		, IFLOGIC
     &                 )

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E3SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS503
C
C  SUBROUTINE:
C
C  INPUT :   (C*80)  DSFULL   = DATA FILE NAME
C  INPUT :   (I*4)   IBSEL    = SELECTED INPUT DATA BLOCK FOR ANALYSIS
C  INPUT :   (L*4)   LFSEL    = .TRUE.  => POLYNOMIAL FIT SELECTED
C				.FALSE. => NO POLYNOMIAL FIT SELECTED
C  OUTPUT:   (L*4)   LDEF1    = .TRUE.  => USER SLECTED AXES LIMITS
C				.FALSE. => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR TEMP OR DENSITY UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR TEMP OR DENSITY UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS IONIZATIONS/PHOTON
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT IONIZATIONS/PHOTON
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C  OUTPUT:   (L*4)   LREP     = .TRUE.  => REPLACE PAPER.TXT
C                               .FALSE. => DON'T
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH    IDL-ADAS   CALLS FLUSH TO CLEAR PIPES
C
C AUTHOR:  Lalit Jalota (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    23-Feb-1995
C
C MODIFIED :	6-MAR-1995	L.JALOTA	CHANGED CALL TO FLUSH TO 
C						XXFLSH. ALSO DEFINED 
C						ILOGIC, LDEF1 PROPERLY.
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 28-02-95
C MODIFIED: L. JALOTA  (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 28-02-95
C MODIFIED: L. JALOTA  (TESSELLA SUPPORT SERVICES PLC)
C               -
C
C VERSION: 1.3                          DATE: 06-03-95
C MODIFIED: L. JALOTA  (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED CALL TO FLUSH TO XXFLSH. 
C               - ALSO DEFINED ILOGIC, LDEF1 PROPERLY.
C
C VERSION: 1.4                          DATE: 07-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS SLIGHTLY AND ADDED CALL TO XXADAS
C
C VERSION: 1.5                          DATE: 13-10-97
C MODIFIED: Martin O'Mullane
C               - IMPLEMENTED APPEND FUNCTIONALITY
C
C VERSION: 1.6					DATE: 13-10-99
C MODIFIED:	RICHARD MARTIN
C		ADDED IFLOGIC FOR 'RETURN TO INPUT FILE SELECTION'
C
C-----------------------------------------------------------------------
      REAL*8       XMIN        , XMAX        , YMIN        , YMAX
C-----------------------------------------------------------------------
      CHARACTER    DSFULL*80   , SAVFIL*80   , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      ,
     &             LFSEL       , LDEF1       , LREP 
C-----------------------------------------------------------------------
      INTEGER      IBSEL       , IFLOGIC     , ILOGIC	     ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) IFLOGIC
      IF (IFLOGIC .EQ. ZERO) THEN 
	 LPEND = .FALSE.
      ELSE
         LPEND = .TRUE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LGRAPH = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               LDEF1  = .TRUE.
	       READ(PIPEIN,*) XMIN
	       READ(PIPEIN,*) XMAX
	       READ(PIPEIN,*) YMIN
	       READ(PIPEIN,*) YMAX
	    ELSE
	       LDEF1  = .FALSE.
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF
C
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
            READ(PIPEIN,*) ILOGIC
            IF(ILOGIC .EQ. ONE) THEN
               LREP = .TRUE.
            ELSE
               LREP = .FALSE.
            ENDIF
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
            CALL XXADAS(CADAS)
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
