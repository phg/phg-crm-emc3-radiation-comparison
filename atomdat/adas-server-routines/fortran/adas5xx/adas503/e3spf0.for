C UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas503/e3spf0.for,v 1.2 2004/07/06 13:40:09 whitefor Exp $	Date  $Date: 2004/07/06 13:40:09 $
C 
      SUBROUTINE E3SPF0( REP  ,  DSFULL ,  LDSEL )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E3SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS503
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME 
C
C  OUTPUT: (L*4)   LDSEL   = .TRUE.  => COPASE DATA SET INFORMATION
C                                       TO BE DISPLAYED BEFORE RUN.
C                          = .FALSE. => COPASE DATA SET INFORMATION
C                                       NOT TO BE DISPLAYED BEFORE RUN.
C	 		      NOTE :in UNIX  port this is handled by IDL.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    23/02/95
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSFULL*80
C-----------------------------------------------------------------------
      LOGICAL      LDSEL
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
