CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas503/e3titl.for,v 1.10 2011/08/09 16:29:40 mog Exp $ Date $Date: 2011/08/09 16:29:40 $
CX
      SUBROUTINE E3TITL( IBSEL  , DSNAME ,
     &                   ESYM   , IZ     ,
     &                   CWAVEL , CINDM  ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E3TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS503/SPEC
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*(*)) DSNAME   = FULL INPUT DATA SET NAME
C
C  INPUT : (C*2)  ESYM     = INPUT DATA SET: EMITTING ION ELEMENT SYMBOL
C  INPUT : (I*4)  IZ       = INPUT DATA SET: EMITTING ION CHARGE-STATE
C
C  INPUT : (C*10) CWAVEL   = SELECTED DATA-BLOCK: WAVELENGTH (ANGS.)
C  INPUT : (C*2)  CINDM    = SELECTED DATA-BLOCK: METASTABLE INDEX
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C          (C*3)  C3       = GENERAL USE 3 BYTE  CHARACTER  STRING
C	   (C*9)  PATH     = UNIX ENVIRONMENT OF CURRENT PATH=$ADASCENT
C	   (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C	   (I*4)  LEN_NAME = LENGTH OF FILENAME
C          (I*4)  IFIRST   = POSITION OF FIRST CHARACTER OF DSNAME
C          (I*4)  ILAST    = POSITION OF LAST CHARACTER OF DSNAME
C
C ROUTINES: 
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    01/05/91
C
C UPDATE:  L. JALOTA  13/3/95   MODIFIED FOR USE UNDER UNIX 
C
C VERSION 1.7	TIM HAMMOND	13/10/95
C		ADDED VARIABLE C3 TO ALLOW 3 DIGIT DATA BLOCKS
C               TO BE READ FROM THE VARIABLE IBSEL
C
C VERSION 1.8	TIM HAMMOND	08/11/95
C		 - REMOVED SUPERFLUOUS VARIABLE TEMP_NAME
C               
C VERSION 1.9	TIM HAMMOND	08/11/95
C		 - REMOVED SUPERFLUOUS VARIABLE I4UNIT
C
C Version :  1.10
C Date    :  08-08-2011
C Modified:  Martin O'Mullane
C              - Line setting TITLX had characters beyond column 72.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL           , IZ
C     INTEGER    XXFCHR		 , XXSLEN
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*(*)      , ESYM*2         , 
     &           CINDM*2         , CWAVEL*10      , TITLX*120 ,
     &           C2*2            , C3*3
C-----------------------------------------------------------------------
	CALL XXSLEN(DSNAME,IFIRST,ILAST)
	LEN_NAME = ILAST-IFIRST+1
	TITLX(1:LEN_NAME+1) = DSNAME(IFIRST:ILAST)
	POS_NOW = LEN_NAME+1
        WRITE(C3,1001) IBSEL
        TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C3 // ';'
	POS_NOW = POS_NOW+9
        WRITE(C2,1000) IZ
        TITLX(POS_NOW:POS_NOW+6) = ESYM // '+' // C2  
	POS_NOW = POS_NOW + 5
	TITLX(POS_NOW:POS_NOW+29)=
     &         ' WAVELENGTH= '//CWAVEL//' MET='//CINDM
C
C-----------------------------------------------------------------------
1000  FORMAT(I2)
1001  FORMAT(I3)
C-----------------------------------------------------------------------
      RETURN
      END
