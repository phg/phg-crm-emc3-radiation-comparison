      PROGRAM TEST_SPEC	
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PRGOGRAM: TEST_SPEC *******************
C
C  PURPOSE: TO TEST THE INTERROGATION ROUTINE SPEC AND ALL IMBEDDED
C  CALLS TO SUBROUTINES. SPEC EXTRACTS AND INTERPOLATES PHOTON EMMISSIVITY
C  COEFFICIENTS FOR EMITTING IONS.
C
C
C  INPUTS:
C
C         (CHAR*80) UIDIN   = CURRENT ADAS SOURCE DATA USER ID  PATHWAY.
C         (CHAR*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C         (CHAR*80) TYPIN   = CURRENT ADAS SOURCE DATA TYPENAME
C         (CHAR*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C
C         (I*4)     IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C         (I*4)     IZIN    = ION CHARGE OF EMITTING ION
C         (I*4)     IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C         (I*4)     ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSIY PAIRS
C         (R*8)     TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C         (R*8)     DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUTS : 
C         (R*8)     WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C         (R*8)     PECA()  = IONIZATIONS PER PHOTON.
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C         (L*4)     LTRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)    LDRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C         (C*120)   CINFO   = INFORMATION STRING (DSN ETC.)
C         (I*4)     IRCODE  = RETURN CODE FROM SUBROUTINE SPEC:
C
C
C  AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C	    31/10/94
C
C  UPDATE:  22/03/95 _ HPS      : INTRODUCED MORE COMPLETE TEST, DIAGNOSTIC
C                                 PRINT AND NEW E3FILE SUBROUTINE.
C
C VERSION 1.2   Hugh Summers    02/08/11
C               - Use new spec.for with changed parameters and replacing
C                 e3data.for with xxdata_15.for 
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     ITVAL
C-----------------------------------------------------------------------
      PARAMETER ( ITVAL = 10)
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , ISIN              ,
     &            IRCODE
      INTEGER     I
C-----------------------------------------------------------------------
      REAL*8      WLNGTH
C-----------------------------------------------------------------------
      CHARACTER    UIDIN*80      , GRPIN*8           ,      
     &             TYPIN*80      , EXTIN*3           ,
     &             CINFO*90      , DSNAME*120         
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)          ,
     &            PECA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      DATA TVAL/ 5.0D0 , 1.0D1 , 1.5D1 , 2.0D1 , 3.0D1 ,
     &           4.0D1 , 5.0D1 , 6.0D1 , 8.0D1 , 1.0D2  /
      DATA DVAL/ 1.0D11, 1.0D11, 1.0D11, 1.0D11, 1.0D11,
     &           1.0D11, 1.0D11, 1.0D11, 1.0D11, 1.0D11 /
C-----------------------------------------------------------------------
c------------------------------------------------------------------------
C  EXAMPLE  SETTING THE ADAS CENTRAL DEFAULT DATABASE PATHWAY,
C-----------------------------------------------------------------------
C
        IRCODE = 0
        DSNAME = ''
C
	UIDIN = '*'
C
 	CALL XXUID(UIDIN)
 
 	GRPIN = '*'
 	TYPIN = '*'
	EXTIN = '*'

	CALL XXSPEC(GRPIN,TYPIN,EXTIN)	
C
        IBSEL = 1
	ISIN  = 0
	IZ0IN = 1
C	

C       dsname='/home/adas/adas/adf15/pec96#c/pec96#c_pju#c3.dat'
C       IBSEL = 3
C 	ISIN  = 3
C 	IZ0IN = 6


	CALL SPEC( DSNAME , IBSEL  , ISIN   , IZ0IN  ,
     &             ITVAL  , TVAL   , DVAL   ,
     &             WLNGTH ,
     &             PECA   , LTRNG  , LDRNG  ,
     &             CINFO  , IRCODE
     &            )
C
	WRITE(6,*) "ADF15 file : ", DSNAME
        WRITE(6,*) "IRCODE     : ", IRCODE
	WRITE(6,*) "CINFO      : ", CINFO
        WRITE(6,*) "IRCODE     : ", IRCODE
        DO I = 1, ITVAL
           WRITE(6,*) "TE, PECA    : ", TVAL(I), PECA(I)
        END DO
C
C-----------------------------------------------------------------------
      END
