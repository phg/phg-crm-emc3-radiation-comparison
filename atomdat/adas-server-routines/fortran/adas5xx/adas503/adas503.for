       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS503 **********************
C
C  ORIGINAL NAME:
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF PHOTON EMISSIVITIES.
C
C            THE SELECTED INPUT DATA SET IS FOR A SPECIFIC EMITTING  ION
C            I.E. ELEMENT AND CHARGE STATE. THIS FILE CONTAINS FOR  THIS
C            ION A NUMBER OF BLOCKS OF DATA REPRESENTING DIFFERENT  WAVE
C            LENGTHS. FOR EACH WAVELENGTH A TABLE OF  IONISATIONS/PHOTON
C            ARE GIVEN FOR COMBINATIONS  OF  ELECTRON  TEMPERATURES  AND
C            DENSITIES.  THIS  PROGRAM  ALLOWS  FOR THE EXAMINATION  AND
C            DISPLAY OF A GIVEN WAVELENGTH AS FOLLOWS:
C
C            TABULATED IONS./PHOTON FOR  SELECTED WAVELENGTH ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
CX DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
CX           DATA SET AS FOLLOWS:-
CX
CX                   'JETSHP.IONELEC.DATA(PEC#<EL><Z>)'
CX
CX            WHERE <EL> REPRESENTS THE EMITTING ION ELEMENT SYMBOL
CX                      E.G. BE, C
CX                  <Z>  REPRESENTS THE EMITTING ION CHARGE  STATE
CX                      E.G. 2 , 1
CX
CX                      (I.E. MEMBER NAMES ARE PEC#BE2 , PEC#C1)
CX
C
CA DATA:     UNDER UNIX SYSTEM THE DATA ARE CONTAINED IN DIRECTORIES
CA           WHICH REFLECT THE DATA TYPE (E.G. ADF15) AND FURTHER PARTITIONED
CA           DEPENDING ON MEMBER NAME
CA              E.G. /disk2/adas/adas/adf13/pec93#be/pec93#be_pjr#be0.dat
CA
C            EACH DATA SET MEMBER IS A TWO WAY TABLE OVER  ELECTRON
C            TEMPERATURE AND DENSITY.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            ELECTRON TEMPERATURES : eV
C            ELECTRON DENSITIES    : cm-3
C            IONISATIONS PER PHOTON:
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NTDIM   = PARAMETER = MAXIMUM NUMBER OF ELECTRON TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDDIM   = PARAMETER = MAXIMUM NUMBER OF ELECTRON DENS-
C                                       ITIES  THAT  CAN  BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF ISPF ENTRED ELECTRON
C                                       TEMPERATURE/DENSITY PAIRS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED IONS PER PHOTON
C                           /TEMPERATUREOR DENSITY PAIRS  FOR GRAPHICAL
C                           OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ0     = INPUT FILE: EMITTING ION - NUCLEAR CHARGE
C          (I*4)  IZ      = INPUT FILE: EMITTING ION - CHARGE
C          (I*4)  IZ1     = INPUT FILE: EMITTING ION - CHARGE + 1
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  IBLK    = DATA-BLOCK WAVELENGTH INDEX
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE/DENSITY
C                           PAIRS.
C          (I*4)  ITTYP   = 1 => 'TIN(array)' UNITS: KELVIN
C                         = 2 => 'TIN(array)' UNITS: EV
C                         = 3 => 'TIN(array)' UNITS: REDUCED
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C
C          (R*8)  R8TCON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'           OR
C                                  LOWER LIMIT FOR DENSITY (cm-3)
C                                  - SEE 'LDFIT' FOR TYPE.
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'           OR
C                                  UPPER LIMIT FOR DENSITY (cm-3)
C                                  - SEE 'LDFIT' FOR TYPE.
C          (R*8)  XMINPU  = GRAPH: LOWER LIMIT FOR TEMPERATURE (eV) or
C                                  DENSITY (cm-3) - see 'LDFIT'
C          (R*8)  XMAXPU  = GRAPH: UPPER LIMIT FOR TEMPERATURE (eV) or
C                                  DENSITY (cm-3) - see 'LDFIT'
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR IONIZATIONS/PHOTON.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR IONIZATIONS/PHOTON.
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
CX         (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
CX                          .FALSE. => NO FILE ALLOCATED TO UNIT 10.
CX         (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
CX                          .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
CA	   (C*80) SAVFIL  = NAME OF FILE TO WHICH DATA WILL BE WRITTEN.
C          (L*4)  LDFIT   = .TRUE.  => FIT AND DISPLAY ELECTRON DENSITY
C                           .FALSE. => FIT AND DISPLAY ELECTRON TEMPS.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
CX         (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
CX                                      GHOST80 GRID FILE.
CX                          .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*2)  ESYM    = INPUT FILE: EMITTING ION - ELEMENT SYMBOL
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CX         (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
CA         (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM IDL)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
CX         (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
CA         (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET - NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IDA()   = INPUT DATA SET- NUMBER OF ELECTRON DENSITIES
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TIN()   = USER ENTERED ELECTRON TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  DIN()   = USER ENTERED ELECTRON DENSITIES. UNITS: CM-3
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TEVA()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TKEL()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TDFITM()= MINIMAX: SELECTED TEMPERATURES (eV) OR
C                                    DENSITIES (CM-3) FOR FITTING.
C                                    (SEE 'LDFIT' FOR TYPE)
C          (R*8)  PECA()  = SPLINE INTERPOLATED OR EXTRAPOLATED PHOTON
C                           EMISSIVITIES FOR THE USER ENTERED ELECTRON
C                           TEMPERATURE/DENSITY PAIRS.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  PECM()  = PHOTON EMISSIVITIES AT 'TDFITM()'.
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TEDA(,) = INPUT DATA SET -
C                           ELECTRON DENSITIES (UNITS: CM-3)
C                           1st DIMENSION: ELECTRON DENSITY INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TVALS(,,)= INPUT DATA SET - ELECTRON TEMPERATURES
C                            1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                                           ( SEE 'TETA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8)  PEC(,,)   =INPUT DATA SET -
C                            FULL SET OF PHOTON EMISSIVITIES VALUES
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: ELECTRON DENSITY     INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*10) CWAVEL()= INPUT DATA FILE: WAVELENGTH (ANGSTROMS)
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CFILE() = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CTYPE() = INPUT DATA FILE: DATA TYPE  CODE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CINDM() = FILE DATA FILE: METASTABLE INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)  LDRNG()=  .TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
CX         E3SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
CA         E3SPF0     ADAS      GATHERS INPUT FILE NAMES VIA IDL
C          E3DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
CX         E3CHKZ     ADAS      VERIFY EMITTER READ FROM MEMBER NAME OK
CX         E3SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
CX         E3ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
CA         E3ISPF     ADAS      GATHERS USERS VALUES VIA IDL
C          E3SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E3TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
CX	   E3OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
CA	   E3OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING IDL
C          E3OUT0     ADAS      RESULTS OUTPUT ROUTINE
CX         XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
CX         XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
CX         XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/10/91
C
C UPDATE:  05/12/91 - PE BRIDEN: 'NSTORE' INCREASED FROM 30 TO 100
C
C UPDATE:  28/02/92 - PE BRIDEN: 'NSTORE' INCREASED FROM 100 TO 150
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  06/05/94 - PE BRIDEN - ADAS91: INCREASED PARAMETER NSTORE
C                                         150 -> 350
C
C UPDATE:  23/02/95 - L. JALOTA - IDL_ADAS : MODIFIED TO RUN UNDER UNIX WITH 
C					     IDL INTERFACE AND PLOTTING.
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 28-02-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - MODIFY FOR RUNNING UNDER UNIX ENVIRONMENT WITH IDL
C
C VERSION: 1.2                          DATE: 28-02-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - 
C
C VERSION: 1.3                          DATE: 02-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - 
C
C VERSION: 1.4                          DATE: 07-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED 'RETURN TO SERIES MENU' FUNCTIONALITY, SEARCH
C                 FOR 'ISTOP' TO SEE CHANGES.
C               - ADDED CALL TO XX0000 TO ENABLE USE OF I4UNIT FUNCTION
C                 (LATER BY OTHER ROUTINES)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.5                          DATE: 15-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - INCREASED PARAMETERS: NSTORE: 350 -> 500
C                                       NTDIM:   20 -> 26
C                                       NDDIM:   20 -> 26
C                                       NDTIN:   20 -> 26
C                 JET VERSION HAS NTDIM -> 35 AND NDTIN REMAINING AT 20,
C                 BUT THE WAY THE IDL WAS ORIGINALLY WRITTEN MAKES IT
C                 EASIER IF THEY REMAIN THE SAME - THE FULL CHANGES CAN
C                 BE IMPLEMENTED LATER.
C VERSION: 1.6                          DATE: 22-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - INCREASED PARAMETERS: NTDIM:   26 -> 35
C                                       NDDIM:   26 -> 35
C                                       NDTIN:   26 -> 35
C                 AS MENTIONED ABOVE.
C
C VERSION: 1.7                          DATE: 13-10-97
C MODIFIED: Martin O'Mullane
C               - IMPLEMENTED APPEND FUNCTIONALITY
C
C VERSION: 1.8                          DATE: 13-10-99
C MODIFIED: Richard Martin
C               - MODIFIED TO ALLOW 'RETURN TO INPUT FILE SELECTION'
C			FROM OUTPUT WIDGET.
C			ALSO PASS 'PEC' ARRAY INTO E3ISPF
C               - INITIALISED STRINGS TITLE, TITLX, TITLM
C
C VERSION: 1.9                          DATE: 01-07-2017
C MODIFIED: Martin O'Mullane
C		- Use xxdata_15 to read adf15 file. This requires
C                 some new arrays some of which are not used in the 
C                 program.
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NTDIM        , NDDIM      , NDTIN      ,
     &           MAXDEG
      INTEGER    IUNT07       , IUNT10       , PIPEIN
      INTEGER    NMX
      INTEGER    L1           , L2
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE = 500 , NTDIM  = 35 , NDDIM =  35 , NDTIN = 35 ,
     &           MAXDEG =  19 )
      PARAMETER( IUNT07 =   7 , IUNT10 = 10 , PIPEIN = 5)
      PARAMETER( NMX    = 100 )
      PARAMETER( L1     =   1 , L2     =  2 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    ISTOP		, ILOGIC       
      INTEGER    NBSEL        , IBLK         , IFORM      ,
     &           IZ0          , IZ           , IZ1        ,
     &           IBSEL        , KPLUS1       ,
     &           ITVAL        , ITTYP        , IPAN
C-----------------------------------------------------------------------
CX    REAL*8     R8TCON
      REAL*8     XMIN         , XMAX         , 
     &           YMIN         , YMAX         ,
     &           TOLVAL
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , OPEN07       ,
     &           LGHOST       , LPEND        , LDFIT      ,
     &           LOSEL        , LFSEL        , 
     &           LDSEL        , LGRD1        , LDEF1      , LREP       ,
     &           LGRAPH       , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  ESYM*2       , REP*3        ,
     &           DATE*8       , DSFULL*80    , SAVFIL*80  ,
     &           TITLE*40     , TITLM*80     , TITLX*120  , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)               ,
     &           ITA(NSTORE)                 , IDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)                  , DIN(NDTIN)              ,
     &           TEVA(NDTIN)                 , TKEL(NDTIN)             ,
     &           PECA(NDTIN)                 , PECM(NMX)               ,
     &           TDFITM(NMX)                 , COEF(MAXDEG+1)
C-----------------------------------------------------------------------
      CHARACTER  CINDM(NSTORE)*2             , CFILE(NSTORE)*8         ,
     &           CTYPE(NSTORE)*8             , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN)                , LDRNG(NDTIN)
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)          , TEDA(NDDIM,NSTORE)
      REAL*8     TVALS(NTDIM,3,NSTORE)       ,
     &           PEC(NTDIM,NDDIM,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , ITVAL  /0/
CX    DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
      DATA OPEN10 /.FALSE./ , OPEN07 /.FALSE./ 
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Extra parameters for xxdata_15 - unused for calculations
C-----------------------------------------------------------------------
      integer   ndptnl  , ndptn   , ndptnc , ndcnct
      integer   ndstack , ndcmt
C-----------------------------------------------------------------------
      parameter ( ndptnl = 4   , ndptn   = 128  , ndptnc =  256 )
      parameter ( ndcnct = 100 , ndstack =  40  , ndcmt  = 4000 )
C-----------------------------------------------------------------------
      integer    nptnl         , ncnct          , ncptn_stack ,
     &           ncmt_stack
C-----------------------------------------------------------------------
      logical    lcmt          , lptn           , lres        , lsup
C-----------------------------------------------------------------------
      integer    nptn(ndptnl)          , nptnc(ndptnl,ndptn)
      integer    iptnla(ndptnl)        , iptna(ndptnl,ndptn)
      integer    iptnca(ndptnl,ndptn,ndptnc)
      integer    icnctv(ndcnct)
      integer    isstgr(nstore)        , iszr(nstore)
      integer    ispbr(nstore)         , isppr(nstore)
C-----------------------------------------------------------------------
      real*8     wavel(nstore)         , pec_max(nstore)
C-----------------------------------------------------------------------
      character  cptn_stack(ndstack)*80,cmt_stack(ndcmt)*80
C-----------------------------------------------------------------------



C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
	
      TITLE='NULL'
      TITLM='NULL'
      TITLX='NULL'  
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
CA UNIX PORT - XXDATE NOW COMMUNICATES WITH IDL VIA PIPE TO GET DATE.
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
CA UNIX PORT - IPAN NO LONGER USED.
CX100 IPAN  = 0
100   CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
CA - UNIX PORT : E3SPF0 MODIFIED TO COMMUNICATE WITH IDL VIA PIPES.
C-----------------------------------------------------------------------
C
      CALL E3SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CA  UNIX PORT - NO LONGER USED, BUT E3SPF0 STILL RETURNS 'YES' OR 'NO' 
CA	 	FROM IDL INTERFACE.
CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CA UNIX PORT - CHANGED IBM SPECIFIC KEYWORD "ACTION" 
CA
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD'  )
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CA UNIX PORT - THIS FUNCTION CARRIED OUT VIA IDL INTERFACE.
C-----------------------------------------------------------------------
CX       IF (LDSEL) THEN
CX          CALL XXTEXT(IUNT10)
CX          GOTO 100
CX       ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
C       CALL E3DATA( IUNT10 , DSFULL ,
C      &             NSTORE , NTDIM  , NDDIM  ,
C      &             IZ0    , IZ     , IZ1    , ESYM  ,
C      &             NBSEL  , ISELA  ,
C      &             CWAVEL , CFILE  , CTYPE  , CINDM ,
C      &             ITA    , IDA    ,
C      &             TETA   , TEDA   ,
C      &             PEC
C      &           )

      call xxdata_15( iunt10 , dsfull ,
     &                nstore , ntdim  , nddim  ,
     &                ndptnl , ndptn  , ndptnc , ndcnct ,
     &                ndstack, ndcmt ,
     &                iz0    , iz     , iz1    , esym   ,
     &                nptnl  , nptn   , nptnc  ,
     &                iptnla , iptna  , iptnca ,
     &                ncnct  , icnctv ,
     &                ncptn_stack     , cptn_stack      ,
     &                lres   , lptn   , lcmt   , lsup   ,
     &                nbsel  , isela  ,
     &                cwavel , cfile  , ctype  , cindm  ,
     &                wavel  , ispbr  , isppr  , isstgr , iszr  ,
     &                ita    , ida    ,
     &                teta   , teda   ,
     &                pec    , pec_max,
     &                ncmt_stack      , cmt_stack
     &              )

C
C-----------------------------------------------------------------------
C VERIFY THAT EMITTER ELEMENT/CHARGE VALUES READ AGREE WITH MEMBER NAME
C-----------------------------------------------------------------------
C
CX    CALL E3CHKZ( DSFULL , ESYM   , IZ     )
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS INPUT ELECTRON TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TETA()' IS IN eV.
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXTCON( L2          , IFORM                ,
     &                      IZ1         ,
     &                      ITA(IBLK)   , TETA(1,IBLK)         ,
     &                                    TVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
CA UNIX PORT - NO LONGER NEEDED.
C-----------------------------------------------------------------------
CX    CALL E3SETP( NBSEL  )
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
CA
CA UNIX PORT - ROUTINE E3ISPF MODIFIED TO COMMUNICATE WITH IDL USER
CA               INTERFACE VIA PIPE.
CA
C-----------------------------------------------------------------------
C
200   CONTINUE
      CALL E3ISPF( IPAN   , LPEND  ,
     &             NSTORE , NTDIM  , NDDIM  , NDTIN  ,
     &             NBSEL  ,
     &             CWAVEL , CFILE  , CTYPE  , CINDM  ,
     &             ITA    , IDA    ,
     &             TVALS  , TEDA   , PEC    ,
     &             TITLE  ,
     &             IBSEL  , LDFIT  ,
     &             ITTYP  , ITVAL  , TIN   , DIN   ,
     &             LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN  , YMAX
     &           )
CA UNIX PORT - NO LONGER USE IPAN
CX    IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN (TIN -> TKEL)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L1 , IZ1 , ITVAL , TIN , TKEL )
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV     (TIN -> TEVA)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L2 , IZ1 , ITVAL , TIN , TEVA )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV IF .NOT.LDFIT
CA - UNIX PORT DONE IN IDL NOW.
C-----------------------------------------------------------------------
C
CX       IF (LDFIT) THEN
CX          XMINPU = XMIN
CX          XMAXPU = XMAX
CX       ELSE
CX          XMINPU = R8TCON( ITTYP , L2 , IZ1 , XMIN )
CX          XMAXPU = R8TCON( ITTYP , L2 , IZ1 , XMAX )
CX       ENDIF
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------
C
      CALL E3SPLN( NTDIM             , NDDIM             ,
     &             ITA(IBSEL)        , IDA(IBSEL)        ,
     &             ITVAL             ,
     &             TETA(1,IBSEL)     , TEDA(1,IBSEL)     ,
     &             TEVA              , DIN               ,
     &             PEC(1,1,IBSEL)    , PECA              ,
     &             LTRNG             , LDRNG
     &           )
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITVAL > 2).
C-----------------------------------------------------------------------
C
      IF (ITVAL.LE.2) LFSEL = .FALSE.
C
         IF (LFSEL) THEN
               IF (LDFIT) THEN
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         ITVAL   , DIN      , PECA    ,
     &                         NMX     , TDFITM   , PECM    ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
               ELSE
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         ITVAL   , TEVA     , PECA    ,
     &                         NMX     , TDFITM   , PECM    ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
               ENDIF
CX          WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      CALL E3TITL( IBSEL         , DSFULL        ,
     &             ESYM          , IZ            ,
     &             CWAVEL(IBSEL) , CINDM(IBSEL)  ,
     &             TITLX
     &           )
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
C-----------------------------------------------------------------------
CA ADDED THE NEW CALL TO E3SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CA A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------
300   CONTINUE
      CALL E3SPF1( DSFULL        ,
     &             IBSEL         , LFSEL         , LDEF1    ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMIN          , XMAX          , YMIN     , YMAX   ,
     &             LPEND         , CADAS	 , LREP, ILOGIC
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
CA IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
C
      IF (ILOGIC.EQ.1) GOTO 200
      IF (ILOGIC.EQ.2) GOTO 100      
C
C----------------------------------------------------------------------
CA IF DESIRED - SAVE RESULTS TO FILE.
C----------------------------------------------------------------------
C 
      IF (L2FILE) THEN 
         IF (LREP.AND.OPEN07)THEN
            OPEN07 = .FALSE.
            CLOSE(IUNT07)
         ENDIF
         IF(.NOT.OPEN07)THEN
            OPEN(UNIT=IUNT07,FILE=SAVFIL,STATUS='UNKNOWN')
            OPEN07 = .TRUE.
         ENDIF
         
         CALL E3OUT0( IUNT07        , LDFIT        , LFSEL    ,
     &                TITLE         , TITLX        , TITLM    , DATE,
     &                IBSEL         , ITVAL        ,
     &                ESYM          , IZ0          , IZ       ,
     &                CWAVEL(IBSEL) , CFILE(IBSEL) ,
     &                CTYPE(IBSEL)  , CINDM(IBSEL) ,
     &                LTRNG         , LDRNG        ,
     &                TEVA          , DIN          ,
     &                TKEL          ,
     &                PECA          ,
     &                KPLUS1        , COEF         , CADAS
     &              )

      ENDIF
C
C-----------------------------------------------------------------------
C  IF SELECTED - GENERATE GRAPH
CA UNIX PORT - E1OUTG COMMUNICATES WITH IDL VIA PIPE.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN
         CALL E3OUTG( LGHOST        ,
     &                TITLE         , TITLX        , TITLM   , DATE   ,
     &                ESYM          , IZ0          , IZ     ,
     &                CWAVEL(IBSEL) , CFILE(IBSEL) ,
     &                CTYPE(IBSEL)  , CINDM(IBSEL) ,
     &                TEVA          , DIN          , PECA    , ITVAL  ,
     &                TDFITM                       , PECM    , NMX    ,
     &                LGRD1         , LDEF1        , LFSEL   , LDFIT  ,
     &                XMIN          , XMAX         , YMIN    , YMAX
     &              )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF (ISTOP.EQ.1) GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C

      IF(OPEN07) CLOSE(IUNT07)
      
 9999 END
