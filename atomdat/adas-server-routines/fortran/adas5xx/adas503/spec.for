      SUBROUTINE SPEC( DSNAME , IBSEL  , ISIN   , IZ0IN  ,
     &                 ITVAL  , TVAL   , DVAL   ,
     &                 WLNGTH ,
     &                 PECA   , LTRNG  , LDRNG  ,
     &                 CINFO  , IRCODE
     &               )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: SPEC **********************
C
C  PURPOSE: TO EXTRACT AND INTERPOLATE  PHOTON EMISSIVITIES FOR
C           EMITTING IONS.
C           USES THE SAME ROUTINES USED BY ADAS503, EXCEPT FOR:
C
C           'E3FILE' - WHICH OPENS THE REQUESTED FILE.
C           'E3CHKB' - WHICH CHECKS INPUT  VALUES  ARE  CONSISTENT  WITH
C                      THE SELECTED DATA-BLOCK 'IBSEL' AND   'IBSEL'  IS
C                      IN RANGE.
C
C            THE FIRST OF THESE FUNCTIONS IS CARRIED  OUT  IN  'ADAS503'
C            VIA ISPF PANELS USING THE ROUTINE 'E3SPF0'  -  ADAS503 DOES
C            NOT REQUIRE THE ROUTINE 'E3CHKB' AS THE USER CANNOT  SELECT
C            AN INVALID VALUE FOR 'IBSEL' OR 'IBSEL'/EMITTER COMBINATION
C
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  I/O   : (C*120) DSNAME  = SOURCE ADF15 DATA FILE. IF EMPTY, FILENAME
C                            CONSTRUCT FROM XXSPEC IS USED.
C  INPUT : (I*4)   IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)   ISIN    = SUPERSTAGE INDEX (ION CHARGE FOR /#01/
C                            OR UNPARTITIONED CASES) 
C                            
C  INPUT : (I*4)   IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C  INPUT : (I*4)   ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSITY PAIRS
C  INPUT : (R*8)   TVAL()  = ELECTRON TEMPERATURES (UNITS: EV)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)   DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (R*8)   WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C  OUTPUT: (R*8)   PECA()  = PHOTON EMISSIVITIES.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LTRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LDRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (C*90)  CINFO   = INFORMATION STRING
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => NORMAL COMPLETION - NO ERROR DETECTED
C                            1 => DATA SET MEMBER FOR EMITTING ION WITH
C                                 CHARGE 'ISIN' & ION CHARGE 'IZ0IN' CAN
C                                 NOT BE FOUND/DOES NOT EXIST.
C                            2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                 AND THOSE IN INPUT FILE.
C                            3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                 OF RANGE OR DOES NOT EXIST.
C                            4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                 ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                            5 => INVALID VALUE FOR 'ISIN' ENTERED.
C                                 ( 0  <= 'ISIN' <= 99 )
C                            9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                 INPUT DATA-SET.
C
C                            IRCODE/10 = odd  => metastable resolution
C                                                detected
C                                      = even => not meta. resolution
C                            IRCODE/20 = odd  => superstage partitioning 
C                                                detected
C                                      = even => not superstaged
C
C          (I*4)   NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)   NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   NDDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON DENS-
C                                      ITIES  THAT  CAN  BE  READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
c
c          (i*4)   ndptnl  = maximum level of partitions
c          (i*4)   ndptn   = maximum no. of partitions in one level
c          (i*4)   ndptnc  = maximum no. of components in a partition
c          (i*4)   ndcnct  = maximum number of elements in connection
c          (i*4)   ndstack = maximum number of partition text lines
c          (i*4)   ndcmt   = maximum number of comment text lines
c                            vector
c
C          (I*4)   IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)   IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)   IZ0LST  = LAST VALUE OF 'IZ0IN' FOR  WHICH  INPUT
C                            DATA WAS READ.
C          (I*4)   IZLAST  = LAST VALUE OF 'ISIN' FOR  WHICH  INPUT
C                            DATA WAS READ.
C          (I*4)   IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)   NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                            DATA SET.
C          (I*4)   IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C          (I*4)   IZ      = INPUT FILE - EMITTING ION - CHARGE
C          (I*4)   IZ1     = INPUT FILE - EMITTING ION - CHARGE + 1
C          (I*4)   IPOS    = USED IN CONVERTING CWAVEL -> WLNGTH
C
C          (L*4)   LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                            .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)   ESYM    = INPUT FILE - EMITTING ION - ELEMENT SYMBOL
C          (C*3)   EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C          (C*3)   EXTLST  = ADAS SOURCE DATA FILE EXT. USED LAST TIME
C                            DATA WAS READ.
CA         (C*80)  UIDIN   = CURRENT ADAS SOURCE DATA USER ID.
CA         (C*80)  UIDLST  = ADAS SOURCE DATA USER ID USED LAST TIME
C                            DATA WAS READ.
C          (C*8)   GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C          (C*8)   GRPLST  = ADAS SOURCE DATA GROUPNAME USED LAST TIME
C                            DATA WAS READ.
CA         (C*80)  TYPIN   = ADAS DATA FILE SUBDIRECTORY (OPTIONAL)
CA         (C*80)  TYPLST  = ADAS DATA FILE SUBDIRECTORY USED LAST TIME
C                            DATA WAS READ.
C          (C*11)  C11     = USED IN CONVERTING CWAVEL -> WLNGTH
CA         (C*80)  DSNREQ  = NAME OF DATA SET REQUESTED
C                            (MAY OR MAY NOT EXIST)
CA         (C*80)  DSNAME  = NAME OF DATA SET INTERROGATED
C
C          (I*4)   ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                            TURES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   IDA()   = INPUT DATA SET-NUMBER OF ELECTRON DENSITIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)   TETA(,) = INPUT DATA SET -
C                            ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   TEDA(,) = INPUT DATA SET -
C                            ELECTRON DENSITIES    (UNITS: cm-3)
C                            1st DIMENSION: ELECTRON DENSITY     INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   PEC(,,)   =INPUT DATA SET -
C                             FULL SET OF IONIZATIONS PER PHOTON
C                             1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                             2nd DIMENSION: ELECTRON DENSITY     INDEX
C                             3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*10)  CWAVEL() = INPUT FILE - WAVELENGTH (ANGSTROMS)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CFILE()  = INPUT FILE - SPECIFIC ION FILE SOURCE
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CTYPE()  = INPUT FILE - TYPE OF DATA (IE EXCIT., ETC)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*2)   CINDM()  = INPUT FILE - METASTABLE INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E3FILE     ADAS      OPEN DATA SET FOR SELECTED EMITTER
C          E3CHKB     ADAS      CHECK VALIDITY OF ION AND 'IBSEL'
C          E3SPLN     ADAS      INTERPOLATE DATA WITH TWO WAY SPLINES
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSPEC     ADAS      FETCHES/SETS ADAS SOURCE DATA FILE NAME
C                               COMPONENTS
C          XXDATA_15  ADAS      READS A COMPLETE ADF15 DATASET
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/10/91
C
C UPDATE:  05/12/91 - PE BRIDEN: 'NSTORE' INCREASED FROM 10 TO 100
C
C UPDATE:  28/02/92 - PE BRIDEN: 'NSTORE' INCREASED FROM 100 TO 150
C
C UPDATE:  10/03/93 - PE BRIDEN: INTRODUCED CALL TO XXUID TO ESTABLISH
C                                IF USERID OF INPUT DATASET CHANGES
C                                BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES UIDIN,UIDLST,DSNREQ)
C
C UPDATE:   2/09/93 - HPS      : INTRODUCED CALL TO XXSPEC TO ESTABLISH
C                                IF USRGRP, USRTYP AND USREXT OF INPUT
C                                DATASET CHANGES BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES GRPIN,GRPLST,TYPIN,
C                                 TYPLST, EXTIN, EXTLST)
C
C UPDATE:   6/05/94 - PEB      : INCREASED PARAMETER NSTORE 150 -> 350
C
C UPDATE:   3/11/94 - L.JALOTA : CHANGED DSNAME, UIDIN SIZE TO 80 CHARS.
C UPDATE:  23/11/94 - L.JALOTA : TIDIED UP STRING LENGTH DEFINITIONS
C
C VERSION: 1.1                  DATE: 25-05-95
C MODIFIED: TIM HAMMOND
C           - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                  DATE: 22-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C           - INCREASED PARAMETERS: NSTORE: 350 -> 500
C                                   NDDIM:   20 -> 26
C                                   NTDIM:   20 -> 35
C           - MODIFIED CONVERSION OF CWAVEL->WLNGTH
C             (ADDED VARIABLES C11 AND IPOS)
C
C VERSION:      1.3                             DATE: 20-09-99
C MODIFIED: RICHARD MARTIN
C               INCREASED TITLX TO CHAR*120
C
C Version:      1.4
C Modified:  Hugh Summers                       Date: 02-08-11
C               Altered to use xxdata_15.for in place of e3data.for,
C               adding superstage data capability. Parameters altered, 
C               added DSNAME(i/o), ISIN replaces IZIN, TITLX content 
C               changed and renamed as CINFO, IRCODE definition extended 
C
C Version  :  1.5
C Date     :  02-08-11
C Modified :  Martin O'Mullane
C               - Save iz0, nptln and nbsel between calls.
C               - Return immediately with error if the file 
C                 does not exist.
C               - Check that the adf15 file exists even it is the 
C                 same name as a previous call.                        
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDIM             , NDDIM
      INTEGER     IZ0MIN         , IZ0MAX
      integer     ndptnl         , ndptn             , ndptnc       ,
     &            ndcnct
      integer     ndstack        , ndcmt
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 500   , NTDIM  = 35       , NDDIM =  26  )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 86                      )
      parameter ( ndptnl = 4     , ndptn = 128       , ndptnc = 256 )      
      parameter ( ndcnct = 100   , ndstack = 40      , ndcmt = 2000 )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , ISIN              ,
     &            ITVAL          , IRCODE            , IRCODE_OLD
      INTEGER     IZ0LST         , IZLAST            ,
     &            IUNIT          , NBSEL             ,
     &            IZ0            , IPOS
      integer     is             , is1 
      integer     nptnl          , ncnct             , ncptn_stack  , 
     &            ncmt_stack
      integer     lenstr  
C-----------------------------------------------------------------------
      REAL*8      WLNGTH
C-----------------------------------------------------------------------
      LOGICAL     LOPEN   , LEXIST
      logical     lptn    , lres    , lsup    , lcmt  
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , EXTIN*3           , EXTLST*3     ,
     &                             UIDIN*80          , UIDLST*80    ,
     &                             GRPIN*8           , GRPLST*8     ,
     &                             TYPIN*80          , TYPLST*80    ,
     &            C11*11         ,
     &            DSNREQ*120     , DSNAME*120        , CINFO*90
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             ,
     &            ITA(NSTORE)               , IDA(NSTORE)
      integer     nptn(ndptnl)              , nptnc(ndptnl,ndptn)
      integer     iptnla(ndptnl)            , iptna(ndptnl,ndptn)
      integer     iptnca(ndptnl,ndptn,ndptnc)
      integer     icnctv(ndcnct)
      integer     isstgr(nstore)            , iszr(nstore)
      integer     ispbr(nstore)             , isppr(nstore)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)           ,
     &            PECA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CINDM(NSTORE)*2           , CFILE(NSTORE)*8       ,
     &            CTYPE(NSTORE)*8           , CWAVEL(NSTORE)*10
      character   cptn_stack(ndstack)*80    , cmt_stack(ndcmt)*80
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDIM,NSTORE)        , TEDA(NDDIM,NSTORE)
      REAL*8      PEC(NTDIM,NDDIM,NSTORE)
      real*8      wavel(nstore)             , pec_max(nstore)
C-----------------------------------------------------------------------
      SAVE        DSNREQ      , UIDLST     , GRPLST     , TYPLST    ,
     &            EXTLST      , IZ0LST     , IZLAST     ,
     &            IUNIT       , IRCODE_OLD
      SAVE        ITA         , IDA        , TETA       , TEDA      ,
     &            PEC         , CTYPE      , LRES       , LSUP      ,
     &            ISPBR       , ISPPR      , IPTNLA     , IS        ,
     &            ISZR        , iz0        , nptnl      , nbsel
C-----------------------------------------------------------------------
      DATA        UIDLST /' '/   , GRPLST /' '/ ,
     &            TYPLST /' '/   ,
     &            EXTLST /' '/   , IZ0LST /0/   , IZLAST /0/       ,
     &            IUNIT  /15/
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C BYPASS IF DATASET IS SAME AS IN PREVIOUS CALL OF SPEC.
C But check that the file exists and return wih ircode=1 if not.
C-----------------------------------------------------------------------

      IF (DSNAME.EQ.DSNREQ) THEN
          if (lenstr(dsname).gt.1) then
             inquire(file=dsname, exist=lexist)
             if (lexist) then
                IRCODE = IRCODE_OLD
                GOTO 10
             else
                ircode = 1
                return
             endif
          endif
      ENDIF
      

      IF(LENSTR(DSNAME).GT.1) THEN
          INQUIRE( FILE=DSNAME, EXIST=LEXIST)
          IF(LEXIST) THEN
               IRCODE = 0
               LOPEN  = .TRUE.
               dsnreq = dsname
               open(unit=iunit, file=dsnreq, status='old')
             else
                ircode = 1
                return
          ENDIF
      ELSE
          IRCODE = 0
          LOPEN  = .FALSE.
C
C-----------------------------------------------------------------------
C ESTABLISH CURRENT ADAS SOURCE DATA UID, FILE AND FILE EXTENSION
C-----------------------------------------------------------------------
C
          UIDIN  = '?'
          CALL XXUID(UIDIN)
C
          GRPIN  = '?'
          TYPIN  = '?'
          EXTIN  = '?'
          CALL XXSPEC(GRPIN , TYPIN , EXTIN)
C
C-----------------------------------------------------------------------
C
          IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN
C
C-----------------------------------------------------------------------
C IF NEW EMITTING ION ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------
C
               IF ( (IZLAST.NE.ISIN)  .OR.
     &              (IZ0LST.NE.IZ0IN) .OR.
     &              (UIDLST.NE.UIDIN) .OR.
     &              (GRPLST.NE.GRPIN) .OR.
     &              (TYPLST.NE.TYPIN) .OR.
     &              (EXTLST.NE.EXTIN)      )  THEN
C
                    CALL E3FILE( IUNIT , IZ0IN , ISIN , IRCODE , 
     &                           DSNREQ )
                    IF (IRCODE.EQ.0) THEN
                       LOPEN  = .TRUE.
                       IZLAST = ISIN
                       IZ0LST = IZ0IN
                       UIDLST = UIDIN
                       GRPLST = GRPIN
                       TYPLST = TYPIN
                       EXTLST = EXTIN
                       DSNAME = DSNREQ
                    ENDIF
               ELSE
                    DSNAME = DSNREQ
                    IRCODE = IRCODE_OLD
                    GO TO 10    
               ENDIF
          ELSE
             IRCODE = 4
             LOPEN = .FALSE.
          ENDIF
      ENDIF 

C-----------------------------------------------------------------------
C  Using general purpose xxdata_15.for.  ircode used to indicate if  
C  superstage encountered or if metastable resolved data.     
C-----------------------------------------------------------------------

       IF(LOPEN)THEN
           call xxdata_15( iunit  , dsname,
     &                     nstore , ntdim , nddim ,
     &                     ndptnl , ndptn , ndptnc, ndcnct,
     &                     ndstack, ndcmt ,
     &                     iz0    , is    , is1   , esym  ,
     &                     nptnl  , nptn  , nptnc ,
     &                     iptnla , iptna , iptnca,
     &                     ncnct  , icnctv,
     &                     ncptn_stack    , cptn_stack    ,
     &                     lres   , lptn  , lcmt  , lsup  ,
     &                     nbsel  , isela ,
     &                     cwavel , cfile , ctype , cindm ,
     &                     wavel  , ispbr , isppr , isstgr, iszr  ,
     &                     ita    , ida   ,
     &                     teta   , teda  ,
     &                     pec    , pec_max,
     &                     ncmt_stack     , cmt_stack
     &                   )
 
           if(lres) then
                ircode = ircode + 10
           endif

           if(lptn) then
                ircode = ircode + 20
            endif

       ENDIF

       IRCODE_OLD = IRCODE
C
C-----------------------------------------------------------------------
C CHECK VALIDITY OF ENTERED 'IBSEL' & SUPERSTAGE/ION CHARGE VALUE.
C-----------------------------------------------------------------------
C
   10  IF ((IRCODE-10*(IRCODE/10)).EQ.0 ) THEN
           CALL E3CHKB( IUNIT , NBSEL  , IBSEL ,
     &                  IZ0IN , ISIN   ,
     &                  IZ0   , IS     ,
     &                  LOPEN , IRCODE
     &                )
       ENDIF
C
C-----------------------------------------------------------------------
C INTERPOLATE WITH TWO WAY SPLINES.
C-----------------------------------------------------------------------
C
       IF ((IRCODE-10*(IRCODE/10)).EQ.0 ) THEN
            CALL E3SPLN( NTDIM             , NDDIM         ,
     &                   ITA(IBSEL)        , IDA(IBSEL)    ,
     &                   ITVAL             ,
     &                   TETA(1,IBSEL)     , TEDA(1,IBSEL) ,
     &                   TVAL              , DVAL          ,
     &                   PEC(1,1,IBSEL)    , PECA          ,
     &                   LTRNG             , LDRNG
     &                  )

C-----------------------------------------------------------------------
C CREATE INFORMATION STRING CINFO FOR OUTPUT.
C-----------------------------------------------------------------------

           write(cinfo(1:90),'(1a7,i4, '//
     &                       ' 1a7,1a8,'//
     &                       ' 1a5,L1, '//
     &                       ' 1a6,L1, '//
     &                       ' 1a7,i2,'//
     &                       ' 1a7,i2,'//
     &                       ' 1a7,i2,'//
     &                       ' 1a4,i2,'//
     &                       ' 1a4,i2,'//
     &                       ' 1a12)')
     &         '/ibsel=',ibsel,
     &         '/ctype=',ctype(ibsel),
     &         '/les=',lres,
     &         '/lsup=',lsup,
     &         '/ispbr=',ispbr(ibsel),
     &         '/isppr=',isppr(ibsel),
     &         '/iptnl=',iptnla(nptnl),
     &         '/is=',is,
     &         '/iz=',iszr(ibsel),
     &         '/           '
                                                
C
C MODIFIED - PEB - 10/04/96
C
            C11  = CWAVEL(IBSEL)//'A'
            IPOS = INDEX(C11,'*')
C
            IF (IPOS.EQ.0) THEN
                 IPOS = INDEX(C11,'A') - 1
                 READ(C11(1:IPOS),*) WLNGTH
            ELSE
                 WLNGTH = 0
            ENDIF
C
C END OF MODIFICATION
C
       ENDIF
C
C
C-----------------------------------------------------------------------
C
      RETURN
      END
