CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas503/e3out0.for,v 1.5 2004/07/06 13:39:36 whitefor Exp $ Date $Date: 2004/07/06 13:39:36 $
CX
       SUBROUTINE E3OUT0( IWRITE   , LDFIT  , LFSEL ,
     &                    TITLE    , TITLX  , TITLM , DATE   ,
     &                    IBSEL    , ITVAL  ,
     &                    ESYM     , IZ0    , IZ    ,
     &                    CWAVEL   , CFILE  ,
     &                    CTYPE    , CINDM  ,
     &                    LTRNG    , LDRNG  ,
     &                    TEVA     , DIN    ,
     &                    TKEL     ,
     &                    PECA     ,
     &                    KPLUS1   , COEF   , CADAS
     &                  )
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E3OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED WAVELENGTH DATA-
C            BLOCK UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS503
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LDFIT   = .TRUE. =>MINIMAX FIT CARRIED OUT ON ELECTRON
C                                    DENSITIES IF FITTED.
C                           .FALSE.=>MINIMAX FIT CARRIED OUT ON ELECTRON
C                                    TEMPERATURES IF FITTED.
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
CX                          SET NAME IN BYTES 1->35.
C  INPUT : (C*120) TITLX  = INFORMATION STRING CONTAINING INPUT DATA-
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IBSEL   = DATA-BLOCK INDEX SELECTED FROM INPUT DATASET
C                           FOR ANALYSIS.
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED ELECTRON TEMPERATURE/
C                           DENSITY PAIRS.
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: EMITTING ION ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: EMITTING ION NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT FILE: EMITTING ION CHARGE-STATE
C
C  INPUT : (C*10) CWAVEL  = SELECTED DATA-BLOCK: WAVELENGTH (ANGS.)
C  INPUT : (C*8)  CFILE   = SELECTED DATA-BLOCK:SPECIFIC ION FILE SOURCE
C  INPUT : (C*8)  CTYPE   = SELECTED DATA-BLOCK: DATA TYPE CODE
C  INPUT : (C*2)  CINDM   = SELECTED DATA-BLOCK: METASTABLE INDEX
C
C  INPUT : (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'PECA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'PECA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C
C  INPUT : (L*4)  LDRNG()= .TRUE.  => OUTPUT 'PECA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           .FALSE. => OUTPUT 'PECA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (eV)
C                           DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C  INPUT : (R*8)  DIN()   = USER ENTERED: ELECTRON TEMPERATURES (cm-3)
C                           DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C
C  INPUT : (R*8)  TKEL()  = USER ENTERED: ELECTRON TEMPERATURES (kelvin)
C                           DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C
C  INPUT : (R*8)  PECA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  IONI-
C                           ZATIONS/PHOTON FOR THE USER ENTERED TEMP./
C                           DENSITY PAIRS.
C                           DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1T     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT ELECTRON TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*1)  C1D     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT ELECTRON DENSITY.     (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CELEM   = EMITTING ION  ELEMENT NAME
C INPUT:   (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C	   (C*80) CSTRNG  = MARKER TO USED TO FIND TITLE IN TITLX
C	   (I*4)  IFIRST  = FIRST POSITION OF MARKER CSTRNG IN TITLX
C	   (I*4)  ILAST   = LAST POSITION OF MARKER CSTRNG IN TITLX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C	   XXFCHR     IDL-ADAS  FINDS POSITION OF ONE STRING IN ANOTHER
C
C AUTHOR  : H. P. SUMMERS
C           K1/1/57
C           JET EXT. 4941
C
C DATE:    11/10/91
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 28-02-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 02-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - MODIFIED TO EXTRACT VARIABLE LENGTH UNIX FILENAME
C                 FROM TITLX
C
C VERSION: 1.3                          DATE: 03-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               -
C
C VERSION: 1.4                          DATE: 22-01-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED HOLLERITH DATA FORMATS FROM OUTPUT
C
C VERSION: 1.5                          DATE: 07-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS SLIGHTLY AND COMMENTED OUT CALL
C                 TO XXADAS - THIS IS NOW HANDLED IN E3SPF1.FOR
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IBSEL         , ITVAL     ,
     &           IZ0           , IZ            , KPLUS1
      INTEGER    I             , IFIRST        , ILAST
C----------------------------------------------------------------------
      LOGICAL    LDFIT         , LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)     , TITLM*(*) , DATE*8  ,
     &           ESYM*2        , CINDM*2       , CFILE*8   ,
     &           CTYPE*8       , CWAVEL*10
      CHARACTER  XFELEM*12     ,
     &           C1T*1         , C1D*1         ,
     &           CELEM*12      , CADAS*80
      CHARACTER  CSTRNG*80
C----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)   , DIN(ITVAL)    ,
     &           TKEL(ITVAL)   ,
     &           PECA(ITVAL)   ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)  , LDRNG(ITVAL)
C----------------------------------------------------------------------
      DATA       CSTRNG/'BLK'/
C----------------------------------------------------------------------
C
C**********************************************************************
C
      CELEM = XFELEM( IZ0 )
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
CX    CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &                 'PHOTON EMISSIVITY INTERROGATION     ',
     &                 'ADAS503' , DATE
      WRITE(IWRITE,1002) TITLE
CX    WRITE(IWRITE,1003) TITLX(1:35) , IBSEL
CA  UNIX PORT - FIND LENGTH OF FILENAME IN TITLX
      CALL XXFCHR(TITLX(1:80), CSTRNG, IFIRST, ILAST)
      WRITE(IWRITE,1003) TITLX(1:IFIRST-1), IBSEL
      WRITE(IWRITE,1004) CELEM  , ESYM   ,
     &                   IZ0    , IZ     ,
     &                   CWAVEL , CFILE  ,
     &                   CTYPE  , CINDM
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES, DENSITIES AND PHOTON EMISSIVITIES
C---------------------------------------------------------------------
C
      WRITE (IWRITE,1005)
C
         DO 1 I=1,ITVAL
            C1T = '*'
            C1D = '*'
            IF (LTRNG(I)) C1T = ' '
            IF (LDRNG(I)) C1D = ' '
            WRITE(IWRITE,1006) TKEL(I)   , C1T , TEVA(I) ,
     &                         DIN(I)    , C1D , PECA(I)
    1    CONTINUE
C
      WRITE (IWRITE,1007)
      WRITE (IWRITE,1008)
      WRITE (IWRITE,1007)
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
               IF (LDFIT) THEN
                  WRITE(IWRITE,1009)
               ELSE
                  WRITE(IWRITE,1010)
               ENDIF
C
            WRITE (IWRITE,1007)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1011) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1012) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1013) TITLM
            WRITE(IWRITE,1007)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,20('*'),' TABULAR OUTPUT FROM ',A36,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,20('*')/)
 1002 FORMAT(1H ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'PHOTON EMISSIVITIES    AS A FUNCTION OF ELECTRON ',
     & 'TEMPERATURE AND DENSITY '/
     & 2X,73('-')/
     & 21X,'DATA GENERATED USING PROGRAM: ADAS503'/21X,37('-'))
CX  1003 FORMAT(1H ,A35,' - DATA-BLOCK: ',I2)
 1003 FORMAT(1H ,'FILE : ', (1A),' - DATA-BLOCK: ',I2)
 1004 FORMAT(1H ,'EMITTING ION INFORMATION:'/
     &       1H ,25('-')/
     &       1H ,'ELEMENT NAME                =', 2X,A12 /
     &       1H ,'ELEMENT SYMBOL              =', 2X,A2  /
     &       1H ,'NUCLEAR CHARGE         (Z0) =', 2X,I3  /
     &       1H ,'CHARGE STATE           (Z)  =', 2X,I3  /
     &       1H ,'WAVELENGTH (angstroms)      =', 2X,A10 /
     &       1H ,'SPECIFIC ION FILE SOURCE    =', 2X,A8  /
     &       1H ,'DATA TYPE  CODE             =', 2X,A8  /
     &       1H ,'METASTABLE INDEX            =', 2X,A2  )
 1005 FORMAT(1H /1H ,7X,'--- ELECTRON TEMPERATURE ---',3X,
     &               'ELECTRON DENSITY',6X,'PHOT. EMISS.'/
     &           1H ,13X,'kelvin',8X,'eV',15X,'cm-3',13X,'cm**3 s-1 ')
 1006 FORMAT(1H ,1P,10X,D10.3,1X,A,D10.3,9X,D10.3,1X,A,8X,D10.3)
 1007 FORMAT(1H ,79('-'))
 1008 FORMAT(1H ,'NOTE: * => PHOTON EMISSIVITIES    EXTRAPOLATED FOR ',
     &                     'TEMPERATURE/DENSITY VALUE')
C
 1009 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL FIT TO ELECTRON DENSITIES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(PHOTON EMISSIVITIES)    VERSUS ',
     &                'LOG10(ELECTRON DENSITY<cm-3>) -')
 1010 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL FIT TO ELECTRON TEMPERATURES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(PHOTON EMISSIVITIES)    VERSUS ',
     &                'LOG10(ELECTRON TEMPERATURE<eV>) -')
 1011 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1012 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1013 FORMAT (1H ,A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
