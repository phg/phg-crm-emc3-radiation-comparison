CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas503/e3chkb.for,v 1.2 2007/07/20 10:44:20 allan Exp $ Date $Date: 2007/07/20 10:44:20 $
CX
      SUBROUTINE E3CHKB( IUNIT  , NBSEL  , IBSEL  ,
     &                   IZ0IN  , IZIN   ,
     &                   IZ0    , IZ     ,
     &                   LOPEN  , IRCODE
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E3CHKB *********************
C
C  PURPOSE: TO CHECK THE SELECTED BLOCK (IBSEL) OF DATA  EXISTS  IN  THE
C           INPUT DATA SET.
C
C           IF SO IT REPRESENTS THE ENTERED VALUES OF
C           'IZ0IN' (NUCLEAR CHARGE OF EMITTING ION)          &
C           'IZIN'  (CHARGE OF EMITTING ION)
C
C           IT ALSO CLOSES THE INPUT DATA SET ALLOCATION IF OPEN.
C
C  CALLING PROGRAM: SPEC
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C  INPUT : (I*4)   NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                            DATA SET.
C  INPUT : (I*4)   IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C
C  INPUT : (I*4)   IZ0IN   = REQUESTED: NUCLEAR CHARGE OF EMITTING ION
C  INPUT : (I*4)   IZIN    = REQUESTED: CHARGE OF EMITTING ION
C
C  INPUT : (I*4)   IZ0     = INPUT FILE: NUCLEAR CHARGE OF EMITTING ION
C  INPUT : (I*4)   IZ      = INPUT FILE: CHARGE OF EMITTING ION
C
C  I/O   : (L*4)   LOPEN   = INPUT : .TRUE.  => INPUT DATA SET OPEN.
C                                    .FALSE. => INPUT DATA SET CLOSED.
C                            OUTPUT: ALWAYS RETURNED AS .FALSE.
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => NO ERROR DETECTED.
C                            2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                 AND THOSE IN INPUT DATA FILE.
C                            3 => SELECTED DATA-BLOCK  OUT OF RANGE  OR
C                                 DOES NOT EXIST.
C
C          (I*4)   I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C
C          (C*80)  DSNAME  = UNIX NAME OF DATA SET OPENED
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E3FILE     ADAS      OPEN DATA SET FOR SELECTED EMITTER
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/10/91
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:   1/11/94 - L. JALOTA - UPDATED TO RUN UNDER UNIX ON DEC-ALPHA
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 25-05-95
C MODIFIED: UNKNOWN (SOMEONE FROM TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 20-07-07
C MODIFIED: Allan Whiteford
C	    - Small modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     I4UNIT
      INTEGER     IUNIT          , IRCODE            ,
     &            NBSEL          , IBSEL             ,
     &            IZ0IN          , IZIN              ,
     &            IZ0            , IZ
C-----------------------------------------------------------------------
      LOGICAL     LOPEN
C-----------------------------------------------------------------------
      CHARACTER   DSNAME*80
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
         IF ( (IBSEL.LE.0) .OR. (IBSEL.GT.NBSEL) ) THEN
C
               IF (.NOT.LOPEN) THEN
                  CALL E3FILE( IUNIT , IZ0IN , IZIN , IRCODE , DSNAME )
                  IF (IRCODE.EQ.0) LOPEN = .TRUE.
               ENDIF
C
               IF (LOPEN) THEN
                  IRCODE = 3
                  WRITE(I4UNIT(-1),1000) IRCODE , NBSEL , IBSEL
               ENDIF
C
         ELSE
C
               IF ( ( IZ0IN.NE.IZ0 ) .OR.
     &              ( IZIN .NE.IZ  )      ) THEN
                  IRCODE = 2
               ELSE
                  IRCODE = 0
               ENDIF
C
         ENDIF
C
      IF (LOPEN) CLOSE( IUNIT )
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X,12('*'),' SPEC  RETURN CODE: ',I1,1X,12('*')/
     &        1X,'E3CHKB ERROR: SELECTED DATA BLOCK OUT OF RANGE'/
     &       15X,'MAXIMUM DATA-BLOCK INDEX  = ',I4/
     &       15X,'DATA-BLOCK INDEX SELECTED = ',I4/
     &        1X,46('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
