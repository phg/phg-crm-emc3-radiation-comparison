CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/adas506.for,v 1.8 2004/07/06 10:58:34 whitefor Exp $ Date $Date: 2004/07/06 10:58:34 $
CX
c#
c#  UNIX PORT Alessandro Lanzafame  adas506.for
c#
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS506 **********************
C
C  VERSION:  1.0  (ADAS91)
C
C  PURPOSE:  TO  GRAPH  AND INTERPOLATE  SELECTED  DATA  FROM  GFT
C            FILES  CONTAINING CONTRIBUTION FUNCTION DATA
C            AS A FUNCTION OF TEMPERATURE ( MODEL DATA)
C
C            IT ALLOWS THE EXAMINATION AND DISPLAY OF THE DATA FOR  A
C            SINGLE SELECTED RADIATIVE TRANSITION AS FOLLOWS:
C
C            TABULATED VALUES FOR THE SELECTED TRANSITION ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE, FROM WHICH INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF REQUESTED A MINIMAX
C            POLYNOMIAL  IS FITTED THROUGH THE  DATA  AND  GRAHICALLY
C            DISPLAYED. THE MINIMAX COEFFICIENTS AND THE ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE ARE OUTPUT
C            TO HARDCOPY.
C
C
Cx  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
Cx            DATA SET AS FOLLOWS:-
Cx
Cx                   'JETSHP.GFT<YR>#<SE>(<MEMBER>)'
Cx
Cx           WHERE, <YR> = IS A TWO DIGIT YEAR NUMBER
Cx                   <SE> = ELEMENT SYMBOL FOR ISO-ELECTRON SERIES
Cx                          (ONE OR TWO CHARACTERS)
Cx
Cx            THE <MEMBER> NAME IS CONSTRUCTED AS FOLLOWS:
Cx
Cx                   <CODE>#<ION>
Cx
Cx            WHERE , <CODE> = MODEL DISTINGUISHING CODE
Cx                    <ION>  = SPECIFIC ION OF THE ISOELECTRONIC
Cx                             SEQUENCE (E.G. 'C3'  EQUIV. TO C+3)
Cx
Cx
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           WAVELENGTHS          : ANGSTROM
C           ELECTRON TEMPERATURES: KELVIN
C           ELECTRON DENSITIES   : CM-3
C           ELECTRON PRESSURE    : K CM-3
C           GFT FUNCTIONS        : CM3 SEC-1
C
C
C  PROGRAM:
C
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAXIMUM OF INPUT DATA FILE TEMPS
C          (I*4)  NDTIN   = PARAMETER = MAX.NUMBER OF ISPF ENTRED TEMPS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C          (I*4)  IUNT20  = PARAMETER = OUTPUT UNIT FOR COLLECTION FILE
C
C          (I*4)  NPSPL   = PARAMETER =
C                           NUMBER  OF SPLINE INTERPOLATED GFT/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED   GFT/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ      =  RECOMBINED ION CHARGE READ FROM INPUT FILE
C          (I*4)  IZ0     =         NUCLEAR CHARGE READ FROM INPUT FILE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE READ FROM INPUT FILE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY INDEX
C                                            LEVELS.
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C                                            TRANSITIONS.
C          (I*4)  ISTRN   = INPUT DATA FILE: SELECTED TRANSITION INDEX
C                                            FOR ANALYSIS.
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GFT/TEMPERATURE
C                                     PAIRS FOR THE SELECTED TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C          (I*4)  IFOUT   = 1 => 'TINE(array)' UNITS: KELVIN
C                         = 2 => 'TINE(array)' UNITS: eV
C                         = 3 => 'TINE(array)' UNITS:REDUCED TEMPERATURE
C          (I*4)  IPAN    = ISPF PANEL NUMBER
CA                          UNIX PORT - IPAN NO LONGER NEEDED BUT KEPT
CA			    SO THAT ARGUMENT LISTS IN SUBROUTINE CALLS
CA			    ARE NOT CHANGED
C
C          (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C          (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C          (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C          (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C          (R*8)  R8TCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C          (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMINK   = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  XMAXK   = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR GFT FUNCTION(CM**3/S)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR GFT FUNCTION(CM**3/S)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
CA                          UNIX PORT - OPEN10 NO LONGER NEEDED BUT KEPT
CA                          SO THAT ARGUMENT LISTS IN SUBROUTINE CALLS
CA                          ARE NOT CHANGED
Cx          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
Cx                         = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LVALID  = .TRUE.  => REQUESTED DATA SET EXISTS & VALID
C                           .FALSE. => REQUESTED DATASET NOT VALID
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*2)  ELEM    = TWO CHARACTER ELEMENT SYMBOL
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CX         (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
CA         (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  I1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  I2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  SCEF()  = INPUT DATA FILE: TEMPERATURES (kelvin)
C          (R*8)  DENSA() = INPUT DATA FILE: ELECTRON DENSITIES (CM-3)
C          (R*8)  PRESA() = INPUT DATA FILE: ELECTRON PRESSURES (KCM-3)
C          (R*8)  RNHNE() = INPUT DATA FILE: HYDROGEN/ELECTRON NO. RATIO
C          (R*8)  TMA()   = INPUT DATA FILE: NOTIONAL TIME(OR ARB.)(SEC)
C          (R*8)  APWL()  = INPUT DATA FILE: APPROXIMATE WAVELENGTHS (A)
C          (R*8)  SWL()   = INPUT DATA FILE: EXACT WAVELENGTHS (A)
C          (R*8)  APWLO   = SELECTED APPROXIMATE WAVELENGTHS (A)
C          (R*8)  SWLO    = SELECTED EXACT WAVELENGTHS (A)
C          (R*8)  GOFTA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GFT VALUE AT 'SCEF()'
C          (R*8)  TINE()  = ISPF ENTERED ELECTRON TEMPERATURE VALUES.
C                           (NOTE: UNITS ARE GIVEN BY 'IFOUT')
C          (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (kelvin)
C          (R*8)  GFTOA() = SPLINE INTEROPLATED GFT VALUE AT 'TOA()'
C          (R*8)  DENOA() = SPLINE INTEROPLATED DENS. VALUE AT 'TOA()'
C          (R*8)  PREOA() = SPLINE INTEROPLATED PRES. VALUE AT 'TOA()'
C          (R*8)  RNHOA() = SPLINE INTEROPLATED NH/NE VALUE AT 'TOA()'
C          (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (kelvin)
C          (R*8)  GFTOSA()= SPLINE INTEROPLATED GFT VALUE AT 'TOSA()'
C          (R*8)  DENOSA()= SPLINE INTEROPLATED DENS. VALUE AT 'TOSA()'
C          (R*8)  PREOSA()= SPLINE INTEROPLATED PRES. VALUE AT 'TOSA()'
C          (R*8)  RNHOSA()= SPLINE INTEROPLATED NH/NE VALUE AT 'TOSA()'
C          (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (kelvin)
C          (R*8)  GFTOMA()= MINIMAX GENERATED GFT VALUE AT 'TOMA()'
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  GFTA(,) = RADIATIVE TRANSITION:
C                            GFT VALUES
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C
C          (C*18) CSTRGA()= CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) STRGA() = NOMENCLATURE FOR LEVEL 'IA()' INCLUDES:
C                           'CSTRGA()' AND QUANTUM NUMBERS.
C
C          (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C          (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C          (L*4)   LCOLL    = .TRUE.  => STORE G(TE) IN COLLECTION FILE
C				.FALSE. => DO NOT STORE
C          (C*80)  FILC   = COLLECTION FILENAMEC
C
C NOTE:    INPUT TEMPERATURES 'TINE()' ARE CONVERTED TO KELVIN 'TOA()'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E6SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          E6SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          E6DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          E6ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          E6TRAN     ADAS      GATHERS SELECTED TANSITION PARAMETERS
C          E6SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E6OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          E6OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO KELVIN
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION: CONVERT TEMP. TO KELVIN
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:     7/04/94
C
C UPDATE:  june26-95 Alessandro Lanzafame, University of Strathclyde
C          idl_adas Unix port
C
C          june26-95 Alessandro Lanzafame NDTIN=20 -> NDTIN=100
C                    Collection file option added
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 26-06-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 26-06-95
C MODIFIED: TIM HAMMOND 
C               - COMMENTED OUT EXTRANEOUS CALLS TO R8TCON.FOR
C
C VERSION: 1.3                          DATE: 09-11-95
C MODIFIED: TIM HAMMOND / ALESSANDRO LANZAFAME
C               - NDTRN=50 -> NDTRN=300
C               - NDTEM,NDTIN,NPSPL,NMX 100->101
C               - STRGA(NDLEV)*18 -> STRGA(NDLEV)*22
C
C VERSION: 1.4                          DATE: 03-04-96
C MODIFIED: TIM HAMMOND 
C               - TIDIED UP HEADER COMMENTS 
C               - ADDED MISSING COMMA AFTER (1A) IN FORMAT 1001
C
C VERSION: 1.5                          DATE: 31-05-96
C MODIFIED: ALESSANDRO LANZAFAME 
C               - NDTRN=300 -> NDTRN=800
C
C VERSION: 1.6                          DATE: 10-07-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED MENU BUTTONS
C
C VERSION: 1.7                          DATE: 06-08-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED OUTPUT UNIT IUNT07 FROM 7 TO 17 TO AVOID
C                 CLASHING WITH STANDARD ERROR STREAM 7 ON HP
C                 WORKSTATIONS.
C
C VERSION: 1.8                          DATE: 06-06-2003
C MODIFIED: Martin O'Mullane
C               - Read adf20 data using xxdata_20 rather than e6data.
C
C-----------------------------------------------------------------------
      INTEGER     NDLEV        , NDTRN        , NDTEM     , NDTIN      ,
     &            MAXDEG
      INTEGER     IUNT07       , IUNT10       , IUNT20
      INTEGER     NPSPL        , NMX
      INTEGER     L1
CA UNIX PORT
      INTEGER     IFIRST       , ILAST        , ISTOP     , PIPEIN
C-----------------------------------------------------------------------
      LOGICAL     LMLOG
C-----------------------------------------------------------------------
      PARAMETER ( NDLEV  = 110 , NDTRN  =800 , NDTEM = 101 , NDTIN =101,
     &            MAXDEG =  19 )
      PARAMETER ( IUNT07 =  17 , IUNT10 =  10, IUNT20 = 20 )
      PARAMETER ( NPSPL  = 101 , NMX    = 101 )
      PARAMETER ( L1     =   1 , PIPEIN = 5)
C-----------------------------------------------------------------------
      PARAMETER ( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER     I4UNIT
      INTEGER     IZ           , IZ0          , IZ1       ,
     &            IL           , ITRAN        , ISTRN     ,
     &            NV           , MAXT         , KPLUS1    ,
     &            IFOUT        , IPAN         , IFORM     ,
     &            IUPPER       , ILOWER       , LUPPER    , LLOWER
C-----------------------------------------------------------------------
CX    REAL*8      R8TCON
      REAL*8      WUPPER    , WLOWER
      REAL*8      XMIN         , XMAX         , XMINK     , XMAXK      ,
     &            YMIN         , YMAX
      REAL*8      TOLVAL
      REAL*8      APWLO        , SWLO
C-----------------------------------------------------------------------
      LOGICAL     OPEN10       , LGHOST       , LPEND     , LOSEL      ,
     &            LFSEL
      LOGICAL     LDSEL        , LGRD1        , LDEF1     , LVALID
CA - UNIX PORT THESE VARIABLES ADDED FOR OUTPUT SELECTION OPTIONS
      LOGICAL     LGRAPH       , L2FILE       , LCOLL     , LEXIST
C-----------------------------------------------------------------------
CX    CHARACTER   REP*3        , ELEM*2       , DATE*8    , DSFULL*80 ,
CX   &            TITLE*40     , TITLM*80  , TITLX*80
      CHARACTER   REP*3        , ELEM*2       , DATE*8    , DSFULL*80 ,
     &            TITLE*40     , TITLM*80  , TITLX*120
CA - UNIX PORT : OUTPUT TEST FILENAME OBTAINED FROM IDL SCREEN
      CHARACTER   SAVFIL*80    , FILC*80
C-----------------------------------------------------------------------
      INTEGER     IA(NDLEV)    , ISA(NDLEV)   , ILA(NDLEV)  ,
     &            I1A(NDTRN)   , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8      XJA(NDLEV)
      REAL*8      COEF(MAXDEG+1)              ,
     &            SCEF(NDTEM)  , DENSA(NDTEM) , PRESA(NDTEM) ,
     &            RNHNE(NDTEM) , TMA(NDTEM)   , GOFTA(NDTEM)
      REAL*8      APWL(NDTRN)  , SWL(NDTRN)
      REAL*8      TINE(NDTIN)  ,
     &            TOA(NDTIN)   , TOSA(NPSPL)  , TOMA(NMX)   ,
     &            GFTOA(NDTIN) , GFTOSA(NPSPL), GFTOMA(NMX) ,
     &            DENOA(NDTIN) , DENOSA(NPSPL),
     &            PREOA(NDTIN) , PREOSA(NPSPL),
     &            RNHOA(NDTIN) , RNHOSA(NPSPL)
      REAL*8      TSCEF(NDTEM,3)      , GFTA(NDTEM,NDTRN)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(NDTIN)
C-----------------------------------------------------------------------
      CHARACTER   CSTRGA(NDLEV)*18            , STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , MAXT   /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
C-----------------------------------------------------------------------
C#
      INTEGER   INDX(NDTRN)
C#
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
CA UNIX PORT - XXDATE NOW COMMUNICATES WITH IDL VIA PIPE TO GET DATE.
C-----------------------------------------------------------------------
C     
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
CA UNIX PORT - IPAN NO LONGER USED.
CX100 IPAN  = 0
100   CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10=.FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
CA - UNIX PORT : E3SPF0 MODIFIED TO COMMUNICATE WITH IDL VIA PIPES.
C-----------------------------------------------------------------------
C
      CALL E6SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CA  UNIX PORT - NO LONGER USED, BUT E3SPF0 STILL RETURNS 'YES' OR 'NO' 
CA	 	FROM IDL INTERFACE.
CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CA UNIX PORT - CHANGED IBM SPECIFIC KEYWORD "ACTION" 
CA
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD'  )
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CA UNIX PORT - THIS FUNCTION CARRIED OUT VIA IDL INTERFACE.
C-----------------------------------------------------------------------
CX       IF (LDSEL) THEN
CX          CALL XXTEXT(IUNT10)
CX          GOTO 100
CX       ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
CA e6data read new format
C-----------------------------------------------------------------------
C
      CALL xxdata_20( IUNT10 , NDLEV  , NDTEM , NDTRN ,
     &                ELEM   , IZ     , IZ0   , IZ1   ,         
     &                IL     ,                                  
     &                IA     , CSTRGA , ISA   , ILA   , XJA   , 
     &                NV     ,                                  
     &                SCEF   , DENSA  , PRESA , RNHNE , TMA   , 
     &                ITRAN  ,                                  
     &                I1A    , I2A    , APWL  , SWL   , GFTA  , 
     &                LVALID , indx                             
     &              )                                           
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------
C
         DO 1 IFORM=1,3
            CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
      CALL E6SETP( IZ0    , IZ    ,
     &             NDLEV  , IL    , ITRAN ,
     &             CSTRGA , ISA   , ILA   , XJA  ,
     &             STRGA
     &           )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
CA
CA UNIX PORT - ROUTINE E6ISPF MODIFIED TO COMMUNICATE WITH IDL USER
CA               INTERFACE VIA PIPE.
CA
C-----------------------------------------------------------------------
C
 200     CONTINUE
         CALL E6ISPF( IPAN   , LPEND ,
     &             NDTIN  , IL    ,
     &             NDTEM  , NV    , TSCEF ,
     &             ITRAN  , I1A   , I2A   , STRGA ,
     &             TITLE  ,
     &             ISTRN  , IFOUT ,
     &             MAXT   , TINE  ,
     &             LFSEL  , LOSEL , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             APWL   , SWL   ,
     &             XMIN   , XMAX  , YMIN  , YMAX
     &           )
CA - UNIX PORT : IPAN NO LONGER USED
CX    IPAN=-1

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C IDENTIFY REQUESTED TRANSITION PARAMETERS
C-----------------------------------------------------------------------
C
      CALL E6TRAN( NDLEV  , NDTRN  , NDTEM ,
     &             IL     , ISTRN  , NV    ,
     &             IA     , XJA    ,
     &             APWL   , SWL    ,
     &             I1A    , I2A    ,
     &             GFTA   ,
     &             IUPPER , ILOWER ,
     &             LUPPER , LLOWER ,
     &             WUPPER , WLOWER ,
     &             APWLO  , SWLO   ,
     &             GOFTA
     &           )
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TINE -> TOA)
C-----------------------------------------------------------------------
C
      CALL XXTCON( IFOUT , L1 , IZ1 , MAXT , TINE , TOA   )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO KELVIN.
C-----------------------------------------------------------------------
C
C      XMINK = R8TCON( IFOUT , L1 , IZ1 , XMIN )
C      XMAXK = R8TCON( IFOUT , L1 , IZ1 , XMAX )
C
C-----------------------------------------------------------------------
C FIT CUBIC SPLINE AND INTERPOLATE GFTS FOR GRAPHICAL & TABULAR OUTPUT
C-----------------------------------------------------------------------
C
      CALL E6SPLN(         LOSEL ,
     &             NV    , MAXT  , NPSPL  ,
     &             SCEF  , TOA   , TOSA   ,
     &             GOFTA , GFTOA , GFTOSA ,
     &             LTRNG
     &           )
C
      CALL E6SPLN(         LOSEL ,
     &             NV    , MAXT  , NPSPL  ,
     &             SCEF  , TOA   , TOSA   ,
     &             DENSA , DENOA , DENOSA ,
     &             LTRNG
     &           )
C
      CALL E6SPLN(         LOSEL ,
     &             NV    , MAXT  , NPSPL  ,
     &             SCEF  , TOA   , TOSA   ,
     &             PRESA , PREOA , PREOSA ,
     &             LTRNG
     &           )
C
      CALL E6SPLN(         LOSEL ,
     &             NV    , MAXT  , NPSPL  ,
     &             SCEF  , TOA   , TOSA   ,
     &             RNHNE , RNHOA , RNHOSA ,
     &             LTRNG
     &           )
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED
C-----------------------------------------------------------------------
C
         IF (LFSEL) THEN
            CALL XXMNMX( LMLOG  , MAXDEG , TOLVAL ,
     &                   NV     , SCEF   , GOFTA  ,
     &                   NMX    , TOMA   , GFTOMA ,
     &                   KPLUS1 , COEF   ,
     &                   TITLM
     &                 )
            WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C  CONSTRUCT DESCRIPTIVE TITLE FOR OUTPUT
C-----------------------------------------------------------------------
C
CA UNIX PORT - MODIFY CONSTRUCTION OF TITLX BECAUSE OF VARIABLE FILENAME
CA              LENGTHS
CA
CX    WRITE (TITLX,1001) DSFULL(1:30),ELEM,'+',IZ,LLOWER,LUPPER,APWLO
      CALL XXSLEN(DSFULL, IFIRST, ILAST)
      WRITE (TITLX,1001) DSFULL(IFIRST:ILAST),ELEM,'+',IZ,
     &                    LLOWER,LUPPER,APWLO
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
C-----------------------------------------------------------------------
C
CA  UNIX PORT - 
CA ADDED THE NEW CALL TO B1SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CA A PIPE TO GET USER SELECTED OUTPUT OPTIONS. NOTE THAT IF USER SELECTS
CA GRAPH X-AXIS RANGES THESE ARE ASSUMED TO BE IN KELVIN SO NO FURTHER
CA CONVERSION IS CARRIED OUT.
C-----------------------------------------------------------------------
300   CONTINUE

      CALL E6SPF1( DSFULL        , ISTRN         , LOSEL    , LFSEL, 
     &             LDEF1,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             LCOLL         , FILC          ,
     &             XMINK         , XMAXK         , YMIN     , YMAX ,
     &             LPEND
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

CA IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
C OUTPUT RESULTS
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
         CALL E6OUT0( IUNT07   , IL    , LFSEL , LOSEL ,
     &             TITLE    , TITLX , TITLM , DATE  ,
     &             IZ0      , IZ1   , IZ    ,
     &             MAXT     , LTRNG ,
     &                        TOA   , GFTOA ,
     &                        DENOA , PREOA , RNHOA ,
     &             LUPPER   , LLOWER,
     &             IUPPER   , ILOWER,
     &             APWLO    , SWLO  ,
     &             CSTRGA   , ISA   , ILA   , XJA   ,
     &             KPLUS1   , COEF
     &           )
         CLOSE(UNIT=IUNT07)
      ENDIF
C
C  IF SELECTED - GENERATE GRAPH
CA UNIX PORT - E1OUTG COMMUNICATES WITH IDL VIA PIPE.
      IF (LGRAPH) THEN
         CALL E6OUTG( LGHOST   ,
     &                TITLE    , TITLX , TITLM , DATE ,
     &                SCEF     , GOFTA , NV    ,
     &                TOMA     , GFTOMA, NMX   ,
     &                TOSA     , GFTOSA, NPSPL ,
     &                LGRD1    , LDEF1 , LFSEL ,
     &                XMINK    , XMAXK , YMIN  , YMAX
     &              )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 9999

      ENDIF
C
C-----------------------------------------------------------------------
C  APPEND SELECTED G(TE) TO COLLECTION DATAFILE
C  CREATE FILE IF DOES NOT EXISTS
C-----------------------------------------------------------------------
      IF (LCOLL) THEN
         CALL E6COLL(IUNT20, FILC, DSFULL, ELEM, IZ,   
     &               ISTRN,  MAXT,   APWLO , SWLO, 
     &               TOA, GFTOA)
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO ISPF PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
CA  UNIX PORT - CONTROL RETURNS TO OUTPUT SELECTION SCREEN
CX    GOTO 200
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
CA UNIX PORT - MODIFY THE FORMAT STATEMENT FOR USE WITH UNIX
cx 1001 FORMAT('FILE: ',A30,' ELEM: ',A2,A1,I2,
cx     &       '  LINE: (',I2.2,'->',I2.2,') WVLN:',F8.1,'A')
 1001 FORMAT((1A),' ELEM: ',A2,A1,I2,
     &       '  LINE: (',I2.2,'->',I2.2,') WVLN:',F8.1,'A')

C
C-----------------------------------------------------------------------
C
 9999 END
