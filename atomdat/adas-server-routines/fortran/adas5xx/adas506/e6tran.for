CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6tran.for,v 1.1 2004/07/06 13:45:37 whitefor Exp $ Date $Date: 2004/07/06 13:45:37 $
CX
       SUBROUTINE E6TRAN( NDLEV  , NDTRN  , NDTEM ,
     &                    IL     , ISTRN  , NV    ,
     &                    IA     , XJA    ,
     &                    APWL   , SWL    ,
     &                    I1A    , I2A    ,
     &                    GFTA   ,
     &                    IUPPER , ILOWER ,
     &                    LUPPER , LLOWER ,
     &                    WUPPER , WLOWER ,
     &                    APWLO  , SWLO   ,
     &                    GOFTA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6TRAN *********************
C
C  PURPOSE:  TO SET UP SELECTED TRANSITION PARAMETERS.
C
C  CALLING PROGRAM: ADAS506
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF INDEX LEVELS
C  INPUT : (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF INPUT FILE TEMPERATURES
C
C  INPUT : (I*4)  IL      = NUMBER OF INDEX LEVELS
C  INPUT : (I*4)  ISTRN   = SELECTED TRANSITION INDEX.
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GFT/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C  INPUT : (I*4)  IA()    = LEVEL INDEX NUMBER ARRAY
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  INPUT : (R*8)  APWL()  = APPROXIMATE WAVELENGTHS OF TRANSITIONS (A)
C  INPUT : (R*8)  SWL()   = EXACT WAVELENGTHS OF TRANSITIONS (A)
C  INPUT : (I*4)  I1A()   = LOWER LEVEL INDEX FOR RADIATIVE
C                           TRANSITION
C  INPUT : (I*4)  I2A()   = UPPER LEVEL INDEX FOR RADAITIVE
C                           TRANSITION
C  INPUT : (I*4)  GFTA(,) = GFT VALUES FOR RADIATIVE TRANSITION
C                            1st DIMENSION: TEMPERATURE INDEX
C                            2nd DIMENSION: TRANSITION INDEX
C
C  OUTPUT: (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C  OUTPUT: (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C
C
C  OUTPUT: (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C  OUTPUT: (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C  OUTPUT: (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C  OUTPUT: (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C  OUTPUT: (R*8)  APWLO   = SELECTED APPROXIMATE WAVELENGTH (A)
C  OUTPUT: (R*8)  SWLO    = SELECTED EXACT WAVELENGTH (A)
C
C  OUTPUT: (R*8)  GOFTA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GFT VALUE AT 'TEMP()'
C
C          (I*4)  I       = GENERAL USE.
C
C ROUTINES: NONE
C
C AUTHOR:  H. P .SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    08/04/94
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER  NDLEV  , NDTRN  , NDTEM ,
     &         IL     , ISTRN  , NV    ,
     &         IUPPER , ILOWER ,
     &         LUPPER , LLOWER
      INTEGER  I
C-----------------------------------------------------------------------
      REAL*8   WUPPER , WLOWER ,
     &         APWLO  , SWLO
C-----------------------------------------------------------------------
      INTEGER  IA(NDLEV)   ,
     &         I1A(NDTRN)  , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8   XJA(NDLEV)   , APWL(NDTRN)       , SWL(NDTRN) ,
     &         GOFTA(NDTEM) , GFTA(NDTEM,NDTRN)
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UPPER AND LOWER ENERGY LEVEL INDEX SPECIFICATIONS
C-----------------------------------------------------------------------
C
      LLOWER=I1A(ISTRN)
      LUPPER=I2A(ISTRN)
         DO 1 I=1,IL
           IF ( IA(I).EQ.LLOWER ) ILOWER=I
           IF ( IA(I).EQ.LUPPER ) IUPPER=I
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP REQUIRED WAVELNGTHS AND GFT FUNCTIONS.
C-----------------------------------------------------------------------
C
         APWLO = APWL(ISTRN)
         SWLO  = SWL(ISTRN)
C
         DO 2 I=1,NV
            GOFTA(I) = GFTA(I,ISTRN)
    2    CONTINUE
C
C-----------------------------------------------------------------------
C ASSIGN INDEX LEVEL ENERGIES AND STATISTICAL WEIGHTS.
C-----------------------------------------------------------------------
C
      WLOWER=(2.0*XJA(ILOWER))+1.0
      WUPPER=(2.0*XJA(IUPPER))+1.0
C-----------------------------------------------------------------------
      RETURN
      END
