CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6setp.for,v 1.2 2004/07/06 13:45:12 whitefor Exp $ Date $Date: 2004/07/06 13:45:12 $
CX
       SUBROUTINE E6SETP( IZ0    , IZ     ,
     &                    NDLEV  , IL     , ITRAN ,
     &                    CSTRGA , ISA    , ILA   , XJA  ,
     &                    STRGA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6SETP *********************
C
C  PURPOSE:  TO SET UP PARAMETERS IN THE SHARED POOLED FOR PANEL DISPLAY
C
C  CALLING PROGRAM: ADAS506
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'E6DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ITRAN   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (C*18) STRGA() = LEVEL DESIGNATIONS
C
C          (C*8)  F6      = PARAMETER = 'VREPLACE'
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  ILVAL   = VALUE OF QUANTUM NUMBER L + 1
C
C          (C*2)  SZ0     =         NUCLEAR CHARGE READ
C          (C*2)  SZ      =  RECOMBINED ION CHARGE READ
C          (C*3)  SCNTE   =  NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (I*4)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*1)  CONFIG()= QUANTUM NUMBER (L) LETTERS
C                           DIMENSION: QUANTUM NUMBER L + 1
C          (C*8)  CHA()   = FUNCTION POOL NAMES: CHARGE VALUES
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           ISPLNK      ISPF        ISPF PANEL SERVICE ROUTINE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C MODIFIED: ALESSANDRO LANZAFAME (UNIVERSITY OF STRATHCLYDE)
C		- UNIX PORT
C
C DATE:	   26/06/95
C
C           september08-95  Alessandro Lanzafame
C                 modified format of CSTRGA and STRGA
C                 commented out superfluous variables
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IZ0           , IZ            , NDLEV    , IL  , ITRAN
CX    INTEGER   ILEN          
      INTEGER   ILEV          , ILVAL
      INTEGER   ISA(IL)       , ILA(IL)
C-----------------------------------------------------------------------
      REAL*8    XJA(IL)
C-----------------------------------------------------------------------
      CHARACTER F6*8
      CHARACTER SZ*2          , SZ0*2         , SCNTE*3  , SIL*3 ,
     &          CONFIG(20)*1
      CHARACTER CHA(4)*8
      CHARACTER CSTRGA(IL)*18 , STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      PARAMETER ( F6='VREPLACE' )
C-----------------------------------------------------------------------
      SAVE      CHA
C-----------------------------------------------------------------------
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
      DATA CHA   / '(SZ0)   ' , '(SZ)    ' , '(SCNTE) ' , '(SIL)   ' /
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      WRITE(SZ0,1000) IZ0
      WRITE(SZ ,1000) IZ
      WRITE(SCNTE,1001) ITRAN
      WRITE(SIL  ,1001) IL
         DO 1 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1002)
     &                   CSTRGA(ILEV)(1:12),
     &                   ISA(ILEV),CONFIG(ILVAL),XJA(ILEV)
               ENDIF
    1    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C SEND STRINGS TO ISPF FUNCTION POOL
C-----------------------------------------------------------------------
C
ca      ILEN=2
ca      CALL ISPLNK( F6 , CHA(1) , ILEN , SZ0   )
ca      ILEN=2
ca      CALL ISPLNK( F6 , CHA(2) , ILEN , SZ    )
ca      ILEN=3
ca      CALL ISPLNK( F6 , CHA(3) , ILEN , SCNTE )
ca      ILEN=3
ca      CALL ISPLNK( F6 , CHA(4) , ILEN , SIL   )
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(I3)
 1002 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
C
C-----------------------------------------------------------------------
C
      RETURN
      END
