CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6coll.for,v 1.5 2004/07/06 13:44:26 whitefor Exp $ Date $Date: 2004/07/06 13:44:26 $
CX
      SUBROUTINE E6COLL (IUNT20, FILC, DSFULL, ELEM, IZ, 
     &                   ISTRN, MAXT, APWLO , SWLO  , 
     &                   TOA, GFTOA)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6COLL *********************
C
C  PURPOSE:  TO STORE SELECTED GFT INTO A COLLECTION FILE.
C
C  CALLING PROGRAM: ADAS506
C
C  SUBROUTINE:
C
Cx  INPUT : (I*4)  MTRAN   =  MAX. NO. OF TRANSITIONS ALLOWED
Cx  INPUT : (I*4)  MTEMP   =  MAXIMUM OF INPUT DATA FILE TEMPS
C  INPUT : (I*4)  IUNIT20  =  UNIT NUMBER FOR COLLECTION FILE
C  INPUT : (C*80) FILC     =  COLLECTION FILENAME
C  INPUT : (C*80) DSFULL   =  INPUT DATA SET NAME 
C  INPUT : (C*2)  ELEM     =  ELEMENT SYMBOL.
C  INPUT : (I*4)  IZ       =  RECOMBINED ION CHARGE
C  INPUT : (I*4)  ISTRN    =  SELECTED TRANSITION INDEX
C  INPUT : (I*4)  MAXT     =  NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C  INPUT : (R*8)  APWLO    =  SELECTED APPROXIMATE WAVELENGTHS (A)
C  INPUT : (R*8)  SWLO     =  SELECTED EXACT WAVELENGTHS (A)
C  INPUT : (R*8)  TOA()    =  ISPF ENTERED TEMPERATURES (kelvin)
C  INPUT : (R*8)  GFTOA()  =  SPLINE INTEROPLATED GFT VALUE AT 'TOA()'
C
C          (I*4)  NTRAN    =  NUMBER OF G(T) FUNCTION IN FINAL COLL. FILE
C          (R*8)  TX(,)    =  STORED TEMPERATURE FROM OLD COLLECTION FILE
C          (R*8)  GFTX(,)  =  STORED G(T) FROM OLD COLLECTION FILE
C          (C*2)  ELEMC()  =  ELEMENT SYMBOL READ FROM COLL FILE
C          (I*4)  IZC()    =  RECOMBINED ION CHARGE READ FROM COLL FILE
C          (I*4)  ISTRNC() =  TRANSITION INDEX FROM COLL FILE
C          (I*4)  NG()     =  NUMBER OF DATA POINTS FOR G(T) IN COLL FILE
C          (L*4)  LFEXST   = .TRUE. COLLECTION FILE EXISTS
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          I4FCTN     ADAS      CONVERTS CHARACTER STRING TO INTEGER
C          I4EIZ0     ADAS      RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C
C AUTHOR  : Alessandro Lanzafame, University of Strathclyde
C
C DATE:    june06-95
C
C UPDATE:
C VERSION:	1.2		DATE:	09-11-95
C MODIFIED: Alessandro Lanzafame
C           - MTRAN: 100 -> 300
C             MTEMP: 100 -> 101
C
C VERSION       1.3             DATE:   31-05-96
C MODIFIED: Alessandro Lanzafame  
C           - MTRAN: 300 -> 800
C
C VERSION       1.4             DATE:   13-10-99
C MODIFIED: Martin O'Mullane  
C           - Replace format statements 1004 and 1005. Not all
C             compilers can use dynamically created format statements.
C
C VERSION       1.5             DATE:   08-07-02
C MODIFIED: Richard Martin 
C           - Fixed format statements introduced in 1.4 to work on SUN's
C
C-----------------------------------------------------------------------
      INTEGER  MTRAN, MTEMP, I, J
      PARAMETER (MTRAN=800, MTEMP=101)
C-----------------------------------------------------------------------
      INTEGER IUNT20, IZ, IZC(MTRAN), ISTRN, ISTRNC(MTRAN), MAXT,
     &        NG(MTRAN),  NTRAN
      INTEGER   I4UNIT, I4FCTN, IABT
C-----------------------------------------------------------------------
      REAL*8  TX(MTEMP,MTRAN), GFTX(MTEMP,MTRAN), AWL(MTRAN), SWL(MTRAN)
      REAL*8  APWLO, SWLO, TOA(*), GFTOA(*)
      REAL*8  R8FCTN
C-----------------------------------------------------------------------
      CHARACTER*80 CLINE
      CHARACTER*80 FILC,  DSFULL, FILEI(MTRAN)
      CHARACTER ELEM*2 , ELEMC(MTRAN)*2 
      CHARACTER CSLASH*1    , IONNAM*5   , C4*4     ,  C7*7   ,
     &          CKEY1*6     , CKEY2*5    , CKEY3*6  ,  CKEY4*5
      CHARACTER fmt_1001*15 , fmt_1002*18 , fmt_1004*15 , fmt_1005*18
C-----------------------------------------------------------------------
      LOGICAL   LFEXST, LGEXST
C-----------------------------------------------------------------------
      DATA      CSLASH / '/' /
      DATA      CKEY1  / 'ITRANS' /   , CKEY2  / 'NTEMP' / ,
     &          CKEY3  / 'APPWAV'  / , CKEY4 /'SPWAV'/
C------------------------------------------------------------------------
      LGEXST=.FALSE.
C------------------------------------------------------------------------
C  setup temperature format statements
C------------------------------------------------------------------------
      write(fmt_1004,1004)MAXT
      write(fmt_1005,1005)MAXT
C------------------------------------------------------------------------
C  Inquire if collection data file exists
C------------------------------------------------------------------------
      LFEXST=.FALSE.
      INQUIRE(FILE=FILC,EXIST=LFEXST)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C  If collection data file exists, store data in local arrays
C  TX and GFTX. If transition already exists in data file substitute
C  old G(T) with selected one
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
      IF (LFEXST) THEN
         OPEN(UNIT=IUNT20, FILE=FILC, STATUS='OLD')
         NTRAN=0
         DO I=1,MTRAN
C-----------------------------------------------------------------------
C  Read line identifying the element symbol, ion charge, number
C  of data points, approx wavelength and spectrscopic wavelength
C-----------------------------------------------------------------------
            READ(IUNT20,'(A80)',END=998) CLINE
            READ(CLINE(1:5),'(1A5)') IONNAM
            IF (IONNAM(1:2).EQ.'XX') GOTO 999
            IF (IONNAM(2:2).EQ.'+') THEN
               ELEMC(I)=IONNAM(1:1) // ' '
            ELSE
               ELEMC(I)=IONNAM(1:2)
            ENDIF
            IZC(I)  = I4FCTN(IONNAM(3:5),IABT)
            IF (IABT .NE.0) IZC(I) = I4FCTN(IONNAM(3:4),IABT)
            CALL XXHKEY(CLINE,CKEY1,CSLASH,C4)
            ISTRNC(I)=I4FCTN(C4,IABT)
            CALL XXHKEY(CLINE,CKEY2,CSLASH,C4)
            NG(I)=I4FCTN(C4,IABT)
C setup format statements
            write(fmt_1001,1001)NG(I)
            write(fmt_1002,1002)NG(I)

            IF (NG(I) .GT. MTEMP) THEN
               WRITE(I4UNIT(-1),*) 'E6COLL ERROR:',
     &                      'MAX DIMENSION EXCEEDED'
               WRITE(I4UNIT(-1),*) 'MTEMP =',MTEMP,
     &                             'NG(',I,') =',NG(I)
               STOP
            ENDIF
            CALL XXHKEY(CLINE,CKEY3,CSLASH,C7)
            AWL(I)=R8FCTN(C7,IABT)
            CALL XXHKEY(CLINE,CKEY4,CSLASH,C7)
            SWL(I)=R8FCTN(C7,IABT)
C-----------------------------------------------------------------------
C  Read input data set name
C-----------------------------------------------------------------------
            READ(IUNT20,'(A80)',END=998)FILEI(I)
C-----------------------------------------------------------------------
C  Store G(T) read from collection file
C-----------------------------------------------------------------------
            READ(IUNT20,fmt_1001) (TX(J,I), J=1,NG(I))
            DO J=1,NG(I)
               TX(J,I)=10**TX(J,I)
            ENDDO
            READ(IUNT20,fmt_1002) (GFTX(J,I), J=1,NG(I))
C-----------------------------------------------------------------------
C  If G(T) corresponds to selected one substitute
C-----------------------------------------------------------------------
            IF( (ELEMC(I)  .EQ. ELEM) .AND.
     &          (IZC(I)    .EQ. IZ)   .AND.
     &          (ISTRNC(I) .EQ. ISTRN))   THEN
               LGEXST=.TRUE.
               AWL(I)=APWLO
               SWL(I)=SWLO
               NG(I)=MAXT
               FILEI(I)=DSFULL
               DO J=1,NG(I)
                  TX(J,I)=TOA(J)
                  GFTX(J,I)=GFTOA(J)
               ENDDO
            ENDIF
            NTRAN=NTRAN+1
         ENDDO
 998     CONTINUE
c         IF (CLINE .NE. '') THEN
c            WRITE(I4UNIT(-1),1000) 
c         ENDIF
 999     CONTINUE
         CLOSE(UNIT=IUNT20)
C-----------------------------------------------------------------------
C  Overwrite collection file
C-----------------------------------------------------------------------
         OPEN(UNIT=IUNT20, FILE=FILC, STATUS='UNKNOWN')
         DO I=1,NTRAN
            WRITE(IUNT20,1003) ELEMC(I),IZC(I),ISTRNC(I),
     &                         NG(I),AWL(I),SWL(I)
            WRITE(IUNT20,'(A80)')FILEI(I)
            WRITE(IUNT20,fmt_1001) (DLOG10(TX(J,I)),J=1,NG(I))
            WRITE(IUNT20,fmt_1002) (GFTX(J,I),J=1,NG(I))
         ENDDO
         IF (.NOT. LGEXST) THEN
            WRITE(IUNT20,1003) ELEM,IZ,ISTRN,MAXT,APWLO,SWLO
            WRITE(IUNT20,'(A80)')DSFULL
            WRITE(IUNT20,fmt_1004) (DLOG10(TOA(I)),I=1,MAXT)
            WRITE(IUNT20,fmt_1005) (GFTOA(I),I=1,MAXT)
         ENDIF
         WRITE(IUNT20,'(A2)')'XX'
         CLOSE(UNIT=IUNT20)
      ELSE
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C  If collection data file does not exist, create it and write the
C  selected transition in it
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
         OPEN(UNIT=IUNT20, FILE=FILC, STATUS='UNKNOWN')
         WRITE(IUNT20,1003) ELEM,IZ,ISTRN,MAXT,APWLO,SWLO
         WRITE(IUNT20,'(A80)')DSFULL
         WRITE(IUNT20,fmt_1004) (DLOG10(TOA(I)),I=1,MAXT)
         WRITE(IUNT20,fmt_1005) (GFTOA(I),I=1,MAXT)
         WRITE(IUNT20,'(A2)')'XX'
         CLOSE(UNIT=IUNT20)
      ENDIF

 1000 FORMAT('************* E6COLL WARNING ****************',/,
     &       'END OF FILE REACHED BEFORE END OF TRANSITIONS',/,
     &       '            INCREASE MTRAN                   ',/,
     &       '*********************************************')
C 1001 FORMAT(<NG(I)>(1X,F4.2))
C 1002 FORMAT(<NG(I)>(1X,1PD10.4))
 1003 FORMAT(1A2,'+',I2,2X,'/ITRANS=',I4,'/NTEMP=',I4,
     &       '/APPWAV=',F12.3,'/SPWAV=',F12.3,'/')
C 1004 FORMAT(<MAXT>(1X,F4.2))
C 1005 FORMAT(<MAXT>(1X,1PD10.4))

 1001 FORMAT('(',i4,'(1X,F4.2)',')')
 1002 FORMAT('(',i4,'(1X,1PD10.4)',')')
 1004 FORMAT('(',i4,'(1X,F4.2)',')')
 1005 FORMAT('(',i4,'(1X,1PD10.4)',')')

      RETURN
      END


