CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6outg.for,v 1.2 2004/07/06 13:45:00 whitefor Exp $ Date $Date: 2004/07/06 13:45:00 $
CX
       SUBROUTINE E6OUTG( LGHOST ,
     &                    TITLE  , TITLX , TITLM , DATE ,
     &                    TEMP   , GOFTA , NENER ,
     &                    TOMA   , GFTOMA, NMX  ,
     &                    TOSA   , GFTOSA, NPSPL ,
     &                    LGRD1  , LDEF1 , LFSEL ,
     &                    XMIN   , XMAX  , YMIN  , YMAX
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED TRANSITION USING GHOST80.
C            (IDENTICAL TO: C1OUTG - EXCEPT SOME VARIABLES HAVE BEEN
C             RENAMED AS HAVE THE MAIN TITLE 'ISPEC(1:48)' AND AXES.
C             - ALSO ARGUMENT 'TITLF(C*80)' REMOVED.)
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                ORIGINAL <SE>LIKE.DATA    (CROSSES    )
C                                SPLINE INTERPOLATED DATA  (FULL CURVE)
C                                MINIMAX FIT TO DATA       (DASH CURVE )
C
C            PLOT IS LOG10(GFT(CM**3/SEC))  VERSUS LOG10(TEMP(KELVIN))
C
C  CALLING PROGRAM: ADAS506
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  TEMP()  = INPUT DATA FILE: TEMPERATURES (kelvin)
C  INPUT : (R*8)  GOFTA() = INPUT DATA FILE: SELECTED TRANSITION -
C                           GFT FUNCTION (CM**3/S) AT 'TEMP()'
C  INPUT : (I*4)  NENER   = INPUT DATA FILE: NUMBER OF GFT/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C  INPUT : (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (kelvin)
C  INPUT : (R*8)  GFTOMA()= GFT FUNCTIONS (CM**3/S) AT 'TOMA()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX  GENERATED   GFT/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (kelvin)
C  INPUT : (R*8)  GFTOSA()= GFT FUNCTIONS (CM**3/S) AT 'TOSA()'
C  INPUT : (I*4)  NPSPL   = NUMBER  OF SPLINE INTERPOLATED GFT/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (K)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (K)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR GFT FUNCT. (CM**3/S)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR GFT FUNCT. (CM**3/S)
C
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80
C                           TAKES NUMBERS AS BEING ZERO = 1.0E-36
C          (R*4)  YDMIN   = PARAMETER = MINIMUM ALLOWED Y-VALUE FOR
C                           PLOTTING. (USED FOR DEFAULT GRAPH SCALING)
C                           (SET TO 'GHZERO'/ZERO TO REMOVE)
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C          (I*4)  IKEY    = NUMBER OF 'KEY()' VALUES TO BE OUTPUT
C          (I*4)  ICOUNT  = NUMBER OF POINTS PLOTTED FOR GRAPH CURVE
C
C          (R*4)  XHIGH   = UPPER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XLOW    = LOWER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YHIGH   = UPPER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YLOW    = LOWER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XA4()   = X-AXIS CO0ORDINATES FOR USE WITH GHOST80
C          (R*4)  YA4()   = Y-AXIS CO0ORDINATES FOR USE WITH GHOST80
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*22) KEY()   = DESCRIPTIVE KEY FOR GRAPH (3 TYPES)
C
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*7)  C7      =  7 BYTE STRING = 'TITLX(1:4)'//'C3BLNK'
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXLIM8      ADAS      SETTING UP OF DEFAULT GRAPH AXES
C          XXGSEL      ADAS      SELECTS POINTS WHICH WILL FIT ON GRAPH
C          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C                      GHOST80   NUMEROUS SUBROUTINES
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    08/04/94
C
C
C VERSION:      1.2             DATE:   09-11-95
C MODIFIED: Alessandro Lanzafame
C           - Commented out superfluous variables
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
       PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
CX     INTEGER I4UNIT
cx       INTEGER NENER      , NMX         , NPSPL
cx       INTEGER I          , IKEY        , ICOUNT
       INTEGER NENER      , NMX         , NPSPL    ,
     &                      I           , 
     &			    IFIRST      , ILAST    ,
     &                      PIPEOU      , ONE      ,  ZERO
	PARAMETER (PIPEOU = 6, ONE = 1, ZERO = 0)
C-----------------------------------------------------------------------
       REAL*8 TEMP(NENER) , GOFTA(NENER) ,
     &        TOMA(NMX)   , GFTOMA(NMX) ,
     &        TOSA(NPSPL) , GFTOSA(NPSPL) ,
     &        XMIN        , XMAX        ,
     &        YMIN        , YMAX
C-----------------------------------------------------------------------
CX     REAL*4 XHIGH       , XLOW        ,
CX   &        YHIGH       , YLOW
CX     REAL*4 XA4(100)    , YA4(100)
C-----------------------------------------------------------------------
       CHARACTER TITLE*40 , TITLX*120    , TITLM*80  , DATE*8
       CHARACTER CADAS*80 ,
     &           ISPEC*88 , DNAME*12    , XTIT*23   , YTIT*23   ,
     &           MNMX0*9  , KEY0*9      , ADAS0*8   , KEY(3)*22
       CHARACTER GRID*1   , PIC*1       , C3BLNK*3   
CX     CHARACTER C7*7
C-----------------------------------------------------------------------
       LOGICAL   LGHOST   , LGRD1       , LDEF1     , LFSEL
C-----------------------------------------------------------------------
       SAVE      ISPEC    , CADAS
C-----------------------------------------------------------------------
       DATA ISPEC(1:48)
     &  /'G(TE) CONTRIBUTION FUNCTION VERSUS TEMPERATURE: '/
       DATA DNAME/'      DATE: '/
       DATA XTIT/' TEMPERATURE (KELVIN)  '/
       DATA YTIT/'GFT FUNCT.  (CM**3/SEC)'/
       DATA ADAS0/'ADAS   :'/
     &      MNMX0/'MINIMAX: '/,
     &      KEY0 /'KEY    : '/,
     &      KEY(1)/'(CROSSES - INPUT DATA)'/,
     &      KEY(2)/' (FULL LINE - SPLINE) '/,
     &      KEY(3)/'(DASH LINE - MINIMAX) '/
       DATA GRID  /' '/   ,
     &      PIC   /' '/   ,
     &      C3BLNK/'   '/ ,
     &      CADAS /' '/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
cx    ISPEC(49:88)=TITLE
cx    CALL XXADAS( CADAS )
C
C-----------------------------------------------------------------------
C INITIALISE GHOST80 IF NOT ALREADY DONE SO
C-----------------------------------------------------------------------
C
cx       IF (.NOT.LGHOST) THEN
cx          CALL PAPER(1)
cx          CALL FILNAM(GRID)
cx          CALL GPSTOP(0)
cx          LGHOST=.TRUE.
cx       ENDIF
cx       IF (LGRD1) THEN
cx          CALL FILON
cx       ELSE
cx          CALL FILOFF
cx       ENDIF
C
C-----------------------------------------------------------------------
C SET UP PLOTTING PARAMETERS
C-----------------------------------------------------------------------
C
cx       IF (LDEF1)THEN
cx          CALL XXLIM8( XLOW    , XHIGH  , TEMP , NENER , GHZERO )
cx          CALL XXLIM8( YLOW    , YHIGH  , GOFTA, NENER , YDMIN  )
cx       ELSE
cx          XLOW=REAL(XMIN)
cx          XHIGH=REAL(XMAX)
cx          YLOW=REAL(YMIN)
cx          YHIGH=REAL(YMAX)
cx       ENDIF
cx       IF (YLOW.GE.YHIGH) THEN
cx          WRITE(I4UNIT(-1),1000) YLOW,YHIGH
cx          RETURN
cx       ELSEIF (XLOW.GE.XHIGH) THEN
cx          WRITE(I4UNIT(-1),1001) XLOW,XHIGH
cx          RETURN
cx       ENDIF
C-----------------------------------------------------------------------
cx    CALL CSPACE(0.0,0.0,0.0,0.0)
cx    CALL PSPACE(0.1,0.9,0.1,0.9)
cx    CALL MAPXYL(XLOW,XHIGH,YLOW,YHIGH)
cx    CALL SCAXYL
cx    CALL GRAXYL
cx    CALL BORDER
C
C-----------------------------------------------------------------------
C  PLOT ORIGINAL GFT<YR>#<SE> VALUES
C-----------------------------------------------------------------------
C
cx    CALL XXGSEL(          XLOW   , YLOW  ,
cx   &                      XHIGH  , YHIGH ,
cx   &             NENER  , TEMP   , GOFTA ,
cx   &             ICOUNT , XA4    , YA4
cx   &           )
cx    IF (ICOUNT.GT.0) CALL PTPLOT(XA4,YA4,1,ICOUNT,162)
C
C-----------------------------------------------------------------------
C  PLOT MINIMAX CURVE (IF REQUESTED)
C-----------------------------------------------------------------------
C
cx       IF (LFSEL) THEN
cx          CALL XXGSEL(          XLOW   , YLOW  ,
cx   &                            XHIGH  , YHIGH ,
cx   &                   NMX    , TOMA   , GFTOMA ,
cx   &                   ICOUNT , XA4    , YA4
cx   &                 )
cx          CALL BROKEN(5,5,5,5)
cx          IF (ICOUNT.GT.0) CALL PTJOIN(XA4,YA4,1,ICOUNT,0)
cx       ENDIF
C
C-----------------------------------------------------------------------
C  PLOT SPLINE INTERPOLATION CURVE
C-----------------------------------------------------------------------
C
cx    CALL XXGSEL(          XLOW   , YLOW  ,
cx   &                      XHIGH  , YHIGH ,
cx   &             NPSPL  , TOSA   , GFTOSA ,
cx   &             ICOUNT , XA4    , YA4
cx   &           )
cx    CALL FULL
cx    IF (ICOUNT.GT.0) CALL PTJOIN(XA4,YA4,1,ICOUNT,0)
C
C-----------------------------------------------------------------------
C  PLOT INFORMATION DATA
C-----------------------------------------------------------------------
C
cx    CALL MAP(0.0,1.0,0.0,1.0)
cx    CALL PSPACE(0.0,1.0,0.0,1.0)
cx    CALL CTRMAG(10)
C-----------------------------------------------------------------------
cx    CALL CTRORI(90.0)
cx    CALL PCSCEN(0.02,0.5,YTIT,23)
cx    CALL CTRORI(0.0)
C-----------------------------------------------------------------------
cx    CALL PCSCEN(0.5,0.02,XTIT,23)
C-----------------------------------------------------------------------
cx    CALL UNDLIN(1)
cx    CALL PLOTCS(0.04,0.99,ISPEC,88)
cx    CALL UNDLIN(0)
cx    CALL TYPECS(DNAME,12)
cx    CALL TYPECS(DATE , 8)
C-----------------------------------------------------------------------
cx    CALL PLOTCS(0.15,0.965,ADAS0,8)
cx    CALL TYPECS(CADAS,80)
C-----------------------------------------------------------------------
cx    C7=TITLX(1:4)//C3BLNK
cx    CALL PLOTCS(0.15,0.95,C7,7)
cx    CALL TYPECS(TITLX(5:80),76)
C-----------------------------------------------------------------------
cx    IKEY=2
cx       IF (LFSEL) THEN
cx          IKEY=3
cx          CALL PLOTCS(0.15,0.935,MNMX0,9)
cx          CALL TYPECS(TITLM,80)
cx       ENDIF
C-----------------------------------------------------------------------
cx    CALL PLOTCS(0.15,0.92,KEY0,9)
C-----------------------------------------------------------------------
cx       DO 1 I=1,IKEY
cx          CALL TYPECS(KEY(I),22)
cx  1    CONTINUE
C-----------------------------------------------------------------------
cx    CALL FRAME
cx    IF (LGRD1) CALL PICSAV(GRID,PIC)
C
C-----------------------------------------------------------------------
C
c 1000 FORMAT(1X,79('*')/1X,'E6OUTG ERROR: GRAPH SCALING - XMIN >= XMAX'/
c     &       15X,'XMIN = ',1PE12.5,'   XMAX = ',1PE12.5/1X,79('*'))
c 1001 FORMAT(1X,79('*')/1X,'E6OUTG ERROR: GRAPH SCALING - YMIN >= YMAX'/
c     &       15X,'YMIN = ',1PE12.5,'   YMAX = ',1PE12.5/1X,79('*'))
C
C-----------------------------------------------------------------------
C
C
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C WRITE OUT TITLES AND DATE INFORMATION
      CALL XXSLEN(TITLX, IFIRST, ILAST)
      WRITE(PIPEOU,'(A120)') TITLX(IFIRST:ILAST)
      WRITE(PIPEOU,'(A80)') TITLM
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
C
C WRITE OUT ARRAY SIZES
      WRITE(PIPEOU,*) NENER
      WRITE(PIPEOU,*) NPSPL
      CALL XXFLSH(PIPEOU)
C
C DATA FROM DATA FILE
      DO 1 I = 1, NENER 
         WRITE(PIPEOU,*) TEMP(I)
 1    CONTINUE
      CALL XXFLSH(PIPEOU)
      DO 2 I = 1, NENER
         WRITE(PIPEOU,*) gofta(I)
 2    CONTINUE
      CALL XXFLSH(PIPEOU)
C
C WRITE OUT INTERPOLATED SPLINE VALUES
	 WRITE(PIPEOU,*) ONE
         DO 3 I = 1, NPSPL
            WRITE(PIPEOU,*) tosa(I)
 3       CONTINUE
         CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NPSPL
            WRITE(PIPEOU,*) gftosa(I)
 4       CONTINUE
         CALL XXFLSH(PIPEOU)
C
C WRITE OUT USER SELECTED AXES RANGES.
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
      ELSE
         WRITE(PIPEOU,*) ONE
	 WRITE(PIPEOU,*) XMIN
	 WRITE(PIPEOU,*) XMAX
	 WRITE(PIPEOU,*) YMIN
	 WRITE(PIPEOU,*) YMAX
      ENDIF
      CALL XXFLSH(PIPEOU)
C
C IF SELECTED WRITE OUT MINMAX FIT VALUES
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 5 I = 1, NMX
	    WRITE(PIPEOU,*) TOMA(I)
 5       CONTINUE
	 CALL XXFLSH(PIPEOU)
         DO 6 I = 1, NMX
	    WRITE(PIPEOU,*) gftoma(I)
 6       CONTINUE
	 CALL XXFLSH(PIPEOU)
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
      RETURN
      END
