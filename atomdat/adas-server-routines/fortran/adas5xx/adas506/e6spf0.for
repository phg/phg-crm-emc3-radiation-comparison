CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6spf0.for,v 1.1 2004/07/06 13:45:14 whitefor Exp $ Date $Date: 2004/07/06 13:45:14 $
CX
c#
c# UNIX PORT: Alessandro Lanzafame    e6spf0    
c#
      SUBROUTINE E6SPF0( REP    , DSFULL , LDSEL )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6SPF0 *********************
C
C  PURPOSE: pipe communication with IDL
C           (after e3spf0) 
C
C  CALLING PROGRAM: ADAS506
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C
C  OUTPUT: (L*4)   LDSEL   = .TRUE.  => COPASE DATA SET INFORMATION
C                                       TO BE DISPLAYED BEFORE RUN.
C                          = .FALSE. => COPASE DATA SET INFORMATION
C                                       NOT TO BE DISPLAYED BEFORE RUN.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Alessandro Lanzafame University of Strathclyde
C
C DATE:    may08-95
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSFULL*80
C-----------------------------------------------------------------------
      LOGICAL      LDSEL
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END




