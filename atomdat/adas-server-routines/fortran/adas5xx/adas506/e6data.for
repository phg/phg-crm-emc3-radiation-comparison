CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6data.for,v 1.3 2004/07/06 13:44:35 whitefor Exp $ Date $Date: 2004/07/06 13:44:35 $
CX
       SUBROUTINE E6DATA( IUNIT  , NDLEV  , NDTEM , NDTRN ,
     &                    ELEM   , IZ     , IZ0   , IZ1   ,
     &                    IL     ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA    ,
     &                    NV     ,
     &                    TEA    , DENSA  , PRESA , RNHNE , TMA    ,
     &                    ITRAN  ,
     &                    I1A    , I2A    , APWL  , SWL   , GFT    ,
     &                    LVALID , INDX
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6DATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT GFT DATA SET OF TYPE ADF20.
C
C  CALLING PROGRAM: ADAS506
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           ELECTRON TEMPERATURE: KELVIN
C           ELECTRON DENSITY    : CM-3
C           ELECTRON PRESSURE   : K CM-3
C           TIME                : NOT SPECIFIED
C           WAVELENGTH          : ANGSTROM
C           GFT COEFFT.         : CM3 SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C
C  OUTPUT: (C*2)  ELEM    = ELEMENT SYMBOL.
C  OUTPUT: (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  OUTPUT: (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C
C  OUTPUT: (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  OUTPUT: (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (I*4)  NV      = INPUT DATA FILE: NUMBER OF TEMP/DENS/PRESS/
C                           TIME SETS
C  OUTPUT: (R*8)  TEA()   = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C  OUTPUT: (R*8)  DENSA() = INPUT DATA FILE: ELECTRON DENSITIES (CM-3)
C  OUTPUT: (R*8)  PRESA() = INPUT DATA FILE: ELECTRON PRESSURES (K CM-3)
C  OUTPUT: (R*8)  TMA()   = INPUT DATA FILE: TIMES OR ARBITRARY (S ?)
C
C  OUTPUT: (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C
C  OUTPUT: (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C  OUTPUT: (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C  OUTPUT: (R*8)  APWL()  = APPROXIMATE TRANSITION WAVELENGTH (A)
C  OUTPUT: (R*8)  SWL()   = EXACT TRANSITION WAVELENGTH (A)  (IF SET)
C  OUTPUT: (R*8)  GFT(,)  = GFT COEFFICIENT FOR TRANSITION (CM3 S-1)
C                           1ST DIMENSION - TEMPERATURE 'TEA()'
C                           2ND DIMENSION - TRANSITION INDEX
C  OUTPUT: (L*4)  LVALID  = .TRUE. DATA SET READ AND APPEARS VALID
C                         = .FALSE. ERROR DETECTED IN READING DATA SET
C#
C# OUTPUT: (I*4)  INDX()  = TRANSITION INDEX (USED IN DEM CODES)
C
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I4FCTN  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I4EIZ0  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE.
C          (I*4)  IABT    = RETURN CODE FROM 'I4FCTN'
C          (I*4)  J       = GENERAL USE.
C          (I*4)  K       = GENERAL USE.
C          (I*4)  NVAL    = GENERAL USE
C          (I*4)  IRECL   = RECORD LENGTH OF INPUT DATASET (<=128)
C
C
C          (C*1)  CSLASH  = '/' - DELIMITER FOR 'XXHKEY'
C          (C*4)  C4      = GENERAL USE FOUR BYTE CHARACTER
C          (C*5)  IONNAM  = EMITTING ION READ FROM DATA SET
C          (C*7)  CKEY1   = 'NLEVELS' - INPUT HEADER KEY
C          (C*6)  CKEY2   = 'NKNOTS'  - INPUT HEADER KEY
C          (C*6)  CKEY3   = 'NLINES'  - INPUT HEADER KEY
C          (C*3)  TITLED  = ELEMENT SYMBOL INCLUDING '+'
C          (C*80) CLINE   = CURRENT ENERGY LEVEL INDEX PARAMETER LINE
C          (C*127)BUFFER  = GENERAL STRING BUFFER STORAGE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          I4FCTN     ADAS      CONVERTS CHARACTER STRING TO INTEGER
C          I4EIZ0     ADAS      RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C
C
C ROUTINES: NONE
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    07/04/94
C
C UPDATE:  APR18-95
C#         A. C. LANZAFAME, DPAP UNIVERSITY OF STRATHCLYDE
C#         TRANSITION INDEX (INDX) ADDED. USED IN DEM CODES
C#         TO IDENTIFY THE TRANSITION
C#
C#         CHARACTER CSTRGA(NDLEV)*(*) changed to CHARACTER*18 CSTRGA(NDLEV)
C#         after experinced unstable behaviour on Sun workstation
C
C UPDATE:
C VERSION:      1.2             DATE:   09-11-95
C MODIFIED: Alessandro Lanzafame
C           - Commented out superfluous variables
C
C
C VERSION: 1.3                          DATE: 06-06-2003
C MODIFIED: Martin O'Mullane
C                   - Warn user that the routine is now deprecated
C                     and that xxdata_20 should be used instead.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   I4UNIT      , I4FCTN         , I4EIZ0     , IABT
      INTEGER   IUNIT       , NDLEV          , NDTEM      , NDTRN      ,
     &          IZ          , IZ0            , IZ1        ,
     &          IL          , NV             , ITRAN      , NVAL
CX    INTEGER   IRECL
      INTEGER   I           , J              , K
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &          I1A(NDTRN)  , I2A(NDTRN)
C#
      INTEGER   INDX(NDTRN)
C#
C-----------------------------------------------------------------------
CX    REAL*8    R8FCTN      
      REAL*8    TEAL
      REAL*8    TEA(NDTEM)  , DENSA(NDTEM)  , PRESA(NDTEM) ,
     &          RNHNE(NDTEM), TMA(NDTEM)
      REAL*8    XJA(NDLEV)
      REAL*8    APWL(NDTRN) , SWL(NDTRN)    ,
     &          GFT(NDTEM,NDTRN)
C-----------------------------------------------------------------------
      LOGICAL   LVALID
C-----------------------------------------------------------------------
      CHARACTER ELEM*2
      CHARACTER*18 CSTRGA(NDLEV)
CX    CHARACTER TITLED*3    
      CHARACTER CLINE*80    , BUFFER*127
      CHARACTER CSLASH*1    , IONNAM*5     , C4*4      ,
     &          CKEY1*7     , CKEY2*6      , CKEY3*6
C-----------------------------------------------------------------------
      SAVE      CSLASH      ,
     &          CKEY1       , CKEY2       , CKEY3
C-----------------------------------------------------------------------
      DATA      CSLASH / '/' /
      DATA      CKEY1  / 'NLEVELS' /   , CKEY2  / 'NKNOTS' / ,
     &          CKEY3  / 'NLINES'  /
C-----------------------------------------------------------------------
      LVALID = .FALSE.
C-----------------------------------------------------------------------
C
C IDENTIFY THE RECORD LENGTH OF THE INPUT DATASET
C (MAXIMUM CURRENTLY ACTIVE USAGE IS 127 BYTES)
C
C-----------------------------------------------------------------------
c      INQUIRE(IUNIT,RECL=IRECL)
c      IRECL = MIN0( IRECL , 127 )
C
C
C***********************************************************************

C-----------------------------------------------------------------------
C Warn user that this routine is deprecated but continue.
C-----------------------------------------------------------------------

      WRITE(I4UNIT(-1),3000)

C***********************************************************************
C INPUT ION SPECIFICATIONS.
C***********************************************************************
C
      READ(IUNIT,'(A80)') CLINE
      READ(CLINE(1:5),'(1A5)') IONNAM
C
         IF ( IONNAM(2:2).EQ.'+' ) THEN
            ELEM  = IONNAM(1:1) // ' '
         ELSE
            ELEM  = IONNAM(1:2)
         ENDIF
C
      IZ0   = I4EIZ0( ELEM )
      IZ    = I4FCTN( IONNAM(3:5) , IABT )
      IF (IABT.NE.0) IZ = I4FCTN( IONNAM(3:4) , IABT )
C
      IZ1   = IZ + 1
C
      CALL XXHKEY( CLINE , CKEY1 , CSLASH , C4  )
      IL = I4FCTN( C4 , IABT )
      IF(IL.GT.NDLEV) THEN
         WRITE(I4UNIT(-1),1001) 'NLEVELS =',IL,' .GT.NDLEV'
         WRITE(I4UNIT(-1),1002)
         RETURN
      ENDIF
C
      CALL XXHKEY( CLINE , CKEY2 , CSLASH , C4  )
      NV = I4FCTN( C4 , IABT )
      IF(NV.GT.NDTEM) THEN
         WRITE(I4UNIT(-1),1001) 'NKNOTS =',NV,' .GT. NDTEM'
         WRITE(I4UNIT(-1),1002)
         RETURN
      ENDIF
C
      CALL XXHKEY( CLINE , CKEY3 , CSLASH , C4  )
      ITRAN = I4FCTN( C4 , IABT )
      IF(ITRAN.GT.NDTRN) THEN
         WRITE(I4UNIT(-1),1001) 'NLINES =',ITRAN,' .GT.NDTRN'
         WRITE(I4UNIT(-1),1002)
         RETURN
      ENDIF
C
C***********************************************************************
C PASS SOURCE FILE NAMES AND OTHER INFORMATION
C***********************************************************************
C
        DO 20 I=1,15
         READ(IUNIT,*)
   20   CONTINUE
C
C***********************************************************************
C READ IN ENERGY LEVEL SPECIFICATIONS
C***********************************************************************
C
         DO 30 I=1,IL
          READ(IUNIT,'(A80)')CLINE
          READ(CLINE,1003,ERR=998)IA(I), CSTRGA(I),ISA(I),ILA(I),XJA(I)
   30   CONTINUE
C
C***********************************************************************
C READ IN TEMPS,DENSITIES ETC. OF PLASMA MODEL
C***********************************************************************
C
        DO 40 I=1,5
         READ(IUNIT,*)
   40   CONTINUE
C
         DO 50 I=1,NV
          READ(IUNIT,'(A80)')CLINE
          READ(CLINE,1004,ERR=998)TEA(I),DENSA(I),PRESA(I),RNHNE(I),
     &                            TMA(I)
   50   CONTINUE
C
C***********************************************************************
C READ IN WAVELENGTHS, TRANSITION INDICES AND GFT TABLES FOR TRANSITIONS
C***********************************************************************
C
        DO 60 I=1,2
         READ(IUNIT,*)
   60   CONTINUE
C
         DO 80 I=1,ITRAN,10
          READ(IUNIT,*)
          NVAL = MIN0(ITRAN,I+9)
C#
C# APR18-95 MODIFICATION: ALESSANDRO LANZAFAME
C#          INDX FOR TRANSITION ADDED
C#
          READ(IUNIT,'(A127)')BUFFER(1:127)
          READ(BUFFER,2001,ERR=999)(INDX(J),J=I,NVAL)
C#
          READ(IUNIT,'(A127)')BUFFER(1:127)
          READ(BUFFER,1005,ERR=999)(APWL(J),J=I,NVAL)
          READ(IUNIT,'(A127)')BUFFER(1:127)
          READ(BUFFER,1005,ERR=999)(SWL(J),J=I,NVAL)
          READ(IUNIT,'(A127)')BUFFER(1:127)
          READ(BUFFER,1006,ERR=999)(I1A(J),I2A(J),J=I,NVAL)
          READ(IUNIT,*)
          READ(IUNIT,*)
          DO 70 K=1,NV
           READ(IUNIT,'(A127)')BUFFER(1:127)
           READ(BUFFER,1007,ERR=999)TEAL,(GFT(K,J),J=I,NVAL)
   70     CONTINUE
   80   CONTINUE
        LVALID = .TRUE.
  100   RETURN
C
  998   WRITE(I4UNIT(-1),1008)CLINE
        WRITE(I4UNIT(-1),1002)
        RETURN
  999   WRITE(I4UNIT(-1),1008)BUFFER
        WRITE(I4UNIT(-1),1002)
        RETURN
C
C
C***********************************************************************
C
 1001 FORMAT(1X,32('*'),' E6DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,28('*'),' LVALID FLAG SET FALSE ',27('*'))
 1003 FORMAT(I5,6X,1A18,1X,2(I1,1X),F4.1)
 1004 FORMAT(2X,1PD8.2,4(3X,1PD8.2))
 1005 FORMAT(16X,10(1X,F7.1,3X))
 1006 FORMAT(16X,10(3X,I2,1X,I2,3X))
 1007 FORMAT(6X,F4.2,6X,10(1PD10.3,1X))
 1008 FORMAT(1X,32('*'),' E6DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA LINE: ',/,A)
 1009 FORMAT(1X,32('*'),' E6DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,A)
C#
 2001 FORMAT(16X,10(4X,I3,4X))
C#
C

 3000  FORMAT(/1X,30('*'),' E6DATA WARNING',30('*'),/,1x,
     &        '  Use of this subroutine is deprecated,',/,1x,
     &        '  Suggest using xxdata_20.for instead.',/, 1x, 75('*'))

C-----------------------------------------------------------------------
C
      END
