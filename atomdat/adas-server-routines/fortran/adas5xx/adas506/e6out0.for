CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas506/e6out0.for,v 1.1 2004/07/06 13:44:54 whitefor Exp $ Date $Date: 2004/07/06 13:44:54 $
CX
       SUBROUTINE E6OUT0( IWRITE   , IL     , LFSEL , LOSEL ,
     &                    TITLE    , TITLX  , TITLM , DATE  ,
     &                    IZ0      , IZ1    , IZ    ,
     &                    MAXT     , LTRNG  ,
     &                               TOA    , GFTOA ,
     &                               DENOA  , PREOA , RNHOA ,
     &                    LUPPER   , LLOWER ,
     &                    IUPPER   , ILOWER ,
     &                    APWLO    , SWLO   ,
     &                    CSTRGA   , ISA    , ILA   , XJA   ,
     &                    KPLUS1   , COEF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E6OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED TRANSITION UNDER
C            ANALYSIS.
C
C  CALLING PROGRAM: ADAS506
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (I*4)  IL      = NUMBER OF INDEX ENERGY LEVELS
C  INPUT : (L*4)  LOSEL   = .TRUE.  => INTERPOLATED   VALUES  FOR   ISPF
C                                      PANEL INPUT VALUES CALCULATED
C                           .FALSE  => INTERPOLATED   VALUES  FOR   ISPF
C                                      PANEL INPUT VALUES NOT CALCULATED
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE
C
C  INPUT : (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C  INPUT : (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C
C  INPUT : (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (kelvin)
C  INPUT : (R*8)  GFTOA() = SPLINE INTEROPLATED GFT   VALUE AT 'TOA()'
C  INPUT : (R*8)  DENOA() = SPLINE INTEROPLATED DENS  VALUE AT 'TOA()'
C  INPUT : (R*8)  PREOA() = SPLINE INTEROPLATED PRES  VALUE AT 'TOA()'
C  INPUT : (R*8)  RNHOA() = SPLINE INTEROPLATED NH/NE VALUE AT 'TOA()'
C
C  INPUT : (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C  INPUT : (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C  INPUT : (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C  INPUT : (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C
C  INPUT : (R*8)  APWLO   = APPROXIMATE WAVELENGTH OF TRANSITION (A)
C  INPUT : (R*8)  SWLO    = EXACT WAVELENGTH OF TRANSITION (A)
C  INPUT : (C*18) CSTRGA()= LEVEL CONFIGURATION FOR INDEX LEVELS
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR INDEX LEVELS
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR INDEX LEVELS
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR INDEX LEVELS
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (R*8)  TK2EV   = KELVINS TO ELECTRON VOLTS CONVERSION
C          (R*8)  TK2RT   = KELVINS TO REDUCED TEMPERATURE CONVERSION
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR  : H. P. SUMMERS, JET
C           K1/1/57
C           JET EXT. 4941
C
C DATE:    08/04/94
C
C UPDATE:
C
C----------------------------------------------------------------------
      REAL*8     TK2EV
C----------------------------------------------------------------------
      PARAMETER( TK2EV=8.6167D-05 )
C----------------------------------------------------------------------
      INTEGER    I           , IWRITE     , IL          ,
     &           IZ0         , IZ1        , IZ          ,
     &           MAXT        , KPLUS1     ,
     &           LUPPER      , LLOWER     ,
     &           IUPPER      , ILOWER
C----------------------------------------------------------------------
      REAL*8     TK2RT       , APWLO      , SWLO
C----------------------------------------------------------------------
      LOGICAL    LFSEL       , LOSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)   , TITLX*(*)  , TITLM*(*)   , DATE*8    ,
     &           TEXTU*5     , TEXTL*5    , CADAS*80
C----------------------------------------------------------------------
      INTEGER    ISA(IL)     , ILA(IL)
C----------------------------------------------------------------------
      REAL*8     XJA(IL)     ,
     &           TOA(MAXT)   , GFTOA(MAXT) ,
     &           DENOA(MAXT) , PREOA(MAXT) , RNHOA(MAXT) ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRNG(MAXT)
C----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18
C----------------------------------------------------------------------
      SAVE       CADAS
C----------------------------------------------------------------------
      DATA       TEXTU       , TEXTL       , CADAS
     &         / 'UPPER'     , 'LOWER'     , ' '        /
C----------------------------------------------------------------------
C
C**********************************************************************
C
      TK2RT=1.0/DBLE(IZ1*IZ1)
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001) ' G(TE) FUNCTION FILES INTERROGATION       ' ,
     &                   'ADAS506' , DATE
      WRITE(IWRITE,1002) TITLE
      WRITE(IWRITE,1003) TITLX
      WRITE(IWRITE,1004) IZ0    , IZ1    , IZ
      WRITE(IWRITE,1005)
      WRITE(IWRITE,1006) LUPPER, TEXTU, CSTRGA(IUPPER)(1:12),
     &                   ISA(IUPPER),
     &                   ILA(IUPPER), XJA(IUPPER)
      WRITE(IWRITE,1006) LLOWER, TEXTL, CSTRGA(ILOWER)(1:12),
     &                   ISA(ILOWER),
     &                   ILA(ILOWER), XJA(ILOWER)
C
C---------------------------------------------------------------------
C OUTPUT SELECTED TRANSITION  WAVELENGTHS
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1007) APWLO, SWLO
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES AND GFTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LOSEL) THEN
            WRITE (IWRITE,1008)
               DO 1 I=1,MAXT
                  IF (LTRNG(I)) THEN
                     WRITE(IWRITE,1009) TOA(I)        , (TK2EV*TOA(I)) ,
     &                                 (TK2RT*TOA(I)) , GFTOA(I)       ,
     &                                 DENOA(I)  , PREOA(I)  , RNHOA(I)
                  ELSE
                     WRITE(IWRITE,1010) TOA(I)        , (TK2EV*TOA(I)) ,
     &                                 (TK2RT*TOA(I))
                  ENDIF
    1          CONTINUE
            WRITE (IWRITE,1011)
         ENDIF
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
            WRITE (IWRITE , 1012 )
              DO 2 I=1,KPLUS1,2
                 IF (I.EQ.KPLUS1) THEN
                    WRITE (IWRITE , 1013 ) I , COEF(I)
                 ELSE
                    WRITE (IWRITE , 1014 ) I , COEF(I) , I+1 , COEF(I+1)
                 ENDIF
    2         CONTINUE
            WRITE (IWRITE , 1015 ) TITLM
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H1,A79)
 1001 FORMAT(1H0,17('*'),' TABULAR OUTPUT FROM ',A42,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,17('*')/)
 1002 FORMAT(1H0,19('-'),1X,A40,1X,19('-')//11X,
     & 'G(TE) CONTRIBUTION FUNCTIONS AS A FUNCTION OF TEMPERATURE'/
     & 11X,57('-')/21X,
     & 'DATA GENERATED USING PROGRAM: ADAS506'/21X,37('-')/)
 1003 FORMAT(1X,A80/)
 1004 FORMAT(' NUCLEAR CHARGE         (Z0) = ', I3 /
     &       ' RECOMBINING ION CHARGE (Z1) = ', I3 /
     &       ' RECOMBINED ION CHARGE  (Z)  = ', I3 /)
 1005 FORMAT(/
     & '    LEVEL      CONFIGURATION      QUANTUM NUMBERS '/
     & 33X,'2S+1  L  (WI-1)/2'/1X,49('-')
     &      )
 1006 FORMAT(1X,I2,' - ',A5,4X,A12,6X,I3,2X,I3,3X,F4.1)
 1007 FORMAT(//' TRANSITION WAVELENGTH (APPROX.) = ',F9.1,' A'
     &        /' TRANSITION WAVELENGTH (EXACT)   = ',F9.1,' A'/)
 1008 FORMAT(/1X,13('-'),' TEMPERATURE ',13('-'),'    GFT    ',
     &        6X,'DENS',9X,'PRESS',8X,'NH/NE',
     &/5X,'KELVIN',9X,'EV',6X,
     &'KELVIN/Z1**2','  CM3 S-1  ',6X,'CM-3',9X,'KCM-3',/1X,88('-'))
 1009 FORMAT(1P,2X,D9.2,6(4X,D9.2))
 1010 FORMAT(1P,2X,D9.2,2(4X,D9.2),
     &          4X,'-- TEMPERATURE VALUE OUT OF RANGE  --')
 1011 FORMAT(1X,88('-'))
C
 1012 FORMAT (1H0/1H0,'MINIMAX POLYNOMIAL - TAYLOR COEFFICIENTS:',
     &          ' LOG10(GFT) vs. LOG10(TEMP<kelvin>)'/1X,79('-'))
 1013 FORMAT ( 12X,'A(',I2,') = ',1P,D17.9)
 1014 FORMAT ( 2(12X,'A(',I2,') = ',1P,D17.9))
 1015 FORMAT (/1X,A79/1X,79('-'))
C
C---------------------------------------------------------------------
C
      RETURN
      END
