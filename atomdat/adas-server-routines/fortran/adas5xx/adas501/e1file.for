CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/e1file.for,v 1.3 2007/07/20 10:44:18 allan Exp $ Date $Date: 2007/07/20 10:44:18 $
CX
      SUBROUTINE E1FILE( IUNIT , IZ0 , IZ , IRCODE , DSNAME )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E1FILE *********************
C
C  PURPOSE: TO OPEN AN IONIZATIONS PER PHOTON DATA SET
C
C           BY DEFAULT AN 'IONELEC' FILE WILL BE USED FOR AN
C           EMITTING ION WITH NUCLEAR CHARGE 'IZ0' AND CHARGE 'IZ'.
C           OR, AN ALTERNATIVE DATA SET CAN BE SPECIFICED.
C           THIS WILL BE CONNECTED TO UNIT 'IUNIT'.
C
CA  DATA FILE OPENED UNDER UNIX : '$ADASCENT/DEFADF/USRGRP/USRTYP/
CA				    USRGRP_<EXT><ELEMENT><CHARGE>'
C
C  CALLING PROGRAM: SSXB
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNIT   = UNIT TO WHICH DATA SET WILL BE CONNECTED
C  INPUT : (I*4)   IZ0     = NUCLEAR CHARGE OF EMITTING ION REQUESTED
C  INPUT : (I*4)   IZ      = ION CHARGE OF EMITTING ION REQUESTED
C
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => DATA SET SUCCESSFULLY CONNECTED
C                            1 => REQUESTED DATA  SET  MEMBER  DOES  NOT
C                                 EXISTS - DATA SET NOT CONNECTED.
C                            6 => INVALID VALUE FOR 'IZ' ENTERED.
C                                 ( 0 <= 'IZ' <= 99 )
C                            9 => REQUESTED DATA  SET  EXISTS BUT CANNOT
C                                 BE OPENED.
CA OUTPUT: (C*80)  DSNAME  = FILE NAME INCLUDING PATH
C
C          (I*4)   IDLEN   = LENGTH, IN BYTES, OF FIXED 'DSNAME' PREFIX
C          (I*4)   LENF1   = FIRST NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF2   = LAST  NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF3   = FIRST NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF4   = LAST  NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF5   = FIRST NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF6   = LAST  NON-BLANK CHR OF 'DSNAME' URESID PART
C          (I*4)   LENF7   = FIRST NON-BLANK CHR OF 'DSNAME' EXTENSION PART
C          (I*4)   LENF8   = LAST  NON-BLANK CHR OF 'DSNAME' EXTENSION PART
C          (I*4)   IZEND   = LAST BYTE WRITTEN TO IN 'CZ'. (= 1 OR 2)
CA         (C*1)   HASH    = '#' if NON-BLANK EXT, ELSE ' '.
C          (C*2)   CZ      = 'IZ' (NO LEADING BLANKS)
C          (C*2)   XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)   ESYM    = ELEMENT SYMBOL FOR NUCLEAR CHARGE 'IZ0'
C          (C*3)   USREXT  = ADAS SOURCE DATA FILE EXTENSION
CA         (C*80)  USERID  = ADAS SOURCE DATA USER ID
C          (C*8)   USRGRP  = ADAS SOURCE DATA GROUPNAME
CA         (C*80)  USRTYP  = OPTIONAL SUB-DIRECTORY
CA	   (C*6)   DEFADF  = DEFAULT ADF DATA DIRECTORY, I.E. ADF13
C
C          (L*4)   LEXIST  = .TRUE.  => REQUESTED  DATA  SET  EXISTS.
C                            .FALSE. => REQUESTED  DATA  SET  DOES  NOT
C                                       EXIST.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSSXB     ADAS      FETCHES/SETS ADAS SOURCE DATA FILENAME
C                               AND FILE EXTENSION
C          XFESYM     ADAS      CHARACTER*2 FUNCTION -
C                               GATHERS ELEMENT SYMBOL FOR NUC. CHARGE
C	   XXSLEN     ADAS      FINDS FIRST AND LAST NON-BLANK
C				CHARACTERS IN STRING.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    01/06/91
C UPDATE:  31/07/91 - PE BRIDEN: ADDED "ACTION='READ'" TO OPEN STATEMENT
C UPDATE:  08/08/91 - PE BRIDEN: ADDED "IRCODE=9" => OPEN ERROR
C UPDATE:  10/03/93 - PE BRIDEN: ADDED CALL TO XXUID AND USERID VARIABLE
C                                - NOW ALLOWS ANY INPUT DATASET USER ID.
C UPDATE:   2/09/93 - HPS      : ADDED CALL TO XXSSXB AND USRGRP, USRTYP
C                                  AND USREXT NAMES
C                                - NOW ALLOWS ANY INPUT DATASET FILENAME
C                                  AND EXTENSION
C UPDATE:  23/11/93 - PEB      : CORRECT ERROR - A '.' HAD MISTAKENLY
C                                BEEN PLACED BEFORE THE MEMBER NAME IN
C                                VARIABLE DSNAME.
C
C UPDATE:  26/10/94 - L. JALOTA: MODIFIED CODE FOR RUNNING UNDER UNIX
C				 USING NEW FILENAMING CONVENTION.
C				 "ACTION" KEYWORD IN OPEN COMMAND IS IBM
C				 SO REMOVED HERE.
C
C UPDATE:   7/11/94 - L. JALOTA: ADDED VARIABLE DEFADF - DEFAULT ADF DIRECTORY
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 02-05-95
C MODIFIED: UNKNOWN (SOMEONE FROM TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C
C VERSION: 1.2				DATE: 19-05-95
C MODIFIED: Hugh Summers
C	    - Introduced hash to eliminate # in file if there is
C             no extension part of the file name alter logic to
C             allow usrtyp, usrext to be a single character.
C
C VERSION: 1.3				DATE: 20-07-07
C MODIFIED: Allan Whiteford
C	    - Modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IUNIT         , IZ0            , IZ         ,
     &            IRCODE        , IDLEN          , IZEND      ,
     &            LENF1         , LENF2          ,
     &            LENF3         , LENF4		 , LENF5      ,
     &	          LENF6		, LENF7		 , LENF8
C-----------------------------------------------------------------------
      CHARACTER   XFESYM*2      , ESYM*2         , CZ*2       ,
     &            USERID*80     , DSNAME*80      ,
     &            USRGRP*8      , USRTYP*80      , USREXT*3  ,
     &            DEFADF*5      , HASH*1
C-----------------------------------------------------------------------
      LOGICAL     LEXIST
C-----------------------------------------------------------------------
      PARAMETER (DEFADF = 'adf13')
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C IDENTIFY ADAS SOURCE DATA USER ID, GROUPNAME , TYPENAME
C AND FILE EXTENSION
C-----------------------------------------------------------------------
C
      USERID = '?'
      CALL XXUID(USERID)
C
      USRGRP = '?'
      USRTYP = '?'
      USREXT = '?'
      CALL XXSSXB(USRGRP , USRTYP , USREXT)
C
C-----------------------------------------------------------------------
C FIND THE FIRST AND LAST NON-BLANK CHARACTER STRINGS IN USRGRP & USRTYP
C-----------------------------------------------------------------------
C
      CALL XXSLEN( USRGRP , LENF1 , LENF2 )
      CALL XXSLEN( USRTYP , LENF3 , LENF4 )
      CALL XXSLEN( USERID , LENF5 , LENF6 )
      CALL XXSLEN( USREXT , LENF7 , LENF8 )
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      DSNAME = USERID(LENF5:LENF6)//'/'//DEFADF//'/'//
     &                    USRGRP(LENF1:LENF2)//'/'
      IDLEN  = INDEX(DSNAME,' ') - 1
C
      IF (LENF7.EQ.0) THEN
         HASH =' '
      ELSE
         HASH = '#'
      ENDIF
C
      IF (LENF3.EQ.0) THEN 
	 DSNAME = DSNAME(1:IDLEN)//USRGRP(LENF1:LENF2)//'_'
     &      //USREXT(LENF7:LENF8)//HASH
      ELSE 
	 DSNAME = DSNAME(1:IDLEN)//USRTYP(LENF3:LENF4)//'/'
     &      //USRGRP(LENF1:LENF2)//'_'//USREXT(LENF7:LENF8)//HASH
      ENDIF
      IDLEN  = INDEX(DSNAME,' ') - 1
      ESYM   = XFESYM( IZ0 )
C
         IF (IZ.LT.0) THEN
            IRCODE = 6
         ELSEIF (IZ.LT.10) THEN
            WRITE(CZ,1000) IZ
            IZEND  = 1
            IRCODE = 0
         ELSEIF (IZ.LT.100) THEN
            WRITE(CZ,1001) IZ
            IZEND  = 2
            IRCODE = 0
         ELSE
            IRCODE = 6
         ENDIF
C
         IF (IRCODE.EQ.0) THEN
C
            IF ( ESYM(2:2) .EQ. ' ' ) THEN
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:1)//CZ(1:IZEND)//'.dat'
            ELSE
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:2)//CZ(1:IZEND)//'.dat'
            ENDIF
C
            INQUIRE( FILE=DSNAME , EXIST=LEXIST )
C
               IF (.NOT.LEXIST) THEN
                  IRCODE = 1
               ELSE
		  OPEN(UNIT=IUNIT,FILE=DSNAME,STATUS='OLD',ERR=9999)
                  IRCODE = 0
               ENDIF
C
         ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C OPEN STATEMENT ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999 IRCODE = 9
      RETURN
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I1,' ')
 1001 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      END
