CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/test_ssxb.for,v 1.1 2004/07/06 15:24:28 whitefor Exp $ Date $Date: 2004/07/06 15:24:28 $
CX
      PROGRAM TEST_SSXB	
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PRGOGRAM: TEST_SSXB *******************
C
C  PURPOSE: TO TEST THE INTERROGATION ROUTINE SSXB AND ALL IMBEDDED
C  CALLS TO SUBROUTINES. SSXB EXTRACTS AND INTERPOLATES IONIZATIONS PER
C  PHOTON FOR EMITTING IONS.
C
C
C  INPUTS:
C
C         (CHAR*80) UIDIN   = CURRENT ADAS SOURCE DATA USER ID  PATHWAY.
C         (CHAR*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C         (CHAR*80) TYPIN   = CURRENT ADAS SOURCE DATA TYPENAME
C         (CHAR*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C
C         (I*4)     IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C         (I*4)     IZIN    = ION CHARGE OF EMITTING ION
C         (I*4)     IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C         (I*4)     ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSIY PAIRS
C         (R*8)     TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C         (R*8)     DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUTS : 
C         (R*8)     WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C         (R*8)     SXBA()  = IONIZATIONS PER PHOTON.
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C         (L*4)     LTRNG() =.TRUE.  => OUTPUT 'SXBA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'SXBA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)    LDRNG() =.TRUE.  => OUTPUT 'SXBA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'SXBA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C         (C*120)   TITLX   = INFORMATION STRING (DSN ETC.)
C         (I*4)     IRCODE  = RETURN CODE FROM SUBROUTINE SSXB:
C
C
C  AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C	    31/10/94
C
C  UPDATE:  22/03/95 _ HPS      : INTRODUCED MORE COMPLETE TEST, DIAGNOSTIC
C                                 PRINT AND NEW E1FILE SUBROUTINE.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     ITVAL
C-----------------------------------------------------------------------
      PARAMETER ( ITVAL = 10)
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , IZIN              ,
     &            IRCODE
C-----------------------------------------------------------------------
      REAL*8      WLNGTH
C-----------------------------------------------------------------------
      CHARACTER    UIDIN*80      , GRPIN*8           ,      
     &             TYPIN*80      , EXTIN*3           ,
     &             TITLX*120             
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)          ,
     &            SXBA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      DATA TVAL/ 5.0D0 , 1.0D1 , 1.5D1 , 2.0D1 , 3.0D1 ,
     &           4.0D1 , 5.0D1 , 6.0D1 , 8.0D1 , 1.0D2  /
      DATA DVAL/ 1.0D11, 1.0D11, 1.0D11, 1.0D11, 1.0D11,
     &           1.0D11, 1.0D11, 1.0D11, 1.0D11, 1.0D11 /
C-----------------------------------------------------------------------
c------------------------------------------------------------------------
C  EXAMPLE  SETTING THE ADAS CENTRAL DEFAULT DATABASE PATHWAY,
C-----------------------------------------------------------------------
C
        IRCODE = 0
C
	UIDIN = '*'
C
 	CALL XXUID(UIDIN)
 
 	GRPIN = '*'
 	TYPIN = '*'
	EXTIN = '*'

	CALL XXSSXB(GRPIN,TYPIN,EXTIN)	
C
        IBSEL = 1
	IZIN  = 0
	IZ0IN = 1
C	

	CALL SSXB( IBSEL  , IZIN   , IZ0IN  ,
     &             ITVAL  , TVAL   , DVAL   ,
     &             WLNGTH ,
     &             SXBA   , LTRNG  , LDRNG  ,
     &             TITLX  , IRCODE
     &            )
C
	WRITE(6,*) "WAVELENGTH : ", WLNGTH 
	WRITE(6,*) "TITLX      : ", TITLX
        WRITE(6,*) "IRCODE     : ", IRCODE
        WRITE(6,*) "SXBA(1)    : ", SXBA(1)
C
C-----------------------------------------------------------------------
      END
