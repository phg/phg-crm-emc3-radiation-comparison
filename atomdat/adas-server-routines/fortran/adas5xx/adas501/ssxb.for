CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/ssxb.for,v 1.3 2004/07/06 15:23:10 whitefor Exp $ Date $Date: 2004/07/06 15:23:10 $
CX
      SUBROUTINE SSXB( IBSEL  , IZIN   , IZ0IN  ,
     &                 ITVAL  , TVAL   , DVAL   ,
     &                 WLNGTH ,
     &                 SXBA   , LTRNG  , LDRNG  ,
     &                 TITLX  , IRCODE
     &               )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: SSXB **********************
C
C  PURPOSE: TO EXTRACT AND INTERPOLATE  IONIZATIONS PER PHOTON FOR
C           EMITTING IONS.
C           USES THE SAME ROUTINES USED BY ADAS501, EXCEPT FOR:
C
C           'E1FILE' - WHICH OPENS THE REQUESTED FILE.
C           'E1CHKB' - WHICH CHECKS INPUT  VALUES  ARE  CONSISTENT  WITH
C                      THE SELECTED DATA-BLOCK 'IBSEL' AND   'IBSEL'  IS
C                      IN RANGE.
C
C            THE FIRST OF THESE FUNCTIONS IS CARRIED  OUT  IN  'ADAS501'
C            VIA ISPF PANELS USING THE ROUTINE 'E1SPF0'  -  ADAS501 DOES
C            NOT REQUIRE THE ROUTINE 'E1CHKB' AS THE USER CANNOT  SELECT
C            AN INVALID VALUE FOR 'IBSEL' OR 'IBSEL'/EMITTER COMBINATION
C
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)   IZIN    = ION CHARGE OF EMITTING ION
C  INPUT : (I*4)   IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C  INPUT : (I*4)   ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSIY PAIRS
C  INPUT : (R*8)   TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)   DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (R*8)   WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C  OUTPUT: (R*8)   SXBA()  = IONIZATIONS PER PHOTON.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LTRNG() =.TRUE.  => OUTPUT 'SXBA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'SXBA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LDRNG() =.TRUE.  => OUTPUT 'SXBA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'SXBA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (C*80)  TITLX   = INFORMATION STRING (DSN ETC.)
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => NORMAL COMPLETION - NO ERROR DETECTED
C                            1 => DATA SET MEMBER FOR EMITTING ION WITH
C                                 CHARGE 'IZIN' & ION CHARGE 'IZ0IN' CAN
C                                 NOT BE FOUND/DOES NOT EXIST.
C                            2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                 AND THOSE IN INPUT FILE.
C                            3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                 OF RANGE OR DOES NOT EXIST.
C                            4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                 ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                            5 => INVALID VALUE FOR 'IZIN' ENTERED.
C                                 ( 0  <= 'IZIN' <= 99 )
C                            9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                 INPUT DATA-SET.
C
C          (I*4)   NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)   NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   NDDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON DENS-
C                                      ITIES  THAT  CAN  BE  READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)   IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)   IZ0LST  = LAST VALUE OF 'IZ0IN' FOR  WHICH  INPUT
C                            DATA WAS READ.
C          (I*4)   IZLAST  = LAST VALUE OF 'IZIN' FOR  WHICH  INPUT
C                            DATA WAS READ.
C          (I*4)   IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)   NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                            DATA SET.
C          (I*4)   IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C          (I*4)   IZ      = INPUT FILE - EMITTING ION - CHARGE
C          (I*4)   IZ1     = INPUT FILE - EMITTING ION - CHARGE + 1
C          (I*4)   IPOS    = USED IN CONVERTING CWAVEL -> WLNGTH
C
C          (L*4)   LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                            .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)   ESYM    = INPUT FILE - EMITTING ION - ELEMENT SYMBOL
C          (C*3)   EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C          (C*3)   EXTLST  = ADAS SOURCE DATA FILE EXT. USED LAST TIME
C                            DATA WAS READ.
CA         (C*80)  UIDIN   = CURRENT ADAS SOURCE DATA PATH
CA	   (C*80)  UIDLST  = ADAS SOURCE DATA PATH USED LAST TIME
C                            DATA WAS READ.
C          (C*8)   GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C          (C*8)   GRPLST  = ADAS SOURCE DATA GROUPNAME USED LAST TIME
C                            DATA WAS READ.
CA         (C*80)  TYPIN   = OPTIONAL SUBDIRECTORY FOR FILE
CA         (C*80)  TYPLST  = OPTIONAL SUBDIRECTORY FOR FILE USED LAST TIME
C                            DATA WAS READ.
C          (C*11)  C11     = USED IN CONVERTING CWAVEL -> WLNGTH
C          (C*80)  DSNREQ  = NAME OF DATA FILE REQUESTED INCLUDING PATH
C                            (MAY OR MAY NOT EXIST)
C          (C*80)  DSNAME  = FULL NAME OF DATA FILE INTERROGATED
C
C          (I*4)   ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                            TURES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   IDA()   = INPUT DATA SET-NUMBER OF ELECTRON DENSITIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)   TETA(,) = INPUT DATA SET -
C                            ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   TEDA(,) = INPUT DATA SET -
C                            ELECTRON DENSITIES    (UNITS: cm-3)
C                            1st DIMENSION: ELECTRON DENSITY     INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   SXB(,,)   =INPUT DATA SET -
C                             FULL SET OF IONIZATIONS PER PHOTON
C                             1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                             2nd DIMENSION: ELECTRON DENSITY     INDEX
C                             3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*10)  CWAVEL() = INPUT FILE - WAVELENGTH (ANGSTROMS)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CFILE()  = INPUT FILE - SPECIFIC ION FILE SOURCE
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CPCODE() = INPUT FILE - SPECIFIC ION PROCESSING CODE
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*2)   CINDM()  = INPUT FILE - METASTABLE INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E1FILE     ADAS      OPEN DATA SET FOR SELECTED EMITTER
C          E1DATA     ADAS      FETCH INPUT DATA FROM SELECTED DATA SET
C          E1CHKB     ADAS      CHECK VALIDITY OF ION AND 'IBSEL'
C          E1SPLN     ADAS      INTERPOLATE DATA WITH TWO WAY SPLINES
C          E1TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSSXB     ADAS      FETCHES/SETS ADAS SOURCE DATA FILE NAME+
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    30/04/91
C
C UPDATE:  05/12/91 - PE BRIDEN: 'NSTORE' INCREASED FROM 10 TO 100
C
C UPDATE:  28/02/92 - PE BRIDEN: 'NSTORE' INCREASED FROM 100 TO 150
C
C UPDATE:  10/03/93 - PE BRIDEN: INTRODUCED CALL TO XXUID TO ESTABLISH
C                                IF USERID OF INPUT DATASET CHANGES
C                                BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES UIDIN,UIDLST,DSNREQ)
C
C UPDATE:   2/09/93 - HPS      : INTRODUCED CALL TO XXSSXB TO ESTABLISH
C                                IF USRGRP, USRTYP AND USREXT OF INPUT
C                                DATA SET CHANGES BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES GRPIN,GRPLST,TYPIN,
C                                 TYPLST, EXTIN, EXTLST)
C UPDATE:   31/10/03 - LALIT JALOTA 
C			CHANGED STRING DIMENSIONS TO 80
C
C VERSION: 1.1                          DATE: 25-05-95
C MODIFIED:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 22-04-96
C MODIFIED:  TIM HAMMOND
C               - INCREASED PARAMETERS: NSTORE 150 -> 250
C                                       NTDIM   20 ->  35
c                                       NDDIM   20 ->  26
C               - MODIFIED CONVERSION OF CWAVEL->WLNGTH
C                 (ADDED VARIABLES C11 AND IPOS)
c
C VERSION: 1.3                          DATE: 17-01-2001
C MODIFIED:  Martin O'Mullane
C               - Removed spurious 2 in 1st column of a comment.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDIM             , NDDIM
      INTEGER     IZ0MIN         , IZ0MAX
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 250   , NTDIM  = 35       , NDDIM =  26  )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 50       )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , IZIN              ,
     &            ITVAL          , IRCODE
      INTEGER     IZ0LST         , IZLAST            ,
     &            IUNIT          , NBSEL             ,
     &            IZ0            , IZ                , IZ1          ,
     &            IPOS
C-----------------------------------------------------------------------
      REAL*8      WLNGTH
C-----------------------------------------------------------------------
      LOGICAL     LOPEN
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , EXTIN*3           , EXTLST*3     ,
     &                             UIDIN*80          , UIDLST*80    ,
     &                             GRPIN*8           , GRPLST*8     ,
     &                             TYPIN*80          , TYPLST*80    ,
     &            C11*11         ,
     &            DSNREQ*80      , DSNAME*80         , TITLX*80
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             ,
     &            ITA(NSTORE)               , IDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)          ,
     &            SXBA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CINDM(NSTORE)*2           , CFILE(NSTORE)*8      ,
     &            CPCODE(NSTORE)*8          , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDIM,NSTORE)        , TEDA(NDDIM,NSTORE)
      REAL*8      SXB(NTDIM,NDDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE        DSNAME      , UIDLST     , GRPLST     , TYPLST   ,
     &            EXTLST      , IZ0LST     , IZLAST     ,
     &            IUNIT
C-----------------------------------------------------------------------
      DATA        DSNAME /' '/   , UIDLST /' '/ , GRPLST /' '/     ,
     &            TYPLST /' '/   ,
     &            EXTLST /' '/   , IZ0LST /0/   , IZLAST /0/       ,
     &            IUNIT  /15/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IRCODE = 0
      LOPEN  = .FALSE.
C
C-----------------------------------------------------------------------
C ESTABLISH CURRENT ADAS SOURCE DATA UID, FILE AND FILE EXTENSION
C-----------------------------------------------------------------------
C
      UIDIN  = '?'
      CALL XXUID(UIDIN)
C
      GRPIN  = '?'
      TYPIN  = '?'
      EXTIN  = '?'
      CALL XXSSXB(GRPIN , TYPIN , EXTIN)
C
C-----------------------------------------------------------------------
C
         IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN
C
C-----------------------------------------------------------------------
C IF NEW EMITTING ION ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------
C
               IF ( (IZLAST.NE.IZIN)  .OR.
     &              (IZ0LST.NE.IZ0IN) .OR.
     &              (UIDLST.NE.UIDIN) .OR.
     &              (GRPLST.NE.GRPIN) .OR.
     &              (TYPLST.NE.TYPIN) .OR.
     &              (EXTLST.NE.EXTIN)      )  THEN
C
                  CALL E1FILE( IUNIT , IZ0IN , IZIN , IRCODE , DSNREQ )
                     IF (IRCODE.EQ.0) THEN
                        LOPEN  = .TRUE.
                        IZLAST = IZIN
                        IZ0LST = IZ0IN
                        UIDLST = UIDIN
                        GRPLST = GRPIN
                        TYPLST = TYPIN
                        EXTLST = EXTIN
                        DSNAME = DSNREQ
                        CALL E1DATA( IUNIT  , DSNAME ,
     &                               NSTORE , NTDIM  , NDDIM  ,
     &                               IZ0    , IZ     , IZ1    , ESYM  ,
     &                               NBSEL  , ISELA  ,
     &                               CWAVEL , CFILE  , CPCODE , CINDM ,
     &                               ITA    , IDA    ,
     &                               TETA   , TEDA   ,
     &                               SXB
     &                             )
		     END IF
               ENDIF
C
C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED EMITTING ION CHARGE VALUE.
C-----------------------------------------------------------------------
C
               IF ( IRCODE.EQ.0 ) THEN
                  CALL E1CHKB( IUNIT , NBSEL  , IBSEL ,
     &                         IZ0IN , IZIN   ,
     &                         IZ0   , IZ     ,
     &                         LOPEN , IRCODE
     &                       )
               ENDIF
C
C-----------------------------------------------------------------------
C 1) INTERPOLATE WITH TWO WAY SPLINES.
C 2) CREATE TITLE FOR OUTPUT.
C-----------------------------------------------------------------------
C
               IF ( IRCODE.EQ.0 ) THEN
                  CALL E1SPLN( NTDIM             , NDDIM         ,
     &                         ITA(IBSEL)        , IDA(IBSEL)    ,
     &                         ITVAL             ,
     &                         TETA(1,IBSEL)     , TEDA(1,IBSEL) ,
     &                         TVAL              , DVAL          ,
     &                         SXB(1,1,IBSEL)    , SXBA          ,
     &                         LTRNG             , LDRNG
     &                       )
                  CALL E1TITL( IBSEL         , DSNAME       ,
     &                         ESYM          , IZ           ,
     &                         CWAVEL(IBSEL) , CINDM(IBSEL) ,
     &                         TITLX
     &                       )
C
C MODIFIED - PEB - 10/04/96
C
                  C11  = CWAVEL(IBSEL)//'A'
                  IPOS = INDEX(C11,'*')
C
                     IF (IPOS.EQ.0) THEN
                       IPOS = INDEX(C11,'A') - 1
                       READ(C11(1:IPOS),*) WLNGTH
                     ELSE
                       WLNGTH = 0
                     ENDIF
C
C END OF MODIFICATION
C
               ENDIF
C
         ELSE
            IRCODE = 4
         ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
