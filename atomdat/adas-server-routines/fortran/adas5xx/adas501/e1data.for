CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/e1data.for,v 1.2 2004/07/06 13:34:29 whitefor Exp $ Date $Date: 2004/07/06 13:34:29 $
CX
       SUBROUTINE E1DATA( IUNIT  , DSNAME ,
     &                    NSTORE , NTDIM  , NDDIM  ,
     &                    IZ0    , IZ     , IZ1    , ESYM  ,
     &                    NBSEL  , ISELA  ,
     &                    CWAVEL , CFILE  , CPCODE , CINDM ,
     &                    ITA    , IDA    ,
     &                    TETA   , TEDA   ,
     &                    SXB
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E1DATA *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT IONIZATIONS PER PHOTON FILE
C            FOR A GIVEN EMITTING ION (ELEMENT AND CHARGE).
C            (MEMBER STORED IN IONELEC.DATA - MEMBER PREFIX 'SXB#').
C
C  CALLING PROGRAM: ADAS501/SSXB
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH BLOCK FORMING A COMPLETE SET OF IONIZATIONS
C           PER PHOTON VALUES FOR GIVEN TEMP./DENSITY COMBINATION. EACH
C           DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER  DATA-
C           BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           TEMPERATURES        : EV
C           DENSITIES           : CM-3
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*80) DSNAME   = NAME OF DATA FILE INCLUDING PATH
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NTDIM    = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDIM    = MAX NUMBER OF ELECTRON DENSITIES    ALLOWED
C
C  OUTPUT: (I*4)  IZ0      = READ - EMITTING ION - NUCLEAR CHARGE
C  OUTPUT: (I*4)  IZ       = READ - EMITTING ION - CHARGE
C  OUTPUT: (I*4)  IZ1      = READ - EMITTING ION - CHARGE + 1
C  OUTPUT: (C*2)  ESYM     = READ - EMITTING ION - ELEMENT SYMBOL
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*10) CWAVEL() = READ - WAVELENGTH (ANGSTROMS)
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CFILE()  = READ - SPECIFIC ION FILE SOURCE
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CPCODE() = READ - SPECIFIC ION PROCESSING CODE
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*2)  CINDM()  = READ - METASTABLE INDEX
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  ITA()    = READ - NUMBER OF ELECTRON TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (I*4)  IDA()    = READ - NUMBER OF ELECTRON DENSITIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TETA(,)  = READ - ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  TEDA(,)  = READ - ELECTRON DENSITIES (UNITS: CM-3)
C                            1st DIMENSION: ELECTRON DENSITY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  SXB(,,)   =READ - FULL SET OF IONIZATIONS PER PHOTON
C                                   VALUES.
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: ELECTRON DENSITY INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: ELECTRON TEMPERATURE INDEX
C          (I*4)  ITD      = ARRAY INDEX: ELECTRON DENSITY     INDEX
C          (I*4)  NTNUM    = NUMBER OF ELECTRON TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  NDNUM    = NUMBER OF ELECTRON DENSITIES    FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*5)  IONNAM   = EMITTING ION READ FROM DATASET
C          (C*6)  CKEY1    = 'FILMEM' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY2    = 'CODE  ' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY3    = 'INDM  ' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL  ' - INPUT BLOCK HEADER KEY
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 4569
C
C DATE:    30/04/91
C
C UPDATE:  05/12/91 - PE BRIDEN: IONNAM NOW ALLOWED TO OCCUPY EITHER
C                                4 OR 5 SPACES IN THE HEADER.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  22/11/94 - L. JALOTA - MODIFIED DSNAME LENGTH FOR UNIX 
C
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0                , I4FCTN              , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NTDIM                 , NDDIM               ,
     &           IZ0                   , IZ                  ,
     &           IZ1                   , NBSEL
      INTEGER    IBLK                  ,
     &           ITT                   , ITD                 ,
     &           NTNUM                 , NDNUM               ,
     &           IPOS1                 , IPOS2               , IABT
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*80             , ESYM*2
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*6               , CKEY2*4             ,
     &           CKEY3*4               , CKEY4*4             ,
     &           IONNAM*5              , C80*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         ,
     &           ITA(NSTORE)           , IDA(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CINDM(NSTORE)*2       , CFILE(NSTORE)*8      ,
     &           CPCODE(NSTORE)*8      , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)    , TEDA(NDDIM,NSTORE)
      REAL*8     SXB(NTDIM,NDDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4
C
C-----------------------------------------------------------------------
C
      DATA       CSLASH / '/' /
      DATA       CKEY1  / 'FILMEM' /   , CKEY2  / 'CODE'   /  ,
     &           CKEY3  / 'INDM'   /   , CKEY4  / 'ISEL'   /
C
C **********************************************************************
C
      LBEND = .FALSE.
C
C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) C80
      READ(C80  ,*   ) NBSEL
C
      IPOS1 = INDEX(C80,'/') + 1
      IPOS2 = IPOS1          + 4
      READ(C80(IPOS1:IPOS2),1001) IONNAM
C
         IF ( IONNAM(2:2).EQ.'+' ) THEN
            ESYM  = IONNAM(1:1) // ' '
         ELSE
            ESYM  = IONNAM(1:2)
         ENDIF
C
      IZ0   = I4EIZ0( ESYM )
      IZ    = I4FCTN( IONNAM(3:5) , IABT )
C
C PEB 05DEC91 - LINE BELOW ADDED
C
      IF (IABT.NE.0) IZ = I4FCTN( IONNAM(3:4) , IABT )
C
      IZ1   = IZ + 1
C
         IF (NBSEL.GT.NSTORE) THEN
            WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
            NBSEL = NSTORE
         ENDIF
C
C***********************************************************************
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C***********************************************************************
C
         DO 1 IBLK=1,NBSEL
C
C-----------------------------------------------------------------------
C INPUT TITLE, WAVELENGTH AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------
C
            IF (.NOT.LBEND) THEN
               READ(IUNIT,1000)   C80
C
                  IF ( C80(1:1).NE.'C') THEN
C
                     IPOS1 =  INDEX(C80,'A') + 1
                     IPOS2 =  INDEX(C80,'/') - 1
C
                        IF (IPOS1.EQ.1) THEN
                           IPOS1        = 11
                           CWAVEL(IBLK) = C80(1:10)
                        ELSEIF (IPOS1.GT.10) THEN
                           CWAVEL(IBLK) = C80(1:10)
                        ELSE
                           CWAVEL(IBLK) = C80(1:IPOS1-1)
                        ENDIF
C
                     READ(C80(IPOS1:IPOS2),*) IDA(IBLK) , ITA(IBLK)
C
                     CALL XXHKEY( C80 , CKEY1 , CSLASH , CFILE(IBLK)  )
                     CALL XXHKEY( C80 , CKEY2 , CSLASH , CPCODE(IBLK) )
                     CALL XXHKEY( C80 , CKEY3 , CSLASH , CINDM(IBLK)  )
                     CALL XXHKEY( C80 , CKEY4 , CSLASH , C2           )
                     ISELA(IBLK) = I4FCTN( C2 , IABT )
C
                     NDNUM = IDA(IBLK)
                     NTNUM = ITA(IBLK)
C
                         IF (NTNUM.GT.NTDIM) THEN
                            WRITE(I4UNIT(-1),2001) DSNAME , IBLK  ,
     &                                            'TEMPERATURES'  ,
     &                                            NTDIM   , NTNUM
                            STOP
                         ENDIF
C
                         IF (NDNUM.GT.NDDIM) THEN
                            WRITE(I4UNIT(-1),2001) DSNAME , IBLK   ,
     &                                            'DENSITIES'      ,
     &                                             NDDIM  , NDNUM
                            STOP
                         ENDIF
C
C-----------------------------------------------------------------------
C INPUT TEMPERATURE, DENSITY AND IONIZATIONS/PHOTON FOR BLOCK
C-----------------------------------------------------------------------
C
                     READ(IUNIT,1002) ( TEDA(ITD,IBLK) , ITD=1,NDNUM )
                     READ(IUNIT,1002) ( TETA(ITT,IBLK) , ITT=1,NTNUM )
C
                        DO 3 ITD=1,NDNUM
                           READ(IUNIT,1002) (SXB(ITT,ITD,IBLK),
     &                                       ITT=1,NTNUM        )
    3                   CONTINUE
C
                  ELSE
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1
                  ENDIF
C
            ENDIF
C
    1    CONTINUE
C
C***********************************************************************
C
 1000 FORMAT(1A80)
 1001 FORMAT(A5)
 1002 FORMAT(8D9.2)
C
 2000 FORMAT(/1X,31('*'),' E1DATA MESSAGE ',31('*')/
     &        2X,'INPUT IONS./PHOTON DATA SET NAME: ',A44/
     &        2X,'NUMBER OF WAVELENGTH DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' E1DATA ERROR ',32('*')/
     &        2X,'INPUT IONS./PHOTON DATA SET NAME: ',A44/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT ELECTRON ',A,' TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' E1DATA MESSAGE ',31('*')/
     &        2X,'INPUT IONS./PHOTON DATA SET NAME: ',A44/
     &        2X,'INCONSISTENCY IN THE NUMBER OF WAVELENGTHS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
