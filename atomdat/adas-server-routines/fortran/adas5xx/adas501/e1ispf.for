C -UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/e1ispf.for,v 1.5 2004/07/06 13:34:48 whitefor Exp $	Date $Date: 2004/07/06 13:34:48 $
C
      SUBROUTINE E1ISPF( IPAN   , LPEND  ,
     &                   NSTORE , NTDIM  , NDDIM  , NDTIN  ,
     &                   NBSEL  ,
     &                   CWAVEL , CFILE  , CPCODE , CINDM ,
     &                   ITA    , IDA    ,
     &                   TVALS  , TEDA   , SXB ,
     &                   TITLE  ,
     &                   IBSEL  , LDFIT  ,
     &                   ITTYP  , ITVAL  , TIN    , DIN   ,
     &                   LOSEL  , LFSEL  , LGRD1  , LDEF1 ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1    , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E1ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS501
C
C  SUBROUTINE:
C
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C 			      DUMMY VARIABLE - KEPT SO THAT ARGUMENT
C 			      LIST IS SAME AS IBM VERSION.
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NSTORE   = MAXIMUM NUMBER OF WAVELENGTH DATA-BLOCKS
C                             WHICH CAN BE READ FROM THE INPUT DATA-SET.
C  INPUT : (I*4)   NTDIM    = MAX. NO. OF ELECTRON TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDDIM    = MAX. NO. OF ELECTRON DENSITIES    ALLOWED
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURE/DENSITY
C                             PAIRS ALLOWED.
C
C  INPUT : (I*4)   NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C
C  INPUT : (C*10)  CWAVEL() = INPUT DATA FILE: WAVELENGTH (ANGSTROMS)
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*8)   CFILE()  = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*8)   CPCODE() = INPUT DATA FILE: PROCESSING CODE
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*2)   CINDM()  = INPUT DATA FILE: METASTABLE INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (I*4)   ITA()    = INPUT FILE - NO. OF ELECTRON TEMPERATURES
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (I*4)   IDA()    = INPUT FILE - NO. OF ELECTRON DENSITIES
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)   TVALS(,,)= INPUT DATA FILE: ELECTRON TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)   TEDA(,)  = INPUT DATA FILE: ELECTRON DENSITIES
C                                              (UNITS: CM-3)
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             3RD DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)   SXB(,,)  =INPUT DATA SET -
C                            FULL SET OF IONISATION PER PHOTON VALUES
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: ELECTRON DENSITY     INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*40)   TITLE   = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)    IBSEL   = SELECTED DATA-BLOCK INDEX FOR ANALYSIS
C  OUTPUT: (L*4)    LDFIT   = .TRUE. => FIT AND DISPLAY ELECTRON DENSITY
C                             .FALSE.=> FIT AND DISPLAY ELECTRON TEMPS.
C
C  OUTPUT: (I*4)    ITTYP   = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)    ITVAL   = NUMBER OF INPUT TEMP/DENSITY PAIRS (1->20)
C  OUTPUT: (R*8)    TIN()   = USER ENTERED ISPF VALUES -
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (R*8)    DIN()   = USER ENTERED ISPF VALUES -
C                             ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (L*4)    LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (IS ALWAYS .TRUE.)
C  OUTPUT: (L*4)    LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)    LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  UNIX PORT - LRGRD1 KEPT SO THAT ARGUMENT LIST IS SAME BUT NOT USED.
C
C  OUTPUT: (R*8)    TOLVAL  = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
CX  UNIX PORT - OUTPUT OPTIONS KEPT SO THAT ARGUMENT LIST IS SAME, BUT
CX 		  GRAPHING OPTIONS SELECTED LATER IN IDL VERSION.
CX                SO LDEF1 IS A DUMMY VARIABLE HERE
CX OUTPUT: (L*4)    LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
CX                          = .FALSE. => DO NOT USE DEFAULT SCALING
C  OUTPUT: (R*8)    XL1     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    XU1     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    YL1     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)    YU1     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C	   (C*80)   DUMMY   = GENERAL PURPOSE CHARACTER STRING
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH    IDL-ADAS	CALLS XXFLSH COMMAND TO CLEAR PIPE.
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:     1/12/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 21-02-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 27-02-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED FLUSH COMMAND
C
C VERSION: 1.3                          DATE: 06-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED CALL TO FLUSH TO XXFLSH
C
C VERSION: 1.4                          DATE: 06-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND REMOVED IMPLICIT DO LOOPS
C                 FROM PIPE COMMUNICATIONS.
C
C VERSION: 1.5
C MODIFIED:     RICHARD MARTIN
C               INCLUDED SXB DATA IN INPUT
C
C-----------------------------------------------------------------------
      INTEGER    IPAN          ,
     &           NSTORE        , NTDIM         , NDDIM        , NDTIN  ,
     &           NBSEL         , IBSEL         , ITTYP        , ITVAL
C-----------------------------------------------------------------------
      REAL*8     TOLVAL        ,
     &           XL1           , XU1           , YL1          , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , LDFIT         , 
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40    
C-----------------------------------------------------------------------
      INTEGER    ITA(NSTORE)              , IDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)               , DIN(NDTIN)
      REAL*8     TEDA(NDDIM,NSTORE)
      REAL*8     TVALS(NTDIM,3,NSTORE)
      REAL*8     SXB(NTDIM,NDDIM,NSTORE)      
C-----------------------------------------------------------------------
      CHARACTER  CINDM(NSTORE)*2          , CFILE(NSTORE)*8          ,
     &           CPCODE(NSTORE)*8         , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      INTEGER    PIPEIN        , PIPEOU   ,
     &           I             , J        , K                        ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C
	WRITE(PIPEOU,*) NSTORE
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NTDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NBSEL
	CALL XXFLSH(PIPEOU)
        DO 1, I=1, NBSEL
            WRITE(PIPEOU,'(A10)') CWAVEL(I)
	    CALL XXFLSH(PIPEOU)
1       CONTINUE
        DO 2, I=1, NBSEL
	    WRITE(PIPEOU,'(A8)') CFILE(I)
	    CALL XXFLSH(PIPEOU)
2       CONTINUE
        DO 3, I=1, NBSEL
	    WRITE(PIPEOU,'(A8)') CPCODE(I)
	    CALL XXFLSH(PIPEOU)
3       CONTINUE
        DO 4, I=1, NBSEL
	    WRITE(PIPEOU,'(A2)') CINDM(I)
	    CALL XXFLSH(PIPEOU)
4       CONTINUE
        DO 5, I=1, NBSEL
	    WRITE(PIPEOU,*) ITA(I)
	    CALL XXFLSH(PIPEOU)
5       CONTINUE
        DO 6, I=1, NBSEL
	    WRITE(PIPEOU,*) IDA(I)
	    CALL XXFLSH(PIPEOU)
6       CONTINUE
        DO 9, K=1, NBSEL
            DO 8, J=1, 3
                DO 7, I=1, ITA(K)
	            WRITE(PIPEOU,'(E12.4)') TVALS(I,J,K)
	            CALL XXFLSH(PIPEOU)
7               CONTINUE
8           CONTINUE
9       CONTINUE
        DO 11, J=1, NBSEL
            DO 10, I=1, IDA(J)
	        WRITE(PIPEOU,'(E12.4)') TEDA(I,J)
	        CALL XXFLSH(PIPEOU)
10          CONTINUE
11      CONTINUE
        DO 14, K=1, NBSEL
          DO 13, J=1, IDA(K)
            DO 12, I=1, ITA(K)
                WRITE(PIPEOU,'(E12.4)') SXB(I,J,K)
                CALL XXFLSH(PIPEOU)
12          CONTINUE
13	    CONTINUE
14      CONTINUE
C
C
C  READ OUTPUTS FROM PIPE FROM IDL
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
C
	READ(PIPEIN,'(A40)') TITLE
C
	READ(PIPEIN,*) IBSEL
C CHANGE IDL INDEX TO FORTRAN INDEX BY ADDING 1
        IBSEL = IBSEL+1
C
	READ(PIPEIN,*) ITTYP
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 0) THEN 
	   LDFIT = .FALSE.
        ELSE 
           LDFIT = .TRUE.
        ENDIF
	READ(PIPEIN,*) ITVAL
	READ(PIPEIN,*) (TIN(I), I=1,NDTIN)
	READ(PIPEIN,*) (DIN(I), I=1,NDTIN)
C
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 1) THEN
	   LFSEL = .TRUE.
	ELSE
	   LFSEL = .FALSE.
	END IF
C
	READ(PIPEIN,*) TOLVAL
	TOLVAL = TOLVAL / 100.0
C
C-----------------------------------------------------------------------
C
	RETURN
	END
