C
C -UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/e1outg.for,v 1.5 2004/07/06 13:35:28 whitefor Exp $	Date $Date: 2004/07/06 13:35:28 $
C
      SUBROUTINE E1OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   ESYM   , IZ0    , IZ    ,
     &                   CWAVEL , CFILE  ,
     &                   CPCODE , CINDM  , 
     &                   TEVA   , DIN    , SXBA  , ITVAL ,
     &                   TDFITM          , SXBM  , NMX   ,
     &                   LGRD1  , LDEF1  , LFSEL , LDFIT ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E1OUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL TO PLOT GRAPH
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE IINTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(IONS PER PHOTON) VERSUS LOG10(TEMP.(eV))  OR
C                                                  LOG10(DENSITY (cm-3))
C
C  CALLING PROGRAM: ADAS501
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED       | NOT
C                           .FALSE. => GHOST80 NOT INITIALISED   | USED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C  INPUT : (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK,TYPE and WLENGTH
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (C*2)  ESYM    = INPUT DATA SET: EMITTING ION ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT DATA SET: EMITTING ION NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA SET: EMITTING ION CHARGE STATE
C
C  INPUT : (C*10) CWAVEL  = SELECTED DATA-BLOCK: WAVELENGTH (ANGS.)
C  INPUT : (C*8)  CFILE   = SELECTED DATA-BLOCK: SPECIFIC ION FILE
C                                                 SOURCE
C  INPUT : (C*8)  CPCODE  = SELECTED DATA-BLOCK: PROCESSING CODE
C  INPUT : (C*2)  CINDM   = SELECTED DATA-BLOCK: METASTABLE INDEX
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (eV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  DIN()   = USER ENTERED: ELECTRON DENSITIES    (cm-3)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  SXBA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  IONI-
C                           ZATIONS/PHOTONS FOR THE USER ENTERED TEMP/
C                           DENSITY PAIRS.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE/DENSITY
C                           PAIRS.
C
C  INPUT : (R*8)  TDFITM()= MINIMAX: SELECTED TEMPERATURES (eV) OR
C                                             DENSITIES (cm-3)
C                                    (SEE 'LDFIT' FOR TYPE)
C  INPUT : (R*8)  SXBM()  = IONIZATIONS PER PHOTON AT 'TDFITM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX GENERATED IONs PER PHOTON
C                           /TEMP. OR DENS. PAIRS FOR GRAPHICAL DISPLAY
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1    = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C  INPUT : (L*4)  LDFIT   = .TRUE. =>MINIMAX FIT CARRIED OUT ON ELECTRON
C                                    DENSITIES  - ELECTRON DENSITIES TO
C                                    BE DISPLAYED.
C                           .FALSE.=>MINIMAX FIT CARRIED OUT ON ELECTRON
C                                    TEMPERATURES -ELECTRON TEMPERATURES
C                                    TO BE DISPLAYED.
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMP/DENSITY
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMP/DENSITY
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR IONIZATIONS/PHOTON
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR IONIZATIONS/PHOTON
C
C
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*28) STRG(1) =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG(2) =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG(3) =  DESCRIPTIVE STRING FOR CHARGE-STATE
C          (C*28) STRG(4) =  DESCRIPTIVE STRING WAVELENGTH
C          (C*28) STRG(5) =  DESCRIPTIVE STRING SPECIFIC ION FILE SOURCE
C          (C*28) STRG(6) =  DESCRIPTIVE STRING PROCESSING CODE
C          (C*28) STRG(7) =  DESCRIPTIVE STRING METASTABLE INDEX
C          (C*32) HEAD1   =  HEADING FOR RECEIVER INFORMATION
C          (C*45) HEAD2A  =  FIRST  HEADER FOR RECEIVER/DONOR TEMP. INFO
C          (C*45) HEAD2B  =  SECOND HEADER FOR RECEIVER/DONOR TEMP. INFO
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C	   XXFLSH      IDL-ADAS  CALLS FLUSH COMMAND TO CLEAR PIPE.
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC) 18-JAN-1995
C
C MODIFIED : 27/2/95	L. JALOTA   	ADDED FLUSH COMMAND AFTER
C					PIPE COMMS. 
C	   : 3/03/95	L.JALOTA	MODIFIED GRAPH ANNOTATION TEXT
C					TO MATCH IBMADAS
C          : 6/03/95    L.JALOTA        CHANGED CALL TO FLUSH TO XXFLSH
C-----------------------------------------------------------------------
C
      INTEGER    IZ0		 , IZ 		    , ITVAL
      INTEGER    NMX     
C-----------------------------------------------------------------------
      REAL*8	 XMIN            , XMAX             ,
     &           YMIN            , YMAX
      LOGICAL    LGHOST          , LGRD1           , LDEF1      , 
     &           LFSEL     , LDFIT
C-----------------------------------------------------------------------
CX    CHARACTER  TITLE*40       , TITLX*80          , TITLM*80  ,
      CHARACTER  TITLE*40       , TITLX*120         , TITLM*80  ,
     &           DATE*8         , ESYM*2            ,
     &           CINDM*2        , CFILE*8           ,
     &           CPCODE*8       , CWAVEL*10
      CHARACTER  STRG(7)*28     , 
     &           HEAD1*32       , HEAD2A*45      , HEAD2B*45
C-----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)    , DIN(ITVAL)        , SXBA(ITVAL)      ,
     &           TDFITM(NMX)                        , SXBM(NMX)
C-----------------------------------------------------------------------
      DATA HEAD1 /'--- EMITTING ION INFORMATION ---'/              ,
     &     HEAD2A/'- ELECTRON TEMPERATURE/DENSITY RELATIONSHIP -' / ,
     &     HEAD2B/'INDEX    TEMPERATURE (eV)    DENSITY (cm-3)  ' /
      DATA STRG(1) /' ELEMENT SYMBOL          = '/ ,
     &     STRG(2) /' NUCLEAR CHARGE          = '/ ,
     &     STRG(3) /' CHARGE STATE             = '/ ,
     &     STRG(4) /' WAVELENGTH (angstroms)  = '/ ,
     &     STRG(5) /' SPECIFIC ION FILE SOURCE = '/ ,
     &     STRG(6) /' PROCESSING CODE         = '/ ,
     &     STRG(7) /' METASTABLE INDEX         = '/
C-----------------------------------------------------------------------
      INTEGER    I
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') ESYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A10)') CWAVEL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CFILE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CPCODE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') CINDM
      CALL XXFLSH(PIPEOU)
C
      WRITE(PIPEOU,*) ITVAL
      CALL XXFLSH(PIPEOU)
      DO  1 I = 1,ITVAL
         WRITE(PIPEOU,*) TEVA(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
      DO  2 I = 1,ITVAL
         WRITE(PIPEOU,*) DIN(I)
         CALL XXFLSH(PIPEOU)
 2    CONTINUE
      DO 3 I = 1,ITVAL
         WRITE(PIPEOU,*) SXBA(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMAX
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMAX
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
         IF (LDFIT) THEN 
	    WRITE(PIPEOU,*) ONE
	    CALL XXFLSH(PIPEOU)
	 ELSE 
            WRITE(PIPEOU,*) ZERO
	    CALL XXFLSH(PIPEOU)
         ENDIF
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
	    WRITE(PIPEOU,*) SXBM(I)
	    CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO 5 I = 1, NMX
            WRITE(PIPEOU,*) TDFITM(I) 
	    CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF

      IF (LDFIT) THEN 
	 WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
      ELSE 
	 WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      WRITE(PIPEOU,'(A28)') (STRG(I), I=1,7)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A45)') HEAD2A
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A45)') HEAD2B
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
C-----------------------------------------------------------------------
      END
