CX  UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/e1titl.for,v 1.9 2010/11/30 13:09:38 mog Exp $ Date $Date: 2010/11/30 13:09:38 $
CX
      SUBROUTINE E1TITL( IBSEL  , DSNAME ,
     &                   ESYM   , IZ     ,
     &                   CWAVEL , CINDM  ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E1TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS501/SSXB
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*(*)) DSNAME   = FULL INPUT DATA SET NAME
C
C  INPUT : (C*2)  ESYM     = INPUT DATA SET: EMITTING ION ELEMENT SYMBOL
C  INPUT : (I*4)  IZ       = INPUT DATA SET: EMITTING ION CHARGE-STATE
C
C  INPUT : (C*10) CWAVEL   = SELECTED DATA-BLOCK: WAVELENGTH (ANGS.)
C  INPUT : (C*2)  CINDM    = SELECTED DATA-BLOCK: METASTABLE INDEX
C
C  OUTPUT: (C*120) TITLX   = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C	   (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C	   (I*4)  LEN_NAME = LENGTH OF FILENAME
C	   (I*4)  IFIRST   = POSITION OF FIRST CHARACTER IN FILENAME
C	   (I*4)  ILAST    = POSITION OF LAST CHARACTER IN FILENAME
C
C ROUTINES: 
C	   XXFCHR = FINDS THE FIRST & LAST OCCURRENCE OF ONE STRING IN
C			ANOTHER.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2620
C
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 02-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - MODIFY FOR RUNNING UNDER UNIX ENVIRONMENT WITH IDL
C
C VERSION: 1.2                          DATE: 02-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - 
C
C VERSION: 1.3                          DATE: 03-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               -
C
C VERSION: 1.4                          DATE: 06-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               -
C
C VERSION: 1.5                          DATE: 13-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               -
C
C VERSION: 1.6                          DATE: 13-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               -
C
C VERSION: 1.7                          DATE: 25-05-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - COMMENTED OUT LINE DEFINING XXSLEN AND XXFCHR
C                 AS INTEGERS.
C
C VERSION: 1.8                          DATE: 06-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND REMOVED SUPERFLUOUS VARIABLES
C
C VERSION : 1.9                          
C DATE    : 30-11-2010
C MODIFIED: Martin O'Mullane
C               - Keep code within 72 characters.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL           , IZ
CX    INTEGER    XXFCHR		 , XXSLEN
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*(*)      , ESYM*2         , 
     &           CINDM*2         , CWAVEL*10      , TITLX*120 ,
     &           C2*2            
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
	 CALL XXSLEN(DSNAME,IFIRST,ILAST)
	 LEN_NAME = ILAST-IFIRST+1
	 TITLX(1:LEN_NAME+1) = DSNAME(IFIRST:ILAST)
	 POS_NOW = LEN_NAME+1
         WRITE(C2,1000) IBSEL
         TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
	 POS_NOW = POS_NOW+9
         WRITE(C2,1000) IZ
         TITLX(POS_NOW:POS_NOW+6) = ESYM // '+' // C2  
	 POS_NOW = POS_NOW + 6
	 TITLX(POS_NOW:POS_NOW+29)=' WAVELENGTH= '//CWAVEL//
     &                             ' MET='//CINDM
C
C-----------------------------------------------------------------------
1000  FORMAT(I2)
C-----------------------------------------------------------------------
      RETURN
      END
