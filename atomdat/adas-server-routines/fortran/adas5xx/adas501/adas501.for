CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas501/adas501.for,v 1.13 2017/07/17 17:09:14 mog Exp $ Date  $Date: 2017/07/17 17:09:14 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS501 **********************
C
C  ORIGINAL NAME: SPRTGRF9
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF IONISATIONS PER PHOTON.
C
C            THE SELECTED INPUT DATA SET IS FOR A SPECIFIC EMITTING  ION
C            I.E. ELEMENT AND CHARGE STATE. THIS FILE CONTAINS FOR  THIS
C            ION A NUMBER OF BLOCKS OF DATA REPRESENTING DIFFERENT  WAVE
C            LENGTHS. FOR EACH WAVELENGTH A TABLE OF  IONISATIONS/PHOTON
C            ARE GIVEN FOR COMBINATIONS  OF  ELECTRON  TEMPERATURES  AND
C            DENSITIES.  THIS  PROGRAM  ALLOWS  FOR THE EXAMINATION  AND
C            DISPLAY OF A GIVEN WAVELENGTH AS FOLLOWS:
C
C            TABULATED IONS./PHOTON FOR  SELECTED WAVELENGTH ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
CX DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
CX           DATA SET AS FOLLOWS:-
CX
CX                    'JETSHP.IONELEC.DATA(SXB#<EL><Z>)'
CX
CX            WHERE <EL> REPRESENTS THE EMITTING ION ELEMENT SYMBOL
CX                       E.G. BE, C
CX                  <Z>  REPRESENTS THE EMITTING ION CHARGE  STATE
CX                       E.G. 2 , 1
CX
CX                       (I.E. MEMBER NAMES ARE SXB#BE2 , SXB#C1)
CX
C
CA DATA:     
CA
CA
C            EACH DATA SET MEMBER IS A TWO WAY TABLE OVER  ELECTRON
C            TEMPERATURE AND DENSITY.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            ELECTRON TEMPERATURES : eV
C            ELECTRON DENSITIES    : cm-3
C            IONISATIONS PER PHOTON:
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NTDIM   = PARAMETER = MAXIMUM NUMBER OF ELECTRON TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDDIM   = PARAMETER = MAXIMUM NUMBER OF ELECTRON DENS-
C                                       ITIES  THAT  CAN  BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF (ISPF) ENTRED ELECTRON
C                                       TEMPERATURE/DENSITY PAIRS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED IONS PER PHOTON
C                           /TEMPERATURE OR DENSITY PAIRS  FOR GRAPHICAL
C                           OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  IZ0     = INPUT FILE: EMITTING ION - NUCLEAR CHARGE
C          (I*4)  IZ      = INPUT FILE: EMITTING ION - CHARGE
C          (I*4)  IZ1     = INPUT FILE: EMITTING ION - CHARGE + 1
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  IBLK    = DATA-BLOCK WAVELENGTH INDEX
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE/DENSITY
C                           PAIRS.
C          (I*4)  ITTYP   = 1 => 'TIN(array)' UNITS: KELVIN
C                         = 2 => 'TIN(array)' UNITS: EV
C                         = 3 => 'TIN(array)' UNITS: REDUCED
C          (I*4)  IPAN    = ISPF PANEL NUMBER 
CA                          UNIX PORT - IPAN NO LONGER NEEDED BUT KEPT
CA			    SO THAT ARGUMENT LISTS IN SUBROUTINE CALLS
CA			    ARE NOT CHANGED
C
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'           OR
C                                  LOWER LIMIT FOR DENSITY (cm-3)
C                                  - SEE 'LDFIT' FOR TYPE.
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'           OR
C                                  UPPER LIMIT FOR DENSITY (cm-3)
C                                  - SEE 'LDFIT' FOR TYPE.
C          (R*8)  XMINPU  = GRAPH: LOWER LIMIT FOR TEMPERATURE (eV) or
C                                  DENSITY (cm-3) - see 'LDFIT'
C          (R*8)  XMAXPU  = GRAPH: UPPER LIMIT FOR TEMPERATURE (eV) or
C                                  DENSITY (cm-3) - see 'LDFIT'
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR IONIZATIONS/PHOTON.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR IONIZATIONS/PHOTON.
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
CA                          UNIX PORT - OPEN10 NO LONGER NEEDED BUT KEPT
CA                          SO THAT ARGUMENT LISTS IN SUBROUTINE CALLS
CA                          ARE NOT CHANGED
CX         (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
CX                          .FALSE. => NO GHOST80 OUTPUT WRITTEN
CA	   (L*4)  LGRAPH  = .TRUE.  => PLOT A GRAPH
CA	                    .FALSE. => DO NOT PLOT A GRAPH
CA         (L*4)  L2FILE  = .TRUE.  => SAVE DATA TO FILE
CA		            .FALSE. => DO NOT SAVE DATA TO FILE
CA         (C*80) SAVFIL  = NAME OF FILE TO SAVE DATA TO.
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LDFIT   = .TRUE.  => FIT AND DISPLAY ELECTRON DENSITY
C                           .FALSE. => FIT AND DISPLAY ELECTRON TEMPS.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
CX         (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
CX                                     GHOST80 GRID FILE.
CX                          .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*2)  ESYM    = INPUT FILE: EMITTING ION - ELEMENT SYMBOL
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CA         (C*80) DSFULL  = FULL INPUT DATA SET NAME FROM IDL-WINDOW
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET - NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IDA()   = INPUT DATA SET- NUMBER OF ELECTRON DENSITIES
C                           DIMENSION: DATA-BLOCK INDEX
CA         (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END
CA                          TO THE PROGRAM.
C
C          (R*8)  TIN()   = USER ENTERED ELECTRON TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  DIN()   = USER ENTERED ELECTRON DENSITIES. UNITS: CM-3
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TEVA()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TKEL()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TDFITM()= MINIMAX: SELECTED TEMPERATURES (eV) OR
C                                    DENSITIES (CM-3) FOR FITTING.
C                                    (SEE 'LDFIT' FOR TYPE)
C          (R*8)  SXBA()  = SPLINE INTERPOLATED OR EXTRAPOLATED IONIZA-
C                           TIONS/PHOTON FOR THE USER ENTERED ELECTRON
C                           TEMPERATURE/DENSITY PAIRS.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  SXBM()  = IONIZATIONS PER PHOTON AT 'TDFITM()'.
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TEDA(,) = INPUT DATA SET -
C                           ELECTRON DENSITIES (UNITS: CM-3)
C                           1st DIMENSION: ELECTRON DENSITY INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TVALS(,,)= INPUT DATA SET - ELECTRON TEMPERATURES
C                            1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                                           ( SEE 'TETA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8)  SXB(,,)   =INPUT DATA SET -
C                            FULL SET OF IONISATION PER PHOTON VALUES
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: ELECTRON DENSITY     INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*10) CWAVEL()= INPUT DATA FILE: WAVELENGTH (ANGSTROMS)
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CFILE() = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CPCODE()= INPUT DATA FILE: PROCESSING CODE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CINDM() = FILE DATA FILE: METASTABLE INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'SXBA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'SXBA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)  LDRNG()=  .TRUE.  => OUTPUT 'SXBA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           .FALSE. => OUTPUT 'SXBA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
CA	   E1SPF0     IDL_ADAS	GATHERS INPUT FILE NAMES VIA IDL WINDOW
C          E1DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          E1CHKZ     ADAS      VERIFY EMITTER READ FROM MEMBER NAME OK
C          E1SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
CA         E1ISPF     IDL_ADAS  GATHERS USERS VALUES VIA IDL WINDOW
C          E1SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E1TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
CA	   E1SPF1     IDL_ADAS  GATHERS USER OUTPUT OPTIONS VIA IDL WINDOW
C          E1OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          E1OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    30/04/91
C
C UPDATE:  11/06/91 PE BRIDEN: ADAS91- 'LDOUT' REMOVED - NOT USED
C
C UPDATE:  05/12/91 PE BRIDEN: ADAS91-'NSTORE' INCREASED FROM 30 TO 100
C
C UPDATE:  28/02/92 PE BRIDEN: ADAS91-'NSTORE' INCREASED FROM 100 TO 150
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 21-02-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - MODIFY FOR RUNNING UNDER UNIX ENVIRONMENT WITH IDL
C
C VERSION: 1.2                          DATE: 02-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED CALL TO I4UNIT TO WRITE TO UNIT 0 NOT STDOUT
C                 WHICH INTERFERES WITH PIPE COMMUNICATIONS
C
C VERSION: 1.3                          DATE: 02-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED CALL TO E1OUTG
C
C VERSION: 1.4                          DATE: 03-03-95
C MODIFIED: L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - 
C
C VERSION: 1.5                          DATE: 06-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED 'RETURN TO SERIES MENU' FUNCTIONALITY, SEARCH
C                 FOR 'ISTOP' TO SEE CHANGES.
C               - ADDED CALL TO XX0000 TO ENABLE USE OF I4UNIT FUNCTION
C                 (LATER BY OTHER ROUTINES)
C
C VERSION: 1.6                          DATE: 06-02-96
C MODIFIED: TIM HAMMOND 
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.7                          DATE: 15-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - INCREASED PARAMETERS: NSTORE: 150 -> 250
C                                       NTDIM:   20 -> 35
C                                       NDDIM:   20 -> 26
C
C VERSION: 1.8                          DATE: 15-04-96
C MODIFIED: TIM HAMMOND
C               - DECREASED PARAMETER: NTDIM: 35 -> 26
C                 (CURRENTLY IDL ROUTINES NEED NTDIM, NDDIM AND
C                  NDTIN SAME)
C
C VERSION: 1.9                          DATE: 22-04-96
C MODIFIED: TIM HAMMOND
C		- INCREASED NTDIM BACK TO 35 ALONG WITH NDDIM AND
C  		  NDTIN.
C
C VERSION: 1.10				DATE: 02-08-96
C MODIFIED: TIM HAMMOND
C               - CHANGED OUTPUT UNIT IUNT07 FROM 7 TO 17 TO AVOID
C                 CLASHING WITH STANDARD ERROR STREAM 7 ON HP 
C                 WORKSTATIONS.
C		- ALSO ADDED WRITE OF MINIMAX INFORMATION AFTER CALL
C 		  TO E1SPLN IN LINE WITH ALL OTHER MAIN ROUTINES.
C
C VERSION: 1.11                          DATE: 10-10-97
C MODIFIED: Martin O'Mullane
C               - IMPLEMENTED APPEND FUNCTIONALITY
C
C VERSION: 1.12                          DATE: 14-10-99
C MODIFIED: Richard Martin
C               - MODIFIED TO ALLOW 'RETURN TO INPUT FILE SELECTION'
C                       FROM OUTPUT WIDGET.
C                       ALSO PASS 'SXB' ARRAY INTO E1ISPF
C		    - INITIALISED STRINGS TITLE, TITLX, TITLM
C
C VERSION: 1.13                          DATE: 17-07-2017
C MODIFIED: Martin O'Mullane
C               - Use xxdata_13 to read adf13 file.
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NTDIM        , NDDIM      , NDTIN      ,
     &           MAXDEG
      INTEGER    IUNT07       , IUNT10       , PIPEIN     , I4UNIT
      INTEGER    NMX
      INTEGER    L1           , L2
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE = 250 , NTDIM  = 35 , NDDIM =  35 , NDTIN = 35 ,
     &           MAXDEG =  19 )
      PARAMETER( IUNT07 =  17 , IUNT10 = 10 , PIPEIN = 5)
      PARAMETER( NMX    = 100 )
      PARAMETER( L1     =   1 , L2     =  2 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    ISTOP	      , ILOGIC
      INTEGER    NBSEL        , IBLK         , IFORM      ,
     &           IZ0          , IZ           , IZ1        ,
     &           IBSEL        , KPLUS1       ,
     &           ITVAL        , ITTYP        , IPAN
C-----------------------------------------------------------------------
      REAL*8     XMIN         , XMAX         , 
     &           YMIN         , YMAX         ,
     &           TOLVAL
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , OPEN07       , 
     &           LGHOST       , LPEND        , LDFIT      ,
     &           LOSEL        , LFSEL        , LGRAPH     , L2FILE     ,
     &           LDSEL        , LGRD1        , LDEF1  	  , LREP
C-----------------------------------------------------------------------
      CHARACTER  ESYM*2       , REP*3        ,
CX   &           DATE*8       , DSFULL*44    ,
     &           DATE*8       , DSFULL*80    , SAVFIL*80  ,
CX   &           TITLE*40     , TITLM*80     , TITLX*80
     &           TITLE*40     , TITLM*80     , TITLX*120  , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)               ,
     &           ITA(NSTORE)                 , IDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)                  , DIN(NDTIN)              ,
     &           TEVA(NDTIN)                 , TKEL(NDTIN)             ,
     &           SXBA(NDTIN)                 , SXBM(NMX)               ,
     &           TDFITM(NMX)                 , COEF(MAXDEG+1)
C-----------------------------------------------------------------------
      CHARACTER  CINDM(NSTORE)*2             , CFILE(NSTORE)*8         ,
     &           CPCODE(NSTORE)*8            , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN)                , LDRNG(NDTIN)
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)          , TEDA(NDDIM,NSTORE)
      REAL*8     TVALS(NTDIM,3,NSTORE)       ,
     &           SXB(NTDIM,NDDIM,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , ITVAL  /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
      DATA OPEN07 /.FALSE./ 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
CX SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES
C- ---------------------------------------------------------------------
C
      CALL XX0000
	
	TITLE='NULL'
	TITLM='NULL'
	TITLX='NULL'
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
CA UNIX PORT - DATE GATHERING MOVED TO IDL, XXDATE COMMUNICATES WITH
CA               IDL VIA PIPE.
CA
      CALL XXDATE( DATE )
CA
C-----------------------------------------------------------------------
CA UNIX PORT - IPAN NO LONGER NEEDED. 
CA
CX100 IPAN  = 0
C
  100 CONTINUE
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
CA UNIX PORT - ROUTINE E1SPF0 MODIFIED TO COMMUNICATE WITH IDL
CA               USER INTERFACE VIA PIPE.
CA
      CALL E1SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
CX IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CA
CA UNIX PORT - GHOST80 NO LONGER USED.  E1SPF0 WILL STILL RETURN
CA               YES/NO FROM IDL INTERFACE.
CA

CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CX UNIX PORT - ACTION KEYWORD ON OPEN STATEMENT IBM SPECIFIC.
CA
CA
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD')
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
C-----------------------------------------------------------------------
CA
CA UNIX PORT - NO LONGER NEEDED, FUNCTIONALITY MOVED TO IDL.
CA
CX       IF (LDSEL) THEN
CX          CALL XXTEXT(IUNT10)
CX          GOTO 100
CX       ENDIF
CX
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
C       CALL E1DATA( IUNT10 , DSFULL ,
C      &             NSTORE , NTDIM  , NDDIM  ,
C      &             IZ0    , IZ     , IZ1    , ESYM  ,
C      &             NBSEL  , ISELA  ,
C      &             CWAVEL , CFILE  , CPCODE , CINDM ,
C      &             ITA    , IDA    ,
C      &             TETA   , TEDA   ,
C      &             SXB
C      &           )

      call xxdata_13( iunt10 , dsfull ,
     &                nstore , ntdim  , nddim  ,
     &                iz0    , iz     , iz1    , esym  ,
     &                nbsel  , isela  ,
     &                cwavel , cfile  , cpcode , cindm ,
     &                ita    , ida    ,
     &                teta   , teda   ,
     &                sxb
     &              )
C-----------------------------------------------------------------------
C VERIFY THAT EMITTER ELEMENT/CHARGE VALUES READ AGREE WITH MEMBER NAME
C-----------------------------------------------------------------------
C
CA UNIX PORT - NO LONGER NEEDED, FUNCTIONALITY MOVED TO IDL.
CA
CX    CALL E1CHKZ( DSFULL , ESYM   , IZ     )
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS INPUT ELECTRON TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TETA()' IS IN eV.
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXTCON( L2          , IFORM                ,
     &                      IZ1         ,
     &                      ITA(IBLK)   , TETA(1,IBLK)         ,
     &                      TVALS(1,IFORM,IBLK)
     &                    )
2           CONTINUE
1        CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
CA     CALL E1SETP( NBSEL  )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
CA
CA UNIX PORT - ROUTINE E1ISPF MODIFIED TO COMMUNICATE WITH IDL USER
CA               INTERFACE VIA PIPE.
CA
200   CONTINUE
      CALL E1ISPF( IPAN   , LPEND  ,
     &             NSTORE , NTDIM  , NDDIM  , NDTIN  ,
     &             NBSEL  ,
     &             CWAVEL , CFILE  , CPCODE , CINDM  ,
     &             ITA    , IDA    ,
     &             TVALS  , TEDA   , SXB,
     &             TITLE  ,
     &             IBSEL  , LDFIT  ,
     &             ITTYP  , ITVAL  , TIN   , DIN   ,
     &             LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN  , YMAX
     &           )
CA
CA UNIX PORT - DO NOT NEED IPAN VARIABLE
CX      IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN (TIN -> TKEL)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L1 , IZ1 , ITVAL , TIN , TKEL )
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV     (TIN -> TEVA)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L2 , IZ1 , ITVAL , TIN , TEVA )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV IF .NOT.LDFIT
C-----------------------------------------------------------------------
C
C        IF (LDFIT) THEN
C           XMINPU = XMIN
C           XMAXPU = XMAX
C        ELSE
C           XMINPU = R8TCON( ITTYP , L2 , IZ1 , XMIN )
C           XMAXPU = R8TCON( ITTYP , L2 , IZ1 , XMAX )
C        ENDIF
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------
C
      CALL E1SPLN( NTDIM             , NDDIM             ,
     &             ITA(IBSEL)        , IDA(IBSEL)        ,
     &             ITVAL             ,
     &             TETA(1,IBSEL)     , TEDA(1,IBSEL)     ,
     &             TEVA              , DIN               ,
     &             SXB(1,1,IBSEL)    , SXBA              ,
     &             LTRNG             , LDRNG
     &           )
 
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITVAL > 2).
C-----------------------------------------------------------------------
C
      IF (ITVAL.LE.2) THEN 
	 LFSEL = .FALSE.
      ENDIF
C
         IF (LFSEL) THEN
               IF (LDFIT) THEN
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         ITVAL   , DIN      , SXBA    ,
     &                         NMX     , TDFITM   , SXBM    ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
	        ELSE
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         ITVAL   , TEVA     , SXBA    ,
     &                         NMX     , TDFITM   , SXBM    ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
               ENDIF
               WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      CALL E1TITL( IBSEL         , DSFULL        ,
     &             ESYM          , IZ            ,
     &             CWAVEL(IBSEL) , CINDM(IBSEL)  ,
     &             TITLX
     &           )
C
C-----------------------------------------------------------------------
CA ADDED THE NEW CALL TO E1SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CA A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------
300   CONTINUE
      CALL E1SPF1( DSFULL        , 
     &             IBSEL         , LFSEL         , LDEF1    ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMIN          , XMAX          , YMIN     , YMAX   ,
     &             LPEND         , CADAS	 , LREP	    , ILOGIC 
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
CA IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
C
      IF (ILOGIC.EQ.1) GOTO 200
      IF (ILOGIC.EQ.2) GOTO 100
C
C----------------------------------------------------------------------
CA IF DESIRED - SAVE RESULTS TO FILE.
C----------------------------------------------------------------------
CA UNIX PORT - E1OUT0 COMMUNICATES WITH IDL VIA PIPE.
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN 
         IF (LREP.AND.OPEN07)THEN
            OPEN07 = .FALSE.
            CLOSE(IUNT07)
         ENDIF
         IF(.NOT.OPEN07)THEN
            OPEN(UNIT=IUNT07,FILE=SAVFIL,STATUS='UNKNOWN')
            OPEN07 = .TRUE.
         ENDIF

         CALL E1OUT0( IUNT07        , LDFIT        , LFSEL   ,
     &                TITLE         , TITLX        , TITLM   , DATE   ,
     &                IBSEL         , ITVAL        ,
     &                ESYM          , IZ0          , IZ      ,
     &                CWAVEL(IBSEL) , CFILE(IBSEL) ,
     &                CPCODE(IBSEL) , CINDM(IBSEL) ,
     &                LTRNG         , LDRNG        ,
     &                TEVA          , DIN          ,
     &                TKEL          ,
     &                SXBA          ,
     &                KPLUS1        , COEF         , CADAS
     &              )

      ENDIF
C
C-----------------------------------------------------------------------
C  IF SELECTED - GENERATE GRAPH
CA UNIX PORT - E1OUTG COMMUNICATES WITH IDL VIA PIPE.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN 
         CALL E1OUTG( LGHOST         ,
     &                 TITLE         , TITLX        , TITLM , DATE, 
     &                 ESYM          , IZ0   , IZ  ,
     &                 CWAVEL(IBSEL) , CFILE(IBSEL) ,
     &                 CPCODE(IBSEL) , CINDM(IBSEL) ,
     &                 TEVA          , DIN          , SXBA  , ITVAL,
     &                 TDFITM        , SXBM         , NMX   ,
     &                 LGRD1         , LDEF1        , LFSEL , LDFIT,
     &                 XMIN          , XMAX         , YMIN  , YMAX
     &              )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF (ISTOP.EQ.1) GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C
      IF(OPEN07) CLOSE(IUNT07)
      
 9999 END
