CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/e2titl.for,v 1.2 2004/07/06 13:38:44 whitefor Exp $ Date $Date: 2004/07/06 13:38:44 $
CX
      SUBROUTINE E2TITL( IBSEL  , DSNAME ,
     &                   CICODE , CFCODE ,
     &                   CIION  , CFION  ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E2TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS502/SSZD
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
CX INPUT : (C*44) DSNAME   = FULL MVS INPUT DATA SET NAME
C  INPUT : (C*80) DSNAME   = DATA FILE NAME UNDER UNIX INCLUDING PATH
C
C  INPUT : (C*2)  CICODE   = SELECTED DATA-BLOCK: INITIAL STATE
C                                                 METASTABLE INDEX.
C  INPUT : (C*2)  CFCODE   = SELECTED DATA-BLOCK: FINAL   STATE
C                                                 METASTABLE INDEX.
C
C  INPUT : (C*5)  CIION    = SELECTED DATA-BLOCK: INITIAL ION
C  INPUT : (C*5)  CFION    = SELECTED DATA-BLOCK: FINAL   ION
C
C  OUTPUT: (C*80) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C  GENERAL :
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C
C	   (I*4)  START    = POSITION OF FIRST NON_BLANK CHARACTER IN
C			      DSNAME
C	   (I*4)  END      = POSITION OF LAST  NON_BLANK CHARACTER IN
C			      DSNAME
C
C ROUTINES: 
CA	   XXSLEN          = UTILITY ROUTINE WHICH FINDS FIRST AND LAST
CA			     NON-BLANK CHARACTERS IN A STRING.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2620
C
C DATE:    07/06/91
C
C UPDATE : L. JALOTA  10/11/94 - MODIFIED TO RUN UNDER UNIX.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL 
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*(*)       ,
     &           CICODE*2        , CFCODE*2       ,
     &           CIION*5         , CFION*5        , TITLX*120
      CHARACTER  C2*2
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
         CALL XXSLEN(DSNAME,IFIRST,ILAST)
         LEN_NAME = ILAST-IFIRST+1
         TITLX(1:LEN_NAME+1) = DSNAME(IFIRST:ILAST)
         POS_NOW = LEN_NAME+1
         WRITE(C2,1000) IBSEL
         TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
         POS_NOW = POS_NOW+9
         TITLX(POS_NOW:POS_NOW+37) =  ' <' // CIION // ' (MET='
     &                               // CICODE // ')> ' // '- <'
     &                               // CFION // ' (MET=' // CFCODE 
     &                               // ')>'
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
