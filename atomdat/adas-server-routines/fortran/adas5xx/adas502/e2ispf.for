CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/e2ispf.for,v 1.2 2004/07/06 13:37:43 whitefor Exp $ Date $Date: 2004/07/06 13:37:43 $
CX
      SUBROUTINE E2ISPF( IPAN   , LPEND  ,
     &                   NSTORE , NTDIM  , NDTIN  ,
     &                   NBSEL  ,
     &                   CICODE , CFCODE , CIION  , CFION ,
     &                   BWNO   ,
     &                   ITA    , TVALS  ,
     &                   TITLE  ,
     &                   IBSEL  ,  
     &                   ITTYP  , ITVAL  , TIN    ,
     &                   LOSEL  , LFSEL  , LGRD1  , LDEF1 ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1    , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E2ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS502
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NSTORE   = MAXIMUM NUMBER OF DIFFERENT DATA-BLOCKS
C                             THAT CAN BE READ FROM THE INPUT DATA-SET.
C  INPUT : (I*4)   NTDIM    = MAX. NO. OF ELECTRON TEMPERATURES ALLOWED
C                             IN SOURCE DATA SET.
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF USER INPUT TEMPERATURE 
C                             VALUES ALLOWED.
C
C  INPUT : (I*4)   NBSEL    = INPUT DATA FILE: NUMBER OF DATA-BLOCKS 
C                             ACCEPTED & READ IN FROM SOURCE DATA SET.	
C
C  INPUT : (C*2)   CICODE() = INPUT DATA FILE: INITIAL STATE META. INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*2)   CFCODE() = INPUT DATA FILE: FINAL   STATE META. INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*5)   CIION()  = INPUT DATA FILE: INITIAL ION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*5)   CFION()  = INPUT DATA FILE: FINAL   ION
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)   BWNO()   = INPUT DATA FILE - EFFECTIVE IONIZATION
C                                          POTENTIAL (UNITS: CM-1)
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (I*4)   ITA()    = INPUT DATA FILE - NO. OF ELECTRON 
C                             TEMPERATURES
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)   TVALS(,,)= INPUT DATA FILE: ELECTRON TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*40)   TITLE   = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)    IBSEL   = USER ENTERED DATA-BLOCK INDEX FOR ANALYSIS
C
C  OUTPUT: (I*4)    ITTYP   = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)    ITVAL   = USER ENTERED NUMBER OF TEMPERATURE 
C                             VALUES (1->24)
C  OUTPUT: (R*8)    TIN()   = USER ENTERED TEMPERATURE VALUES -
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE VALUE INDEX
C
C  OUTPUT: (L*4)    LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)    LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)    LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)    LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)    TOLVAL  = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)    XL1     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    XU1     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    YL1     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)    YU1     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C 	   (I*4)    ILOGIC  = RETURN VALUE FROM IDL WHICH IS USED TO 
C			      REPRESENT A LOGICAL VARIABLE SINCE IDL DOES
C			      HAVE SUCH DATA TYPES.
C           
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH    IDL-ADAS	CALLS XXFLSH COMMAND TO CLEAR PIPE.
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    05/06/91
C
C UNIX-IDL PORT:
C
C UPDATE:  27/11/95   HP SUMMERS - MODIFIED VERSION FOR IDL.
C
C VERSION: 1.1                          DATE: 14-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 14-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REWRITTEN PIPE COMMUNICATIONS TO PREVENT MISSING
C                 OUT OF DATA - IMPLICIT DO LOOPS DO NOT ALLOW THE 
C                 CONSTANT FLUSHING OF THE PIPE TO TAKE PLACE AND
C                 EXPERIENCE HAS SHOWN THAT THIS IS NECESSARY.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     L24
C-----------------------------------------------------------------------
      PARAMETER ( L24 = 24    )
C-----------------------------------------------------------------------
      INTEGER    IPAN          , I4UNIT        ,
     &           NSTORE        , NTDIM         , NDTIN        ,
     &           NBSEL         , IBSEL         , ITTYP        , ITVAL
C-----------------------------------------------------------------------
      REAL*8     TOLVAL        ,
     &           XL1           , XU1           , YL1          , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND         ,  
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    ITA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     BWNO(NSTORE)             , TIN(NDTIN)
      REAL*8     TVALS(NTDIM,3,NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CICODE(NSTORE)*2         , CFCODE(NSTORE)*2         ,
     &           CIION(NSTORE)*5          , CFION(NSTORE)*5
C-----------------------------------------------------------------------
      INTEGER    PIPEIN        , PIPEOU   ,
     &           I             , J        , K                        ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C-----------------------------------------------------------------------
C 
	WRITE(PIPEOU,*)      NSTORE
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NTDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NBSEL
	CALL XXFLSH(PIPEOU)
        DO 1, I=1, NBSEL
            WRITE(PIPEOU,'(A2)') CICODE(I)
	    CALL XXFLSH(PIPEOU)
1       CONTINUE
        DO 2, I=1, NBSEL
	    WRITE(PIPEOU,'(A2)') CFCODE(I)
	    CALL XXFLSH(PIPEOU)
2       CONTINUE
        DO 3, I=1, NBSEL
	    WRITE(PIPEOU,'(A5)') CIION(I)
	    CALL XXFLSH(PIPEOU)
3       CONTINUE
        DO 4, I=1, NBSEL
	    WRITE(PIPEOU,'(A5)') CFION(I) 
	    CALL XXFLSH(PIPEOU)  
4       CONTINUE
        DO 5, I=1, NBSEL
 	    WRITE(PIPEOU,'(F15.1)') BWNO(I)
	    CALL XXFLSH(PIPEOU)
5       CONTINUE
        DO 6, I=1, NBSEL
 	    WRITE(PIPEOU,*) ITA(I)
	    CALL XXFLSH(PIPEOU)
6       CONTINUE
        DO 9, K=1, NBSEL
            DO 8, J=1, 3
                DO 7, I=1, ITA(K)
	            WRITE(PIPEOU,'(E12.4)') TVALS(I,J,K)
	            CALL XXFLSH(PIPEOU)
7               CONTINUE
8           CONTINUE
9       CONTINUE
C
C-----------------------------------------------------------------------
C  READ INPUTS FROM PIPE FROM IDL
C-----------------------------------------------------------------------
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
C
	READ(PIPEIN,'(A40)') TITLE
C
	READ(PIPEIN,*) IBSEL

C --------- CHANGE IDL INDEX TO FORTRAN INDEX BY ADDING 1 ---------

        IBSEL = IBSEL+1
C
	READ(PIPEIN,*) ITTYP
C
	READ(PIPEIN,*) ITVAL
C
        DO 10, I=1, ITVAL
	    READ(PIPEIN,*) TIN(I)
10      CONTINUE
C
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 1) THEN
	   LFSEL = .TRUE.
	ELSE
	   LFSEL = .FALSE.
	END IF
C
	READ(PIPEIN,*) TOLVAL
	TOLVAL = TOLVAL / 100.0
C
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 1) THEN
	   LOSEL = .TRUE.
	ELSE
	   LOSEL = .FALSE.
	END IF
C
C-----------------------------------------------------------------------
C
	RETURN
	END
