CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/e2outg.for,v 1.1 2004/07/06 13:38:03 whitefor Exp $ Date $Date: 2004/07/06 13:38:03 $
CX
      SUBROUTINE E2OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   ESYM   , IZ0    , IZ    , IZ1   ,
     &                   BWNO   ,
     &                   CICODE , CFCODE ,
     &                   CIION  , CFION  ,
     &                   TEVA   , SZDA   , ITVAL ,
     &                   TFITM  , SZDM   , NMX   ,
     &                   LGRD1  , LDEF1  , LFSEL ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E2OUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL TO PLOT GRAPH
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE INTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(ZERO-DENSITY IONIZATION RATE-COEFFT cm**3/s)
C                                       VERSUS
C                             LOG10(ELECTRON TEMPERATURE (eV))
C
C  CALLING PROGRAM: ADAS502
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED   | NOT USED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C  INPUT : (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK, ION INFORMATION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: IONIZING ION - ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: IONIZING ION - NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           IONIZING ION - INITIAL CHARGE
C  INPUT : (I*4)  IZ1     = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           IONIZING ION - FINAL   CHARGE
C
C  INPUT : (R*8)  BWNO    = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           EFFECTIVE IONIZATION POTENTIAL (cm-1).
C
C  INPUT : (C*2)  CICODE  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           INITIAL STATE METSTABLE INDEX
C  INPUT : (C*2)  CFCODE  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           FINAL   STATE METSTABLE INDEX
C  INPUT : (C*5)  CIION   = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           INITIAL ION
C  INPUT : (C*5)  CFION   = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           FINAL   ION
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (eV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  SZDA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED ZERO-
C                           DENSITY IONIZATION RATE-COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE VALUES
C
C  INPUT : (R*8)  TFITM() = MINIMAX: SELECTED TEMPERATURES (eV)
C  INPUT : (R*8)  SZDM()  = IONIZATION RATE-COEFFT AT 'TFITM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX GENERATED IONIZATION RATE-
C                           COEFFT./TEMP. VALUES FOR GRAPHICAL DISPLAY
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEFFT.
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEFFT.
C
C
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*28) STRG1   =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG2   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG3   =  DESCRIPTIVE STRING FOR EFFECTIVE ION. POT.
C          (C*28) STRG4   =  DESCRIPTIVE STRING FOR STATE ION
C          (C*28) STRG5   =  DESCRIPTIVE STRING FOR STATE META. INDEX
C          (C*28) STRG6   =  DESCRIPTIVE STRING FOR TEMP. LIST
C          (C*32) HEAD1   =  HEADING FOR IONISING ION INFORMATION
C          (C*16) HEAD2   =  HEADING FOR INITIAL STATE INFORMATION
C          (C*16) HEAD3   =  HEADING FOR FINAL   STATE INFORMATION
C          (C*24) HEAD4   =  HEADING FOR TEMPERATURE INFORMATION

C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C	   XXFLSH      IDL-ADAS  CALLS FLUSH COMMAND TO CLEAR PIPE.
C
C AUTHOR: H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    27/11/95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 14-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    IZ0             , IZ               , IZ1       ,
     &           ITVAL           , NMX
      INTEGER    I                        
C-----------------------------------------------------------------------
      REAL*8     BWNO            ,
     &           XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST          ,
     &           LGRD1           , LDEF1            , LFSEL
C-----------------------------------------------------------------------
CX    CHARACTER  TITLE*40       , TITLX*80          , TITLM*80  ,
      CHARACTER  TITLE*40       , TITLX*120         , TITLM*80  ,
     &           DATE*8         , ESYM*2            ,
     &           CICODE*2       , CFCODE*2          ,
     &           CIION*5        , CFION*5
      CHARACTER  STRG1*28       , STRG2*28          ,
     &           STRG3*28       , STRG4*28          ,
     &           STRG5*28       , STRG6*28          ,
     &           HEAD1*32       , HEAD2*16          , 
     &           HEAD3*16       , HEAD4*24
C-----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)    , SZDA(ITVAL)      ,
     &           TFITM(NMX)     , SZDM(NMX)
C-----------------------------------------------------------------------
      DATA HEAD1 /'--- IONISING ION INFORMATION ---'/ ,
     &     HEAD2 /' INITIAL STATE: '/                 ,
     &     HEAD3 /' FINAL   STATE: '/                 ,
     &     HEAD4 /' ELECTRON TEMPERATURES: '/
      DATA STRG1 /' ELEMENT SYMBOL           = '/ ,
     &     STRG2 /' NUCLEAR CHARGE           = '/ ,
     &     STRG3 /' EFFECTIVE ION. PT.(cm-1) = '/ ,
     &     STRG4 /'    ION                   = '/ ,
     &     STRG5 /'    METASTABLE INDEX      = '/ ,
     &     STRG6 /'    INDEX     TE(EV)        '/
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') ESYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(F15.1)') BWNO
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)') CIION
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') CICODE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)') CFION
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') CFCODE 
      CALL XXFLSH(PIPEOU)
C
      WRITE(PIPEOU,*) ITVAL
      CALL XXFLSH(PIPEOU)
      DO  1 I = 1,ITVAL
         WRITE(PIPEOU,*) TEVA(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
C
      DO 3 I = 1,ITVAL
         WRITE(PIPEOU,*) SZDA(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMAX
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMAX
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
	    WRITE(PIPEOU,*) SZDM(I)
	    CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO 5 I = 1, NMX
            WRITE(PIPEOU,*) TFITM(I) 
	    CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      WRITE(PIPEOU,'(A28)') STRG1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG3
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG4
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG5
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG6
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A16)') HEAD2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A16)') HEAD3
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A24)') HEAD4
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
