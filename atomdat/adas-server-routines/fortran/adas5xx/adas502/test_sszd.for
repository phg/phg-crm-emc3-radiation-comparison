CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/test_sszd.for,v 1.1 2004/07/06 15:24:32 whitefor Exp $ Date $Date: 2004/07/06 15:24:32 $
CX
      PROGRAM TEST_SSZD	
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: TEST_SSZD *******************
C
C  PURPOSE: TO TEST THE INTERROGATION ROUTINE SSZD AND ALL IMBEDDED
C  CALLS TO SUBROUTINES. SSZD EXTRACTS AND INTERPOLATES ZERO DENSITY
C  IONISATION RATE COEFFICIENTS FOR IONS OF AN ELEMENT
C
C
C  INPUTS:
C
C         (CHAR*80) UIDIN   = CURRENT ADAS SOURCE DATA USER ID  PATHWAY.
C         (CHAR*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C         (CHAR*80) TYPIN   = CURRENT ADAS SOURCE DATA TYPENAME
C         (CHAR*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C
C         (I*4)     IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C         (I*4)     IZ0IN   = NUCLEAR CHARGE OF ELEMENT
C
C         (I*4)     ITVAL   = NO. OF ELECTRON TEMPERATURES
C         (R*8)     TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                             DIMENSION: TEMPERATURE INDEX
C
C  OUTPUTS :
C         (R*8)     BWNO    = IONISATION POTENTIAL (CM-1)
C         (I*4)     IZ      = SELECTED ION CHARGE
C         (I*4)     IZ1     = SELECTED ION CHARGE +1
C
C         (I*4)     METI    = INITIAL ION METASTABLE INDEX
C         (I*4)     METF    = FINAL ION METASTABLE INDEX
C 
C         (R*8)     SZDA()  = IONIZATION COEFFICIENT.
C                             DIMENSION: TEMPERATURE INDEX
C         (L*4)     LTRNG() =.TRUE.  => OUTPUT 'SZDA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'SZDA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE INDEX
C
C         (C*120)   TITLX   = INFORMATION STRING (DSN ETC.)
C         (I*4)     IRCODE  = RETURN CODE FROM SUBROUTINE SSZD:
C
C
C  AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C	    31/10/94
C
C  UPDATE:  24/03/95 - HPS      : INTRODUCED MORE COMPLETE TEST, DIAGNOSTIC
C                                 PRINT AND NEW E2FILE SUBROUTINE.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     ITVAL
C-----------------------------------------------------------------------
      PARAMETER ( ITVAL = 10)
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , IRCODE            ,
     &            IZ             , IZ1               ,
     &            METI           , METF
C-----------------------------------------------------------------------
      REAL*8      BWNO
C-----------------------------------------------------------------------
      CHARACTER   UIDIN*80       , GRPIN*8           ,      
     &            TYPIN*80       , EXTIN*3           ,
     &            TITLX*120             
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , 
     &            SZDA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)
C-----------------------------------------------------------------------
      DATA TVAL/ 5.0D0 , 1.0D1 , 1.5D1 , 2.0D1 , 3.0D1 ,
     &           4.0D1 , 5.0D1 , 6.0D1 , 8.0D1 , 1.0D2  /
C-----------------------------------------------------------------------
c------------------------------------------------------------------------
C  EXAMPLE  SETTING THE ADAS CENTRAL DEFAULT DATABASE PATHWAY,
C-----------------------------------------------------------------------
C
        IRCODE = 0
C
	UIDIN = '*'
C
 	CALL XXUID(UIDIN)
 
 	GRPIN = '*'
 	TYPIN = '*'
	EXTIN = '*'

	CALL XXSSZD(GRPIN,TYPIN,EXTIN)	
C
        IBSEL = 1
	IZ0IN = 1
C	

	CALL SSZD( IBSEL  , IZ0IN  ,
     &             ITVAL  , TVAL   ,
     &             BWNO   , IZ     , IZ1    ,
     &             METI   , METF   , 
     &             SZDA   , LTRNG  , 
     &             TITLX  , IRCODE
     &            )
C 
	WRITE(6,*) "TITLX      : ", TITLX
        WRITE(6,*) "IRCODE     : ", IRCODE
        WRITE(6,*) "BWNO       : ", BWNO
        WRITE(6,*) "IZ         : ", IZ
        WRITE(6,*) "IZ1        : ", IZ1
        WRITE(6,*) "METI       : ", METI
        WRITE(6,*) "METF       : ", METF
        WRITE(6,*) "SZDA(1)    : ", SZDA(1)
C
C-----------------------------------------------------------------------
      END
