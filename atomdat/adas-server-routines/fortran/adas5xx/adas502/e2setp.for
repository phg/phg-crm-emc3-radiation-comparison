CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/e2setp.for,v 1.2 2008/03/26 13:09:39 allan Exp $ Date $Date: 2008/03/26 13:09:39 $
CX
       SUBROUTINE E2SETP( NBSEL )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E2SETP *********************
C
C  PURPOSE:  WRITES THE VALUE OF NBSEL OUT TO IDL
C
C  CALLING PROGRAM: ADAS502
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'XXDATA_07'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBSEL   = NUMBER OF IONIZING ION COMBINATIONS READ IN.
C                           I.E. NUMBER OF DATA-BLOCKS.
C
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TP PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    05/06/91
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 14-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 26-03-2008
C MODIFIED: Allan Whiteford
C               - Changed documentation to reflect data now comes
C                 from XXDATA_07 instead of E2DATA
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBSEL            
      INTEGER    PIPEOU
      PARAMETER ( PIPEOU = 6 )
C-----------------------------------------------------------------------
C
C **********************************************************************
C-----------------------------------------------------------------------
C WRITE NBSEL OUT TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, * ) NBSEL
      CALL XXFLSH( PIPEOU )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
