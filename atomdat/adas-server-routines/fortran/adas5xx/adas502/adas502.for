CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/adas502.for,v 1.7 2008/03/26 13:11:29 allan Exp $ Date $Date: 2008/03/26 13:11:29 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS502 **********************
C
C  ORIGINAL NAME: (NONE)
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF ELECTRON IMPACT IONIZATION RATE COEFFICIENTS.
C
C            THE SELECTED INPUT DATA SET IS FOR A SPECIFIC ELEMENT. THIS
C            FILE CONTAINS FOR THIS ELEMENT A NUMBER OF DATA BLOCKS EACH
C            REPRESENTING DIFFERENT COMBINATIONS OF  INITIAL  AND  FINAL
C            CHARGES. THESE BLOCKS ARE OPTONALLY RESOLVED INTO INITIAL &
C            FINAL METASTABLE STATES. FOR EACH DATA BLOCK  ZERO  DENSITY
C            IONIZATION RATE COEFFICIENTS ARE GIVEN  AS  A  FUNCTION  OF
C            ELECTRON  TEMPERATURE.
C            THIS  PROGRAM  ALLOWS  FOR THE EXAMINATION  AND DISPLAY  OF
C            A GIVEN INITIAL/FINAL ION COMBINATION AS FOLLOWS:
C
C            TABULATED RATE COEFFTS FOR  THE  SELECTED  COMBINATION  ARE
C            FITTED USING A CUBIC  SPLINE  ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.IONELEC.DATA(SZD#<EL>)'
C
C            WHERE <EL> REPRESENTS THE SYMBOL FOR THE PARTICULAR ELEMENT
C                       E.G. BE, C
C
C
C            EACH DATA SET MEMBER IS A ONE WAY TABLE OVER  ELECTRON
C            TEMPERATURE.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            ELECTRON TEMPERATURES : eV
C            IONIZATIONS RATE COEFF: cm**3 sec-1
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NTDIM   = PARAMETER = MAXIMUM NUMBER OF ELECTRON TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF ISPF ENTRED ELECTRON
C                                       TEMPERATURE VALUES.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED IONIZATION RATE
C                           COEFFTS/TEMPERATURE  VALUES  FOR  GRAPHICAL
C                           OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ0     = INPUT FILE: IONIZING ION - NUCLEAR CHARGE
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  IBLK    = DATA-BLOCK INDEX
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE VALUES
C          (I*4)  ITTYP   = 1 => 'TIN(array)' UNITS: KELVIN
C                         = 2 => 'TIN(array)' UNITS: EV
C                         = 3 => 'TIN(array)' UNITS: REDUCED
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C
C          (R*8)  R8TCON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMINPU  = GRAPH: LOWER LIMIT FOR TEMPERATURE (eV)
C          (R*8)  XMAXPU  = GRAPH: UPPER LIMIT FOR TEMPERATURE (eV)
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR IONIZATIONS/PHOTON.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR IONIZATIONS/PHOTON.
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                           .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
CA         (L*4)  L2GRAPH = .TRUE.  => DISPLAY GRAPH
CA                          .FALSE. => DO NOT DISPLAY GRAPH
C
CA         (L*4)  L2FILE  = .TRUE.  => WRITE DATA TO FILE
CA
C
C                          .FALSE. => DO NOT WRITE DATA TO FILE
CA         (C*80) SAVFIL  = NAME OF FILE TO WHICH DATA IS WRITTEN.
C
C          (C*2)  ESYM    = INPUT FILE: IONIZING ION - ELEMENT SYMBOL
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CA         (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
CX         (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM IDL)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
CA         (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
CA         (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET - NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ()    = INPUT FILE: IONIZING ION - INITIAL CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ1()   = INPUT FILE: IONIZING ION - FINAL   CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  BWNO()  = INPUT DATA FILE- EFFECTIVE IONIZATION
C                                            POTENTIAL (UNITS: CM-1)
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TIN()   = USER ENTERED ELECTRON TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TKEL()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TEVA()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TRED()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: REDUCED)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TFITM() = MINIMAX: SELECTED TEMPERATURES (eV) FOR
C                                    FITTING.
C          (R*8)  SZDA()  = SPLINE INTERPOLATED OR EXTRAPOLATED  ZERO-
C                           DENSITY IONIZATION RATE-COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  SZDM()  = IONIZATION RATE COEFFICIENTS AT 'TFITM()'.
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  SZD(,)  = INPUT DATA SET - FULL SET OF ZERO DENSITY
C                           IONIZATION RATE COEFFT. VALUES (cm**3/sec)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TVALS(,,)= INPUT DATA SET - ELECTRON TEMPERATURES
C                            1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                                           ( SEE 'TETA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C
C          (C*2)  CICODE()= INPUT DATA FILE: INITIAL STATE META. INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CFCODE()= INPUT DATA FILE: FINAL   STATE META. INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*5)  CIION() = INPUT DATA FILE: INITIAL ION
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*5)  CFION() = INPUT DATA FILE: FINAL ION
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'SZDA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'SZDA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
CA UNIX PORT - THOSE ROUTINES MARKED WITH 'CX' ARE NOW REPLACED BY NEW
CA 		ROUTINES COMMUNICATING WITH IDL SCREENS INSTEAD
CX         E2SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
CA         E2SPF0     ADAS      GATHERS INPUT FILE NAMES VIA IDL SCREENS
C          XXDATA_07  ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          E2CHKZ     ADAS      VERIFY ELEMENT READ FROM MEMBER NAME OK
CA         E2SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
CX         E2SETP     ADAS      PASSES VARIABLES BACK TO MAIN IDL ROUTINES
CX         E2ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
CA         E2ISPF     ADAS      GATHERS USERS VALUES VIA IDL SCREENS
C          E2SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E2TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
CA         E2OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
CX         E2OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING IDL
C          E2OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
CA	   E2SPF1     IDL-ADAS  OUTPUT/DISPLAY OPTIONS SELECTION SCREEN
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    11/06/91
C
C UPDATE:  05/12/91 - PE BRIDEN: 'NSTORE' INCREASED FROM 30 TO 100
C
C UPDATE:  28/02/92 - PE BRIDEN: 'NSTORE' INCREASED FROM 100 TO 150
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C
C UNIX-IDL PORT:
C
C UPDATE:  27/11/95 - H. P. SUMMERS - IDL-ADAS : MODIFIED FOR USE UNDER 
C					         UNIX WITH IDL INTERFACE
C
C VERSION: 1.1                          DATE: 13-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 29-01-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED XMINPU AND XMAXPU WITH XMIN AND XMAX
C                 IN CALL TO E2OUTG AS OTHERWISE IDL RECIEVES
C                 ZEROES FOR X-AXIS LIMITS AND CRASHES.
C
C VERSION: 1.3                          DATE: 23-05-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - INCREASED NDTIN TO 35
C
C VERSION: 1.4                          DATE: 23-05-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - INCREASED NTDIM TO 35
C
C VERSION: 1.5				DATE: 02-08-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED OUTPUT UNIT IUNT07 FROM 7 TO 17 TO AVOID
C                 CLASHING WITH STANDARD ERROR STREAM 7 ON HP 
C                 WORKSTATIONS.
C
C VERSION: 1.6                          DATE: 21-10-99
C MODIFIED: RICHARD MARTIN
C              - INITIALISED STRINGS TITLE, TITLX, TITLM
C
C VERSION: 1.7                          DATE: 26-03-08
C MODIFIED: Allan Whiteford
C              - Changed call from E2DATA to XXDATA_07
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NTDIM        , NDTIN      , MAXDEG
      INTEGER    IUNT07       , IUNT10       , PIPEIN
      INTEGER    NMX
      INTEGER    L1           , L2           , L3
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE = 150 , NTDIM  = 35 , NDTIN = 35 , MAXDEG = 19 )
      PARAMETER( IUNT07 =  17 , IUNT10 = 10 , PIPEIN = 5 )
      PARAMETER( NMX    = 100 )
      PARAMETER( L1     =   1 , L2     =  2 , L3    =  3 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT       , ISTOP
      INTEGER    NBSEL        , IBLK         , IFORM       ,
     &           IZ0          , IBSEL        , KPLUS1      ,
     &           ITVAL        , ITTYP        , IPAN
C-----------------------------------------------------------------------
      REAL*8     R8TCON
      REAL*8     XMIN         , XMAX         , XMINPU      , XMAXPU    ,
     &           YMIN         , YMAX         ,
     &           TOLVAL
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , LGHOST       , LPEND       
      LOGICAL    LOSEL        , LFSEL        
CA - UNIX PORT THESE VARIABLES ADDED FOR OUTPUT SELECTION OPTIONS
      LOGICAL    LGRAPH       , L2FILE
      LOGICAL    LDSEL        , LGRD1        , LDEF1
C-----------------------------------------------------------------------
CX    CHARACTER  ESYM*2       , REP*3        ,
CX   &           DATE*8       , DSFULL*44    ,
CX   &           TITLE*40     , TITLM*80     , TITLX*80
      CHARACTER  ESYM*2       , REP*3        ,
     &           DATE*8       , DSFULL*80    , CADAS*80 , 
     &           TITLE*40     , TITLM*80     , TITLX*120
CA - UNIX PORT : OUTPUT TEST FILENAME OBTAINED FROM IDL SCREEN
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)               , ITA(NSTORE) ,
     &           IZ(NSTORE)                  , IZ1(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     BWNO(NSTORE)                ,
     &           TIN(NDTIN)                  , TKEL(NDTIN)             ,
     &           TEVA(NDTIN)                 , TRED(NDTIN)             ,
     &           SZDA(NDTIN)                 , SZDM(NMX)               ,
     &           TFITM(NMX)                  , COEF(MAXDEG+1)
C-----------------------------------------------------------------------
      CHARACTER  CICODE(NSTORE)*2            , CFCODE(NSTORE)*2        ,
     &           CIION(NSTORE)*5             , CFION(NSTORE)*5
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN)
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)          , SZD(NTDIM,NSTORE)
      REAL*8     TVALS(NTDIM,3,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , ITVAL  /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000

      TITLE='NULL'
      TITLM='NULL'
      TITLX='NULL'  
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
CA - UNIX PORT : DATE NOW GATHERED VIA IDL
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
CA - UNIX PORT : IPAN NO LONGER USED
CX 100 IPAN = 0
  100 CONTINUE 
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
CX GET INPUT DATA SET NAME (FROM ISPF PANELS)
CA - UNIX PORT : FILENAME NOW GATHERED FROM IDL INTERFACE
CA
C-----------------------------------------------------------------------
C
      CALL E2SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CA - UNIX PORT : XXENDG NO LONGER USED
CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CA UNIX PORT - REMOVE IBM SPECIFIC KEYWORD
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='UNKNOWN')
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CA UNIX PORT - THIS FUNCTION CARRIED OUT VIA IDL INTERFACE 
C-----------------------------------------------------------------------
CX       IF (LDSEL) THEN
CX          CALL XXTEXT(IUNT10)
CX          GOTO 100
CX        ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
      CALL XXDATA_07 ( IUNT10 , DSFULL ,
     &                 NSTORE , NTDIM  ,
     &                 ESYM   , IZ0    ,
     &                 NBSEL  , ISELA  ,
     &                 IZ     , IZ1    ,
     &                 CICODE , CFCODE , CIION , CFION ,
     &                 BWNO   ,
     &                 ITA    ,
     &                 TETA   , SZD
     &               )
C
C-----------------------------------------------------------------------
C VERIFY THAT IONISING ION ELEMENT SYMBOL AGREES WITH MEMBER NAME
CA - UNIX PORT : NOT REQUIRED - PERFORMED IN IDL INTERFACE
C-----------------------------------------------------------------------
C
C     CALL E2CHKZ( DSFULL , ESYM )
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS INPUT ELECTRON TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TETA()' IS IN eV.
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXTCON( L2          , IFORM                ,
     &                      IZ1(IBLK)   ,
     &                      ITA(IBLK)   , TETA(1,IBLK)         ,
     &                                    TVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
      CALL E2SETP( NBSEL  )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
  200 CALL E2ISPF( IPAN   , LPEND  ,
     &             NSTORE , NTDIM  , NDTIN  ,
     &             NBSEL  ,
     &             CICODE , CFCODE , CIION  , CFION  ,
     &             BWNO   ,
     &             ITA    , TVALS  ,
     &             TITLE  ,
     &             IBSEL  , 
     &             ITTYP  , ITVAL  , TIN   ,
     &             LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN  , YMAX
     &           )
CA - UNIX PORT : IPAN NO LONGER USED
CX    IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN  (TIN -> TKEL)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L1 , IZ1(IBSEL) , ITVAL , TIN , TKEL )
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV      (TIN -> TEVA)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L2 , IZ1(IBSEL) , ITVAL , TIN , TEVA )
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO REDUCED (TIN -> TRED)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L3 , IZ1(IBSEL) , ITVAL , TIN , TRED )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV
C-----------------------------------------------------------------------
C
      XMINPU = R8TCON( ITTYP , L2 , IZ1(IBSEL) , XMIN )
      XMAXPU = R8TCON( ITTYP , L2 , IZ1(IBSEL) , XMAX )
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------
C
      CALL E2SPLN( ITA(IBSEL)     , ITVAL    ,
     &             BWNO(IBSEL)    ,
     &             TETA(1,IBSEL)  , TEVA     ,
     &             SZD(1,IBSEL)   , SZDA     ,
     &             LTRNG
     &           )
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITA(IBSEL) > 2).
C-----------------------------------------------------------------------
C
      IF (ITA(IBSEL).LE.2) LFSEL = .FALSE.
C
         IF (LFSEL) THEN
            CALL XXMNMX( LMLOG      , MAXDEG        , TOLVAL       ,
     &                   ITA(IBSEL) , TETA(1,IBSEL) , SZD(1,IBSEL) ,
     &                   NMX        , TFITM         , SZDM         ,
     &                   KPLUS1     , COEF          ,
     &                   TITLM
     &                 )
            WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      CALL E2TITL( IBSEL         , DSFULL        ,
     &             CICODE(IBSEL) , CFCODE(IBSEL) ,
     &             CIION(IBSEL)  , CFION(IBSEL)  ,
     &             TITLX
     &           )
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
C-----------------------------------------------------------------------
C
CA  UNIX PORT - 
CA ADDED THE NEW CALL TO E2SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CA A PIPE TO GET USER SELECTED OUTPUT OPTIONS.
C-----------------------------------------------------------------------
300   CONTINUE
      CALL E2SPF1( DSFULL        , 
     &             IBSEL         , LFSEL         , LDEF1    ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMIN          , XMAX          , YMIN     , YMAX   ,
     &             LPEND         , CADAS
     &           )
CA IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
C OUTPUT TEXT RESULTS
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN
        OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
        CALL E2OUT0( IUNT07       , LFSEL        ,
     &               TITLE        , TITLX        , TITLM    , DATE  ,
     &               IBSEL        , ITVAL        ,
     &               ESYM         , IZ0          ,
     &               IZ(IBSEL)    , IZ1(IBSEL)   ,
     &               BWNO(IBSEL)  ,
     &               CICODE(IBSEL), CFCODE(IBSEL),
     &               CIION(IBSEL) , CFION(IBSEL) ,
     &               LTRNG        ,
     &               TKEL         , TEVA         , TRED     ,
     &               SZDA         ,
     &               KPLUS1       , COEF         , CADAS
     &             )
         CLOSE(UNIT=IUNT07)
      ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT GRAPHICAL RESULTS.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN
        CALL E2OUTG( LGHOST       ,
     &               TITLE        , TITLX        , TITLM    , DATE  ,
     &               ESYM         , IZ0          ,
     &               IZ(IBSEL)    , IZ1(IBSEL)   ,
     &               BWNO(IBSEL)  ,   
     &               CICODE(IBSEL), CFCODE(IBSEL),
     &               CIION(IBSEL) , CFION(IBSEL) ,
     &               TEVA         , SZDA         , ITVAL    ,
     &               TFITM        , SZDM         , NMX      ,
     &               LGRD1        , LDEF1        , LFSEL    , 
     &               XMIN         , XMAX         , YMIN     , YMAX
     &             )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF (ISTOP.EQ.1) GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANEL. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C
 9999 END
