CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas502/e2spf0.for,v 1.1 2004/07/06 13:38:23 whitefor Exp $ Date $Date: 2004/07/06 13:38:23 $
CX

      SUBROUTINE E2SPF0( REP  , DSFULL, LDSEL) 

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E2SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS502
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME , INCLUDING PATH
C
CA - UNIX PORT : LDSEL ONLY USED TO KEEP ARGUMENT LIST THE SAME. 
CA 		 IT'S ORIGINAL FUNCTION IS CARRIED OUT IN IDL NOW
CX  OUTPUT: (L*4)   LDSEL   = .TRUE.  => COPASE DATA SET INFORMATION
CX                                       TO BE DISPLAYED BEFORE RUN.
CX                          = .FALSE. => COPASE DATA SET INFORMATION
CX                                       NOT TO BE DISPLAYED BEFORE RUN.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    28/02/95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 14-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSFULL*80 
C-----------------------------------------------------------------------
      LOGICAL      LDSEL
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
