C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas505/e5spf1.for,v 1.4 2004/07/06 13:43:42 whitefor Exp $ Date $Date: 2004/07/06 13:43:42 $
C
      SUBROUTINE E5SPF1( DSFULL    ,
     &                   IBSEL      , LFSEL      , LDEF1    ,
     &                   LGRAPH     , L2FILE     , SAVFIL   ,
     &                   XMIN       , XMAX       , YMIN     , 
     &                   YMAX       , LPEND      , CADAS    , LREP   ,
     &                   LPASS      , L2PASS     , PASSFIL    
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS501
C
C  SUBROUTINE:
C
C  INPUT :   (C*80)  DSFULL   = DATA FILE NAME
C  INPUT :   (I*4)   IBSEL    = SELECTED INPUT DATA BLOCK FOR ANALYSIS
C  INPUT :   (L*4)   LFSEL    = .TRUE.  => POLYNOMIAL FIT SELECTED
C				.FALSE. => NO POLYNOMIAL FIT SELECTED
C  OUTPUT:   (L*4)   LDEF1    = .TRUE.  => USER SLECTED AXES LIMITS
C				.FALSE. => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR TEMP OR DENSITY UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR TEMP OR DENSITY UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS IONIZATIONS/PHOTON
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT IONIZATIONS/PHOTON
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C  OUTPUT:   (L*4)   LREP     = .TRUE.  => REPLACE PAPER.TXT
C  				.FALSE. => DON'T
C  OUTPUT:   (C*80)  PASSFIL  = FILENAME FOR PASS FILE
C  OUTPUT:   (L*4)   LPASS    = .TRUE.  => REPLACE data505.pass
C  				.FALSE. => DON'T
C  OUTPUT:   (L*4)   L2PASS   = .TRUE.  => SAVE DATA TO PASS FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  Lalit Jalota (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    12/1/95
C
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    21TH MARCH 1996
C
C VERSION: 1.1			DATE: 21-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C 
C VERSION: 1.2			DATE: 02-03-96
C MODIFIED: WILLIAM OSBORN
C	    PROPER HEADER INFORMATION ADDED
C
C VERSION: 1.3			DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN
C           ADDED LREP PARAMETER AND CORRESPONDING PIPE READ
C
C VERSION: 1.4			DATE: 17-02-98
C MODIFIED: Martin O'Mullane
C           Added passing file to write CX adf04 line
C
C-----------------------------------------------------------------------
      REAL*8       XMIN        , XMAX        , YMIN        , YMAX
C-----------------------------------------------------------------------
      CHARACTER    DSFULL*80   , SAVFIL*80   , PASSFIL*80  , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      ,
     &             LFSEL       , LDEF1       , LREP        ,
     &             LPASS       , L2PASS
C-----------------------------------------------------------------------
      INTEGER      IBSEL       , ILOGIC      ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LGRAPH = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               LDEF1  = .TRUE.
	       READ(PIPEIN,*) XMIN
	       READ(PIPEIN,*) XMAX
	       READ(PIPEIN,*) YMIN
	       READ(PIPEIN,*) YMAX
	    ELSE
	       LDEF1  = .FALSE.
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF

         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
            READ(PIPEIN,*) ILOGIC
            IF(ILOGIC .EQ. ONE) THEN
               LPASS = .TRUE.
            ELSE
               LPASS = .FALSE.
            ENDIF
	    L2PASS = .TRUE.
            READ(PIPEIN, '(A)')PASSFIL
         ELSE
            L2PASS = .FALSE.
         ENDIF
C
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
            READ(PIPEIN,*) ILOGIC
            IF(ILOGIC .EQ. ONE) THEN
               LREP = .TRUE.
            ELSE
               LREP = .FALSE.
            ENDIF
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
            CALL XXADAS(CADAS)
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
