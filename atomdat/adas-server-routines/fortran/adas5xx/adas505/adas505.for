       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS505 **********************
C
C  ORIGINAL NAME: SPRTGRF8
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF THERMAL TOTAL CHARGE TRANSFER RATE COEFFICIENTS.
C
C            THE SELECTED INPUT DATA SET IS FOR A SPECIFIC   ELEMENT  AS
C            THE RECEIVING ION - THIS FILE CONTAINS FOR THIS  ELEMENT  A
C            NUMBER OF RECEIVER (RECOMBINING ION CHARGE) & NEUTRAL DONOR
C            (NUCLEAR CHARGE) COMBINATIONS. THIS PROGRAM ALLOWS FOR  THE
C            EXAMINATION  AND  DISPLAY  OF  A  SELECTED  COMBINATION  AS
C            FOLLOWS:
C
C            TABULATED VALUES FOR THE SELECTED COMBINATION  ARE  PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.IONATOM.DATA(TCX#<EL>)'
C
C            WHERE <EL> REPRESENTS THE RECEIVING ION ELEMENT SYMBOL
C
C               E.G. BE,  C    (I.E. MEMBER NAMES ARE TCX#BE , TCX#C)
C
C            EACH DATA SET MEMBER IS A TWO WAY TABLE OVER  RECEIVER  AND
C            DONOR TEMPERATURES.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            TEMPERATURES        : eV
C            RATE-COEFFICIENTS   : cm**3 sec-1
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NTRDIM  = PARAMETER = MAXIMUM NUMBER OF RECEIVER TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NTDDIM  = PARAMETER = MAXIMUM NUMBER OF DONOR    TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF IDL ENTRED RECEIVER
C                                       /DONOR TEMPERATURE PAIRS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT17  = PARAMETER = OUTPUT UNIT FOR PASSING CX DATA
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED RATE-COEFFT./
C                           TEMPERATURE PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  IBLK    = DATA-BLOCK ELEMENT INDEX
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE PAIRS
C          (I*4)  ITTYP   = 1 => 'TRIN/TDIN(array)' UNITS: KELVIN
C                         = 2 => 'TRIN/TDIN(array)' UNITS: EV
C                         = 3 => 'TRIN/TDIN(array)' UNITS: REDUCED
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C
C          (R*8)  R8TCON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  RMASS   = USER ENTERED - RECEIVER ISOTOPIC ATMOIC MASS
C                                          NUMBER.
C          (R*8)  DMASS   = USER ENTERED - DONOR    ISOTOPIC ATMOIC MASS
C                                          NUMBER.
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMINEV  = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: EV
C          (R*8)  XMAXEV  = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: EV
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEF (cm**3/sec)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEF (cm**3/sec)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                           .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LDFIT   = .TRUE.  => FIT AND DISPLAY DONOR    TEMPS.
C                           .FALSE. => FIT AND DISPLAY RECEIVER TEMPS.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM PIPE)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END TO
C                           THE PROGRAM
C          (I*4)  IRZ0()  = NUCLEAR CHARGE OF RECEIVING IMPURITY ION -
C                           READ FROM SELECTED DATA-BLOCK.
C                           DIMENSION: DATA-BLOCK INDEX.
C          (I*4)  IRZ1()  = INITIAL CHARGE OF RECEIVER -
C                           READ FROM SELECTED DATA-BLOCK.
C                           DIMENSION: DATA-BLOCK INDEX.
C          (I*4)  IDZ0()  = NUCLEAR CHARGE OF NEUTRAL DONOR -
C                           READ FROM SELECTED DATA-BLOCK.
C                           DIMENSION: DATA-BLOCK INDEX.
C          (I*4)  ITRA()  = INPUT DATA SET - NUMBER OF RECEIVER TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITDA()  = INPUT DATA SET- NUMBER OF DONOR TEMPERATURES
C                           DIMENSION: DATA-BLOCK INDEX
C
C 	   (C*9)  CDONOR()= DONOR IDENTIFICATION STRING
C	   (C*9)  CRECVR()= RECEIVER IDENTIFICATION STRING
C  	   (C*10) CFSTAT()= FINAL STATE SPECIFICATION STRING
C
C          (R*8)  AMSRA() = INPUT DATA-SET - RECEIVER ATOMIC MASS
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  AMSDA() = INPUT DATA SET  - DONOR    ATOMIC MASS
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TRIN()  = USER ENTERED RECEIVER TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TDIN()  = USER ENTERED DONOR    TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TREVA() = USER ENTERED RECEIVER TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TDEVA() = USER ENTERED DONOR    TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TRKEL() = USER ENTERED RECEIVER TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TDKEL() = USER ENTERED DONOR    TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TFEVM() = MINIMAX: SELECTED TEMPERATURES (eV)
C                                    (SEE 'LDFIT' FOR TYPE)
C          (R*8)  QTCXA() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE USER ENTERED RECEIVER/
C                           DONOR TEMPERATURE PAIRS (UNITS: CM**3/SEC)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  QTCXE() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE DONOR SET EQUAL TO THE
C                           USER ENTERED RECEIVER TEMPERATURE PAIRS
C                           AND VICE-VERSA - DEPENDS ON LDFIT 
C                           (UNITS: CM**3/SEC)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  QTCXM() = RATE-COEFFTS. (cm**3/sec) AT 'TFEVM()'
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  TFRA(,) = INPUT DATA SET -
C                           RECEIVER TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: RECEIVER TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TFDA(,) = INPUT DATA SET -
C                           DONOR    TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: DONOR    TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  QFTEQA(,)= INPUT DATA SET -
C                            EQUAL TEMPERATURE RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1st DIMENSION: RECEIVER TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8) TRVALS(,,)= INPUT DATA SET - RECEIVER TEMPERATURES
C                            1ST DIMENSION: RECEIVER TEMPERATURE INDEX
C                                           ( SEE 'TFRA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8) TDVALS(,,)= INPUT DATA SET - DONOR    TEMPERATURES
C                            1ST DIMENSION: DONOR    TEMPERATURE INDEX
C                                           ( SEE 'TFDA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8)  QFTCXA(,,)=INPUT DATA SET -
C                            FULL SET OF RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1st DIMENSION: DONOR    TEMPERATURE INDEX
C                            2nd DIMENSION: RECEIVER TEMPERATURE INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LEQUA() = INPUT DATA SET - DATA SET ENTRY FORMAT
C                                  .TRUE.  => DATA  SET CONTAINS  EQUAL
C                                             TEMPERATURE COEFFICIENT.
C                                  .FALSE. => DATA SET DOES NOT CONTAIN
C                                             EQUAL TEMPERATURE COEFFT.
C                           DIMENSION: DATA-BLOCK INDEX
C          (L*4)  LTRRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (L*4)  LTDRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C          (L*4)  LTRENG()= .TRUE.  => OUTPUT 'QTCXE()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (L*4)  LTDENG()= .TRUE.  => OUTPUT 'QTCXE()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C          (L*4)  OPEN07  = .TRUE.   => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  OPEN17  = .TRUE.   => UNIT 17 IS OPEN
C                           .FALSE.  => UNIT 17 IS CLOSED
C          (L*4)  LREP    = .TRUE.   => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C          (L*4)  LPASS   = .TRUE.   => DATA505.PASS TO BE REPLACED
C                           .FALSE.  => NOT TO BE REPLACED
C
CC	SOURCA NO LONGER IN ADF14 FILE
CX          (C*26) SOURCA()= INPUT DATA SET - INFORMATION STRING
CX                           DIMENSION: DATA-BLOCK INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E5SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          E5DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          E5SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          E5ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          E5SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E5TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          E5OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          E5OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    21/02/91
C
C UPDATE:  11/06/91 PE BRIDEN: ADAS91 - 'LDOUT' REMOVED - NOT USED
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  25/01/94 - PE BRIDEN - ADAS91: RENAMED FROM ADASm03->ADAS505
C
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    18-03-96
C
C VERSION: 1.1
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C 
C DATE:	   02-03-96
C
C VERSION: 1.2
C MODIFIED: WILLIAM OSBORN
C	    PROPER HEADER INFORMATION ADDED
C
C VERSION: 1.3                               DATE: 22-04-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C           - INCREASED LENGTH OF FILENAME DSFULL TO *80
C           - INCREASED PARAMETERS NTRDIM AND NTDDIM 20->26
C             IN LINE WITH CHANGES AT JET
C
C VERSION: 1.4                               DATE: 22-04-96
C MODIFIED: TIM HAMMOND 
C           - REDUCED NTRDIM AND NTDDIM BACK TO 20 TO PREVENT
C             PROBLEMS IN IDL CAUSED BY SIZING OF ARRAYS (THIS
C             SHOULD EVENTUALLY BE FIXED).
C
C VERSION: 1.5                               DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN
C           - ADDED LREP AND OPEN07 TO DEAL WITH PAPER.TXT OUTPUT
C             PROPERLY
C
C VERSION: 1.6			             DATE: 17-02-98
C MODIFIED: Martin O'Mullane
C	    - increased number of donor, reciever and output temperature 
C             pairs from 20 to 24.
C           - added ADF04 CX H line output option. For this
C              o added QTCXE splined values for Tdonor set equal 
C                to user entered Treceiver.
C              o added passing file to write CX adf04 line to data505.pass.
C
C VERSION: 1.7			             DATE: 17-04-98
C MODIFIED: MARTIN O'MULLANE, RICHARD MARTIN
C		- REMOVED QTCXE FROM CALL TO E5OUT0 WHICH CAUSED CRASH
C
C VERSION : 1.8
C DATE    : 08-03-2006
C MODIFIED: Martin O'Mullane
C               - Close unit17 at end of run.
C
C VERSION : 1.9
C DATE    : 30-06-2015
C MODIFIED: Martin O'Mullane
C               - Replace e5data with xxdata_14.
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NTRDIM       , NTDDIM     , NDTIN      ,
     &           MAXDEG
      INTEGER    IUNT07       , IUNT10       , IUNT17
      INTEGER    PIPEIN       , ISTOP
      INTEGER    NMX
      INTEGER    L1           , L2
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE =  150 , NTRDIM = 24 , NTDDIM =  24 ,
     & 		 NDTIN  = 24   , MAXDEG = 19 )
      PARAMETER( IUNT07 =   7  , IUNT10 = 10 , IUNT17 = 17  ,  
     &           PIPEIN = 5)
      PARAMETER( NMX    = 100  )
      PARAMETER( L1     =   1  , L2     =  2 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    NBSEL        , IBLK         , IFORM      ,
     &           IBSEL        , KPLUS1       ,
     &           ITVAL        , ITTYP        , IPAN
C-----------------------------------------------------------------------
      REAL*8     R8TCON
      REAL*8     RMASS        , DMASS        ,
     &           XMIN         , XMAX         , XMINEV     , XMAXEV     ,
     &           YMIN         , YMAX         ,
     &           TOLVAL
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , LGHOST       , LPEND      , LDFIT      ,
     &           LOSEL        , LFSEL        , LGRAPH     , L2FILE     ,
     &           LDSEL        , LGRD1        , LDEF1      , LCCD       ,
     &           OPEN07       , OPEN17       , LREP       , LPASS      ,
     &           L2PASS
C-----------------------------------------------------------------------
      CHARACTER  REP*3        , DATE*8       , DSFULL*80  , SAVFIL*80  ,
     &           PASSFIL*80   , TITLE*40     , TITLM*80   , TITLX*120  , 
     &           CADAS*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)               , IRZ0(NSTORE)            ,
     &           IRZ1(NSTORE)                , IDZ0(NSTORE)            ,
     &           ITRA(NSTORE)                , ITDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     AMSRA(NSTORE)               , AMSDA(NSTORE)           ,
     &           TRIN(NDTIN)                 , TDIN(NDTIN)             ,
     &           TREVA(NDTIN)                , TDEVA(NDTIN)            ,
     &           TRKEL(NDTIN)                , TDKEL(NDTIN)            ,
     &           QTCXA(NDTIN)                , QTCXM(NMX)              ,
     &           TFEVM(NMX)                  , COEF(MAXDEG+1)          ,
     &           QTCXE(NDTIN)             
C-----------------------------------------------------------------------
      LOGICAL    LEQUA(NSTORE)               ,
     &           LTRRNG(NDTIN)               , LTDRNG(NDTIN)           ,
     &           LTRENG(NDTIN)               , LTDENG(NDTIN)
C-----------------------------------------------------------------------
      CHARACTER  CDONOR(NSTORE)*9            , CRECVR(NSTORE)*9        ,
     &           CFSTAT(NSTORE)*10
C-----------------------------------------------------------------------
      REAL*8     TFRA(NTRDIM,NSTORE)         , TFDA(NTDDIM,NSTORE)     ,
     &           QFTEQA(NTRDIM,NSTORE)
      REAL*8     TRVALS(NTRDIM,3,NSTORE)     , TDVALS(NTDDIM,3,NSTORE) ,
     &           QFTCXA(NTDDIM,NTRDIM,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , ITVAL  /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./ , OPEN07/.FALSE./       ,
     &     OPEN17 /.FALSE./
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )

C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
 100  CONTINUE

         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (VIA IDL PIPE)
C-----------------------------------------------------------------------
C
      CALL E5SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS = 'OLD' )
      OPEN10=.TRUE.

C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
      CALL xxdata_14( IUNT10 , DSFULL ,
     &                NSTORE , NTRDIM , NTDDIM ,
     &                NBSEL  , ISELA  ,
     &                IRZ0   , IRZ1   , IDZ0   ,
     &                LEQUA  ,
     &                CDONOR , CRECVR , CFSTAT ,
     &                AMSRA  , AMSDA  ,
     &                ITRA   , ITDA   ,
     &                TFRA   , TFDA   ,
     &                QFTEQA , QFTCXA
     &              )

C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TRVALS(,)' - CONTAINS INPUT RECEIVER TEMPS. IN 3 FORMS.
C SET UP ARRAY 'TDVALS(,)' - CONTAINS INPUT DONOR    TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TFRA()' AND 'TFDA' ARE IN eV.
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXTCON( L2          , IFORM                ,
     &                      IRZ1(IBLK)  ,
     &                      ITRA(IBLK)  , TFRA(1,IBLK)         ,
     &                                    TRVALS(1,IFORM,IBLK)
     &                    )
               CALL XXTCON( L2          , IFORM                ,
     &                      IRZ1(IBLK)  ,
     &                      ITDA(IBLK)  , TFDA(1,IBLK)         ,
     &                                    TDVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE

      CLOSE(10)

C      DO 19 IFORM=1,NBSEL
C         WRITE(I4UNIT(-1),*)CDONOR(1),CRECVR(1),CFSTAT(1)
C         WRITE(I4UNIT(-1),*)IRZ0(1),IRZ1(1),IDZ0(1)
C 19   CONTINUE

C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL PIPE)
C-----------------------------------------------------------------------
  200 CALL E5ISPF( IPAN   , LPEND  ,
     &             NSTORE , NTRDIM , NTDDIM , NDTIN  ,
     &             NBSEL  ,
     &             IRZ0   , IRZ1   , IDZ0   ,
     &             CDONOR , CRECVR , CFSTAT ,
     &             AMSRA  , AMSDA  ,
     &             ITRA   , ITDA   ,
     &             TRVALS , TDVALS ,
     &             TITLE  ,
     &             IBSEL  , RMASS  , DMASS , LDFIT ,
     &             ITTYP  , ITVAL  , TRIN  , TDIN  ,
     &             LCCD, LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN  , YMAX
     &           )
C
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN (TRIN -> TRKEL, TDIN -> TDKEL)
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L1 , IRZ1(IBSEL) , ITVAL , TRIN , TRKEL )
      CALL XXTCON( ITTYP , L1 , IRZ1(IBSEL) , ITVAL , TDIN , TDKEL )
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV     (TRIN -> TREVA, TDIN -> TDEVA )
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L2 , IRZ1(IBSEL) , ITVAL , TRIN , TREVA )
      CALL XXTCON( ITTYP , L2 , IRZ1(IBSEL) , ITVAL , TDIN , TDEVA )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV.
C-----------------------------------------------------------------------
C
      XMINEV = R8TCON( ITTYP , L2 , IRZ1(IBSEL) , XMIN )
      XMAXEV = R8TCON( ITTYP , L2 , IRZ1(IBSEL) , XMAX )
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------
C
      CALL E5SPLN( NTRDIM            , NTDDIM            ,
     &             ITRA(IBSEL)       , ITDA(IBSEL)       ,
     &             ITVAL             ,
     &             AMSRA(IBSEL)      , AMSDA(IBSEL)      ,
     &             RMASS             , DMASS             ,
     &             TFRA(1,IBSEL)     , TFDA(1,IBSEL)     ,
     &             TREVA             , TDEVA             ,
     &             QFTCXA(1,1,IBSEL) , QTCXA             ,
     &             LTRRNG            , LTDRNG
     &           )

      IF (LDFIT) THEN
         CALL E5SPLN( NTRDIM            , NTDDIM            ,
     &                ITRA(IBSEL)       , ITDA(IBSEL)       ,
     &                ITVAL             ,
     &                AMSRA(IBSEL)      , AMSDA(IBSEL)      ,
     &                RMASS             , DMASS             ,
     &                TFRA(1,IBSEL)     , TFDA(1,IBSEL)     ,
     &                TDEVA             , TDEVA             ,
     &                QFTCXA(1,1,IBSEL) , QTCXE             ,
     &                LTRENG            , LTDENG
     &              )
      ELSE
         CALL E5SPLN( NTRDIM            , NTDDIM            ,
     &                ITRA(IBSEL)       , ITDA(IBSEL)       ,
     &                ITVAL             ,
     &                AMSRA(IBSEL)      , AMSDA(IBSEL)      ,
     &                RMASS             , DMASS             ,
     &                TFRA(1,IBSEL)     , TFDA(1,IBSEL)     ,
     &                TREVA             , TREVA             ,
     &                QFTCXA(1,1,IBSEL) , QTCXE             ,
     &                LTRENG            , LTDENG
     &              )
      ENDIF
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITVAL > 2).
C-----------------------------------------------------------------------
C
      IF (ITVAL.LE.2) LFSEL = .FALSE.
C
         IF (LFSEL) THEN
               IF (LDFIT) THEN
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         ITVAL   , TDEVA    , QTCXA   ,
     &                         NMX     , TFEVM    , QTCXM   ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
               ELSE
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         ITVAL   , TREVA    , QTCXA   ,
     &                         NMX     , TFEVM    , QTCXM   ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
               ENDIF
C            WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      CALL E5TITL( IBSEL         , DSFULL        ,
     &             CDONOR 	 , CRECVR 	 , CFSTAT	 ,
     &             TITLX
     &           )

C
C-----------------------------------------------------------------------
CA ADDED THE NEW CALL TO E5SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CA A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------
300   CONTINUE
      CALL E5SPF1( DSFULL        , 
     &             IBSEL         , LFSEL         , LDEF1    ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMIN          , XMAX          , YMIN     , YMAX   ,
     &             LPEND         , CADAS         , LREP     ,
     &             LPASS         , L2PASS        , PASSFIL    
     &           )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
CA IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
CA IF DESIRED - SAVE RESULTS TO FILE.
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         IF (LREP.AND.OPEN07)THEN
            OPEN07 = .FALSE.
            CLOSE(IUNT07)
         ENDIF
         IF(.NOT.OPEN07)THEN
            OPEN(UNIT=IUNT07,FILE=SAVFIL,STATUS='UNKNOWN')
            OPEN07 = .TRUE.
         ENDIF
         CALL E5OUT0( IUNT07        , LDFIT        , LFSEL       ,
     &                TITLE         , TITLX        , TITLM     , DATE,
     &                IBSEL         , ITVAL        ,
     &                CDONOR(IBSEL) , CRECVR(IBSEL), CFSTAT(IBSEL),
     &                IRZ0(IBSEL)   , IRZ1(IBSEL)  , IDZ0(IBSEL)  ,
     &                AMSRA(IBSEL)  , AMSDA(IBSEL) ,
     &                RMASS         , DMASS        ,
     &                LTRRNG        , LTDRNG       ,
     &                TREVA         , TDEVA        ,
     &                TRKEL         , TDKEL        ,
     &                QTCXA         ,
     &                KPLUS1        , COEF         , CADAS
     &           )
      ENDIF
C
C----------------------------------------------------------------------
CA IF DESIRED - SAVE CX ADF04 DATA TO PASS FILE.
C----------------------------------------------------------------------
C
      IF (L2PASS) THEN
         IF (LPASS.AND.OPEN17)THEN
            OPEN17 = .FALSE.
            CLOSE(IUNT17)
         ENDIF
         IF(.NOT.OPEN17)THEN
            OPEN(UNIT=IUNT17,FILE=PASSFIL,STATUS='UNKNOWN')
            OPEN17 = .TRUE.
         ENDIF
         CALL E5OUT1( IUNT17        , LDFIT        ,  ITVAL       ,
     &                TREVA         , TDEVA        ,
     &                QTCXA         , QTCXE         
     &              )
      ENDIF
C
C-----------------------------------------------------------------------
C  IF SELECTED, GENERATE GRAPH
C-----------------------------------------------------------------------
C
      IF(LGRAPH) THEN
         CALL E5OUTG( LGHOST       ,
     &             TITLE        , TITLX        , TITLM        , DATE   ,
     &             IRZ0(IBSEL)  , IRZ1(IBSEL)  , IDZ0(IBSEL)  ,
     &             CDONOR(IBSEL), CRECVR(IBSEL), CFSTAT(IBSEL),
     &             AMSRA(IBSEL) , AMSDA(IBSEL) ,
     &             RMASS        , DMASS        ,
     &             TREVA        , TDEVA        , QTCXA        , ITVAL  ,
     &             TFEVM                       , QTCXM        , NMX    ,
     &             LGRD1        , LDEF1        , LFSEL        , LDFIT  ,
     &             XMINEV       , XMAXEV       , YMIN         , YMAX
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
         READ(PIPEIN,*) ISTOP
         IF (ISTOP.EQ.1) GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C RETURN TO OUTPUT PANELS. (ON RETURN UNIT 10 IS CLOSED).
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
C UNIX PORT - MINIMAX INFORMATION NOT OUTPUT
C 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE

      IF(OPEN07) CLOSE(IUNT07)
      IF(OPEN17) CLOSE(IUNT17)

      END
