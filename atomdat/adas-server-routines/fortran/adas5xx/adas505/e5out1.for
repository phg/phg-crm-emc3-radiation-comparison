C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas505/e5out1.for,v 1.1 2004/07/06 13:43:03 whitefor Exp $ Date $Date: 2004/07/06 13:43:03 $
C
       SUBROUTINE E5OUT1( IWRITE   , LDFIT  , ITVAL  ,
     &                    TREVA    , TDEVA  ,
     &                    QTCXA    , QTCXE   
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5OUT0 *********************
C
C  PURPOSE:  TO  OUTOUT ADF04 CX H LINE TO END OF DATA505.PASS
C
C  CALLING PROGRAM: ADAS505
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LDFIT   = .TRUE. =>MINIMAX FIT CARRIED OUT ON  DONOR
C                                    TEMPERATURES IF FITTED.
C                           .FALSE.=>MINIMAX FIT CARRIED OUT ON RECEIVER
C                                    TEMPERATURES IF FITTED.
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED RECEIVER/DONOR TEMP-
C                           ERATURE PAIRS.
C  INPUT : (R*8)  TREVA() = USER ENTERED: RECEIVER TEMPERATURES (eV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (R*8)  TDEVA() = USER ENTERED: DONOR    TEMPERATURES (eV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C
C  INPUT : (R*8)  QTCXA() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE USER ENTERED RECEIVER/
C                           DONOR TEMPERATURE PAIRS (UNITS: cm**3/sec)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (R*8)  QTCXE() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE DONOR SET EQUAL TO THE
C                           USER ENTERED RECEIVER TEMPERATURE PAIRS 
C                           (UNITS: CM**3/SEC)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*210)STRING  = ADF04 CX H LINE STRING
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR  : Martin O'Mullane
C           K1/1/43
C           JET EXT. 5313
C
C DATE:    17/02/98
C
C VERSION: 1.1			DATE: 17-02-97
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , ITVAL     , i4unit
      INTEGER    IFIRST        , ILAST     , IPOS     , I
      INTEGER    LENS1         , LENS2
C----------------------------------------------------------------------
      LOGICAL    LDFIT         
C----------------------------------------------------------------------
      CHARACTER  STRING*210    , C9*9
C----------------------------------------------------------------------
      REAL*8     TREVA(ITVAL)  , TDEVA(ITVAL)  ,
     &           QTCXA(ITVAL)  , QTCXE(ITVAL)   
 
C**********************************************************************

C---------------------------------------------------------------------
C write ADF04 H line 
C---------------------------------------------------------------------

      IF (LDFIT) THEN
         WRITE(IWRITE,1020)
      ELSE
         WRITE(IWRITE,1025)
      ENDIF
      STRING = ' '
      IPOS = 0
      DO I = 1,ITVAL
        IF (LDFIT) THEN
           WRITE(C9,'(1PD9.2)') TDEVA(I)
        ELSE
           WRITE(C9,'(1PD9.2)') TREVA(I)
        ENDIF
        STRING(17+IPOS:21+IPOS) = C9(1:5)
        STRING(22+IPOS:24+IPOS) = C9(7:9)
        IPOS = IPOS+8
      END DO
      CALL XXSLEN(STRING,LENS1,LENS2)
      WRITE(IWRITE,'(A)')STRING(1:LENS2)
      
      STRING = '*'
      WRITE(STRING(1:16),'(A)')'H  ?  +?        '
      IPOS = 0
      DO I = 1,ITVAL
        WRITE(C9,'(1PD9.2)') QTCXA(I)
        STRING(17+IPOS:21+IPOS) = C9(1:5)
        STRING(22+IPOS:24+IPOS) = C9(7:9)
        IPOS = IPOS+8
      END DO
      CALL XXSLEN(STRING,LENS1,LENS2)
      WRITE(IWRITE,'(A)')STRING(1:LENS2)

      STRING = '*'
      WRITE(STRING(1:16),'(A)')'H  ?  +?        '
      IPOS = 0
      DO I = 1,ITVAL
        WRITE(C9,'(1PD9.2)') QTCXE(I)
        STRING(17+IPOS:21+IPOS) = C9(1:5)
        STRING(22+IPOS:24+IPOS) = C9(7:9)
        IPOS = IPOS+8
      END DO
      CALL XXSLEN(STRING,LENS1,LENS2)
      WRITE(IWRITE,'(A)')STRING(1:LENS2)

      WRITE(IWRITE,'(/)')


C
C---------------------------------------------------------------------
C
 1020 FORMAT ( / ,'CX H line for input to ADF04 dataset',/,
     &            'Line 1 : Donor Temperature',/,
     &            'Line 2 : CX rate for T receiver / T donor pairs',/,
     &            'Line 3 : CX rate for T receiver',
     &            ' set equal to T donor',/)
 1025 FORMAT ( / ,'CX H line for input to ADF04 dataset',/,
     &            'Line 1 : Receiver Temperature',/,
     &            'Line 2 : CX rate for T receiver / T donor pairs',/,
     &            'Line 3 : CX rate for T donor',
     &            ' set equal to T receiver',/)
C
C---------------------------------------------------------------------
C
      RETURN
      END
