C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas505/e5outg.for,v 1.3 2004/07/06 13:43:11 whitefor Exp $ Date $Date: 2004/07/06 13:43:11 $
      SUBROUTINE E5OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   IRZ0   , IRZ1   , IDZ0  ,
     &			 CDONOR , CRECVR , CFSTAT,
     &                   AMSRA  , AMSDA  ,
     &                   RMASS  , DMASS  ,
     &                   TREVA  , TDEVA  , QTCXA , ITVAL ,
     &                   TFEVM           , QTCXM , NMX   ,
     &                   LGRD1  , LDEF1  , LFSEL , LDFIT ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED DATA-BLOCK USING GHOST80.
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE IINTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(RATE-COEFF.(cm**3/s)) VERSUS LOG10(TEMP.(eV))
C
C  CALLING PROGRAM: ADAS505
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = REDUNDANT BUT RETAINED FROM IBM VERSION
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK,TYPE and SOURCE
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: RECEIVER -
C                                                 NUCLEAR CHARGE
C  INPUT : (I*4)  IRZ1     = SELECTED DATA-BLOCK: RECEIVER -
C                                                 RECOMBINING ION CHARGE
C  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: DONOR    -
C                                                 NUCLEAR CHARGE
C  INPUT : (C*9)  CDONOR  = DONOR IDENTIFICATION STRING
C  INPUT : (C*9)  CRECVR  = RECEIVER IDENTIFICATION STRING
C  INPUT : (C*10) CFSTAT  = FINAL STATE SPECIFICATION STRING
C  INPUT : (R*8)  AMSRA   = INPUT DATA FILE: RECEIVER ATOMIC MASS FOR
C                           THE DATA-BLOCK BEING ASSESSED.
C  INPUT : (R*8)  AMSDA   = INPUT DATA FILE: DONOR    ATOMIC MASS FOR
C                           THE DATA-BLOCK BEING ASSESSED.
C
C  INPUT : (R*8)  RMASS   = USER ENTERED: RECEIVER ISOTOPIC ATOMIC MASS
C  INPUT : (R*8)  DMASS   = USER ENTERED: DONOR    ISOTOPIC ATOMIC MASS
C
C  INPUT : (R*8)  TREVA() = USER ENTERED: RECEIVER TEMPERATURES (eV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (R*8)  TDEVA() = USER ENTERED: DONOR    TEMPERATURES (eV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (R*8)  QTCXA() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE USER ENTERED RECEIVER/
C                           DONOR TEMPERATURE PAIRS (UNITS: cm**3/sec)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED RECEIVER/DONOR TEMP-
C                           ERATURE PAIRS.
C
C  INPUT : (R*8)  TFEVM() = MINIMAX: SELECTED TEMPERATURES (eV)
C                                    (SEE 'LDFIT' FOR TYPE)
C  INPUT : (R*8)  QTCXM() = RATE-COEFFTS. (cm**3/sec) AT 'TFEVM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX  GENERATED RATE-COEFFT./
C                           TEMPERATURE PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C  INPUT : (L*4)  LDFIT   = .TRUE. =>MINIMAX FIT CARRIED OUT ON  DONOR
C                                    TEMPERATURES - DONOR TEMPERATURES
C                                    TO BE DISPLAYED.
C                           .FALSE.=>MINIMAX FIT CARRIED OUT ON RECEIVER
C                                    TEMPERATURES -RECEIVER TEMPERATURES
C                                    TO BE DISPLAYED.
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (eV)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (eV)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEFFT.(cm**3/s)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEFFT.(cm**3/s)
C
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*25) XTITR   = X-AXIS UNITS/TITLE: RECEIVER
C          (C*25) XTITD   = X-AXIS UNITS/TITLE: DONOR
C          (C*28) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
C
C          (C*26) STRG1   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*26) STRG2   =  DESCRIPTIVE STRING FOR RECOMBINING ION CHGE
C          (C*25) STRG3   =  DESCRIPTIVE STRING ATOMIC MASS NUMBER
C          (C*25) STRG4   =  DESCRIPTIVE STRING ISOTOPIC MASS NUMBER
C          (C*32) HEAD1   =  HEADING FOR RECEIVER INFORMATION
C          (C*32) HEAD2   =  HEADING FOR DONOR    INFORMATION
C          (C*44) HEAD3A  =  FIRST  HEADER FOR RECEIVER/DONOR TEMP. INFO
C          (C*44) HEAD3B  =  SECOND HEADER FOR RECEIVER/DONOR TEMP. INFO
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXLIM8      ADAS      SETTING UP OF DEFAULT GRAPH AXES
C          XXGHDI      ADAS      GHOST80 DESCRIPTION LINE + INTEGER
C          XXGHD8      ADAS      GHOST80 DESCRIPTION LINE + REAL*8
C          XXGH88      ADAS      GHOST80 TABLE LISTING WITH 2 x REAL*8
C          XXGSEL      ADAS      SELECTS POINTS WHICH WILL FIT ON GRAPH
C          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C                      GHOST80   NUMEROUS SUBROUTINES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    30/04/91
C
C UPDATE:  07/08/91 - PE BRIDEN: INTRODUCED SUBROUTINE 'XXGSEL'
C
C UPDATE:  25/11/91 - PE BRIDEN: MADE FILNAM/PICSAV ARGUMENT LIST
C                                COMPATIBLE WITH GHOST VERSION 8.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    21TH MARCH 1996
C
C VERSION: 1.1			DATE: 21-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C 
C VERSION: 1.2			DATE: 02-03-96
C MODIFIED: WILLIAM OSBORN
C	    PROPER HEADER INFORMATION ADDED
C
C VERSION: 1.3			DATE: 10-03-96
C MODIFIED: WILLIAM OSBORN
C	    REDUNDANT VARIABLES REMOVED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    ITVAL           , NMX              ,
     &           IRZ0            , IRZ1             , IDZ0
      INTEGER    I               
C-----------------------------------------------------------------------
      REAL*8     AMSRA           , AMSDA            ,
     &           RMASS           , DMASS            ,
     &           XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST          ,
     &           LGRD1           , LDEF1            , LFSEL     , LDFIT
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       ,  TITLX*120         , TITLM*80  ,
     &           DATE*8
      CHARACTER  GRID*1         , PIC*1             , C3BLNK*3  ,
     &           MNMX0*9        , KEY0*9            , ADAS0*8   ,
     &           DNAME*12       ,
     &           XTITR*25       , XTITD*25          , YTIT*28   ,
     &           CADAS*80       , ISPEC*88	    ,
     &		 CDONOR*9 	, CRECVR*9 	    , CFSTAT*10
      CHARACTER  STRG(4)*28     ,
     &           HEAD1*32       , HEAD2*32          ,
     &           HEAD3A*44      , HEAD3B*44
C-----------------------------------------------------------------------
      REAL*8     TREVA(ITVAL)   , TDEVA(ITVAL)      , QTCXA(ITVAL)     ,
     &           TFEVM(NMX)                         , QTCXM(NMX)
C-----------------------------------------------------------------------
      CHARACTER  KEY(2)*28
C-----------------------------------------------------------------------
      SAVE       ISPEC          , CADAS
C-----------------------------------------------------------------------
      DATA ISPEC(1:48)
     &           /'CHARGE-EXCHANGE RATE COEFFT VERSUS TEMPERATURE: '/
      DATA DNAME /'      DATE: '/
      DATA XTITR /'RECEIVER TEMPERATURE (EV)'/
      DATA XTITD /' DONOR TEMPERATURE  (EV) '/
      DATA YTIT  /'RATE-COEFFICIENT (CM**3/SEC)'/
      DATA ADAS0 /'ADAS   :'/
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(CROSSES/FULL LINE - SPLINE)'/  ,
     &     KEY(2)/'  (DASH LINE - MINIMAX)     '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/   ,
     &     C3BLNK/'   '/ ,
     &     CADAS /' '/
      DATA HEAD1 /'----- RECEIVER INFORMATION -----'/              ,
     &     HEAD2 /'-- NEUTRAL DONOR  INFORMATION --'/              ,
     &     HEAD3A/'- RECEIVER/DONOR  TEMPERATURE RELATIONSHIP -' / ,
     &     HEAD3B/'INDEX   RECEIVER TEMP.(EV)  DONOR TEMP.(EV) ' /
      DATA STRG(1) /' STATE                  = '/ ,
     &     STRG(2) /' FINAL RECEIVER STATE   = '/ ,
     &     STRG(3) /' ATOMIC MASS NUMBER     = '/ ,
     &     STRG(4) /' ISOTOPIC ATOMIC MASS    = '/
C-----------------------------------------------------------------------
C
      INTEGER PIPEIN, PIPEOU, ZERO, ONE
      PARAMETER (PIPEIN=5,PIPEOU=6, ONE=1, ZERO=0)
C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IRZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IRZ1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IDZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A9)') CDONOR
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A9)') CRECVR
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A10)') CFSTAT
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) AMSRA
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) AMSDA
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) RMASS
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) DMASS
      CALL XXFLSH(PIPEOU)
C
      WRITE(PIPEOU,*) ITVAL
      CALL XXFLSH(PIPEOU)

      DO  1 I = 1,ITVAL
         WRITE(PIPEOU,*) TREVA(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
      DO  2 I = 1,ITVAL
         WRITE(PIPEOU,*) TDEVA(I)
         CALL XXFLSH(PIPEOU)
 2    CONTINUE
      DO 3 I = 1,ITVAL
         WRITE(PIPEOU,*) QTCXA(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMAX
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMAX
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
         IF (LDFIT) THEN 
	    WRITE(PIPEOU,*) ONE
	    CALL XXFLSH(PIPEOU)
	 ELSE 
            WRITE(PIPEOU,*) ZERO
	    CALL XXFLSH(PIPEOU)
         ENDIF
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
	    WRITE(PIPEOU,*) QTCXM(I)
	    CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO 5 I = 1, NMX
            WRITE(PIPEOU,*) TFEVM(I)
	    CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF

      IF (LDFIT) THEN 
	 WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
      ELSE
	 WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      WRITE(PIPEOU,'(A28)') (STRG(I), I=1,4)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A45)') HEAD3A
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A45)') HEAD3B
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
C-----------------------------------------------------------------------
      END
