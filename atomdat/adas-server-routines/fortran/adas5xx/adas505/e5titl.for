C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas505/e5titl.for,v 1.5 2004/07/06 13:44:13 whitefor Exp $ Date $Date: 2004/07/06 13:44:13 $

      SUBROUTINE E5TITL( IBSEL  , DSFULL ,
     &                   CDONOR , CRECVR , CFSTAT,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS505/SQTCX
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*80) DSFULL   = FULL INPUT DATA SET NAME
C
CX  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: RECEIVER -
CX                                                 NUCLEAR CHARGE
CX  INPUT : (I*4)  IRZ1     = SELECTED DATA-BLOCK: RECEIVER -
CX                                                 RECOMBINING ION CHARGE
CX  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: DONOR    -
CX                                                 NUCLEAR CHARGE
C
C  INPUT : (C*9)  CDONOR   = SELECTED DATA-BLOCK: DONOR IDENTITY
C
C  INPUT : (C*9)  CRECVR   = SELECTED DATA-BLOCK: RECEIVER IDENTITY
C
C  INPUT : (C*10) CFSTAT  = SELECTED DATA-BLOCK: FINAL STATE SPEC.
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    01/05/91
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    21TH MARCH 1996
C
C VERSION: 1.1			DATE: 21-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. CONVERTED TO NEW DATA FORMAT
C 
C VERSION: 1.2			DATE: 02-03-96
C MODIFIED: WILLIAM OSBORN
C	    PROPER HEADER INFORMATION ADDED
C
C VERSION: 1.3			DATE: 10-03-96
C MODIFIED: WILLIAM OSBORN
C	    REDUNDANT VARIABLES REMOVED
C
C VERSION: 1.4			DATE: 10-03-96
C MODIFIED: WILLIAM OSBORN
C	    REDUNDANT COMMENTS REMOVED
C
C VERSION: 1.5			DATE: 22-04-96
C MODIFIED: TIM HAMMOND
C           - INCREASED DSFULL TO *80
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL           , LEN_NAME	,
     &		 IFIRST		 , ILAST	  
C-----------------------------------------------------------------------
      CHARACTER  C2*2
      CHARACTER  DSFULL*80       , TITLX*120
      CHARACTER  CDONOR*9	 , CRECVR*9	, CFSTAT*10
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      CALL XXSLEN(DSFULL,IFIRST,ILAST)
      LEN_NAME=ILAST-IFIRST+1
      WRITE(C2,1000) IBSEL
      TITLX(1:LEN_NAME+9) = DSFULL(IFIRST:ILAST)//' BLK='//C2//'; '
      TITLX(LEN_NAME+10:LEN_NAME+56) = ' DONOR= '//CDONOR//' RECVR= '//
     & CRECVR//' STATE= '//CFSTAT
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
