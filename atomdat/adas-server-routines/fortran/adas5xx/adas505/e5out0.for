C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas505/e5out0.for,v 1.3 2004/07/06 13:43:00 whitefor Exp $ Date $Date: 2004/07/06 13:43:00 $

       SUBROUTINE E5OUT0( IWRITE   , LDFIT  , LFSEL ,
     &                    TITLE    , TITLX  , TITLM , DATE   ,
     &                    IBSEL    , ITVAL  ,
     &   	          CDONOR   , CRECVR , CFSTAT,
     &                    IRZ0     , IRZ1   , IDZ0  ,
     &                    AMSRA    , AMSDA  ,
     &                    RMASS    , DMASS  ,
     &                    LTRRNG   , LTDRNG ,
     &                    TREVA    , TDEVA  ,
     &                    TRKEL    , TDKEL  ,
     &                    QTCXA    ,
     &                    KPLUS1   , COEF   , CADAS
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED RECEIVER/DONOR DATA-
C            BLOCK UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS505
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LDFIT   = .TRUE. =>MINIMAX FIT CARRIED OUT ON  DONOR
C                                    TEMPERATURES IF FITTED.
C                           .FALSE.=>MINIMAX FIT CARRIED OUT ON RECEIVER
C                                    TEMPERATURES IF FITTED.
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM IDL PIPE)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
C                           SET NAME + OTHER INFO
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IBSEL   = DATA-BLOCK INDEX SELECTED FROM INPUT DATASET
C                           FOR ANALYSIS.
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED RECEIVER/DONOR TEMP-
C                           ERATURE PAIRS.
C  INPUT : (C*9)  CDONOR  = SELECTED DATA-BLOCK: DONOR IDENTITY
C
C  INPUT : (C*9)  CRECVR  = SELECTED DATA-BLOCK: RECEIVER IDENTITY
C
C  INPUT : (C*10) CFSTAT  = SELECTED DATA-BLOCK: FINAL STATE SPEC.
C
C  INPUT : (I*4)  IRZ0    = SELECTED DATA-BLOCK: RECEIVER -
C                                                NUCLEAR CHARGE
C  INPUT : (I*4)  IRZ1    = SELECTED DATA-BLOCK: RECEIVER -
C                                                RECOMBINING ION CHARGE
C  INPUT : (I*4)  IRZ0    = SELECTED DATA-BLOCK: DONOR    -
C                                                NUCLEAR CHARGE
C
C  INPUT : (R*8)  AMSRA   = INPUT DATA FILE: RECEIVER ATOMIC MASS FOR
C                           THE DATA-BLOCK BEING ASSESSED.
C  INPUT : (R*8)  AMSDA   = INPUT DATA FILE: DONOR    ATOMIC MASS FOR
C                           THE DATA-BLOCK BEING ASSESSED.
C
C  INPUT : (R*8)  RMASS   = USER ENTERED: RECEIVER ISOTOPIC ATOMIC MASS
C  INPUT : (R*8)  DMASS   = USER ENTERED: DONOR    ISOTOPIC ATOMIC MASS
C
C  INPUT : (L*4)  LTRRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C  INPUT : (L*4)  LTDRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C
C  INPUT : (R*8)  TREVA() = USER ENTERED: RECEIVER TEMPERATURES (eV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (R*8)  TDEVA() = USER ENTERED: DONOR    TEMPERATURES (eV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C  INPUT : (R*8)  TRKEL() = USER ENTERED: RECEIVER TEMPERATURES (kelvin)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  INPUT : (R*8)  TDKEL() = USER ENTERED: DONOR    TEMPERATURES (kelvin)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C  INPUT : (R*8)  QTCXA() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE USER ENTERED RECEIVER/
C                           DONOR TEMPERATURE PAIRS (UNITS: cm**3/sec)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  IRZ     = RECEIVER - RECOMBINED ION CHARGE
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1R     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT RECEIVER TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*1)  C1D     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT DONOR    TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*2)  XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)  CRESYM  = RECEIVER ELEMENT SYMBOL
C          (C*2)  CDESYM  = DONOR    ELEMENT SYMBOL
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CRELEM  = RECEIVER ELEMENT NAME
C          (C*12) CDELEM  = DONOR    ELEMENT NAME
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          XFESYM     ADAS      CHARACTER*2  FUNCTION -
C                               RETURNS ELEMENT SYMBOL FOR GIVEN Z0
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C
C AUTHOR  : PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:    21/02/91
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    20TH MARCH 1996
C
C VERSION: 1.1			DATE: 20-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C 
C VERSION: 1.2			DATE: 02-03-96
C MODIFIED: WILLIAM OSBORN
C	    PROPER HEADER INFORMATION ADDED
C
C VERSION: 1.3			DATE: 09-08-96
C MODIFIED: WILLIAM OSBORN
C	    ADDED COMMA TO FORMAT STATEMENT 1003
C
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IBSEL         , ITVAL     ,
     &           IRZ0          , IRZ1          , IDZ0      ,
     &           KPLUS1
      INTEGER    IRZ           , I
      INTEGER    IFIRST        , ILAST
C----------------------------------------------------------------------
      REAL*8     AMSRA         , AMSDA         ,
     &           RMASS         , DMASS
C----------------------------------------------------------------------
      LOGICAL    LDFIT         , LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)     , TITLM*(*) , DATE*8  ,
     &           CDONOR*9      , CRECVR*9      , CFSTAT*10
      CHARACTER  XFESYM*2      , XFELEM*12     ,
     &           C1R*1         , C1D*1         ,
     &           CRESYM*2      , CDESYM*2      ,
     &           CRELEM*12     , CDELEM*12     ,
     &           CADAS*80
      CHARACTER  CSTRNG*80
C----------------------------------------------------------------------
      REAL*8     TREVA(ITVAL)  , TDEVA(ITVAL)  ,
     &           TRKEL(ITVAL)  , TDKEL(ITVAL)  ,
     &           QTCXA(ITVAL)  ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRRNG(ITVAL) , LTDRNG(ITVAL)
C----------------------------------------------------------------------
C      SAVE       CADAS
C----------------------------------------------------------------------
C      DATA       CADAS /' '/
C----------------------------------------------------------------------
      DATA       CSTRNG/'BLK'/

C**********************************************************************
C
      CRELEM = XFELEM( IRZ0 )
      CDELEM = XFELEM( IDZ0 )
      CRESYM = XFESYM( IRZ0 )
      CDESYM = XFESYM( IDZ0 )
      IRZ    = IRZ1 - 1

C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &                 'CHARGE EXCHANGE RATE-COEFFICIENT INTERROGATION',
     &                 'ADAS505' , DATE
      WRITE(IWRITE,1002) TITLE

C FIND LENGTH OF FILENAME :
      CALL XXFCHR(TITLX(1:80),CSTRNG,IFIRST,ILAST)

      WRITE(IWRITE,1003) TITLX(1:IFIRST-1) , IBSEL

C NEW DATA FORMAT. FOLLOWING LINE NOT NEEDED
C      WRITE(IWRITE,1004) SOURCA

      WRITE(IWRITE,1005) CRECVR , CDONOR ,
     &                   CRELEM , CDELEM ,
     &                   CRESYM , CDESYM ,
     &                   IRZ0   , IDZ0   ,
     &                   IRZ1   ,
     &                   IRZ    ,
     &                   CFSTAT ,
     &                   AMSRA  , AMSDA  ,
     &                   RMASS  , DMASS
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES AND RATE COEFFICIENTS
C---------------------------------------------------------------------
C
      WRITE (IWRITE,1006)
C
         DO 1 I=1,ITVAL
            C1R = '*'
            C1D = '*'
            IF (LTRRNG(I)) C1R = ' '
            IF (LTDRNG(I)) C1D = ' '
            WRITE(IWRITE,1007) TRKEL(I)  , C1R , TREVA(I) ,
     &                         TDKEL(I)  , C1D , TDEVA(I) ,
     &                         QTCXA(I)
    1    CONTINUE
C
      WRITE (IWRITE,1008)
      WRITE (IWRITE,1009)
      WRITE (IWRITE,1008)
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
               IF (LDFIT) THEN
                  WRITE(IWRITE,1010)
               ELSE
                  WRITE(IWRITE,1011)
               ENDIF
C
            WRITE (IWRITE,1008)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1012) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1013) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1014) TITLM
            WRITE(IWRITE,1008)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(A79)
 1001 FORMAT(//,15('*'),' TABULAR OUTPUT FROM ',A46,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,15('*')/)
 1002 FORMAT(/ ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'CHARGE EXCHANGE RATE-COEFFICIENTS AS A FUNCTION OF ',
     & 'RECEIVER/DONOR TEMPERATURE'/
     & 2X,77('-')/
     & 21X,'DATA GENERATED USING PROGRAM: ADAS505'/21X,37('-'))
 1003 FORMAT(1H ,'FILE : ',(1A),' - DATA-BLOCK: ',I2)
C NEW DATA FORMAT. FOLLOWING LINE NOT NEEDED
C 1004 FORMAT(/,'SOURCE: ',A26)
 1005 FORMAT(/,31X,'RECEIVER',6X,'NEUTRAL DONOR'/
     &       1H ,31X,8('-'),6X,13('-')/
     &       1H ,'                             ', 2X,A9  ,  5X,A9  /
     &       1H ,'ELEMENT NAME                =', 2X,A12 ,  2X,A12 /
     &       1H ,'ELEMENT SYMBOL              =', 2X,A2  , 12X,A2  /
     &       1H ,'NUCLEAR CHARGE         (Z0) =', 2X,I6  ,  8X,I6  /
     &       1H ,'RECOMBINING ION CHARGE (Z1) =', 2X,I6  , 13X,'-' /
     &       1H ,'RECOMBINED ION CHARGE  (Z)  =', 2X,I6  , 13X,'-' /
     &       1H ,'FINAL STATE SPECIFICATION   =', 2X,A10 ,  9X,'-' /
     &       1H ,'ATOMIC MASS NUMBER          =', 2X,F6.2,  8X,F6.2/
     &       1H ,'ISOTOPIC ATOMIC MASS NUMBER =', 2X,F6.2,  8X,F6.2)
 1006 FORMAT(/ /,'--- RECEIVER TEMPERATURE ---',3X,
     &               '- NEUTRAL DONOR TEMPERATURE -',3X,
     &               'RATE COEFFICIENT'/
     &           1H ,2(6X,'kelvin',8X,'eV',9X),5X,'cm**3/sec')
 1007 FORMAT(1H ,1P,2(3X,D10.3,1X,A,D10.3,6X),4X,D10.3)
 1008 FORMAT(79('-'))
 1009 FORMAT('NOTE: * => RATE-COEFFICIENT EXTRAPOLATED FOR THIS ',
     &                     'TEMPERATURE VALUE')
C
 1010 FORMAT (1H / / ,'MINIMAX POLYNOMIAL FIT TO DONOR TEMPERATURES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(RATE-COEFFICIENT<cm**3/sec>) versus ',
     &                'LOG10(DONOR TEMPERATURE<eV>) -')
 1011 FORMAT (/ / ,'MINIMAX POLYNOMIAL FIT TO RECEIVER TEMPERATURES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(RATE-COEFFICIENT<cm**3/sec>) versus ',
     &                'LOG10(RECEIVER TEMPERATURE<eV>) -')
 1012 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1013 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1014 FORMAT ( / ,A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
