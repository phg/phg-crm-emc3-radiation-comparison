C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas505/e5ispf.for,v 1.2 2004/07/06 13:42:46 whitefor Exp $ Date $Date: 2004/07/06 13:42:46 $
      SUBROUTINE E5ISPF( IPAN   , LPEND  ,
     &                   NSTORE , NTRDIM , NTDDIM , NDTIN  ,
     &                   NBSEL  ,
     &                   IRZ0   , IRZ1   , IDZ0   ,
     &                   CDONOR , CRECVR , CFSTAT ,
     &                   AMSRA  , AMSDA  ,
     &                   ITRA   , ITDA   ,
     &                   TRVALS , TDVALS ,
     &                   TITLE  ,
     &                   IBSEL  , RMASS  , DMASS , LDFIT ,
     &                   ITTYP  , ITVAL  , TRIN  , TDIN  ,
     &                   LCCD   , LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1   , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS505
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NSTORE   = MAXIMUM NUMBER OF INPUT FILE DATA-BLOCKS/
C                             (RECEIVER/DONOR COMBINATIONS).
C  INPUT : (I*4)   NTRDIM   = MAX. NO. OF RECEIVER TEMPERATURES ALLOWED
C  INPUT : (I*4)   NTDDIM   = MAX. NO. OF DONOR    TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDTIN    = MAXIMUM NUMBER OF INPUT TEMPERATURE PAIRS
C                             ALLOWED.
C
C  INPUT : (I*4)   NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C
C  INPUT : (I*4)   IRZ0()   = NUCLEAR CHARGE OF RECEIVING IMPURITY ION -
C                             READ FROM SELECTED DATA-BLOCK.
C                             DIMENSION: DATA-BLOCK INDEX.
C  INPUT : (I*4)   IRZ1()   = INITIAL CHARGE OF RECEIVER -
C                             READ FROM SELECTED DATA-BLOCK.
C                             DIMENSION: DATA-BLOCK INDEX.
C  INPUT : (I*4)   IDZ0()   = NUCLEAR CHARGE OF NEUTRAL DONOR -
C                             READ FROM SELECTED DATA-BLOCK.
C                             DIMENSION: DATA-BLOCK INDEX.
C
C  INPUT : (C*9)   CDONOR() = READ - DONOR ION IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*9)   CRECVR() = READ - RECEIVER ION IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*10)  CFSTAT() = READ - FINAL STATE SPECIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)   AMSRA()  = INPUT FILE - RECEIVER ATOMIC MASS
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)   AMSDA()  = INPUT FILE - DONOR    ATOMIC MASS
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (I*4)   ITRA()   = INPUT FILE - NO. OF RECEIVER TEMPERATURES
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (I*4)   ITDA()   = INPUT FILE - NO. OF DONOR    TEMPERATURES
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)  TRVALS(,,)= INPUT DATA FILE: RECEIVER TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)  TDVALS(,,)= INPUT DATA FILE: DONOR    TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   IBSEL    = SELECTED DATA-BLOCK INDEX FOR ANALYSIS
C  OUTPUT: (R*8)   RMASS    = RECEIVER ISOTOPIC ATOMIC MASS NUMBER
C  OUTPUT: (R*8)   DMASS    = DONOR    ISOTOPIC ATOMIC MASS NUMBER
C  OUTPUT: (L*4)   LDFIT    = .TRUE.  => FIT AND DISPLAY DONOR    TEMPS.
C                             .FALSE. => FIT AND DISPLAY RECEIVER TEMPS.
C
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)   ITVAL    = NUMBER OF INPUT TEMPERATURE PAIRS (1->20)
C  OUTPUT: (R*8)   TRIN()   = USER ENTERED VALUES -
C                             RECEIVER TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C  OUTPUT: (R*8)   TDIN()   = USER ENTERED VALUES -
C                             DONOR    TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C  OUTPUT: (L*4)   LCCD     = FLAG FOR CCD OUTPUT OPTION (NOT YET IMPLEMENTED)
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  CALLS XXFLSH COMMAND TO CLEAR PIPE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    27/02/91
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C          NO CHANGES FROM IBM VERSION
C
C DATE:    18TH MARCH 1996
C
C VERSION: 1.1			DATE: 18-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C 
C VERSION: 1.2			DATE: 02-03-96
C MODIFIED: WILLIAM OSBORN
C	    PROPER HEADER INFORMATION ADDED
C
C

C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IPAN          ,
     &           NSTORE        , NTRDIM        , NTDDIM       , NDTIN  ,
     &           NBSEL         , IBSEL         , ITTYP        , ITVAL
C-----------------------------------------------------------------------
      REAL*8     RMASS         , DMASS         , TOLVAL       ,
     &           XL1           , XU1           , YL1          , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , LDFIT         , LCCD         ,
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    IRZ0(NSTORE)  , IRZ1(NSTORE)  , IDZ0(NSTORE) ,
     &           ITRA(NSTORE)  , ITDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     AMSRA(NSTORE)            , AMSDA(NSTORE)     ,
     &           TRIN(NDTIN)              , TDIN(NDTIN)
      REAL*8     TRVALS(NTRDIM,3,NSTORE)  , TDVALS(NTDDIM,3,NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CDONOR(NSTORE)*9      , CRECVR(NSTORE)*9    ,
     &           CFSTAT(NSTORE)*10
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
      INTEGER    PIPEIN        , PIPEOU   ,
     &           I             , J        , K                        ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C
	WRITE(PIPEOU,*) NSTORE
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NTRDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NTDDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NBSEL
	CALL XXFLSH(PIPEOU)
        DO 1, I=1, NBSEL
	    WRITE(PIPEOU,'(A9)') CDONOR(I)
	    CALL XXFLSH(PIPEOU)
1       CONTINUE
        DO 2, I=1, NBSEL
	    WRITE(PIPEOU,'(A9)') CRECVR(I)
	    CALL XXFLSH(PIPEOU)
2       CONTINUE
        DO 3, I=1, NBSEL
	    WRITE(PIPEOU,'(A10)') CFSTAT(I)
	    CALL XXFLSH(PIPEOU)
3       CONTINUE
        DO 5, I=1, NBSEL
	    WRITE(PIPEOU,*) AMSRA(I)
	    CALL XXFLSH(PIPEOU)
5       CONTINUE
        DO 6, I=1, NBSEL
	    WRITE(PIPEOU,*) AMSDA(I)
	    CALL XXFLSH(PIPEOU)
6       CONTINUE
        DO 15, I=1, NBSEL
	    WRITE(PIPEOU,*) ITRA(I)
	    CALL XXFLSH(PIPEOU)
15      CONTINUE
        DO 16, I=1, NBSEL
	    WRITE(PIPEOU,*) ITDA(I)
	    CALL XXFLSH(PIPEOU)
16      CONTINUE
        DO 9, K=1, NBSEL
            DO 8, J=1, 3
                DO 7, I=1, ITRA(K)
	            WRITE(PIPEOU,'(E12.4)') TRVALS(I,J,K)
	            CALL XXFLSH(PIPEOU)
7               CONTINUE
8           CONTINUE
9       CONTINUE
        DO 12, K=1,NBSEL
           DO 11, J=1, 3
              DO 10, I=1, ITDA(K)
                 WRITE(PIPEOU,'(E12.4)') TDVALS(I,J,K)
                 CALL XXFLSH(PIPEOU)
 10           CONTINUE
 11        CONTINUE
 12     CONTINUE
C
C
C  READ OUTPUTS FROM PIPE FROM IDL
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
C
	READ(PIPEIN,'(A40)') TITLE
C
	READ(PIPEIN,*) IBSEL
C CHANGE IDL INDEX TO FORTRAN INDEX BY ADDING 1
        IBSEL = IBSEL+1
C
        READ(PIPEIN,*) RMASS
        READ(PIPEIN,*) DMASS
	READ(PIPEIN,*) ITTYP
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 0) THEN 
	   LDFIT = .FALSE.
        ELSE 
           LDFIT = .TRUE.
        ENDIF
	READ(PIPEIN,*) ITVAL
	READ(PIPEIN,*) (TRIN(I), I=1,NDTIN)
	READ(PIPEIN,*) (TDIN(I), I=1,NDTIN)
C
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 1) THEN
	   LFSEL = .TRUE.
	ELSE
	   LFSEL = .FALSE.
	END IF
C
	READ(PIPEIN,*) TOLVAL
	TOLVAL = TOLVAL / 100.0
C
C-----------------------------------------------------------------------
C


      RETURN
      END
