C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9titl.for,v 1.1 2004/07/06 13:47:23 whitefor Exp $ DATE $Date: 2004/07/06 13:47:23 $
C
      SUBROUTINE E9TITL( IBSEL  , DSFULL ,
     &                   CDONOR , CRECVR ,
     &                   CFSTAT , CTYPE ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS509/SSCX
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*80) DSFULL   = FULL MVS INPUT DATA SET NAME
C
C  INPUT : (C*9)  CDONOR   = SELECTED DATA-BLOCK: DONOR
C                                                 IDENTIFICATION.
C  INPUT : (C*9)  CRECVR   = SELECTED DATA-BLOCK: RECEIVER
C                                                 IDENTIFICATION.
C
C  INPUT : (C*10) CFSTAT   = SELECTED DATA-BLOCK: FINAL STATE
C                                                 SPECIFICATION
C  INPUT : (C*2)  CTYPE    = SELECTED DATA-BLOCK: CROSS. SECT. TYPE
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C	   (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C	   (I*4)  LEN_NAME = LENGTH OF FILENAME
C          (I*4)  IFIRST   = POSITION OF FIRST CHARACTER IN FILENAME
C          (I*4)  ILAST    = POSITION OF LAST CHARACTER IN FILENAME
C
C ROUTINES: 
C          XXSLEN          = UTILITY ROUTINE WHICH FINDS FIRST AND LAST
C                            NON-BLANK CHARACTERS IN A STRING.
C
C AUTHOR:  H. P. SUMMERS
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    11/03/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSFULL*80       ,
     &           CDONOR*9        , CRECVR*9       ,
     &           CFSTAT*10       , CTYPE*2        , TITLX*120
      CHARACTER  C2*2
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
         CALL XXSLEN(DSFULL,IFIRST,ILAST)
	 LEN_NAME = ILAST-IFIRST+1
         IF (LEN_NAME.GT.80) IFIRST = ILAST - 80
         LEN_NAME = ILAST-IFIRST+1
         TITLX(1:LEN_NAME+1) = DSFULL(IFIRST:ILAST)
         POS_NOW = LEN_NAME+1         
         WRITE(C2,1000) IBSEL
         TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
         POS_NOW = POS_NOW+9
         TITLX(POS_NOW:120) =  ' <' // CDONOR //'>,<'// CRECVR //
     &                  '>,<' // CFSTAT //  '>' 
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
      RETURN
      END
