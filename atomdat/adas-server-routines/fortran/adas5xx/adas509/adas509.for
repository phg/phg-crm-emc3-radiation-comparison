C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/adas509.for,v 1.7 2006/12/15 14:49:30 mog Exp $ DATE $Date: 2006/12/15 14:49:30 $

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS509 **********************
C
C  ORIGINAL NAME: (NONE)
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF LOW ENERGY CHARGE EXCHANGE CROSS-SECTIONS.  OPTIONALLY
C            A THERMAL AVERAGED TCX DATA SET MAY BE PRODUCED.
C
C            THE SELECTED INPUT DATA SET IS FOR A DONOR/RECEIVER PAIR.
C            THE FILE CONTAINS FOR  THIS DONOR / RECEIVER COMBINATION
C            A NUMBER OF BLOCKS OF DATA REPRESENTING DIFFERENT  CROSS-
C            SECTIONS. A TABLE OF  CROSS-SECTIONS ARE GIVEN FOR
C            SPECIFIED RELATIVE COLLISION ENERGIES.  THE PROGRAM ALLOWS
C            THE EXAMINATION  AND DISPLAY OF A SELECTED CROSS-SECTION
C            AS FOLLOWS:
C
C            TABULATED XSECTS. FOR SELECTED FINAL STATE ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C            IF REQUESTED A THERMALLY AVERAGED TCX DATA FILE OF TYPE 
C            ADF19 MAY BE OUTPUT TO FILE.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.SCX#<DONOR>.DATA(<CODE>#<EL><Z>)'
C
C            WHERE <EL> REPRESENTS THE RECEIVER ION ELEMENT SYMBOL
C                       E.G. BE, C
C                  <Z>  REPRESENTS THE RECEIVER ION CHARGE  STATE
C                       E.G. 2 , 1
C
C            EACH DATA SET MEMBER IS A ONE WAY TABLE OVER  COLLISION
C            ENERGY.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            COLLISION ENERGIES    : EV/AMU
C            CROSS-SECTIONS        : CM2
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NEDIM   = PARAMETER = MAXIMUM NUMBER OF COLLISION ENER-
C                                       GIES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF USER ENTERED TEMP-
C                                       ERATURES FOR THE TCX FILE.
C          (I*4)  NDEIN   = PARAMETER = MAX. NO. OF USER ENTERED COLL-
C                                       ISION ENERGIES.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR TEXT RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR SCX DATA
C          (I*4)  IUNT11  = PARAMETER = OUTPUT UNIT FOR TCX DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED CROSS-SECTION
C                           / ENERGY  FOR GRAPHICAL OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ0     = INPUT FILE: RECEIVER ION  - NUCLEAR CHARGE
C          (I*4)  IZ      = INPUT FILE: RECEIVED ION  - CHARGE
C          (I*4)  IZ1     = INPUT FILE: RECEIVING ION - CHARGE + 1
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN
C                           FROM SCX SOURCE FILE.
C          (I*4)  IBLK    = SCX DATA-BLOCK CROSS-SECTION INDEX
C          (I*4)  IFORM   = ENERGY FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  IEVAL   = NUMBER OF USER ENTERED COLLISION ENERGIES
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURES FOR TCX
C                           OUTPUT FILE
C          (I*4)  IETYP   = 1 => 'EIN(array)' UNITS: CM S-1
C                         = 2 => 'EIN(array)' UNITS: AT. UNITS
C                         = 3 => 'EIN(array)' UNITS: EV/AMU
C          (I*4)  ITTYP   = 1 => 'TIN(array)' UNITS: DONOR EV
C                         = 2 => 'TIN(array)' UNITS: RECVR EV
C                         = 3 => 'TIN(array)' UNITS: EV/AMU
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C
C          (R*8)  AMD     = DONOR ATOMIC MASS NUMBER
C          (R*8)  AMR     = RECEIVER ATOMIC MASS NUMBER
C          (R*8)  R8ECON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR ENERGIES
C                                  UNITS: SEE 'IETYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR ENERGIES
C                                  UNITS: SEE 'IETYP'
C          (R*8)  XMINPU  = GRAPH: LOWER LIMIT FOR ENERGY (EV/AMU)
C          (R*8)  XMAXPU  = GRAPH: UPPER LIMIT FOR ENERGY (EV/AMU)
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR CROSS-SECTION.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR CROSS-SECTION.
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  OPEN07  = .TRUE.  => FILE ALLOCATED TO UNIT 7.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 7.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED   | NOT USED
C                           .FALSE. => GHOST80 NOT INITIALISED
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => ENERGIES FOR TEXT FILE SET
C                           .FALSE. => ENERGIES.  FOR TEXT FILE NOT SET
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LTSEL   = .TRUE.  => OUTPUT TEMPS. FOR TCX FILE SET
C                           .FALSE. => OUTPUT TEMPS FOR TCX FILE NOT SET
C
CA         (L*4)  L2GRAPH = .TRUE.  => DISPLAY GRAPH
CA                          .FALSE. => DO NOT DISPLAY GRAPH
C
CA         (L*4)  L2FILE  = .TRUE.  => WRITE DATA TO TEXT OUTPUT FILE
CA                          .FALSE. => DO NOT WRITE DATA TO TEXT FILE
CA         (L*4)  L2REP   = .TRUE.  => REPLACE TEXT OUTPUT FILE
CA                          .FALSE. => DO NOT REPLACE TEXT FILE
CA         (L*4)  LPASS   = .TRUE.  => WRITE DATA TO TCX OUTPUT FILE
CA                          .FALSE. => DO NOT WRITE TO TCX OUTPUT FILE
C
CA         (C*80) SAVFIL  = NAME OF TEXT FILE TO WHICH DATA IS WRITTEN.
CA         (C*80) DSNPAS  = NAME OF TCX FILE TO WHICH DATA IS WRITTEN.
C
C          (C*2)  ESYM    = INPUT FILE: RECEIVER ION - ELEMENT SYMBOL
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CA         (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM IDL)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
CA         (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IEA()   = INPUT DATA SET - NUMBER OF COLLISION ENER-
C                           GIES.
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  EIN()   = USER ENTERED COLLISION ENERGIES
C                           (NOTE: UNITS ARE GIVEN BY 'IETYP')
C                           DIMENSION: COLLISION ENERGY INDEX
C          (R*8)  TIN()   = USER ENTERED TEMPERATURES FOR TCX FILE
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: TEMPERATURE INDEX
C          (R*8)  VCMS()  = USER ENTERED COLLISION VELOCITIES
C                           (UNITS: CM S-1)
C          (R*8)  VATU()  = USER ENTERED COLLISION VELOCITIES
C                           (UNITS: AT. UNITS)
C          (R*8)  EEVA()  = USER ENTERED COLLISION ENERGIES
C                           (UNITS: EV/AMU)
C                           DIMENSION: COLLISION ENERGY INDEX
C          (R*8)  EFITM() = MINIMAX: SELECTED ENERGIES (eV/AMU)
C                                    FOR FITTING.
C          (R*8)  SCXA()  = SPLINE INTERPOLATED OR EXTRAPOLATED CROSS-
C                           SECTIONS FOR THE USER ENTERED COLLISION
C                           ENERGIES.
C                           DIMENSION: COLLISION ENERGY INDEX
C          (R*8)  SCXM()  = CROSS-SECTIONS AT 'EFITM()'.
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  ALPH0() = INPUT DATA FILE: LOW ENERGY EXTRA. PARM.
C                             DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TEEA(,) = INPUT DATA SET -
C                           COLLISION ENERGIES (UNITS: eV/AMU)
C                           1st DIMENSION: COLLISION ENERGY INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  EVALS(,,)= INPUT DATA SET - COLLISION ENERGIES
C                            1ST DIMENSION: COLLISION ENERGY INDEX
C                                           ( SEE 'TEEA(,)' )
C                            2ND DIMENSION: 1 => CM S-1  (IETYP=1)
C                                           2 => AT. UN. (IETYP=2)
C                                           3 => EV/AMU  (IETYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TVALS(,,)= INPUT DATA SET - COLLISION ENERGIES
C                            1ST DIMENSION: COLLISION ENERGY INDEX
C                                           ( SEE 'TEEA(,)' )
C                            2ND DIMENSION: 1 => DONOR EV(ITTYP=1)
C                                           2 => RECVR EV(ITTYP=2)
C                                           3 => EV/AMU  (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8)  SCX(,)   =INPUT DATA SET -
C                            FULL SET OF CROSS-SECTION VALUES
C                            1st DIMENSION: COLLSION ENERGY INDEX
C                            2ND DIMENSION: DATA-BLOCK INDEX
C
C          (C*9)  CDONOR() = INPUT DATA FILE: DONOR ION IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*9)  CRECVR() = INPUT DATA FILE: RECEIVER ION IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*10) CFSTAT() = INPUT DATA FILE: FINAL STATE SPECIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CTYPE()  = INPUT DATA FILE: CROSS-SECTION TYPE
C                             DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LERNG()=  .TRUE.  => OUTPUT 'SCXA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      COLLISION ENERGY 'EEVA()'.
C                           .FALSE. => OUTPUT 'SCXA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      COLLISION ENERGY 'EEVA()'.
C                           DIMENSION: COLLISION ENERGY INDEX
C
C          (I*4)  NBSELO   = NUMBER OF DATA-BLOCKS FOR TCX FILE.
C          (I*4)  ISELOA() = TCX DATA FILE - SELECTED DATA-BLOCK: 
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (C*9)  CDONRA() = TCX DATA FILE - SELECTED DATA-BLOCK:
C                            DONOR INDENTIFICATION
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (C*9)  CRECRA() = TCX DATA FILE - SELECTED DATA-BLOCK:
C                            RECEIVER IDENTIFICATION
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (C*10) CFSTTA() = WTCX DATA FILE - SELECTED DATA-BLOCK:
C                            RECEIVER FINAL STATE SPECIFICATION
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (C*2)  CTYPEA() = TCX DATA FILE - SELECTED DATA-BLOCK:
C                            CROSS-SECTION TYPE
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (R*8)  AMRA()   = TCX DATA FILE - RECEIVER ATOMIC MASS
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (R*8)  AMDA()   = TCX DATA FILE - DONOR    ATOMIC MASS
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C          (C*80) DSFLLA() = TCX DATA FILE - SOURCE DATA SET
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C
C
C          (I*4)  ITA()    = TCX DATA FILE - NUMBER OF TEMPERATURES
C                            DIMENSION: OUTPUT DATA-BLOCK INDEX
C
C          (R*8)  TPA(,)   = TCX DATAFILE  - TEMPERATURES (UNITS: EV)
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: OUTPUT DATA-BLOCK INDEX
C
C  INPUT : (R*8)  QFTEQA(,)= TCX - EQUAL TEMPERATURE RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1ST DIMENSION: RECEIVER TEMPERATURE INDEX
C                            2ND DIMENSION: OUTPUT DATA-BLOCK INDEX
C  INPUT : (R*8)  QFTCXA(,,)=TCX - FULL SET OF RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1ST DIMENSION: DONOR    TEMPERATURE INDEX
C                            2ND DIMENSION: RECEIVER TEMPERATURE INDEX
C                            3RD DIMENSION: OUTPUT DATA-BLOCK INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
CA UNIX PORT - THOSE ROUTINES MARKED WITH 'CX' ARE NOW REPLACED BY NEW
CA 		ROUTINES COMMUNICATING WITH IDL SCREENS INSTEAD
CX         E9SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
CA         E9SPF0     ADAS      GATHERS INPUT FILE NAMES VIA IDL SCREENS
C          E9DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          E9CHKZ     ADAS      VERIFY EMITTER READ FROM MEMBER NAME OK
CX         E9SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
CA         E9SETP     ADAS      PASSES VARIABLES BACK TO MAIN IDL ROUTINES
CX         E9ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
CA         E9ISPF     ADAS      GATHERS USERS VALUES VIA IDL SCREENS
C          E9SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E9TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
CX         E9OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
CA         E9OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING IDL
C          E9OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
CA	   E9SPF1     IDL-ADAS  OUTPUT/DISPLAY OPTIONS SELECTION SCREEN
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    14/11/95
C
C UPDATE:
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN
C	    - COMMENTED-OUT PROGRAM POSITION MESSAGES
C
C VERSION: 1.3				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN
C	    - MOVED TEST FOR MENU BUTTON AFTER GRAPH TO PROPER PLACE
C
C VERSION: 1.4				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN
C	    - CLOSED IUNT07 AT END OF PROGRAM
C
C VERSION: 1.5				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED WRITE TO I4UNIT
C VERSION: 1.6				DATE: 26-11-97
C MODIFIED: Martin O'Mullane
C           - changed LSPSS to FALSE to interpolate in log space.
C
C VERSION : 1.7				
C DATE    : 15-12-2006
C MODIFIED: Martin O'Mullane
C           - Use xxname() to get producer name.
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NEDIM         , NDEIN      ,
     &           NDTIN        , MAXDEG
      INTEGER    IUNT07       , IUNT10        , IUNT11     , PIPEIN
      INTEGER    NMX          , PIPEOU
      INTEGER    L1           , L2            , L3
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE = 150 , NEDIM  = 24 , NDEIN = 24 ,
     &           NDTIN  = 35  , MAXDEG =  19 )
      PARAMETER( IUNT07 =   7 , IUNT10 = 10 , IUNT11 = 11, PIPEIN = 5 )
      PARAMETER( PIPEOU =   6 , NMX    = 100 )
      PARAMETER( L1     =   1 , L2     = 2  , L3     =  3 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT       , ISTOP
      INTEGER    NBSEL        , IBLK         , IFORM      ,
     &           IZ0          , IZ           , IZ1        ,
     &           IBSEL        , KPLUS1       , 
     &           IEVAL        , IETYP        , 
     &           ITVAL        , ITTYP        , IPAN       ,
     &           ITD          , ITR
      INTEGER    NBSELO
C-----------------------------------------------------------------------
      REAL*8     R8ECON
      REAL*8     XMIN         , XMAX         , XMINPU      , XMAXPU    ,
     &           YMIN         , YMAX         
      REAL*8     TOLVAL       , AMD          , AMR
C-----------------------------------------------------------------------
      LOGICAL    LGHOST       , LGRD1
      LOGICAL    OPEN10       , OPEN11       , OPEN07
      LOGICAL    LPEND        , LDSEL        , LDEF1      ,
     &           LTSEL        , LOSEL        , LFSEL      
      LOGICAL    LGRAPH       , L2FILE       , L2REP
      LOGICAL    LDOUT        , LPASS        , LSETX      , LSPSS
C-----------------------------------------------------------------------
      CHARACTER  ESYM*2       , REP*3        , USER*30    ,
     &           DATE*8       , DSFULL*80    , CADAS*80   ,
     &           TITLE*40     , TITLM*80     , TITLX*120
      CHARACTER  SAVFIL*80    , DSNPAS*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)               ,
     &           IEA(NSTORE)
      INTEGER    ISELOA(NSTORE)              ,
     &           ITA(NSTORE)
      INTEGER    ONE
C-----------------------------------------------------------------------
      REAL*8     ALPH0(NSTORE)
      REAL*8     EIN(NDEIN)                  , TIN(NDTIN)              ,
     &           EEVA(NDEIN)                 , VCMS(NDEIN)             ,
     &           VATU(NDEIN)                 ,
     &           SCXA(NDEIN)                 , SCXM(NMX)               ,
     &           EFITM(NMX)                  , COEF(MAXDEG+1)
      REAL*8     AMRA(NSTORE)                , AMDA(NSTORE)
      REAL*8     TPA(NDTIN,NSTORE)           , RCOUT(NDTIN,NDTIN)      ,
     &           QFTEQA(NDTIN,NSTORE)
      REAL*8     QFTCXA(NDTIN,NDTIN,NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CTYPE(NSTORE)*2             , CRECVR(NSTORE)*9        ,
     &           CFSTAT(NSTORE)*10           , CDONOR(NSTORE)*9
      CHARACTER  DSFLLA(NSTORE)*80
      CHARACTER  CDONRA(NSTORE)*9      , CRECRA(NSTORE)*9    ,
     &           CFSTTA(NSTORE)*10     , CTYPEA(NSTORE)*2
C-----------------------------------------------------------------------
      LOGICAL    LERNG(NDEIN)
C-----------------------------------------------------------------------
      REAL*8     TEEA(NEDIM,NSTORE)
      REAL*8     EVALS(NEDIM,3,NSTORE)       ,
     &           TVALS(NEDIM,3,NSTORE)       ,
     &           SCX(NEDIM,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , IEVAL  /0/       , ITVAL  /0/
      DATA OPEN10 /.FALSE./ , OPEN11/.FALSE./  , OPEN07/.FALSE./
      DATA NBSELO /0/
      DATA ONE    /1/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
      CALL XXNAME(USER)
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
  100 CONTINUE
C      WRITE(I4UNIT(-1),*)'ADAS509.FOR: at LABEL 100' 
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C  SOURCE DATA FILENAME GATHERED FROM IDL INTERFACE
C-----------------------------------------------------------------------
C
      CALL E9SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING TCX FILE - DSNPAS - IF REQUESTED
C & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='UNKNOWN')
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
      CALL E9DATA( IUNT10 , DSFULL ,
     &             NSTORE , NEDIM  ,
     &             ESYM   , IZ0    ,
     &             NBSEL  , ISELA  ,
     &             IZ     , IZ1    ,
     &             CDONOR , CRECVR , CFSTAT , CTYPE ,
     &             ALPH0  ,
     &             IEA    ,
     &             TEEA   ,
     &             SCX
     &           )

C
C-----------------------------------------------------------------------
C SET UP ARRAY 'EVALS(,)' - CONTAINS INPUT ENERGIES. IN 3 FORMS.
C    NOTE: INPUT DATA SET ENERGIES 'TEEA()' IS IN eV/AMU.
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXECON( L3          , IFORM                ,
     &                      IEA(IBLK)   , TEEA(1,IBLK)         ,
     &                                    EVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP IDL FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
      CALL E9SETP( NBSEL  )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES VIA IDL 
C-----------------------------------------------------------------------
C
  200 CONTINUE
C      WRITE(I4UNIT(-1),*)'ADAS509.FOR: at LABEL 200' 
      CALL E9ISPF( IPAN   , LPEND  ,
     &             NSTORE , NEDIM  , NDEIN  , NDTIN  ,
     &             NBSEL  ,
     &             CDONOR , CRECVR , CFSTAT , CTYPE  ,
     &             IEA    ,
     &             EVALS  , TVALS  ,
     &             TITLE  ,
     &             IBSEL  ,
     &             AMD    , AMR    ,
     &             IETYP  , IEVAL  , EIN    ,
     &             ITTYP  , ITVAL  , TIN    ,
     &             LFSEL  , LOSEL  , LTSEL  ,
     &             TOLVAL   
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GO TO 100
C
C-----------------------------------------------------------------------
C CONVERT INPUT ENER/VEL. VALUES INTO EV/AMU     (EIN -> EEVA)
C-----------------------------------------------------------------------
C
          CALL XXECON( IETYP , L1 , IEVAL , EIN , VCMS )
          CALL XXECON( IETYP , L2 , IEVAL , EIN , VATU )
          CALL XXECON( IETYP , L3 , IEVAL , EIN , EEVA )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV/AMU
C-----------------------------------------------------------------------
C
          XMINPU = R8ECON( IETYP , L3 , XMIN )
          XMAXPU = R8ECON( IETYP , L3 , XMAX )
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------
C
          CALL E9SPLN( NEDIM             ,
     &                 IEA(IBSEL)        ,
     &                 IEVAL             ,
     &                 TEEA(1,IBSEL)     ,
     &                 EEVA              ,
     &                 SCX(1,IBSEL)      , SCXA              ,
     &                 LERNG
     &               )
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (IEVAL > 2).
C-----------------------------------------------------------------------
C
          IF (IEVAL.LE.2) LFSEL = .FALSE.
 
             IF (LFSEL) THEN
                  CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                         IEVAL   , EEVA     , SCXA    ,
     &                         NMX     , EFITM    , SCXM    ,
     &                         KPLUS1  , COEF     ,
     &                         TITLM
     &                       )
                 WRITE(I4UNIT(-1),1000) TITLM(1:79)
             ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
          CALL E9TITL( IBSEL         , DSFULL        ,
     &                 CDONOR(IBSEL) , CRECVR(IBSEL) ,
     &                 CFSTAT(IBSEL) , CTYPE(IBSEL)  ,
     &                 TITLX
     &               )
C
C-----------------------------------------------------------------------
C   COMMUNICATE WITH IDL VIA A PIPE TO GET USER SELECTED OUTPUT OPTIONS.
C-----------------------------------------------------------------------
C
  300     CONTINUE
C          WRITE(I4UNIT(-1),*)'ADAS509.FOR: at LABEL 300' 
          CALL E9SPF1( DSFULL        , 
     &                 IBSEL         , LOSEL         , LFSEL    , LDEF1,
     &                 LGRAPH        , L2FILE        , SAVFIL   ,
     &                 LPASS         , DSNPAS        ,
     &                 XMIN          , XMAX          , YMIN     , YMAX ,
     &                 LPEND         , L2REP         , CADAS
     &               )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
C OUTPUT TEXT RESULTS
C----------------------------------------------------------------------
C
      IF (LOSEL.AND.L2FILE) THEN
	  IF(L2REP) THEN
             IF (OPEN07) THEN
                CLOSE(UNIT=IUNT07)
             ENDIF
             OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
             OPEN07 = .TRUE.
          ENDIF
          CALL E9OUT0( IUNT07        , LFSEL        ,
     &                 TITLE         , TITLX        , TITLM       ,
     &                 DATE          , CADAS        ,
     &                 IBSEL         , IEVAL        ,
     &                 ESYM          , IZ0          , IZ          ,
     &                 IZ1           ,
     &                 CDONOR(IBSEL) , CRECVR(IBSEL),
     &                 CFSTAT(IBSEL) , CTYPE(IBSEL) ,
     &                 LERNG         ,
     &                 VCMS          , VATU         , EEVA        ,
     &                 SCXA          ,
     &                 KPLUS1        , COEF
     &               )
       ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT GRAPHICAL RESULTS.
C-----------------------------------------------------------------------
C
       IF (LGRAPH) THEN
          CALL E9OUTG( LGHOST        ,
     &                 TITLE         , TITLX        , TITLM      , DATE,
     &                 ESYM          , IZ0          , IZ         ,
     &                 CDONOR(IBSEL) , CRECVR(IBSEL),
     &                 CFSTAT(IBSEL) , CTYPE(IBSEL) ,
     &                 EEVA          , SCXA         , IEVAL  ,
     &                 EFITM         , SCXM         , NMX    ,
     &                 LGRD1         , LDEF1        , LFSEL      ,
     &                 XMINPU        , XMAXPU       , YMIN       , YMAX
     &               )
       ENDIF
C       WRITE(I4UNIT(-1),*)'ADAS509.FOR: at exit from E9OUTG'
C
C-----------------------------------------------------------------------
C  COMPUTE AND STACK DATA FOR OUTPUT TCX FILE IF REQUIRED
C-----------------------------------------------------------------------
C
       IF (LTSEL.AND.LPASS) THEN
C     
          LSETX = .TRUE.
          LSPSS = .FALSE.
C     
          CALL EXTHER ( NEDIM       , NDTIN         ,
     &                  LSETX       , LSPSS         ,
     &                  AMD         , AMR           , L3     ,
     &                  IEA(IBSEL)  , TEEA(1,IBSEL) , ITVAL  , TIN ,
     &                  SCX(1,IBSEL), RCOUT
     &                 )
C
          NBSELO = NBSELO + 1
          ISELOA(NBSELO) = NBSELO
          ITA(NBSELO) = ITVAL
          AMDA(NBSELO) = AMD
          AMRA(NBSELO) = AMR
          CDONRA(NBSELO) = CDONOR(IBSEL)
          CRECRA(NBSELO) = CRECVR(IBSEL)
          CFSTTA(NBSELO) = CFSTAT(IBSEL)
          DSFLLA(NBSELO) = DSFULL
C     
          DO 10 ITD = 1,ITVAL
             TPA(ITD,NBSELO) = TIN(ITD)
             DO 5 ITR = 1,ITVAL
                QFTCXA(ITD,ITR,NBSELO) = RCOUT(ITD,ITR)
 5           CONTINUE
             QFTEQA(ITD,NBSELO) = QFTCXA(ITD,ITD,NBSELO)
 10       CONTINUE
C
C TELL IDL THAT WE'VE FINISHED CALCULATING
          WRITE(PIPEOU,*)ONE
          LDOUT = .TRUE.
       ENDIF
C       WRITE(I4UNIT(-1),*)'ADAS509.FOR: at exit from EXTHER'
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN
         READ(PIPEIN,*) ISTOP
         IF (ISTOP.EQ.1) GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO ISPF PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
       GOTO 300
C
 9999  CONTINUE
C       WRITE(I4UNIT(-1),*)'ADAS509.FOR: at LABEL 9999' 
       IF (LPASS.AND.LTSEL) THEN
C
          OPEN( UNIT=IUNT11 , FILE=DSNPAS , STATUS='UNKNOWN')
C
          CALL E9WR11( IUNT11   , USER   , DATE   ,
     &                 NSTORE   , NDTIN  ,
     &                 NBSELO   , ISELOA ,
     &                 CDONRA   , CRECRA ,
     &                 CFSTTA   , CTYPEA ,
     &                 DSFLLA   ,
     &                 AMDA     , AMRA   ,
     &                 ITA      ,
     &                 TPA      ,
     &                 QFTEQA   , QFTCXA
     &                )
          CLOSE(IUNT11)
       ENDIF   

       IF (OPEN07) THEN
          CLOSE(IUNT07)
       ENDIF

C TELL IDL THAT WE'VE FINISHED WRITING TCX FILE
       WRITE(PIPEOU,*)ONE
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C
      END
