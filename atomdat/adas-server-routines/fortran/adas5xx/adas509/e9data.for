C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9data.for,v 1.2 2007/07/20 10:45:53 allan Exp $ DATE $Date: 2007/07/20 10:45:53 $
C
       SUBROUTINE E9DATA( IUNIT  , DSNAME ,
     &                    NSTORE , NEDIM  ,
     &                    ESYM   , IZ0    ,
     &                    NBSEL  , ISELA  ,
     &                    IZ     , IZ1    ,
     &                    CDONOR , CRECVR , CFSTAT , CTYPE,
     &                    ALPH0  ,
     &                    IEA    ,
     &                    TEEA   , SCX
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9DATA *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT CHARGE EXCHANGE CROSS-SECTION
C            DATA FOR  GIVEN DONOR AND RECEIVER IONS.
C
C            (MEMBER STORED IN SCX#<DONOR>.DATA (<PREFIX>.#<RECEIVER>)
C             WHERE <DONOR> ='H0' ETC. ; <RECEIVER> ='C2', 'C6' ETC.
C             AND <PREFIX. = <BLANK. OR THREE CHARACTERS
C
C  CALLING PROGRAM: ADAS509/SSCX
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH BLOCK FORMING A COMPLETE SET OF  CROSS-
C           SECTION VALUES FOR GIVEN COLLISION ENERGIES.
C           EACH DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER
C           DATA-BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           COLLISION ENERGIES  : EV/AMU
C           CROSS-SECTION       : CM**2
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*44) DSNAME   = MVS DATA SET NAME OF DATA SET BEING READ
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NEDIM    = MAX NUMBER OF COLLISION ENERGIES ALLOWED
C
C  OUTPUT: (C*2)  ESYM     = READ - RECEIVING ION - ELEMENT SYMBOL
C  OUTPUT: (I*4)  IZ0      = READ - RECEIVING ION - NUCLEAR CHARGE
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IZ       = READ - RECEIVED ION -  CHARGE
C  OUTPUT: (I*4)  IZ1      = READ - RECEIVING ION - CHARGE
C
C  OUTPUT: (C*9)  CDONOR() = READ - DONOR ION IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*9)  CFCODE() = READ - RECEIVER ION IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*10) CFSTAT() = READ - FINAL STATE SPECIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*2)  CTYPE()  = READ - CROSS-SECTION TYPE
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  ALPH0()  = READ - LOW ENERGY EXTRAPOLATION PARM.
C                            DIMENSION: DATA-BLOCK INDEX
C
C
C  OUTPUT: (I*4)  IEA()    = READ - NUMBER OF COLLISION ENERGIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TEEA(,)  = READ - COLLISION ENERGIES (UNITS: eV/AMU)
C                            1st DIMENSION: COLLISION ENERGY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  SCX(,)   =READ - FULL SET OF COLLISION CROSS-
C                                   SECTION VALUES (cm**2)
C                            1st DIMENSION: COLLISION ENERGY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: COLLISION ENERGY INDEX
C          (I*4)  NENUM    = NUMBER OF COLLISION ENERGIES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (R*8)  R8FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*3)  CKEY1    = 'FST'    - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY2    = 'TYPE'   - INPUT BLOCK HEADER KEY
C          (C*5)  CKEY3    = 'ALPH0'  - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL'  - INPUT BLOCK HEADER KEY
C          (C*10) C10      = GENERAL USE TEN BYTE CHARACTER STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      REAL*8 FUNCTION       -
C                               CONVERT CHARACTER STRING TO REAL*8
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    06/06/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 20-07-07
C MODIFIED: Allan Whiteford
C	    - Small modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0                , I4FCTN              , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NEDIM                 , IZ0                 ,
     &           IZ                    , IZ1                 ,
     &           NBSEL
      INTEGER    IBLK                  , ITT                 ,
     &           NENUM                 , IABT                ,
     &           IPOS1                 , IPOS2
C-----------------------------------------------------------------------
      REAL*8     R8FCTN
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*44             , ESYM*2
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*3               , CKEY2*4             ,
     &           CKEY3*5               , CKEY4*4             ,
     &           C10*10                , C80*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         , IEA(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CDONOR(NSTORE)*9      , CRECVR(NSTORE)*9    ,
     &           CFSTAT(NSTORE)*10     , CTYPE(NSTORE)*2
C-----------------------------------------------------------------------
      REAL*8     ALPH0(NSTORE)
      REAL*8     TEEA(NEDIM,NSTORE)    , SCX(NEDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4
C
C-----------------------------------------------------------------------
C
      DATA       CSLASH / '/' /
      DATA       CKEY1  / 'FST'    /   , CKEY2  / 'TYPE'   /  ,
     &           CKEY3  / 'ALPH0'  /   , CKEY4  / 'ISEL'   /
C
C **********************************************************************
C
      LBEND = .FALSE.
C
C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) C80
      READ(C80,* ) NBSEL
C
         IF (NBSEL.GT.NSTORE) THEN
            WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
            NBSEL = NSTORE
         ENDIF
C
C***********************************************************************
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C***********************************************************************
C
         DO 1 IBLK=1,NBSEL
C
C-----------------------------------------------------------------------
C INPUT DONOR/RECEIVER SPECS. AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------
C
            IF (.NOT.LBEND) THEN
                  READ(IUNIT,1000)   C80
C
                  IF(IBLK.EQ.1) THEN
                      IPOS1 = INDEX(C80,'/') + 1
                      IPOS2 = IPOS1          + 1
                      READ(C80(IPOS1:IPOS2),1001) ESYM
C
                      IZ0   = I4EIZ0( ESYM )
                      C2    = C80(14:15)
                      IZ1   = I4FCTN( C2 , IABT )
                      IZ    = IZ1      -1
C
                  ENDIF
C
                  IF ( C80(1:2).NE.'C-') THEN
C
                     CDONOR(IBLK) = C80(1:9)
                     CRECVR(IBLK )= C80( 11:19)
                     IPOS2        = INDEX(C80(20:80),'/') + 19
C
                     READ(C80(20:IPOS2),*) IEA(IBLK)
C
                     CALL XXHKEY( C80 , CKEY1 , CSLASH , CFSTAT(IBLK) )
                     CALL XXHKEY( C80 , CKEY2 , CSLASH , CTYPE(IBLK)  )
                     CALL XXHKEY( C80 , CKEY3 , CSLASH , C10          )
                     CALL XXHKEY( C80 , CKEY4 , CSLASH , C2           )
C
                     ISELA(IBLK) = I4FCTN( C2  , IABT )
                     ALPH0(IBLK) = R8FCTN( C10 , IABT )
                     NENUM       = IEA(IBLK)
C
                         IF (NENUM.GT.NEDIM) THEN
                            WRITE(I4UNIT(-1),2001) DSNAME , IBLK  ,
     &                                            NEDIM   , NENUM
                            STOP
                         ENDIF
C
C-----------------------------------------------------------------------
C INPUT COLLISION ENERGIES, CROSS-SECTIONS FOR BLOCK
C-----------------------------------------------------------------------
C
                     READ(IUNIT,1002) ( TEEA(ITT,IBLK) , ITT=1,NENUM )
C
                     READ(IUNIT,1002) ( SCX(ITT,IBLK)  , ITT=1,NENUM )
C
                  ELSE
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1
                  ENDIF
C
            ENDIF
C
    1    CONTINUE
C
C***********************************************************************
C
 1000 FORMAT(1A80)
 1001 FORMAT(A2)
 1002 FORMAT(6D10.3)
C
 2000 FORMAT(/1X,31('*'),' E9DATA MESSAGE ',31('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'NUMBER OF ELEMENT ION DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' E9DATA ERROR ',32('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT COLLISION ENERGIES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' E9DATA MESSAGE ',31('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'INCONSISTENCY IN THE NUMBER OF DATA-BLOCKS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
