C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9setp.for,v 1.1 2004/07/06 13:47:01 whitefor Exp $ DATE $Date: 2004/07/06 13:47:01 $
C
       SUBROUTINE E9SETP( NBSEL )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9SETP *********************
C
C  PURPOSE:  WRITES THE VALUE OF NBSEL OUT TO IDL
C
C  CALLING PROGRAM: ADAS509
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'E9DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBSEL   = NUMBER OF CHARGE EXCHANGE X-SECTIONS READ IN.
C                           I.E. NUMBER OF DATA-BLOCKS.
C
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    06/03/96
C
C UNIX-IDL PORT:
C
C
C VERSION: 1.1                          DATE: 06-03-96
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBSEL            
      INTEGER    PIPEOU
      PARAMETER ( PIPEOU = 6 )
C-----------------------------------------------------------------------
C
C **********************************************************************
C-----------------------------------------------------------------------
C WRITE NBSEL OUT TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, * ) NBSEL
      CALL XXFLSH( PIPEOU )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
