C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9wr11.for,v 1.2 2006/12/15 14:49:30 mog Exp $ DATE $Date: 2006/12/15 14:49:30 $
C
       SUBROUTINE E9WR11( IUNIT    , USER   , DATE   ,
     &                    NSTORE   , NTDIM  ,
     &                    NBSEL    , ISELA  ,
     &                    CDONRA   , CRECRA ,
     &                    CFSTTA   , CTYPEA ,
     &                    DSFLLA   ,
     &                    AMDA     , AMRA   ,
     &                    ITA      ,
     &                    TPA      ,
     &                    QFTEQA   , QFTCXA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9WR11 *********************
C
C  PURPOSE:  TO  WRITE  DATA  TO THERMAL CHARGE TRANSFER
C            RATE COEFFICIENT PASSING FILE FOR GIVEN DONOR/RECEIVER.
C
C  CALLING PROGRAM: ADAS509
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE WRITTEN TO
C           THE FILE - EACH  BLOCK  FORMING  A  COMPLETE  SET OF  RATE-
C           COEFFICIENTS FOR THE  RECEIVER/DONOR COMBINATION.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           TEMPERATURES        : EV
C           RATE COEFFICIENTS   : CM**3 SEC-1
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*6)  UID      = USER IDENTIFIER OF PRODUCER
C  INPUT : (C*8)  DATE     = DATE
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF DATA-BLOCKS THAT CAN
C                            BE WRITTEN.
C  INPUT : (I*4)  NTDIM    = MAX NUMBER OF RECEIVER & DONOR TEMPERATURES
C                            ALLOWED
C
C  INPUT : (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS WRITTEN
C  INPUT : (I*4)  ISELA()  = WRITE - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*9)  CDONRA() = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                            DONOR INDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*9)  CRECRA() = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                            RECEIVER IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*10) CFSTTA() = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                            RECEIVER FINAL STATE SPECIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*2)  CTYPEA() = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                            CROSS-SECTION TYPE
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)  AMRA     = READ - RECEIVER ATOMIC MASS
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)  AMDA     = READ - DONOR    ATOMIC MASS
C                            DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*44) DSFLLA() = MVS DATA SET NAME OF SOURCE DATA SET
C                            DIMENSION: DATA-BLOCK INDEX
C
C
C  INPUT : (I*4)  ITA()    = READ - NUMBER OF TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)  TPA(,)   = READ - TEMPERATURES (UNITS: EV)
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)  QFTEQA(,)= READ - EQUAL TEMPERATURE RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1st DIMENSION: RECEIVER TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)  QFTCXA(,,)=READ - FULL SET OF RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1st DIMENSION: DONOR    TEMPERATURE INDEX
C                            2nd DIMENSION: RECEIVER TEMPERATURE INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*2)  CEQUAL   = PARAMETER = 'EQ'
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITR      = ARRAY INDEX: RECEIVER TEMPERATURE INDEX
C          (I*4)  ITD      = ARRAY INDEX: DONOR    TEMPERATURE INDEX
C          (I*4)  NTRNUM   = NUMBER OF RECEIVER TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  NTDNUM   = NUMBER OF DONOR    TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C
C
C          (C*10) IONNAM   = READ - DONOR/RECEIVER DESIGNATION STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    17/11/95
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION : 1.2			
C DATE    : 15-12-2006
C MODIFIED: Martin O'Mullane
C           - Replace UID*6 with USER*30, the real name of the producer.
C
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NTDIM                 , NBSEL
      INTEGER    IBLK                  , IT                  ,
     &           ITR                   , ITD 
C-----------------------------------------------------------------------
      CHARACTER  USER*30               , DATE*8
      CHARACTER  CEQUAL*10    
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         ,
     &           ITA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     AMRA(NSTORE)         , AMDA(NSTORE)
      REAL*8     TPA(NTDIM,NSTORE)    ,
     &           QFTEQA(NTDIM,NSTORE)
      REAL*8     QFTCXA(NTDIM,NTDIM,NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  DSFLLA(NSTORE)*80
      CHARACTER  CDONRA(NSTORE)*9      , CRECRA(NSTORE)*9    ,
     &           CFSTTA(NSTORE)*10     , CTYPEA(NSTORE)*2
C-----------------------------------------------------------------------
      DATA       CEQUAL/' *EQUAL**'/
C ----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C WRITE MARKERS FOR NUMBER OF DATA-BLOCKS PRESENT
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1000) NBSEL
C
C
C-----------------------------------------------------------------------
C WRITE OUT DATA FOR EACH OF THE DATA-BLOCKS
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBSEL
C
C----------- TITLE LINE
C
           WRITE(IUNIT,1001)  CDONRA(IBLK)   , CRECRA(IBLK)   ,
     &                        ITA(IBLK)      , ITA(IBLK)      ,
     &                        AMDA(IBLK)     , AMRA(IBLK)     ,
     &                        CFSTTA(IBLK)   , ISELA(IBLK)
C
           WRITE(IUNIT,1002) ( TPA(IT,IBLK)  , IT=1,ITA(IBLK) )
           WRITE(IUNIT,1003) CEQUAL          ,
     &                       ( TPA(IT,IBLK)  , IT=1,MIN0(7,ITA(IBLK)))
           IF(ITA(IBLK).GT.7) WRITE(IUNIT,1002)
     &                        (TPA(IT,IBLK),IT=8,ITA(IBLK))
           DO 2 ITR=1,ITA(IBLK)
             WRITE(IUNIT,1002) QFTEQA(ITR,IBLK)     ,
     &                         (QFTCXA(ITD,ITR,IBLK), ITD=1,ITA(IBLK))
    2      CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
C WRITE OUT COMMENTS SECTION OF DATA-SET
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1004)
C
         DO 3 IBLK = 1,NBSEL
           WRITE(IUNIT,1006)ISELA(IBLK) , CDONRA(IBLK) , CRECRA(IBLK) ,
     &                      CFSTTA(IBLK), DSFLLA(IBLK)(1:80)
    3    CONTINUE
C
      WRITE(IUNIT,1007) USER , DATE
      WRITE(IUNIT,1008)
C
      RETURN
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I5)
 1001 FORMAT(1A9,'/',1A9,2I4,
     &       '/AMD=',F6.1,'/AMR=',F6.1,'/FST=',1A10,'/ISEL=',I5)
 1002 FORMAT(1P,8D9.2)
 1003 FORMAT(1A9,1P,7D9.2)
 1004 FORMAT('C',79('-')/
     &       'C   THERMAL CHARGE EXCHANGE RATE COEFFICIENT SOURCE LIST'/
     &       'C'/
     &       'C   ISEL  DONOR      RECEIVER   FINAL STATE  SOURCE     '/
     &       'C   ----  ---------  ---------  -----------  -----------',
     &       '------------------------')
 1006 FORMAT('C   ',I3,3X,1A9,2X,1A9,2X,1A10,3X,1A80)
 1007 FORMAT('C'/
     &       'C   '/'C'/
     &       'C   Processing code: ADAS509',/
     &       'C   Producer:        ',1A30,/
     &       'C   Date:            ',1A8,/
     &       'C'/
     &       'C',79('-'))
 1008 FORMAT(1A80)
C
C-----------------------------------------------------------------------
C
      END
