C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9vrdc.for,v 1.1 2004/07/06 13:47:26 whitefor Exp $ DATE $Date: 2004/07/06 13:47:26 $
C
      SUBROUTINE E9VRDC( ICIND  ,
     &                   NBSEL  ,
     &                   CDONOR , CRECVR, CFSTAT , CTYPE ,
     &                   SCI    ,
     &                   SDONOR , SRECVR, SFSTAT , STYPE
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9VRDC *********************
C
C  PURPOSE: TO SETUP DONOR/RECVR CHARACTER STRINGS FOR USE WITH SETTING
C           ISPF VARIABLES WHEN  SELECTED CROSS-SECTIONS ARE DISPLAYED.
C
C  CALLING PROGRAM: E9PAN2
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   ICIND    = SELECTED/REQUESTED CROSS-SECTION DATA-BLOCK
C                             INDEX.
C
C  INPUT : (I*4)   NBSEL    = NUMBER OF DATA-BLOCK CROSS-SECTIONS.
C
C  INPUT : (C*9)   CDONOR() = INPUT DATA FILE: DONOR ION IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*9)   CRECVR() = INPUT DATA FILE: RECEIVER ION IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*10)  CFSTAT() = INPUT DATA FILE: FINAL STATE SPECIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*2)   CTYPE()  = INPUT DATA FILE: CROSS-SECTION TYPE
C                             DIMENSION: DATA-BLOCK INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*3)   SCI      = ISPF PANEL VARIABLE: DATA-BLOCK INDEX
C
C  OUTPUT: (C*9)   SDONOR   = ISPF PANEL VARIABLE: DONOR ION IDENTIFICATION
C  OUTPUT: (C*9)   SRECVR   = ISPF PANEL VARIABLE: RECEIVER ION IDENTIFICATION
C  OUTPUT: (C*10)  SFSTAT   = ISPF PANEL VARIABLE: FINAL STATE SPECIFICATION
C  OUTPUT: (C*2)   STYPE    = ISPF PANEL VARIABLE: CROSS-SECTION TYPE
C
C
C ROUTINES: NONE
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    15/11/95
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    ICIND            , NBSEL
C-----------------------------------------------------------------------
      CHARACTER  SCI*3            , STYPE*2          , SRECVR*9        ,
     &           SFSTAT*10        , SDONOR*9
      CHARACTER  CTYPE(NBSEL)*2   , CRECVR(NBSEL)*9  , CFSTAT(NBSEL)*10,
     &           CDONOR(NBSEL)*9
C-----------------------------------------------------------------------
C
         IF (ICIND.EQ.0) THEN
            SCI    = ' '
            STYPE  = ' '
            SRECVR = ' '
            SFSTAT = ' '
            SDONOR = ' '
         ELSE
            WRITE(SCI,1000) ICIND
            STYPE  = CTYPE(ICIND)
            SRECVR = CRECVR(ICIND)
            SFSTAT = CFSTAT(ICIND)
            SDONOR = CDONOR(ICIND)
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I3)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
