C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9outg.for,v 1.1 2004/07/06 13:46:55 whitefor Exp $ DATE $Date: 2004/07/06 13:46:55 $
C
      SUBROUTINE E9OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   ESYM   , IZ0    , IZ    ,
     &                   CDONOR , CRECVR ,
     &                   CFSTAT , CTYPE  ,
     &                   EEVA   , SCXA   , IEVAL ,
     &                   EFITM  , SCXM   , NMX   ,
     &                   LGRD1  , LDEF1  , LFSEL ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9OUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL TO PLOT GRAPH
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE INTERPOLATED POINTS : CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(CROSS-SECTION  (cm**2))
C                                   VERSUS
C                    LOG10(COLLISION ENERGY (eV/AMU))
C
C  CALLING PROGRAM: ADAS509
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK, ION INFORMATION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: RECEIVER - ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: RECEIVER - NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RECEIVED ION - FINAL CHARGE
C
C  INPUT : (C*9)  CDONOR  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           DONOR IDENTIFICATION
C  INPUT : (C*9)  CRECVR  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RECEIVER IDENTIFICATION
C  INPUT : (C*10) CFSTAT  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           FINAL STATE SPECIFICATION
C  INPUT : (C*2)  CTYPE   = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           CROSS-SECTION TYPE
C
C  INPUT : (R*8)  EEVA()  = USER ENTERED: COLLISION ENERGIES (eV/AMU)
C                           DIMENSION: COLLISION ENERGY INDEX
C  INPUT : (R*8)  SCXA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED
C                           CX CROSS-SECTIONS FOR
C                           THE USER ENTERED COLLISION ENERGIES.
C                           DIMENSION: COLLISION ENERGY INDEX
C  INPUT : (I*4)  IEVAL   = NUMBER OF USER ENTERED ENERGY VALUES
C
C  INPUT : (R*8)  EFITM() = MINIMAX: SELECTED ENERGIES (eV/AMU)
C  INPUT : (R*8)  SCXM()  = CROSS-SECTIONS AT 'EFITM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX GENERATED CX CROSS-
C                           SECTION VALUES FOR GRAPHICAL DISPLAY
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR ENERGY
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR ENERGY
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR CROSS-SECTION.
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR CROSS-SECTION.
C
C
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*28) STRG1   =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG2   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG4   =  DESCRIPTIVE STRING FOR STATE ION
C          (C*28) STRG5   =  DESCRIPTIVE STRING FOR STATE ION CHARGE
C          (C*28) STRG6   =  DESCRIPTIVE STRING FOR STATE META. INDEX
C          (C*32) HEAD1   =  HEADING FOR IONISING ION INFORMATION
C          (C*16) HEAD2   =  HEADING FOR INITIAL STATE INFORMATION
C          (C*16) HEAD3   =  HEADING FOR FINAL   STATE INFORMATION
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C	   XXFLSH      IDL-ADAS  CALLS FLUSH COMMAND TO CLEAR PIPE.
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    11/03/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    IZ0             , IZ               ,
     &           IEVAL           , NMX
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST          ,
     &           LGRD1           , LDEF1            , LFSEL
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       , TITLX*120         , TITLM*80  ,
     &           DATE*8         , ESYM*2            ,
     &           CDONOR*9       , CRECVR*9          ,
     &           CFSTAT*10      , CTYPE*2
      CHARACTER  STRG1*28       , STRG2*28          ,
     &           STRG3*28       , STRG4*28          ,
     &           STRG5*28       , STRG6*28          ,
     &           HEAD1*32       , HEAD2*16          , HEAD3*16
C-----------------------------------------------------------------------
      REAL*8     EEVA(IEVAL)    , SCXA(IEVAL)      ,
     &           EFITM(NMX)     , SCXM(NMX)
C-----------------------------------------------------------------------
      DATA HEAD1 /'----- REACTION INFORMATION -----'/ ,
     &     HEAD2 /' DONOR:         '/                 ,
     &     HEAD3 /' RECEIVER:      '/
      DATA STRG1 /' ELEMENT SYMBOL           = '/ ,
     &     STRG2 /' NUCLEAR CHARGE           = '/ ,
     &     STRG3 /'    IDENTIFICATION        = '/ ,
     &     STRG4 /'    FINAL CHARGE          = '/ ,
     &     STRG5 /'    FINAL STATE           = '/ ,
     &     STRG6 /'                          = '/
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') ESYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A9)')  CDONOR
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A9)')  CRECVR 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A10)') CFSTAT
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)')  CTYPE
      CALL XXFLSH(PIPEOU)
C
      WRITE(PIPEOU,*) IEVAL
      CALL XXFLSH(PIPEOU)
      DO  1 I = 1,IEVAL
         WRITE(PIPEOU,*) EEVA(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
C
      DO 3 I = 1,IEVAL
         WRITE(PIPEOU,*) SCXA(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
	    WRITE(PIPEOU,*) SCXM(I)
	    CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO 5 I = 1, NMX
            WRITE(PIPEOU,*) EFITM(I) 
	    CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      WRITE(PIPEOU,'(A28)') STRG1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG3
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG4
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG5
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG6
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A16)') HEAD2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A16)') HEAD3
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
      END

