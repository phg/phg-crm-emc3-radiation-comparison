C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9spln.for,v 1.1 2004/07/06 13:47:20 whitefor Exp $ DATE $Date: 2004/07/06 13:47:20 $
C
      SUBROUTINE E9SPLN( NEDIM  ,
     &                   IEA    , IEVAL   ,
     &                   TEEA   , EEVA    ,
     &                   SCX    , SCXA    ,
     &                   LERNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9SPLN *********************
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE ON LOG(ENERGY <EV/AMU> ) VERSUS
C          LOG(CX CROSS-SECTION <CM2>).
C          INPUT DATA FOR A GIVEN DONOR/RECEIVER COMBINATION DATA-BLOCK.
C
C          USING  ONE-WAY SPLINES  IT  CALCULATES  THE  CROSS-SECTIONS
C          FOR  'IEVAL'  COLLISION ENERGIES VALUES  FROM
C          THE LIST OF COLLISION ENERGIES READ IN FROM THE INPUT FILE
C
C          IF A  VALUE  CANNOT  BE  INTERPOLATED  USING  SPLINES  IT  IS
C          EXTRAPOLATED VIA 'XXSPLE'. (SEE NOTES BELOW).
C
C  CALLING PROGRAM: ADAS509/SSCX
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IEA     = INPUT DATA FILE: NUMBER OF COLLISION ENER-
C                           GIES READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  IEVAL   = NUMBER OF ISPF ENTERED COLLISION ENERGIES
C                           VALUES FOR  WHICH  CX CROSS-SECTIONS
C                           ARE REQUIRED FOR TABULAR/GRAPHICAL OUTPUT.
C
C  INPUT : (R*8)  TEEA()  = INPUT DATA FILE: COLLISION ENERGIES (EV/AMU)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: COLLISION ENERGY INDEX
C  INPUT : (R*8)  EEVA()  = USER ENTERED: COLLISION ENERGIES (EV/AMU)
C                           DIMENSION: COLLISION ENERGY INDEX
C
C  INPUT : (R*8)  SCX()    =INPUT DATA FILE: FULL SET OF CX CROSS-
C                           SECTIONS FOR THE DATA-BLOCK BEING ANALYSED
C                           1ST DIMENSION: COLLISION ENERGY INDEX
C  OUTPUT: (R*8)  SCXA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED
C                           CX CROSS-SECTIONS FOR
C                           THE USER ENTERED COLLISION ENERGIES.
C                           DIMENSION: COLLISION ENERGIES INDEX
C
C  OUTPUT: (L*4)  LERNG()=  .TRUE.  => OUTPUT 'SCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      COLLISION ENERGY 'EEVA()'.
C                           .FALSE. => OUTPUT 'SCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      COLLISION ENERGY 'EEVA()'.
C                           DIMENSION: COLLISION ENERGY INDEX
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF INPUT  ENERGY
C                                       VALUES. MUST BE >= 'IEA'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF OUTPUT ENERGY
C                                       VALUES.  MUST BE >= 'IEVAL'
C          (I*4)  L1      = PARAMETER = 1
C
C
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  COLLISION
C                           ENERGIES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           COLLISION ENERGIES.
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (R*8)  XIN()   = LOG( DATA FILE COLLISION ENERGIES )
C          (R*8)  YIN()   = LOG( DATA FILE CX CROSS-SECTIONS)
C          (R*8)  XOUT()  = LOG( USER ENTERED COLLISION ENERGIES.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED CX CROSS-SECTIONS)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C          ONE-DIMENSIONAL SPLINE CARRIED OUT BY THIS SUBROUTINE:
C
C           LOG( SCX )  vs. LOG( E )
C
C           E      = COLLISION ENERGY (units: eV/AMU)
C           SCX    = CX CROSS-SECTION (units: cm**2)
C
C           Extrapolation criteria:
C
C           Low  E: zero gradient  extrapolation (i.e. DY(1)  = 0.0)
C           High E: zero curvature extrapolation (i.e. DDY(N) = 0.0)
C
C           (These criteria are met by calling XXSPLE with IOPT=4)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    16/11/95
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN                   , NOUT            , L1
C-----------------------------------------------------------------------
      REAL*8     ZERO
C-----------------------------------------------------------------------
      PARAMETER( NIN    = 24           , NOUT = 24       , L1 = 1     )
      PARAMETER( ZERO   = 1.0D-30 )
C-----------------------------------------------------------------------
      INTEGER    NEDIM                 , IEA             , IEVAL
      INTEGER    IET                   , IT              , IOPT
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TEEA(NEDIM)           , EEVA(IEVAL)     ,
     &           SCX(NEDIM)            , SCXA(IEVAL)
      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)
C-----------------------------------------------------------------------
      LOGICAL    LERNG(IEVAL)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.IEA)
     &             STOP ' E9SPLN ERROR: NIN < IEA - INCREASE NIN'
      IF (NOUT.LT.IEVAL)
     &             STOP ' E9SPLN ERROR: NOUT < IEVAL - INCREASE NTOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX  = .TRUE.
      IOPT   = 4
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT COLLISION ENERGIES
C-----------------------------------------------------------------------
C
         DO 1 IET=1,IEA
            XIN(IET) = DLOG( TEEA(IET) )
    1    CONTINUE
C
         DO 2 IT=1,IEVAL
            XOUT(IT) = DLOG( EEVA(IT)  )
    2    CONTINUE
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL USER COLLISION ENERGIES
C-----------------------------------------------------------------------
C
         DO 3 IET=1,IEA
            YIN(IET) = DLOG( SCX(IET)+ZERO)
    3    CONTINUE
C
      CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &             IEA   , XIN     , YIN    ,
     &             IEVAL , XOUT    , YOUT   ,
     &             DF    , LERNG
     &           )
C
C-----------------------------------------------------------------------
C SET UP OUTPUT CX CROSS-SECTION ARRAY.
C-----------------------------------------------------------------------
C
         DO 4 IT=1,IEVAL
            SCXA(IT) = DEXP( YOUT(IT) )
    4    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
