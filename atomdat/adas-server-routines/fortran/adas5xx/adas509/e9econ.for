C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas5xx/adas509/e9econ.for,v 1.1 2004/07/06 13:46:43 whitefor Exp $ DATE $Date: 2004/07/06 13:46:43 $
C
      SUBROUTINE E9ECON( INTYP, OUTTYP, AMD,AMR,IEVAL, EIN, EOUT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E9ECON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF COLLISION  ENERGIES INTO A  SPECIFIED
C           FORM.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  INTYP   = 1 => 'EIN(ARRAY)' UNITS: DONOR   EV
C                            = 2 => 'EIN(ARRAY)' UNITS: RECVR   EV
C                            = 3 => 'EIN(ARRAY)' UNITS: ENERGY  EV/AMU
C  INPUT :    (I*4)  OUTTYP  = 1 => 'EOUT(ARRAY)' UNITS: DONOR  EV
C                            = 2 => 'EOUT(ARRAY)' UNITS: RECVR  EV
C                            = 3 => 'EOUT(ARRAY)' UNITS: ENERGY EV/AMU
C  INPUT :    (R*8)  AMD     = DONOR MASS NUMBER
C  INPUT :    (R*8)  AMR     = RECEIVER MASS NUMBER
C  INPUT :    (I*4)  IEVAL   = NO. OF ENERGIES IN EIN(ARRAY)
C  INPUT :    (R*8)  EIN()   = INPUT  ENERGIES (STATED UNITS)
C  OUTPUT:    (R*8)  EOUT()  = OUTPUT ENERGIES (STATED UNITS)
C
C
C             (I*4)  I       = GENERAL USE
C
C             (R*8)  ECONV() = ENERGY/VELOCITY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    17/11/95
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 30-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    INTYP                  , OUTTYP                  ,
     &           IEVAL
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     AMD                    , AMR
      REAL*8     EIN(IEVAL)             , EOUT(IEVAL)
      REAL*8     ECONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  OUTPUT DONOR ENERGY (EV)
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            ECONV(2) = AMD/AMR
            ECONV(3) = AMD
C
C-----------------------------------------------------------------------
C  OUTPUT RECEIVER ENERGY (EV)
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            ECONV(1) = AMR/AMD
            ECONV(3) = AMR
C
C-----------------------------------------------------------------------
C  OUTPUT ENERGY UNITS: EV/AMU
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            ECONV(1) = 1.0/AMD
            ECONV(2) = 1.0/AMR
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' E9ECON ERROR: INVALID OUTPUT ENERGY TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
         IF (INTYP.EQ.OUTTYP) THEN
            DO 1 I=1,IEVAL
               EOUT(I) = EIN(I)
    1       CONTINUE
         ELSE
            DO 4 I=1,IEVAL
               EOUT(I) = ECONV(INTYP) * EIN(I)
    4       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
