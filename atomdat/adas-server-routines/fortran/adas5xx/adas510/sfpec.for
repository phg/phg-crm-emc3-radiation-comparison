      SUBROUTINE SFPEC( IDPIX  ,
     &                  DSNAME , IBSEL   , ISIN   , IZ0IN  , AMASS ,
     &                  ITVAL  , TVAL    , TPVAL  , DVAL   ,
     &                  LBRD   ,
     &                  WVLMIN , WVLMAX  , NPIX   ,
     &                  FPECA  , FPECBRD , LTRNG  , LDRNG  ,
     &                  CINFO  , IRCODE
     &                )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: SFPEC *********************
C
C  PURPOSE: TO EXTRACT AND INTERPOLATE FEATURE PHOTON EMISSIVITIES FOR
C           EMITTING IONS. THE DOPPLER BROADENED SPECTRAL FEATURE CAN
C           BE RETURNED AS AN OPTION IF A SET OF ION TEMPEATURES AND A
C           MASS ARE GIVEN.
C
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IDPIX   = MAXIMUM NUMBER OF PIXELS FOR FEATURE
C  I/O   : (C*120) DSNAME  = SOURCE ADF40 DATA FILE. IF EMPTY, FILENAME
C                            CONSTRUCT FROM XXSPEC IS USED.
C  INPUT : (I*4)   IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)   ISIN    = SUPERSTAGE INDEX (ION CHARGE FOR /#01/
C                            OR UNPARTITIONED CASES)
C
C  INPUT : (I*4)   IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C  INPUT : (R*8)   AMSSNO  = ATOMIC MASS NUMBER
C
C  INPUT : (I*4)   ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSITY PAIRS
C  INPUT : (R*8)   TVAL()  = ELECTRON TEMPERATURES (UNITS: EV)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)   TPVAL() = ION TEMPERATURES (UNITS: EV)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)   DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  INPUT : (L)     LBRD    =.TRUE.  => APPLY DOPPLER BROADENING AND
C                                      OUTPUT RESULTS IN FPECBRD
C                           .FALSE. => DO NOT APPLY
C
C  OUTPUT: (R*8)   WVLMIN  = SELECTED BLOCK MINIMUM WAVELEN.(ANGSTROM)
C  OUTPUT: (R*8)   WVLMAX  = SELECTED BLOCK MAXIMUM WAVELEN.(ANGSTROM)
C  OUTPUT: (I*4)   NPIX    = SELECTED BLOCK NUMBER OF PIXELS
C
C  OUTPUT: (R*8)   FPECA(,)  = PHOTON EMISSIVITIES.
C                            1ST. DIM: PIXEL NUMBER
C                            2ND. DIM: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (R*8)   FPECBRD(,) = DOPPLER BROADENED PHOTON EMISSIVITIES.
C                            1ST. DIM: PIXEL NUMBER
C                            2ND. DIM: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LTRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LDRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (C*90)  CINFO   = INFORMATION STRING
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => NORMAL COMPLETION - NO ERROR DETECTED
C                            1 => DATA SET MEMBER FOR EMITTING ION WITH
C                                 CHARGE 'ISIN' & ION CHARGE 'IZ0IN' CAN
C                                 NOT BE FOUND/DOES NOT EXIST.
C                            2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                 AND THOSE IN INPUT FILE.
C                            3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                 OF RANGE OR DOES NOT EXIST.
C                            4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                 ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                            5 => INVALID VALUE FOR 'ISIN' ENTERED.
C                                 ( 0  <= 'ISIN' <= 99 )
C                            9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                 INPUT DATA-SET.
C
C                            Mod(10)= odd  => metastable resolution
C                                             detected
C                                   = even => not metastable resolution
C                            Mod(20)= odd  => superstage partitioning
C                                             detected
C                                   = even => not superstaged
C
C          (I*4)   NDPIX   = MAXIMUM NUMBER OF PIXELS FOR FEATURE (INTERNAL)
C          (I*4)   NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)   NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   NDDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON DENS-
C                                      ITIES  THAT  CAN  BE  READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
c
c          (i*4)   ndptnl  = maximum level of partitions
c          (i*4)   ndptn   = maximum no. of partitions in one level
c          (i*4)   ndptnc  = maximum no. of components in a partition
c          (i*4)   ndcnct  = maximum number of elements in connection
c          (i*4)   ndstack = maximum number of partition text lines
c          (i*4)   ndcmt   = maximum number of comment text lines
c                             vector
c
C          (I*4)   IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)   IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)   IZ0LST  = LAST VALUE OF 'IZ0IN' FOR  WHICH  INPUT
C                            DATA WAS READ.
C          (I*4)   IZLAST  = LAST VALUE OF 'ISIN' FOR  WHICH  INPUT
C                            DATA WAS READ.
C          (I*4)   IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)   NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                            DATA SET.
C          (I*4)   IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C
C          (L*4)   LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                            .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)   ESYM    = INPUT FILE - EMITTING ION - ELEMENT SYMBOL
C          (C*3)   EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C          (C*3)   EXTLST  = ADAS SOURCE DATA FILE EXT. USED LAST TIME
C                            DATA WAS READ.
C          (C*80)  UIDIN   = CURRENT ADAS SOURCE DATA USER ID.
C          (C*80)  UIDLST  = ADAS SOURCE DATA USER ID USED LAST TIME
C                            DATA WAS READ.
C          (C*8)   GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C          (C*8)   GRPLST  = ADAS SOURCE DATA GROUPNAME USED LAST TIME
C                            DATA WAS READ.
C          (C*80)  TYPIN   = ADAS DATA FILE SUBDIRECTORY (OPTIONAL)
C          (C*80)  TYPLST  = ADAS DATA FILE SUBDIRECTORY USED LAST TIME
C                            DATA WAS READ.
C          (C*80)  DSNREQ  = NAME OF DATA SET REQUESTED
C                            (MAY OR MAY NOT EXIST)
C          (C*80)  DSNAME  = NAME OF DATA SET INTERROGATED
C
C          (I*4)   ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                            TURES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   IDA()   = INPUT DATA SET-NUMBER OF ELECTRON DENSITIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)   TETA(,) = INPUT DATA SET -
C                            ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   TEDA(,) = INPUT DATA SET -
C                            ELECTRON DENSITIES    (UNITS: cm-3)
C                            1st DIMENSION: ELECTRON DENSITY     INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   FPEC(,,,)  =INPUT DATA SET -
C                             FULL SET OF FEATURE EMISSIVITIES
C                             1st DIM: PIXEL INDEX
C                             2nd DIM: ELECTRON TEMPERATURE INDEX
C                             3rd DIM: ELECTRON DENSITY     INDEX
C                             4th DIM: DATA-BLOCK INDEX
C
C          (C*8)   CFILE()  = INPUT FILE - SPECIFIC ION FILE SOURCE
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CTYPE()  = INPUT FILE - TYPE OF DATA (IE EXCIT., ETC)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*2)   CINDM()  = INPUT FILE - METASTABLE INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          EAFILE     ADAS      OPEN DATA SET FOR SELECTED EMITTER
C          EACHKB     ADAS      CHECK VALIDITY OF ION AND 'IBSEL'
C          EASPLN     ADAS      INTERPOLATE DATA WITH TWO WAY SPLINES
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSPEC     ADAS      FETCHES/SETS ADAS SOURCE DATA FILE NAME
C                               COMPONENTS
C          XXDATA_40  ADAS      READS A COMPLETE ADF40 DATASET
C          C5PIXV     ADAS      DISTRIBUTE DOPPLER BROADENED LINE EMISSION
C                               INTO PIXEL RANGE
C
C
C AUTHOR:  H.P. SUMMERS
C          K1/1/61
C          JET EXT. 4459
C DATE:    02/08/11
C
C
C VERSION : 1.1
C DATE    : 02-08-2011
C MODIFIED: Hugh Summers
C           - First release
C
C VERSION : 1.2
C DATE    : 02-11-2011
C MODIFIED: Martin O'Mullane
C           - Set LOPEN to false if a file inquire shows that
C             the file does not exist.
C           - If the file does not exist set all returned values
C             to zero to avoid confusion between multiple calls.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NDPIX     , NSTORE   , NTDIM         , NDDIM
      INTEGER     IZ0MIN    , IZ0MAX
      integer     ndptnl    , ndptn    , ndptnc        , ndcnct
      integer     ndstack   , ndcmt
C-----------------------------------------------------------------------
      PARAMETER ( NDPIX = 1024   , NSTORE = 10    , NTDIM  = 35  )
      PARAMETER ( NDDIM =  26    , IZ0MIN =   1   , IZ0MAX = 86  )
      parameter ( ndptnl = 4     , ndptn = 128    , ndptnc = 256 )
      parameter ( ndcnct = 100   , ndstack = 40   , ndcmt = 2000 )
C-----------------------------------------------------------------------
      INTEGER     IDPIX          , NPIX           , IBSEL        ,
     &            IZ0IN          , ISIN           ,
     &            ITVAL          , IRCODE         , IRCODE_OLD
      INTEGER     IZ0LST         , IZLAST         ,
     &            IUNIT          , NBSEL
      integer     IZ0            , is             , is1
      integer     nptnl          , ncnct          , ncptn_stack  ,
     &            ncmt_stack
      integer     it             , ip             , ipix         ,
     &            ind1           , ind2           ,
     &            lenstr         , i4unit
C-----------------------------------------------------------------------
      REAL*8      WVLMIN         , WVLMAX         , wvl
      real*8      fpec_mct       , amass          , cpixmx
C-----------------------------------------------------------------------
      LOGICAL     LOPEN          , LEXIST         , lbrd
      logical     lptn           , lres           , lsup         ,
     &            lcmt
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , EXTIN*3        , EXTLST*3     ,
     &                             UIDIN*80       , UIDLST*80    ,
     &                             GRPIN*8        , GRPLST*8     ,
     &                             TYPIN*80       , TYPLST*80    ,
     &            DSNREQ*120     , DSNAME*120     , CINFO*90
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             ,
     &            ITA(NSTORE)               , IDA(NSTORE)
      integer     npixa(nstore)
      integer     nptn(ndptnl)              , nptnc(ndptnl,ndptn)
      integer     iptnla(ndptnl)            , iptna(ndptnl,ndptn)
      integer     iptnca(ndptnl,ndptn,ndptnc)
      integer     icnctv(ndcnct)
      integer     isstgr(nstore)            , ilzr(nstore)         ,
     &            ihzr(nstore)
      integer     ispbr(nstore)             , isppr(nstore)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)          ,
     &            TPVAL(ITVAL)              ,
     &            FPECA(IDPIX,ITVAL)        , FPECBRD(IDPIX,ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CINDM(NSTORE)*2           , CFILE(NSTORE)*8      ,
     &            CTYPE(NSTORE)*8
      character   cptn_stack(ndstack)*80    , cmt_stack(ndcmt)*80
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDIM,NSTORE)        , TEDA(NDDIM,NSTORE)
      REAL*8      FPEC(NDPIX,NTDIM,NDDIM,NSTORE)
      real*8      wvmina(nstore)           , wvmaxa(nstore)       ,
     &            fpec_max(nstore)         , emiss(ndpix)
C-----------------------------------------------------------------------
      SAVE        DSNREQ      , UIDLST     , GRPLST     , TYPLST   ,
     &            EXTLST      , IZ0LST     , IZLAST     ,
     &            IUNIT       , IRCODE_OLD
      SAVE        ITA         , IDA        , TETA       , TEDA     ,
     &            FPEC        , CTYPE      , LRES       , LSUP     ,
     &            ISPBR       , ISPPR      , IPTNLA     , IS       ,
     &            ILZR        , IHZR       , FPEC_MAX              ,
     &            NPIXA       , WVMINA     , WVMAXA
C-----------------------------------------------------------------------
      DATA        UIDLST /' '/ , GRPLST /' '/   ,
     &            TYPLST /' '/   ,
     &            EXTLST /' '/   , IZ0LST /0/   , IZLAST /0/       ,
     &            IUNIT  /15/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  initial internal dimension check
C-----------------------------------------------------------------------

         if (idpix.ne.ndpix) then
            write(i4unit(-1),1001) 'idpix=',idpix,' .ne. ndpix=',ndpix
            write(i4unit(-1),1002)
            stop
         endif

C-----------------------------------------------------------------------
C BYPASS IF DATASET IS SAME AS IN PREVIOUS CALL OF SPEC
C-----------------------------------------------------------------------

      IF(DSNAME.EQ.DSNREQ) THEN
          IRCODE = IRCODE_OLD
          GOTO 10
      ENDIF

      IF(LENSTR(DSNAME).GT.1) THEN
          INQUIRE( FILE=DSNAME, EXIST=LEXIST)
          IF(LEXIST) THEN
               IRCODE = 0
               LOPEN  = .TRUE.
               dsnreq = dsname
               open(unit=iunit, file=dsnreq, status='old')
          else
               lopen = .false.
          ENDIF
      ELSE
          IRCODE = 0
          LOPEN  = .FALSE.

C-----------------------------------------------------------------------
C ESTABLISH CURRENT ADAS SOURCE DATA UID, FILE AND FILE EXTENSION
C-----------------------------------------------------------------------

          UIDIN  = '?'
          CALL XXUID(UIDIN)

          GRPIN  = '?'
          TYPIN  = '?'
          EXTIN  = '?'
          CALL XXSPEC(GRPIN , TYPIN , EXTIN)

C-----------------------------------------------------------------------

          IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN

C-----------------------------------------------------------------------
C IF NEW EMITTING ION ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------

               IF ( (IZLAST.NE.ISIN)  .OR.
     &              (IZ0LST.NE.IZ0IN) .OR.
     &              (UIDLST.NE.UIDIN) .OR.
     &              (GRPLST.NE.GRPIN) .OR.
     &              (TYPLST.NE.TYPIN) .OR.
     &              (EXTLST.NE.EXTIN)      )  THEN

                    CALL EAFILE( IUNIT , IZ0IN , ISIN , IRCODE ,
     &                           DSNREQ )
                     IF (IRCODE.EQ.0) THEN
                        LOPEN  = .TRUE.
                        IZLAST = ISIN
                        IZ0LST = IZ0IN
                        UIDLST = UIDIN
                        GRPLST = GRPIN
                        TYPLST = TYPIN
                        EXTLST = EXTIN
                        DSNAME = DSNREQ
                    ENDIF
               ELSE
                    DSNAME = DSNREQ
                    IRCODE = IRCODE_OLD
                    GO TO 10
               ENDIF
          ELSE
             IRCODE = 4
             LOPEN = .FALSE.
          ENDIF
      ENDIF

C-----------------------------------------------------------------------
C  Using general purpose xxdata_40.for.   ircode used to indicate if
C  superstage encountered or if metastable resolved data.
C-----------------------------------------------------------------------

       IF(LOPEN)THEN
           call xxdata_40( iunit  , dsname  ,
     &                     nstore , ndpix   , ntdim  , nddim  ,
     &                     ndptnl , ndptn   , ndptnc , ndcnct ,
     &                     ndstack, ndcmt   ,
     &                     iz0    , is      , is1    , esym   ,
     &                     nptnl  , nptn    , nptnc  ,
     &                     iptnla , iptna   , iptnca ,
     &                     ncnct  , icnctv  ,
     &                     ncptn_stack      , cptn_stack      ,
     &                     lres   , lptn    , lcmt   , lsup   ,
     &                     nbsel  , isela   ,
     &                     npixa  , cfile   , ctype  , cindm  ,
     &                     ispbr  , isppr   , isstgr , ilzr   , ihzr ,
     &                     wvmina , wvmaxa  ,
     &                     ita    , ida     ,
     &                     teta   , teda    ,
     &                     fpec   , fpec_max,
     &                     ncmt_stack       , cmt_stack
     &                   )

           if(lres) then
                ircode = ircode + 10
           endif

           if(lptn) then
                ircode = ircode + 20
            endif

       ENDIF
       IRCODE_OLD = IRCODE

C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED EMITTING ION CHARGE VALUE.
C-----------------------------------------------------------------------

   10  IF ((IRCODE-10*(IRCODE/10)).EQ.0 ) THEN
           CALL EACHKB( IUNIT , NBSEL  , IBSEL ,
     &                  IZ0IN , ISIN   ,
     &                  IZ0   , IS     ,
     &                  LOPEN , IRCODE
     &                )
       ENDIF

C-----------------------------------------------------------------------
C INTERPOLATE WITH TWO WAY SPLINES.
C-----------------------------------------------------------------------

       IF ((IRCODE-10*(IRCODE/10)).EQ.0 ) THEN

            fpec_mct = FPEC_MAX(IBSEL)*(wvmaxa(ibsel)+wvmina(ibsel))
     &                 /(2.0d0*npixa(ibsel))

            CALL EASPLN( NDPIX             , NTDIM         , NDDIM     ,
     &                   NPIXA(IBSEL)      , ITA(IBSEL)    , IDA(IBSEL),
     &                   ITVAL             ,
     &                   TETA(1,IBSEL)     , TEDA(1,IBSEL) ,
     &                   TVAL              , DVAL          ,
     &                   FPEC(1,1,1,IBSEL) , fpec_mct      ,
     &                   FPECA             ,
     &                   LTRNG             , LDRNG
     &                 )

C-----------------------------------------------------------------------
C CREATE INFORMATION STRING CINFO FOR OUTPUT.
C-----------------------------------------------------------------------

            write(cinfo(1:90),'(1a7,i4,1a7,1a8,1a5,l1,1a6,l1,'//
     &         '1a7,i2,1a7,i2,1a7,i2,1a4,i2,1a5,i2,1a5,i2,1a4)')
     &         '/ibsel=',ibsel,'/ctype=',ctype(ibsel),
     &         '/les=',lres,'/lsup=',lsup,
     &         '/ispbr=',ispbr(ibsel),'/isppr=',isppr(ibsel),
     &         '/iptnl=',iptnla(nptnl),'/is=',is,'/izl=',ilzr(ibsel),
     &         '/izh=',ihzr(ibsel),'/   '

            npix=npixa(ibsel)
            wvlmin=wvmina(ibsel)
            wvlmax=wvmaxa(ibsel)

C-----------------------------------------------------------------------
C Apply optional Doppler broadening
C-----------------------------------------------------------------------

           if (lbrd) then

              do it = 1, itval

                 do ip = 1, npix
                   fpecbrd(ip,it) = 0.0D0
                 end do
                 cpixmx = 0.0D0

                 do ip = 1, npix

                     wvl = wvlmin + (ip-1)*(wvlmax - wvlmin) / (npix-1)

                     call c5pixv( ndpix     , npix  , wvlmin  , wvlmax ,
     &                            cpixmx    ,
     &                            tpval(it) , amass , wvl     ,
     &                            fpeca(ip,it)      ,
     &                            emiss     , ind1  , ind2
     &                           )

                    if (ind1.GT.0.AND.ind2.LE.ndpix) then
                       do ipix = ind1, ind2
                          fpecbrd(ipix,it) = fpecbrd(ipix,it) +
     &                                       emiss(ipix)
                          cpixmx = max(cpixmx, emiss(ipix))
                       end do
                    endif

                 end do

              end do

           endif

       else

C-----------------------------------------------------------------------
C Here the input file does not exist so reset the returned values to
C zero to avoid any confusion from a previous call of the subroutine.
C-----------------------------------------------------------------------

          wvlmin = 0.0
          wvlmax = 0.0
          npix   = 0
          cinfo  = ' '
          do it = 1, itval

             do ip = 1, npix
               fpecbrd(ip,it) = 0.0D0
               fpeca(ip,it) = 0.0D0
             end do

          end do

       ENDIF

C-----------------------------------------------------------------------

 1001 format(1x,31('*'),'  SFPEC ERROR    ',30('*')//
     &       1x,'FAULT IN INTERNAL DIMENSION: ',1a6,i4,1a12,i4)
 1002 format(/1x,29('*'),' PROGRAM TERMINATED ',29('*'))

C-----------------------------------------------------------------------

      RETURN
      END
