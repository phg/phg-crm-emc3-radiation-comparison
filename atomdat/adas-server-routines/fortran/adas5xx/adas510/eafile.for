      SUBROUTINE EAFILE( IUNIT , IZ0 , IZ , IRCODE , DSNAME )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: EAFILE *********************
C
C  PURPOSE: TO OPEN A FEATURE PHOTON EMISSIVITY 'IONELEC' DATA SET
C           BY DEFAULT, OR AN ALTERNATIVE DATA SET IF REQUIRED, FOR
C           EMITTING ION WITH NUCLEAR CHARGE 'IZ0' AND CHARGE 'IZ'.
C           THIS WILL BE CONNECTED TO UNIT 'IUNIT'.
C
C   DATA SET OPENED: $ADASUSER/<DEFADF>/<GROUP>(OPTIONAL)/<TYPE>/
C                        <GROUP_EXT>#<ELEMENT SYMBOL><CHARGE>
C
C  CALLING PROGRAM: SPEC
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNIT   = UNIT TO WHICH DATA SET WILL BE CONNECTED
C  INPUT : (I*4)   IZ0     = NUCLEAR CHARGE OF EMITTING ION REQUESTED
C  INPUT : (I*4)   IZ      = ION CHARGE OF EMITTING ION REQUESTED
C
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => DATA SET SUCCESSFULLY CONNECTED
C                            1 => REQUESTED DATA  SET  MEMBER  DOES  NOT
C                                 EXISTS - DATA SET NOT CONNECTED.
C                            6 => INVALID VALUE FOR 'IZ' ENTERED.
C                                 ( 0 <= 'IZ' <= 99 )
C                            9 => REQUESTED DATA  SET  EXISTS BUT CANNOT
C                                 BE OPENED.
C  OUTPUT: (C*80)  DSNAME  = NAME OF OPENED DATA SET UNDER UNIX
C
C          (I*4)   IDLEN   = LENGTH, IN BYTES, OF FIXED 'DSNAME' PREFIX
C          (I*4)   LENF1   = FIRST NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF2   = LAST  NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF3   = FIRST NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF4   = LAST  NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF5   = LAST  NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF6   = LAST  NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF7   = LAST  NON-BLANK CHR OF 'DSNAME' EXTENSION PART
C          (I*4)   LENF8   = LAST  NON-BLANK CHR OF 'DSNAME' EXTENSION PART
C          (I*4)   IZEND   = LAST BYTE WRITTEN TO IN 'CZ'. (= 1 OR 2)
C          (C*1)   HASH    = '#' IF NON-BLANK EXT, ELSE ' '.
C          (C*2)   CZ      = 'IZ' (NO LEADING BLANKS)
C          (C*2)   XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)   ESYM    = ELEMENT SYMBOL FOR NUCLEAR CHARGE 'IZ0'
C          (C*3)   USREXT  = ADAS SOURCE DATA FILE EXTENSION
C          (C*80)  USERID  = ADAS SOURCE DATA USER ID
C          (C*8)   USRGRP  = ADAS SOURCE DATA GROUPNAME
C          (C*80)  USRTYP  = ADAS SOURCE DATA TYPENAME
C          (C*6)   DEFADF  = DEFAULT DATA DIRECTORY, I.E. ADF13
C
C          (L*4)   LEXIST  = .TRUE.  => REQUESTED  DATA  SET  EXISTS.
C                            .FALSE. => REQUESTED  DATA  SET  DOES  NOT
C                                       EXIST.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSPEC     ADAS      FETCHES/SETS ADAS SOURCE DATA FILENAME
C                               AND FILE EXTENSION
C          XFESYM     ADAS      CHARACTER*2 FUNCTION -
C                               GATHERS ELEMENT SYMBOL FOR NUC. CHARGE
C          XXSLEN     ADAS      FINDS FIRST AND LAST NON-BLANK
C                               CHARACTERS IN STRING.
C
C AUTHOR:  H.P. SUMMERS
C          K1/1/61
C          JET EXT. 4459
C DATE:    02/08/11
C
C
C VERSION : 1.1
C DATE    : 02-08-11
C MODIFIED: Hugh Summers
C            - First release.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IUNIT         , IZ0            , IZ         ,
     &            IRCODE        , IDLEN          , IZEND      ,
     &            LENF1         , LENF2          ,
     &            LENF3         , LENF4          ,
     &            LENF5         , LENF6          ,
     &            LENF7         , LENF8
C-----------------------------------------------------------------------
      CHARACTER   XFESYM*2      , ESYM*2         , CZ*2       ,
     &            USERID*80     , DSNAME*80      , DEFADF*5   ,
     &            USRGRP*8      , USRTYP*80      , USREXT*3   ,
     &            HASH*1
C-----------------------------------------------------------------------
      LOGICAL     LEXIST
C-----------------------------------------------------------------------
C SET DEFAULT DIRECTORY.
      PARAMETER (DEFADF = 'adf40')

C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  IDENTIFY ADAS SOURCE DATA USER ID, GROUPNAME , TYPENAME
C  AND FILE EXTENSION
C  NOTE THAT UNDER UNIX TYPENAME IS AN OPTIONAL SUBDIRECTORY.
C-----------------------------------------------------------------------
C
      USERID = '?'
      CALL XXUID(USERID)
C
      USRGRP = '?'
      USRTYP = '?'
      USREXT = '?'
      CALL XXSPEC(USRGRP , USRTYP , USREXT)
C
C-----------------------------------------------------------------------
C FIND THE FIRST AND LAST NON-BLANK CHARACTER STRINGS IN USRGRP & USRTYP
C-----------------------------------------------------------------------
C
      CALL XXSLEN( USRGRP , LENF1 , LENF2 )
      CALL XXSLEN( USRTYP , LENF3 , LENF4 )
      CALL XXSLEN( USERID , LENF5 , LENF6 )
      CALL XXSLEN( USREXT , LENF7 , LENF8 )
C
C-----------------------------------------------------------------------
C
      DSNAME = USERID(LENF5:LENF6)//'/'//DEFADF//'/'//
     &            USRGRP(LENF1:LENF2)//'/'
      IDLEN  = INDEX(DSNAME,' ') - 1
C
      IF (LENF7.EQ.0) THEN
         HASH =' '
      ELSE
         HASH = '#'
      ENDIF
C
      IF (LENF3.EQ.0) THEN
         DSNAME = DSNAME(1:IDLEN)//USRGRP(LENF1:LENF2)//'_'
     &      //USREXT(LENF7:LENF8)//HASH
      ELSE
         DSNAME = DSNAME(1:IDLEN)//USRTYP(LENF3:LENF4)//'/'
     &      //USRGRP(LENF1:LENF2)//'_'//USREXT(LENF7:LENF8)//HASH
      ENDIF
C
      IDLEN  = INDEX(DSNAME,' ') - 1
      ESYM   = XFESYM( IZ0 )
C
         IF (IZ.LT.0) THEN
            IRCODE = 6
         ELSEIF (IZ.LT.10) THEN
            WRITE(CZ,1000) IZ
            IZEND  = 1
            IRCODE = 0
         ELSEIF (IZ.LT.100) THEN
            WRITE(CZ,1001) IZ
            IZEND  = 2
            IRCODE = 0
         ELSE
            IRCODE = 6
         ENDIF
C
         IF (IRCODE.EQ.0) THEN
C
            IF ( ESYM(2:2) .EQ. ' ' ) THEN
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:1)//CZ(1:IZEND)//'.dat'
            ELSE
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:2)//CZ(1:IZEND)//'.dat'
            ENDIF
C
            INQUIRE( FILE=DSNAME , EXIST=LEXIST )
C
               IF (.NOT.LEXIST) THEN
                  IRCODE = 1
               ELSE
                  OPEN(UNIT=IUNIT,FILE=DSNAME,STATUS='OLD',ERR=9999)
                  IRCODE = 0
               ENDIF
C
         ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C OPEN STATEMENT ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999 IRCODE = 9
      RETURN
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I1,' ')
 1001 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      END
