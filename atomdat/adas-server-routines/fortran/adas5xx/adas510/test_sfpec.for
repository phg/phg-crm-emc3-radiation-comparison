      PROGRAM TEST_SFPEC
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PRGOGRAM: TEST_SFPEC ******************
C
C  PURPOSE: TO TEST THE INTERROGATION ROUTINE SFPEC AND ALL IMBEDDED
C  CALLS TO SUBROUTINES. SFPEC EXTRACTS AND INTERPOLATES FEATURE
C  EMISSIVITY COEFFICIENTS FOR EMITTING IONS.
C
C
C  INPUTS:
C
C         (CHAR*80) UIDIN   = CURRENT ADAS SOURCE DATA USER ID  PATHWAY.
C         (CHAR*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C         (CHAR*80) TYPIN   = CURRENT ADAS SOURCE DATA TYPENAME
C         (CHAR*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C
C         (I*4)     IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C         (I*4)     IZIN    = ION CHARGE OF EMITTING ION
C         (I*4)     IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C         (I*4)     ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSIY PAIRS
C         (R*8)     TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C         (R*8)     DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUTS :
C         (R*8)     WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C         (R*8)     PECA()  = IONIZATIONS PER PHOTON.
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C         (L*4)     LTRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)    LDRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C         (C*120)   TITLX   = INFORMATION STRING (DSN ETC.)
C         (I*4)     IRCODE  = RETURN CODE FROM SUBROUTINE SPEC:
C
C
C
C AUTHOR:  H.P. SUMMERS
C          K1/1/61
C          JET EXT. 4459
C DATE:    29/07/11
C
C
C VERSION : 1.1
C DATE    : 02-08-11
C MODIFIED: Hugh Summers
C             - First version.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     NDPIX   , ITVAL
C-----------------------------------------------------------------------
      PARAMETER ( NDPIX = 1024 , ITVAL = 10)
C-----------------------------------------------------------------------
      INTEGER     IBSEL          , I                 , NPIX        ,
     &            IZ0IN          , ISIN              ,
     &            IRCODE
      integer     i4unit
C-----------------------------------------------------------------------
      LOGICAL     LBRD
C-----------------------------------------------------------------------
      REAL*8      WLNGTH         , WVLMIN            , WVLMAX      ,
     &            AMASS
C-----------------------------------------------------------------------
      CHARACTER    UIDIN*80      , GRPIN*8           ,
     &             TYPIN*80      , EXTIN*3           ,
     &             CINFO*90      , DSNAME*120
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)          ,
     &            FPECA(NDPIX,ITVAL)        , FPECBRD(NDPIX,ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      DATA TVAL/ 5.0D1 , 1.0D2 , 1.5D2 , 2.0D2 , 3.0D2 ,
     &           4.0D2 , 5.0D2 , 6.0D2 , 8.0D2 , 1.0D3  /
      DATA DVAL/ 1.0D11, 1.0D11, 1.0D11, 1.0D11, 1.0D11,
     &           1.0D11, 1.0D11, 1.0D11, 1.0D11, 1.0D11 /
C-----------------------------------------------------------------------
c------------------------------------------------------------------------
C  EXAMPLE  SETTING THE ADAS CENTRAL DEFAULT DATABASE PATHWAY,
C-----------------------------------------------------------------------
C
        IRCODE = 0

C ------ explicit file name tests

        dsname='/home/adas/adas/adf40/fpec40#w/fpec40#w_ic#w34.dat'

        IBSEL = 2
        ISIN  = 34
        IZ0IN = 74
        AMASS = 183.85
        LBRD  = .TRUE.

        UIDIN = '*'


C ------ implicit file name test using xxspec

        CALL XXUID(UIDIN)

        GRPIN = '*'
        TYPIN = '*'
        EXTIN = '*'

        CALL XXSPEC(GRPIN,TYPIN,EXTIN)

        CALL SFPEC( NDPIX  ,
     &              DSNAME , IBSEL   , ISIN   , IZ0IN  , AMASS ,
     &              ITVAL  , TVAL    , TVAL   , DVAL   ,
     &              LBRD   ,
     &              WVLMIN , WVLMAX  , NPIX   ,
     &              FPECA  , FPECBRD ,LTRNG  , LDRNG  ,
     &              CINFO  , IRCODE
     &            )
C
        WRITE(6,'(a13,f10.3)')    "WVLMIN     : ", WVLMIN
        WRITE(6,'(a13,f10.3)')    "WVLMAX     : ", WVLMAX
        WRITE(6,'(a13,i5)')       "NPIX       : ", NPIX
        WRITE(6,'(1a13,1a80)')    "DSNAME     : ", DSNAME
        WRITE(6,'(1a13,1a90)')    "CINFO      : ", CINFO
        WRITE(6,'(1a13,i3)')      "IRCODE     : ", IRCODE
        WRITE(6,'(1a13,1p10e10.2/(13x,1p10e10.2))')
     &                      "FPECA(I,10) : ",(FPECA(I,10),I=1,NPIX)

        WRITE(6,'(1a13,1a80)')"Doppler Broadened feature"
        WRITE(6,'(1a13,1p10e10.2/(13x,1p10e10.2))')
     &                  "FPECBRD(I,10) : ",(FPECBRD(I,10),I=1,NPIX)

C-----------------------------------------------------------------------
      END
