      SUBROUTINE EASPLN( NDPIX  , NTDIM  , NDDIM  ,
     &                   NPIX   , ITA    , IDA    , ITVAL   ,
     &                   TETA   , TEDA   , TEVA   , DIN     ,
     &                   FPEC   , FPEC_MCT        , FPECA   ,
     &                   LTRNG  , LDRNG
     &                 )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: EASPLN *********************
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE AND DENSITY)
C          VERSUS LOG(FEATURE EMISSIVITY COEFFICIENT)
C          INPUT DATA FOR A GIVEN FEATURE DATA-BLOCK.
C
C          USING  TWO-WAY SPLINES IT CALCULATES  THE  FPEC
C          FOR  'ITVAL'  PAIRS OF  ELECTRON TEMPERATURES  AND  DENSITIES
C          FROM THE TWO-DIMENSIONAL TABLE OF TEMPERATURES/DENSITIES READ
C          IN FROM THE INPUT FILE. IF A  VALUE  CANNOT  BE  INTERPOLATED
C          USING SPLINES IT IS EXTRAPOLATED VIA 'XXSPLE'.
C
C  CALLING PROGRAM: ADAS510/SFPEC
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NTDIM   = MAX NUMBER OF PIXELS ALLOWED
C  INPUT : (I*4)  NTDIM   = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDIM   = MAX NUMBER OF ELECTRON DENSITIES    ALLOWED
C
C  INPUT : (I*4)  NPIX    = INPUT DATA FILE: NUMBER OF PIXELS READ FOR
C                           THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  ITA     = INPUT DATA FILE: NUMBER OF ELECTRON TEMPERA-
C                           TURES READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  IDA     = INPUT DATA FILE: NUMBER OF ELECTRON DENSIT-
C                           IES   READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  ITVAL   = NUMBER OF ISPF ENTERED TEMPERATURE/DENSITY
C                           PAIRS  FOR  WHICH  IOINIZATIONS PER PHOTON
C                           ARE REQUIRED FOR TABULAR/GRAPHICAL OUTPUT.
C
C  INPUT : (R*8)  TETA()  = INPUT DATA FILE: ELECTRON TEMPERATURES (EV)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TEDA()  = INPUT DATA FILE: ELECTRON DENSITIES (CM-3)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: ELECTRON DENSITY INDEX
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (EV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  DIN()   = USER ENTERED: ELECTRON DENSITIES (CM-3)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C
C  INPUT : (R*8)  FPEC(,,) =INPUT DATA FILE: FULL SET OF FEATURE EMISS-
C                           IVITY VALUES FOR THE DATA-BLOCK BEING
C                           ANALYSED.
C                           1ST DIM: PIXEL INDEX
C                           2ND DIM: ELECTRON TEMPERATURE INDEX
C                           3RD DIM: ELECTRON DENSITY     INDEX
C  INPUT : (R*8)  FPEC_MCT =APPROX. PHOT. EMISS POWER (PHOT/WVLENGTH)
C                           OVER WAVELENGTH RANGE FOR SELECTED FPEC
C                           (SEE XXDATA_40 DEFINITION)
C  OUTPUT: (R*8)  FPECA(,) =SPLINE INTERPOLATED OR  EXTRAPOLATED  FEATURE
C                           FOR THE USER ENTERED ELECTRON
C                           TEMPERATURE/DENSITY PAIRS.
C                           1ST DIM: PIXEL INDEX
C                           2ND DIM: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'PECA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'PECA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (L*4)  LDRNG()=  .TRUE.  => OUTPUT 'PECA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           .FALSE. => OUTPUT 'PECA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF INPUT  TEMP/DENSITY
C                                       VALUES. MUST BE >= 'ITA'&'IDA'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF OUTPUT TEMP/DENSITY
C                                       PAIRS.  MUST BE >= 'ITVAL'
C          (I*4)  IDPIX   = PARAMETER = MAX. NO. OF PIXELS.
C                                       MUST BE >= 'NPIX'
C          (I*4)  L1      = PARAMETER = 1
C
C          (I*4)  IED     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           DENSITIES.
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           TEMPERATURES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           TEMPERATURE/DENSITY PAIRS .
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (R*8)  XIN()   = 1) LOG( DATA FILE ELECTRON DENSITIES    )
C                           2) LOG( DATA FILE ELECTRON TEMPERATURES )
C          (R*8)  YIN()   = LOG( DATA FILE IONIZATIONS/PHOTON )
C          (R*8)  XOUT()  = 1) LOG( SCALED USER ENTERED ELECTRON DENS. )
C                           2) LOG( SCALED USER ENTERED ELECTRON TEMPS.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED IONIZATIONS/PHOTON )
C          (R*8)  YPASS(,,)= LOG(FEATURE EMISS BIN COUNTS) INTERMEDIARY
C                           ARRAY WHICH STORES INTERPOLATED/EXTRAPOLATED
C                           VALUES  BETWEEN  THE  TWO  SPLINE SECTIONS.
C                           SECTIONS.
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/61
C          JET EXT. 4459
C
C DATE:    02/08/11
C
C VERSION : 1.1
C DATE    : 02-08-11
C MODIFIED: Hugh Summers
C             - First version.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN                   , NOUT            , L1
      INTEGER    IDPIX
C-----------------------------------------------------------------------
      REAL*8     DZERO
C-----------------------------------------------------------------------
      PARAMETER( NIN   = 50            , NOUT = 50       , L1 = 1     ,
     &           IDPIX = 1024 )
      PARAMETER( DZERO = 1.0d-30  )
C-----------------------------------------------------------------------
      INTEGER    NDPIX                 , NTDIM           , NDDIM      ,
     &           ITA                   , IDA             , ITVAL
      INTEGER    IET                   , IED             , IT         ,
     &           NPIX                  , IPIX            , IOPT
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
      REAL*8     FPLMAX                , FPLMIN         , DZEROL
      REAL*8     FPEC_MCT
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TETA(ITA)             , TEDA(IDA)       ,
     &           TEVA(ITVAL)           , DIN(ITVAL)      ,
     &           FPECA(NDPIX,ITVAL)    ,
     &           FPEC(NDPIX,NTDIM,NDDIM)
      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)      ,
     &           YPASS(IDPIX,NOUT,NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)          , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------

      DZEROL = DLOG(DZERO)

      IF (NIN.LT.IDA)
     &             STOP ' EASPLN ERROR: NIN < IDA - INCREASE NIN'
      IF (NIN.LT.ITA)
     &             STOP ' EASPLN ERROR: NIN < ITA - INCREASE NIN'
      IF (NOUT.LT.ITVAL)
     &             STOP ' EASPLN ERROR: NOUT < ITVAL - INCREASE NTOUT'
      IF (IDPIX.LT.NPIX)
     &             STOP ' EASPLN ERROR: IDPIX < NPIX - INCREASE IDPIX'

C
C-----------------------------------------------------------------------
C SET UP FIRST SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
       LSETX = .TRUE.
       IOPT  = 0
C
C-----------------------------------------------------------------------
C FIRST OUTER LOOP OVER PIXEL INDEX
C-----------------------------------------------------------------------
C
       DO 10 IPIX =1,NPIX

C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------
C
           DO 1 IET=1,ITA
              XIN(IET) = DLOG( TETA(IET) )
    1      CONTINUE
C
           DO 2 IT=1,ITVAL
              XOUT(IT) = DLOG( TEVA(IT)  )
    2      CONTINUE

C-----------------------------------------------------------------------
C SPLINE OVER ALL DATASET ELECTRON DENSITIES FOR EACH USER ELECTRON TEMP
C-----------------------------------------------------------------------
C
          DO 3 IED=1,IDA
C
                FPLMAX = DZEROL
                FPLMIN = DLOG(FPEC_MCT)
                DO 4 IET=1,ITA
                   IF(FPEC(IPIX,IET,IED).LE.DZERO)THEN
                       YIN(IET)=DZEROL
                   ELSE
                       YIN(IET) = DLOG( FPEC(IPIX,IET,IED) )
                   ENDIF
                   FPLMAX=DMAX1(FPLMAX,YIN(IET))
                   FPLMIN=DMIN1(FPLMIN,YIN(IET))
    4           CONTINUE
C
                IF(FPLMAX.LE.DZEROL) THEN
                     DO IT=1,ITVAL
                         YPASS(IPIX,IT,IED) = DZEROL
                     ENDDO
                ELSE

                   CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &                          ITA   , XIN     , YIN    ,
     &                          ITVAL , XOUT    , YOUT   ,
     &                          DF    , LTRNG
     &                        )
C

                    DO 5 IT=1,ITVAL
                      YPASS(IPIX,IT,IED) = YOUT(IT)
    5               CONTINUE
                ENDIF
C
    3     CONTINUE

   10   CONTINUE
C
C-----------------------------------------------------------------------
C SET UP SECOND SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
         LSETX = .TRUE.
         IOPT  = 0
C
C-----------------------------------------------------------------------
C SECOND OUTER LOOP OVER PIXEL INDEX
C-----------------------------------------------------------------------
C
         do 20 IPIX=1,NPIX
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON DENSITIES
C-----------------------------------------------------------------------
C
           DO 6 IED=1,IDA
              XIN(IED) = DLOG( TEDA(IED) )
    6      CONTINUE
C
           DO 7 IT=1,ITVAL
              XOUT(IT) = DLOG( DIN(IT)   )
    7      CONTINUE
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL USER ELECTRON TEMPERATURES FOR EACH USER ELECTRON DENS
C-----------------------------------------------------------------------
C
           DO 8 IT=1,ITVAL
C
                 FPLMAX = DZEROL
                 FPLMIN = DLOG(FPEC_MCT)
                 DO 9 IED=1,IDA
                   IF(YPASS(IPIX,IT,IED).LE.DZEROL)THEN
                       YIN(IED)=DZEROL
                   ELSE
                       YIN(IED) = YPASS(IPIX,IT,IED)
                   ENDIF
                   FPLMAX=DMAX1(FPLMAX,YIN(IED))
                   FPLMIN=DMIN1(FPLMIN,YIN(IED))
    9            CONTINUE

                 IF(FPLMAX.LE.DZEROL) THEN
                     YOUT(IT)=DZEROL
                 ELSE

                     CALL XXSPLE( LSETX , IOPT       , R8FUN1   ,
     &                            IDA   , XIN        , YIN ,
     &                            L1    , XOUT(IT)   , YOUT(IT) ,
     &                            DF    , LDRNG(IT)
     &                          )

                 ENDIF
C
    8      CONTINUE
C
C-----------------------------------------------------------------------
C SET UP OUTPUT FEATURE EMISSIVITY ARRAY.
C-----------------------------------------------------------------------
C
           DO 15 IT=1,ITVAL
              FPECA(IPIX,IT) = DEXP( YOUT(IT) )
   15      CONTINUE

   20    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
