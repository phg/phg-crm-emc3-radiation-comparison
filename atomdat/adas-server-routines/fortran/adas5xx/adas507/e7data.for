CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7data.for,v 1.1 2004/07/06 13:45:43 whitefor Exp $ Date  $Date: 2004/07/06 13:45:43 $
CX
       SUBROUTINE E7DATA( IUNIT  , DSNAME ,
     &                    NSTORE , NTDIM  ,
     &                    ESYM   , IZ0    ,
     &                    NBSEL  , ISELA  ,
     &                    IZ     , IZ1    ,
     &                    CWAVEL , CIION  , CICODE , CISCRP , CITYPE ,
     &                    ITA    ,
     &                    TMA    , TETA   , DENSA  , GCF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7DATA *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT CONTRIBUTION FUNCTIONS
C             OF AN ELEMENT AND ITS IONS.
C            (MEMBER STORED IN IONELEC.DATA - MEMBER PREFIX 'GCF#').
C
C  CALLING PROGRAM: ADAS507/SGCF
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH BLOCK FORMING A COMPLETE SET OF CONTRIBUTION
C           FUNCTION VALUES FOR GIVEN TEMPERATURES.
C           EACH DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER
C           DATA-BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           MODEL PARAMETER     : UNSPECIFIED
C           DENSITIES           : CM-3
C           TEMPERATURES        : EV
C           CONTR. FUNCTION     : CM**3 S-1
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*44) DSNAME   = MVS DATA SET NAME OF DATA SET BEING READ
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NTDIM    = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C
C  OUTPUT: (C*2)  ESYM     = READ - RADIATING ION - ELEMENT SYMBOL
C  OUTPUT: (I*4)  IZ0      = READ - RADIATING ION - NUCLEAR CHARGE
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IZ()     = READ - RADIATING ION CHARGE
C                                   ( SET TO -1 IF WHOLE ELEMENT)
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (I*4)  IZ1()    = READ - RADIATING ION CHARGE +1
C                                   ( SET TO 1 IF WHOLE ELEMENT)
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*10) CWAVEL() = READ - WAVELENGTH (ANGSTROMS)
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CIION()  = READ - RADIATING ION (AS <ESYM>+(IZ()> )
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CICODE() = READ - SOURCE PROGRAM
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CISCRP() = READ - SOURCE SCRIPT
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CITYPE() = READ - RADIATION TYPE
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  ITA()    = READ - NUMBER OF ELECTRON TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TMA(,)   = READ - MODEL PARAMETER (UNITS: UNDEFINED)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TETA(,)  = READ - ELECTRON TEMPERATURES (UNITS: EV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  DENSA(,) = READ - ELECTRON DENSITIES    (UNITS: CM-3)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  GCF(,)    =READ - FULL SET OF GENERALISED CONTIBUTION
C                                   FUNCTIONS (CM**3 S-1)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: ELECTRON TEMPERATURE INDEX
C          (I*4)  NTNUM    = NUMBER OF ELECTRON TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (R*8)  R8FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*4)  CKEY1    = 'CODE'  - INPUT BLOCK HEADER KEY
C          (C*6)  CKEY2    = 'SCRIPT - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY3    = 'TYPE'  - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL'  - INPUT BLOCK HEADER KEY
C          (C*10) C10      = GENERAL USE TEN BYTE CHARACTER STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      REAL*8 FUNCTION       -
C                               CONVERT CHARACTER STRING TO REAL*8
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/04/94
C
C UPDATE:
C
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0                , I4FCTN              , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NTDIM                 , IZ0                 ,
     &           NBSEL
      INTEGER    IBLK                  , ITT                 ,
     &           NTNUM                 , IABT                ,
     &           IPOS1                 , IPOS2
C-----------------------------------------------------------------------
      REAL*8     R8FCTN
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*80             , ESYM*2
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*4               , CKEY2*6             ,
     &           CKEY3*4               , CKEY4*4             ,
     &           C10*10                , C80*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         , ITA(NSTORE)         ,
     &           IZ(NSTORE)            , IZ1(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CIION(NSTORE)*5       , CITYPE(NSTORE)*5    ,
     &           CICODE(NSTORE)*8      , CISCRP(NSTORE)*8    ,
     &           CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)    , GCF(NTDIM,NSTORE)   ,
     &           TMA(NTDIM,NSTORE)     , DENSA(NTDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4
C
C-----------------------------------------------------------------------
C
      DATA       CSLASH / '/' /
      DATA       CKEY1  / 'CODE'   /   , CKEY2  / 'SCRIPT' /  ,
     &           CKEY3  / 'TYPE'   /   , CKEY4  / 'ISEL'   /
C
C **********************************************************************
C
      LBEND = .FALSE.
C
C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) C80
      READ(C80  ,*   ) NBSEL
C
         IF (NBSEL.GT.NSTORE) THEN
            WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
            NBSEL = NSTORE
         ENDIF
C
C***********************************************************************
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C***********************************************************************
C
         DO 1 IBLK=1,NBSEL
C
C-----------------------------------------------------------------------
C INPUT TITLE, ION, TYPE AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------
C
            IF (.NOT.LBEND) THEN
               READ(IUNIT,1000)   C80
C
                  IF ( C80(1:1).NE.'C') THEN
C
                     IPOS1 =  INDEX(C80,'A') + 1
                     IPOS2 =  INDEX(C80,'/') - 1
C
                        IF (IPOS1.EQ.1) THEN
                           IPOS1        = 11
                           CWAVEL(IBLK) = C80(1:10)
                        ELSEIF (IPOS1.GT.10) THEN
                           CWAVEL(IBLK) = C80(1:10)
                        ELSE
                           CWAVEL(IBLK) = C80(1:IPOS1-1)
                        ENDIF
C
                     READ(C80(IPOS1:IPOS2),*)  ITA(IBLK)
C
                     IPOS1=IPOS2+2
                     IPOS2=IPOS1+INDEX( C80(IPOS1:80),'/')-1
C
                     CIION(IBLK) = C80(IPOS1:IPOS2)
C
                     ESYM        = C80(IPOS1:IPOS1+1)
                     IF(ESYM(2:2).EQ.'+') ESYM(2:2)=' '
                     IZ0   = I4EIZ0( ESYM )
C
                     C2          = C80(IPOS2-1:IPOS2)
                     IF(C2.EQ.'  ') C2='-1'
                     IZ(IBLK)    = I4FCTN( C2 , IABT )
                     IF(IZ(IBLK).EQ.-1)THEN
                      IZ1(IBLK)=1
                     ELSE
                      IZ1(IBLK)=IZ(IBLK)+1
                     ENDIF
C
                     CALL XXHKEY( C80 , CKEY1 , CSLASH , CICODE(IBLK) )
                     CALL XXHKEY( C80 , CKEY2 , CSLASH , CISCRP(IBLK) )
                     CALL XXHKEY( C80 , CKEY3 , CSLASH , CITYPE(IBLK) )
                     CALL XXHKEY( C80 , CKEY4 , CSLASH , C2           )
C
                     ISELA(IBLK) = I4FCTN( C2  , IABT )
                     NTNUM       = ITA(IBLK)
C
                         IF (NTNUM.GT.NTDIM) THEN
                            WRITE(I4UNIT(-1),2001) DSNAME , IBLK  ,
     &                                            NTDIM   , NTNUM
                            STOP
                         ENDIF
C
C-----------------------------------------------------------------------
C INPUT TIME (OR OTHER), TEMPERATURE, DENSITY AND
C RADIATED POWER COEFFTS. FOR BLOCK
C-----------------------------------------------------------------------
C
                     READ(IUNIT,1002) ( TMA(ITT,IBLK) , ITT=1,NTNUM )
C
                     READ(IUNIT,1002) ( DENSA(ITT,IBLK) , ITT=1,NTNUM )
C
                     READ(IUNIT,1002) ( TETA(ITT,IBLK) , ITT=1,NTNUM )
C
                     READ(IUNIT,1002) ( GCF(ITT,IBLK)  , ITT=1,NTNUM )
C
                  ELSE
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1
                  ENDIF
C
            ENDIF
C
    1    CONTINUE
C
C***********************************************************************
C
 1000 FORMAT(1A80)
 1001 FORMAT(A2)
 1002 FORMAT(8D9.2)
C
 2000 FORMAT(/1X,31('*'),' E7DATA MESSAGE ',31('*')/
     &        2X,'INPUT CONTR. FUNC. DATA SET NAME: ',A44/
     &        2X,'NUMBER OF ELEMENT ION DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' E7DATA ERROR ',32('*')/
     &        2X,'INPUT CONTR. FUNC. DATA SET NAME: ',A44/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT ELECTRON TEMPERATURES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' E7DATA MESSAGE ',31('*')/
     &        2X,'INPUT CONTR. FUNC. DATA SET NAME: ',A44/
     &        2X,'INCONSISTENCY IN THE NUMBER OF DATA-BLOCKS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
