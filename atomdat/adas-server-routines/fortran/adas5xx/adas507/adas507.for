CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/adas507.for,v 1.1 2004/07/06 10:59:32 whitefor Exp $ Date  $Date: 2004/07/06 10:59:32 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS507 **********************
C
C  ORIGINAL NAME: (NONE)
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF GENERALISED CONTRIBUTION FUNCTIONS>
C
C            THE SELECTED INPUT DATA SET IS FOR A SPECIFIC ELEMENT. THIS
C            FILE CONTAINS FOR THIS ELEMENT A NUMBER OF DATA BLOCKS EACH
C            REPRESENTING DIFFERENT RADIATING ION EMISSION
C            THESE BLOCKS ARE OPTONALLY RESOLVED INTO DIFFERENT
C            RADIATION TYPES  AND AN INFORMATION STRING.
C            FOR EACH DATA BLOCK GENERALISED
C            CONTRIBUTION FUNCTIONS ARE GIVEN  AS  A  FUNCTION  OF
C            ELECTRON  TEMPERATURE.
C            THIS  PROGRAM  ALLOWS  FOR THE EXAMINATION  AND DISPLAY  OF
C            A GIVEN ION/TYPE COMBINATION AS FOLLOWS:
C
C            TABULATED CONTR. FUNCS FOR  THE  SELECTED  COMBINATION  ARE
C            FITTED USING A CUBIC  SPLINE  ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.IONELEC.DATA(GCF#<EL>)'
C
C            WHERE <EL> REPRESENTS THE SYMBOL FOR THE PARTICULAR ELEMENT
C                       E.G. BE, C
C
C
C            EACH DATA SET MEMBER IS A ONE WAY TABLE OVER  ELECTRON
C            TEMPERATURE.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            MODEL PARAMETER       : UNSPECIFIED
C            ELECTRON TEMPERATURES : eV
C            ELECTRON DENSITY      : CM-3
C            CONTIBUTION FUNCTION  : CM**3 S-1
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NTDIM   = PARAMETER = MAXIMUM NUMBER OF ELECTRON TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF ISPF ENTRED ELECTRON
C                                       TEMPERATURE VALUES.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NPSPL   = PARAMETER =
C                           NUMBER  OF SPLINE INTERPOLATED GFT/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED radiated power
C                           COEFFTS/TEMPERATURE  VALUES  FOR  GRAPHICAL
C                           OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ0     = INPUT FILE: radiating ion- NUCLEAR CHARGE
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  IBLK    = DATA-BLOCK INDEX
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE VALUES
C          (I*4)  ITTYP   = 1 => 'TIN(array)' UNITS: KELVIN
C                         = 2 => 'TIN(array)' UNITS: EV
C                         = 3 => 'TIN(array)' UNITS: REDUCED
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C
C          (R*8)  R8TCON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMINPU  = GRAPH: LOWER LIMIT FOR TEMPERATURE (eV)
C          (R*8)  XMAXPU  = GRAPH: UPPER LIMIT FOR TEMPERATURE (eV)
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR radiated power.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR radiated power.
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                           .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*2)  ESYM    = INPUT FILE: RADIATING ION- ELEMENT SYMBOL
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET - NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ()    = INPUT FILE: RADIATING ION CHARGE
C                                       ( SET TO -1 IF WHOLE ELEMENT)
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ1()   = INPUT FILE: RADIATING ION CHARGE +1
C                                       ( SET TO 1 IF WHOLE ELEMENT)
C                           DIMENSION: DATA-BLOCK INDEX
C
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TIN()   = USER ENTERED ELECTRON TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TKEL()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TVOA()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TRED()  = USER ENTERED ELECTRON TEMPERATURES
C                           (UNITS: REDUCED)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  TFITM() = MINIMAX: SELECTED TEMPERATURES (eV) FOR
C                                    FITTING.
C          (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (EV)
C          (R*8)  GCFOSA()= SPLINE INTEROPLATED GCF VALUE AT 'TOSA()'
C          (R*8)  DENOSA()= SPLINE INTEROPLATED DENS. VALUE AT 'TOSA()'
C          (R*8)  TMOSA() = SPLINE INTEROPLATED 'TIME' VALUE AT 'TOSA()'
C          (R*8)  DENOA() = SPLINE INTEROPLATED DENS. VALUE AT 'TOSA()'
C          (R*8)  TMOA()  = SPLINE INTEROPLATED 'TIME' VALUE AT 'TOSA()'
C          (R*8)  GCFOA() = SPLINE INTERPOLATED OR EXTRAPOLATED  ZERO-
C                           DENSITY RADIATED POWER COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C          (R*8)  GCFM()  = RADIATED POWER COEFFICIENTS AT 'TFITM()'.
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  TMA(,)  = INPUT DATA SET -
C                           MODEL PARAMETER       (UNITS: UNSPECIFIED)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  DENSA(,)= INPUT DATA SET -
C                           ELECTRON DENSITIES    (UNITS: CM-3)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  GCF(,)  = INPUT DATA SET - FULL SET OF ZERO DENSITY
C                           RADIATED POWER COEFFT. VALUES (W CM**3)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TVALS(,,)= INPUT DATA SET - ELECTRON TEMPERATURES
C                            1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                                           ( SEE 'TETA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C
C          (C*10) CWAVEL() = READ - WAVELENGTH (ANGSTROMS)
C                            DIMENSION: DATA-BLOCK INDEX
C          (C*5)  CIION() = INPUT DATA FILE: RADIATING ION
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CICODE()= INPUT DATA FILE: SOURCE CODE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CISCRP()= INPUT DATA FILE: SOURCE SCRIPT
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*5)  CITYPE()= INPUT DATA FILE: TYPE
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'GCFOA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVOA()'.
C                           .FALSE. => OUTPUT 'GCFOA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVOA()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E7SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          E7DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          E7CHKZ     ADAS      VERIFY ELEMENT READ FROM MEMBER NAME OK
C          E7SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          E7ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          E7SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          E7TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          E7OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          E7OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/04/94
C
C UPDATE:
C
C
C VERSION  : 1.1
C DATE     : 25-01-2001
C MODIFIED : Martin O'Mullane
C              - Ported to unix from JETSHP.ADASXX50.FORT
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NTDIM        , NDTIN      , MAXDEG
      INTEGER    IUNT07       , IUNT10       , PIPEIN
      INTEGER    NPSPL        , NMX
      INTEGER    L1           , L2           , L3
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE = 150 , NTDIM  = 35 , NDTIN = 35 , MAXDEG = 19 )
      PARAMETER( IUNT07 =  17 , IUNT10 = 10 , PIPEIN = 5 )
      PARAMETER( NPSPL  = 100 , NMX    = 100 )
      PARAMETER( L1     =   1 , L2     =  2 , L3    =  3 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT       , ISTOP
      INTEGER    NBSEL        , IBLK         , IFORM       ,
     &           IZ0          , IBSEL        , KPLUS1      ,
     &           ITVAL        , ITTYP        , IPAN
C-----------------------------------------------------------------------
      REAL*8     R8TCON
      REAL*8     XMIN         , XMAX         , XMINPU      , XMAXPU    ,
     &           YMIN         , YMAX         ,
     &           TOLVAL
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , LGHOST       , LPEND       ,
     &           LOSEL        , LFSEL        ,
     &           LDSEL        , LGRD1        , LDEF1
      LOGICAL    LGRAPH       , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  ESYM*2       , REP*3        ,
     &           DATE*8       , DSFULL*80    , CADAS*80    , 
     &           TITLE*40     , TITLM*80     , TITLX*120
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)               , ITA(NSTORE) ,
     &           IZ(NSTORE)                  , IZ1(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)                  , TKEL(NDTIN)             ,
     &           TVOA(NDTIN)                 , TRED(NDTIN)             ,
     &           GCFOA(NDTIN)                , GCFM(NMX)               ,
     &           TFITM(NMX)                  , COEF(MAXDEG+1) ,
     &           TOSA(NPSPL)                 , GCFOSA(NPSPL)           ,
     &           DENOA(NDTIN)                , DENOSA(NPSPL)           ,
     &           TMOA(NDTIN)                 , TMOSA(NPSPL)
C-----------------------------------------------------------------------
      CHARACTER  CIION(NSTORE)*5 , CICODE(NSTORE)*8 , CISCRP(NSTORE)*8 ,
     &           CITYPE(NSTORE)*5, CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN)
C-----------------------------------------------------------------------
      REAL*8     TETA(NTDIM,NSTORE)          , GCF(NTDIM,NSTORE)
      REAL*8     TMA(NTDIM,NSTORE)           , DENSA(NTDIM,NSTORE)
      REAL*8     TVALS(NTDIM,3,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , ITVAL  /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000
      TITLE='NULL'
      TITLM='NULL'
      TITLX='NULL'  

C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )
      
C-----------------------------------------------------------------------

  100 CONTINUE 

C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------

      IF (OPEN10) THEN
         CLOSE(10)
         OPEN10 = .FALSE.
      ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
C-----------------------------------------------------------------------

      CALL E7SPF0( REP , DSFULL , LDSEL )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------

      IF (REP.EQ.'YES') GOTO 9999

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='UNKNOWN')
      OPEN10=.TRUE.

C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------

 
      CALL E7DATA( IUNT10 , DSFULL ,
     &             NSTORE , NTDIM  ,
     &             ESYM   , IZ0    ,
     &             NBSEL  , ISELA  ,
     &             IZ     , IZ1    ,
     &             CWAVEL ,CIION   , CICODE , CISCRP , CITYPE ,
     &             ITA    ,
     &             TMA    , TETA   , DENSA  , GCF
     &           )


C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS INPUT ELECTRON TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TETA()' IS IN eV.
C-----------------------------------------------------------------------

         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXTCON( L2          , IFORM                ,
     &                      IZ1(IBLK)   ,
     &                      ITA(IBLK)   , TETA(1,IBLK)         ,
     &                                    TVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE

C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------

      CALL E7SETP( NBSEL  )

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES
C-----------------------------------------------------------------------

  200 CALL E7ISPF( IPAN   , LPEND  ,
     &             NSTORE , NTDIM  , NDTIN  ,
     &             NBSEL  ,
     &             CWAVEL , CIION  , CICODE , CISCRP , CITYPE ,
     &             ITA    , TVALS  ,
     &             TITLE  ,
     &             IBSEL  ,
     &             ITTYP  , ITVAL  , TIN   ,
     &             LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN  , YMAX
     &           )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN  (TIN -> TKEL)
C-----------------------------------------------------------------------

      CALL XXTCON( ITTYP , L1 , IZ1(IBSEL) , ITVAL , TIN , TKEL )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV      (TIN -> TVOA)
C-----------------------------------------------------------------------

      CALL XXTCON( ITTYP , L2 , IZ1(IBSEL) , ITVAL , TIN , TVOA )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO REDUCED (TIN -> TRED)
C-----------------------------------------------------------------------

      CALL XXTCON( ITTYP , L3 , IZ1(IBSEL) , ITVAL , TIN , TRED )

C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV
C-----------------------------------------------------------------------

      XMINPU = R8TCON( ITTYP , L2 , IZ1(IBSEL) , XMIN )
      XMAXPU = R8TCON( ITTYP , L2 , IZ1(IBSEL) , XMAX )

C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------

      CALL E7SPLN(                  LOSEL    ,
     &             ITA(IBSEL)     , ITVAL    , NPSPL   ,
     &             TETA(1,IBSEL)  , TVOA     , TOSA    ,
     &             GCF(1,IBSEL)   , GCFOA    , GCFOSA  ,
     &             LTRNG
     &           )

      CALL E7SPLN(                  LOSEL    ,
     &             ITA(IBSEL)     , ITVAL    , NPSPL   ,
     &             TETA(1,IBSEL)  , TVOA     , TOSA    ,
     &             TMA(1,IBSEL)   , TMOA     , TMOSA   ,
     &             LTRNG
     &           )

      CALL E7SPLN(                  LOSEL    ,
     &             ITA(IBSEL)     , ITVAL    , NPSPL   ,
     &             TETA(1,IBSEL)  , TVOA     , TOSA    ,
     &             DENSA(1,IBSEL) , DENOA    , DENOSA  ,
     &             LTRNG
     &           )

C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITA(IBSEL) > 2).
C-----------------------------------------------------------------------

      IF (ITA(IBSEL).LE.2) LFSEL = .FALSE.

         IF (LFSEL) THEN
            CALL XXMNMX( LMLOG      , MAXDEG        , TOLVAL       ,
     &                   ITA(IBSEL) , TETA(1,IBSEL) , GCF(1,IBSEL) ,
     &                   NMX        , TFITM         , GCFM         ,
     &                   KPLUS1     , COEF          ,
     &                   TITLM
     &                 )
            WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF

C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------

      CALL E7TITL( IBSEL         , DSFULL        ,
     &             CWAVEL(IBSEL) , CIION(IBSEL)  ,
     &             CICODE(IBSEL) , CISCRP(IBSEL) , CITYPE(IBSEL) ,
     &             TITLX
     &           )

C-----------------------------------------------------------------------
C  GET OUTPUT OPTIONS FROM IDL
C-----------------------------------------------------------------------

 300  CONTINUE
      CALL E7SPF1( DSFULL        , 
     &             IBSEL         , LFSEL         , LDEF1    ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMIN          , XMAX          , YMIN     , YMAX   ,
     &             LPEND         , CADAS
     &           )


C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      IF (LPEND) GOTO 200


C----------------------------------------------------------------------
C OUTPUT RESULTS
C----------------------------------------------------------------------

      IF (L2FILE) THEN
        OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
        CALL E7OUT0( IUNT07        , LFSEL         ,
     &               TITLE         , TITLX         , TITLM   , DATE ,
     &               IBSEL         , ITVAL         ,
     &               ESYM          , IZ0           ,
     &               IZ(IBSEL)     , IZ1(IBSEL)    ,
     &               CWAVEL(IBSEL) , CIION(IBSEL)  ,
     &               CICODE(IBSEL) , CISCRP(IBSEL) , CITYPE(IBSEL) ,
     &               LTRNG         ,
     &               TKEL          , TVOA          , TRED      ,
     &               GCFOA         , DENOA         , TMOA      ,
     &               KPLUS1        , COEF
     &             )
         CLOSE(UNIT=IUNT07)
      ENDIF

C-----------------------------------------------------------------------
C OUTPUT GRAPHICAL RESULTS.
C-----------------------------------------------------------------------

      IF (LGRAPH) THEN
        CALL E7OUTG( LGHOST        ,
     &               TITLE         , TITLX         , TITLM     , DATE ,
     &               ESYM          , IZ0           ,
     &               IZ(IBSEL)     , IZ1(IBSEL)    ,
     &               CWAVEL(IBSEL) , CIION(IBSEL)  ,
     &               CICODE(IBSEL) , CISCRP(IBSEL) , CITYPE(IBSEL) ,
     &               TETA(1,IBSEL) , GCF(1,IBSEL)  , ITA(IBSEL),
     &               TFITM         , GCFM          , NMX       ,
     &               TOSA          , GCFOSA        , NPSPL     ,
     &               LGRD1         , LDEF1         , LFSEL     ,
     &               XMINPU        , XMAXPU        , YMIN      , YMAX
     &           )

C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)

          READ(PIPEIN,*) ISTOP
          IF (ISTOP.EQ.1) GOTO 9999
      ENDIF
      
C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANEL. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------

      GOTO 300

C-----------------------------------------------------------------------
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C-----------------------------------------------------------------------

 9999 CONTINUE
 
      END
