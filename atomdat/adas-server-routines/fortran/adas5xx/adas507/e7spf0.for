CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7spf0.for,v 1.1 2004/07/06 13:46:09 whitefor Exp $ Date  $Date: 2004/07/06 13:46:09 $
CX
      SUBROUTINE E7SPF0( REP  , DSFULL, LDSEL) 

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS507
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME , INCLUDING PATH
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    28/02/95
C
C UNIX-IDL PORT:
C
C VERSION  : 1.1
C DATE     : 25-01-2001
C MODIFIED : Martin O'Mullane
C              - Ported to unix from JETSHP.ADASXX50.FORT
C
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSFULL*80 
C-----------------------------------------------------------------------
      LOGICAL      LDSEL
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL

C-----------------------------------------------------------------------

      RETURN
      END
