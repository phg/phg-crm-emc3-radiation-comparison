CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7titl.for,v 1.1 2004/07/06 13:46:25 whitefor Exp $ Date  $Date: 2004/07/06 13:46:25 $
CX
      SUBROUTINE E7TITL( IBSEL  , DSNAME ,
     &                   CWAVEL , CIION  , CICODE , CISCRP , CITYPE ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS507/SGCF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*44) DSFULL   = FULL MVS INPUT DATA SET NAME
C
C
C  INPUT : (C*10) CWAVEL   = SELECTED DATA-BLOCK: WAVELENGTH
C  INPUT : (C*5)  CIION    = SELECTED DATA-BLOCK: RADIATING ION
C  INPUT : (C*8)  CICODE   = SELECTED DATA-BLOCK: SOURCE CODE
C  INPUT : (C*8)  CISCRP   = SELECTED DATA-BLOCK: SOURCE SCRIPT
C  INPUT : (C*5)  CITYPE   = SELECTED DATA-BLOCK: RADIATION TYPE
C
C  OUTPUT: (C*80) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C
C ROUTINES: NONE
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    18/04/94
C
C VERSION  : 1.1
C DATE     : 25-01-2001
C MODIFIED : Martin O'Mullane
C              - Ported to unix from JETSHP.ADASXX50.FORT
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*80       ,
     &           CWAVEL*10       , CIION*5         ,
     &           CICODE*8        , CISCRP*8        , CITYPE*5        ,
     &           TITLX*120
      CHARACTER  C2*2
C-----------------------------------------------------------------------

      WRITE(C2,1000) IBSEL
      
      CALL XXSLEN(DSNAME,IFIRST,ILAST)
      LEN_NAME = ILAST-IFIRST+1
      TITLX(1:LEN_NAME+1) = DSNAME(IFIRST:ILAST)
      POS_NOW = LEN_NAME+1
      TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
      POS_NOW = POS_NOW+9
      
      TITLX(POS_NOW:POS_NOW+8) =  ' <' // CIION // '>'

C-----------------------------------------------------------------------

 1000 FORMAT(I2)

C-----------------------------------------------------------------------

      RETURN
      END
