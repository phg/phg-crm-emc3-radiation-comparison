CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7spf1.for,v 1.1 2004/07/06 13:46:16 whitefor Exp $ Date  $Date: 2004/07/06 13:46:16 $
CX
      SUBROUTINE E7SPF1( DSFULL     , IBSEL      , LFSEL    , LDEF1 ,  
     &                   LGRAPH     , L2FILE     , SAVFIL   ,
     &                   XMIN       , XMAX       , YMIN     , 
     &                   YMAX       , LPEND      , CADAS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS507
C
C  SUBROUTINE:
C
C  INPUT :   (C*80)  DSFULL   = DATA FILE NAME
C  INPUT :   (I*4)   IBSEL    = SELECTED INPUT DATA BLOCK FOR ANALYSIS
C  INPUT :   (L*4)   LFSEL    = .TRUE.  => POLYNOMIAL FIT SELECTED
C				.FALSE. => NO POLYNOMIAL FIT SELECTED
C  OUTPUT:   (L*4)   LDEF1    = .TRUE.  => USER SLECTED AXES LIMITS
C				.FALSE. => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR TEMPERATURE UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR TEMPERATURE UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS IONIZATION COEFFICIENT
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT IONIZATION COEFFICIENT
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  Lalit Jalota (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    7/3/95 
C
C UNIX-IDL PORT:
C
C
C VERSION  : 1.1
C DATE     : 25-01-2001
C MODIFIED : Martin O'Mullane
C              - Ported to unix from JETSHP.ADASXX50.FORT
C
C-----------------------------------------------------------------------
      REAL*8       XMIN        , XMAX        , YMIN        , YMAX
C-----------------------------------------------------------------------
      CHARACTER    DSFULL*80   , SAVFIL*80   , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      ,
     &             LFSEL       , LDEF1
C-----------------------------------------------------------------------
      INTEGER      IBSEL       , ILOGIC      , I4UNIT      ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
     
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF

C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------

      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LGRAPH = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               LDEF1  = .TRUE.
	       READ(PIPEIN,*) XMIN
	       READ(PIPEIN,*) XMAX
	       READ(PIPEIN,*) YMIN
	       READ(PIPEIN,*) YMAX
	    ELSE
	       LDEF1  = .FALSE.
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF

         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
            CALL XXADAS(CADAS)
         ELSE
            L2FILE = .FALSE.
         ENDIF


      ENDIF

C-----------------------------------------------------------------------

      RETURN
      END
