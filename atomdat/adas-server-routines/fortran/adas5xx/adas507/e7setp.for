CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7setp.for,v 1.1 2004/07/06 13:46:02 whitefor Exp $ Date  $Date: 2004/07/06 13:46:02 $
CX
       SUBROUTINE E7SETP( NBSEL )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7SETP *********************
C
C  PURPOSE:  TO SET UP PARAMETERS IN THE SHARED POOLED FOR PANEL DISPLAY
C            *** IDENTICAL TO: E1SETP
C            *** IDENTICAL TO: C3SETP (EXCEPT 'SNCOMB' -> 'SLINES')
C
C  CALLING PROGRAM: ADAS507
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'E7DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBSEL   = NUMBER OF IONIZING ION COMBINATIONS READ IN.
C                           I.E. NUMBER OF DATA-BLOCKS.
C
C          (C*8)  F6      = PARAMETER = 'VREPLACE'
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C
C          (C*3)  SLINES  = 'NBSEL'
C          (C*8)  CHA     = FUNCTION POOL DIALOG NAME FOR 'NBSEL'.
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           ISPLNK      ISPF        ISPF PANEL SERVICE ROUTINE
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/04/94
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBSEL            
      INTEGER    PIPEOU
      PARAMETER ( PIPEOU = 6 )
C-----------------------------------------------------------------------
C
C **********************************************************************
C-----------------------------------------------------------------------
C WRITE NBSEL OUT TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, * ) NBSEL
      CALL XXFLSH( PIPEOU )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
