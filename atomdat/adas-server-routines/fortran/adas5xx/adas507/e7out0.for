CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7out0.for,v 1.1 2004/07/06 13:45:52 whitefor Exp $ Date  $Date: 2004/07/06 13:45:52 $
CX
       SUBROUTINE E7OUT0( IWRITE   , LFSEL  ,
     &                    TITLE    , TITLX  , TITLM , DATE   ,
     &                    IBSEL    , ITVAL  ,
     &                    ESYM     , IZ0    , IZ    , IZ1    ,
     &                    CWAVEL   , CIION  ,
     &                    CICODE   , CISCRP , CITYPE,
     &                    LTRNG    ,
     &                    TKEL     , TEVA   , TRED  ,
     &                    GCFA     , DENOA  , TMOA  ,
     &                    KPLUS1   , COEF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED DATA-BLOCK
C            UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS507
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
C                           SET NAME IN BYTES 1->35.
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IBSEL   = DATA-BLOCK INDEX SELECTED FROM INPUT DATASET
C                           FOR ANALYSIS.
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED ELECTRON TEMPERATURES
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: RADIATING ION- ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: RADIATING ION- NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION CHARGE
C                                 ( SET TO -1 IF WHOLE ELEMENT)
C  INPUT : (I*4)  IZ1     = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION CHARGE +1
C                                 ( SET TO 1 IF WHOLE ELEMENT)
C
C  INPUT : (C*10) CWAVEL   = READ - WAVELENGTH (ANGSTROMS)
C  INPUT : (C*5)  CIION    = INPUT DATA FILE: RADIATING ION
C  INPUT : (C*8)  CICODE   = INPUT DATA FILE: SOURCE CODE
C  INPUT : (C*8)  CISCRP   = INPUT DATA FILE: SOURCE SCRIPT
C  INPUT : (C*5)  CITYPE   = INPUT DATA FILE: TAYPE
C
C  INPUT : (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'GCFA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'GCFA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (eV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TKEL()  = USER ENTERED: ELECTRON TEMPERATURES (kelvin)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TRED()  = USER ENTERED: ELECTRON TEMPERATURES (reduced
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  INPUT : (R*8)  GCFA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED ZERO-
C                           DENSITY RADIATED POWER COEFFICIENTS FOR
C                           THE USER ENTERED ELECTRON TEMPERATURES.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  DENOA() = SPLINE INTERPOLATED DENS VALUE AT TEVA()
C  INPUT : (R*8)  TMA()   = SPLINE INTERPOLATED 'TIME' VALUE AT TEVA()
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1T     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT ELECTRON TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CELEM   = IONIZING ION  ELEMENT NAME
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C
C AUTHOR  : H. P. SUMMERS, JET
C           K1/1/57
C           JET EXT. 4941
C
C DATE:    18/04/94
C
C VERSION  : 1.1
C DATE     : 25-01-2001
C MODIFIED : Martin O'Mullane
C              - Ported to unix from JETSHP.ADASXX50.FORT
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IBSEL         , ITVAL      ,
     &           IZ0           , IZ            , IZ1        , KPLUS1
      INTEGER    I             , IFIRST        , ILAST  , iunit
C----------------------------------------------------------------------
      LOGICAL    LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*40      , TITLX*120     , TITLM*80   , ESYM*2  ,
     &           CWAVEL*10     , CIION*5       ,
     &           CICODE*8      , CISCRP*8      , CITYPE*5   , DATE*8
      CHARACTER  XFELEM*12     , CELEM*12
      CHARACTER  CSTRNG*80     , CIZ*2         , c1T*2
C----------------------------------------------------------------------
      REAL*8     TKEL(ITVAL)   , TEVA(ITVAL)   , TRED(ITVAL) ,
     &           GCFA(ITVAL)   , DENOA(ITVAL)  , TMOA(ITVAL) ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)
C----------------------------------------------------------------------
      DATA       CSTRNG/'BLK'/
C----------------------------------------------------------------------

      iunit = iwrite
      CELEM = XFELEM( IZ0 )

C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------

      WRITE(iunit,1001)
     &     'GENERALISED CONTRIBUTION FUNCTION INTERROGATION',
     &     'ADAS507' , DATE
      WRITE(iunit,1002) TITLE

      CALL XXFCHR(TITLX(1:80), CSTRNG, IFIRST, ILAST)
      WRITE(iunit,1003) TITLX(1:IFIRST-1), IBSEL
      
      IF(IZ.EQ.-1)THEN
          CIZ='  '
      ELSE
          WRITE(CIZ,'(1A2)')IZ
      ENDIF
      WRITE(iunit,1004) CELEM  , ESYM   ,
     &                   IZ0    , IZ     ,
     &                   CWAVEL , CICODE ,
     &                   CISCRP , CITYPE

C---------------------------------------------------------------------
C OUTPUT TEMPERATURES, DENSITIES AND IONIZATIONS PER PHOTON
C---------------------------------------------------------------------
C
      WRITE (iunit,1005)
C
         DO 1 I=1,ITVAL
            C1T = '*'
            IF (LTRNG(I)) C1T = ' '
            WRITE(iunit,1006) TKEL(I) , C1T , TEVA(I) , ' ' , TRED(I) ,
     &                         GCFA(I) , DENOA(I), TMOA(I)
    1    CONTINUE
C
      WRITE (iunit,1007)
      WRITE (iunit,1008)
      WRITE (iunit,1007) 
      

C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
            WRITE(iunit,1009)
C
            WRITE (iunit,1007)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(iunit,1010) I , COEF(I)
                  ELSE
                     WRITE(iunit,1011) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(iunit,1012) TITLM
            WRITE(iunit,1007)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(A79)
 1001 FORMAT(14('*'),' TABULAR OUTPUT FROM ',A47,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,14('*')/)
 1002 FORMAT(19('-'),1X,A40,1X,19('-')/,1X,
     & 'GENERALISED CONTRIBUTION FUNCTION AS A ',
     & 'FUNCTION OF ELECTRON TEMPERATURE'/
     & 2X,78('-')/
     & 23X,'DATA GENERATED USING PROGRAM: ADAS507'/23X,37('-'))
 1003 FORMAT(A80,' - DATA-BLOCK: ',I2)
 1004 FORMAT('EMITTING ION INFORMATION:'/
     &       1x ,25('-')/
     &       1x ,'ELEMENT NAME                =', 2X,A12 /
     &       1x ,'ELEMENT SYMBOL              =', 2X,A2  /
     &       1x ,'NUCLEAR CHARGE         (Z0) =', I3  /
     &       1x ,'CHARGE STATE           (Z)  =', I3  /
     &       1x ,'WAVELENGTH (angstrom)       =', A10 /
     &       1x ,'SOURCE CODE                 =', 2X,A8  /
     &       1x ,'SOURCE SCRIPT               =', 2X,A8  /
     &       1x ,'RADIATION TYPE              =', 2X,A5  )
 1005 FORMAT(1x /,2X,'-------- ELECTRON TEMPERATURE --------',
     &               7X,'CONTR. FUNC.',3X,'DENSITY',6X,'T PARM.'/
     &           1H ,7X,'Kelvin',8X,'eV',7X,'K/Z1**2',
     &               10X,'unspecified',6X,'cm-3',5X,'unspecified')
 1006 FORMAT(1x ,1P,4X,D10.3,2(1X,A,D10.3),9X,D10.3,3X,D10.3,3X,D10.3)
 1007 FORMAT(1x ,84('-'))
 1008 FORMAT(1x ,'NOTE: * => CONTRIBUTION FUNCTIONS  EXTRAPOLATED ',
     &                     'FOR ELECTRON TEMPERATURE VALUE')
C
 1009 FORMAT (/,'MINIMAX POLYNOMIAL FIT - TAYLOR COEFFICIENTS:'/
     &            1x ,79('-')/
     &            1x ,'- LOG10(CONTRIB. FUNC. <unspecif>) VERSUS ',
     &                'LOG10(ELECTRON TEMPERATURE<eV>) -')
 1010 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1011 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1012 FORMAT (A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
