CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas5xx/adas507/e7outg.for,v 1.1 2004/07/06 13:45:56 whitefor Exp $ Date  $Date: 2004/07/06 13:45:56 $
CX
      SUBROUTINE E7OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   ESYM   , IZ0    , IZ    , IZ1   ,
     &                   CWAVEL , CIION  ,
     &                   CICODE , CISCRP , CITYPE,
     &                   TEVA   , GCFA   , ITVAL ,
     &                   TFITM  , GCFM   , NMX   ,
     &                   TOSA   , GCFOSA , NPSPL ,
     &                   LGRD1  , LDEF1  , LFSEL ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E7OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED DATA-BLOCK USING GHOST80.
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE IINTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(ZERO-DENSITY CONTRIBUTION FUNCTION(ARB. UNTS)
C                                       VERSUS
C                             LOG10(ELECTRON TEMPERATURE (eV))
C
C  CALLING PROGRAM: ADAS507
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK, ION INFORMATION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: RADIATING ION - ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: RADIATING ION - NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION CHARGE
C                                 (SET TO -1 IF WHOLE ELEMENT)
C  INPUT : (I*4)  IZ1     = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RADIATING ION CHARGE +1
C                                 (SET TO 1 IF WHOLE ELEMENT)
C
C  INPUT : (C*10) CWAVEL   = READ - WAVELENGTH (ANGSTROMS)
C  INPUT : (C*5)  CIION    = INPUT DATA FILE: RADIATING ION
C  INPUT : (C*8)  CICODE   = INPUT DATA FILE: SOURCE CODE
C  INPUT : (C*8)  CISCRP   = INPUT DATA FILE: SOURCE SCRIPT
C  INPUT : (C*5)  CITYPE   = INPUT DATA FILE: TAYPE
C
C  INPUT : (R*8)  TEVA()  = INPUT DATA FILE: TEMPERATURES (EV)
C  INPUT : (R*8)  GCFA()  = INPUT DATA FILE: GCF VALUES
C  INPUT : (I*4)  ITVAL   = INPUT DATA FILE: NO. OF TEMPERATURES
C
C  INPUT : (R*8)  TFITM() = MINIMAX: SELECTED TEMPERATURES (eV)
C  INPUT : (R*8)  GCFM()  = CONTRIBUTION FUNCTIONS AT 'TFITM()'
C  INPUT : (R*8)  TOSA()  = SPLINE : SELECTED TEMPERATURES (EV)
C  INPUT : (R*8)  GCFOSA()= CONTRIBUTION FUNCTION AT 'TOSA()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX GENERATED IONIZATION RATE-
C                           COEFFT./TEMP. VALUES FOR GRAPHICAL DISPLAY
C  INPUT : (I*4)  NPSPL   = NUMBER OF SPLINE  GENERATED CONTRIBUTION
C                           FUNCTION  VALUES FOR GRAPHICAL DISPLAY
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEFFT.
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEFFT.
C
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80
C                           TAKES NUMBERS AS BEING ZERO = 1.0E-36
C          (R*4)  YDMIN   = PARAMETER = MINIMUM ALLOWED Y-VALUE FOR
C                           PLOTTING. (USED FOR DEFAULT GRAPH SCALING)
C                           (SET TO 'GHZERO'/ZERO TO REMOVE)
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C          (I*4)  IKEY    = NUMBER OF 'KEY()' VALUES TO BE OUTPUT
C          (I*4)  ICOUNT  = NUMBER OF POINTS PLOTTED FOR GRAPH CURVE
C
C          (R*4)  XHIGH   = UPPER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XLOW    = LOWER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YHIGH   = UPPER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YLOW    = LOWER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XA4()   = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C          (R*4)  YA4()   = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*25) XTIT    = X-AXIS UNITS/TITLE: ELECTRON TEMPERATURE
C          (C*39) YTIT    = Y-AXIS UNITS/TITLE: IONIZATION RATE-COEFFT.
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
C
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*7)  C7      =  7 BYTE STRING = 'TITLX(1:4)'//'C3BLNK'
C          (C*10) C10     =  GENERAL USE 10 BYTE CHARACTER STRING
C
C          (C*28) STRG1   =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG2   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG3   =  DESCRIPTIVE STRING FOR CHARGE STATE
C          (C*28) STRG4   =  DESCRIPTIVE STRING FOR WAVELENGTH
C          (C*28) STRG5   =  DESCRIPTIVE STRING FOR SOURCE CODE
C          (C*28) STRG6   =  DESCRIPTIVE STRING FOR SOURCE SCRIPT
C          (C*28) STRG7   =  DESCRIPTIVE STRING FOR RADIATION TYPE
C          (C*32) HEAD1   =  HEADING FOR EMITTING ION INFORMATION
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXLIM8      ADAS      SETTING UP OF DEFAULT GRAPH AXES
C          XXGHDI      ADAS      GHOST80 DESCRIPTION LINE + INTEGER
C          XXGH88      ADAS      GHOST80 TABLE LISTING WITH 2 x REAL*8
C          XXGSEL      ADAS      SELECTS POINTS WHICH WILL FIT ON GRAPH
C          I4UNIT      ADAS       GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C                      GHOST80   NUMEROUS SUBROUTINES
C
C AUTHOR:  H. P. SDUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    18/04/94
C
C UPDATE:
C
C
C VERSION  : 1.1
C DATE     : 25-01-2001
C MODIFIED : Martin O'Mullane
C              - Ported to unix from JETSHP.ADASXX50.FORT
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
      INTEGER    PIPEIN           , PIPEOU          , ONE      , ZERO
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-32 )
      PARAMETER( PIPEIN=5         , PIPEOU=6        , ONE=1    , ZERO=0)
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IZ0             , IZ               , IZ1       ,
     &           ITVAL           , NMX              , NPSPL
      INTEGER    I               , IKEY             , ICOUNT
C-----------------------------------------------------------------------
      REAL*4     XHIGH           , XLOW             ,
     &           YHIGH           , YLOW
C-----------------------------------------------------------------------
      REAL*8     XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST          ,
     &           LGRD1           , LDEF1            , LFSEL
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       , TITLX*120         , TITLM*80  ,
     &           DATE*8         , ESYM*2            ,
     &           CWAVEL*10      , CIION*5           ,
     &           CICODE*8       , CISCRP*8          , CITYPE*5
      CHARACTER  STRG1*28       , STRG2*28          ,
     &           STRG3*28       , STRG4*28          ,
     &           STRG5*28       , STRG6*28          , STRG7*28      ,
     &           HEAD1*32
C-----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)    , GCFA(ITVAL)      ,
     &           TFITM(NMX)     , GCFM(NMX)        ,
     &           TOSA(NPSPL)    , GCFOSA(NPSPL)
C-----------------------------------------------------------------------
      DATA HEAD1 /'--- EMITTING ION INFORMATION ---'/
      DATA STRG1 /' ELEMENT SYMBOL           = '/ ,
     &     STRG2 /' NUCLEAR CHARGE           = '/ ,
     &     STRG3 /' CHARGE STATE             = '/ ,
     &     STRG4 /' WAVELENGTH (angstroms)   = '/ ,
     &     STRG5 /' SOURCE CODE              = '/ ,
     &     STRG6 /' SOURCE SCRIPT            = '/ ,
     &     STRG7 /' RADIATION TYPE           = '/
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') ESYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ1
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,'(A10)') CWAVEL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)') CIION
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CICODE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CISCRP
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)') CITYPE
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,*) ITVAL
      CALL XXFLSH(PIPEOU)
      DO  1 I = 1,ITVAL
         WRITE(PIPEOU,*) TEVA(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE

      DO 3 I = 1,ITVAL
         WRITE(PIPEOU,*) GCFA(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE

      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMAX
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMAX
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
	    WRITE(PIPEOU,*) GCFM(I)
	    CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO 5 I = 1, NMX
            WRITE(PIPEOU,*) TFITM(I) 
	    CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF

      WRITE(PIPEOU,'(A28)') STRG1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG3
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG4
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG5
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG6
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG7
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------

      RETURN
      END
