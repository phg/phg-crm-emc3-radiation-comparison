      subroutine xxdatm_00f( iunit  , dsdir  , 
     &                       idmaxv ,
     &                       molsym ,
     &                       is_l   , is_u   , lvl_l  , lvl_u  , 
     &                       fcfa   , maxvl  , maxvu  , lerror
     &                     )

      implicit none

c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: xxdatm_00f ******************
c
c  purpose:  To fetch data from a  Frank-Condon factor dataset of data 
c            format mdf00 identified by directory and species level
c            parameters
c
c  subroutine:
c
c  input : (i*4)   iunit     = unit to which input file is allocated
c     	   (c*(*)) dsdir     = fcf mdf00 directory
c          (c*(*)  molsym    = molecule chemical symbol 
c     	   (i*4)   is_l	     = lower state specie index
c     	   (i*4)   is_u	     = upper state specie index
c     	   (i*4)   lvl_l     = lower state electronic index
c     	   (i*4)   lvl_u     = upper state electronic index
c
c output
c
c     	   (r*8)   fcfa(,)   = array with the fcfa factors for the input 
c                              states
c          (i*4)   maxvl     = highest quantum number of bound vib 
c                             levels for state lvl_l
c     	   (i*4)   maxvu     = highest quantum number of bound vib
c                             levels for state lvl_u
c     	   (l*4)   lerror    = .true.  => error in data acquisition
c     	                       .false. => data acquired
c
c
c routines:
c          routine    source     brief description
c          ------------------------------------------------------------
c          xxrmws     adas      removes all blanks in input string
c          xxslen     adas      finds string length excluding leading and 
c                               trailing blanks
c          xxhkey     adas      identify delimited key/value pair in string 
c          lenstr     adas      length of string omitting trailing blanks
c          xxcase     adas      convert string to upper or lower case 
c          xxopen     adas      check if dataset exists and open it  
c
c   author: F. Guzman 
c
c   version  : 1.0
c   date     : 24-03-2011
c   modified : F. Guzman
c              first version
c
c   version  : 1.1
c   date     : 26/05/2011
c   modified : F. Guzman
c              - to include directory files so isotopes can be counted
c
c   version  : 1.2
c   date     : 16/10/2012
c   modified : H. P. Summers
c              - modified to ADAS conventions
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer iunit     , i4unit      , lenstr
      integer idmaxv
      integer is_l      , is_u       , lvl_l, lvl_u
      integer maxvl     , maxvu
      integer j         , k
      integer istart    , istop       , nsubs
c-----------------------------------------------------------------------
      real*8  fcfa(idmaxv,idmaxv)
c-----------------------------------------------------------------------
      character dsdir*(*)    , dsname*128
      character molsym*(*)
      character c128*128     , c20*20    , c5*5
      character ctail*20
      character cs_l*2       , cs_u*2       , clvl_l*2  , clvl_u*2
c-----------------------------------------------------------------------
      logical   lexist       , lerror   
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  Set initial values and zero outputs
c-----------------------------------------------------------------------
      lerror = .false.
      maxvl = 0
      maxvu = 0
      do j=1, idmaxv	     
        do k=1, idmaxv	     
          fcfa(k,j)= 0.0d0   
        enddo   	     
      enddo		     
c-----------------------------------------------------------------------
c  Form required dataset name from inputs
c-----------------------------------------------------------------------
      c20=' '
      ctail = ' '
      call xxslen(molsym,istart,istop)
      c20(1:istop-istart+2) = molsym(istart:istop)//'/'
      write(c20(istop-istart+3:istop-istart+17),'(1a2,2i2,1a1,2i2,1a4)')
     &         'fc',is_l,lvl_l,'_',is_u,lvl_u,'.dat'
      istop = lenstr(c20)
      call xxrepl(c20(1:istop),' ','0',ctail(1:istop),nsubs)
      c20(1:istop)= ctail(1:istop)
      
      dsname = ' '
      c128   = ' '
      call xxslen(dsdir,istart,istop)
      c128(1:istop+20) = dsdir(istart:istop)//'/'//c20
      call xxcase(c128,dsname,'lc')

c-----------------------------------------------------------------------
c  Read dataset
c-----------------------------------------------------------------------
      call xxopen(iunit , dsname , lexist)
      if (lexist) then
         read(iunit,'(1a128)')c128
	 call xxhkey(c128,'max_vib_l','=',c5)
	 read(c5,*)maxvl
	 call xxhkey(c128,'max_vib_u','=',c5)
	 read(c5,*)maxvu
	 call xxhkey(c128,'species_l','=',cs_l)
	 call xxhkey(c128,'species_u','=',cs_u)
	 call xxhkey(c128,'state_l','=',clvl_l)
	 call xxhkey(c128,'state_u','=',clvl_u)
	 
         if(dsname(lenstr(dsname)-12:lenstr(dsname)-4).ne.
     &              (cs_l//clvl_l//'_'//cs_u//clvl_u)) then
             write(i4unit(-1),1000)'dataset name/parameter mismatch'
             write(i4unit(-1),1001)
	     lerror = .true.
	     return
	 endif
	 
         do j=1, maxvu+1
           read(iunit,*) (fcfa(k,j),k=1,maxvl+1)
         enddo
      else
          lerror = .true.	 
      endif
      	 
      close(iunit)

      return
c
c-----------------------------------------------------------------------
c
 1000 format(1x,31('*'),' xxdatm_00 error ',30('*')//
     &       1x,'fault in input dataset: ',a)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
