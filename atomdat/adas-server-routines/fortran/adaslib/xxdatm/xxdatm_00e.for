      subroutine xxdatm_00e( iunit  , dsdir  , 
     &                       idmaxv ,
     &                       molsym ,
     &                       is_l   , lvl_l  , 
     &                       enla   , maxvl  , lerror
     &                     )

      implicit none

c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: xxdatm_00e ******************
c
c  purpose:  To fetch data from a vibrational energy level dataset of  
c            data format mdf00 identified by directory and species 
c            level parameters
c
c  subroutine:
c
c  input : (i*4)   iunit     = unit to which input file is allocated
c     	   (c*(*)) dsdir     = enl mdf00 directory
c          (c*(*)  molsym    = molecule chemical symbol 
c     	   (i*4)   is_l	     = lower state specie index
c     	   (i*4)   lvl_l     = lower state electronic index
c
c output
c
c     	   (r*8)   enla()    = vector with the enl values for the input 
c                              state
c          (i*4)   maxvl     = highest quantum number of bound vib 
c                             levels for state lvl_l
c     	   (l*4)   lerror    = .true.  => error in data acquisition
c     	                       .false. => data acquired
c
c
c routines:
c          routine    source     brief description
c          ------------------------------------------------------------
c          xxrmws     adas      removes all blanks in input string
c          xxslen     adas      finds string length excluding leading and 
c                               trailing blanks
c          xxhkey     adas      identify delimited key/value pair in string 
c          lenstr     adas      length of string omitting trailing blanks
c          xxcase     adas      convert string to upper or lower case 
c          xxopen     adas      check if dataset exists and open it  
c
c   author: F. Guzman 
c
c   version  : 1.0
c   date     : 24-03-2011
c   modified : F. Guzman
c              first version
c
c   version  : 1.1
c   date     : 26/05/2011
c   modified : F. Guzman
c              - to include directory files so  isotopes can be count
c
c   version  : 1.2
c   date     : 16/10/2012
c   modified : H. P. Summers
c              - modified to ADAS conventions
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer iunit     , i4unit      , lenstr
      integer idmaxv
      integer is_l      , lvl_l
      integer maxvl
      integer j         , iv
      integer istart    , istop       , nsubs
c-----------------------------------------------------------------------
      real*8  enla(idmaxv)
c-----------------------------------------------------------------------
      character dsdir*(*)    , dsname*128
      character molsym*(*)
      character c128*128     , c20*20
      character ctail*20
      character cs_l*2       , clvl_l*2
c-----------------------------------------------------------------------
      logical   lexist       , lerror   
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  Set initial values and zero outputs
c-----------------------------------------------------------------------
      lerror = .false.
      maxvl = 0
      do j=1, idmaxv	     
          enla(j)= 0.0d0   
      enddo		     
c-----------------------------------------------------------------------
c  Form required dataset name from inputs
c-----------------------------------------------------------------------
      c20=' '
      ctail = ' '
      call xxslen(molsym,istart,istop)
      c20(1:istop-istart+2) = molsym(istart:istop)//'/'
      write(c20(istop-istart+3:istop-istart+18),'(1a3,2i2,1a4)')
     &         'enu',is_l,lvl_l,'.dat'
      istop = lenstr(c20)
      call xxrepl(c20(1:istop),' ','0',ctail(1:istop),nsubs)
      c20(1:istop)= ctail(1:istop)
      
      dsname = ' '
      c128   = ' '
      call xxslen(dsdir,istart,istop)
      c128(1:istop+20) = dsdir(istart:istop)//'/'//c20
      call xxcase(c128,dsname,'lc')

c-----------------------------------------------------------------------
c  Read dataset
c-----------------------------------------------------------------------
      call xxopen(iunit , dsname , lexist)
      if (lexist) then
         read(iunit,'(1a128)')c128
	 call xxhkey(c128,'species_l','=',cs_l)
	 call xxhkey(c128,'state_l','=',clvl_l)
	 
         if(dsname(lenstr(dsname)-7:lenstr(dsname)-4).ne.
     &              (cs_l//clvl_l)) then
             write(i4unit(-1),1000)'dataset name/parameter mismatch'
             write(i4unit(-1),1001)
	     lerror = .true.
	     close(iunit)
	     return
	 endif

      	 read(iunit,'(1a20)')c20  	 
      	 j=0
   10    j = j + 1
         c20 = ' '	     		      
      	 read(iunit,'(1a20)')c20  	 
      	 if (c20(1:5).eq.'   -1') return
      	 read(c20,'(i5,1f12)') iv, enla(j)
      	 maxvl = iv
	 go to 10   
      endif
      	 
      close(iunit)

      return
c
c-----------------------------------------------------------------------
c
 1000 format(1x,31('*'),' xxdatm_00 error ',30('*')//
     &       1x,'fault in input dataset: ',a)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
