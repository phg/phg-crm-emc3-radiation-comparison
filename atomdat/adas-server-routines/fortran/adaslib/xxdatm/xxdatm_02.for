      SUBROUTINE xxdatm_02( IUNIT  , 
     &                      MSYMB  , ATNUM   , ATSYMB ,
     &                      IND_S  , SYMBS   , CH_ION , BWNO_I  ,
     &                      CH_DIS , BWNO_D  , NDIS   , WNO     ,
     &                      WNOD   , 
     &                      IND_P  , INDEX_P , PATH_P , DESC_P  ,
     &                      IND_E  , ECONUA  , ECONSA , COUPSTA ,
     &                      WT     , RPARAM  , IND_R  , IN_E    ,
     &                      TPARAM , SPARAM  , PARVAL , EPARAM  ,
     &                      CMMT
     &                    )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C
C  ***************** FORTRAN77 SUBROUTINE: xxdatm_02 *******************
C
C  PURPOSE:  To fetch data from input data set of type mdf02.
C
C  CALLING PROGRAM: adas901
C
C  SUBROUTINE:
C  PARAM : (I*4)  MXMMOLNT  = PARAMETER = MAXIMAL NUMBER OF CHARACTER 
C                                         PER MOLECULE(NOW 4: At_N)
C  PARAM : (I*4)  MNE       = PARAMETER = MAX NUMBER OF ELECTRONIC STATES
C  PARAM : (I*4)  NCEST     = PARAMETER = MAX TOTAL NUMBER OF PROCESSES
C  PARAM : (I*4)  MAXPR     = PARAMETER = MAXIMUM NUMBER OF THE INDEX OF 
C                                         PROCESSES
C  PARAM : (I*4)  MNME      = PARAMETER = MAX NUMBER OF DESIRED TEMPERATURES
C  PARAM : (I*4)  MNS       = PARAMETER = MAX NUMBER OF SPECIES IN DIFFERENT 
C                                         MDF02 FILES
C  PARAM : (I*4)  MDP       = PARAMETER = MAX NUMBER OF FORMULA PARAMETERS IN 
C                                         INPUT DATA IN MDF02 FILES
C  PARAM : (I*4)  MNC       = PARAMETER = MAX NUMBER OF DISSOCIATION CHANELS 
C                                         BY SPECIE
C
C  INPUT : (I*4)  IUNIT     = UNIT TO WHICH INPUT FILE IS ALLOCATED
C
C  OUTPUT :(C*2)  MSYMB     = MOLECULAR SYMBOL
C                             (FOR HETERONUCLEAR MOLECULES IT HAS TO BE 
C                             AN ARRAY)
C  OUTPUT :(I*4)  ATNUM     = NUMBER OF ATOMS IN THE MOLECULE
C                             (FOR HETERONUCLEAR MOLECULES IT HAS TO BE 
C                             AN ARRAY)
C  OUTPUT :(C*2)  ATSYMB    = ATOMIC SYMBOLS IN THE MOLECULE
C                             (FOR HETERONUCLEAR MOLECULES IT HAS TO BE 
C                             AN ARRAY)
C  OUTPUT :(I*4)  IND_S     = MAX. INDEX OF SPECIES THAT IS DIFFERENT MOLECULES
C                             ATOMS AND THEIR IONS
C  OUTPUT :(C*10) SYMBS()   = ARRAY OF THE SYMBOLS FOR EACH IND_S
C                               1ST DIM. NUMBER OF SPECIES 
C  OUTPUT :(C*6)  CH_ION()  = IONISATION CHANNELS
C                               1ST DIM. NUMBER OF SPECIES
C  OUTPUT :(R*8)  BWNO_I()  = IONISATION ENERGY 
C                             (MEASURED FROM LOWEST ELECTRONIC STATE MINIMUM) (cm^-1) 
C                               1ST DIM. NUMBER OF SPECIES
C  OUTPUT :(C*6)  CH_DIS(,) = DISSOCIATION CHANNELS
C                               1ST DIM. NUMBER OF SPECIES, 
C                               2ND DIM. NUMBER OF DISS. CHANNEL
C  OUTPUT :(R*8)  BWNO_D()  = DISSOCIATION ENERGY
C                             (MEASURED FROM LOWEST ELECTRONIC STATE MINIMUM) (cm^-1)
C                               1ST DIM. NUMBER OF SPECIES, 
C                               2ND DIM. NUMBER OF DISS. CHANNEL
C  OUTPUT :(I*4)  NDIS()    = NUMBER OF DISSOCIATION CHANNELS PER SPECIE
C                               1ST DIM. NUMBER OF SPECIES
C  OUTPUT :(C*10) WNO(,)    = BOUND ENERGY OF STATES
C                             (MEASURED FROM LOWEST ELECTRONIC STATE  MINIMUM) (cm^-1)
C                               1ST DIM. SPECIE
C                               2ND DIM. EL. STATE
C  OUTPUT :(C*10) WNOD(,)   = ASYMPTOTIC ENERGY OF ELECTRONIC STATES 
C                             (MEASURED FROM LOWEST ELECTRONIC STATE MINIMUM) (cm^-1)  
C                               1ST DIM. SPECIE
C                               2ND DIM. EL. STATE
C  OUTPUT :(I*4)  IND_P     = MAX, INDEX OF ELECTRON IMPACT PROCESSES BETWEEN 
C                             DIFFERENT SPECIES ATOMS AND THEIR IONS
C  OUTPUT :(I*4)  INDEX_P() = INDEX OF DIFFERENT PROCESS IN IND_P
C                               1ST DIM. NUMBER OF PROCESS
C  OUTPUT :(C*60) PATH_P()  = PATH OF THE DIFFERENT PROCESS IN IND_P
C                               1ST DIM. NUMBER OF PROCESSES
C  OUTPUT :(C*40) DESC_P()  = DESCRIPTION OF THE DIFFERENT PROCESS IN IND_P
C                               1ST DIM. NUMBER OF PROCESSES
C  OUTPUT :(I*4)  IND_E()   = MAX. INDEX OF ELECTRONIC STATE FOR EACH IND_S
C                               1ST DIM. NUMBER OF SPECIES, 
C  OUTPUT :(C*8)  ECONUA()  = ELECTRONIC CONFIGURATION IN THE UNITED ATOMS 
C                             LIMIT
C                               1ST DIM. SPECIE
C                               2ND DIM. EL. STATE
C  OUTPUT :(C*4)  ECONSA()  = ELECTRONIC CONFIGURATION IN THE SEPARATED 
C                             ATOMS LIMIT
C                               1ST DIM. SPECIE
C                              2ND DIM. EL. STATE
C  OUTPUT :(C*10) COUPSTA() = COUPLED MOLECULAR STATE ELECTRONIC CONFIGURATION
C                               1ST DIM. SPECIE
C                               2ND DIM. EL. STATE
C  OUTPUT :(R*8)  WT(,)     = STATISTICAL WEIGHT 
C                               1ST DIM. SPECIE
C                               2ND DIM. EL. STATE
C  OUTPUT :(C*15) RPARAM(,) = PARAMETERS FOR EACH REACTION VALUES, ARRAY(IND_R,16)
C                               1ST DIM. EL. STATE
C                               2ND DIM. SPECIE
C  OUTPUT :(I*4)  IND_R     = MAX. NUMBER OF REACTIONS
C  OUTPUT :(I*4)  IN_E(,)   = INDEX OF ELECTRONIC STATE FOR EACH IND_S
C                               1ST DIM. SPECIES INDEX
C                               2ND DIM. EL. INDEX
C  OUTPUT :(R*8)  TPARAM(,) = ENERGY OR TEMPERATURES ARRAY FOR DATA
C                               1ST DIM. PROCESS INDEX, 
C                               2ND DIM. NUMBER OF ENERGIES
C  OUTPUT :(R*8)  SPARAM(,) = CROSS SECTION OR RATES DATA ARRAY 
C                               1ST DIM. PROCESS INDEX, 
C                               2ND DIM. NUMBER OF ENERGIES
C  OUTPUT :(R*8)  PARVAL(,) = PARAMETERS ARRAY FOR FITTING FORMULA
C                               1ST DIM. PROCESS INDEX, 
C                               2ND DIM. NUMBER OF PARAMETERS
C  OUTPUT :(R*8)  EPARAM()  = TRANSITION ENERGY PARAMETER
C                               1ST DIM. PROCESS INDEX, 
C  OUTPUT :(C*145)CMMT()    = COMMENTS ON THE INPUT FILE (100 LINES)
C
C
C  INT.  : (I*4)  IERR      = ERROR RETURN CODE.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          LENSTR     ADAS      RETURNS THE EFFECTIVE LENGTH OF A GIVEN 
C                               STRING (IGNORING TRAILING BLANKS)
C          XXRMWS     ADAS      REMOVES ALL BLANKS IN INPUT STRING
C          I4FCTN     ADAS      RETURNS CHARACTER STRING AS AN INTEGER.
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR: FRANCISCO GUZMAN
C
C VERSION  : 1.0
C DATE     : 11/06/10
C MODIFIED : F.GUZMAN
C          FIRST VERSION
C        - based in the structure of program xxdata_01
C
C
C VERSION  : 1.1
C DATE     : 9/11/2010  
C MODIFIED : F. GUZMAN
C     included a new parameter RPARAM(,15) that 
C     specifies the type of process (capture, ionization...). 
C     Needed for extrapolation
C
C VERSION  : 1.2
C DATE     : 25/03/2011 
C MODIFIED : F. GUZMAN
C     included the new matrix WNO(,) that accounts for the 
C     electronic bound energy
C     included rmws adas routine to avoid confusion in 
C     multiple files case
C
C VERSION  : 1.2.1
C DATE     : 22/09/2011 
C MODIFIED : F. GUZMAN
C     included the new matrix WNOD(,) that accounts for the 
C     electronic asymptotic energy needed after
C
C VERSION  : 2.0
C DATE     : 21/04/2012 
C MODIFIED : F. GUZMAN
C     included the new matrix IN_E to account for electronic
C     index independently
C
C VERSION  : 2.1
C DATE     : 12/08/2012 
C MODIFIED : F. GUZMAN
C     rationalized dimension in SPARAM,TPARAM and PARVAL. 
C     creates MNME, MNS and MDP, MNC parameters
C
C VERSION  : 2.2
C DATE     : 18-10-2012 
C MODIFIED : Martin O'Mullane
C     add index_p as an output
C
C VERSION  : 2.3
C DATE     : 16-11-2012 
C MODIFIED : F. GUZMAN
C     added parameter maxpr for processes index
C
C-----------------------------------------------------------------------
      INTEGER   MNE, NCEST, MAXPR, MNME, MNS, MDP, MNC, MXMMOLNT
C-----------------------------------------------------------------------
      PARAMETER ( MNE  = 60  , NCEST = 5000, MAXPR = 70 ) 
      PARAMETER ( MNME = 50  , MNS   = 20  , MDP = 20   , MNC = 5 )
      PARAMETER ( MXMMOLNT = 4 )
C-----------------------------------------------------------------------
      REAL*8    BWNO_I(MNS)  , BWNOD(MNS), BWNO_D(MNS,MNC)
      REAL*8    WT12(MNS,MNE), WT(MNS,MNE)
      REAL*8    WNO(MNS,MNE) , WNOD(MNS,MNE)
      REAL*8    WNO_R, WNOD_R, WT12_R
      REAL*8    TPARAM(NCEST,MNME), SPARAM(NCEST,MNME)
      REAL*8    PARVAL(NCEST,MDP)
      REAL*8    EPARAM(NCEST)
C-----------------------------------------------------------------------
      INTEGER   IUNIT   , I4UNIT  ,  IERR
      INTEGER   LENSTR  , I4FCTN
      INTEGER   NDIS(MNS)
      INTEGER   NUMC
C-----------------------------------------------------------------------
      INTEGER   N       , L       , I       , J   ,S, IP
      INTEGER   INDJ
      INTEGER   ATNUM   , IND_S   , IND_SOLD, IND_P , IND_R
      INTEGER   IND_E(MNS), IN_E(MNS,MNE)
      INTEGER   NUMER(NCEST), PAR(NCEST), FORM(NCEST), index_p(maxpr)
C-----------------------------------------------------------------------
      CHARACTER CSTRING*10
      CHARACTER MSYMB*10  , ATSYMB*2
      CHARACTER CLINE*145 , CH_ION(MNS)*10  , CHDIS(MNS)*7
      CHARACTER CH_DIS(MNS,MNC)*7
      CHARACTER CMMT(100)*145
      CHARACTER SYMBS(MNS)*10
      CHARACTER PATH_P(MAXPR)*60 , DESC_P(MAXPR)*40
      CHARACTER ECONUA(MNS,MNE)*8, ECONSA(MNS,MNE)*4
      CHARACTER COUPSTA(MNS,MNE)*10
      CHARACTER ECONUA_R*8, ECONSA_R*4, COUPSTA_R*10
      CHARACTER RPARAM(NCEST,MNS)*15
C-----------------------------------------------------------------------
      LOGICAL MOL
      INTEGER CONT1, CONT2
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
      IND_S  = 0
      IND_P  = 0
      IND_R  = 0
      INDJ   = 0
      DO CONT1=1,20
         IND_E(CONT1)=0
      ENDDO
      MOL    = .FALSE.
C-----------------------------------------------------------------------
C READ IN DATA.
C-----------------------------------------------------------------------
C READ HEADER
C-----------------------------------------------------------------------
C READ THE ATOMIC SYMBOL AND NUMBER OF ATOMS OF THE MOLECULE
C-----------------------------------------------------------------------
      READ(IUNIT,1000) CLINE
      READ(CLINE,1001) MSYMB
      NUMC=LENSTR(MSYMB)
      CALL XXRMWS(MSYMB,CSTRING,NUMC)
      MSYMB=CSTRING
      IF (LENSTR(MSYMB).LE. MXMMOLNT) THEN
         DO i=1, LENSTR(MSYMB)
            IF ((MSYMB(I:I).EQ.'_')
     &           .AND.
     &           (I.LE.MXMMOLNT-1)) THEN
               MOL=.TRUE.
               ATSYMB=MSYMB(1:I-1)
               ATNUM= I4FCTN( MSYMB(I+1:I+1) , IERR )
               IF (IERR .NE. 0) THEN
                  WRITE(I4UNIT(-1),2000)
                  WRITE(I4UNIT(-1),2006)
                  STOP
               ENDIF
            ENDIF
         ENDDO
         IF ( MOL.NEQV..TRUE.) then
            WRITE(I4UNIT(-1),2002)
            STOP
         ENDIF
      ELSE
        WRITE(I4UNIT(-1),2002) 
        WRITE(I4UNIT(-1),2003) MXMMOLNT
      ENDIF
C-----------------------------------------------------------------------
C END OF READ HEADER
C-----------------------------------------------------------------------
C START OF READ LOOP.
C-----------------------------------------------------------------------
    1 CONTINUE
C-----------------------------------------------------------------------
C READ NEXT LINE OF DATA FILE.
C-----------------------------------------------------------------------
      READ(IUNIT,1000,END=8) CLINE
C-----------------------------------------------------------------------
C LOOK FOR SECTION AND IGNORE LINE IF COMMENT.
C-----------------------------------------------------------------------
 2    CONTINUE
C-----------------------------------------------------------------------
C LOOK FOR COMMENTS.
C-----------------------------------------------------------------------
      
      IF (INDEX( CLINE,'c--------').NE.0) GOTO 6
      IF (INDEX( CLINE,'C--------').NE.0) GOTO 6
      IF ((CLINE(1:1) .EQ. 'C') .OR. (CLINE(1:1) .EQ. 'c')) THEN
         GOTO 1
      ELSE
         IF (INDEX( CLINE, '/' ) .NE. 0) GOTO 1
         IF (INDEX( CLINE,'species').NE.0) GOTO 5
         IF (INDEX( CLINE,'process').NE.0) GOTO 10
         IF (INDEX( CLINE, 'states').NE.0) GOTO 15
         IF (INDEX( CLINE,'values').NE.0) GOTO 20
      ENDIF
      GOTO 1
C-----------------------------------------------------------------------
C READ INDEXES
C-----------------------------------------------------------------------
 5    CONTINUE
      READ(IUNIT,1000,END=8) CLINE
      IND_SOLD=IND_S
      IF (INDEX(CLINE, '/') .EQ. 1) GOTO 5
      IF (INDEX( CLINE,'species').EQ.1) GOTO 5
      DO WHILE ((INDEX(CLINE, 'process').NE. 1).AND.
     &     (INDEX( CLINE,'values').NE.1).AND.
     &     (INDEX(CLINE, 'states').NE.1))
         INDJ=1
         READ(CLINE,1003) IND_S, SYMBS(IND_S), CH_ION(IND_S), 
     &        BWNO_I(IND_S), CHDIS(IND_S), BWNOD(IND_S)
         IF (IND_S.NE.0) THEN
            IF (IND_S.EQ.IND_SOLD) THEN
               INDJ=INDJ+1
               CH_DIS(IND_S,INDJ)=CHDIS(IND_S)
               BWNO_D(IND_S,INDJ)=BWNOD(IND_S)
            ELSE
               CH_DIS(IND_S,INDJ)=CHDIS(IND_S)
               BWNO_D(IND_S,INDJ)=BWNOD(IND_S)
            ENDIF
            NDIS(IND_S)=INDJ
         ELSE
            WRITE(I4UNIT(-1),2000)
            STOP
         ENDIF
         IF (INDEX(CH_ION(IND_S),'N').NE.0) WRITE(I4UNIT(-1),2007) 
     &        SYMBS(IND_S)
         IF (INDEX(CH_DIS(IND_S,INDJ),'N').NE.0) WRITE(I4UNIT(-1),2008) 
     &        SYMBS(IND_S)
         GOTO 5
      ENDDO
      GOTO 2
C-----------------------------------------------------------------------
C READ PROCESSES
C-----------------------------------------------------------------------
 10   CONTINUE
      READ(IUNIT,1000,END=8) CLINE
      IF (INDEX(CLINE, '/') .EQ. 1) GOTO 10
      DO WHILE ((INDEX( CLINE,'species').NE.1).AND.
     &     (INDEX( CLINE,'values').NE.1).AND.
     &     (INDEX(CLINE, 'states').NE.1))
         IND_P = IND_P + 1
         READ(CLINE,1004) IP, PATH_P(IP), DESC_P(IP)
         index_p(ind_p) = ip
         IF (IND_P.EQ.0) THEN
            WRITE(I4UNIT(-1),2000)
            STOP
         ENDIF
         GOTO 10
      ENDDO
      GOTO 2
C-----------------------------------------------------------------------
C READ STATES
C-----------------------------------------------------------------------
 15   CONTINUE
      READ(IUNIT,1000,END=8) CLINE
      IF (INDEX(CLINE, '/') .EQ. 1) GOTO 15
      DO WHILE ((INDEX(CLINE, 'process').NE. 1).AND.
     &     (INDEX( CLINE,'species').NE.1).AND.
     &     (INDEX( CLINE,'values').NE.1))
         READ(CLINE,1005) S, N, ECONUA_R, 
     &        ECONSA_R, COUPSTA_R, WT12_R,
     &        WNO_R,WNOD_R
         IND_E(S) =  IND_E(S)+1
         IN_E(S,IND_E(S))    = N
         ECONUA(S,IND_E(S))  = ECONUA_R
         ECONSA(S,IND_E(S))  = ECONSA_R
         COUPSTA(S,IND_E(S)) = COUPSTA_R
         WT12(S,IND_E(S))    = WT12_R
         WNO(S,IND_E(S))     =  WNO_R
         WNOD(S,IND_E(S))    =  WNOD_R
         WT(S,IND_E(S))=2.D0*WT12(S,IND_E(S))+1.D0
         IF (IND_S.EQ.0) THEN
            WRITE(I4UNIT(-1),2000)
            STOP
         ENDIF
         GOTO 15
      ENDDO
      GOTO 2
C-----------------------------------------------------------------------
C READ VALUES
C-----------------------------------------------------------------------
 20   CONTINUE
      DO WHILE ((INDEX(CLINE, 'C').NE. 1).AND. (INDEX(CLINE, 'c').NE.1))
         READ(IUNIT,1000,END=8) CLINE
         IF ((INDEX(CLINE, '/') .EQ. 1)
     &        .OR.(INDEX(CLINE, 's') .EQ. 1)) GOTO 20
         IF ((INDEX(CLINE, 'C').EQ. 1).OR. (INDEX(CLINE, 'c').EQ.1)) 
     &        GOTO 30
         IND_R=IND_R+1
C     READING SET OF DATA PARAMETERS
         READ(CLINE,1006) (RPARAM(IND_R,L),L=1,15)
         NUMER(IND_R)=I4FCTN(RPARAM(IND_R,13)(7:8), IERR)
         PAR(IND_R)=I4FCTN(RPARAM(IND_R,12)(5:6),IERR)
         FORM(IND_R)=I4FCTN(RPARAM(IND_R,11)(6:8),IERR) 
         IF (IERR .NE. 0) THEN
            WRITE(I4UNIT(-1),2000)
            WRITE(I4UNIT(-1),2006)
            STOP 
         ENDIF
         READ(RPARAM(IND_R,14)(INDEX(RPARAM(IND_R,14),'DE')+3:13),1010) 
     &        EPARAM(IND_R)
C     READING TEMPERATURES/ENERGIES IN 7-COLUMNS ROW 
         CONT2=0
         DO L=1,INT(NUMER(IND_R)/7)
            CONT2=CONT2+1
            READ(IUNIT,1000,END=8) CLINE
            READ(CLINE,1008) (TPARAM(IND_R,7*(L-1)+I), I=1,7)
         ENDDO
         IF (MOD(NUMER(IND_R),7).GT.0) THEN
            READ(IUNIT,1000,END=8) CLINE
            READ(CLINE,1008) (TPARAM(IND_R,7*CONT2+I), 
     &           I=1,MOD(NUMER(IND_R),7))
         ENDIF
C     READING OMEGAS IN 7-COLUMNS ROW 
         CONT2=0
         DO L=1,INT(NUMER(IND_R)/7)
            CONT2=CONT2+1
            READ(IUNIT,1000,END=8) CLINE
            READ(CLINE,1008) (SPARAM(IND_R,7*(L-1)+I), I=1,7)
         ENDDO
         IF (MOD(NUMER(IND_R),7).GT.0) THEN
            READ(IUNIT,1000,END=8) CLINE
            READ(CLINE,1008) (SPARAM(IND_R,7*CONT2+I), 
     &           I=1,MOD(NUMER(IND_R),7))
         ENDIF  
C     READING FORMULA PARAMETERS IN 6-COLUMNS ROW WITH BETTER PRECISION
         CONT2=0
         IF(PAR(IND_R).GT.0) THEN
            DO L=1,INT(PAR(IND_R)/6)
               CONT2=CONT2+1
               READ(IUNIT,1000,END=8) CLINE
               READ(CLINE,1009) (PARVAL(IND_R,6*(L-1)+I), I=1,6)
            ENDDO
             IF (MOD(PAR(IND_R),6).GT.0) THEN
               READ(IUNIT,1000,END=8) CLINE
               READ(CLINE,1009) (PARVAL(IND_R,6*CONT2+I), 
     &              I=1,MOD(PAR(IND_R),6))
            ENDIF
         ENDIF
      ENDDO
 30   GOTO 2
C-----------------------------------------------------------------------
C POST READ PROCESSING.
C-----------------------------------------------------------------------
 6    CONTINUE
C-----------------------------------------------------------------------
C  READ COMMENTS FOOTER
C-----------------------------------------------------------------------
      CONT2=1
      CMMT(CONT2)=CLINE
      DO WHILE(.TRUE.)
      CONT2=CONT2+1
      READ(IUNIT,1000,END=8) CLINE
      CMMT(CONT2)=CLINE
      ENDDO
C-----------------------------------------------------------------------
 8    CONTINUE
C-----------------------------------------------------------------------
 1000 FORMAT(1A144)
 1001 FORMAT(A10)
 1003 FORMAT(I4,7X,A10,1X,20X,1X,A10,F9.2,7X,A7,3X,F9.2)
 1004 FORMAT(I4,8X,A60,2X,A)
 1005 FORMAT(I4,4X,I4,7X,A8,4X,A4,7X,A10,10X,F3.1,5X,F9.2,2X,F9.2)
 1006 FORMAT(A2,1X,A2,2X,A2,7X,A2,9X,A2,2X,A2,2X,A2,4X,A6,6X,A7,2X,A7,
     &     2X,A8,2X,A6,2X,A8,2X,A13,2X,A2)
 1008 FORMAT(60X,7(2X,F10.4))
 1009 FORMAT(62X,6(1X,F11.4))
 1010 FORMAT(F10.4)
C
 2000 FORMAT(1X,32('*'),' XXDATM_02 ERROR ',32('*')//
     &       2X,'DATA FILE HAS INCORRECT FORMAT.'/
     &       2X,'EXPECTING NEW MDF02 FORMAT.')
 2002 FORMAT(1X,32('*'),' XXDATM_02 ERROR ',32('*')//
     &       2X,'ONLY HOMONUCLEAR MOLECULES ARE ALLOWED', 
     &       ' IN THIS VERSION.')
 2003 FORMAT (1X,32('*'),' XXDATM_01 ERROR ',32('*')//
     &       2X,'MAX. NUNBER OF CHARACTERS: ',I4)
 2006 FORMAT(/1X,30('*'),' PROGRAM TERMINATED ',31('*'))
 2007 FORMAT(1X,32('*'),' XXDATM_02 WARNING',32('*')//
     &       2X,'SPECIE',1X,A10,1X,'HAS NO ION. CHANNELS')
 2008 FORMAT(1X,32('*'),' XXDATM_02 WARNING',32('*')//
     &       2X,'SPECIE',1X,A10,1X,'HAS NO DIS. CHANNELS')
C-----------------------------------------------------------------------
      return
      end
 
