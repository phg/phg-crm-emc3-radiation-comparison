C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8adif.for,v 1.2 2007/04/11 13:01:49 allan Exp $ Date $Date: 2007/04/11 13:01:49 $
C
      FUNCTION R8ADIF( ndim , array , fmean , lmean )         

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8ADIF ******************
C
C  FUNCTION: Finds the absolute difference from the mean.
C
C            For small sample sizes this is more
C            meaningful a measure of the scatter than the
C            variance.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  FUNC:   (I*4)   R8ADIF  = ABSOLUTE DIFFERENCE
C
C  INPUT:  (I*4)   NDIM    = SIZE OF ARRAY
C  INPUT:  (R*8)   ARRAY   = OBVIOUS
C  INPUT:  (L)     LMEAM   = IF TRUE CALCULATE MEAN
C                            IF FALSE USE FMEAN
C  I/O:    (R*8)   FMEAN   = MEAN OF THE DATA
C
C
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          
C  DATE     : 10-04-2007
C  MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    ndim ,  I  , i4unit
C-----------------------------------------------------------------------
      REAL*8     fmean , r8adif , adif
C-----------------------------------------------------------------------
      LOGICAL    lmean
C-----------------------------------------------------------------------
      REAL*8     ARRAY(NDIM) 
C-----------------------------------------------------------------------
  
      if (ndim.lt.2) then
        write(i4unit(-1),100)
        stop
      endif
      
      if (lmean) then
      
         fmean = 0.0
         do i=1,ndim
           fmean = fmean + array(i)
         end do
         fmean = fmean/ndim
         
      endif
      
      adif = 0.0
      do i=1,ndim
        adif = adif + abs(array(i)-fmean)
      end do
      
      r8adif = adif/ndim

      
      return
      
 100  format('Array input to R8ADIF must have more 2 elements')
      
      end 
