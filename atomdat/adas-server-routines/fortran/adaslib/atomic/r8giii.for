      function r8giii(jz, l, e1, e2)

      implicit none
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 REAL*8 FUNCTION: R8GIII *****************
C
C  PURPOSE: CALCULATES GIII GIVEN IN EQUATIONS (11) AND (15) OF
C  A. BURGESS, J. PHYS. B7, L364, 1974.
C
C  SET L=1.SET JZ ZERO FOR THE ZERO CHARGE (NEUTRAL ATOM) CASE.
C  FOR PARTIAL SUMS SET L TO LOWER LIMIT OF SUMMATION WHICH
C  MUST BE GREATER THAN ZERO.
C  SET E1=(KAPPA1)**2 FOR NON ZERO CHARGE,
C        =(K1)**2 FOR ZERO CHARGE.
C  SET E2=(KAPPA2)**2 FOR NON ZERO CHARGE,
C        =(K2)**2 FOR ZERO CHARGE.
C
C
C  AUTHOR   : H P Summers
C  DATE     : 22-11-1984
C
C
C
C  VERSION  : 1.1
C  DATE     : 02-03-2005
C  MODIFIED : Martin O'Mullane
C                - First version in central ADAS.
C                - Make implicit none.
C  VERSION  : 1.2
C  DATE     : 16-05-2005
C  MODIFIED : Allan Whiteford
C                - Call r8fdip and r8fdip0 instead of fdip and fdip0
C                  since the routines were renamed.
C
C  VERSION  : 1.3                          
C  DATE     : 10-04-2007
C  MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      integer    JZ      , L
      integer    L1      , L2
C-----------------------------------------------------------------------
      real*8     r8giii  , r8fdip  , r8fdip0
      real*8     e1      , e2
      real*8     f1      , f2      , el
C-----------------------------------------------------------------------
      external   r8fdip  , r8fdip0
C-----------------------------------------------------------------------

      L1=L
      L2=L-1

      IF(JZ)1,2,1
 1    F1=R8FDIP(E1,L1,E2,L2)
      F2=R8FDIP(E1,L2,E2,L1)
      EL=L
      R8GIII=1.102658D0*((1.0D0+EL*EL*E1)*F1*F1-(1.0D0+EL*EL*E2)*F2*F2)
      R8GIII=R8GIII/(EL*(E1-E2))
      RETURN

 2    F1=R8FDIP0(E1,L1,E2,L2,1.0D-12)
      F2=R8FDIP0(E1,L2,E2,L1,1.0D-12)
      EL=L
      R8GIII=1.102658D0*EL*(E1*F1*F1-E2*F2*F2)/(E1-E2)

      RETURN

      END
