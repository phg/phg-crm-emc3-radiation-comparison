CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxbcon.for,v 1.1 2004/07/06 15:31:38 whitefor Exp $ Date $Date: 2004/07/06 15:31:38 $
CX
      SUBROUTINE XXBCON( INTYP, OUTTYP, IEVAL, EIN, EOUT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXBCON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF BEAM ENERGIES INTO SPECIFIED UNITS
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  INTYP   = 1 => 'EIN(ARRAY)' UNITS: EV/AMU
C                            = 2 => 'EIN(ARRAY)' UNITS: AT. UNITS
C                            = 3 => 'EIN(ARRAY)' UNITS: CM SEC-1
C  INPUT :    (I*4)  OUTTYP  = 1 => 'EOUT(ARRAY)' UNITS: EV/AMU
C                            = 2 => 'EOUT(ARRAY)' UNITS: AT. UNITS
C                            = 3 => 'EOUT(ARRAY)' UNITS: CM SEC-1
C  INPUT :    (I*4)  IEVAL   = NUMBER OF BEAM ENERGIES IN 'EIN(ARRAY)'
C  INPUT :    (R*8)  EIN()   = INPUT BEAM ENERGIES (STATED UNITS)
C  OUTPUT:    (R*8)  EOUT()  = OUTPUT BEAM ENERGIES (STATED UNITS)
C
C             (R*8)  EVUATU  = EV/AMU TO ATOMIC UNITS CONVERSION
C             (R*8)  EVUCMS  = EV/AMU TO CM S-1 CONVERSION
C
C             (I*4)  I       = GENERAL USE
C
C             (R*8)  BCONV() = BEAM ENERGY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            BEAM ENERGY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; BCONV(1) =>             EV/AMU  -> OUTPUT UNITS
C            INTYP = 2 ; BCONV(2) =>           AT. UNITS -> OUTPUT UNITS
C            INTYP = 3 ; BCONV(3) =>           CM SEC-1  -> OUTPUT UNITS
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    20/04/95
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     EVUATU             , EVUCMS
C-----------------------------------------------------------------------
      PARAMETER( EVUATU=6.32678D-03 , EVUCMS=1.38410D+06 )
C-----------------------------------------------------------------------
      INTEGER    INTYP              , OUTTYP             ,
     &           IEVAL
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     EIN(IEVAL)         , EOUT(IEVAL)
      REAL*8     BCONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  OUTPUT BEAM ENERGY UNITS: EV/AMU
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            BCONV(1) = 1.0
            BCONV(2) = 1.0/EVUATU
            BCONV(3) = 1.0/EVUCMS
C
            DO 1 I=1,IEVAL
               IF (INTYP.EQ.1) THEN
                   EOUT(I) = BCONV(1) * EIN(I)
               ELSEIF (INTYP.EQ.2) THEN
                   EOUT(I) = (BCONV(2) * EIN(I))**2
               ELSEIF (INTYP.EQ.3) THEN
                   EOUT(I) = (BCONV(3) * EIN(I))**2
               ENDIF
    1       CONTINUE
C
C-----------------------------------------------------------------------
C  OUTPUT BEAM ENERGY UNITS: AT. UNITS
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            BCONV(1) = EVUATU
            BCONV(2) = 1.0
            BCONV(3) = EVUATU/EVUCMS
C
            DO 2 I=1,IEVAL
               IF (INTYP.EQ.1) THEN
                   EOUT(I) = BCONV(1) * DSQRT(EIN(I))
               ELSEIF (INTYP.EQ.2) THEN
                   EOUT(I) = BCONV(2) * EIN(I)
               ELSEIF (INTYP.EQ.3) THEN
                   EOUT(I) = BCONV(3) * EIN(I)
               ENDIF
    2       CONTINUE
C
C-----------------------------------------------------------------------
C  OUTPUT BEAM ENERGY UNITS: CM SEC-1
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            BCONV(1) = EVUCMS
            BCONV(2) = EVUCMS/EVUATU
            BCONV(3) = 1.0
C
            DO 3 I=1,IEVAL
               IF (INTYP.EQ.1) THEN
                   EOUT(I) = BCONV(1) * DSQRT(EIN(I))
               ELSEIF (INTYP.EQ.2) THEN
                   EOUT(I) = BCONV(2) * EIN(I)
               ELSEIF (INTYP.EQ.3) THEN
                   EOUT(I) = BCONV(3) * EIN(I)
               ENDIF
    3       CONTINUE
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' XXBCON ERROR: INVALID OUTPUT BEAM ENERGY TYPE SPECIFIER'
         ENDIF
      RETURN
      END
