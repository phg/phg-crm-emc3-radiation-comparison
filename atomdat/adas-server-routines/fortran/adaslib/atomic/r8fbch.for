CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8fbch.for,v 1.2 2004/07/06 14:41:24 whitefor Exp $ Date $Date: 2004/07/06 14:41:24 $
CX
      FUNCTION R8FBCH( IZ , XI , ZETA , TE )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8FBCH ******************
C
C  PURPOSE: EVALUATES A SHELL CONTRIBUTION TO THE IONISATION RATE COEFF-
C           ICIENT IN THE BURGESS-CHIDICHIMO APPROXIMATION.
C
C  REFERENCE: MNRAS.(1983)203,1269.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C          (R*8)  R8FBCH  = FUNCTION NAME
C          (I*4)  IZ      = TARGET ION CHARGE NUMBER
C                           (RECOMBINED ION CHARGE).
C          (R*8)  XI      = EFFECTIVE IONISATION POTENTIAL FOR SHELL
C                           (UNITS: RYDBERGS)
C                           (LEVEL ENERGY RELATIVE TO IONISATION POT.)
C          (R*8)  ZETA    = EFFECTIVE NUMBER OF EQUIVALENT ELECTRONS
C                           IN SHELL
C          (R*8)  TE      = ELECTRON TEMPERATURE (IN KELVIN)
C
C          (R*8)  C       = PARAMETER = EQUATION CONSTANT = 2.3
C          (R*8)  DXIPOW  = PARAMETER = EQUATION CONSTANT = 1.5
C          (R*8)  TK2ATE  = PARAMETER = EQUATION CONSTANT = 1.5789D+05
C          (R*8)  R2GAM   = PARAMETER = EQUATION CONSTANT = 2.17161D-08
C          (R*8)  D150    = PARAMETER = 150
C          (R*8)  CR2GAM  = PARAMETER = EQUATION CONSTANT = 'C'*'R2GAM'
C
C          (R*8)  BETA    = EQUATION CONSTANT (SEE NOTE BELOW)
C          (R*8)  Y       =
C          (R*8)  T1      =
C          (R*8)  W       =
C          (R*8)  P       = TEMPORARY PARAMETER STORAGE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8FEEI     ADAS      FUNCTION:
C
C NOTE:
C                         (100*IZ) + 91
C                   SQRT( ------------- ) - 5
C                          (4*IZ) + 3
C           BETA =  -------------------------
C                               4
C
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     04/09/90
C
C VERSION  : 1.2                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8     C                 , DXIPOW             ,
     &           TK2ATE            , R2GAM
      REAL*8     D150
      REAL*8     CR2GAM
C-----------------------------------------------------------------------
      PARAMETER( C      = 2.3D0    , DXIPOW = 1.5D0      ,
     &           TK2ATE = 1.5789D5 , R2GAM  = 2.17161D-8 )
      PARAMETER( D150   = 1.50D2   )
      PARAMETER( CR2GAM = C*R2GAM  )
C-----------------------------------------------------------------------
      INTEGER    IZ
C-----------------------------------------------------------------------
      REAL*8     R8FBCH            , XI                  ,
     &           ZETA              , TE
      REAL*8     R8FEEI            , BETA                ,
     &           Y                 , T1                  ,
     &           W                 , P
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      Y = XI * (TK2ATE/TE)
C-----------------------------------------------------------------------
         IF (Y.LE.D150) THEN
            P      = DBLE( (100*IZ) + 91 ) / DBLE( (4*IZ) + 3 )
            BETA   = 0.25D0 * ( DSQRT(P) - 5.0D0 )
            T1     = (ZETA*DSQRT(Y)*DEXP(-Y)*R8FEEI(Y)) / (XI**DXIPOW)
            P      = 1.0D0 + (1.0D0/Y)
            W      = (DLOG(P))**(BETA/P)
            R8FBCH = CR2GAM*T1*W
         ELSE
            R8FBCH = 0.0D0
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
