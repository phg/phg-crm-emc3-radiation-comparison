C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4lgrp.for,v 1.2 2004/07/06 14:07:28 whitefor Exp $ Date $Date: 2004/07/06 14:07:28 $

      FUNCTION I4LGRP(CHR)
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: I4LGRP *******************
C
C  PURPOSE: RETURNS ANGULAR MOMENTUM QUANTUN NUMBER OF ORBITAL 
C           GIVEN IN THE EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C
C  CALLING PROGRAMS: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :(C*1)  CHR     = HEX. ORBITAL CHARACTER (EG. 'A')
C
C  OUTPUT:(I*4)  I4LGRP  = DECIMAL ORBITAL CHARACTER (E.G. '0')
C
C  AUTHOR:  W.J.DICKSON  
C
C  DATE:    20/10/89
C
C  UPDATE:  19/02/03  H.P. SUMMERS - EXTENDED RANGE
C 
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE.
C
C VERSION: 1.2				DATE: 19/02/03
C MODIFIED: H.P. SUMMERS 
C		- EXTENDED RANGE
C
C-----------------------------------------------------------------------
      CHARACTER*1 CHR    , CHRA(61)
C-----------------------------------------------------------------------
      INTEGER     LGRPA(61)
C-----------------------------------------------------------------------
      DATA CHRA/'1','2','3','4','5','6','7','8','9','A','B',
     &          'C','D','E','F','G','H','I','J','K','L','M',
     &          'N','O','P','Q','R','S','T','U','V','W','X',
     &          'Y','Z','a','b','c','d','e','f','g','h','i',
     &          'j','k','l','m','n','o','p','q','r','s','t',
     &          'u','v','w','x','y','z'/
      DATA LGRPA/0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,
     &           0,1,2,3,4,5,6,0,1,2,3,4,5,6,7,0,1,2,3,4,5,
     &           6,7,8,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C NEXT LINE ADDED TO PRECLUDE WARNINGS FROM F77
      I4LGRP=0

      DO 10 I=1,61
      IF(CHR.EQ.CHRA(I)) THEN
          I4LGRP=LGRPA(I)
          RETURN
      ENDIF
  10  CONTINUE
      RETURN
      END
