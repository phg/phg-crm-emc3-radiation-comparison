       subroutine xxprs3( config , ia_dim     , iz0   , iz    ,
     &                    ia     , ia_vlnce
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxprs3 *********************
c
c  purpose:  To analyse a configuration character string in Standard
c            form into an integer array of occupation numbers in the
c            normal collating order.
c
c            unified version of bbprs3 & g5prs3 and a replacement for
c            these subroutines
c
c  calling program: various
c
c  notes:   1. the normal collating order is 1s,2s,2p,3s,3p,3d etc.
c           2. a strategy for deciding the occupancy of unspecified
c              inner shells is implemented using the nuclear and ion
c              charge.  The strategy may be switchable in a later
c              version.
c           3. opens a central ADAS adf00 file for an element on
c              unit 10.
c
c
c  subroutine:
c
c  input : (c*(*))config   =  configuration string to be parsed
c  input : (i*4)  ia_dim   =  dimensionality of occupancy vector ia
c  input : (i*4)  iz0      =  nuclear charge
c  input : (i*4)  iz       =  ion charge
c
c  output: (i*4)  ia()     =  set of occupation numbers in standard
c                             collating list order
c  output: (i*4)  ia_vlnce =  outer occupied shell index in collating
c                             list
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          i4idfl     adas      unique index from n and l quantum no.
c          xxopen     adas      inquire & open a data file on a unit
c          xxword     adas      parses a string into separate words
c          xxrmve     adas      removes occurrences of a char. in string
c          xxcase     adas      changes a string to upper or lower case
c          xxslen     adas      finds the length of a string excluding
c                               leading and trailing blanks
c          xfesym     adas      fetch the chemical symbol of an element
c
c
c author:  H. P. Summers
c          JA7.08, University of Strathclyde
c          Tel: 0141-548-4196 4941
c
c date:    16/12/02
c
c version : 1.1
c date    : 16-12-2002
c modified: Hugh Summers
c            - First version.
c
c version : 1.2
c date    : 26-08-2009
c modified: Hugh Summers
c            - Changed core filling to prevent false stop.
c
c version : 1.3
c date    : 26-08-2009
c modified: Martin O'Mullane
c            - Change name of first argument to 'config'.
c            - Open central adf00, not ADASUSER version.
c            - Remove verbose printing to screen.
c            - Replace internal 'ind' function with central 'i4idfl'.
c            - Remove redundant variables.
c
c version : 1.4
c date    : 27-08-2009
c modified: Hugh Summers
c            - Check electron count later.
c
c version : 1.5
c date    : 30-06-2010
c modified: Martin O'Mullane
c            - Remove comma before first variable in read statements.
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
       integer     iunt10
c-----------------------------------------------------------------------
       parameter ( iunt10 = 10 )
c-----------------------------------------------------------------------
       integer    ia_dim , iz0   , iz       , ia_vlnce, iq_core
       integer    i      , imax  , j
       integer    n      , l     , iq       , lenstr
       integer    index , i4unit , i4idfl
       integer    nfirst , iwords, maxwrd   , istart  , istop
       integer    indx_min       , indx_max , isum    , isum1
c-----------------------------------------------------------------------
       logical    lexist
c-----------------------------------------------------------------------
       integer    ia(ia_dim)     , i_occupa(66)
       integer    ifirst(20)     , ilast(20)
c-----------------------------------------------------------------------
       character  config*(*)     , strng1*80     , strng2*80
       character  strg*120       , cshel_min*4   , c4*4
       character  xfesym*2       , elem_symb*2   , dsn_adf00*120
       character  adascent*120
c-----------------------------------------------------------------------
       character  c_nshl(35)*1   , c_lshl(22)*1  , c_qshl(36)*1
       character  c_scol(45)*2
c-----------------------------------------------------------------------
       data       c_scol /'1s','2s','2p','3s','3p','3d','4s','4p',
     &                    '4d','4f','5s','5p','5d','5f','5g','6s',
     &                    '6p','6d','6f','6g','6h','7s','7p','7d',
     &                    '7f','7g','7h','7i','8s','8p','8d','8f',
     &                    '8g','8h','8i','8j','9s','9p','9d','9f',
     &                    '9g','9h','9i','9j','9k'/
       data       c_nshl /'1','2','3','4','5','6','7','8','9',
     &                    'a','b','c','d','e','f','g','h','i',
     &                    'j','k','l','m','n','o','p','q','r',
     &                    's','t','u','v','w','x','y','z'/
       data       c_lshl /'s','p','d','f','g','h','i','j','k',
     &                    'l','m','n','o','q','r','t','u','v',
     &                    'w','x','y','z'/
       data       c_qshl /'0','1','2','3','4','5','6','7','8',
     &                    '9','a','b','c','d','e','f','g','h',
     &                    'i','j','k','l','m','n','o','p','q',
     &                    'r','s','t','u','v','w','x','y','z'/
c-----------------------------------------------------------------------

c-------------------------------------------------------
c  convert config to lower case and trim leading blanks
c-------------------------------------------------------

       strng1 = ' '
       strng2 = ' '
       call xxcase( config , strng1    , 'lc'     )
       call xxslen( strng1 , ifirst(1) , ilast(1) )
       lenstr = ilast(1)-ifirst(1)+1
       strng2(1:lenstr) = strng1(ifirst(1):ilast(1))

c-------------------------------------------------------
c  zero output array ia()
c-------------------------------------------------------
       do i=1,ia_dim
         ia(i)=0
       enddo
c-------------------------------------------------------
c  zero working array i_occupa()
c-------------------------------------------------------
       do i=1,66
         i_occupa(i)=0
       enddo
c-------------------------------------------------------------------
c  set variables for minimum and maximum occupied shell index found
c-------------------------------------------------------------------
       indx_min = 66
       indx_max = 0

c--------------------------------------------------------
c  find number of words, check each of length 3 and parse
c  do first check of electron count
c--------------------------------------------------------
       nfirst=1
       maxwrd = 20
       iwords=maxwrd
       call xxword( strng2 , ' '   , nfirst , maxwrd ,
     &              ifirst , ilast , iwords
     &            )

       isum = 0
       do i = 1, iwords
         if ((ilast(i)-ifirst(i)).ne.2) then
             write(i4unit(-1),1001)'shell', i, ' of wrong length'
             write(i4unit(-1),1002)
             stop
          else
              do j=1,35
                if(c_nshl(j).eq.strng2(ifirst(i):ifirst(i))) then
                    n=j
                    go to 10
                endif
              enddo
             write(i4unit(-1),1001)'shell', i, ': n not identified'
             write(i4unit(-1),1002)
             stop
   10        continue
              do j=1,22
                if(c_lshl(j).eq.strng2(ifirst(i)+1:ifirst(i)+1)) then
                    l=j-1
                    go to 20
                endif
              enddo
             write(i4unit(-1),1001)'shell', i, ': l not identified'
             write(i4unit(-1),1002)
             stop
   20        continue

             do j=1,36
                if(c_qshl(j).eq.strng2(ifirst(i)+2:ifirst(i)+2)) then
                    iq=j-1
                    isum=isum+iq
                    if(i4idfl(n,l).lt.indx_min) then
                        indx_min = min0(indx_min,i4idfl(n,l))
                        write(c4,'(1a2,i2)')c_scol(indx_min),iq
                        call xxrmve(c4,cshel_min,' ')
                        call xxslen(cshel_min,istart,istop)
                    endif
                    if(i4idfl(n,l).gt.indx_max) then
                        indx_max = max0(indx_max,i4idfl(n,l))
                    endif
                    go to 30
                endif
              enddo
             write(i4unit(-1),1001)'shell', i, ' iq not identified'
             write(i4unit(-1),1002)
             stop
   30        continue
             i_occupa(i4idfl(n,l))=iq
         endif
       enddo

c-------------------------------------------------------------------
c  fill in inner shell occupancies if required - subject to strategy
c-------------------------------------------------------------------
       if (isum.ne.iz0-iz) then

           elem_symb = xfesym(iz0)
           call getenv("ADASCENT",adascent)
           call xxrmve(adascent//'/adf00/'//
     &               elem_symb//'.dat',
     &               dsn_adf00,' ')

           if(index(dsn_adf00,'adas').le.0)then
               write(i4unit(-1),1003)'ADASCENT environment not set'
               write(i4unit(-1),1002)
               stop
           endif


           lexist = .false.
           call xxopen( iunt10, dsn_adf00 , lexist )
           if (.not.lexist) then
               write(i4unit(-1),1003)dsn_adf00(1:60)
               write(i4unit(-1),1002)
               stop
           endif

           strg=' '
           read(iunt10,'(1a120)')strg(1:120)
           call xxslen(strg,ifirst(1),ilast(1))
           do i=1,iz+1
             strg = ' '
             read(iunt10,'(1a120)')strg
           enddo

           call xxslen(strg,ifirst(1),ilast(1))

           nfirst=1
           maxwrd = 20
           iwords=maxwrd
           call xxword( strg   , ' '   , nfirst , maxwrd ,
     &                  ifirst , ilast , iwords
     &                )


           do i= 3 , iwords
             if(index(strg(ifirst(i):ifirst(i)+1),
     &          cshel_min(istart:istart+1)).gt.0) then
                 imax=i
                 go to 40
             endif
           enddo

c if core not filled by above fill from 1s and count no. electrons
	   isum1 = isum
           do i=3,iwords
               read(strg(ifirst(i)+2:ilast(i)),*)iq_core
               isum1=isum1+iq_core
               if(isum1.eq.iz0-iz) then
                  imax=i+1
                  go to 40
               endif
           enddo

           write(i4unit(-1),1001)' lowest shell', indx_min,
     &                 ' inconsistent with ground config. core.'
           write(i4unit(-1),1002)
           stop

   40      continue

	   if(imax.gt.3) then
	     do i=3,imax-1
	       read(strg(ifirst(i)+2:ilast(i)),*)iq_core
	       isum=isum+iq_core
	       if(isum.le.iz0-iz) then
	           i_occupa(i-2) = iq_core
	       endif   
	     enddo
	   endif

           close(iunt10)

       endif

c-------------------------------------------------------------------
c  find valence shell index and check electron count
c-------------------------------------------------------------------
       do i=66,1,-1
         if(i_occupa(i).gt.0) then
             ia_vlnce = i
             go to 50
         endif
       enddo
   50  continue

       isum = 0
       do i=1,ia_vlnce
         isum=isum+i_occupa(i)
       enddo
       if(isum.ne.iz0-iz) then
           write(i4unit(-1),1001)
     &                'configuration electron count', isum,
     &                                ' is incorrect'
           write(i4unit(-1),1002)
           stop
       endif

c-------------------------------------------------------------------
c  copy back into ia() and verify ia() of sufficient length
c-------------------------------------------------------------------
       if(ia_dim.le.66) then
           do i=1,ia_dim
             ia(i)=i_occupa(i)
           enddo
           do i=ia_dim+1,66
             if(i_occupa(i).gt.0) then
                write(i4unit(-1),1001)
     &                'occupancy vector length required', i,
     &                                ' exceeds ia_dim'
                write(i4unit(-1),1002)
                stop
             endif
           enddo
       else
           do i=1,66
             ia(i)=i_occupa(i)
           enddo
           do i=66,1,-1
             if(i_occupa(i).gt.0) then
                 ia_vlnce = i
                 go to 60
             endif
           enddo
   60  continue
       endif

       return

c-----------------------------------------------------------------------
 1001 format(1x,32('*'),' xxprs3 error ',32('*')//
     &       1x,'fault in config. string: ',a,i3,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,32('*'),' xxprs3 error ',32('*')//
     &       1x,'adf00 file error: ',a)
c-----------------------------------------------------------------------
      end
