CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxecon.for,v 1.2 2004/07/06 15:35:00 whitefor Exp $ Data $Date: 2004/07/06 15:35:00 $
CX
      SUBROUTINE XXECON( INTYP, OUTTYP, IEVAL, EIN, EOUT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXECON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF VELOCITIES/ENERGIES INTO A  SPECIFIED
C           FORM.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  INTYP   = 1 => 'EIN(array)' UNITS: AT. UNITS (VEL.)
C                            = 2 => 'EIN(array)' UNITS: CM/SEC    (VEL.)
C                            = 3 => 'EIN(array)' UNITS: EV/AMU  (ENERGY)
C  INPUT :    (I*4)  OUTTYP  = 1 => 'EOUT(array)' UNITS: AT.UNITS (VEL.)
C                            = 2 => 'EOUT(array)' UNITS: CM/SEC   (VEL.)
C                            = 3 => 'EOUT(array)' UNITS: EV/AMU (ENERGY)
C  INPUT :    (I*4)  IEVAL   = NO. OF VELOCITIES/ENERGIES IN EIN(array)
C  INPUT :    (R*8)  EIN()   = INPUT  VELOCITIES/ENERGIES (STATED UNITS)
C  OUTPUT:    (R*8)  EOUT()  = OUTPUT VELOCITIES/ENERGIES (STATED UNITS)
C
C             (R*8)  AMU2KG  = PARAMETER: AMU TO KG CONVERSION FACTOR
C             (R*8)  EV2J    = PARAMETER: EV TO JOULES CONVERSION FACTOR
C             (R*8)  M2CM    = PARAMETER: METRES TO CM CONVERSION FACTOR
C             (R*8)  VELE0H  = PARAMETER: ORBITAL VELOCITY  (CM/SEC)  OF
C                              AN ELECTRON IN THE SMALLEST  ORBIT  OF  A
C                              HYDROGEN ATOM (BOHR) = 2.1877D+8 CM/SEC
C             (R*8)  AT2VEL  = AT.UNITS (VEL) TO CM/SEC (VEL) CONVERSION
C             (R*8)  VEL2AT  = CM/SEC (VEL) TO AT.UNITS (VEL) CONVERSION
C             (R*8)  VEL2EN  = CM/SEC (VEL) TO EV/AMU (ENGY.) CONVERSION
C             (R*8)  EN2VEL  = EV/AMU (ENGY.) TO CM/SEC (VEL) CONVERSION
C             (R*8)  AT2EN   = AT.UNITS (VEL) TO EV/AMU (ENG) CONVERSION
C             (R*8)  EN2AT   = EV/AMU (ENG) TO AT.UNITS (VEL) CONVERSION
C
C             (I*4)  I       = GENERAL USE
C
C             (R*8)  ECONV() = ENERGY/VELOCITY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            ENERGY/VELOCITY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; ECONV(1) =>  VELOCITY: AT.UNITS -> OUTPUT FORM
C            INTYP = 2 ; ECONV(2) =>  VELOCITY: CM/SEC   -> OUTPUT FORM
C            INTYP = 3 ; ECONV(3) =>  ENERGY  : EV/AMU   -> OUTPUT FORM
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    05/02/91
C
C VERSION: 1.2					DATE: 01-05-96
C MODIFIED: WILLIAM OSBORN
C	    - CORRECTED AT2EN. IT WAS AT2VEL*VEL2EN.
C

C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     AMU2KG                  , EV2J                   ,
     &           M2CM                    , VELE0H
      REAL*8     AT2VEL                  , VEL2AT
      REAL*8     VEL2EN                  , EN2VEL
      REAL*8     AT2EN                   , EN2AT
C-----------------------------------------------------------------------
      PARAMETER( AMU2KG = 1.66053D-27    , EV2J   = 1.602192D-19  ,
     &           M2CM   = 1.0D+02        , VELE0H = 2.1877D+08    )
      PARAMETER( AT2VEL = VELE0H         , VEL2AT = 1.0/VELE0H    )
      PARAMETER( VEL2EN = AMU2KG/( 2.0*EV2J*M2CM*M2CM )           ,
     &           EN2VEL = ( 2.0*EV2J*M2CM*M2CM )/AMU2KG           )
      PARAMETER( AT2EN  = AT2VEL*AT2VEL*VEL2EN                    ,
     &           EN2AT  = EN2VEL*VEL2AT*VEL2AT                    )
C-----------------------------------------------------------------------
      INTEGER    INTYP                  , OUTTYP                  ,
     &           IEVAL
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     EIN(IEVAL)             , EOUT(IEVAL)
      REAL*8     ECONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  OUTPUT VELOCITY UNITS: AT.UNITS
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            ECONV(2) = VEL2AT
            ECONV(3) = EN2AT
C
C-----------------------------------------------------------------------
C  OUTPUT VELOCITY UNITS: CM/SEC
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            ECONV(1) = AT2VEL
            ECONV(3) = EN2VEL
C
C-----------------------------------------------------------------------
C  OUTPUT ENERGY UNITS: EV/AMU
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            ECONV(1) = AT2EN
            ECONV(2) = VEL2EN
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' XXECON ERROR: INVALID OUTPUT VEL./ENERGY TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
         IF (INTYP.EQ.OUTTYP) THEN
            DO 1 I=1,IEVAL
               EOUT(I) = EIN(I)
    1       CONTINUE
         ELSEIF (INTYP.EQ.3) THEN
            DO 2 I=1,IEVAL
               EOUT(I) = DSQRT( ECONV(INTYP) * EIN(I) )
    2       CONTINUE
         ELSEIF (OUTTYP.EQ.3) THEN
            DO 3 I=1,IEVAL
               EOUT(I) = ECONV(INTYP) * EIN(I) * EIN(I)
    3       CONTINUE
         ELSE
            DO 4 I=1,IEVAL
               EOUT(I) = ECONV(INTYP) * EIN(I)
    4       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
