CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/ceigrp.for,v 1.2 2004/07/06 12:03:26 whitefor Exp $ Date $Date: 2004/07/06 12:03:26 $
CX
      FUNCTION CEIGRP(CHR)
C
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: CEIGRP  ********************
C
C  PURPOSE: RETURNS EISSNER CODE FOR AN ORBITAL
C
C
C  INPUT :  (C*2)   CHR    = ORBITAL (EG.  '2S')
C
C  OUTPUT:  (C*1)   CEIGRP = EISSNER CODE
C
C           (I*4)   I      = GENERAL INDEX (EG. 'A')
C
C  SUBROUTINES:  NONE
C  AUTHOR:  W.J. DICKSON, JET
C
C  DATE:    20/10/89

C
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C VERSION: 1.2                          DATE: 19-02-03
C MODIFIED:  H.P. SUMMERS 
C		- EXTENDED RANGE
C           - ALTERED ORBITALS TO INCLUDE 'J'
C           - A POINT OF CONFUSION BETWEEN CONDON
C              & SHORTLEY, COWAN AND ADASDR 
C
C-----------------------------------------------------------------------
      INTEGER      I
C-----------------------------------------------------------------------
      CHARACTER*1  CEIGRP , CHRA(61)
      CHARACTER*2  CHR    , TGRPA(61)   , SGRPA(61)
C-----------------------------------------------------------------------
      DATA CHRA /'1','2','3','4','5','6','7','8','9','A','B',
     &           'C','D','E','F','G','H','I','J','K','L','M',
     &           'N','O','P','Q','R','S','T','U','V','W','X',
     &           'Y','Z','a','b','c','d','e','f','g','h','i',
     &           'j','k','l','m','n','o','p','q','r','s','t',
     &           'u','v','w','x','y','z'/
      DATA TGRPA/'1S','2S','2P','3S','3P','3D','4S','4P','4D','4F','5S',
     &           '5P','5D','5F','5G','6S','6P','6D','6F','6G','6H','7S',
     &           '7P','7D','7F','7G','7H','7I','8S','8P','8D','8F','8G',
     &           '8H','8I','8J','9S','9P','9D','9F','9G','9H','9I','9J',
     &           '9K','AS','AP','AD','AF','AG','AH','AI','AJ','AK','AL',
     &           'BS','BP','BD','BF','BG','BH'/
C     &           '8H','8I','8K','9S','9P','9D','9F','9G','9H','9I','9K',
C     &           '9L','AS','AP','AD','AF','AG','AH','AI','AK','AL','AM',
C     &           'BS','BP','BD','BF','BG','BH'/
      DATA SGRPA/'1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s',
     &           '5p','5d','5f','5g','6s','6p','6d','6f','6g','6h','7s',
     &           '7p','7d','7f','7g','7h','7i','8s','8p','8d','8f','8g',
     &           '8h','8i','8j','9s','9p','9d','9f','9g','9h','9i','9j',
     &           '9k','As','Ap','Ad','Af','Ag','Ah','Ai','Aj','Ak','Al',
     &           'Bs','Bp','Bd','Bf','Bg','Bh'/
C     &           '8h','8i','8K','9s','9p','9d','9f','9g','9h','9i','9k',
C     &           '9l','As','Ap','Ad','Af','Ag','Ah','Ai','Ak','Al','Am',
C     &           'Bs','Bp','Bd','Bf','Bg','Bh'/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      DO 10 I=1,61
        IF(CHR.EQ.TGRPA(I).OR.CHR.EQ.SGRPA(I)) THEN
            CEIGRP=CHRA(I)
            RETURN
        ENDIF
  10  CONTINUE
C
      RETURN
      END
