      function r8giiav(u, u2, n0)

      implicit none
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8GIIAV *****************
C
C  PURPOSE: CALCULATE AVERAGED FREE-BOUND GAUNT FACTOR FOR SUMMED
C  CONTRIBUTIONS OF THE HIGH LYING STATES - SUMMERS AND HOOPER EQU. 19
C
C  INPUT
C      U=HV/IH  WHERE HV IS PHOTON ENERGY
C               AND IH IS THE RYDBERG ENERGY
C      U2=K*TE/IH  WHERE TE IS THE ELECTRON TEMPERATURE
C                  AND K IS THE BOLTZMANN CONSTANT
C      N0       IS THE FIRST LEVEL TO BE COUNTED IN THIS
C               QUASI-CONTINUUM
C  OUTPUT
C      GIIAV=AVERAGED FREE-BOUND GAUNT FACTOR.
C
C
C  AUTHOR   : L D Horton
C  DATE     : 24-06-1997
C
C
C  VERSION  : 1.1
C  DATE     : 02-03-2005
C  MODIFIED : Martin O'Mullane
C                - First version in central ADAS.
C
C  VERSION  : 1.3                          
C  DATE     : 10-04-2007
C  MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NSIMP
C-----------------------------------------------------------------------
      PARAMETER (NSIMP = 400)
C-----------------------------------------------------------------------
      integer    N0
      integer    I
C-----------------------------------------------------------------------
      real*8     r8giiav , r8gbf
      real*8     u       , u2
      REAL*8     X       , XMIN    , XMAX    , EN      , H        ,
     &           U3      , SUM     , GLIM
C-----------------------------------------------------------------------
      external  r8gbf
C-----------------------------------------------------------------------

      XMIN = -1.0D0/(N0*N0*U2)
      XMAX = 0.0D0
      H = (XMAX-XMIN)/(2.0D0*NSIMP)
      EN = N0
      U3 = EN*EN*U - 1.0D0
      SUM = R8GBF(EN,U3)*DEXP(-XMIN)

      DO I = 1,2*NSIMP-1,2
        X = XMIN + H*I
        EN = 1.0D0/DSQRT(-X*U2)
        U3 = EN*EN*U - 1.0D0
        SUM = SUM + 4.0D0*R8GBF(EN,U3)*DEXP(-X)
      ENDDO

      DO I = 2, 2*NSIMP-2,2
        X = XMIN + H*I
        EN = 1.0D0/DSQRT(-X*U2)
        U3 = EN*EN*U - 1.0D0
        SUM = SUM + 2.0D0*R8GBF(EN,U3)*DEXP(-X)
      ENDDO

      GLIM = 1.0D0/ (1.0D0 - 0.2304D0*U**0.3333333D0 +
     &                       0.1076D0*U**0.6666667D0)**0.75D0
      SUM = SUM + GLIM
      R8GIIAV = H/3.0D0*SUM

      RETURN

      END
