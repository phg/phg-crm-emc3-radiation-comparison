CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8bcon.for,v 1.1 2004/07/06 14:40:52 whitefor Exp $ Date $Date: 2004/07/06 14:40:52 $
CX
      FUNCTION R8BCON( INTYP , OUTTYP , EIN )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8ECON ******************
C
C  PURPOSE: TO CONVERT A BEAM ENERGY INTO SPECIFIED UNITS
C           (DOUBLE PRECISION FUNCTION VERSION OF 'XXBCON')
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C             (R*8)  R8BCON  = FUNCTION NAME -
C                              OUTPUT BEAM ENERGY (STATED UNITS)
C             (I*4)  INTYP   = 1 => 'EIN' UNITS: EV/AMU
C                            = 2 => 'EIN' UNITS: AT. UNITS
C                            = 3 => 'EIN' UNITS: CM S-1.
C             (I*4)  OUTTYP  = 1 => 'R8BCON' UNITS: EV/AMU
C                            = 2 => 'R8BCON' UNITS: AT. UNITS
C                            = 3 => 'R8BCON' UNITS: CM S-1.
C             (R*8)  EIN     = INPUT BEAM ENERGY (STATED UNITS)
C
C             (R*8)  EVUATU  = EV/AMU  TO AT. UNITS.
C             (R*8)  EVUCMS  = EV/AMU TO CM S-1.
C
C             (R*8)  BCONV() = BEAM ENERGY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            BEAM ENERGY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; BCONV(1) =>             EV/AMU  -> OUTPUT UNITS
C            INTYP = 2 ; BCONV(2) =>          AT. UNITS  -> OUTPUT UNITS
C            INTYP = 3 ; BCONV(3) =>             CM S-1  -> OUTPUT UNITS
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    21/04/95
C
C UPDATE:  15/05/95	Tim Hammond	UNIX PORT
C			Put under SCCS control
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     EVUATU             , EVUCMS
C-----------------------------------------------------------------------
      PARAMETER( EVUATU=6.32678D-03 , EVUCMS=1.38410D+06 )
C-----------------------------------------------------------------------
      INTEGER    INTYP               , OUTTYP
C-----------------------------------------------------------------------
      REAL*8     R8BCON              , EIN
      REAL*8     BCONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  OUTPUT BEAM ENERGY UNITS: EV/AMU
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            BCONV(1) = 1.0
            BCONV(2) = 1.0/EVUATU
            BCONV(3) = 1.0/EVUCMS
C
            IF (INTYP.EQ.1) THEN
                R8BCON  = BCONV(1) * EIN
            ELSEIF (INTYP.EQ.2) THEN
                R8BCON  = (BCONV(2) * EIN)**2
            ELSEIF (INTYP.EQ.3) THEN
                R8BCON  = (BCONV(3) * EIN)**2
            ENDIF
C
C-----------------------------------------------------------------------
C  OUTPUT BEAM ENERGY UNITS: AT. UNITS
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            BCONV(1) = EVUATU
            BCONV(2) = 1.0
            BCONV(3) = EVUATU/EVUCMS
C
            IF (INTYP.EQ.1) THEN
                R8BCON  = BCONV(1) * DSQRT(EIN)
            ELSEIF (INTYP.EQ.2) THEN
                R8BCON  = BCONV(2) * EIN
            ELSEIF (INTYP.EQ.3) THEN
                R8BCON  = BCONV(3) * EIN
            ENDIF
C
C-----------------------------------------------------------------------
C  OUTPUT BEAM ENERGY UNITS: CM SEC-1
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            BCONV(1) = EVUCMS
            BCONV(2) = EVUCMS/EVUATU
            BCONV(3) = 1.0
C
            IF (INTYP.EQ.1) THEN
                R8BCON  = BCONV(1) * DSQRT(EIN)
            ELSEIF (INTYP.EQ.2) THEN
                R8BCON  = BCONV(2) * EIN
            ELSEIF (INTYP.EQ.3) THEN
                R8BCON  = BCONV(3) * EIN
            ENDIF
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' R8BCON ERROR: INVALID OUTPUT BEAM ENERGY TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
