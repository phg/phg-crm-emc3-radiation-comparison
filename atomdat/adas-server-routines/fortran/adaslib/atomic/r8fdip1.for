C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8fdip1.for,v 1.4 2007/04/23 13:33:41 allan Exp $ Date $Date: 2007/04/23 13:33:41 $
C
       REAL*8 FUNCTION R8FDIP1(E1,L1,E2,L2)                             
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: R8FDIP1 **********************
C
C PURPOSE: CALCULATES THE DIPOLE INTEGRAL I(KAPPA1,L1,KAPPA2,L2,1) FOR 
C          WHERE MIN(E1,E2)/EMAX(E1,E2) < 0.03
C
C NOTE: CREATED BY  ALAN BURGESS FOR USE IN THE DIPOLE INTEGRAL 
C       I(KAPPA1,L1,KAPPA2,L2,1) EVALUATION AS DEFINED IN PHIL. 
C       TRANS. ROY. SOC. A226,255,1970, WHERE E1=KAPPA1**2 AND 
C       E2=KAPPA2**2. APPLIES TO POSITIVE ELECTRON ENERGIES, .  
c       THAT IS THE FREE-FREE CASE.                                              
C
C CALLING PROGRAMS: R8FDIP
C
C INPUT:  (R*8)  E1      = KAPPA1**2 WHERE KAPPA1 IS SCALED INITIAL
C                          ELECTRON WAVE NUMBER 
C INPUT:  (I*4)  L1      = ORBITAL ANGULAR OMENTUM OF INITIAL ELECTRON 
C INPUT:  (R*8)  E2      = KAPPA2**2 WHERE KAPPA2 IS SCALED INITIAL
C                          ELECTRON WAVE NUMBER 
C INPUT:  (I*4)  L2      = ORBITAL ANGULAR OMENTUM OF FINAL ELECTRON 
C
C OUTPUT: (R*8)  R8FDIP1 = I(KAPPA1,L1,KAPPA2,L2,1) 
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8FMON1    ADAS     EVALUATES MONOPOLE INTEGRAL
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C               - FIRST FULLY COMMENTED RELEASE
C
C-----------------------------------------------------------------------
       IF(L1-L2)1,2,3                                                   
    1  L=L1                                                             
       A1=E1                                                            
       A2=E2                                                            
       GO TO 4                                                          
    2  R8FDIP1=0.0D0                                                    
       RETURN                                                           
    3  L=L2                                                             
       A1=E2                                                            
       A2=E1                                                            
    4  LP=L+1                                                           
       ELP=LP                                                           
       B1=DSQRT(1.0D0+ELP*ELP*A2)*R8FMON1(E1,E2,L)                      
       B2=DSQRT(1.0D0+ELP*ELP*A1)*R8FMON1(E1,E2,LP)                     
       IF(B1*B2-1.0D-40)5,5,6                                           
    5  R8FDIP1=0.0D0                                                    
       RETURN                                                           
    6  R8FDIP1=(B1-B2)/ELP                                              
       RETURN                                                           
      END                                                               
