CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxelem.for,v 1.3 2015/05/19 15:50:39 mog Exp $ Date $Date: 2015/05/19 15:50:39 $
CX
      SUBROUTINE XXELEM ( IZ0 , ENAME )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C ******************* FORTRAN77 SUBROUTINE: XXELEM *********************
C
C PURPOSE: TO RETURN THE NAME OF THE ELEMENT WITH NUCLEAR CHARGE IZ0
C
C CALLING PROGRAM: GENERAL USE
C
C SUBROUTINE:
C
C  INPUT : (I*4)  IZ0     = ELEMENT NUCLEAR CHARGE
C  OUTPUT: (C*12) ENAME   = NAME OF ELEMENT WITH NUCLEAR CHARGE 'IZ0'
C
C          (C*12) NAMES() = NAMES OF FIRST 92 ELEMENTS.
C                           ARRAY DIMENSION => NUCLEAR CHARGE
C
C NOTES:    IF NUCLEAR CHARGE IS OUT OF RANGE, I.E.NOT BETWEEN 1 & 92,
C           THEN THE CHARACTER STRING 'ENAME' IS RETURNED BLANK.
C
C ROUTINES: NONE
C
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     05/07/90
C
C UPDATE:   15/01/91 - PE BRIDEN - ADAS91 - ARGUMENTS REVERSED
C
C UPDATE:   25/01/91 - PE BRIDEN - ADAS91 - 'ENAME' & 'NAMES()' DECLARED
C                                           AS C*12 INSTEAD OF C*15.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 21-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2             		DATE: 17-09-99
C            HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C               - INCREASED ELEMENT NUMBER TO 92.
C
C VERSION: 1.3
C DATE   : 19-05-2015
C          Stuart Henderson
C            - Correct spelling of praseodymium.
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER      IZ0
C-----------------------------------------------------------------------
      CHARACTER*12 ENAME , NAMES(92)
C-----------------------------------------------------------------------
      DATA NAMES/'HYDROGEN    ','HELIUM      ','LITHIUM     ',
     &           'BERYLLIUM   ','BORON       ','CARBON      ',
     &           'NITROGEN    ','OXYGEN      ','FLUORINE    ',
     &           'NEON        ','SODIUM      ','MAGNESIUM   ',
     &           'ALUMINIUM   ','SILICON     ','PHOSPHORUS  ',
     &           'SULPHUR     ','CHLORINE    ','ARGON       ',
     &           'POTASSIUM   ','CALCIUM     ','SCANDIUM    ',
     &           'TITANIUM    ','VANADIUM    ','CHROMIUM    ',
     &           'MANGANESE   ','IRON        ','COBALT      ',
     &           'NICKEL      ','COPPER      ','ZINC        ',
     &           'GALLIUM     ','GERMANIUM   ','ARSENIC     ',
     &           'SELENIUM    ','BROMINE     ','KRYPTON     ',
     &           'RUBIDIUM    ','STRONTIUM   ','YTTRIUM     ',
     &           'ZIRCONIUM   ','NIOBIUM     ','MOLYBDENUM  ',
     &           'TECHNETIUM  ','RUTHENIUM   ','RHODIUM     ',
     &           'PALLADIUM   ','SILVER      ','CADMIUM     ',
     &           'INDIUM      ','TIN         ','ANTIMONY    ',
     &           'TELLURIUM   ','IODINE      ','XENON       ',
     &           'CESIUM      ','BARIUM      ','LANTHANUM   ',
     &           'CERIUM      ','PRASEODYMIUM','NEODYMIUM   ',
     &           'PROMETHIUM  ','SAMARIUM    ','EUROPIUM    ',
     &           'GADOLINIUM  ','TERBIUM     ','DYSPROSIUM  ',
     &           'HOLMIUM     ','ERBIUM      ','THULIUM     ',
     &           'YTTERBIUM   ','LUTETIUM    ','HAFNIUM     ',
     &           'TANTALUM    ','TUNGSTEN    ','RHENIUM     ',
     &           'OSMIUM      ','IRIDIUM     ','PLATINUM    ',
     &           'GOLD        ','MERCURY     ','THALLIUM    ',
     &           'LEAD        ','BISMUTH     ','POLONIUM    ',
     &           'ASTATINE    ','RADON       ','FRANCIUM    ',
     &           'RADIUM      ','ACTINIUM    ','THORIUM     ',
     &           'PROTACTINIUM','URANIUM     '/
C-----------------------------------------------------------------------
         IF ( (IZ0.LE.0).OR.(IZ0.GT.92) ) THEN
            ENAME=' '
         ELSE
            ENAME=NAMES(IZ0)
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
