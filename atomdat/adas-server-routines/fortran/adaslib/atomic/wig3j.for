C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/wig3j.for,v 1.2 2005/02/20 17:40:08 mog Exp $ D
C
       REAL*8 FUNCTION WIG3J(A1,A2,A3,B1,B2,B3)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION GAM(500),JGAM(500),K(3),L(2)
       COMMON /FAC/GAM,JGAM
       CALL DELTA2(A1,A2,A3,D,J)
       IF(D-1.0D-20)1,1,2
    1  WIG3J=0.0
       RETURN
    2  IF(A1-DABS(B1)+0.001)1,3,3
    3  IF(A2-DABS(B2)+0.001)1,4,4
    4  IF(A3-DABS(B3)+0.001)1,5,5
    5  I=DABS(B1+B2+B3)+0.999
       IF(I)1,6,1
    6  N1=A1+B1+0.001
       N11=A1+B1+0.501
       N3=A2+B2+0.001
       N31=A2+B2+0.501
       N5=A3+B3+0.001
       N51=A3+B3+0.501
       IF(N1+N3+N5-N11-N31-N51)1,7,1
    7  N2=A1-B1+0.001
       N4=A2-B2+0.001
       N6=A3-B3+0.001
       K(1)=N2
       K(2)=N3
       K(3)=N1+N3-N6
       C1=A3-A2+B1
       IF(C1)8,9,9
    8  L(1)=C1-0.001
       GO TO 10
    9  L(1)=C1+0.001
   10  C1=A3-A1-B2
       IF(C1)11,12,12
   11  L(2)=C1-0.001
       GO TO 13
   12  L(2)=C1+0.001
   13  NMIN=0
       DO 16 I=1,2
       IF(L(I))14,16,16
   14  IF(L(I)+NMIN)15,16,16
   15  NMIN=-L(I)
   16  CONTINUE
       NMAX=K(1)
       DO 18 I=2,3
       IF(K(I)-NMAX)17,18,18
   17  NMAX=K(I)
   18  CONTINUE
       D=D*GAM(N1+1)*GAM(N2+1)*GAM(N3+1)*GAM(N4+1)*GAM(N5+1)*GAM(N6+1)
       D=DSQRT(D)*(-1.0)**(N1-N4+NMIN)
       J=J+JGAM(N1+1)+JGAM(N2+1)+JGAM(N3+1)+JGAM(N4+1)+JGAM(N5+1)
     1 +JGAM(N6+1)
       N1=K(1)-NMIN
       N2=L(1)+NMIN
       N3=K(2)-NMIN
       N4=L(2)+NMIN
       N5=K(3)-NMIN
       D=D/(GAM(NMIN+1)*GAM(N1+1)*GAM(N2+1)*GAM(N3+1)*GAM(N4+1)
     1 *GAM(N5+1))
       M1=JGAM(NMIN+1)+JGAM(N1+1)+JGAM(N2+1)+JGAM(N3+1)+JGAM(N4+1)
     1 +JGAM(N5+1)
       D=D*(8.0)**(J-M1-M1)
       DT=1.0
       DSUM=1.0
       IF(NMAX-NMIN)19,19,20
   19  WIG3J=D
       RETURN
   20  D1=N1+1
       D2=N2
       D3=N3+1
       D4=N4
       D5=N5+1
       D6=NMIN
       NMAX1=NMAX-NMIN
       DO 21 N=1,NMAX1
       D1=D1-1.0
       D2=D2+1.0
       D3=D3-1.0
       D4=D4+1.0
       D5=D5-1.0
       D6=D6+1.0
       DT=-DT*D1*D3*D5/(D2*D4*D6)
       DSUM=DSUM+DT
   21  CONTINUE
       SUM=DSUM
       D=D*SUM
       GO TO 19
      END
