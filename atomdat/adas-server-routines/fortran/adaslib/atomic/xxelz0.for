      SUBROUTINE XXELZ0 ( ELEM , IZ0 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C ******************* FORTRAN77 SUBROUTINE: XXEIZ0 *********************
C
C PURPOSE: TO RETURN THE NUCLEAR CHARGE IZ0 FOR THE ELEMENT NANE
C
C CALLING PROGRAM: GENERAL USE
C
C SUBROUTINE:
C
C  INPUT : (C*2)  ELEN    = ELEMENT NAME WITH NUCLEAR CHARGE 'IZ0'
C  OUTPUT: (I*4)  IZ0     = ELEMENT NUCLEAR CHARGE
C
C          (I*4)  NSYM    = PARAMETER = NUMBER OF SYMBOLS LISTED
C
C          (I*4)  I       = GENERAL ARRAY USE
C
C          (C*2)  SYMBOL()= SYMBOLS OF FIRST 'NSYM' ELEMENTS.
C                           ARRAY DIMENSION => NUCLEAR CHARGE
C
C NOTES:    IF SYMBOL IS NOT RECOGNISED, I.E.NOT IN Z0 RANGE 1 & 'NSYM',
C           THEN THE INTEGER 'IZ0' IS RETURNED AS ZERO.
C
C ROUTINES: NONE
C
C
C AUTHOR:   Martin O'Mullane
C DATE:     25-07-2007
C
C Version : 1.1
C Date    : 25-07-2007
C Modified: Martin O'Mullane
C           - First version.
C
C Version : 1.2
C Date    : 19-05-2015
C Modified: Stuart Henderson
C            - Correct spelling of praseodymium.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      integer    nelem
C-----------------------------------------------------------------------
      parameter( nelem = 92 )
C-----------------------------------------------------------------------
      integer    iz0       , i
C-----------------------------------------------------------------------
      character  elem*(*)  , test*4,  names(nelem)*12
C-----------------------------------------------------------------------
      data names/'HYDROGEN    ','HELIUM      ','LITHIUM     ',
     &           'BERYLLIUM   ','BORON       ','CARBON      ',
     &           'NITROGEN    ','OXYGEN      ','FLUORINE    ',
     &           'NEON        ','SODIUM      ','MAGNESIUM   ',
     &           'ALUMINIUM   ','SILICON     ','PHOSPHORUS  ',
     &           'SULPHUR     ','CHLORINE    ','ARGON       ',
     &           'POTASSIUM   ','CALCIUM     ','SCANDIUM    ',
     &           'TITANIUM    ','VANADIUM    ','CHROMIUM    ',
     &           'MANGANESE   ','IRON        ','COBALT      ',
     &           'NICKEL      ','COPPER      ','ZINC        ',
     &           'GALLIUM     ','GERMANIUM   ','ARSENIC     ',
     &           'SELENIUM    ','BROMINE     ','KRYPTON     ',
     &           'RUBIDIUM    ','STRONTIUM   ','YTTRIUM     ',
     &           'ZIRCONIUM   ','NIOBIUM     ','MOLYBDENUM  ',
     &           'TECHNETIUM  ','RUTHENIUM   ','RHODIUM     ',
     &           'PALLADIUM   ','SILVER      ','CADMIUM     ',
     &           'INDIUM      ','TIN         ','ANTIMONY    ',
     &           'TELLURIUM   ','IODINE      ','XENON       ',
     &           'CESIUM      ','BARIUM      ','LANTHANUM   ',
     &           'CERIUM      ','PRASEODYMIUM','NEODYMIUM   ',
     &           'PROMETHIUM  ','SAMARIUM    ','EUROPIUM    ',
     &           'GADOLINIUM  ','TERBIUM     ','DYSPROSIUM  ',
     &           'HOLMIUM     ','ERBIUM      ','THULIUM     ',
     &           'YTTERBIUM   ','LUTETIUM    ','HAFNIUM     ',
     &           'TANTALUM    ','TUNGSTEN    ','RHENIUM     ',
     &           'OSMIUM      ','IRIDIUM     ','PLATINUM    ',
     &           'GOLD        ','MERCURY     ','THALLIUM    ',
     &           'LEAD        ','BISMUTH     ','POLONIUM    ',
     &           'ASTATINE    ','RADON       ','FRANCIUM    ',
     &           'RADIUM      ','ACTINIUM    ','THORIUM     ',
     &           'PROTACTINIUM','URANIUM     '/
C-----------------------------------------------------------------------
      test = '    '

      do i = 1, min(4, len(elem))
         if ( ichar(elem(i:i)) .ge. ichar('a') .and.
     &        ichar(elem(i:i)) .le. ichar('z')  ) then
            test(i:i)=char(ichar(elem(i:i)) + ichar('A')- ichar('a'))
         else
            test(i:i) = elem(i:i)
         endif
      end do

      iz0 = 0
      do i = 1, nelem
         if ( index(names(i), test).gt.0 ) iz0=i
      end do

C-----------------------------------------------------------------------
      RETURN
      END
