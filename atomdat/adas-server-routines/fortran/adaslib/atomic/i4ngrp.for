CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4ngrp.for,v 1.3 2004/07/06 14:07:42 whitefor Exp $ Date $Date: 2004/07/06 14:07:42 $
CX
      FUNCTION I4NGRP(CHR)
C
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: I4NGRP  ********************
C
C  PURPOSE: RETURNS N QUANTUM NUMBER GIVEN IN THE EISSNER SINGLE
C           HEXADECIMAL CHARACTER FORM
C
C
C  INPUT :  (C*1)   CHR    = HEX. ORBITAL CHARACTER (EG. 'A')
C
C  OUTPUT:  (I*4)   I4NGRP = PRINCIPAL QUANTUM NUMBER FOR ORBITAL
C
C           (I*4)   I      = GENERAL INDEX
C
C  SUBROUTINES:  NONE
C  AUTHOR:  W.J. DICKSON, JET
C
C  DATE:    20/10/89
C
C  UPDATE:  19/02/03  H.P. SUMMERS - EXTENDED RANGE 
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT IN DEFAULT OF ZERO FOR RETURN VALUE.
C
C VERSION: 1.3                          DATE: 19-02-03
C MODIFIED: H.P. SUMMERS 
C		- EXTENDED RANGE 
C
C-----------------------------------------------------------------------
      INTEGER      I
C-----------------------------------------------------------------------
      CHARACTER*1  CHR    , CHRA(61)
C-----------------------------------------------------------------------
      INTEGER      I4NGRP , NGRPA(61)
C-----------------------------------------------------------------------
      DATA CHRA/'1','2','3','4','5','6','7','8','9','A','B',
     &          'C','D','E','F','G','H','I','J','K','L','M',
     &          'N','O','P','Q','R','S','T','U','V','W','X',
     &          'Y','Z','a','b','c','d','e','f','g','h','i',
     &          'j','k','l','m','n','o','p','q','r','s','t',
     &          'u','v','w','x','y','z'/
      DATA NGRPA/   1,   2,   2,   3,   3,   3,   4,   4,   4,   4,   5,
     &              5,   5,   5,   5,   6,   6,   6,   6,   6,   6,   7,
     &              7,   7,   7,   7,   7,   7,   8,   8,   8,   8,   8,
     &              8,   8,   8,   9,   9,   9,   9,   9,   9,   9,   9,
     &              9,  10,  10,  10,  10,  10,  10,  10,  10,  10,  10,
     &             11,  11,  11,  11,  11,  11/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      I4NGRP=0
      DO 10 I=1,61
        IF(CHR.EQ.CHRA(I)) THEN
            I4NGRP=NGRPA(I)
            RETURN
        ENDIF
  10  CONTINUE
C
      RETURN
      END
