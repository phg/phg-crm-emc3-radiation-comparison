CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8zeta.for,v 1.2 2007/04/23 13:33:41 allan Exp $ Date $Date: 2007/04/23 13:33:41 $
CX
      FUNCTION R8ZETA ( ZEFF , N , L )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8ZETA ******************
C
C  PURPOSE: HYDROGENIC SPIN-ORBIT INTERACTION ENERGY ZETA
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8ZETA  = ZETA
C
C  INPUT:  (I*4)  ZEFF    = EFFECTIVE ION CHARGE.
C  INPUT:  (I*4)  N       = PRINCIPAL QUANTUM NUMBER.
C  INPUT:  (I*4)  L       = ORBITAL QUANTUM NUMBER.
C
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XL      = REAL VALUE = L.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    11/10/93
C
C VERSION: 1.1                          DATE: 11-10-93
C MODIFIED: JONATHAN NASH
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C               - FIRST FULLY COMMENTED RELEASE
C
C-----------------------------------------------------------------------
      REAL*8    R8ZETA
C-----------------------------------------------------------------------
      INTEGER   N       , L
C-----------------------------------------------------------------------
      REAL*8    ZEFF
      REAL*8    XN      , XL
C-----------------------------------------------------------------------
C
      XN = DFLOAT( N )
      XL = DFLOAT( L )
C
      R8ZETA = 1.06501D-4 * ZEFF * ( ZEFF / XN )**3
     &         / ( XL * ( 2.0D0 * XL + 1.0D0 ) * ( XL + 1.0D0 ) )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
