      subroutine xxcsaz(cstrg1, cstrg2, ng, lg, np, lp, l_one, l_eis)
      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 function: xxcsaz ***********************
c
c  purpose : analyses a pair of configuration strings in Eissner format
c            to identify if they differ by one orbital and to extract
c            quantum numbers of the differing orbitals.
c
c  calling program: general use
c
c  subroutine:
c
c  input : (c*(*)) cstrg1    = lower configuration string
c  input : (c*(*)) cstrg2    = upper configuration string
c
c  output: (i*4)   ng        = active electron n-shell in lower config.
c  output: (i*4)   lg        = active electron l-shell in lower config.
c  output: (i*4)   np        = active electron n-shell in upper config.
c  output: (i*4)   lp        = active electron l-shell in upper config.
c  output: (i*4)   l_one     = .true.  => single electron orbital shift
c                              .false. => not single electron shift
c  output: (i*4)   l_eis     = .true.  => eissner config. strings
c                              .false. => not eissner config. strings
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          i4eiss     adas      fetch decimal index of eissner orbital
c          i4ndec     adas      fetch n-shell of decimal orbital index
c          i4ldec     adas      fetch l-shell of decimal orbital index
c          xxslen     adas      find first and last characters of string
c
c author:  Hugh Summers
c          JET K1/1/61
c          Tel. 01235-46-4459
c date:    12-08-2009
c
c
c version  : 1.1
c date     : 05-08-2009
c modified : Hugh Summers
c              - first version.
c
c-----------------------------------------------------------------------
      integer  ndshl
c-----------------------------------------------------------------------
      parameter ( ndshl = 61 )
c-----------------------------------------------------------------------
      integer    i4unit   , i4eiss    , i4ndec    , i4ldec
      integer    i        , iq        , ifirst    , ilast
      integer    nshl_1   , nshl_2    , nel_1     , nel_2
      integer    ishl_1   , ishl_2
      integer    ng       , lg        , np        , lp
      integer    icount
c-----------------------------------------------------------------------
      character  cstrg1*(*) , cstrg2*(*)
      character  c80*80
c-----------------------------------------------------------------------
      logical    l_one      , l_eis
c-----------------------------------------------------------------------
      integer    ioccup_1(ndshl)     , ioccup_2(ndshl)
      integer    ioccup_max(ndshl)
c-----------------------------------------------------------------------
      data       ioccup_max/  2,  2,  6,  2,  6, 10,
     &                        2,  6, 10, 14,  2,  6, 10, 14, 18,
     &                        2,  6, 10, 14, 18, 22,
     &                        2,  6, 10, 14, 18, 22, 26,
     &                        2,  6, 10, 14, 18, 22, 26, 30,
     &                        2,  6, 10, 14, 18, 22, 26, 30, 34,
     &                        2,  6, 10, 14, 18, 22, 26, 30, 34, 38,
     &                        2,  6, 10, 14, 18, 22/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------
      ng = 0
      lg = 0
      np = 0
      lp = 0
      l_one=.false.
      l_eis=.true.
      do i=1,ndshl
        ioccup_1(i)=0
        ioccup_2(i)=0
      enddo
c-----------------------------------------------------------------------
c  check if eissner strings then sort into occupancy vectors
c-----------------------------------------------------------------------
      call xxslen(cstrg1,ifirst,ilast)
      if(index(' ',cstrg1(ifirst:ilast)).gt.0)then
          l_eis = .false.
          return
      endif
      if((ilast-ifirst.eq.3).and.index(cstrg1(2:2),'spdfg').gt.0) then
          l_eis = .false.
          return
      endif

      if(mod(ilast-ifirst+1,3).eq.0)then
          c80(1:ilast-ifirst+1)=cstrg1(ifirst:ilast)
          ilast=ilast-ifirst+1
      elseif(mod(ilast-ifirst+1,3).eq.2)then
          c80(1:ilast-ifirst+2)='5'//cstrg1(ifirst:ilast)
          ilast=ilast-ifirst+2
      else
          l_eis = .false.
          return
      endif

      nshl_1 = (ilast-ifirst+1)/3
      nel_1 = 0
      ishl_1 = i4eiss(c80(3:3))

      do i=1,nshl_1
        read(c80(3*i-2:3*i-1),'(i2)')iq
        ioccup_1(i4eiss(c80(3*i:(3*i)))) = mod(iq,50)
        nel_1 = nel_1 + mod(iq,50)
      enddo

c---------------
c  second string
c---------------
      call xxslen(cstrg2,ifirst,ilast)
      if(index(' ',cstrg2(ifirst:ilast)).gt.0)then
          l_eis = .false.
          return
      endif
      if((ilast-ifirst.eq.3).and.index(cstrg2(2:2),'spdfg').gt.0) then
          l_eis = .false.
          return
      endif

      if(mod(ilast-ifirst+1,3).eq.0)then
          c80(1:ilast-ifirst+1)=cstrg2(ifirst:ilast)
          ilast=ilast-ifirst+1
      elseif(mod(ilast-ifirst+1,3).eq.2)then
          c80(1:ilast-ifirst+2)='5'//cstrg2(ifirst:ilast)
          ilast=ilast-ifirst+2
      else
          l_eis = .false.
          return
      endif

      nshl_2 = (ilast-ifirst+1)/3
      nel_2 = 0
      ishl_2 = i4eiss(c80(3:3))

      do i=1,nshl_2
        read(c80(3*i-2:3*i-1),'(i2)')iq
        ioccup_2(i4eiss(c80(3*i:3*i))) = mod(iq,50)
        nel_2 = nel_2 + mod(iq,50)
      enddo

c-----------------------------------------------------------------------
c check electron count and fill in core filled shell differences
c-----------------------------------------------------------------------
      if((ishl_1.lt.ishl_2).and.(nel_1.ne.nel_2)) then
          do i=ishl_1,ishl_2-1
	    ioccup_2(i)= ioccup_max(i)
	    nel_2 = nel_2+ioccup_max(i)
	  enddo
      elseif(ishl_1.gt.ishl_2) then
          do i=ishl_2,ishl_1-1
	    ioccup_1(i)= ioccup_max(i)
	    nel_1 = nel_1+ioccup_max(i)
	  enddo
      endif


      if(nel_1.ne.nel_2) then
          write(i4unit(-1),1000)'nel_1 = ',nel_1,'  ne  nel_2 = ',nel_2
	  write(i4unit(-1),*)'cstrg1=',cstrg1
	  write(i4unit(-1),*)'cstrg2=',cstrg2
          write(i4unit(-1),1001)
          stop
      endif
c-----------------------------------------------------------------------
c  analyse occupancy vectors
c-----------------------------------------------------------------------

      icount=0
      do i=1,ndshl
        if((ioccup_1(i)-ioccup_2(i)).eq.1) then
            ng=i4ndec(i)
            lg=i4ldec(i)
            icount=icount+1
        elseif((ioccup_1(i)-ioccup_2(i)).eq.-1) then
            np=i4ndec(i)
            lp=i4ldec(i)
            icount=icount+1
        elseif(abs(ioccup_1(i)-ioccup_2(i)).gt.1) then
            l_one=.false.
            return
        endif
      enddo
      if(icount.ne.2) then
          l_one=.false.
          return
      else
          l_one=.true.
      endif


      return

c-----------------------------------------------------------------------
c
 1000 format(1x,32('*'),' xxcsaz error ',32('*')//
     &       1x,'fault in input data: ',a,i4,a,i4)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
