CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxeryd.for,v 1.1 2004/07/06 15:35:48 whitefor Exp $ Date $Date: 2004/07/06 15:35:48 $
CX
      SUBROUTINE XXERYD( BWNO , IL , WA  ,
     &                          ER , XIA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXERYD *********************
C
C  PURPOSE: TO CALCULATE THE ENERGY LEVELS IN RYDBERGS ( FROM WAVE NUM-
C           BERS) RELATIVE TO LEVEL 1, AND THE ENERGIES (ALSO IN RYD.)
C           RELATIVE TO THE IONISATION POTENTIAL.
C
C  CALLING PROGRAM:  GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  BWNO    = IONISATION POTENTIAL (CM-1)
C  INPUT :    (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT :    (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C
C  OUTPUT:    (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C  OUTPUT:    (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C
C             (R*8)  WN2RYD  = WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C                              (PARAMETER)
C
C             (I*4)  I       = GENERAL USE
C
C ROUTINES:  NONE
C
C NOTES:     ION. POT. = IONISATION POTENTIAL
C
C AUTHOR:    PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C            K1/0/81
C            JET EXT. 4569
C
C DATE:      05/07/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER IL     , I
C-----------------------------------------------------------------------
      REAL*8  WN2RYD
      REAL*8  BWNO
      REAL*8  WA(IL) , ER(IL) , XIA(IL)
C-----------------------------------------------------------------------
      PARAMETER ( WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 I=1,IL
            ER(I)  = WN2RYD*WA(I)
            XIA(I) = WN2RYD*(BWNO-WA(I))
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
