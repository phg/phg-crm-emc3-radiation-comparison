CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxeiz0.for,v 1.2 2004/07/06 15:35:27 whitefor Exp $ Date $Date: 2004/07/06 15:35:27 $
CX
      SUBROUTINE XXEIZ0 ( ESYM , IZ0 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C ******************* FORTRAN77 SUBROUTINE: XXEIZ0 *********************
C
C PURPOSE: TO RETURN THE NUCLEAR CHARGE IZ0 FOR THE ELEMENT SYMBOL ESYM
C
C CALLING PROGRAM: GENERAL USE
C
C SUBROUTINE:
C
C  INPUT : (C*2)  ESYM    = SYMBOL OF ELEMENT WITH NUCLEAR CHARGE 'IZ0'
C  OUTPUT: (I*4)  IZ0     = ELEMENT NUCLEAR CHARGE
C
C          (I*4)  NSYM    = PARAMETER = NUMBER OF SYMBOLS LISTED
C
C          (I*4)  I       = GENERAL ARRAY USE
C
C          (C*2)  SYMBOL()= SYMBOLS OF FIRST 'NSYM' ELEMENTS.
C                           ARRAY DIMENSION => NUCLEAR CHARGE
C
C NOTES:    IF SYMBOL IS NOT RECOGNISED, I.E.NOT IN Z0 RANGE 1 & 'NSYM',
C           THEN THE INTEGER 'IZ0' IS RETURNED AS ZERO.
C
C ROUTINES: NONE
C
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     21/08/90
C
C UPDATE:     15/01/91 - PE BRIDEN - ADAS91 - ARGUMENTS REVERSED
C
C MODIFIED:
C
C VERSION:  1.1	TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)  14/09/95
C	    -	FIRST RELEASE (UNIX PORT)
C
C VERSION:  1.2 HUGH SUMMERS				     17/09/99
C           -   INCREASED ELEMENT NUMBER TO 92
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NSYM
C-----------------------------------------------------------------------
      PARAMETER( NSYM = 92 )
C-----------------------------------------------------------------------
      INTEGER    IZ0       , I
C-----------------------------------------------------------------------
      CHARACTER  ESYM*2         , SYMBUC(NSYM)*2 , 
     &           SYMBLC(NSYM)*2 , SYMBOL(NSYM)*2
C-----------------------------------------------------------------------
      DATA SYMBOL/'H ','He','Li','Be','B ','C ','N ','O ','F ','Ne',
     &            'Na','Mg','Al','Si','P ','S ','Cl','Ar','K ','Ca',
     &            'Sc','Ti','V ','Cr','Mn','Fe','Co','Ni','Cu','Zn',
     &            'Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y ','Zr',
     &            'Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn',
     &            'Sb','Te','I ','Xe','Cs','Ba','La','Ce','Pr','Nd',
     &            'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb',
     &            'Lu','Hf','Ta','W ','Re','Os','Ir','Pt','Au','Hg',
     &            'Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th',
     &            'Pa','U '/
      DATA SYMBLC/'h ','he','li','be','b ','c ','n ','o ','f ','ne',
     &            'na','mg','al','si','p ','s ','cl','ar','k ','ca',
     &            'sc','ti','v ','cr','mn','fe','co','ni','cu','zn',
     &            'ga','ge','as','se','br','kr','rb','sr','y ','zr',
     &            'nb','mo','tc','ru','rh','pd','ag','cd','in','sn',
     &            'sb','te','i ','xe','cs','ba','la','ce','pr','nd',
     &            'pm','sm','eu','gd','tb','dy','ho','er','tm','yb',
     &            'lu','hf','ta','w ','re','os','ir','pt','au','hg',
     &            'tl','pb','bi','po','at','rn','fr','ra','ac','th',
     &            'pa','u '/
      DATA SYMBUC/'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE',
     &            'NA','MG','AL','SI','P ','S ','CL','AR','K ','CA',
     &            'SC','TI','V ','CR','MN','FE','CO','NI','CU','ZN',
     &            'GA','GE','AS','SE','BR','KR','RB','SR','Y ','ZR',
     &            'NB','MO','TC','RU','RH','PD','AG','CD','IN','SN',
     &            'SB','TE','I ','XE','CS','BA','LA','CE','PR','ND',
     &            'PM','SM','EU','GD','TB','DY','HO','ER','TM','YB',
     &            'LU','HF','TA','W ','RE','OS','IR','PT','AU','HG',
     &            'TL','PB','BI','PO','AT','RN','FR','RA','AC','TH',
     &            'PA','U '/
C-----------------------------------------------------------------------
      IZ0=0
         DO 1 I=1,NSYM
            IF ( (ESYM.EQ.SYMBOL(I)) .OR.
     &           (ESYM.EQ.SYMBLC(I)) .OR.
     &           (ESYM.EQ.SYMBUC(I))) THEN
               IZ0=I
            ENDIF
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
