C   SCCS Info Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/ceprep.for,v 1.4 2012/07/23 10:06:37 mog Exp $ Date: $Date: 2012/07/23 10:06:37 $   
C
      FUNCTION ceprep(str)
      IMPLICIT NONE
C---------------------------------------------------------------------
C                                                                     
C  ****************** FORTRAN 77 FUNCTION: CEPREP ********************
C
C  PURPOSE: Prepares a string for passsing to xxdtes. Takes C*18
C           and returns C*19; add a space to start if not d10 or 
C           f10-f14. In this case a space is added at the end.
C
C  CALLING PROGRAM: VARIOUS
C
C  SUBROUTINE:
C
C  INPUT  :  (C*18)   str    = input configuration (EISSNER)
C  OUTPUT :  (C*19)   ceprep = lengthened configuration
C
C
C  ROUTINES: NONE
C
C  AUTHOR   :  Martin O'Mullane
C  DATE     :  21-11-2003
C
C
C  VERSION  : 1.1
C  DATE     : 21-11-2003
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C  VERSION  : 1.2
C  DATE     : 15-11-2004
C  MODIFIED : Martin O'Mullane
C              - Do not change configuration if first character is a space.
C
C  VERSION  : 1.3
C  DATE     : 18-07-2012
C  MODIFIED : Martin O'Mullane
C              - Allow calling program to set the size of str and output.
C
C  VERSION  : 1.4
C  DATE     : 20-07-2012
C  MODIFIED : Martin O'Mullane
C              - For Eissner configuration with a leading 5 do not 
C                add a leading space.
C
C-------------------------------------------------------------------------------
      INTEGER    L1      , L2
C-------------------------------------------------------------------------------
      CHARACTER  str*(*)  , ceprep*(*),  test*2
C-------------------------------------------------------------------------------

      if (str(1:1).EQ.' ') then
         ceprep = str
         return
      endif

      call xxslen(str, L1, L2)

      test = str(1:2)
      
      if (mod(L2-L1+1,3).EQ.0) then
          if (test.EQ.'60'.OR.
     &        test.EQ.'61'.OR.
     &        test.EQ.'62'.OR.
     &        test.EQ.'63'.OR.
     &        test.EQ.'64')    then
             ceprep = str//' '
          endif
          if (str(1:1).EQ.'5') ceprep = str//' '
      else
         ceprep = ' '//str
      endif

      return
      
      END
