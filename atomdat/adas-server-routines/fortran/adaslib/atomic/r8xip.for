CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8xip.for,v 1.2 2007/04/23 13:40:45 allan Exp $ Date $Date: 2007/04/23 13:40:45 $
CX
      FUNCTION R8XIP( XI , DELTA )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8XIP  ******************
C
C  PURPOSE: EVALUATES IMPACT PARAMETER CROSS-SECTION FIRST BESSEL   
C           INTEGRAL X [BURGESS AND SUMMERS: MNRAS (1976) 
C           172,345 - EQN C12]   
C
C  CALLING PROGRAMS: EIQIP, ZERO1, CXEIQP,CXZERO
C
C  INPUT:  (R*8)  XI      = Z[1/KN-1/KN1]/A0 WITH Z TARGET CHARGE, 
C                              KN, KN1 INITIAL AND FINAL ELECTRON WAVE
C                              NUMBERS AND A0 THE BOHR RADIUS
C  INPUT:  (R*8)  DELTA   = RC[KN-KN1] WITH RC THE CLOSEST APPROACH
c
C  OUTPUT: (R*8)  R8XIP   = X(XI,DELTA) 
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    07/10/93
C
C VERSION 1.1                             DATE: 07/10/93
C MODIFIED: JONATHAN NASH
C           - FIRST VERSION
C VERSION 1.2                             DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C           - COMPLETED COMMENT BLOCK DESCRIPTION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8    R8XIP
C-----------------------------------------------------------------------
      REAL*8    XI      , DELTA
      REAL*8    XM      , X1      , X2      , X3
C-----------------------------------------------------------------------
C
      XM = DABS( XI )
      X1 = 3.142D0 * ( DELTA + 0.25D0 + 0.03125D0 / ( DELTA + 0.5D0 ) )
     &     * DEXP( -2.0D0 * DELTA + 1.1416D0 * ( XM + XM**2 )
     &           / ( 1.0D0 + 0.6D0 * XM + 3.0D0 * XM**2
     &             / ( 6.0D0 + 2.0D0 * DELTA ) ) )
      X2 = 3.0D0 / ( XM + 3.0D0 )
      X3 = 0.2D0 / ( XM + 0.4D0 )
C
      R8XIP = X1 * (1.0D0 - X2 + X2**2 + X3 - 2.0D0 * X3**2 )
C
      IF (XI .LT. 0.0D0) THEN
         R8XIP = R8XIP * DEXP( 6.283D0 * XI )
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
