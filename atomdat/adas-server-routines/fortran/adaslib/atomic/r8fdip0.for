      function r8fdip0(e1,l1,e2,l2,eps)
      implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 program: r8fdip0.for *******************
C
C  purpose: calculates the function i0(k1,l1,k2,l2,1) defined in
C           Phil. Trans. Roy. Soc. a266,255,1970, where
C           e1=k1*k1, e2=k2*k2, and the relative accuracy is
C           approximately eps.
C
C           It is suitable for use in equations (13) etc. of
C           J.Phys.B. 7,l364,1974
C           (original by A. Burgess, DAMTP, University of Cambridge)
C
C
C  subroutine:
C
C  input : (r*8)  e1       = initial electron energy (Ryd)
C  input : (r*8)  l1       = inital orbital angular momentum
C  input : (r*8)  e2       = final electron energy (Ryd)
C  input : (r*8)  l2       = final orbital angular momentum
C  input : (r*8)  eps      = accuracy setting
C
C  output: (r*8)  r8fdip0  = dipole matrix element for neutral atom
C
C
C  Routines:
C          routine    source    brief description
C          -------------------------------------------------------------
C          f21        adas      special quadrature for Burgess codes
C          i4unit     adas      fetch unit number for output of messages
C
C
C  Author:  H. P. Summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   24/02/03
C
C  Update: HP Summers    24/05/04
C          restructure and addded standard warning
C  Update: AD Whiteford  16/03/05
C          renamed to r8fdip
C          Now calls r8f21 instead of just f21, this routine was renamed
C  Update: AD Whiteford  17/05/07
C          Updated comments as part of subroutine documentation
C          procedure.
C
C-----------------------------------------------------------------------
      integer  i4unit
      integer  l1     , l2
      integer  l      , i0    , i
C-----------------------------------------------------------------------
      real*8   r8fdip0, r8f21
      real*8   e1     , e2    , eps
      real*8   el     , e     , p     , p1    , t     , h0    , ti    ,
     &         x      , h     , s     , a     , b     , c     , d     ,
     &         f      , c1    , ai    , aii   , t1
C-----------------------------------------------------------------------
      if(l1-l2)1,2,4
 1    l=l1
      go to 5
 2    write(i4unit(-1),2000)' failed in r8fdip0, l1=l2=',l1
      write(i4unit(-1),2001)
      r8fdip0=0.0d0
 3    return
 4    l=l2
 5    el=l
      r8fdip0=0.5d0/(el+1.0d0)
      if(e1-e2)6,3,7
 6    e=e1/e2
      p=l1-l
      go to 8
 7    e=e2/e1
      p=l2-l
 8    r8fdip0=r8fdip0*e**((el+p+0.5d0)*0.5d0)
C---------------------------------------------------------------------
C To obtain the function e1 of M.J. Seaton, Proc. Phys. Soc. A68,457,
C 1955, remove the 'C' on the next line.
C---------------------------------------------------------------------
C     r8fdip0=1.0d0
C---------------------------------------------------------------------
      if(e-0.5d0)21,20,20
 20   p1=p-0.5d0
      t=p1*(el+1.0d0)*(e-1.0d0)
      i0=l+1
      h0=0.0d0
      do 9 i=1,i0
      ti=i
      h0=h0+1.0d0/ti
 9    continue
      x=1.0d0-e
      h=1.0d0-(p+p+h0+dlog(0.25d0*x))
      s=1.0d0+t*h
      a=el+1.0d0
      b=p1
      c=1.0d0
      d=0.0d0
 10   a=a+1.0d0
      b=b+1.0d0
      c=c+1.0d0
      d=d+1.0d0
      t=t*a*b*x/(c*d)
      h=h+p1/(d*b)+el/(c*a)
      t1=t*h
      s=s+t1
      if(dabs(t1)-eps*dabs(s))13,11,11
 11   if(c-300.0d0)10,12,12
 12   write(i4unit(-1),2000)' failed to converge in r8fdip0'
      write(i4unit(-1),2001)
 13   r8fdip0=r8fdip0*s
      return
 21   a=el+1.0d0
      b=p-0.5d0
      c=el+p+1.5d0
      f=r8f21(a,b,c,e,eps)
      l=l+1
      el=l
      if(p-0.5d0)23,23,24
 23   c1=el+el+1.0d0
      go to 25
 24   c1=1.0d0
 25   do 22 i=1,l
      ai=i
      aii=ai+ai
      c1=c1*ai*ai*4.0d0/(aii*(aii+1.0d0))
 22   continue
      r8fdip0=r8fdip0*f*c1
      return
C-----------------------------------------------------------------------
 2000 format(1x,32('*'),' r8dip0 warning  ',32('*')//
     &       2x,a,2x,i5 )
 2001 format(/1x,29('*'),' program continues ',29('*'))
C-----------------------------------------------------------------------
      end
