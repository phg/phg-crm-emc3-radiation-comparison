C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4eiss.for,v 1.2 2004/07/06 14:06:20 whitefor Exp $ DATE $Date: 2004/07/06 14:06:20 $

      FUNCTION I4EISS( CHR )
      IMPLICIT NONE
C---------------------------------------------------------------------
C                                                                       ********
C  ****************** FORTRAN 77 FUNCTION: I4EISS ********************  ********
C
C  PURPOSE: RETURNS DECIMAL INDEX OF AN ORBITAL GIVEN IN THE EISSNER
C           HEXADECIMAL CHARACTER FORM.
C
C  CALLING PROGRAM: VARIOUS
C
C  SUBROUTINE:
C
C  INPUT  :  (C*1)   CHR    = HEX. ORBITAL CHARACTER (EISSNER)
C  OUTPUT :  (I*4)   I4EISS = DECIMAL ORBITAL INDEX (1 - 61)
C
C            (I*4)   I      = GENERAL INDEX
C            (C*1)   CHRA() = EISSNER HEXADECIMAL ORBITAL LIST
C
C  ROUTINES: NONE
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    04/06/96
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    20TH AUGUST 1996
C
C VERSION: 1.1				DATE: 20-08-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. SET DEFAULT VALUE OF I4EISS TO ZERO 
C VERSION: 1.2				DATE: 19-02-03
C MODIFIED: H.P. SUMMERS - EXTENDED RANGE 
C-------------------------------------------------------------------------------
      INTEGER    I     , I4EISS
C-------------------------------------------------------------------------------
      CHARACTER  CHR*1
C-------------------------------------------------------------------------------
      CHARACTER  CHRA(61)*1
C-------------------------------------------------------------------------------
      DATA CHRA /'1','2','3','4','5','6','7','8','9','A','B',
     &           'C','D','E','F','G','H','I','J','K','L','M',
     &           'N','O','P','Q','R','S','T','U','V','W','X',
     &           'Y','Z','a','b','c','d','e','f','g','h','i',
     &           'j','k','l','m','n','o','p','q','r','s','t',
     &           'u','v','w','x','y','z'/
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
      I4EISS = 0
      DO 10 I=1,61
      IF(CHR.EQ.CHRA(I)) THEN
          I4EISS = I
          RETURN
      ENDIF
  10  CONTINUE
      RETURN
C
      END
