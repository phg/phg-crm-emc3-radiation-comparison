CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8qp.for,v 1.2 2007/04/23 13:33:41 allan Exp $ Date $Date: 2007/04/23 13:33:41 $
CX
      FUNCTION R8QP ( N , L )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8QP   ******************
C
C  PURPOSE: EXPECTATION VALUE <1/R^6> IN HYDROGEN APPROXIMATION FOR USE
c           IN QUADRUPOLE POLARISABILITY ENERGY SHIFT EVALUATION
C
C  CALLING PROGRAM: CXLTHE, CXSODE
C
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8QP    = EXPECTATION VALUE <1/R^6>
C
C  INPUT:  (I*4)  N       = PRINCIPAL QUANTUM NUMBER.
C  INPUT:  (I*4)  L       = ORBITAL QUANTUM NUMBER.
C
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XL      = REAL VALUE = L.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    02/11/93
C
C VERSION: 1.1                          DATE: 02-11-93
C MODIFIED: JONATHAN NASH
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C               - FIRST FULLY COMMENTED RELEASE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8    R8QP
C-----------------------------------------------------------------------
      INTEGER   N       , L
C-----------------------------------------------------------------------
      REAL*8    XN      , XL
C-----------------------------------------------------------------------
C
      XN = DFLOAT( N )
      XL = DFLOAT( L )
C
      R8QP = ( 35.0D0 * XN**4 - 5.0D0 * XN**2 * ( 6.0D0 * XL *
     &       ( XL + 1.0D0 ) - 5.0D0 ) + 3.0D0 * ( XL - 1.0D0 ) *
     &       XL * ( XL + 1.0D0 ) * ( XL + 2.0D0 ) ) /
     &       ( 8.0D0 * XN**7 * ( XL - 1.5D0 ) * ( XL - 1.0D0 ) *
     &       ( XL - 0.5D0 ) * XL * ( XL + 0.5D0 ) * ( XL + 1.0D0 ) *
     &       ( XL + 1.5D0 ) * (XL + 2.0D0 ) * ( XL + 2.5D0 ) )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
