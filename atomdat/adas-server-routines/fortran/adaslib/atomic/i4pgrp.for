CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4pgrp.for,v 1.3 2004/07/06 14:07:52 whitefor Exp $ Date $Date: 2004/07/06 14:07:52 $
CX
      FUNCTION I4PGRP(CHR)
C
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: I4PGRP  ********************
C
C  PURPOSE: RETURNS PARITY OF ORBITAL GIVEN THE EISSNER SINGLE
C           HEXADECIMAL CHARACTER FORM
C
C
C  INPUT :  (C*1)   CHR    = HEX. ORBITAL CHARACTER (EG. 'A')
C
C  OUTPUT:  (I*4)   I4PGRP = PARITY FOR ORBITAL
C
C           (I*4)   I      = GENERAL INDEX
C
C  SUBROUTINES:  NONE
C  AUTHOR:  W.J. DICKSON, JET
C
C  DATE:    20/10/89
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT IN DEFAULT OF ZERO FOR RETURN VALUE
C
C VERSION: 1.2                          DATE: 19-02-03
C MODIFIED: H.P. SUMMERS 
C		- EXTENDED RANGE 
C
C-----------------------------------------------------------------------
      INTEGER      I
C-----------------------------------------------------------------------
      CHARACTER*1  CHR    , CHRA(61)
C-----------------------------------------------------------------------
      INTEGER      I4PGRP , IPGRPA(61)
C-----------------------------------------------------------------------
       DATA CHRA/'1','2','3','4','5','6','7','8','9','A','B',
     &           'C','D','E','F','G','H','I','J','K','L','M',
     &           'N','O','P','Q','R','S','T','U','V','W','X',
     &           'Y','Z','a','b','c','d','e','f','g','h','i',
     &           'j','k','l','m','n','o','p','q','r','s','t',
     &           'u','v','w','x','y','z'/
      DATA IPGRPA/0,0,1,0,1,0,0,1,0,1,0,1,0,1,0,0,1,0,1,0,1,
     &            0,1,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,
     &            0,1,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      I4PGRP=0
      DO 10 I=1,61
        IF(CHR.EQ.CHRA(I)) THEN
            I4PGRP=IPGRPA(I)
            RETURN
        ENDIF
  10  CONTINUE
C
      RETURN
      END
