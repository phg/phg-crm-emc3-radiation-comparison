CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8scon.for,v 1.1 2004/07/06 14:42:48 whitefor Exp $ Data $Date: 2004/07/06 14:42:48 $
CX
      FUNCTION R8SCON( INTYP, OUTTYP, XSIN )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: R8SCON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF CROSS-SECTIONS INTO A SPECIFIED FORM.
C           (DOUBLE PRECISION FUNCTION VERSION OF 'XXSCON')
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C             (R*8)  R8SCON  = FUNCTION NAME -
C                              OUTPUT CROSS-SECTION (STATED UNITS)
C             (I*4)  INTYP   = 1 => 'XSIN' UNITS: CM**2
C                            = 2 => 'XSIN' UNITS: PI*(A0**2)
C             (I*4)  OUTTYP  = 1 => 'R8SCON' UNITS: CM**2
C                            = 2 => 'R8SCON' UNITS: PI*(A0**2)
C             (R*8)  XSIN    = INPUT  CROSS-SECTION (STATED UNITS)
C
C             (R*8)  A0      = PARAMETER: BOHR RADIUS = 5.29177D-09 cm
C             (R*8)  PI      = PARAMETER: Pi = 3.1415926536
C             (R*8)  CM2A0   = PARAMETER: CM**2 TO PI*(A0**2) CONVERSION
C                                         FACTOR.
C             (R*8)  A02CM   = PARAMETER: PI*(A0**2) TO CM**2 CONVERSION
C                                         FACTOR.
C
C             (R*8)  SCONV() = ENERGY/VELOCITY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            ENERGY/VELOCITY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; SCONV(1) =>  VELOCITY: CM**2     -> OUTPUT FORM
C            INTYP = 2 ; SCONV(2) =>  VELOCITY: PI*(A0**2)-> OUTPUT FORM
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    05/02/91
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     A0                      , PI
      REAL*8     A02CM                   , CM2A0
C-----------------------------------------------------------------------
      PARAMETER( A0     = 5.29177D-09    , PI     = 3.1415926536  )
      PARAMETER( A02CM  = PI*A0*A0       , CM2A0  = 1.0/A02CM     )
C-----------------------------------------------------------------------
      INTEGER    INTYP                   , OUTTYP
C-----------------------------------------------------------------------
      REAL*8     R8SCON                  , XSIN
      REAL*8     SCONV(2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  OUTPUT CROSS-SECTION UNITS: CM**2
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            SCONV(1) = 1.0
            SCONV(2) = A02CM
C
C-----------------------------------------------------------------------
C  OUTPUT CROSS-SECTION UNITS: PI*(A0**2)
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            SCONV(1) = CM2A0
            SCONV(2) = 1.0
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' R8SCON ERROR: INVALID OUTPUT CROSS-SECTION TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
      R8SCON = SCONV(INTYP) * XSIN
C-----------------------------------------------------------------------
      RETURN
      END
