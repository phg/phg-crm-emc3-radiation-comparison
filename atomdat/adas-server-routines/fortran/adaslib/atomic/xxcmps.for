      subroutine xxcmps( csta , cstb  , lequiv  )

      implicit none
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXCMPS *********************
C
C  PURPOSE: COMPARES TWO CHARACTER CONFIGURATION STRINGS IN
C           STANDARD FORM AND DETECTS IF THEY ARE EQUIVALENT INDEPENDENT
C           OF THE ORBITALS BEING IN UPPER OR LOWER CASE.
C
C  CALLING PROGRAMS: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C*)    CSTA    = FIRST CONFIGURATION CHARACTER STRING
C  INPUT : (C*)    CSTB    = SECOND CONFIGURATION CHARACTER STRING
C  OUTPUT: (L*4)   LEQUIV  = .TRUE.  => CONFIGURATION STRINGS EQUIVALENT
C                            .FALSE. => STRINGS NOT EQUIVALENT
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXWORD     ADAS     FINDS NUMBER OF WORDS IN A STRING
C          XXCASE     ADAS     CHANGES CASE OF A STRING
C          I4SCHR     ADAS     returns numerical value for number of
C                              equivalent electrons given as hex char.
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    01/10/96
C
C
C UPDATE:
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 14-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SOFTWARE CONTROL
C
C VERSION: 1.2                          DATE: 28-08-97
C MODIFIED: HUGH SUMMERS
C               - EXTENDED TO INCLUDE 'G' STATES
C
C VERSION: 1.3                          DATE: 26-06-98
C MODIFIED: RICHARD MARTIN
C                   - EDITED TO MAKE SURE 'IF' STATEMENT DOESN'T SPAN > 20
C                     LINES OF TEXT -CRASHES ON SUNOS 4.1.x
C
C Version : 1.4                          
C Date    : 14-05-02
C Modified: Hugh Summers
C              - improved upper/lower case handling logic.
C              - extend to arbitrary length strings.
C
C Version : 1.5
C Date    : 30-11-11
C Modified: Hugh Summers
C              - amended faulty logic between upper and lower cases
C              - increased ndword
C
C Version : 1.6
C Date    : 09-05-2019
C Modified: Martin O'Mullane
C              - compare number of electrons with i4schr() to
C                eliminate case problems when number is > 9.
C
C-----------------------------------------------------------------------
      integer   ndword
c-----------------------------------------------------------------------
      parameter ( ndword = 20 )
c-----------------------------------------------------------------------
      integer   i        , nfirst     , iwords      , nwords
      integer   i4schr   , ierr       , na          , nb
c-----------------------------------------------------------------------
      character csta*(*) , cstb*(*)
      character cdelim*1
      character chra*1   , chrb*1
      character corblc*9
c-----------------------------------------------------------------------
      logical   lequiv
c-----------------------------------------------------------------------
      integer   ifirst(ndword)        , ilast(ndword)
c-----------------------------------------------------------------------
      data  corblc /'spdfghijk'/
      data  cdelim  /' '/
c-----------------------------------------------------------------------
c
c---------------------------------------------------------------------
c---------------------------------------------------------------------
c  initial settings
c---------------------------------------------------------------------

      lequiv = .false.

c---------------------------------------------------------------------
c  find number of words in first string for check
c---------------------------------------------------------------------

      nfirst = 1
      iwords = ndword

      call xxword ( csta          , cdelim  , nfirst  ,
     &              iwords        ,
     &              ifirst        , ilast   , nwords
     &            )

c---------------------------------------------------------------------
c  compare words allowing orbit l-value in either upper or lower case
c---------------------------------------------------------------------


      do i = 1,nwords
        
        ierr = 0
        na = i4schr(csta(ilast(i):ilast(i)), ierr)
        ierr = 0
        nb = i4schr(cstb(ilast(i):ilast(i)), ierr)
        
        if ((csta(ifirst(i):ifirst(i)).eq.
     &       cstb(ifirst(i):ifirst(i))).and.
     &      (na.eq.nb)) then
            call xxcase(csta(ifirst(i)+1:ifirst(i)+1),chra,'lc')
            call xxcase(cstb(ifirst(i)+1:ifirst(i)+1),chrb,'lc')
            if ((index(corblc,chra).le.0).or.(chra.ne.chrb)) go to 100
        else
            go to 100
        endif
      
      end do

      lequiv = .true.

  100 return

c-----------------------------------------------------------------------

      end
