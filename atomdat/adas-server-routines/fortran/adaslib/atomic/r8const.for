      FUNCTION R8CONST( key )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8const *****************
C
C  PURPOSE: Returns the fundamental constant corresponding to 'KEY'.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  INPUT:  (C*)   KEY   =  Constant selector key
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C
C
C NOTES:   i. For Rydberg constant XXRAMS (or XXDAMS) is called to
C             determine whether the infinite or mass adjusted rydberg
C             constant is returned.
C
C         ii. Data is taken from Cohen & Taylor, Rev. Mod. Phys. 59,
C             p1121 (1987). As recommended in Atomic Data and Nuclear
C             Data Tables.
C
C        iii. Units are CGS.
C
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    01/04/99
C
C UPDATE:
C
C VERSION : 1.2
C DATE    : ?
C MODIFIED: MARTIN O'MULLANE
C            - Incorrect constants used in RYR and RYD evaluation.
C 
C VERSION : 1.3
C DATE    : 03-11-2011
C MODIFIED: Martin O'Mullane / Francisco Guzman
C            - C, ME and MU set to CGS values.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER        NCONST
C-----------------------------------------------------------------------
      PARAMETER      (NCONST = 8)
C-----------------------------------------------------------------------
      INTEGER        IND      ,  L1    , L2     , I4UNIT
C-----------------------------------------------------------------------
      REAL*8         R8CONST  ,  DMASS , RMASS
C-----------------------------------------------------------------------
      CHARACTER      KEY*(*)  , SMASS*7
C-----------------------------------------------------------------------
      REAL*8         VALS(NCONST)
C-----------------------------------------------------------------------
      CHARACTER*10   KEYS(NCONST)
C-----------------------------------------------------------------------
      DATA KEYS   /'C'             , 'RYR'          , 'RYD'          ,
     &             'RY'            , 'RY2K'         ,
     &             'ME'            , 'MU'           , 'E'/
C-----------------------------------------------------------------------
      DATA VALS   /2.99792458D10   , 109737.31534D0 , 109737.31534D0 ,
     &             109737.31534D0  , 1.57886609D5   ,
     &             9.109389754D-28 , 1.66054021D-24 , 1.6021773349D-19 /
C-----------------------------------------------------------------------

       CALL XXSTUC(KEY)
       CALL XXSLEN(KEY,L1,L2)

       IND = 1
       DO WHILE (IND.LE.NCONST.AND.KEY(L1:L2).NE.KEYS(IND) )
          IND = IND + 1
       END DO

       IF (IND.GT.NCONST) THEN
          WRITE(I4UNIT(-1),1000)KEY
          STOP
       ENDIF


       R8CONST = VALS(IND)


C RYDBERG CONSTANST MAY REQUIRE A MASS CORRECTION IF MASS HAS BEEN SET

       IF (KEY(L1:L2).EQ.'RYR') THEN

          SMASS = '?'
          CALL XXRAMS(SMASS)
          IF (SMASS(1:2).NE.'UN') THEN
             READ(SMASS,*)RMASS
             R8CONST = R8CONST / (1.0D0 + (VALS(6) / (RMASS*VALS(7)) ) )
          ENDIF

       ELSEIF (KEY(L1:L2).EQ.'RYD') THEN

          SMASS = '?'
          CALL XXDAMS(SMASS)
          IF (SMASS(1:2).NE.'UN') THEN
             READ(SMASS,*)DMASS
             R8CONST = R8CONST / (1.0D0 + (VALS(6) / (DMASS*VALS(7)) ) )
          ENDIF

       ENDIF

       RETURN
C-----------------------------------------------------------------------
 1000  FORMAT('R8CONST :  UNKNOWN CONSTANT ',A)
C-----------------------------------------------------------------------

       END
