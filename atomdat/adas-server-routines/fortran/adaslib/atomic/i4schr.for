CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4schr.for,v 1.3 2012/07/18 18:39:22 mog Exp $ Date $Date: 2012/07/18 18:39:22 $
CX
       FUNCTION I4SCHR(CHR,IABT)
C
       IMPLICIT NONE
C----------------------------------------------------------------
C  ************* FORTRAN77 FUNCTION: I4SCHR *********************
C
C PURPOSE: CONVERT FROM CHARACTER REPRESENTATION OF NUMBER OF
C          EQUIVALENT ELECTRONS TO DECIMAL FORM
C
C  FUNCTION:
C  INPUT : (C*1)   CHR     = CHARACTER FORM FOR THE NO. OF
C                            EQUIVALENT ELECTRONS
C  OUTPUT: (I*4)   I4SCHR  = INTEGER FORM FOR THE NO. OF
C                            EQUIVALENT ELECTRONS
C          (I*4)   IABT    = RETURN CODE:
C                               0 => NO ERROR
C                               1 => ERROR (A VALUE 'I4SCHR=0' WILL BE
C                                           RETURNED).
C          (I*4)   I       = GENERAL INTEGER
C
C  SUBROUTINES: NONE
C
C  AUTHOR: H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C  DATE:  30/10/95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 19-02-03
C MODIFIED: H.P. SUMMERS 
C		- EXTENDED RANGE 
C
C VERSION: 1.3                          DATE: 09-12-11
C MODIFIED: H.P. SUMMERS 
C		- MADE UPPER AND LOWER CASE INSENSITIVE. THIS
C                 LIMITS NUMBER OF EQUIVALENT ELECTRONS TO 35. 
C
C---------------------------------------------------------------
       INTEGER   I4SCHR  , I      , IABT
C----------------------------------------------------------------
       CHARACTER CHR*1  , CHR_LC*1
C----------------------------------------------------------------
       CHARACTER CHRA(35)*1
C----------------------------------------------------------------
       DATA  CHRA    /'1','2','3','4','5','6','7','8','9',
     &                'a','b','c','d','e','f','g','h','i',
     &                'j','k','l','m','n','o','p','q','r',
     &                's','t','u','v','w','x','y','z'/
C----------------------------------------------------------------
C----------------------------------------------------------------
      I4SCHR = 0
      IABT   = 0
      
      CALL XXCASE(CHR,CHR_LC,'LC')
C
       DO 20 I=1,35
         IF(CHR_LC.EQ.CHRA(I))THEN
             I4SCHR = I
             RETURN
         ENDIF
   20  CONTINUE
C
       IABT = 1
       RETURN
C----------------------------------------------------------------
       END
