CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8dcon.for,v 1.2 2007/04/11 13:01:49 allan Exp $ Date $Date: 2007/04/11 13:01:49 $
CX
      FUNCTION R8DCON( INTYP, OUTTYP, IZ1, DIN )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8DCON ******************
C
C  PURPOSE: TO CONVERT A DENSITY INTO SPECIFIED UNITS
C
C           (DOUBLE PRECISION FUNCTION VERSION OF 'XXTCON')
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C             (R*8)  R8DCON  = FUNCTION NAME -
C                              OUTPUT DENSITY (STATED UNITS)
C             (I*4)  INTYP   = 1 => 'DIN' UNITS: CM-3
C                            = 2 => 'DIN' UNITS: REDUCED
C             (I*4)  OUTTYP  = 1 => 'R8DCON' UNITS: CM-3
C                            = 2 => 'R8DCON' UNITS: REDUCED
C             (I*4)  IZ1     = RECOMBINING ION CHARGE (= Z+1).
C             (R*8)  DIN     = INPUT DENSITY (STATED UNITS)
C
C             (R*8)  Z1P7   = 'IZ1'**7
C             (R*8)  DCONV() = DENSITY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            DENSITY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; DCONV(1) =>               CM-3  -> OUTPUT UNITS
C            INTYP = 2 ; DCONV(2) =>            REDUCED  -> OUTPUT UNITS
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    04/01/91
C
C UPDATE:  07/08/91 - PE BRIDEN: CHANGED 'DBLE(IZ1**7)' TO 'DBLE(IZ1)**7
C                                TO AVOID INTEGER OVERFLOW IF IZ1>21.
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    INTYP              , OUTTYP             ,
     &           IZ1
C-----------------------------------------------------------------------
      REAL*8     R8DCON             , DIN
      REAL*8     Z1P7               , DCONV(2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      Z1P7 = DBLE(IZ1)**7
C
C-----------------------------------------------------------------------
C  OUTPUT DENSITY UNITS: CM-3
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            DCONV(1) = 1.0
            DCONV(2) = Z1P7
C
C-----------------------------------------------------------------------
C  OUTPUT DENSITY UNITS: REDUCED DENSITY
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            DCONV(1) = 1.0/Z1P7
            DCONV(2) = 1.0
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' R8DCON ERROR: INVALID OUTPUT DENSITY TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
      R8DCON = DCONV(INTYP)*DIN
C-----------------------------------------------------------------------
      RETURN
      END
