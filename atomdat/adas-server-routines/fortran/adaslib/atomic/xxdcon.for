CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxdcon.for,v 1.1 2004/07/06 15:33:55 whitefor Exp $ Date $Date: 2004/07/06 15:33:55 $
CX
      SUBROUTINE XXDCON( INTYP, OUTTYP, IZ1, IDVAL, DIN, DOUT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXDCON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF DENSITIES INTO SPECIFIED UNITS
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C  INPUT :    (I*4)  INTYP   = 1 => 'DIN(array)' UNITS: CM-3
C                            = 2 => 'DIN(array)' UNITS: REDUCED
C  INPUT :    (I*4)  OUTTYP  = 1 => 'DOUT(array)' UNITS: CM-3
C                            = 2 => 'DOUT(array)' UNITS: REDUCED
C  INPUT :    (I*4)  IZ1     = RECOMBINING ION CHARGE (= Z+1).
C  INPUT :    (I*4)  IDVAL   = NUMBER OF DENSITIES IN 'DIN(array)'
C  INPUT :    (R*8)  DIN()   = INPUT DENSITIES (STATED UNITS)
C  OUTPUT:    (R*8)  DOUT()  = OUTPUT DENSITIES (STATED UNITS)
C
C             (I*4)  I       = GENERAL USE
C
C             (R*8)  Z1P7    = 'IZ1'**7
C             (R*8)  DCONV() = DENSITY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            DENSITY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; DCONV(1) =>               CM-3  -> OUTPUT UNITS
C            INTYP = 2 ; DCONV(2) =>            REDUCED  -> OUTPUT UNITS
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    04/02/91
C
C UPDATE:  07/08/91 - PE BRIDEN: CHANGED 'DBLE(IZ1**7)' TO 'DBLE(IZ1)**7
C                                TO AVOID INTEGER OVERFLOW IF IZ1>21.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    INTYP              , OUTTYP             ,
     &           IZ1                , IDVAL
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     DIN(IDVAL)         , DOUT(IDVAL)
      REAL*8     Z1P7               , DCONV(2)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      Z1P7 = DBLE(IZ1)**7
C
C-----------------------------------------------------------------------
C  OUTPUT DENSITY UNITS: CM-3
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            DCONV(1) = 1.0
            DCONV(2) = Z1P7
C
C-----------------------------------------------------------------------
C  OUTPUT DENSITY UNITS: REDUCED DENSITY
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            DCONV(1) = 1.0/Z1P7
            DCONV(2) = 1.0
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' XXDCON ERROR: INVALID OUTPUT DENSITY TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
         DO 1 I=1,IDVAL
            DOUT(I) = DCONV(INTYP) * DIN(I)
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
