CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxsion.for,v 1.1 2004/07/06 15:39:09 whitefor Exp $ Date $Date: 2004/07/06 15:39:09 $
CX
      SUBROUTINE XXSION( SYMB , IZ , SION , LEN )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXSION *********************
C
C  PURPOSE: RETURNS ION ELEMENT SYMBOL AND ION CHARGE AS A STRING
C           CONSTRUCTED AS FOLLOWS <SYMBOL><CHARGE>. IT ALSO RETURNS
C           THE LENGTH OF THE STRING.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C*2)  SYMB      = ION ELEMENT SYMBOL OF BEAM.
C  INPUT : (I*4)  IZ        = ION CHARGE
C
C  INPUT : (C*4)  SION      = ION STRING '<SYMBOL><CHARGE>'.
C  INPUT : (I*4)  LEN       = LENGTH OF ION STRING.
C
C          (I*4)  I         = LENGTH IN BYTES OF NON-BLANK 'DSNAME'.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    21/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IZ      , LEN
C-----------------------------------------------------------------------
      CHARACTER  SYMB*2  , SION*4
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALISE STRING LENGTH.
C-----------------------------------------------------------------------
C
      LEN = 0
C
C-----------------------------------------------------------------------
C BUILD ION STRING.
C-----------------------------------------------------------------------
C
      IF ( SYMB(2:2) .EQ. ' ' ) THEN
         IF ( IZ .LT. 10 ) THEN
            WRITE(SION,1000) SYMB(1:1) , IZ
            LEN = 2
            SION(3:4) = '  '
         ELSE
            WRITE(SION,1001) SYMB(1:1) , IZ
            LEN = 3
            SION(4:4) = ' '
         ENDIF
      ELSE
         IF ( IZ .LT. 10 ) THEN
            WRITE(SION,1000) SYMB , IZ
            LEN = 3
            SION(4:4) = ' '
         ELSE
            WRITE(SION,1001) SYMB , IZ
            LEN = 4
         ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( A , I1 )
 1001 FORMAT( A , I2 )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
