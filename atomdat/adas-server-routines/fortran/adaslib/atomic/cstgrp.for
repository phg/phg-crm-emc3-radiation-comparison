CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/cstgrp.for,v 1.2 2004/07/06 12:24:00 whitefor Exp $ Date $Date: 2004/07/06 12:24:00 $
CX
      FUNCTION CSTGRP(CHR)
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: CSTGRP  ********************
C
C  PURPOSE: RETURNS TERM OF ORBITAL GIVEN IN THE EISSNER SINGLE
C           HEXADECIMAL CHARACTER FORM
C
C
C  INPUT :  (C*1)   CHR    = HEX. ORBITAL CHARACTER (EG. 'A')
C
C  OUTPUT:  (C*2)   CSTGRP = ORBITAL TERM   (E.G. '4F')
C
C           (I*4)   I      = GENERAL INDEX
C
C  SUBROUTINES:  NONE
C  AUTHOR:  W.J. DICKSON, JET
C
C  DATE:    20/10/89
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 19-02-03
C MODIFIED: H.P. SUMMERS 
C			- EXTENDED RANGE
C                 - ALTERED ORBITALS TO INCLUDE 'J'
C                 - A POINT OF CONFUSION BETWEEN CONDON
C                   & SHORTLEY, COWAN AND ADASDR 
C
C-----------------------------------------------------------------------
      INTEGER      I
C-----------------------------------------------------------------------
      CHARACTER*1  CHR    , CHRA(61)
      CHARACTER*2  CSTGRP , TGRPA(61)
C-----------------------------------------------------------------------
      DATA CHRA /'1','2','3','4','5','6','7','8','9','A','B',
     &           'C','D','E','F','G','H','I','J','K','L','M',
     &           'N','O','P','Q','R','S','T','U','V','W','X',
     &           'Y','Z','a','b','c','d','e','f','g','h','i',
     &           'j','k','l','m','n','o','p','q','r','s','t',
     &           'u','v','w','x','y','z'/
      DATA TGRPA/'1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s',
     &           '5p','5d','5f','5g','6s','6p','6d','6f','6g','6h','7s',
     &           '7p','7d','7f','7g','7h','7i','8s','8p','8d','8f','8g',
     &           '8h','8i','8j','9s','9p','9d','9f','9g','9h','9i','9j',
     &           '9k','As','Ap','Ad','Af','Ag','Ah','Ai','Aj','Ak','Al',
     &           'Bs','Bp','Bd','Bf','Bg','Bh'/
c     &           '8h','8i','8k','9s','9p','9d','9f','9g','9h','9i','9k',
c     &           '9l','As','Ap','Ad','Af','Ag','Ah','Ai','Ak','Al','Am',
c     &           'Bs','Bp','Bd','Bf','Bg','Bh'/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      DO 10 I=1,61
        IF(CHR.EQ.CHRA(I)) THEN
            CSTGRP=TGRPA(I)
            RETURN
        ENDIF
  10  CONTINUE
C
      RETURN
      END
