CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxidtl.for,v 1.1 2004/07/06 15:36:36 whitefor Exp $ Data $Date: 2004/07/06 15:36:36 $
CX
       SUBROUTINE XXIDTL( INDEX , N , L )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXIDTL *********************
C
C  PURPOSE:  INVERSE OF FUNCTION I4IDFL. RETURNS THE UNIQUE N AND L
C            QUANTUM NUMBERS WHICH GENERATE THE GIVEN INDEX WHEN PASSED
C            TO I4IDFL.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  INDEX    = INDEX NUMBER.
C
C  OUTPUT: (I*4)  N       = N QUANTUM NUMBER.
C  OUTPUT: (I*4)  L       = L QUANTUM NUMBER.
C
C          (I*4)  ID      = INDEX RETURNED BY FUNCTION I4IDFL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    10/09/93
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   I4IDFL
      INTEGER   INDEX  , N      , L
      INTEGER   ID
C-----------------------------------------------------------------------
C
C***********************************************************************
C INITIALIZE VALUES.
C***********************************************************************
C
      N = 1
      L = 0
C
C***********************************************************************
C DETERMINE N VALUE.
C***********************************************************************
C
    1 CONTINUE
      ID = I4IDFL( N+1, L )
      IF (ID .LE. INDEX) THEN
         N = N + 1
         GOTO 1
      ENDIF
C
C***********************************************************************
C DETERMINE L VALUE.
C***********************************************************************
C
    2 CONTINUE
      ID = I4IDFL( N, L+1 )
      IF (ID .LE. INDEX) THEN
         L = L + 1
         GOTO 2
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
