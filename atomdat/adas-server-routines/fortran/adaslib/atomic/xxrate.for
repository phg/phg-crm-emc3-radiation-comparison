CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxrate.for,v 1.1 2004/07/06 15:38:35 whitefor Exp $ Date $Date: 2004/07/06 15:38:35 $
CX
      SUBROUTINE XXRATE( NDTRN  , NDTEM  , NDLEV ,
     &                   ICNT   , MAXT   ,
     &                   XJA    , ER     , TEMP  ,
     &                   I1A    , I2A    ,
     &                   RATE1  , DRATE1
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXRATE *********************
C
C  PURPOSE: TO CALCULATE THE EXCITATION AND DE-EXCITATION RATE COEFFICI-
C           ENTS FOR A SET OF INPUT TEMPERATURES(rydberg) & TRANSITIONS.
C           VALUES RETURNED ASSUMING UNIT GAMMA VALUES (I.E. GAMMA = 1)
C
C  CALLING PROGRAM:  GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  ICNT    = NUMBER OF TRANSITIONS
C  INPUT :  (I*4)  MAXT    = NUMBER OF ISPF INPUT TEMPERATURES
C
C  INPUT :  (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR GIVEN LEVEL.
C                            NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                            DIMENSION ENERGY LEVEL.
C  INPUT :  (R*8)  TEMP()  = ISPF READ TEMPERATURES (KELVIN)
C
C
C  INPUT :  (I*4)  I1A()   = TRANSITION: LOWER ENERGY LEVEL INDEX
C  INPUT :  (I*4)  I2A()   = TRANSITION: UPPER ENERGY LEVEL INDEX
C
C  OUTPUT:  (R*8)  RATE1(,) = UNIT GAMMA EXCITATION RATE COEFFS(cm**3/s)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: TRANSITION INDEX
C  OUTPUT:  (R*8)  DRATE1(,)= UNIT GAMMA DE-EXCIT'N RATE COEFFS(cm**3/s)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: TRANSITION INDEX
C
C           (I*4)  NLTEM   = PARAMETER = MUST BE >= 'NDTEM'
C
C           (R*8)  TK2ATE  = PARAMETER = EQUATION CONSTANT = 1.5789D+05
C           (R*8)  R2GAM   = PARAMETER = EQUATION CONSTANT = 2.17161D-08
C
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C           (I*4)  IT      = TEMPERATURE ARRAY INDEX
C
C           (R*8)  EUPPER  = SELECTED TRANSITION - UPPER ENERGY
C                            LEVEL RELATIVE TO INDEX LEVEL 1 (Rydbergs)
C           (R*8)  ELOWER  = SELECTED TRANSITION - LOWER ENERGY
C                            LEVEL RELATIVE TO INDEX LEVEL 1 (Rydbergs)
C           (R*8)  WUPPER  = SELECTED TRANSITION - UPPER ENERGY LEVEL
C                            STATISTICAL WEIGHT.
C           (R*8)  WLOWER  = SELECTED TRANSITION - LOWER ENERGY LEVEL
C                            STATISTICAL WEIGHT.
C           (R*8)  SUPPER  = 1/(UPPER LEVEL STATISTICAL WEIGHT)
C           (R*8)  SLOWER  = 1/(LOWER LEVEL STATISTICAL WEIGHT)
C           (R*8)  RYDDIF  = NEGATIVE TRANSITION ENERGY IN RYDBERGS
C                            ( NOTE: 1 Rydberg = 1.09737E5 cm-1)
C
C           (R*8)  ATE()   = EQUATION PARAMETER: DIMENSION - TEMPERATURE
C           (R*8)  GVAL()  = EQUATION PARAMETER (FOR UNIT GAMMA VALUE)
C                            DIMENSION - TEMPERATURE
C
C ROUTINES:  NONE
C
C NOTES:
C            EQUATIONS USED -
C
C                         2.17161E-8 x GAMMA x SQRT(157890 / TEMP)
C            RATE1 = -------------------------------------------------
C                    WLOWER x EXP( (EUPPER-ELOWER) * (157890 / TEMP) )
C
C                         2.17161E-8 x GAMMA x SQRT(157890 / TEMP)
C            DRATE1 =  ---------------------------------------------
C                                        WUPPER
C
C            NOTE: OUTPUT VALUES ARE FOR 'GAMMA=1'
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    18/09/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NLTEM
C-----------------------------------------------------------------------
      REAL*8     TK2ATE             , R2GAM
C-----------------------------------------------------------------------
      PARAMETER( NLTEM = 101        )
C-----------------------------------------------------------------------
      PARAMETER( TK2ATE = 1.5789D+5 , R2GAM = 2.17161D-8 )
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDTEM              , NDLEV       ,
     &           ICNT               , MAXT
      INTEGER    IC                 , IT
C-----------------------------------------------------------------------
      REAL*8     EUPPER             , ELOWER             ,
     &           WUPPER             , WLOWER             ,
     &           SUPPER             , SLOWER             , RYDDIF
C-----------------------------------------------------------------------
      INTEGER    I1A(NDTRN)         , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     XJA(NDLEV)         , ER(NDLEV)          , TEMP(NDTEM) ,
     &           RATE1(NDTEM,NDTRN) , DRATE1(NDTEM,NDTRN)
      REAL*8     ATE(NLTEM)         , GVAL(NLTEM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NLTEM.LT.NDTEM) STOP
     &                   ' XXRATE ERROR: NDTEM > NLTEM. INCREASE NLTEM'
C-----------------------------------------------------------------------
         DO 1 IT=1,MAXT
             ATE(IT)  = TK2ATE/TEMP(IT)
             GVAL(IT) = R2GAM*DSQRT(ATE(IT))
    1    CONTINUE
C-----------------------------------------------------------------------
         DO 2 IC=1,ICNT
            ELOWER = ER(I1A(IC))
            EUPPER = ER(I2A(IC))
            WLOWER = ( 2.0D0 * XJA(I1A(IC)) ) + 1.0D0
            WUPPER = ( 2.0D0 * XJA(I2A(IC)) ) + 1.0D0
            RYDDIF = ELOWER - EUPPER
            SLOWER = 1.0 / WLOWER
            SUPPER = 1.0 / WUPPER
               DO 3 IT=1,MAXT
                  RATE1(IT,IC)  = GVAL(IT)*SLOWER*(DEXP(RYDDIF*ATE(IT)))
                  DRATE1(IT,IC) = GVAL(IT)*SUPPER
    3          CONTINUE
    2    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
