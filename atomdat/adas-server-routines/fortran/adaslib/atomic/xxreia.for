CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxreia.for,v 1.1 2004/07/06 15:38:42 whitefor Exp $ Date $Date: 2004/07/06 15:38:42 $
CX
      SUBROUTINE XXREIA( IUNIT , LSWIT , EIA )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXREIA *********************
C
C  PURPOSE: TO READ IN IONISATION POTENTIALS FROM AN INPUT MASTER
C           CONDENSED FILE ALLOCATED TO UNIT 'IUNIT' WITHOUT KNOWING HOW
C           MANY ARE PRESENT.
C
C  CALLING PROGRAMS: XXIN17 , XXIN80 , XXINST & GENERAL USE.
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH CONDENSED FILE IS ALLOCATED
C  OUTPUT: (L*4)  LSWIT   = .TRUE.  => VALID SET OF 'EIA()' VALUES FOUND
C                           .FALSE. =>INVALID OR NO 'EIA()' VALUES FOUND
C  OUTPUT: (R*8)  EIA()   = IONISATION RATE COEFFICIENTS: ()=ION CHARGE
C                           UNITS - WAVE NUMBERS (CM-1)
C
C          (I*4)  IPOT    = INDEX OF IONISATION RATE COEFFT. VALUE BEING
C                           ANALYSED.
C          (I*4)  IREC    = NUMBER RECORDS READ IN FROM 'IUNIT'
C          (I*4)  IBGN    = FIRST BYTE IN 'STRING' CONTAINING THE 'EIA()
C                           VALUE BEING ANALYSED.
C          (I*4)  IEND    = LAST BYTE IN 'STRING' CONTAINING THE 'EIA()'
C                           VALUE BEING ANALYSED.
C                           (NOTE: 'EIA()' VALUES IF PRESENT ARE STORED
C                                  SIX TO A LINE AS 6F12.1)
C          (I*4)  IABT    = RETURN CODE FROM 'R8FCTN' FUNCTION.
C                           0 => NO ERROR
C                           2 => INVALID FLOATING POINT NUMBER
C                           (A BLANK INPUT STRING TO 'R8FCTN' CAUSES
C                            A ZERO VALUE TO BE RETURNED AND IABT=0)
C          (I*4)  I       = GENERAL ARRAY USE
C
C          (L*4)  LDEND   = .TRUE. =>VALID END TO 'EIA()' SECTION FOUND
C                           .FALSE.=>INVALID END TO 'EIA()'SECTION FOUND
C
C          (C*12) C12     = 12 BYTE STRING FOR STORING 'EIA()' VALUES
C          (C*80) STRING  = 80 BYTE STRING FOR STORING INPUT FILE RECORD
C
C
C NOTE:
C           ON RETURN THE NEXT RECORD SHOULD BE THAT CONTAINING THE
C           REDUCED DENSITY VALUES.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          R8FCTN     ADAS      FUNCTION - CONVERT STRING -> REAL*8 NUM
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    22/08/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE (NO CHANGES)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IUNIT   , IPOT      , IREC    , IBGN     , IEND     ,
     &          I       , IABT
C-----------------------------------------------------------------------
      REAL*8    R8FCTN
C-----------------------------------------------------------------------
      LOGICAL   LSWIT   , LDEND
C-----------------------------------------------------------------------
      CHARACTER C12*12  , STRING*80
C-----------------------------------------------------------------------
      REAL*8    EIA(50)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      LDEND=.FALSE.
      IPOT=0
C-----------------------------------------------------------------------
         DO 1 IREC=1,10
            IF (.NOT.LDEND) THEN
               READ(IUNIT,'(A80)') STRING
C-----------------------------------------------------------------------
                  IF (STRING(2:2).EQ.'-') THEN
                     LDEND=.TRUE.
C-----------------------------------------------------------------------
                  ELSE
                     IBGN=-11
                        DO 2 I=1,6
                           IBGN=IBGN+12
                           IEND=IBGN+11
                           IPOT=IPOT+1
                              IF (IPOT.LE.50) THEN
                                 C12=STRING(IBGN:IEND)
                                 EIA(IPOT)=R8FCTN( C12 , IABT )
                                 IF (IABT.NE.0) STOP
     &    ' XXREIA ERROR: INVALID IONISATION RATE COEFFT. VALUE IN FILE'
                              ENDIF
    2                   CONTINUE
                  ENDIF
C-----------------------------------------------------------------------
            ENDIF
    1    CONTINUE
C-----------------------------------------------------------------------
      IF (.NOT.LDEND) STOP
     &  ' XXREIA ERROR: INVALID IONISATION RATE COEFFT. SECTION IN FILE'
C
C-----------------------------------------------------------------------
C ZERO ANY MISSING 'EIA()' VALUES
C-----------------------------------------------------------------------
C
         IF (IPOT.LT.50) THEN
            DO 3 I=IPOT+1,50
               EIA(I)=0.0D0
    3       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
         IF (EIA(1).GT.0.0D0) THEN
            LSWIT=.TRUE.
         ELSE
            LSWIT=.FALSE.
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
