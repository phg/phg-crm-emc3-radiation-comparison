C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4ndec.for,v 1.2 2004/07/06 14:07:34 whitefor Exp $ DATE $Date: 2004/07/06 14:07:34 $

      FUNCTION I4NDEC(IDEC)
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: I4NDEC  ********************
C
C  PURPOSE: RETURNS N QUANTUM NUMBER OF AN ORBITAL GIVEN IN THE
C           DECIMAL FORM
C
C
C  INPUT :  (I*4)   IDEC   = DECIMAL ORBITAL NUMBER (EG.'10')
C
C  OUTPUT:  (I*4)   I4NDEC = PRINCIPAL QUANTUM NUMBER FOR ORBITAL
C
C           (I*4)   NGRPA()= SET OF N-VALUES FOR DECIMAL ORBITALS
C
C  SUBROUTINES:  NONE
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    06/06/96
C
C  UPDATE:  24/07/96  PEB - REMOVE SPURIOUS EXTRA 'END' STATEMENT.
C
C  UPDATE:  19/02/03  H.P. SUMMERS - EXTENDED RANGE 
C
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    20TH AUGUST 1996
C
C VERSION: 1.1				DATE: 20-08-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 19-02-03
C MODIFIED: H.P. SUMMERS 
C		- EXTENDED RANGE 
C
C-----------------------------------------------------------------------
      INTEGER      IDEC
C-----------------------------------------------------------------------
      INTEGER      I4NDEC , NGRPA(61)
C-----------------------------------------------------------------------
      DATA NGRPA/   1,   2,   2,   3,   3,   3,   4,   4,   4,   4,   5,
     &              5,   5,   5,   5,   6,   6,   6,   6,   6,   6,   7,
     &              7,   7,   7,   7,   7,   7,   8,   8,   8,   8,   8,
     &              8,   8,   8,   9,   9,   9,   9,   9,   9,   9,   9,
     &              9,  10,  10,  10,  10,  10,  10,  10,  10,  10,  10,
     &             11,  11,  11,  11,  11,  11/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
            I4NDEC=NGRPA(IDEC)
C
      END
