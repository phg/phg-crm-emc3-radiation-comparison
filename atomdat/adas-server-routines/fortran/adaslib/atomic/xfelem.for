CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xfelem.for,v 1.3 2015/05/19 15:50:39 mog Exp $ Date $Date: 2015/05/19 15:50:39 $
CX
      FUNCTION XFELEM ( IZ0 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C ************* FORTRAN77 CHARACTER*12 FUNCTION: XFELEM ****************
C
C PURPOSE: TO RETURN THE NAME OF THE ELEMENT WITH NUCLEAR CHARGE IZ0
C          (CHARACTER*12 FUNCTION VERSION OF 'XXELEM')
C
C CALLING PROGRAM: GENERAL USE
C
C FUNCTION:
C
C          (C*12) XFELEM  = FUNCTION NAME -
C                           NAME OF ELEMENT WITH NUCLEAR CHARGE 'IZ0'
C          (I*4)  IZ0     = ELEMENT NUCLEAR CHARGE
C
C          (C*12) NAMES() = NAMES OF FIRST 50 ELEMENTS.
C                           ARRAY DIMENSION => NUCLEAR CHARGE
C
C NOTES:    IF NUCLEAR CHARGE IS OUT OF RANGE, I.E.NOT BETWEEN 1 & 50,
C           THEN THE CHARACTER STRING 'XFELEM' IS RETURNED BLANK.
C
C ROUTINES: NONE
C
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     13/02/91
C
C VERSION: 1.2
C UPDATE:   17/09/99  HUGH SUMMERS - INCREASED ELEMENT NUMBER TO 92
C
C VERSION: 1.3
C DATE   : 19-05-2015
C          Stuart Henderson
C            - Correct spelling of praseodymium.
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER      IZ0
C-----------------------------------------------------------------------
      CHARACTER*12 XFELEM , NAMES(92)
C-----------------------------------------------------------------------
      DATA NAMES/'HYDROGEN    ','HELIUM      ','LITHIUM     ',
     &           'BERYLLIUM   ','BORON       ','CARBON      ',
     &           'NITROGEN    ','OXYGEN      ','FLUORINE    ',
     &           'NEON        ','SODIUM      ','MAGNESIUM   ',
     &           'ALUMINIUM   ','SILICON     ','PHOSPHORUS  ',
     &           'SULPHUR     ','CHLORINE    ','ARGON       ',
     &           'POTASSIUM   ','CALCIUM     ','SCANDIUM    ',
     &           'TITANIUM    ','VANADIUM    ','CHROMIUM    ',
     &           'MANGANESE   ','IRON        ','COBALT      ',
     &           'NICKEL      ','COPPER      ','ZINC        ',
     &           'GALLIUM     ','GERMANIUM   ','ARSENIC     ',
     &           'SELENIUM    ','BROMINE     ','KRYPTON     ',
     &           'RUBIDIUM    ','STRONTIUM   ','YTTRIUM     ',
     &           'ZIRCONIUM   ','NIOBIUM     ','MOLYBDENUM  ',
     &           'TECHNETIUM  ','RUTHENIUM   ','RHODIUM     ',
     &           'PALLADIUM   ','SILVER      ','CADMIUM     ',
     &           'INDIUM      ','TIN         ','ANTIMONY    ',
     &           'TELLURIUM   ','IODINE      ','XENON       ',
     &           'CESIUM      ','BARIUM      ','LANTHANUM   ',
     &           'CERIUM      ','PRASEODYMIUM','NEODYMIUM   ',
     &           'PROMETHIUM  ','SAMARIUM    ','EUROPIUM    ',
     &           'GADOLINIUM  ','TERBIUM     ','DYSPROSIUM  ',
     &           'HOLMIUM     ','ERBIUM      ','THULIUM     ',
     &           'YTTERBIUM   ','LUTETIUM    ','HAFNIUM     ',
     &           'TANTALUM    ','TUNGSTEN    ','RHENIUM     ',
     &           'OSMIUM      ','IRIDIUM     ','PLATINUM    ',
     &           'GOLD        ','MERCURY     ','THALLIUM    ',
     &           'LEAD        ','BISMUTH     ','POLONIUM    ',
     &           'ASTATINE    ','RADON       ','FRANCIUM    ',
     &           'RADIUM      ','ACTINIUM    ','THORIUM     ',
     &           'PROTACTINIUM','URANIUM     '/
C-----------------------------------------------------------------------
         IF ( (IZ0.GT.92).OR.(IZ0.LT.0) ) THEN
            XFELEM = ' '
         ELSE
            XFELEM = NAMES(IZ0)
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
