CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8ecip.for,v 1.2 2007/04/11 13:01:49 allan Exp $ Date $Date: 2007/04/11 13:01:49 $
CX
      FUNCTION R8ECIP( IZC , XI , ZETA , TE )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8ECIP ******************
C
C  PURPOSE:  CALUCLATES THE SHELL CONTRIBUTION TO THE IONISATION RATE
C            COEFFICIENT IN THE ECIP APPROXIMATION OF BURGESS.
C
C            CF. SUMMERS (1974) APPLETON LABORATORY REPORT IM367.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8ECIP  = IONISATION RATE COEFFICIENT.
C                           UNITS: CM3 SEC-1
C
C  INPUT : (I*4)  IZC     = TARGET ION CHARGE NUMBER.
C  INPUT : (R*8)  XI      = EFFECTIVE IONISATION POTENTIAL FOR SHELL.
C                           UNITS: RYD
C  INPUT : (R*8)  ZETA    = EFFECTIVE NUMBER OF EQUIVALENT ELECTRONS FOR
C                           SHELL.
C  INPUT : (R*8)  TE      = ELECTRON TEMPERATURE.
C                           UNITS: K
C
C  PARAM : (I*4) MXT      = 5.
C  PARAM : (R*8) P1       =
C
C          (I*4)  J       = LOOP INDEX
C
C          (R*8)  Z       = REAL VALUE = IZC+1.
C          (R*8)  ATE     =
C          (R*8)  EN      =
C          (R*8)  Y       =
C          (R*8)  AI      =
C          (R*8)  B       =
C          (R*8)  B1      =
C          (R*8)  C       =
C          (R*8)  R       =
C          (R*8)  D       =
C          (R*8)  F       =
C          (R*8)  C1      =
C          (R*8)  C2      =
C          (R*8)  C4      =
C
C          (R*8)  X()     =
C          (R*8)  W()     =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8YIP      ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    03/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8ECIP
C-----------------------------------------------------------------------
      REAL*8     R8YIP
C-----------------------------------------------------------------------
      INTEGER    MXT
      PARAMETER( MXT = 5 )
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 157890.0D0 )
C-----------------------------------------------------------------------
      INTEGER    IZC
      INTEGER    J
C-----------------------------------------------------------------------
      REAL*8     XI      , ZETA    , TE
      REAL*8     Z       , ATE     , EN      , Y       , AI      ,
     &           B       , B1      , C       , R       , D       ,
     &           F       , C1      , C2      , C4
C-----------------------------------------------------------------------
      REAL*8     X(MXT)  , W(MXT)
C-----------------------------------------------------------------------
      DATA       X / 0.26356D0  , 1.41340D0  , 3.59643D0  ,
     &               7.08581D0  , 12.6408D0                 /
      DATA       W / 5.21756D-1 , 3.98667D-1 , 7.59424D-2 ,
     &               3.61176D-3 , 2.337D-5                  /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      Z   = DFLOAT( IZC + 1 )
      ATE = P1 / TE
      EN  = Z / DSQRT( XI )
      Y   = XI * ATE
C
C-----------------------------------------------------------------------
C
      AI = 0.0D0
      DO 1 J = 1 , MXT
         B  = X(J) / Y
         B1 = B + 1.0D0
         C  = DSQRT( B1 )
         R  = ( 1.25D0 * EN**2 + 0.25D0 ) / Z
         D  = ( Z / EN ) * ( R + 2.0D0 * EN**2 * C /
     &        ( ( B + 2.0D0 ) * Z**2 ) ) / ( C + DSQRT( B ) )
         C1 = 1.0D0 / ( B + 2.0D0 )
         F  = 1.0D0
         C4 = R8YIP( 0.0D0 , D )
         C2 = ( C1 * ( B - B1 * C1 * DLOG( B1 ) ) +
     &        0.65343D0 * ( 1.0D0 - 1.0D0 / B1**3 ) * C4 / EN ) * F
         AI = AI + W(J) * C2
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      IF (Y .GT. 180.0D0) THEN
         R8ECIP = 0.0D0
      ELSE
         R8ECIP = 8.68811D-8 * DSQRT( ATE ) * DEXP( -Y ) * ZETA *
     &            AI / XI
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
