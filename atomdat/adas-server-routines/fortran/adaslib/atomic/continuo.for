      subroutine continuo(wave   , tev    , iz0  , iz1  ,
     &                    contff , contin
     &                   )

      implicit none
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: continuo ********************
C
C  PURPOSE: For an given wavelength generate radiative recombination
C           and bremsstrahlung emissivity.
C
C  CALLING PROGRAM: General use.
C
C
C  INPUT : (R*8)  wave    = Wavelength required (A)
C          (R*8)  tev     = Electron temperature (eV)
C          (I*4)  iz0     = Atomic number
C          (I*4)  iz1     = Ion stage + 1
C  OUTPUT: (R*8)  contff  = Free-free emissivity (ph cm3 s-1 A-1)
C          (R*8)  contin  = Total continuum emissivity
C                           (free-free + free-bound)(ph cm3 s-1 A-1)
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          r8gbf      ADAS      Free-free Gaunt factor.
C          r8gav      ADAS      Total gaunt factor for free-free and
C                               quasi-continuous free-bound transitions
C          r8giiiav   ADAS      Maxwellian averaged free-free Gaunt factors
C
C
C  NOTES :  Based on Lorne Horton's original continuo program but
C           coefficients, rather than emission, are returned.
C
C
C
C  AUTHOR:  Martin O'Mullane
C  DATE  :  02-03-2005
C
C
C
C  VERSION : 1.1
C  DATE    : 02-03-2005
C  MODIFIED: Martin O'Mullane
C            - First version
C
C  VERSION : 1.2
C  DATE    : 02-03-2005
C  MODIFIED: Martin O'Mullane
C            - Alter comments to note that contin is the sum of
C              free-free and free-bound continuum emission.
C
C  VERSION : 1.2
C  DATE    : 02-03-2005
C  MODIFIED: Martin O'Mullane
C            - Trap a floating point overflow which appears for
C              high iz1 and low tev combination. Set epsbf to 0.0
C              when this occurs.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      real*8    a0       , alpha  , c       , ih     , h      , k      ,
     &          e        , pi
C-----------------------------------------------------------------------
      parameter (a0 = 5.2918D-09  , alpha = 7.2974D-03)
      parameter (c  = 2.9979D+10  , ih    = 13.606D+00)
      parameter (h  = 6.6261D-27  , k     = 1.3807D-16)
      parameter (e  = 1.6022D-12  , pi    = 3.14159D+00)
C-----------------------------------------------------------------------
      integer    iz0     , iz1
      integer    n       , nmin   , nmax    , ie     , isum   , nfull  ,
     &           ierem
C-----------------------------------------------------------------------
      real*8     r8gbf   , r8gav  , r8giiiav
      real*8     wave    , tev    , contff  , contin
      real*8     q1      , lambda , te      , fnu    , u      , u2     ,
     &           gam2    , fact   , en      , epsbf
C-----------------------------------------------------------------------
      external   r8gbf   , r8gav  , r8giiiav
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------

C  Constants for the calculations

      q1     = 32.0D0*alpha**4.0D0*c/(3.0D0*dsqrt(3.0D0)*pi*a0)
      lambda = wave*1.0D-08
      te     = tev*11604.0D0
      fnu    = c/lambda
      u      = h*fnu/(k*te)


C Begin with Bremsstrahlung

      gam2   = iz1*iz1*ih/tev
      contff = iz1*iz1*q1*(pi*a0*a0*ih/tev)**1.5D0*(tev/ih)
     &            *r8giiiav(u,gam2)*dexp(-u)/wave


C Recombination

      nmin = idint(dsqrt(iz1*iz1*e*ih/(h*fnu))) + 1

C  Skip full levels

      ie    = iz0 - iz1
      isum  = 0
      nfull = 0
      ierem  = ie
      do n = 1, 5
        isum = isum + 2*n*n
        if (isum.lt.ie) then
          nfull = n
          ierem = ie - isum
        endif
      end do

      fact = dfloat(2*(nfull+1)*(nfull+1)-ierem)
     &       /dfloat(2*(nfull+1)*(nfull+1))
      if (nmin.lt.nfull+1) nmin = nfull + 1

C  Use discrete calculation for first ten levels

      nmax   = nmin + 9
      contin = 0.0D0

      do n = nmin, nmax
        en = dfloat(n)
        u2 = en*en*h*fnu/(iz1*iz1*e*ih) - 1.0D0
        if (iz1*iz1*ih/(en*en*tev).LT.169.0D0) then
           epsbf = 2.0D0*q1*(pi*a0*a0*ih/tev)**1.5D0*r8gbf(en,u2)
     &             *iz1**4.0D0/(en**3.0D0)*dexp(iz1*iz1*ih/(en*en*tev))
     &             *dexp(-u)/wave
        else
           epsbf = 0.0D0
        endif

C  Use partial occupancy factor, if necessary

        if (n.eq.nfull+1) epsbf = epsbf*fact

        contin = contin + epsbf

      end do

C  High level free-bound continua + free-free

      gam2  = iz1*iz1*ih/tev
      epsbf = iz1*iz1*q1*(pi*a0*a0*ih/tev)**1.5D0*(tev/ih)
     &         *r8gav(u,gam2,nmax+1)*dexp(-u)/wave

      contin = contin + epsbf

      end
