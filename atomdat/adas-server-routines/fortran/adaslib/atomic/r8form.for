CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8form.for,v 1.2 2004/07/06 14:41:43 whitefor Exp $ Date $Date: 2004/07/06 14:41:43 $
CX
      FUNCTION R8FORM( MXNENG , MXNSHL , N      , L      ,
     &                 IESEL  , ITYPE  , NENRGY , XLCUTA ,
     &                 PL2A   , PL3A
     &               )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 FUNCTION: R8FORM **********************
C
C  PURPOSE:  CALCULATES CHARGE EXCHANGE L-RESOLVED CROSS-SECTION AS A
C            FRACTION OF THE CORRESPONDING N-RESOLVED CROSS-SECTION.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  FUNC  : (R*8)  R8FORM    = L-RESOLVED CROSS-SECTION AS FRACTION OF
C                             N-REOSLVED CROSS-SECTION.
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  N         = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  L         = ORBITAL QUANTUM NUMBER.
C  INPUT : (I*4)  IESEL     = SELECTED ENERGY INDEX.
C  INPUT : (I*4)  ITYPE     = TYPE OF APPROXIMATION TO USE.
C                           = 1 = 2L+1 INCREASE WITH L AND EXPONENTIAL
C                                 CUTOFF AS L/LCUT.
C                           = 2 = 2L+1 INCREASE WITL L, SHARP CUTOFF AT
C                                 MIN(LCUT,N-1).
C                           = 3 = (2L+1)**2 INCREASE WITH L, SHARP
C                                 CUTOFF AT MIN(LCUT,N-1).
C                           = 4 = KRONECKER DELTA(L,MIN(LCUT,N-1)).
C                           = 5 = 2L+1 INCREASE WITH L AND EXP. CUTOFF
C                                 AS MAX(0,L-LCUT)/2.
C                           = 6 = 2L+1 INCREASE WITH L AND EXP. CUTOFF
C                                 AS 2*MAX(0,L-LCUT).
C                           = 7 = NEW PRIMARY FORM BASED ON SPFMAN13
C                                 FITTING PROCEDURE WITH SHARP SWITCHING
C                                 FUNCTIONS.
C                           = 8 = NEW PRIMARY FORM BASED ON SPFMAN13
C                                 FITTING PROCEDURE WITH SOFT SWITCHING
C                                 FUNCTIONS.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES IN DATASET.
C  INPUT : (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C
C  PARAM : (I*4)  MXB       = 'MXBEAM'.
C  PARAM : (I*4)  MXN       = 'MXNSHL'.
C  PARAM : (I*4)  MXTYPE    = NO. OF DIFFERENT APPROXIMATIONS.
C  PARAM : (R*8)  C1        =
C
C          (I*4)  LCUT      = CUT OFF VALUE FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  LC        = CUT OFF VALUE FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  NN        = PRINCIPAL QUANTUM NUMBER LOOP INDEX.
C          (I*4)  LL        = ORBITAL QUANTUM NUMBER LOOP INDEX.
C          (I*4)  IE        = ENERGY LOOP INDEX.
C
C          (R*8)  SUM       =
C          (R*8)  XLC       = REAL VALUE = LC.
C          (R*8)  XL        = REAL VALUE = L.
C          (R*8)  XLL       = REAL VALUE = LL.
C          (R*8)  EF        =
C          (R*8)  XLCRIT    =
C          (R*8)  T         =
C          (R*8)  S1        =
C          (R*8)  S2        =
C          (R*8)  T1        =
C          (R*8)  T2        =
C
C          (R*8)  SUM1A()   = TABLE OF SUMS FOR 1ST APPROXIMATION.
C                             1ST DIMENSION: L CUTOFF
C                             2ND DIMENSION: N-SHELL
C          (R*8)  SUM5A()   = TABLE OF SUMS FOR 5TH APPROXIMATION.
C                             1ST DIMENSION: L CUTOFF
C                             2ND DIMENSION: N-SHELL
C          (R*8)  SUM6A()   = TABLE OF SUMS FOR 6TH APPROXIMATION.
C                             1ST DIMENSION: L CUTOFF
C                             2ND DIMENSION: N-SHELL
C          (R*8)  SUM7A()   = TABLE OF SUMS FOR 7TH APPROXIMATION.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C          (R*8)  SUM8A()   = TABLE OF SUMS FOR 8TH APPROXIMATION.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C
C          (L*4)  LFIRST()  = FLAGS IF FIRST CALL OF APPROXIMATION. ONLY
C                             USED BY APPROX. WHICH REQUIRE AN INTIAL
C                             SUM
C                           = .TRUE.  = FIRST CALL.
C                           = .FALSE. = NOT FIRST CALL.
C                             1ST DIMENSION: APPROX. TYPE INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURN UNIT NO. FOR OUTPUT OF MESSAGES.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    19/10/93
C
C UPDATES:
C
C VERSION: 1.2						DATE: 23-06-98
C MODIFIED: RICHARD MARTIN
C		- INCREASED MXE FORM 30 TO 40.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8FORM
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    MXE          , MXN          , MXTYPE
      PARAMETER( MXE = 40     , MXN = 20     , MXTYPE = 8 )
C-----------------------------------------------------------------------
      REAL*8     C1
      PARAMETER( C1 = 1.2D0 )
C-----------------------------------------------------------------------
      INTEGER    MXNENG  , MXNSHL  , N       , L       , IESEL   ,
     &           ITYPE   , NENRGY
      INTEGER    LCUT    , LC      , NN      , LL      , IE
C-----------------------------------------------------------------------
      REAL*8     SUM     , XLC     , XL      , XLL     , EF      ,
     &           XLCRIT  , T       , S1      , S2      , T1      ,
     &           T2
C-----------------------------------------------------------------------
      REAL*8     XLCUTA(MXNENG)  , PL2A(MXNENG)    , PL3A(MXNENG)
      REAL*8     SUM1A(MXN,MXN)  , SUM5A(MXN,MXN)  , SUM6A(MXN,MXN)  ,
     &           SUM7A(MXE,MXN)  , SUM8A(MXE,MXN)
C-----------------------------------------------------------------------
      LOGICAL    LFIRST(MXTYPE)
C-----------------------------------------------------------------------
      SAVE       SUM1A   , SUM5A   , SUM6A   , SUM7A   , SUM8A   ,
     &           LFIRST
C-----------------------------------------------------------------------
      DATA LFIRST / MXTYPE * .TRUE. /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXE' AND 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXE .LT. MXNENG) THEN
         WRITE(I4UNIT(-1),1000) MXE, MXNENG
         STOP
      ENDIF
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1001) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CHECK INPUT QUANTUM NUMBERS 'N' AND 'L' VALID.
C-----------------------------------------------------------------------
C
      IF ((N .LT. 1) .OR. (N .GT. MXNSHL) .OR.
     &    (L .LT. 0) .OR. (L .GE. N)           ) THEN
C
         R8FORM = 0.0D0
         RETURN
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      LCUT = IDINT( XLCUTA(IESEL) + 0.0001 )
C
C-----------------------------------------------------------------------
C
      IF (ITYPE .EQ. 1) THEN
C
         IF (LFIRST(1)) THEN
            LFIRST(1) = .FALSE.
            DO 11 LC = 1 , MXNSHL
               XLC = DFLOAT( LC )
               SUM = 0.0D0
               DO 12 NN = 1 , MXNSHL
                  LL = NN - 1
                  XLL = DFLOAT( LL )
                  SUM = SUM + ( 2.0D0 * XLL + 1.0D0 ) *
     &                  DEXP( -XLL / XLC )
                  SUM1A(LC,NN) = SUM
   12          CONTINUE
   11       CONTINUE
         ENDIF
C
         LC  = MAX0( 1 , LCUT )
         XLC = DFLOAT( LC )
         XL  = DFLOAT( L )
C
         R8FORM = ( 2.0D0 * XL + 1.0D0 ) * DEXP( -XL / XLC ) /
     &            SUM1A(LC,N)
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 2) THEN
C
         LC  = MIN0( LCUT , N-1 )
         IF (L .GT. LC) THEN
            R8FORM = 0.0D0
         ELSE
            XLC = DFLOAT( LC )
            XL  = DFLOAT( L )
            R8FORM = ( 2.0D0 * XL + 1.0D0 ) / ( XLC + 1.0D0 )**2
        ENDIF
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 3) THEN
C
         LC  = MIN0( LCUT , N-1 )
         IF (L .GT. LC) THEN
            R8FORM = 0.0D0
         ELSE
            XLC = DFLOAT( LC )
            XL  = DFLOAT( L )
            R8FORM = 3.0D0 * ( 2.0D0 * XL + 1.0D0 )**2 /
     &               ( ( XLC + 1.0D0 ) * ( 2.0D0 * XLC + 1.0D0 ) *
     &                 ( 2.0D0 * XLC + 3.0D0 ) )
         ENDIF
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 4) THEN
C
         LC = MIN0( LCUT , N-1 )
         IF (L .NE. LC) THEN
            R8FORM = 0.0D0
         ELSE
            R8FORM = 1.0D0
         ENDIF
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 5) THEN
C
         IF (LFIRST(5)) THEN
            LFIRST(5) = .FALSE.
            DO 51 LC = 1 , MXNSHL
               XLC = DFLOAT( LC )
               SUM = 0.0D0
               DO 52 NN = 1 , MXNSHL
                  LL = NN - 1
                  XLL = DFLOAT( LL )
                  EF = DMAX1( 0.0D0 , XLL-XLC ) / 2.0D0
                  SUM = SUM + ( 2.0D0 * XLL + 1.0D0 ) * DEXP( -EF )
                  SUM5A(LC,NN) = SUM
   52          CONTINUE
   51       CONTINUE
         ENDIF
C
         LC  = MAX0( 1 , LCUT )
         XLC = DFLOAT( LC )
         XL  = DFLOAT( L )
         EF  = DMAX1( 0.0D0 , XL-XLC ) / 2.0D0
C
         R8FORM = ( 2.0D0 * XL + 1.0D0 ) * DEXP( -EF ) / SUM5A(LC,N)
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 6) THEN
C
         IF (LFIRST(6)) THEN
            LFIRST(6) = .FALSE.
            DO 61 LC = 1 , MXNSHL
               XLC = DFLOAT( LC )
               SUM = 0.0D0
               DO 62 NN = 1 , MXNSHL
                  LL = NN - 1
                  XLL = DFLOAT( LL )
                  EF = 2.0D0 * DMAX1( 0.0D0 , XLL-XLC )
                  SUM = SUM + ( 2.0D0 * XLL + 1.0D0 ) * DEXP( -EF )
                  SUM6A(LC,NN) = SUM
   62          CONTINUE
   61       CONTINUE
         ENDIF
C
         LC  = MAX0( 1 , LCUT )
         XLC = DFLOAT( LC )
         XL  = DFLOAT( L )
         EF  = 2.0D0 * DMAX1( 0.0D0 , XL-XLC )
C
         R8FORM = ( 2.0D0 * XL + 1.0D0 ) * DEXP( -EF ) / SUM6A(LC,N)
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 7) THEN
C
         IF (LFIRST(7)) THEN
            LFIRST(7) = .FALSE.
            DO 72 IE = 1 , NENRGY
               XLCRIT = DMAX1( XLCUTA(IE) , 0.1D0 )
               DO 73 NN = 1 , MXNSHL
                  SUM = 0.0D0
                  DO 74 LL = 0 , NN-1
                     XL = DFLOAT( LL )
                     IF (XL .LE. XLCRIT) THEN
                        T = ( ( 2.0D0 * XL + 1.0D0 ) /
     &                        ( 2.0D0 * XLCRIT + 1.0D0 ) )**PL2A(IE)
                     ELSE
                        T = DEXP( PL3A(IE) * ( 1.0D0 - XL / XLCRIT ) )
                     ENDIF
                     SUM = SUM + T
   74             CONTINUE
                  SUM7A(IE,NN) = SUM
   73          CONTINUE
   72       CONTINUE
         ENDIF
C
         XL = DFLOAT( L )
         XLCRIT = DMAX1( XLCUTA(IESEL) , 0.1D0 )
         IF (XL .LE. XLCRIT) THEN
            T = ( ( 2.0D0 * XL + 1.0D0 ) /
     &            ( 2.0D0 * XLCRIT + 1.0D0 ) )**PL2A(IESEL)
         ELSE
            T = DEXP( PL3A(IESEL) * ( 1.0D0 - XL / XLCRIT ) )
         ENDIF
C
         R8FORM = T / SUM7A(IESEL,N)
C
C-----------------------------------------------------------------------
C
      ELSE IF (ITYPE .EQ. 8) THEN
C
         IF (LFIRST(8)) THEN
            LFIRST(8) = .FALSE.
            DO 82 IE = 1 , NENRGY
               XLCRIT = DMAX1( XLCUTA(IE) , 0.1D0 )
               DO 83 NN = 1 , MXNSHL
                  SUM = 0.0D0
                  DO 84 LL = 0 , NN-1
                     XL = DFLOAT( LL )
                     S1 = ( 1.0D0 - DTANH( C1 * ( XL - XLCRIT ) ) ) /
     &                    2.0D0
                     S2 = ( 1.0D0 + DTANH( C1 * ( XL - XLCRIT ) ) ) /
     &                    2.0D0
                     T1 = ( ( 2.0D0 * XL + 1.0D0 ) /
     &                      ( 2.0D0 * XLCRIT + 1.0D0 ) )**PL2A(IE)
                     T2 = DEXP( PL3A(IE) * ( 1.0D0- XL / XLCRIT ) )
                     T  = S1 * T1 + S2 * T2
                     SUM = SUM + T
   84             CONTINUE
                  SUM8A(IE,NN) = SUM
   83          CONTINUE
   82       CONTINUE
         ENDIF
C
         XL = DFLOAT( L )
         XLCRIT = DMAX1( XLCUTA(IESEL) , 0.1D0 )
         S1 = ( 1.0D0 - DTANH( C1 * ( XL - XLCRIT ) ) ) / 2.0D0
         S2 = ( 1.0D0 + DTANH( C1 * ( XL - XLCRIT ) ) ) / 2.0D0
         T1 = ( ( 2.0D0 * XL + 1.0D0 ) /
     &          ( 2.0D0 * XLCRIT + 1.0D0 ) )**PL2A(IESEL)
         T2 = DEXP( PL3A(IESEL) * ( 1.0D0- XL / XLCRIT ) )
         T  = S1 * T1 + S2 * T2
C
         R8FORM = T / SUM8A(IESEL,N)
C
C-----------------------------------------------------------------------
C
      ELSE
C
         R8FORM = 0.0D0
C
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' R8FORM ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXE'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNENG''.'/
     &        2X, 'MXE = ', I3 , '   MXNENG = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXE'' IN SUBROUTINE R8FORM.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' R8FORM ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE R8FORM.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
