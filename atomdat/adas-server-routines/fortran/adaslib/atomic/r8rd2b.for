CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8rd2b.for,v 1.1 2004/07/06 14:42:39 whitefor Exp $ Date $Date: 2004/07/06 14:42:39 $
CX
      FUNCTION R8RD2B ( NU , LU , NL , LL )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8RD2B ******************
C
C  PURPOSE: CLACULATES HYDROGENIC BOUND-BOUND RADIAL INTEGRALS USING
C           RECURRENCE RELATIONS.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8RD2B  = RESULT OF INTEGRAL.
C
C  INPUT:  (I*4)  NU      = UPPER VALUE OF N QUANTUM NUMBER.
C  INPUT:  (I*4)  LU      = UPPER VALUE OF L QUANTUM NUMBER.
C  INPUT:  (I*4)  NL      = LOWER VALUE OF N QUANTUM NUMBER.
C  INPUT:  (I*4)  LL      = LOWER VALUE OF L QUANTUM NUMBER.
C
C  PARAM:  (R*8)  P1      = EQUATION CONSTANT.
C  PARAM:  (R*8)  P12     = EQUATION CONSTANT = P1**2.
C  PARAM:  (R*8)  P2      = EQUATION CONSTANT = 1/P1.
C  PARAM:  (R*8)  P22     = EQUATION CONSTANT = P2**2.
C
C          (I*4)  JS      =
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  XNL     = REAL VALUE = NL.
C          (R*8)  XNL2    = REAL VALUE = XNL**2.
C          (R*8)  XNU     = REAL VALUE = NU.
C          (R*8)  XNU2    = REAL VALUE = XNU**2.
C          (R*8)  XNL1    = REAL VALUE = NL-1.
C          (R*8)  U       =
C          (R*8)  V       =
C          (R*8)  W       =
C          (R*8)  P       =
C          (R*8)  XI      = REAL VALUE = I.
C          (R*8)  XI1     = REAL VALUE = I+1.
C          (R*8)  XI12    = REAL VALUE = XI1**2.
C          (R*8)  T1      =
C          (R*8)  T2      =
C          (R*8)  T3      =
C
C ROUTINES: NONE
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     28/09/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8RD2B
C-----------------------------------------------------------------------
      REAL*8     P1              , P12             ,
     &           P2              , P22
      PARAMETER( P1 = 64.0D0     , P12 = P1**2     ,
     &           P2 = 1.0D0 / P1 , P22 = P2**2       )
C-----------------------------------------------------------------------
      INTEGER    NU      , LU      , NL      , LL
      INTEGER    JS      , I
C-----------------------------------------------------------------------
      REAL*8     XNL     , XNL1    , XNL2    , XNU     , XNU2    ,
     &           U       , V       , W       , P       ,
     &           XI      , XI1     , XI12    ,
     &           T1      , T2      , T3
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INPUT PARAMETERS VALID.
C-----------------------------------------------------------------------
C
      IF ((NU .LT. 2)  .OR. (LU .LT. 0) .OR. (LU .GE. NU) .OR.
     &    (NL .LT. 1)  .OR. (LL .LT .0) .OR. (LL .GE. NL) .OR.
     &    (NL .GE. NU)                                         ) THEN
         R8RD2B = 0.0D0
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      XNL = DFLOAT( NL )
      XNL2 = XNL**2
      XNU = DFLOAT( NU )
      XNU2 = XNU**2
      V = 1.0D0 - XNL2 / XNU2
      W = ( 1.0D0 + XNL / XNU )**2
      U = 8.0D0 * XNL2 / W**2
      P = 1.0D0
      JS = 0
C
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , NL
         XI = DFLOAT( I )
         P  = P * U * ( 1.0D0 - XI**2 / XNU2 )
     &        / ( XI * ( 2.0D0 * XI - 1.0D0 ) )
         IF (DABS( P ) .LT. P22) THEN
            JS = JS - 1
            P  = P * P12
         ENDIF
    1 CONTINUE
C
      P = 2.0D0 * P * XNL / XNU
      P = 4.0D0 * DSQRT( P ) / ( XNU * V**2 )
C
      DO 2 I = NL+1 , NU
         P  = P * V / W
         IF(DABS( P ) .LT. P2) THEN
            JS = JS - 1
            P  = P * P1
         ENDIF
    2 CONTINUE
C
C-----------------------------------------------------------------------
C INTEGRAL CALCULATION FOR (LU-LL)=1.
C-----------------------------------------------------------------------
C
      IF ((LU - LL) .EQ. 1) THEN
         T2 = P
         IF ((NL - LL) .LT. 2) THEN
            T3 = T2
         ELSE
            U  = DSQRT( ( 2.0D0 * XNL - 1.0D0 ) * V )
            T3 = U * T2 / 2.0D0
            DO 3 I = NL-2 , LL+1 , -1
               XI  = DFLOAT( I )
               XI1 = DFLOAT( I + 1 )
               XI12 = XI1**2
               T1  = T2
               T2  = T3
               T3  = T2 * ( 4.0D0 * ( XNL2 - XI12 )
     &               + XI1 * ( 2.0D0 * XI1 - 1.0D0 ) * V )
     &               - 2.0D0 * XNL * U * T1
               U   = DSQRT( ( XNL2 - XI**2 )
     &                      * ( 1.0D0 - XI12 / XNU2 ) )
               T3  = T3 / ( 2.0D0 * XNL * U )
               IF (DABS( T3 ) .GT. P1) THEN
                  JS = JS + 1
                  T3 = T3 * P2
                  T2 = T2 * P2
               ENDIF
    3       CONTINUE
         ENDIF
C
         R8RD2B = XNL2**2 * T3**2 * P12**JS
C
C-----------------------------------------------------------------------
C INTEGRAL CALCULATION FOR (LL-LU)=1.
C-----------------------------------------------------------------------
C
      ELSE IF ((LL - LU) .EQ. 1) THEN
         T2  = P
         XNL1 = DFLOAT( NL - 1 )
         U   = DSQRT( V / ( 1.0D0 - XNL1**2 / XNU2 ) )
         T2  = T2 * U / ( 2.0D0 * XNL )
         IF ((NL - LL) .LT. 2) THEN
            T3 = T2
         ELSE
            U  = DSQRT( (2.0D0 * XNL - 1.0D0)
     &                  * (1.0D0 - ( XNL - 2.0D0 )**2 / XNU2 ) )
            T3 = ( 4.0D0 + XNL1 * V ) * ( 2.0D0 * XNL - 1.0D0 )
     &           * T2 / ( 2.0D0 * XNL * U )
            DO 4 I = NL-3 , LL , -1
               XI  = DFLOAT( I )
               XI1 = DFLOAT( I + 1 )
               XI12 = XI1**2
               T1  = T2
               T2  = T3
               T3  = T2 * ( 4.0D0 * ( XNL2 - XI12 )
     &               + XI1 * ( 2.0D0 * XI1 + 1.0D0 ) * V )
     &               - 2.0D0 * XNL * U * T1
               U   = DSQRT( (XNL2 - XI12 )
     &                      * ( 1.0D0 - XI**2 / XNU2 ) )
               T3  = T3 / ( 2.0D0 * XNL * U )
               IF (DABS(T3) .GT. P1) THEN
                  JS = JS + 1
                  T3 = T3 * P2
                  T2 = T2 * P2
               ENDIF
    4       CONTINUE
         ENDIF
C
         R8RD2B = XNL2**2 * T3**2 * P12**JS
C
C-----------------------------------------------------------------------
C INTEGRAL VALUE FOR ALL OTHER CASES.
C-----------------------------------------------------------------------
C
      ELSE
         R8RD2B = 0.0D0
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
