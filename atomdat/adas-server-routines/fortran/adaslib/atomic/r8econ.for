CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8econ.for,v 1.3 2007/04/11 13:01:49 allan Exp $ Data $Date: 2007/04/11 13:01:49 $
CX
      FUNCTION R8ECON( INTYP, OUTTYP, EIN )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: R8ECON *********************
C
C  PURPOSE: TO CONVERT A VELOCITY/ENERGY INTO A SPECIFIED FORM
C
C           (DOUBLE PRECISION FUNCTION VERSION OF 'XXECON')
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C             (R*8)  R8ECON  = FUNCTION NAME -
C                              OUTPUT VELOCITY/ENERGY (STATED UNITS)
C             (I*4)  INTYP   = 1 => 'EIN' UNITS: AT. UNITS (VEL.)
C                            = 2 => 'EIN' UNITS: CM/SEC    (VEL.)
C                            = 3 => 'EIN' UNITS: EV/AMU  (ENERGY)
C             (I*4)  OUTTYP  = 1 => 'R8ECON' UNITS: AT.UNITS (VEL.)
C                            = 2 => 'R8ECON' UNITS: CM/SEC   (VEL.)
C                            = 3 => 'R8ECON' UNITS: EV/AMU (ENERGY)
C             (R*8)  EIN     = INPUT  VELOCITY/ENERGY (STATED UNITS)
C
C             (R*8)  AMU2KG  = PARAMETER: AMU TO KG CONVERSION FACTOR
C             (R*8)  EV2J    = PARAMETER: EV TO JOULES CONVERSION FACTOR
C             (R*8)  M2CM    = PARAMETER: METRES TO CM CONVERSION FACTOR
C             (R*8)  VELE0H  = PARAMETER: ORBITAL VELOCITY  (CM/SEC)  OF
C                              AN ELECTRON IN THE SMALLEST  ORBIT  OF  A
C                              HYDROGEN ATOM (BOHR) = 2.1877D+8 CM/SEC
C             (R*8)  AT2VEL  = AT.UNITS (VEL) TO CM/SEC (VEL) CONVERSION
C             (R*8)  VEL2AT  = CM/SEC (VEL) TO AT.UNITS (VEL) CONVERSION
C             (R*8)  VEL2EN  = CM/SEC (VEL) TO EV/AMU (ENGY.) CONVERSION
C             (R*8)  EN2VEL  = EV/AMU (ENGY.) TO CM/SEC (VEL) CONVERSION
C             (R*8)  AT2EN   = AT.UNITS (VEL) TO EV/AMU (ENG) CONVERSION
C             (R*8)  EN2AT   = EV/AMU (ENG) TO AT.UNITS (VEL) CONVERSION
C
C             (R*8)  ECONV() = ENERGY/VELOCITY CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            ENERGY/VELOCITY CONVERSION PARAMETERS:
C
C            INTYP = 1 ; ECONV(1) =>  VELOCITY: AT.UNITS -> OUTPUT FORM
C            INTYP = 2 ; ECONV(2) =>  VELOCITY: CM/SEC   -> OUTPUT FORM
C            INTYP = 3 ; ECONV(3) =>  ENERGY  : EV/AMU   -> OUTPUT FORM
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    05/02/91
C
C VERSION: 1.2				DATE: 01-05-96
C MODIFIED: WILLIIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - CORRECTED AT2EN. WAS AT2VEL*VEL2EN
C
C VERSION: 1.2                          DATE: 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     AMU2KG                  , EV2J                   ,
     &           M2CM                    , VELE0H
      REAL*8     AT2VEL                  , VEL2AT
      REAL*8     VEL2EN                  , EN2VEL
      REAL*8     AT2EN                   , EN2AT
C-----------------------------------------------------------------------
      PARAMETER( AMU2KG = 1.66053D-27    , EV2J   = 1.602192D-19  ,
     &           M2CM   = 1.0D+02        , VELE0H = 2.1877D+08    )
      PARAMETER( AT2VEL = VELE0H         , VEL2AT = 1.0/VELE0H    )
      PARAMETER( VEL2EN = AMU2KG/( 2.0*EV2J*M2CM*M2CM )           ,
     &           EN2VEL = ( 2.0*EV2J*M2CM*M2CM )/AMU2KG           )
      PARAMETER( AT2EN  = AT2VEL*AT2VEL*VEL2EN                    ,
     &           EN2AT  = EN2VEL*VEL2AT*VEL2AT                    )
C-----------------------------------------------------------------------
      INTEGER    INTYP                  , OUTTYP
C-----------------------------------------------------------------------
      REAL*8     R8ECON                 , EIN
      REAL*8     ECONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  OUTPUT VELOCITY UNITS: AT.UNITS
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            ECONV(2) = VEL2AT
            ECONV(3) = EN2AT
C
C-----------------------------------------------------------------------
C  OUTPUT VELOCITY UNITS: CM/SEC
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            ECONV(1) = AT2VEL
            ECONV(3) = EN2VEL
C
C-----------------------------------------------------------------------
C  OUTPUT ENERGY UNITS: EV/AMU
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            ECONV(1) = AT2EN
            ECONV(2) = VEL2EN
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' R8ECON ERROR: INVALID OUTPUT VEL./ENERGY TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
         IF (INTYP.EQ.OUTTYP) THEN
            R8ECON = EIN
         ELSEIF (INTYP.EQ.3) THEN
            R8ECON = DSQRT( ECONV(INTYP) * EIN )
         ELSEIF (OUTTYP.EQ.3) THEN
            R8ECON = ECONV(INTYP) * EIN * EIN
         ELSE
            R8ECON = ECONV(INTYP) * EIN
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
