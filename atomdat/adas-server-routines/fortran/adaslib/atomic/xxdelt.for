CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxdelt.for,v 1.1 2004/07/06 15:34:21 whitefor Exp $ Date $Date: 2004/07/06 15:34:21 $
CX
      SUBROUTINE XXDELT( A , B , C , D , J )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: XXDELT ********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT : (R*8)  A       =
C  INPUT : (R*8)  B       =
C  INPUT : (R*8)  C       =
C
C  OUTPUT: (R*8)  D       =
C  OUTPUT: (I*4)  J       =
C
C          (I*4)  J1      =
C          (I*4)  J2      =
C          (I*4)  J3      =
C          (I*4)  J4      =
C          (I*4)  J5      =
C
C          (R*8)  A1      =
C          (R*8)  A2      =
C          (R*8)  A3      =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4JGAM     ADAS      RETURNS VALUE FROM 'JGAM' TABLE.
C          R8GAM      ADAS      RETURNS VALUE FROM 'GAM' TABLE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    07/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER  I4JGAM
      REAL*8   R8GAM
C-----------------------------------------------------------------------
      INTEGER  J
      INTEGER  J1      , J2      , J3      , J4      , J5
C-----------------------------------------------------------------------
      REAL*8   A       , B       , C       , D
      REAL*8   A1      , A2      , A3
C-----------------------------------------------------------------------
C
      A1 = A + B - C + 0.001D0
      A2 = A - B + C + 0.001D0
      A3 = B - A + C + 0.001D0
C
      J1 = IDINT( A1 )
      J2 = IDINT( A2 )
      J3 = IDINT( A3 )
      J4 = J1 + J2 + J3 + 1
      J5 = IDINT( A1 + A2 + A3 + 1.501D0 )
C
C-----------------------------------------------------------------------
C
      IF ((A1 .GE. 0.0D0) .AND. (A2 .GE. 0.0D0) .AND.
     &    (A3 .GE. 0.0D0) .AND. (J4 .EQ. J5)          ) THEN
C
         D = R8GAM( J1+1 ) * R8GAM( J2+1 ) * R8GAM( J3+1 )
     &       / R8GAM( J4+1 )
         J = I4JGAM( J1+1 ) + I4JGAM( J2+1 ) + I4JGAM( J3+1 )
     &       -I4JGAM(J4+1)
C
      ELSE
C
       D = 0.0D0
       J = 0
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
