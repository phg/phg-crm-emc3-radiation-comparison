CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8ah.for,v 1.1 2004/07/06 14:40:45 whitefor Exp $ Date $Date: 2004/07/06 14:40:45 $
CX
      FUNCTION R8AH ( NU , LU , NL , LL )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8AH   ******************
C
C  PURPOSE: CALCULATES A-VALUES FOR HYDROGEN.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8AH    = HYDROGEN A-VALUE.
C
C  INPUT:  (I*4)  NU      = UPPER VALUE OF N QUANTUM NUMBER.
C  INPUT:  (I*4)  LU      = UPPER VALUE OF L QUANTUM NUMBER.
C  INPUT:  (I*4)  NL      = LOWER VALUE OF N QUANTUM NUMBER.
C  INPUT:  (I*4)  LL      = LOWER VALUE OF L QUANTUM NUMBER.
C
C  PARAM:  (R*8)  P1      = EQUATION CONSTANT.
C
C          (R*8)  XNU     = REAL VALUE = NU.
C          (R*8)  XLU     = REAL VALUE = LU.
C          (R*8)  XNL     = REAL VALUE = NL.
C          (R*8)  XLL     = REAL VALUE = LL.
C          (R*8)  T1      =
C          (R*8)  DE      =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8RD2B     ADAS      RETURNS HYDRONIC BOUND-BOUND RADIAL
C                               INTEGRAL.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     29/09/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8AH
C-----------------------------------------------------------------------
      REAL*8     R8RD2B
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 2.67744D+09 )
C-----------------------------------------------------------------------
      INTEGER    NU      , LU      , NL      , LL
C-----------------------------------------------------------------------
      REAL*8     XNU     , XLU     , XNL     , XLL     ,
     &           T1      , DE
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INPUT PARAMETERS VALID.
C-----------------------------------------------------------------------
C
      IF ((NU .LT. 2)  .OR. (LU .LT. 0) .OR. (LU .GE. NU) .OR.
     &    (NL .LT. 1)  .OR. (LL .LT .0) .OR. (LL .GE. NL) .OR.
     &    (NL .GE. NU)                                         ) THEN
         R8AH = 0.0D+00
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE A-VALUE.
C-----------------------------------------------------------------------
C
      XNU = DFLOAT( NU )
      XLU = DFLOAT( LU )
      XNL = DFLOAT( NL )
      XLL = DFLOAT( LL )
      T1  = P1 / ( 2.0D+00 * XLU + 1.0D+00 )
      DE  = 1.0D+00 / XNL**2 - 1.0D+00 / XNU**2
C
      R8AH = T1 * DMAX1( XLU , XLL ) * DE**3
     &       * R8RD2B( NU , LU , NL ,LL)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
