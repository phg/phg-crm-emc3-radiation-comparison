      FUNCTION R8YIP( XI , DELTA )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8YIP  ******************
C
C  PURPOSE: EVALUATES IMPACT PARAMETER CROSS-SECTION SECOND BESSEL
C           INTEGRAL Y [BURGESS AND SUMMERS: MNRAS (1976)
C           172,345 - EQN C14]
C
C  CALLING PROGRAMS: EIQIP, CXEIQP
C
C  INPUT:  (R*8)  XI      = Z[1/KN-1/KN1]/A0 WITH Z TARGET CHARGE,
C                              KN, KN1 INITIAL AND FINAL ELECTRON WAVE
C                              NUMBERS AND A0 THE BOHR RADIUS
C  INPUT:  (R*8)  DELTA   = RC[KN-KN1] WITH RC THE CLOSEST APPROACH
c
C  OUTPUT: (R*8)  R8YIP   = Y(XI,DELTA)
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    07/10/93
C
C VERSION 1.1                             DATE: 07/10/93
C MODIFIED: JONATHAN NASH
C           - FIRST VERSION
C VERSION 1.2                             DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C           - COMPLETED COMMENT BLOCK DESCRIPTION
C
C VERSION : 1.3
C DATE    : 22-10-2010
C MODIFIED: Martin O'Mullane
C           - Incorrect logic (XM should be greter than 0.01) to
C             set w2.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8YIP
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 5.0D0 / 30.0D0 )
C-----------------------------------------------------------------------
      INTEGER    J
C-----------------------------------------------------------------------
      REAL*8     XI      , DELTA
      REAL*8     W       , W1      , W2      , W3      , XM      ,
     &           X       , X0      , X2      , R       , S       ,
     &           T       , Y
C-----------------------------------------------------------------------
      REAL*8     A(20)   , B(20)
C-----------------------------------------------------------------------
      DATA       A / 2.3916D0  , 1.6742D0  , 1.2583D0  , 9.738D-1  ,
     &               7.656D-1  , 6.078D-1  , 4.8561D-1 , 3.8976D-1 ,
     &               3.1388D-1 , 2.5342D-1 , 2.0501D-1 , 1.6610D-1 ,
     &               1.3476D-1 , 1.0944D-1 , 8.896D-2  , 7.237D-2  ,
     &               5.8903D-2 , 4.7971D-2 , 3.9086D-2 , 3.1860D-2   /
      DATA       B / 1.0091D0  , 3.015D-1  , 1.314D-1  , 7.63D-2   ,
     &               5.04D-2   , 3.561D-2  , 2.634D-2  , 1.997D-2  ,
     &               1.542D-2  , 1.205D-2  , 9.50D-3   , 7.57D-3   ,
     &               6.02D-3   , 4.84D-3   , 3.89D-3   , 3.12D-3   ,
     &               2.535D-3  , 2.047D-3  , 1.659D-3  , 1.344D-3    /
C-----------------------------------------------------------------------
C
      W1 = 0.0D0
      W2 = 0.0D0
      W3 = 0.0D0
C
      XM = DABS( XI )
      X  = XM + DELTA
C
      IF (X .LE. 0.1D0) THEN
         T = DLOG( 1.1229D0 / X )
         Y = T + 0.25D0 * X**2 * ( 1.0D0 - 2.0D0 * T**2 )
      ELSE IF (X .GE. 2.0D0) THEN
         T = 1.0D0 / X
         Y = 1.5707963D0 * ( 1.0D0 + T * ( 0.25D0 + T
     &       * ( -0.09375D0 + T * ( 0.0703125D0 - T * 0.0472D0 ) ) ) )
         W1 = -2.0D0 * X
      ELSE
         J  = IDINT( 10.0D0 * X )
         X0 = 0.1D0 * DFLOAT( J )
         R  = 10.0D0 * ( X - X0 )
         S  = 1.0D0 - R
         Y  = R * A(J+1) + S * A(J) + P1 * ( R * ( R**2 - 1.0D0 )
     &        * B(J+1) + S * ( S**2 - 1.0D0 ) * B(J) )
      ENDIF
C
      IF ((X .LE. 0.1D0) .OR.  (X .GE. 2.0D0) .OR.
     &    (XM .GT. 0.01D0)                         ) THEN
         X  = XM**2 / ( XM + DELTA )
         X2 = 0.125D0 * XM**2 / ( XM + 3.0D0 * DELTA )
         R8YIP = Y / ( 1.0D0 + X2**2 )
         W2 = 3.1416D0 * XM - 0.9442D0 * X
     &        * ( 1.0D0 + X * ( 2.14D0 + 14.2D0 * X ) )
     &        / ( 1.0D0 + X * ( 2.44D0 + 12.8D0 * X ) )
      ELSE
         R8YIP = Y
      ENDIF
C
      IF (XI .LT. 0.0D0) W3 = 6.283D0 * XI
C
      W = W1 + W2 + W3
C
      R8YIP = R8YIP * DEXP( W )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
