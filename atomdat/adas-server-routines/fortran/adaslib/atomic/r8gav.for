      function r8gav(u, gam2, n0)

      implicit none
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 REAL*8 FUNCTION: R8GAV ******************
C
C  PURPOSE: CALCULATES TOTAL GAUNT FACTOR FOR FREE-FREE AND
C  QUASI-CONTINUOUS FREE-BOUND TRANSITIONS
C
C  INPUT
C      U=HV/KT  WHERE HV IS PHOTON ENERGY
C               AND KT IS ELECTRON TEMPERATURE (ENERGY UNITS)
C      GAM2=Z*Z*IH/KT  WHERE Z IS TARGET ION CHARGE
C                      AND IH IS THE RYDBERG ENERGY
C      N0       IS THE FIRST BOUND LEVEL TO BE INCLUDED IN THE
C               INTEGRAL
C  OUTPUT
C      GAV=MAXWELLIAN AVERAGED GAUNT FACTOR.
C
C
C  AUTHOR   : L D Horton
C  DATE     : 24-06-1997
C
C
C
C  VERSION  : 1.1
C  DATE     : 02-03-2005
C  MODIFIED : Martin O'Mullane
C                - First version in central ADAS.
C                - Make implicit none.
C
C  VERSION  : 1.2
C  DATE     : 15-03-2006
C  MODIFIED : Allan Whiteford
C                - Removed external giii statement.
C
C  VERSION  : 1.3
C  DATE     : 22-06-2006
C  MODIFIED : Martin O'Mullane
C                - Removed redundant external gbf statement.
C
C  VERSION  : 1.4                          
C  DATE     : 10-04-2007
C  MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      integer    N0
      integer    I
C-----------------------------------------------------------------------
      real*8     r8gav   , r8giii  , r8gbf
      real*8     gam2    , u0
      real*8     sum     , x       , e1        , e2     , en    ,
     &           rgam2   , u       , u1
C-----------------------------------------------------------------------
      real*8     XA(8)   , WA(8)
C-----------------------------------------------------------------------
      data xa / 0.17027963D+00,  0.90370178D+00,  2.25108663D+00,
     &          4.26670017D+00,  7.04590540D+00, 10.75851601D+00,
     &         15.74067864D+00, 22.86313174D+00/
      data wa / 3.69188589D-01,  4.18786781D-01,  1.75794987D-01,
     &          3.33434923D-02,  2.79453624D-03,  9.07650877D-05,
     &          8.48574672D-07,  1.04800117D-09/
C-----------------------------------------------------------------------

      U0 = GAM2/(N0*N0)
      RGAM2=1.0D0/GAM2
      SUM=0.0D0

      DO I=1,8

         X=XA(I)
         E1=RGAM2*(X-U0)
         E2=RGAM2*(X-U0+U)
         IF (E1.LT.0.0D0) THEN
           EN = 1.0D0/DSQRT(-E1)
           U1 = -E2/E1
           SUM = SUM + WA(I)*R8GBF(EN,U1)
         ELSE
           SUM=SUM+WA(I)*R8GIII(1,1,E1,E2)
         ENDIF

      END DO

      R8GAV=DEXP(U0)*SUM

      RETURN

      END
