      subroutine xxdtes( cstrg , leiss  , lstan  , lbndl  , lprnt ,
     &                   cstr_top       , cstr_tail       ,
     &                   nvlce , lvlce
     &                 )

      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxdtes *********************
c
c  purpose: Detects if the configuration string from a specific ion
c           level list line is of eissner form , standard form or
c           neither.
C
C           If neither, the subroutine checks to see if it is a
c           bundle (* in the string) or based on a parent ([..] in
c           the string).  If the string is of Eissner or standard
c           form, the n-shell and l-shell of the outermost
c           (valence)  electron is returned.
c
c  calling programs: general use
c
c  subroutine:
c
c  input : (c*(*)) cstrg    = configuration character string
c  output: (l*4)   leiss    = .true.  => eissner form
c                             .false. => not eissner form
c  output: (l*4)   lstan    = .true.  => standard form
c                             .false. => not standard form
c  output: (l*4)   lbndl    = .true.  => bundled form ('*' found)
c                             .false. => not bundled form
c  output: (l*4)   lprnt    = .true.  => parent form ('[...]' found)
c                             .false. => not parent form
c  output: (c*19)  cstr_top = leading part of config. string in Eissner
c                             format (no leading blank, trailing blanks)
c  output: (c*(*)) cstr_tail= trailing part of config. string in Eissner
c                             format (no leading blank, trailing blanks)
c  output: (i*4)   nvlce    = outer electron n-shell if recognisable
c  output: (i*4)   lvlce    = outer electron l-shell if recognisable
c
c          (i*4)   i       = general use
c          (i*4)   iabt    = return code (see specific function)
c                            0 => ok
c                            1 => fault detected
c          (i*4)   icfsel  = 1 => standard form out, standard form in
c                            2 => eissner  form out, standard form in
c                            3 => standard form out, eissner  form in
c                            4 => eissner  form out, eissner  form in
c          (i*4)   ishel   = shell counter
c          (i*4)   ip      = parity of configuration
c          (i*4)   maxn    = n_shell sum for configuration
c          (i*4)   nshel   = number of shells identified ffrom string
c          (i*4)   ndword  = maximum number of words in string
c          (i*4)   nfirst  = first word to be extracted from string
c          (i*4)   nwords  = number of words in string
c          (i*4)   nela()  = number of electrons in each shell
c          (i*4)   ifirst()= position of first char. of word in string
c          (i*4)   ilast() = position of last char. of word in string
c
c          (c*1)   cdelim  = separators for words in string
c          (c*19)  cstrgo  = general use string
c          (c*19)  strg    = standard form configuration string
c          (c*19)  strge   = eissner form configuration string
c          (c*1)   cheisa()= eissner character for orbitals
c          (c*2)   chstda()= standard orbital spec. for each shell
c          (c*2)   cnela() = chars. for no. of equiv. elec. in shell
c                            (eissner form case)
c          (c*1)   chqa()  = index to hexadecimal conversions
c          (c*1)   chra()  = char. for no. of. equiv. elec. in shell
c                            (standard form case)
c
c routines:
c          routine    source   brief description
c          -------------------------------------------------------------
c          i4fctn     adas     converts character string to integer
c          i4ngrp     adas     returns n quantum number in the
c                              eissner single hexadecimal character form
c          i4pgrp     adas     returns parity of orbital given the
c                              eissner single hexadecimal character form
c          i4schr     adas     returns numerical value for number of
c                              equivalent electrons given as hex> char.
c          cstgrp     adas     returns term of orbital given in the
c                              eissner single hexadecimal character form
c          ceigrp     adas     returns eissner code for orbital
c          xxword     adas     finds number of words in a string
c          xxcmps     adas     compare standard config. strings
c
c
c
c author:  h. p. summers, university of strathclyde
c          ja8.08
c          tel. 0141-553-4196
c
c date:    19/02/03
c
C VERSION: 1.1                          DATE: 19-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 14-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED CHANGES DATED 01/10/96 ABOVE
C
C VERSION: 1.3                          DATE: 28-08-97
C MODIFIED: HUGH SUMMERS
C               - ADDED CHANGES TO CHECK 'G' STATES
C
C VERSION: 1.4                         DATE: 19/02/03
C MODIFIED: HUGH SUMMERS
C               - Rewrite based on g5dtes.for
C
C VERSION: 1.5                         DATE: 28/09/2004
C MODIFIED: Martin O'Mullane
C               - Incorrect redirection when checking the Eissner pattern.
C                 The if statement block checking ir jumped out of the
C                 current sub-block to the end of the previous sub-block
C                 rather than to the end of its own sub-block.
C
C VERSION: 1.6                         DATE: 17/05/2007
C MODIFIED: Allan Whiteford
C           - Updated comments as part of subroutine documentation
C             procedure.
C
C VERSION: 1.7                         DATE: 21/07/2010
C MODIFIED: Hugh Summers
C           - Handle long configuration strings.
C           - Return extra parameters to calling program.
C
C VERSION: 1.8                         DATE: 03/09/2011
C MODIFIED: Hugh Summers
C           - Handle variable length cstr_top string for compatibility
C             with writing and reading adf04 datasets of heavy species.
C
C VERSION : 1.10                         
C DATE    : 05-07-2012
C MODIFIED: Martin O'Mullane
C           - Add the ambiguous configuration to the error message.
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   ndword
c-----------------------------------------------------------------------
      parameter ( ndword = 21 )
c-----------------------------------------------------------------------
      integer   nvlce     , lvlce
      integer   i         , nfirst    , iwords  , nwords
      integer   i4unit
      integer   i4ngrp    , i4lgrp    , i4pgrp  , i4schr   , i4fctn
      integer   ishel     , nshel     , ip      , maxn     , icfsel
      integer   iabt      , i2        , i3      , ir
      integer   len_str   , nw4
      integer   len_cstr_top          , len_cstr_tail
      integer   nd_eiss_top           , nd_stan_top
      integer   nd_eiss_tail          , nd_stan_tail
      integer   nchr_eiss_top
c-----------------------------------------------------------------------
      character cstrg*(*)  , cstr_top*(*) , cstr_tail*(*)
      character cstrgo*64  , strg*64      , strge*64
      character cdelim*1
      character cstr64*64
      character cstgrp*2   , ceigrp*1     , cstr2*2        , cstr1*1
      character clett1*18  , clett2*10
      character fmt1044*13 , fmt1045*12   , fmt1046*18   , fmt1047*12
      character fmt1048*13
c-----------------------------------------------------------------------
      logical*4 leiss     , lstan     , lequiv
      logical*4 lbndl     , lprnt
c-----------------------------------------------------------------------
      integer   nela(ndword)   , ifirst(ndword)      , ilast(ndword)
c-----------------------------------------------------------------------
      character cheisa(ndword)*1 , chstda(ndword)*2  , cnela(ndword)*2
      character chqa(61)*1  , chra(ndword)*1
C-----------------------------------------------------------------------
      data  chqa    /'1','2','3','4','5','6','7','8','9',
     &               'A','B','C','D','E','F','G','H','I',
     &               'J','K','L','M','N','O','P','Q','R',
     &               'S','T','U','V','W','X','Y','Z','a',
     &               'b','c','d','e','f','g','h','i','j',
     &               'k','l','m','n','o','p','q','r','s',
     &               't','u','v','w','x','y','z'/
      data  cdelim  /' '/
      data  clett1  /'spdfghijkSPDFGHIJK'/
      data  clett2  /'spdfgSPDFG'/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initial settings
c-----------------------------------------------------------------------
      leiss = .false.
      lstan = .false.
      lbndl = .false.
      lprnt = .false.
      cstr_top = ' '
      len_cstr_top=len(cstr_top)
      cstr_tail = ' '
      len_cstr_tail=len(cstr_tail)
      nd_eiss_top = (len_cstr_top/3)
      nd_stan_top = (len_cstr_top/4)
      nd_eiss_tail = (len_cstr_tail/4)
      nd_stan_tail = (len_cstr_tail/4)

      nchr_eiss_top =3*(len_cstr_top/3)

      fmt1044 = '(??(1a2,1a1))'
      fmt1045 = '(??(i2,1a1))'
      fmt1046 = '(1x,??(1a2,a1,1x))'
      fmt1047 = '(??(i2,1a1))'
      fmt1048 = '(??(i2,1a1))'

      write(fmt1044(2:3),'(i2)')nd_eiss_top
      write(fmt1045(2:3),'(i2)')nd_stan_top
      write(fmt1046(5:6),'(i2)')nd_eiss_top
      write(fmt1047(2:3),'(1i2)')nd_stan_tail
      write(fmt1048(2:3),'(1i2)')nd_eiss_top

      cstr64=' '
      nvlce = 0
      lvlce = -1

c-----------------------------------------------------------------------
c  check for bundle or parent forms
c-----------------------------------------------------------------------

      if(index(cstrg,'*').gt.0) lbndl = .true.
      if((index(cstrg,'[').gt.0).or.(index(cstrg,']').gt.0))
     &          lprnt = .true.
      if(lbndl.or.lprnt) return

c-----------------------------------------------------------------------
c  normal adf04 config string without leading blank or 'tens decimal'
c  of Eissner first shell in usual abbreviated practice.  This does
c  not preclude an Eissner non-abbreviated specification.
c-----------------------------------------------------------------------
      iwords = ndword
      nfirst = 1
      call xxword ( cstrg         , cdelim  , nfirst  ,
     &              iwords        ,
     &              ifirst        , ilast   , nwords
     &                )

      if(nwords.eq.1) then
c-----------------------------------------------------------------------
c  check configuration string length
c-----------------------------------------------------------------------
          len_str=len(cstrg)
          if(len_str.ge.18)then
              if((ilast(1)-ifirst(1)+1).gt.3) then
c-----------------------------------
c  possibly Eissner - check pattern
c-----------------------------------
                  i3 = (ilast(1)-ifirst(1)+1)/3
                  ir = (ilast(1)-ifirst(1)+1)-3*i3
                  if(ir.eq.0) then
                      do i=1,i3
                        cstr2 = cstrg(ifirst(1)+(i3-1)*3:
     &                                ifirst(1)+i3*3-2)
                        i2=i4fctn(cstr2,iabt)
                        if((iabt.gt.0).or.(i2.lt.51)) go to 55
                      enddo
                      leiss = .true.
                      if((ilast(1)-ifirst(1)+1).gt.nchr_eiss_top) then
                          cstr_top = cstrg(ilast(1)-nchr_eiss_top+1:
     &                                     ilast(1))
                          cstr_tail = cstrg(ifirst(1):
     &                                      ilast(1)-nchr_eiss_top)
                      else
                          cstr_top = cstrg(ifirst(1):ilast(1))
                      endif
                      cstr64(1:len_cstr_top) =cstr_top

   55                 continue
                  elseif(ir.eq.2) then
                      cstr2 = '5'//cstrg(ifirst(1):ifirst(1))
                      i2=i4fctn(cstr2,iabt)
                      if((iabt.gt.0).or.(i2.lt.51)) go to 56
                      do i=1,i3
                        cstr2 = cstrg(ifirst(1)+i3*3-1:
     &                                ifirst(1)+i3*3)
                        i2=i4fctn(cstr2,iabt)
                        if((iabt.gt.0).or.(i2.lt.51)) go to 56
                      enddo
                      leiss = .true.
                      if((ilast(1)-ifirst(1)+1).gt.nchr_eiss_top-1) then
                          cstr_top = cstrg(ilast(1)-nchr_eiss_top+1:
     &                                     ilast(1))
                          cstr_tail = '5'//cstrg(ifirst(1):
     &                                           ilast(1)-nchr_eiss_top)
                      else
                          cstr_top = '5'//cstrg(ifirst(1):ilast(1))
                      endif
                      cstr64(1:len_cstr_top) =cstr_top
   56                 continue
                  endif
              elseif((ilast(1)-ifirst(1)+1).eq.3) then
                  cstr2 = cstrg(ifirst(1):ifirst(1)+1)
                  i2=i4fctn(cstr2,iabt)
                  if((iabt.eq.0).and.(i2.ge.10)) then
                      leiss = .true.
                      if(i2.lt.50) i2=i2+50
                      write(cstr_top(1:2),'(i2)')i2
                      cstr_top(3:3)=cstrg(ilast(1):ilast(1))
                      cstr64(1:len_cstr_top) =cstr_top
                  else
                      cstr1 = cstrg(ifirst(1):ifirst(1))
                      i2=i4fctn(cstr1,iabt)
                      cstr1 = cstrg(ifirst(1)+1:ifirst(1)+1)
                      if((iabt.eq.0).and.
     &                   (index(clett1,cstr1).gt.0))
     &                          lstan = .true.
                  endif
              elseif((ilast(1)-ifirst(1)+1).eq.2) then
                  cstr2 = ' '//cstrg(ifirst(1):ifirst(1))
                  i2=i4fctn(cstr2,iabt)
                  if(iabt.gt.0) go to 58
                  cstr1 = cstrg(ifirst(1)+1:ifirst(1)+1)
                  if((index(clett2,cstr1).gt.0)) then
                      write(i4unit(-1),1003)cstrg
                      write(i4unit(-1),1004)
                  endif
                  leiss = .true.
                  cstr_top='5'//cstrg(ifirst(1):ilast(1))
                      cstr64(1:len_cstr_top) =cstr_top
   58             continue
              endif
          else
c-----------------------------------------------------------------------
c  outdated short format - terminate subroutine
c-----------------------------------------------------------------------
              write(i4unit(-1),1001)'outdated string length = ',len_str
              write(i4unit(-1),1002)
              stop
          endif
          if(leiss)then
              strge = cstr64
              read(cstr64,fmt1044)(cnela(ishel),cheisa(ishel),
     &                          ishel=1,nd_eiss_top)
              do 80 ishel=1,nd_eiss_top
                nela(ishel)=0
                if(cnela(ishel).eq.'  ') go to 80
                nela(ishel) =i4fctn(cnela(ishel),iabt)
                if(iabt.gt.0)then
                    leiss = .false.
                    go to 100
                endif
   80         continue
c
              nshel = 0
              ip = 0
              maxn = 0
   81         continue
              if(nshel.lt.nd_eiss_top) then
                  if(nela(nshel+1).gt.0)then
                      nshel=nshel+1
                      nela(nshel)=mod(nela(nshel),50)
                      chstda(nshel)=cstgrp(cheisa(nshel))
                      ip = ip + nela(nshel)*i4pgrp(cheisa(nshel))
                      maxn = max0(maxn,i4ngrp(cheisa(nshel)))
                      go to 81
                  endif
              endif
              ip = mod(ip,2)

              if(nshel.gt.1.and.nshel.le.nd_eiss_top) then
                  write(strg,fmt1046)chstda(1),chqa(nela(1)),
     &                 (chstda(ishel),chqa(nela(ishel)),
     &                 ishel=2,nshel)
              elseif(nshel.gt.nd_eiss_top) then
                  write(strg,fmt1046)chstda(nshel-nd_eiss_top+1),
     &                  chqa(nela(nshel-nd_eiss_top+1)),
     &                    (chstda(ishel),chqa(nela(ishel)),
     &                    ishel=nshel-nd_eiss_top+2,
     &                    nshel)
              else
                  write(strg,fmt1046)chstda(1),chqa(nela(1))
              endif
c
              icfsel = 2
              call xxcftr( icfsel , strg  , cstrgo )
              if(cstrgo.eq.cstr64) then
                  nvlce = i4ngrp(cheisa(nshel))
                  lvlce = i4lgrp(cheisa(nshel))
                  return
              else
                  leiss = .false.
                  return
              endif
          elseif((.not.leiss).and.lstan) then
              strg = ' '
              strg=' '//cstrg(ifirst(1):ilast(1))
              icfsel = 2
              call xxcftr( icfsel , strg  , cstrgo )
              cstr1= cstrgo(3:3)
              nvlce = i4ngrp(cstr1)
              lvlce = i4lgrp(cstr1)
              cstr_top = cstrgo
              cstr_tail = ' '
              return
          endif
c

      elseif(nwords.gt.1) then
c-----------------------------------------------------------------------
c  multiple word case - check for standard form
c-----------------------------------------------------------------------
          do i=1,nwords
            if(ilast(i)-ifirst(i)+1.ne.3) go to 100
            if(index(clett1,cstrg(ifirst(i)+1:ifirst(i)+1))
     &         .eq.0) go to 100
          enddo

          ip = 0
          maxn = 0
          nshel = nwords
          do ishel =1,nshel
            read(cstrg(ifirst(ishel):ilast(ishel)),'(1a2,1a1)')
     &                                 chstda(ishel),chra(ishel)
            nela(ishel)=i4schr(chra(ishel),iabt)
            if(iabt.gt.0)then
               lstan = .false.
               go to 100
            endif
            cheisa(ishel)=ceigrp(chstda(ishel))
            ip = ip + nela(ishel)*i4pgrp(cheisa(ishel))
            maxn = max0(maxn,i4ngrp(cheisa(ishel)))
          enddo
          ip = mod(ip,2)
          if(nshel.gt.nd_stan_top) then
              write(strge,fmt1045)(nela(ishel)+50,cheisa(ishel),
     &                          ishel=nshel-nd_stan_top+1,nshel)
          else
              write(strge,fmt1045)
     &              (nela(ishel)+50,cheisa(ishel),ishel=1,nshel)
          endif

          if(nshel.gt.nd_eiss_top) then
              write(cstr_top,fmt1048)(nela(ishel)+50,cheisa(ishel),
     &                                ishel=nshel-nd_eiss_top+1,nshel)
              write(cstr_tail,fmt1047)(nela(ishel)+50,cheisa(ishel),
     &                                 ishel=1,nshel-nd_eiss_top)
          else
              write(cstr_top,fmt1048)
     &              (nela(ishel)+50,cheisa(ishel),ishel=1,nshel)
          endif

          icfsel = 3
          nw4 =max0(1,nshel-nd_stan_top+1)
          cstr64=' '//cstrg(ifirst(nw4):ilast(nshel))
          call xxcftr( icfsel , strge , cstrgo )

          call xxcmps(cstr64,cstrgo,lequiv)
          if(lequiv) then
              lstan = .true.
              nvlce = i4ngrp(cheisa(nshel))
              lvlce = i4lgrp(cheisa(nshel))
          else
              lstan = .false.
          endif
c
      endif
c
 100  return
c
c-----------------------------------------------------------------------
 1001 format(1x,32('*'),' xxdtes error ',32('*')//
     &       1x,'fault in configuration specification: ',a,i4)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,32('*'),' xxdtes warning ',32('*')//
     &       1x,'ambiguous eissner configuration : ',a)
 1004 format(/1x,28('*'),' subroutine continues ',28('*'))
c-----------------------------------------------------------------------
      end
