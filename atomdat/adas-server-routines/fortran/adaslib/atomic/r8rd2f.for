CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8rd2f.for,v 1.1 2004/07/06 14:42:46 whitefor Exp $ Date $Date: 2004/07/06 14:42:46 $
CX
       FUNCTION R8RD2F( N , L , L1 , E2 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8RD2F ******************
C
C  PURPOSE: CLACULATES HYDROGENIC BOUND-FREE RADIAL INTEGRALS USING
C           RECURRENCE RELATIONS.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8RD2F  = RESULT OF INTEGRAL.
C
C  INPUT:  (I*4)  N       = PRINCIAPL QUANTUM NUMBER OF BOUND ELECTRON.
C  INPUT:  (I*4)  L       = ORBITAL QUANTUM NUMBER OF BOUND ELECTRON.
C  INPUT:  (I*4)  L1      = ORBITAL QUANTUM NUMBER OF FREE ELECTRON.
C  INPUT:  (R*8)  E2      =
C
C  PARAM:  (R*8)  P1      = EQUATION CONSTANT = 64.
C  PARAM:  (R*8)  P12     = EQUATION CONSTANT = P1**2.
C  PARAM:  (R*8)  P2      = EQUATION CONSTANT = 1/P1.
C  PARAM:  (R*8)  P22     = EQUATION CONSTANT = P2**2.
C
C          (I*4)  JS      =
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XN1     = REAL VALUE = N-1.
C          (R*8)  XN2     = REAL VALUE = XN**2.
C          (R*8)  E       =
C          (R*8)  U       =
C          (R*8)  V       =
C          (R*8)  P       =
C          (R*8)  XI      = REAL VALUE = I.
C          (R*8)  XI1     = REAL VALUE = I+1.
C          (R*8)  XI12    = REAL VALUE = XI1**2.
C          (R*8)  T1      =
C          (R*8)  T2      =
C          (R*8)  T3      =
C
C ROUTINES: NONE
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     04/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8RD2F
C-----------------------------------------------------------------------
      REAL*8     P1              , P12               ,
     &           P2              , P22
      PARAMETER( P1 = 64.0D0     , P12 = P1**2       ,
     &           P2 = 1.0D0 / P1 , P22 = P2**2         )
C-----------------------------------------------------------------------
      INTEGER    N       , L       , L1
      INTEGER    JS      , I
C-----------------------------------------------------------------------
      REAL*8     E2
      REAL*8     XN      , XN1     , XN2     , E       , U       ,
     &           V       , P       , XI      , XI1     , XI12    ,
     &           T1      , T2      , T3
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INPUT PARAMETERS VALID.
C-----------------------------------------------------------------------
C
      IF ((N .LT. 1)  .OR. (L .LT. 0) .OR. (L .GE. N) .OR.
     &    (L1 .LT .0)                                      ) THEN
         R8RD2F = 0.0D0
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      XN  = DFLOAT( N )
      XN2 = XN**2
      E   = DSQRT( -E2 )
      V   = ( 1.0D0 - XN2 * E2 )**2
      U   = 8.0D0 * XN2 / V
      P   = 1.0D0
      JS  = 0
C
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , N
         XI = DFLOAT( I )
         P  = P * U * ( 1.0D0 - XI**2 * E2 ) /
     &        ( XI * ( 2.0D0 * XI - 1.0D0 ) )
         IF (DABS( P ) .LT. P22) THEN
            JS = JS - 1
            P  = P * P12
         ENDIF
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      IF (E .GT. 0.04D0) THEN
         P = XN * P / ( 1.0D0 - DEXP( -6.283185D0 / E ) )
      ELSE
         P = XN * P
      ENDIF
C
      IF (E .GT. 1.0D-5) THEN
         U = -2.0D0 * DATAN( XN * E ) / E
      ELSE
         U = -2.0D0 * XN
      ENDIF
C
      T2 = 7.089815D0 * DSQRT( P ) * DEXP( U ) / V
      V  = 1.0D0 - XN2 * E2
C
C-----------------------------------------------------------------------
C INTEGRAL CALCULATION FOR (L1-L)=1.
C-----------------------------------------------------------------------
C
      IF ((L1 - L) .EQ. 1) THEN
C
         U  = DSQRT( ( 2.0D0 * XN - 1.0D0 ) * V )
         T3 = U * T2 / 2.0D0
C
         IF ((N - L) .LT. 2) THEN
            T3 = T2
         ELSE
            DO 2 I = N-2 , L+1 , -1
               XI  = DFLOAT( I )
               XI1 = DFLOAT( I + 1 )
               XI12 = XI1**2
               T1  = T2
               T2  = T3
               T3  = T2 * ( 4.0D0 * ( XN2 - XI12 ) +
     &               XI1 * ( 2.0D0 * XI1 - 1.0D0 ) * V ) -
     &               2.0D0 * XN * U * T1
               U   = DSQRT( ( XN2 - XI**2 ) *
     &                      ( 1.0D0 - XI12 * E2 ) )
               T3  = T3 / ( 2.0D0 * XN * U )
               IF (DABS( T3 ) .GT. P1) THEN
                  JS = JS + 1
                  T3 = T3 * P2
                  T2 = T2 * P2
               ENDIF
    2       CONTINUE
         ENDIF
C
         R8RD2F = XN2**2 * T3**2 * P12**JS
C
C-----------------------------------------------------------------------
C INTEGRAL CALCULATION FOR (L-L1)=1.
C-----------------------------------------------------------------------
C
      ELSE IF ((L - L1) .EQ. 1) THEN
C
         XN1 = DFLOAT( N - 1 )
         U   = DSQRT( V / ( 1.0D0 - XN1**2 * E2 ) )
         T2  = T2 * U / ( 2.0D0 * XN )
         U   = DSQRT( (2.0D0 * XN - 1.0D0) *
     &                (1.0D0 - E2 * ( XN - 2.0D0 )**2 ) )
         T3  = ( 4.0D0 + XN1 * V ) * ( 2.0D0 * XN - 1.0D0 ) * T2 /
     &         ( 2.0D0 * XN * U )
C
         IF ((N - L) .LT. 2) THEN
            T3 = T2
         ELSE
            DO 3 I = N-3 , L , -1
               XI  = DFLOAT( I )
               XI1 = DFLOAT( I + 1 )
               XI12 = XI1**2
               T1  = T2
               T2  = T3
               T3  = T2 * ( 4.0D0 * ( XN2 - XI12 ) +
     &               XI1 * ( 2.0D0 * XI1 + 1.0D0 ) * V ) -
     &               2.0D0 * XN * U * T1
               U   = DSQRT( (XN2 - XI12 ) *
     &                      ( 1.0D0 - XI**2 * E2 ) )
               T3  = T3 / ( 2.0D0 * XN * U )
               IF (DABS(T3) .GT. P1) THEN
                  JS = JS + 1
                  T3 = T3 * P2
                  T2 = T2 * P2
               ENDIF
    3       CONTINUE
         ENDIF
C
         R8RD2F = XN2**2 * T3**2 * P12**JS
C
C-----------------------------------------------------------------------
C INTEGRAL VALUE FOR ALL OTHER CASES.
C-----------------------------------------------------------------------
C
      ELSE
         R8RD2F = 0.0D0
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
