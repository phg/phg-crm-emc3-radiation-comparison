CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4idfm.for,v 1.3 2007/04/11 13:01:49 allan Exp $ Data $Date: 2007/04/11 13:01:49 $
CX
      FUNCTION I4IDFM( N , L , M )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  *************** FORTRAN77 INTEGER*4 FUNCTION: I4INDL ****************
C
C  PURPOSE:  RETURNS A UNIQUE INDEX NUMBER BASED ON THE VALUE OF THE
C            N, L AND M QUANTUM NUMBERS PASSED TO IT. THE INDEX IS USED
C            TO REFERENCE ARRAYS CONTAINING DATA DEPENDENT ON THE
C            N, L AND M QUANTUM NUMBERS.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  FUNC:   (I*4)   I4IDFM  = INDEX
C
C  INPUT:  (I*4)   N       = N QUANTUM NUMBER.
C  INPUT:  (I*4)   L       = L QUANTUM NUMBER.
C  INPUT:  (I*4)   M       = M QUANTUM NUMBER.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     10/09/93
C
C VERSION  : 1.2                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION  : 1.3                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    I4IDFM
C-----------------------------------------------------------------------
      INTEGER    N  , L  , M
C-----------------------------------------------------------------------
C
C***********************************************************************
C CALCULATE INDEX.
C***********************************************************************
C
      I4IDFM = (((N - 1) * N * (N + 1)) / 6) + ((L * (L + 1)) / 2) +
     &         M + 1
C
C-----------------------------------------------------------------------
C
      RETURN
      END
