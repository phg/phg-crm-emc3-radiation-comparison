CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxceia.for,v 1.2 2007/07/20 10:50:10 allan Exp $ Date $Date: 2007/07/20 10:50:10 $
CX
      SUBROUTINE XXCEIA( EIA )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXCEIA *********************
C
C  PURPOSE: CONVERTS IONISATION POTENTIALS FROM WAVE NUMBERS TO
C           RYDBERGS AND FILLS IN ANY MISSING VALUES UP TO AN ION CHARGE
C           OF 50.
C
C           (IF 'EIA()' IS ALL ZERO - RETURN)
C           (PRIOR TO 16/08/90 WAS KNOWN AS 'E3EIA' - SLIGHTLY AMENDED)
C
C  CALLING PROGRAMS: GENERAL USE
C
C  SUBROUTINE:
C
C  I/O   : (R*8)  EIA()   = IONISATION POTENTIALS: ()=ION CHARGE
C  INPUT :                  UNITS - WAVE NUMBERS (CM-1)
C  OUTPUT:                  UNITS - RYDBERGS
C
C          (R*8) WN2RYD   = WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IPOT    = NUMBER OF IONISATION POTENTIAL VALUES
C                           PRESENT IN THE INPUT FILE.
C          (I*4)  I       = GENERAL USE
C
C          (R*8)  A1      = EXTRAPOLATION EQUATION COEFFICIENT
C          (R*8)  A2      = EXTRAPOLATION EQUATION COEFFICIENT
C          (R*8)  A3      = EXTRAPOLATION EQUATION COEFFICIENT
C          (R*8)  XI      = VALUE AT WHICH EXTRAPOLATION IS REQUIRED
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS     INTEGER*4 FUNCTION -
C                              FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C NOTE:
C           THE NUMBER OF IONISATION COEFFICIENTS PRESENT WILL BE EITHER
C           30 OR 50. IF 30 THEN THE VALUES FROM  31  TO  50 NEED TO  BE
C           EXTRAPOLATED. THE EXTRAPOLATION EQUATION  IS  BASED  ON  THE
C           VALUES OF EIA(20), EIA(25) AND EIA(50).
C
C           THE EXTRAPOLATION EQUATION BEING:
C
C           EIA(XI) = A1 + A2*XI + A2*XI*XI
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    16/08/90
C
C UPDATE:  11/08/93  HP SUMMERS - DO NOT STOP IF IPOT.LE.30, BUT DO NOT
C                                 EXTRAPOLATE
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE (NO CHANGES)
C
C VERSION: 1.2                          DATE: 20-07-07
C MODIFIED: Allan Whiteford
C               - Small modifications to comments to allow for
C                 automatic documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IPOT    , I      , I4UNIT
C-----------------------------------------------------------------------
      REAL*8    WN2RYD
      REAL*8    A1      , A2     , A3     , XI
      REAL*8    EIA(50)
C-----------------------------------------------------------------------
      PARAMETER (WN2RYD=9.11269D-06)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      IPOT=0
         DO 1 I=1,50
            IF (EIA(I).GT.0.0D0) IPOT=I
    1    CONTINUE
C-----------------------------------------------------------------------
      IF (IPOT.EQ.0)  RETURN
C
C-----------------------------------------------------------------------
C CONVERT ION. RATE COEFFICIENTS TO RYDBERGS
C-----------------------------------------------------------------------
C
         DO 2 I=1,IPOT
            EIA(I)=WN2RYD*EIA(I)
    2    CONTINUE
C-----------------------------------------------------------------------
 
      IF (IPOT.LT.30) THEN
          WRITE(I4UNIT(-1),*)'  XXCIEA WARNING: IPOT < 30'
          RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C EXTRAPOLATE ANY MISSING IONISATION RATE COEFFS.
C-----------------------------------------------------------------------
C
         IF (IPOT.LT.50) THEN
            A1=0.02*(EIA(20)-2.0*EIA(25)+EIA(30))
            A2=-0.1*(11.0*EIA(20)-20.0*EIA(25)+9.0*EIA(30))
            A3=15.0*EIA(20)-24.0*EIA(25)+10.0*EIA(30)
               DO 3 I=IPOT+1,50
                  XI=DBLE(I)
                  EIA(I)=A3+XI*(A2+XI*A1)
    3          CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
