      function r8giiiav(u, gam2)

      implicit none
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8GIIIAV ****************
C
C
C  PURPOSE: CALCULATES MAXWELLIAN AVERAGED FREE-FREE GAUNT FACTORS
C
C  INPUT
C      U=HV/KT  WHERE HV IS PHOTON ENERGY
C               AND KT IS ELECTRON TEMPERATURE (ENERGY UNITS)
C      GAM2=Z*Z*IH/KT  WHERE Z IS TARGET ION CHARGE
C               AND IH IS THE RYDBERG ENERGY
C  OUTPUT
C      GIIIAV=MAXWELLIAN AVERAGED FREE-FREE GAUNT FACTOR.
C
C
C  AUTHOR   : H P Summers
C  DATE     : 22-11-1984
C
C
C  VERSION  : 1.1
C  DATE     : 02-03-2005
C  MODIFIED : Martin O'Mullane
C                - First version in central ADAS.
C                - Make implicit none.
C
C  VERSION  : 1.2
C  DATE     : 15-03-2006
C  MODIFIED : Allan Whiteford
C                - Removed external giii statement
C
C  VERSION  : 1.3                          
C  DATE     : 10-04-2007
C  MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      integer    I
C-----------------------------------------------------------------------
      real*8     r8giii  , r8giiiav
      real*8     gam2    , u
      real*8     sum     , x       , e1        , e2     , rgam2
C-----------------------------------------------------------------------
      real*8     XA(8)   , WA(8)
C-----------------------------------------------------------------------
      data xa / 0.17027963D+00,  0.90370178D+00,  2.25108663D+00,
     &          4.26670017D+00,  7.04590540D+00, 10.75851601D+00,
     &         15.74067864D+00, 22.86313174D+00/
      data wa / 3.69188589D-01,  4.18786781D-01,  1.75794987D-01,
     &          3.33434923D-02,  2.79453624D-03,  9.07650877D-05,
     &          8.48574672D-07,  1.04800117D-09/
C-----------------------------------------------------------------------

      RGAM2=1.0D0/GAM2
      SUM=0.0D0

      DO I=1,8
        X=XA(I)
        E1=RGAM2*X
        E2=RGAM2*(X+U)
        SUM=SUM+WA(I)*R8GIII(1,1,E1,E2)
      END DO

      R8GIIIAV=SUM

      RETURN

      END
