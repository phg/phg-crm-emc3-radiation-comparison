CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8wig6.for,v 1.1 2004/07/06 14:42:53 whitefor Exp $ Date $Date: 2004/07/06 14:42:53 $
CX
      FUNCTION R8WIG6( A1 , A2 , A3 , B1 , B2 , B3 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8WIG6 ******************
C
C  PURPOSE:
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8WIG6  =
C
C  INPUT:  (R*8)  A1      =
C  INPUT:  (R*8)  A2      =
C  INPUT:  (R*8)  A3      =
C  INPUT:  (R*8)  B1      =
C  INPUT:  (R*8)  B2      =
C  INPUT:  (R*8)  B3      =
C
C          (I*4)  J1      =
C          (I*4)  J2      =
C          (I*4)  J3      =
C          (I*4)  J4      =
C          (I*4)  J       =
C          (I*4)  NMIN    =
C          (I*4)  NMAX    =
C          (I*4)  N1      =
C          (I*4)  N2      =
C          (I*4)  N3      =
C          (I*4)  N4      =
C          (I*4)  N5      =
C          (I*4)  N6      =
C          (I*4)  N7      =
C          (I*4)  N       = LOOP INDEX.
C          (I*4)  M1      =
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  D1      =
C          (R*8)  D2      =
C          (R*8)  D3      =
C          (R*8)  D4      =
C          (R*8)  D       =
C          (R*8)  DSUM    =
C          (R*8)  DT      =
C          (R*8)  DP1     =
C          (R*8)  DP2     =
C          (R*8)  DP3     =
C          (R*8)  DP4     =
C          (R*8)  DP5     =
C          (R*8)  DP6     =
C          (R*8)  DP7     =
C          (R*8)  DP8     =
C
C          (I*4)  K()     =
C                           DIMENSION: 4
C          (I*4)  L()     =
C                           DIMENSION: 3
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4JGAM     ADAS      RETURNS VALUE FROM 'JGAM' TABLE.
C          R8GAM      ADAS      RETURNS VALUE FROM 'GAM' TABLE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8    R8WIG6
C-----------------------------------------------------------------------
      INTEGER   I4JGAM
      REAL*8    R8GAM
C-----------------------------------------------------------------------
      INTEGER   J1      , J2      , J3      , J4      , J       ,
     &          NMIN    , NMAX    , N1      , N2      , N3      ,
     &          N4      , N5      , N6      , N7      , N       ,
     &          M1      , I
C-----------------------------------------------------------------------
      REAL*8    A1      , A2      , A3      , B1      , B2      ,
     &          B3
      REAL*8    D1      , D2      , D3      , D4      , D       ,
     &          DSUM    , DT      , DP1     , DP2     , DP3     ,
     &          DP4     , DP5     , DP6     , DP7     , DP8
C-----------------------------------------------------------------------
      INTEGER   K(4)    , L(3)
C-----------------------------------------------------------------------
C
      CALL XXDELT( A1 , A2 , A3 , D1 , J1 )
      CALL XXDELT( A1 , B2 , B3 , D2 , J2 )
      CALL XXDELT( A2 , B1 , B3 , D3 , J3 )
      CALL XXDELT( A3 , B1 , B2 , D4 , J4 )
C
      D = D1 * D2 * D3 * D4
      J = J1 + J2 + J3 + J4
C
      IF (D .LE. 1.0E-20) THEN
         R8WIG6 = 0.0D0
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C
      K(1) = IDINT( A1 + A2 + A3 + 0.001 )
      K(2) = IDINT( A1 + B2 + B3 + 0.001 )
      K(3) = IDINT( A2 + B1 + B3 + 0.001 )
      K(4) = IDINT( A3 + B1 + B2 + 0.001 )
      L(1) = IDINT( A1 + B1 + A2 + B2 + 0.001 )
      L(2) = IDINT( A2 + B2 + A3 + B3 + 0.001 )
      L(3) = IDINT( A1 + B1 + A3 + B3 + 0.001 )
C
      NMIN = K(1)
      DO 1 I = 2 , 4
         IF (K(I) .GT. NMIN) NMIN = K(I)
    1  CONTINUE
C
      NMAX = L(1)
      DO 2 I = 2 , 3
         IF (L(I) .LT. NMAX) NMAX = L(I)
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
      N1 = NMIN - K(1)
      N2 = NMIN - K(2)
      N3 = NMIN - K(3)
      N4 = NMIN - K(4)
      N5 = L(1) - NMIN
      N6 = L(2) - NMIN
      N7 = L(3) - NMIN
C
      D = DSQRT( D ) * (-1.0D0)**NMIN
      D = D * R8GAM( NMIN+2 ) / ( R8GAM( N1+1 ) * R8GAM( N2+1 )
     &    * R8GAM( N3+1 ) * R8GAM( N4+1 ) * R8GAM( N5+1 )
     &    * R8GAM( N6+1 ) * R8GAM( N7+1 ) )
      M1 = I4JGAM( NMIN+2 ) - I4JGAM( N1+1 ) - I4JGAM( N2+1 )
     &     - I4JGAM( N3+1 ) - I4JGAM( N4+1 ) - I4JGAM( N5+1 )
     &     - I4JGAM( N6+1 ) - I4JGAM( N7+1 )
      D = D * 8.0D0**( J + 2 * M1 )
C
C-----------------------------------------------------------------------
C
      DSUM = 1.0D0
      DT  = 1.0D0
      DP1 = DFLOAT( N1 )
      DP2 = DFLOAT( N2 )
      DP3 = DFLOAT( N3 )
      DP4 = DFLOAT( N4 )
      DP5 = DFLOAT( N5 + 1 )
      DP6 = DFLOAT( N6 + 1 )
      DP7 = DFLOAT( N7 + 1 )
      DP8 = DFLOAT( NMIN + 1 )
C
      DO 3 N = NMIN , NMAX-1
         DP1 = DP1 + 1.0D0
         DP2 = DP2 + 1.0D0
         DP3 = DP3 + 1.0D0
         DP4 = DP4 + 1.0D0
         DP5 = DP5 - 1.0D0
         DP6 = DP6 - 1.0D0
         DP7 = DP7 - 1.0D0
         DP8 = DP8 + 1.0D0
         DT  = -DT * DP5 * DP6 * DP7 * DP8
     &         / ( DP1 * DP2 * DP3 * DP4 )
         DSUM = DSUM + DT
    3 CONTINUE
C
C-----------------------------------------------------------------------
C
      R8WIG6 = D * DSUM
C
C-----------------------------------------------------------------------
C
      RETURN
      END
