CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/bf.for,v 1.2 2007/04/11 13:01:41 allan Exp $ Date $Date: 2007/04/11 13:01:41 $
CX
       SUBROUTINE BF(G,N,A)                                          
C
       IMPLICIT REAL*8 (A-H,O-Z)                                    
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BF *************************
C
C PURPOSE: Evaluates the hydrogenic <nl|r|kl> bound-free radial integral
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
       DIMENSION G(1000),Z(1000)                                   
       U=64.0                                                     
       V=0.015625                                                
       U=U*U                                                    
       V=V*V                                                   
       UU=U*U                                                 
       VV=V*V                                                
       AA=A*A                                               
       TN=N                                                
       C=AA*TN*TN                                         
       IF(C-0.01)1,1,2                                   
    1  T=0.2-0.142857143*C                              
       T=0.33333333-T*C                                
       T=1.0-T*C                                      
       T=2.0*TN*T                                    
       GO TO 3                                      
    2  T=2.0*DATAN (TN*A)/A                        
    3  T1=1.0+C                                   
       T4=1.0/(T1*T1)                            
       IF(A-0.3)4,4,5                           
    4  T2=1.0                                  
       GO TO 8                                                          
    5  T3=6.2831853/A                           
       IF(T3-0.1)7,7,6                         
    6  T2=1.0-DEXP (-T3)                      
       GO TO 8                               
    7  T2=1.0-0.16666667*T3                 
       T2=1.0-0.2*T3*T2                   
       T2=1.0-0.25*T3*T2                 
       T2=1.0-0.33333333*T3*T2                   
       T2=1.0-0.5*T3*T2                           
       T2=T3*T2                                    
    8  J=0                                          
       J2=1                                          
   25  IF(T-20.0)26,24,24                             
   24  T=0.5*T                                         
       J2=J2+J2                                         
       GO TO 25                                          
   26  T3=DEXP (-T)                                       
       T=1.0                                               
       DO 27 J1=1,J2                                        
       T=T3*T                                                
   28  IF(T-1.0E-10)29,29,27                                  
   29  T=U*T                                                   
       J=J+1                                                    
       GO TO 28                                                  
   27  CONTINUE                                                   
       C=16.0*TN*TN*T4                                             
       G2=3.14159265*TN*T4*T4/T2                                    
       DO 12 JS=1,N                                                  
       S=JS                                                           
   23  IF(G2-1.0E25)9,13,13                                            
   13  G2=VV*G2                                               
       J=J-1                                                 
       GO TO 23                                             
    9  IF(G2-1.0E-25)10,10,11                              
   10  G2=UU*G2                                           
       J=J+1                                             
       GO TO 9                                          
   11  T2=(1.0+S*S*AA)*C/((S+S)*(S+S-1.0))             
   12  G2=T2*G2                                       
       G(N)=4.0*T*DSQRT (G2)                         
       J4=J                                         
       IF(N-2)18,14,14                             
   14  G(N-1)=0.5*G(N)*DSQRT ((TN+TN-1.0)*T1)     
       IF(N-2)18,18,15                           
   15  J1=N-1                                   
       T=TN*TN                                 
       DO 16 J=1,J1                                            
       T2=J                                                     
       T3=J+1                                                    
   16  Z(J)=DSQRT ((T-T2*T2)*(1.0+T3*T3*AA))*2.0*TN               
       J1=N-2                                                      
       DO 17 J=1,J1                                                 
       J2=N-J                                                        
       T2=J2                                                            
       T3=(4.0*(T-T2*T2)+T2*(T2+T2-1.0)*T1)*G(J2)-Z(J2)*G(J2+1)        
       G(J2-1)=T3/Z(J2-1)                                             
   20  IF(G(J2-1)-1.0)17,21,21                               
   21  J3=J2-1                                                
       DO 22 L=J3,N                                            
   22  G(L)=V*G(L)                                              
       J4=J4-1                                                   
       GO TO 20                                                   
   17  CONTINUE                                                    
   18  T=1.0                                                        
       IF(J4)30,31,32                                                
   30  J4=-J4                                                         
       DO 33 J=1,J4                                                    
   33  T=T*U                                                            
       GO TO 31                        
   32  DO 34 J=1,J4                     
   34  T=T*V                             
   31  DO 19 J=1,N                        
       T2=T*G(J)                           
   19  G(J)=T1*T2*T2                        
       RETURN                                
       END                                     
