      SUBROUTINE XXCFTR( ICFSEL , CSTRGI , CSTRGO )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXCFTR *********************
C
C  PURPOSE: CONVERTS A CONFIGURATION CHARACTER STRING, SUCH AS OCCURS
C           IN A SPECIFIC ION FILE LEVEL LIST, BETWEEN EISSNER AND
C           STANDARD FORMS
C
C  CALLING PROGRAMS: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   ICFSEL  = 1 => STANDARD FORM OUT, STANDARD FORM IN
C                            2 => EISSNER  FORM OUT, STANDARD FORM IN
C                            3 => STANDARD FORM OUT, EISSNER  FORM IN
C                            4 => EISSNER  FORM OUT, EISSNER  FORM IN
C  INPUT : (C*(*)) CSTRGI  = CONFIGURATION STRING IN INPUT FORM
C  OUTPUT: (C*(*)) CSTRGO  = CONFIGURATION STRING IN OUTPUT FORM
C
C          (I*4)   I       = GENERAL USE
C          (I*4)   ISHEL   = SHELL COUNTER
C          (I*4)   IP      = PARITY OF CONFIGURATION
C          (I*4)   MAXN    = N_SHELL SUM FOR CONFIGURATION
C          (I*4)   NSHEL   = NUMBER OF SHELLS IDENTIFIED fFROM STRING
C          (I*4)   NELA()  = NUMBER OF ELECTRONS IN EACH SHELL
C
C          (C*19)  STRG    = STANDARD FORM CONFIGURATION STRING
C          (C*19)  STRGE   = EISSNER FORM CONFIGURATION STRING
C          (C*1)   CHEISA()= EISSNER CHARACTER FOR ORBITALS
C          (C*2)   CHSTDA()= STANDARD ORBITAL SPEC. FOR EACH SHELL
C                            (EISSNER FORM CASE)
C          (C*1)   CHQA()  = INDEX TO HEXADECIMAL CONVERSIONS
C          (C*1)   CHRA()  = CHAR. FOR NO. OF. EQUIV. ELEC. IN SHELL
C                            (STANDARD FORM CASE)
C
C          (L*4)   LEISS   = .TRUE.  => EISSNER FORM
C                            .FALSE. => NOT EISSNER FORM
C
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS     FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4NGRP     ADAS     RETURNS N QUANTUM NUMBER IN THE
C                              EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C          I4PGRP     ADAS     RETURNS PARITY OF ORBITAL GIVEN THE
C                              EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C          I4SCHR     ADAS     RETURNS NUMERICAL VALUE FOR NUMBER OF
C                              EQUIVALENT ELECTRONS GIVEN AS HEX> CHAR.
C          CSTGRP     ADAS     RETURNS TERM OF ORBITAL GIVEN IN THE
C                              EISSNER SINGLE HEXADECIMAL CHARACTER FORM
C          CEIGRP     ADAS     RETURNS EISSNER CODE FOR ORBITAL
C          lenstr     ADAS     returns the effective length of a string
C          xxslen     ADAS     identifies first and last non-blank 
C                              characters in a string
C
C
C NOTE:    THE ROUTINE IS USED TO CONVERT THE CONFIGURATION CHARACTER
C          STRING OCCURRING IN ADF04 FILE LEVEL LISTS.  THE STRING
C          LENGTH ALLOCATED TO THIS IS *18 FOLLOWING 1 BLANK SPACE
C          AFTER THE LEVEL INDEX.  A PROBLEM ARISES WHEN THE FIRST
C          SHELL CONTAINS MORE THAN 9 EQUIVALENT ELECTRONS.  IN THIS
C          CASE, OVERSPILL IS ALLOWED INTO THE BLANK CHARACTER SPACE.
C          THE ROUTINE WILL ANALYSE A *19 STRING INCLUDING THE USUALLY
C          BLANK LOCATION OR A *18 STRING EXCLUDING IT.  IN THE LATTER
C          CASE AN INTELLIGENT GUESS IS MADE AS TO WHETHER THE OMITTED
C          BLANK SHOULD IN FACT BE A '1'.  THIS SITUATION OCCURS FOR A
C          LEADING CLOSED D-SHELL.
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    25/10/95
C
C
C UPDATE:  19/02/03  H. P. SUMMERS - EXTENDED RANGE AND STRINGS
C
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 19-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 19-02-03
C MODIFIED: H. P. SUMMERS
C               - EXTENDED RANGE AND STRINGS
C
C VERSION: 1.3                          DATE: 12-12-08
C MODIFIED: Adam Foster
C               - Fixed standard form check to include small 'h'
C               - Allowed output strings longer than 19 characters
C                 to be handled - output is put in first 19 spaces
C                 (previously blank string was returned if ouput string
C                 not 18 or 19 characters long)
C
C VERSION : 1.4
C DATE    : 02-09-11
C MODIFIED: Hugh Summers
C               - Made safe for longer configuration strings with
C                 standard error detection and advise.
C               - Now accepts a triplet starting with 5 as an
C                 acceptable Eissner form.
C
C VERSION : 1.5
C DATE    : 17-07-2012
C MODIFIED: Martin O'Mullane
C               - For the 'Eissner in' choice, adjust the first character
C                 by checking for the number of triplets.
C               - For the 'standard in' choice, adjust by checking if the
C                 first character is a space.
C               - Do not stop on error since this makes xxdtes useless
C                 as a way of determining the form of the configuration.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      integer   ndshel
C-----------------------------------------------------------------------
      parameter ( ndshel = 21 )
C-----------------------------------------------------------------------
      INTEGER   ICFSEL    , I         , L1      , L2
      INTEGER   I4UNIT    , I4NGRP    , I4PGRP  , I4SCHR
      INTEGER   ISHEL     , NSHEL     , IP      , MAXN
      INTEGER   IABT
      integer   lenstr
      integer   nd_eiss_cstrgi       , nd_stan_cstrgi
C-----------------------------------------------------------------------
      CHARACTER CSTRGI*(*), CSTRGO*(*)
      CHARACTER cstr64*64 , strg64*64   , strge64*64
      CHARACTER CSTGRP*2  , CEIGRP*1
      character fmt1045*12              , fmt1046*18
C-----------------------------------------------------------------------
      LOGICAL*4 LEISS
C-----------------------------------------------------------------------
      INTEGER   NELA(ndshel)
C-----------------------------------------------------------------------
      CHARACTER CHEISA(ndshel)*1 , CHSTDA(ndshel)*2  , CHRA(ndshel)*1
      CHARACTER CHQA(61)*1
C-----------------------------------------------------------------------
      DATA CHQA     /'1','2','3','4','5','6','7','8','9',
     &               'A','B','C','D','E','F','G','H','I',
     &               'J','K','L','M','N','O','P','Q','R',
     &               'S','T','U','V','W','X','Y','Z','a',
     &               'b','c','d','e','f','g','h','i','j',
     &               'k','l','m','n','o','p','q','r','s',
     &               't','u','v','w','x','y','z'/
C-----------------------------------------------------------------------
C
C---------------------------------------------------------------------
C  INITIALISE AND PRELIMINARY CHECKS
C---------------------------------------------------------------------

         CSTRGO = ' '
         cstr64 = ' '
         strg64   = ' '
         strge64  = ' '

         IF(ICFSEL.LT.1.OR.ICFSEL.GT.4) THEN
              write(i4unit(-1),1000)icfsel
              write(i4unit(-1),1002)
             STOP
         ENDIF

         if(lenstr(cstrgi)+1.gt.len(cstr64))then
              write(i4unit(-1),1001)'cstr64 to ',lenstr(cstrgi)+1
              write(i4unit(-1),1002)
              stop
         endif

C
C---------------------------------------------------------------------
C  DECIDE INPUT STRING LENGTH.  ADJUST FIRST CHARACTER IN cstr64 IF
C  APPROPRIATE.
C---------------------------------------------------------------------
C
C          IF(LEN(CSTRGI).EQ.18)THEN
C               cstr64=' '//CSTRGI
C               IF((ICFSEL.EQ.3.OR.ICFSEL.EQ.4).AND.
C      &            (cstr64(2:2).EQ.'0'))then
C                   cstr64(1:1)='1'
C               ENDIF
C          ELSEIF(LEN(CSTRGI).GE.19)THEN
C               if(ICFSEL.EQ.1.OR.ICFSEL.EQ.2)then
C                   if(CSTRGI(1:1).ne.' ')then
C                       cstr64=' '//CSTRGI
C                   else
C                       cstr64=CSTRGI
C                   endif
C               else
C                   cstr64=CSTRGI
C               endif
C 
C          ELSE
C               write(i4unit(-1),1001)'cstrgi to ',19
C               write(i4unit(-1),1002)
C               stop
C          ENDIF
         
         call xxslen(CSTRGI,L1,L2)
         
         cstr64 = CSTRGI
         
         if (icfsel.eq.1.or.icfsel.eq.2) then
         
            if (CSTRGI(1:1).ne.' ') cstr64 = ' '//CSTRGI
            
         elseif (icfsel.eq.3.or.icfsel.eq.4) then
         
            if (mod(L2-L1+1, 3).NE.0)  cstr64 = '5'//CSTRGI
         
         endif
         
         nd_eiss_cstrgi = len(cstr64)/3
         nd_stan_cstrgi = len(cstr64)/4

         fmt1045 = '(??(I2,1A1))'
         fmt1046 = '(1X,??(1A2,A1,1X))'

         write(fmt1045(2:3),'(i2)')nd_eiss_cstrgi
         write(fmt1046(5:6),'(i2)')nd_stan_cstrgi


C---------------------------------------------------------------------
C  SCAN LEVEL, CHECK IF EISSNER INPUT FORM OR NOT, PARSE INTO
C  SHELLS AND SET UP strg64, strge64 STRING ARRAYS.
C---------------------------------------------------------------------
C
      IF(ICFSEL.EQ.3.OR.ICFSEL.EQ.4)THEN
C-------- CHECK CONSISTENT WITH EISSNER
C         LEISS = .TRUE.
            IF(INDEX(cstr64,'s').GT.0.OR.
     &         INDEX(cstr64,'p').GT.0.OR.
     &         INDEX(cstr64,'d').GT.0.OR.
     &         INDEX(cstr64,'f').GT.0) THEN
               WRITE(I4UNIT(-1),*)
     &         'XXCFTR ERROR: CONFIGURATION STRING OF INCORRECT FORM: ',
     &          cstr64
               STOP
            ELSE
                 strge64 = cstr64
                 READ(cstr64,fmt1045)(NELA(ISHEL),CHEISA(ISHEL),
     &                               ISHEL=1,nd_eiss_cstrgi)
                 NSHEL = 0
                 IP = 0
                 MAXN = 0
   81            CONTINUE
                 IF(NSHEL.LT.nd_eiss_cstrgi) THEN
                     IF(NELA(NSHEL+1).GT.0)THEN
                         NSHEL=NSHEL+1
                         NELA(NSHEL)=MOD(NELA(NSHEL),50)
                         CHSTDA(NSHEL)=CSTGRP(CHEISA(NSHEL))
                         IP = IP + NELA(NSHEL)*I4PGRP(CHEISA(NSHEL))
                         MAXN = MAX0(MAXN,I4NGRP(CHEISA(NSHEL)))
                         GO TO 81
                     ENDIF
                 ENDIF
                 IP = MOD(IP,2)
                 IF(NSHEL.GT.1.AND.NSHEL.LE.nd_stan_cstrgi) THEN
                     WRITE(strg64,fmt1046)CHSTDA(1),CHQA(NELA(1)),
     &                    (CHSTDA(ISHEL),CHQA(NELA(ISHEL)),
     &                    ISHEL=2,NSHEL)
                 ELSEIF(NSHEL.GT.nd_stan_cstrgi) THEN
                     WRITE(strg64,fmt1046)
     &                    CHSTDA(NSHEL-nd_stan_cstrgi+1),
     &                    CHQA(NELA(NSHEL-nd_stan_cstrgi+1)),
     &                    (CHSTDA(ISHEL),CHQA(NELA(ISHEL)),
     &                    ISHEL=NSHEL-nd_stan_cstrgi+2,NSHEL)
                 ELSE
                     WRITE(strg64,fmt1046)CHSTDA(1),CHQA(NELA(1))
                 ENDIF
            ENDIF
      ELSEIF(ICFSEL.EQ.1.OR.ICFSEL.EQ.2)THEN
C-------- CHECK CONSISTENT WITH STANDARD
C         LEISS = .FALSE.
            IF(INDEX(cstr64,'S').EQ.0.AND.
     &         INDEX(cstr64,'P').EQ.0.AND.
     &         INDEX(cstr64,'D').EQ.0.AND.
     &         INDEX(cstr64,'F').EQ.0.AND.
     &         INDEX(cstr64,'G').EQ.0.AND.
     &         INDEX(cstr64,'H').EQ.0.AND.
     &         INDEX(cstr64,'s').EQ.0.AND.
     &         INDEX(cstr64,'p').EQ.0.AND.
     &         INDEX(cstr64,'d').EQ.0.AND.
     &         INDEX(cstr64,'f').EQ.0.AND.
     &         INDEX(cstr64,'g').EQ.0.AND.
     &         INDEX(cstr64,'h').EQ.0) THEN
               WRITE(I4UNIT(-1),*)
     &         'XXCFTR ERROR: CONFIGURATION STRING OF INCORRECT FORM',
     &          cstr64
C               STOP
            ELSE
                 strg64 = cstr64
                 READ(cstr64,fmt1046)(CHSTDA(ISHEL),CHRA(ISHEL),
     &                             ISHEL=1,nd_stan_cstrgi)
                 NSHEL = 0
                 IP = 0
                 MAXN = 0
   83            CONTINUE
                 IF(NSHEL.LT.nd_stan_cstrgi) THEN
                     IF(CHRA(NSHEL+1).NE.' ') THEN
                         NELA(NSHEL+1)=I4SCHR(CHRA(NSHEL+1),IABT)
                         NSHEL=NSHEL+1
                         CHEISA(NSHEL)=CEIGRP(CHSTDA(NSHEL))
                         IP = IP + NELA(NSHEL)*I4PGRP(CHEISA(NSHEL))
                         MAXN = MAX0(MAXN,I4NGRP(CHEISA(NSHEL)))
                         GO TO 83
                     ENDIF
                 ENDIF
                 IP = MOD(IP,2)
                 IF(NSHEL.GT.1) THEN
                     WRITE(strge64,fmt1045)NELA(1)+50,CHEISA(1),
     &                    (NELA(ISHEL)+50,CHEISA(ISHEL),ISHEL=2,NSHEL)
                 ELSE
                     WRITE(strge64,fmt1045)NELA(1)+50,CHEISA(1)
                 ENDIF
            ENDIF
      ENDIF
C
      IF(ICFSEL.EQ.1.OR.ICFSEL.EQ.3) THEN
          IF(LEN(CSTRGO).GE.lenstr(strg64)) then
              IF(LEN(CSTRGO).EQ.18) then
                  CSTRGO=strg64(2:19)
              else
                  CSTRGO(1:lenstr(strg64))=strg64
              endif
          else
              write(i4unit(-1),1001)'cstrgo to ',len(strg64)
              write(i4unit(-1),1002)
              stop
          endif

      ELSEIF(ICFSEL.EQ.2.OR.ICFSEL.EQ.4) THEN
          IF(LEN(CSTRGO).GE.lenstr(strge64)) then
              IF((LEN(CSTRGO).EQ.18).or.(strge64(1:1).eq.' ')) then
                  CSTRGO(1:lenstr(strge64)-1)=strge64(2:len(strge64))
              else
                  CSTRGO(1:lenstr(strge64))=strge64
              endif
          else
              write(i4unit(-1),1001)'cstrgo to ',len(strge64)
              write(i4unit(-1),1002)
              stop
          endif
      ENDIF
C
      RETURN
C
C-----------------------------------------------------------------------
 1000 format(1x,32('*'),' xxcftr error ',32('*')//
     &       1x,'Non-allowed selection value = ',i2)
 1001 format(1x,32('*'),' xxcftr error ',32('*')//
     &       1x,'Increase string length: ',a,i5)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
C-----------------------------------------------------------------------
      END
