CX UNIX - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxeiam.for,v 1.1 2004/07/06 15:35:04 whitefor Exp $ Date $Date: 2004/07/06 15:35:04 $
CX
       subroutine xxeiam( esym  , xsym )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxeiam *********************
c
c  purpose:  Routine to return the atomic mass number for a given element
c            symbol esym.  
c
c  calling program: gapecf
c
c
c  subroutine:
c
c  input : (i*4)  esym   = the symbol of the element required (note that
c                          case is unimportant as the uppercase of the
c                          input value is always taken).
c  output: (i*4)  xsym   = the float value of the atomic mass of the element.
c                          If the symbol is not recognised then the value
c                          of xsym returned is zero.
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          xxcase     adas      converts a string to upper or lower case
c
c author:  Hugh Summers, University of Strathclyde
c          JA7.08
c          tel. 0141-548-4196
c
c date:    15/01/02
c
c update:
c
c-----------------------------------------------------------------------
       real*8    xsym
c-----------------------------------------------------------------------
       integer  i       , lenstr
c-----------------------------------------------------------------------
       character esym*(*)  , esym_mod*2
c-----------------------------------------------------------------------
       real*8    xsyma(94)
c-----------------------------------------------------------------------
       character esyma(94)*2
c-----------------------------------------------------------------------
       data esyma/'H ','D ','T ','HE','LI','BE','B ','C ','N ','O' ,
     &            'F ','NE','NA','MG','AL','SI','P ','S ','CL','AR',
     &            'K ','CA','SC','TI','V ','CR','MN','FE','CO','NI',
     &            'CU','ZN','GA','GE','AS','SE','BR','KR','RB','SR',
     &            'Y ','ZR','NB','MO','TC','RU','RH','PD','AG','CD',
     &            'IN','SN','SB','TE','I ','XE','CS','BA','LA','CE',
     &            'PR','ND','PM','SM','EU','GD','TB','DY','HO','ER',
     &            'TM','YB','LU','HF','TA','W ','RE','OS','IR','PT',
     &            'AU','HG','TL','PB','BI','PO','AT','RN','FR','RA',
     &            'AC','TH','PA','U '/
       data (xsyma(i),i=1,94)
     &            /1.00794d0,2.00d0,3.00d0,4.00260d0,6.941d0,
     &            9.01218d0,10.81d0,12.011d0,14.0067d0,
     &            15.9994d0,18.998403d0,20.179d0,22.98977d0,
     &            24.305d0,26.98154d0,28.0855d0,30.97376d0,
     &            32.06d0,35.453d0,39.948d0,39.0983d0,
     &            40.08d0,44.9559d0,47.88d0,50.9415d0,
     &            51.996d0,54.9380d0,55.847d0,58.9332d0,
     &            58.69d0,63.546d0,65.38d0,69.72d0,72.59d0,
     &            74.9216d0,78.96d0,79.904d0,83.80d0,
     &            85.4678d0,87.62d0,88.9059d0,91.22d0,
     &            92.9064d0,95.94d0,98.0d0,101.07d0,
     &            102.9055d0,106.42d0,107.8682d0,112.41d0,
     &            114.82d0,118.69d0,121.75d0,127.60d0,
     &            126.9045d0,131.30d0,132.9054d0,137.33d0,
     &            138.9055d0,140.12d0,140.9077d0,144.24d0,
     &            145.0d0,150.4d0,151.96d0,157.25d0,
     &            158.9254d0,162.50d0,164.9304d0,167.26d0,
     &            168.9342d0,173.04d0,174.97d0,178.49d0,
     &            180.9479d0,183.85d0,186.2d0,190.2d0,192.22d0,
     &            195.09d0,196.9665d0,200.59d0,204.37d0,207.2d0,
     &            208.9808d0,210.0d0,210.0d0,222.0d0,223.0d0,
     &            226.0254d0,227.0d0,232.0381d0,231.0359d0,238.029d0/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      lenstr = len(esym)
      
      esym_mod='  '
      if (lenstr.eq.1) then
          call xxcase(esym,esym_mod(1:1),'UC')
      elseif(lenstr.eq.2) then
          call xxcase(esym,esym_mod(1:2),'UC')
      endif
    
      do i=1,94
        if(esym_mod.eq.esyma(i)) then
            xsym=xsyma(i)
            return
        endif
      enddo
       xsym = 0.0

       return
      
      end       
       
