      function i4ldec(idec)
      implicit none
c---------------------------------------------------------------------
c
c  ****************** fortran77 function: i4ldec  ********************
c
c  purpose: returns l quantum number of an orbital given in the
c           decimal form
c
c
c  input :  (i*4)   idec   = decimal orbital number (eg.'10')
c
c  output:  (i*4)   i4ldec = l quantum number for orbital
c
c           (i*4)   lgrpa()= set of l-values for decimal orbitals
c
c  subroutines:  none
c
c  author:  h. p. summers, university of strathclyde
c           ja8.08
c           tel. 0141-553-4196
c
c  date:    05/05/04
c
c  update:
c
c-----------------------------------------------------------------------
      integer      idec
c-----------------------------------------------------------------------
      integer      i4ldec , lgrpa(61)
c-----------------------------------------------------------------------
      data lgrpa/   0,   0,   1,   0,   1,   2,   0,   1,   2,   3,   0,
     &              1,   2,   3,   4,   0,   1,   2,   3,   4,   5,   0,
     &              1,   2,   3,   4,   5,   6,   0,   1,   2,   3,   4,
     &              5,   6,   7,   0,   1,   2,   3,   4,   5,   6,   7,
     &              8,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
     &              0,   1,   2,   3,   4,   5/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
            i4ldec=lgrpa(idec)
c
      end
