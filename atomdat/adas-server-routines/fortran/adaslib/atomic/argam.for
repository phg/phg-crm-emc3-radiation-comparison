C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/argam.for,v 1.4 2004/07/06 11:16:31 whitefor Exp $ Date $Date: 2004/07/06 11:16:31 $
C
       function argam( l   , a   )                                       
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 function: argam   **********************
c
c  purpose:  calculates arggamma(l+1+i*a) where l is an integer not 
c            less than zero
c
c
c  subroutine:
c
c  input : (i*4)  l         = angula momentum >=0 
c  input : (r*8)  a         = real argument 
c                                           
c  output: (r*8)  argam     = arggamma(l+1+i*a)
c                               
c  routines:
c          none
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c  date:    06/06/02
c
c  update:
c
C VERSION: 1.1                          DATE: 04-07-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION.
C VERSION: 1.2                          DATE: 14-06-2000
C MODIFIED: Martin O'Mullane
C               - changed definition from real argam*8().
C
C VERSION: 1.3                          DATE: 19-12-01
C MODIFIED: Martin O'MULLANE
C               - Removed junk from > column 72.
C
C VERSION: 1.4                          DATE: 18-03-03
C MODIFIED: Hugh Summers
C               - Re-written and updated. 
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
       integer   l     , j0    , j     , j1
c-----------------------------------------------------------------------
       real*8    argam , a     , b     , c     , 
     &           d     , d0    , d1    , d2    , d3    , 
     &           u     , z
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
       b=dabs(a)                                                        
       b=250.0*b**0.25-a*a                                              
       j0=l+1                                                           
       c=j0                                                             
       d=c*c                                                            
       z=0.0                                                            
       if(d.lt.b)then                                                      
    1      b=dsqrt (b)                                                      
           j1=b                                                             
           do j=j0,j1                                                     
             d=j                                                              
             d=a/d                                                            
             d1=dabs(d)                                                       
             if(d1.lt.0.1d0) then                                                   
                 d1=d*d                                                           
                 d2=-35.0*d1+45.0                                                 
                 d2=-d1*d2+63.0                                                   
                 d2=-d1*d2+105.0                                                  
                 d1=d-d*d1*d2/315.0 
	     else                                              
                 d1=datan (d)
             endif                                                     
             z=z+d1                                                           
           enddo 
                                                               
           j0=j1+1 
       endif
                                                                
       d=j0                                                             
       d0=d*d                                                           
       u=a*a                                                            
       d1=1.0/(d0+u)                                                    
       d2=d1*d1                                                         
       d3=10.0*d0*d0-20.0*d0*u+2.0*u*u                                  
       d3=d3*d2-21.0*d0+7.0*u                                           
       d3=d3*d2+210.0                                                   
       d1=a*d3*d1/2520.0 
                                                      
       argam=-z+0.5*a*dlog(d0+u)+(d-0.5)*datan(a/d)-a-d1
                      
       return 
                                                                 
      end                                                               
