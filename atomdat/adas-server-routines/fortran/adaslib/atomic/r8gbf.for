CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8gbf.for,v 1.3 2007/04/11 13:01:50 allan Exp $ Date $Date: 2007/04/11 13:01:50 $
CX
      FUNCTION R8GBF( EN , U )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8GBF  ******************
C
C  PURPOSE: CALCULATES BOUND-FREE GAUNT FACTORS
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC  : (R*8)  R8GBF   =
C
C  INPUT : (R*8)  EN      =
C  INPUT : (R*8)  U       = N**2 / K**2 = N**2 * E / Z**2
C
C  PARAM : (R*8)  P1      = 2/3.
C  PARAM : (R*8)  P2      = 25/18.
C  PARAM : (R*8)  P3      = 4/3
C
C          (R*8)  X       =
C          (R*8)  T1      =
C          (R*8)  T2      =
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    04/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION : 1.2                          
C DATE    : 02-03-2005
C MODIFIED: Martin O'Mullane
C            - Change P2 from 25/18 to 28/18 as given by equation B3
C              in Burgess & Summers, MNRAS, vol 174, (1976), p345. 
C
C VERSION  : 1.3                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8GBF
C-----------------------------------------------------------------------
      REAL*8     P1                    , P2                   ,
     &           P3
      PARAMETER( P1 = 2.0D0 / 3.0D0    , P2 = 28.0D0 / 18.0D0 ,
     &           P3 = 4.0D0 / 3.0D0                             )
C-----------------------------------------------------------------------
      REAL*8     EN      , U
      REAL*8     X       , T1      , T2
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------

      X  = ( EN * ( U + 1.0D0 ) )**P1
      T1 = 0.1728D0 * ( U - 1.0D0 ) / X
      T2 = P2 * T1**2 + P3 * 0.0496D0 * ( U**2 + P3 * U + 1.0) / X**2

      R8GBF = 1.0D0 / ( 1.0D0 - P3 * T1 + T2 )**0.75D0

C-----------------------------------------------------------------------

      RETURN
      END
