C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8fdip.for,v 1.4 2007/04/23 13:33:40 allan Exp $ Date $Date: 2007/04/23 13:33:40 $
C
      REAL*8 FUNCTION R8FDIP(E1,L1,E2,L2)                               
      IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: R8FDIP ***********************
C
C PURPOSE: CALCULATES THE DIPOLE INTEGRAL I(KAPPA1,L1,KAPPA2,L2,1)
C
C NOTE: CREATED BY  ALAN BURGESS AS DEFINED IN PHIL. TRANS. ROY. SOC. 
C       A226,255,1970, WHERE E1=KAPPA1**2 AND E2=KAPPA2**2. APPLIES TO
C       POSITIVE ELECTRON ENERGIES, THAT IS THE FREE-FREE CASE.  
C       IT IS SUITABLE FOR USE IN EQUATIONS (8),(9),(10) OR (11) OF           
C       J. PHYS. B. 7,L364,1974.                                              
C
C CALLING PROGRAMS: GENERAL
C
C INPUT:  (R*8)  E1      = KAPPA1**2 WHERE KAPPA1 IS SCALED INITIAL
C                          ELECTRON WAVE NUMBER 
C INPUT:  (I*4)  L1      = ORBITAL ANGULAR OMENTUM OF INITIAL ELECTRON 
C INPUT:  (R*8)  E2      = KAPPA2**2 WHERE KAPPA2 IS SCALED INITIAL
C                          ELECTRON WAVE NUMBER 
C INPUT:  (I*4)  L2      = ORBITAL ANGULAR OMENTUM OF FINAL ELECTRON 
C
C OUTPUT: (R*8)  R8FDIP  = I(KAPPA1,L1,KAPPA2,L2,1) 
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8FDIP1    ADAS     SMALL ENRGY CHGE.APPROX. TO DIPOLE INTEG.
C          R8FDIP2    ADAS     SMALL ENRGY CHGE.APPROX. TO DIPOLE INTEG.
C          R8FMON1    ADAS     EVALUATES MONOPOLE INTEGRAL
C          ARGAM      ADAS     CALCULATES ARGGAMMA(L+1+I*A)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C               - FIRST FULLY COMMENTED RELEASE
C
C-----------------------------------------------------------------------
      IF(E1+E2-1.0D-40) 11,11,12                                        
   11 R8FDIP=0.0D0                                                      
      RETURN                                                            
   12 IF(E1-E2) 1,1,2                                                   
    1 EMIN=E1                                                           
      EMAX=E2                                                           
      GO TO 3                                                           
    2 EMIN=E2                                                           
      EMAX=E1                                                           
    3 T=EMIN/EMAX                                                       
      IF(T-0.02944D0) 4,4,5                                             
    4 R8FDIP=R8FDIP1(E1,L1,E2,L2)                                       
      GO TO 9                                                           
    5 IF(T-0.16667D0) 7,6,6                                             
    6 R8FDIP=R8FDIP2(E1,L1,E2,L2)                                       
      GO TO 9                                                           
    7 R8FDIP=R8FDIP1(E1,L1,E2,L2)                                       
       IF(R8FDIP*R8FDIP-1.0D-40) 6,6,8                                  
    8 RETURN                                                            
    9  IF(R8FDIP*R8FDIP-1.0D-40) 10,10,8                                
   10 WRITE(6,100)                                                      
      RETURN                                                            
  100 FORMAT(17H   R8FDIP FAILURE)                                      
      END                                                               
