CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8tcon.for,v 1.1 2004/07/06 14:42:50 whitefor Exp $ Date $Date: 2004/07/06 14:42:50 $
CX
      FUNCTION R8TCON( INTYP , OUTTYP , IZ1 , TIN )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8TCON ******************
C
C  PURPOSE: TO CONVERT A TEMPERATURE INTO SPECIFIED UNITS
C           (DOUBLE PRESCISION FUNCTION VERSION OF 'XXTCON')
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C             (R*8)  R8TCON  = FUNCTION NAME -
C                              OUTPUT TEMPERATURE (STATED UNITS)
C             (I*4)  INTYP   = 1 => 'TIN' UNITS: KELVIN
C                            = 2 => 'TIN' UNITS: eV
C                            = 3 => 'TIN' UNITS: REDUCED TEMP.
C             (I*4)  OUTTYP  = 1 => 'R8TCON' UNITS: KELVIN
C                            = 2 => 'R8TCON' UNITS: eV
C                            = 3 => 'R8TCON' UNITS: REDUCED TEMP.
C             (I*4)  IZ1     = RECOMBINING ION CHARGE (= Z+1).
C             (R*8)  TIN     = INPUT TEMPERATURE (STATED UNITS)
C
C             (R*8)  EV2KEL  = ELECTRON VOLTS TO KELVIN CONVERSION
C             (R*8)  KEL2EV  = KELVIN TO ELECTRON VOLTS CONVERSION
C
C             (R*8)  Z1P2    = 'IZ1'**2
C             (R*8)  TCONV() = TEMPERATURE CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            TEMPERATURE CONVERSION PARAMETERS:
C
C            INTYP = 1 ; TCONV(1) =>             KELVIN  -> OUTPUT UNITS
C            INTYP = 2 ; TCONV(2) =>                 eV  -> OUTPUT UNITS
C            INTYP = 3 ; TCONV(3) => REDUCED TEMPERATURE -> OUTPUT UNITS
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    04/01/91
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     EV2KEL              , KEL2EV
C-----------------------------------------------------------------------
      PARAMETER( EV2KEL=1.16054D+04  , KEL2EV=8.61668D-05 )
C-----------------------------------------------------------------------
      INTEGER    INTYP               , OUTTYP
      INTEGER    IZ1
C-----------------------------------------------------------------------
      REAL*8     R8TCON              , TIN
      REAL*8     Z1P2                , TCONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      Z1P2 = DBLE(IZ1*IZ1)
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE UNITS: KELVIN
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            TCONV(1) = 1.0
            TCONV(2) = EV2KEL
            TCONV(3) = Z1P2
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE UNITS: EV
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            TCONV(1) = KEL2EV
            TCONV(2) = 1.0
            TCONV(3) = Z1P2*KEL2EV
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE UNITS: REDUCED TEMPERATURE
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            TCONV(1) = 1.0/Z1P2
            TCONV(2) = TCONV(1)*EV2KEL
            TCONV(3) = 1.0
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' R8TCON ERROR: INVALID OUTPUT TEMPERATURE TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
      R8TCON = TCONV(INTYP)*TIN
C-----------------------------------------------------------------------
      RETURN
      END
