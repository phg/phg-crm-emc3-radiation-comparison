CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4idli.for,v 1.2 2007/04/11 13:01:49 allan Exp $ Date $Date: 2007/04/11 13:01:49 $
CX
      FUNCTION I4IDLI( NF , LI , LF )         
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  *************** FORTRAN77 INTEGER*4 FUNCTION: I4IDLI ****************
C
C  PURPOSE:  RETURNS THE INDEX NUMBER OF THE PREDICTED SPETRUM LINE
C            TABLES GIVEN THE ORBITAL QUANTUM NUMBER OF THE INTIAL STATE
C            AND THE PRINCIPAL AND ORBITAL QUANTUM NUMBERS OF THE FINAL
C            STATE.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  FUNC:   (I*4)   I4IDLI  = INDEX
C
C  INPUT:  (I*4)   NF      = PRINCIAPL QUANTUM NUMBER OF FINAL STATE.
C  INPUT:  (I*4)   LI      = ORBITAL QUANTUM NUMBER OF INITIAL STATE.
C  INPUT:  (I*4)   LF      = ORBITAL QUANTUM NUMBER OF FINAL STATE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    14/10/93
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    I4IDLI
C-----------------------------------------------------------------------
      INTEGER    NF      , LI      , LF
C-----------------------------------------------------------------------
C
C***********************************************************************
C CALCULATE INDEX.
C***********************************************************************
C
      IF (LI .EQ. NF) THEN
C
         I4IDLI = 2 * NF - 1
C
      ELSE IF ((LI - LF) .EQ. 1) THEN
C
         I4IDLI = 2 * LI
C
      ELSE IF ((LF - LI) .EQ. 1) THEN
C
         I4IDLI = 2 * LI + 1
C
      ELSE
C
         I4IDLI = 0
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
