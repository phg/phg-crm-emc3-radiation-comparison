CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxtcon.for,v 1.1 2004/07/06 15:39:50 whitefor Exp $ Date $Date: 2004/07/06 15:39:50 $
CX
      SUBROUTINE XXTCON( INTYP, OUTTYP, IZ1, ITVAL, TIN, TOUT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXTCON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF TEMPERATURES INTO SPECIFIED UNITS
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  INTYP   = 1 => 'TIN(array)' UNITS: KELVIN
C                            = 2 => 'TIN(array)' UNITS: eV
C                            = 3 => 'TIN(array)' UNITS: REDUCED TEMP.
C  INPUT :    (I*4)  OUTTYP  = 1 => 'TOUT(array)' UNITS: KELVIN
C                            = 2 => 'TOUT(array)' UNITS: eV
C                            = 3 => 'TOUT(array)' UNITS: REDUCED TEMP.
C  INPUT :    (I*4)  IZ1     = RECOMBINING ION CHARGE (= Z+1).
C  INPUT :    (I*4)  ITVAL   = NUMBER OF TEMPERATURES IN 'TIN(array)'
C  INPUT :    (R*8)  TIN()   = INPUT TEMPERATURES (STATED UNITS)
C  OUTPUT:    (R*8)  TOUT()  = OUTPUT TEMPERATURES (STATED UNITS)
C
C             (R*8)  EV2KEL  = ELECTRON VOLTS TO KELVIN CONVERSION
C             (R*8)  KEL2EV  = KELVIN TO ELECTRON VOLTS CONVERSION
C
C             (I*4)  I       = GENERAL USE
C
C             (R*8)  Z1P2    = 'IZ1'**2
C             (R*8)  TCONV() = TEMPERATURE CONVERSION PARAMETERS
C
C ROUTINES:  NONE
C
C NOTE:
C            TEMPERATURE CONVERSION PARAMETERS:
C
C            INTYP = 1 ; TCONV(1) =>             KELVIN  -> OUTPUT UNITS
C            INTYP = 2 ; TCONV(2) =>                 eV  -> OUTPUT UNITS
C            INTYP = 3 ; TCONV(3) => REDUCED TEMPERATURE -> OUTPUT UNITS
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    04/01/91
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     EV2KEL             , KEL2EV
C-----------------------------------------------------------------------
      PARAMETER( EV2KEL=1.16054D+04 , KEL2EV=8.61668D-05 )
C-----------------------------------------------------------------------
      INTEGER    INTYP              , OUTTYP             ,
     &           IZ1                , ITVAL
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     TIN(ITVAL)         , TOUT(ITVAL)
      REAL*8     Z1P2               , TCONV(3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      Z1P2 = DBLE(IZ1*IZ1)
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE UNITS: KELVIN
C-----------------------------------------------------------------------
C
         IF (OUTTYP.EQ.1) THEN
            TCONV(1) = 1.0
            TCONV(2) = EV2KEL
            TCONV(3) = Z1P2
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE UNITS: EV
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.2) THEN
            TCONV(1) = KEL2EV
            TCONV(2) = 1.0
            TCONV(3) = Z1P2*KEL2EV
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE UNITS: REDUCED TEMPERATURE
C-----------------------------------------------------------------------
C
         ELSEIF (OUTTYP.EQ.3) THEN
            TCONV(1) = 1.0/Z1P2
            TCONV(2) = TCONV(1)*EV2KEL
            TCONV(3) = 1.0
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' XXTCON ERROR: INVALID OUTPUT TEMPERATURE TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
         DO 1 I=1,ITVAL
            TOUT(I) = TCONV(INTYP) * TIN(I)
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
