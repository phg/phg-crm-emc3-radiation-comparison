CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8atab.for,v 1.1 2004/07/06 14:40:49 whitefor Exp $ Date $Date: 2004/07/06 14:40:49 $
CX
      FUNCTION R8ATAB ( IZ1 , NU , LU , NL , LL )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8ATAB ******************
C
C  PURPOSE: CALCULATES HYDRONIC L RESOLVED A-VALUES.
C
C           ON FIRST CALL WITH VALID INPUT PARAMETERS, FUNCTION SETS UP
C           A TABLE OF A-VALUES. ON SUBSEQUENT CALLS VALUES ARE THEN
C           LOOKED UP.
C
C           FUNCTION CHECKS TO SEE IF A-VALUE IS POSSIBLE AND DIPOLE
C           ALLOWED AND RETURNS ZERO IF NOT.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC:   (R*8)  R8ATAB   = HYDROGEN A-VALUE.
C
C  INPUT:  (I*4)  IZ1      = ION CHARGE.
C  INPUT:  (I*4)  NU       = UPPER VALUE OF N QUANTUM NUMBER.
C  INPUT:  (I*4)  LU       = L QUANTUM NUMBER FOR NU.
C  INPUT:  (I*4)  NL       = LOWER VALUE OF N QUANTUM NUMBER.
C  INPUT:  (I*4)  LL       = L QUANTUM NUMBER FOR NL.
C
C  PARAM:  (I*4)  MXN      = MAXIMUM VALUE OF N QUANTUM NUMBER.
C
C          (I*4)  M        = TABLE INDEX.
C          (I*4)  N1       = N QUANTUM NUMBER LOOP INDEX.
C          (I*4)  L1       = N QUANTUM NUMBER LOOP INDEX.
C          (I*4)  N2       = N QUANTUM NUMBER LOOP INDEX.
C          (I*4)  L2       = N QUANTUM NUMBER LOOP INDEX.
C          (I*4)  J0       = TABLE INDEX.
C
C          (R*8)  Z14      = REAL VALUE = IZ1**4
C
C          (L*4)  LFIRST   = .TRUE.  = FIRST TIME FUNCTION CALLED.
C                            .FLASE. = FUNCTION HAS BEEN CALLED BEFORE.
C
C          (I*4)  MA()     = INDEX TABLE FOR 'ATABLE'.
C                            DIMENSION: REFERENCED BY NU QUANTUM NO.
C          (R*8)  ATABLE() = TABLE OF A-VALUES.
C                            DIMENSION: REFERENCED BY NU, LU, NL, LL
C                                       QUANTUM NUMBERS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUPUT OF MESSAGES.
C          R8AH       ADAS      RETURNS HYDRONIC A-VALUE.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     29/09/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8ATAB
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      REAL*8     R8AH
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    IZ1     , NU      , LU      , NL      , LL
      INTEGER    M       , N1      , L1      , N2      , L2      ,
     &           J0
C-----------------------------------------------------------------------
      REAL*8     Z14
C-----------------------------------------------------------------------
      LOGICAL    LFIRST
C-----------------------------------------------------------------------
      INTEGER    MA(MXN)
C-----------------------------------------------------------------------
      REAL*8     ATABLE((MXN+1)*(MXN+2)*(2*MXN-3)/6-(MXN+1)*(MXN-2))
C-----------------------------------------------------------------------
      SAVE       LFIRST , MA       , ATABLE
C-----------------------------------------------------------------------
      DATA       LFIRST / .TRUE. /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INPUT VALUES VALID.
C-----------------------------------------------------------------------
C
      IF (NU .GT. (MXN + 1)) THEN
         WRITE(I4UNIT(-1),1000) MXN+1
         STOP
      ENDIF
C
      IF ((NU .LT. 2) .OR. (LU .LT. 0) .OR. (LU .GE. NU) .OR.
     &    (NL .LT. 1) .OR. (LL .LT .0) .OR. (LL .GE. NL) .OR.
     &    (NL .GE. NU)                                         ) THEN
         R8ATAB = 0.0D+00
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C IF FIRST TIME THEN FILL TABLE.
C-----------------------------------------------------------------------
C
      IF (LFIRST) THEN
C
         LFIRST = .FALSE.
         Z14 = DFLOAT( IZ1 )**4
C
         DO 1 N1 = 2 , MXN
C
            M = ( N1 * ( N1 + 1 ) * ( 2 * N1 - 5 ) ) / 6
     &          - N1 * ( N1 - 3 ) - 1
            MA(N1) = M
            ATABLE(M+1) = Z14 * R8AH( N1 , N1-1 , N1-1 , N1-2 )
C
            J0 = 1 + ( N1 - 1 ) * ( N1 - 2 ) + N1
            DO 2 N2 = 1 , N1-1
              ATABLE(M+J0-N2) = Z14 * R8AH( N1 , 0 , N2 , 1 )
    2       CONTINUE
C
            DO 3 L1 = 1 , N1-2
C
               L2 = L1 - 1
               J0 = ( N1 - L1 - 2 ) * ( N1 - L1 - 1 )
     &              + MAX0( 0 , N1-L1-1 ) + N1
               DO 4 N2 = L1 , N1-1
                  ATABLE(M+J0-N2) = Z14 * R8AH( N1 , L1 , N2 , L2 )
    4          CONTINUE
C
               L2 = L1 + 1
               J0 = 1 + ( N1 - L1 - 2 ) * ( N1 - L1 - 1 ) + N1
               DO 5 N2 = L1+2 , N1-1
                  ATABLE(M+J0-N2) = Z14 * R8AH( N1 , L1 , N2 , L2 )
    5          CONTINUE
C
    3       CONTINUE
C
    1    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C LOOK UP REQUESTED VALUE IN TABLE.
C-----------------------------------------------------------------------
C
      M = MA(NU)
C
      IF (NU .GT. MXN) THEN
         R8ATAB = 0.0D+00
      ELSE IF (IABS(LU - LL) .NE. 1) THEN
         R8ATAB = 0.0D+00
      ELSE IF ((NU - LU) .EQ. 1) THEN
         R8ATAB = ATABLE(M+1)
      ELSE IF (LU .EQ. 0) THEN
         J0 = 1 + ( NU - 1 ) * ( NU - 2 ) + NU
         R8ATAB = ATABLE(M+J0-NL)
      ELSE IF ((LU - LL) .EQ. 1) THEN
         J0 = ( NU - LU - 2 ) * ( NU - LU - 1 )
     &        + MAX0( 0 , NU-LU-1 ) + NU
         R8ATAB = ATABLE(M+J0-NL)
      ELSE
         J0 = 1 + (NU - LU - 2 ) * ( NU - LU - 1) + NU
         R8ATAB = ATABLE(M+J0-NL)
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' R8ATAB ERROR ', 32('*') //
     &        2X, 'VALUE OF N GREATER THAN ', I2, '.' /
     &        2X, 'INCREASE PARAMETER ''MXN'' IN FUNCTION R8ATAB.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
