CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8prov.for,v 1.2 2007/04/23 13:33:41 allan Exp $ Date $Date: 2007/04/23 13:33:41 $
CX
      FUNCTION R8PROV ( N1 , L1 , N2 , L2 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 FUNCTION: R8PROV **********************
C
C  PURPOSE:  APPROXIMATION TO HYDROGENIC OVERLAP INTEGRAL FOR CLASSICAL
C            BINARY ENCOUNTER SPIN CHANGE CROSS-SECTIONS FOR 
c            MAX0(N,N1) LARGE.
C
C  NOTE   : SEE OVLP FOR THE GENERAL CASE    
C
C  CALLING PROGRAM: GENERAL
C
C  FUNC  : (R*8)  R8PROV  = OVERLAP INTEGRAL
C
C  INPUT : (I*4)  N1      = FIRST N QUANTUM NUMBER.
C  INPUT : (I*4)  L1      = FIRST L QUANTUM NUMBER.
C  INPUT : (I*4)  N2      = SECOND N QUANTUM NUMBER.
C  INPUT : (I*4)  L2      = SECOND L QUANTUM NUMBER.
C
C  PARAM : (R*8)  P1      = 0.3
C
C          (I*4)  M       =
C
C          (R*8)  XN1     = REAL VALUE = N1.
C          (R*8)  XL1     = REAL VALUE = L1.
C          (R*8)  XN2     = REAL VALUE = N2.
C          (R*8)  XL2     = REAL VALUE = L2.
C          (R*8)  XM      = REAL VALUE = M.
C          (R*8)  BT      =
C          (R*8)  X1      =
C          (R*8)  X2      =
C          (R*8)  XK      =
C
C ROUTINES: NONE
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     04/10/93
C
C VERSION: 1.1                          DATE: 02-11-093
C MODIFIED: JONATHAN NASH
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 17-04-07
C MODIFIED: HUGH SUMMERS
C               - FIRST FULLY COMMENTED RELEASE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8PROV
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 0.3D0 )
C-----------------------------------------------------------------------
      INTEGER    N1      , L1      , N2      , L2
      INTEGER    M
C-----------------------------------------------------------------------
      REAL*8     XN1     , XL1     , XN2     , XL2     , XM      ,
     &           BT      , X1      , X2      , XK
C-----------------------------------------------------------------------
C
      M = MIN0( N2-1 , N1+1 )
C
      XN1 = DFLOAT( N1 )
      XL1 = DFLOAT( L1 )
      XN2 = DFLOAT( N2 )
      XL2 = DFLOAT( L2 )
      XM  = DFLOAT( M )
C
      BT = XN2**2 / ( XM + 1.0D0 )**2
      X1 = DMAX1( XL1-XM , 0.0D0 )
      X2 = DMAX1( XM-XL1 , 0.0D0 )
      XK = BT * ( XM + 1.0D0 )**2
     &     / ( 2.0D0 * XN2**2 * ( -XL1 + DABS( XM - XL1 )
     &     + ( 1.5D0 + XL1 ) * DLOG( ( 1.0D0 + XL1 ) / ( 1.0D0 + X1 ) )
     &     - ( 0.5D0 - XL1 ) * DLOG( 1.0D0 + X2 ) ) )
C
      IF (L2 .LE. M) THEN
         R8PROV = BT * ( 1.0D0 - P1 ) / XN2**2
     &            + P1 * XK / ( DABS( XL2 - XL1 ) + 1.0D0 )
      ELSE
         R8PROV = BT * (2.0D0 * XM + 1.0D0 )
     &            * DEXP( -2.0D0 * ( XL2 - XM ) )
     &            / ( ( 2.0D0 * XL2 + 1.0D0 ) * XN2**2 )
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
