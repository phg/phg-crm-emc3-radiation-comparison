C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/i4jgrp.for,v 1.3 2004/07/06 14:07:22 whitefor Exp $ Date $Date: 2004/07/06 14:07:22 $

      INTEGER FUNCTION I4JGRP(CHR)
C     
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: I4JGRP  ********************
C
C  PURPOSE: RETURNS DECIMAL FORM OF EISSNER SINGLE HEX CHARACTER
C           ORBITAL GIVEN IN THE EISSNER SINGLE HEXADECIMAL CHARACTER 
C           FORM
C
C  INPUT  (C*2)   CHR    = HEX. ORBITAL CHARACTER (EG. 'A')
C
C  OUTPUT  (I*4)  I4JGRP = DECIMAL ORBITAL CHARACTER (E.G. '10')
C
C  SUBROUTINES:  NONE
C
C  AUTHOR:  W.J. DICKSON, JET
C
C  DATE:    20/10/89
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE.
C VERSION: 1.2				DATE: 19-02-03
C MODIFIED: H.P. SUMMERS 
C		- EXTENDED RANGE
C VERSION: 1.3				DATE: 19-02-03
C MODIFIED: R. MARTIN			DATE: 24-03-03
C		- Fixed bugs in last version.
C
C-----------------------------------------------------------------------
      CHARACTER*1  CHR       , CHRA(61)
C-----------------------------------------------------------------------
      INTEGER      JGRPA(61)
	INTEGER 	 I
C-----------------------------------------------------------------------
      DATA CHRA /'1','2','3','4','5','6','7','8','9','A','B',
     &           'C','D','E','F','G','H','I','J','K','L','M',
     &           'N','O','P','Q','R','S','T','U','V','W','X',
     &           'Y','Z','a','b','c','d','e','f','g','h','i',
     &           'j','k','l','m','n','o','p','q','r','s','t',
     &           'u','v','w','x','y','z'/
      DATA JGRPA/ 1, 2, 3, 4, 5, 6, 7, 8, 9,10,
     &           11,12,13,14,15,16,17,18,19,20,
     &           21,22,23,24,25,26,27,28,29,30,
     &           31,32,33,34,35,36,37,38,39,40,
     &           41,42,43,44,45,46,47,48,49,50,
     &           51,52,53,54,55,56,57,58,59,60,
     &           61/
C-----------------------------------------------------------------------

C NEXT LINE ADDED TO PRECLUDE WARNINGS FROM F77
      I4JGRP=0

      DO 10 I=1,61
      IF(CHR.EQ.CHRA(I)) THEN
          I4JGRP=JGRPA(I)
          RETURN
      ENDIF
  10  CONTINUE
      RETURN
      END
