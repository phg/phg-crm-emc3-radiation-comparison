      subroutine  xxmkrc( ndcnct   ,
     &                    iz0      , iptnl    ,
     &                    ncnct    , icnctv
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxmkrc *********************
c
c  purpose:  to create the root connection vector for an element
c
c  calling program: various
c
c
c  subroutine:
c
c  input : (i*4)  ndcnct      = maximum number of elements in
c                               connection vector
c  input : (i*4)  iz0         = nuclear charge
c  input : (i*4)  iptnl       = root partition level (0 or 1)
c
c
c  output: (i*4)  ncnct       = number of elements in connection vector
c  output: (i*4)  cnctv()     = values of elements of  connection vector
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c  date:    26/05/06
c
c version: 1.1                          date: 26/05/2006
c modified: Hugh P Summers
c               - first edition.
c
c version: 1.2                          date: 17/01/2007
c modified: Hugh P Summers
c               - corrected metastable count for Ne+0.
c
c version  : 1.3
c date     : 15-03-2013
c modified : Martin O'Mullane
c              - extend to Ar.
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   idcnct        , iz0_max_res   , iz0_max
c-----------------------------------------------------------------------
      parameter (idcnct = 100 , iz0_max = 92  , iz0_max_res = 18 )
c-----------------------------------------------------------------------
      integer   i4unit
      integer   ndcnct        , iz0           , iptnl
      integer   ncnct
      integer   i
c-----------------------------------------------------------------------
      integer   icnctv(ndcnct)
      integer   ncncta(iz0_max_res)   , icnctva(iz0_max_res,idcnct)
c-----------------------------------------------------------------------
      data  ncncta(1),(icnctva(1,i),i=1,2)   / 2,1,1/
      data  ncncta(2),(icnctva(2,i),i=1,3)   / 3,2,1,1/
      data  ncncta(3),(icnctva(3,i),i=1,4)   / 4,1,2,1,1/
      data  ncncta(4),(icnctva(4,i),i=1,5)   / 5,2,1,2,1,1/
      data  ncncta(5),(icnctva(5,i),i=1,6)   / 6,2,2,1,2,1,1/
      data  ncncta(6),(icnctva(6,i),i=1,7)   / 7,4,2,2,1,2,1,1/
      data  ncncta(7),(icnctva(7,i),i=1,8)   / 8,3,4,2,2,1,2,1,1/
      data  ncncta(8),(icnctva(8,i),i=1,9)   / 9,4,3,4,2,2,1,2,1,1/
      data  ncncta(9),(icnctva(9,i),i=1,10)  /10,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(10),(icnctva(10,i),i=1,11)/11,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(11),(icnctva(11,i),i=1,12)
     &                 /11,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(12),(icnctva(12,i),i=1,13)
     &                       /12,2,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(13),(icnctva(13,i),i=1,14)
     &                       /13,2,2,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(14),(icnctva(14,i),i=1,15)
     &                       /14,4,2,2,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(15),(icnctva(15,i),i=1,16)
     &                       /15,3,4,2,2,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(16),(icnctva(16,i),i=1,17)
     &                       /16,4,3,4,2,2,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(17),(icnctva(17,i),i=1,18)
     &                       /17,2,4,3,4,2,2,1,2,2,4,3,4,2,2,1,2,1,1/
      data  ncncta(18),(icnctva(18,i),i=1,19)
     &                       /18,2,2,4,3,4,2,2,1,2,2,4,3,4,2,2,1,2,1,1/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c   check internal dimensions
c-----------------------------------------------------------------------
       if(idcnct.ne.ndcnct) then
         write(i4unit(-1),1000)'mismatch of internal dimensions: ',
     &                         'idcnct = ',idcnct,' .ne. ndcnct = ',
     &                          ndcnct
         write(i4unit(-1),1001)
         stop
       endif

       if(iz0.gt.iz0_max) then
         write(i4unit(-1),1000)'cannot form connection for: ',
     &                         'iz0 = ',iz0,' .gt. iz0_max = ',iz0_max
         write(i4unit(-1),1001)
         stop
       endif

c-----------------------------------------------------------------------

       if((iptnl.eq.0).and.(iz0.gt.iz0_max_res)) then
          write(i4unit(-1),1000)'cannot form resolved connection for: ',
     &                          'iz0 = ',iz0,' .gt. iz0_max_res = ',
     &                           iz0_max_res
          write(i4unit(-1),1001)
          stop

       elseif((iptnl.eq.0).and.(iz0.le.iz0_max_res)) then

          ncnct=ncncta(iz0)
          do i=1,iz0+1
            icnctv(i)=icnctva(iz0,i)
          enddo

       elseif(iptnl.eq.1) then
          ncnct=iz0+1
          do i=1,iz0+1
            icnctv(i)=1
          enddo
       else
         write(i4unit(-1),1000)'cannot form non-root connection with: ',
     &                         'iptnl = ',iptnl,' .gt. 1'
         write(i4unit(-1),1001)
         stop
       endif


       return

c-----------------------------------------------------------------------
 1000 format(1x,30('*'),'    xxmkrc error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 1001 format(/1x,30('*'),' program terminated ',29('*'))
c-----------------------------------------------------------------------
       end
