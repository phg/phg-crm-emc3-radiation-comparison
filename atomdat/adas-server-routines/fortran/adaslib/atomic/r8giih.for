CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/r8giih.for,v 1.1 2004/07/06 14:42:05 whitefor Exp $ Date $Date: 2004/07/06 14:42:05 $
CX
      FUNCTION R8GIIH ( VVE    , V      , N      , L      ,
     &                  L1     , LP     , ISP    , LT     ,
     &                  LT1    , IS     , IRES
     &                )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8GIIH ******************
C
C  PURPOSE:  CALCULATES BOUND-FREE G-FACTORS FOR ANGULARLY RESOLVED
C            LEVELS USES HYDROGENIC MATRIX ELEMENTS.
C
C            FOR COMPLETENESS, THE UNRESOLVED, BUNDLED N, GBF (BURGESS
C            AND SUMMERS ,1976) CAN ALSO BE OBTAINED.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNC  : (R*8)  R8GIIH  = BOUND-FREE GAUNT FACTOR
C
C  INPUT : (R*8)  VVE     = V**2*E WHERE E=(FREE ELECTRON ENERGY)/Z**2.
C                           UNITS: RYD
C  INPUT : (R*8)  V       = EFFECTIVE PRINCIPAL QUANTUM NUMBER OF BOUND
C                           ELECTRON.
C  INPUT : (I*4)  N       = PRINCIPAL QUANTUM NUMBER OF BOUND ELECTRON.
C  INPUT : (I*4)  L       = ORBITAL QUANTUM NUMBER OF BOUND ELECTRON.
C  INPUT : (I*4)  L1      = ORBITAL QUANTUM NUMBER OF FREE ELECTRON.
C  INPUT : (I*4)  LP      = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF PARENT STATE.
C  INPUT : (I*4)  ISP     = 2*SP+1 WHERE SP IS TOTAL SPIN OF PARENT
C                           STATE.
C  INPUT : (I*4)  LT      = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF BOUND SYSTEM.
C  INPUT : (I*4)  LT1     = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF FREE SYSTEM.
C  INPUT : (I*4)  IS      = 2*S+1 WHERE S IS TOTAL SPIN OF SYSTEM.
C  INPUT : (I*4)  IRES    = LEVEL OF RESOLUTION.
C                         = 1 :
C                         = 2 : ABOVE LT1 SUM.
C                         = 3 : ABOVE LT SUM.
C                         = 4 : ABOVE S SUM.
C                         = 5 : UNRESOLVED GBF.
C
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XL      = REAL VALUE = L.
C          (R*8)  XL1     = REAL VALUE = L1.
C          (R*8)  XLP     = REAL VALUE = LP.
C          (R*8)  XSP     = REAL VALUE = ISP.
C          (R*8)  XLT     = REAL VALUE = LT.
C          (R*8)  XLT1    = REAL VALUE = LT1.
C          (R*8)  XS      = REAL VALUE = IS.
C          (R*8)  W       =
C          (R*8)  ANG     =
C          (R*8)  CL1     =
C          (R*8)  E2      = -E WHERE E=(FREE ELECTRON ENERGY)/Z**2.
C                           UNITS: RYD
C          (R*8)  THETH   =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8WIG6     ADAS
C          R8RD2F     ADAS      RETURNS HYDROGENIC BOUND-FREE RADIAL
C                               INTEGRALS.
C          R8GBF      ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    04/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8GIIH
C-----------------------------------------------------------------------
      REAL*8     R8WIG6  , R8RD2F  , R8GBF
C-----------------------------------------------------------------------
      INTEGER    N       , L       , L1      , LP      , ISP     ,
     &           LT      , LT1     , IS      , IRES
C-----------------------------------------------------------------------
      REAL*8     VVE     , V
      REAL*8     XN      , XL      , XL1     , XLP     , XSP     ,
     &           XLT     , XLT1    , XS      , W       , ANG     ,
     &           CL1     , E2      , THETH
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      XN  = DFLOAT( N )
      XL  = DFLOAT( L )
      XL1 = DFLOAT( L1 )
      XLP = DFLOAT( LP )
      XSP = DFLOAT( ISP )
C
C-----------------------------------------------------------------------
C
      IF ((IRES .GE. 1) .AND. (IRES .LE. 4)) THEN
C
         XLT1 = DFLOAT( LT1 )
         XLT  = DFLOAT( LT )
         XS   = DFLOAT( IS )
C
         IF (IRES .EQ. 1) THEN
C
            W = R8WIG6( XL , XL1 , 1.0D0 , XLT1 , XLT , XLP )
            ANG = ( 2.0D0 * XLT + 1.0D0 ) * ( 2.0D0 * XLT1 + 1.0D0) *
     &            XS * DMAX1( XL , XL1 ) * W**2 /
     &            ( XSP * ( 2.0D0 * XLP + 1.0D0 ) )
            CL1 = XSP * ( 2.0D0 * XLP + 1.0D0 ) * ANG /
     &            ( ( 2.0D0 * XLT + 1.0D0 ) * XS )
C
         ELSE IF (IRES .EQ. 2) THEN
C
            ANG = ( 2.0D0 * XLT + 1.0D0 ) * XS * DMAX1( XL , XL1 ) /
     &            ( ( 2.0D0 * XL + 1.0D0 ) * XSP *
     &            ( 2.0D0 * XLP + 1.0D0 ) )
            CL1 = XSP * ( 2.0D0 * XLP + 1.0D0 ) * ANG /
     &            ( ( 2.0D0 * XLT + 1.0D0 ) * XS )
C
         ELSE IF (IRES .EQ. 3) THEN
C
            ANG = XS * DMAX1( XL , XL1 ) / XSP
            CL1 = XSP * ANG / ( ( 2.0D0 * XL + 1.0D0 ) * XS )
C
         ELSE
C
            ANG = 2.0D0 * DMAX1( XL , XL1 )
            CL1 = ANG / ( 2.0D0 * ( 2.0D0 * XL + 1.0D0 ) )
C
         ENDIF
C
         E2 = -VVE / V**2
         THETH = ( 1.0D0 + VVE ) * R8RD2F( N , L , L1 , E2 ) / V**4
C
         R8GIIH = 5.412659D-2 * ( 1.0D0+VVE )**3 * ANG * THETH / V
C
C-----------------------------------------------------------------------
C
      ELSE IF (IRES .EQ. 5) THEN
C
         R8GIIH = R8GBF( V , VVE )
C
C-----------------------------------------------------------------------
C
      ELSE
C
         R8GIIH = 0.0D0
C
      ENDIF
C
C
C-----------------------------------------------------------------------
C
      RETURN
      END
