CX  UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/atomic/xxrams.for,v 1.1 2004/07/06 15:38:32 whitefor Exp $ Date $Date: 2004/07/06 15:38:32 $
CX
      SUBROUTINE XXRAMS( VALUE )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXRAMS  *********************
C
C  PURPOSE: ADAS ROUTINE - SETS UP and interrogates a value for 
C           the receiver  mass.
C
C              VALUE ON INPUT   =>   VALUE ON OUTPUT
C
C                     ?                 returns value
C                  <BLANK>              returns value
C                     *                 returns 'UNSET'
C                  <OTHER>              sets value
C                    
C
C  CALLING PROGRAM: GENERAL USE - designed for use with R8CONST
C
C  SUBROUTINE:
C
C   I/O   : (C*7)  VALUE   = VALUE UNDER WHICH ADAS DATA IS STORED
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C NOTE:
C          TO CHECK CURRENT RECEIVER MASS CALL XXRAMS WITH
C          ? AS INPUT.
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    01/04/99
C 
C UPDATE: 
C
C-----------------------------------------------------------------------
      CHARACTER   VALUE*(*) , DEFVAL*7 , ADASVAL*7
C-----------------------------------------------------------------------
      SAVE        ADASVAL
C-----------------------------------------------------------------------
      
      DEFVAL = 'UNSET'

C-----------------------------------------------------------------------
C SET ADAS receiver mass
C-----------------------------------------------------------------------

      IF (VALUE(1:1).EQ.'?') THEN
	VALUE = ADASVAL
      ELSE IF (VALUE(1:1).EQ.'*') THEN
        VALUE   = DEFVAL
        ADASVAL = DEFVAL
      ELSE
        ADASVAL = VALUE
      ENDIF

C-----------------------------------------------------------------------

      RETURN
      END
