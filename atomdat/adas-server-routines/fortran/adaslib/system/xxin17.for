CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxin17.for,v 1.2 2007/04/11 13:01:54 allan Exp $ Date $Date: 2007/04/11 13:01:54 $
CX
      SUBROUTINE XXIN17( IUNIT  , ICLASS , DSNAME , LERROR ,
     &                   NDDEN  , NDTIN  , NDZ1V  ,
     &                   IPRTD  , ISYSD  ,
     &                   IDE    , ITE    , IZE    ,
     &                   DENSR  , TR     , ZIPT   ,
     &                   LSWIT  , EIA    ,
     &                   AIPT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXIN17 *********************
C
C  PURPOSE: TO OPEN AND ACQUIRE DATA FROM MASTER CONDENSED
C           COLLISIONAL-DIELECTRONIC FILES:
C
C           THE FOLLOWING FILES ARE ALLOWED:
C
C             1. RECOMBINATION COEFFICIENTS
C             2. IONISATION    COEFFICIENTS
C             3. CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C             4. METASTABLE CROSS-COUPLING COEFFICIENTS
C             5. PARENT METASTABLE CROSS-COUPLING COEFFICIENTS
C             6. RECOMBINATION-BREMSSTRAHLUNG  POWER COEFFICIENTS
C             7. CHARGE-EXCHANGE RECOMBINATION POWER COEFFICIENTS
C
C             (NOTE: SPECIFIC AND TOTAL LOW LINE POWER COEFFICIENTS
C                    SHOULD BE READ USING 'XXIN80'.
C                    IF  ONLY STANDARD FILES ARE TO BE READ BY  THE
C                    PROGRAM USE 'XXINST'.)
C
C  CALLING PROGRAM: GENERAL USE
C
C  DATA:
C           THE  SOURCE  DATA IS CONTAINED AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.QCD<YR>.DATA
C             5. JETUID.XCD<YR>.DATA
C             6. JETUID.PRB<YR>.DATA
C             7. JETUID.PRC<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C           IF <YR> IS BLANK THEN THE CURRENT RECOMMENDED DATA SETS ARE
C           USED
C
C           THE MEMBERS OF THE PARTITIONED DATA SETS ARE EITHER:
C           1)  <SE><I><J>  FOR PARTIAL MASTER CONDENSED FILES, OR
C           2)  <SE>        FOR STANDARD MASTER CONDENSED FILES
C
C           WHERE: <SE> IS THE ONE OR TWO LETTER ION SEQUENCE CODE.
C                  <I>  IS A  SINGLE  INTEGER  REPRESENTING  THE  PARENT
C                       INDEX OR METASTABLE INDEX DEPENDING ON THE DATA
C                       SET CLASS AND PRODUCTION BY BUNDLE-NS OR
C                       LOW-LEVEL+PROJECTION MODELS
C                  <J>  IS A SINGLE INTEGER REPRESENTING THE SPIN SYSTEM
C                       INDEX, METASTABLE INDEX OR PARENT INDEX
C                       DEPENDING ON THE DATA SET CLASS AND PRODUCTION
C                       BY BUNDLE-NS OR LOW-LEVEL+PROJECTION MODELS
C
C                  E.G. PARTIAL  FILES: 'C12' OR 'HE21'
C                       STANDARD FILES: 'C'   OR 'HE'
C
C           THE  'PARTIAL'  AND  'STANDARD'  MASTER CONDENSED FILES ARE
C           IDENTICAL IN FORM,   EXCEPT  THAT  THREE  ADDITIONAL  LINES
C           ARE INCLUDED  AT  THE  BEGINNING OF  THE  'PARTIAL'  MASTER
C           FILES.   THE  FIRST  OF THESE LINES CONTAINS  A ROW OF  '='
C           SIGNS,  THE SECOND A PARENT/SPIN (OR EQUIVALENTS) PARAMETER
C           LIST,   AND  THE
C           THIRD A ROW OF "-" SIGNS. THIS DIFFERENCE IS USED TO IDENT-
C           IFY WHICH FILE TYPE IS BEING READ.
C
C           THE CHARACTER STRING SEPARATING THE INPUT DATA FOR EACH
C           VALUE OF Z1 IN THE FILE WILL GIVE:
C
C           PARTIAL & STANDARD: THE Z1 VALUE (Z1=) AND DATE (DATE:).
C                (OLDER DATA SETS MAY HAVE 'Z =' INSTEAD OF 'Z1=' HERE)
C           PARTIAL FILES ONLY: THE PARENT (IPRT=) & SPIN SYSTEM (ISYS=)
C                OR EQUIVALENTS (IGRD=) & (IGRD=, JGRD= AND JPRT=)
C                AS FOLLOW:-
C
C               ICLASS     INDI     INDJ
C               ------     ----     ----
C                 1        IPRT     IGRD (OR ISYS)
C                 2        IPRT     IGRD (OR ISYS)
C                 3        IPRT     IGRD (OR ISYS)
C                 4        IPRT     IGRD (OR ISYS)
C                 5        IPRT     IGRD (OR ISYS)
C                 6        IGRD     JGRD
C                 7        IPRT     JPRT
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT DATA SET ALLOCATED
C  INPUT : (I*4)  ICLASS  = UNIT TO WHICH INPUT DATA SET ALLOCATED
C  INPUT : (C*(*))DSNAME  = INPUT MASTER CONDENSED FILE DATA SET NAME
C  OUTPUT: (L*4)  LERROR  = .TRUE.  => ERROR DETECTED IN READING FILE
C                         = .FALSE. => NO ERROR DETECTED IN FILE
C
C  INPUT : (I*4)  NDDEN   = MAX. NUMBER OF REDUCED DENSITIES ALLOWED IN
C                           MASTER CONDENSED FILE FOR A GIVEN SEQUENCE
C  INPUT : (I*4)  NDTIN   = MAX. NO. OF REDUCED TEMPERATURES ALLOWED IN
C                           MASTER CONDENSED FILE FOR A GIVEN SEQUENCE
C  INPUT : (I*4)  NDZ1V   = MAX. NUMBER OF CHARGE STATES ALLOWED IN
C                           MASTER CONDENSED FILE FOR A GIVEN SEQUENCE
C
C  INPUT : (I*4)  IPRTD   = INPUT PARTIAL MASTER CONDENSED FILE:
C                           PARENT INDEX SPECIFIED IN DATA SET NAME.
C  INPUT : (I*4)  ISYSD   = INPUT PARTIAL MASTER CONDENSED FILE: SPIN-
C                           SYSTEM INDEX SPECIFIED IN DATA SET NAME.
C
C  OUTPUT: (I*4)  IDE     = NUMBER OF REDUCED DENSITIES READ FROM INPUT
C                           MASTER CONDENSED FOR A GIVEN SEQUENCE
C  OUTPUT: (I*4)  ITE     = NO. OF REDUCED TEMPERATURES READ FROM INPUT
C                           MASTER CONDENSED FOR A GIVEN SEQUENCE
C  OUTPUT: (I*4)  IZE     = NO. OF CHARGE STATES GIVEN IN THE INPUT
C                           MASTER CONDENSED FOR A GIVEN SEQUENCE
C
C  OUTPUT: (R*8)  DENSR() = SET OF 'IDE' INPUT REDUCED DENSITIES (CM-3/
C                           Z1**7) READ FROM CONDENSED MASTER FILE.
C  OUTPUT: (R*8)  TR()    = SET OF 'ITE'  INPUT  REDUCED  TEMPERATURES
C                           (K/Z1**2) READ FROM CONDENSED MASTER FILE.
C  OUTPUT: (R*8)  ZIPT()  = SET OF 'IZE' INPUT CHARGE STATES READ FROM
C                           CONDENSED MASTER FILE.
C                           (CHARGE STATE = RECOMBINING ION CHARGE)
C
C  OUTPUT: (L*4)  LSWIT   = .TRUE.  => IONISATION POTENTIALS
C                                      INCLUDED IN INPUT MASTER FILE.
C                           .FALSE. => IONISATION POTENTIALS
C                                      NOT INCLUDED IN INPUT MASTER FILE
C  OUTPUT: (R*8)  EIA()   = IONISATION POTENTIALS: ()=ION CHARGE
C                           UNITS: WAVE NUMBERS (CM-1)
C                           (= 0.0 IF NOT SET)
C
C  OUTPUT: (R*8)  AIPT(,,)= CONDENSED MASTER FILE DATA. COLL-DIEL COEFF.
C                           1ST DIMENSION: REDUCED DENSITY ('DENSR()')
C                           2ND DIMENSION: REDUCED TEMPERATURE ('TR()')
C                           3RD DIMENSION: CHARGE STATE ('ZIPT()')
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IPRT    = INPUT PARTIAL MASTER CONDENSED FILE:
C                           PARENT INDEX READ FROM INPUT FILE.
C          (I*4)  ISYS    = INPUT PARTIAL MASTER CONDENSED FILE:
C                           SPIN-SYSTEM INDEX READ FROM INPUT FILE.
C          (I*4)  IPOT    = NUMBER OF IONISATION POTENTIAL VALUES
C                           PRESENT IN THE INPUT FILE.
C          (I*4)  IZ1     = CHARGE STATE READ FROM THE LINE PRECEEDING
C                           AN INPUT BLOCK FROM THE FILE.
C                           (= RECOMBINING ION CHARGE)
C          (I*4)  IBGN    = FIRST BYTE OF INTEREST IN CHARACTER 'STRING'
C          (I*4)  IEND    = LAST  BYTE OF INTEREST IN CHARACTER 'STRING'
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR ION-CHARGE VALUES
C          (I*4)  I       = GENERAL USE
C
C          (L*4)  LPART   = .TRUE.  => REQUESTED INPUT FILE: PARTIAL
C                         = .FALSE. => REQUESTED INPUT FILE: STANDARD
C
C          (C*5)  CPOT    = 'IPOT'
C          (C*5)  CHINDI  = 'IPRT= ' OR 'IGRD= ' DEPENDING ON ICLASS
C          (C*5)  CHINDJ  = 'IGRD= ', 'JGRD= ' OR 'JPRT= ' DEPENDING
C                            ON ICLASS
C          (C*80) STRING  = STRING INTO WHICH 1ST LINE OF INPUT FILE IS
C                           READ TO ENABLE ITS FORMAT TO BE ESTABLISHED.
C
C NOTE:
C          STREAM HANDLING:
C             STREAM 'IUNIT' IS USED FOR READING CONDENSED MASTER FILES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXREIA     ADAS      READ IN UNKNOWN NUMBER OF 'EIA' VALUES
C                               IF PRESENT.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    22/08/90
C
C UPDATE:  05/03/91 - PE BRIDEN - ADAS91: REMOVED OPENING OF DATA SET
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  11/08/93 - HP SUMMERS - CHANGED TO ACCEPT EXTRA DATA CLASSES
C                                  AND USE OF IGRD, JGRD, IPRT, JPRT AS
C                                  ALTERNATIVES TO IPRT AND ISYS.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IUNIT       , NDDEN     , NDTIN     , NDZ1V    ,
     &           IPRTD       , ICLASS    ,
     &           ISYSD       ,
     &                         IDE       , ITE       , IZE
      INTEGER    IPRT        , ISYS      ,
     &           IPOT        , IZ1       , IBGN      , IEND     ,
     &           ID          , IT        , IZ        , I
C-----------------------------------------------------------------------
      CHARACTER  CPOT*5      , CHINDI*5  , CHINDJ*5  ,
     &           DSNAME*(*)  , STRING*80
C-----------------------------------------------------------------------
      LOGICAL    LERROR      , LSWIT       , LPART
C-----------------------------------------------------------------------
      REAL*8     DENSR(NDDEN), TR(NDTIN)   , ZIPT(NDZ1V)
      REAL*8     EIA(250)
      REAL*8     AIPT(NDDEN,NDTIN,NDZ1V)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LERROR=.FALSE.
      LSWIT=.FALSE.
C
C-----------------------------------------------------------------------
C SET CHARACTER STRINGS FOR PARTICULAR TYPE OF DATA SET
C-----------------------------------------------------------------------
C
      IF(ICLASS .GE. 1 .AND. ICLASS .LE. 5) THEN
         CHINDI = 'IPRT= '
         CHINDJ = 'IGRD= '
      ELSEIF(ICLASS .EQ. 6) THEN
         CHINDI = 'IGRD= '
         CHINDJ = 'JGRD= '
      ELSEIF(ICLASS .EQ. 7) THEN
         CHINDI = 'IPRT= '
         CHINDJ = 'JPRT= '
      ELSE
         WRITE(I4UNIT(-1),2006)  ICLASS
         LERROR = .TRUE.
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C READ IN FIRST LINE OF FILE TO ESTABLISH ITS FORMAT: PARTIAL/STANDARD
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) STRING
C-----------------------------------------------------------------------
         IF (STRING(1:1).EQ.'=') THEN
C
C-----------------------------------------------------------------------
C PARTIAL MASTER CONDENSED FILE:
C-----------------------------------------------------------------------
C
            LPART=.TRUE.
C
C CHECK PARENT/SPIN SYSTEM HEADINGS AGREE WITH FILE NAME
C (ALLOW ALTERNATIVES TO SPIN SYSTEM)
C
            READ(IUNIT,1000) STRING
            IBGN=INDEX(STRING,CHINDI)+6
            READ(STRING(IBGN:IBGN),1001) IPRT
            IBGN=INDEX(STRING,CHINDJ)+6
            IF(IBGN.EQ.0.AND.ICLASS.LE.5)THEN
                IBGN=INDEX(STRING,'ISYS= ')+6
            ENDIF
            READ(STRING(IBGN:IBGN),1001) ISYS
               IF ( (IPRT.NE.IPRTD) .OR. (ISYS.NE.ISYSD) ) THEN
                  WRITE(I4UNIT(-1),2000) DSNAME ,
     &            'PARENT/SPIN SYSTEM DISCREPANCY BETWEEN:'
                  WRITE(I4UNIT(-1),2002) IPRTD , ISYSD , IPRT , ISYS
                  WRITE(I4UNIT(-1),2001)
                  LERROR=.TRUE.
                  RETURN
               ENDIF
            READ(IUNIT,1002)
            READ(IUNIT,1003) IZE , ITE , IDE , CPOT
C-----------------------------------------------------------------------
         ELSE
C
C-----------------------------------------------------------------------
C STANDARD MASTER CONDENSED FILE:
C-----------------------------------------------------------------------
C
            LPART=.FALSE.
            READ(STRING(1:20),1003) IZE , ITE , IDE , CPOT
         ENDIF
C-----------------------------------------------------------------------
      IF(IZE.GT.NDZ1V) STOP ' XXIN17 ERROR: IZE > NDZ1V. INCREASE NDZ1V'
      IF(ITE.GT.NDTIN) STOP ' XXIN17 ERROR: ITE > NDTIN. INCREASE NDTIN'
      IF(IDE.GT.NDDEN) STOP ' XXIN17 ERROR: IDE > NDDEN. INCREASE NDDEN'
C-----------------------------------------------------------------------
         IF (CPOT.NE.' ') THEN
            READ(CPOT,1003) IPOT
            IF (IPOT.GT.0) LSWIT=.TRUE.
         ENDIF
C-----------------------------------------------------------------------
      READ(IUNIT,1004) (ZIPT(IZ), IZ=1,IZE)
      READ(IUNIT,1002)
C
C-----------------------------------------------------------------------
C  READ IN IONISATION POTENTIALS IF PRESENT (UNITS: CM**3/SEC).
C  OTHERWISE CHECK THEY ARE NOT PRESENT - IF PRESENT READ THEM.
C  MISSING 'EIA()' VALUES ARE SET TO ZERO.
C-----------------------------------------------------------------------
C
         IF (LSWIT) THEN
            READ(IUNIT,1005) (EIA(I),I=1,IPOT)
            READ(IUNIT,1002)
               DO 2 I=(IPOT+1),50
                  EIA(I)=0.0D0
    2          CONTINUE
         ELSE
            CALL XXREIA( IUNIT , LSWIT , EIA )
         ENDIF
C-----------------------------------------------------------------------
      READ(IUNIT,1006) (DENSR(ID), ID=1,IDE)
      READ(IUNIT,1006) (TR(IT),     IT=1,ITE)
C
C***********************************************************************
C READ IN COEFFICIENTS FOR EACH VALUE OF Z1 LISTED IN THE HEADINGS
C***********************************************************************
C
         DO 3 IZ=1,IZE
C
C-----------------------------------------------------------------------
C CHECK Z1 FOR CURRENT DATA BLOCK IS THAT EXPECTED FROM THE HEADING DATA
C-----------------------------------------------------------------------
C
            READ(IUNIT,1000) STRING
            IBGN=INDEX(STRING,'Z1=')
            IF (IBGN.EQ.0) IBGN=INDEX(STRING,'Z =')
            IBGN=IBGN+3
            IEND=IBGN+1
            READ(STRING(IBGN:IEND),1007) IZ1
               IF (NINT(ZIPT(IZ)).NE.IZ1) THEN
                  WRITE(I4UNIT(-1),2000) DSNAME ,
     &            'Z1 BLOCK HEADING DISCREPANCY FOR:'
                  WRITE(I4UNIT(-1),2003) NINT(ZIPT(IZ)) , IZ1
                  WRITE(I4UNIT(-1),2001)
                  LERROR=.TRUE.
                  RETURN
               ENDIF
C
C-----------------------------------------------------------------------
C PARTIAL FILE: CHECK PARENT/SPIN SYSTEM IS CONSISTENT WITH HEADING DATA
C               (ALLOW ALTERNATIVES TO SPIN SYSTEM)
C-----------------------------------------------------------------------
C
               IF (LPART) THEN
                  IBGN=INDEX(STRING,CHINDI)+6
                  READ(STRING(IBGN:IBGN),1001) IPRT
                  IBGN=INDEX(STRING,CHINDJ)+6
                  IF(IBGN.EQ.0.AND. ICLASS.LE.5)THEN
                      IBGN=INDEX(STRING,'ISYS= ')+6
                  ENDIF
                  READ(STRING(IBGN:IBGN),1001) ISYS
                     IF ( (IPRT.NE.IPRTD) .OR. (ISYS.NE.ISYSD) ) THEN
                        WRITE(I4UNIT(-1),2000) DSNAME ,
     &                             'PARENT/SPIN SYSTEM DISCREPANCY FOR:'
                        WRITE(I4UNIT(-1),2004) IZ1,IPRTD,ISYSD,IPRT,ISYS
                        WRITE(I4UNIT(-1),2001)
                        LERROR=.TRUE.
                        RETURN
                     ENDIF
               ENDIF
C
C-----------------------------------------------------------------------
C READ IN COEFFICIENTS
C-----------------------------------------------------------------------
C
               DO 4 IT=1,ITE
                  READ(IUNIT,1006) (AIPT(ID,IT,IZ),ID=1,IDE)
    4          CONTINUE
C-----------------------------------------------------------------------
    3    CONTINUE
C***********************************************************************
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(A80)
 1001 FORMAT(I1)
 1002 FORMAT()
 1003 FORMAT(3I5,A5)
 1004 FORMAT(8F10.5)
 1005 FORMAT(6F12.1)
 1006 FORMAT(8E10.2)
 1007 FORMAT(I2)
C-----------------------------------------------------------------------
 2000 FORMAT(/1X,32('*'),' XXIN17 ERROR ',32('*')//
     &        1X,'INPUT MASTER CONDENSED FILE: ',A/
     &        1X,A)
 2001 FORMAT(/1X,28('*'),' END OF ERROR MESSAGE ',28('*'))
 2002 FORMAT( 1X,'DATA SET NAME:  PARENT = ',I1,4X,'SPIN SYSTEM = ',I1/
     &        1X,'FILE HEADING :  PARENT = ',I1,4X,'SPIN SYSTEM = ',I1)
 2003 FORMAT( 1X,'CHARGE STATE (Z1) EXPECTED = ',I2/
     &        1X,'CHARGE STATE (Z1) READ     = ',I2)
 2004 FORMAT( 1X,'CHARGE STATE (Z1) = ',I2/
     &        1X,'EXPECTED:   PARENT = ',I1,4X,'SPIN SYSTEM = ',I1/
     &        1X,'READ    :   PARENT = ',I1,4X,'SPIN SYSTEM = ',I1)
 2006 FORMAT( 1X,'ICLASS = ',I2,2X,'IS INCONSISTENT WITH SUBROUTINE XXIN
     &17'    )
 3000 FORMAT(/1X,32('*'),' XXIN17 ERROR ',32('*')//
     &        1X,'INPUT MASTER CONDENSED FILE: ',A/
     &        1X,A/
     &        )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
