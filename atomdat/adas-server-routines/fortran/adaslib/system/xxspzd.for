CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxspzd.for,v 1.1 2004/07/06 15:39:36 whitefor Exp $ Date $Date: 2004/07/06 15:39:36 $
CX
      SUBROUTINE XXSPZD( USRGRP , USRTYP , USREXT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXSPZD *********************
C
C  PURPOSE: ADAS ROUTINE - SETS UP THE DEFAULT USEGRP, USRTYP AND USREXT
C           WHICH IDENTIFY THE FILENAME AND EXTENSION TO BE READ IN
C           SUBROUTINE SPZD.  IT WORKS IN THE SAME MANNER AS XXUID WHICH
C           WHICH ALLOWS THE DEFAULT USER SPACE TO BE SET
C
C
C           USRGRP: VALUE ON INPUT  =>  USRGRP: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA GROUPNAME
C                     *                 DEFAULT ADAS DATA GROUPNAME
C                  <BLANK>              *** USRGRP VALUE NOT CHANGED ***
C                  <OTHER>              *** USRGRP VALUE NOT CHANGED ***
C
C
C           USRTYP: VALUE ON INPUT  =>  USRTYP: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA TYPENAME
C                     *                 DEFAULT ADAS DATA TYPENAME
C                  <BLANK>              *** USRTYP VALUE NOT CHANGED ***
C                  <OTHER>              *** USRTYP VALUE NOT CHANGED ***
C
C
C           USREXT: VALUE ON INPUT  =>  USREXT: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA MEMBER EXTENS.
C                     *                 DEFAULT ADAS DATA MEMBER EXTENS
C                  <BLANK>              *** USREXT VALUE NOT CHANGED ***
C                  <OTHER>              *** USREXT VALUE NOT CHANGED ***
C
C         ? => QUERIES CURRENT ADAS DATA USRGRP, USRTYP OR USREXT
C              SETTING.
C         * => SETS ADAS DATA USEGRP, USRTYP OR USREXT SETTING
C              TO DEFAULT VALUE.
C   <BLANK> => SETS ADAS DATA USRGRP, USRTYP OR USREXT SETTING
C              TO DEFAULT VALUE.
C   <OTHER> => SETS ADAS DATA USRGRP, USRTYP OR USREXT SETTING
C              TO INPUT   VALUE.
C
C  CALLING PROGRAM: SPZD AND MAIN PROGRAMS USING SPZD
C
C  SUBROUTINE:
C
C  I/O   : (C*8)  USRGRP   = USRGRP UNDER WHICH ADAS DATA IS STORED
C                            (IF BLANK DEFAULTS TO DEFGRP)
C
CA I/O   : (C*80) USRTYP   = UNDER UNIX THIS IS OPTIONAL SUB-DIRECTORY
C                            (IF BLANK DEFAULTS TO DEFTYP)
C
C  I/O   : (C*3)  USREXT   = USREXT UNDER WHICH ADAS DATA IS STORED
C                            (IF BLANK DEFAULTS TO DEFEXT)
C
C          (C*8)  DEFGRP   = PARAMETER = DEFAULT USER GROUP FOR ADAS
C                                        DATA SOURCE
C
CA	   (C*80) DEFTYP   = PARAMETER = DEFAULT SUB-DIRECTORY NAME
C                                        WHICH IS BLANK.
C
C          (C*3)  DEFEXT   = PARAMETER = DEFAULT USER EXTENSION FOR ADAS
C                                        DATA SOURCE
C
C          (C*8)  ADASGR   = CURRENT ADAS DATA SOURCE GROUP
C
CA	   (C*80) ADASTY   = CURRENT SUBDIRECTORY NAME
C
C          (C*3)  ADASEX   = CURRENT ADAS DATA SOURCE EXTENSION
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C NOTE:
C          TO CHECK CURRENT ADAS SOURCE USRGRP, USRTYP AND USREXT
C          CALL XXSPZD WITH ?`S AS INPUTS.
C
C AUTHOR:  HUGH P. SUMMERS, JET
C          K1/1/67
C          JET EXT. 4941
C
C DATE:     2/09/93
C
C UPDATE:  21/11/94 - L. JALOTA - ALTERED CHARACTER STRING LENGTHS FOR UNIX
C
C-----------------------------------------------------------------------
      CHARACTER  DEFGRP*8             , DEFTYP*80     , DEFEXT*3
C-----------------------------------------------------------------------
      PARAMETER  (DEFGRP = 'ionelec ' , DEFTYP = ' ' ,
     &            DEFEXT = 'pzd')
C-----------------------------------------------------------------------
      CHARACTER  USRGRP*8             , ADASGR*8      ,
     &           USRTYP*80            , ADASTY*80     ,
     &           USREXT*3             , ADASEX*3
C-----------------------------------------------------------------------
      SAVE       ADASGR , ADASTY , ADASEX
C-----------------------------------------------------------------------
      DATA       ADASGR , ADASTY , ADASEX / DEFGRP , DEFTYP , DEFEXT /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE GROUPNAME
C-----------------------------------------------------------------------
C
      IF (USRGRP(1:1).EQ.'?') THEN
        USRGRP = ADASGR
      ELSE IF (USRGRP(1:1).EQ.'*') THEN
        USRGRP = DEFGRP
        ADASGR = DEFGRP
      ELSE IF (USRGRP(1:1).EQ.' ') THEN
        ADASGR = DEFGRP
      ELSE
        ADASGR = USRGRP
      ENDIF
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE TYPENAME
C-----------------------------------------------------------------------
C
      IF (USRTYP(1:1).EQ.'?') THEN
        USRTYP = ADASTY
      ELSE IF (USRTYP(1:1).EQ.'*') THEN
        USRTYP = DEFTYP
        ADASTY = DEFTYP
      ELSE IF (USRTYP(1:1).EQ.' ') THEN
        ADASTY = DEFTYP
      ELSE
        ADASTY = USRTYP
      ENDIF
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE EXTENSION
C-----------------------------------------------------------------------
C
      IF (USREXT(1:1).EQ.'?') THEN
        USREXT = ADASEX
      ELSE IF (USREXT(1:1).EQ.'*') THEN
        USREXT = DEFEXT
        ADASEX = DEFEXT
      ELSE IF (USREXT(1:1).EQ.' ') THEN
        ADASEX = DEFEXT
      ELSE
        ADASEX = USREXT
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
