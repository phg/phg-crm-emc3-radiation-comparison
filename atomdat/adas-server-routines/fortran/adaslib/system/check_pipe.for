CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/check_pipe.for,v 1.1 2004/07/06 12:04:43 whitefor Exp $ Date $Date: 2004/07/06 12:04:43 $
CX
       SUBROUTINE CHECK_PIPE
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: CHECK_PIPE **********************
C
C  VERSION:  1.0
C
C  PURPOSE:  DIAGNOSTIC CHECK ON THE STATE OF THE FORTRAN-IDL PIPE
C
C  NOTES:  THE EQUIVALENT IDL CHECK_PIPE PROCEDURE MUST BE PUT IN
C          THE IDL CODE TO COMMUNICATE WITH THIS FORTRAN SUBROUTINE.
C          BOTH ROUTINES WRITE A STRING, READ A STRING AND THEN CHECK
C          WHETHER THAT STRING IS AS EXPECTED. A MESSAGE IS THEN
C          OUTPUT INDICATING WHETHER THE PIPE WAS EMPTY BEFORE THE CALLS
C          WERE MADE, SO TELLING THE USER WHETHER PIPE COMMUNICATIONS
C          BETWEEN THE IDL AND FORTRAN HAD BEEN SUCCESSFUL UP TO THAT
C          POINT.
C
C          RATHER THAN GIVING THE 'ERROR' MESSAGE, THE FORTRAN MAY CRASH
C          INDICATING THAT THE IDL HAS WRITTEN TOO MUCH DATA TO THE PIPE
C          BEFORE THIS CALL AND THAT THE DATA WAS NOT A STRING.
C
C          IF THE FORTRAN OR IDL HANGS IN THIS ROUTINE THEN THERE IS A
C          MAJOR (>129 CHARACTER) BLOCKAGE IN THE PIPE.
C          
C  PROGRAM:
C     (I*4)  PIPEIN    =  PIPE INPUT STREAM FROM IDL
C     (I*4)  PIPEOU    =  PIPE OUTPUT STREAM TO IDL
C     (C*129) A        =  OUTPUT STRING
C     (C*129) B        =  INPUT STRING
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     ADAS      FLUSHES I/O BUFFER
C          I4UNIT     ADAS      STANDARD ERROR OUTPUT (SET UP IN XX0000)
C
C-----------------------------------------------------------------------
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    5TH JULY 1996
C
C VERSION: 1.1                          DATE: 05-07-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION.
C-----------------------------------------------------------------------
C
      INTEGER PIPEIN, PIPEOU, I4UNIT
      CHARACTER A*129, B*129
      PARAMETER(PIPEIN = 5, PIPEOU = 6)
      A = "I know I've made some very poor decisions recently, but I "//
     &"can give you my complete assurance that my work will be back "//
     &"to normal."

      WRITE(I4UNIT(-1),*)'CHECK_PIPE (FORTRAN), WRITING.'
      WRITE(PIPEOU,'(A)')A
      CALL XXFLSH(PIPEOU)

      READ(PIPEIN,'(A)') B
      IF(B.EQ.A)THEN
         WRITE(I4UNIT(-1),*)'CHECK_PIPE (FORTRAN): OK'
      ELSE
         WRITE(I4UNIT(-1),*)'CHECK_PIPE (FORTRAN): ERROR'
      ENDIF

      RETURN

      END
