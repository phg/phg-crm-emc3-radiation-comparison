CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxuid.for,v 1.2 2004/07/06 15:40:31 whitefor Exp $ Date $Date: 2004/07/06 15:40:31 $
CX
      SUBROUTINE XXUID( USERID )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXUID  *********************
C
C  PURPOSE: ADAS ROUTINE - SETS UP THE DEFAULT USERID WHICH STORES THE
C           DATA TO BE READ USING STANDARD ADAS DATA READING ROUTINES.
CA	    UNDER UNIX PORT THIS NOW GETS THE ENVIRONMENT VARIABLE 
CA	    "ADASCENT".
C
C           USERID: VALUE ON INPUT  =>  USERID: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA SOURCE USER ID
C                     *                 DEFAULT ADAS DATA SOURCE USER ID
C                  <BLANK>              *** USERID VALUE NOT CHANGED ***
C                  <OTHER>              *** USERID VALUE NOT CHANGED ***
C
C           ? => QUERIES CURRENT ADAS SOURCE USERID SETTING.
C           * => SETS ADAS SOURCE USERID SETTING TO DEFAULT VALUE.
C     <BLANK> => SETS ADAS SOURCE USERID SETTING TO DEFAULT VALUE.
C     <OTHER> => SETS ADAS SOURCE USERID SETTING TO INPUT   VALUE.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
CX  I/O   : (C*6)  USERID   = USERID UNDER WHICH ADAS DATA IS STORED
CX                           (IF BLANK DEFAULTS TO DEFUID)
CX
CX         (C*6)  DEFUID   = PARAMETER = DEFAULT USER ID FOR ADAS DATA
CX                                      SOURCE
CX
CX         (C*6)  ADASID   = CURRENT ADAS DATA SOURCE USER ID
C
C  I/O   : (C*80)  USERID   = USERID UNDER WHICH ADAS DATA IS STORED
C                           (IF BLANK DEFAULTS TO DEFUID)
C
C          (C*80)  DEFUID   = PARAMETER = DEFAULT USER ID FOR ADAS DATA
C                                      SOURCE
C
C          (C*80)  ADASID   = CURRENT ADAS DATA SOURCE USER ID
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C NOTE:
C          TO CHECK CURRENT ADAS SOURCE USERID CALL XXUID WITH
C          ? AS INPUT.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    10/03/93
C 
C UPDATE:  L. JALOTA  - 7/11/94 : ADDED CALL TO GETENV TO FETCH UNIX 
C				  ENVIRONMENT VARIABLE ADASUSER
C
C VERSION: 1.2                          DATE: 08-08-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS 'EXTERNAL GETENV' STATEMENT
C
C-----------------------------------------------------------------------
      CHARACTER  DEFUID*80
C-----------------------------------------------------------------------
      CHARACTER  USERID*80          , ADASID*80              , ENV*80
C-----------------------------------------------------------------------
      SAVE       ADASID
C-----------------------------------------------------------------------
CA GET THE VALUE OF ADASCENT WHICH SETS UP TOPLEVEL DATA DIRECTORY.
      CALL GETENV("ADASCENT", ENV)
      DEFUID = ENV
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE USER ID
C-----------------------------------------------------------------------
C
      IF (USERID(1:1).EQ.'?') THEN
	USERID = ADASID
      ELSE IF (USERID(1:1).EQ.'*') THEN
        USERID = DEFUID
        ADASID = DEFUID
      ELSE IF (USERID(1:1).EQ.' ') THEN
        ADASID = DEFUID
      ELSE
        ADASID = USERID
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
