CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxsszd.for,v 1.1 2004/07/06 15:39:42 whitefor Exp $ Date $Date: 2004/07/06 15:39:42 $
CX
      SUBROUTINE XXSSZD( USRGRP , USRTYP , USREXT )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXSSZD *********************
C
C  PURPOSE: ADAS ROUTINE - SETS UP THE DEFAULT USEGRP, USRTYP AND USREXT
C           WHICH IDENTIFY THE FILENAME AND EXTENSION TO BE READ IN
C           SUBROUTINE SSZD.  IT WORKS IN THE SAME MANNER AS XXUID WHICH
C           WHICH ALLOWS THE DEFAULT USER SPACE TO BE SET
C
C
C           USRGRP: VALUE ON INPUT  =>  USRGRP: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA GROUPNAME
C                     *                 DEFAULT ADAS DATA GROUPNAME
C                  <BLANK>              *** USRGRP VALUE NOT CHANGED ***
C                  <OTHER>              *** USRGRP VALUE NOT CHANGED ***
C
C
C           USRTYP: VALUE ON INPUT  =>  USRTYP: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA TYPEPNAME
C                     *                 DEFAULT ADAS DATA TYPEGROUPNAME
C                  <BLANK>              *** USRTYP VALUE NOT CHANGED ***
C                  <OTHER>              *** USRTYP VALUE NOT CHANGED ***
C
C
C           USREXT: VALUE ON INPUT  =>  USREXT: VALUE ON OUTPUT
C
C                     ?                 CURRENT ADAS DATA MEMBER EXTENS.
C                     *                 DEFAULT ADAS DATA MEMBER EXTENS
C                  <BLANK>              *** USREXT VALUE NOT CHANGED ***
C                  <OTHER>              *** USREXT VALUE NOT CHANGED ***
C
C         ? => QUERIES CURRENT ADAS DATA USRGRP, USRTYP OR USREXT
C              SETTING.
C         * => SETS ADAS DATA USEGRP, USRTYP OR USREXT SETTING
C              TO DEFAULT VALUE.
C   <BLANK> => SETS ADAS DATA USRGRP, USRTYP OR USREXT SETTING
C              TO DEFAULT VALUE.
C   <OTHER> => SETS ADAS DATA USRGRP, USRTYP OR USREXT SETTING
C              TO INPUT   VALUE.
C
C  CALLING PROGRAM: SSZD AND MAIN PROGRAMS USING SSZD
C
C  SUBROUTINE:
C
C I/O   : (C*8)  USRGRP   = USRGRP UNDER WHICH ADAS DATA IS STORED
C                            (IF BLANK DEFAULTS TO DEFGRP)
C
CX I/O   : (C*6)  USRTYP   = USRTYP UNDER WHICH ADAS DATA IS STORED
CA I/O   : (C*80) USRTYP   = OPTIONAL SUBDIRECTORY FOR FILE
C                            (IF BLANK DEFAULTS TO DEFTYP)
C
CX I/O   : (C*3)  USREXT   = USREXT UNDER WHICH ADAS DATA IS STORED
C                            (IF BLANK DEFAULTS TO DEFEXT)
C
C          (C*8)  DEFGRP   = PARAMETER = DEFAULT USER GROUP FOR ADAS
C                                        DATA SOURCE
C
CX         (C*6)  DEFTYP   = PARAMETER = DEFAULT USER TYPE FOR ADAS
CA         (C*80) DEFTYP   = OPTIONAL SUB-DIRECTORY DEFAULT IS BLANK
C                                        DATA SOURCE
C
C          (C*3)  DEFEXT   = PARAMETER = DEFAULT USER EXTENSION FOR ADAS
C                                        DATA SOURCE
C
C          (C*8)  ADASGR   = CURRENT ADAS DATA SOURCE GROUP
C
CX         (C*6)  ADASTY   = CURRENT ADAS DATA SOURCE TYPE
CA         (C*80) ADASTY   = OPTIONAL SUB-DIRECTORY
C
C          (C*3)  ADASEX   = CURRENT ADAS DATA SOURCE EXTENSION
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C NOTE:
C          TO CHECK CURRENT ADAS SOURCE USRGRP, USRTYP AND USREXT
C          CALL XXSSZD WITH ?`S AS INPUTS.
C
C AUTHOR:  HUGH P. SUMMERS, JET
C          K1/1/67
C          JET EXT. 4941
C
C DATE:     2/09/93
C
C UPDATE:  L. JALOTA - 10/11/94 : MODIFIED FOR USE UNDER UNIX
C				  INCREASED STRING LENGTHS
C UPDATE:  L. JALOTA - 22/11/94 : TIDIED UP CHARACTER LENGTH DEFINITIONS.
C-----------------------------------------------------------------------
CX    CHARACTER  DEFGRP*8             , DEFTYP*6      , DEFEXT*3
      CHARACTER  DEFGRP*8             , DEFTYP*80     , DEFEXT*3
C-----------------------------------------------------------------------
CX    PARAMETER  (DEFGRP = 'IONELEC ' , DEFTYP = 'DATA ' ,
CX   &            DEFEXT = 'SZD')
      PARAMETER  (DEFGRP = 'ionelec'  , DEFTYP = ' ' ,
     &            DEFEXT = 'szd')
C-----------------------------------------------------------------------
CX    CHARACTER  USRGRP*8             , ADASGR*8      ,
CX   &           USRTYP*6             , ADASTY*6      ,
      CHARACTER  USRGRP*8             , ADASGR*8      ,
     &           USRTYP*80            , ADASTY*80      ,
     &           USREXT*3             , ADASEX*3
C-----------------------------------------------------------------------
      SAVE       ADASGR , ADASTY , ADASEX
C-----------------------------------------------------------------------
      DATA       ADASGR , ADASTY , ADASEX / DEFGRP , DEFTYP , DEFEXT /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE GROUPNAME
C-----------------------------------------------------------------------
C
      IF (USRGRP(1:1).EQ.'?') THEN
        USRGRP = ADASGR
      ELSE IF (USRGRP(1:1).EQ.'*') THEN
        USRGRP = DEFGRP
        ADASGR = DEFGRP
      ELSE IF (USRGRP(1:1).EQ.' ') THEN
        ADASGR = DEFGRP
      ELSE
        ADASGR = USRGRP
      ENDIF
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE TYPENAME
C-----------------------------------------------------------------------
C
      IF (USRTYP(1:1).EQ.'?') THEN
        USRTYP = ADASTY
      ELSE IF (USRTYP(1:1).EQ.'*') THEN
        USRTYP = DEFTYP
        ADASTY = DEFTYP
      ELSE IF (USRTYP(1:1).EQ.' ') THEN
        ADASTY = DEFTYP
      ELSE
        ADASTY = USRTYP
      ENDIF
C
C-----------------------------------------------------------------------
C SET ADAS DATA SOURCE EXTENSION
C-----------------------------------------------------------------------
C
      IF (USREXT(1:1).EQ.'?') THEN
        USREXT = ADASEX
      ELSE IF (USREXT(1:1).EQ.'*') THEN
        USREXT = DEFEXT
        ADASEX = DEFEXT
      ELSE IF (USREXT(1:1).EQ.' ') THEN
        ADASEX = DEFEXT
      ELSE
        ADASEX = USREXT
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
