CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxinst.for,v 1.2 2007/04/11 13:01:55 allan Exp $ Date $Date: 2007/04/11 13:01:55 $
CX
      SUBROUTINE XXINST( IUNIT  , DSNAME , LERROR ,
     &                   NDDEN  , NDTIN  , NDZ1V  ,
     &                   IDE    , ITE    , IZE    ,
     &                   DENSR  , TR     , ZIPT   ,
     &                   LSWIT  , EIA    ,
     &                   AIPT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXINST *********************
C
C  PURPOSE: TO FETCH DATA FROM STANDARD MASTER CONDENSED
C           COLLISIONAL-DIELECTRONIC FILES.
C
C           THE FOLLOWING FILES ARE ALLOWED:
C
C             1. RECOMBINATION COEFFICIENTS
C             2. IONISATION    COEFFICIENTS
C             3. CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C             4. RECOMBINATION-BREMSSTRAHLUNG  POWER COEFFICIENTS
C             5. CHARGE-EXCHANGE RECOMBINATION POWER COEFFICIENTS
C             6. TOTAL    LINE POWER COEFFICIENTS
C             7. SPECIFIC LINE POWER COEFFICIENTS
C
C             (NOTE: PARTIAL MASTER CONDENSED FILES SHOULD BE READ USING
C                    'XXIN15' & 'XXIN68'.
C                    IF  PARTIAL AND STANDARD FILES ARE BOTH TO BE READ
C                    USE 'XXIN15' AND 'XXIN68')
C
C  CALLING PROGRAM: GENERAL USE
C
C  DATA:
C           THE  SOURCE  DATA IS CONTAINED AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             5. JETUID.PLT<YR>.DATA
C             5. JETUID.PLS<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C           IF <YR> IS BLANK THEN THE CURRENT RECOMMENDED DATA SETS ARE
C           USED
C
C           THE MEMBERS OF THE PARTITIONED DATA SETS MUST BE STANDARD
C           MASTER CONDENSED FILES AND OF THE FORM <SE>
C
C           WHERE: <SE> IS THE ONE OR TWO LETTER ION SEQUENCE CODE.
C
C                  E.G.  'C'   OR 'HE'  ('@' = BARE-NUCLEUS)
C
C                  PARTIAL  FILES WHICH HAVE MEMBERS NAMES WITH EITHER
C                  TWO DIGITS OR A DOUBLE HASH  AFTER THEM  CANNOT  BE
C                  READ USING THIS ROUTINE (SEE 'XXIN15' & 'XXIN68')
C
C           THE  'PARTIAL'  AND  'STANDARD'  MASTER CONDENSED FILES  ARE
C           SIMILAR IN FORM. A MAJOR DIFFERENCE IS THAT ADDITIONAL LINES
C           ARE  INCLUDED  AT  THE  BEGINNING OF  THE  'PARTIAL'  MASTER
C           FILES.    THE  FIRST OF THESE LINES CONTAINING A ROW OF  '='
C           SIGNS.  THIS DIFFERENCE IS USED TO MAKE SURE THAT A STANDARD
C           MASTER FILE IS BEING READ.
C
C           THE CHARACTER STRING SEPARATING THE INPUT DATA FOR EACH
C           VALUE OF Z1 IN THE FILE WILL GIVE:
C
C                        THE Z1 VALUE (Z1=) AND DATE (DATE:).
C              (OLDER DATA SETS MAY HAVE 'Z =' INSTEAD OF 'Z1=' HERE)
C
C           (NOTE: Z1 = ION CHARGE + 1 = RECOMBINING ION CHARGE)
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT DATA SET ALLOCATED
C  INPUT : (C*(*))DSNAME  = INPUT MASTER CONDENSED FILE DATA SET NAME
C  OUTPUT: (L*4)  LERROR  = .TRUE.  => ERROR DETECTED IN READING FILE
C                         = .FALSE. => NO ERROR DETECTED IN FILE
C
C  INPUT : (I*4)  NDDEN   = MAX. NUMBER OF REDUCED DENSITIES ALLOWED IN
C                           MASTER CONDENSED FILE FOR A GIVEN SEQUENCE
C  INPUT : (I*4)  NDTIN   = MAX. NO. OF REDUCED TEMPERATURES ALLOWED IN
C                           MASTER CONDENSED FILE FOR A GIVEN SEQUENCE
C  INPUT : (I*4)  NDZ1V   = MAX. NUMBER OF CHARGE STATES ALLOWED IN
C                           MASTER CONDENSED FILE FOR A GIVEN SEQUENCE
C
C  OUTPUT: (I*4)  IDE     = NUMBER OF REDUCED DENSITIES READ FROM INPUT
C                           MASTER CONDENSED FOR A GIVEN SEQUENCE
C  OUTPUT: (I*4)  ITE     = NO. OF REDUCED TEMPERATURES READ FROM INPUT
C                           MASTER CONDENSED FOR A GIVEN SEQUENCE
C  OUTPUT: (I*4)  IZE     = NO. OF CHARGE STATES GIVEN IN THE INPUT
C                           MASTER CONDENSED FOR A GIVEN SEQUENCE
C
C  OUTPUT: (R*8)  DENSR() = SET OF 'IDE' INPUT REDUCED DENSITIES (CM-3/
C                           Z1**7) READ FROM CONDENSED MASTER FILE.
C  OUTPUT: (R*8)  TR()    = SET OF 'ITE'  INPUT  REDUCED  TEMPERATURES
C                           (K/Z1**7) READ FROM CONDENSED MASTER FILE.
C  OUTPUT: (R*8)  ZIPT()  = SET OF 'IZE' INPUT CHARGE STATES READ FROM
C                           CONDENSED MASTER/METASTABLE FILE.
C                           (CHARGE STATE = ION CHARGE + 1 = RECOMBINING
C
C  OUTPUT: (L*4)  LSWIT   = .TRUE.  => IONISATION RATE COEFFICIENTS
C                                      INCLUDED IN INPUT MASTER FILE.
C                           .FALSE. => IONISATION RATE COEFFICIENTS
C                                      NOT INCLUDED IN INPUT MASTER FILE
C  OUTPUT: (R*8)  EIA()   = IONISATION RATE COEFFICIENTS: ()=ION CHARGE
C                           UNITS: WAVE NUMBERS (CM-1)
C                           (= 0.0 IF NOT SET )
C
C  OUTPUT: (R*8)  AIPT(,,)= CONDENSED MASTER FILE DATA. COLL-DIEL COEFF.
C                           1ST DIMENSION: REDUCED DENSITY ('DENSR()')
C                           2ND DIMENSION: REDUCED TEMPERATURE ('TR()')
C                           3RD DIMENSION: CHARGE STATE ('ZIPT()')
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IPOT    = NUMBER OF IONISATION RATE COEFF. VALUES
C                           PRESENT IN THE INPUT FILE.
C          (I*4)  IZ1     = CHARGE STATE READ FROM THE LINE PRECEEDING
C                           AN INPUT BLOCK FROM THE FILE.
C                           (= ION CHARGE + 1 = RECOMBINING ION CHARGE)
C          (I*4)  IBGN    = FIRST BYTE OF INTEREST IN CHARACTER 'STRING'
C          (I*4)  IEND    = LAST  BYTE OF INTEREST IN CHARACTER 'STRING'
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR ION-CHARGE VALUES
C          (I*4)  I       = GENERAL USE
C
C          (C*5)  CPOT    = 'IPOT'
C          (C*80) STRING  = STRING INTO WHICH 1ST LINE OF INPUT FILE IS
C                           READ TO ENABLE ITS FORMAT TO BE ESTABLISHED.
C
C NOTE:
C          STREAM HANDLING:
C             STREAM 'IUNIT' IS USED FOR READING CONDENSED MASTER FILES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXREIA     ADAS      READ IN UNKNOWN NUMBER OF 'EIA' VALUES
C                               IF PRESENT.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    22/08/90
C
C DATE:    05/03/90 - PE BRIDEN - ADAS91: OPENING OF DATA SET REMOVED.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE (NO CHANGES)
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IUNIT       , NDDEN     , NDTIN     , NDZ1V    ,
     &                         IDE       , ITE       , IZE
      INTEGER    IPOT        , IBGN      , IEND      , IZ1      ,
     &           I           , ID        , IT        , IZ
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*(*)
      CHARACTER  CPOT*5      , STRING*80
C-----------------------------------------------------------------------
      LOGICAL    LERROR      , LSWIT
C-----------------------------------------------------------------------
      REAL*8     DENSR(NDDEN), TR(NDTIN) , ZIPT(NDZ1V)
      REAL*8     EIA(50)
      REAL*8     AIPT(NDDEN,NDTIN,NDZ1V)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LERROR = .FALSE.
      LSWIT  = .FALSE.
C
C-----------------------------------------------------------------------
C READ IN FIRST LINE OF FILE TO ESTABLISH THE FILE IS A STANDARD FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) STRING
C
C-----------------------------------------------------------------------
C INVALID: PARTIAL MASTER CONDENSED FILE:
C-----------------------------------------------------------------------
C
         IF (STRING(1:1).EQ.'=') THEN
            WRITE(I4UNIT(-1),2000) DSNAME ,
     &      'THIS DATA SET IS A PARTIAL AND NOT A STANDARD MASTER FILE'
            WRITE(I4UNIT(-1),2001)
            LERROR=.TRUE.
            RETURN
C
C-----------------------------------------------------------------------
C VALID:  STANDARD MASTER CONDENSED FILE: READ IN ARRAY SIZES AND TEST
C-----------------------------------------------------------------------
C
         ELSE
            READ(STRING(1:20),1001) IZE , ITE , IDE , CPOT
         ENDIF
C-----------------------------------------------------------------------
      IF(IZE.GT.NDZ1V) STOP ' XXINST ERROR: IZE > NDZ1V. INCREASE NDZ1V'
      IF(ITE.GT.NDTIN) STOP ' XXINST ERROR: ITE > NDTIN. INCREASE NDTIN'
      IF(IDE.GT.NDDEN) STOP ' XXINST ERROR: IDE > NDDEN. INCREASE NDDEN'
C
C-----------------------------------------------------------------------
C  ESTABLISH IF IONISATION RATE COEFFICIENTS ARE PRESENT
C-----------------------------------------------------------------------
C
         IF (CPOT.NE.' ') THEN
            READ(CPOT,1001) IPOT
            IF (IPOT.GT.0) LSWIT=.TRUE.
         ENDIF
C
C-----------------------------------------------------------------------
C  READ IN RECOMBINING ION CHARGE VALUES TABULATED IN THE FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1002) (ZIPT(IZ), IZ=1,IZE)
      READ(IUNIT,1003)
C
C-----------------------------------------------------------------------
C  READ IN IONISATION RATE COEFFICIENTS IF PRESENT (UNITS: CM**3/SEC).
C  OTHERWISE CHECK THEY ARE NOT PRESENT - IF PRESENT READ THEM.
C  MISSING 'EIA()' VALUES ARE SET TO ZERO.
C-----------------------------------------------------------------------
C
         IF (LSWIT) THEN
            READ(IUNIT,1004) (EIA(I),I=1,IPOT)
            READ(IUNIT,1003)
               DO 1 I=(IPOT+1),50
                  EIA(I)=0.0D0
    1          CONTINUE
         ELSE
            CALL XXREIA( IUNIT , LSWIT , EIA )
         ENDIF
C
C-----------------------------------------------------------------------
C  READ IN ELECT. DENSITY AND TEMPERATURE VALUES TABULATED IN THE FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1005) (DENSR(ID), ID=1,IDE)
      READ(IUNIT,1005) (TR(IT)   , IT=1,ITE)
C
C***********************************************************************
C READ IN COEFFICIENTS FOR EACH VALUE OF Z1 LISTED IN THE HEADINGS
C***********************************************************************
C
         DO 2 IZ=1,IZE
C
C-----------------------------------------------------------------------
C CHECK Z1 FOR CURRENT DATA BLOCK IS THAT EXPECTED FROM THE HEADING DATA
C-----------------------------------------------------------------------
C
            READ(IUNIT,1000) STRING
            IBGN=INDEX(STRING,'Z1=')
            IF (IBGN.EQ.0) IBGN=INDEX(STRING,'Z =')
            IBGN=IBGN+3
            IEND=IBGN+1
            READ(STRING(IBGN:IEND),1006) IZ1
               IF (NINT(ZIPT(IZ)).NE.IZ1) THEN
                  WRITE(I4UNIT(-1),2000) DSNAME ,
     &                  'Z1 BLOCK HEADING DISCREPANCY FOR:'
                  WRITE(I4UNIT(-1),2002) NINT(ZIPT(IZ)) , IZ1
                  WRITE(I4UNIT(-1),2001)
                  LERROR=.TRUE.
                  RETURN
               ENDIF
C
C-----------------------------------------------------------------------
C READ IN COEFFICIENTS
C-----------------------------------------------------------------------
C
               DO 3 IT=1,ITE
                  READ(IUNIT,1005) (AIPT(ID,IT,IZ),ID=1,IDE)
    3          CONTINUE
C-----------------------------------------------------------------------
    2    CONTINUE
C***********************************************************************
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(A80)
 1001 FORMAT(3I5,A5)
 1002 FORMAT(8F10.5)
 1003 FORMAT()
 1004 FORMAT(6F12.1)
 1005 FORMAT(8E10.2)
 1006 FORMAT(I2)
C-----------------------------------------------------------------------
 2000 FORMAT(/1X,32('*'),' XXINST ERROR ',32('*')//
     &        1X,'INPUT MASTER CONDENSED FILE: ',A/
     &        1X,A)
 2001 FORMAT(/1X,28('*'),' END OF ERROR MESSAGE ',28('*'))
 2002 FORMAT( 1X,'CHARGE STATE (Z1) EXPECTED = ',I2/
     &        1X,'CHARGE STATE (Z1) READ     = ',I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
