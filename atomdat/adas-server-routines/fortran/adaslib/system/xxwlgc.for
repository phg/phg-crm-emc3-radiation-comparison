CX UNIX - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxwlgc.for,v 1.1 2004/07/06 15:40:40 whitefor Exp $ Date $Date: 2004/07/06 15:40:40 $
CX
      SUBROUTINE XXwLGC ( LVAR )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXWLGC *********************
C
C  PURPOSE: Sends a 1 for true or a 0 for false to IDL
C
C  CALLING PROGRAM: GENERAL USE
C
C
C  SUBROUTINE:
C
C
C INPUT:   (L)     LVAT     = Logic variable to write to IDL
C
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     ADAS      FLUSHES I/O BUFFER
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    28-02-2003
C
C-----------------------------------------------------------------------
      INTEGER    PIPEOU     ,   ONE    , ZERO
C-----------------------------------------------------------------------
      PARAMETER( PIPEOU = 6 , ONE = 1  , ZERO = 0)
C----------------------------------------------------------------------
      LOGICAL   LVAR
C-----------------------------------------------------------------------

      IF (LVAR) THEN
        WRITE(PIPEOU,*)ONE
      ELSE
        WRITE(PIPEOU,*)ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
      
C-----------------------------------------------------------------------

      RETURN
      END
