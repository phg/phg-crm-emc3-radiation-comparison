      SUBROUTINE XXFLNM( DSNIN  , DSNFUL , LEXIST )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXFLNM *********************
C
C  PURPOSE: TO PREPARE A UNIX DATASET NAME FROM A STRING WHICH MAY
C           INCLUDE AN ADAS ENVIRONMENT LEADER AND COMMENTS.
C           THE ADAS ENVIRONMENT VARIABLE MUST BE FIRST AND IN DOUBLE
C           QUOTES.  THE COMMENTS MUST EITHER FOLLOW OR PRECEDE A COLON.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C80)  DSNIN    = INPUT STRING FOR INTERROGATION
C
C  OUTPUT: (C80)  DSNFUL   = THE FULL EXPANDED FILE NAME WITHOUT
C                             EXTRANEOUS MATERIAL
C  OUTPUT: (L*4)  LEXIST   = .TRUE. => NAME FORMED AND FILE EXISTS
C                             .FALSE.=> FAILED TO FORM NAME OR FIND FILE
C
C          (C*80) DSN1     = WORK STRING
C          (C*80) DSNTEMP  = WORK STRING
C          (C*80) BLANK    = BLANK STRING
C          (I*4)  LEN1     = STRING INDEX
C          (I*4)  LEN2     = STRING INDEX
C          (I*4)  LEN3     = STRING INDEX
C          (I*4)  LEN4     = STRING INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSLEN     ADAS     FIND BEGINNING AND END OF A STRING
C          I4UNIT     ADAS     FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          lenstr     ADAS     Get length of a string
C          GETENV     UNIX     FETCH AN ENVIRONMENT VARIABLE
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE  :  21/05/96
C
C UPDATE:
C
C VERSION: 1.1                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - ADDED CODE FOR INITIAL COMMENTS AND PUT INTO S.C.C.S.
C
C VERSION : 1.2
C DATE    : 08-05-2016
C MODIFIED: Martin O'Mullane
C           - Let the length of the input and output filenames take their
C             value from the declaration in the calling routine. Stop if 
C             these are longer than the internal 132 character temporary 
C             string. Adds a dependence on lenstr.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
C-----------------------------------------------------------------------
      INTEGER   LEN1, LEN2, LEN3, LEN4, P1, P2
      integer   lenstr , len0
C-----------------------------------------------------------------------
      CHARACTER DSNIN*(*), DSNFUL*(*), DSN1*132, BLANK*132, DSNTEMP*132
C-----------------------------------------------------------------------
      LOGICAL LEXIST
C-----------------------------------------------------------------------
      DATA BLANK /' '/
C-----------------------------------------------------------------------
      LEXIST  = .FALSE.
      DSNTEMP = DSNIN
C-----------------------------------------------------------------------
C DSNTEMP STORES DSNIN SO THAT IT CAN BE RESTORED INTACT AT THE END
C-----------------------------------------------------------------------
C REMOVE COMMENTS.
C ASSUME PRECURSOR COMMENT IF THERE ARE TWO COLONS OR IF THERE IS NO " 
C OR / IN STRING TO LEFT OF COLON.
C-----------------------------------------------------------------------
      len0 = lenstr(dsnin)
      if (len0.GT.132) then
         write(i4unit(-1),1001)
         stop
      endif
 
      P1 = INDEX(DSNIN,":")
      IF(P1.GE.0)THEN
C-----------------------------------------------------------------------
C        THERE ARE ONE OR MORE COLONS
C-----------------------------------------------------------------------
         P2 = INDEX(DSNIN(P1+1:len0),":")
         IF(P2.GT.0)THEN
C-----------------------------------------------------------------------
C           THERE ARE TWO COLONS SO TAKE THE MIDDLE PART OF DSNIN
C-----------------------------------------------------------------------
            DSN1=DSNIN(P1+1:P2+P1-1)
            DSNIN=DSN1
         ELSE
            IF((INDEX(DSNIN(1:P1),'"').LE.0).AND.
     &         (INDEX(DSNIN(1:P1),'/').LE.0))THEN
C-----------------------------------------------------------------------
C              NO " OR / IN LEFT PART => IT'S A COMMENT
C              SO TAKE THE RIGHT HAND PART OF DSNIN
C-----------------------------------------------------------------------
               DSN1=DSNIN(P1+1:len0)
               DSNIN=DSN1
            ELSE
C-----------------------------------------------------------------------
C              TAKE LEFT HAND PART OF DSNIN
C-----------------------------------------------------------------------
               DSN1=DSNIN(1:P1-1)
               DSNIN=DSN1
            ENDIF
         ENDIF   
      ENDIF
      DSN1=BLANK
C-----------------------------------------------------------------------
C LOOK FOR AN ENVIRONMENT VARIABLE WITH DOUBLE QUOTES AROUND IT
C-----------------------------------------------------------------------
      LEN1 = INDEX(DSNIN,'"')
      IF (LEN1.GT.0)THEN
         LEN2=INDEX(DSNIN(LEN1+1:len0),'"')
         IF(LEN2.EQ.0)THEN
C-----------------------------------------------------------------------
C           ONLY ONE QUOTE IN STRING: GIVE ERROR
C-----------------------------------------------------------------------
            WRITE(I4UNIT(-1),1000)
            RETURN
         ENDIF
C-----------------------------------------------------------------------
C        GET THE VALUE OF THE ENVIRONMENT VARIABLE
C-----------------------------------------------------------------------
         CALL GETENV(DSNIN(LEN1+1:LEN2+LEN1-1),DSN1)
         CALL XXSLEN(DSN1,LEN1,LEN2)
         IF(LEN1.EQ.0)THEN
C-----------------------------------------------------------------------
C           ENVIRONMENT VARIABLE NOT FOUND: GIVE ERROR
C-----------------------------------------------------------------------
            WRITE(I4UNIT(-1),1000)
            RETURN
         ELSE
C-----------------------------------------------------------------------
C           TAKE THE STRING TO THE RIGHT OF AND INCLUDING THE FIRST /
C           IN DSNIN AS THE REST OF THE FILENAME
C-----------------------------------------------------------------------
            CALL XXSLEN(DSNIN,LEN3,LEN4)
            LEN3=INDEX(DSNIN,'/')
            DSNFUL=BLANK
            DSNFUL=DSN1(LEN1:LEN2)//DSNIN(LEN3:LEN4)
         ENDIF
      ELSE
C-----------------------------------------------------------------------
C        NO ENVIRONMENT VARIABLE IN DSNIN
C-----------------------------------------------------------------------
         CALL XXSLEN(DSNIN,LEN3,LEN4)
         DSNFUL=BLANK
         DSNFUL=DSNIN(LEN3:LEN4)
      ENDIF

      INQUIRE(FILE=DSNFUL,EXIST=LEXIST)
      IF(.NOT.LEXIST)WRITE(I4UNIT(-1),1000)

C-----------------------------------------------------------------------

      DSNIN = DSNTEMP

      RETURN

 1000 FORMAT('XXFLNM WARNING: FILE NAME NOT RESOLVED OR DOES NOT EXIST')
 1001 FORMAT('XXFLNM ERROR: LENGTH OF INPUT FILE GT 132 CHARACTERS')

      END
