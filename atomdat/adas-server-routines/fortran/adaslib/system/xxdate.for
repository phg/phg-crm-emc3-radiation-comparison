CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxdate.for,v 1.1 2004/07/06 15:33:30 whitefor Exp $ Date $Date: 2004/07/06 15:33:30 $
CX
      SUBROUTINE XXDATE ( DATE )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXDATE *********************
C
C  PURPOSE: GATHERS CURRENT DATE AS A 8 BYTE STRING FROM IDL VIA PIPE
C
C  CALLING PROGRAM: GENERAL USE
C
C
C  SUBROUTINE:
C
C
C OUTPUT:  (C*8)  DATE     = CURRENT DATE (AS 'DD/MM/YY')
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  ANDREW BOWEN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24/05/93
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER DATE*8
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') DATE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
