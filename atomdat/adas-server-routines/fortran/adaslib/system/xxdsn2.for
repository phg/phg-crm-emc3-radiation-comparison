CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxdsn2.for,v 1.1 2004/07/06 15:34:27 whitefor Exp $ Data $Date: 2004/07/06 15:34:27 $
CX
      SUBROUTINE XXDSN2 ( DSNIN   ,
     &                    DSNOUT  , LEXIST
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXDSN2 *********************
C
C  PURPOSE: TO CHECK IF A FULLY QUALIFIED MVS SEQUENTIAL OR PARTITIONED
C           DATA SET EXISTS AND RETURNS THE DATA SET NAME IN A FORM FOR
C           DYNAMIC ALLOCATION.
C
C                   ***** 'DSNIN' MUST BE OF VALID FORM *****
C
C           NOTE: INPUT DSN MUST START WITH THE USERID AND MAY OR MAY
C                 NOT BE ENCLOSED IN QUOTES.  IT CAN ALREADY BE  IN A
C                 FORM SUITABLE FOR DYNAMIC  ALLOCATION IN WHICH CASE
C                 THE OUTPUT DSN AND INPUT DSN  ARE  IDENTICAL   (I.E.
C                 NEITHER HAVE QUOTES AND BOTH START WITH A '/'.)
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C*(*)) DSNIN   = DATASET NAME
C                            (CAN BE ENCLOSED WITHIN APOSTROPHES OR
C                             START WITH A '/')
C
C  OUTPUT: (C*(*)) DSNOUT  = VERSION OF  'DSNIN'  ALETRED SO THAT IT
C                            CAN BE USED TO DYNAMICALLY OPEN THE FILE.
C                            (NOTE: APOSTROPHES REMOVED, / ADDED AT
C                             START IF REQUIRED.)
C  OUTPUT: (L*4)   LEXIST  = .TRUE.  => FILE 'DSNIN' ALREADY EXISTS
C                          = .FALSE. => FILE 'DSNIN' DOES NOT EXISTS
C
C          (C*1)   QUOTE   = PARAMETER: APOSTROPHE (').
C          (C*1)   BSLASH  = PARAMETER: BACK SLASH (/).
C
C          (I*4)   ILEND   = LENGTH IN BYTES OF 'DSNIN'
C          (I*4)   I1      = LOCATION OF FIRST APOSTROPHE IN 'DSNIN'
C          (I*4)   I2      = LOCATION OF SECOND APOSTROPHE IN 'DSNIN'
C          (I*4)   IDSLEN  = IDENTIFIES THE NUMBER OF CHARACTERS PRESENT
C                            IN THE INPUT DATA SET NAME MINUS ONE.
C                            (I.E. NUMBER   OF   CHARACTERS  BETWEEN
C                                  APOSTROPHES IF PRESENT.)
C
C          (L*4)   LQUOTE  = .TRUE.  => APOSTROPHES PRESENT
C                            .FALSE. => NO APOSTROPHES PRESENT
C
C NOTE:
C
C          IDSLEN >=0 => NO APOSTROPHES IN 'DSNIN' OR TWO SEPERATED
C                        BY AT LEAST ONE CHARACTER.
C          IDSLEN =-1 => TWO ADJACENT APOSTROPHES
C          IDSLEN =-2 => ONLY ONE APOSTROPHE FOUND
C
C          IF:
C
C          IDSLEN =-1 ERROR: PROGRAM STOPS
C          IDSLEN =-2 ERROR: PROGRAM STOPS.
C
C
C ROUTINES:   NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    12/12/90  (IN ADAS90 VERSION 1 THIS ROUTINE IS CALLED XXDSN1)
C
C-----------------------------------------------------------------------
      CHARACTER  QUOTE*1     , BSLASH*1
C-----------------------------------------------------------------------
      PARAMETER( QUOTE=''''  , BSLASH='/' )
C-----------------------------------------------------------------------
      INTEGER    IDSLEN      , I1         , I2        , ILEND
C-----------------------------------------------------------------------
      CHARACTER  DSNIN*(*)   , DSNOUT*(*)
C-----------------------------------------------------------------------
      LOGICAL    LEXIST      , LQUOTE
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  LOCATE APOSTROPHES (') IN 'DSNIN'.
C-----------------------------------------------------------------------
C
      ILEND=LEN(DSNIN)
C-----------------------------------------------------------------------
      I1=INDEX(DSNIN(1:ILEND),QUOTE)
C-----------------------------------------------------------------------
         IF (I1.EQ.0) THEN
C
C-----------------------------------------------------------------------
C NO APOSTROPHES IN DATA SET NAME
C-----------------------------------------------------------------------
C
            LQUOTE = .FALSE.
            I1     = 1
            I2     = ILEND - 1
         ELSE
            LQUOTE = .TRUE.
            I1=I1+1
            I2=INDEX(DSNIN(I1:ILEND),QUOTE)
            I2=I2+I1-2
         ENDIF
C-----------------------------------------------------------------------
      IDSLEN=I2-I1
C-----------------------------------------------------------------------
        IF (IDSLEN.EQ.-1) THEN
           STOP
     &      ' XXDSN2 ERROR: SPECIFIED DSN HAS TWO ADJACENT APOSTROPHES'
        ELSEIF (IDSLEN.EQ.-2) THEN
           STOP
     &      ' XXDSN2 ERROR: SPECIFIED DSN HAS ONLY A SINGLE APOSTROPHE'
        ENDIF
C
C-----------------------------------------------------------------------
C INQUIRE ABOUT FILE (ADD '/' IF NEEDED & REMOVE APOSTROPHES)
C-----------------------------------------------------------------------
C
      DSNOUT=' '
C
         IF (DSNIN(I1:I1).EQ.BSLASH) THEN
            IF ( .NOT.LQUOTE ) I2=ILEND
            DSNOUT = DSNIN(I1:I2)
         ELSE
            DSNOUT = BSLASH//DSNIN(I1:I2)
         ENDIF
C-----------------------------------------------------------------------
      INQUIRE( FILE=DSNOUT , EXIST=LEXIST )
C-----------------------------------------------------------------------
      RETURN
      END
