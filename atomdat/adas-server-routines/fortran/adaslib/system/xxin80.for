CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxin80.for,v 1.3 2019/11/23 18:34:39 mog Exp $ Date $Date: 2019/11/23 18:34:39 $
CX
      SUBROUTINE XXIN80( IUNIT  , DSNAME , LERROR ,
     &                   NDDEN  , NDTIN  , NDZ1V  , NDMET ,
     &                   IDE    , ITE    , IZE    ,
     &                   DENSR  , TR     , ZIPT   ,
     &                   IME    , IMETR  , CSTRGA ,
     &                   NPRNT  , IPRNT  , IPSYS  ,
     &                   LSWIT  , EIA    ,
     &                   AIPT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXIN80 *********************
C
C  PURPOSE: TO OPEN AND ACQUIRE DATA FROM MASTER CONDENSED
C           COLLISIONAL-DIELECTRONIC FILES:
C
C           THE FOLLOWING FILES ARE ALLOWED:
C
C             8. TOTAL    LINE POWER COEFFICIENTS
C             9. SPECIFIC LINE POWER COEFFICIENTS
C
C           AND TO OPEN AND ACQUIRE DATA FROM THE FOLLOWING FILE:
C
C            10. METASTABLE POPULATION DATA
C
C             (NOTE: OTHER MASTER  CONDENSED  COLL.-DIEL.  COEFFICIENTS
C                    SHOULD BE READ USING 'XXIN17'.
C                    IF  ONLY STANDARD FILES ARE  TO  BE  READ  BY  THE
C                    PROGRAM USE 'XXINST'.)
C
C  CALLING PROGRAM: GENERAL USE
C
C  DATA:
C           THE  SOURCE  DATA IS CONTAINED AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             8. JETUID.PLT<YR>.DATA
C             9. JETUID.PLS<YR>.DATA
C            10. JETUID.MET<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C           IF <YR> IS BLANK THEN THE CURRENT RECOMMENDED DATA SETS ARE
C           USED
C
C           THE MEMBERS OF THE PARTITIONED DATA SETS ARE EITHER:
C           1)  <SE><#><#>  FOR PARTIAL MASTER CONDENSED FILES, OR
C           2)  <SE>        FOR STANDARD MASTER CONDENSED FILES AND
C                           METASTABLE POPULATION FILE
C
C           WHERE: <SE> IS THE ONE OR TWO LETTER ION SEQUENCE CODE.
C                       NOTE: FOR THE BARE NUCLEUS <SE> = '@'
C                  <#>  IS THE SINGLE CHARACTER '#'
C
C
C                  E.G. PARTIAL  MASTER CONDENSED FILES:'@##' OR 'HE##'
C                       STANDARD AND METASTABLE FILES  : '@'  OR 'HE'
C
C           THE  'PARTIAL'  AND  'STANDARD'  MASTER CONDENSED FILES ARE
C           IDENTICAL IN FORM  EXCEPT:
C           A) EIGHT  ADDITIONAL  LINES ARE INCLUDED  AT THE  BEGINNING
C              OF  THE  'PARTIAL'  MASTER  FILES.   THE FIRST  OF THESE
C              LINES CONTAINS  A ROW OF  '=' SIGNS,  THE NEXT SIX LINES
C              CONTAIN METASTABLE LEVEL  INFORMATION, SUCH AS NUMBER OF
C              LEVELS, PARENT/SPIN INDEXES ETC. THE EIGHTH LINE CONTAINS
C              A ROW OF "-" SIGNS.  THIS DIFFERENCE IS USED TO IDENTIFY
C              WHICH FILE TYPE IS BEING READ.
C           B) THE PARTIAL FILES INCLUDE THE POPULATIONS  FOR  EACH  OF
C              THE METASTABLE LEVELS, WHEREAS THE STANDARD FILES CONTAIN
C              A SINGLE SET OF COMBINED POPULATIONS.
C
C          THE METASTABLE POPULATION FILES HAVE THE SAME FORMAT AS THE
C          PARTIAL MASTER CONDENSED FILES
C
C          THE CHARACTER STRING SEPARATING THE INPUT DATA FOR EACH
C          CHARGE STATE IN THE FILE WILL GIVE:
C
C          THE CHARGE STATE VALUE (Z1=) AND DATE (DATE:).
C                (OLDER DATA SETS MAY HAVE 'Z =' INSTEAD OF 'Z1=' HERE)
C          (CHARGE STATE Z1 = ION CHARGE + 1 = RECOMBINING ION CHARGE)
C
C          UNDER EACH OF THESE LINES THE COEFFTS/POPULATIONS ARE LISTED
C          -IN THE CASE OF THE METASTABLE/PARTIAL FILES VALUES FOR EACH
C          OF THE METASTABLE LEVELS ARE LISTED EACH BEING  PRECEDED  BY
C          A LINE GIVING THE METASTABLE INDEX BETWEEN TWO "/".
C          E.G.  /2/ => METASTABLE LEVEL TWO.
C
C          DATA FOR INDIVIDUAL PARENT/SPIN SYSTEMS ARE LISTED IN THE
C          MEATASTABLE FILES.
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT DATA SET ALLOCATED
C  INPUT : (C*(*))DSNAME  = INPUT MASTER CONDENSED FILE DATA SET NAME
C  OUTPUT: (L*4)  LERROR  = .TRUE.  => ERROR DETECTED IN READING FILE
C                         = .FALSE. => NO ERROR DETECTED IN FILE
C
C  INPUT : (I*4)  NDDEN   = MAX. NUMBER OF REDUCED DENSITIES ALLOWED IN
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C  INPUT : (I*4)  NDTIN   = MAX. NO. OF REDUCED TEMPERATURES ALLOWED IN
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C  INPUT : (I*4)  NDZ1V   = MAX. NUMBER OF CHARGE STATES ALLOWED IN
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C  INPUT : (I*4)  NDMET   = MAX.  NUMBER OF METASTABLE STATES ALLOWED IN
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C                           (SET EQUAL TO 1 IN THIS CASE).
C
C  OUTPUT: (I*4)  IDE     = NUMBER OF REDUCED DENSITIES READ FROM INPUT
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C  OUTPUT: (I*4)  ITE     = NO. OF REDUCED TEMPERATURES READ FROM INPUT
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C  OUTPUT: (I*4)  IZE     = NO. OF CHARGE STATES GIVEN IN THE INPUT
C                           MASTER CONDENSED/METASTABLE FILE FOR A GIVEN
C                           SEQUENCE.
C
C  OUTPUT: (R*8)  DENSR() = SET OF 'IDE' INPUT REDUCED DENSITIES (CM-3/
C                           Z1**7) READ FROM CONDENSED MASTER/METASTABLE
C                           FILE.
C  OUTPUT: (R*8)  TR()    = SET OF 'ITE'  INPUT  REDUCED  TEMPERATURES
C                           (K/Z1**2) READ FROM CONDENSED MASTER/
C                           METASTABLE FILE.
C  OUTPUT: (R*8)  ZIPT()  = SET OF 'IZE' INPUT CHARGE STATES READ FROM
C                           CONDENSED MASTER/METASTABLE FILE.
C                           (CHARGE STATE = ION CHARGE + 1 = RECOMBINING
C                            ION CHARGE)
C
C  OUTPUT: (I*4)  IME     = NO. OF METASTABLE LEVELS CONTAINED IN THE
C                           INPUT MASTER CONDENSED/METASTABLE FILE.
C                           EQUALS 1 FOR STANDARD MASTER CONDENSED FILES
C  OUTPUT: (I*4)  IMETR() =THE ORIGINAL COPDAT INDEX FOR EACH METASTABLE
C                           LEVEL. DIMENSION: METASTABLE LEVEL INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  OUTPUT: (C*12) CSTRGA()=THE DESIGNATION OF EACH METASTABLE LEVEL.
C                           DIMENSION: METASTABLE LEVEL INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  OUTPUT: (I*4)  NPRNT   = NUMBER OF PARENTS CONTAINED IN THE INPUT
C                           MASTER CONDENSED/METASTABLE FILE.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C                           (NOTE: THE NUMBER OF PARENTS CANNOT EXCEED
C                                  THE NUMBER OF METASTABLE LEVELS)
C  OUTPUT: (I*4)  IPRNT() = THE PARENT INDEX FOR INPUT PARENT.
C                           DIMENSION: PARENT/(METASTABLE LEVEL) INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  OUTPUT: (I*4)  IPSYS() = THE SPIN SYSTEM REFERENCE FOR EACH INPUT
C                           PARENT.
C                           DIMENSION: PARENT/(METASTABLE LEVEL) INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  OUTPUT: (L*4)  LSWIT   = .TRUE.  => IONISATION POTENTIALS
C                                      INCLUDED IN INPUT MASTER FILE.
C                           .FALSE. => IONISATION POTENTIALS
C                                      NOT INCLUDED IN INPUT MASTER FILE
C  OUTPUT: (R*8)  EIA()   = IONISATION POTENTIALS: ()=ION CHARGE
C                           UNITS: WAVE NUMBERS (CM-1)
C                           (= 0.0 IF NOT SET)
C
C  OUTPUT: (R*8) AIPT(,,,)= OPTION 6: TOTAL    LINE POWER COEFFICIENTS
C                           OPTION 7: SPECIFIC LINE POWER COEFFICIENTS
C                           OPTION 8: METASTABLE STATE POPULATIONS
C                           1ST DIMENSION: ELECTRON DENSITY INDEX
C                                          ('DENSR()')
C                           2ND DIMENSION: ELECTRON TEMPERATURE INDEX
C                                          ('TR()')
C                           3RD DIMENSION: CHARGE STATE INDEX
C                                          ('ZIPT()')
C                           4TH DIMENSION: METASTABLE STATE INDEX
C                           (OPTIONS 6 & 7 STANDARD FILES ALWAYS = 1)
C
C          (C*1)  CBLNK   = PARAMETER = ' '
C          (C*1)  CEQUAL  = PARAMETER = '='
C          (C*1)  CSTAR   = PARAMETER = '*'
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IPOT    = NUMBER OF IONISATION POTENTIAL VALUES
C                           PRESENT IN THE INPUT FILE.
C          (I*4)  IZ1     = CHARGE STATE READ FROM THE LINE PRECEEDING
C                           AN INPUT BLOCK FROM THE FILE.
C                           (= ION CHARGE + 1 = RECOMBINING ION CHARGE)
C          (I*4)  IMET    = METASTABLE STATE OF CURRENT DATA BLOCK BEING
C                           READ.
C          (I*4)  IBGN    = FIRST BYTE OF INTEREST IN CHARACTER 'STRING'
C          (I*4)  IEND    = LAST  BYTE OF INTEREST IN CHARACTER 'STRING'
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY INDEXES
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE INDEXS
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR ION-CHARGE INDEXES
C          (I*4)  IM      = ARRAY SUBSCRIPT USED FOR METASTABLE INDEXES
C          (I*4)  I       = GENERAL USE
C
C          (L*4)  LPART   = .TRUE.  => REQUESTED INPUT FILE: PARTIAL
C                                             OR METASTABLE POPULATION.
C                         = .FALSE. => REQUESTED INPUT FILE: STANDARD
C
C          (C*1)  C1      = GENERAL USE 1-BYTE CHARACTER STRING.
C                           (STORES METASTABLE STATE ORDER INDEX).
C          (C*5)  CPOT    = 'IPOT'
C
C          (C*80) STRING()= STRINGS  INTO WHICH LINES OF INPUT FILE ARE
C                           READ TO ENABLE ITS FORMAT TO BE ESTABLISHED
C                           AND CONTENTS READ.
C
C
C NOTE:
C          STREAM HANDLING:
C             STREAM 'IUNIT' IS USED FOR READING CONDENSED MASTER FILES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXREIA     ADAS      READ IN UNKNOWN NUMBER OF 'EIA' VALUES
C                               IF PRESENT.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    05/03/91 (DIFFERENT FROM ADAS90 VERSION - REMOVED DSN OPEN)
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  11/08/93 - HP SUMMERS - RENAMED TO XXIN80 FROM XXIN68
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE (NO CHANGES)
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C VERSION  : 1.3                          
C DATE     : 08-03-2018
C MODIFIED : Martin O'Mullane
C               - Increase length allowed from 80 to 132 characters.
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER  CBLNK        , CEQUAL       , CSTAR
C-----------------------------------------------------------------------
      PARAMETER( CBLNK = ' '  , CEQUAL = '=' , CSTAR = '*'  )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IUNIT        ,
     &           NDDEN        , NDTIN     , NDZ1V     , NDMET   ,
     &           IDE          , ITE       , IZE       , IME     , NPRNT
      INTEGER    IPOT         , IZ1       , IMET      ,
     &           IBGN         , IEND      ,
     &           ID           , IT        , IZ        , IM      , I
C-----------------------------------------------------------------------
      CHARACTER  C1*1         , CPOT*5
      CHARACTER  DSNAME*(*)
C-----------------------------------------------------------------------
      LOGICAL    LERROR       , LSWIT     , LPART
C-----------------------------------------------------------------------
      INTEGER    IMETR(NDMET)      , IPRNT(NDMET)      , IPSYS(NDMET)
C-----------------------------------------------------------------------
      REAL*8     DENSR(NDDEN)      , TR(NDTIN)         , ZIPT(NDZ1V)
      REAL*8     EIA(250)
      REAL*8     AIPT(NDDEN,NDTIN,NDZ1V,NDMET)
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(NDMET)*12
      CHARACTER  STRING(5)*132
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LERROR=.FALSE.
      LSWIT=.FALSE.
C
C-----------------------------------------------------------------------
C READ IN FIRST LINE OF FILE TO ESTABLISH ITS FORMAT: PARTIAL/STANDARD
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) STRING(1)
C-----------------------------------------------------------------------
         IF (STRING(1)(1:1).EQ.CEQUAL) THEN
C
C-----------------------------------------------------------------------
C PARTIAL MASTER CONDENSED FILE:
C-----------------------------------------------------------------------
C
            LPART=.TRUE.
            READ(IUNIT,1000) STRING(1)
            IBGN=INDEX(STRING(1),CEQUAL)+1
            READ(STRING(1)(IBGN:80),*) IME
               DO 3 I=1,5
                  READ(IUNIT,1000) STRING(I)
    3          CONTINUE
            IM=0
            NPRNT=0
               DO 4 I=1,NDMET
                  WRITE(C1,1001) I
                  iend = 0
                  IEND=INDEX(STRING(1),C1)
                  if (iend.GT.0) then
                     READ(STRING(3)(IEND:IEND),1002)  C1
                     IF ( (C1.NE.CSTAR) .AND. (C1.NE.CBLNK) ) THEN
                        IM=IM+1
                        IBGN=IEND-11
                        READ(STRING(2)(IBGN:IEND),1003) CSTRGA(IM)
                        IBGN=IEND-1
                        READ(STRING(3)(IBGN:IEND),1004) IMETR(IM)
                        READ(STRING(4)(IEND:IEND),1002) C1
                           IF ((C1.NE.CSTAR).AND.(C1.NE.CBLNK)) THEN
                              NPRNT=NPRNT+1
                              READ(C1,1001)  IPRNT(IM)
                              READ(STRING(5)(IEND:IEND),1001) IPSYS(IM)
                           ELSE
                              IPRNT(IM)=0
                              IPSYS(IM)=0
                           ENDIF
                     ENDIF
                  endif
    4          CONTINUE
C
               IF (IM.NE.IME) THEN
                  WRITE(I4UNIT(-1),2000) DSNAME ,
     &             'A METASTABLE STATE DISCREPANCY EXISTS IN THIS FILE'
                  WRITE(I4UNIT(-1),2002) IME , IM
                  WRITE(I4UNIT(-1),2001)
                  LERROR=.TRUE.
                  RETURN
               ENDIF
C
            READ(IUNIT,1005)
            READ(IUNIT,1006) IZE , ITE , IDE , CPOT
C-----------------------------------------------------------------------
         ELSE
C
C-----------------------------------------------------------------------
C STANDARD MASTER CONDENSED FILE:
C-----------------------------------------------------------------------
C
            LPART = .FALSE.
            IME   = 1
            READ(STRING(1)(1:20),1006) IZE , ITE , IDE , CPOT
         ENDIF
C-----------------------------------------------------------------------
      IF(IME.GT.NDMET) STOP ' XXIN80 ERROR: IME > NDMET. INCREASE NDMET'
      IF(IZE.GT.NDZ1V) STOP ' XXIN80 ERROR: IZE > NDZ1V. INCREASE NDZ1V'
      IF(ITE.GT.NDTIN) STOP ' XXIN80 ERROR: ITE > NDTIN. INCREASE NDTIN'
      IF(IDE.GT.NDDEN) STOP ' XXIN80 ERROR: IDE > NDDEN. INCREASE NDDEN'
C-----------------------------------------------------------------------
         IF (CPOT.NE.' ') THEN
            READ(CPOT,1006) IPOT
            IF (IPOT.GT.0) LSWIT=.TRUE.
         ENDIF
C-----------------------------------------------------------------------
      READ(IUNIT,1007) (ZIPT(IZ), IZ=1,IZE)
      READ(IUNIT,1005)
C
C-----------------------------------------------------------------------
C  READ IN IONISATION POTENTIALS IF PRESENT (UNITS: CM**3/SEC).
C  OTHERWISE CHECK THEY ARE NOT PRESENT - IF PRESENT READ THEM.
C  MISSING 'EIA()' VALUES ARE SET TO ZERO.
C-----------------------------------------------------------------------
C
         IF (LSWIT) THEN
            DO 10 IM=1,IME
             READ(IUNIT,1008) (EIA(50*(IM-1)+I),I=1,IPOT)
                DO 2 I=(IPOT+1),50
                   EIA(50*(IM-1)+I)=0.0D0
    2           CONTINUE
   10       CONTINUE
            READ(IUNIT,1005)
         ELSE
            DO 12 IM=1,IME
             CALL XXREIA( IUNIT , LSWIT , EIA(50*(IM-1)+1))
             IF(.NOT.LSWIT)GO TO 13
   12       CONTINUE
   13       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      READ(IUNIT,1009) (DENSR(ID), ID=1,IDE)
      READ(IUNIT,1009) (TR(IT),     IT=1,ITE)
C
C***********************************************************************
C READ  IN COEFFICIENTS/METASTABLE POPULATIONS  FOR  EACH  VALUE  OF  Z1
C AND LISTED IN THE HEADINGS AND FOR EACH METASTABLE STATE.
C***********************************************************************
C
         DO 5 IZ=1,IZE
C
C-----------------------------------------------------------------------
C CHECK Z1 FOR CURRENT DATA BLOCK IS THAT EXPECTED FROM THE HEADING DATA
C-----------------------------------------------------------------------
C
            READ(IUNIT,1000) STRING(1)
            IBGN=INDEX(STRING(1),'Z1=')
            IF (IBGN.EQ.0) IBGN=INDEX(STRING(1),'Z =')
            IBGN=IBGN+3
            IEND=IBGN+1
            READ(STRING(1)(IBGN:IEND),1004) IZ1
               IF (NINT(ZIPT(IZ)).NE.IZ1) THEN
                  WRITE(I4UNIT(-1),2000) DSNAME ,
     &            'Z1 BLOCK HEADING DISCREPANCY:'
                  WRITE(I4UNIT(-1),2003) NINT(ZIPT(IZ)) , IZ1
                  WRITE(I4UNIT(-1),2001)
                  LERROR=.TRUE.
                  RETURN
               ENDIF
C-----------------------------------------------------------------------
               DO 6 IM=1,IME
                     IF (LPART) THEN
                        READ(IUNIT,1010) IMET
                           IF (IMET.NE.IM) THEN
                              WRITE(I4UNIT(-1),2000) DSNAME ,
     &                        'METASTABLE BLOCK HEADING DISCREPANY FOR:'
                              WRITE(I4UNIT(-1),2004) IZ1 , IM , IMET
                              WRITE(I4UNIT(-1),2001)
                              LERROR=.TRUE.
                              RETURN
                           ENDIF
                     ENDIF
C
C-----------------------------------------------------------------------
C READ IN COEFFICIENTS
C-----------------------------------------------------------------------
C
                     DO 7 IT=1,ITE
                        READ(IUNIT,1009) (AIPT(ID,IT,IZ,IM),ID=1,IDE)
    7                CONTINUE
C-----------------------------------------------------------------------
    6          CONTINUE
C-----------------------------------------------------------------------
    5    CONTINUE
C***********************************************************************
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(A80)
 1001 FORMAT(I1)
 1002 FORMAT(A1)
 1003 FORMAT(A12)
 1004 FORMAT(I2)
 1005 FORMAT()
 1006 FORMAT(3I5,A5)
 1007 FORMAT(8(F10.5,:))
 1008 FORMAT(6F12.1)
 1009 FORMAT(8E10.2)
 1010 FORMAT(1X,I1)
C-----------------------------------------------------------------------
 2000 FORMAT(/1X,32('*'),' XXIN80 ERROR ',32('*')//
     &        1X,'INPUT MASTER CONDENSED FILE: ',A/
     &        1X,A)
 2001 FORMAT(/1X,28('*'),' END OF ERROR MESSAGE ',28('*'))
 2002 FORMAT( 1X,'EXPECTED NUMBER OF STATES (NMET): ',I1/
     &        1X,'READ NUMBER OF STATES IN HEADING: ',I1)
 2003 FORMAT( 1X,'CHARGE STATE (Z1) EXPECTED = ',I2/
     &        1X,'CHARGE STATE (Z1) READ     = ',I2)
 2004 FORMAT( 1X,'CHARGE STATE (Z1) = ',I2/
     &        1X,'EXPECTED (HEADING INDEX) = ',I1/
     &        1X,'READ     (BLOCK   INDEX) = ',I1)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
