CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxopen.for,v 1.1 2004/07/06 15:38:05 whitefor Exp $ Date $Date: 2004/07/06 15:38:05 $
CX
       SUBROUTINE XXOPEN( IUNIT, DSFULL , LEXIST )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXOPEN *********************
C
C  PURPOSE: TO INQUIRE & OPEN INPUT DATA FILE & ALLOCATE TO UNIT 'IUNIT'
C           (READ ONLY)  - IF IT DOES NOT EXISTS A MESSAGE IS  SENT  TO
C           THE SCREEN AND LEXIST IS RETURNED AS FALSE.
C
C  CALLING PROGRAM:  GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT :    (C*(*))DSFULL  = FULL INPUT DATA SET NAME (INCL. USERID)
C                              IN FORM FOR DYNAMIC ALLOCATION.
C  OUTPUT:    (L*4)  LEXIST  = .TRUE.  => DATA SETS EXISTS AND IS OPEN
C                            = .FALSE. => DATA SET DOES NOT EXIST
C
C             (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C             (C*1)  BSLASH  = '/' - MUST BE FIRST 'DSFULL'CHARACTER
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXTERM     ADAS      TERMINATES ADAS PROGRAM WITH MESSAGE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    04/03/91 - ADAS91 VERSION (INCLUDES INQUIRE)
C
C UPDATE:  07/08/91 - PE BRIDEN: ADDED ERROR HANDLING FOR OPEN ERROR.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C-----------------------------------------------------------------------
       INTEGER   I4UNIT
       INTEGER   IUNIT
C-----------------------------------------------------------------------
       LOGICAL   LEXIST
C-----------------------------------------------------------------------
       CHARACTER DSFULL*(*)   , BSLASH*1
C-----------------------------------------------------------------------
       DATA      BSLASH /'/'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  INQUIRE AND DYNAMICALLY OPEN FILE (CHECK IF '/' IS PRESENT)
C-----------------------------------------------------------------------
C
         IF (DSFULL(1:1).NE.BSLASH) THEN
            WRITE(I4UNIT(-1),1000) DSFULL
            WRITE(I4UNIT(-1),1001)
            WRITE(I4UNIT(-1),1003)
            LEXIST = .FALSE.
         ELSE
C
            INQUIRE( FILE=DSFULL , EXIST=LEXIST )
C
               IF (LEXIST) THEN
                  OPEN( UNIT=IUNIT , FILE=DSFULL , status = 'old' ,
     &                   ERR=9999  )
               ELSE
                  WRITE(I4UNIT(-1),1000) DSFULL
                  WRITE(I4UNIT(-1),1002)
                  WRITE(I4UNIT(-1),1003)
               ENDIF
C
         ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C OPEN ERROR HANDLING - SEND MESSAGE AND TERMINATE PROGRAM
C-----------------------------------------------------------------------
C
 9999 WRITE(I4UNIT(-1),1000) DSFULL
      WRITE(I4UNIT(-1),1004)
      CALL XXTERM
      RETURN
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,32('*'),' XXOPEN ERROR ',32('*')//
     &        1X,'DATA SET NAME: ',A)
 1001 FORMAT( 1X,'THIS DATA SET NAME IS NOT IN A FORM VALID FOR ',
     &           ' DYNAMIC ALLOCATION')
 1002 FORMAT( 1X,'THIS DATA SET DOES NOT EXIST/CANNOT BE LOCATED')
 1003 FORMAT(/1X,28('*'),' END OF ERROR MESSAGE ',28('*'))
 1004 FORMAT( 1X,'AN ERROR WAS ENCOUNTERED WHEN TRYING TO OPEN ',
     &           'THIS DATA-SET')
C
C-----------------------------------------------------------------------
C
      END
