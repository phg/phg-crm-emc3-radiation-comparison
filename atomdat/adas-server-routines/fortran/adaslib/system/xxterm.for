CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxterm.for,v 1.2 2004/07/06 15:40:00 whitefor Exp $ Date $Date: 2004/07/06 15:40:00 $
CX
      SUBROUTINE XXTERM
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXTERM *********************
C
C  PURPOSE: TERMINATES PROGRAM WITH MESSAGE.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE: NO VARIABLES
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    17/08/90
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      WRITE(0,1000)
      STOP
C-----------------------------------------------------------------------
 1000 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C-----------------------------------------------------------------------
      END
