C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxguid.for,v 1.1 2004/07/06 15:36:22 whitefor Exp $ Date $Date: 2004/07/06 15:36:22 $

      SUBROUTINE XXGUID( USERID )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXGUID *********************
C
C  PURPOSE: TO FETCH USER IDENTIFIER FROM UNIX
C
C  CALLING PROGRAM:
C
C  SUBROUTINE:
C
C  OUTPUT: (C*6)   USERID   = USER IDENTIFIER
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER    USERID*10
      INTEGER PIPEIN
      PARAMETER (PIPEIN=5)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A10)')USERID

      RETURN
      END
