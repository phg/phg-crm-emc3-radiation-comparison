CX  UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xx0000.for,v 1.1 2004/07/06 15:31:23 whitefor Exp $ Date $Date: 2004/07/06 15:31:23 $
CX
      SUBROUTINE XX0000
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XX0000 *********************
C
C  PURPOSE: ADAS CONFIGURATION FILE FOR SETTING MACHINE DEPENDANT
C           VARIABLES ETC.
C
C  CALLING PROGRAM: ANY MAIN ADAS PROGRAM
C
C
C  SUBROUTINE:
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IDUMP   = GENERAL USE
C
C NOTE:
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      SET UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    23/04/93
C
C MODIFIED:  6/3/94	L. JALOTA  	CHANGED OUTPUT UNIT TO 0 FOR 
C					ERROR MESSAGES SO AS NOT TO 
C					INTERFERE WITH POIPE COMMUN-
C					ICATIONS WITH IDL.
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IDUMP
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IDUMP = I4UNIT(0)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
