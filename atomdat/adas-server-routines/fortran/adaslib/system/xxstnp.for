CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxstnp.for,v 1.1 2004/07/06 15:39:45 whitefor Exp $ Date $Date: 2004/07/06 15:39:45 $
CX
      SUBROUTINE XXSTNP( IUNIT , PGLEN  , NBLOCK , NLINES )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: XXSTNP *******************
C
C  PURPOSE: CHECK WHETHER OUTPUT BLOCK WILL FIT ON CURRENT PAGE. IF NOT
C           A NEW PAGE IS INITIATED, AND LINE COUNT RESET ACCORDINGLY.
C
C  CALLING PROGRAM: GENERAL USE
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT FOR RESULTS
C  INPUT : (I*4)  PGLEN   = OUTPUT PAGE LENGTH IN LINES
C  INPUT : (I*4)  NBLOCK  = NUMBER OF LINES IN BLOCK
C  I/O   : (I*4)  NLINES  = LAST PAGE LINE WRITTEN TOO.
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    05/07/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER  IUNIT , PGLEN , NBLOCK , NLINES
C-----------------------------------------------------------------------
C
      NLINES=NLINES+NBLOCK
         IF (NLINES.GT.PGLEN) THEN
            WRITE(IUNIT,'(1H1)')
            NLINES=NBLOCK+1
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
