CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxadas.for,v 1.2 2004/07/06 15:31:34 whitefor Exp $ Date $Date: 2004/07/06 15:31:34 $
CX
      SUBROUTINE XXADAS( HEADER )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXADAS *********************
C
C  PURPOSE: ADAS ROUTINE   -  GETS AN 80 BYTE CHARACTER STRING HEADER
C           CONTAINING THE ADAS RELEASE & VERSION, THE EXECUTING PROGRAM
C           NAME & VERSION, AND THE CURRENT DATE & TIME FROM IDL VIA
C           THE PIPE.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  OUTPUT: (C*80) HEADER   = ADAS HEADER (AS DESCRIBED ABOVE)
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  ANDREW BOWEN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24/05/93
C
C-----------------------------------------------------------------------
      CHARACTER  HEADER*80
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') HEADER
C
C-----------------------------------------------------------------------
C
      RETURN
      END
