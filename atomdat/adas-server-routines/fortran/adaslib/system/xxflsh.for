CX - UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adaslib/system/xxflsh.for,v 1.1 2004/07/06 15:36:06 whitefor Exp $ Date $Date: 2004/07/06 15:36:06 $
CX
      SUBROUTINE XXFLSH(PIPEOU)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXFLSH *********************
C
C  PURPOSE: ROUTINE FOR SETTING CALL TO "FLUSH" COMMAND DEPENDING ON
C	    OPERATING SYSTEM SPCIFICS.
C
C  CALLING PROGRAM: ANY ADAS PROGRAM WHICH COMMUNICATES VIA A PIPE 
C		    PARTICULARLY TO IDL INTERFACE ROUTINES.
C
C
C  SUBROUTINE:
C
C          (I*4)  PIPEOU  = UNIT NUMBER OF PIPE USED 
C
C NOTE:
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:     6/03/95
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    PIPEOU
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C MAKE CALL TO FLUSH COMMAND
C-----------------------------------------------------------------------
      CALL FLUSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
