CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxmcpy.for,v 1.1 2004/07/06 15:37:17 whitefor Exp $ Date $Date: 2004/07/06 15:37:17 $
CX
      SUBROUTINE XXMCPY( NR , NC , A , B )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXVADD *********************
C
C  PURPOSE: COPIES ONE MATRIX TO ANOTHER.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT : (I*4)  NR      = NUMBER OF ROWS IN MATRICES 'A' AND 'B'.
C  INPUT : (I*4)  NC      = NUMBER OF COLUMNS IN MATRICES 'A' AND 'B'.
C  INPUT : (R*8)  A(,)    = INPUT MATRIX.
C                           1ST DIMENSION: NR
C                           2ND DIMENSION: NC
C
C  OUTPUT: (R*8)  B(,)    = OUTPUT MATRIX.
C                           1ST DIMENSION: NR
C                           2ND DIMENSION: NC
C
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    11/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER   NR      , NC
      INTEGER   I       , J
C----------------------------------------------------------------------
      REAL*8    A(NR,NC)  , B(NR,NC)
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
C
      DO 1 I = 1 , NR
         DO 2 J = 1 , NC
            B(I,J) = A(I,J)
    2    CONTINUE
    1 CONTINUE
C
C----------------------------------------------------------------------
C
      RETURN
      END
