CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/ee3.for,v 1.2 2007/04/23 13:40:45 allan Exp $ Date $Date: 2007/04/23 13:40:45 $
CX
       FUNCTION EE3(X)                                                  
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: EE3 **************************
C
C PURPOSE: EVALUATES EXP(X)E3(X) WHERE E3 IS THE 1ST EXPONENTIAL
C          INTEGRAL
C
C CALLING PROGRAMS: GENERAL
C
C INPUT:  (R*8)  X       = INDEPENDENT VARIABLE
C
C OUTPUT: (R*8)  EE3     = EXP(X)E3(X) 
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          EE2        ADAS     EVALUATES 2ND EXPONENTIAL INTEGRAL
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 17-4-07
C MODIFIED: HUGH SUMMERS
C           - COMPLETED COMMENT BLOCK DESCRIPTION
C
C-----------------------------------------------------------------------
C
       IF(X-30.0D0)1,1,2                                                
    1  EE3=0.5D0*(1.0D0-X*EE2(X))                                       
       GO TO 3                                                          
    2  X1=1.0D0/X                                                       
       EE3=X1*(1.0D0-X1*(3.0D0-X1*(12.0D0-X1*(60.0D0-X1*(360.0D0-X1*    
     1(2520.0D0-X1*20160.0D0))))))                                      
    3  RETURN                                                           
      END                                                               
