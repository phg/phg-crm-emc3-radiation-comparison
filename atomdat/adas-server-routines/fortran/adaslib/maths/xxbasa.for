      subroutine xxbasa(a , ia , b , ib , base , c  , ic)
      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxbasa *********************
c
c  Purpose : to add two numbers, given in the form a*base^ia and
c            b*base^ib, where the base must be the same.
c
c  Calling program: general use
c
c  Subroutine:
c
c  input : (r*8)   a        = mantissa of number
c  input : (i*4)   ia       = characteristic of number
c  input : (r*8)   b        = mantissa of number
c  input : (i*4)   ib       = characteristic of number
c  input : (r*8)   base     = base of number system
c
c  output: (r*8)   c        = mantissa of answer
c  output: (i*4)   ic       = characteristic of answer
c
c Routines:
c          Routine    Source    Brief description
c          ------------------------------------------------------------
c          xxbass     ADAS      To scale a number to a base
c          i4unit     ADAS      Fetch unit number for output of messages
c
c Author:  Hugh Summers
c          JET K1/1/61
c          Tel. 01235-46-4459
c Date:    14-05-2008
c
c
c Version  : 1.1
c Date     : 14-05-2008
c Modified : Hugh Summers
c              - first version.
c
c Version  : 1.2
c Date     : 17-02-2009
c Modified : Hugh Summers
c              - Correct documentation
c              - Added capital letters
c-----------------------------------------------------------------------
      integer    i4unit
      integer    ia       , ib       , ic      , ix
c-----------------------------------------------------------------------
      real*8     a        , b        , c       , x      , base
c-----------------------------------------------------------------------
      if (base.le.0.0d0) then
          write(i4unit(-1),1000)base
          write(i4unit(-1),1001)
          return
      endif

      if(a.eq.0.0d0) then
         x=b
         ix=ib
      elseif(b.eq.0.0d0) then
         x=a
         ix=ib
      else
         if (ia.gt.ib) then
             ix=ia
             x=a+b*base**(ib-ia)
         elseif(ia.lt.ib) then
             ix=ib
             x = b+a*base**(ia-ib)
         else
             x=a+b
             ix=ia
         endif
      endif


      call xxbass( x , ix , c , ic , base)

      return
c
c-----------------------------------------------------------------------
c
 1000 format(1x,31('*'),' xxbasa error ',30('*')//
     &       1x,'faulty in base = ',f12.7)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c

      end
