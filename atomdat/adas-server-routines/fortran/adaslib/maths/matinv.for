CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/matinv.for,v 1.2 2007/04/11 13:01:52 allan Exp $ Date $Date: 2007/04/11 13:01:52 $
CX
      SUBROUTINE MATINV(A,N,B,M,DETERM)                        
      IMPLICIT REAL*8 (A-H,O-Z)                              
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: MATINV ************************
C
C PURPOSE: MATRIX INVERSION WITH ACCOMPANYING SOLUTION
C          OF LINEAR EQUATIONS   
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 12-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
      DIMENSION IPIVOT(30),A(30,30),B(30),INDEX(30,2),PIVOT(30) 
C     INITIALIZATION                                      
   10 DETERM=1.0                            
   15 DO 20 J=1,N                          
   20 IPIVOT(J)=0                         
   30 DO 550 I=1,N                       
C     SEARCH FOR PIVOT ELEMENT          
   40 AMAX=0.0                         
   45 DO 105 J=1,N                    
   50 IF (IPIVOT(J)-1) 60, 105, 60   
   60 DO 100 K=1,N                  
   70 IF (IPIVOT(K)-1) 80, 100, 740                                   
   80 IF ( DABS(AMAX)- DABS(A(J,K))) 85, 100, 100                    
   85 IROW=J                                                        
   90 ICOLUM=K                                                     
   95 AMAX=A(J,K)                                                 
  100 CONTINUE                                                   
  105 CONTINUE                                                  
  110 IPIVOT(ICOLUM)=IPIVOT(ICOLUM)+1                          
C     INTERCHANGE ROWS TO PUT PIVOT ELEMENT ON DIAGONAL       
  130 IF (IROW-ICOLUM) 140, 260, 140                         
  140 DETERM=-DETERM                                        
  150 DO 200 L=1,N                                         
  160 SWAP=A(IROW,L)                         
  170 A(IROW,L)=A(ICOLUM,L)                 
  200 A(ICOLUM,L)=SWAP                     
  205 IF(M) 260, 260, 220                 
  220 SWAP = B(IROW)                     
  230 B(IROW) = B(ICOLUM)               
  250 B(ICOLUM) = SWAP                 
  260 INDEX(I,1)=IROW                 
  270 INDEX(I,2)=ICOLUM              
  310 PIVOT(I)=A(ICOLUM,ICOLUM)     
C     DIVIDE PIVOT ROW BY PIVOT ELEMENT                         
  330 A(ICOLUM,ICOLUM)=1.0                                     
  340 DO 350 L=1,N                                            
  350 A(ICOLUM,L)=A(ICOLUM,L)/PIVOT(I)                      
  355 IF(M) 380, 380, 370                                  
  370 B(ICOLUM) = B(ICOLUM)/PIVOT(I)                      
C     REDUCE NON-PIVOT ROWS                              
  380 DO 550 L1=1,N                          
  390 IF(L1-ICOLUM) 400, 550, 400              
  400 T=A(L1,ICOLUM)                            
  420 A(L1,ICOLUM)=0.0                           
  430 DO 450 L=1,N                                
  450 A(L1,L)=A(L1,L)-A(ICOLUM,L)*T                
  455 IF(M) 550, 550, 500                           
  500 B(L1) = B(L1) - B(ICOLUM)*T                    
  550 CONTINUE                                        
C     INTERCHANGE COLUMNS                              
  600 DO 710 I=1,N                                      
  610 L=N+1-I                                            
  620 IF (INDEX(L,1)-INDEX(L,2)) 630, 710, 630            
  630 JROW=INDEX(L,1)                       
  640 JCOLUM=INDEX(L,2)                      
  650 DO 705 K=1,N                            
  660 SWAP=A(K,JROW)                           
  670 A(K,JROW)=A(K,JCOLUM)                     
  700 A(K,JCOLUM)=SWAP                           
  705 CONTINUE                                    
  710 CONTINUE                                     
  740 RETURN                                        
      END                                            
