CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/matin1.for,v 1.1 2004/07/06 14:20:23 whitefor Exp $ Date $Date: 2004/07/06 14:20:23 $
CX
      SUBROUTINE MATIN1(N,A,B)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  *******************FORTRAN77 SUBROUTINE: MATIN1 *********************
C
C  PURPOSE: MATRIX INVERSION
C
C  CALLING PROGRAM:
C           LSTSQ
C
C  INPUT:
C           N  NUMBER OF DATA POINTS
C
C  I/O      (R*8)  A(,) 'N' BY 'N' MATRIX
C                            INPUT : MATRIX TO BE INVERTED
C                            OUTPUT: INVERTED MATRIX
C           (R*8)  B()  = A.X = B
C                            INPUT : RIGHT HAND SIDE VECTOR 'B'
C                            OUTPUT: SOLUTION VECTOR 'X'
C
C  DATE: 02/07/95				VERSION 1.1
C  WRITTEN: A.LANZAFAME & D.H.BROOKS, UNIV.OF STRATHCLYDE
C           CONVERTED FROM BURGESS BBC BASIC
C
C-----------------------------------------------------------------------
      INTEGER N,I,J,K
C-----------------------------------------------------------------------
      REAL*8 A(N,N),B(N)
      REAL*8 U,T
C-----------------------------------------------------------------------
      IF (N.EQ.1) THEN
          B(1)=B(1)/A(1,1)
          RETURN
      ENDIF
      DO I=1, N-1
          T=DABS(A(I,I))
          J=I
          DO K=I+1, N
              U=DABS(A(K,I))
              IF (U.GT.T) THEN
                  T=U
                  J=K
              ENDIF
          ENDDO
          IF (J .GT. I) THEN
              DO K=I, N
                  T=A(I,K)
                  A(I,K)=A(J,K)
                  A(J,K)=T
              ENDDO
              T=B(I)
              B(I)=B(J)
              B(J)=T
          ENDIF
          T=1./A(I,I)
          DO J=I+1, N
              U=T*A(J,I)
              DO K=I+1, N
                  A(J,K)=A(J,K)-U*A(I,K)
              ENDDO
              B(J)=B(J)-U*B(I)
          ENDDO
      ENDDO
      B(N)=B(N)/A(N,N)
      DO I=N-1, 1, -1
          T=B(I)
          DO J=I+1, N
              T=T-A(I,J)*B(J)
          ENDDO
          B(I)=T/A(I,I)
      ENDDO
C
C-----------------------------------------------------------------------
C
      RETURN
      END

