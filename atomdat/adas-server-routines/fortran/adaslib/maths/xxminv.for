CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxminv.for,v 1.6 2009/02/24 13:51:18 afoster Exp $ Date $Date: 2009/02/24 13:51:18 $
CX
      SUBROUTINE XXMINV( LSOLVE , NDMAX , NDIM   ,
     &                   A      , B     , DINT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXMINV *********************
C
C  PURPOSE: MATRIX INVERSION WITH ACCOMPANYING SOLUTION OF LINEAR
C           EQUATIONS IF REQUESTED.
C
C
C  CALLING PROGRAM: GENERAL USE
C
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LSOLVE   = .TRUE.  => SOLVES  SET  OF   'N'   LINEAR
C                                       EQUATIONS A.X = B, WHERE A,X,B
C                                       ARE MATRICES/VECTORS AND:
C                                       A = 'A(,)' ON INPUT
C                                       X = 'B()'  ON OUTPUT
C                                       B = 'B()'  ON INPUT
C                            .FALSE. => DOES NOT SOLVE ANY EQUATIONS
C                                       AND 'B()' IS NOT USED.
C
C  INPUT : (I*4)  NDMAX    = MAXIMUM VALUE OF 'N' ALLOWED. I.E.
C                            PHYSICAL DIMENSION OF INPUT MATRICES.
C  INPUT : (I*4)  NDIM     = DIMENSIONALITY 'N' OF MATRIX/EQUATIONS
C                            TO BE SOLVED.
C
C  I/O   : (R*8)  A(,)     = 'N' BY 'N' MATRIX
C                            INPUT : MATRIX TO BE INVERTED
C                            OUTPUT: INVERTED MATRIX
C  I/O   : (R*8)  B()      = USED ONLY IF 'LSOLVE=.TRUE.'WHEN THE SET
C                            OF 'N' LINEAR EQUATIONS ARE TO BE SOLVED.
C                            A.X = B
C                            INPUT : RIGHT HAND SIDE VECTOR 'B'
C                            OUTPUT: SOLUTION VECTOR 'X'
C
C  OUTPUT: (R*8)  DINT     = +1 OR -1 DEPENDING ON WHETHER THE  NUMBER
C                            OF  ROW  INTERCHANGES  WAS  EVEN  OR  ODD,
C                            RESPECTIVELY.
C
C          (I*4)  NLMAX    = PARAMETER: MUST BE >= 'NDMAX'
C
C          (I*4)  LROW     = ROW OF INPUT 'A(*,)' CONTAINING LARGEST
C                            NON-ZERO ELEMENT.
C          (I*4)  LCOLUM   = COLUMN OF INPUT 'A(,*)' CONTAINING LARGEST
C                            NON-ZERO ELEMENT.
C                            'LROW' & 'LCOLUM' => PIVOT POSITION IN A(,)
C          (I*4)  I1       = ARRAY INDICES: 1ST LEVEL
C          (I*4)  I2       = ARRAY INDICES: 2ND LEVEL
C          (I*4)  I3       = ARRAY INDICES: 3RD LEVEL
C
C          (R*8)  DMAX     = LARGEST NON-ZERO ELEMENT OF 'A(,)',
C                            AND EQUALS 'ABS(A(LROW,LCOLUM))'
C          (R*8)  SWAP     = TEMPORARAY STORAGE WHEN INTERCHANGING
C                            ROWS/COLUMNS.
C          (R*8)  TVAL     = TEMPORARY STORAGE OF VALUES IN NON-PIVOT
C                            ROWS.
C
C          (I*4)  IPIVOT() = NUMBER OF TIMES PIVOT FOUND IN COLUMN
C                            DIMENSION: COLUMN INDEX
C          (I*4)  INDX(,1) = ROW OF PIVOT IN 'A(,)'
C                            DIMENSION: PIVOT INDEX
C          (I*4)  INDX(,2) = COLUMN OF PIVOT IN 'A(,)'
C                            DIMENSION: PIVOT INDEX
C
C          (R*8)  PIVOT()  = 'DMAX' FOR PIVOT: DIMENSION PIVOT INDEX
C
C
C ROUTINES: NONE
C
C NOTE: THIS IS A STRUCTURED VERSION OF THE SUBROUTINE 'MATINV' WRITTEN
C       BY HUGH SUMMERS.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    31/08/90
C
C UPDATE:  15/05/92 - PE BRIDEN - INCREASED NLMAX FROM 50 -> 75
C
C UPDATE:  26/06/92 - PE BRIDEN - INCREASED NLMAX FROM 75 -> 200
C
C VERSION: 1.2						DATE: 04-05-2000
C MODIFIED: RICHARD MARTIN
C		CHANGED DOCUMENTATION FOR NDIM.
C
C VERSION  : 1.3                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION: 1.4                                 DATE: 02-03-2003
C MODIFIED: Martin O'Mullane & Allan Whiteford
C		- Increased NLMAX to 801 and improved error message.
C
C VERSION: 1.5                                 DATE: 29-05-2003
C MODIFIED: Martin O'Mullane
C		- Increased NLMAX to 1201.
C
C VERSION: 1.6                                 DATE: 08-12-2008
C MODIFIED: Adam Foster
C		- Increased NLMAX to 3501 to bring in line with offline
C                 version 1.8
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NLMAX
C-----------------------------------------------------------------------
      PARAMETER( NLMAX = 4001 )
C-----------------------------------------------------------------------
      INTEGER    NDMAX          , NDIM
      INTEGER    LROW           , LCOLUM        ,
     &           I1             , I2            , I3   , I4UNIT
C-----------------------------------------------------------------------
      REAL*8     DINT
      REAL*8     DMAX           , SWAP          , TVAL
C-----------------------------------------------------------------------
      LOGICAL    LSOLVE
C-----------------------------------------------------------------------
      INTEGER    IPIVOT(NLMAX)  , INDX(NLMAX,2)
C-----------------------------------------------------------------------
      REAL*8     A(NDMAX,NDMAX) , B(NDMAX)
      REAL*8     PIVOT(NLMAX)
C-----------------------------------------------------------------------
      IF (NLMAX.LT.NDMAX) THEN
         WRITE(I4UNIT(-1),*)
     &         'XXMINV ERROR: NDMAX > NLMAX. INCREASE NLMAX'
         WRITE(I4UNIT(-1),*)'NDMAX : ', NDMAX
         WRITE(I4UNIT(-1),*)'NLMAX : ', NLMAX
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C INITIALIZATION
C-----------------------------------------------------------------------
C
      DINT = 1.0D0
C-----------------------------------------------------------------------
         DO 1 I1=1,NDIM
            IPIVOT(I1) = 0
    1    CONTINUE
C
C***********************************************************************
C MAIN BODY OF SUBROUTINE
C***********************************************************************
C
         DO 2 I1=1,NDIM
C
C-----------------------------------------------------------------------
C SEARCH FOR PIVOT ELEMENT
C-----------------------------------------------------------------------
C
            DMAX = 0.0D0
C-----------------------------------------------------------------------
               DO 3 I2=1,NDIM
                  IF (IPIVOT(I2).NE.1) THEN
C-----------------------------------------------------------------------
                     DO 4 I3=1,NDIM
                           IF     (IPIVOT(I3).GT.1) THEN
                              RETURN
                           ELSEIF (IPIVOT(I3).LT.1) THEN
                                 IF (DABS(DMAX).LT.DABS(A(I2,I3))) THEN
                                    LROW   = I2
                                    LCOLUM = I3
                                    DMAX   = A(I2,I3)
                                 ENDIF
                           ENDIF
    4                CONTINUE
C-----------------------------------------------------------------------
                  ENDIF
    3          CONTINUE
C-----------------------------------------------------------------------
            IPIVOT(LCOLUM) = IPIVOT(LCOLUM)+1
C
C-----------------------------------------------------------------------
C INTERCHANGE ROWS TO PUT PIVOT ELEMENT ON DIAGONAL
C-----------------------------------------------------------------------
C
               IF (LROW.NE.LCOLUM) THEN
                  DINT = -DINT
C-----------------------------------------------------------------------
                     DO 5 I2=1,NDIM
                        SWAP         = A(LROW,I2)
                        A(LROW,I2)   = A(LCOLUM,I2)
                        A(LCOLUM,I2) = SWAP
    5                CONTINUE
C-----------------------------------------------------------------------
                     IF (LSOLVE) THEN
                        SWAP      = B(LROW)
                        B(LROW)   = B(LCOLUM)
                        B(LCOLUM) = SWAP
                     ENDIF
               ENDIF
C-----------------------------------------------------------------------
            INDX(I1,1) = LROW
            INDX(I1,2) = LCOLUM
            PIVOT(I1)  = A(LCOLUM,LCOLUM)
C
C-----------------------------------------------------------------------
C DIVIDE PIVOT ROW BY PIVOT ELEMENT
C-----------------------------------------------------------------------
C
            A(LCOLUM,LCOLUM) = 1.0D0
C-----------------------------------------------------------------------
               DO 6 I2=1,NDIM
                  A(LCOLUM,I2) = A(LCOLUM,I2)/PIVOT(I1)
    6          CONTINUE
C-----------------------------------------------------------------------
            IF (LSOLVE) B(LCOLUM) = B(LCOLUM)/PIVOT(I1)
C
C-----------------------------------------------------------------------
C REDUCE NON-PIVOT ROWS
C-----------------------------------------------------------------------
C
               DO 7 I2=1,NDIM
                  IF (I2.NE.LCOLUM) THEN
                     TVAL         = A(I2,LCOLUM)
                     A(I2,LCOLUM) = 0.0D0
C-----------------------------------------------------------------------
                        DO 8 I3=1,NDIM
                           A(I2,I3) = A(I2,I3)-A(LCOLUM,I3)*TVAL
    8                   CONTINUE
C-----------------------------------------------------------------------
                     IF (LSOLVE) B(I2) = B(I2)-B(LCOLUM)*TVAL
                  ENDIF
    7          CONTINUE
C-----------------------------------------------------------------------
    2    CONTINUE
C***********************************************************************
C
C-----------------------------------------------------------------------
C INTERCHANGE COLUMNS
C-----------------------------------------------------------------------
C
         DO 9 I1=NDIM,1,-1
            IF (INDX(I1,1).NE.INDX(I1,2)) THEN
               LROW   = INDX(I1,1)
               LCOLUM = INDX(I1,2)
C-----------------------------------------------------------------------
                  DO 10 I2=1,NDIM
                     SWAP         = A(I2,LROW)
                     A(I2,LROW)   = A(I2,LCOLUM)
                     A(I2,LCOLUM) = SWAP
   10             CONTINUE
C-----------------------------------------------------------------------
            ENDIF
    9    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
