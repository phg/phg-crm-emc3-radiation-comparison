C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxravg.for,v 1.1 2004/07/06 15:38:39 whitefor Exp $ Date $Date: 2004/07/06 15:38:39 $
C
       subroutine xxravg(y1, y2, y3, tolval, ravg, ytest, lfail)

       implicit real*8(a-h,o-z)

C Take tolval difference in the running average test
       
       logical lfail
        
       lfail = .FALSE.
       
       ravg = (y1 + y2 + y3)/3.0
       val  = 1.0 - min(ytest,ravg)/max(ytest,ravg)
       if (val.gt.tolval) lfail = .TRUE.
         
       end
