CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxmerg.for,v 1.2 2006/05/19 16:58:24 mog Exp $ Date $Date: 2006/05/19 16:58:24 $
CX
       subroutine xxmerg( g1   , n1  ,
     &                    g2   , n2   , 
     &                    grid , num
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C ROUTINE: xxmerg
C
C PURPOSE: Merges two grids and eliminates any duplicate entries.
C
C CALLING PROGRAM: general use
C
C  input : (i*4)  ndeng    = maximum number of energies in adf35 file.
C  input : (i*4)  ndedge   = maximum number of energy edges in adf35 file.
C  input : (i*4)  ieng     = actual number of energies.
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          d1merg     NETLIB    Merge two strings of ascending numbers.
C          dcopy      NETLIB    Copies a vector, x, to a vector, y.
C
C  NOTES: The netlib routines form part of this file.
C
C  VERSION  : 1.1                          
C  DATE     : 01-08-2003
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C  VERSION  : 1.2
C  DATE     : 16-02-2005
C  MODIFIED : Martin O'Mullane
C              - Increase number of steps, initialize grid and perform
C                more checks when filling grid().
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       integer   nstep
C-----------------------------------------------------------------------
       parameter (nstep = 21000)
C-----------------------------------------------------------------------
       integer   n1          , n2      , num
       integer   i           , i1      , i2      , i3
C-----------------------------------------------------------------------
       real*8    g1(*)       , g2(*)   , grid(*)
       real*8    tcos(nstep*3)
C-----------------------------------------------------------------------
       integer   itag(nstep)
C-----------------------------------------------------------------------

       do i = 1, nstep
         grid(i) = 0.0
       end do

       i1 = 0
       
       do i = 1, n1
         tcos(i) = g1(i)
       end do
       
       i2 = n1
       
       do i = 1, n2
         tcos(n1+i) = g2(i)
       end do

       i3 = n1 + n2

       call d1merg(tcos, i1, n1, i2, n2, i3)

       num = 1
       grid(num) = tcos(i3+1)
       do i = 2, i3
          if (num.ge.1.and.num.le.nstep) then
             if (tcos(i3+i).NE.grid(num)) then
                num = num + 1
                grid(num) = tcos(i3+i)
             endif
          endif
       end do
       
       
       end
       
       
C BEGINNING OF SUBROUTINE d1merg


      SUBROUTINE D1MERG (TCOS, I1, M1, I2, M2, I3)
C***BEGIN PROLOGUE  D1MERG
C***SUBSIDIARY
C***PURPOSE  Merge two strings of ascending double precision numbers.
C***LIBRARY   SLATEC
C***TYPE      DOUBLE PRECISION (S1MERG-S, D1MERG-D, CMERGE-C, I1MERG-I)
C***AUTHOR  Boland, W. Robert, (LANL)
C           Clemens, Reginald, (PLK)
C***DESCRIPTION
C
C   This subroutine merges two ascending strings of numbers in the
C   array TCOS.  The first string is of length M1 and starts at
C   TCOS(I1+1).  The second string is of length M2 and starts at
C   TCOS(I2+1).  The merged string goes into TCOS(I3+1).
C
C   This routine is currently unused, but was added to complete
C   the set of routines S1MERG and C1MERG (both of which are used).
C
C***ROUTINES CALLED  DCOPY
C***REVISION HISTORY  (YYMMDD)
C   910819  DATE WRITTEN
C***END PROLOGUE  D1MERG
      INTEGER I1, I2, I3, M1, M2
      DOUBLE PRECISION TCOS(*)
C
      INTEGER J1, J2, J3
C
C***FIRST EXECUTABLE STATEMENT  D1MERG
      IF (M1.EQ.0 .AND. M2.EQ.0) RETURN
C
      IF (M1.EQ.0 .AND. M2.NE.0) THEN
         CALL DCOPY (M2, TCOS(I2+1), 1, TCOS(I3+1), 1)
         RETURN
      ENDIF
C
      IF (M1.NE.0 .AND. M2.EQ.0) THEN
         CALL DCOPY (M1, TCOS(I1+1), 1, TCOS(I3+1), 1)
         RETURN
      ENDIF
C
      J1 = 1
      J2 = 1
      J3 = 1
C
   10 IF (TCOS(I1+J1) .LE. TCOS(I2+J2)) THEN
         TCOS(I3+J3) = TCOS(I1+J1)
         J1 = J1+1
         IF (J1 .GT. M1) THEN
            CALL DCOPY (M2-J2+1, TCOS(I2+J2), 1, TCOS(I3+J3+1), 1)
            RETURN
         ENDIF
      ELSE
         TCOS(I3+J3) = TCOS(I2+J2)
         J2 = J2+1
         IF (J2 .GT. M2) THEN
            CALL DCOPY (M1-J1+1, TCOS(I1+J1), 1, TCOS(I3+J3+1), 1)
            RETURN
         ENDIF
      ENDIF
      J3 = J3+1
      GO TO 10
      END




C BEGINNING OF SUBROUTINE dcopy


      subroutine  dcopy(n,dx,incx,dy,incy)
c
c     copies a vector, x, to a vector, y.
c     uses unrolled loops for increments equal to one.
c     jack dongarra, linpack, 3/11/78.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      real*8  dx(*),dy(*)
      integer i,incx,incy,ix,iy,m,mp1,n
c
      if(n.le.0)return
      if(incx.eq.1.and.incy.eq.1)go to 20
c
c        code for unequal increments or equal increments
c          not equal to 1
c
      ix = 1
      iy = 1
      if(incx.lt.0)ix = (-n+1)*incx + 1
      if(incy.lt.0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dy(iy) = dx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
c
c        code for both increments equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,7)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dy(i) = dx(i)
   30 continue
      if( n .lt. 7 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,7
        dy(i) = dx(i)
        dy(i + 1) = dx(i + 1)
        dy(i + 2) = dx(i + 2)
        dy(i + 3) = dx(i + 3)
        dy(i + 4) = dx(i + 4)
        dy(i + 5) = dx(i + 5)
        dy(i + 6) = dx(i + 6)
   50 continue
      return
      end

