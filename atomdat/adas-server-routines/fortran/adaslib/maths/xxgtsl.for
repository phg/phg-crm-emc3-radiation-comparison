CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxgtsl.for,v 1.2 2019/11/23 18:09:52 mog Exp $ Date $Date: 2019/11/23 18:09:52 $
CX
       SUBROUTINE XXGTSL( N  , D  , DU , DL , B  , IFAIL )
       IMPLICIT NONE
C----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXGTSL *********************
C
C  PURPOSE: GIVEN A GENERAL TRIDIAGONAL MATRIX AND A RIGHT HAND SIDE
C           WILL FIND THE SOLUTION OF THE ASSOCIATED SYSTEM OF LINEAR
C           EQUATIONS.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C  INPUT :    (I*4)  N       = ORDER OF TRIDIAGONAL MATRIX
C
C  INPUT :    (R*8)  DL()    = SUBDIAGONAL OF THE MATRIX.  DL(2) THROUGH
C                              DL(N) SHOULD CONTAIN THE SUBDIAGONAL. ON
C                              OUTPUT DL IS DESTROYED
C  INPUT :    (R*8)  D()     = DIAGONAL OF THE MATRIX.  ON OUTPUT D
C                              IS DESTROYED
C  INPUT :    (R*8)  DU()    = SUPERDIAGONAL OF THE MATRIX. DU(2) THROUGH
C                              DU(N) SHOULD CONTAIN THE SUPERDIAGONAL.
C                              OUTPUT DU IS DESTROYED
C  INPUT :    (R*8)  B()     = RIGHT HAND SIDE VECTOR
C
C  OUTPUT:    (R*8)  B()     = SOLUTION VECTOR
C
C  OUTPUT:    (I*4)  IFAIL   = 0 - NORMAL VALUE
C                            = K - IF THE KTH PIVOT ELEMENT BECOMES
C                                  BECOMES EXACTLY ZERO.  THE ROUTINE
C                                  RETURNS WHEN THIS IS THE CASE.
C
C             (I*4)  K       = GENERAL INTEGER
C             (I*4)  KB      = GENERAL INTEGER
C             (I*4)  KP1     = K+1
C             (I*4)  NM1     = N-1
C             (I*4)  NM2     = N-2
C             (R*8)  T       = GENERAL REAL
C
C ROUTINES:  NONE
C
C NOTE:
C            TRANSCRIBED FROM LINPACK PUBLICATION.  VERSION DATED
C            08/14/78, JACK DONGARRA, ARGONNE NATIONAL LABORATORY
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    04/07/95
C
C DATE:    10/07/95			VERSION 1.1
C UPDATE:  TIM HAMMOND, TESSELLA SUPPORT SERVICES PLC
C	   - UNIX PORT
C
C VERSION: 1.2
C DATE   : 03-05-2019
C UPDATE : Martin O'Mullane
C           - Explicitly calculate the n=2 case to avoid an 
C             out of bounds error.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       INTEGER  N      , IFAIL
       INTEGER  K      , KB     , KP1    , NM1    , NM2
C-----------------------------------------------------------------------
       REAL*8   T      , x      , y
C-----------------------------------------------------------------------
       REAL*8   DL(N)  , D(N)   , DU(N)  , B(N)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  The trivial n=2 case
C-----------------------------------------------------------------------
       ifail = 0
       if (n.eq.2) then
          
          y = (b(1)/d(1) - b(2)/dl(2)) / (du(2)/d(1) - d(2)/dl(2))
          x = (b(1) - du(2) * y) / d(1)

          b(1) = x
          b(2) = y
          
          return
          
       endif

C-----------------------------------------------------------------------
       IFAIL = 0
       DL(1) = D(1)
       NM1   = N-1
       IF (NM1 .LT. 1)  GO TO 40
           D(1) = DU(2)
           DU(2) = 0.0D0
C          DU(N) = 0.0D0
C
           DO 30 K=1, NM1
             KP1=K+1
C
C-----------------------------------------------------------------------
C  FIND THE LARGEST OF THE TWO ROWS
C-----------------------------------------------------------------------
C
             IF (DABS(DL(KP1)) .LT. DABS(DL(K))) GO TO 10
C
C-----------------------------------------------------------------------
C  INTERCHANGE ROW
C-----------------------------------------------------------------------
C
                 T      = DL(KP1)
                 DL(KP1) = DL(K)
                 DL(K)  = T
                 T      = D(KP1)
                 D(KP1) = D(K)
                 D(K)   = T
                 T      = DU(KP1+1)
                 DU(KP1+1) = DU(K+1)
                 DU(K+1)  = T
                 T      = B(KP1)
                 B(KP1) = B(K)
                 B(K)   = T
   10        CONTINUE
C
C-----------------------------------------------------------------------
C  ZERO ELEMENTS
C-----------------------------------------------------------------------
C
             IF (DL(K) .NE. 0.0D0) GO TO 20
                 IFAIL = K
                 GO TO 100
   20        CONTINUE
             T      = -DL(KP1)/DL(K)
             DL(KP1) = D(KP1) + T*D(K)
C
             IF (KP1 .LT. N) THEN
                 D(KP1) = DU(KP1+1) + T*DU(K+1)
                 DU(KP1+1) = 0.0D0
             ELSE
                 D(KP1) = T*DU(K+1)
             ENDIF
C
             B(KP1) = B(KP1) + T*B(K)
   30      CONTINUE
   40    CONTINUE
         IF (DL(N) .NE. 0.0D0) GO TO 50
             IFAIL = N
         GO TO 90
   50    CONTINUE
C
C-----------------------------------------------------------------------
C  BACK SOLVE
C-----------------------------------------------------------------------
C
            NM2 = N-2
            B(N) = B(N)/DL(N)
            IF (N .EQ. 1) GO TO 80
                B(NM1) = (B(NM1)-D(NM1)*B(N))/DL(NM1)
                IF (NM2 .LT. 1) GO TO 70
                DO 60 KB = 1,NM2
                  K = NM2 - KB + 1
                  B(K) =(B(K)-D(K)*B(K+1)-DU(K+1)*B(K+2))/DL(K)
   60           CONTINUE
   70       CONTINUE
   80    CONTINUE
   90  CONTINUE
  100 CONTINUE
C
      RETURN
      END
