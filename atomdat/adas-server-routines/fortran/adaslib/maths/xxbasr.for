      subroutine xxbasr(a , ia , base_a, b, ib , base_b)
      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxbasr *********************
c
c  purpose : to rebase a number, given in the form a*base_a^ia to the
c            form b*base_b^ib.
c
c  calling program: general use
c
c  subroutine:
c
c  input : (r*8)   a        = mantissa of number
c  input : (i*4)   ia       = characteristic of number
c  input : (r*8)   base_a   = base of number system
c  input : (r*8)   base_b   = base of new number system
c
c  output: (r*8)   b        = mantissa of number in new system
c  output: (i*4)   ib       = characteristic of number in new system
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          xxbass     adas      to scale a number to a base
c          i4unit     adas      fetch unit number for output of messages
c
c author:  Hugh Summers
c          JET K1/1/61
c          Tel. 01235-46-4459
c date:    07-05-2008
c
c
c version  : 1.1
c date     : 07-05-2008
c modified : Hugh Summers
c              - first version.
c-----------------------------------------------------------------------
      integer    i4unit
      integer    ia       , ib       , ic
c-----------------------------------------------------------------------
      real*8     a        , b        , c       ,
     &           base_a   , base_b
      real*8     x
c-----------------------------------------------------------------------
      if (base_b.le.0.0d0) then
          write(i4unit(-1),1000)base_b
          write(i4unit(-1),1001)
          return
      endif

      x  = float(ia)*log(base_a)/log(base_b)
      ic = int(x)
      c  = a*base_b**(x-ic)

      call xxbass( c , ic , b , ib , base_b )

      return
c
c-----------------------------------------------------------------------
c
 1000 format(1x,31('*'),' xxbasr error ',30('*')//
     &       1x,'faulty in new base: base_b = ',f12.7)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c

      end
