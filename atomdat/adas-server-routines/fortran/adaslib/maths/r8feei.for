CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/r8feei.for,v 1.3 2007/04/23 13:40:45 allan Exp $ Date $Date: 2007/04/23 13:40:45 $
CX
      FUNCTION R8FEEI ( X )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8FEEI ******************
C
C  PURPOSE: EVALUATES EXP(X)E1(X) WHERE E1 IS THE 1ST EXPONENTIAL
C           INTEGRAL
C
C  NOTE:    EXACT DUPLICATE IN OPERATION TO EEI.
C
C  REFERENCE:
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  INPUT:  (R*8)  X       = INPUT PARAMETER
C
C  OUTPUT: (R*8)  R8FEEI  = FUNCTION NAME
C
C          (R*8)  DHALF   = PARAMETER = 0.5
C          (R*8)  CX0     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CX1     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CX2     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CX3     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CX4     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CX5     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CY0     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CY1     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CY2     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CY3     = PARAMETER = EQUATION CONSTANT
C          (R*8)  CY4     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C0N     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C1N     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C2N     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C3N     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C1D     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C2D     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C3D     = PARAMETER = EQUATION CONSTANT
C          (R*8)  C4D     = PARAMETER = EQUATION CONSTANT
C
C          (R*8)  A       = GENERAL VARIABLE
C          (R*8)  Y       = GENERAL VARIABLE
C          (R*8)  Z       = GENERAL VARIABLE
C
C ROUTINES: NONE
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     18/09/90
C
C VERSION  : 1.2                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION  : 1.3                          DATE: 17-4-07
C MODIFIED : HUGH SUMMERS
C            - COMPLETED COMMENT BLOCK DESCRIPTION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8     DHALF
      REAL*8     CX0                   , CX1                   ,
     &           CX2                   , CX3                   ,
     &           CX4                   , CX5
      REAL*8     CY0                   , CY1                   ,
     &           CY2                   , CY3                   ,
     &           CY4
      REAL*8     C0N                   , C1N                   ,
     &           C2N                   , C3N
      REAL*8     C1D                   , C2D                   ,
     &           C3D                   , C4D
C-----------------------------------------------------------------------
      PARAMETER( DHALF =  0.5D0 )
      PARAMETER( CX0 =  0.57721566D0   , CX1  =  0.99999193D0  ,
     &           CX2 =  0.24991055D0   , CX3  =  0.05519968D0  ,
     &           CX4 =  0.00976004D0   , CX5  =  0.00107857D0  )
      PARAMETER( CY0 =  1.D0    , CY1  =  0.9998684D0   ,
     &           CY2 =  0.4982926D0    , CY3  =  0.1595332D0   ,
     &           CY4 =  0.0293641D0    )
      PARAMETER( C0N =  0.2677737343D0 , C1N =  8.6347608925D0 ,
     &           C2N = 18.0590169730D0 , C3N =  8.5733287401D0 )
      PARAMETER( C1D =  3.9584969228D0 , C2D = 21.0996530827D0 ,
     &           C3D = 25.6329561486D0 , C4D =  9.5733223454D0 )
C-----------------------------------------------------------------------
      REAL*8   R8FEEI                  , X
      REAL*8   A                       , Y                     ,
     &         Z
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         IF (X.LE.1.0D0) THEN
            A      = -DLOG(X)-CX0+X*(CX1-X*(CX2-X*(CX3-X*(CX4-X*CX5))))
            Y      = DHALF*X
            Z      = CY0-Y*(CY1-Y*(CY2-Y*(CY3-Y*CY4)))
            R8FEEI = A/(Z*Z)
         ELSE
            R8FEEI=  (C0N+X*(C1N+X*(C2N+X*(C3N+X)))) /
     &               (X*(C1D+X*(C2D+X*(C3D+X*(C4D+X)))))
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
