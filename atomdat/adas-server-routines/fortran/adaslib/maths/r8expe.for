CX  UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/r8expe.for,v 1.1 2004/07/06 14:41:17 whitefor Exp $ Date $Date: 2004/07/06 14:41:17 $
CX
      REAL*8 FUNCTION R8EXPE( VALUE , EXPON , CODE )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8EXPE ******************
C
C  PURPOSE: TO PRODUCE A NUMBER FROM VALUE AND EXPONENT PARTS PROTECTED
C           AGAINST UNDERFLOW AND OVERFLOW. (NUMBER=VALUE*EXP(EXPON))
C 
C  INPUT : (R*8)  VALUE   = VALUE PART OF COMPOSITE NUMBER
C  INPUT : (R*8)  EXPON   = EXPONENT OF COMPOSITE NUMBER
C  INPUT : (C*(*))CODE    = 'ZERO' => ZERO VALUE DELIVERED IF UNDERFLOW
C                           'MIN'  => MINIMUM VALUE DELIVERED
C  OUTPUT: (R*8)  R8EXPE  = EVALUATED NUMBER
C        
C  ROUTINES:
C           NONE
C
C  AUTHOR:   H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C            TEL. 0141-548-4196
C            JET EXT. 5057
C
C  DATE:     16/09/99
C
C  UPDATE:
C
C VERSION:	1.1
C
C-----------------------------------------------------------------------
       REAL*8    VMIN
C-----------------------------------------------------------------------
       PARAMETER ( VMIN = 1.0D-74 )
C-----------------------------------------------------------------------
       REAL*8    VALUE     , EXPON    , T    , TMIN
C-----------------------------------------------------------------------
       CHARACTER CODE*(*)
C-----------------------------------------------------------------------
       T=DLOG(DABS(VALUE))+EXPON
       TMIN = DLOG(VMIN)
       IF(T.GT.-TMIN) THEN
           R8EXPE=DSIGN(1.0D0,VALUE)/VMIN
       ELSEIF((T.LT.TMIN).AND.((CODE.EQ.'ZERO').OR.
     &                         (CODE.EQ.'zero')))THEN
           R8EXPE=0.0D0
       ELSEIF((T.LT.TMIN).AND.((CODE.EQ.'MIN').OR.
     &                         (CODE.EQ.'min')))THEN
           R8EXPE=DSIGN(1.0D0,VALUE)*VMIN
       ELSE
           R8EXPE=VALUE*DEXP(EXPON)
       ENDIF
C-----------------------------------------------------------------------
       RETURN                                                           
      END                                                               
