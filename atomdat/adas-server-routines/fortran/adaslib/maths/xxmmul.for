CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxmmul.for,v 1.1 2004/07/06 15:37:44 whitefor Exp $ Date $Date: 2004/07/06 15:37:44 $
CX
      SUBROUTINE XXMMUL( NR , NRC , NC , A , B , C )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXMMUL *********************
C
C  PURPOSE: MULTIPLIES TWO MATRICES.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT : (I*4)  NR      = NO. OF ROWS IN MATRICES 'A' AND 'C'.
C  INPUT : (I*4)  NRC     = NO. OF COLUMNS IN MATIX 'A' AND ROWS IN
C                           MATIRX 'B'.
C  INPUT : (I*4)  NC      = NO. OF COLUMNS IN MATRICES 'B' AND 'C'.
C  INPUT : (R*8)  A(,)    = FIRST MATRIX STORED AS A LINEAR VECTOR BY
C                           COLUMN.
C                           1ST DIMENSION: NR
C                           2ND DIMENSION: NRC
C  INPUT : (R*8)  B(,)    = SECOND MATRIX STORED AS A LINEAR VECTOR BY
C                           COLUMN.
C                           1ST DIMENSION: NRC
C                           2ND DIMENSION: NC
C
C  OUTPUT: (R*8)  C(,)    = RESULT MATRIX STORED AS LINEAR VECTOR BY
C                           COLUMN.
C                           1ST DIMENSION: NR
C                           2ND DIMENSION: NC
C                           DIMENSION: MXTERM
C
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C          (I*4)  K       = LOOP INDEX.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    10/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER   NR      , NRC    , NC
      INTEGER   I       , J      , K
C----------------------------------------------------------------------
      REAL*8    SUM
C----------------------------------------------------------------------
      REAL*8    A(NR,NRC)  , B(NRC,NC)  , C(NR,NC)
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
C
      DO 1 I = 1 , NR
         DO 2 J = 1 , NC
C
            SUM = 0.0D0
            DO 3 K = 1 , NRC
               SUM = SUM + A(I,K) * B(K,J)
    3       CONTINUE
            C(I,J) = SUM
C
    2    CONTINUE
    1 CONTINUE
C
C----------------------------------------------------------------------
C
      RETURN
      END
