      subroutine xxbass(a , ia , b, ib , base)
      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxbass *********************
c
c  purpose : to scale a number, given in the form a*base^ia such that
c            base <= a < 1/base.
c
c  calling program: general use
c
c  subroutine:
c
c  input : (r*8)   a        = mantissa of number
c  input : (i*4)   ia       = characteristic
c  input : (r*8)   base     = base of number system
c
c  output: (r*8)   b        = modified mantissa of number
c  output: (i*4)   ib       = modified characteristic of number
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c author:  Hugh Summers
c          JET K1/1/61
c          Tel. 01235-46-4459
c date:    07-05-2008
c
c
c version  : 1.1
c date     : 07-05-2008
c modified : Hugh Summers
c              - first version.
c-----------------------------------------------------------------------
      integer    i4unit
      integer    ia     , ib
c-----------------------------------------------------------------------
      real*8     a      , b     , b_abs      , base       , rbase
c-----------------------------------------------------------------------
      if(base.le.0.0d0) then
          write(i4unit(-1),1000)base
          write(i4unit(-1),1001)
          return
      endif

      b = a
      ib = ia

      b_abs = abs(b)
      rbase =1.0d0/base

      if (b_abs.gt.0.0d0) then

   10    if (b_abs.gt.base) then
             b_abs=b_abs*rbase
             ib=ib+1
             go to 10
         elseif (b_abs.le.rbase) then
             b_abs=b_abs*base
             ib=ib-1
             go to 10
         else
             b=sign(b_abs, b)
         endif
      endif

      return
c
c-----------------------------------------------------------------------
c
 1000 format(1x,31('*'),' xxbass error ',30('*')//
     &       1x,'faulty in base = ',f12.7)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c

      end
