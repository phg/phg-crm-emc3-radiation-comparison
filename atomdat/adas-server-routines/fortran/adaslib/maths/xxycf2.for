CX UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxycf2.for,v 1.2 2007/05/16 08:39:23 allan Exp $ Date $Date: 2007/05/16 08:39:23 $
CX
      SUBROUTINE XXYCF2(FCN, M, N, X, FVEC, FJAC, B, C)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C ROUTINE: XXYCF2
C
C PURPOSE: CALCULATES AN ESTIMATE OF THE ELEMENTS OF THE VARIANCE-COVARIANCE
C          MATRIX OF THE ESTIMATED REGRESSION COEFFICIENTS FOR A NON-LINEAR
C          LEAST-SQUARES PROBLEM.
C
C          REPLACES NAG ROUTINE E04YCF WHEN CALLED AFTER E04GCF OR ANOTHER
C          ROUTINE THAT USES FIRST DERIVATIVES.
C*****************************************************************************
C          THIS ROUTINE CAN ONLY BE USED WHEN FIRST DERIVATIVES ARE AVAILABLE.
C*****************************************************************************
C          SEE THE DOCUMENTATION OF E04YCF AND THE E04 SERIES FOR MORE
C          INFORMATION ON THE MATHEMATICS OF CALCULATING THE VARIANCE-
C          COVARIANCE MATRIX.
C
C CALLING PROGRAM: GENERAL USE
C
C INPUT:
C    SUBROUTINE FCN - CALCULATES THE FUNCTIONS AND THEIR DERIVATIVES.
C                     IF ONE IS DIRECTLY REPLACING THE NAG ROUTINE THEN
C                     THIS ARGUMENT WILL BE LSFUN2 IN THE CALLING
C                     PROGRAM, AND LSFUN2 WILL HAVE TO BE MODIFIED TO
C                     ACCOMMODATE THE IFLAG VARIABLE. SEE BELOW FOR DETAILS.
C    (I*4)  M       - NUMBER OF FUNCTIONS
C    (I*4)  N       - NUMBER OF VARIABLES, N<=M
C    (R*8)  X       - VECTOR OF THE ESTIMATED SOLUTION
C    (R*8)  FVEC()  - THE FUNCTION EVALUATED AT X
C    (R*8)  FJAC()  - A WORK MATRIX OF DIMENSION (M,N)
C    (R*8)  B()     - A WORK VECTOR OF DIMENSION N
C
C OUTPUT:
C     (R*8)  C(,)     - THE VARIANCE-COVARIANCE MATRIX. DIMENSIONS NxN.
C
C ROUTINES:
C-----------------------------------------------------------------------
C     NAME     SOURCE   PURPOSE
C-----------------------------------------------------------------------
C     XXMINV   ADAS     INVERTS A SQUARE MATRIX
C     FCN      USER     CALCULATES FUNCTIONS AND THEIR FIRST DERIVATIVES
C {
C    SUBROUTINE FCN(M,N,X,FVEC,FJAC,LDFJAC,IFLAG)
C    INTEGER M,N,IFLAG, LDFJAC
C    REAL*8 X(N), FVEC(M), FJAC(LDFJAC,N)
C  INPUT:
C    M,N    - AS ABOVE
C    LDFJAC - FIRST DIMENSION OF FJAC
C    X      - VECTOR AT WHICH THE FUNCTION IS TO BE EVALUATED
C    IFLAG  - 1=> CALCULATE FVEC BUT DON'T CHANGE FJAC
C           - 2=> CALCULATE FJAC BUT DON'T CHANGE FVEC
C  OUTPUT:
C    FVEC   - FUNCTION EVALUATED AT X OR NOT CHANGED
C    FJAC   - JACOBIAN OF FUNCTION EVALUATED AT X OR NOT CHANGED
C }
C-----------------------------------------------------------------------
C
C AUTHOR: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C
C DATE:   31-05-96
C 
C VERSION 1.1                             DATE: 31-05-96
C MODIFIED: WILLIAM OSBORN
C           - FIRST VERSION
C
C VERSION 1.2                             DATE: 16-05-07
C MODIFIED: ALLAN WHITEFORD
C           - Updated comments as part of subroutine documentation
C             procedure.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER M, N, I, J, IFLAG, K, LWA
      REAL*8 X(N), C(N,N), FSUMSQ, FJAC(M,N), FVEC(M), B(N)
      REAL*8 FAC, DINT
      LOGICAL LSOLVE
      EXTERNAL FCN

C-----------------------------------------------------------------------
C GET JACOBIAN FJAC
C-----------------------------------------------------------------------
      IFLAG = 2
      CALL FCN(M,N,X,FVEC,FJAC,M,IFLAG)
C-----------------------------------------------------------------------
C CALCULATE SUM OF SQUARES AT ESTIMATED SOLUTION
C-----------------------------------------------------------------------
      FSUMSQ = 0.0
      DO 30 I=1,M
         FSUMSQ = FSUMSQ + FVEC(I)**2
 30   CONTINUE

C-----------------------------------------------------------------------
C     CALCULATE HESSIAN, J^T J, AND MULTIPLY BY (M-N)/MIN.VALUE
C-----------------------------------------------------------------------
      FAC = (M-N)/FSUMSQ
      DO 1 I=1,N
         DO 2 J=1,N
            C(I,J)=0.0
            DO 3 K=1,M
               C(I,J)=C(I,J)+FJAC(K,I)*FJAC(K,J)
 3          CONTINUE
            C(I,J)=C(I,J)*FAC
 2       CONTINUE
 1    CONTINUE

C-----------------------------------------------------------------------
C CALCULATE INVERSE
C-----------------------------------------------------------------------
      LSOLVE = .FALSE.
      CALL XXMINV(LSOLVE, N, N, C, B, DINT)

      RETURN

      END
