CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxmnmx.for,v 1.3 2007/04/11 13:01:53 allan Exp $ Date $Date: 2007/04/11 13:01:53 $
CX
      SUBROUTINE XXMNMX( LOGFIT , MAXDEG , TOLVAL ,
     &                   NIN    , XIN    , YIN    ,
     &                   NOUT   , XOUT   , YOUT   ,
     &                   NCOEF  , COEF ,
     &                   MINFO
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXMNMX *********************
C
C  PURPOSE:           TO EVALUATE THE TAYLOR COEFFICIENTS OF THE MINIMAX
C                     POLYNOMIAL.
C
C                     OUTPUTS 'NOUT' INTERPOLATED COORDINATES  USING THE
C                     MINIMAX EVALUATION, WHICH ARE EVENLY  SPACED ALONG
C                     THE X-AXIS COVERING THE X-VALUE RANGE  INPUT.   IF
C                     REQUIRED THE SPACING WILL BE EVENLY  SPACED  ALONG
C                     THE LOG10 TRANSFORMATION OF THE  X-AXIS  (IN  THIS
C                     CASE THE Y-VALUES ARE SIMILARLY TRANSFORMED).
C
C                     * IMPORTANT: 'NOUT' >= 'NIN' (NO. OF INPUT VALUES)
C
C                     MINFO(CHARACTER*80) ON OUTPUT CONTAINS INFORMATION
C                     REGARDING THE SLOPE OF THE CURVE AT THE BOUNDARIES
C                     OF THE EXPERIMENTAL REGION, AND AN ESTIMATE OF THE
C                     MAXIMUM ERROR IN THE MINIMAX INTERPOLATION.
C
C  CALLING PROGRAMS:  GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LOGFIT  = .TRUE.  => PERFORM MINIMAX POLYNOMIAL EVAL-
C                                      UATION ON LOG10 TRANSFORMATIONS
C                                      OF THE INPUT X AND Y DATA.
C                           .FALSE  => PERFORM MINIMAX POLYNOMIAL EVAL-
C                                      UATION ON THE X AND Y VALUES AS
C                                      INPUT.
C  INPUT : (I*4)  MAXDEG  = MAXIMUM   POSSIBLE  DEGREE  OF   POLYNOMIAL
C                           ALLOWED IN THE MINIMAX FITTING (MUST BE< 26)
C                           (IF 'NOUT'<100 => MAX. DEGREES ALLOWED = 22)
C                           NOTE:
C                           'COEF()' MUST HAVE .GE. 'MAXDEG+1' ELEMENTS
C  INPUT : (R*8)  TOLVAL  = PARAMETER = FRACTIONAL TOLERANCE FOR ACCEPT-
C                                       ANCE OF DATA FITTED  BY  MINIMAX
C                                       POLYNOMIAL.  (IF IT EQUALS  ZERO
C                                       THEN RUNS TO MAX. DEGREE).
C
C  INPUT : (I*4)  NIN     = NUMBER OF INPUT KNOTS
C  INPUT : (R*8)  XIN()   = INPUT X-VALUES OF KNOTS
C  INPUT : (R*8)  YIN()   = INPUT Y-VALUES OF KNOTS
C
C  INPUT : (I*4)  NOUT    = NUMBER  OF  OUTPUT  VALUES  REQUIRED  TO  BE
C                           INTERPOLATED USING MINIMAX POLYNOMIAL EVAL-
C                           UATION - SPACED EVENLY BETWEEN THE MINIMUM
C                           AND MAXIMUM VALUES OF THE (TRANSFORMED) X-
C                           VALUES INPUT.
C  OUTPUT: (R*8)  XOUT()  = INITIALLY: ORDERED/(TRANSFORMED) 'XIN()'
C                                      (ASCENDING ORDER).
C                                      'NIN' VALUES
C                           OUTPUT   : X-VALUES FOR WHICH INTERPOLATION
C                                      IS CARRIED OUT.
C                                      'NOUT' VALUES
C  OUTPUT: (R*8)  YOUT()  = INITIALLY: ORDERED/(TRANSFORMED) 'YIN()'
C                                      (ORDERED ACCORDING TO 'XOUT()').
C                                      'NIN' VALUES
C                           OUTPUT   : INTERPOLATED Y-VALUES FOR THE
C                                      X-VALUES ('XOUT()' - OUTPUT)
C                                      'NOUT' VALUES
C
C  OUTPUT: (I*4)  NCOEF   = NUMBER OF MINIMAX FIT COEFFICIENTS
C  OUTPUT: (R*8)  COEF()  = MINIMAX COEFFICIENTS - ARRAY SIZE: MAXDEG+1
C
C  OUTPUT: (C*80) MINFO   = DIAGNOSTIC INFORMATION STRING
C
C          (I*4)  MAXOUT  = PARAMETER = MAXIMUM NUMBER OF OUTPUT X,Y CO-
C                                       ORDINATES THAT CAN BE INTERPOLA-
C                                       TED.
C          (I*4)  MAXTOL  = PARAMETER = POWER OF 10 WHICH REPRESENTS THE
C                                       MAXIMUM FRACTIONAL DIFFERENCE
C                                       ALLOWED BETWEEN ACTUAL (YDATA)
C                                       AND FITTED (YFIT) DATA.
C          (I*4)  MINTOL  = PARAMETER = POWER OF 10 WHICH REPRESENTS THE
C                                       MINIMUM FRACTIONAL DIFFERENCE
C                                       ALLOWED BETWEEN ACTUAL (YDATA)
C                                       AND FITTED (YFIT) DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  ILIMIT  = UPPER LIMIT  OF  THE  NUMBER  OF  DEGREES
C                           ALLOWED FOR MINIMAX POLYNOMIAL EVALUATION.
C          (I*4)  IDEG    = NUMBER OF DEGREES BEING USED FOR MINIMAX
C                           POLYNOMIAL EVALUATION.
C          (I*4)  NDEG    = NUMBER OF DEGREES FOR ACCEPTED MINIMAX FIT.
C          (I*4)  I1      = GENERAL ARRAY INDEX
C          (I*4)  I2      = GENERAL ARRAY INDEX
C
C          (R*8)  XMIN    = MINIMUM (TRANSFORMED) X-VALUE INPUT
C          (R*8)  XMAX    = MAXIMUM (TRANSFORMED) X-VALUE INPUT
C          (R*8)  REF     = OUTPUT FROM 'XXCHEB' = NEGATIVE IF MINIMAX
C                           FITTING PROCEDURE IS CYCLING.
C          (R*8)  SUM     = USED TO SUM UP POLYNOMIAL TERMS WHEN CALCUL-
C                           ATING VALUE OF 'Y' AT GIVEN 'X'.
C          (R*8)  YFIT    = FITTED Y-VALUE USING MINIMAX POLYNOMIAL
C          (R*8)  YDATA   = INPUT Y-VALUE FOR COMPARISON TO FITTED VALUE
C          (R*8)  DIFF    = FRACTIONAL DIFFERENCE BETWEEN ACTUAL (YDATA)
C                           AND  FITTED  (YFIT)  Y-VALUE   FROM  MINIMAX
C                           POLYNOMIAL FITTED.
C          (R*8)  BIG     = LARGEST FRACTIONAL ERROR BETWEEN ACTUAL AND
C                           FITTED Y-VALUES FOR POLYNOMIAL EVALUATION.
C          (R*8)  GRADL   = GRADIENT OF FITTED MINIMAX POLYNOMIAL CURVE
C                           AT LOWER BOUNDARY OF INPUT DATA.
C          (R*8)  GRADU   = GRADIENT OF FITTED MINIMAX POLYNOMIAL CURVE
C                           AT UPPER BOUNDARY OF INPUT DATA.
C          (R*8)  XSTEP   = SEPERATION   OF   OUTPUT  X-VALUES  TO   BE
C                           INTERPOLATED.
C          (R*8)  XVAL    = X-VALUES AT WHICH INTERPOLATION IS TAKING
C                           PLACE.
C
C          (C*6)  CFTYPE  = 'LOGFIT' IF 'LOGFIT=.TRUE.'
C                           'LINFIT' IF 'LOGFIT=.FALSE.'
C
C          (L*4)  LMFIT   = .TRUE.  => MINIMAX POLYNIOMIAL FOUND WHICH
C                                      IS WITHIN DESIRED TOLERANCE.
C                                      (SEE 'TOLVAL')
C                           .FALSE. => NO MINIMAX POLYNIOMIAL FOUND WITH
C                                      DESIRED TOLERANCE.
C                                      (SEE 'TOLVAL')
C
C          (I*4)  INDX()  = ASCENDING ORDER INDEX FOR INPUT X-VALUES
C
C          (R*8)  WRK()   = WORKING SPACE FOR ORDERING DATA
C
C
C NOTES:
C          THIS SUBROUTINE IS AN AMMENDED AND STRUCTURED VERSION OF  THE
C          SUBROUTINE  'MINIMAX'  WRITTEN BY  STEPHEN TURNER,  JET  30TH
C          JULY 1989. IT NO LONGER ORDERS THE INPUT COORDINATES DIRECTLY
C          BUT COPIES THE TO A SECOND ARRAY AND ORDERS THEM.   THEREFORE
C          IF THE INPUT COORDINATES ARE REQUIRED TO BE ORDERED THIS MUST
C          BE CARRIED OUT EXPLICITLY IN THE CALLING OPROGRAM/ROUTINE.
C          THE SUBROUTINE NO LONGERS REQUIRES THE INPUT CO-ORDINATES  IN
C          LOG10 FORM, THIS TRANSFORMATION CAN BE CARRIED OUT EXPLICITLY
C          BY THE SUBROUTINE.   THE  ROUTINE HAS BEEN WRITTEN TO  BE  OF
C          GENERAL RATHER THAN SPECIFIC USE.
C
C          IN THE ORIGINAL 'MINIMAX' SUBROUTINE THE MINIMUM NUMBER OF
C          DEGREES ACCEPTED FOR A FIT WAS 2, IN 'XXMNMX' IT IS 1.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXINDX     ADAS     GIVES INDICES OF SORTED ARRAY (ASCENDNG)
C          XXISRT     ADAS     RE-ORDERS ARRAY ACCORDING TO 'XXINDX'
C          XXCHEB     ADAS     MINIMAX POLYNOMIAL COEFT. EVALUATION
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     12/10/90
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  14/02/94 - PE BRIDEN - ADAS91: CORRECTED BUG WHEN CHECKING
C                                         TOLERANCE FOR LOGFIT - CHANGED
C                                         THE LINE:
C                                                  YDATA = YIN(I1)
C                                         TO:
C                                                  YDATA = YIN(INDX(I1))
C                                         (WOULD CAUSE PROBLEMS IF YIN
C                                          NOT ORDERED ON INPUT.)
C
C UPDATE:  14/02/94 - PE BRIDEN - ADAS91: RECODED SECTION USE TO TEST
C                                         TOLERANCE VALUE - NOW CHECKS
C                                         FOR DIVISION BY 0 AND USES
C                                         LOG10 TO STORES VALUES THUS
C                                         AVOIDING OVERFLOWS.
C                                         (BIG IS NOW STORED AS LOG10
C                                          VALUES INITIALLY)
C                                         INTRODUCED: MAXTOL & MINTOL
C                                                 - / -
C                                         STOP EXECUTING XXCHEB IF
C                                         CYCLING (CHECK REF VARIABLE)
C                                                 - / -
C                                         INITIALISED NDEG AND BIG TO
C                                         STOP ICA WARNING.
C
C UPDATE:  31/10/94 - PE BRIDEN - ADAS91: REPLACED CALL TO NAG ROUTINE
C                                         E02ACF WITH A CALL TO THE NEW
C                                         EQUIVALENT ADAS ROUTINE CALLED
C                                         XXCHEB (IDENTICAL ARGUMENTS).
C UPDATE:  17/1/95 - L JALOTA - IDL-ADAS : MODIFIED ERROR CHECKING 
C					   SECTION TO WRITE TO UNIT 0
C					   NOT STDOUT WHICH INTERFERES
C					   WITH PIPE COMMUNICATIONS.
C       :   6/3/95 - L JALOTA - IDL-ADAS : REPLACED CALL TO I4UNIT.
C
C UNIX-IDL PORT:
C
C VERSION: 1.2                          DATE: 10-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - INCREASED PARAMETER MAXOUT FROM 100 TO 120
C                 PRIMARILY TO PREVENT PROBLEMS WITH ADAS 506 WHERE
C                 101 POINTS ARE REQUIRED.
C
C VERSION  : 1.3                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER    MAXOUT         , MAXTOL      , MINTOL
C----------------------------------------------------------------------
      PARAMETER( MAXOUT = 120   , MAXTOL = 10 , MINTOL = -10 )
C----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    MAXDEG         , NIN         , NOUT      , NCOEF
      INTEGER    ILIMIT         , IDEG        , NDEG      , I1       ,
     &           I2
C----------------------------------------------------------------------
      REAL*8     TOLVAL
      REAL*8     XMIN           , XMAX        , BIG       , REF      ,
     &           SUM            , YFIT        , YDATA     , DIFF     ,
     &           GRADL          , GRADU       , XSTEP     , XVAL
C----------------------------------------------------------------------
      LOGICAL    LOGFIT         , LMFIT
C----------------------------------------------------------------------
      CHARACTER  CFTYPE*6       , MINFO*80
C----------------------------------------------------------------------
      INTEGER    INDX(MAXOUT)
C----------------------------------------------------------------------
      REAL*8     XIN(NIN)       , YIN(NIN)    ,
     &           XOUT(NOUT)     , YOUT(NOUT)  ,
     &           COEF(MAXDEG+1)
      REAL*8     WRK(MAXOUT)
C
C----------------------------------------------------------------------
C
      DATA       BIG / MINTOL / , NDEG / 1 /
C
C-----------------------------------------------------------------------
C PERFORM CHECKS ON VALIDITY OF DATA AND OPTIONS
CA MODIFIED TO WRITE TO UNIT 0 NOT STDOUT FOR IDL-ADAS
C-----------------------------------------------------------------------
      IF (MAXDEG .GT. 25     ) THEN 
         WRITE(0,*) ' XXMNMX ERROR: MAXDEG > 25'
	 STOP
      ENDIF 
      IF (NOUT   .GT. MAXOUT ) THEN
         WRITE(0,*) ' XXMNMX ERROR: NOUT > MAXOUT'
         STOP
      ENDIF
      IF (NIN    .LT. 2      ) THEN 
         WRITE(0,*) ' XXMNMX ERROR: NIN  < 2'
	 STOP
      ENDIF
      IF (NIN    .GT. NOUT   ) THEN
	 WRITE(0,*)  ' XXMNMX ERROR: NIN  > NOUT'
         STOP
      ENDIF
      IF (TOLVAL .LT. 0.0    ) THEN
         WRITE(0,*)  ' XXMNMX ERROR: TOLVAL < 0'
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C COPY INPUT XIN,YIN ARRAYS INTO XOUT,YOUT ARRAYS FOR ORDERING.
C ------------ CARRY OUT LOG TRANSFORMATION IF REQUESTED ---------------
C-----------------------------------------------------------------------
C
         IF (LOGFIT) THEN
            CFTYPE='LOGFIT'
               DO 1 I1 = 1,NIN
                  XOUT(I1) = DLOG10(XIN(I1))
                  YOUT(I1) = DLOG10(YIN(I1))
    1          CONTINUE
         ELSE
            CFTYPE='LINFIT'
               DO 2 I1 = 1,NIN
                  XOUT(I1) = XIN(I1)
                  YOUT(I1) = YIN(I1)
    2          CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C SORT THE DATA SO THAT THE XOUT() ARE IN ASCENDING ORDER
C-----------------------------------------------------------------------
C
      CALL XXINDX( NIN , XOUT , INDX )
      CALL XXISRT( NIN , XOUT , INDX , WRK )
      CALL XXISRT( NIN , YOUT , INDX , WRK )
C
      XMIN=XOUT(1)
      XMAX=XOUT(NIN)
C
C-----------------------------------------------------------------------
C FIND THE HIGHEST DEGREE OF POLYNOMIAL WHICH CAN SAFELY BE USED= ILIMIT
C NOTE: MAXIMUM DEGREE ALLOWED IS 'NIN - 2'. (SEE ADAS ROUTINE 'XXCHEB')
C-----------------------------------------------------------------------
C
      ILIMIT = NINT( 2 * DSQRT(DBLE(NIN)) ) + 2
      ILIMIT = MIN0( ILIMIT , MAXDEG , NIN-2 )
C
C-----------------------------------------------------------------------
C FIND THE DEGREE 'NDEG' OF THE APPROXIMATING POLYNOMIAL WHICH GIVES A
C MAXIMUM ERROR OF LESS THAN A SPECIFIED TOLERANCE (='TOLVAL')
C - IF ADAS ROUTINE XXCHEB STARTS CYCLING (I.E. REF < 0) THEN STOP
C   WITHOUT REQUIRED ACCURACY BEING FOUND.
C-----------------------------------------------------------------------
C
      LMFIT=.FALSE.
      REF  = 0
C
         DO 3 IDEG=1,ILIMIT
C-----------------------------------------------------------------------
            IF ((.NOT.LMFIT).AND.(REF.GE.0)) THEN
               LMFIT = .TRUE.
               BIG   = DBLE(MINTOL)
               NDEG  = IDEG
               NCOEF = IDEG + 1
               CALL XXCHEB(XOUT,YOUT,NIN,COEF,NCOEF,REF)
C
                  DO 4 I1=1,NIN
C
                     SUM  = COEF(1)
C
                        DO 5 I2=1,NDEG
                           SUM=SUM+(COEF(I2+1)*(XOUT(I1)**I2))
    5                   CONTINUE
C
                        IF (LOGFIT) THEN
                           YFIT  = 10.0**SUM
                           YDATA = YIN(INDX(I1))
                        ELSE
                           YFIT  = SUM
                           YDATA = YOUT(I1)
                        ENDIF
C
                     DIFF = ABS(YDATA-YFIT)
C
                        IF (DIFF.GT.0) THEN
                           IF (DIFF.GT.ABS(TOLVAL*YDATA)) LMFIT=.FALSE.
                              IF (ABS(YDATA).EQ.0) THEN
                                 DIFF=DBLE(MAXTOL)
                              ELSE
                                 DIFF=DLOG10(DIFF)-DLOG10(ABS(YDATA))
                              ENDIF
                           IF (DIFF.GT.BIG) BIG = DIFF
                        ENDIF
C
    4             CONTINUE
            ENDIF
C-----------------------------------------------------------------------
    3    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT FINAL ACCURACY FROM LOG VALUES. NOTE: 1.0D-10 < BIG < 1.0D+10
C-----------------------------------------------------------------------
C
      IF     (BIG.LT.DBLE(MINTOL)) THEN
         BIG = 10.0**MINTOL
      ELSEIF (BIG.GT.DBLE(MAXTOL)) THEN
         BIG = 10.0**MAXTOL
      ELSE
         BIG = 10.0**BIG
      ENDIF
C
C-----------------------------------------------------------------------
C IF REQUIRED ACCURACY NOT FOUND SEND MESSAGE TO UNIT 6
C-----------------------------------------------------------------------
C
      IF (.NOT.LMFIT)
     &   WRITE(I4UNIT(-1),1000) CFTYPE,NDEG,(TOLVAL*100.0),(BIG*100.0)
C
C-----------------------------------------------------------------------
C FIND  THE  SLOPE  OF THE POLYNOMIAL CURVE AT  THE  LOWER (=GRADL)  AND
C UPPER (=GRADU) BOUNDARIES/ENDS OF THE EXPERIMENTAL REGION.
C-----------------------------------------------------------------------
C
      GRADL=COEF(2)
         DO 6 I1=2,NDEG
            GRADL = GRADL + ( (COEF(I1+1)*(XMIN**(I1-1))) * DBLE(I1) )
    6    CONTINUE
C
      GRADU=COEF(2)
         DO 7 I1=2,NDEG
            GRADU = GRADU + ( (COEF(I1+1)*(XMAX**(I1-1))) * DBLE(I1) )
    7    CONTINUE
C
      WRITE (MINFO,1001) CFTYPE , NDEG , (BIG*100.0) , GRADL , GRADU
C
C-----------------------------------------------------------------------
C INTERPOLATE 'NOUT' VALUES FOR OUTPUT
C-----------------------------------------------------------------------
C
      XSTEP = (XMAX-XMIN) / DBLE(NOUT-1)
C
         DO 8 I1=1,NOUT
            XVAL = XMIN + ( XSTEP * DBLE(I1-1) )
            SUM  = COEF(1)
               DO 9 I2=1,NDEG
                  SUM = SUM + ( COEF(I2+1) * (XVAL**I2) )
    9          CONTINUE
            XOUT(I1)=XVAL
            YOUT(I1)=SUM
    8    CONTINUE
C
C-----------------------------------------------------------------------
C IF 'LOGFIT' REQUESTED TRANSFORM OUTPUT TO LINEAR FORM
C-----------------------------------------------------------------------
C
         IF (LOGFIT) THEN
            DO 10 I1=1,NOUT
               XOUT(I1) = 10.0**XOUT(I1)
               YOUT(I1) = 10.0**YOUT(I1)
   10       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,31('*'),' XXMNMX WARNING ',31('*')/
     &        1X,' NO MINIMAX POLYNOMIAL COULD BE EVALUATED WITH THE',
     &           ' REQUIRED ACCURACY.'/
     &        1X,' MINIMAX POLYNOMIAL EVALUATION (',A6,'):'/41X,
     &           ' DEGREE             = ',I2/41X,
     &           ' REQUESTED ACCURACY = ',F6.2,'%'/41X,
     &           ' ACCEPTED  ACCURACY = ',F6.2,'%'/
     &        1X,79('*'))
 1001  FORMAT (A6,' - DEGREE=',I2,2X,'ACCURACY=',F6.2,'%',2X,
     &'END GRADIENT: LOWER=',F7.2,1X,'UPPER=',F7.2,' ')
C
C-----------------------------------------------------------------------
C
       RETURN
       END
