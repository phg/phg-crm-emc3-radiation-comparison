      SUBROUTINE XXSPLF( LSETX , LSETY  , IOPT   , FINTX ,
     &                   NIN   , XIN    , YIN    ,
     &                   NOUT  , XOUT   , YOUT   ,
     &                   X     , DY     ,
     &                   Q     , D1     , D2     , D3    ,
     &                   LINTRP
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXSPLF *********************
C
C  PURPOSE:           TO INTERPOLATE/EXTRAPOLATE USING CUBIC SPLINES
C
C                     (IF IOPT < 0 NO EXTRAPOLATION TAKES PLACE = VALUES
C                      SET TO ZERO).- LOGICAL ARRAY 'LINTRP()' SPECIFIES
C                      WHETHER OUTPUT SPLINE IS INTERPOLATED '.TRUE.' OR
C                      EXTRAPOLATED '.FALSE.'.
C
C                      (AS FOR 'XXSPLN' EXCEPT 'LINTRP' ARGUMENT ADDED).
C                      (AS FOR 'XXSPLE' EXCEPT WITH OPTION TO USE
C                       PREVIOUSLY CALCULATED SPLINE DERIVATIVES)
C
C  CALLING PROGRAMS:  GENERAL USE
C
C  SUBROUTINE:
C
C  I/O   : (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           ( 'LSETX' IS ALWAYS RETURN AS '.FALSE.'  ON
C                             RETURN FROM THE SUBROUTINE ).
C                           ** IMPORTANT: SEE NOTES BELOW ON 'LSETX' **
C  I/O   : (L*4)  LSETY   = .TRUE.  => CALCULATE SPLINE DERIVATIVES
C                                      RELATING TO 'YIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE DERIVATIVES
C                                      RELATING TO 'YIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           ( 'LSETY' IS ALWAYS RETURN AS '.FALSE.'  ON
C                             RETURN FROM THE SUBROUTINE ).
C                           ** IMPORTANT: SEE NOTES BELOW ON 'LSETY' **
C  INPUT : (I*4)  IOPT    = SPLINE END CONDITIONS/EXTRAPOLATION CONTROL
C                           SWITCH - SEE NOTES BELOW
C                           I.E. DEFINES THE BOUNDARY DERIVATIVES.
C                                (VALID VALUES = 0, 1, 2, 3, 4)
C                           IF IOPT < 0 THEN NO EXTRAPOLATION TAKES
C                           - ANY VALUES REQUIRING EXTRAPOLATION WILL BE
C                             SET TO ZERO (END CONDITIONS AS FOR IOPT=0)
C  INPUT : (R*8)  FINTX   = INTERPOLATING X-COORDINATE TRANSFORMATION.
C                           EXTERNAL FUNCTION (SEE ROUTINES BELOW)
C
C  INPUT : (I*4)  NIN     = NUMBER OF KNOTS
C  INPUT : (R*8)  XIN()   = X-VALUES OF KNOTS
C  INPUT : (R*8)  YIN()   = Y-VALUES OF KNOTS
C
C  INPUT : (I*4)  NOUT    = NUMBER OF OUTPUT VALUES TO BE INTERPOLATED
C                           EXTRAPOLATED.
C  INPUT : (R*8)  XOUT()  = X-VALUES AT WHICH INTERPOLATION/EXTRAPOLA-
C                           TION REQUIRED
C  OUTPUT: (R*8)  YOUT()  = INTERPOLATED/EXTRAPOLATED Y-VALUES FOR
C                           REQUESTED 'XOUT()' VALUES.
C  
C  I/O   : (R*8)  X()     = TRANSFORMED VALUES OF 'XIN()'. (ARRAY SIZE:
C                           NIN) REQUIRED INPUT IF LSETX IS .FALSE.
C  I/O   : (R*8)  DY()    = DERIVATIVES AT INPUT KNOTS. REQUIRED INPUT
C                           IF LSETY IS .FALSE.
C  I/O   : (R*8)  Q()     = SECOND DERIVATIVE FOR KNOT. REQUIRED INPUT
C                           IF LSETX IS .FALSE. AND LSETY IS .TRUE.
C  I/O   : (R*8)  D1()    = MULTIPLICATION FACTOR USED IN CALCULATING
C                           'U()'. REQUIRED INPUT IF LSETX IS .FALSE.
C                           AND LSETY IS .TRUE.
C  I/O   : (R*8)  D2()    = MULTIPLICATION FACTOR USED IN CALCULATING
C                           'U()'. REQUIRED INPUT IF LSETX IS .FALSE.
C                           AND LSETY IS .TRUE.
C  I/O   : (R*8)  D3()    = MULTIPLICATION FACTOR USED IN CALCULATING
C                           'U()'. REQUIRED INPUT IF LSETX IS .FALSE.
C                           AND LSETY IS .TRUE.
C
C  OUTPUT: (L*4)  LINTRP()= .TRUE.  => 'YOUT()' VALUE INTERPOLATED.
C                           .FALSE. => 'YOUT()' VALUE EXTRAPOLATED.
C                           (ARRAY SIZE: NOUT)
C
C          (I*4)  NKNOTS  = PARAMETER = MAXIMUM  NUMBER OF KNOTS ALLOWED
C          (I*4)  NIOPT   = PARAMETER = MAXIMUM  VALUE OF IOPT ALLOWED
C
C          (I*4)  I       = GENERAL ARRAY USE
C          (I*4)  K       = INDEX OF 'XOUT()' VALUE FOR INTERPOLATION/
C                           EXTRAPOLATION.
C          (I*4)  NIN0    = 'NIN' - 1
C          (I*4)  INTER   = INDEX OF CLOSEST/NEXT HIGHEST VALUE OF
C                           'XIN()' TO THE VALUE OF 'XOUT()' BEING
C                           INTERPOLATED/EXTRAPOLATED. WHEN LOOPING
C                           OVER MULTIPLE YOUT EVALUATIONS, THE INDEX
C                           OF THE LAST EVALUATION IS USED AS THE
C                           INITIAL GUESS FOR THE NEXT.
C          (I*4)  NOPT    = VALUE OF  'IOPT' USED IN  CALCULATING  END-
C                           CONDITIONS   FOR  STORED  'X-VALUE'  SPLINE
C                           PARAMETERS.   (NOTE:  IF  'IOPT < 0',  THEN
C                           'NOPT = 0'.) - I.E. 'NOPT = MAX( 0, IOPT )'.
C
C          (R*8)  XK      = VALUE OF 'XOUT(K)' BEING INTERPOLATED/
C                           EXTRAPOLATED
C          (R*8)  XKK     = TRANSFORMED VALUE OF 'XOUT(K)' BEING
C                           INTERPOLATED/EXTRAPOLATED.
C          (R*8)  T1      = INVERSE OF SEPARATION OF KNOTS EITHER
C                           SIDE OF CURRENT KNOT.
C          (R*8)  T2      = (CURRENT KNOT POSITION TO NEXT HIGHEST KNOT
C                            POSITION) DIVIDED BY 'T1'
C          (R*8)  T3      = (CURRENT KNOT POSITION TO NEXT LOWEST  KNOT
C                            POSITION) DIVIDED BY 'T1'
C          (R*8)  T4      = INTERPOLATION FACTOR FOR CURRENT KNOT
C          (R*8)  DL1     = (REQUESTED 'XOUT()' VALUE TO NEXT HIGHEST
C                            KNOT POSITION) DIVIDED BY SEPARATION OF
C                            KNOTS EITHER SIDE OF 'XOUT(K)'.
C          (R*8)  DL2     = (REQUESTED 'XOUT()' VALUE TO NEXT LOWEST
C                            KNOT POSITION) DIVIDED BY SEPARATION OF
C                            KNOTS EITHER SIDE OF 'XOUT(K)'.
C          (R*8)  DL2     = (REQUESTED 'XOUT()' VALUE TO NEXT LOWEST
C          (R*8)  DL3     =  SEPARATION OF KNOTS EITHER SIDE OF
C                            'XOUT(K)' * 'DL1' * 'DL2'.
C
C          (L*4)  LEXTRP  = .TRUE.  => 'EXTRAPOLATION SWITCHED ON'.
C                           .FALSE. => 'EXTRAPOLATION SWITCHED OFF'.
C
C          (R*8)  QVAL()  = VALUE OF 'Q(1)'   : FUNCTION OF 'NOPT'
C          (R*8)  D2VAL() = VALUE OF 'D2(1)'  : FUNCTION OF 'NOPT'
C          (R*8)  D3VAL() = VALUE OF 'D3(1)'  : FUNCTION OF 'NOPT'
C          (R*8)  UVAL() =  VALUE OF 'U(NIN)' : FUNCTION OF 'NOPT'
C          (R*8)  AGRL() =  POLYNOMIAL CONSTANTS FOR CUBIC SPLINE FOR
C                           GIVEN 'XOUT(K)' VALUE.
C          (R*8)  H()    =  SEPARATION, ALONG X-AXIS, OF KNOT FROM NEXT
C                           HIGHEST KNOT.
C          (R*8)  HINTER =  SEPARATION, ALONG X-AXIS, IN INTERVAL FOR
C                           INTERPOLATION
C          (R*8)  U()    =  TEMPORARY STORAGE OF DECOMPOSED FACTORS
C          (R*8)  DELY() =  SEPARATION, ALONG Y-AXIS, OF KNOT FROM NEXT
C                           HIGHEST KNOT.
C
C          (L*4)  LUVAL()=  .TRUE. => VALUE OF 'UVAL()' REFERS TO RATE
C                                     OF CHANGE OF SLOPE AT FINAL POINT.
C                           .FALSE.=> VALUE OF 'UVAL()' REFERS TO FINAL
C                                     SLOPE
C                            FUNCTION OF 'NOPT'
C
C NOTES: 'LSETX': SET TO .TRUE. ON ENTRY IF A NEW 'XIN' ARRAY IS BEING
C                 USED.  IF THE 'XIN' AXIS IS THE SAME FOR A NUMBER OF
C                 CALLS THEN DO NOT RESET 'LSETX'  -  THIS  SUBROUTINE
C                 SETS IT TO .FALSE. FOR YOU.   IF THE VALUE OF 'NOPT'
C                 IS CHANGED BETWEEN CALLS THEN THE VALUE  OF  'LSETX'
C                 ON  ENTRY IS TAKEN AS BEING EQUAL TO .TRUE.  NOPT IS
C                 INITIALISED TO -1  SO  THAT LSETX WILL BE SET .TRUE.
C                 ON THE FIRST CALL OF THIS SUBROUTINE.
C
C                 THEREFORE 'LSETX' NEED ONLY BE SET TO .TRUE. ON ENTRY
C                 IF ANY ONE OF THE FOLLOWING VALUES HAS CHANGED:
C
C                 'NIN' , 'FINTX' , 'XIN(I), I=1,NIN'
C
C        'LSETY': SET TO .TRUE. ON ENTRY IF A NEW 'YIN' ARRAY IS BEING
C                 USED.  IF THE 'YIN' AXIS IS THE SAME FOR A NUMBER OF
C                 CALLS THEN DO NOT RESET 'LSETY'  -  THIS  SUBROUTINE
C                 SETS IT  TO  .FALSE.  FOR YOU.   IF LSETX IS .TRUE.,
C                 EITHER  ON  ENTRY  OR  BECAUSE THE ROUTINE RESETS IT
C                 (SEE ABOVE) THEN LSETY IS ALSO SET TO .TRUE.
C
C                 THEREFORE 'LSETY' NEED ONLY BE SET TO .TRUE. ON ENTRY
C                 IF YIN HAS CHANGED WHILE THE 'X' VALUES HAVE NOT.
C
C                 CARE: VARIABLES MUST BE USED FOR 'LSETX' AND 'LSETY',
C                       A CONSTANT, I.E.  .TRUE. ,  CANNOT BE DIRECTLY
C                       TYPED AS AN ARGUMENT BECAUSE IT WILL BE CHANGED
C                       TO  .FALSE. ON RETURN.
C
C         SPLINE  END CONDITIONS AND EXTRAPOLATION DEPEND ON 'IOPT' AS
C         FOLLOWS:
C
C         --------------------------------------------------------------
C         | IOPT  | NOPT |  DY(1)  DDY(1)  |  DY(N)   DDY(N)  |EXTRAP'N|
C         |-------|------|-----------------|------------------|--------|
C         | < 0   |   0  |    -     0.0    |    -      0.0    |  NO    |
C         |   0   |   0  |    -     0.0    |    -      0.0    |  YES   |
C         |   1   |   1  |    -     0.0    |  -1.5      -     |  YES   |
C         |   2   |   2  |   0.0     -     |   1.0      -     |  YES   |
C         |   3   |   3  |  -0.5     -     |  -1.5      -     |  YES   |
C         |   4   |   4  |   0.0     -     |    -      0.0    |  YES   |
C         |   5   |   5  |  -4.5     -     |  -1.5      -     |  YES   |
C         |   6   |   6  |  +0.5     -     |    -      0.0    |  YES   |
C         |   7   |   7  |  -3.5     -     |    -      0.0    |  YES   |
C         --------------------------------------------------------------
C
C            NB. OPTIONS TO BE EXTENDED FOR POWER AND CX APPLICATION
C
C         -------------------------------------------------------------
C          IF ( IOPT.LT.0 ) - NO EXTRAPOLATION TAKES PLACE VALUES SET
C                             TO ZERO (CARE IF LOG OF OUTPUT IS NEEDED).
C          IF ( IOPT.GT.7 ) PROGRAM STOPS
C         -------------------------------------------------------------
C
C          THIS SUBROUTINE IS AN AMENDED  AND STRUCTURED VERSION OF  THE
C          SUBROUTINE  'ESPLINE'  WRITTEN BY  H.P. SUMMERS,   JET   26TH
C          OCTOBER 1989.   IT REMOVES THE COMMON BLOCK  /IONSPL/ ,   THE
C          SWITCHES 'ISW & ISW2' AND ALSO THE CASE FOR THE INTERPOLATION
C          OF CHARGE STATE VALUES.   IT INTRODUCES THE FEATURE  THAT  AN
C          ARRAY OF INPUT  'X-VALUES'  CAN BE  INTERPOLATED/EXTRAPOLATED
C          IN ONE CALL.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          FINTX      ------    EXTERNAL  REAL*8  FUNCTION,   USED  TO
C                               TRANSFORM X-COORDINATES.
C          XXHUNT     ------    SEARCH ROUTINE FOR FINDING INTERVAL
C                               CONTAINING A PRESCRIBED VALUE IN A
C                               MONOTONIC VECTOR.  INITIAL GUESSES ARE
C                               USED TO SPEED THE SEARCH.
C
C
C AUTHOR:   LORNE D. HORTON (IPP GARCHING)
C           L5.213
C           IPP EXT. 1635
C DATE:     18/03/03
C
C-----------------------------------------------------------------------
C Notes:  AS FOR 'XXSPLE' BUT WITH 'LSETY' ADDED TO ALLOW
C         EXTERNAL SAVING OF SPLINE COEFFICIENTS (FOR
C         EXAMPLE, WHEN IT IS NECESSARY TO HOLD MORE THAN
C         ONE SPLINE RESULT AT A TIME). IN ADDDITION, A
C         HUNT ALGORITHM 'XXHUNT' FOR SPEEDING EVALUATION
C         HAS BEEN ADDED.
C
C
C XXSPLE COMMENTS
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     14/01/91 - ADAS91: AS FOR 'XXSPLN' BUT WITH 'LINTRP()' ADDED
C
C VERSION: 	1.2
C 
C MODIFIED: LORNE HORTON (JET) 				DATE: 25/10/97
C           - ADDED IOPT CHOICES 5, 6 AND 7
C
C VERSION: 	1.3
C 
C MODIFIED: Martin O'Mullane (JET) 			DATE: 2/6/99
C           - SAVE nin0 and inter variables also. All compilers, ie
C             especially g77, do not automatically save (or initialise 
C             variables to zero).
C
C-----------------------------------------------------------------------
C
C VERSION  : 1.1                          
C DATE     : 18-03-2003
C MODIFIED : Lorne Horton
C              - First version.
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NKNOTS       , NIOPT
C-----------------------------------------------------------------------
      PARAMETER( NKNOTS = 101 , NIOPT = 7 )
C-----------------------------------------------------------------------
      INTEGER    IOPT         , NIN       , NOUT      , NOPT
      INTEGER    I            , NIN0      , K         , INTER
C-----------------------------------------------------------------------
      REAL*8     FINTX
      REAL*8     XK           , XKK
      REAL*8     T1           , T2         , T3       , T4   ,
     &           DL1          , DL2        , DL3
C-----------------------------------------------------------------------
      LOGICAL    LSETX        , LSETY      , LEXTRP
C-----------------------------------------------------------------------
      REAL*8     XIN(NIN)       , YIN(NIN)       ,
     &           XOUT(NOUT)     , YOUT(NOUT)     ,
     &           X(NIN)         , DY(NIN)        , Q(NIN)    ,
     &           D1(NIN)        , D2(NIN)        , D3(NIN)
      REAL*8     QVAL(0:NIOPT)  , D2VAL(0:NIOPT) ,
     &           D3VAL(0:NIOPT) , UVAL(0:NIOPT)
      REAL*8     HINTER         , AGRL(4)
      REAL*8     DELY(NKNOTS)   , H(NKNOTS)      , U(NKNOTS)
C-----------------------------------------------------------------------
      LOGICAL    LINTRP(NOUT)   , LUVAL(0:NIOPT)
C-----------------------------------------------------------------------
      DATA  QVAL / -0.5    , -0.5    ,  0.0    ,  0.0    ,  0.0    ,
     &              0.0    ,  0.0    ,  0.0    / ,
     &      D2VAL/  1.5    ,  1.5    ,  0.0    ,  0.0    ,  0.0    ,
     &              0.0    ,  0.0    ,  0.0    / ,
     &      D3VAL/  0.0    ,  0.0    ,  0.0    , -0.5    ,  0.0    ,
     &             -4.5    ,  0.5    , -3.5    / ,
     &      UVAL /  0.0    , -1.5    ,  1.0    , -1.5    ,  0.0    ,
     &             -1.5    ,  0.0    ,  0.0    /
      DATA  LUVAL/ .TRUE.  , .FALSE. , .FALSE. , .FALSE. , .TRUE.  ,
     &             .FALSE. , .TRUE.  , .TRUE.  /
      DATA  NOPT / -1     /
C-----------------------------------------------------------------------
      SAVE QVAL  , D2VAL , D3VAL , UVAL , LUVAL
      SAVE NOPT
C-----------------------------------------------------------------------
C***********************************************************************
      IF (NKNOTS.LT.NIN)
     &                 STOP ' XXSPLE ERROR: TOO MANY KNOTS REQUESTED'
      IF (IOPT.GT.NIOPT)
     &                 STOP ' XXSPLE ERROR: INVALID IOPT VALUE ENTERED'
C-----------------------------------------------------------------------
      IF (IOPT.LT.0) THEN
         LEXTRP = .FALSE.
         IF (NOPT.NE.0)    LSETX=.TRUE.
      ELSE
         LEXTRP = .TRUE.
         IF (IOPT.NE.NOPT) LSETX=.TRUE.
      ENDIF
C-----------------------------------------------------------------------
      IF (LSETX) THEN
C
C***********************************************************************
C SET UP PARAMETERS RELATING TO 'XIN'-AXIS
C***********************************************************************
C
         LSETY = .TRUE.
         NOPT  = MAX0( 0 , IOPT )
C-----------------------------------------------------------------------
         DO 1 I=1,NIN
            X(I)=FINTX(XIN(I))
    1    CONTINUE
C-----------------------------------------------------------------------
         H(2)  = X(2)-X(1)
         Q(1)  = QVAL(NOPT)
         D2(1) = D2VAL(NOPT)/H(2)
         D3(1) = D3VAL(NOPT)
         NIN0=NIN-1
C-----------------------------------------------------------------------
         DO 2 I=2,NIN0
            H(I+1) = X(I+1) - X(I)
            T1     = 1.0 / ( H(I+1) + H(I) )
            T2     = H(I+1) * T1
            T3     = 1.0 - T2
            T4     = 1.0 / ( ( T2 * Q(I-1) ) + 2.0 )
            Q(I)   = -T3 * T4
            D1(I)  = ( 3.0 * T4 * T2 ) / H(I)
            D2(I)  = ( 3.0 * T4 * T3 ) / H(I+1)
            D3(I)  =  T2 * T4
    2    CONTINUE
C-----------------------------------------------------------------------
         T4      = 1.0 / ( Q(NIN0) + 2.0 )
         D1(NIN) = ( 3.0 * T4 ) / H(NIN)
         D3(NIN) = T4
C-----------------------------------------------------------------------
         LSETX=.FALSE.
C***********************************************************************
      ENDIF
C-----------------------------------------------------------------------
      IF (LSETY) THEN
C
C***********************************************************************
C SET UP CUBIC SPLINE DERIVATIVES
C***********************************************************************
C
         DELY(2) = YIN(2) - YIN(1)
         U(1)    = ( D2(1) * DELY(2) ) + D3(1)
         NIN0    = NIN-1
C-----------------------------------------------------------------------
         DO 3 I=2,NIN0
            DELY(I+1) = YIN(I+1) - YIN(I)
            U(I) = (D1(I)*DELY(I)) + (D2(I)*DELY(I+1)) - (D3(I)*U(I-1))
    3    CONTINUE
C-----------------------------------------------------------------------
         IF (LUVAL(NOPT)) THEN
            U(NIN) = ( D1(NIN)*DELY(NIN) ) - ( D3(NIN)*U(NIN0) )
         ELSE
            U(NIN) = UVAL(NOPT)
         ENDIF
C-----------------------------------------------------------------------
         DY(NIN) = U(NIN)
C-----------------------------------------------------------------------
         DO 4 I=NIN0,1,-1
            DY(I)  = ( Q(I) * DY(I+1) ) + U(I)
    4    CONTINUE
C-----------------------------------------------------------------------
         LSETY=.FALSE.
C***********************************************************************
      ENDIF
C
C***********************************************************************
C SET UP PARAMETERS RELATING TO THE REQUESTED 'XOUT' ARRAY VALUES
C***********************************************************************
C
C-----------------------------------------------------------------------
C SET INTER=0 TO FORCE FULL BISECTION SEARCH FOR FIRST OUTPUT VALUE AND
C THEN USE THE PREVIOUSLY FOUND INTERVAL AS A STARTING POINT FOR THE
C NEXT SEARCH
C-----------------------------------------------------------------------
      INTER = 0
C-----------------------------------------------------------------------
      DO 5 K=1,NOUT
         XK  = XOUT(K)
         XKK = FINTX(XK)
C-----------------------------------------------------------------------
C HUNT FOR THE INTERVAL CONTAINING XK
C-----------------------------------------------------------------------
         CALL XXHUNT(XIN,NIN,XK,INTER)
C-----------------------------------------------------------------------
C EXTRAPOLATE: HIGH 'XOUT' VALUE - IF EXTRAPOLATION SWITCHED ON
C-----------------------------------------------------------------------
         IF (INTER.EQ.NIN) THEN
            INTER     = NIN-1
            LINTRP(K) = .FALSE.
            AGRL(1)   = 0.0
            AGRL(3)   = 0.0
            IF (LEXTRP) THEN
               AGRL(2)   = XKK-X(NIN)
               AGRL(4)   = 1.0
            ELSE
               AGRL(2)   = 0.0
               AGRL(4)   = 0.0
            ENDIF
C-----------------------------------------------------------------------
C EXTRAPOLATE: LOW 'XOUT' VALUE  - IF EXTRAPOLATION SWITCHED ON
C-----------------------------------------------------------------------
         ELSEIF (INTER.EQ.0)   THEN
            INTER     = 1
            LINTRP(K) = .FALSE.
            AGRL(2)   = 0.0
            AGRL(4)   = 0.0
            IF (LEXTRP) THEN
               AGRL(1)   = XKK-X(1)
               AGRL(3)   = 1.0
            ELSE
               AGRL(1)   = 0.0
               AGRL(3)   = 0.0
            ENDIF
C-----------------------------------------------------------------------
C INTERPOLATE:
C-----------------------------------------------------------------------
         ELSE
            LINTRP(K) = .TRUE.
            HINTER    = X(INTER+1) - X(INTER)
            DL1       =  ( X(INTER+1) - XKK    ) / HINTER
            DL2       =  1.0 - DL1
            DL3       =  HINTER * DL1 * DL2
            AGRL(1)   =  DL1 * DL3
            AGRL(2)   = -DL2 * DL3
            AGRL(3)   =  DL1 * DL1 * ( 1.0 + DL2 + DL2 )
            AGRL(4)   =  1.0 - AGRL(3)
         ENDIF
C-----------------------------------------------------------------------
C EVALUATE 'YOUT'-VALUE FOR REQUESTED 'XOUT'-VALUE
C-----------------------------------------------------------------------
         YOUT(K) = ( AGRL(1)* DY(INTER) ) + ( AGRL(2)* DY(INTER+1) )
     &           + ( AGRL(3)*YIN(INTER) ) + ( AGRL(4)*YIN(INTER+1) )
C-----------------------------------------------------------------------
    5 CONTINUE
C***********************************************************************
      RETURN
      END
