CX UNIX - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/r8erfc.for,v 1.1 2004/07/06 14:41:14 whitefor Exp $ Date $Date: 2004/07/06 14:41:14 $
CX
       function r8erfc( x )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 function: r8erfc *********************
c
c  purpose:  calculates the error function erfc(x)   
c
c  calling program: various
c
c
c
c  input : (i*4)  x      = independent variable.
c  output: (r*8)  r8erfc = erfc(x).
c
c  routines:
c          none
c
c  author:  Hugh Summers, University of Strathclyde
c           JA7.08
c           tel. 0141-548-4196
c
c  date:    15/01/02
c
c  update:
c
c-----------------------------------------------------------------------
       real*8    x    , r8erfc   , z    , t
c-----------------------------------------------------------------------

       z = dabs(x)
       t = 1.0d0/(1.0d0+0.5d0*z)
       r8erfc = t*dexp(-z*z-1.26551223d0+t*(1.00002368d0+t*(.37409196d0+
     &          t*(.09678418d0+t*(-.18628806d0+t*(.27886807d0+
     &          t*(-1.13520398d0+t*(1.48851587d0+t*(-.82215223d0+
     &          t*.17087277)))))))))
       if(x.lt.0.0d0)r8erfc=2.0d0-r8erfc
       
       return
       
      end       
       
