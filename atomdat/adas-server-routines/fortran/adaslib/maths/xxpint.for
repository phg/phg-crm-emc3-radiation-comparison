CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxpint.for,v 1.1 2004/07/06 15:38:16 whitefor Exp $ Date $Date: 2004/07/06 15:38:16 $
CX
      subroutine xxpint(ndim, xa, ya, x, y)

      implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 FUNCTION: XXPINT *********************
C
C  PURPOSE:  Order ndim polynominal interpolation.   
C
C  CALLING PROGRAM: adas408
C
C  FUNCTION:
C
C  input : (i*4)  ndim     = number of enteries in input arrays.
C  input : (r*8)  xa       = independent variable.
C  input : (r*8)  ya       = dependent variable.
C  input : (r*8)  x        = use requested x value.
C  output: (r*8)  y        = interpolated y value.
C
C  NOTES: Assume the starting value for y is ya(2).
C         Based on polint.for of Numerical Recipes.
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          i4unit     ADAS      Fetch unit number for message output
C
C
C  VERSION  : 1.1                          
C  DATE     : 23-07-2003
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C-----------------------------------------------------------------------
       integer   nmax
C-----------------------------------------------------------------------
       parameter (nmax = 3 )
C-----------------------------------------------------------------------
       integer   i4unit    , ndim     , i    , m    , ns
C-----------------------------------------------------------------------
       real*8    x         , y        , dy   , den   , ho   , hp   , w
C-----------------------------------------------------------------------
       real*8    xa(ndim)  , ya(ndim)
       real*8    c(nmax)   , d(nmax)
C-----------------------------------------------------------------------
            
      ns = 2
      do i = 1, ndim
        c(i) = ya(i)
        d(i) = ya(i)
      end do
      
      y  = ya(ns)
      ns = ns - 1
      do m = 1, ndim-1
        
        do i = 1, ndim-m
        
          ho  = xa(i) - x
          hp  = xa(i+m) - x
          w   = c(i+1) - d(i)
          den = ho - hp
          
          if (den.EQ.0.0D0) then
            write(i4unit(-1),1001) xa(i)
            write(i4unit(-1),1002)
          endif
          
          den  = w  / den
          d(i) = hp * den
          c(i) = ho * den
        
        end do
        
        if (2*ns.LT.ndim-m) then
          dy = c(ns+1)
        else
          dy = d(ns)
          ns = ns-1
        endif
        y = y + dy
        
      end do
      
C-----------------------------------------------------------------------
 1001 FORMAT(1X,31('*'),' XXPINT ERROR ',30('*')//
     &       1X,'X values within roundoff error: ',1p,e12.5)
 1002 format(/1x,29('*'),' PROGRAM TERMINATED ',29('*'))
C-----------------------------------------------------------------------
      end
