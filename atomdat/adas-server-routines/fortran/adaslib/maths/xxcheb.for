CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxcheb.for,v 1.1 2004/07/06 15:32:11 whitefor Exp $ Date $Date: 2004/07/06 15:32:11 $
CX
      SUBROUTINE XXCHEB( X , Y , N , A , M1 , REF )
      IMPLICIT NONE
C--------------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXCHEB *********************
C
C  PURPOSE:           CARRY OUT CHEBYSCHEV POLYNOMIAL FIT ALGORITHM
C                     (DIRECT REPLACEMENT FOR NAG MINIMAX POLYNIMIAL
C                      COEFT. ROUTINE E02ACF - has same argument list).
C
C  REFERENCE:         TRANSLATION FROM ALGOL CHEBYSCHEV POLYNOMIAL FIT
C                     ALGORITHM BY -
C                     Boothroyd,
C                     Communications of the ACM, 10(12), December 1967
C
C  CALLING PROGRAMS:  XXMNMX
C
C  SUBROUTINE:
C
C  INPUT : (R*8)  X()     = Array of Input X Co-ordinates
C                           Dimension = N
C  INPUT : (R*8)  Y()     = Array of Input Y Co-ordinates
C                           Dimension = N
C  INPUT : (I*4)  N       = Number of Data Points
C                           Dimension = N
C  OUTPUT: (R*8)  A()     = Coefficients of the Fitted Polynomial.
C                           Dimension = M1
C  INPUT : (I*4)  M1      = M + 1 = The order of the polynomial to be
C                           found + 1. The highest order term is
C                           A(M1)*X(M) !!!
C  OUTPUT: (R*8)  REF     = Final Reference Deviation.
C
C  NOTES:     Based on Revision 1.2 (13:26:01 13OCT94) of XXCHEB by CJW
C             from the University of Strathclyde with the following
C             bug correction -
C               The line (within the DO 100 loop):
C                  IF (I.NE.J)
C               was corrected to:
C                  IF (I.NE.RJ)
C
C  ROUTINES:  None
C
C  AUTHOR:    CJW (University of Strahclyde)
C  EDITED BY: PAUL BRIDEN (Tessella Support Services plc)
C             K1/0/37
C             JET ext. 5023
C
C  DATE:      31/10/94
C
C--------------------------------------------------------------------------
C
C--------------------------------------------------------------------------
      INTEGER    MAXNUM
C--------------------------------------------------------------------------
      PARAMETER( MAXNUM = 100 )
C--------------------------------------------------------------------------
      INTEGER  N            , M1
      INTEGER  M            , I            , J             ,
     &         K            , RI           , I1            ,
     &         IMAX         , RJ           , J1
C--------------------------------------------------------------------------
      REAL*8   REF
      REAL*8   D            , H            , AI1           ,
     &         RHI1         , DENOM        , AI            ,
     &         RHI          , XJ           , HMAX          ,
     &         HIMAX        , XI           , HI            ,
     &         ABSHI        , NEXTHI       , PREVH
C--------------------------------------------------------------------------
      INTEGER  R(0:MAXNUM)
C--------------------------------------------------------------------------
      REAL*8   X(N)         , Y(N)         , A(M1)
      REAL*8   RX(0:MAXNUM) , RH(0:MAXNUM) , AL(0:MAXNUM)
C
C--------------------------------------------------------------------------
C
C--------------------------------------------------------------------------
C TEST FOR VALID INPUTS
C 1) CHECK THAT WE HAVE MORE DATA POINTS THAN THE POLYNOMIAL ORDER
C--------------------------------------------------------------------------
C
      IF (M1.GE.MIN(N,MAXNUM))
     & STOP ' XXCHEB ERROR: ORDER OF POLYNOMIAL > NUMBER OF DATA POINTS'
C
C--------------------------------------------------------------------------
C 2) CHECK FOR STRICT ORDERING OF INPUT DATA X(I)
C--------------------------------------------------------------------------
C
         DO 10 I = 1, N-1
            IF (X(I).GE.X(I+1))
     &         STOP ' XXCHEB ERROR: X VALUES NOT IN CORRECT SEQUENCE.'
   10    CONTINUE
C
C--------------------------------------------------------------------------
C     WARNING - the order of the polynomial is defined so that
C     M is the power of the highest order term in the EXTERNAL
C     code, but its coefficient is A(M+1)
c     This is different from the original algol code, where
C     M was the same for both.  In order to use the original algorithm
C     the meaning of the symbols is that of the algol code, until we
C     copy it back to the external array
C     we use array AL(0:MAXNUM) where the original algol used A
C     We copy AL into A, with an offset of 1 to map the 0-origin
C     array AL to the 1-origin A
C--------------------------------------------------------------------------
C
      M     = M1 - 1
      PREVH = 0
C
C--------------------------------------------------------------------------
C INDEX VECTOR FOR INITIAL REFERENCE SET
C--------------------------------------------------------------------------
C
      R(0)  = 1
      R(M1) = N
      D     = (N-1) / M1
      H     = D
C
         DO 20 I = 1, M
            R(I) = H + 1
            H    = H + D
   20    CONTINUE
C
   30 H = -1
C
C--------------------------------------------------------------------------
C SELECT M + 2 REFERENCE PAIRS AND SET ALTERNATING DEVIATION VECTOR
C--------------------------------------------------------------------------
C
         DO 40 I = 0, M1
            RI    = R(I)
            RX(I) = X(RI)
            AL(I) = Y(RI)
            H     = -H
            RH(I) = H
   40    CONTINUE
C
C--------------------------------------------------------------------------
C COMPUTE M + 1 LEADING DIVIDED DIFFERENCES
C--------------------------------------------------------------------------
C
         DO 50 J = 0, M
            I1   = M1
            AI1  = AL(I1)
            RHI1 = RH(I1)
               DO 60 I = M, J, -1
                  DENOM  = RX(I1) - RX(I-J)
                  AI     = AL(I)
                  RHI    = RH(I)
                  AL(I1) = (AI1  - AI)  / DENOM
                  RH(I1) = (RHI1 - RHI) / DENOM
                  I1     = I
                  AI1    = AI
                  RHI1   = RHI
   60          CONTINUE
   50    CONTINUE
C
C--------------------------------------------------------------------------
C EQUATE (M+1)TH DIFFERENCE TO ZERO TO DETERMINE H
C--------------------------------------------------------------------------
C
      H = -AL(M1) / RH(M1)
C
C--------------------------------------------------------------------------
C WITH H KNOWN, COMBINE THE FUNCTION AND DEVIATION DIFFERENCES
C--------------------------------------------------------------------------
C
         DO 70 I = 0, M1
            AL(I) = AL(I) + RH(I)*H
   70    CONTINUE
C
C--------------------------------------------------------------------------
C COMPUTE POLYNOMIAL COEFFICIENTS
C--------------------------------------------------------------------------
C
         DO 80 J = M - 1, 0, -1
            XJ = RX(J)
            I  = J
            AI = AL(I)
               DO 90 I1 = J + 1, M
                  AI1   = AL(I1)
                  AL(I) = AI - ( XJ * AI1 )
                  AI    = AI1
                  I     = I1
   90          CONTINUE
   80    CONTINUE
C
C--------------------------------------------------------------------------
C IF THE REFERENCE DEVIATION IS NOT INCREASING MONOTOMICALLY THEN EXIT
C--------------------------------------------------------------------------
C
      HMAX = ABS(H)
C
         IF (HMAX.LE.PREVH) THEN
            AL(M1) = -HMAX
            GO TO 999
         ENDIF
C
C--------------------------------------------------------------------------
C FIND THE INDEX, IMAX, AND VALUE, HMAX, OF THE LARGEST ABSOLUTE
C ERROR FOR ALL SAMPLE POINTS
C--------------------------------------------------------------------------
C
      PREVH  = HMAX
      AL(M1) = PREVH
      IMAX   = R(0)
      HIMAX  = H
      J      = 0
      RJ     = R(J)
C
         DO 100 I = 1, N
            IF (I.NE.RJ) THEN
               XI    = X(I)
               HI    = AL(M)
                  DO 110 K = M - 1, 0, -1
                     HI = HI * XI + AL(K)
  110             CONTINUE
               HI    = HI - Y(I)
               ABSHI = ABS(HI)
                  IF (ABSHI.GT.HMAX) THEN
                     HMAX  = ABSHI
                     HIMAX = HI
                     IMAX  = I
                  ENDIF
            ELSE
                  IF (J.LT.M1) THEN
                     J  = J + 1
                     RJ = R(J)
                  ENDIF
            ENDIF
  100    CONTINUE
C
C--------------------------------------------------------------------------
C IF THE MAXIMUM ERROR OCCURS AT A NON-REFERENCE POINT,
C EXCHANGE THIS POINT WITH THE NEAREST REFERENCE POINT
C HAVING AN ERROR OF THE SAME SIGN AND REPEAT
C--------------------------------------------------------------------------
C
         IF (IMAX.NE.R(0)) THEN
               DO 120 I = 0, M1
                  IF (IMAX.LT.R(I)) GOTO 130
  120          CONTINUE
            I = M1
  130          IF ((I-I/2*2).EQ.0) THEN
                  NEXTHI = H
               ELSE
                  NEXTHI = -H
               ENDIF
               IF ((HIMAX*NEXTHI) .GT. 0) THEN
                  R(I) = IMAX
               ELSE
                     IF (IMAX.LT.R(0)) THEN
                        J1   = M1
                           DO 140 J = M, 0, -1
                              R(J1) = R(J)
                              J1    = J
  140                      CONTINUE
                        R(0) = IMAX
                     ELSE
                           IF (IMAX.GT.R(M1)) THEN
                              J      = 0
                                 DO 150 J1 = 1, M1
                                    R(J) = R(J1)
                                    J    = J1
  150                            CONTINUE
                              R(M1)  = IMAX
                           ELSE
                              R(I-1) = IMAX
                           ENDIF
                     ENDIF
               ENDIF
            GOTO 30
         ENDIF
C
  999    DO 160 I = 1,M1
            A(I) = AL(I-1)
  160    CONTINUE
C
      REF = AL(M1)
C
      RETURN
      END
