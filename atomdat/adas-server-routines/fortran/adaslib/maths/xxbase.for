      subroutine xxbase( a , ia , base_a , value , code )
      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxbase ********************
c
c  purpose: to produce a number from representation as a*base^ia
c           protected against underflow and overflow.
c
c  input : (r*8)  a       = mantissa part of number
c  input : (r*8)  ia      = characteristic part of number
c  input : (c*(*))code    = 'zero' => zero a delivered if underflow
c                           'min'  => minimum number delivered
c  output: (r*8)  value   = evaluated number
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          xxbasr     adas      convert scaled number to new base
c          i4unit     adas      fetch unit number for output of messages
c
c author:  Hugh Summers
c          JET K1/1/61
c          Tel. 01235-46-4459
c date:    07-05-2008
c
c
c version  : 1.1
c date     : 07-05-2008
c modified : Hugh Summers
c              - first version.
c-----------------------------------------------------------------------
       real*8    vmin     , base_b
c-----------------------------------------------------------------------
       parameter ( vmin = 1.0d-74 , base_b = 2.718281828d0)
c-----------------------------------------------------------------------
       integer   ia    , ib
c-----------------------------------------------------------------------
       real*8    a     , base_a    , b     , value
       real*8    tmin  , t
c-----------------------------------------------------------------------
       character code*(*)
c-----------------------------------------------------------------------
       call xxbasr(a , ia, base_a, b, ib, base_b)

       t=log(abs(b))+ib
       tmin = log(vmin)
       if(t.gt.-tmin) then
           value=sign(1.0d0,b)/vmin
       elseif((t.lt.tmin).and.((code.eq.'zero').or.
     &                         (code.eq.'zero')))then
           value=0.0d0
       elseif((t.lt.tmin).and.((code.eq.'min').or.
     &                         (code.eq.'min')))then
           value=sign(1.0d0,b)*vmin
       else
           value=b*base_b**(ib)
       endif
c-----------------------------------------------------------------------
       return
      end
