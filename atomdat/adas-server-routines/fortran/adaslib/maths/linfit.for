CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/linfit.for,v 1.1 2004/07/06 14:11:15 whitefor Exp $ Date $Date: 2004/07/06 14:11:15 $
CX
       SUBROUTINE LINFIT(X,XSA,Y,YSA,ICT)                               
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: LINFIT *********************
C
C PURPOSE:
C  SUBROUTINE TO PERFORM LINEAR INTERPOLATION                           
C
C
C  INPUT
C      X     = REQUIRED X-VALUE
C      XSA(I)= X-VALUES
C      YSA(I)= Y-VALUES
C      ICT   = NUMBER OF VALUES
C  OUTPUT
C      Y     = RETURNED Y-VALUE
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C
       DIMENSION XSA(10),YSA(10)                                        
       IF(ICT.GT.1)GO TO 10                                             
       Y=YSA(1)                                                         
       RETURN                                                           
   10  IF(X.GT.XSA(1))GO TO 20                                          
       Y=YSA(1)                                                         
       RETURN                                                           
   20  IF(X.LT.XSA(ICT))GO TO 30                                        
       Y=YSA(ICT)                                                       
       RETURN                                                           
   30  I=0                                                              
   40  I=I+1                                                            
       IF(X.GT.XSA(I))GO TO 40                                          
       Y=((X-XSA(I))*YSA(I-1)-(X-XSA(I-1))*YSA(I))/(XSA(I-1)            
     &-XSA(I))                                                          
       RETURN                                                           
      END                                                              
