C UNIX-IDL - SCCS info: Module @(#)f21.for	1.1 Date 03/18/03
C
      function r8f21(a,b,c,d,eps)
      implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 function: r8f21.for ********************
C
C  Purpose:  Evaluates series expansion of hypergeometric function
C            F(a,b;c;d).
C
C  Function:
C
C  Input : (r*8)  a        = 1st numerator parameter a
C  Input : (r*8)  b        = 2nd numerator parameter b
C  Input : (r*8)  d        = 3rd denominator parameter c
C  Input : (r*8)  d        = independent variable (equiv x)
C  Input : (r*8)  eps      = accuracy parameter
C
C  Output: (r*8)  r8f21    = F(a,b;c;d)
C
C  Routines:
C          Routine    Source    Brief description
C          ------------------------------------------------------------
C          i4unit     ADAS     Specifies unit for warning message ouput
C
C  Author:  H. P. summers, University of Strathclyde
C           ja7.08
C           tel. 0141-548-4196
C
C  Date:   24/02/03
C
C  Update: HP Summers    24/05/04
C          restructured and added standard warning
C
C  Update: AD Whiteford  16/03/05
C          renamed to r8f21
C
C  Update: HP Summers    17/04/07
C          Corrected description comments
C
C-----------------------------------------------------------------------
      integer  i4unit
      integer  i
C-----------------------------------------------------------------------
      real*8   r8f21  , a     , b     , c     , d     , eps
      real*8   t      , dd    , sum   , tn1   , tn2   , at    ,
     &         as     , ai
C-----------------------------------------------------------------------
      t=(a*b*d)/c
      dd=1.0d0/(1.0d0-d)
      sum=1.0d0+t
      tn1=0.0d0
      i=1
  3   ai=i
      t=t*(a+ai)*(b+ai)*d/((c+ai)*(1.0d0+ai))
      tn2=t*dd
      r8f21=sum+tn2
      sum=sum+t
      at=dabs(t+tn2-tn1)
      as=dabs(r8f21)*eps
      if(as.gt.at)return
      tn1=tn2
      i=i+1
      if(i.le.300)go to 3
      write(i4unit(-1),2000)' Failed to converge in r8f21'
      write(i4unit(-1),2001)
      return
C-----------------------------------------------------------------------
 2000 format(1x,32('*'),'  r8f21 warning   ',32('*')//
     &       2x,a )
 2001 format(/1x,29('*'),' program continues ',29('*'))
C-----------------------------------------------------------------------
      END
