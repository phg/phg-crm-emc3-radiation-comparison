CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/xxmino.for,v 1.1 2004/07/06 15:37:27 whitefor Exp $ Date $Date: 2004/07/06 15:37:27 $
CX
      SUBROUTINE XXMINO( ND , A , B )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXMINO *********************
C
C  PURPOSE: INVERTS A MATRIX.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT : (I*4)  ND      = DIMENSION OF MATRICES 'A' AND 'B'.
C  INPUT : (R*8)  A(,)    = MATRIX TO BE INVERTED.
C                           1ST DIMENSION: ND
C                           2ND DIMENSION: ND
C
C  OUTPUT: (R*8)  B(,)    = INVERTED MATRIX.
C                           1ST DIMENSION: ND
C                           2ND DIMENSION: ND
C
C
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C
C          (R*8)  DINT    = DUMMY ARGUMENT TO 'XXMINV'.
C
C          (L*4)  LSOLVE  = .FALSE. => 'XXMINV' ONLY INVERTS MATRIX.
C
C  OUTPUT: (R*8)  DUM()   = DUMMY ARGUMENT TO 'XXMINV'.
C                           DIMENSION: ND
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXMINV     ADAS      BOTH INVERTS A MATRIX AND SOLVES A SET
C                               OF SIMULTANEOUS LINEAR EQUATIONS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    10/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER   ND
      INTEGER   I       , J
C----------------------------------------------------------------------
      REAL*8    DINT
C----------------------------------------------------------------------
      LOGICAL   LSOLVE
C----------------------------------------------------------------------
      REAL*8    A(ND,ND)  , B(ND,ND)
      REAL*8    DUM(100)
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
C COPY MATRIX 'A' TO MATRIX 'B'.
C----------------------------------------------------------------------
C
      DO 1 I = 1 , ND
         DO 2 J = 1 , ND
            B(I,J) = A(I,J)
    2    CONTINUE
    1 CONTINUE
C
C----------------------------------------------------------------------
C INVERT MATRIX 'B'.
C----------------------------------------------------------------------
C
      LSOLVE = .FALSE.
      CALL XXMINV( LSOLVE , ND , ND , B , DUM , DINT )
C
C----------------------------------------------------------------------
C
      RETURN
      END
