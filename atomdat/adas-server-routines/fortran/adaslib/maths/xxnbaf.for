      SUBROUTINE XXNBAF (M, NCAP7 , X, Y, W, LAMDA , B, A, DIAG, C ,
     &                   SS , IFAIL )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  PURPOSE: Determines a least-square cubic spline approximation s(x)
C           to the set of data points (x_r, y_r) with weights w_r,
C	    for r=1,2,...,m.
C
C           The value of NCAP7 = ncap+7, where ncap is the number of
C	    intervals of the spline (number of interior knots + 1),
C           and the values of the knots LAMDA(5), LAMDA(6), ...,
C	    LAMDA(NCAP7-4), interior to the data interval, are
C	    prescribed by the user.
C
C	    s has the property that it minimises ss, the sum of the
C	    squares of the weighted residuals eps(r)
C
C		      eps(r) = w(r) * ( s(x(r)) - y(r) ).
C
C	    The procedure produces the minimising value of ss and
C	    the coefficients c(1), c(2),...,c(q), where q=ncap+3=NCAP7-4,
C	    in the B-spline representation
C
C		s(x) = c(1)*N1(x) + c(2)*N2(x) + ... + c(q)*Nq(x) .
C
C	    Here Ni(x) (i=1,2,...,q) denotes the normalised B-spline
C	    of degree 3 defined upon the knots lamda(i-4), lamda(i-3),
C	    lamda(i-2), lamda(i-1), and lamda(i).
C
C  CALLING PROGRAM: VARIOUS
C
C  SUBROUTINE:
C
C  INPUT: (I*4) M            = The number of data points.
C                              CONSTRAINT: M >= MDIST >= 4, where
C                              MDIST is the number of distinct x
C                              values in the data.
C
C  INPUT: (I*4) NCAP7        = NBAR+7, where NBAR is the number of
C                              intervals of the spline (number
C                              of interior knots +1, i.e. the knots 
C                              strictly in the range X(1) to X(M)) 
C                              over which the spline is defined.
C                              CONSTRAINT: 8<= NCAP7 <= MDIST+4,
C                              where MDIST is the number of distinct
C                              x values in the data. 
C
C  INPUT:  (R*8) X()         = The values x_r of the independent variable 
C                              (abscissa), for r=1,2,...,m.
C                              CONSTRAINT: 
C                                 X(1)<=X(2)<=...<=X(M)          
C
C  INPUT:  (R*8) Y()         = The values y_r of the dependent variable 
C                              (ordinate), for r=1,2,...,m.
C
C  INPUT:  (R*8) W()         = The values w_r of the weights,           
C                              for r=1,2,...,m.
C
C  INPUT:  (R*8) LAMDA()     = LAMDA(i) must be set to the (i-4)th
C                              (interior) knot, i=5,6,...,nbar+3.
C                              CONSTRAINT:                          
C                                 X(1) < LAMDA(5) <= LAMDA(6) ... <=
C                                       ... <=LAMDA(NCAP7-4) < X(M) .
C
C  INPUT:  (I*8) IFAIL       = 0 : stop if any error
C                            = 1 : continue if non-fatal error.
C
C  OUTPUT: (R*8) LAMDA()     = Input values are unchanged, and
C                              LAMDA(i), for i=1,2,3,4,NCAP7-3,
C                              NCAP7-2,NCAP7-1,NCAP7 contains the
C                              additional exterior knots introduced by
C                              the routine.
C
C
C  OUTPUT: (R*8) C()         = The coefficients of the B-spline N_i(x),
C                              for i=1,2,...,nbar+3. The remaining 
C                              elements (from NBAR+4 to NBAR+7) are not
C                              used.
C
C  OUTPUT: (R*8) SS          = The residual sum of sqaures
C
C  OUTPUT: (I*4) IFAIL       = 0 : no error detected
C                            = 1 : the knots fail to satisfy the condition
C                                   X(1) < LAMDA(5) <= LAMDA(6) <=...
C                                     <= LAMDA(NCAP7-4) < X(M)
C                            = 2 : The weights are not strictly positive
C                            = 3 : The values of X(R), R=1,M are not in
C                                  non-decreasing order.
C                            = 4 : NCAP7 < 8 (so that the number of
C                                  interior knots is negative) or
C                                  NCAP7 > MDIST + 4, where MDIST is the 
C                                  number of distinct x values in the data
C                                  (so that there cannot be unique solution).
C                            = 5 : The conditions specified by Schoenberg
C                                  and Whitney fail to hold for at least
C                                  one subset of the distinct data abscissae.
C                                  That is, there is no subset of NCAP7-4
C                                  strictly increasing values,
C                                  X(R(1)), X(R(2)),..., X(R(NCAP7-4)),
C                                  among theabscissae such that
C
C                                  X(R(1)) < LAMDA(1) < X(R(5)) 
C                                  X(R(2)) < LAMDA(2) < X(R(6)) 
C                                  ...
C                                  X(R(NCAP7-8))<LAMDA(NCAP7-8)<X(R(NCAP7-4)).
C
C                                  This means that there is no unique 
C                                  solution: there are regions containing
C                                  too many knots compared with the
C                                  number of data points.
C
C          (R*8)  B()        = Set of distinct data abscissae
C    
C          (R*8)  WORK2()    = WORKSPACE
C
C          (I*4)  J          = GENERAL INDEX
C          (I*4)  I          = GENERAL INDEX
C          (I*4)  R          = GENERAL INDEX
C          (I*4)  II         = GENERAL INDEX
C          (R*8)  BI         = GENERAL REAL
C          (R*8)  XI         = GENERAL REAL
C
C  ROUTINES:  NONE
C
C  AUTHORS: Alessandro C. Lanzafame, University of Strathclyde
C
C  REFERENCE: Cox, M.G. and Hayes, J.G. "Curve fitting: A Guide and
C             Suite of Algorithms for the Non-specialist User."
C             Report NAC26, National Physical Laboratory, Middlessex,
C             1973.
C
C  DATE:    12 January 1995
C
C  VERSIION: 1.0a
C  Alessandro Lanzafame, 12 January 1995.
C  Directly derived from Algol text.
C  (Error in passing woking variables)
C
C  VERSION 1.0b
C  Alessandro Lanzafame, 15 January 1995.
C  DIAG(1:NCAP7-4) absorbed in matrix A(1:NCAP7-4,1). 
C  Matrix A(1:NCAP7-4,2:4) becomes A(1:NCAP7-4,1:4). This is to easy
C  the passing of workspaces.
C  WORK1 identified with B. WORK2 with A.
C  Corrected index error in checking remaining Schoennber-Whitney 
C  conditions.
C  COMPILING BUT NOT WORKING.
C
C  VERSION 1.0c
C  Alessandro Lanzafame, 15 January 1995.
C  Knots shifted as in original Algol program text.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 06-07-2004
C MODIFIED: Allan Whiteford
C               - Changed name from dxnbaf to xxnbaf.
C
C VERSION  : 1.3			DATE: 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER M, NCAP, NCAP7, IFAIL
      REAL*8  X(M), Y(M), W(M), SS
CX    REAL*8  WORK1(M), WORK2(4*NCAP7)
      REAL*8  LAMDA(-3:NCAP7-4), C(NCAP7)
C-----------------------------------------------------------------------
      INTEGER I, J
C-----------------------------------------------------------------------
      INTEGER R
      REAL*8  BI, B(M), XI, LAMDA0, LAMDA4
C-----------------------------------------------------------------------
      INTEGER II, JOLD, L, U
C-----------------------------------------------------------------------
      REAL*8  A(1:NCAP7-4,2:4), DIAG(1:NCAP7-4)
      REAL*8  ROW(1:4)
      REAL*8  SIGMA, WI
      REAL*8  K1, K2, K3, K4, K5, K6
      REAL*8  D4, D5, D6, D7, D8, D9
      REAL*8  E2, E3, E4, E5
      REAL*8  N1, N2, N3
      REAL*8  CROW, ROWEL, D, S, DP, SINE, COSINE, ACOL, AROW, CCOL
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      NCAP=NCAP7-7
C
C-----------------------------------------------------------------------
C  INITIAL CHECKS
C-----------------------------------------------------------------------
C
C
C check that the knots are ordered
C
      DO J = NCAP-2, 1, -1
         IF (LAMDA(J) .GT. LAMDA(J+1)) THEN
            IF (IFAIL.EQ.0) THEN
               IFAIL = 1
               STOP '*** XXNBAF ERROR: IFAIL = 1 ***'
            ELSE
               RETURN
            ENDIF
         ENDIF
      ENDDO
C
C check that the weights are positive
C
      DO I = 1, M
         IF (W(I).LE.0.0D0) THEN
            IF (IFAIL.EQ.0) THEN
               IFAIL = 2
               STOP '*** XXNBAF ERROR: IFAIL = 2 ***'
            ELSE
               IFAIL = 2
               RETURN
            ENDIF
         ENDIF
      ENDDO
C
C check the data abscissae are ordered, then form the array B(1:R)
C from the array X(1:M). The array B contains the set of distinct
C data abscissae.
C
      B(1) = X(1)
      BI   = B(1)
      J    = 2
      DO I = 2, M
         XI = X(I)
         IF (XI .LT. BI) THEN
            IF (IFAIL.EQ.0) THEN
               IFAIL = 3
               STOP '*** XXNBAF ERROR: IFAIL = 3 ***'
            ELSE
               IFAIL = 3
               RETURN
            ENDIF
         ENDIF
         IF (XI .GT. BI) THEN
            B(J) = XI
            BI   = B(J)
            J    = J + 1
         ENDIF
      ENDDO
      R = J - 1
C
C check that there are sufficient distinct data abscissae for the
C prescribed number of knots and that the number of interior knots
C is positive (NCAP7 > 8)
C
      IF ((R .LT. NCAP7-4) .OR. NCAP7.LT.8) THEN
         IF (IFAIL.EQ.0) THEN
            IFAIL = 4
            STOP '*** XXNBAF ERROR: IFAIL = 4 ***'
         ELSE 
            IFAIL = 4
            RETURN
         ENDIF
      ENDIF
C
C check the first and last s Schoenberg-Whitney conditions 
C (s=min(ncap-1,4))
C
      DO J = 1, 4
	 IF (J .GE. NCAP) GO TO 20
         I = 1 - J
	 IF ( ( B(J) .GE. LAMDA(J) ) .OR.
     &	      ( LAMDA(NCAP-1+I) .GE. B(R+I) ) ) THEN
            IF (IFAIL.EQ.0) THEN
               IFAIL = 5
               STOP '*** XXNBAF ERROR: IFAIL = 5 ***'
            ELSE
               IFAIL = 5
               RETURN
            ENDIF
         ENDIF
      ENDDO
C
C check all the remaining Schoenberg-Whitney conditions
C
      R = R - 4
      I = 4
      DO J = 5, NCAP-1
         LAMDA0 = LAMDA(J)
         LAMDA4 = LAMDA(J-4)

 10      CONTINUE
         I = I + 1
         BI=B(I)
         IF ( ( I .GT. R ) .OR. ( BI .GE. LAMDA0 ) ) THEN
            IF (IFAIL.EQ.0) THEN
               IFAIL = 5
               STOP '*** XXNBAF ERROR: IFAIL = 5 ***'
            ELSE
               IFAIL = 5
               RETURN
            ENDIF
         ENDIF
         IF ( BI .LE. LAMDA4 ) GO TO 10
      ENDDO

 20   CONTINUE

C
C-----------------------------------------------------------------------
C  In order to define the full B-spline basis, augment the prescribed
C  interior knots by knots of multiplicity 4 at each end of the data
C  range
C-----------------------------------------------------------------------
C
      LAMDA(0) = X(1)
      DO II = -1,-3,-1
         LAMDA(II) = LAMDA(II + 1)
      ENDDO
      LAMDA(NCAP) = X(M)
      DO II = NCAP+1, NCAP+3
         LAMDA(II) = LAMDA(II - 1)
      ENDDO
C
C-----------------------------------------------------------------------
C  Initialise an upper triangular matrix of order ncap+3 (=NCAP7-4)
C  and bandwidth 4 and a right-hand side to zero. The main diagonal
C  of the matrix is stored in the array DIAG(1:NCAP7-4), the
C  super-diagonals in the array A(1:NCAP7-4,2:4) and the right-hand
C  side in the array C(1:NCAP7-4). The solution of this system, after
C  all data points have been processed, yields the coefficients of the 
C  B-spline.
C-----------------------------------------------------------------------
C
      DO I = 1, NCAP+3
	 DO II = 2, 4
	    A(I,II) = 0.0D0
	 ENDDO
	 DIAG(I)= 0.0D0
         C(I)   = 0.0D0
      ENDDO
      SIGMA  = 0.0D0
      J      = 0
      JOLD   = 0

      DO I = 1, M
C
C        for the data point (x(i),y(i)) determine an interval
C	 lamda(j-1) <= x <= lamda(j) containing x(i)
C
         WI = W(I)*W(I)
         XI = X(I)

 30      CONTINUE
	 IF ((XI.GE.LAMDA(J)).AND.(J.LE.(NCAP-1))) THEN
            J = J + 1
            GO TO 30
         ENDIF
         IF (J .NE. JOLD) THEN
C
C           Set certain constants relating to the interval
C	    lamda(j-1) <= x <= lamda(j)
C               
	    K1 = LAMDA(J-3)
	    K2 = LAMDA(J-2)
	    K3 = LAMDA(J-1)
	    K4 = LAMDA(J)
	    K5 = LAMDA(J+1)
	    K6 = LAMDA(J+2)

            D4 = 1.0 / (K4 - K1)
            D5 = 1.0 / (K5 - K2)
            D6 = 1.0 / (K6 - K3)
            D7 = 1.0 / (K4 - K2)
            D8 = 1.0 / (K5 - K3)
            D9 = 1.0 / (K4 - K3)

            JOLD = J
         ENDIF

C
C        Compute and store in the array ROW(1:4) the values of
C        the four normalised cubic B-splines which are non-zero
C        at x=x(i)
C
 40      CONTINUE
         E5 = K5 - XI
         E4 = K4 - XI
         E3 = XI - K3
         E2 = XI - K2

         N2 = E3 * D8 * D9
         N1 = E4 * D7 * D9
         N3 = E3 * N2 * D6
         N2 = (E2 * N1 + E5 * N2) * D5
         N1 = E4 * N1 * D4

         ROW(4) = E3 * N3
         ROW(3) = E2 * N2 + (K6 - XI) * N3
         ROW(2) = (XI - K1) * N1 + E5 * N2
         ROW(1) = E4 * N1

         CROW = Y(I)

C
C        Rotate this row into the band triangular system using plane
C        rotations
C
         DO L = 0, 3
            IF (WI .EQ. 0.0D0) GO TO 50
            ROWEL = ROW(L+1)
	    IF (ROWEL .NE. 0.0D0) THEN
	       D = DIAG(J+L)
               S = WI * ROWEL
               DP = D + S * ROWEL
	       DIAG(J+L) = DP
               COSINE = D / DP
               SINE = S / DP
               WI = COSINE * WI
	       DO U =2, 4-L
		  ACOL = A(J+L,U)
		  AROW = ROW(L+U)
		  A(J+L,U)=COSINE*ACOL + SINE*AROW
		  ROW(L+U) = AROW - ROWEL*ACOL
               ENDDO
	       CCOL = C(J+L)
	       C(J+L) = COSINE * CCOL + SINE*CROW
               CROW = CROW - ROWEL * CCOL
            ENDIF
         ENDDO
         SIGMA = SIGMA + WI*CROW*CROW
 50      CONTINUE
      ENDDO
      SS = SIGMA
C
C     Solve the band triangular system for the B-spline coefficients
C
      L = 1
      DO J = NCAP+2, 1, -1
         IF (L .LT. 4) L=L+1
         U = J-1
         S = C(J)
         DO I=2, L
	    S = S - A(J,I)*C(I+U)
	 ENDDO
	 C(J) = S
      ENDDO

      RETURN
C-----------------------------------------------------------------------
      END

