CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/maths/r8fun2.for,v 1.3 2007/04/11 13:01:53 allan Exp $ Date $Date: 2007/04/11 13:01:53 $
CX
      FUNCTION R8FUN2 ( Z )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8FUN2 ******************
C
C  PURPOSE: RETURN 1 / ( Z+1 )
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C          (R*8)  R8FUN2  = FUNCTION NAME
C          (R*8)  Z       = INPUT VALUE
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     13/08/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION  : 1.2                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION  : 1.3                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
      REAL*8 R8FUN2 , Z
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      R8FUN2=1.0/(Z+1.0)
C-----------------------------------------------------------------------
      RETURN
      END
