      SUBROUTINE XXHUNT( XX , N , X , JLO )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXHUNT *********************
C
C  PURPOSE: GIVEN AN ARRAY XX(1:N), AND GIVEN A VALUE X, RETURNS A
C           VALUE JLO SUCH THAT X IS BETWEEN XX(JLO) AND XX(JLO+1).
C
C           XX(1:N) MUST BE MONOTONIC, EITHER INCREASING OR 
C           DECREASING. JLO=0 OR JLO=N IS RETURNED TO INDICATE THAT
C           X IS OUT OF RANGE. JLO ON INPUT IS TAKEN AS THE INITIAL
C           GUESS FOR JLO ON OUTPUT.
C
C  REFERENCE:  NUMERICAL RECIPES: The Art of Scientific Computing
C              (FORTRAN Version).
C              W.H.Press, B.P.Flannery, S.A.Teukolsky & W.T.Vetterling.
C              (Cambridge University Press, Cambridge). 1989. p.233
C              ISBN 0 521 38330 7
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (R*8)  XX      = MONOTONIC INPUT ARRAY
C  INPUT :    (I*4)  N       = INPUT ARRAY 'XX()' LENGTH
C  INPUT :    (R*8)  X       = VALUE WHERE INTERVAL IS TO BE FOUND
C  I/O   :    (I*4)  JLO     = INPUT: INITIAL GUESS FOR INTERVAL INDEX
C                              OUTPU: RETURNED INTERVAL INDEX
C
C             (I*4)  INC     = HUNT INCREMENT
C             (I*4)  JHI     = UPPER BOUND FOR BRACKETING X
C             (I*4)  JM      = MEAN OF JLO AND JHI
C
C             (L*4)  ASCND   = .TRUE. FOR ASCENDING VALUES IN XX,
C                              .FALSE. FOR DESCENDING VALUES
C
C ROUTINES:  NONE
C
C NOTE:
C
C AUTHOR:  LORNE D. HORTON (IPP GARCHING)
C          L5.213
C          IPP EXT. 1635
C
C DATE:    19/03/03
C
C VERSION:  1.1                         DATE: 19/03/03
C MODIFIED: Lorne Horton
C           - Initial version
C
C UPDATE:   1.2                         DATE: 17/05/07
C MODIFIED: Allan Whiteford
C           - Updated comments as part of subroutine documentation
C             procedure.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER N        , JLO       , INC      , JHI      , JM
C-----------------------------------------------------------------------
      REAL*8  XX(N), X
C-----------------------------------------------------------------------
      LOGICAL ASCND
C-----------------------------------------------------------------------
C
      ASCND=XX(N).GE.XX(1)
C-----------------------------------------------------------------------
C CHECK TO SEE IF INITIAL GUESS IS USEFUL. IF NOT, GO IMMEDIATELY TO
C BISECTION.
C-----------------------------------------------------------------------
      IF (JLO.LE.0 .OR.JLO.GT.N) THEN
         JLO = 0
         JHI = N+1
         GOTO 3
      ENDIF
C-----------------------------------------------------------------------
C HUNT FOR A RANGE WHICH BRACKETS X
C-----------------------------------------------------------------------
      INC = 1
      IF (X.GE.XX(JLO).EQV.ASCND) THEN
    1    JHI = JLO+INC
         IF (JHI.GT.N) THEN
            JHI = N+1
         ELSE IF (X.GE.XX(JHI).EQV.ASCND) THEN
            JLO = JHI
            INC = INC+INC
            GOTO 1
         ENDIF
      ELSE
         JHI=JLO
    2    JLO = JHI-INC
         IF (JLO.LT.1) THEN
            JLO = 0
         ELSE IF (X.LT.XX(JLO).EQV.ASCND) THEN
            JHI = JLO
            INC = INC+INC
            GOTO 2
         ENDIF
      ENDIF
C-----------------------------------------------------------------------
C DO A BISECTION SEARCH TO FIND THE INTERVAL
C-----------------------------------------------------------------------
    3 IF (JHI-JLO.EQ.1) THEN
         IF (X.EQ.XX(N)) JLO = N-1
         IF (X.EQ.XX(1)) JLO = 1
         RETURN
      ENDIF
C-----------------------------------------------------------------------
      JM = (JHI+JLO)/2
      IF (X.GE.XX(JM).EQV.ASCND) THEN
         JLO = JM
      ELSE
         JHI = JM
      ENDIF
      GOTO 3
C-----------------------------------------------------------------------
      END
