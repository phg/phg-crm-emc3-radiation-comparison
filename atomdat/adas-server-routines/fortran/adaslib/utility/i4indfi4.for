C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/i4indfi4.for,v 1.5 2017/09/23 17:59:41 mog Exp $ Date $Date: 2017/09/23 17:59:41 $
C
      FUNCTION I4INDFI4( ndim , array , ind )

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 INTEGER*4 FUNCTION: I4INDFI4 ***************
C
C  PURPOSE : Finds the index in the integer array corresponding to the
C            exact match to value. If the entry is outside the range of
C            array then -1 is returned.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  FUNC:   (I*4)   I4INDFI4  = INDEX
C
C  INPUT:  (I*4)   NDIM      = SIZE OF ARRAY
C  INPUT:  (R*8)   ARRAY     = obvious
C  INPUT:  (R*8)   IND       = SOUGHT VALUE
C
C
C  AUTHOR   : Martin O'Mullane,
C
C  VERSION  : 1.1
C  DATE     : 15-04-2005
C
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C VERSION  : 1.2
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C VERSION  : 1.3
C DATE     : 21-07-2008
C MODIFIED : Martin O'Mullane
C               - Do not assume a monotonic array when checking limits,
C                 determine min/max index values instead.
C
C VERSION  : 1.4
C DATE     : 02-09-2017
C MODIFIED : Martin O'Mullane
C               - Initialize imin to the 32-bit limit of integer*4 
C                 and imax to -1.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      integer    ndim , i4indfi4  , i  , i4unit , ind
      integer    imin , imax
C-----------------------------------------------------------------------
      integer    array(ndim)
C-----------------------------------------------------------------------

      if (ndim.lt.2) then
        write(i4unit(-1),100)
        stop
      endif

      imin = 2147483647
      imax = -1
      do i = 1, ndim
        if (array(i).le.imin) imin = array(i)
        if (array(i).ge.imax) imax = array(i)
      end do

      if (ind.lt.imin .or. ind.gt.imax ) then
        i4indfi4 = -1
        return
      endif

      do i = 1, ndim
        if (ind.EQ.array(i)) then
           i4indfi4 = i
           return
        endif
      end do

 100  format('Array input to I4INDFI4 must have more than 2 elements')

      end
