      subroutine xxrepl( cstrg1 , csub1, csub2, cstrg2 , nsubs )

      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxrepl *********************
c
c  purpose: to replace all occurrences of a substring in a string with
c           an alternative substring
c
c  calling program: general use
c
c  subroutine:
c
c  input : (c*(*)) cstrg1   = input character string for conversion
c  input : (c*(*)) csub1    = character sub-string to replace
c  input : (c*(*)) csub2    = character sub-string to substitute
c
c  output: (c*(*)) cstrg2   = output string after conversion
c  output: (i*4)   nsubs    = number of substitutions made
c
c          (i*4)   i        = general use
c          (i*4)   ilen1    = length of string in bytes
c          (i*4)   ilen2    = length of string in bytes
c          (i*4)   nsubs_max= maximum number of subsitutions allowed
c
c routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c note:
c
c
c author:  Hugh Summers,  University of Strathclyde
c          JA7.08
c          Tel. 0141-548-4196
c
c date  :  17/09/05
c
c version: 1.1                          date: 17/08/2005
c modified: Hugh Summers
c               - first edition.
c
c version: 1.2                          date: 03/01/2007
c modified: Hugh Summers
c               - remove redundant format statements.
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      integer     ilen1       , ilen2      , i
      integer     i4unit      , nsubs      , nsubs_max
c-----------------------------------------------------------------------
      character   cstrg1*(*)  , cstrg2*(*)
      character   csub1*(*)   , csub2*(*)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  checks
c-----------------------------------------------------------------------

      ilen1   = len(cstrg1)
      ilen2   = len(cstrg2)
      if(ilen1.ne.ilen2) then
          write(i4unit(-1),2002)'string mismatch:',
     &                          ' len(cstrg1)=',ilen1,
     &                      '  ne len(cstrg2)=',ilen2
          write(i4unit(-1),2003)
          stop
      endif

      nsubs_max=ilen1

      ilen1   = len(csub1)
      ilen2   = len(csub2)
      if(ilen1.ne.ilen2) then
          write(i4unit(-1),2002)'sub-string mismatch:',
     &                          ' len(csub1)=',ilen1,
     &                      '  ne len(csub2)=',ilen2
          write(i4unit(-1),2003)
          stop
      endif
c-----------------------------------------------------------------------
c  make substitutions
c-----------------------------------------------------------------------
      nsubs_max=nsubs_max/ilen1
      nsubs=0
      cstrg2=cstrg1

   10 i=index(cstrg2,csub1)
      if(i.gt.0) then
          nsubs=nsubs+1
          if(nsubs.gt.nsubs_max)then
              write(i4unit(-1),2002)'maximum substitutions exceeded'
              write(i4unit(-1),2003)
              stop
          endif
          cstrg2(i:i+ilen1-1)=csub2
          go to 10
      endif

      return

c
c-----------------------------------------------------------------------
c
 2002 format(1x,30('*'),'    xxrepl error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
