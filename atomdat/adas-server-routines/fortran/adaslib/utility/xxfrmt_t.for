       subroutine xxfrmt_t( ita_max , isa_max , ila_max , xja_max ,
     &                      iw      , fmt02
     &                    )
       implicit none
c-----------------------------------------------------------------------
c
c ****************** fortran 77 subroutine: xxfrmt_t *******************
c
c  purpose: To determine the format and file length required for the
c           transition strings of an ion
c
c
c subroutine:
c
c  input : (i*4)  ita_max   = largest value for transition index
c  input : (i*4)  isa_max   = largest value for transition multiplicity
c  input : (i*4)  ila_max   = largest value for transition orb. ang.mom
c  input : (r*8)  xja_max   = largest value for transition (stat.wt-1)/2
c
c  output: (i*4)  iw        = field length required for trans. strings
c  output: (c*63) fmt02     = appropriate format specification
c
c routines:
c          routine    source    brief description
c          ----------------------------------------------------------
c         i4unit     adas      fetch unit number for output of messages
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    26/07/06
c
c version: 1.1                          date: 26/07/2006
c modified: hugh p summers
c               - first edition.
c
c-----------------------------------------------------------------------
        integer    ita_max , isa_max , ila_max
        integer    iw      , i1      , i2      , i3    , i4
c-----------------------------------------------------------------------
        real*8     xja_max
c-----------------------------------------------------------------------
        character  fmt02*63
c-----------------------------------------------------------------------
        fmt02="(i*,'(',i*,')',i*,'(',f*.1,')-',i*,'(',i*,')',i*,'(',f*.1
     &,')')"

        i1 = dint(dlog10(dfloat(ita_max)))+1
        i1 = max0(3,i1)
        i2 = dint(dlog10(dfloat(max0(isa_max,1))))+1
        i3 = dint(dlog10(dfloat(max0(ila_max,1))))+1
        i4 = dint(dlog10(dmax1(xja_max,1.0d0)))+3
        i4 = max0(5,i4)

        iw=2*(i1+i2+i3+i4)+9

        write(fmt02(3:3),'(i1)')i1
        write(fmt02(10:10),'(i1)')i2
        write(fmt02(17:17),'(i1)')i3
        write(fmt02(24:24),'(i1)')i4
        write(fmt02(34:34),'(i1)')i1
        write(fmt02(41:41),'(i1)')i2
        write(fmt02(48:48),'(i1)')i3
        write(fmt02(55:55),'(i1)')i4

        return

       end
