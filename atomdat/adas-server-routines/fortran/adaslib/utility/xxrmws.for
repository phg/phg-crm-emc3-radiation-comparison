C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxrmws.for,v 1.1 2004/07/06 15:38:48 whitefor Exp $ Date $Date: 2004/07/06 15:38:48 $
C
      SUBROUTINE XXRMWS( STRING, CSTRING, LENGTH )

      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXRMWS *********************
C
C  PURPOSE: REMOVES ALL BLANKS IN INPUT STRING
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT    : (C*(*)) STRING = STRING.
C
C  OUTPUT   : (C*(*)) CSTRING = COMPRESSED STRING.
C
C  ROUTINES : NONE
C
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          DATE: 17/03/1999
C  MODIFIED : Martin O'Mullane
C             First version.
C
C-----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER    I , K , LENGTH
C----------------------------------------------------------------------
      CHARACTER  STRING*(*) , CSTRING*(*)
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------

      cstring=' '
      K = 1
      DO I = 1 , LEN( STRING )
         IF ( STRING(I:I) .NE. ' ' ) THEN
            CSTRING(K:K) = STRING(I:I)
            K = K + 1
         ENDIF
      END DO
      
      LENGTH = K - 1

C----------------------------------------------------------------------

      RETURN
      END
