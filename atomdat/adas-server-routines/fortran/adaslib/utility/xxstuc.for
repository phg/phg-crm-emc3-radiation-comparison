CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxstuc.for,v 1.1 2004/07/06 15:39:48 whitefor Exp $ Date $Date: 2004/07/06 15:39:48 $
CX
      SUBROUTINE XXSTUC( STRING )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXSTUC *********************
C
C  PURPOSE: ENSURES ALL LETTERS IN INPUT STRING ARE UPPER CASE.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  I/O   : (C*(*)) STRING = STRING.
C
C          (I*4)  IDIFF   = DIFFERENCE IN CODES BETWEEN LOWER AND UPPER
C                           CASE LETTER.
C          (I*4)  I       = LOOP INDEX.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    26/11/93
C
C-----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER    IDIFF   , I
C----------------------------------------------------------------------
      CHARACTER  STRING*(*)
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
C
      IDIFF = ICHAR( 'a' ) - ICHAR( 'A' )
      DO 1 I = 1 , LEN( STRING )
         IF ( ( STRING(I:I) .GE. 'a' ) .AND.
     &        ( STRING(I:I) .LE. 'z' )       ) THEN
            STRING(I:I) = CHAR( ICHAR( STRING(I:I) ) - IDIFF )
         ENDIF
    1 CONTINUE
C
C----------------------------------------------------------------------
C
      RETURN
      END
