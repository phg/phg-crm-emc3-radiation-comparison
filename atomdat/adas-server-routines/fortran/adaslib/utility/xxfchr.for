CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxfchr.for,v 1.3 2007/05/18 08:54:18 allan Exp $ Date $Date: 2007/05/18 08:54:18 $
CX
      SUBROUTINE XXFCHR( CSTRNG , SSTRNG, IFIRST , ILAST )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXFCHR *********************
C
C  PURPOSE: TO IDENTIFY THE FIRST AND LAST OCCURRENCE OF SSTRNG IN
C           CSTRNG, THE VALUES OF WHICH ARE IFIRST , ILAST.
C
C           - IF    NO    OCCURRENCE OF SSTRNG THEN IFIRST=ILAST=0
C           - IF ONLY ONE OCCURRENCE OF SSTRNG THEN IFIRST=ILAST>0
C           - IF OVER ONE OCCURRENCE OF SSTRNG THEN IFIRST>ILAST>0
C
C  NOTE   : ANY TRAILING BLANKS IN THE SEARCH STRING (SSTRNG) ARE
C           IGNORED.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C*(*)) CSTRNG   = INPUT STRING FOR INTERROGATION
C  INPUT : (C*(*)) SSTRNG   = INPUT SEARCH STRING
C
C  OUTPUT: (I*4)   IFIRST   = BYTE POSITION OF FIRST OCCURRENCE OF SSTRNG
C                             IN CSTRNG.
C  OUTPUT: (I*4)   ILAST    = BYTE POSITION OF LAST  OCCURRENCE OF SSTRNG
C                             IN CSTRNG.
C
C          (I*4)   ILENC    = LENGTH OF 'CSTRNG' STRING IN BYTES
C          (I*4)   ILENS    = POSITION OF LAST  NON-BLANK BYTE IN SSTRNG
C          (I*4)   I        = GENERAL USE - INCLUDING DUMP FOR UNWANTED
C                             OUTPUT FROM XXSLEN SUBROUTINE.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSLEN     ADAS      FIND FIRST/LAST NONBLANK BYTES IN STRING
C
C NOTE  :  ANY TRAILING BLANKS IN THE SEARCH STRING (SSTRNG) ARE
C          IGNORED.
C
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE  :  27/10/94
C
C UPDATE:  06/03/95 - REVISED BY PAUL BRIDEN (TESSELLA SUPPORT SERVICES)
C                     1) ANALYSE ALL OF CSTRNG (NOT JUST NON-BLANK
C                        PART).
C                     2) ONLY IGNORE TRAILING BLANKS FOR SSTRNG
C                        (KEEP LEADING BLANKS).
C                     3) MODIFY DO LOOP INDEX RANGE TO ENSURE THAT
C                        YOU DO NOT GO BEYOND THE END OF CSTRNG.
C                     4) VERIFY LENGTH OF SSTRNG IS NON-ZERO.
C
C UPDATE:  17/05/07 - Allan Whiteford
C                     Updated comments as part of subroutine
C                     documentation procedure.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IFIRST     , ILAST
      INTEGER     ILENC      , ILENS      , I
C-----------------------------------------------------------------------
      CHARACTER   CSTRNG*(*) , SSTRNG*(*)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IFIRST = 0
      ILAST  = 0
C
      ILENC  = LEN(CSTRNG)
      CALL XXSLEN(SSTRNG,I,ILENS)
C
         IF ( (ILENC.GE.ILENS) .AND. (ILENS.GT.0) ) THEN
C
           DO 1 I=1,1+ILENC-ILENS
C
              IF (CSTRNG(I:I-1+ILENS).EQ.SSTRNG(1:ILENS)) THEN
                 IF (IFIRST.EQ.0) IFIRST = I
                 ILAST = I
              ENDIF
C
    1      CONTINUE
C
         ENDIF
C
C-----------------------------------------------------------------------
C
       RETURN
       END
