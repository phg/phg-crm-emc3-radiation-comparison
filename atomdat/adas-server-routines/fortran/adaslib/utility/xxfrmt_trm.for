       subroutine xxfrmt_trm( isa_max , ila_max , xja_max ,
     &                        iw      , fmt03
     &                    )
       implicit none
c-----------------------------------------------------------------------
c
c ****************** fortran 77 subroutine: xxfrmt_trm *****************
c
c  purpose: To determine the format and string length required for the
c           term strings of an ion
c
c
c subroutine:
c
c  input : (i*4)  isa_max   = largest value for term multiplicity
c  input : (i*4)  ila_max   = largest value for term orb. ang.mom
c  input : (r*8)  xja_max   = largest value for term (stat.wt-1)/2
c
c  output: (i*4)  iw        = field length required for term strings
c  output: (c*63) fmt03     = appropriate format specification
c
c routines:
c          routine    source    brief description
c          ----------------------------------------------------------
c         i4unit     adas      fetch unit number for output of messages
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    26/07/06
c
c version: 1.1                          date: 26/07/2006
c modified: hugh p summers
c               - first edition.
c
c-----------------------------------------------------------------------
        integer    isa_max , ila_max
        integer    iw      , i1      , i2      , i3
c-----------------------------------------------------------------------
        real*8     xja_max
c-----------------------------------------------------------------------
        character  fmt03*31
c-----------------------------------------------------------------------
        fmt03="('(',i*,')',i*,'(',f*.1,')')   "

        i1 = dint(dlog10(dfloat(max0(isa_max,1))))+1
        i2 = dint(dlog10(dfloat(max0(ila_max,1))))+1
        i3 = dint(dlog10(dmax1(xja_max,1.0d0)))+3
        i3 = max0(5,i3)

        iw= i1+i2+i3+4

        write(fmt03(7:7),'(i1)')i1
        write(fmt03(14:14),'(i1)')i2
        write(fmt03(21:21),'(i1)')i3

        return

       end
