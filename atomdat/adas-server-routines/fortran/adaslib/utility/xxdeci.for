       subroutine xxdeci(x,iw,id,ifail)
       implicit none
c-----------------------------------------------------------------------
c
c ****************** fortran 77 subroutine: dgwpec ********************
c
c  purpose: To decide the number of decimals which can be printed
c           after the decimal point in a fixed format with a specified
c           field length.
c
c
c subroutine:
c
c  input : (r*8)  x         = decimal number for prining in fixed format
c  input : (i*4)  iw        = field length including sign and dec. pt.
c
c  output: (i*4)  id        = number of decimals to print after dec. pt.
c  output  (i*4)  ifail     = 0 => variable is printable as fixed point
c                                  in the specified field length
c                             1 => variable cannot be printed as fixed
c                                   point in the specified field length
c
c routines:
c          routine    source    brief description
c          ----------------------------------------------------------
c         i4unit     adas      fetch unit number for output of messages
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    26/04/06
c
c version: 1.1                          date: 26/04/2006
c modified: hugh p summers
c               - first edition.
c
c-----------------------------------------------------------------------
       integer  ib     , id     , iw     , ifail
c-----------------------------------------------------------------------
       real*8   x      , xm
c-----------------------------------------------------------------------

       if(iw.lt.3)then
           ifail = 1
           return
       else
           xm = dabs(x)
           ib = dint(dlog10(xm))
           id = iw-ib-3
           if (id.ge.0) then
               ifail = 0
           else
               ifail = 1
           endif
        endif

        return

       end
