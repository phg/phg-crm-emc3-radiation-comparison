      FUNCTION i4numlines(ndim , length)         

      implicit none
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 INTEGER*4 FUNCTION: i4numlines *************
C
C  PURPOSE:  Calculates how many lines in a text file are occupied by a
C            vector given a fixed number of entries per line.
C
C  CALLING PROGRAM: General use
C
C  FUNCTION:
C
C  FUNC:   (I*4)   i4numlines  = number of lines
C
C  INPUT:  (I*4)   ndim    = size of array
C  INPUT:  (I*4)   length  = number of entries per line.
C
C
C  AUTHOR   : Martin O'Mullane
C
C
C  VERSION  : 1.1                          
C  DATE     : 22-08-2016
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C-----------------------------------------------------------------------
      integer    i4numlines,  ndim  , length
C-----------------------------------------------------------------------
      
      if (length.LE.0) then
         write(0,*)'i4numlines error: number of entries cannot be 0'
         stop
      endif
      
      i4numlines = ( (ndim/length) + min(1, mod(ndim,length)) )
      
      return

      end 
