      subroutine xxslna( n, stra , length )
      implicit none
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXSLNA *********************
C
C  PURPOSE: To return the maximun non-blank lenght of an array
C           of strings
C
C  CALLING PROGRAM: General Use
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  n        = input array 'stra()' size
C  INPUT : (C*(*)) stra()   = input string array for interrogation
C
C  OUTPUT: (I*4)   LENGTH   = Maximum length found
C
C ROUTINES: NONE
C
C NOTE:
C
C
C AUTHOR:  Martin O'Mullane
C DATE  :  03-08-2009
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      integer     n          , length
      integer     ifirst     , ilast    ,  i
C-----------------------------------------------------------------------
      character   stra(n)*(*)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      length   = -1

      do i = 1, n

          ifirst = 0
          ilast  = 0
          call xxslen(stra(i), ifirst, ilast)

          length = max0(length, ilast)

       end do

C-----------------------------------------------------------------------
       return
       end
