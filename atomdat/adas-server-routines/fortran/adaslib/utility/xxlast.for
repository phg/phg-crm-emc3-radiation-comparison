      subroutine xxlast(cstrg, csrch, indx)
      
      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 function: xxlast ***********************
c
c  purpose : returns the index of the last occurence of a character in
c            a string
c
c  calling program: general use
c
c  subroutine:
c
c  input : (c*(*)) cstrg    = input string for interrogation
c  input : (c*1)   csrch    = search character
c
c  output: (i*4)   indx     = byte position of last  occurence
c                             of csrch in input string.
c
c          (i*4)   i        = general use
c          (i*4)   ilen     = length of 'cstrng' string in bytes
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c author:  Hugh Summers
c          JET K1/1/61
c          Tel. 01235-46-4459
c date:    19-12-2007
c
c
c version  : 1.1
c date     : 19-12-2007
c modified : Hugh Summers
c              - first version.
c-----------------------------------------------------------------------
      integer    i         , indx
c-----------------------------------------------------------------------
      character  cstrg*(*) , csrch*1
c-----------------------------------------------------------------------

      do 10 i = len(cstrg),1,-1
        if (cstrg(i:i) .eq. csrch) go to 20
   10 continue

   20 continue
      indx = i

      return
      end
