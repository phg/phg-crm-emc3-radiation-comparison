CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxi4ss.for,v 1.1 2004/07/06 15:36:32 whitefor Exp $ Date $Date: 2004/07/06 15:36:32 $
CX  
      subroutine xxi4ss(n, lup, ia, itag)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: XXI4SS ********************
C
C  PURPOSE: Sorts an integer array xa and its index array. This is a
C           bubble sort designed for small arrays.         
C                                                                    
C
C  CALLING PROGRAM:  General use
C
C                                                                   
C  SUBROUTINE:
C
C  INPUT       :  (I*4)  N     = Size of input arrays
C  INPUT       :  (L*4)  LUP   = .TRUE. sort in ascending order
C                                .FALSE. descending order
C  INPUT/OUPUT :  (I*4)  IA    = Array to be sorted
C  INPUT/OUPUT :  (I*4)  ITAG  = Original index of sorted XA
C 
C  ROUTINES    : NONE
C
C  NOTES       : Shell sort from Numerical Receipies.
C
C  AUTHOR      : Martin O'Mullane
C
C  DATE        : 7-02-2000
C
C  VERSION     : 1.1
C  
C
C----------------------------------------------------------------------- 
      real*8     aln2i                      , tiny
C-----------------------------------------------------------------------
      parameter (aln2i = 1.0D0/0.69314718D0 , tiny = 1.0D-5)
C-----------------------------------------------------------------------
      integer    n      ,  m      , nn   , j       , k     ,  l    , 
     &           temp   , itemp   , i    , lognb2           
C-----------------------------------------------------------------------
      logical    lup                                      
C-----------------------------------------------------------------------
      integer    ia(*)  ,  itag(*)                                      
C-----------------------------------------------------------------------
 
      if (n.eq.1) return
 
      lognb2=int(log(dble(n))*aln2i+tiny)
 
      m = n
      
      if (lup) then
 
         do nn = 1, lognb2
           m=m/2
           k = n - m
           do j = 1, k
             i = j
   3         continue
             l = i + m
             if (ia(l).lt.ia(i)) then
               temp    = ia(i)
               ia(i)   = ia(l)
               ia(l)   = temp
               itemp   = itag(i)
               itag(i) = itag(l)
               itag(l) = itemp
               i       = i - m
               if (i.ge.1) goto 3
             endif
           end do
         end do
         
      else
      
         do nn = 1, lognb2
           m=m/2
           k = n - m
           do j = 1, k
             i = j
   5         continue
             l = i + m
             if (ia(l).gt.ia(i)) then
               temp    = ia(i)
               ia(i)   = ia(l)
               ia(l)   = temp
               itemp   = itag(i)
               itag(i) = itag(l)
               itag(l) = itemp
               i       = i - m
               if (i.ge.1) goto 5
             endif
           end do
         end do
         
      
      endif
 
      return
 
      end
