C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/i4indf.for,v 1.2 2007/04/11 13:02:01 allan Exp $ Date $Date: 2007/04/11 13:02:01 $
C
      FUNCTION I4INDF( ndim , array ,val )         

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  *************** FORTRAN77 INTEGER*4 FUNCTION: I4INDF ****************
C
C  PURPOSE:  Finds the index in array corresponding to the closest match
C            to value.
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  FUNC:   (I*4)   I4INDF  = INDEX  (if the entry is outside the range
C                                    of array then -1 is returned.)
C
C  INPUT:  (I*4)   NDIM    = SIZE OF ARRAY
C  INPUT:  (R*8)   ARRAY   = obvious
C  INPUT:  (R*8)   VAL     = SOUGHT VALUE
C
C
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    ndim , I4INDF  , I  , i4unit
C-----------------------------------------------------------------------
      REAL*8     VAL , bin , fmin
C-----------------------------------------------------------------------
      REAL*8     ARRAY(NDIM) 
C-----------------------------------------------------------------------
  
  
      bin = array(2)-array(1)
      
      if (ndim.lt.2) then
        write(i4unit(-1),100)
        stop
      endif
      
      if (val.lt.array(1) .or. val.gt.array(ndim) ) then
        i4indf = -1
        return
      endif
      
      
      fmin = val - bin 
      
      i=1
      do while ( array(i).lt.fmin .and. i.le.ndim)
        i=i+1
      end do
      
      i4indf = i
      
      return
      
 100  format('Array input to I4INDF must have more 2 elements')
      
      end 
