CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxgama.for,v 1.3 2007/04/11 13:02:02 allan Exp $ Date $Date: 2007/04/11 13:02:02 $
CX
      SUBROUTINE XXGAMA( MXINDX , JGAM, GAM )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXGAMA *********************
C
C  PURPOSE: SETUP LOOKUP TABLES FOR ROUTINES I4JGAM AND R8GAM
C
C           TABLES 'JGAM' AND 'GAM' CAN BE REFERENCED WITH FUNCTIONS
C           'I4JGAM' AND 'R8GAM', RESPECTIVELY.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT : (I*4)  MXINDX  =
C
C  OUTPUT: (I*4)  JGAM()  =
C                           DIMENSION: REFERENCED BY I.
C  OUTPUT: (I*4)  GAM()   =
C                           DIMENSION: REFERENCED BY I.
C
C  PARAM : (R*8)  P1      = 64.0
C
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  I1      = I-1
C          (I*4)  J1      = USED IN COMPUTATION ( = JGAM(N) ).
C
C          (R*8)  X1      = USED IN COMPUTATION ( = GAM(N) ).
C
C ROUTINES: NONE
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     30/09/93
C
C VERSION  : 1.2                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION  : 1.3                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 64.00D+00 )
C-----------------------------------------------------------------------
      INTEGER    MXINDX
      INTEGER    I       , I1      , J1
C-----------------------------------------------------------------------
      REAL*8     X1
C-----------------------------------------------------------------------
      INTEGER    JGAM(MXINDX)
C-----------------------------------------------------------------------
      REAL*8     GAM(MXINDX)
C-----------------------------------------------------------------------
C
      JGAM(1) = 0
      GAM(1)  = 1.0D+00
C
      DO 1 I = 2 , MXINDX
C
         I1 = I - 1
         X1 = DFLOAT( I1 )
         X1 = X1 * GAM(I1)
         J1 = JGAM(I1)
C
    2    CONTINUE
            IF (X1 .GE. P1) THEN
               J1 = J1 + 1
               X1 = X1 / P1
               GO TO 2
            ENDIF
C
         JGAM(I) = J1
         GAM(I)  = X1
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
