CX  SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxopcs.for,v 1.1 2004/07/06 15:38:02 whitefor Exp $ Date $Date: 2004/07/06 15:38:02 $
CX
      subroutine xxopcs(unit,file,type)

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXOPCS *********************
C
C  PURPOSE: Open a file on a given unit but when a read is done from
C           that unit, only give the contents back in a given case.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT    : (I*4)   UNIT   = Unit to open the file on
C  INPUT    : (C*(*)) FILE   = Input Filename
C  INPUT    : (C*2)   TYPE = Type of case to convert to:
C                       'UC' -> Convert to Upper Case
C                       'LC' -> Convert to Lower Case
C                       Anything else -> No conversion
C
C  OUTPUT   : NONE
C
C  ROUTINES :
C            ROUTINE    SOURCE    BRIEF DESCRIPTION
C            ------------------------------------------------------------
C            XXCASE     ADAS      CONVERT STRING TO UPPER OF LOWER CASE
C
C  AUTHOR   : Allan Whiteford,
C             University of Strathclyde
C
C  VERSION  : 1.1                          DATE: 05/09/2001
C  MODIFIED : Allan Whiteford
C             First version.
C
C-----------------------------------------------------------------------
       integer i
       integer      unit
C-----------------------------------------------------------------------
       character*(*) file
       character*1024 input
       character*1024 output 
       character*2 type
C-----------------------------------------------------------------------
       open(99,file=file)
       open(unit,status='scratch')       

20     read(99,'(A)',end=10) input
       call xxcase(input,output,type)
       do 30 i=len(output),1,-1
30     if (output(i:i) .ne. ' ') goto 40
40     write(unit,'(A)') output(1:i)
       goto 20
       
10     close(99)

       rewind(unit)
       
       end
