CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/i4indfvs.for,v 1.2 2007/04/11 13:02:01 allan Exp $ Date $Date: 2007/04/11 13:02:01 $
CX
      FUNCTION I4INDFVS( ndim , array ,val )         

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  *************** FORTRAN77 INTEGER*4 FUNCTION: I4INDF ****************
C
C  PURPOSE:  Finds the index in array corresponding to the closest match
C            to value.
C
C  NOTES:    This is a version of i4indf but array need not be 
C            monotonically increasing. (vs - variable spacing).
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C
C  FUNC:   (I*4)   I4INDFVS  = INDEX
C
C  INPUT:  (I*4)   NDIM      = SIZE OF ARRAY
C  INPUT:  (R*8)   ARRAY     = obvious
C  INPUT:  (R*8)   VAL       = SOUGHT VALUE
C
C
C  AUTHOR   : Martin O'Mullane,
C
C  VERSION  : 1.1                          
C  DATE     : 23-07-2003
C  MODIFIED : Martin O'Mullane  
C              - First version.
C
C VERSION  : 1.2
C DATE     : 10-104-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      integer    ndim , i4indfvs , i , ival, i4unit
c-----------------------------------------------------------------------
      real*8     val  , fmin     , ftmp
c-----------------------------------------------------------------------
      real*8     array(ndim) 
c-----------------------------------------------------------------------
        
      if (ndim.lt.2) then
        write(i4unit(-1),100)
        stop
      endif
      
      
      fmin = abs(array(1) - val)
      
      ival = 1
      do i = 2, ndim
         
         ftmp = abs(array(i) - val)
         if (ftmp.LT.fmin) then
            fmin = ftmp
            ival = i
         endif
         
      end do
      
      i4indfvs = ival
      
      return
      
 100  format('Array input to I4INDFVS must have more 2 elements')
      
      end
