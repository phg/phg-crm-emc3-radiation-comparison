CX UNIX - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxordr.for,v 1.1 2004/07/06 15:38:09 whitefor Exp $ Date $Date: 2004/07/06 15:38:09 $
CX
      subroutine xxordr(n, lup, xa, ia, iinva)                                         

      implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxordr *******************
c
c  purpose: sorts a real*8 array xa. This is a bubble sort designed for
c           small arrays.         
c                                                                    
c
c  calling program:  general use
c
c                                                                   
c  subroutine:
c
c  input       :  (i*4)  n       = size of input arrays
c  input       :  (l*4)  lup     = .true. sort in ascending order
c                                  .false. descending order
c  input/output:  (r*8)  xa      = array to be sorted
c  output      :  (i*4)  ia      = original index of sorted xa members
c  output      :  (i*4)  iinva   = inverse of ia
c 
c  routines: none
c
c  notes: based on nsort.  subroutine sets ia(i)=i before sorting 
c
c  author:  Susan Turnbull
c
c  date:    22/08/01
c
c  version: 1.1
c  
c-----------------------------------------------------------------------
      integer iswap,  n,  n1,  i,  i1,  j
      integer i4unit                                         
c-----------------------------------------------------------------------
      integer ia(*) , iinva(*)                                      
c-----------------------------------------------------------------------
      logical lup                                      
c-----------------------------------------------------------------------
      real*8  swap                                                  
c-----------------------------------------------------------------------
      real*8  xa(*)                                             
c-----------------------------------------------------------------------
      if(n.eq.0) then
          write(i4unit(-1),1000) 'vector of zero length'
          write(i4unit(-1),1001) 
	  stop
	  
      elseif (n.eq.1) then
         ia(1)=1
	 iinva(1)=1
	 
      else  
      
         do i=1,n
           ia(i)=i
         end do
      
         n1=n-1  
         do i=1,n1
           i1=i+1
           if (lup) then
                                                             
              do j=i1,n 
                if (xa(i).gt.xa(j)) then                   
                   swap=xa(i)                                    
                   xa(i)=xa(j)                                    
                   xa(j)=swap                                      
                   iswap=ia(i)                                      
                   ia(i)=ia(j)                                       
                   ia(j)=iswap 
                endif
              end do
        
           else
        
              do j=i1,n 
                if (xa(i).le.xa(j)) then                   
                   swap=xa(i)                                    
                   xa(i)=xa(j)                                    
                   xa(j)=swap                                      
                   iswap=ia(i)                                      
                   ia(i)=ia(j)                                       
                   ia(j)=iswap 
                endif
              end do
           
           endif
                
         end do              
          
         do i=1,n
            do j=1,n
   	       if (ia(j).eq.i) then 
	          iinva(i)=j
	       endif
	    end do 
         end do
      
      endif
	                                           
      return 
c------------------------------------------------------------------
 1000 format(1x,32('*'),' xxordr error ',32('*')//
     &       1x,'fault in input: ',a)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
c------------------------------------------------------------------                                                          
                                                                
      end                                                         
