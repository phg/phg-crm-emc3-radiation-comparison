CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxisrt.for,v 1.1 2004/07/06 15:36:56 whitefor Exp $ Date $Date: 2004/07/06 15:36:56 $
CX
      SUBROUTINE XXISRT( N , ARR , INDX , WRK )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXISRT *********************
C
C  PURPOSE: SORTS AN ARRAY 'ARR' OF LENGTH 'N' ACCORDING TO THE INDEX
C           'INDX()'  SUCH  THAT  'ARR(INDX(J))'  GOES  TO   'ARR(J)'
C           FOR J=1,2,...,N.   THE INPUT QUANTITIES 'N' AND 'INDX' ARE
C           NOT CHANGED.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  N       = INPUT ARRAY 'ARR()' LENGTH
C  I/O   :    (R*8)  ARR()   = ARRAY TO BE SORTED
C  INPUT :    (I*4)  INDX()  = RE-ORDERING INDEX
C  OUTPUT:    (R*8)  WRK()   = WORKSPACE
C
C             (I*4)  I       = GENERAL USE
C
C ROUTINES:  NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    11/07/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER N        , I
      INTEGER INDX(N)
C-----------------------------------------------------------------------
      REAL*8  ARR(N)   , WRK(N)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
         DO 1 I=1,N
            WRK(I)=ARR(I)
    1    CONTINUE
C-----------------------------------------------------------------------
         DO 2 I=1,N
            ARR(I)=WRK(INDX(I))
    2    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
