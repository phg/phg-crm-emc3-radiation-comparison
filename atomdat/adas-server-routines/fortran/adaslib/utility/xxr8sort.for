CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxr8sort.for,v 1.1 2004/07/06 15:38:25 whitefor Exp $ Date $Date: 2004/07/06 15:38:25 $
CX
      SUBROUTINE XXR8SORT(N, LUP, XA, IA)                                         

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXR8SORT *******************
C
C  PURPOSE: Sorts a REAL*8 array xa and its index array. This is a
C           bubble sort designed for small arrays.         
C                                                                    
C
C  CALLING PROGRAM:  General use
C
C                                                                   
C  SUBROUTINE:
C
C  INPUT       :  (I*4)  N   = Size of input arrays
C  INPUT       :  (L*4)  LUP = .TRUE. sort in ascending order
C                              .FALSE. descending order
C  INPUT/OUPUT :  (R*8)  XA  = Array to be sorted
C  INPUT/OUPUT :  (I*4)  IA  = Original index of sorted XA
C 
C  ROUTINES: NONE
C
C  NOTES: Based on nsort
C
C  AUTHOR:  Martin O'Mullane
C           K1/1/52
C
C  DATE:    19/07/99
C
C  VERSION: 1.1
C  
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
      INTEGER ISWAP  , N , N1, I  , I1, J                                         
C-----------------------------------------------------------------------
      INTEGER IA(*)                                       
C-----------------------------------------------------------------------
      LOGICAL LUP                                      
C-----------------------------------------------------------------------
      REAL*8  SWAP                                                  
C-----------------------------------------------------------------------
      REAL*8  XA(*)                                             
C-----------------------------------------------------------------------

      N1=N-1  
                                                             
      DO I=1,N1
                                                        
        I1=I+1
        if (lup) then
                                                             
           DO J=I1,N 
             IF (XA(I).GT.XA(J)) THEN                   
                SWAP=XA(I)                                    
                XA(I)=XA(J)                                    
                XA(J)=SWAP                                      
                ISWAP=IA(I)                                      
                IA(I)=IA(J)                                       
                IA(J)=ISWAP 
             ENDIF
           END DO
        
        else
        
           DO J=I1,N 
             IF (XA(I).LE.XA(J)) THEN                   
                SWAP=XA(I)                                    
                XA(I)=XA(J)                                    
                XA(J)=SWAP                                      
                ISWAP=IA(I)                                      
                IA(I)=IA(J)                                       
                IA(J)=ISWAP 
             ENDIF
           END DO
           
        endif
                
      END DO              
                                                 
      RETURN                                                    
                                                                
      END                                                         
