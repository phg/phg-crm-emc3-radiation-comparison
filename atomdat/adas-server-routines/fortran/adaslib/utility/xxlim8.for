CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxlim8.for,v 1.3 2007/04/11 13:02:02 allan Exp $ Date $Date: 2007/04/11 13:02:02 $
CX
       SUBROUTINE XXLIM8 ( GMIN , GMAX , X , N , CUTMIN )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXLIM8 *********************
C
C  PURPOSE:
C             FINDS MAXIMA AND MINIMA OF A LIST  OF X VALUES AND RETURNS
C             THE SCALE RANGE FOR PLOTTING ON A LOG TO THE BASE 10 GRID.
C             I.E. LOG10 RANGE TO NEAREST APPROPRIATE INTEGERS.
C
C             (RANGE IN LINEAR UNITS = GMIN -> GMAX)
C             (DOUBLE PRECISION VERSION OF 'XXLIM4')
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  OUTPUT: (R*4)  GMIN    = LOWER LIMIT FOR GRAPH X-AXIS (LINEAR)
C  OUTPUT: (R*4)  GMAX    = UPPER LIMIT FOR GRAPH X-AXIS (LINEAR)
C  INPUT : (R*8)  X()     = INPUT X-VALUES
C  INPUT : (I*4)  N       = NUMBER OF INPUT X-VALUES
C  INPUT : (R*4)  CUTMIN  = MINIMUM ALLOWED VALUE FOR 'GMIN' (LINEAR).
C                           ('CUTMIN=0' EFFECTIVELY REMOVES ITS EFFECT)
C
C          (I*4)  NCMAX   =  PARAMETER = MAXIMUM NUMBER OF LOG10 CYCLES
C                                        ALLOWED IN GHOST80
C
C          (I*4)  IMIN    = MINIMUM LOG10 CYCLE
C          (I*4)  IMAX    = MAXIMUM LOG10 CYCLE
C          (I*4)  I       = GENERAL USE
C
C AUTHOR:    PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C            K1/0/81
C            JET EXT. 4569
C
C DATE:      02/08/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE (NO CHANGES)
C
C VERSION  : 1.2                          
C DATE     : 20-12-2001
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION  : 1.3                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C-----------------------------------------------------------------------
C----------------------------------------------------------------------
       INTEGER NCMAX
C----------------------------------------------------------------------
       PARAMETER( NCMAX = 20 )
C----------------------------------------------------------------------
       INTEGER N      , I
       INTEGER IMIN   , IMAX
C----------------------------------------------------------------------
       REAL*4  GMIN   , GMAX   , CUTMIN
C----------------------------------------------------------------------
       REAL*8  X(N)
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      IF (N.LE.0) RETURN
C----------------------------------------------------------------------
      GMIN=REAL(X(1))
      GMAX=REAL(X(1))
         DO 1 I=2,N
           IF (REAL(X(I)).LE.GMIN) GMIN=REAL(X(I))
           IF (GMAX.LE.REAL(X(I))) GMAX=REAL(X(I))
    1    CONTINUE
C
C----------------------------------------------------------------------
C IF ('GMAX' < 'GMIN') RETURN
C----------------------------------------------------------------------
C
      IF ( GMAX .LT. GMIN ) RETURN
C
C----------------------------------------------------------------------
C IF 'GMAX' & 'GMIN' ARE BOTH BELOW THE CUTOFF RETURN WITH 'GMIN=GMAX'
C----------------------------------------------------------------------
C
      IF (GMIN.LT.CUTMIN) GMIN=CUTMIN
      IF (GMAX.LT.CUTMIN) GMAX=CUTMIN
      IF ( GMAX .EQ. CUTMIN ) RETURN
C----------------------------------------------------------------------
      IMIN=INT(LOG10(GMIN)+0.0001)
      IF (GMIN.LE.1.0) IMIN=IMIN-1
C----------------------------------------------------------------------
      IMAX=INT(LOG10(GMAX)-0.0001)
      IF (1.0.LE.GMAX) IMAX=IMAX+1
C
C----------------------------------------------------------------------
C ('IMAX' - 'IMIN') MUST NOT EXCEED 'NCMAX'
C----------------------------------------------------------------------
C
      IF ( (IMAX-IMIN) .GT. NCMAX ) IMIN=IMAX-NCMAX
C----------------------------------------------------------------------
      GMIN=10.0**REAL(IMIN)
      GMAX=10.0**REAL(IMAX)
C----------------------------------------------------------------------
      RETURN
      END
