CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/nsort.for,v 1.1 2004/07/06 14:24:03 whitefor Exp $ Date $Date: 2004/07/06 14:24:03 $
CX
      SUBROUTINE NSORT(XA,IA,N)                                         
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: NSORT **********************
C
C PURPOSE:
C SUBROUTINE TO SORT AN ARRAY SO THAT XA IS INCREASING ORDER          
C                                                                    
C                                                                   
C INPUT                                                            
C     XA(I)=X-VALUES                                              
C     IA(I)=I-VALUES                                             
C     N=NUMBER OF VALUES                                        
C OUTPUT                                                       
C     XA(I)=SORTED X-VALUES                                   
C     IA(I)=SORTED I-VALUES                                  
C                                                           
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 01-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      REAL XA(*),SWAP                                                  
      INTEGER IA(*),ISWAP                                             
      N1=N-1                                                         
      DO 10 I=1,N1                                                  
        I1=I+1                                                     
        DO 5 J=I1,N                                   
          IF(XA(I).LE.XA(J)) GO TO 5                   
          SWAP=XA(I)                                    
          XA(I)=XA(J)                                    
          XA(J)=SWAP                                      
          ISWAP=IA(I)                                      
          IA(I)=IA(J)                                       
          IA(J)=ISWAP                                        
 5      CONTINUE                                              
 10   CONTINUE                                                 
      RETURN                                                    
C                                                                
      END                                                         
