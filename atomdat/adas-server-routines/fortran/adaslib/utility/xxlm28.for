CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxlm28.for,v 1.2 2007/04/11 13:02:02 allan Exp $ Date $Date: 2007/04/11 13:02:02 $
CX
       SUBROUTINE XXLM28 ( GMIN   , GMAX   ,
     &                     Y      ,
     &                     NDIM1  , NDIM2  ,
     &                     LMAX1  , LMAX2  ,
     &                     CUTMIN
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXLM28 *********************
C
C  PURPOSE:
C           FINDS MAXIMA AND MINIMA OF A 2-DIMENSIONAL ARRAY OF Y-VALUES
C           AND  RETURNS THE SCALE RANGE FOR PLOTTING  ON A LOG  TO  THE
C           BASE 10 GRID.
C
C           I.E. LOG10 RANGE COVERING BOTH DIMENSIONS TO THE NEAREST
C                APPROPRIATE INTEGERS.
C           (RANGE IN LINEAR UNITS = GMIN -> GMAX)
C           (REAL*8 VERSION OF 'XXLM24')
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  OUTPUT: (R*4)  GMIN    = LOWER LIMIT FOR GRAPH Y-AXIS (LINEAR)
C  OUTPUT: (R*4)  GMAX    = UPPER LIMIT FOR GRAPH Y-AXIS (LINEAR)
C
C  INPUT : (R*8)  Y(,)    = TWO-DIMENSIONAL ARRAY OF INPUT Y-VALUES.
C
C  INPUT : (I*4)  NDIM1   = FIRST 'Y(,)' ARRAY DIMENSION BOUND
C  INPUT : (I*4)  NDIM2   = SECOND 'Y(,)' ARRAY DIMENSION BOUND
C
C  INPUT : (I*4)  LMAX1   = RANGE OF FIRST 'Y(,)' ARRAY DIMENSION
C                           TO BE ASSESSED (1 -> LMAX1) (LMAX1<=NDIM1)
C  INPUT : (I*4)  LMAX2   = RANGE OF SECOND 'Y(,)' ARRAY DIMENSION
C                           TO BE ASSESSED (1 -> LMAX2) (LMAX2<=NDIM2)
C
C  INPUT : (R*4)  CUTMIN  = MINMUM VALUE FOR 'GMIN' (LINEAR).
C                           ('CUTMIN=0' EFFECTIVELY REMOVES ITS EFFECT)
C
C          (I*4)  NCMAX   = PARAMETER = MAXIMUM ALLOWED NUMBER OF LOG10
C                                       CYCLES ALLOWED IN GHOST80
C
C          (I*4)  IMIN    = MINIMUM LOG10 CYCLE
C          (I*4)  IMAX    = MAXIMUM LOG10 CYCLE
C          (I*4)  I       = GENERAL USE
C          (I*4)  J       = GENERAL USE
C
C
C NOTES:
C        A MINIMUM AVALUE FOR 'GMIN' IS SPECIFIED IN THE ARGUMENTS.
C        (SEE 'CUTMIN').
C
C ROUTINES: NONE
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     02/08/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE(NO CHANGES MADE)
C
C VERSION  : 1.2                          
C DATE     : 10-04-2007
C MODIFIED : Allan Whiteford
C               - Modified documentation as part of automated
C		  subroutine documentation preparation.
C-----------------------------------------------------------------------
       INTEGER NCMAX
C----------------------------------------------------------------------
       PARAMETER( NCMAX = 20 )
C----------------------------------------------------------------------
       INTEGER NDIM1    , NDIM2
       INTEGER LMAX1    , LMAX2
       INTEGER IMIN     , IMAX
       INTEGER I        , J
C----------------------------------------------------------------------
       REAL*4  GMIN     , GMAX     , CUTMIN
C----------------------------------------------------------------------
       REAL*8  Y(NDIM1,NDIM2)
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      IF ((LMAX1.LE.0).OR.(LMAX2.LE.0)) RETURN
C----------------------------------------------------------------------
      GMIN=REAL(Y(1,1))
      GMAX=REAL(Y(1,1))
         DO 1 J=1,LMAX2
            DO 2 I=1,LMAX1
               IF (REAL(Y(I,J)).LE.GMIN) GMIN=REAL(Y(I,J))
               IF (GMAX.LE.REAL(Y(I,J))) GMAX=REAL(Y(I,J))
    2       CONTINUE
    1    CONTINUE
C
C----------------------------------------------------------------------
C IF ('GMAX' < 'GMIN') RETURN
C----------------------------------------------------------------------
C
      IF ( GMAX .LT. GMIN ) RETURN
C
C----------------------------------------------------------------------
C IF 'GMAX' & 'GMIN' ARE BOTH BELOW THE CUTOFF RETURN WITH 'GMIN=GMAX'
C----------------------------------------------------------------------
C
      IF (GMIN.LT.CUTMIN) GMIN=CUTMIN
      IF (GMAX.LT.CUTMIN) GMAX=CUTMIN
      IF ( GMAX .EQ. CUTMIN ) RETURN
C----------------------------------------------------------------------
      IMIN=INT(LOG10(GMIN)+0.0001)
      IF (GMIN.LE.1.0) IMIN=IMIN-1
C----------------------------------------------------------------------
      IMAX=INT(LOG10(GMAX)-0.0001)
      IF (1.0.LE.GMAX) IMAX=IMAX+1
C
C----------------------------------------------------------------------
C ('IMAX' - 'IMIN') MUST NOT EXCEED 'NCMAX'
C----------------------------------------------------------------------
C
      IF ( (IMAX-IMIN) .GT. NCMAX ) IMIN=IMAX-NCMAX
C----------------------------------------------------------------------
      GMIN=10.0**REAL(IMIN)
      GMAX=10.0**REAL(IMAX)
C----------------------------------------------------------------------
      RETURN
      END
