CX  SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxfcse.for,v 1.1 2004/07/06 15:36:00 whitefor Exp $ Date $Date: 2004/07/06 15:36:00 $
CX
      subroutine xxfcse(filein,fileout,type)

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXFCSE *********************
C
C  PURPOSE: Read in a file, convert it all to upper or lower case and
C           then write it to another file.
C
C  CALLING PROGRAM: GENERAL USE.
C
C  INPUT    : (C*(*)) FILEIN = Input Filename
C  INPUT    : (C*(*)) FILEOUT = Output Filename
C  INPUT    : (C*2)   TYPE = Type of case to convert to:
C                       'UC' -> Convert to Upper Case
C                       'LC' -> Convert to Lower Case
C                       Anything else -> No conversion
C
C  OUTPUT   : NONE
C
C  ROUTINES :
C            ROUTINE    SOURCE    BRIEF DESCRIPTION
C            ------------------------------------------------------------
C            XXCASE     ADAS      CONVERT STRING TO UPPER OF LOWER CASE
C
C  AUTHOR   : Allan Whiteford,
C             University of Strathclyde
C
C  VERSION  : 1.1                          DATE: 05/09/2001
C  MODIFIED : Allan Whiteford
C             First version.
C
C-----------------------------------------------------------------------
       integer i
C----------------------------------------------------------------------
       character*(*)  filein
       character*(*)  fileout
       character*2   type
       character*1024 input
       character*1024 output 
C----------------------------------------------------------------------
       
       open(98,file=filein,status='OLD')
       open(99,file=fileout,status='UNKNOWN')
       
20     read(98,'(A)',end=10) input
       call xxcase(input,output,type)
       do 30 i=len(output),1,-1
30     if (output(i:i) .ne. ' ') goto 40
40     write(99,'(A)') output(1:i)
       goto 20
       
10     close(98)
       close(99)
       
       end        
