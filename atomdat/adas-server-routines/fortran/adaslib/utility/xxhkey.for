      subroutine xxhkey( ctext  , ckey , cbreak , cans )

      implicit none
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXHKEY *********************
C
C  PURPOSE: To extract from a line of text 'ctext' a response to a key
C           in the form of '<ckey> = <cans>'.
C
C  CALLING PROGRAM: General use
C
C  SUBROUTINE:
C
C  INPUT : (C*(*)) ctext   = input text line containing key & responses
C          (C*(*)) ckey    = key text
C          (C*(*)) cbreak  = key/response pair seperator symbol but
C                            enforce that it is one character long.
C
C  OUTPUT: (C*(*)) cans    = Response for given key: blank if not found
C
C ROUTINES:
c          routine    source    brief description
c          ----------------------------------------------------------
c          xxslen     adas      find string less front and tail blanks
C
C NOTES:    This routine extracts from 'ctext' a response to a given key
C           in the form of '<ckey> = <cans>'. e.g. 'file = dsn001'
C           would require as input ckey='file' and would give as output
C           cans='dsn001'. Key/response pairs are typically separated by
C           the character given by 'cbreak' e.g. a slash, but spaces as
C           the separator are also allowed.
C
C           Each key must be followed by an equals sign. The number of
C           spaces between the key and the equal sign and between the
C           response and the equal sign is not important.
C
C           If a key does not exist in 'ctext' then 'cans' is returned
C           blank.
C
C           The key is taken as 'ckey' removing any trailing blanks.
C           Leading blanks are left in place and will used when the
C           the search for the key is made:
C
C           i.e.  'data   ' and 'data' are the same key but
C                 ' data '  and 'data ' are different keys although
C                 both will give the same results if a space exists
C                 before 'data' in the input text line.
C
C           An example of an input text line is:
C
C           8524.0 A    5 7 /FILMEM = FBBH91BE/   CODE=   V2B DLN1   /
C
C           This would give the following:
C
C           CKEY='FILMEM'  =>  CANS='FBBH91BE'
C           CKEY=' FILMEM' =>  CANS=' '
C           CKEY='CODE'    =>  CANS='V2B DLN1'
C           CKEY=' CODE'   =>  CANS='V2B DLN1'
C           CKEY='OTHER'   =>  CANS=' '
C
C           If the character string is shorter than the response then
C           the response is truncated accordingly.
C
C           Spaces can exist in the key. i.e. CKEY='PLOT A'. But care
C           should be taken when using prefixes on a common key base,
C           i.e. 'A PLOT', 'B PLOT'. This is because if a subsequent
C           key to be found is 'PLOT' then  either of these satisfy
C           this criterion as well as 'PLOT' itself.
C
C           An example of an input text line is:
C
C           A FILE=TEST0/B FILE = TEST1/FILE=TEST2/FILE 1=TEST3/FILE 2=/
C
C           This would give the following:
C
C           CKEY='A FILE'  =>  CANS='TEST0'
C           CKEY='B FILE'  =>  CANS='TEST1'
C           CKEY='FILE'    =>  CANS='TEST0' (WRONG RESPONSE PICKED UP)
C           CKEY='FILE 1'  =>  CANS='TEST3'
C           CKEY='FILE 2'  =>  CANS=' '
C
C           It is also possible to embed responses.
C
C           An example of an input text line is:
C
C           FILE 1 =  Z1 = 23 / FILE = FILE 1 = 6 /
C
C           This would give the following:
C
C           CKEY='FILE 1'  =>  CANS='Z1 = 23'
C           CKEY=' FILE 1' =>  CANS='6'
C           CKEY='Z1'      =>  CANS='23'
C
C           Note that for CKEY='FILE'
C           the original version returns CANS='FILE 1 = 6'
C           while this updated version gives CANS='Z1 = 23'
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    26/04/91
C
C VERSION  : 1.2
C DATE     : 31-05-2007
C MODIFIED : H P Summers
C              - Increased robustness for single letter keys occurring
C                elsewhere at non-key positions in strings.
C
C VERSION  : 1.3
C DATE     : 25-03-2019
C MODIFIED : Martin O'Mullane
C             - Re-write to conform better to the specification, in
C               particular for single letter keys where key/value pairs
C               are separated by spaces rather than distinct separators.
C             - Updated file of type adf15 and adf12 (from adas316) were 
C               problematic for the old version.
C             - Values in quotes (eg, 'ca') were not recognized (adf23). 
C             - This version introduces a dependency on xxslen.
C             - Note that there is a regression for embedded keys which
C               contain a space.
C
C VERSION  : 1.4
C DATE     : 15-05-2019
C MODIFIED : Martin O'Mullane
C             - Special case handling for single character keys.
C             - Ensure that all fields are checked if a separator
C               is specified.
C
C-----------------------------------------------------------------------
      integer    ndmax
C-----------------------------------------------------------------------
      parameter (ndmax = 10)
C-----------------------------------------------------------------------
      integer    lentxt    , lenkey    , lenans      , lentmp  ,
     &           lensep    , ikey
      integer    i         , ipos1     , ipos2       , ipos3   ,
     &           L1        , L2        , num
C-----------------------------------------------------------------------
      character  ctext*(*) , ckey*(*)  , cbreak*(*)  , cans*(*)
      character  ctemp*132 , csep*1    , cstr*132    , cstr2*132
C-----------------------------------------------------------------------
      integer    indsep(ndmax)
C-----------------------------------------------------------------------

      do i = 1, ndmax
         indsep(i) = 0
      end do

      lentxt = len(ctext)
      lenkey = len(ckey)
      lensep = len(cbreak)
      lenans = len(cans)
      lentmp = 132

C-----------------------------------------------------------------------
C The break/separator must be of length 1
C-----------------------------------------------------------------------

      if (lensep.eq.0) then
         csep = ' '
      elseif (lensep.eq.1) then
         csep = cbreak
      elseif (lensep.gt.1) then
         goto 999
      endif

C-----------------------------------------------------------------------
C Find the length of the key ignoring any trailing blanks
C-----------------------------------------------------------------------

      do i = 1, lenans
         cans(i:i) = ' '
      end do

      ikey = 0

      do i = 1,lenkey
         if (ckey(i:i).ne.' ') ikey = i
      end do

      if (ikey.eq.0) goto 999

C-----------------------------------------------------------------------
C Non-blank csep is present:
C  * If necessary add separators at start and end of string
C  * Determine the number of possible fields present
C  * Loop through fields checking each for presence of key
C  * Check that a key is not embedded in another key, which can
C    be a problem for single letter keys (eg E= and ISEL= in adf02)
C-----------------------------------------------------------------------

      if (csep.ne.' ') then

          ctemp = ctext(1:lentxt)
          call xxslen(ctemp, L1, L2)
          if (ctemp(L1:L1).NE.csep) ctemp = csep//ctemp(L1:L2)
          call xxslen(ctemp, L1, L2)
          if (ctemp(L2:L2).NE.csep) ctemp = ctemp(L1:L2)//csep
          call xxslen(ctemp, L1, L2)

          num = 0
          do i = L1, L2
             if (ctemp(i:i).eq.csep) then
                num = num + 1
                indsep(num) = i
             endif
          end do
          num = num
          if (num.GT.ndmax) then
             write(0,*)'Too many fields ', num, ' GT ', ndmax
             goto 999
          endif

          do i = 1, num-1
          
             cstr  = ctemp(indsep(i)+1:indsep(i+1)-1)
             call xxslen(cstr, L1, L2)
             
             ipos1 = index(cstr, '=')
             ipos2 = index(cstr, ckey(1:ikey))
                          
             if (ipos2.gt.0.and.ipos2.lt.ipos1) then
                if (ipos1-ipos2.GT.1) then
                    L1 = ipos2 + ikey
                    if ((cstr(L1:L1).EQ. ' ').or.
     &                  (cstr(L1:L1).EQ. '=')) then
                       cstr2= cstr(ipos1+1:L2)
                    endif
                else
                   cstr2 = cstr(ipos1+1:L2)
                endif
                call xxslen(cstr2, L1, L2)
                if (L1.GT.0) then
                   cans = cstr2(L1:L2)
                else
                   cans = ' '
                endif
             endif

          end do

      endif

C-----------------------------------------------------------------------
C The fields in ctext are separated by an arbitrary number of blanks.
C Find '=' after key and test for another '='. If another '=' is present
C remove any non-blank text at end of the line since this is another
C key. Otherwise do nothing. Finally trim blank space from the start
C and end.
C-----------------------------------------------------------------------

      if (csep.eq.' ') then

          ctemp = ' '
          ipos1 = index(ctext(1:lentxt), ckey(1:ikey))
          if (ipos1.eq.0) goto 999

          ctemp = ctext(ipos1:lentxt)
          ipos1 = index(ctemp, '=')
          if (ipos1.eq.0) goto 999
          ctemp = ctemp(ipos1+1:lentxt)

          ipos2 = index(ctemp, '=')
          if (ipos2.eq.0) then
             ctemp = ctemp(1:lentxt)
          else
             ctemp = ctemp(1:ipos2-1)
             call xxslen(ctemp, L1, L2)
             ctemp = ctemp(1:L2)
             i = L2
             do while (ctemp(i:i).ne.' '.and.i.ge.1)
                i     = i - 1
                ipos3 = i
             end do
             ctemp = ctemp(L1:ipos3)
          endif

          call xxslen(ctemp, L1, L2)

          cans  = ctemp(L1:L2)

      endif

C-----------------------------------------------------------------------
C Return cans and for invalid or key found return 'cans' as blank
C-----------------------------------------------------------------------

      return

  999 cans = ' '
      return

C-----------------------------------------------------------------------

      end
