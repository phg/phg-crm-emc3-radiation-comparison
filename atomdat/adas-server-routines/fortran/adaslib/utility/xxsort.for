      subroutine xxsort(n,arr,brr)
      
      implicit none
C-----------------------------------------------------------------------
C
C    ****************** fortran77 subroutine: xxsort ****************** 
C
C  purpose: Sorts array 'arr' into ascending numerical order, with
C           corresponding rearrangement of 'brr'. Uses Shell's method.
C
C  calling program: various
C
C  input        : (i*4) n      = number of array elements
C
C  input/output : (r*8) arr()  = array to be sorted into ascending order
C  input/output : (r*8) brr()  = array corresponding to 'arr'
C
C
C  author: Paul Bryans, University of Strathclyde
C
C  date:   02/12/04
C
C  update: 
C
C-----------------------------------------------------------------------
      integer n , i , j      , inc
C-----------------------------------------------------------------------
      real*8  a , b , arr(n) , brr(n)
C-----------------------------------------------------------------------
      inc=1
    1 inc=3*inc+1
      if(inc.le.n)goto 1
    2 continue
        inc=inc/3
	do i=inc+1,n
	  a=arr(i)
	  b=brr(i)
	  j=i
    3     if(arr(j-inc).gt.a)then
            arr(j)=arr(j-inc)
	    brr(j)=brr(j-inc)
	    j=j-inc
	    if(j.le.inc)goto 4
	  goto 3
	  endif
    4     arr(j)=a
          brr(j)=b
        enddo
      if(inc.gt.1)goto 2
C-----------------------------------------------------------------------
      return
      end
