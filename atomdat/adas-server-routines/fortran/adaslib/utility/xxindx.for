CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/utility/xxindx.for,v 1.1 2004/07/06 15:36:50 whitefor Exp $ Date $Date: 2004/07/06 15:36:50 $
CX
      SUBROUTINE XXINDX( N , ARR , INDX )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXINDX *********************
C
C  PURPOSE: INDEXES AN ARRAY 'ARR' OF LENGTH 'N'. IT OUTPUTS THE ARRAY
C           'INDX()' SUCH THAT 'ARR(INDX(J))' IS  IN  ASCENDING  ORDER
C           FOR J=1,2,...,N.   THE INPUT QUANTITIES 'N' AND 'ARR'  ARE
C           NOT CHANGED.
C
C  REFERENCE:  NUMERICAL RECIPES: The Art of Scientific Computing
C              (FORTRAN Version).
C              W.H.Press, B.P.Flannery, S.A.Teukolsky & W.T.Vetterling.
C              (Cambridge University Press, Cambridge). 1989. p.233
C              ISBN 0 521 38330 7
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  N       = INPUT ARRAY 'ARR()' LENGTH
C  INPUT :    (R*8)  ARR()   = ARRAY TO BE INDEXED
C  OUTPUT:    (I*4)  INDX()  = ASCENDING ORDER INDEX
C
C             (R*8)  INDXT   = TEMPORARY STORAGE FOR 'INDX' ELEMENT
C             (I*4)  L       = "HIRING" PHASE INDEX
C             (I*4)  IR      = "RETIREMENT-AND-PROMOTION"  PHASE INDEX
C             (I*4)  I       = GENERAL USE
C             (I*4)  J       = GENERAL USE
C
C             (R*8)  Q       = TEMPORARY STORAGE FOR 'ARR' ELEMENT
C
C ROUTINES:  NONE
C
C NOTE:      USES THE HEAPSORT METHOD.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER N        , INDXT     , L        , IR       , I       , J
      INTEGER INDX(N)
C-----------------------------------------------------------------------
      REAL*8  Q
      REAL*8  ARR(N)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
         DO 1 I=1,N
            INDX(I)=I
    1    CONTINUE
C-----------------------------------------------------------------------
      IF (N.EQ.1) RETURN
      L=INT(0.5*DBLE(N)) + 1
      IR=N
C-----------------------------------------------------------------------
    2 CONTINUE
         IF (L.GT.1) THEN
            L=L-1
            INDXT=INDX(L)
            Q=ARR(INDXT)
         ELSE
            INDXT=INDX(IR)
            Q=ARR(INDXT)
            INDX(IR)=INDX(1)
            IR=IR-1
               IF (IR.EQ.1) THEN
                  INDX(1)=INDXT
                  RETURN
               ENDIF
         ENDIF
C-----------------------------------------------------------------------
      I=L
      J=L+L
C-----------------------------------------------------------------------
    3    IF (J.LE.IR) THEN
               IF (J.LT.IR) THEN
                  IF (ARR(INDX(J)).LT.ARR(INDX(J+1))) J=J+1
               ENDIF
               IF (Q.LT.ARR(INDX(J))) THEN
                  INDX(I)=INDX(J)
                  I=J
                  J=J+J
               ELSE
                  J=IR+1
               ENDIF
            GOTO 3
         ENDIF
      INDX(I)=INDXT
      GOTO 2
C-----------------------------------------------------------------------
      END
