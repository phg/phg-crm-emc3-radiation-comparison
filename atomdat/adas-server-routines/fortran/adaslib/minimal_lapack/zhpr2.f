*
*  =========== DOCUMENTATION ===========
*
* Online html documentation available at
*            http://www.netlib.org/lapack/explore-html/
*
*  Definition:
*  ===========
*
*       SUBROUTINE ZHPR2(UPLO,N,ALPHA,X,INCX,Y,INCY,AP)
*
*       .. Scalar Arguments ..
*       COMPLEX*16 ALPHA
*       INTEGER INCX,INCY,N
*       CHARACTER UPLO
*       ..
*       .. Array Arguments ..
*       COMPLEX*16 AP(*),X(*),Y(*)
*       ..
*
*
*> \par Purpose:
*  =============
*>
*> \verbatim
*>
*> ZHPR2  performs the hermitian rank 2 operation
*>
*>    A := alpha*x*y**H + conjg( alpha )*y*x**H + A,
*>
*> where alpha is a scalar, x and y are n element vectors and A is an
*> n by n hermitian matrix, supplied in packed form.
*> \endverbatim
*
*  Arguments:
*  ==========
*
*> \param[in] UPLO
*> \verbatim
*>          UPLO is CHARACTER*1
*>           On entry, UPLO specifies whether the upper or lower
*>           triangular part of the matrix A is supplied in the packed
*>           array AP as follows:
*>
*>              UPLO = 'U' or 'u'   The upper triangular part of A is
*>                                  supplied in AP.
*>
*>              UPLO = 'L' or 'l'   The lower triangular part of A is
*>                                  supplied in AP.
*> \endverbatim
*>
*> \param[in] N
*> \verbatim
*>          N is INTEGER
*>           On entry, N specifies the order of the matrix A.
*>           N must be at least zero.
*> \endverbatim
*>
*> \param[in] ALPHA
*> \verbatim
*>          ALPHA is COMPLEX*16
*>           On entry, ALPHA specifies the scalar alpha.
*> \endverbatim
*>
*> \param[in] X
*> \verbatim
*>          X is COMPLEX*16 array of dimension at least
*>           ( 1 + ( n - 1 )*abs( INCX ) ).
*>           Before entry, the incremented array X must contain the n
*>           element vector x.
*> \endverbatim
*>
*> \param[in] INCX
*> \verbatim
*>          INCX is INTEGER
*>           On entry, INCX specifies the increment for the elements of
*>           X. INCX must not be zero.
*> \endverbatim
*>
*> \param[in] Y
*> \verbatim
*>          Y is COMPLEX*16 array of dimension at least
*>           ( 1 + ( n - 1 )*abs( INCY ) ).
*>           Before entry, the incremented array Y must contain the n
*>           element vector y.
*> \endverbatim
*>
*> \param[in] INCY
*> \verbatim
*>          INCY is INTEGER
*>           On entry, INCY specifies the increment for the elements of
*>           Y. INCY must not be zero.
*> \endverbatim
*>
*> \param[in,out] AP
*> \verbatim
*>          AP is COMPLEX*16 array of DIMENSION at least
*>           ( ( n*( n + 1 ) )/2 ).
*>           Before entry with  UPLO = 'U' or 'u', the array AP must
*>           contain the upper triangular part of the hermitian matrix
*>           packed sequentially, column by column, so that AP( 1 )
*>           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 1, 2 )
*>           and a( 2, 2 ) respectively, and so on. On exit, the array
*>           AP is overwritten by the upper triangular part of the
*>           updated matrix.
*>           Before entry with UPLO = 'L' or 'l', the array AP must
*>           contain the lower triangular part of the hermitian matrix
*>           packed sequentially, column by column, so that AP( 1 )
*>           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 2, 1 )
*>           and a( 3, 1 ) respectively, and so on. On exit, the array
*>           AP is overwritten by the lower triangular part of the
*>           updated matrix.
*>           Note that the imaginary parts of the diagonal elements need
*>           not be set, they are assumed to be zero, and on exit they
*>           are set to zero.
*> \endverbatim
*
*  Authors:
*  ========
*
*> \author Univ. of Tennessee
*> \author Univ. of California Berkeley
*> \author Univ. of Colorado Denver
*> \author NAG Ltd.
*
*> \date December 2016
*
*> \ingroup complex16_blas_level2
*
*> \par Further Details:
*  =====================
*>
*> \verbatim
*>
*>  Level 2 Blas routine.
*>
*>  -- Written on 22-October-1986.
*>     Jack Dongarra, Argonne National Lab.
*>     Jeremy Du Croz, Nag Central Office.
*>     Sven Hammarling, Nag Central Office.
*>     Richard Hanson, Sandia National Labs.
*> \endverbatim
*>
*  =====================================================================
      SUBROUTINE zhpr2(UPLO,N,ALPHA,X,INCX,Y,INCY,AP)
*
*  -- Reference BLAS level2 routine (version 3.7.0) --
*  -- Reference BLAS is a software package provided by Univ. of Tennessee,    --
*  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
*     December 2016
*
*     .. Scalar Arguments ..
      COMPLEX*16 ALPHA
      INTEGER INCX,INCY,N
      CHARACTER UPLO
*     ..
*     .. Array Arguments ..
      COMPLEX*16 AP(*),X(*),Y(*)
*     ..
*
*  =====================================================================
*
*     .. Parameters ..
      COMPLEX*16 ZERO
      parameter(zero= (0.0d+0,0.0d+0))
*     ..
*     .. Local Scalars ..
      COMPLEX*16 TEMP1,TEMP2
      INTEGER I,INFO,IX,IY,J,JX,JY,K,KK,KX,KY
*     ..
*     .. External Functions ..
      LOGICAL LSAME
      EXTERNAL lsame
*     ..
*     .. External Subroutines ..
      EXTERNAL xerbla
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC dble,dconjg
*     ..
*
*     Test the input parameters.
*
      info = 0
      IF (.NOT.lsame(uplo,'U') .AND. .NOT.lsame(uplo,'L')) THEN
          info = 1
      ELSE IF (n.LT.0) THEN
          info = 2
      ELSE IF (incx.EQ.0) THEN
          info = 5
      ELSE IF (incy.EQ.0) THEN
          info = 7
      END IF
      IF (info.NE.0) THEN
          CALL xerbla('ZHPR2 ',info)
          RETURN
      END IF
*
*     Quick return if possible.
*
      IF ((n.EQ.0) .OR. (alpha.EQ.zero)) RETURN
*
*     Set up the start points in X and Y if the increments are not both
*     unity.
*
      IF ((incx.NE.1) .OR. (incy.NE.1)) THEN
          IF (incx.GT.0) THEN
              kx = 1
          ELSE
              kx = 1 - (n-1)*incx
          END IF
          IF (incy.GT.0) THEN
              ky = 1
          ELSE
              ky = 1 - (n-1)*incy
          END IF
          jx = kx
          jy = ky
      END IF
*
*     Start the operations. In this version the elements of the array AP
*     are accessed sequentially with one pass through AP.
*
      kk = 1
      IF (lsame(uplo,'U')) THEN
*
*        Form  A  when upper triangle is stored in AP.
*
          IF ((incx.EQ.1) .AND. (incy.EQ.1)) THEN
              DO 20 j = 1,n
                  IF ((x(j).NE.zero) .OR. (y(j).NE.zero)) THEN
                      temp1 = alpha*dconjg(y(j))
                      temp2 = dconjg(alpha*x(j))
                      k = kk
                      DO 10 i = 1,j - 1
                          ap(k) = ap(k) + x(i)*temp1 + y(i)*temp2
                          k = k + 1
   10                 CONTINUE
                      ap(kk+j-1) = dble(ap(kk+j-1)) +
     +                             dble(x(j)*temp1+y(j)*temp2)
                  ELSE
                      ap(kk+j-1) = dble(ap(kk+j-1))
                  END IF
                  kk = kk + j
   20         CONTINUE
          ELSE
              DO 40 j = 1,n
                  IF ((x(jx).NE.zero) .OR. (y(jy).NE.zero)) THEN
                      temp1 = alpha*dconjg(y(jy))
                      temp2 = dconjg(alpha*x(jx))
                      ix = kx
                      iy = ky
                      DO 30 k = kk,kk + j - 2
                          ap(k) = ap(k) + x(ix)*temp1 + y(iy)*temp2
                          ix = ix + incx
                          iy = iy + incy
   30                 CONTINUE
                      ap(kk+j-1) = dble(ap(kk+j-1)) +
     +                             dble(x(jx)*temp1+y(jy)*temp2)
                  ELSE
                      ap(kk+j-1) = dble(ap(kk+j-1))
                  END IF
                  jx = jx + incx
                  jy = jy + incy
                  kk = kk + j
   40         CONTINUE
          END IF
      ELSE
*
*        Form  A  when lower triangle is stored in AP.
*
          IF ((incx.EQ.1) .AND. (incy.EQ.1)) THEN
              DO 60 j = 1,n
                  IF ((x(j).NE.zero) .OR. (y(j).NE.zero)) THEN
                      temp1 = alpha*dconjg(y(j))
                      temp2 = dconjg(alpha*x(j))
                      ap(kk) = dble(ap(kk)) +
     +                         dble(x(j)*temp1+y(j)*temp2)
                      k = kk + 1
                      DO 50 i = j + 1,n
                          ap(k) = ap(k) + x(i)*temp1 + y(i)*temp2
                          k = k + 1
   50                 CONTINUE
                  ELSE
                      ap(kk) = dble(ap(kk))
                  END IF
                  kk = kk + n - j + 1
   60         CONTINUE
          ELSE
              DO 80 j = 1,n
                  IF ((x(jx).NE.zero) .OR. (y(jy).NE.zero)) THEN
                      temp1 = alpha*dconjg(y(jy))
                      temp2 = dconjg(alpha*x(jx))
                      ap(kk) = dble(ap(kk)) +
     +                         dble(x(jx)*temp1+y(jy)*temp2)
                      ix = jx
                      iy = jy
                      DO 70 k = kk + 1,kk + n - j
                          ix = ix + incx
                          iy = iy + incy
                          ap(k) = ap(k) + x(ix)*temp1 + y(iy)*temp2
   70                 CONTINUE
                  ELSE
                      ap(kk) = dble(ap(kk))
                  END IF
                  jx = jx + incx
                  jy = jy + incy
                  kk = kk + n - j + 1
   80         CONTINUE
          END IF
      END IF
*
      RETURN
*
*     End of ZHPR2 .
*
      END
