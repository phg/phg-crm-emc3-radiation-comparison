*> \brief \b DZNRM2
*
*  =========== DOCUMENTATION ===========
*
* Online html documentation available at
*            http://www.netlib.org/lapack/explore-html/
*
*  Definition:
*  ===========
*
*       DOUBLE PRECISION FUNCTION DZNRM2(N,X,INCX)
*
*       .. Scalar Arguments ..
*       INTEGER INCX,N
*       ..
*       .. Array Arguments ..
*       COMPLEX*16 X(*)
*       ..
*
*
*> \par Purpose:
*  =============
*>
*> \verbatim
*>
*> DZNRM2 returns the euclidean norm of a vector via the function
*> name, so that
*>
*>    DZNRM2 := sqrt( x**H*x )
*> \endverbatim
*
*  Authors:
*  ========
*
*> \author Univ. of Tennessee
*> \author Univ. of California Berkeley
*> \author Univ. of Colorado Denver
*> \author NAG Ltd.
*
*> \date December 2016
*
*> \ingroup double_blas_level1
*
*> \par Further Details:
*  =====================
*>
*> \verbatim
*>
*>  -- This version written on 25-October-1982.
*>     Modified on 14-October-1993 to inline the call to ZLASSQ.
*>     Sven Hammarling, Nag Ltd.
*> \endverbatim
*>
*  =====================================================================
      DOUBLE PRECISION FUNCTION dznrm2(N,X,INCX)
*
*  -- Reference BLAS level1 routine (version 3.7.0) --
*  -- Reference BLAS is a software package provided by Univ. of Tennessee,    --
*  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
*     December 2016
*
*     .. Scalar Arguments ..
      INTEGER INCX,N
*     ..
*     .. Array Arguments ..
      COMPLEX*16 X(*)
*     ..
*
*  =====================================================================
*
*     .. Parameters ..
      DOUBLE PRECISION ONE,ZERO
      parameter(one=1.0d+0,zero=0.0d+0)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION NORM,SCALE,SSQ,TEMP
      INTEGER IX
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC abs,dble,dimag,sqrt
*     ..
      IF (n.LT.1 .OR. incx.LT.1) THEN
          norm = zero
      ELSE
          scale = zero
          ssq = one
*        The following loop is equivalent to this call to the LAPACK
*        auxiliary routine:
*        CALL ZLASSQ( N, X, INCX, SCALE, SSQ )
*
          DO 10 ix = 1,1 + (n-1)*incx,incx
              IF (dble(x(ix)).NE.zero) THEN
                  temp = abs(dble(x(ix)))
                  IF (scale.LT.temp) THEN
                      ssq = one + ssq* (scale/temp)**2
                      scale = temp
                  ELSE
                      ssq = ssq + (temp/scale)**2
                  END IF
              END IF
              IF (dimag(x(ix)).NE.zero) THEN
                  temp = abs(dimag(x(ix)))
                  IF (scale.LT.temp) THEN
                      ssq = one + ssq* (scale/temp)**2
                      scale = temp
                  ELSE
                      ssq = ssq + (temp/scale)**2
                  END IF
              END IF
   10     CONTINUE
          norm = scale*sqrt(ssq)
      END IF
*
      dznrm2 = norm
      RETURN
*
*     End of DZNRM2.
*
      END
