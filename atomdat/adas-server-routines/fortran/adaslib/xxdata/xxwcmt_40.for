       subroutine xxwcmt_40( iunit      ,
     &                       ndstore    , ndcmt    , ndfld   , ndsyn   ,
     &                       ndion      , ndopt    , ndlev   ,
     &                       lroot      , lsuper   ,
     &                       nfld       , isyn     , fldk    ,
     &                       lfld       ,
     &                       iz0        , iz1      , popcode ,
     &                       dsn04      , dsn18    ,
     &                       nion       , cion     , lion    ,
     &                       nopt       , copt     , lopt    ,
     &                       nlev       , config   , cterm   , ener    ,
     &                       esym       , dsnpt    ,
     &                       iptnl      , is       , tabul   , units   ,
     &                       nbsel      , nspb     , nspp    ,
     &                       nrange     , npixr    , wvminr  , wvmaxr  ,
     &                       ilzr       , ihzr     ,
     &                       ctype      , ispbr    , isppr   ,
     &                       irg        , ipr      , iwr     ,
     &                       code       , producer , date
     &                     )
       implicit none
c-----------------------------------------------------------------------
c
c ****************** fortran 77 subroutine: xxwcmt_40 ******************
c
c  purpose: To write the comment section of an adf40 file .
c
c  notes:  (1) The  routine checks for the presence of field keys of
c              their synonyms, returns a value for the field if
c              present and the location & range of comment lines
c              associated with the field key if appropriate.  This
c              follows the general pattern of adf comment reading
c              subroutines.
c          (2) Analysis is carried out on the associated comment lines
c              of specific field keys to isolate and return relevant
c              information.  This is specific to the adf number.  These
c              returned data should match the data which needs to be
c              provided to enable writing of identical comment lines
c              by 'xxwcmt_15.for'
c
c  subroutine:
c
c  input : (i*4)  iunit      = unit number for input adf40 file
c  input : (i*4)  ndstore    = maximum number  of  input data-blocks
c                              that can be stored
c  input : (i*4)  ndcmt      = maximum number of comment text lines
c  input : (i*4)  ndfld      = maximum number of search field
c  input : (i*4)  ndion      = maximum number of selective ionis coefft.
c                              inclusions
c  input : (i*4)  ndopt      = maximum number of options keys set in
c                              population code
c  input : (i*4)  ndlev      = maximum number of levels included in
c                              population calculation
c
c  input : (l*4)  lroot      = .true.  => output root comments
c                              .false. => do not output root comments
c  input : (l*4)  lsuper     = .true.  => output superstage comments
c                              .false. => do not output superstage comments
c  input : (i*4)  nfld       = number of fields for adf40 comments
c  input : (i*4)  isyn()     = number of synonyms for fields
c                              1st dim: field index (1->nfld)
c  input :  (c*40) fldk(,)    = field keys
c                              1st dim: field index (1->nfld)
c                              2nd dim: synonymn index (1->isyn())
c  input : (l*4)  lfld()     = .true.  => field  available for comments
c                            = .false. => field not available
c                              1st dim: field index (1->nfld)
c  input : (i*4)  iz0        = nuclear charge
c  input : (i*4)  iz1        = emitting ion charge+1
c  input : (c*7)  popcode    = propulation processing code
c  input : (c*120)dsn04      = adf04 file used by population code
c  input : (c*120)dsn18      = adf18 map file used to access projection
c  input : (i*4)  nion       = number of selective ionis. coefft.
c                             inclusions in population calculation
c  input : (c*5)  cion()     = selec. ionis coefft. spec as (ispb,ispp)
c                              1st dim: ionis coefft. list index
c  input : (l*1)  lion()     = .true. => included
c                            = .false.=> not included
c                              1st dim: ionis coefft. list index
c  input : (i*4)  nopt       = number of option keys present for
c                              population calculation
c  input : (c*6)  copt()     = option specification strings as l*****
c                              1st dim: option list index
c  input : (l*1)  lopt()     = .true. => set
c                            = .false.=> not set
c                              1st dim: option list index
c  input : (i*4)  nlev       = number of energy levels included in
c                              population calculation
c  input : (c*19) config()   = configuration string
c                              1st dim: level list index
c  input : (c*14) cterm()    = term/level specification string
c                              1st dim: level list index
c  input : (r*8)  ener()     = energy level relative to lowest (cm^-1)
c                              1st dim: level list index
c  input : (c*2)  esym       = element symbol
c  input : (c*120)dsnpt      = parent file template used to create
c                              current child partition (blank if root)
c  input : (i*4)  iptnl      = current partition level
c  input : (i*4)  is         = superstage label
c  input : (c*40) tabul      = adf40 quantity tabulated specification
c  input : (c*40) units      = adf40 units use specification
c  input : (i*4)  nspb       = number of excitation (base) drivers for
c                              superstage (= icnctv(is))
c  input : (i*4)  nspp       = number of recombination (parent) drivers
c                              for superstage (=icnctv(is+1))
c  input : (i*4)  nbsel      = number of emissivity line blocks in the
c                              adf40 file
c  input : (i*4)  nrange     = number of distinct wave length ranges
c                              in the  adf40 file
c
c  input : (i*4)  npixr()    = number of pivels in wavelength range
c                              1st dim: wavelength range count
c  input : (r*8)  wvminr()   = min. wavelength (A) of wavelength range
c                              1st dim: wavelength range count
c  input : (r*8)  wvmaxr()   = max. wavelength (A) of wavelength range
c                              1st dim: wavelength range count
c  input : (i*4)  ilzr()     = lowest charge state contributing to
c                              the wavelength range
c                              1st dim: wavelength range count
c  input : (i*4)  ihzr()     = highest charge state contributing to
c                              the wavelength range
c                              1st dim: wavelength range count
c  input : (c*5)  ctype()    = transition type for each line block
c                              1st dim: index of block in adf40 file
c  input : (i*4)  ispbr()    = base driver index for each line block
c                              1st dim: index of block in adf40 file
c  input : (i*4)  isppr()    = parent driver index for each line block
c                              1st dim: index of block in adf40 file
c  input : (i*4)  iszr()     = ion charge relating to each line
c                              1st dim: index of block in adf40 file
c  input : (i*4)  irg()      = transition group attribution of
c                              emissivity  line block
c                              1st dim: index of block in adf40 file
c  input : (i*4)  ipr()      = power ranking of emissivity line
c                              block (note power is a composite
c                              attribute of a transition group)
c                              1st dim: index of block in adf40 file
c  input : (c*7)  code       = ADAS code which generated the
c                              superstage adf40 file
c  input : (c*30) producer   = producer of the adf40 file
c  input : (c*8)  date       = date of creattion of the superstage
c                              adf40 file
c
c routines:
c          routine    source    brief description
c          ----------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          r8fctn     adas      convert string to real number
c          xxslen     adas      find string less front and tail blanks
c          xxcase     adas      convert a string to upper or lower case
c          xxordr     adas      order a real vector retaining indexing
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    15/06/06
c
c
c version  : 1.1
c date     : 15-06-2006
c modified : H P Summers
c             - first version.
c
c-----------------------------------------------------------------------
       integer    iunit
       integer    ndstore    , ndcmt      , ndfld      , ndsyn   ,
     &            ndion      , ndopt      , ndlev
       integer    iz0        , iz1
       integer    nfld
       integer    iptnl      , is         , nspb       , nspp
       integer    nbsel      , nrange
       integer    i1         , i2
       integer    i          , j
       integer    ifirst     , ilast
       integer    nion       , nopt       , nlev
c-----------------------------------------------------------------------
       character  dsn04*80   , dsn18*80   , dsnpt*120
       character  popcode*7  , esym*2     , tabul*60   , units*60
       character  code*7     , producer*30, date*8
       character  cdash*80   , cblank*80  , cddash*40  , clone*1
c-----------------------------------------------------------------------
       logical    lroot      , lsuper
c-----------------------------------------------------------------------
       integer    isyn(ndfld)
       integer    npixr(ndstore)          ,
     &            ispbr(ndstore)          , isppr(ndstore)           ,
     &            ilzr(ndstore)           , ihzr(ndstore)
       integer    irg(ndstore)            , ipr(ndstore)             ,
     &            iwr(ndstore)
c-----------------------------------------------------------------------
       real*8     wvminr(ndstore)         , wvmaxr(ndstore)
       real*8     ener(ndlev)
c-----------------------------------------------------------------------
       character  fldk(ndfld,ndsyn)*40
       character  ctype(ndstore)*5
       character  cion(ndion)*5           , copt(ndopt)*6
       character  config(ndlev)*19        , cterm(ndlev)*14
c-----------------------------------------------------------------------
       logical    lfld(ndfld)
       logical    lion(ndion)             , lopt(ndopt)
c-----------------------------------------------------------------------
       data       cdash/'C----------------------------------------------
     &---------------------------------'/
       data       cblank/'C
     &                                  '/
       data       cddash/'========================================'/
       data       clone / 'C'/
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
       if(lroot) then

           write(iunit,'(1a80)')cdash
           write(iunit,'(a)')clone

c-----------------------------------------------------------------------
c  output field 1
c-----------------------------------------------------------------------
           if(lfld(1))then
               call xxslen(fldk(1,1),ifirst,ilast)
               write(iunit,1001)fldk(1,1)(ifirst:ilast),
     &                          cddash(ifirst:ilast)
           endif
c-----------------------------------------------------------------------
c  output field 2
c-----------------------------------------------------------------------
           if(lfld(2))then
               call xxslen(fldk(2,1),ifirst,ilast)
               write(iunit,1002)fldk(2,1)(ifirst:ilast),iz0
           endif
c-----------------------------------------------------------------------
c  output field 3
c-----------------------------------------------------------------------
           if(lfld(3))then
               call xxslen(fldk(3,1),ifirst,ilast)
               write(iunit,1003)fldk(3,1)(ifirst:ilast),iz1
           endif
c-----------------------------------------------------------------------
c  output field 4
c-----------------------------------------------------------------------
           if(lfld(4))then
               call xxslen(fldk(4,1),ifirst,ilast)
               write(iunit,1004)fldk(4,1)(ifirst:ilast),
     &                          cdash(ifirst+1:ilast+1)
           endif
c-----------------------------------------------------------------------
c  output field 5
c-----------------------------------------------------------------------
           if(lfld(5))then
               call xxslen(fldk(5,1),ifirst,ilast)
               call xxslen(popcode,i1,i2)
               write(iunit,1005)fldk(5,1)(ifirst:ilast),popcode(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 6
c-----------------------------------------------------------------------
           if(lfld(6))then
               call xxslen(fldk(6,1),ifirst,ilast)
               call xxslen(dsn04,i1,i2)
               write(iunit,1006)fldk(6,1)(ifirst:ilast),dsn04(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 7
c-----------------------------------------------------------------------
           if(lfld(7))then
               call xxslen(fldk(7,1),ifirst,ilast)
               call xxslen(dsn18,i1,i2)
               write(iunit,1007)fldk(7,1)(ifirst:ilast),dsn18(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 8
c-----------------------------------------------------------------------
           if(lfld(8))then
               call xxslen(fldk(8,1),ifirst,ilast)
               write(iunit,1008)fldk(8,1)(ifirst:ilast),
     &                      (cion(j),lion(j),j=1,nspp)
               if(nspb.gt.1) then
                   do i=2,nspb
                     write(iunit,1008)cblank(ifirst+1:ilast+1),
     &                      (cion((i-1)*nspp+j),lion((i-1)*nspp+j),
     &                      j=1,nspp)
                   enddo
               endif
               write(iunit,'(a)')clone
           endif
c-----------------------------------------------------------------------
c  output field 9
c-----------------------------------------------------------------------
           if(lfld(9))then
               call xxslen(fldk(9,1),ifirst,ilast)
               write(iunit,1009)fldk(9,1)(ifirst:ilast),
     &                      (copt(i),lopt(i),i=1,min0(nopt,4))
               if(nopt.gt.4) then
                   write(iunit,1009)cblank(ifirst+1:ilast+1),
     &                      (copt(i),lopt(i),i=5,nopt)
               endif
               write(iunit,'(a)')clone
           endif
c-----------------------------------------------------------------------
c  output field 10
c-----------------------------------------------------------------------
           if(lfld(10))then
               call xxslen(fldk(10,1),ifirst,ilast)
               write(iunit,1010)fldk(10,1)(ifirst:ilast),
     &                          cdash(ifirst+1:ilast+1)
           endif
c-----------------------------------------------------------------------
c  output field 11
c-----------------------------------------------------------------------
           if(lfld(11))then
               write(iunit,1011)
               do i=1,nlev
                  write(iunit,'(1a1,i5,3x,1a19,1x,1a14,1x,f15.1)')
     &                         'C',i,config(i),cterm(i),ener(i)
               enddo
               write(iunit,'(a)')clone
               write(iunit,'(a)')clone
               write(iunit,'(a)')clone
           endif


        endif

        if(lsuper) then

           write(iunit,'(1a80)')cdash
           write(iunit,'(a)')clone

cc-----------------------------------------------------------------------
c  output field 12
c-----------------------------------------------------------------------
            if(lfld(12))then
               call xxslen(fldk(12,1),ifirst,ilast)
               write(iunit,1012)fldk(12,1)(ifirst:ilast),
     &                          cddash(ifirst:ilast)
           endif
c-----------------------------------------------------------------------
c  output field 13
c-----------------------------------------------------------------------
           if(lfld(13))then
               call xxslen(fldk(13,1),ifirst,ilast)
               call xxslen(esym,i1,i2)
               write(iunit,1013)fldk(13,1)(ifirst:ilast),esym(i1:i2)
           endif
c-----------------------------------------------------------------------
c  outpute field 14
c-----------------------------------------------------------------------
           if(lfld(14))then
               call xxslen(fldk(14,1),ifirst,ilast)
               call xxslen(dsnpt,i1,i2)
               if(i1.gt.0) then
                   write(iunit,1014)fldk(14,1)(ifirst:ilast),
     &                              dsnpt(i1:i2)
               else
                   write(iunit,1014)fldk(14,1)(ifirst:ilast),
     &                              dsnpt(1:1)
               endif
           endif
c-----------------------------------------------------------------------
c output field 15
c-----------------------------------------------------------------------
           if(lfld(15))then
               call xxslen(fldk(15,1),ifirst,ilast)
               write(iunit,1015)fldk(15,1)(ifirst:ilast),iptnl
           endif
c-----------------------------------------------------------------------
c  output field 16
c-----------------------------------------------------------------------
           if(lfld(16))then
               call xxslen(fldk(16,1),ifirst,ilast)
               write(iunit,1016)fldk(16,1)(ifirst:ilast),is
           endif
c-----------------------------------------------------------------------
c  output field 17
c-----------------------------------------------------------------------
           if(lfld(17))then
               call xxslen(fldk(17,1),ifirst,ilast)
               call xxslen(tabul,i1,i2)
               write(iunit,1017)fldk(17,1)(ifirst:ilast),tabul(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 18
c-----------------------------------------------------------------------
           if(lfld(17))then
               call xxslen(fldk(18,1),ifirst,ilast)
               call xxslen(units,i1,i2)
               write(iunit,1018)fldk(18,1)(ifirst:ilast),units(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 19
c-----------------------------------------------------------------------
           if(lfld(19))then
               call xxslen(fldk(19,1),ifirst,ilast)
               write(iunit,1019)fldk(19,1)(ifirst:ilast),
     &                          cdash(ifirst+1:ilast+1)
               do i=1,nbsel
                 if(ispbr(i).gt.0) then
                     write(iunit,1020)i,npixr(irg(i)),
     &                              wvminr(irg(i)),wvmaxr(irg(i)),
     &                              ilzr(irg(i)),ihzr(irg(i)),
     &                              ctype(i),ispbr(i),nspb,
     &                              irg(i),ipr(i),iwr(i)
                 elseif(isppr(i).gt.0) then
                     write(iunit,1020)i,npixr(irg(i)),
     &                              wvminr(irg(i)),wvmaxr(irg(i)),
     &                              ilzr(irg(i)),ihzr(irg(i)),
     &                              ctype(i),isppr(i),nspp,
     &                              irg(i),ipr(i),iwr(i)
                 endif

               enddo
           endif


c-----------------------------------------------------------------------
c  output field 21
c-----------------------------------------------------------------------
           if(lfld(21))then
            call xxslen(fldk(21,1),ifirst,ilast)
               call xxslen(code,i1,i2)
               write(iunit,1021)cdash,clone,fldk(21,1)(ifirst:ilast),
     &                          code(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 22
c-----------------------------------------------------------------------
           if(lfld(22))then
            call xxslen(fldk(22,1),ifirst,ilast)
               call xxslen(producer,i1,i2)
               write(iunit,1022)fldk(22,1)(ifirst:ilast),
     &                          producer(i1:i2)
           endif
c-----------------------------------------------------------------------
c  output field 23
c-----------------------------------------------------------------------
           if(lfld(23))then
            call xxslen(fldk(23,1),ifirst,ilast)
               call xxslen(date,i1,i2)
               write(iunit,1023)fldk(23,1)(ifirst:ilast),
     &                          date(i1:i2),clone,cdash
           endif

        endif

        return
c
c-----------------------------------------------------------------------
c
 1001  format('C  ',a/
     &        'C  ',a/
     &        'C')
 1002  format('C  ',a,' : ',i2)
 1003  format('C  ',a,'   : ',i2/
     &         'c')
 1004  format('C  ',a/
     &        'C  ',a/
     &        'C')
 1005  format('C  ',a,': ',a/
     &        'C')
 1006  format('C  ',a,'    : ',a)
 1007  format('C  ',a,'  : ',a/
     &        'C')
 1008  format('C  ',a,'  : ',4(1a5,'=',l1,4x))
 1009  format('C  ',a,'  : ',4(1a6,'=',l1,2x))
 1010  format('C  ',a/
     &        'C  ',a/
     &        'C')
 1011  format('C   lv      configuration    (2S+1)L(w-1/2)      ',
     &                 'energy (cm^-1)'/
     &        'C  ---   ------------------- --------------      ',
     &                 '--------------')
 1012  format('C  ',a/
     &        'C  ',a/
     &        'C')
 1013  format('C  ',a,'   : ',a)
 1014  format('C  ',a,'  : ',a)
 1015  format('C  ',a,'  : ',i2)
 1016  format('C  ',a,' : ',i2/
     &        'C')
 1017  format('C  ',a,'       : ',a)
 1018  format('C  ',a,'            : ',a/
     &        'C')
 1019  format('C  ',a/
     &        'C  ',a/
     &        'C  '/
     &        'C  ',' isel   npix       wvlen range (A)   chrge rnge',
     &            '   type  ispb nspb'/
     &        'C  ','                  wvmin       wvmax   zmn  zmx ',
     &            '         ispp nspp    tg pr wr'/
     &        'C  ','----- ------- ---------------------- ----------',
     &            '   ----- ---- ----    -- -- --')
 1020  format('C  ',i5,1x,i6,2f12.4,1x,2i5,3x,1a5,i4,i5,i7,i3,i3)
 1021  format(1a80/a/
     &        'C  ',a,'     : ',a)
 1022  format('C  ',a,' : ',a)
 1023  format('C  ',a,'     : ',a/
     &         a/1a80)
c
c-----------------------------------------------------------------------
c

       end
