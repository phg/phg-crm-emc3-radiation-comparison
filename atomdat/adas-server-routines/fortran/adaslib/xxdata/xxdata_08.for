      SUBROUTINE XXDATA_08( IUNIT  , NDPRT  , NDLEV , NDT   ,
     &                      SEQSYM , IZ     , IZ0   , IZ1   ,
     &                      NPRNT  , NPRNTI , BWNP  ,
     &                      IPA    , CSTRPA , ISPA  , ILPA  , XJPA  ,
     &                      WPA    ,
     &                      IL     , BWNR   ,
     &                      IA     , CSTRGA , ISA   , ILA   , XJA   ,
     &                      WA     ,
     &                      IPRTI  , TPRTI  , ISPRTI, RADR  , LRADR ,
     &                      NTE    , TEA
     &                    )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXDATA_08 ******************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT ADF08 DATA SET.
C
C  CALLING PROGRAM: ADAS411
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C
C  INPUT : (I*4)  NDPRT   = MAXIMUM NUMBER OF PARENT STATES
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF RESOLVED LEVELS
C  INPUT : (I*4)  NDT     = MAX. NUMBER OF ELECTRON TEMPERATURES
C
C  OUTPUT: (C*2)  SEQSYM  = RECOMBINED ION SEQ
C  OUTPUT: (I*4)  IZ      = RECOMBINED ION CHARGE
C  OUTPUT: (I*4)  IZ0     = NUCLEAR CHARGE
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE
C  OUTPUT: (I*4)  NPRNT   = TOTAL NUMBER OF PARENTS
C  OUTPUT: (I*4)  NPRNTI  = NUMBER OF PARENTS WHICH ARE INITIAL PARENTS
C  OUTPUT: (R*8)  BWNP    = BINDING WAVE NO. OF GROUND PARENT (CM-1)
C  OUTPUT: (I*4)  IPA()   = NUMBER OF PARENT ENERGY LEVELS
C  OUTPUT: (C*18) CSTRPA()= NOMENCL./CONFIG. FOR PARENT LEVEL 'IPA()'
C  OUTPUT: (I*4)  ISPA()  = MULTIPLICITY FOR PARENT LEVEL 'IPA()'
C                           NOTE: (ISPA-1)/2 = QUANTUM NUMBER (SP)
C  OUTPUT: (I*4)  ILPA()  = QUANTUM NUMBER (LP) FOR PARENT LEVEL 'IPA()'
C  OUTPUT: (R*8)  XJPA()  = QUANTUM NUMBER (JP) FOR PARENT LEVEL 'IPA()'
C                           NOTE: (2*XJPA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WPA()   = ENERGY RELATIVE TO PARENT LEVEL 1 (CM-1)
C                           FOR PARENT LEVEL 'IPA()'
C
C  OUTPUT: (I*4)  IL      = NUMBER OF ENERGY LEVELS (TERMS) OF
C                           RECOMBINED ION
C  OUTPUT: (R*8)  BWNR    = IONISATION POTENTIAL (CM-1) OF LOWEST LEVEL
C                           OF RECOMBINED ION
C  OUTPUT: (I*4)  IA()    = RECOMBINED ION ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCL./CONFIG. FOR RECOMBINED ION LEVEL
C                           'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR RECOMBINED LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR RECOMBINED LEVEL
C                           'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J) FOR RECOMBINED LEVEL
C                           'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WA()    = ENERGY RELATIVE TO RECOMBINED LEVEL 1 (CM-1)
C                           FOR RECOMBINED LEVEL 'IA()'
C  OUTPUT: (I*4)  IPRTI() = INITIAL PARENT BLOCK INDEX
C  OUTPUT: (C*5)  TPRTI() = INITIAL PARENT BLOCK TERM
C  OUTPUT: (I*4)  ISPRTI()= INITIAL PARENT BLOCK SPIN MULTIPLICITY
C  OUTPUT: (R*8)  TEA()   = ELECTRON TEMPERATURES (K)
C  OUTPUT: (R*8)  RADR(,,)= TERM SELECTIVE DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: LEVEL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: TEMPERATURE INDEX
C  OUTPUT: (L*4)  LRADR(,)= .TRUE. => DIEL. PRESENT FOR LEVEL INDEX
C                           .FALSE.=> DIEL. NOT PRESENT FOR LEVEL INDEX
C                           1ST.DIM: LEVEL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C
C          (I*4)  INDX    = GENERAL INDEX
C          (I*4)  II      = GENERAL INDEX
C          (I*4)  I       = GENERAL INDEX
C          (I*4)  IPI     = GENERAL INDEX
C          (I*4)  IPF     = GENERAL INDEX
C          (I*4)  IPFS    = GENERAL INDEX
C          (I*4)  J       = GENERAL INDEX
C          (I*4)  K       = GENERAL INDEX
C
C          (L)    LDATA   = GENERAL READ/DO NOT READ FLAG
C          (L)    LNOPI   = FLAG TO DETERMINE WHETHER HAVE PASSED
C			    INTO A NEW INITIAL PARENT BLOCK
C
C          (C*20) C20     = GENERAL CHARACTER STRING
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4EIZ0     ADAS      RETURNS NUCL. CHARGE FROM ELEMENT SYMBOL
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C
C  AUTHOR: H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C  DATE:    10/11/97
C
C  UPDATE:  
C          
C VERSION: 1.1						DATE: 10-03-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C NOTES: Copied from dbdata.for. This is v1.1 of xxdata_08.
C 
C VERSION  : 1.1                          
C DATE     : 27-03-2008
C MODIFIED : Allan Whiteford
C              - First version
C
C VERSION  : 1.2                          
C DATE     : 12-01-201
C MODIFIED : Martin O'Mullane
C              - Allow up to 14 temperatures.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8    DZERO
C-----------------------------------------------------------------------
      PARAMETER( DZERO = 1.0D-60 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT      , I4EIZ0
      INTEGER   IUNIT       , NDPRT          , 
     &          NDLEV       , NDT
      INTEGER   IZ          , IZ0            , IZ1        ,
     &          NPRNT       , NPRNTI         , 
     &          IL          , NVALS
      INTEGER   INDX        , NTE
      INTEGER   ILINE       
      INTEGER   I           , IABT           ,
     &          IPI         , 
     &          IFIRST(1)   , ILAST(1)       , IWORD      ,
     &          J           , 
     &          NWORDS  
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRT)  , ISPA(NDPRT)    , ILPA(NDPRT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IPRTI(NDPRT), ISPRTI(NDPRT)
C-----------------------------------------------------------------------
      REAL*8    R8FCTN
      REAL*8    BWNP        , BWNR
C-----------------------------------------------------------------------
      REAL*8    XJPA(NDPRT) , WPA(NDPRT)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)
      REAL*8    TEA(NDT)
      REAL*8    RADR(NDLEV,NDPRT,NDT)
C-----------------------------------------------------------------------
      CHARACTER SEQSYM*2
      CHARACTER C7*7        , CDELIM*7       , C20*20
      CHARACTER CLINE*80    , BUFFER*158
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRT)*(*) , CSTRGA(NDLEV)*(*)
      CHARACTER TPRTI(NDPRT)*5
C-----------------------------------------------------------------------
      LOGICAL   LDATA       
C-----------------------------------------------------------------------
      LOGICAL   LRADR(NDLEV,NDPRT)
C-----------------------------------------------------------------------
      DATA      CDELIM / ' ()<>{}' /
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C ZERO ARRAYS
C-----------------------------------------------------------------------
C
      DO 5 I=1,NDLEV
        DO 4 IPI=1,NDPRT
          DO 3 J=1,NDT
            RADR(I,IPI,J)=0.0D0
            LRADR(I,IPI)=.FALSE.
    3     CONTINUE
    4   CONTINUE
    5 CONTINUE
C
C-----------------------------------------------------------------------
C INPUT ION SPECIFICATIONS.
C-----------------------------------------------------------------------
C
      READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'SEQ=')
      READ(BUFFER(INDX+5:INDX+6),'(A2)')SEQSYM
      INDX=INDEX(BUFFER,'NUCCHG=')
      READ(BUFFER(INDX+7:INDX+8),'(I2)')IZ0
C
      IZ=IZ0-I4EIZ0(SEQSYM)
      IZ1=IZ+1
C
C-----------------------------------------------------------------------
C LOCATE AND READ RECOMBINING ION (PARENT) LEVEL DATA
C-----------------------------------------------------------------------
C
   10 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'NPRNTI')
      IF(INDX.LE.0) THEN
          GO TO 10
      ENDIF
C
      INDX=INDEX(BUFFER,'BWNP=')
      READ(BUFFER(INDX+5:INDX+16),'(F12.1)') BWNP
      INDX=INDEX(BUFFER,'NPRNTI=')
      READ(BUFFER(INDX+7:INDX+8),'(I2)') NPRNTI
      READ(IUNIT,'(A)')CLINE,CLINE,CLINE
C
      LDATA=.TRUE.
C
         DO 20 I=1,NDPRT
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I6)')  ILINE
C
C  PARENT LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     NPRNT=I-1
C
C  PARENT LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'PARENT LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (CLINE,1003) IPA(I)   , C20          ,
     &                                 ISPA(I) , ILPA(I)       ,
     &                                 C7      , BUFFER(1:37)
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:37) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
                     CSTRPA(I)=C20(1:18)
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJPA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'ADF08 DATA SET PARENT ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C
                     IF(BUFFER(ILAST(1):ILAST(1)).EQ.'*') THEN
                         ILAST(1)=ILAST(1)-1
                     ENDIF
                     READ(BUFFER(IFIRST(1):ILAST(1)),*) WPA(I)
C
                  ENDIF
            ENDIF
   20    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF08 DATA SET CONTAINS > ',NDPRT,
     &                          ' PARENT LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  NPRNT = NDLEV
               ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C LOCATE AND READ RECOMBINED ION  LEVEL DATA
C-----------------------------------------------------------------------
C
   30 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'NTRM=')
      IF(INDX.LE.0) THEN
          GO TO 30
      ENDIF
C
      INDX=INDEX(BUFFER,'BWNR=')
      READ(BUFFER(INDX+5:INDX+16),'(F12.1)') BWNR
      INDX=INDEX(BUFFER,'NTRM=')
      READ(BUFFER(INDX+5:INDX+7),'(I3)') IL
      READ(IUNIT,'(A)')CLINE,CLINE,CLINE
C
      LDATA=.TRUE.
C
         DO 40 I=1,NDLEV
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I6)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (CLINE,1003) IA(I)   , C20          ,
     &                                 ISA(I)  , ILA(I)       ,
     &                                 C7      , BUFFER(1:37)
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:37) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
                     CSTRGA(I)=C20(1:18)
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'ADF08 DATA SET LEVEL ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C
                    READ(BUFFER(IFIRST(1):ILAST(1)),*) WA(I)
C
                  ENDIF
            ENDIF
   40    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF08 DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF      
C
C-----------------------------------------------------------------------
C LOCATE DATA BLOCK FOR EACH INITIAL PARENT
C-----------------------------------------------------------------------
C
      DO 140 IPI=1,NPRNTI
C
   75   READ(IUNIT,'(A)') BUFFER
        INDX=INDEX(BUFFER,'PRTI=')
        IF(INDX.LE.0) THEN
            GO TO 75
        ENDIF
C
        INDX=INDEX(BUFFER,'PRTI=')
        READ(BUFFER(INDX+5:INDX+6),'(I2)') IPRTI(IPI)
        INDX=INDEX(BUFFER,'TRMPRT=')
        READ(BUFFER(INDX+7:INDX+11),'(A5)') TPRTI(IPI)
        INDX=INDEX(BUFFER,'SPNPRT=')
        READ(BUFFER(INDX+7:INDX+8),'(I2)') ISPRTI(IPI)
C
        READ(IUNIT,'(11X,1A130)')BUFFER(1:130),BUFFER(1:130)
        J     = 1
        IWORD = 1
        CALL XXWORD( BUFFER(1:130), CDELIM    , IWORD  ,
     &               J            ,
     &               IFIRST(1)    , ILAST(1)  , NTE    )
        READ(BUFFER(1:130),'(14(E10.2,:))')(TEA(J),J=1,NTE)
C
        READ(IUNIT,'(A)')BUFFER
        LDATA=.TRUE.
C
        DO 80 I=1,NDLEV
          IF (LDATA) THEN
              READ (IUNIT,'(A)') BUFFER
              READ (BUFFER,'(I6)')  ILINE
C
C  RAD. RECOM. INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
              IF (ILINE.LE.0) THEN
                  LDATA=.FALSE.
                  NVALS=I-1
                  GO TO 140
              ELSE
                  READ (BUFFER,1006) INDX,
     &                               (RADR(INDX,IPI,J),J=1,NTE)
                  LRADR(INDX,IPI)=.TRUE.
              ENDIF
          ENDIF
   80   CONTINUE
C
  140 CONTINUE
C
C-----------------------------------------------------------------------
C
 1001 FORMAT(1X,30('*'),' XXDATA_08 ERROR ',29('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I6,5X,1A20,1X,2(I1,1X),A7,A37)
 1004 FORMAT(I5)
 1006 FORMAT(I6,5X,14(E10.2,:))
 1012 FORMAT(1X,A,' - READ VALUE = ',A7)
C
C-----------------------------------------------------------------------
C
  999 CONTINUE
  
      RETURN
      END
