C SCCS Info Module @(#)$Header: /home/adascvs/fortran/adaslib/xxdata/xxdata_21.for,v 1.1 2004/07/06 15:32:59 whitefor Exp $ Date: $Date: 2004/07/06 15:32:59 $   
C
      SUBROUTINE xxdata_21( IUNIT  , MXBE   , MXTD   , MXTT   ,
     &                      ITZ    , TSYM   , BEREF  , TDREF  ,
     &                      TTREF  , SVREF  , NBE    , BE     ,
     &                      NTDENS , TDENS  , NTTEMP , TTEMP  ,
     &                      SVT    , SVED   , DSNIN
     &                    )

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: xxdata_21 *******************
C
C  PURPOSE: TO READ DATA FROM AN EFFECTIVE BEAM STOPPING DATA SET.
C           (ADAS FORMAT ADF21).
C
C  CALLING PROGRAM: SBMS / ADAS304
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = UNIT TO WHICH DATA SET IS CONNECTED.
C  INPUT : (I*4)  MXBE      = MAXIMUM NUMBER OF BEAM ENERGIES WHICH CAN
C                             BE READ.
C  INPUT : (I*4)  MXTD      = MAXIMUM NUMBER OF TARGET DENSITIES WHICH
C                             CAN BE READ.
C  INPUT : (I*4)  MXTT      = MAXIMUM NUMBER OF TARGET TEMPERATURES
C                             WHICH CAN BE READ.
C  INPUT : (C*80) DSNIN     = NAME OF FILE TO BE READ.
C  OUTPUT: (I*4)  ITZ       = TARGET ION CHARGE.
C  OUTPUT: (C*2)  TSYM      = TARGET ION ELEMENT SYMBOL.
C  OUTPUT: (R*8)  BEREF     = REFERENCE BEAM ENERGY.
C                             UNITS: EV/AMU
C  OUTPUT: (R*8)  TDREF     = REFERENCE TARGET DENSITY.
C                             UNITS: CM-3
C  OUTPUT: (R*8)  TTREF     = REFERENCE TARGET TEMPERATURE.
C                             UNITS: EV
C  OUTPUT: (R*8)  SVREF     = STOPPING COEFFT. AT REFERENCE BEAM ENERGY,
C                             TARGET DENSITY AND TEMPERATURE.
C                             UNITS: CM3 S-1
C  OUTPUT: (I*4)  NBE       = NUMBER OF BEAM ENERGIES.
C  OUTPUT: (R*8)  BE()      = BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: MXBE
C  OUTPUT: (I*4)  NTDENS    = NUMBER OF TARGET DENSITIES.
C  OUTPUT: (R*8)  TDENS()   = TARGET DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: MXTD
C  OUTPUT: (I*4)  NTTEMP    = NUMBER OF TARGET TEMPERATURES.
C  OUTPUT: (R*8)  TTEMP()   = TARGET TEMPERATURES.
C                             UNITS: EV
C                             DIMENSION: MXTT
C  OUTPUT: (R*8)  SVT()     = STOPPING COEFFT. AT REFERENCE BEAM ENERGY
C                             AND TARGET DENSITY.
C                             UNITS: CM3 S-1
C                             DIMENSION: MXTT
C  OUTPUT: (R*8)  SVED(,)   = STOPPING COEFFT. AT REFERENCE TARGET
C                             TEMPERATURE.
C                             UNITS: CM3 S-1
C                             1ST DIMENSION: MXBE
C                             2ND DIMENSION: MXTD
C
C          (I*4)  I         = ARRAY / LOOP INDEX.
C          (I*4)  J         = ARRAY INDEX.
C
C          (C*80) LINE      = TEXT LINE IN DATA SET.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    07/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C NOTES: Copied from c4data.for. This is v1.1 of xxdata_21.
C 
C
C VERSION  : 1.1                          
C DATE     : 06-02-2004
C MODIFIED : Martin O'Mullane
C              - First version
C              - File unit is closed within the subroutine.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    IUNIT   , MXBE    , MXTD    , MXTT    , ITZ     ,
     &           NBE     , NTDENS  , NTTEMP
      INTEGER    I       , J
C-----------------------------------------------------------------------
      REAL*8     BEREF   , TDREF   , TTREF   , SVREF
C-----------------------------------------------------------------------
      CHARACTER  TSYM*2
      CHARACTER  LINE*80 , DSNIN*80
C-----------------------------------------------------------------------
      REAL*8     BE(MXBE)     , TDENS(MXTD)  , TTEMP(MXTT)  ,
     &           SVT(MXTT)
C-----------------------------------------------------------------------
      REAL*8     SVED(MXBE,MXTD)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------

      OPEN(UNIT=IUNIT, FILE=DSNIN, STATUS='UNKNOWN', ERR=9999)
      
      READ(IUNIT,1000,ERR=9999) ITZ , SVREF , TSYM
      READ(IUNIT,1004,ERR=9999) LINE
      READ(IUNIT,1001,ERR=9999) NBE , NTDENS , TTREF

      IF ( NBE .GT. MXBE ) THEN
         WRITE(I4UNIT(-1),2000) 'BEAM ENERGIES' , NBE , MXBE
         STOP
      ENDIF
      IF ( NTDENS .GT. MXTD ) THEN
         WRITE(I4UNIT(-1),2000) 'TARGET DENSITIES' , NTDENS , MXTD
         STOP
      ENDIF

      READ(IUNIT,1004,ERR=9999) LINE
      READ(IUNIT,1002,ERR=9999) ( BE(I) , I=1,NBE )
      READ(IUNIT,1002,ERR=9999) ( TDENS(I) , I=1,NTDENS )
      READ(IUNIT,1004,ERR=9999) LINE
      
      DO I = 1 , NTDENS
         READ(IUNIT,1002,ERR=9999) ( SVED(J,I) , J=1,NBE )
      END DO

      READ(IUNIT,1004,ERR=9999) LINE
      READ(IUNIT,1003,ERR=9999) NTTEMP , BEREF , TDREF

      IF ( NTTEMP .GT. MXTT ) THEN
         WRITE(I4UNIT(-1),2000) 'TARGET TEMPERATURES' , NTTEMP , MXTT
         STOP
      ENDIF

      READ(IUNIT,1004,ERR=9999) LINE
      READ(IUNIT,1002,ERR=9999) ( TTEMP(I) , I=1,NTTEMP )
      READ(IUNIT,1004,ERR=9999) LINE
      READ(IUNIT,1002,ERR=9999) ( SVT(I) , I=1,NTTEMP )

      close(iunit)
      
C-----------------------------------------------------------------------

 1000 FORMAT( I5 , 8X , D9.3 , 7X , A2)
 1001 FORMAT( 2I5 , 7X , D9.3 )
 1002 FORMAT( 8( 1X , D9.3 ) )
 1003 FORMAT( I5 , 7X , D9.3 , 7X , D9.3 )
 1004 FORMAT( A )
 2000 FORMAT( 1X , 34('*') , ' xxdata_21 ERROR ' , 33('*') //
     &        2X , 'TOO MANY ' , A , ' IN INPUT DATA SET.' /
     &        2X , 'NUMBER IN DATA SET = ' , I3 /
     &        2X , 'MAXIMUM ALLOWED = ' , I3 /
     &        2X , 'INCREASE ARRAY SIZES.' //
     &        1X , 31('*') , ' PROGRAM TERMINATED ' , 30('*') )
 2001 FORMAT( 1X , 34('*') , ' xxdata_21 ERROR ' , 33('*') //
     &        2X , 'AN ERROR OCCURRED READING FROM FILE.'/
     &        2X , 'FILE : ',A /
     &        1X , 31('*') , ' PROGRAM TERMINATED ' , 30('*') )

C-----------------------------------------------------------------------

      RETURN

9999  continue
      close(iunit)
      WRITE(I4UNIT(-1),2001) DSNIN

      STOP

      END
