      subroutine xxdata_23(iunit    ,
     &                     ndlev    , ndmet     , ndtem     , ndtext   ,
     &                     seq      , iz0       , iz        , iz1      ,
     &                     ctype    ,
     &                     bwno_f   , nlvl_f    , lmet_f    , lcstrg_f ,
     &                     ia_f     , code_f    , cstrga_f  ,
     &                     isa_f    , ila_f     , xja_f     , wa_f     ,
     &                     nmet_f   , imeta_f   ,
     &                     bwno_i   , nlvl_i    , lmet_i    , lcstrg_i ,
     &                     ia_i     , code_i    , cstrga_i  ,
     &                     isa_i    , ila_i     , xja_i     , wa_i     ,
     &                     nmet_i   , imeta_i   ,
     &                     nte_ion  , tea_ion   , lqred_ion , qred_ion ,
     &                     nf_a     , indf_a    , lyld_a    , yld_a    ,
     &                     nte_exc  , tea_exc   , lqred_exc , qred_exc ,
     &                     l_ion    , l_aug     , l_exc     ,
     &                     ntext    , ctext
     &                    )

      implicit none
c-----------------------------------------------------------------------
c  ****************** fortran77 subroutine: xxdata_23 ******************
c
c  purpose:  to fetch data from an adf23 data set.
c
c  input : (i*4)  iunit       = unit to which input file is allocated
c          (i*4)  ndlev       = maximum number of energy levels in
c                               either ion stage
c          (i*4)  ndmet       = maximum number of metastables
c          (i*4)  ndtem       = maximum number of temperatures
c          (i*4)  ndtext      = maximum number of comment text lines
c
c  output: (c*2)  seq         = iso-electronic sequence symbol
c          (i*4)  iz0         = nuclear charge
c          (i*4)  iz          = ionising ion charge
c          (i*4)  iz1         = ionised ion charge (=iz+1)
c          (c*2)  ctype       = adf23 file resol. ('ca', 'ls' or 'ic')
c          (r*8)  bwno_f      = ionis. poten. of ionised ion (cm-1)
c          (i*4)  nlvl_f      = number of levels of ionised ion
c          (l*4)  lmet_f      = .true.  => ionised metastables marked
c                               .false. => ionised metastables unmarked
c                                          (default action - mark ground)
c          (l*4)  lcstrg_f    = .true.  => standard config strings for
c                                          ionised ion states
c                               .false. => unreadable config string for
c                                          at least one ionised ion state
c          (i*4)  ia_f()      = index of ionised ion levels
c                               1st dim: ionised ion level index
c          (c*1)  code_f()    = met. or excit. DR parent marker (* or #)
c                               1st dim: ionised ion level index
c          (i*(*))cstrga_f()  = ionised ion configuration strings
c                               1st dim: ionised ion level index
c          (i*4)  isa_f()     = ionised ion level multiplicity
c                               1st dim: ionised ion level index
c          (i*4)  ila_f()     = ionised ion total orb. ang. mom.
c                               1st dim: ionised ion level index
c          (r*8)  xja_f()     = ionised ion level (stat wt-1)/2
c                               1st dim: ionised ion level index
c          (r*8)  wa_f()      = ionised ion level wave number (cm-1)
c                               1st dim: ionised ion level index
c          (i*4)  nmet_f      = number of ionised ion metastables
c          (i*4)  imeta_f()   = pointers to ionised metastables in full
c                               ionised ion state list
c                               1st dim: ionised metastable index
c          (r*8)  bwno_i      = ionis. poten. of ionising ion (cm-1)
c          (i*4)  nlvl_i      = number of levels of ionising ion
c          (l*4)  lmet_i      = .true.  => ionising metastable marked
c                               .false. => ionising metastables unmarked
c                                          (default action - mark ground)
c          (l*4)  lcstrg_i    = .true.  => standard config strings for
c                                          ionising ion states
c                               .false. => unreadable config string for
c                                          at least one ionising ion state
c          (i*4)  ia_i()      = index of ionising ion levels
c                               1st dim: ionising ion level index
c          (c*1)  code_i()    = met. or excit. DR parent marker (* or #)
c                               1st dim: ionising ion level index
c          (i*(*))cstrga_i()  = ionising ion configuration strings
c                               1st dim: ionising ion level index
c          (i*4)  isa_i()     = ionising ion level multiplicity
c                               1st dim: ionising ion level index
c          (i*4)  ila_i()     = ionising ion total orb. ang. mom.
c                               1st dim: ionising ion level index
c          (r*8)  xja_i()     = ionising ion level (stat wt-1)/2
c                               1st dim: ionising ion level index
c          (r*8)  wa_i()      = ionising ion level wave number (cm-1)
c                               1st dim: ionising ion level index
c          (i*4)  nmet_i      = number of ionising ion metastables
c          (i*4)  imeta_i()   = pointers to ionising metastables in full
c                               ionising ion state list
c                               1st dim: ionising metastable index
c          (i*4)  nte_ion()   = number of temperatures for direct ionis-
c                               ation data for initial metastable block
c                               1st dim: ionising ion metastable index
c          (r*8)  tea_ion(,)  = temperatures (K) for direct ionis-
c                               ation data for initial metastable block
c                               1st dim: ionising ion metastable index
c                               2nd dim: temperature index
c          (l*4)  lqred_ion(,)= .true. => direct ionisation data line
c                                         present for ionised ion state
c                               .false.=> data line not present for
c                                         ionised ion state.
c                               1st dim: ionising ion metastable index
c                               2nd dim: ionised ion state index
c          (r*8)  qred_ion(,,)= reduced direct ionisation rate coeffts.
c                               1st dim: ionising ion metastable index
c                               2nd dim: ionised ion state index
c                               3rd dim: temperature index
c          (i*4)  nf_a()      = number of Auger ionised ion final states
c                               1st dim: ionising ion metastable index
c          (i*4)  indf_a(,)   = Auger ionised ion final state
c                               1st dim: ionising ion metastable index
c                               2nd dim: final state index
c          (l*4)  lyld_a(,)   = .true. => Auger data for ionising ion excited state
c                               .false.=> no Auger data
c                               1st dim: ionising ion metastable index
c                               2nd dim: initial state index
c          (r*8)  yld_a(,,)   = Auger yields
c                               1st dim: ionising ion metastable index
c                               2nd dim: ionising ion excited state index
c                               3rd dim: ionised ion state index
c          (i*4)  nte_exc()   = number of temperatures for excitation
c                               data for initial metastable block
c                               1st dim: ionising ion metastable index
c          (r*8)  tea_exc(,)  = temperatures (K) for direct excit-
c                               ation data for initial metastable block
c                               1st dim: ionising ion metastable index
c                               2nd dim: temperature index
c          (l*4)  lqred_exc(,)= .true. => direct excitation data line
c                                         present for excited ion state
c                               .false.=> data line not present for
c                                         excited ion state.
c                               1st dim: ionising ion metastable index
c                               2nd dim: excited ionising ion state index
c          (r*8)  qred_exc(,,)= reduced excitation rate coeffts.
c                               1st dim: ionising ion metastable index
c                               2nd dim: excited ionising ion state index
c                               3rd dim: temperature index
c          (l*4)  l_ion()     = .true. => ionisation data present for metastable
c                               .false.=> ionisation data not present
c                               1st dim: ionising ion metastable index
c          (l*4)  l_aug()     = .true. => Auger data present for metastable
c                               .false.=> Auger data not present
c                               1st dim: ionising ion metastable index
c          (l*4)  l_exc()     = .true. => excitation data present for metastable
c                               .false.=> excitation data not present
c                               1st dim: ionising ion metastable index
c          (i*4)  ntext       = number of commment text lines
c          (c*80) ctext()     = comment text lines
c                               1st dim: index of text lines
c
c
c  routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          i4eiz0     adas      fetch nuclear charge for element symbol
c          xfesym     adas      fetch element symbol for nuclear charge
c          xxcase     adas      convert string to lower or upper case
c          xxhkey     adas      extract a key name value from a string
c          xxlast     adas      find last occurence of char in string
c          xxslen     adas      find first and last characters of string
c          xxdtes     adas      detect if config string is eissner/standard
c          xxcftr     adas      covert config string between eissner/standard
c
c  author:  Hugh Summers
c  date  :  22-05-2008
c
c
c  version  : 1.1
c  date     : 22-05-2008
c  modified : Hugh Summers
c               - first version
c
c  version  : 1.2
c  date     : 23-08-2010
c  modified : Martin O'Mullane
c               - extra arguments for xxdtes.
c
c  version  : 1.3
c  date     : 11-11-2013
c  modified : Martin O'Mullane
c               - final error messages used incorrect format statement.
c
c-----------------------------------------------------------------------
      integer   nsym    , idlev
c-----------------------------------------------------------------------
      real*8    const1
c-----------------------------------------------------------------------
      parameter( nsym = 92 , idlev= 100 , const1=8065.541d0 )
c-----------------------------------------------------------------------
      integer   i4unit     , i4eiz0     , lenstr
      integer   iunit
      integer   ndlev      , ndmet      , ndtem    , ndtext
      integer   iz0        , iz         , iz1      , izf
      integer   nlvl_f     , nlvl_i
      integer   ind1       , ind2       , istart   , istop   ,
     &          i          , j          , indx_fb  , indx_lb
      integer   iprf       , it         , nte
      integer   imet_i     , imet_count , indi     , ilvl
      integer   iword      , nwords     ,
     &          ilen_index , ilen_cnfg  , ilen_s     ,
     &          ilen_l     , ilen_j     , ilen_wnf
      integer   nvlce      , lvlce      , ilen
      integer   nmet_i     , nmet_f
      integer   ntext
c-----------------------------------------------------------------------
      real*8    bwno_f       , bwno_i
c-----------------------------------------------------------------------
      character seq*2      , esym*2     , ctype*2    , xfesym*2
      character c10*10     , c22*22     , c80*80     , c256*256
      character cline*80   , clong*256
      character cbreak*1   , csrch*1
      character f_1004*41  , f_1005*29
      character cstr_top*19, cstr_tail*99
c-----------------------------------------------------------------------
      logical   lmet_f     , lmet_i     , lcstrg_f   , lcstrg_i
      logical   lstan      , leiss      , lbndl      , lprnt     , 
     &          lcheck
c-----------------------------------------------------------------------
      integer   ia_f(ndlev)
      integer   isa_f(ndlev)    , ila_f(ndlev)
      integer   ia_i(ndlev)
      integer   isa_i(ndlev)    , ila_i(ndlev)
      integer   ifirst(1)       , ilast(1)
      integer   nte_ion(ndmet)  , nte_exc(ndmet)     , nf_a(ndmet)
      integer   indf_a(ndmet,ndlev)
      integer   imeta_i(ndmet)  , imeta_f(ndmet)
c-----------------------------------------------------------------------
      real*8    xja_f(ndlev)    , wa_f(ndlev)
      real*8    xja_i(ndlev)    , wa_i(ndlev)
      real*8    tea_ion(ndmet,ndtem)  , qred_ion(ndmet,ndlev,ndtem)
      real*8    tea_exc(ndmet,ndtem)  , qred_exc(ndmet,ndlev,ndtem)
      real*8    yld_a(ndmet,ndlev,ndlev)
      real*8    duma(idlev)
c-----------------------------------------------------------------------
      character code_f(ndlev)*1 , cstrga_f(ndlev)*(*)
      character code_i(ndlev)*1 , cstrga_i(ndlev)*(*)
      character ckey(3)*6       , cans(3)*4
      character ctext(ndtext)*80
c----------------------------------------------------------------------
      logical   l_ion(ndmet)     , l_aug(ndmet)      , l_exc(ndmet)
      logical   lqred_ion(ndmet,ndlev)
      logical   lqred_exc(ndmet,ndlev)
      logical   lyld_a(ndmet,ndlev)
c----------------------------------------------------------------------
      data      ckey/'seq','nucchg','type'/
      data      cbreak,csrch/'/',')'/
c----------------------------------------------------------------------
c
c----------------------------------------------------------------------
c define input parameters
c----------------------------------------------------------------------

c----------------------------------------------------------------------
c read adf23 structure information
c----------------------------------------------------------------------

      read(iunit,'(1a80)') cline
      call xxcase (cline , c80, 'lc')
      ind1 = index(c80,'adf')
      ind2 = index(c80,'adf23')

      if ((ind1 .gt.0) .and. (ind2.eq.0)) then
          write(i4unit(-1),1000) ' wrong adf format'
          write(i4unit(-1),1001)
         stop
      endif
      if (ind1.eq.0) then
          ind1=len(c80)
      else
          ind1=ind1-1
      endif

      call xxhkey(c80(1:ind1),ckey(1),cbreak,cans(1))
      call xxhkey(c80(1:ind1),ckey(2),cbreak,cans(2))
      call xxhkey(c80(1:ind1),ckey(3),cbreak,cans(3))

      call xxslen(cans(1),istart,istop)
      seq='  '
      if ((istop-istart).ge.2)then
          seq(1:istop-istart-1)=cans(1)(istart+1:istop-1)
      elseif ((istop-istart).eq.1.and.cans(1)(1:1).eq.'''') then
          seq(1:1) = cans(1)(2:2) 
      else
          write(i4unit(-1),1000) ' iso-sequence key/value missing'
          write(i4unit(-1),1001)
         stop
      endif

      call xxslen(cans(2),istart,istop)
      if (istart.ge.1)then
          read(cans(2)(istart:istop),*)iz0
      else
          write(i4unit(-1),1000) ' nuclear charge key/value missing'
          write(i4unit(-1),1001)
         stop
      endif

      call xxslen(cans(3),istart,istop)
      if ((istop-istart).eq.3)then
          ctype=cans(3)(istart+1:istop-1)
      else
          write(i4unit(-1),1000) ' adf23 file type key/value missing'
          write(i4unit(-1),1001)
         stop
      endif


c----------------------------------------------------------------------
c check if iz0 is in range
c----------------------------------------------------------------------

      if ((iz0.le.0).or.(iz0.gt.nsym)) then
         write(i4unit(-1),1000)'nuclear charge',iz0,' out of range'
         write(i4unit(-1),1001)
         stop
      endif

      esym=xfesym(iz0)
      iz=iz0-i4eiz0(seq)
      iz1=iz+1

c----------------------------------------------------------------------
c find and read final (ionised ion) level data
c----------------------------------------------------------------------

   10 read(iunit,'(1a80)')cline
      call xxcase (cline , c80, 'lc')
      if(index(c80,'bwnf').gt.0) then
          if(ctype.eq.'ca') then
              write(c10,'(a2,"+",i2," conf")')esym,iz1
          elseif(ctype.eq.'ls') then
              write(c10,'(a2,"+",i2," term")')esym,iz1
          elseif(ctype.eq.'ic') then
              write(c10,'(a2,"+",i2," levl")')esym,iz1
          endif
          
          if(index(c80,c10).le.0) then
              write(i4unit(-1),1002)'Faulty parent indexing line'
              write(i4unit(-1),1003)
          endif
          read(c80(36:52),*)bwno_f
          read(c80(59:61),*)nlvl_f
          if(((bwno_f.le.0.0d0).and.(iz1.ne.iz0)).or.
     &        (nlvl_f.le.0))then
              write(i4unit(-1),1000) 'Faulty parent indexing data'
              write(i4unit(-1),1001)
              stop
          endif
      else
          go to 10
      endif

      do i=1,4
        read(iunit,'(1a80)')cline
      enddo
c----------------------------------------------------------------------
c analyse formatting of first parent level line
c----------------------------------------------------------------------
      call xxcase (cline , c80, 'lc')
      call xxlast (c80, ')',indx_lb)
      indx_fb = index(c80,'(')

      iword = 1
      j     = 1
      call xxword( c80, ' '  , iword   , j          ,
     &             ifirst(1)  , ilast(1) , nwords
     &           )
      ilen_index = len(c80(1:ilast(1)))
      if((c80(ilast(1):ilast(1)).eq.'*').or.
     &   (c80(ilast(1):ilast(1)).eq.'#')) then
          ilen_index = ilen_index-1
      endif
      ilen_cnfg=indx_fb-ilen_index-2
      ilen_j=indx_lb-indx_fb-1
      ilen_wnf= lenstr(c80)-indx_lb
      if(ctype.ne.'ca') then
          ind1=index(c80(indx_fb+1:indx_lb-1),')')
          call xxlast(c80(indx_fb+1:indx_lb-1),'(',ind2)
          ilen_s= ind1-1
          ilen_l= ind2-ind1-1
          ilen_j= indx_lb-indx_fb-ind2-1
      endif

      if(ctype.ne.'ca') then
          f_1004=' '
          write(f_1004,'(a2,i1,a6,i2,a5,i1,a5,i1,a5,i1,a7,i2,a3)')
     &             '(i',ilen_index,',1a,1a',ilen_cnfg,',1x,i',ilen_s,
     &           ',1x,i', ilen_l,',1x,f',ilen_j,'.1,1x,f',ilen_wnf,'.1)'
      else
          f_1005=' '
          write(f_1005,'(a2,i1,a6,i2,a5,i1,a7,i2,a3)')
     &             '(i',ilen_index,',1a,1a',ilen_cnfg,',1x,f',
     &               ilen_j,'.1,1x,f',ilen_wnf,'.1)'
      endif

c----------------------------------------------------------------------
c backspace and read the full set of parent level lines
c----------------------------------------------------------------------
      backspace(iunit)

      nmet_f = 0
      do i=1,nlvl_f
        if(ctype.ne.'ca') then
            read(iunit,f_1004)ia_f(i) , code_f(i) , cstrga_f(i) ,
     &                        isa_f(i), ila_f(i)  , xja_f(i) , wa_f(i)

        else
            read(iunit,f_1005)ia_f(i) , code_f(i) , cstrga_f(i) ,
     &                        xja_f(i), wa_f(i)
            ila_f(i)=0
            isa_f(i)=0
        endif

        if(code_f(i).eq.'*') then
            nmet_f=nmet_f+1
            imeta_f(nmet_f)=i
        endif

      enddo

c----------------------------------------------------------------------
c find and read initial (ionising ion) level data
c----------------------------------------------------------------------

   20 read(iunit,'(1a80)')cline
      call xxcase (cline , c80, 'lc')
      if(index(c80,'bwni').gt.0) then
          if(ctype.eq.'ca') then
              write(c10,'(a2,"+",i2," conf")')esym,iz
          elseif(ctype.eq.'ls') then
              write(c10,'(a2,"+",i2," term")')esym,iz
          elseif(ctype.eq.'ic') then
              write(c10,'(a2,"+",i2," levl")')esym,iz
          endif
          if(index(c80,c10).le.0) then
              write(i4unit(-1),1002)'Faulty initial ion indexing line'
              write(i4unit(-1),1003)
          endif
          read(c80(36:52),*)bwno_i
          read(c80(59:61),*)nlvl_i
          if((bwno_i.le.0.0d0).or.(nlvl_i.le.0))then
              write(i4unit(-1),1000) 'Faulty initial ion indexing data'
              write(i4unit(-1),1001)
              stop
          endif
      else
          go to 20
      endif

      do i=1,3
        read(iunit,'(1a80)')cline
      enddo

c----------------------------------------------------------------------
c  read the full set of initial ion level lines
c----------------------------------------------------------------------
      nmet_i=0
      do i=1,nlvl_i
        if(ctype.ne.'ca') then
            read(iunit,f_1004)ia_i(i) , code_i(i) , cstrga_i(i) ,
     &                        isa_i(i), ila_i(i)  , xja_i(i) , wa_i(i)

        else
            read(iunit,f_1005)ia_i(i) , code_i(i) , cstrga_i(i) ,
     &                        xja_i(i), wa_i(i)
            ila_i(i)=0
            isa_i(i)=0
        endif

        if(code_i(i).eq.'*') then
            nmet_i=nmet_i+1
            imeta_i(nmet_i)=i
        endif

      enddo

c----------------------------------------------------------------------
c  check if metastables marked - otherwise mark just the ground levels
c----------------------------------------------------------------------
       lmet_i = .false.
       do i=1,nlvl_i
         if(code_i(i).eq.'*') lmet_i=.true.
       enddo
       if (.not.lmet_i) then
          nmet_i=1
          imeta_i(1)=1
          code_i(1)='*'
       endif

       lmet_f = .false.
       do i=1,nlvl_f
         if(code_f(i).eq.'*') lmet_f=.true.
       enddo
       if (.not.lmet_f) then
          nmet_f=1
          imeta_f(1)=1
          code_f(1)='*'
       endif

c----------------------------------------------------------------------
c  check that configuration lines are standard and core is unambiguous
c----------------------------------------------------------------------

       lcstrg_i = .true.
       do i=1,nlvl_i
         call xxdtes( cstrga_i(i)    , leiss     , lstan  , 
     &                lbndl          , lprnt     ,
     &                cstr_top       , cstr_tail ,
     &                nvlce          , lvlce  )
         if ((.not.lstan).and.leiss) then
             c22=' '
             ilen=len(cstrga_i(i))
             c22(1:ilen) = cstrga_i(i)
             call xxcftr(3, c22(1:ilen) , cstrga_i(i))
         elseif ((.not.leiss).and.(.not.lstan)) then
             lcstrg_i = .false.
             write(i4unit(-1),1002)'Unreadable ionising config = ',i
             write(i4unit(-1),1003)
         endif
       enddo

       lcstrg_f = .true.
       do i=1,nlvl_f
         call xxdtes( cstrga_f(i)    , leiss     , lstan  , 
     &                lbndl          , lprnt     ,
     &                cstr_top       , cstr_tail ,
     &                nvlce          , lvlce  )
         if ((.not.lstan).and.leiss) then
             c22=' '
             call xxslen(cstrga_f(i), istart , istop )
             ilen=istop-istart+1
             c22(1:ilen) = cstrga_f(i)(istart:istop)
             call xxcftr(3, c22(1:ilen) , cstrga_f(i))
         elseif ((.not.leiss).and.(.not.lstan)) then
             lcstrg_f = .false.
             write(i4unit(-1),1002)'Unreadable ionised config = ',i
             write(i4unit(-1),1003)
         endif
       enddo

c----------------------------------------------------------------------
c  find the initial metastable transition data blocks
c----------------------------------------------------------------------
       do i=1,nmet_i
         l_ion(i)=.false.
         l_aug(i)=.false.
         l_exc(i)=.false.
       enddo
       imet_count = 0
   50  read(iunit,'(1a80)',end=999)cline
       call xxcase(cline,c80,'lc')
       if(index(c80,'meti*').gt.0) then
           call xxslen(c80,istart,istop)
           istart=index(c80,'=')+1
           if((istart.eq.0).or.(istart.gt.istop))then
               write(i4unit(-1),1000)'meti* block marker faulty'
               write(i4unit(-1),1000)
               stop
           else
               imet_count = imet_count+1
               read(c80(istart:istop),*)imet_i
               if(imeta_i(imet_count).ne.imet_i)then
                   write(i4unit(-1),1000)'meti* index out of sequence'
                   write(i4unit(-1),1001)
                   stop
               endif
               if(code_i(imet_i).ne.'*')then
                   write(i4unit(-1),1000)'meti* index', imet_i,
     &                                   ' inconsistent'
                   write(i4unit(-1),1001)
                   stop
               endif
c----------------------------------------------------------------------
c  get the direct ionisation data
c----------------------------------------------------------------------
   60         read(iunit,'(1a256)')clong
              call xxcase(clong,c256,'lc')
              if (index(c256,'meti*').gt.0) then
                  backspace(iunit)
                  go to 50
              elseif (index(c256,'yields').gt.0) then
                  backspace(iunit)
                  go to 70
              elseif (index(c256,'excit rates').gt.0.or. 
     &                index(c256,'excit cross-sections').gt.0) then
                  backspace(iunit)
                  go to 80
              elseif (index(c256,'c-------').gt.0)then
                  go to 999
              elseif (index(c256,'ionis rates').gt.0.or.
     &                index(c256,'ionis cross-sections').gt.0) then
   62             read(iunit,'(1a256)')clong
                  call xxcase(clong,c256,'lc')
                  if (index(c256,'indf').le.0) go to 62

                  call xxslen(c256,istart,istop)
                  istart=index(c256,'=')
                  c256(istart:istart)=' '
                  nte=(istop-istart+1)/10
                  nte_ion(imet_count)=nte
                  read(c256(istart:istop),*)
     &                             (tea_ion(imet_count,it),it=1,nte)
                  read(iunit,'(1a256)')c256
                  do i=1,nlvl_f
                    lqred_ion(imet_count,i)=.false.
                    do it=1,nte
                      qred_ion(imet_count,i,it)=0.0d0
                    enddo
                  enddo
                  do i=1,nlvl_f
                    read(iunit,'(i5,18x,22d10.2)')iprf,
     &                         (duma(it),it=1,nte)
                    if(iprf.gt.0) then
                        lqred_ion(imet_count,iprf)=.true.
                        l_ion(imet_count)=.true.
                        do it=1,nte
                          qred_ion(imet_count,iprf,it)=duma(it)
                        enddo
                    else
                        go to 70
                    endif
                  enddo
              else
                  go to 60
              endif
c----------------------------------------------------------------------
c  get the Auger yields
c----------------------------------------------------------------------
   70         read(iunit,'(1a256)')clong
              call xxcase(clong,c256,'lc')
              if (index(c256,'meti*').gt.0) then
                  backspace(iunit)
                  go to 50
              elseif (index(c256,'excit rates').gt.0.or.
     &                index(c256,'excit cross-sections').gt.0) then
                  l_aug(imet_count)=.false.
                  backspace(iunit)
                  go to 80
              elseif (index(c256,'c-------').gt.0)then
                  go to 999
              elseif (index(c256,'yields').gt.0) then
                  istart=index(c256,esym//'+')
                  if(istart.le.0) then
                      write(i4unit(-1),1000)
     &                    'ion name missing for Auger yield block'
                      write(i4unit(-1),1001)
                      stop
                  endif
                  read(c256(istart+3:istart+4),'(i2)')izf
   72             read(iunit,'(1a256)')c256
                  if(index(c256,'indi').le.0) go to 72
                  call xxslen(c256,istart,istop)
                  istart=24
                  nf_a(imet_count)=(istop-istart+1)/10
                  c256(24:25)='  '
                  read(c256(istart:istop),'(22i10)')
     &                  (indf_a(imet_count,i),i=1,nf_a(imet_count))
                  read(iunit,'(1a256)')c256
                  do i=1,nlvl_i
                    lyld_a(imet_count,i)=.false.
                    do j=1,nf_a(imet_count)
                      yld_a(imet_count,i,j)=0.0d0
                    enddo
                  enddo
                  do i=1,nlvl_i
                    read(iunit,'(i5,18x,22d10.2)')indi,
     &                     (duma(j),j=1,nf_a(imet_count))
                    if(indi.gt.0) then
                        lyld_a(imet_count,indi)=.true.
                        l_aug(imet_count)=.true.
                        do j=1,nf_a(imet_count)
                          yld_a(imet_count,indi,j)=duma(j)
                        enddo
                    else
                        go to 80
                    endif
                  enddo
              else
                  go to 70
              endif
c----------------------------------------------------------------------
c  get the excitation rates
c----------------------------------------------------------------------
   80         read(iunit,'(1a256)')clong
              call xxcase(clong,c256,'lc')
              if (index(c256,'meti*').gt.0) then
                  backspace(iunit)
                  go to 50
              elseif (index(c256,'c-------').gt.0)then
                  l_exc(imet_count)=.false.
                  go to 999
              elseif (index(c256,'excit rates').gt.0.or.
     &                index(c256,'excit cross-sections').gt.0) then
   82             read(iunit,'(1a256)')clong
                  call xxcase(clong,c256,'lc')
                  if(index(c256,'indi').le.0) go to 82

                  call xxslen(c256,istart,istop)
                  istart=index(c256,'=')
                  c256(istart:istart)=' '
                  nte=(istop-istart+1)/10
                  nte_exc(imet_count)=nte
                  read(c256(istart:istop),*)
     &                             (tea_exc(imet_count,it),it=1,nte)
                  read(iunit,'(1a256)')c256
                  do i=1,nlvl_i
                    lqred_exc(imet_count,i)=.false.
                    do it=1,nte
                      qred_exc(imet_count,i,it)=0.0d0
                    enddo
                  enddo
                  do i=1,nlvl_i
                    read(iunit,'(i5,18x,22d10.2)')ilvl,
     &                         (duma(it),it=1,nte)
                    if(ilvl.gt.0) then
                        lqred_exc(imet_count,ilvl)=.true.
                        l_exc(imet_count)=.true.
                        do it=1,nte
                          qred_exc(imet_count,ilvl,it)=duma(it)
                        enddo
                    else
                        backspace(iunit)
                        go to 50
                    endif
                  enddo
              else
                  go to 80
              endif

           endif
       elseif(index(c80,'c-------').gt.0) then
           go to 999
       else
           go to 50
       endif

c----------------------------------------------------------------------
c  final check for valid data
c----------------------------------------------------------------------

  999  continue

       if(imet_count.le.0) then
          write(i4unit(-1),1000)'no metastables'
          write(i4unit(-1),1001)
          stop
      else
          lcheck = .false.
          do i=1,imet_count
            if(l_ion(i).or.(l_exc(i).and.l_aug(i)))lcheck=.true.
          enddo
          if(.not.lcheck) then
              write(i4unit(-1),1000)'no valid coefficient data'
              write(i4unit(-1),1001)
              stop
          endif
      endif


c----------------------------------------------------------------------
c  return comments if present
c----------------------------------------------------------------------

      rewind iunit
      call xxcomm(iunit, ndtext, ctext, ntext)
c
c-----------------------------------------------------------------------
c
 1000 format(1x,31('*'),' xxdata_23 error ',30('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
 1002 format(1x,30('*'),' xxdata_23 warning ',29('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1003 format(/1x,29('*'),' program continues  ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
