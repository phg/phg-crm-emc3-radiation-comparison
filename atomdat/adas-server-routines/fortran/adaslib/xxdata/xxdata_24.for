       subroutine xxdata_24( iunit  , dsname ,
     &                       nstore , nedim  ,
     &                       esym   , iz0    ,
     &                       nbsel  , isela  ,
     &                       iz     , iz1    ,
     &                       cdonor , crecvr , cfstat , ctype,
     &                       alph0  ,
     &                       iea    ,
     &                       teea   , scx
     &                     )
       implicit none
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: xxdata_24 *******************
C
C  PURPOSE:  To  fetch  data  from  input charge exchange cross-section
C            data for  given donor and receiver ions.
C
C  CALLING PROGRAM: ADAS509/SSCX
C
C  DATA:    Up to 'nstore' sets (data-blocks) of data may be  read from
C           the file - each block forming a complete set of  cross-
C           section values for given collision energies.
C           Each data-block  is  analysed independently of any  other
C           data-block.
C
C           The units used in the data file are taken as follows:
C
C           Collision energies  : eV/amu
C           Cross-section       : cm**2
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*44) DSNAME   = FILENAME OF DATA SET BEING READ
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NEDIM    = MAX NUMBER OF COLLISION ENERGIES ALLOWED
C
C  OUTPUT: (C*2)  ESYM     = READ - RECEIVING ION - ELEMENT SYMBOL
C  OUTPUT: (I*4)  IZ0      = READ - RECEIVING ION - NUCLEAR CHARGE
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IZ       = READ - RECEIVED ION -  CHARGE
C  OUTPUT: (I*4)  IZ1      = READ - RECEIVING ION - CHARGE
C
C  OUTPUT: (C*9)  CDONOR() = READ - DONOR ION IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*9)  CFCODE() = READ - RECEIVER ION IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*10) CFSTAT() = READ - FINAL STATE SPECIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*2)  CTYPE()  = READ - CROSS-SECTION TYPE
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  ALPH0()  = READ - LOW ENERGY EXTRAPOLATION PARM.
C                            DIMENSION: DATA-BLOCK INDEX
C
C
C  OUTPUT: (I*4)  IEA()    = READ - NUMBER OF COLLISION ENERGIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TEEA(,)  = READ - COLLISION ENERGIES (UNITS: eV/AMU)
C                            1st DIMENSION: COLLISION ENERGY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  SCX(,)   =READ - FULL SET OF COLLISION CROSS-
C                                   SECTION VALUES (cm**2)
C                            1st DIMENSION: COLLISION ENERGY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: COLLISION ENERGY INDEX
C          (I*4)  NENUM    = NUMBER OF COLLISION ENERGIES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (R*8)  R8FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*3)  CKEY1    = 'FST'    - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY2    = 'TYPE'   - INPUT BLOCK HEADER KEY
C          (C*5)  CKEY3    = 'ALPH0'  - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL'  - INPUT BLOCK HEADER KEY
C          (C*10) C10      = GENERAL USE TEN BYTE CHARACTER STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERT CHARACTER STRING TO REAL*8
C
C
C-----------------------------------------------------------------------
C
C NOTES: Based on e9data.for (H P Summers, 06/06/96).
C
C
C VERSION  : 1.1
C DATE     : 27-03-2006
C MODIFIED : Martin O'Mullane
C              - First version
C
C-----------------------------------------------------------------------
      integer    i4eiz0                , i4fctn              , i4unit
      integer    iunit                 , nstore              ,
     &           nedim                 , iz0                 ,
     &           iz                    , iz1                 ,
     &           nbsel
      integer    iblk                  , itt                 ,
     &           nenum                 , iabt                ,
     &           ipos1                 , ipos2
C-----------------------------------------------------------------------
      real*8     r8fctn
C-----------------------------------------------------------------------
      logical    lbend
C-----------------------------------------------------------------------
      character  dsname*44             , esym*2
      character  cslash*1              , c2*2                ,
     &           ckey1*3               , ckey2*4             ,
     &           ckey3*5               , ckey4*4             ,
     &           c10*10                , c80*80
C-----------------------------------------------------------------------
      integer    isela(nstore)         , iea(nstore)
C-----------------------------------------------------------------------
      character  cdonor(nstore)*9      , crecvr(nstore)*9    ,
     &           cfstat(nstore)*10     , ctype(nstore)*2
C-----------------------------------------------------------------------
      real*8     alph0(nstore)
      real*8     teea(nedim,nstore)    , scx(nedim,nstore)
C-----------------------------------------------------------------------
      data       cslash / '/' /
      data       ckey1  / 'FST'    /   , ckey2  / 'TYPE'   /  ,
     &           ckey3  / 'ALPH0'  /   , ckey4  / 'ISEL'   /
C-----------------------------------------------------------------------
      lbend = .FALSE.
C-----------------------------------------------------------------------



C-----------------------------------------------------------------------
C Read in number of data-blocks present in input file.
C-----------------------------------------------------------------------

      read(iunit,1000) c80
      read(c80,* ) nbsel

      if (nbsel.gt.nstore) then
         write(i4unit(-1),2000) dsname , nstore , nbsel , nstore
         nbsel = nstore
      endif

C-----------------------------------------------------------------------
C Input data for each of the data-blocks
C-----------------------------------------------------------------------

      do iblk=1,nbsel

C-----------------------------------------------------------------------
C Input donor/receiver specs. and other information (check block exists)
C-----------------------------------------------------------------------

          if (.not.lbend) then

             read(iunit,1000)c80

             if(iblk.eq.1) then
                 ipos1 = index(c80,'/') + 1
                 ipos2 = ipos1          + 1
                 read(c80(ipos1:ipos2),1001) esym

                 iz0   = i4eiz0( esym )
                 c2    = c80(14:15)
                 iz1   = i4fctn( c2 , iabt )
                 iz    = iz1 -1

             endif

             if ( c80(1:2).ne.'c-'.or.c80(1:2).ne.'C-') then

                cdonor(iblk) = c80(1:9)
                crecvr(iblk )= c80( 11:19)
                ipos2        = index(c80(20:80),'/') + 19

                read(c80(20:ipos2),*) iea(iblk)

                call xxhkey( c80 , ckey1 , cslash , cfstat(iblk) )
                call xxhkey( c80 , ckey2 , cslash , ctype(iblk)  )
                call xxhkey( c80 , ckey3 , cslash , c10          )
                call xxhkey( c80 , ckey4 , cslash , c2           )

                isela(iblk) = i4fctn( c2  , iabt )
                alph0(iblk) = r8fctn( c10 , iabt )
                nenum       = iea(iblk)

                if (nenum.gt.nedim) then
                   write(i4unit(-1),2001) dsname , iblk  ,
     &                                   nedim   , nenum
                   stop
                endif

C-----------------------------------------------------------------------
C Input collision energies, cross-sections for block
C-----------------------------------------------------------------------

                 read(iunit,1002) ( teea(itt,iblk) , itt=1,nenum )

                 read(iunit,1002) ( scx(itt,iblk)  , itt=1,nenum )

             else

                 write(i4unit(-1),2002) dsname  , nbsel    ,
     &                                 iblk - 1 , iblk - 1
                 lbend = .true.
                 nbsel = iblk - 1

             endif

         endif

      end do

C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1001 FORMAT(A2)
 1002 FORMAT(6D10.3)
 2000 FORMAT(/1X,31('*'),' xxdata_24 MESSAGE ',31('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'NUMBER OF ELEMENT ION DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' xxdata_24 ERROR ',32('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT COLLISION ENERGIES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' xxdata_24 MESSAGE ',31('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'INCONSISTENCY IN THE NUMBER OF DATA-BLOCKS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C-----------------------------------------------------------------------

      return
      end
