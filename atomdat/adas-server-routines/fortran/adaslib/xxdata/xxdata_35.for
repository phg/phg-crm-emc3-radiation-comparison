CX  SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adaslib/xxdata/xxdata_35.for,v 1.1 2004/07/06 15:33:13 whitefor Exp $ Date $Date: 2004/07/06 15:33:13 $
CX
       subroutine xxdata_35( iunit   , 
     &                       ndedge  , ndpts  ,
     &                       iedge   , ipts   , 
     &                       edge    , energy , trans
     &                     )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: xxdata_35 ******************
C
C  PURPOSE:  To fetch data from an adf35 data set.
C
C  SUBROUTINE:
C
C  input : (i*4)  iunit    = unit to which input file is allocated
C  input : (i*4)  ndedge   = maximum number of energy edges
C  input : (i*4)  ndpts    = maximum number of energy points
C
C  output: (i*4)  iedge    = number of energy edges
C  output: (i*4)  ipts     = number of energy points
C  output: (r*8)  edge()   = energies of the edges (eV).
C  output: (r*8)  energy() = tabulated energies (eV).
C  output: (r*8)  trans()  = tabulated transmission.
C
C ROUTINES:
C          routine    source    brief description
C          -------------------------------------------------------------
C          i4unit     ADAS      fetch unit number for message output
C
C
C
C VERSION  : 1.1                          
C DATE     : 19-07-2003
C MODIFIED : Martin O'Mullane
C              - First version
C
C-----------------------------------------------------------------------
      integer   iunit        , ndedge         , ndpts        ,
     &          iedge        , ipts
      integer   i4unit       , i
C-----------------------------------------------------------------------
      character str*1
C-----------------------------------------------------------------------
      real*8    edge(ndedge) , energy(ndpts)  , trans(ndpts) 
C-----------------------------------------------------------------------


      read(iunit,*)ipts, iedge
      
      if (iedge.GT.ndedge) then
         write(i4unit(-1),1001) 'nedges = ',iedge,' .GT. ndedge'
         write(i4unit(-1),1002)
         stop
      endif
      if (ipts.GT.ndpts) then
         write(i4unit(-1),1001) 'npts = ',ipts,' .GT. ndpts'
         write(i4unit(-1),1002)
         stop
      endif
      
      
      read(iunit,*)str
      if (iedge.GT.0) read(iunit,*)(edge(i), i=1, iedge)
      read(iunit,*)str
      read(iunit,*)(energy(i), i=1, ipts)
      read(iunit,*)(trans(i), i=1, ipts)
      
C-----------------------------------------------------------------------

 1001 FORMAT(1X,31('*'),' xxdata_35 ERROR ',30('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I5,A)
 1002 format(/1x,29('*'),' PROGRAM TERMINATED ',29('*'))

C-----------------------------------------------------------------------


      end
