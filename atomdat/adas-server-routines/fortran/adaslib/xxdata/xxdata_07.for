       SUBROUTINE XXDATA_07( IUNIT  , DSNAME ,
     &                       NSTORE , NTDIM  ,
     &                       ESYM   , IZ0    ,
     &                       NBSEL  , ISELA  ,
     &                       IZ     , IZ1    ,
     &                       CICODE , CFCODE , CIION , CFION ,
     &                       BWNO   ,
     &                       ITA    ,
     &                       TETA   , SZD
     &                     )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: XXDATA_07 ******************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT ELECTRON IMPACT IONIZATION
C            RATE COEFFT FILES (ADF07)
C
C            (MEMBER STORED IN IONELEC.DATA - MEMBER PREFIX 'SZD#').
C
C  CALLING PROGRAM: ADAS502/SSZD
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH BLOCK FORMING A COMPLETE SET OF  IONIZATION
C           RATE COEFFICIENT VALUES FOR GIVEN TEMPERATURES.
C           EACH DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER
C           DATA-BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           TEMPERATURES        : EV
C           RATE COEFFT         : CM**3 SEC-1
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NTDIM    = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C
C  OUTPUT: (C*2)  ESYM     = READ - IONISING ION - ELEMENT SYMBOL
C  OUTPUT: (I*4)  IZ0      = READ - IONISING ION - NUCLEAR CHARGE
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IZ()     = READ - IONISING ION - INITIAL CHARGE
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (I*4)  IZ1()    = READ - IONISING ION - FINAL   CHARGE
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*2)  CICODE() = READ - INITIAL STATE METASTABLE INDEX
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*2)  CFCODE() = READ - FINAL   STATE METASTABLE INDEX
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CIION()  = READ - INITIAL ION (as <ESYM>+(IZ()> )
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CFION()  = READ - FINAL   ION (as <ESYM>+<IZ1()>)
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  BWNO()   = READ- EFFECTIVE IONIZATION POTENTIAL (CM-1)
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  ITA()    = READ - NUMBER OF ELECTRON TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TETA(,)  = READ - ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  SZD(,)    =READ - FULL SET OF ZERO DENSITY IONIZATION
C                                   RATE COEFFICIENT VALUES (cm**3/sec)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: ELECTRON TEMPERATURE INDEX
C          (I*4)  NTNUM    = NUMBER OF ELECTRON TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (R*8)  R8FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*4)  CKEY1    = 'I.P.'  - INPUT BLOCK HEADER KEY
C          (C*5)  CKEY2    = 'ICODE' - INPUT BLOCK HEADER KEY
C          (C*5)  CKEY3    = 'FCODE' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL'  - INPUT BLOCK HEADER KEY
C          (C*10) C10      = GENERAL USE TEN BYTE CHARACTER STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      REAL*8 FUNCTION       -
C                               CONVERT CHARACTER STRING TO REAL*8
C          XXCASE     ADAS      SWITCHES CASE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    07/06/91
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  10/11/94 - L. JALOTA - MODIFIED TO RUN UNDER UNIX.
C
C-----------------------------------------------------------------------
C
C NOTES: Copied from e2data.for. This is v1.1 of xxdata_07.
C 
C VERSION  : 1.1                          
C DATE     : 26-03-2008
C MODIFIED : Allan Whiteford
C              - First version
C
C VERSION  : 1.2                         
C DATE     : 21-05-2008
C MODIFIED : Martin O'Mullane
C              - Permit lower case datasets for 2008 onwards data.
C              - Extract iz0 from element symbol in first block.
C
C VERSION  : 1.3                         
C DATE     : 10-08-2018
C MODIFIED : Martin O'Mullane
C              - Use acquired length for dsname.
C
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0                , I4FCTN              , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NTDIM                 , IZ0                 ,
     &           NBSEL
      INTEGER    IBLK                  , ITT                 ,
     &           NTNUM                 , IABT                ,
     &           IPOS1                 , IPOS2
C-----------------------------------------------------------------------
      REAL*8     R8FCTN
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*(*)            , ESYM*2
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*4               , CKEY2*5             ,
     &           CKEY3*5               , CKEY4*4             ,
     &           C10*10                , C80*80              ,
     &           c80uc*80              , blanks*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         , ITA(NSTORE)         ,
     &           IZ(NSTORE)            , IZ1(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CICODE(NSTORE)*2      , CFCODE(NSTORE)*2     ,
     &           CIION(NSTORE)*5       , CFION(NSTORE)*5
C-----------------------------------------------------------------------
      REAL*8     BWNO(NSTORE)          ,
     &           TETA(NTDIM,NSTORE)    , SZD(NTDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4
C-----------------------------------------------------------------------
      DATA       CSLASH / '/' /
      DATA       CKEY1  / 'I.P.'   /   , CKEY2  / 'ICODE'  /  ,
     &           CKEY3  / 'FCODE'  /   , CKEY4  / 'ISEL'   /
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------

      lbend  = .false.
      blanks = '                    ' //
     &         '                    ' //
     &         '                    ' //
     &         '                    '

      read(iunit,1000) c80
      read(c80, *) nbsel

      if (nbsel.gt.nstore) then
         write(i4unit(-1),2000) dsname , nstore , nbsel , nstore
         nbsel = nstore
      endif
      if (nbsel.lt.1) then
         write(i4unit(-1),2003)
         stop
      endif
      
      read(iunit,1000) c80
      ipos1 = 1
      ipos2 = index(c80,'+')
      read(c80(ipos1:ipos2),1001) esym

      iz0 = i4eiz0( esym )
      
      backspace iunit

C-----------------------------------------------------------------------
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C-----------------------------------------------------------------------

         DO 1 IBLK=1,NBSEL
         
C-----------------------------------------------------------------------
C INPUT TITLE, WAVELENGTH AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------

            IF (.NOT.LBEND) THEN
               
               READ(IUNIT,1000)C80

               c80uc = blanks
               call xxcase(c80, c80uc, 'UC')

                  IF ( c80uc(1:2).NE.'C-') THEN

                     CIION(IBLK) = c80uc(1:5)
                     C2          = c80uc(4:5)
                     IZ(IBLK)    = I4FCTN( C2 , IABT )

                     CFION(IBLK) = c80uc( 7:11)
                     C2          = c80uc(10:11)
                     IZ1(IBLK)   = I4FCTN( C2 , IABT )

                     IPOS2       = INDEX(c80uc(13:80),'/') + 12

                     READ(c80uc(13:IPOS2),*) ITA(IBLK)

                     CALL XXHKEY(c80uc , CKEY1 , CSLASH , C10         )
                     CALL XXHKEY(c80uc , CKEY2 , CSLASH , CICODE(IBLK))
                     CALL XXHKEY(c80uc , CKEY3 , CSLASH , CFCODE(IBLK))
                     CALL XXHKEY(c80uc , CKEY4 , CSLASH , C2          )

                     ISELA(IBLK) = I4FCTN(C2  , IABT)
                     BWNO(IBLK)  = R8FCTN(C10 , IABT)
                     NTNUM       = ITA(IBLK)

                     IF (NTNUM.GT.NTDIM) THEN
                        WRITE(I4UNIT(-1),2001) DSNAME , IBLK  ,
     &                                        NTDIM   , NTNUM
                        STOP
                     ENDIF

C-----------------------------------------------------------------------
C INPUT TEMPERATURE AND IONIZATION RATE COEFFTS. FOR BLOCK
C-----------------------------------------------------------------------

                     READ(IUNIT,1002) ( TETA(ITT,IBLK) , ITT=1,NTNUM )
                     READ(IUNIT,1002) ( SZD(ITT,IBLK)  , ITT=1,NTNUM )

                  ELSE
                  
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1

                  ENDIF

            ENDIF

    1    CONTINUE

C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1001 FORMAT(A2)
 1002 FORMAT(6D10.2)

 2000 FORMAT(/1X,29('*'),' XXDATA_07 MESSAGE ',30('*')/
     &        2X,'INPUT RATE COEFFT. DATA SET NAME: ',A/
     &        2X,'NUMBER OF ELEMENT ION DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,29('*'),' XXDATA_07 ERROR ',30('*')/
     &        2X,'INPUT RATE COEEFT. DATA SET NAME: ',A/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT ELECTRON TEMPERATRES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,29('*'),' XXDATA_07 MESSAGE ',30('*')/
     &        2X,'INPUT RATE COEFFT. DATA SET NAME: ',A/
     &        2X,'INCONSISTENCY IN THE NUMBER OF DATA-BLOCKS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2003 FORMAT(/1X,29('*'),' XXDATA_07 MESSAGE ',30('*')/
     &        2X,'INPUT RATE COEFFT. DATA SET NAME: ',A/
     &        2X,'NO DATA-BLOCKS PRESENT',
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C-----------------------------------------------------------------------

      RETURN
      END
