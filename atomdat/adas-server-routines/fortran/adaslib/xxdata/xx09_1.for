      subroutine xx09_1( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                   ndlev    , ndaug    , ndt      ,
     &                   iform    ,
     &                   nprnt    , nprnti   , nprntf   , bwnp     ,
     &                   ipa      , cstrpa   , ispa     , ilpa     ,
     &                   xjpa     , wpa      ,
     &                   nlvl     , bwnr     ,
     &                   ia       , ip       , cstrga   , isa      ,
     &                   ila      , xja      , wa       ,
     &                   auga_res , lauga_res,
     &                   nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                   irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                   lauga_nl ,
     &                   nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                   irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                   iprti    ,
     &                   diel_res , ldiel_res,
     &                   iprtf    ,
     &                   nsysf    , isys     , ispsys   , cspsys   ,
     &                   diel_n   , ldiel_n  ,
     &                   diel_sum , ldiel_sum, diel_tot ,
     &                   nte      , tea
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ******************* fortran77 subroutine: xx09_1 ********************
c
c  purpose:  Helper routine to read Badnell LS-only adf09
c            files as defined in year 1993; these correspond to 
c            iform = 1. All variables are documented in 
c            xxdata_09.for.
c
c  calling program:  xxdata_09
c
c  version  : 1.1
c  date     : 08-04-2016
c  modified : Martin O'Mullane
c               - Split from original xxdata_09.for.
c               - Redundant variables and code paths removed.
c
c-----------------------------------------------------------------------
      integer   ndword
c-----------------------------------------------------------------------
      parameter(ndword = 80 )
c-----------------------------------------------------------------------
      integer   i4unit      , lenstr
      integer   iunit       , ndprt          , ndrep      , ndlrep     ,
     &          ndlev       , ndaug          , ndt
      integer   nprnt       , nprnti         , nprntf     , inrep      ,
     &          nlvl        , nrep           , iaprs_n    , nvals      ,
     &          nlrep       , ilrep          , ipoint     , iaprs_nl   ,
     &          nte
      integer   indx        , indx1          , indx2
      integer   iline       , i              , j          ,
     &          ipi         , ipf            , ipfs       ,
     &          iword       , nwords         ,
     &          iform       , jmin           , irep
      integer   ind1        , ind2           , istart     , istop
      integer   indx_fb     , indx_lb        , ilen_index ,
     &          ilen_cnfg   , ilen_wnf       , ilen_supple,
     &          ilen_s      , ilen_l         , ilen_j
      integer   ilen_f_1004 , ilen_f_1006
c-----------------------------------------------------------------------
      integer   ipa(ndprt)  , ispa(ndprt)    , ilpa(ndprt)
      integer   ia(ndlev)   , isa(ndlev)     , ila(ndlev)
      integer   ip(ndlev)
      integer   irepa_n(ndrep), nrepa(ndrep)   , ipaug_n(ndaug,2)
      integer   iprti(ndprt)
      integer   iprtf(ndprt,ndprt)
      integer   nsysf(ndprt,ndprt)
      integer   isys(ndprt,ndprt,2), ispsys(ndprt,ndprt,2)
      integer   irepa_nl(ndlrep)   , nlrepa_n(ndlrep)     ,
     &          nlrepa_l(ndlrep)   , ipaug_nl(ndaug,2)
      integer   ifirst(ndword)     , ilast(ndword)
c-----------------------------------------------------------------------
      real*8    temp        , bwnp           , bwnr
c-----------------------------------------------------------------------
      real*8    xjpa(ndprt) , wpa(ndprt)
      real*8    xja(ndlev)  , wa(ndlev)
      real*8    tea(ndt)
      real*8    auga_nl(ndlrep,ndaug) , auga_n(ndrep,ndaug)
      real*8    diel_res(ndlev,ndprt,ndt)
      real*8    diel_n(ndrep,ndprt,ndprt,2,ndt)
      real*8    diel_sum(ndprt,ndprt,2,ndt)
      real*8    diel_tot(ndprt,ndt)
      real*8    auga_res(ndlev,ndprt)
c-----------------------------------------------------------------------
      character cdelim*7    , c10*10         , c30*30
      character c80*80      , c128*128       , c0*1     ,
     &          cline*80    , buffer*128     , c1*1     , c512*712
      character f_1004*45   , f_1006*45
c-----------------------------------------------------------------------
      character cstrpa(ndprt)*(*) , cstrga(ndlev)*(*)
      character codepa(ndprt)*1
      character caprs_n(ndaug)*10
      character caprs_nl(ndaug)*10
      character cprti(ndprt)*10
      character cspsys(ndprt,ndprt,2)*30
c-----------------------------------------------------------------------
      logical   ldata       , lnopi
      logical   l_supple    , l_aatp         , l_nrep     , l_nlrep
      logical   ldr_res     , ldr_n
c-----------------------------------------------------------------------
      logical   lauga_res(ndlev,ndprt)
      logical   lauga_n(ndrep,ndaug)
      logical   lauga_nl(ndlrep,ndaug)
      logical   l_prt(ndprt,ndprt)
      logical   ldiel_res(ndlev,ndprt)
      logical   ldiel_n(ndrep,ndprt,ndprt,2)
      logical   ldiel_sum(ndprt,ndprt,2)
c-----------------------------------------------------------------------
      data      cdelim / ' ()<>{}' /
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c locate and read recombining ion (parent) level data
c-----------------------------------------------------------------------

   20 read(iunit,'(a)') c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'bwnp')
      if (indx.le.0) go to 20

      indx=index(buffer,'bwnp=')
      read(buffer(indx+5:indx+16),'(f12.1)') bwnp
      indx=index(buffer,'nprnti=')
      read(buffer(indx+7:indx+8),'(i2)') nprnti
      indx=index(buffer,'nprntf=')
      read(buffer(indx+7:indx+8),'(i2)') nprntf
      read(iunit,'(a)')cline,cline,cline

      ldata=.true.

c----------------------------------------------------------------------
c analyse formatting of first parent level line
c----------------------------------------------------------------------

      l_supple=.false.
      read(iunit,'(a)')cline
      call xxcase (cline , c80, 'lc')
      call xxlast (c80, ')',indx_lb)
      indx_fb = index(c80,'(')

      j     = 1
      iword = ndword
      call xxword( c80, ' '   , j        , iword        ,
     &             ifirst     , ilast    , nwords
     &           )
      ilen_index = len(c80(1:ilast(1)))
      if((c80(ilast(1):ilast(1)).eq.'*').or.
     &   (c80(ilast(1):ilast(1)).eq.'#')) then
          ilen_index  = ilen_index-1
          ilen_supple = 5
          if(c80(ilen_index+2:ilen_index+ilen_supple+1).eq.'     ')then
               l_supple= .true.

          endif
      else
          ilen_supple = 6
          if(c80(ilen_index+1:ilen_index+ilen_supple).eq.'      ')then
               l_supple= .true.
          endif
      endif

      ilen_cnfg=indx_fb-ilen_index-2
      ilen_j=indx_lb-indx_fb-1
      ilen_wnf= lenstr(c80)-indx_lb
      ind1=index(c80(indx_fb+1:indx_lb-1),')')
      call xxlast(c80(indx_fb+1:indx_lb-1),'(',ind2)
      ilen_s= ind1-1
      ilen_l= ind2-ind1-1
      ilen_j= indx_lb-indx_fb-ind2-1

      f_1004=' '
      write(f_1004,'(a2,i1,a7,i2,a5,i1,a5,i1,a5,i1,a7,i2,a6)')
     &         '(i',ilen_index,',1a1,1a',ilen_cnfg,',1x,i',ilen_s,
     &      ',1x,i', ilen_l,',1x,f',ilen_j,'.1,1x,f',ilen_wnf,
     &       '.1,a1)'
      ilen_f_1004  = ilen_index+ilen_cnfg+ilen_s+ilen_l+ilen_j+
     &               ilen_wnf+5
      if (l_supple) then
           f_1006=' '
           write(f_1006,'(a2,i1,a7,i2,a5,i1,a5,i1,a5,i1,a7,i2,a6)')
     &           '(i',ilen_index,',1a1,1a',ilen_cnfg,',1x,i',ilen_s,
     &           ',1x,a', ilen_l,',1x,f',ilen_j,'.1,1x,f',ilen_wnf,
     &           '.1,a1)'
           write(f_1006(6:7),'(1a1,i1)')'i',ilen_supple
           write(f_1006(11:12),'(i2)')ilen_cnfg-ilen_supple+1
           ilen_f_1006  = ilen_index+ilen_cnfg+ilen_s+ilen_l+
     &                    ilen_j+ilen_wnf+5
      else
          f_1006 = f_1004
          ilen_f_1006 = ilen_f_1004
      endif

c----------------------------------------------------------------------
c backspace and read the full set of parent level lines
c----------------------------------------------------------------------

      backspace(iunit)

      do i=1,ndprt

         if (ldata) then
            read (iunit,'(a80)') cline
            read (cline,'(i6)')  iline

c  parent level input information is terminated by iline= 0 or -1

            if (iline.le.0) then

               ldata=.false.
               nprnt=i-1

c  parent level index 'iline' should equal 'i'

            elseif (iline.ne.i) then
               write(i4unit(-1),1001) 'parent level index',iline,
     &                       ' out of order'
               write(i4unit(-1),1002)
               stop
            else
              if (iline.LE.ndprt) then

                 read(cline,f_1004)ipa(i)    , codepa(i) ,           
     &                             cstrpa(i) , ispa(i)   ,           
     &                             ilpa(i)   , xjpa(i)   , wpa(i)    


              endif
            endif
         endif
      enddo

c check if there are more than ndprt parents (in do loop) and restrict
c the data returned to ndprt.

      if (ldata) then
         read (iunit,'(i6)') i
         if (i.gt.0) then
            write(i4unit(-1),1008)
     &                    'adf09 data set contains > ',ndprt,
     &                    ' parent levels'
            write(i4unit(-1),'(A)')' '
            write(i4unit(-1),'(i5,1x, A)')ndprt, ' parents returned'
            write(i4unit(-1),1010)
            nprnt = ndprt
            nprntf = ndprt
         else
            nprnt = ndprt
         endif
      endif

c-----------------------------------------------------------------------
c locate and read recombined ion level and Auger data
c-----------------------------------------------------------------------

   30 read(iunit,'(a)') c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'bwnr')
      if(indx.le.0) go to 30

      indx=index(buffer,'bwnr=')
      read(buffer(indx+5:indx+16),'(f12.1)') bwnr

      indx=index(buffer,'ntrm=')
      read(buffer(indx+5:indx+9),*) nlvl
      read(iunit,'(a)')cline,cline,cline

      l_aatp = .false.
      ldata=.true.

      do i=1,nlvl

         if (ldata) then
            read (iunit,'(a128)') c128
            read (c128,'(i6)')  iline
            if(l_supple) then
                read(c128,f_1006) ia(i)     , ip(i)    ,
     &                            cstrga(i) , isa(i)   ,
     &                            c0        , xja(i)   ,
     &                            wa(i)     , c1
                if (c0(1:1).EQ.'A') then
                   ila(i) = 10
                elseif (c0(1:1).EQ.' ') then
                   ila(i) = -1
                elseif (c0(1:1).EQ.'*') then
                   ila(i) = -1
                else
                   read(c0,*)ila(i)
                endif
                if (ip(i).GT.1.AND.c1.EQ.'*') then
                   j=1
                   iword = ndword
                   call xxword( c128(ilen_f_1006+1:128) ,
     &                           cdelim  , j      , iword,
     &                           ifirst  , ilast  , nwords )
                   if (nwords.gt.1) then
                      do indx=2,nwords
                        read(c128(ilen_f_1006+1+ifirst(indx):
     &                          ilen_f_1006+1+ilast(indx)),*)
     &                          auga_res(i,indx-1)
                        lauga_res(i,indx-1)=.true.
                      enddo
                   endif
                endif
                if (ip(i).GT.7.AND.c1.EQ.'*') then
                    read (iunit,'(a128)') c128
                    if (index(c128,'*').EQ.0.AND.i.LT.nlvl) then         ! adf48 do not have data on next line
                       read(c128,*)(auga_res(i,indx-1),
     &                               indx=nwords, ip(i)-1)
                       do indx=nwords,ip(i)-1
                          lauga_res(i,indx)=.true.
                       end do
                    else
                       backspace iunit
                    endif
                endif

            else
                read(c128,f_1004) ia(i)     , c1       ,
     &                            cstrga(i) , isa(i)   ,
     &                            ila(i)    , xja(i)   ,
     &                            wa(i)
                ip(i)=1
            endif

         endif

      enddo
c
      if (ldata) then
         read (iunit,1004) i
            if (i.gt.0) then
               write(i4unit(-1),1001)
     &                       'adf09 data set contains > ',ndlev,
     &                       ' energy levels'
               write(i4unit(-1),1002)
               stop
            else
c               nlvl = ndlev
            endif
      endif

c-----------------------------------------------------------------------
c locate and read representative nl-level and auger rate data
c-----------------------------------------------------------------------

   40 read(iunit,'(a)')c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'nlrep=')
      indx1=index(buffer,'nrep=')
      if((indx.le.0).and.(indx1.le.0)) then
          go to 40
      elseif(indx1.gt.0) then
          l_nlrep= .false.
          backspace (iunit)
          go to 50
      else
          l_nlrep = .true.

      endif

      indx=index(buffer,'nlrep=')
      read(buffer(indx+6:indx+16),'(i3)') nlrep
      read(iunit,'(a)')buffer


      c512 = ' '
      ipoint=1
  45  c128 = ' '
      read(iunit,'(1a128)')c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'=')
      indx2 = index(buffer,'   l')
      indx1= index(buffer,'--')
      if(buffer(1:16).eq.'     1     1   0') then
          backspace(iunit)
      else
          if ((indx.le.0).and.(indx2.le.0).and.(indx1.le.0))then
              c512(ipoint:ipoint+128)=buffer
              ipoint=ipoint+128
              go to 45
          elseif(indx.gt.0) then
              c512(ipoint:128-indx-1) = buffer(indx+1:128)
              ipoint=128-indx
              go to 45
          elseif(indx2.gt.0) then
              c512(ipoint:128-indx2-12) = buffer(indx2+13:128)
              ipoint=128-indx2+11
              go to 45
          else
              call xxslen(buffer(24:128),istart,istop)
              if(istart.gt.0)then
C No need to warn user
C                  write(i4unit(-1),1001)
C      &               "extra m-m' data on nl-shell separator line"
C                  write(i4unit(-1),1010)
                 c512(ipoint:ipoint+103)=buffer(24:128)
                 ipoint=ipoint+104
                 go to 45
              endif
          endif
      endif

      j=1
      iword=ndword
      call xxword(c512(1:ipoint),cdelim,j,iword, ifirst,
     &            ilast,iaprs_nl)
      do j=1,iaprs_nl
        caprs_nl(j)= c512(ifirst(j):ilast(j))
        indx=index(caprs_nl(j),'-')
        read(caprs_nl(j)(1:indx-1),*)ipaug_nl(j,1)
        read(caprs_nl(j)(indx+1:ilast(j)-ifirst(j)+1),*)ipaug_nl(j,2)
      enddo

      do ilrep=1,nlrep
        read(iunit,*)irepa_nl(ilrep),
     &       nlrepa_n(ilrep),nlrepa_l(ilrep),
     &      (auga_nl(ilrep,j),j=1,iaprs_nl)
c        write(i4unit(-1),*)'vals=',ilrep,irepa_nl(ilrep),
c     &       nlrepa_n(ilrep),nlrepa_l(ilrep),
c     &      (auga_nl(ilrep,j),j=1,iaprs_nl)

c        read(iunit,'(2i6,i4,5x,5(10e10.2))')irepa_nl(ilrep),
c     &       nlrepa_n(ilrep),nlrepa_l(ilrep),
c     &      (auga_nl(ilrep,j),j=1,iaprs_nl)
        do j=1,iaprs_nl
          if(auga_nl(ilrep,j).gt.0.0d0)lauga_nl(ilrep,j)=.true.
        enddo
      enddo

c-----------------------------------------------------------------------
c locate and read representative n-level and auger rate data
c-----------------------------------------------------------------------

   50 read(iunit,'(a)')c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'nrep=')
      indx1=index(buffer,'prti=')
      if((indx.le.0).and.(indx1.le.0)) then
          go to 50
      elseif(indx1.gt.0) then
          l_nrep= .false.
          backspace (iunit)
          backspace (iunit)
          backspace (iunit)
          go to 100
      else
          l_nrep = .true.
      endif

      indx=index(buffer,'nrep=')
      read(buffer(indx+5:indx+16),'(i3)') nrep
      read(iunit,'(a)')buffer

      c512 = ' '
      ipoint=1
  55  c128 = ' '
      read(iunit,'(1a128)')c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'=')
      indx2 = index(buffer,'   n')
      indx1= index(buffer,'--')
      if(buffer(1:12).eq.'     1     1')then
          backspace(iunit)
      else
          if ((indx.le.0).and.(indx2.le.0).and.(indx1.le.0))then
              c512(ipoint:ipoint+128)=buffer
              ipoint=ipoint+128
              go to 55
          elseif(indx.gt.0) then
              c512(ipoint:128-indx-1) = buffer(indx+1:128)
              ipoint=128-indx
              go to 55
          elseif(indx2.gt.0) then
              c512(ipoint:128-indx2-12) = buffer(indx2+13:128)
              ipoint=128-indx2-11
              go to 55
          else
              call xxslen(buffer(24:128),istart,istop)
              if(istart.gt.0)then
C No need to warn user
C                  write(i4unit(-1),1001)
C      &               "extra m-m' data on n-shell separator line"
C                  write(i4unit(-1),1010)
                 c512(ipoint:ipoint+103)=buffer(24:128)
                 ipoint=ipoint+104
                 go to 55
              endif
          endif
      endif

      j=1
      iword=ndword
      call xxword(c512(1:ipoint),cdelim,j,iword, ifirst,
     &            ilast,iaprs_n)
      do j=1,iaprs_n
        caprs_n(j)= c512(ifirst(j):ilast(j))
        indx=index(caprs_n(j),'-')
        read(caprs_n(j)(indx-1:indx-1),'(i1)')ipaug_n(j,1)
        read(caprs_n(j)(indx+1:indx+1),'(i1)')ipaug_n(j,2)
      enddo

      do inrep=1,nrep
        read(iunit,*)irepa_n(inrep),
     &       nrepa(inrep),
     &      (auga_n(inrep,j),j=1,iaprs_n)

        do j=1,iaprs_n
          if(auga_n(inrep,j).gt.0.0d0)lauga_n(inrep,j)=.true.
        enddo
      enddo

c-----------------------------------------------------------------------
c locate data block for each initial parent and get header information
c-----------------------------------------------------------------------

        do ipi=1,ndprt
          do ipf=1,ndprt
              l_prt(ipi,ipf)=.false.
           enddo
        enddo

  100   do 140 ipi=1,nprnti

   75   read(iunit,'(a)',end=999) c128
        call xxcase(c128,buffer,'lc')

        indx=index(buffer,'prti=')
        indx1 = index(buffer,'alft')
        indx2 =  index(buffer,'c----')

        if((indx.le.0).and.(indx1.le.0).and.(indx2.le.0)) then
            go to 75
        elseif(indx1.gt.0)then
            backspace(iunit)
            go to 150
        elseif(indx2.gt.0)then
            go to 200
        endif
c
        indx=index(buffer,'prti=')
        read(buffer(indx+5:indx+6),'(i2)') iprti(ipi)

        if((index(buffer,'lvlprt').gt.0).or.
     &     (index(buffer,'trmprt').gt.0).or.
     &     (index(buffer,'cfgprt').gt.0)) then
            c10=' '
            call xxlast (buffer, ')',indx_lb)
            indx_fb = index(buffer,'(')
            c10(1:indx_lb-indx_fb-1)=buffer(indx_fb+1:indx_lb-1)
            if(iform.eq.1) then
                write(c10(indx_lb-indx_fb+1:indx_lb-indx_fb+5),
     &                '(f4.1)')xjpa(ipi)
            endif
            call xxcase(c10,cprti(ipi),'uc')
        endif

c-----------------------------------------------------------------------
c locate temperature set
c-----------------------------------------------------------------------

  102   read(iunit,'(1a128)')c128
        call xxcase(c128,buffer,'lc')
        indx  = index(buffer,'indx te')
        indx1 = index(buffer,'prtf')
        if((indx.le.0).and.(indx1.le.0))then
            go to 102
        elseif(indx1.gt.0) then
            write(i4unit(-1),1001)'No temperature data for parent',ipi
            write(i4unit(-1),1002)
            stop
        else
            backspace(iunit)
        endif

        c512 = ' '
        ipoint=1
 103    c128 = ' '
        read(iunit,'(1a128)')c128
        call xxcase(c128 , buffer, 'lc')
        indx = index(buffer,'=')
        indx1= index(buffer,'--')
        if ((indx.le.0).and.(indx1.le.0))then
            c512(ipoint:ipoint+128)=buffer
            ipoint=ipoint+128
        elseif(indx.gt.0) then
            c512(ipoint:128-indx-1) = buffer(indx+1:128)
            ipoint=128-indx
            go to 103
        else
            call xxslen(buffer(12:128),istart,istop)
            if(istart.gt.0)then
               c512(ipoint:ipoint+103)=buffer(12:128)
               ipoint=ipoint+104
            endif
        endif

        j     = 1
        iword = ndword

        call xxword( c512      , cdelim    , j  ,
     &               iword     ,
     &               ifirst    , ilast     , nwords    )

        nte = iword
        if (nte.gt.ndt) then
            write(i4unit(-1),1001)
     &    'number of temperatures in adf09 file is greater than ndt'
            stop
        endif

        do j=1,nte
           read(c512(ifirst(j):ilast(j)),'(1e8.2)')tea(j)
        enddo

c-----------------------------------------------------------------------
c locate low-level resolved dielectronic coeffts. for initial parent
c-----------------------------------------------------------------------

        ldata=.true.

        do 80 i=1,ndlev
          if (ldata) then
              read (iunit,'(a128)') buffer
              read (buffer,'(i6)')  iline

c  diel. recom. input information is terminated by iline= 0 or -1

              if (iline.le.0) then
                  ldata=.false.
                  nvals=i-1
              else
                  backspace(iunit)
                  read (iunit,*) indx,
     &                               (diel_res(indx,ipi,j),j=1,nte)

                  ldiel_res(indx,ipi)=.true.
                  ldr_res = .true.
              endif
          endif
   80   continue

c-----------------------------------------------------------------------
c locate data block for each final parent
c-----------------------------------------------------------------------

        ipf   = 1
        lnopi = .true.
        do while (ipf.le.nprntf.and.lnopi)
c--------------------------------------------
c we will eventually run out of dataset -
c in this case end reading
c--------------------------------------------
   85     read(iunit,'(1a128)',end=999) c128
          call xxcase(c128,buffer,'lc')
          indx  = index(buffer,'prtf=')
          indx1 = index(buffer,'prti=')
c--------------------------------------------
c not all final parents are present in the adf09 dataset. if we read
c prti indicating a new block rewind one record and pass control back
c to the initial parent loop. additionally, if we detect the presence
c of tabulated totals (as present in 2000 style datasets) then jump to
c reading those.
c--------------------------------------------
          if (indx1.gt.0) then
             backspace(iunit)
             lnopi = .false.
             goto 130
          endif
          if(indx.le.0) then
              indx1 = index(buffer,'alft( 1)')
              if (indx1.gt.0) then
                  backspace(iunit)
                  goto 150
              endif
              go to 85
          endif

          indx=index(buffer,'prtf=')
          read(buffer(indx+5:indx+6),'(i2)') iprtf(ipi,ipf)

          if(ipf.lt.iprtf(ipi,ipf))then
              nsysf(ipi,ipf)=0
              ipf=ipf+1
              backspace(iunit)
              go to 85
          endif

          if((index(buffer,'lvlprt').gt.0).or.
     &       (index(buffer,'trmprt').gt.0).or.
     &       (index(buffer,'cfgprt').gt.0)) then
              c30=' '
              c10=' '
              call xxlast (buffer, ')',indx_lb)
              indx_fb = index(buffer,'(')
              c10(1:indx_lb-indx_fb-1)=buffer(indx_fb+1:indx_lb-1)
              if(iform.eq.1) then
                  write(c10(indx_lb-indx_fb+1:indx_lb-indx_fb+5),
     &                  '(f4.1)')xjpa(ipf)
              endif
              call xxcase(c10,c30(1:10),'uc')
          endif

          indx=index(buffer,'nsys=')
          read(buffer(indx+5:indx+6),'(i2)') nsysf(ipi,ipf)
c--------------------------------------------------
c      find and process each spin system
c--------------------------------------------------
          do ipfs=1,nsysf(ipi,ipf)
c
   93       read(iunit,'(1a128)') c128
            call xxcase(c128,buffer,'lc')
            indx=index(buffer,'spnsys')
            if(indx.le.0) then
                go to 93
            endif
c
            indx=index(buffer,'spnsys=')
            read(buffer(indx+7:indx+11),'(i2)') ispsys(ipi,ipf,ipfs)
            indx=index(buffer,'sys=')
            read(buffer(indx+4:indx+5),'(i2)')isys(ipi,ipf,ipfs)
            write(c30(11:20),'(1a8,1i2)')'  isys =',
     &                        isys(ipi,ipf,ipfs)
            write(c30(21:30),'(1a8,1i2)')'  ssys =',
     &                        ispsys(ipi,ipf,ipfs)
            cspsys(ipi,ipf,ipfs)=c30
  94        read(iunit,'(1a128)')c128
            call xxcase(c128,buffer,'lc')
            if((index(buffer,'irep').gt.0).or.
     &           (index(buffer,'inrep').gt.0)) then
                 read(iunit,'(1a128)')c128
  95             read(iunit,'(1a128)')c128
                 read(c128(1:6),'(i6)')indx
                 if(indx.gt.0) then
                     backspace(iunit)
                     jmin=1
                     read(iunit,'(i6,5x,10e10.2)')indx,
     &              (diel_n(indx,ipi,ipf,ipfs,j),j=jmin,min(10,nte))
                     ldiel_n(indx,ipi,ipf,ipfs)=.true.
                     ldr_n = .true.
                     jmin = jmin+10
                     do while (jmin.lt.nte)
                        read (iunit,'(11x,10e10.2)')
     &                       (diel_n(indx,ipi,ipf,ipfs,j),
     &                        j=jmin,min(jmin+9,nte))
                        jmin=jmin+10
                     enddo
                     go to 95
                 else
                     go to 94
                 endif
            endif

          enddo

c--------------------------------------------------
c calculate total dr rate now for each initial
c parent, final parent and spin system
c--------------------------------------------------
         if(ldr_n) then
            do ipfs=1,nsysf(ipi,ipf)
              call dasumd ( ndrep    , ndprt     , ndt    ,
     &                      ipi      , ipf       , ipfs   ,
     &                      nte      , nrep      , nrepa  , diel_n  ,
     &                      diel_sum
     &                    )
              do j=1,nte
                if(diel_sum(ipi,ipf,ipfs,j).gt.0.0d0) then
                    ldiel_sum(ipi,ipf,ipfs)=.true.
                endif
              enddo

            enddo
         endif


         ipf = ipf+1

        enddo

  130   continue

  140 continue


c------------------------------------------
c gather totals as stored at end of dataset
c------------------------------------------

  150 continue
      read(iunit,'(1a128)',end=999)c128
      call xxcase(c128,buffer,'lc')
      indx1 = index(buffer,'alft( 1)')
      if (indx1.le.0) goto 150
      read(iunit,'(a)') buffer
      do j=1,nte
          read(iunit,'(e10.2,1x,20e10.2)')
     &               temp,(diel_tot(i,j),i=1,nprnti)
          if (dabs(temp-tea(j))/temp.gt.1.0d-6) then
              write(i4unit(-1),1001) 'partial and total',iline,
     &                             ' te grid mismatch'
              write(i4unit(-1),1002)
              stop
          endif
      end do

c-----------------------------------------------------------------------
  200 continue
c-----------------------------------------------------------------------


c-----------------------------------------------------------------------

 1001 format(1x,31('*'),' xxdata_09 error ',30('*')//
     &       1x,'fault in input data file: ',a,i5,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1004 format(i5)
 1008 format(1x,30('*'),' xxdata_09 warning ',29('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1010 format(/1x,29('*'),' program continues  ',29('*'))

c-----------------------------------------------------------------------

  999 continue

      return
      end
