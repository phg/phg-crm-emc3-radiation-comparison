      subroutine xxdata_48( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                      ndlev    , ndt      ,
     &                      seq      , iz       , iz0      , iz1      ,
     &                      ctype    , iform    ,
     &                      nprnt    , nprnti   , nprntf   , bwnp     ,
     &                      ipa      , cstrpa   , ispa     , ilpa     ,
     &                      xjpa     , wpa      ,
     &                      nlvl     , bwnr     ,
     &                      ia       , ip       , cstrga   , isa      ,
     &                      ila      , xja      , wa       , lauga_res,
     &                      nlrep    , irepa_nl , nlrepa_n , nlrepa_l ,
     &                      nrep     , 
     &                      irepa_n  , nrepa    ,
     &                      iprti    ,
     &                      rrec_res , lrrec_res,
     &                      iprtf    ,
     &                      nsysf    , isys     , ispsys   , cspsys   ,
     &                      rrec_nl  , lrrec_nl , rrec_n   , lrrec_n  ,
     &                      rrec_sum , lrrec_sum, rrec_tot , lrrec_tot,
     &                      nte      , tea
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxdata_48 ******************
c
c  purpose:  to fetch data from input adf48 radiative recombination
c            data set.
c
c  calling program:  various
c
c
c  subroutine:
c
c  input : (i*4)  iunit   = unit to which input file is allocated
c
c  input : (i*4)  ndprt   = maximum number of parent states
c  input : (i*4)  ndrep   = max. number of representative n-shells
c  input : (i*4)  ndlrep  = max. number of representative nl-shells
c  input : (i*4)  ndlev   = maximum number of resolved levels
c  input : (i*4)  ndt     = max. number of electron temperatures
c
c  output: (c*2)  seq     = recombined ion seq
c  output: (i*4)  iz      = recombined ion charge
c  output: (i*4)  iz0     = nuclear charge
c  output: (i*4)  iz1     = recombining ion charge
c  output: (c*2)  ctype   = resolution type (ls/ic/ca)
c  output: (i*4)  iform   = data set organisational form
c                            1: LS 2005 Badnell data form
c                            2: IC 2005 Badnell data form
c                            3: unassigned
c                            4: hybrid LS 2015 Badnell data form
c                            5: hybrid IC 2015 Badnell data form
c                            6: hybrid CA 2015 Badnell data form
c  output: (i*4)  nprnt   = total number of parents
c  output: (i*4)  nprnti  = number of parents which are initial parents
c  output: (i*4)  nprntf  = number of parents which are final parents
c  output: (r*8)  bwnp    = binding wave no. of ground parent (cm-1)
c  output: (i*4)  ipa()   = parent energy level index number
c                           1st dim: parent lvl/trm/cfg index
c  output: (c*18) cstrpa()= nomencl./config. for parent level 'ipa()'
c                           1st dim: parent lvl/trm/cfg index
c  output: (i*4)  ispa()  = multiplicity for parent level 'ipa()'
c                           note: (ispa-1)/2 = quantum number (sp)
c                           1st dim: parent lvl/trm/cfg index
c  output: (i*4)  ilpa()  = quantum number (lp) for parent level 'ipa()'
c                           1st dim: parent lvl/trm/cfg index
c  output: (r*8)  xjpa()  = quantum number (jp) for parent level 'ipa()'
c                           note: (2*xjpa)+1 = statistical weight
c                           1st dim: parent lvl/trm/cfg index
c  output: (r*8)  wpa()   = energy relative to parent level 1 (cm-1)
c                           for parent level 'ipa()'
c                           1st dim: parent lvl/trm/cfg index
c  output: (i*4)  nlvl    = number of energy levels (lvl/trm/cfg) of
c                           recombined ion
c  output: (r*8)  bwnr    = ionisation potential (cm-1) of lowest level
c                           of recombined ion
c  output: (i*4)  ia()    = recombined ion energy level index number
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  ip()    = associated parent of recombined level
c                           1st.dim: lvl/trm/cfg index
c  output: (c*18) cstrga()= nomencl./config. for recombined ion level
c                           'ia()'
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  isa()   = multiplicity for recombined level 'ia()'
c                           note: (isa-1)/2 = quantum number (s)
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  ila()   = quantum number (l) for recombined level
c                           'ia()'
c                           1st.dim: lvl/trm/cfg index
c  output: (r*8)  xja()   = quantum number (j) for recombined level
c                           'ia()'
c                           note: (2*xja)+1 = statistical weight
c                           1st.dim: lvl/trm/cfg index
c  output: (r*8)  wa()    = energy relative to recombined level 1 (cm-1)
c                           for recombined level 'ia()'
c                           1st.dim: lvl/trm/cfg index
c  output: (l*4)  lauga_res() = .true. => lvl/trm/cfg may be Auger
c                                .false.=> below
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  nlrep      = number of representative nl-shells
c  output: (i*4)  irepa_nl() = representative nl-shell index number
c                              1st dim: representative nl-shell index
c  output: (i*4)  irepa_nl() = representative nl-shell index number
c                              1st dim: representative nl-shell index
c  output: (i*4)  nlrepa_n() = n value for representative nl-shell
c                              1st dim: representative nl-shell index
c  output: (i*4)  nlrepa_l() = l value for representative nl-shell
c                              1st dim: representative nl-shell index
c  output: (i*4)  nrep        = number of representative n-shells
c  output: (i*4)  irepa_n()   = representative n-shell index number
c                               1st dim: representative n-shell index
c  output: (i*4)  nrepa()     = n value for representative n-shell
c                               1st dim: representative n-shell index
c  output: (i*4)  iprti()     = initial parent index from block header
c                               1st dim: initial parent index number
c  output: (r*8)  rrec_res(,,)= lvl/trm/cfg resol. rad. rec. coeffts.(cm3 s-1)
c                               1st.dim: level index
c                               2nd.dim: initial parent index
c                               3rd.dim: temperature index
c  output: (l*4)  lrrec_res(,)= .true. => rad. rec. present for lvl/trm/cfg index
c                               .false.=> rad. rec. not present for
c                                          lvl/trm/cfg index
c                               1st.dim: level index
c                               2nd.dim: initial parent index
c  output: (i*4)  iprtf(,)    = final parent index from block header
c                                  1st dim: initial parent index number
c                                  2nd dim: final parent index number
c  output: (i*4)  nsysf(,)    = no. of spin systems built on final parent
c                               defaults to 1 for /ic/ca/
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c  output: (i*4)  isys(,,)    = n-shell spin system index for final parent
c                               defaults to 1 for /ic/ca/
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c                               3rd dim: spin system index number
c  output: (i*4)  ispsys(,,)  = n-shell spin system for final parent
c                               applicable to /ls/ only
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c                               3rd dim: spin system index number
c  output: (c*20) cspsys(,,)  = characterising string for final parent
c                               system - universal for /ls/ic/ca/
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c                               3rd dim: spin system index number
c  output: (r*8)  rrec_nl(,,,,) = nl-shell rad. rec. coeffts.(cm3 s-1)
c                                 1st.dim: repr. nl-shell index
c                                 2nd.dim: initial parent index
c                                 3rd.dim: final parent index
c                                 4th.dim: spin system index
c                                 5th.dim: temperature index
c  output: (r*8)  lrrec_nl(,,,)= .true. => rad. rec. present for repr. nl-shell
c                                .false.=> rad. rec. not present for  nl-shell
c                                1st.dim: repr. nl-shell index
c                                2nd.dim: initial parent index
c                                3rd.dim: final parent index
c                                4th.dim: spin system index
c  output: (r*8)  rrec_n(,,,,) = n-shell rad. rec. coeffts.(cm3 s-1)
c                                1st.dim: repr. n-shell index
c                                2nd.dim: initial parent index
c                                3rd.dim: final parent index
c                                4th.dim: spin system index
c                                5th.dim: temperature index
c  output: (r*8)  lrrec_n(,,,,)= .true. => rad. rec. present for repr. n-shell
c                                .false.=> rad. rec. not present for  n-shell
c                                1st.dim: repr. n-shell index
c                                2nd.dim: initial parent index
c                                3rd.dim: final parent index
c                                4th.dim: spin system index
c  output: (r*8)  rrec_sum(,,,) = sum of n-shell rad. rec. coeffts.(cm3 s-1)
c                                 1st.dim: initial parent index
c                                 2nd.dim: final parent index
c                                 3rd.dim: spin system index
c                                 4th.dim: temperature index
c  output: (r*8)  lrrec_sum(,,) = .true. => rad. rec. sum present
c                                 .false.=> rad. rec. sum not present
c                                 1st.dim: initial parent index
c                                 2nd.dim: final parent index
c                                 3rd.dim: spin system index
c  output: (r*8)  rrec_tot(,)   = total rad. rec. coefficients (cm3 s-1) tabulated
c                                 in dataset
c                                 1st.dim: initial parent index
c                                 2nd.dim: electron temperature index
c  output: (r*8)  lrrec_tot()   = .true. => rad. rec. total present in dataset
c                                 .false.=> rad. rec. total not present
c                                 1st.dim: initial parent index
c  output: (i*4)  nte           = no. of electron temperatures
c  output: (r*8)  tea()         = electron temperatures (k)
c                                 1st.dim: electron temperature index
c
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          xfesym     adas      return atomic symbol for nuclear charge
c          i4eiz0     adas      returns nucl. charge from element symbol
c          lenstr     adas      return effective length of string
c          xxcase     adas      change case of string
c          xxhkey     adas      extract key from a string
c          xxword     adas      extract position of number in buffer
c          dasumd     adas      sum DR over represantitive n-shells
c          xx48_1     adas      read LS 93 form
c          xx48_234   adas      read IC/LS/CA DR Project form
c          xx48_567   adas      read IC/CA hybrid form (W DR Project)
c
c
c  developed from xxdata_09 v1.5
c
c  author:  Martin O'Mullane
c
c  date:    13-04-2016
c
c
c  version  : 1.1
c  date     : 13-04-2016
c  modified : Martin O'Mullane
c               - first version
c
c  version  : 1.2
c  date     : 08-01-2018
c  modified : Martin O'Mullane
c               - Enable reading of type 4 (LS hybrid) files.
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   nsym        , ndword
c-----------------------------------------------------------------------
      real*8    dzero
c-----------------------------------------------------------------------
      parameter( nsym = 92  , dzero = 1.0D-70, ndword = 80 )
c-----------------------------------------------------------------------
      integer   i4unit      , i4eiz0
      integer   iunit       , ndprt          , ndrep      , ndlrep     ,
     &          ndlev       , ndt
      integer   iz          , iz0            , iz1        ,
     &          nprnt       , nprnti         , nprntf     , inrep      ,
     &          nlvl        , nrep           , nlrep      , ilrep      ,
     &          nte
      integer   indx        , i              , j          ,
     &          ipi         , ipf            ,
     &          iform       , is
      integer   ind1        , ind2           , istart     , istop
c-----------------------------------------------------------------------
      integer   ipa(ndprt)  , ispa(ndprt)    , ilpa(ndprt)
      integer   ia(ndlev)   , isa(ndlev)     , ila(ndlev)
      integer   ip(ndlev)
      integer   irepa_n(ndrep), nrepa(ndrep)
      integer   iprti(ndprt)
      integer   iprtf(ndprt,ndprt)
      integer   nsysf(ndprt,ndprt)
      integer   isys(ndprt,ndprt,2), ispsys(ndprt,ndprt,2)
      integer   irepa_nl(ndlrep)   , nlrepa_n(ndlrep)     ,
     &          nlrepa_l(ndlrep)
c-----------------------------------------------------------------------
      real*8    bwnp               , bwnr
c-----------------------------------------------------------------------
      real*8    xjpa(ndprt) , wpa(ndprt)
      real*8    xja(ndlev)  , wa(ndlev)
      real*8    tea(ndt)
      real*8    rrec_res(ndlev,ndprt,ndt)
      real*8    rrec_nl(ndlrep,ndprt,ndprt,2,ndt)
      real*8    rrec_n(ndrep,ndprt,ndprt,2,ndt)
      real*8    rrec_sum(ndprt,ndprt,2,ndt)
      real*8    rrec_tot(ndprt,ndt)
c-----------------------------------------------------------------------
      character seq*2       , esym*2         , ctype*2  , xfesym*2
      character c80*80      , c128*128       , cline*80 , buffer*128
      character cbreak*1
c-----------------------------------------------------------------------
      character cstrpa(ndprt)*(*) , cstrga(ndlev)*(*)
      character cspsys(ndprt,ndprt,2)*30
      character ckey(3)*6       , cans(3)*4
c-----------------------------------------------------------------------
      logical   ldr_res     , ldr_nl         , ldr_n      , l_hybrid
c-----------------------------------------------------------------------
      logical   lauga_res(ndlev)
      logical   lrrec_res(ndlev,ndprt)
      logical   lrrec_nl(ndlrep,ndprt,ndprt,2)
      logical   lrrec_n(ndrep,ndprt,ndprt,2)
      logical   lrrec_sum(ndprt,ndprt,2)
      logical   lrrec_tot(ndprt)
c-----------------------------------------------------------------------
      data      ckey/'seq','nucchg','type'/
c-----------------------------------------------------------------------


c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------

      ldr_res = .false.
      ldr_nl  = .false.
      ldr_n   = .false.

      nlrep = 0
      nrep  = 0

      nprnti = 0
      nprntf = 0
      nlvl   = 0

c-----------------------------------------------------------------------
c zero arrays
c-----------------------------------------------------------------------

      do i=1,ndlev
        do ipi=1,ndprt
          do j=1,ndt
            rrec_res(i,ipi,j)=0.0d0
            lrrec_res(i,ipi)=.false.
          enddo
        enddo
      enddo

      do ipi=1,ndprt
        lrrec_tot(ipi)=.false.
        do j=1,ndt
          rrec_tot(ipi,j)=0.0d0
        enddo
        do ipf=1,ndprt
          do is=1,2
            lrrec_sum(ipi,ipf,is)=.false.
            do ilrep=1,ndlrep
              lrrec_nl(ilrep,ipi,ipf,is)=.false.
              do j=1,ndt
                rrec_nl(ilrep,ipi,ipf,is,ndt)=0.0d0
              enddo
            enddo
            do inrep=1,ndrep
              lrrec_n(inrep,ipi,ipf,is)=.false.
              do j=1,ndt
                rrec_n(inrep,ipi,ipf,is,ndt)=0.0d0
              enddo
            enddo
            do j=1,ndt
              rrec_sum(ipi,ipf,is,j)=dzero
            enddo
          enddo
        enddo
      enddo


c----------------------------------------------------------------------
c read adf48 structure information
c----------------------------------------------------------------------

      read(iunit,'(1a80)') cline
      call xxcase (cline , c80, 'lc')
      ind1 = index(c80,'adf')
      ind2 = index(c80,'adf48')

      if ((ind1 .gt.0) .and. (ind2.eq.0)) then
          write(i4unit(-1),1001) ' wrong adf format'
          write(i4unit(-1),1010)
          stop
      endif
      if (ind1.eq.0) then
          ind1=len(c80)
      else
          ind1=ind1-1
      endif

      call xxhkey(c80(1:ind1),ckey(1),cbreak,cans(1))
      call xxhkey(c80(1:ind1),ckey(2),cbreak,cans(2))
      call xxhkey(c80(1:ind1),ckey(3),cbreak,cans(3))

      call xxslen(cans(1),istart,istop)
      seq=' '
      if(((istop-istart).eq.1).and.(cans(1)(istop:istop).ne."'"))
     &                        then
          seq(1:1)=cans(1)(istop:istop)
      elseif ((istop-istart).ge.2)then
          seq(1:istop-istart-1)=cans(1)(istart+1:istop-1)
      else
          write(i4unit(-1),1001) ' iso-sequence key/value missing'
          write(i4unit(-1),1010)
         stop
      endif

      call xxslen(cans(2),istart,istop)
      if (istart.ge.1)then
          read(cans(2)(istart:istop),*)iz0
      else
          write(i4unit(-1),1001) ' nuclear charge key/value missing'
          write(i4unit(-1),1010)
         stop
      endif

      call xxslen(cans(3),istart,istop)
      if ((istop-istart).eq.3)then
          ctype=cans(3)(istart+1:istop-1)
          if(ctype .eq. 'ca') then
              iform = 11
          elseif(ctype .eq. 'ls') then
              iform = 9
          elseif(ctype .eq. 'ic') then
              iform = 10
          endif
      else
          if(index(c80,'/ca/').gt.0) then
              iform = 3
              ctype='ca'
          elseif (index(c80,'/ls/').gt.0) then
              iform = 1
              ctype='ls'
          elseif (index(c80,'/ic/').gt.0) then
              iform = 2
              ctype='ic'
          endif
      endif


c----------------------------------------------------------------------
c check if iz0 is in range
c----------------------------------------------------------------------

      if ((iz0.le.0).or.(iz0.gt.nsym)) then
         write(i4unit(-1),1001)'nuclear charge',iz0,' out of range'
         write(i4unit(-1),1010)
         stop
      endif

      esym=xfesym(iz0)
      iz=iz0-i4eiz0(seq)
      iz1=iz+1

c----------------------------------------------------------------------
c determine whether file is hybrid or not and adjust iform
c the hybrid form uses nprnt for indexing. The older IC/LS
c datasets use nprnti and nrpntf.
c----------------------------------------------------------------------

      l_hybrid = .true.

   10 read(iunit,'(a)', end=15) c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'nprnti')
      if(indx.le.0) then
          go to 10
      else
          l_hybrid = .false.
      endif
   15 continue
      if (l_hybrid) iform = iform + 3

      rewind(iunit)

c----------------------------------------------------------------------
c call appropriate routine - badnell 93, ic/ls/ca 00 and hybrid ic/ca
c----------------------------------------------------------------------

      if (iform.eq.1.or.iform.eq.2) then

         call xx48_12 ( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                  ndlev    , ndt      ,
     &                  ctype    , iform    ,
     &                  nprnt    , nprnti   , nprntf   , bwnp     ,
     &                  ipa      , cstrpa   , ispa     , ilpa     ,
     &                  xjpa     , wpa      ,
     &                  nlvl     , bwnr     ,
     &                  ia       , ip       , cstrga   , isa      ,
     &                  ila      , xja      , wa       , lauga_res,
     &                  nlrep    , irepa_nl , nlrepa_n , nlrepa_l , 
     &                  nrep     , irepa_n  , nrepa    ,
     &                  iprti    ,
     &                  rrec_res , lrrec_res,
     &                  iprtf    ,
     &                  nsysf    , isys     , ispsys   , cspsys   ,
     &                  rrec_nl  , lrrec_nl , rrec_n   , lrrec_n  ,
     &                  rrec_sum , lrrec_sum, rrec_tot ,
     &                  nte      , tea
     &                )

      elseif (iform.eq.4.or.iform.eq.5.or.iform.eq.6) then

         call xx48_456( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                  ndlev    , ndt      ,
     &                  ctype    , iform    ,
     &                  nprnt    , nprnti   , nprntf   , bwnp     ,
     &                  ipa      , cstrpa   , ispa     , ilpa     ,
     &                  xjpa     , wpa      ,
     &                  nlvl     , bwnr     ,
     &                  ia       , ip       , cstrga   , isa      ,
     &                  ila      , xja      , wa       , lauga_res,
     &                  nlrep    , irepa_nl , nlrepa_n , nlrepa_l , 
     &                  nrep     , irepa_n  , nrepa    ,
     &                  iprti    ,
     &                  rrec_res , lrrec_res,
     &                  iprtf    ,
     &                  nsysf    , isys     , ispsys   , cspsys   ,
     &                  rrec_nl  , lrrec_nl , rrec_n   , lrrec_n  ,
     &                  rrec_sum , lrrec_sum, rrec_tot ,
     &                  nte      , tea
     &                )

      else

         write(i4unit(-1),1000)' unsupported format'
         write(i4unit(-1),1010)
         stop

      endif
c-----------------------------------------------------------------------
 1000 format(1x,31('*'),' xxdata_48 error ',30('*')//
     &       1x,'fault in input data file: ',a)
 1001 format(1x,31('*'),' xxdata_48 error ',30('*')//
     &       1x,'fault in input data file: ',a,i5,a)
 1010 format(/1x,29('*'),' program terminated ',29('*'))
c-----------------------------------------------------------------------


      end
