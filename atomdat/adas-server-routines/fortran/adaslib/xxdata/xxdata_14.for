       SUBROUTINE xxdata_14( IUNIT  , DSNAME ,
     &                       NSTORE , NTRDIM , NTDDIM ,
     &                       NBSEL  , ISELA  ,
     &                       IRZ0   , IRZ1   , IDZ0   ,
     &                       LEQUA  ,
     &                       CDONOR , CRECVR , CFSTAT ,
     &                       AMSRA  , AMSDA  ,
     &                       ITRA   , ITDA   ,
     &                       TFRA   , TFDA   ,
     &                       QFTEQA , QFTCXA
     &                     )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: xxdata_14 *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT THERMAL TOTAL CHARGER TRANSFER
C            RATE COEFFICIENT FILE FOR GIVN RECEIVER ION ELEMENT.
C            (MEMBER STORED IN IONATOM.DATA - MEMBER PREFIX 'TCX#').
C
C  CALLING PROGRAM: ADAS505/SQTCX
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH  BLOCK  FORMING  A  COMPLETE  SET OF  RATE-
C           COEFFICIENTS FOR A GIVEN  RECEIVER/DONOR COMBINATION.  EACH
C           DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER  DATA-
C           BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           TEMPERATURES        : EV
C           RATE COEFFICIENTS   : CM**3 SEC-1
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*44) DSNAME   = MVS DATA SET NAME OF DATA SET BEING READ
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NTRDIM   = MAX NUMBER OF RECEIVER TEMPERATURES ALLOWED
C  INPUT : (I*4)  NTDDIM   = MAX NUMBER OF DONOR    TEMPERATURES ALLOWED
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IRZ0()   = NUCLEAR CHARGE OF RECEIVING IMPURITY ION -
C                            READ FROM SELECTED DATA-BLOCK.
C                            DIMENSION: DATA-BLOCK INDEX.
C  OUTPUT: (I*4)  IRZ1()   = INITIAL CHARGE OF RECEIVER -
C                            READ FROM SELECTED DATA-BLOCK.
C                            DIMENSION: DATA-BLOCK INDEX.
C  OUTPUT: (I*4)  IDZ0()   = NUCLEAR CHARGE OF NEUTRAL DONOR -
C                            READ FROM SELECTED DATA-BLOCK.
C                            DIMENSION: DATA-BLOCK INDEX.
C
C  OUTPUT: (L*4)  LEQUA()  = READ - DATA SET ENTRY FORMAT
C                                   .TRUE.  => DATA  SET CONTAINS  EQUAL
C                                              TEMPERATURE COEFFICIENT.
C                                   .FALSE. => DATA SET DOES NOT CONTAIN
C                                              EQUAL TEMPERATURE COEFFT.
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*9)  CDONOR() = READ - DONOR ION IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*9)  CRECVR() = READ - RECEIVER ION IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*10) CFSTAT() = READ - FINAL STATE SPECIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  AMSRA()  = READ - RECEIVER ATOMIC MASS
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  AMSDA()  = READ - DONOR    ATOMIC MASS
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  ITRA()   = READ - NUMBER OF RECEIVER TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (I*4)  ITDA()   = READ - NUMBER OF DONOR    TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TFRA(,)  = READ - RECEIVER TEMPERATURES (UNITS: EV)
C                            1ST DIMENSION: RECEIVER TEMPERATURE INDEX
C                            2ND DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  TFDA(,)  = READ - DONOR    TEMPERATURES (UNITS: EV)
C                            1ST DIMENSION: DONOR    TEMPERATURE INDEX
C                            2ND DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  QFTEQA(,)= READ - EQUAL TEMPERATURE RATE-COEFFICIENTS
C                                   (UNITS: CM**3 SEC-1)
C                            1ST DIMENSION: RECEIVER TEMPERATURE INDEX
C                            2ND DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  QFTCXA(,,)=READ - FULL SET OF RATE-COEFFICIENTS
C                                   (UNITS: CM**3 SEC-1)
C                            1ST DIMENSION: DONOR    TEMPERATURE INDEX
C                            2ND DIMENSION: RECEIVER TEMPERATURE INDEX
C                            3RD DIMENSION: DATA-BLOCK INDEX
C
C          (C*2)  CEQUAL   = PARAMETER = 'EQ'
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITR      = ARRAY INDEX: RECEIVER TEMPERATURE INDEX
C          (I*4)  ITD      = ARRAY INDEX: DONOR    TEMPERATURE INDEX
C          (I*4)  NTRNUM   = NUMBER OF RECEIVER TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  NTDNUM   = NUMBER OF DONOR    TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C	   (I*7)  N7	   = MIN(7,NDTNUM) REQUIRED TO HANDLE > 7 DONOR TEMPS
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*10) IONNAM   = READ - DONOR/RECEIVER DESIGNATION STRING
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*3)  CKEY1    = 'AMD'    - INPUT BLOCK HEADER KEY
C          (C*3)  CKEY2    = 'AMR'    - INPUT BLOCK HEADER KEY
C          (C*3)  CKEY3    = 'FST'    - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL'  - INPUT BLOCK HEADER KEY
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          R8FCTN     ADAS      REAL*8 FUNCTION    -
C                               CONVERT CHARACTER STRING TO REAL
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    20/02/91
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  15/12/95 - HP SUMMERS- ADAS91: MODIFIED INFOMATION STRING USE
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C          NO CHANGES FROM IBM VERSION
C
C DATE:    20TH MARCH 1996
C
C VERSION: 1.1                  DATE: 20-03-96
C MODIFIED: WILLIAM OSBORN
C           - FIRST VERSION
C
C VERSION: 1.2                  DATE: 10-04-96
C MODIFIED: WILLIAM OSBORN
C           - REMOVED REDUNDANT VARIABLE
C
C VERSION: 1.3			DATE: 19-09-97
C MODIFIED: MARTIN O'MULLANE
C	    - MODIFIED TO ALLOW DATASETS WITH GREATER THAN 7 DONOR
C	      TEMPERATURES TO BE USED.
C
C-----------------------------------------------------------------------
C
C NOTES: Copied from e5data.for. This is v1.1 of xxdata_14.
C
C
C VERSION  : 1.1
C DATE     : 30-06-2015
C MODIFIED : Martin O'Mullane
C              - First version
C
C-----------------------------------------------------------------------
      CHARACTER  CEQUAL*2
C-----------------------------------------------------------------------
      PARAMETER( CEQUAL = 'EQ' )
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0                , I4FCTN              , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NTRDIM                , NTDDIM              , NBSEL
      INTEGER    IBLK                  , IPOS2               ,
     &           ITR                   , ITD                 ,
     &           NTRNUM                , NTDNUM              , IABT
      INTEGER    N7
C-----------------------------------------------------------------------
      REAL*8     R8FCTN
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*44
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*3               , CKEY2*3             ,
     &           CKEY3*3               , CKEY4*4             ,
     &           C6*6                  , C80*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         ,
     &           ITRA(NSTORE)          , ITDA(NSTORE)        ,
     &           IRZ0(NSTORE)          , IRZ1(NSTORE)        ,
     &           IDZ0(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CDONOR(NSTORE)*9      , CRECVR(NSTORE)*9    ,
     &           CFSTAT(NSTORE)*10
C-----------------------------------------------------------------------
      LOGICAL    LEQUA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     AMSRA(NSTORE)         , AMSDA(NSTORE)
      REAL*8     TFRA(NTRDIM,NSTORE)   , TFDA(NTDDIM,NSTORE) ,
     &           QFTEQA(NTRDIM,NSTORE)
      REAL*8     QFTCXA(NTDDIM,NTRDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4
C-----------------------------------------------------------------------
      DATA       CSLASH / '/' /
      DATA       CKEY1  / 'AMD'    /   , CKEY2  / 'AMR'    /  ,
     &           CKEY3  / 'FST'    /   , CKEY4  / 'ISEL'   /
C-----------------------------------------------------------------------
      LBEND = .FALSE.
C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------

      READ(IUNIT,*) NBSEL

         IF (NBSEL.GT.NSTORE) THEN
            WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
            NBSEL = NSTORE
         ENDIF

C-----------------------------------------------------------------------
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C-----------------------------------------------------------------------

         DO IBLK=1,NBSEL

C-----------------------------------------------------------------------
C INPUT DONOR/RECEIVER SPECS. AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------

            IF (.NOT.LBEND) THEN
                  READ(IUNIT,1000)   C80

                  IF ( C80(1:2).NE.'C-') THEN

                     CDONOR(IBLK) = C80(1:9)
                     CRECVR(IBLK )= C80( 11:19)

                     IPOS2        = INDEX(C80(20:80),'/') + 18

                     READ(C80(20:IPOS2),*) ITRA(IBLK) , ITDA(IBLK)

                     CALL XXHKEY( C80 , CKEY1 , CSLASH , C6           )

                     AMSDA(IBLK) = R8FCTN( C6 , IABT )

                     CALL XXHKEY( C80 , CKEY2 , CSLASH , C6           )

                     AMSRA(IBLK) = R8FCTN( C6 , IABT )

                     CALL XXHKEY( C80 , CKEY3 , CSLASH , CFSTAT(IBLK) )
                     CALL XXHKEY( C80 , CKEY4 , CSLASH , C2           )

                     ISELA(IBLK) = I4FCTN( C2  , IABT )

                     IDZ0(IBLK) = I4EIZ0( CDONOR(IBLK)(1:2) )
                     IRZ0(IBLK) = I4EIZ0( CRECVR(IBLK)(1:2) )
                     IRZ1(IBLK) = I4FCTN( CRECVR(IBLK)(4:5) , IABT )

                     NTRNUM = ITRA(IBLK)
                     NTDNUM = ITDA(IBLK)

                     IF (NTRNUM.GT.NTRDIM) THEN
                        WRITE(I4UNIT(-1),2001) DSNAME    , IBLK   ,
     &                                        'RECEIVER' ,
     &                                        NTRDIM     , NTRNUM
                        STOP
                     ENDIF

                     IF (NTDNUM.GT.NTDDIM) THEN
                        WRITE(I4UNIT(-1),2001) DSNAME    , IBLK   ,
     &                                        'DONOR'    ,
     &                                        NTDDIM     , NTDNUM
                        STOP
                     ENDIF

                     READ(IUNIT,1002) ( TFRA(ITR,IBLK) , ITR=1,NTRNUM )
                     READ(IUNIT,1000)  C80

                     LEQUA(IBLK) = ( INDEX(C80(1:9),CEQUAL) .GT. 0 )
                     N7 = MIN(7,NTDNUM)

                     IF ( LEQUA(IBLK) ) THEN
                        READ(C80  ,1003) ( TFDA(ITD,IBLK) ,
     &                                     ITD=1,n7   )
                         IF (N7.LT.NTDNUM) THEN
                            READ(IUNIT,1002) ( TFDA(ITD,IBLK) ,
     &                                         ITD=N7+1,NTDNUM )
                         ENDIF

                           DO ITR=1,NTRNUM
                              READ(IUNIT,1002)  QFTEQA(ITR,IBLK)    ,
     &                                         (QFTCXA(ITD,ITR,IBLK),
     &                                          ITD=1,NTDNUM        )
                           END DO
                     ELSE
                        READ(C80  ,1002) ( TFDA(ITD,IBLK) ,
     &                                     ITD=1,n7+1   )
                         IF (N7.LT.NTDNUM) THEN
                            READ(IUNIT,1002) ( TFDA(ITD,IBLK) ,
     &                                         ITD=N7+2,NTDNUM )
                         endif

                           DO ITR=1,NTRNUM
                              READ(IUNIT,1002) (QFTCXA(ITD,ITR,IBLK),
     &                                          ITD=1,NTDNUM        )
                           END DO
                     ENDIF

                  ELSE
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1
                  ENDIF

            ENDIF

         END DO

C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1002 FORMAT(8D9.2)
 1003 FORMAT(9X,7D9.2)

 2000 FORMAT(/1X,31('*'),' xxdata_14 MESSAGE ',31('*')/
     &        2X,'INPUT RECEIVER ION DATA SET NAME: ',A44/
     &        2X,'NUMBER OF RECEIVER/DONOR DATA-BLOCK COMBINATIONS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' xxdata_14 ERROR ',32('*')/
     &        2X,'INPUT RECEIVER ION DATA SET NAME: ',A44/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT ',A,' ION TEMPERATURES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' xxdata_14 MESSAGE ',31('*')/
     &        2X,'INPUT RECEIVER ION DATA SET NAME: ',A44/
     &        2X,'INCONSISTENCY IN THE NUMBER OF RECEIVER/DONOR',
     &           ' COMBINATIONS EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C-----------------------------------------------------------------------

      RETURN
      END
