       SUBROUTINE xxdata_01( IUNIT  , MXNENG , MXNSHL ,
     &                       SYMBR  , SYMBD  , IZR    , IZD    ,
     &                       INDD   , NENRGY , NMIN   , NMAX   ,
     &                       LPARMS , LSETL  , ENRGYA ,
     &                       ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &                       PL3A   , SIGTA  , SIGNA  , SIGLA
     &                     )

       IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: xxdata_01 *******************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT DATA SET OF TYPE ADF01.
C
C  CALLING PROGRAM: ADAS301/ADAS306/ADAS307/ADAS308/ADAS309
C
C  DATA:
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           COLLISION ENERGIES  : KEV/AMU
C           ALPHA               :
C           TOTAL XSECTS.       : CM2
C           N-SHELL XSECTS.     : CM2
C           NL-SHELL DATA       : CM2
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C
C  OUTPUT: (C*2)  SYMBR     = READ - RECEIVER ION ELEMENT SYMBOL.
C  OUTPUT: (C*2)  SYMBD     = READ - DONOR ION ELMENT SYMBOL.
C  OUTPUT: (I*4)  IZR       = READ - ION CHARGE OF RECEIVER.
C  OUTPUT: (I*4)  IZD       = READ - ION CHARGE OF DONOR.
C  OUTPUT: (I*4)  INDD      = READ - DONOR STATE INDEX.
C  OUTPUT: (I*4)  NENRGY    = NUMBER OF ENERGIES READ.
C  OUTPUT: (I*4)  NMIN      = LOWEST N-SHELL FOR WHICH DATA READ.
C  OUTPUT: (I*4)  NMAX      = HIGHEST N-SHELL FOR WHICH DATA READ.
C  OUTPUT: (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C  OUTPUT: (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C  OUTPUT: (R*8)  ENRGYA()  = READ - COLLISION ENERGIES.
C                             UNITS: EV/AMU (READ AS KEV/AMU)
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  ALPHAA()  = READ - EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (I*4)  LFORMA()  = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  XLCUTA()  = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  PL2A()    = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  PL3A()    = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  SIGTA()   = READ - TOTAL CHARGE EXCHANGE
C                                    CROSS-SECTION.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  SIGNA(,)  = READ - N-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C  OUTPUT: (R*8)  SIGLA(,)  = READ - L-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C
C          (R*8)  ZEROST   = PARAMETER = EFFECTIVE SHIFT APPLIED TO
C                                        CROSS-SECTION VALUES TO AVOID
C                                        ZERO VALUES (WILL NOT AFFECT
C                                        ANY VALUES WHICH ARE GREATER
C                                        THAN AROUND 1.0E+15*ZEROSHFT -
C                                        i.e. 1.0E-25.)
C
C          (I*4)  OLDMIN   = PREVIOUS VALUE READ FOR NMIN.
C          (I*4)  OLDMAX   = PREVIOUS VALUE READ FOR NMAX.
C          (I*4)  IBLK     = CURRENT DATA BLOCK.
C          (I*4)  IVALUE   = USED TO PARSE FOR END OF DATA FLAG (-1).
C          (I*4)  N        = N QUANTUM NUMBER.
C          (I*4)  L        = L QUANTUM NUMBER.
C          (I*4)  I        = LOOP COUNTER.
C          (I*4)  J        = LOOP COUNTER.
C          (I*4)  IERR     = ERROR RETURN CODE.
C          (C*2)  CIZR     = ION CHARGE OF RECEIVER.
C          (C*2)  CIZD     = ION CHARGE OF DONOR.
C          (C*1)  INDD     = DONOR STATE INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4FCTN     ADAS      RETURNS CHARACTER STRING AS AN INTEGER.
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C          XXIDTL     ADAS      INVERSE OF I4IDFL. RETURNS QUANTUM
C                               NUMBERS N AND L FROM INDEX.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    21/09/93
C
C UPDATE:  18/10/93 - J NASH    - ADAS91:
C          UPDATED TO READ L-SPLITTING PARAMETERS IF PRESENT IN DATASET.
C
C UPDATE:  01/05/95 - Tim Hammond - IDLADAS:
C          UNIX port.
C
C UPDATE:  16/05/95 - Tim Hammond - IDLADAS:
C          ADDED AND APPLIED ZEROST PARAMETER => EFFECTIVE ZERO FOR
C          CROSS-SECTIONS (CODING DONE BY PAUL BRIDEN).
C
C-----------------------------------------------------------------------
C
C
C NOTES: Copied from cxdata.for.
C        Remove the redundant titled from argument list.
C        This is v1.1 of xxdata_01.
C
C
C VERSION  : 1.1
C DATE     : 14-09-2004
C MODIFIED : Martin O'Mullane
C              - First version.
C
C VERSION  : 1.2
C DATE     : 26-04-2007
C MODIFIED : Hugh Summers
C             - Remove unused m-subshell data possibility.
C
C VERSION  : 1.3
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Initialize output arrays to zero.
C
C VERSION  : 1.4
C DATE     : 12-06-2008
C MODIFIED : Allan Whiteford
C              - Correctly parse files which contain m-subshell
C                data and print a warning to say that the m-resolved
C                data were ignored.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL *8   ZEROST
C-----------------------------------------------------------------------
      PARAMETER (ZEROST = 1.0D-40)
C-----------------------------------------------------------------------
      INTEGER   I4FCTN  , I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER   IUNIT   , MXNSHL  , MXNENG   ,
     &          IZR     , IZD     , INDD     ,
     &          NENRGY  , NMIN    , NMAX
      INTEGER   NEMORE  , OLDMIN  , OLDMAX  , IBLK    , IVALUE  ,
     &          N       , L       , I       , J       ,
     &          IERR
C-----------------------------------------------------------------------
      LOGICAL   LPARMS  , LSETL   , LWARNM
C-----------------------------------------------------------------------
      CHARACTER SYMBR*2    , SYMBD*2
      CHARACTER CLINE*133  , CIZR*2     , CIZD*2     , CINDD*1
C-----------------------------------------------------------------------
      INTEGER   LFORMA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8    ENRGYA(MXNENG)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &          PL2A(MXNENG)    , PL3A(MXNENG)    , SIGTA(MXNENG)
      REAL*8    SIGNA(MXNENG,MXNSHL)                           ,
     &          SIGLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------

      IVALUE = -1
      NENRGY = 0
      NEMORE = 0
      OLDMIN = 0
      OLDMAX = 0
      IBLK = 0
      LSETL = .FALSE.
      LWARNM = .FALSE.
      do i = 1, mxneng

        enrgya(i) = 0.0D0
        alphaa(i) = 0.0D0
        xlcuta(i) = 0.0D0
        pl2a(i)   = 0.0D0
        pl3a(i)   = 0.0D0
        sigta(i)  = 0.0D0
        do n = 1, mxnshl
           signa(i,n) = 0.0D0
        end do

        do l = 1, mxnshl*(mxnshl+1)/2
           sigla(i,l) = 0.0D0
        end do

      end do

C-----------------------------------------------------------------------
C READ IN DATA.
C-----------------------------------------------------------------------

      READ(IUNIT,1000) CLINE
      READ(CLINE,1001) SYMBR, CIZR, SYMBD, CIZD, CINDD
      IZR = I4FCTN( CIZR , IERR )
      IF (IERR .EQ. 0) IZD = I4FCTN( CIZD , IERR )
      IF (IERR .EQ. 0) INDD = I4FCTN( CINDD , IERR )
      IF (IERR .NE. 0) THEN
         WRITE(I4UNIT(-1),2000)
         WRITE(I4UNIT(-1),2006)
         STOP
      ENDIF

C CHECK FOR L-SPLITTING PARAMETERS FLAG.

      IF ( INDEX( CLINE , 'LPARMS' ) .NE. 0 ) THEN
         LPARMS = .TRUE.
      ELSE
         LPARMS = .FALSE.
      ENDIF

C-----------------------------------------------------------------------
C START OF READ LOOP.
C-----------------------------------------------------------------------

    1 CONTINUE

C-----------------------------------------------------------------------
C READ NEXT LINE OF DATA FILE.
C-----------------------------------------------------------------------

      READ(IUNIT,1000,END=6) CLINE

C-----------------------------------------------------------------------
C IGNORE LINE IF COMMENT.
C-----------------------------------------------------------------------

      IF ((CLINE(1:1) .EQ. 'C') .OR. (CLINE(1:1) .EQ. 'c')) GOTO 1

C-----------------------------------------------------------------------
C CHECK FOR END OF DATA FLAG.
C-----------------------------------------------------------------------

      READ(CLINE,1002) IVALUE
      IF (IVALUE .GT. 0) THEN

C-----------------------------------------------------------------------
C EITHER HEADER FOR DATA BLOCK;
C-----------------------------------------------------------------------

         IF (INDEX( CLINE, '/' ) .NE. 0) THEN

C INCREMENT BLOCK COUNTER AND UPDATE NUMBER OF ENERGIES READ.

            IBLK = IBLK + 1
            NENRGY = NENRGY + NEMORE

C READ NUMBER OF ENERGIES IN NEXT BLOCK. REDUCE IF TOO LARGE.

            READ(CLINE,1002) NEMORE
            IF ((NENRGY + NEMORE) .GT. MXNENG) THEN
               WRITE(I4UNIT(-1),2001) MXNENG
               NEMORE = MXNENG - NENRGY
            ENDIF

C READ MAXIMUM AND MINIMUM N VALUES AND CHECK AGAINST PREVIOUS VALUES.

            READ(IUNIT,1002) NMIN
            IF (OLDMIN .EQ. 0) THEN
               OLDMIN = NMIN
            ELSE IF (NMIN .NE. OLDMIN) THEN
               WRITE(I4UNIT(-1),2002) IBLK, 'MINIMUM', NMIN, OLDMIN
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF

            READ(IUNIT,1002) NMAX
            IF (NMAX .GT. MXNSHL) THEN
               WRITE(I4UNIT(-1),2003) IBLK, NMAX, MXNSHL
               WRITE(I4UNIT(-1),2006)
               STOP
            ELSE IF (OLDMAX .EQ. 0) THEN
               OLDMAX = NMAX
            ELSE IF (NMAX .NE. OLDMAX) THEN
               WRITE(I4UNIT(-1),2002) IBLK, 'MAXIMUM', NMAX, OLDMAX
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF

C READ REMAINDER OF HEADER DATA.

            READ(IUNIT,1003) (ENRGYA(NENRGY+I), I=1,NEMORE)
            READ(IUNIT,1003) (ALPHAA(NENRGY+I), I=1,NEMORE)

            IF (LPARMS) THEN
               READ(IUNIT,1004) (LFORMA(NENRGY+I), I=1,NEMORE)
               READ(IUNIT,1005) (XLCUTA(NENRGY+I), I=1,NEMORE)
               READ(IUNIT,1005) (PL2A(NENRGY+I), I=1,NEMORE)
               READ(IUNIT,1005) (PL3A(NENRGY+I), I=1,NEMORE)
            ENDIF

            READ(IUNIT,1005) (SIGTA(NENRGY+I), I=1,NEMORE)
            READ(IUNIT,1000) CLINE

C INITIALIZE PARTIAL CROSS-SECTION ARRAYS FOR NEXT BLOCK TO -1.

            DO 2 I = NENRGY+1, NENRGY+NEMORE

               DO 3 J = NMIN, NMAX
                  SIGNA(I,J) = -1.0D+00
    3           CONTINUE

               DO 4 J = I4IDFL(NMIN,0), I4IDFL(NMAX,NMAX-1)
                  SIGLA(I,J) = -1.0D+00
    4           CONTINUE

    2       CONTINUE

C-----------------------------------------------------------------------
C OR L-RESOLVED PARTIAL CROSS SECTIONS;
C-----------------------------------------------------------------------

         ELSE IF(CLINE(4:6).NE.'   ' .AND. CLINE(7:9).EQ.'   ') THEN

C READ DATA.

            READ(CLINE,1007) N, L
            IF ((N.GE.NMIN) .AND. (N.LE.NMAX)  .AND.
     &          (L.GE.0)    .AND. (L.LE.(N-1))      ) THEN
               READ (CLINE,1005) (SIGLA(NENRGY+I,I4IDFL(N,L)),
     &                            I=1,NEMORE)
            ELSE
               WRITE(I4UNIT(-1),2004) IBLK, NMIN, NMAX, 'N,L', N, L
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF

C SET L-RESOLVED DATA FLAG.

            LSETL = .TRUE.

C-----------------------------------------------------------------------
C OR OLD-STYLE m-RESOLVED DATA IS FOUND;
C-----------------------------------------------------------------------

         ELSE IF(CLINE(4:6).NE.'   ') THEN
               LWARNM=.TRUE.

C-----------------------------------------------------------------------
C OR N-RESOLVED PARTIAL CROSS SECTIONS.
C-----------------------------------------------------------------------

	 ELSE

C READ DATA.

            READ(CLINE,1008) N
            IF ((N.GE.NMIN) .AND. (N.LE.NMAX)) THEN
               READ (CLINE,1005) (SIGNA(NENRGY+I,N), I=1,NEMORE)
            ELSE
               WRITE(I4UNIT(-1),2004) IBLK, NMIN, NMAX, 'N', N
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF

         ENDIF

C-----------------------------------------------------------------------
C GOTO START OF READ LOOP.
C-----------------------------------------------------------------------

         GOTO 1

      ENDIF

C-----------------------------------------------------------------------
C POST READ PROCESSING.
C-----------------------------------------------------------------------

    6 CONTINUE

C-----------------------------------------------------------------------
C UPDATE NUMBER OF ENERGIES.
C-----------------------------------------------------------------------

      NENRGY = NENRGY + NEMORE

C-----------------------------------------------------------------------
C CONVERT ENERGIES FROM KEV/AMU TO EV/AMU.
C-----------------------------------------------------------------------

      DO 7 I = 1, NENRGY
         ENRGYA(I) = ENRGYA(I) * 1.0D+03
    7 CONTINUE

C-----------------------------------------------------------------------
C CHECK DATA + ADD IN ZERO SHIFT.
C-----------------------------------------------------------------------

      DO 8 I = 1, NENRGY

         DO 9 J = NMIN, NMAX
            IF (SIGNA(I,J) .EQ. -1.0D+00) THEN
               WRITE(I4UNIT(-1),2005) 'N', ENRGYA(I), 'N', J
               WRITE(I4UNIT(-1),2006)
               STOP
            ELSE
               SIGNA(I,J) = SIGNA(I,J) + ZEROST
            ENDIF
    9    CONTINUE

         IF (LSETL) THEN
            DO 10 J = I4IDFL(NMIN,0), I4IDFL(NMAX,NMAX-1)
               IF (SIGLA(I,J) .EQ. -1.0D+00) THEN
                  CALL XXIDTL( J, N, L )
                  WRITE(I4UNIT(-1),2005) 'L', ENRGYA(I), 'N,L', N, L
                  WRITE(I4UNIT(-1),2006)
                  STOP
               ELSE
                  SIGLA(I,J) = SIGLA(I,J) + ZEROST
               ENDIF
   10       CONTINUE
         ENDIF

    8 CONTINUE

      IF (LWARNM) WRITE(I4UNIT(-1),2007)
C-----------------------------------------------------------------------


 1000 FORMAT(1A133)
 1001 FORMAT(A2,1X,A2,5X,A2,1X,A2,2X,A1)
 1002 FORMAT(1X,I4)
 1003 FORMAT(10X,9F9.2)
 1004 FORMAT(10X,9I9)
 1005 FORMAT(10X,1P,9D9.2)
 1006 FORMAT(3I3)
 1007 FORMAT(2I3)
 1008 FORMAT(I3)

 2000 FORMAT(1X,32('*'),' xxdata_01 ERROR ',32('*')//
     &       2X,'DATA FILE HAS INCORRECT FORMAT.'/
     &       2X,'EXPECTING NEW ADF01 FORMAT.')
 2001 FORMAT(1X,31('*'),' xxdata_01 MESSAGE ',31('*')//
     &       2X,'TOTAL NUMBER OF ENERGIES IN INPUT DATA SET TOO GREAT.'/
     &       2X,'ONLY THE FIRST ',I2,'HAVE BEEN ACCEPTED.'//
     &       1X,32('*'),' END OF MESSAGE ',33('*'))
 2002 FORMAT(1X,32('*'),' xxdata_01 ERROR ',32('*')//
     &       2X,'ERROR IN DATA SET BLOCK ',I3,' :'/
     &       2X,A,' READ FOR N DOES NOT MATCH PREVIOUS VALUE.'/
     &       2X,'VALUE READ = ',I3/
     &       2X,'PREVIOUS VALUE = ',I3)
 2003 FORMAT(1X,32('*'),' xxdata_01 ERROR ',32('*')//
     &       2X,'ERROR IN DATA SET BLOCK ',I3,' :'/
     &       2X,'MAXIMUM VALUE OF N TOO GREAT.'/
     &       2X,'VALUE READ  = ',I3/
     &       2X,'MAX ALLOWED = ',I3/
     &       2X,'INCREASE PARAMETER ''MXNSHL''.')
 2004 FORMAT(1X,32('*'),' xxdata_01 ERROR ',32('*')//
     &       2X,'ERROR IN DATA SET BLOCK ',I3,' :'/
     &       2X,'INVALID QUANTUM NUMBERS READ.'/
     &       2X,'NMIN, NMAX = ',I3,1X,I3/
     &       2X,A,' = ',3(I3,1X))
 2005 FORMAT(1X,32('*'),' xxdata_01 ERROR ',32('*')//
     &       2X,'MISSING ',A,'-RESOLVED DATA.'/
     &       2X,'ENERGY = ',F9.2, ' KEV/AMU'/
     &       2X,A,' = ',3(I3,1X))
 2006 FORMAT(/1X,30('*'),' PROGRAM TERMINATED ',31('*'))
 2007 FORMAT(1X,31('*'),' xxdata_01 MESSAGE ',31('*')//
     &       2X,'M-RESOLVED DATA FOUND IN ADF01 FILE BUT NOT READ.'/
     &       1X,32('*'),' END OF MESSAGE ',33('*'))

C-----------------------------------------------------------------------

      RETURN
      END
