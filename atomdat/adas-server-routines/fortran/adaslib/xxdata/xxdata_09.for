      subroutine xxdata_09( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                      ndlev    , ndaug    , ndt      , ndsc     ,
     &                      seq      , iz       , iz0      , iz1      ,
     &                      ctype    , iform    ,
     &                      nprnt    , nprnti   , nprntf   ,
     &                      bwnp     ,
     &                      ipa      , cstrpa   , ispa     , ilpa     ,
     &                      xjpa     , wpa      ,
     &                      ipca     , cstrpca  , xjpca    , wpca     ,
     &                      nlvl     , ncfg     , nippy    , 
     &                      bwnr     ,
     &                      ia       , ip       , cstrga   , isa      ,
     &                      ila      , xja      , wa       ,
     &                      iscp     , iscn     , iscl     ,
     &                      fsc      , wnsc     ,
     &                      auga_res , lauga_res,
     &                      nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                      irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                      lauga_nl ,
     &                      nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                      irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                      iprti    ,
     &                      diel_res , ldiel_res,
     &                      iprtf    ,
     &                      nsysf    , isys     , ispsys   , cspsys   ,
     &                      diel_nl  , ldiel_nl , diel_n   , ldiel_n  ,
     &                      diel_sum , ldiel_sum, diel_tot , ldiel_tot,
     &                      nte      , tea
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxdata_09 ******************
c
c  purpose:  to fetch data from input adf09 data set.
c
c  calling program:  various
c
c
c  subroutine:
c
c  input : (i*4)  iunit   = unit to which input file is allocated
c
c  input : (i*4)  ndprt   = maximum number of parent states
c  input : (i*4)  ndrep   = max. number of representative n-shells
c  input : (i*4)  ndlrep  = max. number of representative nl-shells
c  input : (i*4)  ndlev   = maximum number of resolved levels
c  input : (i*4)  ndaug   = maximum number of auger rate initial and
c                           final parent pairs
c  input : (i*4)  ndt     = max. number of electron temperatures
c  input : (i*4)  nsc     = max. number of configs straddling IP
c
c  output: (c*2)  seq     = recombined ion seq
c  output: (i*4)  iz      = recombined ion charge
c  output: (i*4)  iz0     = nuclear charge
c  output: (i*4)  iz1     = recombining ion charge
c  output: (c*2)  ctype   = resolution type (ls/ic/ca)
c  output: (i*4)  iform   = data set organisational form
c                            1: LS 1993 Badnell data form
c                            2: LS 2000 Badnell data form
c                            3: IC 2000 Badnell data form
c                            4: CA 2000 Badnell data form
c                            5: hybrid LS 2015 Badnell data form
c                            6: hybrid IC 2015 Badnell data form
c                            7: hybrid CA 2015 Badnell data form
c                            8: unassigned
c                            9: LS 2016 ADAS standard data form
c                           10: IC 2016 ADAS standard data form
c                           11: CA 2016 ADAS standard data form
c  output: (i*4)  nprnt   = total number of parents
c  output: (i*4)  nprnti  = number of parents which are initial parents
c  output: (i*4)  nprntf  = number of parents which are final parents
c                           or the number of parent configs for
c                           hybrid IC/LS (types 5 and 6).
c  output: (r*8)  bwnp    = binding wave no. of ground parent (cm-1)
c  output: (i*4)  ipa()   = parent energy level index number
c                           1st dim: parent lvl/trm/cfg index
c  output: (c*18) cstrpa()= nomencl./config. for parent level 'ipa()'
c                           1st dim: parent lvl/trm/cfg index
c  output: (i*4)  ispa()  = multiplicity for parent level 'ipa()'
c                           note: (ispa-1)/2 = quantum number (sp)
c                           1st dim: parent lvl/trm/cfg index
c  output: (i*4)  ilpa()  = quantum number (lp) for parent level 'ipa()'
c                           1st dim: parent lvl/trm/cfg index
c  output: (r*8)  xjpa()  = quantum number (jp) for parent level 'ipa()'
c                           note: (2*xjpa)+1 = statistical weight
c                           1st dim: parent lvl/trm/cfg index
c  output: (r*8)  wpa()   = energy relative to parent level 1 (cm-1)
c                           for parent level 'ipa()'
c                           1st dim: parent lvl/trm/cfg index
c  output: (i*4)  nlvl    = number of energy levels (lvl/trm/cfg) of
c                           recombined ion
c  output: (i*4)  ncfg    = number of averaged energy levels (lvl/trm/cfg)
c                           of recombined ion for hybrid adf09
c                           used instead of nlvl for these
c  output: (i*4)  nippy   = number of nl levels straddling the ionisation
c                           potential.
c  output: (r*8)  bwnr    = ionisation potential (cm-1) of lowest level
c                           of recombined ion
c  output: (i*4)  ia()    = recombined ion energy level index number
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  ip()    = associated parent of recombined level
c                           1st.dim: lvl/trm/cfg index
c  output: (c*18) cstrga()= nomencl./config. for recombined ion level
c                           'ia()'
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  isa()   = multiplicity for recombined level 'ia()'
c                           note: (isa-1)/2 = quantum number (s)
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  ila()   = quantum number (l) for recombined level
c                           'ia()'
c                           1st.dim: lvl/trm/cfg index
c  output: (r*8)  xja()   = quantum number (j) for recombined level
c                           'ia()'
c                           note: (2*xja)+1 = statistical weight
c                           1st.dim: lvl/trm/cfg index
c  output: (r*8)  wa()    = energy relative to recombined level 1 (cm-1)
c                           for recombined level 'ia()'
c                           1st.dim: lvl/trm/cfg index
c  output: (i*4)  ipca()  = parent config averaged index number
c                           1st dim: parent cfg index
c  output: (c*) cstrpca() = nomencl./config. for parent config 'ipca()'
c                           1st dim: parent cfg index
c  output: (r*8)  xjpca() = quantum number (jp) for parent level 'ipca()'
c                           note: (2*xjpa)+1 = statistical weight
c                           1st dim: parent cfg index
c  output: (r*8)  wpca()  = energy relative to parent level 1 (cm-1)
c                           for parent level 'ipca()'
c                           1st dim: parent cfg index
c  output: (i*4)  iscp()  = parent of IP straddling nl-level
c                           1st.dim: cfg index
c  output: (i*4)  iscn()  = n quantum number of IP straddling  nl-level
c                           1st.dim: cfg index
c  output: (i*4)  iscl()  = l quantum number of IP straddling  nl-level
c                           1st.dim: cfg index
c  output: (r*8)  fsc(,)  = fractional statistical weight of the levels
c                           of a config which lie below and above the IP
c                           1st.dim: index
c                           2nd.dim: 1 -> below, 2-> above
c  output: (r*8)  wsc(,)  = the statistical average energies of the levels
c                           of a config which lie below and above the IP
c                           1st.dim: index
c                           2nd.dim: 1 -> below, 2-> above
c  output: (r*8)  auga_res(,) =specific auger rates for resolved levels
c                           1st.dim: lvl/trm/cfg index
c                           2nd.dim: parent pair index
c  output: (l*4)  lauga_res(,) = .true. => auger rate present for lvl/trm/cfg
c                            .false.=> auger rate not present
c                           1st.dim: lvl/trm/cfg index
c                           2nd.dim: parent pair index
c  output: (i*4)  nlrep      = number of representative nl-shells
c  output: (i*4)  iaprs_nl   = number of nl-shell auger rate initial
c                              and final parent pairs
c  output: (c*10) caprs_nl() = nl-shell auger rate parent pair string
c                              1st.dim: parent pair index
c  output: (i*4)  ipaug_nl(,)= initial and final parents index
c                               for nl-shell auger breakups
c                              1st.dim: parent pair index
c                              2nd.dim: 2 (for initial and final parent)
c  output: (i*4)  irepa_nl() = representative nl-shell index number
c                              1st dim: representative nl-shell index
c  output: (i*4)  irepa_nl() = representative nl-shell index number
c                              1st dim: representative nl-shell index
c  output: (i*4)  nlrepa_n() = n value for representative nl-shell
c                              1st dim: representative nl-shell index
c  output: (i*4)  nlrepa_l() = l value for representative nl-shell
c                              1st dim: representative nl-shell index
c  output: (r*8)  auga_nl(,) = auger rates (sec-1)
c                              1st.dim: representative nl-shell index
c                              2nd.dim: parent pair index
c  output: (l*4)  lauga_nl(,) = .true. => auger rate present for nl-shell
c                               .false.=> auger rate not present
c                              1st.dim: representative n-shell index
c                              2nd.dim: parent pair index
c  output: (i*4)  nrep        = number of representative n-shells
c  output: (i*4)  iaprs_n     = number of n-shell auger rate initial
c                               and final parent pairs
c  output: (c*10) caprs_n()   = auger rate parent pair string
c                               1st.dim: parent pair index
c  output: (i*40) ipaug_n(,)  = initial and final parents index
c                               for n-shell auger breakups
c                               1st.dim: parent pair index
c                               2nd.dim: 2 (for initial and final parent)
c  output: (i*4)  irepa_n()   = representative n-shell index number
c                               1st dim: representative n-shell index
c  output: (i*4)  nrepa()     = n value for representative n-shell
c                               1st dim: representative n-shell index
c  output: (r*8)  auga_n(,)   = auger rates (sec-1)
c                               1st.dim: representative n-shell index
c                               2nd.dim: parent pair index
c  output: (l*4)  lauga_n(,)  = .true. => auger rate present for n-shell
c                               .false.=> auger rate not present
c                               1st.dim: representative n-shell index
c                               2nd.dim: parent pair index
c  output: (i*4)  iprti()     = initial parent index from block header
c                               1st dim: initial parent index number
c  output: (r*8)  diel_res(,,)= lvl/trm/cfg resol. diel. coeffts.(cm3 s-1)
c                               1st.dim: level index
c                               2nd.dim: initial parent index
c                               3rd.dim: temperature index
c  output: (l*4)  ldiel_res(,)= .true. => diel. present for lvl/trm/cfg index
c                               .false.=> diel. not present for
c                                          lvl/trm/cfg index
c                               1st.dim: level index
c                               2nd.dim: initial parent index
c  output: (i*4)  iprtf(,)    = final parent index from block header
c                                  1st dim: initial parent index number
c                                  2nd dim: final parent index number
c  output: (i*4)  nsysf(,)    = no. of spin systems built on final parent
c                               defaults to 1 for /ic/ca/
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c  output: (i*4)  isys(,,)    = n-shell spin system index for final parent
c                               defaults to 1 for /ic/ca/
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c                               3rd dim: spin system index number
c  output: (i*4)  ispsys(,,)  = n-shell spin system for final parent
c                               applicable to /ls/ only
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c                               3rd dim: spin system index number
c  output: (c*20) cspsys(,,)  = characterising string for final parent
c                               system - universal for /ls/ic/ca/
c                               1st dim: initial parent index number
c                               2nd dim: final parent index number
c                               3rd dim: spin system index number
c  output: (r*8)  diel_nl(,,,,) = nl-shell dielec. coeffts.(cm3 s-1)
c                                 1st.dim: repr. nl-shell index
c                                 2nd.dim: initial parent index
c                                 3rd.dim: final parent index
c                                 4th.dim: spin system index
c                                 5th.dim: temperature index
c  output: (r*8)  ldiel_nl(,,,)= .true. => diel. present for repr. nl-shell
c                                .false.=> diel. not present for  nl-shell
c                                1st.dim: repr. nl-shell index
c                                2nd.dim: initial parent index
c                                3rd.dim: final parent index
c                                4th.dim: spin system index
c  output: (r*8)  diel_n(,,,,) = n-shell dielec. coeffts.(cm3 s-1)
c                                1st.dim: repr. n-shell index
c                                2nd.dim: initial parent index
c                                3rd.dim: final parent index
c                                4th.dim: spin system index
c                                5th.dim: temperature index
c  output: (r*8)  ldiel_n(,,,,)= .true. => diel. present for repr. n-shell
c                                .false.=> diel. not present for  n-shell
c                                1st.dim: repr. n-shell index
c                                2nd.dim: initial parent index
c                                3rd.dim: final parent index
c                                4th.dim: spin system index
c  output: (r*8)  diel_sum(,,,) = sum of n-shell dielec. coeffts.(cm3 s-1)
c                                 1st.dim: initial parent index
c                                 2nd.dim: final parent index
c                                 3rd.dim: spin system index
c                                 4th.dim: temperature index
c  output: (r*8)  ldiel_sum(,,) = .true. => diel. sum present
c                                 .false.=> diel. sum not present
c                                 1st.dim: initial parent index
c                                 2nd.dim: final parent index
c                                 3rd.dim: spin system index
c  output: (r*8)  diel_tot(,)   = total diel coefficients (cm3 s-1) tabulated
c                                 in dataset
c                                 1st.dim: initial parent index
c                                 2nd.dim: electron temperature index
c  output: (r*8)  ldiel_tot()   = .true. => diel. total present in dataset
c                                 .false.=> diel. total not present
c                                 1st.dim: initial parent index
c  output: (i*4)  nte           = no. of electron temperatures
c  output: (r*8)  tea()         = electron temperatures (k)
c                                 1st.dim: electron temperature index
c
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          xfesym     adas      return atomic symbol for nuclear charge
c          i4eiz0     adas      returns nucl. charge from element symbol
c          lenstr     adas      return effective length of string
c          xxcase     adas      change case of string
c          xxhkey     adas      extract key from a string
c          xxword     adas      extract position of number in buffer
c          dasumd     adas      sum DR over represantitive n-shells
c          xx09_1     adas      read LS 93 form
c          xx09_234   adas      read IC/LS/CA DR Project form
c          xx09_567   adas      read IC/CA hybrid form (W DR Project)
c
c
c  developed from dadata v1.2
c
c  author:  A D Whiteford, University of Strathclyde
c
c  date:    02/05/05
c
c
c  version  : 1.1
c  date     : 22-05-2008
c  modified : Allan Whiteford
c               - first version
c
c  version  : 1.2
c  date     : 22-05-2009
c  modified : Hugh Summers
c               - Major revision to include 'ic and 'ca' forms of data,
c                 and to allow the optional use of the more recent
c                 standardised descriptors used in other data formats
c
c  version  : 1.3
c  date     : 21-08-2009
c  modified : Martin O'Mullane
c               - Remove redundant variables and labels.
c               - Make sure totals are read in.
c               - Remove on-screen warning for m-m' wrapped data.
c
c  version  : 1.4
c  date     : 03-07-2012
c  modified : Martin O'Mullane
c               - Allow index of N- and NL- Auger rate to be greater
c                 than 9.
c               - Increase ndword to 80 and c512 to 712 characters.
c               - Return just the first ndprt number of final parents.
c               - Allow orbital quantum number (ila) of 10 and higher
c                 (l=n, o, p...) given as A, B, C... in adf09 definition.
c               - Also check for '*' or ' ' in ila since this quantum
c                 number can be unsound in radiative (adf48) datasets.
c               - Modify Auger rate reading for more than 6 entries as
c                 adf09 specification wraps these onto following line.
c               - For adf48 there are no Auger rates so test whether it
c                 is necessary to read the following line when INDP > 8.
c
c  version  : 1.5
c  date     : 08-04-2016
c  modified : Martin O'Mullane
c               - Restructured to split the reading of the different
c                 types of adf09 file into subroutines. Identification
c                 of the type (returned as iform) is performed here.
c               - Add hybrid IC and CA files (generated from Tungsten
c                 DR Project). New arguments required for this type of
c                 adf09 file.
c               - Adjust iform to match reality of adf09 formats.
c               - There is a new xxdata_48 subroutine so there is no 
c                 longer a need to consider accommodating its differences.
c
c  version  : 1.6
c  date     : 08-01-2018
c  modified : Martin O'Mullane
c               - Enable reading LS hybrid, type 5, adf09 files.
c               - Return isa() for LS-hybrid form (type 5).
c               - Add ipca, cstrpca, xjpca and wpca for the parent
c                 config for types 5 and 6 (hybrid ls/ic).
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer   nsym        , ndword
c-----------------------------------------------------------------------
      real*8    dzero
c-----------------------------------------------------------------------
      parameter( nsym = 92  , dzero = 1.0D-70, ndword = 80 )
c-----------------------------------------------------------------------
      integer   i4unit      , i4eiz0
      integer   iunit       , ndprt          , ndrep      , ndlrep     ,
     &          ndlev       , ndaug          , ndt        , ndsc
      integer   iz          , iz0            , iz1        ,
     &          nprnt       , nprnti         , nprntf     , inrep      ,
     &          nlvl        , nrep           , iaprs_n    ,
     &          nlrep       , ilrep          , iaprs_nl   ,
     &          nte         , ncfg           , nippy
      integer   indx
      integer   i           , j              ,
     &          ipi         , ipf            ,
     &          iform       , is
      integer   ind1        , ind2           , istart     , istop
c-----------------------------------------------------------------------
      integer   ipa(ndprt)  , ispa(ndprt)    , ilpa(ndprt)
      integer   ia(ndlev)   , isa(ndlev)     , ila(ndlev)
      integer   ip(ndlev)   , ipca(ndprt)
      integer   iscp(ndsc)  , iscn(ndsc)     , iscl(ndsc)
      integer   irepa_n(ndrep), nrepa(ndrep)   , ipaug_n(ndaug,2)
      integer   iprti(ndprt)
      integer   iprtf(ndprt,ndprt)
      integer   nsysf(ndprt,ndprt)
      integer   isys(ndprt,ndprt,2), ispsys(ndprt,ndprt,2)
      integer   irepa_nl(ndlrep)   , nlrepa_n(ndlrep)     ,
     &          nlrepa_l(ndlrep)   , ipaug_nl(ndaug,2)
c-----------------------------------------------------------------------
      real*8    bwnp               , bwnr
c-----------------------------------------------------------------------
      real*8    xjpa(ndprt)  , wpa(ndprt)
      real*8    xjpca(ndprt) , wpca(ndprt)
      real*8    xja(ndlev)   , wa(ndlev)
      real*8    tea(ndt)
      real*8    auga_nl(ndlrep,ndaug) , auga_n(ndrep,ndaug)
      real*8    diel_res(ndlev,ndprt,ndt)
      real*8    diel_nl(ndlrep,ndprt,ndprt,2,ndt)
      real*8    diel_n(ndrep,ndprt,ndprt,2,ndt)
      real*8    diel_sum(ndprt,ndprt,2,ndt)
      real*8    diel_tot(ndprt,ndt)
      real*8    auga_res(ndlev,ndprt)
      real*8    fsc(ndsc,2) , wnsc(ndsc,2)
c-----------------------------------------------------------------------
      character seq*2       , esym*2         , ctype*2  , xfesym*2
      character c80*80      , c128*128       , cline*80 , buffer*128
      character cbreak*1
c-----------------------------------------------------------------------
      character cstrpa(ndprt)*(*) , cstrpca(ndprt)*(*) ,
     &          cstrga(ndlev)*(*)
      character caprs_n(ndaug)*10
      character caprs_nl(ndaug)*10
      character cspsys(ndprt,ndprt,2)*30
      character ckey(3)*6       , cans(3)*4
c-----------------------------------------------------------------------
      logical   ldr_res     , ldr_nl         , ldr_n      , l_hybrid
c-----------------------------------------------------------------------
      logical   lauga_res(ndlev,ndprt)
      logical   lauga_n(ndrep,ndaug)
      logical   lauga_nl(ndlrep,ndaug)
      logical   ldiel_res(ndlev,ndprt)
      logical   ldiel_nl(ndlrep,ndprt,ndprt,2)
      logical   ldiel_n(ndrep,ndprt,ndprt,2)
      logical   ldiel_sum(ndprt,ndprt,2)
      logical   ldiel_tot(ndprt)
c-----------------------------------------------------------------------
      data      ckey/'seq','nucchg','type'/
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c  dimension check
c-----------------------------------------------------------------------

      if(ndword.lt.max0(ndt,ndaug)) then
          write(i4unit(-1),1001)'ndword =',ndword,'  < max(ndt,ndaug)'
          write(i4unit(-1),1010)
          stop
      endif

c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------

      ldr_res = .false.
      ldr_nl  = .false.
      ldr_n   = .false.

      nlrep = 0
      nrep  = 0

      nprnti = 0
      nprntf = 0
      nlvl   = 0
      ncfg   = 0
      nippy  = 0

c-----------------------------------------------------------------------
c zero arrays
c-----------------------------------------------------------------------

      do i=1,ndlev
        do ipi=1,ndprt
          auga_res(i,ipi)=0.0d0
          lauga_res(i,ipi)=.false.
          do j=1,ndt
            diel_res(i,ipi,j)=0.0d0
            ldiel_res(i,ipi)=.false.
          enddo
        enddo
      enddo

      do ipi=1,ndprt
        ldiel_tot(ipi)=.false.
        do j=1,ndt
          diel_tot(ipi,j)=0.0d0
        enddo
        do ipf=1,ndprt
          do is=1,2
            ldiel_sum(ipi,ipf,is)=.false.
            do ilrep=1,ndlrep
              ldiel_nl(ilrep,ipi,ipf,is)=.false.
              do j=1,ndt
                diel_nl(ilrep,ipi,ipf,is,ndt)=0.0d0
              enddo
            enddo
            do inrep=1,ndrep
              ldiel_n(inrep,ipi,ipf,is)=.false.
              do j=1,ndt
                diel_n(inrep,ipi,ipf,is,ndt)=0.0d0
              enddo
            enddo
            do j=1,ndt
              diel_sum(ipi,ipf,is,j)=dzero
            enddo
          enddo
        enddo
      enddo

      do j=1,ndaug
        do ilrep=1,ndlrep
          lauga_nl(ilrep,j)=.false.
          auga_nl(ilrep,j)=0.0d0
        enddo
        do inrep=1,ndrep
          lauga_n(inrep,j)=.false.
          auga_n(inrep,j)=0.0d0
        enddo
      enddo

c----------------------------------------------------------------------
c read adf09 structure information
c----------------------------------------------------------------------

      read(iunit,'(1a80)') cline
      call xxcase (cline , c80, 'lc')
      ind1 = index(c80,'adf')
      ind2 = index(c80,'adf09')

      if ((ind1 .gt.0) .and. (ind2.eq.0)) then
          write(i4unit(-1),1001) ' wrong adf format'
          write(i4unit(-1),1010)
          stop
      endif
      if (ind1.eq.0) then
          ind1=len(c80)
      else
          ind1=ind1-1
      endif

      call xxhkey(c80(1:ind1),ckey(1),cbreak,cans(1))
      call xxhkey(c80(1:ind1),ckey(2),cbreak,cans(2))
      call xxhkey(c80(1:ind1),ckey(3),cbreak,cans(3))

      call xxslen(cans(1),istart,istop)
      seq=' '
      if(((istop-istart).eq.1).and.(cans(1)(istop:istop).ne."'"))
     &                        then
          seq(1:1)=cans(1)(istop:istop)
      elseif ((istop-istart).ge.2)then
          seq(1:istop-istart-1)=cans(1)(istart+1:istop-1)
      else
          write(i4unit(-1),1001) ' iso-sequence key/value missing'
          write(i4unit(-1),1010)
         stop
      endif

      call xxslen(cans(2),istart,istop)
      if (istart.ge.1)then
          read(cans(2)(istart:istop),*)iz0
      else
          write(i4unit(-1),1001) ' nuclear charge key/value missing'
          write(i4unit(-1),1010)
         stop
      endif

      call xxslen(cans(3),istart,istop)
      if ((istop-istart).eq.3)then
          ctype=cans(3)(istart+1:istop-1)
          if(ctype .eq. 'ca') then
              iform = 11
          elseif(ctype .eq. 'ls') then
              iform = 9
          elseif(ctype .eq. 'ic') then
              iform = 10
          endif
      else
          if(index(c80,'/ca/').gt.0) then
              iform = 4
              ctype='ca'
          elseif (index(c80,'/ls/').gt.0) then
              iform = 2
              ctype='ls'
          elseif (index(c80,'/ic/').gt.0) then
              iform = 3
              ctype='ic'
          else
              iform = 1
              ctype='ls'
          endif
      endif


c----------------------------------------------------------------------
c check if iz0 is in range
c----------------------------------------------------------------------

      if ((iz0.le.0).or.(iz0.gt.nsym)) then
         write(i4unit(-1),1001)'nuclear charge',iz0,' out of range'
         write(i4unit(-1),1010)
         stop
      endif

      esym=xfesym(iz0)
      iz=iz0-i4eiz0(seq)
      iz1=iz+1

c----------------------------------------------------------------------
c determine whether file is hybrid or not and adjust iform
c----------------------------------------------------------------------

      l_hybrid = .false.

   10 read(iunit,'(a)', end=15) c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'straddling')
      if(indx.le.0) then
          go to 10
      else
          l_hybrid = .true.
          iform = iform + 3
      endif
   15 continue

      rewind(iunit)

c----------------------------------------------------------------------
c call appropriate routine - badnell 93, ic/ls/ca 00 and hybrid ic/ca
c----------------------------------------------------------------------

      if (iform.eq.1) then

         call xx09_1  ( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                  ndlev    , ndaug    , ndt      ,
     &                  iform    ,
     &                  nprnt    , nprnti   , nprntf   , bwnp     ,
     &                  ipa      , cstrpa   , ispa     , ilpa     ,
     &                  xjpa     , wpa      ,
     &                  nlvl     , bwnr     ,
     &                  ia       , ip       , cstrga   , isa      ,
     &                  ila      , xja      , wa       ,
     &                  auga_res , lauga_res,
     &                  nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                  irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                  lauga_nl ,
     &                  nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                  irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                  iprti    ,
     &                  diel_res , ldiel_res,
     &                  iprtf    ,
     &                  nsysf    , isys     , ispsys   , cspsys   ,
     &                  diel_n   , ldiel_n  ,
     &                  diel_sum , ldiel_sum, diel_tot ,
     &                  nte      , tea
     &                )

      elseif (iform.eq.2.or.iform.eq.3.or.iform.eq.4) then

         call xx09_234( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                  ndlev    , ndaug    , ndt      ,
     &                  ctype    , iform    ,
     &                  nprnt    , nprnti   , nprntf   , bwnp     ,
     &                  ipa      , cstrpa   , ispa     , ilpa     ,
     &                  xjpa     , wpa      ,
     &                  nlvl     , bwnr     ,
     &                  ia       , ip       , cstrga   , isa      ,
     &                  ila      , xja      , wa       ,
     &                  auga_res , lauga_res,
     &                  nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                  irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                  lauga_nl ,
     &                  nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                  irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                  iprti    ,
     &                  diel_res , ldiel_res,
     &                  iprtf    ,
     &                  nsysf    , isys     , ispsys   , cspsys   ,
     &                  diel_nl  , ldiel_nl , diel_n   , ldiel_n  ,
     &                  diel_sum , ldiel_sum, diel_tot ,
     &                  nte      , tea
     &                )

      elseif (iform.eq.5.or.iform.eq.6.or.iform.eq.7) then

         call xx09_567( iunit    , ndprt    , ndrep    , ndlrep   ,
     &                  ndlev    , ndaug    , ndt      , ndsc     ,
     &                  ctype    , iform    ,
     &                  nprnt    , nprnti   , nprntf   ,
     &                  bwnp     ,
     &                  ipa      , cstrpa   , ispa     , ilpa     ,
     &                  xjpa     , wpa      ,
     &                  ipca     , cstrpca  , xjpca    , wpca     ,
     &                  ncfg     , nippy    , bwnr     ,
     &                  ia       , ip       , cstrga   , isa      ,
     &                  xja      , wa       ,
     &                  iscp     , iscn     , iscl     ,
     &                  fsc      , wnsc     ,
     &                  auga_res , lauga_res,
     &                  nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                  irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                  lauga_nl ,
     &                  nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                  irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                  iprti    ,
     &                  diel_res , ldiel_res,
     &                  iprtf    ,
     &                  nsysf    , isys     , ispsys   , cspsys   ,
     &                  diel_nl  , ldiel_nl , diel_n   , ldiel_n  ,
     &                  diel_sum , ldiel_sum, diel_tot ,
     &                  nte      , tea
     &                )

      else

         write(i4unit(-1),1000)' unsupported format'
         write(i4unit(-1),1010)
         stop

      endif

c-----------------------------------------------------------------------
 1000 format(1x,31('*'),' xxdata_09 error ',30('*')//
     &       1x,'fault in input data file: ',a)
 1001 format(1x,31('*'),' xxdata_09 error ',30('*')//
     &       1x,'fault in input data file: ',a,i5,a)
 1010 format(/1x,29('*'),' program terminated ',29('*'))
c-----------------------------------------------------------------------


      end
