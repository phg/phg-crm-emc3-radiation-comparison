       subroutine xxdata_42( fname  ,
     &                    ndtem  , ndden   , ndwvl  , ndpix   , ndmet ,
     &                    dsnin  , dsnexp  , dsnflt ,
     &                    dsnpec , dsnfpec , dsnplt , dsnpltf ,
     &                    celem  , iz0     , iz1    , bwno    ,
     &                    lnormr , nmetr   , imetrr ,
     &                    lioselr, lhselr  , lrselr , liselr  ,
     &                    lnselr , lpselr  , zeffr  ,
     &                    lmetrr , ltsclr  , ldsclr , lbrdir  ,
     &                    numter , numdensr, numwvlr,
     &                    tine   , tinp    , tinh   , dine    , dinp  ,
     &                    npix   , wvmin   , wvmax  , aminr
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 subroutine: xxdata_42 *******************
C
C  Purpose:  To fetch data from an adas810 driver set (adf42).
C
C  Calling program: ADAS810
C
C
C  Subroutine:
C
C  input : (C*80) fname   = Name of adf42 driver file
C  input : (I*4)  ndtem   = maximum number of electron temperatures
C  input : (I*4)  ndden   = maximum number of electron tdensities
C  input : (I*4)  ndwvl   = maximum number of wavelength intervals
C  input : (I*4)  ndpix   = maximum number of pixels per wvln. interval
C  input : (I*4)  ndmet   = maximum number of metastables
C
C  output: (C*80) dsnin   = input adf04 file for population analysis
C  output: (C*80) dsnexp  = input adf18/a17_p208 expansion file
C  output: (C*80) dsnflt  = input adf35 filter file for power data
C  output: (C*80) dsnpec  = output adf15 file for traditional pec data
C  output: (C*80) dsnfpec = output adf40 file for feature pec data
C  output: (C*80) dsnplt  = output adf11 file for line power
C  output: (C*80) dsnpltf = output adf11 file for filtered line power
C
C  output: (C*2)  celem   = element symbol.
C  output: (I*4)  iz      =  recombined ion charge read
C  output: (I*4)  iz0     =         nuclear charge read
C  output: (I*4)  iz1     = recombining ion charge read
C                           (note: iz1 should equal iz+1)
C  output: (R*8)  bwno    = ionisation potential (cm-1) of lowest parent
C
C  output: (I*4)  nte     = number of electron temperatures
C  output: (R*8)  tea()   = electron temperatures (k)
C  output: (I*4)  ndens   = number of electron densities
C  output: (R*8)  densa() = electron densities (cm-3)
C
C  output: (I*4)  nwvl    = wvaelength intervals
C  output: (R*8)  npix()  = numbre of pixels assigned to wavelength interval
C  output: (R*8)  wvmin() = lower limit of wavelength interval (ang)
C  output: (R*8)  wvmax() = upper limit of wavelength interval (ang)
C
C  output: (L*4)  lmetr   = .TRUE.  => identify and resolve metastables
C                         = .FALSE. => do not resolve metastables
C  output: (L*4)  ltscl   = .TRUE.  => input temperatures are z-scaled
C                         = .FALSE. => input temperatures not z-scaled
C  output: (L*4)  ldscl   = .TRUE.  => input densities are z-scaled
C                         = .FALSE. => input densities not z-scaled
C  output: (L*4)  lbrdi   = .TRUE.  => impose ion temperature broadening
C                         = .FALSE. => input densities not z-scaled
C
C
C ROUTINES:
C          Routine    Source    Brief Description
C          -------------------------------------------------------------
C          i4unit     ADAS      Fetch unit number for output of messages
C          xxflnm     ADAS      Checks for & replaces the ADASCENT environment 
C                               variable.
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    28-02-2003
C
C NOTES: Copied from haddat.for
C 
C
C VERSION  : 1.1                          
C DATE     : 28-05-2003
C MODIFIED : Martin O'Mullane
C              - First version
C
C VERSION : 1.2                         
C DATE    : 24-04-2019
C MODIFIED: Martin O'Mullane
C            - Returned filename lengths are defined by argument length.
C            - Increase fortran namelist filenames to 132 characters.
C
C-----------------------------------------------------------------------
      integer   iunit
C-----------------------------------------------------------------------
      parameter (iunit = 42)
C-----------------------------------------------------------------------
      integer    ndtem     , ndden      , ndmet     , ndwvl
      integer    iz0       , iz1        , nmet      , ndpix     ,
     &           numte     , numdens    , numwvl    ,
     &           numter    , numdensr   , numwvlr   , nmetr
      integer    z0        , z1         ,
     &           numtion   , numth      , numdion
      integer    i4unit    , i
C-----------------------------------------------------------------------
      real*8     bwno      , zeff       , zeffr
      real*8     ip        , amin       , aminr
C-----------------------------------------------------------------------
      character  fname*(*) , dsnin*(*)  , dsnexp*(*), dsnflt*(*) ,
     &           dsnpec*(*), dsnfpec*(*), dsnplt*(*), dsnpltf*(*),
     &           celem*2
      character  dsn04*132 , dsn18*132  , dsn35*132 ,
     &           dsn15*132 , dsn40*132  , dsn11*132 , dsn11f*132 ,
     &           element*2
C-----------------------------------------------------------------------
      logical    liosel    , lhsel      , lrsel     , lisel      ,
     &           lnsel     , lpsel      , lmetr     , ltscl      ,
     &           ldscl     , lbrdi      , lnorm
      logical    lioselr   , lhselr     , lrselr    , liselr     ,
     &           lnselr    , lpselr     , lmetrr    , ltsclr     ,
     &           ldsclr    , lbrdir     , lnormr
      logical    lexist    , lte        , ltion     , lth        ,
     &           ldens     , ldion      , lwvl
C-----------------------------------------------------------------------
      integer    imetr(4)  , npix(ndwvl)    , imetrr(ndmet)
C-----------------------------------------------------------------------
      real*8     tine(ndtem)  , tinp(ndtem)    , tinh(ndtem)    ,
     &           dine(ndden)  , dinp(ndden)    ,
     &           wvmin(ndwvl) , wvmax(ndwvl)
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C   User input in form of namelists
C-----------------------------------------------------------------------

      namelist/files/   dsn04   , dsn18  , dsn35   , dsn15   , dsn40 ,
     &                  dsn11   , dsn11f
      namelist/ion/     element , z0     , z1      , ip
      namelist/meta/    lnorm   , nmet   , imetr
      namelist/process/ liosel  , lhsel  , lrsel   , lisel   , lnsel ,
     &                  lpsel   , zeff
      namelist/output/  lmetr   , ltscl  , ldscl   , lbrdi   , numte ,
     &                  numtion , numth  , numdens , numdion , numwvl,
     &                  amin

C----------------------------------------------------------------------
C  Set name list defaults
C-----------------------------------------------------------------------

      dsn04    = 'NULL'
      dsn18    = 'NULL'
      dsn35    = 'NULL'
      dsn15    = 'NULL'
      dsn40    = 'NULL'
      dsn11    = 'NULL'
      dsn11f   = 'NULL'

      element  = 'X'
      z0       = -1
      z1       = -1
      ip       = 0.0D0

      lnorm    = .TRUE.
      nmet     = 1
      imetr(1) = 1
      imetr(2) = 0
      imetr(3) = 0
      imetr(4) = 0

      liosel   = .FALSE.
      lhsel    = .FALSE.
      lrsel    = .FALSE.
      lisel    = .FALSE.
      lnsel    = .FALSE.
      lpsel    = .FALSE.
      zeff     = 1.0D0

      lmetr    = .FALSE.
      ltscl    = .FALSE.
      ldscl    = .FALSE.
      lbrdi    = .FALSE.
      numte    = 0
      numtion  = 0
      numth    = 0
      numdens  = 0
      numdion  = 0
      numwvl   = 0
      amin     = 0.0D0


      do i = 1, ndtem
        tine(i) = 0.0D0
        tinp(i) = 0.0D0
        tinh(i) = 0.0D0
      end do

      do i = 1, ndden
        dine(i) = 0.0D0
        dinp(i) = 0.0D0
      end do


C-----------------------------------------------------------------------
C Read adf42 input file
C-----------------------------------------------------------------------

      inquire(FILE=fname, EXIST=lexist)

      if (lexist) then
         open(unit=iunit, file=fname, status='old')
      else
         write(i4unit(-1),1001)fname, 'Does not exist'
         stop
      endif

      read(iunit,files)
      read(iunit,ion)
      read(iunit,meta)
      read(iunit,process)
      read(iunit,output)

C Checks for size consistency

      lte   = .FALSE.
      ltion = .FALSE.
      lth   = .FALSE.
      ldens = .FALSE.
      ldion = .FALSE.
      lwvl  = .FALSE.

      if (numte.GT.0) then
         lte = .TRUE.
      else
         write(i4unit(-1),1001)'Te must be given', ' '
         stop
      endif

      if (numtion.GT.0) then
         ltion = .TRUE.
         if (numte.NE.numtion) then
           write(i4unit(-1),1001)'numte same as numtion', ' '
           stop
         endif
      endif

      if (numth.GT.0) then
         lth = .TRUE.
         if (numte.NE.numth) then
           write(i4unit(-1),1001)'numte same as numth', ' '
           stop
         endif
      endif

      if (numdens.GT.0) then
         ldens = .TRUE.
      else
         write(i4unit(-1),1001)'Density must be given', ' '
         stop
      endif

      if (numdion.GT.0) then
         ldion = .TRUE.
         if (numte.NE.numdion) then
           write(i4unit(-1),1001)'numdens same as numdion', ' '
           stop
         endif
      endif

      if (numwvl.GT.0) lwvl = .TRUE.


C Read in data, ignoring blank lines - assume sequence is preserved


      if (lte)   read(iunit, *)(tine(i), i=1, numte)
      if (ltion) read(iunit, *)(tinp(i), i=1, numtion)
      if (lth)   read(iunit, *)(tinh(i), i=1, numth)
      if (ldens) read(iunit, *)(dine(i), i=1, numdens)
      if (ldion) read(iunit, *)(dinp(i), i=1, numdion)
      if (lwvl)  read(iunit, *)(npix(i),wvmin(i),wvmax(i),i=1,numwvl)

C-----------------------------------------------------------------------
C Return the data because of different names in namelist
C-----------------------------------------------------------------------

       call xxflnm(dsn04,dsnin,lexist)
         
       dsnexp   = dsn18
       dsnflt   = dsn35
       dsnpec   = dsn15
       dsnfpec  = dsn40
       dsnplt   = dsn11
       dsnpltf  = dsn11f
       celem    = element
       iz0      = z0
       iz1      = z1
       bwno     = ip

       zeffr    = zeff

       lnormr   = lnorm
       nmetr    = nmet
       do i = 1, ndmet
         imetrr(i) = imetr(i)
       end do

       lioselr  = liosel
       lhselr   = lhsel
       lrselr   = lrsel
       liselr   = lisel
       lnselr   = lnsel
       lpselr   = lpsel
       lmetrr   = lmetr
       ltsclr   = ltscl
       ldsclr   = ldscl
       lbrdir   = lbrdi

       numter   = numte
       numdensr = numdens
       numwvlr  = numwvl
       aminr    = amin

C-----------------------------------------------------------------------
C Finally check on npix validity
C-----------------------------------------------------------------------

       do i = 1, numwvl
          if (npix(i).GT.ndpix) then
             write(i4unit(-1),1001)'Too many pixels', ' '
             stop
          endif
       end do

C-----------------------------------------------------------------------
 1001 format(1x,32('*'),' xxdata_42 error ',33('*')//
     &       1x,'Fault in input data file: ',a, 5x, a, /,/,1x, 
     &          30('*'),' Program terminated ',30('*'))
C-----------------------------------------------------------------------

 999  continue

      return

      end
