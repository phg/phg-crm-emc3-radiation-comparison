C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adaslib/xxdata/xxdata_03.for,v 1.2 2004/07/06 15:32:39 whitefor Exp $ Date $Date: 2004/07/06 15:32:39 $
       SUBROUTINE xxdata_03( IUNIT,  IZDIMD, IGDIMD,
     &                       IZ0,    IZL,    IZU,
     &                       IZRA,   IZDA,   IZIA,    IZTA,    IZSA,
     &                       CRRCA,  NRRCA,  ISRRCA,
     &                       NZA,    KSIA,
     &                       N0RA,   V0RA,   PHFCRA,  EDSPRA,  SCLERA,
     &                       CDRCA,  NDRCA,  ISDRCA,
     &                       DEDA,   FDA,    GDA,     NNDA,    MSDA,
     &                       ITYPDA, N0DA,   NCUTA,   V0DA,    PHFCDA,
     &                       CRFCDA, EPSIJA, FIJA,    EDSPDA,  SCLEDA,
     &                       CCIOA,  NCIOSA, NCIORA,  ISCIOA,
     &                       PIOA,   AIOA,   BIOA,    CIOA,    NQIOA,
     &                       ZETAA,  EIONA,  CIA,
     &                       WGHTA,  ENERA,  CRA,
     &                       CPLTA,  NPLTA,  ISPLTA,
     &                       DEPTA,  FPTA,   GPTA,    NNPTA,   SPYLTA,
     &                       CPLSA,  NPLSA,  ISPLSA,  INFO,
     &                       DEPSA,  FPSA,   GPSA,    NNPSA,   SPYLSA,
     &                       LVALID
     &                     )

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8DATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT ATOMPARS DATA SET OF TYPE ADF03.
C
C  CALLING PROGRAM: ADAS408
C
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (I*4)  IZDIMD    = MAXIMUM NUMBER OF IONISATION STAGES
C  INPUT : (I*4)  IGDIMD    = MAXIMUM NUMBER OF GROUPS
C
C
C  THE OUTPUT ARRAYS ARE INDEXED
C            XXX()  = XXX(IZDIMD)         1ST DIMENSION ION STAGE
C
C            XXX(,) = XXX(IZDIMD,IGDIMD)  1ST DIMENSION ION STAGE
C                                         2ND DIMENSION GROUP
C
C
C
C  OUTPUT: (I*4)  IZ0       =  NUCLEAR CHARGE
C  OUTPUT: (I*4)  IZL       =  LOWEST INCLUDED ION
C  OUTPUT: (I*4)  IZU       =  HIGHEST INCLUDED ION
C
C  OUTPUT: (I*4)  IZRA()    =  RECOMBINING ION (RAD.  RECOM.)
C  OUTPUT: (I*4)  IZDA()    =  RECOMBINING ION (DIEL. RECOM.)
C  OUTPUT: (I*4)  IZIA()    =  IONISING ION    (COLL. IONIS.)
C  OUTPUT: (I*4)  IZTA()    =  RADIATING ION   (TOTAL LINE POWER)
C  OUTPUT: (I*4)  IZSA()    =  RADIATING ION   (SPECIFIC LINE POWER)
C
C
C
C  OUTPUT: (C*5)  CRRCA()   =  RADIATIVE RECOM. CODE
C  OUTPUT: (I*4)  NRRCA()   =    - NOT USED -
C  OUTPUT: (I*4)  ISRRCA()  =    - NOT USED -
C
C  OUTPUT: (I*4)  NZA()     = LOWEST ACCESSIBLE SHELL FOR RAD. RECOM.
C  OUTPUT: (I*4)  KSIA()    = NUMBER OF ELECTRONS IN SHELL
C
C  OUTPUT: (I*4)  N0RA()    = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR RAD. RECOM.
C  OUTPUT: (I*4)  V0RA()    = EFFECTIVE PRINCIPAL QUANTUM NUMBER
C                             FOR SHELL
C  OUTPUT: (R*8)  PHFCRA()  = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR SHELL
C  OUTPUT: (R*8)  EDSPRA()  = ENERGY ADJUSTMENT IN LOWEST SHELL
C                             RATE COEFFICIENT
C  OUTPUT: (R*8)  SCLERA()  = MULTIPLIER FOR LOWEST SHELL
C                             RATE COEFFICIENT
C
C
C
C  OUTPUT: (C*5)  CDRCA()   =  DIELECTRONIC RECOM. CODE
C  OUTPUT: (I*4)  NDRCA()   =  NUMBER OF TRANSITIONS FOLLOWING
C  OUTPUT: (I*4)  ISDRCA()  =    - NOT USED -
C
C  OUTPUT: (R*8)  DEDA(,)   = TRANSITION ENERGY (EV)
C  OUTPUT: (R*8)  FDA(,)    = OSCILLATOR STRENGTH
C  OUTPUT: (R*8)  GDA(,)    = GAUNT FACTOR
C  OUTPUT: (I*4)  NNDA(,)   = DELTA N FOR TRANSITION
C  OUTPUT: (I*4)  MSDA(,)   = MERTZ SWITCH (0=OFF, 1=ON)
C
C  OUTPUT: (I*4)  ITYPDA(,) = TYPE OF DIELECTRONIC TRANSITION
C  OUTPUT: (I*4)  N0DA(,)   = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR DIEL. RE
C  OUTPUT: (I*4)  NCUTA(,)  = CUT-OFF PRINC. QUANTUM SHELL IN
C                             GENERAL PROGRAM
C  OUTPUT: (I*4)  V0DA(,)   = EFFECTIVE PRINC. QUANTUM NUMBER
C                             FOR LOWEST ACCESS
C  OUTPUT: (R*8)  PHFCDA(,) = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR LOWEST SHELL
C  OUTPUT: (R*8)  CRFCDA(,) = ADJUSTMENT FOR BETHE CORRECTIONS
C                             IN GENERAL PROGRAM
C  OUTPUT: (R*8)  EPSIJA(,) = Z-SCALED PARENT TRANSITION ENERGY (RYD)
C  OUTPUT: (R*8)  FIJA(,)   = OSCILLATOR STRENGTH FOR TRANSITION
C  OUTPUT: (R*8)  EDSPDA(,) = ENERGY ADJUSTMENT IN BURGESS GENERAL
C                             FORMULA (RYD)
C  OUTPUT: (R*8)  SCLEDA(,) = MULTIPLIER ON BURGESS GENERAL FORMULA
C
C
C
C  OUTPUT: (C*5)  CCIOA()   =  COLLISIONAL IONIS. CODE
C  OUTPUT: (I*4)  NCIOSA()  =  NUMBER OF SHELL  VALUES FOLLOWING
C  OUTPUT: (I*4)  NCIORA()  =  NUMBER OF RESON. VALUES FOLLOWING
C  OUTPUT: (I*4)  ISCIOA()  =    - NOT USED -
C
C  OUTPUT: (R*8)  PIOA(,)   = SHELL IONISATION POTENTIAL (EV)
C  OUTPUT: (R*8)  AIOA(,)   = LOTZ PARAMETER
C  OUTPUT: (R*8)  BIOA(,)   = LOTZ PARAMETER
C  OUTPUT: (R*8)  CIOA(,)   = LOTZ PARAMETER
C  OUTPUT: (I*4)  NQIOA(,)  = EQUIVALENT ELECTRONS IN SHELL
C
C  OUTPUT: (R*8)  ZETAA(,)  = NUMBER OF EQUIVALENT ELECTRONS FOR SHELL
C  OUTPUT: (R*8)  EIONA(,)  = IONISATION ENERGY FOR SHELL (RYD)
C  OUTPUT: (R*8)  CIA(,)    = MULTIPLIER FOR BURGESS-CHIDICHIMO RATE
C                             FOR SHELL
C  OUTPUT: (R*8)  WGHTA(,)  = WEIGHTING FACTOR FOR EXCITATION TO
C                             RESONANCE
C  OUTPUT: (R*8)  ENERA(,)  = EXCITATION ENERGY FOR TRANSITION
C                             TO RESONANCE (RYD)
C  OUTPUT: (R*8)  CRA(,)    = MULTIPLIER ON EXCITATION RATE EXPRESSSION
C
C
C
C  OUTPUT: (C*5)  CPLTA()   =  TOTAL LINE POWER CODE
C  OUTPUT: (I*4)  NPLTA()   =  NUMBER OF TRANSITIONS FOLLOWING
C  OUTPUT: (I*4)  ISPLTA()  =    - NOT USED -
C
C  OUTPUT: (R*8)  DEPTA(,)  = TRANSITION ENERGY (EV)
C  OUTPUT: (R*8)  FPTA(,)   = OSCILLATOR STRENGTH
C  OUTPUT: (R*8)  GPTA(,)   = GAUNT FACTOR
C  OUTPUT: (I*4)  NNPTA(,)  = DELTA N FOR TRANSITION
C
C  OUTPUT: (R*8)  SPYLTA(,) = MULTIPLIER OF VAN REGEMORTER P
C                             FACTOR IN TOTAL POWER
C
C
C
C  OUTPUT: (C*5)  CPLSA()   = SPECIFIC LINE POWER CODE
C  OUTPUT: (I*4)  NPLSA()   =   - NOT USED -
C  OUTPUT: (I*4)  ISPLSA()  =   - NOT USED -
C  OUTPUT: (C*8)  INFO()    = WAVELENGTH OF SPECIFIC LINE FOR
C                             NAMING PURPOSES
C
C  OUTPUT: (R*8)  DEPSA(,)  = TRANSITION ENERGY (EV)
C  OUTPUT: (R*8)  FPSA(,)   = OSCILLATOR STRENGTH
C  OUTPUT: (R*8)  GPSA(,)   = GAUNT FACTOR
C  OUTPUT: (I*4)  NNPSA(,)  = DELTA N FOR TRANSITION
C
C  OUTPUT: (R*8)  SPYLSA(,) = MULTIPLIER OF VAN REGEMORTER P FACTOR
C                             IN SPECIFIC LINE POWER
C
C
C
C  OUTPUT: (L*4)  LVALID  = .TRUE. DATA SET READ AND APPEARS VALID
C                         = .FALSE. ERROR DETECTED IN READING DATA SET
C
C
C
C  PROGRAM:
C          (C*50) ERRMSG()= ERROR MESSAGE STRING
C          (I*4)  IDUM    = PROGRAM USE
C
C
C  ROUTINES:
C          ROUTINE   SOURCE   DESCRIPTION
C          -------------------------------------------------------
C          I4UNIT    ADAS     FETCH UNIT NUMBER FOR MESSAGE OUTPUT
C
C
C  AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C  DATE:    10/05/94
C
C
C NOTES: Copied from d8data.for
C 
C
C VERSION  : 1.1                          
C DATE     : 19-07-2003
C MODIFIED : Martin O'Mullane
C              - First version
C
C VERSION  : 1.2                          
C DATE     : 16-01-2004
C MODIFIED : Martin O'Mullane
C              - V0 in type B radiative recombination is real, not
C                an integer. 
C
C-----------------------------------------------------------------------
C     GENERAL VARIABLES

      INTEGER   IUNIT, IZDIMD, IGDIMD
 
      INTEGER   IZ0, IZL, IZU, IDUM
 
      INTEGER   IZRA(IZDIMD) , IZDA(IZDIMD) , IZIA(IZDIMD) ,
     &          IZTA(IZDIMD) , IZSA(IZDIMD)
 
      CHARACTER ADFID*5 , STRG1*5 , ERRMSG(6)*50
 
      LOGICAL   LVALID
 
      INTEGER   I4UNIT
 
      INTEGER   NUMZ, IK, IDRC, ICIOS, ICIOR, IPLT, IPLS
 
C-----------------------------------------------------------------------
C     RADIATIVE RECOMBINATION

      CHARACTER*5  CRRCA(IZDIMD)
      INTEGER      NRRCA(IZDIMD) , ISRRCA(IZDIMD)
 
      INTEGER      NZA(IZDIMD) , KSIA(IZDIMD)
 
      INTEGER      N0RA(IZDIMD)
      REAL*8       V0RA(IZDIMD) , PHFCRA(IZDIMD) , EDSPRA(IZDIMD),
     &             SCLERA(IZDIMD)
 
C-----------------------------------------------------------------------
C     DIELECTRONIC RECOMBINATION

      CHARACTER*5  CDRCA(IZDIMD)
      INTEGER      NDRCA(IZDIMD)  , ISDRCA(IZDIMD)
 
      REAL*8       DEDA(IZDIMD,IGDIMD) , FDA(IZDIMD,IGDIMD),
     &             GDA(IZDIMD,IGDIMD)
      INTEGER      NNDA(IZDIMD,IGDIMD) , MSDA(IZDIMD,IGDIMD)
 
      INTEGER      ITYPDA(IZDIMD,IGDIMD), N0DA(IZDIMD,IGDIMD),
     &             NCUTA(IZDIMD,IGDIMD)
      REAL*8       V0DA(IZDIMD,IGDIMD) ,
     &             PHFCDA(IZDIMD,IGDIMD), CRFCDA(IZDIMD,IGDIMD),
     &             EPSIJA(IZDIMD,IGDIMD), FIJA(IZDIMD,IGDIMD) ,
     &             EDSPDA(IZDIMD,IGDIMD), SCLEDA(IZDIMD,IGDIMD)
 
C-----------------------------------------------------------------------
C     IONISATION

      CHARACTER*5  CCIOA(IZDIMD)
      INTEGER      NCIOSA(IZDIMD), NCIORA(IZDIMD), ISCIOA(IZDIMD) ,
     &             NQIOA(IZDIMD,IGDIMD)
 
      REAL*8       PIOA(IZDIMD,IGDIMD), AIOA(IZDIMD,IGDIMD),
     &             BIOA(IZDIMD,IGDIMD), CIOA(IZDIMD,IGDIMD)
 
      REAL*8       ZETAA(IZDIMD,IGDIMD),EIONA(IZDIMD,IGDIMD),
     &             CIA(IZDIMD,IGDIMD), WGHTA(IZDIMD,IGDIMD),
     &             ENERA(IZDIMD,IGDIMD),CRA(IZDIMD,IGDIMD)
 
C-----------------------------------------------------------------------
C     TOTAL LINE POWER

      CHARACTER*5  CPLTA(IZDIMD)
      INTEGER      NPLTA(IZDIMD) , ISPLTA(IZDIMD)
 
      REAL*8       DEPTA(IZDIMD,IGDIMD) , FPTA(IZDIMD,IGDIMD)
 
      REAL*8       GPTA(IZDIMD,IGDIMD)
      INTEGER      NNPTA(IZDIMD,IGDIMD)
 
      REAL*8       SPYLTA(IZDIMD,IGDIMD)
 
C-----------------------------------------------------------------------
C     SPECIFIC LINE POWER

      CHARACTER*5  CPLSA(IZDIMD)
      INTEGER      NPLSA(IZDIMD) , ISPLSA(IZDIMD)
      CHARACTER*8  INFO(IZDIMD)
 
      REAL*8       DEPSA(IZDIMD,IGDIMD) , FPSA(IZDIMD,IGDIMD)
 
      REAL*8       GPSA(IZDIMD,IGDIMD)
      INTEGER      NNPSA(IZDIMD,IGDIMD)
 
      REAL*8       SPYLSA(IZDIMD,IGDIMD)
 
C-----------------------------------------------------------------------
 
      DATA  ERRMSG / 'ATOMIC PARAMETER FILE IS NOT ADF03 FORMAT',
     &               'RADIATIVE RECOMBINATION TYPE NOT AVAILABLE',
     &               'DIELECTRONIC RECOMBINATION TYPE NOT AVAILABLE',
     &               'IONISATION TYPE NOT AVAILABLE',
     &               'TOTAL LINE POWER TYPE NOT AVAILABLE',
     &               'SPECIFIC LINE POWER TYPE NOT AVAILABLE'/
     &
C-----------------------------------------------------------------------
      LVALID = .FALSE.
C-----------------------------------------------------------------------
 
 
 
 

C  FETCH ATOMIC PARAMETERS FROM ATOMPARS FILES

 
      READ (IUNIT,1005,ERR=999) IZ0,IZL,IZU,IDUM,IDUM,IDUM,ADFID
      IF (ADFID.NE.'ADF03') THEN
         WRITE(I4UNIT(-1),2005)ERRMSG(1)
         RETURN
      ENDIF
 
      NUMZ = IZU - IZL + 1
 
      DO IK=1,NUMZ
      
        READ(IUNIT,1000,ERR=999)STRG1
        READ (IUNIT,1006,ERR=999)
     &        IZRA(IK),IZDA(IK), IZIA(IK),IZTA(IK),IZSA(IK)
 
        READ(IUNIT,1007,ERR=999)CRRCA(IK),NRRCA(IK),ISRRCA(IK)
        IF (CRRCA(IK).EQ.'RRC#A') THEN
           READ (IUNIT,1008,ERR=999) NZA(IK),KSIA(IK)
        ELSEIF (CRRCA(IK).EQ.'RRC#B') THEN
           READ(IUNIT,1009,ERR=999)
     &          N0RA(IK),V0RA(IK),PHFCRA(IK),EDSPRA(IK), SCLERA(IK)
        ELSE
           WRITE(I4UNIT(-1),2005)ERRMSG(2)
           RETURN
        ENDIF
 
        READ(IUNIT,1007,ERR=999)CDRCA(IK),NDRCA(IK),ISDRCA(IK)
        IF (CDRCA(IK).EQ.'DRC#A') THEN
           DO IDRC = 1,NDRCA(IK)
              READ(IUNIT,1010,ERR=999)
     &             DEDA(IK,IDRC),FDA(IK,IDRC),GDA(IK,IDRC),
     &             NNDA(IK,IDRC),MSDA(IK,IDRC)
           END DO
        ELSEIF (CDRCA(IK).EQ.'DRC#B') THEN
           DO IDRC=1,NDRCA(IK)
             READ(IUNIT,1011,ERR=999)
     &           ITYPDA(IK,IDRC),N0DA(IK,IDRC),NCUTA(IK,IDRC),
     &           V0DA(IK,IDRC),PHFCDA(IK,IDRC),CRFCDA(IK,IDRC),
     &           EPSIJA(IK,IDRC),FIJA(IK,IDRC),EDSPDA(IK,IDRC),
     &           SCLEDA(IK,IDRC)
           END DO
        ELSE
           WRITE(I4UNIT(-1),2005)ERRMSG(3)
           RETURN
        ENDIF
 
        READ(IUNIT,1012,ERR=999)
     &      CCIOA(IK),NCIOSA(IK),NCIORA(IK),ISCIOA(IK)
        IF (CCIOA(IK).EQ.'CIO#A') THEN
           DO ICIOS = 1,NCIOSA(IK)
             READ (IUNIT,1013,ERR=999)
     &             PIOA(IK,ICIOS),AIOA(IK,ICIOS),
     &             BIOA(IK,ICIOS),CIOA(IK,ICIOS),
     &             NQIOA(IK,ICIOS)
           END DO
        ELSEIF (CCIOA(IK).EQ.'CIO#B')THEN
           DO ICIOS=1,NCIOSA(IK)
             READ(IUNIT,1014,ERR=999) ZETAA(IK,ICIOS),EIONA(IK,ICIOS),
     &                                CIA(IK,ICIOS)
           END DO
           IF (NCIORA(IK).GE.1) THEN
              DO ICIOR=1,NCIORA(IK)
                 READ(IUNIT,1014,ERR=999)
     &                WGHTA(IK,ICIOR),ENERA(IK,ICIOR), CRA(IK,ICIOR)
              END DO
           ENDIF
        ELSE
           WRITE(I4UNIT(-1),2005)ERRMSG(4)
           RETURN
        ENDIF
 
        READ(IUNIT,1007,ERR=999)CPLTA(IK),NPLTA(IK),ISPLTA(IK)
        IF (CPLTA(IK).EQ.'PLT#A') THEN
           DO IPLT = 1,NPLTA(IK)
             READ(IUNIT,1015,ERR=999) DEPTA(IK,IPLT),FPTA(IK,IPLT),
     &                                GPTA(IK,IPLT),NNPTA(IK,IPLT)
           END DO
        ELSEIF (CPLTA(IK).EQ.'PLT#B') THEN
           DO IPLT = 1,NPLTA(IK)
             READ(IUNIT,1016,ERR=999) DEPTA(IK,IPLT),FPTA(IK,IPLT),
     &                                SPYLTA(IK,IPLT)
           END DO
        ELSE
           WRITE(I4UNIT(-1),2005)ERRMSG(5)
           RETURN
        ENDIF
 
        READ(IUNIT,1007,ERR=999)
     &       CPLSA(IK),NPLSA(IK),ISPLSA(IK),INFO(IK)
        IF (CPLSA(IK).EQ.'PLS#A') THEN
           DO IPLS = 1,NPLSA(IK)
             READ(IUNIT,1015,ERR=999) DEPSA(IK,IPLS),FPSA(IK,IPLS),
     &                                GPSA(IK,IPLS),NNPSA(IK,IPLS)
            END DO
        ELSEIF (CPLSA(IK).EQ.'PLS#B') THEN
           DO IPLS = 1,NPLSA(IK)
             READ(IUNIT,1016,ERR=999) DEPSA(IK,IPLS),FPSA(IK,IPLS),
     &                                SPYLSA(IK,IPLS)
           END DO
        ELSE
           WRITE(I4UNIT(-1),2005)ERRMSG(6)
           RETURN
        ENDIF
      
      
      END DO
 
 
      LVALID = .TRUE.
      RETURN
 
  999 WRITE(I4UNIT(-1),2009)
      WRITE(I4UNIT(-1),2002)
      
      RETURN
 
 
C-----------------------------------------------------------------------
 
 1000 FORMAT(1A5)
 1005 FORMAT(3I5,10X,3I5,35X,1A5)
 1006 FORMAT(5I5)
 1007 FORMAT(5X,1A5,2I5,2X,1A8)
 1008 FORMAT(10X,2I5)
 1009 FORMAT(10X,I5,4F10.3)
 1010 FORMAT(10X,3F10.3,2I5)
 1011 FORMAT(10X,3I5,3F10.3/15X,4F10.3)
 1012 FORMAT(5X,1A5,3I5)
 1013 FORMAT(10X,4F10.3,I5)
 1014 FORMAT(10X,3F10.3)
 1015 FORMAT(10X,3F10.3,I5)
 1016 FORMAT(10X,4F10.3)
 
 
 2002 FORMAT(/1X,28('*'),' LVALID FLAG SET FALSE ',27('*'))
 2005 FORMAT(1X,'*** xxdata_03  ERROR - ',A50)
 2009 FORMAT(1X,31('*'),' xxdata_03 ERROR ',31('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,A)
 
C-----------------------------------------------------------------------
 
      END
