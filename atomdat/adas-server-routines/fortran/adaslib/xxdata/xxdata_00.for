       subroutine xxdata_00( iunit  , dsname ,
     &                       izdimd , iodimd , imdimd ,
     &                       esym   , iz0    , wf     ,
     &                       bwnoa  , eeva   ,
     &                       iorba  , na     , la     , iqa      ,
     &                       cstr_std        ,
     &                       isa    , ila    , xja    ,
     &                       imeta  , imeta_actual    , eevma    ,
     &                       iorbma , nma    , lma    , iqma     ,
     &                       cstrm_std       ,
     &                       isma   , ilma   , xjma   ,
     &                       lexist , lresol , lquno  , coupling
     &                     )
c
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xxdata_00 ******************
c
c  purpose:  to fetch data from an adf00 data set and detect its main
c            characteristics.
c
c                1. element symbol and nuclear charge
c                2. ionisation potentials (cm-1 and eV)
c                3. shell occupancies in the normal collating order
c                4. outer quantum numbers of ground configutations
c                5. metastable excitation energies & parameters as above
c
c  calling program: various
c
c           ionisation potential: eV
c           configuration:        standard form nlq (incl. integers
c                                 for n>9 and q>9 , lower case
c                                 letter for l and space separators)
c           outer quantum nos.    multiplicity, L and (wt.-1)/2 or J
c
c  subroutine:
c
c  input : (i*4)   iunit     = unit to which input file is allocated
c  input : (c*(*)) dsname    = name of opened data set on iunit
c  input : (i*4)   izdimd    = maximum nuclear charge
c  input : (i*4)   iodimd    = max. number of orbitals
c  input : (i*4)   imdimd    = max. number of metastables
c
c  output: (c*2)   esym      = element symbol.
c  output: (i*4)   iz0       = nuclear charge read
c  output: (r*8)   wf        = work function for ls coupled data (eV)
c  output: (r*8)   bwnoa()   = ionisation potential (eV) of each stage
c                              1st dim: index = ion charge +1
c  output: (r*8)   eeva()    = ionisation potential (eV) of each stage
c                              1st dim: index = ion charge +1
c  output: (i*4)   iorba()   = number of orbital shells in configuration
c                              1st dim: index = ion charge +1
c  output: (i*4)   na(,)     = principal quantum number of shell
c                              1st dim: index = ion charge +1
c                              2nd dim: shell index
c  output: (i*4)   la(,)     = orbital ang. momentum qu. no. of shell
c                              1st dim: index = ion charge +1
c                              2nd dim: shell index
c  output: (i*4)   iqa(,)    = occupancy. of shell
c                              1st dim: index = ion charge +1
c                              2nd dim: shell index
c
c  output: (c*(*)) cstr_std()= configuration string in standard form
c                              1st dim: index = nuclear charge +1
c
c  output: (i*4)   isa()     = multiplicity of ground level
c                              1st dim: index = ion charge +1
c  output: (i*4)   ila()     = total orbital ang. mom. of  of ground level
c                              1st dim: index = ion charge +1
c  output: (r*8)   xja()     = total ang. mom. (or [stat. wt.-1/2])
c                              1st dim: index = ion charge +1
c  output: (i*4)   imeta()   = theor. no. of metastables for each ionis.stage
c                              1st dim: index = ion charge +1
c  output: (i*4)   imeta_actual()= actual. no. of metastables found for each
c                              ionis.stage
c                              1st dim: index = ion charge +1
c  output: (r*8)   eevma(,)  = excitation energy (eV) of each metastable
c                              1st dim: index = ion charge +1
c                              2nd dim: index = metastable index
c  output: (i*4)   iorbma(,) = number of orbital shells in metas. config.
c                              1st dim: index = ion charge +1
c                              2nd dim: index = metastable index
c  output: (i*4)   nma(,,)   = principal quantum number of metas.shell
c                              1st dim: index = ion charge +1
c                              2nd dim: shell index
c                              3rd dim: index = metastable index
c  output: (i*4)   lma(,,)   = orbital ang. mom. qu. no. of metas. shell
c                              1st dim: index = ion charge +1
c                              2nd dim: shell index
c                              3rd dim: index = metastable index
c  output: (i*4)   iqma(,,)  = occupancy. of metas. shell
c                              1st dim: index = ion charge +1
c                              2nd dim: shell index
c                              3rd dim: index = metastable index
c
c  output: (c*(*)) cstrm_std(,)=meta. config. string in standard form
c                              1st dim: index = ion charge +1
c
c                              2nd dim: index = metastable index
c  output: (i*4)   isma(,)   = multiplicity of metastable level
c                              1st dim: index = ion charge +1
c                              2nd dim: index = metastable index
c  output: (i*4)   ilma(,)   = total orbital ang. mom. of metastable level
c                              1st dim: index = ion charge +1
c                              2nd dim: index = metastable index
c  output: (r*8)   xjma(,)   = total ang. mom. (or [stat. wt.-1/2]) of
c                              metastable level
c                              1st dim: index = ion charge +1
c                              2nd dim: index = metastable index
c
c  output: (l*4)   lexist    = .true.  => ionisation potential present
c                            = .false. => not present
c  output: (l*4)   lresol    = .true.  => metastable resolved adf00 file
c                            = .false. => not metastable resolved adf00
c  output: (l*4)   lquno     = .true.  => outer quantum nos. present in adf00 file
c                            = .false. => outer quantum nos. not present
c  output: (c*2)   coupling  = ''  : base form,  unresolved,
c                              'ls':  '_ls' detected in dataset name,
c                              'ic':  '_ic' detected in dataset name,
c                              'ns': unspecified resolved
c
c
c routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          i4fctn     adas      converts from char. to integer  variable
c          xxslen     adas      finds string length excluding leading and
c                               trailing blanks
c          xxword     adas      parses a string into separate words
c                               for ' ()<>{}' delimiters
c          xxcase     adas      changes a string to upper or lower case
c          xfesym     adas      obtain element symbol from nuclear charge
c          xfelem     adas      obtain element name from nuclear charge
c          xxterm     adas      terminate program with a message
c          lenstr     adas      length of string omitting trailing blanks
c          xxhkey     adas      identify delimited key/value pair in string
c
c
c author:  Hugh Summers, University of Strathclyde
c          JA7.08
c          tel. 0141-548-4196
c
c date:    27/04/04
c
c update:  15/12/06  H. P. Summers - extended to handle metastable resolved
c                                    adf00 files
c
c
c version: 1.1                          date: 27-04-04
c modified: H.P. Summers
c               - first version
c
c version: 1.2                          date: 05-01-07
c modified: H. P. Summers
c               - extended to handle metastable resolved
c                                    adf00 files
c
c version: 1.3                          date: 09-04-2010
c modified: Martin O'Mullane
c               - Change integer*4 to integer.
c
c version: 1.4                          date: 14-06-12
c modified: H. P. Summers
c               - extended to read and return ground level quantum
c                 numbers from extended adf00 specification
c               - corrected dimension of imeta, reset nodim = 20
c               - reset string lengths c80_1, c80_2, c80_3 to 128
c
c version: 1.5                          date: 12-09-12
c modified: H. P. Summers
c               - change c80_ to c128_ for consistency
c               - nodim -> 128 for ic resolution
c               - handle outer quantum numbers and full ls and ic
c                 metastable resolution
c               - extra parameters for subroutine.
c
c version : 1.5                          
c date    : 17-08-2016
c modified: Martin O'Mullane
c               - In f_1003 add .1 to the f format when reading the
c                 xja field.
c
c version : 1.6                          
c date    : 18-09-2019
c modified: Martin O'Mullane
c               - Do not covert bwnoa to cm-1 since it is stored in
c                 the adf00 file in eV and eeva, alo in eV, was not
c                 converted.
c               - 3rd argument to xxhkey should be the break character
c                 (/) and not the equal sign (=).
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      integer    nodim    , nzdim    , ndlines
c-----------------------------------------------------------------------
      real*8     dzero
c-----------------------------------------------------------------------
      parameter( nodim = 128 , nzdim = 86 , ndlines = nzdim/18 )
      parameter( dzero = 1.0d-30 )
c-----------------------------------------------------------------------
      integer    iunit    , izdimd   , iodimd   , imdimd
      integer    i4unit   , i4fctn   , iabt     , iz0
      integer    istart   , istop    , lenstr
      integer    i        , j        , k        ,
     &           in       , n        , iq       , ind     , m
      integer    nfirst   , iwords   , maxwrd   , icurrent, nmet     ,
     &           iword    , nwords   , nlines
      integer    ilen_index  , ilen_config    , ilen_s     ,
     &           ilen_l      , ilen_j         , ilen_tran
      integer    lcline      , ii
c-----------------------------------------------------------------------
      integer    iorba(izdimd)     , na(izdimd,iodimd)     ,
     &           la(izdimd,iodimd) , iqa(izdimd,iodimd)
      integer    ifirst(nodim)     , ilast(nodim)
      integer    imeta(izdimd)     , imeta_actual(izdimd)
      integer    iorbma(izdimd,imdimd) ,
     &           nma(izdimd,iodimd,imdimd)    ,
     &           lma(izdimd,iodimd,imdimd)    ,
     &           iqma(izdimd,iodimd,imdimd)
      integer    isa(izdimd), ila(izdimd)
      integer    isma(izdimd,imdimd)          , ilma(izdimd,imdimd)
      integer    locpar(4)
c-----------------------------------------------------------------------
      real*8     wf
c-----------------------------------------------------------------------
      real*8     bwnoa(izdimd)   , eeva(izdimd)  , eevma(izdimd,iodimd)
      real*8     xja(izdimd)     , xjma(izdimd,imdimd)
c-----------------------------------------------------------------------
      character  dsname*(*)  , string*300
      character  esym*2      , xfelem*12    , xfesym*2     , coupling*2
      character  c3*3        , c10*10       , c12*12
      character  c128_1*128  , c128_2*128   , c128_3*128
      character  config*128  , element*12
      character  f_1003*28
c-----------------------------------------------------------------------
      character  cstr_std(izdimd)*(*)
      character  cstrm_std(izdimd,imdimd)*(*)
c-----------------------------------------------------------------------
      character  c_nshl(35)*1   , c_lshl(22)*1  , c_qshl(36)*1
      character  c_scol(45)*2
c-----------------------------------------------------------------------
      logical    lexist         , lresol        , lquno
c-----------------------------------------------------------------------
      data       c_scol /'1s','2s','2p','3s','3p','3d','4s','4p',
     &                   '4d','4f','5s','5p','5d','5f','5g','6s',
     &                   '6p','6d','6f','6g','6h','7s','7p','7d',
     &                   '7f','7g','7h','7i','8s','8p','8d','8f',
     &                   '8g','8h','8i','8j','9s','9p','9d','9f',
     &                   '9g','9h','9i','9j','9k'/
      data       c_nshl /'1','2','3','4','5','6','7','8','9',
     &                   'a','b','c','d','e','f','g','h','i',
     &                   'j','k','l','m','n','o','p','q','r',
     &                   's','t','u','v','w','x','y','z'/
      data       c_lshl /'s','p','d','f','g','h','i','j','k',
     &                   'l','m','n','o','q','r','t','u','v',
     &                   'w','x','y','z'/
      data       c_qshl /'0','1','2','3','4','5','6','7','8',
     &                   '9','a','b','c','d','e','f','g','h',
     &                   'i','j','k','l','m','n','o','p','q',
     &                   'r','s','t','u','v','w','x','y','z'/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c  initial internal dimension check
c-----------------------------------------------------------------------
c
      if (nodim.ne.iodimd) then
          stop ' xxdata_00 error: iodimd .ne. internal nodim param.'
      endif
c
c-----------------------------------------------------------------------
c input first data line and obtain element
c-----------------------------------------------------------------------
c
      c128_1=' '
      c128_2=' '
      read(iunit,'(1a128)') c128_1
      call xxcase(c128_1,c128_2,'lc')

      i=index(c128_2,'wf')
      if (i.gt.0) then
          lresol = .true.
          j=max(index(dsname,'_ls'),index(dsname,'_ic'))
          if(j.gt.0) then
             coupling = dsname(j+1:j+2)
          else
             coupling = 'ns'
          endif
      else
          lresol = .false.
          coupling = ''
      endif

      nfirst=1
      maxwrd = 2
      iwords=maxwrd
      call xxword( c128_2(1:24) , ' '   , nfirst , maxwrd ,
     &              ifirst , ilast , iwords
     &            )

      iabt=0
      iz0=i4fctn(c128_2(ifirst(2):ilast(2)),iabt)

      if(iz0.lt.0) then
          lexist=.true.
          iz0=-iz0
      else
          lexist=.false.
           write(i4unit(-1),1002)'no ionisation potential data'
           write(i4unit(-1),1003)
      endif


      element= xfelem(iz0)
      call xxcase(element,c12,'lc')
      call xxslen(c12,istart,istop)
      if (index(c128_2,c12(istart:istop)).le.0) then
         write(i4unit(-1),1000)'nuclear charge',iz0,
     &                          ' and element name inconsistent'
         write(i4unit(-1),1001)
         stop
      endif
      esym= xfesym(iz0)

c-----------------------------------------------------------------------
c get metastable count vector if resolved
c-----------------------------------------------------------------------

      wf = 0.0d0
      if(lresol) then
         nlines = iz0/18
         string=''
         i = min(index(c128_2,'/'),lenstr(c128_2))
         c10=' '
         call xxhkey(c128_2(i+1:i+13),'wf','/',c10)
         read(c10,*)wf
         string(1:i-ilast(2)) = c128_2(ilast(2)+1:i-1)
         do k=1,nlines
           read(iunit,'(1a128)') c128_2
           call xxslen(c128_2,istart,istop)
           string =string(1:lenstr(string))//'  '
     &     //c128_2(istart:istop)
         enddo
         nmet=iz0
         read(string(1:lenstr(string)),*)(imeta(k),k=1,nmet)
      endif

c-----------------------------------------------------------------------
c  detect formatting of ionisation potential/config/specification
c  from first line then backspace
c-----------------------------------------------------------------------

        if (lresol) then
            read(iunit,'(i2,3x,1pd16.8,a)')ind,eeva(1),c128_1
        else
            read(iunit,'(i2,1pd16.8,a)')ind,eeva(1),c128_1
        endif
        if(index(c128_1,'(').eq.0) then
           lquno=.false.
           ilen_config = lenstr(c128_1)
           write(f_1003,'(a3,i2,a1)')'(1a',ilen_config,')'
        else
           lquno = .true.
           lcline=len(c128_1)
           k=4
           do ii = lcline, 1, -1
             if (k.gt.0.and.(c128_1(ii:ii).eq.'('
     &                 .or.c128_1(ii:ii).eq.')')) then
                 locpar(k) = ii
                 k = k - 1
             endif
           end do

          ilen_config = locpar(1) - 1
          ilen_s      = locpar(2) - locpar(1) - 1
          ilen_l      = locpar(3) - locpar(2) - 1
          ilen_j      = locpar(4) - locpar(3) - 1       ! exclude the ()

          write(f_1003,'(a3,i2,a5,i2.2,a5,i2.2,a5,i1,a3)')
     &          '(1a',ilen_config,',1x,i',ilen_s,',1x,i',
     &           ilen_l,',1x,f',ilen_j,'.1)'

        endif
        backspace(iunit)

c-----------------------------------------------------------------------
c input ionisation potential lines
c-----------------------------------------------------------------------

      do i=1,iz0
        c128_1=' '
        c128_2=' '
        c128_3=' '

        if(lexist) then
            if(lresol) then
                read(iunit,'(i2,3x,1pd16.8,a)')ind,eeva(i),c128_1
                do k=1,imeta(i)
                  read(iunit,'(1a3,i2,1pd16.8,a)')
     &                             c3,ind,eevma(i,k),c128_2
                  if(lenstr(c3).gt.0) then
                      imeta_actual(i)=k-1
                      backspace(iunit)
                      go to 17
                  endif
                  imeta_actual(i)=imeta(i)
                  config = ' '
                  read(c128_2,f_1003)config(1:ilen_config),isma(i,k),
     &                               ilma(i,k),xjma(i,k)
                  call xxcase(config,c128_2,'lc')
                  call xxslen(c128_2,istart,istop)
                  c128_3=' '
                  c128_3(1:istop-istart+1)=c128_2(istart:istop)
                  call xxslen(c128_3,istart,istop)

                  nfirst=1
                  maxwrd = iodimd
                  iwords=maxwrd
                  call xxword( c128_3(1:istop), ' '   , nfirst , maxwrd,
     &                         ifirst         , ilast , iwords
     &                       )

                  iorbma(i,k)=iwords

                  do j=1,iwords
                    do m=1,22
                      in=index(c128_3(ifirst(j):ilast(j)),c_lshl(m))
                      if(in.gt.0) then
                         n=i4fctn(c128_3(ifirst(j):ifirst(j)+in-2),iabt)
                         iq=i4fctn(c128_3(ifirst(j)+in:ilast(j)),iabt)
                         nma(i,j,k)=n
                         lma(i,j,k)=m-1
                         iqma(i,j,k)=iq
                         go to 15
                      endif
                    enddo
   15               continue
                  enddo

                  cstrm_std(i,k)=' '

                  do j=1,iwords
                    c3=c_nshl(nma(i,j,k))//c_lshl(lma(i,j,k)+1)//
     &                 c_qshl(iqma(i,j,k)+1)
                    cstrm_std(i,k)(4*j-3:4*j)=c3//' '
                  enddo

                enddo
            else
                read(iunit,'(i2,1pd16.8,a)')ind,eeva(i),c128_1
            endif
c            bwnoa(i)=109737.26d0*eeva(i)/13.6048d0
            bwnoa(i)=eeva(i)
        else
            read(iunit,'(i2,a)')ind,c128_1
            eeva(i)=0.0d0
            bwnoa(i)=0.0d0
        endif

   17   config = ' '
        if(lquno) then
           read(c128_1,f_1003)config(1:ilen_config),isa(i),ila(i),xja(i)
        else
           read(c128_1,f_1003)config(1:ilen_config)
           isa(i)=0
           ila(i)=0
           xja(i)=0
        endif

c-----------------------------------------------------------------------
c  get orbitals and occupancies for each stage.
c-----------------------------------------------------------------------

        call xxcase(config,c128_2,'lc')
        call xxslen(c128_2,istart,istop)
        c128_1=' '
        c128_1(1:istop-istart+1)=c128_2(istart:istop)
        call xxslen(c128_1,istart,istop)

        nfirst=1
        maxwrd = iodimd
        iwords=maxwrd
        call xxword( c128_1(1:istop), ' '   , nfirst , maxwrd ,
     &              ifirst , ilast , iwords
     &             )

        iorba(i)=iwords

        do j=1,iwords
          do k=1,22
            in=index(c128_1(ifirst(j):ilast(j)),c_lshl(k))
            if(in.gt.0) then
                n=i4fctn(c128_1(ifirst(j):ifirst(j)+in-2),iabt)
                iq=i4fctn(c128_1(ifirst(j)+in:ilast(j)),iabt)
                na(i,j)=n
                la(i,j)=k-1
                iqa(i,j)=iq
                go to 20
            endif
          enddo
   20     continue
        enddo

        cstr_std(i)=' '

        do j=1,iwords
          c3=c_nshl(na(i,j))//c_lshl(la(i,j)+1)//c_qshl(iqa(i,j)+1)
          cstr_std(i)(4*j-3:4*j)=c3//' '
        enddo
c--------------------------------------------
c  tidy up metastable vectors if unresolved
c--------------------------------------------
        if(.not.lresol) then
            imeta(i)      = 1
            imeta_actual(i)=1
            eevma(i,1)    = 0.0d0
            iorbma(i,1)   = iorba(i)
            isma(i,1)=isa(i)
            ilma(i,1)=ila(i)
            xjma(i,1)=xja(i)
            do j=1,iorba(i)
              nma(i,j,1)    = na(i,j)
              lma(i,j,1)    = la(i,j)
              iqma(i,j,1)   = iqa(i,j)
              cstrm_std(i,1)= cstr_std(i)
            enddo
        endif

      enddo

c-----------------------------------------------------------------------
 1000 format(1x,31('*'),' xxdata_00 error ',30('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
 1002 format(1x,30('*'),' xxdata_00 warning ',29('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1003 format(/1x,29('*'),' program continues  ',29('*'))
c-----------------------------------------------------------------------

      return
      end
