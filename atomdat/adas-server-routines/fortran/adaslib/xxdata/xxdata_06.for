       subroutine xxdata_06(iunit  ,
     &                      ndlev  , ndtrn  , ndmet   ,  
     &                      ndqdn  , nvmax  , ndpro   ,
     &                      titled , iz     , iz0     , iz1    , bwno  ,
     &                      npl    , bwnoa  , lbseta  , prtwta , cprta ,
     &                      il     , qdorb  , lqdorb  , qdn    , iorb  ,
     &                      ia     , cstrga , isa     , ila    , xja   ,
     &                      wa     ,
     &                      cpla   , npla   , ipla    , zpla   ,
     &                      nv     , scef   ,
     &                      itran  , maxlev ,
     &                      tcode  , i1a    , i2a     , aval   , scom  ,
     &                      np     , proj   , cpro    , beth   ,
     &                      iadftyp, lprn   , lcpl    , lorb   , lbeth ,
     &                      lityp  , lstyp  , itieactn, ltied
     &                     )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: xxdata_06 ******************
C
C  PURPOSE:  To fetch data from an adf06 data set and detect its main
C            characteristics. This is a fully inclusive version, based
C            on xxdata_04.for, detecting the following:
C
C                1. Multiple parent data on the first line including
C                   the j-resolved case
C                2. Supplementary parent assignment data on level
C                   lines for improved automatic ionisation calculation
C                3. Orbital energy data on the level terminator line
C                4. First bethe coefft. at end of transition lines for
C                   improved asymptotics
C                5. Stacked blocks of data with headers indicating
C                   different possible bare ion projectiles involved in
C                   the ion-impact excitation: this is the primary
C                   difference between the adf06 and adf04 datatype
C
C  calling program: various
C
C  data:
C           The 'real' data in the file is represented in an abbreviated
C           form which omits the "d" or "e" exponent specifier.
C           e.g. 1.23d-06 or 1.23e-06 is represented as 1.23-06
C                6.75d+07 or 6.75e+07 is represented as 6.75+07
C
C           Therefore the form of each 'real' number in the data set is:
C                          n.nn+nn or n.nn-nn
C
C           The units used in the data file are taken as follows:
C
C           ionisation potential: wave number (cm-1)
C           index level energies: wave number (cm-1)
C           temperatures        : kelvin
C           a-values            : sec-1
C           gamma-values        : (dimensionless)
C           S-line              : exp(IP/kTe)*S (cm3/s)
C
C
C  subroutine:
C
C  input : (i*4)  iunit   = unit to which input file is allocated
C  input : (i*4)  ndlev   = max. number of levels that can be read
C  input : (i*4)  ndtrn   = max. number of transitions that can be read
C  input : (i*4)  ndqdn   = max. number of n-shells for quantum defects
C  input : (i*4)  nvmax   = max. number of temperatures that can be read in.
C  input : (i*4)  ndpro   = max. number of ion projectiles that can be read in.
C  input : (i*4)  ndmet   = max. number of metastables allowed
C
C  input : (i*4)  itieactn= 1 return data even if some levels are untied.
C                           0 default behaviour - terminate if untied
C                             levels are present.
C                           On output 1 if untied levels present
C                                     0 for no untied levels.
C
C  output: (c*3)  titled  = element symbol.
C  output: (i*4)  iz      =  recombined ion charge read
C  output: (i*4)  iz0     =         nuclear charge read
C  output: (i*4)  iz1     = recombining ion charge read
C                           (note: iz1 should equal iz+1)
C  output: (r*8)  bwno    = ionisation potential (cm-1) of lowest parent
C  output: (i*4)  npl     = number of parents on first line and used
C                           in level assignments
C  output: (r*8)  bwnoa() = ionisation potential (cm-1) of parents
C  output: (l*4)  lbseta()= .true.  - parent weight set for bwnoa()
C                           .false. - parent weight not set for bwnoa()
C  output: (r*8)  prtwta()= parent weight for bwnoa()
C  output: (c*9)  cprta() = parent name in brackets
C
C  output: (i*4)  il      = input data file: number of energy levels
C  output: (r*8)  qdorb() = quantum defects for orbitals. Negative quantum
C                           defects are forced to zero.
C                           1st dim: index for nl orbital (cf i4idfl.for)
C  output: (l*4)  lqdorb()= .true.  => source data available for qd.
C                         = .false. => source data not availabe qd.=0.0
C  output: (r*8)  qdn()   = quantum defect for n-shells.  non-zero only
C                           for adf04 files with orbital energy data
C                           1st. dim: n-shell (1<=n<=ndqdn)
C  output: (i*4)  iorb    = input data file: number of orbital energies
C
C  output: (i*4)  ia()    = energy level index number
C  output: (c*18) cstrga()= nomenclature/configuration for level 'ia()'
C  output: (i*4)  isa()   = multiplicity for level 'ia()'
C                           note: (isa-1)/2 = quantum number (s)
C  output: (i*4)  ila()   = quantum number (l) for level 'ia()'
C  output: (r*8)  xja()   = quantum number (j-value) for level 'ia()'
C                           note: (2*xja)+1 = statistical weight
C  output: (r*8)  wa()    = energy relative to level 1 (cm-1) for level
C                           'ia()'
C  output: (c*1)  cpla()  = char. specifying 1st parent for level 'ia()'
C                                integer - parent in bwnoa() list
C                                'blank' - parent bwnoa(1)
C                                  'x'   - do not assign a parent
C  output: (i*4)  npla()  = no. of parent/zeta contributions to ionis.
C                           of level
C  output: (i*4)  ipla(,) = parent index for contributions to ionis.
C                           of level
C                           1st dimension: parent index
C                           2nd dimension: level index
C  output: (i*4)  zpla(,) = eff. zeta param. for contributions to ionis.
C                           of level
C                           1st dimension: parent index
C                           2nd dimension: level index
C
C  output: (i*4)  nv      = input data file: number of gamma/temperature
C                           pairs for a given transition.
C  output: (r*8)  scef()  = input data file: electron temperatures (k)
C                           (initially just the mantissa. see 'itpow()')
C                           (note: te=tp=th is assumed)
C
C  output: (i*4)  itran   = input data file: number of transitions
C  output: (i*4)  maxlev  = highest index level in read transitions
C
C  output: (c*1)  tcode() = transition: data type pointer:
C                           ' ' => ion impact transition (ion specified
C                           in header: see details below)
C                           'i','I' => coll. ionis. from lower stage ion
C                           's','S' => Ionisation from current ion
C  output: (i*4)  i1a()   = transition:
C                            lower energy level index (case ' ')
C                            signed parent index (case 's', 'i')
C  output: (i*4)  i2a()   = transition:
C                            upper energy level index (case ' ')
C                            capturing level index (case 's' , 'i')
C  output: (r*8)  aval()  = transition:
C                            E2 a-value (sec-1)      (case ' ')
C                            not used                (case 's' & 'i')
C  output: (r*8)  scom(nvmax,ndtrn,ndpro)
C                         = transition:
C                            gamma values           (case ' ')
C                            rate coefft.(cm3 sec-1)(case 'i')
C                            scaled rate coefft.(cm3 sec-1)(case 's')
C                           1st dimension - temperature 'scef()'
C                           2nd dimension - transition number
C                           3rd dimension - ion projectile case
C  output: (i*4)  np      = input data file: number of ion projectiles
C                           present
C  output: (r*8)  proj(3, ndpro)
C                         = physical parameters for ion projectile cases
C                           1st dim - physical parameters in fixed
C                                     order: target mass (amu),
C                                     projectile charge number,
C                                     projectile mass (amu)
C                           2nd dim - ion projectile case
C  output: (c*5)  cpro(ndpro)
C                         = ion projectile case descriptor in the format
C                         '<mass number>"^"<element symbol>'
C  output: (i*4)  beth(ndpro,ndtrn)
C                         = transition Born limit (case ' ')
C                           1st dim - ion projectile case
C                           2nd dim - transition
C  output: (i*4)  iadftyp = adf06 type: 1=omega vs energy/delta_E
C                                       3=upsilon vs Te,
C                                       4=non-maxwl. vs energy param.,
C                                       5=omega vs Ef; 
C                                       only 1 and 3 supported currently
C  output: (l*4)  lprn    = .true.  => multiple parent data on 1st line
C                         = .false. => multiple parent data not present
C  output: (l*4)  lcpl    = .true.  => parent assignment on level lines
C                         = .false. => parent assignment not present
C  output: (l*4)  lorb    = .true.  => orbital data on level terminator
C                         = .false. => orbital data not present
C  output: (l*4)  lbeth   = .true.  => bethe data on transition lines
C                         = .false. => bethe data not present
C  output: (l*4)  lityp   = .true.  => ionis. trans. from z-1 ion present
C  output: (l*4)  lstyp   = .true.  => ionis. or 3-body recom trans.
C                                      from current ion present
C  output: (l*4)  ltied() = .true.  => specified level tied
C                         = .false. => specified level is untied
C                           dimension => level index
C
C          (i*4)  ntdim   = parameter = max. number of internal temperatures
C                                       (must equal nvmax)
C          (i*4)  npdim   = parameter = number of ion proj. parameters
C          (r*8)  dzero   = parameter = minimum value for 'aval()' and
C                                       'scom()' arrays = 1.0d-30
C
C          (i*4)  i4unit  = function (see routine selection below)
C          (i*4)  iqs     = x-sect data format selector
C          (i*4)  ifail   = failure number from xxpars and xxprs1
C          (i*4)  i       = general use.
C          (i*4)  iabt    = return code from 'r(fctn' (0 => no error)
C                           or from interrogation of 'c10'
C          (i*4)  j       = general use.
C          (i*4)  j1      = input data file - selected transition:
C                            lower energy level index (case ' ' & 'p')
C          (i*4)  j2      = input data file - selected transition:
C                            upper energy level index (case ' ' & 'p')
C                            capturing    level index (case 'h' & 'r')
C          (i*4)  lencst  = byte length of string cstrga()
C          (i*4)  iline   = energy level index for current line
C          (i*4)  itype   = resolution of parent metastables
C                             1 - ls resolved
C                             2 - lsj resolved
C                             3 - arbitrary resolution
C          (i*4)  iapow   = exponent of 'avalm'
C          (i*4)  igpow() = exponent of 'gamma()'
C          (i*4)  itpow() = temperatures - exponent
C                           note: mantissa initially kept in 'scef()'
C
C          (r*4)  zf      = should be equivalent to 'iz1'
C
C          (r*8)  avalm   = input data file - selected transition:
C                           mantissa of:   ('iapow' => exponent)
C                            a-value (sec-1)       (case ' ')
C                            not used              (case 's' & 'i')
C          (r*8)  gamma() = input data file - selected transition:
C                           mantissa of: ('igpow()' => exponent)
C                            gamma values      (case ' ','1','2','3')
C                            rate coefft.(cm3 sec-1)(case 's', 'i')
C                           dimension => temperature 'scef()'
C
C          (c*10) c10     = used to parse value for xja()
C          (c*7)  cdelim  = delimiters for input of data from headers
C          (c*90) c25     = used to parse value to cstrga()
C                            - nb: internal limit is now 90
C          (c*80) cline   = current energy level index parameter line
C          (c*75) string  = tail string of 1st data line for parsing
C          (c*44) strg1   = tail string of level spec lines for parsing
C          (c*500)buffer  = general string buffer storage
C          (c*3)  citpow()= used to parse values to itpow()
C          (c*5)  cscef() = used to parse values to scef()
C
C          (l*4)  ldata   = identifies  whether  the end of an  input
C                           section in the data set has been located.
C                           (.true. => end of section reached)
C          (l*4)  ltchr   = .true.  => current 'tcode()' = 's' or 'i'
C          (l*4)  ltcpr   = .true.  => current 'tcode()' = ' '
C
C
C note:        ltchr        ltcpr            tcode()
C              ----------------------------------------
C              .true.       .false.     =>   'i','s'
C              .false.      .true.      =>   ' '
C              .false.      .false.     =>   invalid case
C
C        for a-values & gamma-values entries less than 'dzero' are taken
C        as being equal to dzero. this affects the 'aval()' and 'scom()'
C        arrays.
C
C routines:
C          routine    source    brief description
C          -------------------------------------------------------------
C          xxpars     ADAS      analyses the adf06 1st string for parents
C          xxprs1     ADAS      analyses the adf06 level string for ionis.
C          i4unit     ADAS      fetch unit number for output of messages
C          r8fctn     ADAS      converts from character to real variable
C          i4fctn     ADAS      converts from char. to integer  variable
C          xxslen     ADAS      finds string length excluding leading and
C                               trailing blanks
C          xxword     ADAS      parses a string into separate words
C                               for ' ()<>{}' delimiters
C
C AUTHOR:  Matthew Bluteau, University of Strathclyde
C
C DATE:    01-09-2016
C
C UPDATE:
C
C
C VERSION  : 1.1
C DATE     : 27-10-2016
C MODIFIED : Matthew Bluteau
C            - First version.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      integer   ntdim       , npdim
C-----------------------------------------------------------------------
      real*8    dzero
C-----------------------------------------------------------------------
      parameter( ntdim = 50 , npdim = 3      , dzero = 1.0D-30 )
C-----------------------------------------------------------------------
      integer   i4unit      , ifail          , itype      , itieactn
      integer   iunit       , ndlev          , ndtrn      , nvmax  ,
     &          ndmet       , ndqdn          , ndpro      ,
     &          iz          , iz0            , iz1        ,
     &          il          , nv             , itran      , np     ,
     &          maxlev      , npl            , iadftyp    , iorb
      integer   i4idfl      , n              , l          , nlast
      integer   iline
      integer   ifirst(3)   , ilast(3)       , iword      , nwords ,
     &          ilen_index  , ilen_config    , ilen_s     ,
     &          ilen_l      , ilen_j         , ilen_tran  ,
     &          ifrst2(npdim+1), ilst2(npdim+1), nfirst
      integer   iqs         , i              , iabt       , j      ,
     &          j1          , j2             , k          ,
     &          lencst      , lcline         , l1         , l2     ,
     &          ibeth       , iplus          , p          , err    ,
     &          iapow       , igpow(ntdim)   , itpow(ntdim)
      integer   ia(ndlev)   , isa(ndlev)     , ila(ndlev) ,
     &          i1a(ndtrn)  , i2a(ndtrn)     ,
     &          ipla(ndmet,ndlev)            , npla(ndlev)
      integer   locpar(4)   , imatcha(ndtrn)
      integer   iwarn_umatch   , iwarn_omatch
C-----------------------------------------------------------------------
      real*8    zf
      real*8    r8fctn
      real*8    bwno        , bwnoa(ndmet)   , prtwta(ndmet)  , bethm  ,
     &          avalm
      real*8    scef(nvmax) , gamma(ntdim)
      real*8    xja(ndlev)  , wa(ndlev)      ,
     &          beth(ndtrn,ndpro)            , aval(ndtrn)    ,
     &          scom(nvmax,ndtrn,ndpro)      ,
     &          zpla(ndmet,ndlev)            , proj(npdim,ndpro)
      real*8    qdorb((ndqdn*(ndqdn+1))/2)   , qdn(ndqdn)
C-----------------------------------------------------------------------
      character titled*3    , tcode(ndtrn)*1 , cstrga(ndlev)*(*),
     &          cpro(ndpro)*5
      character c10*20      , cdelim*7       , c25*99       , c1*1     ,
     &          c2*2        , ctmp*50
      character cline*296   , string*75      , strg1*56     , buffer*500
      character f_1003*35   , f_1005*28      , f_1015*4
      character f_1008*22   , f_1020*10
      character citpow(ntdim)*3              , cscef(ntdim)*5
      character cpla(ndlev)*1,cprta(ndmet)*9
      character caj1*9       , caj2*9        ,
     &          codei*1      , codej*1       , codet*1
C-----------------------------------------------------------------------
      logical   ldata       , ltchr          , ltcpr        , lerror
      logical   lprn        , lcpl           , lorb         , lbeth      
      logical   lityp       , lstyp          , lttyp
C-----------------------------------------------------------------------
      logical   lbseta(ndmet) , ltied(ndlev) , 
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lmatcha(ndtrn), lindx(ndtrn)
C-----------------------------------------------------------------------
      data      cdelim/ ' ()<>{}'/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  initial internal dimension check
C-----------------------------------------------------------------------

         if (nvmax.gt.ntdim) then
            write(i4unit(-1),1001) '(nvmax.gt.ntdim) -'
            write(i4unit(-1),1002)
            stop
         endif

C-----------------------------------------------------------------------
C For configuration strings use the length as passed into routine but
C ignore anything greater than character 99 (internal parsing limit -
C see c25 varaible). Impose a minimum of 5 characters.
C-----------------------------------------------------------------------

      lencst = len(cstrga(1))

      if (lencst.lt.5) then
         write(i4unit(-1),1014)
         write(i4unit(-1),1002)
         stop
      endif

      lencst = max0( len(cstrga(1)) , 99 )

C-----------------------------------------------------------------------
C  set logicals to .false. initially and set warnings count
C-----------------------------------------------------------------------

      lprn  = .false.
      lcpl  = .false.
      lorb  = .false.
      lbeth = .false.

      lityp = .false.
      lstyp = .false.

C-----------------------------------------------------------------------
C input ion specifications and parse first data card string
C-----------------------------------------------------------------------

      read(iunit,1000) titled, iz, iz0, iz1, string

      ifail = 0
      call xxpars( ndmet , string , npl  , bwnoa , lbseta  , prtwta  ,
     &             cprta , ifail  , itype )
      if(npl.ge.1) lprn=.true.

      if(ifail.eq.1) then
         write(i4unit(-1),1001) 'ifail=1 from xxpars - stop '
         write(i4unit(-1),1002)
         stop
      endif
      bwno=bwnoa(1)
      if(bwno.le.0.0d0 .and. iz .ne. iz0) then
         write(i4unit(-1),1001) 'ionisation potential .le. 0'
         write(i4unit(-1),1002)
         stop
      endif

C-----------------------------------------------------------------------
C  read in energy level specifications
C  energy level input information is terminated by iline=-1.
C-----------------------------------------------------------------------

      ldata=.true.
      read (iunit,'(a)') cline
      backspace(iunit)

C-----------------------------------------------------------------------
C  pre-analyse formatting of level line
C
C  Search first configuration line from the end to locate
C  the 4 parenthesis bracketing the quantum numbers
C-----------------------------------------------------------------------

      lcline=len(cline)

      k=4
      do i = lcline, 1, -1
         if (k.gt.0.and.(cline(i:i).eq.'('.or.cline(i:i).eq.')')) then
            locpar(k) = i
            k = k - 1
         endif
      end do

      iword = 1
      j     = 1
      call xxword( cline, ' '  , iword   , j       ,
     &             ifirst(1)  , ilast(1) , nwords
     &           )

      ilen_index  = len(cline(1:ilast(1)))
      ilen_config = locpar(1) - ilen_index - 2
      ilen_s      = locpar(2) - locpar(1) - 1
      ilen_l      = locpar(3) - locpar(2) - 1
      ilen_j      = locpar(4) - locpar(3) + 1        ! include the ()

      write(f_1003,'(a2,i1,a6,i2.2,a5,i2.2,a5,i2.2,a2,i2.2,a3)')
     &             '(i',ilen_index,',1x,1a',ilen_config,',1x,a',ilen_s,
     &             ',1x,a', ilen_l,',a',ilen_j,',a)'

      write(f_1015,'(a2,i1,a1)')'(i',ilen_index,')'

C-------------------------------
C Step through the energy levels
C-------------------------------

      maxlev=0

         do 1 i=1,ndlev

            ltied(i) = .false.

            if (ldata) then
               cline=' '
               read (iunit,'(a)') cline
               read (cline,f_1015)  iline

                  if (iline.le.0) then
                     ldata=.false.
                     il=i-1
C-------------------------------------------------
C  check for orbitals and evaluate quantum defects
C-------------------------------------------------
                     j     = 1
                     iword = 1
                     lcline=len(cline)

                     call xxword( cline(ilen_index+1:lcline),
     &                            cdelim     , iword    ,j      ,
     &                            ifirst(1)  , ilast(1) , iorb   )

                     if (iorb.gt.(ndqdn*(ndqdn+1))/2) then
                         write(i4unit(-1),1001) 
     &                          'adf06 data set contains > ',
     &                          (ndqdn*(ndqdn+1))/2,' orbital energies'
                         write(i4unit(-1),1002)
                         stop
                     endif

                     if(iorb.gt.0) then
                         read(cline(ilen_index+1:lcline),*)
     &                        (qdorb(k),k=1,iorb)
                         do 20 l1 = 1,ndqdn
                           l = l1-1
                           nlast = l
                           do 10 n = l1,ndqdn
                             k = i4idfl(n,l)
                             if(dabs(qdorb(k)).gt.0.0d0) then
                                 qdorb(k) = dmax1((-dfloat(iz1)/
     &                             dsqrt(qdorb(k))+dfloat(n)),0.0d0)
                                 lqdorb(k) = .true.
                                 nlast = n
                             else
                                 if(nlast.gt.l) then
                                     qdorb(k) = qdorb(i4idfl(nlast,l))
                                     lqdorb(k) = .true.
                                 else
                                     qdorb(k) = 0.0d0
                                     lqdorb(k) = .false.
                                 endif
                             endif
   10                      continue
   20                    continue

                         do 40 n = 1,ndqdn
                           qdn(n) = 0.0d0
                           do 30 l1 = 1,n
                             l=l1-1
                             k = i4idfl(n,l)
                             qdn(n) = qdn(n) + (2.0d0*l+1.0d0)
     &                                /(n-qdorb(k))**2
   30                      continue
                           qdn(n) = -n/dsqrt(qdn(n)) + n
   40                    continue
                         lorb = .true.
                     endif
C-------------------------------------------------
C  energy level index 'iline' should equal 'i'
C-------------------------------------------------
                  elseif (iline.ne.i) then
                     write(i4unit(-1),1001) 'energy level index',iline,
     &                                      ' out of order'
                     write(i4unit(-1),1002)
                     stop
                  else
                     read (cline,f_1003) ia(i)  , c25       ,
     &                                   c2     , c1        ,
     &                                   c10    , strg1

                     call xxslen(c2(1:ilen_s), l1, l2)
                     if (l1+l2.GT.0) then
                        read(c2(1:ilen_s),*)isa(i)
                     else
                        isa(i) = -1
                     endif

                     if (c1.NE.' ') then
                        read(c1,*)ila(i)
                     else
                        ila(i) = -1
                     endif

                     ifail = 0
                     call xxprs1(ndmet,strg1,wa(i),cpla(i),npla(i),
     &                           ipla(1,i),zpla(1,i),ifail)
                     if(ifail.eq.1) then
                         write(i4unit(-1),1001)
     &                         'ifail=1 from xxprs1 - stop '
                         write(i4unit(-1),1002)
                         stop
                     endif
                     if(ifail.eq.0) lcpl = .true.

C-----------------------------------------------------
C  Reduce cstrga to lencst
C-----------------------------------------------------

                     if (c25(1:2).eq.'  ') c25=c25(3:25)

                     call xxslen(c25,ifirst(1),ilast(1))
                     ifirst(1)=max0(ilast(1)-(lencst-1),ifirst(1))
                     if (ilast(1).gt.0) then
                        cstrga(i)=c25(ifirst(1):ilast(1))
                     else
                        cstrga(i)=' '
                     endif
C-------------------------------------------------

                     j = index(c10,')') - 1

                        if (c10(1:j).eq.' ') then
                           iabt   = -1
                        else if (j.gt.1) then
                           xja(i) = r8fctn( c10(2:j) , iabt )
                        else
                           iabt   = j - 1
                        endif

                        if (iabt.ne.0) then
                           write(i4unit(-1),1001)
     &                                   'adf06 data set level ',i,
     &                                   ' has invalid weight'
                              if (iabt.eq.-1) then
                                 write(i4unit(-1),1012)
     &                                    'no number found',c10
                              else if (iabt.eq.-2) then
                                 write(i4unit(-1),1012)
     &                                    'no right bracket found',c10
                              else
                                 write(i4unit(-1),1012)
     &                                    'invalid number found' , c10
                              endif
                           write(i4unit(-1),1002)
                           stop
                        endif
                    maxlev=max0(iline,maxlev)
C-------------------------------------------------
                  endif
            endif
    1    continue

         if (ldata) then
            read (iunit,1004) i
               if (i.gt.0) then
                   write(i4unit(-1),1001) 'adf06 data set contains > ',
     &                                    ndlev,' energy levels'
                   write(i4unit(-1),1002)
                  stop
               else
                  il = ndlev
               endif
         endif

C-----------------------------------------------------------------------
C  read in temperatures (kelvin) and data format selector
C
C  check if this is a NIST dataset, which only contains energy levels
C  if so return setting transition data to zero.
C-----------------------------------------------------------------------

      buffer = ' '
      read(iunit,'(a)') buffer

C End of data check

      iword = 1
      j     = 3
      c1    = ' '
      call xxword( buffer  , c1     , iword     ,
     &             j       ,
     &             ifirst  , ilast  , nwords
     &            )
      if (nwords.eq.2) then

         if (buffer(ifirst(1):ilast(1)).eq.'-1'.and.
     &       buffer(ifirst(2):ilast(2)).eq.'-1') then

            nv    = 0
            itran = 0
            do i = i, ndtrn
               i1a(i)   = 0
               i2a(i)   = 0
               aval(i)  = 0.0D0
               tcode(i) = ' '
               do j = 1,ndpro
                  cpro(j) = ' '
                  beth(i,j) = 0.0D0
                  do k = 1, nvmax
                     scom(k,i,j) = 0.0D0
                  end do
               end do
            end do
            return                     ! exit xxdata_06 now

         endif

      endif

C Transition data present so continue

      iword = 3
      j     = 3
      c1    = ' '
      call xxword( buffer  , c1     , iword     ,
     &             j       ,
     &             ifirst  , ilast  , nwords
     &            )

      ilen_tran=ifirst(1)-10

      f_1005='(f5.2,4x,i1,?x,?x,50(a5,a3))'
      f_1008='(a1,i?,i?,51(f5.2,i3))'
      f_1020='(a1,a?,a?)'
      write(c1,'(i1)')(ilen_tran-2)/2
      f_1005(13:13)=c1
      f_1005(16:16)=c1
      write(c1,'(i1)')ilen_tran/2-1
      f_1008(6:6)=c1
      f_1020(6:6)=c1
      write(c1,'(i1)')ilen_tran/2
      f_1008(9:9)=c1
      f_1020(9:9)=c1
      read(buffer,f_1005) zf, iqs, (cscef(i),citpow(i),i=1,ntdim)
      iadftyp = iqs

C-------------------------------------------------
C check data format selector is valid
C-------------------------------------------------

      if((iqs.ne.1).and.(iqs.ne.3).and.(iqs.ne.4).and.
     &   (iqs.ne.5).and.(iqs.ne.6)) then
         write(i4unit(-1),1001) '(iqs.ne.1, 3, 4, 5 or 6) -'
         write(i4unit(-1),1006)
     &   'file contains invalid data format selector ',
     &   '(must equal 1, 3, 4, 5 or 6)'
         write(i4unit(-1),1002)
         stop
      endif
C-------------------------------------------------
C check 'zf' equals iz1
C-------------------------------------------------

      if(nint(zf).ne.iz1) then
         write(i4unit(-1),1001) '(zf.ne.iz1) -'
         write(i4unit(-1),1006)
     &   'file contains inconsistent recombining ion charge values'
         write(i4unit(-1),*)zf,iz1
         write(i4unit(-1),1002)
         stop
      endif

C-------------------------------------------------
C  check that nv isn't greater than nvmax
C-------------------------------------------------

      nv=0
      do j=1,ntdim
         if (cscef(j).ne.' ') then
            nv = j
         endif
      end do
      if (nv.gt.nvmax) then
         write(i4unit(-1),1001) '(nv.gt.nvmax) - nv =',nv
         write(i4unit(-1),1002)
         stop
      endif

C-------------------------------------------------
C identify the number of temperatures values input
C-------------------------------------------------

      nv = 0

      do j=1,nvmax
         if (cscef(j).eq.' ') then
            scef(j)  = 0.0d0
            itpow(j) = 0
         else
            read(cscef(j) ,'(f5.2)') scef(j)
            read(citpow(j), '(i3)')  itpow(j)
            if (scef(j).gt.0.0d0) nv = j
         endif
      end do

C-------------------------------------------------
C  check that at least one valid temperature exists.
C-------------------------------------------------

      if (nv.le.0) then
         write(i4unit(-1),1001) '(nv.le.0) -'
         write(i4unit(-1),1006)
     &   'no valid temperature values found (none > 0.0)'
         write(i4unit(-1),1002)
         stop
      endif

C-------------------------------------------------
C  combine input mantissa and exponent for temperatures
C-------------------------------------------------

      do i=1,nv
         scef(i)  = scef(i)  * ( 10.0D0**dble(itpow(i)) )
      end do

C-------------------------------------------------
C  check temperatures are in a strictly increasing
C  monotonic sequence (required for the later use
C  of the minimax and spline routines)
C-------------------------------------------------

      do i=2,nv
         if (scef(i).le.scef(i-1)) then
            write(i4unit(-1),1007) '(scef(',i,').le.scef(',i-1,')) -'
            write(i4unit(-1),1006)
     &   'temperatures are not in a strict monotonic ascending order'
            write(i4unit(-1),1002)
            stop
          endif
      end do

C-----------------------------------------------------------------------
C read in ion projectile and transition specifications
C-----------------------------------------------------------------------

      lbeth = .false.
C      maxlev = 0
      ldata = .true.
      itran = -1

      do i = 1,ndtrn
         lindx(i) = .TRUE.
      end do

C get the next line, which should be an ion projectile header
      buffer = ' '
      read(iunit,'(a)')buffer
      if(buffer(1:1).ne.'/') then
          write(i4unit(-1),1001) 'transition spec must start with' //
     &         ' ion projectile header'
          write(i4unit(-1),1002)
          stop
      end if
C initialize projectile counter and xxword parameters
      np = 0
      nfirst = 1
      c1 = '/'

C-------------------------------------------------
C loop through possible projectiles
C-------------------------------------------------
      do k=1,ndpro
          if (.not. ldata) then
              exit
          elseif (buffer(1:1).ne.'/') then
C the maximum # of transitions has been exceeded or matched
              buffer = ' '
              read(iunit,'(a)')buffer
              if (buffer(1:1).ne.'/') then
                  backspace(iunit)
                  read (iunit,1010) j
                  if (j.gt.0) then
                    write(i4unit(-1),1007) 'adf06 data set contains > ',
     &               ndtrn,' transitions, the maximum in block ', k-1
                    write(i4unit(-1),1002)
                    stop
                  else
                     if (itran .eq. -1) then
                         itran = ndtrn
                         ldata = .false.
                         exit
                     elseif (itran .eq. ndtrn) then
                         ldata = .false.
                         exit
                     else
                         write(i4unit(-1),1001) 'Projectile block ',k-1,
     &                    ' has a different number of transitions' //
     &                    ' than others in the dataset'
                         write(i4unit(-1),1002)
                         stop
                     end if
                     ldata = .false.
                     exit
                  endif
              else
                  itran = ndtrn
              endif
          end if
          iword = npdim + 1
C split and parse projectile header
          call xxword(buffer, c1, nfirst, iword, ifrst2, ilst2, nwords)
          if(nwords .ne. npdim+2 .or. iword .ne. npdim+1) then
              write(i4unit(-1),1001) 'Ion projectile header ', k,
     &         ' has an invalid # of elements'
              write(i4unit(-1),1002)
              stop
          end if
C unpack the parameters for the projectile
          do j=1,npdim
              ctmp = ' '
              ctmp = buffer(ifrst2(j):ilst2(j))
              p = index(ctmp, '=') + 1
              proj(j,k) = r8fctn(ctmp(p:), err)
              if(err.ge.2) then
                  write(i4unit(-1),1001) 'R8FCTN error code: ', err,
     &             ' encountered while parsing a projectile header.' //
     &             ' Check number formats.'
                  write(i4unit(-1),1002)
                  stop
              end if
          enddo
          p = npdim + 1
C get the projectile string descriptor
          ctmp = ' '
          ctmp = buffer(ifrst2(p):ilst2(p))
          p = index(ctmp, '^')
          if(p.eq.0) then
              write(i4unit(-1),1001) 'Ion projectile header ', k,
     &         'has an incorrectly formatted projectile descriptor'
              write(i4unit(-1),1002)
              stop
          end if
          cpro(k) = ctmp(p-2:p+2)
          np = np + 1
C-------------------------------------------------
C loop through transitions for this projectile
C-------------------------------------------------
         do 6 i=1,ndtrn
            if (ldata) then
               buffer = ' '
               read(iunit,'(a)')buffer
               read(buffer(1:1),'(1a1)')tcode(i)
               call xxslen(buffer,ifirst(1),ilast(1))
C new projectile block indicated by '/' at start of line
               if(tcode(i).eq.'/') then
                   if (itran .eq. -1) then
                       itran = i - 1
                       exit
                   elseif (itran .eq. i-1) then
                       exit
                   else
                       write(i4unit(-1),1001) 'Projectile block ', k,
     &                  ' has a different number of transitions than' //
     &                  ' others in the dataset'
                       write(i4unit(-1),1002)
                       stop
                   end if
               elseif(ilast(1).gt.(8*nv+16+ilen_tran-8)) then
                  read(buffer,f_1008) tcode(i) , j2 , j1 , avalm ,
     &                iapow,(gamma(j),igpow(j),j=1,nv),bethm,ibeth
                  lbeth = .true.
               else
                  read(buffer,f_1008) tcode(i) , j2 , j1 , avalm ,
     &                iapow,(gamma(j),igpow(j),j=1,nv)
                  bethm = 0.0d0
                  ibeth = 0
               endif
C-------------------------------------------------
C transition input information is terminated by j2=-1
C-------------------------------------------------
                  if ((j2.le.0).and.(tcode(i).eq.' ')) then
                     ldata=.false.
                     if (itran .eq. -1) then
                         itran = i - 1
                         exit
                     elseif (itran .eq. i-1) then
                         exit
                     else
                         write(i4unit(-1),1001) 'Projectile block ', k,
     &                    ' has a different number of transitions' //
     &                    ' than others in the dataset'
                         write(i4unit(-1),1002)
                         stop
                     end if
                  else

C-----------------------------------------------------------------------
C set transition type detection logical and identify 'tcode()' type.
C-----------------------------------------------------------------------

                     if((tcode(i).eq.'i').or.(tcode(i).eq.'I'))
     &                                   lityp=.true.
                     if((tcode(i).eq.'s').or.(tcode(i).eq.'S'))
     &                                   lstyp=.true.

                     ltchr = (tcode(i).eq.'i') .or. (tcode(i).eq.'s')
     &                                         .or. (tcode(i).eq.'I')
     &                                         .or. (tcode(i).eq.'S')
                     ltcpr = (tcode(i).eq.' ')

                     if ((.not.ltchr) .and. (.not.ltcpr)) then
                         write(i4unit(-1),1007) 'Unrecognised ' //
     &                    'transition type in projectile block ', k,
     &                    ', row (relative to header) ', i,
     &                    '. Only options are " ", "s", "i"'
                         write(i4unit(-1),1002)
                         stop
                     end if

C-------------------------------------------------
C  Check that transition levels are not above
C  the maximum from the level spec earlier
C-------------------------------------------------
                     if (ltcpr) then
                         if (j1.gt.maxlev .or. j2.gt.maxlev) then
                             write(i4unit(-1),1007) 'Maximum level ' //
     &                        'input exceeded in projectile block ', k,
     &                        ', row (relative to header) ', i
                             write(i4unit(-1),1002)
                             stop
                         endif
                     endif

C-------------------------------------------------
C     ensure tcode and indexing are correct for older
C     data sets which use only the 'S' or 's' notation
C-------------------------------------------------

                     if (((tcode(i).eq.'S').or.(tcode(i).eq.'s')).and.
     &                   (iqs.eq.4)) then
                        read(buffer,f_1020) tcode(i) , caj2, caj1
                        iplus = index(caj1,'+')
                        if (iplus.ne.0) then
                            tcode(i)='t'
                            lttyp   = .true.
                        endif
                        iplus = index(caj2,'-')
                        if (iplus.ne.0) then
                            tcode(i)='t'
                            lttyp   = .true.
                        endif
                     endif

C-------------------------------------------------
C identify that specified excited levels are tied.
C-------------------------------------------------

                     if((.not.ltchr) .and. ltcpr) then
                         ltied(j1) = .true.
                         ltied(j2) = .true.
                     endif

C-------------------------------------------------
C combine input mantissa and exponent for a-value,
C gammas & bethe coeff.
C-------------------------------------------------

                     if (ltcpr) then
                        aval(i)=avalm*( 10.0D0**dble(iapow) )
                        aval(i)=dmax1(aval(i),dzero)

                        if(lbeth) then
                            beth(i,k) = bethm*( 10.0D0**dble(ibeth) )
                            if (bethm.gt.0.0d0) then
                                beth(i,k) = dmax1(beth(i,k),dzero)
                            elseif(bethm.eq.0.0d0) then
                                beth(i,k) = 0.0d0
                            else
                                beth(i,k) = -1.0D0*dmax1(abs(beth(i,k)),
     &                                                 dzero)
                            endif
                        endif

                     endif
                     do j=1,nv
                        scom(j,i,k)=gamma(j)*(10.0D0**dble(igpow(j)))
                        scom(j,i,k)=dmax1(scom(j,i,k),dzero)
                     end do


C-------------------------------------------------
C  check if energy levels are in correct order and
C  match other projectile blocks.
C  if not reverse except for type 4 files.
C-------------------------------------------------

                     if (lindx(i)) then
                        if (ltchr) then
                           i2a(i)=j2
                           i1a(i)=j1
                        elseif (wa(j2).lt.wa(j1).and.(iqs.ne.4)) then
                           i1a(i)=j2
                           i2a(i)=j1
                           write(i4unit(-1),1009) ' xxdata_06 message: U
     &pper and ',
     &                       'lower energy levels have been reversed.',
     &                       'Correct levels are - upper = ',j1,
     &                       '  lower = ',j2
                        else
                           i1a(i)=j1
                           i2a(i)=j2
                        endif
                        lindx(i) = .false.
                     else
                        if ( (i1a(i).ne.j1 .or. i2a(i).ne.j2) .and.
     &                       (i1a(i).ne.j2 .or. i2a(i).ne.j1) ) then
                           write(i4unit(-1),1007) 'Transition ' //
     &                      'ordering mismatch in projectile block ', k,
     &                      ', row (relative to header) ', i,
     &                      '. All blocks must have the same ordering.'
                           write(i4unit(-1),1002)
                           stop
                        endif
                     endif
                  endif
            endif
    6    continue
C-----------------------------------------------------------------------
      end do
C-----------------------------------------------------------------------

      if (ldata) then
         read (iunit,1010) i
            if (i.gt.0) then
               write(i4unit(-1),1001) 'adf06 data set contains > ',
     &                                ndpro,' projectile cases'
               write(i4unit(-1),1002)
               stop
            else
               np = ndpro
            endif
      endif

C-----------------------------------------------------------------------
C check that there are no untied levels
C-----------------------------------------------------------------------

      lerror = .false.

      do i = 1,maxlev
         if (.not.ltied(i)) then
            lerror = .true.
            write(i4unit(-1),1011) i
         endif
      end do

C return or terminate based on itieact setting

      if (itieactn.ne.0) then
         if (lerror) then
            write(i4unit(-1),1003)'Untied levels present'//
     &                            ' fix before proceeding'
            itieactn = 1
         else
            itieactn = 0
         endif
      else
         if (lerror) then
            write(i4unit(-1),1002)
            stop
         endif
      endif

C-----------------------------------------------------------------------
C check that type 4 dataset comprises matched up/down pairs
C-----------------------------------------------------------------------

       if (iadftyp.eq.4) then

            iwarn_umatch = 0
            iwarn_omatch = 0
            do i=1,itran
              lmatcha(i) = .false.
              imatcha(i) = 0
            enddo
            do i=1,itran-1
              if(.not.lmatcha(i)) then
                  call xxcase(tcode(i),codei,'lc')
                  if((codei.ne.'r').and.(codei.ne.'l').and.
     &               (codei.ne.'i').and.(codei.ne.'h'))then
                        if(codei.eq.'s') then
                            codet='t'
                        elseif (codei.eq.'t') then
                            codet = 's'
                        else
                            codet = codei
                        endif
                        do j = i+1,itran
                           if(.not.lmatcha(j)) then
                               call xxcase(tcode(j),codej,'lc')
                               if((i1a(j).eq.i2a(i)).and.
     &                            (i1a(i).eq.i2a(j)).and.
     &                            (codet.eq.codej))then
                                   lmatcha(i) = .true.
                                   imatcha(i) = imatcha(i)+1
                                   lmatcha(j) = .true.
                                   imatcha(j) = imatcha(i)
                               endif
                           endif
                        enddo
                  else
                      lmatcha(i) = .true.
                      imatcha(i) = 0
                  endif
              endif
            enddo

            do i=1,itran
              if(.not.lmatcha(i)) iwarn_umatch = iwarn_umatch+1
              if(imatcha(i).gt.1) iwarn_omatch = iwarn_omatch+1
            enddo
            if (iwarn_umatch.gt.0)then
                write(i4unit(-1),1001)'There are', iwarn_umatch,
     &          ' unmatched transitions in adf04 type 4 dataset'
                write(i4unit(-1),1005)
            endif
            if (iwarn_omatch.gt.0)then
                write(i4unit(-1),1001)'There are', iwarn_omatch,
     &          ' overmatched transitions in adf04 type 4 dataset'
                write(i4unit(-1),1005)
            endif

       endif

C-----------------------------------------------------------------------
 1000 format(1a3,i2,2i10,1a75)
 1001 format(1x,31('*'),' xxdata_06 error ',30('*')//
     &       1x,'Fault in input data file: ',a,i7,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,30('*'),' xxdata_06 warning ',29('*')//
     &       1x,'Fault in input data file: ',a)
 1004 format(i5)
 1005 format(/1x,29('*'),' program continues ',29('*'))
 1006 format(1x,a)
 1007 format(1x,31('*'),' xxdata_06 error ',30('*')//
     &       1x,'Fault in input data file: ',a,2(i2,a))
 1009 format(a,a/17x,2(a,i4)/)
 1010 format(1x,i3)
 1011 format(1x,31('*'),' xxdata_06 error ',30('*')//
     &       1x,'Error in input data file: level ',i4,' is untied')
 1012 format(1x,a,' - read value = ',a7)
 1014 format(1x,31('*'),' xxdata_06 error ',30('*')//
     &       1x,'Increase configuration string to ',
     &          'more than 5 characters')
C-----------------------------------------------------------------------

      return
      end
