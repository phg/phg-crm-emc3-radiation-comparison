       subroutine xxdata_25( iunit  , a25fmt , dsname ,
     &                       ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                       nddef  , ndimp  , ndein  , ndzef ,
     &                       iz0    , iz1    , outfmt ,
     &                       exfile , cxfile ,
     &                       ndens  , id_ref , densa  , denpa , denimpa,
     &                       deniona,
     &                       ntemp  , it_ref , tea    , tpa   , timpa  ,
     &                       tiona  ,
     &                       nzef   , iz_ref , zefa   ,
     &                       nbeam  , ib_ref , bmena  , denha , bmfra  ,
     &                       nimp   , im_ref , zimpa  , amimpa, frimpa ,
     &                       ts     , w      , w1     ,
     &                       cion   , cpy    , nip    , intd  , iprs   ,
     &                       ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                       nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                       jdef   , def    ,
     &                       jcor   , cor    , jmax   , epsil , fij    ,
     &                       wij
     &                     )
       implicit none
c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: xxdata_25 *******************
c
c  purpose:  To fetch  data  from  an adf25 driver dataset.
c
c  Notes:
c
c  Subroutine:
c
c   input : (i*4)  iunit    = unit number for input adf01 file.
c   input : (c*8)  a25fmt   = subdirectory type of adf25 to be read.
c   input : (c*80) dsname   = file name of adf25 format to be read.
c
c   input : (i*4)  ndtem    = maximum number of electron temperatures
c   input : (i*4)  ndden    = maximum number of electron densities
c   input : (i*4)  ndrep    = maximum number of representative n-shells
c   input : (i*4)  ndcor    = maximum number of DR bethe corrections
c   input : (i*4)  nddiel   = maximum number of DR core transitions
c   input : (i*4)  nddef    = maximum number of quantum defects
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c   input : (i*4)  ndein    = maximum number of beam energies
c   input : (i*4)  ndzef    = maximum number of z effectives
c
c   input : (i*4)  iz0      = nuclear charge of bundle-n ion
c   input : (i*4)  iz1      = recombining ion charge of bundle-n ion
c   input : (c*5)  outfmt   = format of output ADAS data format for final
c                             results
c   input : (c*80) cxfile   = file name for charge exchange data input
c   input : (c*80) exfile   = file name for map of proj. matrix output
c
c   input : (i*4)  ndens    = number of electron densities
c   input : (i*4)  id_ref   = reference electron density pointer in vectors
c   input : (i*4)  densa()  = plasma electron density vector (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  denpa()  = plasma H+ density vector (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  denimpa()= plasma mean impurity ion density (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  deniona()= total ion density (plasma+impurity) (cm-3)
c                             1st dim: index of electron density
c
c   input : (i*4)  ntemp    = number of electron temperatures
c   input : (i*4)  id_ref   = reference electron temp. pointer in vectors
c   input : (i*4)  tea()    = plasma electron temp. vector (K)
c                             1st dim: index of electron temperature
c   input : (i*4)  tpa()    = plasma H+ temp. vector (K)
c                             1st dim: index of electron temperature
c   input : (i*4)  timpa()  = plasma mean impurity ion temp (K)
c                             1st dim: index of electron temperature
c   input : (i*4)  tiona()  = mean ion temp (plasma+impurity) (K)
c                             1st dim: index of electron temperature
c
c   input : (i*4)  nzef     = number of plasma zeff
c   input : (i*4)  iz_ref   = reference zeff pointer in vector
c   input : (i*4)  zefa()   = plasma zeff vector
c                             1st dim: index of zeff
c
c   input : (i*4)  nbeam    = number of beam energies
c   input : (i*4)  ib_ref   = reference beam energy pointer in vectors
c   input : (i*4)  bmena()  = beam energy vector (ev/amu)
c                             1st dim: index of beam energies
c   input : (i*4)  denha()  = beam H+ density vector (cm-3)
c                             1st dim: index of beam energies
c   input : (i*4)  bmfra()  = fractions of beam at each energy
c                             1st dim: index of beam energies
c
c   input : (i*4)  nimp     = number of plasma impurities (excl.h+)
c   input : (i*4)  im_ref   = reference impurity pointer in vectors
c   input : (r*8)  zimpa()  = impurity species charge
c                             1st dim: index of impurity
c   input : (r*8)  amimpa() = atomic mass number of impurity species
c                             1st dim: index of impurity
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: index of impurity
c
c   input : (r*8)  ts       = external radiation field temperature (K)
c   input : (r*8)  w        = general radiation dilution factor
c   input : (i*4)  w1       = external radiation field dilution factor
c                             for photo-ionisation form the ground level.
c
c   input : (r*8)  cion     = adjustment multiplier for ground ionis.
c   input : (r*8)  cpy      = adjustment multiplier for VR xsects.
c   input : (i*4)  nip      = range of delta n for IP xsects. (le.4)
c   input : (i*4)  intd     = order of Maxw. quad. for IP xsects.(le.3)
c   input : (i*4)  iprs     = 0  => default to VR xsects. beyond nip range
c                             1  => use PR xsects. beyond nip range
c   input : (i*4)  ilow     = 0  => no special low level data accessed
c                             1  => special low level data accessed
c   input : (i*4)  ionip    = 0 =>   no ion impact collisions included
c                             1 =>ion impact excit. and ionis. included
c   input : (i*4)  nionip   = range of delta n for ion impact
c                             excitation xsects.
c   input : (i*4)  ilprs    = 0 => default to vainshtein xsects.
c                             1 => use lodge-percival-richards xsects.
c   input : (i*4)  ivdisp   = 0 => ion impact at thermal Maxw. energies
c                             1 => ion impact at displaced thermal
c                                  energies according to the neutral
c                                  beam energy parameter
c                               * if(ivdisp=0 then special low level
c                                 data for ion impact is not substituted -
c                                 only vainshtein and lodge et al.
c                                 options are open.  Electron impact
c                                 data substitution does occur.
c   input : (i*4)  nmin     = lowest n-shell for population structure
c   input : (i*4)  nmax     = highest n-shell for population structure
c   input : (i*4)  imax     = number of representative n-shells
c   input : (i*4)  nrep()   = representative n-shells
c                             1st dim: index of representative n-shell
c   input : (r*8)  wbrep()  = dilution factors for nmin->nrep() trans.
c                             1st dim: index of representative n-shell
c   input : (i*4)  jdef     = number of n-shell quantum defects
c   input : (r*8)  def()    = quantum defects for n-shells
c                             1st dim: index of n-shell quantum defects
c                                      upwards from nmin
c   input : (i*4)  jcor     = number of DR Bethe correction factors
c   input : (r*8)  cor()    = DR Bethe correction factors
c                             1st dim: index of correction factor
c   input : (i*4)  jmax     = number of DR core transitions
c   input : (r*8)  epsil()  = reduced energy of core transition
c                             [delta Eij/I_H=(z+1)^2*epsil()]
c                             1st dim: index of DR core transition
c   input : (r*8)  fij()    = absorption oscillator strength for
c                             DR core transition
c                             1st dim: index of DR core transition
c   input : (r*8)  wij()    = dilution factor for DR core transition
c                             1st dim: index of DR core transition
c
c  Routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     xxcase     adas      convert string to upper or lower case
c     xxslen     adas      locate first and last char. of string
c
c
c  Author   : Hugh Summers
c  Date     : 23-05-2007
c
c
c  Version  : 1.1
c  Date     : 23-05-2007
c  Author   : Hugh Summers
c               - First release.
c
c  Version  : 1.2
c  Date     : 21-11-2007
c  Author   : Martin O'Mullane
c               - Increase number of lines to 150 to accommodate full
c                 range of energies/temperatures/densities of 
c                 the dataformat.
c
c-----------------------------------------------------------------------
       integer    ndcod , ndline
c-----------------------------------------------------------------------
       real*8    zero
c-----------------------------------------------------------------------
       parameter (ndcod = 56 , ndline = 150 )
       parameter (zero  = 1.0d-15 )
c-----------------------------------------------------------------------
       integer    i4unit  , iunit
       integer    ndtem   , ndden  , ndrep  , ndcor  , nddiel ,
     &            nddef   , ndimp  , ndein  , ndzef
       integer    iz0     , iz1    , ndens  , id_ref , ntemp  , it_ref ,
     &            nzef    , iz_ref , nbeam  , ib_ref , nimp   , im_ref ,
     &            nip     , intd   , iprs   , ilow   , ionip  , ilprs  ,
     &            ivdisp  , nmin   , nmax   , imax   , jdef   , jcor   ,
     &            jmax    , nionip
       integer    ifirst  , ilast
       integer    iline_count      , iparm_count     , icod   , i      ,
     &            nzef_new
c-----------------------------------------------------------------------
       real*8     ts      , w      , w1     , cion   , cpy
       real*8    checksum , zeff   , zsum1  , zsum2
c-----------------------------------------------------------------------
       character  c80*80     , c320*320
       character  outfmt*5   , exfile*(*)   , cxfile*(*)
       character  dsname*(*) , cset*56      , c56*56
       character  a25fmt*8
c-----------------------------------------------------------------------
       integer    nrep(ndrep+1)
       integer    istart(ndcod )     , istop(ndcod)
       integer    index_parm(ndcod)
c-----------------------------------------------------------------------
       real*8     densa(ndden)     , denpa(ndden)    , denimpa(ndden)  ,
     &            deniona(ndden)   ,
     &            tea(ndtem)       , tpa(ndtem)      , timpa(ndtem)    ,
     &            tiona(ndtem)     ,
     &            zefa(ndzef)      , bmena(ndein)    , denha(ndein)    ,
     &            bmfra(ndein)     , zimpa(ndimp)    , amimpa(ndimp)   ,
     &            frimpa(ndimp)    , wbrep(ndrep)    , def(nddef)      ,
     &            cor(ndcor)       , epsil(nddiel)   , fij(nddiel)     ,
     &            wij(nddiel)
c-----------------------------------------------------------------------
       character  ccoda(ndcod)*9
       character  clines(ndline)*80
c-----------------------------------------------------------------------
       logical    lcoda(ndcod)
c-----------------------------------------------------------------------
       data       ccoda/'z0       ','z1       ','outfmt   ','exfile   ',
     &                  'cxfile   ','ndens    ','id_ref   ','densa    ',
     &                  'denpa    ','denimpa  ','deniona  ','ntemp    ',
     &                  'it_ref   ','tea      ','tpa      ','timpa    ',
     &                  'tiona    ','nzef     ','iz_ref   ','zefa     ',
     &                  'nbeam    ','ib_ref   ','bmena    ','denha    ',
     &                  'bmfra    ','nimp     ','im_ref   ','zimpa    ',
     &                  'amimpa   ','frimpa   ','ts       ','w        ',
     &                  'w1       ','cion     ','cpy      ','nip      ',
     &                  'intd     ','iprs     ','ilow     ','ionip    ',
     &                  'nionip   ','ilprs    ','ivdisp   ','nmin     ',
     &                  'nmax     ','imax     ','nrep     ','wbrep    ',
     &                  'jdef     ','def      ','jcor     ','cor      ',
     &                  'jmax     ','epsil    ','fij      ','wij      '/

c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  fetch in data lines, identify parameters and parameter pointers
c-----------------------------------------------------------------------

        do icod = 1, ndcod
          lcoda(icod)=.false.
        enddo

        iline_count = 0
        iparm_count = 0

   10   c80=' '
        read(iunit,'(1a80)')c80
        if(c80(1:2).ne.'c-')then
            iline_count=iline_count+1
            clines(iline_count)=c80
            if(c80(10:10).eq.':')then
                if(c80(1:10).eq.'         :')then
                    istop(iparm_count)=iline_count
                else
                    iparm_count=iparm_count+1
                    istart(iparm_count)=iline_count
                    istop(iparm_count)=istart(iparm_count)
                    do icod=1,ndcod
                      if(c80(1:9).eq.ccoda(icod))then
                          index_parm(icod) = iparm_count
                          lcoda(icod)=.true.
                      endif
                    enddo
                endif
            endif
            go to 10
        endif
c-----------------------------------------------------------------------
c  process parameters present in turn
c-----------------------------------------------------------------------
        do icod=1,ndcod
          if(lcoda(icod))then
             if (icod .eq. 1) then
                 read(clines(istart(index_parm(icod)))(11:80),*)iz0
             elseif(icod .eq. 2) then
                 read(clines(istart(index_parm(icod)))(11:80),*)iz1
             elseif(icod .eq. 3) then
                 outfmt=' '
                 call xxslen(clines(istart(index_parm(icod)))(11:80),
     &                       ifirst,ilast)
                 outfmt(1:ilast-ifirst+1)=
     &               clines(istart(index_parm(icod)))
     &                    (10+ifirst:10+ilast)
             elseif(icod .eq. 4) then
                 exfile=' '
                 call xxslen(clines(istart(index_parm(icod)))(11:80),
     &                       ifirst,ilast)
                 exfile(1:ilast-ifirst+1)=
     &               clines(istart(index_parm(icod)))
     &               (10+ifirst:10+ilast)
             elseif(icod .eq. 5) then
                 cxfile=' '
                 call xxslen(clines(istart(index_parm(icod)))(11:80),
     &                       ifirst,ilast)
                 cxfile(1:ilast-ifirst+1)=
     &               clines(istart(index_parm(icod)))
     &               (10+ifirst:10+ilast)
             elseif (icod .eq. 6) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ndens
             elseif(icod .eq. 7) then
                 read(clines(istart(index_parm(icod)))(11:80),*)id_ref
             elseif(icod .eq. 8) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(densa(i),i=1,ndens)
             elseif(icod .eq. 9) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(denpa(i),i=1,ndens)
             elseif(icod .eq. 10) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(denimpa(i),i=1,ndens)
             elseif(icod .eq. 11) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(deniona(i),i=1,ndens)
             elseif (icod .eq. 12) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ntemp
             elseif(icod .eq. 13) then
                 read(clines(istart(index_parm(icod)))(11:80),*)it_ref
             elseif(icod .eq. 14) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(tea(i),i=1,ntemp)
             elseif(icod .eq. 15) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(tpa(i),i=1,ntemp)
             elseif(icod .eq. 16) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(timpa(i),i=1,ntemp)
             elseif(icod .eq. 17) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(tiona(i),i=1,ntemp)
             elseif (icod .eq. 18) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nzef
             elseif(icod .eq. 19) then
                 read(clines(istart(index_parm(icod)))(11:80),*)iz_ref
             elseif(icod .eq. 20) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(zefa(i),i=1,nzef)
c
             elseif (icod .eq. 21) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nbeam
             elseif(icod .eq. 22) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ib_ref
             elseif(icod .eq. 23) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(bmena(i),i=1,nbeam)
             elseif(icod .eq. 24) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(denha(i),i=1,nbeam)
             elseif(icod .eq. 25) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(bmfra(i),i=1,nbeam)
c
             elseif (icod .eq. 26) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nimp
             elseif(icod .eq. 27) then
                 read(clines(istart(index_parm(icod)))(11:80),*)im_ref
             elseif(icod .eq. 28) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(zimpa(i),i=1,nimp)
             elseif(icod .eq. 29) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(amimpa(i),i=1,nimp)
             elseif(icod .eq. 30) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(frimpa(i),i=1,nimp)
c
             elseif (icod .eq. 31) then
                 read(clines(istart(index_parm(icod)))(11:80),
     &                 '(1e10.2)')ts
             elseif (icod .eq. 32) then
                 read(clines(istart(index_parm(icod)))(11:80),
     &                 '(1e10.2)')w
             elseif (icod .eq. 33) then
                 read(clines(istart(index_parm(icod)))(11:80),
     &                 '(1e10.2)')w1
c
             elseif (icod .eq. 34) then
                 read(clines(istart(index_parm(icod)))(11:80),
     &                 '(1e10.2)')cion
             elseif (icod .eq. 35) then
                 read(clines(istart(index_parm(icod)))(11:80),
     &                 '(1e10.2)')cpy
             elseif(icod .eq. 36) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nip
             elseif(icod .eq. 37) then
                 read(clines(istart(index_parm(icod)))(11:80),*)intd
             elseif(icod .eq. 38) then
                 read(clines(istart(index_parm(icod)))(11:80),*)iprs
             elseif(icod .eq. 39) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ilow
             elseif(icod .eq. 40) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ionip
             elseif(icod .eq. 41) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nionip
             elseif(icod .eq. 42) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ilprs
             elseif(icod .eq. 43) then
                 read(clines(istart(index_parm(icod)))(11:80),*)ivdisp
c
             elseif(icod .eq. 44) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nmin
             elseif(icod .eq. 45) then
                 read(clines(istart(index_parm(icod)))(11:80),*)nmax
             elseif(icod .eq. 46) then
                 read(clines(istart(index_parm(icod)))(11:80),*)imax
             elseif(icod .eq. 47) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(50i5)')(nrep(i),i=1,imax)
             elseif(icod .eq. 48) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(28e10.2)')(wbrep(i),i=1,imax)
             elseif(icod .eq. 49) then
                 read(clines(istart(index_parm(icod)))(11:80),*)jdef
             elseif(icod .eq.50) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(25f10.5)')(def(i),i=1,jdef)

             elseif(icod .eq. 51) then
                 read(clines(istart(index_parm(icod)))(11:80),*)jcor
             elseif(icod .eq. 52) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(25f10.5)')(cor(i),i=1,jcor)
             elseif(icod .eq. 53) then
                 read(clines(istart(index_parm(icod)))(11:80),*)jmax
             elseif(icod .eq. 54) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(25f10.5)')(epsil(i),i=1,jmax)
             elseif(icod .eq. 55) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(25f10.5)')(fij(i),i=1,jmax)
             elseif(icod .eq. 56) then
                 c320=' '
                 do i=istart(index_parm(icod)),istop(index_parm(icod))
                   c320(70*(i-istart(index_parm(icod)))+1:
     &                 70*(i-istart(index_parm(icod))+1))=
     &                 clines(i)(11:80)
                 enddo
                 read(c320,'(25f10.5)')(wij(i),i=1,jmax)

             endif

          endif

        enddo
c-----------------------------------------------------------------------
c  make checks on parameters according to adf25 subformat type and fill
c  parameter availability logical string
c-----------------------------------------------------------------------
        if(a25fmt.eq.'a25_p316') then
            if(lcoda(8))then
                write(i4unit(-1),1003)ccoda(8)
                write(i4unit(-1),1002)
            endif
            if(lcoda(9))then
                write(i4unit(-1),1003)ccoda(9)
                write(i4unit(-1),1002)
            endif
            if(lcoda(10))then
                write(i4unit(-1),1003)ccoda(10)
                write(i4unit(-1),1002)
            endif
            if(lcoda(15))then
                write(i4unit(-1),1003)ccoda(15)
                write(i4unit(-1),1002)
            endif
            if(lcoda(16))then
                write(i4unit(-1),1003)ccoda(16)
                write(i4unit(-1),1002)
            endif
c-------  warn if hydrogen in impurity list
            checksum=0.0d0
            do i=1,nimp
              if(zimpa(i).eq.1) then
                 write(i4unit(-1),1004)'Hydrogen in impurity list'
                 write(i4unit(-1),1002)
              endif
              checksum=checksum+frimpa(i)
            enddo
c-------  warn if impurity fractions need renormalising
            if(dabs(checksum-1.0d0).gt.zero) then
                write(i4unit(-1),1004)
     &             'impurity fractions have been renormalised'
                write(i4unit(-1),1002)
                do i=1,nimp
                  frimpa(i)=frimpa(i)/checksum
                enddo
            endif
c-------  warn if zeff range must be curtailed
            zsum1=0.0d0
            zsum2=0.0d0
            nzef_new = nzef
            do i=1,nimp
              zsum1=zsum1+zimpa(i)*frimpa(i)
              zsum2=zsum2+zimpa(i)**2*frimpa(i)
            enddo
            do i=1,nzef
              zeff=zefa(i)
              if((zeff-(zsum2/zsum1).gt.zero)) then
                  write(i4unit(-1),1004)
     &               'zeff range has been restricted: nzef = ',i-1
                  write(i4unit(-1),1002)
                  nzef_new=i-1
                  go to 100
              endif
            enddo
  100       nzef=nzef_new
            if(nzef_new.le.0) then
                write(i4unit(-1),1005)'Consistent zeff range is zero'
                write(i4unit(-1),1006)
                stop
            else
                nzef=nzef_new
            endif

        endif

        write(c56,'(56l1)')(lcoda(icod),icod=1,ndcod)
        call xxcase(c56,cset,'lc')
        if(index(cset,'f').gt.0) then
            write(i4unit(-1),1001)cset
            write(i4unit(-1),1002)
        endif

        return
c-----------------------------------------------------------------------
 1001 format(1x,29('*'),' xxdata_25 warning ',29('*')//
     &       1x,'Unset values: cset= ',a)
 1002 format(/1x,29('*'),' Program continues ',29('*'))
 1003 format(1x,29('*'),' xxdata_25 warning ',29('*')//
     &       1x,'ccod = ',1a9,' data redundant: may be overwritten.')
 1004 format(1x,29('*'),' xxdata_25 warning ',29('*')//
     &       1x,a,i3)
 1005 format(1x,29('*'),' xxdata_25 error   ',29('*')//
     &       1x,a)
 1006 format(/1x,29('*'),'Program terminated',29('*'))
c-----------------------------------------------------------------------

      end
