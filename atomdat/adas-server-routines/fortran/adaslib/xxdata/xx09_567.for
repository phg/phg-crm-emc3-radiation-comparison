      subroutine xx09_567(  iunit    , ndprt    , ndrep    , ndlrep   ,
     &                      ndlev    , ndaug    , ndt      , ndsc     ,
     &                      ctype    , iform    ,
     &                      nprnt    , nprnti   , nprntf   ,
     &                      bwnp     ,
     &                      ipa      , cstrpa   , ispa     , ilpa     ,
     &                      xjpa     , wpa      ,
     &                      ipca     , cstrpca  , xjpca    , wpca     ,
     &                      ncfg     , nippy    , bwnr     ,
     &                      ia       , ip       , cstrga   , isa      ,
     &                      xja      , wa       ,
     &                      iscp     , iscn     , iscl     ,
     &                      fsc      , wnsc     ,
     &                      auga_res , lauga_res,
     &                      nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                      irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                      lauga_nl ,
     &                      nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                      irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                      iprti    ,
     &                      diel_res , ldiel_res,
     &                      iprtf    ,
     &                      nsysf    , isys     , ispsys   , cspsys   ,
     &                      diel_nl  , ldiel_nl , diel_n   , ldiel_n  ,
     &                      diel_sum , ldiel_sum, diel_tot ,
     &                      nte      , tea
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: xx09_234 *******************
c
c  purpose:  Helper routine to read Badnell hybrid IC/LS and CA
c            adf09 files as defined in year 2015 (W DR Project); 
c            these correspond to iform = 5, 6, 7. All variables 
c            are documented in xxdata_09.for.
c
c  calling program:  xxdata_09
c
c  version  : 1.1
c  date     : 08-04-2016
c  modified : Martin O'Mullane
c               - Based on original xxdata_09.for.
c
c  version  : 1.2
c  date     : 08-01-2018
c  modified : Martin O'Mullane
c               - Add ipca, cstrpca, xjpca and wpca for the parent
c                 config for types 5 and 6 (hybrid ls/ic).
c               - Return isa() for LS-hybrid form (type 5).
c
c-----------------------------------------------------------------------
      integer   ndword
c-----------------------------------------------------------------------
      parameter(ndword = 80 )
c-----------------------------------------------------------------------
      integer   i4unit      , lenstr
      integer   iunit       , ndprt          , ndrep      , ndlrep     ,
     &          ndlev       , ndaug          , ndt        , ndsc
      integer   nprnt       , nprnti         , nprntf     , inrep      ,
     &          nlvl        , nrep           , iaprs_n    , nvals      ,
     &          nlrep       , ilrep          , ipoint     , iaprs_nl   ,
     &          nte         , ncfg           , nippy
      integer   indx        , indx1          , indx2
      integer   iline       , i              , j          ,
     &          ipi         , ipf            , ipfs       ,
     &          iword       , nwords         ,
     &          iform       , jmin           , irep
      integer   ind1        , ind2           , istart     , istop
      integer   indx_fb     , indx_lb        , ilen_index ,
     &          ilen_cnfg   , ilen_wnf       , ilen_indp  ,
     &          ilen_s      , ilen_l         , ilen_j
      integer   ilen_f_1004 , ilen_f_1005    , ilen_f_1006, ilen_f_1008
c-----------------------------------------------------------------------
      integer   ipa(ndprt)  , ispa(ndprt)    , ilpa(ndprt)
      integer   ia(ndlev)   , isa(ndlev)     , ila(ndlev) ,
     &          ip(ndlev)   , ipca(ndprt)
      integer   iscp(ndsc)  , iscn(ndsc)     , iscl(ndsc)
      integer   irepa_n(ndrep), nrepa(ndrep) , ipaug_n(ndaug,2)
      integer   iprti(ndprt)
      integer   iprtf(ndprt,ndprt)
      integer   nsysf(ndprt,ndprt)
      integer   isys(ndprt,ndprt,2), ispsys(ndprt,ndprt,2)
      integer   irepa_nl(ndlrep)   , nlrepa_n(ndlrep)     ,
     &          nlrepa_l(ndlrep)   , ipaug_nl(ndaug,2)
      integer   ifirst(ndword)     , ilast(ndword)
c-----------------------------------------------------------------------
      real*8    temp        , bwnp           , bwnr
c-----------------------------------------------------------------------
      real*8    xjpa(ndprt)  , wpa(ndprt)
      real*8    xjpca(ndprt) , wpca(ndprt)
      real*8    xja(ndlev)   , wa(ndlev)
      real*8    tea(ndt)
      real*8    auga_nl(ndlrep,ndaug) , auga_n(ndrep,ndaug)
      real*8    diel_res(ndlev,ndprt,ndt)
      real*8    diel_nl(ndlrep,ndprt,ndprt,2,ndt)
      real*8    diel_n(ndrep,ndprt,ndprt,2,ndt)
      real*8    diel_sum(ndprt,ndprt,2,ndt)
      real*8    diel_tot(ndprt,ndt)
      real*8    auga_res(ndlev,ndprt)
      real*8    fsc(ndsc,2) , wnsc(ndsc,2)
c-----------------------------------------------------------------------
      character ctype*2
      character cdelim*7    , c10*10         , c30*30   , c2*2 , c1*1
      character c80*80      , c128*128       ,
     &          cline*80    , buffer*128     , c512*712
      character f_1004*45   , f_1005*33      , f_1006*45, f_1008*45
c-----------------------------------------------------------------------
      character cstrpa(ndprt)*(*) , cstrpca(ndprt)*(*) ,
     &          cstrga(ndlev)*(*)
      character codepa(ndprt)*1
      character caprs_n(ndaug)*10
      character caprs_nl(ndaug)*10
      character cprti(ndprt)*10
      character cspsys(ndprt,ndprt,2)*30
c-----------------------------------------------------------------------
      logical   ldata       , lnopi
      logical   ldr_res     , ldr_nl         , ldr_n
c-----------------------------------------------------------------------
      logical   lauga_res(ndlev,ndprt)
      logical   lauga_n(ndrep,ndaug)
      logical   lauga_nl(ndlrep,ndaug)
      logical   l_prt(ndprt,ndprt)
      logical   ldiel_res(ndlev,ndprt)
      logical   ldiel_nl(ndlrep,ndprt,ndprt,2)
      logical   ldiel_n(ndrep,ndprt,ndprt,2)
      logical   ldiel_sum(ndprt,ndprt,2)
c-----------------------------------------------------------------------
      data      cdelim / ' ()<>{}' /
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c locate and read recombining ion (parent) level data
c and determine formatting and read
c  - parent level or config (ic/ca) : nprnti
c  - parent config (ic/ca)          : nprntf
c  - ic/ca averaged configs         : ncfg
c-----------------------------------------------------------------------

   20 read(iunit,'(a)') c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'bwnp')
      if (indx.le.0) then
         go to 20
      else
         read(buffer(indx+5:indx+16),'(f12.1)') bwnp
      endif

      indx=index(buffer,'nprnt=')
      read(buffer(indx+6:indx+7),'(i2)') nprnti    ! IC/LS/CA parent

      read(iunit,'(a)')cline,cline,cline

      f_1004 = ' '
      f_1005 = ' '
      read(iunit,'(a)')cline
      call xxcase (cline , c80, 'lc')
      call xxlast (c80, ')',indx_lb)
      indx_fb   = index(c80,'(')

      j     = 1
      iword = ndword
      call xxword( c80, ' '   , j        , iword        ,
     &             ifirst     , ilast    , nwords
     &           )
      ilen_index = len(c80(1:ilast(1)))

      ilen_cnfg = indx_fb-ilen_index-2
      ilen_j    = indx_lb-indx_fb-1
      ilen_wnf  = lenstr(c80)-indx_lb
      if (ctype.ne.'ca') then
          ind1=index(c80(indx_fb+1:indx_lb-1),')')
          call xxlast(c80(indx_fb+1:indx_lb-1),'(',ind2)
          ilen_s= ind1-1
          ilen_l= ind2-ind1-1
          ilen_j= indx_lb-indx_fb-ind2-1
          write(f_1004,'(a2,i1,a7,i2,a5,i1,a5,i1,a5,i1,a7,i2,a6)')
     &             '(i',ilen_index,',1a1,1a',ilen_cnfg,',1x,i',ilen_s,
     &          ',1x,i', ilen_l,',1x,f',ilen_j,'.1,1x,f',ilen_wnf,
     &           '.1,a1)'
          ilen_f_1004  = ilen_index+ilen_cnfg+ilen_s+ilen_l+ilen_j+
     &                   ilen_wnf+5
      else
          write(f_1005,'(a2,i1,a7,i2,a5,i1,a7,i2,a6)')
     &             '(i',ilen_index,',1a1,1a',ilen_cnfg,',1x,f',
     &               ilen_j,'.1,1x,f',ilen_wnf,'.1,a1)'
          ilen_f_1005 = ilen_index + ilen_cnfg + ilen_j + ilen_wnf + 3
      endif


      backspace(iunit)

      do i = 1,nprnti

         read (iunit,'(a80)') cline
         read (cline,'(i6)')  iline

         if (iline.le.0) then
            ldata=.false.
         elseif (iline.ne.i) then
            write(i4unit(-1),1001) 'parent level index',iline,
     &                    ' out of order'
            write(i4unit(-1),1002)
            stop
         else
            if(ctype.ne.'ca') then
                read(cline,f_1004)ipa(i)    , codepa(i) ,
     &                            cstrpa(i) , ispa(i)   ,
     &                            ilpa(i)   , xjpa(i)   , wpa(i)

            else
                read(cline,f_1005)ipa(i)    , codepa(i) ,
     &                            cstrpa(i) , xjpa(i)   , wpa(i)
                ilpa(i)=0
                ispa(i)=0
            endif
         endif

      enddo


   25 read(iunit,'(a)') c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'nprnt=')
      if(indx.le.0) then
          go to 25
      else
         read(c128(indx+6:indx+7),'(i2)') nprntf
      endif

      read(iunit,'(a)')cline,cline,cline

      f_1006 = ' '
      read(iunit,'(a)')cline
      ilen_j = indx_lb - indx_fb - 1

      write(f_1006,'(a2,i1,a7,i2,a5,i1,a7,i2,a6)')
     &         '(i',ilen_index,',1a1,1a',ilen_cnfg,',1x,f',
     &           ilen_j,'.1,1x,f',ilen_wnf,'.1,a1)'
      ilen_f_1006 = ilen_index + ilen_cnfg + ilen_j + ilen_wnf + 3


      backspace(iunit)

      do i = 1,nprntf

         read (iunit,'(a80)') cline
         read (cline,'(i6)')  iline

         if (iline.le.0) then
            ldata=.false.
         elseif (iline.ne.i) then
            write(i4unit(-1),1001) 'parent level index',iline,
     &                    ' out of order'
            write(i4unit(-1),1002)
            stop
         else
            read(cline,f_1006)ipca(i)    , codepa(i) ,
     &                        cstrpca(i) , xjpca(i)   , wpca(i)
            if (i.gt.nprnti) ilpa(i)=0
            if (i.gt.nprnti) ispa(i)=0
         endif

      enddo

      nprnt = nprntf


   26 read(iunit,'(a)') c128
      call xxcase(c128 , buffer, 'lc')
      indx=index(buffer,'ncfg=')
      if(indx.le.0) then
          go to 26
      else
         read(buffer(indx+5:indx+9),'(i4)') ncfg
         indx = index(buffer,'bwnr=')
         read(buffer(indx+5:indx+16),'(f12.1)') bwnr
      endif

      read(iunit,'(a)')cline,cline,cline

      f_1008 = ' '
      read(iunit,'(a)')cline
      j     = 1
      iword = ndword
      call xxword( cline, ' ' , j        , iword        ,
     &             ifirst     , ilast    , nwords
     &           )
      ilen_indp = len(c80(ilast(1):ifirst(2)))-1
      ilen_cnfg = indx_fb - ifirst(2) - 1

      ilen_f_1008 = ilen_index + ilen_cnfg + ilen_j + ilen_wnf + 3

c type 5 has a valid isa() value
      
      if (iform.eq.5) then

         write(f_1008,'(a2,i1,a2,i1,a2,i2.2,a8,i2.2,a7,i2.2,a3)')
     &            '(i',ilen_index,
     &            ',i',ilen_indp,
     &            ',a',ilen_cnfg,
     &            ',1x,i1,f',ilen_j-1,
     &            '.1,1x,f',ilen_wnf,
     &            '.1)'

         backspace(iunit)

         do i = 1, ncfg

           read (iunit,'(a128)') c128
           read(c128,f_1008) ia(i)     , ip(i)    ,
     &                       cstrga(i) , isa(i)   , xja(i)   ,
     &                       wa(i)

           ila(i)=0

         enddo
      
      else
      
         write(f_1008,'(a2,i1,a2,i1,a2,i2.2,a5,i2.2,a7,i2.2,a3)')
     &            '(i',ilen_index,
     &            ',i',ilen_indp,
     &            ',a',ilen_cnfg,
     &            ',1x,f',ilen_j,
     &            '.1,1x,f',ilen_wnf,
     &            '.1)'

         backspace(iunit)

         do i = 1, ncfg

           read (iunit,'(a128)') c128
           read(c128,f_1008) ia(i)     , ip(i)    ,
     &                       cstrga(i) , xja(i)   ,
     &                       wa(i)

           ila(i)=0
           isa(i)=0

         enddo
      
      endif

      ldata=.true.

c-----------------------------------------------------------------------
c locate and read representative nl-level and auger rate data
c-----------------------------------------------------------------------

   40 read(iunit,'(a)')c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'nlrep=')
      if(indx.le.0) then
         go to 40
      else
         read(buffer(indx+6:indx+16),'(i3)') nlrep
      endif

      if (nlrep.gt.ndlrep) then
         write(i4unit(-1),1000) 'number of nl-shells ', nlrep,
     &                 ' > dimension permitted ', ndlrep
         write(i4unit(-1),1010)
      endif

      read(iunit,'(a)')cline,cline,cline

      do ilrep=1,min(nlrep, ndlrep)
         read(iunit,*)irepa_nl(ilrep), nlrepa_n(ilrep),nlrepa_l(ilrep)
      end do

c-----------------------------------------------------------------------
c locate and read nl-shell shells straddling the IP
c-----------------------------------------------------------------------

   50 read(iunit,'(a)')c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'nippy=')
      if(indx.le.0) then
         go to 50
      else
         read(buffer(indx+6:indx+10),*) nippy
      endif

      if (nippy.gt.ndsc) then
         write(i4unit(-1),1000) 'number of nl-straddling levels ', 
     &                          nippy,
     &                          ' > dimension permitted ', ndsc
         write(i4unit(-1),1002)
         stop
      endif

      read(iunit,'(a)')cline,cline,cline

      do i = 1, nippy
         read(iunit,*)iscp(i), iscn(i), iscl(i), fsc(i,1), wnsc(i,1),
     &                fsc(i,2), wnsc(i,2)
      enddo

c-----------------------------------------------------------------------
c locate and read representative n-level and auger rate data
c-----------------------------------------------------------------------

   60 read(iunit,'(a)')c128
      call xxcase(c128 , buffer, 'lc')
      indx = index(buffer,'nrep=')
      if(indx.le.0) then
         go to 60
      else
         read(buffer(indx+6:indx+16),'(i3)') nrep
      endif

      read(iunit,'(a)')cline,cline,cline

      do inrep=1,nrep
         read(iunit,*)irepa_n(inrep), nrepa(inrep)
      enddo

c-----------------------------------------------------------------------
c locate data block for each initial parent and get header information
c-----------------------------------------------------------------------

      do ipi=1,ndprt
        do ipf=1,ndprt
            l_prt(ipi,ipf)=.false.
         enddo
      enddo


      do 140 ipi = 1, nprnti

   75   read(iunit,'(a)',end=999) c128
        call xxcase(c128,buffer,'lc')

        indx  = index(buffer,'prti=')
        indx1 = index(buffer,'alft')
        indx2 = index(buffer,'c----')

        if((indx.le.0).and.(indx1.le.0).and.(indx2.le.0)) then
            go to 75
        elseif(indx1.gt.0)then
            backspace(iunit)
            go to 150
        elseif(indx2.gt.0)then
            go to 200
        endif

        indx = index(buffer,'prti=')
        read(buffer(indx+5:indx+6),'(i2)') iprti(ipi)

        if((index(buffer,'lvlprt').gt.0).or.
     &     (index(buffer,'trmprt').gt.0).or.
     &     (index(buffer,'cfgprt').gt.0)) then
            c10=' '
            call xxlast(buffer, ')',indx_lb)
            indx_fb = index(buffer,'(')
            c10(1:indx_lb-indx_fb-1)=buffer(indx_fb+1:indx_lb-1)
            call xxcase(c10,cprti(ipi),'uc')
        endif

c-----------------------------------------------------------------------
c locate temperature set
c-----------------------------------------------------------------------

  102   read(iunit,'(1a128)')c128
        call xxcase(c128,buffer,'lc')
        indx  = index(buffer,'indx te')
        indx1 = index(buffer,'prtf')
        if((indx.le.0).and.(indx1.le.0))then
            go to 102
        elseif(indx1.gt.0) then
            write(i4unit(-1),1001)'No temperature data for parent',ipi
            write(i4unit(-1),1002)
            stop
        else
            backspace(iunit)
        endif

        c512 = ' '
        ipoint=1
 103    c128 = ' '
        read(iunit,'(1a128)')c128
        call xxcase(c128 , buffer, 'lc')
        indx = index(buffer,'=')
        indx1= index(buffer,'--')
        if ((indx.le.0).and.(indx1.le.0))then
            c512(ipoint:ipoint+128)=buffer
            ipoint=ipoint+128
        elseif(indx.gt.0) then
            c512(ipoint:128-indx-1) = buffer(indx+1:128)
            ipoint=128-indx
            go to 103
        else
            call xxslen(buffer(12:128),istart,istop)
            if(istart.gt.0)then
               c512(ipoint:ipoint+103)=buffer(12:128)
               ipoint=ipoint+104
            endif
        endif

        j     = 1
        iword = ndword

        call xxword( c512      , cdelim    , j  ,
     &               iword     ,
     &               ifirst    , ilast     , nwords    )

        nte = iword
        if (nte.gt.ndt) then
            write(i4unit(-1),1001)
     &    'number of temperatures in adf09 file is greater than ndt'
            stop
        endif

        do j=1,nte
           read(c512(ifirst(j):ilast(j)),'(1e8.2)')tea(j)
        enddo

c-----------------------------------------------------------------------
c locate low-level resolved dielectronic coeffts. for initial parent
c-----------------------------------------------------------------------

        ldata=.true.

        do i = 1, ndlev
          if (ldata) then
              read (iunit,'(a128)') buffer
              read (buffer,'(i6)')  iline

c  diel. recom. input information is terminated by iline= 0 or -1

              if (iline.le.0) then
                  ldata=.false.
                  nvals=i-1
              else
                  backspace(iunit)
                  read (iunit,*) indx,
     &                           (diel_res(indx,ipi,j),j=1,nte)
                  ldiel_res(indx,ipi)=.true.
                  ldr_res = .true.
              endif
          endif
        end do

c-----------------------------------------------------------------------
c locate data block for each final parent
c-----------------------------------------------------------------------

        ipf   = 1
        lnopi = .true.
        do while (ipf.le.nprntf.and.lnopi)
c--------------------------------------------
c we will eventually run out of dataset -
c in this case end reading
c--------------------------------------------
   85     read(iunit,'(1a128)',end=999) c128
          call xxcase(c128,buffer,'lc')
          indx  = index(buffer,'prtf=')
          indx1 = index(buffer,'prti=')
c-----------------------------------------------------------------------
c not all final parents are present in the adf09 dataset. if we read
c prti indicating a new block rewind one record and pass control back
c to the initial parent loop. additionally, if we detect the presence
c of tabulated totals (as present in 2000 style datasets) then jump to
c reading those.
c-----------------------------------------------------------------------
          if (indx1.gt.0) then
             backspace(iunit)
             lnopi = .false.
             goto 130
          endif
          if(indx.le.0) then
              indx1 = index(buffer,'alft( 1)')
              if (indx1.gt.0) then
                  backspace(iunit)
                  goto 150
              endif
              go to 85
          endif

          indx=index(buffer,'prtf=')
          read(buffer(indx+5:indx+6),'(i2)') iprtf(ipi,ipf)

          if(ipf.lt.iprtf(ipi,ipf))then
              nsysf(ipi,ipf)=0
              ipf=ipf+1
              backspace(iunit)
              go to 85
          endif

          if (index(buffer,'cfgprt').gt.0) then
              c30=' '
              c10=' '
              call xxlast (buffer, ')',indx_lb)
              indx_fb = index(buffer,'(')
              c10(1:indx_lb-indx_fb-1)=buffer(indx_fb+1:indx_lb-1)
              call xxcase(c10,c30(1:10),'uc')
          endif

          if ((iform.eq.6).or.(iform.eq.7)) then
              nsysf(ipi,ipf) = 1
              isys(ipi,ipf,nsysf(ipi,ipf)) = 1
              ipfs=1
              write(c30(11:20),'(1a8,1i2)')'  isys =',nsysf(ipi,ipf)
              c2  = ' *'
              write(c30(21:30),'(1a8,1a2)')'  ssys =',c2
              cspsys(ipi,ipf,ipfs)=c30
c--------------------------------------------------
c get ilrep and inrep data for 'ic' and 'ca' cases
c--------------------------------------------------
  86          read(iunit,'(1a128)')c128
              call xxcase(c128,buffer,'lc')
              if(index(buffer,'ilrep').gt.0) then
                   read(iunit,'(1a128)')c128
  87               read(iunit,'(1a128)')c128
                   read(c128(1:6),'(i6)')indx
                   if(indx.gt.0) then
                       backspace(iunit)
                       jmin=1
                       read(iunit,'(i6,5x,10e10.2)')indx,
     &                (diel_nl(indx,ipi,ipf,ipfs,j),j=jmin,min(10,nte))
                       ldiel_nl(indx,ipi,ipf,ipfs)=.true.
                       ldr_nl = .true.
                       jmin = jmin+10
                       do while (jmin.lt.nte)
                          read (iunit,'(11x,10e10.2)')
     &                         (diel_nl(indx,ipi,ipf,ipfs,j),
     &                          j=jmin,min(jmin+9,nte))
                          jmin=jmin+10
                       enddo
                       go to 87
                   else
                       go to 86
                   endif
              elseif(index(buffer,'inrep').gt.0) then
                   read(iunit,'(1a128)')c128
  88               read(iunit,'(1a128)')c128
                   read(c128(1:6),'(i6)')indx
                   if(indx.gt.0) then
                       backspace(iunit)
                       jmin=1
                       read(iunit,'(i6,5x,10e10.2)')irep,
     &                (diel_n(indx,ipi,ipf,ipfs,j),j=jmin,min(10,nte))
                       ldiel_n(indx,ipi,ipf,ipfs)=.true.
                       ldr_n = .true.
                       jmin = jmin+10
                       do while (jmin.lt.nte)
                          read (iunit,'(11x,10e10.2)')
     &                         (diel_n(indx,ipi,ipf,ipfs,j),
     &                          j=jmin,min(jmin+9,nte))
                          jmin=jmin+10
                       enddo
                       go to 88
                   else
                       go to 86
                   endif
              elseif((index(buffer,'prtf').gt.0).or.
     &                 (index(buffer,'prti').gt.0).or.
     &                 (index(buffer,'alft').gt.0)) then
                    backspace(iunit)
              endif

          elseif (iform.eq.5) then

              indx1=index(buffer,'nsys=')
              read(buffer(indx1+5:indx1+6),'(i2)') nsysf(ipi,ipf)
c--------------------------------------------------
c      find and process each spin system
c--------------------------------------------------
              do ipfs=1,nsysf(ipi,ipf)

   90           read(iunit,'(1a128)') c128
                call xxcase(c128,buffer,'lc')
                indx=index(buffer,'spnsys')
                if(indx.le.0) then
                    go to 90
                endif

                indx=index(buffer,'spnsys=')
                read(buffer(indx+7:indx+11),'(i2)')ispsys(ipi,ipf,ipfs)

                indx=index(buffer,' sys=')
                if(indx.le.0) then
                    if((ispsys(ipi,ipf,ipfs).ne.ispa(ipf)+1).and.
     &                  (ispsys(ipi,ipf,ipfs).ne.ispa(ipf)-1))then
                          write(i4unit(-1),1000)'spin system ',
     &                      ispsys(ipi,ipf,ipfs),
     &                      ' mismatch with final parent ', ipf
                          write(i4unit(-1),1002)
                          stop
                    else
                          isys(ipi,ipf,ipfs)=ipfs
                    endif
                else
                         read(buffer(indx+5:indx+6),'(i2)')
     &                          isys(ipi,ipf,ipfs)
                endif
                write(c30(11:20),'(1a8,1i2)')'  isys =',
     &                            isys(ipi,ipf,ipfs)
                write(c30(21:30),'(1a8,1i2)')'  ssys =',
     &                            ispsys(ipi,ipf,ipfs)
                cspsys(ipi,ipf,ipfs)=c30

c--------------------------------------------------
c get ilrep and inrep data for 'ls' and 'ca' cases
c--------------------------------------------------

  96             read(iunit,'(1a128)')c128
                 call xxcase(c128,buffer,'lc')
                 if(index(buffer,'ilrep').gt.0) then
                      read(iunit,'(1a128)')c128
  97                  read(iunit,'(1a128)')c128
                      read(c128(1:6),'(i6)')indx
                      if(indx.gt.0) then
                          backspace(iunit)
                          jmin=1
                          read(iunit,'(i6,5x,10e10.2)')indx,
     &                   (diel_nl(indx,ipi,ipf,ipfs,j),
     &                            j=jmin,min(10,nte))
                          ldiel_nl(indx,ipi,ipf,ipfs)=.true.
                          ldr_nl = .true.
                          jmin = jmin+10
                          do while (jmin.lt.nte)
                             read (iunit,'(11x,10e10.2)')
     &                            (diel_nl(indx,ipi,ipf,ipfs,j),
     &                             j=jmin,min(jmin+9,nte))
                             jmin=jmin+10
                          enddo
                          go to 97
                      else
                          go to 96
                      endif
                 elseif(index(buffer,'inrep').gt.0) then
                      read(iunit,'(1a128)')c128
  98                  read(iunit,'(1a128)')c128
                      read(c128(1:6),'(i6)')indx
                      if(indx.gt.0) then
                          backspace(iunit)
                          jmin=1
                          read(iunit,'(i6,5x,10e10.2)')irep,
     &                        (diel_n(indx,ipi,ipf,ipfs,j),
     &                                j=jmin,min(10,nte))
                          ldiel_n(indx,ipi,ipf,ipfs)=.true.
                          ldr_n = .true.
                          jmin = jmin+10
                          do while (jmin.lt.nte)
                             read (iunit,'(11x,10e10.2)')
     &                            (diel_n(indx,ipi,ipf,ipfs,j),
     &                             j=jmin,min(jmin+9,nte))
                             jmin=jmin+10
                          enddo
                          go to 98
                      else
                          go to 96
                      endif
                 elseif((index(buffer,'prtf').gt.0).or.
     &                    (index(buffer,'prti').gt.0).or.
     &                    (index(buffer,'alft').gt.0)) then
                       backspace(iunit)
                 endif

              end do   ! spin system

          endif

c--------------------------------------------------
c calculate total dr rate now for each initial
c parent, final parent and spin system
c--------------------------------------------------

         if(ldr_n) then
            do ipfs=1,nsysf(ipi,ipf)
              call dasumd ( ndrep    , ndprt     , ndt    ,
     &                      ipi      , ipf       , ipfs   ,
     &                      nte      , nrep      , nrepa  , diel_n  ,
     &                      diel_sum
     &                    )
              do j=1,nte
                if(diel_sum(ipi,ipf,ipfs,j).gt.0.0d0) then
                    ldiel_sum(ipi,ipf,ipfs)=.true.
                endif
              enddo

            enddo
         endif

         ipf = ipf+1

        enddo

  130 continue

  140 continue


c------------------------------------------
c gather totals as stored at end of dataset
c------------------------------------------

  150 continue

      read(iunit,'(1a128)',end=999)c128
      call xxcase(c128, buffer, 'lc')
      indx1 = index(buffer, 'alft( 1)')
      if (indx1.le.0) goto 150
      read(iunit,'(a)') buffer

      do j = 1, nte
          read(iunit,'(e10.2,1x,20e10.2)')
     &               temp,(diel_tot(i,j),i=1,nprnti)
          if (dabs(temp-tea(j))/temp.gt.1.0d-6) then
              write(i4unit(-1),1001) 'partial and total',iline,
     &                             ' te grid mismatch'
              write(i4unit(-1),1002)
              stop
          endif
      end do

c-----------------------------------------------------------------------
  200 continue
c-----------------------------------------------------------------------


c-----------------------------------------------------------------------

 1000 format(1x,31('*'),' xxdata_09 error ',30('*')//
     &       1x,'fault in input data file: ',a,i4,a,i4)
 1001 format(1x,31('*'),' xxdata_09 error ',30('*')//
     &       1x,'fault in input data file: ',a,i5,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1010 format(/1x,29('*'),' program continues  ',29('*'))

c-----------------------------------------------------------------------
  999 continue

      return
      end
