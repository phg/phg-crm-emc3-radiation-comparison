      subroutine xxdata_12(iunit  , dsname ,
     &                     ndtem  , ndden  , ndein  , ndzef   , ndmag  ,
     &                     ndstore,
     &                     nbsel  ,
     &                     csymb  , czion  , cwavel , cdonor , crecvr  ,
     &                     ctrans , cfile  , ctype  , cindm  ,
     &                     qefref ,
     &                     enref  , teref  , deref  , zeref   , bmref  ,
     &                     nenera , ntempa , ndensa , nzeffa  , nbmaga ,
     &                     enera  , tempa  , densa  , zeffa   , bmaga  ,
     &                     qenera , qtempa , qdensa , qzeffa  , qbmaga
     &                    )
      implicit none
c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: xxdata_12 *******************
c
c  purpose : To fetch data from input dataset of type adf12.
c
c  notes:    Extensive rewrite of xxdata_12.for which it is a
c            replacement.  Designed to handle automatically produced
c            heavy species data sets.
c
c  calling program: adas316 and general
c
c  subroutine:
c
c  input : (i*4)  iunit     = unit number to read from
c  input : (c*(*))dsname    = file name of data set being read
c  input : (i*4)  ndstore   = maximum number of data blocks allowed
c  input : (i*4)  ndtem     = maximum number of electron temperatures
c  input : (i*4)  ndden     = maximum number of electron densities
c  input : (i*4)  ndein     = maximum number of beam energies
c  input : (i*4)  ndzef     = maximum number of z effectives
c  input : (i*4)  ndmag     = maximum number of magnetic fields
c
c  output: (i*4)  nbsel     = number of blocks present
c
c  output: (c*(*))csymb()   = input data file: element symbol
c                             dimension: data-block index
c  output: (c*(*))czion()   = input data file: emitting ion charge
c                             dimension: data-block index
c  output: (c*(*))cwavel()  = input data file: wavelength (A)
c                             dimension: data-block index
c  output: (c*(*))cdonor()  = input data file: donor neutral atom
c                             dimension: data-block index
c  output: (c*(*))crecvr()  = input data file: receiver nucleus
c                             dimension: data-block index
c  output: (c*(*))ctrans()  = input data file: transition
c                             dimension: data-block index
c  output: (c*(*))cfile()   = input data file: specific ion file source
c                             dimension: data-block index
c  output: (c*(*))ctype()   = input data file: type of emissivity
c                             dimension: data-block index
c  output: (c*(*))cindm()   = file data file: emissivity index
c                             dimension: data-block index
c
c  output: (r*8)  qefref()  = reference value of rate coefficient
c  output: (r*8)  enref()   =     "       "   "  energy
c  output: (r*8)  teref()   =     "       "   "  temperature
c  output: (r*8)  deref()   =     "       "   "  density
c  output: (r*8)  zeref()   =     "       "   "  effective z
c  output: (r*8)  bmref()   =     "       "   "  magnetic field
c  output: (i*4)  nenera()  = number of energies
c  output: (i*4)  ntempa()  = number of temperatures
c  output: (i*4)  ndensa()  = number of densities
c  output: (i*4)  nzeffa()  = number of effective z's
c  output: (i*4)  nbmaga()  = number of magnetic field values
c                             1st. dim: ndstore
c                             (for above arrays)
c
c  output: (r*8)  enera(,)  = energies
c  output: (r*8)  qenera(,) = rate coefficients for energy value
c  output: (r*8)  tempa(,)  = temperatures
c  output: (r*8)  qtempa(,) = rate coefficients for temperatures
c  output: (r*8)  densa(,)  = densities
c  output: (r*8)  qdensa(,) = rate coefficients for desnities
c  output: (r*8)  zeffa(,)  = effective z
c  output: (r*8)  qzeffa(,) = rate coefficients for effective z
c  output: (r*8)  bmaga(,)  = magnetic field
c  output: (r*8)  qbmaga(,) = rate coefficients for magnetic fields
c                             1st dim: 12 or 24  depending on parameter
c                             2nd dim: ndstore
c
c  routines:
c           routine    source    brief description
c           ------------------------------------------------------------
c           i4unit     adas      fetches file handle for error message
c           xxslen     adas      locate first and last char. of string
c           xxhkey     adas      extract response to key on a text line
c           xxcase     adas      convert a string to upper or lower case
c
c
c  AUTHOR:  H P Summers, University of Strathcldye
c  DATE:    31/05/07
c
c  update:
c
c  VERSION  : 1.1
c  DATE     : 05-06-2007
c  MODIFIED : H P Summers
c               - First version.
c
c  VERSION  : 1.2
c  DATE     : 26-03-2019
c  MODIFIED : M O'Mullane
c               - cbreak is ' ' and not ':' which matters for the
c                 updated xxhkey and the adf12 files from adas316.
c
c-----------------------------------------------------------------------
      integer   iunit   , i4unit
      integer   ndtem   , ndden  , ndein  , ndzef , ndmag     ,
     &          ndstore
      integer   istore  , nbsel  , j      , i0    , i1
c-----------------------------------------------------------------------
      character dsname*(*)
      character string*80  , c80*80
      character cbreak*1   , cans*20    , cblank20*20
      character ckey01*1   , ckey02*1   , ckey03*1   , ckey04*1   ,
     &          ckey05*1   , ckey06*1   , ckey07*1   , ckey08*1   ,
     &          ckey09*4
c-----------------------------------------------------------------------
      logical   lkey01     , lkey02     , lkey03     , lkey04     ,
     &          lkey05     , lkey06     , lkey07     , lkey08     ,
     &          lkey09
      logical   lwkey01    , lwkey02    , lwkey03    , lwkey04    ,
     &          lwkey05    , lwkey06    , lwkey07    , lwkey08    ,
     &          lwkey09
c-----------------------------------------------------------------------
      integer   nenera(ndstore)   , ntempa(ndstore)    , ndensa(ndstore)
      integer   nzeffa(ndstore)   , nbmaga(ndstore)
c-----------------------------------------------------------------------
      real*8    qefref(ndstore)
      real*8    enref(ndstore)    , teref(ndstore)
      real*8    deref(ndstore)    , zeref(ndstore)
      real*8    bmref(ndstore)
      real*8    enera(ndein,ndstore) , qenera(ndein,ndstore)
      real*8    tempa(ndtem,ndstore) , qtempa(ndtem,ndstore)
      real*8    densa(ndden,ndstore) , qdensa(ndden,ndstore)
      real*8    zeffa(ndzef,ndstore) , qzeffa(ndzef,ndstore)
      real*8    bmaga(ndmag,ndstore) , qbmaga(ndmag,ndstore)
c-----------------------------------------------------------------------
      character csymb(ndstore)*(*)  , czion(ndstore)*(*)   ,
     &          cwavel(ndstore)*(*) , cdonor(ndstore)*(*)  ,
     &          crecvr(ndstore)*(*) , ctrans(ndstore)*(*)  ,
     &          cfile(ndstore)*(*)  , ctype(ndstore)*(*)   ,
     &          cindm(ndstore)*(*)
c-----------------------------------------------------------------------
      data      ckey01/'e'/    , ckey02/'z'/    , ckey03/'w'/    ,
     &          ckey04/'d'/    , ckey05/'r'/    , ckey06/'n'/    ,
     &          ckey07/'f'/    , ckey08/'m'/    , ckey09/'isel'/
      data      lkey01/.true./ , lkey02/.true./ , lkey03/.true./ ,
     &          lkey04/.true./ , lkey05/.true./ , lkey06/.true./ ,
     &          lkey07/.true./ , lkey08/.true./ , lkey09/.true./
      data      lwkey01/.true./ , lwkey02/.true./ , lwkey03/.true./ ,
     &          lwkey04/.true./ , lwkey05/.true./ , lwkey06/.true./ ,
     &          lwkey07/.true./ , lwkey08/.true./ , lwkey09/.true./
      data      cbreak/' '/
      data      cblank20/'                    '/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      read(iunit,*)nbsel

      if (nbsel.gt.ndstore) then
         write(i4unit(-1),2000) dsname , ndstore , nbsel , ndstore
         nbsel = ndstore
      endif

      do istore=1,nbsel

         string = ' '
         read(iunit,1000) string

c-----------------------------------------------------------------------
c  search for keys and fill output info character vectors
c-----------------------------------------------------------------------

         call xxcase(string,c80,'lc')

         call xxhkey(c80,ckey01,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey01) then
                 write(i4unit(-1),2001)'key = ',ckey01,
     &                                 ' (element symbol) not present'
                 write(i4unit(-1),2002)
                 lkey01 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(csymb(istore)).lt.(i1-i0+1))then
                 if(lwkey01) then
                     write(i4unit(-1),2001)'text for key = ',ckey01,
     &                                     ' (element symbol) truncated'
                     write(i4unit(-1),2002)
                     lwkey01 = .false.
                 endif
                 csymb(istore)(1:len(csymb(istore)))=
     &                cans(i0:i0+len(csymb(istore))-1)
             else
                 csymb(istore)=' '
                 csymb(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey02,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey02) then
                 write(i4unit(-1),2001)'key = ',ckey02,
     &                                 ' (ion charge) not present'
                 write(i4unit(-1),2002)
                 lkey02 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(czion(istore)).lt.(i1-i0+1))then
                 if(lwkey02) then
                     write(i4unit(-1),2001)'text for key = ',ckey02,
     &                                     ' (ion charge) truncated'
                     write(i4unit(-1),2002)
                     lwkey02 = .false.
                 endif
                 czion(istore)(1:len(czion(istore)))=
     &                cans(i0:i0+len(czion(istore))-1)
             else
                 czion(istore)=' '
                 czion(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey03,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey03) then
                 write(i4unit(-1),2001)'key = ',ckey03,
     &                                 ' (wavelength) not present'
                 write(i4unit(-1),2002)
                 lkey03 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(cwavel(istore)).lt.(i1-i0+1))then
                 if(lwkey03) then
                     write(i4unit(-1),2001)'text for key = ',ckey03,
     &                                     ' (wavelength) truncated'
                     write(i4unit(-1),2002)
                     lwkey03 = .false.
                 endif
                 cwavel(istore)(1:len(cwavel(istore)))=
     &                cans(i0:i0+len(cwavel(istore))-1)
             else
                 cwavel(istore)=' '
                 cwavel(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey04,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey04) then
                 write(i4unit(-1),2001)'key = ',ckey04,
     &                                 ' (donor) not present'
                 write(i4unit(-1),2002)
                 lkey04 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(cdonor(istore)).lt.(i1-i0+1))then
                 if(lwkey04) then
                     write(i4unit(-1),2001)'text for key = ',ckey04,
     &                                     ' (donor) truncated'
                     write(i4unit(-1),2002)
                     lwkey04 = .false.
                 endif
                 cdonor(istore)(1:len(cdonor(istore)))=
     &                cans(i0:i0+len(cdonor(istore))-1)
             else
                 cdonor(istore)=' '
                 cdonor(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey05,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey05) then
                 write(i4unit(-1),2001)'key = ',ckey05,
     &                                 ' (receiver) not present'
                 write(i4unit(-1),2002)
                 lkey05 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(crecvr(istore)).lt.(i1-i0+1))then
                 if(lwkey05) then
                     write(i4unit(-1),2001)'text for key = ',ckey05,
     &                                     ' (receiver) truncated'
                     write(i4unit(-1),2002)
                     lwkey05 = .false.
                 endif
                 crecvr(istore)(1:len(crecvr(istore)))=
     &                cans(i0:i0+len(crecvr(istore))-1)
             else
                 crecvr(istore)=' '
                 crecvr(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey06,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey06) then
                 write(i4unit(-1),2001)'key = ',ckey06,
     &                                 ' (transition) not present'
                 write(i4unit(-1),2002)
                 lkey06 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(ctrans(istore)).lt.(i1-i0+1))then
                 if(lwkey06) then
                     write(i4unit(-1),2001)'text for key = ',ckey06,
     &                                 ' (transition) truncated'
                     write(i4unit(-1),2002)
                     lwkey06 = .false.
                 endif
                 ctrans(istore)(1:len(ctrans(istore)))=
     &                cans(i0:i0+len(ctrans(istore))-1)
             else
                 ctrans(istore)=' '
                 ctrans(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey07,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey07) then
                 write(i4unit(-1),2001)'key = ',ckey07,
     &                                 ' (dataset) not present'
                 write(i4unit(-1),2002)
                 lkey07 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(cfile(istore)).lt.(i1-i0+1))then
                 if(lwkey07) then
                     write(i4unit(-1),2001)'text for key = ',ckey07,
     &                                 ' (dataset) truncated'
                     write(i4unit(-1),2002)
                     lwkey07 = .false.
                 endif
                 cfile(istore)(1:len(cfile(istore)))=
     &                cans(i0:i0+len(cfile(istore))-1)
             else
                 cfile(istore)=' '
                 cfile(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey08,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey08) then
                 write(i4unit(-1),2001)'key = ',ckey08,
     &                                 ' (emissivity type) not present'
                 write(i4unit(-1),2002)
                 lkey08 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(ctype(istore)).lt.(i1-i0+1))then
                 if(lwkey08) then
                     write(i4unit(-1),2001)'text for key = ',ckey08,
     &                                 ' (emissivity type) truncated'
                     write(i4unit(-1),2002)
                     lwkey08 = .false.
                 endif
                 ctype(istore)(1:len(ctype(istore)))=
     &                cans(i0:i0+len(ctype(istore))-1)
             else
                 ctype(istore)=' '
                 ctype(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         call xxhkey(c80,ckey09,cbreak,cans)
         if(cans.eq.cblank20) then
             if(lkey09) then
                 write(i4unit(-1),2001)'key = ',ckey09,
     &                                 ' (index) not present'
                 write(i4unit(-1),2002)
                 lkey09 = .false.
             endif
         else
             call xxslen(cans,i0,i1)
             if(len(cindm(istore)).lt.(i1-i0+1))then
                 if(lwkey09) then
                     write(i4unit(-1),2001)'text for key = ',ckey09,
     &                                 ' (index) truncated'
                     write(i4unit(-1),2002)
                     lwkey09 = .false.
                 endif
                 cindm(istore)(1:len(cindm(istore)))=
     &                cans(i0:i0+len(cindm(istore))-1)
             else
                 cindm(istore)=' '
                 cindm(istore)(1:i1-i0+1)=cans(i0:i1)
             endif
         endif

         read(iunit,1001) qefref(istore)
         read(iunit,1001) enref(istore)   , teref(istore)  ,
     &                    deref(istore)   , zeref(istore)  ,
     &                    bmref(istore)
         read(iunit,1002) nenera(istore)  , ntempa(istore) ,
     &                    ndensa(istore)  , nzeffa(istore) ,
     &                    nbmaga(istore)
c-----------------------------------------------------------------------
c  read energy lines allowing for old form zero filled full vectors
c-----------------------------------------------------------------------
   10    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'ener').gt.0) then
             backspace(iunit)
         else
             go to 10
         endif
         read(iunit,1001) (enera(j,istore),j=1,nenera(istore))

   15    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'qener').gt.0) then
             backspace(iunit)
         else
             go to 15
         endif
         read(iunit,1001) (qenera(j,istore),j=1,nenera(istore))
c-----------------------------------------------------------------------
c  read temperature lines allowing for old form zero filled full vectors
c-----------------------------------------------------------------------
   20    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'tiev').gt.0) then
             backspace(iunit)
         else
             go to 20
         endif
         read(iunit,1001) (tempa(j,istore),j=1,ntempa(istore))

   25    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'qtiev').gt.0) then
             backspace(iunit)
         else
             go to 25
         endif
         read(iunit,1001) (qtempa(j,istore),j=1,ntempa(istore))
c-----------------------------------------------------------------------
c  read density lines allowing for old form zero filled full vectors
c-----------------------------------------------------------------------
   30    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'densi').gt.0) then
             backspace(iunit)
         else
             go to 30
         endif
         read(iunit,1001) (densa(j,istore),j=1,ndensa(istore))

   35    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'qdensi').gt.0) then
             backspace(iunit)
         else
             go to 35
         endif
         read(iunit,1001) (qdensa(j,istore),j=1,ndensa(istore))
c-----------------------------------------------------------------------
c  read zeff lines allowing for old form zero filled full vectors
c-----------------------------------------------------------------------
   40    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'zeff').gt.0) then
             backspace(iunit)
         else
             go to 40
         endif
         read(iunit,1001) (zeffa(j,istore),j=1,nzeffa(istore))

   45    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'qzeff').gt.0) then
             backspace(iunit)
         else
             go to 45
         endif
         read(iunit,1001) (qzeffa(j,istore),j=1,nzeffa(istore))
c-----------------------------------------------------------------------
c  read bmag lines allowing for old form zero filled full vectors
c-----------------------------------------------------------------------
   50    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'bmag').gt.0) then
             backspace(iunit)
         else
             go to 50
         endif
         read(iunit,1001) (bmaga(j,istore),j=1,nbmaga(istore))

   55    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'qbmag').gt.0) then
             backspace(iunit)
         else
             go to 55
         endif
         read(iunit,1001) (qbmaga(j,istore),j=1,nbmaga(istore))
c-----------------------------------------------------------------------
c  reposition for next block
c-----------------------------------------------------------------------

   60    read(iunit,1000) string
         call xxcase(string,c80,'lc')
         if(index(c80,'isel').gt.0) then
             backspace(iunit)
         elseif (c80(1:10).eq.'c---------') then
             go to 80
         else
             go to 60
         endif

      enddo

   80 return
c-----------------------------------------------------------------------
 1000 format(1a80)
 1001 format(6d10.2)
 1002 format(5i10)
 2000 format(/1x,31('*'),' xxdata_12 message ',31('*'),/,
     &        2x,'adf12 data set                    : ',a80,/,
     &        2x,'Number of coefficient data-blocks',
     &           ' in input data set too great.',/,
     &        2x,'maximum number allowed     = ',i3,/,
     &        2x,'number present in data set = ',i3,/,
     &        2x,'only the first ',i3,' have been accepted',/,
     &        1x,33('*'),' end of message ',32('*'))
 2001 format(1x,29('*'),' xxdata_12 warning ',29('*')//
     &       1x,'Unset values: ', a, a, a)
 2002 format(/1x,29('*'),' program continues ',29('*'))
c-----------------------------------------------------------------------
      end
