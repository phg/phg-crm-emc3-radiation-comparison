       subroutine xxdata_13( iunit  , dsname ,
     &                       nstore , ntdim  , nddim  ,
     &                       iz0    , iz     , iz1    , esym  ,
     &                       nbsel  , isela  ,
     &                       cwavel , cfile  , cpcode , cindm ,
     &                       ita    , ida    ,
     &                       teta   , teda   ,
     &                       sxb
     &                     )
       implicit none
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: xxdata_13 *******************
C
C  PURPOSE:  To fetch data from input ionisations/photon file
C            for a given emitting ion (element and charge).
C
C  CALLING PROGRAM: ADAS503/ssxb
C
C  DATA:    Up to 'nstore' sets (data-blocks) of data may be  read from
C           the file - each block forming a complete set of ionizations
C           per photon values for given temp/density combination. Each
C           data-block  is  analysed independently of any  other  data-
C           block.
C
C           The units used in the data file are taken as follows:
C
C           Temperatures : eV
C           Densities    : cm-3
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C
C          (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C          (I*4)  NTDIM    = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C          (I*4)  NDDIM    = MAX NUMBER OF ELECTRON DENSITIES    ALLOWED
C
C  OUTPUT: (I*4)  IZ0      = READ - EMITTING ION - NUCLEAR CHARGE
C          (I*4)  IZ       = READ - EMITTING ION - CHARGE
C          (I*4)  IZ1      = READ - EMITTING ION - CHARGE + 1
C          (C*2)  ESYM     = READ - EMITTING ION - ELEMENT SYMBOL
C
C          (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (C*10) CWAVEL() = READ - WAVELENGTH (ANGSTROMS)
C                            DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CFILE()  = READ - SPECIFIC ION FILE SOURCE
C                            DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CPCODE() = READ - DATA PRODUCTION CODE
C                            DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CINDM()  = READ - METASTABLE INDEX
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  ITA()    = READ - NUMBER OF ELECTRON TEMPERATURES
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IDA()    = READ - NUMBER OF ELECTRON DENSITIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  TETA(,)  = READ - ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TEDA(,)  = READ - ELECTRON DENSITIES (UNITS: CM-3)
C                            1st DIMENSION: ELECTRON DENSITY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  sxb(,,)   =READ - PHOTON EMISSIVITY VALUES
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: ELECTRON DENSITY INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C ROUTINE: (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: ELECTRON TEMPERATURE INDEX
C          (I*4)  ITD      = ARRAY INDEX: ELECTRON DENSITY     INDEX
C          (I*4)  NTNUM    = NUMBER OF ELECTRON TEMPERATURES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  NDNUM    = NUMBER OF ELECTRON DENSITIES    FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS1    = GENERAL USE STRING INDEX VARIABLE
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*5)  IONNAM   = EMITTING ION READ FROM DATASET
C          (C*6)  CKEY1    = 'FILMEM' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY2    = 'TYPE  ' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY3    = 'INDM  ' - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY4    = 'ISEL  ' - INPUT BLOCK HEADER KEY
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4EIZ0     ADAS      INTEGER*4 FUNCTION    -
C                               RETURNS Z0 FOR GIVEN ELEMENT SYMBOL
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C-----------------------------------------------------------------------
C
C
C NOTES: Based on xxdata_15.for.
C
C
C VERSION  : 1.1
C DATE     : 17-02-2006
C MODIFIED : Martin O'Mullane
C              - First version
C
C VERSION  : 1.2
C DATE     : 21-01-2007
C MODIFIED : Allan Whiteford
C              - Changed error messages from xxdata_15 to xxdata_13.
C
C VERSION  : 1.3
C DATE     : 15-05-2013
C MODIFIED : Martin O'Mullane
C              - Make sure c80 is lower case before parsing it.
C
C VERSION  : 1.4
C DATE     : 17-07-2017
C MODIFIED : Martin O'Mullane
C              - Use correct case when searching for keys.
C              - Look for CODE rather than TYPE when parsing information.
C
C VERSION  : 1.5                
C DATE     : 21-08-2018
C MODIFIED : Martin O'Mullane
C              - do not limit dsname to 80 characters in format 
C                statements.
C
C-----------------------------------------------------------------------
      integer    i4eiz0                , i4fctn              , i4unit
      integer    iunit                 , nstore              ,
     &           ntdim                 , nddim               ,
     &           iz0                   , iz                  ,
     &           iz1                   , nbsel
      integer    iblk                  ,
     &           itt                   , itd                 ,
     &           ntnum                 , ndnum               ,
     &           ipos1                 , ipos2               , iabt
C-----------------------------------------------------------------------
      logical    lbend
C-----------------------------------------------------------------------
      character  dsname*(*)            , esym*2
      character  cslash*1              , c2*3                ,
     &           ckey1*6               , ckey2*4             ,
     &           ckey3*4               , ckey4*4             ,
     &           ionnam*5              , c80*80              , cstrg*80
C-----------------------------------------------------------------------
      integer    isela(nstore)         ,
     &           ita(nstore)           , ida(nstore)
C-----------------------------------------------------------------------
      character  cindm(nstore)*2       , cfile(nstore)*8      ,
     &           cpcode(nstore)*8      , cwavel(nstore)*10
C-----------------------------------------------------------------------
      real*8     teta(ntdim,nstore)    , teda(nddim,nstore)
      real*8     sxb(ntdim,nddim,nstore)
C-----------------------------------------------------------------------
      save       cslash                ,
     &           ckey1                 , ckey2                ,
     &           ckey3                 , ckey4
C-----------------------------------------------------------------------
      data       cslash / '/' /
      data       ckey1  / 'filmem' /   , ckey2  / 'code'   /  ,
     &           ckey3  / 'indm'   /   , ckey4  / 'isel'   /
C-----------------------------------------------------------------------
      lbend = .false.
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C Read in number of data-blocks present in input file.
C-----------------------------------------------------------------------

      read(iunit,1000) c80
      read(c80, *) nbsel

      ipos1 = index(c80,'/') + 1
      ipos2 = ipos1          + 4
      read(c80(ipos1:ipos2),1001) ionnam

      if ( ionnam(2:2).eq.'+' ) then
         esym  = ionnam(1:1) // ' '
      else
         esym  = ionnam(1:2)
      endif

      iz0   = i4eiz0( esym )
      iz    = i4fctn( ionnam(3:5) , iabt )
      if (iabt.ne.0) iz = i4fctn( ionnam(3:4) , iabt )

      iz1   = iz + 1

      if (nbsel.gt.nstore) then
         write(i4unit(-1),2000) dsname , nstore , nbsel , nstore
         nbsel = nstore
      endif

C-----------------------------------------------------------------------
C Input data for each of the data-blocks
C-----------------------------------------------------------------------

      do iblk=1,nbsel

C-----------------------------------------------------------------------
C Input title, wavelength and other information (check block exists)
C-----------------------------------------------------------------------

         if (.not.lbend) then

            read(iunit,1000)cstrg
            call xxcase(cstrg,c80,'lc')

            if ( c80(1:1).ne.'c') then

               ipos1 =  index(c80,'a') + 1
               ipos2 =  index(c80,'/') - 1

               if (ipos1.eq.1) then
                  ipos1        = 11
                  cwavel(iblk) = c80(1:10)
               elseif (ipos1.gt.10) then
                  cwavel(iblk) = c80(1:10)
               else
                  cwavel(iblk) = c80(1:ipos1-1)
               endif

               read(c80(ipos1:ipos2),*) ida(iblk) , ita(iblk)

               call xxhkey( c80 , ckey1 , cslash , cfile(iblk)  )
               call xxhkey( c80 , ckey2 , cslash , cpcode(iblk) )
               call xxhkey( c80 , ckey3 , cslash , cindm(iblk)  )
               call xxhkey( c80 , ckey4 , cslash , c2           )
               isela(iblk) = i4fctn( c2 , iabt )

               ndnum = ida(iblk)
               ntnum = ita(iblk)

               if (ntnum.gt.ntdim) then
                  write(i4unit(-1),2001) dsname , iblk  ,
     &                                  'TEMPERATURES'  ,
     &                                  ntdim   , ntnum
                  stop
               endif

               if (ndnum.gt.nddim) then
                  write(i4unit(-1),2001) dsname , iblk   ,
     &                                  'DENSITIES'      ,
     &                                  nddim   , ndnum
                  stop
               endif

C-----------------------------------------------------------------------
C Input temperature, density and photon emissivities for block
C-----------------------------------------------------------------------

               read(iunit,1002) ( teda(itd,iblk) , itd=1,ndnum )
               read(iunit,1002) ( teta(itt,iblk) , itt=1,ntnum )

               do itd=1,ndnum
                  read(iunit,1002) (sxb(itt,itd,iblk),
     &                              itt=1,ntnum        )
               end do

            else

               write(i4unit(-1),2002) dsname  , nbsel    ,
     &                               iblk - 1 , iblk - 1
               lbend = .true.
               nbsel = iblk - 1

            endif

         endif

      end do


C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1001 FORMAT(A5)
 1002 FORMAT(8D9.2)

 2000 FORMAT(/1X,31('*'),' xxdata_13 MESSAGE ',31('*')/
     &        2X,'INPUT PHOT. EMISS. DATA SET NAME: ',A/
     &        2X,'NUMBER OF WAVELENGTH DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' xxdata_13 ERROR ',32('*')/
     &        2X,'INPUT PHOT. EMISS. DATA SET NAME: ',A/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT ELECTRON ',A,' TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' xxdata_13 MESSAGE ',31('*')/
     &        2X,'INPUT IONS./PHOTON DATA SET NAME: ',A/
     &        2X,'INCONSISTENCY IN THE NUMBER OF WAVELENGTHS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C-----------------------------------------------------------------------

      return
      end
