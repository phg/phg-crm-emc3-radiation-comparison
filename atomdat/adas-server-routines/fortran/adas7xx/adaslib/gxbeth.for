       subroutine gxbeth( ndcor     , ndbtyp   ,
     &                    ng        , lg       , np        , lp      ,
     &                    ibtyp     , jcor     , cor       , df
     &                   )

       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: gxbeth *********************
c
c purpose : to return the default bethe partial wave correction factors
c           for specific parent transition types
c
c notes:    low partial wave Bethe collision strength correction factors
c           for the different parent transition types are as follow:
c
c           type  transition          (cor(j),j=1,jcor)              df
c           ----  ------------------  -----------------------------  ---
c            1    ng=1,np>=2,lp=lg+1: 0.05,0.30,0.50,0.90            2.0
c            2    ng=2,np=3 ,lp=lg+1: 0.01,0.02,0.20,0.40,0.70,0.90  1.0
c            3    ng=2,np=3,lp=lg-1 : 0.01,0.01,0.01,0.08,0.30,0.70  1.0
c            4    np-ng=0, lp=lg+1  : 0.30,0.35,0.40,0.45,0.70,0.90  0.5
c            5    np-ng=0, lp=lg-1  : 0.30,0.35,0.40,0.45,0.70,0.90  0.5
c            6    np-ng>0, lp=lg+1  : 0.01,0.02,0.20,0.40,0.70,0.90  1.0
c            7    np-ng>0, lp=lg-1  : 0.01,0.01,0.01,0.08,0.30,0.70  1.0
c
c            Results may be externally  adjusted  a global parameter
c            corfac along with df supplied by the subroutine. corfac
c            and df operate as:
c                   cor_new(j)=exp(-corfac/((j-1)**df+0.5))*cor(j)
c
c            where j-1 = l (the partial wave).
c
c  calling program: various
c
c
c
c  subroutine:
c
c  input : (i*4)  ndcor    = maximum no. of Bethe correction factors
c  input : (i*4)  ndbtyp   = maximum no. of Bethe correction factors
c  input : (i*4)  ng       = active electron n-shell for ground level
c                            of parent transition
c  input : (i*4)  lg       = active electron l-shell for ground level
c                            of parent transition
c  input : (i*4)  np       = active electron n-shell for upper level
c                            of parent transition
c  input : (i*4)  lp       = active electron l-shell for upper level
c                            of parent transition
c
c  output: (i*4)  ibtyp    = type of Bethe correction set used
c  output: (i*4)  jcor     = number of partial wave corrections
c  output: (i*4)  cor()    = partial wave corrections
c  output: (i*4)  df       = partial wave scaling parameter
c                            1st dim: partial wave index (=l+1)
c  routines:
c          routine   source    description
c          -------------------------------------------------------
c          i4unit    adas      fetch unit number for output of messages
c
c
c  author:  H. P. Summers, University of Strathclyde
c           tel: 01235-46-4459
c
c  date:   18/06/2009
c
c
c  version  : 1.1
c  date     : 18-06-2009
c  modified : H P Summers
c              - first version.
c
c-----------------------------------------------------------------------
      integer     idcor       , idbtyp
c-----------------------------------------------------------------------
      parameter ( idcor = 20  , idbtyp = 7 )
c-----------------------------------------------------------------------
      integer     i4unit
      integer     ndcor  , ndbtyp
      integer     ng     , lg     , np     , lp
      integer     ibtyp  , jcor
      integer     j
c-----------------------------------------------------------------------
      real*8      df
c-----------------------------------------------------------------------
      integer     jcora(idbtyp)
c-----------------------------------------------------------------------
      real*8      cora(idcor,idbtyp)    , dfa(idbtyp)      , cor(ndcor)
c-----------------------------------------------------------------------
      data jcora/20,20,20,20,20,20,20/
      data dfa/2.0d0,1.0d0,1.0d0,0.5d0,0.5d0,1.0d0,1.0d0/
      data (cora(j,1),j=1,20)/0.05d0,0.3d0,0.5d0,0.9d0,16*1.0d0/
      data (cora(j,2),j=1,20)/0.01d0,0.02d0,0.20d0,0.40d0,0.70d0,0.90d0,
     &14*1.0d0/
      data (cora(j,3),j=1,20)/0.01d0,0.01d0,0.01d0,0.08d0,0.30d0,0.70d0,
     &14*1.0d0/
      data (cora(j,4),j=1,20)/0.30d0,0.35d0,0.40d0,0.45d0,0.70d0,0.90d0,
     &14*1.0d0/
      data (cora(j,5),j=1,20)/0.30d0,0.35d0,0.40d0,0.45d0,0.70d0,0.90d0,
     &14*1.0d0/
      data (cora(j,6),j=1,20)/0.01d0,0.02d0,0.20d0,0.40d0,0.70d0,0.90d0,
     &14*1.0d0/
      data (cora(j,7),j=1,20)/0.01d0,0.01d0,0.01d0,0.08d0,0.30d0,0.70d0,
     &14*1.0d0/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initial checks
c-----------------------------------------------------------------------
      if(idcor.lt.ndcor) then
          write(i4unit(-1),1000)'idcor',idcor,' < ndcor=',ndcor
          write(i4unit(-1),1001)
          stop
      endif

      if(idbtyp.lt.ndbtyp) then
          write(i4unit(-1),1000)'idbtyp',idbtyp,' < ndbtyp=',ndbtyp
          write(i4unit(-1),1001)
          stop
      endif
c-----------------------------------------------------------------------
c  get correct correction factors
c-----------------------------------------------------------------------
      if((ng.eq.1).and.(np.ge.2).and.(lp.eq.lg+1)) then
             ibtyp = 1
      elseif((ng.eq.2).and.(np.eq.3).and.(lp.eq.lg+1)) then
             ibtyp = 2
      elseif((ng.eq.2).and.(np.eq.3).and.(lp.eq.lg-1)) then
             ibtyp = 3
      elseif((ng.eq.np).and.(lp.eq.lg+1)) then
             ibtyp = 4
      elseif((ng.eq.np).and.(lp.eq.lg-1)) then
             ibtyp = 5
      elseif((np.gt.ng).and.(lp.eq.lg+1)) then
             ibtyp = 6
      elseif((np.gt.ng).and.(lp.eq.lg-1)) then
             ibtyp = 7
      else
          write(i4unit(-1),1002)'unknown option - corrections zeroed'
          write(i4unit(-1),*)'ng,lg,np,lp=',ng,lg,np,lp
          write(i4unit(-1),1003)
          jcor=ndcor
          do j=1,jcor
            cor(j)=0.0d0
          enddo
          return
      endif

      jcor = jcora(ibtyp)
      df   = dfa(ibtyp)
      do j=1,jcor
        cor(j)=cora(j,ibtyp)
      enddo

      return
c-----------------------------------------------------------------------
c
 1000 format(1x,30('*'),'   gxbeth error   ',30('*')//
     &       1x,'Incompatible parameters:  ',a,i2,a,i2)
 1001 format(1x,29('*'),' Program terminated ',29('*'))
 1002 format(1x,30('*'),'  gxbeth warning  ',30('*')//
     &       1x,'Incompatible parameters:  ',a,i2,a,i2)
 1003 format(1x,29('*'),' Program continues  ',29('*'))
c
c-----------------------------------------------------------------------
c

       end
