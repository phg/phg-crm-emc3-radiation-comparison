C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas701/adas701.for,v 1.4 2004/07/06 11:03:11 whitefor Exp $ Date $Date: 2004/07/06 11:03:11 $
C
       Program ADAS701

       IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS701 **********************
C
C  VERSION:  1.0
C
C  PURPOSE: Program to launch AUTOSTRUCTURE(AS), which is included as a 
C           set of subroutines. The MAIN level has been renamed ASDECK15.
C           Output datasets are opened here. Modifications to AS are 
C           minimised to moving the file opening up to the top level.
C
C  DATA:   Input AS data is now in adf27
C
C  PROGRAM:
C          (I*4) IUNT05    = INPUT UNIT FROM BATCH FILE.
C          (I*4) IUNT17    = OUTPUT UNIT FOR RUN SUMMARY.
C          (I*4) IUNT09    = PARAMETER = UNIT NUMBER FOR I/O
C          (I*4) LP1       = GENERAL CHARACTER STRING LENGTH INDEX
C          (I*4) LP2       = GENERAL CHARACTER STRING LENGTH INDEX
C          (I*4) IZ0       = NUCLEAR CHARGE
C         (C*80) DSNIN     = INPUT DATA FILE NAME
C         (C*80) PASDIR    = OUTPUT DIRECTORY FOR AS FILES
C         (C*80) DSNPAP    = OUTPUT DATA FILE NAME FOR STREAM 17
C         (C*80) DSN80     = GENERAL DATA SET NAME STRING
C         (C*10) DATE      =
C          (L*4) OPEN09    = .TRUE.  => STREAM 09 DEVICE OPEN
C                          = .FALSE. => STREAM 09 DEVICE NOT OPEN
C          (C*3) REP       = 'YES' => TERMINATE PROGRAM EXECUTION
C                            'NO'  => CONTINUE PROGRAM EXECUTION
C         (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C        (C*120) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                            TIME.
C          (C*8) DATE      = CURRENT DATE (AS 'DD/MM/YY')
C          (L*4) LPAPER    = .TRUE.  => OUTPUT TO PAPER.TXT FILE
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4) LBATCH    = FLAGS HOW PROGRAM IS BEING EXECUTED.
C                            .TRUE.  => PROGRAM RUNNING IN BATCH.
C                            .FALSE. => PROGRAM RUNNING IN FOREGROUND.
C          (L*4) LPEND     = FLAGS IF END OF ANALYSIS REQUESTED.
C                            .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                       SETS
C                            .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                       DATA SETS
C          (L*4) LBTSEL    = FLAGS IF PROGRAM TO BE RUN IN BATCH.
C                            .TRUE.  => RUN PROGRAM IN BATCH.
C                            .FALSE. => RUN PROGRAM IN FOREGROUND.
C
C          (I*4) LOGIC     = VALUE READ FROM PIPE FOR LOGICAL VARIABLES
C
C          (I*4) PIPEIN    = STANDARD INPUT
C          (I*4) PIPEOU    = STANDARD OUTPUT
C
C          (C*1) FORM      = AUTOSTRUCTURE OUTPUT FILE FORMATTING
C                            'U' : UNFORMATTED
C                            'F' : FORMATTED
C
C          (C*1) CPL       = AUTOSTRUCTURE OUTPUT FILE COUPLING
C                            'L' : LS COUPLING
C                            'I' : INTERMEDIATE COUPLING
C          (C*15)OUTNAME() = RENAMED OUTPUT PASSING FILES              
C          (I*4) LISTNAME()= SELECTED OUTPUT PASSING FILES TO BE WRITTEN 
C          (I*4) LISTUNIT()= AUTOSTRUCTURE UNIT NUMBERS OF I/O FILES
C          (I*4) LISTFORM()= OUTPUT FILES FORMATING INFORMATION
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          G1SPF0     ADAS      GATHERS INPUT FILE NAMES FROM IDL OR FILE
C          G1SPF1     ADAS      GATHERS OUTPUT FILE NAMES FROM IDL OR FILE
C          G1COUP     ADAS      DETERMINES FORMAT/COUPLING OF OUTPUT FILES
C          XXSLEN     ADAS      GET LENGTH OF STRING VARIABLE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXFLSH     ADAS      FLUSHES I/O BUFFER
C	   XXDATE     ADAS	OBTAINS DATE FROM IDL
C	   ASDECK16   AS  	AUTOSTRUCTURE CODE
C
C
C  INCLUDE FILE:
C          PARAM : sets dimension parameters for asdeck16                
C
C-----------------------------------------------------------------------
C NOTE:
C
C Minimal changes in AUTOSTRUCTURE MAIN (from ...badnell/autos/adseck15) 
C necessary to make it compatible with being launched from ADAS. 
C
C  1. Comment-out PROGRAM MAIN and uncomment SUBROUTINE ASDECK.
C  2. Comment out file opening statements.
C  3. Replace "read(5" with "READ(39" globally.
C  4. Replace idat=5 with idat=39 in asdeck.
C  5. Replace iread=5 with iread=39 in radwin.
C  6. Replace "write(6" with "WRITE(40" globally.
C  7. Below 1999 CONTINUE in main, uncomment RETURN and comment-out STOP.
C
C  Martin O'Mullane
C  23-06-98
C  Nigel Badnell
C  24-06-98
C
C-----------------------------------------------------------------------
C
C  AUTHOR : Martin O'Mullane,
C           K1/1/43,
C           JET
C 
C  AUTOSTRUCTURE : N. R. BADNELL (1985-1998)   
C
C  VERSION  : 1.1                          DATE: 15/06/1998
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          DATE: 13/10/1999
C  MODIFIED : Richard Martin  
C             Changed asdeck15 to asdeck
C
C  VERSION  : 1.3                          DATE: 16/11/2001
C  MODIFIED : Martin O'Mullane  
C             - AS is now at version 16 (copy of the 8-Nov-2001 version).
C
C
C  VERSION  : 1.4                          DATE: 27/08/2003
C  MODIFIED : Martin O'Mullane  
C             - AS is now at version 18 (copy of the 21-Aug-2003 version).
C             - Some extra AS output units opened as scratch.
C
C-----------------------------------------------------------------------
       INTEGER   LOGIC
       INTEGER   IUNT17     , IUNT09      , PIPEIN    ,
     &           PIPEOU     , ONE
       INTEGER   NDFILE
C-----------------------------------------------------------------------
       PARAMETER ( IUNT17 = 47  , IUNT09 = 39 )
       PARAMETER ( PIPEIN = 5   , PIPEOU = 6  , ONE = 1 )
       PARAMETER ( NDFILE = 12  )
C-----------------------------------------------------------------------
       INTEGER   I4UNIT
       INTEGER   LP1        , LP2  , IUNT     , I       
C-----------------------------------------------------------------------
       INTEGER   LISTNAME(NDFILE)  , LISTUNIT(NDFILE)
C-----------------------------------------------------------------------
       CHARACTER DSNIN*80   , PASDIR*80  , DSN80*80
       CHARACTER DATE*8     , REP*3      , CADAS*120  , DSNPAP*80
       CHARACTER TITLE*40
       CHARACTER FORM*1     , CPL*1
       CHARACTER CFLE*95
C-----------------------------------------------------------------------
       CHARACTER OUTNAME(NDFILE)*15 , LISTFORM(NDFILE)*1
C-----------------------------------------------------------------------
       LOGICAL   OPEN09     , OPEN17
       LOGICAL   LPAPER
       LOGICAL   LBATCH     , LBTSEL      , LPEND
C-----------------------------------------------------------------------
       DATA   OPEN09   /.FALSE./ , OPEN17/.FALSE./
       DATA   LBTSEL   /.FALSE./
       DATA   REP      / 'NO' /  , CADAS / ' ' /
       DATA   LISTUNIT /40,7,8,27,28,10,12,13,14,15,17,18/
       DATA   LISTFORM /'F','F','F','U','U','U',
     &                  'F','F','F','F','F','F'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

       CALL XX0000

C-----------------------------------------------------------------------
C GET PARAMETER LBATCH (IS THIS A BATCH RUN OR NOT?) FROM IDL OR FILE
C-----------------------------------------------------------------------

      READ( PIPEIN, *) LOGIC
      IF (LOGIC.EQ.0) THEN
         LBATCH	 = .FALSE.
      ELSE
         LBATCH = .TRUE.
      ENDIF

C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )
     
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 09 - CLOSE THE UNIT
C-----------------------------------------------------------------------

 50    IF (OPEN09) THEN
          CLOSE(IUNT09)
          OPEN09 = .FALSE.
       ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL OR FILE IF BATCH).
C-----------------------------------------------------------------------

       
       CALL G1SPF0( REP , DSNIN)

       IF (REP.EQ.'YES') THEN
          GOTO 9999
       ENDIF

C-----------------------------------------------------------------------
C GET COUPLING/FORMAT INFORMATION FROM DSNIN
C-----------------------------------------------------------------------
  
      IF (.NOT.OPEN09) CALL G1COUP( DSNIN , FORM , CPL )

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSNIN
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT09 , FILE=DSNIN  , STATUS='UNKNOWN' )
      OPEN09=.TRUE.

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL OR FILE IF BATCH).
C-----------------------------------------------------------------------

 55   LPEND = .FALSE.

      CALL G1SPF1( LBATCH  , FORM      , CPL   ,
     &             LPAPER  , LBTSEL    ,
     &             PASDIR  , DSNPAP    ,
     &             OUTNAME , LISTNAME  ,
     &             LPEND   , TITLE     ,  *201 )
     
C-----------------------------------------------------------------------
C IF PROGRAM RUN COMPLETE THEN RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 50

C-----------------------------------------------------------------------
C IF THE JOB HAS BEEN SELECTED TO RUN IN BATCH THEN GO BACK TO THE
C INPUT OPTIONS
C-----------------------------------------------------------------------

      IF (LBTSEL) GOTO 50

C-----------------------------------------------------------------------
C GET OUTPUT FILE NAMES AND OPEN THEM
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C OPEN PAPER.TXT FILE - IF REQUESTED
C-----------------------------------------------------------------------

      IF ((.NOT.OPEN17) .AND. LPAPER) THEN
         DSN80=' '
         DSN80=DSNPAP
         OPEN(UNIT=IUNT17,FILE=DSN80, STATUS='UNKNOWN')
         OPEN17=.TRUE.
      ENDIF
      
      
C-----------------------------------------------------------------------
C OPEN AUTOSTRUCTURE FILES
C
C Note that units 10 & 12 are input files. They are expected to be in  
C the passing subdirectory.
c
c       	
C The files are 
C  40 : STANDARD OUTPUT
C   7 : LS AUTOIONIZATION RATES, RADIATIVE RATES & ENERGIES
C   8 : IC         "              "                 "
C   9 : OPTIONAL CONTINUUM POTENTIAL (E.G. H-F FROZEN CORE)
C  10 : RESTART ALGEBRA
c  11 : scratch file
C  12 : INPUT BOUND ORBITALS (OPTIONAL)
C  13 : OUTOUT BOUND ORBITALS (OP
C  14 : TERM LIST FOR STG2
C  15 : LEVEL LIST FOR STGJK
C  16 : ORBITAL OVERLAP LIST
C  17 : LS PHOTOIONIZATION DATA
C  18 : IC       "         "
C  27 : AS ols BUT UNFORMATTTED
C  28 : AS oic BUT UNFORMATTED
C
C Added November 2001
C
C  21 : ADASEX TEMPLATE
C  22 : ADASEXJ TEMPLATE
C  23 : INFINITE ENERGY OMEGA-LS
C  24 : INFINITE ENERGY OMEGA-IC
C
C Added August 2003
C
C  19 : TERM ENERGY CORRECTIONS
C  20 : LEVEL ENERGY CORRECTIONS
C
C-----------------------------------------------------------------------

      
      CALL XXSLEN(PASDIR,LP1,LP2)
      
      OPEN(11,STATUS='SCRATCH',FORM='UNFORMATTED')
      
      DO I = 1,NDFILE
        IUNT = LISTUNIT(I)
        IF (LISTNAME(I).EQ.1) THEN
           CFLE = PASDIR(LP1:LP2)//OUTNAME(I)
           IF (LISTFORM(I).EQ.'U') THEN
              OPEN(UNIT=IUNT,FILE=CFLE,
     &             FORM='UNFORMATTED',STATUS='UNKNOWN')
           ELSE
              OPEN(UNIT=IUNT,FILE=CFLE,STATUS='UNKNOWN')
           ENDIF
        ELSE
           OPEN(UNIT=IUNT,STATUS='SCRATCH')          
        ENDIF
      ENDDO           


C-----------------------------------------------------------------------
C Extra output files for R-matrix use are sent to scratch
C-----------------------------------------------------------------------
     
      OPEN(UNIT=21,STATUS='SCRATCH')
      OPEN(UNIT=22,STATUS='SCRATCH')
      OPEN(UNIT=23,STATUS='SCRATCH')
      OPEN(UNIT=24,STATUS='SCRATCH')
      
      OPEN(UNIT=19,STATUS='SCRATCH')
      OPEN(UNIT=20,STATUS='SCRATCH')


C-----------------------------------------------------------------------
C Write something, anything, to paper.txt
C-----------------------------------------------------------------------

      IF (OPEN17) THEN
      
         WRITE(IUNT17,1000) CADAS(2:80)
         WRITE(IUNT17,1001) 'AUTOSTRUCTURE ','ADAS701',DATE
         WRITE(IUNT17,1002) TITLE
         WRITE(IUNT17,1003) dsnin,pasdir(1:lp2)
         
      ENDIF
    


C-----------------------------------------------------------------------
C TELL IDL TO START
C-----------------------------------------------------------------------

      IF(.NOT.LBATCH) THEN
         WRITE(PIPEOU,*)ONE
         CALL XXFLSH(PIPEOU)
      ENDIF

C-----------------------------------------------------------------------
C DO THE WORK - note that if there is a problem in AS it terminates
C               with a STOP. This causes problems in the pipe 
C               communications and an error message is popped up
C               saying that AS has crashed. On canceling the popup
C               the user is returned to the series 7 main menu.
C-----------------------------------------------------------------------

        call asdeck      
	    		
C-----------------------------------------------------------------------
C TELL IDL THAT WE HAVE COMPLETED ONE STAGE FOR PROGRESS WINDOW
C-----------------------------------------------------------------------

      IF(.NOT.LBATCH) THEN
          WRITE(PIPEOU,*)ONE
          CALL XXFLSH(PIPEOU)
      ENDIF

C-----------------------------------------------------------------------
C CLOSE OPEN FILES
C-----------------------------------------------------------------------

      DO I = 1,NDFILE
        IUNT = LISTUNIT(I)
        IF (LISTNAME(I).EQ.1) CLOSE(LISTNAME(I))
      END DO 
      CLOSE(11)     

     
 201  CONTINUE
      
C-----------------------------------------------------------------------
C GO BACK TO DATA-SET INPUT SCREEN
C-----------------------------------------------------------------------

       GO TO 50

     
 9999 CONTINUE
      CLOSE( IUNT17 )
     
      STOP
    
C-----------------------------------------------------------------------

 1000 FORMAT(A79)
 1001 FORMAT(1x,27('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,27('*')/)
 1002 FORMAT(/ ,19('-'),1X,A40,1X,19('-')//)
 1003 FORMAT(1x,'INPUT FILE NAME    : ',A44,/
     &       1X,'OUTPUT SUBDIRECTORY: ',A44,/)

C-----------------------------------------------------------------------
      END
