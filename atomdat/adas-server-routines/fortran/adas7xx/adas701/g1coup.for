C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas701/g1coup.for,v 1.1 2004/07/06 13:56:02 whitefor Exp $ Date $Date: 2004/07/06 13:56:02 $
C
      SUBROUTINE G1COUP( INFILE , FORM , CPL )
 
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G1COUP *********************
C
C  PURPOSE: To determine the coupling scheme and format/unformat choice
C           being used in adf27 datasets. If none are set G1COUP returns
C           Ls coupling and formatted output, the AUTOSTRUCTURE defaults.
C
C  CALLING PROGRAM: ADAS701
C
C  SUBROUTINE:
C
C  INPUT:  (C*80)  INFILE   = ADF27 INPUT FILE
C                                        FILE.
C                             .FALSE. => NO TEXT OUTPUT
C  OUTPUT: (C*1)   FORM     = AUTOSTRUCTURE OUTPUT FILE FORMATTING
C                             'U' : UNFORMATTED
C                             'F' : FORMATTED
C
C  OUTPUT: (C*1)   CPL      = AUTOSTRUCTURE OUTPUT FILE COUPLING
C                             'L' : LS COUPLING
C                             'I' : INTERMEDIATE COUPLING
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSTUC     ADAS      ENSURES ALL LETTERS ARE UPPERCASE
C
C AUTHOR:  Martin O'Mullane
C DATE:    12/06/98
C
C VERSION: 1.1                          DATE: 12-06-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      INTEGER    IUNT, I4UNIT, IP0, IP1
      PARAMETER (IUNT = 42)
C-----------------------------------------------------------------------
      LOGICAL    LCONT
C-----------------------------------------------------------------------
      CHARACTER  INFILE*80  , FORM*1  , CPL*1
      CHARACTER  LINE*133
      CHARACTER  STRG0*7    , STRG1*7 , STRG2*1 , STRG3*1 , 
     &           STRG4*3    , STRG5*5
C-----------------------------------------------------------------------
      DATA STRG0, strg1 /'&SALGEB','&SMINIM'/
      DATA STRG2, strg3 /'&',''''/
      data strg4, strg5 /'CUP','PRINT'/
C-----------------------------------------------------------------------

      OPEN(UNIT=IUNT,FILE=INFILE, STATUS='UNKNOWN')

C set to default to start 

      FORM = 'F'
      CPL  = 'L'
            
 10   READ(IUNT,1000,END=50)LINE
      CALL XXSTUC( LINE )
      
      IF (INDEX(LINE,STRG0).GT.0) THEN

         IF (INDEX(LINE,STRG4).GT.0) THEN
            IP0 = INDEX(LINE,STRG4) 
            IP1 = INDEX(LINE(IP0:133),STRG3)+IP0
            READ(LINE(IP1:IP1),'(A)')CPL
         ELSE
            LCONT = .TRUE.
            DO WHILE (LCONT)
              READ(IUNT,1000,END=50)LINE
              LCONT = INDEX(LINE,STRG2).LE.0
              IF (INDEX(LINE,STRG4).GT.0) THEN
                 IP0 = INDEX(LINE,STRG4) 
                 IP1 = INDEX(LINE(IP0:133),STRG3)+IP0
                 READ(LINE(IP1:IP1),'(A)')CPL
              ENDIF
            END DO
            BACKSPACE(IUNT)
         ENDIF
                
      ELSEIF (INDEX(LINE,STRG1).GT.0) THEN
      
         IF (INDEX(LINE,STRG5).GT.0) THEN
            IP0 = INDEX(LINE,STRG5) 
            IP1 = INDEX(LINE(IP0:133),STRG3)+IP0
            READ(LINE(IP1:IP1),'(A)')FORM
         ELSE
            LCONT = .TRUE.
            DO WHILE (LCONT)
              READ(IUNT,1000,END=50)LINE
              LCONT = INDEX(LINE,STRG2).LE.0
              IF (INDEX(LINE,STRG5).GT.0) THEN
                 IP0 = INDEX(LINE,STRG5) 
                 IP1 = INDEX(LINE(IP0:133),STRG3)+IP0
                 READ(LINE(IP1:IP1),'(A)')FORM
              ENDIF
            END DO
            BACKSPACE(IUNT)
         ENDIF
         
      ENDIF

      GO TO 10
       
  50  CONTINUE

  
      CLOSE(IUNT) 	
      RETURN

 1000 FORMAT(1A133)
      
      END
