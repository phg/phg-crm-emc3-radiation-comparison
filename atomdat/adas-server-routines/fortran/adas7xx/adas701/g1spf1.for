C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas701/g1spf1.for,v 1.1 2004/07/06 13:56:12 whitefor Exp $ Date $Date: 2004/07/06 13:56:12 $
C
      SUBROUTINE G1SPF1( LBATCH  , FORM    , CPL ,
     &                   LPAPER  , LBTSEL    ,
     &                   PASDIR  , DSNPAP    ,
     &                   OUTNAME , LISTNAME  ,
     &                   LPEND   , TITLE     ,  *
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G1SPF1 *********************
C
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL OR
C	    THE INFO FILE (WHICH HAS BEEN REDIRECTED TO STANDARD INPUT).
C
C  CALLING PROGRAM: ADAS701
C
C  SUBROUTINE:
C
C  INPUT:  (C*1)   FORM       = AUTOSTRUCTURE OUTPUT FILE FORMATTING
C                               'U' : UNFORMATTED
C                               'F' : FORMATTED
C
C  INPUT:  (C*1)   CPL        = AUTOSTRUCTURE OUTPUT FILE COUPLING
C                               'L' : LS COUPLING
C                               'I' : INTERMEDIATE COUPLING
C
C  INPUT:  (L*4)   LBATCH     = .TRUE.  => PROGRAM IS IN BATCH
C                               .FALSE. => PROGRAM IN FOREGROUND
C
C
C  OUTPUT: (L*4)   LPAPER     = .TRUE.  => OUTPUT TEXT TO 'PAPER.TXT'
C                                          FILE.
C                               .FALSE. => NO TEXT OUTPUT
C  OUTPUT: (L*4)   LBTSEL     = .TRUE.  => USER HAS SELECTED BATCH EXECUTION
C                               .FALSE. => 'RUN NOW' OR 'CANCEL' OPTION
C
C  OUTPUT: (C*80)  PASDIR     = OUTPUT PASSING SUBDIRECTORY NAME
C  OUTPUT: (C*80)  DSNPAP     = OUTPUT TEXT FILE NAME
C
C  OUTPUT: (C*15)  OUTNAME()  = RENAMED OUTPUT PASSING FILES
C  OUTPUT: (I*4)   LISTNAME() = SELECTED OUTPUT PASSING FILE TO BE WRITTEN
C  OUTPUT: (L*4)   LPEND      = .TRUE.  => USER SELECTED 'CANCEL'
C                               .FALSE. => USER DID NOT
C
C  OUTPUT: (C*40)  TITLE      = USER SUPPLIED TITLE FOR THIS RUN
C
C          (I*4)   PIPEIN     = STANDARD INPUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C DATE:    16/04/98
C
C VERSION: 1.1                          DATE: 16-04-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C-----------------------------------------------------------------------

      INTEGER      PIPEIN, PIPEOU, NDFILE, LOGIC, I
      PARAMETER   (PIPEIN = 5, PIPEOU=6, NDFILE = 12)
C-----------------------------------------------------------------------
      INTEGER      LISTNAME(NDFILE)
C-----------------------------------------------------------------------
      CHARACTER    DSNOUT*80   , PASDIR*80 , DSNPAP*80 ,  TITLE*40
      CHARACTER    FORM*1      , CPL*1
C-----------------------------------------------------------------------
      CHARACTER    OUTNAME(NDFILE)*15
C-----------------------------------------------------------------------
      LOGICAL      OPEN10      , OPEN11    , LBTSEL    ,  LPAPER  ,
     &             LBATCH      , LPEND
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  WRITE COUPLING/FORMAT INFORMATION TO IDL - ONLY IF IN FOREGROUND
C-----------------------------------------------------------------------

      IF (.NOT.LBATCH) THEN
         WRITE(PIPEOU,'(A1)') FORM
         WRITE(PIPEOU,'(A1)') CPL
         CALL XXFLSH(PIPEOU)
      ENDIF
      
C-----------------------------------------------------------------------
C  READ DATA FROM IDL
C-----------------------------------------------------------------------

      READ(PIPEIN,*) LOGIC
      IF(LOGIC.NE.1)THEN
         LPEND = .FALSE.
         READ(PIPEIN,'(A)')PASDIR
         
         DO I = 1,NDFILE
           READ(PIPEIN,'(A)')OUTNAME(I)
           READ(PIPEIN,*)LISTNAME(I)
         END DO
          
         READ(PIPEIN,'(A)')DSNPAP
         READ(PIPEIN,'(A)')TITLE
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LPAPER=.FALSE.
         ELSE
            LPAPER=.TRUE.
         ENDIF
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LBTSEL=.FALSE.
         ELSE
            LBTSEL=.TRUE.
         ENDIF
      ELSE
         LPEND = .TRUE.
      ENDIF

      RETURN
      END
