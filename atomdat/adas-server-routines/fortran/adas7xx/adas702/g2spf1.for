C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas702/g2spf1.for,v 1.1 2004/07/06 13:56:28 whitefor Exp $ Date $Date: 2004/07/06 13:56:28 $
C
      SUBROUTINE G2SPF1( LPEND    , TITLE ,
     &                   PASDIR   , OUTNAME )      
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G2SPF1 *********************
C
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL OR
C	    THE INFO FILE (WHICH HAS BEEN REDIRECTED TO STANDARD INPUT).
C
C  CALLING PROGRAM: ADAS702
C
C  SUBROUTINE:
C
C
C
C  OUTPUT: (L*4)   LPEND      = .TRUE.  => USER SELECTED 'CANCEL'
C                               .FALSE. => USER DID NOT
C
C  OUTPUT: (C*40)  TITLE      = USER SUPPLIED TITLE FOR THIS RUN
C  OUTPUT: (C*80)  PASDIR     = AUTOSTRUCTURE PASSING SUBDIRECTORY NAME
C  OUTPUT: (C*20)  OUTNAME()  = AUTOSTRUCTURE OUTPUT FILES
C
C          (I*4)   PIPEIN     = STANDARD INPUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C DATE:    16/04/98
C
C VERSION: 1.1                          DATE: 16-04-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C-----------------------------------------------------------------------

      INTEGER      PIPEIN, PIPEOU, NDFILE, LOGIC, I
      PARAMETER   (PIPEIN = 5, PIPEOU=6, NDFILE = 4)
C-----------------------------------------------------------------------
      CHARACTER    PASDIR*80 ,  TITLE*40
C-----------------------------------------------------------------------
      CHARACTER    OUTNAME(NDFILE)*20
C-----------------------------------------------------------------------
      LOGICAL      OPEN10      , OPEN11   ,  LPAPER  , LPEND
C-----------------------------------------------------------------------

      
C-----------------------------------------------------------------------
C  READ DATA FROM IDL
C-----------------------------------------------------------------------

      READ(PIPEIN,*) LOGIC
      IF(LOGIC.NE.1)THEN
         
         LPEND = .FALSE.
         READ(PIPEIN,'(A)')PASDIR

         DO I = 1,NDFILE
           READ(PIPEIN,'(A)')OUTNAME(I)
         END DO
         
      ELSE
         LPEND = .TRUE.
      ENDIF

      RETURN
      END
