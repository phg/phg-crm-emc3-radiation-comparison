C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas702/g2open.for,v 1.1 2004/07/06 13:56:17 whitefor Exp $ Date $Date: 2004/07/06 13:56:17 $
C
      SUBROUTINE G2OPEN( PASDIR  , OUTNAME, LISTUNIT, LISTFORM )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G2OPEN *********************
C
C  PURPOSE: TO DETERMINE AUTOSTRUCTURE FILES UNIT NUMBERS FOR ADASDR. 
C
C  CALLING PROGRAM: ADAS702
C
C  SUBROUTINE:
C
C
C  INPUT:  (C*80)  PASDIR     = AUTOSTRUCTURE PASSING SUBDIRECTORY NAME
C  INPUT:  (C*20)  OUTNAME()  = AUTOSTRUCTURE OUTPUT FILES              
C
C  OUTPUT: (C*20)  LISTUNIT() = AUTOSTRUCTURE OUTPUT FILES UNIT NUMBERS            
C  OUTPUT: (L)     LISTFORM() = OUTPUT FILES FORMATING INFORMATION
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C DATE:    19/04/98
C
C VERSION: 1.1                          DATE: 19-04-98
C MODIFIED: Martin O'Mullane
C           First version
C
C-----------------------------------------------------------------------

      INTEGER      IUNT , NDFILE
      INTEGER      MR   , MRU,  I, MDUM, LP1, LP2, I4UNIT
      PARAMETER   (IUNT = 42, NDFILE = 4 )
C-----------------------------------------------------------------------
      INTEGER      listunit(NDFILE)
C-----------------------------------------------------------------------
      CHARACTER    PASDIR*80  ,  FILEIN*100 
C-----------------------------------------------------------------------
      CHARACTER    OUTNAME(NDFILE)*20  
C-----------------------------------------------------------------------
      LOGICAL      LFORM    
C-----------------------------------------------------------------------
      LOGICAL      LISTFORM(NDFILE) 
C-----------------------------------------------------------------------

C Determine what unit numbers to connect the AUTOSTRUCTURE files to
C using the method in ADASDR. Here unformatted files are connected to 
C units 70-73 with unformatted to 75-78.
      
      CALL XXSLEN(PASDIR,LP1,LP2)
      
      MR  = 70
      MRU = 75
      
      DO I= 1, NDFILE
      
         LISTUNIT(I) = 0
         
         FILEIN = PASDIR(LP1:LP2)//OUTNAME(I)
         IF (FILEIN.NE.PASDIR) THEN
         
            OPEN(IUNT,FILE=FILEIN)
            LFORM=.FALSE.
            READ(IUNT,38,END=331,ERR=331)MDUM,MDUM
            LFORM = .TRUE.
  331       CONTINUE
            CLOSE(IUNT)
            
            IF (LFORM) THEN
               LISTUNIT(I) = MR
               LISTFORM(I) = .TRUE.
               MR = MR + 1
            ELSE
               LISTUNIT(I) = MRU
               LISTFORM(I) = .FALSE.
               MRU = MRU + 1
            ENDIF  
              
         ENDIF
         
      END DO
      
  38  FORMAT(5X,I5,5X,I5)

      RETURN
      END
