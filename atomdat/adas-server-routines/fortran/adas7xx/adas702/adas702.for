C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas702/adas702.for,v 1.2 2004/07/06 11:03:41 whitefor Exp $ Date $Date: 2004/07/06 11:03:41 $
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS702 **********************
C
C  VERSION:  1.0
C
C  PURPOSE: Program to launch ADASDR, which is included as a 
C           set of subroutines. The MAIN level has been renamed adasdr.
C           Output datasets are opened here. Modifications to adasdr are 
C           minimised to moving the file opening up to the top level.
C
C  DATA:   Input driving data is now in adf28
C
C  PROGRAM:
C          (I*4) IUNT05    = INPUT UNIT FROM BATCH FILE.
C          (I*4) IUNT17    = OUTPUT UNIT FOR RUN SUMMARY.
C          (I*4) IUNT09    = PARAMETER = UNIT NUMBER FOR I/O
C          (I*4) LP1       = GENERAL CHARACTER STRING LENGTH INDEX
C          (I*4) LP2       = GENERAL CHARACTER STRING LENGTH INDEX
C          (I*4) IZ0       = NUCLEAR CHARGE
C         (C*80) DSNIN     = INPUT DATA FILE NAME
C         (C*80) DSNPAS    = ADF09 PASS FILE
C         (C*80) DSNPAP    = OUTPUT DATA FILE NAME FOR STREAM 17
C         (C*80) DSN80     = GENERAL DATA SET NAME STRING
C         (C*10) DATE      =
C          (L*4) OPEN09    = .TRUE.  => STREAM 09 DEVICE OPEN
C                          = .FALSE. => STREAM 09 DEVICE NOT OPEN
C          (C*3) REP       = 'YES' => TERMINATE PROGRAM EXECUTION
C                            'NO'  => CONTINUE PROGRAM EXECUTION
C         (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C        (C*120) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                            TIME.
C          (C*8) DATE      = CURRENT DATE (AS 'DD/MM/YY')
C          (L*4) LPAPER    = .TRUE. => OUTPUT TO PAPER.TXT FILE
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4) LPASS     = .TRUE. => OUTPUT TO PASS FILE
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4) LBATCH    = FLAGS HOW PROGRAM IS BEING EXECUTED.
C                            .TRUE.  => PROGRAM RUNNING IN BATCH.
C                            .FALSE. => PROGRAM RUNNING IN FOREGROUND.
C          (L*4) LPEND     = FLAGS IF END OF ANALYSIS REQUESTED.
C                            .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                       SETS
C                            .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                       DATA SETS
C          (L*4) LBTSEL    = FLAGS IF PROGRAM TO BE RUN IN BATCH.
C                            .TRUE.  => RUN PROGRAM IN BATCH.
C                            .FALSE. => RUN PROGRAM IN FOREGROUND.
C
C          (I*4) LOGIC     = VALUE READ FROM PIPE FOR LOGICAL VARIABLES
C
C          (I*4) PIPEIN    = STANDARD INPUT
C          (I*4) PIPEOU    = STANDARD OUTPUT
C
C          (C*20)OUTNAME() = AUTOSTRUCTURE OUTPUT FILES              
C          (I*4) LISTUNIT()= AUTOSTRUCTURE UNIT NUMBERS OF I/O FILES
C          (L)   LISTFORM()= OUTPUT FILES FORMATING INFORMATION
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          G2SPF0     ADAS      GATHERS INPUT FILE NAMES FROM IDL OR FILE
C          G2SPF1     ADAS      GATHERS OUTPUT FILE NAMES FROM IDL OR FILE
C          G2SPF2     ADAS      GATHERS OUTPUT FILE NAMES FROM IDL OR FILE
C          XXSLEN     ADAS      GET LENGTH OF STRING VARIABLE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXFLSH     ADAS      FLUSHES I/O BUFFER
C	   XXDATE     ADAS	OBTAINS DATE FROM IDL
C	   ADASDR     AS  	ADASDR CODE
C
C
C
C  AUTHOR : Martin O'Mullane,
C           K1/1/43,
C           JET
C 
C  ADASDR : N. R. BADNELL (1998)   
C
C  VERSION  : 1.1                          DATE: 16/06/1998
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C  VERSION  : 1.2                          DATE: 28/08/2003
C  MODIFIED : Martin O'Mullane  
C             Bring up to date with adasdr v1.10. Remove g2open as
C             input files are now symbolic links made by IDL. This
C             allows adasdr to open them dynamically.
C
C
C-----------------------------------------------------------------------
       INTEGER   LOGIC
       INTEGER   IUNT17     , IUNT09     ,IUNT10      , 
     &           PIPEIN     , PIPEOU     , ONE
       INTEGER   NDFILE     
C-----------------------------------------------------------------------
       PARAMETER ( IUNT17 = 40  , IUNT09 = 39 , IUNT10 = 10 )
       PARAMETER ( PIPEIN = 5   , PIPEOU = 6  , ONE = 1 )
       PARAMETER ( NDFILE = 4   )
C-----------------------------------------------------------------------
       INTEGER   I4UNIT
       INTEGER   LP1        , LP2  , IUNT     , I       
C-----------------------------------------------------------------------
       INTEGER   LISTUNIT(NDFILE)  , LISTDEF(NDFILE*2)
C-----------------------------------------------------------------------
       CHARACTER DSNIN*80   , DSNPAS*80  , DSNPAP*80  , DSN80*80
       CHARACTER DATE*8     , REP*3      , CADAS*120  , PASDIR*80
       CHARACTER TITLE*40
       CHARACTER CFLE*95
C-----------------------------------------------------------------------
       CHARACTER OUTNAME(NDFILE)*20      
C-----------------------------------------------------------------------
       LOGICAL   OPEN09     , OPEN10     , OPEN17     , OPENSC
       LOGICAL   LPAPER
       LOGICAL   LBATCH     , LPASS      , LBTSEL     , LPEND
C-----------------------------------------------------------------------
       LOGICAL   LISTFORM(NDFILE) 
C-----------------------------------------------------------------------
       DATA   OPEN09   /.FALSE./ , OPEN17/.FALSE./, OPENSC/.FALSE./
       DATA   LBTSEL   /.FALSE./
       DATA   REP      / 'NO' /  , CADAS / ' ' /
       DATA   LISTDEF  /70,71,72,73,75,76,77,78/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

       CALL XX0000

C-----------------------------------------------------------------------
C GET PARAMETER LBATCH (IS THIS A BATCH RUN OR NOT?) FROM IDL OR FILE
C-----------------------------------------------------------------------

      READ( PIPEIN, *) LOGIC
      IF (LOGIC.EQ.0) THEN
         LBATCH	 = .FALSE.
      ELSE
         LBATCH = .TRUE.
      ENDIF

C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )
     
C-----------------------------------------------------------------------
C IF OUTPUT FILES ARE ACTIVE CLOSE THEM.
C-----------------------------------------------------------------------

 50    IF (OPEN09) THEN
          CLOSE(IUNT09)
          OPEN09 = .FALSE.
       ENDIF
       IF (OPEN17) THEN
          CLOSE(IUNT17)
          OPEN17 = .FALSE.
       ENDIF
       IF (OPEN10) THEN
          CLOSE(IUNT10)
          OPEN10 = .FALSE.
       ENDIF
       IF (OPENSC) THEN
          CLOSE(IUNT17)
          OPENSC = .FALSE.
       ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL OR FILE IF BATCH).
C-----------------------------------------------------------------------
       
       CALL G2SPF0( REP , DSNIN)

       IF (REP.EQ.'YES') THEN
          GOTO 9999
       ENDIF

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSNIN
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT09 , FILE=DSNIN  , STATUS='UNKNOWN' )
      OPEN09=.TRUE.

C-----------------------------------------------------------------------
C get adasdr input files 
C-----------------------------------------------------------------------
  
  45  LPEND = .FALSE.

      CALL G2SPF1( LPEND   , TITLE   ,
     &             PASDIR  , OUTNAME )  
      
C-----------------------------------------------------------------------
C IF REQUESTED RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 50

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL OR FILE IF BATCH).
C-----------------------------------------------------------------------

 55   LPEND = .FALSE.

      CALL G2SPF2( LPAPER  , LPASS     , LBTSEL    ,
     &             DSNPAS  , DSNPAP    ,
     &             LPEND   , TITLE     ,  *201     )
     
C-----------------------------------------------------------------------
C IF PROGRAM RUN COMPLETE THEN RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 50

C-----------------------------------------------------------------------
C IF THE JOB HAS BEEN SELECTED TO RUN IN BATCH THEN GO BACK TO THE
C INPUT OPTIONS
C-----------------------------------------------------------------------

      IF (LBTSEL) GOTO 50

C-----------------------------------------------------------------------
C GET OUTPUT FILE NAMES AND OPEN THEM
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C OPEN PAPER.TXT FILE - IF REQUESTED
C If not iunt17 must be opened as scratch since adasdr writes to it
C automatically.
C-----------------------------------------------------------------------

      IF ((.NOT.OPEN17) .AND. LPAPER) THEN
         DSN80=' '
         DSN80=DSNPAP
         OPEN(UNIT=IUNT17,FILE=DSN80, STATUS='UNKNOWN')
         OPEN17=.TRUE.
      ELSE
         OPEN(UNIT=IUNT17,STATUS='SCRATCH')
         OPENSC = .TRUE.
      ENDIF

C-----------------------------------------------------------------------
C OPEN adf09 PASS FILE - FORCED TO BE TRUE
C-----------------------------------------------------------------------

      IF ((.NOT.OPEN10) .AND. LPASS) THEN
         DSN80=' '
         DSN80=DSNPAS
         OPEN(UNIT=IUNT10,FILE=DSN80, STATUS='UNKNOWN')
         OPEN10=.TRUE.
      ENDIF
      
      
      



C-----------------------------------------------------------------------
C Write an ADAS header to paper.txt
C-----------------------------------------------------------------------

      IF (OPEN17) THEN
      
         WRITE(IUNT17,1000) CADAS(2:80)
         WRITE(IUNT17,1001) 'ADASDR ','ADAS702',DATE
         WRITE(IUNT17,1002) TITLE
         WRITE(IUNT17,1003) dsnin
         
      ENDIF
    


C-----------------------------------------------------------------------
C TELL IDL TO START
C-----------------------------------------------------------------------

      IF(.NOT.LBATCH) THEN
         WRITE(PIPEOU,*)ONE
         CALL XXFLSH(PIPEOU)
      ENDIF

C-----------------------------------------------------------------------
C DO THE WORK - note that if there is a problem in ADASDR it terminates
C               with a STOP. This causes problems in the pipe 
C               communications and an error message is popped up
C               saying that ADASDR has crashed. On canceling the popup
C               the user is returned to the series 7 main menu.
C-----------------------------------------------------------------------

        call adasdr          
	    		
C-----------------------------------------------------------------------
C TELL IDL THAT WE HAVE COMPLETED ONE STAGE FOR PROGRESS WINDOW
C-----------------------------------------------------------------------

      IF(.NOT.LBATCH) THEN
          WRITE(PIPEOU,*)ONE
          CALL XXFLSH(PIPEOU)
      ENDIF
      

C-----------------------------------------------------------------------
C Append producer information to adf09 file 
C-----------------------------------------------------------------------
      
       WRITE(IUNT10,1080)DATE

 201  CONTINUE
       
C-----------------------------------------------------------------------
C GO BACK TO DATA-SET INPUT SCREEN
C-----------------------------------------------------------------------

       GO TO 50

     
 9999 CONTINUE
      CLOSE( IUNT09 )
      CLOSE( IUNT10 )
      CLOSE( IUNT17 )
    
      STOP
    
C-----------------------------------------------------------------------

 1000 FORMAT(A79)
 1001 FORMAT(1x,27('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,27('*')/)
 1002 FORMAT(/ ,19('-'),1X,A40,1X,19('-')//)
 1003 FORMAT(1x,'INPUT FILE NAME    : ',A44,//)

 1080  FORMAT('C',79('-')/
     &        'C  Dielecronic Recombination rates generated from ',
     &            'AUTOSTRUCTURE'/
     &        'C'/
     &        'C  Program  : ADAS702'/
     &        'C  Producer : who is responsible for this?',/
     &        'C  Date     : ',1A8/
     &        'C',79('-'))
C-----------------------------------------------------------------------
      END
