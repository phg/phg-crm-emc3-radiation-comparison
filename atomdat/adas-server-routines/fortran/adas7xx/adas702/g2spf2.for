C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas702/g2spf2.for,v 1.1 2004/07/06 13:56:34 whitefor Exp $ Date $Date: 2004/07/06 13:56:34 $
C
      SUBROUTINE G2SPF2( LPAPER  , LPASS     , LBTSEL    ,
     &                   DSNPAS  , DSNPAP    ,
     &                   LPEND   , TITLE     ,  *
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G2SPF1 *********************
C
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL OR
C	    THE INFO FILE (WHICH HAS BEEN REDIRECTED TO STANDARD INPUT).
C
C  CALLING PROGRAM: ADAS701
C
C  SUBROUTINE:
C
C
C  OUTPUT: (L*4)   LPAPER     = .TRUE.  => OUTPUT TEXT TO 'PAPER.TXT' FILE.
C                               .FALSE. => NO TEXT OUTPUT
C  OUTPUT: (L*4)   LPASS      = .TRUE.  => OUTPUT TEXT TO ADF09 PASS FILE.
C                               .FALSE. => NO PASS FILE OUTPUT
C  OUTPUT: (L*4)   LBTSEL     = .TRUE.  => USER HAS SELECTED BATCH EXECUTION
C                               .FALSE. => 'RUN NOW' OR 'CANCEL' OPTION
C
C  OUTPUT: (C*80)  DSNPAS     = ADF09 PASS FILE NAME
C  OUTPUT: (C*80)  DSNPAP     = OUTPUT TEXT FILE NAME
C
C  OUTPUT: (L*4)   LPEND      = .TRUE.  => USER SELECTED 'CANCEL'
C                               .FALSE. => USER DID NOT
C
C  OUTPUT: (C*40)  TITLE      = USER SUPPLIED TITLE FOR THIS RUN
C
C          (I*4)   PIPEIN     = STANDARD INPUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C DATE:    16/04/98
C
C VERSION: 1.1                          DATE: 16-04-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C-----------------------------------------------------------------------

      INTEGER      PIPEIN, PIPEOU, LOGIC 
      PARAMETER   (PIPEIN = 5, PIPEOU=6 )
C-----------------------------------------------------------------------
      CHARACTER    DSNOUT*80   , DSNPAS*80 , DSNPAP*80 ,  TITLE*40
C-----------------------------------------------------------------------
      LOGICAL      OPEN10      , OPEN11    , LBTSEL    ,  LPAPER  ,
     &             LPASS       , LPEND
C-----------------------------------------------------------------------

      
C-----------------------------------------------------------------------
C  READ DATA FROM IDL
C-----------------------------------------------------------------------

      READ(PIPEIN,*) LOGIC
      
      IF (LOGIC.NE.1)THEN
     
         LPEND = .FALSE.
         READ(PIPEIN,'(A)')TITLE
         READ(PIPEIN,'(A)')DSNPAP
         READ(PIPEIN,'(A)')DSNPAS
                   
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LPAPER=.FALSE.
         ELSE
            LPAPER=.TRUE.
         ENDIF
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LPASS=.FALSE.
         ELSE
            LPASS=.TRUE.
         ENDIF
         
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LBTSEL=.FALSE.
         ELSE
            LBTSEL=.TRUE.
         ENDIF
     
      ELSE
         LPEND = .TRUE.
      ENDIF

      RETURN
      END
