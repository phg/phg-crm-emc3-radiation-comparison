C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas702/g2spf0.for,v 1.1 2004/07/06 13:56:22 whitefor Exp $ Date $Date: 2004/07/06 13:56:22 $
C
      SUBROUTINE G2SPF0( REP    , DSFULL)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G2SPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL OR INFO FILE (STANDARD
C	    (INPUT) IF BATCH - INPUT DATA SET SPECIFICATIONS.
C
C  CALLING PROGRAM: ADAS702
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR  : Martin O'Mullane
C
C DATE    : 16/06/98
C
C VERSION : 1.1                          DATE: 16-06-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEIN
      PARAMETER ( PIPEIN=5          )

      CHARACTER   REP*3         , DSFULL*80
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C READ IN DATA FROM STANDARD INPUT (IDL OR INFO FILE)
C-----------------------------------------------------------------------


      READ(PIPEIN,'(A)')REP
      READ(PIPEIN,'(A)')DSFULL

      RETURN
      END
