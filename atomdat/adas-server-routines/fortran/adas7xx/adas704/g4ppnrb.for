C UNIX-IDL - SCCS info: @(#)$Header: /home/adascvs/fortran/adas7xx/adas704/g4ppnrb.for,v 1.1 2004/07/06 13:57:14 whitefor Exp $ Date $Date: 2004/07/06 13:57:14 $
C
      subroutine g4ppnrb(iunt09  , isreq  , wghtn  , emin , emax ,
     &                   iunt10  , iunt07 , ltext  , date , user )

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 ROUTINE: G4PPNRB **********************
C
C  PURPOSE: Program to post-process AUTOSTRUCTURE to give the spin 
C           breakdown Auger rates. 
C
C
C-----------------------------------------------------------------------
C
C  Based upon PPAUTS2:
C
C  PROGRAM PPAUTS2
C  ---------------
C
C  PROGRAM TO READ OUTPUT FILE FROM AUTOSTRUCTURE AND STRIP OUT
C  AUTOIONISATION TRANSITION PROBABILITIES OF INTEREST
C
C  REVISED DATA READING ALGORITHM
C
C  THIS VERSION OF CODE INTERROGATES MULTIPLE N AND L OUTPUT AS
C  PREFERRED BY NRB.
C
C  THUS REQUIRES A PRIOR RUN OF AUTOSTRUCTURE WITH OUTPUT GENERALLY
C  WRITTEN TO A FILE SUCH AS
C              JETUID.AS91#C.AS#O1.PASS3
C
C  CHOICES ON THE SPIN (2S+1) OF THE BOUND STATE AND ON THE ENERGY OF
C  THE FREE STATE ENABLE RESOLUTION INTO SPIN SYSTEM AND PARENT
C
C  THE FINAL OUTPUT CONTAINS WEIGHTED SUMS OF AUGER TRANSITION
C  PROBABILITIES, WHICH ARE SUMMED OVER L SEPARATELY AND THEN OVER N.
C
C  EACH INDIVIDUAL AUGER TRANSITION PROBABILITY IS WEIGHTED BY (2J+1)
C
C  AN N-SHELL AVERAGE IS OBTAINED BY NORAMILISING BY THE WEIGHT OF THE
C  ENTIRE N-SHELL. THIS IS INPUT BY THE USER AS A MULTIPLIER ON N**2
C         I.E.
C           N-SHELL WEIGHT = WGHTN * N**2
C
C
C   AUTHOR:  WILLIAM J. DICKSON
C   DATE:  2ND DECEMBER 1992
C   ADDRESS:  K1/1/26, JET JOINT UNDERTAKING
C
C-----------------------------------------------------------------------
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          G4LLVL     ADAS      CHECK WETHER A SPECIFIC TRANSITION
C                               BELONGS TO REQUIRED SET
C
C
C  AUTHOR : Martin O'Mullane,
C           K1/1/43,
C           JET
C 
C
C  VERSION  : 1.1                          
C  DATE     : 15/09/2000
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C
C-----------------------------------------------------------------------
       INTEGER   NDLEV          , NDN      , NDL
C-----------------------------------------------------------------------
       PARAMETER ( NDLEV = 1000 , NDN = 50 , NDL = 10 )
C-----------------------------------------------------------------------
       INTEGER   ISREQ      , IUNT07     , IUNT09   , IUNT10
       INTEGER   ICAL       , NVIN       , LVIN
       INTEGER   ICF        , ICFA       , ICFF     , IFLAG   ,      
     &           IL         , ILEV       , ILR      , ILREP   ,        
     &           IN         , INREP      , IR       , ISM     ,        
     &           ITRAA      , ITRM       , IWA      , J       , K  ,i,
     &           L          , LV         , LVA      , LVF     ,       
     &           NLEVEL     , NLREP      , NLVB     , NLVF    ,        
     &           NMAXEX     , NNREP      , NTRAA    , i4unit  , ifmt
C-----------------------------------------------------------------------
       REAL*8    WGHTN      , EMIN       , EMAX
       REAL*8    AA         , DELC       , DELE     , DELI     , 
     &           SUMAA      , SUMLWAA    , SUMWAA   , WGHT           
C-----------------------------------------------------------------------
       LOGICAL   LTEXT      , lfirst
C-----------------------------------------------------------------------
       character user*30    , date*8
       CHARACTER LINE1*132  , LINE2*80   , WF*5     , 
     &           STRG1*7    , STRG2*10   , STRG3*5
C-----------------------------------------------------------------------
       INTEGER   IND(NDLEV)      , INDF(NDLEV)      , NREP(NDN) , 
     &           LREP(NDL) 
C-----------------------------------------------------------------------
       REAL*8    SIGWAA(NDN,NDL) , SIGAA(NDN,NDL)
C-----------------------------------------------------------------------
       CHARACTER AALINE(NDLEV)*80 
C-----------------------------------------------------------------------
       DATA      STRG1 /'NLEVEL='/
       DATA      STRG2 /'AUTO-IONIZ'/
       DATA      STRG3 /'  NV='/
C-----------------------------------------------------------------------
              

C----------------------------------------------------------------------
C      READ IN VALENCE N,L
C----------------------------------------------------------------------


       ICAL = 0
   90  READ(IUNT09,1000, END=5010 ) LINE1
       IF( LINE1(1:5) .EQ. STRG3 ) THEN
         ICAL = ICAL + 1
         READ(LINE1,1004) NVIN, LVIN
         if (ltext) WRITE(iunt07,1108) LINE1
       ELSE
         GOTO 90
       ENDIF
       

C----------------------------------------------------------------------
C      SORT REPRESENTAITVE N AND L
C----------------------------------------------------------------------

       IF (ICAL .EQ. 1) THEN
          NNREP = 1
          NLREP = 1
          NREP(NNREP) = NVIN
          LREP(NLREP) = LVIN
       ENDIF

       INREP = 0
       DO IR = 1,NNREP
          IF( NREP(IR) .EQ. NVIN) INREP = IR
       END DO
       
       IF (INREP .EQ. 0) THEN
          NNREP = NNREP + 1
          NREP(NNREP) = NVIN
          INREP = NNREP
       ENDIF

       ILREP = 0
       DO IR = 1,NLREP
          IF ( LREP(IR) .EQ. LVIN) ILREP = IR
       END DO
       
       IF(ILREP .EQ. 0) THEN
          NLREP = NLREP + 1
          LREP(NLREP) = LVIN
          ILREP = NLREP
       ENDIF

C----------------------------------------------------------------------
C      READ IN CONFIGURATION LIST
C----------------------------------------------------------------------

   92  READ(IUNT09,1000) LINE1
       IF( LINE1(38:47) .NE. STRG2 ) THEN
         IF (LTEXT) WRITE(iunt07,1108) LINE1
         GOTO 92
       ENDIF

C----------------------------------------------------------------------
C      READ AUGER TRANSITION PROBABILITIES INTO ARRAY  AALINE()
C----------------------------------------------------------------------

       READ(IUNT09,1000) LINE1
       ITRAA = 0
   96  READ(IUNT09,1002) LINE2
       READ(LINE2,1006)  ICF
       IF( ICF .GT. 0) THEN
           ITRAA = ITRAA + 1
           AALINE(ITRAA) = LINE2
           GOTO 96
       ENDIF
       NTRAA = ITRAA

C----------------------------------------------------------------------
C      READ IN LEVEL LIST
C      IDENTIFY LEVELS IN CONTINUUM OR WITH REQUIRED SPIN
C----------------------------------------------------------------------
       
       IF (LTEXT) WRITE(iunt07,1132)  EMIN, EMAX , WGHTN, ISREQ
       IF (LTEXT) WRITE(iunt07,1136)  NTRAA
       IF (LTEXT) WRITE(iunt07,1104)  ISREQ
       IF (LTEXT) WRITE(iunt07,1014)

C-----------------------------------------------------
C      SKIP ENERGY LEVEL INPUT FOR LEVELS ABOVE NMAXEX
C-----------------------------------------------------

       NMAXEX = 20
       IF(NVIN. GE. NMAXEX) GOTO 112

       IFLAG = -1
  100  READ(IUNT09,1000) LINE1
       IF ( LINE1(4:10) .EQ. STRG1 ) THEN
         IFLAG = 1
         READ(LINE1,1010) NLEVEL
         READ(IUNT09,1000) LINE1
         NLVB = 0
         NLVF = 0
         DO ILEV = 1,NLEVEL
            READ(IUNT09,1012) K , LV, ITRM, ISM, L, J, ICF , DELE
            ISM = ABS(ISM)
            IF ( ICF.LT.0 .AND. DELE.GE.EMIN .AND. DELE.LE.EMAX) THEN
               IF (LTEXT) WRITE(iunt07,1016) K, LV, ITRM, 
     &                                       ISM, L, J, ICF, DELE
               NLVF = NLVF + 1
               INDF(NLVF) = LV
               EMIN = DMIN1( EMIN , DELE )
               EMAX = DMAX1( EMAX , DELE )
            ELSEIF(ISM.EQ.ISREQ .AND. DELE.GT.EMIN .AND. ICF.GT.0) THEN
               IF (LTEXT)  WRITE(iunt07,1016) K, LV, ITRM, ISM, 
     &                                        L, J, ICF, DELE
               NLVB = NLVB + 1
               IND(NLVB) = LV
            ENDIF
         END DO
       ENDIF
       IF( IFLAG .LT. 0 ) GOTO 100

  112  CONTINUE


C----------------------------------------------------------------------
C      WORK THROUGH AUTOIONISATION TRANSITION LIST FOR EACH
C      UPPER LEVEL
C----------------------------------------------------------------------

       IF (LTEXT) WRITE(iunt07,1110)
       
       SUMAA  = 0.0D+0
       SUMWAA = 0.0D+0

       DO ITRAA = 1,NTRAA
       
           LINE2(1:80) = AALINE(ITRAA)
           READ(LINE2,1032) ICFA, LVA, IWA, ICFF, LVF,  WF, AA,
     &                                                    DELC, DELI
           IFLAG = -1
           CALL g4llev( LVA , LVF , NLVB, IND, NLVF, INDF , IFLAG )
           IF( IFLAG .GT. 0) THEN
               SUMAA  = SUMAA + DABS(AA)
               SUMWAA = SUMWAA + IWA*DABS(AA)
               IF (LTEXT) WRITE(iunt07,1033) ICFA, LVA, IWA, ICFF, LVF, 
     &                                       WF, AA,DELC, DELI , SUMAA, 
     &                                       SUMWAA
           ENDIF

       END DO
  
  
       SIGWAA(INREP,ILREP) = SUMWAA
       SIGAA(INREP,ILREP)  = SUMAA
       IF (LTEXT) WRITE(iunt07,1134) NVIN,LVIN,
     &                   SIGWAA(INREP,ILREP),SIGAA(INREP,ILREP)

C----------------------------------------------------------------------
       GOTO 90

 5000  WRITE(i4unit(-1),1120)
 5010  CONTINUE

C----------------------------------------------------------------------
C      WRITE N,L TOTALS
C      and the adf18 passing file
C----------------------------------------------------------------------
       
       write(iunt10,2000)
       
       lfirst = .FALSE.
       assign 2010 to ifmt
       
       IF (LTEXT) WRITE(iunt07,1150)  ( LREP(IR), IR = 1, NLREP)
       DO  IN = 1,NNREP
          SUMLWAA = 0.0D+0
          DO 310 IL = 1, NLREP
            SUMLWAA = SUMLWAA + SIGWAA(IN,IL)
  310     CONTINUE
          WGHT = WGHTN * NREP(IN) * NREP(IN)
          IF (LTEXT) WRITE(iunt07,1154)  NREP(IN), SUMLWAA/WGHT , 
     &               SUMLWAA, (SIGWAA(IN,ILR), ILR = 1, NLREP)
     
          if (SUMLWAA.GT.0.0D0) lfirst = .TRUE.
          if (in.eq.nnrep) assign 2030 to ifmt
          
          if (SUMLWAA.GT.0.0D0) write(iunt10,ifmt)nrep(in), sumlwaa/wght
          
          if (lfirst) assign 2020 to ifmt
     
       END DO

C write a comment - preceed with a blank line to avoid problems with
C reading the file in 208.

       write(iunt10,*)' '
       write(iunt10,2040)user, date
       
       

C------FORMAT STATEMENTS FOR READING INPUT FILE

 1000  FORMAT(1A132)
 1002  FORMAT(1A80)
 1004  FORMAT(5X,I5,5X,I5)
 1006  FORMAT(I5)
 1010  FORMAT(10X,I5)
 1012  FORMAT(7I5,F15.6)
 1014  FORMAT(1H ,'    K   LV    T 2S+1    L   2J   CF     (EK-E1)/RY')
 1016  FORMAT(1H ,7I5,F15.6)
 1032  FORMAT(5I5,1A5,3X,D12.5,2F15.6)
 1033  FORMAT(1H ,5I5,1A5,3X,1P,D12.5,0P,2F15.6,5X,1P,2D12.5 )

C------FORMAT STATEMENTS OUTPUT

 1104  FORMAT(//1H ,'LIST OF CONTINUUM LEVELS WITHIN ENERGY FILTER AND B
     &OUND LEVELS WITH 2S+1 =',I2/)
 1108  FORMAT(1A132)
 1110  FORMAT(//1H ,'        I-S            C-S           AUTOIONISATION
     & TRANSITIONS :'/
     &         1H ,'   CF   LV    W   CF   LV    W       AA*SEC
     &E-C(RYD)       E-I(RYD)       SUM(AA)    SUM(W.AA) ')
 1120  FORMAT(//1H ,'ERROR OPENING DATASET')
 1132  FORMAT(1H0,' EMIN = ',F9.6,3X,'EMAX = ',F9.6,
     &            '  WGHTN = ',F7.3,
     &            '    REQUIRED SPIN FOR BOUND STATES, 2S+1 = ',I3)
 1134  FORMAT(/1H ,'NV =',I5,'  LV=',I5,1P,'   SUM(W*AA) =',D12.5,
     &                    3X,'SUM(AA) =',D12.5)
 1136  FORMAT(//' TOTAL NO. OF AUGER TRANSITIONS = ',I5)
 1150  FORMAT(1H1,'  SUM OF (2J+1)*AA '
     &                   / 1H ,'   NV    S(W*AA)/WN    S(W*AA)    L:',
     &                  5(I5,7X))
 1154  FORMAT(1H ,I5,1P,2X,2D12.3,3X,5D12.3)

C------FORMAT STATEMENTS FOR adf18/a09_p204 

 2000  format('Supplementary LS-breakdown autoionisation ',
     &        'data between parent spin pairs',/, 72('-'),/,1x,/,
     &        '  (pmni,isysi ; pmnf)           ncut1    A Auger',
     &        '        slope',/,
     &        '  -------------------           -----   ---------',
     &        '       -----')
 2010  format('     X     X      X',14x,i3,1p,D13.3)
 2020  format(33x,i3,1p,D13.3)
 2030  format(33x,i3,1p,D13.3,'       -5.00')
 2040  FORMAT('C',72('-'),/,
     &        'C',/,'C  Spin breakdown Auger rates calculated from ',
     &              'AUTOSTRUCTURE,',/,
     &        'C  using : XXXX',/,'C',/,
     &        'C  CODE     : ADAS704',/,
     &        'C  PRODUCER : ',A30/,
     &        'C  DATE     : ',1A8,/,'C',/,'C',72('-'))


C----------------------------------------------------------------------

      END
