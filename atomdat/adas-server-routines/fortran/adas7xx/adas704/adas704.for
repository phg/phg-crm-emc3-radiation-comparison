C UNIX-IDL - SCCS info: @(#)$Header: /home/adascvs/fortran/adas7xx/adas704/adas704.for,v 1.1 2004/07/06 11:04:24 whitefor Exp $ Date $Date: 2004/07/06 11:04:24 $
C
       Program ADAS704
       
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS704 **********************
C
C  PURPOSE: Program to post-process AUTOSTRUCTURE to give the spin 
C           breakdown Auger rates. 
C
C  DATA:   Input in adf27/<seq>like/<seq>like_<ion>ic_aut.dat
C
C  PROGRAM:
C          (I*4) IUNT09    = INPUT UNIT FOR ADF27 FILE.
C          (I*4) IUNT07    = OUTPUT UNIT FOR RUN SUMMARY
C          (I*4) IUNT10    = OUTPUT UNIT FOR DATA
C         (C*80) DSNIN     = INPUT DATA FILE NAME
C         (C*80) DSNOUT    = OUTPUT PASSING FILE NAME
C         (C*80) DSNTEXT   = TEXT REPORT FILE NAME
C          (L*4) OPEN09    = .TRUE.  => STREAM 09 DEVICE OPEN
C                          = .FALSE. => STREAM 09 DEVICE NOT OPEN
C          (L*4) OPEN10    = .TRUE.  => STREAM 10 DEVICE OPEN
C                          = .FALSE. => STREAM 10 DEVICE NOT OPEN
C          (C*4) REP       = ACTION FROM IDL
C         (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C        (C*120) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                            TIME.
C          (C*8) DATE      = CURRENT DATE (AS 'DD/MM/YY')
C          (L*4) LTEXT    = .TRUE.  => OUTPUT TO PAPER.TXT FILE
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (I*4) LOGIC     = VALUE READ FROM PIPE FOR LOGICAL VARIABLES
C
C          (I*4) PIPEIN    = STANDARD INPUT
C          (I*4) PIPEOU    = STANDARD OUTPUT
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          G4PPNRB    ADAS      Reads AS file and extracts Auger rates 
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C	   XXDATE     ADAS	OBTAINS DATE FROM IDL
C
C
C
C  AUTHOR : Martin O'Mullane,
C           K1/1/43,
C           JET
C 
C
C  VERSION  : 1.1                          
C  DATE     : 15/09/2000
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C
C-----------------------------------------------------------------------
       INTEGER   LOGIC
       INTEGER   IUNT07     , IUNT09      , iunt10   ,
     &           PIPEIN     , PIPEOU      , ONE
C-----------------------------------------------------------------------
       PARAMETER ( IUNT07 = 7   , IUNT09 = 9  , IUNT10 = 10)
       PARAMETER ( PIPEIN = 5   , PIPEOU = 6  , ONE    = 1 )
C-----------------------------------------------------------------------
       INTEGER   ISREQ       
C-----------------------------------------------------------------------
       REAL*8    WGHTN      , EMIN       , EMAX
C-----------------------------------------------------------------------
       CHARACTER DSNIN*80   , DSNOUT*80  , DSNTEXT*80
       CHARACTER DATE*8     , USER*30    , REP*4
C-----------------------------------------------------------------------
       LOGICAL   OPEN09     , OPEN10
       LOGICAL   LTEXT
C-----------------------------------------------------------------------
       DATA   OPEN09   /.FALSE./ , OPEN10/.FALSE./
       DATA   REP      / 'NO  ' /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

       CALL XX0000


C-----------------------------------------------------------------------
C Get current date and user
C-----------------------------------------------------------------------

      CALL XXDATE(DATE)
      CALL XXNAME(USER)
     
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 09 - CLOSE THE UNIT
C-----------------------------------------------------------------------

 100   continue
 
       IF (OPEN09) THEN
          CLOSE(IUNT09)
          OPEN09 = .FALSE.
       ENDIF

       IF (OPEN10) THEN
          CLOSE(IUNT10)
          OPEN10 = .FALSE.
       ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL OR FILE IF BATCH).
C-----------------------------------------------------------------------

       READ(PIPEIN,'(A)')REP
       READ(PIPEIN,'(A)')DSNIN
       
       
       IF (REP.EQ.'END ') THEN
          GOTO 9999
       ENDIF

C-----------------------------------------------------------------------
C GET DATA FROM PROCESSING SCREEN
C-----------------------------------------------------------------------
   
  200  continue
            
       READ(PIPEIN,'(A)')REP

       IF (REP.EQ.'MENU') THEN
          GOTO 9999
       elseif (rep.eq.'BACK') then
          GOTO 100
       ENDIF
       
       READ(PIPEIN,*) isreq
       READ(PIPEIN,*) wghtn
       READ(PIPEIN,*) emin
       READ(PIPEIN,*) emax



C-----------------------------------------------------------------------
C GET OUTPUT FILES FROM IDL.
C-----------------------------------------------------------------------

  300  continue
  
       READ(PIPEIN,'(A)')REP
       IF (REP.EQ.'MENU') THEN
          GOTO 9999
       ELSEIF (REP.EQ.'BACK') then
          GOTO 200
       ENDIF
       
       
       READ(PIPEIN,'(A)')DSNOUT
       READ(PIPEIN,*)LOGIC
       if (logic.EQ.one) then
          READ(PIPEIN,'(A)')DSNTEXT
          ltext = .TRUE.
       else
          ltext = .FALSE.
       endif        


C-----------------------------------------------------------------------
C OPEN INPUT AND OUTPUT DATA FILES
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT09 , FILE=DSNIN  , STATUS='UNKNOWN' )
      OPEN09=.TRUE.
      OPEN( UNIT=IUNT10 , FILE=DSNOUT  , STATUS='UNKNOWN' )
      OPEN10=.TRUE.

      if (ltext) then
         OPEN( UNIT=IUNT07 , FILE=DSNTEXT  , STATUS='UNKNOWN' )
         WRITE(IUNT07,1001)
     &     'SPIN BREAKDOWN AUGER RATE CALCULATION',
     &     'ADAS704' , DATE
         WRITE(IUNT07,*)' '
      endif
         

      call g4ppnrb(iunt09  , isreq  , wghtn  , emin , emax ,
     &             iunt10  , iunt07 , ltext  , date , user)

      CLOSE( IUNT09 )
      CLOSE( IUNT10 )
      CLOSE( IUNT07 )
     



C-----------------------------------------------------------------------
C GO BACK TO DATA-SET INPUT SCREEN
C-----------------------------------------------------------------------

       GO TO 300

     
 9999 CONTINUE
 
      CLOSE( IUNT09 )
      CLOSE( IUNT10 )
      CLOSE( IUNT07 )
     
    
C-----------------------------------------------------------------------
 1001 FORMAT(1X,11('*'),' TABULAR OUTPUT FROM ',A42,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,11('*'),/)
C-----------------------------------------------------------------------

      END
