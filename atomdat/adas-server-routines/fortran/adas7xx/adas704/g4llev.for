      SUBROUTINE G4LLEV( LVA , LVF , NLVB, IND, NLVF, INDF , IFLAG )

      IMPLICIT REAL*8(A-H,O-Z)
C---------------------------------------------------------------------
C
C******************* FORTRAN77 ROUTINE: G4LLEV ***********************
C
C  PURPOSE:  Determines whether a particular transition belongs
C            to the required set by checking the bound and free
C            states against the level lists.
C
C       INPUT:
C          LVA    -   TEST BOUND LEVEL INDEX
C          LVF    -   TEST FREE LEVEL INDEX
C          NLVB   -   NO. OF BOUND STATES
C          IND()  -   SET OF BOUND STATES
C          NLVF   -   NO. OF FREE STATES
C          INDF() -   SET OF FREE STATES
C
C       OUTPUT:
C          IFLAG  -   = 0, LVA AND LVF DO NOT BELONG TO IND, INDF
C                     = 1, LVA AND LVF DO     BELONG TO IND, INDF
C
C
C  VERSION  : 1.1
C  DATE     : 15/09/2000
C  MODIFIED : Martin O'Mullane
C             First version.
C
C  VERSION  : 1.2
C  DATE     : 16/05/2007
C  MODIFIED : Martin O'Mullane
C             - Updated comments as part of subroutine
C               documentation production.
C
C---------------------------------------------------------------------

       PARAMETER(NDLEV = 1000)

       DIMENSION IND(NDLEV) , INDF(NDLEV)

       IFLAG  = -1
       IFLAGB = -1
       IFLAGF = -1
C----------------------------
C      CHECK BOUND STATES
C----------------------------
       DO 100 ILEV = 1, NLVB
          ILTEST = IND(ILEV)
          IF( ILTEST .EQ.  LVA) THEN
            IFLAGB  = 1
            GOTO 120
          ENDIF
  100  CONTINUE

       IF(IFLAGB .LT. 0) GOTO 500
C
C----------------------------
C      CHECK FREE  STATES
C----------------------------
  120  CONTINUE

       DO 140 ILEV = 1, NLVF
          ILTEST = INDF(ILEV)
          IF( ILTEST .EQ.  LVF) THEN
            IFLAGF  = 1
            GOTO 160
          ENDIF
  140  CONTINUE
       IF(IFLAGF .LT. 0) GOTO 500

  160  IFLAG = 1

  500  RETURN
       END
