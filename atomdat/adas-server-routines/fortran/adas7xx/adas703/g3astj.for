C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3astj.for,v 1.1 2004/07/06 13:56:40 whitefor Exp $ Date $Date: 2004/07/06 13:56:40 $
C
       SUBROUTINE G3ASTJ(IWR04  , IWRP  , LPAPER , IOPT   , DATE ,
     &                   IZ0    , IZ    , IZ1    , NELEC  ,
     &                   BWNO   , MAXT  , TEA    ,
     &                   NAUTO  , ITIA  , ITCA   , AUGA   ,
     &                   NCONFG , ICF   , NLSETA , EPSIL  , EPSILMAX ,
     &                   NLEVL  , NFREE , IBREFA ,
     &                   IA     , ISA   , ILA    , ICFA   , EFREE    ,
     &                   ENERA  , WTA   , IXREFA , IFREFA ,
     &                   LPA    , ISPA  , XJP    ,
     &                   NTRAN  , IIPA  , IIA    , AA     , FA)

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G3ASTJ *********************
C
C  PURPOSE: CALCULATES COLLISION STRENGTHS IN IMPACT PARAMETER
C           APPROXIMATION AND WRITES THE ADF04 AND PAPER.TXT FILES.
C
C  CALLING PROGRAM: ADAS703
C
C  SUBROUTINE:
C
C
C  
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURE/DENSITY
C          (I*4)   BWNO(I)  =  IONISATION POTENTIALS  (CM-1)
C                  TRA(IT)  =  REDUCED TEMPERATURE SET (K)
C                  MAXT     =  NUMBER OF TEMPERATURES  (LE.NDTIN)
C                  IOPT     =  OUTPUT CONFIGURATION FORM
C                              (1=EISSNER,0=STANDARD)
C
C                  IA()     =  LEVL INDEX
C                  ILA()    =  TOTAL ORBITAL QUANTUM NUMBER
C                  ISA()    =  TOTAL SPIN    QUANTUM NUMBER (2*S+1)
C                  IJA()    =  TOTAL ANGUL.  QUANTUM NUMBER (2*J)
C                  ICFA()   =  CONFIGRATION INDEX OF LEVL
C                  ENERA()  =  LEVEL ENERGY (CM-1)
C                  WTA()    =  (LEVL STATISTICAL WEIGHT-1)/2
C                  NLEVL    =  NUMBER OF LEVLS
C                  IIA()    =  LOWER LEVL INDEX OF TRANSITION
C                  IIPA()   =  UPPER LEVL INDEX OF TRANSITION
C                  AA()     =  A-VALUE FOR TRANSITION
C                  GAMSSA(,)=  GAMMAS FOR TRANSITION
C                  NTRAN    =  NUMBER OF TRANSITIONS
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH    IDL-ADAS	CALLS FLUSH TO CLEAR PIPES.
C
C
C AUTHOR   : Martin O'Mullane
C
C DATE     : 29/04/98
C
C VERSION  : 1.1                          DATE: 29-04-98
C MODIFIED : Martin O'Mullane
C            FIRST VERSION
C
C-----------------------------------------------------------------------
       INTEGER    NDLEVL       , NDTRAN       , NDTIN
       INTEGER    NDFREE       , ndp          , NDCFG       , NDPRNT 
C-----------------------------------------------------------------------
       PARAMETER( NDLEVL = 300 , NDTRAN = 9000 , NDTIN = 14 )
       PARAMETER( NDFREE = 100 , ndp    = 5    , NDCFG = 30 )
       PARAMETER( NDPRNT = 10  )
C-----------------------------------------------------------------------
       INTEGER    IWR04      , IWRP       , IOPT
       INTEGER    I          , ICFII      , ICFIIP    ,
     &            ICFSEL     , ICONF      , ICONFG    , ICONF1    ,
     &            II         , 
     &            IIP        , ILM        , IORB      ,
     &            IP         , IS         ,
     &            ISM        , 
     &            IT         , ILEVL      ,
     &            ITRAN      , IZ         ,
     &            IZ0        , IZ1        ,
     &            J          , K          , KP        , 
     &            M          , MAXT       , N         ,
     &            NAUTO      , NCONFG     , NELEC     ,
     &            NEQUIV     , NFREE      , NII       ,
     &            NIIP       , NMET       , NORB      , NP        ,
     &            NSHEL      , NLEVL      , NTRAN
       INTEGER    ind1       , ind2       , i4unit
C-----------------------------------------------------------------------
       REAL*8     BWNMIN
C-----------------------------------------------------------------------
       REAL*8     A          , ATE        , EII        ,
     &            EIIP       , EPSILMAX   , F          ,
     &            RAT        , WII        , WIIP       , WP       ,
     &            WT         , Z1
C-----------------------------------------------------------------------
       CHARACTER  DATE*8     , ename*2    
       CHARACTER  ORBITL*4
       CHARACTER  STRGO*19   
       CHARACTER  SLINE*133  , LLINE*190  , STRING*133
       CHARACTER  BLANKS*133 , EPSILINE*133
       CHARACTER  CONFIG*242 , LGLINE*266
C-----------------------------------------------------------------------
       INTEGER    KA(15)          , JA(15)
       INTEGER    ICF(ndcfg)      , IPOINT(ndcfg)   , NLSETA(ndcfg,30,2)
       INTEGER    IA(NDLEVL)      , IXREFA(NDLEVL)
       INTEGER    ISA(NDLEVL)     , ILA(NDLEVL)     
       INTEGER    ICFA(NDLEVL)
       INTEGER    IBREFA(NDLEVL)  , IFREFA(NDLEVL)
       INTEGER    IIA(NDTRAN)     , IIPA(NDTRAN)
       INTEGER    ITIA(NDTRAN)    , ITCA(NDTRAN)
       INTEGER    IPTYP(10)
       INTEGER    LPA(NDFREE)     , ISPA(NDFREE)
C-----------------------------------------------------------------------
       REAL*8     BWNO(10)       
       REAL*8     GAMSSA(NDTIN,NDTRAN)
       REAL*8     ENERA(NDLEVL)   , WTA(NDLEVL)
       REAL*8     AA(NDTRAN)
C-----------------------------------------------------------------------
       REAL*8     GAMA(NDTIN)    , TEA(NDTIN)
       REAL*8     QII(NDTIN)     , QIIP(NDTIN)
       REAL*8     FA(NDTRAN)     , AUGA(NDTRAN)
       REAL*8     EPSIL(15)
       REAL*8     DRA(NDTIN)
       REAL*8     BENER(10)
       REAL*8     EPS(20)        , OMEG(20)
       REAL*8     XJP(NDFREE)    , EFREE(NDFREE)
C-----------------------------------------------------------------------
       CHARACTER  STRG6(30)*19
       CHARACTER  ORBSYM(15)*1   , CHARA(15)*1    , ORBAR(15)*4
       CHARACTER  LCHRA(10)*1
C-----------------------------------------------------------------------
        LOGICAL   LPAPER   
C-----------------------------------------------------------------------
      DATA SLINE/'
     &
     &             '/
       DATA BLANKS/'
     &
     &             '/

       DATA NORB/15/
       DATA ORBAR/' 1 0',' 2 0',' 2 1',' 3 0',' 3 1',' 3 2',
     & ' 4 0',' 4 1',' 4 2',' 4 3',' 5 0',' 5 1',' 5 2',' 5 3',
     & ' 5 4'/
       DATA ORBSYM/'1','2','3','4','5','6','7','8','9','A','B',
     & 'C','D','E','F'/
       DATA LCHRA /'S','P','D','F','G','H','I','J','K','L'/
C-----------------------------------------------------------------------
       
       CALL XXELEM(IZ0,ENAME)

       IF (LPAPER) THEN
          WRITE(IWRP,1010) ENAME,IZ,IZ0,IZ1,NELEC
          WRITE(IWRP,1008)
       ENDIF
       
       NP        = 1
       IPTYP(1)  = 1
       IPOINT(1) = 1
       IF (LPAPER) THEN
          WRITE(IWRP,1011)ICF(1),IPOINT(1),
     &                    ((NLSETA(1,K,J),J=1,2),K=1,NELEC)
       ENDIF
       
       DO ICONF = 2,NCONFG
         DO IP = 1,NP
           ICONF1 = IPTYP(IP)
           NEQUIV = 0
           DO K = 1,NELEC-1
             IF (NLSETA(ICONF,K,1).EQ.NLSETA(ICONF1,K,1).AND.
     &           NLSETA(ICONF,K,2).EQ.NLSETA(ICONF1,K,2)) THEN
                 NEQUIV = NEQUIV
             ELSE
                 NEQUIV = NEQUIV + 1
             ENDIF
           END DO
           IF (NEQUIV.EQ.0) GO TO 59
         END DO
         NP = NP + 1
         IPTYP(NP) = ICONF
   59    IPOINT(ICONF) = IP
         IF (LPAPER) THEN
            WRITE(IWRP,1011)ICF(ICONF),IPOINT(ICONF),
     &                       ((NLSETA(ICONF,K,J), J=1,2),K=1,NELEC)
         ENDIF
       END DO
       
       IF (LPAPER) WRITE(IWRP,1023)NP
       
       NMET   = 0
       BWNMIN = BWNO(1)
       DO IP =1,NP
         IF (LPAPER) THEN
            WRITE(IWRP,1024)IP,IPTYP(IP),BWNO(IP),
     &                 ((NLSETA(ICF(IPTYP(IP)),K,J),J=1,2),K=1,NELEC-1)
         ENDIF
         BWNMIN = MIN(BWNMIN,BWNO(IP))
         BENER(IP) = BWNO(IP)/109737.3
       END DO


       IF (NFREE.LE.0) THEN
           write(IWR04,1049)ename,IZ,IZ0,IZ1,BWNMIN
       ELSE
           STRING=BLANKS(1:101)
           WRITE(STRING,1083)(BWNMIN+109737.26*(EFREE(KP)-
     &                        EFREE(1)),ISPA(KP),LCHRA(LPA(KP)+1),
     &                        XJP(KP),KP=1,min(NDP,NFREE))
           call xxslen(string,ind1,ind2)
           write(IWR04,1084)ename,IZ,IZ0,IZ1,STRING(1:ind2)
       ENDIF


       DO ICONFG = 1,NCONFG
         WRITE(LGLINE,1009)ICF(ICONFG),IPOINT(ICONFG),
     &                     ((NLSETA(ICONFG,K,J),J=1,2),K=1,NELEC)
         READ(LGLINE,1057)CONFIG
         DO IORB = 1,NORB
           KA(IORB) = 0
         END DO 
         I = 0
    3    IF (CONFIG(6*I+1:6*I+6).NE.'      ') THEN
             ORBITL = CONFIG(6*I+1:6*I+4)
             DO IORB = 1,NORB
               IF (ORBITL.EQ.ORBAR(IORB)) THEN
                   KA(IORB)=KA(IORB)+1
               ENDIF
             END DO
             I = I + 1
             GO TO 3
         ENDIF
         NSHEL=0
         DO IORB=1,NORB
           IF (KA(IORB).NE.0) THEN
               NSHEL = NSHEL + 1
               JA(NSHEL) = KA(IORB)
               IF (NSHEL.NE.1) JA(NSHEL) = JA(NSHEL) + 50
               CHARA(NSHEL) = ORBSYM(IORB)
           ENDIF
         END DO
         WRITE(STRG6(ICONFG),1058)(JA(IS),CHARA(IS),IS=1,NSHEL)
       END DO



       IF (IOPT.EQ.0) ICFSEL=4
       IF (IOPT.EQ.1) ICFSEL=3

       DO ILEVL = 1,NLEVL
         IF (IBREFA(IA(ILEVL)).GT.0)THEN
            CALL XXCFTR(ICFSEL,STRG6(ICFA(ILEVL)),STRGO)
            write(IWR04,1059)IBREFA(IA(ILEVL)),STRGO,
     &                       IABS(ISA(ILEVL)),ILA(ILEVL),WTA(ILEVL),
     &                       109737.3*ENERA(ILEVL)
         ENDIF
       END DO
C--------------------------------------------------------------
C   OUTPUT EPSILON TO COPDT
C--------------------------------------------------------------
          EPSILINE = BLANKS
          WRITE(EPSILINE,1061) (EPSIL(N),N=1,EPSILMAX)
          CALL XXSLEN(EPSILINE,IND1,IND2)
          WRITE(IWR04,1000) EPSILINE(1:IND2)

C-----------------------------------------------------------------------
C  ESTABLISH TEMPERATURE LIST LINE FOR COPDAT FILE
C-----------------------------------------------------------------------
       SLINE = BLANKS
       Z1 = FLOAT(IZ1)
       WRITE(LLINE,1053)Z1,3,(TEA(IT),IT=1,MAXT)
       SLINE(1:16) = LLINE(2:17)
       ISM = 17
       ILM = 19
       DO  IT=1,MAXT
         SLINE(ISM:ISM+7) = LLINE(ILM:ILM+4)//LLINE(ILM+6:ILM+8)
         ISM = ISM + 8
         ILM = ILM + 9
       END DO
       CALL XXSLEN(SLINE,IND1,IND2)
       WRITE(IWR04,1000)SLINE(1:IND2)


C-----------------------------------------------------------------------
C  EVALUATE EXCITATION AND DEEXCITATION RATE USING IMPACT PARAMETER
C  CODE IPRATE
C  IPRATE REQUIRES M=0 (IE. NO SEPARATE COLLISION STRENGTH INPUT DATA)
C                  N=1 (IE. ONLY ONE TEMPERATURE)
C                  TEA(1) (TEMPERATURE IN  K)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------         
C  CROSS-SECTION AND RATE COEFFICIENT SECTION
C-----------------------------------------------------------------------

       DO 100 ITRAN = 1,NTRAN

         II     = IIA(ITRAN)
         IIP    = IIPA(ITRAN)
         WII    = 2.0*WTA(II)  + 1.0
         WIIP   = 2.0*WTA(IIP) + 1.0
         ICFII  = ICFA(II)
         NII    = NLSETA(ICFII,2,1)
         ICFIIP = ICFA(IIP)
         NIIP   = NLSETA(ICFIIP,2,1)
         IP     = MAX(IPOINT(ICFII),IPOINT(ICFIIP))
         EII    = BENER(IP) - ENERA(II)
         EIIP   = BENER(IP) - ENERA(IIP)
         F      = FA(ITRAN)
         A      = ABS(AA(ITRAN))
         M      = 0
         N      = MAXT

         IF (EII-EIIP.GE.1.0D-6) THEN

            CALL GXIMPR(IZ,WII,EII,WIIP,EIIP,M,A,EPS,OMEG,N,
     &                  TEA,RAT,QII,QIIP,GAMA)

            DO IT = 1,MAXT
              ATE = 1.57910D5/TEA(IT)
              GAMA(IT) = 4.60488D7 * WIIP * QIIP(IT) / DSQRT(ATE)
              GAMSSA(IT,ITRAN) = GAMA(IT)
            END DO
            SLINE = BLANKS
            WRITE(LLINE,1052)IBREFA(IIP),IBREFA(II),
     &                       A,(GAMA(IT),IT=1,MAXT)
     
            SLINE = BLANKS(1:80)
            SLINE(1:8) = LLINE(1:8)
            ISM = 9
            ILM = 9
            DO IT= 1,MAXT+1
              SLINE(ISM:ISM+7)=LLINE(ILM:ILM+4)//LLINE(ILM+6:ILM+8)
              ISM = ISM + 8
              ILM = ILM + 9
            END DO
            CALL XXSLEN(SLINE,IND1,IND2)
            WRITE(IWR04,1000)SLINE(1:IND2)
            
         ENDIF
         
  100  CONTINUE
  
  
C-----------------------------------------------------------------------
C  AUGER AND DIELECTRONIC RECOMBINATION SECTION
C-----------------------------------------------------------------------

       DO 150 ITRAN=1,NAUTO
       
         WT = 2.0D0*WTA(IXREFA(ITIA(ITRAN)))+1.0D0
         WP = 2.0D0*XJP(IFREFA(IXREFA(ITCA(ITRAN))))+1.0D0
         
         DO IT = 1,MAXT
           DRA(IT) = 4.0D0*(WT/WP)*
     &               (3.14159*2.8003D-17*2.18D-18/
     &               (1.38D-23*TEA(IT)))**1.5D0*AUGA(ITRAN)*
     &               DEXP(-157890.0*(ENERA(IXREFA(ITIA(ITRAN)))-
     &               ENERA(IXREFA(ITCA(ITRAN))))/TEA(IT))
           IF (DRA(IT).LE.1.0D-78) DRA(IT) = 1.0D-78
         END DO
         
         WRITE(LLINE,1062)'R',IBREFA(IXREFA(ITIA(ITRAN))),
     &                    '+',IFREFA(IXREFA(ITCA(ITRAN))),
     &                    AUGA(ITRAN),(DRA(IT),IT=1,MAXT)
         SLINE=BLANKS
         SLINE(1:8)=LLINE(1:8)
         ISM = 9
         ILM = 9
         DO IT= 1,MAXT+1
           SLINE(ISM:ISM+7)=LLINE(ILM:ILM+4)//LLINE(ILM+6:ILM+8)
           ISM=ISM+8
           ILM=ILM+9
         END DO
         
         CALL XXSLEN(SLINE,IND1,IND2)
         WRITE(IWR04,1000)SLINE(1:IND2)
         
  150  CONTINUE

C-----------------------------------------------------------------------
       write(IWR04,1056)

       write(IWR04,1082)DATE

C-----------------------------------------------------------------------
 1000  FORMAT(A)
 1008  FORMAT(1x,'CONFIG.   NPOINT     N,L FOR OPEN SHELLS')
 1009  FORMAT(1H ,I4,5X,I4,5X,17(2I2,2X))
 1010  FORMAT(1x,1A2,'+',I2,8X,I2,8X,I2,'  NELEC = ',I2)
 1011  FORMAT(1H ,I4,5X,I4,7X,17(2I2,2X))
 1023  FORMAT(1x,'NP=',I5,'      IP    IPTYP(IP)   BWNO(IP)        ',
     &            'nl PARENT')
 1024  FORMAT(1H ,14X,I2,7X,I2,F15.1,7X,17(2I2,2X))
 1049  FORMAT(1A2,'+',I2,8X,I2,8X,I2,F15.1)
 1052  FORMAT(I4,I4,1P,15D9.2)
 1053  FORMAT(1x,F5.2,4X,I1,7X,1P,15D9.2)
 1056  FORMAT(1H ,' -1'/1H ,' -1  -1')
 1057  FORMAT(19X,1A242)
 1058  FORMAT(21(I2,A1))
 1059  FORMAT(2X,I3,1A19,'(',I1,')',I1,'(',F4.1,')',4X,F13.1)
 1061  FORMAT(1H ,'  -1   ',15(1H ,F7.2))
 1062  FORMAT(1A1,I3,2X,1A1,I1,1P,15D9.2)
 1082  FORMAT('C',79('-')/
     &        'C  Specific ion file generated from AUTOSTRUCTURE list fi
     &le'/
     &        'C'/
     &        'C  Program  : ADAS703'/
     &        'C  Producer : who is responsible for this?',/
     &        'C  Date     : ',1A8/
     &        'C',79('-'))
 1083  FORMAT(1X,5(F11.1,'(',I1,1A1,F4.1,')',1X))
 1084  FORMAT(1A2,'+',I2,8X,I2,8X,I2,A)
C-----------------------------------------------------------------------
       END
