C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3ispf.for,v 1.1 2004/07/06 13:56:53 whitefor Exp $ Date $Date: 2004/07/06 13:56:53 $
C
      SUBROUTINE G3ISPF( LPEND  ,
     &                   NDP    , NP     , IPSTR ,
     &                   BWNO   , 
     &                   IOPT   ,
     &                   NDTIN  , TVALS  , 
     &                   TITLE  , 
     &                   ITTYP  , ITVAL  , TIN )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G3ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS703
C
C  SUBROUTINE:
C
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURES ALLOWED.
C  INPUT : (I*4)   NDP      = MAX. NUMBER OF PARENTS ALLOWED.
C  INPUT : (I*4)   NP       = NUMBER OF PARENTS.
C
C  INPUT : (R*8)   TVALS(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C
C  OUTPUT: (C*40)  TITLE    = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   IOPT     = 1 => OUTPUT ENERGY LEVELS IN EISNER FORM
C                           = 2 => NORMAL OUTPUT
C
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (C*40)  IPDSTR() = STRING OF PARENT CONFIGURATION.
C  OUTPUT: (R*8)   BWNO()   = IONISATION POT. OF PARENTS.
C  OUTPUT: (I*4)   ITVAL    = NUMBER OF INPUT TEMP/DENSITY PAIRS (1->14)
C  OUTPUT: (R*8)   TIN()    = USER ENTERED VALUES -
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)   I	    = LOOP COUNTER
C          (I*4)   J        = LOOP COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH    IDL-ADAS	CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR   : Martin O'Mullane
C
C DATE     : 29/04/98
C
C VERSION  : 1.1                          DATE: 29-04-98
C MODIFIED : Martin O'Mullane
C            FIRST VERSION
C
C-----------------------------------------------------------------------
      INTEGER    PIPEIN        , PIPEOU   
C-----------------------------------------------------------------------
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
      INTEGER    ITTYP         , NDTIN    , ITVAL
      INTEGER    IOPT
      INTEGER    NDP           , NP  
      INTEGER    I             , J        , ILOGIC
C-----------------------------------------------------------------------
      LOGICAL    LPEND  
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40      
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)    , TVALS(NDTIN,3)         
      REAL*8     BWNO(NDP)
C-----------------------------------------------------------------------
      CHARACTER  IPSTR(NDP)*9  
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ

	WRITE(PIPEOU,*) NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDP
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NP
	CALL XXFLSH(PIPEOU)

         DO J=1, 3
            DO I=1, NDTIN
               WRITE(PIPEOU,'(E12.4)') TVALS(I,J)
               CALL XXFLSH(PIPEOU)
            END DO
         END DO

         DO I=1, NDP
            WRITE(PIPEOU,'(A9)') IPSTR(I)
            CALL XXFLSH(PIPEOU)
         END DO

C
C  READ OUTPUTS FROM PIPE FROM IDL
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF

	READ(PIPEIN,'(A40)') TITLE

	READ(PIPEIN,*) (BWNO(I), I=1,NDP)

	READ(PIPEIN,*) IOPT

	READ(PIPEIN,*) ITTYP
	READ(PIPEIN,*) ITVAL
	READ(PIPEIN,*) (TIN(I), I=1,NDTIN)


C-----------------------------------------------------------------------

	RETURN
	END
