C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3spf1.for,v 1.1 2004/07/06 13:57:05 whitefor Exp $ Date $Date: 2004/07/06 13:57:05 $
C
      SUBROUTINE G3SPF1( LPASS  , LPAPER ,
     &                   DSNPAS , DSNPAP , 
     &                   LPEND  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G1SPF1 *********************
C
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL OR
C	    THE INFO FILE (WHICH HAS BEEN REDIRECTED TO STANDARD INPUT).
C
C  CALLING PROGRAM: ADAS211
C
C  SUBROUTINE:
C
C  OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT TEXT TO 'PAPER.TXT'
C                                        FILE.
C                             .FALSE. => NO TEXT OUTPUT
C  OUTPUT: (L*4)   LPASS    = .TRUE.  => OUTPUT DATA TO ADF04 PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        ADF04 PASSING FILE.
C
C  OUTPUT: (C*80)  DSNPAS   = OUTPUT PASSING ADF04 NAME
C  OUTPUT: (C*80)  DSNPAP   = OUTPUT TEXT FILE NAME
C
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => USER SELECTED 'CANCEL'
C                             .FALSE. => USER DID NOT
C
C          (I*4)   PIPEIN   = STANDARD INPUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C DATE:    29/04/98
C
C VERSION: 1.1                          DATE: 29-04-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      INTEGER      PIPEIN, ILOGIC, ONE
      PARAMETER   (PIPEIN = 5 , ONE = 1 )
C-----------------------------------------------------------------------
      CHARACTER    DSNPAS*80 , DSNPAP*80 
C-----------------------------------------------------------------------
      LOGICAL      LPAPER  , LPASS , LPEND      
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
         LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF

C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
 
      IF(.NOT.LPEND)THEN
         READ(PIPEIN,'(A)')DSNPAS
         READ(PIPEIN,'(A)')DSNPAP
         READ(PIPEIN,*)ILOGIC
         IF(ILOGIC.EQ.0)THEN
            LPASS=.FALSE.
         ELSE
            LPASS=.TRUE.
         ENDIF
         READ(PIPEIN,*)ILOGIC
         IF(ILOGIC.EQ.0)THEN
            LPAPER=.FALSE.
         ELSE
            LPAPER=.TRUE.
         ENDIF
      ELSE
         LPEND = .TRUE.
      ENDIF

      RETURN
      END
