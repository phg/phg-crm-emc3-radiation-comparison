C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3dat2.for,v 1.2 2004/07/06 13:56:51 whitefor Exp $ Date $Date: 2004/07/06 13:56:51 $
C
       SUBROUTINE G3DAT2(IUNT   , IZ0    , 
     &                   NELEC  ,  
     &                   NCONFG , ICF    , NLSETA   ,
     &                   NEPSIL , EPSIL  , EPSILMAX ,
     &                   NLEVL  , NBOUND , NFREE    , IBREFA ,
     &                   IA     , ISA    , ILA      ,
     &                   IJA    , ICFA   , EFREE    ,
     &                   ENERA  , WTA    , IXREFA   , IFREFA ,
     &                   LPA    , ISPA   , XJP      ,
     &                   NTRAN  , IIPA   , IIA      , AA     , FA )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: G3DAT1 ********************
C
C  VERSION:  1.0
C
C  PURPOSE: READS GENERAL AUTOSTRUCTURE OUTPUT FILE - olg. THE SUBROUTINE IS
C           INDEPENDENT OF COUPLING AND FORMATING OF FILES.
C
C  INPUT:        (I*4) IUNT      = INPUT UNIT 
C                (I*4) IZ0       = NUCLEAR CHARGE
C
C  OUTPUT:       (I*4) NELEC     = NUMBER OF ELECTRONS 
C                (I*4) IA()      = LEVL INDEX
C                (I*4) ILA()     = TOTAL ORBITAL QUANTUM NUMBER
C                (I*4) ISA()     = TOTAL SPIN    QUANTUM NUMBER (2*S+1)
C                (I*4) IJA()     = TOTAL ANGUL.  QUANTUM NUMBER (2*J)
C                (I*4) ICFA()    = CONFIGRATION INDEX OF LEVL
C                (I*4) ENERA()   = LEVEL ENERGY (CM-1)
C                (I*4) WTA()     = (LEVL STATISTICAL WEIGHT-1)/2
C                (I*4) NLEVL     = NUMBER OF LEVLS
C                (I*4) IIA()     = LOWER LEVL INDEX OF TRANSITION
C                (I*4) IIPA()    = UPPER LEVL INDEX OF TRANSITION
C                (I*4  AA()      = A-VALUE FOR TRANSITION
C                (I*4  NTRAN     = NUMBER OF TRANSITIONS
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C  AUTHOR  : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 1/05/1998
C MODIFIED : Martin O'Mullane  
C             - First version.
C
C VERSION  : 1.2                          DATE: 06/12/2002
C MODIFIED : Martin O'Mullane  
C             - The olg AUTOSTRUCTURE file has changed. Modify strg4
C               and format statements 1004 and 1006 to reflect the
C               v16.10 format.
C
C
C-----------------------------------------------------------------------
       INTEGER    NDLEVL     ,  NDTRAN     
       INTEGER    NDFREE
C-----------------------------------------------------------------------
       PARAMETER( NDLEVL=300 ,  NDTRAN=9000)
       PARAMETER( NDFREE=100 )
C-----------------------------------------------------------------------
       INTEGER    I          , IJT        , IJTMAX    ,
     &            INDX       , IST        , ISTMAX    ,
     &            ILVL       , 
     &            IZ0        , IZREAD     ,
     &            J          , K          , LF        ,
     &            LT         , LTMAX      , N         ,
     &            NBOUND     , NCONFG     , NELEC     ,
     &            NEPSIL     , NFREE      , NLEVL     , NTRAN
       INTEGER    iunt       , nval       , i4unit
C-----------------------------------------------------------------------
       REAL*8     EPSILMAX   
C-----------------------------------------------------------------------
       CHARACTER  STRG0*4    , STRG1*12   , STRG2*9   , STRG3*9   ,
     &            STRG4*15   , STRG7*9    , STRG8*10  ,
     &            STRG9*5    , STRG10*6   , STRG11*9  , STRG12*8
       CHARACTER  SLINE*133  
       CHARACTER  BLANKS*133 , LINE*133   , NXLINE*133
       CHARACTER  LGLINE*266
C-----------------------------------------------------------------------
       LOGICAL    LPASS
C-----------------------------------------------------------------------
       INTEGER    ICF(30)        , NLSETA(30,30,2)
       INTEGER    IA(NDLEVL)     , IXREFA(NDLEVL)
       INTEGER    ISA(NDLEVL)    , ILA(NDLEVL)        , IJA(NDLEVL)
       INTEGER    ICFA(NDLEVL)
       INTEGER    IBREFA(NDLEVL) , IFREFA(NDLEVL)
       INTEGER    IIA(NDTRAN)    , IIPA(NDTRAN)
       INTEGER    LPA(NDFREE)    , ISPA(NDFREE)
C-----------------------------------------------------------------------
       REAL*8     ENERA(NDLEVL)  , WTA(NDLEVL)
       REAL*8     AA(NDTRAN)
C-----------------------------------------------------------------------
       REAL*8     FA(NDTRAN)     , WVLA(NDTRAN)    
       REAL*8     EPSIL(15)
       REAL*8     XJP(NDFREE)    , EFREE(NDFREE)
C-----------------------------------------------------------------------
       DATA SLINE/'
     &
     &             '/
       DATA BLANKS/'
     &
     &             '/
       DATA STRG0             /'S.S.'/
       DATA STRG1,STRG2,STRG3 /'ELECTRONS IN',' CONFIGUR','(EK-E1)/R'/
       DATA STRG4,STRG7       /'E1-DATA   K  KP','SLATER-ST'/
       DATA STRG8,STRG9,STRG10/'EPSILON/RY','     ','ATOMIC'/
       DATA STRG11,STRG12     /' LIST OF ','AUTO-ION'/
       DATA LPASS/.FALSE./
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C  STRIP TITLE DATA FROM SUPERSTRUCTURE FILE
C-----------------------------------------------------------------------
       NCONFG=0
   10  READ(iunt,1000,end=50)LINE

       IF (INDEX(LINE,STRG0).GT.0) THEN

          
           READ(iunt,1000)(LINE, I=1,12)   ! skip 12 lines
           INDX=INDEX(LINE,STRG1)
           IF (INDX.GT.0) THEN
              READ(LINE(INDX-5:INDX-1),'(I4)') NELEC
           ELSE
              WRITE(I4UNIT(-1),2001) 
     &              'INPUT FILE NOT IN AUTOSTRUCTURE FORMAT'
              WRITE(I4UNIT(-1),2002)
              STOP
           ENDIF


       ELSEIF (STRG2.EQ.LINE(1:9))THEN
C-----------------------------------------------------------------------
C  STRIP CONFIGURATION DATA FROM SUPERSTRUCTURE FILE
C-----------------------------------------------------------------------
          NCONFG=NCONFG+1
          READ(iunt,1000)NXLINE
          IF(STRG7.NE.NXLINE(2:10)) THEN
              LGLINE=LINE(1:130)//NXLINE(3:130)
              ELSE
              LGLINE=LINE
          ENDIF
          READ(LGLINE,1002)ICF(NCONFG),((NLSETA(NCONFG,K,J),J=1,2),
     &    K=1,NELEC)

       ELSE IF(STRG10.EQ.LINE(78:83))THEN
          READ(LINE,1013) IZREAD
          IF (IZREAD.NE.IZ0) THEN
              WRITE(I4UNIT(-1),2001) 
     &              'RATE FILE NOT COMPATIBLE WITH GENERAL OUTPUT FILE'
              WRITE(I4UNIT(-1),2002)
              STOP
          ENDIF
       ELSE IF(STRG8.EQ.LINE(32:41)) THEN
          NEPSIL=0
   15     READ(iunt,1000) NXLINE
          IF (STRG9.NE.NXLINE(10:14)) THEN
              NEPSIL=NEPSIL+1
              READ(NXLINE,1060) nval,EPSIL(NEPSIL)
              if (nval.lt.0) nepsil=nepsil-1
              GOTO 15
          ENDIF
          EPSILMAX=NEPSIL
          DO  N=1,EPSILMAX
            EPSIL(N)=ABS(EPSIL(N))
          END DO


       ELSE IF ((STRG3.EQ.LINE(76:84)).AND.(.NOT.LPASS)) THEN
C-----------------------------------------------------------------------
C  STRIP ATTRIBUTES AND LEVL ENERGY DATA FROM SUPERSTRUCTURE FILE
C-----------------------------------------------------------------------
          NLEVL=0
          NBOUND=0
          NFREE=0
          LPASS = .TRUE.
   20     READ(iunt,1000)LINE
          IF (STRG11.NE.LINE(1:9)) THEN
              READ(LINE,1003)IA(NLEVL+1),ILVL,
     &                       ISA(NLEVL+1),ILA(NLEVL+1),
     &                       IJA(NLEVL+1),ICFA(NLEVL+1),ENERA(NLEVL+1)
              WTA(NLEVL+1)=DFLOAT(IJA(NLEVL+1))/2.0
              IF(IA(NLEVL+1).GT.0)THEN
                 NLEVL=NLEVL+1
                 IXREFA(ILVL)=IA(NLEVL)
                 IF (NLEVL.GT.NDLEVL) THEN
                     WRITE(I4UNIT(-1),2001)
     &                     'TOO MANY LEVELS',NLEVL,NDLEVL
                     WRITE(I4UNIT(-1),2002)
                     STOP
                 ENDIF
                 IF(ICFA(NLEVL).GT.0) THEN
                     NBOUND=NBOUND+1
                     IBREFA(NLEVL)=NBOUND
                 ELSE
                     LF=NLSETA(IABS(ICFA(NLEVL)),NELEC,2)
                     LT=ILA(NLEVL)
                     IST=ISA(NLEVL)
                     IJT=IJA(NLEVL)
                     IF (NFREE.EQ.0) THEN
                         NFREE=NFREE+1
                         EFREE(NFREE)=ENERA(NLEVL)
                         IFREFA(NLEVL)=NFREE
                         LTMAX=LT
                         ISTMAX=IST
                         IJTMAX=IJT
                         LPA(NFREE)=LTMAX-LF
                         ISPA(NFREE)=ISTMAX-1
                         XJP(NFREE)=0.5D0*(IJTMAX-2.0D0*LF-1.0)
C
                     ELSEIF (ENERA(NLEVL).EQ.EFREE(NFREE)) THEN
                         IFREFA(NLEVL)=NFREE
                         IF (LT.GT.LTMAX) THEN
                             LTMAX=LT
                             LPA(NFREE)=LTMAX-LF
                         ENDIF
                         IF (IST.GT.ISTMAX) THEN
                             ISTMAX=IST
                             ISPA(NFREE)=ISTMAX-1
                         ENDIF
                         IF (IJT.GT.IJTMAX) THEN
                             IJTMAX=IJT
                             XJP(NFREE)=0.5D0*(IJTMAX-2.0D0*LF-1.0)
                         ENDIF
C
                     ELSEIF (ENERA(NLEVL).NE.EFREE(NFREE)) THEN
                         NFREE=NFREE+1
                         EFREE(NFREE)=ENERA(NLEVL)
                         IFREFA(NLEVL)=NFREE
                         LTMAX=LT
                         ISTMAX=IST
                         IJTMAX=IJT
                         LPA(NFREE)=LTMAX-LF
                         ISPA(NFREE)=ISTMAX-1
                         XJP(NFREE)=0.5D0*(IJTMAX-2.0D0*LF-1.0)
C
                     ENDIF
                 ENDIF
                 GO TO 20
              ENDIF
          ENDIF
          

       ELSEIF (STRG4.EQ.LINE(8:22)) THEN
C-----------------------------------------------------------------------
C  STRIP TRANSITIONS AND A-VALUES FROM SUPERSTRUCTURE FILE
C-----------------------------------------------------------------------
          NTRAN=0
   30     READ(iunt,1000)LINE
          IF(LINE(109:111).EQ.'***')THEN
              READ(LINE,1006)IIPA(NTRAN+1),IIA(NTRAN+1),AA(NTRAN+1),
     &                       FA(NTRAN+1)
              WVLA(NTRAN+1)=0.0
          ELSE
              READ(LINE,1004)IIPA(NTRAN+1),IIA(NTRAN+1),AA(NTRAN+1),
     &                       FA(NTRAN+1),WVLA(NTRAN+1)
          ENDIF
          IF(IIA(NTRAN+1).GT.0)THEN
             NTRAN=NTRAN+1
             IF(NTRAN+1.GT.NDTRAN)THEN
              WRITE(I4UNIT(-1),2001) 
     &              'TOO MANY TRANSITION',NTRAN,NDTRAN
              WRITE(I4UNIT(-1),2002)
              STOP
             ENDIF
             GO TO 30
          ENDIF
       ENDIF

       GO TO 10
       
   50  CONTINUE

C-----------------------------------------------------------------------
       
       RETURN

C-----------------------------------------------------------------------
 1000  FORMAT(1A133)
 1002  FORMAT(18X,I3,38X,33(2I2,2X))
 1003  FORMAT(7X,I4,6X,I4,25X,4I5,F19.6)
 1004  FORMAT(14x,i4,i4,5x,e15.3,36x,f10.5,12x,f11.4)
 1006  FORMAT(14x,i4,i4,5x,e15.3,36x,f10.5)
 1013  FORMAT(91X,I2)
 1060  FORMAT(5x,i3,23X,1F11.5)

 2001 FORMAT(1X,32('*'),' G3DAT2 ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,i4,2x,i4)
 2002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C-----------------------------------------------------------------------
       END
