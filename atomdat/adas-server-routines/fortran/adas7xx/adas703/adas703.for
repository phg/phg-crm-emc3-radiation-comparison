C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/adas703.for,v 1.1 2004/07/06 11:04:05 whitefor Exp $ Date $Date: 2004/07/06 11:04:05 $
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS703 **********************
C
C  VERSION: 1.0
C
C  PURPOSE: TO PRODUCE ADF04 FILES FOR DOUBLY EXCITED LEVELS FROM 
C           AUTOSTRUCTURE OUTPUT. PRINTS TO THE ADF04 OUTPUT FILE
C
C            o LEVL LIST, ATTRIBUTES AND ENERGIES IN ADF04 FORM
C            o ORBITAL ENERGY LIST ON LEVEL INDICATOR CARD OF LEVL LIST
C            o LEVL INDICES, A-VALUES AND GAMMMAS OF DIPOLE ALLOWED
C
C            GAMMAS ARE CALCULATED IN THE IMPACT PARAMETER APPROXIMATION
C
C  DATA:   AUTOSTRUCTURE OUTPUT FILES olg AND oic (OR ols/oicu/olsu)
C
C  PROGRAM:
C
C          (I*4) PIPEIN    = STANDARD INPUT
C          (I*4) PIPEOU    = STANDARD OUTPUT
C
C
C          INPUT/OUTPUT STREAM ALLOCATIONS:
C          --------------------------------
C
C          STREAM  9: INPUT  - AUTOSTRUCTURE GENERAL OUTPUT FILE
C          ('IUNT09')
C
C          STREAM 10: INPUT  - AUTOSTRUCTURE RATE OUTPUT FILE
C          ('IUNT10')
C
C          STREAM 17: OUTPUT - MAIN OUTPUT PAPER.TXT
C          ('IUNT17')
C
C          STREAM 18: OUTPUT - PASSING ADF04 OUTPUT FILE
C          ('IUNT18')          
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          G3SPF0     ADAS      GATHERS INPUT FILE NAMES FROM IDL  
C          G3SPF1     ADAS      GATHERS OUTPUT FILE NAMES FROM IDL 
C          G3DAT1     ADAS      READS AUTOSTRUCTURE GENERAL OUTPUT FILE 
C          G3DAT2     ADAS      READS AUTOSTRUCTURE RATE OUTPUT FILE 
C          G3CHIP     ADAS      MAKES STRING FROM PARENT NL VALUES 
C          G3ISPF     ADAS      GATHERS USERS VALUES FOR PROGRAM OPTIONS
C          G3ASTJ     ADAS      CALCULATES EXCITATION RATES AND WRITES 
C                               ADF04 FILES
C          XXSLEN     ADAS      GET LENGTH OF STRING VARIABLE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXTCON     ADAS      CONVERTS ENTERED TEMPS. TO EV/RED/KELVIN
C          XXDATE     ADAS      OBTAINS DATE FROM IDL
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C  AUTHOR  : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C  NOTES   : Based on JETSHP.STRIP.FORT(SASJ91)
C            H P Summers  - 05/08/96
C
C VERSION  : 1.1                          DATE: 28/04/1998
C MODIFIED : Martin O'Mullane  
C            First version.
C
C-----------------------------------------------------------------------
       INTEGER    IUNT10       , IUNT09       , IUNT17      , IUNT18   ,
     &            PIPEIN       , PIPEOU     
       INTEGER    NDLEVL       , NDTRAN       , NDTIN
       INTEGER    NDFREE       , NDP          , NDCFG       , NDPRNT 
C-----------------------------------------------------------------------
       PARAMETER( NDLEVL = 300 , NDTRAN = 9000 , NDTIN = 14 )
       PARAMETER( NDFREE = 100 , NDP   = 5     , NDCFG = 30 )
       PARAMETER( NDPRNT = 10  )
       PARAMETER( IUNT09 = 09  , IUNT10 = 10   , IUNT17 = 17 )
       PARAMETER( IUNT18 = 18  )
       PARAMETER( PIPEIN = 5   , PIPEOU = 6    )
C-----------------------------------------------------------------------
       INTEGER    NBOUND        , NCONFG       , NELEC       ,
     &            NEPSIL        , NFREE        , NLEVL       , NTRAN
       INTEGER    L1            , L2 
       INTEGER    I 
       INTEGER    IZ0           , IZ           ,  IZ1 
       INTEGER    NUMA
       INTEGER    I4UNIT
       INTEGER    ISTOP         , IFORM        , ITTYP       , ITVAL 
       INTEGER    IOPT
       INTEGER    NP
C-----------------------------------------------------------------------
       INTEGER    ICF(NDCFG)      , NLSETA(NDCFG,NDCFG,2)
       INTEGER    IA(NDLEVL)      , IXREFA(NDLEVL)
       INTEGER    ISA(NDLEVL)     , ILA(NDLEVL)        , IJA(NDLEVL)
       INTEGER    ICFA(NDLEVL)
       INTEGER    IBREFA(NDLEVL)  , IFREFA(NDLEVL)
       INTEGER    IIA(NDTRAN)     , IIPA(NDTRAN)
       INTEGER    LPA(NDFREE)     , ISPA(NDFREE)
       INTEGER    ITIA(NDTRAN)    , ITCA(NDTRAN) 
C-----------------------------------------------------------------------
       REAL*8     EPSILMAX
C-----------------------------------------------------------------------
       REAL*8     ENERA(NDLEVL)   , WTA(NDLEVL)
       REAL*8     AA(NDTRAN)
       REAL*8     FA(NDTRAN)      
       REAL*8     EPSIL(15)
       REAL*8     XJP(NDFREE)     , EFREE(NDFREE)
       REAL*8     AUGA(NDTRAN)
       REAL*8     BWNO(NDPRNT)
       REAL*8     TIN(NDTIN)      , TKEL(NDTIN)        
       REAL*8     TETA(NDTIN)     , TVALS(NDTIN,3)      
C-----------------------------------------------------------------------
       CHARACTER  DSNIN1*80       , DSNIN2*80
       CHARACTER  DSNPAP*80       , DSNPAS*80           , DSN80*80
       CHARACTER  DATE*8          , REP*3               , CADAS*120
       CHARACTER  TITLE*40
C-----------------------------------------------------------------------
       CHARACTER IPSTR(NDP)*9
C-----------------------------------------------------------------------
       LOGICAL   OPEN09     , OPEN10      , OPEN17  , OPEN18
       LOGICAL   LPAPER     , LPASS
       LOGICAL   LPEND
C-----------------------------------------------------------------------
       DATA   OPEN09/.FALSE./ , OPEN10/.FALSE./ , OPEN17/.FALSE./
       DATA   OPEN18/.FALSE./
       DATA   REP   / 'NO' /  , CADAS / ' ' /
C-----------------------------------------------------------------------
C  Default reduced temperature set; take as 14 values 
C-----------------------------------------------------------------------
       DATA TETA/ 5.00D+02,1.00D+03,2.00D+03,5.00D+03,1.00D+04,2.00D+04,
     &            5.00D+04,1.00D+05,2.00D+05,5.00D+05,1.00D+06,2.00D+06,
     &            5.00D+06,1.00D+07 /
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

       CALL XX0000

C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

       CALL XXDATE( DATE )
     
C-----------------------------------------------------------------------
C IF FILES ARE ACTIVE CLOSE THE UNITS
C-----------------------------------------------------------------------
 
 100   CONTINUE
  
       IF (OPEN09) THEN
          CLOSE(IUNT09)
          OPEN09 = .FALSE.
       ENDIF
       IF (OPEN10) THEN
          CLOSE(IUNT10)
          OPEN10 = .FALSE.
       ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAMES (FROM IDL PIPE).
C-----------------------------------------------------------------------

       CALL G3SPF0( REP , DSNIN1, DSNIN2)

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE FILES ON UNITS 9 AND 10 AND END RUN
C FILE CLOSURE MOVED TO 9999 SO THAT MENU BUTTON ALSO CLOSES FILES
C-----------------------------------------------------------------------

       IF (REP.EQ.'YES') GOTO 9999

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILES - olg and oic
C-----------------------------------------------------------------------

      CALL XXSLEN(DSNIN1,L1,L2)
      OPEN( UNIT=IUNT09 , FILE=DSNIN1(L1:L2)  , STATUS='UNKNOWN' )
      OPEN09=.TRUE.
        
      CALL XXSLEN(DSNIN2,L1,L2)
      OPEN( UNIT=IUNT10 , FILE=DSNIN2(L1:L2)  , STATUS='UNKNOWN' )
      OPEN10=.TRUE.


C-----------------------------------------------------------------------
C FETCH DATA FROM oic (IUNT10) DATASET
C-----------------------------------------------------------------------

        CALL G3DAT1(IUNT10, IZ0, IZ, NUMA, ITIA, ITCA, AUGA)
        DO I=1,NUMA
          AUGA(I) = DABS(AUGA(I))
        END DO

C-----------------------------------------------------------------------
C FETCH DATA FROM olg (IUNT09) DATASET
C-----------------------------------------------------------------------

       CALL G3DAT2(IUNT09 , IZ0    , 
     &             NELEC  ,
     &             NCONFG , ICF    , NLSETA   ,
     &             NEPSIL , EPSIL  , EPSILMAX ,
     &             NLEVL  , NBOUND , NFREE    , IBREFA ,
     &             IA     , ISA    , ILA      ,
     &             IJA    , ICFA   , EFREE    ,
     &             ENERA  , WTA    , IXREFA   , IFREFA ,
     &             LPA    , ISPA   , XJP      ,
     &             NTRAN  , IIPA   , IIA      , AA     , FA )
     
     	IZ1 = IZ + 1
     
C-----------------------------------------------------------------------
C SET UP ARRAY 'IPSTR()' - CONTAINS CONFIGURATIONS FOR USER SUPPLIED 
C IONISATION POTENTIALS.
C-----------------------------------------------------------------------

      CALL G3CHIP( NCONFG , NELEC , NLSETA , ICF , IPSTR  , NP )
	

C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS INPUT ELECTRON TEMPS. IN 3 FORMS.
C    NOTE: DEFAULT SET OF TEMPERATURES 'TETA()' IS IN REDUCED UNITS.
C-----------------------------------------------------------------------

      DO IFORM=1,3
          CALL XXTCON( 3    , IFORM  , IZ1 , NDTIN , 
     &                 TETA , TVALS(1,IFORM) )
      END DO


C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES FROM IDL
C-----------------------------------------------------------------------

200   CONTINUE
      CALL G3ISPF( LPEND  ,
     &             NDP    , NP     , IPSTR ,
     &             BWNO   , 
     &             IOPT   , 
     &             NDTIN  , TVALS  , 
     &             TITLE  , 
     &             ITTYP  , ITVAL  , TIN )


C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN (TIN -> TKEL)
C-----------------------------------------------------------------------

      CALL XXTCON( ITTYP , 1 , IZ1 , ITVAL , TIN , TKEL )


C-----------------------------------------------------------------------
C GET OUTPUT FILE NAMES FROM IDL.
C-----------------------------------------------------------------------
 300  CONTINUE

      CALL G3SPF1( LPASS   , LPAPER  , 
     &             DSNPAS  , DSNPAP  , 
     &             LPEND   )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 200

C-----------------------------------------------------------------------
C OPEN OUTPUT FILES PAPER.TXT AND PASSING COPDAT FILE - IF REQUESTED
C-----------------------------------------------------------------------

      IF ((.NOT.OPEN17) .AND. LPAPER) THEN
         DSN80=' '
         DSN80=DSNPAP
         OPEN(UNIT=IUNT17,FILE=DSN80, STATUS='UNKNOWN')
         OPEN17=.TRUE.
      ENDIF

      IF ((.NOT.OPEN18) .AND. LPASS) THEN
         DSN80=' '
         DSN80=DSNPAS
         OPEN(UNIT=IUNT18,FILE=DSN80, STATUS='UNKNOWN')
         OPEN18=.TRUE.
      ENDIF


C-----------------------------------------------------------------------
C Write something to paper.txt
C-----------------------------------------------------------------------

      IF (OPEN17) THEN
      
         WRITE(IUNT17,1000) CADAS(2:80)
         WRITE(IUNT17,1001) 'AUTOSTRUCTURE Postprocessor','ADAS703',DATE
         WRITE(IUNT17,1002) TITLE
         WRITE(IUNT17,1003) dsnin1,dsnin2
         
      ENDIF

C-----------------------------------------------------------------------
C DO THE WORK - note that program terminates before here if there is an
C               error in the input files.
C-----------------------------------------------------------------------

       CALL G3ASTJ(IUNT18 , IUNT17 , LPAPER , IOPT   , DATE ,
     &             IZ0    , IZ     , IZ1    , NELEC  ,
     &             BWNO   , ITVAL  , TKEL   ,
     &             NUMA   , ITIA   , ITCA   , AUGA   ,
     &             NCONFG , ICF    , NLSETA , EPSIL  , EPSILMAX ,
     &             NLEVL  , NFREE  , IBREFA ,
     &             IA     , ISA    , ILA    , ICFA   , EFREE    ,
     &             ENERA  , WTA    , IXREFA , IFREFA ,
     &             LPA    , ISPA   , XJP    ,
     &             NTRAN  , IIPA   , IIA    , AA     , FA)
   
C Close paper.txt -- do not allow appending

      IF (OPEN17) THEN
         OPEN17 = .FALSE.
         CLOSE(IUNT17)
      ENDIF
      
C Close adf04 passing file -- do not allow appending     

      IF (OPEN18) THEN
         OPEN18 = .FALSE.
         CLOSE(IUNT18)
      ENDIF
 
C 	Tell IDL writing of the output files is finished

	WRITE(PIPEOU,*)1
	CALL XXFLSH(PIPEOU)
	       
C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANELS. (ON RETURN UNITS ARE CLOSED)
C-----------------------------------------------------------------------

      GOTO 300
    
C-----------------------------------------------------------------------
     
 9999 CONTINUE

      CLOSE( IUNT09 )
      CLOSE( IUNT10 )
      CLOSE( IUNT17 )
      CLOSE( IUNT18 )
     
      STOP
C-----------------------------------------------------------------------

 1000 FORMAT(A79)
 1001 FORMAT(1x,24('*'),' TABULAR OUTPUT FROM ',A28,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,24('*')/)
 1002 FORMAT(/ ,19('-'),1X,A40,1X,19('-')//)
 1003 FORMAT(1x,'AUTOSTRUCTURE general file : ',A44/
     &       1X,'              rate file    : ',A44/)

C-----------------------------------------------------------------------
      END
