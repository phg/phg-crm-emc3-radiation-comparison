C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3chip.for,v 1.1 2004/07/06 13:56:42 whitefor Exp $ Date $Date: 2004/07/06 13:56:42 $
C
      SUBROUTINE G3CHIP( NCONFG , NELEC , NLSETA , ICF ,
     &                   IPSTR  , NP    )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G3CHIP *********************
C
C  PURPOSE: TO SETUP A STRING CONTAINING THE NL OF THE PARENT
C           CONFIGURATION FOR INPUT BY THE USER. 
C
C  CALLING PROGRAM: ADAS703
C
C  SUBROUTINE:
C
C  INPUT:  (I*4)  NCONFG  = PARENT CONF. STRING
C  INPUT:  (I*4)  NELEC   = PARENT CONF. STRING
C  INPUT:  (I*4)  NLSETA()= PARENT CONF. STRING
C  INPUT:  (I*4)  ICF()   = CONFIGRATION INDEX OF LEVEL
C
C  OUTPUT: (C*8)  IPSTR() = PARENT CONF. STRING
C
C  OUTPUT: (I*4)  NP      = NO. PARENTS
C
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    29/04/98
C
C VERSION : 1.1				DATE: 01-05-98
C MODIFIED: Martin O'Mullane
C	    - FIRST VERSION.
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       INTEGER   NDP        , NDCFG 
       PARAMETER(NDP = 5    , NDCFG = 30 )
C-----------------------------------------------------------------------
       INTEGER   NP
       INTEGER   ICONF      , ICONF1    , IP      , J       , K      ,  
     &           NCONFG     , NELEC     , NEQUIV  , I4UNIT    
C-----------------------------------------------------------------------
       INTEGER  ICF(NDCFG)  , NLSETA(NDCFG,NDCFG,2)
       INTEGER  IPTYP(10)
C-----------------------------------------------------------------------
       CHARACTER IPSTR(NDP)*9 , STR(NDP)*3 , CCFG(10)*1  
C-----------------------------------------------------------------------
       DATA CCFG  /'s','p','d','f','g','h','i','j','k','l'/
C-----------------------------------------------------------------------

       NP        = 1
       IPTYP(1)  = 1

       DO ICONF = 2,NCONFG
         DO IP = 1,NP
           ICONF1 = IPTYP(IP)
           NEQUIV = 0
           DO K = 1,NELEC-1
             IF (NLSETA(ICONF,K,1).EQ.NLSETA(ICONF1,K,1).AND.
     &           NLSETA(ICONF,K,2).EQ.NLSETA(ICONF1,K,2)) THEN
                 NEQUIV=NEQUIV
             ELSE
                NEQUIV=NEQUIV+1
             ENDIF
           END DO
           IF (NEQUIV.EQ.0) GO TO 59
         END DO
         NP = NP+1
         IPTYP(NP) = ICONF
  59     CONTINUE
       END DO

       DO IP=1,NP
	 IF (IP.GT.NDP) THEN
            WRITE(I4UNIT(-1),2001)'TOO MANY PARENTS',IP,NDP
            WRITE(I4UNIT(-1),2002)
            STOP
	 ENDIF
         J = ICF(IPTYP(IP))
         DO K = 1,NELEC - 1
           WRITE(STR(K),1001)NLSETA(J,K,1),CCFG(NLSETA(J,K,2)+1)
         END DO
         WRITE(IPSTR(IP),'(5A3)')(str(k), K=1,NELEC-1)
       END DO 


      RETURN
C-----------------------------------------------------------------------
 1001 FORMAT(I1,A1,' ')
 
 2001 FORMAT(1X,32('*'),' G3CHIP ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,i4,2x,i4)
 2002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C-----------------------------------------------------------------------
      END
