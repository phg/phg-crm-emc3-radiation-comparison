C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3dat1.for,v 1.1 2004/07/06 13:56:45 whitefor Exp $ Date $Date: 2004/07/06 13:56:45 $
C
       SUBROUTINE G3DAT1(IUNT, IZ0, IZ,  NUMA, ITA, JTA, AA)

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: G3DAT1 ********************
C
C  VERSION:  1.0
C
C  PURPOSE: READS AUTOSTRUCTURE RATES, RADIATIVE RATES & ENERGIES FROM
C           STANDARD FILES - oic, ols, oicu or olsu. THE SUBROUTINE IS
C           INDEPENDENT OF COUPLING AND FORMATING OF FILES.
C
C  
C  INPUT:        (I*4) IUNT      = INPUT UNIT 
C
C  OUTPUT:       (I*4) IZ0       = ATOMIC NUMBER OF ELEMENT 
C                (I*4) IZ        = RECOMBINED ION STAGE 
C                (I*4) NUMA      = NO. AUGER RATES 
C                (I*4) ITA()     = LOWER CONFIGURATION
C                (I*4) JTA()     = UPPER CONFIGURATION
C                (I*4) AA()      = AUGER RATE
C
C  PROGRAM:
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C  AUTHOR  : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 1/05/1998
C MODIFIED : Martin O'Mullane  
C            First version.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
	integer   ndcfg , ndtran , ndeng , ndrad, ndlist
C-----------------------------------------------------------------------
	parameter (ndcfg =  100)
	parameter (ndeng = 2000, ndrad = 2000, ndlist=10)
	parameter (ndtran = 9000)
C-----------------------------------------------------------------------
	integer   i    , j     , I4UNIT
	integer   iunt , mdum
	integer   nv   , lv
	integer   iz0  , ielec , iz
	integer   ncfg
	integer   ii   , ngr   , ma0   , mb0 ,   numa ,   numra
	integer   neng
C-----------------------------------------------------------------------
	logical   lform, lfound
C-----------------------------------------------------------------------
        integer   qs0(ndcfg)     , ql0(ndcfg)
	integer   ica(ndtran)    , ita(ndtran)    , iwa(ndtran)     ,
     &            jca(ndtran)    , jta(ndtran) 
	integer   ik(ndeng)      , it(ndeng)      , ss(ndeng)       ,
     &            ll(ndeng)      , jj(ndeng)      , lcf(ndeng)
	integer   icr(ndrad)     , itr(ndrad)     , iwr(ndrad)      ,
     &            jcr(ndrad)     , jtr(ndrad)
C-----------------------------------------------------------------------
	real*8    ecore          
C-----------------------------------------------------------------------
	character str*1 
C-----------------------------------------------------------------------
	real*8    aa(ndtran)     , ec(ndtran)     , eion(ndtran)
        real*8    energ(ndeng)
        real*8    ar(ndrad)      , del(ndrad)     , eatom(ndrad) 
C-----------------------------------------------------------------------
	

C start reading files
      
        lform = .FALSE.
        read(iunt,'(5x,i5,5x,i5)',end=5)mdum,mdum
        rewind(iunt)
        lform=.TRUE.
   5    continue
		
 	
C read nl block

  10    continue
	if (lform) then
	   read(iunt,200,end=999)nv,lv
	else
	   read(iunt,end=999)nv,lv
	endif	
  200   format(5x,i5,5x,i5)

	if (lform) then
	   read(iunt,202,end=999)ncfg
	else
	   read(iunt,end=999)ncfg
	endif	
  202   format(i3)
  
        do i=1,ncfg             ! configurations
	   if (lform) then
	      read(iunt,204,end=999)ii,ngr,ma0,mb0,(qs0(j),ql0(j),j=1,10)
	   else
	      read(iunt,end=999)ii,ngr,ma0,mb0,(qs0(j),ql0(j),j=1,10)
	   endif
	end do	
  204   format(2i5,2x,i3,i2,1x,10(i2,a1)) 
  
  	if (lform) then
	   read(iunt,206,end=999)iz0,ielec
	   read(iunt,205,end=999)str   
	else
	   read(iunt,end=999)iz0,ielec 
	endif	
        iz = iz0 - ielec
  205   format(a1)
  206   format(66x,i2,5x,i2)

	i=1
   30	continue             ! autoionisation rates	
  	if (lform) then
	   read(iunt,207,end=999)ica(i),ita(i),iwa(i),jca(i),jta(i),
     &                         aa(i),ec(i),eion(i) 
	else
	   read(iunt,end=999)ica(i),ita(i),iwa(i),jca(i),jta(i),
     &                       aa(i),ec(i),eion(i) 
	endif	
  207   format(5i5,5x,1pe15.5,2(0pf15.6))
        if (ita(i).eq.0) go to 33
  	i=i+1
	goto 30
   33   numa = i-1  

  	if (lform) then
	   read(iunt,208,end=999)neng,ecore 
	else
	   read(iunt,end=999)neng,ecore
	endif	
  208   format(10x,i5,45x,f15.6)
  
	if (neng.eq.0) goto 10
	
  	if (lform) then
	   read(iunt,210,end=999)mdum 
	else
	   read(iunt,end=999)mdum
	endif	
  210   format(26x,a4)

	lfound=.FALSE.  
  	do i=1,neng          ! energies
  	  if (lform) then
	     read(iunt,212,end=999)ik(i),it(i),ss(i),ll(i),jj(i),lcf(i),
     &                           energ(i)
	  else
	     read(iunt,end=999)ik(i),it(i),ss(i),ll(i),jj(i),lcf(i),
     &                         energ(i)
	  endif
  212     format(5x,6i5,f15.6)
	  
	end do

  	if (lform) then
	   read(iunt,214,end=999)str    ! skip 2 lines again
	   read(iunt,214,end=999)str
	else
	   read(iunt,end=999)mdum,mdum
	endif	
  214   format(a1)

	i=1
   40	continue             ! radiative data	
  	if (lform) then
	   read(iunt,216,end=999)icr(i),itr(i),iwr(i),jcr(i),jtr(i),
     &                         ar(i),del(i),eatom(i) 
	else
	   read(iunt,end=999)icr(i),itr(i),iwr(i),jcr(i),jtr(i),
     &                       ar(i),del(i),eatom(i)
	endif	
  216   format(5i5,5x,1pe15.5,2(0pf15.6))
        if (itr(i).eq.0) go to 43
  	i=i+1
	goto 40
   43   numra = i-1
	

C return and read a new nl block

        goto 10

 999    continue
 
 
	return
	end
