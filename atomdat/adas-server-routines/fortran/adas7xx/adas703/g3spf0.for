C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas7xx/adas703/g3spf0.for,v 1.1 2004/07/06 13:56:59 whitefor Exp $ Date $Date: 2004/07/06 13:56:59 $
C
      SUBROUTINE G3SPF0( REP    , DSNIC1 , DSNIC2 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: G3SPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM ADAS703 FILE INPUT SCREEN
C
C  CALLING PROGRAM: ADAS703
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSNIC1  = FIRST INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C  OUTPUT: (C*80)  DSNIC2  = SECOND INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    29/04/98
C
C VERSION : 1.1				DATE: 29-04-98
C MODIFIED: Martin O'Mullane
C	    - FIRST VERSION.
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSNIC1*80      , DSNIC2*80
C-----------------------------------------------------------------------
      INTEGER   PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSNIC1
      READ(PIPEIN,'(A)') DSNIC2


      RETURN
      END
