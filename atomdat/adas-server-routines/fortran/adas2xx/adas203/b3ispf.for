CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3ispf.for,v 1.3 2004/07/06 11:19:37 whitefor Exp $ Date $Date: 2004/07/06 11:19:37 $
CX
      SUBROUTINE B3ISPF( LPEND  ,
     &                   NDTIN  , IL     ,
     &                   NDTEM  , NDTRN  , NVA    , SCEFA ,
     &                   NV     , SCEF   , TSCEF  ,
     &                   IZS    , IZ0    ,
     &                   ITRAN  , I1A    , I2A    , STRGA ,
     &                   IEC1A  , IAC1A  , IAC2A  ,
     &                   FAC2A  , IGC1A  , FGC2A  ,
     &                   TITLE  ,
     &                   ISTRN  , IFOUT  ,
     &                   MAXT   , TINE   ,
     &                   LIBPT  , IFIRST
     &                 )
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL
C
C  CALLING PROGRAM: ADAS203
C
C  SUBROUTINE:
C
C          (I*4)   L3       = PARAMETER = 3
C          (I*4)   NPANEL   = PARAMETER = NUMBER OF ISPF PANELS TO BE
C                                         DISPLAYED.
C          (I*4)   NTABLE   = PARAMETER = NUMBER OF ISPF TABLES TO BE
C                                         ACCESSED.
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NDTEM    = INPUT DATA FILE: MAX. NO. OF TEMPERATURES
C  INPUT : (I*4)   NDTRN    = INPUT DATA FILE: MAX. NO. OF TRANSITIONS
C  INPUT : (I*4)   NVA()    = INPUT DATA FILE: NO. OF GAMMA/TEMP VALUES
C                                              FOR A GIVEN TRANSITION
C                             1ST DIMENSION: TRANSITION INDEX
C  INPUT : (R*8)   SCEFA(,) = INPUT DATA FILE: Z-SCALED ELEC TEMPS (K)
C                             1ST DIMENSION: TEMPERATURE
C                             2ND DIMENSION: TRANSITION INDEX
C  OUTPUT: (I*4)   NV       = SELECTED TRANS.: NUMBER   OF TEMPERATURES
C  OUTPUT: (R*8)   SCEF()   = SELECTED TRANS.: Z-SCALED ELEC TEMPS (K)
C                                              FOR SELECTED TRANSITION
C                             1ST DIMENSION: TEMPERATURE
C  OUTPUT: (R*8)   TSCEF(,) = SELECTED TRANS.: ELECTRON TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  INPUT : (I*4)   IZS      = NUCLEAR CHARGE OF NEUTRAL SEQ. MEMBER
C  OUTPUT: (I*4)   IZ0      = NUCLEAR CHARGE OF SELECTED ION
C
C  INPUT : (I*4)   ITRAN    = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)   I1A()    = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)   I2A()    = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C  INPUT : (C*22)  STRGA()  = LEVEL DESIGNATIONS
C  I/O   : (I*4)   IFIRST  = 0 => FIRST RUN THROUGH CODE FOR GIVEN FILE
C                            1 => NOT FIRST RUN
C
C  I/O   : (I*4)   IEC1A() = TRANSITION ENERGY INTERPOLATION VARIABLE
C                            (1=>Z1 ; 2=>1/Z1)
C                            1ST DIMENSION - TRANSITION NUMBER
C  I/O   : (I*4)   IAC1A() = TRANSITION PROB. INTERPOLATION VARIABLE
C                            (1=>Z1 ; 2=> 1/Z1)
C                            1ST DIMENSION - TRANSITION NUMBER
C  I/O   : (I*4)   IAC2A() = TRANSITION TYPE
C                            (1=>DIPOLE, 2=>NON-DIPOLE, 3=>SPIN CHANGE,
C                             4=>OTHER)
C                            1ST DIMENSION - TRANSITION NUMBER
C  I/O   : (I*4)   FAC2A() = TRANSITION PROB. Z1 SCALING POWER
C                            1ST DIMENSION - TRANSITION NUMBER
C  I/O   : (I*4)   IGC1A() = UPSILON INTERPOLATION VARIABLE
C                            (1=>Z1 ; 2=> 1/Z1)
C                            1ST DIMENSION - TRANSITION NUMBER
C  I/O   : (I*4)   FGC2A() = UPSILON Z1 SCALING POWER
C                            1ST DIMENSION - TRANSITION NUMBER
C
C
C  OUTPUT: (I*4)   LIBPT    = .TRUE. => BAD POINT OPTION SET
C                           = .FALSE.=> BAD POINT OPTION NOT SET
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   ISTRN    = SELECTED ELECTRON IMPACT TRANSITION INDEX
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C
C
C  OUTPUT: (I*4)   LIBPT    = .TRUE. => BAD POINT OPTION SET
C                           = .FALSE.=> BAD POINT OPTION NOT SET
C
C          (I*4)   IT       = GENERAL USE
C          (I*4)   IZ1      = CURRENT ION CHARGE +1
C
C          (L*4)   LOSEL    = .TRUE.  => OUTPUT SELECTED
C                           = .FALSE. => OUTPUT NOT SELECTED
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)    I       = LOOP COUNTER
C          (I*4)   ILOGIC  - USED TO PIPE LOGICAL VALUES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH    IDL-ADAS   CALLS FLUSH TO CLEAR PIPES.
C          XXTCON     ADAS      CONVERT TEMPERATURES TO THREE FORMS
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    18/08/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 01-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.3                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED FINAL SUPERFLUOUS VARIABLE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NTABLE     , NPANEL
      INTEGER   L3         , IFIRST
C-----------------------------------------------------------------------
      PARAMETER ( NTABLE = 5    , NPANEL = 6    )
      PARAMETER ( L3 = 3 )
      INTEGER    PIPEIN        , PIPEOU   , ILOGIC  ,
     &           I            
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
      INTEGER    NDTIN      , IL         , NDTEM     , NDTRN  , NV    ,
     &           ITRAN      , ISTRN      , IFOUT     , MAXT
      INTEGER    IZ0        , IZ1        , IZS
      INTEGER    IT        
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      LOGICAL    LPEND      , LIBPT
      LOGICAL    LOSEL      
C-----------------------------------------------------------------------
      INTEGER    I1A(ITRAN) , I2A(ITRAN)
      INTEGER    NVA(NDTRN)
      INTEGER    IEC1A(NDTRN), IAC1A(NDTRN)   ,
     &           IAC2A(NDTRN), IGC1A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     TSCEF(NDTEM,3)    , TINE(NDTIN)
      REAL*8     SCEF(NDTEM)       , SCEFA(NDTEM,NDTRN)
      REAL*8     FAC2A(NDTRN)      , FGC2A(NDTRN)
C-----------------------------------------------------------------------
      CHARACTER  STRGA(IL)*22
C-----------------------------------------------------------------------
      DATA LOSEL /.TRUE./
C
C-----------------------------------------------------------------------
C    WRITE OUPUTS TO PIPE
C-----------------------------------------------------------------------
C
      IF(IFIRST.EQ.0) THEN
          WRITE (PIPEOU,*) NDTRN
          CALL XXFLSH(PIPEOU)
          DO 1, I=1, ITRAN
              WRITE(PIPEOU, *) I1A(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU, *) I2A(I)
              CALL XXFLSH(PIPEOU)
1         CONTINUE
          WRITE(PIPEOU, *) NDTEM
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, *) NDTIN
          CALL XXFLSH(PIPEOU)
          DO 4, I=1, ITRAN
              WRITE(PIPEOU,*) NVA(I)
              CALL XXFLSH(PIPEOU)
4         CONTINUE
          DO 2, I=1, ITRAN
              NV = NVA(I)
              DO 3 IT = 1, NV
                  WRITE(PIPEOU,*) SCEFA(IT,I)
                  CALL XXFLSH(PIPEOU)
3             CONTINUE
2         CONTINUE
          DO 10, I=1, NDTRN
              WRITE(PIPEOU,*) IEC1A(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*) IAC1A(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*) IAC2A(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*) FAC2A(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*) IGC1A(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*) FGC2A(I)
              CALL XXFLSH(PIPEOU)
10        CONTINUE
          IFIRST = 1
      ENDIF

C
C-----------------------------------------------------------------------
C    READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C

      READ( PIPEIN, *) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
          READ(PIPEIN,*) NV
          READ(PIPEIN,*) ISTRN
          READ(PIPEIN,*) IZ0
          IZ1 = IZ0 - IZS + 1
          DO 5, I=1, NV
              SCEF(I) = SCEFA(I,ISTRN)
5         CONTINUE
          DO 6, I=1, 3
              CALL XXTCON(L3, I, IZ1, NV, SCEF, TSCEF(1,I))
6         CONTINUE
          READ(PIPEIN, '(A40)') TITLE
          READ(PIPEIN,*) MAXT
          READ(PIPEIN,*) IFOUT
          DO 7, I=1, NDTIN
              READ(PIPEIN,*) TINE(I)
7         CONTINUE
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC.EQ.0) THEN
              LIBPT = .TRUE.
          ELSE
              LIBPT = .FALSE.
          ENDIF
          DO 8, I=1, NDTRN
              READ(PIPEIN,*) IEC1A(I)
              READ(PIPEIN,*) IAC1A(I)
              READ(PIPEIN,*) IAC2A(I)
              READ(PIPEIN,*) FAC2A(I)
              READ(PIPEIN,*) IGC1A(I)
              READ(PIPEIN,*) FGC2A(I)
8         CONTINUE
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN

      END
