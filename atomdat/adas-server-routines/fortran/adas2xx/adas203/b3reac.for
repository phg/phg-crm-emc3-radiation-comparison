CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3reac.for,v 1.7 2007/04/23 15:51:16 allan Exp $ Date $Date: 2007/04/23 15:51:16 $
CX
       SUBROUTINE B3REAC( NDTRN , NDTEM , IZDIMD,
     &                    IZMAX , Z1A   ,
     &                    ITRAN ,
     &                    NVA   , SCEFA ,
     &                    I1A   , I2A   , N1A   , N2A   , W1A   , W2A,
     &                    IEC1A , IAC1A , IAC2A , FAC2A , IGC1A , FGC2A,
     &                    CTSTRA, WDEA  , AVALA , SCOMA ,
     &                    IZS   , IZ0   , ISTRN , LIBPT ,
     &                    MAXT  , TEA   ,
     &                    WDE   , AVAL  , SCOM
     &                  )
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3REAC *********************
C
C  PURPOSE:  TO INTERPOLATE DATA FOR A SINGLE TRANSITION FROM A
C            GENERAL Z EXCITATION FILE TO A SELECTED SEQUENCE MEMBER
C
C  CALLING PROGRAM: ADAS203
C
C  DATA:
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C  INPUT : (I*4)  NDTEM   = MAX NUMBER OF INPUT FILE TEMPS.
C  INPUT : (I*4)  IZDIMD  = MAX. NUMBER OF SEQUENCE MEMBERS ALLOWED
C
C  INPUT : (I*4)  IZMAX   = NO. OF SEQUENCE MEMBERS IN GENERAL Z FILE
C  INPUT : (R*8)  Z1A()   = ION CHARGE +1  FOR SEQUENCE MEBERS IN
C                           GENERAL Z FILE
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C
C  INPUT : (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C
C
C  INPUT : (I*4)  NVA()   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  INPUT : (R*8)  SCEFA(,)= INPUT DATA FILE: Z-SCALED ELEC. TEMPS.(K)
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C                                            TRANSITIONS.
C
C
C  INPUT : (I*4)  I1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  I2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  N1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER LEVEL PRINCIPAL QUANTUM NUMBER
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  N2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER LEVEL PRINCIPAL QUANTUM NUMBER
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  W1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER LEVEL STATISTICAL WEIGHT
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  W2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER LEVEL STATISTICAL WEIGHT
C                           1ST DIMENSION - TRANSITION NUMBER
C
C  INPUT : (I*4)  IEC1A() = TRANSITION ENERGY INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=>1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  IAC1A() = TRANSITION PROB. INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  IAC2A() = TRANSITION TYPE
C                           (1=>DIPOLE, 2=>NON-DIPOLE, 3=>SPIN CHANGE,
C                            4=>OTHER)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  FAC2A() = TRANSITION PROB. Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  IGC1A() = UPSILON INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  FGC2A() = UPSILON Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (C*18) CTSTRA()= TRANSITION DESCRIPTOR
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (R*8)  WDEA()  = TRANSITION ENERGY (CM-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C
C  INPUT : (R*8)  AVALA() = ELECTRON IMPACT TRANSITION:
C                            A-VALUES (SEC-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C                           2ND DIMENSION - SEQUENCE MEMBER INDEX
C  INPUT : (R*8)  SCOMA(,,)=ELECTRON IMPACT TRANSITION:
C                            GAMMA VALUES
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C                           3RD DIMENSION - SEQUENCE MEMBER INDEX
C
C  INPUT : (I*8)  MAXT    = NUMBER OF OUTPUT TEMPERATURES
C  INPUT : (R*8)  TEA()   = OUTPUT TEMPERATURES (K)
C
C  OUTPUT: (R*8)  WDE()   = ENERGY OF TRANSITIONS (CM-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (R*8)  AVAL()  = A-VALUE OF TRANSITIONS (SEC-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (R*8)  SCOM(,) = SELECTED TRANSITION GAMMA VALUES:
C                            GAMMA VALUES
C                           1ST DIMENSION - OUTPUT TEMPERATURE
C                           2ND DIMENSION - TRANSITION NUMBER
C
C
C          (I*4)  I       = GENERAL USE.
C          (I*4)  J       = GENERAL USE.
C          (I*4)  J1      = INPUT DATA FILE - SELECTED TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C          (I*4)  J2      = INPUT DATA FILE - SELECTED TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C
C          (R*8)  AVALM   = INPUT DATA FILE - SELECTED TRANSITION:
C                           MANTISSA OF:   ('IAPOW' => EXPONENT)
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C
C	   (R*8)  EIJMOD  = MODULUS OF EIJ
C
C          (C*80) CLINE   = CURRENT ENERGY LEVEL INDEX PARAMETER LINE
C
C          (L*4)  LDATA   = IDENTIFIES  WHETHER  THE END OF AN  INPUT
C                           SECTION IN THE DATA SET HAS BEEN LOCATED.
C                           (.TRUE. => END OF SECTION REACHED)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B2SORT     ADAS      PERFORMS BUBBLE SORT OF 2 REAL ARRAYS
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    17/08/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND 
C               - REPLACED CALLS TO NSORT ROUTINE WITH CALLS TO B2SORT.
C                 NSORT IS USED TO SORT A REAL ARRAY AND ASSOCIATED
C                 INTEGER ARRAY WHEREAS WHAT WAS BEING PASSED TO IT WAS
C                 A REAL ARRAY AND ANOTHER, ASSOCIATED REAL ARRAY. B2SORT
C                 TAKES 2 REAL ARRAYS AS INPUT AND PERFORMS A BUBBLE SORT
C                 ON THEM.
C
C VERSION: 1.3                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.4                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - CORRECTED MINOR SYNTAX ERROR
C
C VERSION: 1.5                          DATE: 23-05-96
C MODIFIED: WILLIAM OSBORN + HUGH SUMMERS
C               - REPLACED EIJ BY MOD(EIJ) IN DETERMINING LINE STRENGTHS
C		  AND UPSILON FIT
C
C VERSION: 1.6                          DATE: 11-04-07
C MODIFIED: ALLAN WHITEFORD
C               - RENAMED SOLVE SUBROUTINE TO B3SOLV.
C
C
C VERSION: 1.6                          DATE: 23-04-07
C MODIFIED: ALLAN WHITEFORD
C               - RENAMED FORM SUBROUTINE TO B3FORM.
C               - RENAMED FORM2 SUBROUTINE TO B3FORM2.
C
C-----------------------------------------------------------------------
      INTEGER   NDSPLN
C-----------------------------------------------------------------------
      PARAMETER ( NDSPLN = 10 )
C-----------------------------------------------------------------------
      INTEGER   NDTRN       , IZDIMD    ,
     &          NDTEM       , ITRAN     ,
     &          ISTRN       , MAXT      ,
     &          IZMAX       ,
     &          IZS         ,IZ0   
      INTEGER   K          , IT
      INTEGER   IEND1       , IENDN          , ITEMP      ,
     &          IFAIL       , IXOPT          , IFORMS     , IENDS
C-----------------------------------------------------------------------
      REAL*8    Z1
      REAL*8    ENI         , ENJ            , REN        , XI         ,
     &          EIJ         , S              , EIJMOD     ,
     &          TE1         , TE2            , GAM1       , GAM2       ,
     &          FXC2        , FXC3           , TE         ,
     &          X           ,
     &          X1          , XN             , DX1        , DXN        ,
     &          G1          , GN             , G          , DG
      REAL*8    Y           , DY             , B3FORM     , B3FORM2    ,
     &          EEI         , EE2
C-----------------------------------------------------------------------
      LOGICAL   LIBPT
C-----------------------------------------------------------------------
      INTEGER   NVA(NDTRN)
      INTEGER   I1A(NDTRN)  , I2A(NDTRN)     ,
     &          N1A(NDTRN)  , N2A(NDTRN)     ,
     &          IEC1A(NDTRN), IAC1A(NDTRN)   ,
     &          IAC2A(NDTRN), IGC1A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8    Z1A(IZDIMD)
      REAL*8    SCEFA(NDTEM,NDTRN)  , TEA(NDTEM)
      REAL*8    W1A(NDTRN)          , W2A(NDTRN)           ,
     &          FAC2A(NDTRN)        , FGC2A(NDTRN)
      REAL*8    WDEA(NDTRN,IZDIMD)  , AVALA(NDTRN,IZDIMD)  ,
     &          SCOMA(NDTEM,NDTRN,IZDIMD)
      REAL*8    WDE(NDTRN)          , AVAL(NDTRN)
      REAL*8    SCOM(NDTEM,NDTRN)
      REAL*8    XSA(NDSPLN)         , YSA(NDSPLN)             ,
     &          XOS(NDSPLN)         , GSA(NDSPLN)         ,
     &          GOS(NDSPLN)         , GOA(NDSPLN)             ,
     &          XPA(NDSPLN)         , APGOA(NDSPLN)
      REAL*8    C1(NDSPLN,NDSPLN-1) , C2(NDSPLN, NDSPLN-1)    ,
     &          C3(NDSPLN,NDSPLN-1) , C4(NDSPLN, NDSPLN-1)
C-----------------------------------------------------------------------
      CHARACTER CTSTRA(NDTRN)*18
C-----------------------------------------------------------------------
      EXTERNAL  B3FORM      , B3FORM2
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  INTERPOLATE ENERGIES TO REQUIRED Z1                                  
C-----------------------------------------------------------------------
C
       Z1 = IZ0-IZS+1
C
       ENI=N1A(ISTRN)                                                   
       ENJ=N2A(ISTRN)                                                   
       REN=DABS(1.0D0/(ENI*ENI)-1.0D0/(ENJ*ENJ))                        
C
       IF(IEC1A(ISTRN).EQ.1) THEN
           XI=Z1                                                        
           DO 31 K=1,IZMAX                                              
             XSA(K)=Z1A(K)                                              
             YSA(K)=WDEA(ISTRN,K)/(1.09737D5*Z1A(K))-Z1A(K)*REN         
   31      CONTINUE
       ELSE
           XI=1.0D0/Z1                                                  
           DO 33 K=1,IZMAX                                              
             XSA(K)=1.0D0/Z1A(K)                                        
             YSA(K)=WDEA(ISTRN,K)/(1.09737D5*Z1A(K))-Z1A(K)*REN         
   33      CONTINUE
       ENDIF
C
CX     CALL NSORT(XSA,YSA,IZMAX)                                        
       CALL B2SORT(XSA,YSA,IZMAX)
C
       IEND1=3                                                          
       IENDN=3                                                          
C
       CALL B2GSPC( XSA   , IZMAX , C1    , C2    , C3    , C4    )     
       CALL B2NFAS( XI    , XSA   , IZMAX , YSA   , Y     , DY    ,
     &              C1    , C2    , C3    , C4    , B3FORM, IFORMS
     &            )
C
       EIJ=(Y+Z1*REN)*Z1                                                
       WDE(ISTRN)=1.09737D5*EIJ                                         
       EIJMOD=DABS(EIJ)
C
C-----------------------------------------------------------------------
C  INTERPOLATE A-VALUES TO REQUIRED Z1                                  
C-----------------------------------------------------------------------
C
       S=0.0D0                                                          
C                                                                       
       IF(IAC1A(ISTRN).EQ.1) THEN                                       
           XI=Z1                                                        
           DO 61 K=1,IZMAX                                              
             XSA(K)=Z1A(K)                                              
             YSA(K)=AVALA(ISTRN,K)/Z1A(K)**FAC2A(ISTRN)                 
   61      CONTINUE
       ELSE                                                             
           XI=1.0D0/Z1                                                  
           DO 66 K=1,IZMAX                                              
             XSA(K)=1.0D0/Z1A(K)                                        
             YSA(K)=AVALA(ISTRN,K)/Z1A(K)**FAC2A(ISTRN)                 
   66      CONTINUE
       ENDIF
C                                                                       
CX     CALL NSORT(XSA,YSA,IZMAX)                                        
       CALL B2SORT(XSA,YSA,IZMAX)
C
       IEND1=3                                                          
       IENDN=3                                                          
C
       CALL B2GSPC( XSA   , IZMAX , C1    , C2    , C3    , C4    )     
       CALL B2NFAS( XI    , XSA   , IZMAX , YSA   , Y     , DY    ,
     &              C1    , C2    , C3    , C4    , B3FORM, IFORMS
     &            )
C
       IF(Y.LE.0.0D0)Y=0.0D0
C
       AVAL(ISTRN)=Z1**FAC2A(ISTRN)*Y                                   
       S=3.73491D-10*W2A(ISTRN)*AVAL(ISTRN)/(EIJMOD*EIJMOD*EIJMOD)
C
C-----------------------------------------------------------------------
C  INTERPOLATE UPSILONS TO REQUIRED Z1                                  
C-----------------------------------------------------------------------
C
       ITEMP=NVA(ISTRN)
C
   98  DO 300 IT=1,ITEMP                                                
         IF(IGC1A(ISTRN).EQ.1)THEN                                      
  160        XI=Z1                                                      
             DO 161 K=1,IZMAX                                           
               XSA(K)=Z1A(K)                                            
               GSA(K)=SCOMA(IT,ISTRN,K)/Z1A(K)**FGC2A(ISTRN)            
  161        CONTINUE
         ELSE                                                           
           XI=1.0D0/Z1                                                  
           DO 166 K=1,IZMAX                                             
             XSA(K)=1.0D0/Z1A(K)                                        
             GSA(K)=SCOMA(IT,ISTRN,K)/Z1A(K)**FGC2A(ISTRN)              
  166      CONTINUE
         ENDIF
C
         CALL B2SORT( XSA  , GSA  , IZMAX )                             
C
         IF(LIBPT)THEN                                                  
C
             CALL LINFIT(XI,XSA,G,GSA,IZMAX)                            
C
         ELSE
             IEND1=3                                                    
             IENDN=3                                                    
C
             CALL B2GSPC( XSA   , IZMAX , C1    , C2   , C3   , C4    ) 
             CALL B2NFAS( XI    , XSA   , IZMAX , GSA  , G    , DG    ,
     &                    C1    , C2    , C3    , C4   ,B3FORM2,IFORMS
     &                  )                                               
C
         ENDIF
C
         GOS(IT)=G                                                      
         GOA(IT)=G                                                      
         XPA(IT)=DLOG10(SCEFA(IT,ISTRN))                                
  300  CONTINUE                                                         
C
       TE1=SCEFA(1,ISTRN)*Z1*Z1                                         
       GAM1=GOA(1)*Z1**FGC2A(ISTRN)                                     
       TE2=SCEFA(ITEMP,ISTRN)*Z1*Z1                                     
       GAM2=GOA(ITEMP)*Z1**FGC2A(ISTRN)                                 
C
       CALL B3SOLV( FXC2  , FXC3  , S     , EIJMOD , IAC2A(ISTRN)  ,
     &              TE1   , GAM1  , TE2   , GAM2   , IFAIL
     &            )
C
       IF(IFAIL.EQ.0) THEN
           IF (IAC2A(ISTRN).EQ.1)     THEN
               IXOPT = 1
           ELSEIF (IAC2A(ISTRN).EQ.2) THEN
               IXOPT = 0
               FXC2=0.0D0                                               
               FXC3=1.0D0                                               
           ELSEIF (IAC2A(ISTRN).EQ.3) THEN
               IXOPT = 1
           ELSE
               IXOPT = 0
               FXC2=0.0D0                                               
               FXC3=1.0D0                                               
           ENDIF
       ELSE
           IXOPT = 0
           IF (IAC2A(ISTRN).EQ.1) THEN
               FXC2=0.0D0                                               
               FXC3=1.3333333D0                                         
           ELSEIF (IAC2A(ISTRN).EQ.2) THEN
               FXC2=0.0D0                                               
               FXC3=1.0D0                                               
           ELSEIF (IAC2A(ISTRN).EQ.3) THEN
               FXC2=0.0D0                                               
               FXC3=0.0D0                                               
           ELSE
               FXC2=0.0D0                                               
               FXC3=1.0D0                                               
           ENDIF
       ENDIF
C
       DO 310 IT=1,ITEMP                                                
         TE=SCEFA(IT,ISTRN)*Z1*Z1                                       
         XI=1.57890D5*EIJMOD/TE                                            
         XSA(IT)=XI/(XI+1.0D0)                                          
         IF(IXOPT.EQ.0)THEN                                             
             IF (IAC2A(ISTRN).EQ.1) THEN
                 GSA(IT)=GOS(IT)-1.3333333D0*S*EEI(XI)/Z1**FGC2A(ISTRN) 
             ELSEIF (IAC2A(ISTRN).EQ.2) THEN
                 GSA(IT)=GOS(IT)                                        
             ELSEIF (IAC2A(ISTRN).EQ.3) THEN
                 GSA(IT)=GOS(IT)                                        
             ELSEIF (IAC2A(ISTRN).EQ.4) THEN
                 GSA(IT)=GOS(IT)                                        
             ENDIF
         ELSE
             IF (IAC2A(ISTRN).EQ.1) THEN
                 GSA(IT)=GOS(IT)*Z1**FGC2A(ISTRN)/
     &                   (FXC3*S*(DLOG(1.0D0+FXC2)+
     &                   EEI((1.0D0+FXC2)*1.5789D5*EIJMOD/TE)))            
             ELSEIF (IAC2A(ISTRN).EQ.2) THEN
                 GSA(IT)=GOS(IT)*Z1**FGC2A(ISTRN)/FXC3                  
             ELSEIF (IAC2A(ISTRN).EQ.3) THEN
                 GSA(IT)=GOS(IT)*Z1**FGC2A(ISTRN)*(1.0D0+FXC2)*TE/
     &                   (FXC3*1.5789D5*
     &                    EIJMOD*EE2((1.0D0+FXC2)*1.5789D5*EIJMOD/TE))        
             ELSEIF (IAC2A(ISTRN).EQ.4) THEN
                 GSA(IT)=GOS(IT)*Z1**FGC2A(ISTRN)/FXC3                  
             ENDIF
         ENDIF
  310  CONTINUE                                                         
C
CX     CALL NSORT(XSA,GSA,ITEMP)                                        
       CALL B2SORT(XSA,GSA,ITEMP)
C
       IF(IXOPT.EQ.0)THEN                                               
           IEND1=3                                                      
           IENDN=3                                                      
       ELSE
           IEND1=1                                                      
           G1=0.0D0                                                     
           IENDN=1                                                      
           GN=0.0D0                                                     
       ENDIF
C                                                                       
       IFORMS=IAC2A(ISTRN)                                              
C                                                                       
       IENDS=1                                                          
       X1=XSA(1)                                                        
       DX1=0.01*X1                                                      
C
       CALL B2NGAS( X1   , DX1   , B3FORM2 , IFORMS , IENDS  )            
C
       IENDS=2                                                          
       XN=XSA(ITEMP)                                                    
       DXN=0.01*XN                                                      
C
       CALL B2NGAS( XN   , DXN   , B3FORM2 , IFORMS , IENDS  )            
       CALL B2GSPC( XSA  , ITEMP , C1    , C2     , C3     , C4  )      
C
       DO 330 K=1,MAXT                                                  
         X=1.57890D5*EIJMOD/TEA(K)                                         
         XI=X/(X+1.0D0)                                                 
         XOS(K)=XI                                                      
C
         CALL B2NFAS( XI   , XSA  , ITEMP , GSA  , Y     ,DY     ,      
     &                C1   , C2   , C3    , C4   , B3FORM2,IFORMS
     &              )
C
         GOS(K)=Y                                                       
C
         IF(IXOPT.EQ.0)THEN                                             
             IF(IAC2A(ISTRN).EQ.1) THEN
                 GOA(K)=Y+1.3333333D0*S*EEI(X)/Z1**FGC2A(ISTRN)         
             ELSEIF (IAC2A(ISTRN).EQ.2) THEN
                 GOA(K)=Y                                               
             ELSEIF (IAC2A(ISTRN).EQ.3) THEN
                 GOA(K)=Y                                               
             ELSEIF (IAC2A(ISTRN).EQ.4) THEN
                 GOA(K)=Y                                               
             ENDIF
         ELSE
             IF(IAC2A(ISTRN).EQ.1) THEN
                 APGOA(K)=FXC3*S*(DLOG(1.0D0+FXC2)+EEI((1.0D0+FXC2)*    
     &                    1.5789D5*EIJMOD/TEA(K)))                         
             ELSEIF(IAC2A(ISTRN).EQ.2) THEN
                 APGOA(K)=FXC3                                          
             ELSEIF(IAC2A(ISTRN).EQ.3) THEN
                 APGOA(K)=FXC3*1.5789D5*EIJMOD*EE2((1.0D0+FXC2)*           
     &                    1.5789D5*EIJMOD/TEA(K))/((1.0D0+FXC2)*TEA(K))    
             ELSEIF(IAC2A(ISTRN).EQ.4) THEN
                 APGOA(K)=FXC3                                          
             ENDIF
             GOA(K)=Y*APGOA(K)/Z1**FGC2A(ISTRN)                         
         ENDIF
C
             SCOM(K,ISTRN)=GOA(K)*Z1**FGC2A(ISTRN)                      
C
  330  CONTINUE                                                         
C
       RETURN
C
C-----------------------------------------------------------------------
C
      END                                                               
