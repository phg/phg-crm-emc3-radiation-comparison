CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3form.for,v 1.2 2007/04/23 15:51:16 allan Exp $ Date $Date: 2007/04/23 15:51:16 $
CX
       FUNCTION B3FORM(I,X)                                               
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 FUNCTION: B3FORM ************************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 23-04-07
C MODIFIED: Allan Whiteford
C               - Renamed from form.for to b3form.for
C
C-----------------------------------------------------------------------
C
       IF(I.NE.3)GO TO 2                                                
       FORM=1.0D0                                                       
       GO TO 5                                                          
    2  IF(I.NE.4)GO TO 5                                                
       FORM=X**3                                                        
    5  RETURN                                                           
      END                                                               
