CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3leve.for,v 1.2 2007/04/23 15:51:16 allan Exp $ Date $Date: 2007/04/23 15:51:16 $
CX
       SUBROUTINE B3LEVE( NDLEV , IZDIMD,
     &                    IZMAX , Z1A   , IZA   , IZ0A  , IZ1A,
     &                    BWNOA , IL    ,
     &                    IA    , NA    , WAA   ,
     &                    IZS   , IZ0   ,
     &                    BWNO  , WAO )
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3LEVE *********************
C
C  PURPOSE:  TO EVALUATE IONISATION AND LEVEL ENERGIES FOR A SELECTED
C            MEMBER OF AN ISOELECTRONIC SEQUENCE FROM THE GENERAL Z DATA
C
C  CALLING PROGRAM: ADAS203
C
C  DATA:
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C
C
C  SUBROUTINE:
C
C          (I*4)  NDSPLN  = PARAMETER = MAXIMUM NUMBER OF SPLINE KNOTS
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  IZDIMD  = MAX. NUMBER OF SEQUENCE MEMBERS ALLOWED
C
C  INPUT : (I*4)  IZMAX   = NUMBER OF SEQUENCE MEMBERS
C  INPUT : (R*8)  Z1A()   = SEQUNCE RECOMBINING ION CHARGES READ
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  INPUT : (I*4)  IZA()   = SEQUENCE RECOMBINED ION CHARGES
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  INPUT : (I*4)  IZ0A()  = SEQUENCE NUCLEAR CHARGES
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  INPUT : (I*4)  IZ1A()  = SEQUNCE RECOMBINING ION CHARGES READ
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (R*8)  BWNOA() = IONISATION POTENTIALS (CM-1)
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  INPUT : (I*4)  NA()    = PRINCIPAL QUANTUM NUMBER OF VALENCE ELECTRON
C  INPUT : (R*8)  WAA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C                           1ST DIMENSION - LEVEL INDEX
C                           2ND DIMENSION - SEQUENCE MEMBER INDEX
C  INPUT : (I*4)  IZS     = NUCLEAR CHARGE OF NEUTRAL SEQUENCE MEMBER
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE OF SELECTED ION
C
C  OUTPUT: (R*8)  BWNO    = IONISATION ENERGY OF SELECTED ION (CM-1)
C  OUTPUT: (R*8)  WAO()   = LEVEL ENERGIES RELATIVE TO LOWEST (CM-1)
C
C          (I*4)  I       = GENERAL USE.
C          (I*4)  IENDN   = SPLINE END CONDITION SWITCH AT LAST POINT
C          (I*4)  IEND1   = SPLINE END CONDITION SWITCH AT FIRST POINT
C          (I*4)  IFORMS  = SPLINE INDEPENDENT VARIABLE FORM SWITCH
C          (I*4)  K       = GENERAL USE.
C
C          (R*8)  C1(,)   = 1ST SPLINE COEFFICIENT MATRIX
C          (R*8)  C2(,)   = 2ND SPLINE COEFFICIENT MATRIX
C          (R*8)  C3(,)   = 3RD SPLINE COEFFICIENT MATRIX
C          (R*8)  C4(,)   = 4TH SPLINE COEFFICIENT MATRIX
C          (R*8)  DY      = GRADIENT OF SPLINE AT POINT
C          (R*8)  ENI     = LEVEL PRINCIPAL QUANTUM NUMBER
C          (R*8)  EN1     = LOWEST LEVEL PRINCIPAL QUANTUM NUMBER
C          (R*8)  E1I     = LEVEL ENERGY (RYDBERGS)
C          (R*8)  B3FORM  = EXTERNAL FUNCTION (SEE SUBROUTINE SECTION)
C          (R*8)  REN     = GENERAL USE
C          (R*8)  XI      = GENERAL USE
C          (R*8)  XSA()   = SPLINE INDEPENDENT VARIABLE AT KNOTS
C          (R*8)  Y       = SPLINE INTERPOLATED VALUE
C          (R*8)  YSA()   = SPLINE DEPENDENT VARIABLE AT KNOTS
C          (R*8)  Z1      = CURRENT ION CHARGE +1
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B2GSPC     ADAS      GENERATES SPLINE COEFFICIENT MATRICES
C          B2NFAS     ADAS      SETS SPLINE ASYMPTOTIC CONDITIONS
C          B3FORM     ADAS      INDEPENDENT VARIABLE FUNCTION FOR SPLINE
C          B2SORT     ADAS      SORTS VECTOR INTO INCREASING ORDER
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    08/01/95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C               - REPLACED CALLS TO NSORT ROUTINE WITH CALLS TO B2SORT.
C                 NSORT IS USED TO SORT A REAL ARRAY AND ASSOCIATED
C                 INTEGER ARRAY WHEREAS WHAT WAS BEING PASSED TO IT WAS
C                 A REAL ARRAY AND ANOTHER, ASSOCIATED REAL ARRAY. B2SORT
C                 TAKES 2 REAL ARRAYS AS INPUT AND PERFORMS A BUBBLE SORT
C                 ON THEM.
C
C VERSION: 1.2                          DATE: 23-04-07
C MODIFIED: ALLAN WHITEFORD
C               - RENAMED FORM SUBROUTINE TO B3FORM.
C
C-----------------------------------------------------------------------
      INTEGER   NDSPLN
C-----------------------------------------------------------------------
      PARAMETER ( NDSPLN = 10 )
C-----------------------------------------------------------------------
      INTEGER   NDLEV       , IZDIMD    ,
     &          IL          ,
     &          IZMAX       , IZS            , IZ0
      INTEGER   I           , K
      INTEGER   IEND1       , IENDN          , IFORMS
C-----------------------------------------------------------------------
      REAL*8    Z1          ,
     &          BWNO
      REAL*8    EN1         , ENI            , REN        , XI         ,
     &          E1I
      REAL*8    Y           , DY             , B3FORM
C-----------------------------------------------------------------------
      INTEGER   IZA(IZDIMD) , IZ0A(IZDIMD)   , IZ1A(IZDIMD)
      INTEGER   IA(NDLEV)   , NA(NDLEV)
C-----------------------------------------------------------------------
      REAL*8    Z1A(IZDIMD)         , BWNOA(IZDIMD)
      REAL*8    WAA(NDLEV,IZDIMD)
      REAL*8    WAO(NDLEV)
      REAL*8    XSA(NDSPLN)         , YSA(NDSPLN)
      REAL*8    C1(NDSPLN,NDSPLN-1) , C2(NDSPLN, NDSPLN-1)    ,
     &          C3(NDSPLN,NDSPLN-1) , C4(NDSPLN, NDSPLN-1)
C-----------------------------------------------------------------------
      EXTERNAL  B3FORM
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  INTERPOLATE IONISATION ENERGIES TO REQUIRED Z1                       
C-----------------------------------------------------------------------
C
       Z1 = IZ0-IZS+1
C
       EN1 = NA(1)                                                      
       REN=1.0D0/(EN1*EN1)                                              
C
       XI=1.0/Z1                                                        
       DO 31 K=1,IZMAX                                                  
         XSA(K)=1.0/Z1A(K)                                              
         YSA(K)=BWNOA(K)/(1.09737D5*Z1A(K))-Z1A(K)*REN                  
   31  CONTINUE
C
       CALL B2SORT(XSA,YSA,IZMAX)                                        
C
       IEND1 = 3                                                        
       IENDN = 3                                                        
C
       CALL B2GSPC( XSA   , IZMAX , C1    , C2    , C3    , C4    )     
       CALL B2NFAS( XI    , XSA   , IZMAX , YSA   , Y     , DY    ,
     &              C1    , C2    , C3    , C4    , B3FORM, IFORMS
     &            )
C
       BWNO = 1.09737D5*(Y+Z1*REN)*Z1                                   
C
C-----------------------------------------------------------------------
C  INTERPOLATE LEVEL ENERGIES TO REQUIRED Z1                            
C-----------------------------------------------------------------------
C
       XI=1.0D0/Z1                                                      
       EN1 = NA(1)                                                      
C
       DO 50 I = 1,IL
         ENI=NA(I)                                                      
         REN=DABS(1.0D0/(EN1*EN1)-1.0D0/(ENI*ENI))                      
C
         DO 33 K=1,IZMAX                                                
           XSA(K)=1.0D0/Z1A(K)                                          
           YSA(K)=WAA(I,K)/(1.09737D5*Z1A(K))-Z1A(K)*REN                
   33    CONTINUE
C
         CALL B2SORT(XSA,YSA,IZMAX)                                      
C
         IEND1=3                                                        
         IENDN=3                                                        
C
         CALL B2GSPC( XSA   , IZMAX , C1    , C2    , C3    , C4    )   
         CALL B2NFAS( XI    , XSA   , IZMAX , YSA   , Y     , DY    ,
     &                C1    , C2    , C3    , C4    , B3FORM, IFORMS
     &              )
C
         E1I=(Y+Z1*REN)*Z1                                              
         WAO(I)=1.09737D5*E1I                                           
C
   50  CONTINUE
C
       RETURN
C
C-----------------------------------------------------------------------
C                                                                       
      END
