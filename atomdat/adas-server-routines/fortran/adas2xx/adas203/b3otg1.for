CX - UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3otg1.for,v 1.4 2004/07/06 11:20:01 whitefor Exp $ Date $Date: 2004/07/06 11:20:01 $
CX
      SUBROUTINE B3OTG1( DATE   ,
     &                   NDTRN  , IZDIMD ,
     &                   SEQSYM , IZS    , TITLE , DSFULL ,
     &                   IZMAX  , ZA     ,
     &                   IZ0    , ISTRN , CTSTR  ,
     &                   N1     , N2     , WDEA   , WDE
     &                 )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3OTG1 *********************
C
C  PURPOSE:  COMMUNICATES GRAPHICS DATA TO IDL
C
C            PROVIDES  GRAPH OF INTERPOLATED TRANSITION ENERGY
C            INCLUDING COMPARATIVE SOURCE DATA FROM
C            GENERAL Z EXCIT. FILE
C
C            PLOT IS (TR. WAVE NO./109737.0*Z1*Z1/(1/N1**2-1/N2**2))/Z1)
C                    VERSUS (Z1)      IF N1 < N2 OTHERWISE
C                    (TR. WAVE NO.)/Z1  VERSUS  (Z1)
C
C  CALLING PROGRAM: ADAS203
C
C  SUBROUTINE:
C
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT : (I*4)  IZDIMD  = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT : (C*2)  SEQSYM  = ISOELECTRONIC SEQUENCE SYMBOL
C  INPUT : (I*4)  IZS     = NUC. CHG. OF ISOELEC. SEQUENCE 1ST MEMBER
C  INPUT : (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*44) DSFULL  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZMAX   = NUMBER OF IONS (STAGES) IN SOURCE FILE
C  INPUT : (R*8)  ZA()    = RECOMBINING ION CHARGES IN SOURCE FILE
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE OF SELECTED ION
C  INPUT : (I*4)  ISTRN   = SELECTED TRANSITION INDEX FROM INPUT FILE
C  INPUT : (C*18) CTSTR   = SELECTED TRANSITION TITLE STRING
C
C  INPUT : (I*4)  N1      = LOWER PRINC. QU. NO. OF SELECTED TRANS.
C  INPUT : (I*4)  N2      = UPPER PRINC. QU. NO. OF SELECTED TRANS.
C  INPUT : (R*8)  WDEA(,) = TRANSITION WAVE NOS. FOR SOURCE IONS
C  INPUT : (R*8)  WDE     = TRANSITION WAVE NO. OF INTERPOLATED ION
C
C          (I*4)  NDIM2   = PARAMETER = MAXIMUM NUMBER OF IONS
C                           (MUST NOT BE LESS THAN 'IZDIMD')
C          (I*4)  NGION   = PARAMETER = MAXIMUM NUMBER OF IONS
C                           WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN  = PARAMETER = IN DEFAULT GRAPH SCALING IS THE
C                           MINIMUM Y-VALUE THAT IS ALLOWED.
C                           (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IZ      = STAGE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IZ1     = ION CHARGE +1
C          (I*4)  IMMAX   = MINIMUM OF: NO. OF STAGES OR NGION'
C
C          (R*4)  X()     = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                           ELECTRON TEMPERATURES
C          (R*4)  X0()    = GENERAL USE
C                           1ST DIMENSION = 1
C          (R*4)  Y(,)    = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                           INPUT SCALED TRANSITION ENERGIES
C                           1ST DIMENSION = ION INDEX
C                           2ND DIMENSION = 1
C          (R*4)  Z(,)    = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                           OUTPUT INTERPOLATED TRANSITION ENERGY
C                           1ST DIMENSION = 1
C                           2ND DIMENSION = 1
C          (R*4)  Z1      = ION CHARGE+1
C
C          (C*80) ISPEC   = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*12) ENAME   = ELEMENT NAME
C          (C*3)  CNAM()  = 3 BYTE STRING FOR INTERP. AND APPROX. COEFFT
C          (C*13) DNAME   = '       DATE: '
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*30) HEAD1   = HEADING FOR LEVEL ASSIGNMENTS
C          (C*30) STRG1   = HEADING FOR LEVEL ASSIGNMENTS
C          (C*30) STRGA() = STRING OF GENERAL Z FILE ELEMENT CHARGES
C                           1ST DIM. - MAXIMUM NO OF ELEMENTS (NDIM2)
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXELEM      ADAS      SETS UP ELEMENT NAME AS STRING
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    03/01/95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 21-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.3				DATE: 23-05-96
C MODIFIED: WILLIAM OSBORN
C	 	- USED MODULUS OF ENERGY DIFFERENCE :
C  ( UPDATE: 21/05/96  HP SUMMERS - PLOT MODULUS(SCALED WAVE NUMBER) +
C				 GHZERO TO AVOID PROBLEMS OF LEVEL ORDER
C				 CHANGE ALONG ISOELECTRONIC SEQUENCE )
C VERSION: 1.4				DATE: 10-07-96
C MODIFIED: WILLIAM OSBORN
C	 	- REMOVED REFERENCE TO I OUTSIDE OF LOOP 'FOR I=1,IZMAX'
C		  Z(I,1) = ABS(Z(I,1))+ ...-> Z(1,1) = ABS(Z(I,1))+ ...
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDIM2    , NGION
      INTEGER   PIPEIN   , PIPEOU
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      REAL*4    CUTMIN   , GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM2=15         , NGION = 55 )
      PARAMETER ( CUTMIN = 1.0E-20 , GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   NDTRN    , IZDIMD
      INTEGER   IZMAX    ,
     &          IZ0      , IZS      , IZ1      , ISTRN    ,
     &          N1       , N2
      INTEGER   IZ       , 
     &          IMMAX    , I        
C-----------------------------------------------------------------------
      REAL*8    WDE      , Z1
C-----------------------------------------------------------------------
      CHARACTER ELEM*2   , TITLE*40 , DSFULL*44, ENAME*12
      CHARACTER SEQSYM*2 , XFESYM*2 , CTSTR*18
      CHARACTER GRID*1   , PIC*1    , C3BLNK*3 , DATE*8   ,
     &          FILE0*9  , MNMX0*9  , KEY0*9   , ADAS0*8  ,
     &          DNAME*13 , GNAME*8  ,
     &          XTIT*25  , YTIT*23  ,
     &          HEAD1*30 ,
     &          STRG1*30 ,
     &          ISPEC*80 , GTIT1*40
C-----------------------------------------------------------------------
      REAL*4    X(NDIM2)            , Y(NDIM2,1)          , Z(1,1)
      REAL*4    XO(1)
C-----------------------------------------------------------------------
      CHARACTER KEY(3)*22           , STRGA(NDIM2)*30
      CHARACTER CNAM(2)*3
C-----------------------------------------------------------------------
      REAL*8    ZA(IZDIMD)          , WDEA(NDTRN,IZDIMD)
C-----------------------------------------------------------------------
      DATA ISPEC(1:40)
     &           /'SCALED TRANS. WAVE NO. VS ION CHARGE+1  '/
      DATA (CNAM(IZ),IZ=1,2)
     &           / 'INT' , 'APX' /
      DATA XTIT  /'ION CHARGE +1            '/
      DATA YTIT  /'SCALED TRANS. WAVE NO. '/
      DATA ADAS0 /'ADAS   :'/                ,
     &     FILE0 /'FILE   : '/               ,
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(CROSS - INTERPOL.   )'/  ,
     &     KEY(2)/' (DASH LINE - SOURCE )'/  ,
     &     KEY(3)/'                    ) '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/    ,
     &     C3BLNK/'   '/ 
      DATA DNAME /'       DATE: '/,
     &     GNAME /'SPECIES:'/
      DATA HEAD1 /'------ SEQUENCE MEMBERS ------'/
      DATA STRG1 /'INDX  NUC.CHG. RECD.ION.  ELEM'/
C-----------------------------------------------------------------------
C
      IMMAX = MIN0( NGION , IZMAX )
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
         DO 1 I=1,IZMAX
            STRGA(I)=' '
               IF ( I.LE.IZMAX ) THEN
                   IZ1 = ZA(I) + 0.001
                   IZ  =IZ1-1
                   ELEM = XFESYM(IZ+IZS)
                   WRITE(STRGA(I)(1:30),1003) I,IZ+IZS,IZ,ELEM
               ENDIF
    1    CONTINUE
         STRGA(IZMAX+1)=' '
         IZ = IZ0-IZS
         Z1 = IZ+1
         ELEM = XFESYM(IZ0)
         WRITE(STRGA(IZMAX+2)(1:30),1008) ' *', IZ0,IZ,ELEM
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
      ISPEC(41:80) = TITLE
C
      CALL XXELEM(IZ0,ENAME)
      WRITE(GTIT1,1000) ENAME,SEQSYM,CTSTR
C
C-----------------------------------------------------------------------
C ON FIRST ENTRY: MAKE SURE SET ARRAY BOUNDS ARE VALID
C-----------------------------------------------------------------------
C
            IF (NDIM2.LT.IZDIMD) STOP
     &                   ' B3OTG1 ERROR: ARRAY DIMENSION NDIM2 < IZDIMD'
C
C-----------------------------------------------------------------------
C SCALE AND CONVERT INPUT VALUES TO REAL*4 ARRAY
C-----------------------------------------------------------------------
C
         DO 2 I =1,IZMAX
            X(I)   = REAL(ZA(I))
            IF (N1.LT.N2) THEN
                Y(I,1) = WDEA(ISTRN,I)/(109737.0*ZA(I)**2*
     &                   (1.0/N1**2-1.0/N2**2))
            ELSE
                Y(I,1) = WDEA(ISTRN,I)/ZA(I)
            ENDIF

	    Y(I,1) = ABS(Y(I,1)) + GHZERO
 
    2    CONTINUE
C
         XO(1)=REAL(Z1)
         IF(N1.LT.N2)THEN
             Z(1,1)=WDE/(109737.0*Z1**2*(1.0/N1**2-1.0/N2**2))
         ELSE
             Z(1,1)=WDE/Z1
         ENDIF

         Z(1,1) = ABS(Z(1,1)) + GHZERO
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) IZMAX
      CALL XXFLSH(PIPEOU)
      DO 200, IZ=1, IZMAX
          WRITE(PIPEOU, *) X(IZ)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, *) Y(IZ,1)
          CALL XXFLSH(PIPEOU)
200   CONTINUE
      WRITE(PIPEOU, *) XO(1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) Z(1,1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A12)') ENAME
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A2)') SEQSYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A18)') CTSTR
      CALL XXFLSH(PIPEOU)
      DO 201, I=1,IZMAX
          IZ1 = ZA(I) + 0.001
          IZ  =IZ1-1
          ELEM = XFESYM(IZ+IZS)
          WRITE(PIPEOU, *) IZ+IZS
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, *) IZ
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, '(A2)') ELEM
          CALL XXFLSH(PIPEOU)
201   CONTINUE
      IZ = IZ0-IZS
      ELEM = XFESYM(IZ0)
      WRITE(PIPEOU, *) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) IZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A2)') ELEM
      CALL XXFLSH(PIPEOU)

 1000 FORMAT(1A12,2X,'SEQ:',1A2,2X,1A18)
 1002 FORMAT(/1X,31('*'),' B3OTG1 MESSAGE ',31('*')/
     &       1X,'ION CHARGE: ',I3,
     &       4X,'NO GRAPH WILL BE OUTPUT BECAUSE:')
 1003 FORMAT(I2,6X,I3,6X,I3,6X,1A2,2X)
 1004 FORMAT(1X,'ALL VALUES ARE BELOW THE CUTOFF OF ',1PE10.3)
 1005 FORMAT(1X,'A SERIOUS ERROR EXISTS IN THE DATA OR B3OTG1')
 1006 FORMAT(1X,31('*'),' END OF MESSAGE ',31('*'))
 1008 FORMAT(1A2,6X,I3,6X,I3,6X,1A2,2X)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
