CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3wr10.for,v 1.6 2004/07/06 11:21:35 whitefor Exp $ Date $Date: 2004/07/06 11:21:35 $
CX
      SUBROUTINE B3WR10( IUNIT  , DSFULL  ,
     &                   NDLEV  , NDTRN   , NDTEM  ,
     &                   IL     , ITRAN   ,
     &                   IZS    , IZ0     ,
     &                   IA     , CSTRGA  , NA     ,
     &                   ISA    , ILA     , XJA    ,
     &                   I1A    , I2A     ,
     &                   IEC1A  , IAC1A   , IAC2A  ,
     &                   FAC2A  , IGC1A   , FGC2A  ,
     &                   LBPTS  , LADJA   ,
     &                   MAXT   , TOA     ,
     &                   BWNO   , WAO     , AVAL   , SCOM ,
     &                   DATE   , USERID  , DSCOPS
     &                 )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B3WR10 *******************
C
C  PURPOSE:  TO OUTPUT DATA TO A SPECIFIC ION (COPASE) FILE
C
C  CALLING PROGRAM: ADAS203
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT NUMBER FOR RESULTS
C  INPUT : (I*4)  NDLEV   = MAX. NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTRN   = MAX. NO. OF TRANSITIONS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM OF INPUT DATA FILE TEMPS
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY INDEX LEVELS.
C  INPUT : (I*4)  ITRAN   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)  IZS     = NUCLEAR CHARGE OF NEUTRAL SEQ. MEMBER
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE OF SELECTED ION
C  INPUT : (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  NA()    = PRINCIPAL QUANTUM NUMBER OF VALENCE ELECTRON
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (I*4)  I1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)  I2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C
C  INPUT : (I*4)  IEC1A() = TRANSITION ENERGY INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=>1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  IAC1A() = TRANSITION PROB. INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  IAC2A() = TRANSITION TYPE
C                           (1=>DIPOLE, 2=>NON-DIPOLE, 3=>SPIN CHANGE,
C                            4=>OTHER)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  FAC2A() = TRANSITION PROB. Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  IGC1A() = UPSILON INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*4)  FGC2A() = UPSILON Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (L*4)  LBPTS() = .FALSE. => BAD POINT OPT. NOT SET FOR TRANS.
C                         = .TRUE.  => BAD POINT OPT. SET FOR TRANS.
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (L*4)  LADJA() = .FALSE. => TRANSITION NOT ADJUSTED
C                                      (DEFAULT SETTINGS USED)
C                         = .TRUE.  => TRANSITION ADJUSTED
C                                      (DEFAULT SETTINGS MODIFIED)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (I*8)  MAXT    = NUMBER OF OUTPUT TEMPERATURES
C  INPUT : (R*8)  TOA()   = OUTPUT TEMPERATURES (K)
C
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C  INPUT : (R*8)  WAO()   = ENERGY OF LEVELS (CM-1)
C                           1ST DIMENSION - LEVEL NUMBER
C  INPUT : (R*8)  WDE()   = ENERGY OF TRANSITIONS (CM-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (R*8)  AVAL()  = A-VALUE OF TRANSITIONS (SEC-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  INPUT : (R*8)  SCOM(,) = SELECTED TRANSITION GAMMA VALUES:
C                            GAMMA VALUES
C                           1ST DIMENSION - OUTPUT TEMPERATURE
C                           2ND DIMENSION - TRANSITION NUMBER
C  INPUT : (C*8)  DATE    = CURRENT DATE
C  INPUT : (C*20) USERID  = USER ID
C  INPUT : (C*80) DSCOPS  = OUTPUT FILE NAME
C
C
C          (I*4) I         = GENERAL USE
C          (I*4) PIPEIN    = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XFESYM     ADAS      FETCHES ELEMENT SYMBOL
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    10/01/95
C
C UPDATE:  03/07/95 - HPS  ALTER LENGTH OF CSTRGA ARRAY TO 18 AND
C                          USE NEW POSITION FOR INPUT/OUTPUT
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND 
C               - CHANGED SO THAT USERID IS READ FROM THE UNIX PIPE.
C               - ALSO MODIFIED FILENAME LENGTHS AND OUTPUT FORMATS
C                 SLIGHTLY
C
C VERSION: 1.3                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND
C               - MOVED USERID AND DATE TO INPUT PARAMETERS
C
C VERSION: 1.4                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND
C               - ADDED OUTPUT FILENAME DSCOPS AS AN INPUT PARAMETER
C
C VERSION: 1.5                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND
C               - ADDED OPENING AND CLOSING OF OUTPUT FILE
C
C VERSION: 1.6                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C-----------------------------------------------------------------------
      INTEGER     NDOUT        , L3           , PIPEIN
C-----------------------------------------------------------------------
      PARAMETER ( NDOUT = 14   , L3 = 3       , PIPEIN = 5)
C-----------------------------------------------------------------------
      INTEGER     IUNIT
      INTEGER     NDLEV        , NDTRN        , NDTEM
      INTEGER     IZ           , IZ0          , IZ1       , IZS      ,
     &            I            ,
     &            IL           , IT           , ITRAN     , 
     &            MAXT         , NTOUT
C-----------------------------------------------------------------------
      REAL*8      BWNO         , Z1
C-----------------------------------------------------------------------
      CHARACTER   DATE*8       , DSFULL*80 , USERID*20  ,
     &            ESYM*2       , XFESYM*2
      CHARACTER   CLINE*133    , BLANKS*133   , CNUM*9
      CHARACTER   DSCOPS*80 
C-----------------------------------------------------------------------
      INTEGER     IA(NDLEV)    , NA(NDLEV)    , ISA(NDLEV)   ,
     &            ILA(NDLEV)
      INTEGER     I1A(NDTRN)   , I2A(NDTRN)   ,
     &            IEC1A(NDTRN) , IAC1A(NDTRN) ,
     &            IAC2A(NDTRN) , IGC1A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8      FAC2A(NDTRN)        , FGC2A(NDTRN)
      REAL*8      XJA(NDLEV)   , WAO(NDLEV)   , AVAL(NDTRN)
      REAL*8      TOA(NDTEM)
      REAL*8      SCOM(NDTEM,NDTRN)
C-----------------------------------------------------------------------
      LOGICAL     LADJA(NDTRN)        ,
     &            LBPTS(NDTRN)
C-----------------------------------------------------------------------
      CHARACTER   CSTRGA(NDLEV)*18
C-----------------------------------------------------------------------
      DATA        BLANKS/' '/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  OPEN OUTPUT FILE
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNIT, FILE=DSCOPS, STATUS='UNKNOWN')
C
C-----------------------------------------------------------------------
C  PREPARE AND OUTPUT FIRST INFORMATION LINE
C-----------------------------------------------------------------------
C
           ESYM = XFESYM(IZ0)
           IZ   = IZ0-IZS
           IZ1  = IZ+1
           WRITE(CLINE,1000)ESYM,'+',IZ,IZ0,IZ1,BWNO
           WRITE(IUNIT,1001)CLINE
C
C-----------------------------------------------------------------------
C  OUTPUT ENERGY LEVEL LINES
C-----------------------------------------------------------------------
C
           DO 10 I = 1,IL
             WRITE(CLINE,1002)I,CSTRGA(I),ISA(I),ILA(I),XJA(I),WAO(I)
             WRITE(IUNIT,1001)CLINE
   10      CONTINUE
C
           CLINE = BLANKS
           CLINE(4:5) = '-1'
           WRITE(IUNIT,1001)CLINE
C
C-----------------------------------------------------------------------
C  OUTPUT TEMPERATURE LINE
C-----------------------------------------------------------------------
C
           NTOUT = MIN0(MAXT,NDOUT)
           Z1    = IZ1
C
           CLINE = BLANKS
           WRITE(CLINE,1003)Z1,L3
C
           DO 20 IT = 1,NTOUT
             WRITE(CNUM,1004)TOA(IT)
             CLINE(8*IT+9:8*IT+13) = CNUM(1:5)
             CLINE(8*IT+14:8*IT+16)= CNUM(7:9)
   20     CONTINUE
C
          WRITE(IUNIT,1001)CLINE
C
C-----------------------------------------------------------------------
C  OUTPUT TRANSITION LINES
C-----------------------------------------------------------------------
C
          DO 40 I=1,ITRAN
            CLINE = BLANKS
            WRITE(CLINE,1005)I2A(I),I1A(I)
            WRITE(CNUM,1004)AVAL(I)
            CLINE(9:13) = CNUM(1:5)
            CLINE(14:16)= CNUM(7:9)
C
            DO 30 IT=1,NTOUT
              WRITE(CNUM,1004)SCOM(IT,I)
              CLINE(8*IT+9:8*IT+13) = CNUM(1:5)
              CLINE(8*IT+14:8*IT+16)= CNUM(7:9)
   30       CONTINUE
C
            WRITE(IUNIT,1001)CLINE
C
   40     CONTINUE
C
          CLINE = BLANKS
          CLINE(3:4) = '-1'
          WRITE(IUNIT,1001)CLINE
          CLINE(7:8) = '-1'
          WRITE(IUNIT,1001)CLINE
C
C-----------------------------------------------------------------------
C  OUTPUT INFORMATION STRINGS
C-----------------------------------------------------------------------
C
          WRITE(IUNIT,1006)
          WRITE(IUNIT,1007)DSFULL
C
          DO 60 I=1,ITRAN
            IF(LADJA(I))THEN
               WRITE(IUNIT,1008)I,IEC1A(I),IAC1A(I),IAC2A(I),FAC2A(I),
     &                          IGC1A(I),FGC2A(I),LBPTS(I)
            ENDIF
   60     CONTINUE
C
          WRITE(IUNIT,1009)USERID,DATE
          WRITE(IUNIT,1006)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A2,1A1,I2,2I10,F15.0)
 1001 FORMAT(1A133)
 1002 FORMAT(I5,1X,1A18,'(',I1,')',I1,'(',F4.1,')',F17.1)
 1003 FORMAT(F5.2,I5)
 1004 FORMAT(1PD9.2)
 1005 FORMAT(2I4)
 1006 FORMAT('C',79('-'))
 1007 FORMAT('C ',/,
     &       'C  Specific ion file created from general Z excit. file',/
     &       'C ',/,
     &       'C  Processing code: ADAS203',/,
     &       'C ',/,
     &       'C  General Z excit. source file: ',1A80,/,
     &       'C ',/,
     &       'C  Scaling parameter adjustments from source file ',
     &           'defaults: ',/,
     &       'C  Indx   IEC1   IAC1   IAC2   FAC2   IGC1   FGC2   LBPTS'
     &          ,/,
     &       'C  ----   ----   ----   ----  ------  ----  ------  -----'
     &      )
 1008 FORMAT('C',I6,5X,I1,6X,I1,6X,I1,3X,F6.2,4X,I1,3X,F6.2,4X,L1)
 1009 FORMAT('C ',/,
     &       'C  Prepared by: ',1A20,/,
     &       'C  Date       : ',1A8)
C
C-----------------------------------------------------------------------
C
      CLOSE(IUNIT)
C
      RETURN
      END
