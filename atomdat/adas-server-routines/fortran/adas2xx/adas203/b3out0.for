CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3out0.for,v 1.3 2004/07/06 11:20:33 whitefor Exp $ Date $Date: 2004/07/06 11:20:33 $
CX
      SUBROUTINE B3OUT0( IUNIT  , DATE   , DSFULL ,
     &                   IZDIMD , NDLEV  , NDTRN  , NDTEM  ,
     &                   SEQSYM , IZMAX  , IZ0A   ,
     &                   IZS    , IZ0    , BWNO   ,
     &                   IL     , ITRAN  ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA    ,
     &                   WAO    ,
     &                   NV     , SCEFA  ,
     &                   MAXT   , TOA    ,
     &                   I1A    , I2A    ,
     &                   IEC1A  , IAC1A  , IAC2A  ,
     &                   FAC2A  , IGC1A  , FGC2A  ,
     &                   LBPTS  , LADJA  , CADAS  , DSOUT0
     &                 )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3OUT0 *********************
C
C  PURPOSE: TO OUTPUT SUMMARY DATA ON INTERPOLATED SPECIFIC ION FILE
C           AND SOURCE GENERAL Z EXCITATION FILE TO STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS203
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  IUNIT  = OUTPUT STREAM NUMBER
C  INPUT :  (C*8)  DATE   = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (C*80) DSFULL = INPUT COPASE DATA SET NAME
C
C  INPUT :  (C*2)  SEQSYM = ISOELECTRONIC SEQUENCE SYMBOL
C  INPUT :  (I*4)  IZS    = NUCLEAR CHARGE OF NEUTRAL SEQ. MEMBER
C  INPUT :  (I*4)  IZ0    = NUCLEAR CHARGE OF SELECTED ION
C  INPUT :  (R*8)  BWNO   = IONISATION POTENTIAL (CM-1)
C
C  INPUT :  (I*4)  ITRAN  = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C
C  INPUT :  (I*4)  IL     = NUMBER OF ENERGY LEVELS
C
C  INPUT :  (I*4)  IA()   = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT :  (I*4)  ISA()  = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT :  (I*4)  ILA()  = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT :  (R*8)  XJA()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  WAO()  = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C  INPUT :  (C*80) DSOUT0 = FILENAME FOR OUTPUT
C
C
C          (R*8)  WN2RYD  = PARAMETER =
C                           WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (R*8)  BRYDO   = IONISATION POTENTIAL (RYDBERGS)
C          (R*8)  BWN     = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           WAVE NUMBERS (CM-1).
C          (R*8)  BRYD    = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           RYDBERGS.
C
C          (I*4)  I       = GENERAL USE
C
C INPUT:   (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    13/01/95
C
C UPDATE:  03/07/95 - HPS  ALTER LENGTH OF CSTRGA ARRAY TO 18 AND
C                          USE NEW POSITION FOR INPUT/OUTPUT
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND
C               - REMOVED CALL TO XXADAS AS THIS IS NOW HANDLED IN THE
C                 ROUTINE B3SPF1 AND CADAS IS PASSED INTO THIS
C                 ROUTINE AS A PARAMETER.
C               - TIDIED UP THE FORMAT OF THE PRINTED OUTPUT
C
C VERSION: 1.3                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND
C               - ENSURED OUTPUT FILE IS CLOSED AT END OF EACH RUN 
C
C-----------------------------------------------------------------------
      REAL*8     WN2RYD        , WN2EV
C-----------------------------------------------------------------------
      PARAMETER( WN2RYD = 9.11269D-06 , WN2EV = 1.23982D-04 )
C-----------------------------------------------------------------------
      INTEGER    IZDIMD        , NDTRN          , NDLEV      , NDTEM
      INTEGER    I
      INTEGER    IUNIT         , IZMAX          ,
     &           IZ            , IZ0            , IZ1        , IZS    ,
     &           ITRAN         ,
     &           IL            , NV             , MAXT
C-----------------------------------------------------------------------
      REAL*8     BWNO          , BWN            , BRYDO      , BRYD    ,
     &           ER
C-----------------------------------------------------------------------
      CHARACTER  SEQSYM*2      , DATE*8         , XFESYM*2   ,
     &           DSFULL*80     , CADAS*80       , DSOUT0*80
C-----------------------------------------------------------------------
      INTEGER    IA(IL)        , ISA(IL)        , ILA(IL)
      INTEGER    IZ0A(IZDIMD)
      INTEGER    I1A(NDTRN)    , I2A(NDTRN)     ,
     &           IEC1A(NDTRN)  , IAC1A(NDTRN)   ,
     &           IAC2A(NDTRN)  , IGC1A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     XJA(IL)       , WAO(IL)
      REAL*8     SCEFA(NV)     , TOA(MAXT)
      REAL*8     FAC2A(NDTRN)  , FGC2A(NDTRN)
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18
C-----------------------------------------------------------------------
      LOGICAL     LADJA(NDTRN)        ,
     &            LBPTS(NDTRN)
C-----------------------------------------------------------------------
C
      BRYDO = WN2RYD * BWNO
C
C-----------------------------------------------------------------------
C OPEN THE OUTPUT FILE
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNIT, FILE=DSOUT0, STATUS='UNKNOWN')
C
C-----------------------------------------------------------------------
C GATHER ADAS HEADER AND WRITE IT OUT
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1000) CADAS(2:80)
C
C-----------------------------------------------------------------------
C OUTPUT ION AND ENERGY LEVEL DETAILS
C-----------------------------------------------------------------------
C
      IZ  = IZ0-IZS
      IZ1 = IZ+1
      WRITE(IUNIT,1001) 'GENERAL Z EXCIT. FILE TO SPECIFIC ION FILE ',
     &                  'ADAS203',DATE
      WRITE(IUNIT,1002) DSFULL
      WRITE(IUNIT,1003) XFESYM(IZ0), '+' , IZ , IZ0 , IZ1 , BWNO , BRYDO
      WRITE(IUNIT,1004)
C
         DO 1 I=1,IL
            BWN  = BWNO  - WAO(I)
            ER   = WN2RYD*WAO(I)
            BRYD = BRYDO - ER
            WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                        ISA(I) , ILA(I)    , XJA(I) ,
     &                        WAO(I) , ER        ,
     &                        BWN    , BRYD
    1    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT 'INPUT GENERAL Z FILE' INFORMATION
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1008) SEQSYM, ITRAN  , IZMAX
      DO 5 I=1,IZMAX
        WRITE(IUNIT,1009)I,IZ0A(I), IZ0A(I)-IZS, XFESYM(IZ0A(I))
    5 CONTINUE
C
      WRITE(IUNIT,*)' '
      WRITE(IUNIT,1010)
      DO 10 I=1,NV
        WRITE(IUNIT,1011)I,SCEFA(I)*IZ1**2,WN2EV*SCEFA(I)*IZ1**2,
     &                   SCEFA(I)
   10 CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT FINAL SPECIFIC ION FILE INFORMATION
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1012)
C
      WRITE(IUNIT,1013)
      DO 15 I=1,MAXT
        WRITE(IUNIT,1011)I,TOA(I),WN2EV*TOA(I),TOA(I)/IZ1**2
   15 CONTINUE
c
      WRITE(IUNIT,1014)
      DO 20 I=1,ITRAN
        IF(LADJA(I))THEN
           WRITE(IUNIT,1015)I,IEC1A(I),IAC1A(I),IAC2A(I),FAC2A(I),
     &                      IGC1A(I),FGC2A(I),LBPTS(I)
        ENDIF
   20 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,16('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,17('*')/)
 1002 FORMAT(1H ,'INPUT GENERAL Z EXCIT. FILE NAME: ',A80/)
 1003 FORMAT(1H ,1X,'SELECTED',6X,'NUCLEAR',8X,'RECOMBINING',6X,
     &           7('-'),' IONIZATION POTENTIAL ',7('-')/
     &           4X,'ION',8X,'CHARGE (Z0)',4X,'ION CHARGE (Z1)',
     &        4X,'(wave number <cm-1>)',5X,'(rydbergs)'/
     &        1X,84('-')/
     &        4X,1A2,1A1,I2,10X,I3,14X,I3,10X,F15.0,5X,F15.7//)
 1004 FORMAT(1H ,55('-'),' ENERGY LEVELS ',55('-')/
     &        1X,'INDEX',3X,'CONFIGURATION',5X,'(2S+1)L(J)',7X,
     &           5('-'),' ENERGY RELATIVE TO LEVEL 1 ',6('-'),4X,
     &           'ENERGY RELATIVE TO IONISATION POTENTIAL'/
     &       45X,2( '(wave number <cm-1>)',5X,'(rydbergs)' ,8X )/
     &        1X,125('-'))
 1005 FORMAT( 1X,I4,1X,A18,3X,'(',I1,')',I1,'(',F6.1,')',
     &        2(6X,F15.0,5X,F15.7,2X) )
 1006 FORMAT(1H /1H ,'- INPUT TEMPERATURES FROM GENERAL Z FILE (TE) -'/
     &            1X,'INDX',4X,'(KELVIN)',8X,'(EV)',8X,'(REDUCED)'/
     &            1X,48('-'))
 1007 FORMAT(2X,I2,1P,3(4X,D10.2))
 1008 FORMAT(1H /1H ,'INPUT GENERAL Z FILE INFORMATION:'/1H ,33('-')/
     &       1H ,'ISOELECTRONIC SEQUENCE NEUTRAL MEMBER    =',2X,1A2/
     &       1H ,'NUMBER OF ELECTRON IMPACT TRANSITIONS    =',1X,I3/
     &       1H ,'NUMBER OF IONS IN GENERAL Z FILE         =',1X,I3//
     &       1H ,'  ------------ INPUT MEMBERS --------------'/
     &       1H ,'  INDEX    NUCLEAR    RECOMBINED    ELEMENT'/
     &       1H ,'           CHARGE     ION CHARGE    SYMBOL '/
     &       1H ,'  -----------------------------------------')
 1009 FORMAT(1H ,4X,I1,9X,I2,10X,I2,10X,1A2)
 1010 FORMAT(1H ,'  -- INPUT GENERAL Z TEMPERATURES AT SELECTED ION --'/
     &       1H ,'  INDEX      (Kelvin)        (eV)         (Reduced) '/
     &       1H ,'  --------------------------------------------------')
 1011 FORMAT(1H ,3X,I2,2X,3(4X,1P,D11.3))
 1012 FORMAT(1H /1H ,'OUTPUT SPECIFIC ION FILE INFORMATION:'/
     &       1H ,37('-')/)
 1013 FORMAT(1H ,'  ---- OUTPUT TEMPERATURES FOR SPECIFIC ION FILE ---'/
     &       1H ,'  INDEX      (Kelvin)        (eV)         (Reduced) '/
     &       1H ,'  --------------------------------------------------')
 1014 FORMAT('  ',/,
     &       1H ,'  -- SCALING PARAMETER ADJUSTMENTS FROM SOURCE FILE',
     &           ' DEFAULTS --'/
     &       1H ,'  INDEX  IEC1   IAC1   IAC2   FAC2   IGC1   FGC2   ',
     &           'LBPTS',/,
     &       1H ,'  -------------------------------------------------',
     &           '------------'
     &      )
 1015 FORMAT(' ',I6,5X,I1,6X,I1,6X,I1,3X,F6.2,4X,I1,3X,F6.2,4X,L1)
C-----------------------------------------------------------------------
      CLOSE(IUNIT)
C
      RETURN
      END
