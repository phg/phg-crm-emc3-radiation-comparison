CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3solv.for,v 1.3 2007/04/11 11:13:09 allan Exp $ Date $Date: 2007/04/11 11:13:09 $
CX
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3solv.for,v 1.3 2007/04/11 11:13:09 allan Exp $ Date $Date: 2007/04/11 11:13:09 $
CX
       SUBROUTINE B3SOLV(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,TE2,GAM2,IFAIL)  
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3SOLV *********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 20-03-96
C MODIFIED: TIM HAMMOND 
C               - ALTERED PRINTOUT FROM UNIT 6 TO I4UNIT(-1)
C
C VERSION: 1.3                          DATE: 11-04-07
C MODIFIED: ALLAN WHITEFORD
C               - RENAMED FROM SOLVE.FOR IN ADASLIB/ATOMIC
C
C-----------------------------------------------------------------------
C
       P1(X)=GAM1/GAM2-(DLOG(1.0D0+X)+EEI((1.0D0+X)*1.5789E5*EIJ/TE1))/ 
     &(DLOG(1.0D0+X)+EEI((1.0D0+X)*1.5789E5*EIJ/TE2))                   
       P3(X)=GAM1/GAM2-EE2((1.0D0+X)*1.5789E5*EIJ/TE1)*TE2/             
     &(EE2((1.0D0+X)*1.5789E5*EIJ/TE2)*TE1)                             
       IFAIL=0                                                          
       ICOUNT=1                                                         
       A=0.0D0                                                          
       B=1.0D0                                                          
       GOTO (1,2,3),IXTYP                                               
       GO TO 2                                                          
    1  VA=P1(A)                                                         
       GOTO 4                                                           
    2  FXC2=0.0D0                                                       
C  ALTERED FOR NON DIPOLE  22 AUG 1984                                  
       FXC3=GAM1                                                        
       RETURN                                                           
C  CHANGED A AND B FOR SPIN CHANGE   22 AUG 1984                        
    3  A=-0.95D0                                                        
       B=0.25D0                                                         
       VA=P3(A)                                                         
    4  B=B+B                                                            
       ICOUNT=ICOUNT+1                                                  
       IF(ICOUNT.LE.12)GO TO 31                                         
   32  WRITE(I4UNIT(-1),101)                                           
       IFAIL=1                                                          
       GO TO 2                                                          
  101  FORMAT(1H ,'FAILURE TO FIX 2 POINT APPROX. FORM-NO OPTIMISING')  
   31  GOTO (5,2,6),IXTYP                                               
    5  VB=P1(B)                                                         
       GO TO 30                                                         
    6  VB=P3(B)                                                         
   30  CONTINUE                                                         
       IF(VB*VA.LE.0.0D0)GO TO 7                                        
       A=B                                                              
       VA=VB                                                            
       GO TO 4                                                          
    7  IF(VA.LE.VB)GO TO 8                                              
       V=VA                                                             
       VA=VB                                                            
       VB=V                                                             
       X=A                                                              
       A=B                                                              
       B=X                                                              
    8  X=(A*VB-B*VA)/(VB-VA)                                            
    9  X1=X                                                             
       GOTO (20,2,21),IXTYP                                             
   20  V=P1(X)                                                          
       GO TO 22                                                         
   21  V=P3(X)                                                          
   22  IF(V)11,16,10                                                    
   10  B=X                                                              
       VB=V                                                             
       GO TO 12                                                         
   11  A=X                                                              
       VA=V                                                             
   12  X=0.5D0*(A+B)                                                    
       GO TO (23,2,24),IXTYP                                            
   23  V=P1(X)                                                          
       GO TO 25                                                         
   24  V=P3(X)                                                          
   25  IF(V)14,16,13                                                    
   13  B=X                                                              
       VB=V                                                             
       GO TO 15                                                         
   14  A=X                                                              
       VA=V                                                             
   15  X=(A*VB-B*VA)/(VB-VA)                                            
       IF(DABS(X1-X)/X-0.01D0)16,16,9                                   
C  ALTERED LOWER X CUT FROM 0.0D0  22 AUG 1984                          
   16  IF(X.LE.-1.0D0)GO TO 32                                          
       FXC2=X                                                           
       GO TO (26,2,27),IXTYP                                            
   26  FXC3=GAM1/(S*(DLOG(1.0D0+X)+EEI((1.0D0+X)*1.5789E5*EIJ/TE1)))    
       RETURN                                                           
   27  FXC3=GAM1*(1.0D0+X)*TE1/(1.5789E5*EIJ*EE2((1.0D0+X)*1.5789E5*    
     &EIJ/TE1))                                                         
       RETURN                                                           
      END                                                               
