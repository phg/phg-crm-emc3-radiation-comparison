CX - UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3spf1.for,v 1.1 2004/07/06 11:21:12 whitefor Exp $ Date $Date: 2004/07/06 11:21:12 $
CX
      SUBROUTINE B3SPF1( LGPH, IGRAPH, IPEND, LCOPS, L2FILE, DSOUT0,
     &                   DSCOPS, CADAS, USERID)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS203
C
C  SUBROUTINE:
C
C  OUTPUT:   (L*4)   LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (I*4)   IGRAPH   = INDEX OF GRAPH TO BE PLOTTED:
C                               1=TRANSITION WAVE NO.
C                               2=SCALED A-VALUE
C                               3=SCALED RATE PARAMETER
C  OUTPUT:   (I*4)   IPEND    = 0 => IMMEDIATE EXIT FROM THE PROGRAM
C                             = 1 => BACK TO PROCESSING SCREEN
C                             = 2 => VIEW GRAPHS
C                             = 3 => PRODUCE OUTPUT AND EXIT PROGRAM
C                             = 4 => PRODUCE OUTPUT AND RETURN TO OUTPUT
C                                    SCREEN
C  OUTPUT:   (L*4)   LCOPS    = .TRUE.  => PRODUCE COPASE FILE
C                               .FALSE. => DO NOT PRODUCE COPASE FILE
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => PRODUCE SUMMARY FILE
C                               .FALSE. => DO NOT PRODUCE SUMMARY FILE
C  OUTPUT:   (C*80)  DSOUT0   = FILENAME FOR SUMMARY OUTPUT
C  OUTPUT:   (C*80)  DSCOPS   = FILENAME FOR COPASE OUTPUT
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C  OUTPUT:   (C*20)  USERID   = USER ID
C
C            (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C            (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C            (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C            (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C          XXADAS     IDL_ADAS  GATHERS ADAS HEADER INFORMATION
C
C AUTHOR:  Tim Hammond  (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    8th March 1996
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-03-96
C MODIFIED: TIM HAMMOND 
C               - FIRST RELEASE
C
C-----------------------------------------------------------------------
      CHARACTER    DSOUT0*80   , DSCOPS*80   , CADAS*80    , USERID*20
C-----------------------------------------------------------------------
      LOGICAL      L2FILE      , LGPH        , LCOPS
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      INTEGER      IGRAPH      , IPEND
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) IPEND
      IF (IPEND.EQ.0) THEN
          RETURN
      ELSEIF (IPEND.EQ.1) THEN
          RETURN
      ELSEIF (IPEND.EQ.2) THEN
          READ(PIPEIN,*) ILOGIC
          IGRAPH = ILOGIC + 1
          LGPH = .TRUE.
      ELSEIF ((IPEND.EQ.3).OR.(IPEND.EQ.4)) THEN
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC.EQ.1) THEN
              LCOPS = .TRUE.
              READ(PIPEIN,'(A80)') DSCOPS
              READ(PIPEIN,'(A20)') USERID
          ELSE
              LCOPS = .FALSE.
          ENDIF
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC.EQ.1) THEN
              L2FILE = .TRUE.
              READ(PIPEIN,'(A80)') DSOUT0
              CALL XXADAS(CADAS)
          ELSE
              L2FILE = .FALSE.
          ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
