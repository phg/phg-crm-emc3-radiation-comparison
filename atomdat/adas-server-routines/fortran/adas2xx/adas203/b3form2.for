CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3form2.for,v 1.2 2007/04/23 15:51:16 allan Exp $ Date $Date: 2007/04/23 15:51:16 $
CX
       FUNCTION B3FORM2(I,XI)                                             
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 FUNCTION: B3FORM2 **********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 23-04-07
C MODIFIED: Allan Whiteford
C               - Renamed from form2.for to b3form2.for
C
C-----------------------------------------------------------------------
C
       X=XI/(1.0D0-XI)                                                  
       GO TO (1,2,9,10,1,2,9,10,5,6,9,10),I                             
    1  FORM2=1.0D0                                                      
       GO TO 15                                                         
    2  FORM2=X*EEI(X)                                                   
       GO TO 15                                                         
    5  FORM2=X*X*EE2(X)                                                 
       GO TO 15                                                         
    6  FORM2=X*X*X*EE3(X)                                               
       GO TO 15                                                         
    9  FORM2=1.0D0                                                      
       GO TO 15                                                         
   10  FORM2=0.0D0                                                      
   15  RETURN                                                           
      END                                                               
