CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3spf0.for,v 1.1 2004/07/06 11:21:03 whitefor Exp $ Date $Date: 2004/07/06 11:21:03 $
CX
      SUBROUTINE B3SPF0( REP    , DSFULL)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS203
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  TIM HAMMOND    (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    29-02-96
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 29-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSFULL*80
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOUT
      PARAMETER(  PIPEIN=5      , PIPEOUT=6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
