CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3data.for,v 1.2 2004/07/06 11:19:28 whitefor Exp $ Date $Date: 2004/07/06 11:19:28 $
CX
       SUBROUTINE B3DATA( IUNIT , NDLEV , NDTRN , NDTEM , IZDIMD,
     &                    SEQSYM, IZMAX , Z1A   , IZA   , IZ0A  , IZ1A,
     &                    BWNOA , IL    ,
     &                    IA    , CSTRGA, NA    , ISA   , ILA   , XJA  ,
     &                    WAA   , ITRAN ,
     &                    NVA   , SCEFA ,
     &                    I1A   , I2A   , N1A   , N2A   , W1A   , W2A,
     &                    IEC1A , IAC1A , IAC2A , FAC2A , IGC1A , FGC2A,
     &                    CTSTRA, WDEA  , AVALA , SCOMA , LADJA
     &                  )
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3DATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT GENERAL Z EXCITATION FILE.
C            (ELECTRON IMPACT TRANSITIONS ONLY).
C
C  CALLING PROGRAM: ADAS203
C
C  DATA:
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C
C
C  SUBROUTINE:
C
C          (I*4)  NDZ     = PARAMETER = MAXIMUM NUMBER OF IONS IN
C                                       A GENERAL Z FILE
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C  INPUT : (I*4)  NDTEM   = MAX NUMBER OF INPUT FILE TEMPS.
C  INPUT : (I*4)  IZDIMD  = MAX. NUMBER OF SEQUENCE MEMBERS ALLOWED
C
C  OUTPUT: (C*2)  SEQSYM  = ELEMENT SYMBOL.
C  OUTPUT: (I*4)  IZMAX   = NUMBER OF SEQUENCE MEMBERS
C  OUTPUT: (R*8)  Z1A()   = SEQUNCE RECOMBINING ION CHARGES READ
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (I*4)  IZA()   = SEQUENCE RECOMBINED ION CHARGES
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (I*4)  IZ0A()  = SEQUENCE NUCLEAR CHARGES
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (I*4)  IZ1A()  = SEQUNCE RECOMBINING ION CHARGES READ
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  OUTPUT: (R*8)  BWNOA() = IONISATION POTENTIALS (CM-1)
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C
C  OUTPUT: (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  OUTPUT: (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  OUTPUT: (I*4)  NA()    = PRINCIPAL QUANTUM NUMBER OF VALENCE ELECTRON
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WAA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C                           1ST DIMENSION - LEVEL INDEX
C                           2ND DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C                                            TRANSITIONS.
C
C  OUTPUT: (I*4)  NVA()   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (R*8)  SCEFA(,)= INPUT DATA FILE: Z-SCALED ELEC. TEMPS.(K)
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C
C  OUTPUT: (I*4)  I1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  I2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  N1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER LEVEL PRINCIPAL QUANTUM NUMBER
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  N2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER LEVEL PRINCIPAL QUANTUM NUMBER
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  W1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER LEVEL STATISTICAL WEIGHT
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  W2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER LEVEL STATISTICAL WEIGHT
C                           1ST DIMENSION - TRANSITION NUMBER
C
C  OUTPUT: (I*4)  IEC1A() = TRANSITION ENERGY INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=>1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  IAC1A() = TRANSITION PROB. INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  IAC2A() = TRANSITION TYPE
C                           (1=>DIPOLE, 2=>NON-DIPOLE, 3=>SPIN CHANGE,
C                            4=>OTHER)
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  FAC2A() = TRANSITION PROB. Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  IGC1A() = UPSILON INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (I*4)  FGC2A() = UPSILON Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (C*18) CTSTRA()= TRANSITION DESCRIPTOR
C                           1ST DIMENSION - TRANSITION NUMBER
C  OUTPUT: (R*8)  WDEA()  = TRANSITION ENERGY (CM-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C
C  OUTPUT: (R*8)  AVALA()  = ELECTRON IMPACT TRANSITION:
C                             A-VALUES (SEC-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C                           2ND DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (R*8)  SCOMA(,,)=ELECTRON IMPACT TRANSITION:
C                            GAMMA VALUES
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C                           3RD DIMENSION - SEQUENCE MEMBER INDEX
C  OUTPUT: (L*4)  LADJA()  = .FALSE. INITIALISATION NO TRANSITIONS
C                                    ADJUSTED YET
C                           1ST DIMENSION - TRANSITION NUMBER
C
C
C          (I*4)  I       = GENERAL USE.
C          (I*4)  IAC1    = CURRENT VALUE OF IAC1 PARAMETER
C          (I*4)  IAC2    = CURRENT VALUE OF IAC2 PARAMETER
C          (I*4)  IEC1    = CURRENT VALUE OF IEC1 PARAMETER
C          (I*4)  IGC1    = CURRENT VALUE OF IGC1 PARAMETER
C          (I*4)  ILINE   = ENERGY LEVEL INDEX FOR CURRENT LINE
C          (I*4)  ITEMP   = GENERAL USE.
C          (I*4)  IZ      = CURRENT ION CHARGE
C          (I*4)  IZS     = ISOELECTRONIC SEQUENCE CHARGE (1ST MEMBER)
C          (I*4)  I4EIZ0  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  J       = GENERAL USE.
C          (I*4)  J1      = INPUT DATA FILE - SELECTED TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C          (I*4)  J2      = INPUT DATA FILE - SELECTED TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IPOS    = GENERAL POSITION IN STRING MARKER.
C          (I*4)  K       = GENERAL USE.
C          (I*4)  NI      = CURRENT LOWER LEVEL PRINCIPAL QUANTUM NUMBER
C          (I*4)  NJ      = CURRENT UPPER LEVEL PRINCIPAL QUANTUM NUMBER
C
C          (R*8)  FAC2    = CURRENT VALUE OF FAC2 PARAMETER
C          (R*8)  FGC2    = CURRENT VALUE OF FGC2 PARAMETER
C          (R*8)  WI      = CURRENT LOWER LEVEL STATISTICAL WEIGHT
C          (R*8)  WJ      = CURRENT UPPER LEVEL STATISTICAL WEIGHT
C
C          (C*2)  CHEQSYM = EXTRA STRING USED FOR SYMBOL CHECKING
C          (C*1)  CMINUS  = STRING OF MINUS SYMBOL USED TO CHECK FOR
C                           SINGLE CHARACTER ELEMENT SYMBOLS
C          (C*80) CLINE   = CURRENT ENERGY LEVEL INDEX PARAMETER LINE
C          (C*18) STRING  = GENERAL USE
C
C          (L*4)  LDATA   = IDENTIFIES  WHETHER  THE END OF AN  INPUT
C                           SECTION IN THE DATA SET HAS BEEN LOCATED.
C                           (.TRUE. => END OF SECTION REACHED)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4EIZ0     ADAS      RETURN NUCLEAR CHARGE OF ELEMENT SYMBOL
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    17/08/94
C
C UPDATE:  03/07/95 - HPS  ALTER LENGTH OF CSTRGA ARRAY TO 18 AND
C                          USE NEW POSITION FOR INPUT/OUTPUT
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 29-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 29-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED ERROR MESSAGES
C               - ADDED ABILITY TO HANDLE SINGLE LETTER ELEMENT
C                 SYMBOLS FOLLOWED BY DASH. E.G. IN HYDROGEN LIKE
C                 FILES THE FIRST TWO CHARACTERS IN THE FILE ARE 'H-'
C                 AND THIS WAS CAUSING PROBLEMS FOR I4EIZ0 BEFORE.
C
C-----------------------------------------------------------------------
      INTEGER   NDZ
C-----------------------------------------------------------------------
      PARAMETER ( NDZ = 7 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT , I4EIZ0
      INTEGER   IUNIT       , NDLEV          , NDTRN      , IZDIMD    ,
     &          NDTEM       , IL             , ITRAN      ,
     &          IZMAX       , IZS            , IZ
      INTEGER   ILINE       , ITEMP
      INTEGER   I           , J              , K
      INTEGER   J1          , J2             , NI         , NJ         ,
     &          IEC1        , IAC1           , IAC2       , IGC1
C-----------------------------------------------------------------------
      REAL*4    WI          , WJ             , FAC2       , FGC2
C-----------------------------------------------------------------------
      CHARACTER SEQSYM*2    , CSTRGA(NDLEV)*18
      CHARACTER CLINE*80
      CHARACTER STRING*18   , CTSTRA(NDTRN)*18
      CHARACTER CHEQSYM*2   , CMINUS*1
C-----------------------------------------------------------------------
      LOGICAL   LDATA
C-----------------------------------------------------------------------
      INTEGER   IPOS
      INTEGER   IZA(IZDIMD) , IZ0A(IZDIMD)   , IZ1A(IZDIMD)
      INTEGER   NVA(NDTRN)
      INTEGER   IA(NDLEV)   , NA(NDLEV)      ,
     &          ISA(NDLEV)  , ILA(NDLEV)     ,
     &          I1A(NDTRN)  , I2A(NDTRN)     ,
     &          N1A(NDTRN)  , N2A(NDTRN)     ,
     &          IEC1A(NDTRN), IAC1A(NDTRN)   ,
     &          IAC2A(NDTRN), IGC1A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8    Z1A(IZDIMD)         , BWNOA(IZDIMD)
      REAL*8    SCEFA(NDTEM,NDTRN)
      REAL*8    W1A(NDTRN)          , W2A(NDTRN)           ,
     &          FAC2A(NDTRN)        , FGC2A(NDTRN)
      REAL*8    XJA(NDLEV)          , WAA(NDLEV,IZDIMD)    ,
     &          WDEA(NDTRN,IZDIMD)  , AVALA(NDTRN,IZDIMD)  ,
     &          SCOMA(NDTEM,NDTRN,IZDIMD)
C-----------------------------------------------------------------------
      LOGICAL   LADJA(NDTRN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INPUT ION SPECIFICATIONS AND IDENTIFY SEQUENCE MEMBER CHARGES
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) SEQSYM, IZMAX,(Z1A(I),I=1,NDZ)
      CMINUS = '-'
      IPOS = INDEX( SEQSYM, CMINUS )
      IF (IPOS.EQ.0) THEN
          CHEQSYM = SEQSYM
      ELSE
          CHEQSYM = SEQSYM(1:1)//" "
      ENDIF
      SEQSYM = CHEQSYM
      IZS = I4EIZ0(SEQSYM)
      IF(IZS.LE.0) THEN
          WRITE(I4UNIT(-1),1001) 'SEQUENCE NOT IDENTIFIED'
          WRITE(I4UNIT(-1),1002)
          STOP
      ENDIF
C
C IDENTIFY THE NUMBER OF SEQUENCE CHARGES INPUT
C
         DO 2 IZ=NDZ,1,-1
            IF(Z1A(IZ).GT.0.0D0) GOTO 3
    2    CONTINUE
C
C CHECK THAT AT LEAST ONE VALID Z EXISTS AND TOTAL EQUALS IZMAX
C
    3    IF ((IZ.LE.0).OR.(IZ.NE.IZMAX)) THEN
            WRITE(I4UNIT(-1),1001) 'CHARGES INVALID'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
      DO 10 I = 1,IZMAX
        IZ1A(I) = Z1A(I)+0.001D0
        IZA(I)  = IZ1A(I)-1
        IZ0A(I) = IZA(I)+IZS
   10 CONTINUE
C
C-----------------------------------------------------------------------
C READ IN ENERGY LEVEL SPECIFICATIONS
C-----------------------------------------------------------------------
C
      LDATA=.TRUE.
C
         DO 1 I=1,NDLEV
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I5)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE=-1.
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                                     ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
                     READ (CLINE,1003) IA(I)  , CSTRGA(I) ,
     &                                 NA(I)  , ISA(I)    , ILA(I)    ,
     &                                 XJA(I)
                  ENDIF
            ENDIF
    1    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'COPASE DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C READ IN BINDING WAVE NUMBERS AND LEVEL WAVENUMBERS
C-----------------------------------------------------------------------
C
        READ(IUNIT,1011)(BWNOA(I),I=1,IZMAX)
C
        DO 11 K=1,IL
         READ(IUNIT,1011)(WAA(K,I),I=1,IZMAX)
   11   CONTINUE
C
C-----------------------------------------------------------------------
C READ IN TRANSITION SPECIFICATIONS
C-----------------------------------------------------------------------
C
      LDATA=.TRUE.
      ITRAN=0
C
         DO 6 I=1,NDTRN
 
            IF (LDATA) THEN
 
               READ(IUNIT,1008) J2  , J1  , NJ  , NI  ,WJ  , WI ,ITEMP,
     &                          IEC1, IAC1, IAC2, FAC2,IGC1,FGC2,
     &                          STRING
C
C TRANSITION INPUT INFORMATION IS TERMINATED BY J2=-1
C
                  IF (J2.LE.0) THEN
                     LDATA=.FALSE.
                  ELSE
C
C-----------------------------------------------------------------------
C ASSEMBLE PARAMETERS IN VECTORS
C-----------------------------------------------------------------------
C
                      ITRAN=ITRAN+1
C
                      LADJA(ITRAN)= .FALSE.
C
                      I1A(ITRAN)=J1
                      I2A(ITRAN)=J2
                      N1A(ITRAN)=NI
                      N2A(ITRAN)=NJ
                      W1A(ITRAN)=WI
                      W2A(ITRAN)=WJ
                      NVA(ITRAN)=ITEMP
                      IEC1A(ITRAN)=IEC1
                      IAC1A(ITRAN)=IAC1
                      IAC2A(ITRAN)=IAC2
                      FAC2A(ITRAN)=FAC2
                      IGC1A(ITRAN)=IGC1
                      FGC2A(ITRAN)=FGC2
                      CTSTRA(ITRAN)=STRING
C
                      READ(IUNIT,1011)(WDEA(ITRAN,K),K=1,IZMAX)
                      READ(IUNIT,1012)(AVALA(ITRAN,K),K=1,IZMAX)
C
                       DO 7 J=1,NVA(ITRAN)
                          READ(IUNIT,1013)SCEFA(J,ITRAN),
     &                              (SCOMA(J,ITRAN,K),K=1,IZMAX)
    7                    CONTINUE
C-----------------------------------------------------------------------
                  ENDIF
            ENDIF
    6    CONTINUE
C
C-----------------------------------------------------------------------
C
         IF (LDATA) THEN
            READ (IUNIT,1010) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &               'COPASE DATA SET CONTAINS > ',NDTRN,' TRANSITIONS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ENDIF
         ENDIF
C
C***********************************************************************
C
 1000 FORMAT(1A2,21X,I2,7F5.1)
 1001 FORMAT(1X,32('*'),' B3DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I5,1X,1A18,1X,3(I1,1X),F4.1)
 1004 FORMAT(I5)
 1007 FORMAT(1X,32('*'),' B3DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,2(I1,A))
 1008 FORMAT(4I5,2F5.1,I5,2X,3I3,F5.1,I3,F5.1,1X,1A18)
 1010 FORMAT(1X,I3)
 1011 FORMAT(10X,7F10.0)
 1012 FORMAT(10X,1P,7D10.2)
 1013 FORMAT(1P,8D10.2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
