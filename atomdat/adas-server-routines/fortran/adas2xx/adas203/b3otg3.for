CX - UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3otg3.for,v 1.2 2004/07/06 11:20:20 whitefor Exp $ Date $Date: 2004/07/06 11:20:20 $
CX
      SUBROUTINE B3OTG3( DATE   ,
     &                   NDTRN  , NDTEM  , IZDIMD ,
     &                   SEQSYM , IZS    , TITLE  , DSFULL ,
     &                   IZMAX  , ZA     ,
     &                   IZ0    , ISTRN  , CTSTR  , LIBPT  ,
     &                   NVA    , SCEFA  ,
     &                   MAXT   , TEA    ,
     &                   FGC2   , SCOMA  , SCOM
     &                 )
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3OTG3 *********************
C
C  PURPOSE:  COMMUNICATES GRAPHICS DATA TO IDL
C
C            PROVIDES  GRAPH OF INTERPOLATED EXCITATION RATE PARAMETER
C            COEFFICIENTS, INCLUDING COMPARATIVE SOURCE DATA FROM
C            GENERAL Z EXCIT. FILE
C
C            PLOT IS  (RATE PARM/Z1**FGC2)  VERSUS  (TE)K)/Z1**2)
C
C  CALLING PROGRAM: ADAS203
C
C  SUBROUTINE:
C
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  IZDIMD  = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT : (C*2)  SEQSYM  = ISOELECTRONIC SEQUENCE SYMBOL
C  INPUT : (I*4)  IZS     = NUC. CHG. OF ISOELEC. SEQUENCE 1ST MEMBER
C  INPUT : (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*44) DSFULL  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZMAX   = NUMBER OF IONS (STAGES) IN SOURCE FILE
C  INPUT : (R*8)  ZA()    = RECOMBINING ION CHARGES IN SOURCE FILE
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE OF SELECTED ION
C  INPUT : (I*4)  ISTRN   = SELECTED TRANSITION INDEX FROM INPUT FILE
C  INPUT : (C*18) CTSTR   = SELECTED TRANSITION TITLE STRING
C
C  INPUT : (I*4)  LIBPT   = .FALSE. => BAD POINT OPTION NOT USED
C                         = .TRUE.  => BAD POINT OPTION USED
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (I*4)  NVA()   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C
C  INPUT : (I*4)  SCEFA(,)=REDUCED TEMPS. FROM SOURCE FILE (K)
C
C  INPUT : (I*4)  MAXT    = NUMBER OF OUTPUT ELECTRON TEMPERATURES
C  INPUT : (R*8)  TEA()   = OUTPUT ELECTRON TEMPERATURES (K)
C
C  INPUT : (R*8)  FGC2    = Z-SCALING POWER FOR RATE PARAMETERS
C  INPUT : (R*8)  SCOMA(,)= SOURCE FILE RADIATIVE COEFFICIENTS
C  INPUT : (R*8)  SCOM()  = OUTPUT INTERPOLATED COEFFT FOR SELECTED ION
C
C          (I*4)  NDIM1   = PARAMETER = MAXIMUM NUMBER OF TEMP. VALUES
C                           (MUST NOT BE LESS THAN 'NDTEM')
C          (I*4)  NDIM2   = PARAMETER = MAXIMUM NUMBER OF STAGES
C                           (MUST NOT BE LESS THAN 'IZDIMD')
C          (I*4)  NGPIC   = PARAMETER = MAXIMUM NUMBER OF IONS
C                           TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGION   = PARAMETER = MAXIMUM NUMBER OF IONS
C                           WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN  = PARAMETER = IN DEFAULT GRAPH SCALING IS THE
C                           MINIMUM Y-VALUE THAT IS ALLOWED.
C                           (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT      = TEMP. INDEX NUMBER FOR ARRAY USE
C          (I*4)  IZ      = STAGE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX   = MINIMUM OF: NO. OF IONS OR NGION'
C
C          (R*4)  X()     = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            ELECTRON TEMPERATURES
C          (R*4)  Y(,)    = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            LEVEL POPULATIONS.
C                            1ST DIMENSION = ELECTRON TEMP. INDEX
C                            2ND DIMENSION = ION  INDEX
C
C          (C*80) ISPEC   = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) ENAME   = ELEMENT NAME
C          (C*3)  CNAM()  = 3 BYTE STRING FOR INTERP. AND APPROX. COEFFT
C          (C*13) DNAME   = '       DATE: '
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*30) HEAD1   = HEADING FOR LEVEL ASSIGNMENTS
C          (C*30) STRG1   = HEADING FOR LEVEL ASSIGNMENTS
C
C          (I*4)  PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)  ONE     = PARAMETER = 1  : USED AS FLAG TO IDL
C          (I*4)  ZERO    = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXELEM      ADAS      SETS UP ELEMENT NAME AS STRING
C          XXFLSH      IDL_ADAS  FLUSHES OUT UNIX PIPE
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    03/01/95
C
C UPDATE:  06/07/95 - PE BRIDEN: - Changed NVA from scalar to ARRAY
C                                  (as it should be).
C                                - Changed references to NVA in code
C                                  to ITEMP.
C                                - Declared ITEMP as INTEGER*4 and set
C                                  it equal to NVA(ISTRN).
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGION
      INTEGER   PIPEIN   , PIPEOU   , ONE      , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6  , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
      REAL*4    CUTMIN   , GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 20  , NDIM2=15         , NGPIC=8 , NGION = 55 )
      PARAMETER ( CUTMIN = 1.0E-20 , GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   NDTRN    , NDTEM    , IZDIMD
      INTEGER   IZMAX    , ITEMP    , MAXT     ,
     &          IZ0      , IZS      , IZ1      , ISTRN
      INTEGER   IT       , IZ       , 
     &          IMMAX    , I        
C-----------------------------------------------------------------------
      REAL*8    FGC2     , Z1
C-----------------------------------------------------------------------
      LOGICAL   LIBPT
C-----------------------------------------------------------------------
      CHARACTER ELEM*2   , TITLE*40 , DSFULL*44, ENAME*12
      CHARACTER SEQSYM*2 , XFESYM*2 , CTSTR*18
      CHARACTER GRID*1   , PIC*1    , C3BLNK*3 , DATE*8   ,
     &          FILE0*9  , MNMX0*9  , KEY0*9   , ADAS0*8  ,
     &          DNAME*13 , GNAME*8  ,
     &          XTIT*25  , YTIT*23  ,
     &          HEAD1*30 ,
     &          STRG1*30 , 
     &          ISPEC*80 , CADAS*80 , GTIT1*40
C-----------------------------------------------------------------------
      INTEGER   NVA(NDTRN)
C-----------------------------------------------------------------------
      REAL*4    X(NDIM1)            , Y(NDIM1,NDIM2)      , Z(NDIM1,2)
      REAL*4    XO(NDIM1)
C-----------------------------------------------------------------------
      CHARACTER KEY(3)*22           , STRGA(NDIM2)*30
      CHARACTER CNAM(2)*3
C-----------------------------------------------------------------------
      REAL*8    ZA(IZDIMD)
      REAL*8    SCEFA(NDTEM,NDTRN)  , TEA(NDTEM)
      REAL*8    SCOMA(NDTEM,NDTRN,IZDIMD) ,
     &          SCOM(NDTEM)
C-----------------------------------------------------------------------
      SAVE      CADAS
C-----------------------------------------------------------------------
      DATA ISPEC(1:40)
     &           /'SCALED RATE PARAAMETER VS SCALED TEMP.: '/
      DATA (CNAM(IZ),IZ=1,2)
     &           / 'INT' , 'APX' /
      DATA XTIT  /'Z-SCALED ELEC. TEMP. (K) '/
      DATA YTIT  /'SCALED RATE PARAMETER  '/
      DATA ADAS0 /'ADAS   :'/                ,
     &     FILE0 /'FILE   : '/               ,
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(FULL LINE - INTERP. )'/  ,
     &     KEY(2)/' (DASH LINE - SOURCE )'/  ,
     &     KEY(3)/'                    ) '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/    ,
     &     C3BLNK/'   '/ ,
     &     CADAS /' '/
      DATA DNAME /'       DATE: '/,
     &     GNAME /'SPECIES:'/
      DATA HEAD1 /'------ SEQUENCE MEMBERS ------'/
      DATA STRG1 /'INDX  NUC.CHG. RECD.ION.  ELEM'/
C
C-----------------------------------------------------------------------
C SET UP VARIABLES FOR OUTPUT
C-----------------------------------------------------------------------
C
      IMMAX = MIN0( NGION , IZMAX )
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
         DO 1 I=1,IZMAX
            STRGA(I)=' '
               IF ( I.LE.IZMAX ) THEN
                   IZ1 = ZA(I) + 0.001
                   IZ  =IZ1-1
                   ELEM = XFESYM(IZ+IZS)
                   WRITE(STRGA(I)(1:30),1003) I,IZ+IZS,IZ,ELEM
               ENDIF
    1    CONTINUE
         STRGA(IZMAX+1)=' '
         IZ = IZ0-IZS
         Z1 = IZ+1
         ELEM = XFESYM(IZ0)
         WRITE(STRGA(IZMAX+2)(1:30),1008) ' *', IZ0,IZ,ELEM
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
      ISPEC(41:80) = TITLE
C
      CALL XXELEM(IZ0,ENAME)
      WRITE(GTIT1,1000) ENAME,SEQSYM,CTSTR
C
C-----------------------------------------------------------------------
C MAKE SURE SET ARRAY BOUNDS ARE VALID
C-----------------------------------------------------------------------
C
      IF (NDIM1.LT.NDTEM) STOP
     &                   ' B2OTG1 ERROR: ARRAY DIMENSION NDIM1 < NDTEM'
      IF (NDIM2.LT.IZDIMD) STOP
     &                   ' B2OTG1 ERROR: ARRAY DIMENSION NDIM2 < IZDIMD'
C
C-----------------------------------------------------------------------
C CONVERT TEMPERATURE VALUES TO REAL*4 ARRAY
C-----------------------------------------------------------------------
C
      ITEMP = NVA(ISTRN)
C
         DO 2 IT=1,ITEMP
            X(IT)=REAL(SCEFA(IT,ISTRN))
    2    CONTINUE
C
         DO 20 IT=1,MAXT
            XO(IT)=REAL(TEA(IT)/(IZ0-IZS+1)**2)
   20    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT AND SCALE INTERPOLATED SCOM RATE PARAMETERS TO REAL*4
C-----------------------------------------------------------------------
C
         DO 3 IT=1,MAXT
            Z(IT,1)=SCOM(IT)/Z1**FGC2
            IF (Z(IT,1).LT.GHZERO) Z(IT,1)=GHZERO
    3    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT AND SCALE SOURCE RATE PARAMETERS TO REAL*4
C-----------------------------------------------------------------------
C
         DO 4 IZ=1,IZMAX
            DO 5 IT=1,ITEMP
               Y(IT,IZ)=SCOMA(IT,ISTRN,IZ)/ZA(IZ)**FGC2
               IF (Y(IT,IZ).LT.GHZERO) Y(IT,IZ)=GHZERO
    5       CONTINUE
    4    CONTINUE
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) IZMAX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) ITEMP
      CALL XXFLSH(PIPEOU)
      DO 299, IZ=1, IZMAX
          DO 298, IT=1, ITEMP
              WRITE(PIPEOU,*) Y(IT,IZ)
              CALL XXFLSH(PIPEOU)
298       CONTINUE
299   CONTINUE
      DO 297, IT=1, ITEMP
          WRITE(PIPEOU,*) X(IT)
          CALL XXFLSH(PIPEOU)
297   CONTINUE
      WRITE(PIPEOU,*) MAXT
      CALL XXFLSH(PIPEOU)
      DO 300, IT=1, MAXT
          WRITE(PIPEOU,*) XO(IT)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*) Z(IT,1)
          CALL XXFLSH(PIPEOU)
300   CONTINUE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A12)') ENAME
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A2)') SEQSYM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A18)') CTSTR
      CALL XXFLSH(PIPEOU)
      DO 301, I=1,IZMAX
          IZ1 = ZA(I) + 0.001
          IZ  =IZ1-1
          ELEM = XFESYM(IZ+IZS)
          WRITE(PIPEOU, *) IZ+IZS
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, *) IZ
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, '(A2)') ELEM
          CALL XXFLSH(PIPEOU)
301   CONTINUE
      IZ = IZ0-IZS
      ELEM = XFESYM(IZ0)
      WRITE(PIPEOU, *) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) IZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A2)') ELEM
      CALL XXFLSH(PIPEOU)
      IF (LIBPT) THEN
          WRITE(PIPEOU,*) ONE
      ELSE 
          WRITE(PIPEOU,*) ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A12,2X,'SEQ:',1A2,2X,1A18)
 1001 FORMAT(I2)
 1002 FORMAT(/1X,31('*'),' B3OTG3 MESSAGE ',31('*')/
     &       1X,'ION CHARGE: ',I3,
     &       4X,'NO GRAPH WILL BE OUTPUT BECAUSE:')
 1003 FORMAT(I2,6X,I3,6X,I3,6X,1A2,2X)
 1004 FORMAT(1X,'ALL VALUES ARE BELOW THE CUTOFF OF ',1PE10.3)
 1005 FORMAT(1X,'A SERIOUS ERROR EXISTS IN THE DATA OR B3OTG3')
 1006 FORMAT(1X,31('*'),' END OF MESSAGE ',31('*'))
 1007 FORMAT(I3,9X,A10)
 1008 FORMAT(1A2,6X,I3,6X,I3,6X,1A2,2X)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
