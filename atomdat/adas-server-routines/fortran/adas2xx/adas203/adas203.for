CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/adas203.for,v 1.5 2004/07/06 10:11:42 whitefor Exp $ Date $Date: 2004/07/06 10:11:42 $
CX
      PROGRAM ADAS203
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS03 **********************
C
C  ORIGINAL NAME: SMODMAN2
C
C  PURPOSE:  TO INTERPOLATE  DATA  FROM A GENERAL Z EXCITATION FILE FOR
C            A SELECTED ISOELECTRONIC SEQUENCE MEMBER AND PREPARE A
C            SPECIFIC ION FILE.
C
C            IT ALLOWS THE EXAMINATION AND DISPLAY OF THE DATA FOR  A
C            SINGLE SELECTED ELECTRON TRANSITION.
C
C  NOTE:     THE FOLLOWING DATA NAMING CONVENTIONS NO LONGER APPLY,
C            BUT ARE LEFT IN FOR COMPLETENESS
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.<SE>LIKE.DATA(<MEMBER>)'
C
C            WHERE, <SE> = ELEMENT SYMBOL FOR ISO-ELECTRON SERIES
C                          (ONE OR TWO CHARACTERS)
C
C            THE <MEMBER> NAME IS CONSTRUCTED AS FOLLOWS:
C
C               <INITIALS OF DATA SOURCE><YEAR>
C
C               E.G. JL1990
C
C               <INITIALS OF DATA SOURCE> - E.G. HPS = HP SUMMERS
C
C               <YEAR> - THIS MAY OR MAY NOT BE ABBREVIATED
C
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C
C
C  PROGRAM:
C
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAXIMUM OF INPUT DATA FILE TEMPS
C          (I*4)  NDTIN   = PARAMETER = MAX.NUMBER OF ISPF ENTRED TEMPS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C          (I*4)  IUNT11  = PARAMETER = OUTPU UNIT FOR PASSING FILE
C          (I*4)  NPSPL   = PARAMETER =
C                           NUMBER  OF SPLINE INTERPOLATED GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED   GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  IZDIMD  = PARAMETER = MAX. NO. OF SEQUENCE MEMBERS
C
C          (I*4)  IZMAX   = NUMBER OF MEMBERS IN INPUT FILE
C          (I*4)  IBPTS   = LOOP COUNTER OVER LBPTS
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C                                            TRANSITIONS.
C          (I*4)  NVA()   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C          (I*4)  IZ      =  RECOMBINED ION CHARGE READ FROM INPUT FILE
C          (I*4)  IZ0     =         NUCLEAR CHARGE READ FROM INPUT FILE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE READ FROM INPUT FILE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  NA()    = PRINC. QUANTUM NO. OF LEVEL VALENCE ELECTRON
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  I1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  I2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  N1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER LEVEL PRINCIPAL QUANTUM NUMBER
C                           1ST DIMENSION - TRANSITION NUMBER
C          (I*4)  N2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER LEVEL PRINCIPAL QUANTUM NUMBER
C                           1ST DIMENSION - TRANSITION NUMBER
C          (I*4)  IZA()   = SEQUENCE RECOMBINED ION CHARGES
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C          (I*4)  IZ0A()  = SEQUENCE NUCLEAR CHARGES
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C          (I*4)  IZ1A()  = SEQUNCE RECOMBINING ION CHARGES READ
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IEC1A() = TRANSITION ENERGY INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=>1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C          (I*4)  IAC1A() = TRANSITION PROB. INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C          (I*4)  IAC2A() = TRANSITION TYPE
C                           (1=>DIPOLE, 2=>NON-DIPOLE, 3=>SPIN CHANGE,
C                            4=>OTHER)
C                           1ST DIMENSION - TRANSITION NUMBER
C          (I*4)  IGC1A() = UPSILON INTERPOLATION VARIABLE
C                           (1=>Z1 ; 2=> 1/Z1)
C                           1ST DIMENSION - TRANSITION NUMBER
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                                     PAIRS FOR THE SELECTED TRANSITION.
C          (I*4)  ISTRN   = INPUT DATA FILE: SELECTED TRANSITION INDEX
C                                            FOR ANALYSIS.
C          (I*4)  ISTRN2  = LOOP COUNTER OVER ALL ISTRN.
C          (I*4)  MAXT    = NUMBER OF USER ENTERED TEMPERATURE VALUES.
C          (I*4)  IFOUT   = 1 => 'TINE(ARRAY)' UNITS: KELVIN
C                         = 2 => 'TINE(ARRAY)' UNITS: EV
C                         = 3 => 'TINE(ARRAY)' UNITS:REDUCED TEMPERATURE
C          (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END
C                           TO THE ROUTINE FROM ANY POINT RATHER THAN
C                           A RETURN TO THE PREVIOUS SCREEN. IF 1 THEN
C                           PROGRAM ENDS, IF 0 THEN IT CONTINUES.
C          (I*4)  IPEND   = FLAG USED ON OUTPUT SCREEN TO DETERMINE 
C                           WHICH OF 5 OPTIONS THE USER HAS CHOSEN
C                             = 0 => IMMEDIATE EXIT FROM THE PROGRAM
C                             = 1 => BACK TO PROCESSING SCREEN
C                             = 2 => VIEW GRAPHS
C                             = 3 => PRODUCE OUTPUT AND EXIT PROGRAM
C                             = 4 => PRODUCE OUTPUT AND RETURN TO OUTPUT
C                                    SCREEN
C          (I*4)  IGRAPH   = INDEX OF GRAPH TO BE PLOTTED:
C                               1=TRANSITION WAVE NO.
C                               2=SCALED A-VALUE
C                               3=SCALED RATE PARAMETER
C          (I*4)  IFIRST   = 0 => FIRST RUN THROUGH CODE FOR GIVEN FILE
C                            1 => NOT FIRST RUN
C
C          (R*8)  W1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER LEVEL STATISTICAL WEIGHT
C                           1ST DIMENSION - TRANSITION NUMBER
C          (R*8)  W2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER LEVEL STATISTICAL WEIGHT
C                           1ST DIMENSION - TRANSITION NUMBER
C          (R*8)  FAC2A() = TRANSITION PROB. Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C          (R*8)  FGC2A() = UPSILON Z1 SCALING POWER
C                           1ST DIMENSION - TRANSITION NUMBER
C          (R*8)  SCEFA(,)= INPUT DATA FILE: Z-SCALED ELEC. TEMPS.(K)
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  AVAL()  = ELECTRON IMPACT TRANSITION:
C                            A-VALUE (SEC-1)
C          (R*8)  SCEF()  = INPUT DATA FILE: TEMPERATURES (KELVIN)
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  SCOM(,) = ELECTRON IMPACT TRANSITION:
C                            GAMMA VALUES
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  TINE()  = USER ENTERED ELECTRON TEMPERATURE VALUES.
C                           (NOTE: UNITS ARE GIVEN BY 'IFOUT')
C          (R*8)  TOA()   = USER ENTERED TEMPERATURES (KELVIN)
C          (R*8)  Z1A()   = RECOMBINING ION CHARGES OF MEMBERS
C          (R*8)  BWNOA() = IONISATION POTENTIALS (CM-1)
C                           1ST DIMENSION - SEQUENCE MEMBER INDEX
C          (R*8)  WAA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C                           1ST DIMENSION - LEVEL INDEX
C                           2ND DIMENSION - SEQUENCE MEMBER INDEX
C          (R*8)  WDEA()  = TRANSITION ENERGY (CM-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C          (R*8)  WDE()   = ENERGY OF TRANSITIONS (CM-1)
C          (R*8)  AVALA()  = ELECTRON IMPACT TRANSITION:
C                             A-VALUES (SEC-1)
C                           1ST DIMENSION - TRANSITION NUMBER
C                           2ND DIMENSION - SEQUENCE MEMBER INDEX
C          (R*8)  SCOMA(,,)=ELECTRON IMPACT TRANSITION:
C                            GAMMA VALUES
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C                           3RD DIMENSION - SEQUENCE MEMBER INDEX
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C          (L*4)  LADJA()  = .FALSE. INITIALISATION NO TRANSITIONS
C                                    ADJUSTED YET
C                           1ST DIMENSION - TRANSITION NUMBER
C          (L*4)  LBPTS() = .FALSE. => BAD POINT OPT. NOT SET FOR TRANS.
C                         = .TRUE.  => BAD POINT OPT. SET FOR TRANS.
C                           1ST DIMENSION - TRANSITION NUMBER
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LIBPT   = .TRUE. => BAD POINT OPTION SET
C                         = .FALSE.=> BAD POINT OPTION NOT SET
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C          (L*4)  LCOPS    = .TRUE.  => PRODUCE COPASE FILE
C                               .FALSE. => DO NOT PRODUCE COPASE FILE
C          (L*4)  L2FILE   = .TRUE.  => PRODUCE SUMMARY FILE
C                               .FALSE. => DO NOT PRODUCE SUMMARY FILE
C
C          (C*80) DSFULL = FULL INPUT DATA SET NAME (READ FROM IDL)
C                          (INCORPORATED INTO 'TITLX')
C          (C*80) DSOUT0  = FILENAME FOR SUMMARY OUTPUT
C          (C*80) DSCOPS  = FILENAME FOR COPASE OUTPUT
C          (C*20) USERID  = USER ID
C          (C*80) CADAS   = HEADER FOR TEXT OUTPUT
C          (C*40) TITLE   = TITLE OF RUN (READ FROM IDL PANEL)
C          (C*3)  TITLED  = ELEMENT SYMBOL.(INCORPORATED INTO 'TITLX')
C          (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE AND  TRANSITION
C          (C*2)  SEQSYM  = ELEMENT SYMBOL.
C          (C*18) CSTRGA()= CONFIGURATION FOR LEVEL 'IA()'
C          (C*22) STRGA() = NOMENCLATURE FOR LEVEL 'IA()' INCLUDES:
C                           'CSTRGA()' AND QUANTUM NUMBERS.
C
C NOTE:    INPUT TEMPERATURES 'TINE()' ARE CONVERTED TO KELVIN 'TOA()'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B3SPF0     IDL_ADAS  GATHERS INPUT FILE NAMES FROM IDL
C          B3SETP     IDL_ADAS  SETS UP PROCESSING PARAMETERS FOR IDL
C          B3DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          B3ISPF     IDL_ADAS  GATHERS USERS VALUES FROM IDL
C          B3SPF1     IDL_ADAS  GATHERS USERS OUTPUT VALUES FROM IDL
C          B3LEVE     IDL_ADAS  EVALUATES IONISATION AND LEVEL ENERGIES
C          B3OTG1     IDL_ADAS  PIPE COMMS FOR GRAPH 1
C          B3OTG2     IDL_ADAS  PIPE COMMS FOR GRAPH 2
C          B3OTG3     IDL_ADAS  PIPE COMMS FOR GRAPH 3
C          B3OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          B3WR10     ADAS      PASSING FILE OUTPUT ROUTINE
C          I4EIZ0     ADAS      FETCH NUCLEAR CHARGE FOR ELEMENT SYMBOL
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXFLSH     IDL_ADAS  FLUSH OUT THE UNIX PIPE.
C
C AUTHOR:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    29-02-96
C
C VERSION: 1.1                          DATE: 29-02-96
C MODIFIED: TIM HAMMOND 
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.3                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - RENAMED COUNTER VARIABLE USED IN LOOP 170 TO STOP
C                 WRONG VALUE BEING LATER ASSIGNED TO THE CHOSEN
C                 TRANSITION INDEX ISTRN.
C
C VERSION: 1.4                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - FURTHER RENAMING OF COUNTER VARIABLE TO IST
C
C VERSION: 1.5				    DATE: 08-04-98
C MODIFIED: RICHARD MARTIN
C		    - INCREASED NDTEM TO 14.
C
C-----------------------------------------------------------------------
      INTEGER     NDLEV        , NDTRN        , NDTEM     , NDTIN      ,
     &            MAXDEG       , IZDIMD       
      INTEGER     IUNT07       , IUNT10       , IUNT11
      INTEGER     IFIRST
      INTEGER     NPSPL        , NMX
      INTEGER     L1
      INTEGER     IGRAPH       , IPEND
      INTEGER     PIPEIN       , PIPEOU       , ONE          , ZERO
      INTEGER     ISTOP
      PARAMETER(  PIPEIN=5     , PIPEOU=6     , ONE=1        , ZERO=0)
      PARAMETER ( NDLEV  = 110 , NDTRN  =2100 , NDTEM= 14 , NDTIN = 20 ,
     &            MAXDEG =  19 , IZDIMD = 7   )
      PARAMETER ( IUNT07 =   7 , IUNT10 =  10 , IUNT11 = 11)
      PARAMETER ( NPSPL  = 100 , NMX    = 100 )
      PARAMETER ( L1     =   1 )
C-----------------------------------------------------------------------
      INTEGER     I4EIZ0
      INTEGER     IZ           , IZ0          , IZ1       , IZS      ,
     &            IZMAX        ,
     &            IL           , ITRAN     , ISTRN   ,
     &            NV           , MAXT      , 
     &            IFOUT
      INTEGER     IST
      INTEGER     IBPTS       
      INTEGER     IA(NDLEV)    , NA(NDLEV)    , ISA(NDLEV)   ,
     &            ILA(NDLEV)   , I1A(NDTRN)   , I2A(NDTRN)
      INTEGER     IZA(IZDIMD)  , IZ0A(IZDIMD) , IZ1A(IZDIMD)
      INTEGER     NVA(NDTRN)   ,
     &            N1A(NDTRN)   , N2A(NDTRN)   ,
     &            IEC1A(NDTRN) , IAC1A(NDTRN) ,
     &            IAC2A(NDTRN) , IGC1A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8      Z1A(IZDIMD)         , BWNOA(IZDIMD)
      REAL*8      SCEFA(NDTEM,NDTRN)
      REAL*8      W1A(NDTRN)          , W2A(NDTRN)           ,
     &            FAC2A(NDTRN)        , FGC2A(NDTRN)
      REAL*8      WAA(NDLEV,IZDIMD)   ,
     &            WDEA(NDTRN,IZDIMD)  , AVALA(NDTRN,IZDIMD)  ,
     &            SCOMA(NDTEM,NDTRN,IZDIMD)
      REAL*8      WAO(NDLEV)
      REAL*8      XJA(NDLEV)          , AVAL(NDTRN)  ,
     &            WDE(NDTRN)
      REAL*8      SCEF(NDTEM)
      REAL*8      TINE(NDTIN)  ,
     &            TOA(NDTIN)  
      REAL*8      TSCEF(NDTEM,3)     , SCOM(NDTEM,NDTRN)
      REAL*8      BWNO
C-----------------------------------------------------------------------
      LOGICAL     OPEN10       , LIBPT        , LPEND
      LOGICAL     LADJA(NDTRN)        ,
     &            LBPTS(NDTRN)
      LOGICAL     LGPH         , LCOPS        , L2FILE
C-----------------------------------------------------------------------
      CHARACTER   REP*3        , DATE*8       , DSFULL*80 ,
     &            TITLED*3     , TITLE*40     , TITLX*80   ,
     &            SEQSYM*2
      CHARACTER   CSTRGA(NDLEV)*18         , STRGA(NDLEV)*22
      CHARACTER   CTSTRA(NDTRN)*18
      CHARACTER   CADAS*80     , DSCOPS*80    , DSOUT0*80
      CHARACTER   USERID*20
C-----------------------------------------------------------------------
      DATA OPEN10 /.FALSE./ 
C-----------------------------------------------------------------------
C *************************** BEGIN ************************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
C

100   CONTINUE

C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT IUNT10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
          CLOSE(IUNT10)
          OPEN10=.FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
      CALL B3SPF0(REP , DSFULL)
      IFIRST = 0
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN
C-----------------------------------------------------------------------
C
      IF (REP.EQ.'YES') THEN
          GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10, FILE=DSFULL, STATUS='UNKNOWN')
      OPEN10 = .TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET AND SET BAD POINT OPTION TO .FALSE.
C-----------------------------------------------------------------------
C
      CALL B3DATA( IUNT10 , NDLEV  , NDTRN , NDTEM , IZDIMD,
     &             SEQSYM , IZMAX  , Z1A   , IZA   , IZ0A  , IZ1A  ,
     &             BWNOA  , IL     ,
     &             IA     , CSTRGA , NA    , ISA   , ILA   , XJA   ,
     &             WAA    , ITRAN  ,
     &             NVA    , SCEFA  ,
     &             I1A    , I2A    , N1A   , N2A   , W1A   , W2A   ,
     &             IEC1A  , IAC1A  , IAC2A , FAC2A , IGC1A , FGC2A ,
     &             CTSTRA , WDEA   , AVALA , SCOMA , LADJA
     &           )
C
      DO 150 IBPTS =1,ITRAN
          LBPTS(IBPTS) = .FALSE.
150   CONTINUE
C
      CLOSE(IUNT10)
      OPEN10 = .FALSE.
C
C-----------------------------------------------------------------------
C SET UP INITIAL PARAMETERS FOR PROCESSING SCREEN (AND PASS TO IDL)
C-----------------------------------------------------------------------
C
      CALL B3SETP( SEQSYM , IZMAX  , Z1A   ,
     &             NDLEV  , IL     , ITRAN ,
     &             CSTRGA , ISA    , ILA   , XJA  ,
     &             STRGA  , IZDIMD
     &           )
C
       IZS = I4EIZ0(SEQSYM)
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
200   CALL B3ISPF( LPEND  ,
     &             NDTIN  , IL    ,
     &             NDTEM  , NDTRN , NVA    , SCEFA ,
     &             NV     , SCEF  , TSCEF  ,
     &             IZS    , IZ0   ,
     &             ITRAN  , I1A   , I2A    , STRGA ,
     &             IEC1A  , IAC1A , IAC2A  ,
     &             FAC2A  , IGC1A , FGC2A  ,
     &             TITLE  ,
     &             ISTRN  , IFOUT ,
     &             MAXT   , TINE  ,
     &             LIBPT  , IFIRST
     &           )
C
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      IF(LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C ANALYSE ENTERED DATA 
C-----------------------------------------------------------------------
C
      LBPTS(ISTRN)=LIBPT
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TINE -> TOA)
C-----------------------------------------------------------------------
C
      IZ1 = IZ0-IZS+1
C
      CALL XXTCON( IFOUT , L1 , IZ1 , MAXT , TINE , TOA   )
C
C-----------------------------------------------------------------------
C IDENTIFY REQUESTED TRANSITION PARAMETERS
C-----------------------------------------------------------------------
C
      CALL B3REAC( NDTRN , NDTEM , IZDIMD,
     &             IZMAX , Z1A   ,
     &             ITRAN ,
     &             NVA   , SCEFA ,
     &             I1A   , I2A   , N1A   , N2A   , W1A   , W2A,
     &             IEC1A , IAC1A , IAC2A , FAC2A , IGC1A , FGC2A,
     &             CTSTRA, WDEA  , AVALA , SCOMA ,
     &             IZS   , IZ0   , ISTRN , LBPTS(ISTRN)  ,
     &             MAXT  , TOA   ,
     &             WDE   , AVAL  , SCOM
     &           )
C
      LADJA(ISTRN) = .TRUE.
C
C-----------------------------------------------------------------------
C INFORM IDL IT CAN CONTINUE
C-----------------------------------------------------------------------
C 
      WRITE(PIPEOU,*) L1
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C GET USER'S OUTPUT SELECTIONS FROM IDL
C-----------------------------------------------------------------------
C
300   CALL B3SPF1( LGPH, IGRAPH, IPEND, LCOPS, L2FILE, DSOUT0,
     &                   DSCOPS, CADAS, USERID)
      IF (IPEND.EQ.0) THEN
          GOTO 9999
      ELSEIF (IPEND.EQ.1) THEN
          GOTO 200
      ELSEIF (IPEND.EQ.2) THEN
          IF (IGRAPH.EQ.1) THEN
              CALL B3OTG1( DATE   ,
     &                     NDTRN  , IZDIMD ,
     &                     SEQSYM , IZS    , TITLE , DSFULL ,
     &                     IZMAX  , Z1A    ,
     &                     IZ0    , ISTRN  , CTSTRA(ISTRN)  ,
     &                     N1A(ISTRN)      , N2A(ISTRN)     ,
     &                     WDEA   , WDE(ISTRN)
     &                   )
          ELSEIF (IGRAPH.EQ.2) THEN
              CALL B3OTG2( DATE   ,
     &                     NDTRN  , IZDIMD ,
     &                     SEQSYM , IZS    , TITLE , DSFULL ,
     &                     IZMAX  , Z1A    ,
     &                     IZ0    , ISTRN  , CTSTRA(ISTRN)  ,
     &                     FAC2A(ISTRN)    ,
     &                     AVALA  , AVAL(ISTRN)
     &                   )
          ELSE
              CALL B3OTG3(DATE   ,
     &                   NDTRN  , NDTEM  , IZDIMD ,
     &                   SEQSYM , IZS    , TITLE  , DSFULL ,
     &                   IZMAX  , Z1A    ,
     &                   IZ0    , ISTRN  ,
     &                   CTSTRA(ISTRN)   , LBPTS(ISTRN)    ,
     &                   NVA    , SCEFA  ,
     &                   MAXT   , TOA    ,
     &                   FGC2A(ISTRN)    , SCOMA  , SCOM(1,ISTRN)
     &                 )
          ENDIF
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF (ISTOP.EQ.1) GOTO 9999
C
      ELSEIF ((IPEND.EQ.3).OR.(IPEND.EQ.4)) THEN
C
C-----------------------------------------------------------------------
C END OF CURRENT FILE ANALYSIS - PROCESS ALL DATA USING PARAMETER
C ADJUSTMENTS IF MADE.
C-----------------------------------------------------------------------
C
          CALL  B3LEVE( NDLEV , IZDIMD,
     &                  IZMAX , Z1A   , IZA   , IZ0A  , IZ1A,
     &                  BWNOA , IL    ,
     &                  IA    , NA    , WAA   ,
     &                  IZS   , IZ0   ,
     &                  BWNO  , WAO
     &                )
C
          DO 170 IST=1,ITRAN
              CALL B3REAC(NDTRN , NDTEM , IZDIMD,
     &                    IZMAX , Z1A   ,
     &                    ITRAN ,
     &                    NVA   , SCEFA ,
     &                    I1A   , I2A   , N1A   , N2A   , W1A   , W2A,
     &                    IEC1A , IAC1A , IAC2A , FAC2A , IGC1A , FGC2A,
     &                    CTSTRA, WDEA  , AVALA , SCOMA ,
     &                    IZS   , IZ0   , IST   , LBPTS(IST)    ,
     &                    MAXT  , TOA   ,
     &                    WDE           ,
     &                    AVAL          , SCOM
     &                   )
  170     CONTINUE
C
C-----------------------------------------------------------------------
C  CONSTRUCT DESCRIPTIVE TITLE FOR OUTPUT AND FETCH FILE NAME
C-----------------------------------------------------------------------
C
          WRITE (TITLX,1001) DSFULL(1:30),TITLED,IZ
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
          IF (OPEN10) THEN
              CLOSE(IUNT10)
              OPEN10=.FALSE.
          ENDIF
C
C----------------------------------------------------------------------
C OUTPUT COPASE FILE IF REQUIRED
C----------------------------------------------------------------------
C
          IF(LCOPS) THEN
              CALL B3WR10( IUNT11 , DSFULL  ,
     &                     NDLEV  , NDTRN   , NDTEM  ,
     &                     IL     , ITRAN   ,
     &                     IZS    , IZ0     ,
     &                     IA     , CSTRGA  , NA     ,
     &                     ISA    , ILA     , XJA    ,
     &                     I1A    , I2A     ,
     &                     IEC1A  , IAC1A   , IAC2A  ,
     &                     FAC2A  , IGC1A   , FGC2A  ,
     &                     LBPTS  , LADJA   ,
     &                     MAXT   , TOA     ,
     &                     BWNO   , WAO     , AVAL  , SCOM ,
     &                     DATE   , USERID  , DSCOPS
     &                   )
          ENDIF
C
C-----------------------------------------------------------------------
C IF REQUESTED OUTPUT TEXT SUMMARY FILE 
C-----------------------------------------------------------------------
C
          IF (L2FILE) THEN
              CALL B3OUT0( IUNT07 , DATE   , DSFULL ,
     &                     IZDIMD , NDLEV  , NDTRN  , NDTEM  ,
     &                     SEQSYM , IZMAX  , IZ0A   ,
     &                     IZS    , IZ0    , BWNO   ,
     &                     IL     , ITRAN  ,
     &                     IA     , CSTRGA , ISA    , ILA    , XJA    ,
     &                     WAO    ,
     &                     NV     , SCEFA(1,1)    ,
     &                     MAXT   , TOA    ,
     &                     I1A    , I2A    ,
     &                     IEC1A  , IAC1A  , IAC2A  ,
     &                     FAC2A  , IGC1A  , FGC2A  ,
     &                     LBPTS  , LADJA  , CADAS  , DSOUT0
     &               )
          ENDIF
C
C-----------------------------------------------------------------------
C SEND FINISHED SIGNAL TO IDL
C-----------------------------------------------------------------------
C
          WRITE(PIPEOU,*) ONE
          CALL XXFLSH(PIPEOU)
          IF (IPEND.EQ.4) GOTO 300
      ENDIF
      IF (IPEND.EQ.2) GOTO 300
C
C-----------------------------------------------------------------------
C
9999  STOP
C
C-----------------------------------------------------------------------
C
 1001 FORMAT('FILE: ',A30,'  ELEMENT: ',A3,'(Z=',I2,')')
C
C-----------------------------------------------------------------------
C

      END
