CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas203/b3setp.for,v 1.3 2004/07/06 11:21:00 whitefor Exp $ Date $Date: 2004/07/06 11:21:00 $
CX
       SUBROUTINE B3SETP( SEQSYM , IZMAX  , Z1A   ,
     &                    NDLEV  , IL     , ITRAN ,
     &                    CSTRGA , ISA    , ILA   , XJA  ,
     &                    STRGA  , IZDIMD
     &                  )
C	
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B3SETP *********************
C
C  PURPOSE:  TO SET UP PARAMETERS USED IN THE IDL PROCESSING SCREEN
C
C  CALLING PROGRAM: ADAS203
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'B3DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZDIMD  = MAXIMUM NUMBER OF SEQUENCE MEMBERS
C  INPUT : (C*2)  SEQSYM  = ELEMENT SYMBOL FOR FIRST SEQUENCE MEMBER
C  INPUT : (I*4)  IZMAX   = NUMBER OF MEMBERS IN INPUT FILE
C  INPUT : (R*8)  Z1A()   = RECOMBINING ION CHARGES OF MEMBERS
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ITRAN   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (C*22) STRGA() = LEVEL DESIGNATIONS
C          (C*22) STARG() = LEVEL DESIGNATIONS IN FORM MORE SUITABLE FOR 
C                           USE BY IDL WIDGETS.
C
C
C          (I*4)  IFIRST  = BYTE POSITION OF START OF NUMBER IN BUFFER
C          (I*4)  ILAST   = BYTE POSITION OF END   OF NUMBER IN BUFFER
C          (I*4)  IWORD   = THE WORD POSITION OF THE REQUIRED DATA IN
C                           A STRING TO BE INTERROGATED BY XXWORD.
C          (I*4)  IZ0     = NUCLEAR CHARGE
C          (I*4)  IZ      = ION CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C          (I*4)  IZS     = SEQUENCE CHARGE NUMBER
C
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  ILVAL   = VALUE OF QUANTUM NUMBER L + 1
C          (I*4)  J       = GENERAL PURPOSE INDEX
C          (I*4)  LENC    = LENGTH OF CHARACTER STRING
C          (I*4)  NWORDS  = NUMBER OF NUMBERS STORED IN BUFFER
C
C          (C*2)  SSEQ    = CHEMICAL SYMBOL OF NEUTRAL SEQ. MEMBER
C          (C*3)  SCNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (C*7)  CDELIM  = DELIMITERS FOR INPUT OF DATA FROM HEADERS
C          (I*4)  SIL     = NUMBER OF ENERGY LEVELS
C          (C*1)  CONFIG()= QUANTUM NUMBER (L) LETTERS
C                           DIMENSION: QUANTUM NUMBER L + 1
C          (C12)  C12     = GENERAL STRING OF LENGTH 12
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)  CZDIMD  = PARAMETER = IZDIMD (IT APPEARS TO BE 
C                           NECESSARY TO HAVE A SEPARATE PARAMETER FOR
C                           DIMENSIONING THE CHARACTER ARRAYS AS USING
C                           IZDIMD CAUSES PROBLEMS ON BOTH DEC AND SUN
C                           FORTRAN COMPILERS)
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C           XXFLSH     IDL_ADAS  FLUSHES OUT UNIX PIPE TO IDL
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    18/08/94
C
C UPDATE:  03/07/95 - HPS  ALTERED TO ALLOW CSTRGA ARRAY OF LENGTH 18
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 29-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 01-03-96
C MODIFIED: TIM HAMMOND 
C               - ADDED PIPE COMMUNICATIONS SECTION
C
C VERSION: 1.3                          DATE: 01-04-96
C MODIFIED: TIM HAMMOND
C               - REMOVED SUPERFLUOUS VARIABLES
C
C-----------------------------------------------------------------------
      INTEGER   IZMAX
      INTEGER   I          , IZ0    , IZ     , IZ1    , IZS
      INTEGER   I4EIZ0
      INTEGER   NDLEV      , IL     , ITRAN
      INTEGER   ILEV          , ILVAL
      INTEGER   ISA(IL)    , ILA(IL)
      INTEGER   IFIRST(15) , ILAST(15)     , IWORD    , NWORDS
      INTEGER   LENC       , J
      INTEGER   PIPEOU  
      INTEGER   IZDIMD    
      INTEGER   CZDIMD
      PARAMETER  (PIPEOU = 6)
      PARAMETER  (CZDIMD = 7)
C-----------------------------------------------------------------------
      REAL*8    XJA(IL)
      REAL*8    Z1A(IZDIMD)
C-----------------------------------------------------------------------
      CHARACTER XFESYM*2       , SEQSYM*2         , ELEM*2
      CHARACTER SSEQ*2         , SCNTE*3  , SIL*3 , CDELIM*7  ,
     &          CONFIG(20)*1
      CHARACTER CSTRGA(IL)*18 , STRGA(NDLEV)*22   
      CHARACTER STARG*22
      CHARACTER ZSTRGA(CZDIMD)*30
      CHARACTER C12*12
C-----------------------------------------------------------------------
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
C-----------------------------------------------------------------------
      DATA      CDELIM / ' ()<>{}' /
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      SSEQ = SEQSYM
C
         IZS = I4EIZ0(SEQSYM)
         WRITE(PIPEOU,*) IZMAX
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) IZDIMD
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) NDLEV
         CALL XXFLSH(PIPEOU)
         DO 1 I=1,IZDIMD
            ZSTRGA(I)=' '
               IF     ( I.LE.IZMAX ) THEN
                   IZ1 = Z1A(I) + 0.001
                   IZ  =IZ1-1
                   IZ0 = IZ+IZS
                   ELEM = XFESYM(IZ0)
                   WRITE(PIPEOU,*) IZ0
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*) IZ
                   CALL XXFLSH(PIPEOU)
                   WRITE(ZSTRGA(I)(1:30),1000) IZ0,IZ,ELEM
               ENDIF
    1    CONTINUE
C
C
      WRITE(SCNTE,1001) ITRAN
      WRITE(SIL  ,1001) IL
      WRITE(PIPEOU,*) IL
      CALL XXFLSH(PIPEOU)
         DO 2 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  IWORD = 1
                  J     = 1
                  CALL XXWORD( CSTRGA(ILEV) , CDELIM   , IWORD  ,
     &                         J            ,
     &                         IFIRST(1)    , ILAST(1) , NWORDS )
                  IWORD = NWORDS
                  J     = 1
                  CALL XXWORD( CSTRGA(ILEV)   , CDELIM   , IWORD  ,
     &                         J              ,
     &                         IFIRST(NWORDS) ,
     &                         ILAST(NWORDS)  , NWORDS )
                  C12   = ' '
                  LENC  = ILAST(NWORDS)-IFIRST(1)+1
C
                  IF(LENC.LE.12)THEN
                      C12(1:LENC) = CSTRGA(ILEV)(IFIRST(1):
     &                                           ILAST(NWORDS))
                  ELSE
                      C12 = CSTRGA(ILEV)(ILAST(NWORDS)-11:
     &                                  ILAST(NWORDS))
                  ENDIF
C
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1002)
     &                   C12,ISA(ILEV),CONFIG(ILVAL),XJA(ILEV)
                  IF (ILEV.LE.IL) THEN
                      WRITE(STARG(1:22),1004)
     &                   C12,ISA(ILEV),CONFIG(ILVAL),XJA(ILEV)
                      WRITE(PIPEOU,'(A22)') STARG
                      CALL XXFLSH(PIPEOU)
                  ENDIF
               ENDIF
    2    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C WRITE RELEVANT PARAMETERS TO IDL - b3ispf.pro
C-----------------------------------------------------------------------
C
      WRITE (PIPEOU, *) IZS
      CALL XXFLSH(PIPEOU)
      WRITE (PIPEOU, *) ITRAN
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I5,7X,I5,9X,1A2,2X)
 1001 FORMAT(I3)
 1002 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
 1003 FORMAT(I2.2)
 1004 FORMAT(1A4,'(',I1,')',A1,'(',F4.1,')')
C
C-----------------------------------------------------------------------
C
      RETURN
      END
