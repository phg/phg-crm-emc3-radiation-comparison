      SUBROUTINE BCSPF1( DSNPAP , LPAPER , DSN04R , L04R ,
     &                   LPEND)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BCSPF1 *********************
C
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL OR
C           THE INFO FILE (WHICH HAS BEEN REDIRECTED TO STANDARD INPUT).
C
C  CALLING PROGRAM: ADAS212
C
C  SUBROUTINE:
C
C  OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT TEXT TO 'PAPER.TXT'
C                             .FALSE. => NO TEXT OUTPUT
C                  L04R     = .TRUE.  => OUTPUT TEXT TO adf04 R-lines file
C                             .FALSE. => NO OUTPUT
C  OUTPUT: (C*80)  DSNPAP   = OUTPUT TEXT FILE NAME
C                  DSN04R   = OUTPUT TEXT FILE NAME
C
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => USER SELECTED 'CANCEL'
C                             .FALSE. => USER DID NOT
C
C          (I*4)   PIPEIN   = STANDARD INPUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    31ST JULY 1996
C
C VERSION: 1.1                          DATE: 31-07-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION.
C
C VERSION : 1.2
C DATE    : 31-08-2009
C MODIFIED: Martin O'Mullane
C               - Return name of adf04 R-lines output file
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      INTEGER      PIPEIN, LOGIC
      PARAMETER   (PIPEIN = 5)
C-----------------------------------------------------------------------
      CHARACTER    DSNPAP*80 , DSN04R*80
C-----------------------------------------------------------------------
      LOGICAL      LPAPER  , L04R        , LPEND
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      READ(PIPEIN,*) LOGIC
      IF(LOGIC.NE.1)THEN
         LPEND = .FALSE.
         READ(PIPEIN,'(A)')DSN04R
         READ(PIPEIN,'(A)')DSNPAP
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            L04R=.FALSE.
         ELSE
            L04R=.TRUE.
         ENDIF
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LPAPER=.FALSE.
         ELSE
            LPAPER=.TRUE.
         ENDIF
      ELSE
         LPEND = .TRUE.
      ENDIF

      RETURN
      END
