       SUBROUTINE ISORT(ND,IAIN,IAOUT,IVSORT,N,LPAPER)
C-----------------------------------------------------------------------
C  PURPOSE: SORT AN INTEGER ARRAY SO THAT IT IS IN INCREASING ORDER
C
C  INPUT
C      ND       = MAXIMUM DIMENSION OF ARRAY
C      IAIN(I)  = INPUT ARRAY
C      N        = NUMBER OF VALUES
C  OUTPUT
C      IAOUT(I) = SORTED ARRAY VALUES
C      IVSORT(I)= INVERSE SORT VALUES
C
C
C AUTHOR: UNKOWN
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 31-7-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 20-7-07
C MODIFIED: Allan Whiteford
C               - Small modification to comments to allow for
C                 automatic documentation preparation.
C               - Removed junk from columnds > 72.
C               - Removed old SCCS header.
C
C-----------------------------------------------------------------------
       DIMENSION IAIN(ND) , IAOUT(ND) , IVSORT(ND)
       LOGICAL LPAPER
       DO 10 I=1,N
        IAOUT(I) = IAIN(I)
        IVSORT(I)=I
   10  CONTINUE
C
       N1=N-1
       DO 30 I=1,N1
        I1=I+1
        DO 20 J=I1,N
         IF(IAOUT(I).LE.IAOUT(J))GO TO 20
         ISWAP=IAOUT(I)
         IAOUT(I)=IAOUT(J)
         IAOUT(J)=ISWAP
         ISWAP=IVSORT(I)
         IVSORT(I)=IVSORT(J)
         IVSORT(J)=ISWAP
   20   CONTINUE
   30  CONTINUE
       IF(LPAPER) THEN
          DO 40 I=1,N
             WRITE(17,*) I,IAIN(I),IAOUT(I),IVSORT(I)
 40       CONTINUE
       ENDIF
       RETURN
      END
