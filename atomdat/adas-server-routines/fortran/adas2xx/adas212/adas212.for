       program adas212

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS212 **********************
C
C  ORIGINAL NAME: POSTLLEV
C
C  VERSION:  1.0
C
C  PURPOSE:  TO SUPPLEMENT SPECIFIC ION FILES OF TYPE ADF04 WITH
C            STATE SELECTIVE DIELECTRONIC RECOMBINATION BY
C            POST-PROCESSING FILES OF TYPE ADF09C
C
C  PROGRAM:
C
C  ROUTINES:
C
C  NOTES:
C
C  AUTHOR:  HP SUMMERS
C           K1/1/57
C           JET EXT. 4941
C
C  DATE:    21 MAR 1992
C
C  UPDATE:  19/06/96  HP SUMMERS - ALTERED NAME TO ADAS212
C
C  UPDATE:  10/02/97  HP SUMMERS - CHANGED IOPT FROM -1 TO 1.
C
C-----------------------------------------------------------------------
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    31ST JULY 1996
C
C VERSION: 1.1                          DATE: 31-07-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION.
C
C VERSION: 1.2                          DATE: 71-10-96
C MODIFIED: WILLIAM OSBORN
C      - CHANGED OPEN07 TO OPEN17 IN DATA STATEMENT
C
C VERSION: 1.3                          DATE: 12-2-97
C MODIFIED: RICHARD MARTIN
C      - CHANGED IOPT FROM -1 TO +1
C      - REMOVED STOP IF DSNSPO DOES NOT EXIST
C
C VERSION: 1.4                          DATE: 21-07-97
C MODIFIED: M O'MULLANE
C      - CHANGED TO ACCEPT 14 TEMPERATURES AND NUMEROUS CHANGES
C        TO PREVENT WRITING NULL STRINGS
C
C VERSION: 1.5                          DATE: 24-02-98
C MODIFIED: RICHARD MARTIN
C        - RESET DSN*** VARIABLES TO CHAR*80
C
C VERSION : 1.6
C DATE    : 20/12/2001
C MODIFIED: Martin O'Mullane
C              - Add real name of producer via XXNAME.
C
C VERSION : 1.7
C DATE    : 02/02/2005
C MODIFIED: Paul Bryans
C              - Read non-Maxwellian parameters
C              - Calculate non-Maxwellian recombination coefficient by
C                converting at the DR resonant energy
C
C VERSION : 1.8
C DATE    : 02/02/2005
C MODIFIED: Allan Whiteford
C              - Changed call to bcspf0 so that it returns the
C                non-Maxwellian parameters and added code
C                to insert them into appropriate variables
C                depending on the distribution type.
C
C VERSION : 1.9
C DATE    : 13/04/2005
C MODIFIED: Paul Bryans
C              - Fixed bug which set Maxwellian data to zero
C              - Calculate resonant energy from first 2 finite points
C                as adf09 can have zero values
C              - Minimised output to 1.0D-30
C
C VERSION : 1.10
C DATE    : 31-08-2009
C MODIFIED: Martin O'Mullane
C              - Extensive restructuring.
C              - Make implicit none.
C              - Remove unsed variables and labels.
C              - Use xxdata_04 and xxdata_09 to read data.
C              - Replace FINTX with R8FUN1 from the central adaslib/maths/.
C              - This version has non-Maxwellian (temporarily) removed.
C              - Output R-lines to file selected by user and ignore
C                the one in the adf18 driver file. This mirrors ADAS211
C                workflow.
C              - DR will be added to existing R-lines. The previous
C                optional addition is removed.
C
C VERSION : 1.11
C DATE    : 19-07-2017
C MODIFIED: Martin O'Mullane
C              - Add extra arguments for xxdata_09.
C              - Remove notes text becaue it was not relevant.
C
C VERSION : 1.12
C DATE    : 02-09-2017
C MODIFIED: Martin O'Mullane
C              - Remove the redundant tev() array.
C
C VERSION : 1.813
C DATE    : 08-071-2018
C MODIFIED: Martin O'Mullane
C              - Further arguments required for hybrid adf09 files
C                in xxdata_09.
C
C-----------------------------------------------------------------------
      integer   pipein   , pipeou   , one    , ndbfil
      integer   iunt12   , iunt13   , iunt14 , iunt17
      integer   ndlev_04 , ndtrn    , ndmet  , nvmax  , ndqdn
      integer   ndprt    , ndrep    , ndt    , ndsc   ,
     &          ndlev_09 , ndaug    , ndlrep , ndjlev
      integer   nrline   , nrzero
      integer   nemax    , ntmax    , iunt37
c-----------------------------------------------------------------------
      parameter( pipein    =    5 , pipeou =     6 , one    =   1 ,
     &           ndbfil    =    6 )
      parameter( iunt12    =   12 , iunt13 =    13 , iunt14 =  14 ,
     &           iunt17    =   17 )
      parameter( ndlev_04  = 1000 , ndtrn  = 25000 , ndmet  =   5 )
      parameter( nvmax     =   14 , ndqdn  =     6 )
      parameter( nemax     =  200 , ntmax  =    50 , iunt37 =  37 )
      parameter( ndprt     =   50 , ndrep  =   250 , ndlrep = 250 ,
     &           ndlev_09  = 5000 , ndaug  =    50 , ndt    = 20  ,
     &           ndsc      = 1000,  ndjlev =  100 )
c-----------------------------------------------------------------------
      integer   nbfil   , nlev      , njlev    , idist    ,
     &          iz      , iz0       , iz1      ,
     &          npl     , iadftyp   , itieactn , iorb     ,
     &          icnte   , icntp     , icntr    , icnth    , iform
      integer   nplr    , npli      , icnti    , icntl    ,
     &          icnts   , il_04     , itran    , maxlev
      integer   iaprs_n , iaprs_nl  , nlrep    , nte_09   ,
     &          nlvl_09 , nprnt     , nprntf   ,
     &          nprnti  , nrep      , nv       , ncfg_09  , nippy_09
      integer   ibfil   , i         , it       , ilev     ,
     &          ip      , i1        , i2       , jlev     , jprt
      integer   lens1   , lens2     , i4unit   , i4indfi4 , iopt
c----------------------------------------------------------------------
      logical   lpaper  , l04r      , lpend    , lexist   , lsj
      logical   open12  , open14    , open17
      logical   lprn    , lcpl      , lorb     , lbeth    ,
     &          letyp   , lptyp     , lrtyp    , lhtyp    ,
     &          lityp   , lstyp     , lltyp    , lsetx
c----------------------------------------------------------------------
      real*8    r8fun1  , bwno      , bwnp     , bwnr
      real*8    kappa   , druval    , dparam
      real*8    y       , ysum
C----------------------------------------------------------------------
      character string*128 , sstrgo*128  , lstrgo*143
      character date*8     , user*30     , rep*3
      character dsnsp*80   , dsnspo*80   , dsn80*80    , dsn04r*80 ,
     &          dsn*80     , dsnxr*80    , dsnpap*80   , dsn37*80
      character seqsym*2   , ctype*2     , titled*3
C----------------------------------------------------------------------
      integer   ispjra(ndjlev)       , jlrefa(ndjlev)
      integer   ispra(ndlev_04)      , ibdra(ndlev_04,ndbfil)
      integer   ipla(ndmet,ndlev_04) , npla(ndlev_04)
      integer   ia_04(ndlev_04)      ,
     &          isa_04(ndlev_04)     , ila_04(ndlev_04)       ,
     &          i1a(ndtrn)           , i2a(ndtrn)
      integer   ietrn(ndtrn)         , iptrn(ndtrn)           ,
     &          irtrn(ndtrn)         , ihtrn(ndtrn)           ,
     &          ie1a(ndtrn)          , ie2a(ndtrn)            ,
     &          ip1a(ndtrn)          , ip2a(ndtrn)
      integer   iitrn(ndtrn)         , iltrn(ndtrn)           ,
     &          istrn(ndtrn)         ,
     &          ia1a(ndtrn)          , ia2a(ndtrn)            ,
     &          il1a(ndlev_04)       , il2a(ndlev_04)         ,
     &          is1a(ndlev_04)       , is2a(ndlev_04)
      integer   ipa_09(ndprt)        , ispa_09(ndprt)         ,
     &          ilpa_09(ndprt)       ,
     &          ia_09(ndlev_09)      , isa_09(ndlev_09)       ,
     &          ila_09(ndlev_09)     , ipca_09(ndprt)         ,
     &          irepa_n(ndrep)       , nrepa(ndrep)           ,
     &          ipaug_n(ndaug,2)     ,
     &          iprti(ndprt)         , ip_09(ndlev_09)        ,
     &          iprtf(ndprt,ndprt)   , nsysf(ndprt,ndprt)     ,
     &          isys(ndprt,ndprt,2)  , ispsys(ndprt,ndprt,2)  ,
     &          irepa_nl(ndlrep)     , nlrepa_n(ndlrep)       ,
     &          nlrepa_l(ndlrep)     , ipaug_nl(ndaug,2)
      integer   irtag(ndtrn)         , ittag(ndtrn)
      integer   iscp(ndsc)           , iscn(ndsc)             ,
     &          iscl(ndsc)
C----------------------------------------------------------------------
      real*8    fspjra(ndjlev)             ,
     &          bwnoa(ndmet)               , prtwta(ndmet)    ,
     &          zpla(ndmet,ndlev_04)       , beth(ndtrn)      ,
     &          qdorb((ndqdn*(ndqdn+1))/2) , qdn(ndqdn)
      real*8    auga(ndtrn)                , wvla(ndlev_04)
      real*8    scef(nvmax)                , scom(nvmax,ndtrn),
     &          aa(ndtrn)                  , aval(ndtrn)      ,
     &          xja_04(ndlev_04)           , wa_04(ndlev_04)
      real*8    xjpa_09(ndprt)             , wpa_09(ndprt)    ,
     &          xjpca_09(ndprt)            , wpca_09(ndprt)   ,
     &          xja_09(ndlev_09)           , wa_09(ndlev_09)  ,
     &          tea_09(ndt)
      real*8    auga_nl(ndlrep,ndaug)      , auga_n(ndrep,ndaug)
      real*8    diel_res(ndlev_09,ndprt,ndt)
      real*8    diel_nl(ndlrep,ndprt,ndprt,2,ndt)
      real*8    diel_n(ndrep,ndprt,ndprt,2,ndt)
      real*8    diel_sum(ndprt,ndprt,2,ndt)
      real*8    diel_tot(ndprt,ndt)
      real*8    auga_res(ndlev_09,ndprt)
      real*8    xin(ndt)                   , yin(ndt)         ,
     &          xout(nvmax)                , yout(nvmax)      ,
     &          dy(nvmax)
      real*8    diel(ndt,ndlev_04,ndprt)
      real*8    fsc(ndsc,2)                , wnsc(ndsc,2)
c----------------------------------------------------------------------
      character dsn09(ndbfil)*80
      character cpla(ndlev_04)*1           , cprta(ndmet)*9          ,
     &          cstrga_04(ndlev_04)*18     , tcode(ndtrn)*1          ,
     &          cstrpa_09(ndprt)*20        , cstrpca_09(ndprt)*20    ,
     &          cstrga_09(ndlev_09)*20     ,
     &          caprs_n(ndaug)*10          , caprs_nl(ndaug)*10      ,
     &          cspsys(ndprt, ndprt,2)*30
c----------------------------------------------------------------------
      logical   ltied(ndlev_04)            , lbseta(ndmet)           ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lss04a(ndlev_04,ndmet)
      logical   lauga_res(ndlev_09,ndprt)
      logical   lauga_n(ndrep,ndaug)
      logical   lauga_nl(ndlrep,ndaug)
      logical   ldiel_res(ndlev_09,ndprt)
      logical   ldiel_nl(ndlrep,ndprt,ndprt,2)
      logical   ldiel_n(ndrep,ndprt,ndprt,2)
      logical   ldiel_sum(ndprt,ndprt,2)
      logical   ldiel_tot(ndprt)
c-----------------------------------------------------------------------
      external  r8fun1
c----------------------------------------------------------------------

C-----------------------------------------------------------------------
C****************** MAIN PROGRAM   *************************************
C-----------------------------------------------------------------------
C

C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000
      CALL XXNAME(USER)

       iopt = 1

C Initialize these strings as blanks to avoid null character problem

      sstrgo = ' '
      DO I=1,NDBFIL
        dsn09(I)  = ' '
      END DO

      OPEN14 = .FALSE.
      OPEN17 = .FALSE.
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  ZERO DIELECTRONIC OUTPUT ARRAYS.
C-----------------------------------------------------------------------

       DO 5 IT = 1,NDT
       DO 5 ILEV = 1,NDLEV_04
       DO 5 IP = 1,NDPRT
        DIEL(IT,ILEV,IP) = 0.0D0
    5  CONTINUE

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL OR FILE IF BATCH).
C-----------------------------------------------------------------------

 500  continue
      CALL BCSPF0( REP , DSNXR, IDIST, DPARAM, dsn37)

      kappa = 0.0D0
      druval = 0.0D0
      if (idist.eq.1) kappa  = dparam
      if (idist.eq.3) druval = dparam

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED:  END RUN
C-----------------------------------------------------------------------

      IF (REP.EQ.'YES') THEN
         GOTO 9999
      ENDIF

C-----------------------------------------------------------------------
C COMMUNICATE WITH THE IDL OUTPUT SCREEN
C-----------------------------------------------------------------------

      CALL BCSPF1(DSNPAP, LPAPER, DSN04R , L04R , LPEND)

C-----------------------------------------------------------------------
C IF CANCEL SELECTED THEN RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 500

C-----------------------------------------------------------------------
C OPEN PAPER.TXT FILE - IF REQUESTED
C-----------------------------------------------------------------------

      IF(OPEN17)THEN
         CLOSE(iunt17)
         OPEN17=.FALSE.
      ENDIF
      IF (LPAPER) THEN
         DSN80=' '
         DSN80=DSNPAP
         OPEN(UNIT=iunt17,FILE=DSN80, STATUS='UNKNOWN')
         OPEN17=.TRUE.
      ENDIF

      IF(LPAPER) THEN
         WRITE(17,*)DSNXR
      ENDIF

      OPEN12 = .FALSE.
      LEXIST = .FALSE.
      INQUIRE( FILE=DSNXR   , EXIST=LEXIST)
      IF(LEXIST) THEN
         OPEN(UNIT=iunt12 , FILE = DSNXR, STATUS = 'UNKNOWN')
         OPEN12 = .TRUE.
      ELSE
         WRITE(I4UNIT(-1),1009) DSNXR
         STOP
      ENDIF

C-----------------------------------------------------------------------
C  OBTAIN ALL FILE NAMES AND DATA FROM CROSS-REFERENCE FILE.
C-----------------------------------------------------------------------

      READ(iunt12,1002)DSN
      CALL XXFLNM(DSN, DSNSP, LEXIST)
      IF (.NOT.LEXIST) THEN
         WRITE(I4UNIT(-1),1009) DSNSP
         STOP
      ENDIF
      IF(LPAPER) THEN
         WRITE(iunt17,*)DSNSP
      ENDIF

      NBFIL  = 0
 10   READ(iunt12,1003) DSN
      IF(DSN(1:1).NE.' ') THEN
         NBFIL = NBFIL + 1
         CALL XXFLNM(DSN, dsn09(NBFIL), LEXIST)
         IF (.NOT.LEXIST) THEN
            WRITE(I4UNIT(-1),1009) dsn09(NBFIL)
            STOP
         ENDIF
         IF(LPAPER) THEN
            WRITE(iunt17,*)dsn09(NBFIL)
         ENDIF
         GO TO 10
      ENDIF

      READ(iunt12,1004) DSN
      CALL XXFLNM(DSN, DSNSPO, LEXIST)
      IF(LPAPER) THEN
         WRITE(iunt17,*)DSNSPO
      ENDIF

      READ(iunt12,2004) DSN
C       CALL XXFLNM(DSN, DSNMCO, LEXIST)
C       IF(LPAPER) THEN
C          WRITE(iunt17,*)DSNMCO
C       ENDIF

      NLEV=0
      LSJ = .FALSE.
 20   READ(iunt12,1005,END=29) STRING
      IF(STRING(1:7).NE.'       ') THEN
         NLEV = NLEV + 1
         READ(STRING,1006)ISPRA(NLEV),
     &        (IBDRA(NLEV,IBFIL),IBFIL=1,NBFIL)
         GO TO 20
      ENDIF

C READ NEXT LINE TO CHECK WHETHER J-RESOLVED SPLIT REQUIRED
C IF SO FETCH CROSS-REFERENCING AND WEIGHTING FACTORS FOR THE SPLIT

      READ(iunt12,1005,END=29)STRING
      IF(STRING(1:10).EQ.'J-resolved') THEN
         LSJ = .TRUE.
         READ(iunt12,1005)(STRING,I=1,4)
         NJLEV=0
 21      READ(iunt12,1005,END=29)STRING
         IF(STRING(1:10).NE.'          ') THEN
            NJLEV=NJLEV+1
            READ(STRING,1022)ISPJRA(NJLEV),FSPJRA(NJLEV),
     &           JLREFA(NJLEV)
            GO TO 21
         ELSE
            GO TO 29
         ENDIF
      ENDIF
 29   CONTINUE


C-----------------------------------------------------------------------
C Read in numerical distribution function (if set) from adf37 dataset.
C-----------------------------------------------------------------------

C       if (idist.eq.2) then
C
C          lexist = .false.
C          inquire( file=dsn37 , exist=lexist)
C          if(lexist) then
C          open( unit=iunt37 , file=dsn37 , status='unknown')
C        else
C          write(i4unit(-1),1009) dsn37
C            stop
C          endif
C
C        call xxdata_37( iunt37 ,
C      &                   nemax  , ntmax  ,
C      &                   title  , icateg , nenerg , nblock ,
C      &                   nform1 , param1 , nform2 , param2 ,
C      &                         ea     , fa     , teff   , mode   ,
C      &                         median , filnam , filout , calgeb ,
C      &                   ealgeb
C      &                  )
C
C          close(iunt37)
C
C       endif

C-----------------------------------------------------------------------
C Close unit 12 and re-open to read specific ion input file
C Setup index array to identify any exisiting R-lines.
C-----------------------------------------------------------------------

      IF (OPEN12) CLOSE(iunt12)
      OPEN(UNIT=iunt12 , FILE = DSNSP, STATUS = 'UNKNOWN')
      OPEN12=.TRUE.

      itieactn = 0

      call xxdata_04( iunt12 ,
     &                ndlev_04, ndtrn , ndmet   , ndqdn , nvmax ,
     &                titled , iz    , iz0     , iz1   , bwno  ,
     &                npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                il_04  , qdorb , lqdorb  , qdn   , iorb  ,
     &                ia_04  , cstrga_04       ,
     &                isa_04 , ila_04, xja_04  ,
     &                wa_04  ,
     &                cpla   , npla  , ipla    , zpla  ,
     &                nv     , scef  ,
     &                itran  , maxlev,
     &                tcode  , i1a   , i2a     , aval  , scom  ,
     &                beth   ,
     &                iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                lstyp  , lltyp , itieactn, ltied
     &              )

      close(iunt12)

      call bxttyp( ndlev_04, ndmet  , ndtrn  , nplr  , npli  ,
     &             itran  , tcode  , i1a    , i2a   , aval  ,
     &             icnte  , icntp  , icntr  , icnth , icnti ,
     &             icntl  , icnts  ,
     &             ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &             iltrn  , istrn  ,
     &                               ie1a   , ie2a  , aa    ,
     &                               ip1a   , ip2a  ,
     &                               ia1a   , ia2a  , auga  ,
     &                               il1a   , il2a  , wvla  ,
     &                               is1a   , is2a  , lss04a
     &           )


      do i = 1, icntr
        irtag(i) = ia2a(i)*1000 + ia1a(i)
      end do
      do i = 1, itran
        ittag(i) = i
      end do

C-----------------------------------------------------------------------
C Open 13 for adf09 dielectronic data input - each file in turn
C-----------------------------------------------------------------------

C     Tell the IDL how much computing there is to do

      write(pipeou,*)nbfil
      call xxflsh(pipeou)

      do 80 ibfil = 1 , nbfil

         lexist = .false.

         inquire( file=dsn09(ibfil) , exist=lexist)
         if(lexist) then
            open(unit=iunt13 , file = dsn09(ibfil), status = 'unknown')
         else
            write(i4unit(-1),1009) dsn09(ibfil)
            stop
         endif

         call xxdata_09( iunt13   , ndprt    , ndrep    , ndlrep   ,
     &                   ndlev_09 , ndaug    , ndt      , ndsc     ,
     &                   seqsym   , iz       , iz0      , iz1      ,
     &                   ctype    , iform    ,
     &                   nprnt    , nprnti   , nprntf   , 
     &                   bwnp     ,
     &                   ipa_09   , cstrpa_09, ispa_09  , ilpa_09  ,
     &                   xjpa_09  , wpa_09   ,
     &                   ipca_09  , cstrpca_09          , 
     &                   xjpca_09 , wpca_09  ,
     &                   nlvl_09  , ncfg_09  , nippy_09 , bwnr     ,
     &                   ia_09    , ip_09    , cstrga_09, isa_09   ,
     &                   ila_09   , xja_09   , wa_09    ,
     &                   iscp     , iscn     , iscl     ,
     &                   fsc      , wnsc     ,
     &                   auga_res , lauga_res,
     &                   nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                   irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                   lauga_nl ,
     &                   nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                   irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                   iprti    ,
     &                   diel_res , ldiel_res,
     &                   iprtf    ,
     &                   nsysf    , isys     , ispsys   , cspsys   ,
     &                   diel_nl  , ldiel_nl , diel_n   , ldiel_n  ,
     &                   diel_sum , ldiel_sum, diel_tot , ldiel_tot,
     &                   nte_09   , tea_09
     &                 )
         close(iunt13)

C-----------------------------------------------------------------------
C  Extract necessary data from adf09 files
C
C  Parents from adf09, jlev is map from adf04 to adf09 index.
C  Spline onto adf04 temperature grid.
C-----------------------------------------------------------------------


         do ip = 1, nprnti
            do ilev = 1, il_04

               jlev = ibdra(ilev, ibfil)
               jprt = ip

               if (jlev.GT.0) then
                  do it = 1, nte_09
                     diel(it, ilev, ip) = diel(it, ilev, ip) +
     &                                    diel_res(jlev, jprt, it)
                  end do
               endif

            end do
         end do

C Tell IDL that we have completed one stage for progress window

        write(pipeou,*)one
        call xxflsh(pipeou)

  80  continue  ! end reading adf09

C-----------------------------------------------------------------------
C  Spline onto adf04 temperature grid, get rate from current R line
C  and add on DR contribution.
C  Write output to file supplied from panel - not the one in the
C  adf18 driver file.
C  If total of R-line is LT n_temp*1e-30 do not output line to file.
C-----------------------------------------------------------------------

      if (open14) close(iunt14)
      open(unit=iunt14 , file = dsn04r, status = 'unknown')
      open14=.true.

      nrline = 0
      nrzero = 0

      do it = 1, nv
         xout(it) = dlog10(scef(it))
      end do

      do ip = 1, nprnti
         do ilev = 1, il_04

            do it = 1, nte_09
               xin(it) = dlog10(tea_09(it))
               y = diel(it,ilev,ip)
               if(y.le.0.0d0)then
                  yin(it) = -30.0d0
               else
                  yin(it) = dlog10(y)
               endif
            end do

            lsetx = .FALSE.
            call xxspln( lsetx  , iopt  , r8fun1 ,
     &                   nte_09 , xin   , yin   ,
     &                   nv     , xout  , yout  ,
     &                   dy
     &                   )

            lsetx = .TRUE.
            do it = 1, nv
              yout(it) = 10.0D0**yout(it)
              if (yout(it).lt.1D-30) yout(it) = 1.0D-30
            enddo

            if (icntr.gt.0) then
               i1 = i4indfi4(icntr, irtag, ilev*1000+ip)
               if (i1.NE.-1) then
                  i2 = i4indfi4(itran, ittag, irtrn(i1))
                  do it = 1, nv
                     yout(it) = yout(it) + scom(it, i2)
                  end do
                  nrline = nrline + 1
               endif
            endif

            ysum = 0.0D0
            do it = 1, nv
              ysum = ysum + yout(it)
            end do

            if (ysum.GT.2.0D-30*nv) then
               write(lstrgo,'(2i4,9x,1p,14d9.2)')
     &                       ilev, ip, (yout(it),it=1,nv)
               sstrgo = ' '
               sstrgo(1:16)=lstrgo(1:16)
               sstrgo(7:7)='+'
               sstrgo(1:1)='R'
               do it = 1,nv
                  sstrgo(9+8*it:16+8*it) = lstrgo(9+9*it:13+9*it)//
     &                                     lstrgo(15+9*it:17+9*it)
               end do
               call xxslen(sstrgo,lens1,lens2)
               write(iunt14,'(a)')sstrgo(1:lens2)
            else
               nrzero = nrzero + 1
            endif
         end do
      end do

C Finish with comments

      call xxdate(date)

      write(14,1016)
      do i = 1, nbfil
         string = 'C      '//dsn09(i)
         call xxslen(string,lens1,lens2)
         write(iunt14,'(A)')string(1:lens2)
      end do
      call xxslen(dsnxr, lens1, lens2)
      write(iunt14,1018)dsnxr(1:lens2)

      write(iunt14,1019)
      do ip = 1, nprnti
         write(iunt14,1023) ip, cstrpa_09(ip), ispa_09(ip),
     &                      ilpa_09(ip), xjpa_09(ip)
      end do

      write(iunt14,1028)nrline
      if (nrzero.GT.0) write(iunt14,1029)nrzero

      write(iunt14,3005)
      write(iunt14,3020)'ADAS212', user, date
      if (open14) close(iunt14)


C-----------------------------------------------------------------------
      GO TO 500
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
 9999 CONTINUE

      write(pipeou,*)one
      call xxflsh(pipeou)

      STOP

C-----------------------------------------------------------------------
 1002  FORMAT(/,/,1A80,/,/,/)
 1003  FORMAT(1A80)
 1004  FORMAT(/,/,1A80)
 2004  FORMAT(1A80,/,/,/,/,/,/)
 1005  FORMAT(1A128)
 1006  FORMAT(I7,6I12)
 1009  FORMAT(/ /,'FILE DOES NOT EXIST: ',1A80)
 1016  FORMAT('C'/'C  Dielectronic data is post-processed',
     &        ' from ADAS adf09 files:'/'C')
 1018  FORMAT('C'/'C  Cross-referencing file :',/,'C',/,
     &        'C      ',A,/,'C')
 1019  FORMAT('C'/'C  Parent metastables:',/,'C')
 1022  FORMAT(I7,7X,F10.5,I7)
 1023  FORMAT('C',I6,2X,A20, 1H(,i1,1H),i1,1H(,f4.1,1H))
 1028  FORMAT('C'/'C  Dielectronic data added to ',i3,' existing ',
     &         'radiative recombination levels')
 1029  FORMAT('C'/'C  Number of R-lines removed for zero rates:', i3)
 3005  FORMAT('C   ')
 3020  FORMAT('C',/,
     &        'C  CODE     : ',1A7/
     &        'C  PRODUCER : ',A30/
     &        'C  DATE     : ',1A8,/,'C',/,'C',79('-'))

C-----------------------------------------------------------------------

      END
