       PROGRAM adas218

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: ADAS218 *********************
C
C
C  VERSION:  1.0
C
C  PURPOSE:  To  calculate low-level populations of excited states of
C            ions, including dependence on metastables,  using master
C            condensed files which contain electron impact rate  data
C            (gamma values) as a function of temperature. These files
C            may also contain information on proton impact transitions
C            and recombination coefficients (free electron and  charge
C            exchange).
C
C            Processes can include electron and proton impact,  spon-
C            taneous emission, free electron recombination and charge
C            exchange recombination depending on the input data set.
C
C            ADAS218 is a variant of ADAS208 which can use adf04 files
C            which have upward and downward effective collision
C            strengths characteristic of non-Maxwellian electron
C            energy distribution functions.
C
C
C  DATA:     The source data is contained in adf04 type 3 or 4 datasets.
C
C  PROGRAM:
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C          (I*4)  NDDEN   = PARAMETER = MAX. NUMBER OF DENSITIES ALLOWED
C          (I*4)  NDMET   = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C          (I*4)  NDSEL   = PARAMETER = MAX. NO. OF USER SUPPLIED TRANSITONS
C
C          (I*4)  IUNT27  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT09  = PARAMETER = INPUT UNIT FOR PROTON DATA SET
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR COPASE DATA SET
C          (I*4)  IUNT11  = PARAMETER = OUTPUT UNIT FOR CONTOUR DATA SET
C          (I*4)  IUNT12  = PARAMETER = OUTPUT UNIT FOR MET.POPULATIONS
C                                       PASSING FILE.
C          (I*4)  IUNT13  = PARAMETER = OUTPUT UNIT FOR GEN. COLL. RAD.
C                                       INFORMATION PASSING FILE.
C          (I*4)  IUNT14  = PARAMETER = OUTPUT UNIT FOR ACD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT15  = PARAMETER = OUTPUT UNIT FOR SCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT16  = PARAMETER = OUTPUT UNIT FOR CCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT17  = PARAMETER = OUTPUT UNIT FOR QCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT18  = PARAMETER = OUTPUT UNIT FOR XCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT19  = PARAMETER = OUTPUT UNIT FOR PRB COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT20  = PARAMETER = OUTPUT UNIT FOR PRC COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT21  = PARAMETER = OUTPUT UNIT FOR PLT COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT22  = PARAMETER = OUTPUT UNIT FOR PLS COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT23  = PARAMETER = OUTPUT UNIT FOR MET COLL. RAD.
C                                       PASSING FILE.
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C
C          (R*8)  D1      = PARAMETER = 1.0D0
C          (R*8)  WN2RYD  = PARAMETER = 9.11269D-06
C
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  ICNTI   = NO. OF IONISATIONS TO Z INPUT
C          (I*4)  ICNTL   = NO. OF SATELLITE DR RECOMBINATIONS INPUT
C          (I*4)  ICNTS   = NO. OF IONISATIONS TO Z+1 INPUT
C          (I*4)  IDOUT   = 1 => INPUT DENSITIES IN CM-3
C                         = 2 => INPUT DENSITIES IN REDUCED FORM
C          (I*4)  IFOUT   = 1 => INPUT TEMPERATURES IN KELVIN
C                         = 2 => INPUT TEMPERATURES IN EV
C                         = 3 => INPUT TEMPERATURES IN REDUCED FORM
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C          (I*4)  IRCODE  = RETURN CODE FROM 'FILEINF' IBM ROUTINE
C                           ( 0 => NO ERROR )
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C          (I*4)  ITSEL   = INDEX OF TEMPERATURE SELECTED FOR GRAPH
C                           (FROM INPUT LIST).
C          (I*4)  IZ      =  RECOMBINED ION CHARGE
C          (I*4)  IZ0     =         NUCLEAR CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C          (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C          (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C          (I*4)  NPL     = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                             BY EXCITED STATE IONISATION IN COPASE
C                             FILE WITH IONISATION POTENTIALS GIVEN
C                             ON THE FIRST DATA LINE
C          (I*4)  NPLR    = NO. OF ACTIVE METASTABLES OF (Z+1) ION
C          (I*4)  NPLI    = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C          (I*4)  NPL3    = NO. OF ACTIVE METASTABLES OF (Z+1) ION  WITH
C                             THREE-BODY RECOMBINATION ON.
C          (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  I       = GENERAL PURPOSE INDEX
C          (I*4)  J       = GENERAL PURPOSE INDEX
C          (I*4)  IP      = PARENT INDEX
C          (I*4)  IS      = ENERGY LEVEL ARRAY INDEX
C          (I*4)  IN      = DENSITY ARRAY INDEX
C          (I*4)  IT      = TEMPERATURE ARRAY INDEX
C          (I*4)  ININ    = DENSITY ARRAY INDEX
C          (I*4)  ITIN    = TEMPERATURE ARRAY INDEX
C
C          (R*8)  R8DCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  R8FBCH  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1) OF LOWEST PARENT
C          (R*8)  BWNOA() = IONISATION POTENTIAL (CM-1) OF PARENTS
C          (L*4)  LBSETA()= .TRUE. - PARENT WEIGHT SET FOR BWNOA()
C                         = .FALSE.- PARENT WEIGHT NOT SET FOR BWNOA()
C          (R*8)  PRTWTA()= WEIGHT FOR PARENT ASSOCIATED WITH BWNOA()
C          (R*8)  ZEFF    = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C          (R*8)  ZEFFSQ  = 'ZEFF' * 'ZEFF'
C          (R*8)  DMINT   = +1 or -1 DEPENDING ON WHETHER THE  NUMBER OF
C                           ROW   INTERCHANGES   WAS   EVEN   OR    ODD,
C                           RESPECTIVELY, WHEN INVERTING A MATRIX  USING
C                           'XXMINV'.
C
C          (R*8)  WVLS    = SHORT WAVELENGTH LIMIT FOR PEC & SXB (A)
C          (R*8)  WVLL    = LONG WAVELENGTH LIMIT FOR PEC & SXB  (A)
C          (R*8)  AVLT    = LOWER LIMIT OF A-VALUES FOR PEC & SXB
C
C          (L*4)  LSOLVE  = .TRUE.  => SOLVE LINEAR EQUATION USING
C                                      'XXMINV'.
C                           .FALSE. =>DO NOT SOLVE LINEAR EQUATION USING
C                                     'XXMINV' - INVERT MATRIX ONLY.
C          (L*4)  LDOUT   = .TRUE.  => OUTPUT DATA GENERATED FOR INPUT
C                                      FILE.
C                           .FALSE. => NO OUTPUT DATA GENERATED FOR
C                                      INPUT FILE.
C          (L*4)  LCONT   = .TRUE.  => OUTPUT DATA TO CONTOUR PASSING
C                                      FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      CONTOUR PASSING FILE.
C          (L*4)  LPASS   = .TRUE.  => OUTPUT DATA TO METPOP PASSING
C                                      FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      METPOP PASSING FILE.
C          (L*4)  LGCR    = .TRUE.  => OUTPUT DATA TO GEN. COLL. RAD.
C                                      PASSING FILE
C                           .FALSE. => NO OUTPUT TO GEN. COLL. RAD.
C                                      PASSING FILE.
C          (L*4)  LDSEL   = .TRUE.  => COPASE DATA SET INFORMATION
C                                      TO BE DISPLAYED BEFORE RUN.
C                         = .FALSE. => COPASE DATA SET INFORMATION
C                                      NOT TO BE DISPLAYED BEFORE RUN.
C          (L*4)  LNORM   = .TRUE.  => IF NMET=1 NORMALISE TOTAL AND
C                                      SPECIFIC LINE POWER OUTPUT FILES
C                                      PLT & PLS TO STAGE TOT. POPULATN.
C                         = .FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                      METASTABLE POPULATIONS.
C          (L*4)  LPSEL   = .TRUE.  => INCLUDE PROTON COLLISIONS
C                         = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C          (L*4)  LZSEL   = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                      PLASMA Z EFFECTIVE'ZEFF'.
C                         = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                      WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                         (ONLY USED IF 'LPSEL=.TRUE.')
C          (L*4)  LIOSEL  = .TRUE.  => INCLUDE IONISATION RATES
C                         = .FALSE. => DO NOT INCLUDE IONISATION RATES
C          (L*4)  LHSEL   = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                      NEUTRAL HYDROGREN.
C                         = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                      FROM NEUTRAL HYDROGREN.
C          (L*4)  LRSEL   = .TRUE.  => INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C          (L*4)  LISEL   = .TRUE.  => INCLUDE ELECTRON IMPACT
C                                      IONISATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C          (L*4)  LNSEL   = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                      FROM DATAFILE IF AVAILABLE
C                         = .FALSE. => DO NOT INCLUDE PROJECTED BUNDLE-N
C                                      DATA
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SETS
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SETS
C          (L*4)  LPDATA  = .TRUE.  => PROJECTED DATA EXISTS AND IS SET
C                         = .FALSE. => PROJECTED DATA DOES NOT EXSIST
C                                      OR IS NOT SET
C          (L*4)  OPEN9   = .TRUE.  => FILE ALLOCATED TO UNIT 9.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 9.
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  OPEN11  = .TRUE.  => FILE ALLOCATED TO UNIT 11.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 11.
C          (L*4)  OPEN12  = .TRUE.  => FILE ALLOCATED TO UNIT 12.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 12.
C          (L*4)  OPEN13  = .TRUE.  => FILE ALLOCATED TO UNIT 13.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 13.
C          (L*4)  OPEN14  = .TRUE.  => FILE ALLOCATED TO UNIT 14.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 14.
C          (L*4)  OPEN15  = .TRUE.  => FILE ALLOCATED TO UNIT 15.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 15.
C          (L*4)  OPEN16  = .TRUE.  => FILE ALLOCATED TO UNIT 16.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 16.
C          (L*4)  OPEN17  = .TRUE.  => FILE ALLOCATED TO UNIT 17.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 17.
C          (L*4)  OPEN18  = .TRUE.  => FILE ALLOCATED TO UNIT 18.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 18.
C          (L*4)  OPEN19  = .TRUE.  => FILE ALLOCATED TO UNIT 19.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 19.
C          (L*4)  OPEN20  = .TRUE.  => FILE ALLOCATED TO UNIT 20.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 20.
C          (L*4)  OPEN21  = .TRUE.  => FILE ALLOCATED TO UNIT 21.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 21.
C          (L*4)  OPEN22  = .TRUE.  => FILE ALLOCATED TO UNIT 22.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 22.
C          (L*4)  OPEN23  = .TRUE.  => FILE ALLOCATED TO UNIT 23.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 23.
C
C          (C*1)  CPLA()  = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IA()'
C                                INTEGER - PARENT IN BWNOA() LIST
C                                'BLANK' - PARENT BWNOA(1)
C                                  'X'   - DO NOT ASSIGN A PARENT
C          (I*4)  NPLA()  = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C          (I*4)  IPLA(,) = PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (I*4)  ZPLA(,) = EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                         = 'NO ' => CONTINUE PROGRAM EXECUTION.
C          (C*3)  TITLED  = ELEMENT SYMBOL.
C          (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C          (C*10)  UID    = USER IDENTIFIER
C          (C*30) USER    = PRODUCERS'S REAL NAME
C          (C*80) DSNOUT  = OUTPUT CONTOUR DATA SET NAME (SEQUENTIAL)
C          (C*80) DSNPAS  = OUTPUT PASSING FILE NAME (SEQUENTIAL)
C          (C*26) DSNGCR  = OUTPUT GCR PASSING FILE NAME (SEQUENTIAL)
C          (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C          (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C          (C*44) DSNINP  = INPUT PROTON DATA SET NAME (SEQUENTIAL)
C                                      NOT USED AT PRESENT
C          (C*44) DSNEXP  = EXPANSION DATA SET NAME
C          (C*44) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C          (C*44) DSN44   = 44 BYTE CHARACTER STRING  FOR  TEMPORARY
C                           STORAGE OF DATA SET NAMES FOR DYNAMIC ALLOC.
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IITRN() = ELECTRON IMPACT IONISATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT IONISATIONS FROM THE LOWER STAGE
C          (I*4)  ILTRN() = SATELLITE DR RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT SATELLITE DR RECOMBINATIONS.
C          (I*4)  ISTRN() = ELECTRON IMPACT IONISATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT IONISATIONS TO UPPER STAGE ION.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (I*4)  IA1A()  = AUGER TRANSITION:
C                            PARENT ENERGY LEVEL INDEX
C          (I*4)  IA2A()  = AUGER TRANSITION:
C                            RECOMBINED ION ENERGY LEVEL INDEX
C          (R*8)  AUGA()  = AUGER TRANSITION: AUG-VALUE (SEC-1)
C                            RECOMBINED ION ENERGY LEVEL INDEX
C          (I*4)  IL1A()  = SATELLITE DR TRANSITION:
C                            RECOMNINING ION  INDEX
C          (I*4)  IL2A()  = SATELLITE DR TRANSITION:
C                            RECOMBINED ION INDEX
C          (R*8)  WVLA()  = SATELLITE DR TRANSITION: PARENT WVLGTH.(A)
C                            DR SATELLITE LINE INDEX
C          (I*4)  IS1A()  = IONISING TRANSITION:
C                            IONISED ION  INDEX
C          (I*4)  IS2A()  = IONISING TRANSITION:
C                            IONISING ION INDEX
C
C          (I*4)  IBSELA(,)=IONISATION DATA BLOCK SELECTION INDICES
C                            1ST DIMENSION - (Z) ION METASTABLE COUNT
C                            2ND DIMENSION - (Z+1) ION METASTABLE COUNT
C
C          (R*8)  PAR(,)  =
C          (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  AA()    = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  TINE()  = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C          (R*8)  TINP()  = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C          (R*8)  TINH()  = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C          (R*8)  TEVA()  = ELECTRON TEMPERATURES (UNITS: EV)
C          (R*8)  TPVA()  = PROTON TEMPERATURES   (UNITS: EV)
C          (R*8)  THVA()  = NEUTRAL HYDROGEN TEMPERATURES (UNITS: EV)
C          (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C          (R*8)  TPA()   = PROTON TEMPERATURES   (UNITS: KELVIN)
C          (R*8)  THA()   = NEUTRAL HYDROGEN TEMPERATURES (UNITS:KELVIN)
C          (R*8)  DINE()  = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C          (R*8)  DINP()  = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C          (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C          (R*8)  DENSPA()= PROTON DENSITIES    (UNITS: CM-3)
C          (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C          (R*8)  RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION: TEMP/DENS INDEX
C                               2ND DIMENSION: PARENT INDEX
C          (R*8)  RATMIA(,)= RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION: TEMP/DENS INDEX
C                               2ND DIMENSION: PARENT INDEX
C          (R*8)  RHS()   = USED ONLY IF 'LSOLVE=.TRUE.' WHEN CALLING
C                           THE SUBROUTINE 'XXMINV'. CONTAINS THE SET
C                           OF 'N' LINEAR EQUATIONS TO BE SOLVED.
C                           INPUT  TO   'XXMINV': RIGHT HAND SIDE VECTOR
C                           OUTPUT FROM 'XXMINV': SOLUTION VECTOR
C                           (ACTS ONLY AS A DUMMY IN THIS PROGRAM)
C          (R*8)  CIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                           FIXED TEMPERATURE.
C                            DIMENSION: ENERGY LEVEL INDEX
C                             DIRECT  AND INDIRECT COUPLINGS
C          (R*8)  CIEPR(,)= PARENT RESOLVED IONISATION RATE COEFFICIENT
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT  AND INDIRECT COUPLINGS
C          (R*8)  DCIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                           FIXED TEMPERATURE.
C                             DIRECT COUPLING ONLY
C                            DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  DCIEPR(,)= PARENT RESOLVED IONISATION RATE COEFFT.
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT COUPLING ONLY
C          (R*8)  V3PR(,) = PARENT RESOLVED THREE-BODY RECOM. COEFFT.
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT  AND INDIRECT COUPLINGS
C          (R*8)  DV3PR(,)= PARENT RESOLVED THREE-BODY RECOM. COEFFT.
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT  COUPLING ONLY
C          (R*8)  VHRED(,)= CHARGE EXCHANGE RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C          (R*8)  VRRED(,)= FREE ELECTRON RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C          (R*8)  VIRED(,)= ELECTRON IMPACT IONISATION FOR STAGE (Z-1):
C                           VECTOR  OF IONISATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: (Z-1) PARENT INDEX
C          (R*8)  VIONR(,)= ELECTRON IMPACT IONISATION FROM STAGE (Z):
C                           VECTOR  OF IONISATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: (Z+1) PARENT INDEX
C
C          (R*8)  SGRDA(,,)= GROUND AND METASTABLE IONISATION RATE COEFF
C                            RETURNED FROM IONELEC FILE TO BE USED IN
C                            EVALUATING THE PHOTON EFFICIENCY.
C                            1ST DIMENSION: TEMPERATURES
C                            2ND DIMENSION: (Z)   ION METASTABLE INDEX
C                            3ND DIMENSION: (Z+1) ION METASTABLE INDEX
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  EXCRE(,) = ELECTRON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRE(,)= ELECTRON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  EXCRP(,) = PROTON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRP(,)= PROTON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  VECH(,,) = CHARGE-EXCHANGE RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C          (R*8)  VECR(,,) = FREE ELECTRON RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C                              DIRECT AND INDIRECT COUPLINGS
C          (R*8)  DVECR(,,) = FREE ELECTRON RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C                              DIRECT COUPLING ONLY
C          (R*8)  VECI(,,) = ELECTRON IMPACT IONISATION:
C                            SPLINED IONISATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C          (R*8)  CRA(,)   = A-VALUE  (sec-1)  MATRIX  COVERING  ALL
C                            TRANSITIONS.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCE(,)  = ELECTRON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCP(,)  = PROTON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CC(,)    = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  CMAT(,)  = (INVERTED)   RATE  MATRIX  COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            PRE  'XXMINV' : NOT-INVERTED
C                            POST 'XXMINV' : INVERTED
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C          (R*8)  CRED(,)  = MATRIX  OF  TRANSITION   RATES   BETWEEN
C                            METASTABLE LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C          (R*8)  CRMAT(,) = INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            BEFORE INPUT  TO   XXMINV: NOT INVERTED
C                            AFTER  OUTPUT FROM XXMINV: AS-ABOVE
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C
C          (R*8) POPAR(,,)= LEVEL POPULATIONS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*4) STVR(,,,) = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*4) STVH(,,,) = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*4) STVI(,,,) = ORDINARY EXCITED LEVEL:
C                             ELECTRON IMPACT IONISATION    COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*4) STACK(,,,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C          (R*8) STVRM(,,,)= METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*8) STVHM(,,,)= METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*8) STVIM(,,,)= METASTABLE LEVEL:
C                             ELECTRON IMPACT IONISATION    COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C                           'I' => Electron Impact Ionisation
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES
C          (C*22) STRGA() = LEVEL DESIGNATIONS
C          (C*70) TMETA(,)= HEADING STRINGS FOR B8GETS PANELS
C                           1ST DIMENSION: (Z)   ION METASTABLE INDEX
C                           2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C
C          (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                       'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C          (L*4)  LTRNG(,)= .TRUE.  => TEMPERATURE VALUE WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                         = .FALSE. =>TEMPERATURE VALUE NOT WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                           1st DIMENSION: TEMPERATURE INDEX.
C                           2nd DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C          (L*4)  LSSETA(,)= .TRUE.  - MET. IONIS RATE SET IN B8GETS
C                            .FALSE.- MET. IONIS RATE NOT SET IN B8GETS
C                           1ST DIMENSION: (Z) ION METASTABLE INDEX
C                           2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C          (L*4)  LSS04A(,)= .TRUE. => IONIS. RATE SET IN ADF04 FILE:
C                            .FALSE.=> NOT SET IN ADF04 FILE
C                            1ST DIM: LEVEL INDEX
C                            2ND DIM: PARENT METASTABLE INDEX
C          (R*8)  PLAS1() =  DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                            POWER TRANSITION GIVEN BY 'ISPTRN'.
C                            (UNITS: ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C          (R*8)  SWVLN() =  WAVELENGTH (ANGSTROM) FOR SPECIFIC LINE
C                            POWER TRANSITION GIVEN BY 'ISPTRN'.
C                            (UNITS: ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C          (I*4)  ISPTRN()=  SPECIFIC LINE POWER: SELECTED ELECTRON
C                            IMPACT TRANSITION INDEX.
C                             1st DIMENSION: METASTABLE  INDEX
C
C          (R*8)  PL(,,)  = TOTAL LINE POWERS FOR METASTABLES. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE.
C                            => P(TOTAL)/N(IMET)        (ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C          (R*8)  PLA(,)  = TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C          (R*8)  PS(,,,) = SPECIFIC LINE POWERS FOR METASTABLES.
C                           THIS IS  ORGINATING IN THE COLLISIONAL-
C                           RADIATIVE SENSE FROM THE METASTABLE AND
C                           TERMINATING ON EACH METASTABLE.
C                            => P(LINE)/N(IMET)        (ERGS SEC-1)
C                             1ST DIMENSION: METASTABLE LINE INDEX
C                             2ND DIMENSION: METASTABLE  INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY     INDEX
C          (R*8)  PSA(,,) =  EQUILIBRIUM SEPCIFIC LINE POWER COEFFTS.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1ST DIMENSION: METASTABLE LINE INDEX
C                             2ND DIMENSION: TEMPERATURE INDEX
C                             3RD DIMENSION: DENSITY     INDEX
C          (R*8)  PH(,,)  = TOTAL CX LINE POWER FOR PARENT. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           PARENT.
C                            => P(TOTAL)/N(IP)          (ERGS SEC-1)
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: DENSITY     INDEX
C                             3RD DIMENSION: PARENT METASTABL INDEX
C          (R*8)  PHA(,)  =  EQUILIBRIUM CX POWER COEFFT.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: DENSITY     INDEX
C          (R*8)  PLA1()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                           (UNITS: ERGS SEC-1) (DIMENSION: LEVEL INDEX)
C          (R*8)  PLBA(,) = HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                           FROM A PARTICULAR METASTABLE TO LEVELS
C                           'IPROJ' UPWARDS. (UNITS: ERGS CM3 SEC-1)
C                             => P/(DENS*N(IMET))
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                            (NOT IMPLEMENTED IN THIS CODE )
C          (R*8)  PR(,,)  = RECOM/BREMS. COEFFT (ERG S-1)
C                             1ST DIM: PARENT INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C          (r*8)  qdorb() = quantum defects for orbitals
C                           1st dim: index for nl orbital (cf i4idfl.for)
C          (l*4)  lqdorb()= .true.  => source data available for qd.
C                         = .false. => source data not available qd.=0.0
C          (r*8)  qdn()   = quantum defect for n-shells.  non-zero only
C                           for adf04 files with orbital energy data
C                           1st. dim: n-shell (1<=n<=ndqdn)
C          (i*4)  iorb    = input data file: number of orbital energies
C          (i*4)  beth()  = transition
C                           1st Bethe coefficient     (case ' ','1','2')
C          (i*4)  iadftyp = adf04 type: 1=omega, 3=upsilon, 4=non-maxwl.
C          (l*4)  lprn    = .true.  => multiple parent data on 1st line
C                         = .false. => multiple parent data not present
C          (l*4)  lcpl    = .true.  => parent assignment on level lines
C                         = .false. => parent assignment not present
C          (l*4)  lorb    = .true.  => orbital data on level terminator
C                         = .false. => orbital data not present
C          (l*4)  lbeth   = .true.  => bethe data on e-transition lines
C                         = .false. => bethe data not present
C          (l*4)  letyp   = .true.  => e- excitation transitions present
C          (l*4)  lptyp   = .true.  => p- excitation transitions present
C          (l*4)  lrtyp   = .true.  => recombination transitions present
C          (l*4)  lhtyp   = .true.  => cx transitions present
C          (l*4)  lityp   = .true.  => ionis. trans. from z-1 ion present
C          (l*4)  lstyp   = .true.  => ionis. trans. from current ion present
C          (l*4)  lltyp   = .true.  => 'l'-line for unresolved DR emissivity
C          (l*4)  ltied() = .true.  => specified level tied
C                         = .false. => specified level is untied
C                           dimension => level index
C          (i*4)  itieactn= 1 return data even if some levels are untied.
C                           0 default behaviour - terminate if untied
C                             levels are present.
C                           On output 1 if untied levels present
C                                     0 for no untied levels.
C          (i*4)  ndqdn   = parameter = max. number of n-shells for quantum
C                                       defects
C          (i*4)  nvmax   = max. number of temperatures that can be read in.
C
C
C NOTE:
C
C          INPUT/OUTPUT STREAM ALLOCATIONS:
C          --------------------------------
C
C          STREAM  9: INPUT - PROTON IMPACT INPUT DATA FILE
C          ('IUNT09')
C
C          STREAM 10: INPUT - SPECIFIC ION RATE DATA INPUT FILE FROM
C          ('IUNT10')         DATABASE (SEE DATA SECTION ABOVE).
C
C          STREAM  7: OUTPUT - MAIN OUTPUT TABLES
C          ('IUNT27')
C
C          STREAM 11: OUTPUT - POPULATION  DATA  FOR DIAGNOSTIC USE, IF
C          ('IUNT11')          REQUESTED.   THIS DATA IS FOR SUBSEQUENT
C                              INPUT  INTO  THE  DIAGNOSTIC AND CONTOUR
C                              GRAPHING PROGRAM 'CONTOUR'.
C
C          STREAM 12: OUTPUT - METASTABLE POPULATION DATA, IF REQUESTED.
C          ('IUNT12')          THIS  DATA IS IN THE FORM  OF  A  PASSING
C                              FILE.
C
C          STREAM 13: OUTPUT - GEN. COLL. RAD. DATA, IF REQUESTED.
C          ('IUNT13')          THIS  DATA IS IN THE FORM  OF  A  PASSING
C                              FILE.
C
C          EXPLANATION OF OUTPUT FILE TO STREAM 7
C          --------------------------------------
C
C          'I'    = COMPLETE   LEVEL INDEX
C          'IMET' = METASTABLE LEVEL INDEX
C          'JMET' = METASTABLE LEVEL INDEX
C          'IORD' = ORDINARY EXCITED LEVEL INDEX
C
C          OPTIONAL POPULATION GRAPHS CAN BE PRODUCED. A SEPARATE GRAPH
C          IS DISPLAYED FOR EACH SINGLE INPUT TEMPERATURE SELECTED FROM
C          THE INPUT TEMPERATURE SET.
C
C          INPUT TEMPERATURES 'TIN?()' ARE CONVERTED TO KELVIN 'T?VA()'.
C
C          POPULATION OF METASTABLE 'IMET'      =>  N(IMET)/N(1)
C          POPULATION OF LEVEL 'IORD', DEPENDENT ON METASTABLE 'IMET'
C                                               =>  N(IORD)/N(IMET)
C
C
C          EXPLANATION OF MATRICES:  CRA(,) , CRCE(,) , CRCP(,) :
C          ------------------------------------------------------
C
C          (IF   'LIOSEL=.FALSE.'   THIS  EXPLANATION  ALSO  VALID  FOR
C           CC(,)  AND  CRED(,) )
C
C                      initial level  (column)
C                 --------------------------------
C                 |   *1  R12  R13  R14  R15  R16
C          final  |  R21   *2  R23  R24  R25  R26
C          level  |  R31  R32   *3  R34  R35  R36
C                 |  R41  R42  R43   *4  R45  R46
C          (row)  |  R51  R52  R53  R54   *5  R56
C                 |  R61  R62  R63  R64  R65   *6
C
C
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C               Diagonal elements represent the negative sum of their
C               respective columns i.e.
C
C                                   6
C                  diagonal: *n = -SUM( Rjn )  ( Rnn = 0)
C                                  j=1
C
C                  e.g. *3 = - ( R13 + R23 + R43 + R53 + R63 )
C
C                  Therefore each column adds to zero.
C
C                  If Rfi = transition rate from i -> f then
C                  *n = - ( total transition rate from level n )
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B8GETP     ADAS      FETCH EXPANSION DATA
C          B8SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          B8ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          B8SPF1     ADAS      GATHERS OUT FILE NAMES VIA ISPF PANELS
CX          B8NORM     ADAS      PERFORM STAGE POPULATION NORMALISATION
C          B8WR12     ADAS      OUTPUT DATA TO METPOP PASSING FILE
C          B8OUT1     ADAS      OUTPUT MAIN RESULTS TO OUTPUT STREAM
C          BADATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          B8TTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          B8SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          B8OUT0     ADAS      OUTPUT ION SPEC. ETC. TO OUTPUT STREAM
C          BXCHKM     ADAS      CHECKS IF TRANSITION EXIST TO METASTABLE
C          BXIORD     ADAS      SETS  UP ORDINARY LEVEL INDEX.
C          BXRATE     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C          B8RCOM     ADAS      ESTABLISHES RECOMBINATION RATE COEFFTS.
C          BXMCRA     ADAS      CONSTRUCTS A-VALUE MATRIX.
C          BXMCRC     ADAS      CONSTRUCTS EXC./DE-EXC. RATE COEF MATRIX
C          B8MCCA     ADAS      CONSTRUCTS WHOLE RATE MATRIX.
C          BXMCMA     ADAS      CONSTRUCTS ORDINARY LEVEL RATE MATRIX.
C          BXSTKA     ADAS      STACK UP ORDINARY POP. DEPENDENCE ON MET
C          B8STKB     ADAS      STACK UP RECOMB. CONTRIBUTION FOR ORD.
C          BXSTKC     ADAS      STACK UP TRANSITION RATE BETWEEN METS.
C          B8STKD     ADAS      STACK UP RECOMB RATE FOR EACH MET. LEVEL
C          B8STKE     ADAS      STACK UP RECOMB(+3-BODY) CONTRI.FOR ORD.
C          B8STKF     ADAS      STACK UP RECOMB(+3-BODY) FOR EACH MET.
C          BXMPOP     ADAS      CALCULATE BASIC MET. LEVEL POPULATIONS.
C          B8STVM     ADAS      CALCULATE MET. LEVEL RECOMB. COEFFTS.
C          B8POPM     ADAS      CALCULATE METASTABLE LEVEL POPULATIONS.
C          B8POPO     ADAS      CALCULATE ORDINARY LEVEL POPULATIONS.
C          B8OUTG     ADAS      OUTPUT GRAPHS USING GHOST80
C          B8WR11     ADAS      OUTPUT DATA TO CONTOUR PASSING FILE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXGUID     ADAS      GATHERS USER NAME AS 10 BYTE STRING
C          XXERYD     ADAS      CONVERTS ENERGIES FROM W.NO. TO RYDBERGS
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO EV.
C          XXDCON     ADAS      CONVERTS ISPF ENTERED DENS. TO CM-3.
C          XXRATE     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C                               FOR UNIT GAMMA VALUE.
C          XXMINV     ADAS      INVERTS MATRIX AND SOLVES EQUATIONS.
C          R8DCON     ADAS      REAL*8 FUNCTION: CONVERT DENSITY TO CM-3
C
C
C-----------------------------------------------------------------------
C
C Based on adas208 as modified by Paul Bryans / H P Summers (24-01-2005).
C
C
C VERSION : 1.1
C DATE    : 27-03-2103
C MODIFIED: Martin O'Mullane
C           - First central ADAS version.
C
C-----------------------------------------------------------------------
       INTEGER   NDLEV  , NDTRN  , NDTEM  , NDDEN  , NDMET
       integer   nvmax  , ndqdn  , NDWVL  , ndsel
       INTEGER   IUNT27 , IUNT09 , IUNT10 , IUNT11 , IUNT12 , IUNT13 ,
     &           IUNT14 , IUNT15 , IUNT16 , IUNT17 , IUNT18 , IUNT19 ,
     &           IUNT20 , IUNT21 , IUNT22 , IUNT23
       INTEGER   L1     , L2     , ICSTMX
       INTEGER   ISTOP  , PIPEIN
C-----------------------------------------------------------------------
       REAL*8    D1     , WN2RYD
C-----------------------------------------------------------------------
       CHARACTER PRGTYP*1   , CADAS*80
C-----------------------------------------------------------------------
       PARAMETER( NDLEV =2000, NDTRN = 520000,
     &            NDTEM = 35 , NDDEN = 24    , NDMET =13,
     &            ndqdn = 8  , nvmax = 14    , NDWVL = 5,
     &            ndsel = 100)
       PARAMETER( IUNT27=27, IUNT09=9 , IUNT10=10, IUNT11=11, IUNT12=12)
       PARAMETER( IUNT13=13, IUNT14=14, IUNT15=15, IUNT16=16, IUNT17=17)
       PARAMETER( IUNT18=18, IUNT19=19, IUNT20=20, IUNT21=21, IUNT22=22)
       PARAMETER( IUNT23=23)
       PARAMETER( L1=1     , L2=2     , ICSTMX=11 )
C-----------------------------------------------------------------------
       PARAMETER( D1 = 1.0D0 , WN2RYD = 9.11269D-06 )
       PARAMETER( PRGTYP = 'M' )
       PARAMETER( PIPEIN = 5 )
C-----------------------------------------------------------------------
       INTEGER   IPAN      ,
     &           IZ        , IZ0       , IZ1     ,
     &           NPL       , NPLR      , NPLI    , NPL3    ,
     &           IL        , NV        , ITRAN   ,
     &           MAXLEV    , NMET      , NORD    , IFOUT   , IDOUT   ,
     &           MAXT      , MAXD      , ITSEL   ,
     &           ICNTE     , ICNTP     , ICNTR   , ICNTH   , ICNTI   ,
     &           ICNTL     , ICNTS     ,
     &           IFORM     , IN        , IS      , IT      , IMET    ,
     &           JMET      , IP        , I       ,
     &           ITIN      , ININ      , IX      , ilev    , ipp     ,
     &           iorb      , iadftyp   , itieactn, nwvl    , ntrsel
C-----------------------------------------------------------------------
       REAL*8    R8DCON    , R8NECIP   , ALFRED
       REAL*8    BWNO      , ZEFF      , ZEFFSQ  , DMINT
       REAL*8    XL1IN     , XU1IN     , X       ,
     &           XL1       , XU1       , YL1     , YU1
       REAL*8    R8EXPE    , SPREF     , spref3
C-----------------------------------------------------------------------
       CHARACTER REP*3     , TITLED*3  , DATE*8    , UID*10     ,
     &           DSNOUT*132,
     &           DSNPAS*132, DSNGCR*132, DSNEXP*132, DSNPAP*132 ,
     &           DSNINP*132, DSNINC*132, DSFULL*132,
     &           TITLE*40  , GTIT1*40  , TMET*70   , DSN80*132  ,
     &           USER*30
C-----------------------------------------------------------------------
       LOGICAL   OPEN27
       LOGICAL   OPEN9     , OPEN10    , OPEN11  , OPEN12  , OPEN13  ,
     &           OPEN14    , OPEN15    , OPEN16  , OPEN17  , OPEN18  ,
     &           OPEN19    , OPEN20    , OPEN21  , OPEN22  , OPEN23
       LOGICAL   LGHOST    ,
     &           LPEND     , LPSEL     , LZSEL   , LIOSEL  ,
     &           LHSEL     , LRSEL     , LISEL   , LNSEL   , LGPH    ,
     &           LGRD1     , LDEF1     , LCONT   , LPASS   , LGCR    ,
     &           LSOLVE    , LPDATA    , LNORM
       LOGICAL   LNEWPA    , LPAPER    , OPUT
       logical   lprn      , lcpl      , lorb    , lbeth
       logical   letyp     , lptyp     , lrtyp   , lhtyp   ,
     &           lityp     , lstyp     , lltyp
C-----------------------------------------------------------------------
       INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     , NPLA(NDLEV)  ,
     &           IA(NDLEV)        ,
     &           ISA(NDLEV)       , ILA(NDLEV)       ,
     &           I1A(NDTRN)       , I2A(NDTRN)       ,
     &           IBSELA(NDMET,NDMET), ISSETA(NDMET,NDMET),
     &           IPLA(NDMET,NDLEV)
       INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &           IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &           IITRN(NDTRN)     , ILTRN(NDTRN)     ,
     &           ISTRN(NDTRN)     ,
     &           IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &           IP1A(NDTRN)      , IP2A(NDTRN)      ,
     &           IA1A(NDTRN)      , IA2A(NDTRN)      ,
     &           IL1A(NDLEV)      , IL2A(NDLEV)      ,
     &           IS1A(NDLEV)      , IS2A(NDLEV)      ,
     &           ISPTRN(NDMET)
      integer    itrlow(ndsel)    , itrup(ndsel)
C-----------------------------------------------------------------------
       REAL*4    STVR(NDLEV,NDTEM,NDDEN,NDMET) ,
     &           STVH(NDLEV,NDTEM,NDDEN,NDMET) ,
     &           STVI(NDLEV,NDTEM,NDDEN,NDMET)
       REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
       REAL*8    SCEF(14)        , BWNOA(NDMET)     , PRTWTA(NDMET) ,
     &           TINE(NDTEM)     , TINP(NDTEM)      , TINH(NDTEM)   ,
     &           TEVA(NDTEM)     , TPVA(NDTEM)      , THVA(NDTEM)   ,
     &           TEA(NDTEM)      , TPA(NDTEM)       , THA(NDTEM)    ,
     &           DINE(NDDEN)     , DINP(NDDEN)      ,
     &           DENSA(NDDEN)    , DENSPA(NDDEN)    ,
     &           RATHA(NDDEN)    ,RATPIA(NDDEN,NDMET),
     &           RATMIA(NDDEN,NDMET),
     &           AA(NDTRN)       , AVAL(NDTRN)      , AUGA(NDTRN)   ,
     &           WVLA(NDLEV)     ,
     &           XJA(NDLEV)      , WA(NDLEV)        ,
     &           XIA(NDLEV)      , ER(NDLEV)        ,
     &           CIE(NDLEV)      , RHS(NDLEV)       ,
     &           CIEPR(NDLEV,NDMET) , V3PR(NDLEV,NDMET)  ,
     &           VRRED(NDMET,NDMET) , VHRED(NDMET,NDMET) ,
     &           VIRED(NDMET,NDMET) , VIONR(NDMET,NDMET) ,
     &           VCRPR(NDMET,NDMET) , ZPLA(NDMET,NDLEV)
       REAL*8    TSCEF(14,3)                , SCOM(14,NDTRN)           ,
     &           SGRDA(NDTEM,NDMET,NDMET)   , ESGRDA(NDTEM,NDMET,NDMET),
     &           SMETA(NDTEM,NDMET,NDMET)   , ESMETA(NDTEM,NDMET,NDMET),
     &           SORDA(NDTEM,NDLEV,NDMET)   , ESORDA(NDTEM,NDLEV,NDMET),
     &           EXCRE(NDTEM,NDTRN)         , EXCRP(NDTEM,NDTRN)       ,
     &           DEXCRE(NDTEM,NDTRN)        , DEXCRP(NDTEM,NDTRN)      ,
     &           VECH(NDTEM,NDLEV,NDMET)    , VECR(NDTEM,NDLEV,NDMET)  ,
     &           VECI(NDTEM,NDLEV,NDMET)    , CRA(NDLEV,NDLEV)         ,
     &           CRCE(NDLEV,NDLEV)          , CRCP(NDLEV,NDLEV)        ,
     &           CC(NDLEV,NDLEV)            , CMAT(NDLEV,NDLEV)        ,
     &           CRED(NDMET,NDMET)          , CRMAT(NDMET,NDMET)       ,
     &           rorda(ndtem,ndlev,ndmet)   , rmeta(ndtem,ndlev,ndmet)
       REAL*8    DVECR(NDTEM,NDLEV,NDMET)
       REAL*8    DCIEPR(NDTEM,NDLEV,NDMET)   , DV3PR(NDTEM,NDLEV,NDMET)
       REAL*8    DCIE(NDTEM,NDLEV)
       REAL*8    PCC(NDLEV,NDLEV)              , PCIE(NDLEV)
       REAL*8    PCIEPR(NDLEV,NDMET)           , PV3PR(NDLEV,NDMET)
       REAL*8    PVCRPR(NDMET,NDMET)           , PVECR(NDLEV,NDMET)
       REAL*8    PR(NDMET,NDTEM,NDDEN)
       REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
       REAL*8    STCKM(NDMET,NDTEM,NDDEN)      ,
     &           STVRM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           STVHM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           STVIM(NDMET,NDTEM,NDDEN,NDMET)
       REAL*8    FVCRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVRRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVHRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVIRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVIONR(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVCRPR(NDMET,NDMET,NDTEM,NDDEN)
       REAL*8    PLA1(NDLEV)           ,
     &           PLAS1(NDMET)          , SWVLN(NDMET)                ,
     &           PLA(NDTEM,NDDEN)      , PSA(NDMET,NDTEM,NDDEN)      ,
     &           PHA(NDTEM,NDDEN)      ,
     &           PLBA(NDMET,NDTEM)
       REAL*8    PL(NDMET,NDTEM,NDDEN) , PS(NDMET,NDMET,NDTEM,NDDEN) ,
     &           PH(NDTEM,NDDEN,NDMET)
       real*8    avlt(ndwvl)     , wvls(ndwvl)      , wvll(ndwvl)
       real*8    qdorb((ndqdn*(ndqdn+1))/2)      , qdn(ndqdn)  ,
     &           beth(ndtrn)
C-----------------------------------------------------------------------
       CHARACTER TCODE(NDTRN)*1  , CSTRGA(NDLEV)*18 , CSTRGB(NDLEV)*18 ,
     &           STRGA(NDLEV)*22  ,
     &           TMETA(NDMET,NDMET)*70 , CPLA(NDLEV)*1
       CHARACTER CPRTA(NDMET)*9  , STRGMF(NDMET)*11 , STRGMI(NDLEV)*12
C-----------------------------------------------------------------------
       LOGICAL   LMETR(NDMET)    , LTRNG(NDTEM,3)   , LBSETA(NDMET)
       LOGICAL   LSSETA(NDMET,NDMET)
       LOGICAL   LSS04A(NDLEV,NDMET)
       logical   lqdorb((ndqdn*(ndqdn+1))/2)
       logical   ltied(ndlev)
C-----------------------------------------------------------------------
       DATA IPAN/0/
       DATA OPEN27 /.FALSE./
       DATA OPEN9 /.FALSE./ , OPEN10/.FALSE./ , OPEN11/.FALSE./ ,
     &      OPEN12/.FALSE./ , OPEN13/.FALSE./ , OPEN14/.FALSE./ ,
     &      OPEN15/.FALSE./ , OPEN16/.FALSE./ , OPEN17/.FALSE./ ,
     &      OPEN18/.FALSE./ , OPEN19/.FALSE./ , OPEN20/.FALSE./ ,
     &      OPEN21/.FALSE./ , OPEN22/.FALSE./ ,
     &      LGHOST/.FALSE./
       DATA MAXD/0/,MAXT/0/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Set machine dependant adas configuration values
C-----------------------------------------------------------------------

       call xx0000
       call xxname(user)
       call xxdate( date )
       call xxguid(uid)

       ntrsel = 0

C=======================================================================
C Input panel
C=======================================================================

 100   continue

       if (open10) close(10)
       open10=.false.

C-----------------------------------------------------------------------
C Get proton, copase input data set names (from ispf)
C-----------------------------------------------------------------------

       call b8spf0( rep    ,
     &              dsninp , dsnexp , dsninc
     &            )

C-----------------------------------------------------------------------
C Is program run is complete?
C-----------------------------------------------------------------------

       if (rep.eq.'YES') goto 9999


C-----------------------------------------------------------------------
C Open adf04 input data file and read in data
C-----------------------------------------------------------------------

       open( unit=iunt10 , file=dsninc , status='old' )
       open10=.true.

       itieactn=0
       call xxdata_04( iunt10 ,
     &                 ndlev  , ndtrn  , ndmet   , ndqdn , nvmax ,
     &                 titled , iz     , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa  , lbseta  , prtwta, cprta ,
     &                 il     , qdorb  , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga , isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla   , ipla    , zpla  ,
     &                 nv     , scef   ,
     &                 itran  , maxlev ,
     &                 tcode  , i1a    , i2a     , aval  , scom  ,
     &                 beth   ,
     &                 iadftyp, lprn   , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp  , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp  , itieactn, ltied
     &               )

C-----------------------------------------------------------------------
C Reduced level nomenclature strings to last 'icstmx' non-blank bytes
C-----------------------------------------------------------------------

       call bxcstr( cstrga , il     , icstmx ,
     &              cstrgb
     &            )

C-----------------------------------------------------------------------
C Sort transitions into transition/recombination types.
C-----------------------------------------------------------------------

       call h9ttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              iadftyp,
     &              icnte  , icntp  , icntr  , icnth , icnti ,
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a
     &            )

C-----------------------------------------------------------------------
C Calculate level energies relative to level 1 & ionisation pot. in ryds
C-----------------------------------------------------------------------

       call xxeryd( bwno , il , wa  ,
     &                     er , xia
     &            )

C-----------------------------------------------------------------------
C Set up array 'tscef(,)' - contains input temperatures in three forms.
C-----------------------------------------------------------------------

       do iform=1,3
          call xxtcon( l1 , iform , iz1 , nv , scef , tscef(1,iform) )
       end do

C-----------------------------------------------------------------------
C Set up processing screen display parameters
C-----------------------------------------------------------------------

       call b8setp( iz0    , iz     ,
     &              ndlev  , ndwvl  , ndsel , il    , icnte ,
     &              cstrgb , isa    , ila   , xja   ,
     &              strga  , npl    , cprta , ndmet ,
     &              lss04a ,
     &              strgmf , strgmi
     &            )

C=======================================================================
C Processing panel
C=======================================================================

 200   continue

       call b8ispf( ipan  , lpend  , titled,
     &              ndtem , ndden  , ndmet , ndsel ,
     &              il    , nv     , tscef ,
     &              title , nmet   , imetr ,
     &              ifout ,
     &              maxt  , tine  , tinp   , tinh  ,
     &              idout ,
     &              maxd  , dine  , dinp   , ratha , ratpia, ratmia,
     &              zeff  , lnorm ,
     &              lpsel , lzsel , liosel , lhsel , lrsel , lisel ,
     &              lnsel , lgph  , nwvl   , wvls  , wvll  , avlt  ,
     &              ntrsel, itrlow, itrup  ,
     &              itsel , gtit1 , lgrd1  , ldef1 ,
     &              xl1in , xu1in , yl1    , yu1   , ibsela,
     &              lsseta, sgrda , isseta , dsfull
     &            )
       ipan=-1

C----------------------------------------------------------------------
C Read signal from idl for immediate termination (1) or continue (0)
C-----------------------------------------------------------------------

       read(pipein,*) istop
       if(istop.eq.1) goto 9999
       if (lpend) goto 100

C-----------------------------------------------------------------------
C Since b8getp outputs to paper.text, we must put the output screen here.
C-----------------------------------------------------------------------

       Call b8spf1( ndtem  , tine   , maxt   , ifout ,
     &              lpend  , lgcr   , uid    ,
     &              lnewpa , lpaper , lcont  , lpass ,
     &              dsnpap , dsnout , dsnpas , dsngcr,
     &              lgph   , itsel  , gtit1  , cadas
     &            )

C-----------------------------------------------------------------------
C Read signal from IDL for immediate termination(1) or continue(0)
C-----------------------------------------------------------------------

       read(pipein,*) istop
       if(istop.eq.1) goto 9999
       if(lpend) goto 200

C----------------------------------------------------------------------
C Open text output data set - dsnpap - if requested.
C----------------------------------------------------------------------

       if ( (lpaper .and. lnewpa)      .or.
     &      (lpaper .and. .not.open27) ) then
          if (open27) close(unit=iunt27)
          dsn80 = ' '
          dsn80 = dsnpap
          open(unit=iunt27, file=dsn80, status='unknown')
          open27=.TRUE.
       endif

C------------------------------------------------------------------------
C Output ion specifications & level information to 'iunt27' (paper.txt)
C-----------------------------------------------------------------------

       if (lpaper) then
          call b8out0(  ndmet  , iunt27 , date   ,
     &         prgtyp , dsninc , dsnexp ,
     &         titled , iz     , iz0    , iz1    , bwno   ,
     &         npl    , nplr   , npli   , bwnoa  ,
     &         icnte  , icntp  , icntr  , icnth  , icnti  ,
     &         icntl  , icnts  ,
     &         il     ,
     &         ia     , cstrga , isa    , ila    , xja    , wa ,
     &         er     , cpla   ,
     &         nv     , tscef  , cadas
     &         )
       endif

       oput = .TRUE.

C-----------------------------------------------------------------------
C Analyse entered data - if not return to dataset selection
C-----------------------------------------------------------------------

C Check if electron impact transitions exist to the selected metastable

       call bxchkm( nmet   , imetr , icnte , ie1a , lmetr )

C Convert input temperatures into ev:  (tin? -> t?va)  (? = e,p,h )

       call xxtcon( ifout, l2, iz1, maxt , tine  , teva   )
       call xxtcon( ifout, l2, iz1, maxt , tinp  , tpva   )
       call xxtcon( ifout, l2, iz1, maxt , tinh  , thva   )

C Convert input temperatures into kelvin:  (tin? -> t?a)  (? = e,p,h )

       call xxtcon( ifout, l1, iz1, maxt , tine  , tea    )
       call xxtcon( ifout, l1, iz1, maxt , tinp  , tpa    )
       call xxtcon( ifout, l1, iz1, maxt , tinh  , tha    )

C Convert input densities, including relevant graph limits, into cm-3:

       call xxdcon( idout, l1, iz1, maxd , dine  , densa  )
       call xxdcon( idout, l1, iz1, maxd , dinp  , denspa )
       xl1 = r8dcon( idout , l1 , iz1 , xl1in )
       xu1 = r8dcon( idout , l1 , iz1 , xu1in )

C Fetch in the ionisation data from the ionelec files

       do imet = 1 , nmet
          is=imetr(imet)
          do ip=1,npl
             write(tmet,1000) iz0 , iz , iz1 , cstrgb(is)(1:icstmx),
     &                        isa(is) , ila(is) , xja(is) , ip
             tmeta(imet,ip)=tmet
          end do
       end do

C-----------------------------------------------------------------------
C  Initialise vecr, vech, veci to zero
C-----------------------------------------------------------------------

       do is=1,ndlev
          do it=1,ndtem
            dcie(it,is) = 0.0D0
            do ip=1,ndmet
                vecr(it,is,ip)  = 0.0D0
                dvecr(it,is,ip) = 0.0D0
                vech(it,is,ip)  = 0.0D0
                veci(it,is,ip)  = 0.0D0
                dciepr(it,is,ip)= 0.0D0
                dv3pr(it,is,ip) = 0.0D0
            end do
          end do
       end do

C-----------------------------------------------------------------------
C Calculate electron impact transitionn excitation and de-excitation
C rate coefs. Note, at this stage assume gamma = 1..
C-----------------------------------------------------------------------

       call xxrate( ndtrn , ndtem  , ndlev ,
     &              icnte , maxt   ,
     &              xja   , er     , tea   ,
     &              ie1a  , ie2a   ,
     &              excre , dexcre
     &            )

C-----------------------------------------------------------------------
C Calculate line power loses for all lines and specific line.
C-----------------------------------------------------------------------
       call b8loss( ndtrn , ndlev  , ndmet ,
     &              icnte , nmet   , imetr , isptrn ,
     &              xja   , er     , aa    ,
     &              ie1a  , ie2a   ,
     &              plas1 , swvln  , pla1
     &             )

C-----------------------------------------------------------------------
C Calculate proton impact transition excitation and de-excitation rate
C coefficients (only if required). Note: at this stage assume gamma = 1.
C-----------------------------------------------------------------------

       if (lpsel) then
          call xxrate( ndtrn , ndtem  , ndlev ,
     &                 icntp , maxt   ,
     &                 xja   , er     , tpa   ,
     &                 ip1a  , ip2a   ,
     &                 excrp , dexcrp
     &               )
       endif

C-----------------------------------------------------------------------
C Setup index of ordinary excited levels in complete level list 'iordr'
C-----------------------------------------------------------------------

       call bxiord( il   ,
     &              nmet , imetr ,
     &              nord , iordr
     &            )

C-----------------------------------------------------------------------
C Electron excitation data option - spline gamma's to give rate coeffts.
C-----------------------------------------------------------------------

       call birate( ndtem      , ndtrn  , d1     ,
     &              nv         , scef   , scom   ,
     &              maxt       , tea    ,
     &              icnte      , ietrn  ,
     &              iadftyp    , tcode  ,
     &              excre      , dexcre ,
     &              ltrng(1,1)
     &            )

C-----------------------------------------------------------------------
C Proton excitation data option - spline gamma's to give rate coeffts.
C-----------------------------------------------------------------------

       if (lpsel) then
          zeffsq = zeff*zeff
          call birate( ndtem      , ndtrn  , d1     ,
     &                 nv         , scef   , scom   ,
     &                 maxt       , tpa    ,
     &                 icntp      , iptrn  ,
     &                 iadftyp    , tcode  ,
     &                 excrp      , dexcrp ,
     &                 ltrng(1,2)
     &               )
       endif

C----------------------------------------------------------------------
C Free electron recombination data option - spline coefficients
C----------------------------------------------------------------------

       if (lrsel) then
          call b8rcom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                 nv    , scef       , scom   ,
     &                 maxt  , tea        ,
     &                 icntr , irtrn      , i2a    , i1a    ,
     &                 dvecr , ltrng(1,1)
     &               )
       endif

C----------------------------------------------------------------------
C Electron impact ionisation data option - spline coefficients
C----------------------------------------------------------------------

       if (lisel) then
          call b8rcom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                 nv    , scef       , scom   ,
     &                 maxt  , tea        ,
     &                 icnti , iitrn      , i2a    , i1a    ,
     &                 veci  , ltrng(1,1)
     &               )
       endif

C----------------------------------------------------------------------
C Charge exchange recombination data option - spline coefficients
C----------------------------------------------------------------------

       if (lhsel) then
          call b8rcom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                 nv    , scef       , scom   ,
     &                 maxt  , tha        ,
     &                 icnth , ihtrn      , i2a    , i1a    ,
     &                 vech  , ltrng(1,3)
     &               )
       endif

C----------------------------------------------------------------------
C Ionisation z --> z+1 data option - spline coefficients
C----------------------------------------------------------------------

       if (liosel) then
          call biscom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                 il    , wa         , npl    , bwnoa  ,
     &                 nmet  , imetr      , nord   , iordr  ,
     &                 nv    , scef       , scom   ,
     &                 maxt  , tea        ,
     &                 icnts , istrn      , is1a   , is2a   ,
     &                 lsseta, sgrda      , esgrda , iadftyp,
     &                 smeta , esmeta     , sorda  , esorda ,
     &                 rmeta , rorda      ,
     &                 ltrng(1,1)
     &               )

       endif

C----------------------------------------------------------------------
C Set up a-value transition matrix 'cra' (units: sec-1)
C----------------------------------------------------------------------

       call bxmcra( ndtrn , ndlev  ,
     &              icnte , il     ,
     &              ie1a  , ie2a   ,
     &              aa    ,
     &              cra
     &            )

C----------------------------------------------------------------------
C Initialise projected indirect couplings
C----------------------------------------------------------------------

       lpdata = .FALSE.
       if( lnsel ) then
           itin = 1
           inin = 1

            call b8getp( iz0    , iz1    , dsnexp , dsninc ,
     &                   ndlev  , ndmet  , ndtem  , ndden  ,
     &                   maxd   , maxt   , densa  , tea    ,
     &                   lpdata , liosel , lrsel  , lhsel  ,
     &                   il     , itin   , inin   ,
     &                   pcc    , pcie   , pciepr , pv3pr  ,
     &                   pvcrpr , pvecr  , iunt27 , open27 ,
     &                   pr
     &                 )

       endif

C-----------------------------------------------------------------------
C  Set up vector of ionisation and three-body recombination coefficients
C  if required - note parent resolved data
C
C  Note that the indexing is quite confusing; most quantities are
C  indexed by parent and level (as well as temperature) but these
C  can vary
C
C      dciepr(IT, IL, IP)  :  IL level list
C      lss04a(IL, IP)      :  IL level list
C      zpla(IP, IL)        :  IL level list
C      sorda(IT, IL, IP)   :  IL ordinary level list
C
C  The level list is 1-number of levels, ordinary is 1 - number of levels
C  less number of metastables.
C
C-----------------------------------------------------------------------

       IF (LIOSEL) THEN

           DO 122 IT = 1,MAXT

              DO 5 IS=1,MAXLEV
                 DO 15 IP=1,NPLA(IS)

C check if (S-lines) from ordinary levels are present in the adf04 file,
C if not use ECIP approximation.

                    ilev = is - nmet
                    ipp  = ipla(ip,is)
                    if (ilev.gt.0.and.lss04a(is,ipp)) then


                        spref = r8expe(sorda(it,ilev,ipp),
     &                                 esorda(it,ilev,ipp),'zero')
                        if (iadftyp.eq.4) spref3 = rorda(it,ilev,ipp)

                        if ( zpla(ip,is) .gt. 0.0d0) then
                           x=xia(is)+wn2ryd*(bwnoa(ipla(ip,is))-bwno)
                           dciepr(it,is,ipla(ip,is))=spref
                           if (iadftyp.eq.4) then
                             alfred = 3.30048d-24 *  spref3 *
     &                                (1.5789d5/tea(it))**1.5
                           else
                             alfred = 3.30048d-24 *  spref *
     &                                (1.5789d5/tea(it))**1.5 *
     &                                exp(1.5789d5*x/tea(it))
                           endif
                           dv3pr(it,is,ipla(ip,is))=
     &                                (2.0d0*xja(is)+1.0d0)*alfred/
     &                                       prtwta(ipla(ip,is))
                        else
                           dciepr(it,is,ipla(ip,is)) = 0.0d+0
                           dv3pr(it,is,ipla(ip,is))  = 0.0d+0
                        endif

                    else

                       if ( zpla(ip,is) .gt. 0.0d0) then
                          x=xia(is)+wn2ryd*(bwnoa(ipla(ip,is))-bwno)
                          dciepr(it,is,ipla(ip,is))=
     &                                r8necip(iz,x,zpla(ip,is)
     &                                       ,tea(it),alfred)
                          dv3pr(it,is,ipla(ip,is))=
     &                               (2.0d0*xja(is)+1.0d0)*alfred/
     &                                      prtwta(ipla(ip,is))
                       else
                          dciepr(it,is,ipla(ip,is)) = 0.0d+0
                          dv3pr(it,is,ipla(ip,is))  = 0.0d+0
                       endif

                    endif

                    dcie(it,is) = dcie(it,is) +
     &                            dciepr(it,is,ipla(ip,is))


   15            continue
    5         continue

C-----------------------------------------------------------------------
C  Substitute special szd rates from b8gets call if available and adjust
C  the three-body rates accordingly
C-----------------------------------------------------------------------

              do 14 imet=1,nmet
                do 13 ip=1,npl
                  if (lsseta(imet,ip))then
                     spref=r8expe(smeta(it,imet,ip),
     &                            esmeta(it,imet,ip),'zero')
                     if (spref.gt.0.0d0.and.
     &                   dciepr(it,imetr(imet),ip).gt.0.0d0) then
                         dv3pr(it,imetr(imet),ip)=spref
     &                             * dv3pr(it,imetr(imet),ip)/
     &                               dciepr(it,imetr(imet),ip)
                     endif
                     dcie(it,imetr(imet))=dcie(it,imetr(imet)) -
     &                                dciepr(it,imetr(imet),ip)+
     &                                spref
                     dciepr(it,imetr(imet),ip)=spref


                  endif
   13           continue
   14       continue

  122    continue

       endif


C-----------------------------------------------------------------------
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C Increment over over all input temperature values
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C-----------------------------------------------------------------------

       do 4 it=1,maxt

C-----------------------------------------------------------------------
C Set up excitation/de-excitation rate coefft. matrices for given temp.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C Electron impact transitions 'crce' - (units: cm**3/sec-1)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

           call bxmcrc( ndtem , ndtrn  , ndlev  ,
     &                  it    , icnte  , il     ,
     &                  ie1a  , ie2a   ,
     &                  excre , dexcre ,
     &                  crce
     &                )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C Proton impact transitions 'crcp' - (units: cm**3/sec-1) - if selected
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
           if (lpsel) then
              call bxmcrc( ndtem , ndtrn  , ndlev  ,
     &                     it    , icntp  , il     ,
     &                     ip1a  , ip2a   ,
     &                     excrp , dexcrp ,
     &                     crcp
     &                    )
           endif

C-----------------------------------------------------------------------
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C Increment over over all input density values
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C-----------------------------------------------------------------------

           do 6 in=1,maxd

C-----------------------------------------------------------------------
C  initialise vectors to zero
C-----------------------------------------------------------------------

               do is=1,ndlev
                  cie(is)  = 0.0d0
                  pcie(is) = 0.0d+0
                  do i = 1,ndlev
                     pcc(is,i) = 0.0d0
                  end do
                  do ip=1,ndmet
                     ciepr(is,ip)  = 0.0d0
                     pciepr(is,ip) = 0.0d0
                     v3pr(is,ip)   = 0.0d0
                     pv3pr(is,ip)  = 0.0d0
                     pvecr(is,ip)  = 0.0d0
                  end do
               end do

C-----------------------------------------------------------------------
C Set up indirect matrices
C-----------------------------------------------------------------------

               if( lpdata ) then
                  itin = it
                  inin = in

                  call b8getp( iz0    , iz1    , dsnexp , dsninc ,
     &                         ndlev  , ndmet  , ndtem  , ndden  ,
     &                         maxd   , maxt   , densa  , tea    ,
     &                         lpdata , liosel , lrsel  , lhsel  ,
     &                         il     , itin   , inin   ,
     &                         pcc    , pcie   , pciepr , pv3pr  ,
     &                         pvcrpr , pvecr  , iunt27 , open27 ,
     &                         pr
     &                       )

              endif

C-----------------------------------------------------------------------
C  Add indirect couplings onto ionisation and recombination vectors
C
C  If indirect coupling have been initiated, set liosel = true
C                                                lrsel  = true
C
C  Note cie has units of cm3s-1 while pcie and pciepr have units of s-1
C       v3pr has units of cm6s-1 while pv3pr has units of cm3s-1
C       vecr and pvecr have units of cm3s-1
C
C  Note that cie does not need to be adjusted
C-----------------------------------------------------------------------

              if( lpdata ) then

                 liosel = .true.
                 lrsel = .true.
                 do 17 is=1,maxlev

                   cie(is) = dcie(it,is)
                   do 18 ip = 1,npl
                     ciepr(is,ip) = dciepr(it,is,ip)
     &                               + pciepr(is,ip) / densa(in)
                     v3pr(is,ip) = dv3pr(it,is,ip)
     &                               + pv3pr(is,ip) / densa(in)
                     vecr(it,is,ip) = dvecr(it,is,ip)
     &                               + pvecr(is,ip)

   18              continue
   17            continue

              else

                  do 23 is=1,maxlev

                   cie(is) = dcie(it,is)
                   do 24 ip = 1,npl
                     ciepr(is,ip) = dciepr(it,is,ip)
                     v3pr(is,ip)  =  dv3pr(it,is,ip)
                     vecr(it,is,ip) = dvecr(it,is,ip)
   24              continue
   23             continue

              endif

C-----------------------------------------------------------------------
C Set up whole rate matrix  - (units: sec-1)
C To add on indirect parts also pass over pcc
C-----------------------------------------------------------------------

              call b8mcca( ndlev     , il         ,
     &                     lpsel     , liosel     , lpdata,
     &                     densa(in) , denspa(in) ,
     &                     cra       , pcc        ,
     &                     crce      , crcp       , cie    ,
     &                     cc
     &                   )

C-----------------------------------------------------------------------
C Set up non-metastable (ordinary excited level) matrix - (units: sec-1)
C-----------------------------------------------------------------------

              call bxmcma( ndlev ,
     &                     nord  , iordr ,
     &                     cc    ,
     &                     cmat
     &                   )

C-----------------------------------------------------------------------
C Invert non-metastable (ordinary excited level) matrix
C-----------------------------------------------------------------------

              lsolve=.false.
              call xxminv( lsolve , ndlev , nord   ,
     &                      cmat   , rhs   , dmint
     &                    )

C-----------------------------------------------------------------------
C Stack up non-metastable population components associated with each
C Metastable level.
C-----------------------------------------------------------------------

              call bxstka( ndlev            , ndmet  ,
     &                     nord             , nmet   ,
     &                     iordr            , imetr  ,
     &                     cmat             , cc     ,
     &                     stack(1,1,it,in)
     &                   )

C-----------------------------------------------------------------------
C Stack up non-metastable state sel. recombs. from (z+1)    -if selected
C-----------------------------------------------------------------------

              if (liosel) then
                  npl3 = max0(npl,nplr)

                  do 36 ip=1,npl3

                      call b8stke( ndtem         , ndlev  , ndmet ,
     &                             it            , nord   ,
     &                                             iordr  ,
     &                             densa(in)     ,
     &                             cmat          , vecr   , v3pr  ,
     &                             ip            ,
     &                             stvr(1,it,in,ip)
     &                           )
   36             continue

              elseif (lrsel.and.(.not.liosel)) then

                  do 37 ip=1,nplr

                      call b8stkb( ndtem         , ndlev  , ndmet ,
     &                             it            , nord   ,
     &                                             iordr  ,
     &                             cmat          , vecr   ,
     &                             ip            ,
     &                             stvr(1,it,in,ip)
     &                           )
   37            continue

              endif

C-----------------------------------------------------------------------
C Stack up non-metastable state sel. c. exch. from (z+1)    -if selected
C-----------------------------------------------------------------------

              do 30 ip=1,nplr

                   if (lhsel) then
                      call b8stkb( ndtem         , ndlev  , ndmet ,
     &                             it            , nord   ,
     &                                             iordr  ,
     &                             cmat          , vech   ,
     &                             ip            ,
     &                             stvh(1,it,in,ip)
     &                           )
                   endif

   30         continue

C-----------------------------------------------------------------------
C Stack up non-metastable state sel. ionis.   from (z-1)    -if selected
C-----------------------------------------------------------------------

              do 31 ip=1,npli

                   if (lisel) then
                      call b8stkb( ndtem         , ndlev  , ndmet ,
     &                             it            , nord   ,
     &                                             iordr  ,
     &                             cmat          , veci   ,
     &                             ip            ,
     &                             stvi(1,it,in,ip)
     &                           )
                   endif

   31         continue

C-----------------------------------------------------------------------
C Stack up metastable level transition rate contributions
C-----------------------------------------------------------------------

              call bxstkc( ndlev            , ndmet            ,
     &                     nord             , nmet             ,
     &                     iordr            , imetr            ,
     &                     cc               , stack(1,1,it,in) ,
     &                     cred
     &                   )

              do 44 imet=1,nmet
                do 44 jmet=1,nmet
                  fvcred(imet,jmet,it,in)=cred(imet,jmet)
   44         continue

C-----------------------------------------------------------------------
C Stack up metastable state sel. recomb.  from (z+1)    -if selected
C-----------------------------------------------------------------------

              if (liosel) then
                  npl3 = max0(npl,nplr)

                  do 38 ip=1,npl3

                      call b8stkf( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvr(1,it,in,ip),
     &                             densa(in),
     &                             vecr  , v3pr          , ip     ,
     &                             vrred
     &                           )
                      do 45 imet=1,nmet
                        fvrred(imet,ip,it,in)=vrred(imet,ip)
   45                 continue

   38             continue

              elseif(lrsel.and.(.not.liosel))then

                  do 39 ip=1,nplr

                      call b8stkd( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvr(1,it,in,ip),
     &                             vecr  , ip    ,
     &                             vrred
     &                           )
                      do 46 imet=1,nmet
                        fvrred(imet,ip,it,in)=vrred(imet,ip)
   46                 continue

   39             continue
              endif

C-----------------------------------------------------------------------
C Stack up metastable state sel. c. exch. from (z+1)    -if selected
C-----------------------------------------------------------------------

              do 32 ip=1,nplr
                   if (lhsel) then
                      call b8stkd( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvh(1,it,in,ip) ,
     &                             vech  , ip   ,
     &                             vhred
     &                           )
                      do 47 imet=1,nmet
                        fvhred(imet,ip,it,in)=vhred(imet,ip)
   47                 continue

                   endif
   32         continue

C-----------------------------------------------------------------------
C Stack up metastable state sel. ionis.   from (z-1)    -if selected
C-----------------------------------------------------------------------

              do 33 ip = 1,npli

                   if (lisel) then
                      call b8stkd( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvi(1,it,in,ip) ,
     &                             veci  , ip   ,
     &                             vired
     &                           )
                      do 48 imet=1,nmet
                        fvired(imet,ip,it,in)=vired(imet,ip)
   48                 continue

                   endif
   33         continue

C-----------------------------------------------------------------------
C Stack up metastable state sel. ionis. from (z) - if selected by liosel
C Stack up parent cross coupling coefficients
C-----------------------------------------------------------------------

              if (liosel) then

                 do 43 imet = 1,nmet

                   do 42 ip=1,npl3

                     vionr(imet,ip)=ciepr(imetr(imet),ip)
                     do 41 i=1,nord
                      vionr(imet,ip)=vionr(imet,ip)+ciepr(iordr(i),ip)
     &                               *stack(i,imet,it,in)
   41                continue
                     fvionr(imet,ip,it,in)=vionr(imet,ip)

   42              continue
   43            continue

                 do 143 ip = 1,npl3

                   do 142 imet=1,npl3

                     vcrpr(ip,imet)=0.0d+0
                     if( ip .ne. imet ) then
                     do 141 i=1,nord
                      vcrpr(ip,imet)=vcrpr(ip,imet)+ciepr(iordr(i),ip)
     &                               *stvr(i,it,in,imet)
  141                continue
                     endif
                     fvcrpr(ip,imet,it,in)=vcrpr(ip,imet)
                     if ( lpdata ) then
                        fvcrpr(ip,imet,it,in) = fvcrpr(ip,imet,it,in)
     &                                        + pvcrpr(ip,imet)
                     endif

  142              continue
  143            continue

              endif


C-----------------------------------------------------------------------
C Calculate metastable populations
C-----------------------------------------------------------------------

              call bxmpop( ndmet             ,
     &                     nmet              ,
     &                     cred              ,
     &                     rhs               , crmat        ,
     &                     stckm(1,it,in)
     &                   )

C-----------------------------------------------------------------------
C  calculate recombinations and charge exchange contributions.
C-----------------------------------------------------------------------

              if (liosel) then

                  do 34 ip=1,npl3

                   call b8stvm( ndmet            ,
     &                          nmet             ,
     &                          crmat            ,
     &                          ip               ,
     &                          vrred            ,
     &                          stvrm(1,it,in,ip)
     &                        )
   34             continue


              elseif ((.not.liosel).and.lrsel) then

                  do 51 ip=1,nplr

                   call b8stvm( ndmet            ,
     &                          nmet             ,
     &                          crmat            ,
     &                          ip               ,
     &                          vrred            ,
     &                          stvrm(1,it,in,ip)
     &                        )
   51             continue

              endif
C-----------------------------------------------------------------------
              do 52 ip=1,nplr

                if (lhsel) then
                   call b8stvm( ndmet            ,
     &                          nmet             ,
     &                          crmat            ,
     &                          ip               ,
     &                          vhred            ,
     &                          stvhm(1,it,in,ip)
     &                        )
                endif
   52         continue
C-----------------------------------------------------------------------
              do 35 ip=1,npli

                if (lisel) then
                   call b8stvm( ndmet            ,
     &                          nmet             ,
     &                          crmat            ,
     &                          ip               ,
     &                          vired            ,
     &                          stvim(1,it,in,ip)
     &                        )
                endif
   35         continue
C-----------------------------------------------------------------------

C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
    6      continue
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *

C-----------------------------------------------------------------------
C Calculate the specific  line power, charge exchange recombination
C And total line powers - equil. & met. resolved.
C-----------------------------------------------------------------------

            do imet = 1,ndmet
              plba(imet,it) = 0.0d+0
            enddo

            do 7 in=1,maxd

               call b8toth( ndlev   , ndmet  , ndtem , ndden ,
     &                      nord    , nmet   , npl   ,
     &                      iordr   , imetr  ,
     &                      it      , maxt   , in    , maxd  ,
     &                      ratpia  ,
     &                      stvhm   , stvh   ,
     &                      pla1    ,
     &                      pha     , ph
     &                    )
               call b8totl( ndlev          , ndmet   ,
     &                      nord           , nmet    ,
     &                      iordr          , imetr   , isptrn ,
     &                      densa(in)      ,
     &                      stckm(1,it,in) , stack(1,1,it,in) ,
     &                      pla1           , plba(1,it)       ,
     &                      pla(it,in)     , pl(1,it,in)      ,
     &                      psa(1,it,in)   , ps(1,1,it,in)
     &                    )
    7       continue


C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    4    continue
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


C-----------------------------------------------------------------------
C Output the generalised collisional-radiative coefficients
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C Calculate the populations of each level.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C Work out the metastable levels first.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

         call b8popm( ndtem  , ndden , ndmet , ndlev ,
     &                npl    , nplr  , npli  ,
     &                maxt   , maxd  , nmet  ,
     &                         densa , imetr ,
     &                         lrsel , lisel , lhsel ,
     &                         ratpia, ratmia, ratha ,
     &                stckm  , stvrm , stvim , stvhm ,
     &                popar
     &              )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C Work out the populations of each ordinary level.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

         call b8popo( ndtem  , ndden , ndmet , ndlev ,
     &                npl    , nplr  , npli  ,
     &                maxt   , maxd  , nmet  , nord  ,
     &                         densa , imetr , iordr ,
     &                         lrsel , lisel , lhsel ,
     &                         ratpia, ratmia, ratha ,
     &                stack  , stvr  , stvi  , stvh  ,
     &                popar
     &              )


C=======================================================================
C Output panel
C=======================================================================

 300    CONTINUE



C***********************************************************************
C End of current file analysis - establish if passing file output req'd
C***********************************************************************
C
C***********************************************************************
C The output screen is put here as well as before the analysis so that
C the analysis does not have to be repeated each time the user returns
C from graphing. OPUT says whether we have just come from analysis
C (.true.) or from the graphing screen (.false.)
C***********************************************************************

        if (.not. oput) then

             call b8spf1( ndtem  , tine   , maxt   , ifout ,
     &                    lpend  , lgcr   , uid    ,
     &                    lnewpa , lpaper , lcont  , lpass ,
     &                    dsnpap , dsnout , dsnpas , dsngcr,
     &                    lgph   , itsel  , gtit1  , cadas
     &                  )

C-----------------------------------------------------------------------
C Read signal from IDL for immediate termination(1) or continue(0)
C-----------------------------------------------------------------------

             read(pipein,*) istop
             if(istop.eq.1) goto 9999
             if(lpend) goto 200

             if ( (lpaper .and. lnewpa) .or.
     &            (lpaper . and. .not.open27) ) then
                if (open27) close(unit=iunt27)
                dsn80 = ' '
                dsn80 = dsnpap
                open(unit=iunt27, file=dsn80, status='unknown')
                open27=.true.
             endif

C------------------------------------------------------------------------
C Output ion specifications & level information to 'iunt27' (paper.text)
C-----------------------------------------------------------------------

             if (lpaper) then
                call b8out0(  ndmet  , iunt27 , date   ,
     &               prgtyp , dsninc , dsnexp ,
     &               titled , iz     , iz0    , iz1    , bwno   ,
     &               npl    , nplr   , npli   , bwnoa  ,
     &               icnte  , icntp  , icntr  , icnth  , icnti  ,
     &               icntl  , icnts  ,
     &               il     ,
     &               ia     , cstrga , isa    , ila    , xja    , wa ,
     &               er     , cpla   ,
     &               nv     , tscef  , cadas
     &               )
             endif

        endif

        oput = .FALSE.


C-----------------------------------------------------------------------
C Open passing pec file output data set - dsnout - if requested
C-----------------------------------------------------------------------

         if ( lcont .and. .not.open11) then
            dsn80 = ' '
            dsn80 = dsnout
            open(unit=iunt11,file=dsn80,status='unknown')
            open11=.true.
         endif

C-----------------------------------------------------------------------
C Open passing sxb file output data set - dsnpas - if requested
C-----------------------------------------------------------------------

         if ( lpass .and. .not.open12 ) then
            dsn80=' '
            dsn80=dsnpas
            open(unit=iunt12,file=dsn80,status='unknown')
            open12=.true.
         endif


C-----------------------------------------------------------------------
C Open passing gcr information output data set - dsngcr - if requested
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Dsngcr has the string '/.pass' at the end which should be replaced by
C Each passing file name. ix stores the relevant length
C-----------------------------------------------------------------------

          ix=index(dsngcr,'/.pass')-1

          if ( lgcr .and. .not.open13 ) then
             dsn80=' '
             dsn80=dsngcr(1:ix)//'/gcr.pass'
             open(unit=iunt13,file=dsn80,status='unknown')
             open13=.true.
          endif

C-----------------------------------------------------------------------
C Open passing iso-electronic master files for output - if requested
C-----------------------------------------------------------------------

          if ( lgcr ) then
            if(.not.open14)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/acd208.pass'
              open(unit=iunt14,file=dsn80,status='unknown')
              open14=.true.
            endif
            if(.not.open15)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/scd208.pass'
              open(unit=iunt15,file=dsn80,status='unknown')
              open15=.true.
            endif
            if(.not.open16)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/ccd208.pass'
              open(unit=iunt16,file=dsn80,status='unknown')
              open16=.true.
            endif
            if(.not.open17)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/qcd208.pass'
              open(unit=iunt17,file=dsn80,status='unknown')
              open17=.true.
            endif
            if(.not.open18)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/xcd208.pass'
              open(unit=iunt18,file=dsn80,status='unknown')
              open18=.true.
            endif
            if(.not.open19)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/prb208.pass'
              open(unit=iunt19,file=dsn80,status='unknown')
              open19=.true.
            endif
            if(.not.open20)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/prc208.pass'
              open(unit=iunt20,file=dsn80,status='unknown')
              open20=.true.
            endif
            if(.not.open21)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/plt208.pass'
              open(unit=iunt21,file=dsn80,status='unknown')
              open21=.true.
            endif
            if(.not.open22)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/pls208.pass'
              open(unit=iunt22,file=dsn80,status='unknown')
              open22=.true.
            endif
            if(.not.open23)then
              dsn80=' '
              dsn80=dsngcr(1:ix)//'/met208.pass'
              open(unit=iunt23,file=dsn80,status='unknown')
              open23=.true.
            endif
          endif

C-----------------------------------------------------------------------
C Output data to passing pec file - dsnout - if requested
C-----------------------------------------------------------------------

          call b8wrps( lcont  , iunt11 , lpass  , iunt12 ,
     &                 dsninc , dsfull , dsnexp , ibsela ,
     &                 titled , date   , user   ,
     &                 ndlev  , ndtem  , ndden  , ndmet  , ndtrn ,
     &                 ndsel  ,
     &                 lnorm  ,
     &                 iz     , iz0    , iz1    ,
     &                 il     , nmet   , nord   ,
     &                 maxt   , maxd   , icntr  , icnti  , icnth ,
     &                 isa    , ila    , xja    ,
     &                 cstrga , wa     ,
     &                 icnte  ,
     &                 ie1a   , ie2a   , aa     ,
     &                 imetr  , iordr  , teva   , densa  ,
     &                 npl    , nplr   , npli   , npl3   ,
     &                 lrsel  , lisel  , lhsel  , liosel ,
     &                 lpsel  , lzsel  , lnsel  ,
     &                 nwvl   , wvls   , wvll   , avlt   ,
     &                 ntrsel , itrlow , itrup  ,
     &                 stvr   , stvi   , stvh   ,
     &                 ratpia , ratmia , stack  ,
     &                 fvionr , sgrda  ,
     &                 lsseta , lss04a
     &               )


C-----------------------------------------------------------------------
C Output data to passing gcr file - dsngcr - if requested
C-----------------------------------------------------------------------

          if (lgcr ) then
             call b8wrmc( iunt13  , iunt14 , iunt15 ,iunt16 , iunt17 ,
     &            iunt18 , iunt19 , iunt20 , iunt21 ,iunt22 , iunt23 ,
     &            dsninc , dsfull , dsnexp ,
     &            titled , date   , user   ,
     &            ndlev  , ndtem  , ndden  , ndmet ,
     &            lnorm  , iz     , iz0    , iz1    ,
     &            ibsela , bwnoa  , prtwta ,
     &            il     , nmet   , nord   , imetr  ,
     &            ia     , isa    , ila    , xja   ,
     &            cstrgb , wa     , maxt   , maxd  , teva   , densa  ,
     &            npl    , nplr   , npl3   , npli  , cprta  ,
     &            lrsel  , lisel  , lhsel  , liosel ,
     &            lpsel  , lzsel  , lnsel  ,
     &            fvcred , fvrred , fvired , fvhred , fvionr ,
     &            fvcrpr , pl     , ph     , ps     , swvln  ,
     &            pr     ,
     &            ratpia , ratmia , stack  , stckm  ,
     &            lsseta , lss04a
     &                          )
          endif

C------------------------------------------------------------------------
C output data to paper.text.
C-----------------------------------------------------------------------

          if (lpaper) then
                   call b8out1( iunt27 ,
     &                 ndlev  , ndtem  , ndden , ndmet ,
     &                 lnorm  ,
     &                 il     , nmet   , nord  ,
     &                 npl    , nplr   , npli  , npl3   ,
     &                 maxt   , maxd   , zeff  ,
     &                 icntp  , icntr  , icnti , icnth  ,
     &                 lpsel  , lzsel  , liosel, lhsel , lrsel ,
     &                 lisel  ,
     &                 lmetr  , imetr  , iordr ,
     &                 strga  ,
     &                 ltrng  , tea    , teva  , tpva  , thva  ,
     &                 densa  , denspa , ratha , ratpia, ratmia,
     &                 popar  ,
     &                 stckm  , stvr   , stvi  , stvh  ,
     &                 stvrm  , stvim  , stvhm  , stack
     &               )
          endif

C-----------------------------------------------------------------------
C CLOSE OPEN PASSING FILE DATASETS.
C-----------------------------------------------------------------------

          IF (LCONT) THEN
             CLOSE(UNIT=IUNT11)
             OPEN11=.FALSE.
          ENDIF
          IF (LPASS) THEN
             CLOSE(UNIT=IUNT12)
             OPEN12=.FALSE.
          ENDIF
          IF (LGCR) THEN
            CLOSE(UNIT=IUNT13)
            CLOSE(UNIT=IUNT14)
            CLOSE(UNIT=IUNT15)
            CLOSE(UNIT=IUNT16)
            CLOSE(UNIT=IUNT17)
            CLOSE(UNIT=IUNT18)
            CLOSE(UNIT=IUNT19)
            CLOSE(UNIT=IUNT20)
            CLOSE(UNIT=IUNT21)
            CLOSE(UNIT=IUNT22)
            CLOSE(UNIT=IUNT23)
            OPEN13=.FALSE.
            OPEN14=.FALSE.
            OPEN15=.FALSE.
            OPEN16=.FALSE.
            OPEN17=.FALSE.
            OPEN18=.FALSE.
            OPEN19=.FALSE.
            OPEN20=.FALSE.
            OPEN21=.FALSE.
            OPEN22=.FALSE.
            OPEN23=.FALSE.
          ENDIF

C----------------------------------------------------------------------
C Set up graphical output if requested.
C----------------------------------------------------------------------

          if (lgph) then
             call b8outg( lghost , date  ,
     &                    ndlev  , ndtem , ndden , ndmet  , 
     &                    titled , title , gtit1 , dsninc , 
     &                    iz     , itsel , teva(itsel)    , 
     &                    lgrd1  , ldef1 ,                  
     &                    xl1    , xu1   , yl1   , yu1    , 
     &                    il     , nmet  , nord  , maxd   , 
     &                    lmetr  , imetr , iordr , densa  , 
     &                    strga  , stack                    
     &                    )                                 

C-----------------------------------------------------------------------
C Read signal from IDL for immediate termination (1) or continue (0)
C-----------------------------------------------------------------------

             read(pipein,*) istop
             if(istop.eq.1) goto 9999

          endif

C=======================================================================
C Back to output panel
C=======================================================================

          GOTO 300

C-----------------------------------------------------------------------
 9999     STOP
C-----------------------------------------------------------------------
 1000  FORMAT('SZD COEFF. Z0=',I2,2X,'Z=',I2,2X,'Z1=',I2,
     &        2X,'LEV=',1A12,2X,'(',I1,')',I1,'(',F4.1,')',
     &        ' TO (Z+1,',I1,')')
C-----------------------------------------------------------------------
      END
