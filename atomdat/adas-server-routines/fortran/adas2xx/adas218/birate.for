      SUBROUTINE BIRATE( NDTEM  , NDTRN  , GSCALE ,
     &                   NTIN   , TIN    , GAMIN  ,
     &                   NTOUT  , TOUT   ,
     &                   ICNT   , ITRN   ,
     &                   IQS    , TCODE  ,
     &                   RATE   , DRATE  ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BIRATE *********************
C
C  PURPOSE: TO CALCULATE THE EXCITATION AND DE-EXCITATION RATE COEFFICI-
C           ENTS FOR A SET OF INPUT TEMPERATURES 'TOUT' & TRANSITIONS OF
C           A SPECIFIED TYPE (ELECTRON OR PROTON IMPACT).
C
C           TRANSITION TYPE SELECTED VIA 'ICNT & ITRN'.
C
C           INPUT RATE COEFFICIENTS 'RATE' & 'DRATE'  ASSUME  THAT  THE
C           GAMMA VALUE IS UNITY, AND ARE GIVEN FOR THE TEMPERATURES IN
C           'TOUT'. THE GAMMA VALUES 'GAMIN' ARE FOR  THE  TEMPERARTURE
C           ARRAY 'TIN'.  SPLINES ARE USED  TO  EXTRAPOLATE/INTERPOLATE
C           THE GAMMA VALUES INTO THE 'TOUT' ARRAY  AND  THESE USED  TO
C           CALCULATE THE CORRECT RATE COEFFICIENTS.
C
C           SPLINE IS CARRIED OUT USING LOG(GAMMA VALUES)
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT :  (I*4)  GSCALE  = SCALING FACTOR FOR OUTPUT GAMMA VALUES
C
C  INPUT :  (I*4)  NTIN    = NUMBER OF TEMPERATURES REPRESENTED IN THE
C                            INPUT DATA SET.
C  INPUT :  (R*8)  TIN()   = TEMPERATURES REPRESENTED IN INPUT DATA SET
C  INPUT :  (R*8)  GAMIN(,)= GAMMA VALUES REPRESENTED IN INPUT DATA SET
C                            1st DIMENSION: TEMPERATURE INDEX ('TIN')
C                            2nd DIMENSION: TRANSITION INDEX
C                                           (SEE: 'ITRN()')
C                            gammas from adf04 type 4: first half of itrn
C                            corresponds to excitation
C                            and second half to de-excitation
C
C  INPUT :  (I*4)  NTOUT   = NUMBER OF ISPF SELECTED TEMPERATURES FOR
C                            OUTPUT.
C  INPUT :  (R*8)  TOUT()  = ISPF SELECTED TEMPERATURES FOR OUTPUT.
C
C  INPUT :  (I*4)  ICNT    = NUMBER OF SELECTED TRANSITIONS
C  INPUT :  (I*4)  ITRN()  = INDEX VALUES IN MAIN TRANSITION ARRAY WHICH
C                            REPRESENT TRANSITIONS OF THE SELECTED TYPE.
C                            USED TO SELECT APPROPRIATE GAMMA VALUES FOR
C                            TRANSITION TYPE.
C  INPUT :  (I*4)  IQS     = ADF04 FILE TYPE
C  INPUT :  (C*1)  TCODE() = transition: data type pointer:
C                            ' ','1','2','3' => elec. impact trans.
C                            'p','P' => proton   impact   transition
C                            'h','H' => charge   exchange recombination
C                            'r','R' => free     electron recombination
C                            'i','I' => coll. ionis. from lower stage ion
C                            's','S' => Ionisation from current ion
C                            'l','L' => L-line for unresolved DR emissivity
C
C  I/O   :  (R*8)  RATE(,) = EXCITATION RATE COEFFS (cm**3/s)
C                            INPUT : UNIT GAMMA VALUES
C                            OUTPUT: TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C  I/O   :  (R*8)  DRATE(,)= DE-EXCIT'N RATE COEFFS (cm**3/s)
C                            INPUT : UNIT GAMMA VALUES
C                            OUTPUT: TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C
C  OUTPUT:  (L*4)  LTRNG() = .TRUE. => TEMPERATURE VALUES WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                          = .FALSE.=>TEMPERATURE VALUE NOT WITHIN RANGE
C                                     READ FROM INPUT COPASE DATA SET.
C                            1st DIMENSION: TEMPERATURE INDEX.
C
C
C           (I*4)  NTDSN   = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C                                        ALLOWED IN INPUT DATA SET = 8
C           (I*4)  NLTEM   = PARAMETER = MUST BE >= 'NDTEM'
C
C           (I*4)  GZERO   = PARAMETER = IF 'GAMIN(1,) < GZERO' THEN ALL
C                                        THE 'RATE' AND  'DRATE'  VALUES
C                                        FOR THE  GIVEN  TRANSITION  ARE
C                                        SAID TO BE ZERO.
C
C           (I*4)  IOPT    = SPLINE END CONDITIONS/EXTRAPOLATION CONTROL
C                            SWITCH - SEE 'XXSPLE'
C                            I.E. DEFINES THE BOUNDARY DERIVATIVES.
C                            (VALID VALUES = 0, 1, 2, 3, 4)
C           (I*4)  ITRANU  = APPROPRIATE TRANSITION INDEX FOR 'GAMIN(,)'
C                            UPSILON
C           (I*4)  ITRAND  = APPROPRIATE TRANSITION INDEX FOR 'GAMIN(,)'
C                            DOWNSILON
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C           (I*4)  IT      = TEMPERATURE ARRAY INDEX
C
C           (R*8)  GAMMA   = SPLINED GAMMA VALUE FOR GIVEN TEMPERATURE
C                            (FROM 'TOUT()') AND TRANSITION.
C           (R*8)  DYIN()  = INTERPOLATED DERIVATIVES
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C
C           (L*4)  LETYP   = .TRUE. => ELECTRON EXCITATION TRANSITIONS
C                                      PRESENT AND IQS IS TYPE 4
C           (L*4)  LSETX   = .TRUE.  => X-AXES ('TIN()' VALUES) NEED TO
C                                       SET IN 'XXSPLE'.
C                            .FALSE. => X-AXES ('TIN()' VALUES) HAVE
C                                       BEEN SET IN 'XXSPLE'.
C                            (NOTE: 'LSETX' IS RESET BY 'XXSPLE')
C
C           (R*8)  LGIN()  = LOG ( 'GAMIN(,)' ) FOR GIVEN TRANSITION
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C           (R*8)  LGOUT() = LOG ( SPLINED GAMMA VALUES )
C                            DIMENSION: TEMPERATURE INDEX ('TOUT()' )
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C
C
C-----------------------------------------------------------------------
C
C Based on b8scom and returns gamma for type 4 adf04 files which have
C separate upward and downward collision strengths.
C
C VERSION  : 1.1
C DATE     : 25/11/2003
C MODIFIED : Paul Bryans
C            - First version.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NTDSN              , NLTEM
C-----------------------------------------------------------------------
      REAL*8     GZERO
C-----------------------------------------------------------------------
      PARAMETER( NTDSN = 14         , NLTEM = 101        )
C-----------------------------------------------------------------------
      PARAMETER( GZERO = 1.01D-70   )
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDTEM              ,
     &           NTIN               , NTOUT              ,
     &           ICNT               , IQS
      INTEGER    IOPT               ,
     &           IC                 , IT                 ,
     &           ITRANU             , ITRAND
C-----------------------------------------------------------------------
      REAL*8     GSCALE             , GAMMA
C-----------------------------------------------------------------------
      LOGICAL    LSETX              , LETYP
C-----------------------------------------------------------------------
      INTEGER    ITRN(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     TIN(NTDSN)         , GAMIN(NTDSN,NDTRN) ,
     &           TOUT(NDTEM)        ,
     &           RATE(NDTEM,NDTRN)  , DRATE(NDTEM,NDTRN)
      REAL*8     DYIN(NTDSN)        ,
     &           LGIN(NTDSN)        , LGOUT(NLTEM)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTEM)
C-----------------------------------------------------------------------
      INTRINSIC  DLOG
C-----------------------------------------------------------------------
      CHARACTER  TCODE(NDTRN)*1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NLTEM.LT.NDTEM) STOP
     &                   ' BIRATE ERROR: NDTEM > NLTEM. INCREASE NLTEM'
C-----------------------------------------------------------------------
      LSETX = .TRUE.
      IOPT  = 0
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C If there is one value and the requested point within EPS do not
C spline but return the values - allow variation of 10K in temperatures.
C-----------------------------------------------------------------------

      if (ntin.eq.1) then

         if (ntout.gt.1) then
            write(0,*)'Only one point in input data'
            stop
         endif

         if (abs(tin(1)-tout(1)).GT.10) then
            write(0,*)'adf04 Te and requested Te too different'
            write(0,*)tin(1), tout(1)
            stop
         endif

         do ic = 1, icnt

            itranu = itrn(ic)
            if (iqs.eq.4) then
               itrand = itrn(ic+icnt)
               if (    (tcode(itranu).eq.' ')
     &             .or.(tcode(itranu).eq.'1')
     &             .or.(tcode(itranu).eq.'2')
     &             .or.(tcode(itranu).eq.'3') )
     &              letyp=.true.
            endif

            gamma = gscale * gamin(1,itranu)
            rate(1,ic)  = rate(1,ic)  * gamma
            if(iqs.eq.3) drate(1,ic) = drate(1,ic) * gamma

            if (letyp) then
               gamma = gscale * gamin(1,itrand)
               drate(1,ic) = drate(1,ic) * gamma
            endif

         end do

         return   ! do not continue

      endif

C-----------------------------------------------------------------------
         DO 1 IC=1,ICNT

            LETYP=.FALSE.
            ITRANU = ITRN(IC)
            IF (IQS.EQ.4) THEN
               ITRAND = ITRN(IC+ICNT)
               IF (    (TCODE(ITRANU).EQ.' ')
     &             .OR.(TCODE(ITRANU).EQ.'1')
     &             .OR.(TCODE(ITRANU).EQ.'2')
     &             .OR.(TCODE(ITRANU).EQ.'3') )
     &              LETYP=.TRUE.
            ENDIF

C-----------------------------------------------------------------------
               IF (GAMIN(1,ITRANU).GT.GZERO) THEN
C-----------------------------------------------------------------------

                  DO 2 IT=1,NTIN
                     LGIN(IT)=DLOG( GAMIN(IT,ITRANU) )
    2             CONTINUE

                  CALL XXSPLE( LSETX , IOPT  , DLOG   ,
     &                         NTIN  , TIN   , LGIN   ,
     &                         NTOUT , TOUT  , LGOUT  ,
     &                         DYIN  , LTRNG
     &                       )


                  DO 3 IT=1,NTOUT
                     GAMMA = GSCALE * DEXP( LGOUT(IT) )
                     RATE(IT,IC)  = RATE(IT,IC)  * GAMMA
                     IF(IQS.EQ.3) DRATE(IT,IC) = DRATE(IT,IC) * GAMMA
    3             CONTINUE

C-----------------------------------------------------------------------
               ELSE
C-----------------------------------------------------------------------

                     DO 4 IT=1,NTOUT
                        RATE(IT,IC)  = 0.0D0
                        DRATE(IT,IC) = 0.0D0
    4                CONTINUE

C-----------------------------------------------------------------------
               ENDIF

C-----------------------------------------------------------------------
C CALCULATE ELECTRON IMPACT DE-EXCITATION RATE FROM DOWNSILON IF USING
C ADF04 TYPE 4 FILE
C-----------------------------------------------------------------------

               IF (LETYP) then
                  if (GAMIN(1,ITRAND).GT.GZERO) THEN

                  DO 5 IT=1,NTIN
                     LGIN(IT)=DLOG( GAMIN(IT,ITRAND) )
    5             CONTINUE

                  CALL XXSPLE( LSETX , IOPT  , DLOG   ,
     &                         NTIN  , TIN   , LGIN   ,
     &                         NTOUT , TOUT  , LGOUT  ,
     &                         DYIN  , LTRNG
     &                       )

                  DO 6 IT=1,NTOUT
                     GAMMA = GSCALE * DEXP( LGOUT(IT) )
                     DRATE(IT,IC) = DRATE(IT,IC) * GAMMA
    6             CONTINUE
                  endif
               ENDIF


    1    CONTINUE
C***********************************************************************
      RETURN
      END
