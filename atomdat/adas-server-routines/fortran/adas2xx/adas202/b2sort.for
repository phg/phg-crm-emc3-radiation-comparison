CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas202/b2sort.for,v 1.1 2004/07/06 11:19:19 whitefor Exp $ Date $Date: 2004/07/06 11:19:19 $
CX
       SUBROUTINE B2SORT(XA,YA,N)                                       
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B2SORT *********************
C
C PURPOSE : TO SORT AN ARRAY SO THAT XA IS IN INCREASING ORDER        
C
C  N.B.  INPUT VALUES ARE ALTERED BY THIS ROUTINE !!!!
C
C  INPUT
C      XA(I)=X-VALUES
C      YA(I)=Y-VALUES
C      N=NUMBER OF VALUES
C  OUTPUT
C      XA(I)=SORTED X-VALUES
C      YA(I)=SORTED Y-VALUES
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C
       DIMENSION XA(10),YA(10)                                          
       N1=N-1                                                           
       DO 10 I=1,N1                                                     
       I1=I+1                                                           
       DO 5 J=I1,N                                                      
       IF(XA(I).LE.XA(J))GO TO 5                                        
       SWAP=XA(I)                                                       
       XA(I)=XA(J)                                                      
       XA(J)=SWAP                                                       
       SWAP=YA(I)                                                       
       YA(I)=YA(J)                                                      
       YA(J)=SWAP                                                       
    5  CONTINUE                                                         
   10  CONTINUE                                                         
       RETURN                                                           
      END                                                               
