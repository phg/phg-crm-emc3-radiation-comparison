CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas202/b2nfas.for,v 1.1 2004/07/06 11:19:09 whitefor Exp $ Date $Date: 2004/07/06 11:19:09 $
CX
       SUBROUTINE B2NFAS(X,XA,N,YA,Y,DY,C1,C2,C3,C4,FORM,IFORMS)        
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B2NFAS *********************
C
C PURPOSE:
C  SUBROUTINE TO PROVIDE A SPLINE INTERPOLATE MAKING USE OF SPECIFIED
C  ASYMPTOTIC BEHAVIOUR
C
C
C  USES LABELLED COMMON /SPL3/
C
C  INPUT
C      X=REQUIRED X-VALUE
C      X(I)=KNOTS
C      N=NUMBER OF KNOTS
C      C1(I,J)=1ST SPINE COEFFICIENT PRECURSOR
C      C2(I,J)=2ND SPINE COEFFICIENT PRECURSOR
C      C3(I,J)=3RD SPINE COEFFICIENT PRECURSOR
C      C4(I,J)=4TH SPINE COEFFICIENT PRECURSOR
C      FORM=EXTERNAL FUNCTION SPECIFYING ASYMPTOTIC FORMS
C      IFORMS=INDEX OF REQUIRED FORM
C  OUTPUT
C      Y=RETURNED Y-VALUE
C      DY=RETURNED DERIVATIVE
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C
       DIMENSION XA(10),YA(10),C1(10,9),C2(10,9),C3(10,9),C4(10,9)      
       COMMON /SPL3/IEND(2),G(2),AB(4),PQ(12),ABRY(40)                  
       DY=0.0D0                                                         
       IF(X.GE.XA(1))GO TO 5                                            
       IENDS=1                                                          
       IN=1                                                             
       GO TO 15                                                         
    5  IF(X.LE.XA(N))GO TO 10                                           
       IENDS=2                                                          
       IN=N                                                             
       GO TO 15                                                         
   10  CALL B2NFIT(X,XA,N,YA,Y,DY,0,C1,C2,C3,C4,0)                      
       RETURN                                                           
   15  K=IEND(IENDS)                                                    
       GOTO(20,30,30,40),K                                              
   20  DY=G(IENDS)*YA(IN)                                               
   25  Y=YA(IN)+(X-XA(IN))*DY                                           
       RETURN                                                           
   30  XX=XA(IN)                                                        
       CALL B2NFIT(XX,XA,N,YA,Y,DY,0,C1,C2,C3,C4,0)                     
       IF(IEND(IENDS).EQ.2)GO TO 25                                     
       IF(IENDS.EQ.2)GO TO 35                                           
       H=XA(2)-XA(1)                                                    
       DDY=2.0D0*(-YA(1)+YA(2)-H*DY)/(H*H)                              
       GO TO 38                                                         
   35  H=XA(N)-XA(N-1)                                                  
       DDY=2.0D0*(YA(N-1)-YA(N)+H*DY)/(H*H)                             
   38  DX=X-XA(IN)                                                      
       Y=YA(IN)+DX*DY+0.5D0*DX*DX*DDY                                   
       RETURN                                                           
   40  A=0.0D0                                                          
       B=0.0D0                                                          
       DO 42 I=1,N                                                      
       A=A+ABRY(20*IENDS-20+I)*YA(I)                                    
   42  B=B+ABRY(20*IENDS-10+I)*YA(I)                                    
       J=4*IFORMS+2*IENDS-5                                             
       Y=A*FORM(J,X)+B*FORM(J+1,X)                                      
       RETURN                                                           
      END                                                               
