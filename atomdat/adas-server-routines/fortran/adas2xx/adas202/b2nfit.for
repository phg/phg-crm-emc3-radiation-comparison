CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas202/b2nfit.for,v 1.1 2004/07/06 11:19:12 whitefor Exp $ Date $Date: 2004/07/06 11:19:12 $
CX
       SUBROUTINE B2NFIT(X,XA,N,YAA,Y,DY,I0,C1,C2,C3,C4,ISW)            
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B2NFIT *********************
C
C PURPOSE:
C  SUBROUTINE TO PERFORM SPLINE INTERPOLATION                           
C
C
C  INPUT
C      X       = REQUIRED X-VALUE
C      XA(I)   = X-VALUES
C      N       = NUMBER OF VALUES
C      YAA(I)  = Y-VALUES (POSSIBLY STORED AS MULTIPLE SETS)
C      I0      = STARTING INDEX(-1) IN YAA ARRAY OF REQUIRED INPUT SET
C      C1(I,J) = 1ST SPLINE COEFFICIENT PRECURSOR
C      C2(I,J) = 2ND SPLINE COEFFICIENT PRECURSOR
C      C3(I,J) = 3RD SPLINE COEFFICIENT PRECURSOR
C      C4(I,J) = 4TH SPLINE COEFFICIENT PRECURSOR
C      ISW     = .LE.0  ORDINARY     SPLINE INTERPOLATION
C              = .GT.0  LOGARITHMIC  SPLINE INTERPOLATION
C  OUTPUT
C      Y       = RETURNED Y-VALUE
C      DY      = RETURNED DERIVATIVE
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-03-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C
       DIMENSION YAA(10),TA(10),XA(10)                                  
       DIMENSION C1(10,9),C2(10,9),C3(10,9),C4(10,9)                    
       DIMENSION CT1(9),CT2(9),CT3(9),CT4(9)                            
       DO 3 I=1,N                                                       
       T=YAA(I+I0)                                                      
       IF(ISW.GT.0)T=DLOG(T)                                            
    3  TA(I)=T                                                          
       DO 24 J=2,N                                                      
       J1=J-1                                                           
       CT1(J1)=0.0                                                      
       CT2(J1)=0.0                                                      
       CT3(J1)=0.0                                                      
       CT4(J1)=0.0                                                      
       DO 23 I=1,N                                                      
       CT1(J1)=CT1(J1)+C1(I,J1)*TA(I)                                   
       CT2(J1)=CT2(J1)+C2(I,J1)*TA(I)                                   
       CT3(J1)=CT3(J1)+C3(I,J1)*TA(I)                                   
   23  CT4(J1)=CT4(J1)+C4(I,J1)*TA(I)                                   
   24  CONTINUE                                                         
       DO 27 J=2,N                                                      
       IF(X.GT.XA(J))GO TO 27                                           
       XB=0.5D0*(XA(J-1)+XA(J))                                         
       J1=J-1                                                           
       GO TO 25                                                         
   27  CONTINUE                                                         
       XB=0.5*(XA(N-1)+XA(N))                                           
       J1=N-1                                                           
   25  XB=X-XB                                                          
       Y=CT1(J1)+XB*(CT2(J1)+XB*(CT3(J1)+XB*CT4(J1)))                   
       DY=CT2(J1)+XB*(2.0D0*CT3(J1)+XB*3.0D0*CT4(J1))                   
       IF(ISW.LE.0)GO TO 26                                             
       Y=DEXP(Y)                                                        
       DY=DY*Y                                                          
   26  RETURN                                                           
       END                                                              
