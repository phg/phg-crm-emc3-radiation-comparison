CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas213/bdspf1.for,v 1.1 2004/07/06 11:37:51 whitefor Exp $ Date $Date: 2004/07/06 11:37:51 $
CX
      SUBROUTINE BDSPF1( YESNO  , DSNPAP , LPAPER ,
     &                   LPEND)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BDSPF1 *********************
C
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL OR
C	    THE INFO FILE (WHICH HAS BEEN REDIRECTED TO STANDARD INPUT).
C
C  CALLING PROGRAM: ADAS213
C
C  SUBROUTINE:
C
C  OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT TEXT TO 'PAPER.TXT'
C                                        FILE.
C                             .FALSE. => NO TEXT OUTPUT
C
C  OUTPUT: (C*80)  DSNPAP   = OUTPUT TEXT FILE NAME
C
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => USER SELECTED 'CANCEL'
C                             .FALSE. => USER DID NOT
C
C          (I*4)   PIPEIN   = STANDARD INPUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  HP SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C DATE:    30TH MARCH 1998
C
C VERSION: 1.1                          DATE: 30-03-98
C MODIFIED: HP SUMMERS
C               - FIRST VERSION.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      INTEGER      PIPEIN, LOGIC
      PARAMETER   (PIPEIN = 5)
C-----------------------------------------------------------------------
      CHARACTER    DSNPAP*80 , YESNO*3
C-----------------------------------------------------------------------
      LOGICAL      LPAPER  , LPEND
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      READ(PIPEIN,*) LOGIC
      IF(LOGIC.NE.1)THEN
         LPEND = .FALSE.
         READ(PIPEIN,'(A)')YESNO
         READ(PIPEIN,'(A)')DSNPAP
         READ(PIPEIN,*)LOGIC
         IF(LOGIC.EQ.0)THEN
            LPAPER=.FALSE.
         ELSE
            LPAPER=.TRUE.
         ENDIF
      ELSE
         LPEND = .TRUE.
      ENDIF

      RETURN
      END
