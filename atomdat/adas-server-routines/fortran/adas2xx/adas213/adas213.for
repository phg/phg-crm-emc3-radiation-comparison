CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas213/adas213.for,v 1.1 2004/07/06 10:24:16 whitefor Exp $ Date $Date: 2004/07/06 10:24:16 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS213 **********************
C
C  ORIGINAL NAME: 
C
C  VERSION:  1.1
C
C  PURPOSE:  TO SUPPLEMENT SPECIFIC ION FILES OF TYPE ADF04 WITH 
C            STATE SELECTIVE ELECTRON IMPACT IONISATION BY 
C            POST-PROCESSING FILES OF TYPE ADF23
C
C  PROGRAM:
C         (I*4) NDLEV       = PARAMETER = MAX. NO. OF LEVELS 
C         (I*4) NDREP       = PARAMETER = MAX. NO. OF REPRESENT. N-SHELLS 
C         (I*4) NDJLEV      = PARAMETER = MAX. NO. OF J-RESOLVED LEVELS 
C         (I*4) NDTRN       = PARAMETER = MAX. NO. OF TRANSITIONS 
C         (I*4) NDMET       = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C         (I*4) NDT         = PARAMETER = MAX. NO. OF ELEC. TEMPS IN ADF23 FILE
C         (I*4) NDPRT       = PARAMETER = MAX. NO. OF PARENTS 
C         (I*4) NVMAX       = PARAMETER = MAX. NO. OF ELEC. TEMPS IN ADF04 FILE 
C         (I*4) NDPRTI      = PARAMETER = MAX. NO. OF INTERM. PARENT STATES
C         (I*4) NDAUG       = PARAMETER = ??
C         (I*4) IUNT12      = PARAMETER = UNIT FOR X-REF FILE & INPUT ADF04 FILE
C         (I*4) IUNT13      = PARAMETER = UNIT FOR ADF23 IONIS. DATA FILE
C         (I*4) IUNT14      = PARAMETER = UNIT FOR OUTPUT ADF04 FILE
C         (I*4) IUNT17      = PARAMETER = UNIT FOR TEXT OUTPUT FILE
C
C         (I*4) NTRM        = NUMBER OF TERMS IN X-REF FILE.
C         (I*4) NPTRM       = NUMBER OF PARENT TERMS IN X-REF FILE.
C         (I*4) NLVL        = NUMBER OF LEVELS IN X-REF FILE.
C         (I*4) NPLVL       = NUMBER OF PARENT LEVELS IN X-REF FILE.
C         (I*4) IOPT        = 
C         (I*4) I4UNIT      = FUNCTION   
C         (I*4) IERROR      = 0 => X-REF FILE OK
C                             1 => FAULT IN XREF FILE DATASETS
C                             2 => FAULT IN XREF FILE TERM COUNT
C                             3 => FAULT IN XREF FILE LEVEL COUNT
C         (I*4) NBFIL       = FORCED TO UNITY 
C         (I*4) IZ          =  RECOMBINED ION CHARGE READ
C         (I*4) IZ0         =         NUCLEAR CHARGE READ
C         (I*4) IZ1         = RECOMBINING ION CHARGE READ
C                              (NOTE: IZ1 SHOULD EQUAL IZ+1)
C         (I*4) IGZ         = RECOMBINED ION CHARGE FROM ADF23 FILE
C         (I*4) IGZ0        = NUCLEAR CHARGE FROM ADF23 FILE
C         (I*4) IGZ1        = RECOMBINING ION CHARGE FROM ADF23 FILE
C         (I*4) IL          = NO. OF LEVELS IN INPUT ADF04 FILE
C         (I*4) NV          = NO. OF TEMPERATURES IN INPUT ADF04 FILE
C         (I*4) ITRAN       = NO. OF TRANSITIONS IN INPUT ADF04 FILE
C         (I*4) MAXLEV      = INDEX OF HIGHEST LEVEL IN INPUT ADF04 FILE
C         (I*4) NPL         = NUMBER OF PARENTS ON FIRST LINE OF INPUT ADF04
C                              FILE AND USED IN LEVEL ASSIGNMENTS
C         (I*4) NPRF        = NUMBER OF FINAL PARENTS
C         (I*4) NPRFM       = NUMBER OF FINAL PARENTS WHICH ARE METASTABLES
C         (I*4) NPRI        = NUMBER OF FINAL PARENTS WHICH ARE INTERMEDIATE
C                              PARENTS FOR REPR. N-SHELL DOUBLY EXCITED STATES
C         (I*4) NGLEV       = NUMBER OF ENERGY LEVELS (TERMS) OF THE
C                              IONISING ION FROM ADF23 FILE
C         (I*4) NTE         = NUMBER OF ELECTRON TEMPS. FROM ADF23 FILE
C         (I*4) NLEVM       = NUMBER OF IONISING ION LEVELS WHICH ARE
C                              METASTABLES
C         (I*4) NPF         = ??
C         (I*4) ISTRM()     = SPEC. ION FILE TERM INDEX FROM X-REF
C                             1ST.DIM.: TERM COUNTER IN X-REF FILE 
C         (I*4) IGTRM()     = IONIS. FILE TERM INDEX FROM X-REF
C                             1ST.DIM.: TERM COUNTER IN X-REF FILE 
C         (I*4) ISPTRM()    = SPEC. ION FILE PRNT. TERM INDEX FROM X-REF
C                             1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C         (I*4) IGPTRM()    = IONIS. FILE PRNT. TERM INDEX FROM X-REF
C                             1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C         (I*4) ISLVL()     = SPEC. ION FILE LEVEL INDEX FROM X-REF
C                             1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C         (I*4) JTREF()     = SP. ION FILE TERM ASSOCIATED WITH LEVEL
C                             FROM X-REF FILE.
C                             1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C         (I*4) ISPLVL()    = SPEC. ION FILE PRNT. LEVEL INDEX FROM X-REF
C                             1ST.DIM.: PRNT. LEVEL COUNTER IN X-REF FILE 
C         (I*4) JTPREF()    = SP. ION FILE PRNT. TERM ASSOCIATED WITH 
C                             PRNT. LEVEL FROM X-REF FILE.
C                             1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C         (I*4) ISPTRM()    = SPEC. ION FILE PRNT. TERM INDEX FROM X-REF
C                             1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C         (I*4) IGPTRM()    = IONIS. FILE PRNT. TERM INDEX FROM X-REF
C                             1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE
C
C         (R*8) FSLVL()     = FRACTIONATION OF TERM RATES AMONG LEVELS
C                             1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C         (R*8) FSPLVL()    = FRACTIONATION OF PRNT. TERM RATES AMONG 
C                             PRNT. LEVELS
C                             1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C
C         (L*4) OPEN12      = .TRUE. => UNIT12 OPEN FOR I/O
C                             .FALSE.=> UNIT12 NOT OPEN FOR I/O
C         (L*4) OPEN13      = .TRUE. => UNIT13 OPEN FOR I/O
C                             .FALSE.=> UNIT13 NOT OPEN FOR I/O
C         (L*4) OPEN14      = .TRUE. => UNIT14 OPEN FOR I/O
C                             .FALSE.=> UNIT14 NOT OPEN FOR I/O
C         (L*4) OPEN17      = .TRUE. => UNIT17 OPEN FOR I/O
C                             .FALSE.=> UNIT17 NOT OPEN FOR I/O
C         (L*4) LEXIST      = .TRUE. => DATA FILE EXISTS
C                             .FALSE.=> DATA FILE DOES NOT EXIST
C         (L*4) LSJ         = .TRUE. => J-RESOL. INFO. IN X-REF FILE
C                             .FALSE.=> NO J-RESOL. IN X-REF FILE
C         (L*4) LSETX       = .TRUE. => UNIT12 OPEN FOR I/O
C                             .FALSE.=> UNIT12 NOT OPEN FOR I/O
C         (L*4) LPAPER      = .TRUE. => TEXT OUTPUT TO PAPER.TXT 
C                             .FALSE.=> NO TEXT OUTPUT TO PAPER.TXT
C         (L*4) LPEND       = .TRUE. => EXIT FROM PROGRAM
C                             .FALSE.=> EXECUTE PROGRAM
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          BDSPF0     ADAS      GATHERS INPUT FILE NAME FROM IDL
C          BDSPF1     ADAS      GATHERS OUTPUT FILE NAME FROM IDL
C          BDXREF     ADAS      GATHERS DATA FROM CROSS-REFERENCE FILE
C          BDWR14     ADAS      WRITE OUT SUPPLEMENTED ADF04 FILE
C          BADATA     ADAS      READ ADF04 FILE FOR SUPPLEMENTATION
C          DDDATA     ADAS      READ ADF23 FILE FOR IONISATION DATA
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXGUID     ADAS      GATHERS USER NAME FROM IDL
C          XXFLNM     ADAS      CONVERT SHORT FILE NAME TO FULL NAME
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C  AUTHOR:  HP SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    30 MAR 1998
C
C  UPDATE:  
C
C  VERSION: 1.1                          DATE: 30-03-98
C  MODIFIED: HP SUMMERS
C               - FIRST VERSION.
C
C
C-----------------------------------------------------------------------
       INTEGER   NDLEV    , NDREP   , NDJLEV  , 
     &           NDTRN    , NDMET   , NDT     ,
     &           NDPRT    , NVMAX   ,
     &           NDPRTI   , NDAUG   
       INTEGER   IUNT12   , IUNT13  , IUNT14  , IUNT17
C-----------------------------------------------------------------------   
       PARAMETER ( NDLEV  = 100  , NDREP  = 100 , NDJLEV = 100 )
       PARAMETER ( NDTRN  = 200)
       PARAMETER ( NDMET  = 5    , NDT    = 10  , NDPRT  = 40 )
       PARAMETER ( NDPRTI = 10   , NDAUG  = 8 )
       PARAMETER ( NVMAX  = 14 )
       PARAMETER ( IUNT12 = 12   , IUNT13 = 13  , IUNT14 = 14   ,
     &             IUNT17 = 17 )
C-----------------------------------------------------------------------
       INTEGER   NTRM     , NPTRM   , NLVL    , NPLVL   
       INTEGER   IOPT     , I4UNIT 
       INTEGER   IERROR   , NBFIL
       INTEGER   IZ       , IZ0     , IZ1     ,
     &           IGZ      , IGZ0    , IGZ1    , 
     &           IL       , NV      , ITRAN   ,
     &           MAXLEV   , NPL
       INTEGER   NPRF     , NPRFM   , NPRI    , NGLEV   , NTE    ,
     &           NLEVM    , NPF         
C-----------------------------------------------------------------------
       REAL*8    FINTX
       REAL*8    BWNO     , BWNF    , BWNI            
C-----------------------------------------------------------------------
       CHARACTER DSNXR*80   , DSNSP*80   , DSNSPO*80   
       CHARACTER DSNPAP*80  , DSN80*80   , DSNBD*80    
       CHARACTER TITLED*3   , SEQSYM*2
       CHARACTER YESNO*3
       CHARACTER REP*3
       CHARACTER USERID*10  , DATE*8
C-----------------------------------------------------------------------
       LOGICAL   OPEN12   , OPEN13   , OPEN14   , OPEN17
       LOGICAL   LEXIST   , LSJ      , LSETX
       LOGICAL   LPAPER   , LPEND
       LOGICAL   LRAUG       
C-----------------------------------------------------------------------
       INTEGER   ISTRM(NDLEV)   , IGTRM(NDLEV)  , 
     &           ISPTRM(NDPRT)  , IGPTRM(NDPRT) ,
     &           ISLVL(NDJLEV)  , JTREF(NDJLEV) ,
     &           ISPLVL(NDPRT)  , JTPREF(NDPRT) 
       INTEGER   IA(NDLEV)      , ISA(NDLEV)    , ILA(NDLEV) ,
     &           I1A(NDTRN)     , I2A(NDTRN)    ,
     &           IPLA(NDMET,NDLEV)              , NPLA(NDLEV)
       INTEGER   IPA(NDPRT)     , ISPA(NDPRT)   , ILPA(NDPRT)
       INTEGER   IGA(NDLEV)     , IGSA(NDLEV)   , IGLA(NDLEV)
       INTEGER   INDF(NDPRT) 
       INTEGER   IMETI(NDMET)   , NSYSM(NDMET)  , NREPM(NDMET)
       INTEGER   IPRTI(NDMET,NDPRTI)            , ISYSI(NDMET,NDPRTI)  , 
     &           ISPSYI(NDMET,NDPRTI)
       INTEGER   IMETF(NDMET,NDPRTI,NDPRT)    , NVALS(NDMET,NDPRTI)
       INTEGER   NPIS(NDMET)                  , NREPI(NDMET,NDPRTI)    ,
     &           IREP(NDREP,NDMET,NDPRTI)   
       INTEGER   IPRFM(NDPRT)   , ILEVM(NDLEV)  , IPRI(NDPRTI)
C-----------------------------------------------------------------------
       REAL*8    FSLVL(NDJLEV)  , FSPLVL(NDPRT)
       REAL*8    TEA(NDT)
       REAL*8    BWNOA(NDMET)   , PRTWTA(NDMET)      , 
     &           SCEF(NVMAX)
       REAL*8    XJA(NDLEV)     , WA(NDLEV)          ,
     &           AVAL(NDTRN)    , SCOM(NVMAX,NDTRN)  ,
     &           ZPLA(NDMET,NDLEV)
       REAL*8    XJPA(NDPRT)    , WPA(NDPRT)
       REAL*8    XGJA(NDLEV)    , WGA(NDLEV)
       REAL*8    RAUG(NDLEV,NDPRT)  
       REAL*8    RION(NDMET,NDPRT,NDT) , REXC(NDMET,NDLEV,NDT)
       REAL*8    AUGN(NDREP,NDMET,NDPRTI,NDPRT)                        ,
     &           EXCN(NDREP,NDMET,NDPRTI,NDT)
C-----------------------------------------------------------------------
       CHARACTER TCODE(NDTRN)*1   , CSTRGA(NDLEV)*18 
       CHARACTER CPLA(NDLEV)*1    , CPRTA(NDMET)*9
       CHARACTER CSTRPA(NDPRT)*18 , CGSTRGA(NDLEV)*18
C-----------------------------------------------------------------------
       LOGICAL   LBSETA(NDMET)
       LOGICAL   LSYSM(NDMET)       , LREPM(NDMET)
       LOGICAL   LIONIS(NDMET)      , LEXCIT(NDMET)
       LOGICAL   LRION(NDMET,NDPRT) , LREXC(NDMET,NDLEV)
       LOGICAL   LEXCN(NDREP,NDMET,NDPRTI)
C-----------------------------------------------------------------------
       INTEGER   PIPEIN   , PIPEOU   , ONE
C-----------------------------------------------------------------------
       PARAMETER ( PIPEIN = 5 , PIPEOU = 6 , ONE = 1)
C-----------------------------------------------------------------------
       DATA  OPEN12/.FALSE./ , OPEN13/.FALSE./ , OPEN14/.FALSE./,
     &       OPEN17/.FALSE./
       DATA  LEXIST/.FALSE./ , LSETX/.FALSE./
       DATA  IOPT/1/
C-----------------------------------------------------------------------
       EXTERNAL FINTX
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C****************** MAIN PROGRAM   *************************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
       CALL XX0000
C-----------------------------------------------------------------------
C SET USERID AND DATE
C-----------------------------------------------------------------------
C
       CALL XXDATE(DATE)
       CALL XXGUID(USERID)
C
C-----------------------------------------------------------------------
C GET CROSS-REFERENCE DATA SET NAME FROM IDL.
C-----------------------------------------------------------------------
C
 500   CALL BDSPF0( REP , DSNXR)

C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED:  END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C COMMUNICATE WITH THE IDL OUTPUT SCREEN
C-----------------------------------------------------------------------
C

       CALL BDSPF1(YESNO, DSNPAP, LPAPER, LPEND)
C
C-----------------------------------------------------------------------
C IF CANCEL SELECTED THEN RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 500

C
C-----------------------------------------------------------------------
C OPEN PAPER.TXT FILE - IF REQUESTED
C-----------------------------------------------------------------------
C
      IF (LPAPER) THEN
          IF (OPEN17)  CLOSE(UNIT=IUNT17)
          DSN80=' '
          DSN80=DSNPAP
          OPEN(UNIT=17,FILE=DSN80, STATUS='UNKNOWN')
          OPEN17=.TRUE.
      ENDIF
C
      IF(LPAPER) THEN
         WRITE(IUNT17,*)DSNXR
      ENDIF
C
C
C-----------------------------------------------------------------------
C OPEN CROSS-REFERENCE FILE AND READ IT IN
C-----------------------------------------------------------------------
C
      IF (OPEN12)  CLOSE(UNIT=IUNT12)
      INQUIRE( FILE=DSNXR   , EXIST=LEXIST)
      IF (LEXIST) THEN
         OPEN(UNIT=IUNT12 , FILE = DSNXR, STATUS = 'OLD')
         OPEN12 = .TRUE.
      ELSE
         WRITE(I4UNIT(-1),1009) DSNXR
         STOP
      ENDIF
C
      CALL  BDXREF( IUNT12 , NDLEV  , NDJLEV , NDPRT  ,
     &              DSNSP  , DSNBD  , DSNSPO ,  
     &              NTRM   , NPTRM  , NLVL   , NPLVL  ,
     &              ISTRM  , IGTRM  , ISPTRM , IGPTRM ,
     &              ISLVL  , FSLVL  , JTREF  ,
     &              ISPLVL , FSPLVL , JTPREF ,
     &              LSJ    , IERROR
     &            )
C
      CLOSE(UNIT=IUNT12)
      OPEN12=.FALSE.
C     
C-----------------------------------------------------------------------
C     CLOSE IUNT12 AND OPEN AGAIN AS SPECIFIC ION INPUT FILE
C     OPEN IUNT14 FOR THE SUPPLEMENTED SPECIFIC ION FILE OUTPUT
C-----------------------------------------------------------------------
C     
      IF (OPEN12) CLOSE(UNIT=IUNT12)
      OPEN(UNIT=IUNT12 , FILE = DSNSP, STATUS = 'OLD')
      OPEN12=.TRUE.
C     
       CALL BADATA( IUNT12 , NDLEV  , NDTRN , NDMET ,
     &              TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
     &              NPL    , BWNOA  , LBSETA, PRTWTA, CPRTA ,
     &              IL     ,
     &              IA     , CSTRGA , ISA   , ILA   , XJA   , WA ,
     &              CPLA   , NPLA   , IPLA  , ZPLA  ,
     &              NV     , SCEF   ,
     &              ITRAN  , MAXLEV ,
     &              TCODE  , I1A    , I2A   , AVAL  , SCOM     
     &            )
C
      CLOSE(UNIT=IUNT12)
      OPEN12=.FALSE.
C     
C-----------------------------------------------------------------------
C     TELL THE IDL HOW MUCH COMPUTING THERE IS TO DO
C     OPEN 13 FOR IONISATION DATA INPUT AND READ IT IN 
C-----------------------------------------------------------------------
C
      NBFIL = 1
      WRITE(PIPEOU,*)NBFIL
      CALL XXFLSH(PIPEOU)
C
      LEXIST = .FALSE.
      IF (OPEN13) CLOSE(13)
      OPEN13=.FALSE.
      INQUIRE( FILE=DSNBD , EXIST=LEXIST)
      IF(LEXIST) THEN
          OPEN(UNIT=13 , FILE = DSNBD, STATUS = 'UNKNOWN')
          OPEN13=.TRUE.
      ELSE
          WRITE(I4UNIT(-1),1009) DSNBD
          STOP
      ENDIF
C     
C-----------------------------------------------------------------------
C     FETCH NECESSARY DATA FROM IONISATION DATA FILE
C-----------------------------------------------------------------------
C     
      CALL  DDDATA( IUNT13 , NDPRT  , NDPRTI, NDREP , NDLEV ,
     &              NDMET  , NDAUG  , NDT   ,
     &              SEQSYM , IGZ    , IGZ0  , IGZ1  ,
     &              NPRF   , BWNF   , NPRFM , IPRFM ,
     &              NPRI   , IPRI   ,
     &              IPA    , CSTRPA , ISPA  , ILPA  , XJPA  ,
     &              WPA    ,
     &              NGLEV  , BWNI   , NLEVM , ILEVM ,
     &              IGA    , CGSTRGA, IGSA  , IGLA  , XGJA  ,
     &              WGA    ,
     &              LRAUG  , NPF    , INDF  , RAUG  ,
     &              IMETI  , LSYSM  , NSYSM , LREPM , NREPM , 
     &              LIONIS , LRION  , RION  ,
     &              LEXCIT , LREXC  , REXC  ,
     &              NPIS   , IPRTI  , ISYSI , ISPSYI,
     &              IMETF  , NVALS  , NREPI , IREP  ,
     &              AUGN   , LEXCN  , EXCN   ,
     &              NTE    , TEA
     &            )
C
C-----------------------------------------------------------------------
C     TELL IDL THAT WE HAVE COMPLETED ONE STAGE FOR PROGRESS WINDOW
C-----------------------------------------------------------------------
C
        WRITE(PIPEOU,*)ONE
        CALL XXFLSH(PIPEOU)
C     
C-----------------------------------------------------------------------
C     OPEN IUNT14 FOR THE SUPPLEMENTED SPECIFIC ION FILE OUTPUT
C-----------------------------------------------------------------------
C
      IF (OPEN14) CLOSE(14)
      OPEN(UNIT=IUNT14, FILE = DSNSPO, STATUS = 'UNKNOWN')
      OPEN14=.TRUE.
C-----------------------------------------------------------------------
C     OUTPUT DATA TO FILE AND PRINTER
C-----------------------------------------------------------------------
      CALL  BDWR14( IUNT14 , NDLEV  , NDJLEV , NDPRT  ,
     &              NDPRTI , NDMET  , NDT    , NVMAX  , NDTRN ,
     &              USERID , DATE   ,
     &              TITLED , IZ     , IZ0    , IZ1    , BWNO  ,
     &              NPL    , BWNOA  , LBSETA , PRTWTA , CPRTA ,
     &              NTRM   , NPTRM  , NLVL   , NPLVL  ,
     &              ISTRM  , IGTRM  , ISPTRM , IGPTRM ,
     &              ISLVL  , FSLVL  , JTREF  ,
     &              ISPLVL , FSPLVL , JTPREF ,
     &              SEQSYM , IGZ    , IGZ0   , IGZ1   ,
     &              NPRF   , NPRFM  , IPRFM  , NPRI   , IPRI   ,
     &              IPA    , CSTRPA , ISPA   , ILPA  , XJPA  ,
     &              WPA    , NGLEV  , BWNI   , NLEVM  , ILEVM  ,
     &              WGA    , NTE    , TEA    , LRION  , RION   ,
     &              LSJ    , IL     , 
     &              IA     , CSTRGA , ISA    , ILA   , XJA   ,
     &              WA     , CPLA   , NPLA   , IPLA   , ZPLA  ,
     &              NV     , SCEF   , ITRAN  , 
     &              TCODE  , I1A    , I2A   , AVAL  , SCOM   ,     
     &              DSNSP  , DSNBD  , DSNXR , IERROR  )
C
      CLOSE(UNIT=IUNT14)
      OPEN14=.FALSE.
C     
      GO TO 500
      
 9999 CONTINUE
      STOP
C
C-----------------------------------------------------------------------
C
 1009  FORMAT(/ /,'FILE DOES NOT EXIST: ',1A80)
      END
