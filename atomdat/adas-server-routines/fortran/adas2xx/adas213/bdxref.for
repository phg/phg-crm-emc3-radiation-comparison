CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas213/bdxref.for,v 1.1 2004/07/06 11:38:03 whitefor Exp $ Date $Date: 2004/07/06 11:38:03 $
CX
      SUBROUTINE  BDXREF( IUNIT  , NDLEV  , NDJLEV , NDPRT  ,
     &                    DSNSP  , DSNBD  , DSNSPO ,  
     &                    NTRM   , NPTRM  , NLVL   , NPLVL  ,
     &                    ISTRM  , IGTRM  , ISPTRM , IGPTRM ,
     &                    ISLVL  , FSLVL  , JTREF  ,
     &                    ISPLVL , FSPLVL , JTPREF ,
     &                    LSJ    , IERROR
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BDXREF *********************
C
C  PURPOSE: TO FETCH DATA FROM INPUT ADF18/A23_A04 CROSS-REFERENCE FILE.
C
C  CALLING PROGRAM: ADAS213
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4) IUNIT       = UNIT TO WHICH INPUT FILE IS ALLOCATED
C
C  INPUT : (I*4) NDLEV       = MAX. NO. OF TERMS THAT CAN BE READ
C  INPUT : (I*4) NDJLEV      = MAX. NO. OF LEVELS THAT CAN BE READ
C  INPUT : (I*4) NDPRT       = MAX. NO. OF PARENT STATES
C 
C  OUTPUT: (C*80) DSNSP      = INPUT ADF04 FILE NAME 
C  OUTPUT: (C*80) DSNBD      = ADF23 IONISATION DATA FILE NAME
C  OUTPUT: (C*80) DSNSP      = OUTPUT ADF04 FILE NAME
C 
C  OUTPUT: (I*4) NTRM        = NUMBER OF TERMS IN X-REF FILE.
C  OUTPUT: (I*4) NPTRM       = NUMBER OF PARENT TERMS IN X-REF FILE.
C  OUTPUT: (I*4) NLVL        = NUMBER OF LEVELS IN X-REF FILE.
C  OUTPUT: (I*4) NPLVL       = NUMBER OF PARENT LEVELS IN X-REF FILE.
C
C  OUTPUT: (I*4) ISTRM()     = SPEC. ION FILE TERM INDEX FROM X-REF
C                              1ST.DIM.: TERM COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) IGTRM()     = IONIS. FILE TERM INDEX FROM X-REF
C                              1ST.DIM.: TERM COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) ISPTRM()    = SPEC. ION FILE PRNT. TERM INDEX FROM X-REF
C                              1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) IGPTRM()    = IONIS. FILE PRNT. TERM INDEX FROM X-REF
C                              1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) ISLVL()     = SPEC. ION FILE LEVEL INDEX FROM X-REF
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) JTREF()     = SP. ION FILE TERM ASSOCIATED WITH LEVEL
C                              FROM X-REF FILE.
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) ISPLVL()    = SPEC. ION FILE PRNT. LEVEL INDEX FROM X-REF
C                              1ST.DIM.: PRNT. LEVEL COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) JTPREF()    = SP. ION FILE PRNT. TERM ASSOCIATED WITH 
C                              PRNT. LEVEL FROM X-REF FILE.
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) ISPTRM()    = SPEC. ION FILE PRNT. TERM INDEX FROM X-REF
C                              1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C  OUTPUT: (I*4) IGPTRM()    = IONIS. FILE PRNT. TERM INDEX FROM X-REF
C                              1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE
C
C  OUTPUT: (R*8) FSLVL()     = FRACTIONATION OF TERM RATES AMONG LEVELS
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C  OUTPUT: (R*8) FSPLVL()    = FRACTIONATION OF PRNT. TERM RATES AMONG 
C                              PRNT. LEVELS
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C  OUTPUT: (L*4) LSJ         = .TRUE. => J-RESOL. INFO. IN X-REF FILE
C                              .FALSE.=> NO J-RESOL. IN X-REF FILE
C  OUTPUT: (I*4) IERROR      = 0 => X-REF FILE OK
C                              1 => FAULT IN XREF FILE DATASETS
C                              2 => FAULT IN XREF FILE TERM COUNT
C                              3 => FAULT IN XREF FILE LEVEL COUNT
C
C          (C*18) C18     = GENERAL CHARACTER STRING
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXFLNM     ADAS      CONVERT SHORT FILE NAME TO FULL NAME
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C  DATE:    02/04/98
C
C  UPDATE:
C
C  VERSION: 1.1                                         DATE: 23-06-98
C  MODIFIED: HUGH SUMMERS
C               FIRST VERSION.        
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   NDLEV    , NDJLEV  , NDPRT
      INTEGER   IUNIT    , I4UNIT
      INTEGER   NTRM     , NPTRM   , NLVL    , NPLVL
      INTEGER   IERROR   , I   
C-----------------------------------------------------------------------
      CHARACTER CSTRM*3    , CGTRM*3    , CSPTRM*3    , CGPTRM*3
      CHARACTER DSNBD*80   , DSNSP*80   , DSNSPO*80   , DSN*80  
      CHARACTER STRING*128
C-----------------------------------------------------------------------
      LOGICAL   LSJ        , LEXIST
C-----------------------------------------------------------------------
      INTEGER   ISTRM(NDLEV)   , IGTRM(NDLEV)  , 
     &          ISPTRM(NDPRT)  , IGPTRM(NDPRT) ,
     &          ISLVL(NDJLEV)  , JTREF(NDJLEV) ,
     &          ISPLVL(NDPRT)  , JTPREF(NDPRT) 
C-----------------------------------------------------------------------
      REAL*8    FSLVL(NDJLEV)  , FSPLVL(NDPRT)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  OBTAIN ALL FILE NAMES AND DATA FROM CROSS-REFERENCE FILE.
C-----------------------------------------------------------------------
C
      IERROR=0
C
      READ(IUNIT,1002)DSN
      CALL XXFLNM(DSN, DSNSP, LEXIST)
      IF (.NOT.LEXIST) THEN
         WRITE(I4UNIT(-1),1009) DSNSP
         IERROR=1
      ENDIF
C     
      READ(IUNIT,1003) DSN
      CALL XXFLNM(DSN, DSNBD, LEXIST)
      IF (.NOT.LEXIST) THEN
         WRITE(I4UNIT(-1),1009) DSNBD
         IERROR=1
      ENDIF
C     
      READ(IUNIT,1004) DSN
      CALL XXFLNM(DSN, DSNSPO, LEXIST)
C
C-----------------------------------------------------------------------
C  OBTAIN TERM AND PARENT TERM CROSS-REFERENCING.
C-----------------------------------------------------------------------
C
      READ(IUNIT,'(1A80)')(STRING,I=1,5)
      LSJ=.FALSE.
C     
      NTRM=0
 20   READ(IUNIT,1005,END=9999) STRING
      IF(STRING(1:7).NE.'       ') THEN
         NTRM = NTRM + 1
         READ(STRING,1006)CSTRM, CGTRM
         IF(CSTRM.EQ.'  *') THEN
             ISTRM(NTRM) = -1
         ELSE
             READ(CSTRM,'(I3)')ISTRM(NTRM)
         ENDIF
         IF(CGTRM.EQ.'  *') THEN
             IGTRM(NTRM) = -1
         ELSE
             READ(CGTRM,'(I3)')IGTRM(NTRM)
         ENDIF
         GO TO 20
      ENDIF
C
      IF(NTRM.LE.0)THEN
          IERROR=2
          GO TO 9999
      ENDIF
C
      READ(IUNIT,'(1A80)')(STRING,I=1,2)
C     
      NPTRM=0
 21   READ(IUNIT,1005,END=9999) STRING
      IF(STRING(1:7).NE.'       ') THEN
         NPTRM = NPTRM + 1
         READ(STRING,1006)CSPTRM, CGPTRM
         IF(CSPTRM.EQ.'  *') THEN
             ISPTRM(NPTRM) = -1
         ELSE
             READ(CSPTRM,'(I3)')ISPTRM(NPTRM)
         ENDIF
         IF(CGPTRM.EQ.'  *') THEN
             IGPTRM(NPTRM) = -1
         ELSE
             READ(CGPTRM,'(I3)')IGPTRM(NPTRM)
         ENDIF
         GO TO 21
      ENDIF
C
      IF(NPTRM.LE.0)THEN
          IERROR=2
          GO TO 9999
      ENDIF
C     
C-----------------------------------------------------------------------
C     SEARCH TO CHECK WHETHER J-RESOLVED SPLIT REQUIRED
C     IF SO FETCH CROSS-REFERENCING AND WEIGHTING FACTORS FOR THE LEVELS
C     AND SPECIFIC ION FILE TERM WITH WHICH IT IS ASSOCIATED.
C-----------------------------------------------------------------------
C     
 22   READ(IUNIT,1005,END=9999)STRING
      IF(STRING(1:10).EQ.'J-resolved') THEN
C
         READ(IUNIT,1005)(STRING,I=1,4)
         LSJ = .TRUE.
C
         NLVL=0
 23      READ(IUNIT,1005,END=9999)STRING
         IF(STRING(1:10).NE.'          ') THEN
            NLVL=NLVL+1
            READ(STRING,1022)ISLVL(NLVL),FSLVL(NLVL),
     &           JTREF(NLVL)
            GO TO 23
         ENDIF
C
         READ(IUNIT,1005)(STRING,I=1,2)
C
         NPLVL=0
 24      READ(IUNIT,1005,END=9999)STRING
         IF(STRING(1:10).NE.'          ') THEN
            NPLVL=NPLVL+1
            READ(STRING,1022)ISPLVL(NPLVL),FSPLVL(NPLVL),
     &           JTPREF(NPLVL)
            GO TO 24
         ENDIF
         GO TO 29
      ELSE
         GO TO 22
      ENDIF
 29   CONTINUE
C
      IF((NLVL.LE.0).OR.(NPLVL.LE.0))THEN
          IERROR=3
          GO TO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C
 9999 RETURN
C
 1002 FORMAT(/,/,1A80,/,/,/)
 1003 FORMAT(1A80)
 1004 FORMAT(/,/,/,1A80)
 1005 FORMAT(1A128)
 1006 FORMAT(4X,A3,9X,A3)
 1009 FORMAT(/ /,'FILE DOES NOT EXIST: ',1A80)
 1022 FORMAT(I7,7X,F10.5,I9)
      END
