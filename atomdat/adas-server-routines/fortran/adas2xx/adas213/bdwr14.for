CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas213/bdwr14.for,v 1.1 2004/07/06 11:38:00 whitefor Exp $ Date $Date: 2004/07/06 11:38:00 $
CX
      SUBROUTINE  BDWR14( IUNIT  , NDLEV  , NDJLEV , NDPRT  ,
     &                    NDPRTI , NDMET  , NDT    , NVMAX  , NDTRN ,
     &                    USERID , DATE   ,
     &                    TITLED , IZ     , IZ0    , IZ1    , BWNO  ,
     &                    NPL    , BWNOA  , LBSETA , PRTWTA , CPRTA ,
     &                    NTRM   , NPTRM  , NLVL   , NPLVL  ,
     &                    ISTRM  , IGTRM  , ISPTRM , IGPTRM ,
     &                    ISLVL  , FSLVL  , JTREF  ,
     &                    ISPLVL , FSPLVL , JTPREF ,
     &                    SEQSYM , IGZ    , IGZ0   , IGZ1   ,
     &                    NPRF   , NPRFM  , IPRFM  , NPRI   , IPRI   ,
     &                    IPA    , CSTRPA , ISPA   , ILPA   , XJPA  ,
     &                    WPA    , NGLEV  , BWNI   , NLEVM  , ILEVM  ,
     &                    WGA    , NTE    , TEA    , LRION  , RION   ,
     &                    LSJ    , IL     , 
     &                    IA     , CSTRGA , ISA    , ILA    , XJA   ,
     &                    WA     , CPLA   , NPLA   , IPLA   , ZPLA   ,
     &                    NV     , SCEF   , ITRAN  , 
     &                    TCODE  , I1A    , I2A    , AVAL   , SCOM  ,     
     &                    DSNSP  , DSNBD  , DSNXR  , IERROR )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BDWR14 *********************
C
C  PURPOSE: TO INTERPOLATE IONISATION RATE DATA FROM ADF23 FILE
C           AND WRITE SUPPLEMENTED ADF04 FILE.
C
C  CALLING PROGRAM: ADAS213
C
C  SUBROUTINE:
C
C  INPUT : (I*4) IUNIT       = UNIT TO WHICH INPUT FILE IS ALLOCATED
C
C  INPUT : (I*4) NDLEV       = MAX. NO. OF TERMS THAT CAN BE READ
C  INPUT : (I*4) NDJLEV      = MAX. NO. OF LEVELS THAT CAN BE READ
C  INPUT : (I*4) NDPRT       = MAX. NO. OF PARENT STATES
C  INPUT : (I*4) NDPRTI      = MAX. NO. OF INTERMEDIATE PARENT STATES
C  INPUT : (I*4) NDMET       = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4) NDT         = MAX. NUMBER OF ELECTRON TEMPS IN ADF23 FILE
C  INPUT : (I*4) NVMAX       = MAX. NUMBER OF ELECTRON TEMPS IN ADF04 FILE 
C  INPUT : (I*4) NDTRN       = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C  INPUT : (C*10)USERID      = USER IDENTIFIER OF CODE EXECUTOR.
C  INPUT : (C*8) DATE        = DATE (AS DD/MM/YY).
C  INPUT : (C*3) TITLED      = ELEMENT SYMBOL.
C  INPUT : (I*4) IZ          =  RECOMBINED ION CHARGE READ
C  INPUT : (I*4) IZ0         =         NUCLEAR CHARGE READ
C  INPUT : (I*4) IZ1         = RECOMBINING ION CHARGE READ
C                              (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (R*8) BWNO        = IONISATION POTENTIAL (CM-1) OF LOWEST PARENT
C  INPUT : (I*4) NPL         = NUMBER OF PARENTS ON FIRST LINE AND USED
C                              IN LEVEL ASSIGNMENTS
C  INPUT : (R*8) BWNOA()     = IONISATION POTENTIAL (CM-1) OF PARENTS
C  INPUT : (L*4) LBSETA()    = .TRUE.  - PARENT WEIGHT SET FOR BWNOA()
C                              .FALSE. - PARENT WEIGHT NOT SET FOR BWNOA()
C  INPUT : (R*8) PRTWTA()    = PARENT WEIGHT FOR BWNOA()
C  INPUT : (C*9) CPRTA()     = PARENT NAME IN BRACKETS
C
C  INPUT : (I*4) NTRM        = NUMBER OF TERMS IN X-REF FILE.
C  INPUT : (I*4) NPTRM       = NUMBER OF PARENT TERMS IN X-REF FILE.
C  INPUT : (I*4) NLVL        = NUMBER OF LEVELS IN X-REF FILE.
C  INPUT : (I*4) NPLVL       = NUMBER OF PARENT LEVELS IN X-REF FILE.
C
C  INPUT : (I*4) ISTRM()     = SPEC. ION FILE TERM INDEX FROM X-REF
C                              1ST.DIM.: TERM COUNTER IN X-REF FILE 
C  INPUT : (I*4) IGTRM()     = IONIS. FILE TERM INDEX FROM X-REF
C                              1ST.DIM.: TERM COUNTER IN X-REF FILE 
C  INPUT : (I*4) ISPTRM()    = SPEC. ION FILE PRNT. TERM INDEX FROM X-REF
C                              1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C  INPUT : (I*4) IGPTRM()    = IONIS. FILE PRNT. TERM INDEX FROM X-REF
C                              1ST.DIM.: PRNT. TERM COUNTER IN X-REF FILE 
C  INPUT : (I*4) ISLVL()     = SPEC. ION FILE LEVEL INDEX FROM X-REF
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C  INPUT : (R*8) FSLVL()     = FRACTIONATION OF TERM RATES AMONG LEVELS
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C  INPUT : (I*4) JTREF()     = SP. ION FILE TERM ASSOCIATED WITH LEVEL
C                              FROM X-REF FILE.
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE 
C  INPUT : (I*4) ISPLVL()    = SPEC. ION FILE PRNT. LEVEL INDEX FROM X-REF
C                              1ST.DIM.: PRNT. LEVEL COUNTER IN X-REF FILE 
C  INPUT : (R*8) FSPLVL()    = FRACTIONATION OF PRNT. TERM RATES AMONG 
C                              PRNT. LEVELS
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C  INPUT : (I*4) JTPREF()    = SP. ION FILE PRNT. TERM ASSOCIATED WITH 
C                              PRNT. LEVEL FROM X-REF FILE.
C                              1ST.DIM.: LEVEL COUNTER IN X-REF FILE
C  INPUT : (C*2)  SEQSYM     = RECOMBINED ION SEQ
C  INPUT : (I*4)  IGZ        = RECOMBINED ION CHARGE FROM ADF23 FILE
C  INPUT : (I*4)  IGZ0       = NUCLEAR CHARGE FROM ADF23 FILE
C  INPUT : (I*4)  IGZ1       = RECOMBINING ION CHARGE FROM ADF23 FILE
C  INPUT : (I*4)  NPRF       = NUMBER OF FINAL PARENTS
C  INPUT : (I*4)  NPRFM      = NUMBER OF FINAL PARENTS WHICH ARE METASTABLES
C  INPUT : (I*4)  IPRFM()    = CROSS-REFERENCING OF FINAL METASTABLE
C                              PARENTS TO FINAL PARENT LIST.
C  INPUT : (I*4)  NPRI       = NUMBER OF FINAL PARENTS WHICH ARE INTERMEDIATE
C                              PARENTS FOR REPR. N-SHELL DOUBLY EXCITED STATES
C  INPUT : (I*4)  IPRI()     = CROSS-REFERENCING OF INTERMEDIATE
C                              PARENTS TO FINAL PARENT LIST.
C  INPUT : (I*4)  IPA()      = INDEX OF FINAL PARENT ENERGY LEVELS
C  INPUT : (C*18) CSTRPA()   = NOMENCL./CONFIG. FOR PARENT LEVEL 'IPA()'
C  INPUT : (I*4)  ISPA()     = MULTIPLICITY FOR PARENT LEVEL 'IPA()'
C                              NOTE: (ISPA-1)/2 = QUANTUM NUMBER (SP)
C  INPUT : (I*4)  ILPA()     = QUANTUM NUMBER (LP) FOR PARENT LEVEL 'IPA()'
C  INPUT : (R*8)  XJPA()     = QUANTUM NUMBER (JP) FOR PARENT LEVEL 'IPA()'
C                              NOTE: (2*XJPA)+1 = STATISTICAL WEIGHT
C  INPUT : (R*8)  WPA()      = ENERGY RELATIVE TO PARENT LEVEL 1 (CM-1)
C                              FOR PARENT LEVEL 'IPA()'
C  INPUT : (I*4)  NGLEV      = NUMBER OF ENERGY LEVELS (TERMS) OF THE
C                              IONISING ION FROM ADF23 FILE
C  INPUT : (R*8)  BWNI       = IONISATION POTENTIAL (CM-1) OF LOWEST LEVEL
C                              OF IONISING ION
C  INPUT : (I*4)  NLEVM      = NUMBER OF IONISING ION LEVELS WHICH ARE
C                              METASTABLES
C  INPUT : (I*4)  ILEVM()    = CROSS-REFERENCING OF IONISNG ION METASTABLES
C                              TO IONISING ION LEVEL LIST.
C  INPUT : (R*8)  WGA()      = ENERGY RELATIVE TO RECOMBINED LEVEL 1 (CM-1)
C                              FOR RECOMBINED LEVEL 'IA()' FROM ADF23 FILE
C  INPUT : (I*4)  NTE        = NUMBER OF ELECTRON TEMPS. FROM ADF23 FILE
C  INPUT : (R*8)  TEA()      = ELECTRON TEMPERATURES (K) FROM ADF23 FILE
C  INPUT : (L*4)  LRION(,)   = .TRUE.  => DATA PRESENT FOR FINAL STATE
C                              .FALSE. => DATA NOT PRESENT FOR FINAL STATE
C                              1ST.DIM: IONISING ION METASTABLE INDEX
C                              2ND.DIM: FINAL PARENT INDEX
C  INPUT : (R*8)  RION(,,)   = STATE SELECTIVE DIRECT IONISATION COEFFICIENTS
C                              1ST.DIM: IONISING ION METASTABLE INDEX
C                              2ND.DIM: FINAL PARENT INDEX
C                              3RD.DIM: ELECTRON TEMPERATURE INDEX 
C  INPUT : (L*4)  LSJ        = .TRUE. => J-RESOL. INFO. IN X-REF FILE
C                              .FALSE.=> NO J-RESOL. IN X-REF FILE
C  INPUT : (I*4)  IL         = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)  IA()       = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()   = NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()      = MULTIPLICITY FOR LEVEL 'IA()'
C                              NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()      = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()      = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                              NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (R*8)  WA()       = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                              'IA()'
C  INPUT : (C*1)  CPLA()     = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IA()'
C                              INTEGER - PARENT IN BWNOA() LIST
C                              'BLANK' - PARENT BWNOA(1)
C                                'X'   - DO NOT ASSIGN A PARENT
C                              1ST DIM.: LEVEL INDEX 
C  INPUT : (I*4)  NPLA()     = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                              OF LEVEL
C                              1ST DIM.: PARENT INDEX
C  INPUT : (I*4)  IPLA(,)    = PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                              OF LEVEL
C                              1ST DIM.: PARENT INDEX
C                              2ND DIM.: LEVEL INDEX
C  INPUT : (R*8)  ZPLA(,     = EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                              OF LEVEL
C                              1ST DIM.: PARENT INDEX
C  INPUT : (C*92) CIONP      = STRING CONTAINING LEVEL TERMINATOR AND
C                              IONISATION POTENTIALS
C
C  INPUT : (I*4)  NV         = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                              PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  SCEF()     = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C                              (INITIALLY JUST THE MANTISSA. SEE 'ITPOW()')
C                              (NOTE: TE=TP=TH IS ASSUMED)
C
C  INPUT : (I*4)  ITRAN      = INPUT DATA FILE: NUMBER OF TRANSITIONS
C  INPUT : (C*1)  TCODE()    = TRANSITION: DATA TYPE POINTER:
C                              ' ' => Electron Impact   Transition
C                              'P' => Proton   Impact   Transition
C                              'H' => Charge   Exchange Recombination
C                              'R' => Free     Electron Recombination
C  INPUT : (I*4)  I1A()      = TRANSITION:
C                               LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                               SIGNED PARENT      INDEX (CASE 'H' & 'R')
C  INPUT : (I*4)  I2A()      = TRANSITION:
C                               UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                               CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C  INPUT : (R*8)  AVAL()     = TRANSITION:
C                               A-VALUE (SEC-1)          (CASE ' ')
C                               NEUTRAL BEAM ENERGY      (CASE 'H')
C                               NOT USED                 (CASE 'P' & 'R')
C  INPUT : (R*8)  SCOM(,)    = TRANSITION:
C                               GAMMA VALUES             (CASE ' ' & 'P')
C                               RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                              1ST DIMENSION - TEMPERATURE 'SCEF()'
C                              2ND DIMENSION - TRANSITION NUMBER
C  INPUT : (C*80) DSNSP      = INPUT ADF04 FILE NAME 
C  INPUT : (C*80) DSNBD      = ADF23 IONISATION DATA FILE NAME
C  INPUT : (C*80) DSNXR      = ADF18 CROSS-REFERENCE FILE NAME 
C  OUTPUT: (I*4)  IERROR     = 0 => X-REF FILE OK
C                              1 => FAULT IN XREF FILE DATASETS
C                              2 => FAULT IN XREF FILE TERM COUNT
C                              3 => FAULT IN XREF FILE LEVEL COUNT
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C  DATE:    03/04/98
C
C  UPDATE: 
C
C  VERSION: 1.1						DATE: 23-06-98
C  MODIFIED: HUGH SUMMERS
C		FIRST VERSION.        
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IUNT12   , ITDIM   , ITTRN
C-----------------------------------------------------------------------
      PARAMETER ( IUNT12 = 12 , ITDIM = 14 , ITTRN = 100 )
C-----------------------------------------------------------------------
      INTEGER   NDLEV    , NDJLEV  , NDPRT   , NDPRTI  , NDMET   ,
     &          NDT      , NVMAX   , NDTRN
      INTEGER   NTRM     , NPTRM   , NLVL    , NPLVL   
      INTEGER   IUNIT     
      INTEGER   IZ       , IZ0     , IZ1     ,
     &          IGZ      , IGZ0    , IGZ1    
      INTEGER   NPL
      INTEGER   NPRF     , NPRFM   , NPRI    ,
     &          NGLEV    , NLEVM   , NTE 
      INTEGER   IL       , ITRAN   , NV                
      INTEGER   IERROR   , I       , IT      , IPOS    , IQS     , J
      INTEGER   IOPT     , IMI     , IPF     , IST     , ISP     , IS
      INTEGER   ICOUNTS  , IC      , ICS     , IT0        
C-----------------------------------------------------------------------
      REAL*8    BWNO     , BWNI    , Z1      , FINTX   , Y
      REAL*8    WSP      , WST     , DELTAE    
C-----------------------------------------------------------------------
      CHARACTER DSNBD*80   , DSNSP*80   , DSNXR*80  
      CHARACTER TITLED*3   , SEQSYM*2
      CHARACTER STRING*128 , STRG1*65   , BLANKS*128
      CHARACTER USERID*10  , DATE*8
      CHARACTER C7*7       , C8*8       , C9*9         , C10*10
C-----------------------------------------------------------------------
      LOGICAL   LSJ        , LCOM       , LSETX        , LACCUM        
C-----------------------------------------------------------------------
      INTEGER   IPRFM(NDPRT)   , ILEVM(NDLEV)  , IPRI(NDPRTI)
      INTEGER   IPA(NDPRT)     , ISPA(NDPRT)   , ILPA(NDPRT)
      INTEGER   ISTRM(NDLEV)   , IGTRM(NDLEV)  , 
     &          ISPTRM(NDPRT)  , IGPTRM(NDPRT) ,
     &          ISLVL(NDJLEV)  , JTREF(NDJLEV) ,
     &          ISPLVL(NDPRT)  , JTPREF(NDPRT) 
      INTEGER   IA(NDLEV)      , ISA(NDLEV)    , ILA(NDLEV) ,
     &          IPLA(NDMET,NDLEV)              , NPLA(NDLEV)
      INTEGER   I1A(NDTRN)     , I2A(NDTRN)    
C-----------------------------------------------------------------------
      REAL*8    BWNOA(NDMET)   , PRTWTA(NDMET) 
      REAL*8    XJPA(NDPRT)    , WPA(NDPRT)    , WGA(NDLEV)
      REAL*8    FSLVL(NDJLEV)  , FSPLVL(NDPRT)
      REAL*8    XJA(NDLEV)     , WA(NDLEV)     ,
     &          ZPLA(NDMET,NDLEV)
      REAL*8    SCEF(NVMAX)
      REAL*8    AVAL(NDTRN)    , SCOM(NVMAX,NDTRN)
      REAL*8    TEA(NDT)       , RION(NDMET,NDPRT,NDT) 
C
      REAL*8    XIN(ITDIM)     , YIN(ITDIM)    , DY(ITDIM)
      REAL*8    XOUT(ITDIM)    , YOUT(ITDIM)   , YOUTS(ITDIM,ITTRN)   
C-----------------------------------------------------------------------
      CHARACTER CPRTA(NDMET)*9
      CHARACTER CSTRPA(NDPRT)*18 
      CHARACTER CSTRGA(NDLEV)*18 
      CHARACTER CPLA(NDLEV)*1   
      CHARACTER TCODE(NDTRN)*1
      CHARACTER C8S(ITTRN)*8   
C-----------------------------------------------------------------------
      LOGICAL   LRION(NDMET,NDPRT) , LBSETA(NDMET)
C-----------------------------------------------------------------------
      EXTERNAL  FINTX
C-----------------------------------------------------------------------
      DATA     BLANKS/' '/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      IERROR = 0
C
      IF (LSJ) THEN
          IERROR = 1
          GO TO 9999
      ENDIF
C
      IF ((IZ0.NE.IGZ0).OR.(IZ.NE.IGZ)) THEN
          IERROR = 2
          GO TO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C  CHECK CONSISTENCY OF PARENTS FOR HEADER LINE
C-----------------------------------------------------------------------
C
      IF (NPL.EQ.NPRFM)THEN
          WRITE(STRING,'(1A3,I2,2I10,5(F12.0,1X,1A4))')TITLED,
     &         IZ,IZ0,IZ1,(BWNOA(I),CPRTA(I)(1:4),I=1,NPL)
          WRITE(IUNIT,'(1A128)') STRING
      ENDIF
C
C-----------------------------------------------------------------------
C  WRITE OUT LEVEL LIST FROM ADF04 FILE
C-----------------------------------------------------------------------
C
      DO I=1,IL
        IF(CPLA(I).EQ.'X') THEN
            WRITE(STRG1,1000)WA(I)
            STRG1(17:19)='{X}'
        ELSE
            WRITE(STRG1,1000)WA(I)
            DO J=1,NPLA(I)
              WRITE(C10,1001)IPLA(J,I),ZPLA(J,I)
              STRG1(7+10*J:16+10*J)=C10
            ENDDO
        ENDIF
        WRITE(C7,1002)XJA(I)
        WRITE(STRING,1003) IA(I), 
     &           CSTRGA(I), ISA(I), ILA(I), C7, STRG1
        WRITE(IUNIT,'(1A128)') STRING
      ENDDO
C
      WRITE(IUNIT,'(I5)') -1    
      
C
       STRING = BLANKS
       Z1 = IZ1
       IQS = 3
       WRITE(STRING,'(F5.2,I5)')Z1,IQS
       IPOS = 0
       DO IT = 1,NV
         WRITE(C9,'(1PD9.2)') SCEF(IT)
         STRING(17+IPOS:21+IPOS) = C9(1:5)
         STRING(22+IPOS:24+IPOS) = C9(7:9)
         IPOS =IPOS+8
       ENDDO
       WRITE(IUNIT,'(1A128)')STRING
C
C-----------------------------------------------------------------------
C  WRITE OUT TRANSITION LIST FROM ADF04 FILE
C-----------------------------------------------------------------------
C
       DO I=1,ITRAN
         STRING=BLANKS
         WRITE(STRING,1004)TCODE(I), I2A(I),I1A(I)
         IF((TCODE(I).EQ.'R').OR.(TCODE(I).EQ.'H')) STRING(7:7)='+'
         IF(TCODE(I).EQ.'I') STRING(7:7)='-'
         IF(AVAL(I).GT.0.0D0) THEN
             WRITE(C9,'(1P,1D9.2)')AVAL(I)
             STRING(9:16)=C9(1:5)//C9(7:9)
         ENDIF
         DO IT=1,NV
           WRITE(C9,'(1P,1D9.2)')SCOM(IT,I)
           STRING(9+8*IT:16+8*IT)=C9(1:5)//C9(7:9)
         ENDDO
         WRITE(IUNIT,'(1A128)')STRING
       ENDDO
C
C-----------------------------------------------------------------------
C  ADD NEW S-LINES FOR STATE SELECTIVE IONISATION
C-----------------------------------------------------------------------
C
       LSETX = .FALSE.
       IOPT  = 4
C
       DO IT=1,NTE
         XIN(IT)= DLOG10(TEA(IT))
       ENDDO
C
       DO IT=1,NV
         XOUT(IT)=DLOG10(SCEF(IT))
       ENDDO
C
       ICOUNTS = 0
       DO 70  IPF=1,NPRF
C
C-- IDENTIFY SPECIFIC ION FILE TARGET PARENT FROM X-REF FILE
C
         DO IS=1,NPTRM
           IF(IGPTRM(IS).EQ.IPF) THEN
               ISP=ISPTRM(IS)
               WSP=WPA(IPF)
               GO TO 20
           ENDIF
         ENDDO
         ISP=-1
C         
   20    DO 50 IMI=1,NLEVM
C
C-- IDENTIFY SPECIFIC ION FILE TARGET TERM FROM X-REF FILE
C
           DO IS=1,NPTRM
             IF(IGTRM(IS).EQ.IMI) THEN
                 IST=ISTRM(IS)
                 WST=WGA(IMI)
                 GO TO 30
             ENDIF
           ENDDO
           IST=-1
C
   30      IF(.NOT.LRION(IMI,IPF))GO TO 50  
C
C-- CHECK IF ISP/IST PAIR ALREADY USED.  IF SO ACCUMULATE
C
           WRITE(C8,'(2I4)')IST,ISP
           LACCUM=.FALSE.
           IF(ICOUNTS.GT.1) THEN
             DO IC=1,ICOUNTS-1
               IF(C8.EQ.C8S(IC)) THEN
                   LACCUM = .TRUE.
                   ICS=IC
                   GO TO 35
               ENDIF 
             ENDDO
           ENDIF
           ICOUNTS=ICOUNTS+1
           ICS=ICOUNTS
           C8S(ICS)=C8
C
   35      DELTAE= BWNI-WST+WSP
C
           IT0=0
           DO IT=1,NTE
C
C--  SCALE THE IONISATION EXPONENTIAL FACTOR BEFORE INTERPOLATION
C
             Y  = RION(IMI,IPF,IT)
             IF(Y.LE.0.0D0)THEN
                 YIN(IT)=-30.0
                 IT0=IT
             ELSE
                 YIN(IT)=DLOG10(Y)+0.624863*DELTAE/TEA(IT)
             ENDIF
           ENDDO
C
           IF(IT0.GT.0)THEN
               DO IT=1,IT0
                 YIN(IT)=YIN(IT0+1)+0.624863*DELTAE*
     &                   (1.0/TEA(IT)-1.0/TEA(IT0+1))
               ENDDO
           ENDIF               
C
           CALL XXSPLN( LSETX , IOPT  , FINTX ,
     &                  NTE   , XIN   , YIN   ,
     &                  NV    , XOUT  , YOUT  ,
     &                  DY
     &                )
           LSETX=.TRUE.
C
           DO IT=1,NV
             YOUT(IT)=YOUT(IT)-0.624863*DELTAE/SCEF(IT)
             IF(YOUT(IT).LE.-30.0) YOUT(IT)=-30.0
             IF(LACCUM) THEN
                 YOUTS(IT,ICS)=YOUTS(IT,ICS)+10.0D0**YOUT(IT)
             ELSE
                 YOUTS(IT,ICS)=10.0D0**YOUT(IT)
             ENDIF
           ENDDO
C                        

 50      CONTINUE
 70    CONTINUE
C
C-- OUTPUT IONISATION LINES TO THE SPECIFIC ION FILE
C   
       DO IC=1,ICOUNTS
         STRING=BLANKS
         STRING(1:8)=C8S(IC)
         STRING(7:7)='+'
         STRING(1:1)='S'
         DO IT=1,NV
           WRITE(C9,'(1P,1D9.2)')YOUTS(IT,IC)
           STRING(9+8*IT:16+8*IT)=C9(1:5)//C9(7:9)
         ENDDO
         WRITE(IUNIT,1005)STRING
       ENDDO
C
C-----------------------------------------------------------------------
C  WRITE OUT COMMENTS FROM ADF04 FILE AND ADD IONISATION COMMENTS
C-----------------------------------------------------------------------
C
      OPEN(UNIT=IUNT12 , FILE = DSNSP, STATUS = 'OLD')
C
      LCOM=.FALSE.
  100 READ(IUNT12,1005,END=200)STRING
      IF(LCOM.AND.((STRING(1:1).EQ.'C').OR.(STRING(1:1).EQ.'c'))) THEN
          WRITE(IUNIT,1005)STRING
      ELSEIF(STRING(1:6).EQ.'C-----') THEN
          LCOM=.TRUE.
          WRITE(IUNIT,1005)STRING
      ENDIF
      GO TO 100
C
  200 CLOSE(UNIT=IUNT12)
C
      WRITE(IUNIT,1007)DSNBD, DSNXR,DSNSP, USERID, DATE
      WRITE(IUNIT,1006)
C
 1000  FORMAT(F15.0)
 1001  FORMAT(2X,'{',I1,'}',F5.3)
 1002  FORMAT('(',F5.1,')')
 1003  FORMAT(I5,1X,1A18,1X,'(',I1,')',I1,A7,1A56)
 1004  FORMAT(1A1,I3,I4)
 1005  FORMAT(1A128)
 1006  FORMAT('C',80('-'))
 1007  FORMAT('C',/,
     &        'C     Ionisation data is post-processed from file',/,
     &        'C',/,'C      ',A,/,'C',/,
     &        'C     The cross-referencing file is',/,
     &        'C',/,'C      ',A,/,'C',/,
     &        'C     The source specific ion file is',/,
     &        'C',/,'C      ',A,/,'C',/,'C',/,
     &        'C  Producer: ',A,/,
     &        'C  Date:     ',A,/,
     &        'C'
     &       )   
C
C-----------------------------------------------------------------------
C
 9999 RETURN
C
      END
      
