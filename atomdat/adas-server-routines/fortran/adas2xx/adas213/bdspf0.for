CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas213/bdspf0.for,v 1.1 2004/07/06 11:37:44 whitefor Exp $ Date $Date: 2004/07/06 11:37:44 $
CX
      SUBROUTINE BDSPF0( REP    , DSFULL)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BDSPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL OR INFO FILE (STANDARD
C	    (INPUT) IF BATCH - INPUT DATA SET SPECIFICATIONS.
C
C  CALLING PROGRAM: ADAS213
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  HP SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    30TH MARCH 1998
C
C VERSION: 1.1                          DATE: 30-03-98
C MODIFIED: HP SUMMERS
C               - FIRST VERSION.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEIN
      PARAMETER ( PIPEIN=5          )
C
      CHARACTER   REP*3         , DSFULL*80
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C READ IN DATA FROM STANDARD INPUT (IDL OR INFO FILE)
C-----------------------------------------------------------------------
C

      READ(PIPEIN,'(A)')REP
      READ(PIPEIN,'(A)')DSFULL

      RETURN
      END

