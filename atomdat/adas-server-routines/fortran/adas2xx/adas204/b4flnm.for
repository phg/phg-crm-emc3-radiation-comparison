CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/b4flnm.for,v 1.4 2007/07/20 09:46:30 allan Exp $ Date $Date: 2007/07/20 09:46:30 $
CX
       SUBROUTINE B4FLNM ( ADAS_C, ADAS_U, DSNIN, DSNFUL, LEXIST)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B4FLNM *********************
C
C  PURPOSE: TO PREPARE A UNIX DATASET NAME FROM A STRING WHICH MAY
C           INCLUDE AN ADAS ENVIRONMENT VARIABLE AND COMMENTS.
C
C           THE ADAS ENVIRONMENT VARIABLE MUST BE FIRST AND IN DOUBLE
C           QUOTES.  THE COMMENTS MUST EITHER FOLLOW A COLON.
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C120) DSNIN    = INPUT STRING FOR INTERROGATION
C  INPUT : (C*80) ADAS_C   = CENTRAL ADAS LOCATION (FROM IDL)
C  INPUT : (C*80) ADAS_U   = USER ADAS LOCATION (FROM IDL)
C
C  OUTPUT: (C120) DSNFUL   = THE FULL EXPANDED FILE NAME WITHOUT
C                            EXTRANEOUS MATERIAL
C  OUTPUT: (L*4)  LEXIST   = .TRUE. => NAME FORMED AND FILE EXISTS
C                            .FALSE.=> FAILED TO FORM NAME OR FIND FILE
C
C          (C*120) DSN1     = WORK STRING
C          (C*120) DSNTEMP  = WORK STRING
C          (C*120) BLANK    = BLANK STRING
C          (I*4)   LEN1     = STRING INDEX
C          (I*4)   LEN2     = STRING INDEX
C          (I*4)   LEN3     = STRING INDEX
C          (I*4)   LEN4     = STRING INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSLEN     ADAS     FIND BEGINNING AND END OF A STRING
C          I4UNIT     ADAS     FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE  :  22/08/96
C
C VERSION: 1.3                                            DATE: 3-12-98
C MODIFIED: M.O'MULLANE
C           - Pass in ADAS environment variables
C
C VERSION: 1.4                                            DATE: 20-7-07
C MODIFIED: Allan Whiteford
C           - Small modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
      INTEGER I4UNIT
      INTEGER LEN1   , LEN2 , LEN3 , LEN4 , len5  , len6
      INTEGER P1     , P2   , p3   , p4
C-----------------------------------------------------------------------
      CHARACTER DSNIN*120  , DSNFUL*120 , DSN1*120,  DSNTEMP*120
      CHARACTER source*8   , BLANK*120
      CHARACTER ADAS_C*80  , ADAS_U*80
C-----------------------------------------------------------------------
      LOGICAL LEXIST
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Reset variables from last run
C-----------------------------------------------------------------------
      lexist = .false.  
      dsn1   = ' '

C-----------------------------------------------------------------------
C REMOVE COMMENTS.
C ASSUME PRECURSOR COMMENT IF TWO COLONS OR IF NO " OR / IN STRING 
C TO LEFT OF COLON.
C-----------------------------------------------------------------------

      P1 = INDEX(DSNIN,":")
      IF(P1.GE.0)THEN
         P2 = INDEX(DSNIN(P1+1:120),":")
         IF(P2.GT.0)THEN
            DSN1=DSNIN(P1+1:P2+P1-1)
         ELSE
            IF (P1.EQ.0) THEN
              P3=INDEX(DSNIN(1:120),'"')
              P4=INDEX(DSNIN(1:120),'/')
            ELSE
              P3=INDEX(DSNIN(1:P1),'"')
              P4=INDEX(DSNIN(1:P1),'/')
            ENDIF
            IF((P3.LE.0).AND.(P4.LE.0))THEN
               DSN1=DSNIN(P1+1:120)
            ELSE
               IF (P1.EQ.0) THEN
                 DSN1=DSNIN(1:)
               ELSE
                 DSN1=DSNIN(1:P1-1)
               ENDIF
            ENDIF
         ENDIF   
      ENDIF

C-----------------------------------------------------------------------
        

      LEN1 = INDEX(dsn1,'"')
      IF (LEN1.GT.0)THEN
         LEN2=INDEX(dsn1(LEN1+1:120),'"')
         IF(LEN2.EQ.0)THEN
            WRITE(I4UNIT(-1),1000)
            RETURN
         ENDIF
         source = dsn1(LEN1+1:LEN2+LEN1-1)
         CALL XXSLEN(source,LEN1,LEN2)
         IF(LEN1.EQ.0)THEN
            WRITE(I4UNIT(-1),1000)dsn1
            RETURN
         ELSE
            CALL XXSLEN(dsn1,LEN3,LEN4)
            LEN3=INDEX(dsn1,'/')
            if (source(5:8).eq.'USER') then
               call xxslen(adas_u,len5,len6)
               dsnful = adas_u(len5:len6)//dsn1(len3:len4)
            else
               call xxslen(adas_c,len5,len6)
               dsnful = adas_c(len5:len6)//dsn1(len3:len4)
            endif
         ENDIF
      ELSE
         CALL XXSLEN(dsn1,LEN3,LEN4)
         DSNFUL=dsn1(LEN3:LEN4)
      ENDIF

      INQUIRE(FILE=DSNFUL,EXIST=LEXIST)
      IF (.NOT.LEXIST) WRITE(I4UNIT(-1),1000)dsnful

C-----------------------------------------------------------------------

      RETURN

 1000 FORMAT(' B4FLNM WARNING: FILE NAME NOT RESOLVED',
     &       ' OR DOES NOT EXIST ',/,' File : ',a)


       end
