CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/b4sumd.for,v 1.1 2004/07/06 11:23:12 whitefor Exp $ Date $Date: 2004/07/06 11:23:12 $
CX
       SUBROUTINE B4SUMD ( NDREP  , NDT     ,
     &                     MAXTM  , IREPMAX , IREP   , DRMF   , DRMS   ,
     &                     EIJN   , PWTEMP   
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C **************** FORTRAN 77 SUBROUTINE: B4SUMD  **********************
C
C  VERSION: 2.0
C
C  PURPOSE: TO SUM BADNELL DIELECTRONIC RATE COEFFICIENT DATA OVER THE
C           REPRESENTATIVE SET TO GIVE ZERO DENSITY TOTAL AND
C           RADIATED POWER FROM SATELLITE LINES
C       
C  CALLING PROGRAM: B4DATD
C
C
C  INPUT:
C  INPUT :  (I*4) NDREP    = MAXIMUM NUMBER OF REPRESENTATIVE LEVELS
C  INPUT :  (I*4) NDT      = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT :  (I*4) DRMF(,)  = BADNELL DIELECTRONIC DATA (CM3 S-1)
C                            1ST DIM.: REPRESENTATIVE LEVEL INDEX
C                            2ND DIM.: TEMPERATURE INDEX
C  INPUT :  (I*4) NBT      = NO. OF TEMPERATURES
C  INPUT :  (I*4) IREPMAX  = NO OF REPRESENTATIVE LEVELS
C  INPUT :  (I*4) IREP()   = SET OF REPRESENTATIVE LEVELS
C  INPUT :  (R*8) EIJN()   = SATELLITE. ENERGY AS A FUNCTION OF 
C                            REPRESENTATIVE LEVEL (K)
C
C  OUTPUT:  (R*8) DRMS()   = SUMMED DR RATE COEFFICIENTS (CM3 S-1)
C                            1ST DIM.: TEMPERATURE INDEX
C  OUTPUT:  (R*8) PWTEMP() = SAT. RADIATED POWER (UNITS ERG S-1 CM3)
C                            1ST DIM.: TEMPERATURE INDEX
C
C           (I*4) NREP     = GENERAL LEVEL INDEX
C           (I*4) IN       = GENERAL INDEX
C           (I*4) IT       = GENERAL INDEX
C           (R*8) V        = GENERAL VARIABLE FOR N-SHELL
C           (R*8) V1       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) Y        = GENERAL VARIABLE FOR N-SHELL
C           (R*8) YP       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) Y0       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) Y1       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) PW       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) PW1      = GENERAL VARIABLE FOR N-SHELL
C
C
C  AUTHOR:   WILLIAM J. DICKSON, JET JOINT UNDERTAKING
C
C  DATE:     14TH DECEMBER 1992
C
C  UPDATE: 15/12/92  WJ DICKSON - REVISED ALGORITHM HAS BETTER 
C                                 AGREEMENT WITH INTERNAL SUM
C                                 CALCULATED BY MAINCL
C
C  UPDATE: 31/01/97  HP SUMMERS - CHANGED NAME TO B4SUMD 
C
C  VERSION: 1.1						DATE: 05-03-98
C  MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C                 
C-------------------------------------------------------------------
       REAL*8      DMIN
C-------------------------------------------------------------------
       PARAMETER ( DMIN = 1.00D-70 )
C-------------------------------------------------------------------
       INTEGER     IN       , IT      , MAXTM   , IBREP     , 
     &             NDREP    , NDT     , IREPMAX , NREP      , 
     &             N0       , N1   
C-------------------------------------------------------------------
       REAL*8      V        , V1      , Y       , YP        ,
     &             Y0       , Y1      ,
     &             PW       , PW1     , YP0     , YP1
C-------------------------------------------------------------------
       INTEGER     IREP(NDREP)
C-------------------------------------------------------------------
       REAL*8      DRMS(NDT)          ,  DRMF(NDREP,NDT)    , 
     &             EIJN(NDREP)        ,  PWTEMP(NDT)
C-------------------------------------------------------------------
C-------------------------------------------------------------------
       DO  90 IT = 1,MAXTM
         DRMS(IT)  = 0.0D+0
         PWTEMP(IT)  = 0.0D+0
   90  CONTINUE
C
c       WRITE(6,1100) NDREP, NDT, MAXTM, IREPMAX
c       DO IBREP = 1,IREPMAX
c          WRITE(6,1102) IREP(IBREP), ( DRMF(IBREP,IT) , IT = 1,MAXTM )
c       ENDDO
C
       DO 100 IT = 1,MAXTM
C
         DRMS(IT)    = DRMF(1,IT)
         PWTEMP(IT)  = DRMF(1,IT) * EIJN(1)
C
c         IF( IT .EQ. 5) WRITE(6,1108)
C
         DO 120 IN = 2,IREPMAX
            N1 =  IREP(IN)
            N0 =  IREP(IN-1)
            V  =  DFLOAT(N0)
            V1 =  DFLOAT(N1)
            Y1 =  DLOG10( DMAX1( DRMF(IN   , IT) , DMIN ) )
            Y0 =  DLOG10( DMAX1( DRMF(IN-1 , IT) , DMIN ) )
C
            PW1  = DRMF(IN ,IT)   *  EIJN(IN)
            PW   = DRMF(IN-1 ,IT) *  EIJN(IN-1)
            YP1  = DLOG10( DMAX1 ( PW1,  DMIN ) )
            YP0  = DLOG10( DMAX1 ( PW ,  DMIN ) )
C
           DO 130 NREP = N0+1, N1
C
              Y =  Y1 - (N1 - NREP)/(N1-N0) * (Y1 - Y0)
              DRMS(IT)  =  DRMS(IT) + 10 ** Y
C
              YP =  YP1 - (N1 - NREP)/(N1-N0) * (YP1 - YP0)
              PWTEMP(IT) =  PWTEMP(IT) + 10 ** YP
C
  130      CONTINUE
C
  120    CONTINUE
         PWTEMP(IT) = PWTEMP(IT) * 1.3804D-16
  100  CONTINUE
C
c       WRITE(6,1104) ( DRMS(IT) , IT = 1,MAXTM )
C
       RETURN
C-------------------------------------------------------------------
 1100  FORMAT(1H ,'NDREP=',I3,3X,'NDT=',I3,3X,'MAXTM=',I3,3X,
     &            'IREPMAX=',I3)
 1102  FORMAT(1H , I5, 1P, 12D10.2)
 1104  FORMAT(1H ,'SUM  ', 1P, 12D10.2)
 1108  FORMAT(1H ,'  IN   N1   N0   DRMS       ALFN    ALFN1      PWTEMP
     &      PW      PW1   ')
 1110  FORMAT(1H ,3I5,1P,6D10.2)
C-------------------------------------------------------------------
       END
