       SUBROUTINE B4DATD ( XRMEMB    , NPMNCL , IMAXX  ,
     &                     NREPX     , MAXTM  , TEM    ,
     &                     NDBFILM   , NBFIL  , NCUTMC ,
     &                     AUGM      , DRM    , DRMSF  ,
     &                     PWSAT     , DSNXRT , OPEN17 ,
     &                     dsnin     , adas_c , adas_u
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 SUBROUTINE: B4DATD  ************************
C
C  VERSION:  1.1
C
C  PURPOSE:  PROCESS  DIELECTRONIC DATA FILES TO PREPARE
C            DIELECTRONIC AND AUGER DATA FOR ADAS204
C
C            THE DR FILE LAYOUT IS SPECIFIED BY THE ADF09 FORMAT
C
C  DATA:     THE SOURCE DATA IS ACCESSED THROUGH A CROSS-REFERENCE FILE
C                 /../adas/adf18/a09_p204/<ion>n.dat
C            WHERE <ION> DENOTES THE RECOMBINED ION (EG. C4)
C
C            THE PARENT CROSS-REFERENCING IS BASED ON THE ADAS204
C            DRIVING INPUT DATA FILE SPECIFIED BY THE ADF25 FORMAT
C                 /../adas/adf25/bns<yr>#<seq>/bns<yr>#<seq>_<code>.dat
C            WHERE <yr>   IS A TWO DIGIT YEAR NUMBER
C                  <seq>  IS THE ISO=ELECTRONIC SEQUENCE SYMBOL
C                  <code> IS AN ION CODE (eg. c4) OR ELEMENT CODE
C                         (EG. c ) IF A NUMBER OF IONS OF THE
C                         ISO-ELECTRONIC SEQUENCE ARE STACKED
C                         SEQUENTIALLY.
C
C            THE FILE NAMES ARE ANALYSED BY ADAS204 AND WARNINGS ISSUED
C            IF APPROPRIATE.  THESE WARNINGS ARE NOT NECESSARILY FATAL.
C            FOR EXAMPLE, THE ADF18 FILE CONTAINS THE NAME OF ITS
C            EXPECTED DRIVING ADF25 FILE.  THESE DIFFER IF THE ADF25
C            FILE IS DRIVING A COMPLETE ISO-ELECTRONIC SEQUENCE CALC.
C            RATHER THAN JUST A SINGLE ION CASE.
C
C
C  INPUT:  (C*8)  XRMEMB  = CROSS-REFERENCE PARTITIONED DATA SET MEMBER
C          (I*4)  IMAXX   = NUMBER OF REPRESENTATIVE LEVELS IN THE
C                               EXTENDED SET REQUIRED FOR THE MAIN CODE
C          (I*4)  NREPX() = REPRESENTATIVE N-SHELLS FOR THE MAIN CODE
C          (I*4)  NPMNCL  = NUMBER OF PARENTS INCLUDED IN THE MAIN CODE
C                               ( GIVEN BY THE <INMEMB> FILE )
C          (I*4)  MAXTM   = NUMBER OF TEMPERATURES USED IN MAIN CODE
C          (R*8)  TEM()   = TEMPERATURES (K) USED IN THE MAIN CODE
C          (I*4)  NDBFILM = PARAMETER = MAXIMUM NUMBER OF DR FILES
C                                       MUST BE GREATER THAN NDBFIL
C          (C*120)DSNXRT  = FIRST PART OF CROSS REFERENCE FILE NAME
C            (L)  OPEN17  = .FALSE. -OUTPUT TO UNIT=17 SWITCHED OFF.
C
C  OUTPUT: (I*4)  NCUTMC(,) = N-SHELL CUT FOR AUGER RATES (AUGER CHANNEL
C                             OPENS AT NCUTMC+1)
C                                1ST. INDEX = INITIAL PARENT
C                                2ND. INDEX = FINAL PARENT
C          (R*8)  AUGM(,,,) = AUGER RATES (SEC-1)
C                                1ST INDEX =  REPRESENTATIVE LEVEL
C                                2ND INDEX =  INITIAL PARENT
C                                3RD INDEX =  INITIAL SPIN SYSTEM
C                                4TH INDEX =  FINAL PARENT
C          (R*8)  DRM(,,,,) = DIELECTRONIC RATE COEFFTS. (CM3 SEC-1)
C                                1ST INDEX =  REPRESENTATIVE LEVEL
C                                2ND INDEX =  TEMPERATURE
C                                3RD INDEX =  INITIAL PARENT
C                                4TH INDEX =  INITIAL SPIN SYSTEM
C                                5TH INDEX =  FINAL PARENT
C          (I*4)  NBFIL     = NUMBER OF DR FILES
C
C  PROGRAM:(I*4)  NDREP   = PARAMETER = MAXIMUM NUMBER OF
C                                       REPRESENTATIVE LEVELS
C          (I*4)  NDPRT   = PARAMETER = MAXIMUM NUMBER OF PARENTS
C          (I*4)  NDSYS   = PARAMETER = MAXIMUM NUMBER OF SPIN SYSTEMS
C          (I*4)  NDT     = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C          (I*4)  NDBFIL  = PARAMETER = MAXIMUM NUMBER OF DR FILES
C          (I*4)  NDPAIR  = PARAMETER = MAXIMUM NUMBER OF AUGER RATE
C                                       PARENT PAIRS
C          (I*4)  NDREP   = PARAMETER = MAXIMUM NUMBER OF MAIN CODE
C                                       REPRESENTATIVE LEVELS
C          (I*4)  NDBREP  = PARAMETER = MAXIMUM NUMBER OF DR
C                                       REPRESENTATIVE LEVELS
C
C          (C*1)  CHARS1  = ONE CHARACTER
C          (C*4)  CHARS4  = FOUR CHARACTERS
C          (C*120)DSNBD() = DR DIELECTRONIC DATA FILE MEMBER NAMES
C          (C*30) BPDS    = DR PARENT STATE DESCRIPTOR
C          (C*30) BPDSC() = DR PARENT STATE DESCRIPTOR ARRAY
C          (C*120)DSNMC   = MAINCL CODE INPUT FILE MEMBER NAME
C          (C*120)DSNMCO  = MAINCL CODE OUTPUT FILE MEMBER NAME
C          (C*120)DSN     = CHARACTER FILE NAME WORKSPACE
C          (C*120)DSHORT  = CURRENT FILE NAME WITH SYMBOLIC NAMES
C          (C*8)  MEMBER  = FILE MEMBER NAME WORKSPACE
C          (C*80) STRING  = LINE OUT STRING
C          (C*89) LSTRGO  = LONG LINE OUT STRING
C          (L*4)  OPEN12  = 'TRUE' IF OPEN
C          (L*4)  OPEN13  = 'TRUE' IF OPEN
C          (L*4)  OPEN14  = 'TRUE' IF OPEN
C          (L*4)  LEXIST  = 'TRUE' IF FILE EXISTS
C          (L*4)  LSJ     = 'TRUE' IF FILE EXISTS
C          (L*4)  LSETX   = 'TRUE' IF SPLINE UNINITIATED
C
C          (I*4)  I         = RUNNING INDEX
C          (I*4)  IBDPA()   = PARENT INDEX IN THE COMPLETE DR LIST
C          (I*4)  IBFIL     = RUNNING INDEX FOR DR FILES
C          (I*4)  IBREP     = RUNNING REPRESENTATIVE SHELL INDEX
C          (I*4)  IBMAX()   = NUMBER OF DR REPRESENTATIVE LEVELS
C                                 1ST. INDEX = DR FILE INDEX
C          (I*4)  IBPR      = CURRENT PARENT READ FROM DR FILE
C          (I*4)  IBPRIA(,) = INITIAL PARENT INDEX FROM LIST FOR A FILE
C                                 1ST. INDEX = LEVEL INDEX
C                                 2ND. INDEX = DR FILE INDEX
C          (I*4)  IBPRFA(,) = FINAL   PARENT INDEX FROM LIST FOR A FILE
C                                 1ST. INDEX = LEVEL INDEX
C                                 2ND. INDEX = DR FILE INDEX
C          (I*4)  IBREP     = RUNNING INDEX FOR REPRESENTATIVE LEVELS
C          (I*4)  IC        = COUNTER OF N-SHELLS BELOW AUGER CUT
C          (I*4)  IF        = RUNNING INDEX ON TOTAL PARENT LIST
C          (I*4)  II        = RUNNING INDEX ON TOTAL PARENT LIST
C          (I*4)  IMNPA()   = PARENT INDEX CORRESPONDING TO MAIN CODE
C          (I*4)  IND       = CHARACTER INDEX POSITION MARKER ON STRING
C          (I*4)  IOPT      =  SPLINE END CONDITION OPTION (SET =-1)
C          (I*4)  IPI       = INITIAL PARENT OF SUPPL. AUGERING STATE
C          (I*4)  IPF       = FINAL PARENT AFTER SUPPL. AUGER
C          (I*4)  ISYSI     = INITIAL SPIN INDX. OF SUPPL.AUGERING STATE
C          (I*4)  IS        = RUNNING INDEX
C          (I*4)  ISREP     = SUPPLEMENTARY REPRESENTATIVE LEVEL INDEX
C          (I*4)  ISUPPLE   = NUMBER OF SUPPLE. AUGER RATES
C          (I*4)  IPAIRS    = RUNNING INDEX FOR AUGER RATE PARENT PAIRS
C          (I*4)  IPRT      = RUNNING INDEX FOR PARENTS
C          (I*4)  IPT       = RUNNING INDEX ON TOTAL PARENT COUNT FROM
C                             DR FILES
C          (I*4)  IR        = UNSPECIFIED LINE COUNTER
C          (I*4)  IREAD     = FLAG FOR READ OPTION
C          (I*4)  IREFI()   = INITIAL PARENT FOR AUGER RATE IN FULL LIST
C          (I*4)  IREFF()   = FINAL PARENT FOR AUGER RATE IN FULL LIST
C          (I*4)  IREP      = MAIN CODE REPRESENTATIVE LEVEL COUNTER
C          (I*4)  IRFF      = POINTER TO FINAL PARENT IN FULL LIST
C          (I*4)  IRFI      = POINTER TO INITIAL PARENT IN FULL LIST
C          (I*4)  IS        = SPIN SYSTEM INDEX
C          (I*4)  ISET(,,)   = FLAG FOR INPUT OF SUPP. AUGER DATA
C                                       ISET = 0  NO SUPP. DATA
C                                       ISET = 1  SUPP. DATA
C                                       1ST INDEX - IPRT
C                                       2ND INDEX - ISYS
C                                       3RD INDEX - JPRT
C          (I*4)  ISPF      = FINAL PARENT SPIN FOR AUGER RATE
C          (I*4)  ISPFA(,)  = FINAL PARENT SPIN FOR AUGER RATE
C                                 1ST. INDEX = AUGER PARENT PAIR
C                                 2ND. INDEX = DR FILE INDEX
C          (I*4)  ISPI      = INITIAL PARENT SPIN FOR AUGER RATE
C          (I*4)  ISPIA(,)  = FINAL PARENT SPIN FOR AUGER RATE
C                                 1ST. INDEX = AUGER PARENT PAIR
C                                 2ND. INDEX = DR FILE INDEX
C          (I*4)  IST1    = PARAMETER = MAIN OUTPUT STREAM
C          (I*4)  ISYS      = RUNNING INDEX FOR SPIN SYSTEMS
C          (I*4)  IT        = RUNNING INDEX FOR TEMPERATURES
C          (I*4)  JPRT      = RUNNING INDEX FOR PARENTS
C          (I*4)  LEN1      = FIRST NON-BLANK CHARACTER IN MEMBER NAME
C          (I*4)  LEN2      = LAST NON-BLANK CHARACTER IN MEMBER NAME
C          (I*4)  MP()      = INITIAL PARENT INDEX FOR AUGER RATE
C          (I*4)  MPA()     = FINAL PARENT INDEX FOR AUGER RATE
C          (I*4)  NBCUT(,)  = N-SHELL CUT FOR AUGER RATES (AUGER CHANNEL
C                             OPENS AT NBCUT+1)
C                                 1ST. INDEX = AUGER PARENT PAIR
C                                 2ND. INDEX = DR FILE INDEX
C          (I*4)  NBFIL     = NUMBER OF DR FILES TO BE INCLUDED
C          (I*4)  NBREP(,)  = DR REPRESENTATIVE LEVEL N -VALUE
C                                 1ST. INDEX = LEVEL INDEX
C                                 2ND. INDEX = DR FILE INDEX
C          (I*4)  NBT       = NUMBER OF DR TEMPERATURES
C          (I*4)  NCUTS     = FIRST OPENING NSHELL FOR SUPPL. AUGER
C          (I*4)  NDAUG     = PARAMETER = MAXIMUM N-SHELL OF SPECIFIC
C                                       AUGER DATA
C          (I*4)  NPAIRS    = NUMBER OF AUGER RATE PARENT PAIRS
C          (I*4)  NPRNT     =
C          (I*4)  NPRNTF()  = NUMBER OF FINAL DR PARENTS FOR FILE
C          (I*4)  NPRNTI()  = NUMBER OF INITIAL DR PARENTS FOR FILE
C          (I*4)  NPTOT     = TOTAL NUMBER OF PARENTS ACCUMULATED FROM
C          (I*4)  NREP      = VALUE OF REPRESENTATIVE N-SHELL NREPX(IREP)
C                             DR FILES
C          (I*4)  NSREP()   = SUPPLEMENTARY AUGER REPRESENT. N-SHELLS
C          (I*4)  NTOP      = MARKS DRM ARRAY ZERO FOR N>NTOP
C
C          (R*8)  AAS       = SUPPL. AUGER COEFFT. AT NCUTS (SEC-1)
C          (R*8)  AUGTMP(N)  = TEMPORARY STORE OF SUPP. AUGER RATES
C                                       1ST INDEX - N-SHELL VALUE
C          (R*8)  DDRROUT() = SCALED DIELECTRONIC DATA FOR SPLINE IN N
C          (R*8)  DELTAE    = SATELLITE ENERGY LEVEL ( K)
C          (R*8)  DRRIN()   = SCALED DIELECTRONIC DATA FOR SPLINE IN N
C          (R*8)  DRMSF(,,,,)  SUMMED DR COEFFICIENT
C                                       1ST INDEX - FILE
C                                       2ND INDEX - TEMPERATURE
C                                       3RD INDEX - INITIAL PARENT
C                                       4TH INDEX - SPIN SYSTEM
C                                       5TH INDEX - FINAL PARENT
C          (R*8)  DRMS()       TEMPORARY STORE OF SUMMED DR RATES
C                                       1ST INDEX - TEMPERATURE
C          (R*8)  DRMF(,)      TEMPORARY STORE OF DR RATES FOR NBREP
C                                       1ST INDEX - REPRESENTATIVE LEVEL
C                                       2ND INDEX - TEMPERATURE
C          (R*8)  DTMP()    = TEMPORARY STORE OF DIEL. COEFFICIENTS
C          (R*8)  DRROUT()  = SCALED DIELECTRONIC DATA FOR SPLINE IN N
C          (R*8)  DY()      = WORK VECTOR FOR SPLINE
C          (R*8)  SLOPE     = N POWER FOR SUPPL. AUGER RATE ABOVE NCUTS
C          (R*8)  SYSFAC(,) = SPIN SYSTEM RESOLUTION OF AUGER RATES
C                                 1ST. INDEX = AUGER RATE INDEX ON LINE
C                                 2ND. INDEX = SPIN SYSTEM
C          (R*8)  TEB()     = DR TEMPERATURES (K)
C          (R*8)  XIN()     = WORK VECTOR FOR SPLINES
C          (R*8)  XOUT()    = WORK VECTOR FOR SPLINE
C          (R*8)  XNBREP()  = DR REPRES. LEVEL N -VALUE AS A REAL
C                                 1ST. INDEX = LEVEL INDEX
C          (R*8)  XNREPX()  = REPRES. LEVEL N-VALUE FROM MAIN CODE AS A
C                                  REAL
C          (R*8)  YIN()     = WORK VECTOR FOR SPLINES
C          (R*8)  YOUT()    = WORK VECTOR FOR SPLINE
C
C
C          (R*8)  XNREP    = REAL VARIABLE FORM OF NREP
C
C          (R*8)  XICENH   = IC ENHANCEMENT FACTOR FOR SPECIFIC
C                            N-SHELL
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C           B4FLNM      ADAS    EXPAND FILENAME SYMBOLIC PART IF PRESENT
C           B4SUMD      ADAS    SUMS DR COEFFICIENTS OVER ALL N-SHELLS
C           FINTB       HPS     CONVERTS X-VALUES FOR N SHELL SPINE
C           XXSLEN      ADAS    FINDS LENGTH OF NON-BLANK PART OF STRINGS
C           XXSPLN      ADAS    GENERAL CUBIC SPLINE
C           xxdata_09   ADAS    Read adf09 DR files
C
C
C  AUTHOR:  HUGH P. SUMMERS, JET
C           K1/1/57
C           JET EXT. 4941
C
C  DATE:    12/05/92
C
C  UPDATE:  04/06/92,  WILLIAM J. DICKSON , JET
C           ADJUSTED FORMAT STATEMENTS FROM ORIGINAL SPEC.
C           TO READ DR FILES WITH CHARACTERS SHIFTED ONE
C           SPACE TO THE LEFT.
C           DEFINED OUTPUT STREAM BY PARAMETER IST1
C
C  UPDATE:  07/92,  WILLIAM J. DICKSON , JET
C           DEFINE VALUE OF LSETX AT BEGINNING OF CODE
C
C  UPDATE:  27/08/92,  WILLIAM J. DICKSON , JET
C           (1)  ALLOW FOR SPECIFIC DATA FOR LOWEST N-SHELLS WHEN
C           INPUTING SUPPLEMENTARY AUGER TRANSITION PROBABILITIES
C           (2)  DEFINE VARIABLE ISET TO MARK SUPPLEMENTARY DATA INPUT
C
C  UPDATE:  06/09/92,  WILLIAM J. DICKSON , JET
C           XREF FILES NOW STORED UNDER JETXLE
C
C  UPDATE:  14/12/92,  WILLIAM J. DICKSON , JET
C           SET UP ROUTINE TO SUM DR COEFFICIENTS OVER
C           REPRESENTATIVE SET
C  UPDATE:  13/11/93,  WILLIAM J. DICKSON , JET
C           (1)  ALLOW FOR IC ENHANCEMENT FACTOR TO BE READ IN AS PART
C                FILE AND SUBSEQUENT ADJUSTMENT OF DR RATE COEFFICIENT
C                  CHECK CODING AROUND FORMAT STATEMENT 1036.
C                  (NOTE THAT 1037 WAS ADDED AT THIS STAGE)
C
C  UPDATE:  29/05/96  HP SUMMERS - COMPLETED UNIX FILE NAME PROCUREMENT
C                                  WITH ENVIRONMENT VARIABLE SYMBOL
C                                  SUBSTITUTION USING B4FLNM
C  UPDATE:  22/01/97  HP SUMMERS - CHANGED NAME TO B4DATD FROM BDMNCL1
C                                  AND SUBROUTINE BDDRSM2 TO B4SUMD
C  UPDATE:  11/02/97  HP SUMMERS - IMPROVED INTERPOLATION OF SUPPLE.
C                                  AUGER DATA FROM X-REF FILE.
C  UPDATE:  17/02/97  HP SUMMERS - IMPROVED INTERPOLATION OF DR. DATA
C                                  WITH N, TO ENSURE ABSOLUTE ZEROS
C                                  ABOVE CUT-OFF N-SHELL
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                                  DATE: 05-03-98
C MODIFIED: H. SUMMERS
C                   - MODIFIED VESION OF BDMNCL1.FOR v 1.1
C
C VERSION: 1.2                                  DATE: 26-11-98
C MODIFIED: Martin O'Mullane
C                   - redefine DSNXRT as the full DR supplement file
C                     name. It is now given in the adf25 dataset and
C                     passed through to here.
C
C VERSION: 1.3                                  DATE: 22-09-2000
C MODIFIED: Martin O'Mullane
C                   - Initialize ibmax to 0 to avoid troubles in the
C                     H-like case where we have no DR.
C
C
C VERSION : 1.4
C DATE    : 02-11-2010
C MODIFIED: Martin O'Mullane
C              - Re-write to use xxdata_09 to read DR data.
C              - Increase dimension of NSREP.
C              - Remove unused variables.
C
C VERSION : 1.5
C DATE    : 21-01-2013
C MODIFIED: Martin O'Mullane
C              - Increase dimension of representative n arrays to 100.
C
C VERSION : 1.6
C DATE    : 19-07-2017
C MODIFIED: Martin O'Mullane
C              - Extra arguments required for xxdata_09.
C
C VERSION : 1.8
C DATE    : 08-071-2018
C MODIFIED: Martin O'Mullane
C              - Further arguments required for hybrid adf09 files
C                in xxdata_09.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       INTEGER     NDREP     , NDPRT     , NDSYS    , NDBFIL   ,
     &             NDT       , NDPAIR    , NDBREP   , NDAUG    ,
     &             IST1      ,
     &             ndlev     , ndlrep    , ndaug_09 , ndrep_09 ,
     &             ndsc      , iunt09
C-----------------------------------------------------------------------
       PARAMETER ( NDREP  = 100   , NDPRT = 12  , NDSYS  = 2     )
       PARAMETER ( NDBFIL = 6     , NDT   = 24  , NDPAIR = 20    )
       PARAMETER ( NDBREP = 100   , NDAUG = 550 , ndsc   = 1000  )
       PARAMETER ( IST1   = 17   )
       parameter ( ndlev  = 5000 , ndlrep = 250  , ndaug_09 = 20 ,
     &             ndrep_09 = 100, iunt09 = 49   )
C-----------------------------------------------------------------------
       INTEGER     NPMNCL    , IMAXX     , MAXTM    , NDBFILM  ,
     &             NBFIL     , IOPT      , IPRT     , JPRT     ,
     &             ISYS      , IBREP     , IT       , IBFIL    ,
     &             IREP      , LEN1      , LEN2     , LEN5     ,
     &             LEN4      , LEN6      , I4UNIT   , I        ,
     &             NPRNT     , ISUPPLE   , NREP     , IREAD    ,
     &             IPI       , ISYSI     , IPF      , NCUTS    ,
     &             NPTOT     , IPAIRS    , NPAIRS   , ISPI     ,
     &             II        , ISPF      , NBT      , IC       ,
     &             IREPMAX   , IS        , IF       , ISREP    ,
     &             NTOP
C-----------------------------------------------------------------------
       REAL*8      AAS       , SLOPE     , XNCUTS   , XNREP    ,
     &             DELTAE    , sum       , fintb
C-----------------------------------------------------------------------
       CHARACTER   DSNBD(NDBFIL)*120
       CHARACTER   BPDSC(NDPRT)*30 , BPDS*30
       CHARACTER   DSNXR*120 , DSNMC*120 , DSNMCO*120   , DSHORT*120
       CHARACTER   DSN*120   , XRMEMB*8  , MEMBER*8
       CHARACTER   DSNXRT*120
       CHARACTER   STRING*80
       CHARACTER   DSNIN*120 , ADAS_C*80 , ADAS_U*80
C-----------------------------------------------------------------------
       LOGICAL     OPEN12   , OPEN17
       LOGICAL     LEXIST   , LSETX
C-----------------------------------------------------------------------
       INTEGER     IMNPA(NDPRT)         , IBDPA(NDPRT,NDBFIL)
       INTEGER     IBPRIA(NDPRT,NDBFIL) , IBPRFA(NDPRT,NDBFIL)
       INTEGER     ISPFA(NDPAIR,NDBFIL) , ISPIA(NDPAIR,NDBFIL)
       INTEGER     IREFI(NDPAIR)        , IREFF(NDPAIR)
       INTEGER     MPA(NDPAIR)          , MP(NDPAIR)
       INTEGER     NBCUT(NDPAIR,NDBFIL) , IBMAX(NDBFIL)
       INTEGER     NCUTMC(NDPRT,NDPRT)  , NBREP(NDBREP,NDBFIL)
       INTEGER     NREPX(NDREP)         , NPRNTI(NDBFIL)
       INTEGER     NPRNTF(NDBFIL)       , NSREP(NDREP*2)
       INTEGER     ISET(NDPRT,NDSYS,NDPRT)
C-----------------------------------------------------------------------
       REAL*8      DRROUT(NDREP) , DDRROUT(NDREP)
       REAL*8      XNREPX(NDREP)
       REAL*8      XNBREP(NDBREP) , DRRIN(NDBREP)
       REAL*8      SYSFAC(NDPAIR,2)
       REAL*8      TEM(NDT)
       REAL*8      TEB(NDT) , XIN(NDT) , YIN(NDT) , DTMP(NDT)
       REAL*8      XOUT(NDT) , YOUT(NDT) , DY(NDT)
       REAL*8      AUGM(NDREP,NDPRT,NDSYS,NDPRT)
       REAL*8      DRM(NDREP,NDT,NDPRT,NDSYS,NDPRT)
       REAL*8      AUGTMP(NDAUG)
       REAL*8      DRMSF(NDBFILM,NDT,NDPRT,NDSYS,NDPRT)
       REAL*8      DRMS(NDT)
       REAL*8      DRMF(NDREP,NDT)
       REAL*8      EIJN(NDREP) ,  PWTEMP(NDT)
       REAL*8      PWSAT(NDBFILM,NDT,NDPRT,NDSYS,NDPRT)
C-----------------------------------------------------------------------
       DATA  OPEN12/.FALSE./
       DATA  LEXIST/.FALSE./ , LSETX/.TRUE./
       DATA  IOPT/-1/
C-----------------------------------------------------------------------
       EXTERNAL FINTB
C-----------------------------------------------------------------------
C xxdata_09 variables
C-----------------------------------------------------------------------
       integer   iz      , iz0      , iz1        , iform      ,
     &           nte_09  , iaprs_n  , iaprs_nl   , nprnti_09  ,
     &           nlvl_09 , nprntf_09, nlrep      , ncfg_09    , 
     &           nippy_09
C-----------------------------------------------------------------------
       real*8    bwnp           , bwnr
C-----------------------------------------------------------------------
       character ctype*2        , seqsym*2
C-----------------------------------------------------------------------
       integer   ipa_09(ndprt)  , ispa_09(ndprt)    , ilpa_09(ndprt) ,
     &           ip_09(ndlev)   , ipaug_n(ndaug_09,2) ,
     &           ipaug_nl(ndaug_09,2)
       integer   ia_09(ndlev)       , isa_09(ndlev) , ila_09(ndlev)  ,
     &           iprtf(ndprt,ndprt) , iprti(ndprt)  ,
     &           irepa_n(ndrep_09)  ,
     &           irepa_nl(ndlrep)   , ispsys(ndprt,ndprt,2)          ,
     &           nlrepa_l(ndlrep)   , nlrepa_n(ndlrep)               ,
     &           nrepa(ndrep_09)    , nsysf(ndprt,ndprt)             ,
     &           isys_09(ndprt,ndprt,2)
       integer   iscp(ndsc)         , iscn(ndsc)                     , 
     &           iscl(ndsc)         , ipca_09(ndprt)
C-----------------------------------------------------------------------
       real*8    xjpa_09(ndprt) , wpa_09(ndprt)
       real*8    xja_09(ndlev)  , wa_09(ndlev)
       real*8    xjpca_09(ndprt), wpca_09(ndprt)
       real*8    tea_09(ndt)
       real*8    auga_nl(ndlrep,ndaug_09) , auga_n(ndrep_09,ndaug_09)
       real*8    diel_res(ndlev,ndprt,ndt)
       real*8    diel_nl(ndlrep,ndprt,ndprt,2,ndt)
       real*8    diel_n(ndrep_09,ndprt,ndprt,2,ndt)
       real*8    diel_sum(ndprt,ndprt,2,ndt)
       real*8    diel_tot(ndprt,ndt)
       real*8    auga_res(ndlev,ndprt)
       real*8    fsc(ndsc,2)                , wnsc(ndsc,2)
C-----------------------------------------------------------------------
       logical   lauga_res(ndlev,ndprt)
       logical   lauga_n(ndrep_09,ndaug_09)
       logical   lauga_nl(ndlrep,ndaug_09)
       logical   ldiel_res(ndlev,ndprt)
       logical   ldiel_nl(ndlrep,ndprt,ndprt,2)
       logical   ldiel_n(ndrep_09,ndprt,ndprt,2)
       logical   ldiel_sum(ndprt,ndprt,2)
       logical   ldiel_tot(ndprt)
C-----------------------------------------------------------------------
       character cstrpa_09(ndprt)*20 , cstrga_09(ndlev)*20, 
     &           cstrpca_09(ndprt)*20
       character caprs_n(ndaug_09)*10
       character caprs_nl(ndaug_09)*10
       character cspsys(ndprt,ndprt,2)*30
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C****************** MAIN PROGRAM   *************************************
C-----------------------------------------------------------------------
C
       LSETX = .TRUE.

       DO IBFIL = 1, NDBFIL
         IBMAX(IBFIL) = 0
         do ii = 1, ndprt
           ibpria(ii,ibfil) = 0
           ibprfa(ii,ibfil) = 0
         end do
         do ii = 1, ndpair
           ispfa(ii,ibfil) = 0
           ispia(ii,ibfil) = 0
         end do
       END DO
C-----------------------------------------------------------------------
C  ZERO AUGER AND DIELECTRONIC OUTPUT ARRAYS.  NOTE LOGARITHMIC
C-----------------------------------------------------------------------
C
       IF (OPEN17) WRITE(IST1,1000)

       DO 5 IPRT = 1,NDPRT
       DO 5 JPRT = 1,NDPRT
         NCUTMC(IPRT,JPRT)=1000
       DO 5 ISYS = 1,NDSYS
         ISET(IPRT,ISYS,JPRT) = 0
       DO 5 IBREP = 1,NDBREP
         AUGM(IBREP,IPRT,ISYS,JPRT) = 0.0D0
       DO 5 IT = 1,NDT
         DRM(IBREP,IT,IPRT,ISYS,JPRT) = 0.0D0
    5  CONTINUE
C
       DO 4 IPRT = 1,NDPRT
       DO 4 JPRT = 1,NDPRT
       DO 4 ISYS = 1,NDSYS
       DO 4 IBFIL = 1,NDBFIL
       DO 4 IT = 1,NDT
         DRMSF(IBFIL,IT,IPRT,ISYS,JPRT) = 0.0D0
         PWSAT(IBFIL,IT,IPRT,ISYS,JPRT) = 0.0D0
    4  CONTINUE
C
C-----------------------------------------------------------------------
C  SET UP XOUT AND XNREPX VECTOR
C-----------------------------------------------------------------------
C
       DO 6 IT=1,MAXTM
        XOUT(IT)=DLOG10(TEM(IT))
    6  CONTINUE
       DO 7 IREP=1,IMAXX
        XNREPX(IREP)=NREPX(IREP)
    7  CONTINUE




C-----------------------------------------------------------------------
C  IDENTIFY CROSS-REFERENCE FILE AND OPEN IT.
C
C  Perform a self cosistency check
C    -  Check if the adf25 (bns) file identified in the cross-reference file
C       corresponds to the one driving the ADAS204 calculation.
C-----------------------------------------------------------------------

      CALL XXSLEN(DSNXRT,LEN1,LEN2)
      dsnxr = dsnxrt(len1:len2)
      IF (OPEN17) WRITE(IST1,1041)DSNXR

      CALL XXSLEN(XRMEMB,LEN5,LEN6)

      OPEN12 = .FALSE.
      LEXIST = .FALSE.
      IF (OPEN12) CLOSE(12)
      INQUIRE( FILE=DSNXR   , EXIST=LEXIST)
      IF(LEXIST) then
         OPEN(UNIT=12 , FILE = DSNXR, STATUS='UNKNOWN')
         OPEN12 = .TRUE.
      ENDIF

C-----------------------------------------------------------------------
C  OBTAIN ALL FILE NAMES AND DATA FROM CROSS-REFERENCE FILE.
C-----------------------------------------------------------------------

      READ(12,1002)DSHORT
      CALL B4FLNM(adas_c, adas_u, DSHORT,DSNMC,LEXIST)

C Check (i) - do adf25 file names match

      if (dsnmc.ne.dsnin) then
         WRITE(I4UNIT(-1),2001)dsnin,dsnmc
      endif


C Check (ii) - does the adf25 file name match with the ion stage (XRMEMB)
C EXTRACT THE MEMBER NAME FROM DSNMC

      CALL XXSLEN(DSNMC,LEN5,LEN6)

      LEN4=0
 255  LEN5 = INDEX(DSNMC(LEN4+1:LEN6),'_')
      IF(LEN5.GT.0)THEN
         LEN4=LEN4+LEN5
         GOTO 255
      ELSE
         LEN5=LEN4
      ENDIF
      LEN6 = INDEX(DSNMC, '.')
      if (len5.eq.0.and.len6.eq.0) then
         member=' '
      else
         MEMBER=DSNMC(LEN5+1:LEN6-1)
      endif
      CALL XXSLEN(MEMBER,LEN1,LEN2)
      if (len2+1.LE.8) MEMBER(LEN2+1:LEN2+1)='n'
      IF(MEMBER.NE.XRMEMB) THEN
        WRITE(I4UNIT(-1),2004)dsnmc,member,xrmemb
      ENDIF


C Now read in adf09 DR filenames

      NBFIL  = 0
 10   READ(12,1003) DSHORT
      IF(DSHORT(1:1).NE.' ')THEN
         CALL B4FLNM(adas_c, adas_u,DSHORT,DSN,LEXIST)
         NBFIL = NBFIL + 1
         DSNBD(NBFIL) = DSN
         GO TO 10
      ENDIF

      IF (OPEN17) WRITE(IST1,1042)(DSNBD(IBFIL),IBFIL=1,NBFIL)

      READ(12,1004) DSHORT
      CALL B4FLNM(adas_c, adas_u,DSHORT,DSNMCO,LEXIST)

      IF (OPEN17) WRITE(IST1,1043)('bd',I,I=1,NBFIL)
      NPRNT=0
 20   READ(12,1005) STRING
      IF(STRING(1:7).NE.'       ') THEN
         NPRNT = NPRNT + 1
         READ(STRING,1006)IMNPA(NPRNT),
     &        (IBDPA(NPRNT,IBFIL),IBFIL=1,NBFIL)
         IF (OPEN17) WRITE(IST1,1044) IMNPA(NPRNT),
     &        (IBDPA(NPRNT,IBFIL),IBFIL=1,NBFIL)
         GO TO 20
      ENDIF
C--------------------------------------
C  FETCH SUPPLEMENTARY LS BREAKDOWN
C  AUGER DATA IF PRESENT IN XREF FILE.
C  SET SWITCHS ISET() ACCORDINGLY.
C--------------------------------------
      READ(12,1005,END=25)STRING
      IF(STRING(1:7).EQ.'Supplem')THEN
         IF (OPEN17) WRITE(IST1,1053)
         READ(12,1005)(STRING,I=1,4)
         ISUPPLE=0
 22      READ(12,1005,END=25)STRING
         IF(STRING(1:7).NE.'       ')THEN
            DO NREP = 1,NDAUG
               AUGTMP(NREP) = 0.0D+0
            END DO
            IREAD = 0
 23         CONTINUE
            IF(IREAD .EQ. 0) THEN
               READ(STRING,1052)IPI,ISYSI,IPF,NCUTS,AAS,SLOPE
               IF (OPEN17) WRITE(IST1,1054)IPI,ISYSI,IPF,NCUTS,AAS,SLOPE
               IREAD = 1
               ISET(IPI,ISYSI,IPF) = 1
            ELSE
               READ(STRING,1062) NCUTS,AAS,SLOPE
               IF (OPEN17) WRITE(IST1,1064)  NCUTS,AAS,SLOPE
            ENDIF
C-------------
            IF(AAS .GT. 0.0D0 .AND. SLOPE .EQ. 0.0D0) THEN
               ISUPPLE=ISUPPLE+1
               NSREP(ISUPPLE)=NCUTS
               NCUTMC(IPI,IPF)= MIN(NCUTS,NCUTMC(IPI,IPF))
               AUGTMP(NCUTS) = AAS
               READ(12,1005) STRING
               GOTO 23
            ELSE
               ISUPPLE=ISUPPLE+1
               NSREP(ISUPPLE)=NCUTS
               NCUTMC(IPI,IPF)= MIN(NCUTS,NCUTMC(IPI,IPF))
               XNCUTS=NCUTS
            ENDIF
            NCUTMC(IPI,IPF)= NCUTMC(IPI,IPF) - 1
C--------------------------------------
C  FILL SUPPLEMENTARY AUGER DATA IN
C  ADAS204 REPRESENTATIVE N-SHELL SET.
C--------------------------------------
            DO 24 IREP=1,IMAXX
               NREP  = NREPX(IREP)
               XNREP = XNREPX(IREP)
               IF(NREP .LE. NCUTMC(IPI,IPF) ) THEN
                  AUGM(IREP,IPI,ISYSI,IPF)=0.0D0
               ELSEIF( AUGTMP(NREP) .GT. 0.0D0) THEN
                  AUGM(IREP,IPI,ISYSI,IPF) = AUGTMP(NREP)
                  DO IS=1,ISUPPLE
                    IF(NSREP(IS).EQ.NREP)THEN
                        ISREP=IS
                    ENDIF
                  ENDDO
               ELSEIF(NREP.LT.NCUTS)THEN
                  AUGM(IREP,IPI,ISYSI,IPF)=(XNREP-NSREP(ISREP+1))/
     &                 (NSREP(ISREP)-NSREP(ISREP+1))*
     &                 AUGTMP(NSREP(ISREP))+(XNREP-NSREP(ISREP))/
     &                 (NSREP(ISREP+1)-NSREP(ISREP))*
     &                 AUGTMP(NSREP(ISREP+1))
               ELSE
                  AUGM(IREP,IPI,ISYSI,IPF)= AAS*(XNREP/XNCUTS)**SLOPE
               ENDIF
C-------------
 24         CONTINUE
            GO TO 22
         ENDIF
      ENDIF
 25   CONTINUE



C-----------------------------------------------------------------------
C  Open adf09 dielectronic data input - each file in turn
C  zero data used between calls.
C-----------------------------------------------------------------------

      do ibfil = 1 , nbfil

         nprnti_09 = 0
         nprntf_09 = 0
         nte_09    = 0
         iaprs_n   = 0

         do ii = 1, ndprt
           cstrpa_09(ii) = '                    '
           ispa_09(ii) = 0
           ilpa_09(ii) = 0
           xjpa_09(ii) = 0.0D0
         end do

        do ii = 1, ndpair
          mp(ii) = 0
          mpa(ii) = 0
        end do

        do is = 1, 2
           do ii = 1, ndaug_09
             ipaug_n(ii, is) = 0
           end do
        end do
        do ii = 1, ndaug_09
          do irep = 1, ndrep_09
              auga_n(irep, ii) = 0.0D0
            end do
         end do

         do it = 1, ndt
           tea_09(it) = 0.0D0
           do is = 1, 2
             do if = 1, ndprt
               do ii = 1, ndprt
                 do irep = 1, ndrep_09
                   diel_n(irep, ii, if, is, it) = 0.0D0
                 end do
               end do
             end do
           end do
         end do

         open(unit=iunt09 , file = dsnbd(ibfil), status='old')
         call xxdata_09( iunt09   , ndprt    , ndrep_09 , ndlrep   ,
     &                   ndlev    , ndaug_09 , ndt      , ndsc     ,
     &                   seqsym   , iz       , iz0      , iz1      ,
     &                   ctype    , iform    ,
     &                   nprnt    , nprnti_09, nprntf_09, 
     &                   bwnp     ,
     &                   ipa_09   , cstrpa_09, ispa_09  , ilpa_09  ,
     &                   xjpa_09  , wpa_09   ,
     &                   ipca_09  , cstrpca_09          , 
     &                   xjpca_09 , wpca_09  ,
     &                   nlvl_09  , ncfg_09  , nippy_09 , bwnr     ,
     &                   ia_09    , ip_09    , cstrga_09, isa_09   ,
     &                   ila_09   , xja_09   , wa_09    ,
     &                   iscp     , iscn     , iscl     ,
     &                   fsc      , wnsc     ,
     &                   auga_res , lauga_res,
     &                   nlrep    , iaprs_nl , caprs_nl , ipaug_nl ,
     &                   irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,
     &                   lauga_nl ,
     &                   nrep     , iaprs_n  , caprs_n  , ipaug_n  ,
     &                   irepa_n  , nrepa    , auga_n   , lauga_n  ,
     &                   iprti    ,
     &                   diel_res , ldiel_res,
     &                   iprtf    ,
     &                   nsysf    , isys_09  , ispsys   , cspsys   ,
     &                   diel_nl  , ldiel_nl , diel_n   , ldiel_n  ,
     &                   diel_sum , ldiel_sum, diel_tot , ldiel_tot,
     &                   nte_09   , tea_09
     &                 )
         close(iunt09)


C Parent configurations

         nprnti(ibfil) = nprnti_09
         nprntf(ibfil) = nprntf_09
         nprnt         = nprnti_09
         nptot         = nprnt

         do i = 1, nptot
            write(BPDSC(i), '(a20,1H(,i1,1H),i1,1H(,f4.1,1H))')
     &                        cstrpa_09(i), ispa_09(i),
     &                        ilpa_09(i), xjpa_09(i)
         end do
         do if = 1, nprntf_09
            ibprfa(if,ibfil) = ipa_09(if)
         end do
         do ii = 1, nprnti_09
            ibpria(ii,ibfil) = ipa_09(ii)
         end do

C Identify parents and spin systems for Auger rates

         npairs = iaprs_n
         do ipairs = 1, npairs
            mp(ipairs) = ipaug_n(ipairs,1)
            mpa(ipairs) = ipaug_n(ipairs,2)
            nbcut(ipairs,ibfil)=1000
         end do

         do ipairs = 1, npairs

            bpds=bpdsc(ibpria(mp(ipairs),ibfil))
            read(bpds,1026)ispi
            ispia(ipairs,ibfil)=ispi
            irefi(ipairs)=0
            do ii = 1,nprnt
               if(ibdpa(ii,ibfil).eq.mp(ipairs))irefi(ipairs)=ii
            end do

            bpds=bpdsc(ibprfa(mpa(ipairs),ibfil))
            read(bpds,1026)ispf
            ispfa(ipairs,ibfil)=ispf
            ireff(ipairs)=0
            do ii = 1,nprnt
               if(ibdpa(ii,ibfil).eq.mpa(ipairs))ireff(ipairs)=ii
            end do

C  If final parent is not in the adf25 dataset, allocate to the lowest parent

            if(ireff(ipairs).gt.npmncl.or.ireff(ipairs).eq.0)
     &           ireff(ipairs)=1

            if(ispi.eq.ispf.and.ispi.eq.1)then
               sysfac(ipairs,1)=1.0D0
               sysfac(ipairs,2)=0.0D0
            elseif(ispi.eq.ispf)then
               sysfac(ipairs,1)=1.0D0
               sysfac(ipairs,2)=1.0D0
            elseif(ispi.lt.ispf.anD.ispi.eq.1)then
               sysfac(ipairs,1)=1.0D0
               sysfac(ipairs,2)=0.0D0
            elseif(ispi.lt.ispf.anD.ispi.gt.1)then
               sysfac(ipairs,1)=0.0D0
               sysfac(ipairs,2)=1.0D0
            elseif(ispi.gt.ispf)then
               sysfac(ipairs,1)=1.0D0
               sysfac(ipairs,2)=0.0D0
            endif

         end do


C Fill AUGM but bypass if data was in adf18 file

         ibmax(ibfil) = nrep

         do irep = 1, ibmax(ibfil)

            nbrep(irep, ibfil) = nrepa(irep)
            do ipairs=1,npairs

                if(irefi(ipairs).gt.0.and.ireff(ipairs).gt.0) then
                   if(auga_n(irep, ipairs).le.0.0d0)then
                      nbcut(ipairs,ibfil) = nbrep(irep,ibfil)
                   endif
                   do isys = 1,2
                     if( iset(irefi(ipairs),isys,ireff(ipairs)).ne.1)
     &                       augm(irep,irefi(ipairs),
     &                            isys,ireff(ipairs)) =
     &                       augm(irep,irefi(ipairs),
     &                       isys,ireff(ipairs)) +
     &                       sysfac(ipairs,isys)*auga_n(irep, ipairs)
                   end do
                endif

            end do
         end do

         do ipairs = 1,npairs
            if (open17) write(ist1,1048)mp(ipairs),
     &           ibprfa(mp(ipairs),ibfil),
     &           irefi(ipairs),ispia(ipairs,ibfil),
     &           mpa(ipairs),ibprfa(mpa(ipairs),ibfil),
     &           ireff(ipairs),ispfa(ipairs,ibfil),
     &           nbcut(ipairs,ibfil)
            if(irefi(ipairs).ne.0.and.ireff(ipairs).ne.0) then
               ncutmc(irefi(ipairs),ireff(ipairs))= min0(
     &       ncutmc(irefi(ipairs),ireff(ipairs)),nbcut(ipairs,ibfil))
            endif
         end do


C  Find the dielectronic parents and data

         nbt = nte_09
         do it = 1, nbt
           teb(it) = tea_09(it)
           xin(it) = dlog10(teb(it))
         end do

         do ii=1,nprnti(ibfil)
           do is=1,2
             do if=1,nprntf(ibfil)

               do irep = 1, ndrep
                 do it = 1, ndt
                    drmf(irep, it) = 0.0D0
                 end do
               end do

               do irep = 1, ibmax(ibfil)

                   do it = 1, ndt
                      dtmp(it) = 0.0D0
                   end do

                   ic  = 1
                   sum = 0.0D0
                   do it = 1, nbt
                      dtmp(it) = diel_n(irep, ii, if, is, it)
                      if (dtmp(it).le.0.0d0) ic=ic+1
                      sum = sum + dtmp(it)
                      drmf(irep, it) = 0.0D0
                   end do

                   if (sum.GT.0.0D0) then
                      DELTAE=-DLOG((TEB(IC)/TEB(NBT))**1.5D0*(DTMP(IC)/
     &                        DTMP(NBT)))/(1.0D0/TEB(IC)-1.0D0/TEB(NBT))
                      EIJN(IREP) = DELTAE

                      DO IT=1,NBT
                         IF(IT.LT.IC)THEN
                            YIN(IT)=DLOG10(TEB(IC)**1.5D0*DTMP(IC)*
     &                              DEXP(DELTAE/TEB(IC)))
                         ELSE
                            YIN(IT)=DLOG10(TEB(IT)**1.5D0*DTMP(IT)*
     &                              DEXP(DELTAE/TEB(IT)))
                         ENDIF
                      end do

                      CALL XXSPLN( LSETX , IOPT , FINTB ,
     &                             NBT   , XIN  , YIN   ,
     &                             MAXTM , XOUT , YOUT  , DY)


                      DO IT=1,MAXTM
                         IF(YOUT(IT).EQ.0.0D0.AND.
     &                      XOUT(IT).LE.XIN(1))   YOUT(IT)=YIN(1)
                         IF(YOUT(IT).EQ.0.0D0.AND.
     &                      XOUT(IT).GE.XIN(NBT)) YOUT(IT)=YIN(NBT)
                         YOUT(IT)=YOUT(IT)-1.5D0*XOUT(IT)-0.434294D0*
     &                            DELTAE/TEM(IT)
                         IF(YOUT(IT).LE.-78.0D0) THEN
                            YOUT(IT)=0.0D0
                         ELSE
                            YOUT(IT)=10.0D0**YOUT(IT)
                         ENDIF
                         DRM(IREP,IT,II,IS,IF)=DRM(IREP,IT,II,IS,IF)+
     &                                         YOUT(IT)
                         DRMF(IREP,IT) = YOUT(IT)
                      end do

                   endif

               end do  ! nrep

               IREPMAX = IBMAX(IBFIL)
               CALL B4SUMD ( NDREP , NDT      ,
     &                       MAXTM , IREPMAX  ,
     &                       NBREP(1,IBFIL)   , DRMF,  DRMS ,
     &                       EIJN  ,  PWTEMP  )
               DO IT = 1,MAXTM
                  DRMSF(IBFIL,IT,II,IS,IF) = DRMS(IT)
                  PWSAT(IBFIL,IT,II,IS,IF) = PWTEMP(IT)
               end do
             end do      ! final parent
            end do       ! spin
         end do          ! initial parent

      end do             !end loop over adf09 files


      IF (OPEN17) WRITE(IST1,1050)
      IF (OPEN17) WRITE(IST1,1051)NPTOT,NBFIL

C-----------------------------------------------------------------------
C  CHECK ALL DR REPRESENTATIVE N-SHELL SETS ARE ALL THE SAME
C-----------------------------------------------------------------------

      DO 90 IBFIL=1,NBFIL
         IF(IBMAX(IBFIL).NE.IBMAX(1)) THEN
               WRITE(I4UNIT(-1),*)'**** NUMBER OF REPRESENTATIVE LEVELS
     &DIFFER IN DR FILES'
            STOP
         ENDIF
         DO 89 IBREP=1,IBMAX(1)
            IF(NBREP(IBREP,1).NE.NBREP(IBREP,IBFIL))THEN
               WRITE(I4UNIT(-1),*)'**** REPRESENTATIVE LEVELS DIFFER IN
     &DR FILES'
             STOP
          ENDIF
 89    CONTINUE
 90   CONTINUE




C-----------------------------------------------------------------------
C  SPLINE DIEL. AND AUGER DATA FROM DR REPRESENTATIVE N-SHELLS TO
C  ADAS204 REPRESENTATIVE N-SHELLS USING FINTB
C-----------------------------------------------------------------------

      LSETX=.TRUE.

C Zero xnbrep if there is no DR

      IF (NBFIL.GT.0) THEN
         DO 91 IBREP=1,IBMAX(1)
            XNBREP(IBREP)=NBREP(IBREP,1)
 91      CONTINUE
      ELSE
         DO IBREP=1, NDBREP
            XNBREP(IBREP)=0.0D0
         END DO




      ENDIF

      DO 99 II=1,NPRNT
         DO 99 IS=1,2
            DO 99 IF=1,NPRNT
C---------------------------------------
C  BYPASS SPLINE OF AUGER DATA IF
C  SUPPLEMENTARY CASE SINCE ALREADY DONE
C---------------------------------------
               IF(ISET(II,IS,IF) .EQ. 1) GO TO 97
               IF(II.LE.IF)GO TO 98
C---------------
C  AUGER SPLINE
C---------------

               DO 92 IBREP=IBMAX(1),1,-1
                  DRRIN(IBREP)=XNBREP(IBREP)**3*AUGM(IBREP,II,IS,IF)
                  IF(NCUTMC(II,IF).GE.NBREP(IBREP,1)) THEN
                     DRRIN(IBREP)=DRRIN(IBREP+1)
                  ENDIF
 92            CONTINUE
               CALL XXSPLN( LSETX    , IOPT    , FINTB           ,
     &                      IBMAX(1) , XNBREP  , DRRIN           ,
     &                      IMAXX    , XNREPX  , DRROUT          ,
     &                      DDRROUT)
               DO 93 IREP=1,IMAXX
                  IF(NCUTMC(II,IF).GE.NREPX(IREP))THEN
                     AUGM(IREP,II,IS,IF)=0.0D0
                  ELSE
                     AUGM(IREP,II,IS,IF)=XNREPX(IREP)**(-3)
     &                                    *DRROUT(IREP)
                  ENDIF
 93            CONTINUE
C---------------
C  DIEL SPLINE
C---------------

   98          CONTINUE
               DO 96 IT=1,MAXTM
                 NTOP=0
                 DO 94 IBREP=IBMAX(1),1,-1
                   DRRIN(IBREP)=XNBREP(IBREP)**3
     &                          *DRM(IBREP,IT,II,IS,IF)
                   IF(DRRIN(IBREP).GT.0.0D0)
     &                NTOP=MAX(NTOP,NBREP(IBREP,1))
 94              CONTINUE
                 CALL XXSPLN( LSETX    , IOPT    , FINTB           ,
     &                        IBMAX(1) , XNBREP  , DRRIN           ,
     &                        IMAXX    , XNREPX  , DRROUT          ,
     &                        DDRROUT)
                 DO 95 IREP=1,IMAXX
                   IF(NREPX(IREP).GT.NTOP) THEN
                       DRM(IREP,IT,II,IS,IF)=0.0D0
                   ELSE
                       DRM(IREP,IT,II,IS,IF)=XNREPX(IREP)**(-3)
     &                                       *DRROUT(IREP)
                   ENDIF
                   IF(DRM(IREP,IT,II,IS,IF).LE.1.0D-60)
     &                   DRM(IREP,IT,II,IS,IF)=0.0D0
 95              CONTINUE
 96            CONTINUE
 97            CONTINUE
 99   CONTINUE


      IF(OPEN12) CLOSE(12)

      RETURN
C-----------------------------------------------------------------------
 1000  FORMAT(//,132('-')/
     &        1H ,'B4DATD: DIELECTRONIC DATA PREPARATION'/
     &        1H ,132('-'))
 1002  FORMAT(/,/,1A120,/,/,/)
 1003  FORMAT(1A120)
 1004  FORMAT(/,/,1A120,/,/,/,/,/,/)
 1005  FORMAT(1A80)
 1006  FORMAT(I7,6I12)
 1026  FORMAT(21X,I1)
 1041  FORMAT(//,'CROSS-REFERENCE FILE FOR DR DATA  - '/
     &        1H ,A)
 1042  FORMAT(//,'FILES OF DIELECTRONIC DATA     - '/
     &        1H ,A/
     &       (1H ,A))
 1043  FORMAT(//,'CROSS-REFERENCING OF PARENTS'/
     &        1H ,'----------------------------'/
     &        1H ,'       mncl',7X,10(A2,I1,7X))
 1044  FORMAT(1H ,11(7X,I3))
 1048  FORMAT(1H ,8X,I2,11X,I2,11X,I2,11X,I2,19X,I2,11X,I2,11X,I2,11X,
     &        I2,I8)
 1050  FORMAT(//,//////,'SUMMARY'/
     &        1H ,'-------')
 1051  FORMAT(1H ,'   NPTOT=',I2,3X,'NBFIL=',I2)
 1052  FORMAT(2I6,1X,I6,13X,I4,D13.3,7X,F5.1)
 1053  FORMAT(//,'SUPPLEMENTARY AUGER RATES'/
     6        1H ,'-------------------------'/
     &        1H ,'     mncl parent and spin indexing'/
     &        1H ,'     iprti  isysi  iprtf    ncut1      aa     slope')
 1054  FORMAT(1H ,6X,I3,4X,I3,4X,I3,4X,I4,3X,1PD10.3,2X,0PF5.2)
 1062  FORMAT(32X,I4,D13.3,7X,F5.1)
 1064  FORMAT(1H ,27X,I4,3X,1PD10.3,2X,0PF5.2)

 2001 format('B4DATD : *** MISMATCH OF ADF25 DRIVER DATASET AND ',
     &       'FILENAME IN ADF18 X-REF FILE ***',/,' adf25 : ',a,/,
     &       ' adf18 : ',a,/,
     &       '*** CHECK DATASETS - ADAS204 CONTINUES ***')

 2004 format('B4DATD : *** MISMATCH OF DRIVER DATASET MEMBER NAME ',
     &       ' AND ION *** ',/,' FILE         : ',a,/,
     &                         ' adf25 MEMBER : ',a,/,
     &                         ' ION          : ',a,/,
     &       '*** CHECK DATASETS - ADAS204 CONTINUES ***')
C-----------------------------------------------------------------------
      END
