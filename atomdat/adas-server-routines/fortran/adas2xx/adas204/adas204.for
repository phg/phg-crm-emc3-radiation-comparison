CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/adas204.for,v 1.16 2013/01/22 15:44:46 mog Exp $ Date $Date: 2013/01/22 15:44:46 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ************  FORTRAN 77 PROGRAM:  ADAS204  *************************
C
C  PREVIOUS NAMES: MAINBNS
C
C  VERSION:  1.0
C
C  PURPOSE: PROGRAM TO EVALUATE BUNDLED N POPULATION STRUCTURE,
C           DENSITY, DEPENDENT COLLISIONAL-DIELECTRONIC RECOMBINATION
C           AND IONISATION COEFFICIENTS AND RADIATIVE POWER LOSS
C           COEFFICIENTS. IT IS A COMPREHENSIVE DEVELOPMENT OF THE
C           PARENT/SPIN SYSTEM RESOLVED MODEL AND INCLUDES THE
C           CALCULATION OF PROJECTION MATRICES
C
C  NOTES:
C   (A) INPUT DATA IS MODIFIED FROM THAT OF THE ORIGINAL BUNDLE-N
C       CODES. THIS IS TO ALLOW TAILORING OF COEFFICIENTS FOR
C       RADIATIVE RECOMBINATION, DIELECTRONIC RECOMBINATION AND
C       COLLISIONAL IONISATION DESIGNED TO MATCH BEST AVAILABLE
C       ZERO-DENSITY DATA FROM INDEPENDENT SOURCES.
C       A FORMATTED TEMPLATE INPUT DATA FILE FOR ADAS204 MAY BE
C       PREPARED BY THE ADAS SERIES 4 CODE ADAS407.  THE DATA FILE
C       IS INCOMPLETE WITH USER ADJUSTMENT REQUIRED BEFORE EXECUTION.
C       THE ALTERATIONS TO THE TEMPLATE INPUT FILES INCLUDE
C       (1) IN THE COMPLETION OF THE 'FILEINFO' NAMELIST ON THE
C           FIRST LINE OF THE FILE
C       (2) COMPLETION OF THE LINE WITH NMIN AND THE QUANTUM DEFECTS
C       (3) THE SETTING OF THE VARIABLE IBSEL1 AND THE COMPLETION OF
C           LINES WITH THE PARENT WEIGHT AND ISZD VARIABLES.
C           (SEE BELOW FOR DESCRIPTION OF INPUT VARIABLES)
C
C       PRE-PREPARED INPUT DATASET HAVE BEEN GIVEN THE ADAS CLASS
C       NUMBER 'ADF25'.  THEY ARE STORED IN SUB-DIRECTORIES BY YEAR
C       AND ISO-ELECTRONIC SEQUENCE IN THE CENTRAL ADAS DATABASE AS
C       EXEMPLIFIED BELOW:
C             /../adas/adas/adf25/bns93#c/bns93#c_o2.dat
C
C
C   (B) ZERO DENSITY 'EXACT' IONISATION AND RECOMBINATION DATA CAN BE
C       INPUT IN TWO WAYS.
C          IF AVAILABLE, EXACT INTERPOLATED ZERO DENSITY RECOMBINATION
C       (RADIATIVE + DIELECTRONIC TOTAL) AND IONISATION DATA CAN BE READ
C       FROM THE ADF25 INPUT FILE. THIS IS USED IN THE FINAL OUTPUT DATA
C       TABLES.  THE ADAS204 CALCULATIONS ARE ONLY USED TO PROVIDE DENSITY
C       DEPENDENT RATIO CORRECTIONS TO THE EXACT DATA.
C          ALTERNATIVELY, DATA CAN BE READ FROM EXTERNAL FILES AND THEN
C       USED THROUGHOUT THE CALCULATION.  THE TWO EXTERNAL INPUT OPTIONS
C       AVAILABLE AT PRESENT ARE
C          (1)  THE USE OF IONISATION RATE COEFFICIENTS FROM AN
C               ADF07 'SZD' FILE.IN THIS CASE, THE INDEX NUMBERS
C                ARE CONTAINED IN THE INPUT FILE (CF.(A)(3) ABOVE)
C          (2)  THE USE OF N-SHELL SELECTIVE DIELECTRONIC RECOMBINATION
C               DATA FROM AN  ADF09 FILE. IN THIS CASE, THE
C               NAME OF A CROSS REFERENCING FILE IS CONTAINED IN THE
C               INPUT DATA FILE (CF. (A)(1) ABOVE). THE CROSS REFERENCING
C               FILE MATCHES THE PARENTS IN THE DR DATA FILE TO THE
C               ADAS204 CALCULATION.
C       IN CASE (2) THERE IS NO OPTION BUT TO USE THE APPROXIMATE FORM
C       FOR RADIATIVE RECOMBINATION CALCULATED INTERNALLY IN ADAS204.
C
C   (C) ADAS204 IS DEVELOPED FROM THE LOW TEMPERATURE VERSION OF
C       THE HYDROGENIC BUNDLE-N CODE.  IT IS THUS DESIGNED TO PERMIT
C       SOLUTION  AT LOW TE. POPULATIONS OF N-SHELLS BELOW A CRITICAL
C       VALUE 'NLWS' ARE THEREFORE EXPRESSED IN THE EXPTE(N)*BN
C       REPRESENTATION WHEREAS N-SHELLS EQUAL TO OR ABOVE NLWS
C       ARE EXPRESSED IN THE CN-REPRESENTATION.  SUBSEQUENT DEVELOPMENTS
C       HAVE BEEN SUCH THAT THE CODE DOES NOT WORK FOR TOO LOW
C       TEMPERATURES. IN REDUCED UNITS  THIS IS FOR   THETA < 5.0E3K.
C       ATTEMPTS TO REMEDY THIS SITUATION ARE IN PROGRESS.
C
C   (D) CHARGE EXCHANGE RECOMBINATION IS SWITCHED OFF, ALTHOUGH
C       THERE IS APPARENT DATA ENTRY FOR IT IN THE INPUT DATA FILE
C
C   (E) PERCIVAL-RICHARDS, VAN REGEMORTER AND IMPACT-PARAMETER
C       CROSS-SECTIONS CAN BE USED.  THE OPTION FOR INCLUDING
C       SPECIAL LOW LEVEL CROSS-SECTIONS IS SWITCHED OFF ALTHOUGH
C       THERE IS AN APPARENT ENTRY FOR IT IN THE DATA FILE
C
C   (F) TO OBTAIN THE CASCADE LINE POWER FROM RECOMBINATION ONLY
C       A GROUND STATE DILUTION W1=1 IS SET IN THE INPUT DATA
C       SET. THIS MEANS THAT THE COLLISIONAL-DIELECTRONIC
C       IONISATION COEFFICIENT IS MEANINGLESS. A SEPARATE LOOP
C       IS INTRODUCED TO PRODUCE THE CORRECT IONISATION COEFFT.
C
C   (G) RADIATIVE POWER LOSS COEFFICIENTS ARE GENERATED.  THESE
C       DO NOT INCLUDE ADJUSTMENTS FOR STIMULATED RECOMBINATION ETC.
C       ONLY THE SPONTANEOUS PROCESSES (BOUND-BOUND, BOUND-FREE
C       AND FREE-FREE) ARE INCLUDED.
C
C   (H) OPTICALLY THICK CASE A/B/C ARE AVAILABLE BY APPROPRIATE
C       DILUTION SETTINGS.
C
C   (I) IN THE INTERNAL ADAS204 CALCULATION OF DIELECTRONIC RECOMBINATION
C       COEFFICEINTS, THE BURGESS GENERAL PROGRAM SUB-ROUTINES ARE USED.
C       THESE REQUIRE BETHE APPROXIMATION CORRECTIONS FOLLOWING THE TYPES
C       AND SPECIFICATIONS GIVEN IN THE SUBOUTINE BURGF. THAT IS
C       AS USED IN DIELECTRONIC DATA PREPARATION (CF ADAS103).
C       THE ACTUAL BETHE CORRECTIONS 'COR' REQUIRED IN ADAS204 ARE
C       GENERATED ON DATA ENTRY AND STACKED LINEARLY IN A
C       LENGTHENED COR(J) VECTOR. THE NUMBER OF CORRECTIONS FOR
C       EACH TRANSITION IS FIXED AT 20.  COR IS REFERENCED INTO THE
C       DIELECTRONIC ROUTINE 'DIELCL' BY STARTING LOCATION.
C
C   (J) NEW ADJUSTMENT OF DIELECTRONIC RATES INTRODUCED 12 DEC 1989
C       PARAMETERS EDISGP,SCALGP  WHICH ALL DIELECTRONIC COMPONENT
C       TRANSITIONS EQUALLY
C
C   (K) AT LOW ELECTRON TEMPERATURE, IONISATION RATE COEFFICIENTS
C       FROM THE LOWEST LEVELS ARE ZERO.  SOLUTION IS ONLY POSSIBLE
C       WITH A DILUTED RADIATION FIELD FOR THE GROUND LEVEL
C       PRESENT.  THE COLLISIONAL-DIELECTRONIC RECOMB. COEFFT. IS
C       MEANINGFUL, AND THE COLLISIONAL-DIELECTRONIC IONISATION
C       COEFFICIENT IS SET TO ZERO.  RECOMBINATION/CASCADE POWER
C       IS CALCULATED MEANINGFULLY.
C
C
C
C  FILE HANDLING:
C
C      5   PIPEIN  PIPE COMMUNICATION FROM IDL          -
C
C      6   PIPEOU  PIPE COMMUNICATION FROM IDL          -
C
C     22   IUNT22  MAIN INPUT FILE OF FORMAT ADF25      - /../adf25/...
C                   (SELECTED IN IDL)
C     17   IUNT17  MAIN OUTPUT                          - adas204.pass1
C                     (DEFINED IN ADAS204.FOR)
C      8   IUNT08  POPULATION OUTPUT (FAIRLY EXTENSIVE) - adas204.pass2
C                     (DEFINED IN ADAS204.FOR)
C      9   IUNT09  AUXILLARY OUTPUT  (FAIRLY EXTENSIVE) - adas204.pass3
C                     (DEFINED IN ADAS204.FOR)
C     18   IUNT18  PROJECTION MATRICES (EXTENDED FILE)  - cbnm.pass
C                     (DEFINED IN ADAS204.FOR)
C     19   IUNT19  PROJECTION MATRICES (PRINTABLE FILE) - cbnmpr.pass
C                     (DEFINED IN ADAS204.FOR)
C     76   IUNT76  CR RECOMBINATION COEFFICIENTS        - acd.pass
C                   (DEFINED IN ADAS204.FOR)
C     61   IUNT61  CR IONISATION COEFFICIENTS           - scd.pass
C                     (DEFINED IN ADAS204.FOR)
C     62   IUNT62  CR PARENT CROSS COUPLING COEFFICIENTS- xcd.pass
C                     (DEFINED IN ADAS204.FOR)
C     63   IUNT63  CR RAD. POWER COEFFICIENTS           - prb.pass
C                     (DEFINED IN ADAS204.FOR)
C     64   IUNT64  WVLGTH. BINNED CASCADE POWER COEFFTS.- pcasbin.pass
C                     (DEFINED IN ADAS204.FOR)
C
C	     OPEN08  .FALSE. - OUTPUT SWITCHED OFF TO IUNT08
C	     OPEN09  .FALSE. - OUTPUT SWITCHED OFF TO IUNT09
C	     OPEN17  .FALSE. - OUTPUT SWITCHED OFF TO IUNT17
C	     OPEN20  .FALSE. - OUTPUT SWITCHED OFF TO IUNT20
C
C  UNITS FOR PRINCIPAL VARIABLES:
C
C      TE       =TEMPERATURE                            (K)
C      DENS     =ELECTRON DENSITY                       (CM-3)
C      ACD      =COLL. DIEL. RECOM. COEFFT.(FROM MAINCL)(CM3 SEC-1)
C      ACD(NE=0)=ZERO DENSITY COLL. DIEL. COEFFT.
C      SCD(W1=0)=COLL. DIEL. IONIS. COEFFT. (FROM MAINCL WITH W1=0)
C                                                       (CM3 SEC-1)
C      CC PWR   =CASCADE POWER      (DEF.  P/N+)        (ERGS SEC-1)
C      RR PWR   =RAD. RECOM. POWER  (DEF.  P/N+)        (ERGS SEC-1)
C      DR PWR   =DIEL. RECOM. POWER (DEF.  P/N+)        (ERGS SEC-1)
C      BS PWR   =BREMS. POWER       (DEF.  P/N+)        (ERGS SEC-1)
C      CP/NE    =ZERO DENSITY CASC. POWER
C                                   (DEF.  P/(DENS*N+)) (ERGS CM3 SEC-1)
C      THETA    =TE/Z1**2                               (K)
C      RHO      =DENS/Z1**7                             (CM-3)
C      PWR      =TOTAL RECOM.+CASC.+BREMS. POWER
C                                   (DEF.  P/(DENS*N+)) (ERGS CM3 SEC-1)
C
C
C
C  PROPOSED UPDATES:
C
C     (A)  PROPER INCLUSION OF PRTWGHT FOR LOWEST N-SHELL
C
C     (B)  INCLUSION OF FILTER FUNCTION ON RADIATED POWERS
C             FFILT(ENER)  R*8 FUNCTION  - FILTER TRANSPARENCY
C
C     (C)  CALCULATION OF ELECTRON POWER LOSS FUNCTIONS
C
C          QCREP()   R*8    ELECTRON COOLING VECTOR DUE TO THE
C                           COLLISIONAL PROCESSES OF EXCITATION,
C                           DEEXCITATION AND IONISATION.
C                              1ST INDEX - REPRESENTATIVE LEVEL
C          QCREPC()  R*8    QCREP ADJUSTED FOR THE CONDENSED LEVEL SET.
C                              1ST INDEX - REPRESENTATIVE LEVEL
C          QDRREP()  R*8    ELECTRON COOLING VECTOR DUE TO DR
C                              1ST INDEX - REPRESENTATIVE LEVEL
C          QRRREP()  R*8    ELECTRON COOLING VECTOR DUE TO RR
C                              1ST INDEX - REPRESENTATIVE LEVEL
C          QTRREP()  R*8    ELECTRON COOLING VECTOR DUE TO T-B RECOM.
C                              1ST INDEX - REPRESENTATIVE LEVEL
C
C          QCE       R*8    QCREP SUMMED OVER EXCITATION CONTRIBUTIONS
C                           TO POPULATIONS
C          QCR       R*8    QCREP SUMMED OVER RECOMBINATION CONTRIBUTION
C                           TO POPULATIONS
C          QDR       R*8    QDRREP SUMMED OVER ALL N-SHELLS
C          QRR       R*8    QRRREP SUMMED OVER ALL N-SHELLS
C          QTR       R*8    QTRREP SUMMED OVER ALL N-SHELLS
C
C          QELR(,)   R*8    RECOMBINING ELECTRON POWER LOSS COEFFICIENT
C                              1ST INDEX - TEMPERATURE
C                              2ND INDEX - DENSITY
C          QELE(,)   R*8    EXCITING ELELCTRON POWER LOSS COEFFICIENT
C                              1ST INDEX - TEMPERATURE
C                              2ND INDEX - DENSITY
C
C
C     (D)  BINNING OF CASCADE POWER FOR RECOMBINATION SPECTRA
C                    W.J. DICKSON,  M. O'MULLANE    15/10/93
C
C         NBIN   PARAMETER  NUMBER OF ENERGY BINS
C         XENER      R*8    IONISATION ENERGY
C         WEBIN      R*8    WIDTH OF ENERGY BIN
C         IBIN       I*4    INDEX OF ENERGY BIN
C         PREPBIN(,) R*8    BINNED CASCADE RADIATED POWER
C                              1ST INDEX - REPRESENTATIVE LEVEL
C                              2ND INDEX - ENERGY BIN
C         RLBIN()    R*8    BINNED ARRAY OF RL
C                              1ST INDEX - ENERGY BIN
C         RLREPBN(,) R*8    BINNED VERSION OF RLREP
C                              1ST INDEX - REPRESENTATIVE LEVEL
C                              2ND INDEX - ENERGY BIN
C         PCASBIN(,,) R*8   BINNED CASCADE POWER
C                              1ST INDEX - TEMPERATURE
C                              2ND INDEX - DENSITY
C                              3RD INDEX - ENERGY BIN
C
C
C
C
C  PROGRAM VARIABLES:
C
C          (I*4)  I         = GENERAL INDEX
C          (I*4)  I1        = GENERAL INDEX
C          (I*4)  I2        = GENERAL INDEX
C          (I*4)  I3        = GENERAL INDEX
C          (I*4)  IBD       = INDEX IN NREPX(INMAXX) OF PARTICULAR N-SHELL
C                             REFERENCING SAME N-SHELL IN EXTERNAL DR DATA
C          (I*4)  IBIN      = BIN INDEX FOR ENERGY RESOLVED POWER
C          (I*4)  IBSEL     = SELECTOR INDEX IN EXTERNAL ADF07 'SZD'
C                             DATASET
C          (I*4)  IBSEL1    = INPUT - SWITCH TO USE EXTERNAL SOURCE
C                                     FOR IONIS. RATE COEFFTS. FROM
C                                     LOWEST LEVEL OF EACH SPIN SYSTEM
C                                     <= 0  SET LIOSEL = .FALSE.
C                                     >  0  SET LIOSEL = .TRUE.
C          (I*4)  ICALC     = POPULATION CALCULATION PASS COUNTER
C                             1=> POP. CALC. FOR RECOM & CASC. POWER
C                                 ( EXTENDED REPRESENT. N-SHELL SET)
C                             2=> POP. CALC. FOR IONIS.
C                                 ( CONTIGUOUS FIRST 30 N-SHELLS)
C          (I*4)  ICTPR     = INPUT - N-CUT FOR PARENT-GROUND STATE CALC.
C          (I*4)  IDCN      = GENERAL INDEX
C          (I*4)  IDEF      = GENERAL INDEX FOR QUANTUM DEFECTS
C          (I*4)  IEDMAT    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IEDMAT    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IECION    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IETREC    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IEDREC    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IERREC    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IEXREC    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IERSYS    = SWITCH PARAMETER FOR B4PROJ
C                              0=> LEAVE DIRECT PART IN ??? MATRIX
C                              1=> SUBTRACT DIRECT PART FROM ??? MATRIX
C          (I*4)  IGRD      = INPUT - GROUND STATE INDEX OF CALCULATION
C          (I*4)  IIGRP     = GENERAL INDEX FOR IONISATION SHELL GROUPS
C          (I*4)  ILOW      = INPUT - **** NOT USED IN THIS CODE ****
C          (I*4)  IMARK     = 20*(J-1)+1 = LOCATOR FOR BETHE CORRECTIONS
C                                          FOR INTERNAL DR TRANSITION J.
C          (I*4)  IMAX      = NO. OF REPRESENT. N-SHELLS FOR PARTICULAR
C                             PARENT/SPIN SYSTEM CALC.
C          (I*4)  IMAX1     = SET TO IMAX IN INTERMEDIATE CALCULATION
C          (I*4)  IMAXC1    = NUMBER OF REPRESENTATIVE N-SHELLS IN
C                             CALCULATION WITH LEVEL CONDENSATION.
C          (I*4)  IMAXC2    = NUMBER OF REPRESENTATIVE N-SHELLS IN
C                             CALCULATION WITHOUT LEVEL CONDENSATION.
C          (I*4)  IMAXRF    = INPUT - REFERENCE NO. OF REPRESENT. N-SHELLS
C          (I*4)  METIS     = OUTPUT FROM B4SSZD.
C                             INITIAL STATE METASTABLE INDEX
C          (I*4)  METFS     = OUTPUT FROM B4SSZD.
C                             FINAL STATE METASTABLE INDEX
C          (I*4)  INMAXX    = NO. OF DIFFERENT REPRESENTATIVE N-SHELLS
C                             USED IN POPULATION CALCULATIONS. THIS IS
C                             THE AUGMENTED SET AS REQUIRED BOTH FOR THE
C                             MAIN COLL. RAD. CALCS. AND THE SHORT IONIS.
C                             CALCS.  INITIALISED AND SENT TO B4DATD TO
C                             FETCH EXTERNAL DR DATA FROM FILES.
C          (I*4)  INTD      = INPUT -  <3 USE 2 POINT QUADRATURE FOR PY
C                                      =3 USE 3 POINT QUADRATURE FOR PY
C                                      >3 USE 4 POINT QUADRATURE FOR PY
C          (I*4)  IONE      = INTEGER 1.
C          (I*4)  IPRS      = INPUT - <=0 USE VAN REGEMORTER EXCIT. RATES
C          (I*4)  IPRT      = INPUT - PARENT INDEX OF CALCULATION
C          (I*4)  IPRTCAL   = VARIABLE FOR CALL TO B4PROJ.
C                             PARENT INDEX FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  IR        = GENERAL REPRESENTATIVE N-SHELL INDEX
C          (I*4)  IRCODE    = RETURN CODE FROM B4SSZD. SEE SUBROUTINE
C                             DOCUMENTAT.
C          (I*4)  IREP      = GENERAL INDEX FOR REPRESENTATIVE LEVELS
C          (I*4)  IRGRP     = GENERAL INDEX FOR EXCIT/IONIS. GROUPS
C          (I*4)  IRESO     = GENERAL INDEX FOR EXCIT/IONIS. RESONANCES
C          (I*4)  ISHEL     = GENERAL INDEX FOR IONISATION SHELLS
C          (I*4)  ISPN      = INPUT - SPIN OF GROUND STATE
C          (I*4)  ISPR      = INPUT - PARENT SPIN
C          (I*4)  IUNT08    = PARAMETER = OUTPUT UNIT FOR POPULATION RESULTS
C          (I*4)  IUNT09    = PARAMETER = OUTPUT UNIT FOR SUPPLEMENTARY TEXT
C          (I*4)  IUNT17    = PARAMETER = OUTPUT UNIT FOR TEXT RESULTS
C          (I*4)  IUNT18    = PARAMETER = OUTPUT UNIT FOR PROJ. MATRICES -
C                                         CONDENSED READABLE TEXT FORM
C          (I*4)  IUNT19    = PARAMETER = OUTPUT UNIT FOR PROJ. MATRICES -
C                                         (ADF17)
C          (I*4)  IUNT22    = PARAMETER = INPUT UNIT FOR PRIMARY DATA
C                                         (ADF25)
C
C          (I*4)  ISYS      = GENERAL INDEX FOR SPIN SYSTEMS
C                                      >0 USE PERCIVAL/RICHARDS RATES
C          (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  ITT       = 0.5*(JTEM+1.001)
C          (I*4)  ITRANS    = GENERAL INDEX FOR DR CORE TRANSITIONS
C          (I*4)  ITVAL     = INPUT TO B4SSZD. SET EQUAL TO JTEM
C          (I*4)  IX        = GENERAL INDEX
C          (I*4)  IZ        = INTEGER FORM OF Z
C          (I*4)  IZ0IN     = INPUT TO B4SSZD. SET EQUAL TO NUCCHG
C          (I*4)  IZS       = OUTPUT FROM B4SSZD.
C                             IONISING ION - INITIAL CHARGE OF SELECTED BLOCK
C          (I*4)  IZ1S      = OUTPUT FROM B4SSZD.
C                             IONISING ION - FINAL CHARGE OF SELECTED BLOCK
C          (I*4)  J         = GENERAL INDEX
C          (I*4)  JCOR      = JCORA FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  JDEF      = JDEFSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  JDENS     = GENERAL INDEX FOR DENSITY
C          (I*4)  JDENSM    = INPUT - NUMBER OF ELECTRON DENSITIES
C          (I*4)  JPRT      = GENERAL INDEX FOR PARENT STATES
C          (I*4)  JSTEM     = ** SPECIAL TEMP. INDEX - NOT USED **
C          (I*4)  JTE       = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  JTEM      = INPUT - NUMBER OF ELECTRON TEMPERATURES
C          (I*4)  K         = GENERAL INDEX
C          (I*4)  KPRT      = GENERAL INDEX FOR PARENT STATES
C          (I*4)  L         =
C          (I*4)  LEN1      = GENERAL STRING LENGTH
C          (I*4)  LEN2      = GENERAL STRING LENGTH
C          (I*4)  LOGIC     = SWITCH SIGNAL FROM IDL FOR FOREGROUND
C                             OR BACKGROUND EXECUTION
C                             0=> FOREGROUND ; 1=> BACKGROUND
C          (I*4)  METIS     = INITIAL METASTABLE INDEX
C          (I*4)  METFS     = FINAL METASTABLE INDEX
C          (I*4)  MONE      = INTEGER -1.
C          (I*4)  MTWO      = INTEGER -2.
C          (I*4)  N         = GENERAL N-SHELL
C          (I*4)  N1        = GENERAL N-SHELL
C          (I*4)  N11       = GENERAL N-SHELL
C          (I*4)  N2        = N11-N
C          (I*4)  N3        = N-N1
C          (I*4)  NBFIL     = NO. OF EXTERNAL DR FILES FOUND- OUTPUT
C                             FROM B4DATD (BDMNCL1)
C          (I*4)  NBIN      = PARAMETER = MAX. NUMBER OF BINS FOR SPECTRAL
C                                         DISTRIBUTION OF POWER.
C          (I*4)  NCALC     = 2 = REPEATS OF SAME POPULATION CALCULATION
C                                 WITH DIFFERENT REPRESENT. N-SHELL SETS
C                                 FOR (1) RECOM, CASCADE POWER (2) IONIS.
C          (I*4)  NCUT      = CUT-OFF N-SHELL IN CURRENT CALC.
C          (I*4)  NDBFILM   = PARAMETER = MAX. NUMBER OF BADNELL DR FILES
C          (I*4)  NDDIM     = PARAMETER = MAX. NUMBER OF DENSITIES
C          (I*4)  NDPRTBD   = PARAMETER = MAX. NUMBER OF PARENTS ALLOWED
C                                         IN BADNELL DR DATA SETS
C          (I*4)  NGRDIM    = PARAMETER = MAX. NUMBER OF GROUND STATES
C          (I*4)  NGRSTK    = MAX. NO. OF GROUND STATES IN CALC.
C          (I*4)  NIGRP     = NIGSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  NIP       = INPUT - RANGE OF DELTA N FOR WHICH
C                                   IP RATE COEFFICIENTS ARE USED
C          (I*4)  NL        = *** GENERAL N-SHELL  ? UNUSED  ? ***
C          (I*4)  NLWS      = PARAMETER = MIN. N-SHELL FOR CN REPRESENT.
C          (I*4)  NMAX      = HIGHEST N-SHELL IN CURRENT CALC.
C          (I*4)  NMAX1     = NMAX+NIP
C          (I*4)  NMAXC1    = MAXIMUM REPRESENTATIVE N-SHELL IN
C                             CALCULATION WITH LEVEL CONDENSATION.
C                             SET EQUAL TO NMAXRF
C          (I*4)  NMAXC2    = PARAMETER = MAX. N-SHELL IN CALC. WITH
C                                         NO INTERPOLATION
C          (I*4)  NMAXRF    = INPUT - REFERENCE HIGHEST REPRESENT. N-SHELL
C          (I*4)  NMIN      = NMINSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  NMINRF    = INPUT - REFERENCE LOWEST REPRESENT. N-SHELL
C          (I*4)  NN        = NO. OF CALC. LOOPS EXPECTED, SIGNAL TO IDL
C          (I*4)  N0        = N0STK   FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  NPARNT    = NAMELIST - NUMBER OF PARENTS IN CALCULATION
C          (I*4)  NPATH     = NO. OF PARENT/SPIN SYSTEM CALC. PATHWAYS
C          (I*4)  NPRDIM    = PARAMETER = MAX. NUMBER OF PARENTS
C          (I*4)  NPRSTK    = MAX. NO. OF PARENTS IN CALC.
C          (I*4)  NREPDM    = PARAMETER = MAX. NUMBER OF REPRESENT. LEVELS
C          (I*4)  NRESU     = PARAMETER = MAX. N-SHELL FOR PROJECT. CALC.
C          (I*4)  NRGRP     = NRGSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  NSHEL     = NUMBER OF PROJECTED N-SHELLS
C          (I*4)  NSYDIM    = PARAMETER = MAX. NUMBER OF SPIN SYSTEMS
C          (I*4)  NTDIM     = PARAMETER = MAX. NUMBER OF TEMPERATURES
C          (I*4)  NTRANS    = NTRSTK  FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (I*4)  NTEST     = GENERAL N-SHELL USED IN EXTERNAL DR MAPPING
C          (I*4)  NTST      = GENERAL TEST N-SHELL
C          (I*4)  NUCCHG    = NAMELIST - NUCLEAR CHARGE OF ELEMENT
C                                        ( < 0 TERMINATES THE CALCULATION)
C          (I*4)  PIPEIN    = PARAMETER = UNIT FOR READ FROM UNIX PIPE
C          (I*4)  PIPEOU    = PARAMETER = UNIT FOR WRITE TO UNIX PIPE
C
C
C          (R*8)  A1        = COMMON FACTOR OF HYDROGENIC DIPOLE REACTIONS
C                             CONTAINING BOUND-BOUND GAUNT FACTOR
C          (R*8)  A2        = INDUCED RAD. PART OF EXCIT/DEEXCIT RATE
C                             EXCLUDING A1
C          (R*8)  A3        = COLLISIONAL PART OF EXCIT/DEEXCIT RATE
C                             EXCLUDING A1
C          (R*8)  AAG       = SUMMED AUGER TRANSITION PROBABILITY (S-1)
C          (R*8)  AAGFAC    = SUMMED AUGER TRANS. PROB. SCALED WITH
C                               N**2*EXP(IN/KTE)/1.57456D10 FOR N>=NWLS
C                               N**2/1.57456D10             FOR N< NWLS
C          (R*8)  AAGMUL    = MULTIPLIER USED FOR AUGER TRANS. PROBS.
C                               EXP(IN/KTE)    FOR N>=NWLS
C                               1              FOR N< NWLS
C          (R*8)  ACNST     = CONSTANT FOR CALL TO B4PROJ
C          (R*8)  ADIR      =
C          (R*8)  ADIREX    =
C          (R*8)  AD        = ACCUMULATOR OF DR PARTS
C          (R*8)  ADS       = PART OF DR RATE (ONE CORE TRANSITION)
C          (R*8)  AI        = PART OF IONISATION RATE
C          (R*8)  ADJUST    =
C          (R*8)  ADSGP     = PART OF GP DR RATE (ONE CORE TRANSITION)
C          (R*8)  AION      = DIAGONAL DIRECT LOSS PART OF FIRST ON-DIAG
C                             ELEMENT OF COLL-RAD MATRIX
C          (R*8)  ALFA      = COLLISIONAL RADIATIVE RECOMBINATION COEFFT.
C          (R*8)  AL0C1     = SSYSWT*AL0
C          (R*8)  AL0       = TOTAL ZERO DENSITY RECOMBINATION COEFFT.
C                             OBTAINED BY EXPANDING AL0REP() FOR
C                             REPRESENT. N-SHELLS OVER ALL N-SHELLS AND
C                             SUMMING.
C          (R*8)  APION     = PHOTOIONISATION CONTRIB. TO DIAGONAL
C                             VECTOR OF COLL. RAD. MATRIX A() SCALED WITH
C                               N**2*EXP(IN/KTE)/1.57456D10 FOR N>=NWLS
C                               N**2/1.57456D10             FOR N< NWLS
C          (R*8)  AREC      =
C          (R*8)  A1CNST    = CONSTANT FOR CALL TO B4PROJ
C          (R*8)  ATE       = 157890.0*Z*Z/TE
C          (R*8)  ATOT      =
C          (R*8)  ATS       = 157890.0*Z*Z/TS
C                             (EQUAVALENT TO  Z**2/(TS/RH) )
C          (R*8)  BMENER    = INPUT - NEUTRAL HYDROGEN BEAM (EV/AMU)
C          (R*8)  BWNOS     = IONISATION POTENTIAL (CM-1) FROM B4SSZD
C                             OF SELECTED DATA BLOCK
C          (R*8)  C         = 6.60074E-24*DENS*SSYSWT*(157890.0/TE)**1.5
C          (R*8)  C1REC     = 1.03928E-13 * (Z**4) * ((157890.0/TE)**1.5)
C          (R*8)  C2ION     = 1.5744D10*Z**4 / (DENS*NREP(1)*NREP(1))
C          (R*8)  CFAC      = CORFA  FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  CION      = INPUT - SCALING FACTOR FOR ECIP IONISATION
C                                     RATE COEFF
C          (R*8)  CIONPTS   =
C          (R*8)  CN1       = TEMPORARY CN VALUE WHEN OBTAINING DELTA(CN)
C          (R*8)  CN2       = TEMPORARY CN VALUE WHEN OBTAINING DELTA(CN)
C          (R*8)  CN3       = TEMPORARY CN VALUE WHEN OBTAINING DELTA(CN)
C          (R*8)  CPL       = 2.26530D-24*DENS*(157890.0/TE)**1.5*Z**6
C          (R*8)  CPT       = *** DR L-SHELL COMPONENT - NOT USED ***
C          (R*8)  CPY       = INPUT - SCALING FACTOR FOR IP EXCITATION
C                                     RATE COEFF
C          (R*8)  COR       = BETHE CORRECTIONS IN INTERNAL DR CALC.
C          (R*8)  COUT      =
C          (R*8)  D         = (1/N**2 - 1/N'**2)
C          (R*8)  D1        = 1.0/(D*D)
C          (R*8)  DEF       = CURRENT QUANTUM DEFECT
C          (R*8)  DEFN      = N-V
C          (R*8)  DELE      = SIGNED D FOR ELECTRON ENERGY LOSS CALC.
C          (R*8)  DENS      = CURRENT ELECTRON DENSITY (CM-3)
C          (R*8)  DENSH     = INPUT - NEUTRAL HYDROGEN BEAM DENSITY (CM-3)
C          (R*8)  DENSP     = CURRENT PROTON DENSITY (CM-3)
C          (R*8)  DETERM    = ** OUTPUR PARM FROM B4MATV - NOT USED **
C          (R*8)  DF        = DFA  FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  DRMC      = GENERAL VARIABLE ASSIGNED TO DR COEFFTS.
C          (R*8)  EDISGP    = EGPSTK  FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  EDISP     = EDISTK  FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  EI        = ENERGY OF LEVEL I (RYD) ?
C          (R*8)  EIJ       = ENERGY DIFF. BETWEEN LEVELS I AND J (RYD)
C          (R*8)  EIONR     = IONIS. POT (RYD) FOR RAD. RECOMB.
C                             FOR PARTICULAR PARENT/SPIN SYSTEM CALC.
C          (R*8)  EJ        = ENERGY OF LEVEL J (RYD) ?
C          (R*8)  EN        = REAL FORM OF N-SHELL N
C          (R*8)  EN1       = REAL FORM OF N-SHELL N1
C          (R*8)  EN11      = REAL FORM OF N-SHELL N11
C          (R*8)  ENC       = 1.0/DSQRT(EN2(N)-ENMAX2)
C          (R*8)  ENMAX2    = 1.0 / ((NMAX+0.5)*(NMAX+0.5))
C          (R*8)  ESECIP    = EXPTE(N)*SECIP
C          (R*8)  ESGRD     = EXP(1.4338*BWNOM(,)/TEA())*SGRD
C          (R*8)  EXPON     = GENERAL VARIABLE ASSIGNED TO EXPONENTIAL
C                             ENERGY/KTE FACTORS
C          (R*8)  EYE       = DEXP(ATE*ENMAX2)
C          (R*8)  F         =1.96028D0*G*D*D*D*EN3(N)*EN2(N)*EN3(N11) =
C                            HYDROGEN OSCILLATOR STRENGTH
C          (R*8)  FACTOR    = 1.0
C          (R*8)  FGP       = SCALGP*DEXP(ATE*EDISGP/(Z**2))
C          (R*8)  FINTER    = FUNCTION = INTERPOLATION VARIATE
C          (R*8)  FLUX      = NEUTRAL BEAM HYDROGEN FLUX (CM2 S-1)
C          (R*8)  G         = BOUND-BOUND HYDROGENIC GAUNT FACTOR GI(N,N')
C          (R*8)  GBB       = FUNCTION = BOUND BOUND GAUNT FACTOR
C          (R*8)  NGFFMH    = FUNCTION = FREE FREE GAUNT FACTOR
C          (R*8)  PHF       = GENERAL VARIABLE ASSIGNED TO PHFRA
C          (R*8)  PHFRAC    = PHFSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  PHI       = SEATON'S PHI FACTOR = F*D/(Z*Z)
C          (R*8)  PION      = PHOTOIONISATION INTEGRAL
C          (R*8)  PREC      = RADIATIVE RECOMBINATION INTEGRAL
C          (R*8)  PSTIM     = STIMULATED RECOMBINATION INTEGRAL
C          (R*8)  PY        = P-FACTOR FOR ELECTRONS
C          (R*8)  PYP       = P-FACTOR FOR IONS
C          (R*8)  PY0       = -DLOG(ATE)-0.41
C          (R*8)  QCR       =
C          (R*8)  QCRC1     =
C          (R*8)  QCT       =
C          (R*8)  QRD       =
C          (R*8)  QRR       =
C          (R*8)  QRRC1     =
C          (R*8)  QRREC     =
C          (R*8)  QRT       =
C          (R*8)  QRTC1     =
C          (R*8)  QRX3      =
C          (R*8)  R         =
C          (R*8)  RA        =
C          (R*8)  RAD       =
C          (R*8)  RB        =
C          (R*8)  RBC1      =
C          (R*8)  RD        =
C          (R*8)  RDC1      =
C          (R*8)  RHDIR     =
C          (R*8)  RDEXC     =
C          (R*8)  RHO       = 1.27787E-17/Z**7*DENS*DSQRT(ATE)
C          (R*8)  RHOP      = 1.27787E-17/Z**7*DENSP*DSQRT(X/TP)
C          (R*8)  RHSD      =
C          (R*8)  RL        =
C          (R*8)  RL0       =
C          (R*8)  RLC1      =
C          (R*8)  RL0C1     =
C          (R*8)  RR        =
C          (R*8)  RRC1      =
C          (R*8)  RREC      =
C          (R*8)  RRN       =
C          (R*8)  RX3       =
C          (R*8)  S         =
C          (R*8)  SCALE     = SGASTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  SCALGP    = SGPSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  SD        =
C          (R*8)  SDIR      =
C          (R*8)  SECIP     = ECIP IONISATION RATE COEFFICIENT (CM3 S-1)
C          (R*8)  SGRD      = GROUND STATE IONIS. RATE COEFFT. (CM-3 S-1)
C          (R*8)  SSYSWT    = SPIN SYSTEM WEIGHT FRACTION FOR CURRENT
C                             PARENT/SPIN SYSTEM CALC.
C          (R*8)  STOT      =
C          (R*8)  SUM1      =
C          (R*8)  SUM2      =
C          (R*8)  SUM3      =
C          (R*8)  SUMION    =
C          (R*8)  TE        = ELECTRON TEMPERATURE (K)
C          (R*8)  TP        = PROTON TEMPERATURE (K)
C          (R*8)  TS        = INPUT - RADIATION FIELD TEMPERATURE (K)
C          (R*8)  V0        = V0STK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C          (R*8)  W         = INPUT - GENERAL RADIATION DILUTION FACTOR
C          (R*8)  W1        = INPUT - GROUND STATE RADIATION DILUTION
C                                     FACTOR
C          (R*8)  WEBIN     = EN2(NMIN) / NBIN
C          (R*8)  WEIJJ     =
C          (R*8)  WI        = STATISTICAL WEIGHT OF STATE I (=2*NI**2)
C          (R*8)  WJ        = STATISTICAL WEIGHT OF STATE J (=2*NJ**2)
C          (R*8)  X         =
C          (R*8)  X1        = GENERAL VARIABLE
C          (R*8)  X2        = GENERAL VARIABLE
C          (R*8)  X3        = GENERAL VARIABLE
C          (R*8)  X4        = ** CX CONTRIB. TO RECOM - NOU USED **
C          (R*8)  XL        = L QUANTUM NUMEBR (NON-INTEGER)
C          (R*8)  Y         = ATE*D
C          (R*8)  YE        = ATE*(EN2(N)-ENMAX2)
C          (R*8)  YS        = ATS*D
C          (R*8)  Z         = INPUT - RECOMBINING ION CHARGE
C          (R*8)  Z1        = Z
C          (R*8)  ZCOL      = Z-1.0
C
C
C          (C*80) DASHES    = CHARACTER STRING OF DASH SYMBOLS
C          (C*10) DATE      = CURRENT DATE
C          (C*120)DSNIN     = INPUT FILE NAME (FROM IDL)
C          (C*120)DSNOUT    = GENERAL STRING FOR OUTPUT FILE NAMES
C          (C*80) PASSDIR   = DIRECTORY FOR PASSING FILES (FROM IDL)
C          (C*120)DSNXRT    = FIRST PART OF CROSS REFERENCE FILE NAME
C                             (FROM IDL)
C          (C*120)ioniz     = adf07 better ionisation data filename
C          (C*120)drsup     = adf09 better DR data filename
C          (C*80) ADAS_C    = CENTRAL ADAS LOCATION
C          (C*80) ADAS_U    = USER ADAS LOCATION
C          (C*80) EQUALS    = CHARACTER STRING OF EQUALS SYMBOLS
C          (C*3)  REP       = 'YES'=> CANCEL SELECTED ON IDL INPUT SCREEN
C			      'NO' => NOT SELECTED
C          (C*2)  SEQ       = NAMELIST - RECOMBINED ION ISO-ELECT. SEQ.
C          (C*120)TITLX     = OUTPUT FROM B4SSZD. INFORMATION STRING
C          (C*2)  TRMPRT    = INPUT - PARENT TERM CHARACTER
C          (C*80) USERID    = SOURCE DATA USER ID (CENTRAL ADAS OR USER)
C                             FOR USE IN XXUID
C          (C*8)  XRMEMB    = NAMELIST - REF. MEMBER NAME FOR EXTERNAL
C                                        DR DATA, IF NON-BLANK OTHERWISE
C                                        USE INTERNAL DR CALCULATION.
C
C
C          (L*4)  LBATCH    = .TRUE. =>  PROGRAM IS BEING RUN AS A UNIX
C                                        BATCH JOB:INFO OUTPUT DOWN THE
C                                        PIPE IS SUPRESSED
C                             .FALSE.=>  PROGRAM IS BEING RUN WITH THE IDL
C			                 INTERFACE
C          (L*4)  LBTSEL    = .TRUE. => THE 'RUN IN BATCH' OPTION WAS
C                                       SELECTED ON THE IDL INTERFACE: NO
C                                       CALCULATION IS DONE SINCE IDL SETS
C                                       UP A JOB TO RUN LATER.
C                             .FALSE.=> THE RUN NOW OPTION WAS SELECTED:
C                                       CALCULATION PROCEEDS
C          (L*4)  LBIN      = .TRUE. => CALC. BINNED RADIATED POWER
C                             .FALSE.=> DO NOT CALC. BINNED RADIATED POWER
C          (L*4)  LDRSEL    = .TRUE. => READ IN EXTERNAL DR COFFTS.
C                             .FALSE.=> DO NOT READ EXTERNAL DR DATA
C          (L*4)  LECOOL    = .TRUE. => CALC. COOLING RATE COEFFTS.
C                             .FALSE.=> DO NOT CALC. COOLING RATES
C          (L*4)  LFILT     = .TRUE. =>  *** NOT USED AT PRESENT ***
C                             .FALSE.=>  *** NOT USED AT PRESENT ***
C          (L*4)  LPROJ     = .TRUE. => CALC. LOW N-SHELL PROJ. MATRICES
C                             .FALSE.=> DO NOT CALC. PROJ. MATRICES
C          (L*4)  LRESOL    = .TRUE. => PARENT/SPIN SYSTEM RESOLVED CASE
C                             .FALSE.=> UNRESOLVED CASE
C
C
C          (I*4)  IGRSTK()  = PARENT/SPIN SYSTEM ARRAY OF IGRD VALUES
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (I*4)  INTER()   =
C          (I*4)  ISZD(,,)  = INPUT - SELECTOR INDEX FOR EXTERNAL SOURCE
C                                     IONIS. RATES FROM SZD (ADF07) FILE
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C                                     3rd DIM.: FINAL PARENT INDEX
C          (I*4)  ITYPEA()  = ITYSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (I*4)  ITYSTK(,,)= INPUT - DR CORE TRANSITION TYPE
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (I*4)  IZETA(,)  = IZESTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                            1ST INDEX - IONISATION SHELL
C                            2ND INDEX - IONISATION GROUP
C          (I*4)  IZESTK(,,,)=INPUT - NO. OF EQUIV. ELECS. IN IONIS.
C                                     SHELL/GROUP
C                                     1st DIM.: SHELL INDEX
C                                     2nd DIM.: IONISATION GROUP INDEX
C                                     3rd DIM.: PARENT INDEX
C                                     4th DIM.: SPIN SYSTEM INDEX
C          (I*4)  JCORA()   = NUMBER OF BETHE CORRECTIONS IN DR CALC.
C                             1st DIM.: NUMBER OF CORE TRANSITION TYPES
C          (I*4)  JDEFSTK(,)= INPUT -  NO. OF QU. DEFS. FOR LOWEST
C                                      N-SHELLS STARTING AT N=NMINSTK
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (I*4)  N0STK(,)  = INPUT - LOWEST ACCESSIBLE LEVEL FOR RAD.
C                                     RECOMB.
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (I*4)  N1A()     = N1ASTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (I*4)  N1ASTK(,,)= INPUT - LOWSET ACCESSIBLE LEVEL FOR DR
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NCTAA(,,) = INPUT - NCUT FOR ALT. AUG. CHAN. DURING DR
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NCTA()    = NCTAA FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C          (I*4)  NCTSTK(,) = PARENT/GROUND STATE ARRAY OF ICTPR
C                             1ST INDEX - PARENT
C                             2ND INDEX - GROUND STATE
C          (I*4)  NCUTMC(,) = N-SHELL CUT FOR AUGER RATES (AUGER CHANNEL
C                             OPENS AT NCUTMC+1). RETURNED FROM B4DATD.
C                                1ST. DIM.: = INITIAL PARENT INDEX
C                                2ND. DIM.: = FINAL PARENT INDEX
C          (I*4)  NIGSTK(,) = INPUT - NO. OF IONIS. SHELL GROUPS
C                                     (AT MOST 2)
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NMINSTK(,)= INPUT - MIN. QU. SHELL FOR POP. CALC.
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NOW()     = RETURN OF DATE FROM SUNROUTINE DATIM
C          (I*4)  NREP()    = SET OF REPRESENTAIVE N-SHELLS USED DURING
C                             1ST INDEX - IREP
C          (I*4)  NREP1()   =
C          (I*4)  NREPC()   = VARIABLE FOR CALL TO B4PROJ
C                             SET OF REPRESENTATIVE LEVELS USED
C                             DURING CALCULATION WITH LEVEL CONDENSATION
C                             1ST INDEX - IREP
C          (I*4)  NREPI()   = VARIABLE FOR CALL TO B4PROJ
C                             SET OF REPRESENTATIVE LEVELS USED DURING
C                             CALCULATION WITHOUT LEVEL CONDENSATION
C                             1ST INDEX - IREP
C          (I*4)  NREPRF()  = INPUT - REFERENCE REPRESENTATIVE N-SHELLS
C                                     1st. DIM.: REPRESENT. SHELL INDEX
C          (I*4)  NREPX()   = AUGMENTED REPRESENTATIVE LEVEL SET.
C                             INITIALISED AND SENT TO B4DATD TO FETHC
C                             EXTERNAL DR DATA FROM FILE.
C                             1st DIM.: REPRESENTATIVE LEVEL INDEX
C          (I*4)  NRESOA()  = NRESTK FOR PARTICULAR PARENT SPIN SYSTEM
C                             GROUP
C                             1ST INDEX - EXCITATION-IONISATION GROUP
C          (I*4)  NRESTK(,,) =INPUT - NO. OF RESONS. IN EXCIT-IONIS. GROUP
C                                     1st DIM.: EXCIT./IONIS. GROUP INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NRGSTK(,) = INPUT - NO. OF EXCITATION-IONISATION
C                                     SHELL GROUPS (AT MOST 2)
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NSHELA()  = NSHELA FOR PARTICULAR PARENT SPIN SYSTEM
C                             GROUP
C                             1ST INDEX - IONISATION GROUP
C          (I*4)  NSHSTK(,,)= INPUT - NO. OF SHELLS IN IONIS. GROUP
C                                     1st DIM.: IONISATION GROUP INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (I*4)  NSYSTK()  = NO OF SPIN SYSTEM PATHS FOR EACH PARENT.
C                             1st DIM.: PARENT INDEX
C          (I*4)  NTRSTK(,) = INPUT - NO. OF DR CORE TRANSITIONS FOR
C                                     INTERNAL CALCULATION
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C
C
C
C          (R*8)  A()        = TEMPORARY STORE OF COLUMN OF CIJ MATRIX
C                             FOR NJ
C                             1st DIM.: NI INDEX
C          (R*8)  AC()      =
C                             1st DIM.: NI INDEX
C          (R*8)  ACI()     =
C          (R*8)  AEXSTK(,,)= INPUT - EXACT VALUE OF ZERO DENSITY RECOMB.
C                                     RATE COEFFT.
C                                     1ST DIM.: ELECTRON TEMP. INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  AGRL()    =
C          (R*8)  ALFAA(,)  = COLL.-DIEL. RECOMB. RATE COEFF.
C                             1st DIM.: ELECTRON TEMPERATURE INDEX
C                             2nd DIM.: ELECTRON DENSITY INDEX
C          (R*8)  ALFEX()   = AEXSTK FOR PARTICULAR PARENT/SPIN
C                             SYSTEM CALCULATION
C                             1st DIM.: ELECTRON TEMPERATURE INDEX
C          (R*8)  ALF0      = ZERO DENSITY RECOMBINATION COEFFICIENT
C                             CALCULATED BY SUMMING AL0REP OVER
C                             REPRESENTATIVE LEVELS.
C                             1st DIM.: ELECTRON TEMPERATURE INDEX
C          (R*8)  AL0REP()  = TOTAL ZERO DENSITY RECOMBINATION COEFFT.
C                             AS OBTAINED FROM STATE RESOLVED DATA IN
C                             CALCULATION
C                             1st DIM.: REPRESENTATIVE LEVE INDEX
C          (R*8)  ALOSS     =
C          (R*8)  ALOSSA(,) = RESOLVED AUTOION. CONTRIB. TO DIAGONONAL
C                             VECTOR OF COLL. RAD. MATRIX A() SCALED WITH
C                               N**2*EXP(IN/KTE)/1.57456D10 FOR N>=NWLS
C                               N**2/1.57456D10             FOR N< NWLS
C                             1ST DIM.: FINAL PARENT INDEX
C                             2ND DIM.: REPRESENTATIVE LEVEL INDEX
C          (R*8)  ALOSSC    =
C          (R*8)  ALOSSI(,) = RESOLVED DIRECT IONIS. CONTRIB. TO DIAG.
C                             VECTOR OF COLL. RAD. MATRIX A() SCALED WITH
C                               N**2*EXP(IN/KTE)/1.57456D10 FOR N>=NWLS
C                               N**2/1.57456D10             FOR N< NWLS
C                             1ST DIM.: FINAL PARENT INDEX
C                             2ND DIM.: REPRESENTATIVE LEVEL INDEX
C
C          (R*8)  ALOSSR(,) = ALOSSA(,)+ALOSSI(,)
C          (R*8)  ALOSST    =
C          (R*8)  APRT()    =
C          (R*8)  ARED(,)   =
C          (R*8)  AREDC(,)  = VARIABLE FOR CALL TO B4PROJ
C                             CONDENSED COLLISIONAL RADIATIVE MATRIX FOR
C                             CALCULATION WITH MATRIX CONDENSATION
C                             (CN SOLUTION)
C                             (EXCLUDES AUTOIONISATION FOR LEVELS LE NRESU)
C                             1ST INDEX - REPRESENTATIVE LEVEL
C                             2ND INDEX - REPRESENTATIVE LEVEL
C          (R*8)  AREDI(,)  = VARIABLE FOR CALL TO B4PROJ
C                             COLLISIONAL RADIATIVE MATRIX FOR CALCULATION
C                             WITHOUT MATRIX CONDENSATION (CN SOLUTION)
C                             (EXCLUDES AUTOIONISATION FOR LEVELS LE NRESU)
C                             1ST INDEX - REPRESENTATIVE LEVEL
C                             2ND INDEX - REPRESENTATIVE LEVEL
C          (R*8)  AREDJK    =
C          (R*8)  AREDN     =
C          (R*8)  ATOTR     =
C          (R*8)  ARES(,,,) = TOTAL RESOLVED COLLISIONAL DIELECTRONIC
C                              RECOMBINATION COEFFICIENT (CM3 S-1)
C                                1st DIM.: INITIAL PARENT INDEX
C                                2nd DIM.: FINAL GROUND STATE INDEX
C                                3rd DIM.: ELECTRON TEMPERATURE INDEX
C                                4th DIM.: ELECTRON DENSITY INDEX
C          (R*8)  AUGM(,,,) = PARENT/SPIN SYSTEM RESOLVED AUGER RATES
C                             READ FROM EXTERNAL DR FILE IN SUBROUTINE
C                             B4DATD (SEC-1).
C                                1st DIM.: REPRESENT. LEVEL INDEX (NREPX)
C                                2nd DIM.: INITIAL PARENT INDEX
C                                3rd DIM.: INITIAL SPIN SYSTEM INDEX
C                                4th DIM.: FINAL PARENT INDEX
C          (R*8)  BREP()    = B-FACTOR FOR REPRESENTATIVE N-SHELL
C                             1ST INDEX - IREP
C          (R*8)  BWNOM(,)  = STACKED LOWEST IONISATION POTENTIALS (CM-1)
C                             FOR GIVEN INITIAL PARENT/GROUND PAIR,
C                             DEDUCED FROM BWNOSA (WITH DEFAULTS)
C                             1ST. DIM.: INITIAL PARENT INDEX
C                             2ND. DIM.: INITIAL GROUND INDEX
C          (R*8)  BWNOSA(,,)= STACKED IONISATION POTENTIALS (CM-1)FROM
C                             B4SSXD
C                             1ST. DIM.: INITIAL PARENT INDEX
C                             2ND. DIM.: INITIAL GROUND INDEX
C                             3RD. DIM.: FINAL PARENT INDEX
C          (R*8)  CI()      = CISTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALC.
C                             1ST INDEX -  IONISATION GROUP
C          (R*8)  CIONPT()  = VARIABLE FOR CALL TO B4PROJ.
C                             COLLISIONAL IONISATION CONTRIBUTION TO AREDC
C                             1ST INDEX - REPRESENTATIVE LEVEL
C          (R*8)  CIONRA(,) = VARIABLE FOR CALL TO B4PROJ.
C                             AUTO-IONISATION CONTRIBUTION TO AREDI
C                             1ST INDEX - PARENT
C                             2ND INDEX - REPRESENTATIVE LEVEL
C          (R*8)  CIONRI(,) = VARIABLE FOR CALL TO B4PROJ.
C                             COLLISIONAL IONISATION CONTRIBUTION TO AREDI
C                             1ST INDEX - PARENT
C                             2ND INDEX - REPRESENTATIVE LEVEL
C          (R*8)  CISTK(,,) = INPUT - SCALING FACTOR FOR APPROX. FORM
C                                     FOR INTERNAL IONISATION RATE
C                                     COEFFT. CALCULATION
C                                     1st DIM.: IONISATION GROUP INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  CORA(,)   = BETHE CORRECTIONS IN DR CALC.
C                             1ST DIM.: BETHE CORRECTION FACTOR INDEX
C                             2nd DIM.: NUMBER OF CORE TRANSITION TYPES
C          (R*8)  CORFA()    = CORSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (R*8)  CORSTK(,,)= INPUT - DR CORRECTION FACTOR
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  CR()      = CRSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - EXCITATION-AUTOIONISATION GROUP
C          (R*8)  CRSTK(,,) = INPUT - SCALING FACTOR FOR APPROX. FORM
C                                     FOR INTERNAL EXCIT./IONIS.RATE
C                                     COEFFT. CALCULATION
C                                     1st DIM.: EXCIT./IONIS. GROUP INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  DEFECT()  = DEFSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - N-SHELL
C          (R*8)  DEFSTK(,,)= INPUT - SET OF QUANTUM DEFECTS
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C                                     3rd DIM.: QU. DEF. INDEX
C          (R*8)  DELCN()   =
C          (R*8)  DENSA()   = INPUT - ELECTRON DENSITIES  (CM**-3)
C                                     1st. DIM: DENSITY INDEX
C          (R*8)  DENPA()   = INPUT - PROTON DENSITIES    (CM**-3)
C                                     1st. DIM: DENSITY INDEX
C          (R*8)  DEXPBN()  =
C          (R*8)  DEXPTE()  = EQUAL TO DEXP( (Z/(N-DEF)**2) * 157890.0/TE )
C                             1ST INDEX - REPRESENTATIVE LEVEL
C          (R*8)  DEXPTS()  = EQUAL TO DEXP( (Z/(N-DEF)**2) * 157890.0/TS )
C                             1ST INDEX - REPRESENTATIVE LEVEL
C          (R*8)  DFA()     = DR APPROXIMATE FORM PARM. IN EXPONENTIAL
C                             1st DIM.: NUMBER OF CORE TRANSITION TYPES
C          (R*8)  DG()      =
C          (R*8)  DGS1()    =
C          (R*8)  DGS2()    =
C          (R*8   DRECPT()  = VARIABLE FOR CALL TO B4PROJ.
C                             DIELECTRONIC RECOMBINATION CONTRIBUTION TO RHSC
C                             1ST INDEX - REPRESENTAIVE LEVEL
C          (R*8)  DRM(,,,,) =PARENT/SPIN SYSTEM RESOLVED DIELECTRONIC
C                            RECOMB. RATE COEFFTS. READ FROM EXTERNAL
C                            DR FILE IN SUBROUTINE B4DATD (CM 3 SEC-1).
C                                1st DIM.: REPRESENT. LEVEL INDEX (NREPX)
C                                2nd DIM.: TEMPERATURE INDEX
C                                3rd DIM.: INITIAL PARENT INDEX
C                                4th DIM.: INITIAL SPIN SYSTEM INDEX
C                                5th DIM.: FINAL PARENT INDEX
C          (R*8)  DRMU(,)   = DR RATE COEFFTS FOR GROUND STATE PARENT
C                             SUMMED OVER FINAL PARENT AND SPIN SYSTEM.
C                             RETURNED FROM SUBROUTINE B4DATD. (CM3 S-1)
C                                1st DIM.: REPRESENT. LEVEL INDEX (NREPX)
C                                2nd DIM.: TEMPERATURE INDEX
C          (R*8)  DRMSF(,,,)= PARENT/SPIN SYSTEM RESOLVED DR RATE COEFFTS.
C                             SUMMED OVER N-SHELL
C                             RETURNED FROM SUBROUTINE B4DATD. (CM3 S-1)
C                                1st DIM.: EXTERNAL DR FILE INDEX
C                                2nd DIM.: INITIAL PARENT INDEX
C                                3rd DIM.: INITIAL SPIN SYSTEM INDEX
C                                4th DIM.: FINAL PARENT INDEX
C          (R*8)  DVEC()    = VARIABLE FOR CALL TO B4PROJ.
C                             CONVERSION FACTOR FOR BN --> POPULATION
C                             1ST INDEX - REPRESENTATIVE LEVEL
C          (R*8)  EDISTK(,) = INPUT -  FITTING PARM. FOR RAD. RECOMB.
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (R*8)  EEIJ()    =
C          (R*8)  EGPSTK(,) = INPUT - FITTING PARM. FOR DR
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (R*8)  EION(,)   = EIOSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - IONISATION SHELL
C                             2ND INDEX - IONISATION GROUP
C          (R*8)  EIOSTK(,,,)=INPUT - SCALED IONIS. ENERGY (RYD)
C                                     1st DIM.: IONISATION GROUP INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C                                     4th DIM.: SPIN SYSTEM INDEX
C          (R*8)  ENER(,)   = ENESTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX -  RESONANCE
C                             2ND INDEX -  EXCITATION AUTOIONISATION GROUP
C          (R*8)  ENESTK(,,,)=INPUT - SCALED ENERGY FOR RESONANCE (RYD)
C                                     1st DIM.: SHELL INDEX
C                                     2nd DIM.: IONISATION GROUP INDEX
C                                     3rd DIM.: PARENT INDEX
C                                     4th DIM.: SPIN SYSTEM INDEX
C          (R*8)  EN2()     = EQUAL TO 1.0/(N-DEF)**2
C                             1ST INDEX - N-SHELL
C          (R*8)  EN23()    = EQUAL TO 1.0/N*0.667
C                             1ST INDEX - N-SHELL
C          (R*8)  EN3()     = EQUAL TO 1.0/(N-DEF)**3
C                             1ST INDEX - N-SHELL
C          (R*8)  EPSIL()   = EPSSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (R*8)  EPSSTK(,,)= INPUT - SCALED DR CORE TRANS. ENERGY (RYD)
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  ESEX()    = EXP(1.4388*BWNOS(CM-1)/TEA(K))*SEX()
C                             1ST DIM.: ELECTRON TEMPERATURE
C          (R*8)  ESEXPRT() = EXP(1.4388*BWNOS(CM-1)/TEA(K))*SEXPRT()
C                             1ST DIM.: FINAL PARENT INDEX
C          (R*8)  ESZDA()   = EXP(1.4388*BWNOS(CM-1)/TE(K))*SZDA()
C                             1ST INDEX - TEMPERATURE INDEX
C          (R*8)  ESZDEX(,,,)=EXP(1.4388*BWNOS(CM-1)/TE(K))*SZDEX(,,,)
C                             1ST DIM.: INITIAL PARENT INDEX
C                             2ND DIM.: INITIAL GROUND INDEX
C                             3RD DIM.: FINAL PARENT INDEX
C                             4TH DIM.: TEMPERATURE INDEX
C          (R*8)  FIJ()     = FIJSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (R*8)  FIJSTK(,,)= INPUT - SCALED DR CORE TRANS. OSC. STR.
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  FLAG()    =
C          (R*8)  OGARL()   =
C          (R*8)  PAA()     =
C          (R*8)  PHDSTK(,,)= INPUT - PHASE FACTOR FOR LOWEST LEVEL DR.
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  PHFRA()   = PHDSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (R*8)  PHFSTK(,) = INPUT - PHASE FACTOR FOR LOWEST LEVEL RAD.
C                                     RECOMB.
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (R*8)  POP()     = POPULATION OF REPRESENTATIVE N-SHELL
C                             1ST INDEX - IREP
C          (R*8)  PREP()    =
C          (R*8)  PRTWGT(,,)= INPUT -  ASSIGNS THE FRACTIONS OF THE
C                                      EXACT ZERO DENSITY IONIS. RATE
C                                      COEFFT. DATA (SEX) FROM THE
C                                      DATA SET TO DIFFERENT FINAL
C                                      PARENTS.  DATA FROM B4SSZD IF
C                                      USED OVERRIDES THIS.
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C                                     3rd DIM.: FINAL PARENT INDEX
C          (R*8)  PW0()     =
C          (R*8)  PWSAT(,,,,)=PARENT/SPIN SYSTEM RESOLVED DR SAT. RAD.
C                             POWER RETURNED FROM SUBROUTINE B4DATD
C                             (ERGS-1CM-3)
C                                1st DIM.: EXTERNAL DR FILE INDEX
C                                2nd DIM.: TEMPERATURE INDEX
C                                3rd DIM.: INITIAL PARENT INDEX
C                                4th DIM.: INITIAL SPIN SYSTEM INDEX
C                                5th DIM.: FINAL PARENT INDEX
C          (R*8)  PYMAT(,)  =
C          (R*8)  QPRT      =
C          (R*8)  QRES(,,,)  = TOTAL RESOLVED COLLISIONAL DIELECTRONIC
C                              CROSS-COUPLING COEFFICIENT (CM3 S-1)
C                                1st DIM.: INITIAL PARENT INDEX
C                                2nd DIM.: FINAL PARENT INDEX
C                                3rd DIM.: ELECTRON TEMPERATURE INDEX
C                                4th DIM.: ELECTRON DENSITY INDEX
C          (R*8)  QRESP(,,,,)= PARTIAL RESOLVED COLLISIONAL DIELECTRONIC
C                              CROSS-COUPLING COEFFICIENT (CM3 S-1)
C                                1st DIM.: INITIAL PARENT INDEX
C                                2nd DIM.: INTERMDIATE SPIN SYSTEM INDEX
C                                3rd DIM.: FINAL PARENT INDEX
C                                4th DIM.: ELECTRON TEMPERATURE INDEX
C                                5th DIM.: ELECTRON DENSITY INDEX
C          (R*8)  RATS()    = RATIO OF UNRESOLVED IONISATION RATE TO SUM OF
C                             RESOLVED IONISATION RATES FOR PARTICULAR
C                             TEMPERATURE
C                             1ST INDEX - ELECTRON DENSITY
C          (R*8)  RDREP     =
C          (R*8)  RHS       =
C          (R*8)  RHSC()    = VARIABLE FOR CALL TO B4PROJ.
C                             CONDENSED RHS VECTOR FOR CALCULATION WITH LEVEL
C                             CONDENSATION.  (CN SOLUTION)
C                             1ST INDEX - REPRESENTAIVE LEVEL
C          (R*8)  RHSI()    = VARIABLE FOR CALL TO B4PROJ.
C                             CONDENSED RHS VECTOR FOR CALCULATION WITHOUT
C                             LEVEL CONDENSATION.  (CN SOLUTION)
C                             1ST INDEX - REPRESENTATIVE LEVEL
C          (R*8)  RHSIRC()  = VARIABLE FOR CALL TO B4PROJ.
C                             RECOMBINATION CONTRIBUTION TO RHSI
C                             1ST INDEX - REPRESENTATIVE LEVEL
C          (R*8)  RHSJK     =
C          (R*8)  RHSN      =
C          (R*8)  RHSREC    =
C          (R*8)  RHS1      =
C          (R*8)  RLREP     =
C          (R*8)  RL0REP    =
C          (R*8)  RRECPT()  = VARIABLE FOR CALL TO B4PROJ.
C                             RADIATIVE RECOMBINATION CONTRIBUTION TO RHSC
C                             1ST DIM.: REPRESENTATIVE LEVEL
C          (R*8)  RRREP     =
C          (R*8)  S0        =
C          (R*8)  SAA       =
C          (R*8)  SAAP      =
C          (R*8)  SCASTK(,) = INPUT - FITTING PARM. FOR RAD. RECOMB.
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (R*8)  SEX()     = SEXSTK FOR CURRENT PARENT/SPIN SYSTEM
C                             1ST DIM.: ELECTRON TEMPERATURE
C          (R*8)  SEXPRT()  = RESOLVED ZERO DENSITY IONIS. RATE COEFFT.
C                             (CM3 S-1) FOR CURRENT PARENT/SPIN SYSTEM
C                             1ST DIM.: FINAL PARENT INDEX
C          (R*8)  SEXSTK(,,)= INPUT - EXACT VALUE OF ZERO DENSITY IONIS.
C                                     RATE COEFFT. (CM3 S-1)
C                                     1ST DIM.: ELECTRON TEMP. INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  SGPSTK(,) = INPUT - FITTING PARM. FOR DR
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (R*8)  SPNSTK()  = SET OF NGRSTK GROUND STATE SPINS
C                             1st DIM.: GROUND STATE INDEX
C          (R*8)  SPRSTK()  = SET OF NPRSTK PARENT STATE SPINS
C                             1st DIM.: PARENT INDEX
C          (R*8)  SPRT      =
C          (R*8)  SRES(,,,) = TOTAL RESOLVED COLLISIONAL DIELECTRONIC
C                              IONISATION COEFFICIENT (CM3 S-1)
C                                1st DIM.: INITIAL GROUND STATE INDEX
C                                2nd DIM.: FINAL PARENT INDEX
C                                3RD DIM.: ELECTRON TEMPERATURE INDEX
C                                5th DIM.: ELECTRON DENSITY INDEX
C          (R*8)  SRESP(,,,,)= PARTIAL RESOLVED COLLISIONAL DIELECTRONIC
C                              IONISATION COEFFICIENT (CM3 S-1)
C                                1st DIM.: INITIAL GROUND STATE INDEX
C                                2nd DIM.: FINAL PARENT INDEX
C                                3rd DIM.: INTERMEDIATE PARENT INDEX
C                                4th DIM.: ELECTRON TEMPERATURE INDEX
C                                5th DIM.: ELECTRON DENSITY INDEX
C          (R*8)  SSWSTK(,) = PARENT/GROUND STATE SET OF SSYSWTs
C                             1st DIM.: PARENT INDEX
C                             2nd DIM.: GROUND STATE INDEX
C
C          (R*8)  STOTR     =
C          (R*8)  SUMPR     =
C          (R*8)  SUMPRT    =
C          (R*8)  SZDA()    = ZERO DENSITY IONIS. RATE COEFFTS
C                             OUTPUT FROM B4SSZD. (CM3 S-1)
C                             1ST INDEX - TEMPERATURE INDEX
C          (R*8)  SZDEX(,,,)= STACKED ZERO DENSITY IONIS RATE COEFFTS.
C                             FROM B4SSZD.  (CM3 S-1)
C                             1ST DIM.: INITIAL PARENT INDEX
C                             2ND DIM.: INITIAL GROUND INDEX
C                             3RD DIM.: FINAL PARENT INDEX
C                             4TH DIM.: TEMPERATURE INDEX
C          (R*8)  TEA()     = INPUT - ELECTRON TEMPERATURES (K)
C                                     1st. DIM: TEMPERATURE INDEX
C          (R*8)  TPA()     = INPUT - PROTON   TEMPERATURES (K)
C                                     1st. DIM: TEMPERATURE INDEX
C          (R*8)  TRECPT()  = VARIABLE FOR CALL TO B4PROJ.
C                             THREE BODY RECOMBINATION CONTRIBUTION TO RHSC
C                             1ST INDEX - REPRESENTAIVE LEVEL
C          (R*8)  TVAL()    = INPUT FOR B4SSZD.
C                             SET OF TEA() ELECTRON TEMPERATURES CONVERTED
C                             TO EV
C                             1ST INDEX - TEMPERATURE
C          (R*8)  V0STK(,)  = INPUT - EFF. PRINC. QU. NO OF N0STK
C                                     1st DIM.: PARENT INDEX
C                                     2nd DIM.: SPIN SYSTEM INDEX
C          (R*8)  V1A()     = V1ASTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (R*8)  V1ASTK(,,)= INPUT - EFF. PRINC. QU. NO. OF N1ASTK
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  VEC       =
C          (R*8)  WB        =
C          (R*8)  WBLOG     =
C          (R*8)  WBREP     =
C          (R*8)  WBREPRF() = INPUT - RADIATION DIULUTION FOR GROUND TO
C                                    REPRESENT. LEVEL
C                                    1st. DIM.: REPRESENT. SHELL INDEX
C          (R*8)  WEIJ      =
C          (R*8)  WGHT(,)   = WGHSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - RESONANCE
C                             2ND INDEX - EXCITATION-AUTOIONISATION GROUP
C          (R*8)  WGHSTK(,,,)=INPUT - WEIGHTING FOR RESONANCE
C                                     1st DIM.: RESONANCE INDEX
C                                     2nd DIM.: RESONANCE GROUP INDEX
C                                     3rd DIM.: PARENT INDEX
C                                     4th DIM.: SPIN SYSTEM INDEX
C          (R*8)  WIJ()     = WIJSTK FOR PARTICULAR PARENT/SPIN SYSTEM
C                             CALCULATION
C                             1ST INDEX - CORE TRANSITION
C          (R*8)  WIJSTK(,,)= INPUT - DR CORE TRANS. RADIATION DILUTION
C                                     FACTOR
C                                     1st DIM.: TRANSITION INDEX
C                                     2nd DIM.: PARENT INDEX
C                                     3rd DIM.: SPIN SYSTEM INDEX
C          (R*8)  XRECPT()  = VARIABLE FOR CALL TO B4PROJ.
C                             CHARGE EXCHANGE RECOMBINATION CONTRIBUTION
C                             TO RHSC
C                             1ST INDEX - REPRESENTAIVE LEVEL
C
C
C
C          (C*4)  TRMSTK()  = SET OF NPRTSK PARENT TERM CHARS.
C                             1st DIM.: PARENT INDEX
C
C
C
C          (L*4)  LIOSEL(,) = .TRUE. => READ IN ZERO DENSITY IONIS. RATE
C                                       COEFFTS FROM EXTERNAL SZD FILE
C                             .FALSE.=> DO NOT READ EXTERNAL IONIS. DATA
C                                       1st DIM.: PARENT INDEX
C                                       2nd DIM.: SPIN SYSTEM INDEX
C          (L*4)  LTRNG()   = OUTPUT FROM B4SSZD.
C                             .TRUE. => OUTPUT SZDA WAS INTERPOLATED
C                             .FALSE.=> OUTPUT SZDA WAS EXTRAPOLATED .
C                             1ST INDEX - ELECTRON TEMPERATUREC
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B4DATA     ADAS      GATHERS RELEVANT DATA FROM PRIMARY FILE
C          B4DATD     ADAS      FETCH EXTERNAL DR DATA FROM FILE
C          B4SPFX     ADAS      GATHERS INPUT FILE NAMES VIA IDL
C          B4PROJ     ADAS      CALCULATE CONDENSED PROJECTION MATRICES
C          B4MATV     ADAS      MATRIX INVERSION WITH PARAMETERISATION
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXGUID     ADAS      GATHERS USER NAME AS 10 BYTE STRING
C          XXFLSH     ADAS      FLUSH THE UNIX BI-DIRECTIONAL PIPE
C          DATIM      ADAS      TO RETURN CURRENT DATA
C          COLINT     ADAS      CALCULATE ECIP IONISATION RATE FUNCTION
C          DIELCL     ADAS      CALCULATE BURGESS PROGARM DR RATE FUNCTION
C          PHOTOLT    ADAS      CALCULATE PHOTO-INTEGRALS
C          PYIP       ADAS      RETURNS IMPACT PARAMETER PY FACTORS
C          PYVR       ADAS      RETURNS VAN REGEMORTER PY FACTORS
C          PYPR       ADAS      RETURNS PERCIVAL-RICHARDS PY FACTORS
C          R2PHOTO    ADAS      RETURNS ENERGY AVERAGED GBF.
C          B4SSZD     ADAS      VERSION OF SSZD TO CALL SZD FILE
C          EEI        ADAS      EXPONENTIAL INTEGRAL
C          GBB        ADAS      BOUND-BOUND GAUNT FACTOR
C          FINTER     ADAS
C          NGFFMH     ADAS      FREE-FREE GAUNT FACTOR
C          XXNAME     ADAS      FINDS REAL NAME OF USER
C
C
C
C AUTHOR: H. P. SUMMERS (W. J. DICKSON), UNIVERSITY OF STRATHCLYDE
C         JA8.08
C         TEL. 0141-553-4196
C
C DATE:    ??/09/93
C
C UPDATE:  21/08/96 W. OSBORN - CONVERSION FOR UNIX
C
C UPDATE:  21/01/97 HP SUMMERS - REVISION OF PRELIMINARY UNIX VERSION
C
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 21-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C               - CHANGED NAME FROM MAINBNS TO ADAS204.
C               - UNIT NUMBERS CHANGED SO AS NOT TO CONFLICT WITH PIPE
C                 COMM. STREAMS.
C               - INFORMATION THAT WAS PASSED USING ENVIRONMENT VARIABLES
C                 NOW READ IN FROM IDL OR A BATCH FILE.
C               - IF RUNNING IN INTERACTIVE MODE, THE PROGRAM WRITES
C                 PROGRESS INFORMATION TO THE UNIX PIPE FOR IDL TO READ.
C
C VERSION: 1.2                          DATE: 30-08-96
C MODIFIED: WILLIAM OSBORN
C     - ADDED PIPE FLUSHES
C
C VERSION: 1.3                          DATE: 11-09-96
C MODIFIED: WILLIAM OSBORN
C     - ADDED CALL TO I4PIPE FOR PIPE OUTPUT SINCE DIRECT PIPE OUTPUT FROM
C       THIS ROUTINE WASN'T WORKING ON SUNS. NO IDEA WHY NOT.
C
C VERSION: 1.4                          DATE: 17-10-96
C MODIFIED: WILLIAM OSBORN
C     - DEFINED PIPEIN AND PIPEOU AS INTEGER RATHER THAN IMPLICIT REAL*8
C
C VERSION: 1.5                          DATE: 22-10-96
C MODIFIED: WILLIAM OSBORN
C     - ADDED USERID INPUT SO THAT XXUID CAN BE CALLED TO ENQUIRE WHAT
C       THE CURRENT USER DIRECTORY IS
C     - REMOVED I4PIPE SINCE IT WAS THE NON-INTEGER PIPEOU THAT WAS CAUSING
C       THE PROBLEM
C
C VERSION: 1.6                          DATE: 14-02-97
C MODIFIED: RICHARD MARTIN
C     - CHANGED DECLARED INTEGER 'PIPOUT' TO 'PIPEOU'
C
C VERSION: 1.7                          DATE: 04-03-98
C MODIFIED: L.HORTON, H.SUMMERS, M.O'MULLANE, R.MARTIN
C   		-INCREASED DIMENSIONS OF NTDIM TO 24 FROM 15
C                        NDDIM TO 24 FROM 8
C		-CHANGED THE DIMENSIONING OF A FEW VARIABLES WHICH ARE DECLARED
C		 BY SPIN SYSTEM AND USED BY GROUND STATE.  I.E.
C        		SZDEX(NPRDIM,NGRDIM,NPRDIM,NTDIM)
C        		ESZDEX(NPRDIM,NGRDIM,NPRDIM,NTDIM)
C        		BWNOSA(NPRDIM,NGRDIM,NPRDIM)
C        		BWNOM(NPRDIM,NGRDIM)
C 		 Here, I have to say I'm confused about the use of APRT in the
C		 code. It gets variously used by parent and ground state label
C		 rather than confusing spin system and ground state labels, so
C		 I've left it alone (Any ideas Hugh?) (LH 20/5/97)
C		-NUMEROUS FORMAT CHANGES TO REFLECT INCREASED NUMBERS OF TE/NE (LH)
C		-CHANGE TO     alossi(jprt,i) = esexprt(jprt) / esgrd * aion
C		 FROM		   			= sexprt(jprt) / sgrd * aion (8/7/97)
C		-REMOVED WRITING NCUT TO XCD OUTPUT (SET TO 0). SEE CALL TO FORMAT
C		 STMT 2007	(17/11/97)
C		-ADDED COMMENT SECTION TO ACD, SCD, XCD AND PRB OUTPUT. (19/11/97)
C	 	-ADDED LOGICAL PARAMETERS OPEN08, OPEN09, OPEN17, OPEN20
C		 TO ENABLE OUTPUT TO 'ADAS204.PASS?' TO BE SWITCHED OFF.
C		     (RM 04-03-98)
C
C VERSION: 1.8                          DATE: 09-03-98
C MODIFIED: H.SUMMERS
C   		-PASSED RECOM/CASCADE/BREMS. POWER COEFFT. TO B4PROJ FOR
C                ADDITION TO THE CONDENSED/PROJECTION MATRIX FILE OUTPUT
C
C VERSION: 1.9                         DATE: 03-12-98
C MODIFIED: Martin O'Mullane
C   		- Include the supplementary DR cross-referencing file and
C                 the supplementary adf07 ionisation file in the adf25
C                 namelist. The code has beem modified to open these
C                 files directly rather than constructing appropriate
C                 file names from fragmentary data in the adf25 driver.
C
C               - Consistency with the old style is ensured by testing
C                 that XRMEMB is the same as the adf25 file extension.
C
C VERSION: 1.10                        DATE: 14-10-99
C MODIFIED: Richard Martin
C		Initialised strings ioniz & drsup with 'NULL'
C
C VERSION: 1.11                        DATE: 13-09-2002
C MODIFIED: Martin O'Mullane
C   		- Add more selectivity to writing output files. These
C                 can be individually selected via IDL panels. Add
C                 more openXX logical variables and logic throughout
C                 the code to implement this functionality. Do not allow
C                 writing to units 8, 9 and 20 as before but allow user
C                 to write to unit 17 (paper.txt) if desired.
C                 
C VERSION: 1.12                        DATE: 23-05-2005
C MODIFIED: Martin O'Mullane
C   		- Export population data (bn, cn etc) as an optional
C                 output file.
C
C VERSION : 1.13                        
C DATE    : 22-02-2006
C MODIFIED: Martin O'Mullane
C   		- Trap too big an exponent to exp() when calculating 
C                 the ionisation rate from adf07 file. This produced
C                 NaNs in adf17 files from Sun and g77 executables.
C
C VERSION : 1.14                        
C DATE    : 29-09-2009
C MODIFIED: Martin O'Mullane
C   		- Initialize iz0in.
C               - Trim date to 8 characters to prevent binary junk.
C
C VERSION : 1.15
C DATE    : 14-10-2010
C MODIFIED: Martin O'Mullane
C   		- Re-dimension arrays with nrepdm+1 to nrepdm.
C               - Ensure a maximum of 1.0D-30 for AEXSTK and SEXSTK.
C               - In setting AI in case of ionisation rate from adf07
C                 do not re-scale if ESGRD is not sensible. 
C
C VERSION : 1.16
C DATE    : 21-01-2013
C MODIFIED: Martin O'Mullane
C              - Increase dimension of representative n arrays to 100.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       INTEGER   PIPEIN  , PIPEOU
       INTEGER   IUNT08  , IUNT09
       INTEGER   IUNT17  , IUNT18  , IUNT19  , IUNT20  , IUNT22
       INTEGER   IUNT76  , IUNT61  , IUNT62  , IUNT63  , IUNT64
       INTEGER   IUNT30
       INTEGER   NTDIM   , NDDIM   , NPRDIM  , NSYDIM  , NREPDM
       INTEGER   NGRDIM  , NDPRTBD , NMAXC2  , NLWS    , NRESU
       INTEGER   NDBFILM
       INTEGER   NBIN
C-----------------------------------------------------------------------
       REAL*8    EM      , CUT
C-----------------------------------------------------------------------
       LOGICAL	 OPEN08  , OPEN09  ,   OPEN20
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6                              )
       PARAMETER( IUNT08 = 8 , IUNT09 = 9                              )
       PARAMETER( IUNT17 =17 , IUNT18 =18 , IUNT19 =19  , IUNT20 =20  ,
     &            IUNT22 =22 , IUNT30 =30 )
       PARAMETER( IUNT76 =76 , IUNT61 =61 , IUNT62 =62  , IUNT63 =63   )
       PARAMETER( IUNT64 =64                                           )
       PARAMETER( NTDIM  =24 , NDDIM  =24 , NPRDIM  = 4 , NSYDIM = 2   )
       PARAMETER( NREPDM =100 , NGRDIM = 4 , NDPRTBD =12 , NMAXC2 =40  )
       PARAMETER( NLWS   = 5 , NRESU  = 5 , NDBFILM = 6                )
       PARAMETER( NBIN   =10                                           )
       PARAMETER( EM     = 918.0  , CUT   = 150.0                      )
       PARAMETER( OPEN08=.FALSE.  , OPEN09=.FALSE.     , OPEN20=.FALSE.)
C-----------------------------------------------------------------------
       INTEGER   IBD     , IBIN    , IBSEL   , ICALC   ,
     &           IDCN    , IEDMAT  , IECION  , IETREC  ,
     &           IEDREC  , IERREC  ,
     &           IEXREC  , IERSYS  , IMAX1   ,
     &           IPRT    , ISPR    , ICTPR   , IGRD    , ISPN    ,
     &           I       , I1      , I2      , I3      ,
     &           IDEF    , ISYS    , IT      , ITRANS  , IMARK   ,
     &           IMAX    , IMAXC1  , IMAXC2  , IONE    ,
     &           IIGRP   , ISHEL   , IRGRP   , IRESO   , IMAXRF  ,
     &           IBSEL1  , INTD    , INMAXX  , IPRS    , IREP    ,
     &           ITT     , ILOW    , IRCODE  , IX      , IZ      ,
     &           IZS     , ITVAL   , IZ0IN   , IZ1S    , IR
       INTEGER   J       , JPRT    , JDENSM  , JTEM    , JSTEM   ,
     &           JCOR    , JDEF    , JDENS   , JTE
       INTEGER   K       , KPRT
       INTEGER   L       , LEN1    , LEN2    , LEN3    , LEN4    ,
     &           LOGIC
       INTEGER   N       , N1      , N11     , N2      , N3      ,
     &           NBFIL   , NIGRP   , NL      , NMAXC1  , NRGRP   ,
     &           NUCCHG  , NPARNT  , NGRSTK  , NPRSTK  ,
     &           NCUT    , NIP     , NN      , NMAX    , NMIN    ,
     &           NSHEL   , NMAX1   , NCALC   , NTEST   , NTRANS  ,
     &           NTST    , NMINRF  , NMAXRF  , NPATH   , N0
       INTEGER   METIS   , METFS   , MONE    , MTWO    , i4unit
C-----------------------------------------------------------------------
       REAL*8    A1      , A2      , A3      , AI      , AION    ,
     &           AAG     , A1CNST  , ACNST   , ADIREX  ,
     &           AAGFAC  , APION   , AL0     , ADIR    , AAGMUL  ,
     &           ADS     , AD      , ADSGP   , ADJUST  , ATOT    ,
     &           AREC    , ALFA    , AL0C1   , ATE     , ATS     ,
     &           BWNOS
       REAL*8    C       , CN1     , CN2     , CN3     , C2ION   ,
     &           CPL     , CPT     , C1REC   , CN      , CIONPTS ,
     &           D       , D1      , DEFN    , DETERM  , DENS    ,
     &           DENSP   , DEF     , DRMC    , DELE    ,
     &           EI      , EJ      , ENC     , EIJ     , EN1     ,
     &           EN11    , ENMAX2  , EYE     , EN      , ESECIP  ,
     &           ESGRD   , EXPON   ,
     &           F       , FACTOR  , FGP     , FINTER  ,
     &           G       , GBB
       REAL*8    PION    , PREC    , PSTIM   , PHI     , PHF     ,
     &           PY      , PY0     , PYP     ,
     &           QRT     , QRD     , QRR     , QCR     , QRRC1   ,
     &           QRTC1   , QCRC1   , QCT     , QRREC   , QRX3
       REAL*8    R       , RL      , RD      , RR      , RL0     ,
     &           RL0C1   , RLC1    , RHDIR   ,
     &           RRC1    , RDC1    , RBC1    , RHO     , RHOP    ,
     &           RA      , RRN     , RDEXC   , RAD     , RREC    ,
     &           RX3     , RHSD    , RB
       REAL*8    SECIP   , SATEN   , STOT    , SUM1    , SUM2    ,
     &           SD      , SDIR    , S       , SUM3    , SGRD    ,
     &           SUMION
       REAL*8    TE      , TP      , TS      , WEBIN   ,
     &           W       , Z       , CFAC    , CION    , CPY
       REAL*8    XL      , W1
       REAL*8    BMENER  , DENSH   , DF      , EDISP   , EDISGP  , EIONR
       REAL*8    FLUX    , NGFFMH
       REAL*8    PHFRAC  , SCALE   , SCALGP  , SSYSWT  , V0
       REAL*8    X       , X1      , X2      , X3      , X4      ,
     &           YE      , Y       , YS      , WEIJJ   ,
     &           WI      , WJ      , W2      , ZCOL    , Z1
C-----------------------------------------------------------------------
       CHARACTER SEQ*2      , XRMEMB*8   , TRMPRT*4    , DATE*10
       CHARACTER STRING*80  , EQUALS*80  , DASHES*80   , USERID*80
       CHARACTER DSNIN*120  , PASSDIR*80 , DSNXRT*120
       CHARACTER REP*3
       CHARACTER DSNOUT*120
       CHARACTER SUBSTRG*50
       CHARACTER TITLX*120
       CHARACTER ADAS_C*80  , ADAS_U*80
       CHARACTER IONIZ*120  , DRSUP*120
       CHARACTER DSNSZD*120 , DSNDR*120
       CHARACTER REALNAME*30
C-----------------------------------------------------------------------
       LOGICAL   LRESOL  ,  LDRSEL , LPROJ   , LFILT   , LBIN
       LOGICAL   LECOOL  ,  LBATCH , LBTSEL  , lexist
       logical   open17  , open18  , open19  , open76  , 
     &           open61  , open62  , open63  , open64  , open30
C-----------------------------------------------------------------------
       INTEGER   NREPC(NREPDM)              , NREPI(NREPDM)
       INTEGER   NREPRF(NREPDM+1)           ,  NREP1(NREPDM+1)
       INTEGER   NREP(NREPDM+1)             , NREPX(NREPDM)
       INTEGER   NMINSTK(NPRDIM,NSYDIM)     , JDEFSTK(NPRDIM,NSYDIM)
       INTEGER   N0STK(NPRDIM,NSYDIM)
       INTEGER   NTRSTK(NPRDIM,NSYDIM)      , ITYSTK(12,NPRDIM,NSYDIM)
       INTEGER   N1ASTK(12,NPRDIM,NSYDIM)   , NCTAA(12,NPRDIM,NSYDIM)
       INTEGER   NIGSTK(NPRDIM,NSYDIM)      , NRGSTK(NPRDIM,NSYDIM)
       INTEGER   ISZD(NPRDIM,NSYDIM,NPRDIM)
       INTEGER   NSHSTK(2,NPRDIM,NSYDIM)    , IZESTK(15,2,NPRDIM,NSYDIM)
       INTEGER   NRESTK(2,NPRDIM,NSYDIM)
       INTEGER   NSYSTK(NPRDIM)             , IGRSTK(NPRDIM,NSYDIM)
       INTEGER   NCTSTK(NPRDIM,NGRDIM)      , NCUTMC(NDPRTBD,NDPRTBD)
       INTEGER   ITYPEA(12)   , N1A(12)     , NCTA(12)
       INTEGER   NSHELA(2)    , NRESOA(2)   , IZETA(15,2)
       INTEGER   JCORA(7)
       INTEGER   INTER(550)
       INTEGER   opstatus(9)  , opunits(9)
C-----------------------------------------------------------------------
       REAL*8    DENSA(NDDIM) , TEA(NTDIM)  , DENPA(NDDIM) ,
     &           TPA(NTDIM)
       REAL*8    WBREPRF(NREPDM)
       REAL*8    DEFSTK(NPRDIM,NSYDIM,5)    , V0STK(NPRDIM,NSYDIM)
       REAL*8    PHFSTK(NPRDIM,NSYDIM)      , EDISTK(NPRDIM,NSYDIM)
       REAL*8    SCASTK(NPRDIM,NSYDIM)      , V1ASTK(12,NPRDIM,NSYDIM)
       REAL*8    PHDSTK(12,NPRDIM,NSYDIM)   , EPSSTK(12,NPRDIM,NSYDIM)
       REAL*8    FIJSTK(12,NPRDIM,NSYDIM)   , CORSTK(12,NPRDIM,NSYDIM)
       REAL*8    WIJSTK(12,NPRDIM,NSYDIM)   , EGPSTK(NPRDIM,NSYDIM)
       REAL*8    SGPSTK(NPRDIM,NSYDIM)      , SSWSTK(NPRDIM,NGRDIM)
       REAL*8    SPRSTK(NPRDIM)             , SPNSTK(NGRDIM)
       REAL*8    PRTWGT(NPRDIM,NSYDIM,NPRDIM)
       REAL*8    CISTK(2,NPRDIM,NSYDIM)     , EIOSTK(15,2,NPRDIM,NSYDIM)
       REAL*8    CRSTK(2,NPRDIM,NSYDIM)     , WGHSTK(15,2,NPRDIM,NSYDIM)
       REAL*8    ENESTK(15,2,NPRDIM,NSYDIM)
       REAL*8    AEXSTK(NTDIM,NPRDIM,NSYDIM)
       REAL*8    SEXSTK(NTDIM,NPRDIM,NSYDIM)
       REAL*8    DRM(NREPDM,NTDIM,NDPRTBD,NSYDIM,NDPRTBD)
       REAL*8    DRMU(NREPDM,NTDIM)
       REAL*8    DRMSF(NDBFILM,NTDIM,NDPRTBD,NSYDIM,NDPRTBD)
       REAL*8    PWSAT(NDBFILM,NTDIM,NDPRTBD,NSYDIM,NDPRTBD)
       REAL*8    AUGM(NREPDM,NDPRTBD,NSYDIM,NDPRTBD)
       REAL*8    SRESP(NGRDIM,NPRDIM,NPRDIM,NTDIM,NDDIM)
       REAL*8    QRESP(NPRDIM,NGRDIM,NPRDIM,NTDIM,NDDIM)
       REAL*8    SRES(NGRDIM,NPRDIM,NTDIM,NDDIM)
       REAL*8    ARES(NPRDIM,NGRDIM,NTDIM,NDDIM)
       REAL*8    QRES(NPRDIM,NPRDIM,NTDIM,NDDIM)
       REAL*8    ARED(NREPDM,NREPDM)         ,    AREDN(NREPDM,NREPDM)
       REAL*8    RHS(NREPDM)       , BREP(NREPDM)    , DEXPBN(NREPDM)
       REAL*8    WBREP(NREPDM)     , WBLOG(NREPDM)   , RHSN(NREPDM)
       REAL*8    POP(NREPDM)       , RHS1(NREPDM)    , DELCN(NREPDM)
       REAL*8    DG(NREPDM+1)      , DGS1(NREPDM+1)  , DGS2(NREPDM+1)
       REAL*8    VEC(NREPDM+1)     , AL0REP(NREPDM+1)
       REAL*8    PREP(NREPDM+1)    , RLREP(NREPDM+1) , RDREP(NREPDM+1)
       REAL*8    RRREP(NREPDM+1)   , RL0REP(NREPDM+1)
       REAL*8    ALFEX(NTDIM),SEX(NTDIM),ALF0(NTDIM),S0(NTDIM)
       REAL*8    ESEX(NTDIM),PW0(NTDIM)
       REAL*8    ALFAA(NTDIM,NDDIM),SAA(NTDIM,NDDIM),PAA(NTDIM,NDDIM)
       REAL*8    RHSJK(NREPDM+1) , AREDJK(NREPDM,NREPDM)
       REAL*8    RHSREC(NREPDM)
       REAL*8    ALOSSR(NPRDIM,NREPDM)  , SPRT(NPRDIM), APRT(NPRDIM)
       REAL*8    QPRT(NPRDIM) , COUT(NPRDIM,NREPDM)
       REAL*8    ALOSST(NREPDM), SUMPR(NPRDIM), SUMPRT(NPRDIM)
       REAL*8    ALOSSI(NPRDIM,NREPDM), ALOSSA(NPRDIM,NREPDM)
       REAL*8    TVAL(NTDIM), SZDA(NTDIM) , ESZDA(NTDIM)
       REAL*8    SZDEX(NPRDIM,NGRDIM,NPRDIM,NTDIM), SEXPRT(NPRDIM)
       REAL*8    ESZDEX(NPRDIM,NGRDIM,NPRDIM,NTDIM)
       REAL*8    ESEXPRT(NPRDIM)
       REAL*8    BWNOSA(NPRDIM,NGRDIM,NPRDIM), BWNOM(NPRDIM,NGRDIM)
       REAL*8    ATOTR(NTDIM,NDDIM), STOTR(NTDIM,NDDIM), RATS(NDDIM)
       REAL*8    AREDC(NREPDM,NREPDM), RHSC(NREPDM), DVEC(NREPDM)
       REAL*8    AREDI(NREPDM,NREPDM), RHSI(NREPDM), RHSIRC(NREPDM)
       REAL*8    TRECPT(NREPDM), DRECPT(NREPDM), RRECPT(NREPDM)
       REAL*8    XRECPT(NREPDM)
       REAL*8    CIONPT(NREPDM), CIONRI(NPRDIM,NREPDM)
       REAL*8    CIONRA(NPRDIM,NREPDM)
       REAL*8    QELR(NTDIM,NDDIM)
       REAL*8    QCREP(NREPDM+1)   , QCREPC(NREPDM+1)
       REAL*8    QRDREP(NREPDM+1)  , QRRREP(NREPDM+1)
       REAL*8    QRTREP(NREPDM+1)
       REAL*8    PREPBIN(NREPDM+1,NBIN) , RLBIN(NBIN)
       REAL*8    RLREPBN(NREPDM+1,NBIN)
       REAL*8    PCASBIN(NTDIM,NDDIM,NBIN)
       REAL*8    COR(140)    , DEFECT(10)    , EPSIL(12)     , FIJ(12)
       REAL*8    WIJ(12)     , EEIJ(12)      , WEIJ(12)
       REAL*8    EION(15,2)  , WGHT(15,2)    , ENER(15,2)
       REAL*8    CI(2)       , CR(2)
       REAL*8    EN2(550)    , EN3(550)      , DEXPTE(550)   ,
     &           DEXPTS(550) , OGARL(1100)
       REAL*8    CORA(20,7)  , DFA(7)
       REAL*8    V1A(12)     , PHFRA(12)     , CORFA(12)
       REAL*8    A(550)      , AGRL(550,3)   , EN23(550)     , FLAG(550)
       REAL*8    AC(550)     , ACI(550)      , WB(550)
       REAL*8    PYMAT(550,4)
C-----------------------------------------------------------------------
       CHARACTER TRMSTK(NPRDIM)*4
       character opfiles(9)*80
C-----------------------------------------------------------------------
       LOGICAL   LTRNG(NTDIM),  LIOSEL(NPRDIM,NSYDIM)
C-----------------------------------------------------------------------
       NAMELIST /FILINFO/ NUCCHG, NPARNT, SEQ,  XRMEMB , ioniz, drsup
C-----------------------------------------------------------------------
       DATA DASHES/'----------------------------------------------------
     &----------------------------'/
       DATA EQUALS/'====================================================
     &============================'/
       DATA JCORA/20,20,20,20,20,20,20/
       DATA DFA/2.0D0,1.0D0,1.0D0,0.5D0,0.5D0,1.0D0,1.0D0/
       DATA (CORA(J,1),J=1,20)/0.05D0,0.3D0,0.5D0,0.9D0,16*1.0D0/
       DATA (CORA(J,2),J=1,20)/0.01D0,0.02D0,0.20D0,0.40D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,3),J=1,20)/0.01D0,0.01D0,0.01D0,0.08D0,0.30D0,0.70D0
     &,14*1.0D0/
       DATA (CORA(J,4),J=1,20)/0.30D0,0.35D0,0.40D0,0.45D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,5),J=1,20)/0.30D0,0.35D0,0.40D0,0.45D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,6),J=1,20)/0.01D0,0.02D0,0.20D0,0.40D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,7),J=1,20)/0.01D0,0.01D0,0.01D0,0.08D0,0.30D0,0.70D0
     &,14*1.0D0/
       DATA IONE/1/,MONE/-1/,MTWO/-2/
       data opunits /17,18,19,76,61,62,63,64,30/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C   *********************** START OF PROGRAM  *************************
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
       CALL XX0000
     
       CALL XXNAME(REALNAME)
       
       iz0in = -1
       do i = 1, NREPDM
         NREPC(i) = 0
         NREPI(i) = 0
       end do
C
C-----------------------------------------------------------------------
C  GET PARAMETER LBATCH (IS THIS A BATCH RUN OR NOT?) FROM IDL OR FILE
C-----------------------------------------------------------------------
C
       READ( PIPEIN, *) LOGIC
       IF (LOGIC.EQ.0) THEN
          LBATCH = .FALSE.
       ELSE
          LBATCH = .TRUE.
       ENDIF
C
C-----------------------------------------------------------------------
C  SET LOGICAL CONTROL CHARACTERS
C-----------------------------------------------------------------------
C
       LBIN   = .TRUE.
       LECOOL = .FALSE.
       LPROJ  = .TRUE.
       LFILT  = .FALSE.
C
C-----------------------------------------------------------------------
C  GET DATE FOR OUTPUT
C-----------------------------------------------------------------------
C
       CALL XXDATE(DATE)
C
C-----------------------------------------------------------------------
C  GET INFO. FROM IDL INPUT AND OUTPUT SCREENS
C  (OR FROM A FILE IF RUNNING IN BATCH)
C
C-----------------------------------------------------------------------
C
 99    CONTINUE
       CALL B4SPF0( REP     , DSNIN    , LBTSEL , ADAS_C , ADAS_U ,
     &              opfiles , opstatus )

C
C-----------------------------------------------------------------------
C  IF CANCEL SELECTED THEN EXIT
C-----------------------------------------------------------------------
C
       IF (REP.EQ.'YES') GOTO 5000

C-----------------------------------------------------------------------
C  IF THE JOB HAS BEEN SELECTED TO RUN IN BATCH THEN GO BACK TO THE
C  INPUT OPTIONS
C-----------------------------------------------------------------------
C
       IF (LBTSEL) GOTO 99
C
C-----------------------------------------------------------------------
C  OPEN MAIN INPUT FILE
C-----------------------------------------------------------------------
C
      OPEN(UNIT=IUNT22, FILE=DSNIN, STATUS='UNKNOWN')
C
C-----------------------------------------------------------------------
C  OPEN OUTPUT FILES
C-----------------------------------------------------------------------
      open17 = .FALSE.
      open18 = .FALSE.
      open19 = .FALSE.
      open76 = .FALSE.
      open61 = .FALSE.
      open62 = .FALSE.
      open63 = .FALSE.
      open64 = .FALSE.
      open30 = .FALSE.
      
      do i=1,9
        if (opstatus(i).EQ.1) then
           OPEN(UNIT=opunits(i),FILE=opfiles(i), STATUS='UNKNOWN')
           if (opunits(i).EQ.17) open17 = .TRUE.
           if (opunits(i).EQ.18) open18 = .TRUE.
           if (opunits(i).EQ.19) open19 = .TRUE.
           if (opunits(i).EQ.76) open76 = .TRUE.
           if (opunits(i).EQ.61) open61 = .TRUE.
           if (opunits(i).EQ.62) open62 = .TRUE.
           if (opunits(i).EQ.63) open63 = .TRUE.
           if (opunits(i).EQ.64) open64 = .TRUE.
           if (opunits(i).EQ.30) open30 = .TRUE.
        endif
      end do


C-----------------------------------------------------------------------
C  READ NAMELIST FILINFO - START OR RESTART CALC. DEPENDING ON NUCCHG
C
C  NOTE:  BMENER AND DENSH NOT USED, CHARGE EXCHANGE IS SWITCHED OFF
C
C-----------------------------------------------------------------------

       NUCCHG = -1
       NPARNT = 0
       SEQ    = '--'
       XRMEMB = '        '
       ioniz  = 'NULL'
       drsup  = 'NULL'
  197  READ(IUNT22,FILINFO)
  
       IF (NUCCHG.LT.0) THEN 	
C
  198     CONTINUE
          IF (OPEN08) CLOSE(IUNT08)
          IF (OPEN09) CLOSE(IUNT09)
          CLOSE(IUNT18)
          CLOSE(IUNT19)
          CLOSE(IUNT22)
          CLOSE(IUNT76)
          CLOSE(IUNT61)
          CLOSE(IUNT62)
          CLOSE(IUNT63)
          CLOSE(IUNT64)
          CLOSE(IUNT30)
C-----------------------------------------------------------------------
C  TELL IDL INFORMATION WIDGET THAT WE'VE COME TO THE END OF THIS INPUT
C  FILE
C-----------------------------------------------------------------------
          IF (.NOT.LBATCH) THEN
             WRITE(PIPEOU,*) MONE
             CALL XXFLSH(PIPEOU)
          ENDIF
C-----------------------------------------------------------------------
C  UNIX-IDL CONVERSION: GO BACK TO THE IDL INPUT SCREEN ONCE PROCESSING
C  IS FINISHED
C-----------------------------------------------------------------------
          GO TO 99
C
       ENDIF
       
       READ(IUNT22,100) JDENSM, JTEM, TS, W, Z, CION, CPY, W1
       
       JSTEM=0
       READ(IUNT22,2000)NIP,INTD,IPRS,ILOW

       READ(IUNT22,134)(DENSA(J),J=1,JDENSM)

       READ(IUNT22,134)(TEA(J),J=1,JTEM)

       READ(IUNT22,134)(DENPA(J),J=1,JDENSM)

       READ(IUNT22,134)(TPA(J),J=1,JTEM)
C
       READ(IUNT22,134)BMENER,DENSH

       FLUX=1.38317498E6*DSQRT(BMENER)*DENSH

       READ(IUNT22,102) NMINRF,NMAXRF,IMAXRF

       READ(IUNT22,103) (NREPRF(I),I=1,IMAXRF)

       READ (IUNT22,134) (WBREPRF(I),I=1,IMAXRF)
       


C-----------------------------------------------------------------------
C	WRITE OUT DATA TO 'adas204.pass4' IF OPEN17=.TRUE.
C-----------------------------------------------------------------------
	IF (OPEN17) THEN
          WRITE(IUNT17,3998) DATE(1:2) , DATE(4:5) , DATE(7:8)
          WRITE(IUNT17,117)
          WRITE(IUNT17,101)JDENSM,JTEM,TS,W,Z,CION,CPY,W1
          WRITE(IUNT17,137)
          WRITE(IUNT17,1999)NIP,INTD,IPRS,ILOW
          WRITE(IUNT17,135)
          WRITE(IUNT17,134)(DENSA(J),J=1,JDENSM)
          WRITE(IUNT17,136)
          WRITE(IUNT17,134)(TEA(J),J=1,JTEM)
          WRITE(IUNT17,141)
          WRITE(IUNT17,134)(DENPA(J),J=1,JDENSM)
          WRITE(IUNT17,142)
          WRITE(IUNT17,134)(TPA(J),J=1,JTEM)
C
          WRITE(IUNT17,145)
          WRITE(IUNT17,146) BMENER,DENSH,FLUX
          WRITE(IUNT17,118)
          WRITE(IUNT17,102) NMINRF,NMAXRF,IMAXRF,NLWS
          WRITE(IUNT17,119)
          WRITE(IUNT17,103) (NREPRF(I),I=1,IMAXRF)

          WRITE(IUNT17,140)
          WRITE(IUNT17,134) (WBREPRF(I),I=1,IMAXRF)
        ENDIF
C-----------------------------------------------------------------------
C  MULTIPLE PARENT/SPIN SYSTEM PARAMETRISED RADIATIVE, DIELECTRONIC
C  AND IONISATION INPUT AND STACKING SECTION.
C  NOTE THE SPIN SYSTEM INDEX ISYS IS INCREMENTED SEQUENTIALLY.  THUS
C  THE FIRST SPIN SYSTEM IS NOT NECESSARILY THAT OF THE GROUND LEVEL.
C  THE INDEX WHICH GIVES THE RECOMBINED METASTABLE ORDERING IS IGRD.
C  IT IS INITAILLY READ FROM THE INPUT AND ASSIGNED AS
C  IGRSTK(IPRT,ISYS) = IGRD
C-----------------------------------------------------------------------
       NGRSTK=0
       NPRSTK=0
       DO 790 IPRT=1,NPRDIM
        NSYSTK(IPRT)=0
        DO 790 ISYS = 1,NSYDIM
          LIOSEL(IPRT,ISYS) = .FALSE.
  790  CONTINUE
       READ(IUNT22,2002)STRING
       IF(STRING(1:5).EQ.'-----')THEN
C
           LRESOL = .TRUE.
C-----------------------------------------------------------------------
C  PARENT/ RESOLVED DATA GROUPS EXPECTED SEPARATED BY SHORT DASHES
C-----------------------------------------------------------------------
  800      READ(IUNT22,2002)STRING
           READ(STRING,2004) IPRT,TRMPRT,ISPR,ICTPR,IGRD,ISPN
           NPRSTK=MAX0(NPRSTK,IPRT)
C
C          NSYSTK(IPRT)=MAX0(NSYSTK(IPRT),ISYS)
           NSYSTK(IPRT)=NSYSTK(IPRT) + 1
           ISYS = NSYSTK(IPRT)
           IGRSTK(IPRT,ISYS) = IGRD
           NGRSTK = MAX0(NGRSTK,IGRD)
           NCTSTK(IPRT,IGRD)=ICTPR
C
           TRMSTK(IPRT)=TRMPRT
           SPRSTK(IPRT)=ISPR
           SPNSTK(IGRD)=ISPN
C-----------------------------------
C  INPUT QUANTUM DEFECT PARMS.
C-----------------------------------
           READ(IUNT22,3077) NMINSTK(IPRT,ISYS), JDEFSTK(IPRT,ISYS) ,
     &                             (DEFSTK(IPRT,ISYS,IDEF), IDEF = 1,5)
C-----------------------------------
C  INPUT RAD. RECOMB. PARMS.
C-----------------------------------
           READ(IUNT22,3040) N0STK(IPRT,ISYS),V0STK(IPRT,ISYS),
     &            PHFSTK(IPRT,ISYS),EDISTK(IPRT,ISYS),SCASTK(IPRT,ISYS)
           READ(IUNT22,3039) NTRSTK(IPRT,ISYS)
C-----------------------------------
C  INPUT DIEL. RECOM. PARMS.
C-----------------------------------
           DO 805 ITRANS=1,NTRSTK(IPRT,ISYS)
            READ(IUNT22,3041)ITYSTK(ITRANS,IPRT,ISYS),
     &      N1ASTK(ITRANS,IPRT,ISYS),
     &      V1ASTK(ITRANS,IPRT,ISYS),PHDSTK(ITRANS,IPRT,ISYS),
     &      NCTAA(ITRANS,IPRT,ISYS),
     &      EPSSTK(ITRANS,IPRT,ISYS),FIJSTK(ITRANS,IPRT,ISYS),
     &      CORSTK(ITRANS,IPRT,ISYS),WIJSTK(ITRANS,IPRT,ISYS)
  805      CONTINUE
           READ(IUNT22,3044)EGPSTK(IPRT,ISYS),SGPSTK(IPRT,ISYS)
C-----------------------------------
C  INPUT COLL. IONIS. PARMS.
C-----------------------------------
           READ(IUNT22,3081) NIGSTK(IPRT,ISYS), NRGSTK(IPRT,ISYS),
     &                       IBSEL1
           IF (IBSEL1 .GT. 0) LIOSEL(IPRT,ISYS) = .TRUE.
           DO JPRT = 1,NPARNT
             READ(IUNT22,3082)  PRTWGT(IPRT,ISYS,JPRT),
     &                                 ISZD(IPRT,ISYS,JPRT)
           ENDDO
           IF(NIGSTK(IPRT,ISYS).GT.0)THEN
               DO 810 IIGRP=1,NIGSTK(IPRT,ISYS)
                READ(IUNT22,3039)NSHSTK(IIGRP,IPRT,ISYS)
                READ(IUNT22,3042)CISTK(IIGRP,IPRT,ISYS),
     &          (IZESTK(ISHEL,IIGRP,IPRT,ISYS),
     &          EIOSTK(ISHEL,IIGRP,IPRT,ISYS),
     &          ISHEL=1,NSHSTK(IIGRP,IPRT,ISYS))
  810          CONTINUE
           ENDIF
           IF(NRGSTK(IPRT,ISYS).GT.0)THEN
               DO 815 IRGRP=1,NRGSTK(IPRT,ISYS)
                READ(IUNT22,3039)NRESTK(IRGRP,IPRT,ISYS)
                READ(IUNT22,3043)CRSTK(IRGRP,IPRT,ISYS),
     &          (WGHSTK(IRESO,IRGRP,IPRT,ISYS),
     &          ENESTK(IRESO,IRGRP,IPRT,ISYS),
     &          IRESO=1,NRESTK(IRGRP,IPRT,ISYS))
  815          CONTINUE
           ENDIF
           READ(IUNT22,134)(AEXSTK(J,IPRT,ISYS),J=1,JTEM)
           READ(IUNT22,134)(SEXSTK(J,IPRT,ISYS),J=1,JTEM)
           
           do j=1,jtem
              AEXSTK(J,IPRT,ISYS)=max(AEXSTK(J,IPRT,ISYS),1.0D-30)
              SEXSTK(J,IPRT,ISYS)=max(AEXSTK(J,IPRT,ISYS),1.0D-30)
           end do
           
           READ(IUNT22,2002)STRING
           IF(STRING(11:15).NE.'-----')THEN
               GO TO 800
           ENDIF
       ELSE
C
           LRESOL = .FALSE.
C-----------------------------------------------------------------------
C   UNRESOLVED CASE SINGLE PARENT/SPIN SYSTEM
C-----------------------------------------------------------------------
           IPRT = 1
           IGRD = 1
           ISYS = 1
           NPRSTK=1
           NGRSTK=1
           NSYSTK(1)   = 1
           NCTSTK(1,1) = NMAXRF
           TRMSTK(1)   = '(1S)'
           SPRSTK(1)   = 1
           SPNSTK(1)   = 2
           IGRSTK(1,1) = 1
C
           READ(STRING,3077) NMINSTK(IPRT,ISYS), JDEFSTK(IPRT,ISYS) ,
     &                             (DEFSTK(IPRT,ISYS,IDEF), IDEF = 1,5)
C
           READ(IUNT22,3040) N0STK(IPRT,ISYS),V0STK(IPRT,ISYS),
     &            PHFSTK(IPRT,ISYS),EDISTK(IPRT,ISYS),SCASTK(IPRT,ISYS)
           READ(IUNT22,3039) NTRSTK(1,1)
           DO 820 ITRANS=1,NTRSTK(1,1)
              READ(IUNT22,3041)ITYSTK(ITRANS,1,1),
     &        N1ASTK(ITRANS,1,1),
     &        V1ASTK(ITRANS,1,1),PHDSTK(ITRANS,1,1),
     &        NCTAA(ITRANS,1,1),
     &        EPSSTK(ITRANS,1,1),FIJSTK(ITRANS,1,1),
     &        CORSTK(ITRANS,1,1),WIJSTK(ITRANS,1,1)
  820      CONTINUE
           READ(IUNT22,3044)EGPSTK(1,1),SGPSTK(1,1)
C
           READ(IUNT22,3081) NIGSTK(IPRT,ISYS), NRGSTK(IPRT,ISYS),
     &                       IBSEL1
           IF (IBSEL1 .GT. 0) LIOSEL(IPRT,ISYS) = .TRUE.
           DO JPRT = 1,NPARNT
             READ(IUNT22,3082)  PRTWGT(IPRT,ISYS,JPRT),
     &                                 ISZD(IPRT,ISYS,JPRT)
           ENDDO
           IF(NIGSTK(1,1).GT.0)THEN
               DO 825 IIGRP=1,NIGSTK(1,1)
                READ(IUNT22,3039)NSHSTK(IIGRP,1,1)
                READ(IUNT22,3042)CISTK(IIGRP,1,1),
     &          (IZESTK(ISHEL,IIGRP,1,1),
     &          EIOSTK(ISHEL,IIGRP,1,1),
     &          ISHEL=1,NSHSTK(IIGRP,1,1))
  825          CONTINUE
           ENDIF
           IF(NRGSTK(1,1).GT.0)THEN
               DO 830 IRGRP=1,NRGSTK(1,1)
                READ(IUNT22,3039)NRESTK(IRGRP,1,1)
                READ(IUNT22,3043)CRSTK(IRGRP,1,1),
     &          (WGHSTK(IRESO,IRGRP,1,1),
     &          ENESTK(IRESO,IRGRP,1,1),
     &          IRESO=1,NRESTK(IRGRP,1,1))
  830          CONTINUE
           ENDIF
           READ(IUNT22,134)(AEXSTK(J,1,1),J=1,JTEM)
           READ(IUNT22,134)(SEXSTK(J,1,1),J=1,JTEM)
           do j=1,jtem
              AEXSTK(J,1,1)=max(AEXSTK(J,1,1),1.0D-30)
              SEXSTK(J,1,1)=max(AEXSTK(J,1,1),1.0D-30)
           end do
       ENDIF

C----------------------------------------------------------------------
C  IF NPRSTK IS NOT EQUAL TO NPARNT THEN END CALCULATION
C----------------------------------------------------------------------
C
       IF(NPRSTK. NE. NPARNT ) THEN
          IF (OPEN17) WRITE(IUNT17,3999) NPRSTK, NPARNT
C----------------------------------------------------------------------
C  INFORM IDL THAT WE'VE FOUND AN ERROR
C----------------------------------------------------------------------
          IF(.NOT.LBATCH)THEN
             WRITE(PIPEOU,*)MTWO
             CALL XXFLSH(PIPEOU)
          ENDIF
          GOTO 5000
       ENDIF
C-----------------------------------------------------------------------
C  ZERO MATRICES WHICH HOLD RESOLVED COEFFICIENTS
C-----------------------------------------------------------------------
       DO 821 JTE=1,NTDIM
         DO 821 JDENS=1,NDDIM
           DO 821 IPRT=1,NPRDIM
             DO 821 JPRT=1,NPRDIM
               DO KPRT=1,NPRDIM
                 SRESP(IPRT,JPRT,KPRT,JTE,JDENS) = 0.0D+0
                 QRESP(IPRT,JPRT,KPRT,JTE,JDENS) = 0.0D+0
               ENDDO
               SRES(IPRT,JPRT,JTE,JDENS)  = 0.0D+0
               QRES(IPRT,JPRT,JTE,JDENS)  = 0.0D+0
               ARES(IPRT,JPRT,JTE,JDENS)  = 0.0D+0
  821  CONTINUE
C-----------------------------------------------------------------------
C  WRITE DETAILS TO PASS1 (IUNT17) FILE ON PARENT/METASTABLE PATHWAYS
C-----------------------------------------------------------------------
       NPATH = 0
       DO JPRT = 1,NPRSTK
         NPATH = NPATH + NSYSTK(JPRT)
       ENDDO
       IF (OPEN17) WRITE(IUNT17,3087) SEQ, NUCCHG, NPRSTK , NPATH
       IF (OPEN17) WRITE(IUNT17,3090)
       DO JPRT = 1, NPRSTK
         DO ISYS = 1, NSYSTK(JPRT)
           IGRD = IGRSTK(JPRT,ISYS)
           SSYSWT = SPNSTK(IGRD)/(2.0D0*SPRSTK(JPRT))
           IF (OPEN17) WRITE(IUNT17,3091) JPRT, TRMSTK(JPRT),
     &             SPRSTK(JPRT), IGRSTK(JPRT,ISYS), SPNSTK(IGRD), SSYSWT
         ENDDO
       ENDDO
C-----------------------------------------------------------------------
C  INITIALISE SWITCHES FOR SUBROUTINE B4PROJ WHICH
C  CALCULATES PROJECTION MATRICES.
C  WRITE HEADER DATA TO CBNM (IUNT18) AND CMBNPR (IUNT19) FILES
C-----------------------------------------------------------------------
       IEDMAT = 0
       IECION = 0
       IETREC = 0
       IEDREC = 0
       IERREC = 0
       IEXREC = 0
       IERSYS = 0
       IF( LPROJ ) THEN
         if (open18) then
            WRITE(IUNT18,3100) SEQ, NUCCHG, NPRSTK , JDENSM, JTEM
            WRITE(IUNT18,3108) IEDMAT, IECION, IETREC, IEDREC, IERREC,
     &                       IEXREC, IERSYS
            WRITE(IUNT18,3101) (DENSA(J),J=1,JDENSM)
            WRITE(IUNT18,3102)(TEA(J),J=1,JTEM)
            WRITE(IUNT18,3103)
         endif
         if (open19) then
            WRITE(IUNT19,3100) SEQ, NUCCHG, NPRSTK , JDENSM, JTEM
            WRITE(IUNT19,3101) (DENSA(J),J=1,JDENSM)
            WRITE(IUNT19,3102)(TEA(J),J=1,JTEM)
            WRITE(IUNT19,3103)
         endif
       ENDIF
C-------------------------------------------------------------------
C  FETCH EXACT RECOMBINATION AND AUGER DATA FROM EXTERNAL DR FILES IF
C  AVAILABLE.
C  SET DR REPRESENT. N-SHELLS AS ALL THE N-SHELLS UP TO NMAXC2 AND
C  THEN ALL INPUT (REFERENCE) REPRESENT. N-SHELLS > NMAXC2.
C-------------------------------------------------------------------
       NBFIL = 0
       call b4flnm(adas_c, adas_u, drsup, dsndr, ldrsel)
       IF( .not.ldrsel) THEN
           INMAXX  =  0
       ELSE
           LDRSEL  = .TRUE.
           INMAXX  = NMAXC2
           DO IREP = 1,INMAXX
             NREPX(IREP) = IREP
           ENDDO
           DO IREP = 1,IMAXRF
              IF( NREPRF(IREP) .GT. NREPX(INMAXX) ) THEN
                   INMAXX = INMAXX + 1
                   NREPX(INMAXX) = NREPRF(IREP)
              ENDIF
           ENDDO
           CALL B4DATD ( XRMEMB , NPRSTK , INMAXX , NREPX , JTEM , TEA,
     &                   NDBFILM, NBFIL  , NCUTMC , AUGM  , DRM  ,
     &                   DRMSF  , PWSAT  , dsndr  , OPEN17,
     &                   dsnin  , adas_c , adas_u
     &                 )
       ENDIF
C-----------------------------------------------------------------------
C  IF UNRESOLVED CASE, SUM DR RATE COEFFICIENT FOR EACH DR REPRESENT.
C  N-SHELL AND FOR THE LOWEST INITIAL PARENT OVER FINAL SPIN SYSTEMS
C  AND PARENTS.
C-----------------------------------------------------------------------
       IF( .NOT. LRESOL) THEN
C
         DO 822 IREP = 1, INMAXX
           DO 822 IT = 1,JTEM
             DRMU(IREP,IT) = 0.0D+00
             DO 822 ISYS = 1,2
               DO 822 JPRT = 1,NDPRTBD
                 DRMU(IREP,IT) = DRMU(IREP,IT)
     &                            + DRM(IREP,IT,1,ISYS,JPRT)
  822    CONTINUE
C
       ENDIF
C-----------------------------------------------------------------------
C  AUGMENT SET OF REFERENCE REPRESENT. N-SHELLS TO INCLUDE
C  N-SHELLS ADJACENT TO ALTERNATIVE AUGER CUT-OFFS.
C  NOTE THE CREATES THE REPRESENT. N-SHELLS FOR THE POPULATION
C  CALCULATION.
C-----------------------------------------------------------------------
       NMAX = NMAXRF
       DO 833 IPRT = 1,NPRSTK
        NCUT = 1000
        DO 831 JPRT = 1,IPRT
         ICTPR = NCUTMC(IPRT,JPRT)
         DO 837 NTST = ICTPR-2, ICTPR+1
           DO 836 IREP=1,IMAXRF-1
            IF(NREPRF(IREP) .EQ. NTST) THEN
               GOTO 836
C  ***** ? GO TO 837  ********
            ELSEIF(NTST.GT.NREPRF(IREP) .AND.NTST.LT.NREPRF(IREP+1))THEN
               DO 834 IR = IMAXRF,IREP+1,-1
                 NREPRF(IR+1) = NREPRF(IR)
  834          CONTINUE
               IMAXRF = IMAXRF+1
               NREPRF(IREP+1) = NTST
C  ***** ? GO TO 837  ********
            ENDIF
  836      CONTINUE
  837    CONTINUE
  831   CONTINUE
  833  CONTINUE

       IF (OPEN17) THEN
          WRITE(IUNT17,149)
          DO IPRT = 1, NPRSTK
            WRITE(IUNT17,150) IPRT, (NCUTMC(IPRT,JPRT) , JPRT = 1,IPRT)
          ENDDO
          WRITE(IUNT17,151) IMAXRF
          WRITE(IUNT17,103) (NREPRF(IREP) , IREP = 1,IMAXRF)
       ENDIF
C
C-----------------------------------------------------------------------
C  SET UP ACTUAL CASE FOR EXECUTION
C
C  START LOOPING THROUGH PARENTS AND SPIN SYSTEMS
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  TELL IDL HOW MANY LOOPS THERE ARE
C-----------------------------------------------------------------------
       IF (.NOT.LBATCH) THEN
          NN=0
          DO 989 I=1,NPRSTK
             NN=NN+NSYSTK(I)*JDENSM*JTEM
 989      CONTINUE
          WRITE(PIPEOU,*)NN
          CALL XXFLSH(PIPEOU)
       ENDIF
C-----------------------------------------------------------------------
C  PARENT LOOP
C-----------------------------------------------------------------------
       DO 514 IPRT=1,NPRSTK
C
        IF( LPROJ ) THEN
           if (open18) WRITE(IUNT18,3104) IPRT, TRMSTK(IPRT), 
     &                                    SPRSTK(IPRT)
           if (open19) WRITE(IUNT19,3104) IPRT, TRMSTK(IPRT), 
     &                                    SPRSTK(IPRT)
        ENDIF
C-----------------------------------------------------------------------
C  SPIN SYSTEM LOOP
C-----------------------------------------------------------------------
        DO 513 ISYS=1,NSYSTK(IPRT)
C
         IGRD = IGRSTK(IPRT,ISYS)
         ICTPR=NCTSTK(IPRT,IGRD)
         SSYSWT = SPNSTK(IGRD)/(2.0D0*SPRSTK(IPRT))
         SSWSTK(IPRT,IGRD) = SSYSWT
         IF (OPEN17) WRITE(IUNT17,2005)IPRT, TRMSTK(IPRT), SPRSTK(IPRT),
     &                 NCTSTK(IPRT,IGRD), IGRD, SPNSTK(IGRD), SSYSWT
         NSHEL = NRESU - NMINSTK(IPRT,ISYS) + 1
         IF( LPROJ ) THEN
            if (open18) WRITE(IUNT18,3105) SPNSTK(IGRD) , NSHEL
            if (open19) WRITE(IUNT19,3105) SPNSTK(IGRD) , NSHEL
         ENDIF
C-----------------------------------------------------------------------
C  SET RADIATIVE RECOMBINATION PARAMETERS FOR PARENT/SPIN SYSTEM
C-----------------------------------------------------------------------
         N0     = N0STK(IPRT,ISYS)
         NMIN   = NMINSTK(IPRT,ISYS)
         V0     = V0STK(IPRT,ISYS)
         PHFRAC = PHFSTK(IPRT,ISYS)
         EDISP  = EDISTK(IPRT,ISYS)
         SCALE  = SCASTK(IPRT,ISYS)
         EIONR  = (Z/V0)**2
C-----------------------------------------------------------------------
C  SET LOW N-SHELL QUANTUM DEFECTS FOR PARENT/SPIN SYSTEM
C-----------------------------------------------------------------------
         JDEF = JDEFSTK(IPRT,ISYS)
         DO IDEF = 1,JDEF
            DEFECT(IDEF) = DEFSTK(IPRT,ISYS,IDEF)
         ENDDO
         IF (OPEN17) THEN
          WRITE(IUNT17,3078) NMIN , JDEF, ( DEFECT(IDEF), IDEF =1,JDEF)
          WRITE(IUNT17,3022) N0, V0, PHFRAC, EDISP, SCALE
	 ENDIF
C-----------------------------------------------------------------------
C  SET DI-ELECTRONIC PARAMETERS FOR PARTICULAR PARENT/SPIN SYSTEM
C-----------------------------------------------------------------------
       NTRANS = NTRSTK(IPRT,ISYS)
       IF (OPEN17) WRITE(IUNT17,3023)NTRANS
       DO 930 ITRANS=1,NTRANS
          ITYPEA(ITRANS) = ITYSTK(ITRANS,IPRT,ISYS)
          N1A(ITRANS)    = N1ASTK(ITRANS,IPRT,ISYS)
          V1A(ITRANS)    = V1ASTK(ITRANS,IPRT,ISYS)
          PHFRA(ITRANS)  = PHDSTK(ITRANS,IPRT,ISYS)
          NCTA(ITRANS)   = NCTAA(ITRANS,IPRT,ISYS)
          EPSIL(ITRANS)  = EPSSTK(ITRANS,IPRT,ISYS)
          FIJ(ITRANS)    = FIJSTK(ITRANS,IPRT,ISYS)
          CORFA(ITRANS)  = CORSTK(ITRANS,IPRT,ISYS)
          WIJ(ITRANS)    = WIJSTK(ITRANS,IPRT,ISYS)
          IF (OPEN17) WRITE(IUNT17,3024) ITYPEA(ITRANS),N1A(ITRANS),
     &                  V1A(ITRANS),PHFRA(ITRANS),NCTA(ITRANS),
     &              EPSIL(ITRANS),FIJ(ITRANS),CORFA(ITRANS),WIJ(ITRANS)
          JCOR = JCORA(ITYPEA(ITRANS))
          DF   = DFA(ITYPEA(ITRANS))
          CFAC = CORFA(ITRANS)
          DO 929 J=1,JCOR
             XL = J-1
             COR(20*(ITRANS-1)+J)=CORA(J,ITYPEA(ITRANS)) *
     &                                      DEXP(-CFAC/(XL**DF+ 0.5D0))
  929     CONTINUE
  930  CONTINUE
       EDISGP = EGPSTK(IPRT,ISYS)
       SCALGP = SGPSTK(IPRT,ISYS)
       IF (OPEN17) WRITE(IUNT17,3047)EDISGP,SCALGP
C-----------------------------------------------------------------------
C  SET IONISATION PARAMETERS FOR PARTICULAR PARENT/SPIN SYSTEM
C-----------------------------------------------------------------------
       NIGRP  = NIGSTK(IPRT,ISYS)
       NRGRP  = NRGSTK(IPRT,ISYS)
       IF (OPEN17) THEN
          IF ( LIOSEL(IPRT,ISYS) ) THEN
             WRITE(IUNT17,3025) NIGRP,NRGRP, 'TRUE '
          ELSE
             WRITE(IUNT17,3025) NIGRP,NRGRP, 'FALSE'
          ENDIF
          DO JPRT = 1,NPRSTK
             WRITE(IUNT17,3030) JPRT,PRTWGT(IPRT,ISYS,JPRT),
     &                          ISZD(IPRT,ISYS,JPRT)
          ENDDO
       ENDIF
C--------------------------------------------------------------
C  SET MINIMUM IONIS. POTEN. FOR PARENT/GROUND SYSTEM FOR SCALING
C--------------------------------------------------------------
       BWNOM(IPRT,IGRD)=0.0D0
       IF(NIGRP.GT.0)THEN
           IF (OPEN17) WRITE(IUNT17,3026)
           DO 840 IIGRP=1,NIGRP
            NSHELA(IIGRP)=NSHSTK(IIGRP,IPRT,ISYS)
            CI(IIGRP)=CISTK(IIGRP,IPRT,ISYS)
            DO 835 ISHEL=1,NSHELA(IIGRP)
             IZETA(ISHEL,IIGRP)=IZESTK(ISHEL,IIGRP,IPRT,ISYS)
             EION(ISHEL,IIGRP)=EIOSTK(ISHEL,IIGRP,IPRT,ISYS)
             IF(BWNOM(IPRT,IGRD).EQ.0.0D0)THEN
                 BWNOM(IPRT,IGRD)=109737.3D0*EION(ISHEL,IIGRP)
             ELSE
                 BWNOM(IPRT,IGRD)=DMIN1(BWNOM(IPRT,IGRD),109737.3D0*
     &                                  EION(ISHEL,IIGRP))
             ENDIF
  835       CONTINUE
            IF (OPEN17) WRITE(IUNT17,3027)CI(IIGRP),(IZETA(ISHEL,IIGRP),
     &                  EION(ISHEL,IIGRP), ISHEL=1,NSHELA(IIGRP))
  840      CONTINUE
       ENDIF
       IF(NRGRP.GT.0)THEN
           IF (OPEN17) WRITE(IUNT17,3028)
           DO 850 IRGRP=1,NRGRP
            NRESOA(IRGRP)=NRESTK(IRGRP,IPRT,ISYS)
            CR(IRGRP)=CRSTK(IRGRP,IPRT,ISYS)
            DO 845 IRESO=1,NRESOA(IRGRP)
             WGHT(IRESO,IRGRP)=WGHSTK(IRESO,IRGRP,IPRT,ISYS)
             ENER(IRESO,IRGRP)=ENESTK(IRESO,IRGRP,IPRT,ISYS)
  845       CONTINUE
            IF (OPEN17) WRITE(IUNT17,3029)CR(IRGRP),(WGHT(IRESO,IRGRP),
     &      ENER(IRESO,IRGRP), IRESO=1,NRESOA(IRGRP))
  850      CONTINUE
       ENDIF
C-----------------------------------------------------------------------
C  SET EXACT ZERO DENSITY RECOM. AND IONIS. COEFFTS.
C  FROM INPUT DATASET FOR PARTICULAR PARENT/SPIN SYSTEM
C-----------------------------------------------------------------------
       DO 860 J=JTEM,1,-1
        ALFEX(J)=AEXSTK(J,IPRT,ISYS)
        SEX(J)=SEXSTK(J,IPRT,ISYS)
C----------------
C  ESTABLISH ESEX
C----------------
        IF(SEX(J).LE.0.0D0)THEN
            ESEX(J)=ESEX(J+1)
        ELSE
            ESEX(J)=DEXP(1.4388D0*BWNOM(IPRT,IGRD)/TEA(J))*SEX(J)
        ENDIF
  860  CONTINUE
       IF (OPEN17) THEN
          WRITE(IUNT17,3054)
          WRITE(IUNT17,134)(ALFEX(J),J=1,JTEM)
          WRITE(IUNT17,3055)
          WRITE(IUNT17,134)(SEX(J),J=1,JTEM)
       ENDIF
C----------------------------------------------------------------------
C  SUPPLEMENT IONISATION  DATA FROM EXTERNAL ADF07 SOURCE
C----------------------------------------------------------------------
       IF( LIOSEL(IPRT,ISYS) ) THEN

           call b4flnm(adas_c, adas_u, ioniz, dsnszd, lexist)

           if (.not.lexist) goto 777  ! no file - abort overwriting

           BWNOM(IPRT,IGRD)=0.0D0
           DO JPRT = 1, NPRSTK
             BWNOSA(IPRT,IGRD,JPRT)=0.0D0
             IBSEL  = ISZD(IPRT,ISYS,JPRT)
             IF (IBSEL .GT. 0 ) THEN
               IZ0IN   = NUCCHG
               ITVAL   = JTEM
               DO JTE = 1,ITVAL
                TVAL(JTE) = TEA(JTE) / 1.1604D+04
               ENDDO
               CALL B4SSZD( dsnszd , IBSEL  , IZ0IN  ,
     &                      ITVAL  , TVAL   ,
     &                      BWNOS  , IZS    , IZ1S   ,
     &                      METIS  , METFS  ,
     &                      SZDA   , ESZDA  , LTRNG  ,
     &                      TITLX  , IRCODE , OPEN17 )
               IF (OPEN17) WRITE(IUNT17,*)'IBSEL,BWNOS,SZDA(2),
     &                         ESZDA(2)=',IBSEL,BWNOS,SZDA(2),ESZDA(2)
               BWNOSA(IPRT,IGRD,JPRT)=BWNOS
C
               IF(BWNOM(IPRT,IGRD).EQ.0.0D0) THEN
                   BWNOM(IPRT,IGRD)=BWNOS
               ELSE
                   BWNOM(IPRT,IGRD)=DMIN1(BWNOM(IPRT,IGRD),BWNOS)
               ENDIF
C
               DO JTE = 1,ITVAL
                 SZDEX(IPRT,IGRD,JPRT,JTE)  = SZDA(JTE)
                 ESZDEX(IPRT,IGRD,JPRT,JTE) = ESZDA(JTE)
               ENDDO
             ELSE
               DO JTE = 1, JTEM
                 SZDEX(IPRT,IGRD,JPRT,JTE)  = 0.00D+00
                 ESZDEX(IPRT,IGRD,JPRT,JTE) = 0.00D+00
               ENDDO
             ENDIF
           ENDDO
C----------------------------------------------------------------------
C  OVERWRITE 'EXACT' ZERO DENSITY IONISATION COEFFTS. FROM INPUT DATASET
C----------------------------------------------------------------------
           DO JTE = 1, JTEM
             ESEX(JTE) = 0.0D+0
             DO JPRT = 1, NPRSTK
               ESEX(JTE) = ESEX(JTE) +
     &                     DEXP(1.4388D0*(BWNOM(IPRT,IGRD)-
     &                          BWNOSA(IPRT,IGRD,JPRT))/TEA(JTE))*
     &                     ESZDEX(IPRT,IGRD,JPRT,JTE)
             ENDDO
C
             EXPON=1.4388D0*BWNOM(IPRT,IGRD)/TEA(JTE)
             IF(EXPON.GT.CUT) THEN
                 SEX(JTE)=0.0D0
             ELSE
                 SEX(JTE)=DEXP(-EXPON)*ESEX(JTE)
             ENDIF
C
           ENDDO
C
           IF (OPEN17) THEN
            WRITE(IUNT17,3083)
            DO JPRT = 1,NPRSTK
              WRITE(IUNT17,3084) JPRT,(SZDEX(IPRT,IGRD,JPRT,JTE),
     &                          JTE = 1,JTEM)
            ENDDO
            WRITE(IUNT17,3085) (SEX(JTE), JTE = 1,JTEM)
C
            WRITE(IUNT17,3092)
            DO JPRT = 1,NPRSTK
              WRITE(IUNT17,3093) JPRT,BWNOSA(IPRT,IGRD,JPRT),
     &                           (ESZDEX(IPRT,IGRD,JPRT,JTE),
     &                           JTE = 1,JTEM)
            ENDDO
            WRITE(IUNT17,3094) BWNOM(IPRT,IGRD),
     &                         (ESEX(JTE), JTE = 1,JTEM)
C
	     ENDIF

 777       continue

        ENDIF
C-----------------------------------------------------------------------
C  START OF CALCULATION
C-----------------------------------------------------------------------
       IF (OPEN17) WRITE(IUNT17,3072)
C
       DO 76 J=1,ITRANS
         IF(WIJ(J)*FIJ(J)-1.0D-50)71,72,72
   71    WEIJ(J)=0.0
         GO TO 76
   72    X=Z+1.0
         X=X*X*EPSIL(J)*157890.0/TS
         IF(X-CUT)73,71,71
   73    IF(X-0.001)74,75,75
   74    WEIJ(J)=WIJ(J)/(X*(1.0+X*(0.5+0.16666667*X)))
         GO TO 76
   75    WEIJ(J)=WIJ(J)/(DEXP(X)-1.0)
   76  CONTINUE
C-----------------------------------------------------------------------
C  START OF MAIN TEMPERATURE AND DENSITY LOOPS
C-----------------------------------------------------------------------
       DO 401 JDENS=1,JDENSM
C
         DENS  = DENSA(JDENS)
         DENSP = DENPA(JDENS)
         PY    = 0.0
         PYP   = 0.0
C
       DO 400 JTE=1,JTEM
C
          DO I = 1,NREPDM
            DO JPRT = 1,NPRSTK
             CIONRI(JPRT,I) = 0.00D+0
             CIONRA(JPRT,I) = 0.00D+0
            ENDDO
            CIONPT(I) = 0.00D+0
            RHSC(I)   = 0.0D+0
            RHSI(I)   = 0.0D+0
            RHSIRC(I) = 0.0D+0
          ENDDO
C-----------------------------------------------------------------------
C  SET REPRESENTATIVE LEVELS FOR EACH CALCULATION
C  SET DILUTION FIELD EQUAL TO ZERO
C-----------------------------------------------------------------------
       NCALC = 2
C
       DO 4000 ICALC = 1, NCALC
C
         IF(ICALC .LE. 1) THEN
           NMAX =  NMAXRF
           IMAX =  0
           DO 4001 IREP=1,IMAXRF
             IF(NREPRF(IREP).GE.NMIN.AND.NREPRF(IREP).LE.NMAX)THEN
                IMAX        = IMAX+1
                NREP(IMAX)  = NREPRF(IREP)
                NREPC(IMAX) = NREPRF(IREP)
C               WBREP(IMAX) = WBREPRF(IREP)
                WBREP(IMAX) = 0.0D+0
             ENDIF
 4001      CONTINUE
           IMAXC1 = IMAX
           NMAXC1 = NMAXRF
         ELSE
           NMAX = NMAXC2
           IMAX = 0
           DO 4002 IREP=1,NMAXC2
             IF(IREP.GE.NMIN .AND. IREP.LE.NMAX) THEN
                IMAX        = IMAX+1
                NREP(IMAX)  = IREP
                NREPI(IMAX) = NREPRF(IREP)
C               WBREP(IMAX) = WBREPRF(IREP)
                WBREP(IMAX) = 0.0D+0
             ENDIF
 4002      CONTINUE
           IMAXC2 = IMAX
         ENDIF
C-----------------------------------------------------------------------
C  DEFINE GENERAL VECTORS AND VARIABLES
C-----------------------------------------------------------------------
       TE   =  TEA(JTE)
       TP   =  TPA(JTE)
       X    =  157890.0*Z*Z
       ATE  =  X/TE
       ATS  =  X/TS
       RHO  =  1.27787D-17/Z**7
       RHOP =  RHO*DENSP*DSQRT(X/TP)
       RHO  =  RHO*DENS*DSQRT(ATE)
       PY0  = -DLOG(ATE)-0.41
C
       DO 79 J=1,NTRANS
         IF(FIJ(J)-0.000001)77,77,78
   77    EEIJ(J)=0.0
         GO TO 79
   78    X=Z+1.0
         X=X*X*EPSIL(J)*157890.0/TE
         IF(X.GT.CUT)THEN
             EEIJ(J)=0.0D0
         ELSE
             EEIJ(J)=DEXP(-X)
         ENDIF
   79  CONTINUE
C
       X = NMAX
       ENMAX2 = 1.0 / ((X+0.5)*(X+0.5))
       EYE    = DEXP(ATE*ENMAX2)
C
       IF(ICALC .LE. 1 .AND. OPEN08) THEN
         WRITE(IUNT08,131)
         WRITE(IUNT08,133) IPRT, ISYS, ICTPR, DENS, TE ,
     &                     DENSP,TP, ATE, ATS, RHO
       ENDIF
       if(icalc .le. 1 .and. open30) then
         write(iunt30,131)
         write(iunt30,133) iprt, isys, ictpr, dens, te ,
     &                     densp,tp, ate, ats, rho
       endif
C
       NMAX1=NMAX+NIP
       DO 64 N=1,NMIN
         EN=N
         EN23(N)=EN**(-0.6666667)
   64  CONTINUE
       DO 202 N=NMIN,NMAX1
         EN=N
         DEF=0.0
         J=N+1-NMIN
         IF(J-JDEF)191,191,192
  191    DEF=DEFECT(J)
  192    A1=1.0/(EN-DEF)
         EN2(N)=A1*A1
         EN3(N)=EN2(N)*A1
         EN23(N)=EN**(-0.6666667)
         FLAG(N)=FINTER(EN)
C
         EXPON=ATE*EN2(N)
         IF(EXPON.GT.CUT)THEN
             DEXPTE(N)=DEXP(CUT)
         ELSE
             DEXPTE(N)=DEXP(EXPON)
         ENDIF
C
         X=ATS*EN2(N)
         IF(X-CUT)201,200,200
  200    DEXPTS(N)=0.0
         GO TO 202
  201    DEXPTS(N)=DEXP(X)
  202  CONTINUE
C-----------------------------------------------------------------------
C  SET UP VARIABLES AND VECTORS ASSOCIATED WITH INTERPOLATION
C-----------------------------------------------------------------------
       DO 1 N=1,NMAX
         EN=N
         OGARL(N) = DLOG(EN)
         N1 = NMAX+N
         EN1= N1
         OGARL(N1) = DLOG(EN1)
    1  CONTINUE
       NREP(IMAX+1) = NMAX+NMAX
       I=3
       N1=NMIN+2
       DO 4 N=N1,NMAX
         IF(N+N-NREP(I)-NREP(I+1))3,3,2
    2    I=I+1
    3    INTER(N)=I
    4  CONTINUE
       INTER(NMIN)=2
       INTER(NMIN+1)=2
       DO 80 I=1,IMAX+1
         VEC(I)  = 0.0D0
         DG(I)   = 0.0D0
         DGS1(I) = 0.0D0
         DGS2(I) = 0.0D0
   80  CONTINUE
       DO 8 N=NMIN,NMAX
          X  = FLAG(N)
          I  = INTER(N)
          I1 = NREP(I-1)
          I2 = NREP(I)
          X1 = FLAG(I1)
          X2 = FLAG(I2)
          IF(IMAX-I)6,6,5
    5     I3 = NREP(I+1)
          X3 = FLAG(I3)
          GO TO 7
    6     X3=0.0
    7     AGRL(N,1) = (X-X2)*(X-X3)/((X1-X2)*(X1-X3))
          AGRL(N,2) = (X-X1)*(X-X3)/((X2-X1)*(X2-X3))
          AGRL(N,3) = (X-X1)*(X-X2)/((X3-X1)*(X3-X2))
          DG(I-1)   = DG(I-1)  + AGRL(N,1)**2
          DG(I)     = DG(I)    + AGRL(N,2)**2
          DG(I+1)   = DG(I+1)  + AGRL(N,3)**2
          DGS1(I-1) = DGS1(I-1)+ AGRL(N,1)*AGRL(N,2)
          DGS1(I)   = DGS1(I)  + AGRL(N,2)*AGRL(N,3)
          DGS2(I-1) = DGS2(I-1)+ AGRL(N,1)*AGRL(N,3)
          VEC(I-1)  = VEC(I-1) + AGRL(N,1)
          VEC(I)    = VEC(I)   + AGRL(N,2)
          VEC(I+1)  = VEC(I+1) + AGRL(N,3)
    8  CONTINUE
C-----------------------------------------------------------------------
C  SET UP ARRAY OF IMPACT PARAMETER PY FACTORS
C-----------------------------------------------------------------------
       ZCOL=Z-1.0
       IF(NIP .GT. 0) THEN
          DO 500 N=NMIN,NMAX
            EN=N
            DO 501 I=1,NIP
              N11=N+I
              EN11=N11
              D=EN2(N)-EN2(N11)
              D1=1.0/(D*D)
              PHI=1.56*EN3(N)*EN2(N)*EN3(N11)*D1*D1/(Z*Z)
              WI=2.0*EN*EN
              WJ=2.0*EN11*EN11
              R=0.25*(5.0*EN*EN+1.0)/Z
              PY  = 0.0D+0
              PYP = 0.0D+0
              IF(DENS .GT. 0.0D+0 ) THEN
                 CALL PYIP(D,1.0D0,ZCOL,PHI,CPY,WI,WJ,R,TE,INTD,PY)
              ENDIF
              IF(DENSP .GT. 0.0D+0) THEN
                 CALL PYIP(D,EM,ZCOL,PHI,CPY,WI,WJ,R,TP,INTD,PYP)
              ENDIF
              PYMAT(N,I) = PY*RHO+PYP*RHOP
  501       CONTINUE
  500     CONTINUE
       ENDIF
C-----------------------------------------------------------------------
C  SET UP VECTOR OF LYMAN LINE DILUTIONS
C-----------------------------------------------------------------------
       IF(WBREP(1))60,60,62
   60  DO 61 N=NMIN,NMAX
         WB(N)=W
   61  CONTINUE
       GO TO 63
   62  DO 49 I=1,IMAX
         WBLOG(I)=DLOG(WBREP(I))
   49  CONTINUE
       N1=NREP(IMAX-1)-1
       DO 50 N=NMIN,N1
         I = INTER(N)
         W2= WBLOG(I-1)*AGRL(N,1)+WBLOG(I)*AGRL(N,2)+WBLOG(I+1)
     1       *AGRL(N,3)
         WB(N)=DEXP(W2)
   50  CONTINUE
       W2=WBREP(IMAX)
       N1=N1+1
       DO 48 N=N1,NMAX
         WB(N)=W2
   48  CONTINUE
   63  CONTINUE
C      WRITE(IUNT08,134)(WB(N),N=NMIN,NMAX)
       IF (OPEN08) WRITE(IUNT08,128)
C-----------------------------------------------------------------------
C  ZERO ARRAYS AND VARIABLES FOR POWER AND ENERGY LOSS COEFFICIENTS
C  DEFINE VALUE OF CPL - CONVERSION FACTOR FOR POWER LOSS
C-----------------------------------------------------------------------
       DO IBIN = 1,NBIN
         RLBIN(IBIN) = 0.0D+0
       ENDDO
       RL  = 0.0D0
       RD  = 0.0D0
       RR  = 0.0D0
       RL0 = 0.0D0
       AL0 = 0.0D0
       QRT = 0.0D+0
       QRD = 0.0D+0
       QRR = 0.0D+0
       QCR = 0.0D+0
       DO I = 1,IMAX+1
          PREP(I) = 0.0D0
          DO IBIN = 1,NBIN
            PREPBIN(I,IBIN) = 0.0D+00
            RLREPBN(I,IBIN) = 0.0D+00
          ENDDO
          RLREP(I)  = 0.0D0
          RDREP(I)  = 0.0D0
          RRREP(I)  = 0.0D0
          QCREP(I)  = 0.0D0
          QCREPC(I) = 0.0D0
          QRDREP(I) = 0.0D0
          QRTREP(I) = 0.0D0
          QRRREP(I) = 0.0D0
       ENDDO
C
       CPL = 2.26530D-24*DENS*(157890.0/TE)**1.5*Z**6
C-----------------------------------------------------------------------
C START OF MAIN LOOP THROUGH REPRESENT. N-SHELLS
C-----------------------------------------------------------------------
       DO 30 I=1,IMAX
C
       N=NREP(I)
       EN=N
C
       RHS(I)  = 0.0
       RHSREC(I) = 0.0D+0
       DO JPRT = 1,NPRSTK
         ALOSSR(JPRT,I) = 0.00D+0
         ALOSSI(JPRT,I) = 0.00D+0
         ALOSSA(JPRT,I) = 0.00D+0
       ENDDO
       A(N)   = 0.0
       AC(N)  = 0.0
       ACI(N) = 0.0
C-----------------------------------------------------------------------
C  DEFINE CROSS-REFERENCING N-SHELL FOR DR RECOMBINATION AND AUGER DATA
C-----------------------------------------------------------------------
       IBD = 0
       IF( LDRSEL) THEN
          DO IX = 1,INMAXX
            NTEST = NREPX(IX)
            IF( N .EQ. NTEST) IBD = IX
          ENDDO
       ENDIF
       IF( IBD .LE. 0) LDRSEL =  .FALSE.
C-----------------------------------------------------------------------
C  CONTRIBUTIONS FROM N-SHELLS WITH N11 > NREP(I) TO COLL.RAD. MATRIX ROW
C-----------------------------------------------------------------------
       N1=N+1
C
       DO 16 N11=N1,NMAX
C
       W2=W
       IF(N-NMIN)51,51,52
   51  W2=WB(N11)
   52  CONTINUE
       D=EN2(N)-EN2(N11)
       Y=ATE*D
       YS=ATS*D
       D=1.0/D
       EN11=N11
       X2=EN2(N11)
       N2=N11-N
       X=EN23(N)*EN23(N2)/EN23(N11-1)
       G=GBB(EN11,EN,X2,X)
       A1=EN3(N)*EN3(N11)*D*G
   11  FACTOR=1.0D0
       IF(IPRS.GT.0)GOTO 13
         CALL PYVR(Y,Z,PY)
         GOTO 15
   13   EI=EN2(N)
        WI=2.0D0*EN*EN
        EJ=EN2(N11)
        WJ=2.0D0*EN11*EN11
        F=1.96028D0*G*D*D*D*EN3(N)*EN2(N)*EN3(N11)
        PHI=F*D/(Z*Z)
        CALL PYPR(EI,EJ,N,N11,1.0D0,Z,PHI,WI,WJ,TE,INTD,PY,RDEXC)
   15  PY=PY*FACTOR*RHO
       NTEST=N11-N
       IF(NTEST-NIP)195,195,196
  195  PY=PYMAT(N,NTEST)
  196  CONTINUE
       IF(YS-0.001)203,203,204
  203  A2=W2/(YS+0.5*YS*YS)
       GO TO 209
  204  IF(DEXPTS(N))205,205,208
  205  IF(YS-CUT)207,206,206
  206  A2=0.0
       GO TO 209
  207  A2=W2/(DEXP(YS)-1.0)
       GO TO 209
  208  A2=W2/(DEXPTS(N)/DEXPTS(N11)-1.0)
  209  A3=3.06998*D*D*D*PY
C
       IF(N.LT.NLWS.AND.N11.LT.NLWS)THEN
           A(N11)=-A1*(1.0+A2+A3)
           EXPON=ATE*(EN2(N)-EN2(N11))
           IF(EXPON.GT.CUT)THEN
               A(N)=A(N)+A1*A2
           ELSE
               A(N)=A(N)+A1*(A2+A3*DEXP(-EXPON))
           ENDIF
       ELSEIF(N.LT.NLWS.AND.N11.GE.NLWS)THEN
           A(N11)=-A1*(1.0+A2+A3)*DEXPTE(N11)
           EXPON=ATE*(EN2(N)-EN2(N11))
           IF(EXPON.GT.CUT)THEN
               A(N)=A(N)+A1*A2
           ELSE
               A(N)=A(N)+A1*(A2+A3*DEXP(-EXPON))
           ENDIF
           RHS(I)=RHS(I)+A1*(1.0+A2+A3)*DEXPTE(N11)
       ELSEIF(N.GE.NLWS)THEN
           A(N11)=-A1*(1.0+A2+A3)*DEXPTE(N11)
           A(N)=A(N)+A1*(A2*DEXPTE(N)+A3*DEXPTE(N11))
           RHS(I)=RHS(I)+A1*(DEXPTE(N11)  +A2*(DEXPTE(N11)-DEXPTE(N)))
       ENDIF
C------PREPARE ARRAYS FOR CALL TO <V2CLDBN>  WJD 9/6/91--------
       IF(ICALC .EQ. 1) THEN
         AC(N11)=-A1*(1.0+A2+A3)*DEXPTE(N11)
         AC(N)=AC(N)+A1*(A2*DEXPTE(N)+A3*DEXPTE(N11))
         RHSC(I)=RHSC(I)+A1*(DEXPTE(N11)  +A2*(DEXPTE(N11)-DEXPTE(N)))
       ELSE
         ACI(N11)=-A1*(1.0+A2+A3)*DEXPTE(N11)
         ACI(N)=ACI(N)+A1*(A2*DEXPTE(N)+A3*DEXPTE(N11))
         RHSI(I)=RHSI(I)+A1*(DEXPTE(N11)  +A2*(DEXPTE(N11)-DEXPTE(N)))
       ENDIF
C-------------------------------------
C  EVALUATE ELECTRON COOLING COEFFICIENT
C-------------------------------------
C      IF( LECOOL ) THEN
C        DELE = -1.0D+0 * DABS( EN2(N) - EN2(N11) )
C        QCT = 0.0D+0
C        IF( N .LT. NLWS )THEN
C            EXPON=ATE*(EN2(N)-EN2(N11))
C            IF(EXPON.GT.CUT)THEN
C                QCT = 0.0D+0
C            ELSE
C                QCT = DELE * A1*A3*DEXP(-EXPON)
C            ENDIF
C        ELSEIF(N.GE.NLWS)THEN
C            QCT = DELE * A1*A3*DEXPTE(N11)
C        ENDIF
C        QCREP(I) = QCREP(I) + QCT
C      ENDIF
C
C-------------------------------------
   16  CONTINUE
C-----------------------------------------------------------------------
C  NOW CONSIDER DIRECT LOSSES FROM, AND RECOMBINATION TO NREP(I)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  PREPARE IONISATION RATE DATA.
C  IF LIOSEL THEN DRAW IONISATION RATE FOR NMIN FROM ADF07 DATA FILE
C  OTHERWISE SPLIT UP EXACT IONISATION DATA BY PARENT WEIGHTS.
C-----------------------------------------------------------------------
       YE=ATE*(EN2(N)-ENMAX2)
       YS=ATS*(EN2(N)-ENMAX2)
       ENC=1.0/DSQRT(EN2(N)-ENMAX2)
       CALL COLINT(YE,Z,ENC,AI)
       AI=AI/CION
       W2=W
C
       IF( N .LE. NMIN) THEN

         AI=CION*AI
         W2=W1
           ESECIP=0.43171*RHO*AI*ENC*ENC*1.57456D10*Z**4/DENS
         IF(ATE*EN2(N).LT.CUT)THEN
           SECIP=ESECIP/DEXPTE(N)
         ELSE
           SECIP=0.0D0
         ENDIF
C
         IF( LIOSEL(IPRT,ISYS) ) THEN
           ESGRD = 0.0D+0
           DO JPRT = 1, NPRSTK
             ESEXPRT(JPRT) = ESZDEX(IPRT,IGRD,JPRT,JTE)
             expon = 1.4338D0*(BWNOM(IPRT,IGRD)-
     &                         BWNOSA(IPRT,IGRD,JPRT))/TEA(JTE)
             if (expon.LT.cut) then
                ESGRD = ESGRD + DEXP(expon)*ESEXPRT(JPRT)
             else
                esgrd = 1.0D50
             endif
           ENDDO
         ELSE
           ESGRD = ESEX(JTE)
           DO JPRT = 1, NPRSTK
             ESEXPRT(JPRT) = ESGRD * PRTWGT(IPRT,ISYS,JPRT)
           ENDDO
         ENDIF
         EXPON = 1.4338D0*BWNOM(IPRT,IGRD)/TEA(JTE)
         IF(EXPON.LT.CUT)THEN
             SGRD=DEXP(-EXPON)*ESGRD
         ELSE
             SGRD=0.0D0
         ENDIF
         S0(JTE)=SGRD
         IF(ESECIP.GT.0.0D0.and.esgrd.lt.1.0D49)THEN
             AI=AI*ESGRD/ESECIP
         ENDIF
       ENDIF
       AION = 0.43171*RHO*AI*EN*EN*ENC*ENC
C
C----------------------------------
C  PREPARE AUTO-IONISATION RATE
C----------------------------------
C
C      IF(N-NMIN) 217,217,216
  216  CONTINUE
       AAG = 0.00D+0
       DO JPRT = 1, IPRT
C         IF(LDRSEL .AND. N .GT. NMIN ) THEN
          IF(LDRSEL .AND. N .GT. ICTPR ) THEN
            ALOSSA(JPRT,I) = AUGM(IBD,IPRT,ISYS,JPRT)
          ELSE
            ALOSSA(JPRT,I) = 0.0D+0
          ENDIF
          AAG = AAG + ALOSSA(JPRT,I)
       ENDDO
C
       AAGFAC  =  EN * EN * AAG /( Z**4 * 1.57456D10)
       IF(N.GE.NLWS)THEN
           AAGFAC = DEXPTE(N)*AAGFAC
       ENDIF
C
C-----------------------------------
C  PREPARE PHOTO-IONISATION RATE
C-----------------------------------
C
  217  IF(YS-CUT)223,223,220
C
  220  X=YS-ATE*EN2(N)
       IF(X-CUT)222,222,221
  221  X1=0.0
       X2=0.0
       GO TO 224
  222  X1=DEXP(-X)/(YS+1.0)
       X2=X1*DEXP(-YE)
       GO TO 224
C ******** SWITCH OFF PSTIM  ************
  223  CALL PHOTOLT(PION,PREC,PSTIM,Z,TE,TS,ENC,N,NLWS,1,0,0)
       IF(N.GE.NLWS)THEN
           X1=DEXPTE(N)*PION
           X2=DEXPTE(N)*PSTIM
       ELSE
           X1=PION
           X2=PSTIM
       ENDIF
C
  224  CONTINUE
       APION = 0.5*W2*EN3(N)*X1
C---------------------------------------
C  ADD ON CONTRIBUTIONS FROM IONISATION
C  AND AUTO-IONISATION TO LOSS MATRIX
C
C   CIONPT - ARRAY FOR B4PROJ
C   EXCLUDE AUGER TRANSITION PROBABILITES
C   FOR LEVELS LE NRESU
C---------------------------------------
C
        CIONPTS = 0.0D+0
        DO JPRT = 1, IPRT
            ALOSSA(JPRT,I) = EN * EN * ALOSSA(JPRT,I) /
     &                                    ( Z**4 * 1.57456D10)
            IF(N.GT.NRESU) CIONPTS = CIONPTS + DEXPTE(N)*ALOSSA(JPRT,I)
            IF(ICALC .EQ. 2) CIONRA(JPRT,I) = DEXPTE(N)*ALOSSA(JPRT,I)
C
            IF(N.GE.NLWS)THEN
               ALOSSA(JPRT,I) = DEXPTE(N)*ALOSSA(JPRT,I)
            ENDIF
C
            IF( N .LE. NMIN) THEN
                IF( SGRD.GT.0.0D0) THEN
C                   ALOSSI(JPRT,I) = SEXPRT(JPRT) / SGRD * AION
          alossi(jprt,i) = esexprt(jprt) / esgrd * aion
                ELSE
                    ALOSSI(JPRT,I) = 0.0D+0
                ENDIF
            ELSE
                ALOSSI(JPRT,I) = AION * PRTWGT(IPRT,ISYS,JPRT)
            ENDIF
C
            CIONPTS = CIONPTS + ALOSSI(JPRT,I)
            IF(ICALC .EQ. 2)  CIONRI(JPRT,I) = ALOSSI(JPRT,I)
            IF(N.GE.NLWS)THEN
               ALOSSR(JPRT,I) = ALOSSA(JPRT,I) + ALOSSI(JPRT,I)
            ELSE
              IF(ATE*EN2(N).LE.CUT)THEN
                  ALOSSR(JPRT,I)=ALOSSA(JPRT,I)+ALOSSI(JPRT,I)/DEXPTE(N)
              ELSE
                  ALOSSR(JPRT,I) = ALOSSA(JPRT,I) + 0.0D+0
              ENDIF
            ENDIF
        ENDDO
C---------------------------------------
C  ADD CONTRIBUTIONS FROM IONISATION
C  AND PHOTO-IONISATION TO
C  COLLISIONAL-RADIATIVE MATRICES
C  SUBTRACT THREE BODY RECOMBINATION
C  CONTRIBUTION FROM RHS FOR LEVELS < N0
C---------------------------------------
       IF(N.GE.NLWS)THEN
           IF(N.EQ.NREP(1)) ADIR = A(N) + AION
           A(N) = A(N) + AION + APION + AAGFAC
           RHS(I)  = RHS(I) - AAGFAC
           IF(N .LT. N0) RHS(I) = RHS(I) - AION
       ELSE
           IF(N.EQ.NREP(1))THEN
               RHDIR = AION
               IF(N .LT. N0) RHDIR = RHDIR - AION
               IF(ATE*EN2(N).LE.CUT)THEN
                   ADIR = A(N) + AION/DEXPTE(N)
               ELSE
                   ADIR = A(N)
               ENDIF
           ENDIF
           IF(ATE*EN2(N).LE.CUT)THEN
               A(N) = A(N) + AION/DEXPTE(N) + APION + AAGFAC
           ELSE
               A(N) = A(N) + APION + AAGFAC
           ENDIF
           RHS(I) = RHS(I) + AION + APION
           IF(N .LT. N0) RHS(I) = RHS(I) - AION
       ENDIF
       IF(N .GE. N0) RHSREC(I) = RHSREC(I) + AION
C
C------PREPARE MATRICES FOR CALL TO B4PROJ
       AAGMUL = 1.0
       IF(N .LT. NLWS) AAGMUL = DEXPTE(N)
       IF(ICALC .EQ. 1) THEN
         CIONPT(I) = CIONPTS
         AC(N) = AC(N) + CIONPTS
         IF (N .GT. NRESU) RHSC(I)  = RHSC(I) - AAGFAC*AAGMUL
       ELSE
         ACI(N) = ACI(N) + CIONPTS
         RHSI(I)  = RHSI(I) - AAGFAC*AAGMUL
         IF(N .GE. N0) RHSIRC(I) = RHSIRC(I) + AION
       ENDIF
       IF(N .LT. N0) RHSC(I) = RHSC(I) - AION
       IF(ICALC. EQ. 1) THEN
         TRECPT(I) = AION
         IF(N .LT. N0) TRECPT(I) = 0.0D+0
       ENDIF
C------------------------------------------------
C  CONTRIBUTIONS TO ELECTRON COOLING COEFF
C  FROM IONISATION AND THREE-BODY RECOMB.
C------------------------------------------------
       IF( LECOOL) THEN
          DELE = -1.0 * DABS( 1.0 / (ENC*ENC) )
C
          QCT = 0.0D+0
          IF(N.LT.NLWS)THEN
            IF(ATE*EN2(N).LE.CUT)THEN
               QCT = DELE * AION / DEXPTE(N)
            ELSE
               QCT = 0.0D+0
            ENDIF
          ELSE
             QCT = DELE * AION
          ENDIF
          QCREP(I) = QCREP(I) + QCT
C
          QRTREP(I) = -1.0 * CPL * DELE * AION
C
       ENDIF
C---------------------------------------
C  PREPARE DIELECTRONIC COEFFICIENT
C  ADJUSTMENT BASED ON EDISGP,SCALGP
C---------------------------------------
C
       ADS=0.0
C
       IF(N-NMIN) 303,302,302
C
  302  CONTINUE
       FGP=SCALGP*DEXP(ATE*EDISGP/(Z**2))
       DO 301 J=1,NTRANS
           IF( N .GT. NCTA(J) ) GO TO 301
           F=FIJ(J)
           EIJ=EPSIL(J)
           WEIJJ=WEIJ(J)
           IF(F-0.000001) 301,301,300
  300      IF(N.LT.N1A(J))GO TO 301
           IF(N.EQ.N1A(J))THEN
             DEFN=N-V1A(J)
             PHF=PHFRA(J)
             EXPON=(Z+1.0)*(Z+1.0)*EIJ*157890.0/TE - ATE/(V1A(J)**2)
             SATEN = ( (Z+1.0)/Z)**2*EIJ-1.0/(V1A(J)**2)
           ELSE
             DEFN=0.0D0
             PHF=1.0D0
             EXPON = (Z+1.0)*(Z+1.0)*EIJ*157890.0/TE - ATE*EN2(N)
             SATEN = ((Z+1.0)/Z)**2*EIJ - EN2(N)
           ENDIF
           IMARK = 20*(J-1)+1
           JCOR  = 20
           CALL DIELCL(Z,EIJ,F,WEIJJ,COR(IMARK),JCOR,N,DEFN,AD,L,CPT)
           AD=FGP*PHF*AD
C
           IF(N.LT.NLWS)THEN
               IF(EXPON.LE.CUT)THEN
                   ADS = ADS + AD*(1.0+WEIJJ)*DEXP(-EXPON)
               ENDIF
               IF(N.EQ.NREP(1)) ADIR = ADIR + AD*WEIJJ
                A(N) = A(N) + AD*WEIJJ
           ELSE
               ADS = ADS + AD*(EEIJ(J) - WEIJJ*(1.0-EEIJ(J)))*DEXPTE(N)
               IF(N.EQ.NREP(1)) ADIR=ADIR+AD*WEIJJ*DEXPTE(N)
               A(N) = A(N) + AD*WEIJJ*DEXPTE(N)
           ENDIF
C
C-------------------------------------
C  EVALUATE DIELECTRONIC RECOMBINATION
C  POWER COEFFICIENT
C-------------------------------------
C
           IF(EXPON.LE.CUT)THEN
              RAD=SATEN*AD*DEXP(-EXPON)
           ELSE
              RAD=0.0D0
           ENDIF
           RDREP(I)=RDREP(I)+RAD
C
  301  CONTINUE
C-------------------------------------------
C  SUBSTITUTE EXTERNAL DR DATA FROM FILE
C  IF AVAILABLE
C-------------------------------------------
       C1REC = 1.03928D-13 * (Z**4) * ( (157890.0/TE)**1.5 )
       IF( LDRSEL ) THEN
         ADSGP = ADS
         IF( LRESOL) THEN
           ADS = DRM(IBD,JTE,IPRT,ISYS,IPRT) / ( C1REC * SSYSWT )
         ELSE
           ADS = DRMU(IBD,JTE) / ( C1REC * SSYSWT )
         ENDIF
         IF( ADSGP .GT. 1.0D-60) THEN
             RDREP(I)=  (ADS / ADSGP) * RDREP(I)
         ELSE
             RDREP(I) = 0.0D+0
         ENDIF
       ENDIF
C
        RDREP(I)=CPL*RDREP(I)
C
  303  CONTINUE
C
C--------------------------------------
C  EVALUATE RADIATIVE RECOMBINATION AND
C  RECOMBINATION POWER COEFFICIENTS.
C  SWITCH TO SCALED VON GOELER RATE IF
C  RECOMBINING TO THE GROUND LEVEL
C--------------------------------------
C
       IF(N.GE.N0) THEN
          CALL PHOTOLT(PION,PREC,PSTIM,Z,TE,TS,ENC,N,NLWS,0,1,0)
          IF(N.GE.NLWS)THEN
              X3 = DEXPTE(N)*PREC
          ELSE
              X3 = PREC
          ENDIF
          CALL R2PHOTO(ATE,EN,RREC,QRREC)
          RX3  = 0.5*EN3(N) *  RREC/ATE
          IF (LECOOL) QRX3 = 0.5*EN3(N) * QRREC/ATE
          IF(N .EQ. N0) THEN
             ADJUST = SCALE*PHFRAC*(1.57890D5*EIONR/TE)**EDISP
             X3     = ADJUST * PREC
             RX3    = ADJUST * RX3
             IF (LECOOL) QRX3   = ADJUST * QRX3
          ENDIF
       ELSEIF(N .LT. N0) THEN
          X3   = 0.0
          RX3  = 0.0
          IF (LECOOL) QRX3 = 0.0
       ENDIF
C
       X4   = 0.0D+0
  494  X4 = 9.62134D12*DENSH*X4/(DENS*Z*ATE**1.5D0)
C
C----------------------------
C  ADD RECOMBINATION CONTRIBUTIONS
C  TO C-R MATRICES
C----------------------------
       IF(I.EQ.1) RHSD = RHS(I) + 0.5*EN3(N)*(W*X2+X3)  + ADS + X4
       RHS(I)    = RHS(I) + 0.5*EN3(N)*(W*X2-W2*X1 +X3) + ADS + X4
       RHSREC(I) = RHSREC(I) + 0.5*EN3(N)*X3 + ADS + X4
       AL0REP(I) = 1.03936D-13*Z*ATE**1.5D0*(0.5*EN3(N)*
     &             (W*X2+X3)+ADS+X4)
C
C-----------------------------
C  SET POWER LOSS COEFFICIENTS
C-----------------------------
C
       RRREP(I)  = CPL * RX3
       RL0REP(I) = CPL*(EN2(NMIN)-EN2(N))*(0.5*EN3(N)*(W*X2+
     &                                                 X3)+ADS+X4)/DENS
       IF( LECOOL) QRRREP(I) = CPL * QRX3
C------PREPARE ARRAYS FOR CALL TO <B4PROJ>
       IF(ICALC .EQ. 1) THEN
         RHSC(I)   = RHSC(I) + 0.5*EN3(N)*X3  +  ADS + X4
         DRECPT(I) = ADS
         RRECPT(I) = 0.5*EN3(N)*X3
         XRECPT(I) = X4
       ELSE
         RHSI(I)   = RHSI(I)   + 0.5*EN3(N)*X3  +  ADS + X4
         RHSIRC(I) = RHSIRC(I) + 0.5*EN3(N)*X3  +  ADS + X4
       ENDIF
C-----------------------------------------------------------------------
C  CONTRIBUTIONS FROM N-SHELLS WITH N1 < NREP(I) TO COLL.RAD. MATRIX ROW
C-----------------------------------------------------------------------
       PREP(I)   = 0.0D0
       DO IBIN = 1,NBIN
         PREPBIN(I,IBIN) = 0.0D+0
       ENDDO
C
       IF(N-NMIN) 26,26,17
C
   17  N2=N-1
C
       DO 25 N1=NMIN,N2
C
       W2=W
       IF(N1-NMIN)53,53,54
   53  W2=WB(N)
   54  CONTINUE
       D   =  EN2(N1)-EN2(N)
       Y   =  ATE*D
       YS  =  ATS*D
       D   =  1.0/D
       EN1 =  N1
       X2  =  EN2(N)
       N3  =  N-N1
       X   =  EN23(N1)*EN23(N3)/EN23(N-1)
       G   =  GBB(EN,EN1,X2,X)
       A1  =  EN3(N)*EN3(N1)*D*G
C
       FACTOR = 1.0D0
       IF(IPRS.LE.0) THEN
         CALL PYVR(Y,Z,PY)
       ELSE
         EI  = EN2(N1)
         WI  = 2.0D0*EN1*EN1
         EJ  = EN2(N)
         WJ  = 2.0D0*EN*EN
         F   = 1.96028D0*G*D*D*D*EN3(N1)*EN2(N1)*EN3(N)
         PHI = F*D/(Z*Z)
         CALL PYPR(EI,EJ,N1,N,1.0D0,Z,PHI,WI,WJ,TE,INTD,PY,RDEXC)
       ENDIF
C
       PY    = PY*FACTOR*RHO
       NTEST = N - N1
       IF(NTEST .LE. NIP) THEN
          PY=PYMAT(N1,NTEST)
       ENDIF
       A3 = 3.06998 *D*D*D *PY
C
       IF(YS-0.001)225,225,226
  225  A2=W2/(YS+0.5*YS*YS)
       GO TO 231
  226  IF(DEXPTS(N1))227,227,230
  227  IF(YS-CUT)229,228,228
  228  A2=0.0
       GO TO 231
  229  A2=W2/(DEXP(YS)-1.0)
       GO TO 231
  230  A2=W2/(DEXPTS(N1)/DEXPTS(N)-1.0)
  231  CONTINUE
C
       IF(N.LT.NLWS)THEN
           EXPON=ATE*(EN2(N1)-EN2(N))
           IF(EXPON.GT.CUT)THEN
               A(N1)=-A1*A2
           ELSE
               A(N1)=-A1*(A2+A3*DEXP(-EXPON))
           ENDIF
           A(N)=A(N)+A1*(1.0+A2+A3)
       ELSEIF(N.GE.NLWS.AND.N1.LT.NLWS)THEN
           EXPON=ATE*(EN2(N1)-EN2(N))
           IF(EXPON.GT.CUT)THEN
               A(N1)=-A1*A2
           ELSE
               A(N1)=-A1*(A2+A3*DEXP(-EXPON))
           ENDIF
           A(N)=A(N)+A1*(1.0+A2+A3)*DEXPTE(N)
           RHS(I)=RHS(I)-A1*(1.0+A2+A3)*DEXPTE(N)
       ELSEIF(N.GE.NLWS.AND.N1.GE.NLWS)THEN
           A(N1)=-A1*(A2*DEXPTE(N1)+A3*DEXPTE(N))
           A(N)=A(N)+A1*(1.0+A2+A3)*DEXPTE(N)
           RHS(I)=RHS(I)-A1*(DEXPTE(N)  +A2*(DEXPTE(N)-DEXPTE(N1)))
       ENDIF
C------PREPARE ARRAYS FOR CALL TO <V2CLDBN> WJD 9/6/92-----------
       IF(ICALC .EQ. 1) THEN
         AC(N1)=-A1*(A2*DEXPTE(N1)+A3*DEXPTE(N))
         AC(N)=AC(N)+A1*(1.0+A2+A3)*DEXPTE(N)
         RHSC(I)=RHSC(I)-A1*(DEXPTE(N)  +A2*(DEXPTE(N)-DEXPTE(N1)))
       ELSE
         ACI(N1)=-A1*(A2*DEXPTE(N1)+A3*DEXPTE(N))
         ACI(N)=ACI(N)+A1*(1.0+A2+A3)*DEXPTE(N)
         RHSI(I)=RHSI(I)-A1*(DEXPTE(N)  +A2*(DEXPTE(N)-DEXPTE(N1)))
       ENDIF
C-------------------------------------
C  EVALUATE LINE POWER COEFFICIENT
C  AND ELECTRON COOLING COEFFICIENT
C-------------------------------------
       RA       = EN3(N)*EN3(N1)*G
       PREP(I)  = PREP(I) + RA
       DELE = 1.0 * DABS( EN2(N1) - EN2(N) )
C
C      IF( LECOOL) THEN
C         DELE = 1.0 * DABS( EN2(N1) - EN2(N) )
C         QCT = 0.0D+0
C         IF(N.LT.NLWS)THEN
C            QCT = DELE * A1 * A3
C         ELSE
C            QCT = DELE * A1 * A3 *DEXPTE(N)
C         ENDIF
C         QCREP(I) = QCREP(I) + QCT
C      ENDIF
C
       IF( LBIN ) THEN
          WEBIN = EN2(NMIN) / NBIN
          IBIN = MIN0( NBIN, IDINT( DELE/WEBIN ) +1 )
          PREPBIN(I,IBIN) = PREPBIN(I,IBIN) + RA
C         WRITE(IUNT64,5010) I, N, N1, DELE, WEBIN, IBIN
       ENDIF
 5010  FORMAT(3I5,1P,2D12.4,0P,I5)
C
C
   25  CONTINUE
C
C
       IF(N.GE.NLWS)THEN
           PREP(I)  = CPL*DEXPTE(N) *  PREP(I)
           IF( LBIN ) THEN
             DO IBIN = 1,NBIN
               PREPBIN(I,IBIN)  = CPL*DEXPTE(N) *  PREPBIN(I,IBIN)
             ENDDO
           ENDIF
       ELSE
           PREP(I)  = CPL *  PREP(I)
           IF( LBIN ) THEN
             DO IBIN = 1,NBIN
               PREPBIN(I,IBIN)  = CPL* PREPBIN(I,IBIN)
             ENDDO
           ENDIF
       ENDIF
C
   26  CONTINUE
C
       IF( LECOOL ) QCREP(I) = CPL * QCREP(I)
C-----------------------------------------------------------------------
C  SAMPLE OUTPUT FOR REPRESENTATIVE N-SHELLS
C-----------------------------------------------------------------------
C
       IF( LDRSEL ) THEN
         DRMC = DRM(IBD,JTE,IPRT,ISYS,IPRT)
         IF (LRESOL) DRMC  = DRMU(IBD,JTE)
       ELSE
         DRMC = 0.0D0
       ENDIF
       RRN = 1.03936D-13*Z*ATE**1.5D0*RRECPT(I)
       IF (OPEN08) WRITE(IUNT08,109) I, N, A(N), RHS(I), DRMC,
     &                         DRECPT(I), RRN , RHSC(I)
C
C-----------------------------------------------------------------------
C  TRANSFORM C-R ARRAY TO CONTRACTED SCHEME
C-----------------------------------------------------------------------
C
       DO 27 J=1,IMAX
         IF(ICALC .EQ. 1) THEN
           AREDC(I,J)=0.0
         ELSE
           AREDI(I,J)=0.0
         ENDIF
         ARED(I,J)=0.0
   27  CONTINUE
C
       DO 29 N=NMIN,NMAX
         J=INTER(N)
         ARED(I,J-1) = ARED(I,J-1) + AGRL(N,1) * A(N)
         ARED(I,J)   = ARED(I,J)   + AGRL(N,2) * A(N)
         IF(ICALC .EQ. 1) THEN
           AREDC(I,J-1) = AREDC(I,J-1) + AGRL(N,1) * AC(N)
           AREDC(I,J)   = AREDC(I,J)   + AGRL(N,2) * AC(N)
         ELSE
           AREDI(I,J-1) = AREDI(I,J-1) + AGRL(N,1) * ACI(N)
           AREDI(I,J)   = AREDI(I,J)   + AGRL(N,2) * ACI(N)
         ENDIF
         IF(J-IMAX)28,29,29
   28    ARED(I,J+1) = ARED(I,J+1) + AGRL(N,3) * A(N)
         IF(ICALC .EQ. 1) THEN
           AREDC(I,J+1) = AREDC(I,J+1) + AGRL(N,3) * AC(N)
         ELSE
           AREDI(I,J+1) = AREDI(I,J+1) + AGRL(N,3) * ACI(N)
         ENDIF
   29  CONTINUE

C-----------------------------------------------------------------------
C  TRANSFORM RADIATED POWER VECTORS TO EXPANDED SCHEME
C-----------------------------------------------------------------------
C
       IF(I.GT.2) RLREP(I-2) = RLREP(I-2) + PREP(I)*DGS2(I-2)
       IF(I.GT.1) RLREP(I-1) = RLREP(I-1) + PREP(I)*DGS1(I-1)
       RLREP(I) = RLREP(I) + PREP(I)*DG(I)
       IF(I.LT.IMAX-1) RLREP(I+2) = RLREP(I+2) + PREP(I)*DGS2(I)
       IF(I.LT.IMAX)   RLREP(I+1) = RLREP(I+1) + PREP(I)*DGS1(I)
       IF(NREP(I).GE.NLWS)THEN
           RL = RL + PREP(I)*VEC(I)
       ENDIF
       RD  = RD  + RDREP(I)  * VEC(I)
       RR  = RR  + RRREP(I)  * VEC(I)
       RL0 = RL0 + RL0REP(I) * VEC(I)
       AL0 = AL0 + AL0REP(I) * VEC(I)
C
       IF (LECOOL ) THEN
          IF(I.GT.2) QCREPC(I-2) = QCREPC(I-2) + QCREP(I)*DGS2(I-2)
          IF(I.GT.1) QCREPC(I-1) = QCREPC(I-1) + QCREP(I)*DGS1(I-1)
          QCREPC(I) = QCREPC(I) + QCREP(I)*DG(I)
          IF(I.LT.IMAX-1) QCREPC(I+2) = QCREPC(I+2) + QCREP(I)*DGS2(I)
          IF(I.LT.IMAX)   QCREPC(I+1) = QCREPC(I+1) + QCREP(I)*DGS1(I)
          IF(NREP(I).GE.NLWS)THEN
              QCR = QCR + QCREP(I)*VEC(I)
          ENDIF
          QRD  = QRD  + QRDREP(I) * VEC(I)
          QRR  = QRR  + QRRREP(I) * VEC(I)
          QRT  = QRT  + QRTREP(I) * VEC(I)
       ENDIF
C
       IF( LBIN ) THEN
        DO IBIN = 1,NBIN
          IF(I.GT.2) RLREPBN(I-2,IBIN) =
     &                RLREPBN(I-2,IBIN) + PREPBIN(I,IBIN)*DGS2(I-2)
          IF(I.GT.1) RLREPBN(I-1,IBIN) =
     &                RLREPBN(I-1,IBIN) + PREPBIN(I,IBIN)*DGS1(I-1)
          RLREPBN(I,IBIN) = RLREPBN(I,IBIN) + PREPBIN(I,IBIN)*DG(I)
          IF(I.LT.IMAX-1) RLREPBN(I+2,IBIN) =
     &                RLREPBN(I+2,IBIN)+PREPBIN(I,IBIN)*DGS2(I)
          IF(I.LT.IMAX)   RLREPBN(I+1,IBIN) =
     &                RLREPBN(I+1,IBIN)+PREPBIN(I,IBIN)*DGS1(I)
          IF(NREP(I).GE.NLWS)THEN
             RLBIN(IBIN)  = RLBIN(IBIN) + PREPBIN(I,IBIN)*VEC(I)
          ENDIF
        ENDDO
       ENDIF
C
C-----------------------------------------------------------------------
C  END OF MAIN LOOP IN N-SHELLS
C-----------------------------------------------------------------------
   30  CONTINUE
C-----------------------------------------------------------------------
C  CALCULATE DR SATELITE POWER IF BADNELL DATA IS BEING USED
C-----------------------------------------------------------------------
C      IF( LDRSEL ) THEN
C         RD = 0
C         DO IBFIL = 1,NBFIL
C           RD = RD + PWSAT(IBFIL,JTE,IPRT,ISYS,IPRT)
C         ENDDO
C         RD = RD * DENS/SSYSWT
C      ENDIF
C
C-----------------------------------------------------------------------
       IF( IPRT .GT. 1 .AND. ICALC .EQ. 2) THEN
C-----------------------------------------------------------------------
C  NOW ASSEMBLE COMPLETE ALOSS ARRAY AND TRANSFORM TO CONTRACTED SCHEME
C        ALOSS  -  MATRIX FOR ENTIRE SET OF N
C        ALOSSR -  MATRIX FOR REPRESENTATIVE SET
C        ALOSSC -  ALOSS CONTRACTED OVER REPRESENTATIVE LEVELS
C-----------------------------------------------------------------------
C
C      DO JPRT = 1,IPRT
C         ALOSS(JPRT,NMIN) = ALOSSR(JPRT,1)
C      ENDDO
C      DO 240 N = NMIN+1,NMAX
C         ALOSS(JPRT,N) = 0.0D+0
C         EN = N
C--------------------------------
C  PREPARE IONISATION RATE
C--------------------------------
C         YE=ATE*(EN2(N)-ENMAX2)
C         YS=ATS*(EN2(N)-ENMAX2)
C         ENC=1.0/DSQRT(EN2(N)-ENMAX2)
C         CALL COLINT(YE,Z,ENC,AI)
C         AI=AI/CION
C         AION = 0.43171*RHO*AI*EN*EN*ENC*ENC
C-------------------------------------
C  PREPARE AUTO-IONISATION RATE
C-------------------------------------
C         DO JPRT = 1, IPRT
C            CALL V2AUG(IPRT,ISYS,N,JPRT,AAGPAR)
C            IF(JPRT .EQ. (IPRT-1) .AND. N .GT. ICTPR) THEN
C               AN0 = 1.42D+16
C               AAGPAR = AN0 / ( EN ** 3 )
C            ELSE
C               AAGPAR = 0.0D+0
C            ENDIF
C            ALOSS(JPRT,N) = AAGPAR
C         ENDDO
C------------------------------------------
C  ADD ON CONTRIBUTIONS FROM IONISATION
C  AND AUTO-IONISATION TO LOSS MATRIX
C------------------------------------------
C          DO JPRT = 1, IPRT
C              ALOSS(JPRT,N) = EN * EN * ALOSS(JPRT,N) /
C    &                                       ( Z**4 * 1.57456D10)
C              IF(N.GE.NLWS)THEN
C                 ALOSS(JPRT,N) = DEXPTE(N)*ALOSS(JPRT,N)
C              ENDIF
C
C              IF(JPRT .EQ. IPRT)THEN
C                  PRTWGT(JPRT) = 1.0D+0
C              ELSE
C                  PRTWGT(JPRT) = 0.0D+0
C              ENDIF
C              AIONPRT = AION * PRTWGT(JPRT)
C              IF(N.GE.NLWS)THEN
C                 ALOSS(JPRT,N) = ALOSS(JPRT,N) + AIONPRT
C              ELSE
C                IF(ATE*EN2(N).LE.CUT)THEN
C                    ALOSS(JPRT,N) = ALOSS(JPRT,N) + AIONPRT/DEXPTE(N)
C                ELSE
C                    ALOSS(JPRT,N) = ALOSS(JPRT,N) + 0.0D+0
C                ENDIF
C              ENDIF
C          ENDDO
C 240  CONTINUE
C------------------------------------------------
C  TRANSFORM ALOSS ARRAY TO CONTRACTED SCHEME
C------------------------------------------------
C
C      DO 244 JPRT = 1,IPRT
C      DO 241 J=1,IMAX
C        ALOSSC(JPRT,J)=0.0
C 241  CONTINUE
C      DO 242 N=NMIN,NMAX
C        J=INTER(N)
C        WRITE(IUNT08,1169) N, J, (AGRL(N,I), I=1,3)
C        ALOSSC(JPRT,J-1) = ALOSSC(JPRT,J-1) + AGRL(N,1) *ALOSS(JPRT,N)
C        ALOSSC(JPRT,J)   = ALOSSC(JPRT,J)   + AGRL(N,2) *ALOSS(JPRT,N)
C        IF(J .LT. IMAX) THEN
C         ALOSSC(JPRT,J+1) = ALOSSC(JPRT,J+1) + AGRL(N,3) *ALOSS(JPRT,N)
C        ENDIF
C 242  CONTINUE
C 244  CONTINUE
       DO I = 1,IMAX
         ALOSST(I) = 0.0D+0
         DO JPRT = 1,IPRT
             ALOSST(I) = ALOSST(I) + ALOSSR(JPRT,I)
         ENDDO
         DO JPRT = 1,IPRT
             COUT(JPRT,I) = ARED(1,I) - ALOSST(I) + ALOSSR(JPRT,I)
         ENDDO
       ENDDO
C
C      WRITE(IUNT08,1168) (JPRT, JPRT = 1,IPRT) , (JPRT, JPRT = 1,IPRT),
C    &               (JPRT, JPRT = 1,IPRT)
C      DO I = 1,IMAX
C        WRITE(IUNT08,1169) I, NREP(I), (ALOSSR(JPRT,I), JPRT = 1, IPRT),
C      ENDDO
C-----------------------------------------------------------------------
C  SUM RHS OVER ALL N-SHELLS
C-----------------------------------------------------------------------
       SUM3 = 0.0D+0
C      IF( NREP(1) .GE. NLWS ) THEN
C         RHSREC(1) =  RHSD
C      ELSE
C         RHSREC(I) =  RHSD + RHDIR
C      ENDIF
C      RHSREC(1) = RHS(1)
       DO I = 1,IMAX
         NL = NREP(I)
C------- ?  NL IS NOT USED ? ---------
         SUM3 = SUM3 + RHSREC(I)
       ENDDO
C----------------------------------------
C  METASTABLE RESOLVED C-R COEFFICIENTS
C----------------------------------------
       DO JPRT = 1, NPRDIM
          SPRT(JPRT) = 0.0D+0
          APRT(JPRT) = 0.0D+0
          QPRT(JPRT) = 0.0D+0
       ENDDO
       DO 36 I=2,IMAX
         RHSJK(I-1)=RHS(I)
         DO 37 J=2,IMAX
   37    AREDJK(I-1,J-1)=ARED(I,J)
   36  CONTINUE
C
       CALL B4MATV(AREDJK,IMAX-1,RHSJK,2,DETERM)
C
       STOT = 0.0D+0
       ATOT = 0.0D+0
       AREC = 0.0D+0
       DO JPRT = 1,IPRT
         SUMPRT(JPRT) = 0.0D+00
       ENDDO
C      WRITE(IUNT08,1176)
       DO 38 J=2,IMAX
         SUM1   = 0.0D+0
         SUM2   = 0.0D+0
         DO JPRT = 1,IPRT
           SUMPR(JPRT)  = 0.0D+0
         ENDDO
         SUMION = 0.0D+0
         DO 39 K=2,IMAX
           SUM1 = SUM1 + AREDJK(J-1,K-1) * ARED(K,1)
           SUM2 = SUM2 + AREDJK(J-1,K-1) * RHSREC(K)
           DO JPRT = 1,IPRT
             SUMPR(JPRT) = SUMPR(JPRT) + COUT(JPRT,K) * AREDJK(K-1,J-1)
           ENDDO
           SUMION = SUMION + ALOSSR(2,K) * AREDJK(K-1,J-1)
   39    CONTINUE
         DO JPRT = 1, IPRT
            SUMPRT(JPRT) = SUMPRT(JPRT) + ARED(J,1) * SUMPR(JPRT)
            SPRT(JPRT) = SPRT(JPRT) + ALOSSR(JPRT,J)*SUM1
            IF(JPRT .EQ. IPRT) THEN
              QPRT(JPRT) = 0.0D+0
              ATOT       = ATOT       + ALOSSR(JPRT,J)*SUM2
            ELSE
              QPRT(JPRT) = QPRT(JPRT) + ALOSSR(JPRT,J)*SUM2
            ENDIF
         ENDDO
         STOT = STOT + ARED(1,J) * SUM1
         AREC = AREC + ARED(1,J) * SUM2
   38  CONTINUE
C
       ADIREX = ADIR - ALOSST(1)
	IF (OPEN08) THEN
           DO JPRT = 1, IPRT
             WRITE(IUNT08,1162) JPRT, QPRT(JPRT)
           ENDDO
           WRITE(IUNT08,1163) SUM3 , ATOT
           WRITE(IUNT08,1164) ADIR , STOT
           WRITE(IUNT08,1165) RHS(1), AREC
           WRITE(IUNT08,1166)
	ENDIF
C
       DO JPRT = 1, IPRT
          SPRT(JPRT) = ALOSSR(JPRT,1) - SPRT(JPRT)
C         SPRT(JPRT) = ALOSSR(JPRT,1) + ADIREX - SUMPRT(JPRT)
       ENDDO
       ATOT   =  SUM3   - ATOT
       STOT   =  ADIR   - STOT
       APRT(IGRD)  =  RHSREC(1) - AREC
C
       C1REC = 1.03928D-13 * (Z**4) * (157890.0/TE)**1.5 * SSYSWT
       C2ION = 1.5744D10*Z**4 / (DENS*NREP(1)*NREP(1))
       DO JPRT = 1, IPRT
         IF(NREP(1).GE.NLWS)THEN
             SPRT(JPRT) = C2ION * SPRT(JPRT) / DEXPTE(NREP(1))
             IF(JPRT.EQ.1) STOT = C2ION * STOT /DEXPTE(NREP(1))
         ELSE
             X=ATE*EN2(NREP(1))
             IF(X.LT.CUT)THEN
                 SPRT(JPRT) = C2ION * SPRT(JPRT)
                 IF(JPRT.EQ.1) STOT = C2ION * STOT
             ELSE
                 SPRT(JPRT) = 0.0D+0
                 IF(JPRT.EQ.1) STOT = 0.0D+0
             ENDIF
         ENDIF
         IF(NREP(1).GE.NLWS)THEN
             QPRT(JPRT) = C1REC * DEXPTE(NREP(1)) * APRT(JPRT)
             IF(JPRT.EQ.1) ATOT = C1REC * DEXPTE(NREP(1)) * ATOT
             IF(JPRT.EQ.1) APRT(IGRD)= C1REC*DEXPTE(NREP(1))* APRT(IGRD)
         ELSE
             QPRT(JPRT) = C1REC * QPRT(JPRT)
             IF(JPRT.EQ.1) ATOT = C1REC *  ATOT
             IF(JPRT.EQ.1) APRT(IGRD)= C1REC * APRT(IGRD)
         ENDIF
       ENDDO
C
       STOTR(JTE,JDENS)  = STOT
       ATOTR(JTE,JDENS)  = ATOT
       ARES(IPRT,IGRD,JTE,JDENS)   =   APRT(IGRD)
       DO JPRT = 1, IPRT
          IF( SPRT(JPRT) .LT. 1.00D-60 .OR. SPRT(JPRT) .GE. 1.00) THEN
             SPRT(JPRT) = 1.00D-60
          ENDIF
          SRESP(IGRD,JPRT,IPRT,JTE,JDENS) =  SPRT(JPRT)
          QRESP(IPRT,JPRT,IGRD,JTE,JDENS) =  QPRT(JPRT)
       ENDDO
C
C--------------------------------
C
       ELSE IF(ICALC .EQ. 1) THEN
C
C--------------------------------
C
C-----------------------------------------------------------------------
C      ORIGINAL INVERSIONS AND C-R MANIPUALATIONS
C-----------------------------------------------------------------------
C
       DO 34 I=1,IMAX
         RHSN(I)=RHS(I)
         DO 34 J=1,IMAX
           AREDN(I,J)=ARED(I,J)
   34  CONTINUE
C
C      WRITE(IUNT08,1000)
C      WRITE(IUNT08,1001) ADIR, (ARED(1,J) , J = 2,IMAX) , RHSD+RHSDIR
C      DO I = 2,IMAX
C        WRITE(IUNT08,1001) (ARED(I,J) , J = 1,IMAX) , RHS(I)
C      ENDDO
C
       IF(NREP(1).GE.NLWS)THEN
           RHSN(1)=RHSD
           AREDN(1,1)=ADIR
           CALL B4MATV (AREDN,IMAX,RHSN,2,DETERM)
           SD = 1.5744D10*Z**4/(DENS*DEXPTE(NREP(1))*NREP(1)*NREP(1)*
     &          AREDN(1,1))
       ELSE
           X=ATE*EN2(NREP(1))
           IF(X.LT.CUT)THEN
               RHSN(1) = RHSD+RHDIR
               AREDN(1,1) = ADIR
               CALL B4MATV(AREDN,IMAX,RHSN,2,DETERM)
               SD = 1.5744D10*Z**4/(DENS*NREP(1)*NREP(1)*
     &                AREDN(1,1))
           ELSE
               SD=0.0D0
           ENDIF
       ENDIF
C
       CALL B4MATV(ARED,IMAX,RHS,2,DETERM)
C
       IF(NREP(1).GE.NLWS)THEN
           SDIR=1.5744D10*Z**4/(DENS*DEXPTE(NREP(1))*NREP(1)*NREP(1)*
     &     ARED(1,1))
       ELSE
           SDIR=1.5744D10*Z**4/(DENS*NREP(1)*NREP(1)*ARED(1,1))
       ENDIF
C      IF (OPEN17) WRITE(IUNT17,1995)SDIR,SD,ADIR,RHSD
       C = 6.60074D-24*DENS*SSYSWT*(157890.0/TE)**1.5
C
       DO 31 I=1,IMAX
       RL = RL+ RLREP(I)*RHS(I)
       IF( LECOOL ) QCR = QCR + QCREPC(I) * RHS(I)
C
       IF( LBIN) THEN
         DO IBIN = 1,NBIN
            RLBIN(IBIN) = RLBIN(IBIN) + RLREPBN(I,IBIN) * RHS(I)
         ENDDO
       ENDIF
C
       N=NREP(I)
       IF(I-1)601,601,600
  600  IDCN=INTER(N-1)
       CN1=RHS(IDCN-1)
       CN2=RHS(IDCN)
       IF(IMAX-IDCN)603,603,602
  602  CN3=RHS(IDCN+1)
       GO TO 604
  603  CN3=0.0
  604  CONTINUE
       IF(I-IDCN)605,606,605
  605  DELCN(I)=-CN1*AGRL(N-1,1)-CN2*AGRL(N-1,2)-CN3*(AGRL(N-1,3)-1.0)
       GO TO 601
  606  DELCN(I)=(CN2-CN1)*AGRL(N-1,1)+(CN2-CN3)*AGRL(N-1,3)
  601  CONTINUE
C
       DELCN(1)=0.0
       IF(NREP(I).GE.NLWS)THEN
           BREP(I)=1.0+RHS(I)
           DEXPBN(I)=DEXPTE(N)*BREP(I)
           EN=N
           POP(I)=C*EN*EN*DEXPBN(I)
       ELSE
           DEXPBN(I)=RHS(I)
           X=ATE*EN2(NREP(I))
           IF(X.GE.CUT)THEN
               BREP(I)=0.0D0
               RHS(I)=-1.0D0
           ELSE
               BREP(I)=DEXPBN(I)/DEXPTE(N)
               RHS(I)=BREP(I)-1.0D0
           ENDIF
           EN=N
           POP(I)=C*EN*EN*DEXPBN(I)
       ENDIF
       DVEC(I) = EN*EN*DEXPTE(N)
C
       NREP1(I) = NREP(I)
       RHS1(I)  = RHS(I)
   31  CONTINUE
       IMAX1 = IMAX
C-------------------------------------------
C  EVALUATE BREMSSTRAHLUNG POWER COEFFICIENT
C-------------------------------------------
       RB = CPL*0.25*NGFFMH(ATE)/(ATE*ATE)
C
C------------------------------------------
C  FINALISE COLLISIONAL-RADIATIVE VARIABLES
C------------------------------------------
       C = 1.03928D-13*Z*SSYSWT*ATE*DSQRT(ATE)
       IF(NREP(1).GE.NLWS)THEN
           ALFA=C*BREP(1)/ARED(1,1)
       ELSE
           ALFA=C*DEXPBN(1)/ARED(1,1)
       ENDIF
       S     = ALFA/POP(1)
       AL0C1 = SSYSWT*AL0
       RL0C1 = SSYSWT*RL0
       RLC1  = SSYSWT*RL
       RRC1  = SSYSWT*RR
       QRRC1 = SSYSWT*QRR
       QRTC1 = SSYSWT*QRT
       QCRC1 = SSYSWT*QCR
       RDC1  = SSYSWT*RD
       RBC1  = SSYSWT*RB
       PW0(JTE)  = RL0+(RR+RD+RB)/DENS
       ALF0(JTE) = AL0
       S0(JTE)   = SGRD
       ALFAA(JTE,JDENS) = ALFA
       IF( SD .LT. 1.00D-60 .OR. SD .GT. 1.00) SD = 1.00D-60
       SAA(JTE,JDENS)   = SD
       PAA(JTE,JDENS)   = SSYSWT * (RL+RR+RD+RB)/DENS
       IF (LECOOL) QELR(JTE,JDENS) = SSYSWT * (QCR + QRT) /DENS
C
       IF( LBIN) THEN
         DO IBIN = 1,NBIN
            PCASBIN(JTE,JDENS,IBIN) = SSYSWT * RLBIN(IBIN) /DENS
         ENDDO
       ENDIF
C
       IF(IPRT .LE. 1) THEN
          SRESP(IGRD,IPRT,IPRT,JTE,JDENS) =  SD
          STOTR(JTE,JDENS)                =  SD
       ENDIF
       ARES(IPRT,IGRD,JTE,JDENS) =  ALFA
C
       IF (OPEN09) THEN
         WRITE(IUNT09,4300)TE, DENS
         WRITE(IUNT09,4304)
       ENDIF
         QRTC1 = 0.0D+0
         DO I = 1,IMAX1
           CN = RHS(I)
           IF( I .EQ. 1) CN = -1.0D+00
             IF (OPEN09) WRITE(IUNT09,157) NREP1(I),BREP(I), QCREP(I),
     &     	 	   QRRREP(I), QRTREP(I) , CN, VEC(I),
     &                     CN * VEC(I) * QRTREP(I)
           QRTC1 = QRTC1 + QRTREP(I) * CN * VEC(I)
         ENDDO
         IF (LECOOL) QELR(JTE,JDENS) = SSYSWT * QRTC1  /DENS
       IF (OPEN09) THEN
            WRITE(IUNT09,4306)
            WRITE(IUNT09,1995) QCR , QRRC1 , QRTC1
       ENDIF
      if (icalc .le. 1 .and. open30)  then
       write(iunt30,'(8H    IMAX, i3)')imax1
       write(iunt30,129)
       write(iunt30,112) (nrep1(i),rhs1(i),brep(i),dexpbn(i),
     &                                      pop(i),delcn(i),i=1,imax1)
      endif
C
C----------------------
C
       ENDIF
C
C
 4000  CONTINUE
C
C--------------------------------------------------------------------
C    CALL V2CLDBN TO ESTABLISH PROJECTION MATRICES
C--------------------------------------------------------------------
        IF( LPROJ ) THEN
          Z1=Z
          ACNST=1.03928D-13*Z*ATE*DSQRT(ATE)
          A1CNST=6.60074D-24*(157890.0D0/TE)**1.5D0
          CALL B4PROJ(W1, JTE, JDENS,NMIN, NMAXC1, NREPC, IMAXC1,
     &                 NRESU , AREDC, RHSC, CIONPT,
     &                 TRECPT, DRECPT, RRECPT, XRECPT,
     &                 NPRSTK, NMAXC2, NREPI, IMAXC2, AREDI, RHSI,
     &                 CIONRI, CIONRA, RHSIRC,
     &                 IEDMAT, IECION, IETREC, IEDREC, IERREC, IEXREC,
     &                 IERSYS, SSYSWT, IPRT,
     &                 DVEC, ACNST, A1CNST ,
     &                 OPEN18, OPEN19, OPEN20,
     &                 PAA(JTE,JDENS)
     &               )
        ENDIF
C
C---------------------------------------------------------------------
C  WRITE OUT DATA TO MAIN OUTPUT
C---------------------------------------------------------------------
C
       IF (OPEN17) WRITE(IUNT17,3073) TE  , DENS , ALFA , AL0C1 , SD  ,
     &                  SGRD, RLC1, RRC1 , RDC1 , RBC1  , RL0C1
C
C---------------------------------------------------------------------
C  WRITE OUT DATA TO EXTENDED OUTPUT
C---------------------------------------------------------------------
C
      IF (OPEN08)  THEN
       WRITE(IUNT08,129)
       WRITE(IUNT08,112) (NREP1(I),RHS1(I),BREP(I),DEXPBN(I),
     &                                      POP(I),DELCN(I),I=1,IMAX1)
       WRITE(IUNT08,130)
       WRITE(IUNT08,116) ALFA, S, AL0C1, AREDN(1,1) , ARED(1,1)
       WRITE(IUNT08,1996)
       WRITE(IUNT08,1995) RL, RR, RD, RB, RL0
       WRITE(IUNT08,1170)
       WRITE(IUNT08,1171)
       WRITE(IUNT08,1172) ALFA, APRT(IGRD),
     &                            (QPRT(JPRT), JPRT=1,IPRT-1),ATOT
       WRITE(IUNT08,1173) SD , STOT     ,
     &                            (SPRT(JPRT), JPRT=1,IPRT )
      ENDIF

C-----------------------------------------------------------------------
C TELL IDL THAT WE HAVE FINISHED ONE PASS OF THE LOOPS
C-----------------------------------------------------------------------
       IF(.NOT.LBATCH)THEN
          WRITE(PIPEOU,*)IONE
          CALL XXFLSH(PIPEOU)
       ENDIF
C-----------------------------------------------------------------------

  400  CONTINUE
  401  CONTINUE
C
C-----------------------------------------------------------------------
C     END OF MAIN TEMPERATURE AND DENSITY LOOPS
C-----------------------------------------------------------------------
C
C----------------------------------------
C  WRITE OUT DATA TO STREAM IUNT17
C
C  (A) WRITE OUT UNRESOLVED COEFFICIENTS
C----------------------------------------
        IF (OPEN17) THEN
           WRITE(IUNT17,3056)
           WRITE(IUNT17,3057)(DENSA(JDENS),JDENS=1,JDENSM)
           DO 403 JTE=1,JTEM
  403      WRITE(IUNT17,3058)TEA(JTE),(ALFAA(JTE,JDENS),JDENS=1,JDENSM)
           WRITE(IUNT17,3059)(DENSA(JDENS),JDENS=1,JDENSM)
           DO 404 JTE=1,JTEM
           IF(SEX(JTE).GT.0.0D0.AND.S0(JTE).GT.0.0D0)THEN
              WRITE(IUNT17,3060)TEA(JTE),(SAA(JTE,JDENS),JDENS=1,JDENSM)
           ENDIF
  404      CONTINUE
C
           WRITE(IUNT17,3061)(DENSA(JDENS),JDENS=1,JDENSM)
           DO 405 JTE=1,JTEM
  405      WRITE(IUNT17,3062)TEA(JTE),(PAA(JTE,JDENS),JDENS=1,JDENSM)
	ENDIF
C
       IF( LECOOL.AND.OPEN09) THEN
         WRITE(IUNT09,4310)(DENSA(JDENS),JDENS=1,JDENSM)
         DO JTE=1,JTEM
           WRITE(IUNT09,3062)TEA(JTE),(QELR(JTE,JDENS),JDENS=1,JDENSM)
         ENDDO
       ENDIF
C
       IF (OPEN17) WRITE(IUNT17,3063)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
       ITT=0.5*(JTEM+1.001)
       IZ=Z
       DO 406 JTE=1,JTEM
	 IF (OPEN17) THEN
        IF(JTE.NE.ITT) THEN
         WRITE(IUNT17,3064)TEA(JTE)/Z**2,ALFEX(JTE),
     &                (ALFAA(JTE,JDENS)/ALF0(JTE),JDENS=1,JDENSM)
        ELSE
         WRITE(IUNT17,3065)IZ,TEA(JTE)/Z**2,ALFEX(JTE),
     &                 (ALFAA(JTE,JDENS)/ ALF0(JTE),JDENS=1,JDENSM)
        ENDIF
       ENDIF
  406  CONTINUE
       IF (OPEN17) WRITE(IUNT17,3066)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
       ITT=0.5*(JTEM+1.001)
       DO 407 JTE=1,JTEM
       IF(SEX(JTE).GT.1.0D-60.AND.S0(JTE).GT.1.0D-60)THEN
	    IF (OPEN17) THEN
           IF(JTE.NE.ITT)THEN
             WRITE(IUNT17,3067) TEA(JTE)/Z**2,SEX(JTE),
     &                         (SAA(JTE,JDENS)/S0( JTE),JDENS=1,JDENSM)
           ELSE
             WRITE(IUNT17,3068) IZ,TEA(JTE)/Z**2,SEX(JTE),
     &                        (SAA(JTE,JDENS)/ S0(JTE),JDENS=1,JDENSM)
           ENDIF
	    ENDIF
       ELSE
           JSTEM=JTE
       ENDIF
  407  CONTINUE
       IF (OPEN17) WRITE(IUNT17,3069)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
       ITT=0.5*(JTEM+1.001)
       IF (OPEN17) THEN
        DO 408 JTE=1,JTEM
        IF(JTE.NE.ITT)THEN
         WRITE(IUNT17,3070) TEA(JTE)/Z**2,PW0(JTE),
     &                  (PAA(JTE,JDENS)/PW0( JTE),JDENS=1,JDENSM)
        ELSE
         WRITE(IUNT17,3071) IZ,TEA(JTE)/Z**2,PW0(JTE),
     &                   (PAA(JTE,JDENS)/ PW0(JTE),JDENS=1,JDENSM)
        ENDIF
  408   CONTINUE
       ENDIF
C      WRITE(IUNT17,3074)
C
C-------------------------------------------------
C  (B) WRITE OUT METASTABLE RESOLVED COEFFICIENTS
C------------------------------------------------
       IF (OPEN17) THEN
       WRITE(IUNT17,4100)
C
       DO 410 JPRT = 1,NPRSTK
        WRITE(IUNT17,4102) IGRD, JPRT, IPRT,
     &                     (DENSA(JDENS),JDENS=1,JDENSM)
        DO 412 JTE=1,JTEM
           WRITE(IUNT17,4101) TEA(JTE),
     &            ( SRESP(IGRD,JPRT,IPRT,JTE,JDENS) , JDENS = 1,JDENSM)
  412   CONTINUE
  410  CONTINUE
C
        IF(IPRT .GT. 1) THEN
        WRITE(IUNT17,4104) IGRD, IPRT, (DENSA(JDENS),JDENS=1,JDENSM)
        DO 414 JTE=1,JTEM
           WRITE(IUNT17,4101) TEA(JTE),
     &            ( STOTR(JTE,JDENS) , JDENS = 1,JDENSM)
  414   CONTINUE
        ENDIF
C
        WRITE(IUNT17,4106) IPRT, IGRD, (DENSA(JDENS),JDENS=1,JDENSM)
        DO 416 JTE=1,JTEM
           WRITE(IUNT17,4101) TEA(JTE),
     &            ( ARES(IPRT,IGRD,JTE,JDENS) , JDENS = 1,JDENSM)
  416   CONTINUE
C
       DO 420 JPRT = 1,IPRT-1
        WRITE(IUNT17,4108) IPRT, JPRT, IGRD,
     &                     (DENSA(JDENS),JDENS=1,JDENSM)
        DO 422 JTE=1,JTEM
           WRITE(IUNT17,4101) TEA(JTE),
     &            ( QRESP(IPRT,JPRT,IGRD,JTE,JDENS) , JDENS = 1,JDENSM)
  422   CONTINUE
  420  CONTINUE
C
        IF(IPRT .GT. 1) THEN
        WRITE(IUNT17,4110) IPRT, IGRD, (DENSA(JDENS),JDENS=1,JDENSM)
        DO 430 JTE=1,JTEM
           WRITE(IUNT17,4101) TEA(JTE),
     &           ( ATOTR(JTE,JDENS) , JDENS = 1,JDENSM)
  430   CONTINUE
        ENDIF
	ENDIF
C
C-------------------------------------------------
C  (C) WRITE OUT COMPARISON BETWEEN (A) AND (B)
C------------------------------------------------
	IF (OPEN17) THEN
       WRITE(IUNT17,4200)
C
       WRITE(IUNT17,4202) (DENSA(JDENS),JDENS=1,JDENSM)
       DO 440 JTE=1,JTEM
          WRITE(IUNT17,4201) TEA(JTE),
     &  (ARES(IPRT,IGRD,JTE,JDENS)/ALFAA(JTE,JDENS) , JDENS = 1,JDENSM)
  440  CONTINUE
C
       WRITE(IUNT17,4204) (DENSA(JDENS),JDENS=1,JDENSM)
	ENDIF
	
       DO 442 JTE=1,JTEM
          DO 441 JDENS = 1,JDENSM
            IF(SAA(JTE,JDENS) .EQ. STOTR(JTE,JDENS) ) THEN
               RATS(JDENS) = 1.00
            ELSEIF(SAA(JTE,JDENS) .LE. 0.0D0) THEN
               RATS(JDENS) = 0.00D0
            ELSE
               RATS(JDENS) = STOTR(JTE,JDENS) / SAA(JTE,JDENS)
            ENDIF
  441     CONTINUE
          IF (OPEN17) WRITE(IUNT17,4201) TEA(JTE),
     &		(RATS(JDENS) , JDENS = 1,JDENSM)
C    &            ( STOTR(JTE,JDENS)/SAA(JTE,JDENS) , JDENS = 1,JDENSM)
  442  CONTINUE
C
C------------------------------------------------------------------
C      WRITE DATA TO POWER FILE
C------------------------------------------------------------------
       call xxslen(dsnin,len1,len2)
       STRING = DASHES
       WRITE(SUBSTRG,2001) IPRT,IGRD,IZ,DATE(1:2),DATE(4:5),DATE(7:8)
       STRING(31:80)=SUBSTRG
       if (open62) then
          WRITE(IUNT62,2002)EQUALS
          WRITE(IUNT62,2006) IPRT, TRMSTK(IPRT),SPRSTK(IPRT),
     &                       NCTSTK(IPRT,IGRD),IGRD,SPNSTK(IGRD),
     &                       SSWSTK(IPRT,IGRD)
          WRITE(IUNT62,2002)DASHES
          WRITE(IUNT62,2003)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
          WRITE(IUNT62,2003)(TEA(JTE)/Z**2,JTE=1,JTEM)
          WRITE(IUNT62,2002)STRING
          DO 512 JTE=1,JTEM
           WRITE(IUNT62,2003)(PAA(JTE,JDENS),JDENS=1,JDENSM)
  512     CONTINUE
          write(iunt62,5005)iz,iz0in,dsnin(1:len2),realname,date(1:8)
       endif
C------------------------------------------------------------------
C      WRITE DATA TO POWER (BINNED) FILE
C------------------------------------------------------------------
C
       IF( LBIN .AND. open64 ) THEN
         STRING = DASHES
         WRITE(SUBSTRG,2001) IPRT,IGRD,IZ,DATE(1:2),DATE(4:5),DATE(7:8)
         STRING(31:80)=SUBSTRG
         WRITE(IUNT64,5002) NUCCHG, NBIN, Z*Z*EN2(NMIN)
         WRITE(IUNT64,2002)DASHES
         WRITE(IUNT64,2003)(DENSA(JDENS),JDENS=1,JDENSM)
         WRITE(IUNT64,2003)(TEA(JTE),JTE=1,JTEM)
         WRITE(IUNT64,2002)STRING
         DO 612 IBIN = 1,NBIN
           WRITE(IUNT64,5008) IBIN
           DO 612 JTE=1,JTEM
            WRITE(IUNT64,2003)(PCASBIN(JTE,JDENS,IBIN),JDENS=1,JDENSM)
  612    CONTINUE
       ENDIF

C
 5002 FORMAT('  NUCCHG = ',I2,'   NBIN = ',I2,'  I.POT.(RH) = ',D12.4)
 5008 FORMAT('/',I2,'/')
C-----------------------------------------------------
C
C
  513  CONTINUE
  514  CONTINUE
       IF( LPROJ .AND. open18)  WRITE(IUNT18,3106)

C-----------------------------------------------------------------------
C    END OF PARENT AND SPIN SYSTEM LOOPS
C
C    SUM RESOLVED COEFFICIENTS OVER INTERMEDIATE STATES
C-----------------------------------------------------------------------
C      WRITE(IUNT08,181) JTEM, JDENSM, NGRSTK, NPRSTK
  181  FORMAT('JTEM = ',I3,'  JDENSM = ',I3,' NGRSTK = ',I3,
     &                                      ' NPRSTK = ',I3)
       DO 540 JTE=1,JTEM
         DO 541 JDENS=1,JDENSM
C
           DO IGRD=1,NGRSTK
             DO IPRT=1,NPRSTK
               DO JPRT=1,NPRSTK
                 SRES(IGRD,IPRT,JTE,JDENS)  = SRES(IGRD,IPRT,JTE,JDENS)
     &                                + SRESP(IGRD,IPRT,JPRT,JTE,JDENS)
C                WRITE(IUNT08,180) JTE, JDENS, IGRD, IPRT, JPRT,
C    &                                 SRESP(IGRD,IPRT,JPRT,JTE,JDENS),
C    &                                  SRES(IGRD,IPRT,JTE,JDENS)
  180            FORMAT(5I5,1P,2D12.3)
               ENDDO
             ENDDO
           ENDDO
C
           DO IPRT=1,NPRSTK
             DO JPRT=1,NPRSTK
               DO IGRD=1,NGRSTK
                 QRES(IPRT,JPRT,JTE,JDENS)  = QRES(IPRT,JPRT,JTE,JDENS)
     &                                + QRESP(IPRT,JPRT,IGRD,JTE,JDENS)
               ENDDO
             ENDDO
           ENDDO
C
  541    CONTINUE
  540  CONTINUE
C-----------------------------------------------------------------------
C  PREPARE PASSING FILES FOR CONDENSED MASTER FILE ASSEMBLY AS FOLLOWS:
C
C          1.  ACD.PASS     -   STREAM 60
C          2.  SCD.PASS     -   STREAM 61
C          3.  PRB.PASS     -   STREAM 62
C          4.  XCD.PASS     -   STREAM 63   (WJD  19/5/92)
C          5.  PCASBIN.PASS -   STREAM 64   (WJD  15/10/93)
C
C-----------------------------------------------------------------------
       call xxslen(dsnin,len1,len2)
C
       STRING=DASHES
       if (open76) then
          DO 520 IPRT = 1,NPRSTK
            DO 522 IGRD = 1, NGRSTK
              WRITE(SUBSTRG,2001) IPRT,IGRD,IZ,
     &                            DATE(1:2),DATE(4:5),DATE(7:8)
              STRING(31:80)=SUBSTRG
              WRITE(IUNT76,2002)EQUALS
              WRITE(IUNT76,2006)IPRT,TRMSTK(IPRT),SPRSTK(IPRT),
     &                          NCTSTK(IPRT,IGRD),IGRD,SPNSTK(IGRD),
     &                          SSWSTK(IPRT,IGRD)
              WRITE(IUNT76,2002)DASHES
              WRITE(IUNT76,2003)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
              WRITE(IUNT76,2003)(TEA(JTE)/Z**2,JTE=1,JTEM)
              WRITE(IUNT76,2002)STRING
              DO 524 JTE=1,JTEM
                WRITE(IUNT76,2003)(ARES(IPRT,IGRD,JTE,JDENS),
     &                            JDENS=1,JDENSM)
  524         CONTINUE
  522       CONTINUE
  520     CONTINUE
          write(iunt76,5005)iz,iz0in,dsnin(1:len2),realname,date(1:8)
       endif
C-----------------------------------------------------
       if (open61) then
          DO 530 IGRD = 1,NGRSTK
            DO 532 IPRT = 1, NPRSTK
               WRITE(SUBSTRG,2001) IPRT,IGRD,IZ,
     &                             DATE(1:2),DATE(4:5),DATE(7:8)
               STRING(31:80)=SUBSTRG
               WRITE(IUNT61,2002)EQUALS
               WRITE(IUNT61,2006)IPRT,TRMSTK(IPRT),SPRSTK(IPRT),
     &                           NCTSTK(IPRT,IGRD),IGRD,SPNSTK(IGRD),
     &                           SSWSTK(IPRT,IGRD)
               WRITE(IUNT61,2002)DASHES
               WRITE(IUNT61,2003)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
               WRITE(IUNT61,2003)(TEA(JTE)/Z**2,JTE=1,JTEM)
               WRITE(IUNT61,2002)STRING
               DO 534 JTE=1,JTEM
C                RATIO=SEX(JTE)/S0(JTE)
                 DO 536 JDENS=1,JDENSM
C                  SAAP(JDENS)=SAA(JTE,JDENS)*RATIO
                   IF(SRES(IGRD,IPRT,JTE,JDENS) .LE. 1.0D-48)THEN
                      SRES(IGRD,IPRT,JTE,JDENS) = 0.0D+0
                   ENDIF
  536            CONTINUE
                 WRITE(IUNT61,2003)(SRES(IGRD,IPRT,JTE,JDENS),
     &                             JDENS=1,JDENSM)
  534          CONTINUE
  532        CONTINUE
  530     CONTINUE
          write(iunt61,5007)iz,iz0in,dsnin(1:len2),realname,date(1:8)
       endif
C-----------------------------------------------------
C      WRITE(IUNT62,2002)EQUALS
C      WRITE(IUNT62,2006) IPRT,TRMSTK(IPRT),SPRSTK(IPRT),
C    &           NCTSTK(IPRT,IGRD),IGRD,SPNSTK(IGRD),SSWSTK(IPRT,IGRD)
C      WRITE(IUNT62,2002)DASHES
C      WRITE(IUNT62,2003)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
C      WRITE(IUNT62,2003)(TEA(JTE)/Z**2,JTE=1,JTEM)
C      WRITE(IUNT62,2002)STRING
C      DO 512 JTE=1,JTEM
C       WRITE(IUNT62,2003)(PAA(JTE,JDENS),JDENS=1,JDENSM)
C 512  CONTINUE
C
C-----------------------------------------------------
       if (open63) then
          DO 544 IPRT = 1,NPRSTK
            DO 546 JPRT = 1, IPRT-1
              WRITE(SUBSTRG,2008) IPRT,JPRT,IZ,
     &                            DATE(1:2),DATE(4:5),DATE(7:8)
              STRING(31:80)=SUBSTRG
              WRITE(IUNT63,2002)EQUALS
Cldh              WRITE(IUNT63,2007)IPRT,TRMSTK(IPRT),SPRSTK(IPRT),
Cldh        &                       NCTSTK(IPRT,IGRD),JPRT,TRMSTK(JPRT),
Cldh        &                       SPRSTK(JPRT)
              WRITE(IUNT63,2007)IPRT,TRMSTK(IPRT),SPRSTK(IPRT),
     &                          0,JPRT,TRMSTK(JPRT),
     &                          SPRSTK(JPRT)
              WRITE(IUNT63,2002)DASHES
              WRITE(IUNT63,2003)(DENSA(JDENS)/Z**7,JDENS=1,JDENSM)
              WRITE(IUNT63,2003)(TEA(JTE)/Z**2,JTE=1,JTEM)
              WRITE(IUNT63,2002)STRING
              DO 548 JTE=1,JTEM
                WRITE(IUNT63,2003)(QRES(IPRT,JPRT,JTE,JDENS),
     &                            JDENS=1,JDENSM)
  548         CONTINUE
  546       CONTINUE
  544     CONTINUE
          write(iunt63,5005)iz,iz0in,dsnin(1:len2),realname,date(1:8)
       endif
C---------------------------------------------------------------------
C
C  GO TO 197 AND READ IN NEW VALUE OF JDENSM
C---------------------------------------------------------------------
C
       GO TO 197

 5000  CONTINUE

       STOP
C
C
  100  FORMAT(2I5,  1P,6E10.2)
  101  FORMAT(I5,5X,I5,5X,  1P,6E10.2)
  102  FORMAT(4I5)
  103  FORMAT(14I5)
  104  FORMAT(  1P,3E20.5)
  105  FORMAT(2I5,  1P,3E20.5)
  106  FORMAT(2I5,  1P,6E20.5)
  107  FORMAT(2I5,  1P,4E20.5)
  108  FORMAT(2I5,  1P,3E20.5)
  109  FORMAT(2I5,  1P,6E20.5)
  110  FORMAT(  1P,11E12.3)
  111  FORMAT(  1P,6E15.5)
CX  112  FORMAT(I5,  1P,8E15.5)
  112  FORMAT(I5,  1P,5E15.5)
  113  FORMAT(I5)
  114  FORMAT(7F10.5)
  115  FORMAT(7F10.5)
  116  FORMAT(  1P,5E20.5)
  117  FORMAT(//,' JDENSM     JTEM      TRAD        W         Z
     &CION    CP(Y)     W1')
  118  FORMAT(//19HNMIN NMAX IMAX NLWS)
  119  FORMAT(//19H   NREP(I),I=1,IMAX)
  120  FORMAT(//4HJCOR)
  121  FORMAT(//18H   COR(J),J=1,JCOR)
  122  FORMAT(//4HJMAX)
  123  FORMAT(//20H   EPSIL(J),J=1,JMAX)
  124  FORMAT(//18H   FIJ(J),J=1,JMAX)
  125  FORMAT(//4HJDEF)
  126  FORMAT(//29H   QUANTUM DEFECT(J),J=1,JDEF)
  127  FORMAT(/53H     157890ZZ/TE       157890ZZ/TRAD              RHO)
  128  FORMAT(1HO,'  I    N              A(N)              RHS(I)
     1       DIELEC                CHEXCH           RHSREC(I)')
  129  FORMAT(/79H  N           CN             BN        EXP(XN)BN     P
     2OPULATION     C(N)-C(N-1))
  130  FORMAT(/81H       RECOMB COEFT        IONIZN COEFT     REC COEFT(
     &NE=0)     ION COEFT(W1=0)  )
  131  FORMAT(/ / ,'    IPAR    ISYS   ICTPR     NE     TE ',
     &            '        NP         TP                157890ZZ/TE
     &157890ZZ/TRAD           RHO')
  132  FORMAT( 1P,2D10.2,3D20.5)
  133  FORMAT( 3I8,1P,4E10.2,3D20.5)
  134  FORMAT(  1P,7E10.2)
  135  FORMAT(//20H DENSA(J),J=1,JDENSM)
  136  FORMAT(//16H TEA(J),J=1,JTEM)
  137  FORMAT(//36H  NIP       INTD      IPRS      ILOW)
  138  FORMAT(  1P,6E12.4)
  139  FORMAT(  1P,1E12.4)
  140  FORMAT(//20H   WBREP(I),I=1,IMAX)
  141  FORMAT(//20H DENPA(J),J=1,JDENSM)
  142  FORMAT(//16H TPA(J),J=1,JTEM)
  143  FORMAT(//,'   NP         TP                157890ZZ/TE     15789
     &0ZZ/TRAD           RHO')
  144  FORMAT(//18H   WIJ(J),J=1,JMAX)
  145  FORMAT(//,'BEAM ENER.(EV)',10X,'H DENSITY(CM-3)',10X,
     &'FLUX(CM-2SEC-1)')
  146  FORMAT(5X,1PE10.2,15X,1PE10.2,15X,1PE10.2)
  149  FORMAT(///1H ,'----------------------------------------------'/
     &        //,' IPRT:  NCUT(IPRT,JPRT), JPRT = 1,IPRT ')
  150  FORMAT(1H ,I4,6X, 10I6)
  151  FORMAT(//,'  NREP(I), I=1,IMAXRF,  ADJUSTED FOR NCUT. IMAXRF = '
     &            ,I4)
  155  FORMAT(1H ,' AMG = ',1P,D10.2,2X,' AM1 = ',D10.2,
     &            '  AMT = ',D10.2)
  156  FORMAT(1H ,' S1G = ',1P,D10.2,2X,' S1M = ',D10.2,
     &            '  S1T = ',D10.2)
  157  FORMAT(I5,  1P,7E15.5)
C
 1000  FORMAT(//,' COLLISIONAL RADIATIVE MATRIX BEFORE INVERSION'/)
 1001  FORMAT(1H ,1P,10D13.5)
 1002  FORMAT(//,' COLLISIONAL RADIATIVE MATRIX AFTER  INVERSION'/)
 1003  FORMAT(1H ,1P,13X,9D10.2)
 1131  FORMAT(//,/'------------'/
     &            '    IPAR    ISYS   ICTPR     NE     TE ')
 1161  FORMAT(1H ,' JPRT  = ',I3,' ALOSS = ',1P,D10.3,' SPRT = ',D10.3)
 1162  FORMAT(1H ,' JPRT  = ',I3,'  QPRT = ',1P,D10.3)
 1163  FORMAT(1H ,' SUM3  = ',1P,D10.3,'        ATOT = ',D10.2)
 1164  FORMAT(1H ,' ADIR  = ',1P,D10.3,'        STOT = ',D10.2)
 1165  FORMAT(1H ,' RHS(1)= ',1P,D10.3,'        AREC = ',D10.2)
 1166  FORMAT(1H ,' ')
 1167  FORMAT(//,' AGRL :  N      J   IGRL = ',9(5X,I3,4X) )
 1168  FORMAT(//,' ALOSS: IREP  NREP  JPRT = ',9(5X,I3,4X) )
 1169  FORMAT(1H , 8X, I3, 2X, I4, 10X, 1P, 8D12.3 )
 1170  FORMAT(/'            MAINCL       <------------------MAINBNS-----
     &------------->')
 1171  FORMAT('                         IPAR=   0         1          2
     &              ')
 1172  FORMAT(' ALFA : ',1P,1D12.3,5X,5D12.3)
 1173  FORMAT(' S    : ',1P,1D12.3,5X,5D12.3)
 1176  FORMAT(//,' IREP NREP      A(J,1)    A(1,J)       ALT(I) AL(1
     &,I)   AL(2,I)     SUMION' )
 1177  FORMAT(1H , 2I5, 1P, 3X, 2D10.2, 3X, 3D10.2, 3X,3D10.2)
 1178  FORMAT(1H ,33X,1P,D10.2,23X,D10.2,23X,D10.2)
C
 1995  FORMAT(1P,5E20.5)
 1996  FORMAT(//,'        LINE POWER        RAD RECOM POWER    DIEL REC
     &OM POWER     BREMSSTRAHLUNG  (LN PWR)/NE (NE=0) ')
 1997  FORMAT(' W1 IS SWITCHED OFF....W1 = ',  1P,1E12.2)
 1998  FORMAT(' NH IS SWITCHED OFF....NH = ',  1P,1E12.2)
 1999  FORMAT(I5,3(5X,I5))
 2000  FORMAT(4I5)
 2001  FORMAT('/ IPRT=',I2,'  / IGRD=',I2,'  / Z1=',I2,'   / DATE= ',
     &        1A2,'.',1A2,'.',1A2)
 2002  FORMAT(1A80)
 2003  FORMAT(1P,8D10.2)
 2004  FORMAT(4X,I2,11X,1A4,10X,I2,9X,I4,6X,I2,9X,I2)
 2005  FORMAT(/ / /1H ,'IPRT = ',I2,5X,'TRMPRT = ',1A4,5X,'SPNPRT = ',
     & F3.0,5X,'NCTPRT = ',I4,5X,'IGRD = ',I2,5X,'SPNSYS = ',F3.0,5X,
     & 'SSYSWT = ',F5.2 / 1H ,
     & '----------------------------------------------------------------
     &-------------------------------------------------')
 2006  FORMAT('IPRT=',I2,2X,'TRMPRT=',1A4,2X,'SPNPRT=',F3.0,
     & 2X,'NCTPRT=',I4,2X,'IGRD=',I2,2X,'SPNSYS=',F3.0,2X,
     & 'SSYSWT=',F5.2)
 2007  FORMAT('IPRT=',I2,2X,'TRMPRT=',1A4,2X,'SPNPRT=',F3.0,
     & 2X,'NCTPRT=',I4,2X,'JPRT=',I2,2X,'TRMPRT=',1A4,2X,
     & 'SPNPRT=',F3.0)
 2008  FORMAT('/ IPRT=',I2,'  / JPRT=',I2,'  / Z1=',I2,'   / DATE= ',
     &        1A2,'.',1A2,'.',1A2)
 3022  FORMAT(//,'RADIATIVE RECOMBINATION PARAMETERS'/1H ,4X,'N0=',I2,
     &5X,'V0=',F10.5,5X,'PHFRAC=',F10.5,5X,'EDISP=',F10.5,5X,'SCALE=',
     &F10.5)
 3023  FORMAT(//,'DIELECTRONIC RECOMBINATION PARAMETERS',5X,'NTRANS=',
     &I2/1H ,'  ITYPE N1     V1       PHFRAC  NCUT   EPSIJ      FIJ
     & CORFAC     WIJ     EDISGP    SCALGP')
 3024  FORMAT(1H ,2I5,2F10.5,I5,3F10.5,1P,D10.2)
 3025  FORMAT(//,'IONISATION COEFFICIENT PARAMETERS',5X,'NIGRP=',I2,5X,
     & 'NRGRP=',I2,5X,'LIOSEL = ',1A5)
 3026  FORMAT(1H ,6X,' CI         ZT,EPSI PAIRS')
 3027  FORMAT(1H ,F10.3,4X,6(I6,F9.5)/1H ,14X,6(I6,F9.5)/
     &1H ,14X,3(I6,F9.5))
 3028  FORMAT(1H ,6X,' CR         WGHT,EPSR PAIRS')
 3029  FORMAT(1H ,F10.3,4X,6(F6.2,F9.5)/1H ,14X,6(F6.2,F9.5)/
     &1H ,14X,3(F6.2,F9.5))
 3030  FORMAT(1H ,'JPRT = ',I2,4X,'PRTWGT =',F6.3,4X,'IBSEL = ',I2)
 3039  FORMAT(14I5)
 3040  FORMAT(I5,4F10.5)
 3041  FORMAT(I2,I5,2F9.4,I5,3F10.5,D10.2)
 3044  FORMAT(50X,2F10.5)
 3047  FORMAT(1H ,70X,2F10.5)
 3042  FORMAT(F5.3,5(I5,F10.5)/5X,5(I5,F10.5)/5X,5(I5,F10.5))
 3043  FORMAT(F5.3,5(F5.2,F10.5)/5X,5(F5.2,F10.5)/5X,5(F5.2,F10.5))
 3054  FORMAT(//,'ZERO DENSITY EXACT RECOM COEFTS')
 3055  FORMAT(//,'ZERO DENSITY EXACT IONIS COEFTS')
 3056  FORMAT(/ / )
 3057  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC RECOMBINATION COEFFICIEN
     &T'///,'TE(K)   DENS',1P,10D12.4,2(/1H ,12X,1P,10D12.4))
 3058  FORMAT(1H ,1P,11D12.4)
 3059  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC IONISATION COEFFICIENT'
     &///,'TE(K)   DENS',1P,10D12.4,2(/1H ,12X,10D12.4))
 3060  FORMAT(1H ,1P,11D12.4)
 3061  FORMAT(///1H ,'BREMS, RECOM & CASCADE POWER COEFFICIENT'
     &///,'TE(K)   DENS',1P,10D12.4,2(/1H ,12X,10D12.4))
 3062  FORMAT(1H ,1P,11D12.4,2(/1H ,12X,10D12.4))
 3063  FORMAT(///1H ,'EXACT RECOM COEFT RATIO TABLE WITH REDUCED PARAME
     &TERS'///,'   Z1   THETA      ALF    RHO=',1P,10D10.2,
     & 2(/1H ,30X,10D10.2))
 3064  FORMAT(1H ,5X,1P,2D10.2,5X,1P,10D10.2,2(/1H ,30X,10D10.2))
 3065  FORMAT(1H ,I5,1P,2D10.2,5X,1P,10D10.2,2(/1H ,30X,10D10.2))
 3066  FORMAT(///1H ,'EXACT IONIS COEFT RATIO TABLE WITH REDUCED PARAME
     &TERS'///,'   Z1   THETA       S     RHO=',1P,10D10.2,
     & 2(/1H ,30X,10D10.2))
 3067  FORMAT(1H ,5X,1P,2D10.2,5X,1P,10D10.2,2(/1H ,30X,10D10.2))
 3068  FORMAT(1H ,I5,1P,2D10.2,5X,1P,10D10.2,2(/1H ,30X,10D10.2))
 3069  FORMAT(///1H ,'EXACT POWER COEFT RATIO TABLE WITH REDUCED PARAME
     &TERS'///,'   Z1   THETA      PWR    RHO=',1P,10D10.2,
     & 2(/1H ,30X,10D10.2))
 3070  FORMAT(1H ,5X,1P,2D10.2,5X,1P,10D10.2,2(/1H ,30X,10D10.2))
 3071  FORMAT(1H ,I5,1P,2D10.2,5X,1P,10D10.2,2(/1H ,30X,10D10.2))
 3072  FORMAT(//,'   TE(K)       DENS       ACD      ACD(NE=0)  SCD(W1=
     &0)   S(NE=0)    CC PWR     RR PWR     DR PWR     BS PWR   CP/NE (N
     &E=0)')
 3073  FORMAT(1H ,1P,11D11.4)
 3074  FORMAT(/ / )
 3077  FORMAT(5X,I2,8X,I2,3X,5(F6.3))
 3078    FORMAT(//, 'N SHELL PARAMETERS'/1H ,4X,
     &               'NMIN=',I3,3X,'JDEF=',I3,3X,'DEFECTS=',5F7.3)
 3080  FORMAT(6X,1A2,11X,I2,9X,I3)
 3081  FORMAT(2I5,11X,I2)
 3082  FORMAT(21X,F6.3,9X,I3)
 3083  FORMAT(//,'ZERO DENSITY EXACT IONIS COEFTS FROM SZD FILE :')
 3084  FORMAT(1H ,'JPRT =',I2,':',1P,8D10.2/10X,8D10.2/10X,8D10.2)
 3085  FORMAT(1H ,'TOTAL   :',1P,8D10.2/10X,8D10.2/10X,8D10.2)
 3087  FORMAT(//,' SEQUENCE = ',1A2,3X,'NUCCHG =',I3,3X,'NPRSTK =',I2,
     &            3X,'NO. OF IONIS/RECOM PATHWAYS =',I3)
 3090  FORMAT(//,'  JPRT   TRMPRT   SPNPRT   IGRD   SPNGRD   SSYWGT')
 3091  FORMAT(1H ,3X,I2,5X,1A4,5X,F3.0,6X,I2,6X,F3.0,5X,F5.3)
 3092  FORMAT(//,'EXPONENT SCALED EXACT IONIS COEFTS FROM SZD FILE :')
 3093  FORMAT(1H ,'JPRT =',I2,' BWNO =',F15.1,':',1P,8D10.2/32X,8D10.2,
     & /10X,8D10.2)
 3094  FORMAT(1H ,'TOTAL    BWNO =',F15.1,':',1P,8D10.2/32X,8D10.2,
     & /10X,8D10.2)
 3100  FORMAT(1H ,'SEQUENCE = ',1A2,3X,'NUCCHG =',I2,3X,'NPARNT =',I2,
     &            3X,'MAXD =',I2,3X,'MAXT =',I2/)
 3101  FORMAT(1H ,'NE(CM-3) = ',1P,12D10.2/1H ,11X,12D10.2)
 3102  FORMAT(1H ,'TE(K)    = ',1P,12D10.2/1H ,11X,12D10.2)
 3103  FORMAT(12X)
 3104  FORMAT(1H ,'IPRT = ',I2,5X,'TRMPRT = ',1A4,5X,'SPNPRT = ',F3.0/
     &        1H ,'--------------------------------------------')
 3105  FORMAT(1H ,49X,'SPNSYS = ',F3.0,5X,'NSHEL = ',I2/
     &        1H ,49X,'---------------------------')
 3106  FORMAT('---------------------------------------------------------
     &------------------------------------------------------------------
     &------------------------------------------------------------------
     &------------------------------------------------------------')
 3108  FORMAT(1H ,'IEDMAT =',I2,3X,'IECION =',I2,3X,
     &            'IETREC =',I2,3X,'IEDREC =',I2,3X,
     &            'IERREC =',I2,3X,'IEXREC =',I2,3X,
     &            'IERSYS =',I2 /)
 3998  FORMAT(/ / /1H ,'MAINBNS: BUNDLE-N POPULATION CODE FOR PARENT/
     &METASTABLE RESOLVED COEFFICIENTS (',1A2,'/',1A2,'/',1A2     ,')'/
     &            1H ,'-------------------------------------------------
     &-----------------------------------------')
 3999  FORMAT(//,'END OF PROGRAM:  NPARNT =',I3,4X,'NPRSTK =',I3)
C
 4100  FORMAT(///1H ,' METASTABLE RESOLVED COEFFICIENTS ' /
     &            1H ,' -------------------------------- ' )
 4101  FORMAT(1H ,1P,11D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
 4102  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC IONISATION COEFFICIENT :
     & IGRD = ',I2,' TO JPRT = ',I2,' (VIA IPRT = ',I2,')',
     &    ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
 4104  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC IONISATION COEFFICIENT :
     & TOTAL FROM IGRD = ',I2,' (VIA  IPRT = ',I2,')',
     &   ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
 4106  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC RECOMBINATION COEFFICIEN
     &T : IPRT = ',I2,' TO IGRD = ',I2,
     &    ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
 4108  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC CROSS-COUPLING COEFFICIE
     &NT : IPRT = ',I2,' TO JPRT = ',I2,' (VIA IGRD = ',I2,')',
     &    ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
 4110  FORMAT(///1H ,'COLLISIONAL DIELECTRONIC RECOMBINATION COEFFICIEN
     &T : TOTAL FROM IPRT = ',I2,' (VIA IGRD = ',I2,')',
     &    ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
C
 4200  FORMAT(//,/1H ,' COMPARISON BETWEEN STANDARD AND RESOLVED COEFFI
     &CIENTS ',/   1H ,' -----------------------------------------------
     &------ ' )
 4201  FORMAT(1H ,1P,1D12.4,0P,10(F9.3,3X),/1H ,12X,10(F9.3,3X),
     &       /1H ,12X,10(F9.3,3X))
 4202  FORMAT(///1H ,'( ACDA MET. RESOLVED )/( ACDA - SUMMERS ) ',
     &    ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
 4204  FORMAT(///1H ,'(SUM OF SCDA MET. RESOLVED )/( SCDA - SUMMERS )',
     &    ///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
C
 4300  FORMAT(1H /1H ,'TE = ',1P,D10.2,3X,'NE = ',D10.2)
 4304  FORMAT(1H /1H ,' NREP       BN            QCREP          QRRREP
     &     QRTREP       QRTREP*CN(I) ')
 4306  FORMAT(1H /1H ,9X,'QCRC1             QRRC1               QRTC1')
 4310  FORMAT(///1H ,'RECOMBINING ELECTRON COOLING COEFFICIENT '
     &///,'TE(K)   DENS',1P,10D12.4/1H ,12X,10D12.4/1H ,12X,10D12.4)
C
 5005  format(80('-'),/,
     &        'C ',/,
     &        'C  Information',/,
     &        'C  ===========',/,
     &        'C ',/,
     &        'C  Ion Charge +1 :',i3,4x,'Nuclear charge :',i3,/,
     &        'C  Input File : ',a,/,
     &        'C ',/,
     &        'C  Code     : ADAS204',/,
     &        'C  Producer : ',a30,/,
     &        'C  Date     : ',a10,/,
     &        80('-'))

 5007  format(80('-'),/,
     &        'C ',/,
     &        'C  Information',/,
     &        'C  ===========',/,
     &        'C ',/,
     &        'C  Ion Charge +1 :',i3,4x,'Nuclear charge :',i3,/,
     &        'C  Input File : ',a,/,
     &        'C  Ionisation from adf07 ionelec data : ',/,
     &        'C  Meta. ion coeff. selector          : insel(?,?)',/,
     &        'C ',/,
     &        'C  Code     : ADAS204',/,
     &        'C  Producer : ',a30,/,
     &        'C  Date     : ',a10,/,
     &        80('-'))

      END
