CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/photolt.for,v 1.1 2004/07/06 14:28:06 whitefor Exp $ Date $Date: 2004/07/06 14:28:06 $
CX
       SUBROUTINE PHOTOLT(PION,PREC,PSTIM,Z,TE,TP,EN,N,NCUT,
     & KPION,KPREC,KPSTIM)
       IMPLICIT REAL*8 (A-H,O-Z)                                        
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 SUBROUTINE: PHOTOLT ************************
C
C  VERSION:  1.0
C
C  PURPOSE: 
C
C  VARIANT OF PHOTO:  MULTIPLIES PREC BY EXP(XNT) IF N<NCUT
C
C  NOT YET FULLY ANNOTATED
C
C  ******** H.P.SUMMERS, JET                  23 JAN 1990     **********
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 22-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED. NO CHANGES.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       DIMENSION A(10),B(10),U(10),V(10),R(10),S(10),D(10),WA(10),WB(10)
       XNT=157890.0*Z*Z/(TE*EN*EN)                                      
       XNTP=XNT*TE/TP                                                   
       UXNT=DLOG(XNT)                                                   
       UXNTP=DLOG(XNTP)                                                 
       MAX=6                                                            
       A(1)=0.2228466                                                   
       A(2)=1.1889321                                                   
       A(3)=2.9927363                                                   
       A(4)=5.7751436                                                   
       A(5)=9.8374674                                                   
       A(6)=15.9828740                                                  
       WA(1)=0.45896467                                                 
       WA(2)=0.41700083                                                 
       WA(3)=0.11337338                                                 
       WA(4)=0.010399197                                                
       WA(5)=0.0002610172                                               
       WA(6)=0.00000089855                                              
       B(1)=-0.9324695                                                  
       B(2)=-0.6612094                                                  
       B(3)=-0.2386192                                                  
       B(4)=0.2386192                                                   
       B(5)=0.6612094                                                   
       B(6)=0.9324695                                                   
       WB(1)=0.1713245                                                  
       WB(2)=0.3607616                                                  
       WB(3)=0.4679139                                                  
       WB(4)=0.4679139                                                  
       WB(5)=0.3607616                                                  
       WB(6)=0.1713245                                                  
       GO TO 20                                                         
    1  T1=0.0                                                           
       DO 4 I=1,MAX                                                     
       UI=(U(I)-COR)/COR                                                
    4  T1=FACTOR*GBF(EN,UI)*WA(I)/D(I)+T1                               
       IF(SWIT-0.5)5,10,10                                              
    5  T2=0.0                                                           
       DO 7 J=1,MAX                                                     
       VJ=(V(J)-COR)/COR                                                
    7  T2=COEFFT*GBF(EN,VJ)*WB(J)/R(J)+T2                               
       T=T1+T2                                                          
       GO TO (29,39,49),K                                               
   10  T=T1                                                             
       GO TO (29,39,49),K                                               
   20  SWIT=XNT                                                         
       COR=XNT                                                          
       K=1                                                              
       IF(KPREC)28,28,21                                                
   21  IF(SWIT-0.5)24,22,22                                             
   22  DO 23 I=1,MAX                                                    
       D(I)=A(I)+XNT                                                    
   23  U(I)=D(I)                                                        
C
       IF(N.GE.NCUT)THEN
           FACTOR=DEXP (-SWIT)                                          
       ELSE
           FACTOR=1.0D0
       ENDIF
C
       GO TO 1                                                          
   24  P=-UXNT                                                          
       Q=-P                                                             
C
       IF(N.GE.NCUT)THEN
           COEFFT=0.5*P                                                 
       ELSE
           COEFFT=0.5*P*DEXP(XNT)
       ENDIF
C
       DO 25 I=1,MAX                                                    
       D(I)=A(I)+1.0                                                    
       U(I)=D(I)                                                        
        BI=B(I)                                                         
       S(I)=DEXP(0.5*(P*BI+Q))                                          
       V(I)=S(I)                                                        
       SI=S(I)                                                          
   25  R(I)=DEXP(SI)                                                    
C
       IF(N.GE.NCUT)THEN
           FACTOR=0.367879441                                           
       ELSE
           FACTOR=0.367879441*DEXP(XNT)
       ENDIF
C
       GO TO 1                                                          
   28  PREC=0.0                                                         
       GO TO 30                                                         
   29  PREC=T                                                           
   30  SWIT=XNTP                                                        
       COR=XNTP                                                         
       K=2                                                              
       IF(KPION)38,38,31                                                
   31  IF(SWIT-0.5)34,32,32                                             
   32  DO 33 I=1,MAX                                                    
       X=A(I)+XNTP                                                      
       D(I)=X*(1.0-DEXP(-X))                                            
   33  U(I)=X                                                           
       FACTOR=DEXP(-SWIT)                                               
       GO TO 1                                                          
   34  AP=1.0                                                           
       BP=1.0/XNTP                                                      
       P=BP-AP                                                          
       Q=BP+AP                                                          
       COEFFT=0.5*P                                                     
       DO 35 I=1,MAX                                                    
       U(I)=(A(I)+1.0)                                                  
       AI=A(I)                                                          
       D(I)=(A(I)+1.0)*(1.0-DEXP(-AI)*0.367879441)                      
       X=2.0/(P*B(I)+Q)                                                 
       V(I)=X                                                           
       R(I)=(1.0+0.5*X*(1.0+0.33333*X*(1.0+0.25*X*(1.0+0.2*X*           
     & (1.0+0.1666667*X)))))                                            
   35  CONTINUE                                                         
       FACTOR=0.367879441                                               
       GO TO 1                                                          
   38  PION=0.0                                                         
       GO TO 40                                                         
   39  PION=T                                                           
   40  SWIT=XNTP                                                        
       COR=XNTP                                                         
       K=3                                                              
       IF(KPSTIM)48,48,41                                               
   41  IF(TP-TE)42,42,60                                                
   42  IF(SWIT-0.5)45,45,43                                             
   43  DO  44 I=1,MAX                                                   
       U(I)=(A(I)+XNTP)                                                 
       UI=U(I)                                                          
       D(I)=(A(I)+1.0)*(1.0-DEXP(-UI))*DEXP(TP*UI/TE)                   
   44  CONTINUE                                                         
       FACTOR=DEXP(-SWIT)                                               
       GO TO 1                                                          
   45  AP = 1.0                                                         
       BP=1.0/XNTP                                                      
       P=BP-AP                                                          
       Q=BP+AP                                                          
       COEFFT=0.5*P                                                     
       DO 46 I=1,MAX                                                    
       U(I)=(A(I)+1.0)                                                  
       UI=U(I)                                                          
       D(I)=(A(I)+1.0)*(1.0-DEXP(-UI))*DEXP( TP*UI/TE)                  
       X=2.0/(P*B(I)+Q)                                                 
       V(I)=X                                                           
       Y=TP*X/TE                                                        
       R(I)=(1.0+0.5*X*(1.0+0.333333*X*(1.0+0.25*X*(1.0+0.2*X*(1.0      
     & +0.1666667*X)))))                                                
       DENOM=(1.0-Y*(1.0-0.5*Y*(1.0-0.33333*Y*(1.0-0.25*Y*(1.0-0.2*Y    
     & )))))                                                            
       R(I)=R(I)/DENOM                                                  
   46  CONTINUE                                                         
       FACTOR=0.367879441                                               
       GO TO 1                                                          
   60  SWIT=XNT                                                         
       IF(SWIT-TP/TE)93,91,91                                           
   91  DO 92 I=1,MAX                                                    
       U(I)=TE*(A(I)+XNT)/TP                                            
       UI=U(I)                                                          
   92  D(I)=(A(I)+XNT)*(DEXP(UI)-1.0)                                   
       FACTOR=DEXP(-SWIT)                                               
       SWIT=1.0                                                         
       GO TO 1                                                          
   93  AP=TE/TP                                                         
       BP=1.0/XNT                                                       
       P=BP-AP                                                          
       Q=BP+AP                                                          
       COEFFT=0.5*P                                                     
       DO 94 I=1,MAX                                                    
       X=2.0/(P*B(I)+Q)                                                 
       Y=TE*X/TP                                                        
       V(I)=Y                                                           
       R(I)=(TE/TP)*(1.0+0.5*Y*(1.0+0.33333*Y*(1.0+0.25*Y*              
     & (1.0+0.2*Y))))*DEXP(X)                                           
       FACTOR=DEXP(-TP/TE)                                              
       U(I)=TE*A(I)/TP+1.0                                              
       UI=U(I)                                                          
   94  D(I)=(TP/TE)*UI*(DEXP(UI)-1.0)                                   
       SWIT=0.0                                                         
       GO TO 1                                                          
   48  PSTIM=0.0                                                        
       GO TO 50                                                         
   49  PSTIM=T                                                          
   50  RETURN                                                           
      END                                                               
