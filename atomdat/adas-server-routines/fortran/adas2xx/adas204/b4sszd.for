CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/b4sszd.for,v 1.3 2008/03/26 16:17:39 allan Exp $ Date $Date: 2008/03/26 16:17:39 $
CX
      SUBROUTINE B4SSZD( dsname , IBSEL  , IZ0IN  ,
     &                   ITVAL  , TVAL   ,
     &                   BWNO   , IZ     , IZ1  ,
     &                   METI   , METF   ,
     &                   SZDA   , ESZDA  , LTRNG  ,
     &                   TITLX  , IRCODE , OPEN17
     &               )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: B4SSZD ********************
C
C  PURPOSE: TO EXTRACT AND  INTERPOLATE  ZERO-DENSITY  IONIZATION  RATE-
C           COEFFICIENTS FOR GIVEN ELEMENT NUCLEAR CHARGE AND DATA-BLOCK
C           FOR AN INPUT SET OF ELECTRON TEMPERATURES (eV).
C
C
C  CALLING PROGRAM: GENERAL USE
C
C  SUBROUTINE:
C
C  INPUT : (C*80) DSNAME  = ADF07 DATAFILE NAME UNDER UNIX INCLUDING PATH
C  INPUT : (I*4)  IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)  IZ0IN   = NUCLEAR CHARGE OF REQUIRED ELEMENT
C
C  INPUT : (I*4)  ITVAL   = NUMBER OF ELECTRON TEMPERATURE VALUES
C  INPUT : (R*8)  TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  OUTPUT: (R*8)  BWNO    = INPUT FILE - SELECTED DATA-BLOCK:
C                           EFFECTIVE IONIZATION POTENTIAL (cm-1).
C  OUTPUT: (I*4)  IZ      = INPUT FILE - SELECTED DATA BLOCK:
C                           IONIZING ION - INITIAL CHARGE
C  OUTPUT: (I*4)  IZ1     = INPUT FILE - SELECTED DATA BLOCK:
C                           IONIZING ION - FINAL   CHARGE
C
C  OUTPUT: (I*4)  METI    = INPUT FILE - SELECTED DATA-BLOCK:
C                           INITIAL STATE METSTABLE INDEX
C  OUTPUT: (I*4)  METF    = INPUT FILE - SELECTED DATA-BLOCK:
C                           FINAL   STATE METSTABLE INDEX
C
C  OUTPUT: (R*8)  SZDA()  = ZERO-DENSITY IONIZATION RATE-COEFFICIENTS
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  OUTPUT: (R*8)  ESZDA() = EXP((BWNO/109737.3)*(IH/KTE))*SZDA()
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  OUTPUT: (L*4)  LTRNG() =.TRUE.  => OUTPUT 'SZDA()'  VALUE WAS INTER-
C                                     POLATED  FOR  THE  USER  ENTERED
C                                     ELECTRON TEMPERATURE 'TVAL()'.
C                          .FALSE. => OUTPUT 'SZDA()'  VALUE WAS EXTRA-
C                                     POLATED  FOR  THE  USER  ENTERED
C                                     ELECTRON TEMPERATURE 'TVAL()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  OUTPUT: (C*120)TITLX   = INFORMATION STRING (DSN ETC.)
C  OUTPUT: (I*4)  IRCODE  = RETURN CODE FROM SUBROUTINE:
C                           0 => NORMAL COMPLETION - NO ERROR DETECTED
C                           2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                AND THOSE IN INPUT FILE.
C                           3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                OF RANGE OR DOES NOT EXIST.
C                           4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                           9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                INPUT DATA-SET.
C
C          (I*4)  NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)  NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)  IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)  IZ0LST  = LAST VALUE OF 'IZ0IN' FOR  WHICH  INPUT
C                           DATA WAS READ.
C          (I*4)  IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)  NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                           DATA SET.
C          (I*4)  IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C
C          (L*4)  LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                           .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)  ESYM    = INPUT FILE - IONIZING ION - ELEMENT SYMBOL
C          (C*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C          (C*3)  EXTLST  = ADAS SOURCE DATA FILE EXT. USED LAST TIME
C                           DATA WAS READ.
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZOUT() = INPUT DATA FILE: IONIZING ION INITIAL CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ1OUT()= INPUT DATA FILE: IONIZING ION FINAL   CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  BWNOUT()= INPUT DATA FILE: EFFECTIVE IONIZATION POT.
C                           (UNITS: cm-1).
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  SZD(,)    =INPUT DATA SET -
C                            FULL SET OF IONIZATIONS RATE-COEFFICIENTS
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*2)  CICODE()= INPUT DATA FILE - INITIAL STATE META. INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CFCODE()= INPUT DATA FILE - FINAL   STATE META. INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*6)  CIION() = INPUT DATA FILE - INITIAL ION
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*6)  CFION() = INPUT DATA FILE - FINAL   ION
C                           DIMENSION: DATA-BLOCK INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXDATA_07  ADAS      FETCH INPUT DATA FROM SELECTED DATA SET
C          B4SPLN     ADAS      INTERPOLATE DATA WITH ONE-WAY SPLINES
C          E2TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C
C
C Original version
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 6023
C
C DATE:    07/06/91
C
C
C UPDATE:    17/02/97 - H P SUMMERS: RENAME SSZD AS B4SSZD. EXTRACT 
C                                    Exp(I/KTE) * S AS WELL AS S
C
C UPDATE:    04-03-97 - R. MARTIN: ADDED OPEN17 FOR SWITCHING OUTPUT TO
C			           'adas204.pass1' ON AND OFF.
C
C VERSION: 1.2                                           DATE: 03-12-98 
C MODIFIED: Martin O'Mullane: 
C                       Rewritten to account for adf07 filename
C                       being included in the adf25 namelist. It is
C                       now much simplified.
C
C VERSION: 1.3                                           DATE: 26-03-08 
C MODIFIED: Allan Whiteford: 
C                      Changed call from E2DATA to XXDATA_07.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDIM
      INTEGER     IZ0MIN         , IZ0MAX
      INTEGER     IUNIT
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 160   , NTDIM  = 35       )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 60       )
      PARAMETER(  IUNIT  =  16   )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          , IZ0IN             , ITVAL       ,
     &            IZ             , IZ1               ,
     &            METI           , METF              ,
     &            IRCODE
      INTEGER     IZ0LST         , NBSEL             ,
     &            IZ0            , i4unit
C-----------------------------------------------------------------------
      REAL*8      BWNO
C-----------------------------------------------------------------------
      LOGICAL     LOPEN, OPEN17
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , EXTIN*3           , EXTLST*3     ,
     &            titlx*120      , dsname*120        , dsntmp*80    
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             , ITA(NSTORE)          ,
     &            IZOUT(NSTORE)             , IZ1OUT(NSTORE)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , SZDA(ITVAL)          ,
     &            BWNOUT(NSTORE)            , ESZDA(ITVAL)
      REAL*8      TETA(NTDIM,NSTORE)        , SZD(NTDIM,NSTORE)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CICODE(NSTORE)*2          , CFCODE(NSTORE)*2     ,
     &            CIION(NSTORE)*6           , CFION(NSTORE)*6
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IRCODE = 0

      IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN

         open(iunit, file=dsname, status='OLD')
         LOPEN  = .TRUE.

         IF (OPEN17) WRITE(17,*)'IUNIT,IZ0IN,DSNREQ=',
     &                           IUNIT,IZ0IN,DSNAME
     
         dsntmp=dsname(1:80)
         CALL XXDATA_07 ( IUNIT  , dsntmp ,
     &                    NSTORE , NTDIM  ,
     &                    ESYM   , IZ0    ,
     &                    NBSEL  , ISELA  ,
     &                    IZOUT  , IZ1OUT ,
     &                    CICODE , CFCODE , CIION , CFION ,
     &                    BWNOUT ,
     &                    ITA    ,
     &                    TETA   , SZD
     &                  )
         close(iunit)


C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED ELEMENT NUCLEAR CHARGE VALUE.
C Formerly done with e2chkb.
C-----------------------------------------------------------------------

         IF ( (IBSEL.LE.0) .OR. (IBSEL.GT.NBSEL) ) THEN
            IRCODE = 3
         ELSE

            IF ( IZ0IN.NE.IZ0 ) THEN
               IRCODE = 2
            ELSE
               IRCODE = 0
            ENDIF
         ENDIF

C-----------------------------------------------------------------------
C 1) INTERPOLATE/EXTRAPOLATE WITH ONE DIMENSIONAL SPLINE.
C 2) CREATE TITLE FOR OUTPUT.
C-----------------------------------------------------------------------

         IF ( IRCODE.EQ.0 ) THEN
            CALL B4SPLN( ITA(IBSEL)        , ITVAL         ,
     &                   BWNOUT(IBSEL)     ,
     &                   TETA(1,IBSEL)     , TVAL          ,
     &                   SZD(1,IBSEL)      ,
     &                   SZDA              , ESZDA         ,
     &                   LTRNG
     &                 )
            CALL E2TITL( IBSEL         , DSNAME        ,
     &                   CICODE(IBSEL) , CFCODE(IBSEL) ,
     &                   CIION(IBSEL)  , CFION(IBSEL)  ,
     &                   TITLX
     &                 )
C
            BWNO = BWNOUT(IBSEL)
            IZ   = IZOUT(IBSEL)
            IZ1  = IZ1OUT(IBSEL)
            READ(CICODE(IBSEL),*) METI
            READ(CFCODE(IBSEL),*) METF
C
         ENDIF
C
         
      ELSE
         IRCODE = 4
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
