CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/b4spf0.for,v 1.3 2005/05/24 15:39:23 mog Exp $ Date $Date: 2005/05/24 15:39:23 $
CX
      SUBROUTINE B4SPF0( REP, DSNIN, LBTSEL, ADAS_C, ADAS_U,
     &                   opfiles, opstatus)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B4SPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL OR THE INFO FILE SET
C           UP FOR BATCH EXECUTION FOR INPUT AND OUTPUT FILE NAMES
C
C  NOTE : REPLACES B4SPFX.FOR
C         Rationalised input. Cross reference file is now read in
C         from the driver adf25 file.
C
C  CALLING PROGRAM: ADAS204
C
C  SUBROUTINE:
C
C  OUTPUT: (C*120) DSNIN   = INPUT FILE NAME
C  OUTPUT: (C*80)  PASSDIR = DIRECTORY NAME FOR PASSING FILES
C  OUTPUT: (C*120) DSNXRT  = FIRST PART OF CROSS REFERENCE FILE NAME
C  OUTPUT: (C*3)   REP     = 'YES' => CANCEL SELECTED FROM INPUT SCREEN
C			     'NO'  => NOT SELECTED
C  OUTPUT: (L*4)   LBTSEL  = .TRUE. => 'RUN IN BATCH' SELECTED
C                            .FALSE. => 'RUN NOW' SELECTED
C  OUTPUT: (C*80)  ADAS_C  = CENTRAL ADAS LOCATION
C  OUTPUT: (C*80)  ADAS_U  = USER ADAS LOCATION
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    26-11-98
C
C VERSION: 1.1                          DATE: 26-11-98
C MODIFIED:  Martin O'Mullane
C     - FIRST VERSION.
C
C VERSION: 1.2                          DATE: 13-09-2002
C MODIFIED:  Martin O'Mullane
C     - Read in output files and a write/no-write action list.
C
C VERSION: 1.3                          DATE: 23-05-2005
C MODIFIED: Martin O'Mullane
C     - Add another optional output file for populations.
C
C-----------------------------------------------------------------------
      INTEGER     PIPEIN
C-----------------------------------------------------------------------
      PARAMETER ( PIPEIN = 5  )
C-----------------------------------------------------------------------
      INTEGER     IPEND     , I
C-----------------------------------------------------------------------
      LOGICAL     LBTSEL
C-----------------------------------------------------------------------
      CHARACTER   DSNIN*120 , PASSDIR*80, REP*3
      CHARACTER   ADAS_C*80 , ADAS_U*80
C-----------------------------------------------------------------------
      INTEGER     opstatus(9)
C-----------------------------------------------------------------------
      character   opfiles(9)*80
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C READ IN DATA FROM STANDARD INPUT (IDL OR INFO FILE)
C-----------------------------------------------------------------------


 10   READ(PIPEIN,'(A)')REP
      IF(REP.EQ.'NO')THEN

         READ(PIPEIN,'(A)')DSNIN
         READ(PIPEIN,'(A)')ADAS_C
         READ(PIPEIN,'(A)')ADAS_U

         READ(PIPEIN, *) IPEND
         IF(IPEND.NE.1)THEN
            do i=1,9
              READ(PIPEIN,'(A)')opfiles(i)
              READ(PIPEIN,*)opstatus(i)
            end do
         ELSE
            GOTO 10
         ENDIF

      ENDIF
      
C IPEND = 2 FOR BATCH EXECUTION

      IF(IPEND.EQ.2)THEN
         LBTSEL=.TRUE.
      ELSE
         LBTSEL=.FALSE.
      ENDIF

 99   CONTINUE
      RETURN
      END

