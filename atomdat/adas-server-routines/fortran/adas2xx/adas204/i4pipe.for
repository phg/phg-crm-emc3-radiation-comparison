CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/i4pipe.for,v 1.1 2004/07/06 14:07:56 whitefor Exp $ Date $Date: 2004/07/06 14:07:56 $
CX
       SUBROUTINE I4PIPE(NUM)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: I4PIPE ***********************
C
C  VERSION:  1.0
C
C  PURPOSE: WRITES AN INTEGER TO THE PIPE FOR ADAS204
C
C  NOTES:  FOR SOME STRANGE REASON, THE PIPE REFUSES TO FLUSH ON SUN
C          MACHINES IF WRITES ARE PUT DIRECTLY INTO ADAS204.FOR. THIS
C          ROUTINE SEEMS TO MAKE THINGS WORK.
C          
C  PROGRAM:
C     (I*4)  PIPEOU    =  PIPE OUTPUT STREAM TO IDL
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     ADAS      FLUSHES I/O BUFFER
C
C-----------------------------------------------------------------------
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    11TH SEPTEMBER 1996
C
C VERSION: 1.1                          DATE: 11-09-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION.
C-----------------------------------------------------------------------
C
      INTEGER PIPEOU, NUM
      PARAMETER(PIPEOU = 6)

      WRITE(PIPEOU,*)NUM
      CALL XXFLSH(PIPEOU)

      RETURN

      END
