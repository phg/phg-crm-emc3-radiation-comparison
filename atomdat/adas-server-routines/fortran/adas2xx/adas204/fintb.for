CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/fintb.for,v 1.2 2004/07/06 13:54:44 whitefor Exp $ Date $Date: 2004/07/06 13:54:44 $
CX
       REAL*8 FUNCTION FINTB(X)
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 SUBROUTINE: FINTB   ************************
C
C  VERSION:  1.0
C
C  PURPOSE:  UNKNOWN
C
C  THIS SUBROUTINE IS NOT YET PROPERLY DOCUMENTED
C
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 22-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED. NO CHANGES.
C
C VERSION: 1.2                          DATE: 20-09-99
C MODIFIED:     RICHARD MARTIN
C               ADDED "REAL*8" TO  "FUNCTION FINTB(X)"
C
C-----------------------------------------------------------------------
       IMPLICIT REAL*8 (A-H,O-Z)
       FINTB=X
       RETURN
       END
