CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/b4spfx.for,v 1.2 2004/07/06 11:23:00 whitefor Exp $ Date $Date: 2004/07/06 11:23:00 $
CX
      SUBROUTINE B4SPFX( REP, DSNIN, PASSDIR, DSNXRT,
     &                   LBTSEL, USERID)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B4SPFX *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL OR THE INFO FILE SET
C           UP FOR BATCH EXECUTION FOR INPUT AND OUTPUT FILE NAMES
C
C  CALLING PROGRAM: MAINBN
C
C  SUBROUTINE:
C
C  OUTPUT: (C*120) DSNIN   = INPUT FILE NAME
CX  OUTPUT: (C*120) DSNPAP  = TEXT OUTPUT FILE NAME
C  OUTPUT: (C*80)  PASSDIR = DIRECTORY NAME FOR PASSING FILES
C  OUTPUT: (C*120) DSNXRT  = FIRST PART OF CROSS REFERENCE FILE NAME
CX  OUTPUT: (L*4)   LPAPER  = .TRUE. => TEXT OUTPUT SELECTED
CX	                     .FALSE.=> NOT SELECTED
C  OUTPUT: (C*3)   REP     = 'YES' => CANCEL SELECTED FROM INPUT SCREEN
C			     'NO'  => NOT SELECTED
C  OUTPUT: (L*4)   LBTSEL  = .TRUE. => 'RUN IN BATCH' SELECTED
C                            .FALSE. => 'RUN NOW' SELECTED
C  OUTPUT: (C*80)  USERID  = SOURCE DATA USER ID (CENTRAL ADAS OR USER)
C                            FOR USE IN XXUID
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    09TH AUGUST 1996
C
C VERSION: 1.1                          DATE: 09-08-96
C MODIFIED: WILLIAM OSBORN
C     - FIRST VERSION.
C
C VERSION: 1.2                          DATE: 22-10-96
C MODIFIED: WILLIAM OSBORN
C     - ADDED USERID PARAMETER AND PIPE READ
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEIN
      PARAMETER ( PIPEIN=5          )
C
      CHARACTER   DSNIN*120, PASSDIR*80, DSNXRT*120, REP*3, USERID*80
      LOGICAL     LBTSEL
      INTEGER     IPEND
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C READ IN DATA FROM STANDARD INPUT (IDL OR INFO FILE)
C-----------------------------------------------------------------------
C

 10   READ(PIPEIN,'(A)')REP
      IF(REP.EQ.'NO')THEN

         READ(PIPEIN,'(A)')DSNIN
         READ(PIPEIN,'(A)')DSNXRT
         READ(PIPEIN,'(A)')USERID

         READ(PIPEIN, *) IPEND
         IF(IPEND.NE.1)THEN
C            READ(PIPEIN,'(A)')DSNPAP
            READ(PIPEIN,'(A)')PASSDIR
C            READ(PIPEIN,*)LOGIC
C            IF(LOGIC.EQ.1)THEN
C               LPAPER=.TRUE.
C            ELSE
C               LPAPER=.FALSE.
C            ENDIF
         ELSE
            GOTO 10
         ENDIF

      ENDIF
C IPEND = 2 FOR BATCH EXECUTION
      IF(IPEND.EQ.2)THEN
         LBTSEL=.TRUE.
      ELSE
         LBTSEL=.FALSE.
      ENDIF

 99   CONTINUE
      RETURN
      END

