CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/bddrsm2.for,v 1.1 2004/07/06 11:37:39 whitefor Exp $ Date $Date: 2004/07/06 11:37:39 $
CX
       SUBROUTINE BDDRSM2( NDREP , NDT  ,
     &                     MAXTM  , IREPMAX, IREP , DRMF , DRMS ,
     &                     EIJN   , PWTEMP    )
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C **************** FORTRAN 77 SUBROUTINE: BDDRSUM **********************
C
C    VERSION   2.0
C
C    AUTHOR:   WILLIAM J. DICKSON
C              JET JOINT UNDERTAKING
C
C    DATE:     14TH DECEMBER 1992
C
C
C    PURPOSE:
C    --------
C       TO SUM BADNELL DIELECTRONIC RATE COEFFICIENT DATA OVER THE
C       REPRESENTATIVE SET TO GIVE ZERO DENSITY TOTAL AND
C       RADIATED POWER FROM SATELLITE LINES
C
C       CALLING PROGRAM BDMNCLT
C
C
C    INPUT:
C    ------
C     NDREP         -  MAXIMUM NUMBER OF REPRESENTATIVE LEVELS
C     NDT           -  MAXIMUM NUMBER OF TEMPERATURES
C     DRMF(,)       -  BADNELL DIELECTRONIC DATA
C                          1ST INDEX   -  REPRESENTATIVE LEVEL
C                          2ND INDEX   -  TEMPERATURE
C     NBT           -  NO OF TEMPERATURES
C     IREPMAX       -  NO OF REPRESENTATIVE LEVELS
C     IREP          -  SET OF REPRESENTATIVE LEVELS
C     EIJN()        -  SAT. ENERGY AS A FUNCTION OF REPRESENTATIVE LEVEL
C                         UNITS - KELVIN
C    OUTPUT:
C    -------
C     DRMS()        -  SUMMED DR RATE COEFFICIENTS
C                          1ST INDEX    -  TEMPERATURE
C
C     PWTEMP()      -  SAT. RADIATED POWER (UNITS ERG S-1 CM3)
C                          1ST INDEX    -  TEMPERATURE
C
C
C    UPDATE:
C    -------
C     15/12/92   REVISED ALGOROTHM HAS BETTER AGREEMENT WITH
C                INTERNAL SUM CALCULATED BY MAINCL
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 22-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED. NO CHANGES.
C
C-----------------------------------------------------------------------
C-------------------------------------------------------------------
C
       PARAMETER ( DMIN = 1.00D-70 )
C
       DIMENSION  DRMS(NDT)   ,  DRMF(NDREP,NDT) , IREP(NDREP)
       DIMENSION  EIJN(NDREP) ,  PWTEMP(NDT)
C
       DO  90 IT = 1,MAXTM
         DRMS(IT)  = 0.0D+0
         PWTEMP(IT)  = 0.0D+0
   90  CONTINUE
C
C      WRITE(6,1100) NDREP, NDT, MAXTM, IREPMAX
C      DO IBREP = 1,IREPMAX
C         WRITE(6,1102) IREP(IBREP), ( DRMF(IBREP,IT) , IT = 1,MAXTM )
C      ENDDO
C
       DO 100 IT = 1,MAXTM
C
         DRMS(IT)    = DRMF(1,IT)
         PWTEMP(IT)  = DRMF(1,IT) * EIJN(1)
C
C        IF( IT .EQ. 5) WRITE(6,1108)
C
         DO 120 IN = 2,IREPMAX
            N1 =  IREP(IN)
            N0 =  IREP(IN-1)
            V  =  DFLOAT(N0)
            V1 =  DFLOAT(N1)
            Y1 =  DLOG10( DMAX1( DRMF(IN   , IT) , DMIN ) )
            Y0 =  DLOG10( DMAX1( DRMF(IN-1 , IT) , DMIN ) )
C
            PW1  = DRMF(IN ,IT)   *  EIJN(IN)
            PW   = DRMF(IN-1 ,IT) *  EIJN(IN-1)
            YP1  = DLOG10( DMAX1 ( PW1,  DMIN ) )
            YP0  = DLOG10( DMAX1 ( PW ,  DMIN ) )
C
           DO 130 NREP = N0+1, N1
C
              Y =  Y1 - (N1 - NREP)/(N1-N0) * (Y1 - Y0)
              DRMS(IT)  =  DRMS(IT) + 10 ** Y
C
              YP =  YP1 - (N1 - NREP)/(N1-N0) * (YP1 - YP0)
              PWTEMP(IT) =  PWTEMP(IT) + 10 ** YP
C
  130      CONTINUE
C
  120    CONTINUE
         PWTEMP(IT) = PWTEMP(IT) * 1.3804D-16
  100  CONTINUE
C
C      WRITE(6,1104) ( DRMS(IT) , IT = 1,MAXTM )
C
 1100  FORMAT(1H ,'NDREP=',I3,3X,'NDT=',I3,3X,'MAXTM=',I3,3X,
     &            'IREPMAX=',I3)
 1102  FORMAT(1H , I5, 1P, 12D10.2)
 1104  FORMAT(1H ,'SUM  ', 1P, 12D10.2)
 1108  FORMAT(1H ,'  IN   N1   N0   DRMS       ALFN    ALFN1      PWTEMP
     &      PW      PW1   ')
 1110  FORMAT(1H ,3I5,1P,6D10.2)
C
       RETURN
       END
