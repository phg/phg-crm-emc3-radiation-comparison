CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/dielcl.for,v 1.2 2004/07/06 13:32:17 whitefor Exp $ Date $Date: 2004/07/06 13:32:17 $
CX
       SUBROUTINE DIELCL(Z,EIJ,F,T,COR,JCOR,N,DEF,AD,L,CPT)             
       IMPLICIT REAL*8 (A-H,O-Z)                                        
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 SUBROUTINE: DIELCL  ************************
C
C  VERSION:  1.0
C
C  PURPOSE:  THIS SUBROUTINE IS NOT YET PROPERLY DOCUMENTED
C
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 22-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED. NO CHANGES.
C
C VERSION: 1.2                          DATE: 09-09-96
C MODIFIED: WILLIAM OSBORN
C               - COMMENTED-OUT LINE 'CPT=CPTS(L+1)' WHICH WAS GIVING
C                 A COMPILER WARNING AND IS NOT YET NEEDED.
C-----------------------------------------------------------------------

       DIMENSION THETA(1000),COR(20),CPTS(1000)                         
C      WRITE(6,103)Z,EIJ,F,T,N,DEF
C 103  FORMAT(1H ,1P,4D12.4,I5,1P,D12.4)
C      WRITE(6,104)(COR(J),J=1,JCOR)
C 104  FORMAT(10F10.5)
       AD=0.0                                                           
       ZZ=Z*Z                                                           
       Z1=(Z+1.0)*(Z+1.0)                                               
       EN=N                                                             
       EN=EN-DEF                                                        
       T1=Z1*EIJ/ZZ-1.0/(EN*EN)                                         
       Z2=Z1*EIJ/ZZ                                                     
C  X:SECT REQUIREMENT:T4=4.68145D-33*Z2*Z2*F/T1                         
       T4=0.51013*Z2*Z2*F                                               
       E=T1*ZZ                                                          
       IF(T1)4,4,1                                                      
    1  A=DSQRT(T1)                                                      
       CALL BF(THETA,N,A)                                               
       B=0.777135E-6*ZZ*ZZ                                              
       J1=JCOR                                                          
       IF(N-JCOR)5,6,6                                                  
    5  J1=N                                                             
    6  DO 2 J=1,J1                                                      
    2  THETA(J)=COR(J)*THETA(J)                                         
C      WRITE(6,102)(THETA(J),J=1,N)                                     
C 102  FORMAT(1P,10D10.2)                                               
       C3=0.0                                                           
       DO 3 J=1,N                                                       
       TJ=J                                                             
       TH=THETA(J)*TJ*EN*EN                                             
       TJ=TJ+TJ-1.0                                                     
       T3=TJ*TH/(B*TJ*(1.0+T)+TH)                                       
C      WRITE(6,102)T3                                                   
       THETA(J)=TH/(B*TJ*(1.0+T))                                       
C  X-SECT REQUIREMENT:CPTS(J)=T3*T4                                     
       CPTS(J)=T3*T4                                                    
    3  C3=C3+T3                                                         
C  X-SECT REQUIREMENT:WRITE(6,100)(CPTS(J),J=1,N)                       
       Z2=Z1*EIJ/ZZ                                                     
       AD=0.51013*Z2*Z2*F*C3                                            
C      WRITE(6,102)C3,AD                                                
C      CPT=CPTS(L+1)                                                    
C  X-XECT REQUIREMENT:ADC=C3*T4                                         
C  X-SECT REQUIREMENT:WRITE(6,101)ADC                                   
       IF(N-10)7,8,8                                                    
    7  NP=N                                                             
       GO TO 9                                                          
    8  NP=10                                                            
    9  CONTINUE                                                         
C      WRITE(6,100)(THETA(J),J=1,NP)                                    
C 100  FORMAT(1P,10E12.2)                                               
C 101  FORMAT(1P,E12.2//)                                               
    4  RETURN                                                           
      END                                                               
