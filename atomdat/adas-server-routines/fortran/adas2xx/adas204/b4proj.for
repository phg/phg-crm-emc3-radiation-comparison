CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/b4proj.for,v 1.8 2013/01/22 15:44:46 mog Exp $ Date $Date: 2013/01/22 15:44:46 $
CX
       SUBROUTINE B4PROJ ( W1     , JTE    , JDENS  ,
     &                     NMIN   , NMAX   , NREP   , IMAX   ,
     &                     NRESU  , ARED   , RHS    , CIONPT ,
     &                     TRECPT , DRECPT , RRECPT , XRECPT ,
     &                     NPRT   , NMAXI  , NREPI  ,
     &                     IMAXI  , AREDI  , RHSI   ,
     &                     CIONRI , CIONRA , RHSIRC ,
     &                     IEDMAT , IECION , IETREC ,
     &                     IEDREC , IERREC , IEXREC ,
     &                     IERSYS , SSYSWT , IPRTCAL,
     &                     DVEC   , ACNST  , A1CNST , 
     &                     OPEN18 , OPEN19 , OPEN20 ,
     &                     PRB
     &                   )
       IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 SUBROUTINE: B4PROJ  ************************
C
C  VERSION:  1.1
C
C  PURPOSE: SUBROUTINE TO ESTABLISH THE PROJECTED INFLUENCE OF HIGH
C           N-SHELLS IN THE BUNDLE-N COLLISIONAL DIELECTRONIC MODEL
C           ON LOW N-SHELLS
C
C  BOTH THE RECOMBINATION AND IONISATION PATHWAYS THROUGH THE HIGH
C  LEVELS ARE TAKEN INTO ACCOUNT AS WELL AS THE INDIRECT COUPLINGS OF
C  LOW RESOLVED LEVELS VIA THE HIGH BUNDLE-N LEVELS.
C
C  THE SUBROUTINE IS USED AS AN ARBITRARY CALL FROM WITHIN THE
C  CONVENTIONAL BNDLEN ROUTINE FOLLOWING ESTABLISHMENT OF THE
C  CONDENSED COLLISIONAL-DIELECTRONIC MATRIX AND RIGHT-HAND SIDE
C
C  THE ROUTINE PROVIDES TABULAR OUTPUT AND FOR THE MOMENT PREPARES A
C  PASSING FILE FOR FURTHER PROCESSING IN THE A-D-A-S STRUCTURE
C
C
C  INPUT:
C      W1       = GROUND STATE RADIATION DILUTION FACTOR
C      JTE      = TEMPERATURE INDEX
C      JDENS    = DENSITY INDEX
C      NMIN     = LOWEST N-SHELL
C      NMAX     = HIGHEST N-SHELL
C      NREP(I)  = SET OF REPRESENTATIVE LEVELS
C      IMAX     = NUMBER OF REPRESENTATIVE LEVELS
C      NRESU    = UPPER LIMIT OF PROJECTED N-SHELLS
C
C      ARED(I,J)= CONDENSED COLLISONAL-DIELECTRONIC MATRIX (CN SOLUTION)
C                  (EXCLUDES AUTO-IONISATION RATES FOR LEVELS LE NRESU)
C      RHS(I)   = CONDENSED RIGHT-HAND-SIDE                (CN SOLUTION)
C                  (EXCLUDES AUTO-IONISATION RATES FOR LEVELS LE NRESU)
C      CIONPT(I)= COLLISIONAL IONISATION CONTRIBUTION TO ARED(I,I)
C      TRECPT(I)= THREE BODY   RECOMBINATION CONTRIBUTION TO RHS(I)
C      DRECPT(I)= DIELECTRONIC RECOMBINATION CONTRIBUTION TO RHS(I)
C      RRECPT(I)= RADIATIVE    RECOMBINATION CONTRIBUTION TO RHS(I)
C      XRECPT(I)= CHARGE EXCHANGE RECOMB.    CONTRIBUTION TO RHS(I)
C
C      NPRT     = NUMBER OF PARENT STATES
C      IMAXI
C      NMAXI
C      NREPI(I)   DATA FOR PROJECTION OF IONISATION VECTORS
C      AREDI(I,J)           SMALL (40X40) MATRIX , CN SOLUTION
C      RHSI(I)
C      RHSIRC(I)= RECOMBINATION CONTRIBUTION TO RHS
C      CIONRI   = DIRECT IONISATION DATA, PARENT RESOLVED
C      CIONRA   = AUTO-IONISATION DATA,   PARENT RESOLVED
C
C      SSYSWT   = SPIN SYSTEM WEIGHT
C      IPRTCAL  = INDEX OF PARENT FOR CALCULATION
C
C      DVEC(I)  = CONVERSION FACTOR FOR BN --> POPULATION
C      ACNST    = 1.03928D-13*Z*ATE*DSQRT(ATE)
C      A1CNST   = 6.60074D-24*DENS*(157890.0/TE)**1.5
C
C      PCION(I) = DIRECT IONISATION RATE FROM LOW LEVEL SET
C                         POPULATION REPRESENTATION
C      PRB      = RECOM/CASCADE/BREMS. POWER COEFFT.
C
C      OUTPUT - POPULATION REPRESENTATION (WRITTEN TO FILE CBNM.PASS)
C      --------------------------------------------------------------
C      PCRMAT(I,J) = PROJECTED INFLUENCE OF HIGH LEVELS ON LOW LEVEL SET
C      PCRL(I,J)   = DIRECT EXCIT/RADIATIVE COUPLING IN LOW LEVEL SET
C      PCIONRP(IPRT,I)  = PROJECTED IONISATION VECTOR (PARENT RESOLVED)
C      PCIONRI(IPRT,I)  = DIRECT IONISATION VECTOR FROM LOW LEVEL SET
C                                                     (PARENT RESOLVED)
C      PCQINRP(IPRT)    = INDIRECT PARENT CROSS COUPLING COEFFICIENT
C                                                     (PARENT RESOLVED)
C      PCRRHS(I)  = PROJECTED INFLUENCE OF HIGH LEVELS ON RHS
C      PTREC(I)   = DIRECT THREE BODY RECOMBINATION RATE
C      PDREC(I)   = DIRECT DIELECTRONIC RECOMBINATION RATE
C      PRREC(I)   = DIRECT RADIATIVE RECOMBINATION RATE
C      PXREC(I)   = DIRECT CX RECOMBINATION RATE
C      PRB        = RECOM/CASCADE/BREMS. POWER COEFFT.
C
C
C      OUPUT CONTROL CHARACTERS
C      ------------------------
C      IEDMAT   = 0 PCRL ADDED ONTO PCRMAT
C                 1 PCRL NOT ADDED ON
C      IECION   = 0 PCION  ADDED ONTO TO PCRMAT
C                   PCIONRI ADDED ONTO PCIONRP
C                 1 PCION NOT ADDED ON
C                   PCIONRI NOT ADDED ON
C      IETREC   = 0 PTREC ADDED ONTO PCRRHS
C                 1 PTREC NOT ADDED ON
C      IEDREC   = 0 PDREC ADDED ONTO PCRRHS
C                 1 PDREC NOT ADDED ON
C      IERREC   = 0 PRREC ADDED ONTO PCRRHS
C                 1 PRREC NOT ADDED ON
C      IEXREC   = 0 PXREC ADDED ONTO PCRRHS
C                 1 PXREC NOT ADDED ON
C      IERSYS   = 0 RECOMBINATION AND INDIRECT PARENT CROSS COUPLING
C                   RATES MULTIPLIED BY SPIN SYSTEM WEIGHT
C                 1 RECOMBINATION AND INDIRECT PARENT CROSS COUPLING
C                   RATES NOT MULTIPLIED BY SPIN SYSTEM WEIGHT
C
C
C  AUTHOR:  WILLIAM J. DICKSON, JET JOINT UNDERTAKING
C
C  DATE:  24TH AUGUST 1992
C
C  UPDATE:  30/01/97  HP SUMMERS - CHANGED NAME TO B4PROJ FROM V2CLDBN
C
C  UPDATE:  29/04/97  HP SUMMERS - ADJUSTMENTS DURING RE-VALIDATION
C
C  UPDATE:  09/07/97  HP SUMMERS - INTRODUCE IOUT18 AND IOUT19 FOR CBNM
C                                  AND CBNMPR PASSING FILES
C
C  UPDATE:  09/03/98  HP SUMMERS - RECOM/CASCADE/BREMS. POWER NOW
C                                  FETCHED AS INPUT PRB AND RELAYED TO
C                                  CBNM FILE.  CONVERTED TO EXPLICIT
C                                  TYPE DECLARATIONS.
C
C-----------------------------------------------------------------------
C
C  VERSION: 1.1						DATE: 05-03-98
C  MODIFIED: H.SUMMERS, L.HORTON, M.O'MULLANE
C		- BASED ON v2cldbc.for v1.2.
C  VERSION: 1.2						DATE: 09-03-98
C  MODIFIED: H.SUMMERS, L.HORTON, M.O'MULLANE
C		- RECOM/CASCADE/BREMS. POWER NOW FETCHED AS INPUT PRB AND
C		  RELAYED TO CBNM FILE.  CONVERTED TO EXPLICIT TYPE DECLARATIONS.
C  VERSION: 1.3						DATE: 08-12-98
C		HP SUMMERS & RICHARD MARTIN
C		- REMOVED TWO OBSOLETE WRITE STATEMENTS.
C
C  VERSION: 1.4						DATE: 03-08-2000
C		Martin O'Mullane
C		- Changed IPRT\N to IPRT/N to avoid \N being interpreted
C                 as an escape character.
C 
C  VERSION: 1.5						DATE: 11-09-2002
C		Martin O'Mullane
C		- Add open18 and open19 to give finer control over the
C                 output files; extra logic to use them added throughout.
C
C  VERSION: 1.6						DATE: 16-05-2007
C		Allan Whiteford
C		- Modified comments as part of subroutine documentation
C                 procedure.
C
C
C VERSION : 1.7
C DATE    : 14-10-2010
C MODIFIED: Martin O'Mullane
C   		- Dimension the input arguments nrep and nrepi to 
C                 ndim (was ndim+1) to be the same as the calling 
C                 routine.
C               - Initialise nrep and nrepi.
C
C VERSION : 1.8
C DATE    : 21-01-2013
C MODIFIED: Martin O'Mullane
C              - Increase dimension of representative n arrays to 100.
C
C-----------------------------------------------------------------------
       INTEGER   NDIM    , NLDIM    , NDMAX    , NDMET    ,
     &           IOUT20  , IOUT18   , IOUT19
C-----------------------------------------------------------------------
       PARAMETER ( NDIM   = 100 , NLDIM  = 30 , NDMAX = 550 , NDMET = 4)
       PARAMETER ( IOUT20 = 20 , IOUT18 = 18 , IOUT19 = 19 )
C-----------------------------------------------------------------------
       INTEGER   JTE     , JDENS    , NMIN     , NMAX     , IMAX
       INTEGER   NRESU   , NPRT     , NMAXI    , IMAXI
       INTEGER   IEDMAT  , IECION   , IETREC   , IEDREC   , IERREC
       INTEGER   IERSYS  , IPRTCAL
       INTEGER   IHI      , ILOW     , ILOWK
       INTEGER   ILOWJ   , K        , IHJ      , IHMAX    , IH
       INTEGER   IPRT    , ILMAXI   , IHMAXI   , IREP     , IUI
       INTEGER   ILI     , ITOTI    , IMINI    , ILMAX    , NPARNT
       INTEGER   NLEV    , NSHEL    , IEXREC   , IPASS    , I
       INTEGER   IU      , IL       , ITOT     , IMIN     , J
       INTEGER   NRESL
C-----------------------------------------------------------------------
       REAL*8    W1      , SSYSWT   , ACNST    , ACNST1   , PRB
       REAL*8    DETERM  , A1CNST   , PHSFAC   , RPHSFC
C-----------------------------------------------------------------------
       LOGICAL   OPEN18  , OPEN19   , OPEN20
C-----------------------------------------------------------------------
       CHARACTER SEQ*2       , REFMEM*8
       CHARACTER LSTRING*133 , STRNG2*64
       CHARACTER STRNG3*11   , STRNG4*85 , STRNG5*68 , STRNG6*68
C--------------------------------------------------- --------------------
       INTEGER   NREP(NDIM)    , NREPI(NDIM)    , IPOINTA(NDMAX)
C-----------------------------------------------------------------------
       REAL*8    ARED(NDIM,NDIM) , RHS(NDIM)        , DVEC(NDIM)
       REAL*8    AREDI(NDIM,NDIM), RHSI(NDIM)       , RHSIRC(NDIM)
       REAL*8    AEFF(NDIM,NDIM) , AREDEX(NDIM,NDIM), RHSEX(NDIM)
       REAL*8    FEX(NDIM,NDIM)  , REX(NDIM)
       REAL*8    CIONPT(NDIM)    , CIONRI(NDMET,NDIM)
       REAL*8    CIONRA(NDMET,NDIM) , CQINRP(NDMET)
       REAL*8    CIONRT(NDMET,NDIM) , CIONRP(NDMET,NDIM)
       REAL*8    TRECPT(NDIM)       , DRECPT(NDIM)
       REAL*8    RRECPT(NDIM)       , XRECPT(NDIM)
       REAL*8    AREDL(NDIM,NDIM), AREDH(NDIM,NDIM) , RHSL(NDIM)
       REAL*8    RHSH(NDIM)      , BREDL(NDIM,NDIM) , VEC(NDIM)
       REAL*8    AMAT(NDIM,NDIM) , RS(NDIM)         , CHMAT(NDIM,5)
       REAL*8    RH(NDIM)
       REAL*8    PCRMAT(NDIM,NDIM)  , PCRRHS(NDIM) , PCRL(NDIM,NDIM)
       REAL*8    PEXMAT(NLDIM,NLDIM), PEXRHS(NLDIM), PCIONRI(NDMET,NDIM)
       REAL*8    PCIONRA(NDMET,NDIM), PCQINRP(NDMET)
       REAL*8    PCION(NDIM)        , PCIONRP(NDMET,NDIM)
       REAL*8    PTREC(NDIM)        , PDREC(NDIM)  , PRREC(NDIM)
       REAL*8    PXREC(NDIM)
       REAL*8    TEST(NDIM,NDIM)    , ARED2(NDIM,NDIM), RHS2(NDIM)
C-----------------------------------------------------------------------
       CHARACTER SYMBA(30)*2
C-----------------------------------------------------------------------
       NAMELIST /SEQINF/ SEQ, REFMEM, NPARNT, NSHEL, NLEV

C-----------------------------------------------------------------------
       DATA LSTRING/'
     &
     &                '/
       DATA IPASS/0/
       DATA SYMBA/'H ','HE','LE','BE','B ','C ','N ','O ','F ','NE',
     &            'NA','MG','AL','SI','P ','S ','CL','AR','K ','CA',
     &            'SC','TI','V ','CR','MN','FE','CO','NI','CU','ZN'/
C-----------------------------------------------------------------------
       do i = 1, ndmax
          ipointa(i) = 0
       end do
       do i = imax+1,ndim
          NREP(i) =0
          NREPI(i)=0
       end do
C-----------------------------------------------------------------------
       IF(OPEN20)THEN
          write(iout20,*)'W1      : ',W1     
          write(iout20,*)'JTE     : ',JTE    
          write(iout20,*)'JDENS   : ',JDENS  
          write(iout20,*)'NMIN    : ',NMIN   
          write(iout20,*)'NMAX    : ',NMAX   
          write(iout20,*)'NREP    : ',NREP   
          write(iout20,*)'IMAX    : ',IMAX   
          write(iout20,*)'NRESU   : ',NRESU  
          write(iout20,*)'ARED    : ',ARED   
          write(iout20,*)'RHS     : ',RHS    
          write(iout20,*)'CIONPT  : ',CIONPT 
          write(iout20,*)'TRECPT  : ',TRECPT 
          write(iout20,*)'DRECPT  : ',DRECPT 
          write(iout20,*)'RRECPT  : ',RRECPT 
          write(iout20,*)'XRECPT  : ',XRECPT 
          write(iout20,*)'NPRT    : ',NPRT   
          write(iout20,*)'NMAXI   : ',NMAXI  
          write(iout20,*)'NREPI   : ',NREPI  
          write(iout20,*)'IMAXI   : ',IMAXI  
          write(iout20,*)'AREDI   : ',AREDI  
          write(iout20,*)'RHSI    : ',RHSI   
          write(iout20,*)'CIONRI  : ',CIONRI 
          write(iout20,*)'CIONRA  : ',CIONRA 
          write(iout20,*)'RHSIRC  : ',RHSIRC 
          write(iout20,*)'IEDMAT  : ',IEDMAT 
          write(iout20,*)'IECION  : ',IECION 
          write(iout20,*)'IETREC  : ',IETREC 
          write(iout20,*)'IEDREC  : ',IEDREC 
          write(iout20,*)'IERREC  : ',IERREC 
          write(iout20,*)'IEXREC  : ',IEXREC 
          write(iout20,*)'IERSYS  : ',IERSYS 
          write(iout20,*)'SSYSWT  : ',SSYSWT 
          write(iout20,*)'IPRTCAL : ',IPRTCAL
          write(iout20,*)'DVEC    : ',DVEC   
          write(iout20,*)'ACNST   : ',ACNST  
          write(iout20,*)'A1CNST  : ',A1CNST 
          write(iout20,*)'OPEN18  : ',OPEN18 
          write(iout20,*)'OPEN19  : ',OPEN19 
          write(iout20,*)'OPEN20  : ',OPEN20 
          write(iout20,*)'PRB     : ',PRB    
       ENDIF

       IF(OPEN20)THEN
           WRITE(IOUT20,2021) IECION,IEDREC,IERREC,IEXREC,IETREC
       ENDIF
C
       DO 10 I=1,IMAX
        IPOINTA(NREP(I))=I
   10  CONTINUE
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2007)
           WRITE(IOUT20,2008)(IPOINTA(I),I=1,18)
       ENDIF
C
C-----------------------------------------------------------------------
C  ZERO FINAL EXPANDED LOW LEVEL MATRIX AND RIGHT HAND SIDE IN
C  POPULATION REPRESENTATION
C-----------------------------------------------------------------------
       DO 101 I=1,NLDIM
        PEXRHS(I)=0.0D0
        DO 100 J=1,NLDIM
         PEXMAT(I,J)=0.0D0
  100   CONTINUE
  101  CONTINUE
C-----------------------------------------------------------------------
C  DEFINE PHASE FACTORS AND PROJECTION LIMITS
C-----------------------------------------------------------------------
C
         PHSFAC = 1.0
         RPHSFC = 0.0
         NRESL  = NMIN
C
         IL=0
   12    IL=IL+1
         IF(IL.LT.IMAX.AND.NRESL.NE.NREP(IL))GO TO 12
         IU=IL-1
   13    IU=IU+1
         IF(NRESU.NE.NREP(IU).AND.IU.LT.IMAX)GO TO 13
         IF(IU.EQ.IMAX)THEN
C
             IF(OPEN20)THEN
                 WRITE(IOUT20,2001)
             ENDIF
C
                 GO TO 190
         ENDIF
         IMIN=IPOINTA(NRESL)
         ITOT=IMAX-IMIN+1
         ILMAX=IU-IL+1
C
         IF(OPEN20)THEN
             WRITE(IOUT20,2007)
             WRITE(IOUT20,2009) JTE, JDENS,NRESL,NRESU,IL,IU,ILMAX,
     &                    IMIN,IMAX,ITOT,SSYSWT
         ENDIF
C
C-----------------------------------------------------------------------
C  ELIMINATE COUPLINGS BETWEEN N-SHELLS BELOW NRESL AND
C  ALL HIGHER N-SHELLS UP TO IMAX
C-----------------------------------------------------------------------
C
         IF(OPEN20)THEN
             WRITE(IOUT20,2007)
             DO 15 I=1,IMAX
              WRITE(IOUT20,2006)(ARED(I,J),J=1,IMAX),RHS(I)
   15        CONTINUE
         ENDIF
C
         DO 30 I=1,IMAX
          RHSH(I)=RHS(I)
          DO 20 J=1,IMAX
           AREDH(I,J)=ARED(I,J)
   20     CONTINUE
   30    CONTINUE
         IF(IMIN.GT.1)THEN
             DO 35 K=1,IMIN-1
              DO 33 I=IMIN,IMAX
               AREDH(I,I) = AREDH(I,I)+ARED(K,I)
               RHSH(I)    = RHSH(I) - ARED(K,I) + ARED(I,K)
               AREDH(K,I) = 0.0D0
               AREDH(K,K) = AREDH(K,K)+ARED(I,K)
               RHSH(K)    = RHSH(K) -ARED(I,K) + ARED(K,I)
               AREDH(I,K) = 0.0D0
   33         CONTINUE
   35        CONTINUE
         ENDIF
C
         IF(OPEN20)THEN
             WRITE(IOUT20,2007)
             DO 37 I=1,IMAX
              WRITE(IOUT20,2006)(AREDH(I,J),J=1,IMAX),RHSH(I)
   37        CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C  ELIMINATE COUPLINGS BETWEEN N-SHELLS BELONGING TO LOW LEVELS.
C
C  ELIMINATE IONISATION, DIELECTRONIC, RADIATIVE AND
C  CHARGE EXCHANGE RECOMBINATION PARTS BELONGING TO THE LOW LEVELS.
C
C  CN-REPRESENTATION.
C-----------------------------------------------------------------------
       DO 50 I=IL,IU
        DO 40 J=IL,IU
         IF(J .NE. I)THEN
            AREDH(I,I) = AREDH(I,I) + ARED(J,I)
            RHSH(I)    = RHSH(I) - ARED(J,I) + ARED(I,J)
            AREDH(J,I) = 0.0D0
         ENDIF
   40   CONTINUE
        AREDH(I,I)=AREDH(I,I)-CIONPT(I)
        RHSH(I)=RHSH(I)-DRECPT(I)
        RHSH(I)=RHSH(I)-RRECPT(I)
        RHSH(I)=RHSH(I)-XRECPT(I)
   50  CONTINUE
C-----------------------------------------------------------------------
C  ADJUST TRANSITION RATES TO IMIN FOR PHASE SPACE OCCUPANCY FACTOR
C-----------------------------------------------------------------------
       DO 51 J=IMIN+1,IMAX
         AREDH(1,J) = PHSFAC  * AREDH(1,J)
         AREDH(J,J) = AREDH(J,J) - RPHSFC*AREDH(1,J)
         RHSH(1)    = RHSH(1) - RPHSFC*AREDH(1,J)
         RHSH(J)    = RHSH(J) + RPHSFC*AREDH(1,J)
   51  CONTINUE
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2007)
           DO 52 I=1,IMAX
            WRITE(IOUT20,2006) (AREDH(I,J),J=1,IMAX),RHSH(I)
   52      CONTINUE
       ENDIF
C
C-----------------------------------------------------------------------
C  OBTAIN PARTS OF ARED,RHS EQUATIONS NOT INCLUDED IN THE PROJECTION
C
C  ENSURE LOW LEVEL DIRECT COUPLINGS DO NOT CONTAIN IONISATION COMPONENT
C-----------------------------------------------------------------------
       DO 70 I=1,IMAX
         RHSL(I) = RHS(I) - RHSH(I)
         DO 60 J=1,IMAX
             AREDL(I,J) = ARED(I,J)-AREDH(I,J)
   60    CONTINUE
         AREDL(I,I) = AREDL(I,I)-CIONPT(I)
   70  CONTINUE
C
       IF(OPEN20)THEN
          WRITE(IOUT20,2007)
           DO 72 I=1,IMAX
            WRITE(IOUT20,2006)(AREDL(I,J),J=1,IMAX),RHSL(I)
   72      CONTINUE
       ENDIF
C
C-----------------------------------------------------------------------
C  FIRST INVERSION - HIGH LEVEL MATRIX ONLY AND SOLVE N-SHELL EQUATIONS.
C                    TRANSCRIBE RELEVANT ROWS AND COLUMNS INTO AMAT & RS
C-----------------------------------------------------------------------
       DO 76 I=IMIN,IMAX
        RS(I-IMIN+1) = RHSH(I)
        DO 74 J=IMIN,IMAX
         AMAT(I-IMIN+1,J-IMIN+1) = AREDH(I,J)
   74   CONTINUE
   76  CONTINUE
       CALL B4MATV(AMAT,ITOT,RS,1,DETERM)
       DO 90 I=1,ILMAX
        VEC(I)=0.0D0
        DO 80 J=1,ILMAX
         BREDL(I,J)=AMAT(I,J)
   80   CONTINUE
   90  CONTINUE
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2007)
           DO 92 I=1,ITOT
             DO  K=1,ITOT
               TEST(I,K) = 0.0D+0
               DO J=1,ITOT
                TEST(I,K) = TEST(I,K) + AMAT(I,J) * AREDH(J,K)
               ENDDO
             ENDDO
             WRITE(IOUT20,2006)(TEST(I,K),K=1,ITOT)
   92      CONTINUE
       ENDIF
C
C
C-----------------------------------------------------------------------
C  SECOND INVERSION - HIGH LEVEL CONTRIBUTIONS TO LOW N-SHELLS.
C-----------------------------------------------------------------------
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2007)
           DO 94 I=1,ILMAX
            WRITE(IOUT20,2006)(BREDL(I,J),J=1,ILMAX),VEC(I)
   94      CONTINUE
       ENDIF
C
       CALL B4MATV(BREDL,ILMAX,VEC,1,DETERM)
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2007)
       ENDIF
C
       DO 96 I=1,ILMAX
        VEC(I)=0.0D0
        DO 95 J=1,ILMAX
         VEC(I)=VEC(I)+BREDL(I,J)*RS(J)
   95   CONTINUE
C
        IF(OPEN20)THEN
            WRITE(IOUT20,2006)(BREDL(I,J),J=1,ILMAX),VEC(I)
        ENDIF
C
   96  CONTINUE
C
C---------------------------------------------------------------------
C      SECTION OF CODE TO CALCULATE INDIRECT COUPLINGS
C               WJD    3RD JULY 1992
C---------------------------------------------------------------------
C      A) CALCULATE EXCITATION CONTRIBUTIONS TO HIGH LEVELS
C         1.  TRANSCRIBE HIGH LEVEL MATRIX TO ARRAYS
C         2.  INVERT HIGH LEVEL MATRIX
C         3.  MULTIPLY EXCITATION RATES BY INVERTED MATRIX
C      ----------------------------------------------------
       IHMAX  = IMAX  - IU
       DO I = 1,IHMAX
         RHSEX(I) = RHS(IU + I)
         DO J = 1,IHMAX
           AREDEX(I,J) = ARED(IU+I,IU+J)
         ENDDO
       ENDDO
C
       CALL B4MATV( AREDEX, IHMAX, RHSEX, 1, DETERM )
C
       DO IHJ = 1,IHMAX
          DO ILOW = 1,ILMAX
             FEX(IHJ,ILOW) = 0.0D+0
             DO IHI = 1,IHMAX
                FEX(IHJ,ILOW) =  FEX(IHJ,ILOW)  -
     &                           AREDEX(IHJ,IHI) * ARED(IU+IHI,ILOW)
             ENDDO
          ENDDO
       ENDDO
C      -------------------------------
C      B) CALCULATE INDIRECT COUPLINGS
C      -------------------------------
       IF (OPEN20) then
          WRITE(IOUT20,2007)
       endif
       DO ILOWJ = 1,ILMAX
          DO ILOWK = 1,ILMAX
C            AEFF(ILOWJ,ILOWK) = ARED(ILOWJ,ILOWK)
             AEFF(ILOWJ,ILOWK) = AREDH(ILOWJ,ILOWK)
             DO IH = 1,IHMAX
                AEFF(ILOWJ,ILOWK) = AEFF(ILOWJ,ILOWK) +
     &                            ARED(ILOWJ,IU+IH) * FEX(IH,ILOWK)
             ENDDO
          ENDDO
          IF (OPEN20) WRITE(IOUT20,2006)(AEFF(ILOWJ,J),J=1,ILMAX)
       ENDDO
C
C---------------------------------------------------------------------
C      SECTION OF CODE TO CONDENSE IONISATION VECTORS
C               WJD    3RD JULY 1992
C---------------------------------------------------------------------
         ILI = 0
  212    ILI = ILI+1
         IF(ILI .LT. IMAXI .AND. NRESL .NE. NREPI(ILI) )GO TO 212
         IUI = ILI-1
  213    IUI = IUI+1
         IF(NRESU .NE. NREPI(IUI) .AND. IUI.LT. IMAXI)GO TO 213
         IF(IUI .EQ. IMAXI)THEN
C
             IF(OPEN20)THEN
                 WRITE(IOUT20,2001)
             ENDIF
C
                 GO TO 190
         ENDIF
         IMINI  = IPOINTA(NRESL)
         ITOTI  = IMAXI-IMINI+1
         ILMAXI = IUI-ILI+1
C
         IF(OPEN20) WRITE(IOUT20,2009)  JTE,  JDENS, NRESL, NRESU, ILI,
     &   	IUI,ILMAXI,IMINI, IMAXI, ITOTI, SSYSWT
C      ---------------------------------------------------------------
C      A) REMOVE DIRECT IONISATION COUPLINGS FROM IONISATION VECTORS
C      ---------------------------------------------------------------
       DO IPRT = 1,NPRT
C
          DO IREP = 1,IMAXI
C
             CIONRT(IPRT,IREP) = 0.0D+0
             IF(IREP .LE. IUI ) THEN
               CIONRT(IPRT,IREP) = CIONRA(IPRT,IREP)
             ELSE
               CIONRT(IPRT,IREP) = CIONRA(IPRT,IREP)+CIONRI(IPRT,IREP)
             ENDIF
C
          ENDDO
C
       ENDDO
C
C      ----------------------------------------------------
C      B) CALCULATE EXCITATION CONTRIBUTIONS TO HIGH LEVELS
C         1.  TRANSCRIBE HIGH LEVEL MATRIX TO ARRAYS
C         2.  INVERT HIGH LEVEL MATRIX
C         3.  MULTIPLY EXCITATION RATES BY INVERTED MATRIX
C      ----------------------------------------------------
       IHMAXI = IMAXI - IUI
       DO I = 1,IHMAXI
         RHSEX(I) = RHSI(IUI + I)
         DO J = 1,IHMAXI
           AREDEX(I,J) = AREDI(IUI+I,IUI+J)
         ENDDO
       ENDDO
	 IF(OPEN20) THEN
         DO 315 I=1,IMAXI
           WRITE(IOUT20,2006) (AREDEX(I,J),J=1,10),RHSEX(I)
  315    CONTINUE
  	 ENDIF
C
       CALL B4MATV( AREDEX, IHMAXI, RHSEX, 1, DETERM )
C
       DO IHJ = 1,IHMAXI
          DO ILOW = 1,ILMAXI
             FEX(IHJ,ILOW) = 0.0D+0
             DO IHI = 1,IHMAXI
                FEX(IHJ,ILOW) =  FEX(IHJ,ILOW)  -
     &                           AREDEX(IHJ,IHI) * AREDI(IUI+IHI,ILOW)
             ENDDO
          ENDDO
       ENDDO
C
       DO IHJ = 1,IHMAXI
             REX(IHJ) = 0.0D+0
             DO IHI = 1,IHMAXI
                REX(IHJ) =  REX(IHJ) +
     &                           AREDEX(IHJ,IHI) * RHSIRC(IUI+IHI)
             ENDDO
       ENDDO
C
C      ------------------------------
C      C) CONDENSE IONISATION VECTORS
C      ------------------------------
       DO IPRT = 1,NPRT
          DO ILOW = 1,ILMAXI
             CIONRP(IPRT,ILOW) =   CIONRT(IPRT,ILOW)
             DO IH = 1,IHMAXI
                CIONRP(IPRT,ILOW) = CIONRP(IPRT,ILOW) +
     &                         CIONRT(IPRT,IUI+IH) * FEX(IH,ILOW)
             ENDDO
          ENDDO
       ENDDO
C
C      ------------------------------------
C      D) CALCULATE INDIRECT CROSS COUPLING
C      ------------------------------------
       DO IPRT = 1,NPRT
          CQINRP(IPRT) =   0.0D+0
          IF( IPRT .LT. IPRTCAL) THEN
            DO IH = 1,IHMAXI
                CQINRP(IPRT) = CQINRP(IPRT) +
     &                         CIONRT(IPRT,IUI+IH) * REX(IH)
            ENDDO
          ENDIF
       ENDDO
C
	 IF (OPEN20) THEN
         DO IH = 1,IHMAXI
          WRITE(IOUT20,3332)
          WRITE(IOUT20,3333) (CIONRT(IPRT,IUI+IH), IPRT = 1,NPRT) ,
     &                        REX(IH)
          WRITE(IOUT20,3332)
         ENDDO
 3332    FORMAT(1H )
 3333    FORMAT(1H ,1P,8D14.3)
	 ENDIF
C
C-----------------------------------------------------------------------
C  CONVERT TO POPULATION REPRESENTATION OF THE PROJECTION MATRIX AND
C  RIGHT HAND SIDE
C-----------------------------------------------------------------------
       DO 98 I=1,ILMAX
C
         PCRRHS(I)=0.0D0
         DO 97 J=1,ILMAX
            PCRMAT(I,J) = ACNST * BREDL(I,J) / (A1CNST*DVEC(J+IL-1))
            PCRL(I,J)   = ACNST * AREDL(I,J) / (A1CNST*DVEC(J+IL-1))
            PCRRHS(I)   = PCRRHS(I) + BREDL(I,J) * (RS(J)+1.0D0)
   97   CONTINUE
        PCRRHS(I) = ACNST * PCRRHS(I)
        PCION(I)  = ACNST * CIONPT(I+IL-1)/(A1CNST*DVEC(I+IL-1))
        DO IPRT = 1,NPRT
            PCIONRI(IPRT,I) = ACNST*CIONRI(IPRT,I+IL-1) /
     &                                            (A1CNST*DVEC(I+IL-1))
            PCIONRA(IPRT,I) = ACNST*CIONRA(IPRT,I+IL-1) /
     &                                            (A1CNST*DVEC(I+IL-1))
            PCIONRP(IPRT,I) = ACNST*CIONRP(IPRT,I+IL-1)  /
     &                                            (A1CNST*DVEC(I+IL-1))
        ENDDO
        PTREC(I)  = ACNST * TRECPT(I+IL-1)
        PDREC(I)  = ACNST * DRECPT(I+IL-1)
        PRREC(I)  = ACNST * RRECPT(I+IL-1)
        PXREC(I)  = ACNST * XRECPT(I+IL-1)
C
C      ----------------------------------------------------------------
C      ADD AUTO-IONISATION TRANSITION PROBABILITIES ONTO PROJECTED DATA
C      ----------------------------------------------------------------
C
       DO IPRT = 1,NPRT
            PCRMAT(I,I) = PCRMAT(I,I) + PCIONRA(IPRT,I)
       ENDDO
C
C      -----------------------------------------------------------------
C      OPTIONALLY ADD DIRECT COUPLINGS BACK ONTO PROJECTED DATA
C      AND MULTIPLY BY SPIN SYSTEM WEIGHT
C      -----------------------------------------------------------------
C
        IF(IEDMAT .EQ. 0) THEN
          DO J = 1, ILMAX
            PCRMAT(I,J) = PCRMAT(I,J) + PCRL(I,J)
          ENDDO
        ENDIF
        IF(IECION .EQ. 0) THEN
          PCRMAT(I,I) = PCRMAT(I,I) + PCION(I)
          DO IPRT = 1,NPRT
            PCIONRP(IPRT,I) = PCIONRP(IPRT,I) + PCIONRI(IPRT,I)
          ENDDO
        ENDIF
        IF(IETREC .EQ. 0) THEN
          PCRRHS(I) = PCRRHS(I) + PTREC(I)
        ENDIF
        IF(IEDREC .EQ. 0) THEN
          PCRRHS(I) = PCRRHS(I) + PDREC(I)
        ENDIF
        IF(IERREC .EQ. 0) THEN
          PCRRHS(I) = PCRRHS(I) + PRREC(I)
        ENDIF
        IF(IEXREC .EQ. 0) THEN
          PCRRHS(I) = PCRRHS(I) + PXREC(I)
        ENDIF
C
        IF(IERSYS .EQ. 0) THEN
          PCRRHS(I) = PCRRHS(I) * SSYSWT
          PTREC(I)  = PTREC(I)  * SSYSWT
          PDREC(I)  = PDREC(I)  * SSYSWT
          PRREC(I)  = PRREC(I)  * SSYSWT
          PXREC(I)  = PXREC(I)  * SSYSWT
        ENDIF
C
   98  CONTINUE
C-----------------------------------------------------------------------
C      CONVERT INDIRECT CROSS COUPLING RATE TO POPULATION
C      REPRESENTAION AND OPTIONALLY MULTIPLY BY SPIN SYSTEM WEIGHT
C-----------------------------------------------------------------------
        DO IPRT = 1,NPRT
            PCQINRP(IPRT) = ACNST * CQINRP(IPRT)
            IF(IERSYS .EQ. 0) THEN
               PCQINRP(IPRT) = PCQINRP(IPRT) * SSYSWT
            ENDIF
        ENDDO
C-----------------------------------------------------------------------
C      WRITE DATA TO CBNM.PASS
C-----------------------------------------------------------------------
       IF(OPEN20) WRITE(IOUT20,2009) JTE, JDENS, NRESL,NRESU,IL,IU,
     &                     ILMAX,IMIN,IMAX,ITOT,SSYSWT
       DO 99 I = 1,ILMAX
        IF(I .EQ. 1) THEN
          WRITE(STRNG3,2102) JDENS, JTE
        ELSE
          WRITE(STRNG3,2103)
        ENDIF
        WRITE(STRNG4,2104)  (PCRMAT(I,J),J=1,ILMAX)
        WRITE(STRNG5,2105)  (PCIONRP(IPRT,I), IPRT=1,NPRT)
        WRITE(STRNG6,2105)   PCRRHS(I)
        IF (open18) WRITE(IOUT18,2107)  STRNG3, NREP(I+IL-1), 
     &                                  STRNG4, STRNG5, STRNG6
   99  CONTINUE
C
       DO 102 I = 1,ILMAX
        IF(I .EQ. 1) THEN
          WRITE(STRNG3,2106)
        ELSE
          WRITE(STRNG3,2103)
        ENDIF
        WRITE(STRNG4,2104)  (PCRL(I,J),J=1,ILMAX)
        WRITE(STRNG5,2105)  (PCIONRI(IPRT,I), IPRT=1,NPRT)
        WRITE(STRNG6,2105)   PTREC(I), PDREC(I), PRREC(I), PXREC(I)
        IF (open18) WRITE(IOUT18,2107)  STRNG3, NREP(I+IL-1), 
     &                                  STRNG4, STRNG5, STRNG6
  102  CONTINUE
C-----------------------------------------------------------------------
C      COMBINE PRB AND INDIR. PARENT X-COUPLING ON STREAM 18 OUTPUT LINE
C-----------------------------------------------------------------------
       WRITE(STRNG3,2111)
       WRITE(STRNG4,2112)  PRB
       WRITE(STRNG5,2105) (PCQINRP(IPRT), IPRT=1,NPRT)
       if (open18) then
          WRITE(IOUT18,2110)  STRNG3,STRNG4,STRNG5
          WRITE(IOUT18,2108)
       endif
C-----------------------------------------------------------------------
C      WRITE DATA TO CLDLBN2.PASS
C-----------------------------------------------------------------------
       if (open19) then
          WRITE(IOUT19,2018)
          DO 103 I=1,ILMAX
           WRITE(STRNG2,2019) NREP(I+IL-1), (PCRMAT(I,J),J=1,ILMAX)
           WRITE(IOUT19,2020) STRNG2,PCRRHS(I),PTREC(I),PDREC(I),
     &                        PRREC(I), PXREC(I)
  103     CONTINUE
C
          WRITE(STRNG2,2032) (NREP(I+IL-1), I=1,ILMAX )
          WRITE(IOUT19,2030)  STRNG2, (NREP(I+IL-1), I=1,ILMAX)
          DO 104 IPRT = 1,NPRT
            WRITE(STRNG2,2019) IPRT,  (PCIONRP(IPRT,J),J=1,ILMAX)
            WRITE(IOUT19,2020) STRNG2, (PCIONRI(IPRT,J), J=1,ILMAX)
  104     CONTINUE
       endif
C
C-----------------------------------------------------------------------
C  CONTINUE CN-REPRESENTATION SOLUTION TO OBTAIN DEPENDENCE OF
C  HIGH NSHELLS ON THE LOW N-SHELLS AND RECOMBINATION EXPLICITLY.
C-----------------------------------------------------------------------
       DO 114 I=ILMAX+1,ITOT
        RH(I)=RS(I)
        DO 113 J=1,ILMAX
         CHMAT(I,J)=0.0D0
         DO 112 K=1,ILMAX
          CHMAT(I,J)=CHMAT(I,J)+AMAT(I,J)*BREDL(J,K)
  112    CONTINUE
         RH(I)=RH(I)-CHMAT(I,J)*RS(J)
  113   CONTINUE
  114  CONTINUE
C
C-----------------------------------------------
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2013)
       ENDIF

C       WRITE(IOUT20,2013)
C       WRITE(LSTRING,2014)(NREP(I),I=IL,IU)
C       LSTRING(29+14*2*ILMAX:31+14*2*ILMAX) ='RS'

       IF(OPEN20)THEN
         WRITE(IOUT20,2015)LSTRING
       ENDIF

       IF(OPEN20) WRITE(IOUT20,2015)LSTRING
       DO 115 I=ILMAX+1,ITOT

          IF(OPEN20)THEN
             WRITE(IOUT20,2016) I+IL-1, NREP(I+IL-1),
     &                (CHMAT(I,J),J=1,ILMAX),RH(I)
          ENDIF

          IF(OPEN20) WRITE(IOUT20,2016) I+IL-1,NREP(I+IL-1),
     &                (CHMAT(I,J),J=1,ILMAX),RH(I)
  115  CONTINUE
C-----------------------------------------------------------------------
C  FINAL CHECK INVERSION OF ORIGINAL MATRIX
C-----------------------------------------------------------------------
       DO 144 I=1,IMAX
        RHS2(I)=RHS(I)
        DO 143 J=1,IMAX
         ARED2(I,J)=ARED(I,J)
  143   CONTINUE
  144  CONTINUE
       CALL B4MATV(ARED2,IMAX,RHS2,1,DETERM)
C
       IF(OPEN20)THEN
           WRITE(IOUT20,2007)
           DO 145 I=1,IMAX
             WRITE(IOUT20,2006)(ARED2(I,J),J=1,IMAX),RHS2(I)
  145      CONTINUE
       ENDIF
C
  190  CONTINUE
C
  999  RETURN
C-----------------------------------------------------------------------
 2000  FORMAT(//,'FILE OPEN FAILURE IN CLDLBN2  -  CASE IGNORED!')
 2001  FORMAT(//,'NRESL OR NRESU INAPPROPRIATE  -  CASE IGNORED!')
 2002  FORMAT(//,1A30,15X,'Z0 = ',I2,5X,'Z1 = ',I2///,1A80)
 2003  FORMAT(1H ,1A80)
 2004  FORMAT(1A80)
 2005  FORMAT(I3,1A22,I4,3I3,3X,F7.4,1X,F7.4,1X,F7.4,1X,F7.4,1X,F7.4)
 2006  FORMAT(1H ,1P,11D12.4)
 2007  FORMAT(//)
 2008  FORMAT(1H ,6I12)
 2009  FORMAT(//,'JTE=',I3,3X,'JDENS=',I3,3X,
     & 'NRESL=',I3,3X,'NRESU=',I3,3X,'IL=',I3,3X,'IU=',I3,3X,
     & 'ILMAX=',I3,3X,'IMIN=',I3,3X,'IMAX=',I3,3X,'ITOT=',I3,3X,
     & 'SSYSWT=',F6.2)
 2010  FORMAT(//,'PRES- REPRESENTATION  : PRES COMPONENTS'/
     &        1H ,'INDX CODE                     SP SH PT      PEXMAT(I,
     &I)    PEXRHS(I)     PECION(I)     PEDREC(I)     PERREC(I)     PEXR
     &EC(I)')
 2011  FORMAT(1H ,I3,1A22,I4,3I3,3X,1P,6D14.5)
 2012  FORMAT(//,'PRES- REPRESENTATION  : PRES <---> PRES AND RHS')
 2013  FORMAT(//,'CN  - REPRESENTATION  : CN <--- CNLOW  DEPENDENCE')
 2014  FORMAT(1H ,'    I    N         NLOW=',5(I4,10X))
 2015  FORMAT(1A133)
 2016  FORMAT(1H ,2I5,10X,1P,5D14.5)
 2017  FORMAT(//,'DENS=',1P,D10.2,2X,'TE=',1P,D10.2,2X,'DENSP=',
     & 1P,D10.2,2X,'TP=',1P,D10.2,2X,'BMENER=',1P,D10.2,2X,'DENSH=',
     & 1P,D10.2,2X,'W1=',1P,D10.2)
 2018  FORMAT(//,'PN  - REPRESENTATION  :' /
     &        1H ,'   N    PN <---> PN   MATRIX
     &               PCRRHS(I)    PTREC(N)    PDREC(N)    PRREC(N)    PX
     &REC(N)')
 2019  FORMAT(1H ,I3,1P,5D12.4)
 2020  FORMAT(1A64,3X,1P,5D12.4)
 2021  FORMAT(//,'IECION = ',I2,5X,'IEDREC = ',I2,5X,'IERREC = ',I2,5X,
     & 'IEXREC = ',I2,5X,'IETREC = ',I2)
 2030  FORMAT(//,'IONISATION VECTORS  :' /
     &        1H ,'           PROJECTED DATA
     &                        DIRECT IONISATION DATA  ' /
     &        1H ,'IPRT/N ',1A64,3X,I3,4(9X,I3))
 2032  FORMAT(1H ,5(I3,9X))
C
 2102  FORMAT('ID=',I2,' IT=',I2)
 2103  FORMAT('           ')
 2104  FORMAT(1P,5D17.10)
 2105  FORMAT(1P,4D17.10)
 2106  FORMAT('      (DIR)')
 2107  FORMAT(1H ,1A11,' N=',I2,2X,1A85,2X,1A68,2X,1A68)
 2108  FORMAT(1H ,1PD12.4)
 2110  FORMAT(1H ,1A11,7X,1A85,2X,1A68)
 2111  FORMAT('      (PRB)')
 2112  FORMAT(1PD17.10,68X)
C--------------------------------------------------------------------------
      END
