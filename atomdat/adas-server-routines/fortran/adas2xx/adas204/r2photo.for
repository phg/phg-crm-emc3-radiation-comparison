CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas204/r2photo.for,v 1.1 2004/07/06 14:40:39 whitefor Exp $ Date $Date: 2004/07/06 14:40:39 $
CX
       SUBROUTINE R2PHOTO(ATE,EN,RREC,QRREC)                            
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C  *********  FORTRAN 77 SUBROUTINE *******************
C
C            NAME:  R2PHOTO
C
C         VERSION:  2.0
C
C  PREVIOUS NAMES:  JETSHP.NMAINCH(RPHOTO)  (H.P. SUMMERS)
C
C          AUTHOR:  H.P. SUMMERS/ W.J. DICKSON
C
C            DATE:  19/10/93
C
C    PURPOSE:
C    --------
C    UPDATED VERSION OF RPHOTO TO ALLOW
C
C      (I)  RETURN OF ENERGY AVERAGED ELECTRON COOLING COEFFICIENT
C
C
C    INPUT:
C    ------
C    ATE         R*8      EQUAL TO  (157890.0 /TE) *Z*Z
C    EN          R*8      N SHELL
C
C
C    OUTPUT:
C    -------
C    RREC        R*8      ENERGY AVERAGED BOUND-FREE GAUNT FACTOR
C                          (= INT FROM 0 TO INFINITY OF GII*EXP(-X) DX)
C    QRREC       R*8      ENERGY AVERAGED BOUND-FREE GAUNT FACTOR FOR
C                         CALCULATION OF ELECTRON COOLING FUNCTION.
C                          ( = RREC REDUCED BY FACTOR OF E/HV,
C                                WHERE E  = ELECTRON ENERGY
C                                      HV = PHOTON ENERGY )
C
C   NOTES:
C   ------
C    TE IS IN KELVIN UNITS
C    X = E/KT
C
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 22-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED. NO CHANGES.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
       DIMENSION XA(6),WA(6)                                            
C
       DATA NX/6/                                                       
       DATA XA/0.2228466042D0,1.1889321017D0,2.9927363261D0,            
     &         5.7751435691D0,9.8374674184D0,15.9828739806D0/           
       DATA WA/4.58964673950D-1,4.17000830772D-1,1.13373382074D-1,      
     &         1.03991974531D-2,2.61017202815D-4,8.98547906430D-7/      
C
C-----------------------------------------------------------------------
C      START OF CALCULATION
C-----------------------------------------------------------------------
C
       T1=ATE/(EN*EN)                                                   
       SUM  = 0.0D0                                                     
       SUMQ = 0.0D0
       DO 20 I=1,NX                                                     
         X = XA(I)                                                      
         U = X/T1                                                       
         Q = X / (X + T1)
         SUM  = SUM+GBF(EN,U)*WA(I)                                     
         SUMQ = SUMQ + Q * GBF(EN,U)*WA(I)
   20  CONTINUE
       RREC  = SUM                                                      
       QRREC = SUMQ
C      WRITE(6,1000) ATE, EN, RREC, QRREC                               
C1000  FORMAT(1H ,'R2PHOTO:',1P,4D12.4)                                 
C
C-----------------------------------------------------------------------
C      END OF CALCULATION - RETURN
C-----------------------------------------------------------------------
C
       RETURN                                                           
      END                                                               
