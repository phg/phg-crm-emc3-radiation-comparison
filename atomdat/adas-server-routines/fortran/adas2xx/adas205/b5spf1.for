CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas205/b5spf1.for,v 1.7 2004/07/06 11:24:47 whitefor Exp $ Date $Date: 2004/07/06 11:24:47 $
CX
      SUBROUTINE B5SPF1( NDTEM  , TINE   , MAXT   , IFOUT  ,
     &                   LPEND  ,
     &                   LNEWPA , LPAPER , LCONT  , LPASS  ,
     &                   DSNPAP , DSNOUT , DSNPAS ,
     &                   LGPH   , ITSEL  , GTIT1
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B5SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS205
C
C  SUBROUTINE:
C
C  INPUT:  (I*4)   NDTEM    = PARAMETER = MAX. NO. OF TEMPERATURES
C                                         ALLOWED
CX INPUT:  (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
CX INPUT:  (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES
CX                            ( 1 -> 'NDTEM')
CX INPUT:  (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
CX                          = 2 => INPUT TEMPERATURES IN EV
CX                          = 3 => INPUT TEMPERATURES IN REDUCED FORM
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => OUTPUT OPTIONS CANCELLED.
C                             .FALSE. => PROCESS OUTPUT OPTIONS.
C
CX OUTPUT: (L*4)   LNEWPA   = .TRUE.  => NEW TEXT OUTPUT FILE OR
CX                                       REPLACEMENT OF EXISTING FILE
CX                                       REQUIRED.
CX                            .FALSE. => ALLOW APPEND ON EXISTING OPEN
CX                                       TEXT FILE.
C
CX OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT DATA TO TEXT OUTPUT
CX                                       FILE.
CX                            .FALSE. => NO OUTPUT OF CURRENT DATA TO
CX                                       TEXT OUTPUT FILE.
C  OUTPUT: (L*4)   LCONT    = .TRUE.  => OUTPUT DATA TO CONTOUR PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        CONTOUR PASSING FILE.
C  OUTPUT: (L*4)   LPASS    = .TRUE.  => OUTPUT DATA TO METPOP PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        METPOP PASSING FILE.
C
C  OUTPUT: (C*80)  DSNPAP   = OUTPUT TEXT FILE NAME
C  OUTPUT: (C*80)  DSNOUT   = OUTPUT CONTOUR DATA SET NAME (SEQUENTIAL)
C  OUTPUT: (C*80)  DSNPAS   = OUTPUT PASSING FILE NAME (SEQUENTIAL)
CX OUTPUT: (L*4)   LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
CX                          = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
CX OUTPUT: (I*4)   ITSEL    = INDEX OF TEMPERATURE SELECTED FOR GRAPH
CX                          (FROM INPUT LIST).
CX OUTPUT: (C*40)  GTIT1    = ENTERED TITLE FOR GRAPH
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  ANDREW BOWEN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    26/04/93
C
C MODIFIED:
C          05/04/95 TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C                   ADDED CALL TO XXFLSH ROUTINE TO FLUSH OUT THE PIPE AFTER
C                   IT HAS BEEN WRITTEN TO.
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM     , MAXT      , IFOUT     ,
     &           ITSEL     , LOGIC     , I
C-----------------------------------------------------------------------
      REAL*8       TINE(NDTEM) 
C-----------------------------------------------------------------------
      CHARACTER    DSNPAP*80   , DSNOUT*80   , DSNPAS*80   , GTIT1*40
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LPAPER      , LCONT       , LNEWPA  ,
     &             LPASS       , LGPH
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1 )
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  WRITE LIST OF ELECTRON TEMPERATURES TO IDL
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) MAXT
      WRITE(PIPEOU,*) (TINE(I),I=1,MAXT)
      WRITE(PIPEOU,*) IFOUT
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C  READ DATA FROM IDL
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LGPH = .TRUE.
          ELSE
              LGPH = .FALSE.
          ENDIF
C
          IF (LGPH) THEN
              READ(PIPEIN,*) ITSEL
              READ(PIPEIN,'(A)') GTIT1
          ENDIF
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPAPER = .TRUE.
          ELSE
              LPAPER = .FALSE.
          ENDIF
C
          IF (LPAPER) THEN
              READ(PIPEIN,*) LOGIC
              IF (LOGIC.EQ.ONE) THEN
                  LNEWPA = .FALSE.
              ELSE
                  LNEWPA = .TRUE.
              ENDIF
              READ(PIPEIN,'(A)') DSNPAP
          ENDIF
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LCONT = .TRUE.
          ELSE
              LCONT = .FALSE.
          ENDIF
C
          IF (LCONT) READ(PIPEIN,'(A)') DSNOUT
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPASS = .TRUE.
          ELSE
              LPASS = .FALSE.
          ENDIF
C
          IF (LPASS) READ(PIPEIN,'(A)') DSNPAS
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
