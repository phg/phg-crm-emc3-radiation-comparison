CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas205/b5wr12.for,v 1.1 2004/07/06 11:25:23 whitefor Exp $ Date $Date: 2004/07/06 11:25:23 $
CX
      SUBROUTINE B5WR12( IUNIT  , DATE   , IZ1   , IL    ,
     &                   NDMET  , NDTEM  , NDDEN ,
     &                   NMET   , IMETR  ,
     &                   IFOUT  , MAXT   , TINE  ,
     &                   IDOUT  , MAXD   , DINE  ,
     &                   CSTRGA , STCKM
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B5WR12 *********************
C
C  PURPOSE: TO OUTPUT METASTABLE POPULATION PARAMETERS TO THE PASSING
C           FILE ON STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS205
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  IUNIT   = OUTPUT STREAM NUMBER
C  INPUT :  (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (I*4)  IZ1     = RECOMBINING ION CHARGE
C                            (NOTE: IZ1 SHOULD EQUAL Z+1)
C  INPUT :  (I*4)  IL      = NUMBER OF INDEX ENERGY LEVELS
C
C  INPUT :  (I*4)  NDMET   = MAX. NO. OF METASTABLES ALLOWED
C  INPUT :  (I*4)  NDTEM   = MAX. NO. OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDDEN   = MAX. NUMBER OF DENSITIES ALLOWED
C
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES ( 1 -> 5 )
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLES IN COMPLETE LEVEL LIST
C
C  INPUT :  (I*4)  IFOUT   = 1 => INPUT TEMPERATURES IN KELVIN
C                            2 => INPUT TEMPERATURES IN EV
C                            3 => INPUT TEMPERATURES IN REDUCED FORM
C  INPUT :  (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES (1 -> 20)
C  INPUT :  (R*8)  TINE()  = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C
C  INPUT :  (I*4)  IDOUT   = 1 => INPUT DENSITIES IN CM-3
C                            2 => INPUT DENSITIES IN REDUCED FORM
C  INPUT :  (I*4)  MAXD    = NUMBER OF INPUT DENSITIES (1 -> 20)
C  INPUT :  (R*8)  DINE()  = ELECTRON DENSITIES (UNITS: SEE 'IFOUT')
C
C  INPUT :  (C*18) CSTRGA()= INDEX LEVEL CONFIGURATIONS
C  INPUT :  (R*8)  STCKM(,,)=METASTABLE STATE POPULATIONS:
C                            1ST DIMENSION = METASTABLE STATE INDEX
C                            2ND DIMENSION = TEMPERATURE INDEX
C                            3RD DIMENSION = DENSITY INDEX
C
C           (I*4)  L1      = PARAMETER = 1
C           (I*4)  L2      = PARAMETER = 2
C           (I*4)  L3      = PARAMETER = 3
C
C           (I*4)  I       = GENERAL USE
C           (I*4)  IM      = ARRAY INDEX POINTER FOR METASTABLE STATES
C           (I*4)  IT      = ARRAY INDEX POINTER FOR TEMPERATURES
C           (I*4)  ID      = ARRAY INDEX POINTER FOR DENSITIES
C
C           (R*8)  RDEN()  = ELECTRON DENSITIES (UNITS: REDUCED FORM)
C           (R*8)  RTEM()  = ELECTRON TEMPERATURES (UNITS: REDUCED FORM)
C
C           (C*1)  CSTAR   = '*'
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXTCON     ADAS      CONVERTS ENTERED TEMP. VALUES TO EV.
C          XXDCON     ADAS      CONVERTS ENTERED DENSITY VALUES TO CM-3.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  20/05/93 - ADAS91 PEB: TO REFLECT CHANGES IN BXDATA THE
C                                 CHARACTER ARRAY CSTRGA IS NOW 18 BYTES
C                                 INSTEAD OF 12.
C                                 NOTE: ONLY THE FIRST 12 BYTES ARE
C                                       OUTPUT TO THE PASSING FILE.
C
C-----------------------------------------------------------------------
      INTEGER   L1           , L2            , L3
C-----------------------------------------------------------------------
      PARAMETER ( L1=1 , L2=2 , L3=3 )
C-----------------------------------------------------------------------
      INTEGER   IUNIT        , IZ1           , IL         ,
     &          NDMET        , NDTEM         , NDDEN      ,
     &          NMET         ,
     &          IFOUT        , MAXT          ,
     &          IDOUT        , MAXD
      INTEGER   I            , IM            , IT         , ID
C-----------------------------------------------------------------------
      CHARACTER CSTAR*1      , DATE*8
C-----------------------------------------------------------------------
      INTEGER   IMETR(NMET)
C-----------------------------------------------------------------------
      REAL*8    RTEM(20)     , RDEN(20)
      REAL*8    TINE(MAXT)   , DINE(MAXD)    , STCKM(NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      CHARACTER CSTRGA(IL)*18
C-----------------------------------------------------------------------
      DATA      CSTAR/'*'/
C
C-----------------------------------------------------------------------
C
      CALL XXDCON( IDOUT , L2 , IZ1 , MAXD , DINE , RDEN )
      CALL XXTCON( IFOUT , L3 , IZ1 , MAXT , TINE , RTEM )
C-----------------------------------------------------------------------
      WRITE(IUNIT,1000)
      WRITE(IUNIT,1001) NMET
      WRITE(IUNIT,1002) (I               , I=1,NMET)
      WRITE(IUNIT,1003) (CSTRGA(I)(1:12) , I=1,NMET)
      WRITE(IUNIT,1004) (IMETR(I)        , I=1,NMET)
      WRITE(IUNIT,1005) (CSTAR           , I=1,NMET)
      WRITE(IUNIT,1006) (CSTAR           , I=1,NMET)
      WRITE(IUNIT,1007)
      WRITE(IUNIT,1008) L1 , MAXT , MAXD
      WRITE(IUNIT,1009) DBLE(IZ1)
      WRITE(IUNIT,1007)
      WRITE(IUNIT,1007)
      WRITE(IUNIT,1010) (RDEN(I)         , I=1,MAXD)
      WRITE(IUNIT,1010) (RTEM(I)         , I=1,MAXT)
      WRITE(IUNIT,1011) IZ1, DATE
         DO 1 IM=1,NMET
            WRITE(IUNIT,1012) IM
               DO 2 IT=1,MAXT
                  WRITE(IUNIT,1010) (STCKM(IM,IT,ID), ID=1,MAXD)
    2          CONTINUE
    1    CONTINUE
 
C-----------------------------------------------------------------------
 1000 FORMAT(80('='))
 1001 FORMAT('NMET         =',1X,I1)
 1002 FORMAT('ORDER        =',1X,5(12X,I1))
 1003 FORMAT('DESIGNATION  =',1X,5(1X,A12))
 1004 FORMAT('COPDAT INDEX =',1X,5(11X,I2))
 1005 FORMAT('PARENT REF.  =',1X,5(12X,A1))
 1006 FORMAT('SPNSYS REF.  =',1X,5(12X,A1))
 1007 FORMAT(80('-'))
 1008 FORMAT(3I5)
 1009 FORMAT(F10.5)
 1010 FORMAT(1P,8D10.2)
 1011 FORMAT(15('-'),' METASTABLE POPULATIONS ',15('-'),
     &       '/ Z1=',I2,3X,'/ DATE= ',A8)
 1012 FORMAT('/',I1,'/')
C-----------------------------------------------------------------------
      RETURN
      END
