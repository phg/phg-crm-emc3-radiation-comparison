CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas205/b5ispf.for,v 1.5 2004/07/06 11:23:26 whitefor Exp $ Date $Date: 2004/07/06 11:23:26 $
CX

      SUBROUTINE B5ISPF( IPAN  , LPEND  ,
     &                   NDTEM , NDDEN  , NDMET , IL    ,
     &                   NV    , TSCEF  ,
     &                   TITLE , NMET   , IMETR ,
     &                   IFOUT ,
     &                   MAXT  , TINE  , TINP   , TINH  ,
     &                   IDOUT ,
     &                   MAXD  , DINE  , DINP   , RATHA , RATIA ,
     &                   ZEFF  ,
     &                   LPSEL , LZSEL , LISEL  , LHSEL , LRSEL , LGPH ,
     &                   ITSEL , GTIT1 , LGRD1  , LDEF1 ,
     &                   XL1   , XU1   , YL1    , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B5ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS205
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTEM    = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDDEN    = MAX. NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)   NDMET    = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER OF TEMPERATURES
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (I*4)   NMET     = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  OUTPUT: (I*4)   IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINP()   = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINH()   = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C
C  OUTPUT: (I*4)   IDOUT    = 1 => INPUT DENSITIES IN CM-3
C                           = 2 => INPUT DENSITIES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 20)
C  OUTPUT: (R*8)   DINE()   = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   DINP()   = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   RATHA()  = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  OUTPUT: (R*8)   RATIA()  = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C
C  OUTPUT: (R*8)   ZEFF     = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C
C  OUTPUT: (L*4)   LPSEL    = .TRUE.  => INCLUDE PROTON COLLISIONS
C                           = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  OUTPUT: (L*4)   LZSEL    = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                        PLASMA Z EFFECTIVE'ZEFF'.
C                           = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                        WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                           (ONLY USED IF 'LPSEL=.TRUE.')
C  OUTPUT: (L*4)   LISEL    = .TRUE.  => INCLUDE IONISATION RATES
C                           = .FALSE. => DO NOT INCLUDE IONISATION RATES
C  OUTPUT: (L*4)   LHSEL    = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                        NEUTRAL HYDROGREN.
C                           = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                        FROM NEUTRAL HYDROGREN.
C  OUTPUT: (L*4)   LRSEL    = .TRUE.  => INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  OUTPUT: (L*4)   LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                           = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C
C  OUTPUT: (I*4)   ITSEL    = TEMPERATURE SELECTED FOR GRAPH (FROM INPUT
C                             LIST).
C  OUTPUT: (C*40)  GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C
C          (L*4)   LMCHNG   = .TRUE.  => METASTABLE INDEX ALLOCATIONS
C                                        READ FROM TABLE NO LONGER VALID
C                           = .FALSE. => METASTABLE INDEX ALLOCATIONS
C                                        READ FROM TABLE ARE VALID
C          (L*4)   LTCHNG   = .TRUE.  => ENTERED TEMPERATURE VALUES MAY
C                                        HAVE BEEN ALTERED BY USER.
C                           = .FALSE. => ENTERED TEMPERATURE VALUES HAVE
C                                        NOT BEEN ALTERED BY USER.
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   I        = LOOP INCREMENT
C          (I*4)   J        = LOOP INCREMENT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  ANDREW BOWEN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    01/04/93
C
C MODIFIED:
C          05/04/95 TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C                   ADDED CALLS TO XXFLSH ROUTINE AFTER WRITING TO PIPE
C                   IN ORDER TO FLUSH THE PIPE OUT - THIS IS NEEDED FOR
C                   CERTAIN PLATFORMS.
C
C          07/04/95 TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C                   CHANGED TSCEF TO A (14,3) ARRAY IN LINE WITH OTHER
C                   ROUTINES.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   IPAN       , NMET       ,
     &          NDTEM      , NDDEN      , NDMET     , IL      , NV    ,
     &          IFOUT      , MAXT       ,
     &          IDOUT      , MAXD       ,
     &          ITSEL
      INTEGER   I          , J
C-----------------------------------------------------------------------
      REAL*8    ZEFF       ,
     &          XL1        , XU1        , YL1       , YU1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40  , GTIT1*40
C-----------------------------------------------------------------------
      LOGICAL    LPEND
      LOGICAL    LPSEL     , LZSEL      , LISEL     , LHSEL     ,
     &           LRSEL     , LGPH       , LGRD1     , LDEF1
      LOGICAL    LMCHNG    , LTCHNG
C-----------------------------------------------------------------------
      INTEGER    IMETR(NDMET) , LOGIC
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C-----------------------------------------------------------------------
      REAL*8     TSCEF(14,3)  ,
     &           TINE(NDTEM) , TINP(NDTEM)  , TINH(NDTEM) ,
     &           DINE(NDDEN) , DINP(NDDEN)  , RATHA(NDDEN), RATIA(NDDEN)
C-----------------------------------------------------------------------
      SAVE       LMCHNG      , LTCHNG
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      IF (LPEND) THEN
          WRITE(PIPEOU,*) ONE
      ELSE
          WRITE(PIPEOU,*) ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDTEM
      WRITE(PIPEOU,*) NDDEN
      WRITE(PIPEOU,*) NDMET
      WRITE(PIPEOU,*) IL
      WRITE(PIPEOU,*) NV
      WRITE(PIPEOU,*) ((TSCEF(I,J),I=1,14),J=1,3)
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF

      READ(PIPEIN,'(A)') TITLE
      READ(PIPEIN,*) NMET
      READ(PIPEIN,*) (IMETR(I),I=1,NDMET)
      READ(PIPEIN,*) IFOUT
      READ(PIPEIN,*) MAXT
      READ(PIPEIN,*) (TINE(I),I=1,NDTEM)
      READ(PIPEIN,*) (TINP(I),I=1,NDTEM)
      READ(PIPEIN,*) (TINH(I),I=1,NDTEM)
      READ(PIPEIN,*) IDOUT
      READ(PIPEIN,*) MAXD
      READ(PIPEIN,*) (DINE(I),I=1,NDDEN)
      READ(PIPEIN,*) (DINP(I),I=1,NDDEN)
      READ(PIPEIN,*) (RATHA(I),I=1,NDDEN)
      READ(PIPEIN,*) (RATIA(I),I=1,NDDEN)
      READ(PIPEIN,*) ZEFF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPSEL = .TRUE.
      ELSE
          LPSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LZSEL = .TRUE.
      ELSE
          LZSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LISEL = .TRUE.
      ELSE
          LISEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LHSEL = .TRUE.
      ELSE
          LHSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LRSEL = .TRUE.
      ELSE
          LRSEL = .FALSE.
      ENDIF
C
C
      RETURN
      END
