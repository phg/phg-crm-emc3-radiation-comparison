CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas201/b1data.for,v 1.1 2004/07/06 11:17:47 whitefor Exp $ Date $Date: 2004/07/06 11:17:47 $
CX
       SUBROUTINE B1DATA( IUNIT  , NDLEV  , NDTRN ,
     &                    TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
     &                    IL     ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA   , WA ,
     &                    NV     , SCEF   ,
     &                    ITRAN  ,
     &                    I1A    , I2A    , AVAL  , SCOM
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B1DATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT SPECIFIC Z EXCITATION FILE.
C            (ELECTRON IMPACT TRANSITIONS ONLY).
C
C  CALLING PROGRAM: ADAS201
C
C  DATA:
C           THE 'REAL' DATA IN THE FILE IS REPRESENTED IN AN ABBREVIATED
C           FORM WHICH OMITS THE "D" OR "E" EXPONENT SPECIFIER.
C           e.g. 1.23D-06 or 1.23E-06 IS REPRESENTED AS 1.23-06
C                6.75D+07 or 6.75E+07 IS REPRESENTED AS 6.75+07
C
C           THEREFORE THE FORM OF EACH 'REAL' NUMBER IN THE DATA SET IS:
C                          N.NN+NN or N.NN-NN
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C           RATE COEFFT.        : CM3 SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C
C  OUTPUT: (C*3)  TITLED  = ELEMENT SYMBOL.
C  OUTPUT: (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  OUTPUT: (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  OUTPUT: (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  OUTPUT: (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  OUTPUT: (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*12) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  OUTPUT: (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  OUTPUT: (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C                           (INITIALLY JUST THE MANTISSA. SEE 'ITPOW()')
C                           (NOTE: TE=TP=TH IS ASSUMED)
C
C  OUTPUT: (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C                                            TRANSITIONS.
C
C  OUTPUT: (I*4)  I1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C  OUTPUT: (I*4)  I2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C  OUTPUT: (R*8)  AVAL()  = ELECTRON IMPACT TRANSITION:
C                            A-VALUE (SEC-1)
C  OUTPUT: (R*8)  SCOM(,) = ELECTRON IMPACT TRANSITION:
C                            GAMMA VALUES
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (I*4)  NDTEM   = PARAMETER = MAX NUMBER OF INPUT FILE TEMPS.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IQS     = X-SECT DATA FORMAT SELECTOR
C                           NOTE: IQS=3 ONLY ALLOWED IN THIS PROGRAM
C          (I*4)  I       = GENERAL USE.
C          (I*4)  J       = GENERAL USE.
C          (I*4)  J1      = INPUT DATA FILE - SELECTED TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C          (I*4)  J2      = INPUT DATA FILE - SELECTED TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  ILINE   = ENERGY LEVEL INDEX FOR CURRENT LINE
C          (I*4)  IAPOW   = EXPONENT OF 'AVALM'
C          (I*4)  IGPOW() = EXPONENT OF 'GAMMA()'
C          (I*4)  ITPOW() = TEMPERATURES - EXPONENT
C                           NOTE: MANTISSA INITIALLY KEPT IN 'SCEF()'
C
C          (R*4)  ZF      = SHOULD BE EQUIVALENT TO 'IZ1'
C
C          (R*8)  AVALM   = INPUT DATA FILE - SELECTED TRANSITION:
C                           MANTISSA OF:   ('IAPOW' => EXPONENT)
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  GAMMA() = INPUT DATA FILE - SELECTED TRANSITION:
C                           MANTISSA OF: ('IGPOW()' => EXPONENT)
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           DIMENSION => TEMPERATURE 'SCEF()'
C
C          (C*1)  TCODE   = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C          (C*80) CLINE   = CURRENT ENERGY LEVEL INDEX PARAMETER LINE
C
C          (L*4)  LDATA   = IDENTIFIES  WHETHER  THE END OF AN  INPUT
C                           SECTION IN THE DATA SET HAS BEEN LOCATED.
C                           (.TRUE. => END OF SECTION REACHED)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  16/11/90 - LEVEL LINE READ AS A CHARACTER*80 STRING FIRST
C                     (PE BRIDEN)
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM
C-----------------------------------------------------------------------
      PARAMETER ( NDTEM = 8 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   IUNIT       , NDLEV          , NDTRN      ,
     &          IZ          , IZ0            , IZ1        ,
     &          IL          , NV             , ITRAN
      INTEGER   ILINE
      INTEGER   IQS         , I              , J          , J1  , J2  ,
     &          IAPOW       , IGPOW(NDTEM)   , ITPOW(NDTEM)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &          I1A(NDTRN)  , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*4    ZF
C-----------------------------------------------------------------------
      REAL*8    BWNO        , AVALM
      REAL*8    SCEF(NDTEM) , GAMMA(NDTEM)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)      ,
     &          AVAL(NDTRN) , SCOM(NDTEM,NDTRN)
C-----------------------------------------------------------------------
      CHARACTER TITLED*3    , TCODE*1        , CSTRGA(NDLEV)*12
      CHARACTER CLINE*80
C-----------------------------------------------------------------------
      LOGICAL   LDATA
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C***********************************************************************
C INPUT ION SPECIFICATIONS.
C***********************************************************************
C
      READ(IUNIT,1000) TITLED, IZ, IZ0, IZ1, BWNO
         IF(BWNO.LE.0.0D0) THEN
            WRITE(I4UNIT(-1),1001) 'IONISATION POTENTIAL .LE. 0'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C***********************************************************************
C READ IN ENERGY LEVEL SPECIFICATIONS
C***********************************************************************
C
      LDATA=.TRUE.
C
         DO 1 I=1,NDLEV
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I5)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE=-1.
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                                     ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
                     READ (CLINE,1003) IA(I)  , CSTRGA(I) ,
     &                                 ISA(I) , ILA(I)    ,
     &                                 XJA(I) , WA(I)
                  ENDIF
            ENDIF
    1    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'COPASE DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF
C
C***********************************************************************
C READ IN TEMPERATURES (KELVIN) AND DATA FORMAT SELECTOR
C***********************************************************************
C
      READ(IUNIT,1005) ZF, IQS, (SCEF(I),ITPOW(I),I=1,8)
C
C CHECK DATA FORMAT SELECTOR IS VALID
C
         IF(IQS.NE.3) THEN
            WRITE(I4UNIT(-1),1001) '(IQS.NE.3) -'
            WRITE(I4UNIT(-1),1006)
     &      'FILE CONTAINS INVALID DATA FORMAT SELECTOR (MUST EQUAL 3)'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C CHECK 'ZF' EQUALS IZ1
C
         IF(NINT(ZF).NE.IZ1) THEN
            WRITE(I4UNIT(-1),1001) '(ZF.NE.IZ1) -'
            WRITE(I4UNIT(-1),1006)
     &      'FILE CONTAINS INCONSISTANT RECOMBINING ION CHARGE VALUES'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C IDENTIFY THE NUMBER OF TEMPERATURES VALUES INPUT
C
         DO 2 NV=8,1,-1
            IF(SCEF(NV).GT.0.0D0) GOTO 3
    2    CONTINUE
C
C CHECK THAT AT LEAST ONE VALID TEMPERATURE EXISTS.
C
    3    IF (NV.LE.0) THEN
            WRITE(I4UNIT(-1),1001) '(NV.LE.0) -'
            WRITE(I4UNIT(-1),1006)
     &      'NO VALID TEMPERATURE VALUES FOUND (NONE > 0.0)'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C-----------------------------------------------------------------------
C COMBINE INPUT MANTISSA AND EXPONENT FOR TEMPERATURES
C-----------------------------------------------------------------------
C
         DO 4 I=1,NV
            SCEF(I)  = SCEF(I)  * ( 10.0**DBLE(ITPOW(I)) )
    4    CONTINUE
C
C-----------------------------------------------------------------------
C CHECK TEMPERATURES ARE IN A STRICTLY INCREASING MONOTONIC SEQUENCE
C (REQUIRED FOR THE LATER USE OF THE MINIMAX AND SPLINE NAG ROUTINES)
C-----------------------------------------------------------------------
C
         DO 5 I=2,NV
            IF (SCEF(I).LE.SCEF(I-1)) THEN
               WRITE(I4UNIT(-1),1007) '(SCEF(',I,').LE.SCEF(',I-1,')) -'
               WRITE(I4UNIT(-1),1006)
     &      'TEMPERATURES ARE NOT IN A STRICT MONOTONIC ASCENDING ORDER'
               WRITE(I4UNIT(-1),1002)
               STOP
             ENDIF
    5    CONTINUE
C
C***********************************************************************
C READ IN TRANSITION SPECIFICATIONS
C***********************************************************************
C
      LDATA=.TRUE.
      ITRAN=0
C
         DO 6 I=1,NDTRN
            IF (LDATA) THEN
               READ(IUNIT,1008) TCODE    , J2 , J1 , AVALM , IAPOW,
     &                          (GAMMA(J),IGPOW(J),J=1,NV)
C
C TRANSITION INPUT INFORMATION IS TERMINATED BY J2=-1
C
                  IF (J2.LE.0) THEN
                     LDATA=.FALSE.
                  ELSEIF (TCODE.EQ.' ') THEN
C
C-----------------------------------------------------------------------
C COMBINE INPUT MANTISSA AND EXPONENT FOR A-VALUE, GAMMAS
C-----------------------------------------------------------------------
C
                      ITRAN=ITRAN+1
                      AVAL(ITRAN)=AVALM*( 10.0**DBLE(IAPOW) )
                         DO 7 J=1,NV
                          SCOM(J,ITRAN)=GAMMA(J)*(10.0**DBLE(IGPOW(J)))
    7                    CONTINUE
C
C-----------------------------------------------------------------------
C CHECK IF ENERGY LEVELS ARE IN CORRECT ORDER. IF NOT REVERSE.
C-----------------------------------------------------------------------
C
                         IF (WA(J2).LT.WA(J1)) THEN
                            I1A(ITRAN)=J2
                            I2A(ITRAN)=J1
                            WRITE(I4UNIT(-1),1009)
     &                       ' B1DATA MESSAGE: UPPER AND ',
     &                       'LOWER ENERGY LEVELS HAVE BEEN REVERSED.',
     &                       'CORRECT LEVELS ARE - UPPER = ',J1,
     &                       '  LOWER = ',J2
                         ELSE
                            I1A(ITRAN)=J1
                            I2A(ITRAN)=J2
                         ENDIF
C-----------------------------------------------------------------------
                  ENDIF
            ENDIF
    6    CONTINUE
C
C-----------------------------------------------------------------------
C
         IF (LDATA) THEN
            READ (IUNIT,1010) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &               'COPASE DATA SET CONTAINS > ',NDTRN,' TRANSITIONS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ENDIF
         ENDIF
C
C***********************************************************************
C
 1000 FORMAT(1A3,I2,2I10,F15.0)
 1001 FORMAT(1X,32('*'),' B1DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I5,3X,1A12,5X,2(I1,1X),F4.1,3X,F15.0)
 1004 FORMAT(I5)
 1005 FORMAT(F5.2,4X,I1,6X,8(F5.2,I3))
 1006 FORMAT(1X,A)
 1007 FORMAT(1X,32('*'),' B1DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,2(I1,A))
 1008 FORMAT(A1,I3,I4,9(F5.2,I3))
 1009 FORMAT(A,A/17X,2(A,I2)/)
 1010 FORMAT(1X,I3)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
