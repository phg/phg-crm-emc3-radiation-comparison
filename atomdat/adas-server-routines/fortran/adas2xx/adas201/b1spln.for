CX UNIX PORT SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas201/b1spln.for,v 1.2 2004/07/06 11:18:59 whitefor Exp $ Date $Date: 2004/07/06 11:18:59 $
CX

      SUBROUTINE B1SPLN(         LOSEL ,
     &                   NV    , MAXT  , NPSPL  ,
     &                   SCEF  , TOA   , TOSA   ,
     &                   GAMMA , GAMOA , GAMOSA ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B1SPLN *********************
C
C  (IDENTICAL TO: C1SPLN (EXCEPT SOME VARIABLE NAMES ARE CHANGED))
C
C  PURPOSE:
C         1) PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE) VERSUS LOG(GAMMA)
C            INPUT DATA. ('SCEF' VERSUS 'GAMMA' , NV DATA PAIRS)
C
C         2) INTERPOLATES 'MAXT'  GAMMA VALUES USING ABOVE SPLINES AT
C            TEMPERATURES READ IN FROM ISPF PANELS FOR TABULAR OUTPUT.
C            (ANY TEMPERATURE VALUES WHICH REQUIRED EXTRAPOLATION TO
C             TAKE PLACE ARE SET TO ZERO).
C                 - THIS STEP ONLY TAKES PLACE IF 'LOSEL=.TRUE.' -
C
C         3) INTERPOLATES 'NPSPL' GAMMA VALUES USING ABOVE SPLINES AT
C            TEMPERATURES EQUI-DISTANCE ON RANGE OF LOG(TEMPERATURES)
C            STORED IN INPUT 'SCEF' ARRAY.
C
C  CALLING PROGRAM: ADAS201
C
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LOSEL   = .TRUE.  => CALCULATE GAMMAS FOR INPUT TEMPS.
C                                      READ FROM ISPF PANEL.
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS READ FOR THE TRANSITION BEING ASSESSED
C  INPUT : (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES AT
C                           WHICH INTERPOLATED GAMMA VALUES ARE REQUIRED
C                           FOR TABULAR OUTPUT.
C  INPUT : (I*4)  NPSPL   = NUMBER OF  SPLINE  INTERPOLATED  GAMMA/TEMP.
C                           REQUIRED FOR GRAPHICAL DISPLAY.
C
C  INPUT : (I*4)  SCEF()  = INPUT DATA FILE: TEMPERATURES (KELVIN)
C  INPUT : (I*4)  TOA()   = ISPF PANEL ENTERED TEMPERATURES (KELVIN)
C  OUTPUT: (I*4)  TOSA()  = 'NPSPL' TEMPERATURES FOR GRAPHICAL OUTPUT
C                           (KELVIN).
C
C  INPUT : (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                           GAMMA VALUES AT 'SCEF()'.
C  OUTPUT: (I*4)  GAMOA() = SPLINE INTERPOLATED GAMMA VALUES AT 'TOA()'
C                           (EXTRAPOLATED VALUES = 0.0).
C  OUTPUT: (R*8)  GAMOSA()= SPLINE INTERPOLATED GAMMA VALUES AT 'TOSA()'
C
C  OUTPUT: (L*4)  LTRNG() = .TRUE.  => OUTPUT   SPLINE    VALUE    WAS
C                                      INTERPOLATED FOR 'DLOG(TOA()'.
C                           .FALSE. => OUTPUT   SPLINE    VALUE    WAS
C                                      EXTRAPOLATED FOR 'DLOG(TOA()'.
C                                      (NOTE: 'YOUT()=0' AS 'IOPT < 0').
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF  INPUT  TEMP/GAMMA
C                                      PAIRS MUST BE >= 'NV'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF 'OUTPUT TEMP/GAMMA
C                                      PAIRS MUST BE >= 'MAXT' & 'NPSPL'
C
C          (I*4)  IARR    = ARRAY SUBSCRIPT USED FOR TEMP/GAMMA PAIRS
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (R*8)  TSTEP   = THE SIZE OF STEP BETWEEN 'XOUT()' VALUES FOR
C                           GRAPHICAL  OUTPUT  TEMP/GAMMA  PAIRS  TO  BE
C                           CALCULATED USING SPLINES.
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  XIN()   = LOG( 'SCEF()' )
C          (R*8)  YIN()   = LOG( 'GAMMA()' )
C          (R*8)  XOUT()  = LOG(TEMPERATURES AT WHICH SPLINES REQUIRED)
C          (R*8)  YOUT()  = LOG(OUTPUT SPLINE INTERPOLATED GAMMA VALUES)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C          (L*4)  LDUMP() = .TRUE.  => OUTPUT SPLINE VALUE INTRPOLATED
C                                      FOR 'YOUT()'.
C                           .FALSE. => OUTPUT SPLINE VALUE EXTRAPOLATED
C                                      FOR 'YOUT()'.
C                                      (NOTE: USED AS A DUMMY ARGUMENT.
C                                             ALL VALUES WILL BE TRUE.)
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    05/02/91
C
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C VERSION:  1.2				DATE: 27-09-95
C		- INCREASED PARAMETER NIN FROM 8 TO 14 (SAME
C		  CHANGE MADE IN THE JET IBM CODES BY PAUL BRIDEN).
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN          , NOUT
C-----------------------------------------------------------------------
      PARAMETER( NIN = 14     , NOUT = 100    )
C-----------------------------------------------------------------------
      INTEGER    NV           , MAXT          , NPSPL
      INTEGER    IOPT         , IARR
C-----------------------------------------------------------------------
      REAL*8     R8FUN1       , TSTEP
C-----------------------------------------------------------------------
      LOGICAL    LOSEL        , LSETX
C-----------------------------------------------------------------------
      REAL*8     SCEF(NV)     , GAMMA(NV)     ,
     &           TOA(MAXT)    , GAMOA(MAXT)   ,
     &           TOSA(NPSPL)  , GAMOSA(NPSPL)
      REAL*8     XIN(NIN)     , YIN(NIN)      ,
     &           XOUT(NOUT)   , YOUT(NOUT)
      REAL*8     DF(NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(MAXT)  , LDUMP(NOUT)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.NV)
     &             STOP ' B1SPLN ERROR: NIN < NV - INCREASE NIN'
      IF (NOUT.LT.MAXT)
     &             STOP ' B1SPLN ERROR: NOUT < MAXT - INCREASE NTOUT'
      IF (NOUT.LT.NPSPL)
     &             STOP ' B1SPLN ERROR: NOUT < NPSPL - INCREASE NGOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH OFF EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = -1
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT GAMMA/TEMP PAIRS
C-----------------------------------------------------------------------
C
         DO 1 IARR=1,NV
            XIN(IARR) = DLOG( SCEF(IARR)  )
            YIN(IARR) = DLOG( GAMMA(IARR) )
    1    CONTINUE
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED TEMP/GAMMA PAIRS FOR TABULAR OUTPUT - IF REQUESTED
C-----------------------------------------------------------------------
C
         IF (LOSEL) THEN
C
               DO 2 IARR=1,MAXT
                  XOUT(IARR) = DLOG(TOA(IARR))
    2          CONTINUE
C
            CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &                   NV    , XIN    , YIN    ,
     &                   MAXT  , XOUT   , YOUT   ,
     &                   DF    , LTRNG
     &                 )
C
               DO 3 IARR=1,MAXT
                  IF (LTRNG(IARR)) THEN
                     GAMOA(IARR) = DEXP( YOUT(IARR) )
                  ELSE
                     GAMOA(IARR) = 0.0
                  ENDIF
    3          CONTINUE
C
         ENDIF
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED TEMP/GAMMA PAIRS FOR GRAPH OUTPUT- 'NPSPL' VALUES
C-----------------------------------------------------------------------
C
      TSTEP = ( XIN(NV)-XIN(1) ) / DBLE( NPSPL-1 )
C
         DO 4 IARR=1,NPSPL
            XOUT(IARR) = ( DBLE(IARR-1)*TSTEP ) + XIN(1)
    4    CONTINUE
C
      CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &             NV    , XIN    , YIN    ,
     &             NPSPL , XOUT   , YOUT   ,
     &             DF    , LDUMP
     &           )
C
         DO 5 IARR=1,NPSPL
            TOSA(IARR)   = DEXP( XOUT(IARR) )
            GAMOSA(IARR) = DEXP( YOUT(IARR) )
    5    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
