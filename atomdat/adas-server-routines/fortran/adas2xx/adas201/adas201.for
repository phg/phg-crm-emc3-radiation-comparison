CX UNIX PORT SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas201/adas201.for,v 1.8 2004/07/06 10:10:39 whitefor Exp $ Date $Date: 2004/07/06 10:10:39 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS201 **********************
C
C  ORIGINAL NAME: SPRTGRF5
C
C  VERSION:  1.1
C
C  PURPOSE:  TO  GRAPH  AND INTERPOLATE  SELECTED  DATA  FROM  MASTER
C            CONDENSED FILES  CONTAINING ELECTRON  IMPACT  RATE  DATA
C            (GAMMA VALUES) AS A FUNCTION OF TEMPERATURE.
C
C            IT ALLOWS THE EXAMINATION AND DISPLAY OF THE DATA FOR  A
C            SINGLE SELECTED ELECTRON TRANSITION AS FOLLOWS:
C
C            TABULATED VALUES FOR THE SELECTED TRANSITION ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE, FROM WHICH INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF REQUESTED A MINIMAX
C            POLYNOMIAL  IS FITTED THROUGH THE  DATA  AND  GRAHICALLY
C            DISPLAYED. THE MINIMAX COEFFICIENTS AND THE ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE ARE OUTPUT
C            TO HARDCOPY.
C
C
C  NOTE:     THIS PROGRAM ONLY ANALYSES THE ELECTRON IMPACT RATE DATA.
C            (I.E. TRANSITION ENTRIES WHERE TCODE() = ' ').
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.<SE>LIKE.DATA(<MEMBER>)'
C
C            WHERE, <SE> = ELEMENT SYMBOL FOR ISO-ELECTRON SERIES
C                          (ONE OR TWO CHARACTERS)
C
C            THE <MEMBER> NAME IS CONSTRUCTED AS FOLLOWS:
C
C               <INITIALS OF DATA SOURCE><YEAR><ELEMENT SYMBOL><*>
C
C               E.G. HPS89BE,  BKT1988C
C
C               <INITIALS OF DATA SOURCE> - E.G. HPS = HP SUMMERS
C
C               <YEAR> - THIS MAY OR MAY NOT BE ABBREVIATED
C
C               IF <ELEMENT SYMBOL> IS PRESENT =>
C                                       SPECIFIC  ION  FILE.  ONE
C                                       ELEMENT ONLY.
C                                           E.G. BE = BERYLIUM
C                                                C  = CARBON
C               IF <ELEMENT SYMBOL> IS MISSING =>
C                                       GENERAL Z EXCITATION FILES
C                                       CONTAINING BETWEEN 2 AND 7
C                                       ELEMENTS (INCLUSIVE). THIS
C                                       PROGRAM WILL  NOT  ANALYSE
C                                       THESE  FILES,  THEY   MUST
C                                       PASS THROUGH 'SPFMODM2' OR
C                                       AN    EQUIVALENT   PROGRAM
C                                       FIRST.
C
C               <*> - FURTHER EXTENSIONS TO MEMBER NAME MAY BE PRESENT,
C                    THESE MAY REFER TO DATA SET GENERATION STATISTICS.
C
C           EACH VALID MEMBER HAS ITS DATA REPRESENTED IN AN ABBREVIATED
C           FORM WHICH OMITS THE "D" OR "E" EXPONENT SPECIFIER.
C           e.g. 1.23D-06 or 1.23E-06 IS REPRESENTED AS 1.23-06
C                6.75D+07 or 6.75E+07 IS REPRESENTED AS 6.75+07
C
C           THEREFORE THE FORM OF EACH REAL NUMBER IN THE DATA SET IS:
C                          N.NN+NN or N.NN-NN
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C
C
C  PROGRAM:
C
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAXIMUM OF INPUT DATA FILE TEMPS
C          (I*4)  NDTIN   = PARAMETER = MAX.NUMBER OF ISPF ENTRED TEMPS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NPSPL   = PARAMETER =
C                           NUMBER  OF SPLINE INTERPOLATED GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED   GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  ICSTMX  = PARAMETER = 11 =
C                           MAXIMUM SIZE OF NOMENCLATURE STRING OUTPUT
C                           FOR PANELS AND PASSING DATASETS.(see CSTRGB)
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ      =  RECOMBINED ION CHARGE READ FROM INPUT FILE
C          (I*4)  IZ0     =         NUCLEAR CHARGE READ FROM INPUT FILE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE READ FROM INPUT FILE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY INDEX
C                                            LEVELS.
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C                                            TRANSITIONS.
C          (I*4)  ISEL    = INPUT DATA FILE: SELECTED ELECTRON IMPACT
C                                     TRANSITION INDEX FOR ANALYSIS.
C                                     (FOR USE WITH 'IETRN()', 'IE1A()'
C                                      'IE2A()' ARRAYS)
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                                     PAIRS FOR THE SELECTED TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C          (I*4)  IFOUT   = 1 => 'TINE(array)' UNITS: KELVIN
C                         = 2 => 'TINE(array)' UNITS: eV
C                         = 3 => 'TINE(array)' UNITS:REDUCED TEMPERATURE
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C          (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C          (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C          (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C          (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C          (R*8)  R8TCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  EUPPER  = SELECTED TRANSITION: UPPER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  ELOWER  = SELECTED TRANSITION: LOWER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C          (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  AA      = SELECTED TRANSITION A-VALUE (SEC-1)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMINK   = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  XMAXK   = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE COEFF. (cm**3/s)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE COEFF. (cm**3/s)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                         = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
CA         (L*4)  L2GRAPH = .TRUE.  => DISPLAY GRAPH
CA                          .FALSE. => DO NOT DISPLAY GRAPH
C
CA         (L*4)  L2FILE  = .TRUE.  => WRITE DATA TO FILE
CA                          .FALSE. => DO NOT WRITE DATA TO FILE
C
CA         (C*80) SAVFIL  = NAME OF FILE TO WHICH DATA IS WRITTEN.
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*3)  TITLED  = ELEMENT SYMBOL.(INCORPORATED INTO 'TITLX')
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CX         (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
CA         (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM IDL)
C                           (INCORPORATED INTO 'TITLX')
CX         (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CA         (C*40) TITLE   = TITLE OF RUN (READ FROM IDL PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
CX         (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
CA         (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  SCEF()  = INPUT DATA FILE: TEMPERATURES (kelvin)
C          (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GAMMA VALUE AT 'SCEF()'
C          (R*8)  RATE()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           EXCITATION RATE COEFF. (cm**3/s) AT 'SCEF()'
C          (R*8)  DRATE() = INPUT DATA FILE: SELECTED TRANSITION -
C                           DEEXCITATION RATE COEF.(cm**3/s) AT 'SCEF()'
C
C          (R*8)  TINE()  = ISPF ENTERED ELECTRON TEMPERATURE VALUES.
C                           (NOTE: UNITS ARE GIVEN BY 'IFOUT')
C          (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (kelvin)
C          (R*8)  GAMOA() = SPLINE INTEROPLATED GAMMA VALUE AT 'TOA()'
C          (R*8)  ROA()   = EXCITATION RATE COEFF.(cm**3/s) AT 'TOA()'
C          (R*8)  DROA()  = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOA()'
C
C          (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (kelvin)
C          (R*8)  GAMOSA()= SPLINE INTEROPLATED GAMMA VALUE AT 'TOSA()'
C          (R*8)  ROSA()  = EXCITATION RATE COEFF.(cm**3/s) AT 'TOSA()'
C          (R*8)  DROSA() = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOSA()'
C
C          (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (kelvin)
C          (R*8)  GAMOMA()= MINIMAX GENERATED GAMMA VALUE AT 'TOMA()'
C          (R*8)  ROMA()  = EXCITATION RATE COEFF.(cm**3/s) AT 'TOMA()'
C          (R*8)  DROMA() = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOMA()'
C
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES.
C          (C*22) STRGA() = NOMENCLATURE FOR LEVEL 'IA()' INCLUDES:
C                           'CSTRGA()' AND QUANTUM NUMBERS.
C	   (I*4)  IFIRST  = FIRST NON-BLANK CHARCTER IN FILENAME
C
C	   (I*4)  ILAST   = LAST  NON-BLANK CHARCTER IN FILENAME
C
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
C
C NOTE:    INPUT TEMPERATURES 'TINE()' ARE CONVERTED TO KELVIN 'TOA()'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
CA UNIX PORT - THOSE ROUTINES MARKED WITH 'CX' ARE NOW REPLACED BY NEW
CA 		ROUTINES COMMUNICATING WITH IDL SCREENS INSTEAD
CX         B1SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
CA         B1SPF0     ADAS      GATHERS INPUT FILE NAMES VIA IDL SCREENS
CX         B1ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
CA         B1ISPF     ADAS      GATHERS USERS VALUES VIA IDL SCREENS
C          B1TRAN     ADAS      GATHERS SELECTED TANSITION PARAMETERS
C          B1SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
CX         B1OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
CA         B1OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING IDL
C          B1OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          B1RATE     ADAS      CALCULATES RATE COEFFICIENTS
CX         BXSETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
CA         BXSETP     ADAS      PASSES VARIABLES BACK TO MAIN IDL ROUTINES
C          BXDATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          BXCSTR     ADAS      EXTRACT NON-BLANK CHARS. FROM STRING
C          BXTTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO KELVIN
C	   XXSLEN     ADAS      FINDS POSITIONS OF SPACES IN STRING
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION: CONVERT TEMP. TO KELVIN
CA	   B1SPF1     IDL-ADAS  OUTPUT/DISPLAY OPTIONS SELECTION SCREEN
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    12/10/90
C
C UPDATE:  11/12/90 - PE BRIDEN: ADAS91 - REPLACED 'XXOPEN' WITH FORTRAN
C                                         'OPEN' STATEMENT (AS 'DSFULL'
C                                         IS IN APPROPRIATE FORM).
C
C UPDATE:  08/01/91 - P.E.BRIDEN: ONLY CALCULATED (DE-)EXCITATION  RATES
C                                 FOR   ISPF   PANEL  INPUT  VALUES   IF
C                                 REQUESTED. (PLACED CALL TO 'B1RATE'
C                                 FOR CALCULATING 'SPLINE INTERPOLATED
C                                 VALUES' BEFORE THAT FOR THE 'ISPF
C                                 PANEL INPUT VALUES.)
C
C                                 ADDED 'LFSEL' & 'LOSEL' AS  THIRD  AND
C                                 FOURTH ARGUMENTS WHEN CALLING 'B1OUT0'
C
C UPDATE:  11/01/91 - P.E.BRIDEN: ADAS91- NAG  SPLINE CURVE FITTING  AND
C                                         INTERPOLATION SECTION REPLACED
C                                         BY CALL TO SUBROUTINE B1RATE.
C
C UPDATE:  17/01/91 - P.E.BRIDEN: ADAS91- RENAMED 'XXOUTR' -> 'XXOUT0'
C
C UPDATE:  21/01/91 - P.E.BRIDEN: ADAS91- INTRODUCED  'TSCEF(,)'  PASSED
C                                         AS ARGUMENT TO 'B1ISPF'  ALONG
C                                         WITH 'NV' AND 'NDTEM'.
C                                       - 'ITTYP' RENAMED 'IFOUT'
C
C UPDATE:  23/01/91 - P.E.BRIDEN: ADAS91- INTRODUCED  'LTRNG()'
C
C UPDATE:  28/01/91 - P.E.BRIDEN: ADAS91- INTRODUCED  'R8TCON'
C
C UPDATE:  20/02/91 - P.E.BRIDEN: ADAS91- REMOVED IBM 'ERRSET' ROUTINE
C
C UPDATE:  11/06/91 - P.E.BRIDEN: ADAS91- 'LDOUT' REMOVED - NOT USED
C
C UPDATE:  22/10/92 - PE BRIDEN: ADAS91 - CHANGED NDLEV FROM  30 ->  110
C                                         CHANGED NDTRN FROM 300 -> 2100
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  21/02/95 - PE BRIDEN - ADAS91: VERSION NUMBER CHANGED FROM
C                                         1.0 to 1.1
C                                         CHANGES MADE TO ADAS201 SO
C                                         THAT IT NOW USES BXDATA IN
C                                         INSTEAD OF B1DATA.
C                                         CHANGES INCLUDED:
C                                         - REPLACE B1DATA WITH BXDATA
C                                         - REPLACE B1SETP WITH BXSETP
C                                         - ADD CALLS TO BXCSTR & BXTTYP
C                                         - INCREASED NDTEM PARAMETER
C                                           FROM 8 TO 14.
C                                         - CHANGE SIZE OF CSTRGA FROM
C                                           12 TO 18 BYTES.
C                                         - ADDED A NUMBER OF NEW
C                                           VARIABLES: MAXLEV, TCODE(),
C                                           CSTRGB(), ICSTMX, ICNT?,
C                                           I?TRN(), IE1A(), IE2A(),
C                                           IP1A(), IP2A()
C                                           NOTE: A NUMBER OF THESE
C                                           VARIABLES ARE NOT ACTUALLY
C                                           USED BY THE PROGRAM IN ITS
C                                           CALCULATIONS.
C                                         - ONE ARGUMENT TO B1TRAN WAS
C                                           CHANGED: ISEL->IETRN(ISEL)
C                                         - ONE ARGUMENT TO B1OUT0 WAS
C                                           CHANGED: CSTRGA->CSTRGB
C
C UNIX-IDL PORT:
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    14/03/95
C
C VERSION: 1.1				     DATE: 14/03/95
C MODIFIED: LALIT JALOTA
C	    - FIRST VERSION
C
C	????????????
C 
C VERSION: 1.4				     DATE: 04/06/96
C MODIFIED: WILLIAM OSBORN
C	    VERSION INFO PUT INTO STANDARD STYLE AND
C	    MENU BUTTON CODE ADDED
C
C VERSION: 1.5                               DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN
C           - ADDED LREP AND OPEN07 TO DEAL WITH PAPER.TXT OUTPUT
C             PROPERLY
C
C VERSION:	1.6						DATE: 21-10-99
C MODIFIED: RICHARD MARTIN
C		- INITIALISED STRINGS TITLE, TITLX, TITLM
C
C VERSION:	1.7						DATE: 21-11-02
C MODIFIED: RICHARD MARTIN
C		- INCREASED NDLEV TO 300 & NDTRN TO 8000
C
C VERSION: 1.8                               DATE: 29-04-03
C MODIFIED: Martin O'Mullane
C		- Use xxdata_04 to read adf04 file. This requires
C                 new arrays some of which are not used in the 
C                 population calculation.
C               - bxttyp parameter list extended. original istrn replaced
C                 by isel.
C		- Increase NDLEV to 1000 and NDTRN to 15000
C		- Increased format 1001 from I2 to I4 to allow for 
C   		  more levels.
C               - AA replaced with AASEL in b1tran and b1out0.
C
C-----------------------------------------------------------------------
      INTEGER     NDLEV        , NDTRN        , NDTEM     , NDTIN      ,
     &            MAXDEG
      INTEGER     NDQDN        , NDMET
      INTEGER     IUNT07       , IUNT10       , PIPEIN
      INTEGER     NPSPL        , NMX
      INTEGER     L1           , ICSTMX	      , IFIRST    , ILAST
C-----------------------------------------------------------------------
      LOGICAL     LMLOG
C-----------------------------------------------------------------------
      PARAMETER ( NDLEV  =1000 , NDTRN  =15000 , NDTEM = 14 ,
     &            NDTIN  =  20 , MAXDEG =  19 )
      PARAMETER ( NDQDN  = 6   , NDMET  = 5   )
      PARAMETER ( IUNT07 =   7 , IUNT10 =  10 )
      PARAMETER ( NPSPL  = 100 , NMX    = 100 )
      PARAMETER ( L1     =   1 , ICSTMX =  11 )
      PARAMETER ( PIPEIN = 5)
C-----------------------------------------------------------------------
      PARAMETER ( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER     I4UNIT       , ISTOP
      INTEGER     IZ           , IZ0          , IZ1       ,
     &            IL           , ITRAN        , ISEL      , MAXLEV     ,
     &            ICNTE        , ICNTP        , ICNTR     , ICNTH      ,
     &            NV           , MAXT         , KPLUS1    ,
     &            IFOUT        , IPAN         , IFORM     ,
     &            IUPPER       , ILOWER       , LUPPER    , LLOWER
C-----------------------------------------------------------------------
      REAL*8      R8TCON
      REAL*8      EUPPER       , ELOWER       , WUPPER    , WLOWER     ,
     &            BWNO         , AASEL
      REAL*8      XMIN         , XMAX         , XMINK     , XMAXK      ,
     &            YMIN         , YMAX
      REAL*8      TOLVAL
C-----------------------------------------------------------------------
      LOGICAL     OPEN10       , LGHOST       , LPEND     , LOSEL      ,
     &            LFSEL      
CA - UNIX PORT THESE VARIABLES ADDED FOR OUTPUT SELECTION OPTIONS
      LOGICAL     LGRAPH       , L2FILE       , LREP      , OPEN07
      LOGICAL     LDSEL        , LGRD1        , LDEF1
C-----------------------------------------------------------------------
CX    CHARACTER   REP*3        , DATE*8       , DSFULL*44 ,
CX   &            TITLED*3     , TITLE*40     , TITLM*80  , TITLX*80
      CHARACTER   REP*3        , DATE*8       , DSFULL*80 ,
     &            TITLED*3     , TITLE*40     , TITLM*80  , TITLX*120
CA - UNIX PORT : OUTPUT TEST FILENAME OBTAINED FROM IDL SCREEN
      CHARACTER   SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER     IA(NDLEV)    , ISA(NDLEV)   , ILA(NDLEV)  ,
     &            I1A(NDTRN)   , I2A(NDTRN)
      INTEGER     IETRN(NDTRN) , IPTRN(NDTRN) ,
     &            IRTRN(NDTRN) , IHTRN(NDTRN) ,
     &            IE1A(NDTRN)  , IE2A(NDTRN)  ,
     &            IP1A(NDTRN)  , IP2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8      XJA(NDLEV)   , WA(NDLEV)    ,
     &            AVAL(NDTRN)
      REAL*8      COEF(MAXDEG+1)              ,
     &            SCEF(NDTEM)  , GAMMA(NDTEM) ,
     &            RATE(NDTEM)  , DRATE(NDTEM)
      REAL*8      TINE(NDTIN)  ,
     &            TOA(NDTIN)   , TOSA(NPSPL)  , TOMA(NMX)   ,
     &            GAMOA(NDTIN) , GAMOSA(NPSPL), GAMOMA(NMX) ,
     &            ROA(NDTIN)   , ROSA(NPSPL)  , ROMA(NMX)   ,
     &            DROA(NDTIN)  , DROSA(NPSPL) , DROMA(NMX)
      REAL*8      TSCEF(NDTEM,3)      , SCOM(NDTEM,NDTRN)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(NDTIN)
C-----------------------------------------------------------------------
      CHARACTER   TCODE(NDTRN)*1 , CSTRGA(NDLEV)*18 , CSTRGB(NDLEV)*18 ,
     &            STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , MAXT   /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./, OPEN07 /.FALSE./
      DATA XMIN   /0/       , XMAX   /0/
C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   npl           , iadftyp       , itieactn , iorb    
      integer   nplr          , npli          , icnti    , icntl   , 
     &          icnts
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      integer   ipla(ndmet,ndlev)             , npla(ndlev)
      integer   iitrn(ndtrn)                  , iltrn(ndtrn)       , 
     &          istrn(ndtrn)                  ,                    
     &          ia1a(ndtrn)                   , ia2a(ndtrn)        , 
     &          il1a(ndlev)                   , il2a(ndlev)        , 
     &          is1a(ndlev)                   , is2a(ndlev)        
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet) , aa(ndtrn)          ,
     &          zpla(ndmet,ndlev)             , beth(ndtrn)        ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
      real*8    auga(ndtrn)                   , wvla(ndlev)
C----------------------------------------------------------------------
      character cpla(ndlev)*1                 , cprta(ndmet)*9      
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  , lbseta(ndmet)      ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lss04a(ndlev,ndmet)
C----------------------------------------------------------------------

                


C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000

	TITLE='NULL'
	TITLM='NULL'
	TITLX='NULL'	

C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
CA - UNIX PORT : DATE NOW GATHERED VIA IDL
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
CA - UNIX PORT : IPAN NO LONGER USED
CX 100 IPAN = 0
100   CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10=.FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
CX GET INPUT DATA SET NAME (FROM ISPF PANELS)
CA - UNIX PORT : FILENAME NOW GATHERED FROM IDL INTERFACE
CA
C-----------------------------------------------------------------------
C
      CALL B1SPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CA - UNIX PORT : XXENDG NO LONGER USED
CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CA UNIX PORT - REMOVE IBM SPECIFIC KEYWORD
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='UNKNOWN')
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CA UNIX PORT - THIS FUNCTION CARRIED OUT VIA IDL INTERFACE 
C-----------------------------------------------------------------------
CX       IF (LDSEL) THEN
CX          CALL XXTEXT(IUNT10)
CX          GOTO 100
CX       ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------

       itieactn = 0
       
       call xxdata_04( iunt10 , 
     &                 ndlev  , ndtrn , ndmet   , ndqdn , ndtem ,
     &                 titled , iz    , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,     
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )

C-----------------------------------------------------------------------
C REDUCED LEVEL NOMENCLATURE STRINGS TO LAST 'ICSTMX' NON-BLANK BYTES
C-----------------------------------------------------------------------

      CALL BXCSTR( CSTRGA , IL     , ICSTMX ,
     &             CSTRGB
     &           )

C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPES.
C-----------------------------------------------------------------------

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )

C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------

         DO 1 IFORM=1,3
            CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
    1    CONTINUE

C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------

      CALL BXSETP( IZ0    , IZ    ,
     &             NDLEV  , IL    , ICNTE ,
     &             CSTRGB , ISA   , ILA   , XJA  ,
     &             STRGA
     &           )

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------

  200 CONTINUE
      CALL B1ISPF( IPAN   , LPEND ,
     &             NDTIN  , IL    ,
     &             NDTEM  , NV    , TSCEF ,
     &             ICNTE  , IE1A  , IE2A  , STRGA ,
     &             TITLE  ,
     &             ISEL   , IFOUT ,
     &             MAXT   , TINE  ,
     &             LFSEL  , LOSEL , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX  , YMIN  , YMAX
     &           )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100

C-----------------------------------------------------------------------
C IDENTIFY REQUESTED TRANSITION PARAMETERS
C-----------------------------------------------------------------------

      CALL B1TRAN( NDLEV  , NDTRN         , NDTEM ,
     &             IL     , IETRN(ISEL)   , NV    ,
     &             IA     , WA            , XJA   ,
     &             I1A    , I2A           , AA    , SCOM  ,
     &             IUPPER , ILOWER        ,
     &             LUPPER , LLOWER        ,
     &             WUPPER , WLOWER        ,
     &             EUPPER , ELOWER        ,
     &             AASEL  , GAMMA
     &           )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TINE -> TOA)
C-----------------------------------------------------------------------

      CALL XXTCON( IFOUT , L1 , IZ1 , MAXT , TINE , TOA   )

C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO KELVIN.
C-----------------------------------------------------------------------

      XMINK = R8TCON( IFOUT , L1 , IZ1 , XMIN )
      XMAXK = R8TCON( IFOUT , L1 , IZ1 , XMAX )

C-----------------------------------------------------------------------
C FIT CUBIC SPLINE AND INTERPOLATE GAMMAS FOR GRAPHICAL & TABULAR OUTPUT
C-----------------------------------------------------------------------

      CALL B1SPLN(         LOSEL ,
     &             NV    , MAXT  , NPSPL  ,
     &             SCEF  , TOA   , TOSA   ,
     &             GAMMA , GAMOA , GAMOSA ,
     &             LTRNG
     &           )

C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED
C-----------------------------------------------------------------------

         IF (LFSEL) THEN
            CALL XXMNMX( LMLOG  , MAXDEG , TOLVAL ,
     &                   NV     , SCEF   , GAMMA  ,
     &                   NMX    , TOMA   , GAMOMA ,
     &                   KPLUS1 , COEF   ,
     &                   TITLM
     &                 )
	 ENDIF

C-----------------------------------------------------------------------
C CONVERT GAMMA VALUES INTO (DE-)EXCITATION RATE COEFFICIENTS
C-----------------------------------------------------------------------
C
C 1) ORIGINAL <se>LIKE.DATA VALUES
C
      CALL B1RATE(  NV   , SCEF   , GAMMA  ,
     &                     EUPPER , ELOWER ,
     &                     WUPPER , WLOWER ,
     &                     RATE   , DRATE
     &           )
C
C 2) SPLINE INTERPOLATED VALUES
C
      CALL B1RATE( NPSPL , TOSA   , GAMOSA ,
     &                     EUPPER , ELOWER ,
     &                     WUPPER , WLOWER ,
     &                     ROSA   , DROSA
     &           )
C
C 3) ISPF PANEL INPUT VALUES (IF REQUESTED)
C
         IF (LOSEL) THEN
            CALL B1RATE( MAXT  , TOA    , GAMOA  ,
     &                           EUPPER , ELOWER ,
     &                           WUPPER , WLOWER ,
     &                           ROA    , DROA
     &                 )
         ENDIF
C
C 4) MINIMAX VALUES (IF REQUESTED)
C
         IF (LFSEL) THEN
            CALL B1RATE( NMX   , TOMA   , GAMOMA ,
     &                           EUPPER , ELOWER ,
     &                           WUPPER , WLOWER ,
     &                           ROMA   , DROMA
     &                 )
         ENDIF

C-----------------------------------------------------------------------
C  CONSTRUCT DESCRIPTIVE TITLE FOR OUTPUT
C-----------------------------------------------------------------------

      CALL XXSLEN(DSFULL, IFIRST, ILAST)
      WRITE (TITLX,1001) DSFULL(IFIRST:ILAST),TITLED,IZ,LLOWER,LUPPER

C-----------------------------------------------------------------------
C  GENERATE GRAPH
CA  UNIX PORT - 
CA ADDED THE NEW CALL TO B1SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CA A PIPE TO GET USER SELECTED OUTPUT OPTIONS. NOTE THAT IF USER SELECTS
CA GRAPH X-AXIS RANGES THESE ARE ASSUMED TO BE IN KELVIN SO NO FURTHER
CA CONVERSION IS CARRIED OUT.
C-----------------------------------------------------------------------

300   CONTINUE
      CALL B1SPF1( DSFULL        , ISEL          , LFSEL    , LDEF1,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMINK         , XMAXK         , YMIN     , YMAX   ,
     &             LPEND         , LREP
     &           )


C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 200


C----------------------------------------------------------------------
C OUTPUT RESULTS - MOVED BEFORE GRAPHING SO THAT PAPER OUTPUT IS
C ACHIEVED EVEN WHEN MENU BUTTON IS PRESSED ON GRAPH SCREEN
C----------------------------------------------------------------------

      IF (L2FILE) THEN
         IF(LREP.AND.OPEN07)THEN
            CLOSE(IUNT07)
            OPEN07 = .FALSE.
         ENDIF
         IF(.NOT.OPEN07)THEN
            OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
            OPEN07 = .TRUE.
         ENDIF
         CALL B1OUT0( IUNT07   , IL    , LFSEL , LOSEL ,
     &                TITLE    , TITLX , TITLM , DATE  ,
     &                IZ0      , IZ1   , IZ    , BWNO  ,
     &                MAXT     , LTRNG ,
     &                TOA      , GAMOA , DROA  , ROA   ,
     &                AASEL    , LUPPER, LLOWER, EUPPER, ELOWER,
     &                IUPPER   , ILOWER,
     &                CSTRGB   , ISA   , ILA   , XJA   ,
     &                KPLUS1   , COEF
     &              )
      ENDIF

C----------------------------------------------------------------------
C  IF SELECTED - GENERATE GRAPH
CA UNIX PORT - E1OUTG COMMUNICATES WITH IDL VIA PIPE.
C----------------------------------------------------------------------
      IF (LGRAPH) THEN
         CALL B1OUTG( LGHOST   ,
     &                TITLE    , TITLX , TITLM , DATE ,
     &                SCEF     , RATE  , NV    ,
     &                TOMA     , ROMA  , NMX   ,
     &                TOSA     , ROSA  , NPSPL ,
     &                LGRD1    , LDEF1 , LFSEL ,
     &                XMINK    , XMAXK , YMIN  , YMAX
     &              )


C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 9999

      ENDIF

C-----------------------------------------------------------------------
C  RETURN TO ISPF PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------

      GOTO 300

C-----------------------------------------------------------------------

 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
 1001 FORMAT((1A),'  ELEMENT: ',A3,'(Z=',I2,')',
     &       '  TRANSITION: Q (',I4,'->',I4,')')

C-----------------------------------------------------------------------

 9999 CONTINUE

      IF(OPEN07) CLOSE(IUNT07)

      END
