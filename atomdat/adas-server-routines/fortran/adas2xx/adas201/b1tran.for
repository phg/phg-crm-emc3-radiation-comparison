CX UNIX PORT SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas201/b1tran.for,v 1.1 2004/07/06 11:19:03 whitefor Exp $ Date $Date: 2004/07/06 11:19:03 $
CX
       SUBROUTINE B1TRAN( NDLEV  , NDTRN  , NDTEM ,
     &                    IL     , ISTRN  , NV    ,
     &                    IA     , WA     , XJA   ,
     &                    I1A    , I2A    , AVAL  , SCOM ,
     &                    IUPPER , ILOWER ,
     &                    LUPPER , LLOWER ,
     &                    WUPPER , WLOWER ,
     &                    EUPPER , ELOWER ,
     &                    AA     , GAMMA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B1TRAN *********************
C
C  PURPOSE:  TO SET UP SELECTED TRANSITION PARAMETERS.
C
C  CALLING PROGRAM: ADAS201
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF INDEX LEVELS
C  INPUT : (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF INPUT FILE TEMPERATURES
C
C  INPUT : (I*4)  IL      = NUMBER OF INDEX LEVELS
C  INPUT : (I*4)  ISTRN   = SELECTED TRANSITION INDEX.
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C  INPUT : (I*4)  IA()    = LEVEL INDEX NUMBER ARRAY
C  INPUT : (R*8)  WA()    = LEVEL ENERGIES RELATIVE TO LEVEL 1 (CM-1)
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  INPUT : (I*4)  I1A()   = LOWER LEVEL INDEX FOR ELECTRON IMPACT
C                           TRANSITION
C  INPUT : (I*4)  I2A()   = UPPER LEVEL INDEX FOR ELECTRON IMPACT
C                           TRANSITION
C  INPUT : (I*4)  AVAL()  = A-VALUE FOR ELECTRON IMPACT TRANSITION
C  INPUT : (I*4)  SCOM(,) = GAMMA VALUES FOR ELECTRON IMPACT TRANSITION
C                            1st DIMENSION: TEMPERATURE INDEX
C                            2nd DIMENSION: TRANSITION INDEX
C
C  OUTPUT: (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C  OUTPUT: (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C
C
C  OUTPUT: (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C  OUTPUT: (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C  OUTPUT: (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C  OUTPUT: (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C
C  OUTPUT: (R*8)  EUPPER  = SELECTED TRANSITION: UPPER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C  OUTPUT: (R*8)  ELOWER  = SELECTED TRANSITION: LOWER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C
C  OUTPUT: (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GAMMA VALUE AT 'TEMP()'
C  OUTPUT: (R*8)  AA      = SELECTED TRANSITION A-VALUE (SEC-1)
C
C          (I*4)  I       = GENERAL USE.
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER  NDLEV  , NDTRN  , NDTEM ,
     &         IL     , ISTRN  , NV    ,
     &         IUPPER , ILOWER ,
     &         LUPPER , LLOWER
      INTEGER  I
C-----------------------------------------------------------------------
      REAL*8   WUPPER , WLOWER ,
     &         EUPPER , ELOWER ,
     &         AA
C-----------------------------------------------------------------------
      INTEGER  IA(NDLEV)   ,
     &         I1A(NDTRN)  , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8   WA(NDLEV)   , XJA(NDLEV)   ,
     &         AVAL(NDTRN) , GAMMA(NDTEM) , SCOM(NDTEM,NDTRN)
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UPPER AND LOWER ENERGY LEVEL INDEX SPECIFICATIONS
C-----------------------------------------------------------------------
C
      LLOWER=I1A(ISTRN)
      LUPPER=I2A(ISTRN)
         DO 1 I=1,IL
           IF ( IA(I).EQ.LLOWER ) ILOWER=I
           IF ( IA(I).EQ.LUPPER ) IUPPER=I
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP REQUIRED A-VALUE AND GAMMA COEFFICIENTS.
C-----------------------------------------------------------------------
C
      AA   = AVAL(ISTRN)
         DO 2 I=1,NV
            GAMMA(I) = SCOM(I,ISTRN)
    2    CONTINUE
C
C-----------------------------------------------------------------------
C ASSIGN INDEX LEVEL ENERGIES AND STATISTICAL WEIGHTS.
C-----------------------------------------------------------------------
C
      ELOWER=WA(ILOWER)
      EUPPER=WA(IUPPER)
      WLOWER=(2.0*XJA(ILOWER))+1.0
      WUPPER=(2.0*XJA(IUPPER))+1.0
C-----------------------------------------------------------------------
      RETURN
      END
