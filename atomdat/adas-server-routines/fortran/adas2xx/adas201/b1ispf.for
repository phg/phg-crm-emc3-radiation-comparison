CA  UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas201/b1ispf.for,v 1.1 2004/07/06 11:17:50 whitefor Exp $ Date $Date: 2004/07/06 11:17:50 $
CA
      SUBROUTINE B1ISPF( IPAN   , LPEND  ,
     &                   NDTIN  , IL     ,
     &                   NDTEM  , NV     , TSCEF ,
     &                   ITRAN  , I1A    , I2A   , STRGA ,
     &                   TITLE  ,
     &                   ISTRN  , IFOUT  ,
     &                   MAXT   , TINE   ,
     &                   LFSEL  , LOSEL  , LGRD1 , LDEF1 ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1   , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B1ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL AND TO RETURN USER SELECTED 
C           OPTIONS AND VALUES.
C
C  CALLING PROGRAM: ADAS201
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF TEMPERATURES ALLOWED
C
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NDTEM    = INPUT DATA FILE: MAX. NO. OF TEMPERATURES
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER   OF TEMPERATURES
C
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  INPUT : (I*4)   ITRAN    = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (I*4)   I1A()    = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)   I2A()    = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C  INPUT : (C*22)  STRGA()  = LEVEL DESIGNATIONS
C
C  OUTPUT: (C*40)  TITLE    = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   ISTRN    = SELECTED ELECTRON IMPACT TRANSITION INDEX
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT.
C                           = .FALSE. => - DO NOT DO THE ABOVE -
CA UNIX PORT - LGRD1 ONLY USED TO KEEP ARGUMENT LIST THE SAME
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
CA UNIX PORT - AXES SCALING CHOSEN LATER UNDER IDL-ADAS
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C 	   (I*4)   ILOGIC   = RETURN VALUE FROM IDL WHICH IS USED TO 
C			      REPRESENT A LOGICAL VARIABLE SINCE IDL DOES
C			      HAVE SUCH DATA TYPES.
C	   
C	   (I*4)   I	    = GENERAL PURPOSE COUNTER
C
C	   (I*4)   J	    = GENERAL PURPOSE COUNTER
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS 'FLUSH' TO CLEAR PIPE
C
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    8/3/95
C
C-----------------------------------------------------------------------
      INTEGER    ILEN
      INTEGER    IPAN       ,
     &           NDTIN      , IL         , NDTEM     , NV    ,
     &           ITRAN      , ISTRN      , IFOUT     , MAXT
      INTEGER    IABT       , IPANRC
C-----------------------------------------------------------------------
      REAL*8     TOLVAL     ,
     &           XL1        , XU1        , YL1       , YU1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      LOGICAL    LPEND
      LOGICAL    LFSEL      , LOSEL      , LGRD1     , LDEF1
C-----------------------------------------------------------------------
      INTEGER    I1A(ITRAN) , I2A(ITRAN)
C-----------------------------------------------------------------------
      REAL*8     TSCEF(NDTEM,3)         , TINE(NDTIN)
C-----------------------------------------------------------------------
      CHARACTER  STRGA(IL)*22
C-----------------------------------------------------------------------
      INTEGER    PIPEOU     , PIPEIN
      PARAMETER  (PIPEOU=6  , PIPEIN=5)
C-----------------------------------------------------------------------
      INTEGER    I          , J         , ILOGIC
      CHARACTER*80 TEMP
C-----------------------------------------------------------------------
C  WRITE VARIABLES OUT TO IDL VIA PIPE
C-----------------------------------------------------------------------
      WRITE(PIPEOU, *) NDTIN
      WRITE(PIPEOU, *) IL
      WRITE(PIPEOU, *) NDTEM
      WRITE(PIPEOU,*) NV
      WRITE(PIPEOU, *) ITRAN
      CALL XXFLSH(PIPEOU)
      DO J = 1, 3
	 DO I = 1, NDTEM
            WRITE(PIPEOU, *) TSCEF(I,J)
         ENDDO
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO I = 1, ITRAN
         WRITE(PIPEOU, *) I1A(I)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO I = 1, ITRAN
         WRITE(PIPEOU, *) I2A(I)
      ENDDO
      CALL XXFLSH(PIPEOU)
C-----------------------------------------------------------------------
C     READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
      READ(PIPEIN, *) ILOGIC
      IF (ILOGIC .EQ. 1 ) THEN
         LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      END IF
      READ(PIPEIN,'(A)') TITLE
      READ(PIPEIN,*) ISTRN
C UNIX PORT - CHANGE IDL INDEX TO FORTRAN INDEX 
      ISTRN = ISTRN + 1
      READ(PIPEIN,*) IFOUT
      READ(PIPEIN,*) MAXT
      READ(PIPEIN,*) (TINE(I), I=1, MAXT)
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. 1) THEN 
	 LFSEL = .TRUE.
         READ(PIPEIN,*) TOLVAL
         TOLVAL = TOLVAL /100.0
      ELSE
         LFSEL = .FALSE.
      ENDIF
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. 1) THEN 
         LOSEL = .TRUE.
      ELSE 
         LOSEL = .FALSE.
      ENDIF
C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
      END
