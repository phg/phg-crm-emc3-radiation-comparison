CX UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas201/b1outg.for,v 1.2 2004/07/06 11:18:13 whitefor Exp $ Date $Date: 2004/07/06 11:18:13 $
CX
       SUBROUTINE B1OUTG( LGHOST ,
     &                    TITLE  , TITLX , TITLM , DATE ,
     &                    TEMP   , RATE  , NENER ,
     &                    TOMA   , ROMA  , NMX   ,
     &                    TOSA   , ROSA  , NPSPL ,
     &                    LGRD1  , LDEF1 , LFSEL ,
     &                    XMIN   , XMAX  , YMIN  , YMAX
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B1OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED TRANSITION USING IDL.
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                ORIGINAL <SE>LIKE.DATA    (CROSSES    )
C                                SPLINE INTERPOLATED DATA  (FULL CURVE)
C                                MINIMAX FIT TO DATA       (DASH CURVE )
C
C            PLOT IS LOG10(RATE(CM**3/SEC))  VERSUS LOG10(TEMP(KELVIN))
C
C  CALLING PROGRAM: ADAS201
C
C  SUBROUTINE:
C
CA UNIX PORT - LGHOST USED TO KEEP ARGUMENT LIST THE SAME.
CA
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  TEMP()  = INPUT DATA FILE: TEMPERATURES (kelvin)
C  INPUT : (R*8)  RATE()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           EXCITATION RATE COEFF. (cm**3/s) AT 'TEMP()'
C  INPUT : (I*4)  NENER   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C  INPUT : (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (kelvin)
C  INPUT : (R*8)  ROMA()  = EXCITATION RATE COEFF.(cm**3/s) AT 'TOMA()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX  GENERATED   GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (kelvin)
C  INPUT : (R*8)  ROSA()  = EXCITATION RATE COEFF.(cm**3/s) AT 'TOSA()'
C  INPUT : (I*4)  NPSPL   = NUMBER  OF SPLINE INTERPOLATED GAMMA/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
CA UNIX PORT - LGRD1 USED ONLY TO KEEP ARGUMENT LIST THE SAME.
CA
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (K)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (K)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE COEFF. (cm**3/s)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE COEFF. (cm**3/s)
C
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C          (I*4)  IKEY    = NUMBER OF 'KEY()' VALUES TO BE OUTPUT
C          (I*4)  ICOUNT  = NUMBER OF POINTS PLOTTED FOR GRAPH CURVE
C
C          (C*12) DNAME   = '      DATE: '
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*22) KEY()   = DESCRIPTIVE KEY FOR GRAPH (3 TYPES)
C
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*7)  C7      =  7 BYTE STRING = 'TITLX(1:4)'//'C3BLNK'
C	   (I*4)  PIPEOU  = PARAMETER : UNIT NUMBER OF PIPE
C	   (I*4)  ONE	  = PARAMETER : VARIABLE USED AS LOGICAL FOR IDL
C	   (I*4)  ZERO    = PARAMETER : VARIABLE USED AS LOGICAL FOR IDL
C	   (I*4)  IFIRST  = POSITION OF FIRST NO-BLNK CHARACTER IN STRING
C	   (I*4)  ILAST   = POSITION OF LAST  NO-BLNK CHARACTER IN STRING
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C	   XXSLEN      ADAS      GET POSITION OF NON-BLANK CHARACTERS.
C
C AUTHOR:  L. JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    08/03/95
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       INTEGER I4UNIT
       INTEGER NENER      , NMX         , NPSPL    ,
     &                      I           , IKEY     , ICOUNT  , 
     &			    IFIRST      , ILAST    ,
     &                      PIPEOU      , ONE      ,  ZERO
	PARAMETER (PIPEOU = 6, ONE = 1, ZERO = 0)
C-----------------------------------------------------------------------
       REAL*8 TEMP(NENER) , RATE(NENER) ,
     &        TOMA(NMX)   , ROMA(NMX)   ,
     &        TOSA(NPSPL) , ROSA(NPSPL) ,
     &        XMIN        , XMAX        ,
     &        YMIN        , YMAX
C-----------------------------------------------------------------------
       CHARACTER TITLE*40 , TITLX*120   , TITLM*80  , DATE*8
       CHARACTER DNAME*12 , 
     &           MNMX0*9  , KEY0*9      , ADAS0*8   , KEY(3)*22
       CHARACTER GRID*1   , PIC*1       , C3BLNK*3  , C7*7
C-----------------------------------------------------------------------
       LOGICAL   LGHOST   , LGRD1       , LDEF1     , LFSEL
C-----------------------------------------------------------------------
       DATA DNAME/'      DATE: '/
       DATA ADAS0/'ADAS   :'/
     &      MNMX0/'MINIMAX: '/,
     &      KEY0 /'KEY    : '/,
     &      KEY(1)/'(CROSSES - INPUT DATA)'/,
     &      KEY(2)/' (FULL LINE - SPLINE) '/,
     &      KEY(3)/'(DASH LINE - MINIMAX) '/
       DATA GRID  /' '/   ,
     &      PIC   /' '/   ,
     &      C3BLNK/'   '/ 
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C WRITE OUT TITLES AND DATE INFORMATION
      CALL XXSLEN(TITLX, IFIRST, ILAST)
      WRITE(PIPEOU,'(A120)') TITLX(IFIRST:ILAST)
      WRITE(PIPEOU,'(A80)') TITLM
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
C
C WRITE OUT ARRAY SIZES
      WRITE(PIPEOU,*) NENER
      WRITE(PIPEOU,*) NPSPL
      CALL XXFLSH(PIPEOU)
C
C DATA FROM DATA FILE
      DO 1 I = 1, NENER 
         WRITE(PIPEOU,*) TEMP(I)
 1    CONTINUE
      CALL XXFLSH(PIPEOU)
      DO 2 I = 1, NENER
         WRITE(PIPEOU,*) RATE(I)
 2    CONTINUE
      CALL XXFLSH(PIPEOU)
C
C WRITE OUT INTERPOLATED SPLINE VALUES
	 WRITE(PIPEOU,*) ONE
         DO 3 I = 1, NPSPL
            WRITE(PIPEOU,*) TOSA(I)
 3       CONTINUE
         CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NPSPL
            WRITE(PIPEOU,*) ROSA(I)
 4       CONTINUE
         CALL XXFLSH(PIPEOU)
C
C WRITE OUT USER SELECTED AXES RANGES.
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
      ELSE
         WRITE(PIPEOU,*) ONE
	 WRITE(PIPEOU,*) XMIN
	 WRITE(PIPEOU,*) XMAX
	 WRITE(PIPEOU,*) YMIN
	 WRITE(PIPEOU,*) YMAX
      ENDIF
      CALL XXFLSH(PIPEOU)
C
C IF SELECTED WRITE OUT MINMAX FIT VALUES
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO 5 I = 1, NMX
	    WRITE(PIPEOU,*) TOMA(I)
 5       CONTINUE
	 CALL XXFLSH(PIPEOU)
         DO 6 I = 1, NMX
	    WRITE(PIPEOU,*) ROMA(I)
 6       CONTINUE
	 CALL XXFLSH(PIPEOU)
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
      END
