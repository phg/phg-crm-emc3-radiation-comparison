CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas209/b9pars.for,v 1.4 2010/04/09 13:54:33 mog Exp $ Date $Date: 2010/04/09 13:54:33 $
CX

       SUBROUTINE B9PARS(NDMET,STRING,NPT,BWNOA,LSETA,
     &                   PRTWTA,CPRTA,IFAIL,ITYPE)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B9PARS *********************
C
C  PURPOSE:  TO ANALYSE THE TAIL CHARACTER STRING OF THE FIRST LINE OF
C            A SPECIFIC ION FILE INTO BINDING WAVE NUMBERS FOR DIFFERENT
C            PARENTS AND STATISTICAL WEIGHTS FOR THE PARENTS.
C            MODIFICATION OF B8PARS.
C
C  CALLING PROGRAM: ADAS209
C
C  NOTES: DETECT  -  BINDING WAVE NUMBER WHICH PRECEED TERM ASSIGNATION
C                -   TERM CONTAINED IN '(..)'.
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDMET    =  MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (C*(*))STRING   =  STRING TO BE PARSED
C
C  OUTPUT: (I*4)  NPT      =  NUMBER OF BINDING WAVE NUMBERS DETECTED
C  OUTPUT: (L*4)  LSETA()  =  .TRUE.  - PARENT TERM SET FOR THIS W.NO.
C                             .FALSE. - PARENT TERM NOT SET FOR W.NO.
C  OUTPUT: (L*4)  LFND     =  .TRUE.  - L QUANTUM NUMBER PRESENT IN
C                                       STRING
C                             .FALSE. - NO L QUANTUM NUMBER DETECTED
C  OUTPUT: (R*8)  BWNOA()  =  BINDING WAVE NUMBERS
C  OUTPUT: (R*8)  PRTWTA() =  PARENT STATISTICAL WEIGHTS
C  OUTPUT: (C*9)  CPRTA()  =  PARENT NAME IN BRACKETS
C  OUTPUT: (I*4)  IFAIL    =  0 - SUBROUTINE CONCLUDES CORRECTLY
C                             1 - FAULT DETECTED IN SUBROUTINE
C                             2 - SINGLE IONISATION POTENTIAL DETECTED
C
C          (I*4)  MAXWRD   =  MAXIMUM NUMBER OF WORDS SOUGHT INITIALLY
C                             INITIALLY, FINALLY NUMBER ACTUALLY FOUND
C          (I*4)  NFIRST   =  FIRST WORD TO BE EXTRACTED FROM STRING
C          (I*4)  IFIRST() =  INDEX OF FIRST CHAR. OF WORD () IN STRING
C          (I*4)  ILAST()  =  INDEX OF LAST  CHAR. OF WORD () IN STRING
C          (I*4)  IWORDS   =  NUMBER OF WORDS FOUND IN STRING
C          (I*4)  IABT     =  FAILURE NUMBER FROM R8FCTN
C          (I*4)  NCHAR    =  NUMBER OF CHARACTERS IN SUBSTRING
C          (I*4)  I        =  GENERAL USE
C          (I*4)  J        =  GENERAL USE
C          (I*4)  K        =  GENERAL USE
C          (I*4)  IC       =  GENERAL USE
C OUTPUT:  (I*4)  ITYPE    =  RESOLUTION OF PARENT METASTABLES
C                             1 - LS RESOLVED
C                             2 - LSJ RESOLVED
C                             3 - ARBITRARY RESOLUTION
C          (I*4)  ITP      =  FLAG FOR INCOMPATIBLE TYPES
C          (I*4)  ITYP     =  COPY OF CURRENT ITYPE
C          (I*4)  KMRK     =  POSITION MARKER IN THE STRING FOR PARENT
C                             L QUANTUM NUMBER
C          (R*8)  TWTA()   =  (2L+1) VALUE FOR PARENT L QUANTUM NUMBER
C          (C*1)  CTRMA()  =  PARENT L QUANTUM NUMBER LETTER SET
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          I4FCTN     ADAS      CONVERTS FROM CHAR. TO INTEGER  VARIABLE
C          XXWORD     ADAS      PARSES A STRING INTO SEPARATE WORDS
C                               FOR ' ()<>{}' DELIMITERS
C
C AUTHOR:  HP SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    22/06/92
C
C UPDATE:   8/07/93 - HPS  ALTERED TO USE XXWORD PARSING ROUTINE
C
C UPDATE:  11/05/95 - HPS  ADD CPRTA TO PARAMETER LIST
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 27-06-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 13/11/95
C MODIFIED: DAVID BROOKS (UNIVERSITY OF STRATHCLYDE)
C               - EXTENDED ROUTINE TO HANDLE J/ARBITRARY
C                 RESOLVED PARENT METASTABLE INFORMATION
C                 IN THE FIRST DATA CARD STRING. INTRODUCED
C                 ITYPE TO FLAG RESOLUTION IF REQUIRED.
C
C VERSION: 1.3                          DATE: 26-01-96
C MODIFIED: DAVID BROOKS
C               - PASSED ITYPE FLAG OUT.
C
C VERSION : 1.4
C DATE    : 09-04-2010
C MODIFIED: Martin O'Mullane
C           - Change integer*4 to integer.
C
C-----------------------------------------------------------------------
       INTEGER   NDMET
C
       CHARACTER STRING*(*)   , SSTRNG*15  , CTRMA(9)*1
       CHARACTER CDELIM*7     , CPRTA(NDMET)*9
C
       INTEGER   NPT          , IABT       , IC        , I     , IFAIL
       INTEGER   NFIRST       , MAXWRD     , IWORDS    , ITYPE , ITP  
       INTEGER   IFIRST(10)   , ILAST(10)  , KMRK      , J     , K
       INTEGER   I4FCTN       , I4UNIT     , ITYP
C
       LOGICAL   LSETA(NDMET) , LFND
C
       REAL*8    BWNOA(NDMET) , PRTWTA(NDMET)          , TWTA(9)
       REAL*8    R8FCTN
C-----------------------------------------------------------------------
       DATA  CTRMA/ 'S' , 'P' , 'D' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K'/
       DATA  TWTA / 1.0 , 3.0 , 5.0 , 7.0 , 9.0 , 11.0, 13.0, 15.0,17.0/
       DATA CDELIM/' ()<>{}'/
C-----------------------------------------------------------------------
       NPT    = 0
       IFAIL  = 0
C
       DO 10 I =1,NDMET
        BWNOA(I)=0.0D0
        LSETA(I)=.FALSE.
        PRTWTA(I)=0.0D0
   10  CONTINUE
C
       NFIRST=1
       MAXWRD=2*NDMET
       CALL XXWORD(STRING,CDELIM,NFIRST,MAXWRD,IFIRST,ILAST,IWORDS)
C
       IF(IWORDS.EQ.0)THEN
           WRITE(I4UNIT(-1),1001)'NO IONISATION POTENTIAL'
           WRITE(I4UNIT(-1),1002)
           IFAIL = 1
           RETURN
       ELSEIF (IWORDS.EQ.1) THEN
           BWNOA(1)=R8FCTN(STRING(IFIRST(1):ILAST(1)),IABT)
           IF(IABT.GT.0)THEN
               WRITE(I4UNIT(-1),1001)'FAULT IN IONIS. POTENTIAL'
               WRITE(I4UNIT(-1),1002)
               IFAIL = 1
               RETURN
           ENDIF
               IFAIL = 2
           RETURN
       ENDIF
C
       IF (MOD(IWORDS,2).NE.0)THEN
           WRITE(I4UNIT(-1),1001)'MISMATCH OF PARENTS'
           WRITE(I4UNIT(-1),1002)
           IFAIL = 1
           RETURN
       ENDIF
C
       ITYPE = 0
       ITYP = 0
       ITP = 0
       NPT=IWORDS/2
       DO 100 I = 1,NPT
        BWNOA(I)=R8FCTN(STRING(IFIRST(2*I-1):ILAST(2*I-1)),IABT)
        IF(IABT.GT.0)THEN
            WRITE(I4UNIT(-1),1001)'FAULT IN IONIS. POTENTIAL',I
            WRITE(I4UNIT(-1),1002)
            IFAIL = 1
            RETURN
        ENDIF
C
C-----------------------------------------------------------------------
C DETECT WHETHER L QUANTUM NUMBER IS PRESENT IN THE STRING 
C-----------------------------------------------------------------------
       LFND = .FALSE.
         DO 12 K = IFIRST(2*I),ILAST(2*I)
           DO 13 J = 1,9
             IF(STRING(K:K).EQ.CTRMA(J))THEN
               LFND = .TRUE.
               KMRK = K
             ENDIF
 13        CONTINUE
 12      CONTINUE
C-----------------------------------------------------------------------
C DECIDE ON RESOLUTION TYPE 
C-----------------------------------------------------------------------
       IF(.NOT.LFND)THEN
         ITYPE = 3
       ELSE
         IF(KMRK.EQ.ILAST(2*I))THEN
           ITYPE = 1
         ELSE
           ITYPE = 2 
         ENDIF  
       ENDIF     
C-----------------------------------------------------------------------
C SPECIFIC PARENT WEIGHT HANDLING FOR DIFFERENT TYPES
C-----------------------------------------------------------------------
        IF(ITYPE.EQ.1)THEN
          DO 20 IC=1,9
           IF(STRING(ILAST(2*I):ILAST(2*I)).EQ.CTRMA(IC))THEN
               PRTWTA(I)=I4FCTN(STRING(IFIRST(2*I):
     &                     ILAST(2*I)-1),IABT)*TWTA(IC)
               IF(IABT.GT.0)THEN
                   WRITE(I4UNIT(-1),1001)'FAULT IN PARENT TERM',I
                   WRITE(I4UNIT(-1),1002)
                   IFAIL = 1
                   RETURN
               ELSE
                   LSETA(I)=.TRUE.
               ENDIF
               CPRTA(I) = STRING(IFIRST(2*I)-1:ILAST(2*I)+1)
           ENDIF
   20     CONTINUE
C
        ELSE IF(ITYPE.EQ.2)THEN
          DO 21 IC = 1,9
            IF(STRING(KMRK:KMRK).EQ.CTRMA(IC))THEN
              PRTWTA(I)=R8FCTN(STRING(KMRK+1:ILAST(2*I)),IABT)
              IF(IABT.GT.0)THEN
                  WRITE(I4UNIT(-1),1001)'FAULT IN PARENT TERM',I
                  WRITE(I4UNIT(-1),1002)
                  IFAIL = 1
                  RETURN
              ELSE
                  LSETA(I)=.TRUE.
              ENDIF
              CPRTA(I) = STRING(IFIRST(2*I)-1:ILAST(2*I)+1)
            ENDIF
   21     CONTINUE
C
        ELSE IF(ITYPE.EQ.3)THEN
              PRTWTA(I) = R8FCTN(STRING(IFIRST(2*I):ILAST(2*I)),IABT)
              IF(IABT.GT.0)THEN
                  WRITE(I4UNIT(-1),1001)'FAULT IN PARENT TERM',I
                  WRITE(I4UNIT(-1),1002)
                  IFAIL = 1
                  RETURN
              ELSE
                  LSETA(I)=.TRUE.
              ENDIF
              CPRTA(I) = STRING(IFIRST(2*I)-1:ILAST(2*I)+1)
        ENDIF
C
C FLAG CHANGES IN THE RESOLUTION
C
       IF(ITYP.NE.ITYPE.AND.ITYP.NE.0)THEN
         ITP = 3
       ENDIF
       ITYP = ITYPE
  100  CONTINUE
C
       IF(ITP.EQ.3)THEN
         ITYPE = 3
       ENDIF
       RETURN
C-----------------------------------------------------------------------
 1001 FORMAT(1X,32('*'),' B9PARS ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,27('*'),' SUBROUTINE TERMINATED ',28('*'))
C-----------------------------------------------------------------------
      END
