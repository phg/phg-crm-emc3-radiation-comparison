CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas209/b9ispf.for,v 1.6 2004/07/06 11:33:40 whitefor Exp $ Date $Date: 2004/07/06 11:33:40 $
CX
       SUBROUTINE B9ISPF( SEQX   , IZ0X   ,
     &                    NJPRTX , NBPRTX , NJLEVX , NBLEVX ,
     &                    INDJP  , CNJP   , INDBP  , CNBP   ,
     &                    INDJL  , CNJL   , INDBL  , CNBL   ,
     &                    BNDLS  , NDLEV  , IL     , C18    ,
     &                    NSTAR  , INDBLO , NGAP   , BNDPR  ,
     &                    MGAP   , INDBPO , NPL    , BWNOA  ,
     &                    PRTWTA , CPRT   , NDMET  , CDEFAULT,
     &                    LPEND
     &            )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  
C  ******************* FORTRAN 77 SUBROUTINE B9ISPF ********************
C
C  PURPOSE: WRITE PARENT METASTABLE INFORMATION TO IDLPIPE
C           THEN READ BUNDLES FROM PIPE
C
C  SUBROUTINES: CHINDX (RENUMERS INDEXING AND SELECTS TERMS 
C                       FOR LEVELS/PARENTS)
C
C  AUTHOR: DAVID H.BROOKS, UNIV.OF STRATHCLYDE 
C
C  VERSION 1.1:                                     29/03/95
C
C  VERSION 1.2:                                     17/07/95
C  MODIFIED: TIM HAMMOND, TESSELLA SUPPORT SERVICES PLC
C		- CHANGED FORMAT STRING 1007 SLIGHTLY TO
C                 ALLOW FOR LARGER VALUES THAN PREVIOUSLY
C
C  VERSION 1.3:                                     23/10/95
C  MODIFIED: DAVID H.BROOKS, UNIV.OF STRATHCLYDE
C               - CHANGED I1 TO I3 IN FORMAT STRING 1006
C                 TO ALLOW FOR BUNDLE INDEXING UP TO 999
C
C  VERSION 1.4:                                     06/12/95
C  MODIFIED: DAVID H.BROOKS, UNIV.OF STRATHCLYDE
C               - ALTERED LENGTH OF CPRTA FROM 4 TO 9 AND CPSTRG
C                 FROM 32 TO 37 TO HANDLE J-RESOLVED PARENT
C                 METASTABLES.ALSO, CHANGED FORMAT STRINGS 1007
C                 & 1008 TO COPE WITH THIS. CORRECTED ERROR IN
C                 ALLOWING FORMAT STRING 1006 TO INCREASE TO I3,
C                 THIS MEANT THAT NPL WAS NOT SENT THROUGH CORRECTLY
C                 SO ADDED FORMAT 1009 TO DISTINGUISH THE TWO CASES.
C
C  VERSION 1.5:                                     19/01/96
C  MODIFIED: DAVID H.BROOKS
C               - ALTERED FORMAT STRING 1007 AND INCREASED CPSTRG TO 49
C                 CHARACTERS AND CHANGED DIMENSION OF ALL NJLEVX ARRAYS
C                 TO NDLEV SO AS TO WORK ON DEC.
C
C  VERSION 1.6:                                     26/01/96
C  MODIFIED: DAVID H.BROOKS
C               - ADDED CONSISTENCY CHECK ON NBLEVX
C
C-----------------------------------------------------------------------
       INTEGER BNDLS, PIPEIN, PIPEOU, IZ0X, I, J, MK
       INTEGER NJPRTX, NBPRTX, NJLEVX, NBLEVX, NPL
       INTEGER INDJP, INDBP, INDJL, INDBL, NDLEV, IL, NSTAR, NGP
       INTEGER INDBLO, NGAP, BNDPR, INDBPO, MGAP, NDMET, ILOGIC
       INTEGER I4UNIT
C-----------------------------------------------------------------------
       REAL*8  PRTWTA, BWNOA
C-----------------------------------------------------------------------
       CHARACTER SEQX*2
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       CHARACTER CNJP(NDLEV)*18 , CNBP(NDLEV)*18
       CHARACTER C18(NDLEV)*18  , CPRT(NDMET)*9
       CHARACTER CNJL(NDLEV)*18 , CNBL(NDLEV)*18
       CHARACTER CPSTRG*49, CDEFAULT*4
C-----------------------------------------------------------------------
       DIMENSION BNDLS(NDLEV) , PRTWTA(NDMET), BWNOA(NDMET)
       DIMENSION INDJP(NDLEV) , INDBP(NDLEV)
       DIMENSION INDJL(NDLEV) , INDBL(NDLEV) , NSTAR(NDLEV)
       DIMENSION INDBLO(NDLEV), NGAP(NDLEV)  , BNDPR(NDLEV)
       DIMENSION INDBPO(NDLEV), MGAP(NDLEV)
C-----------------------------------------------------------------------
       PARAMETER(PIPEIN = 5, PIPEOU = 6)      
C***********************************************************************
C MODIFICATION TO WRITE PARENT INFO. TO PIPE
C***********************************************************************
       WRITE(PIPEOU, 1006)NPL
       IF(NPL.EQ.0)THEN
         I = 1
         CPRT(I) = CDEFAULT
         PRTWTA(I) = 1.0
           WRITE(CPSTRG, 1007)I,CPRT(I),PRTWTA(I),BWNOA(I)
           WRITE(PIPEOU, 1008)CPSTRG
       ELSE
         DO 5 I = 1 , NPL
           WRITE(CPSTRG, 1007)I,CPRT(I),PRTWTA(I),BWNOA(I)
           WRITE(PIPEOU, 1008)CPSTRG
 5       CONTINUE
       ENDIF
       CALL XXFLSH(PIPEOU)
C***********************************************************************
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE UNLESS CANCEL SELECTED 
C-----------------------------------------------------------------------
       READ(PIPEIN,'(I1)')ILOGIC
         IF (ILOGIC.EQ.1)THEN
           LPEND = .TRUE.
         ELSE
           LPEND = .FALSE.
         ENDIF
C
       IF (.NOT.LPEND) THEN 
       READ(PIPEIN,1002)NGP
C       READ(PIPEIN,1001)SEQX
       READ(PIPEIN,1002)IZ0X
       READ(PIPEIN,1002)NJPRTX
       READ(PIPEIN,1002)NBPRTX
       READ(PIPEIN,1002)NJLEVX
       READ(PIPEIN,1002)NBLEVX
C
C
       DO 10 I = 1 , NJPRTX
         READ(PIPEIN,1003)INDJP(I),BNDPR(I),CNJP(I)
 10    CONTINUE
       DO 15 I = 1 , NJLEVX
         READ(PIPEIN,1009)BNDLS(I)
   15  CONTINUE
C-----------------------------------------------------------------------
C  MARK ORIGINAL INDEXING
C-----------------------------------------------------------------------
       DO 20 I = 1 , NJLEVX
         INDJL(I) = I
   20  CONTINUE
C-----------------------------------------------------------------------
C FOLLOWING SECTION CALCULATES BUNDLED INDEXING AND TERMS 
C FOR LEVELS AND PARENTS, CALLS CHINDX
C----------------------------------------------------------------------- 
       DO 30 I = 1 , NJLEVX
         READ(C18(I),1005)CNJL(I)
   30  CONTINUE
       CALL CHINDX(  CNJL  ,  INDJL  ,  NJLEVX  ,  NJLEVX  ,
     &               CNBL  ,  INDBL  ,  BNDLS   ,  NGAP    ,
     &               INDBLO 
     &            )
       CALL CHINDX(  CNJP  ,  INDJP  ,  NJLEVX  ,  NJPRTX  ,
     &               CNBP  ,  INDBP  ,  BNDPR   ,  MGAP    ,
     &               INDBPO
     &            )
C
       MK = 0
       DO 32 I = 1, NJLEVX
         IF(INDBL(I).GT.MK)THEN
           MK = INDBL(I)
         ENDIF
   32  CONTINUE
       IF(NBLEVX.GT.MK)THEN
         NBLEVX = MK
       ENDIF
C-----------------------------------------------------------------------
C  ALTERATION TO MAKE AN ARRAY WHICH MARKS THE BUNDLED LEVELS WITH A '*'
C-----------------------------------------------------------------------
      DO 55 I = 1,NBLEVX
        NSTAR(I) = 0
   55 CONTINUE
C
      DO 60 I = 1,NGP
        DO 70 J = 1,NJLEVX
          IF(BNDLS(J).EQ.I)THEN
            NSTAR(INDBL(J)) = 1
          ENDIF
   70   CONTINUE
   60 CONTINUE
      ENDIF
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A80)
 1001 FORMAT(3X,1A2)
 1002 FORMAT(I5)
 1003 FORMAT(I5,1X,I5,1X,1A18,1X)
 1005 FORMAT(1A18)
 1006 FORMAT(1X,I1) 
 1007 FORMAT(1X,I3,6X,1A9,6X,'(',F4.1,')',9X,F9.1)
 1008 FORMAT(1A49)  
 1009 FORMAT(1X,I3)
C-----------------------------------------------------------------------
       RETURN
       END
