CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas209/b9spf1.for,v 1.1 2004/07/06 11:34:32 whitefor Exp $ Date $Date: 2004/07/06 11:34:32 $
CX
      SUBROUTINE B9SPF1( L2FILE     , SAVFIL     ,
     &                   LPEND
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B9SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS209
C
C  SUBROUTINE:
C
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  Lalit Jalota (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    7/3/95 
C
C
C MODIFIED: DAVID H.BROOKS (UNIV.OF STRATHCLYDE)
C           ALTERED B1SPF1.F TO B9SPF1.F FOR USE IN ADAS209.
C           REMOVED ALL GRAPHICS OUTPUT REFERENCES WHICH ARE NOT 
C           REQUIRED FOR ADAS209 
C           11/5/95
C-----------------------------------------------------------------------
      CHARACTER    SAVFIL*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , L2FILE
      INTEGER      ILOGIC      ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
C-----------------------------------------------------------------------
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
