C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas209/adas209.for,v 1.6 2004/07/06 10:20:59 whitefor Exp $ Date $Date: 2004/07/06 10:20:59 $
       Program ADAS209
       
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS209 **********************
C
C  PURPOSE:  Bundles adf04 files according to mapping supplied by 
C            the user.
C
C  NOTE:     This is the amalgamation of two routines (adas209 and adasdev)
C            From v1.6 onwards adasdev is removed. 
C
C  PROGRAM:
C
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAXIMUM OF INPUT DATA FILE TEMPS
C          (I*4)  NDTIN   = PARAMETER = MAX.NUMBER OF ISPF ENTRED TEMPS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZ      =  RECOMBINED ION CHARGE READ FROM INPUT FILE
C          (I*4)  IZ0     =         NUCLEAR CHARGE READ FROM INPUT FILE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE READ FROM INPUT FILE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY INDEX
C                                            LEVELS.
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF ELECTRON IMPACT
C                                            TRANSITIONS.
C          (I*4)  ISTRN   = INPUT DATA FILE: SELECTED ELECTRON IMPACT
C                                     TRANSITION INDEX FOR ANALYSIS.
C                                     (FOR USE WITH 'IETRN()', 'IE1A()'
C                                      'IE2A()' AND 'AVALE()' ARRAYS)
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                                     PAIRS FOR THE SELECTED TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES.
C          (I*4)  IFOUT   = 1 => 'TINE(array)' UNITS: KELVIN
C                         = 2 => 'TINE(array)' UNITS: eV
C                         = 3 => 'TINE(array)' UNITS:REDUCED TEMPERATURE
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C          (I*4)  IUPPER  = SELECTED TRANSITION: UPPER LEVEL ARRAY INDEX
C          (I*4)  ILOWER  = SELECTED TRANSITION: LOWER LEVEL ARRAY INDEX
C          (I*4)  LUPPER  = SELECTED TRANSITION: UPPER INDEX LEVEL
C          (I*4)  LLOWER  = SELECTED TRANSITION: LOWER INDEX LEVEL
C
C          (R*8)  R8TCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  EUPPER  = SELECTED TRANSITION: UPPER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  ELOWER  = SELECTED TRANSITION: LOWER ENERGY LEVEL
C                           RELATIVE TO INDEX LEVEL 1. (CM-1)
C          (R*8)  WUPPER  = SELECTED TRANSITION: UPPER LEVEL STAT. WT.
C          (R*8)  WLOWER  = SELECTED TRANSITION: LOWER LEVEL STAR. WT.
C                           (NOTE: STAT. WT. = STATISTICAL WEIGHT)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  AA      = SELECTED TRANSITION A-VALUE (SEC-1)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'IFOUT'
C          (R*8)  XMINK   = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  XMAXK   = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: KELVIN
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE COEFF. (cm**3/s)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE COEFF. (cm**3/s)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
CA         (C*80) SAVFIL  = NAME OF FILE TO WHICH DATA IS WRITTEN.
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*3)  TITLED  = ELEMENT SYMBOL.(INCORPORATED INTO 'TITLX')
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
CX         (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
CA         (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM IDL)
C                           (INCORPORATED INTO 'TITLX')
CX         (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CA         (C*40) TITLE   = TITLE OF RUN (READ FROM IDL PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
CX         (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
CA         (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C          (R*8)  AVALE() = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  SCEF()  = INPUT DATA FILE: TEMPERATURES (kelvin)
C          (R*8)  GAMMA() = INPUT DATA FILE: SELECTED TRANSITION -
C                                            GAMMA VALUE AT 'SCEF()'
C          (R*8)  RATE()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           EXCITATION RATE COEFF. (cm**3/s) AT 'SCEF()'
C          (R*8)  DRATE() = INPUT DATA FILE: SELECTED TRANSITION -
C                           DEEXCITATION RATE COEF.(cm**3/s) AT 'SCEF()'
C
C          (R*8)  TINE()  = ISPF ENTERED ELECTRON TEMPERATURE VALUES.
C                           (NOTE: UNITS ARE GIVEN BY 'IFOUT')
C          (R*8)  TOA()   = ISPF ENTERED TEMPERATURES (kelvin)
C          (R*8)  GAMOA() = SPLINE INTEROPLATED GAMMA VALUE AT 'TOA()'
C          (R*8)  ROA()   = EXCITATION RATE COEFF.(cm**3/s) AT 'TOA()'
C          (R*8)  DROA()  = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOA()'
C
C          (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (kelvin)
C          (R*8)  GAMOSA()= SPLINE INTEROPLATED GAMMA VALUE AT 'TOSA()'
C          (R*8)  ROSA()  = EXCITATION RATE COEFF.(cm**3/s) AT 'TOSA()'
C          (R*8)  DROSA() = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOSA()'
C
C          (R*8)  TOMA()  = MINIMAX: SELECTED TEMPERATURES (kelvin)
C          (R*8)  GAMOMA()= MINIMAX GENERATED GAMMA VALUE AT 'TOMA()'
C          (R*8)  ROMA()  = EXCITATION RATE COEFF.(cm**3/s) AT 'TOMA()'
C          (R*8)  DROMA() = DEEXCITATION RATE COEF.(cm**3/s) AT 'TOMA()'
C
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (L*4)  LTRNG() = .TRUE.  => TEMPERATURE 'TOA()' IN RANGE
C                           .FALSE. => TEMPERATURE 'TOA()' OUT OF RANGE
C                           (RANGE = INPUT TEMPERATURE RANGE)
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES.
C          (C*22) STRGA() = NOMENCLATURE FOR LEVEL 'IA()' INCLUDES:
C                           'CSTRGA()' AND QUANTUM NUMBERS.
C	   (I*4)  IFIRST  = FIRST NON-BLANK CHARCTER IN FILENAME
C
C	   (I*4)  ILAST   = LAST  NON-BLANK CHARCTER IN FILENAME
C
C
C NOTE:    INPUT TEMPERATURES 'TINE()' ARE CONVERTED TO KELVIN 'TOA()'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B9DATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          BXCSTR     ADAS      EXTRACT NON-BLANK CHARS. FROM STRING
C          BXTTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO KELVIN
C	   XXSLEN     ADAS      FINDS POSITIONS OF SPACES IN STRING
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION: CONVERT TEMP. TO KELVIN
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    12/10/90
C
C UNIX PORT:
C VERSION: 1.1                          DATE: 11/05/95
C UPDATE:  D.H.BROOKS- IDL-ADAS : 
C              - CREATED FROM ADAS201.FOR - MODIFIED FOR USE WITH 
C                ADAS209, BY ADDING CALL TO ADASDEV
C
C VERSION: 1.2                          DATE: 24/10/95
C UPDATE:  TIM HAMMOND 
C              - COMMENTED OUT ALL REFERENCES TO OPENING OF UNIT10
C                AS THEY ARE SUPERFLUOUS.
C
C VERSION: 1.3                          DATE: 19/01/96
C UPDATE:  DAVID BROOKS/TIM HAMMOND		
C              - TIDIED UP AND REMOVED SUPERFLUOUS COMMENTS LEFT OVER
C                FROM ADAS201
C              - ADDED ISTOP FOR RETURN TO MENU
C
C VERSION: 1.4				DATE: 18/04/96
C UPDATE:  WILLIAM OSBORN
C		- INCREASED NDLEV AND NDTRN
C
C VERSION: 1.5				DATE: 20/11/98
C UPDATE:  DAVID BROOKS
C		- INCREASED NDLEV AND NDTRN
C
C-----------------------------------------------------------------------------
C Comments from adasdev.for
C-----------------------------------------------------------------------------
C
C VERSION: 1.1                          DATE: 27-06-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 17-07-95
C MODIFIED: TIM HAMMOND 
C               - INCREASED LENGTH OF FILENAME DSNINP TO 80
C
C VERSION: 1.3                          DATE: 17-07-95
C MODIFIED: TIM HAMMOND
C               - MODIFIED FORMAT 1003 TO COPE WITH LONGER FILENAME
C
C VERSION: 1.4                          DATE: 19-01-96
C MODIFIED: DAVID BROOKS/TIM HAMMOND
C               - TIDIED UP COMMENTS
C               - ADDED ISTOP FOR RETURN TO MENU & MOVED POSITION
C                 OF BPRTWT WHICH WERE IN ERROR
C               - INCREASED LENGTH OF CPRTA TO 9 (SEE B9DATA)
C               - ALTERED FORMAT STATEMENTS 1008 AND 1010 AND 
C                 INCREASED WRITE LINES TO HANDLE MORE PARENTS AND 
C                 CTERMA (FROM *4 TO *7)
C
C VERSION: 1.5                          DATE: 26-01-96
C MODIFIED: DAVID BROOKS
C               - ADDED LTRAP TO DISPLAY WARNING ABOUT BUNDLING
C                 DIFFERENT TERMS/CONFIGURATIONS. WE NOW LET THIS
C                 GO AHEAD ALLOWING WIDER CAPABILITIES AND LEAVING
C                 THE USER TO CHECK THE OUTPUT. ALSO, ALTERED
C                 PARENT WEIGHT PRINTOUT & CALCULATION OF ZPBA'S
C
C VERSION: 1.6                          DATE: 05-03-96
C MODIFIED: DAVID BROOKS
C               - ADDED ZSUM ARRAY TO RE-NORMALISE ZETA FACTORS BY
C                 LEVEL BUNDLE SUM. SHOULD THIS BE A WEIGHTED SUM ?
C
C VERSION: 1.7				DATE: 18/04/96
C UPDATE:  WILLIAM OSBORN
C		- INCREASED NDLEV AND NDTRN
C
C VERSION: 1.8				DATE: 25/04/96
C UPDATE:  WILLIAM OSBORN
C		- AFTER STATEMENTS 90 AND 210 INCREASED FORMAT (A80) TO (A133)
C		  TO ALLOW ALL TEMPERATURES TO BE SEEN
C
C VERSION: 1.9				DATE: 18/11/96
C UPDATE:  DAVID H. BROOKS
C               - MODIFICATIONS TO ALLOW BUNDLING OF RADIATIVE RECOMBINATION
C                 DATA (INCLUDING ADDITION OF NEW VARIABLE CPLUS).
C VERSION: 1.10				DATE: 20/11/96
C UPDATE:  DAVID H. BROOKS
C               - ALTERED FORMAT STATEMENTS 1000 & 1012   
C
C VERSION: 1.11				DATE: 18/11/98
C UPDATE:  DAVID H. BROOKS
C               - INCREASED NDLEV & NDTRN                    
C
C VERSION: 1.12				DATE: 01/11/2001
C UPDATE:  Martin O'Mullane
C               - Add bundling of S-lines. The rates are calculated from
C                 the data in the adf04 file and are then statistically 
C                 summed. This sum is then divided by the new, bundled, 
C                 exponential factor.
C              - Add real name of producer via XXNAME.
C
C-----------------------------------------------------------------------
C
C VERSION : 1.6
C DATE    : 14-04-2004
C MODIFIED: Martin O'Mullane
C            - Incorporated the adasdev routine into the top level.
C            - Use xxdata_04 to access large datasets and adjust
C              the writing of the bundled adf04 files for large
C              xja values.
C            - Remove all superfulous comments and variables.
C
C-----------------------------------------------------------------------

       INTEGER  NDLEV       , NDTRN          , NDMET      , ndtem
       INTEGER  NDQDN
       INTEGER  IUNT09      , IUNT11         , PIPEIN
C----------------------------------------------------------------------
       PARAMETER ( NDLEV  = 1000  , NDTRN = 50000 , NDMET = 4 ,
     &             ndtem  = 14    , NDQDN = 6     )
       PARAMETER ( IUNT09 = 9   , IUNT11 = 11)
       PARAMETER ( PIPEIN = 5 )
C----------------------------------------------------------------------
       INTEGER   IZ          , IZ0            , IZ1        ,
     &           IL          , NV             , ITRAN      ,
     &           MAXLEV      , NPL            , IT         ,
     &           I           , IP             , IJ         ,
     &           IPJ         , ISX            , ILX        ,
     &           IPOS        , IQS            , ITR        ,
     &           ITRANB      , IB             , JB
       INTEGER   I4UNIT      , ISTOP          , ITYP       , ifail
       INTEGER   IZ0X        ,
     &           NJPRTX      , NBPRTX         , NJLEVX     , NBLEVX
       INTEGER   LEN3        , LEN4           , nfmt       , ioff
C-----------------------------------------------------------------------
       REAL*8    BWNO        , ESUM           , WSUM       ,
     &           WT          , ESHIFT         , BPRTWT     ,
     &           BPWNO       , Z1
       real*8    fmul        , xlsmax
C----------------------------------------------------------------------
       CHARACTER REP*3       , USER*30        , DATE*8
       CHARACTER TITLED*3      
       CHARACTER SEQX*2      , C9*9           , CSN*8
       CHARACTER STRING*133  , BLANKS*133     , SAVFIL*80
       CHARACTER CDEFAULT*4  , CPLUS*1        , DSFULL*80  , fmt*49 
C-----------------------------------------------------------------------
       INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &           I1A(NDTRN)  , I2A(NDTRN)     ,
     &           I1BA(NDTRN) , I2BA(NDTRN)    ,
     &           IPLA(NDMET,NDLEV)            , NPLA(NDLEV)
       INTEGER   INDJP(NDLEV)  , INDBP(NDLEV)  ,
     &           INDJL(NDLEV)  , INDBL(NDLEV)
       INTEGER   ISB(NDLEV)    , ILB(NDLEV)    , NPBA(NDLEV) ,
     &           IPBA(NDMET,NDLEV),BNDLS(NDLEV),BNDPR(NDLEV)
       INTEGER   NSTAR(NDLEV), NGAP(NDLEV), INDBLO(NDLEV)
       INTEGER   MGAP(NDLEV),  INDBPO(NDLEV)
C----------------------------------------------------------------------
       REAL*8   BWNOA(NDMET)   , PRTWTA(NDMET)
       REAL*8   SCEF(ndtem)    , ZSUM(NDLEV)
       REAL*8   XJA(NDLEV)     , WA(NDLEV)           ,
     &          AVAL(NDTRN)    , SCOM(ndtem,NDTRN)   ,
     &          AVALB(NDTRN)   , SCOMB(ndtem,NDTRN)  ,
     &          ZPLA(NDMET,NDLEV)
       REAL*8   ZPBA(NDMET,NDLEV) , ENLS(NDLEV)  , XLSA(NDLEV)  ,
     &          BPWNOA(NDMET)     , BPRTWTA(NDMET)
C-----------------------------------------------------------------------
       CHARACTER TCODEB(NDTRN)*1, TCODE(NDTRN)*1 , CSTRGA(NDLEV)*18
       CHARACTER CPLA(NDLEV)*1
       CHARACTER CNJP(NDLEV)*18 , CNBP(NDLEV)*18 ,
     &           CNJL(NDLEV)*18 , CNBL(NDLEV)*18 , CFGB(NDLEV)*18
       CHARACTER CPBA(NDLEV)*1  , CTERMA(NDMET)*8, CPRTA(NDMET)*9
C----------------------------------------------------------------------
       LOGICAL  LEXIST          , LSET,   LPEND,  L2FILE, LTRAP
       LOGICAL  LBSETA(NDMET)   , LPBA(NDMET,NDLEV)
C----------------------------------------------------------------------
       DATA LEXIST/.FALSE./
C-----------------------------------------------------------------------
       DATA  CDEFAULT/'(1S)'/
       DATA  CPLUS/'+'/
       DATA BLANKS/' '/
C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 - unused for calculations
C-----------------------------------------------------------------------
      integer   iadftyp       , itieactn      , iorb    
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      real*8    beth(ndtrn)                   ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000
      CALL XXNAME(USER)
      CALL XXDATE(DATE)

C-----------------------------------------------------------------------
 100  CONTINUE
      
      CALL B9SPF0( REP , DSFULL)
      IF (REP.EQ.'YES') GOTO 9999

C-----------------------------------------------------------------------
C Read in the adf04 data
C-----------------------------------------------------------------------

       ltrap = .FALSE.
       call xxopen(iunt09, dsfull, lexist)

       if (lexist) then
          itieactn = 0
       
          call xxdata_04( iunt09 , 
     &                    ndlev  , ndtrn , ndmet   , ndqdn , ndtem ,
     &                    titled , iz    , iz0     , iz1   , bwno  ,
     &                    npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                    il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                    ia     , cstrga, isa     , ila   , xja   ,
     &                    wa     ,
     &                    cpla   , npla  , ipla    , zpla  ,
     &                    nv     , scef  ,
     &                    itran  , maxlev,
     &                    tcode  , i1a   , i2a     , aval  , scom  ,
     &                    beth   ,     
     &                    iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                    letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                    lstyp  , lltyp , itieactn, ltied
     &                  )
           
           rewind iunt09 
           read(iunt09,'(25x,a75)')string
           ifail = 0
           call b9pars(ndmet  , string , npl   , bwnoa , lbseta , 
     &                 prtwta , cprta  , ifail , ityp  )
           
           close(iunt09)
        
        else
            write(i4unit(-1),*)'input data set does not exist'
            stop
       endif
       lexist = .FALSE.
       
       
C----------------------------------------------------------------------
C Get the bundling map
C----------------------------------------------------------------------

 1     CONTINUE
       CALL B9ISPF( SEQX   , IZ0X   ,
     &              NJPRTX , NBPRTX , NJLEVX , NBLEVX ,
     &              INDJP  , CNJP   , INDBP  , CNBP   ,
     &              INDJL  , CNJL   , INDBL  , CNBL   ,
     &              BNDLS  , NDLEV  , IL     , CSTRGA ,
     &              NSTAR  , INDBLO , NGAP   , BNDPR  ,
     &              MGAP   , INDBPO , NPL    , BWNOA  ,
     &              PRTWTA , CPRTA  , NDMET  , CDEFAULT,
     &              LPEND
     &            )

C----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE(0)
C----------------------------------------------------------------------

       READ(PIPEIN,*) ISTOP
       IF (ISTOP.EQ.1) GOTO 9999

C----------------------------------------------------------------------
C  EVALUATE LS COUPLED ENERGY LEVELS
C----------------------------------------------------------------------

       IF (.NOT.LPEND)THEN
       CALL B9SPF1( L2FILE, SAVFIL, LPEND)
       
       IF (LPEND) GO TO 1

       OPEN(IUNT11,FILE=SAVFIL,STATUS='UNKNOWN')
       IF(NJLEVX.NE.IL) THEN
           WRITE(I4UNIT(-1),*)'INCONSISTENT TOTAL NUMBERS OF LEVELS'
           STOP
       ENDIF

       DO 40 I=1,NBLEVX
         ZSUM(I) = 0
         LSET = .FALSE.
         ESUM = 0.0D0
         WSUM = 0.0D0
         NPBA(I) = 0
         CPBA(I) = ' '

         DO 12 IP = 1,NBPRTX
           IPBA(IP,I) = 0
           ZPBA(IP,I) = 0.0D0
           LPBA(IP,I) = .FALSE.
   12    CONTINUE

         DO 20 IJ = 1,NJLEVX
           IF(INDBL(IJ).EQ.I) THEN
             ZSUM(I) = ZSUM(I)+1
               IF(.NOT.LSET) THEN
                   LSET = .TRUE.
                   ISB(I) = ISA(IJ)
                   ILB(I) = ILA(IJ)
                   CFGB(I) = CNBL(IJ)
               ELSE
                   ISX = ISA(IJ)
                   ILX = ILA(IJ)
                   IF(ISX.NE.ISB(I).OR.ILX.NE.ILB(I).OR.
     &                              CFGB(I).NE.CNJL(IJ))THEN
                       LTRAP = .TRUE.
                   ENDIF
               ENDIF
               WT = 2.0D0*XJA(IJ)+1.0D0
               WSUM = WSUM + WT
               ESUM = ESUM +WT*WA(IJ)
               IF(CPLA(IJ).EQ.' ')THEN
                   GO TO 20
               ELSEIF(CPLA(IJ).EQ.'X')THEN
                   CPBA(I) = 'X'
               ELSE
                   DO 15 IP = 1,NBPRTX
                     DO 13 IPJ = 1,NPLA(IJ)
                        IF(INDBP(IPLA(IPJ,IJ)).EQ.IP)THEN
                           IPBA(IP,I) = INDBP(IP)
                           ZPBA(IP,I) = ZPBA(IP,I)+ZPLA(IPJ,IJ)
                           LPBA(IP,I) = .TRUE.
                       ENDIF
   13                CONTINUE
   15              CONTINUE
               ENDIF
          ENDIF
   20    CONTINUE
         
         ENLS(I) = ESUM/WSUM
         XLSA(I) = (WSUM-1.0D0)/2.0D0
	 xlsmax = max(XLSA(I), xlsmax)
         
         DO 25 IP = 1,NBPRTX
           IF(LPBA(IP,I))THEN
               NPBA(I) = NPBA(I) + 1
           ENDIF
   25    CONTINUE
         DO 28 IP = 1,NBPRTX
           IF(LPBA(IP,I))THEN
               WRITE(CPBA(I),'(I1)')IP
               GO TO 40
           ENDIF
   28    CONTINUE
   
   40  CONTINUE
   
       DO 21 IP = 1, NBPRTX
         DO 22 I = 1, NBLEVX
           ZPBA(IP,I) = ZPBA(IP,I)/ZSUM(I)
   22    CONTINUE
   21  CONTINUE
       ESHIFT = ENLS(1)
       
       DO 50 I = 1,NBLEVX
         ENLS(I) = ENLS(I)-ESHIFT
   50  CONTINUE
       
       IF(LTRAP)THEN
         WRITE(I4UNIT(-1),*)'*** ADAS209 WARNING : BUNDLING DIFFERENT',
     &     ' TERMS OR CONFIGURATIONS ***'
         WRITE(I4UNIT(-1),*)'*** - THERE MAY BE REPEATED TRANSITIONS',
     &     ' IN THE OUTPUT FILE       ***'
       ENDIF

C----------------------------------------------------------------------
C  SET LS-COUPLED PARENTS FOR FIRST LINE
C----------------------------------------------------------------------

       IF(NPL.GT.0)THEN
           DO 60 IP = 1,NBPRTX
             BPRTWT = 0.0D0
             BPWNO = 0.0D0
             DO 55 IPJ = 1,NPL
               IF(INDBP(IPJ).EQ.IP) THEN
                   BPRTWT = BPRTWT+PRTWTA(IPJ)
                   BPWNO = BPWNO + PRTWTA(IPJ)*BWNOA(IPJ)
                   CTERMA(IP) = CNBP(IPJ)(1:8)
               ENDIF
   55        CONTINUE
             BPWNOA(IP) = BPWNO/BPRTWT - ESHIFT
             BPRTWTA(IP) = BPRTWT
   60      CONTINUE
           DO 61 IP = 1, NBPRTX
             IF(ITYP.EQ.2)THEN
               WRITE(CSN,'(1A8)')CTERMA(IP)
               WRITE(CTERMA(IP),1015)CSN(1:3),BPRTWTA(IP)
             ELSE IF(ITYP.EQ.3)THEN
               WRITE(CTERMA(IP),1016)BPRTWTA(IP)
             ENDIF
   61      CONTINUE
           STRING = BLANKS
           WRITE(STRING,1001) TITLED,IZ,IZ0,IZ1,(BPWNOA(IP),
     &                        CTERMA(IP),IP=1,NBPRTX)
       ELSE
           BPWNO = BWNO-ESHIFT
           CTERMA(1) = CNBP(1)(1:8)
           STRING = BLANKS
           WRITE(STRING,1001)TITLED,IZ,IZ0,IZ1,BPWNO,CTERMA(1)
       ENDIF
       WRITE(IUNT11,'(1A116)')STRING


C Make a format string and position offsetd based on sixe of xlsmax
C Write bundled levels with a star '*'

       fmt = '(I5,1X,1A17,1H ,1H(,I1,1H),I1,1H(,FX.1,1H),F16.1)'
       if (xlsmax.LT.100000.0) nfmt = 7
       if (xlsmax.LT.10000.0)  nfmt = 6
       if (xlsmax.LT.1000.0)   nfmt = 5
       if (xlsmax.LT.100.0)    nfmt = 4
       write(fmt(36:36),'(I1)')nfmt
       ioff = nfmt-4

       DO 80 I = 1,NBLEVX
         STRING = BLANKS
         IF (NSTAR(I).EQ.1) write(fmt(15:15),'(A)')'*'
         WRITE(STRING,fmt)I,CFGB(I),ISB(I),ILB(I),XLSA(I),ENLS(I)
         IF(CPBA(I).EQ.' ')THEN
             CONTINUE
         ELSEIF (CPBA(I).EQ.'X') THEN
             WRITE(STRING(ioff+51:ioff+54),'(1X,1A3)')'{X}'
         ELSE
             IPOS = 0
             DO 70 IP = 1,NBPRTX
               IF(LPBA(IP,I))THEN
                   WRITE(STRING(ioff+51+IPOS:ioff+59+IPOS),
     &	                 '(1X,1A1,I1,1A1,F5.3)')
     &                   '{',IPBA(IP,I),'}',ZPBA(IP,I)
                   IPOS = IPOS + 9
               ENDIF
   70        CONTINUE
         ENDIF
         WRITE(IUNT11,'(1A93)')STRING
	 write(fmt(15:15),'(A)')' '
   80  CONTINUE
       WRITE(IUNT11,'(1A5)') '   -1'

C----------------------------------------------------------------------
C  WRITE OUT ZEFF, IQS AND TEMPERATURE LINE
C----------------------------------------------------------------------

       STRING = BLANKS
       Z1  = IZ1
       IQS = 3
       WRITE(STRING,'(F5.2,I5)')Z1,IQS
       IPOS = 0
       DO 90 IT = 1,NV
         WRITE(C9,'(1PD9.2)') SCEF(IT)
         STRING(17+IPOS:21+IPOS) = C9(1:5)
         STRING(22+IPOS:24+IPOS) = C9(7:9)
         IPOS =IPOS+8
   90  CONTINUE
       WRITE(IUNT11,'(1A133)')STRING

C----------------------------------------------------------------------
C  ASSIGN TRANSITIONS TO LS-COUPLED GROUPS
C----------------------------------------------------------------------

       ITRANB = 0

       DO 220 I = 1,ITRAN
         IF(TCODE(I).EQ.' '.OR.TCODE(I).EQ.'R'.OR.TCODE(I).EQ.'S')THEN
             IF(TCODE(I).EQ.' ') IB = INDBL(I1A(I))
             IF(TCODE(I).EQ.'R') IB = INDBP(I1A(I))
             IF(TCODE(I).EQ.'S') IB = INDBP(I1A(I))
             JB = INDBL(I2A(I))
             IF (ITRANB.GT.0) THEN
                 DO 117 ITR = 1,ITRANB
                   IF(TCODEB(ITR).EQ.' '.AND.TCODE(I).EQ.' '.AND.         
     &                I1BA(ITR).EQ.IB.AND.I2BA(ITR).EQ.JB)THEN
                         AVALB(ITR) = AVALB(ITR)+AVAL(I)*
     &                              (2.0D0*XJA(I2A(I))+1.0D0)
                       DO 115 IT = 1,NV
                         SCOMB(IT,ITR)=SCOMB(IT,ITR)+SCOM(IT,I)
  115                  CONTINUE
                       GO TO 120
                   ELSEIF(TCODEB(ITR).EQ.'R'.AND.TCODE(I).EQ.'R'.AND.
     &                I1BA(ITR).EQ.IB.AND.I2BA(ITR).EQ.JB)THEN
                       DO 116 IT = 1,NV
                         SCOMB(IT,ITR)=SCOMB(IT,ITR)+SCOM(IT,I)
  116                  CONTINUE
                       GO TO 120
                   elseif(tcodeb(itr).eq.'S'.and.tcode(i).eq.'S'.and.
     &                 i1ba(itr).eq.ib.and.i2ba(itr).eq.jb)then
                       
                       if (npl.gt.0) then
                          fmul = -1.4387998D0*(bwnoa(I1A(I))-wa(I2A(I)))
                       else
                          fmul = -1.4387998D0*(bwno-wa(I2A(I)))
                       endif
                       do it = 1,nv
                         scomb(it,itr) = scomb(it,itr) + 
     &                                   scom(it,i) * exp(fmul/scef(it))
     &                                   * (2.0D0*XJA(I2A(I))+1.0D0)
                       end do
                       
                       go to 120
                   ENDIF
  117            CONTINUE
             ENDIF
             ITRANB = ITRANB + 1
             I1BA(ITRANB) = IB
             I2BA(ITRANB) = JB
             IF(TCODE(I).EQ.' ') TCODEB(ITRANB) = ' '
             IF(TCODE(I).EQ.'R') TCODEB(ITRANB) = 'R'
             if(tcode(i).eq.'S') tcodeb(itranb) = 'S'
             
             IF(TCODE(I).EQ.' ')THEN
               AVALB(ITRANB) = AVALB(ITRANB)+AVAL(I)*
     &                         (2.0D0*XJA(I2A(I))+1.0D0)
             ENDIF
             
             if (npl.gt.0) then
                fmul = -1.4387998D0*(bwnoa(I1A(I))-wa(I2A(I)))
             else
                fmul = -1.4387998D0*(bwno-wa(I2A(I)))
             endif
             
             DO IT = 1,NV
               if (tcode(i).eq.'S') then
                  scomb(it,itranb) = scomb(it,itranb) +
     &                               scom(it,i) * exp(fmul/scef(it)) *
     &                               (2.0D0*XJA(I2A(I))+1.0D0)
               else
                  SCOMB(IT,ITRANB)=SCOMB(IT,ITRANB)+SCOM(IT,I)
               endif
             END DO

  120        CONTINUE
         ENDIF
  220  CONTINUE

C----------------------------------------------------------------------
C  WRITE OUT THE RESULTS
C----------------------------------------------------------------------

       DO 300 I = 1,ITRANB
         IF(TCODEB(I).EQ.' '.OR.TCODEB(I).EQ.'R'.OR.
     &      TCODEB(I).EQ.'S')THEN
             STRING = BLANKS
             fmul = 1.0D0
             IF(TCODEB(I).EQ.' ') THEN
               WRITE(STRING,'(1A1,I3,I4)')TCODEB(I),I2BA(I),I1BA(I)
               WRITE(C9,'(1PD9.2)') AVALB(I)/(2.0D0*XLSA(I2BA(I))+1.0D0)
             ELSEIF(TCODEB(I).EQ.'R')THEN
               WRITE(STRING,'(1A1,I3,2X,1A1,I1)')TCODEB(I),I2BA(I),
     &                                     CPLUS,I1BA(I)
               WRITE(C9,'(1A9)') BLANKS                          
             elseif(tcodeb(i).eq.'S')then
               write(string,'(1A1,I3,2X,1A1,I1)')tcodeb(i),i2ba(i),
     &                                     cplus,i1ba(i)
               write(c9,'(1a9)') blanks
               if (npl.gt.0) then
                  fmul = 1.4387998D0*(bpwnoa(i1ba(i))-enls(i2ba(i)))
               else
                  fmul = 1.4387998D0*(bpwno-enls(i2ba(i)))
               endif
                                        
             ENDIF
             STRING(9:13) = C9(1:5)
             STRING(14:16) = C9(7:9)
             IPOS = 0
             DO 210 IT = 1,NV
               if (tcodeb(i).eq.'S') then
                  write(c9,'(1pd9.2)') scomb(it,i) * exp(fmul/scef(it))
     &                                 / (2.0D0*XLSA(I2BA(I))+1.0D0)
              else
                  WRITE(C9,'(1PD9.2)') SCOMB(IT,I)
               endif
               STRING(17+IPOS:21+IPOS) = C9(1:5)
               STRING(22+IPOS:24+IPOS) = C9(7:9)
               IPOS =IPOS+8
  210        CONTINUE
         ENDIF
         IF(I1BA(I).NE.I2BA(I).OR.I1BA(I).EQ.I2BA(I)
     &     .AND.(TCODEB(I).EQ.'R'.or.tcodeb(i).eq.'S'))THEN
           WRITE(IUNT11,'(1A133)')STRING
         ENDIF

C ABOVE ALTERATION CUTS OUT FINE STRUCTURE TRANSITIONS
C BETWEEN THE NEW BUNDLES IN THE DATA PRINTOUT -DHB 11.04.95

  300  CONTINUE


       WRITE(IUNT11,'(1A4)')'  -1'
       WRITE(IUNT11,'(1A8)')'  -1  -1'

       CALL XXSLEN(dsfull,LEN3,LEN4)
       WRITE(IUNT11,1003) dsfull(LEN3:LEN4)
       
       WRITE(IUNT11,1004)(INDJL(I),I=1,NJLEVX)
       WRITE(IUNT11,1005)
       WRITE(IUNT11,1006)(BNDLS(I),I=1,NJLEVX)
       WRITE(IUNT11,1007)
       WRITE(IUNT11,1008)(CNJP(I),I=1,NJPRTX)
       WRITE(IUNT11,1009)
       WRITE(IUNT11,1010)(BNDPR(I),I=1,NJPRTX)

       WRITE(IUNT11,3020)'ADAS209', USER, DATE

       CLOSE(IUNT11,STATUS = 'KEEP')

       ENDIF

C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100


C----------------------------------------------------------------------
 1001  FORMAT(1A3,I2,2I10,1X,5(F10.1,1A8,1X))
 1003  FORMAT('C',79('-')/
     &        'C'/
     &        'C  File generated by compression of a J-resolved file'/
     &        'C'/
     &        'C  Source file: ',A/
     &        'C'/
     &        'C  Original level indexing:')
 1004  FORMAT('C ',I5,I5,I5,I5,I5,I5,I5,I5,I5,I5)
 1005  FORMAT('C  Selection Vector:')
 1006  FORMAT('C ',I5,I5,I5,I5,I5,I5,I5,I5,I5,I5)
 1007  FORMAT('C '/
     &        'C  Original parent metastables: ')
C 1008  FORMAT('C ',3X,1A26)
 1008  FORMAT('C ',3X,1A7,1X,1A7,1X,1A7,1X,1A7,1X,1A7)
 1009  FORMAT('C  Parent bundling vector: ')
 1010  FORMAT('C ',I5,3X,I5,3X,I5,3X,I5,3X,I5)
 1015  FORMAT(1A3,F4.1,')')
 1016  FORMAT('(',F4.1,')')

 3020   FORMAT('C',/,
     &         'C  CODE     : ',1A7/
     &         'C  PRODUCER : ',A30/
     &         'C  DATE     : ',1A8,/,'C',/,'C',79('-'))

C----------------------------------------------------------------------
 9999 CONTINUE
      END
