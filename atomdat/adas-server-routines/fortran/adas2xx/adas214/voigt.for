C UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas214/voigt.for,v 1.3 2004/07/06 15:28:17 whitefor Exp $ Date $Date: 2004/07/06 15:28:17 $
CX
       subroutine voigt ( iprofile, y0, yl, yt, x, y, iz, w2f, wid,
     &  		  fact, xmin, xmax)
       implicit none
C
C------------------------------------------------------------------
C
C	*****************FORTRAN77 SUBROUTINE:	VOIGT *************
C
C  ORIGINAL NAME: VOIGT.BAS
C			(DEVELOPED BY KURT BEHRINGER)
C
C  VERSION:	1.0
C
C  PURPOSE:	COMPUTES LINE PROFILES
C
C  CALLING PROGRAM: ESCAPE.FOR
C
C  INPUT:
C
C
C
C  ROUTINES:
C		ROUTINE		SOURCE		BRIEF DESCRIPTION
C		-------------------------------------------------
C
C
C  AUTHOR:	K.H.BEHRINGER (IPF, UNIVERSITY OF STUTTGART)
C
C  DATE:	31/4/98
C
C  UPDATE: 
C
C  VERSION:	1.1					DATE: 18-06-98
C  MODIFIED: STUART LOCH			
C		 - CONVERTED TO FORTRAN FOR ADAS.
C
C  VERSION:	1.2					DATE: 26-11-98
C  MODIFIED: STUART LOCH			
C		 - CONVOLVED PROFILES ARE NORMALISED TO
C		   ACCOUNT FOR NUMERICAL INACCURACIES WHICH
C		   GAVE ESCAPE FACTORS GREATER THAN ONE
C  VERSION: 1.3                                         DATE: 19-02-99
C  MODIFIED: STUART lOCH
C               -DIMENSIONS OF X,Y, YT, YL AND MODPROF INCREASED FROM 1000
C                TO 3000, TO ACCOMODATE POSSIBLE INCREASES IN INTEGRATION
C                LIMIT OF LINE PROFILE

c------------------------------------------------------------------
       integer	 iz
c------------------------------------------------------------------
       integer iprofile,       i,      k,	ii,
     &		iizz,	i2zz,	iikk, i4unit
c------------------------------------------------------------------
       real*8	a,	b,	w2,	w3,	wid, x(3000),
     &  	y(3000),y0,	yt(3000),w2f,wid100,	wid1000,
     &		inte,	dx,	yl(3000), xmin,   xmax, fact,
     &	 	intemod
c------------------------------------------------------------------
c       character
c------------------------------------------------------------------
       if (iprofile.lt.0) goto 10
       if (iprofile.eq.1) then
          w3=0.60056*wid
	  y0=1./(1.06446*wid)
       endif	
       if (iprofile.eq.2) then
          w3=0.5*wid
          y0=1./(w3*3.14159)
       endif
       if (iprofile.eq.3) then 
          w3=0.5*wid
          y0=1./(w3*2.64261)
       endif
       if (iprofile.gt.5) then 
	  inte=0
          b=iprofile/100.
          a=1.-b
          w3=0.60056*wid
          w2=0.60056*w2f
          y0=a/(1.06446*wid)+b/(1.06446*w2f)
          do 20 i=1, iz
	     y(i)=a*dexp(-(x(i)/w3)**2)/(1.06446*wid)+
     & 	     b*dexp(-(x(i)/w2)**2)/(1.06446*w2f)
	     if (i.gt.1) inte=inte+(y(i)+y(i-1))*10.*wid/iz
c	     write(i4unit(-1),*)'inte=',(y(i)+y(i-1))*10.*wid/iz, i
   20	  end do
c	  write(i4unit(-1),*)'inte=',inte
	  intemod=0
          do 25 i=1, iz
	     y(i)=y(i)*0.95686/inte
	     if (i.gt.1) intemod=intemod+(y(i)+y(i-1))*10.*wid/200.
   25	  end do
c	  write(i4unit(-1),*)'intemod=',intemod

c this renormalises the profile to have area 0.95686, in accordance 
c with the original Behringer opacity code.  This is done to 
c compensate for a second doppler width that takes the profile away 
c from its normalised value

	  goto 9999
       endif

        inte=0	
       do 40 i=1,iz
          if (iprofile.eq.1) y(i)=dexp(-(x(i)/w3)**2)/(1.06446*wid)
          if (iprofile.eq.2) y(i)=1./(w3*3.14159)/(1+(x(i)/w3)**2)
          if (iprofile.eq.3) y(i)=1./(w3*2.64261)/(1+(x(i)/w3)**2.5)
	  if ((i.gt.1).and.(iprofile.eq.1)) 
     &        inte=inte+(y(i)+y(i-1))*2.1*wid/iz
	  if ((i.gt.1).and.(iprofile.eq.2)) 
     &        inte=inte+(y(i)+y(i-1))*100*wid/iz
	  if ((i.gt.1).and.(iprofile.eq.3)) 
     &        inte=inte+(y(i)+y(i-1))*50*wid/iz
   40  end do
c	write(i4unit(-1),*)'inte=',inte
       goto 9999

   10  w2=w2f/2
       w3=0.60056*wid
       iizz=2*iz
       i2zz=iizz+50
       dx=(x(2)-x(1))/2
       do 50 i=iz, 1, -1
           ii=2*i
	   x(ii)=x(i)
           x(ii-1)=x(ii)-dx
   50  end do

	 inte=0
       x(1)=0
       do 60 i=1,i2zz
	   if (i.gt.iizz) x(i)=x(i-1)+dx
	   if (iprofile.eq.-2) yt(i)=1./(w2*3.14159)/
     &				     (1.+(x(i)/w2)**2)
	   if (iprofile.eq.-3) yt(i)=1./(w2*2.64261)/
     & 				     (1.+(x(i)/w2)**2.5)
           if (yt(i).gt.yt(1)*0.01) wid100=x(i)
	   if (yt(i).gt.yt(1)*0.0001) wid1000=x(i)
c	   if (i.gt.1) inte=inte+(yt(i)+yt(i-1))*dx
	   if (x(i).lt.6*wid) then
		iikk=i
	   else
		goto 60
	   endif
	   yl(i)=dexp(-(x(i)/w3)**2)/(1.06446*wid)
   60  end do
c	   write(i4unit(-1),*)'inte=',inte
c	   write(i4unit(-1),*)'wid100=',wid100
c	   write(i4unit(-1),*)'wid1000=',wid1000
c       do i=1,i2zz
c          if (i.lt.100) write(i4unit(-1),*)'yt=',yt(i), x(i), i
c       end do
c       do 65 i=1,i2zz
c	  if (iprofile.eq.-2) yt(i)=yt(i)*1.00232/inte
c	  if (iprofile.eq.-3) yt(i)=yt(i)*0.99955/inte
c   65  end do
c normalises yt

       do 90 i=1, iizz
	  y(i)=yt(i)*yl(1)
	  do 70 k=2,iikk
	     if ((i+k-1).gt.i2zz) goto 80
	     y(i)=y(i)+(yt(i+k-1)+yt(abs(i-k)+1))*yl(k)
   70     end do
   80     continue
          y(i)=y(i)*dx
          if (i.gt.1) inte=inte+(y(i)+y(i-1))*dx
   90  end do
c       write(i4unit(-1),*)'inte*=',inte
       do 95 i=1,iizz
	  if (iprofile.eq.-2) y(i)=y(i)*1.00215/inte
	  if (iprofile.eq.-3) y(i)=y(i)*0.99951/inte
   95  end do
   	   
       y0 = y(1)
 
       do 100 i=1,iz
	   ii=2*i
	   x(i)=x(ii)
	   y(i)=y(ii)
	   yl(i)=yl(ii)
	   yt(i)=yt(ii)
	   if (i.gt.iz) then
	      x(i)=0.0
	      y(i)=0.0
	      yl(i)=0.0
	      yt(i)=0.0
	   endif
  100  end do
 9999  end
