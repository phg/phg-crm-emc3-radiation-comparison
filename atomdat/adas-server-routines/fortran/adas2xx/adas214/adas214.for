CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas214/adas214.for,v 1.5 2005/03/18 15:16:43 mog Exp $ Date $Date: 2005/03/18 15:16:43 $
CX
        program adas214

        implicit none
C-----------------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: adas214 ***************************
C
C  Original name: adasesc.bas
C                 (developed by K. H. Behringer)
C
C  Version:  1.0
C  Purpose:  Escape factor filter for specific ion files.
C            Converts optically thin specific ion file into an optically thick
C            file by computing escape factors in a particular geometry.
C  Data:
C
C  Program:
C          (i*4)  iunt10  = parameter = logical unit 10
C          (i*4)  iunt11  = parameter = logical unit 11
C          (i*4)  iunt12  = parameter = logical unit 12
C          (i*4)  ndcyl   = parameter = max. number of strips for
C          (i*4)  nddens  = parameter = max. number of points over ion profile
C          (i*4)  ndlev   = parameter = max. number of levels for ion
C          (i*4)  ndprof  = parameter = max. number of points over line profile
C          (i*4)  ndtrn   = papameter = max. no. of transitions allowed
C          (i*4)  ndtem   = papameter = max. no. of temperatures allowed
C          (i*4)  ndzcyl  = parameter = max. number of strips for z integrals
C          (i*4)  nzdim   = papameter = max. no. of ions allowed
C
C          (i*4)  ics     = 1 for sphere geometry of plasma
C                         = 2 for disk geometry of plasma
C                         = 3 for cylinder geometry of plasma
C          (i*4)  iden    = 1 for homogenous density distribution
C                         = 2 for linear density distribution
C                         = 3 for parabolic density distribution
C          (i*4)  iprofile= 1 for Doppler line profile
C                         = 2 for Lorentzian line profile
C                         = 3 for Holtzmark line profile
C                         = 10 for Double Doppler line profile
C                         = -2 for Voigt line profile
C                         = -3 for Doppler-Holtsmark line profile
C          (i*4)  ia()    = energy level index number
C          (i*4)  il      = input data file:number of energy levels
C          (i*4)  isa()   = multiplicity for level 'ia()'
C                           note: (isa-1)/2 = quantum number (s)
C          (i*4)  ila()   = quantum number (l) for level 'ia()'
C          (i*4)  izc     = recombined ion charge read
C          (i*4)  iz0     =        nuclear charge read
C          (i*4)  iz1     = recombining ion charge read
C                               (note: iz1 should equal izc+1)
C          (i*4)  jj      = index for optical depth, used in interpolation
C          (i*4)  jlev()  = level statistical weight
C          (i*4)  j0      = number of density values for integration over
C                           cylinder/slab
C          (i*4)  lupnr   = upper level index
C          (i*4)  ldnnr   = lower level index
C          (i*4)  num     = index of top level
C
C          (i*4)  ifail   = 0 bdxcoef subroutine successful
C                           1 bdxcoef subroutine failure or warning
C          (i*4)  inda(,) = identifier for spectrum line (10000*il+iu)
C                           1st dimension - index of lines for an ion
C                           2nd dimension - ion count index
C          (i*4)  nind()  = number of lines for an ion
C                           1st dimension - ion count index
C          (i*4)  nion    = number of ions to be computed in population calculation
C          (i*4)  nspec() = number of levels for an ion
C                           1st dimension - ion count index
C          (i*4)  ntemp   = the number of temperature values
C          (i*4)  s       = counter used to write warning about levels
C                           which are too thick for escape factor
C          (i*4)  wrnu()  = upper level index which of line which
C                           is too thick
C          (i*4)  wrnl()  = lower level index which of line which
C                           is too thick
C          (i*4)  xja()   = quantum number (j-value) for level 'ia()'
C                           note: (2*xja)+1 = statistical weight
C
C
C          (r*8)  A       = Einstein A-coefft. (sec-1)
C          (r*8)  aa      = cofficient used in absorption coefficient
C          (r*8)  Aescap  = A-value x escape factor
C          (r*8)  alpha   = absorption coefficient at line centre
C          (r*8)  alphelp = Optical depth for given transition
C                           (=absorption coeff*line of sight depth)
C          (r*8)  bndls() = multipliers to Boltzmann desities of levels
C          (r*8)  bwno    = ionisation potential (cm-1)
C          (r*8)  den     = central ion density  (cm-3), from IDL
C          (r*8)  elev()  = level energy (cm-1)
C          (r*8)  expop   = fraction of total population above metastables
C          (r*8)  f       = oscillator strength
C          (r*8)  help    = partition function
C          (r*8)  help1   = Number for A-coefft from adf04 file
C          (r*8)  help2   = Power of A-coefft from adf04 file
C          (r*8)  h11     = total population of metastable levels
C          (r*8)  kk      = Boltzmann factor  (for cm-1 and K )
C          (r*8)  lam     = wavelength of transition (nm)
C          (r*8)  ll      = radius of profile    (cm)
C          (r*8)  mm      = atomic mass number of impurity ion
C          (r*8)  nm()    = population density of levels
C          (r*8)  nn      = central ion density  (cm-3)
C          (r*8)  sum1()  = sum used in escape for homogenous density
C          (r*8)  sum2()  = sum used in escape for linear density
C          (r*8)  sum3()  = sum used in escape for parabolic density
C          (r*8)  sum4()  = final sum in escape using sum1, 2 or 3
C          (r*8)  thetacyl= escape factor for transition
C          (r*8)  te      = electron temperature (eV)
C          (r*8)  tg      = neutral temperature  (eV)
C          (r*8)  tte     = electron temperature (K)
C          (r*8)  ttg     = neutral temperature  (K)
C          (r*8)  underp  = excited state underpopulation w.r.t.level 1
C          (r*8)  w       = full width Doppler profile in nm.
C          (r*8)  wid     = full width Doppler profile for
C                           lambda=100nm, wid in pm
C          (r*8)  x0()    = Absorption coefficient at line centre/10
C          (r*8)  x1()    = Absorption coefficient at line centre
C          (r*8)  x2()    = Absorption coeff. for each line on
C                           normalised escape factor plot
C          (r*8)  y0      = not known yet
C          (r*8)  y2()    = used to draw line on normalised edcape
C                           factor plot
C          (r*8)  coef(,,)= emissivity for spectrum line (10000*j+i)
C                           1st dimension - index of lines for an ion
C                           2nd dimension - temperature index
C                           3rd dimension - ion count index
C          (r*8)  densa() = electron densities used in bdxcoef (cm-3)
C          (r*8)  denspa()= proton densities used in bdxcoef (cm-3)
C          (r*8)  popar(,)= level populations
C                           1st dimension: level index
C                           2nd dimension: temperature index
C          (r*8)  ratha() = ratio (neutral h density/electron density)
C          (r*8)  ratia() = ratio (n(z+1)/n(z) stage abundances)
C          (r*8)  tea()   = input electron temperatures(eV)
C          (r*8)  tpa()   = input proton temperatures(eV)
C          (r*8)  tha()   = input neutral hydrogen temperatures(eV)
C          (r*8)  wa()    = energy relative to level 1 (cm-1) for level 'ia()'
C          (r*8)  zeff    = plasma z effective (if 'lzsel'=.true.)
C                            (if 'lzsel'=.false. =>'zeff=1.0')
C
C
C          (c*18) cstrga()= nomenclature/configuration for level 'ia()'
C          (c*8)  date    = current date
C          (c*140) lllstr    = input line string
C          (c*80) savfil  = filename for modified adf04 file
C          (c*51) spec(,) = information string for level
C                           1st dimension - index of levels for an ion
C                           2nd dimension - ion count index
C          (c*9)  string  = temporary string
C          (c*3)  titled  = element symbol
C
C
C          (l*4)  open05  = .true.  => file allocated to unit 5
C                         = .false. => no file allocated to unit 5
C          (l*4)  open06  = .true.  => file allocated to unit 6
C                         = .false. => no file allocated to unit 6
C          (l*4)  open10  = .true.  => file allocated to unit 10
C                         = .false. => no file allocated to unit 10
C          (l*4)  open11  = .true.  => file allocated to unit 11
C                         = .false. => no file allocated to unit 11
C
C          (l*4)  lfxist()= .true.  => copase file for this ion
C                         = .false  => no copase file for this ion
C          (l*4)  lhsel   = .true.  => charge transfer to be included
C                         = .false. => charge transfer to be excluded
C          (l*4)  lisel   = .true.  => ionisation to be included
C                         = .false. => ionisation to be excluded
C          (l*4)  lpsel   = .true.  => proton data to be included
C                         = .false. => proton data to be excluded
C          (l*4)  lrsel   = .true.  => recombination to be included
C                         = .false. => recombination to be excluded
C          (l*4)  lzsel   = .true.  => scale proton data with zeff
C                         = .false. => do not scale proton data
C
C
C  Routines:
C           routine     source     brief description
C           -------     ------     -----------------
C           beispf      ADAS       reads plasma variables from IDL code
C           bespf0      ADAS       pipe communication with IDL
C                                  for input dataset name
C           beoutg      ADAS       pipe communication with IDL for plotting
C           bexcoef     ADAS       to  calculate  complete sets of spectrum
C                                  line emissivities for the ions of an element
C                                  and populations of every level
C           xxadas      ADAS       generates title for paper.txt file
C           escape      ADAS       calculates escape factors
C           voigt       ADAS       calculates line profiles
C           qpoly       ADAS       calculates quadratic log interpolation
C           i4unit      ADAS       assigns stream for messages
C           xxdate      ADAS       fetches current date
C
C  Author:  K. H. Behringer  (IPF, University of Stuttgart)
C
C  Date:    30/01/94
C
C  VERSION: 1.1                                         DATE: 18-06-98
C               Stuart Loch
C               -Converted to FORTRAN for ADAS. Put under SCCS control.
C
C  VERSION: 1.2                                         DATE: 26-11-98
C               Stuart Loch & Richard Martin
C               -Escape factors calculation extended to greater
C                optical depths, via a linear extrapolation.
C                Also, the modified A-value is set to zero if optical
C                depth too big to calculate escape factor (and puts
C                a warning in the modified adf04 file).
C                Now also outputs emergent flux escape factor
C                to paper.txt
C                - Changed savfil, dsfull, texdsn from char*60 to char*80
C
C  VERSION: 1.3                                         DATE: 19-02-99
C               Stuart Loch
C               -Dimensions of x,y and modprof increased from 1000
C                to 3000, to accomodate possible increases in integration
C                limit of line profile
C
C  VERSION: 1.4                                         DATE: 28-04-99
C               Richard Martin & Stuart Loch
C               - Changed string variable lll$ to lllstr to allow
C                 compilation on Linux.
C               - incresed size of lllstr from 80 to 140 to allow
C                 for adf04 files with more columns to be processed.
C               - ndtrn increased from 1100 to 2000 to allow for
C                 longer adf04 files to be processed.
C               - set A-value to 1.e-30 instead of 0 for transitions
C                 with optical depths too large to calculate escape
C                 factors for.
C
C VERSION : 1.5                               
C DATE    : 15-03-2005
C MODIFIED: Martin O'Mullane
C		- Declare variables in ADAS standard way.
C		- Change parameter list of beoutg.
C
C-----------------------------------------------------------------------
       integer   ndlev  , ndprof , nddens , ndzcyl , ndval  , ndcyl  , 
     &           iunt10 , iunt11 , ics    , pipein ,                   
     &           iden   , iunt12 , iunt13 ,                            
     &           pipeou , nzdim  , ndtrn  , ndtem  , ntemp  , one    , 
     &           iz     , grpsel          
C-----------------------------------------------------------------------
       parameter(ndlev  = 100 , ndprof = 100 , nddens = 45     ,
     &           ndzcyl = 100 , ndval  = 10  , ndcyl  = 100    ,
     &           iunt10 = 10     ,
     &           iunt11 = 11  , pipein = 5   , pipeou = 6      ,
     &           iunt12 = 12  , one    = 1   )
       parameter(nzdim  = 1   , ndtrn  = 2000, ndtem = 101     ,
     &           iunt13 = 13)
C-----------------------------------------------------------------------
       integer   i4unit      , help2     , jj          , j0          , 
     &           lupnr       , ldnnr     , num         , njlevx      ,
     &           toggle_geo  , toggle_den, iprofile    , profile     , 
     &           ifail       , nion      , idone       , icancel     , 
     &           imenu       , txtout    , nener       , texout      , 
     &           scanopt     , steps     , numet       , midval    
       integer   i           , j         , k           , l           , 
     &           m           , n         , z           , outbut      , 
     &           izc         , iz0       , iz1         , il          ,
     &           s
C-----------------------------------------------------------------------
       real*8    A           , aa        , Aescap      , alphelp     , 
     &           expop       , f         , help        , help1       , 
     &           h11         , kk        , lam         , ll          , 
     &           nn          , thetacyl  , te          , tg          ,
     &           tte         , ttg       , underp      , w           ,
     &           alpha       , den       , mm          , zlen        ,
     &           y0          , tempti    , tempte      , len         , 
     &           zeff    
       real*8    xx1         , yy1       , xx2         , yy2         ,
     &           xx3         , yy3       , xx4         , yy4         ,   
     &           wid         , totp      , minst       , maxst       ,
     &           dmult       , bwno      , grad2       , grad4       , 
     &           b2          , b4
C-----------------------------------------------------------------------
       logical   open05      , open06    , open10      , open11      , 
     &           open12      , open13  
       logical   lpend       , lpsel     , lzsel       , lisel       , 
     &           lhsel       , lrsel     , lfsel
C-----------------------------------------------------------------------
       character lllstr*140  , string*9  , date*8      , dsfull*80   ,
     &           savfil*80   , rep*3     , title*40    , titlx*120   , 
     &           titlm*80    , texdsn*80 , profname*20 , geoname*10  , 
     &           densname*15 , cadas*80  , titled*3    
C-----------------------------------------------------------------------
       integer   jlev(ndlev)   , nind(nzdim)   , inda(ndtrn,nzdim)   , 
     &           nspec(nzdim)  , lev(2,10)     , ia(ndlev)           , 
     &           isa(ndlev)    , ila(ndlev)    , wrnl(ndlev)         , 
     &           wrnu(ndlev)
C-----------------------------------------------------------------------
       real*8    x0(nddens)    , x1(nddens)    , x2(10,ndval)        ,
     &           y2(10,ndval)  , sum(nddens)   , sum1(nddens)        ,
     &           sum2(nddens)  , sum3(nddens)  , sum4(nddens)        ,
     &           elev(ndlev)   , nm(ndlev)     , bndls(ndlev)        ,
     &           tea(ndtem)    , tpa(ndtem)    , tha(ndtem)          ,
     &           densa(ndtem)  , denspa(ndtem) , ratha(ndtem)        ,
     &           ratia(ndtem)  , x(3000)       , y(3000)             ,
     &           coef(ndtrn,ndtem,nzdim)       , popar(ndlev,ndtem)
       real*8    densarr(ndlev), ratio(10,ndlev)                     ,
     &           flxesc(ndlev,ndlev)           , flxn(ndlev,ndlev)   ,
     &           ratio1(ndlev) , modprof(30,3000)                    ,
     &           xja(ndlev)    , wa(ndlev)     , wrnalp(ndlev)
C-----------------------------------------------------------------------
       logical   lfxist(nzdim)
C-----------------------------------------------------------------------
       character spec(ndlev,nzdim)*51          , scratch(nzdim)*80   , 
     &           cstrga(ndlev)*18
C-----------------------------------------------------------------------
       data underp / 0.1 /
       data open05/.true./ , open06/.true./ , open10/.false./ ,
     &      open11/.false./, open12/.false./, open13/.false./ ,
     &      cadas/' '/
C-----------------------------------------------------------------------
C ***************************** MAIN PROGRAM ***************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C  Set machine dependent ADAS configuration values
C-----------------------------------------------------------------------

       call xx0000

C-----------------------------------------------------------------------
C  Fetch date.
C-----------------------------------------------------------------------

       call xxdate(date)

C-----------------------------------------------------------------------
C  Obtain input file name from IDL
C-----------------------------------------------------------------------

  100   continue
  
        call bespf0(rep, dsfull)
        if(rep.eq.'YES')  goto 9999

C-----------------------------------------------------------------------
C  Obtain user input data from IDL
C-----------------------------------------------------------------------

  200   continue
  
        call beispf(mm, toggle_geo, toggle_den, tempti, tempte,
     &             den, len, bndls, ndlev, njlevx ,
     &             lpend, profile, dmult, zlen,
     &             scanopt, steps, minst, maxst, lev, numet,
     &             densarr )

C-----------------------------------------------------------------------
C  Read signal from IDL for immediate termination
C-----------------------------------------------------------------------
        read(pipein, *) idone
        read(pipein, *) icancel
        read(pipein, *) imenu

        if (icancel.eq.1) goto 100
        if (imenu.eq.1) goto 9999

C-----------------------------------------------------------------------
C  Obtain output file name from IDL
C-----------------------------------------------------------------------

  300  continue
  
       if (open10) close(10)
       open10 = .false.
       open (unit=iunt10,file=dsfull,status='OLD')
       open10 = .true.

       read(pipein, *)idone
       read(pipein, *)icancel
       read(pipein, *)imenu

       if (icancel.eq.1) goto 200
       if (imenu.eq.1) goto 9999

C-----------------------------------------------------------------------
C    Read in output file names and paper file name from IDL
C-----------------------------------------------------------------------

       read(pipein,*)outbut
       if (outbut.eq.1) read(pipein,*)grpsel
       read(pipein,*)texout
       if (texout.eq.1) then
          read(pipein,'(A)')texdsn
          call xxadas( cadas )
       endif
       read(pipein,*)txtout
       if (txtout.eq.1) then
          read(pipein,'(A)')savfil
       endif

C-----------------------------------------------------------------------
C   Open output file, and scratch file for results from code
C-----------------------------------------------------------------------

       if (scanopt.eq.1.and.txtout.eq.1) then
          if (open11) close(11)
          open11 = .false.
          open (unit=iunt11,file=savfil,status='unknown')
          open11 = .true.
       endif
       scratch(1)='adas214_scratch'
       if (open12) close(12)
       open12 = .false.
       open (unit=iunt12, file=scratch(1), status='unknown')
       open12 = .true.

C-----------------------------------------------------------------------
C  Execute calculation of the escape factors, define input choices
C-----------------------------------------------------------------------

       if (profile.eq.0) then
          iprofile=1
          profname='Doppler'
       endif
       if (profile.eq.1) then
          iprofile=2
          profname='Lorentzian'
       endif
       if (profile.eq.2) then
          iprofile=3
          profname='Holtzmark'
       endif
       if (profile.eq.3) then
          iprofile=10
          profname='Double Doppler'
       endif
       if (profile.eq.4) then
          iprofile=-2
          profname='Voigt'
       endif
       if (profile.eq.5) then
          iprofile=-3
          profname='Doppler-Holtzmark'
       endif
       ics=toggle_geo+1
       iden=toggle_den+1
       if (ics.eq.1) geoname='Sphere'
       if (ics.eq.2) geoname='Disk'
       if (ics.eq.3) geoname='Cylinder'
       if (iden.eq.1) densname='Homogeneous'
       if (iden.eq.2) densname='Linear'
       if (iden.eq.3) densname='Parabolic'
       ll=len
       ttg=tempti
       tte=tempte
       te=tte/11605.0
       tg=ttg/11605.0
       
       wid=100*0.000077*dsqrt(tg/mm)*1000
       kk = 1.602D-19/8065.0D0/1.38D-23

C-----------------------------------------------------------------------
C  calculate normalised escape factor theta (output as sum4)
C-----------------------------------------------------------------------
       call escape( ndprof, nddens, ndcyl , iz,
     &              tg    , j0    , mm    ,
     &              x0    , x1    , y0    ,
     &              sum1  , sum2  , sum3  ,sum4   , alpha   ,
     &              ics   , iden  , sum   , iprofile, zlen, y,
     &              x, wid, dmult, modprof)

C--------------------------------------------------------------------
C  linear extrapolation to higher optical depths
C--------------------------------------------------------------------
       grad2=(dlog10(sum2(j0-1))-dlog10(sum2(j0)))
     &       /(dlog10(x0(j0-1))-dlog10(x0(j0)))
       grad4=(dlog10(sum4(j0-1))-dlog10(sum4(j0)))
     &       /(dlog10(x0(j0-1))-dlog10(x0(j0)))
       b2=dlog10(sum2(j0))-(grad2*dlog10(x0(j0)))
       b4=dlog10(sum4(j0))-(grad4*dlog10(x0(j0)))
       do i=j0+1,j0+10
          x0(i)=0.0001*(10**(i/4.))
          sum2(i)=grad2*dlog10(x0(i))+b2
          sum4(i)=grad4*dlog10(x0(i))+b4
          sum2(i)=10**sum2(i)
          sum4(i)=10**sum4(i)
       end do

C-----------------------------------------------------------------------
C  read  ADAS adf04 input file
C-----------------------------------------------------------------------
       call d7data(iunt10, ndlev, ndtrn,
     &             titled, izc   , iz0  , iz1  , bwno ,
     &             il    ,
     &             ia    , cstrga, isa , ila  , xja  , wa
     &             )
       num=il
       do i=1,il
          jlev(i)=2.0*xja(i)+1.0
          elev(i)=wa(i)
       end do

       if (open10) close(10)
       open10 = .false.
       open (unit=iunt10,file=dsfull,status='OLD')
       open10 = .true.
       
       read(iunt10,'(1a140)')lllstr
       if (scanopt.eq.1.and.txtout.eq.1) write(iunt11,'(1a140)')lllstr
       write(iunt12,'(1a140)')lllstr

   10  read(iunt10,'(1a140)')lllstr
       if (scanopt.eq.1.and.txtout.eq.1) write(iunt11,'(1a140)')lllstr
       write(iunt12,'(1a140)')lllstr
       if(lllstr(1:5).ne.'   -1') go to 10

C-----------------------------------------------------------------------
C  calculate partition function , Boltzmann densities of metastables and
C  other (possibly underpopulated) levels.  All levels not chosen to be
C  metastables are given population densities of 1/10th their Boltzmann
C  densities
C-----------------------------------------------------------------------

       if (scanopt.eq.1) then
          nn=den
          steps=0
       endif
       midval=1+steps/2
       
       do 280 l=1,steps+1
            
          s=0
          if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1) then
             if (open10) close(10)
             open10 = .false.
             open (unit=iunt10,file=dsfull,status='OLD')
             open10 = .true.
             if (open11) close(11)
             open11 = .false.
             open (unit=iunt11,file=savfil,status='unknown')
             open11 = .true.
             read(iunt10,'(1a140)')lllstr
             write(iunt11,'(1a140)')lllstr
   15        read(iunt10,'(1a140)')lllstr
             write(iunt11,'(1a140)')lllstr
             if(lllstr(1:5).ne.'   -1')then
                 go to 15
             endif
          endif
          if (scanopt.eq.0) nn=densarr(l)
          if (l.gt.1) then
              if (open10) close(10)
              open10 = .false.
              open (unit=iunt10,file=dsfull,status='OLD')
              open10 = .true.
              if (open12) close(12)
              open12 = .false.
              open(unit=iunt12,file=scratch(1),status='unknown')
              open12=.true.
   20         read(iunt10,'(1a140)')lllstr
              write(iunt12,'(1a140)')lllstr
              if(lllstr(1:5).ne.'   -1') go to 20
          endif
          
          help = 0.0
          h11  = 0.0
          do i=1,num
            if (bndls(i).eq.0) bndls(i)=underp
            nm(i)=jlev(i)*dexp(-elev(i)*kk/tte)
            nm(i)=bndls(i)*nm(i)
            if(bndls(i).gt.underp) h11=h11+nm(i)
            help=help+nm(i)
          end do
       
          expop=1.0-h11/help
          do i=1,num
            nm(i)=nn*nm(i)/help
          end do
          
C-----------------------------------------------------------------------
C  read temperatures and then each transition
C  gets A-value and calculates wavelength, oscillator strength, doppler
C  width and absorption coefficient for each transition.
C-----------------------------------------------------------------------
       
          read(iunt10,'(1a140)')lllstr                                    
          if (scanopt.eq.1.and.txtout.eq.1) 
     &        write(iunt11,'(1a140)')lllstr 
          if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)               
     &        write(iunt11,'(1a140)')lllstr                               
          write(iunt12,'(1a140)')lllstr                                   
          
          m=0
   50     read(iunt10,'(1a140)')lllstr
          m=m+1
          if(lllstr(1:1).ne.' ') goto 80
          read(lllstr,'(t2,i3,t6,i3,t10,f4.2,t14,i3)')lupnr,
     &          ldnnr,help1,help2
          if (lupnr.lt.0) goto 80
          A=help1*10.0d0**help2
          lam=0.01d0/(elev(lupnr)-elev(ldnnr))*1.0d9
          f=A*jlev(lupnr)*lam*lam/jlev(ldnnr)/6.670249d13
          aa=8.85D-20*lam**2
          w=lam*0.000077*dsqrt(tg/mm)
          alphelp=nm(ldnnr)*f*ll*aa*y0/w*wid
          if ((scanopt.eq.1).or.(scanopt.eq.0.and.l.eq.midval)) then
             do j=1,numet
                if ((ldnnr.eq.lev(2,j)).and.
     &             (lupnr.eq.lev(1,j)))then
                   x2(j,1)=alphelp
                   x2(j,2)=alphelp
                   y2(j,1)=1.0
                   y2(j,2)=1e-12
                endif
             end do
          endif

C-----------------------------------------------------------------------
C  interpolate values for theta
C  (uses the results from escape to find which escape factor corresponds
C  to an equivalent optical depth to alphelp, just calculated)
C-----------------------------------------------------------------------
          jj=0
          do j=3,(j0+10)
            if(alphelp.ge.x0(j-1).and.alphelp.lt.x0(j))jj=j
          end do
          
          if(alphelp.gt.x0(j0+10))then
              s=s+1
              wrnl(s)=ldnnr
              wrnu(s)=lupnr
              wrnalp(s)=alphelp
              thetacyl=0.0
              goto 75
          endif
          if(jj.eq.0)then
             thetacyl=1.0
          else
              xx1 = log(x0(jj-2))
              yy1 = log(sum4(jj-2))
              xx2 = log(x0(jj-1))
              yy2 = log(sum4(jj-1))
              xx3 = log(x0(jj))
              yy3 = log(sum4(jj))
              xx4 = log(alphelp)
              call qpoly(xx1,yy1,xx2,yy2,xx3,yy3,xx4,yy4)
              thetacyl = dexp(yy4)
          endif
          
   75     Aescap=thetacyl*A
          if (thetacyl.eq.0.0) Aescap=1.0e-30
          if (Aescap.eq.A) then
              if (scanopt.eq.1.and.txtout.eq.1)
     &              write(iunt11,'(1a140)')lllstr
              if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)
     &              write(iunt11,'(1a140)')lllstr
              write(iunt12,'(1a140)')lllstr
          else
              write(string,'(1pd9.2)')Aescap
              lllstr(9:13)  = string(1:5)
              lllstr(14:16) = string(7:9)
              if (scanopt.eq.1.and.txtout.eq.1)
     &              write(iunt11,'(1a140)')lllstr
              if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)
     &              write(iunt11,'(1a140)')lllstr
              write(iunt12,'(1a140)')lllstr
          endif
          go to 50

C-----------------------------------------------------------------------
C  complete output file with annotation
C-----------------------------------------------------------------------
   80     if (scanopt.eq.1.and.txtout.eq.1)
     &           write(iunt11,'(1a140)')lllstr
          if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)
     &           write(iunt11,'(1a140)')lllstr
          write(iunt12,'(1a140)')lllstr
   90     read(iunt10,'(1a140)',end=101)lllstr
          if (scanopt.eq.1.and.txtout.eq.1)
     &           write(iunt11,'(1a140)')lllstr
          if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)
     &           write(iunt11,'(1a140)')lllstr
          write(iunt12,'(1a140)')lllstr
          go to 90
  101     if (scanopt.eq.1.and.txtout.eq.1)
     &           write(iunt11,110)date,dsfull,densname,geoname,
     &                             profname,mm,tte,ttg,ll,nn
          if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)
     &           write(iunt11,110)date,dsfull,densname,geoname,
     &                             profname,mm,tte,ttg,ll,nn
          write(iunt12,110)date,dsfull,densname,geoname,profname,
     &                      mm,tte,ttg,ll,nn
  110     format('C',79('-')/'C  ',1a8/
     &           'C  produced with ADAS214 and the following data:'/
     &           'C  input data file: ',a60/
     &           'C  dens distn: ',1a15/
     &           'C  geometry: ',1a10/
     &           'C  profile: ',1a20/
     &           'C  m = ',f3.0,', Te = ',f9.1,'K, Tg = ',f9.1,
     &           'K, l = ',1pd10.2,'cm,'/
     &           'C  na or nion = ',1pd10.2,'cm-3'/
     &           'C',79('-'))

          if ((s.gt.0).and.(txtout.eq.1)) then
               if (l.eq.midval) then
                  write(iunt11,112)
                  do i=1,s
                     write(iunt11,115)wrnu(i),wrnl(i),wrnalp(i)
                  end do
                  write(iunt11,113)
               endif
  112     format('C Warning : Following transitions too optically ', 
     &           'thick for escape factor calculation.'/,
     &           'C           A-value set to zero :')
  113     format('C',79('-'))
  115     format('C  ',i2,' -',i2, '  :  optical depth = ',e9.2)
          endif

          if (scanopt.eq.1.and.txtout.eq.1) close(iunt11)
          if (scanopt.eq.0.and.l.eq.midval.and.txtout.eq.1)
     &        close(iunt11)
          close(iunt12)
          close(iunt10)
          open12=.false.
          open10=.false.

C-----------------------------------------------------------------------
C  calculate level populations using modified adf04 file
C-----------------------------------------------------------------------
       
          ifail=0
          nion=1
          lpsel=.false.
          lzsel=.false.
          lisel=.false.
          lhsel=.false.
          lrsel=.false.
          do z=1,nion
             lfxist(z)=.true.
          end do
          ntemp=1
          do k=1,ntemp
             tea(k)=te
             tpa(k)=tg
             tha(k)=tg
             densa(k)=nn
             denspa(k)=0.0
             ratha(k)=0.0
             ratia(k)=0.0
          end do
          zeff=1.0

          call bexcoef(scratch,   ifail ,   lfxist,   nion  ,
     &                 ntemp  ,   inda  ,   nind  ,   nspec ,
     &                 lpsel  ,   lzsel ,   lisel ,   lhsel ,
     &                 lrsel  ,   tea   ,   tpa   ,   tha   ,
     &                 densa  ,   denspa,   ratha ,   ratia ,
     &                 zeff   ,   coef  ,   spec  ,   popar )


          totp=0.
          do i=1,num
             totp=popar(i,1)+totp
          end do
          do i=1,num
             nm(i)=nn*popar(i,1)/totp
          end do


          if (scanopt.eq.1) goto 320

C-------------------------------------------------------------
C calculate emergent flux escape factor, using modified populations
C note: only performed if scanning is selected
C-------------------------------------------------------------

C open adf04 input file

          if (open10) close(10)
          open10 = .false.
          open (unit=iunt10,file=dsfull,status='OLD')
          open10 = .true.
          
C-----------------------------------------------------------------------
C  read temperatures and then each transition
C  gets A-value and calculates wavelength, oscillator strength, doppler
C  width and absorption coefficient for each transition.
C-----------------------------------------------------------------------
  180     read(iunt10,'(1a140)')lllstr
          if(lllstr(1:5).ne.'   -1') go to 180
          read(iunt10,'(1a140)')lllstr
  190     read(iunt10,'(1a140)')lllstr
          if(lllstr(1:1).ne.' ') goto 240
          read(lllstr,'(t2,i3,t6,i3,t10,f4.2,t14,i3)')lupnr,
     &         ldnnr,help1,help2
          if (lupnr.lt.0) goto 240
          A=help1*10.0d0**help2
          lam=0.01d0/(elev(lupnr)-elev(ldnnr))*1.0d9
          f=A*jlev(lupnr)*lam*lam/jlev(ldnnr)/6.670249d13
          aa=0.0
          w=0.0
          alphelp=0.0
          aa=8.85D-20*lam**2
          w=lam*0.000077*dsqrt(tg/mm)
          alphelp=nm(ldnnr)*f*ll*aa*y0/w*wid

C-----------------------------------------------------------------------
C  interpolate values for theta
C  (uses the results from escape to find which escape factor corresponds
C  to an equivalent optical depth to alphelp, just calculated)
C-----------------------------------------------------------------------
          jj=0
          do j=3,(j0+10)
             if(alphelp.ge.x0(j-1).and.alphelp.lt.x0(j))jj=j
          end do
          if(alphelp.gt.x0(j0+10))then
             thetacyl=0.0
             goto 225
          endif
          if(jj.eq.0)then
              thetacyl=1.0
          else
              xx1 = log(x0(jj-2))
              yy1 = log(sum2(jj-2))
              xx2 = log(x0(jj-1))
              yy2 = log(sum2(jj-1))
              xx3 = log(x0(jj))
              yy3 = log(sum2(jj))
              xx4 = log(alphelp)
              call qpoly(xx1,yy1,xx2,yy2,xx3,yy3,xx4,yy4)
              thetacyl = dexp(yy4)
          endif

  225     Aescap=thetacyl*A
          if (thetacyl.eq.0.0) Aescap=1.0e-30
          do j=1,numet
             if (ldnnr.eq.lev(2,j).and.lupnr.eq.lev(1,j)) then
                flxesc(j,l)=Aescap
                flxn(j,l)=nm(lupnr)
             endif
          end do
          goto 190

  240     continue
          close(iunt10)

  280  continue



C---------------------------------------------------------------------
C Calculate emergent flux ratios
C---------------------------------------------------------------------
       do m=1,numet
          do n=1,steps
              ratio(m,n)=((flxn(m,n)/flxn(1,n))*
     &                   (flxesc(m,n)/flxesc(1,n)))
          end do
       end do

C-------------------------------------------------------------------------
C  Tell IDL to continue
C-----------------------------------------------------------------------
  320  continue
       write(pipeou,*) one
       call xxflsh(pipeou)

C-----------------------------------------------------------------------
C  Delete scratch file
C-----------------------------------------------------------------------
       if (open12) close(12)
       open12 = .false.
       open (unit=iunt12,file=scratch(1),status='unknown')
       open12 = .true.
       close (iunt12, status='delete')
       open12 = .false.

C-----------------------------------------------------------------------
C   write paper.txt file with ratio results
C-----------------------------------------------------------------------
       if (texout.eq.1) then

           if (open10) close(10)
           open10 = .false.
           open (unit=iunt10,file=dsfull,status='unknown')
           open10 = .true.

           if (open13) close(13)
           open13 = .false.
           open (unit=iunt13,file=texdsn,status='unknown')
           open13 = .true.

           write(iunt13,325) cadas(2:80)
  325      format(a79)

           write(iunt13,326)'ESCAPE FACTORS ',
     &                      'ADAS214',date
  326      format(27('*'),' TABULAR OUTPUT FROM ',A22,
     &                    ' PROGRAM: ', A7,1X,'- DATE: ',
     &                     A8,1X,27('*')/)

  330      read(iunt10,'(1a140)')lllstr
           write(iunt13,'(1a140)')lllstr
           if (lllstr(1:5).ne.'   -1') go to 330

  340      read(iunt10,'(1a140)')lllstr
           if (lllstr(1:1).ne.'C') goto 340
           write(iunt13,'(1a140)')lllstr

           write(iunt13,350)
           write(iunt13,360) (x0(i) , sum4(i), i=1,(j0+10))
           write(iunt13,365)
           write(iunt13,355)
           write(iunt13,360) (x0(i) , sum2(i), i=1,(j0+10))
           write(iunt13,365)
  350      format(4x,'Optical Depth',2x,'Population Escape Factor',/)
  355      format(4x,'Optical Depth',2x,'Emergent Flux Escape Factor',/)
  360      format(3x,1p,e10.3,5x,e10.3)
  365      format('C',79('-'))

           write(iunt13,370)
  370      format(4x,'Profile Intensity',4x,
     &            'Wavelength(from line centre) /pm')
           write(iunt13,380) (y(i) , x(i), i=1,iz)
           write(iunt13,365)
  380      format(3x,1p,e10.3,11x,e10.3)

           if (scanopt.eq.0) then
              write(iunt13,390)
              write(iunt13,400) (densarr(i)*len , i=1,steps)
              do j=1,numet
                 write(iunt13,410)lev(1,j), lev(2,j),
     &                    (ratio(j,i), i=1,steps)
  410            format(1p, i3, 3x, i3, 18x, 15e12.4)
              end do
              write(iunt13,365)
  390         format(3x, 'Transition',15x,'Column Density')
  400         format(/'Upper',3x,'Lower',5x, '  (CM-2)=', 1P, 15e12.4)
              
              write(iunt13,395)
  395         format(/'Upper',3x,'Lower',16x,'Absorption factor')
              do j=1,numet
                 write(iunt13,410)lev(1,j), lev(2,j),x2(j,1)
              end do
  
           endif

           if (scanopt.eq.0) then
              write(iunt13,430)date,dsfull,densname,geoname,
     &                         profname,mm,tte,ttg,ll,densarr(midval)
           endif
           if (scanopt.eq.1) then
             write(iunt13,430)date,dsfull,densname,geoname,
     &                      profname,mm,tte,ttg,ll,nn
           endif
  430      format('C',79('-')/'C  ',1a8/
     &            'C  produced with ADAS214 and the following data:'/
     &            'C  input data file: ',a60/
     &            'C  dens distn.: ',1a15/
     &            'C  geometry: ',1a10/
     &            'C  profile: ',1a20/
     &            'C  m = ',f3.0,', Te = ',f9.1,'K, Tg = ',f9.1,
     &            'K, l = ',1pd10.2,'cm,'/
     &            'C  na or nion = ',1pd10.2,'cm-3'/
     &            'C',79('-')
     &            )
           close(iunt13)
           close(iunt10)
       
       endif

C-------------------------------------------------------------
C pipe values to be graphed to IDL
C-------------------------------------------------------------

       if (outbut.eq.1) then
          title='what1?'
          titlx=dsfull
          titlm='what2?'
          if (grpsel.eq.0) then
            nener=1

            call beoutg(titlx   , titlm   , date , 
     &                  x0      , sum4    , nddens  ,        
     &                  grpsel  , numet   , steps   ,        
     &                  ratio   , densarr , modprof , ndlev  ,       
     &                  x2      , y2      , sum2    , nddens )       
          endif

          if (grpsel.eq.1) then
            nener=1
            call beoutg (titlx   , titlm   , date    ,
     &                   x       , y       , iz      ,
     &                   grpsel  , numet   , steps   ,
     &                   ratio   , densarr , modprof , ndlev  ,
     &                   x2      , y2      , sum2    , nddens )
          endif

          if (grpsel.eq.2) then
            nener=1
            do i=1,steps
               ratio1(i)=ratio(2,i)
            end do
            call beoutg (titlx  , titlm   , date    ,
     &                  densarr , ratio1  , steps   ,
     &                  grpsel  , numet   , steps   ,
     &                  ratio   , densarr , modprof , ndlev  ,
     &                  x2      , y2      , sum2    , nddens )
          endif
       else
          goto 300
       endif

       read (pipein,*)imenu
       if (imenu.eq.0) goto 300

C-------------------------------------------------------------
 9999  stop
C-------------------------------------------------------------
       end
