CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas214/qpoly.for,v 1.1 2004/07/06 14:39:23 whitefor Exp $ Date $Date: 2004/07/06 14:39:23 $
CX
       subroutine qpoly( x1, y1, x2, y2, x3, y3, x, y)
       implicit none
c
c------------------------------------------------------------------
C  **************FORTRAN77 SUBROUTINE:QPOLY ************
C VERSION 1.0
C
C PURPOSE :QUADRATIC INTERPOLATION OF X1,Y1,X2,Y2,X3,Y3 -> Y FOR GIVEN X 
C
C INPUT : 
C	 (R*8)	X1 = 
C	 (R*8)	Y1 = 
C	 (R*8)	X2 = 
C	 (R*8)	Y2 = 
C	 (R*8)	X3 = 
C	 (R*8)	Y3 =
C 
C ROUTINES:
C
C AUTHOR: K.H. BEHRINGER (IPF, UNIVERSITY OF STUTTGART)
C
C DATE 02/04/98 
C
C VERSION: 1.1						DATE: 18-06-98
C MODIFIED:	STUART LOCH
C		- CONVERTED TO FORTRAN FOR ADAS.
C  
c-----------------------------------------------------------------
       real*8	x1,	y1,	x2,	y2,	
     & 		x3,	y3,	x,	y,
     & 		DY2,	DY1,	DX2,	DX1,
     &		DX0,	c,	b,	a
c-----------------------------------------------------------------
       DY2 = y3 - y2
       DY1 = y2 - y1
       DX2 = x3 - x2
       DX1 = x2 - x1
       DX0 = x3 - x1
       c = (DY2/DX2 - DY1/DX1)/DX0
       b = (DY1*(x3+x2)/DX1 - DY2*(x2+x1)/DX2)/DX0
       a = y2-b*x2-c*x2**2
       y = a+b*x+c*x**2
c
       return
       End
	
