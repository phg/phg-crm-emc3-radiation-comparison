CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas214/escape.for,v 1.5 2010/06/30 15:29:14 mog Exp $ Date $Date: 2010/06/30 15:29:14 $
CX
       subroutine escape( ndprof, nddens, ndcyl , iz    ,
     &                    tg    , j0    , mm    , x0    , x1    ,
     &                    y0    , sum1  , sum2  , sum3  ,  
     &                    sum4  , alpha   , ics , iden    ,
     &                    sum   , iprofile , zlen, y, x, wid,
     &			  dmult, modprof)
       implicit none
c
c------------------------------------------------------------------------------
c
c  *********&********** fortran77 subroutine: escape **************************
c
c  original name: escape.bas
c                 (developed by K. H. Behringer)
c
c  version:  1.0
c
c  purpose: computes escape factors.
c
c  calling program: adas214
c
c  input:
c          (i*4)  nddens  = parameter = max. number of points over ion profile 
c          (i*4)  ndcyl   = parameter = max. number of intervals for cylinder
c          (i*4)  ndprof  = parameter = max. number of points over line profile
c
c	   (i*4)  ics     = 1 for slab geometry of plasma
c	                  = 2 for cyl. geometry of plasma
c          (i*4)  iden    = 1 for homogenous density distribution
c                         = 2 for linear density 
c                         = 3 for parabolic density 
c          (i*4)  iprofile= 1 for Doppler line profile
c                         = 2 for Lorentzian line profile
c                         = 3 for Holtzmark line profile
c                         = 10 for Double Doppler line profile
c                         = -2 for Voigt line profile
c                         = -3 for Doppler Holtzmark line profile
c          (i*4)  iz      = number of points over line profile
c          (i*4)  j0      = number of density values for integration over 
c                           cylinder/slab 
c          (i*4)  k0      = number of density values - must be enough for 
c                           interpolation
c          (i*4)  z0      = number of intervals for cylinder/slab
c
c          (r*8)  allam   = Spectral absorption coefficient
c          (r*8)  alpha   = neutral density*oscillator strength*length
c          (r*8)  domeg   = delta omega in cylinder/slab
c          (r*8)  l       = Length of plasma (0 to inf. on one side)
c          (r*8)  mm      = atomic mass number
c          (r*8)  omega   = solid angle in cylinder/slab
c          (r*8)  r       = radius of cylinder
c          (r*8)  sum1()  = sum assuming homogeneous density 
c          (r*8)  sum2()  = sum assuming linear density 
c          (r*8)  sum3()  = sum assuming parabolic density 
c          (r*8)  sum4()  = used in sum for escape factor 
c          (r*8)  sum()   = sum1/2/3, depending upon options chosen for density profile
c          (r*8)  tg      = neutral temperature  (eV)
c          (r*8)  w       = Full Doppler width calculated for lambda=100nm
c          (r*8)  x0()    = Absorption coefficient at line centre/10
c          (r*8)  x1()    = Absorption coefficient at line centre
c          (r*8)  y       = Line profile intensity
c          (r*8)  y0      = Line profile intensity (?) at line centre
c          (r*8)  ff      = Used in calculation of escape factor
c          (r*8)  xl      = log of x1, used for interpolation
c          (r*8)  yl      = log of sum, used in interpolation
c          (r*8)  zlen    = variable used to select length along plasma integration to be performed
c
c
c  routines:
c           routine     source     brief description
c           -------------------------------------------------------------------
c           faltung     ADAS       computes line profiles
c
c  author:  K. H. Behringer  (IPF, University of Stuttgart)
c
c  date:    30/01/94
c
c  update:  ????	brought up to date with latest Behringer code
c
c  VERSION: 	1.1					DATE: 18-06-98
c  MODIFIED: STUART LOCH
c		- CONVERTED TO FORTRAN
c             
c  VERSION: 	1.2					DATE: 26-11-98
c  MODIFIED: STUART LOCH
c		- CHECKS IF LINE PROFILE IS INTEGRATED FAR ENOUGH,
c		  IF NOT THE PROFILE IS RE-EVALUATED TO TWICE THE
c		  PREVIOUS WIDTH, USING THE SAME STEP SIZE AS BEFORE.
c  VERSION: 1.3                                         DATE: 19-02-99
c  MODIFIED: STUART lOCH
c               -DIMENSIONS OF X,Y, YT, YL AND MODPROF INCREASED FROM 1000
c                TO 3000, TO ACCOMODATE POSSIBLE INCREASES IN INTEGRATION
c                LIMIT OF LINE PROFILE
c  VERSION: 1.4                                         DATE: 24-09-99
c  MODIFIED: STUART lOCH
c               -REMOVED EMPTY DO LOOP
C            
c  VERSION : 1.5                                         
c  DATE    : 30-06-2010
c  MODIFIED: Martin O'Mullane
c               -Relace 1e-99 with 1.0D-99 to enable use of Lahey compiler.
C            
c------------------------------------------------------------------------------
       integer  iz    , itdens , iden , iprofile, k0
c------------------------------------------------------------------------------
       parameter ( itdens=40, k0 = 30)
c------------------------------------------------------------------------------
       integer  ndprof , nddens , ndcyl  , i4unit ,
     &          j0     , z0     ,
     &          i      , k      , j      , ics    , ii
c------------------------------------------------------------------------------
       real*8    tg    , wid   , 
     &           l     , dl    , ee    , xx    ,
     &           yy    , omega , rr    , theta , ctet  , domeg ,
     &           y0    , alpha , dl0   , r     , zlen 
       real*8    x0(nddens)    , x1(nddens)    ,
     &           sum1(nddens)  , sum(nddens)   ,
     &           sum2(nddens)  , sum3(nddens)  , sum4(nddens)  ,
     &           x(3000)       , y(3000)       , ff            ,
     &           xl(itdens)    , yl(3000)      , yt(3000)
       real*8    dx    , mm    , allam         , rr2 		,
     &		 xmax  , xmin  , w2f, fact, dmult,
     &		 modprof(k0,3000), yfirst, ylast, profmult
c------------------------------------------------------------------------------
       j0 = k0-1
       z0 = ndcyl
       iz = 200
c------------------------------------------------------------------------------
c  choose and calculate profile, full width 1, scaled later
c------------------------------------------------------------------------------
c
	profmult=0
	   if (iprofile.eq.1) then
	      profmult=2.1
	      w2f=0
	   endif
c (Doppler)
	   if (iprofile.eq.2) then
	      profmult=100
	      w2f=0
	   endif
c (Lorentzian)
	   if (iprofile.eq.3) then
	      profmult=50
	      w2f=0
	   endif
c (Holtzmark)
	   if (iprofile.eq.10) then
	      profmult=10
	      w2f=dmult*wid
	   endif
c (Double Doppler)
	   if (iprofile.eq.-2) then
	      profmult=40
	      w2f=dmult*wid
	   endif
c (Voigt)
	   if (iprofile.eq.-3) then
	      profmult=15
	      w2f=dmult*wid
	   endif
c (Doppler-Holtsmark)
c
    5	dx=profmult*wid/iz
        xmax=profmult*wid

       do 10 i=1,iz
         x(i) = (i-0.5)*dx
         y(i)  = 0.0
   10  end do
       do 15 i=(iz+1),(3*iz)
         x(i) = 0.0
         y(i) = 0.0
   15  end do
       call voigt(iprofile, y0, yl, yt, x, y, iz, w2f, wid,
     &            fact, xmin, xmax)
       do 16 i=1,iz
         if (y(i).lt.1.0D-99) y(i) = 0.0
   16  end do
c-----------------------------------------------------------------------------
c  Calculation of escape factors for the 3 density types for a range of
c  optical depths.  Values are calculated using lambda = 100nm, 
c  though the value is irrelevant as they are only used to produce
c  the range of optical depths.  The actual optical depth through 
c  the plasma is calculated later. 'sum' is then made to be the escape 
c  factor for the chosen density distribution.
c-----------------------------------------------------------------------------
c
       yfirst=0.0
       ylast=0.0
       do 40 ii=1,k0
         x1(ii)=0.0001*(10**(ii/4.))
         alpha  = x1(ii)/y0
         sum1(ii) = 0.0d0
         sum2(ii) = 0.0d0
c         sum3(ii) = 0.0d0
         do 30 i=1,iz
           allam=alpha*y(i)
           ee=dexp(-allam)
           if (ee.lt.(0.99)) then
              yy=2.0/(alpha*allam)-(2.0/alpha+2.0/(alpha*allam))*ee
              if (iden.eq.1) then
                 yt(i)=y(i)/2.0/allam*(1.0-dexp(-2.0*allam))
              endif
              if (iden.eq.2) then
		 yt(i) = y(i)*(1.0-2.0*dexp(-allam)+
     &                   dexp(-2.0*allam))/allam**2
              endif
              if (iden.eq.3) then
                 yt(i)=2.0-2.0/allam+(2.0+2.0/allam)*dexp(-2.0*allam)
                 yt(i)=(0.75*y(i)/allam**2.0)*yt(i)
              endif
	      if ((ii.eq.1).and.(i.eq.iz)) yfirst=yt(i)
	      if ((ii.eq.k0).and.(i.eq.iz)) ylast=yt(i)
           else
              yy=y(i)*(1.0-2.0/3.0*allam+(allam**2)/3.0)
              yt(i)=y(i)
	      if ((ii.eq.1).and.(i.eq.iz)) yfirst=yt(i)
	      if ((ii.eq.k0).and.(i.eq.iz)) ylast=yt(i)
c (yt is modified line profile)
           endif
           if (iden.eq.1) sum1(ii)=sum1(ii)+y(i)*ee
           if (iden.eq.2) sum1(ii)=sum1(ii)+1.0/alpha-1.0/alpha*ee
           if (iden.eq.3) sum1(ii)=sum1(ii)+yy
           sum2(ii)=sum2(ii)+yt(i)
   30    end do
	      do 31 i=1,iz
	         modprof(ii,i)=yt(i)
   31	      enddo
         sum1(ii)=2.0*sum1(ii)*dx
         sum2(ii)=2.0*sum2(ii)*dx
         yl(ii)=dlog(sum1(ii))
c         x1(ii)=alpha*y0 /w
         xl(ii)=dlog(x1(ii))

         if (ics.eq.1) then
            x0(ii) = x1(ii)
            sum4(ii) = sum1(ii)
            j0=k0
         endif
   40  end do
       if ((0.95*yfirst).gt.ylast) then
c	  write(i4unit(-1),*)'warning'
	  if (iz.eq.800) goto 45
	  profmult=2*profmult
	  iz=2*iz
	  goto 5
       endif
   45  if (ics.eq.1) goto 9999
c note:can extract modified line profile here^
c-----------------------------------------------------------------------------
c  integration over cylinder/slab - new density loop
c     integration over solid angle of slab/cylinder 
c     using results in sum      
c-----------------------------------------------------------------------------
       do 70 j=1,j0
         sum4(j)=0.0d0
         omega=0.0	
         l=0.0
         r=1.0
         x0(j)=0.0001*(10**(j/4.))
         dl0=zlen*r/z0
         dl=dl0
         do 60 i=1,z0
           l=l+dl/2.0
           rr2=r**2+l**2
           theta=datan2(l,r)
           ctet=dcos(theta)
           if (ics.eq.2) then
             domeg=l*dl*ctet/rr2  
           endif
           if (ics.eq.3) then
             domeg=r*dl*ctet/rr2  
           endif
           l=l+dl/2.0
           if (i.eq.z0)  then 
              rr=0.5*(l+dsqrt(r**2+l**2))
              if (ics.eq.2) domeg=domeg+l*r/(rr**2)
              if (ics.eq.3) domeg=domeg+(r**2)/2.0/(rr**2)
           endif 
           dl=dl0/ctet
           xx=dsqrt(rr2)*x0(j) 
           omega=omega+domeg
           do 50 k=1,k0
             if ((xx.ge.x1(k)).and.(xx.lt.x1(k+1))) then
                ff=dexp(yl(k)+(dlog(xx)-xl(k))/(xl(k+1)-xl(k))*
     &			(yl(k+1)-yl(k)))
               goto 55
             endif
   50      end do
   55      continue 
           if (xx.gt.x1(k0)) then
               ff=dexp(yl(k0-1)+(dlog(xx)-xl(k0-1))/
     &          (xl(k0)-xl(k0-1))*(yl(k0)-yl(k0-1)))
           endif
           sum4(j)=sum4(j)+domeg*ff
c           write(i4unit(-1),*)theta/3.14159*180,rr2,omega,sum4(j)
   60    end do
         sum4(j)=sum4(j)/omega
   70  end do
       return
 9999 end
