CX UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas214/beoutg.for,v 1.3 2005/03/18 15:15:13 mog Exp $ Date $Date: 2005/03/18 15:15:13 $
CX
       SUBROUTINE BEOUTG( TITLX  , TITLM   , DATE    ,
     &                    TEMP   , RATE    , NENER   ,
     &                    GRPSEL , NUMET   , STEPS   ,
     &                    RATIO  , DENSARR , MODPROF , NDLEV  ,
     &                    X2     , Y2      , SUM2    , NDDENS
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BEOUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED TRANSITION USING IDL.
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                ORIGINAL <SE>LIKE.DATA    (CROSSES    )
C                                SPLINE INTERPOLATED DATA  (FULL CURVE)
C                                MINIMAX FIT TO DATA       (DASH CURVE )
C
C            PLOT IS LOG10(RATE(CM**3/SEC))  VERSUS LOG10(TEMP(KELVIN))
C
C  CALLING PROGRAM: ADAS214
C
C  SUBROUTINE:
C
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE and  TRANSITION
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  TEMP()  = INPUT DATA FILE: TEMPERATURES (kelvin)
C  INPUT : (R*8)  RATE()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           EXCITATION RATE COEFF. (cm**3/s) AT 'TEMP()'
C  INPUT : (I*4)  NENER   = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXSLEN      ADAS      GET POSITION OF NON-BLANK CHARACTERS.
C
C AUTHOR:  STUART LOCH, UNIVERSITY OF STRATHCLYDE.
C               BASED ON B1OUTG.FOR  v1.2
C
C DATE:    18/06/98
C
C VERSION: 1.1                                          DATE: 18-06-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL.
C VERSION: 1.2                                          DATE: 26-11-98
C MODIFIED: STUART LOCH
C               - NOW PIPES ACROSS SUM2 TO PLOT FLUX ESCAPE FACTOR
C
C VERSION : 1.3
C DATE    : 15-03-2005
C MODIFIED: Martin O'Mullane
C               - Declare variables in ADAS standard way.
C               - Change parameter list.
C               - Remove redundant variables and code.
C               - Make dimensions compatible with adas214 calling program.
C               - MODPROF is (30,3000) not (30,10000).
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       INTEGER    PIPEOU
C-----------------------------------------------------------------------
       PARAMETER (PIPEOU = 6 )
C-----------------------------------------------------------------------
       INTEGER  NENER        , I           , J         ,
     &          IFIRST       , ILAST       , NDDENS    , STEPS     ,
     &          NDLEV        , NUMET       , GRPSEL
C-----------------------------------------------------------------------
       REAL*8    TEMP(NENER)               , RATE(NENER)           ,
     &           RATIO(10,NDLEV)           , DENSARR(NDLEV)        ,
     &           MODPROF(30,3000)          , X2(10,10)             ,
     &           Y2(10,10)                 , SUM2(NDDENS)
C-----------------------------------------------------------------------
       CHARACTER TITLX*120   , TITLM*80    , DATE*8
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------

C WRITE OUT TITLES AND DATE INFORMATION

       CALL XXSLEN(TITLX, IFIRST, ILAST)
       WRITE(PIPEOU,'(A120)') TITLX(IFIRST:ILAST)
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,'(A80)') TITLM
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,'(A8)') DATE
       CALL XXFLSH(PIPEOU)

C WRITE OUT ARRAY SIZES

       WRITE(PIPEOU,*) NENER
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)NUMET
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)STEPS
       CALL XXFLSH(PIPEOU)


C DATA FROM DATA FILE

       IF (GRPSEL.LT.2) THEN
         DO I = 1, NENER
            WRITE(PIPEOU,*) TEMP(I)
         END DO
         CALL XXFLSH(PIPEOU)
         DO I = 1, NENER
            WRITE(PIPEOU,*) RATE(I)
         END DO
         CALL XXFLSH(PIPEOU)
       ELSE
         DO I=1,NUMET
            DO J=1,STEPS
               WRITE(PIPEOU,*)RATIO(I,J)
               CALL XXFLSH(PIPEOU)
            END DO
         END DO
         DO I=1,STEPS
            WRITE(PIPEOU,*)DENSARR(I)
            CALL XXFLSH(PIPEOU)
         END DO
       END IF

       IF (GRPSEL.EQ.0) THEN
           DO J=1,NUMET
              DO I=1,2
                 WRITE(PIPEOU,*)X2(J,I)
                 CALL XXFLSH(PIPEOU)
                 WRITE(PIPEOU,*)Y2(J,I)
                 CALL XXFLSH(PIPEOU)
              ENDDO
           ENDDO
           DO I=1,NDDENS
              WRITE(PIPEOU,*)SUM2(I)
              CALL XXFLSH(PIPEOU)
           ENDDO
       ENDIF

       IF (GRPSEL.EQ.1) THEN
          DO I=1,30
             DO J=1,NENER
                WRITE(PIPEOU,*)MODPROF(I,J)
                CALL XXFLSH(PIPEOU)
             ENDDO
          ENDDO
       ENDIF


C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
      END
