CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas214/beispf.for,v 1.2 2004/07/06 11:38:10 whitefor Exp $ Date $Date: 2004/07/06 11:38:10 $
CX
       SUBROUTINE BEISPF( MM, TOGGLE_GEO   , TOGGLE_DEN   ,
     &                    TEMPTI  , TEMPTE , DEN    , LEN ,
     &                    BNDLS   , NDLEV  ,
     &                    NJLEVX  , LPEND  , 
     &                    PROFILE , DMULT  , ZLEN    ,
     &			  SCANOPT, STEPS, MINST, MAXST, 
     &			  LEV, NUMET , DENSARR)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  
C  ******************* FORTRAN 77 SUBROUTINE BEISPF ********************
C
C  PURPOSE: READ PLASMA VARIABLES FROM IDL CODE
C           
C
C  AUTHOR: STUART LOCH, UNIV.OF STRATHCLYDE 
C
C  MODIFIED: 1.1						DATE: 18-06-98
C		STUART LOCH
C		PUT UNDER SCCS CONTROL.
C  MODIFIED: 1.2						DATE: 22-05-2002
C		RICHARD MARTIN
C		CORRECTED DIMENSIONING PROBLEM WITH DENSARR.
C
C-----------------------------------------------------------------------
       INTEGER TOGGLE_GEO, TOGGLE_DEN, ILOGIC, NJLEVX 
       INTEGER NDLEV, PIPEIN, PIPEOU, I, J,  PROFILE
       INTEGER SCANOPT, STEPS, NUMET, LEV(2,NDLEV)
C-----------------------------------------------------------------------
       REAL*8  TEMPTI, TEMPTE, DEN, LEN, MM, ZLEN
C-----------------------------------------------------------------------
       REAL*8 BNDLS(NDLEV) , MINST, MAXST,  
     & 	      DENSARR(NDLEV), DMULT
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       PARAMETER(PIPEIN = 5, PIPEOU = 6)      
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE UNLESS CANCEL SELECTED 
C-----------------------------------------------------------------------
       READ(PIPEIN,'(I1)')ILOGIC
         IF (ILOGIC.EQ.1)THEN
           LPEND = .TRUE.
         ELSE
           LPEND = .FALSE.
         ENDIF

       IF (LPEND) GOTO 40
C
       

       READ(PIPEIN,*)MM
       READ(PIPEIN,*)NJLEVX
       READ(PIPEIN,*)TOGGLE_GEO
       READ(PIPEIN,*)TOGGLE_DEN
       READ(PIPEIN,*)TEMPTI
       READ(PIPEIN,*)TEMPTE
       READ(PIPEIN,*)DEN
       READ(PIPEIN,*)LEN
       READ(PIPEIN,*)PROFILE
       READ(PIPEIN,*)DMULT
       READ(PIPEIN,*)ZLEN

       DO 10 I = 1 , NJLEVX
         READ(PIPEIN,*)BNDLS(I)
   10  CONTINUE

       READ(PIPEIN,*)NUMET
       DO 30 I=1,2
	  DO 20 J=1,NUMET
	     READ(PIPEIN,*)LEV(I,J)
   20     CONTINUE
   30  CONTINUE

c	VARIABLES FOR SCANNING
       READ(PIPEIN,*)SCANOPT
       IF (SCANOPT.EQ.0) THEN
         READ(PIPEIN,*)STEPS
         READ(PIPEIN,*)MINST
         READ(PIPEIN,*)MAXST
         DO 35 I=1,(STEPS)
            READ(PIPEIN,*)DENSARR(I)
   35    CONTINUE

       ENDIF

   40  RETURN
       END
