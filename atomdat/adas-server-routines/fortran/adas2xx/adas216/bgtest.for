C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgtest.for,v 1.2 2004/07/06 11:41:19 whitefor Exp $ Date $Date: 2004/07/06 11:41:19 $
C
      SUBROUTINE BGTEST( IZ1    , IL      ,
     &                   IA     , ISA     , ILA   , XJA   , WA   ,
     &                   NV     , SCEF    ,
     &                   ITRAN  , 
     &                   TCODE  , I1A     , I2A   , AVAL  , SCOM ,
     &                   numcom , outcom
     &                 )
      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGTEST *********************
C
C  PURPOSE:   Examines the collison strengths of adf04 files for any 
C             discrepencies, outlyiny points, mistakes etc.
C
C             Three methods are used for checking
C               o  fit a minmax polynominal and flag excessive errors
C               o  find large deviations from a 3-point running average
C               o  find excessive changes in slope
C
C             The first method really checks for smoothness with the
C             second looking for outlying points. (The minmax fit
C             should also spot these). The third is not so successful
C             and care should be taken in using it.
C
C             All collisions and temperatures are transformed by the
C             Burgess-Tully method before the tests are applied.
C
C             Based on off-line test_adf04.for code (Martin O'Mullane,
C             16-2-99).
C
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  
C
C  ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           BGTRAN     ADAS      RETURNS BURGESS-TULLY TRANSFORMED 
C                                TEMPERATURE AND COLLISION STRENGTH
C           I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C   AUTHOR  : MARTIN O'MULLANE,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          DATE: 17/03/1999
C  MODIFIED : MARTIN O'MULLANE
C             FIRST VERSION.
C
C  VERSION  : 1.2                          DATE: 16/11/2001
C  MODIFIED : Martin O'Mullane
C             Problem with comment array being overwritten. Add check
C             but continue to process.
C
C-----------------------------------------------------------------------
       INTEGER    NDLEV  , NDTRN , NVMAX 
       INTEGER    maxdeg 
       INTEGER    nc_1   , nc_2  , nc_3
       real*8     tol_1  , tol_2 , tol_3
C-----------------------------------------------------------------------
       PARAMETER( NDLEV  = 100 , NDTRN  = 2100 , NVMAX  = 14  )
       PARAMETER( maxdeg = 7 ) 
       PARAMETER( tol_1  = 0.1D0 , tol_2 = 0.4D0 , tol_3 = 0.8D0 ) 
       PARAMETER( nc_1   = 800   , nc_2  = 100*nvmax , 
     &            nc_3   = 100*nvmax) 
C-----------------------------------------------------------------------
       INTEGER    ncoef  , nout   , nv     , iz1    , il     
       INTEGER    icnte  , icntp  , icntr  , icnth  , icnti    , itran 
       INTEGER    iout   , k1     , k2     , k3     , it       , kk    ,
     &            i      , numcom
C-----------------------------------------------------------------------
       REAL*8     wtl    , wtu    , ain    , wvnou  , wvnol
       REAL*8     sl_1   , sl_2   , test   , ravg   , accuracy , c      
C-----------------------------------------------------------------------
       character  typ*1  ,  minfo*80    , acc_str*6  , tag_str*1
C-----------------------------------------------------------------------
       LOGICAL    lfail  , logfit
C-----------------------------------------------------------------------
       INTEGER    isa(ndlev)     , ila(ndlev)   , ia(ndlev)   ,  
     &            i1a(ndtrn)     , i2a(ndtrn)
       integer    ietrn(ndtrn)   , ie1a(ndtrn)  , ie2a(ndtrn)  
C-----------------------------------------------------------------------
       real*8     xin(nvmax)     , yin(nvmax)
       real*8     tein(nvmax)    , upsin(nvmax)
       real*8     xout(nvmax)    , yout(nvmax)  , coef(nvmax)
       real*8     aa(ndtrn)      , cea(ndtrn)   ,
     &            xja(ndlev)     , wa(ndlev)    , aval(ndtrn) , 
     &            scef(nvmax)    , scom(nvmax,ndtrn)
C-----------------------------------------------------------------------
       CHARACTER  com_1(nc_1)*80  , com_2(nc_2)*80   , com_3(nc_3)*80
       CHARACTER  outcom(210+200*nvmax)*80  
       character  pecode(ndtrn)*1  , tecode(ndtrn)*1 , tcode(ndtrn)*1 ,
     &            cstrga(ndlev)*18
C-----------------------------------------------------------------------

C initialise the comments counters
       
       k1 = 1   
       k2 = 1   
       k3 = 1   
       
       
C initialise settings for test 1 - minmax polynominal fit
     
       logfit = .FALSE.
       ncoef  = maxdeg+1
       nout   = nv
       
       call bfttyp( ndlev  , ndtrn  ,
     &              iz1    , il     ,
     &              ia     , cstrga , isa    , ila   , xja   , wa ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti ,
     &              ietrn  , pecode , tecode , ie1a  , ie2a  , aa ,
     &              cea
     &            )
              
       
       
C-----------------------------------------------------------------------
C  Cycle over excitation only transitions
C-----------------------------------------------------------------------
       
       do i=1,icnte
       
         c     = cea(i)
         typ   = tecode(i)
         ain   = aa(i)
         wvnou = wa(ie2a(i))
         wvnol = wa(ie1a(i))
         wtu   = 2.0d0*xja(ie2a(i))+1.0D0
         wtl   = 2.0d0*xja(ie1a(i))+1.0D0
         
         do it = 1,nv
           tein(it)  = scef(it)
           upsin(it) = scom(it,i)
         end do

         call  bgtran( typ   , c      ,
     &                 ain   , wvnou  , wvnol , wtu   , wtl  ,
     &                 tein  , upsin  , nv    ,
     &                 xin   , yin 
     &               )
 

C  Test 1: minmax polynominal

          call xxmnmx(logfit , maxdeg , tol_1  , 
     &                nv     , xin    , yin    ,
     &                nout   , xout   , yout   ,
     &                ncoef  , coef   , minfo  )
     
          acc_str = minfo(30:35)
          if (k1.LT.nc_1) then
             if (acc_str.ne.'******') then
                read(acc_str,*)accuracy
                if ( accuracy/1.0D2 .gt. tol_1) then
                   tag_str=' '
                   if (accuracy/1.0D2.gt.2.0D0*tol_1) tag_str='*'
                   write(com_1(k1),300)i,i1a(i),i2a(i),
     &                                  accuracy, tol_1*1.0D2, tag_str
                   k1 = k1 + 1
                endif
             else
                write(com_1(k1),310)i,i1a(i),i2a(i)
                k1 = k1 + 1
             endif
          endif
          
          if (k1.EQ.nc_1) write(com_1(k1),*)'Report limit reached'


C  Test 2: running average

          call xxravg(yin(2),yin(3),yin(4), tol_2, ravg, yin(1), lfail)
          if (lfail.AND.k2.LT.nc_2) then
             write(com_2(k2),350)i,i1a(i),i2a(i),1,yin(1),ravg
             k2=k2+1
          endif
          
          call xxravg(yin(1),yin(3),yin(4), tol_2, ravg, yin(2), lfail)
          if (lfail.AND.k2.LT.nc_2) then
             write(com_2(k2),350)i,i1a(i),i2a(i),2,yin(2),ravg
             k2=k2+1
          endif
          
          do kk = 3,nv-2
          
            call xxravg(yin(kk-2), yin(kk-1), yin(kk+1), tol_2, ravg,
     &                   yin(kk), lfail)
            if (lfail.AND.k2.LT.nc_2) then
               write(com_2(k2),350)i,i1a(i),i2a(i),kk,yin(kk),ravg
               k2=k2+1
            endif
          
          end do
          
          call xxravg(yin(nv-3), yin(nv-2), yin(nv), tol_2, ravg, 
     &                 yin(nv-1), lfail)
          if (lfail.AND.k2.LT.nc_2) then
             write(com_2(k2),350)i,i1a(i),i2a(i),nv-1,yin(nv-1),ravg
             k2=k2+1
          endif
          
          call xxravg(yin(nv-3), yin(nv-2), yin(nv-1), tol_2, ravg, 
     &                 yin(nv), lfail)
          if (lfail.AND.k2.LT.nc_2) then
             write(com_2(k2),350)i,i1a(i),i2a(i),nv,yin(nv),ravg
             k2=k2+1
          endif

          if (k2.EQ.nc_2) write(com_2(k2),*)'Report limit reached'

C  Test 3: changing slope

          
          do kk = 2, nv-1
  
             sl_1 = ( yin(kk) - yin(kk-1) ) / ( xin(kk) - xin(kk-1) ) 
             sl_2 = ( yin(kk+1) - yin(kk) ) / ( xin(kk+1) - xin(kk) ) 

             if (abs(sl_2).GT.0.0) then
                test = 1.0D0 - abs(sl_1)/abs(sl_2)
             else
                test = 1.0D0
             endif
              	
             if (test.gt.tol_3.AND.k3.LT.nc_3) then
                write(com_3(k3),370)i,i1a(i),i2a(i),kk,sl_1,sl_2
                k3=k3+1
             endif
             
          end do
          if (k3.EQ.nc_3) write(com_3(k3),*)'Report limit reached'
  
             
       end do
 
       
       
C-----------------------------------------------------------------------------
C  Assemble output of the 3 tests into outcom array.
C-----------------------------------------------------------------------------

       iout = 1
       write(outcom(iout),400)
       iout = iout+1
       write(outcom(iout),420)
       iout = iout+1
       
       do i=1,k1-1
          write(outcom(iout),410)com_1(i)
          iout = iout + 1
       end do
       write(outcom(iout),420)
       iout = iout + 1

       write(outcom(iout),450)
       iout = iout+1
       write(outcom(iout),420)
       iout = iout+1
       do i=1,k2-1
          write(outcom(iout),410)com_2(i)
          iout = iout + 1
       end do
       write(outcom(iout),420)
       iout = iout + 1
     
       write(outcom(iout),470)
       iout = iout+1
       write(outcom(iout),420)
       iout = iout+1
       do i=1,k3-1
          write(outcom(iout),410)com_3(i)
          iout = iout + 1
       end do
       write(outcom(iout),420)
       iout = iout+1

       numcom = iout - 1
       
C-----------------------------------------------------------------------------

 300   format('Transition ',i4,' (Level ',i3,' to ',i3,
     &         ')   Accuracy : ',f6.2,'%  Required : ',f6.2,'%',1x,a1) 
 310   format('SERIOUS problem with Transition ',i4,' (Level ',i3,
     &        ' to ',i3,')') 


 350   format('Transition ',i4,' (Level ',i3,' to ',i3,1p,
     &         ')    Temp(',i2,') = ',d9.2,'  Average = ',d9.2) 

 370   format('Transition ',i4,' (Level ',i3,' to ',i3,') at Temp(',
     &        i2,')  Slopes : ',1p,d9.2,2x,d9.2) 


 400   format('Minmax polynominal test :')
 410   format(a)
 420   format(1x,' ')
 450   format('Running average test :')
 470   format('Unexpected slope change test :')

C-----------------------------------------------------------------------------

       end
