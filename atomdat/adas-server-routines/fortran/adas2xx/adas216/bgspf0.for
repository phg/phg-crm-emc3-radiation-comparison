C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgspf0.for,v 1.1 2004/07/06 11:41:03 whitefor Exp $ Date $Date: 2004/07/06 11:41:03 $
C
      SUBROUTINE BGSPF0( REP    , DSFULL, DSNEXP)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGSPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL OR INFO FILE (STANDARD
C	    (INPUT) IF BATCH - INPUT DATA SET SPECIFICATIONS.
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME  
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C  OUTPUT: (C*80)  DSNEXP  = EXPANSION FILE NAME  
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR  : Martin O'Mullane
C
C DATE    : 17/03/1999
C
C VERSION : 1.1                          DATE: 17/03/1999
C MODIFIED: Martin O'Mullane
C           FIRST VERSION.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEIN
      PARAMETER ( PIPEIN=5          )

      CHARACTER   REP*3         , DSFULL*80      , DSNEXP*80
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C READ IN DATA FROM STANDARD INPUT (IDL OR INFO FILE)
C-----------------------------------------------------------------------


      READ(PIPEIN,'(A)')REP
      READ(PIPEIN,'(A)')DSFULL
      READ(PIPEIN,'(A)')DSNEXP

      RETURN
      END
