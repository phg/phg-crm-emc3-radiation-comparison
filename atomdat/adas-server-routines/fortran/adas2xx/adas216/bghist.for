C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bghist.for,v 1.1 2004/07/06 11:40:09 whitefor Exp $ Date $Date: 2004/07/06 11:40:09 $
C
      SUBROUTINE BGHIST( IUNIT , ILEV  , ITE   , IDEN  , 
     &                   IHIST , ih_out 
     &                 )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGHIST *********************
C
C  PURPOSE:  WRITES UNFORMATTED HISTOGRAM DATA TO IUNIT.
C
C  CALLING PROGRAM: ADAS216
C
C  INPUT : (I*4)   IUNIT    = UNIT NO. OF OPENED UNFORMATTED FILE
C  INPUT : (R*8)   POP      = POPULATION ARRAY
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
       INTEGER   NDLEV     , NDTEM    , NDDEN   , NDBIN
       REAL*8    UMIN      , UMAX     , UBIN
C-----------------------------------------------------------------------
       PARAMETER( NDLEV  = 100   , NDTEM = 20     , NDDEN = 24      )       
       PARAMETER( umin   = 0.6D0 , umax  = 1.4D0  , ubin  = 0.005D0 )
       PARAMETER( ndbin  = 1 + ( (umax-umin)/ubin ) ) 
C-----------------------------------------------------------------------
       INTEGER   ILEV      , ITE      , IDEN
       INTEGER   IUNIT     , KH       , KD      , KT     , KL 
C-----------------------------------------------------------------------
       INTEGER   IHIST(NDLEV,NDTEM,NDDEN,NDBIN) 
       integer*2 IH_OUT(ILEV,ITE,IDEN,NDBIN) 
C-----------------------------------------------------------------------

       do kh=1,ndbin
         do kd=1,iden
           do kt=1,ite
             do kl=1,ilev
            
               ih_out(kl,kt,kd,kh) = ihist(kl,kt,kd,kh)
             
             end do
           end do
         end do
       end do

       write(iunit)ih_out      

       end
