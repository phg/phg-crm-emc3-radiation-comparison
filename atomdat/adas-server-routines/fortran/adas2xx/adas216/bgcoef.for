C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgcoef.for,v 1.1 2004/07/06 11:40:00 whitefor Exp $ Date $Date: 2004/07/06 11:40:00 $
C
       SUBROUTINE BGCOEF(LPSEL  , LZSEL  , LIOSEL ,
     &                   LHSEL  , LRSEL  , LISEL  , LNSEL ,
     &                   NMET   , IMETR  , IFOUT  , IDOUT ,
     &                   MAXT   , TINE   , TINP   , TINH  ,
     &                   MAXD   , DINE   , DINP   ,
     &                   AA     , XIA    , XJA    , ER    , ZEFF  ,
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH , ICNTI ,
     &                   IETRN  , IPTRN  , IRTRN  , IHTRN , IITRN ,
     &                   IE1A   , IE2A   , IP1A   , IP2A  , 
     &                   I1A    , I2A    ,
     &                   IL     , NV     , SCEF   , TSCEF , SCOM  ,
     &                   IUNT27 , OPEN27 , IZ0    , IZ1   , 
     &                   DSNEXP , DSNINC ,
     &                   MAXLEV , npl    , nplr   , npli,NPLA   , IPLA , 
     &                   BWNO   , BWNOA  , PRTWTA ,
     &                   POPAR  
     &                  )
      

       IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGCOEF *********************
C
C  PURPOSE: CALCULATES COLLISIONAL-RADIATIVE POPULATION
C
C  CALLING PROGRAM: ADAS216
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C  THIS IS A SUBROUTINE VERSION OF ADAS208 WITHOUT THE SEARCH FOR IONISATION
C  RATES FROM ADF07 FILES.
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       INTEGER   NDLEV  , NDTRN  , NDTEM  , NDDEN  , NDMET
       INTEGER   IUNT27 
       INTEGER   L1     , L2     , I4UNIT
C-----------------------------------------------------------------------
       REAL*8    D1     , WN2RYD
C-----------------------------------------------------------------------
       PARAMETER( NDLEV  = 100 , NDTRN  = 2100, NDTEM = 20 , 
     &            NDDEN  = 24  , NDMET  = 4 )
       PARAMETER( L1     = 1   , L2     = 2 )
C-----------------------------------------------------------------------
       PARAMETER( D1 = 1.0D0 , WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
       INTEGER   IZ        , IZ0       , IZ1     ,
     &           NPL       , NPLR      , NPLI    , NPL3    ,
     &           IL        , NV        ,  
     &           MAXLEV    , NMET      , NORD    , IFOUT   , IDOUT   ,
     &           MAXT      , MAXD      ,  
     &           ICNTE     , ICNTP     , ICNTR   , ICNTH   , ICNTI   ,
     &           IN        , IS        , IT      , IMET    ,
     &           JMET      , IP        , I       ,  
     &           ITIN      , ININ
C-----------------------------------------------------------------------
       REAL*8    R8NECIP   , ALFRED    , X
       REAL*8    BWNO      , ZEFF      , ZEFFSQ  , DMINT
C-----------------------------------------------------------------------
       CHARACTER DSNEXP*80 , DSNINC*80  
C-----------------------------------------------------------------------
       LOGICAL   OPEN27
       LOGICAL   LPSEL     , LZSEL     , LIOSEL  ,
     &           LHSEL     , LRSEL     , LISEL   , LNSEL    , LSOLVE ,
     &           LPDATA 
C-----------------------------------------------------------------------
       INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     , NPLA(NDLEV)  ,
     &           I1A(NDTRN)       , I2A(NDTRN)       ,
     &           IPLA(NDMET,NDLEV)
       INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &           IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &           IITRN(NDTRN)     ,
     &           IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &           IP1A(NDTRN)      , IP2A(NDTRN)      ,
     &           ISTRN(NDMET)
C-----------------------------------------------------------------------
       REAL*8    SCEF(14)        , BWNOA(NDMET)     , PRTWTA(NDMET) ,
     &           TINE(NDTEM)     , TINP(NDTEM)      , TINH(NDTEM)   ,
     &           TEVA(NDTEM)     , TPVA(NDTEM)      , THVA(NDTEM)   ,
     &           TEA(NDTEM)      , TPA(NDTEM)       , THA(NDTEM)    ,
     &           DINE(NDDEN)     , DINP(NDDEN)      ,
     &           DENSA(NDDEN)    , DENSPA(NDDEN)    ,
     &           RATHA(NDDEN)    ,RATPIA(NDDEN,NDMET),
     &           RATMIA(NDDEN,NDMET),
     &           AA(NDTRN)       , 
     &           XJA(NDLEV)      , 
     &           XIA(NDLEV)      , ER(NDLEV)        ,
     &           CIE(NDLEV)      , RHS(NDLEV)       ,
     &           CIEPR(NDLEV,NDMET) , V3PR(NDLEV,NDMET)  ,
     &           VRRED(NDMET,NDMET) , VHRED(NDMET,NDMET) ,
     &           VIRED(NDMET,NDMET) , VIONR(NDMET,NDMET) ,
     &           VCRPR(NDMET,NDMET) , ZPLA(NDMET,NDLEV)
       REAL*8    TSCEF(14,3)                   , SCOM(14,NDTRN)        ,
     &           EXCRE(NDTEM,NDTRN)            , EXCRP(NDTEM,NDTRN)    ,
     &           DEXCRE(NDTEM,NDTRN)           , DEXCRP(NDTEM,NDTRN)   ,
     &           VECH(NDTEM,NDLEV,NDMET)       ,VECR(NDTEM,NDLEV,NDMET),
     &           VECI(NDTEM,NDLEV,NDMET)       , CRA(NDLEV,NDLEV)      ,
     &           CRCE(NDLEV,NDLEV)             , CRCP(NDLEV,NDLEV)     ,
     &           CC(NDLEV,NDLEV)               , CMAT(NDLEV,NDLEV)     ,
     &           CRED(NDMET,NDMET)             , CRMAT(NDMET,NDMET)

       REAL*8    DVECR(NDTEM,NDLEV,NDMET)
       REAL*8    DCIEPR(NDTEM,NDLEV,NDMET)   , DV3PR(NDTEM,NDLEV,NDMET)
       REAL*8    DCIE(NDTEM,NDLEV)
 
       REAL*8    PCC(NDLEV,NDLEV)              , PCIE(NDLEV)
       REAL*8    PCIEPR(NDLEV,NDMET)           , PV3PR(NDLEV,NDMET)
       REAL*8    PVCRPR(NDMET,NDMET)           , PVECR(NDLEV,NDMET)
       REAL*8    PR(NDMET,NDTEM,NDDEN)

       REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
       REAL*8    STCKM(NDMET,NDTEM,NDDEN)      ,
     &           STVRM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           STVHM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           STVIM(NDMET,NDTEM,NDDEN,NDMET)
       REAL*8    FVCRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVRRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVHRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVIRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVIONR(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVCRPR(NDMET,NDMET,NDTEM,NDDEN)
       REAL*4    STVR(NDLEV,NDTEM,NDDEN,NDMET) ,
     &           STVH(NDLEV,NDTEM,NDDEN,NDMET) ,
     &           STVI(NDLEV,NDTEM,NDDEN,NDMET)
       REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
       REAL*8    PLA1(NDLEV)           ,
     &           PLAS1(NDMET)          , SWVLN(NDMET)     
C-----------------------------------------------------------------------
       LOGICAL   LMETR(NDMET)          , LTRNG(NDTEM,3)   
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ******************************* START ********************************
C-----------------------------------------------------------------------




C-----------------------------------------------------------------------
C CHECK IF ELECTRON IMPACT TRANSITIONS EXIST TO THE SELECTED METASTABLE
C-----------------------------------------------------------------------

            CALL BXCHKM( NMET   , IMETR , ICNTE , IE1A , LMETR )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV:  (TIN? -> T?VA)  (? = E,P,H )
C-----------------------------------------------------------------------

            CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINE  , TEVA   )
            CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINP  , TPVA   )
            CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINH  , THVA   )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TIN? -> T?A)  (? = E,P,H )
C-----------------------------------------------------------------------

            CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINE  , TEA    )
            CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINP  , TPA    )
            CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINH  , THA    )

C-----------------------------------------------------------------------
C CONVERT INPUT DENSITIES INTO CM-3:
C-----------------------------------------------------------------------

            CALL XXDCON( IDOUT, L1, IZ1, MAXD , DINE  , DENSA  )
            CALL XXDCON( IDOUT, L1, IZ1, MAXD , DINP  , DENSPA )


C-----------------------------------------------------------------------
C  INITIALISE VECR, VECH, VECI TO ZERO
C-----------------------------------------------------------------------

               DO 22 IS=1,NDLEV
                  DO 21 IT=1,NDTEM
                    DCIE(IT,IS)=0.0D0
                    DO 20 IP=1,NDMET
                        VECR(IT,IS,IP)   = 0.0D0
                        DVECR(IT,IS,IP)  = 0.0D0
                        VECH(IT,IS,IP)   = 0.0D0
                        VECI(IT,IS,IP)   = 0.0D0
                        DCIEPR(IT,IS,IP) = 0.0D0
                        DV3PR(IT,IS,IP)  = 0.0D0
   20                CONTINUE
   21             CONTINUE
   22          CONTINUE
C-----------------------------------------------------------------------
C CALCULATE ELECTRON IMPACT TRANSITION EXCITATION & DE-EXCITION RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.
C-----------------------------------------------------------------------

            CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                   ICNTE , MAXT   ,
     &                   XJA   , ER     , TEA   ,
     &                   IE1A  , IE2A   ,
     &                   EXCRE , DEXCRE
     &                 )

C-----------------------------------------------------------------------
C CALCULATE LINE POWER LOSES FOR ALL LINES AND SPECIFIC LINE.
C-----------------------------------------------------------------------

            CALL B8LOSS( NDTRN , NDLEV  , NDMET ,
     &                   ICNTE , NMET   , IMETR , ISTRN  ,
     &                   XJA   , ER     , AA    ,
     &                   IE1A  , IE2A   ,
     &                   PLAS1 , SWVLN  , PLA1
     &                  )

C-----------------------------------------------------------------------
C CALCULATE PROTON IMPACT TRANSITION EXCITATION & DE-EXCITION RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.             (ONLY IF REQUIRED).
C-----------------------------------------------------------------------

               IF (LPSEL) THEN
                  CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                         ICNTP , MAXT   ,
     &                         XJA   , ER     , TPA   ,
     &                         IP1A  , IP2A   ,
     &                         EXCRP , DEXCRP
     &                       )
               ENDIF

C-----------------------------------------------------------------------
C SETUP INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE LEVEL LIST 'IORDR'
C-----------------------------------------------------------------------

            CALL BXIORD( IL   ,
     &                   NMET , IMETR ,
     &                   NORD , IORDR
     &                 )

C-----------------------------------------------------------------------
C ELECTRON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------

            CALL BXRATE( NDTEM      , NDTRN  , D1     ,
     &                   NV         , SCEF   , SCOM   ,
     &                   MAXT       , TEA    ,
     &                   ICNTE      , IETRN  ,
     &                   EXCRE      , DEXCRE ,
     &                   LTRNG(1,1)
     &                 )

C-----------------------------------------------------------------------
C PROTON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------

               IF (LPSEL) THEN
                  ZEFFSQ = ZEFF*ZEFF
                  CALL BXRATE( NDTEM      , NDTRN  , ZEFFSQ ,
     &                         NV         , SCEF   , SCOM   ,
     &                         MAXT       , TPA    ,
     &                         ICNTP      , IPTRN  ,
     &                         EXCRP      , DEXCRP ,
     &                         LTRNG(1,2)
     &                       )
               ENDIF

C----------------------------------------------------------------------
C FREE ELECTRON RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LRSEL) THEN
                  CALL B8RCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , TEA        ,
     &                         ICNTR , IRTRN      , I2A    , I1A    ,
     &                         DVECR  , LTRNG(1,1)
     &                       )
               ENDIF

C----------------------------------------------------------------------
C ELECTRON IMPACT IONISATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LISEL) THEN
                  CALL B8RCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , TEA        ,
     &                         ICNTI , IITRN      , I2A    , I1A    ,
     &                         VECI  , LTRNG(1,1)
     &                       )
               ENDIF

C----------------------------------------------------------------------
C CHARGE EXCHANGE RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LHSEL) THEN
                  CALL B8RCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , THA        ,
     &                         ICNTH , IHTRN      , I2A    , I1A    ,
     &                         VECH  , LTRNG(1,3)
     &                       )
               ENDIF

C----------------------------------------------------------------------
C SET UP A-VALUE TRANSITION MATRIX 'CRA' (UNITS: SEC-1)
C----------------------------------------------------------------------

            CALL BXMCRA( NDTRN , NDLEV  ,
     &                   ICNTE , IL     ,
     &                   IE1A  , IE2A   ,
     &                   AA    ,
     &                   CRA
     &                 )

C----------------------------------------------------------------------
C INITIALISE PROJECTED INDIRECT COUPLINGS
C----------------------------------------------------------------------

            LPDATA = .FALSE.
            IF( LNSEL ) THEN
                ITIN = 1
                ININ = 1

                 CALL B8GETP( IZ0    , IZ1    , DSNEXP , DSNINC ,
     &                        NDLEV  , NDMET  , NDTEM  , NDDEN  ,
     &                        MAXD   , MAXT   , DENSA  , TEA    ,
     &                        LPDATA , LIOSEL , LRSEL  , LHSEL  ,
     &                        IL     , ITIN   , ININ   ,
     &                        PCC    , PCIE   , PCIEPR , PV3PR  ,
     &                        PVCRPR , PVECR  , IUNT27 , OPEN27 ,
     &                        PR
     &                      )

            ENDIF


C-----------------------------------------------------------------------
C  SET UP VECTOR OF IONISATION AND THREE-BODY RECOMBINATION COEFFICIENTS
C  IF REQUIRED - NOTE PARENT RESOLVED DATA
C-----------------------------------------------------------------------

       IF (LIOSEL) THEN

       DO 122 IT = 1,MAXT

            DO 5 IS=1,MAXLEV
             DO 15 IP=1,NPLA(IS)
              IF( ZPLA(IP,IS) .GT. 0.0D0) THEN
                X=XIA(IS)+WN2RYD*(BWNOA(IPLA(IP,IS))-BWNO)
                DCIEPR(IT,IS,IPLA(IP,IS))=
     &                       R8NECIP(IZ,X,ZPLA(IP,IS)
     &                              ,TEA(IT),ALFRED)
                DV3PR(IT,IS,IPLA(IP,IS))=
     &                     (2.0*XJA(IS)+1.0)*ALFRED/
     &                             PRTWTA(IPLA(IP,IS))
              ELSE
                DCIEPR(IT,IS,IPLA(IP,IS)) = 0.0D+0
                DV3PR(IT,IS,IPLA(IP,IS))  = 0.0D+0
              ENDIF
              DCIE(IT,IS) = DCIE(IT,IS)
     &                      + DCIEPR(IT,IS,IPLA(IP,IS))
   15        CONTINUE
    5       CONTINUE

  122  CONTINUE
C
       ENDIF





C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C INCREMENT OVER OVER ALL INPUT TEMPERATURE VALUES
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

               DO 4 IT=1,MAXT

C-----------------------------------------------------------------------
C SET UP EXCITATION/DE-EXCIATATION RATE COEFFT. MATRICES FOR GIVEN TEMP.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ELECTRON IMPACT TRANSITIONS 'CRCE' - (UNITS: CM**3/SEC-1)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                  CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                         IT    , ICNTE  , IL     ,
     &                         IE1A  , IE2A   ,
     &                         EXCRE , DEXCRE ,
     &                         CRCE
     &                       )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C PROTON IMPACT TRANSITIONS 'CRCP' - (UNITS: CM**3/SEC-1) - IF SELECTED
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                     IF (LPSEL) THEN
                        CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                               IT    , ICNTP  , IL     ,
     &                               IP1A  , IP2A   ,
     &                               EXCRP , DEXCRP ,
     &                               CRCP
     &                              )
                     ENDIF

C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C INCREMENT OVER OVER ALL INPUT DENSITY VALUES
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *

                     DO 6 IN=1,MAXD

C-----------------------------------------------------------------------
C  INITIALISE VECTORS TO ZERO
C-----------------------------------------------------------------------

               DO 2 IS=1,NDLEV
                  CIE(IS)  = 0.0D0
                  PCIE(IS) = 0.0D+0
                  DO 19 I = 1,NDLEV
                    PCC(IS,I) = 0.0D0
   19             CONTINUE
                  DO 120 IP=1,NDMET
                     CIEPR(IS,IP)  = 0.0D0
                     PCIEPR(IS,IP) = 0.0D0
                     V3PR(IS,IP)   = 0.0D0
                     PV3PR(IS,IP)  = 0.0D0
                     PVECR(IS,IP)  = 0.0D0
  120             CONTINUE
    2          CONTINUE


C-----------------------------------------------------------------------
C SET UP INDIRECT MATRICES
C-----------------------------------------------------------------------

                     IF( LPDATA ) THEN
                        ITIN = IT
                        ININ = IN

                 CALL B8GETP( IZ0    , IZ1    , DSNEXP , DSNINC ,
     &                        NDLEV  , NDMET  , NDTEM  , NDDEN  ,
     &                        MAXD   , MAXT   , DENSA  , TEA    ,
     &                        LPDATA , LIOSEL , LRSEL  , LHSEL  ,
     &                        IL     , ITIN   , ININ   ,
     &                        PCC    , PCIE   , PCIEPR , PV3PR  ,
     &                        PVCRPR , PVECR  , IUNT27 , OPEN27 ,
     &                        PR
     &                      )

                     ENDIF

C-----------------------------------------------------------------------
C  ADD INDIRECT COUPLINGS ONTO IONISATION AND RECOMBINATION VECTORS
C
C  IF INDIRECT COUPLING HAVE BEEN INITIATED, SET LIOSEL = TRUE
C                                                LRSEL  = TRUE
C
C  NOTE CIE HAS UNITS OF CM3S-1 WHILE PCIE AND PCIEPR HAVE UNITS OF S-1
C       V3PR HAS UNITS OF CM6S-1 WHILE PV3PR HAS UNITS OF CM3S-1
C       VECR AND PVECR HAVE UNITS OF CM3S-1
C
C  NOTE THAT CIE DOES NOT NEED TO BE ADJUSTED
C-----------------------------------------------------------------------
C
                     IF( LPDATA ) THEN

                        LIOSEL = .TRUE.
                        LRSEL = .TRUE.
                        DO 17 IS=1,MAXLEV

                          CIE(IS) = DCIE(IT,IS)
                          DO 18 IP = 1,NPL
                            CIEPR(IS,IP) = DCIEPR(IT,IS,IP)
     &                                      + PCIEPR(IS,IP) / DENSA(IN)
                            V3PR(IS,IP) = DV3PR(IT,IS,IP)
     &                                      + PV3PR(IS,IP) / DENSA(IN)
                            VECR(IT,IS,IP) = DVECR(IT,IS,IP)
     &                                      + PVECR(IS,IP)
C
   18                     CONTINUE
   17                   CONTINUE

                     ELSE

                         DO 23 IS=1,MAXLEV

                          CIE(IS) = DCIE(IT,IS)
                          DO 24 IP = 1,NPL
                            CIEPR(IS,IP) = DCIEPR(IT,IS,IP)
                            V3PR(IS,IP)  =  DV3PR(IT,IS,IP)
                            VECR(IT,IS,IP) = DVECR(IT,IS,IP)
   24                     CONTINUE
   23                    CONTINUE

                     ENDIF


C-----------------------------------------------------------------------
C SET UP WHOLE RATE MATRIX  - (UNITS: SEC-1)
C
C   TO ADD ON INDIRECT PARTS ALSO PASS OVER PCC
C-----------------------------------------------------------------------

                        CALL B8MCCA( NDLEV     , IL         ,
     &                               LPSEL     , LIOSEL     , LPDATA,
     &                               DENSA(IN) , DENSPA(IN) ,
     &                               CRA       , PCC        ,
     &                               CRCE      , CRCP       , CIE    ,
     &                               CC
     &                             )

C-----------------------------------------------------------------------
C SET UP NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX - (UNITS: SEC-1)
C-----------------------------------------------------------------------

                        CALL BXMCMA( NDLEV ,
     &                               NORD  , IORDR ,
     &                               CC    ,
     &                               CMAT
     &                             )

C-----------------------------------------------------------------------
C INVERT NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX
C-----------------------------------------------------------------------

                        LSOLVE=.FALSE.
                        CALL XXMINV( LSOLVE , NDLEV , NORD   ,
     &                               CMAT   , RHS   , DMINT
     &                             )

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE POPULATION COMPONENTS ASSOCIATED WITH EACH
C METASTABLE LEVEL.
C-----------------------------------------------------------------------

                        CALL BXSTKA( NDLEV            , NDMET  ,
     &                               NORD             , NMET   ,
     &                               IORDR            , IMETR  ,
     &                               CMAT             , CC     ,
     &                               STACK(1,1,IT,IN)
     &                             )

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE STATE SEL. RECOMBS. FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN
                       NPL3 = MAX0(NPL,NPLR)

                       DO 36 IP=1,NPL3

                           CALL B8STKE( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  DENSA(IN)     ,
     &                                  CMAT          , VECR   , V3PR  ,
     &                                  IP            ,
     &                                  STVR(1,IT,IN,IP)
     &                                )
   36                  CONTINUE

                   ELSEIF (LRSEL.AND.(.NOT.LIOSEL)) THEN

                       DO 37 IP=1,NPLR

                           CALL B8STKB( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  CMAT          , VECR   ,
     &                                  IP            ,
     &                                  STVR(1,IT,IN,IP)
     &                                )
   37                 CONTINUE
                   ENDIF

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE STATE SEL. C. EXCH. FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 30 IP=1,NPLR

                        IF (LHSEL) THEN
                           CALL B8STKB( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  CMAT          , VECH   ,
     &                                  IP            ,
     &                                  STVH(1,IT,IN,IP)
     &                                )
                        ENDIF
   30              CONTINUE

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE STATE SEL. IONIS.   FROM (Z-1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 31 IP=1,NPLI

                        IF (LISEL) THEN
                           CALL B8STKB( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  CMAT          , VECI   ,
     &                                  IP            ,
     &                                  STVI(1,IT,IN,IP)
     &                                )
                        ENDIF
   31              CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE LEVEL TRANSITION RATE CONTRIBUTIONS
C-----------------------------------------------------------------------

                     CALL BXSTKC( NDLEV            , NDMET            ,
     &                            NORD             , NMET             ,
     &                            IORDR            , IMETR            ,
     &                            CC               , STACK(1,1,IT,IN) ,
     &                            CRED
     &                          )
                     DO 44 IMET=1,NMET
                       DO 44 JMET=1,NMET
                         FVCRED(IMET,JMET,IT,IN)=CRED(IMET,JMET)
   44                CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. RECOMB.  FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN
                       NPL3 = MAX0(NPL,NPLR)

                       DO 38 IP=1,NPL3

                           CALL B8STKF( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVR(1,IT,IN,IP),
     &                                  DENSA(IN),
     &                                  VECR  , V3PR          , IP     ,
     &                                  VRRED
     &                                )
                           DO 45 IMET=1,NMET
                             FVRRED(IMET,IP,IT,IN)=VRRED(IMET,IP)
   45                      CONTINUE

   38                   CONTINUE

                   ELSEIF(LRSEL.AND.(.NOT.LIOSEL))THEN

                       DO 39 IP=1,NPLR

                           CALL B8STKD( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVR(1,IT,IN,IP),
     &                                  VECR  , IP    ,
     &                                  VRRED
     &                                )
                           DO 46 IMET=1,NMET
                             FVRRED(IMET,IP,IT,IN)=VRRED(IMET,IP)
   46                      CONTINUE

   39                  CONTINUE
                   ENDIF

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. C. EXCH. FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 32 IP=1,NPLR
                        IF (LHSEL) THEN
                           CALL B8STKD( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVH(1,IT,IN,IP) ,
     &                                  VECH  , IP   ,
     &                                  VHRED
     &                                )
                           DO 47 IMET=1,NMET
                             FVHRED(IMET,IP,IT,IN)=VHRED(IMET,IP)
   47                      CONTINUE

                        ENDIF
   32              CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. IONIS.   FROM (Z-1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 33 IP = 1,NPLI

                        IF (LISEL) THEN
                           CALL B8STKD( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVI(1,IT,IN,IP) ,
     &                                  VECI  , IP   ,
     &                                  VIRED
     &                                )
                           DO 48 IMET=1,NMET
                             FVIRED(IMET,IP,IT,IN)=VIRED(IMET,IP)
   48                      CONTINUE

                        ENDIF
   33              CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. IONIS. FROM (Z) - IF SELECTED BY LIOSEL
C
C STACK UP PARENT CROSS COUPLING COEFFICIENTS
C
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN

                   DO 43 IMET = 1,NMET

                     DO 42 IP=1,NPL3

                       VIONR(IMET,IP)=CIEPR(IMETR(IMET),IP)
                       DO 41 I=1,NORD
                        VIONR(IMET,IP)=VIONR(IMET,IP)+CIEPR(IORDR(I),IP)
     &                                 *STACK(I,IMET,IT,IN)
   41                  CONTINUE
                       FVIONR(IMET,IP,IT,IN)=VIONR(IMET,IP)

   42                CONTINUE
   43              CONTINUE

                   DO 143 IP = 1,NPL3

                     DO 142 IMET=1,NPL3

                       VCRPR(IP,IMET)=0.0D+0
                       IF( IP .NE. IMET ) THEN
                       DO 141 I=1,NORD
                        VCRPR(IP,IMET)=VCRPR(IP,IMET)+CIEPR(IORDR(I),IP)
     &                                 *STVR(I,IT,IN,IMET)
  141                  CONTINUE
                       ENDIF
                       FVCRPR(IP,IMET,IT,IN)=VCRPR(IP,IMET)
                       IF ( LPDATA ) THEN
                          FVCRPR(IP,IMET,IT,IN) = FVCRPR(IP,IMET,IT,IN)
     &                                          + PVCRPR(IP,IMET)
                       ENDIF

  142                CONTINUE
  143              CONTINUE

                   ENDIF

C-----------------------------------------------------------------------
C CALCULATE METASTABLE POPULATIONS
C-----------------------------------------------------------------------

                     CALL BXMPOP( NDMET             ,
     &                            NMET              ,
     &                            CRED              ,
     &                            RHS               , CRMAT        ,
     &                            STCKM(1,IT,IN)
     &                          )

C-----------------------------------------------------------------------
C  CALCULATE RECOMBINATIONS AND CHARGE EXCHANGE CONTRIBUTIONS.
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN

                       DO 34 IP=1,NPL3
 
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VRRED            ,
     &                               STVRM(1,IT,IN,IP)
     &                             )
   34                  CONTINUE

      
                   ELSEIF ((.NOT.LIOSEL).AND.LRSEL) THEN

                       DO 51 IP=1,NPLR
 
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VRRED            ,
     &                               STVRM(1,IT,IN,IP)
     &                             )
   51                  CONTINUE

                   ENDIF
C-----------------------------------------------------------------------
                   DO 52 IP=1,NPLR

                     IF (LHSEL) THEN
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VHRED            ,
     &                               STVHM(1,IT,IN,IP)
     &                             )
                     ENDIF
   52              CONTINUE
C-----------------------------------------------------------------------
                   DO 35 IP=1,NPLI

                     IF (LISEL) THEN
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VIRED            ,
     &                               STVIM(1,IT,IN,IP)
     &                             )
                     ENDIF
   35              CONTINUE
C-----------------------------------------------------------------------

C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    6             CONTINUE
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    4          CONTINUE
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

C-----------------------------------------------------------------------
C CALCULATE THE POPULATIONS OF EACH LEVEL.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE METASTABLE LEVELS FIRST.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            CALL B8POPM( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   NPL    , NPLR  , NPLI  ,
     &                   MAXT   , MAXD  , NMET  ,
     &                            DENSA , IMETR ,
     &                            LRSEL , LISEL , LHSEL ,
     &                            RATPIA, RATMIA, RATHA ,
     &                   STCKM  , STVRM , STVIM , STVHM ,
     &                   POPAR
     &                 )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE POPULATIONS OF EACH ORDINARY LEVEL.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            CALL B8POPO( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   NPL    , NPLR  , NPLI  ,
     &                   MAXT   , MAXD  , NMET  , NORD  ,
     &                            DENSA , IMETR , IORDR ,
     &                            LRSEL , LISEL , LHSEL ,
     &                            RATPIA, RATMIA, RATHA ,
     &                   STACK  , STVR  , STVI  , STVH  ,
     &                   POPAR
     &                 )


C-----------------------------------------------------------------------
 9999 RETURN
C-----------------------------------------------------------------------

      END
