C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgwexp.for,v 1.1 2004/07/06 11:41:29 whitefor Exp $ Date $Date: 2004/07/06 11:41:29 $
C
      SUBROUTINE BGWEXP( IUNIT  ,
     &                   NDLEV  , NDTEM  , NDDEN   , NDTR    ,
     &                   ILEV   , ITE    , IDEN    ,
     &                   ind_t  , err_t  , index_t , adiff_t , rt     ,
     &                   ind_d  , err_d  , index_d , adiff_d , rd     ,
     &                   int_out, it_out ,errt_out , adt_out , rt_out ,
     &                   ind_out, id_out ,errd_out , add_out , rd_out   
     &                 )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGWEXP *********************
C
C  PURPOSE:  WRITES (UNFORMATTED) EXPLORE ERROR DATA TO IUNIT.
C
C  CALLING PROGRAM: ADAS216
C
C  INPUT : (I*4)   IUNIT    = UNIT NO. OF OPENED UNFORMATTED FILE
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
       INTEGER   NDLEV     , NDTEM    , NDDEN   , NDTR
       INTEGER   ILEV      , ITE      , IDEN
       INTEGER   IUNIT     , KH       , KD      , KT     , KL 
C-----------------------------------------------------------------------
       INTEGER   ind_t(ndlev,ndden)         , index_t(ndlev,ndden,ndtr)
       INTEGER   ind_d(ndlev,ndtem)         , index_d(ndlev,ndtem,ndtr)
C-----------------------------------------------------------------------
       REAL*8    adiff_t(ndlev,ndden,ndtr)  , err_t(ndlev,ndden,ndtr) ,
     &           rt(ndlev,ndtem,ndden,ndtr)  
       REAL*8    adiff_d(ndlev,ndtem,ndtr)  , err_d(ndlev,ndtem,ndtr) ,
     &           rd(ndlev,ndtem,ndden,ndtr)  
C-----------------------------------------------------------------------
       integer   int_out(ilev,iden)         , ind_out(ilev,ite)
       integer   it_out(ilev,iden,ndtr)     , id_out(ilev,ite,ndtr)
C-----------------------------------------------------------------------
       REAL*4    adt_out(ilev,iden,ndtr)   , errt_out(ilev,iden,ndtr) ,
     &           rt_out(ilev,ite,iden,ndtr)
       REAL*4    add_out(ilev,ite,ndtr)    , errd_out(ilev,ite,ndtr)  ,
     &           rd_out(ilev,ite,iden,ndtr)
C-----------------------------------------------------------------------
    
C write out variation againt temperature 
     
       do kh=1,ndtr
         do kd=1,iden
           do kl=1,ilev
              
              int_out(kl,kd)     = ind_t(kl,kd)
              it_out(kl,kd,kh)   = index_t(kl,kd,kh)
              errt_out(kl,kd,kh) = err_t(kl,kd,kh)
              adt_out(kl,kd,kh)  = adiff_t(kl,kd,kh)
              
              do kt=1,ite
      
               rt_out(kl,kt,kd,kh) = rt(kl,kt,kd,kh)
             
             end do
           
           end do
         end do
       end do

       write(iunit)int_out      
       write(iunit)it_out    
       write(iunit)errt_out    
       write(iunit)adt_out    
       write(iunit)rt_out    

C and density

       do kh=1,ndtr
         do kt=1,ite
           do kl=1,ilev
              
              ind_out(kl,kt)     = ind_d(kl,kt)
              id_out(kl,kt,kh)   = index_d(kl,kt,kh)
              errd_out(kl,kt,kh) = err_d(kl,kt,kh)
              add_out(kl,kt,kh)  = adiff_d(kl,kt,kh)
              
              do kd=1,iden

                rd_out(kl,kt,kd,kh) = rd(kl,kt,kd,kh)
             
             end do
           
           end do
         end do
       end do

       write(iunit)ind_out      
       write(iunit)id_out    
       write(iunit)errd_out    
       write(iunit)add_out    
       write(iunit)rd_out    

       end
