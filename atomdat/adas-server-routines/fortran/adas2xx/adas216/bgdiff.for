C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgdiff.for,v 1.1 2004/07/06 11:40:03 whitefor Exp $ Date $Date: 2004/07/06 11:40:03 $
C
      SUBROUTINE BGDIFF( ILEV  , maxt  , maxd    , 
     &                   popun , popar ,
     &                   error , index , 
     &                   ind_t , err_t , index_t , adiff_t , rt , 
     &                   ind_d , err_d , index_d , adiff_d , rd   
     &                 )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGDIFF *********************
C
C  PURPOSE:  Calculates the absolute difference from 1.0 for temperature
C            or density and updates the set of arrays holding the top
C            ndtr contributing transitions. 
C
C  CALLING PROGRAM: ADAS216
C
C  INPUT : (R*8)   POP      = POPULATION ARRAY
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          R8ADIF     ADAS      calculates absolute difference of array
C
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
       INTEGER    NDLEV , NDTEM , NDDEN , NDTR 
C-----------------------------------------------------------------------
       PARAMETER( NDLEV  = 100 , NDTR  = 20 , NDTEM  = 20  , 
     &            NDDEN  = 24  )
C-----------------------------------------------------------------------
       INTEGER    ilev  , maxt  , maxd  , index
       INTEGER    kl    , kt    , kd    , nel    , nt    , nd     ,
     &            is    , i     , j     , kin
C-----------------------------------------------------------------------
       REAL*8     error
       REAL*8     fmean , ad    , a     , es     , r8adif
C-----------------------------------------------------------------------
       LOGICAL    LMEAN 
C-----------------------------------------------------------------------
       INTEGER    ind_t(ndlev,ndden)         , index_t(ndlev,ndden,ndtr)
       INTEGER    ind_d(ndlev,ndtem)         , index_d(ndlev,ndtem,ndtr)
C-----------------------------------------------------------------------
       REAL*8     adiff_t(ndlev,ndden,ndtr)  , err_t(ndlev,ndden,ndtr) ,
     &            temp(ndtem)                , rts(ndtem)              ,
     &            rtemp(ndtem)                
       REAL*8     rt(ndlev,ndtem,ndden,ndtr)  
       REAL*8     adiff_d(ndlev,ndtem,ndtr)  , err_d(ndlev,ndtem,ndtr) ,
     &            demp(ndden)                , rds(ndden)              ,
     &            rdemp(ndden)                
       REAL*8     rd(ndlev,ndtem,ndden,ndtr)  
       REAL*8     POPUN(NDLEV,NDTEM,NDDEN)   , POPAR(NDLEV,NDTEM,NDDEN)
C-----------------------------------------------------------------------
       
       
C We know what the mean should be

       lmean = .FALSE.
       fmean = 1.0            
       
              
C Variation vs temperature first - loop over levels and densities
 
       do kl=1,ilev
         do kd = 1,maxd
			
C Determine the 'contributing' ratios to the absolute difference
C set unphysical ratios to 0.0 - pick this up in inspection widget.

           nel = 1
           nt  = 1
           do kt=1,maxt
             if (popun(kl,kt,kd).ne.0.0 ) then
                temp(nel) = popar(kl,kt,kd)/popun(kl,kt,kd)
                rtemp(kt) = popar(kl,kt,kd)/popun(kl,kt,kd)
                nel = nel+1
                nt  = nt+1
             else
                temp(nt)  = 0.0
                rtemp(kt) = 0.0
                nt = nt+1
             endif
           end do
           nel = nel-1
           
           if (nel.le.2) then        ! abandon temperatures
              goto 100
           else 
              ad = r8adif(nel,temp,fmean,lmean)
           endif
           

C If this is the first time set position 1 for absolute difference, ratio(te),
C error multipler and index.

           if (ind_t(kl,kd).eq.0) then
 
              adiff_t(kl,kd,1) = ad
              ind_t(kl,kd) = 1
              
              kin = 1
              err_t(kl,kd,kin)   = error
              index_t(kl,kd,kin) = index
              do kt=1,maxt
                rt(kl,kt,kd,kin) = rtemp(kt)
              end do
			
           else

C We are limiting the number of contributing transitions to the top ndtr 
C transitions. Therefore increment ind_t but limit it to a max of ndtr. 
 
              ind_t(kl,kd) = ind_t(kl,kd) + 1
              ind_t(kl,kd) = min(ind_t(kl,kd),ndtr)
              kin          = ind_t(kl,kd)
 
C There are 3 possibilities as we go down the transition list
C   i. if LT ndtr-1 just add the info
C  ii. if EQ ndtr-1 then set position(ndtr) to position(1) and fill
C                   position(ndtr-1) with the latest info. Set absolute
C                   difference of position(1) to zero.
C iii. we have filled ndtr positions. Now we want to include the current
C      info only if it is greater than all at least one of the others. In
C      this case insert it and discard the smallest. Use a simple insertion
C      sort - order N**2 from Numerical Receipies but N is ~50 - on adiff.
C      Once sorted we can test on position(1).

              if (kin.lt.ndtr-1) then
 
                 adiff_t(kl,kd,kin) = ad
                 err_t(kl,kd,kin)   = error
                 index_t(kl,kd,kin) = index
                 do kt=1,maxt
                   rt(kl,kt,kd,kin) = rtemp(kt)
                 end do

              elseif (kin.eq.ndtr-1) then
 
                 adiff_t(kl,kd,ndtr) = adiff_t(kl,kd,1)
                 err_t(kl,kd,ndtr)   = err_t(kl,kd,1)  
                 index_t(kl,kd,ndtr) = index_t(kl,kd,1)
                 do kt=1,maxt
                   rt(kl,kt,kd,ndtr) = rt(kl,kt,kd,1)
                 end do
                 
                 adiff_t(kl,kd,ndtr-1) = ad
                 err_t(kl,kd,ndtr-1)   = error  
                 index_t(kl,kd,ndtr-1) = index 
                 do kt=1,maxt
                   rt(kl,kt,kd,ndtr-1) = rtemp(kt)
                 end do
                 
                 adiff_t(kl,kd,1)      = 0.0
 
              else
                 
                 if ( ad.gt.adiff_t(kl,kd,1)) then
                   
                    kin = 1
                    adiff_t(kl,kd,kin) = ad
                    err_t(kl,kd,kin)   = error
                    index_t(kl,kd,kin) = index
                    do kt=1,maxt
                      rt(kl,kt,kd,kin) = rtemp(kt)
                    end do
                    
                 endif
 
                 do j=2,ndtr
                 
                   a  = adiff_t(kl,kd,j)
                   es = err_t(kl,kd,j)
                   is = index_t(kl,kd,j)
                   do kt=1,maxt
                     rts(kt) = rt(kl,kt,kd,j)
                   end do
                   
                   do i=j-1,1,-1
                     if (adiff_t(kl,kd,i).le.a) goto 10
                     adiff_t(kl,kd,i+1) = adiff_t(kl,kd,i)
                     err_t(kl,kd,i+1)   = err_t(kl,kd,i)
                     index_t(kl,kd,i+1) = index_t(kl,kd,i)
                     do kt=1,maxt
                       rt(kl,kt,kd,i+1) = rt(kl,kt,kd,i)
                     end do
                   end do
                   
                   i=0
                   
  10               adiff_t(kl,kd,i+1) = a
                   err_t(kl,kd,i+1)   = es
                   index_t(kl,kd,i+1) = is
                   do kt=1,maxt
                     rt(kl,kt,kd,i+1) = rts(kt)
                   end do
     
                end do
 
             endif
 
          endif
 
  100    continue

         end do
       end do



C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C And now the whole lot again for densities
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

       lmean = .FALSE.
       fmean = 1.0  
                 
       do kl=1,ilev
         do kt = 1,maxt
			
C Determine the 'contributing' ratios to the absolute difference
C set unphysical ratios to 0.0 - pick this up in inspection widget.

           nel = 1
           nd  = 1
           do kd=1,maxd
             if (popun(kl,kt,kd).ne.0.0 ) then
                demp(nel) = popar(kl,kt,kd)/popun(kl,kt,kd)
                rdemp(kd) = popar(kl,kt,kd)/popun(kl,kt,kd)
                nel = nel+1
                nd  = nd+1
             else
                demp(nd)  = 0.0
                rdemp(kd) = 0.0
                nd = nd+1
             endif
      
           end do
           nel = nel-1
           if (nel.le.2) then        ! abandon densities
              goto 200
           else 
              ad = r8adif(nel,demp,fmean,lmean)
           endif


C If this is the first time set position 1 for absolute difference, ratio(te),
C error multipler and index.

           if (ind_d(kl,kt).eq.0) then
 
              adiff_d(kl,kt,1) = ad
              ind_d(kl,kt) = 1
              
              kin = 1
              err_d(kl,kt,kin)   = error
              index_d(kl,kt,kin) = index
              do kd=1,maxd
                rd(kl,kt,kd,kin) = rdemp(kd)
              end do
			
           else

C We are limiting the number of contributing transitions to the top ndtr 
C transitions. Therefore increment ind_d but limit it to a max of ndtr. 
 
              ind_d(kl,kt) = ind_d(kl,kt) + 1
              ind_d(kl,kt) = min(ind_d(kl,kt),ndtr)
              kin          = ind_d(kl,kt)
 
C There are 3 possibilities as we go down the transition list
C   i. if LT ndtr-1 just add the info
C  ii. if EQ ndtr-1 then set position(ndtr) to position(1) and fill
C                   position(ndtr-1) with the latest info. Set absolute
C                   difference of position(1) to zero.
C iii. we have filled ndtr positions. Now we want to include the current
C      info only if it is greater than all at least one of the others. In
C      this case inserd it and discard the smallest. Use a simple inserdion
C      sord - order N**2 from Numerical Receipies but N is ~50 - on adiff.
C      Once sorded we can test on position(1).

              if (kin.lt.ndtr-1) then
 
                 adiff_d(kl,kt,kin) = ad
                 err_d(kl,kt,kin)   = error
                 index_d(kl,kt,kin) = index
                 do kd=1,maxd
                   rd(kl,kt,kd,kin) = rdemp(kd)
                 end do

              elseif (kin.eq.ndtr-1) then
 
                 adiff_d(kl,kt,ndtr) = adiff_d(kl,kt,1)
                 err_d(kl,kt,ndtr)   = err_d(kl,kt,1)  
                 index_d(kl,kt,ndtr) = index_d(kl,kt,1)
                 do kd=1,maxd
                   rd(kl,kt,kd,ndtr) = rd(kl,kt,kd,1)
                 end do
                 
                 adiff_d(kl,kt,ndtr-1) = ad
                 err_d(kl,kt,ndtr-1)   = error  
                 index_d(kl,kt,ndtr-1) = index 
                 do kd=1,maxd
                   rd(kl,kt,kd,ndtr-1) = rdemp(kd)
                 end do
                 
                 adiff_d(kl,kt,1)      = 0.0
 
              else
                 
                 if ( ad.gt.adiff_d(kl,kt,1)) then
                   
                    kin = 1
                    adiff_d(kl,kt,kin) = ad
                    err_d(kl,kt,kin)   = error
                    index_d(kl,kt,kin) = index
                    do kd=1,maxd
                      rd(kl,kt,kd,kin) = rdemp(kd)
                    end do
                    
                 endif
 
                 do j=2,ndtr
                 
                   a  = adiff_d(kl,kt,j)
                   es = err_d(kl,kt,j)
                   is = index_d(kl,kt,j)
                   do kd=1,maxd
                     rds(kd) = rd(kl,kt,kd,j)
                   end do
                   
                   do i=j-1,1,-1
                     if (adiff_d(kl,kt,i).le.a) goto 20
                     adiff_d(kl,kt,i+1) = adiff_d(kl,kt,i)
                     err_d(kl,kt,i+1)   = err_d(kl,kt,i)
                     index_d(kl,kt,i+1) = index_d(kl,kt,i)
                     do kd=1,maxd
                       rd(kl,kt,kd,i+1) = rd(kl,kt,kd,i)
                     end do
                   end do
                   
                   i=0
                   
  20               adiff_d(kl,kt,i+1) = a
                   err_d(kl,kt,i+1)   = es
                   index_d(kl,kt,i+1) = is
                   do kd=1,maxd
                     rd(kl,kt,kd,i+1) = rds(kd)
                   end do
     
                end do
 
             endif
 
          endif
 
  200    continue

         end do
       end do
            
       
       
       return
       
       end
