C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgperr.for,v 1.1 2004/07/06 11:40:40 whitefor Exp $ Date $Date: 2004/07/06 11:40:40 $
C
      SUBROUTINE BGPERR( NDGEN  , NDSPF   , 
     &                   NUM    , ERRARR  ,
     &                   NARR   , DEF     , GEN     , SPF  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGRERR *********************
C
C  PURPOSE: PARSES ERROR INFORMATION FROM ADF04 FILES. 
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NDGEN    = MAX. NUMBER OF GENERAL ERRORS ALLOWED
C  INPUT : (I*4)   NDSPF    = MAX. NUMBER OF SPECIFIC ERRORS ALLOWED
C
C  INPUT:  (I*4)   NUM      = NUMBER OF ERROR LINES
C  INPUT:  (C*70)  ERRARR() = COMPRESSED STRING OF ERRORS
C
C
C  OUTPUT: (R*8)   NARR(3)  = NO. OF ERROR TYPES FOR EXCITATION
C                               1ST INDEX:  NO. DEFAULT (0 OR 1)
C                               2ND INDEX:  NO. GENERAL 
C                               3RD INDEX:  NO. SPECIFIC 
C  OUTPUT: (R*8)   DEF      = DEFAULT ERROR
C  OUTPUT: (R*8)   GEN(,)   = GENERAL ERROR
C                               1ST DIMENSION: SOURCE/DESTINATION LEVEL
C                               2ND DIMENSION: ERROR
C
C  OUTPUT: (R*8)   SPF(,)   = SPECIFIC ERROR
C                               1ST DIMENSION: FIRST LEVEL
C                               2ND DIMENSION: SECOND LEVEL
C                               3RD DIMENSION: ERROR
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFCHR     ADAS      GETS OCCURANCES OF SUBSTRING IN A STRING
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C  AUTHOR  : MARTIN O'MULLANE,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 17/03/1999
C MODIFIED : MARTIN O'MULLANE  
C            FIRST VERSION.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDGEN      , NDSPF
      INTEGER   I          , J     
      INTEGER   NUM        , NDEF         , NGEN       , NSPF
      INTEGER   I1M        , I2M          , I1S        , I2S
      INTEGER   I1ST       , I2ST         , IDUM   
      INTEGER   ILEN
      INTEGER   I4UNIT
C-----------------------------------------------------------------------
      CHARACTER STR*70  
      CHARACTER STR1*4     , STR2*4       , STR3*20    , str4*40
C-----------------------------------------------------------------------
      REAL*8    DEF        , GEN(NDGEN,2) , SPF(NDSPF,3)      
      REAL*8    S1         , S2     
C-----------------------------------------------------------------------
      INTEGER   NARR(3)
C-----------------------------------------------------------------------
      CHARACTER ERRARR(1+NDGEN+NDSPF)*70 , sstrg*1
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
       
       
       NDEF = 0
       NGEN = 0
       NSPF = 0
       
       if (num.eq.0) goto 9999
       
       DO I = 1,NUM
       
         STR  = ERRARR(I)
         ILEN = LEN(STR)        

C find '-' seperator of levels
     
         CALL XXFCHR(STR,'-',I1M,I2M)
         IF (I1M.GT.0.AND.I1M.EQ.I2M) THEN
         
C find size of error value - work from right to left to find the seperator
C space between the error and the second level. Note xxfchr cannot find
C spaces as a search string.
         
            STR4 = STR(I1M+1:ILEN)
            ILEN = LEN(STR4)  
            CALL XXSLEN(STR4,I1S,I2S)
            I1S = 0 
            J   = I2S      
            DO WHILE (I1S.EQ.0.OR.J.LE.1) 
               IF (STR4(J:J).EQ.' ')  I1S = J
               J = J - 1
            END DO
            IF (I1S.EQ.0) THEN
               WRITE(I4UNIT(-1),1001)'INVALID ERROR CODE ',STR
               WRITE(I4UNIT(-1),1002)
               STOP
            ENDIF
         ELSE
            WRITE(I4UNIT(-1),1001)'INVALID ERROR CODE ',STR
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
         
         ILEN = LEN(STR)  
         STR1 = STR(1:I1M-1)
         STR2 = STR(I1M+1:I1M+I1S-1)
         STR3 = STR(I1M+I1S+1:ilen)
         CALL XXFCHR(STR1,'*',I1ST,IDUM)
         CALL XXFCHR(STR2,'*',I2ST,IDUM)

         IF (i1st.gt.0.AND.i2st.gt.0) THEN
            NDEF = NDEF + 1
            READ(STR3,*)DEF 
         ENDIF

         IF (i1st.gt.0.AND.i2st.eq.0) THEN
            NGEN = NGEN + 1
            READ(STR2,*)GEN(NGEN,1)
            READ(STR3,*)GEN(NGEN,2)
         ENDIF
            
         IF (i1st.eq.0.AND.i2st.gt.0) THEN
            NGEN = NGEN + 1
            READ(STR1,*)GEN(NGEN,1)
            READ(STR3,*)GEN(NGEN,2)
         ENDIF

         IF (i1st.eq.0.AND.i2st.eq.0) THEN
            NSPF = NSPF + 1
            READ(STR1,*)S1
            READ(STR2,*)S2
            READ(STR3,*)SPF(NSPF,3)
            SPF(NSPF,1) = min(s1,s2)
            SPF(NSPF,2) = max(s1,s2)
         ENDIF
            
       END DO  
           

       NARR(1) = NDEF
       NARR(2) = NGEN
       NARR(3) = NSPF
       
C-----------------------------------------------------------------------
C REMOVE DUPLICATE ENTERIES - LATER!!
C-----------------------------------------------------------------------



C-----------------------------------------------------------------------

 9999  continue

       RETURN

C-----------------------------------------------------------------------
 1000  FORMAT(1A80)
 1001  FORMAT(1X,32('*'),' BGPERR ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,A)
 1002  FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C-----------------------------------------------------------------------

      END
