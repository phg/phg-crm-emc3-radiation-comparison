C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgeset.for,v 1.1 2004/07/06 11:40:06 whitefor Exp $ Date $Date: 2004/07/06 11:40:06 $
C
      SUBROUTINE BGESET( NDTRN  , NDGEN  , NDSPF  , NPLR   , NPLI  ,
     &                   ITRAN  , TCODE  , 
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH  , ICNTI ,
     &                   IETRN  , IPTRN  , IRTRN  , IHTRN  , IITRN ,
     &                   IE1A   , IE2A   , IP1A   , IP2A   ,
     &                   NUMEXC , DEFEXC , GENEXC , SPFEXC ,
     &                   NUMREC , DEFREC , GENREC , SPFREC ,
     &                   NUMCXR , DEFCXR , GENCXR , SPFCXR ,
     &                   NUMION , DEFION , GENION , SPFION ,
     &                   ERRMUL   
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGESET *********************
C
C  PURPOSE:  TO SET THE ERROR MULTIPLER ACCORDING TO ERROR VALUES
C
C  CALLING PROGRAM: ADAS216
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
       INTEGER   NDTRN        , NDGEN           , NDSPF      ,
     &           NPLR         , NPLI            , ITRAN      ,
     &           ICNTE        , ICNTP           , ICNTR      , ICNTH   ,
     &           ICNTI
       INTEGER   I            , J
       INTEGER   IETRN(NDTRN) , IPTRN(NDTRN)    ,
     &           IRTRN(NDTRN) , IHTRN(NDTRN)    ,
     &           IITRN(NDTRN) ,
     &           IE1A(NDTRN)  , IE2A(NDTRN)     ,
     &           IP1A(NDTRN)  , IP2A(NDTRN)
       INTEGER   NUMEXC(3)    , NUMREC(3)       ,
     &           NUMCXR(3)    , NUMION(3)
C-----------------------------------------------------------------------
       REAL*8    DEFEXC       , GENEXC(NDGEN,2) , SPFEXC(NDSPF,3)  ,
     &           DEFREC       , GENREC(NDGEN,2) , SPFREC(NDSPF,3)  ,
     &           DEFCXR       , GENCXR(NDGEN,2) , SPFCXR(NDSPF,3)  ,
     &           DEFION       , GENION(NDGEN,2) , SPFION(NDSPF,3)
       REAL*8    ERRMUL(NDTRN)
C-----------------------------------------------------------------------
       CHARACTER TCODE(NDTRN)*1
C-----------------------------------------------------------------------

      do i = 1, ICNTE
        errmul(ietrn(i)) = 0.0
      end do

      if (numexc(1).eq.1) then
         do i = 1, ICNTE
           errmul(ietrn(i)) = defexc / 100.0
         end do
      endif
      
      if (numexc(2).gt.0) then
      
        do j = 1, numexc(2)
          do i = 1, ICNTE
            if ( ie1a(ietrn(i)).eq.int(genexc(j,1)) ) then
               errmul(ietrn(i)) = genexc(j,2) / 100.0
            endif
          end do
        end do
      
      endif
      
      if (numexc(3).gt.0) then
      
        do j = 1, numexc(3)
          do i = 1, ICNTE
            if ( ie1a(ietrn(i)).eq.int(spfexc(j,1)) .and.
     &           ie2a(ietrn(i)).eq.int(spfexc(j,2)) ) then
               errmul(ietrn(i)) = spfexc(j,3) / 100.0
            endif
          end do
        end do
      
      endif
      
      end
