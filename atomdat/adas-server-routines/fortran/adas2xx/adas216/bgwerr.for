C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgwerr.for,v 1.2 2006/01/04 18:14:34 mog Exp $ Date $Date: 2006/01/04 18:14:34 $
C
      SUBROUTINE BGWERR( IUNIT  ,
     &                   NUMEXC , DEFEXC , GENEXC , SPFEXC ,
     &                   NUMREC , DEFREC , GENREC , SPFREC ,
     &                   NUMCXR , DEFCXR , GENCXR , SPFCXR ,
     &                   NUMION , DEFION , GENION , SPFION 
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGWERR *********************
C
C  PURPOSE:  Writes error information to IUNIT
C
C  CALLING PROGRAM: ADAS216
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C VERSION  : 1.2
C DATE     : 03-01-2006
C MODIFIED : Martin O'Mullane
C              - Increase NDSPF to 2100.
C
C-----------------------------------------------------------------------
       INTEGER   NDGEN  , NDSPF
C-----------------------------------------------------------------------
       PARAMETER( NDGEN  = 5   , NDSPF = 2100   )
C-----------------------------------------------------------------------
       INTEGER   IUNIT        , J
C-----------------------------------------------------------------------
       INTEGER   NUMEXC(3)    , NUMREC(3)       ,
     &           NUMCXR(3)    , NUMION(3)
C-----------------------------------------------------------------------
       REAL*8    DEFEXC       , GENEXC(NDGEN,2) , SPFEXC(NDSPF,3)  ,
     &           DEFREC       , GENREC(NDGEN,2) , SPFREC(NDSPF,3)  ,
     &           DEFCXR       , GENCXR(NDGEN,2) , SPFCXR(NDSPF,3)  ,
     &           DEFION       , GENION(NDGEN,2) , SPFION(NDSPF,3)
C-----------------------------------------------------------------------

      write(iunit,1000)

C Excitation - only process considered at present! 
      
      write(iunit,1010)

      if (numexc(1).eq.1) then
         write(iunit,1100)defexc  
      endif
      write(iunit,1005)
      
      if (numexc(2).gt.0) then
      
        do j = 1, numexc(2)
          write(iunit,1110)int(genexc(j,1)),genexc(j,2)
        end do
      
      endif
      write(iunit,1005)
      
      if (numexc(3).gt.0) then
      
        do j = 1, numexc(3)
          write(iunit,1120)int(spfexc(j,1)),int(spfexc(j,2)),spfexc(j,3)
        end do
      
      endif
      write(iunit,1005)
      
      
      write(iunit,1200)
      
C-----------------------------------------------------------------------
 1000 format(80('-'),/,'Error Analysis - errors adopted ',/) 
 1005 format(1x) 
 1010 format('Excitation Process','------------------',/) 
 1100 format('Default error              : ',f5.1,'%') 
 1110 format('General error to level ',i3,' : ',f5.1,'%') 
 1120 format('Specific error ',i3,' -',i3,'    : ',f5.1,'%') 
 1200 format(80('-'),/) 
C-----------------------------------------------------------------------
      
      end
