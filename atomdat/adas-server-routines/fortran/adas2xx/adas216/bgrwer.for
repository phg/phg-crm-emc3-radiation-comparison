C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgrwer.for,v 1.1 2004/07/06 11:40:46 whitefor Exp $ Date $Date: 2004/07/06 11:40:46 $
C
      SUBROUTINE BGRWER( NDGEN  , NDSPF   , 
     &                   IRW    ,
     &                   NARR   , DEF     , GEN     , SPF  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGRERR *********************
C
C  PURPOSE: TO READ OR WRITE ERROR INFORMATION FROM/TO IDL. THE SIZE
C           OF THE ARRAYS HAS ALREADY BEEN PASSES TO IDL.
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NDGEN    = MAX. NUMBER OF GENERAL ERRORS ALLOWED
C  INPUT : (I*4)   NDSPF    = MAX. NUMBER OF SPECIFIC ERRORS ALLOWED
C
C  INPUT:  (I*4)   irw      = switch for read/write to idl
C                             irw = 0 read
C                             irw = 1 write
C
C
C  OUTPUT: (R*8)   NARR(3)  = NO. OF ERROR TYPES FOR EXCITATION
C                               1ST INDEX:  NO. DEFAULT (0 OR 1)
C                               2ND INDEX:  NO. GENERAL 
C                               3RD INDEX:  NO. SPECIFIC 
C  OUTPUT: (R*8)   DEF      = DEFAULT ERROR
C  OUTPUT: (R*8)   GEN(,)   = GENERAL ERROR
C                               1ST DIMENSION: SOURCE/DESTINATION LEVEL
C                               2ND DIMENSION: ERROR
C
C  OUTPUT: (R*8)   SPF(,)   = SPECIFIC ERROR
C                               1ST DIMENSION: FIRST LEVEL
C                               2ND DIMENSION: SECOND LEVEL
C                               3RD DIMENSION: ERROR
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C  AUTHOR  : MARTIN O'MULLANE,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 17/03/1999
C MODIFIED : MARTIN O'MULLANE  
C            FIRST VERSION.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
C-----------------------------------------------------------------------
      PARAMETER( PIPEIN=5  , PIPEOU=6   )
C-----------------------------------------------------------------------
      INTEGER   NDGEN      , NDSPF
      INTEGER   IRW        , I            , j               
      INTEGER   I4UNIT
C-----------------------------------------------------------------------
      INTEGER   NARR(3)
C-----------------------------------------------------------------------
      REAL*8    DEF        , GEN(NDGEN,2) , SPF(NDSPF,3)      
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
       
       if (irw.eq.0) then
              
          do i = 1,3
            READ(PIPEIN,*) narr(i)
          end do
          
          if (narr(1).eq.1) then
             READ(PIPEIN,*) def
          endif
          
          do i = 1,narr(2)
            do j = 1,2
              READ(PIPEIN,*) gen(i,j)
            end do
          end do
         
          do i = 1,narr(3)
            do j = 1,3
              READ(PIPEIN,*) spf(i,j)
            end do
          end do
          
       else
       
          do i = 1,3
            WRITE(PIPEOU,*) narr(i)
            CALL XXFLSH(PIPEOU)
          end do
          
          if (narr(1).eq.1) then
             WRITE(PIPEOU,*) def
             CALL XXFLSH(PIPEOU)
           endif
          
          if (narr(2).ge.1) then
             do i = 1,narr(2)
               do j = 1,2
                 WRITE(PIPEOU,*) gen(i,j)
                CALL XXFLSH(PIPEOU)
               end do
             end do
          endif
         
          if (narr(3).ge.1) then
             do i = 1,narr(3)
               do j = 1,3
                 WRITE(PIPEOU,*) spf(i,j)
                 CALL XXFLSH(PIPEOU)
               end do
             end do
          endif
          
       endif



C-----------------------------------------------------------------------
       RETURN

       END
