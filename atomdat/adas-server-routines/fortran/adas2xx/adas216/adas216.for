C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/adas216.for,v 1.3 2006/01/04 18:15:03 mog Exp $ Date $Date: 2006/01/04 18:15:03 $
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 PROGRAM: ADAS216 **********************
C
C  VERSION: 1.0
C
C  PURPOSE: 
C           
C
C           
C  PARAMETER: (I*4)   NDGEN    = MAX. NUMBER OF GENERAL ERRORS ALLOWED
C  PARAMETER: (I*4)   NDSPF    = MAX. NUMBER OF SPECIFIC ERRORS ALLOWED
C
C  INPUT : (I*4)   IUNT     = UNIT NO. OF OPENED ADF04 FILE
C
C  OUTPUT: (R*8)   NUMEXC(3)= NO. OF ERROR TYPES FOR EXCITATION
C                               1ST INDEX:  NO. DEFAULT (0 OR 1)
C                               2ND INDEX:  NO. GENERAL 
C                               3RD INDEX:  NO. SPECIFIC 
C  OUTPUT: (R*8)   DEFEXC   = DEFAULT ERROR
C  OUTPUT: (R*8)   GENEXC(,)= GENERAL ERROR
C                               1ST DIMENSION: SOURCE/DESTINATION LEVEL
C                               2ND DIMENSION: ERROR
C
C  OUTPUT: (R*8)   SPFEXC(,)= SPECIFIC ERROR
C                               1ST DIMENSION: FIRST LEVEL
C                               2ND DIMENSION: SECOND LEVEL
C                               3RD DIMENSION: ERROR
C 
C NOTE : SIMILAR DEFINITIONS FOR RECOMBINATION, CHARGE EXCHANGE
C        AND IONISATION.
C           
C           
C
C           
C
C  DATA:   
C
C  PROGRAM:
C
C          (I*4) PIPEIN    = STANDARD INPUT
C          (I*4) PIPEOU    = STANDARD OUTPUT
C
C
C          INPUT/OUTPUT STREAM ALLOCATIONS:
C          --------------------------------
C
C          STREAM  9: INPUT  - ADF04 FILE
C          ('IUNT09')
C
C          STREAM 17: OUTPUT - MAIN OUTPUT PAPER.TXT
C          ('IUNT17')
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          BGSPF0     ADAS      GATHERS INPUT FILE NAME FROM IDL  
C          BGSPF1     ADAS      GATHERS OUTPUT FILE NAMES FROM IDL 
C          BGDAT1     ADAS      READS AUTOSTRUCTURE GENERAL OUTPUT FILE 
C          BGDAT2     ADAS      READS AUTOSTRUCTURE RATE OUTPUT FILE 
C          BGCHIP     ADAS      MAKES STRING FROM PARENT NL VALUES 
C          BGISPF     ADAS      GATHERS USERS VALUES FOR PROGRAM OPTIONS
C          BGASTJ     ADAS      CALCULATES EXCITATION RATES AND WRITES 
C                               ADF04 FILES
C          BGRERR     ADAS      READS ERRORS FROM ADF04 FILES
C
C          XXSLEN     ADAS      GET LENGTH OF STRING VARIABLE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXTCON     ADAS      CONVERTS ENTERED TEMPS. TO EV/RED/KELVIN
C          XXDATE     ADAS      OBTAINS DATE FROM IDL
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C          TIMEF      SYSTEM    
C          DLRAN      NETLIB
C
C          BGWR33     ADAS      WRITES DATA TO UNFORMATTED FILE
C
C AUTHOR   : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          
C DATE     : 17/03/1999
C MODIFIED : Martin O'Mullane  
C            First version.
C
C VERSION  : 1.2                          
C DATE     : 20/10/1999
C MODIFIED : Martin O'Mullane  
C            - Changes to accommodate the revised b8ttyp routine.  
C
C VERSION  : 1.3
C DATE     : 03-01-2006
C MODIFIED : Martin O'Mullane
C               - Use xxdata_04 to read adf04 file. This requires
C                 new arrays some of which are not used in the
C                 population calculation.
C               - bxttyp used instead of b8ttyp.
C               - Increase NDSPF to 2100 the same as NDTRN.
C
C-----------------------------------------------------------------------
       INTEGER    NDGEN        , NDSPF
       INTEGER    IUNT09       , IUNT17      , IUNT27 , IUNT33 ,
     &            ICSTMX       , PIPEIN      , PIPEOU     
       INTEGER    NDLEV        , NDTRN       , NDTEM  , NDDEN  , NDMET
       INTEGER    NVMAX        , NDQDN
       INTEGER    ONE          , ZERO
       integer    ndbin        , ndtr
       real*8     umin         , umax        , ubin   , iumax
       real*8     epsilon
C-----------------------------------------------------------------------
       PARAMETER( IUNT09 = 09  , IUNT17 = 17   , IUNT27 = 07  , 
     &            IUNT33 = 33  )
       PARAMETER( ICSTMX = 11  )
       PARAMETER( PIPEIN = 5   , PIPEOU = 6    )
        
       PARAMETER( NDTEM  = 20  , NDDEN  = 24   , NDMET  = 4   )
       PARAMETER( NDLEV  = 100 , NDTRN  = 2100 , NVMAX  = 14  )
       PARAMETER( NDGEN  = 5   , NDSPF  = 2100 )
       PARAMETER( NDQDN  = 6   )
       
       PARAMETER( ONE    = 1   , ZERO  = 0     )
       
       PARAMETER( umin   = 0.6D0 , umax  = 1.4D0   , ubin = 0.005D0 )
       PARAMETER( iumax  = 100 )
       PARAMETER( ndbin  = 1 + ( (umax-umin)/ubin ) )   
       PARAMETER( ndtr   = 20 )   
C-----------------------------------------------------------------------
       INTEGER    I            , IFORM       , L1      , L2 
       INTEGER    IZ0          , IZ          , IZ1     , 
     &            npl          , nplr        , NPLI   
       INTEGER    ISTOP        , I4UNIT      , LOGIC
       INTEGER    NION         , IFAIL       ,
     &            IL           , NV          , ITRAN   ,
     &            MAXLEV       , NMET        , NORD    , 
     &            IFOUT        , IDOUT       ,
     &            MAXT         , MAXD        , NIND    , NSPEC                  
       INTEGER    ICNTE        , ICNTP       , ICNTR   , ICNTH   ,  
     &            ICNTI        , ICNTL       , ICNTS
       INTEGER    numiter
       INTEGER    indlevel     , indtev      , inddens ,
     &            indl_st      , indt_st     , indd_st    
       INTEGER    it,in, idum, id, ilev, itr , j, k, ioff, ih, i4indf
       INTEGER    kl, kt, kd , kh
       INTEGER    indtype      , indtd       ,
     &            indty_st     , indtd_st    ,
     &            ierrsign     , ierrs_st    , itranused , index
       INTEGER    ianalysis    , numcom
C-----------------------------------------------------------------------
       CHARACTER  DSNIN*80     , DSNEXP*80   , DUMPFILE*80  
       CHARACTER  DSNPAP*80    , DSN80*80
       CHARACTER  DATE*8       , REP*3       , CADAS*120
       CHARACTER  CELEM*3      , TITLE*40
C-----------------------------------------------------------------------
       REAL*8    BWNO      , ZEFF   
       REAL*8    XMIN      , XMAX      , YMIN      , YMAX
       REAL*8    ELAPSED   , ELAPSED_s , ELAPSED_F , TIMEF
       real*8    x         , dlarnd    , pdiv      , ratio
       REAL*8    errtol    , errt_st   , sem       , error 
       real*4    dum       , dtime
C-----------------------------------------------------------------------
       LOGICAL   OPEN09    , OPEN17    , OPEN27
       LOGICAL   LPAPER    , LREP      , LGRAPH  , LDEF
       LOGICAL   LPEND     , LDSEL     , LPSEL   , LZSEL   , LIOSEL  ,
     &           LHSEL     , LRSEL     , LISEL   , LNSEL
       LOGICAL   lnewind   , lback
C-----------------------------------------------------------------------
       INTEGER   INDA(NDTRN)
       INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     ,
     &           IA(NDLEV)        ,
     &           ISA(NDLEV)       , ILA(NDLEV)       ,
     &           I1A(NDTRN)       , I2A(NDTRN)       ,
     &           NPLA(NDLEV)      , IPLA(NDMET,NDLEV)
       INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &           IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &           IITRN(NDTRN)     , ILTRN(NDTRN)   ,
     &           ISTRN(NDTRN)     ,
     &           IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &           IP1A(NDTRN)      , IP2A(NDTRN)      ,
     &           IA1A(NDTRN)      , IA2A(NDTRN)      ,
     &           IL1A(NDLEV)      , IL2A(NDLEV)      ,
     &           IS1A(NDLEV)      , IS2A(NDLEV)
       INTEGER   NUMEXC(3)        , NUMREC(3)        ,
     &           NUMCXR(3)        , NUMION(3)
       INTEGER   ISEED(4)
       integer   ihist(NDLEV,NDTEM,NDDEN,ndbin) 
       INTEGER   ind_t(ndlev,ndden)    , index_t(ndlev,ndden,ndtr)
       INTEGER   ind_d(ndlev,ndtem)    , index_d(ndlev,ndtem,ndtr)
C-----------------------------------------------------------------------
       integer*2 IH_OUT(NDLEV,NDTEM,NDDEN,NDBIN) 
C-----------------------------------------------------------------------
       integer   int_out(ndlev,ndden)       , ind_out(ndlev,ndtem)
       integer   it_out(ndlev,ndden,ndtr)   , id_out(ndlev,ndtem,ndtr)
C-----------------------------------------------------------------------
       REAL*4    adt_out(ndlev,ndden,ndtr) , errt_out(ndlev,ndden,ndtr),
     &           rt_out(ndlev,ndtem,ndden,ndtr)
       REAL*4    add_out(ndlev,ndtem,ndtr) , errd_out(ndlev,ndtem,ndtr),
     &           rd_out(ndlev,ndtem,ndden,ndtr)
C-----------------------------------------------------------------------
       REAL*4    TARRY(2)
       REAL*4    data_out(NDLEV,NDTEM,NDDEN)
C-----------------------------------------------------------------------
       REAL*8    COEF(NDTRN,NDTEM)
       REAL*8    SCEF(NVMAX)     , BWNOA(NDMET)     , PRTWTA(NDMET) ,
     &           TINE(NDTEM)     , TINP(NDTEM)      , TINH(NDTEM)   ,
     &           TEVA(NDTEM)     , TPVA(NDTEM)      , THVA(NDTEM)   ,
     &           TEA(NDTEM)      , TPA(NDTEM)       , THA(NDTEM)    ,
     &           DINE(NDDEN)     , DINP(NDDEN)      ,
     &           DENSA(NDTEM)    , DENSPA(NDTEM)    ,
     &           RATHA(NDTEM)    , RATPIA(NDDEN,NDMET),
     &           RATIA(NDTEM)    ,
     &           RATMIA(NDDEN,NDMET),
     &           AA(NDTRN)       , AVAL(NDTRN)      , AUGA(NDTRN)   ,
     &           WVLA(NDLEV)     ,
     &           XJA(NDLEV)      , WA(NDLEV)        , ER(NDLEV)     ,
     &           XIA(NDLEV)      , ZPLA(NDMET,NDLEV)
       REAL*8    TSCEF(NVMAX,3)  , SCOM(NVMAX,NDTRN)
       REAL*8    DEFEXC          , GENEXC(NDGEN,2)  , SPFEXC(NDSPF,3)  ,
     &           DEFREC          , GENREC(NDGEN,2)  , SPFREC(NDSPF,3)  ,
     &           DEFCXR          , GENCXR(NDGEN,2)  , SPFCXR(NDSPF,3)  ,
     &           DEFION          , GENION(NDGEN,2)  , SPFION(NDSPF,3)

       REAL*8    ERRMUL(NDTRN)
       REAL*8    POPUN(NDLEV,NDTEM,NDDEN)  , POPAR(NDLEV,NDTEM,NDDEN)  ,
     &           SCOM_NEW(NVMAX,NDTRN)
       REAL*8    xhist(ndbin)
       REAL*8    adiff_t(ndlev,ndden,ndtr) , err_t(ndlev,ndden,ndtr)   ,
     &           rt(ndlev,ndtem,ndden,ndtr)  
       REAL*8    adiff_d(ndlev,ndtem,ndtr) , err_d(ndlev,ndtem,ndtr)   ,
     &           rd(ndlev,ndtem,ndden,ndtr)  
C-----------------------------------------------------------------------
       CHARACTER TCODE(NDTRN)*1  , CSTRGA(NDLEV)*18 , CSTRGB(NDLEV)*18 ,
     &           STRGA(NDLEV)*22
 
       CHARACTER CPLA(NDLEV)*1   , CPRTA(NDMET)*9
       CHARACTER STRGMF(NDMET)*11, STRGMI(NDLEV)*12
       CHARACTER outcom(210+200*nvmax)*80
C-----------------------------------------------------------------------
       LOGICAL   LBSETA(NDMET)
       LOGICAL   LSS04A(NDLEV,NDMET)
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   iadftyp       , itieactn , iorb    
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      real*8    beth(ndtrn)   , qdorb((ndqdn*(ndqdn+1))/2)         , 
     &          qdn(ndqdn)
C----------------------------------------------------------------------
      logical   ltied(ndlev)  , lqdorb((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------



C----------------------------------------------------------------------
       DATA      OPEN09/.FALSE./ , OPEN17/.FALSE./
       DATA      REP   / 'NO' /  , CADAS / ' ' /
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

       CALL XX0000

C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

       CALL XXDATE( DATE )
     
C-----------------------------------------------------------------------
C For the histogram x-axis - it runs from umin to umax with ubin binsize.
C Also zero the histograms.
C-----------------------------------------------------------------------

      do i=1, ndbin
        xhist(i) = i*ubin + (umin-ubin)
      end do


       do kh=1,ndbin
         do kd=1,ndden
           do kt=1,ndtem
             do kl=1,ndlev
            
               ihist(kl,kt,kd,kh) = 0
             
             end do
           end do
         end do
       end do
       
       lback = .FALSE.
       
C-----------------------------------------------------------------------
C initialise explore errors number of top transitions array
C-----------------------------------------------------------------------

       do kd=1,ndden
         do kl=1,ndlev
         
           ind_t(kl,kd)=0
         
         end do
       end do
          
       do kt=1,ndtem
         do kl=1,ndlev
         
           ind_t(kl,kt)=0
         
         end do
       end do
          
C-----------------------------------------------------------------------
C IF INPUT FILE IS OPEN CLOSE THE UNIT
C-----------------------------------------------------------------------
 
 100   CONTINUE
  
       IF (OPEN09) THEN
          CLOSE(IUNT09)
          OPEN09 = .FALSE.
       ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAMES (FROM IDL PIPE).
C-----------------------------------------------------------------------

       CALL BGSPF0( REP , DSNIN , DSNEXP )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE FILE ON UNIT 9 AND END RUN
C FILE CLOSURE MOVED TO 9999 SO THAT MENU BUTTON ALSO CLOSES FILES
C-----------------------------------------------------------------------

       IF (REP.EQ.'YES') GOTO 9999

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE AND READ IN ERRORS FOLLOWED BY THE DATA
C-----------------------------------------------------------------------

      CALL XXSLEN(DSNIN,L1,L2)
      OPEN( UNIT=IUNT09 , FILE=DSNIN(L1:L2)  , STATUS='OLD' )
      OPEN09=.TRUE.
        
      CALL BGRERR( IUNT09 , 
     &             NUMEXC , DEFEXC  , GENEXC  , SPFEXC  ,
     &             NUMREC , DEFREC  , GENREC  , SPFREC  ,
     &             NUMCXR , DEFCXR  , GENCXR  , SPFCXR  ,
     &             NUMION , DEFION  , GENION  , SPFION  )

      REWIND(IUNT09)

      itieactn = 0
      
      call xxdata_04( iunt09 , 
     &                ndlev  , ndtrn , ndmet   , ndqdn , nvmax ,
     &                celem  , iz    , iz0     , iz1   , bwno  ,
     &                npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                ia     , cstrga, isa     , ila   , xja   ,
     &                wa     ,
     &                cpla   , npla  , ipla    , zpla  ,
     &                nv     , scef  ,
     &                itran  , maxlev,
     &                tcode  , i1a   , i2a     , aval  , scom  ,
     &                beth   ,     
     &                iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                lstyp  , lltyp , itieactn, ltied
     &              )

      CLOSE(IUNT09)

C-----------------------------------------------------------------------
C REDUCED LEVEL NOMENCLATURE STRINGS TO LAST 'ICSTMX' NON-BLANK BYTES
C-----------------------------------------------------------------------

       CALL BXCSTR( CSTRGA , IL     , ICSTMX  , CSTRGB   )

C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPES.
C-----------------------------------------------------------------------

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )

C-----------------------------------------------------------------------
C Test integrity of the adf04 file - electron impact only
C-----------------------------------------------------------------------
      
      CALL BGTEST( IZ1    , IL      ,
     &             IA     , ISA     , ILA   , XJA   , WA   ,
     &             NV     , SCEF    ,
     &             itran  ,
     &             TCODE  , I1A     , I2A   , AVAL  , SCOM ,
     &             numcom , outcom
     &           )

C-----------------------------------------------------------------------
C CALCULATE LEVEL ENERGIES RELATIVE TO LEVEL 1 & IONISATION POT. IN RYDS
C-----------------------------------------------------------------------

       CALL XXERYD( BWNO   , IL     , WA      , ER       , XIA )

C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------

       DO IFORM = 1,3
          CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
       END DO

C-----------------------------------------------------------------------
C SET UP INFO TO BE PASSED TO IDL FOR DISPLAY
C-----------------------------------------------------------------------

       CALL BGSETM( IZ0    , IZ     ,
     &              NDLEV  , IL     , ICNTE ,
     &              CSTRGB , ISA    , ILA   , XJA   ,
     &              STRGA  , NPL    , CPRTA , NDMET ,
     &              STRGMF , STRGMI
     &            )
 
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL)
C-----------------------------------------------------------------------

 200   CONTINUE

       CALL BGISPF( LPEND  , CELEM,
     &              NDTEM  , NDDEN   , NDMET   , IL      ,
     &              NVMAX  , NV      , TSCEF   ,
     &              numcom , outcom  ,
     &              NDGEN  , NDSPF   ,
     &              NUMEXC , DEFEXC  , GENEXC  , SPFEXC  ,
     &              NUMREC , DEFREC  , GENREC  , SPFREC  ,
     &              NUMCXR , DEFCXR  , GENCXR  , SPFCXR  ,
     &              NUMION , DEFION  , GENION  , SPFION  ,
     &              TITLE  , NMET    , IMETR   ,
     &              IFOUT  ,
     &              MAXT   , TINE    , TINP    , TINH    ,
     &              IDOUT  ,
     &              MAXD   , DINE    , DINP    ,
     &              RATHA  , RATPIA  , RATMIA  ,
     &              ZEFF   ,
     &              LPSEL  , LZSEL   , LIOSEL  , LHSEL   ,
     &              LRSEL  , LISEL   , LNSEL   )
 
  

C----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100

C-----------------------------------------------------------------------
C GET ERROR ANALYSIS OPTIONS (FROM IDL) - BGISPF2
C-----------------------------------------------------------------------

 300   CONTINUE

C GET ERROR MULTIPLER ON ALL TRANSITIONS
         
       CALL BGESET( NDTRN  , NDGEN  , NDSPF  , NPLR   , NPLI  ,
     &              ITRAN  , TCODE  ,
     &              ICNTE  , ICNTP  , ICNTR  , ICNTH  , ICNTI ,
     &              IETRN  , IPTRN  , IRTRN  , IHTRN  , IITRN ,
     &              IE1A   , IE2A   , IP1A   , IP2A   ,
     &              NUMEXC , DEFEXC , GENEXC , SPFEXC ,
     &              NUMREC , DEFREC , GENREC , SPFREC ,
     &              NUMCXR , DEFCXR , GENCXR , SPFCXR ,
     &              NUMION , DEFION , GENION , SPFION ,
     &              ERRMUL
     &             )
     
        
C UNPERTURBED POPULATION

      CALL BGCOEF(LPSEL  , LZSEL  , LIOSEL ,
     &            LHSEL  , LRSEL  , LISEL  , LNSEL ,
     &            NMET   , IMETR  , IFOUT  , IDOUT ,
     &            MAXT   , TINE   , TINP   , TINH  ,
     &            MAXD   , DINE   , DINP   ,
     &            AA     , XIA    , XJA    , ER    , ZEFF  ,
     &            ICNTE  , ICNTP  , ICNTR  , ICNTH , ICNTI ,
     &            IETRN  , IPTRN  , IRTRN  , IHTRN , IITRN ,
     &            IE1A   , IE2A   , IP1A   , IP2A  ,
     &            I1A    , I2A    ,
     &            IL     , NV     , SCEF   , TSCEF , SCOM  ,
     &            IUNT27 , OPEN27 , IZ0    , IZ1   ,
     &            DSNEXP , DSNIN  ,
     &            MAXLEV , npl    , nplr   , npli  ,NPLA   , IPLA   ,
     &            BWNO   , BWNOA  , PRTWTA ,
     &            POPUN
     &           )

        
C-----------------------------------------------------------------------
C WRITE HISTOGRAM AND NUMTRAN INFO TO IDL via BGISPF2. 
C-----------------------------------------------------------------------
       
       
       WRITE(PIPEOU,*)umin
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)umax
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)ubin
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)iumax
       CALL XXFLSH(PIPEOU)
       
       WRITE(PIPEOU,*)icnte              ! excitation only for now
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)ndtr               ! number of significant one to inspect 
       CALL XXFLSH(PIPEOU)       
       
C Read DUMPFILE name from IDL 

       READ(PIPEIN,'(a)') dumpfile
       CALL XXSLEN(dumpfile,L1,L2)


C Read stored analysis method  from default file 

       READ(PIPEIN,*) ianalysis
      
       

C-----------------------------------------------------------------------
C DO THE WORK !!
C INTERACT WITH IDL - read/write to cw_adas216_errorproc.pro       
C-----------------------------------------------------------------------


C set the random number generator and initialize the saved monitor options
C for cumulative and explore errors options

      call bgseed(iseed)
      
      if (.not.lback) ioff = 0

      indl_st  = -1
      indt_st  = -1
      indd_st  = -1

      indl_st   = -1
      indty_st  = -1
      indtd_st  = -1
      errt_st   = -1
      ierrs_st  = -9


       
C START THE INTERACTION
C return codes from IDL (cw_adas216_errorproc and bgispf2) :
C     -999  :  start cumulative error calculation
C     -888  :  start explore errors calculation
C        0  :  Done (trapped in cw_adas216_cumerr and cw_adas216_experr
C                    to make sure that something has been done.)
C        1  :  Cancel
C        2  :  Menu button pressed


      READ(PIPEIN,*) idum
      
      if (idum.eq.-999) then          
         goto 350
      elseif (idum.eq.-888) then
         goto 370
      elseif (idum.eq.0) then
         if (ianalysis.eq.0) then
            goto 350
         else
            goto 370
         endif
      elseif (idum.eq.2) then
         goto 395
      elseif (idum.eq.1) then
         goto 200
      else
         write(i4unit(-1),*)'There is something odd afoot!'
      endif
      
      
      
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C CUMULATIVE error option
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      
  350 CONTINUE         
      
      OPEN( UNIT=IUNT33 , FILE=dumpfile(L1:L2) ,FORM='UNFORMATTED')
      
      READ(PIPEIN,*) numiter
      READ(PIPEIN,*) indlevel 
      READ(PIPEIN,*) indtev
      READ(PIPEIN,*) inddens
      

C Check if these have changed since last iteration - if so pass the
C new monitor ploy data to IDL, otherwise continue. Check also if 
C this is the first run.  
      
      lnewind = (indlevel.ne.indl_st).or.(indtev.ne.indt_st).or.
     &          (inddens.ne.indd_st) 
      
      if ( lnewind.and.(ioff.gt.0) ) then
         
         WRITE(PIPEOU,*) one
         CALL XXFLSH(PIPEOU)
         
         do j = 1, ndbin
           WRITE(PIPEOU,*)ihist(indlevel,indtev,inddens,j)
           CALL XXFLSH(PIPEOU)
         end do
           
      else
      
         WRITE(PIPEOU,*) zero
         CALL XXFLSH(PIPEOU)
        
      endif
      
      indl_st  = indlevel
      indt_st  = indtev
      indd_st  = inddens


C unperturbed population for this set
      
      pdiv = popun(indlevel,indtev,inddens)
  
      
C set the time at start of iteration

      elapsed_s = dtime(tarry)
       
       
       do i=1,numiter
       
C         x = dlarnd(3,iseed)   ! should this be moved into inner do loop???
         
         do itr = 1, icnte
           do it=1,nv
             x = dlarnd(3,iseed)  
             scom_new(it,itr) = scom(it,itr) * (1.0 + x * errmul(itr))
           end do
         end do
     
         call xxflsh(0)
         
         CALL BGCOEF(LPSEL  , LZSEL  , LIOSEL ,
     &               LHSEL  , LRSEL  , LISEL  , LNSEL ,
     &               NMET   , IMETR  , IFOUT  , IDOUT ,
     &               MAXT   , TINE   , TINP   , TINH  ,
     &               MAXD   , DINE   , DINP   ,
     &               AA     , XIA    , XJA    , ER    , ZEFF  ,
     &               ICNTE  , ICNTP  , ICNTR  , ICNTH , ICNTI ,
     &               IETRN  , IPTRN  , IRTRN  , IHTRN , IITRN ,
     &               IE1A   , IE2A   , IP1A   , IP2A  ,
     &               I1A    , I2A    ,
     &               IL     , NV     , SCEF   , TSCEF , SCOM_new  ,
     &               IUNT27 , OPEN27 , IZ0    , IZ1   ,
     &               DSNEXP , DSNIN  ,
     &               MAXLEV , npl    , nplr   , npli  ,NPLA   , IPLA  ,
     &               BWNO   , BWNOA  , PRTWTA ,
     &               POPAR
     &              )
         
         call xxflsh(0)
         
         
C update histogram 
         
         do kd=1,maxd
           do kt=1,maxt
             do kl=1,il

               if (popun(kl,kt,kd).ne.0.0) then

                  ratio = popar(kl,kt,kd) / popun(kl,kt,kd)
                  ih    = i4indf(ndbin, xhist, ratio)
                  if (ih.gt.0) then
                     ihist(kl,kt,kd,ih) = ihist(kl,kt,kd,ih) + 1
                  endif
                                    
               endif

             end do
           end do
         end do
         

C inform IDL that an iteration has passed

         if (pdiv.eq.0.0) then 
           ratio = 99.99
         else
            ratio = popar(indlevel,indtev,inddens)/pdiv
         endif

         WRITE(PIPEOU,*) ratio
         CALL XXFLSH(PIPEOU)
                            
       end do
       
C Write unperturbed population and histograms to the unformatted DUMPFILE.
C Close the dumpfile as additional iterations update the histogram and
C the file must be replaced. The data reduction takes place here!

       call bgwr33( iunt33 , il , maxt , maxd , popun , data_out )
       call bghist( iunt33 , il , maxt , maxd , ihist , ih_out   )
       
       close(iunt33)
   
       
       elapsed_f = dtime(tarry)
  
       WRITE(PIPEOU,*) (ELAPSED_F ) 
       CALL XXFLSH(PIPEOU)


C Skip explore error option
       
       goto 390

       
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C EXPLORE ERRORS option
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

  370  CONTINUE
       
 
C Set scom_new to scom and reset ind_t / ind_d

       do itr = 1, icnte
         do it=1,nv
           scom_new(it,itr) = scom(it,itr)
         end do
       end do

       do kd=1,ndden
         do kl=1,ndlev
         
           ind_t(kl,kd)=0
         
         end do
       end do
       
       do kt=1,ndtem
         do kl=1,ndlev
         
           ind_t(kl,kt)=0
         
         end do
       end do
 
       
C Open dumpfile and read in info from IDL
      
       OPEN( UNIT=IUNT33 , FILE=dumpfile(L1:L2) ,FORM='UNFORMATTED')
 
       READ(PIPEIN,*) errtol
       READ(PIPEIN,*) indlevel
       READ(PIPEIN,*) indtype
       READ(PIPEIN,*) indtd
      

C Check if these have changed since last iteration - if so start again
      
       lnewind = (indlevel.ne.indl_st).or.(indtype.ne.indty_st).or.
     &           (indtd.ne.indtd_st).or.(errtol.ne.errt_st)
 
       if ( lnewind ) then
 
          WRITE(PIPEOU,*) one
          CALL XXFLSH(PIPEOU)
 
       else
 
          WRITE(PIPEOU,*) zero
          CALL XXFLSH(PIPEOU)
          goto 377
 
       endif
 
       indl_st   = indlevel
       indty_st  = indtype
       indtd_st  = indtd
       errt_st   = errtol



      
C Tell IDL the number of transitions (again??? why??)

       WRITE(PIPEOU,*) icnte  
       CALL XXFLSH(PIPEOU)

     
     
C set the time at start of iteration

       elapsed_s = dtime(tarry)
 
 
       itranused = 0
 
        do itr = 1,icnte
        
C adjust the collision strengths in turn - firstly positive
         
          do it=1,nv
            scom_new(it,itr) = scom(it,itr) * (1.0 + errmul(itr))
          end do
 
          CALL BGCOEF(LPSEL  , LZSEL  , LIOSEL ,
     &                LHSEL  , LRSEL  , LISEL  , LNSEL ,
     &                NMET   , IMETR  , IFOUT  , IDOUT ,
     &                MAXT   , TINE   , TINP   , TINH  ,
     &                MAXD   , DINE   , DINP   ,
     &                AA     , XIA    , XJA    , ER    , ZEFF  ,
     &                ICNTE  , ICNTP  , ICNTR  , ICNTH , ICNTI ,
     &                IETRN  , IPTRN  , IRTRN  , IHTRN , IITRN ,
     &                IE1A   , IE2A   , IP1A   , IP2A  ,
     &                I1A    , I2A    ,
     &                IL     , NV     , SCEF   , TSCEF , SCOM_new  ,
     &                IUNT27 , OPEN27 , IZ0    , IZ1   ,
     &                DSNEXP , DSNIN  ,
     &                MAXLEV , npl    , nplr   , npli  ,NPLA   , IPLA ,
     &                BWNO   , BWNOA  , PRTWTA ,
     &                POPAR
     &               )
         

          

C send IDL the +ive errors for monitoring

         if (indtype.eq.0) then

           do j =1, maxt
             if (popun(indlevel,j,indtd).eq.0) then 
                 ratio = 99.99
              else
                 ratio = popar(indlevel,j,indtd)/popun(indlevel,j,indtd)
              endif
              WRITE(PIPEOU,*)ratio 
              CALL XXFLSH(PIPEOU)
            end do

         else  

            do j =1, maxd
              if (popun(indlevel,indtd,j).eq.0) then 
                 ratio = 99.99
              else
                 ratio = popar(indlevel,indtd,j)/popun(indlevel,indtd,j)
              endif
              WRITE(PIPEOU,*)ratio 
              CALL XXFLSH(PIPEOU)
            end do

         endif      
              
C update the (ndtr) subset to be passed to IDL for inspection
 
         error = 1.0 + errmul(itr)
         index = i1a(itr)*1000 + i2a(itr)

         call bgdiff( il     , maxt  , maxd    ,
     &                popun  , popar ,
     &                error  , index ,
     &                ind_t  , err_t , index_t , adiff_t , rt ,
     &                ind_d  , err_d , index_d , adiff_d , rd 
     &              )
             
              
C Reset scom_new to original value and adjust the collision 
C strengths for negative errors.
         
          do it=1,nv
            scom_new(it,itr) = scom(it,itr) 
            scom_new(it,itr) = scom(it,itr) * (1.0 - errmul(itr))
          end do

 
          CALL BGCOEF(LPSEL  , LZSEL  , LIOSEL ,
     &                LHSEL  , LRSEL  , LISEL  , LNSEL ,
     &                NMET   , IMETR  , IFOUT  , IDOUT ,
     &                MAXT   , TINE   , TINP   , TINH  ,
     &                MAXD   , DINE   , DINP   ,
     &                AA     , XIA    , XJA    , ER    , ZEFF  ,
     &                ICNTE  , ICNTP  , ICNTR  , ICNTH , ICNTI ,
     &                IETRN  , IPTRN  , IRTRN  , IHTRN , IITRN ,
     &                IE1A   , IE2A   , IP1A   , IP2A  ,
     &                I1A    , I2A    ,
     &                IL     , NV     , SCEF   , TSCEF , SCOM_new  ,
     &                IUNT27 , OPEN27 , IZ0    , IZ1   ,
     &                DSNEXP , DSNIN  ,
     &                MAXLEV , npl    , nplr   , npli  ,NPLA   , IPLA ,
     &                BWNO   , BWNOA  , PRTWTA ,
     &                POPAR
     &               )
         

                           
C inform IDL that an iteration has passed by sending the -ive errors to plot

         if (indtype.eq.0) then

           do j =1, maxt
              if (popun(indlevel,j,indtd).eq.0) then 
                 ratio = 99.99
              else
                 ratio = popar(indlevel,j,indtd)/popun(indlevel,j,indtd)
              endif
              WRITE(PIPEOU,*)ratio 
              CALL XXFLSH(PIPEOU)
            end do

         else  

            do j =1, maxd
              if (popun(indlevel,indtd,j).eq.0) then 
                 ratio = 99.99
              else
                 ratio = popar(indlevel,indtd,j)/popun(indlevel,indtd,j)
              endif
              WRITE(PIPEOU,*)ratio 
              CALL XXFLSH(PIPEOU)
            end do

         endif      


C update the (ndtr) subset to be passed to IDL for inspection
 
         error = 1.0 - errmul(itr)
         index = i1a(itr)*1000 + i2a(itr)
         
         call bgdiff( il     , maxt  , maxd    ,
     &                popun  , popar ,
     &                error  , index ,
     &                ind_t  , err_t , index_t , adiff_t , rt ,
     &                ind_d  , err_d , index_d , adiff_d , rd 
     &              )

C reset scom_new to original value 
         
          do it=1,nv
            scom_new(it,itr) = scom(it,itr) 
          end do


       end do


C Write data to the dumpfile.

       call bgwexp( iunt33 ,
     &              ndlev  , ndtem , ndden   , ndtr     ,
     &              il     , maxt  , maxd    ,
     &              ind_t  , err_t , index_t , adiff_t  , rt ,
     &              ind_d  , err_d , index_d , adiff_d  , rd ,
     &              int_out, it_out ,errt_out , adt_out , rt_out ,
     &              ind_out, id_out ,errd_out , add_out , rd_out   
     &            )
       
        
       close(iunt33)
   
       
 377   CONTINUE
 
       elapsed_f = dtime(tarry)
  
       WRITE(PIPEOU,*) (ELAPSED_F)
       CALL XXFLSH(PIPEOU)
          

C Skip anything else! 
       
       goto 390

       
       
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  READ OUTPUT FROM 2nd (or subsequent) GO button or from bgispf2.pro
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
 
 390   CONTINUE
       
         
       read(pipein,*)idum

       if (idum.eq.-999) then  
          ioff = ioff + numiter
          goto 350
       elseif (idum.eq.-888) then
          goto 370
       elseif (idum.eq.1) then 
          LPEND = .TRUE.
       elseif (idum.eq.0) then 
          LPEND = .FALSE.
          READ(PIPEIN,'(A)') TITLE
       elseif (idum.eq.2) then
          LPEND = .FALSE.
       else
          write(i4unit(-1),*)'Should never reach this statement'
       endif 
       
         
C----------------------------------------------------------------------
C READ SIGNAL FROM IDL to either go back, continue or exit
C-----------------------------------------------------------------------

 395   CONTINUE
       
       READ(PIPEIN,*) ISTOP
       IF (ISTOP.EQ.1) GOTO 9999


C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO PROCESSING PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 200




C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN (TIN -> TKEL)
C-----------------------------------------------------------------------

C      CALL XXTCON( ITTYP , 1 , IZ1 , ITVAL , TIN , TKEL )


C-----------------------------------------------------------------------
C GET OUTPUT FILE NAMES FROM IDL.
C-----------------------------------------------------------------------

 400  CONTINUE


      CALL BGSPF1( LGRAPH     , LPAPER     , DSNPAP   ,
     &             LPEND      , CADAS      , LREP
     &           )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO ERROR CALCULATION WINDOW.
C-----------------------------------------------------------------------

      IF (LPEND) then
         lback = .TRUE.
         GOTO 300
      endif
      


C-----------------------------------------------------------------------
C OPEN OUTPUT FILES PAPER.TXT 
C-----------------------------------------------------------------------

      if (lpaper) then
       
         IF (OPEN17 .AND. LREP) THEN
            OPEN17=.FALSE.
            CLOSE(IUNT17)
         ENDIF
         IF (.not.open17) then
            DSN80=' '
            DSN80=DSNPAP
            OPEN(UNIT=IUNT17,FILE=DSN80, STATUS='UNKNOWN')
            OPEN17=.TRUE.
         ENDIF


C Write adf04 details and integrity test results to paper.text.
C Write errors used in this analysis.
C Remaining details are written from IDL. 

         CALL BGOUT0( NDMET  , IUNT17 , DATE   ,
     &                DSNIN  , DSNEXP ,
     &                CELEM  , IZ     , IZ0    , IZ1    , BWNO   ,
     &                NPL    , NPLR   , NPLI   , BWNOA  ,
     &                ICNTE  , ICNTP  , ICNTR  , ICNTH  , ICNTI  ,
     &                IL     ,
     &                IA     , CSTRGA , ISA    , ILA    , XJA    ,
     &                WA     , ER     , CPLA   ,
     &                NV     , TSCEF  , CADAS  ,
     &                nvmax  , numcom , outcom
     &              )
         CALL BGWERR( IUNT17 ,
     &                NUMEXC , DEFEXC , GENEXC , SPFEXC ,
     &                NUMREC , DEFREC , GENREC , SPFREC ,
     &                NUMCXR , DEFCXR , GENCXR , SPFCXR ,
     &                NUMION , DEFION , GENION , SPFION
     &               )
      
         
         if (ianalysis.eq.0) then
            write(iunt17,3001)max(ioff,numiter)
         else
             write(iunt17,3005)
         endif
         
         CLOSE(IUNT17)
          
      endif

C-----------------------------------------------------------------------
C Graphics output 
C-----------------------------------------------------------------------

      if (lgraph) then
       
         READ(PIPEIN,*) ISTOP
         IF (ISTOP.EQ.1) GOTO 9999
         
      endif

C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANELS. (ON RETURN UNITS ARE CLOSED)
C-----------------------------------------------------------------------

      GOTO 400
    
C-----------------------------------------------------------------------
     
 9999 CONTINUE

      IF (OPEN09) CLOSE( IUNT09 )
      IF (OPEN17) CLOSE( IUNT17 )
     
C-----------------------------------------------------------------------

 2000 format('maxt, maxd, nlev :',3i5)
 2005 format(1p,20d10.3)
 2007 format(a)
 2010 format(1p,8d10.3)

 3001 format('Cumulative error analysis results for',i5,' iterations',
     &       /,/,'Mean and standard deviation are tabulated',/)

 3005 format('Exploration of errors analysis results',/)
C-----------------------------------------------------------------------
      END
