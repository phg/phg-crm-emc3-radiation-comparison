C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgispf2.for,v 1.1 2004/07/06 11:40:18 whitefor Exp $ Date $Date: 2004/07/06 11:40:18 $
C
      SUBROUTINE BGISPF2( LPEND  , TITLE  
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGISPF2 ********************
C
C  PURPOSE: ERROR ANALYSIS PROCESSING PANEL INPUT FOR ADAS216
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTEM    = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDDEN    = MAX. NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)   NDMET    = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER OF TEMPERATURES
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (I*4)   NMET     = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  OUTPUT: (I*4)   IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINP()   = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINH()   = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C
C  OUTPUT: (I*4)   IDOUT    = 1 => INPUT DENSITIES IN CM-3
C                           = 2 => INPUT DENSITIES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 20)
C  OUTPUT: (R*8)   DINE()   = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   DINP()   = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   RATHA()  = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  OUTPUT: (R*8)   RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION:  TEMP/DENS INDEX
C                               2ND DIMENSION:  PARENT INDEX
C  OUTPUT: (R*8)   RATMIA(,)= RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION:  TEMP/DENS INDEX
C                               2ND DIMENSION:  PARENT INDEX
C
C  OUTPUT: (R*8)   ZEFF     = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C
C  OUTPUT: (L*4)   LPSEL    = .TRUE.  => INCLUDE PROTON COLLISIONS
C                           = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  OUTPUT: (L*4)   LZSEL    = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                        PLASMA Z EFFECTIVE'ZEFF'.
C                           = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                        WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                           (ONLY USED IF 'LPSEL=.TRUE.')
C  OUTPUT: (L*4)   LIOSEL   = .TRUE.  => INCLUDE EXCITED STATE IONIS.
C                           = .FALSE. => DO NOT INCLUDE EXC. STATE IONIS
C  OUTPUT: (L*4)   LHSEL    = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                        NEUTRAL HYDROGREN.
C                           = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                        FROM NEUTRAL HYDROGREN.
C
C  OUTPUT: (L*4)   LRSEL    = .TRUE.  => INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  OUTPUT: (L*4)   LISEL    = .TRUE.  => INCLUDE ELECTRON IMPACT
C                                        IONISATION FROM LOWER STAGE ION
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  OUTPUT: (L*4)   LNSEL    = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                        FROM DATAFILE IF AVAILABEL
C                           = .FALSE. => DO NOT INCLUDE PROJECTED
C                                        BUNDLE-N DATA.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C  AUTHOR  : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 17/03/1999
C MODIFIED : Martin O'Mullane  
C            First version.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   I          , J          , K
C-----------------------------------------------------------------------
      REAL*8    ZEFF
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40  
C-----------------------------------------------------------------------
      LOGICAL    LPEND     , LPSEL
C-----------------------------------------------------------------------
      INTEGER    I4UNIT    , LOGIC
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C-----------------------------------------------------------------------
      REAL*8     DEFEXC          
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------

      IF (LPEND) THEN
          WRITE(PIPEOU,*) ONE
      ELSE
          WRITE(PIPEOU,*) ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
      
     
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
          GOTO 999
      ELSE
          LPEND = .FALSE.
      ENDIF

      READ(PIPEIN,'(A)') TITLE
     
      

 999  RETURN
      END
