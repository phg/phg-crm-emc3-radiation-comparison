C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgtran.for,v 1.1 2004/07/06 11:41:22 whitefor Exp $ Date $Date: 2004/07/06 11:41:22 $
C
      SUBROUTINE  BGTRAN( TYP   , C      ,
     &                    AIN   , WVNOU  , WVNOL , WTU   , WTL  ,
     &                    TEIN  , UPSIN  , NV    ,
     &                    x     , y   
     &                  )
      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGTRAN *********************
C
C This subroutine is based on ADAS215's bftran.for - it is modified to
C output the Burgess and Tully x and y vectors.
C
C PURPOSE : TO IMPLEMENT THE TRANSFORMATION DESCRIBED BY
C           BURGESS AND TULLY ( SEE REFERENCE (1)) WHICH
C           IS USED TO ASSESS AND COMPACT DATA.
C
C
C REFERENCES:
C           (1) A.BURGESS AND J.A.TULLY
C           ON THE ANALYSIS OF COLLISION STRENGTHS
C           AND RATE COEFFICIENTS.
C           ASTRON.ASTROPHYS.254,436-453 (1992 )
C
C           (2) SUMMERS.H.P
C               ADAS USERS MANUAL ( 1ST EDITION ).
C
C INPUT :
C       (R*8) Z1          = THE ION CHARGE +1.
C       (C*1) TYP         = BURGESS & TULLY TRANSITION TYPE CODE
C       (R*8) C           = THE ADJUSTABLE PARAMETER ASSOCIATED
C                           WITH THE BURGESS AND TULLY
C                           TRANSFORMATION ( SEE REFERENCE (1)).
C       (R*8) AIN         = THE EINSTEIN `A` CO-EFFICIENT. THIS
C                           IS READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (R*8) WVNOU       = THE WAVENUMBER OF THE UPPER LEVEL.
C                           THIS IS READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (R*8) WVNLO       = THE WAVENUMBER OF THE LOWER LEVEL.
C                           THIS IS READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (R*8) WTU         = THE STATISTICAL WEIGHT OF THE UPPER
C                           LEVEL. THIS IS OBTAINED BY
C
C       (R*8) WTL         = THE STATISTICAL WEIGHT OF THE LOWER
C                           LEVEL. THIS IS OBTAINED BY
C
C       (R*8) TEIN        = THE TEMPERATURE ARRAY (K). THIS
C                           DATA IS READ DIRECTLY FROM THE
C                           ADF04 TYPE FILE.
C       (R*8) UPSIN       = THE ARRAY CONTAING THE EFFECTIVE
C                           COLLISION STRENGTH. THIS DATA IS
C                           READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (I*4) NV          = THE NUMBER OF TEMPERATURE/EFFECTIVE
C                           COLLISION STRENGTH PAIRS FOR A GIVEN
C                           TRANSITION.
C
C OUTPUT:
C
C       (R*8) X           = THE X ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (R*8) Y           = THE Y ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C
C
C       (R*8) E           = THE MATHEMATICAL CONSTANT E.
C       (R*8) CONST       = CLUSTER OF PHYSICAL CONSTANTS.
C                           SEE PAGE 12 OF REFERENCE (2).
C       (R*8) EIJIN       = THE TRANSITION ENERGY (RYD).
C       (R*8) FIJIN       = THE OSCILLATOR STRENGTH.
C       (R*8) ET          = GENERAL CONSTANT.
C       (R*8) C           = THE BURGESS C PARAMETER.
C       (R*8) DY          = DERIVATIVES AT INPUT KNOTS.
C                           SEE XXSPLN FOR FUTHER DETAILS.
C       (R*8) XOUT        = X ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (R*8) YOUT        = Y ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (I*4) NVMAX       = THE MAXIMUM NUMBER OF TEMPERTURES
C                           THAT CAN BE READ.
C       (I*4) IOPT        = GENERAL PARAMETER ASSOCIATED WITH
C                           THE SUBROUTINE XXSPLN.
C       (I*4) I           = GENERAL VARIABLE WHICH IS USED AS
C                           A COUNTER.
C       (LOG) LSETX       = PARAMETER ASSOCIATED WITH THE
C                           SUBROUTINE XXSPLN.
C
C AUTHOR:   H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL.  0141-553-4196
C
C DATE:    04/06/98
C
C
C VERSION  : 1.1                          DATE: 17/03/1999
C MODIFIED : MARTIN O'MULLANE
C            FIRST VERSION.
C
C----------------------------------------------------------------
      INTEGER   NVMAX
C----------------------------------------------------------------
      REAL*8    E , CONST
C----------------------------------------------------------------
      PARAMETER ( NVMAX = 14 , CONST= 8.03231E+09 )
      PARAMETER ( E=2.718281828  )
C----------------------------------------------------------------
      INTEGER   ITYPE    ,       NV      ,      NVN     ,
     &          I        ,       IOPT
      INTEGER   I4UNIT
C----------------------------------------------------------------
      REAL*8    AIN      ,       EIJIN   ,      WVNOU   ,
     &          WVNOL    ,       WTU     ,      WTL     ,
     &          FIJIN    ,       ET      ,
     &          C         
      REAL*8    TEIN(NV)     , UPSIN(NV) 
      REAL*8    X(NVMAX)     , Y(NVMAX)     , DY(NVMAX) 
C----------------------------------------------------------------
      CHARACTER TYP*1
C----------------------------------------------------------------
      LOGICAL   LSETX
C----------------------------------------------------------------


C----------------------------------------------------------------
C  EXECUTE BURGESS TRANSFORMATION DEPENDING ON TRANSITION TYPE
C  SEE EQUATIONS 23 TO 38 IN REFERENCE (1).
C
C  N.B   109737.26 IS THE RYDBER CONSTANT.
C        157890 IS THE CONVERSION BETWEEN RYDBERGS TO ERGS
C        DIVDED BY THE BOLTZMAN CONSTANT ( CGS UNITS ).
C----------------------------------------------------------------

      READ(TYP,'(1I1)')ITYPE
      IF((ITYPE.LE.0).OR.(ITYPE.GT.4)) THEN
         WRITE(I4UNIT(-1),1001)
         WRITE(I4UNIT(-1),1002)
         STOP
      ENDIF
     
      EIJIN = (WVNOU-WVNOL)/109737.26
      IF(EIJIN.GT.0.0D0) THEN
          FIJIN = (1.0/CONST)*WTU*AIN/(EIJIN*EIJIN*WTL)
      ELSE
          FIJIN = 0.0D0
      ENDIF

      DO 10 I = 1,NV
        ET   = 157890.0*EIJIN/TEIN(I)
        IF (ITYPE.EQ.1) THEN
            X(I) = 1-DLOG( C ) / DLOG (1.0/ET + C )
            Y(I) = UPSIN(I) /DLOG(1.0/ET + E )
        ELSEIF (ITYPE.EQ.2) THEN
            X(I) = (1.0/ET )/ ( (1.0/ET) + C )
            Y(I) = UPSIN(I)
        ELSEIF (ITYPE.EQ.3) THEN
            X(I) = (1.0/ET )/ ( (1.0/ET) + C )
            Y(I) = UPSIN(I)* ( (1.0/ET) +1 )
        ENDIF
  10  CONTINUE
      RETURN

C---------------------------------------------------------------
 1001 FORMAT(1X,32('*'),' BGTRAN ERROR ',32('*')//
     &       1X,'TYPE OUT OF RANGE: ')
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C---------------------------------------------------------------

      END
