C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgseed.for,v 1.1 2004/07/06 11:40:53 whitefor Exp $ Date $Date: 2004/07/06 11:40:53 $
C
      SUBROUTINE BGSEED( ISEED )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGSEED *********************
C
C  PURPOSE: Sets iseed to a value based on the time/date. This seed
C           is for use with the LAPACK routine dlarnd.
C
C           The seed array of the random number generator
C           elements must be between 0 and 4095, and ISEED(4)
C           must be odd.
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   ISEED()  = 4 element seed
C                            
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C  AUTHOR  : MARTIN O'MULLANE,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 17/03/1999
C MODIFIED : MARTIN O'MULLANE  
C            FIRST VERSION.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER      ISEED(4)
C-----------------------------------------------------------------------

      iseed(1) = 4004
      iseed(2) = 390
      iseed(3) = 76
      iseed(4) = 33
       
       
      RETURN

      END
