C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgrerr.for,v 1.3 2007/05/18 08:51:03 allan Exp $ Date $Date: 2007/05/18 08:51:03 $
C
      SUBROUTINE BGRERR( IUNT   , 
     &                   NUMEXC , DEFEXC  , GENEXC  , SPFEXC  , 
     &                   NUMREC , DEFREC  , GENREC  , SPFREC  , 
     &                   NUMCXR , DEFCXR  , GENCXR  , SPFCXR  ,
     &                   NUMION , DEFION  , GENION  , SPFION  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGRERR *********************
C
C  PURPOSE: READS ERROR INFORMATION IN ADF04 SPECIFIC ION FILE AND 
C           PASSES BACK 4 ARRAYS ASSOCIATED WITH EACH TRANSITION TPYE
C           OF EXCITATION, RECOMBINATION AND CHARGE EXCHANGE.
C
C           THERE ARE 3 TYPES OF ERROR FOR EACH TYPE, NAMELY
C           DEFAULT,  GENERAL OR SPECIFIC.
C            - GENERAL IS ERROR FROM/TO A PARTICULAR LEVEL (LIMITED TO 5).
C            - SPECIFIC IS THE ERROR TO BE APPLIED TO ONE TRANSITION
C              (LIMITED TO 20). 
C            - DEFAULT IS FOR THE REST (ONLY 1!).
C           ERRORS ARE GIVEN AS A PERCENTAGE INTEPRETTATED AS VALUE +/- 
C           THE PERCENTAGE ERROR. 
C           ERRORS ARE IN FREE-FORMAT MARKED BY LINES IN ADF04 FILE. E.G.
C           FOR EXCITATION AND RECOMBINATION SPECIFICATION
C            
C            C+++ERROR  
C            C Excitation
C            C
C            C    *-*  4.7 
C            C     3-*  3.2
C            C     *- 4  3.2 
C            C     1-*  10.3 
C            C      1- 2  5.4 
C            C      31- 2  7.4
C            C      31- 4  7.4
C            C Recombination
C            C
C            C  * -*   20.0
C            C  *-1  30.0
C            C+++ERROR
C
C           HERE *-* REPRESENTS DEFAULT, 1-* OR *-3 REPRESENTS GENERAL, 3-5 
C           DEFINES ERROR ON SPECIFIC TRANSITION. CAPITALISATION OR SPACING
C           IS NOT IMPORTANT.   
C
C  CALLING PROGRAM: ADAS216
C
C  SUBROUTINE:
C
C  PARAMETER: (I*4)   NDGEN    = MAX. NUMBER OF GENERAL ERRORS ALLOWED
C  PARAMETER: (I*4)   NDSPF    = MAX. NUMBER OF SPECIFIC ERRORS ALLOWED
C
C  INPUT : (I*4)   IUNT     = UNIT NO. OF OPENED ADF04 FILE
C
C  OUTPUT: (R*8)   NUMEXC(3)= NO. OF ERROR TYPES FOR EXCITATION
C                               1ST INDEX:  NO. DEFAULT (0 OR 1)
C                               2ND INDEX:  NO. GENERAL 
C                               3RD INDEX:  NO. SPECIFIC 
C  OUTPUT: (R*8)   DEFEXC   = DEFAULT ERROR
C  OUTPUT: (R*8)   GENEXC(,)= GENERAL ERROR
C                               1ST DIMENSION: SOURCE/DESTINATION LEVEL
C                               2ND DIMENSION: ERROR
C
C  OUTPUT: (R*8)   SPFEXC(,)= SPECIFIC ERROR
C                               1ST DIMENSION: FIRST LEVEL
C                               2ND DIMENSION: SECOND LEVEL
C                               3RD DIMENSION: ERROR
C 
C NOTE : SIMILAR DEFINITIONS FOR RECOMBINATION, CHARGE EXCHANGE
C        AND IONISATION.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSTUC     ADAS      CONVERTS STRING TO UPPERCASE
C          XXRMWS     ADAS      REMOVES BLANKS FROM A STRING
C          BGPERR     ADAS      PARSES ERROR STRING
C
C
C  AUTHOR  : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          
C DATE     : 17/03/1999
C MODIFIED : Martin O'Mullane  
C            First version.
C
C VERSION  : 1.2
C DATE     : 03-01-2006
C MODIFIED : Martin O'Mullane
C              - Increase NDSPF to 2100.
C
C VERSION  : 1.3
C DATE     : 18-05-2007
C MODIFIED : Allan Whiteford
C              - Updated comments as part of subroutine documentation
C                procedure.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDGEN      , NDSPF
C-----------------------------------------------------------------------
      PARAMETER(NDGEN  = 5 , NDSPF = 2100 )
C-----------------------------------------------------------------------
      INTEGER   IUNT       
      INTEGER   L1
      INTEGER   NEXC       , NREC       , NCXR       , NION
C-----------------------------------------------------------------------
      CHARACTER LINE*80    , CLINE*80   , STRG0*8    
C-----------------------------------------------------------------------
      LOGICAL   LEXC       , LREC       , LCXR       , LION
C-----------------------------------------------------------------------
      REAL*8    DEFEXC     , GENEXC(NDGEN,2) , SPFEXC(NDSPF,3)   , 
     &          DEFREC     , GENREC(NDGEN,2) , SPFREC(NDSPF,3)   , 
     &          DEFCXR     , GENCXR(NDGEN,2) , SPFCXR(NDSPF,3)   , 
     &          DEFION     , GENION(NDGEN,2) , SPFION(NDSPF,3)   
C-----------------------------------------------------------------------
      INTEGER   NUMEXC(3)  , NUMREC(3)       , NUMCXR(3) ,  NUMION(3)
C-----------------------------------------------------------------------
      CHARACTER ERRARR((1+NDGEN+NDSPF),3)*70 
C-----------------------------------------------------------------------

       DATA STRG0  /'+++ERROR'/

C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
       
       NEXC = 0
       NREC = 0
       NCXR = 0
       NION = 0

       LEXC = .FALSE.
       LREC = .FALSE. 
       LCXR = .FALSE. 
       LION = .FALSE. 


  10   READ(IUNT,1000,END=50)LINE
       
       CALL XXSTUC(LINE)
       CALL XXRMWS(LINE,CLINE,L1)
       IF (L1.GT.0.AND.INDEX(CLINE,STRG0).GT.0) THEN
          
  20      READ(IUNT,1000,END=50)LINE
          CALL XXSTUC(LINE)
          CALL XXRMWS(LINE,CLINE,L1)
          
          IF (INDEX(CLINE,STRG0).GT.0) THEN
             GOTO 50
          ELSE
             IF (L1.GT.1) THEN
                IF (CLINE(2:3).EQ.'EX') THEN
                   LEXC = .TRUE.
                   LREC = .FALSE.
                   LCXR = .FALSE.
                   LION = .FALSE.
                ELSEIF (CLINE(2:3).EQ.'RE') THEN
                   LEXC = .FALSE.
                   LREC = .TRUE.
                   LCXR = .FALSE.
                   LION = .FALSE.
                ELSEIF (CLINE(2:3).EQ.'CH') THEN
                   LEXC = .FALSE.
                   LREC = .FALSE.
                   LCXR = .TRUE.
                   LION = .FALSE.
                ELSEIF (CLINE(2:3).EQ.'IO') THEN
                   LEXC = .FALSE.
                   LREC = .FALSE.
                   LCXR = .FALSE.
                   LION = .TRUE.
                ELSE
                   IF (LEXC) THEN
                      NEXC = NEXC + 1
                      ERRARR(NEXC,1)=LINE(2:)
                   ELSEIF (LREC) THEN
                      NREC = NREC + 1
                      ERRARR(NREC,2)=LINE(2:)
                   ELSEIF (LCXR) THEN
                      NCXR = NCXR + 1
                      ERRARR(NCXR,3)=LINE(2:)
                   ELSEIF (LION) THEN
                      NION = NION + 1
                      ERRARR(NION,3)=LINE(2:)
                   ENDIF
                ENDIF
             ENDIF
          ENDIF
          GOTO 20
     
       ENDIF
       GOTO 10

  50   CONTINUE
  

C-----------------------------------------------------------------------
C NOW PARSE THE ERROR BLOCKS TO GET DEFAULT, GENERAL AND SPECIFIC 
C TRANSITION ERRORS
C-----------------------------------------------------------------------


        CALL BGPERR(NDGEN   , NDSPF,
     &              NEXC    , ERRARR(1,1)  ,
     &              NUMEXC  , DEFEXC       , GENEXC   , SPFEXC )

        CALL BGPERR(NDGEN   , NDSPF,
     &              NREC    , ERRARR(1,2)  ,
     &              NUMREC  , DEFREC       , GENREC   , SPFREC )


        CALL BGPERR(NDGEN   , NDSPF,
     &              NCXR    , ERRARR(1,3)  ,
     &              NUMCXR  , DEFCXR       , GENCXR   , SPFCXR )
     
        CALL BGPERR(NDGEN   , NDSPF,
     &              NION    , ERRARR(1,3)  ,
     &              NUMION  , DEFION       , GENION   , SPFION )
     



C-----------------------------------------------------------------------
       RETURN

C-----------------------------------------------------------------------
 1000  FORMAT(1A80)
C-----------------------------------------------------------------------

      END
