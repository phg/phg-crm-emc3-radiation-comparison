C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgout0.for,v 1.1 2004/07/06 11:40:24 whitefor Exp $ Date $Date: 2004/07/06 11:40:24 $
C
      SUBROUTINE BGOUT0( NDMET  , IUNIT  , DATE   ,
     &                   DSNC80 , DSNE80 ,
     &                   CELEM  , IZ     , IZ0    , IZ1    , BWNO   ,
     &                   NPL    , NPLR   , NPLI   , BWNOA  ,
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH  , ICNTI  ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA    , 
     &                   WA     , ER     , CPLA   ,
     &                   NV     , TSCEF  , CADAS  ,
     &                   nvmax  , numcom , outcom  
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGOUT0 *********************
C
C  PURPOSE: TO OUTPUT ION SPECIFICATIONS, INDEXED ENERGY LEVELS AND
C           WAVE NUMBERS RELATIVE  TO GROUND TO STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS216
C
C  BASED ON: B8OUT0
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDMET  = MAXIMUM NO. OF METASTABLES ALLOWED
C  INPUT :  (I*4)  IUNIT  = OUTPUT STREAM NUMBER
C  INPUT :  (C*8)  DATE   = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (C*80) DSNC80 = INPUT COPASE    DATA SET NAME
C  INPUT :  (C*80) DSNE80 = INPUT EXPANSION DATA SET NAME
C
C  INPUT :  (C*3)  CELEM  = ELEMENT SYMBOL.
C  INPUT :  (I*4)  IZ     = RECOMBINED ION CHARGE
C  INPUT :  (I*4)  IZ0    =        NUCLEAR CHARGE
C  INPUT :  (I*4)  IZ1    = RECOMBINING ION CHARGE
C                            (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT :  (R*8)  BWNO   = IONISATION POTENTIAL(CM-1) FOR LOWEST PARENT
C  INPUT :  (I*4)  NPL    = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                             BY EXCITED STATE IONISATION IN COPASE
C                             FILE WITH IONISATION POTENTIALS GIVEN
C                             ON THE FIRST DATA LINE
C  INPUT :  (I*4)  NPLR   = NO. OF ACTIVE METASTABLES FOR (Z+1) ION
C  INPUT :  (I*4)  NPLI   = NO. OF ACTIVE METASTABLES FOR (Z-1) ION
C  INPUT :  (R*8)  BWNOA()= IONISATION POTENTIAL (CM-1) FOR PARENTS
C
C  INPUT :  (I*4)  ICNTE  = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTP  = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTR  = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT :  (I*4)  ICNTH  = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C  INPUT :  (I*4)  ICNTI  = NO. OF ELECTRON IMPACT IONISATIONS    INPUT
C
C  INPUT :  (I*4)  IL     = NUMBER OF ENERGY LEVELS
C
C  INPUT :  (I*4)  IA()   = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT :  (I*4)  ISA()  = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT :  (I*4)  ILA()  = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT :  (R*8)  XJA()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  WA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  INPUT : (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           FOR LEVEL 'IA()'
C  INPUT : (C*1)  CPLA()  = INDEX OF PARENTS FOR MEMBERS OF LEVEL LIST
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  TSCEF(,)= INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURE (NOTE: TE=TP=TH)
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C  INPUT : (I*4)  numcom  = number of significant lines in outcom 
C  INPUT : (C*80) outcom()= results of integrity test of the adf04
C                           collision strength data   
C
C          (R*8)  WN2RYD  = PARAMETER =
C                           WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (R*8)  BRYDO   = IONISATION POTENTIAL (RYDBERGS)
C          (R*8)  BWN     = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           WAVE NUMBERS (CM-1).
C          (R*8)  BRYD    = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           RYDBERGS.
C
C          (I*4)  I       = GENERAL USE
C          (I*4)  IP      = GENERAL USE
C
C          (C*1)  CHAR1   = GENERAL USE
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C
C AUTHOR   : Martin O'Mullane,
C            K1/1/43,
C            JET
C
C VERSION  : 1.1                          DATE: 17/03/1999
C MODIFIED : Martin O'Mullane  
C            First version.
C
C-----------------------------------------------------------------------
      REAL*8     WN2RYD
C-----------------------------------------------------------------------
      PARAMETER( WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
      INTEGER    NDMET         , I           , IP         , I4UNIT
      INTEGER    IUNIT         ,
     &           IZ            , IZ0         , IZ1        ,
     &           NPL           , NPLR        , NPLI       ,
     &           ICNTE         , ICNTP       , ICNTR      , ICNTH  ,
     &           ICNTI         ,
     &           IL            , NV          , nvmax      , numcom
C-----------------------------------------------------------------------
      REAL*8     BWNO          , BWN         , BRYDO      , BRYD
C-----------------------------------------------------------------------
      CHARACTER  CELEM*3       , DATE*8      , PRGTYP*1   ,
     &           DSNC80*80     , DSNE80*80   , CADAS*80   , CHAR1*1
      CHARACTER  CTEMP*80
C-----------------------------------------------------------------------
      INTEGER    IA(IL)        , ISA(IL)     , ILA(IL)
C-----------------------------------------------------------------------
      REAL*8     BWNOA(NDMET)
      REAL*8     XJA(IL)       , WA(IL)      , ER(IL)
      REAL*8     TSCEF(14,3)
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18 , CPLA(IL)*1  , outcom(210+200*nvmax)*80
C-----------------------------------------------------------------------


      BRYDO = WN2RYD * BWNO

C-----------------------------------------------------------------------
C GATHER ADAS HEADER AND WRITE IT OUT
C-----------------------------------------------------------------------

      WRITE(IUNIT,1000) CADAS(2:80)

C-----------------------------------------------------------------------
C OUTPUT ION AND ENERGY LEVEL DETAILS
C-----------------------------------------------------------------------

      WRITE(IUNIT,1001) 'ERROR ANALYSIS SYSTEM ','ADAS216',DATE

      WRITE(IUNIT,1002) DSNC80,DSNE80
      WRITE(IUNIT,1003) CELEM , IZ , IZ0 , IZ1 , BWNO , BRYDO
        
      IF (NPL.GT.1) THEN
          WRITE(IUNIT,1009) (BWNOA(I),WN2RYD*BWNOA(I),I=2,NPL)
      ENDIF
      WRITE(IUNIT,1010)
      WRITE(IUNIT,1004)

      DO I=1,IL
         IF(CPLA(I).EQ.'X') THEN
             WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                         ISA(I) , ILA(I)    , XJA(I) , CPLA(I),
     &                         WA(I)  , ER(I)
         ELSE
             IF(CPLA(I).EQ.' ')THEN
                 IP=1
             ELSE
                 CHAR1=CPLA(I)
                 READ(CHAR1,'(I1)') IP
             ENDIF
             BWN  = BWNOA(IP)  - WA(I)
             BRYD = WN2RYD*BWNOA(IP) - ER(I)
             WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                         ISA(I) , ILA(I)  , XJA(I) , CPLA(I) ,
     &                         WA(I)  , ER(I)     ,
     &                         BWN    , BRYD
         ENDIF
      END DO

C-----------------------------------------------------------------------
C OUTPUT ADF04 TEMPERATURES
C-----------------------------------------------------------------------

      WRITE(IUNIT,1006)
      DO  I=1,NV
         WRITE(IUNIT,1007) I , TSCEF(I,1) , TSCEF(I,2) , TSCEF(I,3)
      END DO

C-----------------------------------------------------------------------
C OUTPUT ADF04 INFORMATION
C-----------------------------------------------------------------------

      WRITE(IUNIT,1008) ICNTE , ICNTP , ICNTH , ICNTR , ICNTI ,
     &                  NPLR  , NPLI


C-----------------------------------------------------------------------
C OUTPUT RESULTS OF THE INTEGRITY TEST OF THE ADF04 FILE 
C-----------------------------------------------------------------------

      write(iunit,1100)dsnc80
      
      do i=1,numcom
        call xxwstr(iunit,outcom(i))
      end do

      write(iunit,1110)
C-----------------------------------------------------------------------

 1000 FORMAT(A79)
 1001 FORMAT(27('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,27('*')/)
 1002 FORMAT('INPUT COPASE    FILE NAME: ',A80/
     &        'INPUT EXPANSION FILE NAME: ',A80/)
 1003 FORMAT(1X,'ION',6X,'NUCLEAR',8X,'RECOMBINING',6X,
     &           7('-'),' IONIZATION POTENTIALS',6('-')/
     &           9X,'CHARGE (Z0)',4X,'ION CHARGE (Z1)',
     &        4X,'(wave number <cm-1>)',5X,'(rydbergs)'/
     &        1X,78('-')/
     &        1X,1A3,I2,7X,I3,14X,I3,10X,F15.0,5X,F15.7//)
 1004 FORMAT(55('-'),' ENERGY LEVELS ',55('-')/
     &        2X,'INDEX',4X,'CONFIGURATION',3X,'(2S+1)L(J)','  PRT  ',
     &           5('-'),' ENERGY RELATIVE TO LEVEL 1 ',6('-'),4X,
     &           'ENERGY RELATIVE TO PARENT IONIS. POTEN.'/
     &       45X,2( '(wave number <cm-1>)',5X,'(rydbergs)' ,8X )/
     &        125('-'))
 1005 FORMAT( 1X,I4,2X,A18,2X,'(',I1,')',I1,'(',F4.1,')',
     &        '  {',1A1,'}   ',F15.0,5X,F15.7,8X,F15.0,5X,F15.7 )
 1006 FORMAT(/'-- INPUT COPASE FILE TEMPERATURES: (TE=TP=TH) --'/
     &            1X,'INDEX',4X,'(kelvin)',8X,'(eV)',8X,'(reduced)'/
     &            48('-'))
 1007 FORMAT(2X,I2,1P,3(4X,D10.2))
 1008 FORMAT(/'INPUT COPASE FILE INFORMATION:'/,30('-')/
     &       'NUMBER OF ELECTRON IMPACT TRANSITIONS       =',1X,I4/
     &       'NUMBER OF PROTON   IMPACT TRANSITIONS       =',1X,I4/
     &       'NUMBER OF STATE SELECTIVE CAPTURES BY (Z+1) =',1X,I4/
     &       'NUMBER OF STATE SELECTIVE RECOMBS. BY (Z+1) =',1X,I4/
     &       'NUMBER OF STATE SELECTIVE IONIS. FROM (Z-1) =',1X,I4/
     &       'NUMBER OF ACTIVE METASTABLE STATES OF (Z+1) =',1X,I4/
     &       'NUMBER OF ACTIVE METASTABLE STATES OF (Z-1) =',1X,I4,/)
 1009 FORMAT(42X,F15.0,5X,F15.7)
 1010 FORMAT(/)

 1100 FORMAT(80('-'),/,
     &       'RESULTS OF INTEGRITY TEST FOR ADF04 FILE : ',A,/)
 1110 FORMAT(/,80('-'),/)

C-----------------------------------------------------------------------
      RETURN
      END
