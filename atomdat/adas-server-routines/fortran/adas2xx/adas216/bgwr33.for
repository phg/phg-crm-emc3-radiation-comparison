C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas216/bgwr33.for,v 1.1 2004/07/06 11:41:31 whitefor Exp $ Date $Date: 2004/07/06 11:41:31 $
C
      SUBROUTINE BGWR33( iunit , 
     &                   ILEV  , ITE   , IDEN , pop , data_out
     &                 )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BGWR33 *********************
C
C  PURPOSE:  WRITES UNFORMATTED DATA TO IUNIT
C
C  CALLING PROGRAM: ADAS216
C
C  INPUT : (I*4)   IUNIT    = UNIT NO. OF OPENED UNFORMATTED FILE
C  INPUT : (R*8)   POP      = POPULATION ARRAY
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C  
C  AUTHOR   : Martin O'Mullane,
C             K1/1/43,
C             JET
C
C  VERSION  : 1.1                          
C  DATE     : 17/03/1999
C
C  MODIFIED : Martin O'Mullane  
C             First version.
C
C-----------------------------------------------------------------------
       INTEGER    NDLEV          , NDTEM          , NDDEN
C-----------------------------------------------------------------------
       PARAMETER( NDLEV  = 100   , NDTEM = 20     , NDDEN = 24      )       
C-----------------------------------------------------------------------
       INTEGER    ILEV      , ITE      , IDEN
       INTEGER    IUNIT     , I        , J       , K
C-----------------------------------------------------------------------
       REAL*8     pop(NDLEV,NDTEM,NDDEN)
C-----------------------------------------------------------------------
       REAL*4     data_out(ILEV,ITE,IDEN) 
C-----------------------------------------------------------------------

       do k=1,iden
         do j=1,ite
           do i=1,ilev
             data_out(i,j,k) = pop(i,j,k)
           end do
         end do
       end do

       write(iunit)data_out      

       end
