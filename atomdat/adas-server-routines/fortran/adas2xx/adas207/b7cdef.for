CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7cdef.for,v 1.1 2004/07/06 11:26:18 whitefor Exp $ Data $Date: 2004/07/06 11:26:18 $
CX
      SUBROUTINE B7CDEF( LCFLOG , NCONTR  ,
     &                   RMIN   , RMAX    ,
     &                   ISEL   , CONTR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7CDEF *********************
C
C  PURPOSE: GRAPHICAL OPTION 1: SETTING UP OF DEFAULT CONTOUR VALUES
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (L*4)   LCFLOG   = (DEFAULT CONTOUR VALUES):
C                             .TRUE.  => LOGARITHMIC CONTOUR FORMAT
C                             .FALSE. => LINEAR CONTOUR FORMAT
C  INPUT : (I*4)   NCONTR   = NUMBER OF DEFAULT CONTOUR VALUES REQUIRED
C
C  INPUT : (R*8)   RMIN     = MINIMUM SPECTRUM LINE RATIO
C  INPUT : (R*8)   RMAX     = MAXIMUM SPECTRUM LINE RATIO
C
C  OUTPUT: (I*4)   ISEL     = NUMBER OF DEFAULT CONTOUR VALUES = NCONTR.
C  OUTPUT: (R*8)   CONTR()  = DEFAULT CONTOUR VALUES
C
C          (R*8)   MAXRNG   = PARAMETER = CONTOUR RANGE IF 'RMIN.LE.0'
C                                         (SEE NOTES ON 'RGAP')
C
C          (I*4)   IVAL     = CONTOUR VALUE INDEX NUMBER
C
C          (R*8)   RGAP     = IF LCFLOG=.TRUE.  =>LOG10 CONTOUR INTERVAL
C                             IF LCFLOG=.FALSE. =>CONTOUR INTERVAL
C                             NOTE: IF  'LCFLOG=.TRUE.' AND  'RMIN <= 0'
C                                   THEN 'RGAP' IS RESTRICTED TO A VALUE
C                                   WHICH WILL LEAD  TO  CONTOUR  VALUES
C                                   COVERING A RANGE 'MAXRNG'  I.E. FROM
C                                   '(RMAX/MAXRNG)' -> 'RMAX'.
C
C ROUTINES: NONE
C
C NOTE:
C          CONTOURS: DIVIDE RANGE COVERED BY LINE RATIOS INTO 'NCONTR+1'
C                    INTERVALS. THE MINMUM AND MAXIMUM VALUES THEMSELVES
C                    ARE NOT USED AS CONTOUR VALUES.
C
C          IF (LCFLOG) THEN TAKE EQUALLY SPACED CONTOUR VALUES ON LOG10
C          SCALE.
C
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    17/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     MAXRNG
C-----------------------------------------------------------------------
      PARAMETER( MAXRNG = 1.0D+20 )
C-----------------------------------------------------------------------
      INTEGER    NCONTR           , ISEL
      INTEGER    IVAL
C-----------------------------------------------------------------------
      REAL*8     RMIN             , RMAX
      REAL*8     RGAP
C-----------------------------------------------------------------------
      LOGICAL    LCFLOG
C-----------------------------------------------------------------------
      REAL*8     CONTR(NCONTR)
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
      ISEL=NCONTR
C-----------------------------------------------------------------------
         IF (LCFLOG) THEN
C
               IF (RMIN.GT.0.0D0) THEN
                  RGAP=(RMAX/RMIN)**(1.0/DBLE(NCONTR+1))
                  CONTR(1)=RMIN*RGAP
               ELSE
                  RGAP=MAXRNG**(1.0/DBLE(NCONTR+1))
                  CONTR(1)=(RMAX/MAXRNG)*RGAP
               ENDIF
C
               DO 1 IVAL=2,NCONTR
                  CONTR(IVAL)=CONTR(IVAL-1)*RGAP
    1          CONTINUE
C
         ELSE
            RGAP=(RMAX-RMIN)*(1.0/DBLE(NCONTR+1))
            CONTR(1)=RMIN+RGAP
               DO 2 IVAL=2,NCONTR
                  CONTR(IVAL)=CONTR(IVAL-1)+RGAP
    2          CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
