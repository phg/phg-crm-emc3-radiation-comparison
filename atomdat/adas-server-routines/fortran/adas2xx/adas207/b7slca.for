CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7slca.for,v 1.1 2004/07/06 11:28:11 whitefor Exp $ Data $Date: 2004/07/06 11:28:11 $
CX
      SUBROUTINE B7SLCA( NDLEV  , NDMET  , NMET   ,
     &                   NSTRN  ,
     &                   ISTRN  , KSTRN  , LSTRN  , ASTRN ,
     &                   SLCMET , SLCORD ,
     &                   STACK  ,
     &                   EM
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7SLCA *********************
C
C  PURPOSE: TO CALCULATE SPECTRUM LINE INTENSITY FOR FIXED TEMPERATURE
C           AND DENSITY FOR A GIVEN COMPOSITE ASSEMBLY.
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C
C  INPUT : (I*4)  NSTRN   = NO. OF LINES CHOSEN FOR COMPOSITE ASSEMBLY
C
C  INPUT : (I*4)  ISTRN() = SELECTED TRANSITION INDEXES FOR COMPOSITE
C                             LINE ASSEMBLY
C  INPUT : (I*4)  KSTRN() = ORDINARY/METASTABLE LEVEL INDEX OF UPPER
C                           LEVEL OF COMPOSITE LINE TRANSITION. (SEE
C                                                            'LSTRN()')
C  INPUT : (L*4)  LSTRN() = .TRUE.  => COMPOSITE LINE IS METASTABLE
C                           .FALSE. => COMPOSITE LINE IS ORDINARY
C  INPUT : (R*8)  ASTRN() = COMPOSITE LINE A-VALUE (SEC-1)
C
C  INPUT : (R*8) SLCMET() = METASTABLE LEVEL SPECTRUM-LINE COEFFICIENTS
C                           FOR FIXED TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE INDEX
C  INPUT : (R*8) SLCORD() = ORDINARY LEVEL SPECTRUM-LINE COEFFICIENTS
C                           FOR FIXED TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY LEVEL INDDEX
C
C  INPUT : (R*8) STACK(,) = POPULATION DEPENDENCE
C                           FOR FIXED TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY LEVEL INDEX
C                            2nd DIMENSION: METASTABLE INDEX
C
C  OUTPUT: (R*8)  EM      =  SPECTRUM-LINE INTENSITY FOR COMPOSITE
C                            ASSEMBLY AT GIVEN TEMPERATURE AND DENSITY.
C
C          (I*4)  IU      = COMPOSITE LINE UPPER-LEVEL INDEX
C          (I*4)  I       = COMPOSITE LINE ARRAY INDEX
C          (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C
C          (R*8)  EMPART  =  PARTIAL SUM USED WHEN CALCULATING 'EM'
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    09/10/90
C
C UPDATE:  25/06/91 - CORRECTED ERROR IN ALGREBRA WHICH  AFFECTS  NON-
C                     EQUILIBRIUM CONDITIONS - ADDED VARIABLE 'EMPART'.
C
C-----------------------------------------------------------------------
      INTEGER   NDMET        , NDLEV        , NMET     ,
     &          NSTRN
      INTEGER   I            , IM           , IU
C-----------------------------------------------------------------------
      REAL*8    EM           , EMPART
C-----------------------------------------------------------------------
      INTEGER   ISTRN(NSTRN)
      INTEGER   KSTRN(NSTRN)
C-----------------------------------------------------------------------
      REAL*8    ASTRN(NSTRN)
      REAL*8    SLCMET(NDMET)               , SLCORD(NDLEV)
      REAL*8    STACK (NDLEV,NDMET)
C-----------------------------------------------------------------------
      LOGICAL   LSTRN(NSTRN)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      EM=0.0
         DO 1 I=1,NSTRN
            IU=KSTRN(I)
               IF (LSTRN(I)) THEN
                  EM=EM+(ASTRN(I)*SLCMET(IU))
               ELSE
                  EMPART = 0.0
                     DO 2 IM=1,NMET
                        EMPART = EMPART + ( STACK(IU,IM) * SLCMET(IM) )
    2                CONTINUE
                  EM = EM + ( ASTRN(I) * ( EMPART + SLCORD(IU) ) )
               ENDIF
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
