CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7outg.for,v 1.2 2004/07/06 11:27:49 whitefor Exp $ Data $Date: 2004/07/06 11:27:49 $
CX
      SUBROUTINE B7OUTG( IOPT   , LGHOST , DATE   ,
     &                   NDDEN  , NDTEM  , IGMAX  , ICMAX  ,
     &                   IZ     , TITLED , GTIT1  , DSNINC ,
     &                   ISEL   , CONTR  , IGSEL  ,
     &                   LGRD1  , LDEF1  , LOGINT ,
     &                   XMIN   , XMAX   , YMIN   , YMAX   ,
     &                   NMET   , MAXD   , MAXT   , NSTRN1 , NSTRN2 ,
     &                   XMMULT , DENSA  , TEA    , CSTRN1 , CSTRN2 ,
     &                   STRGM  ,
     &                   RAT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7OUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL.
C
C            OPTION 1 - SPECTRUM LINE RATIO ON TEMP/DEN PLANE CONTOURS
C            OPTION 2 - SPECTRUM LINE RATION VS. TEMPERATURE PLOT
C            OPTION 3 - SPECTRUM LINE RATION VS. DENSITY PLOT
C
C            ELECTRON TEMPERATURES ARE IN KELVIN
C            ELECTRON DENSITIES    ARE IN CM-3
C
C            PLOTS ARE LOG/LOG.
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C
C  INPUT : (I*4)  IOPT    = GRAPHICAL OUTPUT OPTION NUMBER:
C                           1 => CONTOUR PLOT
C                           2 => SPECTRUM-LINE RATIO VS. TEMP. PLOT
C                           2 => SPECTRUM-LINE RATIO VS. DENSITY PLOT
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF INPUT DENSITIES ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF INPUT TEMPERATURES ALLOWED
C  INPUT : (I*4)  IGMAX   = MAXIMUM NUMBER OF TEMPERATURE OR DENSITY
C                           VALUES THAT CAN BE PLOTTED ON A SINGLE
C                           GRAPH. MUST BE <= 20.
C  INPUT : (I*4)  ICMAX   = MAXIMUM NUMBER OF USER ENTERED CONTOUR
C                           VALUES THAT CAN BE PLOTTED ON A SINGLE
C                           GRAPH. MUST BE 20.
C
C  INPUT : (I*4)  IZ      = RECOMBINED ION CHARGE
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C  INPUT : (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*44) DSNINC  = ASSOCIATED COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  ISEL    = OPTION 1 - NUMBER OF OWN CONTOUR VALUES
C                                      ENTERED (=0 IF DEAULT SELECTED)
C                           OPTION 2 - NO. OF DENSITIES SELECTED FOR
C                                      GRAPHING (FROM INPUT LIST).
C                           OPTION 3 - NO. OF TEMPERATURES SELECTED
C                                      FOR GRAPHING (FROM INPUT LIST).
C  INPUT : (R*8)  CONTR() = OPTION 1 - CONTOUR VALUES
C                           OPTION 2 - NOT USED
C                           OPTION 3 - NOT USED
C  INPUT : (I*4)  IGSEL() = OPTION 1 - NOT USED
C                           OPTION 2 - INDEXES OF DENSITIES SELECTED
C                                      FOR GRAPHING.
C                           OPTION 3 - INDEXES OF TEMPERATURES
C                                      SELECTED FOR GRAPHING.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C  INPUT : (L*4)  LOGINT  = OPTION 1:
C                           .TRUE.  => LOGARITHMIC INTERPOLATION
C                           .FALSE. => LINEAR INTERPOLATION
C                           OPTION 2 - NOT USED
C                           OPTION 3 - NOT USED
C
C  INPUT : (R*8)  XMIN    = OPTION 1 - NOT USED
C                           OPTION 2 - LOWER LIMIT FOR X-AXIS OF GRAPH
C                           OPTION 3 - LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX    = OPTION 1 - NOT USED
C                           OPTION 2 - UPPER LIMIT FOR X-AXIS OF GRAPH
C                           OPTION 3 - UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN    = OPTION 1 - NOT USED
C                           OPTION 2 - LOWER LIMIT FOR Y-AXIS OF GRAPH
C                           OPTION 3 - LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX    = OPTION 1 - NOT USED
C                           OPTION 2 - UPPER LIMIT FOR Y-AXIS OF GRAPH
C                           OPTION 3 - UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT ELECTRON DENSITIES
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT ELECTRON TEMPERATURES
C  INPUT : (I*4)  NSTRN1  = NO. OF LINES CHOSEN FOR FIRST COMPOSITE
C                           ASSEMBLY
C  INPUT : (I*4)  NSTRN2  = NO. OF LINES CHOSEN FOR SECOND COMPOSITE
C                           ASSEMBLY
C
C
C  INPUT : (R*8)  XMMULT()= METASTABLE LEVEL SCALING FACTORS
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES (UNITS: CM-3)
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (C*40) CSTRN1()= SELECTED TRANSITION TITLES FOR FIRST
C                           COMPOSITE LINE ASSEMBLY
C  INPUT : (C*40) CSTRN2()= SELECTED TRANSITION TITLES FOR SECOND
C                           COMPOSITE LINE ASSEMBLY
C
C  INPUT : (C*22) STRGM() = METASTABLE LEVEL DESIGNATIONS
C                           DIMENSION: METASTABLE LEVEL INDEX
C
C  INPUT : (R*8)  RAT(,)  = SPECTRUM LINE INTENSITY RATIOS:
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   I, J     = LOOP INCREMENT
C          (I*4)   LOGIC    = USED TO PIPE LOGICAL VALUES
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C NOTES:
C
C       GRAPHICAL OPTION 1:
C
C       CONTOURS ARE BASED ON A LOG10(TEMP.) VS. LOG10(DENSITY) GRID
C
C       THEREFORE CONTOURS ARE PLOTTED USING LINEAR MAPPING AND LOG10
C       TEMPERATURE AND DENSITY VALUES. THEREFORE RE-MAP PLOTTING REGION
C       USING SAME VECTOR SPACE AND EQUIVALENT RANGES.
C
C       IF THIS IS NOT CARRIED OUT THE APPEARANCE OF THE PLOT IS OF
C       EXPONENTIAL CURVES CONNECTING POINTS.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    17/10/90
C
C UPDATE:  14/01/91 - PE BRIDEN - ADAS91 - IF LAST OUTPUT POINT ON GRAPH
C                                          IS OFF SCREEN WRITE LINE  KEY
C                                          AT LAST PLOTTED POSITION.
C                                          BEFORE THE KEY WAS NOT OUTPUT
C                                          - (ADDED VARIABLE 'IPLOT') -
C
C UPDATE:  17/01/91 - PE BRIDEN - ADAS91 - ADDED HEADER INFO. TO OUTPUT
C
C UPDATE:  25/01/91 - PE BRIDEN - ADAS91 - INTRODUCE 'NCTEM' AND 'NCDEN'
C
C UPDATE:  29/01/91 - PE BRIDEN: SET 'CADAS' TO BLANK AT START (VIA DATA
C                                STATEMENT) AND ADDED 'SAVE CADAS'.
C
C UPDATE:  13/08/91 - PE BRIDEN - ADAS91 - CORRECTED ERROR:
C                                          'NCTEM' & 'NCDEN'  BOTH  SET
C                                           EQUAL TO 'NDIM1', THE ARRAY
C                                           BOUND FOR 'SURFAS'. (BEFORE
C                                           THE EQUALLED 'NDTEM & NDDEN'
C                                           BY MISTAKE   -  THIS HAD NO
C                                           EFFECT ON ADAS207 AS NDTEM=
C                                           NDDEN=NDIM1.)
C
C UPDATE:  25/11/91 - PE BRIDEN: MADE FILNAM/PICSAV ARGUMENT LIST
C                                COMPATIBLE WITH GHOST VERSION 8.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  27/03/95 - SP BELLAMY - UNIX PORT
C
C VERSION: 1.1                          DATE: ??
C
C VERSION: 1.2                          DATE: 06-08-96
C MODIFIED: TIM HAMMOND
C           - ADDED A WRITE TO I4UNIT WHICH ALTHOUGH IT SHOULD
C             HAVE NO EFFECT APPEARS TO BE NEEDED TO STOP THE CODE
C             PRODUCING A MASSIVE CORE DUMP ON THE HP.
C
C-----------------------------------------------------------------------
      INTEGER   IOPT     , NDDEN    , NDTEM    , IGMAX    , ICMAX    ,
     &          IZ       , ISEL     , NMET     , MAXD     , MAXT     ,
     &          NSTRN1   , NSTRN2
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LOGINT
C-----------------------------------------------------------------------
      CHARACTER TITLED*3 , GTIT1*40 , DSNINC*80, DATE*8
C-----------------------------------------------------------------------
      INTEGER   IGSEL(IGMAX)
C-----------------------------------------------------------------------
      REAL*8    CONTR(ICMAX)        , XMMULT(NMET)         ,
     &          DENSA(NDDEN)        , TEA(NDTEM)           ,
     &          RAT(NDTEM,NDDEN)
C-----------------------------------------------------------------------
      CHARACTER STRGM(NMET)*22, CSTRN1(NSTRN1)*40, CSTRN2(NSTRN2)*40
C-----------------------------------------------------------------------
      INTEGER    LOGIC     , I4UNIT
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO    , I, J
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(I4UNIT(-1),*)" "

      WRITE( PIPEOU, * ) IOPT
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A8)' ) DATE
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NDDEN
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NDTEM
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) IGMAX
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) ICMAX
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) IZ
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A3)' ) TITLED
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A40)' ) GTIT1
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A80)' ) DSNINC
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) ISEL
      CALL XXFLSH( PIPEOU )

      IF( IOPT.EQ.1 ) THEN
         WRITE( PIPEOU, * ) (CONTR(I),I=1,ISEL)
         CALL XXFLSH( PIPEOU )
      ELSE
         WRITE( PIPEOU, * ) (IGSEL(I),I=1,ISEL)
         CALL XXFLSH( PIPEOU )
      ENDIF

      WRITE(I4UNIT(-1),*)" "
      IF( LDEF1 ) THEN
         WRITE( PIPEOU, * ) ONE
      ELSE
         WRITE( PIPEOU, * ) ZERO
      ENDIF
      CALL XXFLSH( PIPEOU )

      IF( IOPT.EQ.1 ) THEN
         IF( LOGINT ) THEN
            WRITE( PIPEOU, * ) ONE
         ELSE
            WRITE( PIPEOU, * ) ZERO
         ENDIF
         CALL XXFLSH( PIPEOU )
      ELSE
         IF( .NOT.LDEF1 ) THEN
            WRITE( PIPEOU, * ) XMIN
            CALL XXFLSH( PIPEOU )
            WRITE( PIPEOU, * ) XMAX
            CALL XXFLSH( PIPEOU )
            WRITE( PIPEOU, * ) YMIN
            CALL XXFLSH( PIPEOU )
            WRITE( PIPEOU, * ) YMAX
            CALL XXFLSH( PIPEOU )
         ENDIF
      ENDIF

      WRITE( PIPEOU, * ) NMET
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) MAXD
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) MAXT
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NSTRN1
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NSTRN2
      CALL XXFLSH( PIPEOU )

      WRITE(I4UNIT(-1),*)" "
      IF( NMET.GT.0 ) THEN
         WRITE( PIPEOU, * ) (XMMULT(I),I=1,NMET)
         CALL XXFLSH( PIPEOU )
      ENDIF

      IF( MAXD.GT.0 ) THEN
         WRITE( PIPEOU, * ) (DENSA(I),I=1,MAXD)
         CALL XXFLSH( PIPEOU )
      ENDIF

      IF( MAXT.GT.0 ) THEN
         WRITE( PIPEOU, * ) (TEA(I),I=1,MAXT)
         CALL XXFLSH( PIPEOU )
      ENDIF

      IF( NSTRN1.GT.0 ) THEN
         DO 100 I = 1, NSTRN1
            WRITE( PIPEOU, '(A40)' ) CSTRN1(I)
            CALL XXFLSH( PIPEOU )
  100    CONTINUE
      ENDIF

      WRITE(I4UNIT(-1),*)" "
      IF( NSTRN2.GT.0 ) THEN
         DO 200 I = 1, NSTRN2
            WRITE( PIPEOU, '(A40)' ) CSTRN2(I)
            CALL XXFLSH( PIPEOU )
  200    CONTINUE
      ENDIF

      IF( NMET.GT.0 ) THEN
         DO 300 I = 1, NMET
            WRITE( PIPEOU, '(A22)' ) STRGM(I)
            CALL XXFLSH( PIPEOU )
  300    CONTINUE
      ENDIF

      DO 500 J = 1, MAXD
         DO 400 I = 1, MAXT
            WRITE( PIPEOU, * ) RAT( I, J )
            CALL XXFLSH( PIPEOU )
  400    CONTINUE
  500 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
