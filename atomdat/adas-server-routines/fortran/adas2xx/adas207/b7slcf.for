CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7slcf.for,v 1.1 2004/07/06 11:28:15 whitefor Exp $ Data $Date: 2004/07/06 11:28:15 $
CX
      SUBROUTINE B7SLCF( NDMET  , NDLEV  , NDTEM  , NDDEN ,
     &                   NMET   , NORD   , MAXT   , MAXD  ,
     &                   XMMULT , RATIA  , RATHA  , DENSA ,
     &                            ICNTR  , ICNTH  ,
     &                   STCKM  , STVR   , STVH   ,
     &                   STVRM  , STVHM  ,
     &                   SLCMET , SLCORD
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B7SLCF *******************
C
C  PURPOSE: TO SET UP COEFFICIENTS FOR CALCULATING SPECTRUM-LINE
C           INTENSITIES. (THESE ARE FUNCTIONS OF LEVEL, TEMP. & DENSITY)
C
C
C  CALLING PROGRAM: ADAS207
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4) NDMET    = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4) NDLEV    = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4) NDTEM    = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4) NDDEN    = MAXIMUM NUMBER OF DENSITIES ALLOWED
C
C  INPUT : (I*4) NMET     = NUMBER OF METASTABLES LEVELS: 1<=NMET<=NDMET
C  INPUT : (I*4) NORD     = NUMBER OF ORDINARY LEVELS
C  INPUT : (I*4) MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C  INPUT : (I*4) MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C
C  INPUT : (R*8) XMMULT() = METASTABLE LEVELS: SCALING FACTORS
C  INPUT : (R*8) RATHA()  = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  INPUT : (R*8) RATIA()  = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C  INPUT : (R*8) DENSA()  = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (I*4) ICNTR    = NUMBER OF FREE ELECTRON RECOMBINATIONS
C  INPUT : (I*4) ICNTH    = NUMBER OF NEUTRAL HYDROGEN CHARGE EXCHANGES
C
C  INPUT : (R*8) STCKM(,,)= METASTABLE POPULATIONS STACK
C                            1st DIMENSION: METASTABLE INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVR(,,) = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                            1st DIMENSION: ORDINARY LEVEL INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVH(,,) =  CHARGE EXCHANGE COEFFICIENTS
C                            1st DIMENSION: ORDINARY LEVEL INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C
C  INPUT : (R*8) STVRM(,,)= METASTABLE FREE ELECTRON RECOMBINATION
C                           COEFFICIENTS.
C                            1st DIMENSION: METASTABLE INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVHM(,,)= METASTABLE CHARGE EXCHANGE COEFFICIENTS
C                            1st DIMENSION: METASTABLE INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C
C  OUTPUT: (R*8) SLCMET(,,)= METASTABLE LEVEL SPECTRUM-LINE COEFFICIENTS
C                            1st DIMENSION: METASTABLE INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C  OUTPUT: (R*8) SLCORD(,,)= ORDINARY LEVEL SPECTRUM-LINE COEFFICIENTS
C                            1st DIMENSION: ORDINARY LEVEL INDDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C
C          (I*4) IM       = METASTABLE LEVEL ARRAY INDEX
C          (I*4) IO       = ORDINARY LEVEL   ARRAY INDEX
C          (I*4) IT       = TEMPERATURE      ARRAY INDEX
C          (I*4) IN       = DENSITY          ARRAY INDEX
C
C          (R*8) DRIVAL   = DENSITY * 'RATIA()' (FOR GIVEN DENSITY)
C          (R*8) DRHVAL   = DENSITY * 'RATIA()' * 'RATHA()'
C                                               (FOR GIVEN DENSITY)
C
C          (L*4) LCNTR    = .TRUE.  => 'ICNTR' > 0
C                           .FALSE. => 'ICNTR' = 0
C          (L*4) LCNTH    = .TRUE.  => 'ICNTH' > 0
C                           .FALSE. => 'ICNTH' = 0
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    09/10/90
C
C UPDATE:  25/06/91 - CORRECTED ERROR IN ALGEBRA WHICH  AFFECTS  NON-
C                     EQUILIBRIUM CONDITIONS.
C
C-----------------------------------------------------------------------
      INTEGER NDMET  , NDLEV  , NDTEM  , NDDEN
      INTEGER NMET   , NORD   , MAXT   , MAXD
      INTEGER IM     , IO     , IT     , IN
      INTEGER ICNTR  , ICNTH
C-----------------------------------------------------------------------
      REAL*8  DRIVAL , DRHVAL
C-----------------------------------------------------------------------
      LOGICAL LCNTR  , LCNTH
C-----------------------------------------------------------------------
      REAL*8  XMMULT(NDMET)
      REAL*8  RATIA (NDDEN)             , RATHA (NDDEN)             ,
     &        DENSA (NDDEN)
      REAL*8  SLCMET(NDMET,NDTEM,NDDEN) , SLCORD(NDLEV,NDTEM,NDDEN)
      REAL*8  STCKM (NDMET,NDTEM,NDDEN)
      REAL*8  STVHM (NDMET,NDTEM,NDDEN) , STVRM (NDMET,NDDEN,NDDEN)
      REAL*8  STVH  (NDLEV,NDTEM,NDDEN) , STVR  (NDLEV,NDTEM,NDDEN)
C
C-----------------------------------------------------------------------
C
         IF (ICNTR.GT.0) THEN
            LCNTR=.TRUE.
         ELSE
            LCNTR=.FALSE.
         ENDIF
C-----------------------------------------------------------------------
         IF (ICNTH.GT.0) THEN
            LCNTH=.TRUE.
         ELSE
            LCNTH=.FALSE.
         ENDIF
C-----------------------------------------------------------------------
         DO 1 IN=1,MAXD
C-----------------------------------------------------------------------
            IF (LCNTR) THEN
               DRIVAL = DENSA(IN)*RATIA(IN)
            ELSE
               DRIVAL = 0.0D0
            ENDIF
C-----------------------------------------------------------------------
            IF (LCNTH) THEN
               DRHVAL = DENSA(IN)*RATIA(IN)*RATHA(IN)
            ELSE
               DRHVAL = 0.0D0
            ENDIF
C-----------------------------------------------------------------------
               DO 2 IT=1,MAXT
C
C-----------------------------------------------------------------------
C METASTABLE LEVELS
C-----------------------------------------------------------------------
C
                  DO 3 IM=1,NMET
                     SLCMET(IM,IT,IN)=STCKM(IM,IT,IN)
                        IF (IM.NE.1) THEN
                           SLCMET(IM,IT,IN)=        XMMULT(IM) *
     &                                      (     SLCMET(IM,IT,IN)
     &                                      + (DRIVAL*STVRM(IM,IT,IN))
     &                                      + (DRHVAL*STVHM(IM,IT,IN)) )
                        ENDIF
    3             CONTINUE
C
C-----------------------------------------------------------------------
C ORDINARY LEVELS
C-----------------------------------------------------------------------
C
                  DO 4 IO=1,NORD
                     SLCORD(IO,IT,IN)=(DRIVAL*STVR(IO,IT,IN))+
     &                                (DRHVAL*STVH(IO,IT,IN))
    4             CONTINUE
C-----------------------------------------------------------------------
    2          CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
