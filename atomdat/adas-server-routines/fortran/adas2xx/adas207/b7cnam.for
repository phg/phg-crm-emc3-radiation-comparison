CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7cnam.for,v 1.1 2004/07/06 11:26:24 whitefor Exp $ Data $Date: 2004/07/06 11:26:24 $
CX
       SUBROUTINE B7CNAM( NDLEV  , STRGA ,
     &                    ICNTE  , IE1A  , IE2A ,
     &                    NSTRN  , ISTRN ,
     &                    CSTRN
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7CNAM *********************
C
C  PURPOSE: TO SET UP THE TITLES FOR COMPOSITE LINE ASSEMBLY TRANSITIONS
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX.
C  INPUT : (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX.
C
C  INPUT : (I*4)  NSTRN   = NO. OF LINES CHOSEN FOR COMPOSITE ASSEMBLY
C  INPUT : (I*4)  ISTRN() = SELECTED TRANSITION INDEXES FOR COMPOSITE
C                           LINE ASSEMBLY
C
C  OUTPUT: (C*40) CSTRN() = SELECTED TRANSITION TITLE FOR COMPOSITE
C                           LINE ASSEMBLY
C
C          (I*4)  I       = GENERAL USE.
C          (I*4)  J       = GENERAL USE.
C          (I*4)  ILEV(1) = COMPOSITE LINE TRANSITION: LOWER LEVEL INDEX
C          (I*4)  ILEV(2) = COMPOSITE LINE TRANSITION: UPPER LEVEL INDEX
C
C          (C*18) CLEV(1) = COMPOSITE LINE TRANSITION: LOWER LEVEL TITLE
C                           (BRACKETS REMOVED FROM AROUND QUANTUM NOS.)
C          (C*18) CLEV(2) = COMPOSITE LINE TRANSITION: UPPER LEVEL TITLE
C                           (BRACKETS REMOVED FROM AROUND QUANTUM NOS.)
C          (C*22) C22     = GENERAL USE 22 BYTE CHARACTER STRING
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   NDLEV           , ICNTE           , NSTRN
      INTEGER   I               , J
C-----------------------------------------------------------------------
      INTEGER   ILEV(2)
      INTEGER   ISTRN(NSTRN)    , IE1A(ICNTE)     , IE2A(ICNTE)
C-----------------------------------------------------------------------
      CHARACTER C22*22          , CLEV(2)*18
      CHARACTER STRGA(NDLEV)*22 , CSTRN(NSTRN)*40
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SETUP COMPOSITE LINE TITLES (REMOVING BRACKETS AROUND QUANTUM NUMBERS)
C-----------------------------------------------------------------------
C
         DO 1 I=1,NSTRN
            ILEV(1)=IE1A(ISTRN(I))
            ILEV(2)=IE2A(ISTRN(I))
               DO 2 J=1,2
                  C22=STRGA(ILEV(J))
                  CLEV(J)=C22(1:12)//C22(14:14)//C22(16:16)//C22(18:21)
    2          CONTINUE
            CSTRN(I)=CLEV(2)//' - '//CLEV(1)//' '
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
