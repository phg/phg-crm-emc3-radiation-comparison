CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/adas207.for,v 1.10 2004/07/06 10:17:54 whitefor Exp $ Data $Date: 2004/07/06 10:17:54 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: ADAS207 ********************
C
C  ORIGINAL NAME: SPNFPRG1
C
C               THIS PROGRAM WAS DEVELOPED FROM THE ORIGINAL PROGRAM IN:
C               'JETSHP.COPASE.FORT(SPNFPRG1)'. IT NO LONGER CARRIES OUT
C               THE INFLUX ANALYSIS.
C
C  VERSION:  1.0
C
C  PURPOSE:  TO INTEROGATE THE CONTOUR PASSING FILE OUTPUT FROM EITHER:
C            'SPFPOPN' OR 'SPFPOPP'.
C
C            THE CONTOUR PASSING  FILE  CONTAINS  POPULATION  DATA  FOR
C            DIAGNOSTIC USE.
C
C            IT ALLOWS YOU TO PLOT THE FOLLOWING:
C
C            1) DIAGNOSTIC CONTOURS
C            2) LINE RATIO - TEMPERATURE PLOT
C            3) LINE RATIO - DENSITY PLOT
C
C
C  DATA:    THE DATA IS CONTAINED IN A SEQUENTIAL DATASETS, USUALLY
C           CALLED '<USERID>.CONTOUR.PASS'.
C
C           THIS CONTAINS THE FILE NAME OF THE COPASE DATASET USED TO
C           CONSTRUCT THE CONTOUR PASSING FILE.
C
C           THE UNITS USED IN THE ASSOCIATED COPASE DATA FILE ARE:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           RATE COEFFICIENTS   : CM3 SEC-1
C
C
C  PROGRAM:
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C          (I*4)  NDDEN   = PARAMETER = MAX. NUMBER OF DENSITIES ALLOWED
C          (I*4)  NDMET   = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C          (I*4)  NDLNE   = PARAMETER = MAX. NUMBER OF LINES ALLOWED PER
C                                       COMPOSITE ASSEMBLY.
C          (I*4)  NCONTR  = PARAMETER = NUMBER OF DEFAULT CONTOUR VALUES
C                                       GENERATED IF REQUIRED.
C                                       (MUST NOT EXCEED 'ICMAX')
C
C          (I*4)  ICMAX   = PARAMETER =
C                           MAXIMUM NUMBER OF USER ENTERED CONTOUR
C                           VALUES THAT CAN BE PLOTTED ON A SINGLE
C                           GRAPH. MUST BE 20.
C          (I*4)  IGMAX   = PARAMETER =
C                           MAXIMUM NUMBER OF TEMPERATURE OR DENSITY
C                           VALUES THAT CAN BE PLOTTED ON A SINGLE
C                           GRAPH. MUST BE <= 20.
C
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT09  = PARAMETER = INPUT UNIT- COPASE DATA SET
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT- CONTOUR PASSING FILE
C
C          (R*4)  D1E30   = PARAMETER = 1.0D+30
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IOPT    = GRAPHICAL OPTION NUMBER:
C                           1 => CONTOUR PLOT
C                           2 => SPECTRUM-LINE RATIO VS TEMP. PLOT
C                           3 => SPECTRUM-LINE RATIO VS DENSITY PLOT
C          (I*4)  ISEL    = OPTION 1 - NUMBER OF OWN/DEFAULT CONTOUR
C                                      VALUES ENTERED. (DEFAULT=NCONTR)
C                           OPTION 2 - NO. OF DENSITIES SELECTED FOR
C                                      GRAPHING (FROM INPUT LIST).
C                           OPTION 3 - NO. OF TEMPERATURES SELECTED
C                                      FOR GRAPHING (FROM INPUT LIST).
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C          (I*4)  IZ      =  RECOMBINED ION CHARGE
C          (I*4)  IZ0     =         NUCLEAR CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C          (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C          (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C          (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C          (I*4)  IN      = DENSITY INDEX FOR ARRAY USE
C          (I*4)  IT      = TEMPERATURE INDEX FOR ARRAY USE
C          (I*4)  NSTRN1  = NO. OF LINES CHOSEN FOR FIRST COMPOSITE
C                           ASSEMBLY
C          (I*4)  NSTRN2  = NO. OF LINES CHOSEN FOR SECOND COMPOSITE
C                           ASSEMBLY
C
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  RMIN    = MINIMUM SPECTRUM-LINE INTENSITY RATIO VALUE
C          (R*8)  RMAX    = MAXIMUM SPECTRUM-LINE INTENSITY RATIO VALUE
C          (R*8)  XL1     = OPTION 1 - NOT USED
C                           OPTION 2 - LOWER LIMIT FOR X-AXIS OF GRAPH
C                           OPTION 3 - LOWER LIMIT FOR X-AXIS OF GRAPH
C          (R*8)  XU1     = OPTION 1 - NOT USED
C                           OPTION 2 - UPPER LIMIT FOR X-AXIS OF GRAPH
C                           OPTION 3 - UPPER LIMIT FOR X-AXIS OF GRAPH
C          (R*8)  YL1     = OPTION 1 - NOT USED
C                           OPTION 2 - LOWER LIMIT FOR Y-AXIS OF GRAPH
C                           OPTION 3 - LOWER LIMIT FOR Y-AXIS OF GRAPH
C          (R*8)  YU1     = OPTION 1 - NOT USED
C                           OPTION 2 - UPPER LIMIT FOR Y-AXIS OF GRAPH
C                           OPTION 3 - UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                         = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SETS
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SETS
C          (L*4)  OPEN9   = .TRUE.  => FILE ALLOCATED TO UNIT 9.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 9.
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LEXIST  = .TRUE.  => STATED FILE ALREADY EXISTS
C                         = .FALSE. => STATED FILE DOES NOT EXISTS
C          (L*4)  LEQUIL  = .TRUE.  =>     EQUILIBRIUM CONDITIONS
C                         = .FALSE. => NON-EQUILIBRIUM CONDITIONS
C          (L*4)  LGEND   = .TRUE.  => END GRAPHICAL ANALYSIS OF
C                                      CURRENT DATA
C                         = .FALSE. => CONTINUE GRAPHICAL ANALYSIS OF
C                                      CURRENT DATA
C          (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LOGINT  = OPTION 1:
C                           .TRUE.  => LOGARITHMIC INTERPOLATION
C                           .FALSE. => LINEAR INTERPOLATION
C                           OPTION 2 - NOT USED
C                           OPTION 3 - NOT USED
C          (L*4)  LCFLOG  = OPTION 1 (DEFAULT CONTOUR VALUES):
C                           .TRUE.  => LOGARITHMIC CONTOUR SPACING
C                           .FALSE. => LINEAR CONTOUR SPACING
C                           OPTION 2 - NOT USED
C                           OPTION 3 - NOT USED
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                         = 'NO ' => CONTINUE PROGRAM EXECUTION.
C          (C*3)  TITLED  = ELEMENT SYMBOL.
C          (C*8)  DATE    = CURRENT DATE AS 'DD.MM.YY'
C          (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C          (C*44) DSNPAS  = INPUT CONTOUR PASSING FILE DSN (SEQUENTIAL)
C          (C*44) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C          (C*44) DSN44   = 44 BYTE CHARACTER STRING  FOR  TEMPORARY
C                           STORAGE OF DATA SET NAMES FOR DYNAMIC ALLOC.
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IGSEL() = OPTION 1 - NOT USED
C                           OPTION 2 - INDEXES OF DENSITIES SELECTED
C                                      FOR GRAPHING.
C                           OPTION 3 - INDEXES OF TEMPERATURES
C                                      SELECTED FOR GRAPHING.
C          (I*4)  ISTRN1()= SELECTED TRANSITION INDEXES FOR FIRST
C                           COMPOSITE LINE ASSEMBLY
C          (I*4)  ISTRN2()= SELECTED TRANSITION INDEXES FOR SECOND
C                           COMPOSITE LINE ASSEMBLY
C          (I*4)  KSTRN1()= ORDINARY/METASTABLE LEVEL INDEX OF UPPER
C                           LEVEL OF COMPOSITE LINE TRANSITION FROM
C                           FIRST COMPOSITE ASSEMBLY. (SEE 'LSTRN1()')
C          (I*4)  KSTRN2()= ORDINARY/METASTABLE LEVEL INDEX OF UPPER
C                           LEVEL OF COMPOSITE LINE TRANSITION FROM
C                           SECOND COMPOSITE ASSEMBLY. (SEE 'LSTRN2()')
C
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C          (R*8)  WATRAN()= TRANSITION ENERGY (CM-1)
C          (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C          (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C          (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C          (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C          (R*8)  AVAL()  = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  XMMULT()= METASTABLE LEVELS: SCALING FACTORS
C          (R*8)  CONTR() = OPTION 1 - USER/DEFAULT CONTOUR VALUES
C                           OPTION 2 - NOT USED
C                           OPTION 3 - NOT USED
C          (R*8)  ASTRN1()= COMPOSITE LINE A-VALUE (SEC-1) FOR FIRST
C                           COMPOSITE LINE ASSEMBLY
C          (R*8)  ASTRN2()= COMPOSITE LINE A-VALUE (SEC-1) FOR SECOND
C                           COMPOSITE LINE ASSEMBLY
C          (R*8)  EM1(,)  = SPECTRUM-LINE INTENSITY FOR FIRST COMPOSITE
C                           ASSEMBLY AT GIVEN TEMPERATURE AND DENSITY.
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C          (R*8)  EM2(,)  = SPECTRUM-LINE INTENSITY FOR SECOND COMPOSITE
C                           ASSEMBLY AT GIVEN TEMPERATURE AND DENSITY.
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C          (R*8)  RAT(,)  = SPECTRUM LINE INTENSITY RATIOS:
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C                           ( = EM1(,) / EM2(,) )
C          (R*8) SLCMET(,,)= METASTABLE LEVEL SPECTRUM-LINE COEFFICIENTS
C                            1st DIMENSION: METASTABLE INDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C          (R*8) SLCORD(,,)= ORDINARY LEVEL SPECTRUM-LINE COEFFICIENTS
C                            1st DIMENSION: ORDINARY LEVEL INDDEX
C                            2nd DIMENSION: TEMPERATURE INDEX
C                            3rd DIMENSION: DENSITY INDEX
C          (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVR(,,)  = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVH(,,)  =  CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVRM(,,) = METASTABLE FREE ELECTRON RECOMBINATION
C                            COEFFICIENTS.
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVHM(,,) = METASTABLE CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STACK(,,,)= POPULATION DEPENDENCE
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C
C
C          (C*1)  MTRGA() = 'M' => METASTABLE LEVEL
C                           ' ' => ORDINARY   LEVEL
C                           DIMENSION: LEVEL INDEX
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*22) STRGA() = LEVEL DESIGNATIONS
C          (C*22) STRGM() = METASTABLE LEVEL DESIGNATIONS
C          (C*40) CSTRN1()= SELECTED TRANSITION TITLES FOR FIRST
C                           COMPOSITE LINE ASSEMBLY
C          (C*40) CSTRN2()= SELECTED TRANSITION TITLES FOR SECOND
C                           COMPOSITE LINE ASSEMBLY
C
C          (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                      'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C          (L*4)  LSTRN1()= FIRST COMPOSITE LINE ASSEMBLY:
C                           .TRUE.  => COMPOSITE LINE IS METASTABLE
C                           .FALSE. => COMPOSITE LINE IS ORDINARY
C          (L*4)  LSTRN2()= SECOND COMPOSITE LINE ASSEMBLY:
C                           .TRUE.  => COMPOSITE LINE IS METASTABLE
C                           .FALSE. => COMPOSITE LINE IS ORDINARY
C
C
CX         (L*4)  LPAPER  = .TRUE.  => OUTPUT DATA TO TEXT OUTPUT FILE.
CX                          .FALSE. => NO OUTPUT OF CURRENT DATA TO
CX                                     CONTOUR TEXT OUTPUT FILE.
CX         (L*4)  LNEWPA    .TRUE.  => NEW TEXT OUTPUT FILE OR
CX                                     REPLACEMENT OF EXISTING FILE
CX                                     REQUIRED.
CX                          .FALSE. => ALLOW APPEND ON EXISTING OPEN
CX                                     TEXT FILE.
CX         (C*80) DSNPAP  = INPUT TEXT OUTPUT DATA SET NAME
CX         (L*4)  OPEN7   = .TRUE.  => FILE ALLOCATED TO UNIT 7.
CX                        = .FALSE. => NO FILE ALLOCATED TO UNIT 7.
C
C
C NOTE:
C          STREAM  9: INPUT  - COPASE FILE USED IN CONSTRUCTING THE
C          ('IUNT09')          CONTOUR PASSING FILE
C
C          STREAM 10: INPUT  - CONTOUR PASSING FILE
C          ('IUNT10')
C
C          STREAM 17: OUTPUT - MAIN OUTPUT TABLES
C          ('IUNT07')
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B7SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          B7DATA     ADAS      GATHERS DATA FROM INPUT CONTOUR FILE
C          B7DATC     ADAS      GATHERS DATA FROM ASSOCIATED COPASE FILE
C          B7CHKM     ADAS      CHECKS IF TRANSITION EXIST TO METASTABLE
C          B7SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          B7OUT0     ADAS      OUTPUT ION SPEC. ETC. TO OUTPUT STREAM
C          B7ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          B7SLCF     ADAS      SET UP COEFFTS FOR LINE INTENSITY CALCS
C          B7CNAM     ADAS      SET UP TITLES FOR COMPOSITE LINES
C          B7CTYP     ADAS      IDENTIFY ORD AND MET COMPOSITE LINES
C          B7SLCA     ADAS      CALCULATE SPECTRUM LINE INTENSITIES
C          B7LRAT     ADAS      CALCULATE SPECTRUM LINE RATIOS
C          B7SETR     ADAS      SET UP CONTOUR RANGE IN ISPF FUNC. POOL
C          B7SPF1     ADAS      GATHERS GRAPHICAL PARMS VIA ISPF PANELS
C          B7CDEF     ADAS      SETS UP DEFAULT CONTOUR VALUES
C          B7OUTG     ADAS      OUTPUT GRAPHS USING GHOST80
C          B7OUT1     ADAS      OUTPUT MAIN RESULTS TO OUTPUT STREAM
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXDSN2     ADAS      TO CHECK IF SEQUENTIAL DATA SET EXISTS
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    17/10/90
C
C UPDATE:  06/12/90 - PE BRIDEN - 'XXDSN1' RENAMED 'XXDSN2'
C
C UPDATE:  12/12/90 - PE BRIDEN: ADAS91 -'DSNPAS' INCREASED TO 44 BYTES.
C                                       - REPLACED 'XXOPEN' WITH FORTRAN
C                                        'OPEN' STATEMENT (AS 'DSNPAS' &
C                                        'DSNINC' IN APPROPRIATE FORM).
C
C UPDATE:  17/01/91 - PE BRIDEN: ADAS91 - RENAMED SUBROUTINES AS FOLLOWS
C                                         'B7WR7A' -> 'B7OUT0'
C                                         'B7WR7B' -> 'B7OUT1'
C
C UPDATE:  20/02/91 - P.E.BRIDEN: ADAS91- REMOVED IBM 'ERRSET' ROUTINE
C
C UPDATE:  22/10/92 - PE BRIDEN: ADAS91 - CHANGED NDLEV FROM  30 ->  110
C                                         CHANGED NDTRN FROM 300 -> 2100
C                                         CHANGED NDTEM FROM  20 ->   12
C                                         CHANGED NDDEN FROM  20 ->   12
C                                         CHANGED NDMET FROM   5 ->    4
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  28/01/94 - PE BRIDEN - ADAS91: CHANGED CSTRGA C*12 -> C*18
C
C UPDATE:  08/03/95 - S.P.BELLAMY - UNIX PORT
C 
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 30-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SOFTWARE CONTROL
C
C VERSION: 1.2                          DATE: 07-04-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED NDTEM FROM 12 -> 20
C                 CHANGED NDDEN FROM 12 -> 20
C                 CHANGED NDMET FROM  4 ->  5
C
C VERSION: 1.3                          DATE: 03-05-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PLACED STACK IN COMMON IN LINE WITH CHANGES MADE
C                 BY PAUL BRIDEN TO JET CODES.
C
C VERSION: 1.4                          DATE: 24-05-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED NDLNE FROM 10 -> 20
C
C VERSION: 1.5                          DATE: 12-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED DENSA AND NDDEN TO ARGUMENT LIST FOR B7ISPF
C                 FOR EARLIER PASSING TO IDL
C
C VERSION: 1.6                          DATE: 04-06-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED MENU BUTTON CODE
C
C VERSION: 1.7                                  DATE: 06-08-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED OUTPUT UNIT IUNT07 FROM 7 TO 17 TO AVOID
C                 CLASHING WITH STANDARD ERROR STREAM 7 ON HP
C                 WORKSTATIONS.
C
C VERSION: 1.8                                  DATE: 14-11-01
C MODIFIED: RICHARD MARTIN
C		- INCREASED NDLEV TO 130 AND NDTRN TO 4100
C		- ADDED WATRAN TO B7DATC CALL.
C		- ADDED WATRAN, STACK, STCKM, AVALUE AND OTHER PARAMETERS
C		  TO CALL TO B7ISPF.FOR
C
C VERSION: 1.9                                  DATE: 19-11-02
C MODIFIED: Martin O'Mullane & Allan Whiteford
C		- Increased NDLEV to 300, and NDTRN to 8000
C                 to accommodate xenon adf04 files.
C
C
C VERSION: 1.10                                 DATE: 02-05-2003
C MODIFIED: Martin O'Mullane
C		- Use xxdata_04 to read adf04 file.
C               - b7datc rewritten to just do consistency test
C                 between contour and adf04 file. bxttyp is used
C                 to indentify transition types.
C		- Increase NDLEV to 1000 and NDTRN to 15000.
C               - New xxx_C variables refer to contour file values.
C               - Remove the superfluous CADAS207 common block, a
C                 relic from the IBM days.
C
C-----------------------------------------------------------------------
      INTEGER   NDLEV  , NDTRN  , NDTEM  , NDDEN  , NDMET  , NDLNE
      INTEGER   NVMAX  , NDQDN
      INTEGER   NCONTR
      INTEGER   ICMAX  , IGMAX
      INTEGER   IUNT07 , IUNT09 , IUNT10 , ISTOP  , PIPEIN
C-----------------------------------------------------------------------
      REAL*8    D1E30
C-----------------------------------------------------------------------
      PARAMETER ( NDLEV =1000, NDTRN=15000, NDTEM= 20, NDDEN=20, 
     &            NDMET =5   , NDLNE=20   , PIPEIN= 5)
      PARAMETER ( NVMAX=14  , NDQDN=6   )
      PARAMETER ( NCONTR= 9 )
      PARAMETER ( ICMAX =20 , IGMAX= 10 )
      PARAMETER ( IUNT07=17 , IUNT09= 9, IUNT10=10 )
C-----------------------------------------------------------------------
      PARAMETER ( D1E30  = 1.0D+30   )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   IPAN      , IL        ,
     &          IZ        , IZ0       , IZ1    ,
     &          NMET      , NORD      ,
     &          MAXT      , MAXD      ,
     &          ICNTE     , ICNTR     , ICNTH
      INTEGER   IOPT      , ISEL      ,
     &          NSTRN1    , NSTRN2    ,
     &          IN        , IT        , I
      INTEGER   IZ_C      , IZ0_C     , IZ1_C   ,IL_C 
      INTEGER   ITRAN     , MAXLEV    , NV
C-----------------------------------------------------------------------
      REAL*8    BWNO      , RMIN      , RMAX    , BWNO_C    ,
     &          XL1       , XU1       , YL1     , YU1
C-----------------------------------------------------------------------
      CHARACTER REP*3     , TITLED*3  , DATE*8  ,
     &          GTIT1*40  , TITLED_C*3,
     &          DSNPAS*80 , DSNINC*80 , DSN44*80 , DSNPAP*80
C-----------------------------------------------------------------------
      LOGICAL   OPEN7     , OPEN9     , OPEN10    , LGHOST  , LPEND   ,
     &          LEXIST    , LEQUIL    , LGEND     ,
     &          LOGINT    , LCFLOG    , LGRD1     , LDEF1   ,
     &          LPAPER    , LNEWPA
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     ,
     &          IA(NDLEV)        ,
     &          ISA(NDLEV)       , ILA(NDLEV)
      INTEGER   IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &          IGSEL(IGMAX)
      INTEGER   ISTRN1(NDLNE)    , ISTRN2(NDLNE)    ,
     &          KSTRN1(NDLNE)    , KSTRN2(NDLNE)
      INTEGER   IA_C(NDLEV)      , ISA_C(NDLEV)     , ILA_C(NDLEV)  
      INTEGER   I1A(NDTRN)       , I2A(NDTRN) 
      INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &          IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &          IP1A(NDTRN)      , IP2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8    XJA(NDLEV)       , WA(NDLEV)        , WATRAN(NDTRN) ,
     &          TEA(NDTEM)       , DENSA(NDDEN)     ,
     &          RATHA(NDDEN)     , RATIA(NDDEN)     ,
     &          AVAL(NDTRN)      , XMMULT(NDMET)
      REAL*8    CONTR(ICMAX)     ,
     &          ASTRN1(NDLNE)    , ASTRN2(NDLNE)
      REAL*8    EM1(NDTEM,NDDEN) , EM2(NDTEM,NDDEN) , RAT(NDTEM,NDDEN)
      REAL*8    SLCMET(NDMET,NDTEM,NDDEN)     ,
     &          SLCORD(NDLEV,NDTEM,NDDEN)
      REAL*8    STVR(NDLEV,NDTEM,NDDEN)       , STVH(NDLEV,NDTEM,NDDEN)
      REAL*8    STCKM(NDMET,NDTEM,NDDEN)      ,
     &          STVRM(NDMET,NDTEM,NDDEN)      , STVHM(NDMET,NDTEM,NDDEN)
      REAL*8    STACK(NDLEV,NDMET,NDTEM,NDDEN)
      REAL*8    XJA_C(NDLEV)                  , AA(NDTRN)
      REAL*8    SCEF(NVMAX)                   , SCOM(NVMAX,NDTRN)
C-----------------------------------------------------------------------
      CHARACTER MTRGA(NDLEV)*1   , CSTRGA(NDLEV)*18 , STRGA(NDLEV)*22 ,
     &          STRGM(NDMET)*22  , CSTRN1(NDLNE)*40 , CSTRN2(NDLNE)*40
      CHARACTER TCODE(NDTRN)*1   , CSTRGA_C(NDLEV)*18 
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NDMET)     ,
     &          LSTRN1(NDLNE)    , LSTRN2(NDLNE)
C-----------------------------------------------------------------------
      DATA IPAN/0/
      DATA OPEN7 /.FALSE./ , OPEN9 /.FALSE./ , OPEN10/.FALSE./ ,
     &     LGHOST/.FALSE./

C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   npl           , iadftyp       , itieactn , iorb    
      integer   nplr          , npli          , icnti    , icntl   , 
     &          icnts         , icntp
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      integer   ipla(ndmet,ndlev)             , npla(ndlev)
      integer   iitrn(ndtrn)                  , iltrn(ndtrn)       , 
     &          istrn(ndtrn)                  ,                    
     &          ia1a(ndtrn)                   , ia2a(ndtrn)        , 
     &          il1a(ndlev)                   , il2a(ndlev)        , 
     &          is1a(ndlev)                   , is2a(ndlev)        
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet) , 
     &          zpla(ndmet,ndlev)             , beth(ndtrn)        ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
      real*8    auga(ndtrn)                   , wvla(ndlev)
C----------------------------------------------------------------------
      character cpla(ndlev)*1                 , cprta(ndmet)*9      
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  , lbseta(ndmet)      ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lss04a(ndlev,ndmet)
C----------------------------------------------------------------------


C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )

C-----------------------------------------------------------------------
  100 CONTINUE
      IPAN=0

C-----------------------------------------------------------------------
C IF FILES ARE ACTIVE ON UNITS 9 OR 10 - CLOSE THE UNITS
C-----------------------------------------------------------------------

      IF (OPEN9 ) CLOSE(9)
      OPEN9 =.FALSE.
      IF (OPEN10) CLOSE(10)
      OPEN10=.FALSE.

C-----------------------------------------------------------------------
C GET INPUT CONTOUR-PASSING FILE DATA SET NAME (FROM ISPF)
C-----------------------------------------------------------------------

      CALL B7SPF0( REP , DSNPAS )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------

         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF

C-----------------------------------------------------------------------
C OPEN INPUT CONTOUR-PASSING DATA FILE - DSNPAS
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT10 , FILE=DSNPAS , STATUS='OLD' )
      OPEN10=.TRUE.

C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED CONTOUR DATASET
C-----------------------------------------------------------------------

      CALL B7DATA( IUNT10   ,
     &             NDLEV    , NDTEM    , NDDEN  , NDMET  ,
     &             DSNINC   , TITLED_C ,
     &             IZ_C     , IZ0_C    , IZ1_C  , BWNO_C ,
     &             IL_C     , NMET     , NORD   ,
     &             MAXT     , MAXD     , ICNTR  , ICNTH  ,
     &             IA_C     , ISA_C    , ILA_C  , XJA_C  ,
     &             CSTRGA_C ,
     &             IMETR    , IORDR    , TEA    , DENSA  ,
     &             STCKM    , STVR     , STVH   ,
     &             STVRM    , STVHM    , STACK
     &           )

C-----------------------------------------------------------------------
C VERIFY IF ASSOCIATED COPASE DATA FILE (DSNINC) EXISTS
C
C FOR UNIX/IDL MAKE THIS A FATAL ERROR
C BECAUSE IDL WILL NOT KNOW TO GO BACK TO FILE SELECTION
C
C LONGER TERM SOLUTION MAYBE TO ADD EXTRA COMMUNICATION
C TO IDL AND PASS THE ERROR MESSAGE BACK THROUGH THE PIPE
C
C-----------------------------------------------------------------------

      CALL XXDSN2( DSNINC ,
     &             DSN44  , LEXIST
     &           )
         
      IF (.NOT.LEXIST) THEN
         WRITE(I4UNIT(-1),1000) DSNPAS , DSN44
         GOTO 9999
      ENDIF

C-----------------------------------------------------------------------
C RE-ASSIGN COPASE FILE NAME, OPEN IT AND READ IN DATA
C-----------------------------------------------------------------------

      DSNINC=DSN44

      OPEN( UNIT=IUNT09 , FILE=DSNINC , STATUS='OLD' )
      OPEN9=.TRUE.


       itieactn = 0
       
       call xxdata_04( iunt09 , 
     &                 ndlev  , ndtrn  , ndmet   , ndqdn , nvmax ,
     &                 titled , iz     , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa  , lbseta  , prtwta, cprta ,
     &                 il     , qdorb  , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga , isa     , ila   , xja   , 
     &                 wa     ,
     &                 cpla   , npla   , ipla    , zpla  ,
     &                 nv     , scef   ,
     &                 itran  , maxlev ,
     &                 tcode  , i1a    , i2a     , aval  , scom  , 
     &                 beth   ,     
     &                 iadftyp, lprn   , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp  , lrtyp   , lhtyp , lityp , 
     &                 lstyp  , lltyp  , itieactn, ltied
     &                )


C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPES.
C-----------------------------------------------------------------------

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )

C-----------------------------------------------------------------------
C VERIFY IF ADF04 FILE AND CONTOUR FILE DATA ARE CONSISTENT
C-----------------------------------------------------------------------

       call b7datc( ndlev   ,
     &              titled_c , iz_c     , iz0_c  , iz1_c , 
     &              bwno_c   , il_c     ,    
     &              ia_c     , cstrga_c , isa_c  , ila_c , xja_c ,  
     &              titled   , iz       , iz0    , iz1   , 
     &              bwno     , il       ,    
     &              ia       , cstrga   , isa    , ila   , xja
     &             )

C-----------------------------------------------------------------------
C Set up transition energy vector
C-----------------------------------------------------------------------

       do i = 1, icnte
          watran(i) = wa(ie2a(i))-wa(ie1a(i))
       end do

C-----------------------------------------------------------------------
C CHECK IF ELECTRON IMPACT TRANSITIONS EXIST TO THE SELECTED METASTABLE
C-----------------------------------------------------------------------

      CALL B7CHKM( NMET   , IMETR , ICNTE , IE1A , LMETR )
	
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETRS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------

      CALL B7SETP( DSNINC ,
     &             IZ0    , IZ     ,
     &             TITLED , BWNO   ,
     &             NDLEV  , IL     , ICNTE  , ICNTH , ICNTR ,
     &             NDMET  , NMET   , IMETR  ,
     &             NDDEN  , MAXD   , DENSA  ,
     &             NDTEM  , MAXT   , TEA    ,
     &             CSTRGA , ISA    , ILA    , XJA   ,
     &             MTRGA  , STRGA  , STRGM
     &           )


C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------

  200       CALL B7ISPF( IPAN   , LPEND  ,
     &                   NDLNE  ,
     &                   IL     , NMET   , MAXT	  , MAXD   ,
     &                   ICNTE  , ICNTH  , ICNTR  ,
     &                   IE1A   , IE2A   , MTRGA  , STRGA  ,
     &			 WATRAN , STCKM  , STACK  , AVAL ,
     &                   LEQUIL , XMMULT ,
     &                   RATHA  , RATIA  ,
     &                   NSTRN1 , ISTRN1 ,
     &                   NSTRN2 , ISTRN2 , DENSA  , NDLEV ,
     &			 NDMET  , NDTEM  , NDDEN  , NDTRN
     &                 )
      IPAN=-1

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

      IF (LPEND) GOTO 100

C-----------------------------------------------------------------------
C SET UP COEFFICIENTS FOR CALCULATING LINE INTENSITIES
C-----------------------------------------------------------------------

      CALL B7SLCF( NDMET  , NDLEV  , NDTEM  , NDDEN ,
     &             NMET   , NORD   , MAXT   , MAXD  ,
     &             XMMULT , RATIA  , RATHA  , DENSA ,
     &                      ICNTR  , ICNTH  ,
     &             STCKM  , STVR   , STVH   ,
     &             STVRM  , STVHM  ,
     &             SLCMET , SLCORD
     &           )

C-----------------------------------------------------------------------
C SET UP THE TITLES FOR COMPOSITE LINE ASSEMBLY TRANSITIONS
C-----------------------------------------------------------------------

      CALL B7CNAM( NDLEV  , STRGA  ,
     &             ICNTE  , IE1A   , IE2A  ,
     &             NSTRN1 , ISTRN1 ,
     &             CSTRN1
     &           )
      CALL B7CNAM( NDLEV  , STRGA  ,
     &             ICNTE  , IE1A   , IE2A  ,
     &             NSTRN2 , ISTRN2 ,
     &             CSTRN2
     &           )

C-----------------------------------------------------------------------
C IDENTIFY LINES IN COMPOSITE ASSEMBLY AS 'METASTABLE' OR 'ORDINARY'
C-----------------------------------------------------------------------

      CALL B7CTYP( NSTRN1 , ISTRN1 ,
     &             ICNTE  , NMET   , NORD   ,
     &             IE2A   , IMETR  , IORDR  ,
     &             AVAL   ,
     &             KSTRN1 , LSTRN1 , ASTRN1
     &           )
      CALL B7CTYP( NSTRN2 , ISTRN2 ,
     &             ICNTE  , NMET   , NORD   ,
     &             IE2A   , IMETR  , IORDR  ,
     &             AVAL   ,
     &             KSTRN2 , LSTRN2 , ASTRN2
     &           )

C***********************************************************************
C CALCULATE SPECTRUM LINE INTENSITIES AND RATIOS
C***********************************************************************

      RMIN=D1E30
      RMAX=-D1E30
         DO 3 IT=1,MAXT
            DO 4 IN=1,MAXD

C-----------------------------------------------------------------------
C CALCULATE SPECTRUM LINE INTENSITY FOR EACH COMPOSITE ASSEMBLY IN TURN
C-----------------------------------------------------------------------

               CALL B7SLCA( NDLEV  , NDMET    , NMET   ,
     &                      NSTRN1 ,
     &                      ISTRN1 , KSTRN1   , LSTRN1 , ASTRN1 ,
     &                      SLCMET(1,IT,IN)   , SLCORD(1,IT,IN) ,
     &                      STACK (1,1,IT,IN) ,
     &                      EM1(IT,IN)
     &                    )
               CALL B7SLCA( NDLEV  , NDMET    , NMET   ,
     &                      NSTRN2 ,
     &                      ISTRN2 , KSTRN2   , LSTRN2 , ASTRN2 ,
     &                      SLCMET(1,IT,IN)   , SLCORD(1,IT,IN) ,
     &                      STACK (1,1,IT,IN) ,
     &                      EM2(IT,IN)
     &                    )
C
C-----------------------------------------------------------------------
C CALCULATE SPECTRUM LINE RATIO
C-----------------------------------------------------------------------
C
               CALL B7LRAT( EM1(IT,IN) , EM2(IT,IN)  ,
     &                      RMIN       , RMAX        ,
     &                      RAT(IT,IN)
     &                    )
   4        CONTINUE
   3     CONTINUE
C
C***********************************************************************
C END OF CURRENT ANALYSIS: DATA OUTPUT TO 'IUNT07' & GRAPHICAL SECTION
C***********************************************************************
C
C-----------------------------------------------------------------------
C SET UP SPECTRUM LINE RATIOS MAXIMUM AND MINIMUM IN ISPF FUNCTION POOL
C-----------------------------------------------------------------------

      CALL B7SETR( RMIN   , RMAX    )

C-----------------------------------------------------------------------
C IDENTIFY GRAPHICAL OUTPUT FORM FROM ISPF PANELS
C-----------------------------------------------------------------------

300   CONTINUE
      CALL B7SPF1( MAXD   , MAXT    , RMIN   , RMAX   ,
     &             ICMAX  , IGMAX   ,
     &             LGEND  ,
     &             IOPT   , ISEL    ,
     &             LGRD1  , LDEF1   , LOGINT , LCFLOG ,
     &             GTIT1  ,
     &             CONTR  , IGSEL   ,
     &             XL1    , XU1     , YL1    , YU1 ,
     &             LPAPER , LNEWPA  , DSNPAP
     &           )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

      IF (LGEND) GOTO 200


CX----------------------------------------------------------------------
CX ULTRIX PORT - OPEN TEXT OUTPUT DATA SET - DSNPAP - IF REQUESTED.
CX               THIS IS NEW FUNCTIONALITY.  IF LNEWPA IS .TRUE. THEN
CX               EITHER A NEW FILE IS TO BE CREATED OR AN EXISTING FILE
CX               IS TO BE REPLACED.  ANY FILE WHICH IS CURRENTLY OPEN
CX               MUST BE CLOSED FIRST.  IF LNEWPA IS .FALSE. AND LPAPER
CX               IS TRUE THEN OUTPUT IS APPENDED TO A PREVIOUSLY OPEN
CX               FILE IF ANY.
CX----------------------------------------------------------------------

      IF( (LPAPER .AND. LNEWPA) .OR. (LPAPER .AND. .NOT.OPEN7) ) THEN
         IF (OPEN7) CLOSE(UNIT=IUNT07)
         DSN44=DSNPAP
         OPEN(UNIT=IUNT07,FILE=DSN44,STATUS='UNKNOWN')
         OPEN7=.TRUE.
      ENDIF

      IF( LPAPER ) THEN
         CALL B7OUT0( IUNT07 , DATE   , DSNPAS , DSNINC ,
     &             TITLED , IZ     , IZ0    , IZ1    , BWNO ,
     &             IL     ,
     &             IA     , CSTRGA , ISA    , ILA    , XJA  , WA ,
     &             ICNTR  , ICNTH  ,
     &             NMET   , IMETR  , LMETR  , STRGA  ,
     &             MAXT   , TEA    ,
     &             MAXD   , DENSA
     &           )

         CALL B7OUT1( IUNT07 , LEQUIL ,
     &             NDLEV  , NDMET  , NDTEM  , NDDEN  ,
     &                      NMET   , MAXT   , MAXD   ,
     &                      IMETR  , TEA    , DENSA  ,
     &                      XMMULT ,
     &             ICNTE  , ICNTR  , ICNTH  ,
     &             IE1A   , IE2A   ,
     &             STRGA  , RATHA  , RATIA  ,
     &             NSTRN1 , ISTRN1 , LSTRN1 , ASTRN1 ,
     &             EM1    ,
     &             NSTRN2 , ISTRN2 , LSTRN2 , ASTRN2 ,
     &             EM2    ,
     &             RAT
     &           )
      ENDIF

C UNIX/IDL PORT USES LGRD1 FLAG TO SELECT GRAPHICAL OUTPUT

      IF( .NOT.LGRD1 ) GOTO 300

C-----------------------------------------------------------------------
C  SET UP DEFAULT CONTOUR VALUES - IF REQUESTED : OPTION 1 ONLY.
C-----------------------------------------------------------------------

         IF ( LDEF1 .AND. (IOPT.EQ.1) ) THEN
            CALL B7CDEF( LCFLOG , NCONTR  ,
     &                   RMIN   , RMAX    ,
     &                   ISEL   , CONTR
     &                 )
         ENDIF

C-----------------------------------------------------------------------
C  SET UP GRAPHICAL OUTPUT - IF REQUESTED
C-----------------------------------------------------------------------

      CALL B7OUTG( IOPT  , LGHOST , DATE  ,
     &             NDDEN , NDTEM  , IGMAX , ICMAX  ,
     &             IZ    , TITLED , GTIT1 , DSNINC ,
     &             ISEL  , CONTR  , IGSEL ,
     &             LGRD1 , LDEF1  , LOGINT,
     &             XL1   , XU1    , YL1   , YU1    ,
     &             NMET  , MAXD   , MAXT  , NSTRN1 , NSTRN2,
     &             XMMULT, DENSA  , TEA   , CSTRN1 , CSTRN2,
     &             STRGM ,
     &             RAT
     &           )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

      GOTO 300

C-----------------------------------------------------------------------
 9999 STOP
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------

 1000 FORMAT(1X,30('*'),' ADAS207  MESSAGE ',30('*')/
     &       1X,'CONTOUR  PASSING  FILE DSN: ',A/
     &       1X,'ASSOCIATED COPASE FILE DSN: ',A//
     &       1X,'MESSAGE: THE ASSOCIATED COPASE FILE DOES NOT EXIST.'/
     &       1X,78('*')/)

C-----------------------------------------------------------------------

      END
