CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7ispf.for,v 1.6 2004/07/06 11:27:07 whitefor Exp $ Data $Date: 2004/07/06 11:27:07 $
CX
      SUBROUTINE B7ISPF( IPAN   , LPEND  ,
     &                   NDLNE  ,
     &                   IL     , NMET   , MAXT	 , MAXD  ,
     &                   ICNTE  , ICNTH  , ICNTR ,
     &                   IE1A   , IE2A   , MTRGA , STRGA ,
     &			 WATRAN , STCKM , STACK , AVAL ,
     &                   LEQUIL , XXMULT ,
     &                   RATHA  , RATIA  ,
     &                   NSTRN1 , ISTRN1 ,
     &                   NSTRN2 , ISTRN2 , DENSA , NDLEV ,
     &			 NDMET  , NDTEM  , NDDEN , NDTRN
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDLNE    = MAXIMUM NUMBER OF COMPOSITE LINES ALLOWED
C
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)   NMET     = NUMBER OF METASTABLES
C  INPUT : (I*4)   MAXD     = NUMBER OF INPUT DENSITIES
C  INPUT : (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES
C
C  INPUT : (I*4)   ICNTE    = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)   ICNTH    = NUMBER OF FREE ELECTRON RECOMBINATIONS
C  INPUT : (I*4)   ICNTR    = NO. OF CHARGE EXCHANGE RECOMBINATIONS
C
C  INPUT : (I*4)   IE1A()   = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)   IE2A()   = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C  INPUT : (C*1)   MTRGA()  = 'M' => METASTABLE LEVEL
C                             ' ' => ORDINARY   LEVEL
C                             DIMENSION: LEVEL INDEX
C  INPUT : (C*22)  STRGA()  = LEVEL DESIGNATIONS
C  INPUT : (R*8)  WATRAN()  = TRANSITION ENERGY (CM-1)
C  INPUT : (R*8) STACK(,,,) = POPULATION DEPENDENCE
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STCKM(,,)  = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8)    AVAL()  = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C  INPUT : (R*8)   DENSA()  = ELECTRON DENSITIES (UNITS: CM-3)
C  INPUT : (I*4)   NDLEV    = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)   NDDEN    = MAXIMUM NUMBER OF INPUT DENSITIES ALLOWED
C  INPUT : (I*4)   NDTRN    = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C  INPUT : (I*4)   NDTEM    = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDMET    = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C  OUTPUT: (L*4)   LEQUIL   = .TRUE.  =>     EQUILIBRIUM CONDITIONS
C                           = .FALSE. => NON-EQUILIBRIUM CONDITIONS
C  OUTPUT: (R*8)   XXMULT() = METASTABLE LEVELS: SCALING FACTORS
C
C  OUTPUT: (R*8)   RATHA()  = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  OUTPUT: (R*8)   RATIA()  = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C
C  OUTPUT: (I*4)   NSTRN1   = NUMBER OF LINES CHOSEN FOR FIRST COMPOSITE
C                             ASSEMBLY
C  OUTPUT: (I*4)   ISTRN1() = SELECTED TRANSITION INDEXES FOR FIRST
C                             COMPOSITE LINE ASSEMBLY
C
C  OUTPUT: (I*4)   NSTRN2   = NUMBER OF LINES CHOSEN FOR SECOND
C                             COMPOSITE ASSEMBLY
C  OUTPUT: (I*4)   ISTRN2() = SELECTED TRANSITION INDEXES FOR SECOND
C                             COMPOSITE LINE ASSEMBLY
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   I        = LOOP INCREMENT
C          (I*4)   LOGIC    = USED TO PIPE LOGICAL VALUES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  26/11/90 - ADAS91 - PE BRIDEN - AMENDED 'XXDISP' AND 'XXPAN0'
C                                          ARGUMENT LISTS. THEY NOW IN-
C                                          CLUDE DISPLAY RETURN CODES.
C                                          IF 'RETURN' OR 'END' ENTERED
C                                          ON A PANEL, EXCEPT VIA PFKEY,
C                                          PROGRAM TERMINATES.
C
C UPDATE:  09/03/95 - UNIX   - SP BELLAMY  UNIX/IDL PORT
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 30-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 12-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED IMPLICIT DO LOOPS FROM PIPE COMMUNICATIONS
C                 WRITING TO IDL
C
C VERSION: 1.3                          DATE: 12-02-96
C MODIFIED: TIM HAMMOND
C               - ADDED WRITING OF VARIABLE DENSA TO IDL AS THIS IS
C                 NEEDED IN THE PROCESSING WIDGET (OCCASIONALLY)
C
C VERSION: 1.4                          DATE: 03-04-96
C MODIFIED: TIM HAMMOND 
C               - MODIFIED SO DATA IS ONLY READ FROM IDL WHEN USER
C                 SELECTS 'DONE' RATHER THAN ALL THE TIME.
C
C VERSION: 1.5                          DATE: 06-08-96
C MODIFIED: TIM HAMMOND
C           - ADDED A WRITE TO I4UNIT WHICH ALTHOUGH IT SHOULD
C             HAVE NO EFFECT APPEARS TO BE NEEDED TO STOP THE CODE
C             PRODUCING A MASSIVE CORE DUMP ON THE HP.
C
C VERSION: 1.6                          DATE: 14-11-01
C MODIFIED: RICHARD MARTIN
C		- MODIFIED TO PASS WATRAN, STACK, STCKM & AVAL TO IDL.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IPAN      , NDLNE      , IL        , NMET    ,
     &           MAXT	   , MAXD       , ICNTE     , ICNTH   , ICNTR,
     &           NSTRN1    , NSTRN2     , NDDEN     , NDTRN   ,
     &	     NDLEV	   , NDMET	    , NDTEM
      INTEGER    ITEMP     , IDENS
C-----------------------------------------------------------------------
      LOGICAL    LPEND     , LEQUIL
C-----------------------------------------------------------------------
      INTEGER    IE1A(ICNTE)            , IE2A(ICNTE)
      INTEGER    ISTRN1(NDLNE)          , ISTRN2(NDLNE)
C-----------------------------------------------------------------------
      REAL*8     XXMULT(NMET)
      REAL*8     RATHA(MAXD)            , RATIA(MAXD)
      REAL*8     DENSA(NDDEN)
	REAL*8     WATRAN(NDTRN)	    , AVAL(NDTRN)
	REAL*8     STCKM(NDMET,NDTEM,NDDEN)
	REAL*8     STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      CHARACTER  MTRGA(IL)*1            , STRGA(IL)*22
C-----------------------------------------------------------------------
      INTEGER    LOGIC     , I4UNIT
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO    , I , K
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
C
C     BIZARRELY THE NEXT LINE APPEARS TO BE NEEDED TO STOP THE HP
C     CRASHING
C
      WRITE(I4UNIT(-1),*) " "

      IF( LPEND ) THEN
          WRITE( PIPEOU, * ) ONE
      ELSE
          WRITE( PIPEOU, * ) ZERO
      ENDIF
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NDLNE
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) IL
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NDLEV
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NMET
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) MAXD
      CALL XXFLSH( PIPEOU )

      DO 5, I=1, MAXD
          WRITE( PIPEOU, * ) DENSA(I)
          CALL XXFLSH( PIPEOU )
5     CONTINUE

      WRITE( PIPEOU, * ) ICNTE
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) ICNTH
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) ICNTR
      CALL XXFLSH( PIPEOU )

      WRITE(I4UNIT(-1),*) " "

      DO 1, I=1, ICNTE
          WRITE( PIPEOU, * ) IE1A(I)
          CALL XXFLSH( PIPEOU )
1     CONTINUE

      DO 2, I=1, ICNTE
          WRITE( PIPEOU, * ) IE2A(I)
          CALL XXFLSH( PIPEOU )
2     CONTINUE

      DO 3, I=1, IL
          WRITE( PIPEOU, '(A1)' ) MTRGA(I)
          CALL XXFLSH( PIPEOU )
3     CONTINUE

      DO 4, I=1, IL
          WRITE( PIPEOU, '(A22)' ) STRGA(I)
          CALL XXFLSH( PIPEOU )
4     CONTINUE

      DO 6, I=1, ICNTE
          WRITE( PIPEOU, * ) REAL(WATRAN(I))
          CALL XXFLSH( PIPEOU )
6     CONTINUE

	ITEMP=INT(MAXT/2)
	IDENS=INT(MAXD/2)
	DO 7, I=1,NMET
      	WRITE( PIPEOU, * ) STCKM(I,ITEMP,IDENS)
      	CALL XXFLSH( PIPEOU )
		DO 8, K=1,NDLEV
      		WRITE( PIPEOU, * ) STACK(K,I,ITEMP,IDENS)
      		CALL XXFLSH( PIPEOU )				
8		CONTINUE
7	CONTINUE

      DO 9, I=1, ICNTE
          WRITE( PIPEOU, * ) REAL(AVAL(I))
          CALL XXFLSH( PIPEOU )
9     CONTINUE

C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF

      IF (.NOT.LPEND) THEN
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LEQUIL = .TRUE.
          ELSE
              LEQUIL = .FALSE.
          ENDIF

          READ(PIPEIN,*) (XXMULT(I),I=1,NMET)

          READ(PIPEIN,*) (RATHA(I),I=1,MAXD)

          READ(PIPEIN,*) (RATIA(I),I=1,MAXD)

          READ(PIPEIN,*) NSTRN1

          IF( NSTRN1.GT.0 ) THEN
             READ(PIPEIN,*) (ISTRN1(I),I=1,NSTRN1)
          ENDIF

          READ(PIPEIN,*) NSTRN2

          IF( NSTRN2.GT.0 ) THEN
             READ(PIPEIN,*) (ISTRN2(I),I=1,NSTRN2)
          ENDIF
      ENDIF

C
C-----------------------------------------------------------------------
C
      RETURN
      END
