CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7chkm.for,v 1.1 2004/07/06 11:26:21 whitefor Exp $ Data $Date: 2004/07/06 11:26:21 $
CX
      SUBROUTINE B7CHKM( NMET   , IMETR , ICNTE , IE1A , LMETR )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7CHKM *********************
C
C  PURPOSE: TO CHECK IF TRANSITIONS EXIST TO THE METASTABLE LEVELS.
C           (IDENTICAL TO: 'BXCHKM' )
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NMET    = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  INPUT : (I*4)   IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  INPUT : (I*4)   ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT : (I*4)   IE1A()  = ELECTRON IMPACT TRANSITION: LOWER ENERGY
C                            LEVEL INDEX.
C
C  OUTPUT: (L*4)   LMETR() = .TRUE.  =>ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                       'IMETR()'.
C                            .FALSE. =>ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C
C          (I*4)  I4UNIT   = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)   I       = GENERAL USE
C          (I*4)   J       = GENERAL USE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   NMET        , ICNTE
      INTEGER   I          , J
      INTEGER   IMETR(NMET) , IE1A(ICNTE)
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NMET)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
         DO 1 I = 1,NMET
            LMETR(I)=.FALSE.
               DO 2 J = 1,ICNTE
                  IF ( IE1A(J) .EQ. IMETR(I) ) LMETR(I)=.TRUE.
    2          CONTINUE
            IF ( .NOT.LMETR(I) ) WRITE(I4UNIT(-1),1000) I,IMETR(I)
    1    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,31('*'),' B7CHKM WARNING ',31('*')/
     &        1X,' METASTABLE INDEX: ',I3,' - LEVEL INDEX: ',I3/
     &        1X,' NO ELECTRON IMPACT TRANSITIONS EXIST TO THIS LEVEL'/
     &        1X,79('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
