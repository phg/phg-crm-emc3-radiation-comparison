CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7datc.for,v 1.6 2004/07/06 11:26:48 whitefor Exp $ Data $Date: 2004/07/06 11:26:48 $
CX
       SUBROUTINE B7DATC( ndlev   ,
     &                   titled_c , iz_c     , iz0_c  , iz1_c , 
     &                   bwno_c   , il_c     ,    
     &                   ia_c     , cstrga_c , isa_c  , ila_c , xja_c ,  
     &                   titled   , iz       , iz0    , iz1   , 
     &                   bwno     , il       ,    
     &                   ia       , cstrga   , isa    , ila   , xja
     &                  )

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7DATC *********************
C
C  PURPOSE:  Check consistency of adf04 file with contour passing file 
C            data.
C
C  CALLING PROGRAM: ADAS207
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ndlev     = maximum number of levels that can be read
C
C  INPUT : (C*3)  titled_c  = contour file: element symbol.
C  INPUT : (I*4)  iz_c      = contour file: recombined ion charge read
C  INPUT : (I*4)  iz0_c     = contour file: nuclear charge read
C  INPUT : (I*4)  iz1_c     = contour file: recombining ion charge read
C  INPUT : (R*8)  bwno_c    = contour file: ionisation potential (cm-1)
C  INPUT : (I*4)  il_c      = contour file: number of energy levels
C  INPUT : (I*4)  ia_c()    = contour file: energy level index number
C  INPUT : (C*18) cstrga_c()= contour file: configuration for level 'ia()'
C  INPUT : (I*4)  isa_c()   = contour file: multiplicity for level 'ia()'
C  INPUT : (I*4)  ila_c()   = contour file: quantum no. (L) for level 'ia()'
C  INPUT : (R*8)  xja_c()   = contour file: quantum no.(J) for level 'ia()'
C
C  INPUT : (C*3)  titled    = adf04 file: element symbol.
C  INPUT : (I*4)  iz        = adf04 file: recombined ion charge read
C  INPUT : (I*4)  iz0       = adf04 file: nuclear charge read
C  INPUT : (I*4)  iz1       = adf04 file: recombining ion charge read
C  INPUT : (R*8)  bwno      = adf04 file: ionisation potential (cm-1)
C  INPUT : (I*4)  il        = adf04 file: number of energy levels
C  INPUT : (I*4)  ia()      = adf04 file: energy level index number
C  INPUT : (C*18) cstrga()  = adf04 file: configuration for level 'ia()'
C  INPUT : (I*4)  isa()     = adf04 file: multiplicity for level 'ia()'
C  INPUT : (I*4)  ila()     = adf04 file: quantum no. (L) for level 'ia()'
C  INPUT : (R*8)  xja()     = adf04 file: quantum no.(J) for level 'ia()'
C
C
C NOTE: Replaces original b7datc.for by Paul Briden. This version only
C       checks consistency and returns the transition energy.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C VERSION: 1.7                          DATE: 01-05-2003
C MODIFIED: Martin O'Mullane
C		    - Rewrite and functionality changed.
C
C-----------------------------------------------------------------------
      integer   ndlev
      integer   iz_c         , iz0_c          , iz1_c      , il_c
      integer   iz           , iz0            , iz1        , il
      integer   i            , i4unit
C-----------------------------------------------------------------------
      real*8    bwno_c       , bwno           , res
C-----------------------------------------------------------------------
      character titled_c*3   , titled*3 
      character cerror*32    , cerr*56
C-----------------------------------------------------------------------
      integer   ia(ndlev)    , isa(ndlev)     , ila(ndlev)
      integer   ia_c(ndlev)  , isa_c(ndlev)   , ila_c(ndlev)
C-----------------------------------------------------------------------
      real*8    xja_c(ndlev) , xja(ndlev)
C-----------------------------------------------------------------------
      character cstrga_c(ndlev)*18 , cstrga(ndlev)*18
C-----------------------------------------------------------------------

      cerror = 'Contour/adf04 file discrepancy: '

C-----------------------------------------------------------------------
C Check ion and ionisation potential
C-----------------------------------------------------------------------
      
      cerr = ' '
      res = abs(bwno_c - bwno)

      if (res.GT.1.0)         cerr=cerror//' Ionisation potential   '
      if (titled_c.NE.titled) cerr=cerror//' Ion symbol             '
      if (iz1_c.NE.iz1)       cerr=cerror//' Recombining ion charge '
      if (iz0_c.NE.iz0)       cerr=cerror//' Nuclear charge (z0)    '
      if (iz_c.NE.iz)         cerr=cerror//' Recombined ion charge  '
      
      if (cerr.ne.' ') then
         write(i4unit(-1),1001) cerr
         write(i4unit(-1),1002)
         stop
      endif

C-----------------------------------------------------------------------
C Check consistency of levels lists
C-----------------------------------------------------------------------
      
      if (il_c.NE.il) then
      
         cerr = cerror//' No. of levels mismatch '
         write(i4unit(-1),1001) cerr
         write(i4unit(-1),1002)
         stop
      
      endif
      
      
       do i = 1, il

         cerr = ' '
         
         if (xja_c(i).ne.xja(i)) cerr=cerror//' level **** j-value     '
         if (ila_c(i).ne.ila(i)) cerr=cerror//' level **** l-value     '
         if (isa_c(i).ne.isa(i)) cerr=cerror//' level **** s-value     '
         if (ia_c(i).ne.ia(i))   cerr=cerror//' level **** index       '
         if (cstrga_c(i).ne.cstrga(i)) then
             cerr = cerror//' level *** configuration'
         endif
         
         if (cerr.ne.' ') then
            write(cerr(40:43),'(i4)') i
            write(i4unit(-1),1001) cerr
            write(i4unit(-1),1002)
            stop
         endif
       
       end do

C-----------------------------------------------------------------------

 1001 FORMAT(1X,'B7DATC ERROR: ',A)
 1002 FORMAT(/,1X,29('*'),' PROGRAM TERMINATED ',29('*'))

C-----------------------------------------------------------------------

      RETURN
      END
