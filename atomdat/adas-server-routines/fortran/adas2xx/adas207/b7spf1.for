CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7spf1.for,v 1.2 2004/07/06 11:28:27 whitefor Exp $ Data $Date: 2004/07/06 11:28:27 $
CX
      SUBROUTINE B7SPF1( MAXD   , MAXT    , RMIN    , RMAX   ,
     &                   ICMAX  , IGMAX   ,
     &                   LGEND  ,
     &                   IOPT   , ISEL    ,
     &                   LGRD1  , LDEF1   , LOGINT  , LCFLOG ,
     &                   GTIT1  ,
     &                   CONTR  , IGSEL   ,
     &                   XL1    , XU1     , YL1    , YU1 ,
CX UNIX/IDL PORT - ADD NEW ARGUMENTS FOR TEXT OUTPUT
     &                   LPAPER , LNEWPA  , DSNPAP
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7SPF1 *********************
C
C  PURPOSE: GRAPHICAL ANALYSIS OF DATA: ISPF PANEL INPUT SUBROUTINE
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   MAXD     = NUMBER OF DENSITY VALUES ENTERED
C  INPUT : (I*4)   MAXT     = NUMBER OF TEMPERATURE VALUES ENTERED
C  INPUT : (R*8)   RMIN     = MINIMUM SPECTRUM LINE RATIO
C  INPUT : (R*8)   RMAX     = MAXIMUM SPECTRUM LINE RATIO
C
C  INPUT : (I*4)   IGMAX    = MAXIMUM NUMBER OF TEMPERATURE OR DENSITY
C                             VALUES THAT CAN BE PLOTTED ON A SINGLE
C                             GRAPH. MUST BE <= 20.
C  INPUT : (I*4)   ICMAX    = MAXIMUM NUMBER OF USER ENTERED CONTOUR
C                             VALUES THAT CAN BE PLOTTED ON A SINGLE
C                             GRAPH. MUST BE 20.
C
C  I/O   : (L*4)   LGEND    = .TRUE.  => END GRAPHICAL ANALYSIS OF
C                                        CURRENT DATA
C                           = .FALSE. => CONTINUE GRAPHICAL ANALYSIS OF
C                                        CURRENT DATA
C
C  OUTPUT: (I*4)   IOPT     = GRAPHICAL OPTION NUMBER:
C                             1 => CONTOUR PLOT
C                                  (PANEL: P20716A)
C                             2 => SPECTRUM-LINE RATIO VS TEMP. PLOT
C                                  (PANEL: P20716B)
C                             3 => SPECTRUM-LINE RATIO VS DENSITY PLOT
C                                  (PANEL: P20716C)
C
C  OUTPUT: (I*4)   ISEL     = OPTION 1 - NUMBER OF OWN CONTOUR VALUES
C                                        ENTERED (0 IF DEFAULT SELECTED)
C                             OPTION 2 - NO. OF DENSITIES SELECTED FOR
C                                        GRAPHING (FROM INPUT LIST).
C                             OPTION 3 - NO. OF TEMPERATURES SELECTED
C                                        FOR GRAPHING (FROM INPUT LIST).
C
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C  OUTPUT: (L*4)   LOGINT   = OPTION 1:
C                             .TRUE.  => LOGARITHMIC INTERPOLATION
C                             .FALSE. => LINEAR INTERPOLATION
C                             OPTION 2 - NOT USED
C                             OPTION 3 - NOT USED
C  OUTPUT: (L*4)   LCFLOG   = OPTION 1 (DEFAULT CONTOUR VALUES):
C                             .TRUE.  => LOGARITHMIC CONTOUR SPACING
C                             .FALSE. => LINEAR CONTOUR SPACING
C                             OPTION 2 - NOT USED
C                             OPTION 3 - NOT USED
C
C  OUTPUT: (C*40)  GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C
C  OUTPUT: (R*8)   CONTR()  = OPTION 1 - USER ENTERED CONTOUR VALUES
C                             OPTION 2 - NOT USED
C                             OPTION 3 - NOT USED
C  OUTPUT: (I*4)   IGSEL()  = OPTION 1 - NOT USED
C                             OPTION 2 - INDEXES OF DENSITIES SELECTED
C                                        FOR GRAPHING.
C                             OPTION 3 - INDEXES OF TEMPERATURES
C                                        SELECTED FOR GRAPHING.
C
C  OUTPUT: (R*8)   XL1      = OPTION 1 - NOT USED
C                             OPTION 2 - LOWER LIMIT FOR X-AXIS OF GRAPH
C                             OPTION 3 - LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = OPTION 1 - NOT USED
C                             OPTION 2 - UPPER LIMIT FOR X-AXIS OF GRAPH
C                             OPTION 3 - UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = OPTION 1 - NOT USED
C                             OPTION 2 - LOWER LIMIT FOR Y-AXIS OF GRAPH
C                             OPTION 3 - LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = OPTION 1 - NOT USED
C                             OPTION 2 - UPPER LIMIT FOR Y-AXIS OF GRAPH
C                             OPTION 3 - UPPER LIMIT FOR Y-AXIS OF GRAPH
C
CX OUTPUT: (L*4)  LPAPER  = .TRUE.  => OUTPUT DATA TO TEXT OUTPUT FILE.
CX                          .FALSE. => NO OUTPUT OF CURRENT DATA TO
CX                                     CONTOUR TEXT OUTPUT FILE.
CX OUTPUT: (L*4)  LNEWPA    .TRUE.  => NEW TEXT OUTPUT FILE OR
CX                                     REPLACEMENT OF EXISTING FILE
CX                                     REQUIRED.
CX                          .FALSE. => ALLOW APPEND ON EXISTING OPEN
CX                                     TEXT FILE.
CX OUTPUT: (C*80) DSNPAP  = INPUT TEXT OUTPUT DATA SET NAME
CX
CX         (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
CX         (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
CX         (I*4)   I        = LOOP INCREMENT
CX         (I*4)   LOGIC    = USED TO PIPE LOGICAL VALUES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    17/10/90
C
C UPDATE:  26/11/90 - ADAS91 - PE BRIDEN - AMENDED   'XXDISP'   ARGUMENT
C                                          LIST. IT NOW INCLUDES DISPLAY
C                                          RETURN CODES.
C                                          IF 'RETURN' OR 'END' ENTERED
C                                          ON A PANEL, EXCEPT VIA PFKEY,
C                                          PROGRAM TERMINATES.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  20/03/95 - SP BELLAMY - UNIX/IDL PORT
C
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1                          DATE:??
C
C VERSION: 1.2                          DATE: 06-08-96
C MODIFIED: TIM HAMMOND
C           - ADDED A WRITE TO I4UNIT WHICH ALTHOUGH IT SHOULD
C             HAVE NO EFFECT APPEARS TO BE NEEDED TO STOP THE CODE
C             PRODUCING A MASSIVE CORE DUMP ON THE HP.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   MAXD       , MAXT       ,
     &          ICMAX      , IGMAX      ,
     &          IOPT       , ISEL
C-----------------------------------------------------------------------
      REAL*8    RMIN       , RMAX       ,
     &          XL1        , XU1        , YL1       , YU1
C-----------------------------------------------------------------------
      CHARACTER  GTIT1*40
C-----------------------------------------------------------------------
      LOGICAL    LGEND
      LOGICAL    LGRD1     , LDEF1      , LOGINT    , LCFLOG
C-----------------------------------------------------------------------
      INTEGER    IGSEL(IGMAX)
C-----------------------------------------------------------------------
      REAL*8     CONTR(ICMAX)
C-----------------------------------------------------------------------
      LOGICAL    LPAPER    , LNEWPA
      CHARACTER  DSNPAP*80
C-----------------------------------------------------------------------
      INTEGER    LOGIC     , I4UNIT
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO    , I
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
C
C     BIZARRELY THE NEXT LINE APPEARS TO BE NEEDED TO STOP THE HP
C     CRASHING
C
      WRITE(I4UNIT(-1),*) "    "

      IF( LGEND ) THEN
          WRITE( PIPEOU, * ) ONE
      ELSE
          WRITE( PIPEOU, * ) ZERO
      ENDIF
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) MAXD
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) MAXT
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) RMIN
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) RMAX
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) IGMAX
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) ICMAX
      CALL XXFLSH( PIPEOU )

C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      WRITE(I4UNIT(-1),*) "     "
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LGEND = .TRUE.
      ELSE
          LGEND = .FALSE.
      ENDIF

      IF( .NOT.LGEND ) THEN
         READ(PIPEIN,*) IOPT

         READ(PIPEIN,*) ISEL

         READ(PIPEIN,*) LOGIC
         IF (LOGIC.EQ.ONE) THEN
            LGRD1 = .TRUE.
         ELSE
            LGRD1 = .FALSE.
         ENDIF

         READ(PIPEIN,*) LOGIC
         IF (LOGIC.EQ.ONE) THEN
            LDEF1 = .TRUE.
         ELSE
            LDEF1 = .FALSE.
         ENDIF

         IF( IOPT.EQ.1 ) THEN
            READ(PIPEIN,*) LOGIC
            IF (LOGIC.EQ.ONE) THEN
               LOGINT = .TRUE.
            ELSE
               LOGINT = .FALSE.
            ENDIF

            READ(PIPEIN,*) LOGIC
            IF (LOGIC.EQ.ONE) THEN
               LCFLOG = .TRUE.
            ELSE
               LCFLOG = .FALSE.
            ENDIF
         ENDIF

         READ(PIPEIN,'(A)') GTIT1

         IF( IOPT.EQ.1 ) THEN
            IF( ISEL.GT.0 ) THEN
               READ(PIPEIN,*) (CONTR(I),I=1,ISEL)
            ENDIF
         ELSE
            READ(PIPEIN,*) (IGSEL(I),I=1,ISEL)
            IF( .NOT.LDEF1 ) THEN
               READ(PIPEIN,*) XL1
               READ(PIPEIN,*) XU1
               READ(PIPEIN,*) YL1
               READ(PIPEIN,*) YU1
            ENDIF
         ENDIF

         READ(PIPEIN,*) LOGIC
         IF (LOGIC.EQ.ONE) THEN
            LPAPER = .TRUE.
         ELSE
            LPAPER = .FALSE.
         ENDIF

         IF( LPAPER ) THEN
            READ(PIPEIN,*) LOGIC
            IF (LOGIC.EQ.ONE) THEN
               LNEWPA = .TRUE.
            ELSE
               LNEWPA = .FALSE.
            ENDIF
            READ(PIPEIN,'(A)') DSNPAP
         ENDIF

      ENDIF

C
C-----------------------------------------------------------------------
C
      RETURN
      END
