CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7data.for,v 1.1 2004/07/06 11:26:30 whitefor Exp $ Data $Date: 2004/07/06 11:26:30 $
CX
      SUBROUTINE B7DATA( IUNIT  ,
     &                   NDLEV  , NDTEM  , NDDEN  , NDMET ,
     &                   DSNINC , TITLED ,
     &                   IZ     , IZ0    , IZ1    , BWNO  ,
     &                   IL     , NMET   , NORD   ,
     &                   MAXT   , MAXD   , ICNTR  , ICNTH ,
     &                   IA     , ISA    , ILA    , XJA   ,
     &                   CSTRGA ,
     &                   IMETR  , IORDR  , TEA    , DENSA ,
     &                   STCKM  , STVR   , STVH   ,
     &                   STVRM  , STVHM  , STACK
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B7DATA *******************
C
C  PURPOSE:  TO INPUT DATA FROM A CONTOUR PASSING FILE.
C            POPULATION  DATA  FOR DIAGNOSTIC USE.
C
C  CALLING PROGRAM: ADAS207
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = INPUT UNIT NUMBER FOR RESULTS
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
CX  OUTPUT: (C*44) DSNINC  = INPUT COPASE DATA SET NAME (IN QUOTES),
C  OUTPUT: (C*80) DSNINC  = INPUT COPASE DATA SET NAME (IN QUOTES),
C                           USED TO GENERATE 'CONTOUR' DATA.
C  OUTPUT: (C*3)  TITLED  = ELEMENT SYMBOL.
C
C  OUTPUT: (I*4)  IZ      =  RECOMBINED ION CHARGE
C  OUTPUT: (I*4)  IZ0     =         NUCLEAR CHARGE
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  OUTPUT: (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  OUTPUT: (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  OUTPUT: (I*4)  NMET    = NUMBER OF METASTABLES LEVELS: 1<=NMET<=NDMET
C  OUTPUT: (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C
C  OUTPUT: (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C  OUTPUT: (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C  OUTPUT: (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  OUTPUT: (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  OUTPUT: (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C
C  OUTPUT: (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  OUTPUT: (I*4)  IORDR() = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                           LIST.
C  OUTPUT: (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  OUTPUT: (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C
C  OUTPUT: (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  OUTPUT: (R*8) STVR(,,)  = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  OUTPUT: (R*8) STVH(,,)  =  CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  OUTPUT: (R*8) STVRM(,,) = METASTABLE FREE ELECTRON RECOMBINATION
C                            COEFFICIENTS.
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  OUTPUT: (R*8) STVHM(,,) = METASTABLE CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  OUTPUT: (R*8) STACK(,,,)= POPULATION DEPENDENCE
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C
C          (I*4) I4UNIT    = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4) I         = GENERAL USE
C          (I*4) J         = GENERAL USE
C          (I*4) K         = GENERAL USE
C          (I*4) L         = GENERAL USE
C
C NOTE:
C          THIS INPUT DATA IS FROM THE PROGRAM 'SPFPOPN/P'
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  22/10/92 - PEB: INCLUDED ERROR HANDLING FOR ARRAY OVERFLOWS
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  28/01/94 - PE BRIDEN - ADAS91: INCREASED CSTRGA C*12 -> C*18
C                                        FORMAT 1003 CHANGED ACCORDINGLY
C
C UPDATE:  09/03/95 - SP BELLAMY -  UNIX: INCREASE DSNINC TO 80
C                                         AND CHANGE FORMAT 1000
C
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   IUNIT     ,
     &          NDLEV     , NDTEM      , NDDEN   , NDMET
      INTEGER   IZ        , IZ0        , IZ1     ,
     &          IL        , NMET       , NORD    ,
     &          MAXT      , MAXD       , ICNTR   , ICNTH
      INTEGER   I         , J          , K       , L
C-----------------------------------------------------------------------
      REAL*8    BWNO
C-----------------------------------------------------------------------
      CHARACTER TITLED*3         , DSNINC*80
      CHARACTER CSTRGA(NDLEV)*18
C-----------------------------------------------------------------------
      INTEGER   IA(NDLEV)        , ISA(NDLEV)       , ILA(NDLEV)
      INTEGER   IMETR(NDMET)     , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8   XJA(NDLEV)        , TEA(NDTEM)       , DENSA(NDDEN)
      REAL*8   STCKM(NDMET,NDTEM,NDDEN)
      REAL*8   STVR(NDLEV,NDTEM,NDDEN)        , STVH(NDLEV,NDTEM,NDDEN)
      REAL*8   STVRM(NDMET,NDTEM,NDDEN)       , STVHM(NDMET,NDTEM,NDDEN)
      REAL*8   STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      READ(IUNIT,1000) DSNINC
      READ(IUNIT,1001) TITLED, IZ   , IZ0 , IZ1  , BWNO
      READ(IUNIT,1002) IL    , NMET ,NORD , MAXT , MAXD , ICNTR , ICNTH
C
         IF (IL.GT.NDLEV) THEN
            WRITE(I4UNIT(-1),1006) NDLEV , ' ENERGY LEVELS'
            WRITE(I4UNIT(-1),1007)
            STOP
         ELSEIF (NMET.GT.NDMET) THEN
            WRITE(I4UNIT(-1),1006) NDMET , ' METASTABLES'
            WRITE(I4UNIT(-1),1007)
            STOP
         ELSEIF (MAXT.GT.NDTEM) THEN
            WRITE(I4UNIT(-1),1006) NDTEM , ' TEMPERATURES'
            WRITE(I4UNIT(-1),1007)
            STOP
         ELSEIF (MAXD.GT.NDDEN) THEN
            WRITE(I4UNIT(-1),1006) NDDEN , ' DENSITIES'
            WRITE(I4UNIT(-1),1007)
            STOP
         ENDIF
C
      READ(IUNIT,1003) ( IA(I),CSTRGA(I),ISA(I),ILA(I),XJA(I), I=1,IL )
      READ(IUNIT,1002) ( IMETR(I) , I=1,NMET )
      READ(IUNIT,1002) ( IORDR(I) , I=1,NORD )
      READ(IUNIT,1004) ( TEA(I)   , I=1,MAXT )
      READ(IUNIT,1004) ( DENSA(I) , I=1,MAXD )
      READ(IUNIT,1005) ( ( ( ( STACK(I,J,K,L) , I=1,NORD ), J=1,NMET ),
     &                                          K=1,MAXT ), L=1,MAXD )
      READ(IUNIT,1005) ( ( (   STCKM(I,J,K)   , I=1,NMET ), J=1,MAXT ),
     &                                          K=1,MAXD )
C-----------------------------------------------------------------------
         IF (ICNTR.GT.0) THEN
            READ(IUNIT,1005) ( ( ( STVR(I,J,K), I=1,NORD ), J=1,MAXT ),
     &                                          K=1,MAXD )
            IF (NMET.GT.1) READ(IUNIT,1005)
     &                       ( ( ( STVRM(I,J,K), I=2,NMET ), J=1,MAXT ),
     &                                           K=1,MAXD )
         ENDIF
C-----------------------------------------------------------------------
         IF(ICNTH.GT.0) THEN
            READ(IUNIT,1005) ( ( ( STVH(I,J,K), I=1,NORD ), J=1,MAXT ),
     &                                          K=1,MAXD )
            IF (NMET.GT.1) READ(IUNIT,1005)
     &                       ( ( ( STVHM(I,J,K), I=2,NMET ), J=1,MAXT ),
     &                                           K=1,MAXD )
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(A80)
 1001 FORMAT(A3,I2,2I10,F15.0)
 1002 FORMAT(16I5)
 1003 FORMAT(I5,1X,A18,1X,I1,1X,I1,1X,F4.1)
 1004 FORMAT(8E10.2)
 1005 FORMAT(6E12.4)
 1006 FORMAT(1X,32('*'),' B7DATA ERROR ',32('*')//
     & 1X,'FAULT IN INPUT DATA FILE: CONTOUR DATA SET CONTAINS > ',I3,A)
 1007 FORMAT(/1X,26('*'),' PROGRAM TERMINATED ',29('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
