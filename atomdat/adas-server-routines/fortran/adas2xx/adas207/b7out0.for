CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7out0.for,v 1.2 2004/07/06 11:27:33 whitefor Exp $ Data $Date: 2004/07/06 11:27:33 $
CX
      SUBROUTINE B7OUT0( IUNIT  , DATE   , DSNP44 , DSNC44 ,
     &                   TITLED , IZ     , IZ0    , IZ1    , BWNO ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA  , WA ,
     &                   ICNTR  , ICNTH  ,
     &                   NMET   , IMETR  , LMETR  , STRGA  ,
     &                   MAXT   , TEA    ,
     &                   MAXD   , DENSA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7OUT0 *********************
C
C  PURPOSE: TO OUTPUT ION SPECIFICATIONS, INDEXED ENERGY LEVELS ,
C           WAVE NUMBERS RELATIVE  TO GROUND AND OTHER DATA STORED
C           IN THE CONTOUR PASSING FILE. OUTPUT TO STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = OUTPUT STREAM NUMBER
C  INPUT : (C*8)  DATE     = CURRENT DATE AS 'DD/MM/YY'
CX UNIX/IDL PORT - MAKE FILE NAME VARIABLES 80 CHARACTERS
CX  INPUT : (C*44) DSNP44   = INPUT CONTOUR-PASSING FILE DSN
CX  INPUT : (C*44) DSNC44   = ASSOCIATED COPASE FILE DSN
CX INPUT : (C*80) DSNP44   = INPUT CONTOUR-PASSING FILE DSN
CX INPUT : (C*80) DSNC44   = ASSOCIATED COPASE FILE DSN
C
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  INPUT : (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS
C  INPUT : (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS
C
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  INPUT : (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                      'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C
C          (R*8)  WN2RYD  = PARAMETER =
C                           WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C
C          (R*8)  R8DCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  R8TCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  BRYDO   = IONISATION POTENTIAL (RYDBERGS)
C          (R*8)  BWN     = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           WAVE NUMBERS (CM-1).
C          (R*8)  BRYD    = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           RYDBERGS.
C          (R*8)  ERVAL   = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C          (R*8)  TEV     = ELECTRON TEMPERATURE (eV)
C          (R*8)  TRED    = ELECTRON TEMPERATURE (reduced)
C          (R*8)  DRED    = ELECTRON DENSITY (reduced)
C
C          (I*4)  I       = GENERAL USE
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          R8DCON     ADAS      FUNCTION - DENSITY UNIT CONVERSION
C          R8TCON     ADAS      FUNCTION - TEMPERATURE UNIT CONVERSION
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  17/01/91 - PE BRIDEN: ADDED HEADER INFORMATION TO OUTPUT
C                              - RENAMED SUBROUTINE (ORIGINALLY B7WR7A)
C
C UPDATE:  28/01/91 - PE BRIDEN: REFORMATTED OUTPUT. INTRODUCED 'WN2RYD'
C                                'BRYD' & 'BRYDO'. RENAMED 'BW' -> 'BWN'
C
C UPDATE:  29/01/91 - PE BRIDEN: SET 'CADAS' TO BLANK AT START (VIA DATA
C                                STATEMENT) AND ADDED 'SAVE CADAS'.
C
C UPDATE:  28/01/94 - PE BRIDEN: ADAS91 - INCREASED CSTRGA C*12 -> C*18
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 30-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 09-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED OUTDATED HOLLERITH CODES WITH SPACES
C
C-----------------------------------------------------------------------
      INTEGER    L1            , L2             , L3
C-----------------------------------------------------------------------
      REAL*8     WN2RYD
C-----------------------------------------------------------------------
      PARAMETER( L1 = 1        , L2 = 2         , L3 = 3     )
C-----------------------------------------------------------------------
      PARAMETER( WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
      INTEGER    I
      INTEGER    IUNIT         , ICNTR          , ICNTH      ,
     &           IZ            , IZ0            , IZ1        ,
     &           IL            , NMET           ,
     &           MAXT          , MAXD
C-----------------------------------------------------------------------
      REAL*8     R8DCON        , R8TCON
      REAL*8     BWNO          , BWN            , BRYDO      , BRYD   ,
     &           ERVAL         , TEV            , TRED       , DRED
C-----------------------------------------------------------------------
CX UNIX/IDL PORT - MAKE FILE NAMES 80 CHARACTERS
      CHARACTER  TITLED*3      , DATE*8         ,
CX     &           DSNP44*44     , DSNC44*44      , CADAS*80
     &           DSNP44*80     , DSNC44*80      , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    IA(IL)        , ISA(IL)        , ILA(IL)
      INTEGER    IMETR(NMET)
C-----------------------------------------------------------------------
      REAL*8     XJA(IL)       , WA(IL)         ,
     &           TEA(MAXT)     , DENSA(MAXD)
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18 , STRGA(IL)*22
C-----------------------------------------------------------------------
      LOGICAL    LMETR(IL)
C-----------------------------------------------------------------------
      SAVE       CADAS
C-----------------------------------------------------------------------
      DATA       CADAS/' '/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      BRYDO = WN2RYD * BWNO
C
C-----------------------------------------------------------------------
C GATHER ADAS HEADER AND WRITE IT OUT
C-----------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
      WRITE(IUNIT,1000) CADAS(2:80)
C
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1001) 'SPECTRUM LINE DIAGNOSTIC','ADAS207',DATE
      WRITE(IUNIT,1002) DSNP44,DSNC44
      WRITE(IUNIT,1003) TITLED , IZ , IZ0 , IZ1 , BWNO , BRYDO
      WRITE(IUNIT,1004)
C
         DO 1 I=1,IL
            ERVAL = WN2RYD * WA(I)
            BWN   = BWNO   - WA(I)
            BRYD  = BRYDO  - ERVAL
            WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                        ISA(I) , ILA(I)    , XJA(I) ,
     &                        WA(I)  , ERVAL     ,
     &                        BWN    , BRYD
    1    CONTINUE
C-----------------------------------------------------------------------
       WRITE(IUNIT,1006) (IL - NMET) , NMET
C-----------------------------------------------------------------------
          DO 2 I=1,NMET
             IF ( LMETR(I) ) THEN
                WRITE(IUNIT,1007) I , IMETR(I) , STRGA(IMETR(I))
             ELSE
                WRITE(IUNIT,1008) I , IMETR(I) , STRGA(IMETR(I))
             ENDIF
    2     CONTINUE
C-----------------------------------------------------------------------
          IF ( ICNTH.LE.0 ) THEN
             WRITE(IUNIT,1009)
          ELSE
             WRITE(IUNIT,1010) ICNTH
          ENDIF
C-----------------------------------------------------------------------
          IF ( ICNTR.LE.0 ) THEN
             WRITE(IUNIT,1011)
          ELSE
             WRITE(IUNIT,1012) ICNTR
          ENDIF
C-----------------------------------------------------------------------
      WRITE(IUNIT,1013) MAXT , MAXD
      WRITE(IUNIT,1014)
      WRITE(IUNIT,1015)
C
         DO 3 I=1,MAXT
            TEV  = R8TCON( L1 , L2 , IZ1 , TEA(I) )
            TRED = R8TCON( L1 , L3 , IZ1 , TEA(I) )
               IF (I.LE.MAXD) THEN
                  DRED = R8DCON( L1 , L2 , IZ1 , DENSA(I) )
                  WRITE(IUNIT,1016) I , TEA(I)   , TEV      , TRED   ,
     &                              I , DENSA(I) , DRED
               ELSE
                  WRITE(IUNIT,1017) I , TEA(I)   , TEV      , TRED
               ENDIF
    3    CONTINUE
C
         IF (MAXD.GT.MAXT) THEN
            DO 4 I=MAXT+1,MAXD
               DRED = R8DCON( L1 , L2 , IZ1 , DENSA(I) )
               WRITE(IUNIT,1018) I , DENSA(I) , DRED
    4       CONTINUE
         ENDIF
C
      WRITE(IUNIT,1015)
      WRITE(IUNIT,1019)
C-----------------------------------------------------------------------
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,26('*'),' TABULAR OUTPUT FROM ',A24,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,26('*')/)
CX UNIX/IDL PORT - ALTER FORMAT FOR 80 CHARACTER FILE NAMES
CX 1002 FORMAT(1H ,'INPUT CONTOUR-PASSING FILE DSN: ',A44/
CX     &        1X,'ASSOCIATED   COPASE   FILE DSN: ',A44/)
 1002 FORMAT(1H ,'INPUT CONTOUR-PASSING FILE DSN: ',A/
     &        1X,'ASSOCIATED   COPASE   FILE DSN: ',A/)
 1003 FORMAT(1H ,1X,'ION',6X,'NUCLEAR',8X,'RECOMBINING',6X,
     &           7('-'),' IONIZATION POTENTIAL ',7('-')/
     &           9X,'CHARGE (Z0)',4X,'ION CHARGE (Z1)',
     &        4X,'(wave number <cm-1>)',5X,'(rydbergs)'/
     &        1X,78('-')/
     &        1X,1A3,I2,7X,I3,14X,I3,10X,F15.0,5X,F15.7//)
 1004 FORMAT(1H ,55('-'),' ENERGY LEVELS ',55('-')/
     &        2X,'INDEX',4X,'CONFIGURATION',3X,'(2S+1)L(J)',7X,
     &           5('-'),' ENERGY RELATIVE TO LEVEL 1 ',6('-'),4X,
     &           'ENERGY RELATIVE TO IONISATION POTENTIAL'/
     &       45X,2( '(wave number <cm-1>)',5X,'(rydbergs)' ,8X )/
     &        1X,125('-'))
 1005 FORMAT( 1X,I4,3X,A18,1X,'(',I1,')',I1,'(',F4.1,')',
     &        2(8X,F15.0,5X,F15.7) )
 1006 FORMAT(1H /1H ,'METASTABLE INFORMATION:'/1H ,23('-')/
     &       1H ,'NUMBER OF ORDINARY LEVELS',8X,'=',1X,I3/
     &       1H ,'NUMBER OF METASTABLES'   ,12X,'=',2X,I2/
     &       1H ,16('-'),' METASTABLE DETAILS ',16('-')/
     &       1H ,'METASTABLE',4X,'ENERGY LEVEL',4X,
     &           4('-'),' DESIGNATION ',5('-')/
     &        3X,'INDEX',10X,'INDEX'/1X,52('-'))
 1007 FORMAT(1H ,3X,I2,12X,I3,10X,1A22)
 1008 FORMAT(1H ,I4,7X,I4,12X,1A22,4X,
     & '***** NO ELECTRON IMPACT TRANSITIONS EXIST TO THIS LEVEL *****')
 1009 FORMAT(1H /1H ,
     & '***** NEUTRAL HYDROGEN CHARGE EXCHANGE  NOT INCLUDED *****')
 1010 FORMAT(1H /1H ,
     & 'NUMBER OF NEUTRAL HYDROGEN CHARGE EXCHANGES INCLUDED = ',I3)
 1011 FORMAT(1H ,
     & '*****   FREE ELECTRON RECOMBINATION  NOT  INCLUDED   *****')
 1012 FORMAT(1H ,
     & 'NUMBER OF FREE ELECTRON RECOMBINATIONS INCLUDED      = ',I3)
 1013 FORMAT(1H /1H ,'INPUT PLASMA PARAMETERS:'/1H ,24('-')//
     &       1H ,'NUMBER OF ELECTRON TEMPERATURES = ',I2/
     &       1H ,'NUMBER OF ELECTRON DENSITIES    = ',I2)
 1014 FORMAT(1H ,'INDEX',3X,8('-'),' TE:  ELECTRON TEMPERATURES ',
     &           8('-'),10X,'INDEX',3X,'-- NE: ELECTRON DENSITIES --'/
     &       11X,'(kelvin)',9X,'(eV)',9X,'(reduced)',24X,
     &           '(cm-3)',7X,'(reduced)')
 1015 FORMAT(1H ,52('-'),10X,36('-'))
 1016 FORMAT( 2X,I2,5X,1P,3(D11.3,4X),10X,I2,5X,2(D11.3,3X))
 1017 FORMAT( 2X,I2,5X,1P,3(D11.3,4X))
 1018 FORMAT(64X,I2,5X,1P,2(D11.3,3X))
 1019 FORMAT(1H /1H ,'TABLE KEY:'/1H ,10('-')/
     &       1H ,'NE    = ELECTRON DENSITY'/
     &       1H ,'TE    = ELECTRON TEMPERATURE')
C-----------------------------------------------------------------------
      RETURN
      END
