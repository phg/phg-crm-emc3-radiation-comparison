CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7lrat.for,v 1.2 2007/07/20 09:46:32 allan Exp $ Data $Date: 2007/07/20 09:46:32 $
CX
      SUBROUTINE B7LRAT( EM1    , EM2    ,
     &                   RMIN   , RMAX   ,
     &                   RAT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7LRAT *********************
C
C  PURPOSE: TO CALCULATE THE SPECTRUM-LINE INTENSITY RATIO FOR TWO
C           COMPOSITE LINES FROM THEIR INDIVIDUAL SPECTRUM LINE
C           INTENSITIES.
C
C           INTENSITIES AT FIXED TEMPERATURE AND DENSITY. 'RMIN'
C           AND 'RMAX' CONTAIN MINIMUM AND MAXIMUM RATIO VALUES.
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (R*8)  EM1     = FIRST COMPOSITE ASSEMBLY SPECTRUM-LINE
C                           INTENSITY, AT FIXED TEMPERATURE & DENSITY.
C  INPUT : (R*8)  EM2     = SECOND COMPOSITE ASSEMBLY SPECTRUM-LINE
C                           INTENSITY, AT FIXED TEMPERATURE & DENSITY.
C
C  I/O   : (R*8)  RMIN    = MINIMUM SPECTRUM-LINE INTENSITY RATIO VALUE
C  I/O   : (R*8)  RMAX    = MAXIMUM SPECTRUM-LINE INTENSITY RATIO VALUE
C
C  OUTPUT: (R*8)  RAT     = SPECTRUM-LINE INTENSITY RATIO
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C VERSION: 1.2                                            
C DATE: 20-07-07
C MODIFIED: Allan Whiteford
C           - Small modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
      REAL*8    EM1          , EM2
      REAL*8    RMIN         , RMAX        ,
     &          RAT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
         IF     (EM1.LE.0.0D0) THEN
            RAT=EM2
         ELSEIF (EM2.LE.0.0D0) THEN
            RAT=EM1
         ELSE
            RAT=EM1/EM2
         ENDIF
C-----------------------------------------------------------------------
      IF (RAT.LT.RMIN) RMIN=RAT
      IF (RAT.GT.RMAX) RMAX=RAT
C-----------------------------------------------------------------------
      RETURN
      END
