CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7ctyp.for,v 1.1 2004/07/06 11:26:27 whitefor Exp $ Data $Date: 2004/07/06 11:26:27 $
CX
      SUBROUTINE B7CTYP( NSTRN  , ISTRN  ,
     &                   ICNTE  , NMET   , NORD   ,
     &                   IE2A   , IMETR  , IORDR  ,
     &                   AVAL   ,
     &                   KSTRN  , LSTRN  , ASTRN
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7CTYP *********************
C
C  PURPOSE: TO IDENTIFY COMPOSITE LINE UPPER-LEVEL-INDEXES AS EITHER
C           ORDINARY OR METASTABLE LEVELS AND TO FIND THEIR ORDINARY
C           OR METASTABLE LEVEL INDEXES. ALSO IDENTIFIES COMPOSITE
C           LINE A-VALUES.
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NSTRN   = NO. OF LINES CHOSEN FOR COMPOSITE ASSEMBLY
C  INPUT : (I*4)  ISTRN() = SELECTED TRANSITION INDEXES FOR COMPOSITE
C                             LINE ASSEMBLY
C
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C
C  INPUT : (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C  INPUT : (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C
C  INPUT : (I*4)  AVAL()  = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C
C  OUTPUT: (I*4)  KSTRN() = ORDINARY/METASTABLE LEVEL INDEX OF UPPER
C                           LEVEL OF COMPOSITE LINE TRANSITION (SEE:
C                                                             'LSTRN()')
C  OUTPUT: (L*4)  LSTRN() = .TRUE.  => COMPOSITE LINE IS METASTABLE
C                           .FALSE. => COMPOSITE LINE IS ORDINARY
C  OUTPUT: (R*8)  ASTRN() = COMPOSITE LINE A-VALUE (SEC-1)
C
C          (I*4)  IULEV   = COMPOSITE LINE UPPER-LEVEL INDEX
C          (I*4)  I       = COMPOSITE LINE ARRAY INDEX
C          (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C          (I*4)  IO      = ORDINARY LEVEL ARRAY INDEX
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
       INTEGER   NSTRN        , ICNTE        , NMET        , NORD
       INTEGER   I            , IM           , IO          , IULEV
C-----------------------------------------------------------------------
       INTEGER   ISTRN(NSTRN) , IE2A(ICNTE)  , IMETR(NMET) , IORDR(NORD)
       INTEGER   KSTRN(NSTRN)
C-----------------------------------------------------------------------
       REAL*8    ASTRN(NSTRN) , AVAL(ICNTE)
C-----------------------------------------------------------------------
       LOGICAL   LSTRN(NSTRN)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
         DO 1 I=1,NSTRN
C-----------------------------------------------------------------------
            IULEV    = IE2A(ISTRN(I))
            ASTRN(I) = AVAL(ISTRN(I))
            LSTRN(I) = .FALSE.
            KSTRN(I) = 0
C-----------------------------------------------------------------------
               DO 2 IM=1,NMET
                  IF (IULEV.EQ.IMETR(IM)) THEN
                      LSTRN(I)=.TRUE.
                      KSTRN(I)=IM
                  ENDIF
    2          CONTINUE
C-----------------------------------------------------------------------
               IF (.NOT.LSTRN(I)) THEN
                  DO 3 IO=1,NORD
                     IF (IULEV.EQ.IORDR(IO)) KSTRN(I)=IO
    3            CONTINUE
               ENDIF
C-----------------------------------------------------------------------
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
