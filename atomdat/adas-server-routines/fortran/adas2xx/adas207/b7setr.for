CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7setr.for,v 1.1 2004/07/06 11:28:05 whitefor Exp $ Data $Date: 2004/07/06 11:28:05 $
CX
       SUBROUTINE B7SETR( RMIN , RMAX )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7SETR *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL
C            (SETS UP MINIMIMUM AND MAXIMUM OF SPEXTRUM LINE RATIO)
C
C  CALLING PROGRAM: ADAS207
C
C
C  SUBROUTINE:
C
C  INPUT : (R*8)  RMIN    = MINIMUM SPECTRUM LINE INTENSITY RATIO VALUE
C  INPUT : (R*8)  RMAX    = MAXIMUM SPECTRUM LINE INTENSITY RATIO VALUE
C
CX          (C*8)  F6      = PARAMETER = 'VREPLACE'
C
CX          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C
C          (C*10) SRMIN   = MINIMUM SPECTRUM LINE INTENSITY RATIO VALUE
C          (C*10) SRMAX   = MAXIMUM SPECTRUM LINE INTENSITY RATIO VALUE
CX          (C*8)  CHA()   = FUNCTION POOL NAMES
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  09/03/95 - UNIX:   SP BELLAMY - PIPE COMMUNICATION WITH IDL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
CX      CHARACTER F6*8
C-----------------------------------------------------------------------
CX      PARAMETER ( F6     = 'VREPLACE'     )
C-----------------------------------------------------------------------
CX      INTEGER   ILEN
C-----------------------------------------------------------------------
      REAL*8    RMIN          , RMAX
C-----------------------------------------------------------------------
      CHARACTER SRMIN*10      , SRMAX*10
CX      CHARACTER CHA(2)*8
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
CX      SAVE      CHA
C-----------------------------------------------------------------------
CX      DATA CHA   / '(SRMIN) ' , '(SRMAX) ' /
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      WRITE(SRMIN ,1000) RMIN
      WRITE(SRMAX ,1000) RMAX
C
C-----------------------------------------------------------------------
C SEND STRINGS TO ISPF FUNCTION POOL
C-----------------------------------------------------------------------
C
CX      ILEN=10
CX      CALL ISPLNK( F6 , CHA(1) , ILEN , SRMIN )
      WRITE( PIPEOU, '(A10)' ) SRMIN
      CALL XXFLSH( PIPEOU )

CX      ILEN=10
CX      CALL ISPLNK( F6 , CHA(2) , ILEN , SRMAX )
      WRITE( PIPEOU, '(A10)' ) SRMAX
      CALL XXFLSH( PIPEOU )

C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1PD10.4)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
