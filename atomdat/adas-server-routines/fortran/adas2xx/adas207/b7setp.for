CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7setp.for,v 1.1 2004/07/06 11:27:59 whitefor Exp $ Data $Date: 2004/07/06 11:27:59 $
CX
       SUBROUTINE B7SETP( DSN44  ,
     &                    IZ0    , IZ     ,
     &                    TITLED , BWNO   ,
     &                    NDLEV  , IL     , ICNTE  , ICNTH , ICNTR ,
     &                    NDMET  , NMET   , IMETR  ,
     &                    NDDEN  , MAXD   , DENSA  ,
     &                    NDTEM  , MAXT   , TEA    ,
     &                    CSTRGA , ISA    , ILA    , XJA   ,
     &                    MTRGA  , STRGA  , STRGM
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7SETP *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS207
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'B7DATA' & 'B7DATC'
C
C  SUBROUTINE:
C
C  INPUT : (C*44) DSN44   = DATA SET NAME OF ASSOCIATED COPASE FILE
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (WAVE NUMBER) (CM-1)
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)  ICNTH   = NUMBER OF CHARGE EXCHANGE RECOMBINATIONS
C  INPUT : (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS
C
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= NDMET)
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET')
C
C  INPUT : (I*4)  NDDEN   = MAXIUMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> NDDEN )
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C
C  INPUT : (I*4)  NDTEM   = MAXIUMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> NDTEM )
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES  (UNITS: KELVIN)
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (C*1)  MTRGA() = 'M' => METASTABLE LEVEL
C                           ' ' => ORDINARY   LEVEL
C                           DIMENSION: LEVEL INDEX
C  OUTPUT: (C*22) STRGA() = LEVEL DESIGNATIONS
C                           DIMENSION: LEVEL INDEX
C  OUTPUT: (C*22) STRGM() = METASTABLE LEVEL DESIGNATIONS
C                           DIMENSION: METASTABLE LEVEL INDEX
C
C          (C*1)  C1H     = PARAMETER = 'H'
C          (C*1)  C1M     = PARAMETER = 'M'
C          (C*1)  C1R     = PARAMETER = 'R'
C          (C*8)  F6      = PARAMETER = 'VREPLACE'
C          (C*12) CRATHA  = PARAMETER = DEFAULT VALUE FOR (NEUTRAL H
C                                       DENSITY/ELECTRON DENSITY) RATIO
C          (C*12) CRATIA  = PARAMETER = DEFAULT VALUE FOR (STAGE ABUND-
C                                       ANCIES) RATIO.
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)  ILVAL   = VALUE OF QUANTUM NUMBER L + 1
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  IM      = ARRAY COUNTER FOR METASTABLE LEVEL INDEX
C          (I*4)  IT      = ARRAY COUNTER FOR TEMPERATURE INDEX
C          (I*4)  IN      = ARRAY COUNTER FOR DENSITY INDEX
C
C          (C*3)  C3      = GENERAL USE 3 BYTE CHARACTER STRING
C          (C*12) C12     = GENERAL USE 12 BYTE CHARACTER STRING
C          (C*22) C22     = GENERAL USE 22 BYTE CHARACTER STRING
C          (C*2)  SZ0     =         NUCLEAR CHARGE READ
C          (C*2)  SZ      =  RECOMBINED ION CHARGE READ
C          (C*10) SIONPT  =  IONISATION POTENTIAL (UNITS: WAVE NUMBERS)
C          (C*3)  SCNTE   =  NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (C*3)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*2)  SRAT1   = '  ' => NO CHARGE EXCHANGE OR FREE ELECTRON
C                                   RECOMBINATIONS
C                           'H ' => CHARGE EXCHANGE RECOMBINATIONS BUT
C                                   NO FREE ELECTRON RECOMBINATIONS
C                           ' R' => FREE ELECTRON RECOMBINATIONS BUT
C                                   NO CHARGE EXCHANGE RECOMBINATIONS
C                           'HR' => CHARGE EXCHANGE AND FREE ELECTRON
C                                   RECOMBINATIONS
C          (C*1)  CONFIG()= QUANTUM NUMBER (L) LETTERS
C                           DIMENSION: QUANTUM NUMBER L + 1
C          (C*8)  CHA()   = FUNCTION POOL NAMES: CHARGE VALUES ETC.
C          (C*8)  CHB()   = FUNCTION POOL NAMES: LEVEL DESIGNATIONS
C          (C*43) DSN43   = DSN OF ASSOCIATED COPASE FILE IGNORING FIRST
C                           BYTE ('/').
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  28/01/94 - ADAS91: PE BRIDEN - INCREASED CSTRGA C*12 -> C*18
C
C UPDATE:  09/03/95 - UNIX:   SP BELLAMY - PIPE COMMUNICATION WITH IDL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER C1H*1         , C1M*1         , C1R*1
      CHARACTER F6*8
      CHARACTER CRATHA*12     , CRATIA*12
C-----------------------------------------------------------------------
      PARAMETER ( C1H    = 'H'            , C1M    = 'M'            ,
     &            C1R    = 'R'            )
      PARAMETER ( F6     = 'VREPLACE'     )
      PARAMETER ( CRATHA = '1.0E-03     ' , CRATIA = '1.0E-04     ' )
C-----------------------------------------------------------------------
      INTEGER   NDLEV         , NDMET         , NDDEN    , NDTEM
      INTEGER   IZ0           , IZ            ,
     &          IL            , NMET          , MAXD     , MAXT     ,
     &          ICNTE         , ICNTH         , ICNTR
      INTEGER   ILEN          , ILVAL
      INTEGER   ILEV          , IM            , IT       , IN
      INTEGER   ISA(IL)       , ILA(IL)       , IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8    BWNO
      REAL*8    XJA(IL)       , DENSA(NDDEN)  , TEA(NDTEM)
C-----------------------------------------------------------------------
      CHARACTER C3*3          , C12*12        , C22*22
      CHARACTER TITLED*3
      CHARACTER SZ0*2         , SZ*2          , SRAT1*2      , SCNTE*3 ,
     &          SIL*3         , SIONPT*10     , DSN43*80     ,
     &          DSN44*80      , CONFIG(20)*1
      CHARACTER CHA(8)*8      , CHB(6)*8
      CHARACTER MTRGA(NDLEV)*1
      CHARACTER CSTRGA(IL)*18 , STRGA(NDLEV)*22 , STRGM(NDMET)*22
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      SAVE      CHA            , CHB
C-----------------------------------------------------------------------
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
      DATA CHA   / '(SION)  ' , '(SZ)    ' , '(SZ0)   ' , '(SIONPT)' ,
     &             '(SCNTE) ' , '(SIL)   ' , '(SRAT1) ' , '(SDSN10)' /
      DATA CHB   / '(SMI**) ' , '(SMD**) ' , '(STE**) ' , '(SDE**) ' ,
     &             '(SDH**) ' , '(SNZ**) ' /
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      WRITE(SZ    ,1000) IZ
      WRITE(SZ0   ,1000) IZ0
      WRITE(SIONPT,1001) BWNO
      WRITE(SCNTE ,1002) ICNTE
      WRITE(SIL   ,1002) IL
      SRAT1='  '
      IF ( ICNTH.GT.0 ) SRAT1(1:1) = C1H
      IF ( ICNTR.GT.0 ) SRAT1(2:2) = C1R
CX      DSN43=DSN44(2:44)
      DSN43=DSN44
         DO 1 ILEV=1,NDLEV
            MTRGA(ILEV)=' '
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1003)
     &             CSTRGA(ILEV)(1:12),ISA(ILEV),CONFIG(ILVAL),XJA(ILEV)
               ENDIF
    1    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C SEND STRINGS TO ISPF FUNCTION POOL
C-----------------------------------------------------------------------
C
CX      ILEN=3
CX      CALL ISPLNK( F6 , CHA(1) , ILEN , TITLED  )
      WRITE( PIPEOU, '(A3)' ) TITLED
      CALL XXFLSH( PIPEOU )

CX      ILEN=2
CX      CALL ISPLNK( F6 , CHA(2) , ILEN , SZ      )
      WRITE( PIPEOU, '(A2)' ) SZ
      CALL XXFLSH( PIPEOU )

CX      ILEN=2
CX      CALL ISPLNK( F6 , CHA(3) , ILEN , SZ0     )
      WRITE( PIPEOU, '(A2)' ) SZ0
      CALL XXFLSH( PIPEOU )

CX      ILEN=10
CX      CALL ISPLNK( F6 , CHA(4) , ILEN , SIONPT  )
      WRITE( PIPEOU, '(A10)' ) SIONPT
      CALL XXFLSH( PIPEOU )

CX      ILEN=3
CX      CALL ISPLNK( F6 , CHA(5) , ILEN , SCNTE   )
      WRITE( PIPEOU, '(A3)' ) SCNTE
      CALL XXFLSH( PIPEOU )

CX      ILEN=3
CX      CALL ISPLNK( F6 , CHA(6) , ILEN , SIL     )
      WRITE( PIPEOU, '(A3)' ) SIL
      CALL XXFLSH( PIPEOU )

CX      ILEN=2
CX      CALL ISPLNK( F6 , CHA(7) , ILEN , SRAT1   )
      WRITE( PIPEOU, '(A2)' ) SRAT1
      CALL XXFLSH( PIPEOU )

CX      ILEN=43
CX      CALL ISPLNK( F6 , CHA(8) , ILEN , DSN43   )
      WRITE( PIPEOU, '(A80)' ) DSN43
      CALL XXFLSH( PIPEOU )

C-----------------------------------------------------------------------
         WRITE( PIPEOU, * ) NDMET
         CALL XXFLSH( PIPEOU )

         DO 2 IM=1,NDMET
            WRITE(CHB(1)(5:6),1004) IM
            WRITE(CHB(2)(5:6),1004) IM
               IF (IM.LE.NMET) THEN
                  ILEV=IMETR(IM)
                  MTRGA(ILEV)=C1M
                  STRGM(IM)=STRGA(ILEV)
                  WRITE(C3,1002) ILEV
                  C22=STRGA(ILEV)
               ELSE
                  STRGM(IM)=' '
                  C3=' '
                  C22=' '
               ENDIF

CX            ILEN=3
CX            CALL ISPLNK( F6 , CHB(1) , ILEN , C3   )
            WRITE( PIPEOU, '(A3)' ) C3
            CALL XXFLSH( PIPEOU )

CX            ILEN=22
CX            CALL ISPLNK( F6 , CHB(2) , ILEN , C22  )
            WRITE( PIPEOU, '(A22)' ) C22
            CALL XXFLSH( PIPEOU )

    2    CONTINUE
C-----------------------------------------------------------------------
         WRITE( PIPEOU, * ) NDTEM
         CALL XXFLSH( PIPEOU )

         DO 3 IT=1,NDTEM
            WRITE(CHB(3)(5:6),1004) IT
               IF (IT.LE.MAXT) THEN
                  WRITE(C12,1005) TEA(IT)
               ELSE
                  C12=' '
               ENDIF

CX            ILEN=12
CX            CALL ISPLNK( F6 , CHB(3) , ILEN , C12  )
            WRITE( PIPEOU, '(A12)' ) C12
            CALL XXFLSH( PIPEOU )

    3    CONTINUE
C-----------------------------------------------------------------------
         WRITE( PIPEOU, * ) NDDEN
         CALL XXFLSH( PIPEOU )

         DO 4 IN=1,NDDEN
            WRITE(CHB(4)(5:6),1004) IN
            WRITE(CHB(5)(5:6),1004) IN
            WRITE(CHB(6)(5:6),1004) IN
               IF (IN.LE.MAXD) THEN
                  WRITE(C12,1005) DENSA(IN)
               ELSE
                  C12=' '
               ENDIF

CX            ILEN=12
CX            CALL ISPLNK( F6 , CHB(4) , ILEN , C12    )
            WRITE( PIPEOU, '(A12)' ) C12
            CALL XXFLSH( PIPEOU )

CX            ILEN=12
CX            CALL ISPLNK( F6 , CHB(5) , ILEN , CRATHA )
            WRITE( PIPEOU, '(A12)' ) CRATHA
            CALL XXFLSH( PIPEOU )

CX            ILEN=12
CX            CALL ISPLNK( F6 , CHB(6) , ILEN , CRATIA )
            WRITE( PIPEOU, '(A12)' ) CRATIA
            CALL XXFLSH( PIPEOU )

    4    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(1PD10.4)
 1002 FORMAT(I3)
 1003 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
 1004 FORMAT(I2.2)
 1005 FORMAT(1PD12.5)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
