CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7out1.for,v 1.3 2004/07/06 11:27:43 whitefor Exp $ Data $Date: 2004/07/06 11:27:43 $
CX
      SUBROUTINE B7OUT1( IUNIT  , LEQUIL ,
     &                   NDLEV  , NDMET  , NDTEM  , NDDEN  ,
     &                            NMET   , MAXT   , MAXD   ,
     &                            IMETR  , TEA    , DENSA  ,
     &                            XMMULT ,
     &                   ICNTE  , ICNTR  , ICNTH  ,
     &                   IE1A   , IE2A   ,
     &                   STRGA  , RATHA  , RATIA  ,
     &                   NSTRN1 , ISTRN1 , LSTRN1 , ASTRN1 ,
     &                   EM1    ,
     &                   NSTRN2 , ISTRN2 , LSTRN2 , ASTRN2 ,
     &                   EM2    ,
     &                   RAT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B7OUT1 *******************
C
C  PURPOSE: OUTPUT OF MAIN RESULTS (SPECTRUM LINE INTENSITIES & RATIOS)
C
C  CALLING PROGRAM: ADAS207
C
C  DATA:
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           TEMPERATURES        : KELVIN
C           DENSITIES           : CM-3
C           A-VALUES            : SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LEQUIL  = .TRUE.  =>     EQUILIBRIUM CONDITIONS
C                         = .FALSE. => NON-EQUILIBRIUM CONDITIONS
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLE LEVELS (1<=NMET<=NDMET)
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> NDTEM)
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> NDDEN )
C
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C
C  INPUT : (R*8)  XMMULT()= METASTABLE LEVEL SCALING FACTORS
C
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT : (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT : (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT : (I*4)  IE1A()   = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)  IE2A()   = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C  INPUT : (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  INPUT : (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C
C  INPUT : (I*4)  NSTRN1  = NO. OF LINES CHOSEN FOR FIRST COMPOSITE
C                           ASSEMBLY
C  INPUT : (I*4)  ISTRN1()= SELECTED TRANSITION INDEXES FOR FIRST
C                           COMPOSITE LINE ASSEMBLY
C  INPUT : (L*4)  LSTRN1()= FIRST COMPOSITE LINE ASSEMBLY:
C                           .TRUE.  => COMPOSITE LINE IS METASTABLE
C                           .FALSE. => COMPOSITE LINE IS ORDINARY
C  INPUT : (R*8)  ASTRN1()= COMPOSITE LINE A-VALUE (SEC-1) FOR FIRST
C                           COMPOSITE LINE ASSEMBLY
C
C  INPUT : (R*8)  EM1(,)  = SPECTRUM-LINE INTENSITY FOR FIRST COMPOSITE
C                           ASSEMBLY AT GIVEN TEMPERATURE AND DENSITY.
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C
C  INPUT : (I*4)  NSTRN2  = NO. OF LINES CHOSEN FOR SECOND COMPOSITE
C                           ASSEMBLY
C  INPUT : (I*4)  ISTRN2()= SELECTED TRANSITION INDEXES FOR SECOND
C                           COMPOSITE LINE ASSEMBLY
C  INPUT : (L*4)  LSTRN2()= SECOND COMPOSITE LINE ASSEMBLY:
C                           .TRUE.  => COMPOSITE LINE IS METASTABLE
C                           .FALSE. => COMPOSITE LINE IS ORDINARY
C  INPUT : (R*8)  ASTRN2()= COMPOSITE LINE A-VALUE (SEC-1) FOR SECOND
C                           COMPOSITE LINE ASSEMBLY
C
C  INPUT : (R*8)  EM2(,)  = SPECTRUM-LINE INTENSITY FOR SECOND COMPOSITE
C                           ASSEMBLY AT GIVEN TEMPERATURE AND DENSITY.
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C
C  INPUT : (R*8)  RAT(,)  = SPECTRUM LINE INTENSITY RATIOS:
C                           1st DIMENSION = ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION = ELECTRON DENSITY INDEX
C                           ( = EM1(,) / EM2(,) )
C
C          (I*4)  PGLEN   = PARAMETER = NUMBER OF LINES PER OUTPUT PAGE
C
C          (C*3)  CMET    = PARAMETER = 'MET'  (METASTABLE LEVEL)
C          (C*3)  CORD    = PARAMETER = 'ORD'  (ORDINARY LEVEL)
C          (C*6)  C1ST    = PARAMETER = 'FIRST '
C          (C*6)  C2ND    = PARAMETER = 'SECOND'
C
C          (I*4)  NBLOCK  = NUMBER OF LINES IN CURRENT OUTPUT BLOCK.
C          (I*4)  NLINES  = LAST PAGE LINE WRITTEN.
C                           IF 'NLINES+NBLOCK' > 'PGLEN' START NEW PAGE.
C          (I*4)  ISTOP   = NUMBER OF OUTPUT DENSITY BLOCKS REQUIRED
C                           FOR SPECTRUM LINE INTENSITY RESULTS.
C                         = 1 => 'MAXD' < 11
C                         = 2 => 'MAXD' > 10
C          (I*4)  IB      = OUTPUT BLOCK INDEX NUMBER FOR ARRAY USE
C          (I*4)  IC      = COMPOSITE ASSEMBLY LINE NUMBER FOR ARRAY USE
C          (I*4)  IM      = METASTABLE LEVEL NUMBER FOR ARRAY USE
C          (I*4)  IN      = DENSITY INDEX NUMBER FOR ARRAY USE
C          (I*4)  IT      = TEMPERATURE INDEX NUMBER FOR ARRAY USE
C
C          (C*3)  C3LEV   = COMPOSITE ASSEMBLY LINE TYPE: 'ORD'INARY OR
C                                                         'MET'ASTABLE
C
C          (I*4)  INBGN(1)= STARTING DENSITY INDEX FOR OUTPUT BLOCK 1=1
C          (I*4)  INBGN(2)= STARTING DENSITY INDEX FOR OUTPUT BLOCK 2
C                           IF 'ISTOP=1' (I.E. 'MAXD' < 11) = NOT USED
C                           IF 'ISTOP=2' (I.E. 'MAXD' > 10) = 11
C          (I*4)  INEND(1)= FINAL DENSITY INDEX FOR OUTPUT BLOCK 1.
C                           IF 'ISTOP=1' = 'MAXD'
C                           IF 'ISTOP=2' = 10
C          (I*4)  INBGN(2)= FINAL DENSITY INDEX FOR OUTPUT BLOCK 2
C                           IF 'ISTOP=1' (I.E. 'MAXD' < 11) = NOT USED
C                           IF 'ISTOP=2' (I.E. 'MAXD' > 10) = 'MAXD'
C
C
C NOTE:
C          DENSITIES ARE OUTPUT IN BLOCKS OF TEN.
C
C          AN 'OUTPUT BLOCK' IS A SINGLE CONTAINED OUTPUT TABLE
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSTNP     ADAS      STARTS NEW PAGE IF CURRENT PAGE FULL
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  17/01/91 - PE BRIDEN: RENAMED SUBROUTINE (ORIGINALLY B7WR7B)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 30-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 09-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED HOLLERITH CONSTANTS FROM OUTPUT
C
C VERSION: 1.3                          DATE: 14-11-01
C MODIFIED: RICHARD MARTIN
C		    - MODIFIED FORMAT STATEMENT 1012 TO COPE WITH LARGER 
C			TRANSITION INDICES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   PGLEN
C-----------------------------------------------------------------------
      CHARACTER CMET*3    , CORD*3    , C1ST*6    , C2ND*6
C-----------------------------------------------------------------------
      PARAMETER ( PGLEN = 63 )
C-----------------------------------------------------------------------
      PARAMETER ( CMET = 'MET'        , CORD = 'ORD'        ,
     &            C1ST = 'FIRST '     , C2ND = 'SECOND'     )
C-----------------------------------------------------------------------
      INTEGER   IUNIT     ,
     &          NDLEV     , NDTEM     , NDDEN       , NDMET
      INTEGER   NMET      , MAXT      , MAXD        ,
     &          ICNTE     , ICNTR     , ICNTH       ,
     &          NSTRN1    , NSTRN2
      INTEGER   NBLOCK    , NLINES    , ISTOP       ,
     &          IB        , IC        , IM          , IN        , IT
C-----------------------------------------------------------------------
      LOGICAL   LEQUIL
C-----------------------------------------------------------------------
      CHARACTER C3LEV*3
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)     , IE1A(ICNTE)     , IE2A(ICNTE)
      INTEGER   ISTRN1(NSTRN1)   , ISTRN2(NSTRN2)
      INTEGER   INBGN(2)         , INEND(2)
C-----------------------------------------------------------------------
      REAL*8    XMMULT(NDMET)    ,
     &          TEA(NDTEM)       , DENSA(NDDEN)     ,
     &          RATHA(NDDEN)     , RATIA(NDDEN)
      REAL*8    ASTRN1(NSTRN1)   , ASTRN2(NSTRN2)
      REAL*8    EM1(NDTEM,NDDEN) , EM2(NDTEM,NDDEN) ,
     &          RAT(NDTEM,NDDEN)
C-----------------------------------------------------------------------
      LOGICAL   LSTRN1(NSTRN1)   , LSTRN2(NSTRN2)
C-----------------------------------------------------------------------
      CHARACTER STRGA(NDLEV)*22
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ESTABLISH OUTPUT BLOCKS FOR SPECTRUM LINE INTENSITIES / RATIOS
C-----------------------------------------------------------------------
C
         IF (MAXD.LE.10) THEN
            ISTOP=1
            INBGN(1)=1
            INEND(1)=MAXD
         ELSE
            ISTOP=2
            INBGN(1)=1
            INEND(1)=10
            INBGN(2)=11
            INEND(2)=MAXD
         ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT HEADING AND CONDITIONS
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1000)
C-----------------------------------------------------------------------
         IF (LEQUIL) THEN
            WRITE(IUNIT,1001) 'EQUILIBRIUM'
         ELSE
            WRITE(IUNIT,1001) 'NON-EQUILIBRIUM'
         ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT METASTABLE SCALING DETAILS
C-----------------------------------------------------------------------
C
      NLINES=3
      NBLOCK=NMET+4
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
C-----------------------------------------------------------------------
      WRITE(IUNIT,1002)
         DO 1 IM=1,NMET
            WRITE(IUNIT,1003) IM,IMETR(IM),STRGA(IMETR(IM)),XMMULT(IM)
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ESTABLISH WHETHER 'NEUTRAL H DENS/ELECTRON DENS'/ 'STAGE ABUND.' USED
C-----------------------------------------------------------------------
C
         IF    ( (ICNTH.GT.0) .AND. (ICNTR.GT.0) ) THEN
            NBLOCK=MAXD+6
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1005)
               DO 2 IN=1,MAXD
                  WRITE(IUNIT,1004) DENSA(IN) , RATHA(IN) , RATIA(IN)
    2          CONTINUE
            WRITE(IUNIT,1006)
C-----------------------------------------------------------------------
         ELSEIF ( ICNTH.GT.0 ) THEN
            NBLOCK=MAXD+5
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1007)
               DO 3 IN=1,MAXD
                  WRITE(IUNIT,1004) DENSA(IN) , RATHA(IN)
    3          CONTINUE
            WRITE(IUNIT,1008)
C-----------------------------------------------------------------------
         ELSEIF ( ICNTR.GT.0 ) THEN
            NBLOCK=MAXD+5
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1009)
               DO 4 IN=1,MAXD
                  WRITE(IUNIT,1004) DENSA(IN) , RATIA(IN)
    4          CONTINUE
            WRITE(IUNIT,1010)
         ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT COMPOSITE ASSEMBLY DETAILS (1st & 2nd ASSEMBLIES)
C-----------------------------------------------------------------------
C
      NBLOCK=NSTRN1+5
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
      WRITE(IUNIT,1011) C1ST
         DO 5 IC=1,NSTRN1
               IF (LSTRN1(IC)) THEN
                  C3LEV=CMET
               ELSE
                  C3LEV=CORD
               ENDIF
            IT=ISTRN1(IC)
            WRITE(IUNIT,1012) IC    , IT              , IE2A(IT) ,
     &                        C3LEV , STRGA(IE2A(IT)) , STRGA(IE1A(IT)),
     &                        ASTRN1(IC)
    5    CONTINUE
C-----------------------------------------------------------------------
      NBLOCK=NSTRN2+5
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
      WRITE(IUNIT,1011) C2ND
         DO 6 IC=1,NSTRN2
               IF (LSTRN2(IC)) THEN
                  C3LEV=CMET
               ELSE
                  C3LEV=CORD
               ENDIF
            IT=ISTRN2(IC)
            WRITE(IUNIT,1012) IC    , IT              , IE2A(IT) ,
     &                        C3LEV , STRGA(IE2A(IT)) , STRGA(IE1A(IT)),
     &                        ASTRN2(IC)
    6    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT SPECTRUM LINE INTENSITY RESULTS FOR 1st & 2nd COMPOSITE ASSMBLY
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1013)
      NLINES=1
C-----------------------------------------------------------------------
         DO 7 IB=1,ISTOP
            NBLOCK=MAXT+7
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1014) C1ST
            WRITE(IUNIT,1015) ( DENSA(IN) , IN=INBGN(IB),INEND(IB) )
            WRITE(IUNIT,1016)
               DO 8 IT=1,MAXT
                  WRITE(IUNIT,1017)   TEA(IT)   ,
     &                        ( EM1(IT,IN), IN=INBGN(IB),INEND(IB) )
    8          CONTINUE
    7    CONTINUE
C-----------------------------------------------------------------------
      WRITE(IUNIT,'()')
C-----------------------------------------------------------------------
         DO 9 IB=1,ISTOP
            NBLOCK=MAXT+7
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1014) C2ND
            WRITE(IUNIT,1015) ( DENSA(IN) , IN=INBGN(IB),INEND(IB) )
            WRITE(IUNIT,1016)
               DO 10 IT=1,MAXT
                  WRITE(IUNIT,1017)   TEA(IT)   ,
     &                        ( EM2(IT,IN), IN=INBGN(IB),INEND(IB) )
   10          CONTINUE
    9    CONTINUE
C-----------------------------------------------------------------------
      WRITE(IUNIT,'()')
C
C-----------------------------------------------------------------------
C OUTPUT SPECTRUM LINE INTENSITY RATIOS
C-----------------------------------------------------------------------
C
         DO 11 IB=1,ISTOP
            NBLOCK=MAXT+7
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1018)
            WRITE(IUNIT,1015) ( DENSA(IN) , IN=INBGN(IB),INEND(IB) )
            WRITE(IUNIT,1016)
               DO 12 IT=1,MAXT
                  WRITE(IUNIT,1017)   TEA(IT)   ,
     &                        ( RAT(IT,IN), IN=INBGN(IB),INEND(IB) )
   12          CONTINUE
   11    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1H ,41('*'),' SPECTRUM LINE INTENSITIES & RATIOS: ',
     &                   'CONTROL DATA ',41('*'))
 1001 FORMAT(1H ,'TRANSIENT CONDITIONS: ',A)
 1002 FORMAT(1H /
     &       1H ,10('-'),' METASTABLE LEVEL SCALING FACTORS ',10('-')/
     &       1H ,' INDEX  LEVEL',7X,'DESIGNATION',8X,'SCALING FACTOR'/
     &       1H ,54('-'))
 1003 FORMAT(1H ,2X,I2,5X,I2,4X,A22,4X,1PD11.4)
 1004 FORMAT(1H ,5X,1P,D11.4,11X,D11.4,4X,D11.4)
 1005 FORMAT(1H /1H ,
     &  'ELECTRON DENSITY (CM-3)',6X,'N(H)/NE',6X,'N(ION)/N(1)'/
     &  1H ,54('-'))
 1006 FORMAT(1H ,'KEY:',
     &    '  H(H)/NE   = NEUTRAL HYDROGEN DENSITY/ELECTRON DENSITY'/
     & 5X,'H(ION)/N(1) = STAGE ABUNDANCIES')
 1007 FORMAT(1H ,/1H ,
     &  'ELECTRON DENSITY (CM-3)',6X,'N(H)/NE'/1H ,38('-'))
 1008 FORMAT(1H ,'KEY:',
     &    '  H(H)/NE   = NEUTRAL HYDROGEN DENSITY/ELECTRON DENSITY')
 1009 FORMAT(1H ,1H ,
     &  'ELECTRON DENSITY (CM-3)',4X,'N(ION)/N(1)'/1H ,38('-'))
 1010 FORMAT(1H ,'KEY:',
     &    'H(ION)/N(1) = STAGE ABUNDANCIES')
 1011 FORMAT(1H /1H ,33('-'),1X,A6,' COMPOSITE LINE GROUP ASSEMBLY ',
     &           34('-')/1H ,7X,'TRANSTN',3X,'-- UPPER LEVEL --',1X,
     &           19('-'),' TRANSITION ',20('-')/
     &       1H ,'INDEX',3X,'INDEX',6X,'INDEX',4X,'TYPE',3X,5('-'),
     &           ' UPPER LEVEL ',6('-'),3X,5('-'),' LOWER LEVEL ',
     &            6('-'),4X,'A-VALUE (SEC-1)'/1H ,105('-'))
 1012 FORMAT(1H ,1X,I2,5X,I4,9X,I3,6X,A3,4X,'<',A22,'> - <',A22,'>',
     &           6X,1P,D9.2)
 1013 FORMAT(1H ,43('*'),' SPECTRUM LINE INTENSITIES & RATIOS: ',
     &                   ' RESULTS ',43('*'))
 1014 FORMAT(1H /1X,A6,
     &    ' COMPOSITE LINE GROUP ASSEMBLY - SPECTRUM LINE INTENSITIES'/
     &    1X,64('-'))
 1015 FORMAT(1H ,'NE  (CM-3) ',2X,1P,10D10.2)
 1016 FORMAT(1H ,'TE (KELVIN)',103('-'))
 1017 FORMAT(1H ,1P,D10.2,' | ',1P,10D10.2)
 1018 FORMAT(1H /1X,'SPECTRUM LINE INTENSITY RATIO'/1X,30('-'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
