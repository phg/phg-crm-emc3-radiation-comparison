CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas207/b7spf0.for,v 1.1 2004/07/06 11:28:18 whitefor Exp $ Data $Date: 2004/07/06 11:28:18 $
CX
      SUBROUTINE B7SPF0( REP , DSNPAS )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B7SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS207
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*44)  DSNPAS  = INPUT CONTOUR PASSING FILE DSN (SEQUENTIAL)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  S.P.BELLAMY (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    08/03/95 - UNIX PORT
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSNPAS*80
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSNPAS
C
C-----------------------------------------------------------------------
C
      RETURN
      END
