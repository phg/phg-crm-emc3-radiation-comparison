       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C ******************** FORTRAN PROGRAM : ADAS215  **********************
C
C  PURPOSE : TO TRANSFORM AN ADF04 FILE TO A DIFFERENT TEMPERATURE RANGE
C            USING BURGESS/TULLY TRANSFORMATIONS
C
C  PROGRAM:
C         (I*4) IUNT09      = PARAMETER = UNIT NUMBER FOR STEAM 9
C         (I*4) IUNT10      = PARAMETER = UNIT NUMBER FOR STEAM 9
C         (I*4) IUNT11      = PARAMETER = UNIT NUMBER FOR STEAM 11
C         (I*4) L1          = PARAMETER = INTEGER 1
C         (I*4) PIPEIN      = PARAMETER = INPUT STREAM NUMBER FROM PIPE
C         (I*4) NDPLOT      = PARAMETER = MAX. NO. OF DISPLAYED GRAPHS
C         (I*4) NDLNE       = PARAMETER = MAX. NO. OF TRANS. SELECTIONS
C         (I*4) NVMAX       = PARAMETER = MAX. NO. OF TEMPS. IN ADF04 FILE 
C         (I*4) NDLEV       = PARAMETER = MAX. NO. OF ENERGY LEVELS
C                                         IN AN ADF04 .
C         (I*4) NDTRN       = PARAMETER = MAX. NO. OF TRANS. IN AN 
C                                         ADF04 FILE.
C
C         (I*4) NDMET       = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C
C         (I*4) IPAN        = PROCESSING WINDOW COMPLETION SWITCH
C         (I*4) IZ          = RECOMBINED ION CHARGE..
C         (I*4) IZO         = NUCLEAR CHARGE. 
C         (I*4) IZ1         = RECOMBINING ION CHARGE.
C         (I*4) IL          = NO. OF ENERGY LEVELS FROM ADF04 FILE.
C         (I*4) NV          = NO. OF TEMPS. FROM INPUT ADF04 FILE.
C         (I*4) ITRAN       = NO. OF TRANSITIONS FROM INPUT ADF04 FILE.
C         (I*4) NPLOT       = NO. OF TRANSITIONS SELECTED FOR PLOTTING.
C         (I*4) MAXLEV      = HIGHEST INDEXED LEVEL FROM INPUT ADF04 FILE.
C         (I*4) NPL         = NO. OF PARENTS ON FIRST LINE OF ADF04 FILE
C         (I*4) IT          = GENERAL INDEX FOR TEMPERATURE
C         (I*4) NVN         = NO. OF TEMPS. FOR OUTPUT ADF04 FILE.
C         (I*4) IFORM       = GENERAL INDEX FOR TEMPERATURE FORM SELECTION.
C         (I*4) IFOUT       = INDEX OF TEMPERATURE FORM SELECTED IN IDL.
C         (I*4) ISTOP       = STOP SIGNAL INTEGER SEND FROM IDL.
C         (I*4) ICNTE       = NO OF ELECTRON IMPACT TRANS. IN ADF04 FILE
C         (I*4) ICNTP       = NO OF PROTON IMPACT TRANS. IN ADF04 FILE
C         (I*4) ICNTR       = NO OF RECOM. IMPACT TRANS. IN ADF04 FILE
C         (I*4) ICNTH       = NO OF CX. IMPACT TRANS. IN ADF04 FILE
C         (I*4) ICNTI       = NO OF IONIS. IMPACT TRANS. IN ADF04 FILE
C         (I*4) NSTRN1      = NO OF TRANSITIONS SELECTED FOR DISPLAY
C         (I*4) I           = GENERAL INDEX
C         (I*4) IE          = GENERAL INDEX
C
C         (I*4) IA()        = LEVEL INDEX FROM ADF04 FILE
C                             1ST DIM.: LEVEL INDEX
C         (I*4) ISA()       = LEVEL MULTIPLICITY FROM ADF04 FILE
C                             1ST DIM.: LEVEL INDEX
C         (I*4) ILA()       = LEVEL ORBITAL ANG. MOM FROM ADF04 FILE
C                             1ST DIM.: LEVEL INDEX
C         (I*4) I1A()       = TRANSITION LOWER LEVEL INDEX FROM ADF04 FILE
C                             1ST DIM.: TRANSITION INDEX
C         (I*4) I2A()       = TRANSITION UPPER LEVEL INDEX FROM ADF04 FILE
C                             1ST DIM.: TRANSITION INDEX
C         (I*4) IETRN()     = EL-TRANS. CROSS-REF. INDEX IN FULL TRANS. LIST
C                             1ST DIM.: ELECTRON IMPACT TRANSITION INDEX
C         (I*4) IE1A()      = EL-TRANS. LOWER LEVEL INDEX FROM ADF04 FILE
C                             1ST DIM.: ELECTRON IMPACT TRANSITION INDEX
C         (I*4) IE2A()      = EL-TRANS UPPER LEVEL INDEX FROM ADF04 FILE
C                             1ST DIM.: ELECTRON IMPACT TRANSITION INDEX
C         (I*4) NPLA()      = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                             OF LEVEL
C                             1ST DIM.: PARENT INDEX
C         (I*4) IPLA(,)     = PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                             OF LEVEL
C                             1ST DIM.: PARENT INDEX
C                             2ND DIM.: LEVEL INDEX
C         (I*4) ISTRN1()    = EL-TRANS. INDICES OF TRANS.SELECTED FOR PLOT
C                             1ST DIM.: SELECTED PLOT INDEX
C         (I*4) IDXPLT()    = EL-TRANS. INDICES OF TRANS.SELECTED FOR PLOT
C                             1ST DIM.: SELECTED PLOT INDEX
C
C         (R*8) AIN         = A-VALUE OF SELECTED TRANSITION
C         (R*8) WVNOU       = WAVE NO. OF UPPER LEVEL OF SELECTED TRANS.
C         (R*8) WVNLO       = WAVE NO. OF LOWER LEVEL OF SELECTED TRANS.
C         (R*8) WTU         = STAT. WT. OF UPPER LEVEL OF SELECTED TRANS.
C         (R*8) WTL         = STAT. WT. OF LOWER LEVEL OF SELECTED TRANS.
C         (R*8) C           = C-VALUE FOR SELECTED TRANS.
C         (R*8) XMIN        = LOWER LIMIT FOR SCALED TEMP UNITS 
C         (R*8) XMAX        = UPPER LIMIT FOR SCALED TEMP UNITS
C         (R*8) YMIN        = LOWER LIMITS FOR SCALED UPSILON COEFFT.
C         (R*8) YMAX        = UPPER LIMIT FOR SCALED UPSILON COEFFT.
C         (R*8) BWNO        = IONISATION POTENTIAL ( CM-1 ).
C
C         (R*8) BWNOA()     = IONISATION POTENTIAL (CM-1) OF PARENTS
C                             1ST.DIM.: PARENT INDEX
C         (R*8) PRTWTA()    = PARENT WEIGHT FOR BWNOA()
C                             1ST.DIM.: PARENT INDEX
C         (R*8) SCEF()      = ELECTRON TEMPS (K) FROM INPUT ADF04 FILE
C                             1ST.DIM.: INPUT TEMPERATURE INDEX
C         (R*8) XJA()       = (STAT.WT.-1)/2 FOR LEVELS FROM ADF04 FILE
C                             1ST.DIM.: LEVEL INDEX
C         (R*8) WA          = ENRGYS. (CM-1) OF LEVELS FROM INPUT ADF04 FILE
C                             1ST.DIM.: LEVEL INDEX
C         (R*8) AVAL()      = A-VALUES FOR TRANS. FROM INPUT ADF04 FILE.
C                             1ST.DIM.: TRANSITION INDEX
C         (R*8) SCOM(,)     = UPSILONS FOR TRANS. FROM INPUT ADF04 FILE.
C                             1ST DIM.: INPUT TEMPERATURE INDEX
C                             2ND.DIM.: TRANSITION INDEX
C         (R*8) ZPLA(,)     = EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                             OF LEVEL
C                             1ST DIM.: PARENT INDEX
C                             2ND DIM.: LEVEL INDEX
C         (R*8) SCOMN(,)    = UPSILONS FOR TRANS. FOR OUTPUT ADF04 FILE.
C                             1ST DIM.: OUTPUT TEMPERATURE INDEX
C                             2ND.DIM.: EL-TRANS INDEX
C         (R*8) YOUPLT(,)   = OUTPUT UPSILONS SELECTED FOR PLOTTING.
C                             1ST DIM.: OUTPUT TEMPERATURE INDEX
C                             2ND.DIM.: PLOT SELECTION  INDEX
C         (R*8) YINPLT(,)   = INPUT UPSILONS SELECTED FOR PLOTTING.
C                             1ST DIM.: INPUT TEMPERATURE INDEX
C                             2ND.DIM.: PLOT SELECTION  INDEX
C         (R*8) TEIN()      = INPUT TEMPS (K) FOR SELECTED TRANS.
C                             1ST DIM.: INPUT TEMPERATURE INDEX.
C         (R*8) UPSIN()     = INPUT UPSILONS FOR SELECTED TRANS.
C                             1ST DIM.: INPUT TEMPERATURE INDEX.
C         (R*8) TEOUT()     = OUTPUT TEMPS (K) FOR SELECTED TRANS.
C                             1ST DIM.: OUTPUT TEMPERATURE INDEX.
C         (R*8) UPSOUT      = OUTPUT UPSILONS FOR SELECTED TRANS.
C                             1ST DIM.: OUTPUT TEMPERATURE INDEX.
C         (R*8) AA()        = A-VALUE FOR EL-TRANS.
C                             1ST DIM.: EL-TRANS. INDEX.
C         (R*8) CEA()       = ASSIGNED C-VALUE FOR EL-TRANS.
C                             1ST DIM.: EL-TRANS. INDEX.
C         (R*8) CPLT()      = ASSIGNED C-VALUE FOR TRANS. FOR PLOTTING
C                             1ST DIM.: PLOT SELECTION INDEX.
C         (R*8) TOUT()      = OUTPUT TEMPS RETURNED FROM IDL
C                             UNITS AS SELECTED IN IDL
C                             1ST DIM.: OUTPUT TEMPERATURE INDEX.
C         (R*8) TSCEF(,)    = INPUT TEMPS IN THREE FORMS
C                             1ST DIM.: INPUT TEMPERATURE INDEX.
C                             2ND DIM.: TEMP. FORM: 1=K, 2=EV, 3=RED.
C         (R*8) TSCEFO(,)   = OUTPUT TEMPS IN THREE FORMS
C                             1ST DIM.: INPUT TEMPERATURE INDEX.
C                             2ND DIM.: TEMP. FORM: 1=K, 2=EV, 3=RED.
C         (R*8) ER()        = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                             1ST DIM.: LEVEL INDEX
C         (R*8) XIA()       = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                             1ST DIM.: LEVEL INDEX
C
C         (C*3) REP         = SIGNAL FROM IDL (HISTORIC)
C         (C*8) DATE        = DATE (DD/MM/YR) RETURNED FROM IDL
C         (C*8) USERID      = USER NAME RETURNED FROM IDL
C         (C*80) DSNINC     = DATASET NAME FOR INPUT ADF04 FILE
C         (C*80) SAVFIL     = DATASET NAME FOR OUTPUT TEXT FILE
C         (C*80) DSNPAS     = DATASET NAME FOR OUTPUT ADF04 FILE
C         (C*80) CADAS      =  HEADER FOR TEXT OUTPUT
C         (C*40) TITLE      =  HEADER FOR GRAPH OUTPUT
C         (C*120) TITLX     = INFORMATION STRING CONTAINING: INPUT DATA
C                             SET-NAME AND FURTHER INFORMATION READ FROM 
C                             DATA-SET.
C         (C*92) CIONP      = STRING WITH LEVEL TERMINATOR LINE FROM 
C                             INPUT ADF04 FILE
C         (C*3) TITLED      =  ELEMENT SYMBOL ('SS+')
C         (C*9) CPRTA()     = PARENT NAME IN BRACKETS
C                             1ST DIM.: PARENT INDEX 
C         (C*1) CPLA()      = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IA()'
C                                INTEGER - PARENT IN BWNOA() LIST
C                                'BLANK' - PARENT BWNOA(1)
C                                  'X'   - DO NOT ASSIGN A PARENT
C                             1ST DIM.: LEVEL INDEX 
C         (C*1) TCODE()     = TRANS. TYPE CODE FORM INPUT ADF04 FILE
C                             1ST DIM.: TRANSITION INDEX 
C         (C*1) TYP         = TRANS. TYPE CODE FOR SELECTED TRANS.
C         (C*1) PECODE()    = PLOT SEL. CODE FOR EL-TRANS. FROM IDL
C                             1ST DIM.: EL-TRANS. INDEX 
C         (C*1) TECODE()    = TYPE  SEL. CODE FOR EL-TRANS. FROM IDL
C                             1ST DIM.: EL-TRANS. INDEX 
C         (C*1) TYPPLT()    = TYPE  SEL. CODE FOR PLOT TRANS. FROM IDL
C                             1ST DIM.: PLOT SELECTION INDEX 
C         (C*18) CSTRGA()   = REDUCED CONFIG. STRING FOR LEVEL FROM ADF04 FILE
C                             1ST DIM.: LEVEL INDEX 
C         (C*22) STRGA()    = FULL CONFIG. STRING FOR LEVEL FROM ADF04 FILE
C                             1ST DIM.: LEVEL INDEX 
C
C
C         (L*4)  OPEN9      = .TRUE.  => STREAM 9 DATA SET OPENED
C                             .FALSE. => STREAM 9 DATA SET NOT OPENED
C         (L*4)  LPEND      = .TRUE.  => FINISH PROGRAM
C                             .FALSE. => RETURN TO PROGRAM
C         (L*4)  LOSEL      = .TRUE.  => OUTPUT TEMPERATURES SET
C                             .FALSE. => OUTPUT TEMPERATURES NOT SET
C         (L*4)  LDEF1      = .TRUE.  => USER SELECTED AXES LIMITS
C			      .FALSE. => NO USER SUPPLIED LIMITS
C         (L*4)  LGRAPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C         (L*4)  L2FILE     = .TRUE.  => SAVE TEXT DATA TO FILE
C			      .FALSE. => DO NOT SAVE TEXT DATA TO FILE
C         (L*4)  LPASS      = .TRUE.  => STORE OUTPUT ADF04 FILE
C			      .FALSE. => DO NOT STORE
C         (L*4)  LPEND      = .TRUE.  => PROCESS OUTPUT OPTIONS
C  			      .FALSE. => CANCEL OUTPUT OPTIONS
C         (L*4)  L2REP      = .TRUE.  => REPLACE OUTPUT TEXT FILE
C                             .FALSE. => DON'T REPLACE OUTPUT FILE
C         (L*4)  LBSETA()   = .TRUE.  - PARENT WEIGHT SET FOR BWNOA()
C                             .FALSE. - PARENT WEIGHT NOT SET FOR BWNOA()
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          BFSPF0     ADAS      GATHERS INPUT FILE NAMES VIA IDL 
C          BFDATA     ADAS      GATHERS DATA FROM INPUT ADF04 FILE
C          BFSETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          BFISPF     ADAS      GATHERS USERS VALUES VIA IDL
C          BFTITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          BFSPF1     ADAS      GATHERS GRAPHICAL PARMS VIA IDL
C          BFOUTG     ADAS      OUTPUT GRAPHS USING IDL
C          BFWR11     ADAS      OUTPUT MODIFIED ADF04 DATASET TO FILE
C          BFOUT0     ADAS      OUTPUT ION SPEC. ETC. TO OUTPUT STREAM
C          BFOUT1     ADAS      OUTPUT MAIN RESULTS TO OUTPUT STREAM
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXGUID     ADAS      GATHERS USER NAME AS 10 BYTE STRING
C          XXTCON     ADAS      CONVERTS TEMPERATURES TO THREE FORMS
C          XXERYD     ADAS      CONVERTS ENERGIES FROM W.NO. TO RYDBERGS
C
C
C
C  AUTHOR:   H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C            JA8.08
C            TEL.  0141-553-4196.
C
C  DATE:    04/06/98
C
C UPDATE:
C 
C VERSION: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.				
C
C VERSION: 	1.2					DATE: 15/04/99
C MODIFIED:	Martin O'Mullane
C		- Change parameters of subroutines BFTRAN and BFWR11.
C               - BFTRAN is extended to type 4 and includes the limit 
C                 points for type 1 and 4 transitions.
C
C VERSION: 	1.3					DATE: 06/10/99
C MODIFIED:	Martin O'Mullane
C		- If the final splined collision strength is negative
C                 add y=0 at x=1 as the boundary point.
C
C VERSION: 	1.4					DATE: 21-03-00
C MODIFIED:	RICHARD MARTIN
C		Removed NDTRN from call to bfout0 
C                  
C VERSION : 1.5					
C DATE    : 16-01-2007
C MODIFIED: Martin O'Mullane
C             - Remove reading USERID from IDL. 
C	
C VERSION : 1.6					
C DATE    : 01-11-2009
C MODIFIED: Martin O'Mullane
C             - Replace bfdata with xxdata_04 but keep bfttyp.
C             - Increase levels to 1500 and transitions to 15000.
C	
C VERSION : 1.7				
C DATE    : 30-11-2017
C MODIFIED: Martin O'Mullane
C             - Increase number of transitions to 100000.
C             - Allow untied levels.
C	
C-----------------------------------------------------------------------
      INTEGER   NVMAX       , NDPLOT      , NDQDN   , NDTEM
      INTEGER   NDLEV       , NDTRN       , NDMET   , NDLNE
      INTEGER   IUNT09      , IUNT10      , IUNT11
      INTEGER   L1
      INTEGER   PIPEIN 
      REAL*8    DZERO
C-----------------------------------------------------------------------
      PARAMETER( NVMAX = 14  , NDTEM = 14     )
      PARAMETER( NDLEV = 1500, NDTRN = 100000 , NDMET = 5   , 
     &           NDQDN  = 6  , NDLNE = 20 )
      PARAMETER( NDPLOT= 20 )
      PARAMETER( IUNT09 = 9 , IUNT10 = 10     , IUNT11= 11  )
      PARAMETER( PIPEIN = 5 )
      PARAMETER( L1     = 1 )
      PARAMETER( DZERO  = 1.0D-30 )
C-----------------------------------------------------------------------
      INTEGER   IPAN        
      INTEGER   IZ          , IZ0            , IZ1        ,
     &          IL          , NV             , ITRAN      , NPLOT      ,
     &          MAXLEV      , NPL            , IT         ,
     &          NVN         , IFORM          , IFOUT      , ISTOP
      INTEGER   ICNTE       , ICNTP          , ICNTR      , ICNTH      ,
     &          ICNTI       
      INTEGER   NSTRN1         
      INTEGER   I           , IE    
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &          I1A(NDTRN)  , I2A(NDTRN)     ,
     &          IPLA(NDMET,NDLEV)            , NPLA(NDLEV)
      INTEGER   IETRN(NDTRN),
     &          IE1A(NDTRN) , IE2A(NDTRN)
      INTEGER   ISTRN1(NDLNE)
      INTEGER   IDXPLT(NDPLOT)                    
C-----------------------------------------------------------------------
      REAL*8    AIN    , WVNOU   , WVNOL   ,
     &          WTU    , WTL     , C
      REAL*8    XMIN   , XMAX    , YMIN    , YMAX
      REAL*8    GTOT
C-----------------------------------------------------------------------
      LOGICAL   LNEG
C-----------------------------------------------------------------------
      REAL*8    BWNO        , BWNOA(NDMET)   , PRTWTA(NDMET)
      REAL*8    SCEF(NVMAX)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)                   ,
     &          AVAL(NDTRN) , SCOM(NVMAX,NDTRN)           ,
     &          ZPLA(NDMET,NDLEV)                         ,
     &          SCOMN(NVMAX,NDTRN)                        ,
     &          YOUPLT(NVMAX,NDPLOT)    , YINPLT(NVMAX,NDPLOT) 
      REAL*8    TEIN(NVMAX)  , UPSIN(NVMAX) ,
     &          TOUT(NVMAX)  , TEOUT(NVMAX) , UPSOUT(NVMAX)
      REAL*8    AA(NDTRN)    , CEA(NDTRN)   , CPLT(NDPLOT)
      REAL*8    TSCEF(NVMAX,3)              , TSCEFO(NVMAX,3)       ,    
     &          XIA(NDLEV)   , ER(NDLEV)    
C-----------------------------------------------------------------------
      CHARACTER REP*3       , DATE*8
      CHARACTER DSNINC*80   , SAVFIL*80      , DSNPAS*80
      CHARACTER CADAS*80    , TITLE*40       , TITLX*120
      CHARACTER CIONP*92
      CHARACTER TITLED*3    , TCODE(NDTRN)*1 , CSTRGA(NDLEV)*18  ,
     &          STRGA(NDLEV)*22
      CHARACTER TYP*1
      CHARACTER CPLA(NDLEV)*1,CPRTA(NDMET)*9
      CHARACTER PECODE(NDTRN)*1  , TECODE(NDTRN)*1
      CHARACTER TYPPLT(NDPLOT)*1
C-----------------------------------------------------------------------
      LOGICAL   OPEN9       , LPEND
      LOGICAL   LDEF1       , LGRAPH         , L2FILE     , LOSEL   ,
     &          LPASS       , L2REP
C-----------------------------------------------------------------------
      LOGICAL   LBSETA(NDMET) , LNEGLIST(NDTRN)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       DATA OPEN9 /.FALSE./
       DATA LNEGLIST /NDTRN*.FALSE./
C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 - unused for calculations
C-----------------------------------------------------------------------
      integer   iadftyp       , itieactn      , iorb    
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      real*8    beth(ndtrn)                   ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------


C-----------------------------------------------------------------------
C****************** MAIN PROGRAM   *************************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
       CALL XX0000
C
C-----------------------------------------------------------------------
C GET DATE
C-----------------------------------------------------------------------
C
       CALL XXDATE(DATE)

C-----------------------------------------------------------------------
  100 IPAN=0
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 9  - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
      IF (OPEN9) CLOSE(9)
      OPEN9 =.FALSE.
C
C-----------------------------------------------------------------------
C GET INPUT SPECIFIC ION FILE DATA SET NAME (FROM ISPF)
C-----------------------------------------------------------------------
C
      CALL BFSPF0( REP , DSNINC )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED:  END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF
C-----------------------------------------------------------------------
C OPEN ASSOCIATED SPECIFIC ION FILE  - DSNINC
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT09 , FILE=DSNINC , STATUS='OLD' )
      OPEN9=.TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM ASSOCIATED SPECIFIC ION FILE
C-----------------------------------------------------------------------
C
       itieactn = 1
       
       call xxdata_04( iunt09 , 
     &                 ndlev  , ndtrn , ndmet   , ndqdn , ndtem ,
     &                 titled , iz    , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,     
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )

       cionp = '   -1'
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------
C
         DO 1 IFORM=1,3
            CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SORT OUT ELECTRON IMPACT TRANSITIONS & PROVIDE INITIAL CLASSIFICATION
C-----------------------------------------------------------------------
C
      CALL BFTTYP( NDLEV  , NDTRN  ,
     &             IZ1    , IL     ,
     &             IA     , CSTRGA , ISA    , ILA   , XJA   , WA ,
     &             ITRAN  , TCODE  , I1A    , I2A   , AVAL  ,
     &             ICNTE  , ICNTP  , ICNTR  , ICNTH , ICNTI ,
     &             IETRN  , PECODE , TECODE , IE1A   , IE2A , AA ,
     &             CEA
     &           )
C
C-----------------------------------------------------------------------
C SET UP POOL VARIABLES AND SEND TO IDL
C-----------------------------------------------------------------------
C
       CALL BFSETP( DSNINC ,
     &              IZ0    , IZ     ,
     &              TITLED , BWNO   ,
     &              NDLEV  , IL     , ICNTE  ,
     &              NVMAX  , NV     , SCEF   ,
     &              CSTRGA , ISA    , ILA    , XJA   ,
     &              STRGA
     &            )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL)
C-----------------------------------------------------------------------
C
  200  CALL BFISPF( IPAN   , LPEND  ,
     &              NDLNE  , NVMAX  ,
     &              IL     ,
     &              ICNTE  , ICNTH  , ICNTR ,
     &              PECODE , TECODE , IE1A  , IE2A  , CEA   , STRGA ,
     &              NV     , TSCEF  ,
     &              NSTRN1 , ISTRN1 ,
     &              IFOUT  , NVN    , TOUT  ,
     &              LOSEL  
     &            )
      IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT OUTPUT TEMPERATURES INTO KELVIN:  (TOUT -> TEOUT) 
C-----------------------------------------------------------------------
C
            CALL XXTCON( IFOUT, L1, IZ1, NVN , TOUT  , TEOUT    )
C
C-----------------------------------------------------------------------
C SET UP  'TSCEFO(,)' - CONTAINS OUTPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------
C
         DO 5 IFORM=1,3
            CALL XXTCON( IFOUT , IFORM , IZ1 , NVN , TOUT , 
     &                   TSCEFO(1,IFORM) )
    5    CONTINUE
C
C-----------------------------------------------------------------------
C COMPUTE INTERPOLATED UPSILONS USING BURGESS-TULLY TRANSOFRMATIONS
C-----------------------------------------------------------------------
C
       NPLOT=0
C
       DO 20 IE = 1, ICNTE
         I=IETRN(IE)
         C=CEA(IE)
         TYP=TECODE(IE)
         AIN=AA(IE)
         WVNOU=WA(IE2A(IE))
         WVNOL=WA(IE1A(IE))
         WTU=2.0D0*XJA(IE2A(IE))+1.0D0
         WTL=2.0D0*XJA(IE1A(IE))+1.0D0
         GTOT = 0.0D0
         DO 15 IT = 1,NV
           TEIN(IT) = SCEF(IT)
           UPSIN(IT) = SCOM(IT,I)
           GTOT = GTOT + UPSIN(IT)
   15    CONTINUE

C If gamma is zero do not bother extrapolating
         
         if (gtot.LE.nv*DZERO) then
            do it=1,nvn
              SCOMN(IT,IE) = dzero
            end do
            goto 20
         endif

         LNEG = .FALSE.
         CALL  BFTRAN( TYP   , C      , LNEG  ,
     &                 AIN   , WVNOU  , WVNOL , WTU   , WTL  ,
     &                 TEIN  , UPSIN  , NV    ,
     &                 TEOUT , UPSOUT , NVN
     &               )
C
         
         if (upsout(nvn).lt.0.0D0) then
            
            LNEG = .TRUE.
            LNEGLIST(IE) = .TRUE.
            CALL  BFTRAN( TYP   , C      , LNEG  ,
     &                    AIN   , WVNOU  , WVNOL , WTU   , WTL  ,
     &                    TEIN  , UPSIN  , NV    ,
     &                    TEOUT , UPSOUT , NVN   )

         endif
         
         
         DO 17 IT = 1,NVN
           SCOMN(IT,IE) = UPSOUT(IT)
   17    CONTINUE
C
C-----------------------------------------------------------------------
C COLLECT OUTPUT DATA FOR GRAPHING  
C-----------------------------------------------------------------------
C
         IF((PECODE(IE).EQ.'p').OR.(PECODE(IE).EQ.'P'))THEN
             NPLOT=NPLOT+1
             IDXPLT(NPLOT)=IE
             TYPPLT(NPLOT)=TYP
             CPLT(NPLOT)=C
             DO 18 IT=1,NVN
               YOUPLT(IT,NPLOT)=UPSOUT(IT)
   18        CONTINUE
             DO 19 IT=1,NV
               YINPLT(IT,NPLOT)=UPSIN(IT)
   19        CONTINUE            
         ENDIF            
C
   20  CONTINUE
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-SET UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
          CALL BFTITL( DSNINC        ,
     &                 TITLX
     &               )
C
C-----------------------------------------------------------------------
C   COMMUNICATE WITH IDL VIA A PIPE TO GET USER SELECTED OUTPUT OPTIONS.
C-----------------------------------------------------------------------
C
300   CONTINUE
          CALL BFSPF1( DSNINC        , LOSEL         , LDEF1    ,
     &                 LGRAPH        , L2FILE        , SAVFIL   ,
     &                 LPASS         , DSNPAS        ,
     &                 XMIN          , XMAX          , YMIN     , YMAX ,
     &                 LPEND         , L2REP         , CADAS
     &               )

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
CX    IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 200

C
C-----------------------------------------------------------------------
C OUTPUT GRAPHICAL RESULTS.
C-----------------------------------------------------------------------
C
       IF (LGRAPH) THEN
          CALL BFOUTG( NVMAX         , NDPLOT       ,
     &                 TITLE         , TITLX        , DATE       ,
     &                 IZ0           , IZ           ,
     &                 NPLOT         , 
     &                 IDXPLT        , TYPPLT       , CPLT       ,
     &                 TEIN          , YINPLT       , NV         ,
     &                 TEOUT         , YOUPLT       , NVN        ,
     &                 LDEF1         , 
     &                 XMIN          , XMAX         , YMIN       , YMAX
     &               )
       ENDIF

       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999

       GO TO 300
C
C-----------------------------------------------------------------------
C WRITE OUTPUT ADF04 FILE AND TEXT FILE
C-----------------------------------------------------------------------
 9999  IF(LOSEL.AND.LPASS) THEN
C
            OPEN( UNIT=IUNT11 , FILE=DSNPAS , STATUS='UNKNOWN')
C 
            CALL BFWR11( IUNT11 , DSNINC , 
     &                   NDMET  , NDLEV  , NDTRN , NVMAX ,
     &                   DATE   , 
     &                   TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
     &                   NPL    , BWNOA  , LBSETA, PRTWTA, CPRTA ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA   , ILA   , XJA   , WA ,
     &                   CPLA   , NPLA   , IPLA  , ZPLA  ,
     &                   CIONP  ,
     &                   NVN    , TEOUT  ,
     &                   ICNTE  , MAXLEV ,
     &                   TECODE , IE1A   , IE2A  , AA    , SCOMN 
     &            )
C
            CLOSE(IUNT11)
       ENDIF
C
       IF(LOSEL.AND.L2FILE) THEN
C
            CALL XXERYD( BWNO , IL , WA  ,
     &                     ER , XIA
     &                 )
C
            OPEN( UNIT=IUNT10 , FILE=SAVFIL , STATUS='UNKNOWN')
C 
            CALL BFOUT0( IUNT10 , 
     &                   DATE   , 'T'    , DSNINC , 'NULL' ,
     &                   TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH  ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA  , WA ,
     &                   ER     ,
     &                   NV     , TSCEF  ,
     &                   NVN    , TSCEFO ,
     &                   PECODE , TECODE , IE1A   , IE2A   , CEA  , 
     &                   CADAS  , LNEGLIST
     &                 )
C
            CLOSE(IUNT09)
       ENDIF
C
C---- NOTIFY IDL THAT FILE WRITING IS COMPLETE  -----
C
C       WRITE(PIPEOU,*)ONE
C
       STOP
C-----------------------------------------------------------------------
       END
