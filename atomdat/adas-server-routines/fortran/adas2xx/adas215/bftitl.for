C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bftitl.for,v 1.1 2004/07/06 11:39:41 whitefor Exp $ Date $Date: 2004/07/06 11:39:41 $
C
      SUBROUTINE BFTITL( DSFULL ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFTITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-SET.
C
C  CALLING PROGRAM: ADAS215
C
C  SUBROUTINE:
C
C  INPUT : (C*80) DSFULL   = FULL MVS INPUT DATA SET NAME
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C	   (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C	   (I*4)  LEN_NAME = LENGTH OF FILENAME
C          (I*4)  IFIRST   = POSITION OF FIRST CHARACTER IN FILENAME
C          (I*4)  ILAST    = POSITION OF LAST CHARACTER IN FILENAME
C
C ROUTINES: 
C          XXSLEN          = UTILITY ROUTINE WHICH FINDS FIRST AND LAST
C                            NON-BLANK CHARACTERS IN A STRING.
C
C AUTHOR:  H. P. SUMMERS
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    04/06/98
C
C UPDATE:
C
C VERSION: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSFULL*80       ,
     &           TITLX*120
C-----------------------------------------------------------------------
         CALL XXSLEN(DSFULL,IFIRST,ILAST)
	 LEN_NAME = ILAST-IFIRST+1
         IF (LEN_NAME.GT.80) IFIRST = ILAST - 80
         LEN_NAME = ILAST-IFIRST+1
         TITLX(1:LEN_NAME+1) = DSFULL(IFIRST:ILAST)
         POS_NOW = LEN_NAME+1         
         TITLX(POS_NOW:POS_NOW+8) =  '        '
         POS_NOW = POS_NOW+9
         TITLX(POS_NOW:120) =  '   ' 
C
C-----------------------------------------------------------------------
C
      RETURN
      END
