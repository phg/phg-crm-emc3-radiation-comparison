C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bfsetp.for,v 1.1 2004/07/06 11:39:19 whitefor Exp $ Date $Date: 2004/07/06 11:39:19 $
C
       SUBROUTINE BFSETP( DSN44  ,
     &                    IZ0    , IZ     ,
     &                    TITLED , BWNO   ,
     &                    NDLEV  , IL     , ICNTE  ,
     &                    NDTEM  , MAXT   , TEA    ,
     &                    CSTRGA , ISA    , ILA    , XJA   ,
     &                    STRGA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFSETP *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS215
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'BFDATA' & 'BFDATC'
C
C  SUBROUTINE:
C
C  INPUT : (C*80) DSN44   = DATA SET NAME OF ASSOCIATED adf04 FILE
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (WAVE NUMBER) (CM-1)
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (I*4)  NDTEM   = MAXIUMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> NDTEM )
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES  (UNITS: KELVIN)
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (C*22) STRGA() = LEVEL DESIGNATIONS
C                           DIMENSION: LEVEL INDEX
C
C          (I*4)  ILVAL   = VALUE OF QUANTUM NUMBER L + 1
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  IT      = ARRAY COUNTER FOR TEMPERATURE INDEX
C
C          (C*3)  C3      = GENERAL USE 3 BYTE CHARACTER STRING
C          (C*12) C12     = GENERAL USE 12 BYTE CHARACTER STRING
C          (C*22) C22     = GENERAL USE 22 BYTE CHARACTER STRING
C          (C*2)  SZ0     =         NUCLEAR CHARGE READ
C          (C*2)  SZ      =  RECOMBINED ION CHARGE READ
C          (C*10) SIONPT  =  IONISATION POTENTIAL (UNITS: WAVE NUMBERS)
C          (C*3)  SCNTE   =  NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (C*3)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*1)  CONFIG()= QUANTUM NUMBER (L) LETTERS
C                           DIMENSION: QUANTUM NUMBER L + 1
C          (C*8)  CHA()   = FUNCTION POOL NAMES: CHARGE VALUES ETC.
C          (C*8)  CHB()   = FUNCTION POOL NAMES: LEVEL DESIGNATIONS
C          (C*43) DSN43   = DSN OF ASSOCIATED COPASE FILE IGNORING FIRST
C                           BYTE ('/').
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    04/06/98
C
C UPDATE: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.	  
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   NDLEV         , NDTEM
      INTEGER   IZ0           , IZ            ,
     &          IL            , MAXT          ,
     &          ICNTE
      INTEGER   ILVAL
      INTEGER   ILEV          , IT
      INTEGER   ISA(IL)       , ILA(IL)       
C-----------------------------------------------------------------------
      REAL*8    BWNO
      REAL*8    XJA(IL)       , TEA(NDTEM)
C-----------------------------------------------------------------------
      CHARACTER C12*12
      CHARACTER TITLED*3
      CHARACTER SZ0*2         , SZ*2          , SCNTE*3 ,
     &          SIL*3         , SIONPT*10     ,
     &          DSN44*80      , CONFIG(20)*1
      CHARACTER CHA(8)*8      , CHB(6)*8
      CHARACTER CSTRGA(IL)*18 , STRGA(NDLEV)*22 
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      SAVE      CHA            , CHB
C-----------------------------------------------------------------------
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
      DATA CHA   / '(SION)  ' , '(SZ)    ' , '(SZ0)   ' , '(SIONPT)' ,
     &             '(SCNTE) ' , '(SIL)   ' , '(SRAT1) ' , '(SDSN10)' /
      DATA CHB   / '(SMI**) ' , '(SMD**) ' , '(STE**) ' , '(SDE**) ' ,
     &             '(SDH**) ' , '(SNZ**) ' /
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      WRITE(SZ    ,1000) IZ
      WRITE(SZ0   ,1000) IZ0
      WRITE(SIONPT,1001) BWNO
      WRITE(SCNTE ,1002) ICNTE
      WRITE(SIL   ,1002) IL
         DO 1 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1003)
     &             CSTRGA(ILEV)(1:12),ISA(ILEV),CONFIG(ILVAL),XJA(ILEV)
               ENDIF
    1    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C SEND STRINGS TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, '(A3)' ) TITLED
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A2)' ) SZ
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A2)' ) SZ0
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A10)' ) SIONPT
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A3)' ) SCNTE
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A3)' ) SIL
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, '(A80)' ) DSN44
      CALL XXFLSH( PIPEOU )

C-----------------------------------------------------------------------
         WRITE( PIPEOU, * ) NDTEM
         CALL XXFLSH( PIPEOU )

         DO 3 IT=1,NDTEM
            WRITE(CHB(3)(5:6),1004) IT
               IF (IT.LE.MAXT) THEN
                  WRITE(C12,1005) TEA(IT)
               ELSE
                  C12=' '
               ENDIF

            WRITE( PIPEOU, '(A12)' ) C12
            CALL XXFLSH( PIPEOU )

    3    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(1PD10.4)
 1002 FORMAT(I3)
 1003 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
 1004 FORMAT(I2.2)
 1005 FORMAT(1PD12.5)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
