C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bfoutg.for,v 1.1 2004/07/06 11:39:12 whitefor Exp $ Date $Date: 2004/07/06 11:39:12 $
C
      SUBROUTINE BFOUTG( NVMAX  , NDPLOT ,
     &                   TITLE  , TITLX  , DATE  ,
     &                   IZ0    , IZ     ,
     &                   NPLOT  ,
     &                   IDXPLT , TYPPLT , CPLT       ,
     &                   TEIN   , YINPLT , NV         ,
     &                   TEOUT  , YOUPLT , NVN        ,
     &                   LDEF1  ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFOUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL TO PLOT GRAPH
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE INTERPOLATED POINTS : CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C
C            PLOT IS LOG10(UPSILON) V LOG10(TEMP.(K))
C
C  CALLING PROGRAM: ADAS215
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NVMAX   = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT : (I*4)  NDPLOT  = MAXIMUM NUMBER OF RATE COEFFTS. TO PLOT
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK, ION INFORMATION
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IZ0     = INPUT FILE: RECEIVER - NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           RECEIVED ION - FINAL CHARGE
C
C  INPUT : (I*4)  NPLOT   = NUMBER OF DISTINCT PLOTS
C  INPUT : (I*4)  IDXPLT()= INDEX NUMBER OF PLOT TRANSITION IN FULL
C                           ELECTRON IMPACT TRANSITION LIST
C                           1ST. DIM: PLOT INDEX NUMBER
C  INPUT : (C*1)  TYPPLT()= BURGESS - TULLY TYPE OF PLOT TRANSITION 
C                           1ST. DIM: PLOT INDEX NUMBER
C  INPUT : (C*1)  CPLT()  = BURGESS - TULLY C FOR PLOT TRANSITION 
C                           1ST. DIM: PLOT INDEX NUMBER
C  INPUT : (R*8)  TEIN()  = TEMPS FOR RATE COEFFTS FROM ADF04 FILE (K)
C                           1ST. DIM: INPUT TEMPERATURE INDEX
C  INPUT : (R*8)  YINPLT(,)= UPSILONS FOR PLOT TRANSITIONS FROM ADF04 
C                            FILE
C                           1ST. DIM: INPUT TEMPERATURE INDEX
C                           2ND. DIM: PLOT INDEX NUMBER
C  INPUT : (I*4)  NV      = NUMBER OF TEMPS. FROM ADF04 FILE
C  INPUT : (R*8)  TEOUT() = TEMPS FOR RATE COEFFTS FOR ADF04 FILE (K)
C                           1ST. DIM: OUTPUT TEMPERATURE INDEX
C  INPUT : (R*8)  YOUPLT(,)= UPSILONS FOR PLOT TRANSITIONS FOR ADF04 
C                            FILE
C                           1ST. DIM: OUTPUT TEMPERATURE INDEX
C                           2ND. DIM: PLOT INDEX NUMBER
C  INPUT : (I*4)  NVN     = NUMBER OF TEMPS. FOR ADF04 FILE OUTPUT
C
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR ENERGY
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR ENERGY
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR CROSS-SECTION.
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR CROSS-SECTION.
C
C
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*28) STRG1   =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG2   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG4   =  DESCRIPTIVE STRING FOR STATE ION
C          (C*28) STRG5   =  DESCRIPTIVE STRING FOR STATE ION CHARGE
C          (C*28) STRG6   =  DESCRIPTIVE STRING FOR STATE META. INDEX
C          (C*32) HEAD1   =  HEADING FOR IONISING ION INFORMATION
C          (C*16) HEAD2   =  HEADING FOR INITIAL STATE INFORMATION
C          (C*16) HEAD3   =  HEADING FOR FINAL   STATE INFORMATION
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C	   XXFLSH      IDL-ADAS  CALLS FLUSH COMMAND TO CLEAR PIPE.
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    04/06/98
C
C UPDATE: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    NVMAX           , NDPLOT
      INTEGER    IZ0             , IZ               ,
     &           NPLOT           , NV               , NVN
      INTEGER    I               , IT
C-----------------------------------------------------------------------
      REAL*8     XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       , TITLX*120         , 
     &           DATE*8      
      CHARACTER  STRG1*28       , STRG2*28          ,
     &           STRG3*28       , STRG4*28          ,
     &           STRG5*28       , STRG6*28          ,
     &           HEAD1*32       , HEAD2*32          , HEAD3*32
C-----------------------------------------------------------------------
      INTEGER    IDXPLT(NDPLOT)
C-----------------------------------------------------------------------
      REAL*8     TEIN(NVMAX)    , TEOUT(NVMAX)
      REAL*8     YINPLT(NVMAX,NDPLOT) , YOUPLT(NVMAX,NDPLOT)
      REAL*8     CPLT(NDPLOT)  
C-----------------------------------------------------------------------
      CHARACTER  TYPPLT(NDPLOT)*1
C-----------------------------------------------------------------------
      DATA HEAD1 /'---- TRANSITION INFORMATION ----'/ ,
     &     HEAD2 /' E-IMPACT   BURGESS & TULLY     '/                 ,
     &     HEAD3 /'  INDEX      TYPE    C-VALUE    '/
      DATA STRG1 /'                            '/ ,
     &     STRG2 /'                            '/ ,
     &     STRG3 /'                            '/ ,
     &     STRG4 /'                            '/ ,
     &     STRG5 /'                            '/ ,
     &     STRG6 /'                            '/
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) NVMAX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDPLOT
      CALL XXFLSH(PIPEOU)
      
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ
      CALL XXFLSH(PIPEOU)
C
      WRITE(PIPEOU,*) NPLOT
      CALL XXFLSH(PIPEOU)
      
      DO  1 I = 1,NPLOT
         WRITE(PIPEOU,*) IDXPLT(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
C
      DO 2 I = 1,NPLOT
         WRITE(PIPEOU,*) TYPPLT(I)
	 CALL XXFLSH(PIPEOU)
 2    CONTINUE
C 
      DO 3 I = 1,NPLOT
         WRITE(PIPEOU,*) CPLT(I)
	 CALL XXFLSH(PIPEOU)
 3    CONTINUE
C
      WRITE(PIPEOU,*) NV
      CALL XXFLSH(PIPEOU)
C
      DO 4 I = 1,NV
         WRITE(PIPEOU,*) TEIN(I)
	 CALL XXFLSH(PIPEOU)
 4    CONTINUE
C
      DO 6 I = 1,NPLOT
        DO 5 IT=1,NV
          WRITE(PIPEOU,*) YINPLT(IT,I)
	 CALL XXFLSH(PIPEOU)
 5      CONTINUE
 6    CONTINUE
C
      WRITE(PIPEOU,*) NVN
      CALL XXFLSH(PIPEOU)
C
      DO 7 I = 1,NVN
         WRITE(PIPEOU,*) TEOUT(I)
	 CALL XXFLSH(PIPEOU)
 7    CONTINUE
C
      DO 9 I = 1,NPLOT
        DO 8 IT=1,NVN
          WRITE(PIPEOU,*) YOUPLT(IT,I)
	 CALL XXFLSH(PIPEOU)
 8      CONTINUE
 9    CONTINUE
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      WRITE(PIPEOU,'(A28)') STRG1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG3
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG4
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG5
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A28)') STRG6
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD2
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD3
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
      END

