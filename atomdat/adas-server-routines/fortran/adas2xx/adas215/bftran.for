C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bftran.for,v 1.4 2009/09/03 15:15:04 mog Exp $ Date $Date: 2009/09/03 15:15:04 $
C
      SUBROUTINE  BFTRAN( TYP   , C      , LNEG  ,
     &                    AIN   , WVNOU  , WVNOL , WTU   , WTL  ,
     &                    TEIN  , UPSIN  , NV    ,
     &                    TEOUT , UPSOUT , NVN
     &                  )
      IMPLICIT NONE
C----------------------------------------------------------------
C
C ************* FORTRAN SUBROUTINE  BFTRAN  *******************
C
C PURPOSE : TO IMPLEMENT THE TRANSFORMATION DESCRIBED BY
C           BURGESS AND TULLY ( SEE REFERENCE (1)) WHICH
C           IS USED TO ASSESS AND COMPACT DATA.
C
C
C REFERENCES:
C           (1) A.BURGESS AND J.A.TULLY
C           ON THE ANALYSIS OF COLLISION STRENGTHS
C           AND RATE COEFFICIENTS.
C           ASTRON.ASTROPHYS.254,436-453 (1992 )
C
C           (2) SUMMERS.H.P
C               ADAS USERS MANUAL ( 1ST EDITION ).
C
C INPUT :
C       (C*1) TYP         = BURGESS & TULLY TRANSITION TYPE CODE
C       (R*8) C           = THE ADJUSTABLE PARAMETER ASSOCIATED
C                           WITH THE BURGESS AND TULLY
C                           TRANSFORMATION ( SEE REFERENCE (1)).
C       (L)   LNEG        = IF TYPE 2 GOES NEGATIVE AT HIGH
C                           TEMPERATURES THE COLLISION STRENGTH HAS
C                           A ZERO LIMITING VALUE AT INFINITY. 
C       (R*8) AIN         = THE EINSTEIN `A` CO-EFFICIENT. THIS
C                           IS READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (R*8) WVNOU       = THE WAVENUMBER OF THE UPPER LEVEL.
C                           THIS IS READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (R*8) WVNLO       = THE WAVENUMBER OF THE LOWER LEVEL.
C                           THIS IS READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (R*8) WTU         = THE STATISTICAL WEIGHT OF THE UPPER
C                           LEVEL. THIS IS OBTAINED BY
C
C       (R*8) WTL         = THE STATISTICAL WEIGHT OF THE LOWER
C                           LEVEL. THIS IS OBTAINED BY
C
C       (R*8) TEIN        = THE TEMPERATURE ARRAY (K). THIS
C                           DATA IS READ DIRECTLY FROM THE
C                           ADF04 TYPE FILE.
C       (R*8) UPSIN       = THE ARRAY CONTAING THE EFFECTIVE
C                           COLLISION STRENGTH. THIS DATA IS
C                           READ DIRECTLY FROM THE ADF04
C                           TYPE FILE.
C       (I*4) NV          = THE NUMBER OF TEMPERATURE/EFFECTIVE
C                           COLLISION STRENGTH PAIRS FOR A GIVEN
C                           TRANSITION.
C       (I*4) NVN         = THE NUMBER OF TEMPERATURES/EFFECTIVE
C                           COLLISION STRENGTH PAIRS FOR A GIVEN
C                           TRANSITION. THIS PARAMETER IS IN
C                           FACT THE NUMBER OF USER DEFINED
C                           TEMPERATURE POINTS AT WHICH THE
C                           EFFECTIVE COLLISION STRENGTH
C                           HAS TO BE EVALUATED AT.
C       (R*8) TEOUT       = THE TEMPERATURE RANGE FOR WHICH
C                           THE EFFECTIVE COLLISION STRENGTH IS
C                           REQUIRED.
C
C OUTPUT:
C
C       (R*8) UPSOUT      = THE ARRAY OF EFFECTIVE COLLISION
C                           STRENGTHS THAT ARE REQUIRED.
C
C
C       (R*8) E           = THE MATHEMATICAL CONSTANT E.
C       (R*8) CONST       = CLUSTER OF PHYSICAL CONSTANTS.
C                           SEE PAGE 12 OF REFERENCE (2).
C       (R*8) EIJIN       = THE TRANSITION ENERGY (RYD).
C       (R*8) FIJIN       = THE OSCILLATOR STRENGTH.
C       (R*8) ET          = GENERAL CONSTANT.
C       (R*8) C           = THE BURGESS C PARAMETER.
C       (R*8) X           = THE X ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (R*8) Y           = THE Y ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (R*8) DY          = DERIVATIVES AT INPUT KNOTS.
C                           SEE XXSPLN FOR FUTHER DETAILS.
C       (R*8) XOUT        = X ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (R*8) YOUT        = Y ARRAY ASSOCIATED WITH THE
C                           BURGESS AND TULLY TRANSFORMATION.
C       (R*8) r8fun1      = INTERPOLATING X COORDINATE
C                           TRANSFORMATION ( SEE SUBROUTINE
C                           XXSPLN ). EXTERNAL FUNCTION.
C       (I*4) NVMAX       = THE MAXIMUM NUMBER OF TEMPERTURES
C                           THAT CAN BE READ.
C       (I*4) NFIT        = NVMAX+1 - ALLOWS LIMIT POINT TO BE
C                           ADDED TO TYPE 1 AND 4 FITS.
C       (I*4) IOPT        = GENERAL PARAMETER ASSOCIATED WITH
C                           THE SUBROUTINE XXSPLN.
C       (I*4) I           = GENERAL VARIABLE WHICH IS USED AS
C                           A COUNTER.
C       (LOG) LSETX       = PARAMETER ASSOCIATED WITH THE
C                           SUBROUTINE XXSPLN.
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8CONST    ADAS      RETURNS FUNDAMENTAL ATOMIC CONSTANTS
C
C
C AUTHOR:   H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL.  0141-553-4196
C
C DATE:    04/06/98
C
C UPDATE:  
C
C VERSION: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION: 	1.2					DATE: 15/04/99
C MODIFIED:	Martin O'Mullane
C               - If type 1 or 4 add in the limit point 
C                 to the spline fit.
C		- Added support for type 4 transitions. 
C
C VERSION: 	1.3					DATE: 08/10/99
C MODIFIED:	Martin O'Mullane
C               - Certain type 2 and 3 transitions have a limiting
C                 point of zero at infinity. We deduce this by
C                 fitting and if the values go negative rerun with
C                 LNEG set and add y=0 at x=1 to the fit.
C
C VERSION : 	1.4					
C DATE    :     03-09-2009
C MODIFIED:	Martin O'Mullane
C               - Replace 'FINTX' with adaslib/maths 'R8FUN1'
C
C----------------------------------------------------------------
      INTEGER   NVMAX   ,  NFIT 
C----------------------------------------------------------------
      REAL*8    E , CONST
C----------------------------------------------------------------
      PARAMETER ( NVMAX = 14          , NFIT = NVMAX + 1 )
      PARAMETER ( CONST = 8.03231D+09 , E = 2.718281828D0  )
C----------------------------------------------------------------
      INTEGER   ITYPE    ,       NV      ,      NF      ,
     &          NVN      ,       I       ,      IOPT
      INTEGER   I4UNIT
C----------------------------------------------------------------
      REAL*8    AIN      ,       EIJIN   ,      WVNOU   ,
     &          WVNOL    ,       WTU     ,      WTL     ,
     &          FIJIN    ,       ET      ,
     &          C        ,       r8fun1  
      REAL*8    R8CONST  , RZ       ,       R2K
C----------------------------------------------------------------
      REAL*8    TEIN(NV)     , UPSIN(NV)    ,
     &          TEOUT(NVN)   , UPSOUT(NVN)
      REAL*8    X(NFIT)      , Y(NFIT)      , DY(NFIT)  ,
     &          XOUT(NVMAX)  , YOUT(NVMAX)
C----------------------------------------------------------------
      CHARACTER TYP*1
C----------------------------------------------------------------
      LOGICAL   LSETX    , LNEG
C----------------------------------------------------------------
      EXTERNAL  r8fun1
C----------------------------------------------------------------


C----------------------------------------------------------------
C  EXECUTE BURGESS TRANSFORMATION DEPENDING ON TRANSITION TYPE
C  SEE EQUATIONS 23 TO 38 IN REFERENCE (1).
C----------------------------------------------------------------

      RZ  = R8CONST('RY')
      R2K = R8CONST('RY2K')
	 
      READ(TYP,'(1I1)')ITYPE
      IF((ITYPE.LE.0).OR.(ITYPE.GT.4)) THEN
         WRITE(I4UNIT(-1),1001)
         WRITE(I4UNIT(-1),1002)
         STOP
      ENDIF
     
      EIJIN = (WVNOU-WVNOL)/RZ
      IF (EIJIN.GT.0.0D0) THEN
          FIJIN = (1.0D0/CONST)*WTU*AIN/(EIJIN*EIJIN*WTL)
      ELSE
          FIJIN = 0.0D0
      ENDIF
      
      

      DO 10 I = 1,NV
        ET   = R2K*EIJIN/TEIN(I)
        IF (ITYPE.EQ.1) THEN
            X(I) = 1.0D0-DLOG( C ) / DLOG (1.0D0/ET + C )
            Y(I) = UPSIN(I) /DLOG(1.0D0/ET + E )
        ELSEIF (ITYPE.EQ.2) THEN
            X(I) = (1.0D0/ET )/ ( (1.0D0/ET) + C )
            Y(I) = UPSIN(I)
        ELSEIF (ITYPE.EQ.3) THEN
            X(I) = (1.0D0/ET )/ ( (1.0D0/ET) + C )
            Y(I) = UPSIN(I)* ( (1.0D0/ET) + 1.0D0 )
        ELSEIF (ITYPE.EQ.4) THEN
            X(I) = 1.0D0-DLOG( C ) / DLOG (1.0D0/ET + C )
            Y(I) = UPSIN(I) /DLOG(1.0D0/ET + C )
        ENDIF
  10  CONTINUE
  
       
C----------------------------------------------------------------
C IF TYPE 1 OR 4 ADD IN LIMIT POINT TO FIT.
C----------------------------------------------------------------

      IF (ITYPE.EQ.1.OR.ITYPE.EQ.4) THEN
         NF    = NV + 1
         X(NF) = 1.0D0
         Y(NF) = 4.0D0*WTL*FIJIN/EIJIN
         IF (X(NF-1).EQ.1.0D0) NF = NF - 1       ! just might happen
      ELSE
      	NF=NV
      ENDIF
  
C----------------------------------------------------------------
C IF TYPE 2 OR 3  AND LNEG IS SET ADD IN LIMIT POINT TO FIT.
C----------------------------------------------------------------
      
      IF ( (ITYPE.EQ.2.OR.ITYPE.EQ.3).AND.LNEG ) THEN
         NF    = NV + 1
         X(NF) = 1.0D0
         Y(NF) = 0.0D0
         IF (X(NF-1).EQ.1.0D0) NF = NF - 1       ! just might happen
      ENDIF
  

      DO 20 I = 1,NVN
        ET   = R2K*EIJIN/TEOUT(I)
        IF (ITYPE.EQ.1) THEN
            XOUT(I) = 1.0D0 - DLOG( C ) / DLOG (1.0D0/ET + C )
        ELSEIF (ITYPE.EQ.2) THEN
            XOUT(I) = (1.0D0/ET )/ ( (1.0D0/ET) + C )
        ELSEIF (ITYPE.EQ.3) THEN
            XOUT(I) = (1.0D0/ET )/ ( (1.0D0/ET) + C )
        ELSEIF (ITYPE.EQ.4) THEN
            XOUT(I) = 1.0D0 - DLOG( C ) / DLOG (1.0D0/ET + C )
        ENDIF
  20  CONTINUE

C---------------------------------------------------------------
C COMPUTE SPLINE FIT  - SEE XXPLN FOR APPROPRIATE SETTINGS.
C---------------------------------------------------------------

      LSETX = .TRUE.
      IOPT  = 4
      CALL XXSPLN( LSETX , IOPT  , r8fun1 ,
     &             NF    , X     , Y     ,
     &             NVN   , XOUT  , YOUT  ,
     &             DY
     &           )
     

C---------------------------------------------------------------
C COMPUTE INVERSE OF THE BURGESS ANT TULLY TRANSFORMATION.
C---------------------------------------------------------------

      DO 40 I = 1,NVN
        ET   = R2K*EIJIN/TEOUT(I)
        IF (ITYPE.EQ.1) THEN
            UPSOUT(I)=YOUT(I)*DLOG((1.0D0/ET)+E)
        ELSEIF (ITYPE.EQ.2) THEN
            UPSOUT(I)=YOUT(I)
        ELSEIF (ITYPE.EQ.3) THEN
            UPSOUT(I)=YOUT(I)/ ( (1.0D0/ET)+ 1.0D0 )
        ELSEIF (ITYPE.EQ.4) THEN
            UPSOUT(I)=YOUT(I)*DLOG((1.0D0/ET)+C)
        ENDIF
  40  CONTINUE

      RETURN

C---------------------------------------------------------------
 1001 FORMAT(1X,32('*'),' BFTRAN ERROR ',32('*')//
     &       1X,'TYPE OUT OF RANGE: ')
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C---------------------------------------------------------------

      END
