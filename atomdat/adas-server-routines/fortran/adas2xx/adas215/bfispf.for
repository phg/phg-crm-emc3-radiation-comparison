C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bfispf.for,v 1.1 2004/07/06 11:38:52 whitefor Exp $ Date $Date: 2004/07/06 11:38:52 $
C
      SUBROUTINE BFISPF( IPAN   , LPEND  ,
     &                   NDLNE  , NVMAX  ,
     &                   IL     ,
     &                   ICNTE  , ICNTH  , ICNTR ,
     &                   PECODE , TECODE , IE1A  , IE2A , CEA , STRGA ,
     &                   NV     , TSCEF  ,
     &                   NSTRN1 , ISTRN1 ,
     &                   IFOUT  , NVN    , TOUT  ,
     &                   LOSEL  
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFISPF *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS215
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDLNE    = MAXIMUM NUMBER OF COMPOSITE LINES ALLOWED
C  INPUT : (I*4)   NVMAX    = MAXIMUM NUMBER OF INPUT TEMPS. ALLOWED
C
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   ICNTE    = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)   ICNTH    = NUMBER OF FREE ELECTRON RECOMBINATIONS
C  INPUT : (I*4)   ICNTR    = NO. OF CHARGE EXCHANGE RECOMBINATIONS
C
C  INPUT : (C*1)  PECODE()  = ELECTRONIC TRANSITION PLOT SELECTOR:
C                             ' ' => do not plot
C                             'P' or 'p' => plot
C  INPUT ; (C*1)  TECODE()  = ELECTRONIC TRANSITION: DATA TYPE POINTER:
C                             ' ' => unassigned
C                             '1' => dipole
C                             '2' => non-dipole, non-spin change
C                             '3' => spin change
C                             '4' => small oscillator strength
C  INPUT : (I*4)   IE1A()   = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)   IE2A()   = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C  I/O   : (R*8)   CEA()    = ELECTRON IMPACT TRANSITION:
C                              BURGESS & TULLY C-VALUES
C  INPUT : (C*22)  STRGA()  = LEVEL DESIGNATIONS
C  INPUT : (I*4)   NV       = NUMBER OF INPUT TEMPERATURES
C  INPUT : (R*8)   TSCEF(,) = ELECTRON TEMPERATURES (UNITS: K)
C
C  OUTPUT: (I*4)   NSTRN1   = NUMBER OF LINES CHOSEN FOR FIRST COMPOSITE
C                             ASSEMBLY
C  OUTPUT: (I*4)   ISTRN1() = SELECTED TRANSITION INDEXES FOR FIRST
C                             COMPOSITE LINE ASSEMBLY
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C  OUTPUT: (I*4)   NVN      = NUMBER OF OUTPUT TEMPERATURES ( 1 -> 14)
C  OUTPUT: (R*8)   TOUT()   = OUTPUT TEMPERATURES (UNITS: SEE 'IFOUT')
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => OUTPUT TEMPERATURES PRESENT
C                             .FALSE. => NO OUTPUT TEMPERATURES 
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   I        = LOOP INCREMENT
C          (I*4)   LOGIC    = USED TO PIPE LOGICAL VALUES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    04/06/98
C
C UPDATE: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IPAN      , NDLNE      , NVMAX   ,
     &           IL        , NV         , NVN     , IFOUT   ,
     &           ICNTE     , ICNTH      , ICNTR   ,
     &           NSTRN1    
      INTEGER    I         , J       
C-----------------------------------------------------------------------
      LOGICAL    LPEND     , LOSEL
C-----------------------------------------------------------------------
      INTEGER    IE1A(ICNTE)            , IE2A(ICNTE)
      INTEGER    ISTRN1(NDLNE)          
C-----------------------------------------------------------------------
      REAL*8     TSCEF(NVMAX,3)         , TOUT(NVMAX)
      REAL*8     CEA(ICNTE)
C-----------------------------------------------------------------------
      CHARACTER  STRGA(IL)*22
      CHARACTER  PECODE(ICNTE)*1        , TECODE(ICNTE)*1
C-----------------------------------------------------------------------
      INTEGER    LOGIC     , I4UNIT
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO    
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
C
C     BIZARRELY THE NEXT LINE APPEARS TO BE NEEDED TO STOP THE HP
C     CRASHING
C
      WRITE(I4UNIT(-1),*) " "

      IF( LPEND ) THEN
          WRITE( PIPEOU, * ) ONE
      ELSE
          WRITE( PIPEOU, * ) ZERO
      ENDIF
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NDLNE
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NVMAX
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) IL
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) NV
      CALL XXFLSH( PIPEOU )

      DO 5, I=1, NVMAX
        DO 3 J=1,3
          WRITE( PIPEOU, * ) TSCEF(I,J)
          CALL XXFLSH( PIPEOU )
3       CONTINUE
5     CONTINUE

      WRITE( PIPEOU, * ) ICNTE
      WRITE( PIPEOU, * ) ICNTH
      CALL XXFLSH( PIPEOU )

      WRITE( PIPEOU, * ) ICNTR
      CALL XXFLSH( PIPEOU )

      WRITE(I4UNIT(-1),*) " "

      DO 1, I=1, ICNTE
          WRITE( PIPEOU, * ) IE1A(I)
          CALL XXFLSH( PIPEOU )
1     CONTINUE

      DO 2, I=1, ICNTE
          WRITE( PIPEOU, * ) IE2A(I)
          CALL XXFLSH( PIPEOU )
2     CONTINUE

      DO 4, I=1, IL
          WRITE( PIPEOU, '(A22)' ) STRGA(I)
          CALL XXFLSH( PIPEOU )
4     CONTINUE

      DO 6, I=1, ICNTE
          WRITE( PIPEOU, '(A1)') PECODE(I)
          CALL XXFLSH( PIPEOU )
6     CONTINUE

      DO 7, I=1, ICNTE
          WRITE( PIPEOU, '(A1)' ) TECODE(I)
          CALL XXFLSH( PIPEOU )
7     CONTINUE

      DO 8, I=1, ICNTE
          WRITE( PIPEOU, '(F5.2)' ) CEA(I)
          CALL XXFLSH( PIPEOU )
8     CONTINUE

C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
C
      IF (.NOT.LPEND) THEN
          READ(PIPEIN,*) NSTRN1

          IF( NSTRN1.GT.0 ) THEN
             READ(PIPEIN,*) (ISTRN1(I),I=1,NSTRN1)
          
		 DO J=1,ICNTE
            	READ(PIPEIN,'(A1)') PECODE(J) 
             	READ(PIPEIN,'(A1)') TECODE(J)
		 ENDDO

		 DO J=1,ICNTE
            	READ(PIPEIN,*) CEA(J)
		 ENDDO
		 
          ENDIF

          LOSEL = .FALSE.
          READ(PIPEIN,*) IFOUT
          READ(PIPEIN,*) NVN

          IF( NVN.GT.0 ) THEN
                 LOSEL = .TRUE.

		 DO J=1,NVN
            	READ(PIPEIN,*) TOUT(J)
		 ENDDO
		 
          ENDIF

      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
