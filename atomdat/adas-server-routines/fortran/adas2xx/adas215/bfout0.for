C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bfout0.for,v 1.5 2019/04/29 19:07:12 mog Exp $ Date $Date: 2019/04/29 19:07:12 $
C
      SUBROUTINE BFOUT0( IUNIT  , 
     &                   DATE   , PRGTYP , DSNC80 , DSNP80 ,
     &                   TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH  ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA  , WA ,
     &                   ER     ,
     &                   NV     , TSCEF  ,
     &                   NVN    , TSCEFO ,
     &                   PECODE , TECODE , IE1A   , IE2A   , CEA  , 
     &                   CADAS  , LNEGLIST
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFOUT0 *********************
C
C  PURPOSE: TO OUTPUT ION SPECIFICATIONS, INDEXED ENERGY LEVELS AND
C           WAVE NUMBERS RELATIVE  TO GROUND TO STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS215
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  IUNIT  = OUTPUT STREAM NUMBER
C  INPUT :  (I*4)  NDTRN  = MAX. NO. OF TRANS. IN AN ADF04 FILE.
C  INPUT :  (C*8)  DATE   = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (C*1)  PRGTYP = PROGRAM TYPE
C  INPUT :  (C*(*))DSNC80 = INPUT COPASE DATA SET NAME
C  INPUT :  (C*(*))DSNP80 = INPUT PROTON DATA SET NAME
C
C  INPUT :  (C*3)  TITLED = ELEMENT SYMBOL.
C  INPUT :  (I*4)  IZ     =  RECOMBINED ION CHARGE
C  INPUT :  (I*4)  IZ0    =         NUCLEAR CHARGE
C  INPUT :  (I*4)  IZ1    = RECOMBINING ION CHARGE
C                            (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT :  (R*8)  BWNO   = IONISATION POTENTIAL (CM-1)
C
C  INPUT :  (I*4)  ICNTE  = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTP  = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTR  = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT :  (I*4)  ICNTH  = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT :  (I*4)  IL     = NUMBER OF ENERGY LEVELS
C
C  INPUT :  (I*4)  IA()   = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT :  (I*4)  ISA()  = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT :  (I*4)  ILA()  = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT :  (R*8)  XJA()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  WA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  INPUT : (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           FOR LEVEL 'IA()'
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURE (NOTE: TE=TP=TH)
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C  INPUT : (I*4)  NVN     = OUTPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  TSCEFO(,)= OUTPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURE (NOTE: TE=TP=TH)
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C  INPUT : (C*1) PECODE()  = PLOT SEL. CODE FOR EL-TRANS. FROM IDL
C                            1ST DIM.: EL-TRANS. INDEX 
C  INPUT : (C*1) TECODE()  = TYPE  SEL. CODE FOR EL-TRANS. FROM IDL
C                            1ST DIM.: EL-TRANS. INDEX 
C  INPUT : (I*4) IE1A()    = EL-TRANS. LOWER LEVEL INDEX FROM ADF04 FILE
C                            1ST DIM.: ELECTRON IMPACT TRANSITION INDEX
C  INPUT : (I*4) IE2A()    = EL-TRANS UPPER LEVEL INDEX FROM ADF04 FILE
C                            1ST DIM.: ELECTRON IMPACT TRANSITION INDEX
C  INPUT : (R*8) CEA()     = ASSIGNED C-VALUE FOR EL-TRANS.
C                            1ST DIM.: EL-TRANS. INDEX.
C  INPUT : (C*80) CADAS    =  HEADER FOR TEXT OUTPUT
C  INPUT : (L)    LNEGLIST()= LIST OF TRANSITIONS WHICH WENT NEGATIVE
C                            1ST DIM.: EL-TRANS. INDEX 
C
C          (R*8)  WN2RYD  = PARAMETER =
C                           WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (R*8)  BRYDO   = IONISATION POTENTIAL (RYDBERGS)
C          (R*8)  BWN     = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           WAVE NUMBERS (CM-1).
C          (R*8)  BRYD    = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           RYDBERGS.
C
C          (I*4)  I       = GENERAL USE
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHLCYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    04/06/98
C
C UPDATE: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.	
C
C UPDATE: 	1.2					DATE: 16/04/99
C MODIFIED:	MARTIN O'MULLANE & RICHARD MARTIN
C		- MINOR CORRECTION TO FORMAT STATEMENT 1011
C
C UPDATE: 	1.3					DATE: 08/10/99
C MODIFIED:	Martin O'Mullane
C		- Added information on whether type 2 has gone negative
C
C UPDATE: 	1.4					DATE: 21-03-00
C MODIFIED:	RICHARD MARTIN
C		Removed NDTRN from subroutine statement.
C
C UPDATE  : 	1.5
C DATE    :     2904-2019
C MODIFIED:	Martin O'Mullane
C		- Length of the input file names set by argument length.
C
C-----------------------------------------------------------------------
      REAL*8     WN2RYD
C-----------------------------------------------------------------------
      PARAMETER( WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
      INTEGER    I
      INTEGER    IUNIT         , NDTRN          ,
     &           IZ            , IZ0            , IZ1        ,
     &           ICNTE         , ICNTP          , ICNTR      , ICNTH  ,
     &           IL            , NV             , NVN
C-----------------------------------------------------------------------
      PARAMETER( NDTRN = 2000 )     
C-----------------------------------------------------------------------
      REAL*8     BWNO          , BWN            , BRYDO      , BRYD
C-----------------------------------------------------------------------
      CHARACTER  TITLED*3      , DATE*8         , PRGTYP*1   ,
     &           DSNC80*(*)    , DSNP80*(*)     , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    IA(IL)        , ISA(IL)        , ILA(IL)
      INTEGER    IE1A(NDTRN)   , IE2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     XJA(IL)       , WA(IL)         , ER(IL)
      REAL*8     TSCEF(14,3)   , TSCEFO(14,3)   , CEA(NDTRN)    
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18     
      CHARACTER  PECODE(NDTRN)*1  , TECODE(NDTRN)*1  , NC(NDTRN)*1
C-----------------------------------------------------------------------
      LOGICAL    LNEGLIST(NDTRN)
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
      BRYDO = WN2RYD * BWNO
C
C-----------------------------------------------------------------------
C WRITE OUT ADAS HEADER 
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1000) CADAS(2:80)
C
C-----------------------------------------------------------------------
C OUTPUT ION AND ENERGY LEVEL DETAILS
C-----------------------------------------------------------------------

C Fill NC block with transitions which have gone negative

      do i=1, icnte
        if (lneglist(i)) then
           nc(i) = '*'
        else
           nc(i) = ' '
        endif
      end do


      IF (PRGTYP.EQ.'M') THEN
         WRITE(IUNIT,1001) 'METASTABLE POPULATION ','ADAS205',DATE
      ELSEIF (PRGTYP.EQ.'T') THEN
         WRITE(IUNIT,1001) 'TEMPERATURE REGRIDDING','ADAS215',DATE
      ELSE
         WRITE(IUNIT,1001) 'LINE POWER CALCULATION','ADAS206',DATE
      ENDIF
      WRITE(IUNIT,1002) DSNC80
      IF (DSNP80(1:4).NE.'NULL') WRITE(IUNIT,1009) DSNP80
      WRITE(IUNIT,1003) TITLED , IZ , IZ0 , IZ1 , BWNO , BRYDO
      WRITE(IUNIT,1004)
C
      DO 1 I=1,IL
         BWN  = BWNO  - WA(I)
         BRYD = BRYDO - ER(I)
         WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                     ISA(I) , ILA(I)    , XJA(I) ,
     &                     WA(I)  , ER(I)     ,
     &                     BWN    , BRYD
    1 CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT 'INPUT ADF04 COPASE FILE' TEMPERATURES
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1006)
         DO 2 I=1,NV
            WRITE(IUNIT,1007) I , TSCEF(I,1) , TSCEF(I,2) , TSCEF(I,3)
    2    CONTINUE
C
C-----------------------------------------------------------------------
C INPUT ADF04 FILE INFORMATION
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1008) ICNTE , ICNTP , ICNTH , ICNTR
C
C-----------------------------------------------------------------------
C OUTPUT ADF04 FILE TEMPERATURES
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1010)
         DO 3 I=1,NVN
            WRITE(IUNIT,1007) I , TSCEFO(I,1) , TSCEFO(I,2) , 
     &                        TSCEFO(I,3)
    3    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT ADF04 FILE BURGESS & TULLY TRANSITION PARAMETERS
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1011)
      
      WRITE(IUNIT,1012) 
     &  (I, IE2A(I),IE1A(I),TECODE(I),CEA(I),NC(I), i=1,icnte)

      WRITE(IUNIT,1020)
      
C-----------------------------------------------------------------------
C
 1000 FORMAT(A79)
 1001 FORMAT(27('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,27('*')/)
 1002 FORMAT('INPUT ADF04 FILE NAME : ',A80/)
 1003 FORMAT(1X,'ION',6X,'NUCLEAR',8X,'RECOMBINING',6X,
     &           7('-'),' IONIZATION POTENTIAL ',7('-')/
     &           9X,'CHARGE (Z0)',4X,'ION CHARGE (Z1)',
     &        4X,'(wave number <cm-1>)',5X,'(rydbergs)'/
     &        1X,78('-')/
     &        1X,1A3,I2,7X,I3,14X,I3,10X,F15.0,5X,F15.7//)
 1004 FORMAT(55('-'),' ENERGY LEVELS ',55('-')/
     &        2X,'INDEX',4X,'CONFIGURATION',3X,'(2S+1)L(J)',7X,
     &           5('-'),' ENERGY RELATIVE TO LEVEL 1 ',6('-'),4X,
     &           'ENERGY RELATIVE TO IONISATION POTENTIAL'/
     &       45X,2( '(wave number <cm-1>)',5X,'(rydbergs)' ,8X )/
     &        1X,125('-'))
 1005 FORMAT( 1X,I4,2X,A18,2X,'(',I1,')',I1,'(',F6.1,')',
     &        2(6X,F15.0,5X,F15.7,2X) )
 1006 FORMAT(/'-- INPUT ADF04  FILE TEMPERATURES: (TE=TP=TH) --'/
     &            1X,'INDEX',4X,'(kelvin)',8X,'(eV)',8X,'(reduced)'/
     &            1X,48('-'))
 1007 FORMAT(2X,I2,1P,3(4X,D10.2))
 1008 FORMAT(/'INPUT ADF04  FILE INFORMATION:'/,30('-')/
     &       'NUMBER OF ELECTRON IMPACT TRANSITIONS    =',1X,I4/
     &       'NUMBER OF PROTON   IMPACT TRANSITIONS    =',1X,I4/
     &       'NUMBER OF CHARGE EXCHANGE RECOMBINATIONS =',1X,I4/
     &       'NUMBER OF FREE   ELECTRON RECOMBINATIONS =',1X,I4)
 1009 FORMAT('INPUT PROTON FILE NAME: ',A80/)
 1010 FORMAT(/'-- OUTPUT ADF04 FILE TEMPERATURES: (TE=TP=TH) --'/
     &            1X,'INDEX',4X,'(kelvin)',8X,'(eV)',8X,'(reduced)'/
     &            1X,48('-'))
 1011 FORMAT(/'-- OUTPUT ADF04 FILE INTERPOLATION PARAMETERS --',/,
     &            4(1X,'IND', 2X,' IU - IL',2X,'T',2X,'C-VAL',5X),/,
     &         116('-'))
 1012 FORMAT(4(1X,I3, 2X,I3,' -',I3 ,2X,1A1,2X,F5.3,A1,4X))
 1020 FORMAT(/,'* - LIMIT POINT OF 0 AT INFINITY ADDED TO THIS',
     &       ' TRANSITION FOR EXTRAPOLATION')
C-----------------------------------------------------------------------
      RETURN
      END
