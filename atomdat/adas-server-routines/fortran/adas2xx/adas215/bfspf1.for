C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bfspf1.for,v 1.1 2004/07/06 11:39:35 whitefor Exp $ Date $Date: 2004/07/06 11:39:35 $
C
      SUBROUTINE BFSPF1( DSFULL     , LOSEL      , LDEF1    ,  
     &                   LGRAPH     , L2FILE     , SAVFIL   ,
     &                   LPASS      , DSNPAS     ,
     &                   XMIN       , XMAX       , YMIN     , YMAX    , 
     &                   LPEND      , L2REP      , CADAS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFSPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS215
C
C  SUBROUTINE:
C
C  INPUT :   (C*80)  DSFULL   = DATA FILE NAME
C  INPUT :   (L*4)   LOSEL    = .TRUE.  => OUTPUT TEMPERATURES SET
C                               .FALSE. => OUTPUT TEMPERATURES NOT SET
C  OUTPUT:   (L*4)   LDEF1    = .TRUE.  => USER SELECTED AXES LIMITS
C				.FALSE. => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE TEXT DATA TO FILE
C				.FALSE. => DO NOT SAVE TEXT DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING TEXT DATA
C  OUTPUT:   (L*4)   LPASS    = .TRUE.  => STORE OUTPUT TCX DATA IN FILE
C				.FALSE. => DO NOT STORE
C  OUTPUT:   (C*80)  DSNPAS   =  TCX DATA OUTPUT FILENAME
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR ENERGY UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR ENERGY UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS FOR CROSS-SECTION
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT FOR CROSS-SECTION
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (L*4)   L2REP    = .TRUE.  => REPLACE OUTPUT FILE
C                               .FALSE. => DON'T REPLACE OUTPUT FILE
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    28/05/98 
C
C UPDATE:
C 
C VERSION: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
      REAL*8       XMIN        , XMAX        , YMIN        , YMAX
C-----------------------------------------------------------------------
      CHARACTER    DSFULL*80   , SAVFIL*80   , DSNPAS*80   , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      , LOSEL    ,
     &             LDEF1       , LPASS       , L2REP
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER ( PIPEIN=5     , PIPEOU=6    , ONE=1       , ZERO=0 )
C-----------------------------------------------------------------------
C  WRITE TO IDL
C-----------------------------------------------------------------------

         IF (LOSEL) THEN
            WRITE(PIPEOU,*) ONE
         ELSE
            WRITE(PIPEOU,*) ZERO
         ENDIF
         CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN

         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LGRAPH = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               LDEF1  = .TRUE.
	       READ(PIPEIN,*) XMIN
	       READ(PIPEIN,*) XMAX
	       READ(PIPEIN,*) YMIN
	       READ(PIPEIN,*) YMAX
	    ELSE
	       LDEF1  = .FALSE.
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF
C
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
            CALL XXADAS(CADAS)
            READ(PIPEIN,*) ILOGIC
            IF (ILOGIC .EQ. ONE) THEN 
               L2REP = .TRUE.
            ELSE
               L2REP = .FALSE.
            ENDIF
         ENDIF

         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC.EQ.ONE) THEN
            LPASS = .TRUE.
            READ(PIPEIN,'(A)') DSNPAS
         ELSE
            LPASS = .FALSE.
         ENDIF
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END

