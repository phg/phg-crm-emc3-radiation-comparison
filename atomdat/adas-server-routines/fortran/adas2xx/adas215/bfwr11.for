C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas215/bfwr11.for,v 1.3 2019/05/09 17:15:26 mog Exp $ Date $Date: 2019/05/09 17:15:26 $
C
       SUBROUTINE BFWR11( IUNT11 , DSNINP , 
     &                    NDMET  , NDLEV  , NDTRN , NVMAX ,
     &                    DATE   , 
     &                    TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
     &                    NPL    , BWNOA  , LBSETA, PRTWTA, CPRTA ,
     &                    IL     ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA   , WA ,
     &                    CPLA   , NPLA   , IPLA  , ZPLA  ,
     &                    CIONP  ,
     &                    NVN    , SCEFN  ,
     &                    ITRAN  , MAXLEV ,
     &                    TCODE  , I1A    , I2A   , AVAL  , SCOMN
     &                  )
       IMPLICIT NONE
C----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BFWR11 *********************
C
C  PURPOSE:  PRODUCES AN ADF04 TYPE FILE, WHERE THE CONTENTS IS
C            CONSIDERED AS THE OUTPUT DATA SET FROM ADAS215.
C
C  CALLING PROGRAM: ADAS215
C
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNT11  = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (C*(*))DSNINP  = NAME OF INPUT ADF04 FILE
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF PARENTS 
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS 
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS 
C  INPUT : (I*4)  NVMAX   = MAX. NUMBER OF TEMPERATURES 
C  INPUT : (C*8)  DATE    = DATE (AS DD/MM/YY).
C  INPUT : (C*10) USERID  = USER IDENTIFIER OF CODE EXECUTOR.
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C  INPUT : (I*4)  NPL     = NO. OF PARENTS ON FIRST LINE OF ADF04 FILE
C  INPUT : (R*8)  BWNOA() = IONISATION POTENTIAL (CM-1) OF PARENTS
C                           1ST.DIM.: PARENT INDEX
C  INPUT : (R*8)  PRTWTA()= PARENT WEIGHT FOR BWNOA()
C                           1ST.DIM.: PARENT INDEX
C  INPUT : (C*9)  CPRTA()  = PARENT NAME IN BRACKETS
C                            1ST DIM.: PARENT INDEX 
C  INPUT : (L*4)  LBSETA() = .TRUE.  - PARENT WEIGHT SET FOR BWNOA()
C                            .FALSE. - PARENT WEIGHT NOT SET FOR BWNOA()
C
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C  INPUT : (C*1)  CPLA()  = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IA()'
C                           INTEGER - PARENT IN BWNOA() LIST
C                           'BLANK' - PARENT BWNOA(1)
C                             'X'   - DO NOT ASSIGN A PARENT
C                           1ST DIM.: LEVEL INDEX 
C  INPUT : (I*4)  NPLA()  = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIM.: PARENT INDEX
C  INPUT : (I*4)  IPLA(,) = PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIM.: PARENT INDEX
C                           2ND DIM.: LEVEL INDEX
C  INPUT : (R*8)  ZPLA(,  = EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIM.: PARENT INDEX
C  INPUT : (C*92) CIONP   = STRING CONTAINING LEVEL TERMINATOR AND
C                           IONISATION POTENTIALS
C
C  INPUT : (I*4)  NVN     = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  SCEFN() = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C                           (INITIALLY JUST THE MANTISSA. SEE 'ITPOW()')
C                           (NOTE: TE=TP=TH IS ASSUMED)
C
C  INPUT : (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C  INPUT : (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C
C  INPUT : (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C  INPUT : (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            SIGNED PARENT      INDEX (CASE 'H' & 'R')
C  INPUT : (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C  INPUT : (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C  INPUT : (R*8)  SCOMN(,)= TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXNAME     ADAS      FINDS REAL NAME OF USER
C          XXWSTR     ADAS      WRITES STRING TO A UNIT WITH TRAILING
C                               BLANKS REMOVED
C
C
C  AUTHOR : H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL.  0141-553-4196
C
C  DATE:    04/06/96
C
C  UPDATE: 
C
C VERSION: 	1.1					DATE: 09/08/98
C MODIFIED:	RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.				
C
C VERSION: 	1.2					DATE: 15/04/99
C MODIFIED:	Martin O'Mullane
C		- Add real name of user via XXNAME.
C		- Remove trailing blanks from adf04 file.
C
C VERSION: 	1.3					
C DATE:         09-05-2019
C MODIFIED:	Martin O'Mullane
C		- Length of dsninp set by argument.
C               - Increase length of strg2 to 80 characters.
C
C----------------------------------------------------------------------
       INTEGER  NDLEV       , NDTRN          , NDMET      , NVMAX
       INTEGER  IUNT11
C----------------------------------------------------------------------
       INTEGER   IZ          , IZ0            , IZ1        ,
     &           IL          , NVN            , ITRAN      ,
     &           MAXLEV      , IT             , NPL        ,
     &           I           , ICHAR          , IFIRST     ,
     &           ILAST       , J              ,
     &           IPOS        , IQS            , L1         , L2
C-----------------------------------------------------------------------
       REAL*8    BWNO        , Z1
C----------------------------------------------------------------------
       INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &           I1A(NDTRN)  , I2A(NDTRN)
       INTEGER   IPLA(NDMET,NDLEV)            , NPLA(NDLEV)
C----------------------------------------------------------------------
       REAL*8   BWNOA(NDMET)   , PRTWTA(NDMET)
       REAL*8   SCEFN(NVMAX)
       REAL*8   XJA(NDLEV)     , WA(NDLEV)           ,
     &          AVAL(NDTRN)    , SCOMN(NVMAX,NDTRN)
       REAL*8   ZPLA(NDMET,NDLEV)             
C-----------------------------------------------------------------------
       CHARACTER DSNINP*(*)     
       CHARACTER TITLED*3      , TCODE(NDTRN)*1 , CSTRGA(NDLEV)*18
       CHARACTER CPLA(NDLEV)*1 , CPRTA(NDMET)*9
       CHARACTER CDELIM*7      , C9*9
       CHARACTER STRING*133    , BLANKS*133
       CHARACTER STRG1*75      , STRG2*80
       CHARACTER CIONP*92
C-----------------------------------------------------------------------
       CHARACTER DATE*8        , REALNAME*30
C-----------------------------------------------------------------------
       LOGICAL  LBSETA(NDMET)
C-----------------------------------------------------------------------
       DATA  CDELIM/' ()<>{}'/
       DATA BLANKS/' '/
C-----------------------------------------------------------------------

       CALL XXNAME(REALNAME)
       CALL XXSLEN(DSNINP,L1,L2)
       
C-----------------------------------------------------------------------
C  WRITE OUT FIRST LINE
C----------------------------------------------------------------------
       
       STRING = BLANKS
       STRG1  = BLANKS(1:75)
       IF(NPL.GT.0) THEN
           IPOS=5
           DO I=1,NPL
             CALL XXSLEN(CPRTA(I),IFIRST,ILAST)
             ICHAR=ILAST-IFIRST+1
             WRITE(STRG1(IPOS:IPOS+9),'(F10.0)') BWNOA(I)
             WRITE(STRG1(IPOS+10:IPOS+10+ICHAR),'(A)')
     &               CPRTA(I)(IFIRST:ILAST)
             IPOS=IPOS+10+ICHAR
           ENDDO
           WRITE(STRING,'(1A3,I2,2I10,1A75)')TITLED,IZ,IZ0,IZ1,STRG1
       ELSE
           WRITE(STRING,1001)TITLED,IZ,IZ0,IZ1,BWNO
       ENDIF
       CALL XXWSTR(IUNT11,STRING)

       DO 80 I = 1,IL
         STRING = BLANKS
         STRG2  = BLANKS(1:80)
         IF(NPLA(I).LE.0) THEN
             WRITE(STRG2(1:16),'(F16.1)')WA(I)
         ELSEIF((NPLA(I).EQ.1).AND.(CPLA(I).EQ.' ')) THEN
             WRITE(STRG2(1:16),'(F16.1)')WA(I)
         ELSEIF((NPLA(I).EQ.1).AND.(CPLA(I).EQ.'X')) THEN
             WRITE(STRG2(1:16),'(F16.1)')WA(I)
             STRG2(20:22)='{X}'
         ELSEIF((NPLA(I).GE.1).AND.
     &       ((CPLA(I).NE.' ').OR.(CPLA(I).NE.'X'))) THEN
             WRITE(STRG2(1:16),'(F16.1)')WA(I)
             IPOS=19
             DO J=1,NPLA(I)
               WRITE(STRG2(IPOS:IPOS+8),'('' {'',I1,''}'',
     &                1F5.3)')IPLA(J,I),ZPLA(J,I)
               IPOS=IPOS+9
             ENDDO
         ENDIF
             
         WRITE(STRING,1000)I,CSTRGA(I),ISA(I),ILA(I),XJA(I),
     &                     STRG2
         CALL XXWSTR(IUNT11,STRING)
   80  CONTINUE
       CALL XXWSTR(IUNT11,CIONP)

C----------------------------------------------------------------------
C  WRITE OUT ZEFF, IQS AND TEMPERATURE LINE
C----------------------------------------------------------------------

       STRING = BLANKS
       Z1  = IZ1
       IQS = 3
       WRITE(STRING,'(F5.2,I5)')Z1,IQS
       IPOS = 0
       DO 90 IT = 1,NVN
         WRITE(C9,'(1PD9.2)') SCEFN(IT)
         STRING(17+IPOS:21+IPOS) = C9(1:5)
         STRING(22+IPOS:24+IPOS) = C9(7:9)
         IPOS =IPOS+8
   90  CONTINUE
       CALL XXWSTR(IUNT11,STRING)

C----------------------------------------------------------------------
C  WRITE OUT TRANSITION LINES
C----------------------------------------------------------------------

       DO 300 I = 1,ITRAN
         STRING = BLANKS
         WRITE(STRING,'(1X,I3,I4)')I2A(I),I1A(I)
         WRITE(C9,'(1PD9.2)') AVAL(I)
         STRING(9:13) = C9(1:5)
         STRING(14:16) = C9(7:9)
         IPOS = 0
         DO 210 IT = 1,NVN
           WRITE(C9,'(1PD9.2)') SCOMN(IT,I)
           STRING(17+IPOS:21+IPOS) = C9(1:5)
           STRING(22+IPOS:24+IPOS) = C9(7:9)
           IPOS =IPOS+8
  210    CONTINUE
         CALL XXWSTR(IUNT11,STRING)
  300  CONTINUE
       WRITE(IUNT11,'(1A4)')'  -1'
       WRITE(IUNT11,'(1A8)')'  -1  -1'


       WRITE(IUNT11,1003)'ADAS215',DSNINP(1:L2),REALNAME,DATE

       RETURN

C----------------------------------------------------------------------
 1000  FORMAT(I5,1X,1A18,'(',I1,')',I1,'(',F4.1,')',A)
 1001  FORMAT(1A3,I2,2I10,F15.1)
 1002  FORMAT(1A3,I2,2I10,F15.1)
 1003  FORMAT('C',79('-')/
     &        'C'/
     &        'C  File generated by altering the temperature set of ',
     &        'an ADF04 file'/
     &        'C'/
     &        'C  Program     : ',1A7/
     &        'C'/
     &        'C  Source file : ',A,/
     &        'C'/
     &        'C  Producer    : ',A,/
     &        'C'/
     &        'C  Date        : ',1A8/
     &        'C',79('-'))
C----------------------------------------------------------------------

      END
