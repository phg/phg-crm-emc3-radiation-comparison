CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas210/baubnd.for,v 1.3 2004/07/06 11:36:11 whitefor Exp $ Date $Date: 2004/07/06 11:36:11 $
CX
       SUBROUTINE BAUBND( ITRAN  , ITRANB , ITRAN2 , I1A   , I2A   ,
     &                    I1BA   , I2BA   , I1A2   , I2A2  , AVAL  , 
     &                    AVALB  , SCOM   , SCOMB  , SCOM2 , NV2   ,
     &                    NDTRN  , NVMAX  , TCODE  , TCODEB, TCODE2, 
     &                    INDBL  , NJLEVX , SCOMU  , TCODEU, I1UA  ,
     &                    I2UA   , PRERAT , ILA2   , ISA2  , XJA2  ,
     &                    NDLEV  , IL2    , BNDLS  , NCHK  , IUA   ,
     &                    ILUA   , ISUA   , CSTRGUA, WUA   , XJUA  ,
     &                    IA     , ILA    , ISA    , CSTRGS, WA    ,
     &                    XJA    , IA2    , CSTRGA2, WA2   , NBLEVX,
     &                    INDUL  , NCHKU  , ISORT  , INDBS , AVALU ,
     &                    AVAL2  , PREA   , ITRANU , IUL   , XLSA  ,
     &                    BNDPR  , NBCPRT , IL3    , CPRTAU, IA3   , 
     &                    ILA3   , ISA3   , XJA3   , WA3   , BWNO2 , 
     &                    NPL2   , BWNOA2 , PRTWTA2, CPRTA2, NDMET ,
     &                    IPMDFLG, BWNOAU , CPLA2  , NPLA2 , IPLA2 ,
     &                    ZPLA2  , CPLAU  , NPLAU  , IPLAU , ZPLAU ,
     &                    IMRK   , PRTWTAU, IRCHK  , NZEROS)
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BAUBND *********************
C
C  PURPOSE: TO UNBUNDLE A SPECIFIC ION FILE ACCORDING TO THE SPLIT UP
C           FRACTIONS OBTAINED FROM A SUPERSTRUCTURE FILE, FILLING IN 
C           WITH THE STATISTICAL METHOD OF SARAPH, SEATON & SHEMMING 
C           (1969), WHEN NO DATA IS AVAILABLE.
C
C  CALLING PROGRAM: ADAS210
C
C  SUBROUTINE:
C
C  INPUT:
C            (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C            (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C            (I*4)  NDMET   = MAX. NUMBER OF METASTABLES ALLOWED
C            (I*4)  NVMAX   = MAX. NUMBER OF TEMPERATURES
C            (I*4)  ITRAN   = SUPERSTRUCTURE FILE: NO. OF TRANSITIONS
C            (I*4)  ITRANB  = BUNDLED SUPERSTRUCTURE FILE: NO. OF TRANSITIONS
C            (I*4)  ITRAN2  = INPUT DATA FILE: NO. OF TRANSITIONS
C            (I*4)  I1A()   = TRANSITION: IN SUPERSTRUCTURE FILE
C                             LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             SIGNED PARENT INDEX (CASE 'H','R' & 'I')
C            (I*4)  I2A()   = TRANSITION: IN SUPERSTRUCTURE FILE
C                             UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             CAPTURING LEVEL INDEX (CASE 'H','R' & 'I')
C            (I*4)  I1BA()  = TRANSITION: IN BUNDLED SUPERSTRUCTURE FILE
C                             LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             SIGNED PARENT INDEX (CASE 'H','R' & 'I')
C            (I*4)  I2BA()  = TRANSITION: IN BUNDLED SUPERSTRUCTURE FILE
C                             UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             CAPTURING LEVEL INDEX (CASE 'H','R' & 'I')
C            (I*4)  I1A2()  = TRANSITION: IN INPUT DATA FILE
C                             LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             SIGNED PARENT INDEX (CASE 'H','R' & 'I')
C            (I*4)  I2A2()  = TRANSITION: IN INPUT DATA FILE
C                             UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             CAPTURING LEVEL INDEX (CASE 'H','R' & 'I')
C            (R*8)  AVAL()  = TRANSITION: IN SUPERSTRUCTURE FILE
C                             A-VALUE (SEC-1)          (CASE ' ')
C                             NEUTRAL BEAM ENERGY      (CASE 'H')
C                             NOT USED             (CASE 'P','R' & 'I')
C            (R*8)  AVALB() = TRANSITION: IN BUNDLED SUPERSTRUCTURE FILE
C                             A-VALUE (SEC-1)          (CASE ' ')
C                             NEUTRAL BEAM ENERGY      (CASE 'H')
C                             NOT USED             (CASE 'P','R' & 'I')
C            (R*8)  AVAL2() = TRANSITION: IN INPUT DATA FILE
C                             A-VALUE (SEC-1)          (CASE ' ')
C                             NEUTRAL BEAM ENERGY      (CASE 'H')
C                             NOT USED             (CASE 'P','R' & 'I')
C            (R*8)  SCOM(,) = TRANSITION: IN SUPERSTRUCTURE FILE
C                             GAMMA VALUES             (CASE ' ' & 'P')
C                             RATE COEFFT.(CM3 SEC-1)(CASE 'H','R' & 'I')
C                            1ST DIMENSION - TEMPERATURE 'SCEF()'
C                            2ND DIMENSION - TRANSITION NUMBER
C            (R*8)  SCOMB(,)= TRANSITION: IN BUNDLED SUPERSTRUCTURE FILE
C                             GAMMA VALUES             (CASE ' ' & 'P')
C                             RATE COEFFT.(CM3 SEC-1)(CASE 'H','R' & 'I')
C                            1ST DIMENSION - TEMPERATURE 'SCEF()'
C                            2ND DIMENSION - TRANSITION NUMBER
C            (R*8)  SCOM2(,)= TRANSITION: IN INPUT DATA FILE
C                             GAMMA VALUES             (CASE ' ' & 'P')
C                             RATE COEFFT.(CM3 SEC-1)(CASE 'H','R' & 'I')
C                            1ST DIMENSION - TEMPERATURE 'SCEF()'
C                            2ND DIMENSION - TRANSITION NUMBER
C            (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                            ' ' => Electron Impact   Transition
C                            'P' => Proton   Impact   Transition
C                            'H' => Charge   Exchange Recombination
C                            'R' => Free     Electron Recombination
C                            'I' => Coll. ionisation from lower stage ion
C                             IN SUPERSTRUCTURE FILE
C            (C*1)  TCODEB()= TRANSITION: DATA TYPE POINTER:
C                             IN BUNDLED SUPERSTRUCTURE FILE - SAME CODES
C                             AS TCODE ABOVE
C            (C*1)  TCODE2()= TRANSITION: DATA TYPE POINTER:
C                             IN INPUT DATA FILE - SAME CODES
C                             AS TCODE ABOVE
C            (I*4)  NV2     = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                             PAIRS FOR A GIVEN TRANSITION.
C            (I*4)  INDBL() = VECTOR CONTAINING THE BUNDLED SUPERSTRUCTURE
C                             FILE INDICES AT THE ORIGINAL INDEX LOCATIONS
C            (I*4)  NJLEVX  = THE NO. OF LEVELS IN THE SUPERSTRUCTURE FILE
C            (I*4)  IA()    = SUPERSTRUCTURE FILE ENERGY LEVEL INDEX NUMBER 
C            (C*18) CSTRGS()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C                             CONVERTED TO STANDARD FROM EISNER FORM
C            (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                             NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C            (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C            (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                             NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C            (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                            'IA()'
C            (I*4)  IL2     = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C            (I*4)  IA2()   = INPUT DATA ENERGY LEVEL INDEX NUMBER
C            (C*18) CSTRGA2()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA2()'
C            (I*4)  ISA2()  = MULTIPLICITY FOR LEVEL 'IA2()'
C                             NOTE: (ISA2-1)/2 = QUANTUM NUMBER (S)
C            (I*4)  ILA2()  = QUANTUM NUMBER (L) FOR LEVEL 'IA2()'
C            (R*8)  XJA2()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA2()'
C                             NOTE: (2*XJA2)+1 = STATISTICAL WEIGHT
C            (R*8)  WA2()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                            'IA2()'
C            (I*4)  IL3     = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C            (I*4)  IA3()   = PARENT SUPERSTRUCTURE ENERGY LEVEL INDEX
C                             NUMBER
C            (C*18) CPRTAU()= NOMENCLATURE/CONFIGURATION FOR NEW PARENTS
C            (I*4)  ISA3()  = MULTIPLICITY FOR LEVEL 'IA3()'
C                             NOTE: (ISA3-1)/2 = QUANTUM NUMBER (S)
C            (I*4)  ILA3()  = QUANTUM NUMBER (L) FOR LEVEL 'IA3()'
C            (R*8)  XJA3()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA3()'
C                             NOTE: (2*XJA3)+1 = STATISTICAL WEIGHT
C            (R*8)  WA3()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                            'IA3()'
C            (I*4)  BNDLS() = LEVEL/TERM SELECTION VECTOR
C            (I*4)  BNDPR() = PARENT METASTABLE SELECTION VECTOR
C            (I*4)  NBLEVX  = THE NO. OF LEVELS IN THE BUNDLED 
C                             SUPERSTRUCTURE FILE
C            (R*8)  XLSA()  = QUANTUM NUMBER (J-VALUE) FOR BUNDLED 
C                             SUPERSTRUCTURE LEVEL 'I2BA()' 
C                             NOTE: (2*XLSA)+1 = STATISTICAL WEIGHT
C            (I*4)  NBCPRT  = NUMBER OF SELECTED CONTRIBUTIONS TO PARENTS
C            (R*8)  BWNO2   = IONISATION POTENTIAL (CM-1) OF LOWEST PARENT
C                             IN INPUT DATA FILE
C            (I*4)  NPL2    = NUMBER OF PARENTS ON FIRST LINE OF INPUT
C                             DATA FILE AND USED IN LEVEL ASSIGNMENTS
C            (R*8)  BWNOA2()= IONISATION POTENTIAL (CM-1) OF PARENTS
C                             IN INPUT DATA FILE
C            (R*8)  PRTWTA2()= PARENT WEIGHT FOR BWNOA2()
C            (C*9)  CPRTA2()= PARENT NAME IN BRACKETS IN INPUT DATA FILE
C            (C*1)  CPLA2() = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IA2()'
C                                INTEGER - PARENT IN BWNOA2() LIST
C                                'BLANK' - PARENT BWNOA2(1)
C                                  'X'   - DO NOT ASSIGN A PARENT
C            (I*4)  NPLA2() = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                             OF LEVEL IN INPUT DATA FILE
C            (I*4)  IPLA2(,)= PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                             OF LEVEL IN INPUT DATA FILE
C                            1ST DIMENSION: PARENT INDEX
C                            2ND DIMENSION: LEVEL INDEX
C            (I*4)  ZPLA2(,)= EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                             OF LEVEL IN INPUT DATA FILE
C                            1ST DIMENSION: PARENT INDEX
C                            2ND DIMENSION: LEVEL INDEX
C            (I*4)  IPMDFLG = FLAG FOR PARENT SUPERSTRUCTURE FILE 
C                             AVAILABILITY
C
C  OUTPUT:
C            (I*4)  ITRANU  = OUTPUT DATA FILE: NO. OF TRANSITIONS
C            (I*4)  I1UA()  = TRANSITION: IN OUTPUT DATA FILE
C                             LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             SIGNED PARENT INDEX (CASE 'H','R' & 'I')
C            (I*4)  I2UA()  = TRANSITION: IN OUTPUT DATA FILE
C                             UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                             CAPTURING LEVEL INDEX (CASE 'H','R' & 'I')
C            (R*8)  AVALU() = TRANSITION: IN OUTPUT DATA FILE
C                             A-VALUE (SEC-1)          (CASE ' ')
C                             NEUTRAL BEAM ENERGY      (CASE 'H')
C                             NOT USED             (CASE 'P','R' & 'I')
C            (R*8)  SCOMU(,)= TRANSITION: IN OUTPUT DATA FILE
C                             GAMMA VALUES             (CASE ' ' & 'P')
C                             RATE COEFFT.(CM3 SEC-1)(CASE 'H','R' & 'I')
C                            1ST DIMENSION - TEMPERATURE 'SCEF()'
C                            2ND DIMENSION - TRANSITION NUMBER
C            (C*1)  TCODEU()= TRANSITION: DATA TYPE POINTER:
C                             IN OUTPUT DATA FILE - SAME CODES
C                             AS TCODE ABOVE
C            (R*8)  PRERAT(,)=ARRAY OF PREMULTIPLIERS FOR THE J-RESOLVED
C                             LEVELS. RATIO OF INPUT DATA TO BUNDLED DATA
C                             FOR A TRANSITION
C                            1ST DIMENSION - TEMPERATURE 'SCEF()'
C                            2ND DIMENSION - TRANSITION NUMBER
C            (R*8)  PREA()  = PREMULTIPLIERS FOR THE SUPERSTRUCTURE 
C                             A-VALUES. RATIO OF STAT. WEIGHTED INPUT 
C                             DATA TO BUNDLED DATA.
C            (I*4)  IUA()   = ENERGY LEVEL INDEX NUMBER
C            (C*18) CSTRGUA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IUA()'
C            (I*4)  ISUA()  = MULTIPLICITY FOR LEVEL 'IUA()'
C                             NOTE: (ISUA-1)/2 = QUANTUM NUMBER (S)
C            (I*4)  ILUA()  = QUANTUM NUMBER (L) FOR LEVEL 'IUA()'
C            (R*8)  XJUA()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IUA()'
C                             NOTE: (2*XJUA)+1 = STATISTICAL WEIGHT
C            (R*8)  WUA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                            'IUA()'
C            (I*4)  INDUL() = VECTOR CONTAINING THE UNBUNDLED FILE 
C                             INDICES AT THE ORIGINAL INDEX LOCATIONS
C            (I*4)  IUL     = OUTPUT DATA FILE: NUMBER OF ENERGY LEVELS
C            (R*8)  BWNOAU()= IONISATION POTENTIAL (CM-1) OF PARENTS
C                             IN OUTPUT DATA FILE
C            (R*8)  PRTWTAU()= PARENT WEIGHT FOR BWNOAU()
C            (C*1)  CPLAU() = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IUA()'
C                                INTEGER - PARENT IN BWNOA2() LIST
C                                'BLANK' - PARENT BWNOA2(1)
C                                  'X'   - DO NOT ASSIGN A PARENT
C            (I*4)  NPLAU() = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                             OF LEVEL IN OUTPUT DATA FILE
C            (I*4)  IPLAU(,)= PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                             OF LEVEL IN OUTPUT DATA FILE
C                            1ST DIMENSION: PARENT INDEX
C                            2ND DIMENSION: LEVEL INDEX
C            (I*4)  ZPLAU(,)= EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                             OF LEVEL IN OUTPUT DATA FILE
C                            1ST DIMENSION: PARENT INDEX
C                            2ND DIMENSION: LEVEL INDEX
C            (I*4)  NCHK()  = VECTOR NOTING REPEATED USER SELECTIONS
C                             AND HOW OFTEN THEY OCCUR, FOR LEVELS
C            (I*4)  NCHKU() = VECTOR NOTING THE LEVELS SELECTED FOR
C                             UNBUNDLING AND THEIR NEW POSITIONING
C            (I*4)  ISORT() = CROSS REFERENCE VECTOR FOR NEW INDEXING
C            (I*4)  INDBS() = CROSS REFERENCE VECTOR FOR NEW BUNDLED
C                             SUPERSTRUCTURE INDEXING
C            (I*4)  IMRK()  = VECTOR NOTING REPEATED USER SELECTIONS
C                             AND HOW OFTEN THEY OCCUR, FOR PARENTS
C            (I*4)  IRCHK() = VECTOR NOTING THE PARENTS SELECTED FOR
C                             UNBUNDLING AND THEIR NEW POSITIONING
C
C  ROUTINES: NONE
C
C  AUTHOR:   DAVID H.BROOKS (UNIV.OF STRATHCLYDE) EXT.4213/4205
C
C  DATE:     12/01/96
C            
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 26-1-96
C MODIFIED: DAVID H.BROOKS
C               - ALTERED MATCHING OF LSJ LEVELS TO DATASETS IN ORDER
C                 TO ALLOW PARTIAL SPLITTING OF THE SOURCE FILE.
C
C VERSION: 1.3                          DATE: 19-11-98
C MODIFIED: DAVID H.BROOKS
C               - MODIFIED TO ALLOW EXTRA LEVELS TO BE INTERSPERSED
C                 WITH THE ONES BEING USED. THESE ARE OMITTED FROM
C                 THE ACTUAL CALCULATIONS BY NOTING THEIR POSITIONS
C                 IN BNDLS.
C
C-----------------------------------------------------------------------
       INTEGER ITRAN , ITRANB, ITRAN2, NDTRN, NVMAX, I4UNIT, I, J, K
       INTEGER NJLEVX, IT    , ITRANU, NDLEV, IUL  , IL2, L, MK , IMK
       INTEGER NSTOP , NBLEVX, JMK   , SPNMK, NV2  , NBCPRT, IL3
       INTEGER NPL2  , NDMET , IPMDFLG, ICNT , IPOS, NZEROS
C-----------------------------------------------------------------------
       INTEGER I1A(NDTRN)  , I2A(NDTRN) , I1BA(NDTRN) , I2BA(NDTRN)
       INTEGER I1A2(NDTRN) , I2A2(NDTRN), INDBL(NJLEVX), INDUL(NDLEV)
       INTEGER I1UA(NDTRN) , I2UA(NDTRN), ISORT(NDLEV), INDBS(NDLEV) 
       INTEGER BNDLS(NDLEV), NCHK(NJLEVX),NCHKU(NDLEV) 
       INTEGER IUA(NDLEV)  , ILUA(NDLEV), ISUA(NDLEV) 
       INTEGER IA(NDLEV)   , ILA(NDLEV) , ISA(NDLEV)
       INTEGER IA2(NDLEV)  , ILA2(NDLEV), ISA2(NDLEV)
       INTEGER IA3(NDLEV)  , ILA3(NDLEV), ISA3(NDLEV)
       INTEGER BNDPR(NDLEV), NPLA2(NDLEV), IPLA2(NDMET,NDLEV)
       INTEGER NPLAU(NDLEV), IPLAU(2*NDMET,NDLEV), IMRK(2*NDMET)
       INTEGER IRCHK(2*NDMET)
C-----------------------------------------------------------------------
       REAL*8 AVAL(NDTRN) , AVALB(NDTRN), AVALU(NDTRN), AVAL2(NDTRN)
       REAL*8 SCOM(NVMAX,NDTRN), SCOMB(NVMAX,NDTRN), SCOM2(NVMAX,NDTRN) 
       REAL*8 SCOMU(NVMAX,NDTRN), PRERAT(NVMAX,NDTRN) , PREA(NDTRN)
       REAL*8 XJA2(NDLEV) , WA2(NDLEV) 
       REAL*8 XJA3(NDLEV) , WA3(NDLEV) 
       REAL*8 XJUA(NDLEV) , WUA(NDLEV)
       REAL*8 XJA(NDLEV)  , WA(NDLEV) , XLSA(NDLEV)
       REAL*8 BWNO2, BWNOA2(NDMET), PRTWTA2(NDMET), ESUM, ESUM2
       REAL*8 BWNOAU(2*NDMET), ZPLA2(NDMET,NDLEV), ZPLAU(2*NDMET,NDLEV)
       REAL*8 PRTWTAU(2*NDMET)  
C-----------------------------------------------------------------------
       CHARACTER TCODE(NDTRN)*1 , TCODEB(NDTRN)*1 , TCODE2(NDTRN)*1
       CHARACTER TCODEU(NDTRN)*1, CSTRGUA(NDLEV)*18, CSTRGS(NDLEV)*18
       CHARACTER CSTRGA2(NDLEV)*18, CPRTAU(2*NDMET)*9, CPRTA2(NDMET)*9
       CHARACTER CSN*1, CTRMA(9)*1, CPLA2(NDLEV)*1, CPLAU(NDLEV)*1
C-----------------------------------------------------------------------
       LOGICAL LDONE , LFOUND , LFND , LIN , LSPLIT , LSTART
C-----------------------------------------------------------------------
       DATA CTRMA/ 'S' , 'P' , 'D' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K'/
C-----------------------------------------------------------------------
C  CALCULATE UNBUNDLED PARENT METASTABLE INFORMATION 
C-----------------------------------------------------------------------
       IF(IPMDFLG.EQ.1.AND.NBCPRT.NE.0)THEN
         DO 1 J = 1, NBCPRT
           ESUM  = (2*XJA3(J)+1)*WA3(J)
           ESUM2 = BWNOA2(BNDPR(J))
           DO 2 K = 1, NBCPRT
             IF(BNDPR(J).EQ.BNDPR(K).AND.J.NE.K)THEN
               ESUM = ESUM+(2*XJA3(K)+1)*WA3(K)
             ENDIF
 2         CONTINUE
          ESUM = ESUM/PRTWTA2(BNDPR(J))
          CSN = CTRMA(ILA3(J)+1)
          PRTWTAU(J) = 2*XJA3(J)+1
          WRITE(CPRTAU(J),1000)ISA3(J),CSN,PRTWTAU(J)
          BWNOAU(J) = ESUM2-ESUM+WA3(J)  
 1       CONTINUE
       ENDIF
C-----------------------------------------------------------------------
C  CALCULATE NO. OF NEW LEVELS & FLAG WHETHER ENOUGH SS DATA AVAILABLE
C-----------------------------------------------------------------------
       IUL = 0
       MK = 1
       DO 51 I = 1, NJLEVX
         NCHK(I) = -1
 51    CONTINUE
       DO 52 I = 1, IL2
         IUL = IUL+1
 52    CONTINUE
C
       DO 53 J = 1,NJLEVX
         LDONE = .FALSE.
           DO 54 K = 1,MK
             IF(BNDLS(J).EQ.NCHK(K))THEN
               LDONE = .TRUE.
             ENDIF
 54        CONTINUE
          IF(.NOT.LDONE)THEN
            DO 55 L = 1, NJLEVX
              IF(BNDLS(J).EQ.BNDLS(L).AND.J.NE.L.AND.BNDLS(J).NE.0)THEN
                NCHK(MK) = BNDLS(J)
                MK       = MK+1
                IUL      = IUL+1
              ENDIF
 55         CONTINUE
          ENDIF
 53    CONTINUE
C-----------------------------------------------------------------------
C  CALCULATE INDEX TRANSFORMATION VECTOR & SORT INDEXING  
C-----------------------------------------------------------------------
C 
       NZEROS=0
       DO 56 I = 1, IUL
         IF(BNDLS(I).EQ.0)THEN
           NZEROS=NZEROS+1
         ENDIF
 56    CONTINUE
C
       MK = -1
       DO 57 I = 1, IUL+NZEROS
         INDUL(I) = BNDLS(I)
         ISORT(I) = I
         NCHKU(I) = -1
         IF(BNDLS(I).GT.MK)THEN
          MK = BNDLS(I)
         ENDIF
 57    CONTINUE
C       MK  = 1
       IMK = 1
       DO 58 I = 1, IUL+NZEROS
         LFOUND = .FALSE.
           IF(INDUL(I).EQ.0)THEN
               MK=MK+1
               INDUL(I) = MK
           ELSE
             DO 60 J = 1, IMK
               IF(NCHKU(J).EQ.BNDLS(I))THEN
                 LFOUND = .TRUE.
               ENDIF
 60          CONTINUE
             IF(.NOT.LFOUND)THEN
               NCHKU(IMK) = BNDLS(I)
               IMK = IMK+1
C               MK = MK+1
             ENDIF
           ENDIF
 58    CONTINUE
C
C
       MK = 1
       DO 66 I = 1, IUL+NZEROS
        IF(BNDLS(I).NE.0)THEN
        LFOUND = .FALSE.
         DO 67 J = I+1, IUL+NZEROS
           IF(INDUL(I).EQ.INDUL(J))THEN
             LFOUND = .TRUE.
           ENDIF
 67      CONTINUE
         IF(.NOT.LFOUND)THEN
           IF(INDUL(I).NE.0)THEN
             INDBS(MK) = INDUL(I)
             MK = MK+1
           ENDIF
         ENDIF
         ENDIF
 66    CONTINUE
       IF(IUL.LE.NJLEVX.OR.IUL.EQ.IL2) THEN
         NSTOP = IUL
       ELSE
         NSTOP = NJLEVX
       ENDIF
C-----------------------------------------------------------------------
C  COPY NEW INDEX, CONFIGURATIONS ETC.
C-----------------------------------------------------------------------
       DO 68 I = 1, NSTOP+NZEROS
        IF(BNDLS(I).NE.0)THEN
         LFND = .FALSE.
         IF(IUL.EQ.IL2)THEN
           IUA(I)     = IA2(I)
           CSTRGUA(I) = CSTRGA2(I)
           ISUA(I)    = ISA2(I)
           ILUA(I)    = ILA2(I)
           XJUA(I)    = XJA2(I)
           WUA(I)     = WA2(I)
         ELSE
           DO 69 J = 1, IUL+NZEROS
               IF(BNDLS(J).NE.0)THEN
               IF(INDUL(J).EQ.INDUL(I).AND.J.NE.I)THEN
                 LFND = .TRUE.
               ENDIF
               ENDIF
 69        CONTINUE
           IF(LFND)THEN
               IUA(I)     = I
               CSTRGUA(I) = CSTRGS(ISORT(I))
               ISUA(I)    = ISA(ISORT(I))
               ILUA(I)    = ILA(ISORT(I))
               XJUA(I)    = XJA(ISORT(I))
               WUA(I)     = WA(ISORT(I))
           ELSE
               IUA(I)     = I
               CSTRGUA(I) = CSTRGA2(INDUL(I))
               ISUA(I)    = ISA2(INDUL(I))
               ILUA(I)    = ILA2(INDUL(I))
               XJUA(I)    = XJA2(INDUL(I))
               WUA(I)     = WA2(INDUL(I))
           ENDIF
         ENDIF
        ENDIF
 68    CONTINUE
C
       IF(NSTOP.EQ.NJLEVX)THEN
         IMK = NSTOP+1
         DO 80 I = NBLEVX+1, IL2
             IUA(IMK)     = IMK
             CSTRGUA(IMK) = CSTRGA2(I)
             ISUA(IMK)    = ISA2(I)
             ILUA(IMK)    = ILA2(I)
             XJUA(IMK)    = XJA2(I)
             WUA(IMK)     = WA2(I)
             IMK          = IMK+1
 80      CONTINUE
         IF(IUL.NE.IMK-1)THEN
           WRITE(I4UNIT(-1),*)'INCONSISTENT TOTAL NUMBER OF LEVELS'
           STOP
         ENDIF
       ENDIF
C-----------------------------------------------------------------------
C  CALCULATE IONISATION PARENT CONTRIBUTIONS
C-----------------------------------------------------------------------
       DO 96 I = 1, IUL+NZEROS
         IF(BNDLS(I).NE.0)THEN
         CPLAU(I) = CPLA2(INDUL(I))
         NPLAU(I) = NPLA2(INDUL(I))
         DO 97 J = 1, NPLAU(I)
           IPLAU(J,I) = IPLA2(J,INDUL(I))
           ZPLAU(J,I) = ZPLA2(J,INDUL(I))
 97      CONTINUE
         ENDIF
 96    CONTINUE
       IF(IPMDFLG.EQ.1.AND.NBCPRT.NE.0)THEN
         DO 98 I = 1, NBCPRT
           IMRK(I) = -1
           DO 99 J = 1, NBCPRT
             IF(BNDPR(I).EQ.BNDPR(J).AND.I.NE.J.AND.BNDPR(I).NE.0)THEN
               DO 100 K = 1, I
                 IF(BNDPR(I).NE.IMRK(K))THEN
                   IMRK(I)=BNDPR(I)
                 ENDIF
 100           CONTINUE
             ENDIF
 99        CONTINUE
 98      CONTINUE
C
         DO 102 I = 1, IUL+NZEROS
           IF(BNDLS(I).NE.0)THEN
           MK = 1
           LIN = .FALSE.
           LSTART = .FALSE.
           DO 104 J = 1, NPLAU(I)
            ICNT = 0
            IPOS = 999
            LFND = .FALSE.
             DO 106 K = 1, NBCPRT
               IF(IPLAU(J,I).EQ.IMRK(K))THEN
                 LFND = .TRUE.
                 ICNT = ICNT+1
                 IF(K.LT.IPOS)THEN
                   IPOS = K
                 ENDIF
               ENDIF
 106         CONTINUE
             IF(LFND)THEN
               DO 107 K = 1, ICNT
                 IF(LSTART)THEN
                   IPLAU(MK,I) = MK
                   ZPLAU(MK,I) = (ZPLA2(IMRK(IPOS+K-1),INDUL(I))*
     .                           PRTWTAU(MK))/PRTWTA2(IMRK(IPOS+K-1))
                 ELSE
                   IPLAU(MK,I) = IPLAU(J,I)+MK-1
                   ZPLAU(MK,I) = ZPLA2(J,I)*PRTWTAU(IPLAU(MK,I))
                   ZPLAU(MK,I) = ZPLAU(MK,I)/PRTWTA2(IPLA2(J,I))
                 ENDIF
                 MK          = MK+1
                 LIN = .TRUE.
 107           CONTINUE
               LSTART = .FALSE.
             ELSE
               IF(LIN)THEN
                 IPLAU(MK,I) = MK
               ELSE
                 IPLAU(MK,I) = IPLAU(J,I)
                 LSTART = .TRUE.
               ENDIF
               ZPLAU(MK,I) = ZPLA2(J,INDUL(I))
               MK          = MK+1
             ENDIF
 104       CONTINUE
         NPLAU(I) = MK-1
         ENDIF
 102     CONTINUE
       ENDIF
C
       DO 120 I = 1, NBCPRT
        LFOUND = .FALSE.
        ICNT = 1
        MK   = 1
        IRCHK(I) = -1
         IF(IMRK(I).GT.0)THEN
           DO 122 J = 1, MK
             IF(IRCHK(J).EQ.IMRK(I))THEN
               LFOUND = .TRUE.
             ENDIF
 122       CONTINUE
           IF(.NOT.LFOUND)THEN
            IRCHK(MK) = IMRK(I)
            MK = MK+1
             DO 123 J = 1, NBCPRT
               IF(IMRK(I).EQ.IMRK(J).AND.I.NE.J)THEN
                 ICNT = ICNT+1
               ENDIF
 123         CONTINUE
             DO 124 J = 1, IUL+NZEROS
             IF(BNDLS(J).NE.0)THEN
              LIN = .FALSE.
               DO 126 K = 1, NPLAU(J)
                 IF(IPLA2(K,INDUL(J)).EQ.IMRK(I))THEN
                   LIN = .TRUE.
                 ENDIF
 126           CONTINUE
               IF(.NOT.LIN)THEN
                 DO 128 K = 1, NPLAU(J)
                   IF(IPLAU(K,J).GT.IMRK(I))THEN
                     IPLAU(K,J) = IPLAU(K,J)+ICNT-1
                   ENDIF
 128             CONTINUE
               ENDIF
               ENDIF
 124         CONTINUE
           ENDIF
         ENDIF
 120   CONTINUE
C-----------------------------------------------------------------------
C  PERFORM THE SPLIT UP              
C-----------------------------------------------------------------------
       ITRANU = 0
       DO 175 I = 1,ITRAN2
         LSPLIT = .FALSE.
         LFND = .FALSE.
         IF(TCODE2(I).EQ.' ')THEN
         DO 180 J = 1, IUL+NZEROS
           IF(BNDLS(J).NE.0)THEN
           IF(I1A2(I).EQ.INDUL(J))THEN
             DO 185 K = 1, IUL+NZEROS
               IF(BNDLS(K).NE.0)THEN
               IF(I1A2(I).EQ.INDUL(K).AND.J.NE.K)THEN
                 LSPLIT = .TRUE.
               ENDIF
               ENDIF
 185         CONTINUE
           ENDIF
           IF(I2A2(I).EQ.INDUL(J))THEN
             DO 186 K = 1, IUL+NZEROS
               IF(BNDLS(K).NE.0)THEN
               IF(I2A2(I).EQ.INDUL(K).AND.J.NE.K)THEN
                 LSPLIT = .TRUE.
               ENDIF
               ENDIF
 186         CONTINUE
           ENDIF
           ENDIF
 180     CONTINUE
           IF(LSPLIT)THEN
             DO 200 J = 1, ITRANB
               IF(TCODEB(J).EQ.' '.AND.I1A2(I).EQ.INDBS(I1BA(J)).AND.
     &                  I2A2(I).EQ.INDBS(I2BA(J)))THEN
                 DO 210 IT = 1, NV2
                    PRERAT(IT,I) = SCOM2(IT,I)/SCOMB(IT,J)
 210             CONTINUE
                  PREA(I)=(AVAL2(I)/AVALB(J))*(2*XLSA(INDBS(I2BA(J)))+1)
                  LFOUND    = .TRUE.
               ELSE IF(TCODEB(J).EQ.' '.AND.I1A2(I).EQ.INDBS(I2BA(J))
     &             .AND.I2A2(I).EQ.INDBS(I1BA(J)))THEN
               LFND = .TRUE.
                 DO 211 IT = 1, NV2
                    PRERAT(IT,I) = SCOM2(IT,I)/SCOMB(IT,J)
 211             CONTINUE
C                  PREA(I)=(AVAL2(I)/AVALB(J))*(2*XLSA(INDBS(I1BA(J)))+1)
                  LFOUND    = .TRUE.
               ENDIF
 200         CONTINUE
             IF(.NOT.LFOUND)THEN
                DO 410 K = 1,IUL+NZEROS
                   IF(BNDLS(K).NE.0)THEN
                   IF(I1A2(I).EQ.INDUL(K))THEN
                      DO 420 L = 1, IUL+NZEROS
                         IF(I2A2(I).EQ.INDUL(L))THEN
                           ITRANU         = ITRANU+1
                           I1UA(ITRANU)   = ISORT(K)
                           I2UA(ITRANU)   = ISORT(L)
                           TCODEU(ITRANU) = ' '
                           AVALU(ITRANU)  = 1.00d-30 
                           DO 500 IT = 1, NV2
                              SCOMU(IT,ITRANU) = SCOM2(IT,I)*
     .           ((2*XJUA(ISORT(K))+1)*(2*XJUA(ISORT(L))+1))/
     . 	         ((2*ILUA(ISORT(K))+1)*ISUA(ISORT(K))*
     .           (2*ILUA(ISORT(L))+1)*ISUA(ISORT(L)))      
 500                       CONTINUE
                         ENDIF
 420                  CONTINUE
                   ENDIF
                   ENDIF
 410            CONTINUE
             ELSE
               DO 300 K = 1, ITRAN
                 IF(TCODE(K).EQ.' '.AND.I1A2(I).EQ.INDUL(I1A(K)).AND.
     &                   I2A2(I).EQ.INDUL(I2A(K)).OR.
     &              TCODE(K).EQ.' '.AND.I2A2(I).EQ.INDUL(I1A(K)).AND.
     &                   I1A2(I).EQ.INDUL(I2A(K)))THEN
                          ITRANU         = ITRANU+1
                          I1UA(ITRANU)   = ISORT(I1A(K))
                          I2UA(ITRANU)   = ISORT(I2A(K))
                            IF(I1UA(ITRANU).GT.I2UA(ITRANU))THEN
                              I1UA(ITRANU)   = ISORT(I2A(K))
                              I2UA(ITRANU)   = ISORT(I1A(K))
                            ENDIF
                          TCODEU(ITRANU) = ' '
                            IF(LFND)THEN
                              AVALU(ITRANU)  = 1.00d-30
                            ELSE
                              AVALU(ITRANU)  = PREA(I)*AVAL(K)
                            ENDIF
                          DO 400 IT = 1, NV2
                            SCOMU(IT,ITRANU) = PRERAT(IT,I)*SCOM(IT,K)
 400                      CONTINUE
                 ENDIF
 300           CONTINUE
             ENDIF 
           ELSE
             ITRANU = ITRANU+1
               DO 520 K = 1, IUL+NZEROS
                 IF(BNDLS(K).NE.0)THEN
                 IF(I1A2(I).EQ.INDUL(K))THEN
                   I1UA(ITRANU) = K
                 ENDIF
                 IF(I2A2(I).EQ.INDUL(K))THEN
                   I2UA(ITRANU) = K
                 ENDIF
                 ENDIF
 520           CONTINUE
             TCODEU(ITRANU) = ' '
             AVALU(ITRANU)  = AVAL2(I)
             DO 600 IT = 1, NV2
               SCOMU(IT,ITRANU) = SCOM2(IT,I) 
 600         CONTINUE
           ENDIF
         ENDIF
       LFOUND = .FALSE.
 175   CONTINUE
C-----------------------------------------------------------------------
 1000  FORMAT('(',I1,1A1,F3.1,')')
       RETURN
C-----------------------------------------------------------------------
       END
