CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas210/baspf0.for,v 1.1 2004/07/06 11:35:12 whitefor Exp $ Date $Date: 2004/07/06 11:35:12 $
CX
      SUBROUTINE BASPF0( REP  , DSFULL1, DSFULL2, DSFULL3, IPMDFLG ) 
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BASPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS210
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL1  = INPUT DATA SET NAME , INCLUDING PATH
C  OUTPUT: (C*80)  DSFULL2  = INPUT SUPERSTRUCTURE TEMPLATE NAME ,
C                             INCLUDING PATH
C
CA - UNIX PORT : LDSEL ONLY USED TO KEEP ARGUMENT LIST THE SAME. 
CA 		 IT'S ORIGINAL FUNCTION IS CARRIED OUT IN IDL NOW
CX  OUTPUT: (L*4)   LDSEL   = .TRUE.  => COPASE DATA SET INFORMATION
CX                                       TO BE DISPLAYED BEFORE RUN.
CX                          = .FALSE. => COPASE DATA SET INFORMATION
CX                                       NOT TO BE DISPLAYED BEFORE RUN.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES: NONE
C          
C AUTHOR:  LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    28/02/95
C
C MODIFIED: 
C          DAVID H.BROOKS (UNIV. OF STRATHCLYDE) RM. COL.4.13B EXT.4213
C          -REMOVED REDUNDANT LDSEL &  ADDED DSFULL2 TO CARRY NAME
C           OF SUPERSTRUCTURE TEMPLATE FOR ADAS210. CHANGED DSFULL TO
C           DSFULL2.
C
C          DAVID H.BROOKS  24.11.95
C          ADDED FLAG FOR AVAILABILITY OF PARENT DATA SET. 
C          ADDED DSFULL3 TO CARRY PARENT DATA SET NAME.
C
C
C VERSION: 1.1                          DATE: 19-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      CHARACTER REP *3 , DSFULL1*80 , DSFULL2*80 , DSFULL3*80
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU    , IPMDFLG , I4UNIT
C-----------------------------------------------------------------------
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL1
      READ(PIPEIN,'(A)') DSFULL2
C-----------------------------------------------------------------------
C  READ PARENT FILE AVAILABILITY
C-----------------------------------------------------------------------
      READ(PIPEIN,'(1X,I1)') IPMDFLG
      IF (IPMDFLG.EQ.1)THEN
        READ(PIPEIN,'(A)') DSFULL3
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
