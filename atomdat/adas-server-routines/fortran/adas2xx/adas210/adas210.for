CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas210/adas210.for,v 1.4 2010/08/23 15:34:01 mog Exp $ Date $Date: 2010/08/23 15:34:01 $
CX
       IMPLICIT NONE
C----------------------------------------------------------------------
C  ******************** FORTRAN77 PROGRAM  ADAS210 ********************
C
C  PURPOSE: TO UNBUNDLE LS-RESOLVED SPECIFIC ION FILES
C
C  INPUT/OUTPUT:
C           SEE SUBROUTINES
C
C  ROUTINES:
C           ROUTINE SOURCE  BRIEF DESCRIPTION
C           ----------------------------------------------------------
C           XX0000  ADAS    SETS MACHINE DEPENDANT ADAS CONFIGURATION
C           XXDATE  ADAS    FETCHES CURRENT DATE
C           BASPF0  ADAS    FETCHES USER INPUT SELECTIONS
C           BADATA  ADAS    FETCHES DATA SETS
C           XXCFTR  ADAS    CONVERTS BETWEEN EISSNER & STANDARD FORMS
C           BAISPF  ADAS    FETCHES USER PROCESSING SELECTIONS
C           BASPF1  ADAS    FETCHES USER OUTPUT SELECTIONS
C           BAUBND  ADAS    PERFORMS UNBUNDLING
C
C  AUTHOR:
C           DAVID H.BROOKS (UNIV.OF STRATHCLYDE) EXT.4213/4205
C
C  UPDATE - 13.11.95 DAVID H.BROOKS
C                    INCREASED LENGTH OF CPRTA TO 9 (SEE B9DATA)
C                    ADDED DSNTMP FOR SUPERSTRUCTURE TEMPLATE
C                    & CHANGED VARIABLES TO HANDLE TWO INPUT FILES.
C                    2ND FILE VARIABLES NOW END IN A 2.
C
C  UPDATE - 24.11.95 DAVID H.BROOKS
C                    ADDED IPMDFLG & DSPTMP TO FLAG PARENT FILE
C                    AVAILABILITY AND ITS' NAME. ALSO, ADDED VARIABLES
C                    ENDING IN 3 FOR 3RD FILE.
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C VERSION: 1.1                          DATE: 19-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 26-1-96
C MODIFIED:  DAVID H.BROOKS
C               - CORRECTED BPRTWT & BPWNO AS IN ADAS209 & ADDED
C                 DEFAULT PARENT PRINTOUT TO BE CONSISTENT WITH
C                 ADAS209.ALSO, ALTERED CALCULATION OF ZPBA'S.
C
C VERSION: 1.3                          DATE: 20-11-98
C MODIFIED:  DAVID H.BROOKS
C               - UPPED NO. OF LEVELS TO 250 AND NO. OF TRANSITIONS
C                 TO 8000. PASSED PARAMETER NZEROS TO BAUBND. UPPED
C                 FORMAT STATEMENTS IN PRINTOUT TO ALLOW MORE TEMPERATURES
C                 AND ALTERED LEVEL NUMBERING TO BE CONSISTENT WITH 
C                 MODIFICATIONS TO BAUBND AND CHINDX.
C
C  Version  : 1.4
C  Date     : 23-08-2010
C  Modified : Martin O'Mullane
C               - extra arguments for xxdtes.
C
C----------------------------------------------------------------------
       INTEGER  NDLEV       , NDTRN          , NDMET      , NVMAX
       INTEGER  IUNT09      , IUNT11         , IUNT12     , ITHREE
       INTEGER  IPMDFLG     , IUNT08         , ISTOP      , NZEROS
       INTEGER  PIPEIN      , PIPEOU         , IUNT10     , NBCPRT
C----------------------------------------------------------------------
       PARAMETER ( NDLEV  = 250 , NDTRN = 8000 , NDMET = 5 ,NVMAX= 14 )
       PARAMETER ( IUNT09 = 9   , IUNT11 = 11  , IUNT12 = 12 )
       PARAMETER ( ITHREE = 3   , IUNT08 = 8   , IUNT10 = 10 )
       PARAMETER ( PIPEIN = 5   , PIPEOU = 6   )
C----------------------------------------------------------------------
       INTEGER   IZ          , IZ0            , IZ1        ,
     &           IL          , NV             , ITRAN      ,
     &           MAXLEV      , NPL            , IT         ,
     &           I           , IP             , IJ         ,
     &           IPJ         , ISX            , ILX        ,
     &           IPOS        , IQS            , ITR        ,
     &           ITRANB      , IB             , JB         ,
     &           JPOS        , IZ2            , IZ02       ,
     &           IZ12        , NPL2           , IL2        ,
     &           NV2         , MAXLEV2        , ITRAN2     ,
     &           IZ3         , IZ03           , IZ13       ,
     &           NPL3        , IL3            , NV3        ,
     &           MAXLEV3     , ITRAN3         , IUL        ,
     &           ICOUNT
       INTEGER   I4UNIT      , J              , IPU
       INTEGER   IZ0X        , ITRANU         ,
     &           NJPRTX      , NBPRTX         , NJLEVX     , NBLEVX  ,
     &           lvlce
C-----------------------------------------------------------------------
       REAL*8    BWNO        , ESUM           , WSUM       ,
     &           WT          , ESHIFT         , BPRTWT     ,
     &           BPWNO       , Z1             , BWNO2      ,
     &           BWNO3       , Z12
C----------------------------------------------------------------------
       INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &           I1A(NDTRN)  , I2A(NDTRN)     , 
     &           I1UA(NDTRN) , I2UA(NDTRN)    , 
     &           I1A2(NDTRN) , I2A2(NDTRN)    , 
     &           I1A3(NDTRN) , I2A3(NDTRN)    , 
     &           I1BA(NDTRN) , I2BA(NDTRN)    ,
     &           IPLA(NDMET,NDLEV)            , NPLA(NDLEV),
     &           IPLA2(NDMET,NDLEV)           , NPLA2(NDLEV),
     &           IPLA3(NDMET,NDLEV)           , NPLA3(NDLEV),
     &           IPLAU(2*NDMET,NDLEV)           , NPLAU(NDLEV)
       INTEGER   INDJP(NDLEV), INDBP(NDLEV)  ,
     &           INDJL(NDLEV), INDBL(NDLEV)  , INDUL(NDLEV)
       INTEGER   ISB(NDLEV)  , ILB(NDLEV)    , NPBA(NDLEV) ,
     &           IPBA(NDMET,NDLEV),BNDLS(NDLEV),BNDPR(NDLEV)
       INTEGER   NSTAR(NDLEV), NGAP(NDLEV), INDBLO(NDLEV)
       INTEGER   MGAP(NDLEV) ,  INDBPO(NDLEV) , IMRK(2*NDMET)
       INTEGER   INDCPB(NDLEV),  INDCPL(NDLEV), NCHK(NDLEV)
       INTEGER   IA2(NDLEV)  , ISA2(NDLEV)    , ILA2(NDLEV) 
       INTEGER   IA3(NDLEV)  , ISA3(NDLEV)    , ILA3(NDLEV) 
       INTEGER   IUA(NDLEV)  , ISUA(NDLEV)    , ILUA(NDLEV) 
       INTEGER   NCHKU(NDLEV) 
       INTEGER   IRCHK(2*NDMET), ISORT(NDLEV) , INDBS(NDLEV)
       INTEGER   NVLCE(NDLEV), NVLCE3(NDLEV)
C----------------------------------------------------------------------
       REAL*8   BWNOA(NDMET)   , PRTWTA(NDMET)
       REAL*8   BWNOA2(NDMET)  , PRTWTA2(NDMET)
       REAL*8   BWNOA3(NDMET)  , PRTWTA3(NDMET)
       REAL*8   BWNOAU(2*NDMET), PRTWTAU(2*NDMET)  
       REAL*8   SCEF(NVMAX)    , SCEF2(NVMAX) , SCEF3(NVMAX)
       REAL*8   XJA(NDLEV)     , WA(NDLEV)           ,
     &          AVAL(NDTRN)    , SCOM(NVMAX,NDTRN)   ,
     &          AVALB(NDTRN)   , SCOMB(NVMAX,NDTRN)  ,
     &          ZPLA(NDMET,NDLEV), ZPLA2(NDMET,NDLEV), 
     &          ZPLA3(NDMET,NDLEV), ZPLAU(2*NDMET,NDLEV)
       REAL*8   TWTA(9), PRERAT(NVMAX,NDTRN) , PREA(NDTRN)
       REAL*8   ZPBA(NDMET,NDLEV) , ENLS(NDLEV)  , XLSA(NDLEV)  ,
     &          BPWNOA(NDMET)     , BPRTWTA(NDMET)
       REAL*8   XJA2(NDLEV)    , WA2(NDLEV)          ,
     &          AVAL2(NDTRN)   , SCOM2(NVMAX,NDTRN)  
       REAL*8   XJA3(NDLEV)    , WA3(NDLEV)          ,
     &          AVAL3(NDTRN)   , SCOM3(NVMAX,NDTRN) 
       REAL*8   XJUA(NDLEV)    , WUA(NDLEV)          ,          
     &          AVALU(NDTRN)   , SCOMU(NVMAX,NDTRN)  
C-----------------------------------------------------------------------
       CHARACTER DSNINP*80, DSNOUT*44, DSNREF*44, DSNTMP*80, DSPTMP*80
       CHARACTER TITLED*3    , TCODE(NDTRN)*1 
       CHARACTER TCODEB(NDTRN)*1  
       CHARACTER SEQX*2      , CDELIM*7       , C9*9
       CHARACTER STRING*133     , BLANKS*133, SAVFIL*80     
       CHARACTER TITLED2*3   , TCODE2(NDTRN)*1 
       CHARACTER TITLED3*3   , TCODE3(NDTRN)*1 , TCODEU(NDTRN)*1 
       CHARACTER CSTRGA(NDLEV)*18 ,  CSTRGUA(NDLEV)*18
       CHARACTER CSTRGA2(NDLEV)*18,  CSTRGS(NDLEV)*18
       CHARACTER CSTRGA3(NDLEV)*18,  CSTRGS3(NDLEV)*18
       CHARACTER CSTRG19*19
       character cstr_top*19      , cstr_tail*99
C-----------------------------------------------------------------------
       CHARACTER CPLA(NDLEV)*1  , CPLA2(NDLEV)*1 , CPLA3(NDLEV)*1
       CHARACTER CNJP(NDLEV)*18 , CNBP(NDLEV)*18 ,
     &           CNJL(NDLEV)*18 , CNBL(NDLEV)*18 , CFGB(NDLEV)*18
       CHARACTER CTRMA(9)*1
       CHARACTER CPBA(NDLEV)*1  , CTERMA(NDMET)*7
       CHARACTER DATE*8         , UID*6
       CHARACTER CPRTA(NDMET)*9 , CDEFAULT*4     , CPRTA2(NDMET)*9
       CHARACTER CPRTA3(NDMET)*9 , REP*3
       CHARACTER CPRTAU(2*NDMET)*9, CPLAU(NDLEV)*1
C----------------------------------------------------------------------
       LOGICAL  LEXIST          , LSET,     LPEND,  L2FILE
       LOGICAL  LBSETA(NDMET)   , LPBA(NDMET,NDLEV)
       LOGICAL  LBSETA2(NDMET)  , LBSETA3(NDMET) , OPEN10
       LOGICAL  LEISS           , LSTAN          , lprnt      ,
     &          lbndl
C----------------------------------------------------------------------
       DATA LEXIST/.FALSE./
C-----------------------------------------------------------------------
       DATA  CTRMA/ 'S' , 'P' , 'D' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K'/
       DATA  TWTA / 1.0 , 3.0 , 5.0 , 7.0 , 9.0 , 11.0, 13.0, 15.0,17.0/
       DATA  CDELIM/' ()<>{}'/
       DATA  CDEFAULT/'(1S)'/
C-----------------------------------------------------------------------
       DATA BLANKS/' '/
       DATA OPEN10 /.FALSE./
C-----------------------------------------------------------------------
C ************************** MAIN PROGRAM ******************************
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES 
C-----------------------------------------------------------------------
       CALL XX0000
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
CA - UNIX PORT : DATE NOW GATHERED VIA IDL
       CALL XXDATE( DATE )
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT TEN CLOSE THE UNIT
C-----------------------------------------------------------------------
       IF (OPEN10) THEN
         CLOSE(10)
         OPEN10=.FALSE.
       ENDIF
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAMES
C-----------------------------------------------------------------------
 100   CONTINUE
       CALL BASPF0( REP , DSNINP , DSNTMP , DSPTMP , IPMDFLG )
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED : TERMINATE
C-----------------------------------------------------------------------
       IF (REP.EQ.'YES') THEN
         GOTO 9999
       ENDIF
C-----------------------------------------------------------------------
C OPEN SUPERSTRUCTURE TEMPLATE DATA SET
C-----------------------------------------------------------------------
       CALL XXOPEN(IUNT09,DSNTMP,LEXIST)
C
       IF (LEXIST) THEN
           CALL BADATA( IUNT09 , NDLEV  , NDTRN , NDMET ,
     &                  TITLED , IZ     , IZ0   , IZ1   , 
     &                  BWNO   , NPL    , BWNOA , LBSETA, 
     &                  PRTWTA , CPRTA  , IL    , IA    , 
     &                  CSTRGA , ISA    , ILA   , XJA   ,
     &                  WA     , CPLA   , NPLA  , IPLA  , 
     &                  ZPLA   , NV     , SCEF  , ITRAN , 
     &                  MAXLEV , TCODE  , I1A   , I2A   , 
     &                  AVAL   , SCOM
     &                )
           CLOSE(IUNT09)
        ELSE
            WRITE(I4UNIT(-1),*)'INPUT SUPERSTRUCTURE DATA SET ',
     &                         ' DOES NOT EXIST'
            STOP
       ENDIF
C
       LEXIST = .FALSE.
C-----------------------------------------------------------------------
C OPEN DATA SET
C-----------------------------------------------------------------------
       CALL XXOPEN(IUNT12,DSNINP,LEXIST)
C
       IF (LEXIST) THEN
           CALL BADATA( IUNT12  , NDLEV   , NDTRN  , NDMET  ,
     &                  TITLED2 , IZ2     , IZ02   , IZ12   , 
     &                  BWNO2   , NPL2    , BWNOA2 , LBSETA2, 
     &                  PRTWTA2 , CPRTA2  , IL2    , IA2    ,
     &                  CSTRGA2 , ISA2    , ILA2   , XJA2   , 
     &                  WA2     , CPLA2   , NPLA2  , IPLA2  , 
     &                  ZPLA2   , NV2     , SCEF2  , ITRAN2 ,
     &                  MAXLEV2 , TCODE2  , I1A2   , I2A2   ,
     &                  AVAL2   , SCOM2
     &                )
           CLOSE(IUNT12)
        ELSE
            WRITE(I4UNIT(-1),*)'INPUT DATA SET DOES NOT EXIST'
            STOP
       ENDIF
C
       LEXIST = .FALSE.
C-----------------------------------------------------------------------
C OPEN SUPERSTRUCTURE PARENT TEMPLATE DATA SET IF AVAILABLE
C-----------------------------------------------------------------------
       IF (IPMDFLG.EQ.1) THEN
         CALL XXOPEN(IUNT08,DSPTMP,LEXIST)
C
         IF (LEXIST) THEN
             CALL BADATA( IUNT08  , NDLEV   , NDTRN  , NDMET  ,
     &                    TITLED3 , IZ3     , IZ03   , IZ13   , 
     &                    BWNO3   , NPL3    , BWNOA3 , LBSETA3, 
     &                    PRTWTA3 , CPRTA3  , IL3    , IA3    ,
     &                    CSTRGA3 , ISA3    , ILA3   , XJA3   , 
     &                    WA3     , CPLA3   , NPLA3  , IPLA3  , 
     &                    ZPLA3   , NV3     , SCEF3  , ITRAN3 ,
     &                    MAXLEV3 , TCODE3  , I1A3   , I2A3   ,
     &                    AVAL3   , SCOM3
     &                   )
             CLOSE(IUNT08)
          ELSE
              WRITE(I4UNIT(-1),*)'INPUT PARENT SUPERSTRUCTURE ',
     &                           'DATA SET DOES NOT EXIST'
              STOP
         ENDIF
C
         LEXIST = .FALSE.
       ENDIF
C-----------------------------------------------------------------------
C CONVERT FROM EISSNER FORMS TO STANDARD FORMS IF NECESSARY
C-----------------------------------------------------------------------
       DO 1 I = 1,IL
          LEISS = .FALSE.
          LSTAN = .FALSE.
          CSTRG19 = ' '//CSTRGA(I)
          CALL XXDTES( CSTRG19  , LEISS     , LSTAN ,
     &                lbndl     , lprnt     ,
     &                cstr_top  , cstr_tail ,
     &                NVLCE(I)  , lvlce     )
          IF(LEISS)THEN
            CALL XXCFTR( ITHREE,  CSTRGA(I),  CSTRGS(I) )
          ELSE IF(LSTAN) THEN
            CSTRGS(I) = CSTRGA(I)
          ELSE 
            WRITE(I4UNIT(-1),*)'CONFIGURATION STRING IS NOT IN A ',
     &      'RECOGNISABLE FORM'
          ENDIF
 1     CONTINUE
C
       DO 2 I = 1,IL3
          LEISS = .FALSE.
          LSTAN = .FALSE.
          CSTRG19 = ' '//CSTRGA3(I)
          CALL XXDTES( CSTRG19  , LEISS     , LSTAN ,
     &                lbndl     , lprnt     ,
     &                cstr_top  , cstr_tail ,
     &                NVLCE3(I) , lvlce     )
          IF(LEISS)THEN
            CALL XXCFTR( ITHREE,  CSTRGA3(I),  CSTRGS3(I))
          ELSE IF(LSTAN) THEN
            CSTRGS3(I) = CSTRGA3(I)
          ELSE 
            WRITE(I4UNIT(-1),*)'CONFIGURATION STRING IS NOT IN A ',
     &      'RECOGNISABLE FORM'
          ENDIF
 2     CONTINUE
C-----------------------------------------------------------------------
C COMMUNICATE WITH PROCESSING OPTIONS WIDGET
C-----------------------------------------------------------------------
 3     CONTINUE
       CALL BAISPF( SEQX   , IZ0X   ,
     &              NJPRTX , NBPRTX , NJLEVX , NBLEVX  ,
     &              INDJP  , CNJP   , INDBP  , CNBP    ,
     &              INDJL  , CNJL   , INDBL  , CNBL    ,
     &              BNDLS  , NDLEV  , IL     , CSTRGS  ,
     &              NSTAR  , INDBLO , NGAP   , BNDPR   ,
     &              MGAP   , INDBPO , NPL2   , BWNOA2  ,
     &              PRTWTA2, CPRTA2 , NDMET  , CDEFAULT,
     &              LPEND  , ISA    , ILA    , XJA     ,
     &              WA     , IPMDFLG, IL3    , CSTRGS3 ,
     &              ISA3   , ILA3   , XJA3   , WA3     ,
     &              NBCPRT 
     &            )
C----------------------------------------------------------------------
C  READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE(0)
C----------------------------------------------------------------------
C
       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999
C
C----------------------------------------------------------------------
C  EVALUATE LS COUPLED ENERGY LEVELS
C----------------------------------------------------------------------
C
       IF (.NOT.LPEND)THEN
       CALL BASPF1( L2FILE     , SAVFIL    ,
     &              LPEND      
     &            )
       IF (LPEND)THEN
         GO TO 3
       ENDIF
       OPEN(IUNT11,FILE=SAVFIL,STATUS='UNKNOWN')
       IF(NJLEVX.NE.IL) THEN
           WRITE(I4UNIT(-1),*)'INCONSISTENT TOTAL NUMBERS OF LEVELS'
           STOP
       ENDIF
C
       DO 40 I=1,NBLEVX
         LSET = .FALSE.
         ESUM = 0.0
         WSUM = 0.0
         NPBA(I) = 0
         CPBA(I) = ' '
C
         DO 12 IP = 1,NBPRTX
           IPBA(IP,I) = 0
           ZPBA(IP,I) = 0.0
           LPBA(IP,I) = .FALSE.
   12    CONTINUE
C
         DO 20 IJ = 1,NJLEVX
           IF(INDBL(IJ).EQ.I) THEN
               IF(.NOT.LSET) THEN
                   LSET = .TRUE.
                   ISB(I) = ISA(IJ)
                   ILB(I) = ILA(IJ)
                   CFGB(I) = CNBL(IJ)
               ELSE
                   ISX = ISA(IJ)
                   ILX = ILA(IJ)
                   IF(ISX.NE.ISB(I).OR.ILX.NE.ILB(I))THEN
                       WRITE(I4UNIT(-1),*)'INCONSISTENT ASSIGNMENTS',
     &                 ' OF LS TERMS'
                       STOP
                   ENDIF
               ENDIF
               WT = 2.0*XJA(IJ)+1.0
               WSUM = WSUM + WT
               ESUM = ESUM +WT*WA(IJ)
               IF(CPLA(IJ).EQ.' ')THEN
                   GO TO 20
               ELSEIF(CPLA(IJ).EQ.'X')THEN
                   CPBA(I) = 'X'
               ELSE
                   DO 15 IP = 1,NBPRTX
                     DO 13 IPJ = 1,NPLA(IJ)
                       IF(INDBP(IPLA(IPJ,IJ)).EQ.IP)THEN
                           IPBA(IP,I) = INDBP(IP)
                           ZPBA(IP,I) = ZPBA(IP,I)+ZPLA(IPJ,IJ)
                           LPBA(IP,I) = .TRUE.
                       ENDIF
   13                CONTINUE
   15              CONTINUE
               ENDIF
          ENDIF
   20    CONTINUE
         ENLS(I) = ESUM/WSUM
         XLSA(I) = (WSUM-1.0)/2.0
         DO 25 IP = 1,NBPRTX
           IF(LPBA(IP,I))THEN
               NPBA(I) = NPBA(I) + 1
           ENDIF
   25    CONTINUE
         DO 28 IP = 1,NBPRTX
           IF(LPBA(IP,I))THEN
               WRITE(CPBA(I),'(I1)')IP
               GO TO 40
           ENDIF
   28    CONTINUE
   40  CONTINUE
       ESHIFT = ENLS(1)
       DO 50 I = 1,NBLEVX
         ENLS(I) = ENLS(I)-ESHIFT
   50  CONTINUE
C
C----------------------------------------------------------------------
C  SET LS-COUPLED PARENTS FOR FIRST LINE
C----------------------------------------------------------------------
C
       IF(NPL2.GT.0)THEN
           BPRTWT = 0.0
           BPWNO = 0.0
           DO 60 IP = 1,NBPRTX
             DO 55 IPJ = 1,NPL2
               IF(INDBP(IPJ).EQ.IP) THEN
                   BPRTWT = BPRTWT+PRTWTA2(IPJ)
                   BPWNO = BPWNO + PRTWTA2(IPJ)*BWNOA2(IPJ)
                   CTERMA(IP) = CNBP(IPJ)
               ENDIF
   55        CONTINUE
             BPWNOA(IP) = BPWNO/BPRTWT - ESHIFT
             BPRTWTA(IP) = BPRTWT
   60      CONTINUE
           STRING = BLANKS
           WRITE(STRING,1001) TITLED,IZ,IZ0,IZ1,(BPWNOA(IP),
     &                        CTERMA(IP),IP=1,NBPRTX)
       ELSE
           BPWNO = BWNO-ESHIFT
           CTERMA(1) = CNBP(1)
           STRING = BLANKS
           WRITE(STRING,1001)TITLED,IZ,IZ0,IZ1,BPWNO,CTERMA(1)
       ENDIF
C       WRITE(IUNT11,'(1A86)')STRING
C
       DO 80 I = 1,NBLEVX
         STRING = BLANKS
C ALTERATION TO WRITE BUNDLED LEVELS WITH A STAR '*'
           IF(NSTAR(I).EQ.1)THEN
             WRITE(STRING,1012)I,CFGB(I),ISB(I),ILB(I),XLSA(I),ENLS(I)
           ELSE
             WRITE(STRING,1000)I,CFGB(I),ISB(I),ILB(I),XLSA(I),ENLS(I)
           ENDIF
         IF(CPBA(I).EQ.' ')THEN
             CONTINUE
         ELSEIF (CPBA(I).EQ.'X') THEN
             WRITE(STRING(48:51),'(1X,1A3)')'{X}'
         ELSE
             IPOS = 0
             DO 70 IP = 1,NBPRTX
               IF(LPBA(IP,I))THEN
                   WRITE(STRING(48+IPOS:56+IPOS),'(1X,1A1,I1,1A1,F5.3)')
     &                   '{',IPBA(IP,I),'}',ZPBA(IP,I)
                   IPOS = IPOS + 9
               ENDIF
   70        CONTINUE
         ENDIF
C         WRITE(IUNT11,'(1A80)')STRING
   80  CONTINUE
C----------------------------------------------------------------------
C  ASSIGN TRANSITIONS TO LS-COUPLED GROUPS
C----------------------------------------------------------------------
C
       ITRANB = 0
C
       DO 200 I = 1,ITRAN
         IF(TCODE(I).EQ.' ')THEN
             IB = INDBL(I1A(I))
             JB = INDBL(I2A(I))
             IF(ITRANB.GT.0) THEN
                 DO 117 ITR = 1,ITRANB
                   IF(TCODEB(ITR).EQ.' '.AND.
     &                I1BA(ITR).EQ.IB.AND.I2BA(ITR).EQ.JB)THEN
                       AVALB(ITR) = AVALB(ITR)+AVAL(I)*
     &                            (2.0D0*XJA(I2A(I))+1.0)
                       DO 115 IT = 1,NV
                         SCOMB(IT,ITR)=SCOMB(IT,ITR)+SCOM(IT,I)
  115                  CONTINUE
                       GO TO 120
                   ENDIF
  117            CONTINUE
             ENDIF
             ITRANB = ITRANB + 1
             I1BA(ITRANB) = IB
             I2BA(ITRANB) = JB
             TCODEB(ITRANB) = ' '
             AVALB(ITRANB) = AVALB(ITRANB)+AVAL(I)*
     &                       (2.0D0*XJA(I2A(I))+1.0)
             DO 118 IT = 1,NV
               SCOMB(IT,ITRANB)=SCOMB(IT,ITRANB)+SCOM(IT,I)
  118        CONTINUE
  120        CONTINUE
         ENDIF
  200  CONTINUE
C
C----------------------------------------------------------------------
C  CALL UNBUNDLING ROUTINE 
C----------------------------------------------------------------------
C
       CALL BAUBND( ITRAN  , ITRANB , ITRAN2 , I1A   , I2A   , 
     &              I1BA   , I2BA   , I1A2   , I2A2  , AVAL  , 
     &              AVALB  , SCOM   , SCOMB  , SCOM2 , NV2   , 
     &              NDTRN  , NVMAX  , TCODE  , TCODEB, TCODE2, 
     &              INDBL  , NJLEVX , SCOMU  , TCODEU, I1UA  ,
     &              I2UA   , PRERAT , ILA2   , ISA2  , XJA2  ,
     &              NDLEV  , IL2    , BNDLS  , NCHK  , IUA   ,
     &              ILUA   , ISUA   , CSTRGUA, WUA   , XJUA  ,
     &              IA     , ILA    , ISA    , CSTRGS, WA    ,
     &              XJA    , IA2    , CSTRGA2, WA2   , NBLEVX,
     &              INDUL  , NCHKU  , ISORT  , INDBS , AVALU , 
     &              AVAL2  , PREA   , ITRANU , IUL   , XLSA  ,
     &              BNDPR  , NBCPRT , IL3    , CPRTAU, IA3   ,
     &              ILA3   , ISA3   , XJA3   , WA3   , BWNO2 ,
     &              NPL2   , BWNOA2 , PRTWTA2, CPRTA2, NDMET ,
     &              IPMDFLG, BWNOAU , CPLA2  , NPLA2 , IPLA2 ,
     &              ZPLA2  , CPLAU  , NPLAU  , IPLAU , ZPLAU ,
     &              IMRK   , PRTWTAU, IRCHK  , NZEROS)
C
C----------------------------------------------------------------------
C  WRITE OUT THE RESULTS
C----------------------------------------------------------------------
C
       IF(NBCPRT.GT.0)THEN
         WRITE(STRING,1001)TITLED2,IZ2,IZ02,IZ12,(BWNOAU(IPU),
     &                     CPRTAU(IPU),IPU=1,NBCPRT)
         WRITE(IUNT11,'(1A116)')STRING
       ELSE
         WRITE(STRING,1001)TITLED2,IZ2,IZ02,IZ12,BWNOA2(1),
     &                     CPRTA2(1)
         WRITE(IUNT11,'(1A116)')STRING
       ENDIF
       ICOUNT=0
       DO 297 I = 1,IUL+NZEROS
         IF(BNDLS(I).NE.0)THEN
         ICOUNT=ICOUNT+1
         STRING = BLANKS
         WRITE(STRING,1000)ICOUNT,CSTRGUA(I),ISUA(I),ILUA(I),
     &                     XJUA(I),WUA(I)
         IF(CPLAU(I).EQ.' ')THEN
             CONTINUE
         ELSEIF (CPLAU(I).EQ.'X') THEN
             WRITE(STRING(48:51),'(1X,1A3)')'{X}'
         ELSE
             IPOS = 0
             DO 298 IPU = 1, NPLAU(I)
               IF(IPLAU(IPU,I).LE.NBCPRT)THEN
C               IF(LPBA(IPU,I))THEN
                   WRITE(STRING(48+IPOS:56+IPOS),'(1X,1A1,I1,1A1,F5.3)')
     &                   '{',IPLAU(IPU,I),'}',ZPLAU(IPU,I)
                   IPOS = IPOS + 9
C               ENDIF
               ENDIF
 298         CONTINUE
         ENDIF
         WRITE(IUNT11,'(1A93)')STRING
         ENDIF
 297   CONTINUE
       WRITE(IUNT11,'(1A5)') '   -1'
C
       STRING = BLANKS
       Z12  = IZ12
       IQS = 3
       WRITE(STRING,'(F5.2,I5)')Z12,IQS
       IPOS = 0
       DO 299 IT = 1,NV2
         WRITE(C9,'(1PD9.2)') SCEF2(IT)
         STRING(17+IPOS:21+IPOS) = C9(1:5)
         STRING(22+IPOS:24+IPOS) = C9(7:9)
         IPOS =IPOS+8
 299   CONTINUE
       WRITE(IUNT11,'(1A100)')STRING
C
       DO 300 I = 1,ITRANU
         IF(TCODEU(I).EQ.' ')THEN
             STRING = BLANKS
             WRITE(STRING,'(1X,I3,I4)')I2UA(I),I1UA(I)
             WRITE(C9,'(1PD9.2)') AVALU(I)
             STRING(9:13) = C9(1:5)
             STRING(14:16) = C9(7:9)
             IPOS = 0
             DO 210 IT = 1,NV2
               WRITE(C9,'(1PD9.2)') SCOMU(IT,I)
               STRING(17+IPOS:21+IPOS) = C9(1:5)
               STRING(22+IPOS:24+IPOS) = C9(7:9)
               IPOS =IPOS+8
  210        CONTINUE
         ENDIF
         IF(I1UA(I).NE.I2UA(I))THEN
           WRITE(IUNT11,'(1A100)')STRING
         ENDIF
  300  CONTINUE
       WRITE(IUNT11,'(1A4)')'  -1'
       WRITE(IUNT11,'(1A8)')'  -1  -1'
C
       WRITE(IUNT11,1003)'ADAS210',DSNINP,DSNTMP
       WRITE(IUNT11,1004)(INDJL(I),I=1,NJLEVX)
       WRITE(IUNT11,1005)
       WRITE(IUNT11,1006)(BNDLS(I),I=1,NJLEVX)
       WRITE(IUNT11,1007)
       WRITE(IUNT11,1008)(CPRTA2(I),I=1,NBCPRT)
       WRITE(IUNT11,1009)
       WRITE(IUNT11,1010)(BNDPR(I),I=1,NBCPRT)
       WRITE(IUNT11,1011)DATE
       CLOSE(IUNT11,STATUS = 'KEEP')
C
       ELSE
         GOTO 100
       ENDIF
C       CLOSE(IUNT10,STATUS='KEEP')
 9999  STOP
C
C----------------------------------------------------------------------
 1000  FORMAT(I5,1X,1A18,'(',I1,')',I1,'(',F4.1,')',F11.1,2X)
 1001  FORMAT(1A3,I2,2I10,1X,5(F10.1,1A7,1X))
 1002  FORMAT(1A3,I2,2I10,1X,F10.1)
 1003  FORMAT('C',79('-')/
     &        'C'/
     &        'C  File generated by expansion of an LS-resolved file'/
     &        'C'/
     &        'C  Program: ',1A7/
     &        'C'/
     &        'C  Source file: ',1A80/
     &        'C  Template file: '/
     &        'C  ',1A60/
     &        'C'/
     &        'C  Template File indexing:')
 1004  FORMAT('C ',I5,I5,I5,I5,I5,I5,I5,I5,I5,I5)
 1005  FORMAT('C  Source File Assignment:')
 1006  FORMAT('C ',I5,I5,I5,I5,I5,I5,I5,I5,I5,I5)
 1007  FORMAT('C '/
     &        'C  Original parent metastables: ')
 1008  FORMAT('C ',3X,1A7,1X,1A7,1X,1A7,1X,1A7,1X,1A7)
 1009  FORMAT('C  Parent unbundling vector: ')
 1010  FORMAT('C ',I8,I8,I8,I8,I8)
 1011  FORMAT('C '/
     &        'C'/
     &        'C  Insert producer id here:'/
     &        'C  Date: ',1A8/
     &        'C'/
     &        'C',79('-'))
 1012  FORMAT(I5,1X,1A17,'*','(',I1,')',I1,'(',F4.1,')',F11.1,2X)
 1013  FORMAT(1A8)
 1014  FORMAT(1A4,1X)
C----------------------------------------------------------------------
C
      END
