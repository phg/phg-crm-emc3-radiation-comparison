CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas210/baprs1.for,v 1.2 2010/04/09 13:54:33 mog Exp $ Date $Date: 2010/04/09 13:54:33 $
CX
       SUBROUTINE BAPRS1(NDMET,STRING,WNO,CPL,NPT,IPLA,ZPLA,IFAIL)
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BAPRS1 *********************
C
C  PURPOSE:  TO ANALYSE THE TAIL CHARACTER STRING OF A LEVEL DATA LINE
C            OF A SPECIFIC ION FILE INTO WAVE-NUMBER AND SETS OF
C            (PARENT IDENTIFIER, EFFECTIVE ZETA FOR THE PARENT) PAIRS.
C
C  CALLING PROGRAM: ADAS210
C
C  NOTES: DETECT  -  LEVEL WAVE NUMBER WHICH PRECEEDS FIRST '{'
C                 -  SETS OF   PARENT INDEX CONTAINED IN '{.}'
C                              FOLLOWED BY EFFECTIVE ZETA
C         NB. 'X' AS FIRST PARENT ASSIGNMENT MEANS EXCLUDE IONISATION
C             FROM THIS LEVEL.
C             NO PARENT ASSIGNMENT MEANS TAKE LOWEST PARENT WITH
C             ZETA =1.
C             LOWEST PARENT BUT NO ZETA MEANS TAKE ZETA =1.
C             IF THERE IS MORE THAN ONE PARENT THEN ZETA'S MUST BE IN.
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDMET    =  MAXIMUM NUMBER OF PARENTS
C  INPUT : (C*(*))STRING   =  STRING TO BE PARSED
C
C  OUTPUT: (R*8)  WNO      =  EXCITATION WAVE NUMBER OF LEVEL RELATIVE
C                             TO LOWEST PARENT
C  OUTPUT: (C*1)  CPL      =  LEAD PARENT FOR IONISATION  OR 'X'
C  OUTPUT: (I*4)  NPT      =  NUMBER OF PARENTS DETECTED
C  OUTPUT: (I*4)  IPLA()   =  PARENT INDICES.
C  OUTPUT: (R*8)  ZPLA()   =  EFFECTIVE ZETA FOR PARENT IPLA()
C  OUTPUT: (I*4)  IFAIL    =  0 - SUBROUTINE CONCLUDES CORRECTLY
C                             1 - FAULT DETECTED IN SUBROUTINE
C                             2 - SINGLE IONISATION POTENTIAL DETECTED
C
C          (I*4)  MAXWRD   =  MAXIMUM NUMBER OF WORDS SOUGHT INITIALLY
C                             INITIALLY, FINALLY NUMBER ACTUALLY FOUND
C          (I*4)  NFIRST   =  FIRST WORD TO BE EXTRACTED FROM STRING
C          (I*4)  IFIRST() =  INDEX OF FIRST CHAR. OF WORD () IN STRING
C          (I*4)  ILAST()  =  INDEX OF LAST  CHAR. OF WORD () IN STRING
C          (I*4)  IWORDS   =  NUMBER OF WORDS FOUND IN STRING
C
C          (L*4)  LSET     =  .TRUE.  -  WAVE NUMBER PART SET
C                             .FALSE. -  WAVE NUMBER PART NOT SET
C          (L*4)  LWNO     =  .TRUE.  -  IN THE WAVE NUMBER PART
C                             .FALSE. -  NOT IN THE WAVE NUMBER PART
C          (L*4)  LPRNT    =  .TRUE.  -  IN A PARENT SPECIFIER
C                             .FALSE. -  NOT IN A PARENT SPECIFIER
C          (L*4)  LZETA    =  .TRUE.  -  IN A ZETA SPECIFIER
C                             .FALSE. -  NOT IN A ZETA SPECIFIER
C          (I*4)  IC       =  GENERAL USE
C          (I*4)  IABT     =  FAILURE NUMBER FROM R8FCTN
C          (I*4)  NCHAR    =  NUMBER OF CHARACTERS IN SUBSTRING
C          (C*15) SSTRNG   =  ISOLATED SUBSTRING
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          I4FCTN     ADAS      CONVERTS FROM CHAR. TO INTEGER  VARIABLE
C          XXWORD     ADAS      PARSES A STRING INTO SEPARATE WORDS
C                               FOR ' ()<>{}' DELIMITERS
C
C AUTHOR:  HP SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    22/06/92
C
C UPDATE:   8/07/93 - HPS  ALTERED TO USE XXWORD PARSING ROUTINE
C
C UPDATE:  21/12/95 - DHB  INCREASED SIZE OF IFIRST & ILAST TO 12 IN 
C                          LINE WITH CHANGE TO NDMET
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION : 1.2
C DATE    : 09-04-2010
C MODIFIED: Martin O'Mullane
C           - Change integer*4 to integer.
C
C-----------------------------------------------------------------------
       CHARACTER STRING*(*)   , SSTRNG*15  , CPL*1
       CHARACTER CDELIM*7
C
       INTEGER   NDMET
       INTEGER   NPT          , IABT       , IC        , NCHAR     , I
       INTEGER   IFAIL
       INTEGER   IPLA(NDMET)
       INTEGER   NFIRST       , MAXWRD     , IWORDS
       INTEGER   IFIRST(12)   , ILAST(12)
       INTEGER   I4FCTN       , I4UNIT
C
       LOGICAL   LSET         , LWNO         , LPRNT        , LZETA
C
       REAL*8    WNO
       REAL*8    ZPLA(NDMET)
       REAL*8    R8FCTN
C-----------------------------------------------------------------------
       DATA CDELIM/' ()<>{}'/
C-----------------------------------------------------------------------
       LSET   = .FALSE.
       LPRNT  = .FALSE.
       LZETA  = .FALSE.
       NPT    = 0
       NCHAR  = 0
       IFAIL = 0
       WNO=0.0D0
C
       DO 10 IC=1,NDMET
        IPLA(IC)=0
        ZPLA(IC)=0.0D0
   10  CONTINUE
C
       NFIRST=1
       MAXWRD=2*NDMET+1
       CALL XXWORD(STRING,CDELIM,NFIRST,MAXWRD,IFIRST,ILAST,IWORDS)
C
       IF(IWORDS.EQ.0)THEN
           WRITE(I4UNIT(-1),1001)'NO EXCITATION ENERGY'
           WRITE(I4UNIT(-1),1002)
           IFAIL = 1
           RETURN
       ELSEIF (IWORDS.EQ.1) THEN
           WNO=R8FCTN(STRING(IFIRST(1):ILAST(1)),IABT)
           IF(IABT.GT.0)THEN
               WRITE(I4UNIT(-1),1001)'FAULT IN EXCIT. ENERGY'
               WRITE(I4UNIT(-1),1002)
               IFAIL = 1
               RETURN
           ENDIF
               NPT=1
               CPL='1'
               IPLA(1)=1
               ZPLA(1)=1.0
               IFAIL = 2
           RETURN
       ELSEIF (IWORDS.EQ.2) THEN
           IF(STRING(IFIRST(2):ILAST(2)).EQ.'X')THEN
               WNO=R8FCTN(STRING(IFIRST(1):ILAST(1)),IABT)
               IF(IABT.GT.0)THEN
                   WRITE(I4UNIT(-1),1001)'FAULT IN EXCIT. ENERGY'
                   WRITE(I4UNIT(-1),1002)
                   IFAIL = 1
                   RETURN
               ENDIF
               NPT=1
               CPL='X'
               IPLA(1)=1
               ZPLA(1)=0.0
               IFAIL = 2
               RETURN
           ELSE
               WNO=R8FCTN(STRING(IFIRST(1):ILAST(1)),IABT)
               IF(IABT.GT.0)THEN
                   WRITE(I4UNIT(-1),1001)'FAULT IN EXCIT. ENERGY'
                   WRITE(I4UNIT(-1),1002)
                   IFAIL = 1
                   RETURN
               ENDIF
               NPT=1
               IPLA(1)=I4FCTN(STRING(IFIRST(2):ILAST(2)),IABT)
               IF(IABT.GT.0.OR.IPLA(1).GT.NDMET)THEN
                   WRITE(I4UNIT(-1),1001)'FAULT IN PARENT INDEX '
                   WRITE(I4UNIT(-1),1002)
                   IFAIL = 1
                   RETURN
               ENDIF
               CPL=STRING(IFIRST(2):ILAST(2))
               ZPLA(1)=1.0
               IFAIL = 2
               RETURN
           ENDIF
       ENDIF
       WNO=R8FCTN(STRING(IFIRST(1):ILAST(1)),IABT)
       IF(IABT.GT.0)THEN
           WRITE(I4UNIT(-1),1001)'FAULT IN EXCIT. ENERGY'
           WRITE(I4UNIT(-1),1002)
           IFAIL = 1
           RETURN
       ENDIF
C
       IF (MOD(IWORDS-1,2).NE.0)THEN
           WRITE(I4UNIT(-1),1001)'MISMATCH OF PARENTS'
           WRITE(I4UNIT(-1),1002)
           IFAIL = 1
           RETURN
       ENDIF
C
       NPT=(IWORDS-1)/2
C
       DO 100 I=1,NPT
        IPLA(I)=I4FCTN(STRING(IFIRST(2*I):ILAST(2*I)),IABT)
            IF(IABT.GT.0)THEN
                WRITE(I4UNIT(-1),1001)'FAULT IN PARENT INDEX '
                WRITE(I4UNIT(-1),1002)
                IFAIL = 1
                RETURN
            ENDIF
        ZPLA(I)=R8FCTN(STRING(IFIRST(2*I+1):ILAST(2*I+1)),IABT)
            IF(IABT.GT.0)THEN
                WRITE(I4UNIT(-1),1001)'FAULT IN ZETA VALUE   '
                WRITE(I4UNIT(-1),1002)
                IFAIL = 1
                RETURN
            ENDIF
  100  CONTINUE
       CPL=STRING(IFIRST(2):ILAST(2))
       RETURN
C-----------------------------------------------------------------------
 1001 FORMAT(1X,32('*'),' BAPRS1 ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,27('*'),' SUBROUTINE TERMINATED ',28('*'))
C-----------------------------------------------------------------------
      END
