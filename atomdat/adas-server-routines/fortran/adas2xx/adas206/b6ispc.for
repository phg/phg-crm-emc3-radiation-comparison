CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6ispc.for,v 1.1 2004/07/06 11:25:26 whitefor Exp $ Date $Date: 2004/07/06 11:25:26 $
CX
      SUBROUTINE B6ISPC( NORD  , IORDR , ISULEV ,
     &                   IORDS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6ISPC *********************
C
C  PURPOSE: TO IDENTIFY IN THE ORDINARY LEVEL INDEX THE INDEX  FOR  THE
C           UPPER LEVEL OF THE SPECIFIC LINE POWER TRANSITION REQUESTED.
C
C  CALLING PROGRAM:  ADAS206
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS.
C  INPUT : (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C  INPUT : (I*4)  ISULEV  = UPPER ENERGY LEVEL OF SPECIFIC LINE POWER
C                           TRNSITION.
C  OUTPUT: (I*4)  IORDS   = INDEX OF SPECIFIC LINE POWER TRANSITION
C                           UPPER LEVEL IN ORDINARY LEVEL INDEX.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IO      = ORDINARY EXCITED LEVEL NUMBER COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXTERM     ADAS      TERMINATES PROGRAM WITH MESSAGE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER  I4UNIT
      INTEGER  NORD         , ISULEV         , IORDS
      INTEGER  IO
C-----------------------------------------------------------------------
      INTEGER  IORDR(NORD)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IORDS = 0
C-----------------------------------------------------------------------
         DO 1 IO=1,NORD
            IF ( IORDR(IO).EQ.ISULEV ) IORDS = IO
    1    CONTINUE
C-----------------------------------------------------------------------
         IF (IORDS.EQ.0) THEN
            WRITE(I4UNIT(-1),1000) ISULEV
            CALL XXTERM
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,32('*'),' B6ISPC ERROR ',32('*')/
     &       1X,'THE UPPER LEVEL OF SPECIFIC LINE POWER TANSITION (',
     &       I2,') IS A METASTABLE LEVEL')
C
C----------------------------------------------------------------------
C
      RETURN
      END
