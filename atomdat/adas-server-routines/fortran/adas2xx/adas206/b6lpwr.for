CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6lpwr.for,v 1.1 2004/07/06 11:25:39 whitefor Exp $ Date $Date: 2004/07/06 11:25:39 $
CX
      SUBROUTINE B6LPWR( NDTEM  ,
     &                   IT     , ICNTE  , IL     , NMET  ,
     &                   IZ1    , IPROJ  ,
     &                            IE1A   , IE2A   , IMETR ,
     &                   TEMP   ,
     &                   ER     , XIA    , EXCRE  ,
     &                   TPL0   , TPLBA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6LPWR *********************
C
C  PURPOSE: TO CALCULATE ZERO DENSITY AND HIGH N PROJECTION LINE POWERS,
C           FOR A GIVEN TEMPERATURE.
C
C  CALLING PROGRAM:  ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C
C  INPUT :  (I*4)  IT      = INDEX OF TEMPERATURE VALUE BEING ASSESSED
C  INPUT :  (I*4)  ICNTE   = NUMBER OF SELECTED ELECTRON IMPACT TRANSTNS
C  INPUT :  (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (I*4)  IZ1     = RECOMBINING ION CHARGE
C  INPUT :  (I*4)  IPROJ   = SPECIFIES INDEX OF LOWEST  LEVEL FOR WHICH
C                            EXTRAPOLATION TO HIGHER N  OF THE RADIATED
C                            POWER IS TO BE PERFORMED. ALL LEVELS ABOVE
C                            AND INCLUDING 'IPROJ' ARE TREATED.
C                           IF 'IPROJ' > 'IL' => EXTRAP'TN SWITCHED OFF
C
C  INPUT :  (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX.
C                            DIMENSION: TRANSITION INDEX
C  INPUT :  (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX.
C                            DIMENSION: TRANSITION INDEX
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C
C  INPUT :  (R*8)  TEMP    = TEMPERATURE (KELVIN)
C
C  INPUT :  (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                            DIMENSION: LEVEL INDEX
C  INPUT :  (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                            DIMENSION: LEVEL INDEX
C  INPUT :  (R*8)  EXCRE(,)= EXCITATION RATE COEFFS (cm**3/s)
C                            1st DIMENSION: TEMPERATURE INDEX
C                            2nd DIMENSION: TRANSITION INDEX
C
C  OUTPUT:  (R*8)  TPL0   = ZERO DENSITY LINE POWER ARISING FROM EXCI-
C                           TATION ONLY FROM THE GROUND LEVEL  FOR  A
C                           GIVEN TEMPERATURE 'TEMP'.
C                           (UNITS: ERGS CM3 SEC-1).
C  OUTPUT:  (R*8)  TPLBA()= HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                           FROM A PARTICULAR METASTABLE TO LEVELS
C                           'IPROJ' UPWARDS FOR A GIVEN TEMPERATURE
C                           'TEMP'.
C                           (UNITS: ERGS CM3 SEC-1)
C                             DIMENSION: METASTABLE  INDEX
C
C           (R*8)  TK2ATE  = PARAMETER = EQUATION CONSTANT = 1.5789D+05
C           (R*8)  R2LOSS  = PARAMETER = EQUATION CONSTANT = 2.17958D-11
C                            (CONVERTS RYDBERGS/SEC TO ERGS/SEC)
C
C           (I*4)  LLOWER  = SELECTED ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C           (I*4)  LUPPER  = SELECTED ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C           (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C
C           (R*8)  ATE     = EQUATION PARAMETER = 'TK2ATE'/'TEMP'
C           (R*8)  Z1      = 'IZ1'
C           (R*8)  Z2ATE   = 'Z1' * 'Z1' * 'ATE'
C           (R*8)  Z2ATE2  = 1.0 / ('Z1' * 'Z1' * 'ATE')
C           (R*8)  Z2ATEX  = SQRT( 1 / ('Z1' * 'Z1' * 'ATE' * 'ATE' ) )
C           (R*8)  V       = 'Z1' / SQRT('XIA()')
C           (R*8)  VP      = 'V'/(1+'V')
C           (R*8)  ATEL    = 'ATE' * 'XIA(LLOWER)'
C           (R*8)  ATEU    = 'ATE' * 'XIA(LUPPER)'
C           (R*8)  ATEUP   = 'ATEU' * 'VP' * 'VP'
C           (R*8)  PLB1    = USED IN CALCULATING 'PLB'
C           (R*8)  PLB2    = USED IN CALCULATING 'PLB'
C           (R*8)  PLB3    = USED IN CALCULATING 'PLB'
C           (R*8)  PLB     = HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                            FROM A PARTICULAR METASTABLE LEVEL 'LLOWER'
C                            TO  THE  LEVEL  'LUPPER'  FOR  TEMPERATURE
C                            'TEMP'.
C                            (UNITS: ERGS CM3 SEC-1)
C
C ROUTINES: NONE
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  24/01/91 - PE BRIDEN: SERIOUS ERROR-'TPLBA()' WAS INCORRECTLY
C                                DECLARED AS INTEGER - IT MUST BE REAL*8
C                                - THEREFORE 'TPLBA()' NOW REAL*8 -
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     TK2ATE              , R2LOSS
C-----------------------------------------------------------------------
      PARAMETER( TK2ATE = 1.5789D+05 , R2LOSS = 2.17958D-11 )
C-----------------------------------------------------------------------
      INTEGER    NDTEM               ,
     &           IT                  , ICNTE                ,
     &           IL                  , NMET                 ,
     &           IZ1                 , IPROJ
      INTEGER    LLOWER              , LUPPER               ,
     &           IM                  , IC
C-----------------------------------------------------------------------
      REAL*8     TEMP                , TPL0
      REAL*8     ATE                 , Z1                   ,
     &           Z2ATE               , Z2ATE2               ,
     &           Z2ATEX              , V                    ,
     &           VP                  , ATEL                 ,
     &           ATEU                , ATEUP                ,
     &           PLB1                , PLB2                 ,
     &           PLB3                , PLB
C-----------------------------------------------------------------------
      INTEGER    IE1A(ICNTE)         , IE2A(ICNTE)          ,
     &           IMETR(NMET)
C-----------------------------------------------------------------------
      REAL*8     ER(IL)              , XIA(IL)              ,
     &           TPLBA(NMET)         ,
     &           EXCRE(NDTEM,ICNTE)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ZERO OUTPUT VALUES
C-----------------------------------------------------------------------
C
      TPL0=0.0D0
C-----------------------------------------------------------------------
         DO 1 IM=1,NMET
            TPLBA(IM)=0.0D0
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP TEMPERATURE AND CHARGE CONSTANTS
C-----------------------------------------------------------------------
C
      ATE    = TK2ATE/TEMP
      Z1     = DBLE(IZ1)
      Z2ATE  = Z1*Z1*ATE
      Z2ATE2 = 1.0D0/(Z2ATE*ATE)
      Z2ATEX = 1.0D0/DSQRT(Z2ATE)
C
C-----------------------------------------------------------------------
C EVALUATE ZERO DENSITY LINE POWER.
C-----------------------------------------------------------------------
C
         DO 2 IC=1,ICNTE
            IF (IE1A(IC).EQ.1)
     &          TPL0 = TPL0 + ( R2LOSS*ER(IE2A(IC))*EXCRE(IT,IC) )
    2    CONTINUE
C
C-----------------------------------------------------------------------
C EVALUATE HIGH N PROJECTION PARTS - IF REQUIRED.
C-----------------------------------------------------------------------
C
         IF (IPROJ.LE.IL) THEN
            DO 3 IC=1,ICNTE
               IF (IE2A(IC).GE.IPROJ) THEN
                  LLOWER = IE1A(IC)
                  LUPPER = IE2A(IC)
                     DO 4 IM=1,NMET
                        IF (IMETR(IM).EQ.LLOWER) THEN
                           V     = Z1 / DSQRT( XIA(LUPPER) )
                           VP    = V/(V+1.0)
                           ATEL  = ATE * XIA(LLOWER)
                           ATEU  = ATE * XIA(LUPPER)
                           ATEUP = ATEU * ( VP*VP )
                           PLB1  = Z2ATEX * (ATEUP**1.5)
                           PLB1  = 1.0D0 + ( (ATEL-ATEUP)*(1.0D0+PLB1) )
                           PLB1  = PLB1 * DEXP(ATEUP-ATEU)
                           PLB2  = DEXP(-ATEU)*(1.0D0+ATEL)
                           PLB3  = 0.5 * R2LOSS * V * V * V * Z2ATE2 *
     &                             EXCRE(IT,IC)
                           PLB   = PLB3 * ( PLB1 - PLB2 )
                           TPLBA(IM) = TPLBA(IM) + PLB
                           IF (IM.EQ.1) TPL0 = TPL0 + PLB
                        ENDIF
    4                CONTINUE
               ENDIF
    3       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
