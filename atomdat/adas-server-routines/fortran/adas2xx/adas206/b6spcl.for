CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6spcl.for,v 1.1 2004/07/06 11:25:51 whitefor Exp $ Date $Date: 2004/07/06 11:25:51 $
CX
      SUBROUTINE B6SPCL( NDLEV   , NDMET   ,
     &                   IORDS   , NMET    ,
     &                   DENSX   ,
     &                   STCKMX  , STACKX ,
     &                   PLAS1   ,
     &                   PLASX   , PLSX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6SPCL *********************
C
C  PURPOSE: TO  CALCULATE  SPECIFIC  LINE  POWERS  FOR  METASTABLES  AND
C           SPECIFIC EQUILIBRIUM LINE POWER.
C
C  CALLING PROGRAM:  ADAS206
C
C  SUBROUTINE:
C
C  INPUT : (I*4) NDLEV    = PARAMETER = MAX NO. OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4) NDMET    = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C
C  INPUT : (I*4) IORDS    = INDEX OF SPECIFIC LINE POWER TRANSITION
C                           UPPER ENERGY LEVEL IN ORDINARY LEVEL ARRAY.
C  INPUT : (I*4) NMET     = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C
C  INPUT : (R*8) DENSX    = ELECTRON DENSITY (UNITS: CM-3)
C
C  INPUT : (R*8) STCKMX() = METASTABLE POPULATIONS STACK
C                           AT FIXED TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE INDEX
C  INPUT : (R*4) STACKX(,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                           ON METASTABLE LEVEL. AT FIXED TEMPERATURE
C                           AND DENSITY.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C
C  INPUT : (R*8)  PLAS1   = DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                           POWER TRANSITION.
C                           (UNITS: ERGS SEC-1)
C
C  OUTPUT: (R*8)  PLASX   = SPECIFIC EQUILIBRIUM LINE PWR COEFFICIENTS.
C                           AT FIXED TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS CM3 SEC-1)
C  OUTPUT: (R*8)  PLSX()  = SPECIFIC LINE POWERS FOR METASTABLES. THIS
C                           IS THE SUM OF ALL EMISSIONS  ORGINATING IN
C                           THE  COLLISIONAL-RADIATIVE  SENSE FROM THE
C                           METASTABLE. AT FIXED TEMPERATURE AND DENSITY
C                           (UNITS: ERGS SEC-1 )
C                             DIMENSION: METASTABLE  INDEX
C
C          (I*4) IM       = METASTABLE LEVEL ARRAY INDEX
C
C
C
C ROUTINES: NONE
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  20/05/93-P BRIDEN: STACKX ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV         , NDMET               ,
     &           IORDS         , NMET
      INTEGER    IM
C-----------------------------------------------------------------------
      REAL*8     DENSX         , PLAS1               , PLASX
C-----------------------------------------------------------------------
      REAL*8     STCKMX(NDMET)       ,
     &           PLSX(NDMET)
      REAL*4     STACKX(NDLEV,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      PLASX = 0.0D0
C-----------------------------------------------------------------------
         DO 1 IM=1,NMET
            PLSX(IM) = ( PLAS1 * DBLE(STACKX(IORDS,IM)) ) / DENSX
            PLASX    = PLASX + ( PLSX(IM) * STCKMX(IM) )
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
