CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6spf0.for,v 1.2 2004/07/06 11:25:57 whitefor Exp $ Date $Date: 2004/07/06 11:25:57 $
CX
      SUBROUTINE B6SPF0( REP  , DSNINC )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS206
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSNINC  = FULL INPUT COPASE DATA SET NAME
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    27/02/91 - ADAS91 VERSION (DIFFERENT TO ADAS90 VERSION)
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2                               DATE: 24-04-03
C MODIFIED: Martin O'Mullane
C		- Remove redundant ion adf04 file and LDSEL
C                 which were only used in the IBM version. 
C                 These were not passed from IDL in any case.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSNINC*80
C-----------------------------------------------------------------------
      INTEGER      PIPEIN   , PIPEOU
      PARAMETER (  PIPEIN=5 , PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSNINC

C-----------------------------------------------------------------------

      RETURN
      END
