CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/adas206.for,v 1.4 2004/07/06 10:17:10 whitefor Exp $ Date $Date: 2004/07/06 10:17:10 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: ADAS206 *********************
C
C  ORIGINAL NAME: SPFPOPP
C                 (DEVELOPED FROM THE ORIGINAL PROGRAM: POPCODEP)
C
C  VERSION:  1.1 (Reflects changes made to V1.0 on 18/05/93)
C
C
C  PURPOSE:  TO  CALCULATE  TOTAL-LINE RADIATED POWER,   POPULATIONS  OF
C            METASTABLES IN EQUILIBRIUM, POPULATIONS OF  EXCITED  LEVELS
C            AND THEIR DEPENDENCE ON METASTABLE LEVELS.  THE INPUT  DATA
C            DERIVES FROM MASTER CONDENSED FILES WHICH CONTAIN  ELECTRON
C            IMPACT RATE DATA (GAMMA VALUES) AS FUNCTION OF TEMPERATURE.
C            THESE FILES MAY ALSO CONTAIN INFORMATION ON  PROTON  IMPACT
C            TRANSITIONS AND RECOMBINATION COEFFICIENTS (FREE ELECTRON &
C            CHARGE EXCHANGE).
C
C            PROCESSES  CAN  INCLUDE  ELECTRON AND PROTON IMPACT,  SPON-
C            TANEOUS EMISSION,   FREE ELECTRON RECOMBINATION  AND CHARGE
C            EXCHANGE RECOMBINATION DEPENDING ON THE INPUT DATA SET.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED IN PARTITIONED DATASETS AS
C            FOLLOWS:-
C
C                    'JETSHP.<SE>LIKE.DATA(<MEMBER>)'
C
C            WHERE, <SE> = ELEMENT SYMBOL FOR ISO-ELECTRON SERIES
C                          (ONE OR TWO CHARACTERS)
C
C            THE <MEMBER> NAME IS CONSTRUCTED AS FOLLOWS:
C
C               <INITIALS OF DATA SOURCE><YEAR><ELEMENT SYMBOL><*>
C
C               E.G. HPS89BE,  BKT1988C
C
C               <INITIALS OF DATA SOURCE> - E.G. HPS = HP SUMMERS
C
C               <YEAR> - THIS MAY OR MAY NOT BE ABBREVIATED
C
C               IF <ELEMENT SYMBOL> IS PRESENT =>
C                                       SPECIFIC  ION  FILE.  ONE
C                                       ELEMENT ONLY.
C                                           E.G. BE = BERYLIUM
C                                                C  = CARBON
C               IF <ELEMENT SYMBOL> IS MISSING =>
C                                       GENERAL Z EXCITATION FILES
C                                       CONTAINING BETWEEN 2 AND 7
C                                       ELEMENTS (INCLUSIVE). THIS
C                                       PROGRAM WILL  NOT  ANALYSE
C                                       THESE  FILES,  THEY   MUST
C                                       PASS THROUGH 'SPFMODM2' OR
C                                       AN    EQUIVALENT   PROGRAM
C                                       FIRST.
C
C               <*> - FURTHER EXTENSIONS TO MEMBER NAME MAY BE PRESENT,
C                    THESE MAY REFER TO DATA SET GENERATION STATISTICS.
C
C           EACH VALID MEMBER HAS ITS DATA REPRESENTED IN AN ABBREVIATED
C           FORM WHICH OMITS THE "D" OR "E" EXPONENT SPECIFIER.
C           e.g. 1.23D-06 or 1.23E-06 IS REPRESENTED AS 1.23-06
C                6.75D+07 or 6.75E+07 IS REPRESENTED AS 6.75+07
C
C           THEREFORE THE FORM OF EACH REAL NUMBER IN THE DATA SET IS:
C                          N.NN+NN or N.NN-NN
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C           NEUTRAL BEAM ENERGY :
C           RATE COEFFICIENTS   : CM3 SEC-1
C
C
C  PROGRAM:
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C          (I*4)  NDDEN   = PARAMETER = MAX. NUMBER OF DENSITIES ALLOWED
C          (I*4)  NDMET   = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT09  = PARAMETER = INPUT UNIT FOR PROTON DATA SET
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR COPASE DATA SET
C          (I*4)  IUNT11  = PARAMETER = OUTPUT UNIT FOR CONTOUR DATA SET
C          (I*4)  IUNT12  = PARAMETER = OUTPUT UNIT FOR TOTAL LINE POWER
C                                       PASSING FILE.
C          (I*4)  IUNT13  = PARAMETER = OUTPUT UNIT FOR SPECIFIC LNE PWR
C                                       PASSING FILE.
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  ICSTMX  = PARAMETER = 11 =
C                           MAXIMUM SIZE OF NOMENCLATURE STRING OUTPUT
C                           FOR PANELS AND PASSING DATASETS.(see CSTRGB)
C
C          (R*8)  D1      = PARAMETER = 1.0D0
C
C          (C*1)  PRGTYP  = PARAMETER = PROGRAM TYPE = 'L' (LNEPWR)
C
C          (I*4)  ISEL    = SPECIFIC LINE POWER: SELECTED ELECTRON
C                           IMPACT TRANSITION INDEX. (FOR USE WITH
C                           'IE1A()' , 'IE2A()' AND 'AA()' ARRAYS)
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  IDOUT   = 1 => INPUT DENSITIES IN CM-3
C                         = 2 => INPUT DENSITIES IN REDUCED FORM
C          (I*4)  IFOUT   = 1 => INPUT TEMPERATURES IN KELVIN
C                         = 2 => INPUT TEMPERATURES IN EV
C                         = 3 => INPUT TEMPERATURES IN REDUCED FORM
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C          (I*4)  IPROJ   = SPECIFIES INDEX OF LOWEST  LEVEL FOR WHICH
C                           EXTRAPOLATION TO HIGHER N  OF THE RADIATED
C                           POWER IS TO BE PERFORMED. ALL LEVELS ABOVE
C                           AND INCLUDING 'IPROJ' ARE TREATED.
C                           IF 'IPROJ' > 'IL' => EXTRAP'TN SWITCHED OFF
C          (I*4)  IRCODE  = RETURN CODE FROM 'FILEINF' IBM ROUTINE
C                           ( 0 => NO ERROR )
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C          (I*4)  ITSEL   = INDEX OF TEMPERATURE SELECTED FOR GRAPH
C                           (FROM INPUT LIST).
C          (I*4)  IZ      =  RECOMBINED ION CHARGE
C          (I*4)  IZ0     =         NUCLEAR CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C          (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C          (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C          (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C          (I*4)  IORDS   = INDEX OF SPECIFIC LINE POWER TRANSITION
C                           UPPER ENERGY LEVEL IN 'IORDR()'.
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IN      = DENSITY ARRAY INDEX
C          (I*4)  IT      = TEMPERATURE ARRAY INDEX
C          (I*4)  IS      = ENERGY LEVEL ARRAY INDEX
C
C          (R*8)  R8DCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  R8FBCH  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  ZEFF    = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C          (R*8)  ZEFFSQ  = 'ZEFF' * 'ZEFF'
C          (R*8)  PLAS1   = DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                           POWER TRANSITION GIVEN BY 'ISEL'.
C                           (UNITS: ERGS SEC-1)
C          (R*8)  DMINT   = +1 or -1 DEPENDING ON WHETHER THE  NUMBER OF
C                           ROW   INTERCHANGES   WAS   EVEN   OR    ODD,
C                           RESPECTIVELY, WHEN INVERTING A MATRIX  USING
C                           'XXMINV'.
C          (R*8)  XL1IN   = LOWER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: SEE 'IDOUT')
C          (R*8)  XU1IN   = UPPER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: SEE 'IDOUT')
C          (R*8)  XL1     = LOWER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: CM-3)
C          (R*8)  XU1     = UPPER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: CM-3)
C          (R*8)  YL1     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C          (R*8)  YU1     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (L*4)  LSOLVE  = .TRUE.  => SOLVE LINEAR EQUATION USING
C                                      'XXMINV'.
C                           .FALSE. =>DO NOT SOLVE LINEAR EQUATION USING
C                                     'XXMINV' - INVERT MATRIX ONLY.
C          (L*4)  LDOUT   = .TRUE.  => OUTPUT DATA GENERATED FOR INPUT
C                                      FILE.
C                           .FALSE. => NO OUTPUT DATA GENERATED FOR
C                                      INPUT FILE.
C          (L*4)  LCONT   = .TRUE.  => OUTPUT DATA TO CONTOUR PASSING
C                                      FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      CONTOUR PASSING FILE.
C          (L*4)  LPTOT   = .TRUE.  => OUTPUT DATA TO TOTAL LINE POWER
C                                      PASSING FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      TOTAL LINE POWER PASSING FILE.
C          (L*4)  LPSPC   = .TRUE.  => OUTPUT DATA TO SPECIFIC LNE PWR
C                                      PASSING FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      SPECIFIC LNE PWR PASSING FILE.
C          (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LGPH    = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                         = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                         = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LDSEL   = .TRUE.  => COPASE DATA SET INFORMATION
C                                      TO BE DISPLAYED BEFORE RUN.
C                         = .FALSE. => COPASE DATA SET INFORMATION
C                                      NOT TO BE DISPLAYED BEFORE RUN.
C          (L*4)  LLSEL   = .TRUE.  => OUTPUT LINE POWER RATIOS FORMED
C                                      ACCORDING TO POWER  FOR  LOWEST
C                                      FIRST OF THE INPUT DENSITIES.
C                         = .FALSE. => OUTPUT LINE POWER RATIOS FORMED
C                                      ACCORDING TO ZERO DENSITY POWER
C          (L*4)  LNORM   = .TRUE.  => IF NMET=1 NORMALISE TOTAL AND
C                                      SPECIFIC LINE POWER OUTPUT FILES
C                                      PLT & PLS TO STAGE TOT. POPULATN.
C                         = .FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                      METASTABLE POPULATIONS.
C          (L*4)  LPSEL   = .TRUE.  => INCLUDE PROTON COLLISIONS
C                         = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C          (L*4)  LZSEL   = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                      PLASMA Z EFFECTIVE'ZEFF'.
C                         = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                      WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                         (ONLY USED IF 'LPSEL=.TRUE.')
C          (L*4)  LISEL   = .TRUE.  => INCLUDE IONISATION RATES
C                         = .FALSE. => DO NOT INCLUDE IONISATION RATES
C          (L*4)  LHSEL   = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                      NEUTRAL HYDROGREN.
C                         = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                      FROM NEUTRAL HYDROGREN.
C          (L*4)  LRSEL   = .TRUE.  => INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SETS
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SETS
C          (L*4)  OPEN9   = .TRUE.  => FILE ALLOCATED TO UNIT 9.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 9.
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  OPEN11  = .TRUE.  => FILE ALLOCATED TO UNIT 11.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 11.
C          (L*4)  OPEN12  = .TRUE.  => FILE ALLOCATED TO UNIT 12.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 12.
C          (L*4)  OPEN13  = .TRUE.  => FILE ALLOCATED TO UNIT 13.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 13.
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                         = 'NO ' => CONTINUE PROGRAM EXECUTION.
C          (C*3)  TITLED  = ELEMENT SYMBOL.
C          (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C          (C*80) DSNPAP  = OUTPUT TEXT FILE NAME
C          (C*80) DSNOUT  = OUTPUT CONTOUR DATA SET NAME
C          (C*80) DSNTOT  = OUTPUT TOTAL LINE POWER PASSING FILE NAME
C          (C*80) DSNSPC  = OUTPUT SPECIFIC LNE PWR PASSING FILE NAME
C          (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C          (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C          (C*80) DSNINP  = INPUT PROTON DATA SET NAME (SEQUENTIAL)
C          (C*80) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C          (C*80) DSN80   = 80 BYTE CHARACTER STRING  FOR  TEMPORARY
C                           STORAGE OF DATA SET NAMES FOR DYNAMIC ALLOC.
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  PAR(,)  =
C          (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  AA()    = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  TINE()  = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C          (R*8)  TINP()  = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C          (R*8)  TINH()  = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C          (R*8)  TEVA()  = ELECTRON TEMPERATURES (UNITS: EV)
C          (R*8)  TPVA()  = PROTON TEMPERATURES   (UNITS: EV)
C          (R*8)  THVA()  = NEUTRAL HYDROGEN TEMPERATURES (UNITS: EV)
C          (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C          (R*8)  TPA()   = PROTON TEMPERATURES   (UNITS: KELVIN)
C          (R*8)  THA()   = NEUTRAL HYDROGEN TEMPERATURES (UNITS:KELVIN)
C          (R*8)  DINE()  = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C          (R*8)  DINP()  = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C          (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C          (R*8)  DENSPA()= PROTON DENSITIES    (UNITS: CM-3)
C          (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C          (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C          (R*8)  RHS()   = USED ONLY IF 'LSOLVE=.TRUE.' WHEN CALLING
C                           THE SUBROUTINE 'XXMINV'. CONTAINS THE SET
C                           OF 'N' LINEAR EQUATIONS TO BE SOLVED.
C                           INPUT  TO   'XXMINV': RIGHT HAND SIDE VECTOR
C                           OUTPUT FROM 'XXMINV': SOLUTION VECTOR
C                           (ACTS ONLY AS A DUMMY IN THIS PROGRAM)
C          (R*8)  CIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                           FIXED TEMPERATURE.
C                            DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  VHRED() = CHARGE EXCHANGE RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C          (R*8)  VRRED() = FREE ELECTRON RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  EXCRE(,) = ELECTRON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRE(,)= ELECTRON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  EXCRP(,) = PROTON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRP(,)= PROTON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  VECH(,)  = CHARGE-EXCHANGE RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C          (R*8)  VECR(,)  = FREE ELECTRON RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C          (R*8)  CRA(,)   = A-VALUE  (sec-1)  MATRIX  COVERING  ALL
C                            TRANSITIONS.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCE(,)  = ELECTRON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCP(,)  = PROTON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CC(,)    = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  CMAT(,)  = (INVERTED)   RATE  MATRIX  COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            PRE  'XXMINV' : NOT-INVERTED
C                            POST 'XXMINV' : INVERTED
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C          (R*8)  CRED(,)  = MATRIX  OF  TRANSITION   RATES   BETWEEN
C                            METASTABLE LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C          (R*8)  CRMAT(,) = INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            BEFORE INPUT  TO   XXMINV: NOT INVERTED
C                            AFTER  OUTPUT FROM XXMINV: AS-ABOVE
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C
C          (R*8)  PL(,,)  = TOTAL LINE POWERS FOR METASTABLES. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE.
C                            => P(TOTAL)/N(IMET)        (ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C          (R*8)  PLS(,,) = SPECIFIC LINE POWERS FOR METASTABLES. THIS
C                           IS THE SPECIFIC EMISSION ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE. (SEE 'ISEL')
C                            => P(SPECIFIC)/N(IMET)  (ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C          (R*8)  PL0()   = ZERO DENSITY LINE POWER ARISING FROM EXCI-
C                           TATION ONLY FROM THE GROUND LEVEL.
C                           (UNITS: ERGS CM3 SEC-1). (DIMENSION: TEMP.)
C                             => P/(DENS*N(1))
C          (R*8)  PLA(,)  = TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C          (R*8)  PLAS(,) = SPECIFIC EQUILIBRIUM LINE PWR COEFFICIENTS.
C                            => P(SPECIFIC)/(DENS*N(1)) (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C          (R*8)  PLA1()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                           (UNITS: ERGS SEC-1) (DIMENSION: LEVEL INDEX)
C          (R*8)  PLBA(,) = HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                           FROM A PARTICULAR METASTABLE TO LEVELS
C                           'IPROJ' UPWARDS. (UNITS: ERGS CM3 SEC-1)
C                             => P/(DENS*N(IMET))
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C
C          (R*8) POPAR(,,) = LEVEL POPULATIONS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVR(,,)  = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVH(,,)  = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*4) STACK(,,,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C          (R*8) STVRM(,,) = METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STVHM(,,) = METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES.
C          (C*22) STRGA() = LEVEL DESIGNATIONS
C
C          (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                       'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C          (L*4)  LTRNG(,)= .TRUE.  => TEMPERATURE VALUE WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                         = .FALSE. =>TEMPERATURE VALUE NOT WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                           1st DIMENSION: TEMPERATURE INDEX.
C                           2nd DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C
C NOTE:
C
C          INPUT/OUTPUT STREAM ALLOCATIONS:
C          --------------------------------
C
C          STREAM  9: INPUT - PROTON IMPACT INPUT DATA FILE
C          ('IUNT09')
C
C          STREAM 10: INPUT - SPECIFIC ION RATE DATA INPUT FILE FROM
C          ('IUNT10')         DATABASE (SEE DATA SECTION ABOVE).
C
C          STREAM  7: OUTPUT - MAIN OUTPUT TABLES
C          ('IUNT07')
C
C          STREAM 11: OUTPUT - POPULATION  DATA  FOR DIAGNOSTIC USE, IF
C          ('IUNT11')          REQUESTED.   THIS DATA IS FOR SUBSEQUENT
C                              INPUT  INTO  THE  DIAGNOSTIC AND CONTOUR
C                              GRAPHING PROGRAM 'CONTOUR'.
C
C          STREAM 12: OUTPUT - TOTAL LINE POWER DATA, IF REQUESTED.
C          ('IUNT12')          THIS  DATA IS IN THE FORM  OF  A  PASSING
C                              FILE.
C
C          STREAM 13: OUTPUT - SPECIFIC LINE POWER DATA, IF REQUESTED.
C          ('IUNT13')          THIS  DATA IS IN THE FORM  OF  A  PASSING
C                              FILE.
C
C          OPTIONAL POPULATION GRAPHS CAN BE PRODUCED. A SEPARATE GRAPH
C          IS DISPLAYED FOR EACH SINGLE INPUT TEMPERATURE SELECTED FROM
C          THE INPUT TEMPERATURE SET.
C
C          INPUT TEMPERATURES 'TIN?()' ARE CONVERTED TO KELVIN 'T?VA()'.
C
C          EXPLANATION OF OUTPUT FILE TO STREAM 7
C          --------------------------------------
C
C          'I'    = COMPLETE   LEVEL INDEX
C          'IMET' = METASTABLE LEVEL INDEX
C          'IORD' = ORDINARY EXCITED LEVEL INDEX
C
C          POPULATION OF METASTABLE 'IMET'      =>  N(IMET)/N(1)
C          POPULATION OF LEVEL 'IORD', DEPENDENT ON METASTABLE 'IMET'
C                                               =>  N(IORD)/N(IMET)
C
C          POWER FOR EACH METASTABLE IS THE SUM OF ALL EMISSION ORIGINA-
C          TING IN THE COLLISIONAL-RADIATIVE SENSE FROM THE METASTABLE.
C                                               =>  P/N(IMET)
C                                                          (ERGS SEC-1)
C
C          TOTAL EQUILIBRIUM POWER IS THE SUMMED LINE POWER ASSUMING
C          THE EQUILIBRIUM FRACTIONS FOR THE METASTABLE POPULATIONS AS
C          GIVEN IN THE FIRST TABLE ON EACH PAGE.
C                                                =>  P/(DENS*N(1))
C                                                      (ERGS CM3 SEC-1)
C          THETA = TE/Z1**2
C          RHO   = DENS/Z1**7
C
C
C          EXPLANATION OF MATRICES:  CRA(,) , CRCE(,) , CRCP(,) :
C          ------------------------------------------------------
C
C          (IF   'LISEL=.FALSE.'   THIS  EXPLANATION  ALSO  VALID  FOR
C           CC(,)  AND  CRED(,) )
C
C                      initial level  (column)
C                 --------------------------------
C                 |   *1  R12  R13  R14  R15  R16
C          final  |  R21   *2  R23  R24  R25  R26
C          level  |  R31  R32   *3  R34  R35  R36
C                 |  R41  R42  R43   *4  R45  R46
C          (row)  |  R51  R52  R53  R54   *5  R56
C                 |  R61  R62  R63  R64  R65   *6
C
C
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C               Diagonal elements represent the negative sum of their
C               respective columns i.e.
C
C                                   6
C                  diagonal: *n = -SUM( Rjn )  ( Rnn = 0)
C                                  j=1
C
C                  e.g. *3 = - ( R13 + R23 + R43 + R53 + R63 )
C
C                  Therefore each column adds to zero.
C
C                  If Rfi = transition rate from i -> f then
C                  *n = - ( total transition rate from level n )
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B6SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          B6ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          B6LOSS     ADAS      CALCULATE DIRECT LINE POWER LOSSES.
C          B6ISPC     ADAS      FIND SPECFIC LINE POWER IN ORD.LEV.INDEX
C          B6LPWR     ADAS      CALC. 0 DENSITY/HIGH N EXTRAP. LNE PWRS
C          B6NORM     ADAS      PERFORM STAGE POPULATIONS NORMALISATION
C          B6TOTL     ADAS      CALCULATE TOTAL EQUIL. & MET. LINE PWRS
C          B6SPCL     ADAS      CALCULATE SPECIFIC EQUIL.&MET. LINE PWRS
C          B6SPF1     ADAS      GATHERS OUT FILE NAMES VIA ISPF PANELS
C          B6WR12     ADAS      OUTPUT DATA TO TOTAL LNEPWR PASSING FILE
C          B6WR13     ADAS      OUTPUT DATA TO SP. LNEPWR PASSING FILE
C          B6OUT1     ADAS      OUTPUT MAIN RESULTS TO OUTPUT STREAM
C          BXDATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          BXTTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          BXSETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          BXOUT0     ADAS      OUTPUT ION SPEC. ETC. TO OUTPUT STREAM
C          BXCHKM     ADAS      CHECKS IF TRANSITION EXIST TO METASTABLE
C          BXIORD     ADAS      SETS  UP ORDINARY LEVEL INDEX.
C          BXRATE     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C          BXRCOM     ADAS      ESTABLISHES RECOMBINATION RATE COEFFTS.
C          BXMCRA     ADAS      CONSTRUCTS A-VALUE MATRIX.
C          BXMCRC     ADAS      CONSTRUCTS EXC./DE-EXC. RATE COEF MATRIX
C          BXMCCA     ADAS      CONSTRUCTS WHOLE RATE MATRIX.
C          BXMCMA     ADAS      CONSTRUCTS ORDINARY LEVEL RATE MATRIX.
C          BXSTKA     ADAS      STACK UP ORDINARY POP. DEPENDENCE ON MET
C          BXSTKB     ADAS      STACK UP RECOMB. CONTRIBUTION FOR ORD.
C          BXSTKC     ADAS      STACK UP TRANSITION RATE BETWEEN METS.
C          BXSTKD     ADAS      STACK UP RECOMB RATE FOR EACH MET. LEVEL
C          BXMPOP     ADAS      CALCULATE BASIC MET. LEVEL POPULATIONS.
C          BXSTVM     ADAS      CALCULATE MET. LEVEL RECOMB. COEFFTS.
C          BXPOPM     ADAS      CALCULATE METASTABLE LEVEL POPULATIONS.
C          BXPOPO     ADAS      CALCULATE ORDINARY LEVEL POPULATIONS.
C          BXOUTG     ADAS      OUTPUT GRAPHS USING GHOST80
C          BXWR11     ADAS      OUTPUT DATA TO CONTOUR PASSING FILE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXERYD     ADAS      CONVERTS ENERGIES FROM W.NO. TO RYDBERGS
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO EV.
C          XXDCON     ADAS      CONVERTS ISPF ENTERED DENS. TO CM-3.
C          XXRATE     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C                               FOR UNIT GAMMA VALUE.
C          XXMINV     ADAS      INVERTS MATRIX AND SOLVES EQUATIONS.
C          R8DCON     ADAS      REAL*8 FUNCTION: CONVERT DENSITY TO CM-3
C          R8FBCH     ADAS      REAL*8 FUNCTION:EVALUATES SHELL CONTRIB.
C                               TO IONISATION RATE  COEFFICIENT  IN  THE
C                               BURGESS-CHIDICHIMO APPROX.
C          FILEINF    IBM       SETS UP ATTRIBUTES TO OPEN DYNAMIC FILE
C
C         ( ROUTINES 'B5????' ARE USED IN PROGRAM  ADAS205  ONLY.
C           ROUTINES 'B6????' ARE USED IN PROGRAM  ADAS206  ONLY.
C           ROUTINES 'BX????' ARE USED IN PROGRAMS ADAS205 & ADAS206 )
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    08/10/90
C
C UPDATE:  10/12/90 - PE BRIDEN: ADAS91 - 'DSNINP' INCREASED TO 44 BYTES
C
C UPDATE:  11/12/90 - PE BRIDEN: ADAS91 - REPLACED 'XXOPEN' WITH FORTRAN
C                                        'OPEN' STATEMENT (AS 'DSNINC' &
C                                        'DSNINP' IN APPROPRIATE FORM).
C
C UPDATE:  17/01/91 - PE BRIDEN: ADAS91 - RENAMED SUBROUTINES AS FOLLOWS
C                                         'BXWR7A' -> 'BXOUT0'
C                                         'B6WR7B' -> 'B6OUT1'
C
C UPDATE:  23/01/91 - PE BRIDEN: ADAS91 - INTRODUCED  'TSCEF(,)'  PASSED
C                                         AS ARGUMENT TO B6ISPF & BXOUT0
C                                         ALONG WITH 'NV'.
C                                       - REMOVED 'SCEF' AND 'NV' FROM
C                                         'B6OUT1' ARGUMENT LIST.
C
C UPDATE:  24/01/91 - ADAS91 - PE BRIDEN - RE-ORDERED 'L?SEL'  ARGUMENTS
C                                          IN SUBROUTINES B6ISPF, B6OUT1
C                                        - RESTRUCTURED 'B6OUT1'.
C                                        - ADDED 'LTRNG(,)'
C
C UPDATE:  28/01/91 - ADAS91 - PE BRIDEN - REORDERED ARGUMENTS IN B6OUT1
C                                        - INTRODUCED FUNCTION 'R8DCON'
C
C UPDATE:  31/01/91 - PE BRIDEN: ADAS91 - ADDED 'LTRNG' AS ARGUMENT TO
C                                         'BXRATE' & 'BXRCOM'.
C
C UPDATE:  20/02/91 - P.E.BRIDEN: ADAS91- REMOVED IBM 'ERRSET' ROUTINE
C
C UPDATE:  09/04/91 - PE BRIDEN: COMMENTED OUT SECTION REFERING  TO  THE
C                                PROTON INPUT FILE NAME - UNCOMMENT THIS
C                                SECTION WHEN THE  PROGRAMS  ADAS205/206
C                                REQUIRE THE USE OF THE PROTON FILE.
C                                THE RELEVANT LINES  ARE  COMMENTED  OUT
C                                USING 'C*' IN COLUMNS 1 AND 2.
C
C UPDATE:  15/05/92 - P.E.BRIDEN: ADAS91- INCREASED NDLEV FROM 30 -> 75
C
C UPDATE:  26/05/92 - PE BRIDEN: ADAS91 - CHANGED NDLEV FROM  75 ->  110
C                                         CHANGED NDTRN FROM 300 -> 2100
C                                         CHANGED NDTEM FROM  20 ->   12
C                                         CHANGED NDDEN FROM  20 ->   12
C                                         CHANGED NDMET FROM   5 ->    4
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  18/05/93 - PE BRIDEN - ADAS91: INCLUDED SPECIAL NORMALISATION
C                                         HANDLING WHEN NMET=1.
C                                         ADDED VARIABLE LNORM AND CALL
C                                         TO SUBROUTINE B6NORM.
C                                         ADAS206 VERSION NUMBER
C                                         INCREMENTED FROM 1.0 TO 1.1
C
C UPDATE:  20/05/93 - PE BRIDEN - ADAS91: TO REFLECT CHANGES IN BXDATA
C                                         THE FOLLOWING ARRAY DIMENSION/
C                                         SIZE CHANGES WERE MADE:
C                                       1) CHARACTER CSTRGA *12 -> *18
C                                       2) SCOM(8,NDTRN)->SCOM(14,NDTRN)
C                                       3) SCEF(8)      -> SCEF(14)
C                                       4) TSCEF(8,3)   -> TSCEF(14,3)
C                                       5) REAL*8 STACK(,,,) -> REAL*4
C
C UPDATE:  06/07/93 - PE BRIDEN - ADAS91: ADDED VARIABLES CSTRGB() and
C                                         ICSTMX. (REQD. AS ONLY THE
C                                         LAST 11 NON-BLANK BYTES OF THE
C                                         LEVEL NOMENCLATURE STRINGS ARE
C                                         OUTPUT ON PANELS AND PASSING
C                                         FILE DATASETS.)
C                                         REPLACED CSTRGA WITH CSTRGB IN
C                                         THE CALLING ARGUMENT LISTS OF
C                                         ROUTINES: BXSETP,B6WR12,B6WR13
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				     DATE:05/06/97
C MODIFIED: RICHARD MARTIN
C	    -INCREASED NDTEM 12 -> 20
C
C VERSION: 1.3				     DATE:18-03-03
C MODIFIED: RICHARD MARTIN
C	    -INCREASED NDLEV, NDTRN, NDDEN, NDMET
C
C
C VERSION: 1.4                               DATE: 29-04-03
C MODIFIED: Martin O'Mullane
C		- Use xxdata_04 to read adf04 file. This requires
C                 new arrays some of which are not used in the 
C                 population calculation.
C               - bxttyp parameter list extended. 
C               - Remove redundant DSNINP and LDSEL.
C               - Send a signal to IDL at each Te and density step
C                 to enable a progress bar.
C		- Increase NDLEV to 1000 and NDTRN to 15000.
C               - If ionisation from excited levels is requested
C                 only activate if all levels are below the IP.
C
C-----------------------------------------------------------------------
      INTEGER   NDLEV  , NDTRN  , NDTEM  , NDDEN  , NDMET
      INTEGER   NVMAX  , NDQDN
      INTEGER   IUNT07 , IUNT09 , IUNT10 , IUNT11 , IUNT12 , IUNT13
      INTEGER   L1     , L2     , ICSTMX , ISTOP  , PIPEIN
      INTEGER   PIPEOU , ONE
C-----------------------------------------------------------------------
      REAL*8    D1
C-----------------------------------------------------------------------
      CHARACTER PRGTYP*1
C-----------------------------------------------------------------------
      PARAMETER( NDLEV=4000, NDTRN=500000, NDTEM=20, 
     &           NDDEN=20  , NDMET=5 )
      PARAMETER( NVMAX=14 , NDQDN=6   )
      PARAMETER( IUNT07=7 , IUNT09=9 , IUNT10=10, IUNT11=11, IUNT12=12 ,
     &           IUNT13=13 )
      PARAMETER( L1=1     , L2=2     , ICSTMX=11, PIPEIN=5             )
      PARAMETER( PIPEOU=6 , ONE=1    )
C-----------------------------------------------------------------------
      PARAMETER( D1 = 1.0D0 )
C-----------------------------------------------------------------------
      PARAMETER( PRGTYP = 'L' )
C-----------------------------------------------------------------------
      INTEGER   IRCODE    ,
     &          IZ        , IZ0       , IZ1     ,
     &          IL        , NV        , ITRAN   ,
     &          MAXLEV    , NMET      , NORD    , IFOUT   , IDOUT   ,
     &          MAXT      , MAXD      , ITSEL   , ISEL    , IPROJ   ,
     &          ICNTE     , ICNTP     , ICNTR   , ICNTH   , IORDS   ,
     &          IFORM     , IN        , IS      , IT      , i4unit
C-----------------------------------------------------------------------
      REAL*8    R8DCON    , R8FBCH
      REAL*8    BWNO      , ZEFF      , ZEFFSQ  , DMINT
      REAL*8    PLAS1
      REAL*8    XL1IN     , XU1IN     ,
     &          XL1       , XU1       , YL1     , YU1
C-----------------------------------------------------------------------
      CHARACTER REP*3     , TITLED*3  , DATE*8  ,
     &          DSNPAP*80 , DSNOUT*80 ,
     &          DSNTOT*80 , DSNSPC*80 ,
     &          DSNINC*80 , DSN80*80  ,
     &          TITLE*40  , GTIT1*40
C-----------------------------------------------------------------------
      LOGICAL   OPEN7     , OPEN9     , OPEN10  ,
     &          LGHOST    ,
     &          LPEND     , LPSEL     , LZSEL   , LISEL   ,
     &          LHSEL     , LRSEL     , LLSEL   , LNORM   , LGPH    ,
     &          LGRD1     , LDEF1     , LCONT   , LPTOT   , LPSPC   ,
     &          LDOUT     , LSOLVE    , LPAPER  , LNEWPA  , lion
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     ,
     &          IA(NDLEV)        ,
     &          ISA(NDLEV)       , ILA(NDLEV)       ,
     &          I1A(NDTRN)       , I2A(NDTRN)
      INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &          IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &          IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &          IP1A(NDTRN)      , IP2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8    SCEF(NVMAX)     ,
     &          TINE(NDTEM)     , TINP(NDTEM)      , TINH(NDTEM) ,
     &          TEVA(NDTEM)     , TPVA(NDTEM)      , THVA(NDTEM) ,
     &          TEA(NDTEM)      , TPA(NDTEM)       , THA(NDTEM)  ,
     &          DINE(NDDEN)     , DINP(NDDEN)      ,
     &          DENSA(NDDEN)    , DENSPA(NDDEN)    ,
     &          RATHA(NDDEN)    , RATIA(NDDEN)     ,
     &          AA(NDTRN)       , AVAL(NDTRN)      ,
     &          XJA(NDLEV)      , WA(NDLEV)        ,
     &          XIA(NDLEV)      , ER(NDLEV)        ,
     &          CIE(NDLEV)      , RHS(NDLEV)       ,
     &          VRRED(NDMET)    , VHRED(NDMET)
      REAL*8    TSCEF(NVMAX,3)                , SCOM(NVMAX,NDTRN)     ,
     &          EXCRE(NDTEM,NDTRN)            , EXCRP(NDTEM,NDTRN)    ,
     &          DEXCRE(NDTEM,NDTRN)           , DEXCRP(NDTEM,NDTRN)   ,
     &          VECH(NDTEM,NDLEV)             , VECR(NDTEM,NDLEV)     ,
     &          CRA(NDLEV,NDLEV)              ,
     &          CRCE(NDLEV,NDLEV)             , CRCP(NDLEV,NDLEV)     ,
     &          CC(NDLEV,NDLEV)               , CMAT(NDLEV,NDLEV)     ,
     &          CRED(NDMET,NDMET)             , CRMAT(NDMET,NDMET)
      REAL*8    PL0(NDTEM)                    , PLA1(NDLEV)           ,
     &          PLA(NDTEM,NDDEN)              , PLAS(NDTEM,NDDEN)     ,
     &          PLBA(NDMET,NDTEM)
      REAL*8    PL(NDMET,NDTEM,NDDEN)         , PLS(NDMET,NDTEM,NDDEN)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
      REAL*8    STVR(NDLEV,NDTEM,NDDEN)       , STVH(NDLEV,NDTEM,NDDEN)
      REAL*8    STCKM(NDMET,NDTEM,NDDEN)      ,
     &          STVRM(NDMET,NDTEM,NDDEN)      , STVHM(NDMET,NDTEM,NDDEN)
      REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      CHARACTER TCODE(NDTRN)*1  , CSTRGA(NDLEV)*18 , CSTRGB(NDLEV)*18 ,
     &          STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NDMET)    , LTRNG(NDTEM,3)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      DATA OPEN9 /.FALSE./ , OPEN10/.FALSE./
      DATA MAXD/0/,MAXT/0/
C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   npl           , iadftyp       , itieactn , iorb    
      integer   nplr          , npli          , icnti    , icntl   , 
     &          icnts
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      integer   ipla(ndmet,ndlev)             , npla(ndlev)
      integer   iitrn(ndtrn)                  , iltrn(ndtrn)       , 
     &          istrn(ndtrn)                  ,                    
     &          ia1a(ndtrn)                   , ia2a(ndtrn)        , 
     &          il1a(ndlev)                   , il2a(ndlev)        , 
     &          is1a(ndlev)                   , is2a(ndlev)        
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet) , 
     &          zpla(ndmet,ndlev)             , beth(ndtrn)        ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
      real*8    auga(ndtrn)                   , wvla(ndlev)
C----------------------------------------------------------------------
      character cpla(ndlev)*1                 , cprta(ndmet)*9      
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  , lbseta(ndmet)      ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lss04a(ndlev,ndmet)
C----------------------------------------------------------------------


C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )

 100   CONTINUE

C-----------------------------------------------------------------------
C IF FILES ARE ACTIVE ON UNITS 9 OR 10 - CLOSE THE UNITS
C-----------------------------------------------------------------------

      IF (OPEN10) CLOSE(10)
      OPEN10=.FALSE.

C-----------------------------------------------------------------------
C GET COPASE INPUT DATA SET NAME (FROM IDL SCREEN VIA PIPE)
C-----------------------------------------------------------------------

      CALL B6SPF0( REP , DSNINC )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------

         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF

C-----------------------------------------------------------------------
C OPEN COPASE INPUT DATA FILE - DSNINC
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT10 , FILE=DSNINC , STATUS='OLD' )
      OPEN10=.TRUE.

C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED COPASE DATASET
C-----------------------------------------------------------------------

       itieactn = 0
       
       call xxdata_04( iunt10 , 
     &                 ndlev  , ndtrn , ndmet   , ndqdn , nvmax ,
     &                 titled , iz    , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,     
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )


C-----------------------------------------------------------------------
C REDUCED LEVEL NOMENCLATURE STRINGS TO LAST 'ICSTMX' NON-BLANK BYTES
C-----------------------------------------------------------------------

      CALL BXCSTR( CSTRGA , IL     , ICSTMX ,
     &             CSTRGB
     &           )

C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPES.
C-----------------------------------------------------------------------

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )

C-----------------------------------------------------------------------
C CALCULATE LEVEL ENERGIES RELATIVE TO LEVEL 1 & IONISATION POT. IN RYDS
C-----------------------------------------------------------------------

      CALL XXERYD( BWNO , IL , WA  ,
     &             ER   , XIA
     &           )

C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------

      DO 1 IFORM=1,3
         CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
 1    CONTINUE


C-----------------------------------------------------------------------
C WRITE INFORMATION NEEDED ON PROCESSING SCREEN TO IDL VIA PIPE
C-----------------------------------------------------------------------

      CALL BXSETP( IZ0    , IZ     ,
     &             NDLEV  , IL     , ICNTE ,
     &             CSTRGB , ISA    , ILA   , XJA  ,
     &             STRGA
     &           )
     

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL PROCESSING SCREEN VIA PIPE)
C-----------------------------------------------------------------------

  200 CONTINUE
  
      CALL B6ISPF( LPEND  ,
     &             NDTEM , NDDEN  , NDMET , IL    ,
     &             NV    , TSCEF  ,
     &             ICNTE , IE1A   , IE2A  , STRGA ,
     &             TITLE , IPROJ  , NMET  , IMETR ,
     &             ISEL  ,
     &             IFOUT ,
     &             MAXT  , TINE  , TINP   , TINH  ,
     &             IDOUT ,
     &             MAXD  , DINE  , DINP   , RATHA , RATIA ,
     &             ZEFF  ,
     &             LLSEL , LNORM , LPSEL  , LZSEL , LISEL , LHSEL ,
     &             LRSEL
     &           )


C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C GO BACK TO INPUT SCREEN IF CANCEL WAS PRESSED
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100

C***********************************************************************
C ANALYSE ENTERED DATA IF DONE WAS PRESSED
C***********************************************************************
C
C-----------------------------------------------------------------------
C Check that ALL levels are below ion potential - if not swithch off
C ionisation from excited states.
C-----------------------------------------------------------------------
	
      lion = lisel
      if (lion) then
        do is = 1, il
          if (xia(is).LE.0.0D0) lion = .FALSE.
        end do
        if (.not.lion) write(i4unit(-1),1000)
      endif

C-----------------------------------------------------------------------
C CHECK IF ELECTRON IMPACT TRANSITIONS EXIST TO THE SELECTED METASTABLE
C-----------------------------------------------------------------------

      CALL BXCHKM( NMET   , IMETR , ICNTE , IE1A , LMETR )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV:  (TIN? -> T?VA)  (? = E,P,H )
C-----------------------------------------------------------------------

      CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINE  , TEVA   )
      CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINP  , TPVA   )
      CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINH  , THVA   )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TIN? -> T?A)  (? = E,P,H )
C-----------------------------------------------------------------------

      CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINE  , TEA    )
      CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINP  , TPA    )
      CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINH  , THA    )

C-----------------------------------------------------------------------
C CONVERT INPUT DENSITIES, INTO CM-3:
C-----------------------------------------------------------------------

      CALL XXDCON( IDOUT, L1, IZ1, MAXD , DINE  , DENSA  )
      CALL XXDCON( IDOUT, L1, IZ1, MAXD , DINP  , DENSPA )

C-----------------------------------------------------------------------
C  INITIALISE VECR, VECH AND CIE TO ZERO
C-----------------------------------------------------------------------

      DO 2 IS=1,NDLEV
         CIE(IS)  = 0.0D0
         DO 3 IT=1,NDTEM
            VECR(IT,IS) = 0.0D0
            VECH(IT,IS) = 0.0D0
 3       CONTINUE
 2    CONTINUE

C-----------------------------------------------------------------------
C CALCULATE ELECTRON IMPACT TRANSITN EXCITATION & DE-EXCIT'N RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.
C-----------------------------------------------------------------------

      CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &             ICNTE , MAXT   ,
     &             XJA   , ER     , TEA   ,
     &             IE1A  , IE2A   ,
     &             EXCRE , DEXCRE
     &           )

C-----------------------------------------------------------------------
C CALCULATE LINE POWER LOSES FOR ALL LINES AND SPECIFIC LINE.
C-----------------------------------------------------------------------

      CALL B6LOSS( NDTRN , NDLEV  ,
     &             ICNTE , ISEL   ,
     &             XJA   , ER     , AA    ,
     &             IE1A  , IE2A   ,
     &             PLAS1 , PLA1
     &           )

C-----------------------------------------------------------------------
C CALCULATE PROTON IMPACT TRANSITION EXCITATION & DE-EXCIT'N RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.             (ONLY IF REQUIRED).
C-----------------------------------------------------------------------

      IF (LPSEL) THEN
         CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                ICNTP , MAXT   ,
     &                XJA   , ER     , TPA   ,
     &                IP1A  , IP2A   ,
     &                EXCRP , DEXCRP
     &              )
      ENDIF

C-----------------------------------------------------------------------
C SETUP INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE LEVEL LIST 'IORDR'
C-----------------------------------------------------------------------

      CALL BXIORD( IL   ,
     &             NMET , IMETR ,
     &             NORD , IORDR
     &           )

C-----------------------------------------------------------------------
C FIND INDEX OF SPECIFIC LINE POWER TRANSITION UPPER LEVEL IN 'IORDR'
C-----------------------------------------------------------------------

      CALL B6ISPC( NORD  , IORDR , IE2A(ISEL) ,
     &             IORDS
     &           )

C-----------------------------------------------------------------------
C ELECTRON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------

      CALL BXRATE( NDTEM      , NDTRN  , D1     ,
     &             NV         , SCEF   , SCOM   ,
     &             MAXT       , TEA    ,
     &             ICNTE      , IETRN  ,
     &             EXCRE      , DEXCRE ,
     &             LTRNG(1,1)
     &           )

C-----------------------------------------------------------------------
C PROTON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------

      IF (LPSEL) THEN
         ZEFFSQ = ZEFF*ZEFF
         CALL BXRATE( NDTEM      , NDTRN  , ZEFFSQ ,
     &                NV         , SCEF   , SCOM   ,
     &                MAXT       , TPA    ,
     &                ICNTP      , IPTRN  ,
     &                EXCRP      , DEXCRP ,
     &                LTRNG(1,2)
     &              )
      ENDIF

C----------------------------------------------------------------------
C FREE ELECTRON RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

      IF (LRSEL) THEN
         CALL BXRCOM( NDTEM , NDTRN      , NDLEV  ,
     &                NV    , SCEF       , SCOM   ,
     &                MAXT  , TEA        ,
     &                ICNTR , IRTRN      , I2A    ,
     &                VECR  , LTRNG(1,1)
     &              )
      ENDIF

C----------------------------------------------------------------------
C CHARGE EXCHANGE RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

      IF (LHSEL) THEN
         CALL BXRCOM( NDTEM , NDTRN      , NDLEV  ,
     &                NV    , SCEF       , SCOM   ,
     &                MAXT  , THA        ,
     &                ICNTH , IHTRN      , I2A    ,
     &                VECH  , LTRNG(1,3)
     &              )
      ENDIF

C----------------------------------------------------------------------
C SET UP A-VALUE TRANSITION MATRIX 'CRA' (UNITS: SEC-1)
C----------------------------------------------------------------------

      CALL BXMCRA( NDTRN , NDLEV  ,
     &             ICNTE , IL     ,
     &             IE1A  , IE2A   ,
     &             AA    ,
     &             CRA
     &           )




C======================================================================
C Loop over Te and density - send a signal to IDL at each step.
C======================================================================
C
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C INCREMENT OVER OVER ALL INPUT TEMPERATURE VALUES
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

      DO 4 IT=1,MAXT

C-----------------------------------------------------------------------
C EVALUATE ZERO DENSITY/HIGH N PROJECTION LINE POWERS.
C-----------------------------------------------------------------------

         CALL B6LPWR( NDTEM   ,
     &                IT      , ICNTE      , IL     , NMET  ,
     &                IZ1     , IPROJ      ,
     &                IE1A       , IE2A   , IMETR ,
     &                TEA(IT) ,
     &                ER      , XIA        , EXCRE  ,
     &                PL0(IT) , PLBA(1,IT)
     &              )

C-----------------------------------------------------------------------
C SET UP EXCITATION/DE-EXCIATATION RATE COEFFT. MATRICES FOR GIVEN TEMP.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ELECTRON IMPACT TRANSITIONS 'CRCE' - (UNITS: CM**3/SEC-1)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

         CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                IT    , ICNTE  , IL     ,
     &                IE1A  , IE2A   ,
     &                EXCRE , DEXCRE ,
     &                CRCE
     &              )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C PROTON IMPACT TRANSITIONS 'CRCP' - (UNITS: CM**3/SEC-1) - IF SELECTED
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

         IF (LPSEL) THEN
            CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                   IT    , ICNTP  , IL     ,
     &                   IP1A  , IP2A   ,
     &                   EXCRP , DEXCRP ,
     &                   CRCP
     &                 )
         ENDIF

C-----------------------------------------------------------------------
C  SET UP VECTOR OF IONISATION RATE COEFFICIENTS - IF REQUIRED
C-----------------------------------------------------------------------

         IF (lion) THEN
            DO 5 IS=1,MAXLEV
               CIE(IS) = R8FBCH( IZ, XIA(IS), D1, TEA(IT) )
 5          CONTINUE
         ENDIF

C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C INCREMENT OVER OVER ALL INPUT DENSITY VALUES
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *

         DO 6 IN=1,MAXD

            write(pipeou,*)one
            call xxflsh(pipeou)

C-----------------------------------------------------------------------
C SET UP WHOLE RATE MATRIX  - (UNITS: SEC-1)
C-----------------------------------------------------------------------

            CALL BXMCCA( NDLEV     , IL         ,
     &                   LPSEL     , LISEL      ,
     &                   DENSA(IN) , DENSPA(IN) ,
     &                   CRA       ,
     &                   CRCE      , CRCP       , CIE    ,
     &                   CC
     &                 )

C-----------------------------------------------------------------------
C SET UP NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX - (UNITS: SEC-1)
C-----------------------------------------------------------------------

            CALL BXMCMA( NDLEV ,
     &                   NORD  , IORDR ,
     &                   CC    ,
     &                   CMAT
     &                 )

C-----------------------------------------------------------------------
C INVERT NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX
C-----------------------------------------------------------------------

            LSOLVE=.FALSE.
            CALL XXMINV( LSOLVE , NDLEV , NORD   ,
     &                   CMAT   , RHS   , DMINT
     &                  )
     
C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE POPULATION COMPONENTS ASSOCIATED WITH EACH
C METASTABLE LEVEL.
C-----------------------------------------------------------------------

            CALL BXSTKA( NDLEV            , NDMET  ,
     &                   NORD             , NMET   ,
     &                   IORDR            , IMETR  ,
     &                   CMAT             , CC     ,
     &                   STACK(1,1,IT,IN)
     &                 )

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE FREE-ELECT. RECOMB. CONTRIBUTIONS -IF SELECTED
C-----------------------------------------------------------------------

            IF (LRSEL) THEN
               CALL BXSTKB( NDTEM            , NDLEV  ,
     &                      IT               , NORD   ,
     &                      IORDR  ,
     &                      CMAT             , VECR   ,
     &                      STVR(1,IT,IN)
     &                    )
            ENDIF

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE CHARGE EXCHANGE CONTRIBUTIONS -IF SELECTED
C-----------------------------------------------------------------------

            IF (LHSEL) THEN
               CALL BXSTKB( NDTEM            , NDLEV  ,
     &                      IT               , NORD   ,
     &                      IORDR  ,
     &                      CMAT             , VECH   ,
     &                      STVH(1,IT,IN)
     &                    )
            ENDIF

C-----------------------------------------------------------------------
C STACK UP METASTABLE LEVEL TRANSITION RATE CONTRIBUTIONS
C-----------------------------------------------------------------------

            CALL BXSTKC( NDLEV            , NDMET            ,
     &                   NORD             , NMET             ,
     &                   IORDR            , IMETR            ,
     &                   CC               , STACK(1,1,IT,IN) ,
     &                   CRED
     &                 )

C-----------------------------------------------------------------------
C STACK UP METASTABLE FREE-ELECT. RECOMB. RATE CONTRIBUTIONS-IF SELECTED
C-----------------------------------------------------------------------

            IF (LRSEL) THEN
               CALL BXSTKD( NDTEM , NDLEV         , NDMET  ,
     &                      IT    , NORD          , NMET   ,
     &                      IORDR , IMETR         ,
     &                      CC    , STVR(1,IT,IN) , VECR   ,
     &                      VRRED
     &                    )
            ENDIF

C-----------------------------------------------------------------------
C STACK UP METASTABLE CHARGE EXCHANGE RATE CONTRIBUTIONS - IF SELECTED
C-----------------------------------------------------------------------

            IF (LHSEL) THEN
               CALL BXSTKD( NDTEM , NDLEV         , NDMET  ,
     &                      IT    , NORD          , NMET   ,
     &                      IORDR         , IMETR  ,
     &                      CC    , STVH(1,IT,IN) , VECH   ,
     &                      VHRED
     &                    )
            ENDIF

C-----------------------------------------------------------------------
C CALCULATE METASTABLE POPULATIONS
C-----------------------------------------------------------------------

            CALL BXMPOP( NDMET             ,
     &                   NMET              ,
     &                   CRED              ,
     &                   RHS               , CRMAT        ,
     &                   STCKM(1,IT,IN)
     &                 )

C-----------------------------------------------------------------------
C CALCULATE RECOMBINATIONS AND CHARGE EXCHANGE CONTRIBUTIONS.
C-----------------------------------------------------------------------

 
            IF (LRSEL) THEN
               CALL BXSTVM( NDMET            ,
     &                      NMET             ,
     &                      CRMAT            ,
     &                      VRRED            ,
     &                      STVRM(1,IT,IN)
     &                    )
            ENDIF
C-----------------------------------------------------------------------
            IF (LHSEL) THEN
               CALL BXSTVM( NDMET            ,
     &                      NMET             ,
     &                      CRMAT            ,
     &                      VHRED            ,
     &                      STVHM(1,IT,IN)
     &                    )
            ENDIF
C-----------------------------------------------------------------------
 6       CONTINUE
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C
C-----------------------------------------------------------------------
C CALCULATE THE SPECIFIC AND TOTAL EQUIL. & MET. LINE POWERS.
C-----------------------------------------------------------------------

         DO 7 IN=1,MAXD
            CALL B6TOTL( NDLEV          , NDMET            ,
     &                   NORD           , NMET             ,
     &                   IORDR          , IMETR            ,
     &                   DENSA(IN)      ,
     &                   STCKM(1,IT,IN) , STACK(1,1,IT,IN) ,
     &                   PLA1           , PLBA(1,IT)       ,
     &                   PLA(IT,IN)     , PL(1,IT,IN)
     &                 )
            CALL B6SPCL( NDLEV          , NDMET            ,
     &                   IORDS          , NMET             ,
     &                   DENSA(IN)      ,
     &                   STCKM(1,IT,IN) , STACK(1,1,IT,IN) ,
     &                   PLAS1          ,
     &                   PLAS(IT,IN)    , PLS(1,IT,IN)
     &                 )

C-----------------------------------------------------------------------
C STAGE POPULATION NORMALISATION (IF REQUESTED/APPLICABLE)
C-----------------------------------------------------------------------

            IF ( LNORM .AND. (NMET.EQ.1) ) THEN
               CALL B6NORM( NDLEV       , NDMET         ,
     &                      NORD        ,
     &                      STACK(1,1,IT,IN)            ,
     &                      PLA(IT,IN)  , PL(1,IT,IN)   ,
     &                      PLAS(IT,IN) , PLS(1,IT,IN)
     &                    )
            ENDIF

 7       CONTINUE
C-----------------------------------------------------------------------
 4    CONTINUE
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C
C-----------------------------------------------------------------------
C CALCULATE THE POPULATIONS OF EACH LEVEL.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE METASTABLE LEVELS FIRST.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      CALL BXPOPM( NDTEM  , NDDEN , NDMET , NDLEV ,
     &             MAXT   , MAXD  , NMET  ,
     &             DENSA , IMETR ,
     &             LRSEL , LHSEL ,
     &             RATIA , RATHA ,
     &             STCKM  , STVRM , STVHM ,
     &             POPAR
     &           )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE POPULATIONS OF EACH ORDINARY LEVEL.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      CALL BXPOPO( NDTEM  , NDDEN , NDMET , NDLEV ,
     &             MAXT   , MAXD  , NMET  , NORD  ,
     &             DENSA , IMETR , IORDR ,
     &             LRSEL , LHSEL ,
     &             RATIA , RATHA ,
     &             STACK  , STVR  , STVH  ,
     &             POPAR
     &           )

C-----------------------------------------------------------------------
C BXOUTG MOVED TO THE END OF THE PROGRAM WITH THE OTHER OUTPUT ROUTINES.
C-----------------------------------------------------------------------
C
C***********************************************************************
C END OF CURRENT FILE ANALYSIS - ESTABLISH IF GRAPHICAL AND PASSING FILE
C OUTPUT REQ'D
C***********************************************************************

 300  CONTINUE

      CALL B6SPF1( NDTEM  , TINE   , MAXT   , IFOUT  ,
     &             LPEND  ,
     &             LNEWPA , LPAPER , LCONT  , LPTOT  ,
     &             LPSPC  , DSNPAP , DSNOUT , DSNTOT , DSNSPC ,
     &             LGPH   , ITSEL  , GTIT1
     &           )


C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

      IF(LPEND) GOTO 200

C----------------------------------------------------------------------
C UNIX-IDL PORT - OPEN TEXT OUTPUT DATA SET - DSNPAP - IF REQUESTED.
C               THIS IS NEW FUNCTIONALITY.  IF LNEWPA IS .TRUE. THEN
C               EITHER A NEW FILE IS TO BE CREATED OR AN EXISTING FILE
C               IS TO BE REPLACED.  ANY FILE WHICH IS CURRENTLY OPEN
C               MUST BE CLOSED FIRST.  IF LNEWPA IS .FALSE. AND LPAPER
C               IS TRUE THEN OUTPUT IS APPENDED TO A PREVIOUSLY OPEN
C               FILE IF ANY.
C----------------------------------------------------------------------

      IF ( (LPAPER .AND. LNEWPA) .OR. 
     &     (LPAPER .AND. .NOT.OPEN7) ) THEN
         IF (OPEN7) CLOSE(UNIT=IUNT07)
         DSN80=' '
         DSN80=DSNPAP
         OPEN(UNIT=IUNT07,FILE=DSN80,STATUS='UNKNOWN')
         OPEN7=.TRUE.
      ENDIF

C-----------------------------------------------------------------------
C OPEN CONTOUR PLOT OUTPUT DATA SET - DSNOUT - IF REQUESTED
C-----------------------------------------------------------------------

      IF ( LCONT ) THEN
         DSN80=' '
         DSN80=DSNOUT
         OPEN(UNIT=IUNT11,FILE=DSN80,STATUS='UNKNOWN')
      ENDIF

C-----------------------------------------------------------------------
C OPEN TOTAL PASSING FILE OUTPUT DATA SET - DSNTOT - IF REQUESTED
C-----------------------------------------------------------------------

      IF ( LPTOT ) THEN
         DSN80=' '
         DSN80=DSNTOT
         OPEN(UNIT=IUNT12,FILE=DSN80,STATUS='UNKNOWN')
      ENDIF

C-----------------------------------------------------------------------
C OPEN SPECIFIC PASSING FILE OUTPUT DATA SET - DSNSPC - IF REQUESTED
C-----------------------------------------------------------------------

      IF ( LPSPC ) THEN
         DSN80=' '
         DSN80=DSNSPC
         OPEN(UNIT=IUNT13,FILE=DSN80,STATUS='UNKNOWN')
      ENDIF

C-----------------------------------------------------------------------
C OUTPUT DATA TO CONTOUR PLOT FILE - DSNOUT - IF REQUESTED
C-----------------------------------------------------------------------

      IF (LCONT) CALL BXWR11( IUNT11 , DSNINC , TITLED ,
     &                        NDLEV  , NDTEM  , NDDEN  , NDMET ,
     &                        IZ     , IZ0    , IZ1    , BWNO  ,
     &                        IL     , NMET   , NORD   ,
     &                        MAXT   , MAXD   , ICNTR  , ICNTH ,
     &                        IA     , ISA    , ILA    , XJA   ,
     &                        CSTRGA ,
     &                        IMETR  , IORDR  , TEA    , DENSA ,
     &                        STCKM  , STVR   , STVH   ,
     &                        STVRM  , STVHM  , STACK
     &                      )

C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING TOTAL LNE PWR FILE - DSNTOT - IF REQUESTED
C-----------------------------------------------------------------------

      IF (LPTOT) CALL B6WR12( IUNT12 , DATE   , IZ1   , IL    ,
     &                        NDMET  , NDTEM  , NDDEN ,
     &                        LNORM  ,
     &                        NMET   , IMETR  ,
     &                        IFOUT  , MAXT   , TINE  ,
     &                        IDOUT  , MAXD   , DINE  ,
     &                        CSTRGB , PL
     &                      )

C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING SPECIFIC LNE PWR FILE - DSNSPC - IF REQUESTED
C-----------------------------------------------------------------------

      IF (LPSPC) CALL B6WR13( IUNT13 , DATE   , IZ1   , IL    ,
     &                        NDMET  , NDTEM  , NDDEN ,
     &                        LNORM  ,
     &                        NMET   , IMETR  ,
     &                        IFOUT  , MAXT   , TINE  ,
     &                        IDOUT  , MAXD   , DINE  ,
     &                        IE1A(ISEL)      , IE2A(ISEL)    ,
     &                        CSTRGB , PLS
     &                      )
      
      IF(LCONT) CLOSE(UNIT=IUNT11)
      IF(LPTOT) CLOSE(UNIT=IUNT12)
      IF(LPSPC) CLOSE(UNIT=IUNT13)

C-----------------------------------------------------------------------
C OUTPUT ION SPECIFICATIONS & LEVEL INFORMATION TO 'IUNT07' (PAPER.TEXT)
C-----------------------------------------------------------------------

      IF(LPAPER)THEN
         CALL BXOUT0( IUNT07 , DATE   , PRGTYP , DSNINC ,
     &                TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &                ICNTE  , ICNTP  , ICNTR  , ICNTH  ,
     &                IL     ,
     &                IA     , CSTRGA , ISA    , ILA    , XJA    , WA ,
     &                ER     ,
     &                NV     , TSCEF
     &              )

C-----------------------------------------------------------------------
C OUTPUT DATA TO PAPER.TEXT.
C-----------------------------------------------------------------------

         CALL B6OUT1( IUNT07 , IZ1    ,
     &                NDLEV  , NDTEM  , NDDEN , NDMET ,
     &                LNORM  ,
     &                IL     , NMET   , NORD  ,
     &                MAXT   , MAXD   , ZEFF  ,
     &                ICNTP  , ICNTR  , ICNTH  ,
     &                IPROJ  , LLSEL  ,
     &                LPSEL  , LZSEL  , LISEL , LHSEL , LRSEL ,
     &                LMETR  , IMETR  , IORDR ,
     &                IE1A(ISEL)      , IE2A(ISEL)    ,
     &                STRGA  ,
     &                LTRNG  , TEA    , TEVA  , TPVA  , THVA  ,
     &                DENSA  , DENSPA , RATHA , RATIA ,
     &                PLA1   , PL     , PLA   ,
     &                PLAS1  , PLS    , PLAS  ,
     &                PL0    , PLBA   ,
     &                POPAR  ,
     &                STCKM  , STVR   , STVH  ,
     &                STVRM  , STVHM  , STACK
     &              )
      ENDIF

            
      IF (LGPH) THEN
         CALL BXOUTG( LGHOST , DATE  ,
     &                NDLEV  , NDTEM , NDDEN , NDMET  ,
     &                TITLED , TITLE , GTIT1 , DSNINC ,
     &                IZ     , ITSEL , TEVA(ITSEL)    ,
     &                LGRD1  , LDEF1 ,
     &                XL1    , XU1   , YL1   , YU1    ,
     &                IL     , NMET  , NORD  , MAXD   ,
     &                LMETR  , IMETR , IORDR , DENSA  ,
     &                STRGA  , STACK
     &              )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 9999
         
      ENDIF
      
      GOTO 300
      
C-----------------------------------------------------------------------

 1000 format(1x,31('*'),' ADAS206 warning ',30('*')//
     &       1x,'Some levels are above ionisation potential',
     &          ' - ionisation from ALL excited levels switced off')
C-----------------------------------------------------------------------

 9999 STOP

      END
