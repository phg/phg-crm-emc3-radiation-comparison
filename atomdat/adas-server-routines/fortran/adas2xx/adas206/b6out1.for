CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6out1.for,v 1.2 2004/07/06 11:25:48 whitefor Exp $ Date $Date: 2004/07/06 11:25:48 $
CX
      SUBROUTINE B6OUT1( IUNIT  , IZ1    ,
     &                   NDLEV  , NDTEM  , NDDEN , NDMET  ,
     &                   LNORM  ,
     &                   IL     , NMET   , NORD  ,
     &                   MAXT   , MAXD   , ZEFF  ,
     &                   ICNTP  , ICNTR  , ICNTH ,
     &                   IPROJ  , LLSEL  ,
     &                   LPSEL  , LZSEL  , LISEL , LHSEL  , LRSEL ,
     &                   LMETR  , IMETR  , IORDR ,
     &                   ILOWER , IUPPER ,
     &                   STRGA  ,
     &                   LTRNG  , TEA    , TEVA  , TPVA   , THVA   ,
     &                   DENSA  , DENSPA , RATHA , RATIA  ,
     &                   PLA1   , PL     , PLA   ,
     &                   PLAS1  , PLS    , PLAS  ,
     &                   PL0    , PLBA   ,
     &                   POPAR  ,
     &                   STCKM  , STVR   , STVH  ,
     &                   STVRM  , STVHM  , STACK
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B6OUT1 *******************
C
C  PURPOSE: OUTPUT OF MAIN RESULTS (LINE POWER)
C
C  CALLING PROGRAM: ADAS206
C
C  DATA:
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C           NEUTRAL BEAM ENERGY :
C           RATE COEFFICIENTS   : CM3 SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT FOR RESULTS
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT :  (L*4) LNORM   = .TRUE.  => IF NMET=1 THEN TOTAL AND SPECIFIC
C                                      LINE POWER OUTPUT FILES PLT/PLS
C                                      NORMALISED TO STAGE TOT.POPULATN.
C                                      (** NORM TYPE = T)
C                         = .FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                      METASTABLE POPULATIONS.
C                                      (** NORM TYPE = M)
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 20)
C  INPUT : (R*8)  ZEFF    = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C
C  INPUT : (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C  INPUT : (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT : (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT : (I*4)  IPROJ   = SPECIFIES INDEX OF LOWEST  LEVEL FOR WHICH
C                           EXTRAPOLATION TO HIGHER N  OF THE RADIATED
C                           POWER IS TO BE PERFORMED. ALL LEVELS ABOVE
C                           AND INCLUDING 'IPROJ' ARE TREATED.
C                           IF 'IPROJ' > 'IL' => EXTRAP'TN SWITCHED OFF
C  INPUT : (L*4)  LLSEL   = .TRUE.  => OUTPUT LINE POWER RATIOS FORMED
C                                      ACCORDING TO POWER  FOR  LOWEST
C                                      FIRST OF THE INPUT DENSITIES.
C                         = .FALSE. => OUTPUT LINE POWER RATIOS FORMED
C                                      ACCORDING TO ZERO DENSITY POWER
C
C  INPUT : (L*4)  LPSEL   = .TRUE.  => INCLUDE PROTON COLLISIONS
C                         = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  INPUT : (L*4)  LZSEL   = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                      PLASMA Z EFFECTIVE'ZEFF'.
C                         = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                      WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                         (ONLY USED IF 'LPSEL=.TRUE.')
C  INPUT : (L*4)  LISEL   = .TRUE.  => INCLUDE IONISATION RATES
C                         = .FALSE. => DO NOT INCLUDE IONISATION RATES
C  INPUT : (L*4)  LHSEL   = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                      NEUTRAL HYDROGREN.
C                         = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                      FROM NEUTRAL HYDROGREN.
C  INPUT : (L*4)  LRSEL   = .TRUE.  => INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C
C  INPUT : (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                      'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C  INPUT : (I*4)  IORDR() = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                           LIST.
C
C  INPUT : (I*4)  ILOWER  = SPECIFIC LINE POWER: SELECTED ELECTRON
C                           IMPACT TRANSITION LOWER LEVEL INDEX
C  INPUT : (I*4)  IUPPER  = SPECIFIC LINE POWER: SELECTED ELECTRON
C                           IMPACT TRANSITION UPPER LEVEL INDEX
C
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C
C  INPUT : (L*4)  LTRNG(,)= .TRUE.  => TEMPERATURE VALUE WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                         = .FALSE. =>TEMPERATURE VALUE NOT WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                           1st DIMENSION: TEMPERATURE INDEX.
C                           2nd DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (R*8)  TEVA()  = ELECTRON TEMPERATURES (UNITS: EV)
C  INPUT : (R*8)  TPVA()  = PROTON TEMPERATURES   (UNITS: EV)
C  INPUT : (R*8)  THVA()  = NEUTRAL HYDROGEN TEMPERATURES   (UNITS: EV)
C
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT : (R*8)  DENSPA()= PROTON DENSITIES    (UNITS: CM-3)
C  INPUT : (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  INPUT : (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C
C  INPUT : (R*8)  PLA1()  = DIRECT LINE POWER LOSS FROM EACH LEVEL.
C                           (UNITS: ERGS SEC-1) (DIMENSION: LEVEL INDEX)
C  INPUT : (R*8)  PL(,,)  = TOTAL LINE POWERS FOR METASTABLES. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE.
C                            => P(TOTAL)/N(IMET)        (ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C  INPUT : (R*8)  PLA(,)  = TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                            => P(TOTAL)/(DENS*N(1))   (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C
C  INPUT : (R*8)  PLAS1   = DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                           POWER TRANSITION GIVEN BY 'ISTRN'.
C                           (UNITS: ERGS SEC-1)
C  INPUT : (R*8)  PLS(,,) = SPECIFIC LINE POWERS FOR METASTABLES. THIS
C                           IS THE SPECIFIC EMISSION ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE. (SEE 'ISTRN')
C                            => P(SPECIFIC)/N(IMET)  (ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C  INPUT : (R*8)  PLAS(,) = SPECIFIC EQUILIBRIUM LINE PWR COEFFICIENTS.
C                            => P(SPECIFIC)/(DENS*N(1)) (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C
C  INPUT : (R*8)  PL0()   = ZERO DENSITY LINE POWER ARISING FROM EXCI-
C                           TATION ONLY FROM THE GROUND LEVEL.
C                           (UNITS: ERGS CM3 SEC-1). (DIMENSION: TEMP.)
C                             => P/(DENS*N(1))
C  INPUT : (R*8)  PLBA(,) = HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                           FROM A PARTICULAR METASTABLE TO LEVELS
C                           'IPROJ' UPWARDS. (UNITS: ERGS CM3 SEC-1)
C                             => P/(DENS*N(IMET))
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C
C  INPUT : (R*8)  POPAR(,,) =  LEVEL POPULATIONS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C  INPUT : (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVR(,,)  = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVH(,,)  =  CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C  INPUT : (R*8) STVRM(,,) = METASTABLE FREE ELECTRON RECOMBINATION
C                            COEFFICIENTS.
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVHM(,,) = METASTABLE CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*4) STACK(,,,)= POPULATION DEPENDENCE
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C
C          (I*4)  PGLEN   = PARAMETER = NUMBER OF LINES PER OUTPUT PAGE
C
C          (I*4)  NBLOCK  = NUMBER OF LINES IN CURRENT OUTPUT BLOCK.
C          (I*4)  NLINES  = LAST PAGE LINE WRITTEN.
C                           IF 'NLINES+NBLOCK' > 'PGLEN' START NEW PAGE.
C          (I*4)  MIND    = MINIMUM OF 10 AND 'MAXD'
C          (I*4)  I       = GENERAL USE
C          (I*4)  J       = GENERAL USE
C          (I*4)  IT      = TEMPERATURE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IN      = DENSITY INDEX NUMBER FOR ARRAY USE
C          (I*4)  IUSEP   = NUMBER OF PROTON IMPACT TRANSITIONS USED
C          (I*4)  IUSER   = NUMBER OF FREE ELECTRON RECOMBINATIONS USED
C          (I*4)  IUSEH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS USED
C          (I*4)  ITMID   = MID-TEMPERATURE INDEX = 0.5 * 'MAXT'
C
C          (R*8)  Z1R2    = 1.0/('IZ1' SQUARED)
C          (R*8)  Z1R7    = 1.0/('IZ1' TO THE POWER SEVEN)
C          (R*8)  THETA   = REDUCED TEMPERATURE VALUE
C          (R*8)  PLVAL   = LINE POWER COEFFTS (ZERO/LOWEST DENSITY)
C          (R*8)  PLRAT   = LINE POWER RATIO
C
C          (L*4)  LPRNG   = .TRUE.  => PROTON INPUT PARAMETERS USED
C                           .FALSE. => PROTON INPUT PARAMETERS NOT USED
C          (L*4)  LHRNG   = .TRUE.  => NEUTRAL H INPUT PARAMETERS USED
C                           .FALSE. => NEUTRAL H INPUT PARMS. NOT USED
C          (L*4)  LRRNG   = .TRUE.  => FREE ELEC. RECOMB. PARMS USED
C                           .FALSE. => FREE ELEC. RECOMB. PARMS NOT USED
C
C          (C*2)  CZ1     = 'IZ1' IN CHARACTER FORM WHEN 'IT'='ITMID'
C                           OTHERWISE IS BLANK.
C          (C*6)  CRATMX  = PARAMETER = ' > 100' (MAX. OUTPUT RATIO)
C          (C*32) C32     = GENERAL USE 32 BYTE CHARACTER STRING
C          (C*44) CLSEL   = IF (LLSEL=.TRUE.) =
C                         'LINE POWER FOR LOWEST OF THE INPUT DENSITIES'
C                           IF (LLSEL=.FALSE.) =
C                         'ZERO DENSITY LINE POWER                     '
C
C          (C*1)  CTRNG(6)= ' ' => OUTPUT VALUES FOR THIS TEMPERATURE
C                                  INTERPOLATED.
C                         = '*' => OUTPUT VALUES FOR THIS TEMPERATURE
C                                  EXTRAPOLATED.
C                         = '#' => NOT USED
C                           1st DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C                                          DENSITY TYPE -
C                                          4) => PROTON
C                                          RATIO TYPE -
C                                          5) => 'RATHA'
C                                          6) => 'RATIA'
C          (C*6)  CROUT() = OUTPUT RATIO AS CHARACTER*6 AT A GIVEN TEMP-
C                           PERATURE.
C                           DIMENSION: DENSITY INDEX
C                           (IF RATIO > 99.999 => 'CROUT' = 'CRATMX')
C
C
C NOTE:
C          ONLY THE FIRST TEN DENSITIES ARE OUTPUT.
C
C          AN 'OUTPUT BLOCK' IS A SINGLE CONTAINED OUTPUT TABLE
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSTNP     ADAS      STARTS NEW PAGE IF CURRENT PAGE FULL
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    09/10/90
C
C UPDATE:  21/11/90 - PE BRIDEN - CODING ADDED TO REMOVE THE POSSIBILITY
C                                 OF DIVISION BY ZERO  WHEN  CALCULATING
C                                 LINE POWER RATIOS. ALSO RATIOS GREATER
C                                 THAN 100 ARE LISTED AS BEING ' > 100'.
C
C                                 WHEN RATIOS ARE REQUIRED THEY ARE CAL-
C                                 CULATED AS 'X/Y' INSTEAD OF 'X*(1/Y)'.
C                                 THIS IS BECAUSE IN VS FORTRAN ALTHOUGH
C                                 'Y' MAY NOT CREATE AN UNDERFLOW  ERROR
C                                 '1/Y'  CAN  STILL  CREATE AN  OVERFLOW
C                                 ERROR (E.G. Y = 1.0D-77)
C
C                                 IN THIS PROGRAM 'X/Y' SHOULD NEVER BE
C                                 SUCH THAT AN OVERFLOW ERROR OCCURS.
C
C UPDATE:  17/01/91 - PE BRIDEN: RENAMED SUBROUTINE (ORIGINALLY B6WR7B)
C                              - STARTED OUTPUT ON NEW PAGE
C
C UPDATE:  31/01/91 - PE BRIDEN: REFORMATTED OUTPUT. REMOVED 'SCEF' AND
C                                'NV' FROM ARGUMENT LIST ETC.
C
C UPDATE:  25/03/91 - PE BRIDEN: REFORMATTED OUTPUT (STATEMENTS 1012/3)
C
C UPDATE:  07/08/91 - PE BRIDEN: CHANGED 'DBLE(IZ1**7)' TO 'DBLE(IZ1)**7
C                                TO AVOID INTEGER OVERFLOW IF IZ1>21.
C
C UPDATE:  18/05/93 - PE BRIDEN: SPECIFY NORMALISATION ON OUTPUT.
C                                NEW ARGUMENT    - LNORM
C                                CHANGED FORMATS - 1107, 1114
C
C UPDATE:  20/05/93- P BRIDEN: STACK ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 29/04/2003
C MODIFIED:	Martin O'Mullane
C		- Increased format 1022 from I2 to I4 to allow for 
c   		  more levels.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   PGLEN
C-----------------------------------------------------------------------
      CHARACTER CRATMX*6
C-----------------------------------------------------------------------
      PARAMETER ( PGLEN  = 63       )
C-----------------------------------------------------------------------
      PARAMETER ( CRATMX = ' > 100' )
C-----------------------------------------------------------------------
      INTEGER   IUNIT     , IZ1       ,
     &          NDLEV     , NDTEM     , NDDEN     , NDMET
      INTEGER   IL        ,
     &          NMET      , NORD      ,
     &          MAXT      , MAXD      ,
     &          ICNTP     , ICNTR     , ICNTH
      INTEGER   ILOWER    , IUPPER    ,
     &          IPROJ
      INTEGER   NBLOCK    , NLINES    , MIND      ,
     &          I         , J         , IN        , IT      ,
     &          IUSEP     , IUSER     , IUSEH
      INTEGER   ITMID
C-----------------------------------------------------------------------
      REAL*8    ZEFF      , PLAS1
      REAL*8    Z1R2      , Z1R7      , THETA     , PLVAL   , PLRAT
C-----------------------------------------------------------------------
      LOGICAL   LLSEL     , LNORM
      LOGICAL   LPSEL     , LZSEL     , LISEL     , LHSEL   , LRSEL
      LOGICAL   LPRNG     , LHRNG     , LRRNG
C-----------------------------------------------------------------------
      CHARACTER CZ1*2     , C32*32    , CLSEL*44
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)    , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8    TEVA(NDTEM)     , TPVA(NDTEM)      ,
     &          THVA(NDTEM)     , TEA(NDTEM)       ,
     &          DENSA(NDDEN)    , DENSPA(NDDEN)    ,
     &          RATHA(NDDEN)    , RATIA(NDDEN)
      REAL*8    PLA1(NDLEV)     , PL0(NDTEM)        ,
     &          PLA(NDTEM,NDDEN), PLAS(NDTEM,NDDEN) ,
     &          PLBA(NDMET,NDTEM)
      REAL*8    PL(NDMET,NDTEM,NDDEN)         ,
     &          PLS(NDMET,NDTEM,NDDEN)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
      REAL*8    STVR(NDLEV,NDTEM,NDDEN)       , STVH(NDLEV,NDTEM,NDDEN)
      REAL*8    STCKM(NDMET,NDTEM,NDDEN)      ,
     &          STVRM(NDMET,NDTEM,NDDEN)      , STVHM(NDMET,NDTEM,NDDEN)
      REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NDMET)    , LTRNG(NDTEM,3)
C-----------------------------------------------------------------------
      CHARACTER CTRNG(6)*1      , STRGA(NDLEV)*22
      CHARACTER CROUT(10)*6
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      MIND=MIN0(10,MAXD)
      Z1R2=1.0/DBLE(IZ1*IZ1)
      Z1R7=1.0/DBLE(IZ1)**7
C-----------------------------------------------------------------------
      WRITE(IUNIT,1000) NORD  , NMET
C-----------------------------------------------------------------------
         DO 1 I=1,NMET
            IF ( LMETR(I) ) THEN
               WRITE(IUNIT,1001) I , IMETR(I) , STRGA(IMETR(I))
            ELSE
               WRITE(IUNIT,1002) I , IMETR(I) , STRGA(IMETR(I))
            ENDIF
    1    CONTINUE
C-----------------------------------------------------------------------
      WRITE(IUNIT,1100) IPROJ
C-----------------------------------------------------------------------
      WRITE(IUNIT,1101) IUPPER        , ILOWER        ,
     &                  STRGA(IUPPER) , STRGA(ILOWER) ,
     &                  PLAS1
C
C-----------------------------------------------------------------------
C OUTPUT TEMPERATURES, DENSITIES AND RATIOS
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1003) MAXT , MAXD
      WRITE(IUNIT,1004)
      WRITE(IUNIT,1005)
C
      CTRNG(2) = '#'
      CTRNG(3) = '#'
      CTRNG(4) = '#'
      CTRNG(5) = '#'
      CTRNG(6) = '#'
C
      LPRNG = LPSEL .AND. (ICNTP.GT.0)
      LHRNG = LHSEL .AND. (ICNTH.GT.0)
      LRRNG = LRSEL .AND. (ICNTR.GT.0)
C
      IF (LPRNG)           CTRNG(4) = ' '
      IF (LHRNG)           CTRNG(5) = ' '
      IF (LHRNG.OR.LRRNG)  CTRNG(6) = ' '
C
         DO 2 IT=1,MAXT
C
               IF (LTRNG(IT,1)) THEN
                  CTRNG(1) = ' '
               ELSE
                  CTRNG(1) = '*'
               ENDIF
C
               IF (LPRNG) THEN
                  IF (LTRNG(IT,2)) THEN
                     CTRNG(2) = ' '
                  ELSE
                     CTRNG(2) = '*'
                  ENDIF
               ENDIF
C
               IF (LHRNG) THEN
                  IF (LTRNG(IT,3)) THEN
                     CTRNG(3) = ' '
                  ELSE
                     CTRNG(3) = '*'
                  ENDIF
               ENDIF
C
               IF (IT.LE.MAXD) THEN
                  WRITE(IUNIT,1006) IT , TEVA(IT)   , CTRNG(1)   ,
     &                                   TPVA(IT)   , CTRNG(2)   ,
     &                                   THVA(IT)   , CTRNG(3)   ,
     &                              IT , DENSA(IT)  ,
     &                                   DENSPA(IT) , CTRNG(4)   ,
     &                                   RATHA(IT)  , CTRNG(5)   ,
     &                                   RATIA(IT)  , CTRNG(6)
               ELSE
                  WRITE(IUNIT,1007) IT , TEVA(IT)  , CTRNG(1)   ,
     &                                   TPVA(IT)  , CTRNG(2)   ,
     &                                   THVA(IT)  , CTRNG(3)
               ENDIF
    2    CONTINUE
C
         IF (MAXD.GT.MAXT) THEN
            DO 3 IN=MAXT+1,MAXD
                  WRITE(IUNIT,1008) IN , DENSA(IN)  ,
     &                                   DENSPA(IN) , CTRNG(4) ,
     &                                   RATHA(IN)  , CTRNG(5) ,
     &                                   RATIA(IN)  , CTRNG(6)
    3       CONTINUE
         ENDIF
C
      WRITE(IUNIT,1005)
      WRITE(IUNIT,1009)
      WRITE(IUNIT,1005)
      WRITE(IUNIT,'( / / )')
C
C-----------------------------------------------------------------------
C OUTPUT SELECTED OPTIONS
C-----------------------------------------------------------------------
C
      C32 = 'PROTON IMPACT COLLISIONS'
         IF (LPSEL) THEN
            IUSEP = ICNTP
               IF (LZSEL) THEN
                  WRITE(IUNIT,1012) C32 , ZEFF
               ELSE
                  WRITE(IUNIT,1013) C32
               ENDIF
         ELSE
            IUSEP = 0
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'IONIZATION RATES'
         IF (LISEL) THEN
            WRITE(IUNIT,1010) C32
         ELSE
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'NEUTRAL HYDROGEN CHARGE EXCHANGE'
         IF (LHSEL) THEN
            IUSEH = ICNTH
            WRITE(IUNIT,1010) C32
         ELSE
            IUSEH = 0
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'FREE ELECTRON RECOMBINATION'
         IF (LRSEL) THEN
            IUSER = ICNTR
            WRITE(IUNIT,1010) C32
         ELSE
            IUSER = 0
            WRITE(IUNIT,1011) C32
         ENDIF
C
         IF (LLSEL) THEN
            CLSEL = 'LINE POWER FOR LOWEST OF THE INPUT DENSITIES'
         ELSE
            CLSEL = 'ZERO DENSITY LINE POWER'
         ENDIF
      WRITE(IUNIT,1102) CLSEL
C
C-----------------------------------------------------------------------
C OUTPUT TABLE KEY:
C-----------------------------------------------------------------------
C
         IF (IPROJ.GT.IL) THEN
            C32 = '(ergs cm3 sec-1)'
         ELSE
            C32 = '--- NOT USED ---'
         ENDIF
      WRITE(IUNIT,1014) IUSEP , IUSER     , IUSEH
      WRITE(IUNIT,1103) IPROJ , C32(1:16)
      IF (MAXD.GT.10) WRITE(IUNIT,1015)
C-----------------------------------------------------------------------
         DO 4 IT=1,MAXT
            WRITE(IUNIT,1016) ( DENSA(I) , I=1,MIND )
            WRITE(IUNIT,1017) TEVA(IT), TEA(IT), PL0(IT)
               DO 5 I=1,NMET
                  WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                      (STCKM(I,IT,IN),IN=1,MIND)
    5          CONTINUE
            NLINES=5+NMET
C-----------------------------------------------------------------------
C  WRITE OUT RECOMBINATION AND CHARGE EXCHANGE DATA (IF ANY)
C-----------------------------------------------------------------------
               IF (NMET.GT.1) THEN
                  IF ( LRSEL .AND. (ICNTR.GT.0) ) THEN
                     NBLOCK=NMET+1
                     CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                     WRITE(IUNIT,1019)
     &                         'METASTABLE RECOMBINATION COEFFICIENTS'
                        DO 6 I=2,NMET
                           WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                      (STVRM(I,IT,IN),IN=1,MIND)
    6                   CONTINUE
                  ENDIF
                  IF ( LHSEL .AND. (ICNTH.GT.0) ) THEN
                     NBLOCK=NMET+1
                     CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                     WRITE(IUNIT,1019)
     &                       'METASTABLE CHARGE EXCHANGE COEFFICIENTS'
                        DO 7 I=2,NMET
                           WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                      (STVHM(I,IT,IN),IN=1,MIND)
    7                   CONTINUE
                  ENDIF
               ENDIF
C-----------------------------------------------------------------------
            NBLOCK=1
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,'()')
               DO 8 I=1,NMET
                  NBLOCK=NORD+7
                  CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                  WRITE(IUNIT,1020) I , PLA1(IMETR(I)) , PLBA(I,IT)
                     DO 9  J=1,NORD
                        WRITE(IUNIT,1018) J , IORDR(J) ,
     &                                    (STACK(J,I,IT,IN),IN=1,MIND)
    9                CONTINUE
                  WRITE(IUNIT,1104) ( PL(I,IT,IN)  , IN=1,MIND )
                  WRITE(IUNIT,1105) ( PLS(I,IT,IN) , IN=1,MIND )
    8          CONTINUE
C-----------------------------------------------------------------------
C  WRITE OUT RECOMBINATION AND CHARGE EXCHANGE DATA IF ANY
C-----------------------------------------------------------------------
               IF ( LRSEL .AND. (ICNTR.GT.0) ) THEN
                  NBLOCK=NORD+2
                  CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                  WRITE(IUNIT,1019) 'RECOMBINATION COEFFICIENTS'
                     DO 10 I=1,NORD
                        WRITE(IUNIT,1018) I , IORDR(I) ,
     &                                       (STVR(I,IT,IN),IN=1,MIND)
   10                CONTINUE
               ENDIF
               IF ( LHSEL .AND. (ICNTH.GT.0) ) THEN
                  NBLOCK=NORD+2
                  CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                  WRITE(IUNIT,1019) 'CHARGE EXCHANGE COEFFICIENTS'
                     DO 11 I=1,NORD
                        WRITE(IUNIT,1018) I , IORDR(I) ,
     &                                       (STVH(I,IT,IN),IN=1,MIND)
   11                CONTINUE
               ENDIF
    4    CONTINUE
C-----------------------------------------------------------------------
C  WRITE OUT THE TOTAL-LINE-POWER DATA.
C-----------------------------------------------------------------------
      WRITE(IUNIT,1106)
      NLINES=1
      NBLOCK=MAXT+7
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
C
         IF ( LNORM .AND. (NMET.EQ.1) ) THEN
            WRITE(IUNIT,1107) 'T'
         ELSE
            WRITE(IUNIT,1107) 'M'
         ENDIF
C
      WRITE(IUNIT,1023) ( DENSA(IN) , IN=1,MIND )
      WRITE(IUNIT,1024)
         DO 12 IT=1,MAXT
            WRITE(IUNIT,1025) TEVA(IT) , ( PLA(IT,IN), IN=1,MIND )
   12    CONTINUE
C
      NBLOCK=MAXT+7
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
      ITMID=INT( 0.5*( REAL(MAXT)+1.001 ) )
      WRITE(IUNIT,1108) CLSEL , ( DENSA(IN)*Z1R7 , IN=1,MIND )
         DO 13 IT=1,MAXT
               IF (LLSEL) THEN
                  PLVAL=PLA(IT,1)
               ELSE
                  PLVAL=PL0(IT)
               ENDIF
            THETA=TEA(IT)*Z1R2
            CZ1=' '
            IF (IT.EQ.ITMID) WRITE(CZ1,'(I2)') IZ1
               IF (PLVAL.EQ.0.0D0) THEN
                  WRITE(IUNIT,1109) CZ1 , THETA , PLVAL
               ELSE
                     DO 14 IN=1,MIND
                        PLRAT=PLA(IT,IN)/PLVAL
                           IF (PLRAT.GT.99.999) THEN
                              WRITE(CROUT(IN),1110) CRATMX
                           ELSE
                              WRITE(CROUT(IN),1111) PLRAT
                           ENDIF
   14                CONTINUE
                  WRITE(IUNIT,1112) CZ1 , THETA , PLVAL ,
     &                              ( CROUT(IN), IN=1,MIND )
               ENDIF
   13    CONTINUE
C-----------------------------------------------------------------------
C  WRITE OUT THE SPECIFIC-LINE-POWER DATA.
C-----------------------------------------------------------------------
      WRITE(IUNIT,1113)
      NLINES=1
      NBLOCK=MAXT+7
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
C
         IF ( LNORM .AND. (NMET.EQ.1) ) THEN
            WRITE(IUNIT,1114) 'T'
         ELSE
            WRITE(IUNIT,1114) 'M'
         ENDIF
C
      WRITE(IUNIT,1023) ( DENSA(IN) , IN=1,MIND )
      WRITE(IUNIT,1024)
         DO 15 IT=1,MAXT
            WRITE(IUNIT,1025) TEVA(IT) , ( PLAS(IT,IN), IN=1,MIND )
   15    CONTINUE
C
      NBLOCK=MAXT+7
      CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
      WRITE(IUNIT,1115) ( DENSA(IN)*Z1R7 , IN=1,MIND )
         DO 16 IT=1,MAXT
            PLVAL=PLAS(IT,1)
            THETA=TEA(IT)*Z1R2
            CZ1=' '
            IF (IT.EQ.ITMID) WRITE(CZ1,'(I2)') IZ1
               IF (PLVAL.EQ.0.0D0) THEN
                  WRITE(IUNIT,1109) CZ1 , THETA , PLVAL
               ELSE
                     DO 17 IN=1,MIND
                        PLRAT=PLAS(IT,IN)/PLVAL
                           IF (PLRAT.GT.99.999) THEN
                              WRITE(CROUT(IN),1110) CRATMX
                           ELSE
                              WRITE(CROUT(IN),1111) PLRAT
                           ENDIF
   17                CONTINUE
                  WRITE(IUNIT,1112) CZ1 , THETA , PLVAL ,
     &                              ( CROUT(IN), IN=1,MIND )
               ENDIF
   16    CONTINUE
C-----------------------------------------------------------------------
C  WRITE OUT THE POPULATIONS.
C-----------------------------------------------------------------------
      WRITE(IUNIT,1021)
      NLINES=1
         DO 18 I=1,IL
            NBLOCK=MAXT+7
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1022) I
            WRITE(IUNIT,1023) ( DENSA(IN) , IN=1,MIND )
            WRITE(IUNIT,1024)
               DO 19 IT=1,MAXT
                  WRITE(IUNIT,1025) TEVA(IT),(POPAR(I,IT,IN), IN=1,MIND)
   19          CONTINUE
   18    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( / / / / / / ,'METASTABLE INFORMATION:'/1H ,23('-')/
     &        / / ,'NUMBER OF ORDINARY LEVELS',8X,'=',1X,I3/
     &       1H ,'NUMBER OF METASTABLES'   ,12X,'=',2X,I2/
     &        / / ,16('-'),' METASTABLE DETAILS ',16('-')/
     &       1H ,'METASTABLE',4X,'ENERGY LEVEL',4X,
     &           4('-'),' DESIGNATION ',5('-')/
     &        3X,'INDEX',10X,'INDEX'/1X,52('-'))
 1001 FORMAT(1H ,3X,I2,12X,I3,10X,1A22)
 1002 FORMAT(1H ,I4,7X,I4,12X,1A22,4X,
     & '***** NO ELECTRON IMPACT TRANSITIONS EXIST TO THIS LEVEL *****')
 1003 FORMAT( / / / / / ,'OUTPUT PLASMA PARAMETERS:'/1H ,25('-')//
     &       1H ,'NUMBER OF TEMPERATURES     = ',I2/
     &       1H ,'NUMBER OF DENSITIES/RATIOS = ',I2)
 1004 FORMAT(/ /,'INDEX',3X,9('-'),' TEMPERATURES (UNITS: eV) ',9('-'),
     &       10X,'INDEX',4X,'DENSITIES (UNITS: cm-3)',6X,9('-'),
     &           ' RATIOS ',9('-')/
     &       11X,'ELECTRON',8X,'PROTON',4X,'NEUTRAL HYDROGEN',20X,
     &           'ELECTRON',7X,'PROTON',11X,'NH/NE',6X,'N(Z+1)/N(Z)'/
     &       13X,'<TE>',11X,'<TP>',11X,'<TH>',28X,'<NE>',10X,'<NP>')
 1005 FORMAT(1H ,52('-'),10X,64('-'))
 1006 FORMAT( 2X,I2,5X,1P,3(D11.3,1X,A1,2X),
     &       10X,I2,2X,   2(3X,D11.3),1X,A1,3X,2(D11.3,1X,A1,1X))
 1007 FORMAT( 2X,I2,5X,1P,3(D11.3,1X,A1,2X))
 1008 FORMAT(64X,I2,2X,1P,2(3X,D11.3),1X,A1,3X,2(D11.3,1X,A1,1X))
 1009 FORMAT(1H ,'KEY: * = WARNING - TEMPERATURE OUT OF RANGE',
     &       19X,'NOTE: NH/NE',7X,
     &           '= NEUTRAL HYDROGEN DENSITY / ELECTRON DENSITY'/
     &       1H ,17X,'- EXTRAPOLATION REQUIRED',
     &       27X,'N(Z+1)/N(Z) = STAGE ABUNDANCIES'/
     &       1H ,5X,'# = PARAMETER NOT USED IN CALCULATIONS')
 1010 FORMAT(1H ,A32,1X,'- INCLUDED    ')
 1011 FORMAT(1H ,A32,1X,'- NOT INCLUDED')
 1012 FORMAT(1H ,A32,1X,'- INCLUDED    ',
     &                 ' - SCALED WITH PLASMA Z-EFFECTIVE (=',F5.1,')')
 1013 FORMAT(1H ,A32,1X,'- INCLUDED    ',
     &                 ' - NOT SCALED WITH PLASMA Z-EFFECTIVE')
 1014 FORMAT( / / / / / / ,'TABLE KEY:'/1H ,10('-')/
     &     / / ,'NE    = ELECTRON DENSITY'/
     &    1H ,'TE    = ELECTRON TEMPERATURE'/
     &     / / ,'I     = ENERGY   LEVEL INDEX'/
     &    1H ,'IMET  = METASTABLE     INDEX'/
     &    1H ,'IORD  = ORDINARY LEVEL INDEX'/ / / /
     &    / /,'NUMBER OF PROTON IMPACT COLLISIONS       INCLUDED =',I5/
     &    1H ,'NUMBER OF FREE   ELECTRON RECOMBINATIONS INCLUDED =',I5/
     &    1H ,'NUMBER OF CHARGE EXCHANGE RECOMBINATIONS INCLUDED =',I5/)
 1015 FORMAT(/ / ,'NOTE: OUTPUT RESTRICTED TO FIRST TEN DENSITIES ONLY')
 1016 FORMAT(/ / / ,'NE(CM-3)=',1P,10E12.4)
 1017 FORMAT(1H ,132('-')/
     &        / / ,'EQUILIBRIUM METASTABLE POPULATION DEPENDENCE ON ',
     &       'DENSITY AT TE =' ,1P,E10.2,' EV  = ',E10.2,' KELVIN'/
     &       5X,'PL0 =',1PD12.4/1H ,2X,'IMET',2X,'I')
 1018 FORMAT(2I5,1P,10E12.4)
 1019 FORMAT( / / ,A)
 1020 FORMAT( / / ,'POPULATION AND POWER DEPENDENCE ON DENSITY AND ',
     &       'METASTABLE',I3/1H ,'PLA1 =',1PD12.4,5X,'PLBA =',1PD12.4/
     &       1H ,'  IORD  I')
 1021 FORMAT( / / / ,53('*'),' EQUILIBRIUM  POPULATIONS ',53('*'))
 1022 FORMAT(///' LEVEL = ',I4,' - EQUILIBRIUM POPULATION',/1X,37('-'))
 1023 FORMAT( / / ,'NE (CM-3) ',3X,1P,10D10.2)
 1024 FORMAT(1H ,'TE (EV)',4X,103('-'))
 1025 FORMAT(1H ,1P,D10.2,' | ',1P,10D10.2)
C
 1100 FORMAT( / / / / / ,'LOWER INDEX BOUND FOR PROJECTION = ',I3)
 1101 FORMAT( / / ,'SPECIFIC LINE POWER - SELECTED ELECTRON IMPACT ',
     &           'TRANSITION (',I2.2,'-',I2.2,'): ',A22,' - ',A22/
     &        / / ,'SPECIFIC LINE POWER - DIRECT LINE POWER LOSS = ',
     &           1PD12.4,' ergs sec-1'/)
 1102 FORMAT( / / ,' TABULATED TOTAL EQUILIBRIUM LINE POWER RATIOS ',
     &           ' REPRESENT THE RATIO TO THE ',A)
 1103 FORMAT( / / / / / ,'LINE POWER PARAMETERS:'/
     & / / ,'THETA = REDUCED TEMPERATURES (kelvin/Z1**2)'/
     &1H ,'RHO   = REDUCED DENSITIES    (cm-3/Z1**7)'/
     &1H ,'Z1    = RECOMBINING ION CHARGE'/
     & //,'PL    = LINE POWER  COEFFICIENT  USED  AS  DENOMINATOR  FOR'/
     &1H ,8X,     'CALCULATING LINE POWER RATIOS (ergs cm3 sec-1)'/
     &1H ,'PL0   = ZERO DENSITY LINE POWER  ARISING  FROM  EXCITATIONS'/
     &1H ,8X,     'ONLY FROM THE GROUND LEVEL (ergs cm3 sec-1)'/
     &1H ,'PLA1  = DIRECT LINE POWER LOSS FROM METASTABLE (ergs sec-1)'/
     &1H ,'PLBA  = HIGH N PROJECTED POWER  BASED ON  EXCITATIONS  FROM'/
     &1H ,8X,     'METASTABLE TO LEVELS ',I3,' UPWARDS ',A16)
 1104 FORMAT(1H ,'-LINE PWR-'/1H ,'TOTAL   ',1X,1P,10D12.4)
 1105 FORMAT(1H ,'SPECIFIC',1X,1P,10D12.4)
 1106 FORMAT( / / / ,42('*'),
     &       ' TOTAL EQUILIBRIUM LINE POWER COEFFICIENT/RATIO ',42('*'))
 1107 FORMAT( / / /1X,
     &       'TOTAL EQUILIBRIUM LINE POWER COEFFICIENT',4X,
     &       'NORMALISATION = ',A1/1X,61('-'))
 1108 FORMAT( / / /1X,'TOTAL EQUILIBRIUM LINE POWER RATIO TABLE',
     &           ' WITH REDUCED PARAMETERS - RATIO TO THE ',A/
     &       1H ,124('-')/
     &        / / ,3X,'Z1',3X,'THETA',7X,'PL',4X,'RHO=',1P,10D10.2/
     &           1X,130('-'))
 1109 FORMAT(1H ,3X,A2,1P,2D10.2,4X,
     &     '--- PL = 0 => DIVISION BY ZERO: NO RATIOS CALCULATED ---')
 1110 FORMAT(A6)
 1111 FORMAT(F6.3)
 1112 FORMAT(1H ,3X,A2,1P,2D10.2,3X,10(4X,A6))
 1113 FORMAT( / / / ,40('*'),
     &    ' SPECIFIC EQUILIBRIUM LINE POWER COEFFICIENT/RATIO ',41('*'))
 1114 FORMAT( / / /1X,
     &       'SPECIFIC EQUILIBRIUM LINE POWER COEFFICIENT',4X,
     &       'NORMALISATION = ',A1/1X,64('-'))
 1115 FORMAT( / / /1H ,'SPECIFIC EQUILIBRIUM LINE POWER RATIO TABLE',
     &               ' WITH REDUCED PARAMETERS - RATIO TO THE LINE',
     &               ' POWER FOR LOWEST OF THE INPUT DENSITIES'/
     &       1H ,127('-')/
     &        / / ,3X,'Z1',3X,'THETA',7X,'PL',4X,'RHO=',1P,10D10.2/
     &       1H ,130('-'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
