CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6ispf.for,v 1.1 2004/07/06 11:25:30 whitefor Exp $ Date $Date: 2004/07/06 11:25:30 $
CX
      SUBROUTINE B6ISPF( LPEND  ,
     &                   NDTEM , NDDEN  , NDMET , IL    ,
     &                   NV    , TSCEF  ,
     &                   ICNTE , IE1A   , IE2A  , STRGA ,
     &                   TITLE , IPROJ  , NMET  , IMETR ,
     &                   ISTRN ,
     &                   IFOUT ,
     &                   MAXT  , TINE   , TINP  , TINH  ,
     &                   IDOUT ,
     &                   MAXD  , DINE   , DINP  , RATHA , RATIA ,
     &                   ZEFF  ,
     &                   LLSEL , LNORM  , LPSEL , LZSEL , LISEL , LHSEL,
     &                   LRSEL
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6ISPF *********************
C
C  PURPOSE: ISPF PANEL INPUT SUBROUTINE FOR 'ADAS206'
C
C  CALLING PROGRAM: ADAS206
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTEM    = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDDEN    = MAX. NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)   NDMET    = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER OF TEMPERATURES
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  INPUT : (I*4)   ICNTE    = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT : (I*4)   IE1A()   = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)   IE2A()   = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C  INPUT : (C*22)  STRGA()  = LEVEL DESIGNATIONS
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (I*4)   IPROJ    = INDEX OF LOWER LEVEL BOUND FOR N EXTRAP'LN
C  OUTPUT: (I*4)   NMET     = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  OUTPUT: (I*4)   IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C
C  OUTPUT: (I*4)   ISTRN    = SELECTED ELECTRON IMPACT TRANSITION INDEX
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINP()   = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINH()   = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C
C  OUTPUT: (I*4)   IDOUT    = 1 => INPUT DENSITIES IN CM-3
C                           = 2 => INPUT DENSITIES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 20)
C  OUTPUT: (R*8)   DINE()   = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   DINP()   = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   RATHA()  = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  OUTPUT: (R*8)   RATIA()  = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C
C  OUTPUT: (R*8)   ZEFF     = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C
C  OUTPUT: (L*4)   LLSEL    = .TRUE.  => OUTPUT LINE POWER RATIOS FORMED
C                                        ACCORDING TO POWER  FOR  LOWEST
C                                        FIRST OF THE INPUT DENSITIES.
C                           = .FALSE. => OUTPUT LINE POWER RATIOS FORMED
C                                        ACCORDING TO ZERO DENSITY POWER
C  OUTPUT: (L*4)   LPSEL    = .TRUE.  => INCLUDE PROTON COLLISIONS
C                           = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  OUTPUT: (L*4)   LZSEL    = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                        PLASMA Z EFFECTIVE'ZEFF'.
C                           = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                        WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                             (ONLY USED IF 'LPSEL=.TRUE.')
C  OUTPUT: (L*4)   LISEL    = .TRUE.  => INCLUDE IONISATION RATES
C                           = .FALSE. => DO NOT INCLUDE IONISATION RATES
C  OUTPUT: (L*4)   LHSEL    = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                        NEUTRAL HYDROGREN.
C                           = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                        FROM NEUTRAL HYDROGREN.
C  OUTPUT: (L*4)   LNORM    = .TRUE.  => IF NMET=1 NORMALISE TOTAL AND
C                                        SPECIFIC LINE PWR OUTPUT FILES
C                                        PLT & PLS TO STAGE TOT POPULATN
C                           = .FALSE. => IF NOT NORMALISE TO IDENTIFIED
C                                        METASTABLE POPULATIONS.
C
C  OUTPUT: (L*4)   LRSEL    = .TRUE.  => INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   I        = LOOP INCREMENT
C          (I*4)   J        = LOOP INCREMENT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  26/11/90 - ADAS91 - PE BRIDEN - AMENDED 'XXDISP' AND 'XXPAN0'
C                                          ARGUMENT LISTS. THEY NOW IN-
C                                          CLUDE DISPLAY RETURN CODES.
C                                          IF 'RETURN' OR 'END' ENTERED
C                                          ON A PANEL, EXCEPT VIA PFKEY,
C                                          PROGRAM TERMINATES.
C
C UPDATE:  23/01/91 - ADAS91 - PE BRIDEN - ADDED  'NV'  AND  'TSCEF'  AS
C                                          ARGUMENTS TO THIS  SUBROUTINE
C                                          AND TO 'B6PAN3'.
C                                        - SET  'IFOUT=0'  BEFORE FIRST
C                                          CALL OF B6PAN3 IF 'IPAN>=0'.
C
C UPDATE:  24/01/91 - ADAS91 - PE BRIDEN - RE-ORDERED 'L?SEL'  ARGUMENTS
C                                          IN THIS SUBROUTINE AND B6PAN5
C
C UPDATE:  19/03/91 - ADAS91 - PE BRIDEN - REPLACED B6PAN6 WITH 'XXPANH'
C
C UPDATE:  18/05/93 - ADAS91 - PE BRIDEN - ADDED ARGUMENT LNORM - NEW
C                                          PANEL ENTRY REQUESTING THE
C                                          TYPE OF NORMALISATION REQD.
C                                          THIS ARGUMENT AND NMET ADDED
C                                          TO B6PAN5.
C
C UPDATE:  20/05/93 - ADAS91 - PE BRIDEN -TO REFLECT CHANGES IN BXDATA
C                                         THE FOLLOWING ARRAY DIMENSION/
C                                         SIZE CHANGES WERE MADE:
C                                         TSCEF(8,3) -> TSCEF(14,3)
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NMET       ,
     &          NDTEM      , NDDEN      , NDMET     , IL      , NV    ,
     &          IFOUT      , MAXT       ,
     &          IDOUT      , MAXD       ,
     &          ITSEL      , ICNTE      , IPROJ     , ISTRN
      INTEGER   I          , J
C-----------------------------------------------------------------------
      REAL*8    ZEFF
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40  , GTIT1*40
C-----------------------------------------------------------------------
      LOGICAL    LPEND
      LOGICAL    LLSEL     , LNORM      , LPSEL     , LZSEL    , LISEL ,
     &           LHSEL     , LRSEL      , LGPH      , LGRD1    , LDEF1
C-----------------------------------------------------------------------
      INTEGER    IE1A(ICNTE) , IE2A(ICNTE)
      INTEGER    IMETR(NDMET), LOGIC
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
C-----------------------------------------------------------------------
      REAL*8     TSCEF(14,3) ,
     &           TINE(NDTEM) , TINP(NDTEM)  , TINH(NDTEM) ,
     &           DINE(NDDEN) , DINP(NDDEN)  , RATHA(NDDEN), RATIA(NDDEN)
C-----------------------------------------------------------------------
      CHARACTER  STRGA(IL)*22
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      IF (LPEND) THEN
          WRITE(PIPEOU,*) ONE
      ELSE
          WRITE(PIPEOU,*) ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDTEM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDDEN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDMET
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NV
      CALL XXFLSH(PIPEOU)
      DO 1 I=1,14
         DO 2 J=1,3
            WRITE(PIPEOU,*)TSCEF(I,J)
            CALL XXFLSH(PIPEOU)
 2       CONTINUE
 1    CONTINUE
      WRITE(PIPEOU,*) ICNTE
      CALL XXFLSH(PIPEOU)
      DO 3 I=1,ICNTE
         WRITE(PIPEOU,*) IE1A(I)
         CALL XXFLSH(PIPEOU)
 3    CONTINUE
      DO 4 I=1,ICNTE
         WRITE(PIPEOU,*) IE2A(I)
         CALL XXFLSH(PIPEOU)
 4    CONTINUE
C STRGA IS NOT OUTPUT SINCE IT HAS ALREADY BEEN PASSED IN BXSETP
C      DO 5 I=1,IL
C         WRITE(PIPEOU,'(A22)') STRGA(I)
C         CALL XXFLSH(PIPEOU)
C 5    CONTINUE

C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF

      READ(PIPEIN,'(A)') TITLE
      READ(PIPEIN,*) IPROJ
      READ(PIPEIN,*) NMET
      DO 6 I=1,NDMET
         READ(PIPEIN,*)IMETR(I)
 6    CONTINUE
      READ(PIPEIN,*) ISTRN
      READ(PIPEIN,*) IFOUT
      READ(PIPEIN,*) MAXT
      DO 7 I=1,NDTEM
         READ(PIPEIN,*)TINE(I)
 7    CONTINUE
      DO 8 I=1,NDTEM
         READ(PIPEIN,*) TINP(I)
 8    CONTINUE
      DO 9 I=1,NDTEM
         READ(PIPEIN,*) TINH(I)
 9    CONTINUE
      READ(PIPEIN,*) IDOUT
      READ(PIPEIN,*) MAXD
      DO 10 I=1,NDDEN
         READ(PIPEIN,*) DINE(I)
 10   CONTINUE
      DO 11 I=1,NDDEN
         READ(PIPEIN,*) DINP(I)
 11   CONTINUE
      DO 12 I=1,NDDEN
         READ(PIPEIN,*) RATHA(I)
 12   CONTINUE
      DO 13 I=1,NDDEN
         READ(PIPEIN,*) RATIA(I)
 13   CONTINUE
      READ(PIPEIN,*) ZEFF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LLSEL = .TRUE.
      ELSE
          LLSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPSEL = .TRUE.
      ELSE
          LPSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LZSEL = .TRUE.
      ELSE
          LZSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LISEL = .TRUE.
      ELSE
          LISEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LHSEL = .TRUE.
      ELSE
          LHSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LNORM = .TRUE.
      ELSE
          LNORM = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LRSEL = .TRUE.
      ELSE
          LRSEL = .FALSE.
      ENDIF
C
      RETURN
      END
