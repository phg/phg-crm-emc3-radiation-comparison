C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6loss.for,v 1.1 2004/07/06 11:25:36 whitefor Exp $ Date $Date: 2004/07/06 11:25:36 $
C
      SUBROUTINE B6LOSS( NDTRN  , NDLEV  ,
     &                   ICNTE  , ISTRN  ,
     &                   XJA    , ER     , AVAL  ,
     &                   IE1A   , IE2A   ,
     &                   SLOSS  , TLOSS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6LOSS *********************
C
C  PURPOSE: TO CALCULATE THE DIRECT LINE POWER LOSS FOR EACH LEVEL AND
C           FOR THE SPECIFIC LINE POWER TRANSITION GIVEN BY 'ISTRN'.
C
C  CALLING PROGRAM:  ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  ICNTE   = NUMBER OF ELERCTRON IMPACT TRANSITIONS
C  INPUT :  (I*4)  ISTRN   = SPECIFIC LINE POWER: SELECTED ELECTRON
C                            IMPACT TRANSITION INDEX. (FOR USE WITH
C                            'IE1A()' , 'IE2A()' AND 'AA()' ARRAYS)
C
C
C  INPUT :  (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR GIVEN LEVEL.
C                            NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                            DIMENSION: ENERGY LEVEL.
C  INPUT :  (R*8)  AVAL()  = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C                            DIMENSION: ENERGY LEVEL.
C
C  INPUT :  (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C  INPUT :  (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C  OUTPUT:  (R*8)  SLOSS    = DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                             POWER TRANSITION GIVEN BY 'ISTRN'.
C                             (UNITS: ERGS SEC-1)
C  OUTPUT:  (R*8)  TLOSS()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                             (UNITS: ERGS SEC-1)
C                             DIMENSION: LEVEL INDEX
C
C           (R*8)  R2LOSS  = PARAMETER = EQUATION CONSTANT = 2.17958D-11
C                            (CONVERTS RYDBERGS/SEC TO ERGS/SEC)
C
C           (I*4)  LLOWER  = SELECTED ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C           (I*4)  LUPPER  = SELECTED ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C
C
C ROUTINES:  NONE
C
C NOTES:
C            EQUATIONS USED -
C
C            FOR EACH TRANSITION - DIRECT LINE POWER LOSS IS GIVEN BY:
C
C            LOSS  = 'R2LOSS' x AVALUE x (ENERGY DIFFERENCE)
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  29/07/92 - CORRECT ERROR - ZERO TLOSS OVER NDLEV INSTEAD OF
C                                     ICNTE.
C
C UNIX-IDL PORT:
C
C DATE: UNKNOWN
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C DATE:    10-05-96
C
C VERSION: 1.1
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R2LOSS
C-----------------------------------------------------------------------
      PARAMETER( R2LOSS = 2.17958D-11 )
C-----------------------------------------------------------------------
      INTEGER    NDTRN                , NDLEV        ,
     &           ICNTE                , ISTRN
      INTEGER    LLOWER               , LUPPER       ,
     &           IC
C-----------------------------------------------------------------------
      REAL*8     SLOSS
C-----------------------------------------------------------------------
      INTEGER    IE1A(NDTRN)          , IE2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     XJA(NDLEV)           , ER(NDLEV)    ,
     &           AVAL(NDTRN)          , TLOSS(NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IC=1,NDLEV
            TLOSS(IC)=0.0D0
    1    CONTINUE
C-----------------------------------------------------------------------
         DO 2 IC=1,ICNTE
            LLOWER = IE1A(IC)
            LUPPER = IE2A(IC)
            TLOSS(LUPPER) = TLOSS(LUPPER) +
     &                     ( R2LOSS*AVAL(IC)*( ER(LUPPER)-ER(LLOWER) ) )
    2    CONTINUE
C-----------------------------------------------------------------------
      SLOSS = R2LOSS*AVAL(ISTRN)*( ER(IE2A(ISTRN))-ER(IE1A(ISTRN)) )
C-----------------------------------------------------------------------
      RETURN
      END
