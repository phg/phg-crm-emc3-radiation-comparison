CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6spf1.for,v 1.1 2004/07/06 11:26:03 whitefor Exp $ Date $Date: 2004/07/06 11:26:03 $
CX
      SUBROUTINE B6SPF1( NDTEM  , TINE   , MAXT   , IFOUT  ,
     &                   LPEND  ,
     &                   LNEWPA , LPAPER , LCONT  , LPTOT  ,
     &                   LPSPC  , DSNPAP , DSNOUT , DSNTOT , DSNSPC ,
     &                   LGPH   , ITSEL  , GTIT1
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6SPF1 *********************
C
C  PURPOSE: TO PASS AND RECEIVE DATA FROM THE IDL OUTPUT DISPLAY SCREEN
C           VIA THE PIPE
C
C  CALLING PROGRAM: ADAS206
C
C  SUBROUTINE:
C
C  INPUT:  (I*4)   NDTEM    = PARAMETER = MAX. NO. OF TEMPERATURES
C                                         ALLOWED
C  INPUT:  (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C  INPUT:  (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES
C                             ( 1 -> 'NDTEM')
C  INPUT:  (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => OUTPUT OPTIONS CANCELLED.
C                             .FALSE. => PROCESS OUTPUT OPTIONS.
C
C  OUTPUT: (L*4)   LNEWPA   = .TRUE.  => NEW TEXT OUTPUT FILE OR
C                                        REPLACEMENT OF EXISTING FILE
C                                        REQUIRED.
C                             .FALSE. => ALLOW APPEND ON EXISTING OPEN
C                                        TEXT FILE.
C
C  OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT DATA TO TEXT OUTPUT
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        TEXT OUTPUT FILE.
C  OUTPUT: (L*4)   LCONT    = .TRUE.  => OUTPUT DATA TO CONTOUR PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        CONTOUR PASSING FILE.
C  OUTPUT: (L*4)   LPTOT    = .TRUE.  => OUTPUT DATA TO TOTAL LINE POWER
C                                        PASSING FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        TOTAL LINE POWER PASSING FILE.
C  OUTPUT: (L*4)   LPSPC    = .TRUE.  => OUTPUT DATA TO SPECIFIC LNE PWR
C                                        PASSING FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        SPECIFIC LNE PWR PASSING FILE.
C
C  OUTPUT: (C*80)  DSNPAP   = OUTPUT TEXT FILE NAME
C  OUTPUT: (C*80)  DSNOUT   = OUTPUT CONTOUR DATA SET NAME
C  OUTPUT: (C*80)  DSNTOT   = OUTPUT TOTAL LINE POWER PASSINF FILE NAME
C  OUTPUT: (C*80)  DSNSPC   = OUTPUT SPECIFIC LNE PWR PASSINF FILE NAME
C  OUTPUT: (L*4)   LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                           = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT: (I*4)   ITSEL    = INDEX OF TEMPERATURE SELECTED FOR GRAPH
C                           (FROM INPUT LIST).
C  OUTPUT: (C*40)  GTIT1    = ENTERED TITLE FOR GRAPH
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  26/11/90 - ADAS91 - PE BRIDEN - AMENDED   'XXDISP'   ARGUMENT
C                                          LIST. IT NOW INCLUDES DISPLAY
C                                          RETURN CODES.
C                                          IF 'RETURN' OR 'END' ENTERED
C                                          ON A PANEL, EXCEPT VIA PFKEY,
C                                          PROGRAM TERMINATES.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER      NDTEM  , MAXT , IFOUT , ITSEL , LOGIC , I , I4UNIT
C-----------------------------------------------------------------------
      REAL*8       TINE(NDTEM)
C-----------------------------------------------------------------------
      CHARACTER    DSNOUT*80   , DSNTOT*80 , DSNSPC*80 , GTIT1*40 , 
     &             DSNPAP*80
C-----------------------------------------------------------------------
      LOGICAL      LCONT       , LPTOT     , LPSPC    ,
     &             LPEND       , LPAPER    , LNEWPA   , LGPH
C-----------------------------------------------------------------------
      INTEGER      PIPEIN      , PIPEOU    , ONE
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1 )
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  WRITE LIST OF ELECTRON TEMPERATURES TO IDL
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) MAXT
      CALL XXFLSH(PIPEOU)
      DO 1 I=1,MAXT
         WRITE(PIPEOU,*) TINE(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
      WRITE(PIPEOU,*) IFOUT
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C  READ DATA FROM IDL
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LGPH = .TRUE.
          ELSE
              LGPH = .FALSE.
          ENDIF
C
          IF (LGPH) THEN
              READ(PIPEIN,*) ITSEL
              READ(PIPEIN,'(A)') GTIT1
          ENDIF
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPAPER = .TRUE.
          ELSE
              LPAPER = .FALSE.
          ENDIF
C
          IF (LPAPER) THEN
              READ(PIPEIN,*) LOGIC
              IF (LOGIC.EQ.ONE) THEN
                  LNEWPA = .FALSE.
              ELSE
                  LNEWPA = .TRUE.
              ENDIF
              READ(PIPEIN,'(A)') DSNPAP
          ENDIF
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LCONT = .TRUE.
          ELSE
              LCONT = .FALSE.
          ENDIF
C
          IF (LCONT) READ(PIPEIN,'(A)') DSNOUT
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPTOT = .TRUE.
          ELSE
              LPTOT = .FALSE.
          ENDIF
C
          IF (LPTOT) READ(PIPEIN,'(A)') DSNTOT
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPSPC = .TRUE.
          ELSE
              LPSPC = .FALSE.
          ENDIF
C
          IF (LPSPC) READ(PIPEIN,'(A)') DSNSPC
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
