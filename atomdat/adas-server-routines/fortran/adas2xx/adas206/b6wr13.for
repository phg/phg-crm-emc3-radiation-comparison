CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6wr13.for,v 1.1 2004/07/06 11:26:15 whitefor Exp $ Date $Date: 2004/07/06 11:26:15 $
CX
      SUBROUTINE B6WR13( IUNIT  , DATE   , IZ1    , IL    ,
     &                   NDMET  , NDTEM  , NDDEN  ,
     &                   LNORM  ,
     &                   NMET   , IMETR  ,
     &                   IFOUT  , MAXT   , TINE   ,
     &                   IDOUT  , MAXD   , DINE   ,
     &                   ILOWER , IUPPER ,
     &                   CSTRGA , PLS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6WR13 *********************
C
C  PURPOSE: TO OUTPUT SPECIFIC LINE POWER PARAMETERS TO THE PASSING
C           FILE ON STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  IUNIT   = OUTPUT STREAM NUMBER
C  INPUT :  (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (I*4)  IZ1     = RECOMBINING ION CHARGE
C                            (NOTE: IZ1 SHOULD EQUAL Z+1)
C  INPUT :  (I*4)  IL      = NUMBER OF INDEX ENERGY LEVELS
C
C  INPUT :  (I*4)  NDMET   = MAX. NO. OF METASTABLES ALLOWED
C  INPUT :  (I*4)  NDTEM   = MAX. NO. OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDDEN   = MAX. NUMBER OF DENSITIES ALLOWED
C
C  INPUT :  (L*4)  LNORM   =.TRUE.  => IF NMET=1 THEN TOTAL AND SPECIFIC
C                                      LINE POWER OUTPUT FILES PLT/PLS
C                                      NORMALISED TO STAGE TOT.POPULATN.
C                                      (** NORM TYPE = T)
C                          =.FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                      METASTABLE POPULATIONS.
C                                      (** NORM TYPE = M)
C
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES ( 1 -> 5 )
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLES IN COMPLETE LEVEL LIST
C
C  INPUT :  (I*4)  IFOUT   = 1 => INPUT TEMPERATURES IN KELVIN
C                            2 => INPUT TEMPERATURES IN EV
C                            2 => INPUT TEMPERATURES IN REDUCED FORM
C  INPUT :  (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES (1 -> 20)
C  INPUT :  (R*8)  TINE()  = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C
C  INPUT :  (I*4)  IDOUT   = 1 => INPUT DENSITIES IN CM-3
C                            2 => INPUT DENSITIES IN REDUCED FORM
C  INPUT :  (I*4)  MAXD    = NUMBER OF INPUT DENSITIES (1 -> 20)
C  INPUT :  (R*8)  DINE()  = ELECTRON DENSITIES (UNITS: SEE 'IFOUT')
C
C  INPUT :  (I*4)  ILOWER  = SPECIFIC LINE POWER: SELECTED ELECTRON
C                            IMPACT TRANSITION LOWER LEVEL INDEX
C  INPUT :  (I*4)  IUPPER  = SPECIFIC LINE POWER: SELECTED ELECTRON
C                            IMPACT TRANSITION UPPER LEVEL INDEX
C
C  INPUT :  (C*18) CSTRGA()= INDEX LEVEL CONFIGURATIONS
C  INPUT :  (R*8)  PLS(,,) = SPECIFIC LINE POWERS FOR METASTABLES. THIS
C                            IS THE SPECIFIC EMISSION ORGINATING IN THE
C                            COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                            METASTABLE. (SEE 'ISTRN')
C                             => P(SPECIFIC)/N(IMET)  (ERGS SEC-1)
C                              1st DIMENSION: METASTABLE  INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY     INDEX
C
C           (I*4)  L1      = PARAMETER = 1
C           (I*4)  L2      = PARAMETER = 2
C           (I*4)  L3      = PARAMETER = 3
C
C           (I*4)  I       = GENERAL USE
C           (I*4)  IM      = ARRAY INDEX POINTER FOR METASTABLE STATES
C           (I*4)  IT      = ARRAY INDEX POINTER FOR TEMPERATURES
C           (I*4)  ID      = ARRAY INDEX POINTER FOR DENSITIES
C
C           (R*8)  RDEN()  = ELECTRON DENSITIES (UNITS: REDUCED FORM)
C           (R*8)  RTEM()  = ELECTRON TEMPERATURES (UNITS: REDUCED FORM)
C
C           (C*1)  CSTAR   = '*'
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXTCON     ADAS      CONVERTS ENTERED TEMP. VALUES TO EV.
C          XXDCON     ADAS      CONVERTS ENTERED DENSITY VALUES TO CM-3.
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  18/05/93 - PE BRIDEN: ADDED NORMALISATION INFO TO OUTPUT.
C                                NEW ARGUMENT   - LNORM
C                                CHANGED FORMAT - 1011
C
C UPDATE:  20/05/93 - ADAS91 PEB: TO REFLECT CHANGES IN BXDATA THE
C                                 CHARACTER ARRAY CSTRGA IS NOW 18 BYTES
C                                 INSTEAD OF 12.
C                                 NOTE: ONLY THE FIRST 12 BYTES ARE
C                                       OUTPUT TO THE PASSING FILE.
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
      INTEGER   L1           , L2            , L3
C-----------------------------------------------------------------------
      PARAMETER ( L1=1 , L2=2 , L3=3 )
C-----------------------------------------------------------------------
      INTEGER   IUNIT        , IZ1           , IL         ,
     &          NDMET        , NDTEM         , NDDEN      ,
     &          NMET         ,
     &          IFOUT        , MAXT          ,
     &          IDOUT        , MAXD          ,
     &          ILOWER       , IUPPER
      INTEGER   I            , IM            , IT         , ID
C-----------------------------------------------------------------------
      LOGICAL   LNORM
C-----------------------------------------------------------------------
      CHARACTER CSTAR*1      , DATE*8
C-----------------------------------------------------------------------
      INTEGER   IMETR(NMET)
C-----------------------------------------------------------------------
      REAL*8    RTEM(20)     , RDEN(20)
      REAL*8    TINE(MAXT)   , DINE(MAXD)    , PLS(NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      CHARACTER CSTRGA(IL)*18
C-----------------------------------------------------------------------
      DATA      CSTAR/'*'/
C
C-----------------------------------------------------------------------
C
      CALL XXDCON( IDOUT , L2 , IZ1 , MAXD , DINE , RDEN )
      CALL XXTCON( IFOUT , L3 , IZ1 , MAXT , TINE , RTEM )
C-----------------------------------------------------------------------
      WRITE(IUNIT,1000)
      WRITE(IUNIT,1001) NMET
      WRITE(IUNIT,1002) (I               , I=1,NMET)
      WRITE(IUNIT,1003) (CSTRGA(I)(1:12) , I=1,NMET)
      WRITE(IUNIT,1004) (IMETR(I)        , I=1,NMET)
      WRITE(IUNIT,1005) (CSTAR           , I=1,NMET)
      WRITE(IUNIT,1006) (CSTAR           , I=1,NMET)
      WRITE(IUNIT,1007)
      WRITE(IUNIT,1008) L1 , MAXT , MAXD
      WRITE(IUNIT,1009) DBLE(IZ1)
      WRITE(IUNIT,1007)
      WRITE(IUNIT,1007)
      WRITE(IUNIT,1010) (RDEN(I)         , I=1,MAXD)
      WRITE(IUNIT,1010) (RTEM(I)         , I=1,MAXT)
C
         IF ( LNORM .AND. (NMET.EQ.1) ) THEN
            WRITE(IUNIT,1011) IUPPER , ILOWER , 'T' , IZ1 , DATE
         ELSE
            WRITE(IUNIT,1011) IUPPER , ILOWER , 'M' , IZ1 , DATE
         ENDIF
C
         DO 1 IM=1,NMET
            WRITE(IUNIT,1012) IM
               DO 2 IT=1,MAXT
                  WRITE(IUNIT,1010) (PLS(IM,IT,ID), ID=1,MAXD)
    2          CONTINUE
    1    CONTINUE
 
C-----------------------------------------------------------------------
 1000 FORMAT(80('='))
 1001 FORMAT('NMET         =',1X,I1)
 1002 FORMAT('ORDER        =',1X,5(12X,I1))
 1003 FORMAT('DESIGNATION  =',1X,5(1X,A12))
 1004 FORMAT('COPDAT INDEX =',1X,5(11X,I2))
 1005 FORMAT('PARENT REF.  =',1X,5(12X,A1))
 1006 FORMAT('SPNSYS REF.  =',1X,5(12X,A1))
 1007 FORMAT(80('-'))
 1008 FORMAT(3I5)
 1009 FORMAT(F10.5)
 1010 FORMAT(1P,8D10.2)
 1011 FORMAT(12('-'),' SPECIFIC LINE POWER: ',I2.2,' - ',I2.2,1X,
     &       '/ NORM=',A1,4X,'/ Z1=',I2,3X,'/ DATE= ',A8)
 1012 FORMAT('/',I1,'/')
C-----------------------------------------------------------------------
      RETURN
      END
