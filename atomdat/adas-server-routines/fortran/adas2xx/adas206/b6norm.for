CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas206/b6norm.for,v 1.1 2004/07/06 11:25:42 whitefor Exp $ Date $Date: 2004/07/06 11:25:42 $
CX
      SUBROUTINE B6NORM( NDLEV  , NDMET  ,
     &                   NORD   ,
     &                   STCK   ,
     &                   PLAX   , PLX    ,
     &                   PLASX  , PLSX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B6STOT *********************
C
C  PURPOSE: TO NORMALISE TOTAL/SPECIFIC LINE POWERS FOR LEVEL 1
C           AND TOTAL EQUILIBRIUM LINE POWERS TO STAGE TOTAL POPULATION.
C
C  CALLING PROGRAM:  ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C
C  INPUT :  (R*4)  STCK(,) = POPULATION MATRIX COVERING ALL NON-METAST-
C                            ABLE/ORDINARY EXCITED LEVELS AS FUNCTION
C                            OF METASTABLE INDEX.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C
C  I/O   :  (R*8)  PLAX    = INPUT:
C                            TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                            AT FIXED TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS CM3 SEC-1)
C                            OUTPUT:
C                            NORMALISED TO TOTAL STAGE POPULATION
C  I/O   :  (R*8)  PLX     = INPUT:
C                            TOTAL LINE POWERS FOR LEVEL 1 AT FIXED
C                            TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS SEC-1).
C                            OUTPUT:
C                            NORMALISED TO TOTAL STAGE POPULATION
C
C  I/O   :  (R*8)  PLASX   = INPUT:
C                            SPECIFIC EQULIBRIUM LINE PWR COEFFICIENTS.
C                            AT FIXED TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS CM3 SEC-1)
C                            OUTPUT:
C                            NORMALISED TO TOTAL STAGE POPULATION
C  I/O   :  (R*8)  PLSX    = INPUT:
C                            SPECIFIC LINE PWR FOR LEVEL 1 AT FIXED
C                            TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS SEC-1).
C                            OUTPUT:
C                            NORMALISED TO TOTAL STAGE POPULATION
C
C           (I*4)  IS1     = ORDINARY EXCITED LEVEL INDEX
C
C           (R*8)  STOTX   = VARIABLE USED TO SUM STAGE TOTAL POPULATN.
C                            (INITIAL VALUE = 1 => GROUND)
C
C ROUTINES: NONE
C
C NOTE:
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    18/05/93
C
C UPDATE:  20/05/93 - P BRIDEN: STCK ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06/06/96
C
C VERSION: 1.1				     DATE:06/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV              , NDMET              ,
     &           NORD               , IS1
C-----------------------------------------------------------------------
      REAL*8     PLAX               , PLX                ,
     &           PLASX              , PLSX               ,
     &           STOTX
C-----------------------------------------------------------------------
      REAL*4     STCK(NDLEV,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      STOTX = 1.0
C
         DO 1 IS1=1,NORD
            STOTX = STOTX + DBLE(STCK(IS1,1))
    1    CONTINUE
C
      STOTX = 1.0 / STOTX
C
      PLAX  = STOTX * PLAX
      PLX   = STOTX * PLX
      PLASX = STOTX * PLASX
      PLSX  = STOTX * PLSX
C
C-----------------------------------------------------------------------
C
      RETURN
      END
