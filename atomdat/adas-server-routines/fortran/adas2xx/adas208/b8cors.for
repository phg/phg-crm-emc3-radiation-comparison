      SUBROUTINE B8CORS(NUMT , TEVA , COEFF)

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: B8CORP ********************
C
C  PURPOSE: There is a low temperature problem in the calculation
C           of the sxb coefficients. Replace the effetive zero
C           values with an exp(E/kT) derived from first good points.
C
C
C  CALLING PROGRAM: ADAS208 (B8WRPS)
C
C
C  INPUT : (I*4)  NUMT    = NUMBER OF TEMPERATURES
C  INPUT : (I*4)  TEVA    = TEMPERATURES
C
C  I/O   : (R*4) COEFF()  = SXB DATA
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLN     ADAS      SPLINE SUBROUTINE
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  Martin O'Mullane
C
C
C DATE:    17-02-2006
C
C
C VERSION : 1.1
C DATE    : 17-02-2006
C MODIFIED: Martin O'Mullane
C           - First version
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      real*8     zero
C-----------------------------------------------------------------------
      parameter (zero = 1.0D-40)
C-----------------------------------------------------------------------
      integer    numt
      integer    it         , ilast       , i1    , i2
      integer    i4unit
C-----------------------------------------------------------------------
      real*8     a          , b           , x1    , x2   , y1   , y2
C-----------------------------------------------------------------------
      logical    lnone
C-----------------------------------------------------------------------
      real*8     teva(numt) , coeff(numt)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Find indices of effective zero coefficients
C-----------------------------------------------------------------------


      ilast = 0
      lnone = .TRUE.
      do it = 1, numt
         lnone = lnone .AND. (coeff(it).GT.zero)
         if (coeff(it).LE.zero) ilast=it
      end do

      if (lnone) then
         return
      else
         write(i4unit(-1),1000)
      endif


      if (.NOT.lnone) then

        i1 = ilast + 1
        i2 = ilast + 2

        if (i2.LE.numt) then
        
           x1 = teva(i1)
           x2 = teva(i2)
           y1 = coeff(i1)
           y2 = coeff(i2)

           b = (log(y2) - log(y1)) / (1.0D0/x2 - 1.0D0/x1)
           a = y1 / exp(b/x1)

           do it = 1, ilast
              coeff(it) = a * exp(b/teva(it))
           end do
           
        endif

      endif

C-----------------------------------------------------------------------
 1000 format(1x,28('*'),' B8CORS INFORMATION ',28('*'),/,/,
     &       1x,'SXB DATA CORRECTED FOR LOW TE')
C-----------------------------------------------------------------------

      return
      end
