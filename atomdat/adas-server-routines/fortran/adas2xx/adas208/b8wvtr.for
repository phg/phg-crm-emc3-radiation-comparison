       subroutine b8wvtr( ndsel  ,
     &                    ntrsel , itrlow , itrup ,
     &                    ie1a   , ie2a   ,
     &                    lwvtr
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: b8wvtr *********************
C
C  purpose:  Check if a transition corresponds to one in the list
C            supplied by the user.
C
C  calling program: b8wrps
C
C  subroutine:
C
C  input : (i*4)  ndsel    = maximum number of user supplied transitions
C  input : (i*4)  ntrsel   = number of user supplied transitions
C  input : (i*4)  itrlow() = lower index of transition
C  input : (i*4)  itrup()  = upper index of transition
C  input : (i*4)  ie1a     = lower index of transition to test
C  input : (i*4)  ie2a     = upper index of transition to test
C
C  output: (l*4)  lwvtr   = .true.  => spectrum line in selected range
C                         = .false. => spectrum line in selected range
C
C  VERSION  : 1.1
C  DATE     : 27-09-2018
C  MODIFIED :  Martin O'Mullane
C              - First version
C
C-----------------------------------------------------------------------
       integer   ndsel
       integer   ntrsel        , i
       integer   ie1a          , ie2a
C-----------------------------------------------------------------------
       integer   itrlow(ndsel) , itrup(ndsel)
C-----------------------------------------------------------------------
       logical   lwvtr
C-----------------------------------------------------------------------


C If no transitions are set return immediately and do not alter
C the lwvtr variable since it can be cascaded through different
C selection methods.

       if (ntrsel.LE.0) return

C Compare current transition to set requested by user

       lwvtr = .FALSE.
       do i = 1, ntrsel
          if (ie2a.eq.itrup(i).and.ie1a.eq.itrlow(i)) lwvtr = .TRUE.
       end do

       end
