C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8loss.for,v 1.2 2007/07/20 09:46:33 allan Exp $ Date $Date: 2007/07/20 09:46:33 $
C
      SUBROUTINE B8LOSS( NDTRN  , NDLEV  , NDMET ,
     &                   ICNTE  , NMET   , IMETR , ISTRN  ,
     &                   XJA    , ER     , AVAL  ,
     &                   IE1A   , IE2A   ,
     &                   SLOSS  , SWVLN  , TLOSS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8LOSS *********************
C
C  PURPOSE: TO CALCULATE THE DIRECT LINE POWER LOSS FOR EACH LEVEL AND
C           IDENTIFY THE STRONGEST SPECIFIC LINE POWER TRANSITIONS TO
C           EACH METASTABLE LEVEL.
C
C           (MODIFICATION OF B6LOSS)
C
C  CALLING PROGRAM:  ADAS208
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT :  (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES
C  INPUT :  (I*4)  IMETR() = METASTABLE INDICES IN LEVEL LIST
C                            DIMENSION: METASTABLE COUNT INDEX
C  OUTPUT:  (I*4)  ISTRN() = SPECIFIC LINE POWER: SELECTED ELECTRON
C                            IMPACT TRANSITION INDEX. (FOR USE WITH
C                            'IE1A()' , 'IE2A()' AND 'AA()' ARRAYS)
C                            WHICH GIVES LARGEST POWER TO METASTABLE
C                            DIMENSION: METASTABLE COUNT INDEX
C
C
C  INPUT :  (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR GIVEN LEVEL.
C                            NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                            DIMENSION: ENERGY LEVEL.
C  INPUT :  (R*8)  AVAL()  = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C                            DIMENSION: ENERGY LEVEL.
C
C  INPUT :  (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C  INPUT :  (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C  OUTPUT:  (R*8)  SLOSS()  = DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                             POWER TRANSITION GIVEN BY 'ISTRN' FOR EACH
C                             METASTABLE (UNITS: ERGS SEC-1)
C                             DIMENSION: METASTABLE COUNT INDEX
C  OUTPUT:  (R*8)  SWVLN()  = WAVELENGTH (ANGSTROM) FOR SPECIFIC LINE
C                             POWER TRANSITION GIVEN BY 'ISTRN' FOR EACH
C                             METASTABLE (UNITS: ERGS SEC-1)
C                             DIMENSION: METASTABLE COUNT INDEX
C  OUTPUT:  (R*8)  TLOSS()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                             (UNITS: ERGS SEC-1)
C                             DIMENSION: LEVEL INDEX
C
C           (R*8)  R2LOSS  = PARAMETER = EQUATION CONSTANT = 2.17958D-11
C                            (CONVERTS RYDBERGS/SEC TO ERGS/SEC)
C           (R*8)  WCVRN   = PARAMETER = EQUATION CONSTANT = 911.268
C                            (CONVERTS RYD. TRANS ENERGY TO WAVELENGTH
C                             IN ANGSTROM)
C           (R*8)  SCURR   = CURRENT INDIVIDUAL LINE POWER
C
C           (I*4)  LLOWER  = SELECTED ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C           (I*4)  LUPPER  = SELECTED ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C           (I*4)  IM      = METASTABLE COUNT INDEX
C
C
C ROUTINES:  NONE
C
C NOTES:
C            EQUATIONS USED -
C
C            FOR EACH TRANSITION - DIRECT LINE POWER LOSS IS GIVEN BY:
C
C            LOSS  = 'R2LOSS' x AVALUE x (ENERGY DIFFERENCE)
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  29/07/92 - CORRECT ERROR - ZERO TLOSS OVER NDLEV INSTEAD OF
C                                     ICNTE.
C UPDATE:  23/05/96 - CONVERTED B6LOSS TO B8LOSS, CHANGED ISTRN TO
C                     OUTPUT INDEX OF STRONGEST RADIATING TRANSITION TO
C                     EACH METASTABLE.
C
C***********************************************************************
C PUT UNDER S.C.C.S CONTROL:
C
C VERSION: 1.1				DATE: 15/07/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER S.C.C.S
C 
C VERSION: 1.2				DATE: 20/07/07
C MODIFIED: Allan Whiteford
C	    - Small modification to comments to allow for automatic
C             documentation preparation.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R2LOSS               , WCVRN
C-----------------------------------------------------------------------
      PARAMETER( R2LOSS = 2.17958D-11 , WCVRN = 911.268 )
C-----------------------------------------------------------------------
      INTEGER    NDTRN                , NDLEV        ,
     &           NDMET                , NMET         ,
     &           ICNTE
      INTEGER    LLOWER               , LUPPER       ,
     &           IC                   , IM
C-----------------------------------------------------------------------
      REAL*8     SCURR
C-----------------------------------------------------------------------
      INTEGER    IE1A(NDTRN)          , IE2A(NDTRN)
      INTEGER    IMETR(NDMET)         , ISTRN(NDMET)
C-----------------------------------------------------------------------
      REAL*8     XJA(NDLEV)           , ER(NDLEV)    ,
     &           AVAL(NDTRN)          , TLOSS(NDLEV)
      REAL*8     SLOSS(NDMET)         , SWVLN(NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IM = 1,NMET
           ISTRN(IM) = 0
           SLOSS(IM) = 0.0D0
           SWVLN(IM) = 0.0D0
    1    CONTINUE
C
         DO 2 IC=1,NDLEV
            TLOSS(IC)=0.0D0
    2    CONTINUE
C-----------------------------------------------------------------------
         DO 4 IC=1,ICNTE
            LLOWER = IE1A(IC)
            LUPPER = IE2A(IC)
            SCURR  =  R2LOSS*AVAL(IC)*( ER(LUPPER)-ER(LLOWER) )
            TLOSS(LUPPER) = TLOSS(LUPPER) + SCURR
C
            DO 3 IM = 1,NMET
              IF ((LLOWER.EQ.IMETR(IM)).AND.
     &            (SCURR.GT.SLOSS(IM))) THEN
                  ISTRN(IM) = IC
                  SLOSS(IM) = SCURR
                  SWVLN(IM) = WCVRN/( ER(LUPPER)-ER(LLOWER))
              ENDIF
    3       CONTINUE
C
    4    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
