       subroutine b8wvrg( ndwvl  ,
     &                    nwvl   , wvmin  , wvmax  , amin ,
     &                    wvl    , aval   ,
     &                    lwvrg  , iwvrg
     &                  )

       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: b8wvrg *********************
c
c  purpose:  check if a line wavelength is in one of the selected
c            wavelength intervals and exceeds the A-value threshold.
c
c  calling program: hapecf
c
c
c  subroutine:
c
c  input : (i*4)  ndwvl   = maximum number of wavelength intervals
c
c  input : (i*4)  nwvl    = wvaelength intervals
c  input : (r*8)  wvmin() = lower limit of wavelength interval (ang)
c  input : (r*8)  wvmax() = upper limit of wavelength interval (ang)
c  input : (r*8)  amin()  = A-value threshold for interval (s-1)
c
c  input : (r*8)  wvl     = input line wavelength for test (ang)
c  input : (r*8)  aval    = input A-value for test (s-1)
c
c  output: (l*4)  lwvrg   = .true.  => spectrum line in selected range
c                         = .false. => spectrum line in selected range
c  output: (i*4)  iwvrg   = index of wavelength range in which line lies
c                           if lwvrg = .true. otherwise set to zero
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c
c  VERSION  : 1.1
c  DATE     : 15-03-2013
c  MODIFIED :  Martin O'Mullane
c              - First version - based on hawvrg.for.
c
c-----------------------------------------------------------------------
      integer   ndwvl
      integer   nwvl        , iwvrg
      integer   i
c-----------------------------------------------------------------------
      real*8    wvl         , aval
c-----------------------------------------------------------------------
      real*8    wvmin(ndwvl), wvmax(ndwvl) , amin(ndwvl)
c-----------------------------------------------------------------------
      logical   lwvrg
c-----------------------------------------------------------------------

c----------------------------------------------------------------------
c  set lwvrg to .false.  and iwvrg  to zero initially
c-----------------------------------------------------------------------
          lwvrg = .false.
          iwvrg = 0

c-----------------------------------------------------------------------
c test spectrum line
c-----------------------------------------------------------------------

       i = 0

   10  i = i + 1
       if (i.le.nwvl) then
           if ( (wvl.ge.wvmin(i)).and.
     &          (wvl.lt.wvmax(i)).and.
     &          (aval.ge.amin(i))      ) then
               lwvrg = .true.
               iwvrg = i
               go to 20
           else
               go to 10
           endif
       endif

   20  return

c-----------------------------------------------------------------------
      end
