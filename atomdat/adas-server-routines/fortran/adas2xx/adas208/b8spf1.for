C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8spf1.for,v 1.4 2013/03/18 23:21:21 mog Exp $ Date $Date: 2013/03/18 23:21:21 $
C
      SUBROUTINE B8SPF1( NDTEM  , TINE   , MAXT   , IFOUT  ,
     &                   LPEND  , LGCR   , UID    ,
     &                   LNEWPA , LPAPER , LCONT  , LPASS  ,
     &                   DSNPAP , DSNOUT , DSNPAS , DSNGCR ,
     &                   LGPH   , ITSEL  , GTIT1  , CADAS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8SPF1 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH PANELS FOR PASSING FILE OUTPUT.
C           (OUTPUT DATA SET SPECIFICATIONS).
C
C  CALLING PROGRAM: ADAS208
C
C  SUBROUTINE:
C
C  INPUT:  (I*4)   NDTEM    = PARAMETER = MAX. NO. OF TEMPERATURES
C                                         ALLOWED
CX INPUT:  (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
CX INPUT:  (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES
CX                            ( 1 -> 'NDTEM')
CX INPUT:  (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
CX                          = 2 => INPUT TEMPERATURES IN EV
CX                          = 3 => INPUT TEMPERATURES IN REDUCED FORM
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => OUTPUT OPTIONS CANCELLED.
C                             .FALSE. => PROCESS OUTPUT OPTIONS.
C
CX OUTPUT: (L*4)   LNEWPA   = .TRUE.  => NEW TEXT OUTPUT FILE OR
CX                                       REPLACEMENT OF EXISTING FILE
CX                                       REQUIRED.
CX                            .FALSE. => ALLOW APPEND ON EXISTING OPEN
CX                                       TEXT FILE.
C
CX OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT DATA TO TEXT OUTPUT
CX                                       FILE.
CX                            .FALSE. => NO OUTPUT OF CURRENT DATA TO
CX                                       TEXT OUTPUT FILE.
CX OUTPUT: (L*4)   LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
CX                          = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
CX OUTPUT: (I*4)   ITSEL    = INDEX OF TEMPERATURE SELECTED FOR GRAPH
CX                          (FROM INPUT LIST).
CX OUTPUT: (C*40)  GTIT1    = ENTERED TITLE FOR GRAPH
C  OUTPUT: (L*4)   LCONT    = .TRUE.  => OUTPUT DATA TO PEC PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        PEC PASSING FILE.
C  OUTPUT: (L*4)   LPASS    = .TRUE.  => OUTPUT DATA TO SXB PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        SXB PASSING FILE.
C  OUTPUT: (L*4)   LGCR     = .TRUE.  => OUTPUT DATA TO GCR PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        GCR PASSING FILE.
C
C  OUTPUT: (C*80)  DSNOUT   = OUTPUT PEC DATA SET NAME (SEQUENTIAL)
C  OUTPUT: (C*80)  DSNPAS   = OUTPUT SXB DATA SET NAME (SEQUENTIAL)
C  OUTPUT: (C*26)  DSNGCR   = OUTPUT GCR DATA SET NAME (SEQUENTIAL)
C
C****** NEED TO CHECK WHICH OF THESE VARIABLES STILL REMAIN ****
C          (I*4)   L1       = PARAMETER = 1
C
C          (C*8)   F3       = PARAMETER = 'VDEFINE '
C          (C*8)   F5       = PARAMETER = 'CHAR    '
C
C          (I*4)  IPANRC   = RETURN CODE FROM ISPF PANEL DISPLAY
C          (I*4)  IDPAN    = INDEX FOR 'DPANEL' ISPF PANEL FOR DISPLAY
C          (I*4)  ILEN     = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C
C          (C*8)  DTABLE() = ISPF TABLE NAMES FOR 'DPANEL()'
C          (C*8)  DPANEL() = ISPF PANEL NAMES (FOR DISPLAY)
C          (C*8)  DPAN     = SPECIFIED ISPF PANEL NAME
C          (C*4)  KEY      = 'PF' KEY VALUE IF PRESSED E.G. = 'PF03'
C          (C*6)  USERID   = USER ID UNDER WHICH PROGRAM IS RUN
C          (C*8)  CURPOS   = CURSOR POSITION WHEN PANEL (RE)-DISPLAYED
C                            (PANEL VARIABLE NAME IN BRACKETS)
C          (C*8)  MSGTXT   = ERROR MESSAGE NAME (BLANK => NO ERROR)
C                            FOR NEXT DISPLAYED PANEL
C                            (MESSAGE NAME IN BRACKETS)
C          (C*8)  OUTDSN   = OUTPUT PEC PASSING FILE: LIBRARY NAME
C          (C*8)  PASDSN   = OUTPUT SXB PASSING FILE: LIBRARY NAME
C          (C*8)  GCRDSN   = OUTPUT GCR PASSING FILE: LIBRARY NAME
C
C          (L*4)  LREPLC   = .TRUE.  => IF PEC FILE EXISTS REPLACE
C                            .FALSE. => IF PEC FILE EXISTS DO NOT
C                                       REPLACE IT.
C          (L*4)  LREPLP   = .TRUE.  => IF SXB  FILE EXISTS REPLACE
C                            .FALSE. => IF SXB  FILE EXISTS DO NOT
C                                       REPLACE IT.
C          (L*4)  LREPLG   = .TRUE.  => IF GCR  FILE EXISTS REPLACE
C                            .FALSE. => IF GCR  FILE EXISTS DO NOT
C                                       REPLACE IT.
C          (L*4)  LEXIST   = .TRUE.  => DATASET EXISTS
C                            .FALSE. => DATASET DOES NOT EXIST
C          (C*80) CADAS    = ADAS HEADER: INCLUDES RELEASE,PROGRAM,TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     ADAS      FLUSH PIPE
C          XXADAS     ADAS      GET HEADER FROM IDL VIA PIPE
C
C***** END *****
C AUTHOR:  H P SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/01/92
C
C UPDATE:  10/07/92 HP SUMMERS  - INCLUDE GCR FILE
C
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C UPDATE:  02/05/96 DH BROOKS   - LGCR FILE INCLUDED FOR 208.
C                                 READING OF HEADER BY XXADAS MOVED HERE 
C                                 TO ALLOW INCLUSION OF ISTOP.
C***********************************************************************
C PUT UNDER S.C.C.S. CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER S.C.C.S.
C 
C VERSION: 1.2				DATE: 20/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - ADDED CALLS TO XXFLSH AND MADE LOOPS EXPLICIT
C
C VERSION: 1.3                          DATE: 25/11/04
C MODIFIED: MARTIN TORNEY (UNIVERSITY OF STRATHCLYDE)
C           - FIXED BUG WHERE DSNOUT AND DSNPAS STRINGS CAN CONTAIN
C             RANDOM DATA AT THE END OF THE STRING
C 
C VERSION : 1.4
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Increase size of filename variables from 80 to 132 characters.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      INTEGER    NDTEM     , MAXT      , IFOUT     ,
     &           ITSEL     , LOGIC     , I
      INTEGER    PIPEIN    , PIPEOU     , ONE  , IX
C-----------------------------------------------------------------------
      REAL*8     TINE(NDTEM) 
C-----------------------------------------------------------------------
      CHARACTER  DSNPAP*132   , DSNOUT*132   , DSNPAS*132 , GTIT1*40
      CHARACTER  DSNGCR*132   , UID*10       , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL    LPEND       , LPAPER      , LCONT        , LNEWPA  ,
     &           LPASS       , LGPH        , LGCR
C-----------------------------------------------------------------------
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1 )
C-----------------------------------------------------------------------
C  WRITE LIST OF ELECTRON TEMPERATURES TO IDL
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) MAXT
      CALL XXFLSH(PIPEOU)
      DO 99 I=1,MAXT
         WRITE(PIPEOU,*) TINE(I)
 99   CONTINUE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IFOUT
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C  READ DATA FROM IDL
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LGPH = .TRUE.
          ELSE
              LGPH = .FALSE.
          ENDIF
C
          IF (LGPH) THEN
              READ(PIPEIN,*) ITSEL
              READ(PIPEIN,'(A)') GTIT1
          ENDIF
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPAPER = .TRUE.
          ELSE
              LPAPER = .FALSE.
          ENDIF
C
          IF (LPAPER) THEN
              READ(PIPEIN,*) LOGIC
              IF (LOGIC.EQ.ONE) THEN
                  LNEWPA = .FALSE.
              ELSE
                  LNEWPA = .TRUE.
              ENDIF
              READ(PIPEIN,'(A)') DSNPAP
          ENDIF
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LCONT = .TRUE.
          ELSE
              LCONT = .FALSE.
          ENDIF
C
C         IF (LCONT) READ(PIPEIN,'(A)') DSNOUT
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LPASS = .TRUE.
          ELSE
              LPASS = .FALSE.
          ENDIF
C
C         IF (LPASS) READ(PIPEIN,'(A)') DSNPAS
C
          READ(PIPEIN,*) LOGIC
          IF (LOGIC.EQ.ONE) THEN
              LGCR = .TRUE.
          ELSE
              LGCR = .FALSE.
          ENDIF
CC
CC CONSTRUCT PASSING FILE NAMES - NOW DONE AUTOMATICALLY
CC
C         DSNOUT='/home/'//UID//'/adas/pass/pec.pass'
C         DSNPAS='/home/'//UID//'/adas/pass/sxb.pass'

C READ PASSING FILE DIRECTORY FROM IDL

          READ(PIPEIN,'(A)')DSNGCR

          IX = INDEX(DSNGCR,'.pass')-1
          DSNOUT = DSNGCR(1:IX)//'pec.pass'
          DSNPAS = DSNGCR(1:IX)//'sxb.pass'

      ENDIF
C-----------------------------------------------------------------------
C GATHER ADAS HEADER TO PASS TO B8OUT0
C-----------------------------------------------------------------------
      IF(LPAPER.AND.(.NOT.LPEND)) THEN
        CALL XXADAS( CADAS )      
      ENDIF
C-----------------------------------------------------------------------
C
      RETURN
      END
