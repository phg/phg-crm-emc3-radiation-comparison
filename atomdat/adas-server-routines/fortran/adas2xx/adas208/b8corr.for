C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8corr.for,v 1.5 2018/08/03 17:21:29 mog Exp $ Date $Date: 2018/08/03 17:21:29 $
C
      SUBROUTINE B8CORR(MAXT, MAXD, NMET, NPL3, TEVA, COEFF)


      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8CORR *********************
C
C  PURPOSE: Corrects unphysical low temperature recombination
C           contributions to a PEC.
C
C           There is a low temperature problem in the production
C           calculation which gives unphysical recombination
C           coefficients. Generally the first 3-4 temperatures in the
C           ADAS 96 standard are affected. This routine replaces the
C           first 4 temperatures from the  recombination contribution
C           with extrapolated values from the remaining data.
C
C          
C
C  CALLING PROGRAM: ADAS208 (B8WRMC)
C
C  INPUT : (I*4)  MAXT    = NUMBER OF TEMPERATURES
C  INPUT : (I*4)  MAXD    = NUMBER OF DENSITIES
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES
C  INPUT : (I*4)  NPL3    = NUMBER OF ACTIVE METAS. FOR RE+3B OF (Z+1) ION
C
C  I/O   : (R*8) COEFF()  = (Z+1)-(Z) RECOM GEN. COLL. RAD. COEFFTS.
C                           (FVRRED IN ADAS208 CALL)
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: (Z) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  Martin O'Mullane
C          
C
C DATE:    14-09-99
C
C
C VERSION: 1.1                          DATE: 14-09-99
C MODIFIED: Martin O'Mullane 
C           - First version
C
C VERSION: 1.2                          DATE: 26-10-99
C MODIFIED: Martin O'Mullane 
C           - Change the condition for extrapolating. If there are
C             coeff .GT. 1.0 then initiate extrapolation. Also 
C             consider eachmetastable separately.
C
C VERSION: 1.3                          DATE: 20-07-07
C MODIFIED: Allan Whiteford
C           - Small modification to comments to allow for automatic
C             documentation preparation.
C
C VERSION : 1.4                       
C DATE    : 31-07-2009
C MODIFIED: Martin O'Mullane
C           - ITAG should be an integer and initialised. 
C
C VERSION : 1.5
C DATE    : 05-12-2017
C MODIFIED: Matthew Bluteau
C           - Increase max number of metastables to 13 (up from 4).
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM    , NDDEN      , NDMET
      INTEGER    NIN      , NOUT       , IWRONG
C-----------------------------------------------------------------------
      PARAMETER( NIN = 35 , NOUT  = 35 , IWRONG = 3 )
      PARAMETER( NDTEM=35 , NDDEN = 24 , NDMET  = 13 )
C-----------------------------------------------------------------------
      INTEGER    ITA                   , IDA             , ITVAL      ,
     &           maxt                  , maxd            ,
     &           nmet                  , npl3
      INTEGER    IET                   , IED             , IT         ,
     &           IOPT                  , IL              , IR         ,
     &           ID                    , ind
      INTEGER    i4unit
C-----------------------------------------------------------------------
      REAL*8     R8FUN1   , REPL       , ymin            , ymax
C-----------------------------------------------------------------------
      LOGICAL    LSETX    , LNONE
C-----------------------------------------------------------------------
      INTEGER    ITAG(NDTEM)
C-----------------------------------------------------------------------
      REAL*8     COEFF(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8     DATA(NDMET,NDMET,NDTEM-IWRONG,NDDEN)
      REAL*8     TEST(NDTEM)
      REAL*8     TEVA(NDTEM)

      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)    
C-----------------------------------------------------------------------
      LOGICAL    LRET(NDMET)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Capture silly inputs and don't do anything if the coeff is monotonic
C in temperature.
C-----------------------------------------------------------------------
      
      if (maxt-iwrong .LT. 4) then
         write(i4unit(-1),*)'B8CORR - need more points to spline'
         return
      endif
      
      do il=1, nmet
        LRET(il) = .TRUE.
        do ir = 1, npl3
          do id = 1, maxd
            do it=1,maxt
               test(it) = coeff(il,ir,it,id)
               itag(it) = it
               LRET(il) = LRET(il) .AND. (coeff(il,ir,it,id).LT.1.0D0)
            end do
            call xxr8sort(maxt,.TRUE.,test,itag)
            do ind = 1, maxt-1
               LRET(il) = LRET(il) .AND. (itag(ind+1)-itag(ind).EQ.1)
            end do
          end do
        end do
      end do
      
      lnone = .true.
      do il =1, nmet
        if (.not.LRET(il)) then
           write(i4unit(-1),1000)il
           lnone = .FALSE.
        endif
      end do
              
      IF (LNONE) RETURN 

 1000 FORMAT(1X,28('*'),' B8CORR INFORMATION ',28('*'),/,/,
     &       1X,'LOW TE RECOMBINATION DATA CORRECTED',/,
     &       1X,'FOR METASTABLE : ',I2,/)

C-----------------------------------------------------------------------
C SET UP FIRST SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------

      LSETX = .TRUE.
      IOPT  = 0

C-----------------------------------------------------------------------
C Set up the 'new' coeff array by removing the first 
C IWRONG temperatures.
C-----------------------------------------------------------------------

       do id=1,maxd
         do it=1,maxt
           do ir=1,npl3
             do il=1,nmet
               if (it.GT.IWRONG) then
                  data(il,ir,it-iwrong,id) = coeff(il,ir,it,id)
               endif
             end do
           end do
         end do
       end do

C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------


      DO IET=iwrong+1,maxt
         XIN(IET-iwrong) = DLOG( TEVA(IET) )
      END DO

      DO IT=1,maxt
         XOUT(IT) = DLOG( TEVA(IT)  )
      END DO

      ITA   = MAXT - IWRONG
      ITVAL = MAXT
      IDA   = MAXD
	
      
C-----------------------------------------------------------------------
C Run over all metastables of ion (NMET) and z+1 ion (NPL3) 
C-----------------------------------------------------------------------

      do 2 il = 1, nmet
      do 1 ir = 1, npl3

C SPLINE OVER ALL DATASET ELECTRON DENSITIES FOR EACH USER ELECTRON TEMP

         if (.not.lret(il)) then
            DO IED=1,IDA
 
               DO IET=1,ITA
                  YIN(IET) = DLOG( DATA(IL,IR,IET,IED) )
               END DO
 
               CALL XXSPLN( LSETX , IOPT    , R8FUN1 ,
     &                      ITA   , XIN     , YIN    ,
     &                      ITVAL , XOUT    , YOUT   ,
     &                      DF
     &                    )
               DO IT=1,ITVAL
                 repl = DEXP( YOUT(IT) )
                 ymin = min(repl,COEFF(IL,IR,IT,IED))
                 ymax = max(repl,COEFF(IL,IR,IT,IED))
                 if (it.LE.IWRONG.AND.(ymin/ymax).LT.0.9) then
                    COEFF(IL,IR,IT,IED) = DEXP( YOUT(IT) )
                 endif
               END DO
 
            END DO
         endif 

C-----------------------------------------------------------------------
   1  CONTINUE
   2  CONTINUE
C-----------------------------------------------------------------------

      RETURN
      END
