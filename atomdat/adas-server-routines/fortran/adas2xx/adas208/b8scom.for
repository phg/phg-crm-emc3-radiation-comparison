C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8scom.for,v 1.3 2006/02/23 14:48:18 mog Exp $ Date $Date: 2006/02/23 14:48:18 $
C
      SUBROUTINE B8SCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                   IL    , WA         , NPL    , BWNOA  , 
     &                   NMET  , IMETR      , NORD   , IORDR  ,
     &                   NV    , SCEF       , SCOM   ,
     &                   MAXT  , TEA        , 
     &                   ICNTS , ISTRN      , IS1A   , IS2A   ,
     &                   LSSETA, SGRDA      , ESGRDA ,     
     &                   SMETA , ESMETA     , SORDA  , ESORDA ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8SCOM *********************
C
C  PURPOSE: TO ESTABLISH IONISATION RATE COEFFICIENTS  Z --> Z+1 FOR  A 
C           SET  OF TEMPERATURES GIVEN BY THE ARRAY 'TEA()' USING CUBIC 
C           SPLINES ON  A SET OF RATE  COEFFICIENTS  COVERING  THE
C           TEMPERATURES GIVEN BY THE ARRAY 'SCEF()'.
C
C           IONISATION DATA COMES EITHER FROM AN INTERACTIVE SEARCH VIA
C           THE ADAS208/ADAS502 ROUTE OR DIRECTLY FROM THE INPUT ADF04
C           FILE.  
C
C           THE OUTPUT IS SEPARATED INTO THE METASTABLE PART (SMETA)AND 
C           THE ORDINARY LEVEL PART (SORDA) APPROPRIATELY INDEXED.  
C           EXPONENTIAL FACTORS (ESMETA AND (ESORDA) ARE KEPT SEPARATE 
C           FROM THE REMAINDER OF THE RATE COEFFICIENTS.
C
C           IONISATION TYPE IS SELECTED VIA 'ICNTS' & 'ISTRN'
C
C           RATE COEFFICIENTS ARE GIVEN FOR A NUMBER OF IONISING LEVELS
C           AND THE ARRAY 'SGRDA(,,)' REPRESENTS  COEFFTS.  FOR COMB-
C           INATIONS OF TEMPERATURE, IONISING LEVEL INDEX AND FINAL 
C           PARENT INDEX.
C
C           SPLINE IS CARRIED OUT USING LOG(RATE COEFFICIENT VALUES)
C
C  CALLING PROGRAM:  ADAS208
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED

C  INPUT :  (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT :  (R*8)  WA()    = ENERGY LEVELS RELATIVE TO LOWEST(CM-1)
C  INPUT :  (I*4)  NPL     = NUMBER OF PARENTS
C  INPUT :  (R*8)  BWNOA() = PARENT ENERGIES RELATIVE TO RECOMBINED
C                            ION GROUND LEVEL (CM-1) 
C
C  INPUT :  (I*4)  NMET    = NUMBER OF RECOMBINED METASTABLES
C  INPUT :  (I*4)  IMETR() = INDICES OF METASTABLES IN FULL LEVEL LIST    
C  INPUT :  (I*4)  NORD    = NUMBRE OF ORDINARY EXCITED LEVELS
C  INPUT :  (I*4)  IORDR() = INDICES OF ORDINARY LEVELS IN FULL LEVEL LIST 
C
C  INPUT :  (I*4)  NV      = NUMBER OF TEMPERATURES REPRESENTED IN THE
C                            INPUT DATA SET.
C  INPUT :  (R*8)  SCEF()  = TEMPERATURES REPRESENTED IN INPUT DATA SET
C  INPUT :  (R*8)  SCOM(,)= RATE COEFF. REPRESENTED IN INPUT DATA SET
C                            1st DIMENSION: TEMPERATURE INDEX ('SCEF')
C                            2nd DIMENSION: IONISATION INDEX
C                                           (SEE: 'ISTRN()')
C
C  INPUT :  (I*4)  MAXT    = NUMBER OF ISPF SELECTED TEMPERATURES FOR
C                            OUTPUT.
C  INPUT :  (R*8)  TEA()   = ISPF SELECTED TEMPERATURES FOR OUTPUT.
C
C  INPUT :  (I*4)  ICNTS   = NUMBER OF SELECTED IONISATIONS
C  INPUT :  (I*4)  ISTRN() = INDEX VALUES IN MAIN TRANSITION ARRAY WHICH
C                            REPRESENT IONISATIONS  OF THE  SELECTED
C                            TYPE - USED TO SELECT APPROPRIATE RATE COEFFTS 
C                            FOR IONISATION Z --> Z+1 TYPE.
C  INPUT :  (I*4)  IS1A(  )= PARENT INDEX.
C                            DIMENSION: 'TRANSITION'/IONISATION  INDEX
C  INPUT :  (I*4)  IS2A()  = IONISING LEVELS INDICES.
C                            DIMENSION: 'TRANSITION'/IONISATION INDEX
C
C  INPUT :  (I*4)  LSSETA(,)=.TRUE.  => IONISATION DATA FROM ADAS502 ROUTE
C                            .FALSE. => NOT AVAILABLE FROM ADAS502 ROUTE
C                             1ST DIM: METASTABLE INDEX FROM MET. LIST
C                             2ND DIM: PARENT INDEX
C  INPUT :  (R*8)  SGRDA(,,)= INPUT IONISATION RATE COEFFT. VALUES.
C                             FROM THE ADAS208/ADAS502 LOOP
C                             (EXCLUDING EXPONENTIAL TEMPERATURE FACTOR)
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: IONISING LEVEL INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C  INPUT :  (R*8)  ESGRDA(,,)= IONISATION RATE COEFFT. EXPONENTIAL FACTORS 
C                              FROM THE ADAS208/ADAS502 LOOP
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: IONISING LEVEL INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C
C  OUTPUT:  (R*8)  SMETA(,,)= SPLINED IONISATION RATE COEFFT. VALUES.
C                             FOR THE METASTABLES
C                             (EXCLUDING EXPONENTIAL TEMPERATURE FACTOR)
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: IONISING METASTABLE INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C  OUTPUT:  (R*8)  ESMETA(,,)= SPLINED IONISATION RATE COEFFT. 
C                              EXPONENTIAL TEMPERATURE FACTORS.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: IONISING METASTABLE INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C  OUTPUT:  (R*8)  SORDA(,,)= SPLINED IONISATION RATE COEFFT. VALUES.
C                             FOR THE ORDINARY LEVELS
C                             (EXCLUDING EXPONENTIAL TEMPERATURE FACTOR)
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: IONISING ORDINARY LEVEL INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C  OUTPUT:  (R*8)  ESORDA(,,)= SPLINED IONISATION RATE COEFFT. 
C                              EXPONENTIAL TEMPERATURE FACTORS.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: IONISING ORDINARY LEVEL INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C
C  OUTPUT:  (L*4)  LTRNG() = .TRUE. => TEMPERATURE VALUES WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                          = .FALSE.=>TEMPERATURE VALUE NOT WITHIN RANGE
C                                     READ FROM INPUT COPASE DATA SET.
C                            1st DIMENSION: TEMPERATURE INDEX.
C
C
C           (I*4)  NTDSN   = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C                                        ALLOWED IN INPUT DATA SET = 14
C           (I*4)  NLTEM   = PARAMETER = MUST BE >= 'NDTEM'
C
C           (I*4)  IOPT    = SPLINE END CONDITIONS/EXTRAPOLATION CONTROL
C                            SWITCH - SEE 'XXSPLE'
C                            I.E. DEFINES THE BOUNDARY DERIVATIVES.
C                            (VALID VALUES = 0, 1, 2, 3, 4)
C           (I*4)  I       = GENERAL INDEX
C           (I*4)  ICAP    = CAPTURING LEVEL INDEX BEING ASSESSED.
C           (I*4)  IC      = RECOMBINATION ARRAY INDEX
C           (I*4)  IP      = PARENT INDEX
C           (I*4)  IT      = TEMPERATURE ARRAY INDEX
C
C           (R*8)  DYIN()  = INTERPOLATED DERIVATIVES
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C
C           (L*4)  LSETX   = .TRUE.  => X-AXES ('TIN()' VALUES) NEED TO
C                                       SET IN 'XXSPLE'.
C                            .FALSE. => X-AXES ('TIN()' VALUES) HAVE
C                                       BEEN SET IN 'XXSPLE'.
C                            (NOTE: 'LSETX' IS RESET BY 'XXSPLE')
C
C           (R*8)  LSCOM() = LOG ( 'SCOM(,)' ) FOR GIVEN IONISING LEVEL
C                            DIMENSION: TEMPERATURE INDEX ('SCEF()')
C           (R*8)  LSGRD()= LOG ( SPLINED IONIS  RATE COEFTS )
C                            DIMENSION: TEMPERATURE INDEX ('TEA()' )
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C
C
C AUTHOR:  HP SUMMERS, UNIVERSITY OF STRATHCLYDE 
C          TEL. 0141-548-4196 
C
C DATE:    14/09/99
C
C 
C VERSION  : 1.2                        
C DATE     : 19/12/2000
C MODIFIED : Martin O'Mullane
C            - Excluded S values GT 1.0 from the spline fit in order
C              to compensate for numerical problems at low temperatures. 
C
C VERSION  : 1.3                        
C DATE     : 17/02/2006
C MODIFIED : Martin O'Mullane
C            - Te values for S-line splining may not be the same
C              so set lsetx to TRUE before call to xxsple.
C            - Set unused values in redscef and redlscom to 0.0.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NTDSN              , NLTEM
C-----------------------------------------------------------------------
      PARAMETER( NTDSN = 14         , NLTEM = 101        )
C-----------------------------------------------------------------------
      REAL*8     ZERO
C-----------------------------------------------------------------------
      PARAMETER( ZERO = 1.0D-70 )
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDTEM              , NDLEV      ,
     &           NDMET              ,
     &           IL                 , NPL                , NMET       ,
     &           NORD               , NV                 , MAXT       ,
     &           ICNTS
      INTEGER    IOPT               , I                  , ICAP       ,
     &           IC                 , IT                 , IP         ,
     &           IMET               , IORD               , IMETS      ,
     &           IORDS              , ISTART             , J 
      INTEGER    I4UNIT             , nspln
C-----------------------------------------------------------------------
      REAL*8     R8EXPE
C-----------------------------------------------------------------------
      LOGICAL    LSETX             ,  LMET
C-----------------------------------------------------------------------
      INTEGER    IS1A(NDLEV)       , IS2A(NDLEV)         , ISTRN(NDTRN)
      INTEGER    IMETR(NDMET)      , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8     WA(NDLEV)         , BWNOA(NDMET)
      REAL*8     SCEF(NDTEM)       , SCOM(NTDSN,NDTRN)   ,
     &           TEA(NDTEM)        
      REAL*8     SGRDA(NDTEM,NDMET,NDMET) , ESGRDA(NDTEM,NDMET,NDMET) ,
     &           SMETA(NDTEM,NDMET,NDMET) , ESMETA(NDTEM,NDMET,NDMET) ,
     &           SORDA(NDTEM,NDLEV,NDMET) , ESORDA(NDTEM,NDLEV,NDMET) 
      REAL*8     DYIN(NTDSN)        ,
     &           LSCOM(NTDSN)       , LSGRD(NLTEM)
      real*8     v(ntdsn)
      real*8     redscef(ntdsn)     , redlscom(ntdsn)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTEM)       , LSSETA(NDMET,NDMET)
C-----------------------------------------------------------------------
      INTRINSIC  DLOG
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      SAVE
C------------------------------------------------------------------------
      IF (NLTEM.LT.NDTEM) STOP
     &                   ' B8SCOM ERROR: NDTEM > NLTEM. INCREASE NLTEM'
C-----------------------------------------------------------------------
      IOPT  = 0
C--------------------------------------------------------------------------
C  FIX UP IONISATION RATES FROM ADAS502 PASS BY SEPARATING INTO EXPONENTIAL
C  AND RESIDUAL PARTS
C--------------------------------------------------------------------------
         DO 50 I=1,NMET
           DO 45 J=1,NMET
             IF(LSSETA(I,J)) THEN
                 ISTART=0
   20            ISTART=ISTART+1
                 IF (SGRDA(ISTART,I,J).LE.ZERO) GO TO 20
                 DO 25 IT=MAXT,1,-1
                   ESGRDA(IT,I,J)= 1.4387998D0*
     &                             (-BWNOA(J)+WA(IMETR(I)))/TEA(IT)
                   IF(IT.LT.ISTART) THEN
                       SGRDA(IT,I,J)=SGRDA(ISTART,I,J)
                   ELSE
                       SGRDA(IT,I,J)=SGRDA(IT,I,J)/DEXP(ESGRDA(IT,I,J))
                   ENDIF
   25            CONTINUE
             ENDIF
   45      CONTINUE
   50    CONTINUE                              
C--------------------------------------------------------------------------
C  PASS THROUGH Z -> Z+1 IONISATION BLOCK FROM ADF04 FILE AND INTERPOLATE 
C  TO OUTPUT TEMPERATURES.  KEEP SEPARATION INTO EXPONENTIAL AND RESIDUAL
C  PARTS.  TABULATE METASTABLE AND ORDINARY IONISATIONS IN SEPARATE ARRAYS
C--------------------------------------------------------------------------
         DO 1 IC=1,ICNTS
            I=ISTRN(IC)
            ICAP=IS2A(IC)
            IP  =IABS(IS1A(IC))
C
            LMET=.FALSE.
            DO IMET=1,NMET
              IF(ICAP.EQ.IMETR(IMET)) THEN
                  LMET = .TRUE.
                  IMETS = IMET
              ENDIF
            ENDDO
            IF(.NOT.LMET) THEN
                DO IORD=1,NORD
                  IF(ICAP.EQ.IORDR(IORD))THEN
                      IORDS=IORD
                  ENDIF
                ENDDO
            ENDIF
C-----------------------------------------------------------------------
               DO 2 IT=1,NV
                  LSCOM(IT)=DLOG( SCOM(IT,I) )
    2          CONTINUE
C-----------------------------------------------------------------------
            nspln = 0
            do it = 1, nv
              redscef(it)  = 0.0
              redlscom(it) = 0.0
              if (SCOM(IT,I).LT.1.0D0) then
                 nspln = nspln + 1
                 redscef(nspln)  = scef(it)
                 redlscom(nspln) = DLOG( SCOM(IT,I) )
              endif
            end do
            lsetx=.TRUE.

            call xxsple( lsetx , iopt    , dlog      ,
     &                   nspln , redscef , redlscom  ,
     &                   maxt  , tea     , lsgrd     ,
     &                   dyin  , ltrng
     &                 )

C             CALL XXSPLE( LSETX , IOPT  , DLOG   ,
C      &                   NV    , SCEF  , LSCOM  ,
C      &                   MAXT  , TEA   , LSGRD ,
C      &                   DYIN  , LTRNG
C      &                 )
C-----------------------------------------------------------------------
               DO 3 IT=1,MAXT
                  IF(LMET) THEN 
                      ESMETA(IT,IMETS,IP) = 1.4387998D0*
     &                                     (-BWNOA(IP)+WA(ICAP))/TEA(IT)
                      SMETA(IT,IMETS,IP) = DEXP( LSGRD(IT) )
                  ELSE
                      ESORDA(IT,IORDS,IP) = 1.4387998D0*
     &                                     (-BWNOA(IP)+WA(ICAP))/TEA(IT)
                      SORDA(IT,IORDS,IP) = DEXP( LSGRD(IT) )
                  ENDIF
    3          CONTINUE
C-----------------------------------------------------------------------
c         if(lmet) then
c            write(i4unit(-1),*)'lmet,imets,ip=',lmet,imets,ip
c            write(i4unit(-1),*)'esmeta=',(esmeta(it,imets,ip),it=1,maxt)
c            write(i4unit(-1),*)'smeta=',(smeta(it,imets,ip),it=1,maxt)
c            write(i4unit(-1),*)'esgrda=',(esgrda(it,imets,ip),it=1,maxt)
c            write(i4unit(-1),*)'sgrda=',(sgrda(it,imets,ip),it=1,maxt)
c         else
c            write(i4unit(-1),*)'lmet,iords,ip=',lmet,iords,ip
c            write(i4unit(-1),*)'esorda=',(esorda(it,iords,ip),it=1,maxt)
c            write(i4unit(-1),*)'sorda=',(sorda(it,iords,ip),it=1,maxt)
c         endif
c
C-----------------------------------------------------------------------         
    1    CONTINUE
C-----------------------------------------------------------------------
C  MERGE SGRDA INTO SMETA WITH SGRDA HIGHER PRIORITY
C  RESET LSSETA TO INDICATE COMBINATION OF BOTH IONISATION RATE SOURCES
C-----------------------------------------------------------------------
         DO 10 I=1,NDMET
           DO 8 J=1,NDMET
c             write(i4unit(-1),*)'imet,ip=',i,j
             IF(SGRDA(1,I,J).GT.ZERO) THEN
                 DO 7 IT=1,MAXT
                   SMETA(IT,I,J)=SGRDA(IT,I,J)
                   ESMETA(IT,I,J)=ESGRDA(IT,I,J)
    7            CONTINUE
            ENDIF
            IF(SMETA(1,I,J).GT.0.0D0) LSSETA(I,J)=.TRUE.
c             do 6 it=1,maxt
c               v(it)=r8expe(smeta(it,i,j),esmeta(it,i,j),'ZERO')
c    6        continue
c             write(i4unit(-1),*)(esmeta(it,i,j),it=1,maxt)
c             write(i4unit(-1),*)(smeta(it,i,j),it=1,maxt)
c             write(i4unit(-1),*)(v(it),it=1,maxt)
    8      CONTINUE
   10    CONTINUE 
c         do 12 i=1,ndmet
c           write(i4unit(-1),*)(lsseta(i,j),j=1,ndmet)
c   12    continue
      RETURN
      END
