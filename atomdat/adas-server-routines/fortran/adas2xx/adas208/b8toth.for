C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8toth.for,v 1.1 2004/07/06 11:31:42 whitefor Exp $ Date $Date: 2004/07/06 11:31:42 $
C
      SUBROUTINE B8TOTH( NDLEV   , NDMET  , NDTEM , NDDEN ,
     &                   NORD    , NMET   , NPL   ,
     &                   IORDR   , IMETR  ,
     &                   IT      , MAXT   , IN    , MAXD  ,
     &                   RATPIA  ,
     &                   STVHM   , STVH   ,
     &                   PLA1    ,
     &                   PHA     , PH
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8TOTH *********************
C
C  PURPOSE: TO CALCULATE TOTAL CHARGE EXCHANGE DRIVEN LINE POWER.
C
C  NOTE: CODE EXECUTES FOR ONE TEMPERATURE AND DENSITY INDEX AT A TIME
C
C  CALLING PROGRAM:  ADAS208
C
C  SUBROUTINE:
C
C  INPUT : (I*4) NDLEV    = PARAMETER = MAX. NO. OF LEVELS ALLOWED
C  INPUT : (I*4) NDMET    = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C  INPUT : (I*4) NDTEM    = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C  INPUT : (I*4) NDDEN    = PARAMETER = MAX. NO. OF DENSITIES ALLOWED
C
C  INPUT : (I*4) NORD     = NUMBER OF ORD. LEVELS (1 <= NORD <= 'NDLEV')
C  INPUT : (I*4) NMET     = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C  INPUT : (I*4) NPL      = NUMBER OF PARENT METASTABLES (NPL<= 'NDMET')
C
C  INPUT : (I*4) IORDR()  = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                           LIST (ARRAY SIZE = 'NDLEV' )
C  INPUT : (I*4) IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C  INPUT : (I*4) IT       = CURRENT TEMPERATURE INDEX
C  INPUT : (I*4) MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C  INPUT : (I*4) IN       = CURRENT DENSITY INDEX
C  INPUT : (I*4) MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C  INPUT : (R*8) RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                            1ST DIMENSION: DENS INDEX
C                            2ND DIMENSION: PARENT INDEX
C
C  INPUT : (R*8) STVHM(,,,)= METASTABLE LEVEL:
C                           CHARGE-EXCHANGE RECOMBINATION POPUL. PART
C                           (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVH(,,,)= ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION POPUL. PART
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*8)  PLA1()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                           (UNITS: ERGS SEC-1) (DIMENSION: LEVEL INDEX)
C
C  OUTPUT: (R*8)  PH(,,)  = TOTAL CX LINE POWER FOR PARENT. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           PARENT.
C                            => P(TOTAL)/N(IP)          (ERGS SEC-1)
C                             1ST DIMENSION: PARENT METASTABL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C  OUTPUT: (R*8)  PHA(,)  =  EQUILIBRIUM CX POWER COEFFT.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C
C          (I*4) IM       = METASTABLE LEVEL ARRAY INDEX
C          (I*4) IS       = ORDINARY LEVEL ARRAY INDEX
C          (I*4) IP       = PARENT METASTABLE INDEX
C
C
C
C ROUTINES: NONE
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    24/05/96
C
C UPDATE:
C
C***********************************************************************
C PUT UNDER S.C.C.S CONTROL:
C
C VERSION: 1.1				DATE: 15/07/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER S.C.C.S
C 
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDMET         , NDLEV               ,
     &           NDTEM         , NDDEN               ,
     &           MAXT          , MAXD                ,
     &           NMET          , NORD                , NPL
      INTEGER    IM            , IS                  , IP             ,
     &           IT            , IN
C-----------------------------------------------------------------------
      INTEGER    IMETR(NMET)   , IORDR(NORD)
C-----------------------------------------------------------------------
      REAL*4     STVH(NDLEV,NDTEM,NDDEN,NDMET)
C-----------------------------------------------------------------------
      REAL*8     STVHM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           PLA1(NDLEV)                    ,
     &           PHA(NDTEM,NDDEN)               ,
     &           PH(NDTEM,NDDEN,NDMET)
      REAL*8     RATPIA(NDDEN,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
          PHA(IT,IN) = 0.0D0
          DO 3 IP=1,NPL
            PH(IT,IN,IP) = 0.0D0
            DO 1 IM=1,NMET
              PH(IT,IN,IP) = PH(IT,IN,IP) +
     &                       PLA1(IMETR(IM))*STVHM(IM,IT,IN,IP)
    1       CONTINUE
            DO 2 IS=1,NORD
              PH(IT,IN,IP) = PH(IT,IN,IP) +
     &                       ( PLA1(IORDR(IS)) * STVH(IS,IT,IN,IP) )
    2       CONTINUE
            PHA(IT,IN) = PHA(IT,IN) + ( PH(IT,IN,IP) * RATPIA(IN,IP))
    3     CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
