C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8popm.for,v 1.1 2004/07/06 11:30:30 whitefor Exp $ Date $Date: 2004/07/06 11:30:30 $
C
      SUBROUTINE B8POPM( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   NPL    , NPLR  , NPLI  ,
     &                   MAXT   , MAXD  , NMET  ,
     &                            DENSA , IMETR ,
     &                            LRSEL , LISEL , LHSEL ,
     &                            RATPIA, RATMIA, RATHA ,
     &                   STCKM  , STVRM , STVIM , STVHM ,
     &                   POPAR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8POPM *********************
C
C  PURPOSE: TO CONSTRUCT METASTABLE LEVEL POPULATIONS.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NPL     = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                              BY EXCITED STATE IONISATION IN COPASE
C                              FILE WITH IONISATION POTENTIALS GIVEN
C                              ON THE FIRST DATA LINE
C  INPUT :  (I*4)  NPLR    = NO. OF ACTIVE METASTABLES OF (Z+1) ION
C  INPUT :  (I*4)  NPLI    = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C
C  INPUT :  (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 ->'NDTEM')
C  INPUT :  (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 ->'NDDEN')
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES LEVELS ( 1 ->'NDMET')
C
C  INPUT :  (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C
C  INPUT :  (L*4)  LRSEL   = .TRUE.  => FREE ELECTRON RECOMBINATION
C                                       REQUESTED.
C                          = .FALSE. => FREE ELECTRON RECOMBINATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN REQUESTED.
C                          = .FALSE. => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN NOT REQUESTED.
C  INPUT :  (L*4)  LISEL   = .TRUE.  => IONISATION FROM LOWER IONIS.
C                                       STAGE REQUESTED.
C                          = .FALSE. => IONISATION FROM LOWER IONIS.
C                                       STAGE NOT REQUESTED.
C
C  INPUT :  (R*8)  RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION: TEMP/DENS INDEX
C                               2ND DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  RATMIA(,)= RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION: TEMP/DENS INDEX
C                               2ND DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C
C  INPUT :  (R*8)  STCKM(,,)= METASTABLE POPULATIONS STACK:
C                               1st DIMENSION: METASTABLE INDEX
C                               2nd DIMENSION: TEMPERATURE INDEX
C                               3rd DIMENSION: DENSITY INDEX
C  INPUT :  (R*8)  STVRM(,,,)= METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                               1ST DIMENSION: METASTABLE INDEX
C                               2ND DIMENSION: TEMPERATURE INDEX
C                               3RD DIMENSION: DENSITY INDEX
C                               4TH DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  STVIM(,,,)= METASTABLE LEVEL:
C                             ELECTRON IMPACT IONISATION  COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                               1ST DIMENSION: METASTABLE INDEX
C                               2ND DIMENSION: TEMPERATURE INDEX
C                               3RD DIMENSION: DENSITY INDEX
C                               4TH DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  STVHM(,,,)= METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                              1st DIMENSION: METASTABLE INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C                              4TH DIMENSION: PARENT INDEX
C
C  OUTPUT:  (R*8)  POPAR(,,)= LEVEL POPULATIONS
C                             1ST DIMENSION: LEVEL INDEX
C                             2ND DIMENSION: TEMPERATURE INDEX
C                             3RD DIMENSION: DENSITY INDEX
C                             (ON OUTPUT CONTAINS POPULATIONS FOR
C                              METASTABLE LEVELS ONLY.)
C
C           (R*8) DCOEF     = DENSITY MULTIPLIED BY RELEVANT RATIOS  FOR
C                             CALCULATING RECOMBINATION CONTRIBUTIONS.
C
C           (I*4) IT        = TEMPERATURE ARRAY INDEX
C           (I*4) IP        = PARENT INDEX
C           (I*4) IN        = DENSITY ARRAY INDEX
C           (I*4) IM        = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C AUTHOR:  HP SUMMERS (UPGRADE OF BXPOPM BY PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/06/92
C
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM        , NDDEN        , NDMET   , NDLEV   ,
     &          MAXT         , MAXD         , NMET    ,
     &          NPL          , NPLR         , NPLI
      INTEGER   IT           , IN           , IM      , IP
C-----------------------------------------------------------------------
      REAL*8    DCOEF
C-----------------------------------------------------------------------
      LOGICAL   LRSEL        , LHSEL        , LISEL
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDDEN)                ,
     &          RATPIA(NDDEN,NDMET)         , RATMIA(NDDEN,NDMET) ,
     &          RATHA(NDDEN)
      REAL*8    STCKM(NDMET,NDTEM,NDDEN)   ,
     &          STVRM(NDMET,NDTEM,NDDEN,NDMET) ,
     &          STVIM(NDMET,NDTEM,NDDEN,NDMET),
     &          STVHM(NDMET,NDTEM,NDDEN,NDMET)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C METASTABLE LEVEL POPULATIONS.
C-----------------------------------------------------------------------
C
         DO 1 IN=1,MAXD
            DO 2 IT=1,MAXT
               DO 3 IM=1,NMET
                  POPAR(IMETR(IM),IT,IN)=STCKM(IM,IT,IN)
    3          CONTINUE
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD FREE ELECTRON RECOMB. CONTRIBUTION TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            DO 7 IN=1,MAXD
              DO 6 IP=1,NPLR
                DCOEF = RATPIA(IN,IP)*DENSA(IN)
                DO 5 IT=1,MAXT
                  DO 4 IM=1,NMET
                    POPAR(IMETR(IM),IT,IN)=POPAR(IMETR(IM),IT,IN) +
     &                                 ( DCOEF*STVRM(IM,IT,IN,IP))
    4             CONTINUE
    5           CONTINUE
    6         CONTINUE
    7       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD IONISATION FROM LOWER ION STAGE TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LISEL) THEN
            DO 11 IN=1,MAXD
              DO 10 IP=1,NPLI
                DCOEF = RATMIA(IN,IP)*DENSA(IN)
                DO 9 IT=1,MAXT
                  DO 8 IM=1,NMET
                    POPAR(IMETR(IM),IT,IN)=POPAR(IMETR(IM),IT,IN) +
     &                                 ( DCOEF*STVIM(IM,IT,IN,IP))
    8             CONTINUE
    9           CONTINUE
   10         CONTINUE
   11       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD CHARGE EXCHANGE RECOMB. CONTRIBUTION TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            DO 15 IN=1,MAXD
              DO 14 IP=1,NPLR
                DCOEF = RATHA(IN)*RATPIA(IN,IP)*DENSA(IN)
                DO 13 IT=1,MAXT
                  DO 12 IM=1,NMET
                    POPAR(IMETR(IM),IT,IN)=POPAR(IMETR(IM),IT,IN) +
     &                                      ( DCOEF*STVHM(IM,IT,IN,IP) )
   12             CONTINUE
   13           CONTINUE
   14         CONTINUE
   15       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
