C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8mcca.for,v 1.1 2004/07/06 11:29:47 whitefor Exp $ Date $Date: 2004/07/06 11:29:47 $
C
      SUBROUTINE B8MCCA( NDLEV  , IL     ,
     &                   LPSEL  , LISEL  , LPDATA,
     &                   DENE   , DENP   ,
     &                   CRA    , PCC    ,
     &                   CRCE   , CRCP   , CIE   ,
     &                   CC
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8MCCA *********************
C
C  PURPOSE: TO  CONSTRUCT WHOLE RATE MATRIX 'CC' FOR TRANSITIONS BETWEEN
C           ALL ENERGY LEVELS AT A FIXED TEMPERATURE  AND  GIVEN DENSITY
C           'DENE/DENP'.
C
C          UPDATED VERSION OF BXMCCA, TO INCORPORATE INDIRECT COUPLINGS
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  IL      = NUMBER OF ENERGY LEVELS
C
C  INPUT :  (L*4)  LPSEL   = .TRUE.  => INCLUDE PROTON COLLISIONS
C                            .FALSE. => DO NOT INCLUDE PROTON COLLISIONS
C  INPUT :  (L*4)  LISEL   = .TRUE.  => INCLUDE IONISATION RATES
C                            .FALSE. => DO NOT INCLUDE IONISATION RATES
C  INPUT :  (L*4)  LPDATA  = .TRUE.  => INCLUDE INDIRECT COUPLINGS
C                            .FALSE. => DO NOT INCLUDE INDIRECT COUPLING
C
C  INPUT :  (R*8)  DENE    = ELECTRON DENSITY (UNITS: CM-3)
C  INPUT :  (R*8)  DENP    = PROTON DENSITY (UNITS: CM-3)
C
C  INPUT :  (R*8)  CRA(,)  = A-VALUE (sec-1)  MATRIX  COVERING   ALL
C                            TRANSITIONS.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C
C  INPUT :  (R*8)  PCC(,)  = INDIRECT RATE MATRIX COVERING ALL
C                            TRANSITIONS (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C
C  INPUT :  (R*8)  CRCE(,) = ELECTRON IMPACT TRANSITIONS:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C  INPUT :  (R*8)  CRCP(,) = PROTON IMPACT TRANSITIONS:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C  INPUT :  (R*8)  CIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                            FIXED TEMPERATURE.
C                            DIMENSION: ENERGY LEVEL INDEX
C
C  OUTPUT:  (R*8)  CC(,)   = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C
C           (I*4)  IS1     = ENERGY LEVEL ARRAY INDEX
C           (I*4)  IS2     = ENERGY LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C
C AUTHOR:  WILLIAM J. DICKSON (MOSTLY COPIED FROM BXMCCA)
C          K1/1/36
C          JET EXT. 5057
C
C DATE:    06/01/92
C
C UNIX-IDL PORT:
C
C DATE: UNKNOWN
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV              , IL
      INTEGER    IS1                , IS2
C-----------------------------------------------------------------------
      REAL*8     DENE               , DENP
C-----------------------------------------------------------------------
      LOGICAL    LPSEL              , LISEL              , LPDATA
C-----------------------------------------------------------------------
      REAL*8     CRA(NDLEV,NDLEV)   , pcc(NDLEV,NDLEV)   ,
     &           CRCE(NDLEV,NDLEV)  , CRCP(NDLEV,NDLEV)  ,
     &           CIE(NDLEV)
      REAL*8     CC(NDLEV,NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ADD ELECTRON IMPACT COMPONENT
C-----------------------------------------------------------------------
C
         DO 1 IS1=1,IL
               DO 2 IS2=1,IL
                  CC(IS2,IS1) = CRA(IS2,IS1) + ( DENE * CRCE(IS2,IS1) )
    2          CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD PROTON IMPACT COMPONENT - IF REQUIRED
C-----------------------------------------------------------------------
C
         IF (LPSEL) THEN
            DO 3 IS1=1,IL
                  DO 4 IS2=1,IL
                    CC(IS2,IS1) = CC(IS2,IS1) + ( DENP * CRCP(IS2,IS1) )
    4             CONTINUE
    3       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD IONISATION RATE COMPONENT - IF REQUIRED
C-----------------------------------------------------------------------
C
         IF (LISEL) THEN
            DO 5 IS1=1,IL
               CC(IS1,IS1) = CC(IS1,IS1) - ( DENE * CIE(IS1) )
    5       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD INDIRECT COUPLINGS - IF REQUIRED
C-----------------------------------------------------------------------
C
         IF (LPDATA) THEN
            DO 6 IS1=1,IL
                  DO 7 IS2=1,IL
                    CC(IS2,IS1) = CC(IS2,IS1) - PCC(IS2,IS1)
    7             CONTINUE
    6       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
