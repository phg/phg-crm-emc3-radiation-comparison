
C
      FUNCTION R8NECIP( IZ , XI , ZETA , TE , ALFRED)
      IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C  **************** FORTRAN77 REAL*8 FUNCTION: R8NECIP *****************
C
C  PURPOSE: EVALUATES A SHELL CONTRIBUTION TO THE IONISATION RATE
C           COEFFICIENT IN THE ECIP APPROXIMATION, AND RETURNS
C           THE THREE-BODY RECOMBINATION COEFFICIENT IN REDUCED
C           FORM IE. OMITTING THE WEIGHTING FACTOR W(I)/W+
C
C  CF. SUMMERS (1974)APPLETON LABORATORY REPORT IM367
C
C
C  BASED UPON ADASXX20(R8NBCH) AND COPASE(RECIP2)
C
C  CALLING PROGRAM: GENERAL USE
C
C  FUNCTION:
C          (R*8)  R8NECIP = FUNCTION NAME
C          (I*4)  IZ      = TARGET ION CHARGE NUMBER
C                           (RECOMBINED ION CHARGE).
C          (R*8)  XI      = EFFECTIVE IONISATION POTENTIAL FOR SHELL
C                           (UNITS: RYDBERGS)
C                           (LEVEL ENERGY RELATIVE TO IONISATION POT.)
C          (R*8)  ZETA    = EFFECTIVE NUMBER OF EQUIVALENT ELECTRONS
C                           IN SHELL
C          (R*8)  TE      = ELECTRON TEMPERATURE (IN KELVIN)
C          (R*8)  ALFRED  = THREE-BODY COEFFICIENT*W+/W(I)  (CM**6 S-1)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8YIP      ADAS      FUNCTION:
C
C
C AUTHOR:   W.J. DICKSON
C           K1/1/36
C           JET EXT. 5057
C
C DATE:     05/01/93
C
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1                          DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C           - FIRST PUT UNDER SCCS
C
C VERSION  : 1.2
C DATE     : 16-02-2006
C MODIFIED : Martin O'Mullane
C               - Removed mainframe listing information beyond column 72.
C
C VERSION  : 1.3
C DATE     : 20-07-2007
C MODIFIED : Allan Whiteford
C               - Small modification to comments to allow for automatic
C                 documentation preparation.
C-----------------------------------------------------------------------
      PARAMETER( CL = 2.3D0        , DXIPOW = 1.5D0      ,
     &           TK2ATE = 1.5789D5 , R2GAM  = 2.17161D-8 )
      PARAMETER( D150   = 1.50D2   )
      PARAMETER( CR2GAM = CL*R2GAM , CALF = 3.30048D-24 )
C-----------------------------------------------------------------------
       DIMENSION X(5),W(5)
C
        DATA X / 0.26356 , 1.41340 , 3.59643 , 7.08581 ,
     &                       12.64080  /
        DATA W / 0.521756   , 0.398667   , 0.0759424 ,
     &                       0.00361176 , 0.00002337 /
C-----------------------------------------------------------------------
C
       Z     = DFLOAT( IZ+1 )
       ATE   = TK2ATE/TE
       EN    = Z/DSQRT(XI)
       Y     = XI*ATE
       IZETA = ZETA
       J1    = 5
       AI    = 0.0
C
       DO 1 J=1,J1
         V     =  X(J)
         B     =  V/Y
         B1    =  B+1.0
         C     =  DSQRT(B1)
         R     =  (1.25*EN*EN+0.25)/Z
         DELTA =  (Z/EN)*(R+2.0*EN*EN*C/((B+2.0)*Z*Z))/(C+DSQRT(B))
         C1    =  1.0/(B+2.0)
         F     =  1.0
         C4    =  R8YIP(0.0D0,DELTA)
         C2  =(C1*(B-B1*C1*DLOG(B1))+0.65343*(1.0-1.0/(B1**3))*C4/EN)*F
         AI    =  AI+W(J)*C2
         Q     =  4.0*C2/B1
    1  CONTINUE
C
       ALFRED = CALF * (ATE ** 1.5) * 8.68811D-8*DSQRT(ATE)*ZETA*AI/XI
C
       IF( Y .LT. D150 ) THEN
           R8NECIP = 8.68811D-8*DSQRT(ATE)*DEXP(-Y)*ZETA*AI/XI
       ELSE
           R8NECIP = 0.0D0
       ENDIF
C
       RETURN
C
      END
