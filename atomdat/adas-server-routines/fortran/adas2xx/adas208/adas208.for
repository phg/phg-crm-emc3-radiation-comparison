       PROGRAM adas208
       
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: ADAS208 *********************
C
C  ORIGINAL NAME: SPFPOPN
C                 (DEVELOPED FROM THE ORIGINAL PROGRAM: SMETPOP)
C
C  VERSION:  1.0
C
C  PURPOSE:  TO  CALCULATE LOW-LEVEL POPULATIONS OF EXCITED STATES OF
C            IONS, INCLUDING DEPENDENCE ON METASTABLES,  USING MASTER
C            CONDENSED FILES WHICH CONTAIN ELECTRON IMPACT RATE  DATA
C            (GAMMA VALUES) AS A FUNCTION OF TEMPERATURE. THESE FILES
C            MAY ALSO CONTAIN INFORMATION ON PROTON IMPACT TRANSITIONS
C            AND RECOMBINATION COEFFICIENTS (FREE ELECTRON AND  CHARGE
C            EXCHANGE).
C
C            PROCESSES CAN INCLUDE ELECTRON AND PROTON IMPACT,  SPON-
C            TANEOUS EMISSION, FREE ELECTRON RECOMBINATION AND CHARGE
C            EXCHANGE RECOMBINATION DEPENDING ON THE INPUT DATA SET.
C
C
C  DATA:     THE SOURCE DATA IS CONTAINED IN PARTITIONED DATASETS AS
C            FOLLOWS:-
C
C                    'JETSHP.<SE>LIKE.DATA(<MEMBER>)'
C
C            WHERE, <SE> = ELEMENT SYMBOL FOR ISO-ELECTRON SERIES
C                          (ONE OR TWO CHARACTERS)
C
C            THE <MEMBER> NAME IS CONSTRUCTED AS FOLLOWS:
C
C               <INITIALS OF DATA SOURCE><YEAR><ELEMENT SYMBOL><*>
C
C               E.G. HPS89BE,  BKT1988C
C
C               <INITIALS OF DATA SOURCE> - E.G. HPS = HP SUMMERS
C
C               <YEAR> - THIS MAY OR MAY NOT BE ABBREVIATED
C
C               IF <ELEMENT SYMBOL> IS PRESENT =>
C                                       SPECIFIC  ION  FILE.  ONE
C                                       ELEMENT ONLY.
C                                           E.G. BE = BERYLIUM
C                                                C  = CARBON
C               IF <ELEMENT SYMBOL> IS MISSING =>
C                                       GENERAL Z EXCITATION FILES
C                                       CONTAINING BETWEEN 2 AND 7
C                                       ELEMENTS (INCLUSIVE). THIS
C                                       PROGRAM WILL  NOT  ANALYSE
C                                       THESE  FILES,  THEY   MUST
C                                       PASS THROUGH 'SPFMODM2' OR
C                                       AN    EQUIVALENT   PROGRAM
C                                       FIRST.
C
C               <*> - FURTHER EXTENSIONS TO MEMBER NAME MAY BE PRESENT,
C                    THESE MAY REFER TO DATA SET GENERATION STATISTICS.
C
C           EACH VALID MEMBER HAS ITS DATA REPRESENTED IN AN ABBREVIATED
C           FORM WHICH OMITS THE "D" OR "E" EXPONENT SPECIFIER.
C           e.g. 1.23D-06 or 1.23E-06 IS REPRESENTED AS 1.23-06
C                6.75D+07 or 6.75E+07 IS REPRESENTED AS 6.75+07
C
C           THEREFORE THE FORM OF EACH REAL NUMBER IN THE DATA SET IS:
C                          N.NN+NN or N.NN-NN
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C           NEUTRAL BEAM ENERGY :
C           RATE COEFFICIENTS   : CM3 SEC-1
C
C
C  PROGRAM:
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C          (I*4)  NDDEN   = PARAMETER = MAX. NUMBER OF DENSITIES ALLOWED
C          (I*4)  NDMET   = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C          (I*4)  NDSEL   = PARAMETER = MAX. NO. OF USER SUPPLIED TRANSITONS
C
C          (I*4)  IUNT27  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR COPASE DATA SET
C          (I*4)  IUNT11  = PARAMETER = OUTPUT UNIT FOR CONTOUR DATA SET
C          (I*4)  IUNT12  = PARAMETER = OUTPUT UNIT FOR MET.POPULATIONS
C                                       PASSING FILE.
C          (I*4)  IUNT13  = PARAMETER = OUTPUT UNIT FOR GEN. COLL. RAD.
C                                       INFORMATION PASSING FILE.
C          (I*4)  IUNT14  = PARAMETER = OUTPUT UNIT FOR ACD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT15  = PARAMETER = OUTPUT UNIT FOR SCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT16  = PARAMETER = OUTPUT UNIT FOR CCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT17  = PARAMETER = OUTPUT UNIT FOR QCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT18  = PARAMETER = OUTPUT UNIT FOR XCD COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT19  = PARAMETER = OUTPUT UNIT FOR PRB COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT20  = PARAMETER = OUTPUT UNIT FOR PRC COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT21  = PARAMETER = OUTPUT UNIT FOR PLT COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT22  = PARAMETER = OUTPUT UNIT FOR PLS COLL. RAD.
C                                       PASSING FILE.
C          (I*4)  IUNT23  = PARAMETER = OUTPUT UNIT FOR MET COLL. RAD.
C                                       PASSING FILE.
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  ICSTMX  = PARAMETER = 11 =
C                           MAXIMUM SIZE OF NOMENCLATURE STRING OUTPUT
C                           FOR PANELS AND PASSING DATA SETS.(see CSTRGB)
C
C          (R*8)  D1      = PARAMETER = 1.0D0
C          (R*8)  WN2RYD  = PARAMETER = 9.11269D-06
C
C          (C*1)  PRGTYP  = PARAMETER = PROGRAM TYPE = 'M' (METPOP)
C
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  ICNTI   = NO. OF IONISATIONS TO Z INPUT
C          (I*4)  ICNTL   = NO. OF SATELLITE DR RECOMBINATIONS INPUT
C          (I*4)  ICNTS   = NO. OF IONISATIONS TO Z+1 INPUT
C          (I*4)  IDOUT   = 1 => INPUT DENSITIES IN CM-3
C                         = 2 => INPUT DENSITIES IN REDUCED FORM
C          (I*4)  IFOUT   = 1 => INPUT TEMPERATURES IN KELVIN
C                         = 2 => INPUT TEMPERATURES IN EV
C                         = 3 => INPUT TEMPERATURES IN REDUCED FORM
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C          (I*4)  ITSEL   = INDEX OF TEMPERATURE SELECTED FOR GRAPH
C                           (FROM INPUT LIST).
C          (I*4)  IZ      =  RECOMBINED ION CHARGE
C          (I*4)  IZ0     =         NUCLEAR CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C          (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C          (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C          (I*4)  NPL     = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                             BY EXCITED STATE IONISATION IN COPASE
C                             FILE WITH IONISATION POTENTIALS GIVEN
C                             ON THE FIRST DATA LINE
C          (I*4)  NPLR    = NO. OF ACTIVE METASTABLES OF (Z+1) ION
C          (I*4)  NPLI    = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C          (I*4)  NPL3    = NO. OF ACTIVE METASTABLES OF (Z+1) ION  WITH
C                             THREE-BODY RECOMBINATION ON.
C          (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  I       = GENERAL PURPOSE INDEX
C          (I*4)  J       = GENERAL PURPOSE INDEX
C          (I*4)  IP      = PARENT INDEX
C          (I*4)  IS      = ENERGY LEVEL ARRAY INDEX
C          (I*4)  IN      = DENSITY ARRAY INDEX
C          (I*4)  IT      = TEMPERATURE ARRAY INDEX
C          (I*4)  ININ    = DENSITY ARRAY INDEX
C          (I*4)  ITIN    = TEMPERATURE ARRAY INDEX
C
C          (R*8)  R8DCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  R8FBCH  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1) OF LOWEST PARENT
C          (R*8)  BWNOA() = IONISATION POTENTIAL (CM-1) OF PARENTS
C          (L*4)  LBSETA()= .TRUE. - PARENT WEIGHT SET FOR BWNOA()
C                         = .FALSE.- PARENT WEIGHT NOT SET FOR BWNOA()
C          (R*8)  PRTWTA()= WEIGHT FOR PARENT ASSOCIATED WITH BWNOA()
C          (R*8)  ZEFF    = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C          (R*8)  ZEFFSQ  = 'ZEFF' * 'ZEFF'
C          (R*8)  DMINT   = +1 or -1 DEPENDING ON WHETHER THE  NUMBER OF
C                           ROW   INTERCHANGES   WAS   EVEN   OR    ODD,
C                           RESPECTIVELY, WHEN INVERTING A MATRIX  USING
C                           'XXMINV'.
C          (R*8)  XL1IN   = LOWER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: SEE 'IDOUT')
C          (R*8)  XU1IN   = UPPER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: SEE 'IDOUT')
C          (R*8)  XL1     = LOWER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: CM-3)
C          (R*8)  XU1     = UPPER LIMIT FOR X-AXIS OF GRAPH
C                           (UNITS: CM-3)
C          (R*8)  YL1     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C          (R*8)  YU1     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (R*8)  WVLS    = SHORT WAVELENGTH LIMIT FOR PEC & SXB (A)
C          (R*8)  WVLL    = LONG WAVELENGTH LIMIT FOR PEC & SXB  (A)
C          (R*8)  AVLT    = LOWER LIMIT OF A-VALUES FOR PEC & SXB
C
C          (L*4)  LSOLVE  = .TRUE.  => SOLVE LINEAR EQUATION USING
C                                      'XXMINV'.
C                           .FALSE. =>DO NOT SOLVE LINEAR EQUATION USING
C                                     'XXMINV' - INVERT MATRIX ONLY.
C          (L*4)  LCONT   = .TRUE.  => OUTPUT DATA TO CONTOUR PASSING
C                                      FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      CONTOUR PASSING FILE.
C          (L*4)  LPASS   = .TRUE.  => OUTPUT DATA TO METPOP PASSING
C                                      FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      METPOP PASSING FILE.
C          (L*4)  LGCR    = .TRUE.  => OUTPUT DATA TO GEN. COLL. RAD.
C                                      PASSING FILE
C                           .FALSE. => NO OUTPUT TO GEN. COLL. RAD.
C                                      PASSING FILE.
C          (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LGPH    = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                         = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                         = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LNORM   = .TRUE.  => IF NMET=1 NORMALISE TOTAL AND
C                                      SPECIFIC LINE POWER OUTPUT FILES
C                                      PLT & PLS TO STAGE TOT. POPULATN.
C                         = .FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                      METASTABLE POPULATIONS.
C          (L*4)  LPSEL   = .TRUE.  => INCLUDE PROTON COLLISIONS
C                         = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C          (L*4)  LZSEL   = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                      PLASMA Z EFFECTIVE'ZEFF'.
C                         = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                      WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                         (ONLY USED IF 'LPSEL=.TRUE.')
C          (L*4)  LIOSEL  = .TRUE.  => INCLUDE IONISATION RATES
C                         = .FALSE. => DO NOT INCLUDE IONISATION RATES
C          (L*4)  LHSEL   = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                      NEUTRAL HYDROGREN.
C                         = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                      FROM NEUTRAL HYDROGREN.
C          (L*4)  LRSEL   = .TRUE.  => INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C          (L*4)  LISEL   = .TRUE.  => INCLUDE ELECTRON IMPACT
C                                      IONISATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C          (L*4)  LNSEL   = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                      FROM DATAFILE IF AVAILABLE
C                         = .FALSE. => DO NOT INCLUDE PROJECTED BUNDLE-N
C                                      DATA
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SETS
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SETS
C          (L*4)  LPDATA  = .TRUE.  => PROJECTED DATA EXISTS AND IS SET
C                         = .FALSE. => PROJECTED DATA DOES NOT EXSIST
C                                      OR IS NOT SET
CX         (L*4)  OPEN27   = .TRUE.  => FILE ALLOCATED TO UNIT 7.
CX                        = .FALSE. => NO FILE ALLOCATED TO UNIT 7.
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  OPEN11  = .TRUE.  => FILE ALLOCATED TO UNIT 11.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 11.
C          (L*4)  OPEN12  = .TRUE.  => FILE ALLOCATED TO UNIT 12.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 12.
C          (L*4)  OPEN13  = .TRUE.  => FILE ALLOCATED TO UNIT 13.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 13.
C          (L*4)  OPEN14  = .TRUE.  => FILE ALLOCATED TO UNIT 14.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 14.
C          (L*4)  OPEN15  = .TRUE.  => FILE ALLOCATED TO UNIT 15.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 15.
C          (L*4)  OPEN16  = .TRUE.  => FILE ALLOCATED TO UNIT 16.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 16.
C          (L*4)  OPEN17  = .TRUE.  => FILE ALLOCATED TO UNIT 17.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 17.
C          (L*4)  OPEN18  = .TRUE.  => FILE ALLOCATED TO UNIT 18.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 18.
C          (L*4)  OPEN19  = .TRUE.  => FILE ALLOCATED TO UNIT 19.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 19.
C          (L*4)  OPEN20  = .TRUE.  => FILE ALLOCATED TO UNIT 20.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 20.
C          (L*4)  OPEN21  = .TRUE.  => FILE ALLOCATED TO UNIT 21.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 21.
C          (L*4)  OPEN22  = .TRUE.  => FILE ALLOCATED TO UNIT 22.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 22.
C          (L*4)  OPEN23  = .TRUE.  => FILE ALLOCATED TO UNIT 23.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 23.
C
C          (C*1)  CPLA()  = CHAR. SPECIFYING 1ST PARENT FOR LEVEL 'IA()'
C                                INTEGER - PARENT IN BWNOA() LIST
C                                'BLANK' - PARENT BWNOA(1)
C                                  'X'   - DO NOT ASSIGN A PARENT
C          (I*4)  NPLA()  = NO. OF PARENT/ZETA CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C          (I*4)  IPLA(,) = PARENT INDEX FOR CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (I*4)  ZPLA(,) = EFF. ZETA PARAM. FOR CONTRIBUTIONS TO IONIS.
C                           OF LEVEL
C                           1ST DIMENSION: PARENT INDEX
C                           2ND DIMENSION: LEVEL INDEX
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                         = 'NO ' => CONTINUE PROGRAM EXECUTION.
C          (C*3)  TITLED  = ELEMENT SYMBOL.
C          (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C          (C*10)  UID    = USER IDENTIFIER
C          (C*30) USER    = PRODUCERS'S REAL NAME
C          (C*80) DSNOUT  = OUTPUT CONTOUR DATA SET NAME (SEQUENTIAL)
C          (C*80) DSNPAS  = OUTPUT PASSING FILE NAME (SEQUENTIAL)
C          (C*26) DSNGCR  = OUTPUT GCR PASSING FILE NAME (SEQUENTIAL)
C          (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C          (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C          (C*44) DSNINP  = INPUT PROTON DATA SET NAME (SEQUENTIAL)
C                                      NOT USED AT PRESENT
C          (C*44) DSNEXP  = EXPANSION DATA SET NAME
C          (C*44) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C          (C*44) DSN44   = 44 BYTE CHARACTER STRING  FOR  TEMPORARY
C                           STORAGE OF DATA SET NAMES FOR DYNAMIC ALLOC.
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IITRN() = ELECTRON IMPACT IONISATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT IONISATIONS FROM THE LOWER STAGE
C          (I*4)  ILTRN() = SATELLITE DR RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT SATELLITE DR RECOMBINATIONS.
C          (I*4)  ISTRN() = ELECTRON IMPACT IONISATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT IONISATIONS TO UPPER STAGE ION.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (I*4)  IA1A()  = AUGER TRANSITION:
C                            PARENT ENERGY LEVEL INDEX
C          (I*4)  IA2A()  = AUGER TRANSITION:
C                            RECOMBINED ION ENERGY LEVEL INDEX
C          (R*8)  AUGA()  = AUGER TRANSITION: AUG-VALUE (SEC-1)
C                            RECOMBINED ION ENERGY LEVEL INDEX
C          (I*4)  IL1A()  = SATELLITE DR TRANSITION:
C                            RECOMNINING ION  INDEX
C          (I*4)  IL2A()  = SATELLITE DR TRANSITION:
C                            RECOMBINED ION INDEX
C          (R*8)  WVLA()  = SATELLITE DR TRANSITION: PARENT WVLGTH.(A)
C                            DR SATELLITE LINE INDEX
C          (I*4)  IS1A()  = IONISING TRANSITION:
C                            IONISED ION  INDEX
C          (I*4)  IS2A()  = IONISING TRANSITION:
C                            IONISING ION INDEX
C
C          (I*4)  IBSELA(,)=IONISATION DATA BLOCK SELECTION INDICES
C                            1ST DIMENSION - (Z) ION METASTABLE COUNT
C                            2ND DIMENSION - (Z+1) ION METASTABLE COUNT
C
C          (R*8)  PAR(,)  =
C          (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  AA()    = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  TINE()  = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C          (R*8)  TINP()  = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C          (R*8)  TINH()  = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C          (R*8)  TEVA()  = ELECTRON TEMPERATURES (UNITS: EV)
C          (R*8)  TPVA()  = PROTON TEMPERATURES   (UNITS: EV)
C          (R*8)  THVA()  = NEUTRAL HYDROGEN TEMPERATURES (UNITS: EV)
C          (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C          (R*8)  TPA()   = PROTON TEMPERATURES   (UNITS: KELVIN)
C          (R*8)  THA()   = NEUTRAL HYDROGEN TEMPERATURES (UNITS:KELVIN)
C          (R*8)  DINE()  = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C          (R*8)  DINP()  = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C          (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C          (R*8)  DENSPA()= PROTON DENSITIES    (UNITS: CM-3)
C          (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C          (R*8)  RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION: TEMP/DENS INDEX
C                               2ND DIMENSION: PARENT INDEX
C          (R*8)  RATMIA(,)= RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION: TEMP/DENS INDEX
C                               2ND DIMENSION: PARENT INDEX
C          (R*8)  RHS()   = USED ONLY IF 'LSOLVE=.TRUE.' WHEN CALLING
C                           THE SUBROUTINE 'XXMINV'. CONTAINS THE SET
C                           OF 'N' LINEAR EQUATIONS TO BE SOLVED.
C                           INPUT  TO   'XXMINV': RIGHT HAND SIDE VECTOR
C                           OUTPUT FROM 'XXMINV': SOLUTION VECTOR
C                           (ACTS ONLY AS A DUMMY IN THIS PROGRAM)
C          (R*8)  CIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                           FIXED TEMPERATURE.
C                            DIMENSION: ENERGY LEVEL INDEX
C                             DIRECT  AND INDIRECT COUPLINGS
C          (R*8)  CIEPR(,)= PARENT RESOLVED IONISATION RATE COEFFICIENT
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT  AND INDIRECT COUPLINGS
C          (R*8)  DCIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                           FIXED TEMPERATURE.
C                             DIRECT COUPLING ONLY
C                            DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  DCIEPR(,)= PARENT RESOLVED IONISATION RATE COEFFT.
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT COUPLING ONLY
C          (R*8)  V3PR(,) = PARENT RESOLVED THREE-BODY RECOM. COEFFT.
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT  AND INDIRECT COUPLINGS
C          (R*8)  DV3PR(,)= PARENT RESOLVED THREE-BODY RECOM. COEFFT.
C                           ARRAY FOR FIXED TEMPERATURE.
C                            1ST DIMENSION: ENERGY LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C                             DIRECT  COUPLING ONLY
C          (R*8)  VHRED(,)= CHARGE EXCHANGE RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C          (R*8)  VRRED(,)= FREE ELECTRON RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C          (R*8)  VIRED(,)= ELECTRON IMPACT IONISATION FOR STAGE (Z-1):
C                           VECTOR  OF IONISATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: (Z-1) PARENT INDEX
C          (R*8)  VIONR(,)= ELECTRON IMPACT IONISATION FROM STAGE (Z):
C                           VECTOR  OF IONISATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: (Z+1) PARENT INDEX
C
C          (R*8)  SGRDA(,,)= GROUND AND METASTABLE IONISATION RATE COEFF
C                            RETURNED FROM IONELEC FILE TO BE USED IN
C                            EVALUATING THE PHOTON EFFICIENCY.
C                            1ST DIMENSION: TEMPERATURES
C                            2ND DIMENSION: (Z)   ION METASTABLE INDEX
C                            3ND DIMENSION: (Z+1) ION METASTABLE INDEX
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  EXCRE(,) = ELECTRON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRE(,)= ELECTRON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  EXCRP(,) = PROTON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRP(,)= PROTON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  VECH(,,) = CHARGE-EXCHANGE RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C          (R*8)  VECR(,,) = FREE ELECTRON RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C                              DIRECT AND INDIRECT COUPLINGS
C          (R*8)  DVECR(,,) = FREE ELECTRON RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C                              DIRECT COUPLING ONLY
C          (R*8)  VECI(,,) = ELECTRON IMPACT IONISATION:
C                            SPLINED IONISATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3ND DIMENSION: PARENT INDEX.
C          (R*8)  CRA(,)   = A-VALUE  (sec-1)  MATRIX  COVERING  ALL
C                            TRANSITIONS.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCE(,)  = ELECTRON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCP(,)  = PROTON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CC(,)    = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  CMAT(,)  = (INVERTED)   RATE  MATRIX  COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            PRE  'XXMINV' : NOT-INVERTED
C                            POST 'XXMINV' : INVERTED
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C          (R*8)  CRED(,)  = MATRIX  OF  TRANSITION   RATES   BETWEEN
C                            METASTABLE LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C          (R*8)  CRMAT(,) = INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            BEFORE INPUT  TO   XXMINV: NOT INVERTED
C                            AFTER  OUTPUT FROM XXMINV: AS-ABOVE
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C
C          (R*8) POPAR(,,)= LEVEL POPULATIONS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C          (R*4) STVR(,,,) = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*4) STVH(,,,) = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*4) STVI(,,,) = ORDINARY EXCITED LEVEL:
C                             ELECTRON IMPACT IONISATION    COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*4) STACK(,,,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C          (R*8) STVRM(,,,)= METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*8) STVHM(,,,)= METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*8) STVIM(,,,)= METASTABLE LEVEL:
C                             ELECTRON IMPACT IONISATION    COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C          (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C                           'I' => Electron Impact Ionisation
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*18) CSTRGB()= AS CSTRGA() BUT ONLY TAKING THE LAST
C                           'ICSTMX' NON-BLANK BYTES
C          (C*22) STRGA() = LEVEL DESIGNATIONS
C
C          (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                       'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C          (L*4)  LTRNG(,)= .TRUE.  => TEMPERATURE VALUE WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                         = .FALSE. =>TEMPERATURE VALUE NOT WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                           1st DIMENSION: TEMPERATURE INDEX.
C                           2nd DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C          (L*4)  LSSETA(,)= .TRUE.  - MET. IONIS RATE SET IN B8GETS
C                            .FALSE.- MET. IONIS RATE NOT SET IN B8GETS
C                           1ST DIMENSION: (Z) ION METASTABLE INDEX
C                           2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C          (L*4)  LSS04A(,)= .TRUE. => IONIS. RATE SET IN ADF04 FILE:
C                            .FALSE.=> NOT SET IN ADF04 FILE
C                            1ST DIM: LEVEL INDEX
C                            2ND DIM: PARENT METASTABLE INDEX
C          (R*8)  PLAS1() =  DIRECT LINE POWER LOSS FOR SPECIFIC LINE
C                            POWER TRANSITION GIVEN BY 'ISPTRN'.
C                            (UNITS: ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C          (R*8)  SWVLN() =  WAVELENGTH (ANGSTROM) FOR SPECIFIC LINE
C                            POWER TRANSITION GIVEN BY 'ISPTRN'.
C                            (UNITS: ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C          (I*4)  ISPTRN()=  SPECIFIC LINE POWER: SELECTED ELECTRON
C                            IMPACT TRANSITION INDEX.
C                             1st DIMENSION: METASTABLE  INDEX
C
C          (R*8)  PL(,,)  = TOTAL LINE POWERS FOR METASTABLES. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE.
C                            => P(TOTAL)/N(IMET)        (ERGS SEC-1)
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY     INDEX
C          (R*8)  PLA(,)  = TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1st DIMENSION: TEMPERATURE INDEX
C                             2nd DIMENSION: DENSITY     INDEX
C          (R*8)  PS(,,,) = SPECIFIC LINE POWERS FOR METASTABLES.
C                           THIS IS  ORGINATING IN THE COLLISIONAL-
C                           RADIATIVE SENSE FROM THE METASTABLE AND
C                           TERMINATING ON EACH METASTABLE.
C                            => P(LINE)/N(IMET)        (ERGS SEC-1)
C                             1ST DIMENSION: METASTABLE LINE INDEX
C                             2ND DIMENSION: METASTABLE  INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY     INDEX
C          (R*8)  PSA(,,) =  EQUILIBRIUM SEPCIFIC LINE POWER COEFFTS.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1ST DIMENSION: METASTABLE LINE INDEX
C                             2ND DIMENSION: TEMPERATURE INDEX
C                             3RD DIMENSION: DENSITY     INDEX
C          (R*8)  PH(,,)  = TOTAL CX LINE POWER FOR PARENT. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           PARENT.
C                            => P(TOTAL)/N(IP)          (ERGS SEC-1)
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: DENSITY     INDEX
C                             3RD DIMENSION: PARENT METASTABL INDEX
C          (R*8)  PHA(,)  =  EQUILIBRIUM CX POWER COEFFT.
C                            => P(TOTAL)/(DENS*N(1))  (ERGS CM3 SEC-1)
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: DENSITY     INDEX
C          (R*8)  PLA1()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                           (UNITS: ERGS SEC-1) (DIMENSION: LEVEL INDEX)
C          (R*8)  PLBA(,) = HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                           FROM A PARTICULAR METASTABLE TO LEVELS
C                           'IPROJ' UPWARDS. (UNITS: ERGS CM3 SEC-1)
C                             => P/(DENS*N(IMET))
C                             1st DIMENSION: METASTABLE  INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                            (NOT IMPLEMENTED IN THIS CODE )
C          (R*8)  PR(,,)  = RECOM/BREMS. COEFFT (ERG S-1)
C                             1ST DIM: PARENT INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C
C
C NOTE:
C
C          INPUT/OUTPUT STREAM ALLOCATIONS:
C          --------------------------------
C
C
C          STREAM 10: INPUT - SPECIFIC ION RATE DATA INPUT FILE FROM
C          ('IUNT10')         DATABASE (SEE DATA SECTION ABOVE).
C
C          STREAM  7: OUTPUT - MAIN OUTPUT TABLES
C          ('IUNT27')
C
C          STREAM 11: OUTPUT - POPULATION  DATA  FOR DIAGNOSTIC USE, IF
C          ('IUNT11')          REQUESTED.   THIS DATA IS FOR SUBSEQUENT
C                              INPUT  INTO  THE  DIAGNOSTIC AND CONTOUR
C                              GRAPHING PROGRAM 'CONTOUR'.
C
C          STREAM 12: OUTPUT - METASTABLE POPULATION DATA, IF REQUESTED.
C          ('IUNT12')          THIS  DATA IS IN THE FORM  OF  A  PASSING
C                              FILE.
C
C          STREAM 13: OUTPUT - GEN. COLL. RAD. DATA, IF REQUESTED.
C          ('IUNT13')          THIS  DATA IS IN THE FORM  OF  A  PASSING
C                              FILE.
C
C          EXPLANATION OF OUTPUT FILE TO STREAM 7
C          --------------------------------------
C
C          'I'    = COMPLETE   LEVEL INDEX
C          'IMET' = METASTABLE LEVEL INDEX
C          'JMET' = METASTABLE LEVEL INDEX
C          'IORD' = ORDINARY EXCITED LEVEL INDEX
C
C          OPTIONAL POPULATION GRAPHS CAN BE PRODUCED. A SEPARATE GRAPH
C          IS DISPLAYED FOR EACH SINGLE INPUT TEMPERATURE SELECTED FROM
C          THE INPUT TEMPERATURE SET.
C
C          INPUT TEMPERATURES 'TIN?()' ARE CONVERTED TO KELVIN 'T?VA()'.
C
C          POPULATION OF METASTABLE 'IMET'      =>  N(IMET)/N(1)
C          POPULATION OF LEVEL 'IORD', DEPENDENT ON METASTABLE 'IMET'
C                                               =>  N(IORD)/N(IMET)
C
C
C          EXPLANATION OF MATRICES:  CRA(,) , CRCE(,) , CRCP(,) :
C          ------------------------------------------------------
C
C          (IF   'LIOSEL=.FALSE.'   THIS  EXPLANATION  ALSO  VALID  FOR
C           CC(,)  AND  CRED(,) )
C
C                      initial level  (column)
C                 --------------------------------
C                 |   *1  R12  R13  R14  R15  R16
C          final  |  R21   *2  R23  R24  R25  R26
C          level  |  R31  R32   *3  R34  R35  R36
C                 |  R41  R42  R43   *4  R45  R46
C          (row)  |  R51  R52  R53  R54   *5  R56
C                 |  R61  R62  R63  R64  R65   *6
C
C
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C               Diagonal elements represent the negative sum of their
C               respective columns i.e.
C
C                                   6
C                  diagonal: *n = -SUM( Rjn )  ( Rnn = 0)
C                                  j=1
C
C                  e.g. *3 = - ( R13 + R23 + R43 + R53 + R63 )
C
C                  Therefore each column adds to zero.
C
C                  If Rfi = transition rate from i -> f then
C                  *n = - ( total transition rate from level n )
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B8GETP     ADAS      FETCH EXPANSION DATA
C          B8SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          B8ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          B8SPF1     ADAS      GATHERS OUT FILE NAMES VIA ISPF PANELS
C          B8OUT1     ADAS      OUTPUT MAIN RESULTS TO OUTPUT STREAM
C          BADATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          B8TTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          B8SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          B8OUT0     ADAS      OUTPUT ION SPEC. ETC. TO OUTPUT STREAM
C          BXCHKM     ADAS      CHECKS IF TRANSITION EXIST TO METASTABLE
C          BXIORD     ADAS      SETS  UP ORDINARY LEVEL INDEX.
C          BXRATE     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C          B8RCOM     ADAS      ESTABLISHES RECOMBINATION RATE COEFFTS.
C          BXMCRA     ADAS      CONSTRUCTS A-VALUE MATRIX.
C          BXMCRC     ADAS      CONSTRUCTS EXC./DE-EXC. RATE COEF MATRIX
C          B8MCCA     ADAS      CONSTRUCTS WHOLE RATE MATRIX.
C          BXMCMA     ADAS      CONSTRUCTS ORDINARY LEVEL RATE MATRIX.
C          BXSTKA     ADAS      STACK UP ORDINARY POP. DEPENDENCE ON MET
C          B8STKB     ADAS      STACK UP RECOMB. CONTRIBUTION FOR ORD.
C          BXSTKC     ADAS      STACK UP TRANSITION RATE BETWEEN METS.
C          B8STKD     ADAS      STACK UP RECOMB RATE FOR EACH MET. LEVEL
C          B8STKE     ADAS      STACK UP RECOMB(+3-BODY) CONTRI.FOR ORD.
C          B8STKF     ADAS      STACK UP RECOMB(+3-BODY) FOR EACH MET.
C          BXMPOP     ADAS      CALCULATE BASIC MET. LEVEL POPULATIONS.
C          B8STVM     ADAS      CALCULATE MET. LEVEL RECOMB. COEFFTS.
C          B8POPM     ADAS      CALCULATE METASTABLE LEVEL POPULATIONS.
C          B8POPO     ADAS      CALCULATE ORDINARY LEVEL POPULATIONS.
C          B8OUTG     ADAS      OUTPUT GRAPHS USING GHOST80
C          B8WRPS     ADAS      OUTPUT DATA TO PEC AND SXB FILES
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      GATHERS CURRENT DATE AS 8 BYTE STRING
C          XXGUID     ADAS      GATHERS USER NAME AS 10 BYTE STRING
C          XXERYD     ADAS      CONVERTS ENERGIES FROM W.NO. TO RYDBERGS
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO EV.
C          XXDCON     ADAS      CONVERTS ISPF ENTERED DENS. TO CM-3.
C          XXRATE     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C                               FOR UNIT GAMMA VALUE.
C          XXMINV     ADAS      INVERTS MATRIX AND SOLVES EQUATIONS.
C          R8DCON     ADAS      REAL*8 FUNCTION: CONVERT DENSITY TO CM-3
C
C         ( ROUTINES 'B8????' ARE USED IN PROGRAM  ADAS208  ONLY.
C           ROUTINES 'B5????' ARE USED IN PROGRAM  ADAS205  ONLY.
C           ROUTINES 'B6????' ARE USED IN PROGRAM  ADAS206  ONLY.
C           ROUTINES 'BX????' ARE USED IN PROGRAMS ADAS205 & ADAS206 )
C
C AUTHOR:  HP SUMMERS (UPGRADE OF ADAS205 BY PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    24/07/93
C
C UPDATE:
C (1)  IF PROJECTION DATA USED THEN LIOSEL SET TO TRUE
C
C (2)  CALCULATE LINE POWER USING ROUTINES B8LOSS AND B8TOTL
C      OUTPUT LINE POWER COEFFICIENTS WITH ROUTINE B8WRMC
C
C
C UNIX-IDL PORT:
C
C DATE: UNKNOWN
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C UPDATE:  01/03/96  HP SUMMERS - INCREASE PANEL TEMP. AND DENSITY SETS
C
C UPDATE:  12/03/96  DH BROOKS - INCREASED SIZE OF DSNINC & DSNEXP TO 80
C                                ALSO, DSNGCR,DSNPAS,DSNOUT
C UPDATE:  11/04/96  DH BROOKS - ADDED CALL TO MORE ADVANCED BADATA 
C
C UPDATE:  24/05/96  HP SUMMERS - SPECIFIC LINE POWER DEVELOPMENT. MADE
C                                 ISPTRN AND PLAS1 VECTORS FOR EACH META.
C                                 B8LOSS REPLACES B6LOSS
C
C UPDATE:  03/06/96  HP SUMMERS - INCLUDED CX RECOMB POWER WITH NEW SUB-
C                                 ROUTINE B8TOTH.  CHANGED FROM XXDATE
C                                 TO BXDATE.
C
C UPDATE:  29/08/96  HPS - ADDED WAVELENGTH & A-VALUE LIMITS FOR PEC &
C                          SXB FILES
C
C UPDATE:  09/03/98  HPS - ADDED PRB TO DATA PASSED FROM THE PROJECTION
C                          MATRICES AND GIVEN AS OUTPUT FROM ADAS208
C***********************************************************************
C PUT UNDER S.C.C.S. CONTROL:
C
C VERSION: 1.1				DATE: 10/05/95
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER S.C.C.S.
C 
C VERSION: 1.2				DATE: 13/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - INCREASED SIZE OF DSFULL TO 80
C		ADDED CALL TO B8SPF1 BEFORE ANALYSIS
C
C VERSION: 1.3				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS OF B8WRMC TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.4				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS OF B8WR11 TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.5				DATE: 28-05-96
C MODIFIED: WILLIAM OSBORN
C	    PUT CALL TO B8OUT0 AFTER BOTH CALLS TO B8SPF1 SO THAT
C           HEADER IS ALWAYS OUTPUT BEFORE INFO FROM B8GETP
C
C VERSION: 1.6				DATE: 05-06-96
C MODIFIED: WILLIAM OSBORN
C	    ADDED MENU BUTTON CODE AFTER GRAPHICAL SCREEN
C
C VERSION: 1.7				DATE: 15-07-96
C MODIFIED: WILLIAM OSBORN
C	    ADDED HUGH'S CHANGES DATED 24/5/96 AND 3/06/96 ABOVE
C	    AND CORRECTED ERROR DUE TO A CALCULATION LINE BEING
C	    MISTAKENLY COMMENTED OUT
C
C VERSION: 1.8				DATE: 09-08-96
C MODIFIED: WILLIAM OSBORN
C	    PUT IN THE LAST TWO PARAMETERS IN THE SECOND CALL TO B8GETP
C           WHICH WERE MISSING.
C
C VERSION: 1.9				DATE: 30-09-96
C MODIFIED: WILLIAM OSBORN
C	    - ADDED HUGH'S CHANGES DATED 29/08/96 ABOVE
C           - CORRECTED PARAMETERS OF B8WRMC - PH, PS AND SWVLN WERE
C             MISSING
C           - CHANGED IUNT07 TO 17 TO AVOID CLASHING WITH STANDARD ERROR
C             STREAM 7 ON HP WORKSTATIONS
C
C VERSION: 1.10				DATE: 30-09-96
C MODIFIED: WILLIAM OSBORN
C           INCREASED NDTRN TO 1000
C
C VERSION: 1.11				DATE: 02-07-97
C MODIFIED: RICHARD MARTIN
C           INCREASED NDTRN TO 2100
C
C VERSION: 1.12				DATE: 02-03-98
C MODIFIED: RICHARD MARTIN.
C           CHANGED IUNT07 TO IUNT27 TO AVOID CONFLICT WITH IUNT17.
C		SEE NOTE ON VERSION 1.9 .
C
C VERSION: 1.13				DATE: 09-03-98
C MODIFIED: HUGH SUMMERS.
C		 - ADDED PRB TO DATA PASSED FROM THE PROJECTION
C              MATRICES AND GIVEN AS OUTPUT FROM ADAS208
C
C VERSION:  1.14                        DATE: 2/09/99
C MODIFIED: Martin O'Mullane
C           - Add real name of user via XXNAME.
C           - Removed nulls on output files
C           - Added extra information to PEC/SXB output.
C           - Changed delimeter from () to _. in constructing
C             8 character name in PEC/SXB block header lines.
C           - format error in b8getp.
C           - increased NDTRN to 2500 (from 2100)
C           - Comments expanded. Alter calling of b8wrmc,
C             b8wr11 and b8wr12.
C
C VERSION: 1.15				DATE: 13/09/99
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - ADDED DETECTION OF L-LINES AND S-LINES
C 
C
C VERSION:  1.16                        DATE: 10/11/2000
C MODIFIED: Martin O'Mullane
C           - Added utilisation of S-lines for ordinary levels. The 
C             code was restructured with a lot of multiply commented
C             out lines removed. Note tha the {} identifers on the
C             configuration lines in the adf04 are enforced and that
C             the ECIP formulation is the default.
C
C
C VERSION : 1.17                        
C DATE    : 13/02/2006
C MODIFIED: Martin O'Mullane
C           - Remove commented out IBM specific material.
C           - Use xxdata_04 to read adf04 datasets.
C           - Remove unecessary variables. 
C           - Increase levels to 150 and transitions to 5500. 
C           - Replace b8wr11 and b8wr12 with bswrps. The strongest
C             50 pecs, rather than the first 50 are written (with
C             a consistent sxb set). 
C
C VERSION : 1.17                        
C DATE    : 31-07-2009
C MODIFIED: Martin O'Mullane
C           - Increase levels to 1000 and transitions to 100000. 
C
C VERSION : 1.18                       
C DATE    : 10-02-2010
C MODIFIED: Martin O'Mullane
C           - Set unit23 (GCR met file) status to FALSE at start,
C             the same as the others. 
C
C VERSION : 1.19                       
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - PECs (and SXB) are sorted by power and wavelength (similar 
C             to adas810. The wavelength and A-value selection criteria
C             are now arrays (of size ndwvl). Changes to b8setp, bsispf
C             and b8wrps required.
C           - ndlev increased to 1500 and ndtran to 350000.
C           - Check for zero density before evaluating the 3-body rate.
C           - Increase size of filename variables from 80 to 132 characters.
C
C VERSION : 1.20                 
C DATE    : 24-07-2017
C MODIFIED: Martin O'Mullane
C           - Increase number of levels (1500->2500) and transitions
C             (350000->520000).
C           - Do not write the pls208.pass file.
C 
C VERSION : 1.21
C DATE    : 19-09-2017
C MODIFIED: Martin O'Mullane
C           - Increase number of metastables to 13 (up from 4).
C           - Remove the redundant TMETA array of strings for
C             b8gets panels.
C 
C VERSION : 1.22
C DATE    : 29-09-2018
C MODIFIED: Martin O'Mullane
C           - Allow user to specify transition indices to be output
C             in the PEC file. Requires argument changes to b8setp,
C             b8ispf and b8wrps.
C
C-----------------------------------------------------------------------
       INTEGER   NDLEV  , NDTRN  , NDTEM  , NDDEN  , NDMET  , ndqdn  ,
     &           nvmax  , NDWVL  , ndsel
       INTEGER   IUNT27 , IUNT10 , IUNT11 , IUNT12 , IUNT13 ,
     &           IUNT14 , IUNT15 , IUNT16 , IUNT17 , IUNT18 , IUNT19 ,
     &           IUNT20 , IUNT21 , IUNT22 , IUNT23
       INTEGER   L1     , L2     , ICSTMX , I4UNIT
       INTEGER   ISTOP  , PIPEIN
C-----------------------------------------------------------------------
       REAL*8    D1     , WN2RYD
C-----------------------------------------------------------------------
       CHARACTER PRGTYP*1   , CADAS*80
C-----------------------------------------------------------------------
       PARAMETER( NDLEV =2500, NDTRN = 520000,
     &            NDTEM = 35 , NDDEN = 24    , NDMET = 13, 
     &            ndqdn = 8  , nvmax = 14    , NDWVL = 5 ,
     &            ndsel = 100)
       PARAMETER( IUNT27=27, IUNT10=10, IUNT11=11, IUNT12=12)
       PARAMETER( IUNT13=13, IUNT14=14, IUNT15=15, IUNT16=16, IUNT17=17)
       PARAMETER( IUNT18=18, IUNT19=19, IUNT20=20, IUNT21=21, IUNT22=22)
       PARAMETER( IUNT23=23)
       PARAMETER( L1=1     , L2=2     , ICSTMX=11 )
C-----------------------------------------------------------------------
       PARAMETER( D1 = 1.0D0 , WN2RYD = 9.11269D-06 )
       PARAMETER( PRGTYP = 'M' )
       PARAMETER( PIPEIN = 5 )
C-----------------------------------------------------------------------
       INTEGER   IPAN      ,
     &           IZ        , IZ0       , IZ1     ,
     &           NPL       , NPLR      , NPLI    , NPL3    ,
     &           IL        , NV        , ITRAN   ,
     &           MAXLEV    , NMET      , NORD    , IFOUT   , IDOUT   ,
     &           MAXT      , MAXD      , ITSEL   ,
     &           ICNTE     , ICNTP     , ICNTR   , ICNTH   , ICNTI   ,
     &           ICNTL     , ICNTS     ,
     &           IFORM     , IN        , IS      , IT      , IMET    ,
     &           JMET      , IP        , I       ,
     &           ITIN      , ININ      , IX      , ilev    , ipp     ,
     &           iorb      , itieactn  , iadftyp , nwvl    , ntrsel
C-----------------------------------------------------------------------
       REAL*8    R8DCON    , R8NECIP   , ALFRED
       REAL*8    BWNO      , ZEFF      , ZEFFSQ  , DMINT
       REAL*8    XL1IN     , XU1IN     , X       ,
     &           XL1       , XU1       , YL1     , YU1
       REAL*8    R8EXPE    , SPREF
C-----------------------------------------------------------------------
       CHARACTER REP*3     , TITLED*3  , DATE*8    , UID*10     ,
     &           DSNOUT*132,
     &           DSNPAS*132, DSNGCR*132, DSNEXP*132, DSNPAP*132 ,
     &           DSNINP*132, DSNINC*132, DSFULL*132,
     &           TITLE*40  , GTIT1*40  , DSN80*132 ,
     &           USER*30
C-----------------------------------------------------------------------
       LOGICAL   OPEN27
       LOGICAL   OPEN10    , OPEN11    , OPEN12  , OPEN13  ,
     &           OPEN14    , OPEN15    , OPEN16  , OPEN17  , OPEN18  ,
     &           OPEN19    , OPEN20    , OPEN21  , OPEN22  , OPEN23
       LOGICAL   LGHOST    ,
     &           LPEND     , LPSEL     , LZSEL   , LIOSEL  ,
     &           LHSEL     , LRSEL     , LISEL   , LNSEL   , LGPH    ,
     &           LGRD1     , LDEF1     , LCONT   , LPASS   , LGCR    ,
     &           LSOLVE    , LPDATA    , LNORM
       LOGICAL   LNEWPA    , LPAPER    , OPUT
       logical   lprn      , lcpl      , lorb    , lbeth   ,
     &           letyp     , lptyp     , lrtyp   , lhtyp   ,
     &           lityp     , lstyp     , lltyp
C-----------------------------------------------------------------------
       INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     , NPLA(NDLEV)  ,
     &           IA(NDLEV)        ,
     &           ISA(NDLEV)       , ILA(NDLEV)       ,
     &           I1A(NDTRN)       , I2A(NDTRN)       ,
     &           IBSELA(NDMET,NDMET), ISSETA(NDMET,NDMET), 
     &           IPLA(NDMET,NDLEV)
       INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &           IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &           IITRN(NDTRN)     , ILTRN(NDTRN)     ,
     &           ISTRN(NDTRN)     ,
     &           IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &           IP1A(NDTRN)      , IP2A(NDTRN)      ,
     &           IA1A(NDTRN)      , IA2A(NDTRN)      ,
     &           IL1A(NDLEV)      , IL2A(NDLEV)      ,
     &           IS1A(NDLEV)      , IS2A(NDLEV)      ,
     &           ISPTRN(NDMET)
      integer    itrlow(ndsel)    , itrup(ndsel)
C-----------------------------------------------------------------------
       REAL*4    STVR(NDLEV,NDTEM,NDDEN,NDMET) ,
     &           STVH(NDLEV,NDTEM,NDDEN,NDMET) ,
     &           STVI(NDLEV,NDTEM,NDDEN,NDMET)
       REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
       REAL*8    SCEF(nvmax)     , BWNOA(NDMET)     , PRTWTA(NDMET) ,
     &           TINE(NDTEM)     , TINP(NDTEM)      , TINH(NDTEM)   ,
     &           TEVA(NDTEM)     , TPVA(NDTEM)      , THVA(NDTEM)   ,
     &           TEA(NDTEM)      , TPA(NDTEM)       , THA(NDTEM)    ,
     &           DINE(NDDEN)     , DINP(NDDEN)      ,
     &           DENSA(NDDEN)    , DENSPA(NDDEN)    ,
     &           RATHA(NDDEN)    ,RATPIA(NDDEN,NDMET),
     &           RATMIA(NDDEN,NDMET),
     &           AA(NDTRN)       , AVAL(NDTRN)      , AUGA(NDTRN)      , 
     &           WVLA(NDLEV)     ,
     &           XJA(NDLEV)      , WA(NDLEV)        ,
     &           XIA(NDLEV)      , ER(NDLEV)        ,
     &           CIE(NDLEV)      , RHS(NDLEV)       ,
     &           CIEPR(NDLEV,NDMET) , V3PR(NDLEV,NDMET)  ,
     &           VRRED(NDMET,NDMET) , VHRED(NDMET,NDMET) ,
     &           VIRED(NDMET,NDMET) , VIONR(NDMET,NDMET) ,
     &           VCRPR(NDMET,NDMET) , ZPLA(NDMET,NDLEV)
       REAL*8    TSCEF(nvmax,3)             , SCOM(nvmax,NDTRN)        ,
     &           SGRDA(NDTEM,NDMET,NDMET)   , ESGRDA(NDTEM,NDMET,NDMET),
     &           SMETA(NDTEM,NDMET,NDMET)   , ESMETA(NDTEM,NDMET,NDMET),
     &           SORDA(NDTEM,NDLEV,NDMET)   , ESORDA(NDTEM,NDLEV,NDMET),
     &           EXCRE(NDTEM,NDTRN)         , EXCRP(NDTEM,NDTRN)       ,
     &           DEXCRE(NDTEM,NDTRN)        , DEXCRP(NDTEM,NDTRN)      ,
     &           VECH(NDTEM,NDLEV,NDMET)    , VECR(NDTEM,NDLEV,NDMET)  ,
     &           VECI(NDTEM,NDLEV,NDMET)    , CRA(NDLEV,NDLEV)         ,
     &           CRCE(NDLEV,NDLEV)          , CRCP(NDLEV,NDLEV)        ,
     &           CC(NDLEV,NDLEV)            , CMAT(NDLEV,NDLEV)        ,
     &           CRED(NDMET,NDMET)          , CRMAT(NDMET,NDMET)
       REAL*8    DVECR(NDTEM,NDLEV,NDMET)
       REAL*8    DCIEPR(NDTEM,NDLEV,NDMET)   , DV3PR(NDTEM,NDLEV,NDMET)
       REAL*8    DCIE(NDTEM,NDLEV)
       REAL*8    PCC(NDLEV,NDLEV)              , PCIE(NDLEV)
       REAL*8    PCIEPR(NDLEV,NDMET)           , PV3PR(NDLEV,NDMET)
       REAL*8    PVCRPR(NDMET,NDMET)           , PVECR(NDLEV,NDMET)
       REAL*8    PR(NDMET,NDTEM,NDDEN)
       REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
       REAL*8    STCKM(NDMET,NDTEM,NDDEN)      ,
     &           STVRM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           STVHM(NDMET,NDTEM,NDDEN,NDMET) ,
     &           STVIM(NDMET,NDTEM,NDDEN,NDMET)
       REAL*8    FVCRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVRRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVHRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVIRED(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVIONR(NDMET,NDMET,NDTEM,NDDEN) ,
     &           FVCRPR(NDMET,NDMET,NDTEM,NDDEN)
       REAL*8    PLA1(NDLEV)           ,
     &           PLAS1(NDMET)          , SWVLN(NDMET)                ,
     &           PLA(NDTEM,NDDEN)      , PSA(NDMET,NDTEM,NDDEN)      ,
     &           PHA(NDTEM,NDDEN)      ,
     &           PLBA(NDMET,NDTEM)
       REAL*8    PL(NDMET,NDTEM,NDDEN) , PS(NDMET,NDMET,NDTEM,NDDEN) ,
     &           PH(NDTEM,NDDEN,NDMET)
       real*8    beth(ndtrn)           , qdn(ndqdn)                  ,
     &           qdorb((ndqdn*(ndqdn+1))/2)  
      real*8     avlt(ndwvl)     , wvls(ndwvl)      , wvll(ndwvl)
C-----------------------------------------------------------------------
       CHARACTER TCODE(NDTRN)*1  , CSTRGA(NDLEV)*18 , CSTRGB(NDLEV)*18 ,
     &           STRGA(NDLEV)*22 , CPLA(NDLEV)*1
       CHARACTER CPRTA(NDMET)*9  , STRGMF(NDMET)*11 , STRGMI(NDLEV)*12
C-----------------------------------------------------------------------
       LOGICAL   LMETR(NDMET)    , LTRNG(NDTEM,3)   , LBSETA(NDMET)
       LOGICAL   LSSETA(NDMET,NDMET)
       LOGICAL   LSS04A(NDLEV,NDMET)
       logical   ltied(ndlev)    , lqdorb((ndqdn*(ndqdn+1))/2)
C-----------------------------------------------------------------------
       DATA IPAN/0/
       DATA OPEN27 /.FALSE./
       DATA OPEN10/.FALSE./ , OPEN11/.FALSE./ ,
     &      OPEN12/.FALSE./ , OPEN13/.FALSE./ , OPEN14/.FALSE./ ,
     &      OPEN15/.FALSE./ , OPEN16/.FALSE./ , OPEN17/.FALSE./ ,
     &      OPEN18/.FALSE./ , OPEN19/.FALSE./ , OPEN20/.FALSE./ ,
     &      OPEN21/.FALSE./ , OPEN22/.FALSE./ , OPEN23/.FALSE./ ,
     &      LGHOST/.FALSE./
       DATA MAXD/0/,MAXT/0/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

       CALL XX0000
       CALL XXNAME(USER)

C-----------------------------------------------------------------------
C GATHER CURRENT DATE AND USER IDENTIFIER
C-----------------------------------------------------------------------
       CALL XXDATE( DATE )
       CALL XXGUID(UID)

C-----------------------------------------------------------------------
C Start of ADAS208
C-----------------------------------------------------------------------

 100   CONTINUE

C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE IT
C-----------------------------------------------------------------------

       IF (OPEN10) CLOSE(10)
       OPEN10=.FALSE.

C-----------------------------------------------------------------------
C GET PROTON, COPASE INPUT DATA SET NAMES (FROM ISPF)
C-----------------------------------------------------------------------

       CALL B8SPF0( REP    ,
     &              DSNINP , DSNEXP , DSNINC 
     &            )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED  END RUN
C-----------------------------------------------------------------------

       IF (REP.EQ.'YES') THEN
         GOTO 9999
       ENDIF

C-----------------------------------------------------------------------
C OPEN COPASE INPUT DATA FILE - DSNINC AND READ IN DATA
C-----------------------------------------------------------------------

       OPEN( UNIT=IUNT10 , FILE=DSNINC , STATUS='OLD' )
       OPEN10=.TRUE.

       itieactn = 0
       
       call xxdata_04( iunt10 , 
     &                 ndlev  , ndtrn , ndmet   , ndqdn , nvmax ,
     &                 TITLED , iz    , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,     
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )

C-----------------------------------------------------------------------
C REDUCED LEVEL NOMENCLATURE STRINGS TO LAST 'ICSTMX' NON-BLANK BYTES
C-----------------------------------------------------------------------

       CALL BXCSTR( CSTRGA , IL     , ICSTMX ,
     &              CSTRGB
     &            )

C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPES.
C-----------------------------------------------------------------------

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )
C-----------------------------------------------------------------------
C CALCULATE LEVEL ENERGIES RELATIVE TO LEVEL 1 & IONISATION POT. IN RYDS
C-----------------------------------------------------------------------

       CALL XXERYD( BWNO , IL , WA  ,
     &                     ER , XIA
     &            )

C-----------------------------------------------------------------------
C SET UP ARRAY 'TSCEF(,)' - CONTAINS INPUT TEMPERATURES IN THREE FORMS.
C-----------------------------------------------------------------------

       DO IFORM=1,3
          CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
       END DO

C-----------------------------------------------------------------------
C COMMUNICATE WITH PROCESSING PANEL
C-----------------------------------------------------------------------

       CALL B8SETP( IZ0    , IZ     ,
     &              NDLEV  , NDWVL  , NDSEL , IL    , ICNTE ,
     &              CSTRGB , ISA    , ILA   , XJA   ,
     &              STRGA  , NPL    , CPRTA , NDMET ,
     &              LSS04A ,
     &              STRGMF , STRGMI 
     &            )

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM PANELS)
C-----------------------------------------------------------------------

 200   CONTINUE
 
       CALL B8ISPF( IPAN  , LPEND  , TITLED,
     &              NDTEM , NDDEN  , NDMET , ndsel ,
     &              IL    , NV     , TSCEF ,
     &              TITLE , NMET   , IMETR ,
     &              IFOUT ,
     &              MAXT  , TINE  , TINP   , TINH  ,
     &              IDOUT ,
     &              MAXD  , DINE  , DINP   , RATHA , RATPIA, RATMIA,
     &              ZEFF  , LNORM ,
     &              LPSEL , LZSEL , LIOSEL , LHSEL , LRSEL , LISEL ,
     &              LNSEL , LGPH  , NWVL   , WVLS  , WVLL  , AVLT  ,
     &              ntrsel, itrlow, itrup  ,
     &              ITSEL , GTIT1 , LGRD1  , LDEF1 ,
     &              XL1IN , XU1IN , YL1    , YU1   , IBSELA,
     &              LSSETA, SGRDA , ISSETA , DSFULL
     &            )
       IPAN=-1

C----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999
       IF (LPEND) GOTO 100

C----------------------------------------------------------------------
C READ OUTPUT OPTIONS FROM IDL
C-----------------------------------------------------------------------

       CALL B8SPF1( NDTEM  , TINE   , MAXT   , IFOUT ,
     &              LPEND  , LGCR   , UID    ,
     &              LNEWPA , LPAPER , LCONT  , LPASS ,
     &              DSNPAP , DSNOUT , DSNPAS , DSNGCR,
     &              LGPH   , ITSEL  , GTIT1  , CADAS
     &            )
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION(1) OR CONTINUE(0)
C-----------------------------------------------------------------------

       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999
       IF(LPEND) GOTO 200

CX----------------------------------------------------------------------
CX ULTRIX PORT - OPEN TEXT OUTPUT DATA SET - DSNPAP - IF REQUESTED.
CX               THIS IS NEW FUNCTIONALITY.  IF LNEWPA IS .TRUE. THEN
CX               EITHER A NEW FILE IS TO BE CREATED OR AN EXISTING FILE
CX               IS TO BE REPLACED. ANY FILE WHICH IS CURRENTLY OPEN
CX               MUST BE CLOSED FIRST. IF LNEWPA IS .FALSE. AND LPAPER
CX               IS TRUE THEN OUTPUT IS APPENDED TO A PREVIOUSLY OPEN
CX               FILE IF ANY.
CX----------------------------------------------------------------------

            IF ( (LPAPER .AND. LNEWPA) .OR.
     &                             (LPAPER . AND. .NOT.OPEN27) ) THEN
                IF (OPEN27) CLOSE(UNIT=IUNT27)
                DSN80 = ' '
                DSN80 = DSNPAP
                OPEN(UNIT=IUNT27,FILE=DSN80,STATUS='UNKNOWN')
                OPEN27=.TRUE.
             ENDIF

C------------------------------------------------------------------------
C OUTPUT ION SPECIFICATIONS & LEVEL INFORMATION TO 'IUNT27' (PAPER.TEXT)
C-----------------------------------------------------------------------
CX
CX ULTRIX PORT - ROUTINE MOVED HERE FROM THE BEGINNING OF THE PROGRAM
CX               (AND TO AFTER THE SECOND B8SPF1 CALL LATER ON)
CX               WRITING OF TEXT OUTPUT NOW UNDER USER CONTROL VIA
CX               IDL USER INTERFACE AND LPAPER.
CX
             IF (LPAPER) THEN
                CALL B8OUT0(  NDMET  , IUNT27 , DATE   ,
     &               PRGTYP , DSNINC , DSNEXP ,
     &               TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &               NPL    , NPLR   , NPLI   , BWNOA  ,
     &               ICNTE  , ICNTP  , ICNTR  , ICNTH  , ICNTI  ,
     &               ICNTL  , ICNTS  ,
     &               IL     ,
     &               IA     , CSTRGA , ISA    , ILA    , XJA    , WA ,
     &               ER     , CPLA   ,
     &               NV     , TSCEF  , CADAS
     &               )
             ENDIF

             OPUT = .TRUE.



C***********************************************************************
CX ANALYSE ENTERED DATA - IF NOT END OF ANALYSIS (OLD)
CX ULTRIX PORT -          IF NOT RETURN TO DATASET SELECTION (NEW)
C***********************************************************************
C
C-----------------------------------------------------------------------
C CHECK IF ELECTRON IMPACT TRANSITIONS EXIST TO THE SELECTED METASTABLE
C-----------------------------------------------------------------------

            CALL BXCHKM( NMET   , IMETR , ICNTE , IE1A , LMETR )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO EV:  (TIN? -> T?VA)  (? = E,P,H )
C-----------------------------------------------------------------------

            CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINE  , TEVA   )
            CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINP  , TPVA   )
            CALL XXTCON( IFOUT, L2, IZ1, MAXT , TINH  , THVA   )

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN:  (TIN? -> T?A)  (? = E,P,H )
C-----------------------------------------------------------------------

            CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINE  , TEA    )
            CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINP  , TPA    )
            CALL XXTCON( IFOUT, L1, IZ1, MAXT , TINH  , THA    )

C-----------------------------------------------------------------------
C CONVERT INPUT DENSITIES, INCLUDING RELEVANT GRAPH LIMITS, INTO CM-3:
C-----------------------------------------------------------------------

            CALL XXDCON( IDOUT, L1, IZ1, MAXD , DINE  , DENSA  )
            CALL XXDCON( IDOUT, L1, IZ1, MAXD , DINP  , DENSPA )
            XL1 = R8DCON( IDOUT , L1 , IZ1 , XL1IN )
            XU1 = R8DCON( IDOUT , L1 , IZ1 , XU1IN )

C-----------------------------------------------------------------------
C  INITIALISE VECR, VECH, VECI TO ZERO
C-----------------------------------------------------------------------

               DO 22 IS=1,NDLEV
                  DO 21 IT=1,NDTEM
                    DCIE(IT,IS)=0.0D0
                    DO 20 IP=1,NDMET
                        VECR(IT,IS,IP)  = 0.0D0
                        DVECR(IT,IS,IP) = 0.0D0
                        VECH(IT,IS,IP)  = 0.0D0
                        VECI(IT,IS,IP)  = 0.0D0
                        DCIEPR(IT,IS,IP)  = 0.0D0
                        DV3PR(IT,IS,IP) = 0.0D0
   20                CONTINUE
   21             CONTINUE
   22          CONTINUE

C-----------------------------------------------------------------------
C CALCULATE ELECTRON IMPACT TRANSITN EXCITATION & DE-EXCIT'N RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.
C-----------------------------------------------------------------------

            CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                   ICNTE , MAXT   ,
     &                   XJA   , ER     , TEA   ,
     &                   IE1A  , IE2A   ,
     &                   EXCRE , DEXCRE
     &                 )

C-----------------------------------------------------------------------
C CALCULATE LINE POWER LOSES FOR ALL LINES AND SPECIFIC LINE.
C-----------------------------------------------------------------------

            CALL B8LOSS( NDTRN , NDLEV  , NDMET ,
     &                   ICNTE , NMET   , IMETR , ISPTRN ,
     &                   XJA   , ER     , AA    ,
     &                   IE1A  , IE2A   ,
     &                   PLAS1 , SWVLN  , PLA1
     &                  )

C-----------------------------------------------------------------------
C CALCULATE PROTON IMPACT TRANSITION EXCITATION & DE-EXCIT'N RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.             (ONLY IF REQUIRED).
C-----------------------------------------------------------------------

               IF (LPSEL) THEN
                  CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                         ICNTP , MAXT   ,
     &                         XJA   , ER     , TPA   ,
     &                         IP1A  , IP2A   ,
     &                         EXCRP , DEXCRP
     &                       )
               ENDIF

C-----------------------------------------------------------------------
C SETUP INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE LEVEL LIST 'IORDR'
C-----------------------------------------------------------------------

            CALL BXIORD( IL   ,
     &                   NMET , IMETR ,
     &                   NORD , IORDR
     &                 )

C-----------------------------------------------------------------------
C ELECTRON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------

            CALL BXRATE( NDTEM      , NDTRN  , D1     ,
     &                   NV         , SCEF   , SCOM   ,
     &                   MAXT       , TEA    ,
     &                   ICNTE      , IETRN  ,
     &                   EXCRE      , DEXCRE ,
     &                   LTRNG(1,1)
     &                 )

C-----------------------------------------------------------------------
C PROTON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------

               IF (LPSEL) THEN
                  ZEFFSQ = ZEFF*ZEFF
                  CALL BXRATE( NDTEM      , NDTRN  , ZEFFSQ ,
     &                         NV         , SCEF   , SCOM   ,
     &                         MAXT       , TPA    ,
     &                         ICNTP      , IPTRN  ,
     &                         EXCRP      , DEXCRP ,
     &                         LTRNG(1,2)
     &                       )
               ENDIF

C----------------------------------------------------------------------
C FREE ELECTRON RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LRSEL) THEN
                  CALL B8RCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , TEA        ,
     &                         ICNTR , IRTRN      , I2A    , I1A    ,
     &                         DVECR  , LTRNG(1,1)
     &                       )

               ENDIF

C----------------------------------------------------------------------
C ELECTRON IMPACT IONISATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LISEL) THEN
                  CALL B8RCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , TEA        ,
     &                         ICNTI , IITRN      , I2A    , I1A    ,
     &                         VECI  , LTRNG(1,1)
     &                       )

               ENDIF

C----------------------------------------------------------------------
C CHARGE EXCHANGE RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LHSEL) THEN
                  CALL B8RCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , THA        ,
     &                         ICNTH , IHTRN      , I2A    , I1A    ,
     &                         VECH  , LTRNG(1,3)
     &                       )
               ENDIF

C----------------------------------------------------------------------
C IONISATION Z --> Z+1 DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------

               IF (LIOSEL) THEN
                  CALL B8SCOM( NDTEM , NDTRN      , NDLEV  , NDMET  ,
     &                         IL    , WA         , NPL    , BWNOA  , 
     &                         NMET  , IMETR      , NORD   , IORDR  ,
     &                         NV    , SCEF       , SCOM   ,
     &                         MAXT  , TEA        , 
     &                         ICNTS , ISTRN      , IS1A   , IS2A   ,
     &                         LSSETA, SGRDA      , ESGRDA ,
     &                         SMETA , ESMETA     , SORDA  , ESORDA ,
     &                         LTRNG(1,1) 
     &                       )
          
               ENDIF

C----------------------------------------------------------------------
C SET UP A-VALUE TRANSITION MATRIX 'CRA' (UNITS: SEC-1)
C----------------------------------------------------------------------

            CALL BXMCRA( NDTRN , NDLEV  ,
     &                   ICNTE , IL     ,
     &                   IE1A  , IE2A   ,
     &                   AA    ,
     &                   CRA
     &                 )

C----------------------------------------------------------------------
C INITIALISE PROJECTED INDIRECT COUPLINGS
C----------------------------------------------------------------------

            LPDATA = .FALSE.
            IF( LNSEL ) THEN
                ITIN = 1
                ININ = 1
                CALL B8GETP( IZ0    , IZ1    , DSNEXP , DSNINC ,
     &                       NDLEV  , NDMET  , NDTEM  , NDDEN  ,
     &                       MAXD   , MAXT   , DENSA  , TEA    ,
     &                       LPDATA , LIOSEL , LRSEL  , LHSEL  ,
     &                       IL     , ITIN   , ININ   ,
     &                       PCC    , PCIE   , PCIEPR , PV3PR  ,
     &                       PVCRPR , PVECR  , IUNT27 , OPEN27 ,
     &                       PR
     &                     )

            ENDIF

C-----------------------------------------------------------------------
C  SET UP VECTOR OF IONISATION AND THREE-BODY RECOMBINATION COEFFICIENTS
C  IF REQUIRED - NOTE PARENT RESOLVED DATA
C
C  The original implementation here was very messy so this version 
C  removes a lot of the commented out code.
C
C  Note that the indexing is quite confusing; most quantities are
C  indexed by parent and level (as well as temperature) but these 
C  can vary 
C
C      dciepr(IT, IL, IP)  :  IL level list
C      lss04a(IL, IP)      :  IL level list
C      zpla(IP, IL)        :  IL level list
C      sorda(IT, IL, IP)   :  IL ordinary level list
C
C  The level list is 1-number of levels, ordinary is 1 - number of levels
C  less number of metastables.
C
C-----------------------------------------------------------------------

       IF (LIOSEL) THEN

           DO 122 IT = 1,MAXT
           
              DO 5 IS=1,MAXLEV
                 DO 15 IP=1,NPLA(IS)
                 
C check if (S-lines) from ordinary levels are present in the adf04 file,
C if not use ECIP approximation.

                    ilev = is - nmet
                    ipp  = IPLA(IP,IS)
                    if (ilev.GT.0.and.lss04a(is,ipp)) then
                    
                        spref = R8EXPE(SORDA(IT,ilev,ipp),
     &                                 ESORDA(IT,ilev,ipp),'ZERO')
                        IF ( ZPLA(IP,IS) .GT. 0.0D0) THEN
                           X=XIA(IS)+WN2RYD*(BWNOA(IPLA(IP,IS))-BWNO)
                           DCIEPR(IT,IS,IPLA(IP,IS))=spref
                           if (spref.GT.0.0D0) then
                              alfred = 3.30048D-24 * spref * 
     &                                 (1.5789D5/tea(it))**1.5 * 
     &                                 exp(1.5789D5*x/tea(it))
                              DV3PR(IT,IS,IPLA(IP,IS))=
     &                                   (2.0D0*XJA(IS)+1.0D0)*ALFRED/
     &                                          PRTWTA(IPLA(IP,IS))
                           else
                              DV3PR(IT,IS,IPLA(IP,IS)) = 1.0D-40
                           endif
                        ELSE
                           DCIEPR(IT,IS,IPLA(IP,IS)) = 0.0D+0
                           DV3PR(IT,IS,IPLA(IP,IS))  = 0.0D+0
                        ENDIF
                        
                    else
                     
                       IF ( ZPLA(IP,IS) .GT. 0.0D0) THEN
                          X=XIA(IS)+WN2RYD*(BWNOA(IPLA(IP,IS))-BWNO)
                          DCIEPR(IT,IS,IPLA(IP,IS))=
     &                                R8NECIP(IZ,X,ZPLA(IP,IS)
     &                                       ,TEA(IT),ALFRED)
                          DV3PR(IT,IS,IPLA(IP,IS))=
     &                               (2.0D0*XJA(IS)+1.0D0)*ALFRED/
     &                                      PRTWTA(IPLA(IP,IS))
                       ELSE
                          DCIEPR(IT,IS,IPLA(IP,IS)) = 0.0D+0
                          DV3PR(IT,IS,IPLA(IP,IS))  = 0.0D+0
                       ENDIF
                       
                    endif

                    DCIE(IT,IS) = DCIE(IT,IS) +
     &                            DCIEPR(IT,IS,IPLA(IP,IS))
   15            CONTINUE
    5         CONTINUE

C-----------------------------------------------------------------------
C  SUBSTITUTE SPECIAL SZD RATES FROM B8GETS CALL IF AVAILABLE
C  ADJUST THREE-BODY RATES ACCORDINGLY
C-----------------------------------------------------------------------
  
              DO 14 IMET=1,NMET
                DO 13 IP=1,NPL
                  IF (LSSETA(IMET,IP))THEN
                     SPREF=R8EXPE(SMETA(IT,IMET,IP),
     &                            ESMETA(IT,IMET,IP),'ZERO')
                     IF (SPREF.GT.0.0D0.AND.
     &                   DCIEPR(IT,IMETR(IMET),IP).GT.0.0D0) THEN
                         DV3PR(IT,IMETR(IMET),IP)=SPREF
     &                             * DV3PR(IT,IMETR(IMET),IP)/
     &                               DCIEPR(IT,IMETR(IMET),IP)
                     ENDIF
                     DCIE(IT,IMETR(IMET))=DCIE(IT,IMETR(IMET)) -
     &                                DCIEPR(IT,IMETR(IMET),IP)+
     &                                SPREF
                     DCIEPR(IT,IMETR(IMET),IP)=SPREF
                       

                  ENDIF
   13           CONTINUE
   14       CONTINUE

  122    CONTINUE

       ENDIF
         

C-----------------------------------------------------------------------
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C INCREMENT OVER OVER ALL INPUT TEMPERATURE VALUES
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C-----------------------------------------------------------------------

               DO 4 IT=1,MAXT

C-----------------------------------------------------------------------
C SET UP EXCITATION/DE-EXCITATION RATE COEFFT. MATRICES FOR GIVEN TEMP.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ELECTRON IMPACT TRANSITIONS 'CRCE' - (UNITS: CM**3/SEC-1)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                  CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                         IT    , ICNTE  , IL     ,
     &                         IE1A  , IE2A   ,
     &                         EXCRE , DEXCRE ,
     &                         CRCE
     &                       )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C PROTON IMPACT TRANSITIONS 'CRCP' - (UNITS: CM**3/SEC-1) - IF SELECTED
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                     IF (LPSEL) THEN
                        CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                               IT    , ICNTP  , IL     ,
     &                               IP1A  , IP2A   ,
     &                               EXCRP , DEXCRP ,
     &                               CRCP
     &                              )
                     ENDIF

C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C INCREMENT OVER OVER ALL INPUT DENSITY VALUES
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *

                     DO 6 IN=1,MAXD
C-----------------------------------------------------------------------
C  INITIALISE VECTORS TO ZERO
C-----------------------------------------------------------------------

               DO 2 IS=1,NDLEV
                  CIE(IS)  = 0.0D0
                  PCIE(IS) = 0.0D+0
                  DO 19 I = 1,NDLEV
                    PCC(IS,I) = 0.0D0
   19             CONTINUE
                  DO 120 IP=1,NDMET
                     CIEPR(IS,IP)  = 0.0D0
                     PCIEPR(IS,IP) = 0.0D0
                     V3PR(IS,IP)   = 0.0D0
                     PV3PR(IS,IP)  = 0.0D0
                     PVECR(IS,IP)  = 0.0D0
  120             CONTINUE
    2          CONTINUE


C-----------------------------------------------------------------------
C SET UP INDIRECT MATRICES
C-----------------------------------------------------------------------

                     IF( LPDATA ) THEN
                        ITIN = IT
                        ININ = IN

                 CALL B8GETP( IZ0    , IZ1    , DSNEXP , DSNINC ,
     &                        NDLEV  , NDMET  , NDTEM  , NDDEN  ,
     &                        MAXD   , MAXT   , DENSA  , TEA    ,
     &                        LPDATA , LIOSEL , LRSEL  , LHSEL  ,
     &                        IL     , ITIN   , ININ   ,
     &                        PCC    , PCIE   , PCIEPR , PV3PR  ,
     &                        PVCRPR , PVECR  , IUNT27 , OPEN27 ,
     &                        PR
     &                      )

                     ENDIF

 
C-----------------------------------------------------------------------
C  ADD INDIRECT COUPLINGS ONTO IONISATION AND RECOMBINATION VECTORS
C
C  IF INDIRECT COUPLING HAVE BEEN INITIATED, SET LIOSEL = TRUE
C                                                LRSEL  = TRUE
C
C  NOTE CIE HAS UNITS OF CM3S-1 WHILE PCIE AND PCIEPR HAVE UNITS OF S-1
C       V3PR HAS UNITS OF CM6S-1 WHILE PV3PR HAS UNITS OF CM3S-1
C       VECR AND PVECR HAVE UNITS OF CM3S-1
C
C  NOTE THAT CIE DOES NOT NEED TO BE ADJUSTED
C-----------------------------------------------------------------------

                     IF( LPDATA ) THEN

                        LIOSEL = .TRUE.
                        LRSEL = .TRUE.
                        DO 17 IS=1,MAXLEV

C                         CIE(IS) = DCIE(IT,IS) + PCIE(IS)/DENSA(IN)
                          CIE(IS) = DCIE(IT,IS)
                          DO 18 IP = 1,NPL
                            CIEPR(IS,IP) = DCIEPR(IT,IS,IP)
     &                                      + PCIEPR(IS,IP) / DENSA(IN)
                            V3PR(IS,IP) = DV3PR(IT,IS,IP)
     &                                      + PV3PR(IS,IP) / DENSA(IN)
                            VECR(IT,IS,IP) = DVECR(IT,IS,IP)
     &                                      + PVECR(IS,IP)
   18                     CONTINUE
   17                   CONTINUE
 
                     ELSE
 
                         DO 23 IS=1,MAXLEV
 
                          CIE(IS) = DCIE(IT,IS)
                          DO 24 IP = 1,NPL
                            CIEPR(IS,IP) = DCIEPR(IT,IS,IP)
                            V3PR(IS,IP)  =  DV3PR(IT,IS,IP)
                            VECR(IT,IS,IP) = DVECR(IT,IS,IP)
   24                     CONTINUE
   23                    CONTINUE
 
                     ENDIF

C-----------------------------------------------------------------------
C SET UP WHOLE RATE MATRIX  - (UNITS: SEC-1)
C
C   TO ADD ON INDIRECT PARTS ALSO PASS OVER PCC
C-----------------------------------------------------------------------

                        CALL B8MCCA( NDLEV     , IL         ,
     &                               LPSEL     , LIOSEL     , LPDATA,
     &                               DENSA(IN) , DENSPA(IN) ,
     &                               CRA       , PCC        ,
     &                               CRCE      , CRCP       , CIE    ,
     &                               CC
     &                             )

C-----------------------------------------------------------------------
C SET UP NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX - (UNITS: SEC-1)
C-----------------------------------------------------------------------

                        CALL BXMCMA( NDLEV ,
     &                               NORD  , IORDR ,
     &                               CC    ,
     &                               CMAT
     &                             )
C-----------------------------------------------------------------------
C INVERT NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX
C-----------------------------------------------------------------------
                        LSOLVE=.FALSE.
                        CALL XXMINV( LSOLVE , NDLEV , NORD   ,
     &                               CMAT   , RHS   , DMINT
     &                             )
                
C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE POPULATION COMPONENTS ASSOCIATED WITH EACH
C METASTABLE LEVEL.
C-----------------------------------------------------------------------

                        CALL BXSTKA( NDLEV            , NDMET  ,
     &                               NORD             , NMET   ,
     &                               IORDR            , IMETR  ,
     &                               CMAT             , CC     ,
     &                               STACK(1,1,IT,IN)
     &                             )

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE STATE SEL. RECOMBS. FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN
                       NPL3 = MAX0(NPL,NPLR)

                       DO 36 IP=1,NPL3

                           CALL B8STKE( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  DENSA(IN)     ,
     &                                  CMAT          , VECR   , V3PR  ,
     &                                  IP            ,
     &                                  STVR(1,IT,IN,IP)
     &                                )
   36                  CONTINUE

                   ELSEIF (LRSEL.AND.(.NOT.LIOSEL)) THEN

                       DO 37 IP=1,NPLR

                           CALL B8STKB( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  CMAT          , VECR   ,
     &                                  IP            ,
     &                                  STVR(1,IT,IN,IP)
     &                                )
   37                 CONTINUE
                   ENDIF

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE STATE SEL. C. EXCH. FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 30 IP=1,NPLR

                        IF (LHSEL) THEN
                           CALL B8STKB( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  CMAT          , VECH   ,
     &                                  IP            ,
     &                                  STVH(1,IT,IN,IP)
     &                                )
                        ENDIF
   30              CONTINUE

C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE STATE SEL. IONIS.   FROM (Z-1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 31 IP=1,NPLI

                        IF (LISEL) THEN
                           CALL B8STKB( NDTEM         , NDLEV  , NDMET ,
     &                                  IT            , NORD   ,
     &                                                  IORDR  ,
     &                                  CMAT          , VECI   ,
     &                                  IP            ,
     &                                  STVI(1,IT,IN,IP)
     &                                )
                        ENDIF
   31              CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE LEVEL TRANSITION RATE CONTRIBUTIONS
C-----------------------------------------------------------------------

                     CALL BXSTKC( NDLEV            , NDMET            ,
     &                            NORD             , NMET             ,
     &                            IORDR            , IMETR            ,
     &                            CC               , STACK(1,1,IT,IN) ,
     &                            CRED
     &                          )
                     DO 44 IMET=1,NMET
                       DO 44 JMET=1,NMET
                         FVCRED(IMET,JMET,IT,IN)=CRED(IMET,JMET)
   44                CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. RECOMB.  FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN
                       NPL3 = MAX0(NPL,NPLR)

                       DO 38 IP=1,NPL3

                           CALL B8STKF( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVR(1,IT,IN,IP),
     &                                  DENSA(IN),
     &                                  VECR  , V3PR          , IP     ,
     &                                  VRRED
     &                                )
                           DO 45 IMET=1,NMET
                             FVRRED(IMET,IP,IT,IN)=VRRED(IMET,IP)
   45                      CONTINUE

   38                   CONTINUE

                   ELSEIF(LRSEL.AND.(.NOT.LIOSEL))THEN

                       DO 39 IP=1,NPLR

                           CALL B8STKD( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVR(1,IT,IN,IP),
     &                                  VECR  , IP    ,
     &                                  VRRED
     &                                )
                           DO 46 IMET=1,NMET
                             FVRRED(IMET,IP,IT,IN)=VRRED(IMET,IP)
   46                      CONTINUE

   39                  CONTINUE
                   ENDIF

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. C. EXCH. FROM (Z+1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 32 IP=1,NPLR
                        IF (LHSEL) THEN
                           CALL B8STKD( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVH(1,IT,IN,IP) ,
     &                                  VECH  , IP   ,
     &                                  VHRED
     &                                )
                           DO 47 IMET=1,NMET
                             FVHRED(IMET,IP,IT,IN)=VHRED(IMET,IP)
   47                      CONTINUE

                        ENDIF
   32              CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. IONIS.   FROM (Z-1)    -IF SELECTED
C-----------------------------------------------------------------------

                   DO 33 IP = 1,NPLI

                        IF (LISEL) THEN
                           CALL B8STKD( NDTEM , NDLEV         , NDMET  ,
     &                                  IT    , NORD          , NMET   ,
     &                                          IORDR         , IMETR  ,
     &                                  CC    , STVI(1,IT,IN,IP) ,
     &                                  VECI  , IP   ,
     &                                  VIRED
     &                                )
                           DO 48 IMET=1,NMET
                             FVIRED(IMET,IP,IT,IN)=VIRED(IMET,IP)
   48                      CONTINUE

                        ENDIF
   33              CONTINUE

C-----------------------------------------------------------------------
C STACK UP METASTABLE STATE SEL. IONIS. FROM (Z) - IF SELECTED BY LIOSEL
C
C STACK UP PARENT CROSS COUPLING COEFFICIENTS
C
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN

                   DO 43 IMET = 1,NMET

                     DO 42 IP=1,NPL3

                       VIONR(IMET,IP)=CIEPR(IMETR(IMET),IP)
                       DO 41 I=1,NORD
                        VIONR(IMET,IP)=VIONR(IMET,IP)+CIEPR(IORDR(I),IP)
     &                                 *STACK(I,IMET,IT,IN)
   41                  CONTINUE
                       FVIONR(IMET,IP,IT,IN)=VIONR(IMET,IP)

   42                CONTINUE
   43              CONTINUE

                   DO 143 IP = 1,NPL3

                     DO 142 IMET=1,NPL3

                       VCRPR(IP,IMET)=0.0D+0
                       IF( IP .NE. IMET ) THEN
                       DO 141 I=1,NORD
                        VCRPR(IP,IMET)=VCRPR(IP,IMET)+CIEPR(IORDR(I),IP)
     &                                 *STVR(I,IT,IN,IMET)
  141                  CONTINUE
                       ENDIF
                       FVCRPR(IP,IMET,IT,IN)=VCRPR(IP,IMET)
                       IF ( LPDATA ) THEN
                          FVCRPR(IP,IMET,IT,IN) = FVCRPR(IP,IMET,IT,IN)
     &                                          + PVCRPR(IP,IMET)
                       ENDIF

  142                CONTINUE
  143              CONTINUE

                   ENDIF

C-----------------------------------------------------------------------
C CALCULATE METASTABLE POPULATIONS
C-----------------------------------------------------------------------

                     CALL BXMPOP( NDMET             ,
     &                            NMET              ,
     &                            CRED              ,
     &                            RHS               , CRMAT        ,
     &                            STCKM(1,IT,IN)
     &                          )

C-----------------------------------------------------------------------
C  CALCULATE RECOMBINATIONS AND CHARGE EXCHANGE CONTRIBUTIONS.
C-----------------------------------------------------------------------

                   IF (LIOSEL) THEN

                       DO 34 IP=1,NPL3
 
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VRRED            ,
     &                               STVRM(1,IT,IN,IP)
     &                             )
   34                  CONTINUE

      
                   ELSEIF ((.NOT.LIOSEL).AND.LRSEL) THEN

                       DO 51 IP=1,NPLR
 
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VRRED            ,
     &                               STVRM(1,IT,IN,IP)
     &                             )
   51                  CONTINUE

                   ENDIF
C-----------------------------------------------------------------------
                   DO 52 IP=1,NPLR

                     IF (LHSEL) THEN
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VHRED            ,
     &                               STVHM(1,IT,IN,IP)
     &                             )
                     ENDIF
   52              CONTINUE
C-----------------------------------------------------------------------
                   DO 35 IP=1,NPLI

                     IF (LISEL) THEN
                        CALL B8STVM( NDMET            ,
     &                               NMET             ,
     &                               CRMAT            ,
     &                               IP               ,
     &                               VIRED            ,
     &                               STVIM(1,IT,IN,IP)
     &                             )
                     ENDIF
   35              CONTINUE
C-----------------------------------------------------------------------

    6             CONTINUE
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C
C-----------------------------------------------------------------------
C CALCULATE THE SPECIFIC  LINE POWER, CHARGE EXCHANGE RECOMBINATION
C AND TOTAL LINE POWERS - EQUIL. & MET. RESOLVED.
C-----------------------------------------------------------------------

                  DO IMET = 1,NDMET
                    PLBA(IMET,IT) = 0.0D+0
                  ENDDO

                  DO 7 IN=1,MAXD
                     CALL B8TOTH( NDLEV   , NDMET  , NDTEM , NDDEN ,
     &                            NORD    , NMET   , NPL   ,
     &                            IORDR   , IMETR  ,
     &                            IT      , MAXT   , IN    , MAXD  ,
     &                            RATPIA  ,
     &                            STVHM   , STVH   ,
     &                            PLA1    ,
     &                            PHA     , PH
     &                          )
                     CALL B8TOTL( NDLEV          , NDMET   ,
     &                            NORD           , NMET    ,
     &                            IORDR          , IMETR   , ISPTRN ,
     &                            DENSA(IN)      ,
     &                            STCKM(1,IT,IN) , STACK(1,1,IT,IN) ,
     &                            PLA1           , PLBA(1,IT)       ,
     &                            PLA(IT,IN)     , PL(1,IT,IN)      ,
     &                            PSA(1,IT,IN)   , PS(1,1,IT,IN)
     &                          )
    7             CONTINUE

C-----------------------------------------------------------------------
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    4          CONTINUE
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C
C-----------------------------------------------------------------------
C OUTPUT THE GENERALISED COLLISIONAL-RADIATIVE COEFFICIENTS
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C CALCULATE THE POPULATIONS OF EACH LEVEL.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE METASTABLE LEVELS FIRST.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            CALL B8POPM( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   NPL    , NPLR  , NPLI  ,
     &                   MAXT   , MAXD  , NMET  ,
     &                            DENSA , IMETR ,
     &                            LRSEL , LISEL , LHSEL ,
     &                            RATPIA, RATMIA, RATHA ,
     &                   STCKM  , STVRM , STVIM , STVHM ,
     &                   POPAR
     &                 )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE POPULATIONS OF EACH ORDINARY LEVEL.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            CALL B8POPO( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   NPL    , NPLR  , NPLI  ,
     &                   MAXT   , MAXD  , NMET  , NORD  ,
     &                            DENSA , IMETR , IORDR ,
     &                            LRSEL , LISEL , LHSEL ,
     &                            RATPIA, RATMIA, RATHA ,
     &                   STACK  , STVR  , STVI  , STVH  ,
     &                   POPAR
     &                 )


C-----------------------------------------------------------------------
 300    CONTINUE
C-----------------------------------------------------------------------

C***********************************************************************
C END OF CURRENT FILE ANALYSIS - ESTABLISH IF PASSING FILE OUTPUT REQ'D
C***********************************************************************
C
C***********************************************************************
C THE OUTPUT SCREEN IS PUT HERE AS WELL AS BEFORE THE ANALYSIS SO THAT
C THE ANALYSIS DOES NOT HAVE TO BE REPEATED EACH TIME THE USER RETURNS
C FROM GRAPHING. OPUT SAYS WHETHER WE HAVE JUST COME FROM ANALYSIS 
C (.TRUE.) OR FROM THE GRAPHING SCREEN (.FALSE.)
C***********************************************************************

        IF (.NOT. OPUT) THEN

             CALL B8SPF1( NDTEM  , TINE   , MAXT   , IFOUT ,
     &                    LPEND  , LGCR   , UID    ,
     &                    LNEWPA , LPAPER , LCONT  , LPASS ,
     &                    DSNPAP , DSNOUT , DSNPAS , DSNGCR,
     &                    LGPH   , ITSEL  , GTIT1  , CADAS
     &                  )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION(1) OR CONTINUE(0)
C-----------------------------------------------------------------------

             READ(PIPEIN,*) ISTOP
             IF(ISTOP.EQ.1) GOTO 9999
             IF(LPEND) GOTO 200


CX----------------------------------------------------------------------
CX ULTRIX PORT - OPEN TEXT OUTPUT DATA SET - DSNPAP - IF REQUESTED.
CX               THIS IS NEW FUNCTIONALITY.  IF LNEWPA IS .TRUE. THEN
CX               EITHER A NEW FILE IS TO BE CREATED OR AN EXISTING FILE
CX               IS TO BE REPLACED. ANY FILE WHICH IS CURRENTLY OPEN
CX               MUST BE CLOSED FIRST. IF LNEWPA IS .FALSE. AND LPAPER
CX               IS TRUE THEN OUTPUT IS APPENDED TO A PREVIOUSLY OPEN
CX               FILE IF ANY.
CX----------------------------------------------------------------------

             IF ( (LPAPER .AND. LNEWPA) .OR.
     &            (LPAPER . AND. .NOT.OPEN27) ) THEN
                IF (OPEN27) CLOSE(UNIT=IUNT27)
                DSN80 = ' '
                DSN80 = DSNPAP
                OPEN(UNIT=IUNT27,FILE=DSN80,STATUS='UNKNOWN')
                OPEN27=.TRUE.
             ENDIF
     
C------------------------------------------------------------------------
C OUTPUT ION SPECIFICATIONS & LEVEL INFORMATION TO 'IUNT27' (PAPER.TEXT)
C-----------------------------------------------------------------------
CX
CX ULTRIX PORT - ROUTINE MOVED HERE FROM THE BEGINNING OF THE PROGRAM. 
CX               WRITING OF TEXT OUTPUT NOW UNDER USER CONTROL VIA
CX               IDL USER INTERFACE AND LPAPER. 
CX
CX   B8OUT0 IS ONLY CALLED HERE IF THE OUTPUT SCREEN HAS BEEN REDISPLAYED
CX   AND PAPER.TXT HAS BEEN CHOSEN
CX
             IF (LPAPER) THEN
                CALL B8OUT0(  NDMET  , IUNT27 , DATE   ,
     &               PRGTYP , DSNINC , DSNEXP ,
     &               TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &               NPL    , NPLR   , NPLI   , BWNOA  ,
     &               ICNTE  , ICNTP  , ICNTR  , ICNTH  , ICNTI  ,
     &               ICNTL  , ICNTS  ,
     &               IL     ,
     &               IA     , CSTRGA , ISA    , ILA    , XJA    , WA ,
     &               ER     , CPLA   ,
     &               NV     , TSCEF  , CADAS
     &               )
             ENDIF

C CORRESPONDS TO 'IF (.NOT. OPUT) THEN '
          ENDIF

          OPUT = .FALSE.

C-----------------------------------------------------------------------
C OPEN PASSING PEC FILE OUTPUT DATA SET - DSNOUT - IF REQUESTED
C-----------------------------------------------------------------------

               IF ( LCONT .AND. .NOT.OPEN11) THEN
                  DSN80 = ' '
                  DSN80 = DSNOUT
                  OPEN(UNIT=IUNT11,FILE=DSN80,STATUS='UNKNOWN')
                  OPEN11=.TRUE.
               ENDIF

C-----------------------------------------------------------------------
C OPEN PASSING SXB FILE OUTPUT DATA SET - DSNPAS - IF REQUESTED
C-----------------------------------------------------------------------

                IF ( LPASS .AND. .NOT.OPEN12 ) THEN
                   DSN80=' '
                   DSN80=DSNPAS
                   OPEN(UNIT=IUNT12,FILE=DSN80,STATUS='UNKNOWN')
                   OPEN12=.TRUE.
                ENDIF

C-----------------------------------------------------------------------
C OPEN PASSING GCR INFORMATION OUTPUT DATA SET - DSNGCR - IF REQUESTED
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C DSNGCR HAS THE STRING '/.pass' AT THE END WHICH SHOULD BE REPLACED BY
C EACH PASSING FILE NAME. IX STORES THE RELEVANT LENGTH
C-----------------------------------------------------------------------

                IX=INDEX(DSNGCR,'/.pass')-1

                IF ( LGCR .AND. .NOT.OPEN13 ) THEN
                   DSN80=' '
                   DSN80=DSNGCR(1:IX)//'/gcr.pass'
                   OPEN(UNIT=IUNT13,FILE=DSN80,STATUS='UNKNOWN')
                   OPEN13=.TRUE.
                ENDIF

C-----------------------------------------------------------------------
C OPEN PASSING ISO-ELECTRONIC MASTER FILES FOR OUTPUT - IF REQUESTED
C-----------------------------------------------------------------------

             IF ( LGCR ) THEN
               IF(.NOT.OPEN14)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/acd208.pass'
                 OPEN(UNIT=IUNT14,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN14=.TRUE.
               ENDIF
               IF(.NOT.OPEN15)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/scd208.pass'
                 OPEN(UNIT=IUNT15,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN15=.TRUE.
               ENDIF
               IF(.NOT.OPEN16)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/ccd208.pass'
                 OPEN(UNIT=IUNT16,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN16=.TRUE.
               ENDIF
               IF(.NOT.OPEN17)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/qcd208.pass'
                 OPEN(UNIT=IUNT17,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN17=.TRUE.
               ENDIF
               IF(.NOT.OPEN18)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/xcd208.pass'
                 OPEN(UNIT=IUNT18,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN18=.TRUE.
               ENDIF
               IF(.NOT.OPEN19)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/prb208.pass'
                 OPEN(UNIT=IUNT19,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN19=.TRUE.
               ENDIF
               IF(.NOT.OPEN20)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/prc208.pass'
                 OPEN(UNIT=IUNT20,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN20=.TRUE.
               ENDIF
               IF(.NOT.OPEN21)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/plt208.pass'
                 OPEN(UNIT=IUNT21,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN21=.TRUE.
               ENDIF
               IF(.NOT.OPEN22)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/pls208.pass'
C                 OPEN(UNIT=IUNT22,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN22=.FALSE.
               ENDIF
               IF(.NOT.OPEN23)THEN
                 DSN80=' '
                 DSN80=DSNGCR(1:IX)//'/met208.pass'
                 OPEN(UNIT=IUNT23,FILE=DSN80,STATUS='UNKNOWN')
                 OPEN23=.TRUE.
               ENDIF
             ENDIF

C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING PEC AND/OR SXB FILES IF REQUESTED
C-----------------------------------------------------------------------

      call b8wrps( lcont  , iunt11 , lpass  , iunt12 ,
     &             dsninc , dsfull , dsnexp , ibsela ,
     &             titled , date   , user   ,
     &             ndlev  , ndtem  , ndden  , ndmet  , ndtrn ,
     &             ndsel  ,
     &             lnorm  ,
     &             iz     , iz0    , iz1    ,
     &             il     , nmet   , nord   ,
     &             maxt   , maxd   , icntr  , icnti  , icnth ,
     &             isa    , ila    , xja    ,
     &             cstrga , wa     ,
     &             icnte  ,
     &             ie1a   , ie2a   , aa     ,
     &             imetr  , iordr  , teva   , densa  ,
     &             npl    , nplr   , npli   , npl3   ,
     &             lrsel  , lisel  , lhsel  , liosel ,
     &             lpsel  , lzsel  , lnsel  ,
     &             nwvl   , wvls   , wvll   , avlt   ,
     &             ntrsel , itrlow , itrup  ,
     &             stvr   , stvi   , stvh   ,
     &             ratpia , ratmia , stack  ,
     &             fvionr , sgrda  ,
     &             lsseta , lss04a
     &           )

C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING GCR FILE - DSNGCR - IF REQUESTED
C  ******** TEMPORARY USING SUBROUTINE B8WR13L FOR BILLY
C  ******** TEMPORARY USING SUBROUTINE B8WRMC
C
C    NOTE : AS FROM 31/1/92 ARGUMENT LIST FOR B8WRMC IS ALTERED
C    TO INCLUDE PARENT CROSS COUPLING COEFFICIENTS AND
C    LINE POWER COEFFICIENTS
C
C Replace UID with real name USER on 2/9/99
C-----------------------------------------------------------------------

            IF (LGCR ) THEN
               CALL B8WRMC( IUNT13  , IUNT14 , IUNT15 ,IUNT16 , IUNT17 ,
     &              IUNT18 , IUNT19 , IUNT20 , IUNT21 ,IUNT22 , IUNT23 ,
     &              DSNINC , DSFULL , DSNEXP ,
     &              TITLED , DATE   , USER   ,
     &              NDLEV  , NDTEM  , NDDEN  , NDMET ,
     &              LNORM  , IZ     , IZ0    , IZ1    ,
     &              IBSELA , BWNOA  , PRTWTA ,
     &              IL     , NMET   , NORD   , IMETR  ,
     &              IA     , ISA    , ILA    , XJA   ,
     &              CSTRGB , WA     , MAXT   , MAXD  , TEVA   , DENSA  ,
     &              NPL    , NPLR   , NPL3   , NPLI  , CPRTA  ,
     &              LRSEL  , LISEL  , LHSEL  , LIOSEL ,
     &              LPSEL  , LZSEL  , LNSEL  ,
     &              FVCRED , FVRRED , FVIRED , FVHRED , FVIONR ,
     &              FVCRPR , PL     , PH     , PS     , SWVLN  ,
     &              PR     ,
     &              RATPIA , RATMIA , STACK  , STCKM  ,
     *              LSSETA , LSS04A
     &                            )
             ENDIF

C------------------------------------------------------------------------
C OUTPUT DATA TO PAPER.TEXT.
C-----------------------------------------------------------------------

             IF (LPAPER) THEN
                      CALL B8OUT1( IUNT27 ,
     &                    NDLEV  , NDTEM  , NDDEN , NDMET ,
     &                    LNORM  ,
     &                    IL     , NMET   , NORD  ,
     &                    NPL    , NPLR   , NPLI  , NPL3   ,
     &                    MAXT   , MAXD   , ZEFF  ,
     &                    ICNTP  , ICNTR  , ICNTI , ICNTH  ,
     &                    LPSEL  , LZSEL  , LIOSEL, LHSEL , LRSEL ,
     &                    LISEL  ,
     &                    LMETR  , IMETR  , IORDR ,
     &                    STRGA  ,
     &                    LTRNG  , TEA    , TEVA  , TPVA  , THVA  ,
     &                    DENSA  , DENSPA , RATHA , RATPIA, RATMIA,
     &                    POPAR  ,
     &                    STCKM  , STVR   , STVI  , STVH  ,
     &                    STVRM  , STVIM  , STVHM  , STACK
     &                  )
             ENDIF

CX-----------------------------------------------------------------------
CX ULTRIX PORT - CLOSE OPEN PASSING FILE DATASETS. THIS IS NEW
CX               FUNCTIONALITY.
CX-----------------------------------------------------------------------

            IF (LCONT) THEN
               CLOSE(UNIT=IUNT11)
               OPEN11=.FALSE.
            ENDIF
            IF (LPASS) THEN
               CLOSE(UNIT=IUNT12)
               OPEN12=.FALSE.
            ENDIF
            IF (LGCR) THEN
              CLOSE(UNIT=IUNT13)
              CLOSE(UNIT=IUNT14)
              CLOSE(UNIT=IUNT15)
              CLOSE(UNIT=IUNT16)
              CLOSE(UNIT=IUNT17)
              CLOSE(UNIT=IUNT18)
              CLOSE(UNIT=IUNT19)
              CLOSE(UNIT=IUNT20)
              CLOSE(UNIT=IUNT21)
              CLOSE(UNIT=IUNT22)
              CLOSE(UNIT=IUNT23)
              OPEN13=.FALSE.
              OPEN14=.FALSE.
              OPEN15=.FALSE.
              OPEN16=.FALSE.
              OPEN17=.FALSE.
              OPEN18=.FALSE.
              OPEN19=.FALSE.
              OPEN20=.FALSE.
              OPEN21=.FALSE.
              OPEN22=.FALSE.
              OPEN23=.FALSE.
            ENDIF

CX----------------------------------------------------------------------
CX SET UP GRAPHICAL OUTPUT IF REQUESTED.
CX----------------------------------------------------------------------

              IF (LGPH) THEN
                 CALL B8OUTG( LGHOST , DATE  ,
     &                NDLEV  , NDTEM , NDDEN , NDMET  ,
     &                TITLED , TITLE , GTIT1 , DSNINC ,
     &                IZ     , ITSEL , TEVA(ITSEL)    ,
     &                LGRD1  , LDEF1 ,
     &                XL1    , XU1   , YL1   , YU1    ,
     &                IL     , NMET  , NORD  , MAXD   ,
     &                LMETR  , IMETR , IORDR , DENSA  ,
     &                STRGA  , STACK
     &                )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

                 READ(PIPEIN,*) ISTOP
                 IF(ISTOP.EQ.1) GOTO 9999

              ENDIF

C----------------------------------------------------------------------
          GOTO 300
C----------------------------------------------------------------------
 9999 continue
      stop
C-----------------------------------------------------------------------
 1000  FORMAT('SZD COEFF. Z0=',I2,2X,'Z=',I2,2X,'Z1=',I2,
     &        2X,'LEV=',1A12,2X,'(',I1,')',I1,'(',F4.1,')',
     &        ' TO (Z+1,',I1,')')
C-----------------------------------------------------------------------
      END
