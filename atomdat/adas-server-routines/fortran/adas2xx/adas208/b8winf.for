      SUBROUTINE B8WINF( IUNIT  , LADF10 , DATE   , USER    ,
     &                   NDLEV  ,
     &                   DSNINC , DSFULL , DSNEXP ,
     &                   IZ0    , IZ1    ,
     &                   IL     , NMET   , NPL    , IBSELA  ,
     &                   LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                   LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                   LSSETA , LSS04A
     &                 )
      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B8WINF *******************
C
C  PURPOSE:  Write information section of adf10, adf13 and adf15 files.
C
C
C  CALLING PROGRAM: ADAS208 - b8wrmc, b8wr11, b8wr12
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = OUTPUT UNIT NUMBER
C  INPUT : (I*4)  LADF10   = .TRUE. IF WRITING COMMENTS TO ADF10 FILE.
C  INPUT : (C*8)  DATE     = CURRENT DATE.
C  INPUT : (C*30) USER     = USER IDENTIFIER
C  INPUT : (I*4)  NDLEV    = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDMET    = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (C*80) DSNINC   = INPUT COPASE DATA SET NAME (IN QUOTES).
C  INPUT : (C*80) DSFULL   = INPUT SZD  DATA SET NAME (IN QUOTES).
C  INPUT : (C*80) DSNEXP   = INPUT EXPANSION FILE
C  INPUT : (I*4)  IZ0      =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ1      = RECOMBINING ION CHARGE READ
C                            (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (I*4)  IL       = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  NMET     = NUMBER OF METASTABLES LEVELS: 1<=NMET<=NDMET
C  INPUT : (I*4)  NPL      = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                              BY EXCITED STATE IONISATION IN COPASE
C                              FILE WITH IONISATION POTENTIALS GIVEN
C                              ON THE FIRST DATA LINE
C  INPUT : (I*4)  IBSELA(,)=IONISATION DATA BLOCK SELECTOR INDICES
C                           1ST DIMENSION - (Z)   ION METASTABLE COUNT
C                           2ND DIMENSION - (Z+1) ION METASTABLE COUNT
C  INPUT : (L*4)  LRSEL    = .TRUE.  -  RECOMB  OF (Z+1) ION ACTIVE
C                            .FALSE. -  RECOMB. OF (Z+1) ION INACTIVE
C  INPUT : (L*4)  LISEL    = .TRUE.  -  IONIS.  OF (Z-1) ION ACTIVE
C                            .FALSE. -  IONIS.  OF (Z-1) ION INACTIVE
C  INPUT : (L*4)  LHSEL    = .TRUE.  -  CX REC. OF (Z+1) ION ACTIVE
C                            .FALSE. -  CX REC. OF (Z+1) ION INACTIVE
C  INPUT : (L*4)  LIOSEL   = .TRUE.  -  IONIS.  OF (Z) ION ACTIVE
C                            .FALSE. -  IONIS.  OF (Z) ION INACTIVE
C  INPUT : (L*4)  LPSEL    = .TRUE.  => INCLUDE PROTON COLLISIONS
C                          = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  INPUT : (L*4)  LZSEL    = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                       PLASMA Z EFFECTIVE'ZEFF'.
C                          = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                       WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                            (ONLY USED IF 'LPSEL=.TRUE.')
C  INPUT : (L*4)  LNSEL    = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                        FROM DATAFILE IF AVAILABLE
C                          = .FALSE. => DO NOT INCLUDE PROJECTED
C                                       BUNDLE-N DATA
C  INPUT : (L*4)  LNORM    = .TRUE.  => IF NMET=1 THEN VARIOUS
C                                       IONISATION OUTPUT FILE
C                                       NORMALISED TO STAGE TOT.POPULATN.
C                                       (** NORM TYPE = T)
C                          = .FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                       METASTABLE POPULATIONS.
C                                        (** NORM TYPE = M)
C  INPUT : (L*4)  LSSETA(,)= .TRUE.  => MET. IONIS RATE SET IN B8GETS
C                            .FALSE. => MET. IONIS RATE NOT SET IN B8GETS
C                             1ST DIMENSION: (Z) ION METASTABLE INDEX
C                             2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C  INPUT : (L*4)  LSS04A(,)= .TRUE.  => IONIS. RATE SET IN ADF04 FILE:
C                            .FALSE. => NOT SET IN ADF04 FILE
C                             1ST DIM: LEVEL INDEX
C                             2ND DIM: PARENT METASTABLE INDEX
C
C
C  INTERNAL:
C
C          (L*4)  LION     = .TRUE.  => SOME/ALL MET. IONIS RATE IS SET
C          (L*4)  LIONALL  = .TRUE.  => ALL MET. IONIS RATES ARE SET
C          (L*4)  LIONA    = .TRUE.  => ALL MET. IONIS RATES FROM ADF04 FILE
C          (L*4)  LIONOV   = .TRUE.  => SOME/ALL MET. IONIS RATES TAKEN
C                                       FROM ADF07 FILE
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSLEN     ADAS     FINDS LENGTH OF NON-BLANK STRINGS
C
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    8/10/99
C
C VERSION : 1.1
C DATE    : 8/10/99
C MODIFIED: Martin O'Mullane
C           - First version
C
C VERSION : 1.2
C DATE    : 21/03/00
C MODIFIED: Martin O'Mullane
C               - Removed NDMET from input parameter list
C
C VERSION : 1.3
C DATE    : 31-07-2009
C MODIFIED: Martin O'Mullane
C               - Do not write anything if no expansion file set.
C
C VERSION : 1.4
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Increase size of filename variables from 80 to 132 characters.
C           - Trim whitespace from the producer's name.
C
C VERSION : 1.5
C DATE    : 05-12-2017
C MODIFIED: Matthew Bluteau
C           - Increase number of metastables to 13 (up from 4).
C
C-----------------------------------------------------------------------
      INTEGER   IUNIT     , NMET       , NDMET   , NPL     ,
     &          NDLEV     , IL         , IZ0     , IZ1
      INTEGER   II        , IP         , i
      INTEGER   L1        , L2         , L3      , L4      , L5     ,
     &          L6
C-----------------------------------------------------------------------
      PARAMETER ( NDMET = 13 )
C-----------------------------------------------------------------------
      CHARACTER DATE*8    , USER*30
      CHARACTER DSNINC*132, DSFULL*132 , DSNEXP*132
      character str*132
C-----------------------------------------------------------------------
      LOGICAL   LRSEL     , LISEL      , LHSEL   , LIOSEL
      LOGICAL   LPSEL     , LZSEL      , LNSEL   , LNORM
      logical   ladf10
      logical   lionall   , lion       , liona   , lionov
C-----------------------------------------------------------------------
      INTEGER   IBSELA(NDMET,NDMET)
C-----------------------------------------------------------------------
      character source(NDMET,NDMET)*3
C-----------------------------------------------------------------------
      LOGICAL   LSSETA(NDMET,NDMET)   , LSS04A(NDLEV,NDMET)
C-----------------------------------------------------------------------




C Find lengths of files

       call xxslen(DSNINC,L1,L2)
       call xxslen(DSFULL,L3,L4)

C Was an expansion file used

       call xxslen(DSNEXP,L5,L6)

       if (.NOT.LNSEL) then
          str = 'No projection data was used in this case.'
       else
          str = dsnexp
       endif
       call xxslen(str,L5,L6)


C Find the source of the ionisation data

       lionall = .TRUE.
       lion    = .FALSE.
       liona   = .TRUE.
       lionov  = .FALSE.
       do i=1, nmet
         do ip=1,npl
           lionall = lionall.and.lsseta(i,ip)
           lion    = lion.or.lsseta(i,ip)
           liona   = liona.and.lss04a(i,ip)
           lionov  = lionov.or.(ibsela(i,ip).NE.0)
         end do
       end do


       if (ladf10) WRITE(IUNIT,2004)

       if (L5.eq.0.and.L6.eq.0) then
          write(iunit,2041)iz0,iz1, dsninc(l1:l2)
       else
          write(iunit,2042)iz0,iz1, dsninc(l1:l2), str(l5:l6)
       endif

       if (lion) then

          if (.not.lionall) WRITE(IUNIT,2060)
          if (.not.liona.or.lionov) then
             if (L3.LT.L4) WRITE(IUNIT,2070)DSFULL(L3:L4)
          endif

          do i=1, nmet
            do ip=1,npl
              write(source(i,ip),'(a3)')'  N'
              if (lss04a(i,ip)) write(source(i,ip),'(a3)')'  *'
              if (ibsela(i,ip).NE.0)
     &            write(source(i,ip),'(i3)')IBSELA(I,IP)
            end do
          end do

          WRITE(IUNIT,2010) (source(1,IP),1,IP,IP=1,NPL)
          IF (NMET.GT.1)THEN
              DO II=2,NMET
                WRITE(IUNIT,2020) (source(II,IP),II,IP,IP=1,NPL)
              END DO
          ENDIF
          WRITE(IUNIT,2030)

       else
          WRITE(IUNIT,2050)
       endif

       WRITE(IUNIT,2015) LNORM,LPSEL,LZSEL,LIOSEL,
     &                   LHSEL,LRSEL,LISEL,LNSEL

       if (ladf10) then       
          call xxslen(user,L1,L2)
          WRITE(IUNIT,2014) 'ADAS208',USER(L1:L2),DATE
          WRITE(IUNIT,2004)
       endif

C-----------------------------------------------------------------------

 2004  FORMAT(80('-'))

 2010   FORMAT('C '/,
     &         'C  META. ION. COEFF. SELECTOR:',
     &            13(A3,'(',I2,',',I2,') '))
 2020 FORMAT('C',29X,13(A3,'(',I2,',',I2,') '))
 2030 FORMAT('C',29X,' * - rate from specific ion file',/,
     &       'C',29X,' N - no rate available',/
     &       'C')


 2014  FORMAT('C',/,
     &        'C  CODE     : ',1A7,/,
     &        'C  PRODUCER : ',A,/,
     &        'C  DATE     : ',1A8,/,
     &        'C')
 2015  FORMAT('C  OPTIONS  : ','LNORM=',L1,2X,'LPSEL=',L1,2X,
     &                        'LZSEL=',L1,2X,'LIOSEL=',L1/
     &        'C            ',' LHSEL=',L1,2X,'LRSEL=',L1,2X,
     &                        'LISEL=',L1,2X,'LNSEL =',L1)


 2041  FORMAT('C '/,
     &        'C  INFORMATION'/,
     &        'C  -----------'/,
     &        'C '/,
     &        'C  NUCLEAR CHARGE = ',I2,/,
     &        'C  ION CHARGE +1  = ',I2,/,
     &        'C '/,
     &        'C  SPECIFIC ION FILE  : ',A,/,
     &        'C  EXPANSION FILE     : No projection used')

 2042  FORMAT('C '/,
     &        'C  INFORMATION'/,
     &        'C  -----------'/,
     &        'C '/,
     &        'C  NUCLEAR CHARGE = ',I2,/,
     &        'C  ION CHARGE +1  = ',I2,/,
     &        'C '/,
     &        'C  SPECIFIC ION FILE  : ',A,/,
     &        'C  EXPANSION FILE     : ',A)

 2050  FORMAT('C '/,
     &        'C  No ionisation data has been included',/,'C')
 2060  FORMAT('C '/,
     &        'C  Partial ionisation data provided',/,'C')
 2070  FORMAT('C '/,
     &        'C  IONIS. COEFF. FILE : ',A)
C-----------------------------------------------------------------------

       RETURN
       END
