C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8spf0.for,v 1.2 2013/03/18 23:21:29 mog Exp $ Date $Date: 2013/03/18 23:21:29 $
C
      SUBROUTINE B8SPF0( REP    ,
     &                   DSNINP , DSNINX , DSNINC 
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8SPF0 *********************
C
C  PURPOSE: TO FETCH DATA SET NAME AND EXPANSION FILE NAME FROM IDL VIA
C           UNIX PIPE.
C           (INPUT DATA SET SPECIFICATIONS).
C           *** IDENTICAL TO B6SPF0 EXCEPT DTABLE='P20809' ***
C           ***                     AND    DPANEL='P20809' ***
C
C  CALLING PROGRAM: ADAS208
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSNINP  = INPUT PROTON DATA SET NAME (SEQUENTIAL)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C  OUTPUT: (C*80)  DSNINX  = INPUT EXPANSION DATA SET NAME (SEQUENTIAL)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C  OUTPUT: (C*80)  DSNINC  = INPUT COPASE DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C
C AUTHOR:  D H BROOKS (UNIV>OF STRATHCLYDE)  03-MAY-1996
C          CUT OUT EVERYTHING FROM B8SPF0 FROM IBM AND REPLACED IT
C          WITH IDL-ADAS ALTERATIONS.
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C VERSION : 1.2
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Increase size of filename variables from 80 to 132 characters.
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3   , DSNINX*132 , DSNINC*132 , DSNINP*132
C-----------------------------------------------------------------------
      INTEGER      PIPEIN       , PIPEOU   ,  I4UNIT
C-----------------------------------------------------------------------
      PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
C ALTERATIONS FOR IDL ADAS
C-----------------------------------------------------------------------
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSNINC
      READ(PIPEIN,'(A)') DSNINX
C-----------------------------------------------------------------------
      RETURN
      END
