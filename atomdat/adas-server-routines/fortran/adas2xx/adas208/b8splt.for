      SUBROUTINE B8SPLT( NTDIM  , NDDIM  ,
     &                   ITA    , IDA    , ITVAL   , IDVAL  ,
     &                   TETA   , TEDA   , TOUT    , DOUT   ,
     &                   CINA   , COUTA  ,
     &                   LTRNG  , LDRNG  ,
     &                   iopt_te, iopt_dens
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8SPLT *********************
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE AND DENSITY)
C          VERSUS LOG(COLLISIONAL-RADIATIVE MATRIX COEFFICIENTS)
C          INPUT DATA
C
C          USING  TWO-WAY SPLINES IT CALCULATES  THE  INTERPOL. COEFFTS.
C          FOR  'ITVAL' ELECTRON TEMPERATURES  AND  'IDVAL' DENSITIES
C          FROM THE TWO-DIMENSIONAL TABLE OF TEMPERATURES/DENSITIES READ
C          IN FROM THE INPUT FILE. IF A  VALUE  CANNOT  BE  INTERPOLATED
C          USING SPLINES IT IS EXTRAPOLATED VIA 'XXSPLE'.
C
C          This is a special version of B8SPLN which examines the input
C          data for zeros (effective zeros of 1.0D-72) and eliminates
C          these from the spline. This is to avoid NaNs propagating
C          throughout the population calculation. So far only one
C          array is affected - PCRMAT in B8GETP. Only do a spline
C          fit if at least half of the points are present.
C
C  CALLING PROGRAM: ADAS208/B8GETP
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NTDIM    = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDIM    = MAX NUMBER OF ELECTRON DENSITIES    ALLOWED
C
C  INPUT : (I*4)  ITA      = INPUT DATA  : NUMBER OF ELECTRON TEMPERA-
C                            TURES
C  INPUT : (I*4)  IDA      = INPUT DATA  : NUMBER OF ELECTRON DENSIT-
C                            IES
C  INPUT : (I*4)  ITVAL    = OUTPUT DATA : NUMBER OF TEMPERATURES
C  INPUT : (I*4)  IDVAL    = OUTPUT DATA : NUMBER OF DENSITIES
C
C  INPUT : (R*8)  TETA()   = INPUT DATA  : ELECTRON TEMPERATURES (K)
C  INPUT : (R*8)  TEDA()   = INPUT DATA  : ELECTRON DENSITIES (CM-3)
C  INPUT : (R*8)  TOUT()   = OUTPUT DATA : ELECTRON TEMPERATURES (K)
C  INPUT : (R*8)  DOUT()   = OUTPUT DATA : ELECTRON DENSITIES (CM-3)
C
C  INPUT : (R*8)  CINA(,)  = INPUT DATA FILE: FULL SET OF COLL. RAD.
C                             COEFFICIENTS FOR THE DATA-BLOCK BEING
C                             ANALYSED.
C                             1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                             2ND DIMENSION: ELECTRON DENSITY     INDEX
C INPUT :  (I*4)  IOPT_TE  = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                            SPLINE ROUTOUTE XXSPLE FOR EXTRAPOLATION
C                            IN TEMPERATURE.
C INPUT :  (I*4)  IOPT_DENS= DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                            SPLINE ROUTOUTE XXSPLE FOR EXTRAPOLATION
c                            IN DENSITY.
C
C  OUTPUT: (R*8)  COUTA(,) = SPLINE INTERPOLATED COLL. RAD. CEOFFICIENTS
C                            THE USER ENTERED TEMPERATURES AND DENSITIES
C                            1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2ND DIMENSION: ELECTRON DENSITY     INDEX
C
C  OUTPUT: (L*4)  LTRNG()  =  .TRUE.  => OUTPUT 'COUTA()' VALUE WAS INTER-
C                                        POLATED  FOR  THE  USER  ENTERED
C                                        ELECTRON TEMPERATURE 'TOUT'()'.
C                             .FALSE. => OUTPUT 'COUTA()' VALUE WAS EXTRA-
C                                        POLATED  FOR  THE  USER  ENTERED
C                                        ELECTRON TEMPERATURE 'TOUT'()'.
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (L*4)  LDRNG()  =  .TRUE.  => OUTPUT 'COUTA()' VALUE WAS INTER-
C                                        POLATED  FOR  THE  USER  ENTERED
C                                        ELECTRON DENSITY 'DOUT()'.
C                             .FALSE. => OUTPUT 'COUTA()' VALUE WAS EXTRA-
C                                        POLATED  FOR  THE  USER  ENTERED
C                                        ELECTRON DENSITY 'DOUT()'.
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF INPUT  TEMP/DENSITY
C                                       VALUES. MUST BE >= 'ITA'&'IDA'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF OUTPUT TEMP/DENSITY
C                                       PAIRS.  MUST BE >= 'ITVAL'
C          (I*4)  IED     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           DENSITIES.
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           TEMPERATURES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           TEMPERATURES.
C          (I*4)  IN      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           DENSITIES.
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTOUT'E 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATOUT'G
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATOUT'G TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTOUT'ES SECTION BELOW)
C
C          (R*8)  XIN()   = 1) LOG( DATA FILE ELECTRON DENSITIES    )
C                           2) LOG( DATA FILE ELECTRON TEMPERATURES )
C          (R*8)  YIN()   = LOG( INPUT COLL. RAD COEFFTS.)
C          (R*8)  XOUT()  = 1) LOG( SCALED USER ENTERED ELECTRON DENS. )
C                           2) LOG( SCALED USER ENTERED ELECTRON TEMPS.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED IONIZATIONS/PHOTON )
C          (R*8)  YPASS(,)= LOG( COL. RAD. COEFFTS.) INTERMEDIATE ARRAY
C                           WHICH   STORES   INTERPOLATED/EXTRAPOLATED
C                           VALUES  BETWEEN  THE  TWO  SPLINE SECTIONS.
C                           SECTIONS.
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C ROUTOUT'ES:
C          ROUTOUT'E  SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTOUT'E (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H.P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/07/92
C
C
C
C VERSION : 1.1
C DATE    : 10/05/96
C MODIFIED: Martin O'Mullane
C           - First version
C
C VERSION : 1.2
C DATE    : 24/09/2004
C MODIFIED: Martin O'Mullane
C           - The check to avoid integrating over zeros in the input
C             can result in no valid points. This causes xxsple an
C             out of bounds error in xxsple. Add a check to avoid
C             the call in this case.
C
C VERSION : 1.3
C DATE    : 17-09-2009
C MODIFIED: Martin O'Mullane
C           - Remove effective zeros in spline in both Te and Ne
C             directions.
C
C VERSION : 1.4
C DATE    : 08-05-2016
C MODIFIED: Martin O'Mullane
C           - xin was set incorrectly when excluding temperature 
C             effective-zeros in the density loop.
C           - Use splined data when more than half the input data
C             is non-zero, ie when ITA/2 or IDA/2 rather than NIN/2.
C
C VERSION : 1.5
C DATE    : 10-04-2018
C MODIFIED: Martin O'Mullane
C           - Add separate extrapolation options for temperature
C             and density.
C           - Set a threshold value and use a consistent log value
C             for interim comparisons.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN                   , NOUT
C-----------------------------------------------------------------------
      PARAMETER( NIN = 35              , NOUT = 35        )
C-----------------------------------------------------------------------
      INTEGER    NTDIM                 , NDDIM           ,
     &           ITA                   , IDA             , ITVAL
      INTEGER    IET                   , IED             , IT         ,
     &           IOPT                  , IN              , IDVAL      ,
     &           NUMV
      integer    iopt_te               , iopt_dens
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
      real*8     thresh                , thresh_ln
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TETA(ITA)             , TEDA(IDA)       ,
     &           TOUT(ITVAL)           , DOUT(IDVAL)     ,
     &           COUTA(NTDIM,NDDIM)    ,
     &           CINA(NTDIM,NDDIM)
      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)      ,
     &                                   YPASS(NOUT,NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)          , LDRNG(IDVAL)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
      
C-----------------------------------------------------------------------
      IF (NIN.LT.IDA)
     &             STOP ' B8SPLN ERROR: NIN < IDA - INCREASE NIN'
      IF (NIN.LT.ITA)
     &             STOP ' B8SPLN ERROR: NIN < ITA - INCREASE NIN'
      IF (NOUT.LT.ITVAL)
     &             STOP ' B8SPLN ERROR: NOUT < ITVAL - INCREASE NTOUT'

      thresh    = 1.0D-60
      thresh_ln = DLOG(thresh)
      
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------

      DO IET=1,ITA
         XIN(IET) = DLOG( TETA(IET) )
      END DO

      DO IT=1,ITVAL
         XOUT(IT) = DLOG( TOUT(IT) )
      END DO

C-----------------------------------------------------------------------
C Spline over all dataset electron densities for each user electron
C temperature but remove any effective zeros before splining.
C-----------------------------------------------------------------------

      iopt  = iopt_te

      DO IED=1,IDA

        numv=0
        DO IET=1,ITA
           if (cina(iet,ied).gt.thresh) then
               numv = numv+1
               xin(numv) = DLOG( TETA(IET) )
               YIN(numv) = DLOG( CINA(IET,IED) )
           endif
        END DO

        if (numv.gt.1) then

 10        continue       
           LSETX = .TRUE.
           CALL XXSPLE( LSETX , iopt    , R8FUN1 ,
     &                  numv  , XIN     , YIN    ,
     &                  ITVAL , XOUT    , YOUT   ,
     &                  DF    , LTRNG
     &                )

           if (numv.gt.ita/2) then
               DO IT=1,ITVAL
                  YPASS(IT,IED) = YOUT(IT)
               END DO
           else
               DO IT=1,ITVAL
                  YPASS(IT,IED) = thresh_ln
               END DO
           endif
           
        else
        
           do it=1,itval
              ypass(it,ied) = thresh_ln
           end do
        
        endif
      
      END DO


C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON DENSITIES
C-----------------------------------------------------------------------

      DO IN=1,IDVAL
         XOUT(IN) = DLOG( DOUT(IN) )
      END DO

C-----------------------------------------------------------------------
C SPLINE OVER ALL USER DENSITIES FOR EACH USER TEMPERATURE
C-----------------------------------------------------------------------

      iopt  = iopt_dens
      
      DO IT=1,ITVAL

         NUMV=0
         DO IED=1,IDA
           IF (YPASS(IT,IED).GE.thresh_ln) THEN
              NUMV = NUMV+1
              XIN(NUMV) = DLOG( TEDA(IED) )
              YIN(NUMV) = YPASS(IT,IED)
           endif
         END DO

         if (numv.GT.1) then

            LSETX = .TRUE.
            CALL XXSPLE( LSETX  , IOPT       , R8FUN1   ,
     &                   NUMV   , XIN        , YIN      ,
     &                   IDVAL  , XOUT       , YOUT     ,
     &                   DF     , LDRNG
     &                 )

C-----------------------------------------------------------------------
C SET UP OUTPUT COEFFICIENTS
C-----------------------------------------------------------------------

            DO IN=1,IDVAL
               if (NUMV.gt.ida/2) then
                  COUTA(IT,IN) = DEXP( YOUT(IN) )
               else
                  COUTA(IT,IN) = thresh
               endif
            END DO

         else

             DO IN=1,IDVAL
                COUTA(IT,IN) = thresh
             END DO

         endif

      END DO

C-----------------------------------------------------------------------

      RETURN
      END
