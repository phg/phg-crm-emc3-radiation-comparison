C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8stvm.for,v 1.1 2004/07/06 11:31:39 whitefor Exp $ Date $Date: 2004/07/06 11:31:39 $
C
      SUBROUTINE B8STVM( NDMET  ,
     &                   NMET   ,
     &                   CRMAT  ,
     &                   IP     ,
     &                   VRED   ,
     &                   STVM
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8STVM *********************
C
C  PURPOSE: TO  CALCULATE  AND STACK UP IN  'STVM'  THE METASTABLE LEVEL
C           RECOMBINATION  COEFFICIENTS  FOR  A  GIVEN  TEMPERATURE  AND
C           DENSITY.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (R*8)  CRMAT(,)= INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C           (I*4)  IP      = PARENT INDEX
C
C  INPUT :  (R*8)  VRED(,) = VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                            FOR EACH METASTABLE LEVEL.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C  OUTPUT:  (R*8)  STVM()  = RECOMBINATION  CONTRIBUTION  FOR  EACH
C                            METASTABLE LEVEL. (UNITS: CM**3)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            (LEVEL 1 IS TAKEN AS ZERO)
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C           (I*4)  IM1     = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IM2     = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C NOTE:
C           STVM(IM1)       SUM( (the transistion rate from IM2 to IM1)
C                                x (the recombination rate contribution
C                                   for metastable level IM2) )
C
C                           (IM1 & IM2 = METASTABLE LEVEL INDEX)
C
C                           ABOVE SUM IS OVER ALL METASTABLE LEVELS
C                           EXCEPT LEVEL ONE.
C
C
C AUTHOR:  HP SUMMERS ( UPGRADE OF BXSTVM BY PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/06/92
C
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDMET               , NMET
      INTEGER    IM1                 , IM2                , IP
C-----------------------------------------------------------------------
      REAL*8     CRMAT(NDMET,NDMET)  , VRED(NDMET,NDMET)      ,
     &           STVM(NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      STVM(1)=0.0D0
         DO 1 IM1=2,NMET
            STVM(IM1)=0.0D0
               DO 2 IM2=2,NMET
                  STVM(IM1) = STVM(IM1) -
     &                        ( CRMAT(IM1-1,IM2-1) * VRED(IM2,IP) )
    2          CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
