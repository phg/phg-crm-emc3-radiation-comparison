C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8rcom.for,v 1.1 2004/07/06 11:30:39 whitefor Exp $ Date $Date: 2004/07/06 11:30:39 $
C
      SUBROUTINE B8RCOM( NDTEM  , NDTRN  , NDLEV  , NDMET   ,
     &                   NTIN   , TIN    , RCIN   ,
     &                   NTOUT  , TOUT   ,
     &                   ICNT   , ITRN   , ICLEV  , IC2LEV  ,
     &                   RCOUT  , LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8RCOM *********************
C
C  PURPOSE: TO ESTABLISH RECOMBINATION RATE COEFFICIENTS  FOR  A SET  OF
C           TEMPERATURES GIVEN BY THE ARRAY 'TOUT()' USING CUBIC SPLINES
C           ON  A SET OF RATE  COEFFICIENTS  COVERING  THE  TEMPERATURES
C           GIVEN BY THE ARRAY 'TIN()'.
C
C           RECOMBINATION TYPE IS SELECTED VIA 'ICNT' & 'ITRN'
C
C           RATE COEFFICIENTS ARE GIVEN FOR A NUMBER OF CAPTURING LEVELS
C           AND THE ARRAY 'RCOUT(,,)' REPRESENTS  COEFFTS.  FOR COMB-
C           INATIONS OF TEMPERATURE, CAPTURING LEVEL INDEX AND PARENT
C           INDEX.
C
C           SPLINE IS CARRIED OUT USING LOG(RATE COEFFICIENT VALUES)
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF RECOMBINATIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT :  (I*4)  NTIN    = NUMBER OF TEMPERATURES REPRESENTED IN THE
C                            INPUT DATA SET.
C  INPUT :  (R*8)  TIN()   = TEMPERATURES REPRESENTED IN INPUT DATA SET
C  INPUT :  (R*8)  RCIN(,) = RATE COEFF. REPRESENTED IN INPUT DATA SET
C                            1st DIMENSION: TEMPERATURE INDEX ('TIN')
C                            2nd DIMENSION: RECOMBINATION INDEX
C                                           (SEE: 'ITRN()')
C
C  INPUT :  (I*4)  NTOUT   = NUMBER OF ISPF SELECTED TEMPERATURES FOR
C                            OUTPUT.
C  INPUT :  (R*8)  TOUT()  = ISPF SELECTED TEMPERATURES FOR OUTPUT.
C
C  INPUT :  (I*4)  ICNT    = NUMBER OF SELECTED RECOMBINATIONS
C  INPUT :  (I*4)  ITRN()  = INDEX VALUES IN MAIN TRANSITION ARRAY WHICH
C                            REPRESENT RECOMBINATIONS  OF THE  SELECTED
C                            TYPE
C                            USED TO SELECT APPROPRIATE RATE COEFFTS FOR
C                            RECOMBINATION TYPE.
C  INPUT :  (I*4)  ICLEV() = CAPTURING LEVELS INDICES.
C                            DIMENSION: 'TRANSITION'/RECOMBINATION INDEX
C  INPUT :  (I*4)  IC2LEV()= PARENT INDEX.
C                            DIMENSION: 'TRANSITION'/RECOMB/IONIS  INDEX
C
C  OUTPUT:  (R*8)  RCOUT(,,)= SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C                            3RD DIMENSION: PARENT INDEX.
C
C  OUTPUT:  (L*4)  LTRNG() = .TRUE. => TEMPERATURE VALUES WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                          = .FALSE.=>TEMPERATURE VALUE NOT WITHIN RANGE
C                                     READ FROM INPUT COPASE DATA SET.
C                            1st DIMENSION: TEMPERATURE INDEX.
C
C
C           (I*4)  NTDSN   = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C                                        ALLOWED IN INPUT DATA SET = 14
C           (I*4)  NLTEM   = PARAMETER = MUST BE >= 'NDTEM'
C
C           (I*4)  IOPT    = SPLINE END CONDITIONS/EXTRAPOLATION CONTROL
C                            SWITCH - SEE 'XXSPLE'
C                            I.E. DEFINES THE BOUNDARY DERIVATIVES.
C                            (VALID VALUES = 0, 1, 2, 3, 4)
C           (I*4)  IRECMB  = APPROPRIATE RECOMBINATN INDEX FOR 'RCIN(,)'
C           (I*4)  ICAP    = CAPTURING LEVEL INDEX BEING ASSESSED.
C           (I*4)  IC      = RECOMBINATION ARRAY INDEX
C           (I*4)  IP      = PARENT INDEX
C           (I*4)  IT      = TEMPERATURE ARRAY INDEX
C
C           (R*8)  DYIN()  = INTERPOLATED DERIVATIVES
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C
C           (L*4)  LSETX   = .TRUE.  => X-AXES ('TIN()' VALUES) NEED TO
C                                       SET IN 'XXSPLE'.
C                            .FALSE. => X-AXES ('TIN()' VALUES) HAVE
C                                       BEEN SET IN 'XXSPLE'.
C                            (NOTE: 'LSETX' IS RESET BY 'XXSPLE')
C
C           (R*8)  LRCIN() = LOG ( 'RCIN(,)' ) FOR GIVEN CAPTURING LEVEL
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C           (R*8)  LRCOUT()= LOG ( SPLINED RECOMB.IONIS  RATE COEFTS )
C                            DIMENSION: TEMPERATURE INDEX ('TOUT()' )
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C
C
C AUTHOR:  HP SUMMERS (UPGRADE OF BXRCOM BY PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/06/92
C
C UPDATE:  12/07/93  HPS - MODIFICATIONS TO MAKE CONSISTENT WITH
C                          LATEST VERSION OF B8DATA
C
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NTDSN              , NLTEM
C-----------------------------------------------------------------------
      PARAMETER( NTDSN = 14         , NLTEM = 101        )
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDTEM              , NDLEV      ,
     &           NDMET              ,
     &           NTIN               , NTOUT              ,
     &           ICNT
      INTEGER    IOPT               , IRECMB             , ICAP       ,
     &           IC                 , IT                 , IP
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      INTEGER    ICLEV(NDTRN)       , IC2LEV(NDTRN)      , ITRN(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     TIN(NTDSN)         , RCIN(NTDSN,NDTRN)  ,
     &           TOUT(NDTEM)        ,
     &           RCOUT(NDTEM,NDLEV,NDMET)
      REAL*8     DYIN(NTDSN)        ,
     &           LRCIN(NTDSN)       , LRCOUT(NLTEM)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTEM)
C-----------------------------------------------------------------------
      INTRINSIC  DLOG
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NLTEM.LT.NDTEM) STOP
     &                   ' B8RCOM ERROR: NDTEM > NLTEM. INCREASE NLTEM'
C-----------------------------------------------------------------------
      LSETX = .TRUE.
      IOPT  = 0
C***********************************************************************
         DO 1 IC=1,ICNT
            IRECMB=ITRN(IC)
C-----------------------------------------------------------------------
               DO 2 IT=1,NTIN
                  LRCIN(IT)=DLOG( RCIN(IT,IRECMB) )
    2          CONTINUE
C-----------------------------------------------------------------------
            CALL XXSPLE( LSETX , IOPT  , DLOG   ,
     &                   NTIN  , TIN   , LRCIN  ,
     &                   NTOUT , TOUT  , LRCOUT ,
     &                   DYIN  , LTRNG
     &                 )
C-----------------------------------------------------------------------
            ICAP=ICLEV(IRECMB)
            IP  =IABS(IC2LEV(IRECMB))
C-----------------------------------------------------------------------
               DO 3 IT=1,NTOUT
                  RCOUT(IT,ICAP,IP) = DEXP( LRCOUT(IT) )
    3          CONTINUE
C-----------------------------------------------------------------------
    1    CONTINUE
C***********************************************************************
      RETURN
      END
