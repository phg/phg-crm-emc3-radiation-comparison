C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8stkf.for,v 1.2 2018/10/03 09:20:15 mog Exp $ Date $Date: 2018/10/03 09:20:15 $
C
      SUBROUTINE B8STKF( NDTEM  , NDLEV  , NDMET  ,
     &                   IT     , NORD   , NMET   ,
     &                            IORDR  , IMETR  ,
     &                   CC     , STV    ,
     &                   DENS   ,
     &                   VEC    , V3     , IP     ,
     &                   VRED
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8STKF *********************
C
C  PURPOSE: TO STACK UP IN  'VRED'  THE RECOMBINATION RATE CONTRIBUTIONS
C           FOR  EACH  METASTABLE  LEVEL  FOR  A  GIVEN TEMPERATURE  AND
C           DENSITY AND INCLUDE THREE-BODY RECOMBINATION PART
C
C  CALLING PROGRAM:  ADAS208
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  IT      = INDEX DENOTING THE TEMPERATURE
C  INPUT :  (I*4)  IP      = PARENT INDEX
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (R*8)  DENS    = ELECTRON DENSITY (CM-3)
C  INPUT :  (R*8)  CC(,)   = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C  INPUT :  (R*4)  STV()   = RECOMBINATION  CONTRIBUTION  FOR  EACH
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: CM**3)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: ORDINARY EXCITED LEVEL INDEX
C  INPUT :  (R*8)  VEC(,,) = RECOMBINATION RATE COEFFT. VALUES.
C                            (UNITS: CM**3/SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: TEMPERATURE INDEX ('IT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX
C                            3ND DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  V3(,)   = THREE-BODY RECOMB. RATE COEFFT. VALUES.
C                            (UNITS: CM**6/SEC-1)
C                            VALUES FOR A SPECIFIC TEMPERATURE.
C                            1ST DIMENSION: CAPTURING LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C
C  OUTPUT:  (R*8)  VRED(,) = VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                            FOR EACH METASTABLE LEVEL.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1ST DIMENSION: METASTABLE LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C
C           (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IS      = ORDINARY EXCITED LEVEL INDEX
C
C
C ROUTINES: NONE
C
C NOTE:
C           VRED(IM,IP)   =    ( THE RECOMBINATION RATE FOR IM )
C                                              +
C                           SUM( (the transistion rate from ordinary
C                                 level IS to IM)  x  (the recombin-
C                                 ation  contribution  for  ordinary
C                                 level IS) )
C
C                           ABOVE SUM IS OVER ALL ORDINARY LEVELS.
C
C
C AUTHOR:  HP SUMMERS  (UPGRADE OF BXSTKD BY PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/06/92
C
C UPDATE:  12/07/93  HPS - CHANGE STV DIMENSION TO R*4
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM              , NDLEV              , NDMET      ,
     &           IT                 , NORD               , NMET
      INTEGER    IM                 , IS                 , IP
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)       , IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8     DENS
C-----------------------------------------------------------------------
      REAL*8     CC(NDLEV,NDLEV)    ,
     &           VEC(NDTEM,NDLEV,NDMET)   , VRED(NDMET,NDMET)  ,
     &           V3(NDLEV,NDMET)
C-----------------------------------------------------------------------
      REAL*4     STV(NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IM=1,NMET
              VRED(IM,IP) = VEC(IT,IMETR(IM),IP)+DENS*V3(IMETR(IM),IP)
              DO 2 IS=1,NORD
                 VRED(IM,IP) = VRED(IM,IP) + ( CC(IMETR(IM),IORDR(IS)) *
     &                                        STV(IS)        )
    2         CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
