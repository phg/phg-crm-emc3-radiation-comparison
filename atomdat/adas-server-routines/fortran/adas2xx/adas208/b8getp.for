       SUBROUTINE B8GETP( IZ0    , IZ1    , DSNEXP , DSNSPF ,
     &                    NDLEV  , NDMETI , NDTEMI , NDDENI ,
     &                    MAXD   , MAXT   , DENSA  , TEA    ,
     &                    LPDATA , LIOSEL , LRSEL  , LHSEL  ,
     &                    IL     , ITIN   , IDIN   ,
     &                    PCC    , PCIE   , PCIEPR , PV3PR  ,
     &                    PVCRPR , PVECR  , IUNT27 , OPEN27 ,
     &                    PR
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8GETP *********************
C
C  PURPOSE:  TO FETCH DATA FROM EXPANSION FILE AND CONDENSED BUNDLE-N
C            MATRIX FILE AND COMBINE WITH COLLISIONAL-RADIATIVE
C            DATA FOR IN THE LOW LEVEL POPULATION SOLUTION.
C
C  CALLING PROGRAM: ADAS208
C
C  DATA:
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (I*4)  IZ1     = ION CHARGE+1 (=CHARGE OF PARENT)
C  INPUT : (C*(*))DSNEXP  = FULL NAME OF EXPANSION FILE INCLUDING '/UID'
C  INPUT : (C*(*))DSNSPF  = FULL NAME OF SPEC. ION FILE READ IN MAIN
C                           PROGRAM INCLUDING '/UID'
C  INPUT : (I*4)  NDLEV   = MAX. NUMBER OF ENERGY LEVELS ALLOWED
C                           IN MAIN PROGRAM
C  INPUT : (I*4)  NDMETI  = MAX. NUMBER OF METASTABLE LEVELS ALLOWED
C                           IN MAIN PROGRAM
C  INPUT : (I*4)  NDTEMI  = MAX. NUMBER OF TEMPERATURES ALLOWED
C                           IN MAIN PROGRAM
C  INPUT : (I*4)  NDDENI  = MAX. NUMBER OF DENSITIES ALLOWED
C                           IN MAIN PROGRAM
C  INPUT : (I*4)  MAXD    = NUMBER OF DENSITIES IN MAIN PROGRAM
C  INPUT : (I*4)  MAXT    = NUMBER OF TEMPERATURES IN MAIN PROGRAM
C  INPUT : (R*8)  DENSA() = SET OF DENSITIES (CM-3) IN MAIN PROGRAM
C  INPUT : (R*8)  TEA()   = SET OF TEMPERATURES (K) IN MAIN PROGRAM
C  INPUT : (L*4)  LPDATA  = .TRUE. - EXPANSION DATA EXISTS AND IS SET
C                           .FALSE.- NO EXPANSION DATA OR NOT SET
C  INPUT : (L*4)  LIOSEL  = .TRUE. - INCLUDE DIRECT IONISATION ON OUTPUT
C                           .FALSE.- DO NOT INCLUDE
C  INPUT : (L*4)  LHSEL   = .TRUE. - INCLUDE ELECTRON RECOM  ON OUTPUT
C                           .FALSE.- DO NOT INCLUDE
C  INPUT : (L*4)  LRSEL   = .TRUE. - INCLUDE CHARGE EXCHANGE ON OUTPUT
C                           .FALSE.- DO NOT INCLUDE
C  INPUT : (I*4)  IL      = INPUT COPASE FILE - NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ITIN    = INDEX OF REQUIRED TEMPERATURE IN TEA() SET
C  INPUT : (I*4)  IDIN    = INDEX OF REQUIRED DENSITY IN DENSA() SET
C
C  INPUT : (I*4)  IUNT27  = UNIT FOR PAPER.TEXT OUTPUT
C  INPUT : (L*4)  OPEN27  = .TRUE. - PAPER.TEXT HAS BEEN OPENED
C                           .FALSE.- PAPER.TEXT HAS NOT BEEN OPENED
C
C  OUTPUT: (R*8)  PCC(,)  = PROJETED COLL. RAD. LOW LEVEL MATRIX
C                              1ST DIM: ENERGY LEVEL INDEX
C                              2ND DIM: ENERGY LEVEL INDEX
C  OUTPUT: (R*8)  PCIE()  = PROJECTED COLL. RAD. ION. COEFFT. VECTOR
C                              1ST DIM: ENERGY LEVEL INDEX
C  OUTPUT: (R*8)  PCIEPR(,)=PROJECTED PARENT RESOLVED COLL. RAD. ION
C                           MATRIX
C                              1ST DIM: ENERGY LEVEL INDEX
C                              2ND DIM: PARENT INDEX
C  OUTPUT: (R*8)  PV3PR(,)= DIRECT PARENT RESOLVED THREE
C                           BODY RECOMB. COEFFT MATRIX
C                              1ST DIM: ENERGY LEVEL INDEX
C                              2ND DIM: PARENT INDEX
C                              UNITS : CM3S-1
C  OUTPUT: (R*8)  PVCRPR(,,) =PROJECTED INDIRECT PARENT CQ COEFFICIENT
C                             MATRIX FROM SPECIFIC PARENT TO
C                             FINAL PARENT IN PN REPRESENTATION
C                             SUMMED OVER SPIN SYSTEMS
C                             1ST DIM.: FINAL   PARENT INDEX
C                             2ND DIM.: INITIAL PARENT INDEX
C  OUTPUT: (R*8)  PVECR(,)= PROJECTED PARENT RESOLVED COLL. RAD.
C                           RECOMB. COEFFT MATRIX ( RR + DR + 3B )
C                              1ST DIM: ENERGY LEVEL INDEX
C                              2ND DIM: PARENT INDEX
C                              UNITS : CM3S-1
C  OUTPUT: (R*8)  PR( ,,) = RECOM/BREMS. COEFFT (ERG S-1)
C                              1ST DIM: PARENT INDEX
C                              2ND DIM: TEMPERATURE INDEX
C                              3RD DIM: DENSITY INDEX
C
C
C          (C*80) DSNCPM  = FULL NAME OF COND.MAT. FILE INCLUDING '/UID'
C                           EXPANDED IF NECESSARY FROM SYMBOLIC FILENAME
C                           IN NAMELIST IN EXPANSION FILE
C          (C*80) DSNREF  = FULL NAME OF SPEC. ION FILE INCLUDING '/UID'
C                           EXPANDED IF NECESSARY FROM SYMBOLIC FILENAME
C                           IN NAMELIST IN EXPANSION FILE
C          (C*80) DSHORT  = TEMPORARY STRING
C          (C*11) PTSYMA()= PARENT SYMMETRY (2SP+1 LP) AS CHARACTERS
C                             1ST DIMENSION: PARENT INDEX
C          (I*4)  NPTSPA()= PARENT SPIN (2SP+1)
C                             1ST DIMENSION: PARENT INDEX
C          (I*4)  NSPSYS()= NO. OF SPIN SYSTEMS ASSOCIATED WITH PARENT
C                             1ST DIMENSION: PARENT INDEX
C          (I*4)  NCUTP() = N-SHELL CUT-OFF ASSOCIATED WITH AUGER
C                           PROCESSES FOR THE PARENT
C                             1ST DIMENSION: PARENT INDEX
C          (R*8)  DEPA()  = BINDING ENERGY (RYD) OF LOWEST AUGER
C                           N-SHELL FOR THE PARENT
C                             1ST DIMENSION: PARENT INDEX
C          (I*4)  NSHEL   = NUMBER OF N-SHELLS INVOLVED IN EXPANSION
C          (I*4)  NSHELA()= N-SHELLS INVOLVED IN THE EXPANSION
C                             1ST DIMENSION: SHELL INDEX (<= NSHEL)
C          (I*4)  NSPIN   = NUMBER OF SPIN SYSTEMS FOR CURRENT PARENT
C          (I*4)  NSPNA(,)= SPIN OF SYSTEM (2S+1)
C                             1ST DIMENSION: SPIN SYSTEM INDEX
C                             2ND DIMENSION: PARENT INDEX
C          (I*4)  NLWSTA(,)= LOWEST N-SHELL INCLUDED FOR THE SPIN SYSTEM
C                             1ST DIMENSION: SPIN SYSTEM INDEX
C                             2ND DIMENSION: PARENT INDEX
C          (R*8)  PLWSTA(,)= PHASE SPACE OCCUPANCY FACTOR FOR LOWEST
C                            N-SHELL FOR SPIN SYSTEM
C                             1ST DIMENSION: SPIN SYSTEM INDEX
C                             2ND DIMENSION: PARENT INDEX
C          (R*8)  FLWSTA(,)= FRACTIONAL PARENTAGE (EQUIV. ELECTRONS) FOR
C                            FOR IONISATION FROM LOWEST LEVEL OF
C                            SPIN SYSTEM
C                             1ST DIMENSION: SPIN SYSTEM INDEX
C                             2ND DIMENSION: PARENT INDEX
C          (R*8)  FRACPRT  = TEMP. STORE OF FRACTIONAL PARENTAGE
C          (I*4)  INDA()  = LEVEL INDEX WITH RESPECT TO SPEC. ION FILE
C                             1ST DIMENSION: COUNTER OVER EXP. RECORDS
C          (C*11) LVSYMA()= LEVEL SYMMETRY AND ADDITIONAL INFO.ON CONFIG
C                             1ST DIMENSION: COUNTER OVER EXP. RECORDS
C          (I*4)  LSZDA() = SZD FILE SELECTOR FOR RECORD (IF REQUIRED)
C                             1ST DIMENSION: COUNTER OVER EXP. RECORDS
C          (I*4)  LSPA()  = SPIN SYSTEM (2S+1) FOR RECORD
C                             1ST DIMENSION: COUNTER OVER EXP. RECORDS
C          (I*4)  LSHA()  = ACTIVE N SHELL FOR RECORD
C                             1ST DIMENSION: COUNTER OVER EXP. RECORDS
C          (I*4)  LPTA()  = PARENT INDEX  FOR RECORD
C                             1ST DIMENSION: COUNTER OVER EXP. RECORDS
C          (R*8)  WGHTA(,)= WEIGHTING FOR EXPANSION FOR RECORD
C                             1ST DIMENSION: COUNTER OVER EXP RECORDS
C                             2ND DIMENSION: NSHELL INDEX
C          (I*4)  NMET    = NUMBER OF '*' LEVELS COUNTED
C                           (NB. USE ONLY ONCE FOR A GIVEN LEVEL
C                                EVEN THOUGH ANOTHER RECORD FOR THE
C                                LEVEL MAY EXIST)
C          (I*4)  IMETR() = LEVEL INDEX OF METASTABLES '*'ED
C                             1ST DIMENSION: METASTABLE COUNTER(<=NMET)
C          (C*250)LSTRNG  = COND. BUNDLE-N. MATRIX (CBNM) FILE RECORD
C          (C*2)  SEQM    = SEQUENCE IDENTIFIER GIVEN ON CBNM FILE
C          (I*4)  NUCGM   = NUCLEAR CHARGE GIVEN ON CBNM FILE
C          (I*4)  NPRTM   = NO. OF PARENTS GIVEN ON CBNM FILE
C          (I*4)  MAXDM   = NO. OF DENSITIES GIVEN ON CBNM FILE
C          (I*4)  MAXTM   = NO. OF TEMPERATURES GIVEN ON CBNM FILE
C          (I*4)  IPRT    = PARENT INDEX
C          (I*4)  IPRTM   = PARENT INDEX IN CBNM FILE
C          (C*4)  TRMPM   = PARENT TERM SPECIFICATION AS (2SP+1LP)
C          (I*4)  SPNPM   = PARENT SPIN (2SP+1)
C          (I*4)  ISYSM   = SPIN SYSTEM INDEX IN CBNM FILE
C          (I*4)  SSYSM(,)= SPIN SYSTEM IN CBNM FILE
C                             1ST DIM.: PARENT INDEX       (<=NDMET)
C                             2ND DIM.: SPIN SYSTEM INDEX  (<=2)
C          (I*4)  NSYSM() = NO OF SPIN SYSTEM IN CBNM FILE FOR PARENT
C                             1ST DIM.: PARENT INDEX       (<=NDMET)
C          (I*4)  NSHLM(,)= NO. OF N-SHELLS IN CBNM FILE
C                             1ST. DIM.: PARENT INDEX      (<=NDMET)
C                             2ND. DIM.: SPIN SYSTEM INDEX (<=2)
C          (R*8)  DENSM() = ELECTRON DENSITIES (CM-3) ON CBNM FILE
C                             1ST DIMENSION: DENSITY INDEX (<=NDMAX)
C          (R*8)  TEM()   = ELECTRON TEMPS.  (K) ON CBNM FILE
C                             1ST DIMENSION: TEMP. INDEX (<=NTMAX)
C          (R*8)  PCRMAT(,,,,,)=PROJECTED COLLISIONAL-RADIATIVE MATRIX
C                               IN P-REPRESENTATION WITHOUT ELIMINATIONS
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: DENSITY INDEX
C                               3RD DIM.: ROW INDEX
C                               4TH DIM.: COLUMN INDEX
C                               5TH DIM.: PARENT INDEX
C                               6TH DIM.: SPIN SYSTEM INDEX
C          (R*8)  PIOMAT(,,,,,)=PROJECTED COLLISIONAL-RADIATIVE IONIS.
C                               MATRIX TO RESOLVED + METASTABLES
C                               IN P-REPRESENTATION WITHOUT ELIMINATIONS
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: DENSITY INDEX
C                               3RD DIM.: ROW INDEX
C                               4TH DIM.: COLUMN INDEX (+ METASTABLES)
C                               5TH DIM.: PARENT INDEX
C                               6TH DIM.: SPIN SYSTEM INDEX
C          (R*8)  PQPIND(,,,,,)=PROJECTED INDIRECT PARENT CQ COEFFICIENT
C                               MATRIX FROM SPECIFIC PARENT, SPIN TO
C                               FINAL PARENT IN PN REPRESENTATION
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: DENSITY INDEX
C                               3TH DIM.: FINAL   PARENT INDEX
C                               4TH DIM.: INITIAL PARENT INDEX
C                               5TH DIM.: SPIN SYSTEM INDEX
C          (R*8)  PCRRHS(,,,,)= PROJECTED COLLISIONAL-RADIATIVE RECOM.
C                               RHS. FROM A SPECIFIED PARENT AND IN
C                               A SPECIFIED SPIN SYSTEM
C                               IN P-REPRESENTATION WITHOUT ELIMINATIONS
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: DENSITY INDEX
C                               3RD DIM.: ROW INDEX
C                               5TH DIM.: PARENT INDEX
C                               6TH DIM.: SPIN SYSTEM INDEX
C          (R*8)  PRB(,,,)    = RECOM/BREMS. COEFFT (
C                               1ST DIM: TEMPERATURE INDEX
C                               2ND DIM: DENSITY INDEX
C                               3RD DIM: PARENT INDEX
C                               4TH DIM: SPIN SYSTEM INDEX
C          (R*8)  DCRMAT(,,,) = DIRECT COLLISIONAL-RADIATIVE MATRIX
C                               IN P-REPRESENTATION FOR LOW N-SHELLS
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: DENSITY INDEX
C                               3RD DIM.: ROW INDEX
C                               4TH DIM.: COLUMN INDEX
C          (R*8)  DIOMAT(,,,) = DIRECT COLLISIONAL-RADIATIVE IONIS.
C                               MATRIX TO RESOLVED + METASTABLES
C                               IN P-REPRESENTATION FOR LOW N-SHELLS
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: DENSITY INDEX
C                               3RD DIM.: ROW INDEX
C                               4TH DIM.: COLUMN INDEX (+ METASTABLES)
C          (R*8)  DTREC(,)   =  DIRECT THREE-BODY RECOMBINATION COEFFTS.
C                               FROM A SPECIFIED PARENT AND IN A
C                               SPECIFIED SPIN SYSTEM
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: ROW INDEX
C          (R*8)  DDREC(,)   =  DIRECT DIELECTR.  RECOMBINATION COEFFTS.
C                               FROM A SPECIFIED PARENT AND IN A
C                               SPECIFIED SPIN SYSTEM
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: ROW INDEX
C          (R*8)  DRREC(,)   =  DIRECT RADIATIVE  RECOMBINATION COEFFTS.
C                               FROM A SPECIFIED PARENT AND IN A
C                               SPECIFIED SPIN SYSTEM
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: ROW INDEX
C          (R*8)  DXREC(,)   =  DIRECT CH. EXCH.  RECOMBINATION COEFFTS.
C                               FROM A SPECIFIED PARENT AND IN A
C                               SPECIFIED SPIN SYSTEM DUE TO H(1S)
C                               1ST DIM.: TEMPERATURE INDEX
C                               2ND DIM.: ROW INDEX
C          (I*4)  NM()       =  LOW N-SHELLS FOR PARENT SPIN SYSTEM
C                               COMBINATION
C                               1ST. DIM.: N-SHELL INDEX
C          (I*4)  NSUP(,)    =  HIGHEST N-SHELL REQUIRED FOR EXPANSION
C                               FOR THE PARENT AND SPIN SYSTEM
C                               1ST. DIM.: PARENT INDEX
C                               2ND. DIM.: SPIN SYSTEM INDEX
C          (I*4)  ISPIN      =  GENERAL INDEX
C          (I*4)  IPT        =  GENERAL INDEX
C          (I*4)  JPT        =  GENERAL INDEX
C          (I*4)  I          =  GENERAL INDEX
C          (I*4)  J          =  GENERAL INDEX
C          (I*4)  II         =  GENERAL INDEX
C          (I*4)  JJ         =  GENERAL INDEX
C          (I*4)  IR         =  GENERAL INDEX
C          (I*4)  IC         =  GENERAL INDEX
C          (I*4)  IS         =  GENERAL INDEX
C          (I*4)  KI         =  GENERAL INDEX
C          (I*4)  KJ         =  GENERAL INDEX
C          (I*4)  IN         =  DENSITY INDEX
C          (I*4)  IT         =  TEMPERATURE INDEX
C          (I*4)  NUP        =  UPPER N-SHELL FOR CURRENT EXPANSION
C          (I*4)  IMAX       =  NO. OF SHELLS REQUIRED IN EXPANSION
C          (L*4)  LSOLVE     = .TRUE. -INVERSION WITH SOLN. OF EQUATIONS
C                            = .FALSE. -INVERSION ONLY
C          (R*8)  AMAT(,)    = TEMPORARY ARRAY FOR INVERTING
C          (R*8)  BRHS()     = TEMPORARY R.H.S FOR EQUATION SOLUTION
C          (R*8)  DINTX      = + OR - DEPENDING ON INTERCHANGES IN
C                              INVERSION SUBROUTINE XXMINV
C          (R*8)  PCRTMP(,)  = TEMPORARY PROJECTED COLL. RAD. MATRIX
C                              TO BE CONDENSED TO PCRMAT
C          (R*8)  DCRTMP(,)  = TEMPORARY DIRECT COLL. RAD. MATRIX
C                              TO BE CONDENSED TO DCRMAT
C          (R*8)  PIOTMP(,)  = TEMPORARY PROJECTED IONIS. MATRIX
C                              TO BE CONDENSED TO PIOMAT
C          (R*8)  PRHTMP(,)  = TEMPORARY PROJECTED R.H.S. VECTOR
C                              TO BE CONDENSED TO PCRRHS
C          (R*8)  PQPTMP()   = TEMPORARY INDIRECT PARENT QC COEFFICIENT
C                              TO BE CONDENSED TO PQPIND
C                                 1ST INDEX  - FINAL PARENT
C          (R*8)  SUM        = GENERAL USE FOR SUMMING
C          (R*8)  Z0         = NUCLEAR CHARGE
C          (R*8)  Z1         = ION CHARGE+1 (=CHARGE OF PARENT)
C          (R*8)  SSYSWT     = FRACTIONAL WEIGHTING OF SPIN SYSTEM
C                              FOR PARTICULAR PARENT TO BE USED IF
C                              RECOMBINATION COEFFICIENTS ARE GIVEN IN
C                              THE MULTIPLIED UP FORM.
C          (L*4)  LTRNG()    = .TRUE. - OUTPUT VALUE WAS EXTRAPOLATED
C                                       FOR TEMPERATURE
C                              .FALSE. - OUTPUT VALUE NOT EXTRAPOLATED
C          (L*4)  LDRNG()    = .TRUE. - OUTPUT VALUE WAS EXTRAPOLATED
C                                       FOR DENSITY
C                              .FALSE. - OUTPUT VALUE NOT EXTRAPOLATED
C          (I*4)  IUP        = NUP-NM(1)+1
C          (R*8)  V          = TEMPORARY REAL NUMBER
C          (R*8)  ARRIN(,)   = TEMPORARY ARRAY FOR INPUT TO SPLINING
C          (R*8)  ARROUT(,)  = TEMPORARY ARRAY FOR IOUTPUT FROM SPLINING
C          (R*8)  TEMIN      = MINIMUM TEMPERATURE BELOW WHICH COEFFT.
C                              SHOULD BE SET TO ZERO
C          (R*8)  DEMIN      = MINIMUM DENSITY  BELOW WHICH COEFFT.
C                              SHOULD BE SET TO ZERO
C          (R*8)  DETMP      = TEMPORARY VALUE OF DEMIN
C          (R*8)  TETMP      = TEMPORARY VALUE OF TEMIN
C                              SHOULD BE SET TO ZERO
C          (I*4)  IUPA(,)    = DIMENSION OF FINAL CONDENSED N-SHELL
C                              MATRIX
C                              1ST DIM: PARENT INDEX
C                              2ND DIM: SPIN SYSTEM INDEX
C
C          (I*4)  IEDMAT   = 0 PCRL ADDED ONTO PCRMAT
C                            1 PCRL NOT ADDED ON
C          (I*4)  IECION   = 0 PCION  ADDED ONTO TO PCRMAT
C                              PCIONRI ADDED ONTO PCIONRP
C                            1 PCION NOT ADDED ON
C                              PCIONRI NOT ADDED ON
C          (I*4)  IETREC   = 0 PTREC ADDED ONTO PCRRHS
C                            1 PTREC NOT ADDED ON
C          (I*4)  IEDREC   = 0 PDREC ADDED ONTO PCRRHS
C                            1 PDREC NOT ADDED ON
C          (I*4)  IERREC   = 0 PRREC ADDED ONTO PCRRHS
C                            1 PRREC NOT ADDED ON
C          (I*4)  IEXREC   = 0 PXREC ADDED ONTO PCRRHS
C                            1 PXREC NOT ADDED ON
C          (I*4)  IERSYS   = 0 RECOMBINATION RATES MULTIPLIED
C                                  BY SPIN SYSTEM WEIGHT
C                            1 RECOMBINATION RATES NOT MULTIPLIED
C                                  BY SSYSWT
C
C ROUTINES: NONE
C
C STREAM HANDLING :
C           7     OUTPUT (PAPER.TEXT)
C          14     EXPANSION FILE
C          15     CONDENSED MATRIX MASTER FILE
C
C AUTHOR:  HP SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    18/08/92
C
C-----------------------------------------------------------------------
C
C UPDATE:  WJ DICKSON
C          K1/1/26
C
C   DATE:  JANUARY 1993
C
C          NUMEROUS ADJUSTMENTS AND UPDATES
C
C-----------------------------------------------------------------------
C
C UPDATE:  WJ DICKSON
C          K1/1/26
C
C   DATE:  12TH AUGUST 1993
C
C          INCLUSION OF VARIABLES IEFPRS AND IEFPRE AND CORRESPONDING
C          ADJUSTMENTS TO DIO , PCR AND PIO MATRICES. FRACTIONAL
C          PARENTAGE COEFFICIENTS AS GIVEN BY EXPANSION FILE
C          ( VARIABLE  FLWSTA )
C
C          (I*4)  IEFPRS   = 0 GROUND STATE IONISATION RATE COEFFICIENTS
C                              HAVE BEEN MULTIPLYIED BY FRACTIONAL
C                              PARENTAGE COEFFICIENT IN MAINBNS
C                            1 GROUND STATE IONISATION RATE COEFFICIENTS
C                              HAVE NOT BEEN MULTIPLYIED BY FRACTIONAL
C                              PARENTAGE COEFFICIENT IN MAINBNS
C
C          (I*4)  IEFPRE   = 0 ELEMENTS OF MAIN C-R MATRIX ARISING
C                              FROM GROUND STATE
C                              HAVE BEEN MULTIPLYIED BY FRACTIONAL
C                              PARENTAGE COEFFICIENT IN MAINBNS
C                            1 ELEMENTS OF MAIN C-R MATRIX ARISING
C                              FROM GROUND STATE
C                              HAVE NOT BEEN MULTIPLYIED BY FRACTIONAL
C                              PARENTAGE COEFFICIENT IN MAINBNS
C
C UNIX-IDL PORT:
C
C DATE: UNKNOWN
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C UPDATE:  29/03/96  HPS - INCREASE PARAMETER SETTINGS NDTEM: 20->35
C                                                      NDDEN: 20->24
C
C UPDATE:  18/04/96  HPS - ALTER FORMAT 2008 FOR READING TEMPS. AND
C                          DENS. FROM CBNM FILE FOR CONSISTENCY WITH
C                          NEW PRODUCTION VERSION OF ADAS204
C UPDATE:  18/04/96  HPS - ALTER B8SPLNX TO B8SPLN IN 2ND AND 3RD
C                          CALLS IN THE SUBROUTINE
C UPDATE:  03/05/96  DHB - ALTERED IBM SPECIFIC STATEMENTS. INCREASED
C                          SIZE OF DSNINC & DSNSPF TO 80.
C UPDATE:  09/03/98  HPS - ADDED PR TO PARAMETER LIST. PREPARED FROM PRB
C                          FROM PROJECTION MATRIX FILE BY INTERPOLATION.
C                          CORRECTED PB TO INCLUDE SUM OVER SPIN SYSTEMS
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C DATE:    10-05-96
C
C VERSION: 1.1                          DATE: 10-05-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C           - FIRST PUT UNDER SCCS
C
C VERSION: 1.2
C MODIFIED: WILLIAM OSBORN              DATE: 13-05-96
C           - ADDED IUNT27 AND OPEN27 TO ALLOW PAPER.TEXT OUTPUT
C
C VERSION: 1.3
C MODIFIED: WILLIAM OSBORN + HPS        DATE: 28-05-96
C           - ADDED CALL TO XXFLNM TO EXPAND FILENAMES
C
C VERSION: 1.4
C MODIFIED: TIM HAMMOND                 DATE: 02-08-96
C           - CHANGED NAME OF VARIABLE DINT TO DINTX AS DINT IS THE
C             NAME OF AN INTRINSIC FUNCTION ON HP WORKSTATIONS
C
C VERSION: 1.5
C MODIFIED: RICHARD MARTIN                 DATE: 02-03-98
C           - CHANGED IUNT7 TO IUNT27 AND OPEN7 TO OPEN27.
C
C VERSION: 1.6
C MODIFIED: HUGH SUMMERS                   DATE: 09-03-98
C               - ADDED PR TO PARAMETER LIST. PREPARED FROM PRB
C             FROM PROJECTION MATRIX FILE BY INTERPOLATION.
C             CORRECTED PB TO INCLUDE SUM OVER SPIN SYSTEMS
C
C VERSION:   1.7                                   DATE: 2/09/99
C MODIFIED:  Martin O'Mullane
C            - Format error in 2020. Skip 70X not 80X.
C
C VERSION:   1.8                                   DATE: 26/10/99
C MODIFIED:  Martin O'Mullane
C            - Added call to b8splt to correct for odd behaviour
C              at low ne high Te recombination data.
C
C VERSION:   1.9                                   DATE: 08/12/99
C MODIFIED:  Martin O'Mullane
C            - Added check for expansion data which did not have any
C              weighting factors. Only proceed if LPTA is non zero.
C              It is the do 56 loop.
C            - The assumption that variables are saved between calls
C              should not be made (Linux again). Anyway it's bad
C              practice. Data required when called with LPDATA true
C              is now saved.
C
C VERSION:   1.10                                   DATE: 13/02/2006
C MODIFIED:  Martin O'Mullane
C            - Increase number of levels to 150.
C            - Write status of projection to screen.
C            - Index error in FLWSTA corrected.
C
C VERSION:   1.11                                   DATE: 05/11/2007
C MODIFIED:  Adam Foster
C            - Increase number of levels to 3500.
C
C VERSION : 1.12
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Increase size of filename variables from 80 to 132 characters.
C           - Add name of adf17 file to error message (COND. MAT. FILE
C             DOES NOT EXIST).
C
C VERSION : 1.13
C DATE    : 28-08-2017
C MODIFIED: Martin O'Mullane
C           - Add trap for negative n-upper. Can occur because of an
C             extra iprt/spin block in adf17 or if parents are omitted
C             in the adf18/a17_p208 mapping file.
C
C VERSION : 1.14
C DATE    : 19-09-2017
C MODIFIED: Martin O'Mullane
C           - Increase number of metastables to 13 (up from 4).
C           - Replace all calls to b8spln with b8splt to eliminate
C             ringing because of near-zero values.
C           - Set separate iopt for temperature and density in
C             call to b8splt.
C
C VERSION : 1.15
C DATE    : 20-12-2017
C MODIFIED: Matthew Bluteau
C           - Increase string length to 250 to accommodate more parents
C             in adf18 expansion file.
C
C VERSION : 1.16
C DATE    : 24-04-2018
C MODIFIED: Martin O'Mullane
C           - Re-factor the code to enable an IC-resolved adf18
C             cross-referencing file to use an LS-resolved adf17
C             condensed projection matrix file.
C           - Reduce NDREC to 2500 (to match NDLEV of adas208).
C           - Short labelled do loops replaced with do/end do.
C           - Identify the two major sections: the initialization 
C             and interpolation over te/dens section when first called
C             and the data return block for subsequent calls.
C           - Allow different te and dens extrapolation options in
C             calls to b8splt.
C           - Calculate the statistical split for IC adf18 datasets
C             and a map from IC to LS parents.
C           - Apply the map and statistical split when assembling 
C             the return data.
C           
C VERSION : 1.17
C DATE    : 24-04-2019
C MODIFIED: Martin O'Mullane
C            - Input file names, dsnspf and dsnexp, have their lengths 
C              defined by the argument length.
C
C-----------------------------------------------------------------------
      INTEGER  IUNT14     , IUNT15    , I4UNIT
      INTEGER  NDMET      , NDREC     , NDSHL
      INTEGER  NDDEN      , NDTEM
      INTEGER  NDMETI     , NDLEV
      INTEGER  NDDENI     , NDTEMI
C-----------------------------------------------------------------------
      PARAMETER( IUNT14 = 14 , IUNT15 = 15 )
      PARAMETER( NDTEM  = 35 , NDDEN = 24  , NDMET = 13)
      PARAMETER( NDREC  =2500, NDSHL =  5  )
C-----------------------------------------------------------------------
      INTEGER   NPARNT      , NSHEL       , NLEV       , IPARNT
      INTEGER   JPARNT
      INTEGER   K           , KREC        , NMET       , NSPIN
      INTEGER   NUCGM       , NPRTM       , MAXDM      , MAXTM
      INTEGER   I           , J           , IN         , IT
      INTEGER   IPRT        , IPRTM       , ISYSM
      INTEGER   II          , JJ          , IR         , IS
      INTEGER   KI          , KJ          , IC
      INTEGER   IMAX        , NUP         , IUP
      INTEGER   MAXD        , MAXT
      INTEGER   IZ0         , IZ1
      INTEGER   IL          , ITIN        , IDIN
      INTEGER   ISPIN       , IPT         , JPT
      INTEGER   IEDMAT      , IECION      , IETREC     , IEDREC
      INTEGER   IERREC      , IEXREC      , IERSYS
      INTEGER   ITPRN       , INPRN
      INTEGER   IEFPRS      , IEFPRE
      INTEGER   IUNT27
      integer   lenstr      , ntrm        , ipm        , jpm
C-----------------------------------------------------------------------
      CHARACTER DSNEXP*(*)  , DSNSPF*(*)  , DSNREF*132
      CHARACTER DSNCPM*132  , DSHORT*132
      CHARACTER SEQ*2       , STRING*250  , STRNG1*250  , SUBSTRG*11
      CHARACTER SEQM*2      , LSTRNG*250  , TRMPM*4
C-----------------------------------------------------------------------
      REAL*8    SPNPM       , SUM         , DINTX      , V       , TEMIN
      REAL*8    DEMIN       , DETMP       , TETMP
      REAL*8    Z0          , Z1          , SSYSWT     , PRBSP
      REAL*8    FRACPRT
C-----------------------------------------------------------------------
      LOGICAL   LEXIST      , LSOLVE      , LPDATA
      LOGICAL   LIOSEL      , LHSEL       , LRSEL
      LOGICAL   OPEN27
      logical   licres
C-----------------------------------------------------------------------
      INTEGER   NPTSPA(NDMET)  , NSPSYS(NDMET)  , NCUTP(NDMET)
      INTEGER   NSHELA(NDSHL)  , NSPNA(2,NDMET) , NLWSTA(2,NDMET)
      INTEGER   IMAXSTA(2,NDMET)
      INTEGER   INDA(NDREC)    , LSZDA(NDREC)
      INTEGER   LSPA(NDREC)    , LSHA(NDREC)    , LPTA(NDREC)
      INTEGER   IMETR(NDMET*2) , NSHLM(NDMET,2) , NM(NDSHL)
      INTEGER   NSYSM(NDMET)   , NSUP(NDMET,2)  , IUPA(NDMET,2)
      integer   map(ndmet)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDDENI)        , TEA(NDTEMI)
      REAL*8    PCC(NDLEV,NDLEV)     , PCIE(NDLEV)
      REAL*8    PCIEPR(NDLEV,NDMETI) , PV3PR(NDLEV,NDMETI)
      REAL*8    PVECR(NDLEV,NDMETI)
      REAL*8    PVCRPR(NDMETI,NDMETI)
      REAL*8    PR(NDMETI,NDTEMI,NDDENI)    , PRB(NDTEM,NDDEN,NDMET,2)
C
      REAL*8    DEPA(NDMET)
      REAL*8    PLWSTA(2,NDMET), FLWSTA(2,NDMET)
      REAL*8    WGHTA(NDREC,NDSHL)
      REAL*8    DENSM(NDDEN)   , TEM(NDTEM)  , SSYSM(NDMET,2)
      REAL*8    PCRMAT(NDTEM,NDDEN,NDSHL,NDSHL,NDMET,2)
      REAL*8    DCRMAT(NDTEM,NDDEN,NDSHL,NDSHL)
      REAL*8    PCRRHS(NDTEM,NDDEN,NDSHL,NDMET,2)
      REAL*8    PTRRHS(NDTEM,NDDEN,NDSHL,NDMET,2)
      REAL*8    PIOMAT(NDTEM,NDDEN,NDSHL,NDMET,NDMET,2)
      REAL*8    PQPIND(NDTEM,NDDEN,NDMET,NDMET,2)
      REAL*8    DIOMAT(NDTEM,NDDEN,NDSHL,NDMET)
      REAL*8    DTREC(NDTEM,NDSHL)
      REAL*8    DDREC(NDTEM,NDSHL)
      REAL*8    DRREC(NDTEM,NDSHL)
      REAL*8    DXREC(NDTEM,NDSHL)
      REAL*8    PCRTMP(NDSHL,NDSHL)  , PIOTMP(NDSHL,NDMET)
      REAL*8    DCRTMP(NDSHL,NDSHL)
      REAL*8    PRHTMP(NDSHL) , PQPTMP(NDMET)
      REAL*8    AMAT(NDSHL,NDSHL)      , BRHS(NDSHL)
      REAL*8    ARRIN(NDTEM,NDDEN)     , ARROUT(NDTEM,NDDEN)
      real*8    pr_init(ndmet,ndtem,ndden)
      real*8    frac_ic(ndmet)         , sum_ic(ndmet)
      real*8    flwsta_ls(2,ndmet)
C-----------------------------------------------------------------------
      CHARACTER PTSYMA(NDMET)*11   , LVSYMA(NDREC)*26
      CHARACTER TRMPRT(NDMET)*4
      character prt_ls(ndmet)*4
C-----------------------------------------------------------------------
      LOGICAL   LTRNG(NDTEM)       , LDRNG(NDDEN)
C-----------------------------------------------------------------------
      save      krec   , inda    , lspa   , lsha   , lpta  ,
     &          wghta  , nparnt  , nspin  , nsup   , iupa  , nprtm ,
     &          nspna  , nspsys  , nptspa ,
     &          pqpind , pcrrhs  , ptrrhs , piomat , pcrmat,
     &          prt_ls , map     , frac_ic, pr_init, flwsta_ls
c-----------------------------------------------------------------------
      NAMELIST  /SEQINF/SEQ,DSNREF,DSNCPM,NPARNT,NSHEL,NLEV
C-----------------------------------------------------------------------
      external  lenstr
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C INITIAL SET UP OF SOME VARIABLES
C-----------------------------------------------------------------------

       Z0=IZ0
       Z1=IZ1

       ITPRN =    -5
       INPRN =    -5

       string  = ' '
       STRNG1  = ' '
       SUBSTRG = ' '
       SEQM    = ' '
       LSTRNG  = ' '
       TRMPM   = ' '

C-----------------------------------------------------------------------
C OUTPUT HEADER
C-----------------------------------------------------------------------

       IF (.NOT.LPDATA) THEN
          IF (OPEN27) WRITE(IUNT27,3000)
       ENDIF

C-----------------------------------------------------------------------
C CHECK SIZE OF ARRAYS AND VALUES OF ITIN , IDIN
C-----------------------------------------------------------------------

       IF( IDIN .GT. MAXD .OR. ITIN .GT. MAXT ) THEN
         LPDATA = .FALSE.
         IF (OPEN27) THEN
         WRITE(IUNT27,*) 'B8GETP FAIL: IDIN/ITIN GREATER THAN MAXD/MAXT'
         WRITE(IUNT27,3100)
         ENDIF
         RETURN
       ENDIF
       IF( NDTEM .LT. MAXT) THEN
         LPDATA = .FALSE.
         IF (OPEN27) THEN
            WRITE(IUNT27,*) 'B8GETP FAIL: NDTEM LESS THAN MAXT'
            WRITE(IUNT27,3100)
         ENDIF
         RETURN
       ENDIF
       IF( NDDEN .LT. MAXD) THEN
         LPDATA = .FALSE.
         IF (OPEN27) THEN
            WRITE(IUNT27,*) 'B8GETP FAIL: NDDEN LESS THAN MAXD'
            WRITE(IUNT27,3100)
         ENDIF
         RETURN
       ENDIF
       IF( NDREC .LT. NDLEV) THEN
         LPDATA = .FALSE.
         IF (OPEN27) THEN
            WRITE(IUNT27,*) 'B8GETP FAIL: NDREC LESS THAN NDLEV'
            WRITE(IUNT27,3100)
         ENDIF
         RETURN
       ENDIF
       IF( NDMET .LT. NDMETI) THEN
         LPDATA = .FALSE.
         IF (OPEN27) THEN
            WRITE(IUNT27,*) 'B8GETP FAIL: NDMET LESS THAN NDMETI'
            WRITE(IUNT27,3100)
         ENDIF
         RETURN
       ENDIF

C-----------------------------------------------------------------------
C IF EXPANSION DATA NOT GATHERED, THEN FETCH IT. OTHERWISE EVALUATE
C EXPANDED PROJECTION MATRICES FOR GIVEN TEMPERATURE AND DENSITY
C
C Evaluate the output arrays and vectors over the NPARNT of adf18.
C If these are IC-resolved parents, then apply the map(parent) and 
C statistical split (frac_ic(parent)) to the LS-resolved adf17 values.
C
C The map(parent) should be 1:1 for LS-resolved adf18 and adf17 data.
C-----------------------------------------------------------------------

       IF (LPDATA) THEN

           DO I = 1,NDLEV

             PCIE(I)=0.0D0
             DO IPARNT = 1,NDMET
               PV3PR(I,IPARNT)  = 0.0D0
               PVECR(I,IPARNT)  = 0.0D0
               PCIEPR(I,IPARNT) = 0.0D0
             END DO
             DO J = 1,NDLEV
               PCC(I,J)=0.0D0
             END DO

           END DO

           DO J = 1,NDMET
             DO I = 1,NDMET
                PVCRPR(J,I) = 0.0D+0
             END DO
           END DO

           DO 300 IPARNT = 1, nparnt

             ipm = map(iparnt)

             NSPIN=NSPSYS(IPARNT)

             DO 290 ISPIN=1,NSPIN

               DO JPARNT = 1, nparnt
                 jpm = map(jparnt)
                 PVCRPR(JPARNT,IPARNT) =
     &               PVCRPR(JPARNT,IPARNT) +
     *                         frac_ic(iparnt) * frac_ic(jparnt) *
     &                         PQPIND(ITIN,IDIN,jpm,ipm,ISPIN)
               ENDDO

               DO 280 KI = 1, KREC

                 if (ki.gt.il) goto 280

                 I = INDA(KI)
                 IF (LSPA(KI).EQ.NSPNA(ISPIN,IPARNT)
     &                       .AND.LPTA(KI).EQ.IPARNT)THEN

                   IPT = LSHA(KI)
                   IR  = LSHA(KI)-NSUP(IPARNT,ISPIN)+IUPA(ipm,ISPIN)

                   if (ir.gt.0) then

                      PVECR(I,IPARNT)=PVECR(I,IPARNT) +
     &                                   WGHTA(KI,IPT)*frac_ic(iparnt)*
     &                             PCRRHS(ITIN,IDIN,IR,ipm,ISPIN)
                      PV3PR(I,IPARNT)=PV3PR(I,IPARNT) +
     &                                   WGHTA(KI,IPT)*frac_ic(iparnt)*
     &                        PTRRHS(ITIN,IDIN,IR,ipm,ISPIN)

C              project direct ionisation paths only

                      DO IS=IPARNT,IPARNT
                        PCIEPR(I,IS)=PCIEPR(I,IS) +
     &                         frac_ic(iparnt)*
     &                         PIOMAT(ITIN,IDIN,IR,IS,ipm,ISPIN)
                      END DO

                   endif


                   DO KJ = 1, KREC

                     if (kj.gt.il) goto 270
                     J = INDA(KJ)
                     IF(LSPA(KJ).EQ.NSPNA(ISPIN,IPARNT)
     &                          .AND.LPTA(KJ).EQ.IPARNT)THEN
                         JPT = LSHA(KJ)
                         IS  = LSHA(KJ)-NSUP(IPARNT,ISPIN)+
     &                         IUPA(ipm,ISPIN)
                         if (is.gt.0.and.ir.gt.0) then
                            IF (IPT.EQ.JPT)THEN
                                IF(I.EQ.J)THEN
                                    PCC(I,J)=PCC(I,J)+frac_ic(iparnt)*
     &                                       PCRMAT(ITIN,IDIN,IR,
     &                                              IS,ipm,ISPIN)
                                ENDIF
                            ELSE
                                PCC(I,J)=PCC(I,J) +
     &                                   WGHTA(KI,IPT) *
     &                                   frac_ic(iparnt)*
     &                                   PCRMAT(ITIN,IDIN,
     &                                          IR,IS,ipm,ISPIN)
                            ENDIF
                         endif
                     ENDIF

  270                CONTINUE
                   END DO

                 ENDIF

  280          CONTINUE    ! KI=1,KREC
  290        CONTINUE      ! ISPIN=1,NSPIN
  300      CONTINUE        ! IPARNT=1,nprtm

           DO I = 1, IL
             PCIE(I) = 0.0D+0
             DO IPARNT = 1, nparnt
               PCIE(I) = PCIE(I) + frac_ic(iparnt)*PCIEPR(I,map(iparnt))
             END DO
           END DO


C Calculate the prb coefficient from the pr_init adf18 value

           do ipt = 1, nparnt
              pr(ipt, itin, idin) = frac_ic(ipt) *
     &                              pr_init(map(ipt), itin, idin)
           end do

           RETURN

       ENDIF



C-----------------------------------------------------------------------
C Initialization section. If LDATA=.TRUE. this part is not reached.
C
C Read in the adf17 data, and interpolate the output arrays over the
C adas208 temperature and density set.
C
C Construct a mapping from IC-resolved adf18 parents to LS-resolved
C and the statistical split (frac_ic) . Note that the adf17 is LS.
C
C There is a slight dependence on values read from adf18 on data
C taken from the adf18 file. Use the calculated flwsta_ls for this.
C-----------------------------------------------------------------------

       do ipt = 1, nparnt
          sum_ic(ipt) = 0.0D0
          do k = 1, 2
             flwsta(k, iprt) = 0.0D0
          end do
       end do

C-----------------------------------------------------------------------
C VERIFY EXISTENCE OF EXPANSION FILE AND OPEN IT
C-----------------------------------------------------------------------

       INQUIRE( FILE=DSNEXP , EXIST=LEXIST )
       IF (.NOT.LEXIST) THEN
          IF (OPEN27) THEN
             WRITE(IUNT27,*)'B8GETP FAIL: EXPANSION FILE DOES NOT EXIST'
             WRITE(IUNT27,*)'B8GETP FAIL: EXPANSION FILE DOES NOT EXIST'
             WRITE(0,3100)
          ENDIF
          LPDATA = .FALSE.
          RETURN
       ELSE
          OPEN(UNIT=IUNT14 , FILE=DSNEXP)
          IF (OPEN27) THEN
             WRITE(IUNT27,3002) DSNEXP
          ENDIF
       ENDIF

C-----------------------------------------------------------------------
C GATHER DATA FROM EXPANSION FILE INCLUDING NAMELIST, CHECK CONSISTENCY
C OF FILE NAMES AND ISSUE APPROPRIATE WARNINGS
C-----------------------------------------------------------------------

       READ(IUNT14,SEQINF)
       IF (OPEN27) THEN
          WRITE(IUNT27,3006) SEQ , NPARNT , NSHEL , NLEV
       ENDIF

C-----------------------------------------------------------------------
C EXPAND FILENAMES USING XXFLNM
C-----------------------------------------------------------------------

       DSHORT=DSNREF
       CALL XXFLNM(DSHORT,DSNREF,LEXIST)
       DSHORT=DSNCPM
       CALL XXFLNM(DSHORT,DSNCPM,LEXIST)


       IF ( DSNSPF.NE.DSNREF ) THEN
          IF (OPEN27) THEN
             WRITE(IUNT27,*)'B8GETP:WARNING - SPECIFIC ION FILE ',
     &                      'NAME AND REFERENCE DO NOT MATCH'
             WRITE(IUNT27,*)'DSNSPF=',DSNSPF
             WRITE(IUNT27,*)'DSNREF=',DSNREF
          ENDIF
       ENDIF

       IF ( IL .NE. NLEV ) THEN
          IF (OPEN27) THEN
             WRITE(IUNT27,*)'B8GETP FAIL: LEVEL TOTAL IN EXPANSION ',
     &                      'FILE INCONSISTENT '
             WRITE(IUNT27,3100)
             WRITE(0,*)'B8GETP FAIL: LEVEL TOTAL IN EXPANSION ',
     &                 'FILE INCONSISTENT '
          ENDIF
          LPDATA = .FALSE.
          RETURN
       ENDIF

C Parent overview

       READ(IUNT14,2004)STRING,STRING
       DO K=1,NPARNT
          PTSYMA(K)=STRING(9+12*K:19+12*K)
       END DO

       READ(IUNT14,2004)STRING
       DO K=1,NPARNT
         SUBSTRG=STRING(9+12*K:19+12*K)
         READ(SUBSTRG,*)NPTSPA(K)
       END DO

       READ(IUNT14,2004)STRING
       DO K=1,NPARNT
         SUBSTRG=STRING(9+12*K:19+12*K)
         READ(SUBSTRG,*) NSPSYS(K)
       END DO


C Determine if LS or IC resolved and make map of IC->LS if IC-resolved
C
C Assume the adf18 parents are identified as '(2P)' and '(2P 0.5)' for LS/IC

       if (lenstr(PTSYMA(1)).GT.4) then
          licres = .TRUE.
       else
          licres = .FALSE.
       endif

       if (licres) then

C         ic-resolved

          do k = 1, nparnt
             prt_ls(k)  = PTSYMA(k)(1:3)//')'
             read(PTSYMA(k), '(3x, f4.1)')frac_ic(k)
             sum_ic(k) = 0.0D0
          end do

          k      = 1
          map(k) = 1
          do i = 2, nparnt
             do j = 1, nparnt
                if (prt_ls(j).eq.prt_ls(i)) then
                   if (map(j).eq.0) then
                      k = k + 1
                      map(j) = k
                   else
                      map(i) = map(j)
                   endif
                   goto 10
                endif
             enddo
  10         continue
          end do
          ntrm = k

          do i = 1, nparnt
             sum_ic(map(i)) = sum_ic(map(i)) +
     &                       (2.0D0 * frac_ic(i) + 1.0D0)
          end do
          do i = 1, nparnt
             frac_ic(i) = (2.0D0 * frac_ic(i) + 1.0D0) / sum_ic(map(i))
          end do

       else

C         ls-resolved so set the multipliers to 1.0 and 1:1 mappping

          do i = 1, nparnt
             prt_ls(i)  = ptsyma(i)(1:4)
             frac_ic(i) = 1.0D0
             map(i)     = i
          end do

       endif


C Data from adf18 for each parent

       DO 50 IPARNT=1,NPARNT

          IF (OPEN27) THEN
             WRITE(IUNT27,3008) IPARNT, PTSYMA(IPARNT),
     &                          NPTSPA(IPARNT), NSPSYS(IPARNT)
          ENDIF

          do k=1,4
             READ(IUNT14,2004)string
          end do
          IF (OPEN27) WRITE(IUNT27,3010) STRING
          NSPIN = NSPSYS(IPARNT)
          DO K=1,NSPIN
            SUBSTRG=STRING(9+12*K:19+12*K)
            READ(SUBSTRG,*)NSPNA(K,IPARNT)
          END DO

          READ(IUNT14,2004)STRING
          IF (OPEN27) WRITE(IUNT27,3010) STRING
          DO K=1,NSPIN
            SUBSTRG=STRING(9+12*K:19+12*K)
            READ(SUBSTRG,*)NLWSTA(K,IPARNT)
          END DO

          READ(IUNT14,2004)STRING
          IF (OPEN27) WRITE(IUNT27,3010) STRING
          DO K=1,NSPIN
            SUBSTRG=STRING(9+12*K:19+12*K)
            READ(SUBSTRG,*)IMAXSTA(K,IPARNT)
          END DO

          READ(IUNT14,2004)STRING
          IF (OPEN27) WRITE(IUNT27,3010) STRING
          DO K=1,NSPIN
            SUBSTRG=STRING(9+12*K:19+12*K)
            READ(SUBSTRG,*)PLWSTA(K,IPARNT)
          END DO

          READ(IUNT14,2004)STRING
          IF (OPEN27) WRITE(IUNT27,3010) STRING
          DO K=1,NSPIN
            SUBSTRG=STRING(9+12*K:19+12*K)
            READ(SUBSTRG,*)FLWSTA(K,IPARNT)
            flwsta_ls(k,map(iparnt)) = flwsta_ls(k,map(iparnt)) +
     &                                 FLWSTA(K,IPARNT)
          END DO

   50  CONTINUE


C Mapping bundle-n to levels

       do k=1, 4
         READ(IUNT14,2004)string
       end do
       READ(IUNT14,2004)STRNG1,STRING

       NMET=0
       KREC=0

       DO IPARNT=1,NDMET
         NSUP(IPARNT,1)=0
         NSUP(IPARNT,2)=0
       END DO

   55  READ(IUNT14,2004,END=60)STRING
       IF(STRING(1:5).EQ.'     ')THEN
           GO TO 60
       ELSE

           KREC=KREC+1
           READ(STRING,2005)INDA(KREC),LVSYMA(KREC),LSZDA(KREC),
     &                      LSPA(KREC),LSHA(KREC),
     &                      LPTA(KREC),(WGHTA(KREC,J),J=1,NSHEL)

           if (LPTA(KREC).GT.0) THEN
              DO IS = 1,NSPSYS(LPTA(KREC))
                IF(LSPA(KREC).EQ.NSPNA(IS,LPTA(KREC)))THEN
                   NSUP(LPTA(KREC),IS) =
     &                  MAX0(NSUP(LPTA(KREC),IS),LSHA(KREC))
                ENDIF
              END DO
           endif

           IF(LVSYMA(KREC)(1:1).EQ.'*')THEN
               NMET=NMET+1
               IMETR(NMET)=INDA(KREC)
           ENDIF
           GO TO 55

       ENDIF

   60  continue
       CLOSE(IUNT14)


       IF (OPEN27) WRITE(IUNT27,1013)
       DO IPARNT=1,NPARNT
         IF (OPEN27) WRITE(IUNT27,1014)IPARNT, (NSUP(IPARNT,IS),IS=1,2)
       END DO
       IF (OPEN27) WRITE(IUNT27,1013)


C-----------------------------------------------------------------------
C VERIFY EXISTENCE OF CONDENSED BUNDLE-N MATRIX FILE AND OPEN IT
C-----------------------------------------------------------------------

       IF ( (DSNCPM(1:2).EQ.'/ ').OR.(DSNCPM(1:2).EQ.'  ') ) THEN
          IF (OPEN27) WRITE(IUNT27,3100)
          RETURN
       ELSE
          INQUIRE( FILE=DSNCPM , EXIST=LEXIST )
          IF (.NOT.LEXIST) THEN
              IF (OPEN27) THEN
            WRITE(IUNT27,*)'B8GETP FAIL: COND. MAT. FILE DOES NOT EXIST'
            WRITE(IUNT27,'(A)')DSNCPM
                 WRITE(IUNT27,3100)
              ENDIF
              LPDATA = .FALSE.
              RETURN
          ELSE
              OPEN(UNIT=IUNT15 , FILE=DSNCPM)
              IF (OPEN27) WRITE(IUNT27,3004) DSNCPM
          ENDIF
       ENDIF

C-----------------------------------------------------------------------
C GATHER DATA FROM ADF17 CONDENSED BUNDLE-N MATRIX FILE
C-----------------------------------------------------------------------

       READ(IUNT15,2006)LSTRNG
       READ(LSTRNG,2007)SEQM,NUCGM,NPRTM,MAXDM,MAXTM
       READ(IUNT15,2006)LSTRNG,LSTRNG
       READ(LSTRNG,2017)IEDMAT, IECION, IETREC, IEDREC, IERREC, IEXREC,
     &                  IERSYS, IEFPRS, IEFPRE
       READ(IUNT15,2006)LSTRNG
       READ(IUNT15,2008)(DENSM(IN),IN=1,MAXDM)
       READ(IUNT15,2008)(TEM(IT),IT=1,MAXTM)
       READ(IUNT15,2006)LSTRNG

       IF (OPEN27) WRITE(IUNT27,3012) SEQM , NUCGM , NPRTM
       IF (OPEN27) THEN
          WRITE(IUNT27,3014) IEDMAT, IECION, IEDREC, IERREC,
     &                       IEXREC, IERSYS, IEFPRS, IEFPRE
          WRITE(IUNT27,3016) MAXDM
          WRITE(IUNT27,3020) (DENSM(IN),IN=1,MAXDM)
          WRITE(IUNT27,3018) MAXTM
          WRITE(IUNT27,3020) (TEM(IT),IT=1,MAXTM)
       ENDIF

       READ(IUNT15,2006)LSTRNG

C      --------------------------------
C       Loop over parents in adf17 file
C      --------------------------------

       DO 200 IPRT = 1, NPRTM

         IF (LSTRNG(2:5).NE.'IPRT') THEN
            IF (OPEN27) THEN
               WRITE(IUNT27,*)
     &             'B8GETP FAIL: - CBNM PARENT LINE NOT IN POSITION'
               WRITE(IUNT27,3100)
               LPDATA = .FALSE.
            ENDIF
            RETURN
         ELSE
            READ(LSTRNG,2009)IPRTM,TRMPM,SPNPM
            IF(IPRT.NE.IPRTM)THEN
               IF (OPEN27) THEN
                  WRITE(IUNT27,*)
     &                'B8GETP FAIL: - CBNM INCORRECT PARENT INDEX'
                  WRITE(IUNT27,3100)
               ENDIF
               LPDATA = .FALSE.
               RETURN
            ENDIF
         ENDIF

C        zero the recombination power PR array

         DO IN=1,MAXDM
           DO IT=1,MAXTM
             PR(IPRTM,IT,IN)=0.0D+0
           END DO
         END DO

c        jump back to line 80 until we can skip to 195
C        (ie the next LS parent becomes active)

         ISYSM=0
         READ(IUNT15,2006)LSTRNG
  80     READ(IUNT15,2006)LSTRNG
         IF(LSTRNG(51:56).NE.'SPNSYS')THEN
             IF(LSTRNG(2:5).EQ.'IPRT')THEN
                 GO TO 195
             ELSEIF(LSTRNG(1:10).EQ.'----------')THEN
                 GO TO 195
             ELSE
                 IF (OPEN27) THEN
                    WRITE(IUNT27,*)
     &                   'B8GETP FAIL: - CBNM SPIN SYSTEM LINE NOT IN',
     &                   ' POSITION'
                    WRITE(IUNT27,3100)
                 ENDIF
                 LPDATA = .FALSE.
                 RETURN
             ENDIF
         ENDIF

         ISYSM=ISYSM+1
         NUP=NSUP(IPRT,ISYSM)
         READ(LSTRNG,2010)SSYSM(IPRT,ISYSM),NSHLM(IPRT,ISYSM)
         SSYSWT = SSYSM(IPRT,ISYSM) / ( 2.0D0 * SPNPM )
         IF (OPEN27) WRITE(IUNT27,1030) IPRT, ISYSM, NSHLM(IPRT,ISYSM)
         READ(IUNT15,2006)LSTRNG

         DO I = 1,5
            PRHTMP(I) = 0.0D+0
            DO J = 1,5
              PCRTMP(I,J) = 0.0D+0
              DCRTMP(I,J) = 0.0D+0
            ENDDO
         ENDDO
         DO J = 1,4
           PQPTMP(J)   =  0.0D+0
            DO I = 1,5
              PIOTMP(I,J) = 0.0D+0
            ENDDO
         ENDDO

C        ---------------------------------
C        Loop over temperature and density
C        ---------------------------------

         DO 150 IN=1,MAXDM
           DO 145 IT=1,MAXTM

C            -------------
C            ZERO MATRICES
C            -------------
             DO I = 1,5
               IF(I .LE. 4) PQPIND(IT,IN,I,IPRTM,ISYSM)=0.0D+0
               PCRRHS(IT,IN,I,IPRTM,ISYSM)=0.0D+0
               PTRRHS(IT,IN,I,IPRTM,ISYSM)=0.0D+0
               DTREC(IT,I)  = 0.0D+0
               DRREC(IT,I)  = 0.0D+0
               DDREC(IT,I)  = 0.0D+0
               DXREC(IT,I)  = 0.0D+0
               DO J = 1,5
                 DCRMAT(IT,IN,I,J) = 0.0D+0
                 PCRMAT(IT,IN,I,J,IPRTM,ISYSM)=0.0D+0
               ENDDO
               DO J = 1,4
                 DIOMAT(IT,IN,I,J) = 0.0D+0
                 PIOMAT(IT,IN,I,J,IPRTM,ISYSM)=0.0D+0
               ENDDO
             ENDDO
C            -------------

             DO I=1,NSHLM(IPRT,ISYSM)
               READ(IUNT15,2011)NM(I),(PCRTMP(I,J),J=1,5),
     &                                (PIOTMP(I,J),J=1,4),
     &                                 PRHTMP(I)
             END DO


             DO I=1,NSHLM(IPRT,ISYSM)
               READ(IUNT15,2011)NM(I),(DCRTMP(I,J),J=1,5),
     &                                (DIOMAT(IT,IN,I,J),J=1,4),
     &                                 DTREC(IT,I),DDREC(IT,I),
     &                                 DRREC(IT,I),DXREC(IT,I)
             END DO

             READ(IUNT15,2020) PRB(IT,IN,IPRT,ISYSM),(PQPTMP(J),J=1,4)

             IF(IN.EQ.INPRN .AND. IT.EQ.ITPRN )THEN
                IF (OPEN27) THEN
                   WRITE(IUNT27,1050) DENSM(INPRN) , TEM(ITPRN)
                   WRITE(IUNT27,1040)
                   WRITE(IUNT27,2012)((PCRTMP(I,J),J=1,5),I=1,5)
                   WRITE(IUNT27,*)'PIOTMP ='
                   WRITE(IUNT27,2013)((PIOTMP(I,J),J=1,4),I=1,5)
                   WRITE(IUNT27,*)'PRHTMP ='
                   WRITE(IUNT27,2012)(PRHTMP(I),I=1,5)
                   WRITE(IUNT27,*)'PQPTMP ='
                   WRITE(IUNT27,2012)(PQPTMP(I),I=1,4)

                   WRITE(IUNT27,*)'DCRMAT ='
                   WRITE(IUNT27,2012)((DCRTMP(I,J),J=1,5),I=1,5)
                   WRITE(IUNT27,*)'DIOMAT ='
                   WRITE(IUNT27,2013)((DIOMAT(IT,IN,I,J),J=1,4),I=1,5)
                   WRITE(IUNT27,*)'DTREC ='
                   WRITE(IUNT27,2012)(DTREC(IT,I),I=1,5)
                   WRITE(IUNT27,*)'DRREC ='
                   WRITE(IUNT27,2012)(DRREC(IT,I),I=1,5)
                   WRITE(IUNT27,*)'DDREC ='
                   WRITE(IUNT27,2012)(DDREC(IT,I),I=1,5)
                   WRITE(IUNT27,*)'DXREC ='
                   WRITE(IUNT27,2012)(DXREC(IT,I),I=1,5)
                ENDIF
             ENDIF

C-----------------------------------------------------------------------
C CHECK FOR NEGATIVES IN PRHTMP
C     - IF SO, ZERO AND SUBSTITUTE DIRECT PARTS ACCORDING TO FORM
C-----------------------------------------------------------------------

             DO IR=1,NSHLM(IPRT,ISYSM)
               IF(PRHTMP(IR) .LT. 0.0D0)THEN
                  PRHTMP(IR) = 0.0D+0
                  IF( IETREC .EQ. 0) THEN
                     PRHTMP(IR) = PRHTMP(IR) + DTREC(IT,IR)
                  ENDIF
                  IF( IEDREC .EQ. 0) THEN
                     PRHTMP(IR) = PRHTMP(IR) + DDREC(IT,IR)
                  ENDIF
                  IF( IERREC .EQ. 0) THEN
                     PRHTMP(IR) = PRHTMP(IR) + DRREC(IT,IR)
                  ENDIF
                  IF( IEXREC .EQ. 0) THEN
                     PRHTMP(IR) = PRHTMP(IR) + DXREC(IT,IR)
                  ENDIF
               ENDIF
             END DO

C-----------------------------------------------------------------------
C ADD ON DIRECT PROCESSES IF THEY ARE NOT ALREADY ADDED ON
C-----------------------------------------------------------------------

             DO IR = 1,NSHLM(IPRT,ISYSM)

               DO IC = 1,NSHLM(IPRT,ISYSM)
                 IF( IEDMAT .EQ. 1) THEN
                   PCRTMP(IR,IC) = PCRTMP(IR,IC) + DCRTMP(IR,IC)
                 ENDIF
               END DO

               IF( IETREC .EQ. 1) THEN
                 PRHTMP(IR) = PRHTMP(IR) + DTREC(IT,IR)
               ENDIF
               IF( IEDREC .EQ. 1) THEN
                 PRHTMP(IR) = PRHTMP(IR) + DDREC(IT,IR)
               ENDIF
               IF( IERREC .EQ. 1) THEN
                 PRHTMP(IR) = PRHTMP(IR) + DRREC(IT,IR)
               ENDIF
               IF( IEXREC .EQ. 1) THEN
                 PRHTMP(IR) = PRHTMP(IR) + DXREC(IT,IR)
               ENDIF
               IF( IERSYS .EQ. 1) THEN
                   PRHTMP(IR)    =  PRHTMP(IR) * SSYSWT
                   DTREC(IT,IR)  =  DTREC(IT,IR)  * SSYSWT
                   DDREC(IT,IR)  =  DDREC(IT,IR)  * SSYSWT
                   DRREC(IT,IR)  =  DRREC(IT,IR)  * SSYSWT
                   DXREC(IT,IR)  =  DXREC(IT,IR)  * SSYSWT
               ENDIF

               DO IC = 1,NPRTM
                 IF( IECION .EQ. 1 ) THEN
                   PIOTMP(IR,IC) = PIOTMP(IR,IC) + DIOMAT(IT,IN,IR,IC)
                   PCRTMP(IR,IR) = PCRTMP(IR,IR) + DIOMAT(IT,IN,IR,IC)
                 ENDIF
                 IF( IERSYS .EQ. 1 .AND. IR .EQ. 1) THEN
                   PQPTMP(IC) = PQPTMP(IC) * SSYSWT
                 ENDIF
               END DO


             END DO


             IF(IN.EQ.INPRN .AND. IT.EQ.ITPRN )THEN
                IF (OPEN27) THEN
                   WRITE(IUNT27,1042)
                   WRITE(IUNT27,2012)((PCRTMP(I,J),J=1,5),I=1,5)
                   WRITE(IUNT27,*)'PIOTMP ='
                   WRITE(IUNT27,2013)((PIOTMP(I,J),J=1,4),I=1,5)
                   WRITE(IUNT27,*)'PRHTMP ='
                   WRITE(IUNT27,2012)(PRHTMP(I),I=1,5)
                   WRITE(IUNT27,*)'PQPTMP ='
                   WRITE(IUNT27,2012)(PQPTMP(I),I=1,4)
                ENDIF
             ENDIF

C-----------------------------------------------------------------------
C CORRECT MATRICES FOR FRACTIONAL PARENTAGE COEFFICIENTS FOR
C GROUND STATE IONISATION AND EXCITATION
C            WJD       13/8/93
C-----------------------------------------------------------------------
C
C             FRACPRT = FLWSTA(IPRT,ISYSM)
             FRACPRT = FLWSTA_ls(ISYSM,IPRT)

C            -----------------------------------
C            FIRST CORRECT EXCITATION COMPONENTS
C            -----------------------------------

             IF( IEFPRE .EQ. 1) THEN

                 IF( IEFPRS .EQ. 1) THEN
                    PCRTMP(1,1) = PCRTMP(1,1) * FRACPRT
                 ELSE
                   DO IC = 1,NPRTM
                      PCRTMP(1,1) = ( PCRTMP(1,1) - DIOMAT(IT,IN,IC,1))
     &                                *FRACPRT +  DIOMAT(IT,IN,IC,1)
                   ENDDO
                 ENDIF

                 DO IC = 1,NPRTM
                    PIOTMP(IC,1) = (PIOTMP(IC,1) - DIOMAT(IT,IN,IC,1) )
     &                                *FRACPRT +  DIOMAT(IT,IN,IC,1)
                 ENDDO

                 DO IR = 1,NSHLM(IPRT,ISYSM)
                    PCRTMP(IR,1) = PCRTMP(IR,1) * FRACPRT
                 ENDDO

             ENDIF

C            ------------------------------------------------------
C            NOW CORRECT IONISATION AND THREE BODY RECOM COMPONENTS
C            ------------------------------------------------------

             IF( IEFPRS .EQ. 1) THEN

                 DO IC = 1,NPRTM
                    PIOTMP(IC,1) = (PIOTMP(IC,1) - DIOMAT(IT,IN,IC,1) )
     &                                +  DIOMAT(IT,IN,IC,1) * FRACPRT
                    PCRTMP( 1,1) = (PCRTMP( 1,1) - DIOMAT(IT,IN,IC,1) )
     &                                +  DIOMAT(IT,IN,IC,1) * FRACPRT
                    DIOMAT(IT,IN,IC,1) =  DIOMAT(IT,IN,IC,1) * FRACPRT
                 ENDDO
                 PRHTMP(1)       =  (PRHTMP(1) - DTREC(IT,1) )
     &                               +  FRACPRT * DTREC(IT,1)
                 DTREC(IT,1)     =   DTREC(IT,1) * FRACPRT

             ENDIF

C-----------------------------------------------------------------------
C    CHECK SIZE OF CONDENSED MATRICES AGAINST MAXIMUM NSHELL OF LOW
C    LEVEL DATA
C-----------------------------------------------------------------------

             IUP = NUP-NM(1)+1
             if (iup.lt.0) write(0,*)'Warning: negative N-upper for ',
     &                                '(iprt, isysm) = ', iprt, isysm
             if (iup.gt.0) IUPA(IPRT,ISYSM)=IUP
             IF (NUP.LT. NM(NSHLM(IPRT,ISYSM)) .and. iup.GT.0 )THEN

C-----------------------------------------------------------------------
C PROJECTED MATRIX FROM CBNM EXCEEDS REQUIREMENT - CONDENSE FURTHER
C-----------------------------------------------------------------------

               IMAX=NM(NSHLM(IPRT,ISYSM))-NUP
               DO II=1,IMAX
                 DO JJ=1,IMAX
                 AMAT(II,JJ)=PCRTMP(II+IUP,JJ+IUP)
                 END DO
               END DO

               LSOLVE=.FALSE.
               CALL XXMINV(LSOLVE,NDSHL,IMAX,AMAT,BRHS,DINTX)

               IF(IN.EQ.INPRN .AND. IT.EQ.ITPRN )THEN
                   IF (OPEN27) THEN
                      WRITE(IUNT27,1044)
                      DO I = 1,IMAX
                         WRITE(IUNT27,2012) (AMAT(I,J),J=1,IMAX)
                      ENDDO
                   ENDIF
               ENDIF


               DO IR=1,IUP

                 SUM=PRHTMP(IR)
                 DO II=1,IMAX
                   DO JJ=1,IMAX
                     SUM = SUM -
     &                     PCRTMP(IR,JJ+IUP)*AMAT(JJ,II)*PRHTMP(II+IUP)
                   END DO
                 END DO
                 PCRRHS(IT,IN,IR,IPRTM,ISYSM)=SUM

                 DO IS=1,IUP
                   SUM=PCRTMP(IR,IS)
                   DO II=1,IMAX
                     DO JJ=1,IMAX
                        SUM=SUM-PCRTMP(IR,JJ+IUP)*AMAT(JJ,II)*
     &                      PCRTMP(II+IUP,IS)
                     END DO
                   END DO
                   PCRMAT(IT,IN,IR,IS,IPRTM,ISYSM)=SUM
                 END DO

                 DO IS=1,NPRTM
                   SUM=PIOTMP(IR,IS)
                   DO II=1,IMAX
                     DO JJ=1,IMAX
                       SUM=SUM-PIOTMP(JJ+IUP,IS)*AMAT(JJ,II)*
     &                     PCRTMP(II+IUP,IR)
                     END DO
                   END DO
                   PIOMAT(IT,IN,IR,IS,IPRTM,ISYSM) = SUM
                 END DO

                 DO IC = 1, IUP
                    SUM = DCRTMP(IR,IC)
                    IF( IC .EQ. IR) THEN
                       DO II = 1,IMAX
                          SUM = SUM + DCRTMP(IUP+II,IC)
                       ENDDO
                    ENDIF
                    DCRMAT(IT,IN,IR,IC) = SUM
                 ENDDO

               END DO


               DO IS=1,IPRT-1
                 SUM = PQPTMP(IS)
                 DO II=1,IMAX
                   DO JJ=1,IMAX
                     SUM = SUM +
     &                     PIOTMP(JJ+IUP,IS)*AMAT(JJ,II)*PRHTMP(II+IUP)
                   END DO
                 END DO
                 PQPIND(IT,IN,IS,IPRTM,ISYSM) = SUM
               END DO

               IF(IN.EQ.INPRN .AND. IT.EQ.ITPRN )THEN
                  IF (OPEN27) THEN
                     WRITE(IUNT27,1046)
                     WRITE(IUNT27,2012)((PCRMAT(IT,IN,I,J,IPRTM,ISYSM),
     &                    J=1,5),I=1,5)
                     WRITE(IUNT27,*)'DCRMAT ='
                     WRITE(IUNT27,2012)((DCRMAT(IT,IN,I,J),
     &                    J=1,5),I=1,5)
                     WRITE(IUNT27,*)'PIOMAT ='
                     WRITE(IUNT27,2013)((PIOMAT(IT,IN,I,J,IPRTM,ISYSM),
     &                    J=1,4),I=1,5)
                     WRITE(IUNT27,*)'PCRRHS ='
                  WRITE(IUNT27,2012)(PCRRHS(IT,IN,I,IPRTM,ISYSM),I=1,5)
                     WRITE(IUNT27,*)'PQPIND ='
                  WRITE(IUNT27,2012)(PQPIND(IT,IN,I,IPRTM,ISYSM),I=1,4)
                  ENDIF
               ENDIF

             ELSE

                DO I=1,NSHLM(IPRT,ISYSM)

                  DO J=1,5
                    PCRMAT(IT,IN,I,J,IPRTM,ISYSM)=PCRTMP(I,J)
                  END DO
                  DO J=1,5
                    DCRMAT(IT,IN,I,J) = DCRTMP(I,J)
                  END DO
                  DO J=1,4
                    PIOMAT(IT,IN,I,J,IPRTM,ISYSM)=PIOTMP(I,J)
                  END DO
                  PCRRHS(IT,IN,I,IPRTM,ISYSM)=PRHTMP(I)
                END DO

                DO IC = 1,4
                  PQPIND(IT,IN,IC,IPRTM,ISYSM)=PQPTMP(IC)
                END DO

             ENDIF

C-----------------------------------------------------------------------
C PERFORM DIRECT COEFFICIENT SUBTRACTIONS FROM CONDENSED ARRAYS HERE
C-----------------------------------------------------------------------

             DO IR = 1,IUP

               DO IC= 1,IUP
                 PCRMAT(IT,IN,IR,IC,IPRTM,ISYSM) =
     &              PCRMAT(IT,IN,IR,IC,IPRTM,ISYSM) -
     &              DCRMAT(IT,IN,IR,IC)
               END DO
               DO IC= 1,NPRTM
                 PCRMAT(IT,IN,IR,IR,IPRTM,ISYSM) =
     &              PCRMAT(IT,IN,IR,IR,IPRTM,ISYSM) -
     &              DIOMAT(IT,IN,IR,IC)
               END DO

               IF ( LRSEL ) THEN
                  PCRRHS(IT,IN,IR,IPRTM,ISYSM) =
     &              PCRRHS(IT,IN,IR,IPRTM,ISYSM)  - DDREC(IT,IR)
     &                                            - DRREC(IT,IR)
               ENDIF
               PCRRHS(IT,IN,IR,IPRTM,ISYSM) =
     &              PCRRHS(IT,IN,IR,IPRTM,ISYSM)  - DTREC(IT,IR)

               IF ( .NOT. LIOSEL ) THEN
                  PTRRHS(IT,IN,IR,IPRTM,ISYSM) =
     &              PTRRHS(IT,IN,IR,IPRTM,ISYSM)  + DTREC(IT,IR)
               ENDIF

               IF ( LHSEL ) THEN
                  PCRRHS(IT,IN,IR,IPRTM,ISYSM) =
     &              PCRRHS(IT,IN,IR,IPRTM,ISYSM) - DXREC(IT,IR)
               ENDIF

               DO IC= 1,NPRTM
                 IF ( LIOSEL ) THEN
                    PIOMAT(IT,IN,IR,IC,IPRTM,ISYSM) =
     &              PIOMAT(IT,IN,IR,IC,IPRTM,ISYSM) -
     &              DIOMAT(IT,IN,IR,IC)
                 ENDIF
               END DO

             END DO

             READ(IUNT15,2006) LSTRNG


  145      CONTINUE        ! end of density loop IN=1,MAXDM
  150    CONTINUE          ! end of Te loop      IT=1,MAXTM

C-----------------------------------------------------------------------
C INTERPOLATE WITH TWO WAY CUBIC SPLINE  - SUBROUTINE B8SPLN
C CHECK ARRAYS FOR CORRECT SIGNS OR ZEROS, FIX UP IF NECESSARY AND
C CONVERT TO POSITIVE FOR SPLINING.
C RECORRECT TO ZEROES BELOW TEMIN OR DEMIN
C CHECK PCRMAT COLUMN SUMS >=0 AFTER SPLINING
C
C Note the call to b8splT here rather than b8spln which removes effective
C zeros before splining.  In particulat, the PCRMAT array has effective zeros
C not all of which are at the beginning these need to eliminated before
C splining.
C
C Split into three groups
C    (a) do loop 180 over IR for PCRRHS, PTRRHS
C                       and over IR,IS for PCRMAT and PIOMAT
C    (b) do loop 190 only over IS for PQPIND
C    (c) prb for each parent
C-----------------------------------------------------------------------

C        ------------------------------------------------------
C        (a) loop over IR for PCRRHS, PTRRHS, PCRMAT and PIOMAT
C        ------------------------------------------------------

         DO 180 IR=1,IUP

C          ** pcrrhs()

           TEMIN=TEM(MAXTM)
           DO IT=MAXTM,1,-1
             DO IN=1,MAXDM
               V=PCRRHS(IT,IN,IR,IPRTM,ISYSM)
               IF(V.LE.0.0D0)THEN
                   IF (OPEN27) WRITE(IUNT27,2014) IT,IN,IR
                   V=0.0D0
               ENDIF
               IF(V.EQ.0.0D0.AND.IT.LT.MAXTM)THEN
                   V=ARRIN(IT+1,IN)
               ELSEIF(V.EQ.0.0D0)THEN
                   V=1.0D-72
               ELSE
                   TEMIN=TEM(IT)
               ENDIF
               ARRIN(IT,IN)=V
             END DO
           END DO

           CALL b8splt(NDTEM , NDDEN ,
     &                 MAXTM , MAXDM , MAXT  , MAXD  ,
     &                 TEM   , DENSM , TEA   , DENSA ,
     &                 ARRIN , ARROUT,
     &                 LTRNG , LDRNG , 1     , 2)

           DO IT=1,MAXT
             DO IN=1,MAXD
                IF(TEA(IT).LT.TEMIN)THEN
                   PCRRHS(IT,IN,IR,IPRTM,ISYSM)=0.0D0
                ELSE
                  PCRRHS(IT,IN,IR,IPRTM,ISYSM)=ARROUT(IT,IN)
                ENDIF
             END DO
           END DO


C          ** ptrrhs()

           TEMIN=TEM(MAXTM)
           DO IT=MAXTM,1,-1
             DO IN=1,MAXDM
               V=PTRRHS(IT,IN,IR,IPRTM,ISYSM)
               IF(V.LE.0.0D0)THEN
                   V=0.0D0
               ENDIF
               IF(V.EQ.0.0D0.AND.IT.LT.MAXTM)THEN
                   V=ARRIN(IT+1,IN)
               ELSEIF(V.EQ.0.0D0)THEN
                   V=1.0D-72
               ELSE
                   TEMIN=TEM(IT)
               ENDIF
               ARRIN(IT,IN)=V
             END DO
           END DO

           CALL b8splt(NDTEM , NDDEN ,
     &                 MAXTM , MAXDM , MAXT  , MAXD  ,
     &                 TEM   , DENSM , TEA   , DENSA ,
     &                 ARRIN , ARROUT,
     &                 LTRNG , LDRNG , 0     , 4)

           DO IT=1,MAXT
              DO IN=1,MAXD
                IF(TEA(IT).LT.TEMIN)THEN
                    PTRRHS(IT,IN,IR,IPRTM,ISYSM)=0.0D0
                ELSE
                    PTRRHS(IT,IN,IR,IPRTM,ISYSM)=ARROUT(IT,IN)
                ENDIF
              END DO
           END DO

C          ---------------------
C          interior loop over IS
C          ---------------------

C          ** ptrrhs()

           DO IS=1,IUP
             DO IT=MAXTM,1,-1
               DO IN=1,MAXDM
                 IF(IR.EQ.IS)THEN
                     V=PCRMAT(IT,IN,IR,IS,IPRTM,ISYSM)
                 ELSE
                     V=-PCRMAT(IT,IN,IR,IS,IPRTM,ISYSM)
                 ENDIF
                 IF(V.LE.0.0D0)THEN
                    V=1.0D-72
                 ENDIF
                 ARRIN(IT,IN)=V
               END DO
             END DO

             CALL b8splt (NDTEM , NDDEN ,
     &                    MAXTM , MAXDM , MAXT  , MAXD  ,
     &                    TEM   , DENSM , TEA   , DENSA ,
     &                    ARRIN , ARROUT,
     &                    LTRNG , LDRNG , 0     , 4)

             DO IT=1,MAXT
               DO IN=1,MAXD
                 IF(IR.EQ.IS)THEN
                     PCRMAT(IT,IN,IR,IS,IPRTM,ISYSM)=ARROUT(IT,IN)
                 ELSE
                     PCRMAT(IT,IN,IR,IS,IPRTM,ISYSM)=-ARROUT(IT,IN)
                 ENDIF
               END DO
             END DO

           END DO

C          ** piomat()

           DO IS=1,NPRTM
C
             TEMIN=TEM(MAXTM)
             DO IT=MAXTM,1,-1
               DO IN=1,MAXDM
                V=PIOMAT(IT,IN,IR,IS,IPRTM,ISYSM)
                IF(V.LE.0.0D0)THEN
                    V = 1.0D-72
                ENDIF
                ARRIN(IT,IN)=V
               END DO
             END DO
C
             CALL b8splt(NDTEM , NDDEN ,
     &                   MAXTM , MAXDM , MAXT  , MAXD  ,
     &                   TEM   , DENSM , TEA   , DENSA ,
     &                   ARRIN , ARROUT,
     &                   LTRNG , LDRNG , 0     , 4)

             DO IT=1,MAXT
               DO IN=1,MAXD
                 PIOMAT(IT,IN,IR,IS,IPRTM,ISYSM)=ARROUT(IT,IN)
               END DO
             END DO

           END DO


  180    CONTINUE      ! end first of group


C        ----------
C        (b) PQPIND
C        ----------

         DO 190 IS=1,NPRTM

           TEMIN=TEM(MAXTM)
           DO IT=MAXTM,1,-1
             DO IN=1,MAXDM
               V=PQPIND(IT,IN,IS,IPRTM,ISYSM)
               IF(V.LE.0.0D0)THEN
                   V=0.0D0
               ENDIF
               IF(V.EQ.0.0D0.AND.IT.LT.MAXTM)THEN
                   V=ARRIN(IT+1,IN)
               ELSEIF(V.EQ.0.0D0)THEN
                   V=1.0D-72
               ELSE
                  TEMIN=TEM(IT)
               ENDIF
               ARRIN(IT,IN)=V
             END DO
           END DO

           CALL b8splt(NDTEM , NDDEN ,
     &                 MAXTM , MAXDM , MAXT  , MAXD  ,
     &                 TEM   , DENSM , TEA   , DENSA ,
     &                 ARRIN , ARROUT,
     &                 LTRNG , LDRNG , 0     , 0)

           DO IT=1,MAXT
             DO IN=1,MAXD
               IF(TEA(IT).LT.TEMIN)THEN
                  PQPIND(IT,IN,IS,IPRTM,ISYSM)=0.0D0
               ELSE
                  PQPIND(IT,IN,IS,IPRTM,ISYSM)=ARROUT(IT,IN)
               ENDIF
             END DO
           END DO

  190    CONTINUE


C        --------------------------------------
C        (c) recombination-bremsstrahlung power
C        --------------------------------------


         TEMIN=TEM(MAXTM)
         DO IT=MAXTM,1,-1
           DO IN=1,MAXDM
             V=PRB(IT,IN,IPRTM,ISYSM)
             IF(V.LE.0.0D0)THEN
                 V=0.0D0
             ENDIF
             IF(V.EQ.0.0D0.AND.IT.LT.MAXTM)THEN
                 V=ARRIN(IT+1,IN)
             ELSEIF(V.EQ.0.0D0)THEN
                 V=1.0D-72
             ELSE
                TEMIN=TEM(IT)
             ENDIF
             ARRIN(IT,IN)=V
           END DO
         END DO

         CALL b8splt(NDTEM , NDDEN ,
     &               MAXTM , MAXDM , MAXT  , MAXD  ,
     &               TEM   , DENSM , TEA   , DENSA ,
     &               ARRIN , ARROUT,
     &               LTRNG , LDRNG , 1     , 2)

         DO IT=1,MAXT
           DO IN=1,MAXD
c             IF(TEA(IT).LT.TEMIN)THEN
c                 PRBSP=0.0D0
c             ELSE
                 PRBSP=ARROUT(IT,IN)
c             ENDIF
              PR_INIT(IPRTM,IT,IN)=PR_INIT(IPRTM,IT,IN)+PRBSP
           END DO
         END DO


C  Return for next adf17 parent
         IT=-1
         IN=-1

         GO TO 80

  195    NSYSM(IPRT)=ISYSM

  200  CONTINUE                   ! end loop over adf17 parents (IPRT = 1, NPRTM)

       CLOSE(IUNT15)

       LPDATA = .TRUE.
       IF (OPEN27) THEN
          WRITE(IUNT27,3026) MAXD
          WRITE(IUNT27,3020) (DENSA(IN),IN=1,MAXD )
          WRITE(IUNT27,3028) MAXT
          WRITE(IUNT27,3020) (TEA(IT),IT=1,MAXT )
          WRITE(IUNT27,3100)
          WRITE(IUNT27,3090)
          WRITE(IUNT27,3100)
          WRITE(0,3090)
       ENDIF
       RETURN

C-----------------------------------------------------------------------
 1010  FORMAT(1H ,'IPARNT=',I3,3X,'NPARNT=',I3,3X,'ISPIN=',I3,3X,
     &            'NSPIN=',I3,3X,'SSYSWT=',F6.3,3X,'FRACPRT=',F6.3,3X,
     &            'IUPA =',I3,3X,'NSUP=',I3)
 1012  FORMAT(1H ,'ISPIN=',I3,3X,'NSPIN=',I3,3X,'NPARNT=',I3,3X,
     &            'FLWSTA=',F6.3)
 1013  FORMAT(1H ,3X)
 1014  FORMAT(1H ,'IPARNT=',I3,3X,'NSUP(ISYS=1)=',I3,3X,
     &            'NSUP(ISYS=2)=',I3)
 1016 FORMAT(1H ,'  KI=',I3,3X,'IND =',I3,
     &           'KREC=',I3,3X,'IPT=',I3,3X,
     &           'IR=',I3,3X,'ITIN=',I3,3X,'IDIN=',I3)
 1018 FORMAT(1H ,'   KI=',I3,3X,'IND=',I3,3X,
     &              'IR=',I3,3X,
     &              'IS=',I3,3X,'NPRTM=',I3)
 1020 FORMAT(1H ,1P,'PV3PR(I,IPARNT)=',D10.2,3X,
     &          'WGHTA(KI,IPT)=',D10.2,3X,
     &          'PCRRHS(ITIN,IDIN,IR,IPARNT,ISPIN)=',D10.2)
 1022 FORMAT(1H ,1P,'PCIEPR(I,IS)=',D10.2,3X,
     &          'PIOMAT(ITIN,IDIN,IR,IS,IPARNT,ISPIN)=',D10.2)
 1024 FORMAT(1H ,'  KJ=',I3,3X,'KREC=',I3,3X,'J=',I3,3X,'JPT=',I3,3X,
     &           'IS=',I3)
 1026 FORMAT(1H ,1P,'PCC(I,J)=',D10.2,3X,
     &          'PCRMAT(ITIN,IDIN,IR,IS,IPARNT,ISPIN)=',D10.2)
 1028 FORMAT(1H ,1P,'PCC(I,J)=',D10.2,3X,
     &          'WGHTA(KI,IPT)=',D10.2,3X,
     &          'PCRMAT(ITIN,IDIN,IR,IS,IPARNT,ISPIN)=',D10.2)
 1030 FORMAT(1H /,1H ,'IPRT=',I3,3X,'ISYSM=',I3,3X,'NSHLM=',I3/
     &              1H ,'--------------------------------'/)
 1032 FORMAT(1H /,1H ,'IPRT=',I3,3X,'ISYSM=',I3,3X,'NSHLM=',I3,3X,
     &                'IUPA = ',I3)
 1040 FORMAT(1H /,1H ,'MATRICES READ IN FROM CONDENSED FILE :' /
     &            1H ,'PCRTMP =')
 1042 FORMAT(1H /,1H ,'MATRICES AFTER ADDITION OF DIRECT PARTS :' /
     &            1H ,'PCRTMP =')
 1044 FORMAT(1H /,1H ,'AMAT = ')
 1046 FORMAT(1H /,1H ,'MATRICES AFTER CONDENSATION :' /
     &            1H ,'PCRMAT =')
 1047 FORMAT(1H /,1H ,'MATRICES AFTER SUBTRACTION OF DIRECT PARTS :' /
     &            1H ,'PCRMAT =')
 1048 FORMAT(1H /,1H ,'MATRICES AFTER INTERPOLATION :' /
     &            1H ,'PCRMAT FINAL =')
 1050 FORMAT(/ /,3X,' TEST OUTPUT FOR DENSM = ',1P,D10.2,3X,
     &                               'TEM = ',D10.2)

 2004  FORMAT(1A250)
 2005  FORMAT(I3,1A22,I4,3I3,3X,F7.4,1X,F7.4,1X,F7.4,1X,F7.4,1X,F7.4)
 2006  FORMAT(1A250)
 2007  FORMAT(12X,1A2,11X,I2,11X,I2,9X,I2,9X,I2)
C----------------------------------------------------------------
C  ALTER FORMAT FOR CONSISTENCY WITH LDH CBNM FILES
C2008  FORMAT(12X,1P,15D10.2)
C----------------------------------------------------------------
 2008  FORMAT(12X,1P,12D10.2)
 2009  FORMAT(8X,I2,14X,1A4,14X,F3.0)
 2010  FORMAT(59X,F3.0,13X,I2)
 2011  FORMAT(15X,I2,2X,1P,5D17.10,2X,1P,4D17.10,2X,1P,4D17.10)
 2012  FORMAT(1H ,1P,5D12.4)
 2013  FORMAT(1H ,1P,4D12.4)
 2014  FORMAT(1H ,'*** WARNING - PCRRHS(IT=',I2,',IN=',I2,',IR=',I2,')=0
     &, CORRECTIVE ACTION TAKEN')
 2015  FORMAT(1H ,'*** WARNING - PCRMAT(IT=',I2,',IN=',I2,',IR=',I2,',IS
     &=',I2,')=0, CORRECTIVE ACTION TAKEN')
 2016  FORMAT(1H ,'*** WARNING - PIOMAT(IT=',I2,',IN=',I2,',IR=',I2,',IS
     &=',I2,')=0, CORRECTIVE ACTION TAKEN')
 2017  FORMAT(9X,I2,8(11X,I2))
 2018  FORMAT(1H ,'*** WARNING - PQPIND(IT=',I2,',IN=',I2,',IS=',I2,
     & ')=0, CORRECTIVE ACTION TAKEN')
 2020  FORMAT(19X,1PD17.10,70X,1P,4D17.10)
 2022  FORMAT(1H ,'*** WARNING - PTRRHS(IT=',I2,',IN=',I2,',IR=',I2,')=0
     &, CORRECTIVE ACTION TAKEN')

 3000  FORMAT(3X/
     &        / /,100('-')/
     &        1H ,'   INITIATION OF INDIRECT ARRAYS' /
     &        / /,100('-')/
     &        3X)
 3002  FORMAT(/ /,'  EXPANSION DATA FILE : ',A /
     &        1H ,'  --------------------- ')
 3004  FORMAT(/ /,'  CONDENSED MATRIX MASTER FILE : ',A /
     &        1H ,'  ------------------------------- ')
 3006  FORMAT(1H ,' SEQ = ',1A2,3X,'NPARNT =',I2,3X,
     &            'NSHELM = ',I2,3X,'NLEV =',I2)
 3008  FORMAT(/ /,'IPARNT = ',I2,':   TERM = ',1A11,3X,
     &             'NPTSPA = ',I2,3X,'NSPSYS = ',I2 )
 3010  FORMAT(1H ,1A80)
 3012  FORMAT(1H ,' SEQM = ',1A2,3X,'NUCGM = ',I2,3X,'NPRTM = ',I2)
 3014  FORMAT(1H ,' IEDMAT = ',I2,3X,
     &            ' IECION = ',I2,3X,
     &            ' IETREC = ',I2,3X,
     &            ' IEDREC = ',I2,3X,
     &            ' IERREC = ',I2,3X,
     &            ' IEXREC = ',I2,3X,
     &            ' IERSYS = ',I2,3X,
     &            ' IEFPRS = ',I2,3X,
     &            ' IEFPRE = ',I2)
 3016  FORMAT(1H ,' MAXD = ',I2,3X,'DENSA(CM-3) =')
 3018  FORMAT(1H ,' MAXT = ',I2,3X,'TEA(K) =')
 3020  FORMAT(1H ,1P,8D10.2)
 3026  FORMAT(1H ,' MAXD = ',I2,3X,'DENSA(CM-3) =')
 3028  FORMAT(1H ,' MAXT = ',I2,3X,'TEA(K) =')
 3090  FORMAT(1H ,'INITIATION OF MATRICES SUCCESSFUL. LPDATA = TRUE')
 3100  FORMAT(/ /,100('-'))
C-----------------------------------------------------------------------

      END
