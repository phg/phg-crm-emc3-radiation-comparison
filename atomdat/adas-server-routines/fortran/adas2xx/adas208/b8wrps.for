      subroutine b8wrps( open15 , iunt15 , open13 , iunt13 ,
     &                   dsninc , dsfull , dsnexp , ibsela ,
     &                   titled , date   , user   ,
     &                   ndlev  , ndtem  , ndden  , ndmet  , ndtrn ,
     &                   ndsel  ,
     &                   lnorm  ,
     &                   iz     , iz0    , iz1    ,
     &                   il     , nmet   , nord   ,
     &                   maxt   , maxd   , icntr  , icnti  , icnth ,
     &                   isa    , ila    , xja    ,
     &                   cstrga , wa     ,
     &                   icnte  ,
     &                   ie1a   , ie2a   , aa     ,
     &                   imetr  , iordr  , teva   , densa  ,
     &                   npl    , nplr   , npli   , npl3   ,
     &                   lrsel  , lisel  , lhsel  , liosel ,
     &                   lpsel  , lzsel  , lnsel  ,
     &                   nwvl   , wvls   , wvll   , avlt   ,
     &                   ntrsel , itrlow , itrup  ,
     &                   stvr   , stvi   , stvh   ,
     &                   ratpia , ratmia , stack  ,
     &                   fvionr , sgrda  ,
     &                   lsseta , lss04a
     &                 )

      implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B8WRPS *******************
C
C  PURPOSE:  To output data to pec and sxb passing files.
C
C  CALLING PROGRAM: ADAS208
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNT15    = UNIT NUMBER FOR PECS.
C  INPUT : (L*4)  OPEN15    =.TRUE. IF SEC OUTPUT IS REQUIRED
C  INPUT : (I*4)  IUNT13    = UNIT NUMBER FOR SXBS.
C  INPUT : (L*4)  OPEN13    =.TRUE. IF SXB OUTPUT IS REQUIRED
C  INPUT : (C*44) DSNINC    = INPUT ADF04 DATA SET NAME
C  INPUT : (C*80) DSFULL    = INPUT ADF07 DATA SET NAME
C  INPUT : (C*80) DSNEXP    = INPUT EXPANSION FILE
C  INPUT : (I*4)  IBSELA(,) = IONISATION DATA BLOCK SELECTION INDICES
C                              1ST DIMENSION - (Z) ION METASTABLE COUNTER
C                              2ND DIMENSION - (Z+1) ION METASTABLE COUNTER
C  INPUT : (C*3)  TITLED    = ELEMENT SYMBOL.
C  INPUT : (C*8)  DATE      = CURRENT DATE.
C  INPUT : (C*30) USER      = FULL NAME OF AUTHOR.
C
C  INPUT : (I*4)  NDLEV     = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM     = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN     = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET     = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NDTRN     = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C  INPUT : (I*4)  NDSEL     = PARAMETER = MAX. NO. OF USER SUPPLIED TRANSITONS
C  INPUT : (L*4)  LNORM     =.TRUE.  => IF NMET=1 THEN VARIOUS
C                                       EMISSIVITY OUTPUT FILES
C                                       NORMALISED TO STAGE TOT.POPULATN.
C                                       (** NORM TYPE = T)
C                           =.FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                       METASTABLE POPULATIONS.
C                                        (** NORM TYPE = M)
C  INPUT : (I*4)  IZ        =  RECOMBINED ION CHARGE READ
C  INPUT : (I*4)  IZ0       =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ1       = RECOMBINING ION CHARGE READ
C                             (NOTE: IZ1 SHOULD EQUAL IZ+1)
C
C  INPUT : (I*4)  IL        = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  NMET      = NUMBER OF METASTABLES LEVELS: 1<=NMET<=NDMET
C  INPUT : (I*4)  NORD      = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C  INPUT : (I*4)  MAXT      = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C  INPUT : (I*4)  MAXD      = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C  INPUT : (I*4)  ICNTR     = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT : (I*4)  ICNTI     = NUMBER OF LOWER STAGE IONISATIONS      INPUT
C  INPUT : (I*4)  ICNTH     = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C  INPUT : (I*4)  ISA()     = MULTIPLICITY FOR LEVEL 'IA()'
C                             NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()     = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()     = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                             NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (C*18) CSTRGA()  = NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (R*8)  WA()      = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                             DIMENSION: LEVEL INDEX
C  INPUT : (I*4)  ICNTE     = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT : (I*4)  IE1A()    = ELECTRON IMPACT TRANSITION:
C                              LOWER ENERGY LEVEL INDEX
C  INPUT : (I*4)  IE2A()    = ELECTRON IMPACT TRANSITION:
C                              UPPER ENERGY LEVEL INDEX
C  INPUT : (R*8)  AA()      = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C  INPUT : (I*4)  IMETR()   = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  INPUT : (I*4)  IORDR()   = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                             LIST.
C  INPUT : (R*8)  TEVA()    = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (R*8)  DENSA()   = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT : (I*4)  NPL       = NO. OF METASTABLES OF(Z+1) ION ACCESSED
C                                BY EXCITED STATE IONISATION IN COPASE
C                                FILE WITH IONISATION POTENTIALS GIVEN
C                                ON THE FIRST DATA LINE
C  INPUT : (I*4)  NPLR      = NO. OF ACTIVE METASTABLES OF (Z+1) ION
C  INPUT : (I*4)  NPLI      = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C  INPUT : (I*4)  NPL3      = NO. OF ACTIVE METASTABLES OF (Z+1) ION WITH
C                             THREE-BODY RECOMBINATION ON.
C  INPUT   (L*4)  LRSEL     = .TRUE.  => INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  INPUT : (L*4)  LISEL     = .TRUE.  => INCLUDE ELECTRON IMPACT
C                                        IONISATION.
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  INPUT : (L*4)  LHSEL     = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                        NEUTRAL HYDROGREN.
C                           = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                        FROM NEUTRAL HYDROGREN.
C  INPUT : (L*4)  LIOSEL    = .TRUE.  => INCLUDE IONISATION RATES
C                           = .FALSE. => DO NOT INCLUDE IONISATION RATES
C                              FOR RECOM AND  3-BODY
C  INPUT : (L*4)  LPSEL     = .TRUE.  => INCLUDE PROTON COLLISIONS
C                           = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  INPUT : (L*4)  LZSEL     = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                        PLASMA Z EFFECTIVE'ZEFF'.
C                           = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                         WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                             (ONLY USED IF 'LPSEL=.TRUE.')
C  INPUT : (L*4)  LNSEL     = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                         FROM DATAFILE IF AVAILABLE
C                           = .FALSE. => DO NOT INCLUDE PROJECTED
C                                         BUNDLE-N DATA
C  INPUT : (R*8)  WVLS      = SHORT WAVELENGTH LIMIT FOR PEC & SXB (A)
C  INPUT : (R*8)  WVLL      = LONG WAVELENGTH LIMIT FOR PEC & SXB  (A)
C  INPUT : (R*8)  AVLT      = LOWER LIMIT OF A-VALUES FOR PEC & SXB
C
C  INPUT : (I*4)  NTRSEL    = Number of transitions supplied by user
C  INPUT : (I*4)  ITRLOW()  = Lower levels of requested transitions
C  INPUT : (I*4)  ITRUP()   = Upper levels of requested transitions
C
C  INPUT : (R*4) STVR(,,,)  = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                              1st DIMENSION: ORDINARY LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C                              4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVI(,,,)  = ELECTRON IMPACT IONISATION  COEFFICIENTS
C                              1st DIMENSION: ORDINARY LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C                              4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVH(,,,)  =  CHARGE EXCHANGE COEFFICIENTS
C                              1st DIMENSION: ORDINARY LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) RATPIA(,)  = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                              1ST DIMENSION: TEMP/DENS INDEX
C                              2ND DIMENSION: PARENT INDEX
C  INPUT : (R*8) RATMIA(,)  = RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                              1ST DIMENSION: TEMP/DENS INDEX
C                              2ND DIMENSION: PARENT INDEX
C  INPUT : (R*4) STACK(,,,) = POPULATION DEPENDENCE
C                              1st DIMENSION: ORDINARY LEVEL INDEX
C                              2nd DIMENSION: METASTABLE INDEX
C                              3rd DIMENSION: TEMPERATURE INDEX
C                              4th DIMENSION: DENSITY INDEX
C  INPUT : (R*8) FVIONR(,,,)= GEN. COLL. RAD. IONIS. RATE COEFFTS.
C                             1ST DIMENSION: (Z) ION METASTABLE INDEX
C                             2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C  INPUT : (R*8)  SGRDA(,,) = GROUND & METASTABLE IONISATION RATE
C                             COEFFICIENTS  FROM SZD FILES (CM3 SEC-1)
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: (Z) ION METASTABLE INDEX
C                             3RD DIMENSION: (Z+1) ION METASTABLE INDEX
C  INPUT : (L*4) LSSETA(,)  = .TRUE.  - MET. IONIS RATE SET IN B8GETS
C                             .FALSE.- MET. IONIS RATE NOT SET IN B8GETS
C                              1ST DIMENSION: (Z) ION METASTABLE INDEX
C                              2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C  INPUT : (L*4) LSS04A(,)  = .TRUE. => IONIS. RATE SET IN ADF04 FILE:
C                             .FALSE.=> NOT SET IN ADF04 FILE
C                               1ST DIM: LEVEL INDEX
C                               2ND DIM: PARENT METASTABLE INDEX
C
C          (I*4) NOTRN      = PARAMETER = MAXIMUM NUMBER OF TRANSITIONS
C          (I*4) NDPEC      = PARAMETER = MAXIMUM NUMBER OF PECS PER
C                                         METASTABLE FOR OUTPUT
C          (I*4) METCNT     = COUNTER OF PECS FOR EACH METASTABLE
C
C          (I*4) I4UNIT     = FUNCTION (SEE ROUTINE SELECTION BELOW)
C
C          (I*4) I          = GENERAL USE
C          (I*4) IP         = GENERAL USE
C          (I*4) J          = GENERAL USE
C          (I*4) K          = GENERAL USE
C          (I*4) L          = GENERAL USE
C
C          (R*8) DUM1       = GENERAL USE- DUMMY
C          (R*8) DUM2       = GENERAL USE- DUMMY
C          (R*8) DUM3       = GENERAL USE- DUMMY
C
C ROUTINES:
C          -------------------------------------------------------------
C          i4unit     ADAS      Fetch unit number for output of messages
C          b8norm     ADAS      Perform  stage population normalisation
C          b8corp     ADAS      'fixes' low te problem in rec. data of pecs
C          b8winf     ADAS      Determines ionis. source and writes
C                               comment block
C          xxeiam     ADAS      Get 2-character name of element.
C          xxslen     ADAS      Returns lenght of string.
C          xxordr     ADAS      Sorts a real*8 array.
C
C-----------------------------------------------------------------------
C
C NOTES: Based on b8wr11, b8wr12 and hapecf.
C
C
C VERSION  : 1.1
C DATE     : 15-02-2006
C MODIFIED : Martin O'Mullane
C              - First version.
C
C VERSION  : 1.2
C DATE     : 08-03-2006
C MODIFIED : Martin O'Mullane
C              - Make sure that CX pec data which is 0.0 is written 
C                as 1.0E-70 to avoid splining problems when reading
C                adf15 datasets.
C
C VERSION : 1.3                       
C DATE    : 30-07-2009
C MODIFIED: Martin O'Mullane
C           - Increase number of transitions to 100000.
C
C VERSION : 1.4                       
C DATE    : 18-09-2009
C MODIFIED: Martin O'Mullane
C           - Remove b8corp ad-hoc correction - no longer required
C             because of changes in b8splt.
C           - Write 1.0D-74 rather than zero.
C
C VERSION : 1.5                       
C DATE    : 02-05-2012
C MODIFIED: Martin O'Mullane
C           - For un-normalized CX data write the maximum of 1E-74 
C             and the value rather than the minimum.
C
C VERSION : 1.5                       
C DATE    : 12-03-2013
C MODIFIED: Martin O'Mullane
C           - Sort transitions according to power and wavelength. 
C             The same way as adas810 which ranks PECs (and associated SXBs) 
C             by power within the wavelength region rather than cutting  
C             the number of PECs at max_pecout.
C           - The lower and upper wavelenght and A-value selection criteria 
C             are now arrays of size ndwvl.
C           - notrn increased to 350000.
C           - Increase size of filename variables from 80 to 132 characters.
C
C VERSION : 1.6                       
C DATE    : 11-04-2017
C MODIFIED: Martin O'Mullane
C           - Do not write partition vector if the atom is heavier
C             than argon (Z0 GT 18).
C
C VERSION : 1.7               
C DATE    : 24-07-2017
C MODIFIED: Martin O'Mullane
C           - Increase number of transitions (350000->520000).
C
C VERSION : 1.8
C DATE    : 19-09-2017
C MODIFIED: Martin O'Mullane
C           - Increase number of metastables to 13 (up from 4).
C
C VERSION : 1.9
C DATE    : 05-12-2017
C MODIFIED: Matthew Bluteau
C           - Matched setting of idion (dimension of ionization
C             specifiers) to the new metastable dimensions.
C
C VERSION : 1.10
C DATE    : 29-09-2018
C MODIFIED: Martin O'Mullane
C           - Allow user to specify transition indices to be output
C             in the PEC file.
C
C-----------------------------------------------------------------------
      integer   notrn    , ndpec
      integer   ntdim    , nddim   ,   nmdim  , ndcnct  , ndstack
      integer   ndstore  , max_pecout         , ndwvl
      integer   idcmt    , idfld   , idsyn    , idion   , idopt
C-----------------------------------------------------------------------
      parameter ( notrn = 520000, ndpec = 20000 )
      parameter ( ntdim = 35    , nddim = 24   )
      parameter ( nmdim = 13    , ndwvl = 5    )
      parameter ( max_pecout = 100 )
      parameter ( ndstore = 3*nmdim*ndpec )
      parameter ( ndcnct = 100    , ndstack = 40 )
      parameter ( idcmt = 500     , idfld  = 30  , idsyn = 1 )
      parameter ( idion = nmdim*nmdim            , idopt  = 10 )
C-----------------------------------------------------------------------
      integer   i4unit
      integer   ndlev     , ndtem      , ndden   , ndmet   , ndtrn  ,
     &          ndsel
      integer   iunt15    , iunt13     ,
     &          npl       , nplr       , npli    , npl3    ,
     &          iz        , iz0        , iz1     ,
     &          il        , nmet       , nord    ,
     &          maxt      , maxd       , icnte   , icntr   , icnti  ,
     &          icnth
      integer   i         , j          , k       , l       , ii     ,
     &          isel      , iulev      , kstrn   , io      ,
     &          isel1     , isel2      , isel3   , isel4   ,
     &          isel1_out ,
     &          index1    , index2     , metcnt  , len_cterm
      integer   nblocks   , isel_r     , isel_h  , isel_i
      integer   im        , ip
      integer   iw        , id         , ifail   ,
     &          ita_max   , isa_max    , ila_max , iptnl
      integer   nfld_15   , nion       , nopt    , nspb    , nspp
      integer   iwvrg     , nwvl       , ncnct   , ncptn_stack      ,
     &          ntrsel
C-----------------------------------------------------------------------
      real*8    wvl
      real*8    dum1      , dum2       , dum3
      real*8    sum1      , srate
      real*8    amssno    , de
      real*8    pecpow    , xja_max
C-----------------------------------------------------------------------
      character titled*3        , dsninc*132    , filmem*8 , dsfull*132
      character date*8          , dsnexp*132    , trans*29
      character metas*12        , user*30
      character film15_exc*23   , film15_rec*23
      character fmt01*9         , fmt02*63      , fmt03*31
      character popcode*7       , esym*2        , code*7   , 
     &          producer*30     , dsnpt*120     , flexp*132
      character tabul_15*60     , units_15*60
C-----------------------------------------------------------------------
      logical   lnorm
      logical   lrsel  , lisel  , lhsel  , liosel
      logical   lpsel  , lzsel  , lnsel
      logical   lup
      logical   ladf10
      logical   open15 , open13
      logical   lroot  , lsuper , lwvrg
C-----------------------------------------------------------------------
      integer   isa(ndlev)       , ila(ndlev)
      integer   imetr(ndmet)     , iordr(ndlev)
      integer   ie1a(ndtrn)      , ie2a(ndtrn)
      integer   ima(notrn)       , nma(notrn)
      integer   indx_peca(notrn) , inde_peca(notrn)  , invdx_peca(notrn)
      integer   isyn_15(idfld)
      integer   ibsela(ndmet,ndmet)
      integer   itg(ndstore)     , ipr(ndstore)      , iwr(ndstore)
      integer   ispbr(ndstore)   , isppr(ndstore)    , iszr(ndstore)
      integer   icnctv(ndcnct)
      integer   itrlow(ndsel)    , itrup(ndsel)      ,
     &          ilower(max_pecout)                   ,
     &          iupper(max_pecout)
C-----------------------------------------------------------------------
      real*8    avlt(ndwvl)       , wvls(ndwvl)      , wvll(ndwvl)
      real*8    pec(max_pecout)   , sum(max_pecout)  , sxba(max_pecout)
      real*8    xja(ndlev)        , teva(ndtem)      , densa(ndden)
      real*8    wa(ndlev)         , aa(ndtrn)        , wvla(notrn)
      real*8    peca_mx(notrn)
      real*8    sgrda(ndtem,ndmet,ndmet)
      real*8    fvionr(ndmet,ndmet,ndtem,ndden)
      real*8    ratpia(ndden,ndmet)  , ratmia(ndden,ndmet)
      real*8    wvl_sort(max_pecout) , wtrans(ndstore)
C-----------------------------------------------------------------------
      real*4    stvr(ndlev,ndtem,ndden,ndmet) ,
     &          stvi(ndlev,ndtem,ndden,ndmet) ,
     &          stvh(ndlev,ndtem,ndden,ndmet)
      real*4    stack(ndlev,ndmet,ndtem,ndden)
C-----------------------------------------------------------------------
      character cstrga(ndlev)*18     , transa(notrn)*29 ,
     &          metasa(10)*12        , type(4)*5        , 
     &          cterm(ndlev)*14      , config(ndlev)*19
      character cion(idion)*5        , copt(idopt)*6
      character cwvla(ndpec)*10      , ctrans(max_pecout)*35
      character cptn_stack(ndstack)*80
      character ctype(ndstore)*5
      character fldk_15(idfld,idsyn)*40
C-----------------------------------------------------------------------
      logical   lsseta(ndmet,ndmet)  , lss04a(ndlev,ndmet)
      logical   lfld_15(idfld)
      logical   lion(idion)          , lopt(idopt)
C-----------------------------------------------------------------------
      data      type/'EXCIT','RECOM','CHEXC','IONIS'/
      data      film15_exc/'pl=0*:ss=**:pb=**:sz=**'/
      data      film15_rec/'pl=0*:ss=**:pp=**:sz=**'/
      data      ladf10/.FALSE./
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Check external dimensions against internal dimension parameters
C-----------------------------------------------------------------------

      if((ndtem.ne.ntdim).or.(ndden.ne.nddim).or.
     &   (ndtrn.ne.notrn).or.(ndmet.ne.nmdim))    then
         write(i4unit(-1),2011)
         write(i4unit(-1),2013)
         write(i4unit(-1),*)ndtem,ndden,ndtrn,ndmet
         write(i4unit(-1),*)ntdim,nddim,notrn,nmdim
         stop
      endif

      if(max_pecout.ne.ndsel) then
         write(i4unit(-1),2011)
         write(i4unit(-1),2013)
         write(i4unit(-1),*)'max_pecout and ndsel', max_pecout, ndsel
         stop
      endif

c-----------------------------------------------------------------------
c  field key declarations
c-----------------------------------------------------------------------

       nfld_15 = 23

       do i=1,nfld_15
         isyn_15(i) = 1
         lfld_15(i) = .true.
       enddo
       lfld_15(12) = .FALSE.
       lfld_15(14) = .FALSE.
       lfld_15(15) = .FALSE.

       fldk_15(1,1)  = 'root partition information              '
       fldk_15(2,1)  = 'nuclear charge                          '
       fldk_15(3,1)  = 'ion charge+1                            '
       fldk_15(4,1)  = 'population calculation information      '
       fldk_15(5,1)  = 'population processing code              '
       fldk_15(6,1)  = 'adf04 source file                       '
       fldk_15(7,1)  = 'adf18/a17_p208 file                     '
       fldk_15(8,1)  =  'meta. ion. coeff. selector             '
       fldk_15(9,1)  = 'options                                 '
       fldk_15(10,1) = 'energy levels                           '
       fldk_15(11,1) = 'lv      configuration                   '
       fldk_15(12,1) = 'superstage partition information        '
       fldk_15(13,1) = 'element symbol                          '
       fldk_15(14,1) = 'parent template                         '
       fldk_15(15,1) = 'partition level                         '
       fldk_15(16,1) = 'charge state                            '
       fldk_15(17,1) = 'tabulation                              '
       fldk_15(18,1) = 'units                                   '
       fldk_15(19,1) = 'photon emissivity atomic transitions    '
       fldk_15(20,1) = 'isel  wvle                              '
       fldk_15(21,1) = 'CODE                                    '
       fldk_15(22,1) = 'PRODUCER                                '
       fldk_15(23,1) = 'DATE                                    '

       lroot  = .TRUE.
       lsuper = .TRUE.

       nopt = 8

       copt(1) = 'lnorm '
       copt(2) = 'lpsel '
       copt(3) = 'lzsel '
       copt(4) = 'liosel'
       copt(5) = 'lhsel '
       copt(6) = 'lrsel '
       copt(7) = 'lisel '
       copt(8) = 'lnsel '

       lopt(1) = lnorm
       lopt(2) = lpsel
       lopt(3) = lzsel
       lopt(4) = liosel
       lopt(5) = lhsel
       lopt(6) = lrsel
       lopt(7) = lisel
       lopt(8) = lnsel

       if(nmet.gt.1) then
           iptnl = 0
           nspb  = nmet
           nspp  = nplr
       else
           iptnl = 1
           nspb  = 1
           nspp  = nplr
       endif

       write(film15_exc(5:5),'(i1)')iptnl
       write(film15_exc(10:11),'(i2)')iz
       write(film15_exc(22:23),'(i2)')iz
       write(film15_rec(5:5),'(i1)')iptnl
       write(film15_rec(10:11),'(i2)')iz
       write(film15_rec(22:23),'(i2)')iz

       nion = 0
       do i=1,nmet
         do ip =1,npl
           nion=nion+1
           write(cion(nion),'(a1,i1,a1,i1,a1)')'(',i,',',ip,')'
           lion(nion)=lss04a(i,ip)
         enddo
       enddo

       esym=titled(1:2)
       popcode  = 'ADAS208'
       producer = user(1:20)
       code     = 'adas208'
       dsnpt=' '

       tabul_15 = ' '
       tabul_15(1:32) = 'photon emissivity coefft (te,ne)'
       units_15=' '
       units_15(1:45) = 'ph. emis coef(cm^3 s^-1); te (ev); ne (cm^-3)'

       if (lnsel) then
          flexp = dsnexp
       else
          flexp = 'No projection data included'
       endif

C-----------------------------------------------------------------------
C Get element mass number & setup adf04 file member name for header
C-----------------------------------------------------------------------

      call xxeiam(titled(1:2),amssno)

      call xxslen(dsninc,index1,index2)
      if((index2-index1).gt.4) then
          if(dsninc(index2-3:index2).eq.'.dat') then
              index1=max(index2-11,1)
              index2=index2-4
          else
              index1=max(index2-7,1)
          endif
          index1=index1+index(dsninc(index1:index2),'/')
      endif

c------ initialise term parameter maxima

      isa_max=0
      ila_max=0
      xja_max=0.0d0

c-------and identify maximum values for term parameters

      do i = 1, il
        isa_max=max0(isa_max,isa(i))
        ila_max=max0(ila_max,ila(i))
        xja_max=dmax1(xja_max,xja(i))
        config(i)=cstrga(i)//' '
      enddo

c------make the term string format to fit

       call xxfrmt_trm(isa_max,ila_max,xja_max,iw,fmt03)
       len_cterm=len(cterm(1))
       if(iw.gt.len_cterm)then
           fmt03="('  (',i1,')',i1,'(',f5.1,') ')"
           write(i4unit(-1),3002)iw
       endif

       do i=1,il
         cterm(i)=' '
         write(cterm(i)(len_cterm-iw+1:len_cterm),fmt03)
     &                  isa(i),ila(i),xja(i)
       enddo

      do i = 1 , nmet
          write(metas,1007)imetr(i),isa(imetr(i)),ila(imetr(i)),
     &                     xja(imetr(i))
          metasa(i)=metas
      end do



C-----------------------------------------------------------------------
C Start processing the pecs
C-----------------------------------------------------------------------

      isel   = 0

      do 19 i = 1 , nmet

        metcnt = 0
          do 18 j = 1 , icnte

C-----------------------------------------------------------------------
C  Limit pecs by A-value and wavelength - check only for 1st metastable
C-----------------------------------------------------------------------
            if(i.eq.1) then

                 iulev = ie2a(j)
                  do 16 io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                        de    = wa(ie2a(j))-wa(ie1a(j))
                        wvl   = 1.0D8/de

                        call b8wvrg( ndwvl  ,
     &                               nwvl   , wvls   , wvll   , avlt ,
     &                               wvl    , aa(j)  ,
     &                               lwvrg  , iwvrg
     &                             )
                        call b8wvtr( ndsel   ,
     &                               ntrsel  , itrlow  , itrup ,
     &                               ie1a(j) , ie2a(j) ,
     &                               lwvrg
     &                             )

                        if (.not.lwvrg) goto 16

                        metcnt = metcnt+1
                        peca_mx(metcnt)=0.0D0
                        inde_peca(metcnt)=j
                        wvla(metcnt)=wvl
                        do 14 k = 1 , maxd
                          do 13 l=1,maxt
                            pec(l) = aa(j)*stack(kstrn,i,l,k)/densa(k)
                            if(lnorm.and.(nmet.eq.1))then
                               call b8norm( ndlev , ndmet ,
     &                                      nord  ,
     &                                      stack(1,1,l,k),
     &                                      pec(l), dum1  ,
     &                                      dum2  , dum3
     &                                    )
                            endif
                            if(pec(l).le.1.0D-74) pec(l)=1.0D-74
c-----------------------------------------------------------------------
c  revise pec power definition to use sum of metastable contributions
c-----------------------------------------------------------------------
                            if(k.eq.1) then
                              pecpow=0.0d0
                              do ii=1,nmet
                                pecpow=pecpow+stack(kstrn,ii,l,k)
                              enddo
                              pecpow=pecpow*aa(j)/densa(k)
                              peca_mx(metcnt)=dmax1(peca_mx(metcnt),
     &                                              pecpow)
                            endif
   13                       continue
   14                     continue
                        go to 17
                    endif
   16             continue
            else
              if(inde_peca(metcnt+1).eq.j)then
                  iulev = ie2a(j)
                  do io = 1 , nord
                    if ( iulev .eq. iordr(io) ) then
                        kstrn = io
                    endif
                  enddo
                  metcnt=metcnt+1
              endif
            endif
c------------limited allocation to pec stack but warn if exceeded
   17     if(metcnt.ge.ndpec) then
             write(i4unit(-1),3001)ndpec,  ndpec*(icnte/metcnt)
             go to 60
          endif

   18     continue

   60     continue

C-----------------------------------------------------------------------
C Based on the first metastable - order the pec list, strongest first,
C restrict number to max_pecout.
C
C For user supplied transition indices sort on ind_low + ind_upper*1000
C
C We may have no valid data - if so return from routine.
C-----------------------------------------------------------------------


        if (i.eq.1) then

            if (ntrsel.GT.0) then
            
               isel1_out = metcnt
               do ii = 1, metcnt
                  j             = inde_peca(ii)
                  indx_peca(ii) = ii
                  wvl_sort(ii)  = wvla(ii)
                  iupper(ii)    = ie2a(j)
                  ilower(ii)    = ie1a(j)
               end do
            
               call b8trord( ndsel  ,
     &                       ntrsel , itrlow , itrup ,
     &                       max_pecout      ,
     &                       metcnt , ilower , iupper,
     &                       ipr
     &                     )

               do ii = 1, metcnt
                  wvl_sort(ii) = wvla(ipr(ii))
               end do
            
            else

               lup = .false.
               
               if (metcnt.GT.0) then
                  call xxordr(metcnt, lup, peca_mx, 
     &                        indx_peca, invdx_peca)
                  isel1_out=min(metcnt,max_pecout)
               else
                  write(i4unit(-1),3000)
                  goto 19
               endif

c------        now sort restricted list by wavelength

               do ii=1,isel1_out
                 ipr(ii)=ii
                 wvl_sort(ii)=wvla(indx_peca(ii))
               enddo

               lup=.true.

               call xxr8sort(isel1_out,lup,wvl_sort,ipr)
               
            endif

c------ set pointers and initialise transition parameter maxima

            ita_max=0
            isa_max=0
            ila_max=0
            xja_max=0.0d0

            do ii=1,isel1_out

c-------and make the wavelength string for the output set

              wvl=wvla(indx_peca(ipr(ii)))
              wtrans(ii)=wvl
              iw=8
              call xxdeci(wvl,iw,id,ifail)
              if((ifail.eq.0).and.(id.lt.10)) then
                 fmt01='(f9.?,1x)'
                 write(fmt01(5:5),'(i1)')id
                 write(cwvla(ii),fmt01)wvl
              else
                 cwvla(ii)=' ******** '
              endif

c-------and identify maximum values for transition parameters

              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do io = 1, nord
                if ( iulev .eq. iordr(io) ) then
                   kstrn = io
                   ita_max=max0(ita_max,ie2a(j))
                   ita_max=max0(ita_max,ie1a(j))
                   isa_max=max0(isa_max,isa(ie2a(j)))
                   isa_max=max0(isa_max,isa(ie1a(j)))
                   ila_max=max0(ila_max,ila(ie2a(j)))
                   ila_max=max0(ila_max,ila(ie1a(j)))
                   xja_max=dmax1(xja_max,xja(ie2a(j)))
                   xja_max=dmax1(xja_max,xja(ie1a(j)))
                endif
              enddo
            enddo

c------make the transition string format to fit

            call xxfrmt_t(ita_max,isa_max,ila_max,xja_max,iw,fmt02)
            if(iw.gt.len(ctrans(1)))then
                fmt02="(i3,'(',i1,')',i1,'(',f5.1,')-',i1,'(',i1,')',i1,
     &'(',f5.1,')')"
                write(i4unit(-1),3002)iw
            endif

            do ii=1,isel1_out

c-------and make the transition string for the output set

              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do io = 1, nord
                if ( iulev .eq. iordr(io) ) then
                    write(ctrans(ii),fmt02)ie2a(j),isa(ie2a(j)),
     &                                     ila(ie2a(j)),xja(ie2a(j)),
     &                                     ie1a(j),isa(ie1a(j)),
     &                                     ila(ie1a(j)),xja(ie1a(j))
                endif
              enddo
            enddo

        endif

C-----------------------------------------------------------------------
C Initialise iunt15 for pec output and set headers when i=1.
C Output strongest pec set directly to file for each metastable.
C Also need to determine how many recombination, CX and ionisation
C pecs are required.
C-----------------------------------------------------------------------

      if (i.eq.1) then

         isel_r = 0
         isel_h = 0
         isel_i = 0
         if (liosel .and. lrsel) then
            if(lnorm.and.(nmet.eq.1)) then
               isel_r = isel1_out
            else
               isel_r = isel1_out * npl3
            endif
         elseif((.not.liosel).and.lrsel.and.(icntr.gt.0))then
            if(lnorm.and.(nmet.eq.1))then
               isel_r = isel1_out
            else
               isel_r = isel1_out * nplr
            endif
         endif
         if(lhsel.and.(icnth.gt.0))then
            if(lnorm.and.(nmet.eq.1))then
               isel_h = isel1_out
            else
               isel_h = isel1_out * nplr
            endif
         endif
         if(lisel.and.(icnti.gt.0))then
            if(lnorm.and.(nmet.eq.1))then
               isel_i = isel1_out
            else
               isel_i = isel1_out * npli
            endif
         endif

         nblocks =  isel1_out*nmet + isel_r + isel_h +isel_i

      endif

c-----------------------------------------------------------------------
c top line and partition vector - nb we are within the isel loop
c-----------------------------------------------------------------------

      if (open15.and.i.eq.1) then
      
         write(iunt15,2003) nblocks, titled , iz
         
         if (iz0.LE.18) then
         
            call xxmkrc( ndcnct   ,
     &                   iz0      , iptnl    ,
     &                   ncnct    , icnctv
     &                 )

            write(iunt15,'(80(1H-))')
            write(iunt15,'(14i5)')(icnctv(ii),ii=1,ncnct)

            call xxmkrp( ndstack       ,
     &                   iz0           , iptnl       ,
     &                   ncptn_stack   , cptn_stack
     &                 )

            write(iunt15,'(80(1H-))')
            do ii=1,ncptn_stack
              write(iunt15,'(1a80)') cptn_stack(ii)
            enddo
            write(iunt15,'(80(1H-))')
         
         endif
         
      endif
      
c-----------------------------------------------------------------------
c PECs of different types
c-----------------------------------------------------------------------

      do ii=1,isel1_out
c        j=inde_peca(indx_peca(ii))
        j=inde_peca(indx_peca(ipr(ii)))

        iulev = ie2a(j)
        do io = 1, nord
          if ( iulev .eq. iordr(io) ) then
              kstrn = io
          endif
        enddo

        isel = isel + 1
        ima(isel) = i
        
        ispbr(isel) = i
        isppr(isel) = 0
        iszr(isel) = iz
        itg(isel)=j
        iwr(isel)=ii
        ipr(isel)=ipr(ii)
        ctype(isel)=type(1)
        wtrans(isel)=wvl_sort(ii)

        wvl  = 1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
        wvla(isel)=wvl

        if(lnorm.and.(nmet.eq.1))then
            write(film15_exc(16:17),'(1x,1a1)')'1'
            if (open15) write(iunt15,1001) cwvla(ii) , maxd , maxt ,
     &                         film15_exc , type(1) ,'ispb','t', isel
        else
            write(film15_exc(16:17),'(i2)')i
            if (open15) write(iunt15,1000) cwvla(ii) , maxd , maxt ,
     &                         film15_exc , type(1) ,'ispb', i , isel
        endif
        if (open15) write(iunt15,1002) (densa(k),k=1,maxd)
        if (open15) write(iunt15,1002) (teva(k),k=1,maxt)
        do k = 1 , maxd
           do l=1,maxt
              pec(l) = aa(j)*stack(kstrn,i,l,k)/densa(k)
              if(lnorm.and.(nmet.eq.1))then
                  call b8norm( ndlev , ndmet ,
     &                         nord  ,
     &                         stack(1,1,l,k),
     &                         pec(l), dum1  ,
     &                         dum2  , dum3
     &                        )
              endif
              if(pec(l).le.1.0D-74) pec(l)=1.0D-74
           enddo
           if (open15) write(iunt15,1002)(pec(l),l=1,maxt)
        enddo
      enddo
   19 continue
      isel1=isel

C-----------------------------------------------------------------------
C  Only write RECOM block if lrsel is selected (liosel turns on 3-body
C  part which can generate recombination data).
C-----------------------------------------------------------------------

      if (liosel .and. lrsel .and. open15) then

        if (lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 25 ii=1,isel1_out
              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do 24 io = 1 , nord
                if ( iulev .eq. iordr(io) ) then
                    kstrn = io
                    wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                    call b8wvrg( ndwvl  ,
     &                           nwvl   , wvls   , wvll   , avlt ,
     &                           wvl    , aa(j)  ,
     &                           lwvrg  , iwvrg
     &                         )
                     call b8wvtr( ndsel   ,
     &                            ntrsel  , itrlow  , itrup ,
     &                            ie1a(j) , ie2a(j) ,
     &                            lwvrg
     &               )
     
                    if (.not.lwvrg) goto 25
                    
                    metcnt = metcnt+1
                    isel = isel + 1
                    wvla(isel)=wvl
                    ima(isel) = 0
                    write(film15_rec(16:17),'(1x,1a1)')'1'
                    if (open15) write(iunt15,1001) cwvla(ii) , 
     &                     maxd , maxt ,
     &                     film15_rec , type(2) ,'ispp','t', isel
                    if (open15)write(iunt15,1002)(densa(k),k=1,maxd)
                    if (open15)write(iunt15,1002)(teva(k),k=1,maxt)
                    do 23 k = 1 , maxd
                      sum1=0.0
                      do  i=1,npl3
                        sum1=sum1+ratpia(k,i)
                      end do
                      if(sum1.le.0.0D0) then
                          write(i4unit(-1),2012)
     &                    'Parent population sum .le. 0'
                          write(i4unit(-1),2013)
                          stop
                      endif
                      do l=1,maxt
                        sum(l)=0.0D0
                        do  i=1,npl3
                          sum(l)=sum(l)+aa(j)*stvr(kstrn,l,k,i)*
     &                                  ratpia(k,i)
                        end do
                        sum(l)=sum(l)/sum1
                        if(sum(l).le.1.0D-74) sum(l)=1.0D-74
                      end do
                      if (open15)write(iunt15,1002)(sum(l),l=1,maxt)
                      wtrans(isel) = wvl
                      ctype(isel)  = type(2)              
                      ispbr(isel)  = 0
                      isppr(isel)  = 1
                      iszr(isel)   = iz
                      itg(isel)    = j
                      iwr(isel)    = ii
                      ipr(isel)    = ipr(ii)
   23               continue
                    go to 25
                endif
   24         continue
          if (metcnt.ge.ndpec) go to 130
   25     continue

        else

            do 29 i = 1 , npl3
             metcnt=0
             do 28 ii=1,isel1_out
              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do 27 io = 1 , nord
                if ( iulev .eq. iordr(io) ) then
                   kstrn = io
                   wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                   call b8wvrg( ndwvl  ,
     &                          nwvl   , wvls   , wvll   , avlt ,
     &                          wvl    , aa(j)  ,
     &                          lwvrg  , iwvrg
     &                        )
                   call b8wvtr( ndsel   ,
     &                          ntrsel  , itrlow  , itrup ,
     &                          ie1a(j) , ie2a(j) ,
     &                          lwvrg
     &                        )
     
                   if (.not.lwvrg) goto 28
                   
                   metcnt = metcnt+1
                   isel = isel + 1
                   wvla(isel)=wvl
                   ima(isel) = i
                   write(film15_rec(16:17),'(i2)')i
                   if (open15) write(iunt15,1000) cwvla(ii) , 
     &                    maxd , maxt ,
     &                    film15_rec , type(2) ,'ispb', i , isel
                   if (open15) write(iunt15,1002)(densa(k),k=1,maxd)
                   if (open15) write(iunt15,1002)(teva(k),k=1,maxt)
                   do 26 k = 1 , maxd
                     if (open15) write(iunt15,1002)
     &                      (max(aa(j)*stvr(kstrn,l,k,i), 1.0D-74)
     &                                 , l=1,maxt)
   26              continue
                   wtrans(isel) = wvl
                   ctype(isel)  = type(2)              
                   ispbr(isel)  = 0
                   isppr(isel)  = i
                   iszr(isel)   = iz
                   itg(isel)    = j
                   iwr(isel)    = ii
                   ipr(isel)    = ipr(ii)
                   go to 28
                endif
   27         continue
              if(metcnt.ge.ndpec) go to 29
   28        continue
   29       continue
        endif
 130    continue

      elseif((.not.liosel).and.lrsel.and.(icntr.gt.0).and.open15) then

        if(lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 35 ii=1,isel1_out
              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do 34 io = 1 , nord
                if ( iulev .eq. iordr(io) ) then
                    kstrn = io
                    wvl   = 1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                    call b8wvrg( ndwvl  ,
     &                           nwvl   , wvls   , wvll   , avlt ,
     &                           wvl    , aa(j)  ,
     &                           lwvrg  , iwvrg
     &                         )
                    call b8wvtr( ndsel   ,
     &                           ntrsel  , itrlow  , itrup ,
     &                           ie1a(j) , ie2a(j) ,
     &                           lwvrg
     &                         )
     
                    if (.not.lwvrg) goto 35
                    
                    metcnt = metcnt+1
                    isel = isel + 1
                    wvla(isel)=wvl
                    ima(isel) = 0
                    write(film15_rec(16:17),'(1x,1a1)')'1'
                    if (open15) write(iunt15,1001) cwvla(ii) , 
     &                     maxd , maxt ,
     &                     film15_rec , type(2) ,'ispp','t', isel
                    if (open15)write(iunt15,1002)(densa(k),k=1,maxd)
                    if (open15)write(iunt15,1002)(teva(k),k=1,maxt)
                    do 33 k = 1 , maxd
                      sum1=0.0
                      do 30 i=1,nplr
                       sum1=sum1+ratpia(k,i)
   30                 continue
                      if(sum1.le.0.0D0) then
                          write(i4unit(-1),2012)
     &                          'Parent population sum .le. 0'
                          write(i4unit(-1),2013)
                          stop
                      endif
                      do 32 l=1,maxt
                        sum(l)=0.0D0
                        do 31 i=1,nplr
                         sum(l)=sum(l)+aa(j)*stvr(kstrn,l,k,i)*
     &                                 ratpia(k,i)
   31                   continue
                        sum(l)=sum(l)/sum1
                        if(sum(l).le.1.0D-74) sum(l)=1.0D-74
   32                 continue
                      if (open15)write(iunt15,1002)(sum(l),l=1,maxt)
                      wtrans(isel) = wvl
                      ctype(isel)  = type(2)              
                      ispbr(isel)  = 0
                      isppr(isel)  = 1
                      iszr(isel)   = iz
                      itg(isel)    = j
                      iwr(isel)    = ii
                      ipr(isel)    = ipr(ii)
   33               continue
                    go to 35
                endif
   34         continue
              if (metcnt.ge.ndpec) go to 140
   35     continue

        else

            do 39 i = 1 , nplr
             metcnt = 0
             do 38 ii=1,isel1_out
              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do 37 io = 1 , nord
                if ( iulev .eq. iordr(io) ) then
                    kstrn = io
                    wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                    call b8wvrg( ndwvl  ,
     &                           nwvl   , wvls   , wvll   , avlt ,
     &                           wvl    , aa(j)  ,
     &                           lwvrg  , iwvrg
     &                         )
                    call b8wvtr( ndsel   ,
     &                           ntrsel  , itrlow  , itrup ,
     &                           ie1a(j) , ie2a(j) ,
     &                           lwvrg
     &                         )
     
                    if (.not.lwvrg) goto 38
                    
                    metcnt = metcnt+1
                    isel = isel + 1
                    wvla(isel)=wvl
                    ima(isel) = i
                    write(film15_rec(16:17),'(i2)')i
                    if (open15) write(iunt15,1000) cwvla(ii) ,
     &                     maxd , maxt ,
     &                     film15_rec , type(2) ,'ispb', i , isel
                    if (open15)write(iunt15,1002)(densa(k),k=1,maxd)
                    if (open15)write(iunt15,1002)(teva(k),k=1,maxt)
                    do 36 k = 1 , maxd
                       if (open15)write(iunt15,1002)
     &                        (max(aa(j)*stvr(kstrn,l,k,i), 1.0D-74)
     &                                   , l=1,maxt)
   36               continue
                    wtrans(isel) = wvl
                    ctype(isel)  = type(2)
                    ispbr(isel)  = 0
                    isppr(isel)  = i
                    iszr(isel)   = iz
                    itg(isel)    = j
                    iwr(isel)    = ii
                    ipr(isel)    = ipr(ii)
                    go to 38
                endif
   37         continue
              if(metcnt.ge.ndpec) go to 39
   38        continue
   39       continue
        endif

      endif
 140  isel2=isel

C-----------------------------------------------------------------------

      if(lhsel.and.(icnth.gt.0) .and. open15) then

        if(lnorm.and.(nmet.eq.1))then
           metcnt = 0
           do 45 ii=1,isel1_out
              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do 44 io = 1 , nord
                if ( iulev .eq. iordr(io) ) then
                    kstrn = io
                    wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                    call b8wvrg( ndwvl  ,
     &                           nwvl   , wvls   , wvll   , avlt ,
     &                           wvl    , aa(j)  ,
     &                           lwvrg  , iwvrg
     &                         )
                    call b8wvtr( ndsel   ,
     &                           ntrsel  , itrlow  , itrup ,
     &                           ie1a(j) , ie2a(j) ,
     &                           lwvrg
     &                         )
     
                    if (.not.lwvrg) goto 45
                    
                    metcnt = metcnt+1
                    isel = isel + 1
                    wvla(isel)=wvl
                    ima(isel) = 0
                    write(film15_rec(16:17),'(1x,1a1)')'1'
                    if (open15) write(iunt15,1001) cwvla(ii) , 
     &                     maxd , maxt ,
     &                     film15_rec , type(3) ,'ispp','t', isel
                    if (open15)write(iunt15,1002)(densa(k),k=1,maxd)
                    if (open15)write(iunt15,1002)(teva(k),k=1,maxt)
                    do 43 k = 1 , maxd
                     sum1=0.0
                     do 40 i=1,nplr
                      sum1=sum1+ratpia(k,i)
   40                continue
                     if(sum1.le.0.0D0) then
                         write(i4unit(-1),2012)
     &                         'Parent population sum .le.0'
                         write(i4unit(-1),2013)
                         stop
                     endif
                     do 42 l=1,maxt
                      sum(l)=0.0D0
                      do 41 i=1,nplr
                       sum(l)=sum(l)+aa(j)*stvh(kstrn,l,k,i)*
     &                               ratpia(k,i)
   41                 continue
                       sum(l)=sum(l)/sum1
                       if(sum(l).le.1.0D-70) sum(l)=1.0D-70
   42                continue
                     if (open15)write(iunt15,1002)(sum(l),l=1,maxt)
                     wtrans(isel) = wvl
                     ctype(isel)  = type(3)              
                     ispbr(isel)  = 0
                     isppr(isel)  = 1
                     iszr(isel)   = iz
                     itg(isel)    = j
                     iwr(isel)    = ii
                     ipr(isel)    = ipr(ii)
   43               continue
                    go to 45
                endif
   44         continue
              if (metcnt.ge.ndpec) go to 150
   45     continue

        else

           do 49 i = 1 , nplr
             metcnt = 0
             do 48 ii=1,isel1_out
              j=inde_peca(indx_peca(ipr(ii)))
              iulev = ie2a(j)
              do 47 io = 1 , nord
                if ( iulev .eq. iordr(io) ) then
                    kstrn = io
                    wvl   = 1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
                    call b8wvrg( ndwvl  ,
     &                           nwvl   , wvls   , wvll   , avlt ,
     &                           wvl    , aa(j)  ,
     &                           lwvrg  , iwvrg
     &                         )
                    call b8wvtr( ndsel   ,
     &                           ntrsel  , itrlow  , itrup ,
     &                           ie1a(j) , ie2a(j) ,
     &                           lwvrg
     &                         )
     
                    if (.not.lwvrg) goto 48
                    
                    metcnt = metcnt+1
                    isel = isel + 1
                    wvla(isel)=wvl
                    ima(isel) = i
                    write(film15_rec(16:17),'(i2)')i
                    if (open15) write(iunt15,1000) cwvla(ii) , 
     &                    maxd , maxt ,
     &                    film15_rec , type(3) ,'ispb', i , isel
                    if (open15)write(iunt15,1002)(densa(k),k=1,maxd)
                    if (open15)write(iunt15,1002)(teva(k),k=1,maxt)
                    do 46 k = 1 , maxd
                      if (open15) write(iunt15,1002)
     &                     (max(1.0D-70, aa(j)*stvh(kstrn,l,k,i))
     &                                 , l=1,maxt)
   46               continue
                    wtrans(isel) = wvl
                    ctype(isel)  = type(3)             
                    ispbr(isel)  = 0
                    isppr(isel)  = i
                    iszr(isel)   = iz
                    itg(isel)    = j
                    iwr(isel)    = ii
                    ipr(isel)    = ipr(ii)
                    go to 48
                endif
   47         continue
              if (metcnt.ge.ndpec) go to 49
   48        continue
   49       continue
        endif

      endif
 150  isel3=isel

C-----------------------------------------------------------------------
C       if(lisel.and.(icnti.gt.0) .and. open15)then
C 
C         if(lnorm.and.(nmet.eq.1))then
C            metcnt = 0
C            do 55 ii=1,isel1_out
C               j=inde_peca(indx_peca(ii))
C               if ( aa(j) .gt. avlt ) then
C                   iulev = ie2a(j)
C                   do 54 io = 1 , nord
C                     if ( iulev .eq. iordr(io) ) then
C                         kstrn = io
C                         wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
C                         if (wvl.LT.wvls.OR.wvl.GT.wvll) goto 55
C                         metcnt = metcnt+1
C                         isel = isel + 1
C                         wvla(isel)=wvl
C                         ima(isel) = 0
C                         write(trans,1006)ie2a(j),isa(ie2a(j)),
C      &                               ila(ie2a(j)),
C      &                               xja(ie2a(j)),ie1a(j),isa(ie1a(j)),
C      &                               ila(ie1a(j)),xja(ie1a(j))
C                         transa(isel)=trans
C                         write(iunt15,1014) wvla(isel) , maxd , maxt ,
C      &                         filmem , type(4) ,'T', isel
C                         write(iunt15,1001) (densa(k),k=1,maxd)
C                         write(iunt15,1001) (teva(k),k=1,maxt)
C                         do 53 k = 1 , maxd
C                          sum1=0.0
C                          do 50 i=1,npli
C                           sum1=sum1+ratmia(k,i)
C    50                    continue
C                          if(sum1.le.0.0D0) then
C                              write(i4unit(-1),2012)
C      &                             'Parent population sum .le.0'
C                              write(i4unit(-1),2013)
C                              stop
C                          endif
C                          do 52 l=1,maxt
C                           sum(l)=0.0D0
C                           do 51 i=1,npli
C                            sum(l)=sum(l)+aa(j)*stvi(kstrn,l,k,i)*
C      &                                   ratmia(k,i)
C    51                     continue
C                            sum(l)=sum(l)/sum1
C    52                    continue
C                          write(iunt15,1001)(sum(l),l=1,maxt)
C    53                   continue
C                         go to 55
C                     endif
C    54             continue
C               endif
C               if (metcnt.ge.ndpec) go to 160
C    55     continue
C 
C         else
C 
C             do 59 i = 1 , npli
C              metcnt = 0
C              do 58 ii=1,isel1_out
C               j=inde_peca(indx_peca(ii))
C               if ( aa(j) .gt. avlt ) then
C                   iulev = ie2a(j)
C                   do 57 io = 1 , nord
C                     if ( iulev .eq. iordr(io) ) then
C                         kstrn = io
C                         wvl   =1.0D8/(wa(ie2a(j))-wa(ie1a(j)))
C                         if (wvl.LT.wvls.OR.wvl.GT.wvll) goto 58
C                         metcnt = metcnt+1
C                         isel = isel + 1
C                         wvla(isel)=wvl
C                         ima(isel) = i
C                         write(trans,1006)ie2a(j),isa(ie2a(j)),
C      &                               ila(ie2a(j)),
C      &                               xja(ie2a(j)),ie1a(j),isa(ie1a(j)),
C      &                               ila(ie1a(j)),xja(ie1a(j))
C                         write(iunt15,1000) wvla(isel) , maxd , maxt ,
C      &                          filmem , type(4) , i , isel
C                         write(iunt15,1001) (densa(k),k=1,maxd)
C                         write(iunt15,1001) (teva(k),k=1,maxt)
C                         transa(isel)=trans
C                         do 56 k = 1 , maxd
C                           write(iunt15,1001)
C      &                           (max(aa(j)*stvi(kstrn,l,k,i), 1.0D-74)
C      &                                     , l=1,maxt)
C    56                   continue
C                         go to 58
C                     endif
C    57             continue
C               endif
C               if (metcnt.ge.ndpec) go to 59
C    58        continue
C    59       continue
C         endif
C 
C       endif
C
 
 160  isel4=isel



C-----------------------------------------------------------------------
C Add standard comments and configurarions details
C-----------------------------------------------------------------------

      if (open15) then

         call xxwcmt_15( iunt15     ,
     &                   ndstore    , idcmt    , idfld   , idsyn   ,
     &                   idion      , idopt    , ndlev   ,
     &                   lroot      , lsuper   ,
     &                   nfld_15    , isyn_15  , fldk_15 ,
     &                   lfld_15    ,
     &                   iz0        , iz1      , popcode ,
     &                   dsninc     , flexp    ,
     &                   nion       , cion     , lion    ,
     &                   nopt       , copt     , lopt    ,
     &                   il         , config   , cterm   , wa      ,
     &                   esym       , dsnpt    ,
     &                   iptnl      , iz       , tabul_15, units_15,
     &                   isel4      , nspb     , nspp    ,
     &                   isel1_out  , ctrans   , wtrans  ,
     &                   ctype      , ispbr    , isppr   , iszr    ,
     &                   itg        , ipr      , iwr     ,
     &                   code       , producer , date
     &                  )

      endif


C-----------------------------------------------------------------------
C Write SXB file for same PEC set if required
C-----------------------------------------------------------------------

      filmem = ' '
      call xxslen(dsninc,index1,index2)
      if((index2-index1).gt.4) then
          if(dsninc(index2-3:index2).eq.'.dat') then
              index1=max(index2-11,1)
              index2=index2-4
          else
              index1=max(index2-7,1)
          endif
          index1=index1+index(dsninc(index1:index2),'/')
      endif
      filmem=dsninc(index1:index2)

      if (open13) then

         nblocks =  isel1_out*nmet
         write(iunt13,2004) nblocks, titled , iz

         isel=0
         do 250 i = 1 , nmet
            metcnt = 0
            write(metas,1007)imetr(i),isa(imetr(i)),ila(imetr(i)),
     &                       xja(imetr(i))
            do 240 ii=1,isel1_out
C               j=inde_peca(indx_peca(ii))
               j=inde_peca(indx_peca(ipr(ii)))
               iulev = ie2a(j)
               kstrn = 0
               do im = 1 , nmet
                   if ( iulev .eq. imetr(im) ) then
                       kstrn = im
                       go to 240
                   endif
               end do
               do io = 1 , nord
                   if ( iulev .eq. iordr(io) ) kstrn = io
               end do
               wvl = 1.0d8/(wa(ie2a(j))-wa(ie1a(j)))
               call b8wvrg( ndwvl  ,
     &                      nwvl   , wvls   , wvll   , avlt ,
     &                      wvl    , aa(j)  ,
     &                      lwvrg  , iwvrg
     &                    )
               call b8wvtr( ndsel   ,
     &                      ntrsel  , itrlow  , itrup ,
     &                      ie1a(j) , ie2a(j) ,
     &                      lwvrg
     &                    )
     
               if (.not.lwvrg) goto 240
               
               metcnt = metcnt+1
               isel = isel + 1
               wvla(isel)=wvl
               ima(isel) = i
               nma(isel) = nmet
               write(trans,1006)ie2a(j),isa(ie2a(j)),ila(ie2a(j)),
     &                          xja(ie2a(j)),ie1a(j),isa(ie1a(j)),
     &                          ila(ie1a(j)),xja(ie1a(j))
               transa(isel)=trans
               if(lnorm.and.(nmet.eq.1))then
                   write(iunt13,1009) wvla(isel) , maxd , maxt ,
     &                                filmem , 'T' , isel
               else
                   write(iunt13,1010) wvla(isel) , maxd , maxt ,
     &                                filmem , i , isel
               endif
               write(iunt13,1002) (densa(k),k=1,maxd)
               write(iunt13,1002) (teva(k),k=1,maxt)
               do 220 k = 1 , maxd
                   do 222 l = 1 , maxt
                       if(stack(kstrn,i,l,k).le.0.0D0)then
                          sxba(l)=1.0D-74
                       else
                          srate=0.0d0
                          do ip=1,npl
                             if(liosel)then
                                 srate=srate+fvionr(i,ip,l,k)
                             else
                                 srate=srate+sgrda(l,i,ip)
                             endif
                          end do
                          sxba(l)= (densa(k)*srate)/(aa(j)*
     &                              stack(kstrn,i,l,k))
                       endif
                        if(sxba(l).le.1.0D-74)sxba(l)=1.0D-74
  222              continue
                   call b8cors(maxt, teva, sxba)
                   write(iunt13,1002)(sxba(l),l=1,maxt)
  220          continue
               if(metcnt.ge.ndpec) go to 250
  240       continue
  250    continue

C comments/configurations/transitions etc

         write(iunt13,1003)
         call b8winf( iunt13 , ladf10 , date   , user    ,
     &                ndlev  ,
     &                dsninc , dsfull , dsnexp ,
     &                iz0    , iz1    ,
     &                il     , nmet   , npl    , ibsela  ,
     &                lrsel  , lisel  , lhsel  , liosel  ,
     &                lpsel  , lzsel  , lnsel  , lnorm   ,
     &                lsseta , lss04a
     &               )


         write(iunt13,2110)
         do i = 1, il
            write(iunt13,2150)i, cstrga(i), isa(i), ila(i),
     &                        xja(i), wa(i)
         end do
         write(iunt13,2160)

         write(iunt13,4008)

         do i = 1,isel
             if(lnorm.and.(nmet.eq.1))then
                 write(iunt13,4011) i, wvla(i), transa(i),
     &                             '            ', 'T' , nma(i)
             else
                 write(iunt13,4004) i, wvla(i), transa(i),
     &                              metasa(ima(i)), ima(i), nma(i)
             endif
         end do

         write(iunt13,2014)'ADAS208', user, date

      endif



C-----------------------------------------------------------------------
 1000 format(1a10,2i4,' /',1a23,'/type=',1a5,
     &       '/',1a4,' = ',i1,'/isel = ',i4)
 1001 format(1a10,2i4,' /',1a23,'/type=',1a5,
     &       '/',1a4,' = ',1a1,'/isel = ',i4)
 1002 format(1p,8e9.2)
C-----------------------------------------------------------------------
 2003 format(i5,'    /',1a2,':',i2,' photon emissivity coefficients/')
 2011 format(1x,32('*'),' B8WRPS error ',32('*')//
     &       1x,'Mismatch of internal dimension parameters')
 2012 format(1x,32('*'),' B8WRPS error ',32('*')//
     &       1x,'Fault in pec output: ',a,i3,a)
 2013 format(/1x,29('*'),' Program terminated ',29('*'))
C-----------------------------------------------------------------------
 3000 format(1x,30('*'),' B8WRPS warning ',30('*')//
     &       1x,'No valid data in wavelength region -',
     &       1x,'No PEC file will be written', /,
     &       1x,76('*'))
 3001 format(1x,30('*'),' B8WRPS warning ',30('*')//
     &       1x,'truncation of lines applied - ',
     &       1x,'increase ndpec =', i6,' to at least ',i6 /,
     &       1x,76('*'))
 3002 format(1x,30('*'),' B8WRPS warning ',30('*')//
     &       1x,'field transition string too short - ',
     &       1x,'increase ctrans() strings to *',i2/,
     &       1x,76('*'))
C-----------------------------------------------------------------------
 1003 format('C---------------------------------------------------------
     &--------------'/'C'/'C  IONISATIONS PER PHOTON:',/,'C')
 1006 format(i4,'(',i1,')',i1,'(',f4.1,')-',i4,'(',i1,')',i1,'(',f4.1,
     &       ')')
 1007 format(i2,'(',i1,')',i1,'(',f4.1,')')
 1009 FORMAT(F8.1,' A',2I4,' /FILMEM = ',1A8,'/                ',
     &       '/INDM = ',1A1,'/ISEL = ',I4)
 1010 FORMAT(F8.1,' A',2I4,' /FILMEM = ',1A8,'/                ',
     &       '/INDM = ',I1,'/ISEL = ',I4)
 2004 format('  ',i3,'    /',1a3,i2,' IONISATIONS PER PHOTON/')
 2014   format('C',/,
     &         'C  CODE     : ',1a7/
     &         'C  PRODUCER : ',a30/
     &         'C  DATE     : ',1a8,/,'C',/,'C',79('-'))
 2110 format('C',/'C',/,'C   Configuration           (2s+1)l(w-1/2)',
     &       '          Energy (cm**-1)')
 2150 format('C',3x,i3,3x,a18,3x,'(',i1,')',i1,'(',f4.1,')',5x,f12.1)
 2160 format('C',/'C')
 4008 FORMAT('C'/
     &'C  ISEL  WAVELENGTH       TRANSITION               TYPE   ',
     &'METASTABLE  IMET NMET'/
     &'C  ----  ----------  ----------------------------  ----- ',
     &' ----------- ---- ---- ')
 4004 FORMAT('C',I4,'.',F11.2,3X,1A29,2X,'EXCIT',1X,1A12,I4,2X,I4)
 4011 FORMAT('C',I4,'.',F11.2,3X,1A29,2X,'EXCIT',1X,1A12,3X,1A1,2X,I4)
C-----------------------------------------------------------------------

      return
      end
