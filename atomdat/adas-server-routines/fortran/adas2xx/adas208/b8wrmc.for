      SUBROUTINE B8WRMC( IUNIT  , IUNT14 , IUNT15 , IUNT16 , IUNT17 ,
     &                   IUNT18 , IUNT19 , IUNT20 , IUNT21 ,
     &                   IUNT22 , IUNT23 ,
     &                   DSNINC , DSFULL , DSNEXP ,
     &                   TITLED , DATE   , USER   ,
     &                   NDLEV  , NDTEM  , NDDEN  , NDMET  ,
     &                   LNORM  , IZ     , IZ0    , IZ1    ,
     &                   IBSELA , BWNOA  , PRTWTA ,
     &                   IL     , NMET   , NORD   , IMETR  ,
     &                   IA     , ISA    , ILA    , XJA    ,
     &                   CSTRGA , WA     ,
     &                   MAXT   , MAXD   , TEVA   , DENSA  ,
     &                   NPL    , NPLR   , NPL3   , NPLI   , CPRTA  ,
     &                   LRSEL  , LISEL  , LHSEL  , LIOSEL ,
     &                   LPSEL  , LZSEL  , LNSEL  , FVCRED ,
     &                   FVRRED , FVIRED , FVHRED , FVIONR ,
     &                   FVCRPR , PL     , PH     , PS     , SWVLN  ,
     &                   PR     ,
     &                   RATPIA , RATMIA , STACK  , STCKM  ,
     &                   LSSETA , LSS04A
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B8WRMC ****************** *
C
C  PURPOSE:  TO OUTPUT DATA TO GENERALISED COLLISIONAL RADIATIVE
C            COEFFICIENT PASSING FILE MASTER.PASS
C            FINAL STORAGE IS EXPECTED TO BE IN MASTER CONDENSED FILES
C
C
C  CALLING PROGRAM: ADAS208
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT NUMBER FOR GCR INFORMATION
C  INPUT : (I*4)  IUNT14  = OUTPUT UNIT NUMBER FOR ACD DATA
C  INPUT : (I*4)  IUNT15  = OUTPUT UNIT NUMBER FOR SCD DATA
C  INPUT : (I*4)  IUNT16  = OUTPUT UNIT NUMBER FOR CCD DATA
C  INPUT : (I*4)  IUNT17  = OUTPUT UNIT NUMBER FOR QCD DATA
C  INPUT : (I*4)  IUNT18  = OUTPUT UNIT NUMBER FOR XCD DATA
C  INPUT : (I*4)  IUNT19  = OUTPUT UNIT NUMBER FOR PRB DATA
C  INPUT : (I*4)  IUNT20  = OUTPUT UNIT NUMBER FOR PRC DATA
C  INPUT : (I*4)  IUNT21  = OUTPUT UNIT NUMBER FOR PLT DATA
C  INPUT : (I*4)  IUNT22  = OUTPUT UNIT NUMBER FOR PLS DATA
C  INPUT : (I*4)  IUNT23  = OUTPUT UNIT NUMBER FOR MET DATA
C  INPUT : (C*80) DSNINC  = INPUT COPASE DATA SET NAME (IN QUOTES).
C  INPUT : (C*80) DSFULL  = INPUT SZD  DATA SET NAME (IN QUOTES).
C  INPUT : (C*80) DSNEXP  = INPUT EXPANSION FILE
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C  INPUT : (C*8)  DATE    = CURRENT DATE.
C  INPUT : (C*30) USER    = USER IDENTIFIER
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (I*4)  IBSELA(,)=IONISATION DATA BLOCK SELECTOR INDICES
C                           1ST DIMENSION - (Z)   ION METASTABLE COUNT
C                           2ND DIMENSION - (Z+1) ION METASTABLE COUNT
C  INPUT : (R*8)  BWNOA() = IONISATION POTENTIALS TO (Z+1) METAS.(CM-1)
C  INPUT : (R*8)  PRTWTA()= STATISTICAL WEIGHTS OF (Z+1) METASTABLES
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT : (L*4)  LNORM   =.TRUE.  => IF NMET=1 THEN VARIOUS
C                                     IONISATION OUTPUT FILE
C                                     NORMALISED TO STAGE TOT.POPULATN.
C                                     (** NORM TYPE = T)
C                         =.FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                     METASTABLE POPULATIONS.
C                                      (** NORM TYPE = M)
C
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES LEVELS: 1<=NMET<=NDMET
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C  INPUT : (I*4)  IMETR() = INDEX OF (Z) METAS. IN COMPLETE LEVEL LIST
C
C  INPUT : (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                           DIMENSION: LEVEL INDEX
C  INPUT : (R*8)  AA()    = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C
C  INPUT : (R*8)  SGRDA(,,)=GROUND & METASTABLE IONISATION RATE
C                           COEFFICIENTS  FROM SZD FILES (CM3 SEC-1)
C                           1ST DIMENSION: TEMPERATURE INDEX
C                           2ND DIMENSION: (Z)   ION METASTABLE INDEX
C                           3RD DIMENSION: (Z+1) ION METASTABLE INDEX
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C
C  INPUT : (I*4)  NPL      = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                              BY EXCITED STATE IONISATION IN COPASE
C                              FILE WITH IONISATION POTENTIALS GIVEN
C                              ON THE FIRST DATA LINE
C  INPUT : (I*4)  NPLR     = NO. OF ACTIVE METAS. FOR RECOM OF (Z+1) ION
C  INPUT : (I*4)  NPL3     = NO. OF ACTIVE METAS. FOR RE+3B OF (Z+1) ION
C  INPUT : (I*4)  NPLI     = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C
C  INPUT : (L*4)  LRSEL    = .TRUE.  -  RECOMB  OF (Z+1) ION ACTIVE
C                            .FALSE. -  RECOMB. OF (Z+1) ION INACTIVE
C  INPUT : (L*4)  LISEL    = .TRUE.  -  IONIS.  OF (Z-1) ION ACTIVE
C                            .FALSE. -  IONIS.  OF (Z-1) ION INACTIVE
C  INPUT : (L*4)  LHSEL    = .TRUE.  -  CX REC. OF (Z+1) ION ACTIVE
C                            .FALSE. -  CX REC. OF (Z+1) ION INACTIVE
C  INPUT : (L*4)  LIOSEL   = .TRUE.  -  IONIS.  OF (Z) ION ACTIVE
C                            .FALSE. -  IONIS.  OF (Z) ION INACTIVE
C  INPUT : (L*4)  LPSEL    = .TRUE.  => INCLUDE PROTON COLLISIONS
C                          = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  INPUT : (L*4)  LZSEL    = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                       PLASMA Z EFFECTIVE'ZEFF'.
C                          = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                       WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                          (ONLY USED IF 'LPSEL=.TRUE.')
C  INPUT : (L*4)  LNSEL    = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                        FROM DATAFILE IF AVAILABLE
C                          = .FALSE. => DO NOT INCLUDE PROJECTED
C                                       BUNDLE-N DATA
C
C  INPUT : (R*8) FVCRED(,,,) = (Z)-(Z) CROSS GEN. COLL. RAD. COEFFTS.
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: (Z) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8) FVRRED(,,,) = (Z+1)-(Z) RECOM GEN. COLL. RAD. COEFFTS.
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: (Z+1) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8) FVIRED(,,,) = (Z-1)-(Z) IONIS GEN. COLL. RAD. COEFFTS.
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: (Z-1) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8) FVHRED(,,,) = (Z+1)-(Z) CX R. GEN. COLL. RAD. COEFFTS.
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: (Z+1) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8) FVIONR(,,,) = (Z)-(Z+1) IONIS GEN. COLL. RAD. COEFFTS.
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: (Z+1) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8) FVCRPR(,,,) = (Z+1)-(Z+1) CROSS COLL. RAD. COEFFTS.
C                             1ST DIMENSION: (Z+1) METASTABLE INDEX
C                                   FINAL STATE
C                             2ND DIMENSION: (Z+1) METASTABLE INDEX
C                                   INITIAL STATE
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8) PL(,,)     = TOTAL LINE POWER COEFFICIENTS
C                             1ST DIMENSION: (Z) METASTABLE INDEX
C                             2ND DIMENSION: TEMPERATURE INDEX
C                             3RD DIMENSION: DENSITY INDEX
C                                 UNITS: ERG SEC-1
C  INPUT : (R*8) PH(,,)     = CX RECOMBINATION POWER COEFFICIENTS
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: DENSITY INDEX
C                             3RD DIMENSION: (Z+1) PARENT METAS. INDEX
C                                 UNITS: ERG SEC-1
C  INPUT : (R*8) PS(,,,)    = SPECIFIC LINE POWER COEFFICIENTS
C                             1ST DIMENSION: METASTABLE LINE INDEX
C                             2ND DIMENSION: (Z) METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C                                 UNITS: ERG SEC-1
C  INPUT : (R*8) SWVLN()    = WAVELENGTHS (ANGSTROM) OF SPECIFIC LINES
C                             1ST DIMENSION: METASTABLE LINE INDEX
C  INPUT : (R*8)  PR(,,)    = RECOM/BREMS. COEFFT (ERG S-1)
C                             1ST DIM: PARENT INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C  INPUT : (R*8)  RATPIA(,) = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                             1ST DIMENSION: TEMP/DENS INDEX
C                             2ND DIMENSION: PARENT INDEX
C  INPUT : (R*8)  RATMIA(,) = RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                             1ST DIMENSION: TEMP/DENS INDEX
C                             2ND DIMENSION: PARENT INDEX
C
C  INPUT : (R*4)  STACK(,,,)= POPULATION DEPENDENCE
C                             1ST DIMENSION: ORDINARY LEVEL INDEX
C                             2ND DIMENSION: METASTABLE INDEX
C                             3RD DIMENSION: TEMPERATURE INDEX
C                             4TH DIMENSION: DENSITY INDEX
C  INPUT : (R*8)  STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (L*4)  LSSETA(,) = .TRUE.  - MET. IONIS RATE SET IN B8GETS
C                             .FALSE.- MET. IONIS RATE NOT SET IN B8GETS
C                              1ST DIMENSION: (Z) ION METASTABLE INDEX
C                              2ND DIMENSION: (Z+1) ION METASTABLE INDEX
C  INPUT : (L*4)  LSS04A(,) = .TRUE. => IONIS. RATE SET IN ADF04 FILE:
C                             .FALSE.=> NOT SET IN ADF04 FILE
C                              1ST DIM: LEVEL INDEX
C                              2ND DIM: PARENT METASTABLE INDEX
C
C          (R*8) DE        = ENERGY FOR TRANSITION ( CM-1)
C                            (IONIS. POT. FOR IONISATION COEFFTS.
C                             EXCIT. ENR. FOR EXCITATION COEFFTS.)
C
C          (I*4) I         = GENERAL USE
C          (I*4) IP        = GENERAL USE
C          (I*4) J         = GENERAL USE
C          (I*4) K         = GENERAL USE
C          (I*4) L         = GENERAL USE
C          (R*8) Z1        = RECOMBINING ION CHARGE
C          (R*8) DUM1      = GENERAL USE
C          (R*8) DUM2      = GENERAL USE
C          (R*8) DUM3      = GENERAL USE
C          (R*8) TR()      = REDUCED TEMPERATUTES ( TE(K) / Z1*Z1)
C          (R*8) DR()      = REDUCED DENSITIES    ( NE/ Z1**7)
C          (R*8) SUM()     = GENERAL USE IN RENORMALISATION
C          (R*8) FMULT()   = GENERAL USE IN RENORMALISATION
C
C
C
C ROUTINES:  
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B8CORR     ADAS      'FIXES' LOW TE PROBLEM IN REC. DATA
C          B8WINF     ADAS      DETERMINES IONIS. SOURCE AND WRITES 
C                               COMMENT BLOCK
C
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    24/06/92
C
C UPDATE:  13/08/93  HP SUMMERS  - INCLUDE NORMALISING TO TOTALS WHEN
C                                  (LNORM. AND.(NMET.EQ.1)
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C UPDATE:  04/03/96  HP SUMMERS  - OUTPUT C-R DATA TO SEPARATE FILES
C                                  INCLUDE METASTABLE FRACTION FILE
C                                  USE IUNIT FOR INFORMATION.
C UPDATE:  03/05/96  DH BROOKS   - CHANGED DSNINC & DSNEXP TO 80
C                                  CHARACTERS. ALTERED FORMATS 1003
C                                  & 2042 TO ACCOMODATE.
C UPDATE:  13/05/96  HP SUMMERS  - CORRECT TITLE LINE ON QCD208.PASS
C                                  FILE TO GIVE CORRECT JGRD, IGRD
C                                  NAMES.
C UPDATE:  24/05/96  HP SUMMERS  - ADDED SPECIFIC LINE DATA, PS AND
C                                  SWVLN TO PARAMETER LIST
C UPDATE:  03/06/96  HP SUMMERS  - ADDED CX RECOMBINATION DATA, PH
C
C UPDATE:  23/07/96  HP SUMMERS  - TIDY UP NAMES IN OUTPUT FILES FOR
C                                  CONSISTENCY
C
C
C UPDATE:  09/03/98  HP SUMMERS  - ADDED PRB TO DATA PASSED FROM THE 
C                                  PROJECTION MATRICES AND GIVEN AS 
C                                  OUTPUT FROM ADAS208
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C VERSION: 1.2				DATE: 13/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - INCREASED SIZE OF DSFULL TO 80
C
C VERSION: 1.3				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.4				DATE: 15-07-96
C MODIFIED: WILLIAM OSBORN
C	    ADDED HUGH'S CORRECTIONS DATED 13/05/96, 24/05/96 AND
C	    03/06/96 ABOVE
C
C VERSION: 1.5				DATE: 30-09-96
C MODIFIED: WILLIAM OSBORN
C	    ADDED HUGH'S CORRECTIONS DATED 23/07/96 ABOVE
C
C VERSION: 1.6				DATE: 18/10/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C           - ADDED CHECK FOR INDEX2.EQ.0 IN STRING PROCESSING
C
C VERSION: 1.7				DATE: 01/12/97
C MODIFIED: RICHARD MARTIN 
C		- FIXED BUG IN WRITING OUT OF TEMPERATURES IN PLT208.PASS,
C		  PLS208.PASS & MET208.PASS
C
C VERSION: 1.8				DATE: 09/03/98
C MODIFIED: HUGH SUMMERS 
C	  	- ADDED PRB TO DATA PASSED FROM THE  PROJECTION MATRICES 
C		  AND GIVEN AS OUTPUT FROM ADAS208.
C
C VERSION:  1.9                                   DATE: 2/09/99
C MODIFIED: Martin O'Mullane
C           - Pass in real name of author rather than uid.
C           - Removed nulls on output files
C           - Changed delimeter from () to _. in getting FILEMEM
C           - Incorrect logic in writing xcd files. Moved ENDIF to
C             end of inner DO loop. Don't write info blocks for
C             data which does not exist.
C           - Wrong header written to xcd passing file. Replace IGRD
C             with JPRT in the header line. 
C           - PRB output changed to write coefficient summed over
C             spin systems.
C           - Introduce b8corr to correct low Te problem
C             in recombination coefficients.
C           - Add source of ionisation data to comments with
C             call to b8winf. This also tidies up writing of the
C             comment block.
C
C VERSION:  1.10                                  DATE: 8/11/99
C MODIFIED: Martin O'Mullane
C           - Write effective zeros for SCD, MET and XCD data.
C           - Removed NDMET from call to b8winf
C
C VERSION : 1.11
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Increase size of filename variables from 80 to 132 characters.
C
C VERSION : 1.12                
C DATE    : 24-07-2017
C MODIFIED: Martin O'Mullane
C           - Do not write the pls208.pass file.
C 
C VERSION : 1.5
C DATE    : 05-12-2017
C MODIFIED: Matthew Bluteau
C           - Increase NOTRN to 300 (up from 64) to allow more lines 
C             in the reporting file, gcr.pass.
C           - Use i2 format for metastable labelling.
C
C-----------------------------------------------------------------------
      INTEGER   NOTRN
C-----------------------------------------------------------------------
      PARAMETER ( NOTRN = 300 )
C-----------------------------------------------------------------------
      REAL*8    DZERO
C-----------------------------------------------------------------------
      PARAMETER ( DZERO = 1.0D-72 )
C-----------------------------------------------------------------------
      INTEGER   NDLEV     , NDTEM      , NDDEN   , NDMET
      INTEGER   IUNIT     ,
     &          IUNT14    , IUNT15     , IUNT16  , IUNT17  ,
     &          IUNT18    , IUNT19     , IUNT20  , IUNT21  ,
     &          IUNT22    , IUNT23     ,
     &          NPL       , NPLR       , NPL3    , NPLI    ,
     &          IZ        , IZ0        , IZ1     , NZ      ,
     &          IL        , NMET       , NORD    ,
     &          MAXT      , MAXD
      INTEGER   I         , J          , K       , L       , IM     ,
     &          ISEL      , IULEV      , KSTRN   , IO      , IP     ,
     &          INDEX1    , INDEX2     , II
C-----------------------------------------------------------------------
      CHARACTER TITLED*3         , DSNINC*132    , FILMEM*8  , TYPE*9
      CHARACTER CSTRGA(NDLEV)*18 , DATE*8        , DSFULL*132, 
     &          DSNEXP*132
      CHARACTER TRANS*61         , TRANSA(NOTRN)*61
      CHARACTER METAS*12         , METASA(10)*12
      CHARACTER CHINDI*5         , CHINDJ*5      , USER*30
C-----------------------------------------------------------------------
      LOGICAL   LNORM     , LSTRN
      LOGICAL   LRSEL     , LISEL      , LHSEL   , LIOSEL
      LOGICAL   LPSEL     , LZSEL      , LNSEL
      LOGICAL   LADF10
C-----------------------------------------------------------------------
      INTEGER   IA(NDLEV)        , ISA(NDLEV)       , ILA(NDLEV)
      INTEGER   IMETR(NDMET)
      INTEGER   IBSELA(NDMET,NDMET)
C-----------------------------------------------------------------------
      REAL*8   DE     , Z1     , DUM1    , DUM2     , DUM3
C-----------------------------------------------------------------------
      REAL*8   TR(50) , DR(50) , SUM(50) , FMULT(50)
      REAL*8   XJA(NDLEV)        , TEVA(NDTEM)      , DENSA(NDDEN)
      REAL*8   WA(NDLEV)         , BWNOA(NDMET)     , PRTWTA(NDMET)
      REAL*8   FVCRED(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8   FVRRED(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8   FVIRED(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8   FVHRED(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8   FVIONR(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8   FVCRPR(NDMET,NDMET,NDTEM,NDDEN)
      REAL*8   PL(NDMET,NDTEM,NDDEN) , PS(NDMET,NDMET,NDTEM,NDDEN)  ,
     &         PH(NDTEM,NDDEN,NDMET) , SWVLN(NDMET)
      REAL*8   PR(NDMET,NDTEM,NDDEN)
      REAL*8   RATPIA(NDDEN, NDMET)  , RATMIA(NDDEN,NDMET)
      REAL*8   STCKM(NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      REAL*4   STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      CHARACTER CPRTA(NDMET)*9 
C-----------------------------------------------------------------------
      LOGICAL   LSSETA(NDMET,NDMET)  , LSS04A(NDLEV,NDMET)
C-----------------------------------------------------------------------
      DATA      TYPE/'EXCIT    '/
      DATA      LADF10/.TRUE./
C-----------------------------------------------------------------------

      INDEX1 = INDEX(DSNINC,'_')
      INDEX2 = INDEX(DSNINC,'.')
      IF(INDEX2.NE.0)THEN
         FILMEM = DSNINC(INDEX1+1:INDEX2-1)
      ELSE
         FILMEM = ' '
      ENDIF
      WRITE(IUNIT,1002) TITLED , IZ

      ISEL=0
      NZ = 1
      Z1 = DFLOAT(IZ1)

      DO 60 I = 1,MAXT
         TR(I) = 1.1605D+04 * TEVA(I) / (Z1 * Z1)
   60 CONTINUE
      DO 62 I = 1,MAXD
         DR(I) = DENSA(I) / (Z1 ** 7)
   62 CONTINUE

      DO  2 I = 1 , NMET
          WRITE(METAS,1007)IMETR(I),ISA(IMETR(I)),ILA(IMETR(I)),
     &                     XJA(IMETR(I))
          METASA(I)=METAS
    2 CONTINUE


C-----------------------------------------------------------------------
C    OUTPUT METASTABLE CROSS COUPLING COEFFICIENTS
C-----------------------------------------------------------------------

      WRITE(IUNT17,2034)
      DO  12 I = 1 , NMET
          DO 10 J = 1 , NMET
            CHINDI = 'IGRD='
            CHINDJ = 'JGRD='
            IF(I.NE.J) THEN
                  ISEL = ISEL + 1
                  WRITE(TRANS,1006)IZ, J, METASA(J),
     &                             IZ  , I, METASA(I) , 'EXCIT'
                  TRANSA(ISEL)=TRANS
                  DE = WA(IMETR(I))-WA(IMETR(J))
                  IF(DE.LE. 1.0D-70) DE= 1.00D-70
                  WRITE(IUNT17,2000)
                  WRITE(IUNT17,2022)J,I
                  WRITE(IUNT17,2004)
                  WRITE(IUNT17,2010)NZ,MAXT,MAXD,0
                  WRITE(IUNT17,2011)Z1
                  WRITE(IUNT17,2004)
                  WRITE(IUNT17,2004)
                  WRITE(IUNT17,2006) (DR(K) ,K=1,MAXD)
                  WRITE(IUNT17,2006) (TR(K) ,K=1,MAXT)
                  WRITE(IUNT17,2008) CHINDI, J,  CHINDJ,
     &                                             I, IZ1 , DATE
                  DO  5 L = 1 , MAXT
                    WRITE(IUNT17,2006)
     &                  (max(dzero,FVCRED(I,J,L,K)/DENSA(K)),K=1,MAXD)                
    5             CONTINUE
                  CALL B8WINF( IUNT17 , LADF10 , DATE   , USER    ,
     &                         NDLEV  , 
     &                         DSNINC , DSFULL , DSNEXP ,
     &                         IZ0    , IZ1    ,
     &                         IL     , NMET   , NPL    , IBSELA  ,
     &                         LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                         LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                         LSSETA , LSS04A
     &                        )
            ENDIF
   10     CONTINUE
   12  CONTINUE

C-----------------------------------------------------------------------
C    OUTPUT RECOMBINATION COEFFICIENTS
C-----------------------------------------------------------------------

      IF (LIOSEL) THEN

         call b8corr(maxt,maxd,nmet,npl3,teva,fvrred)

         WRITE(IUNT14,2032)
         CHINDI = 'IPRT='
         CHINDJ = 'IGRD='

         IF(LNORM.AND.(NMET.EQ.1))THEN
             I = 1
             J = 1
             ISEL = ISEL + 1
             DE = 0.0D0
             WRITE(TRANS,1006)IZ, I, METASA(I),
     &                        IZ+1, J, 'SEE '//FILMEM, 'REC-T'
             TRANSA(ISEL)=TRANS
             WRITE(IUNT14,2000)
             WRITE(IUNT14,2002)J,I
             WRITE(IUNT14,2004)
             WRITE(IUNT14,2010)NZ,MAXT,MAXD,0
             WRITE(IUNT14,2011)Z1
             WRITE(IUNT14,2004)
             WRITE(IUNT14,2004)
             WRITE(IUNT14,2006) (DR(K) ,K=1,MAXD)
             WRITE(IUNT14,2006) (TR(K) ,K=1,MAXT)
             WRITE(IUNT14,2009) CHINDI, J,  CHINDJ, I,  IZ1, DATE
             DO 16 K=1,MAXD
              SUM(K)=0.0D0
              DO 15 IP=1,NPL3
               SUM(K)=SUM(K)+RATPIA(K,IP)
   15         CONTINUE
              IF(SUM(K).EQ.0.0D0) THEN
                  FMULT(K) = 1.0
              ELSE
                  FMULT(K) = RATPIA(K,1)/SUM(K)
              ENDIF
   16        CONTINUE
             DO  17 L = 1 , MAXT
               WRITE(IUNT14,2006)(FVRRED(I,J,L,K)*FMULT(K) ,K=1,MAXD)
   17        CONTINUE
             CALL B8WINF( IUNT14 , LADF10 , DATE   , USER    ,
     &                    NDLEV  ,
     &                    DSNINC , DSFULL , DSNEXP ,
     &                    IZ0    , IZ1    ,
     &                    IL     , NMET   , NPL    , IBSELA  ,
     &                    LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                    LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                    LSSETA , LSS04A
     &                   )
         ELSE
             DO  22 I = 1 , NMET
              DO 21 J = 1 , NPL3
               ISEL = ISEL + 1
               DE = 0.0D0
               WRITE(TRANS,1006)IZ, I, METASA(I),
     &                          IZ+1, J, 'SEE '//FILMEM, 'RECOM'
               TRANSA(ISEL)=TRANS
               WRITE(IUNT14,2000)
               WRITE(IUNT14,2002)J,I
               WRITE(IUNT14,2004)
               WRITE(IUNT14,2010)NZ,MAXT,MAXD,0
               WRITE(IUNT14,2011)Z1
               WRITE(IUNT14,2004)
               WRITE(IUNT14,2004)
               WRITE(IUNT14,2006) (DR(K) ,K=1,MAXD)
               WRITE(IUNT14,2006) (TR(K) ,K=1,MAXT)
               WRITE(IUNT14,2008) CHINDI, J,  CHINDJ, I,  IZ1, DATE
                   DO  20 L = 1 , MAXT
                       WRITE(IUNT14,2006)(FVRRED(I,J,L,K) ,K=1,MAXD)
   20              CONTINUE
                   CALL B8WINF( IUNT14 , LADF10 , DATE   , USER    ,
     &                          NDLEV  , 
     &                          DSNINC , DSFULL , DSNEXP ,
     &                          IZ0    , IZ1    ,
     &                          IL     , NMET   , NPL    , IBSELA  ,
     &                          LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                          LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                          LSSETA , LSS04A
     &                         )
   21         CONTINUE
   22        CONTINUE
         ENDIF

C---------

      ELSEIF (LRSEL.AND.(.NOT.LIOSEL)) THEN

         WRITE(IUNT14,2032)
         CHINDI = 'IPRT='
         CHINDJ = 'IGRD='

         IF(LNORM.AND.(NMET.EQ.1))THEN
             I = 1
             J = 1
             ISEL = ISEL + 1
             DE = 0.0D0
             WRITE(TRANS,1006)IZ, I, METASA(I),
     &                        IZ+1, J, 'SEE '//FILMEM, 'REC-T'
             TRANSA(ISEL)=TRANS
             WRITE(IUNT14,2000)
             WRITE(IUNT14,2002)J,I
             WRITE(IUNT14,2004)
             WRITE(IUNT14,2010)NZ,MAXT,MAXD,0
             WRITE(IUNT14,2011)Z1
             WRITE(IUNT14,2004)
             WRITE(IUNT14,2004)
             WRITE(IUNT14,2006) (DR(K) ,K=1,MAXD)
             WRITE(IUNT14,2006) (TR(K) ,K=1,MAXT)
             WRITE(IUNT14,2009) CHINDI, J,  CHINDJ, I,  IZ1, DATE
             DO 26 K=1,MAXD
              SUM(K)=0.0D0
              DO 25 IP=1,NPLR
               SUM(K)=SUM(K)+RATPIA(K,IP)
   25         CONTINUE
              IF(SUM(K).EQ.0.0D0) THEN
                  FMULT(K) = 1.0
              ELSE
                  FMULT(K) = RATPIA(K,1)/SUM(K)
              ENDIF
   26        CONTINUE
             DO  27 L = 1 , MAXT
               WRITE(IUNT14,2006)(FVRRED(I,J,L,K)*FMULT(K) ,K=1,MAXD)
   27        CONTINUE
             CALL B8WINF( IUNT14 , LADF10 , DATE   , USER    ,
     &                    NDLEV  ,
     &                    DSNINC , DSFULL , DSNEXP ,
     &                    IZ0    , IZ1    ,
     &                    IL     , NMET   , NPL    , IBSELA  ,
     &                    LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                    LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                    LSSETA , LSS04A
     &                   )
         ELSE
             DO  32 I = 1 , NMET
              DO 31 J = 1 , NPLR
               ISEL = ISEL + 1
               DE = 0.0D0
               WRITE(TRANS,1006)IZ, I, METASA(I),
     &                          IZ+1, J, 'SEE '//FILMEM, 'RECOM'
               TRANSA(ISEL)=TRANS
               WRITE(IUNT14,2000)
               WRITE(IUNT14,2002)J,I
               WRITE(IUNT14,2004)
               WRITE(IUNT14,2010)NZ,MAXT,MAXD,0
               WRITE(IUNT14,2011)Z1
               WRITE(IUNT14,2004)
               WRITE(IUNT14,2004)
               WRITE(IUNT14,2006) (DR(K) ,K=1,MAXD)
               WRITE(IUNT14,2006) (TR(K) ,K=1,MAXT)
               WRITE(IUNT14,2008) CHINDI, J,  CHINDJ, I,  IZ1, DATE
               DO  30 L = 1 , MAXT
                   WRITE(IUNT14,2006)(FVRRED(I,J,L,K) ,K=1, MAXD)
   30          CONTINUE
               CALL B8WINF( IUNT14 , LADF10 , DATE   , USER    ,
     &                      NDLEV  , 
     &                      DSNINC , DSFULL , DSNEXP ,
     &                      IZ0    , IZ1    ,
     &                      IL     , NMET   , NPL    , IBSELA  ,
     &                      LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                      LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                      LSSETA , LSS04A
     &                     )
   31         CONTINUE
   32        CONTINUE
         ENDIF
      ENDIF

C-----------------------------------------------------------------------
C    OUTPUT IONISATION COEFFICIENTS
C-----------------------------------------------------------------------

      IF (LIOSEL) THEN

            WRITE(IUNT15,2030)
            CHINDI = 'IPRT='
            CHINDJ = 'IGRD='

            IF(LNORM.AND.(NMET.EQ.1))THEN
                I = 1
                J = 1
                DE = BWNOA(J)-WA(IMETR(I))
                ISEL = ISEL + 1
                WRITE(TRANS,1006)IZ, I, METASA(I),
     &                           IZ+1, J, 'SEE '//FILMEM, 'ION-T'
                  TRANSA(ISEL)=TRANS
                  WRITE(IUNT15,2000)
                  WRITE(IUNT15,2002)J,I
                  WRITE(IUNT15,2004)
                  WRITE(IUNT15,2010)NZ,MAXT,MAXD,0
                  WRITE(IUNT15,2011)Z1
                  WRITE(IUNT15,2004)
                  WRITE(IUNT15,2004)
                  WRITE(IUNT15,2006) (DR(K) ,K=1,MAXD)
                  WRITE(IUNT15,2006) (TR(K) ,K=1,MAXT)
                  WRITE(IUNT15,2009) CHINDI, J,  CHINDJ, I,  IZ1, DATE
                  DO  37 L = 1 , MAXT
                   DO 36 K=1,MAXD
                    SUM(K)=0.0D0
                     DO 35 IP=1,NPL3
                      SUM(K)=SUM(K)+FVIONR(I,IP,L,K)
   35                CONTINUE
                     CALL B8NORM( NDLEV , NDMET ,
     &                            NORD  ,
     &                            STACK(1,1,L,K),
     &                            SUM(K), DUM1  ,
     &                            DUM2  , DUM3
     &                          )
   36               CONTINUE
                   WRITE(IUNT15,2006)(max(dzero,SUM(K)), K=1, MAXD)
   37             CONTINUE
                  CALL B8WINF( IUNT15 , LADF10 , DATE   , USER   ,
     &                         NDLEV  , 
     &                         DSNINC , DSFULL , DSNEXP ,
     &                         IZ0    , IZ1    ,
     &                         IL     , NMET   , NPL    , IBSELA  ,
     &                         LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                         LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                         LSSETA , LSS04A
     &                        )
            ELSE
                DO 42 I=1,NMET
                 DO 41 J = 1 , NPL3
                  DE = BWNOA(J)-WA(IMETR(I))
                  ISEL = ISEL + 1
                  WRITE(TRANS,1006)IZ, I, METASA(I),
     &                             IZ+1, J, 'SEE '//FILMEM, 'IONIS'
                  TRANSA(ISEL)=TRANS
                  WRITE(IUNT15,2000)
                  WRITE(IUNT15,2002)J,I
                  WRITE(IUNT15,2004)
                  WRITE(IUNT15,2010)NZ,MAXT,MAXD,0
                  WRITE(IUNT15,2011)Z1
                  WRITE(IUNT15,2004)
                  WRITE(IUNT15,2004)
                  WRITE(IUNT15,2006) (DR(K) ,K=1,MAXD)
                  WRITE(IUNT15,2006) (TR(K) ,K=1,MAXT)
                  WRITE(IUNT15,2008) CHINDI, J,  CHINDJ, I,  IZ1, DATE
                  DO  40 L = 1 , MAXT
                     WRITE(IUNT15,2006)
     &                   (max(dzero,FVIONR(I,J,L,K)), K=1, MAXD)
   40             CONTINUE
                  CALL B8WINF( IUNT15 , LADF10 , DATE   , USER    ,
     &                         NDLEV  ,
     &                         DSNINC , DSFULL , DSNEXP ,
     &                         IZ0    , IZ1    ,
     &                         IL     , NMET   , NPL    , IBSELA  ,
     &                         LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                         LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                         LSSETA , LSS04A
     &                        )
   41            CONTINUE
   42           CONTINUE
            ENDIF
            
      ENDIF

C-----------------------------------------------------------------------
C    OUTPUT PARENT CROSS COUPLING COEFFICIENTS
C-----------------------------------------------------------------------

      WRITE(IUNT18,2038)
      CHINDI = 'IPRT='
      CHINDJ = 'JPRT='

      IF(LNORM.AND.(NMET.EQ.1))THEN
         CONTINUE
      ELSE
         DO  67 I = 1 , NPL3
          DO 68 J = 1 , NPL3
           IF(I.NE.J) THEN
                ISEL = ISEL + 1
                WRITE(TRANS,1006)IZ+1, J, 'SEE '//FILMEM,
     &                           IZ+1, I, 'SEE '//FILMEM , 'PRTQC'
                TRANSA(ISEL)=TRANS
                DE = 0.0D+0
                WRITE(IUNT18,2000)
                WRITE(IUNT18,2025)J,I
                WRITE(IUNT18,2004)
                WRITE(IUNT18,2010)NZ,MAXT,MAXD,0
                WRITE(IUNT18,2011)Z1
                WRITE(IUNT18,2004)
                WRITE(IUNT18,2004)
                WRITE(IUNT18,2006) (DR(K) ,K=1,MAXD)
                WRITE(IUNT18,2006) (TR(K) ,K=1,MAXT)
                WRITE(IUNT18,2008) CHINDI, J,  CHINDJ,
     &                                           I, IZ1 , DATE
                DO 65 L = 1 , MAXT
                 WRITE(IUNT18,2006)
     &                (max(dzero,FVCRPR(I,J,L,K)) , K=1,MAXD)             
   65           CONTINUE
                CALL B8WINF( IUNT18 , LADF10 , DATE   , USER    ,
     &                       NDLEV  , 
     &                       DSNINC , DSFULL , DSNEXP ,
     &                       IZ0    , IZ1    ,
     &                       IL     , NMET   , NPL    , IBSELA  ,
     &                       LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                       LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                       LSSETA , LSS04A
     &                      )
            ENDIF
   68      CONTINUE
   67     CONTINUE
      ENDIF

C-----------------------------------------------------------------------
C    OUTPUT CX COEFFICIENTS
C-----------------------------------------------------------------------

          IF (LHSEL) THEN

             WRITE(IUNT16,2033)
             CHINDI = 'IPRT='
             CHINDJ = 'IGRD='

             IF(LNORM.AND.(NMET.EQ.1))THEN
                 I = 1
                 J = 1
                 ISEL = ISEL + 1
                 DE = 0.0D0
                 WRITE(TRANS,1006)IZ, I, METASA(I),
     &                            IZ+1, J, 'SEE '//FILMEM, 'CXR-T'
                 TRANSA(ISEL)=TRANS
                 WRITE(IUNT16,2000)
                 WRITE(IUNT16,2002)J,I
                 WRITE(IUNT16,2004)
                 WRITE(IUNT16,2010)NZ,MAXT,MAXD,0
                 WRITE(IUNT16,2011)Z1
                 WRITE(IUNT16,2004)
                 WRITE(IUNT16,2004)
                 WRITE(IUNT16,2006) (DR(K) ,K=1,MAXD)
                 WRITE(IUNT16,2006) (TR(K) ,K=1,MAXT)
                 WRITE(IUNT16,2009) CHINDI, J,  CHINDJ, I,  IZ1, DATE
                 DO 46 K=1,MAXD
                  SUM(K)=0.0D0
                  DO 45 IP=1,NPLR
                   SUM(K)=SUM(K)+RATPIA(K,IP)
   45             CONTINUE
                  IF(SUM(K).EQ.0.0D0) THEN
                      FMULT(K) = 1.0
                  ELSE
                      FMULT(K) = RATPIA(K,1)/SUM(K)
                  ENDIF
   46            CONTINUE
                 DO  47 L = 1 , MAXT
                  WRITE(IUNT16,2006)(FVHRED(I,J,L,K)*FMULT(K) ,K=1,MAXD)
   47            CONTINUE
                 CALL B8WINF( IUNT16 , LADF10 , DATE   , USER    ,
     &                        NDLEV  , 
     &                        DSNINC , DSFULL , DSNEXP ,
     &                        IZ0    , IZ1    ,
     &                        IL     , NMET   , NPL    , IBSELA  ,
     &                        LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                        LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                        LSSETA , LSS04A
     &                       )
             ELSE
                 DO  52 I = 1 , NMET
                  DO 51 J = 1 , NPLR
                   ISEL = ISEL + 1
                   DE = 0.0D0
                   WRITE(TRANS,1006)IZ, I, METASA(I),
     &                              IZ+1, J, 'SEE '//FILMEM, 'CXREC'
                   TRANSA(ISEL)=TRANS
                   WRITE(IUNT16,2000)
                   WRITE(IUNT16,2002)J,I
                   WRITE(IUNT16,2004)
                   WRITE(IUNT16,2010)NZ,MAXT,MAXD,0
                   WRITE(IUNT16,2011)Z1
                   WRITE(IUNT16,2004)
                   WRITE(IUNT16,2004)
                   WRITE(IUNT16,2006) (DR(K) ,K=1,MAXD)
                   WRITE(IUNT16,2006) (TR(K) ,K=1,MAXT)
                   WRITE(IUNT16,2008) CHINDI, J,  CHINDJ, I,  IZ1, DATE
                   DO  50 L = 1 , MAXT
                    WRITE(IUNT16,2006)(FVHRED(I,J,L,K) ,K=1, MAXD)
   50              CONTINUE
                   CALL B8WINF( IUNT16 , LADF10 , DATE   , USER    ,
     &                          NDLEV  ,
     &                          DSNINC , DSFULL , DSNEXP ,
     &                          IZ0    , IZ1    ,
     &                          IL     , NMET   , NPL    , IBSELA  ,
     &                          LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                          LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                          LSSETA , LSS04A
     &                         )
   51             CONTINUE
   52            CONTINUE
             ENDIF
           ENDIF

C-----------------------------------------------------------------------
C    OUTPUT CX RECOMBINATION POWER COEFFICIENTS
C-----------------------------------------------------------------------

          IF (LHSEL) THEN

             WRITE(IUNT20,2035)
             CHINDI = 'IPRT='
             CHINDJ = 'IGRD='

             IF(LNORM.AND.(NMET.EQ.1))THEN
                 J = 1
                 I = 1
                 ISEL = ISEL + 1
                 DE = 0.0D0
                 WRITE(TRANS,1006)IZ, I, METASA(I),
     &                            IZ+1, J, 'SEE '//FILMEM, 'PRC-T'
                 TRANSA(ISEL)=TRANS
                 WRITE(IUNT20,2000)
                 WRITE(IUNT20,2002)J,I
                 WRITE(IUNT20,2004)
                 WRITE(IUNT20,2010)NZ,MAXT,MAXD,0
                 WRITE(IUNT20,2011)Z1
                 WRITE(IUNT20,2004)
                 WRITE(IUNT20,2004)
                 WRITE(IUNT20,2006) (DR(K) ,K=1,MAXD)
                 WRITE(IUNT20,2006) (TR(K) ,K=1,MAXT)
                 WRITE(IUNT20,2009) CHINDI, J,  CHINDJ, 0,  IZ1, DATE
                 DO 146 K=1,MAXD
                  SUM(K)=0.0D0
                  DO 145 IP=1,NPLR
                   SUM(K)=SUM(K)+RATPIA(K,IP)
  145             CONTINUE
                  IF(SUM(K).EQ.0.0D0) THEN
                      FMULT(K) = 1.0
                  ELSE
                      FMULT(K) = RATPIA(K,1)/SUM(K)
                  ENDIF
  146            CONTINUE
                 DO  147 L = 1 , MAXT
                  WRITE(IUNT20,2006)(PH(L,K,J)*FMULT(K) ,K=1,MAXD)
  147            CONTINUE
                 CALL B8WINF( IUNT20 , LADF10 , DATE   , USER    ,
     &                        NDLEV  ,
     &                        DSNINC , DSFULL , DSNEXP ,
     &                        IZ0    , IZ1    ,
     &                        IL     , NMET   , NPL    , IBSELA  ,
     &                        LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                        LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                        LSSETA , LSS04A
     &                       )
             ELSE
                  I = 1
                  DO 151 J = 1 , NPLR
                   ISEL = ISEL + 1
                   DE = 0.0D0
                   WRITE(TRANS,1006)IZ, I, METASA(I),
     &                              IZ+1, J, 'SEE '//FILMEM, 'PCTOT'
                   TRANSA(ISEL)=TRANS
                   WRITE(IUNT20,2000)
                   WRITE(IUNT20,2002)J,0
                   WRITE(IUNT20,2004)
                   WRITE(IUNT20,2010)NZ,MAXT,MAXD,0
                   WRITE(IUNT20,2011)Z1
                   WRITE(IUNT20,2004)
                   WRITE(IUNT20,2004)
                   WRITE(IUNT20,2006) (DR(K) ,K=1,MAXD)
                   WRITE(IUNT20,2006) (TR(K) ,K=1,MAXT)
                   WRITE(IUNT20,2008) CHINDI, J,  CHINDJ, 0,  IZ1, DATE
                   DO  150 L = 1 , MAXT
                    WRITE(IUNT20,2006)(PH(L,K,J) ,K=1, MAXD)
  150              CONTINUE
                   CALL B8WINF( IUNT20 , LADF10 , DATE   , USER    ,
     &                          NDLEV  ,
     &                          DSNINC , DSFULL , DSNEXP ,
     &                          IZ0    , IZ1    ,
     &                          IL     , NMET   , NPL    , IBSELA  ,
     &                          LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                          LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                          LSSETA , LSS04A
     &                         )
  151             CONTINUE
  152            CONTINUE
             ENDIF
           ENDIF

C-----------------------------------------------------------------------
C    OUTPUT LINE POWER COEFFICIENTS
C-----------------------------------------------------------------------


       WRITE(IUNT21,2036)
       WRITE(IUNT21,2000)
       WRITE(IUNT21,2001)NMET
       WRITE(IUNT21,2003)(I,I=1,NMET)
       WRITE(IUNT21,2005)(CSTRGA(IMETR(I))(1:13),I=1,NMET)
       WRITE(IUNT21,2007)(IMETR(I),I=1,NMET)
       WRITE(IUNT21,2016)('*',I=1,NMET)
       WRITE(IUNT21,2017)('*',I=1,NMET)
       WRITE(IUNT21,2004)
       WRITE(IUNT21,2010)NZ,MAXT,MAXD,0
       WRITE(IUNT21,2011)Z1
       WRITE(IUNT21,2004)
       WRITE(IUNT21,2004)
       WRITE(IUNT21,2006)(DR(K),K=1,MAXD)
       WRITE(IUNT21,2006)(TR(K),K=1,MAXT)
       WRITE(IUNT21,2020)IZ1,DATE

       IF(LNORM.AND.(NMET.EQ.1))THEN
           I = 1
           J = 0
           DE = 0.0D+0
           ISEL = ISEL + 1
           WRITE(TRANS,1006)IZ, I, METASA(I),
     &                      IZ, J, 'SEE '//FILMEM, 'PLT-T'
           TRANSA(ISEL)=TRANS
           WRITE(IUNT21,2019) I
           DO  83 L = 1 , MAXT
            DO 82 K= 1, MAXD
             SUM(K)=PL(I,L,K)
             CALL B8NORM( NDLEV , NDMET ,
     &                    NORD  ,
     &                    STACK(1,1,L,K),
     &                    SUM(K), DUM1  ,
     &                    DUM2  , DUM3
     &                          )
   82       CONTINUE
            WRITE(IUNT21,2006)(SUM(K) , K=1, MAXD)
   83      CONTINUE
       ELSE
            DO 86 I=1,NMET
                J = 0
                DE = 0.0D+0
                ISEL = ISEL + 1
                WRITE(TRANS,1006)IZ, I, METASA(I),
     &                           IZ, J, 'SEE '//FILMEM, 'PLTOT'
                TRANSA(ISEL)=TRANS
                WRITE(IUNT21,2019) I
                  DO  85 L = 1 , MAXT
                      WRITE(IUNT21,2006)(PL(I,L,K) , K=1, MAXD)
   85             CONTINUE
   86       CONTINUE
       ENDIF
       CALL B8WINF( IUNT21 , LADF10 , DATE   , USER    ,
     &              NDLEV  ,
     &              DSNINC , DSFULL , DSNEXP ,
     &              IZ0    , IZ1    ,
     &              IL     , NMET   , NPL    , IBSELA  ,
     &              LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &              LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &              LSSETA , LSS04A
     &             )

C-----------------------------------------------------------------------
C    OUTPUT RECOMBINATION+BREMSSTRAHLUNG POWER COEFFICIENTS
C-----------------------------------------------------------------------

        WRITE(IUNT19,2040)
        CHINDI = 'IPRT='
        CHINDJ = 'IGRD='

        IF (LNORM.AND.(NMET.EQ.1)) THEN
            I = 1
            J = 1
            ISEL = ISEL + 1
            WRITE(TRANS,1006)IZ, I, METASA(I),
     &                       IZ+1, J, 'SEE '//FILMEM, 'PRB-T'
            TRANSA(ISEL)=TRANS
            WRITE(IUNT19,2000)
            WRITE(IUNT19,2002)J,I
            WRITE(IUNT19,2004)
            WRITE(IUNT19,2010)NZ,MAXT,MAXD,0
            WRITE(IUNT19,2011)Z1
            WRITE(IUNT19,2004)
            WRITE(IUNT19,2004)
            WRITE(IUNT19,2006) (DR(K) ,K=1,MAXD)
            WRITE(IUNT19,2006) (TR(K) ,K=1,MAXT)
            WRITE(IUNT19,2009) CHINDI, J,  CHINDJ, 0,  IZ1, DATE
            DO  K=1,MAXD
               SUM(K)=0.0D0
               DO IP=1,NPLR
                 SUM(K)=SUM(K)+RATPIA(K,IP)
               END DO
               IF (SUM(K).EQ.0.0D0) THEN
                  FMULT(K) = 1.0
               ELSE
                 FMULT(K) = RATPIA(K,1)/SUM(K)
               ENDIF
            END DO
            DO L = 1 , MAXT
              WRITE(IUNT19,2006)(PR(J,L,K)*FMULT(K) ,K=1,MAXD)
            END DO
            CALL B8WINF( IUNT19 , LADF10 , DATE   , USER    ,
     &                   NDLEV  ,
     &                   DSNINC , DSFULL , DSNEXP ,
     &                   IZ0    , IZ1    ,
     &                   IL     , NMET   , NPL    , IBSELA  ,
     &                   LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                   LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                   LSSETA , LSS04A
     &                  )
        ELSE
            I = 1
            DO 181 J = 1 , NPLR
              ISEL = ISEL + 1
              WRITE(TRANS,1006)IZ, I, METASA(I),
     &                         IZ+1, J, 'SEE '//FILMEM, 'PRTOT'
              TRANSA(ISEL)=TRANS
              WRITE(IUNT19,2000)
              WRITE(IUNT19,2002)J,0
              WRITE(IUNT19,2004)
              WRITE(IUNT19,2010)NZ,MAXT,MAXD,0
              WRITE(IUNT19,2011)Z1
              WRITE(IUNT19,2004)
              WRITE(IUNT19,2004)
              WRITE(IUNT19,2006) (DR(K) ,K=1,MAXD)
              WRITE(IUNT19,2006) (TR(K) ,K=1,MAXT)
              WRITE(IUNT19,2008) CHINDI, J,  CHINDJ, 0,  IZ1, DATE
              DO  L = 1 , MAXT
                 WRITE(IUNT19,2006)(PR(J,L,K) ,K=1, MAXD)
              END DO
              CALL B8WINF( IUNT19 , LADF10 , DATE   , USER    ,
     &                     NDLEV  ,
     &                     DSNINC , DSFULL , DSNEXP ,
     &                     IZ0    , IZ1    ,
     &                     IL     , NMET   , NPL    , IBSELA  ,
     &                     LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &                     LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &                     LSSETA , LSS04A
     &                    )
  181       CONTINUE
        ENDIF

C-----------------------------------------------------------------------
C    OUTPUT METASTABLE FRACTIONS
C-----------------------------------------------------------------------

       WRITE(IUNT23,2039)
       WRITE(IUNT23,2000)
       WRITE(IUNT23,2001)NMET
       WRITE(IUNT23,2003)(I,I=1,NMET)
       WRITE(IUNT23,2005)(CSTRGA(IMETR(I))(1:13),I=1,NMET)
       WRITE(IUNT23,2007)(IMETR(I),I=1,NMET)
       WRITE(IUNT23,2016)('*',I=1,NMET)
       WRITE(IUNT23,2017)('*',I=1,NMET)
       WRITE(IUNT23,2004)
       WRITE(IUNT23,2010)NZ,MAXT,MAXD,0
       WRITE(IUNT23,2011)Z1
       WRITE(IUNT23,2004)
       WRITE(IUNT23,2004)
       WRITE(IUNT23,2006)(DR(K),K=1,MAXD)
       WRITE(IUNT23,2006)(TR(K),K=1,MAXT)
       WRITE(IUNT23,2018)IZ1,DATE
C
       DO 96 I=1,NMET
           J = 0
           DE = 0.0D+0
           ISEL = ISEL + 1
           WRITE(TRANS,1006)IZ, I, METASA(I),
     &                      IZ, J, 'SEE '//FILMEM, 'MET'
           TRANSA(ISEL)=TRANS
           WRITE(IUNT23,2019) I
           DO  95 L = 1 , MAXT
              WRITE(IUNT23,2006)(max(dzero,STCKM(I,L,K)), K=1, MAXD)
   95      CONTINUE
   96  CONTINUE
       CALL B8WINF( IUNT23 , LADF10 , DATE   , USER    ,
     &              NDLEV  ,
     &              DSNINC , DSFULL , DSNEXP ,
     &              IZ0    , IZ1    ,
     &              IL     , NMET   , NPL    , IBSELA  ,
     &              LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &              LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &              LSSETA , LSS04A
     &             )



C-----------------------------------------------------------------------
C  COMPLETE SUMMARY FILE WITH INFORMATION LINES
C-----------------------------------------------------------------------

       CALL B8WINF( IUNIT  , LADF10 , DATE   , USER    ,
     &              NDLEV  ,
     &              DSNINC , DSFULL , DSNEXP ,
     &              IZ0    , IZ1    ,
     &              IL     , NMET   , NPL    , IBSELA  ,
     &              LRSEL  , LISEL  , LHSEL  , LIOSEL  ,
     &              LPSEL  , LZSEL  , LNSEL  , LNORM   ,
     &              LSSETA , LSS04A
     &             )
      WRITE(IUNIT,1008)

      DO 120 I = 1,ISEL
          WRITE(IUNIT,1004) I , TRANSA(I)
  120 CONTINUE
      WRITE(IUNIT,1005) DATE

C-----------------------------------------------------------------------

 1000 FORMAT(1A3,I2,'/',1A3,I2,'/',I5,'/',I5,'/DE= ',F11.0,
     &       '/TYPE = ',1A5,
     &       '/IMI= ',I1,'/IMF= ',I1,'/ISEL= ',I3)
 1001 FORMAT(1P,8E9.2)
 1002 FORMAT(' /',1A3,I2,' GENERALISED COLL. RAD. COEFFTS./'/'
     &   (to be pasted into iso-electronic master files)'/)
 1003 FORMAT('C---------------------------------------------------------
     &--------------'/'C'/'C  COEFFICIENT LIST:              '/'C'/
     &'C  SOURCE SPECIFIC ION   FILE:',A,/
     &'C  SOURCE IONIS. COEFFT. FILE:',A,/
     &'C  META. ION. COEFF. SELECTOR:',5(I3,'(',I1,',',I1,')  '))
 1004 FORMAT('C',I4,'.',3X,1A61)
 1005 FORMAT('C'/'C   NOTES:  PROCESSED ',1A8/'C'/'C--------------------
     &-----------------------------------------------------------')
 1006 FORMAT(I2,2X,I2,4X,1A12,6X,
     &       I2,2X,I2,4X,1A12,6X,1A5)
 1007 FORMAT(I2,'(',I1,')',I1,'(',F4.1,')')
 1008 FORMAT('C  POPULATION PROCESSING CODE:  ADAS208'/'C'/
     &'C  ISEL  ZI  IMI    METASTABLE       ZF  IMF    METASTABLE
     &TYPE        '/
     &'C  ----  --  ---    -----------      --  ---    -----------
     &-----       ')
 1009 FORMAT('C',29X,5(I3,'(',I1,',',I1,')  '))
C
C-----------------------------------------------------------------------
C
 2000   FORMAT(80('='))
 2001   FORMAT('NMET         = ',I2)
 2002   FORMAT('IPRT=',I2,'  TRMPRT=',4X,'  SPNPRT=',3X,
     &         '  NCRPRT=1000','  IGRD=',I2,'  SPNSYS=',3X,
     &         '  SSYSWT=',5X)
 2003   FORMAT('ORDER        = ',5I13)
 2004   FORMAT(80('-'))
 2005   FORMAT('DESIGNATION  = ',5A13)
 2006   FORMAT(1P,8D10.2)
 2007   FORMAT('COPDAT INDEX = ',5I13)
 2008   FORMAT(30('-'),'/ ',1A5, I2,'  / ',1A5,I2,'  / Z1=',I2,
     &           '   / DATE=',1A8)
 2009   FORMAT(18('-'),' /NORM= T  /''/ ',1A5, I2,'  / ',1A5,I2,
     &         '  / Z1=',I2,'   / DATE=',1A8)
 2010   FORMAT(4I5)
 2011   FORMAT(F10.5)
 2012   FORMAT('C  META. ION. COEFF. SELECTOR:',
     &          5(I3,'(',I1,',',I1,')  '))
 2013   FORMAT('C',29X,5(I3,'(',I1,',',I1,')  '))
 2014   FORMAT('C',/,
     &         'C  CODE     : ',1A7,/
     &         'C  PRODUCER : ',A30,/
     &         'C  DATE     : ',1A8)
 2015   FORMAT('C  SWITCHES: ','LNORM=',L1,2X,'LPSEL=',L1,2X,
     &                         'LZSEL=',L1,2X,'LIOSEL=',L1/
     &         'C            ','LHSEL=',L1,2X,'LRSEL=',L1,2X,
     &                         'LISEL=',L1,2X,'LNSEL=',L1)
 2016   FORMAT('PARENT REF.  = ',5(12X,A1))
 2017   FORMAT('SPNSYS REF.  = ',5(12X,A1))
 2018   FORMAT(16('-'),' METASTABLE FRACTIONS ',16('-'),
     &         '/ Z1=',I2,'   / DATE=',1A8)
 2019   FORMAT('/',I2,'/')
 2020   FORMAT(18('-'),' TOTAL LINE POWER ',18('-'),
     &         '/ Z1=',I2,'   / DATE=',1A8)
 2021   FORMAT(15('-'),' SPECIFIC LINE POWER ',7('-'),'/',F8.1,
     &         '/ Z1=',I2,'   / DATE=',1A8)
 2022   FORMAT('IGRD=',I2,'  TRMGRD=',4X,'  SPNGRD=',3X,
     &         '  NCRPRT=1000','  JGRD=',I2,'  SPNSYS=',3X,
     &         '  SSYSWT=',5X)
 2025   FORMAT('IPRT=',I2,'  TRMPRT=',4X,'  SPNPRT=',3X,
     &         '  NCTPRT=1000','  JPRT=',I2,'  SPNSYS=',3X,
     &         '  SSYSWT=',5X)
C
 2030   FORMAT('C'/'C  IONISATION DATA'/'C')
 2032   FORMAT('C'/'C  RECOMBINATION DATA'/'C')
 2033   FORMAT('C'/'C  CX RECOMBINATION DATA'/'C')
 2034   FORMAT('C'/'C  METASTABLE CROSS COUPLING DATA'/'C')
 2035   FORMAT('C'/'C  CX RECOMBINATION POWER DATA'/'C')
 2036   FORMAT('C'/'C  TOTAL LINE POWER DATA'/'C')
 2037   FORMAT('C'/'C  SPECIFIC LINE POWER DATA'/'C')
 2038   FORMAT('C'/'C  PARENT CROSS COUPLING DATA'/'C')
 2039   FORMAT('C'/'C  METASTABLE FRACTION DATA'/'C')
 2040   FORMAT('C'/'C  RECOM/CASACDE/BREMS. POWER DATA'/'C')
C
 2042   FORMAT('C '/
     &         'C  INFORMATION'/
     &         'C  -----------'/
     &         'C '/
     &         'C  ION CHARGE +1 = ',I2,5X,'NUCLEAR CHARGE = ',I2/
     &         'C  SPECIFIC ION FILE  : ',A,/
     &         'C  IONIS. COEFFT. FILE: ',A,/
     &         'C  EXPANSION FILE     : ',A)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
