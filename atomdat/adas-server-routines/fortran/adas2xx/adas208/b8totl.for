C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8totl.for,v 1.2 2007/07/20 09:46:33 allan Exp $ Date $Date: 2007/07/20 09:46:33 $
C
      SUBROUTINE B8TOTL( NDLEV   , NDMET  ,
     &                   NORD    , NMET   ,
     &                   IORDR   , IMETR  , ISTRN ,
     &                   DENSX   ,
     &                   STCKMX  , STACKX ,
     &                   PLA1    , PLBAX  ,
     &                   PLAX    , PLX    ,
     &                   PSAX    , PSX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8TOTL *********************
C
C  PURPOSE: TO CALCULATE TOTAL/SPECIFIC LINE POWERS FOR METASTABLES
C           AND TOTAL/SPECIFIC EQUILIBRIUM LINE POWERS.
C
C           DEVELOPMENT OF B6TOTL.
C
C  NOTE:    A SPECIFIC LINE IS EVALUATED WHICH TERMINATES ON EACH
C           METASTABLE.  EACH IS RESOLVED INTO THE PART DRIVEN BY EACH
C           METASTABLE.  THE EQUILIBRIUM POWER IN EACH OF THESE LINES IS
C           ALSO EVALUATED USING THE EQUILIBRIUM METASTABLE FRACTIONS.
C
C  CALLING PROGRAM:  ADAS208
C
C  SUBROUTINE:
C
C  INPUT : (I*4) NDLEV    = PARAMETER = MAX. NO. OF LEVELS ALLOWED
C  INPUT : (I*4) NDMET    = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C
C  INPUT : (I*4) NORD     = NUMBER OF ORD. LEVELS (1 <= NORD <= 'NDLEV')
C  INPUT : (I*4) NMET     = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C
C  INPUT : (I*4) IORDR()  = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                           LIST (ARRAY SIZE = 'NDLEV' )
C  INPUT : (I*4) IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C  INPUT : (I*4)  ISTRN() = SPECIFIC LINE POWER: SELECTED ELECTRON
C                           IMPACT TRANSITION INDEX. (FOR USE WITH
C                           'IE1A()' , 'IE2A()' AND 'AA()' ARRAYS)
C                           WHICH GIVES LARGEST POWER TO METASTABLE
C                            DIMENSION: METASTABLE LINE COUNT INDEX
C  INPUT : (R*8) DENSX    = ELECTRON DENSITY (UNITS: CM-3)
C
C  INPUT : (R*8) STCKMX() = METASTABLE POPULATIONS STACK
C                           AT FIXED TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE INDEX
C  INPUT : (R*4) STACKX(,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                           ON METASTABLE LEVEL. AT FIXED TEMPERATURE
C                           AND DENSITY.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C
C  INPUT : (R*8)  PLA1()  = DIRECT LINE POWER LOSS FOR EACH LEVEL.
C                           (UNITS: ERGS SEC-1) (DIMENSION: LEVEL INDEX)
C  INPUT : (R*8)  PLBAX() = HIGH N PROJECTED POWER BASED ON EXCITATIONS
C                           FROM A PARTICULAR METASTABLE TO LEVELS
C                           'IPROJ' UPWARDS. (UNITS: ERGS CM3 SEC-1)
C                           AT FIXED TEMPERATURE.
C                             DIMENSION: METASTABLE  INDEX
C
C  OUTPUT: (R*8)  PLAX    = TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                           AT FIXED TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS CM3 SEC-1)
C  OUTPUT: (R*8)  PLX()   = TOTAL LINE POWERS FOR METASTABLES. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE. AT FIXED TEMPERATURE AND DENSITY
C                           (UNITS: ERGS SEC-1 )
C                             DIMENSION: METASTABLE  INDEX
C
C  OUTPUT: (R*8)  PSAX()  = TOTAL EQUILIBRIUM LINE POWER COEFFICIENTS.
C                           AT FIXED TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS CM3 SEC-1)
C                             DIMENSION: METASTABLE  INDEX
C  OUTPUT: (R*8)  PSX(,)  = TOTAL LINE POWERS FOR METASTABLES. THIS IS
C                           THE SUM OF ALL EMISSIONS ORGINATING IN THE
C                           COLLISIONAL-RADIATIVE   SENSE   FROM   THE
C                           METASTABLE. AT FIXED TEMPERATURE AND DENSITY
C                           (UNITS: ERGS SEC-1 )
C                             1ST. DIMENSION: METASTABLE INDEX FOR LINE
C                             2ND. DIMENSION: METASTABLE INDEX OF DRIVER
C
C          (I*4) IM       = METASTABLE LEVEL ARRAY INDEX
C          (I*4) IS       = ORDINARY LEVEL ARRAY INDEX
C          (I*4) ISL      = SPECIFIC LINE POWER INDEX
C
C
C
C ROUTINES: NONE
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  20/05/93-P BRIDEN: STACKX ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C UPDATE:  24/05/96  HP SUMMERS - EXTENSION FOR SPECIFIC LINE POWER
C
C***********************************************************************
C PUT UNDER S.C.C.S CONTROL:
C
C VERSION: 1.1				DATE: 16/07/95
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER S.C.C.S
C
C VERSION: 1.2				DATE: 20/07/07
C MODIFIED: Allan Whiteford
C	    - Small modification to comments to allow for automatic
C             documentation preparation.
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDMET         , NDLEV               ,
     &           NMET          , NORD
      INTEGER    IM            , IS                  , ISL
C-----------------------------------------------------------------------
      REAL*8     DENSX         , PLAX
C-----------------------------------------------------------------------
      INTEGER    IMETR(NMET)   , IORDR(NORD)         , ISTRN(NDMET)
C-----------------------------------------------------------------------
      REAL*8     STCKMX(NDMET) ,
     &           PLA1(NDLEV)   , PLBAX(NDMET)        ,
     &           PLX(NDMET)
      REAL*8     PSAX(NDMET)   , PSX(NDMET,NDMET)
      REAL*4     STACKX(NDLEV,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      PLAX = 0.0D0
C-----------------------------------------------------------------------
         DO 1 IM=1,NMET
            PLX(IM) = PLA1(IMETR(IM)) + ( PLBAX(IM) * DENSX )
C
               DO 2 IS=1,NORD
                  PLX(IM) = PLX(IM) +
     &                      ( PLA1(IORDR(IS)) * DBLE(STACKX(IS,IM)) )
    2          CONTINUE
C
            PLX(IM) = PLX(IM)  / DENSX
            PLAX    = PLAX + ( PLX(IM) * STCKMX(IM) )
    1    CONTINUE
C-----------------------------------------------------------------------
         DO 5 ISL=1,NMET
            PSAX(ISL) = 0.0D0
            DO 4 IM=1,NMET
               PSX(ISL,IM) = 0.0D0
C
               IF(ISTRN(ISL).GT.0) THEN
                   DO 3 IS = 1, NORD
                      IF (IORDR(IS).EQ.ISTRN(ISL)) THEN
                          PSX(ISL,IM) =  PLA1(IORDR(IS)) *
     &                                   DBLE(STACKX(IS,IM))
                          PSX(ISL,IM) = PSX(ISL,IM)  / DENSX
                      ENDIF
    3              CONTINUE
                   PSAX(ISL) = PSAX(ISL) + ( PSX(ISL,IM) * STCKMX(IM) )
               ENDIF
C
    4       CONTINUE
    5    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END



