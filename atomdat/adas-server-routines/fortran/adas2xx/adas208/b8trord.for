       subroutine b8trord( ndsel  ,
     &                     ntrsel , itrlow , itrup ,
     &                     max_pec,
     &                     npec   , ilower , iupper,
     &                     ia
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: b8wvtr *********************
C
C  purpose:  Find the index of PECs ordered according to the user
C            supplied transitions.
C
C  calling program: b8wrps
C
C  subroutine:
C
C  input : (i*4)  ndsel    = maximum number of user supplied transitions
C  input : (i*4)  ntrsel   = number of user supplied transitions
C  input : (i*4)  itrlow() = lower index of transition
C  input : (i*4)  itrup()  = upper index of transition
C  input : (i*4)  max_pec  = maximum number of PECs allowed
C  input : (i*4)  npec     = number of PECs allowed
C  input : (i*4)  ilower() = lower index of transition in adf04 file
C  input : (i*4)  iupper() = upper index of transition in adf04 file
C
C  output: (i*4)  ia()    = index of PEC in user supplied order
C
C  VERSION  : 1.1
C  DATE     : 27-09-2018
C  MODIFIED :  Martin O'Mullane
C              - First version
C
C-----------------------------------------------------------------------
       integer     mpec
C-----------------------------------------------------------------------
       parameter ( mpec = 100 )
C-----------------------------------------------------------------------
       integer     ndsel          , max_pec
       integer     ntrsel         , npec             , i   , j
C-----------------------------------------------------------------------
       integer     ind_pec(mpec)
       integer     itrlow(ndsel)   , itrup(ndsel)    ,
     &             ilower(max_pec) , iupper(max_pec)
       integer     ia(*)
C-----------------------------------------------------------------------

       if (mpec.LT.max_pec) then
          write(0,1000)'Internal dimension mpec is too small'
          write(0,1001)
       endif

       do i = 1, ntrsel
          ia(i) = i
       end do

C Combine adf04 transition indices into one for comparison

       do i = 1, max_pec
          ind_pec(i) = iupper(i)*10000 + ilower(i)
       end do

       do j = 1, ntrsel
          do i = 1, npec
             if (ind_pec(i).eq.(itrup(j)*10000 + itrlow(j))) ia(j) = i
          end do
       end do

C------------------------------------------------------------------
 1000 format(1x,32('*'),' b8trord error ',32('*')//
     &       1x,'fault in input: ',a)
 1001 format(/1x,29('*'),' program terminated ',29('*'))
C------------------------------------------------------------------

       end                                                         
