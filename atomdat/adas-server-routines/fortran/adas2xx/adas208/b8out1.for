C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8out1.for,v 1.4 2004/07/06 11:30:18 whitefor Exp $ Date $Date: 2004/07/06 11:30:18 $
C
C      DEBUG SUBCHK
C      END DEBUG
      SUBROUTINE B8OUT1( IUNIT ,
     &                   NDLEV , NDTEM  , NDDEN , NDMET ,
     &                   LNORM ,
     &                   IL    , NMET   , NORD  ,
     &                   NPL   , NPLR   , NPLI  , NPL3  ,
     &                   MAXT  , MAXD   , ZEFF  ,
     &                   ICNTP , ICNTR  , ICNTI , ICNTH ,
     &                   LPSEL , LZSEL  , LIOSEL, LHSEL , LRSEL ,
     &                   LISEL ,
     &                   LMETR , IMETR  , IORDR ,
     &                   STRGA ,
     &                   LTRNG , TEA    , TEVA  , TPVA  , THVA  ,
     &                   DENSA , DENSPA , RATHA , RATPIA, RATMIA,
     &                   POPAR ,
     &                   STCKM , STVR   , STVI  , STVH  ,
     &                   STVRM , STVIM  , STVHM , STACK
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: B8OUT1 *******************
C
C  PURPOSE: OUTPUT OF MAIN RESULTS (METASTABLE POPULATIONS)
C
C  CALLING PROGRAM: ADAS20T
C
C  DATA:
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C           NEUTRAL BEAM ENERGY :
C           RATE COEFFICIENTS   : CM3 SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT FOR RESULTS
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C  INPUT : (I*4)  NPL     = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                             BY EXCITED STATE IONISATION IN COPASE
C                             FILE WITH IONISATION POTENTIALS GIVEN
C                             ON THE FIRST DATA LINE.
C  INPUT : (I*4)  NPLR    = NO. OF ACTIVE METASTABLES OF (Z+1) ION
C  INPUT : (I*4)  NPLI    = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C  INPUT : (I*4)  NPL3    = NO. OF ACTIVE METASTABLES OF (Z-1) ION WITH
C                           THREE-BODY RECOMBINATION
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 20)
C  INPUT : (R*8)  ZEFF    = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C
C  INPUT : (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C  INPUT : (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT : (I*4)  ICNTI   = NUMBER OF LOWER STAGE IONISATIONS      INPUT
C  INPUT : (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT : (L*4)  LNORM   = .TRUE.  => IF NMET=1 NORMALISE TOTAL AND
C                                      SPECIFIC LINE POWER OUTPUT FILES
C                                      PLT & PLS TO STAGE TOT. POPULATN.
C                         = .FALSE. => OTHERWISE NORMALISE TO IDENTIFIED
C                                      METASTABLE POPULATIONS.
C  INPUT : (L*4)  LPSEL   = .TRUE.  => INCLUDE PROTON COLLISIONS
C                         = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  INPUT : (L*4)  LZSEL   = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                      PLASMA Z EFFECTIVE'ZEFF'.
C                         = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                      WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                         (ONLY USED IF 'LPSEL=.TRUE.')
C  INPUT : (L*4)  LIOSEL  = .TRUE.  => INCLUDE EXCITED STATE IONISATION
C                         = .FALSE. => DO NOT INCLUDE EXC. STATE IONIS.
C  INPUT : (L*4)  LHSEL   = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                      NEUTRAL HYDROGREN.
C                         = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                      FROM NEUTRAL HYDROGREN.
C  INPUT : (L*4)  LRSEL   = .TRUE.  => INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C  INPUT : (L*4)  LISEL   = .TRUE.  => INCLUDE ELECTRON IMPACT
C                                      IONISATION CONTRIBUTIONS
C                         = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                      RECOMBINATION.
C
C  INPUT : (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                      'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET')
C  INPUT : (I*4)  IORDR() = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                           LIST.
C
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C
C  INPUT : (L*4)  LTRNG(,)= .TRUE.  => TEMPERATURE VALUE WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                         = .FALSE. =>TEMPERATURE VALUE NOT WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                           1st DIMENSION: TEMPERATURE INDEX.
C                           2nd DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (R*8)  TEVA()  = ELECTRON TEMPERATURES (UNITS: EV)
C  INPUT : (R*8)  TPVA()  = PROTON TEMPERATURES   (UNITS: EV)
C  INPUT : (R*8)  THVA()  = NEUTRAL HYDROGEN TEMPERATURES   (UNITS: EV)
C
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT : (R*8)  DENSPA()= PROTON DENSITIES    (UNITS: CM-3)
C  INPUT : (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  INPUT : (R*8)  RATPIA(,) = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                                1ST DIMENSION: DENSITY INDEX
C                                2ND DIMENSION: PARENT INDEX
C  INPUT : (R*8)  RATMIA(,) = RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                                1ST DIMENSION: DENSITY INDEX
C                                2ND DIMENSION: PARENT INDEX
C  INPUT : (R*8)  POPAR(,,) =  LEVEL POPULATIONS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C  INPUT : (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*4) STVR(,,,) = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVI(,,,) = ELECTRON IMPACT IONISATION  COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVH(,,,) =  CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C
C  INPUT : (R*8) STVRM(,,,)= METASTABLE FREE ELECTRON RECOMBINATION
C                            COEFFICIENTS.
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*8) STVIM(,,,)= METASTABLE ELECTRON IMPACT IONISATION
C                            COEFFICIENTS.
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*8) STVHM(,,,)= METASTABLE CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STACK(,,,)= POPULATION DEPENDENCE
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C
C          (I*4)  PGLEN   = PARAMETER = NUMBER OF LINES PER OUTPUT PAGE
C
C          (I*4)  NBLOCK  = NUMBER OF LINES IN CURRENT OUTPUT BLOCK.
C          (I*4)  NLINES  = LAST PAGE LINE WRITTEN.
C                           IF 'NLINES+NBLOCK' > 'PGLEN' START NEW PAGE.
C          (I*4)  MIND    = MINIMUM OF 10 AND 'MAXD'
C          (I*4)  I       = GENERAL USE
C          (I*4)  IP      = GENERAL USE
C          (I*4)  J       = GENERAL USE
C          (I*4)  IT      = TEMPERATURE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IN      = DENSITY INDEX NUMBER FOR ARRAY USE
C          (I*4)  IUSEP   = NUMBER OF PROTON IMPACT TRANSITIONS USED
C          (I*4)  IUSER   = NO. OF STATE SELECTIVE RECOM. OF (Z+1) USED
C          (I*4)  IUSEI   = NO. OF STATE SELECTIVE IONIS. BY (Z-1) USED
C          (I*4)  IUSEH   = NO. OF STATE SELECTIVE CX BY (Z+1)     USED
C
C          (L*4)  LPRNG   = .TRUE.  => PROTON INPUT PARAMETERS USED
C                           .FALSE. => PROTON INPUT PARAMETERS NOT USED
C          (L*4)  LHRNG   = .TRUE.  => NEUTRAL H INPUT PARAMETERS USED
C                           .FALSE. => NEUTRAL H INPUT PARMS. NOT USED
C          (L*4)  LRRNG   = .TRUE.  => FREE ELEC. RECOMB. PARMS USED
C                           .FALSE. => FREE ELEC. RECOMB. PARMS NOT USED
C          (L*4)  LIRNG   = .TRUE.  => LOWER STAGE IONIS. PARMS USED
C                           .FALSE. => FREE ELEC. RECOMB. PARMS NOT USED
C
C          (C*32) C32     = GENERAL USE 32 BYTE CHARACTER STRING
C
C          (C*1)  CTRNG(7)= ' ' => OUTPUT VALUES FOR THIS TEMPERATURE
C                                  INTERPOLATED.
C                         = '*' => OUTPUT VALUES FOR THIS TEMPERATURE
C                                  EXTRAPOLATED.
C                         = '#' => NOT USED
C                           1st DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C                                          DENSITY TYPE -
C                                          4) => PROTON
C                                          RATIO TYPE -
C                                          5) => 'RATHA'
C                                          6) => 'RATPIA'
C                                          7) => 'RATMIA'
C
C NOTE:
C          ONLY THE FIRST TEN DENSITIES ARE OUTPUT.
C
C          AN 'OUTPUT BLOCK' IS A SINGLE CONTAINED OUTPUT TABLE
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSTNP     ADAS      STARTS NEW PAGE IF CURRENT PAGE FULL
C
C
C AUTHOR:  H P SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/01/92
C
C UPDATE:  12/06/92  HP SUMMERS - EXTENSION TO MULTIPLE PARENTS AND
C                                 INNER SHELL IONISATION CONTRIBUTIONS
C                                 MODIFICATIONS TO MAKE COMPATIBLE
C                                 WITH B8DATA
C
C UNIX-IDL PORT:
C
C DATE: UNKNOWN
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C VERSION: 1.2				DATE: 13/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - REMOVED OUTDATED HOLERITH CONSTANTS: 1H1, 1H0
C 
C VERSION: 1.3				DATE: 05/07/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - REMOVED MORE OUTDATED HOLERITH CONSTANTS
C 
C VERSION: 1.4				DATE: 24/09/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - REMOVED SOME 1H HOLERITH CONSTANTS TO MAKE OUTPUT MORE
C             UNIFORM
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   PGLEN
C-----------------------------------------------------------------------
      PARAMETER ( PGLEN = 63 )
C-----------------------------------------------------------------------
      INTEGER   IUNIT     ,
     &          NDLEV     , NDTEM     , NDDEN     , NDMET
      INTEGER   IL        ,
     &          NMET      , NORD      ,
     &          NPL       , NPLR      , NPLI      , NPL3      ,
     &          MAXT      , MAXD      ,
     &          ICNTP     , ICNTR     , ICNTI     , ICNTH
      INTEGER   NBLOCK    , NLINES    , MIND      ,
     &          I         , IP        , J         , IN        , IT    ,
     &          IUSEP     , IUSER     , IUSEI     , IUSEH
C-----------------------------------------------------------------------
      REAL*8    ZEFF
C-----------------------------------------------------------------------
      LOGICAL   LNORM
      LOGICAL   LPSEL     , LZSEL     , LIOSEL    , LHSEL   , LRSEL
      LOGICAL   LISEL     , LPRNG     , LHRNG     , LRRNG   , LIRNG
C-----------------------------------------------------------------------
      CHARACTER C32*32
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)    , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8    TEVA(NDTEM)     , TPVA(NDTEM)      ,
     &          THVA(NDTEM)     , TEA(NDTEM)       ,
     &          DENSA(NDDEN)    , DENSPA(NDDEN)    ,
     &          RATHA(NDDEN)    , RATPIA(NDDEN,NDMET) ,
     &          RATMIA(NDDEN,NDMET)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
      REAL*8    STCKM(NDMET,NDTEM,NDDEN)
      REAL*8    STVRM(NDMET,NDTEM,NDDEN,NDMET) ,
     &          STVIM(NDMET,NDTEM,NDDEN,NDMET)
      REAL*8    STVHM(NDMET,NDTEM,NDDEN,NDMET)
C-----------------------------------------------------------------------
      REAL*4    STVR(NDLEV,NDTEM,NDDEN,NDMET) ,
     &          STVI(NDLEV,NDTEM,NDDEN,NDMET) ,
     &          STVH(NDLEV,NDTEM,NDDEN,NDMET)
      REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NDMET)    , LTRNG(NDTEM,3)
C-----------------------------------------------------------------------
      CHARACTER CTRNG(7)*1      , STRGA(NDLEV)*22
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      MIND=MIN0(10,MAXD)
C-----------------------------------------------------------------------
      WRITE(IUNIT,1000) NORD , NMET
C-----------------------------------------------------------------------
         DO 1 I=1,NMET
            IF ( LMETR(I) ) THEN
               WRITE(IUNIT,1001) I , IMETR(I) , STRGA(IMETR(I))
            ELSE
               WRITE(IUNIT,1002) I , IMETR(I) , STRGA(IMETR(I))
            ENDIF
    1    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT TEMPERATURES, DENSITIES AND RATIOS
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1003) MAXT , MAXD
      WRITE(IUNIT,1004)
      WRITE(IUNIT,1005)
C
      CTRNG(2) = '#'
      CTRNG(3) = '#'
      CTRNG(4) = '#'
      CTRNG(5) = '#'
      CTRNG(6) = '#'
      CTRNG(7) = '#'
C
      LPRNG = LPSEL .AND. (ICNTP.GT.0)
      LHRNG = LHSEL .AND. (ICNTH.GT.0)
      LRRNG = LRSEL .AND. (ICNTR.GT.0)
      LIRNG = LISEL .AND. (ICNTI.GT.0)
C
      IF (LPRNG)                    CTRNG(4) = ' '
      IF (LHRNG)                    CTRNG(5) = ' '
      IF (LRRNG)                    CTRNG(6) = ' '
      IF (LIRNG)                    CTRNG(7) = ' '
C
         DO 2 IT=1,MAXT
C
               IF (LTRNG(IT,1)) THEN
                  CTRNG(1) = ' '
               ELSE
                  CTRNG(1) = '*'
               ENDIF
C
               IF (LPRNG) THEN
                  IF (LTRNG(IT,2)) THEN
                     CTRNG(2) = ' '
                  ELSE
                     CTRNG(2) = '*'
                  ENDIF
               ENDIF
C
               IF (LHRNG) THEN
                  IF (LTRNG(IT,3)) THEN
                     CTRNG(3) = ' '
                  ELSE
                     CTRNG(3) = '*'
                  ENDIF
               ENDIF
C
               IF (IT.LE.MAXD) THEN
                  WRITE(IUNIT,1006) IT , TEVA(IT)    , CTRNG(1)   ,
     &                                   TPVA(IT)    , CTRNG(2)   ,
     &                                   THVA(IT)    , CTRNG(3)   ,
     &                              IT , DENSA(IT)   ,
     &                                   DENSPA(IT)  , CTRNG(4)
               ELSE
                  WRITE(IUNIT,1007) IT , TEVA(IT)  , CTRNG(1)   ,
     &                                   TPVA(IT)  , CTRNG(2)   ,
     &                                   THVA(IT)  , CTRNG(3)
               ENDIF
    2    CONTINUE
C
         IF (MAXD.GT.MAXT) THEN
            DO 3 IN=MAXT+1,MAXD
               WRITE(IUNIT,1008) IN , DENSA(IN)   ,
     &                                DENSPA(IN)  , CTRNG(4)
    3       CONTINUE
         ENDIF
C
      WRITE(IUNIT,1005)
      WRITE(IUNIT,1009)
      WRITE(IUNIT,1005)
      WRITE(IUNIT,'(/)')
C
      WRITE(IUNIT,1033)
      WRITE(IUNIT,1034)
      WRITE(IUNIT,1035)
      DO 33 IN=1,MAXD
        WRITE(IUNIT,1037) IN ,
     &                         (RATPIA(IN,IP),CTRNG(6),IP=1,NPLR)
        WRITE(IUNIT,1038)       RATHA(IN)    , CTRNG(5),
     &                         (RATMIA(IN,IP),CTRNG(7),IP=1,NPLI)
   33 CONTINUE
C
      WRITE(IUNIT,1035)
      WRITE(IUNIT,1039)
      WRITE(IUNIT,1035)
      WRITE(IUNIT,'(/)')
C
C-----------------------------------------------------------------------
C OUTPUT SELECTED OPTIONS
C-----------------------------------------------------------------------
C
      C32 = 'PROTON IMPACT COLLISIONS'
         IF (LPSEL) THEN
            IUSEP = ICNTP
               IF (LZSEL) THEN
                  WRITE(IUNIT,1012) C32 , ZEFF
               ELSE
                  WRITE(IUNIT,1013) C32
               ENDIF
         ELSE
            IUSEP = 0
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'EXCITED STATE IONIZATION RATES'
         IF (LIOSEL) THEN
            WRITE(IUNIT,1010) C32
         ELSE
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'STATE SELECTIVE CX BY (Z+1)'
         IF (LHSEL) THEN
            IUSEH = ICNTH
            WRITE(IUNIT,1010) C32
         ELSE
            IUSEH = 0
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'STATE SELECTIVE RECOM. BY (Z+1)'
         IF (LRSEL) THEN
            IUSER = ICNTR
            WRITE(IUNIT,1010) C32
         ELSE
            IUSER = 0
            WRITE(IUNIT,1011) C32
         ENDIF
C
      C32 = 'STATE SELECTIVE IONIS. OF (Z-1)'
         IF (LISEL) THEN
            IUSEI = ICNTI
            WRITE(IUNIT,1010) C32
         ELSE
            IUSEI = 0
            WRITE(IUNIT,1011) C32
         ENDIF
      C32 = 'COEFF. NORMAL. TO STAGE TOTALS '
         IF (LNORM.AND.(NMET.EQ.1)) THEN
            WRITE(IUNIT,1010) C32
         ELSE
            WRITE(IUNIT,1011) C32
         ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT TABLE KEY:
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1014) IUSEP , IUSER , IUSEI , IUSEH
      IF (MAXD.GT.10) WRITE(IUNIT,1015)
C-----------------------------------------------------------------------
          DO 4 IT=1,MAXT
             WRITE(IUNIT,1016) ( DENSA(I) , I=1,MIND )
             WRITE(IUNIT,1017) TEVA(IT) , TEA(IT)
                DO 5 I=1,NMET
                   WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                       (STCKM(I,IT,IN),IN=1,MIND)
    5           CONTINUE
             NLINES=5+NMET
C
C-----------------------------------------------------------------------
C  WRITE OUT RECOMB., IONIS. AND CHARGE EXCHANGE DATA (IF ANY)
C-----------------------------------------------------------------------
C
                IF (NMET.GT.1) THEN
                   IF(LIOSEL)THEN
                      DO 58 IP=1,NPL3
                       NBLOCK=NMET+1
                       CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                       WRITE(IUNIT,1019)
     &                         'METASTABLE STATE SELECTIVE RECOM. FROM',
     &                         ' (Z+1) STATE', IP
                          DO 57 I=2,NMET
                             WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                    (STVRM(I,IT,IN,IP),IN=1,MIND)
   57                     CONTINUE
   58                 CONTINUE
                   ELSEIF((.NOT.LIOSEL).AND.LRSEL.AND.(ICNTR.GT.0))THEN
                      DO 50 IP=1,NPLR
                       NBLOCK=NMET+1
                       CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                       WRITE(IUNIT,1019)
     &                         'METASTABLE STATE SELECTIVE RECOM. FROM',
     &                         ' (Z+1) STATE', IP
                          DO 6 I=2,NMET
                             WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                    (STVRM(I,IT,IN,IP),IN=1,MIND)
    6                     CONTINUE
   50                 CONTINUE
                   ENDIF
C
                   IF ( LISEL .AND. (ICNTI.GT.0) ) THEN
                      DO 51 IP=1,NPLI
                       NBLOCK=NMET+1
                       CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                       WRITE(IUNIT,1019)
     &                         'METASTABLE STATE SELECTIVE IONIS. FROM',
     &                         ' (Z-1) STATE', IP
                          DO 16 I=2,NMET
                             WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                    (STVIM(I,IT,IN,IP),IN=1,MIND)
   16                     CONTINUE
   51                 CONTINUE
                   ENDIF
C
                   IF ( LHSEL .AND. (ICNTH.GT.0) ) THEN
                      DO 52 IP=1,NPLR
                       NBLOCK=NMET+1
                       CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                       WRITE(IUNIT,1019)
     &                         'METASTABLE STATE SELECTIVE CX.    FROM',
     &                         ' (Z+1) STATE', IP
                          DO 7 I=2,NMET
                             WRITE(IUNIT,1018) I , IMETR(I) ,
     &                                   (STVHM(I,IT,IN,IP),IN=1,MIND)
    7                     CONTINUE
   52                 CONTINUE
                   ENDIF
                ENDIF
C-----------------------------------------------------------------------
             NBLOCK=1
             CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
             WRITE(IUNIT,'()')
                DO 8 I=1,NMET
                   NBLOCK=NORD+3
                   CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                   WRITE(IUNIT,1020) I
                      DO 9  J=1,NORD
                         WRITE(IUNIT,1018) J , IORDR(J) ,
     &                                     (STACK(J,I,IT,IN),IN=1,MIND)
    9                 CONTINUE
    8           CONTINUE
C
C-----------------------------------------------------------------------
C  WRITE OUT RECOMB., IONIS. AND CHARGE EXCHANGE DATA IF ANY
C-----------------------------------------------------------------------
C
                IF(LIOSEL)THEN
                   DO 60 IP=1,NPL3
                    NBLOCK=NORD+2
                    CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                    WRITE(IUNIT,1019)
     &                         'ORDINARY STATE SELECTIVE RECOM. FROM',
     &                         ' (Z+1) STATE', IP
                       DO 59 I=1,NORD
                          WRITE(IUNIT,1018) I , IORDR(I) ,
     &                                     (STVR(I,IT,IN,IP),IN=1,MIND)
   59                  CONTINUE
   60              CONTINUE
                ELSEIF((.NOT.LIOSEL).AND.LRSEL.AND.(ICNTR.GT.0))THEN
                   DO 53 IP=1,NPLR
                    NBLOCK=NORD+2
                    CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                    WRITE(IUNIT,1019)
     &                         'ORDINARY STATE SELECTIVE RECOM. FROM',
     &                         ' (Z+1) STATE', IP
                       DO 10 I=1,NORD
                          WRITE(IUNIT,1018) I , IORDR(I) ,
     &                                     (STVR(I,IT,IN,IP),IN=1,MIND)
   10                  CONTINUE
   53              CONTINUE
                ENDIF
C
                IF ( LISEL .AND. (ICNTI.GT.0) ) THEN
                   DO 54 IP=1,NPLI
                    NBLOCK=NORD+2
                    CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                    WRITE(IUNIT,1019)
     &                         'ORDINARY STATE SELECTIVE IONIS. FROM',
     &                         ' (Z-1) STATE', IP
                       DO 20 I=1,NORD
                          WRITE(IUNIT,1018) I , IORDR(I) ,
     &                                     (STVI(I,IT,IN,IP),IN=1,MIND)
   20                  CONTINUE
   54              CONTINUE
                ENDIF
C
                IF ( LHSEL .AND. (ICNTH.GT.0) ) THEN
                   DO 55 IP=1,NPLR
                    NBLOCK=NORD+2
                    CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
                    WRITE(IUNIT,1019)
     &                         'ORDINARY STATE SELECTIVE CX.    FROM',
     &                         ' (Z+1) STATE', IP
                       DO 11 I=1,NORD
                          WRITE(IUNIT,1018) I , IORDR(I) ,
     &                                     (STVH(I,IT,IN,IP),IN=1,MIND)
   11                  CONTINUE
   55              CONTINUE
                ENDIF
    4     CONTINUE
C
C-----------------------------------------------------------------------
C  WRITE OUT THE POPULATIONS.
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1021)
      NLINES=1
         DO 12 I=1,IL
            NBLOCK=MAXT+7
            CALL XXSTNP ( IUNIT , PGLEN , NBLOCK , NLINES )
            WRITE(IUNIT,1022) I
            WRITE(IUNIT,1023) ( DENSA(IN) , IN=1,MIND )
            WRITE(IUNIT,1024)
               DO 13 IT=1,MAXT
                  WRITE(IUNIT,1025) TEVA(IT),(POPAR(I,IT,IN), IN=1,MIND)
   13          CONTINUE
   12    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/ / / / ,'METASTABLE INFORMATION:'/23('-')/
     &        / ,'NUMBER OF ORDINARY LEVELS',8X,'=',1X,I3/
     &       ,'NUMBER OF METASTABLES'   ,12X,'=',2X,I2/
     &        / ,16('-'),' METASTABLE DETAILS ',16('-')/
     &       1H ,'METASTABLE',4X,'ENERGY LEVEL',4X,
     &           4('-'),' DESIGNATION ',5('-')/
     &        3X,'INDEX',10X,'INDEX'/1X,52('-'))
 1001 FORMAT(1H ,3X,I2,12X,I3,10X,1A22)
 1002 FORMAT(1H ,I4,7X,I4,12X,1A22,4X,
     & '***** NO ELECTRON IMPACT TRANSITIONS EXIST TO THIS LEVEL *****')
 1003 FORMAT( / / / ,'OUTPUT PLASMA PARAMETERS:'/,25('-')//
     &       'NUMBER OF TEMPERATURES     = ',I2/
     &       'NUMBER OF DENSITIES/RATIOS = ',I2)
 1004 FORMAT( / ,'INDEX',3X,9('-'),' TEMPERATURES (UNITS: eV) ',9('-'),
     &       10X,'INDEX',4X,'DENSITIES (UNITS: CM-3)',6X/
     &       11X,'ELECTRON',8X,'PROTON',4X,'NEUTRAL HYDROGEN',20X,
     &           'ELECTRON',7X,'PROTON',11X/
     &       13X,'<TE>',11X,'<TP>',11X,'<TH>',28X,'<NE>',10X,'<NP>')
 1005 FORMAT(52('-'),10X,38('-'))
 1006 FORMAT( 2X,I2,5X,1P,3(D11.3,1X,A1,2X),
     &       10X,I2,2X,   2(3X,D11.3),1X,A1)
 1007 FORMAT( 2X,I2,5X,1P,3(D11.3,1X,A1,2X))
 1008 FORMAT(64X,I2,2X,1P,2(3X,D11.3),1X,A1)
 1009 FORMAT('KEY: * = WARNING - TEMPERATURE OUT OF RANGE'/
     &       17X,'- EXTRAPOLATION REQUIRED'/
     &       5X,'# = PARAMETER NOT USED IN CALCULATIONS')
 1010 FORMAT(A32,1X,'- INCLUDED    ')
 1011 FORMAT(A32,1X,'- NOT INCLUDED')
 1012 FORMAT(A32,1X,'- INCLUDED    ',
     &                 ' - SCALED WITH PLASMA Z-EFFECTIVE (=',F5.1,')')
 1013 FORMAT(A32,1X,'- INCLUDED    ',
     &                 ' - NOT SCALED WITH PLASMA Z-EFFECTIVE')
 1014 FORMAT(/ / / / ,'TABLE KEY:'/10('-')/
     &     / ,'NE    = ELECTRON DENSITY'/
     &    'TE    = ELECTRON TEMPERATURE'/
     &     / ,'I     = ENERGY   LEVEL INDEX'/
     &    'IMET  = METASTABLE     INDEX'/
     &    'IORD  = ORDINARY LEVEL INDEX'/ / /
     &     / ,'NUMBER OF PROTON IMPACT COLLISIONS        INCLUDED =',I5/
     &    'NUMBER OF STATE SELECTIVE RECOM. BY (Z+1) INCLUDED =',I5/
     &    'NUMBER OF STATE SELECTIVE IONIS. BY (Z-1) INCLUDED =',I5/
     &    'NUMBER OF STATE SELECTIVE CX BY (Z+1)     INCLUDED =',I5/
     &   )
 1015 FORMAT( / ,'NOTE: OUTPUT RESTRICTED TO FIRST TEN DENSITIES ONLY')
 1016 FORMAT(/ / ,'NE(CM-3)=',1P,10E12.4)
 1017 FORMAT(132('-')/
     &        / ,'EQUILIBRIUM METASTABLE POPULATION DEPENDENCE ON ',
     &       'DENSITY AT TE =' ,1P,E10.2,' EV  = ',E10.2,' KELVIN'/
     &       2X,'IMET',2X,'I')
 1018 FORMAT(2I5,1P,10E12.4)
 1019 FORMAT( / ,A,A,I2)
 1020 FORMAT( / ,'POPULATION DEPENDENCE ON DENSITY AND METASTABLE',
     &       I3/'  IORD  I')
 1021 FORMAT(/ / ,53('*'),' EQUILIBRIUM  POPULATIONS ',53('*'))
 1022 FORMAT( / /' LEVEL = ',I2,' - EQUILIBRIUM POPULATION',/1X,35('-'))
 1023 FORMAT( / ,'NE (CM-3) ',3X,1P,10D10.2)
 1024 FORMAT('TE (EV)',4X,103('-'))
 1025 FORMAT(1H ,1P,D10.2,' | ',1P,10D10.2)
C
 1033 FORMAT( / ,'OUTPUT PLASMA PARAMETERS FOR EQUILIBRIUM POPULATIONS:'
     &      /,53('-'))
 1034 FORMAT( / ,'INDEX',3X,9('-'),' RATIOS N(Z+1,M)/N(Z,1) ',15('-'),
     &       ' NH/NE ',14('-'),' RATIOS N(Z-1,M)/N(Z,1) ',11('-')/
     &       13X,'M=1',12X,'M=2',12X,'M=3',
     &       27X,'M=1',12X,'M=2',12X,'M=3')
 1035 FORMAT(112('-'))
 1037 FORMAT( 2X,I2,5X,1P,3(D11.3,1X,A1,2X))
 1038 FORMAT(1H+,53X,1P,4(D11.3,1X,A1,2X))
 1039 FORMAT('NOTE: NH/NE',11X,
     &           '= NEUTRAL HYDROGEN DENSITY / ELECTRON DENSITY'/
     &       ,6X,'N(Z+1,M)/N(Z,1) = ABUNDANCES OF (Z+1) METASTABLES'/
     &       ,6X,'N(Z-1,M)/N(Z,1) = ABUNDANCES OF (Z-1) METASTABLES')
C
C-----------------------------------------------------------------------
C
      RETURN
      END
