C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8spln.for,v 1.3 2004/07/06 11:31:22 whitefor Exp $ Date $Date: 2004/07/06 11:31:22 $
C
      SUBROUTINE B8SPLN( NTDIM , NDDIM ,
     &                   ITA    , IDA    , ITVAL   , IDVAL  ,
     &                   TETA   , TEDA   , TOUT    , DOUT   ,
     &                   CINA   ,          COUTA   ,
     &                                     LTRNG   , LDRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8SPLN *******************  **
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE AND DENSITY)
C          VERSUS LOG(COLLISIONAL-RADIATIVE MATRIX COEFFICIENTS)
C          INPUT DATA
C
C          USING  TWO-WAY SPLINES IT CALCULATES  THE  INTERPOL. COEFFTS.
C          FOR  'ITVAL' ELECTRON TEMPERATURES  AND  'IDVAL' DENSITIES
C          FROM THE TWO-DIMENSIONAL TABLE OF TEMPERATURES/DENSITIES READ
C          IN FROM THE INPUT FILE. IF A  VALUE  CANNOT  BE  INTERPOLATED
C          USING SPLINES IT IS EXTRAPOLATED VIA 'XXSPLE'.
C
C  CALLING PROGRAM: ADAS208/B8GETP
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NTDIM   = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDIM   = MAX NUMBER OF ELECTRON DENSITIES    ALLOWED
C
C  INPUT : (I*4)  ITA     = INPUT DATA  : NUMBER OF ELECTRON TEMPERA-
C                           TURES
C  INPUT : (I*4)  IDA     = INPUT DATA  : NUMBER OF ELECTRON DENSIT-
C                           IES
C  INPUT : (I*4)  ITVAL   = OUTPUT DATA : NUMBER OF TEMPERATURES
C  INPUT : (I*4)  IDVAL   = OUTPUT DATA : NUMBER OF DENSITIES
C
C  INPUT : (R*8)  TETA()  = INPUT DATA  : ELECTRON TEMPERATURES (K)
C  INPUT : (R*8)  TEDA()  = INPUT DATA  : ELECTRON DENSITIES (CM-3)
C  INPUT : (R*8)  TOUT()  = OUTPUT DATA : ELECTRON TEMPERATURES (K)
C  INPUT : (R*8)  DOUT()  = OUTPUT DATA : ELECTRON DENSITIES (CM-3)
C
C
C  INPUT : (R*8)  CINA(,)  =INPUT DATA FILE: FULL SET OF COLL. RAD.
C                           COEFFICIENTS FOR THE DATA-BLOCK BEING
C                           ANALYSED.
C                           1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2ND DIMENSION: ELECTRON DENSITY     INDEX
C  OUTPUT: (R*8)  COUTA(,)= SPLINE INTERPOLATED COLL. RAD. CEOFFICIENTS
C                           THE USER ENTERED TEMPERATURES AND DENSITIES
C                           1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2ND DIMENSION: ELECTRON DENSITY     INDEX
C
C  OUTPUT: (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'COUTA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TOUT'()'.
C                           .FALSE. => OUTPUT 'COUTA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TOUT'()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (L*4)  LDRNG()=  .TRUE.  => OUTPUT 'COUTA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DOUT()'.
C                           .FALSE. => OUTPUT 'COUTA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DOUT()'.
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF INPUT  TEMP/DENSITY
C                                       VALUES. MUST BE >= 'ITA'&'IDA'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF OUTPUT TEMP/DENSITY
C                                       PAIRS.  MUST BE >= 'ITVAL'
C          (I*4)  IED     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           DENSITIES.
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           TEMPERATURES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           TEMPERATURES.
C          (I*4)  IN      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           DENSITIES.
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTOUT'E 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATOUT'G
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATOUT'G TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTOUT'ES SECTION BELOW)
C
C          (R*8)  XIN()   = 1) LOG( DATA FILE ELECTRON DENSITIES    )
C                           2) LOG( DATA FILE ELECTRON TEMPERATURES )
C          (R*8)  YIN()   = LOG( INPUT COLL. RAD COEFFTS.)
C          (R*8)  XOUT()  = 1) LOG( SCALED USER ENTERED ELECTRON DENS. )
C                           2) LOG( SCALED USER ENTERED ELECTRON TEMPS.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED IONIZATIONS/PHOTON )
C          (R*8)  YPASS(,)= LOG( COL. RAD. COEFFTS.) INTERMEDIATE ARRAY
C                           WHICH   STORES   INTERPOLATED/EXTRAPOLATED
C                           VALUES  BETWEEN  THE  TWO  SPLINE SECTIONS.
C                           SECTIONS.
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C ROUTOUT'ES:
C          ROUTOUT'E  SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTOUT'E (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H.P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/07/92
C
C
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C VERSION: 1.2				DATE: 23/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - INCREASED NOUT TO 35
C 
C VERSION: 1.3				DATE: 30/09/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - INCREASED NIN TO 35
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN                   , NOUT
C-----------------------------------------------------------------------
      PARAMETER( NIN =35              , NOUT = 35        )
C-----------------------------------------------------------------------
      INTEGER    NTDIM                 , NDDIM           ,
     &           ITA                   , IDA             , ITVAL
      INTEGER    IET                   , IED             , IT         ,
     &           IOPT                  , IN              , IDVAL
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TETA(ITA)             , TEDA(IDA)       ,
     &           TOUT(ITVAL)           , DOUT(IDVAL)     ,
     &           COUTA(NTDIM,NDDIM)    ,
     &           CINA(NTDIM,NDDIM)
      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)      ,
     &                                   YPASS(NOUT,NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)          , LDRNG(IDVAL)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.IDA)
     &             STOP ' B8SPLN ERROR: NIN < IDA - INCREASE NIN'
      IF (NIN.LT.ITA)
     &             STOP ' B8SPLN ERROR: NIN < ITA - INCREASE NIN'
      IF (NOUT.LT.ITVAL)
     &             STOP ' B8SPLN ERROR: NOUT < ITVAL - INCREASE NTOUT'
C
C-----------------------------------------------------------------------
C SET UP FIRST SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = 0
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------
C
         DO 1 IET=1,ITA
            XIN(IET) = DLOG( TETA(IET) )
    1    CONTINUE
C
         DO 2 IT=1,ITVAL
            XOUT(IT) = DLOG( TOUT(IT) )
    2    CONTINUE
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL DATASET ELECTRON DENSITIES FOR EACH USER ELECTRON TEMP
C-----------------------------------------------------------------------
C
         DO 3 IED=1,IDA
C
               DO 4 IET=1,ITA
                  YIN(IET) = DLOG( CINA(IET,IED) )
    4          CONTINUE
C
            CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &                   ITA   , XIN     , YIN    ,
     &                   ITVAL , XOUT    , YOUT   ,
     &                   DF    , LTRNG
     &                 )
C
               DO 5 IT=1,ITVAL
                  YPASS(IT,IED) = YOUT(IT)
    5          CONTINUE
C
    3    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP SECOND SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = 0
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON DENSITIES
C-----------------------------------------------------------------------
C
         DO 6 IED=1,IDA
            XIN(IED) = DLOG( TEDA(IED) )
    6    CONTINUE
C
         DO 7 IN=1,IDVAL
            XOUT(IN) = DLOG( DOUT(IN)  )
    7    CONTINUE
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL USER DENSITIES FOR EACH USER TEMPERATURE
C-----------------------------------------------------------------------
C
         DO 12 IT=1,ITVAL
C
               DO 9 IED=1,IDA
                  YIN(IED) = YPASS(IT,IED)
    9          CONTINUE
C
            CALL XXSPLE( LSETX , IOPT       , R8FUN1   ,
     &                   IDA   , XIN        , YIN      ,
     &                   IDVAL , XOUT       , YOUT     ,
     &                   DF    , LDRNG
     &                 )
C
C-----------------------------------------------------------------------
C SET UP OUTPUT COEFFICIENTS
C-----------------------------------------------------------------------
C
         DO 10 IN=1,IDVAL
            COUTA(IT,IN) = DEXP( YOUT(IN) )
   10    CONTINUE
C
   12    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
