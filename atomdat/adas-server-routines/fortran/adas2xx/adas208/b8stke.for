C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8stke.for,v 1.1 2004/07/06 11:31:32 whitefor Exp $ Date $Date: 2004/07/06 11:31:32 $
C
      SUBROUTINE B8STKE( NDTEM  , NDLEV  , NDMET  ,
     &                   IT     , NORD   ,
     &                            IORDR  ,
     &                   DENS   ,
     &                   CMAT   , VEC    , V3     ,
     &                   IP     ,
     &                   STV
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8STKE *********************
C
C  PURPOSE: TO STACK UP IN  'STV'  THE  RECOMBINATION CONTRIBUTION  FOR
C           EACH  NON-METASTABLE/ORDINARY  EXCITED  LEVEL  FOR  A GIVEN
C           TEMPERATURE AND DENSITY, BUT ADDING A THREE-BODY
C           RECOMBINATION PART FROM V3
C
C  CALLING PROGRAM:  ADAS208
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT :  (I*4)  IT      = INDEX DENOTING THE TEMPERATURE
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (R*8)  DENS    = ELECTRON DENSITY (CM-3)
C  INPUT :  (R*8)  CMAT(,) = INVERTED   RATE   MATRIX   COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS
C                            TRANSITIONS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C  INPUT :  (I*4)  IP      = PARENT INDEX
C
C  INPUT :  (R*8)  VEC(,,) = RECOMBINATION RATE COEFFT. VALUES.
C                            (UNITS: CM**3/SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: TEMPERATURE INDEX ('IT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX
C                            3rd DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  V3(,)   = THREE-BODY RECOMB. RATE COEFFT. VALUES.
C                            (UNITS: CM**6/SEC-1)
C                            VALUES FOR A SPECIFIED TEMPERATURE.
C                            1ST DIMENSION: CAPTURING LEVEL INDEX
C                            2ND DIMENSION: PARENT INDEX
C
C  OUTPUT:  (R*4)  STV()   = RECOMBINATION  CONTRIBUTION  FOR  EACH
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: CM**3)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: ORDINARY EXCITED LEVEL INDEX
C
C           (I*4)  IS1     = ORDINARY EXCITED LEVEL INDEX
C           (I*4)  IS2     = ORDINARY EXCITED LEVEL INDEX
C
C           (R*8)  COEF    = VARIABLE USED TO SUM COEFFICIENT VALUES
C
C
C ROUTINES: NONE
C
C NOTE:
C        IF:     n  =  number of ordinary/non-metastable levels
C            R(nxn) = Rate matrix (SEC-1) covering transistions between
C                     all possible pairs of ordinary levels.
C                     row   : final   level
C                     column: initial level
C                     (Inverse R-1(nxn) = 'CMAT(,)' )
C            V(n)   = Recombination rate vector (CM**3 SEC-1) covering
C                     all ordinary levels.
C                     ( = 'VEC()' - ordinary level part ).
C            S(n)   = Recombination contribution vector (CM**3) covering
C                     all ordinary levels ( = 'STV()' ).
C
C           Therefore:  R(nxn).S(n) = V(n)
C
C            =>         S(n)  = R-1(nxn).V(n)
C
C
C
C AUTHOR:  HP SUMMERS  (UPGRADE OF BXSTKB BT PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/06/92
C
C UPDATE:  12/07/93  HPS - CHANGE STV DIMENSION TO R*4
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM              , NDLEV              , NDMET   ,
     &           IT                 , IP                 , NORD
      INTEGER    IS1                , IS2
C-----------------------------------------------------------------------
      REAL*8     COEF               , DENS
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8     CMAT(NDLEV,NDLEV)  , VEC(NDTEM,NDLEV,NDMET) ,
     &           V3(NDLEV,NDMET)
      REAL*4     STV(NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IS1=1,NORD
            COEF = 0.0D0
               DO 2 IS2=1,NORD
                  COEF = COEF - ( CMAT(IS1,IS2)*(VEC(IT,IORDR(IS2),IP)
     &                                       +DENS*V3(IORDR(IS2),IP)))
    2          CONTINUE
            STV(IS1) = COEF
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
