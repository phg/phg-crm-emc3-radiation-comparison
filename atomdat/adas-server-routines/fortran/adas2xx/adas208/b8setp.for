CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8setp.for,v 1.5 2018/10/03 09:22:56 mog Exp $ Date $Date: 2018/10/03 09:22:56 $
CX

       SUBROUTINE B8SETP( IZ0    , IZ     ,
     &                    NDLEV  , NDWVL  , NDSEL , IL    , ICNTE ,
     &                    CSTRGA , ISA    , ILA   , XJA   ,
     &                    STRGA  , NPL    , CPRTA , NDMET ,
     &                    LSS04A ,
     &                    STRGMF , STRGMI
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8SETP *********************
C
C  PURPOSE:  TO SET UP PARAMETERS IN THE SHARED POOLED FOR PANEL DISPLAY
C
C  CALLING PROGRAM: ADAS208
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'BADATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZ0     =  NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (I*4)  NPL     = NUMBER OF PARENTS IN INPUT DATA SET
C  INPUT : (C*9)  CPRTA() = PARENT NAME FROM INPUT DATA SET
C  INPUT : (I*4)  NDMET   = MAX.NO.OF METASTABLES ALLOWED
C  I/O   : (L*4)  LSS04A(,)= .TRUE. => IONIS. RATE SET IN ADF04 FILE:
C                            .FALSE.=> NOT SET IN ADF04 FILE
C                            1ST DIM: LEVEL INDEX
C                            2ND DIM: PARENT METASTABLE INDEX
C
C  OUTPUT: (C*22) STRGA() = LEVEL DESIGNATIONS
C  OUTPUT: (C*11) STRGMF()= INFORMATION STRINGS FOR IDL
C  OUTPUT: (C*12) STRGMI()= INFORMATION STRINGS FOR IDL
C
C          (C*8)  F6      = PARAMETER = 'VREPLACE'
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  J       = VALUE OF QUANTUM NUMBER L + 1
C          (I*4)  LFPOOL  = NO. OF LEVEL STRINGS SENT TO FUNCTION POOL
C
C          (C*2)  SZ0     =         NUCLEAR CHARGE READ
C          (C*2)  SZ      =  RECOMBINED ION CHARGE READ
C          (C*4)  SCNTE   =  NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (C*4)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*1)  CONFIG()= QUANTUM NUMBER (L) LETTERS
C                           DIMENSION: QUANTUM NUMBER L + 1
C          (C*8)  CHA()   = FUNCTION POOL NAMES: CHARGE VALUES
C          (C*8)  CHB()   = FUNCTION POOL NAMES: LEVEL DESIGNATIONS <=99
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  D.H.BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE:    11/04/96
C
C VERSION: 1.0                            DATE: 11/04/96
C MODIFIED: DH BROOKS
C      - CREATED FROM SKELETON OF BXSETP. ADDED NPL, NDMET & CPRTA TO
C        PARAMETER LIST & SENT CPRTA THROUGH TO IDL AS STRGMF. ALSO
C        BUILT STRGMI TO SEND THROUGH FOR 502V208.
C
C VERSION: 1.1                            DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C      - CHANGED DIMENSIONING OF CPRTA TO AGREE WITH ADAS208 AND ALTERED
C        LOOP TO WRITE STRGMF SO ARRAY INDEX DIDN'T GO OUT OF BOUNDS.
C        PUT UNDER S.C.C.S.
C
C VERSION: 1.2                            DATE: 20/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C      - ADDED XXFLSH CALLS
C 
C VERSION: 1.3				DATE: 13/09/99
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - ADDED TRUTH TABLE FOR S-LINE IONISATION DATA AVAILABILITY
C             IN ADF04 FILE TO INFORMATION TRANSFER TO IDL
C
C VERSION : 1.4                       
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Send value of ndwvl to IDL. 
C
C VERSION : 1.5                     
C DATE    : 27-09-2018
C MODIFIED: Martin O'Mullane
C           - Send value of ndsel to IDL.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IZ0           , IZ       , NDLEV    , IL     ,
     &          ICNTE         , NDMET    , I        , NDWVL  ,
     &          NDSEL
      INTEGER   ILEV          , ILVAL    , LFPOOL   , NPL    , IMET
      INTEGER   ISA(IL)       , ILA(IL)
C-----------------------------------------------------------------------
      REAL*8    XJA(IL)
C-----------------------------------------------------------------------
      CHARACTER F6*8
      CHARACTER SZ*2          , SZ0*2         , SCNTE*4  , SIL*4  ,
     &          CONFIG(20)*1
      CHARACTER CHA(4)*8      , CHB*8
      CHARACTER CSTRGA(IL)*18 , STRGA(NDLEV)*22 , STRGMI(NDLEV)*12
      CHARACTER CPRTA(NDMET)*9  , STRGMF(NDMET)*11
C-----------------------------------------------------------------------
      PARAMETER ( F6='VREPLACE' )
C-----------------------------------------------------------------------
      SAVE      CHA            , CHB
C-----------------------------------------------------------------------
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
      DATA CHA   / '(SZ0)   ' , '(SZ)    ' , '(SCNTE) ' , '(SIL)   ' /
      DATA CHB   / '(STR**) ' /
C-----------------------------------------------------------------------
      LOGICAL  LSS04A(NDLEV,NDMET)
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6 )

C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      WRITE(SZ0,1000) IZ0
      WRITE(SZ ,1000) IZ
      WRITE(SCNTE,1001) ICNTE
      WRITE(SIL  ,1001) IL

         DO 1 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1002)
     &                   CSTRGA(ILEV)(1:12),ISA(ILEV),CONFIG(ILVAL), 
     &                      XJA(ILEV)
               ENDIF
    1    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
         DO 3 ILEV=1,NDLEV
            STRGMI(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGMI(ILEV)(1:12),1005)
     &                   ILEV,ISA(ILEV),CONFIG(ILVAL), 
     &                      XJA(ILEV)
               ENDIF
    3    CONTINUE
      IF (IL.LT.NDLEV) STRGMI(IL+1) = '*LEVELS END*'
C
         DO 5 IMET=1,NDMET
             STRGMF(IMET)=' '
               IF(IMET.LE.NPL.AND.NPL.LE.NDMET)THEN
                  WRITE(STRGMF(IMET)(1:11),1004)IMET,CPRTA(IMET)
               ENDIF
    5    CONTINUE
         IF( NPL.LT.NDMET) STRGMF(NPL+1) = '*MET END*'
C
C-----------------------------------------------------------------------
C SEND STRINGS TO ISPF FUNCTION POOL
C ULTIRX PORT - VARIABLES WRITTEN TO IDL VIA PIPE INSTEAD OF TO
C ISPF FUNCTION POOL
C-----------------------------------------------------------------------
C
C     ILEN=2
C     CALL ISPLNK( F6 , CHA(1) , ILEN , SZ0   )
C     ILEN=2
C     CALL ISPLNK( F6 , CHA(2) , ILEN , SZ    )
C     ILEN=3
C     CALL ISPLNK( F6 , CHA(3) , ILEN , SCNTE )
C     ILEN=3
C     CALL ISPLNK( F6 , CHA(4) , ILEN , SIL   )
C
      WRITE(PIPEOU,'(A2)') SZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') SZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A4)') SCNTE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A4)') SIL
      CALL XXFLSH(PIPEOU)
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ONLY SEND LEVEL STRINGS TO FUNCTION POOL FOR FIRST 99 LEVELS
C ULTIRX PORT - VARIABLES WRITTEN TO IDL VIA PIPE INSTEAD OF TO
C ISPF FUNCTION POOL
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
      LFPOOL = MIN0(NDLEV,99)
      WRITE(PIPEOU,'(i2)') LFPOOL
      CALL XXFLSH(PIPEOU)
C
      DO 10 ILEV=1,LFPOOL
C     WRITE(CHB(5:6),1003) ILEV
C     ILEN=22
C     CALL ISPLNK( F6 , CHB , ILEN , STRGA(ILEV) )
         WRITE(PIPEOU,*) STRGA(ILEV)
         CALL XXFLSH(PIPEOU)
 10   CONTINUE
      
c      CALL XXFLSH(PIPEOU)
C     
      DO 15 ILEV = 1,LFPOOL
         WRITE(PIPEOU,*) STRGMI(ILEV)
         CALL XXFLSH(PIPEOU)
 15   CONTINUE

C
      WRITE(PIPEOU,*) NDMET
      CALL XXFLSH(PIPEOU)
      
      WRITE(PIPEOU,*) NDWVL
      CALL XXFLSH(PIPEOU)
      
      WRITE(PIPEOU,*) NDSEL
      CALL XXFLSH(PIPEOU)
      
      WRITE(PIPEOU,*) NPL
      CALL XXFLSH(PIPEOU)
      DO 20 IMET=1,NDMET
         WRITE(PIPEOU,*) STRGMF(IMET)
         CALL XXFLSH(PIPEOU)
 20   CONTINUE
 
C-----------------------------------------------------------------------
C  SEND AVAILABILITY OF IONISATION RATES IN ADF04 FILE INFORMATION
C-----------------------------------------------------------------------
      WRITE(PIPEOU,*)NDLEV
      CALL XXFLSH(PIPEOU)
      DO ILEV=1,NDLEV
        DO IMET=1,NDMET
           IF (LSS04A(ILEV,IMET)) THEN
               I=1
           ELSE
               I=0
           ENDIF
           WRITE(PIPEOU,*)I
           CALL XXFLSH(PIPEOU)
        ENDDO
      ENDDO    

C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(I4)
 1002 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
 1003 FORMAT(I2.2)
 1004 FORMAT(I2,1A9)
 1005 FORMAT(I2,'(',I1,')',A1,'(',F4.1,')')
C
C-----------------------------------------------------------------------
C
      RETURN
      END
