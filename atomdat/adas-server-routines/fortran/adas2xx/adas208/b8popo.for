C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8popo.for,v 1.1 2004/07/06 11:30:33 whitefor Exp $ Date $Date: 2004/07/06 11:30:33 $
C
      SUBROUTINE B8POPO( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   NPL    , NPLR  , NPLI  ,
     &                   MAXT   , MAXD  , NMET  , NORD  ,
     &                            DENSA , IMETR , IORDR ,
     &                            LRSEL , LISEL , LHSEL ,
     &                            RATPIA, RATMIA, RATHA ,
     &                   STACK  , STVR  , STVI  , STVH  ,
     &                   POPAR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8POPO *********************
C
C  PURPOSE: TO CONSTRUCT ORDINARY/NON-METASTABLE LEVEL POPULATIONS.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NPL     = NO. OF METASTABLES OF (Z+1) ION ACCESSED
C                              BY EXCITED STATE IONISATION IN COPASE
C                              FILE WITH IONISATION POTENTIALS GIVEN
C                              ON THE FIRST DATA LINE
C  INPUT :  (I*4)  NPLR    = NO. OF ACTIVE METASTABLES OF (Z+1) ION
C  INPUT :  (I*4)  NPLI    = NO. OF ACTIVE METASTABLES OF (Z-1) ION
C
C  INPUT :  (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 ->'NDTEM')
C  INPUT :  (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 ->'NDDEN')
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES LEVELS ( 1 ->'NDMET')
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ( 1 ->'NDLEV')
C
C  INPUT :  (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (L*4)  LRSEL   = .TRUE.  => FREE ELECTRON RECOMBINATION
C                                       REQUESTED.
C                          = .FALSE. => FREE ELECTRON RECOMBINATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LISEL   = .TRUE.  => ELECTRON IMPACT IONISATION
C                                       REQUESTED.
C                          = .FALSE. => ELECTRON IMPACT IONISATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN REQUESTED.
C                          = .FALSE. => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN NOT REQUESTED.
C
C  INPUT :  (R*8)  RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                              1ST DIMENSION: TEMP/DENS INDEX
C                              2ND DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  RATMIA(,)= RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                              1ST DIMENSION: TEMP/DENS INDEX
C                              2ND DIMENSION: PARENT INDEX
C  INPUT :  (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C
C  INPUT : (R*4) STACK(,,,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C  INPUT : (R*4) STVR(,,,) = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVI(,,,) = ORDINARY EXCITED LEVEL:
C                             ELECTRON IMPACT IONISATION  COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C  INPUT : (R*4) STVH(,,,) = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C                             4TH DIMENSION: PARENT INDEX
C
C  I/O   :  (R*8)  POPAR(,,)= LEVEL POPULATIONS
C                              1st DIMENSION: LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C                               ON INPUT : CONTAINS POPULATIONS FOR
C                                          METASTABLE LEVELS ONLY.
C                               ON OUTPUT: CONTAINS POPULATIONS FOR
C                                          ALL LEVELS.
C
C           (R*8) DCOEF     = DENSITY MULTIPLIED BY RELEVANT RATIOS  FOR
C                             CALCULATING RECOMBINATION CONTRIBUTIONS.
C
C           (I*4) IT        = TEMPERATURE ARRAY INDEX
C           (I*4) IP        = PARENT INDEX
C           (I*4) IN        = DENSITY ARRAY INDEX
C           (I*4) IO        = ORDINARY LEVEL ARRAY INDEX
C           (I*4) IM        = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C AUTHOR:  HP SUMMERS  (UPDATE OF BXPOPO BY PE BRIDEN)
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/06/92
C
C UPDATE:  12/07/93  HPS - CHANGE STSCK, STVR, STVI, STVH
C                          DIMENSIONS TO R*4
C***********************************************************************
C UNIX-IDL PORT:
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C DATE: UNKNOWN
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE: 10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM        , NDDEN        , NDMET   , NDLEV   ,
     &          NPL          , NPLR         , NPLI    ,
     &          MAXT         , MAXD         , NMET    , NORD
      INTEGER   IT           , IN           , IM      , IO      , IP
C-----------------------------------------------------------------------
      REAL*8    DCOEF
C-----------------------------------------------------------------------
      LOGICAL   LRSEL        , LISEL        , LHSEL
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)                , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDDEN)                ,
     &          RATPIA(NDDEN,NDMET)         , RATMIA(NDDEN,NDMET)    ,
     &          RATHA(NDDEN)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      REAL*4    STVR(NDLEV,NDTEM,NDDEN,NDMET) ,
     &          STVI(NDLEV,NDTEM,NDDEN,NDMET) ,
     &          STVH(NDLEV,NDTEM,NDDEN,NDMET)
      REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  ORDINARY/NON-METSTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         DO 1 IN=1,MAXD
            DO 2 IT=1,MAXT
               DO 3 IO=1,NORD
                  POPAR(IORDR(IO),IT,IN)=0.0D0
                     DO 4 IM=1,NMET
                        POPAR(IORDR(IO),IT,IN)= POPAR(IORDR(IO),IT,IN) +
     &                   ( POPAR(IMETR(IM),IT,IN) * STACK(IO,IM,IT,IN) )
    4                CONTINUE
    3          CONTINUE
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD FREE ELECTRON RECOMB. CONTRIBUTION TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            DO 8 IN=1,MAXD
              DO 7 IP=1,NPLR
                DCOEF = RATPIA(IN,IP)*DENSA(IN)
                DO 6 IT=1,MAXT
                  DO 5 IO=1,NORD
                     POPAR(IORDR(IO),IT,IN)=POPAR(IORDR(IO),IT,IN) +
     &                                      ( DCOEF*STVR(IO,IT,IN,IP))
    5             CONTINUE
    6           CONTINUE
    7         CONTINUE
    8       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD IONISATION FROM LOWER IONIS. STAGE TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LISEL) THEN
            DO 12 IN=1,MAXD
              DO 11 IP=1,NPLI
                DCOEF = RATMIA(IN,IP)*DENSA(IN)
                DO 10 IT=1,MAXT
                  DO 9 IO=1,NORD
                     POPAR(IORDR(IO),IT,IN)=POPAR(IORDR(IO),IT,IN) +
     &                                      ( DCOEF*STVI(IO,IT,IN,IP))
    9             CONTINUE
   10           CONTINUE
   11         CONTINUE
   12       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD CHARGE EXCHANGE RECOMB. CONTRIBUTION TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            DO 16 IN=1,MAXD
              DO 15 IP=1,NPLR
                DCOEF = RATHA(IN)*RATPIA(IN,IP)*DENSA(IN)
                DO 14 IT=1,MAXT
                  DO 13 IO=1,NORD
                    POPAR(IORDR(IO),IT,IN)=POPAR(IORDR(IO),IT,IN) +
     &                                     ( DCOEF*STVH(IO,IT,IN,IP) )
   13             CONTINUE
   14           CONTINUE
   15         CONTINUE
   16       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
