      SUBROUTINE B8ISPF( IPAN  , LPEND  , TITLED ,
     &                   NDTEM , NDDEN  , NDMET  , ndsel ,
     &                   IL    , NV     , TSCEF  ,
     &                   TITLE , NMET   , IMETR  ,
     &                   IFOUT ,
     &                   MAXT  , TINE   , TINP   , TINH  ,
     &                   IDOUT ,
     &                   MAXD  , DINE   , DINP   , 
     &                   RATHA , RATPIA , RATMIA ,
     &                   ZEFF  , LNORM  ,
     &                   LPSEL , LZSEL  , LIOSEL , 
     &                   LHSEL , LRSEL  , LISEL  ,
     &                   LNSEL , LGPH   , 
     &                   NWVL  , WVLS   , WVLL   , AVLT  ,
     &                   ntrsel, itrlow , itrup  ,
     &                   ITSEL , GTIT1  , LGRD1  , LDEF1 ,
     &                   XL1   , XU1    , YL1    , YU1   , IBSELA,
     &                   LSSETA, SGRDA  , ISSETA , DSFULL
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: B8ISPF *********************
C
C  PURPOSE: ISPF PANEL INPUT SUBROUTINE FOR 'ADAS20T'
C
C  CALLING PROGRAM: ADAS20T
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDTEM    = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDDEN    = MAX. NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)   NDMET    = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)   NDSEL    = PARAMETER = MAX. NO. OF USER SUPPLIED TRANSITONS
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER OF TEMPERATURES
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (I*4)   NMET     = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  OUTPUT: (I*4)   IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 20)
C  OUTPUT: (R*8)   TINE()   = ELECTRON TEMPERATURES (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINP()   = PROTON TEMPERATURES   (UNITS: SEE 'IFOUT')
C  OUTPUT: (R*8)   TINH()   = NEUTRAL HYDROGEN TEMPERATURES
C                                                   (UNITS: SEE 'IFOUT')
C
C  OUTPUT: (I*4)   IDOUT    = 1 => INPUT DENSITIES IN CM-3
C                           = 2 => INPUT DENSITIES IN REDUCED FORM
C
C  OUTPUT: (I*4)   MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 20)
C  OUTPUT: (R*8)   DINE()   = ELECTRON DENSITIES  (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   DINP()   = PROTON DENSITIES    (UNITS: SEE 'IDOUT')
C  OUTPUT: (R*8)   RATHA()  = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C  OUTPUT: (R*8)   RATPIA(,)= RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION:  TEMP/DENS INDEX
C                               2ND DIMENSION:  PARENT INDEX
C  OUTPUT: (R*8)   RATMIA(,)= RATIO ( N(Z-1)/N(Z)  STAGE ABUNDANCIES )
C                               1ST DIMENSION:  TEMP/DENS INDEX
C                               2ND DIMENSION:  PARENT INDEX
C
C  OUTPUT: (R*8)   ZEFF     = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C
C  OUTPUT: (L*4)   LPSEL    = .TRUE.  => INCLUDE PROTON COLLISIONS
C                           = .FALSE. =>DO NOT INCLUDE PROTON COLLISIONS
C  OUTPUT: (L*4)   LZSEL    = .TRUE.  => SCALE PROTON COLLISIONS WITH
C                                        PLASMA Z EFFECTIVE'ZEFF'.
C                           = .FALSE. => DO NOT SCALE PROTON COLLISIONS
C                                        WITH PLASMA Z EFFECTIVE 'ZEFF'.
C                           (ONLY USED IF 'LPSEL=.TRUE.')
C  OUTPUT: (L*4)   LIOSEL   = .TRUE.  => INCLUDE EXCITED STATE IONIS.
C                           = .FALSE. => DO NOT INCLUDE EXC. STATE IONIS
C  OUTPUT: (L*4)   LHSEL    = .TRUE.  => INCLUDE CHARGE TRANSFER FROM
C                                        NEUTRAL HYDROGREN.
C                           = .FALSE. => DO NOT INCLUDE CHARGE TRANSFER
C                                        FROM NEUTRAL HYDROGREN.
C  OUTPUT: (L*4)   LNORM    = .TRUE.  => IF NMET=1 NORMALISE EMISSIVITY
C                                        AND RELATED OUTPUT FILES
C                                        TO STAGE TOT POPULATN
C                           = .FALSE. => IF NOT NORMALISE TO IDENTIFIED
C                                        METASTABLE POPULATIONS.
C
C  OUTPUT: (L*4)   LRSEL    = .TRUE.  => INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  OUTPUT: (L*4)   LISEL    = .TRUE.  => INCLUDE ELECTRON IMPACT
C                                        IONISATION FROM LOWER STAGE ION
C                           = .FALSE. => DO NOT INCLUDE FREE ELECTRON
C                                        RECOMBINATION.
C  OUTPUT: (L*4)   LISEL    = .TRUE.  => INCLUDE PROJECTED BUNDLE-N DATA
C                                        FROM DATAFILE IF AVAILABEL
C                           = .FALSE. => DO NOT INCLUDE PROJECTED
C                                        BUNDLE-N DATA.
C  OUTPUT: (L*4)   LGPH     = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                           = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C
C  OUTPUT: (I*4)   NVLS     = NUMBER OF WAVELENGTH INTERVALS
C  OUTPUT: (R*8)   WVLS     = SHORT WAVELENGTH LIMIT FOR PEC & SXB (A)
C  OUTPUT: (R*8)   WVLL     = LONG WAVELENGTH LIMIT FOR PEC & SXB  (A)
C  OUTPUT: (R*8)   AVLT     = LOWER LIMIT OF A-VALUES FOR PEC & SXB
C
C  OUTPUT : (I*4)  NTRSEL   = Number of transitions supplied by user
C  OUTPUT : (I*4)  ITRLOW() = Lower levels of requested transitions
C  OUTPUT : (I*4)  ITRUP()  = Upper levels of requested transitions
C
C  OUTPUT: (I*4)   ITSEL    = TEMPERATURE SELECTED FOR GRAPH (FROM INPUT
C                             LIST).
C  OUTPUT: (C*40)  GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  OUTPUT: (C*80)  DSFULL   = FULL FILE NAME FROM 502 IDL INPUT SCREEN
C
C          (I*4)   NTABLE   = PARAMETER = NUMBER OF ISPF TABLES TO BE
C                                         ACCESSED.
C          (I*4)   NPANEL   = PARAMETER = NUMBER OF ISPF PANELS TO BE
C                                         DISPLAYED.
C          (I*4)   L20      = PARAMETER = 20
C
C          (C*1)   GMAX     = PARAMETER =MAXIMUM NUMBER OF  TEMPERATURES
C                                        THAT CAN BE PLOTTED ON A SINGLE
C                                        GRAPH, AS A CHARACTER STRING.
C                                        (MUST BE LEFT AS '1')
C          (C*8)   F3       = PARAMETER = 'VDEFINE '
C          (C*8)   F5       = PARAMETER = 'CHAR    '
C
C          (I*4)   IPANRC   = RETURN CODE FROM ISPF PANEL DISPLAY
C          (I*4)   ILEN     = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)   IABT     = RETURN CODE FROM 'B8PAN?' ROUTINES
C                             ( 0 => NO ERROR)
C
C          (C*2)   P        = NEXT PANEL NUMBER TO BE DISPLAYED.
C          (C*2)   PLAST    = PANEL NUMBER REQUESTED FROM LAST DISPLAYED
C                             PANEL.
C          (C*4)   KEY      = 'PF' KEY VALUE IF PRESSED (AS 'PF??')
C          (C*8)   HCURPOS  = CURSOR POSITION IDENTIFYING LAST SELECTED
C                             OPTION ON PANEL 'XXPANH'.
C          (C*8)   CURPOS   = CURSOR POSITION WHEN PANEL (RE)-DISPLAYED
C                             (PANEL VARIABLE NAME IN BRACKETS)
C          (C*8)   MSGTXT   = ERROR MESSAGE NAME (BLANK => NO ERROR)
C                             FOR NEXT DISPLAYED PANEL
C                             (MESSAGE NAME IN BRACKETS)
C          (C*8)   PNAME    = IF (IABT.NE.0) PANEL VARIABLE WHERE ERROR
C                             FOUND (IN BRACKETS)
C
C          (C*8)   DTABLE() = ISPF TABLE NAMES FOR PANEL VARIABLES
C          (C*8)   DPANEL() = ISPF PANEL NAMES FOR DISPLAY
C
C          (L*4)   LMCHNG   = .TRUE.  => METASTABLE INDEX ALLOCATIONS
C                                        READ FROM TABLE NO LONGER VALID
C                           = .FALSE. => METASTABLE INDEX ALLOCATIONS
C                                        READ FROM TABLE ARE VALID
C          (L*4)   LTCHNG   = .TRUE.  => ENTERED TEMPERATURE VALUES MAY
C                                        HAVE BEEN ALTERED BY USER.
C                           = .FALSE. => ENTERED TEMPERATURE VALUES HAVE
C                                        NOT BEEN ALTERED BY USER.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          B8PAN1     ADAS      FETCH VALUES FROM PANEL 'P20811'
C          B8PAN2     ADAS      FETCH VALUES FROM PANEL 'P20812'
C          B8PAN3A    ADAS      FETCH VALUES FROM PANEL 'P20813A'
C          B8PAN3B    ADAS      FETCH VALUES FROM PANEL 'P20813B'
C          B8PAN3C    ADAS      FETCH VALUES FROM PANEL 'P20813C'
C          B8PAN4     ADAS      FETCH VALUES FROM PANEL 'P20814'
C          XXPANH     ADAS      FETCH VALUES FROM PANEL 'P20815'
C          XXSTOP     ADAS      TO TEST IF PROGRAM IS TO BE TERMINATED
C          XXPAN0     ADAS      DISPLAY/FETCH VALUES FROM PANEL 'P20810'
C          XXCREC     ADAS      RECALL ISPF VARIABLES FROM TABLES
C          XXPKEY     ADAS      NEXT ISPF PANEL NUMBER FOR DISPLAY
C          XXDISP     ADAS      DISPLAY A GIVEN PANEL (WITH MESSAGE)
C          XXFERR     ADAS      SET UP PANEL PARAMETERS AFTER ERROR
C          ISPLNK     ISPF      ISPF PANEL ROUTINE
C
C
C AUTHOR:  H P SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/01/92
C
C UPDATE:  10/07/92  HP SUMMERS  - INCLUDE BUNDLE-N DATA OPTION
C
C UPDATE:  12/07/93  HPS - MODIFICATIONS FOR CONSISTENCY IN USE OF
C                          NORMALISATION WITH LATEST ADAS206
C
C UNIX-IDL PORT:
C
C DATE: UNKNOWN
C
C AUTHOR: DAVID H BROOKS, UNIVERSITY OF STRATHCLYDE
C
C UPDATE:  01/05/96  DHB - MODIFIED TO READ IONISATION COEFFICIENT
C
C***********************************************************************
C PUT UNDER SCCS CONTROL:
C
C VERSION: 1.1				DATE:10/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - FIRST PUT UNDER SCCS
C 
C VERSION: 1.2				DATE: 13/05/96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - ADDED DSFULL STRING TO RETURN FILENAME FROM ADAS 502
C 
C VERSION: 1.2				DATE: 30/09/96
C MODIFIED: WILLIAM OSBORN + HUGH SUMMERS
C           - ADDED WAVELENGTH & A-VALUE LIMITS FOR PEC &
C                          SXB FILES
C           - MADE LOOP FOR WRITE OF TSCEF EXPLICIT
C
C VERSION: 1.3                          DATE: 14-10-96
C MODIFIED: WILLIAM OSBORN
C           - ADDED A WRITE TO I4UNIT WHICH ALTHOUGH IT SHOULD
C             HAVE NO EFFECT APPEARS TO BE NEEDED TO STOP THE CODE
C             PRODUCING A MASSIVE CORE DUMP ON THE HP.
C
C VERSION: 1.4				DATE: 02-07-97
C MODIFIED: RICHARD MARTIN
C	    - ADDED READ FROM PIPE TO OBTAIN LNORM.
C
C VERSION : 1.5                      
C DATE    : 17-03-2013
C MODIFIED: Martin O'Mullane
C           - Wavelength limits and Amin are now arrays.
C           - Increase size of filename variables from 80 to 132 characters.
C
C VERSION : 1.6
C DATE    : 29-09-2018
C MODIFIED: Martin O'Mullane
C           - Allow user to specify transition indices to be output
C             in the PEC file.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU     , ONE       , ZERO
      INTEGER    NDWVL          
C-----------------------------------------------------------------------
      PARAMETER( PIPEIN=5  , PIPEOU=6   , ONE=1     , ZERO=0 )
      PARAMETER( NDWVL = 5)
C-----------------------------------------------------------------------
      INTEGER   IPAN       , NMET       , 
     &          NDTEM      , NDDEN      , NDMET     , ndsel  , 
     &          IL         , NV         ,
     &          IFOUT      , MAXT       ,
     &          IDOUT      , MAXD       ,
     &          ITSEL      , nwvl       , ntrsel
      INTEGER   I          , J          , K
C-----------------------------------------------------------------------
      REAL*8    ZEFF       ,
     &          XL1        , XU1        , YL1       , YU1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40  , GTIT1*40, TITLED*3, DSFULL*132
C-----------------------------------------------------------------------
      LOGICAL    LNORM
      LOGICAL    LPEND
      LOGICAL    LPSEL     , LZSEL      , LIOSEL    , LHSEL     ,
     &           LRSEL     , LISEL      , LNSEL     , LGPH      , 
     &           LGRD1     , LDEF1
      LOGICAL    LMCHNG    , LTCHNG
C-----------------------------------------------------------------------
      INTEGER    IMETR(NDMET) , LOGIC
      INTEGER    I4UNIT
      INTEGER    IBSELA(NDMET,NDMET)    , ISSETA(NDMET,NDMET)
      integer    itrlow(ndsel)          , itrup(ndsel)
C-----------------------------------------------------------------------
      REAL*8     TSCEF(14,3) ,
     &           TINE(NDTEM) , TINP(NDTEM)  , TINH(NDTEM) ,
     &           DINE(NDDEN) , DINP(NDDEN)  , RATHA(NDDEN), 
     &           RATPIA(NDDEN,NDMET) , RATMIA(NDDEN,NDMET)
      REAL*8     SGRDA(NDTEM,NDMET,NDMET)
      real*8     avlt(ndwvl)     , wvls(ndwvl)      , wvll(ndwvl)
C-----------------------------------------------------------------------
      LOGICAL LSSETA(NDMET,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      IF (LPEND) THEN
          WRITE(PIPEOU,*) ONE
      ELSE
          WRITE(PIPEOU,*) ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A3)') TITLED
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDTEM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDDEN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NV
      CALL XXFLSH(PIPEOU)
C
C     BIZARRELY THE NEXT LINE APPEARS TO BE NEEDED TO STOP THE HP
C     CRASHING
C
      WRITE(I4UNIT(-1),*) " "
      DO 1 J=1,3
         DO 2 I=1,14
            WRITE(PIPEOU,*)TSCEF(I,J)
            CALL XXFLSH(PIPEOU)
 2       CONTINUE
 1    CONTINUE
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPEND = .TRUE.
          GOTO 999
      ELSE
          LPEND = .FALSE.
      ENDIF

      READ(PIPEIN,'(A)') TITLE
      READ(PIPEIN,*) NMET
      READ(PIPEIN,*) (IMETR(I),I=1,NDMET)
      READ(PIPEIN,*) IFOUT
      READ(PIPEIN,*) MAXT
      READ(PIPEIN,*) (TINE(I),I=1,NDTEM)
      READ(PIPEIN,*) (TINP(I),I=1,NDTEM)
      READ(PIPEIN,*) (TINH(I),I=1,NDTEM)
      READ(PIPEIN,*) IDOUT
      READ(PIPEIN,*) MAXD
      READ(PIPEIN,*) (DINE(I),I=1,NDDEN)
      READ(PIPEIN,*) (DINP(I),I=1,NDDEN)
      READ(PIPEIN,*) (RATHA(I),I=1,NDDEN)
      DO 10 J = 1, NDMET
        DO 20 I = 1, NDDEN
          READ(PIPEIN,*)RATPIA(I,J)
 20     CONTINUE
 10   CONTINUE
      DO 30 J = 1, NDMET
        DO 40 I = 1, NDDEN
          READ(PIPEIN,*)RATMIA(I,J)
 40     CONTINUE
 30   CONTINUE

      READ(PIPEIN,*) ZEFF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LPSEL = .TRUE.
      ELSE
          LPSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LZSEL = .TRUE.
      ELSE
          LZSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LISEL = .TRUE.
      ELSE
          LISEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LHSEL = .TRUE.
      ELSE
          LHSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LRSEL = .TRUE.
      ELSE
          LRSEL = .FALSE.
      ENDIF
C
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LIOSEL = .TRUE.
      ELSE
          LIOSEL = .FALSE.
      ENDIF

      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LNSEL = .TRUE.
      ELSE
          LNSEL = .FALSE.
      ENDIF
      
      READ(PIPEIN,*) LOGIC
      IF (LOGIC.EQ.ONE) THEN
          LNORM = .TRUE.
      ELSE
          LNORM = .FALSE.
      ENDIF
      
      DO 50 I = 1, NDMET
        DO 60 J = 1, NDMET
          READ(PIPEIN,*)IBSELA(I,J)
 60     CONTINUE
 50   CONTINUE

      DO 70 I = 1, NDMET
        DO 80 J = 1, NDMET
          READ(PIPEIN,*)ISSETA(I,J)
          IF( ISSETA(I,J).EQ.ONE)THEN
            LSSETA(I,J) = .TRUE.
          ELSE
            LSSETA(I,J) = .FALSE.
          ENDIF
 80     CONTINUE
 70   CONTINUE
      
      DO 90 K = 1, NDTEM
        DO 100 I = 1, NDMET
          DO 110 J = 1, NDMET
            READ(PIPEIN,*) SGRDA(K,I,J)
 110      CONTINUE
 100   CONTINUE
 90   CONTINUE

      READ(PIPEIN,'(A)')DSFULL

      read(pipein,*)nwvl
      do i = 1, nwvl
        read(pipein,*)wvls(i)
        read(pipein,*)wvll(i)
        read(pipein,*)avlt(i)
      end do

      read(pipein,*)ntrsel
      if (ntrsel.GT.0) then
         do i = 1, ntrsel
           read(pipein,*)itrlow(i)
           read(pipein,*)itrup(i)
         end do
      endif

C     
C
 999  RETURN
      END
