C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adas208/b8corp.for,v 1.6 2018/08/03 17:21:29 mog Exp $ Date $Date: 2018/08/03 17:21:29 $
C
      SUBROUTINE B8CORP(NLEV   , MAXT   , MAXD   , NMET  ,
     &                  TEVA   , COEFF  )

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: B8CORP ********************
C
C  PURPOSE: Corrects unphysical low temperature recombination
C           contributions to a PEC.
C
C           There is a low temperature problem in the production
C           calculation which gives unphysical recombination
C           contributions to the PECs. Generally the first 3-4
C           temperatures in the ADAS 96 standard are affected. This
C           routine replaces the first 4 temperatures from the
C           recombination contribution with extrapolated values
C           from the remaining data.
C
C
C  CALLING PROGRAM: ADAS208 (B8WR11)
C
C
C  INPUT : (I*4)  NLEV    = NUMBER OF LEVELS
C  INPUT : (I*4)  MAXT    = NUMBER OF TEMPERATURES
C  INPUT : (I*4)  MAXD    = NUMBER OF DENSITIES
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES
C
C  I/O   : (R*4) COEFF()  = RECOMBINATION CONTRIBUTION TO THE PEC
C                           (STVR IN ADAS208 CALL)
C                           1ST DIMENSION : LEVELS
C                           2ND DIMENSION : TEMPERATURES
C                           3RD DIMENSION : DENSITIES
C                           4TH DIMENSION : METASTABLES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLN     ADAS      SPLINE SUBROUTINE
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  Martin O'Mullane
C
C
C DATE:    14-09-99
C
C
C VERSION: 1.1                          DATE: 14-09-99
C MODIFIED: Martin O'Mullane
C           - First version
C
C VERSION : 1.2
C DATE    : 26-10-99
C MODIFIED: Martin O'Mullane
C           - Consider each level separately for extrapolation.
C           - Increase NDLEV in line with adas208.
C
C VERSION : 1.3
C DATE    : 20-07-07
C MODIFIED: Allan Whiteford
C           - Small modification to comments to allow for automatic
C             documentation preparation.
C
C VERSION : 1.4
C DATE    : 02-09-2007
C MODIFIED: Martin O'Mullane
C           - ITAG was defined as real*8 rather than integer
C
C VERSION : 1.5
C DATE    : 31-07-2009
C MODIFIED: Martin O'Mullane
C           - ITAG was not initialised.
C           - Increase NDLEV to 1000.
C
C VERSION : 1.6
C DATE    : 05-12-2017
C MODIFIED: Matthew Bluteau
C           - Increase max number of metastables to 13 (up from 4).
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM    , NDDEN      , NDMET           , NDLEV
      INTEGER    NIN      , NOUT       , IWRONG
C-----------------------------------------------------------------------
      PARAMETER( NIN = 35 , NOUT  = 35 , IWRONG = 4 )
      PARAMETER( NDTEM=35 , NDDEN = 24 , NDLEV  = 1000   , NDMET  = 13 )
C-----------------------------------------------------------------------
      INTEGER    ITA                   , IDA             , ITVAL      ,
     &           maxt                  , maxd            ,
     &           nmet                  , nlev
      INTEGER    IET                   , IED             , IT         ,
     &           IOPT                  , IL              , IR         ,
     &           ID                    , ind
      INTEGER    i4unit
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
C-----------------------------------------------------------------------
      LOGICAL    LSETX                 , LNONE
C-----------------------------------------------------------------------
      INTEGER    ITAG(NDTEM)
C-----------------------------------------------------------------------
      REAL*4     COEFF(NDLEV,NDTEM,NDDEN,NDMET)
      REAL*8     DATA(NDLEV,NDTEM-IWRONG,NDDEN,NDMET)
      REAL*8     TEST(NDTEM)
      REAL*8     TEVA(NDTEM)

      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)
C-----------------------------------------------------------------------
      LOGICAL    LRET(NDLEV)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Capture silly inputs and don't do anything if the coeff is monotonic
C in temperature.
C-----------------------------------------------------------------------

      if (maxt-iwrong .LT. 4) then
         write(i4unit(-1),*)'B8CORP - need more points to spline'
         return
      endif

      do il=1, nlev
        LRET(il) = .TRUE.
        do ir = 1, nmet
          do id = 1, maxd
            do it=1,maxt
              test(it) = coeff(il,it,id,ir)
              itag(it) = it
              LRET(il) = LRET(il) .AND. (coeff(il,it,id,ir).LT.1.0D0)
            end do
            call xxr8sort(maxt,.TRUE.,test,itag)
            do ind = 1, maxt-1
               LRET(il) = LRET(il) .AND. (itag(ind+1)-itag(ind).EQ.1)
            end do
          end do
        end do
      end do

      lnone = .true.
      do il =1, nlev
        if (.not.LRET(il)) then
           write(i4unit(-1),1000)il
           lnone = .FALSE.
        endif
      end do

      IF (LNONE) RETURN

 1000 FORMAT(1X,28('*'),' B8CORP INFORMATION ',28('*'),/,/,
     &       1X,'RECOMBINATION PEC DATA CORRECTED FOR LOW TE',/,
     &       1X,'FOR LEVEL : ',I4,/)

C-----------------------------------------------------------------------
C SET UP FIRST SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------

      LSETX = .TRUE.
      IOPT  = 0

C-----------------------------------------------------------------------
C Set up the 'new' coeff array by removing the first
C IWRONG temperatures.
C-----------------------------------------------------------------------

       do ir=1,nmet
         do id=1,maxd
           do it=1,maxt
               do il=1,nlev
               if (it.GT.IWRONG) then
                  data(il,it-iwrong,id,ir) = coeff(il,it,id,ir)
               endif
             end do
           end do
         end do
       end do


C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------


      DO IET=iwrong+1,maxt
         XIN(IET-iwrong) = DLOG( TEVA(IET) )
      END DO

      DO IT=1,maxt
         XOUT(IT) = DLOG( TEVA(IT)  )
      END DO

      ITA   = MAXT - IWRONG
      ITVAL = MAXT
      IDA   = MAXD


C-----------------------------------------------------------------------
C Run over all metastables of ion (NMET) and z+1 ion (nlev)
C-----------------------------------------------------------------------

      do 2 ir = 1, nmet
      do 1 il = 1, nlev

C SPLINE OVER ALL DATASET ELECTRON DENSITIES FOR EACH USER ELECTRON TEMP

         if (.not.lret(il)) then
            DO IED=1,IDA

               DO IET=1,ITA
                  YIN(IET) = DLOG( DATA(IL,IET,IED,IR) )
               END DO

               CALL XXSPLN( LSETX , IOPT    , R8FUN1 ,
     &                      ITA   , XIN     , YIN    ,
     &                      ITVAL , XOUT    , YOUT   ,
     &                      DF
     &                    )

               DO IT=1,ITVAL
                 COEFF(IL,IT,IED,IR) = DEXP( YOUT(IT) )
               END DO

            END DO
         endif


C-----------------------------------------------------------------------
   1     CONTINUE
   2     CONTINUE
C-----------------------------------------------------------------------

      RETURN
      END
