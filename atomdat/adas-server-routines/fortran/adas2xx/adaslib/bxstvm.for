CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxstvm.for,v 1.1 2004/07/06 11:46:47 whitefor Exp $ Date $Date: 2004/07/06 11:46:47 $
CX
      SUBROUTINE BXSTVM( NDMET  ,
     &                   NMET   ,
     &                   CRMAT  ,
     &                   VRED   ,
     &                   STVM
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXSTVM *********************
C
C  PURPOSE: TO  CALCULATE  AND STACK UP IN  'STVM'  THE METASTABLE LEVEL
C           RECOMBINATION  COEFFICIENTS  FOR  A  GIVEN  TEMPERATURE  AND
C           DENSITY.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (R*8)  CRMAT(,)= INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C
C  INPUT :  (R*8)  VRED()  = VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                            FOR EACH METASTABLE LEVEL.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C  OUTPUT:  (R*8)  STVM()  = RECOMBINATION  CONTRIBUTION  FOR  EACH
C                            METASTABLE LEVEL. (UNITS: CM**3)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            (LEVEL 1 IS TAKEN AS ZERO)
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C           (I*4)  IM1     = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IM2     = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C NOTE:
C           STVM(IM1)       SUM( (the transistion rate from IM2 to IM1)
C                                x (the recombination rate contribution
C                                   for metastable level IM2) )
C
C                           (IM1 & IM2 = METASTABLE LEVEL INDEX)
C
C                           ABOVE SUM IS OVER ALL METASTABLE LEVELS
C                           EXCEPT LEVEL ONE.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDMET               , NMET
      INTEGER    IM1                 , IM2
C-----------------------------------------------------------------------
      REAL*8     CRMAT(NDMET,NDMET)  , VRED(NDMET)        ,
     &           STVM(NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      STVM(1)=0.0D0
         DO 1 IM1=2,NMET
            STVM(IM1)=0.0D0
               DO 2 IM2=2,NMET
                  STVM(IM1) = STVM(IM1) -
     &                        ( CRMAT(IM1-1,IM2-1) * VRED(IM2) )
    2          CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
