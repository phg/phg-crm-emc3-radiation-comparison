CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxrate.for,v 1.1 2004/07/06 11:45:56 whitefor Exp $ Date $Date: 2004/07/06 11:45:56 $
CX
      SUBROUTINE BXRATE( NDTEM  , NDTRN  , GSCALE ,
     &                   NTIN   , TIN    , GAMIN  ,
     &                   NTOUT  , TOUT   ,
     &                   ICNT   , ITRN   ,
     &                   RATE   , DRATE  ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXRATE *********************
C
C  PURPOSE: TO CALCULATE THE EXCITATION AND DE-EXCITATION RATE COEFFICI-
C           ENTS FOR A SET OF INPUT TEMPERATURES 'TOUT' & TRANSITIONS OF
C           A SPECIFIED TYPE (ELECTRON OR PROTON IMPACT).
C
C           TRANSITION TYPE SELECTED VIA 'ICNT & ITRN'.
C
C           INPUT RATE COEFFICIENTS 'RATE' & 'DRATE'  ASSUME  THAT  THE
C           GAMMA VALUE IS UNITY, AND ARE GIVEN FOR THE TEMPERATURES IN
C           'TOUT'. THE GAMMA VALUES 'GAMIN' ARE FOR  THE  TEMPERARTURE
C           ARRAY 'TIN'.  SPLINES ARE USED  TO  EXTRAPOLATE/INTERPOLATE
C           THE GAMMA VALUES INTO THE 'TOUT' ARRAY  AND  THESE USED  TO
C           CALCULATE THE CORRECT RATE COEFFICIENTS.
C
C           SPLINE IS CARRIED OUT USING LOG(GAMMA VALUES)
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF TRANSITIONS ALLOWED
C  INPUT :  (I*4)  GSCALE  = SCALING FACTOR FOR OUTPUT GAMMA VALUES
C
C  INPUT :  (I*4)  NTIN    = NUMBER OF TEMPERATURES REPRESENTED IN THE
C                            INPUT DATA SET.
C  INPUT :  (R*8)  TIN()   = TEMPERATURES REPRESENTED IN INPUT DATA SET
C  INPUT :  (R*8)  GAMIN(,)= GAMMA VALUES REPRESENTED IN INPUT DATA SET
C                            1st DIMENSION: TEMPERATURE INDEX ('TIN')
C                            2nd DIMENSION: TRANSITION INDEX
C                                           (SEE: 'ITRN()')
C
C  INPUT :  (I*4)  NTOUT   = NUMBER OF ISPF SELECTED TEMPERATURES FOR
C                            OUTPUT.
C  INPUT :  (R*8)  TOUT()  = ISPF SELECTED TEMPERATURES FOR OUTPUT.
C
C  INPUT :  (I*4)  ICNT    = NUMBER OF SELECTED TRANSITIONS
C  INPUT :  (I*4)  ITRN()  = INDEX VALUES IN MAIN TRANSITION ARRAY WHICH
C                            REPRESENT TRANSITIONS OF THE SELECTED TYPE.
C                            USED TO SELECT APPROPRIATE GAMMA VALUES FOR
C                            TRANSITION TYPE.
C
C  I/O   :  (R*8)  RATE(,) = EXCITATION RATE COEFFS (cm**3/s)
C                            INPUT : UNIT GAMMA VALUES
C                            OUTPUT: TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C  I/O   :  (R*8)  DRATE(,)= DE-EXCIT'N RATE COEFFS (cm**3/s)
C                            INPUT : UNIT GAMMA VALUES
C                            OUTPUT: TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C
C  OUTPUT:  (L*4)  LTRNG() = .TRUE. => TEMPERATURE VALUES WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                          = .FALSE.=>TEMPERATURE VALUE NOT WITHIN RANGE
C                                     READ FROM INPUT COPASE DATA SET.
C                            1st DIMENSION: TEMPERATURE INDEX.
C
C
C           (I*4)  NTDSN   = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C                                        ALLOWED IN INPUT DATA SET = 8
C           (I*4)  NLTEM   = PARAMETER = MUST BE >= 'NDTEM'
C
C           (I*4)  GZERO   = PARAMETER = IF 'GAMIN(1,) < GZERO' THEN ALL
C                                        THE 'RATE' AND  'DRATE'  VALUES
C                                        FOR THE  GIVEN  TRANSITION  ARE
C                                        SAID TO BE ZERO.
C
C           (I*4)  IOPT    = SPLINE END CONDITIONS/EXTRAPOLATION CONTROL
C                            SWITCH - SEE 'XXSPLE'
C                            I.E. DEFINES THE BOUNDARY DERIVATIVES.
C                            (VALID VALUES = 0, 1, 2, 3, 4)
C           (I*4)  ITRAN   = APPROPRIATE TRANSITION INDEX FOR 'GAMIN(,)'
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C           (I*4)  IT      = TEMPERATURE ARRAY INDEX
C
C           (R*8)  GAMMA   = SPLINED GAMMA VALUE FOR GIVEN TEMPERATURE
C                            (FROM 'TOUT()') AND TRANSITION.
C           (R*8)  DYIN()  = INTERPOLATED DERIVATIVES
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C
C           (L*4)  LSETX   = .TRUE.  => X-AXES ('TIN()' VALUES) NEED TO
C                                       SET IN 'XXSPLE'.
C                            .FALSE. => X-AXES ('TIN()' VALUES) HAVE
C                                       BEEN SET IN 'XXSPLE'.
C                            (NOTE: 'LSETX' IS RESET BY 'XXSPLE')
C
C           (R*8)  LGIN()  = LOG ( 'GAMIN(,)' ) FOR GIVEN TRANSITION
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C           (R*8)  LGOUT() = LOG ( SPLINED GAMMA VALUES )
C                            DIMENSION: TEMPERATURE INDEX ('TOUT()' )
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  31/01/91 - PE BRIDEN - ADAS91 - INTRODUCED 'LTRNG'
C                                        - REPLACED XXSPLN WITH XXSPLE
C
C UPDATE:  26/03/91 - PE BRIDEN - ADAS91 - IF 'GAMIN(1,)' <='GZERO' THEN
C                                          SET 'RATE' AND 'DRATE' TO 0.0
C                                          FOR ALL TEMPERATURE VALUES.
C                                           * INCLUDED  FOR  LATER  USE.
C                                             AT PRESENT 'BXDATA'  MAKES
C                                             SURE 'GAMIN' HAS A MINIMUM
C                                             VALUE OF 1.00D-30. *
C
C UPDATE:  11/12/91 - PE BRIDEN - ADAS91 -NLTEM INCREASED FROM 20 to 101
C
C UPDATE:  20/05/93 - PE BRIDEN - ADAS91 -NTDSN INCREASED FROM 8 to 14
C                                         (REFLECTS CHANGES TO BXDATA)
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NTDSN              , NLTEM
C-----------------------------------------------------------------------
      REAL*8     GZERO
C-----------------------------------------------------------------------
      PARAMETER( NTDSN = 14         , NLTEM = 101        )
C-----------------------------------------------------------------------
      PARAMETER( GZERO = 1.01D-70   )
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDTEM              ,
     &           NTIN               , NTOUT              ,
     &           ICNT
      INTEGER    IOPT               , ITRAN              ,
     &           IC                 , IT
C-----------------------------------------------------------------------
      REAL*8     GSCALE             , GAMMA
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      INTEGER    ITRN(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     TIN(NTDSN)         , GAMIN(NTDSN,NDTRN) ,
     &           TOUT(NDTEM)        ,
     &           RATE(NDTEM,NDTRN)  , DRATE(NDTEM,NDTRN)
      REAL*8     DYIN(NTDSN)        ,
     &           LGIN(NTDSN)        , LGOUT(NLTEM)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTEM)
C-----------------------------------------------------------------------
      INTRINSIC  DLOG
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NLTEM.LT.NDTEM) STOP
     &                   ' BXRATE ERROR: NDTEM > NLTEM. INCREASE NLTEM'
C-----------------------------------------------------------------------
      LSETX = .TRUE.
      IOPT  = 0
C***********************************************************************
         DO 1 IC=1,ICNT
C
            ITRAN=ITRN(IC)
C
C-----------------------------------------------------------------------
               IF (GAMIN(1,ITRAN).GT.GZERO) THEN
C-----------------------------------------------------------------------
C
                     DO 2 IT=1,NTIN
                        LGIN(IT)=DLOG( GAMIN(IT,ITRAN) )
    2                CONTINUE
C
                  CALL XXSPLE( LSETX , IOPT  , DLOG   ,
     &                         NTIN  , TIN   , LGIN   ,
     &                         NTOUT , TOUT  , LGOUT  ,
     &                         DYIN  , LTRNG
     &                       )
C
                     DO 3 IT=1,NTOUT
                        GAMMA        = GSCALE * DEXP( LGOUT(IT) )
                        RATE(IT,IC)  = RATE(IT,IC)  * GAMMA
                        DRATE(IT,IC) = DRATE(IT,IC) * GAMMA
    3                CONTINUE
C
C-----------------------------------------------------------------------
               ELSE
C-----------------------------------------------------------------------
C
                     DO 4 IT=1,NTOUT
                        RATE(IT,IC)  = 0.0
                        DRATE(IT,IC) = 0.0
    4                CONTINUE
C
C-----------------------------------------------------------------------
               ENDIF
C-----------------------------------------------------------------------
C
    1    CONTINUE
C***********************************************************************
      RETURN
      END
