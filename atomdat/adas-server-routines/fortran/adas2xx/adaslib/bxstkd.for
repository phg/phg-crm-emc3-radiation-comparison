CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxstkd.for,v 1.1 2004/07/06 11:46:44 whitefor Exp $ Date $Date: 2004/07/06 11:46:44 $
CX
      SUBROUTINE BXSTKD( NDTEM  , NDLEV  , NDMET  ,
     &                   IT     , NORD   , NMET   ,
     &                            IORDR  , IMETR  ,
     &                   CC     , STV    , VEC    ,
     &                   VRED
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXSTKD *********************
C
C  PURPOSE: TO STACK UP IN  'VRED'  THE RECOMBINATION RATE CONTRIBUTIONS
C           FOR  EACH  METASTABLE  LEVEL  FOR  A  GIVEN TEMPERATURE  AND
C           DENSITY.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  IT      = INDEX DENOTING THE TEMPERATURE
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (R*8)  CC(,)   = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C  INPUT :  (R*8)  STV()   = RECOMBINATION  CONTRIBUTION  FOR  EACH
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: CM**3)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: ORDINARY EXCITED LEVEL INDEX
C  INPUT :  (R*8)  VEC(,)  = RECOMBINATION RATE COEFFT. VALUES.
C                            (UNITS: CM**3/SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: TEMPERATURE INDEX ('IT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX
C
C  OUTPUT:  (R*8)  VRED()  = VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                            FOR EACH METASTABLE LEVEL.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C           (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IS      = ORDINARY EXCITED LEVEL INDEX
C
C
C ROUTINES: NONE
C
C NOTE:
C           VRED(IM)      =    ( the recombination rate for IM )
C                                              +
C                           SUM( (the transistion rate from ordinary
C                                 level IS to IM)  x  (the recombin-
C                                 ation  contribution  for  ordinary
C                                 level IS) )
C
C                           ABOVE SUM IS OVER ALL ORDINARY LEVELS.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM              , NDLEV              , NDMET      ,
     &           IT                 , NORD               , NMET
      INTEGER    IM                 , IS
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)       , IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8     CC(NDLEV,NDLEV)    , STV(NDLEV)         ,
     &           VEC(NDTEM,NDLEV)   , VRED(NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IM=1,NMET
            VRED(IM) = VEC(IT,IMETR(IM))
              DO 2 IS=1,NORD
                 VRED(IM) = VRED(IM) + ( CC(IMETR(IM),IORDR(IS)) *
     &                                        STV(IS)        )
    2         CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
