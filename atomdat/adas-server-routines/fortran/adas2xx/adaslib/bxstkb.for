CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxstkb.for,v 1.1 2004/07/06 11:46:38 whitefor Exp $ Date $Date: 2004/07/06 11:46:38 $
CX
      SUBROUTINE BXSTKB( NDTEM  , NDLEV  ,
     &                   IT     , NORD   ,
     &                            IORDR  ,
     &                   CMAT   , VEC    ,
     &                   STV
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXSTKA *********************
C
C  PURPOSE: TO STACK UP IN  'STV'  THE  RECOMBINATION CONTRIBUTION  FOR
C           EACH  NON-METASTABLE/ORDINARY  EXCITED  LEVEL  FOR  A GIVEN
C           TEMPERATURE AND DENSITY.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  IT      = INDEX DENOTING THE TEMPERATURE
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (R*8)  CMAT(,) = INVERTED   RATE   MATRIX   COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS
C                            TRANSITIONS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C
C  INPUT :  (R*8)  VEC(,)  = RECOMBINATION RATE COEFFT. VALUES.
C                            (UNITS: CM**3/SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: TEMPERATURE INDEX ('IT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX
C
C  OUTPUT:  (R*8)  STV()   = RECOMBINATION  CONTRIBUTION  FOR  EACH
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: CM**3)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: ORDINARY EXCITED LEVEL INDEX
C
C           (I*4)  IS1     = ORDINARY EXCITED LEVEL INDEX
C           (I*4)  IS2     = ORDINARY EXCITED LEVEL INDEX
C
C           (R*8)  COEF    = VARIABLE USED TO SUM COEFFICIENT VALUES
C
C
C ROUTINES: NONE
C
C NOTE:
C        IF:     n  =  number of ordinary/non-metastable levels
C            R(nxn) = Rate matrix (SEC-1) covering transistions between
C                     all possible pairs of ordinary levels.
C                     row   : final   level
C                     column: initial level
C                     (Inverse R-1(nxn) = 'CMAT(,)' )
C            V(n)   = Recombination rate vector (CM**3 SEC-1) covering
C                     all ordinary levels.
C                     ( = 'VEC()' - ordinary level part ).
C            S(n)   = Recombination contribution vector (CM**3) covering
C                     all ordinary levels ( = 'STV()' ).
C
C           Therefore:  R(nxn).S(n) = V(n)
C
C            =>         S(n)  = R-1(nxn).V(n)
C
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM              , NDLEV              ,
     &           IT                 , NORD
      INTEGER    IS1                , IS2
C-----------------------------------------------------------------------
      REAL*8     COEF
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8     CMAT(NDLEV,NDLEV)  , VEC(NDTEM,NDLEV)   ,
     &           STV(NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IS1=1,NORD
            COEF = 0.0D0
               DO 2 IS2=1,NORD
                  COEF = COEF - ( CMAT(IS1,IS2)*VEC(IT,IORDR(IS2)) )
    2          CONTINUE
            STV(IS1) = COEF
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
