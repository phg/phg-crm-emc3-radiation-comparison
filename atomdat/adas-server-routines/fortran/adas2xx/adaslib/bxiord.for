CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxiord.for,v 1.5 2004/07/06 11:44:24 whitefor Exp $ Date $Date: 2004/07/06 11:44:24 $
CX
      SUBROUTINE BXIORD( IL   ,
     &                   NMET , IMETR ,
     &                   NORD , IORDR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXIORD *********************
C
C  PURPOSE: TO SET UP THE INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C           LEVEL LIST 'IORDR()'.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS (MET. & ORD.)
C
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLE STATES
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLES IN COMPLETE LEVEL LIST
C
C  OUTPUT: (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS.
C  OUTPUT: (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IS      = ENERGY LEVEL ARRAY INDEX
C          (I*4)  IM      = METASTABLE LEVEL NUMBER COUNTER
C          (I*4)  IO      = ORDINARY EXCITED LEVEL NUMBER COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXTERM     ADAS      TERMINATES PROGRAM WITH MESSAGE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C NOTE:    'NMET' + 'NORD' = 'IL'
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C VERSION: 1.5					DATE: 26-06-97
C MODIFIED: H.P. SUMMERS, RICHARD MARTIN
C		- CHANGED LINE IM=IM+1 TO IM=MIN(IM+1,NMET-1)
C		  THIS ENSURES UPPER ARRAY BOUND OF IMETR IS NOT
C		  EXCEEDED.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER  I4UNIT
      INTEGER  IL           , NMET           , NORD
      INTEGER  IS           , IM             , IO
C-----------------------------------------------------------------------
      INTEGER                 IMETR(NMET)    , IORDR(IL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IO = 0
      IM = 0
C-----------------------------------------------------------------------
         DO 1 IS=1,IL
               IF ( IS.EQ.IMETR(IM+1) )THEN
                  IM = MIN(IM+1,NMET-1)
               ELSE
                  IO = IO+1
                  IORDR(IO) = IS
               ENDIF
    1    CONTINUE
C-----------------------------------------------------------------------
      NORD = IO
C-----------------------------------------------------------------------
         IF (IM+1.NE.NMET) THEN
            WRITE(I4UNIT(-1),1000) IM+1 , NMET
            CALL XXTERM
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,32('*'),' BXIORD ERROR ',32('*')/
     &       1X,'ERROR IN NUMBER OF METASTABLE SATES IDENTIFIED.'/
     &       1X,'EXPECTED = ',I5,5X,' FOUND = ',I4)
C
C----------------------------------------------------------------------
C
      RETURN
      END
