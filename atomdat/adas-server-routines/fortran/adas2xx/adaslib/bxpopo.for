CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxpopo.for,v 1.1 2004/07/06 11:45:52 whitefor Exp $ Date $Date: 2004/07/06 11:45:52 $
CX
      SUBROUTINE BXPOPO( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   MAXT   , MAXD  , NMET  , NORD  ,
     &                            DENSA , IMETR , IORDR ,
     &                            LRSEL , LHSEL ,
     &                            RATIA , RATHA ,
     &                   STACK  , STVR  , STVH  ,
     &                   POPAR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXPOPO *********************
C
C  PURPOSE: TO CONSTRUCT ORDINARY/NON-METASTABLE LEVEL POPULATIONS.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 ->'NDTEM')
C  INPUT :  (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 ->'NDDEN')
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES LEVELS ( 1 ->'NDMET')
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ( 1 ->'NDLEV')
C
C  INPUT :  (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (L*4)  LRSEL   = .TRUE.  => FREE ELECTRON RECOMBINATION
C                                       REQUESTED.
C                          = .FALSE. => FREE ELECTRON RECOMBINATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN REQUESTED.
C                          = .FALSE. => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN NOT REQUESTED.
C
C  INPUT :  (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C  INPUT :  (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C
C  INPUT : (R*4) STACK(,,,)= ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVR(,,)  = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVH(,,)  = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C
C  I/O   :  (R*8)  POPAR(,,)= LEVEL POPULATIONS
C                              1st DIMENSION: LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C                               ON INPUT : CONTAINS POPULATIONS FOR
C                                          METASTABLE LEVELS ONLY.
C                               ON OUTPUT: CONTAINS POPULATIONS FOR
C                                          ALL LEVELS.
C
C           (R*8) DCOEF     = DENSITY MULTIPLIED BY RELEVANT RATIOS  FOR
C                             CALCULATING RECOMBINATION CONTRIBUTIONS.
C
C           (I*4) IT        = TEMPERATURE ARRAY INDEX
C           (I*4) IN        = DENSITY ARRAY INDEX
C           (I*4) IO        = ORDINARY LEVEL ARRAY INDEX
C           (I*4) IM        = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  20/05/93- P BRIDEN: STACK ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM        , NDDEN        , NDMET   , NDLEV   ,
     &          MAXT         , MAXD         , NMET    , NORD
      INTEGER   IT           , IN           , IM      , IO
C-----------------------------------------------------------------------
      REAL*8    DCOEF
C-----------------------------------------------------------------------
      LOGICAL   LRSEL        , LHSEL
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)                , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDDEN)                ,
     &          RATIA(NDDEN)                , RATHA(NDDEN)
      REAL*8    STVR(NDLEV,NDTEM,NDDEN)     , STVH(NDLEV,NDTEM,NDDEN)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
      REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  ORDINARY/NON-METSTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         DO 1 IN=1,MAXD
            DO 2 IT=1,MAXT
               DO 3 IO=1,NORD
                  POPAR(IORDR(IO),IT,IN)=0.0D0
                     DO 4 IM=1,NMET
                        POPAR(IORDR(IO),IT,IN)= POPAR(IORDR(IO),IT,IN) +
     &                                        ( POPAR(IMETR(IM),IT,IN) *
     &                                        DBLE(STACK(IO,IM,IT,IN)) )
    4                CONTINUE
    3          CONTINUE
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD FREE ELECTRON RECOMB. CONTRIBUTION TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            DO 5 IN=1,MAXD
               DCOEF = RATIA(IN)*DENSA(IN)
                  DO 6 IT=1,MAXT
                     DO 7 IO=1,NORD
                        POPAR(IORDR(IO),IT,IN)=POPAR(IORDR(IO),IT,IN) +
     &                                         ( DCOEF*STVR(IO,IT,IN) )
    7                CONTINUE
    6             CONTINUE
    5       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD CHARGE EXCHANGE RECOMB. CONTRIBUTION TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            DO 8 IN=1,MAXD
               DCOEF = RATHA(IN)*RATIA(IN)*DENSA(IN)
                  DO 9 IT=1,MAXT
                     DO 10 IO=1,NORD
                        POPAR(IORDR(IO),IT,IN)=POPAR(IORDR(IO),IT,IN) +
     &                                         ( DCOEF*STVH(IO,IT,IN) )
   10                CONTINUE
    9             CONTINUE
    8       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
