CX UNIX - SCCS info: @(#)bxcoef.for	1.1    Date 03/17/03
CX
       subroutine bxcoef(idlev  , idtrn  , idtem  , idden  , idmet ,
     &                   nmet   , imetr  , nord   , iordr  ,  
     &                   maxt   , tine   , tinp   , tinh   , ifout ,
     &                   maxd   , dine   , dinp   , idout  ,
     &                   lpsel  , lzsel  , liosel ,
     &                   lhsel  , lrsel  , lisel  , lnsel  ,
     &                   iz     , iz0    , iz1    , 
     &                   npl    , bwno   , bwnoa  , prtwta ,
     &                   npla   , ipla   , zpla   , nplr   , npli  ,
     &                   il     , maxlev , xja    , wa     , zeff  ,
     &                   xia    , er     ,
     &                   icnte  , icntp  , icntr  , icnth  , icnti ,
     &                   icntl  , icnts  , 
     &                   ietrn  , iptrn  , irtrn  , ihtrn  , iitrn ,
     &                   iltrn  , istrn  ,
     &                   ie1a   , ie2a   , ip1a   , ip2a   , aa    ,
     &                   ia1a   , ia2a   , auga   ,
     &                   il1a   , il2a   , wvla   ,
     &                   is1a   , is2a   , lss04a ,
     &                   i1a    , i2a    ,
     &                   nv     , scef   , scom   ,
     &                   dsnexp , dsninc , iunt27 , open27 ,  
     &                   stckm  , stvr   , stvi   , stvh   ,
     &                   stvrm  , stvim  , stvhm  , stack  ,
     &                   ltick  
     &                  )


       implicit none

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXCOEF *********************
C
C  PURPOSE: Calculates collisional-radiative populations for series 8 
C           codes.  Modification of bgcoef.for 
C
C  CALLING PROGRAM: Series 2 population codes.
C
C
C  PARAMETERS:
C           (I*4)  ndlev   = parameter = max. number of levels allowed
C           (I*4)  ndtrn   = parameter = max. no. of transitions allowed
C           (I*4)  ndtem   = parameter = max. no. of temperatures allowed
C           (I*4)  ndden   = parameter = max. number of densities allowed
C           (I*4)  ndmet   = parameter = max. no. of metastables allowed
C
C
C  INPUT :  (I*4)  nmet    = number of metastables (1 <= nmet <= 'ndmet')
C  INPUT :  (I*4)  imetr() = index of metastable in complete level list
C                            (array size = 'ndmet' )
C  INPUT :  (I*4)  nord    = number of ordinary levels (1 <= nmet <= 'ndmet')
C  INPUT :  (I*4)  iordr() = index of metastable in complete level list
C                            (array size = 'ndmet' )
C
C  INPUT :  (I*4)  maxt    = number of input temperatures ( 1 -> 'ndtem')
C  INPUT :  (R*8)  tine()  = electron temperatures (units: see 'ifout')
C  INPUT :  (R*8)  tinp()  = proton temperatures   (units: see 'ifout')
C  INPUT :  (R*8)  tinh()  = neutral hydrogen temperatures
C  INPUT :  (I*4)  ifout   = 1 => input temperatures in kelvin
C                          = 2 => input temperatures in ev
C                          = 3 => input temperatures in reduced form
C
C  INPUT :  (I*4)  maxd    = number of input densities ( 1 -> 'ndden')
C  INPUT :  (R*8)  dine()  = electron densities  (units: see 'idout')
C  INPUT :  (R*8)  dinp()  = proton densities    (units: see 'idout')
C  INPUT :  (I*4)  idout   = 1 => input densities in cm-3
C                          = 2 => input densities in reduced form
C
C  INPUT :  (L*4)  lpsel   = .true.  => include proton collisions
C                          = .false. =>do not include proton collisions
C  INPUT :  (L*4)  lzsel   = .true.  => scale proton collisions with
C                                       plasma z effective'zeff'.
C                          = .false. => do not scale proton collisions
C                                       with plasma z effective 'zeff'.
C                            (only used if 'lpsel=.true.')
C  INPUT :  (L*4)  liosel  = .true.  => include ionisation rates
C                          = .false. => do not include ionisation rates
C
C  INPUT :  (L*4)  lhsel   = .true.  => include charge transfer from
C                                       neutral hydrogren.
C                          = .false. => do not include charge transfer
C                                       from neutral hydrogren.
C  INPUT :  (L*4)  lrsel   = .true.  => include free electron
C                                       recombination.
C                          = .false. => do not include free electron
C                                       recombination.
C  INPUT :  (L*4)  lisel   = .true.  => include electron impact
C                                       ionisation.
C                          = .false. => do not include free electron
C                                       recombination.
C  INPUT :  (L*4)  lnsel   = .true.  => include projected bundle-n data
C                                       from datafile if available
C                          = .false. => do not include projected bundle-n
C                                       data
C  INPUT :  (I*4)  iz      = recombined ion charge
C  INPUT :  (I*4)  iz0     = nuclear charge
C  INPUT :  (I*4)  iz1     = recombining ion charge
C                            (note: iz1 should equal iz+1)
C
C  INPUT :  (I*4)  npl     = no. of metastables of (z+1) ion accessed
C                            by excited state ionisation in copase
C                            file with ionisation potentials given
C                            on the first data line
C  INPUT :  (R*8)  bwno    = ionisation potential (cm-1) of lowest parent
C  INPUT :  (R*8)  bwnoa() = ionisation potential (cm-1) of parents
C  INPUT :  (R*8)  prtwta()= weight for parent associated with bwnoa()
C
C  INPUT :  (I*4)  npla()  = no. of parent/zeta contributions to ionis.
C                            of level
C  INPUT :  (I*4)  ipla(,) = parent index for contributions to ionis.
C                            of level
C                            1st dimension: parent index
C                            2nd dimension: level index
C  INPUT :  (I*4)  zpla(,) = eff. zeta param. for contributions to ionis.
C                            of level
C                            1st dimension: parent index
C                            2nd dimension: level index
C  INPUT :  (I*4)  nplr    = no. of active metastables of (z+1) ion
C  INPUT :  (I*4)  npli    = no. of active metastables of (z-1) ion
C
C  INPUT :  (I*4)  il      = input data file: number of energy levels
C  INPUT :  (I*4)  maxlev  = highest index level in read transitions
C  INPUT :  (R*8)  xja()   = quantum number (j-value) for level 'ia()'
C                            note: (2*xja)+1 = statistical weight
C  INPUT :  (R*8)  wa()    = energy relative to level 1 (cm-1)
C                            dimension: level index
C  INPUT :  (R*8)  zeff    = plasma z effective ( if 'lzsel' = .true.)
C                            (if 'lzsel' = .false. => 'zeff=1.0')
C
C  INPUT :  (R*8)  xia()   = energy relative to ion. pot. (rydbergs)
C                            dimension: level index
C           (R*8)  er()    = energy relative to level 1 (rydbergs)
C                            dimension: level index
C
C  INPUT :  (I*4)  icnte   = number of electron impact transitions input
C  INPUT :  (I*4)  icntp   = number of proton impact transitions input
C  INPUT :  (I*4)  icntr   = number of free electron recombinations input
C  INPUT :  (I*4)  icnth   = no. of charge exchange recombinations input
C  INPUT :  (I*4)  icnti   = no. of ionisations to z input
C  INPUT :  (I*4)  icntl   = no. of satellite dr recombinations input
C  INPUT :  (I*4)  icnts   = no. of ionisations to z+1 input
C
C  INPUT :  (I*4)  ietrn() = electron impact transition:
C                            index values in main transition arrays which
C                            represent electron impact transitions.
C  INPUT :  (I*4)  iptrn() = proton impact transition:
C                            index values in main transition arrays which
C                            represent proton impact transitions.
C  INPUT :  (I*4)  irtrn() = free electron recombination:
C                            index values in main transition arrays which
C                            represent free electron recombinations.
C  INPUT :  (I*4)  ihtrn() = charge exchange recombination:
C                            index values in main transition arrays which
C                            represent charge exchange recombinations.
C  INPUT :  (I*4)  iitrn() = electron impact ionisation:
C                            index values in main transition arrays which
C                            represent ionisations from the lower stage
C  INPUT :  (I*4)  iltrn() = satellite dr recombination:
C                            index values in main transition arrays which
C                            represent satellite dr recombinations.
C  INPUT :  (I*4)  istrn() = electron impact ionisation:
C                            index values in main transition arrays which
C                            represent ionisations to upper stage ion.
C
C  INPUT :  (I*4)  ie1a()  = electron impact transition:
C                             lower energy level index
C  INPUT :  (I*4)  ie2a()  = electron impact transition:
C                             upper energy level index
C  INPUT :  (I*4)  ip1a()  = proton impact transition:
C                             lower energy level index
C  INPUT :  (I*4)  ip2a()  = proton impact transition:
C                             upper energy level index
C  INPUT :  (R*8)  aa()    = electron impact transition: a-value (sec-1)
C
C  INPUT :  (I*4)  ia1a()  = auger transition:
C                             parent energy level index
C  INPUT :  (I*4)  ia2a()  = auger transition:
C                             recombined ion energy level index
C  INPUT :  (R*8)  auga()  = auger transition: aug-value (sec-1)
C                             recombined ion energy level index
C  INPUT :  (I*4)  il1a()  = satellite dr transition:
C                             recomnining ion  index
C  INPUT :  (I*4)  il2a()  = satellite dr transition:
C                             recombined ion index
C  INPUT :  (R*8)  wvla()  = satellite dr transition: parent wvlgth.(a)
C                             dr satellite line index
C  INPUT :  (I*4)  is1a()  = ionising transition:
C                             ionised ion  index
C  INPUT :  (I*4)  is2a()  = ionising transition:
C                             ionising ion index
C  INPUT :  (L*4)  lss04a(,)= .true. => ionis. rate set in adf04 file:
C                             .false.=> not set in adf04 file
C                             1st dim: level index
C                             2nd dim: parent metastable index
C
C  INPUT :  (I*4)  nv      = input data file: number of gamma/temperature
C                            pairs for a given transition.
C  INPUT :  (R*8)  scef()  = input data file: electron temperatures (k)
C  INPUT :  (R*8)  scom(,) = transition:
C                             gamma values    input :  (case ' ' & 'p')
C                             rate coefft. (cm3 sec-1) (case 'h' & 'r')
C                            1st dimension - temperature 'scef()'
C                            2nd dimension - transition number
C
C  INPUT :  (C*(*))dsnexp  = expansion data set name
C  INPUT :  (C*(*))dsninc  = input specific ion file (adf04)
C  INPUT :  (I*4)  iunt27  = output unit for results from expansion routine
C  INPUT :  (L*4)  open27  = .true.  => file allocated to unit 7.
C                          = .false. => no file allocated to unit 7.
C
C
C
C  OUTPUT : (R*8)  
C                  
C                  
C                  
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          b8getp     ADAS      Fetch expansion data
C          bxchkm     ADAS      Checks if transition exist to metastable
C          bxiord     ADAS      Sets  up ordinary level index.
C          bxrate     ADAS      Calculates exc. & de-exc. rate coeffts.
C          b8loss     ADAS      Calculates direct line power loss
C          b8rcom     ADAS      Establishes recombination rate coeffts.
C          bxmcra     ADAS      Constructs a-value matrix.
C          bxmcrc     ADAS      Constructs exc./de-exc. rate coef matrix
C          b8mcca     ADAS      Constructs whole rate matrix.
C          bxmcma     ADAS      Constructs ordinary level rate matrix.
C          bxstka     ADAS      Stack up ordinary pop. dependence on met
C          b8stkb     ADAS      Stack up recomb. contribution for ord.
C          bxstkc     ADAS      Stack up transition rate between mets.
C          b8stkd     ADAS      Stack up recomb rate for each met. level
C          b8stke     ADAS      Stack up recomb(+3-body) contri.for ord.
C          b8stkf     ADAS      Stack up recomb(+3-body) for each met.
C          bxmpop     ADAS      Calculate basic met. level populations.
C          b8stvm     ADAS      Calculate met. level recomb. coeffts.
C          xxtcon     ADAS      Converts ispf entered temps. to ev.
C          xxdcon     ADAS      Converts ispf entered dens. to cm-3.
C          xxrate     ADAS      Calculates exc. & de-exc. rate coeffts.
C                               For unit gamma value.
C          xxminv     ADAS      Inverts matrix and solves equations.
C
C
C
C  This is a subroutine version of adas208 without the search for ionisation
C  rates from adf07 files.  Modified original 'bgcoef.for' subroutine by
C  Martin O'Mullane to add extra variables to the returned parameter set.  
C  
C  AUTHOR:  H. P. Summers, University of Strathclyde
C          JA8.08
C          Tel. 0141-553-4196
C
C  DATE:    20/04/02
C
C  UPDATE: 
C
C  VERSION  : 1.1                          
C  DATE     : 20-01-2003
C  MODIFIED : Martin O'Mullane  
C             - based on bgcoef v 1.1.
C             - Remove calculation of populations and lsseta as input.
C
C  VERSION  : 1.2                          
C  DATE     : 29-05-2003
C  MODIFIED : Martin O'Mullane  
C             - Increase NDLEV to 1200.
C
C  VERSION  : 1.3                          
C  DATE     : 05-12-2008
C  MODIFIED : Adam Foster
C             - Increase number of levels to 3500
C             - Increase number of transitions to 380000
C             - Incorporate changes from offline version 1.5:
C             - Change first dimension of ipla and zpla to 2*ndmet
C               to accommodate changes in xxdata_04 permitting
C               Jpj parents.
C
C  VERSION  : 1.3                          
C  DATE     : 29-09-2009
C  MODIFIED : Martin O'Mullane  
C             - Revert ipla and zpla changes for non-offline version.
C
C  VERSION  : 1.4                          
C  DATE     : 24-04-2019
C  MODIFIED : Martin O'Mullane
C             - Filename lengths are defined by argument length.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       integer   ndlev  , ndtrn  , ndtem  , ndden  , ndmet
       integer   l1     , l2
       integer   pipeou , one
C-----------------------------------------------------------------------
       real*8    d1     , wn2ryd
C-----------------------------------------------------------------------
       parameter( ndlev  = 4000, ndtrn  = 500000, ndtem = 24 , 
     &            ndden  = 24  , ndmet  = 4 )
       parameter( l1     = 1   , l2     = 2 )
       parameter( pipeou = 6   , one    = 1 )
C-----------------------------------------------------------------------
       parameter( d1 = 1.0d0 , wn2ryd = 9.11269d-06 )
C-----------------------------------------------------------------------
       integer   idlev  , idtrn  , idtem  , idden  , idmet 
       integer   iunt27 
       integer   iz        , iz0       , iz1     ,
     &           npl       , nplr      , npli    , npl3    ,
     &           il        , nv        ,  
     &           maxlev    , nmet      , nord    , ifout   , idout   ,
     &           maxt      , maxd      ,  
     &           icnte     , icntp     , icntr   , icnth   , icnti   ,
     &           icntl     , icnts     ,
     &           in        , is        , it      , imet    ,
     &           jmet      , ip        , i       ,  
     &           itin      , inin      , ilev    , ipp   , j
C-----------------------------------------------------------------------
       real*8    r8necip   , alfred    , x
       real*8    bwno      , zeff      , zeffsq  , dmint
       real*8    r8expe    , spref
C-----------------------------------------------------------------------
       character dsnexp*(*), dsninc*(*)  
C-----------------------------------------------------------------------
       logical   open27    , ltick
       logical   lpsel     , lzsel     , liosel  ,
     &           lhsel     , lrsel     , lisel   , lnsel    , lsolve ,
     &           lpdata 
C-----------------------------------------------------------------------
       integer   imetr(ndmet)     , iordr(ndlev)     , npla(ndlev)  ,
     &           i1a(ndtrn)       , i2a(ndtrn)       ,
     &           ipla(ndmet,ndlev)
       integer   ietrn(ndtrn)     , iptrn(ndtrn)     ,
     &           irtrn(ndtrn)     , ihtrn(ndtrn)     ,
     &           iitrn(ndtrn)     , iltrn(ndtrn)     ,
     &           istrn(ndtrn)     ,  
     &           ie1a(ndtrn)      , ie2a(ndtrn)      ,
     &           ip1a(ndtrn)      , ip2a(ndtrn)      ,
     &           ia1a(ndtrn)      , ia2a(ndtrn)      ,
     &           il1a(ndlev)      , il2a(ndlev)      ,
     &           is1a(ndlev)      , is2a(ndlev)
C-----------------------------------------------------------------------
       real*4    stvr(idlev,idtem,idden,idmet) ,
     &           stvh(idlev,idtem,idden,idmet) ,
     &           stvi(idlev,idtem,idden,idmet)
       real*4    stack(idlev,idmet,idtem,idden)
C-----------------------------------------------------------------------
       real*8    scef(14)        , bwnoa(ndmet)     , prtwta(ndmet) ,
     &           tine(ndtem)     , tinp(ndtem)      , tinh(ndtem)   ,
     &           teva(ndtem)     , tpva(ndtem)      , thva(ndtem)   ,
     &           tea(ndtem)      , tpa(ndtem)       , tha(ndtem)    ,
     &           dine(ndden)     , dinp(ndden)      ,
     &           densa(ndden)    , denspa(ndden)    ,
     &           aa(ndtrn)       , auga(ndtrn)      , wvla(ndlev)   ,
     &           xja(ndlev)      , wa(ndlev)        ,
     &           xia(ndlev)      , er(ndlev)        ,
     &           cie(ndlev)      , rhs(ndlev)       ,
     &           ciepr(ndlev,ndmet) , v3pr(ndlev,ndmet)  ,
     &           vrred(ndmet,ndmet) , vhred(ndmet,ndmet) ,
     &           vired(ndmet,ndmet) , vionr(ndmet,ndmet) ,
     &           vcrpr(ndmet,ndmet) , zpla(ndmet,ndlev)
       real*8    scom(14,ndtrn)              ,
     &           excre(ndtem,ndtrn)          , excrp(ndtem,ndtrn)     ,
     &           dexcre(ndtem,ndtrn)         , dexcrp(ndtem,ndtrn)    ,
     &           vech(ndtem,ndlev,ndmet)     , vecr(ndtem,ndlev,ndmet),
     &           veci(ndtem,ndlev,ndmet)     , cra(ndlev,ndlev)       ,
     &           crce(ndlev,ndlev)           , crcp(ndlev,ndlev)      ,
     &           cc(ndlev,ndlev)             , cmat(ndlev,ndlev)      ,
     &           cred(ndmet,ndmet)           , crmat(ndmet,ndmet)

       real*8    sgrda(ndtem,ndmet,ndmet)   , esgrda(ndtem,ndmet,ndmet),
     &           smeta(ndtem,ndmet,ndmet)   , esmeta(ndtem,ndmet,ndmet),
     &           sorda(ndtem,ndlev,ndmet)   , esorda(ndtem,ndlev,ndmet)
     
       real*8    dvecr(ndtem,ndlev,ndmet)
       real*8    dciepr(ndtem,ndlev,ndmet)   , dv3pr(ndtem,ndlev,ndmet)
       real*8    dcie(ndtem,ndlev)
 
       real*8    pcc(ndlev,ndlev)            , pcie(ndlev)
       real*8    pciepr(ndlev,ndmet)         , pv3pr(ndlev,ndmet)
       real*8    pvcrpr(ndmet,ndmet)         , pvecr(ndlev,ndmet)
       real*8    pr(ndmet,ndtem,ndden)

       real*8    stckm(idmet,idtem,idden)      ,
     &           stvrm(idmet,idtem,idden,idmet) ,
     &           stvhm(idmet,idtem,idden,idmet) ,
     &           stvim(idmet,idtem,idden,idmet)
       real*8    fvcred(ndmet,ndmet,ndtem,ndden) ,
     &           fvrred(ndmet,ndmet,ndtem,ndden) ,
     &           fvhred(ndmet,ndmet,ndtem,ndden) ,
     &           fvired(ndmet,ndmet,ndtem,ndden) ,
     &           fvionr(ndmet,ndmet,ndtem,ndden) ,
     &           fvcrpr(ndmet,ndmet,ndtem,ndden)
       real*8    pla1(ndlev)           ,
     &           plas1(ndmet)          , swvln(ndmet)     
C-----------------------------------------------------------------------
       logical   lmetr(ndmet)          , ltrng(ndtem,3)   
       logical   lsseta(idmet,idmet)   , lss04a(idlev,idmet)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ******************************* start ********************************
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  Check maximum dimensions
C-----------------------------------------------------------------------

      if (ndlev.ne.idlev) stop
     &                    ' BXCOEF parameter mismatch: idlev .ne. ndlev'
      if (ndtrn.ne.idtrn) stop
     &                    ' BXCOEF parameter mismatch: idtrn .ne. ndtrn'
      if (ndtem.ne.idtem) stop
     &                    ' BXCOEF parameter mismatch: idtem .ne. ndtem'
      if (ndden.ne.idden) stop
     &                    ' BXCOEF parameter mismatch: idden .ne. ndden'
      if (ndmet.ne.idmet) stop
     &                    ' BXCOEF parameter mismatch: idmet .ne. ndmet'



C-----------------------------------------------------------------------
C Check if electron impact transitions exist to the selected metastable
C-----------------------------------------------------------------------

      call bxchkm( nmet   , imetr , icnte , ie1a , lmetr )

C-----------------------------------------------------------------------
C Convert input temperatures into ev:  (tin? -> t?va)  (? = e,p,h )
C-----------------------------------------------------------------------

      call xxtcon( ifout, l2, iz1, maxt , tine  , teva   )
      call xxtcon( ifout, l2, iz1, maxt , tinp  , tpva   )
      call xxtcon( ifout, l2, iz1, maxt , tinh  , thva   )

C-----------------------------------------------------------------------
C Convert input temperatures into kelvin:  (tin? -> t?a)  (? = e,p,h )
C-----------------------------------------------------------------------

      call xxtcon( ifout, l1, iz1, maxt , tine  , tea    )
      call xxtcon( ifout, l1, iz1, maxt , tinp  , tpa    )
      call xxtcon( ifout, l1, iz1, maxt , tinh  , tha    )

C-----------------------------------------------------------------------
C Convert input densities into cm-3:
C-----------------------------------------------------------------------

      call xxdcon( idout, l1, iz1, maxd , dine  , densa  )
      call xxdcon( idout, l1, iz1, maxd , dinp  , denspa )


C-----------------------------------------------------------------------
C  only accept ionisation data from s-lines in adf04 files
C-----------------------------------------------------------------------

      do imet = 1 , nmet
         is = imetr(imet)
      end do
   
      do imet = 1 , ndmet
         do ip=1, ndmet
            lsseta(imet,ip)=.false.
         end do
      end do
      
C-----------------------------------------------------------------------
C  initialise vecr, vech, veci to zero
C-----------------------------------------------------------------------

      do is=1,ndlev
         do it=1,ndtem
           dcie(it,is)=0.0d0
           do ip=1,ndmet
               vecr(it,is,ip)   = 0.0D0
               dvecr(it,is,ip)  = 0.0D0
               vech(it,is,ip)   = 0.0D0
               veci(it,is,ip)   = 0.0D0
               dciepr(it,is,ip) = 0.0D0
               dv3pr(it,is,ip)  = 0.0D0
           end do
         end do
      end do



C-----------------------------------------------------------------------
C Calculate electron impact transition excitation & de-excition rate coefs.
C Note: at this stage assume gamma = 1.
C-----------------------------------------------------------------------
        

      call xxrate( ndtrn , ndtem  , ndlev ,
     &             icnte , maxt   ,
     &             xja   , er     , tea   ,
     &             ie1a  , ie2a   ,
     &             excre , dexcre
     &           )
     

C-----------------------------------------------------------------------
C Calculate line power loses for all lines and specific line.
C-----------------------------------------------------------------------

      call b8loss( ndtrn , ndlev  , ndmet ,
     &             icnte , nmet   , imetr , istrn  ,
     &             xja   , er     , aa    ,
     &             ie1a  , ie2a   ,
     &             plas1 , swvln  , pla1
     &            )

C-----------------------------------------------------------------------
C Calculate proton impact transition excitation & de-excition rate coefs.
C Note: at this stage assume gamma = 1.             (only if required).
C-----------------------------------------------------------------------

      if (lpsel) then
         call xxrate( ndtrn , ndtem  , ndlev ,
     &                icntp , maxt   ,
     &                xja   , er     , tpa   ,
     &                ip1a  , ip2a   ,
     &                excrp , dexcrp
     &              )
      endif

C-----------------------------------------------------------------------
C Setup index of ordinary excited levels in complete level list 'iordr'
C-----------------------------------------------------------------------

      call bxiord( il   ,
     &             nmet , imetr ,
     &             nord , iordr
     &           )

C-----------------------------------------------------------------------
C Electron excitation data option - spline gamma's to give rate coeffts.
C-----------------------------------------------------------------------

      call bxrate( ndtem      , ndtrn  , d1     ,
     &             nv         , scef   , scom   ,
     &             maxt       , tea    ,
     &             icnte      , ietrn  ,
     &             excre      , dexcre ,
     &             ltrng(1,1)
     &           )

C-----------------------------------------------------------------------
C Proton excitation data option - spline gamma's to give rate coeffts.
C-----------------------------------------------------------------------

      if (lpsel) then
         zeffsq = zeff*zeff
         call bxrate( ndtem      , ndtrn  , zeffsq ,
     &                nv         , scef   , scom   ,
     &                maxt       , tpa    ,
     &                icntp      , iptrn  ,
     &                excrp      , dexcrp ,
     &                ltrng(1,2)
     &              )
      endif

C----------------------------------------------------------------------
C Free electron recombination data option - spline coefficients
C----------------------------------------------------------------------

      if (lrsel) then
         call b8rcom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                nv    , scef       , scom   ,
     &                maxt  , tea        ,
     &                icntr , irtrn      , i2a    , i1a    ,
     &                dvecr  , ltrng(1,1)
     &              )
      endif

C----------------------------------------------------------------------
C Electron impact ionisation data option - spline coefficients
C----------------------------------------------------------------------

      if (lisel) then
         call b8rcom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                nv    , scef       , scom   ,
     &                maxt  , tea        ,
     &                icnti , iitrn      , i2a    , i1a    ,
     &                veci  , ltrng(1,1)
     &              )
      endif

C----------------------------------------------------------------------
C Charge exchange recombination data option - spline coefficients
C----------------------------------------------------------------------

      if (lhsel) then
         call b8rcom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                nv    , scef       , scom   ,
     &                maxt  , tha        ,
     &                icnth , ihtrn      , i2a    , i1a    ,
     &                vech  , ltrng(1,3)
     &              )
      endif

C----------------------------------------------------------------------
C Ionisation z --> z+1 data option - spline coefficients
C----------------------------------------------------------------------

      if (liosel) then
         call b8scom( ndtem , ndtrn      , ndlev  , ndmet  ,
     &                il    , wa         , npl    , bwnoa  , 
     &                nmet  , imetr      , nord   , iordr  ,
     &                nv    , scef       , scom   ,
     &                maxt  , tea        , 
     &                icnts , istrn      , is1a   , is2a   ,
     &                lsseta, sgrda      , esgrda ,
     &                smeta , esmeta     , sorda  , esorda ,
     &                ltrng(1,1) 
     &              )
      
      endif

C----------------------------------------------------------------------
C Set up A-value transition matrix 'cra' (units: sec-1)
C----------------------------------------------------------------------

      call bxmcra( ndtrn , ndlev  ,
     &             icnte , il     ,
     &             ie1a  , ie2a   ,
     &             aa    ,
     &             cra
     &           )

C----------------------------------------------------------------------
C Initialise projected indirect couplings
C----------------------------------------------------------------------

      lpdata = .false.
      if( lnsel ) then
          itin = 1
          inin = 1

           call b8getp( iz0    , iz1    , dsnexp , dsninc ,
     &                  ndlev  , ndmet  , ndtem  , ndden  ,
     &                  maxd   , maxt   , densa  , tea    ,
     &                  lpdata , liosel , lrsel  , lhsel  ,
     &                  il     , itin   , inin   ,
     &                  pcc    , pcie   , pciepr , pv3pr  ,
     &                  pvcrpr , pvecr  , iunt27 , open27 ,
     &                  pr
     &                )

      endif

C-----------------------------------------------------------------------
C  set up vector of ionisation and three-body recombination coefficients
C  if required - note parent resolved data
C
C  the original implementation here was very messy so this version 
C  removes a lot of the commented out code.
C
C  note that the indexing is quite confusing; most quantities are
C  indexed by parent and level (as well as temperature) but these 
C  can vary 
C
C      dciepr(it, il, ip)  :  il level list
C      lss04a(il, ip)      :  il level list
C      zpla(ip, il)        :  il level list
C      sorda(it, il, ip)   :  il ordinary level list
C
C  the level list is 1-number of levels, ordinary is 1 - number of levels
C  less number of metastables.
C
C-----------------------------------------------------------------------

       if (liosel) then

           do 122 it = 1,maxt
           
              do 5 is=1,maxlev
                 do 15 ip=1,npla(is)
                 
C check if (s-lines) from ordinary levels are present in the adf04 file,
C if not use ecip approximation.
                    
                    ilev = is - nmet
                    ipp  = ipla(ip,is)
                    if (ilev.gt.0.and.lss04a(is,ipp)) then
                    
                        spref = r8expe(sorda(it,ilev,ipp),
     &                                 esorda(it,ilev,ipp),'zero')
                        if ( zpla(ip,is) .gt. 0.0d0) then
                           x=xia(is)+wn2ryd*(bwnoa(ipla(ip,is))-bwno)
                           dciepr(it,is,ipla(ip,is))=spref
                           alfred = 3.30048d-24 *  spref * 
     &                              (1.5789d5/tea(it))**1.5d0 * 
     &                              exp(1.5789d5*x/tea(it))
                           dv3pr(it,is,ipla(ip,is))=
     &                                (2.0d0*xja(is)+1.0d0)*alfred/
     &                                       prtwta(ipla(ip,is))
                        else
                           dciepr(it,is,ipla(ip,is)) = 0.0d+0
                           dv3pr(it,is,ipla(ip,is))  = 0.0d+0
                        endif
                        
                    else
                     
                       if ( zpla(ip,is) .gt. 0.0d0) then
                          x=xia(is)+wn2ryd*(bwnoa(ipla(ip,is))-bwno)
                          dciepr(it,is,ipla(ip,is))=
     &                                r8necip(iz,x,zpla(ip,is)
     &                                       ,tea(it),alfred)
                          dv3pr(it,is,ipla(ip,is))=
     &                               (2.0d0*xja(is)+1.0d0)*alfred/
     &                                      prtwta(ipla(ip,is))
                       else
                          dciepr(it,is,ipla(ip,is)) = 0.0d+0
                          dv3pr(it,is,ipla(ip,is))  = 0.0d+0
                       endif
                       
                    endif

                    dcie(it,is) = dcie(it,is) +
     &                            dciepr(it,is,ipla(ip,is))
   15            continue
    5         continue

C-----------------------------------------------------------------------
C  substitute special szd rates from b8gets call if available
C  adjust three-body rates accordingly
C-----------------------------------------------------------------------
  
              do 14 imet=1,nmet
                do 13 ip=1,npl
                  if (lsseta(imet,ip))then
                     spref=r8expe(smeta(it,imet,ip),
     &                            esmeta(it,imet,ip),'zero')
                     if (spref.gt.0.0d0.and.
     &                   dciepr(it,imetr(imet),ip).gt.0.0d0) then
                         dv3pr(it,imetr(imet),ip)=spref
     &                             * dv3pr(it,imetr(imet),ip)/
     &                               dciepr(it,imetr(imet),ip)
                     endif
                     dcie(it,imetr(imet))=dcie(it,imetr(imet)) -
     &                                dciepr(it,imetr(imet),ip)+
     &                                spref
                     dciepr(it,imetr(imet),ip)=spref
                       

                  endif
   13           continue
   14       continue

  122    continue

       endif
         


C-----------------------------------------------------------------------
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C Increment over over all input temperature values
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C-----------------------------------------------------------------------
	


      do 4 it=1,maxt
       
C-----------------------------------------------------------------------
C set up excitation/de-exciatation rate coefft. matrices for given temp.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C electron impact transitions 'crce' - (units: cm**3/sec-1)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

         call bxmcrc( ndtem , ndtrn  , ndlev  ,
     &                it    , icnte  , il     ,
     &                ie1a  , ie2a   ,
     &                excre , dexcre ,
     &                crce
     &              )

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C proton impact transitions 'crcp' - (units: cm**3/sec-1) - if selected
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

         if (lpsel) then
            call bxmcrc( ndtem , ndtrn  , ndlev  ,
     &                   it    , icntp  , il     ,
     &                   ip1a  , ip2a   ,
     &                   excrp , dexcrp ,
     &                   crcp
     &                  )
         endif

C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C increment over over all input density values
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *

         do 6 in=1,maxd

C  send a progress event if requested

             if (ltick) then
                write(pipeou,*)one
                call xxflsh(pipeou)
             endif

C-----------------------------------------------------------------------
C  initialise vectors to zero
C-----------------------------------------------------------------------

             do is=1,ndlev
                cie(is)  = 0.0d0
                pcie(is) = 0.0d+0
                do i = 1,ndlev
                  pcc(is,i) = 0.0d0
                end do
                do ip=1,ndmet
                   ciepr(is,ip)  = 0.0d0
                   pciepr(is,ip) = 0.0d0
                   v3pr(is,ip)   = 0.0d0
                   pv3pr(is,ip)  = 0.0d0
                   pvecr(is,ip)  = 0.0d0
                end do
             end do


C-----------------------------------------------------------------------
C set up indirect matrices
C-----------------------------------------------------------------------

             if (lpdata) then
                        
                 itin = it
                 inin = in

                 call b8getp( iz0    , iz1    , dsnexp , dsninc ,
     &                        ndlev  , ndmet  , ndtem  , ndden  ,
     &                        maxd   , maxt   , densa  , tea    ,
     &                        lpdata , liosel , lrsel  , lhsel  ,
     &                        il     , itin   , inin   ,
     &                        pcc    , pcie   , pciepr , pv3pr  ,
     &                        pvcrpr , pvecr  , iunt27 , open27 ,
     &                        pr
     &                      )

             endif

C-----------------------------------------------------------------------
C  Add indirect couplings onto ionisation and recombination vectors
C
C  If indirect coupling have been initiated, set liosel = true
C                                                lrsel  = true
C
C  Note cie has units of cm3s-1 while pcie and pciepr have units of s-1
C       v3pr has units of cm6s-1 while pv3pr has units of cm3s-1
C       vecr and pvecr have units of cm3s-1
C
C  Note that cie does not need to be adjusted
C-----------------------------------------------------------------------
C
             if (lpdata) then

                 liosel = .TRUE.
                 lrsel  = .TRUE.
                 do is=1,maxlev

                   cie(is) = dcie(it,is)
                   do ip = 1,npl
                     ciepr(is,ip) = dciepr(it,is,ip)
     &                               + pciepr(is,ip) / densa(in)
                     v3pr(is,ip) = dv3pr(it,is,ip)
     &                               + pv3pr(is,ip) / densa(in)
                     vecr(it,is,ip) = dvecr(it,is,ip)
     &                               + pvecr(is,ip)
                   end do
                 
                 end do

             else

                 do is=1,maxlev

                   cie(is) = dcie(it,is)
                   do ip = 1,npl
                     ciepr(is,ip) = dciepr(it,is,ip)
                     v3pr(is,ip)  =  dv3pr(it,is,ip)
                     vecr(it,is,ip) = dvecr(it,is,ip)
                   end do
   
                 end do

             endif


C-----------------------------------------------------------------------
C set up whole rate matrix  - (units: sec-1)
C
C   to add on indirect parts also pass over pcc
C-----------------------------------------------------------------------

             call b8mcca( ndlev     , il         ,
     &                    lpsel     , liosel     , lpdata,
     &                    densa(in) , denspa(in) ,
     &                    cra       , pcc        ,
     &                    crce      , crcp       , cie    ,
     &                    cc
     &                  )

C-----------------------------------------------------------------------
C set up non-metastable (ordinary excited level) matrix - (units: sec-1)
C-----------------------------------------------------------------------

              call bxmcma( ndlev ,
     &                     nord  , iordr ,
     &                     cc    ,
     &                     cmat
     &                   )

C-----------------------------------------------------------------------
C invert non-metastable (ordinary excited level) matrix
C-----------------------------------------------------------------------

              lsolve=.FALSE.
              call xxminv( lsolve , ndlev , nord   ,
     &                     cmat   , rhs   , dmint
     &                   )

C-----------------------------------------------------------------------
C stack up non-metastable population components associated with each
C metastable level.
C-----------------------------------------------------------------------

              call bxstka( ndlev            , ndmet  ,
     &                     nord             , nmet   ,
     &                     iordr            , imetr  ,
     &                     cmat             , cc     ,
     &                     stack(1,1,it,in)
     &                   )

C-----------------------------------------------------------------------
C stack up non-metastable state sel. recombs. from (z+1)    -if selected
C-----------------------------------------------------------------------

              if (liosel) then
                  
                  npl3 = max0(npl,nplr)

                  do ip=1,npl3

                     call b8stke( ndtem         , ndlev  , ndmet ,
     &                            it            , nord   ,
     &                                            iordr  ,
     &                            densa(in)     ,
     &                            cmat          , vecr   , v3pr  ,
     &                            ip            ,
     &                            stvr(1,it,in,ip)
     &                          )
                  end do

              elseif (lrsel.and.(.not.liosel)) then

                  do ip=1,nplr

                     call b8stkb( ndtem         , ndlev  , ndmet ,
     &                            it            , nord   ,
     &                                            iordr  ,
     &                            cmat          , vecr   ,
     &                            ip            ,
     &                            stvr(1,it,in,ip)
     &                          )
                  end do
              
              endif

C-----------------------------------------------------------------------
C stack up non-metastable state sel. c. exch. from (z+1)    -if selected
C-----------------------------------------------------------------------

              do ip=1,nplr

                   if (lhsel) then
                      call b8stkb( ndtem         , ndlev  , ndmet ,
     &                             it            , nord   ,
     &                                             iordr  ,
     &                             cmat          , vech   ,
     &                             ip            ,
     &                             stvh(1,it,in,ip)
     &                           )
                   endif
                   
              end do

C-----------------------------------------------------------------------
C stack up non-metastable state sel. ionis.   from (z-1)    -if selected
C-----------------------------------------------------------------------

              do ip=1,npli

                   if (lisel) then
                      call b8stkb( ndtem         , ndlev  , ndmet ,
     &                             it            , nord   ,
     &                                             iordr  ,
     &                             cmat          , veci   ,
     &                             ip            ,
     &                             stvi(1,it,in,ip)
     &                           )
                   endif
              end do

C-----------------------------------------------------------------------
C stack up metastable level transition rate contributions
C-----------------------------------------------------------------------

              call bxstkc( ndlev  , ndmet            ,
     &                     nord   , nmet             ,
     &                     iordr  , imetr            ,
     &                     cc     , stack(1,1,it,in) ,
     &                     cred
     &                   )
              
              do imet=1,nmet
                do jmet=1,nmet
                  fvcred(imet,jmet,it,in)=cred(imet,jmet)
                end do
              end do

C-----------------------------------------------------------------------
C stack up metastable state sel. recomb.  from (z+1)    -if selected
C-----------------------------------------------------------------------

              if (liosel) then
                  
                  npl3 = max0(npl,nplr)

                  do ip=1,npl3

                      call b8stkf( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvr(1,it,in,ip),
     &                             densa(in),
     &                             vecr  , v3pr          , ip     ,
     &                             vrred
     &                           )
                      do imet=1,nmet
                        fvrred(imet,ip,it,in)=vrred(imet,ip)
                      end do

                  end do

              elseif (lrsel.and.(.not.liosel)) then

                  do ip=1,nplr

                      call b8stkd( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvr(1,it,in,ip),
     &                             vecr  , ip    ,
     &                             vrred
     &                           )
                      do imet=1,nmet
                        fvrred(imet,ip,it,in)=vrred(imet,ip)
                      end do

                  end do
              endif

C-----------------------------------------------------------------------
C stack up metastable state sel. c. exch. from (z+1)    -if selected
C-----------------------------------------------------------------------

              do ip=1,nplr
                 
                 if (lhsel) then
                    
                    call b8stkd( ndtem , ndlev         , ndmet  ,
     &                           it    , nord          , nmet   ,
     &                                   iordr         , imetr  ,
     &                           cc    , stvh(1,it,in,ip) ,
     &                           vech  , ip   ,
     &                           vhred
     &                         )
     
                    do imet=1,nmet
                      fvhred(imet,ip,it,in)=vhred(imet,ip)
                    end do

                 endif
              
              end do

C-----------------------------------------------------------------------
C stack up metastable state sel. ionis.   from (z-1)    -if selected
C-----------------------------------------------------------------------

              do ip = 1,npli

                   if (lisel) then
                      
                      call b8stkd( ndtem , ndlev         , ndmet  ,
     &                             it    , nord          , nmet   ,
     &                                     iordr         , imetr  ,
     &                             cc    , stvi(1,it,in,ip) ,
     &                             veci  , ip   ,
     &                             vired
     &                           )
                      
                      do imet=1,nmet
                        fvired(imet,ip,it,in)=vired(imet,ip)
                      end do

                   endif
                   
              end do

C-----------------------------------------------------------------------
C stack up metastable state sel. ionis. from (z) - if selected by liosel
C
C stack up parent cross coupling coefficients
C
C-----------------------------------------------------------------------

              if (liosel) then

                 do imet = 1,nmet
                   do ip=1,npl3

                     vionr(imet,ip)=ciepr(imetr(imet),ip)
                     do i=1,nord
                        vionr(imet,ip)=vionr(imet,ip) +
     &                                 ciepr(iordr(i),ip)
     &                                 *stack(i,imet,it,in)
                     end do
                     fvionr(imet,ip,it,in)=vionr(imet,ip)

                   end do
                 end do

                 do ip = 1,npl3
                   do imet=1,npl3

                     vcrpr(ip,imet)=0.0d+0
                     
                     if( ip .ne. imet ) then
                        do i=1,nord
                           vcrpr(ip,imet)=vcrpr(ip,imet) +
     &                                    ciepr(iordr(i),ip)
     &                                    *stvr(i,it,in,imet)
                        end do
                     endif
                     fvcrpr(ip,imet,it,in)=vcrpr(ip,imet)
                     if ( lpdata ) then
                        fvcrpr(ip,imet,it,in) = fvcrpr(ip,imet,it,in)
     &                                        + pvcrpr(ip,imet)
                     endif

                   end do
                 end do

              endif

C-----------------------------------------------------------------------
C calculate metastable populations
C-----------------------------------------------------------------------

              call bxmpop( ndmet             ,
     &                     nmet              ,
     &                     cred              ,
     &                     rhs               , crmat        ,
     &                     stckm(1,it,in)
     &                   )

C-----------------------------------------------------------------------
C  calculate recombinations and charge exchange contributions.
C-----------------------------------------------------------------------

               if (liosel) then

                   do ip=1,npl3
 
                      call b8stvm( ndmet            ,
     &                             nmet             ,
     &                             crmat            ,
     &                             ip               ,
     &                             vrred            ,
     &                             stvrm(1,it,in,ip)
     &                           )
                   end do

      
               elseif ((.not.liosel).and.lrsel) then

                   do ip=1,nplr
 
                      call b8stvm( ndmet            ,
     &                             nmet             ,
     &                             crmat            ,
     &                             ip               ,
     &                             vrred            ,
     &                             stvrm(1,it,in,ip)
     &                           )
                   end do

               endif
C-----------------------------------------------------------------------
               do ip=1,nplr

                 if (lhsel) then
                    call b8stvm( ndmet            ,
     &                           nmet             ,
     &                           crmat            ,
     &                           ip               ,
     &                           vhred            ,
     &                           stvhm(1,it,in,ip)
     &                         )
                 endif
                 
               end do
C-----------------------------------------------------------------------
               do ip=1,npli

                 if (lisel) then
                    call b8stvm( ndmet            ,
     &                           nmet             ,
     &                           crmat            ,
     &                           ip               ,
     &                           vired            ,
     &                           stvim(1,it,in,ip)
     &                         )
                 endif
               
               end do
C-----------------------------------------------------------------------

C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    6       continue
C*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    4 continue
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C-----------------------------------------------------------------------
      return
C-----------------------------------------------------------------------

      end
