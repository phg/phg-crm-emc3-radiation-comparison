CX UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxcstr.for,v 1.1 2004/07/06 11:43:42 whitefor Exp $ Date $Date: 2004/07/06 11:43:42 $
CX
       SUBROUTINE BXCSTR( CSTRGA , IL     , IMAX  ,
     &                    CSTRGB
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXCSTR *********************
C
C  PURPOSE:  TO TAKE THE LAST 'IMAX' NON-BLANK BYTES OF THE 'CSTRGA'
C            STRING AND PLACE THEM IN THE 'CSTRGB' STRING.
C
C  CALLING PROGRAM: ADAS205/ADAS206
C
C
C  SUBROUTINE:
C
C  INPUT : (C*(*))CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  IMAX    = NUMBER OF NON-BLANK BYTES TO BE EXTRACTED
C                           FROM THE END OF THE INPUT STRING.
C
C  OUTPUT: (C*(*))CSTRGB()= LAST 'IMAX' NON-BLANK BYTES FROM 'CSTRGA()'
C
C          (I*4)  IFIRST  = POSITION OF FIRST NON-BLANK BYTE IN CSTRGA()
C          (I*4)  ILAST   = POSITION OF LAST  NON-BLANK BYTE IN CSTRGA()
C          (I*4)  ILEN    = LENGTH IN BYTES OF NON-BLANK PART OF CSTRGA
C          (I*4)  I       = GENERAL USE (ARRAY INDEX)
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           XXSLEN      ADAS        FIRST/LAST NONBLANK BYTES IN STRING
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    06/07/93
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IL             , IMAX
      INTEGER   IFIRST         , ILAST          , ILEN       , I
C-----------------------------------------------------------------------
      CHARACTER CSTRGA(IL)*(*) , CSTRGB(IL)*(*)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      DO 1 I=1,IL
C
          CALL XXSLEN( CSTRGA(I) , IFIRST , ILAST )
C
          ILEN = ILAST - IFIRST + 1
C
          IF (ILEN.GT.IMAX) IFIRST = ILAST - IMAX + 1
C
          CSTRGB(I) = CSTRGA(I)(IFIRST:ILAST)
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
