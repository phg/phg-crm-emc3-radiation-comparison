CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxmpop.for,v 1.1 2004/07/06 11:44:40 whitefor Exp $ Date $Date: 2004/07/06 11:44:40 $
CX
      SUBROUTINE BXMPOP( NDMET ,
     &                   NMET  ,
     &                   CRED  ,
     &                   RHS   , CRMAT ,
     &                   STKM
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXMPOP *********************
C
C  PURPOSE: TO CALCULATE AND STACK UP IN  'STKM'  THE  METASTABLE LEVEL
C           POPULATIONS FOR A GIVEN TEMPERATURE AND DENSITY.
C
C           ALSO OUTPUTS INVERTED METASTABLE RATE MATRIX.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (R*8)  CRED(,) = MATRIX  OF  TRANSITION   RATES   BETWEEN
C                            METASTABLE LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C
C  OUTPUT:  (R*8)  RHS()   = GENERAL MATRIX SOLUTION WORK SPACE:
C                            USED IN SOLUTION OF 'NMET-1' LINEAR EQNS.
C                                           A.X=B
C                            INPUT TO XXMINV: RIGHT HAND SIDE VECTOR 'B'
C                             (RHS(IM) = -(RATE FROM LEVEL 'IM+1' TO 1))
C                             (UNITS: SEC-1)
C                            OUTPUT FROM XXMINV: SOLUTION VECTOR 'X'
C                             (RHS(IM) = POPULATION OF LEVEL 'IM+1')
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL - 1
C  OUTPUT:  (R*8)  CRMAT(,)= INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            BEFORE INPUT  TO   XXMINV: NOT INVERTED
C                            AFTER  OUTPUT FROM XXMINV: AS-ABOVE
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C
C  OUTPUT:  (R*8)  STKM()  = METASTABLE LEVEL POPULATION MATRIX.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C           (L*4)  LSOLVE  = PARAMETER = .TRUE.
C                                   => USE 'XXMINV' TO SOLVE A SET  OF
C                                      LINEAR EQUATIONS A.X = B, WHERE
C                                      A,X,B ARE MATRICES/VECTORS AND:
C                                       A='CRMAT(,)' INPUT  TO   XXMINV
C                                       B='RHS()'    INPUT  TO   XXMINV
C                                       X='RHS()'    OUTPUT FROM XXMINV
C
C           (I*4)  NMET1   = 'NMET - 1'
C           (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IM1     = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IM2     = METASTABLE LEVEL ARRAY INDEX
C
C           (R*8)  DMINT   = +1 or -1 DEPENDING ON WHETHER THE NUMBER OF
C                            ROW   INTERCHANGES   WAS   EVEN   OR   ODD,
C                            RESPECTIVELY, WHEN INVERTING A MATRIX USING
C                            'XXMINV'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXMINV     ADAS      INVERTS MATRIX AND SOLVES EQUATIONS.
C
C NOTE:
C        THE SOLUTION OF METASTABLE POPULATIONS GIVEN BELOW IS BASED ON
C        METASTABLE LEVEL 1 HAVING A POPULATION OF UNITY (1.0).
C
C        IF:     m  =  number of metastable levels - 1
C
C            R(mxm) = Rate matrix (sec-1) covering transistions between
C                     all possible pairs of metastable levels (except 1)
C                     row   : final   level
C                     column: initial level
C                     (R(mxm)   = 'CRMAT(,)' on input  to   XXMINV)
C                     (R-1(mxm) = 'CRMAT(,)' on output from XXMINV)
C            V(m)   = Rate vector (sec-1) covering transistions between
C                     each metastable level (except 1) and met. level 1
C                     ( = 'RHS()' on input  to   XXMINV)
C            P(m)   = Metastable level populations - levels 2 -> 'NMET'
C                     ( = 'RHS()' on output from XXMINV)
C
C           Therefore:  R(mxm).P(m) = V(m)
C
C            =>         P(m) = R-1(mxm).V(m)
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LOGICAL    LSOLVE
C-----------------------------------------------------------------------
      PARAMETER( LSOLVE = .TRUE.        )
C-----------------------------------------------------------------------
      INTEGER    NDMET                  , NMET
      INTEGER    NMET1                  , IM          ,
     &           IM1                    , IM2
C-----------------------------------------------------------------------
      REAL*8     DMINT
C-----------------------------------------------------------------------
      REAL*8     CRED(NDMET,NDMET)      ,
     &           RHS(NDMET)             ,
     &           CRMAT(NDMET,NDMET)     ,
     &           STKM(NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         IF (NMET.EQ.1) THEN
            STKM(1)=1.0D0
         ELSE
C-----------------------------------------------------------------------
C  DISPLACE ARRAY DOWN FOR INVERSION
C-----------------------------------------------------------------------
               DO 1 IM1=2,NMET
                  IM = IM1-1
                     DO 2 IM2=2,NMET
                        CRMAT(IM2-1,IM) = CRED(IM2,IM1)
    2                CONTINUE
                  RHS(IM) = -CRED(IM1,1)
    1          CONTINUE
C-----------------------------------------------------------------------
C  INVERT MATRIX AND SOLVE EQUATIONS.
C-----------------------------------------------------------------------
            NMET1  = NMET  - 1
            CALL XXMINV( LSOLVE  , NDMET  , NMET1  ,
     &                   CRMAT   , RHS    , DMINT
     &                 )
C-----------------------------------------------------------------------
C  STACK METASTABLE POPULATIONS
C-----------------------------------------------------------------------
            STKM(1) = 1.0D0
               DO 3 IM=2,NMET
                  STKM(IM) = RHS(IM-1)
    3          CONTINUE
         ENDIF
C***********************************************************************
      RETURN
      END
