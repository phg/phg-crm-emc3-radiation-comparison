CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxwr11.for,v 1.1 2004/07/06 11:47:05 whitefor Exp $ Date $Date: 2004/07/06 11:47:05 $
CX
      SUBROUTINE BXWR11( IUNIT  , DSNINC , TITLED ,
     &                   NDLEV  , NDTEM  , NDDEN  , NDMET ,
     &                   IZ     , IZ0    , IZ1    , BWNO  ,
     &                   IL     , NMET   , NORD   ,
     &                   MAXT   , MAXD   , ICNTR  , ICNTH ,
     &                   IA     , ISA    , ILA    , XJA   ,
     &                   CSTRGA ,
     &                   IMETR  , IORDR  , TEA    , DENSA ,
     &                   STCKM  , STVR   , STVH   ,
     &                   STVRM  , STVHM  , STACK
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: BXWR11 *******************
C
C  PURPOSE:  TO OUTPUT DATA TO CONTOUR PASSING FILE.
C            POPULATION  DATA  FOR DIAGNOSTIC USE.
C
C  CALLING PROGRAM: ADAS205/ADAS206
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT NUMBER FOR RESULTS
CX INPUT : (C*44) DSNINC  = INPUT COPASE DATA SET NAME (IN QUOTES).
CA INPUT : (C*80) DSNINC  = INPUT COPASE DATA SET NAME (IN QUOTES).
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT : (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES LEVELS: 1<=NMET<=NDMET
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C
C  INPUT : (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 -> 'NDTEM')
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 -> 'NDDEN')
C  INPUT : (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT : (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT : (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  INPUT : (I*4)  IORDR() = INDEX OF ORDINARY LEVELS IN COMPLETE LEVEL
C                           LIST.
C  INPUT : (R*8)  TEA()   = ELECTRON TEMPERATURES (UNITS: KELVIN)
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C
C  INPUT : (R*8) STCKM(,,) = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVR(,,)  = FREE ELECTRON RECOMBINATION COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVH(,,)  =  CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVRM(,,) = METASTABLE FREE ELECTRON RECOMBINATION
C                            COEFFICIENTS.
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*8) STVHM(,,) = METASTABLE CHARGE EXCHANGE COEFFICIENTS
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C                             3rd DIMENSION: DENSITY INDEX
C  INPUT : (R*4) STACK(,,,)= POPULATION DEPENDENCE
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C
C          (I*4) I         = GENERAL USE
C          (I*4) J         = GENERAL USE
C          (I*4) K         = GENERAL USE
C          (I*4) L         = GENERAL USE
C
C NOTE:
C          THIS OUTPUT DATA IS FOR SUBSEQUENT INPUT INTO THE DIAGNOSTIC
C          AND CONTOUR GRAPHING PROGRAM 'CONTOUR'.
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C
C UPDATE:  20/05/93 - PE BRIDEN - ADAS91: TO REFLECT CHANGES IN BXDATA
C                                         THE FOLLOWING ARRAY DIMENSION/
C                                         SIZE CHANGES WERE MADE:
C                                       1) CHARACTER CSTRGA *12 -> *18
C                                          (FORMAT STMT 1003 CHANGED)
C
C UPDATE:  20/05/93- P BRIDEN: STACK ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C-----------------------------------------------------------------------
      INTEGER   NDLEV     , NDTEM      , NDDEN   , NDMET
      INTEGER   IUNIT     ,
     &          IZ        , IZ0        , IZ1     ,
     &          IL        , NMET       , NORD    ,
     &          MAXT      , MAXD       , ICNTR   , ICNTH
      INTEGER   I         , J          , K       , L
C-----------------------------------------------------------------------
      REAL*8    BWNO
C-----------------------------------------------------------------------
CX    CHARACTER TITLED*3         , DSNINC*44
      CHARACTER TITLED*3         , DSNINC*80
      CHARACTER CSTRGA(NDLEV)*18
C-----------------------------------------------------------------------
      INTEGER   IA(NDLEV)        , ISA(NDLEV)       , ILA(NDLEV)
      INTEGER   IMETR(NDMET)     , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8   XJA(NDLEV)        , TEA(NDTEM)       , DENSA(NDDEN)
      REAL*8   STCKM(NDMET,NDTEM,NDDEN)
      REAL*8   STVR(NDLEV,NDTEM,NDDEN)        , STVH(NDLEV,NDTEM,NDDEN)
      REAL*8   STVRM(NDMET,NDTEM,NDDEN)       , STVHM(NDMET,NDTEM,NDDEN)
      REAL*4   STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      WRITE(IUNIT,1000) DSNINC
      WRITE(IUNIT,1001) TITLED, IZ   , IZ0 , IZ1  , BWNO
      WRITE(IUNIT,1002) IL    , NMET ,NORD , MAXT , MAXD , ICNTR , ICNTH
      WRITE(IUNIT,1003) ( IA(I),CSTRGA(I),ISA(I),ILA(I),XJA(I), I=1,IL )
      WRITE(IUNIT,1002) ( IMETR(I) , I=1,NMET )
      WRITE(IUNIT,1002) ( IORDR(I) , I=1,NORD )
      WRITE(IUNIT,1004) ( TEA(I)   , I=1,MAXT )
      WRITE(IUNIT,1004) ( DENSA(I) , I=1,MAXD )
      WRITE(IUNIT,1005) ( ( ( ( STACK(I,J,K,L) , I=1,NORD ), J=1,NMET ),
     &                                           K=1,MAXT ), L=1,MAXD )
      WRITE(IUNIT,1005) ( ( (   STCKM(I,J,K)   , I=1,NMET ), J=1,MAXT ),
     &                                           K=1,MAXD )
C-----------------------------------------------------------------------
         IF (ICNTR.GT.0) THEN
            WRITE(IUNIT,1005) ( ( ( STVR(I,J,K), I=1,NORD ), J=1,MAXT ),
     &                                           K=1,MAXD )
            IF (NMET.GT.1) WRITE(IUNIT,1005)
     &                       ( ( ( STVRM(I,J,K), I=2,NMET ), J=1,MAXT ),
     &                                           K=1,MAXD )
         ENDIF
C-----------------------------------------------------------------------
         IF(ICNTH.GT.0) THEN
            WRITE(IUNIT,1005) ( ( ( STVH(I,J,K), I=1,NORD ), J=1,MAXT ),
     &                                           K=1,MAXD )
            IF (NMET.GT.1) WRITE(IUNIT,1005)
     &                       ( ( ( STVHM(I,J,K), I=2,NMET ), J=1,MAXT ),
     &                                           K=1,MAXD )
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A)
 1001 FORMAT(A3,I2,2I10,F15.0)
 1002 FORMAT(16I5)
 1003 FORMAT(I5,1X,A18,1X,I1,1X,I1,1X,F4.1)
 1004 FORMAT(1P,8E10.2)
 1005 FORMAT(1P,6E12.4)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
