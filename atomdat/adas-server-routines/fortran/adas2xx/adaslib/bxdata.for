CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxdata.for,v 1.9 2004/07/06 11:44:07 whitefor Exp $ Date $Date: 2004/07/06 11:44:07 $
CX
       SUBROUTINE BXDATA( IUNIT  , NDLEV  , NDTRN ,
     &                    TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
     &                    IL     ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA   , WA ,
     &                    NV     , SCEF   ,
     &                    ITRAN  , MAXLEV ,
     &                    TCODE  , I1A    , I2A   , AVAL  , SCOM
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXDATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT COPASE DATA SET.
C
C  CALLING PROGRAM: ADAS205/ADAS206/ADAS201
C
C  DATA:
C           THE 'REAL' DATA IN THE FILE IS REPRESENTED IN AN ABBREVIATED
C           FORM WHICH OMITS THE "D" OR "E" EXPONENT SPECIFIER.
C           e.g. 1.23D-06 or 1.23E-06 IS REPRESENTED AS 1.23-06
C                6.75D+07 or 6.75E+07 IS REPRESENTED AS 6.75+07
C
C           THEREFORE THE FORM OF EACH 'REAL' NUMBER IN THE DATA SET IS:
C                          N.NN+NN or N.NN-NN
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C           TEMPERATURES        : KELVIN
C           A-VALUES            : SEC-1
C           GAMMA-VALUES        :
C           RATE COEFFT.        : CM3 SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C
C  OUTPUT: (C*3)  TITLED  = ELEMENT SYMBOL.
C  OUTPUT: (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  OUTPUT: (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  OUTPUT: (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  OUTPUT: (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  OUTPUT: (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  OUTPUT: (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  OUTPUT: (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C                           (INITIALLY JUST THE MANTISSA. SEE 'ITPOW()')
C                           (NOTE: TE=TP=TH IS ASSUMED)
C
C  OUTPUT: (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C  OUTPUT: (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C
C  OUTPUT: (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C  OUTPUT: (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            SIGNED PARENT      INDEX (CASE 'H' & 'R')
C  OUTPUT: (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C  OUTPUT: (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C  OUTPUT: (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C
C          (I*4)  NVMAX   = PARAMETER = MAX. NUMBER OF TEMPERATURES
C                                       THAT CAN BE READ IN.
C          (I*4)  MTIED   = PARAMETER = MUST BE GREATER THAN OR EQUAL TO
C                                       THE MAX. NO. OF LEVELS.
C          (R*8)  DZERO   = PARAMETER = MINIMUM VALUE FOR 'AVAL()' AND
C                                       'SCOM()' ARRAYS = 1.0D-30
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IQS     = X-SECT DATA FORMAT SELECTOR
C                           NOTE: IQS=3 ONLY ALLOWED IN THIS PROGRAM
C          (I*4)  I       = GENERAL USE.
C          (I*4)  IABT    = RETURN CODE FROM 'R8FCTN' (0 => NO ERROR)
C                           OR FROM INTERROGATION OF 'C7'
C          (I*4)  IFIRST  = BYTE POSITION OF START OF NUMBER IN BUFFER
C          (I*4)  ILAST   = BYTE POSITION OF END   OF NUMBER IN BUFFER
C          (I*4)  IWORD   = THE WORD POSITION OF THE REQUIRED DATA IN
C                           A STRING TO BE INTERROGATED BY XXWORD.
C          (I*4)  J       = GENERAL USE.
C          (I*4)  J1      = INPUT DATA FILE - SELECTED TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C          (I*4)  J2      = INPUT DATA FILE - SELECTED TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  LENCST  = BYTE LENGTH OF STRING CSTRGA()
C          (I*4)  NWORDS  = NUMBER OF NUMBERS STORED IN BUFFER
C          (I*4)  ILINE   = ENERGY LEVEL INDEX FOR CURRENT LINE
C          (I*4)  IAPOW   = EXPONENT OF 'AVALM'
C          (I*4)  IGPOW() = EXPONENT OF 'GAMMA()'
C          (I*4)  ITPOW() = TEMPERATURES - EXPONENT
C                           NOTE: MANTISSA INITIALLY KEPT IN 'SCEF()'
C
C          (R*4)  ZF      = SHOULD BE EQUIVALENT TO 'IZ1'
C
C          (R*8)  AVALM   = INPUT DATA FILE - SELECTED TRANSITION:
C                           MANTISSA OF:   ('IAPOW' => EXPONENT)
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  GAMMA() = INPUT DATA FILE - SELECTED TRANSITION:
C                           MANTISSA OF: ('IGPOW()' => EXPONENT)
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           DIMENSION => TEMPERATURE 'SCEF()'
C
C          (C*7)  C7      = USED TO PARSE VALUE FOR XJA()
C          (C*7)  CDELIM  = DELIMITERS FOR INPUT OF DATA FROM HEADERS
C          (C*18) C18     = USED TO PARSE VALUE TO CSTRGA()
C          (C*80) CLINE   = CURRENT ENERGY LEVEL INDEX PARAMETER LINE
C          (C*128)BUFFER  = GENERAL STRING BUFFER STORAGE
C          (C*3)  CITPOW()= USED TO PARSE VALUES TO ITPOW()
C          (C*5)  CSCEF() = USED TO PARSE VALUES TO SCEF()
C          (C*7)  CFORM7  = FORMAT FOR INTERNAL READING OF REAL NUMBER
C
C          (L*4)  LDATA   = IDENTIFIES  WHETHER  THE END OF AN  INPUT
C                           SECTION IN THE DATA SET HAS BEEN LOCATED.
C                           (.TRUE. => END OF SECTION REACHED)
C          (L*4)  LTCHR   = .TRUE.  => CURRENT 'TCODE()' = 'H' OR 'R'.
C                         = .FALSE. => CURRENT 'TCODE()'.NE.'H' OR 'R'.
C          (L*4)  LTCPR   = .TRUE.  => CURRENT 'TCODE()' = 'P' OR 'R'.
C                         = .FALSE. => CURRENT 'TCODE()'.NE.'P' OR 'R'.
C          (L*4)  LERROR  = .TRUE.  => UNTIED LEVEL FOUND
C                         = .FALSE. => ALL LEVELS TIED
C          (L*4)  LTIED() = .TRUE.  => SPECIFIED LEVEL TIED
C                         = .FALSE. => SPECIFIED LEVEL IS UNTIED
C                           DIMENSION => LEVEL INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C
C NOTE:            LTCHR        LTCPR         TCODE()
C                 -----------------------------------
C                 .TRUE.       .TRUE.    =>     'R'
C                 .TRUE.       .FALSE.   =>     'H'
C                 .FALSE.      .TRUE.    =>     'P'
C                 .FALSE.      .FALSE.   =>     ' '
C
C        FOR A-VALUES & GAMMA-VALUES ENTRIES LESS THAN 'DZERO' ARE TAKEN
C        AS BEING EQUAL TO DZERO. THIS AFFECTS THE 'AVAL()' AND 'SCOM()'
C        ARRAYS.
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  16/11/90 - LEVEL LINE READ AS A CHARACTER*80 STRING FIRST
C                     (PE BRIDEN)
C
C UPDATE:  01/05/92 - CHECK MADE TO MAKE SURE NO UNTIED LEVEL EXISTS.
C                     IF UNTIED LEVELS EXIST PROGRAM IS TERMINATED
C                     WITH A MESSAGE.
C                     (PE BRIDEN)
C
C UPDATE:  26/06/92 - INCREASED PARAMETER MTIED FROM 100 TO 200
C
C UPDATE:  30/07/92 - INPUT VARIABLE 'XJA' NOW ALLOWED TO HAVE A LENGTH
C                     OF BETWEEN 1 AND 6 STARTING AT COLUMN 30 - IT MUST
C                     BE FOLLOWED BY A ')' WHICH CANNOT BE PLACED AFTER
C                     COLUMN 36. INTRODUCED VARIABLE 'C7' TO PARSE VALUE
C                     AND USE FUNCTION R8FCTN TO INTERROGATE C7.
C                     - EDITED FORMAT STATEMENT 1003 ACCORDINGLY.
C                     - INTRODUCED FORMAT STATEMENT 1012.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  20/05/93 - PE BRIDEN - ADAS91: MAJOR REVISION -
C                                         MODIFIED TO READ IN NEW INPUT
C                                         DATA-SET STYLE AND ALSO ALLOW
C                                         THE OLD-STYLE TO BE READ.
C                                         ARGUMENT DIMENSIONS CHANGED
C                                         BUT CODE ROUTINE SHOULD SPOT
C                                         CASES WHERE THE ORIGINAL
C                                         ARGUMENT DIMS ARE USED AND
C                                         ACT ACCORDINGLY.
C
C UPDATE:  05/08/93 - PE BRIDEN - ADAS91: MINOR REVISION -
C                                         IF DATA TYPE POINTER (TCODE())
C                                         EQUALS 'H' (Charge Exchange
C                                         Recomb.) or 'R' (Free Electron
C                                         Recomb.) - I1A()  now stores
C                                         the signed parent index(see
C                                         I1A() above)
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 04-05-93
C MODIFIED: ANDREW BOWEN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 04-05-93
C MODIFIED: ANDREW BOWEN 
C               - ERROR WRIES CHANGED TO UNIT 0.
C
C VERSION: 1.3                          DATE: 21-03-95
C MODIFIED: LALIT JALOTA (TESSELLA SUPPORT SERVICES PLC)
C               - 
C
C VERSION: 1.4                          DATE: 23-03-95
C MODIFIED: LALIT JALOTA 
C               -
C
C VERSION: 1.5                          DATE: 02-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C               - INSTEAD OF USING FORMAT SPECIFIER F15.0 WHEN
C                 INTERNALLY READING A FLOATING POINT NUMBER,
C                 CREATE THE APPROPRIATE SPECIFIER WITHIN CFORM7 
C                 AND USE THIS.
C
C VERSION: 1.6				DATE: 24-06-97
C MODIFIED: HUGH SUMMERS
C               - CHANGED PARAMETER MTIED FROM 200 TO 300
C
C VERSION: 1.7				DATE: 26-02-97
C MODIFIED: M.O'MULLANE AND R. MARTIN	
C		    - CHANGED 'I2' TO 'I4' TO IN FORMAT STATEMENT 1011
C
C VERSION: 1.8				DATE: 20-09-99
C MODIFIED: R. MARTIN	
C		    - CHANGED 'I3' TO 'I4' TO IN FORMAT STATEMENT 1001
C
C VERSION: 1.9				DATE: 28-05-2003
C MODIFIED: Martin O'Mullane
C		    - Warn user that the routine is now deprecated
C                     and that xxdata_04 should be used instead.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   NVMAX       , MTIED
C-----------------------------------------------------------------------
      REAL*8    DZERO
C-----------------------------------------------------------------------
      PARAMETER( NVMAX = 14 , MTIED = 1000    , DZERO = 1.0D-30 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   IUNIT       , NDLEV          , NDTRN      ,
     &          IZ          , IZ0            , IZ1        ,
     &          IL          , NV             , ITRAN      ,
     &          MAXLEV
      INTEGER   ILINE
      INTEGER   IQS         , I              , IABT       ,
     &          IFIRST(1)   , ILAST(1)       , IWORD      ,
     &          J           , J1             , J2         ,
     &          LENCST      , NWORDS         , IAPOW      ,
     &          IGPOW(NVMAX), ITPOW(NVMAX)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV) ,
     &          I1A(NDTRN)  , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*4    ZF
C-----------------------------------------------------------------------
      REAL*8    R8FCTN
      REAL*8    BWNO        , AVALM
      REAL*8    SCEF(NVMAX) , GAMMA(NVMAX)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)                   ,
     &          AVAL(NDTRN) , SCOM(NVMAX,NDTRN)
C-----------------------------------------------------------------------
      CHARACTER TITLED*3    , TCODE(NDTRN)*1 , CSTRGA(NDLEV)*(*)
      CHARACTER CFORM7*7
      CHARACTER C7*7        , CDELIM*7       , C18*18     ,
     &          CLINE*80    , BUFFER*128
      CHARACTER CITPOW(NVMAX)*3              , CSCEF(NVMAX)*5
C-----------------------------------------------------------------------
      LOGICAL   LDATA       , LTCHR          , LTCPR      , LERROR
C-----------------------------------------------------------------------
      LOGICAL   LTIED(MTIED)
C-----------------------------------------------------------------------
      DATA      CFORM7 / '(F??.0)' /
      DATA      CDELIM / ' ()<>{}' /
C-----------------------------------------------------------------------
      SAVE      CDELIM      , CFORM7
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Warn user that this routine is deprecated but continue.
C-----------------------------------------------------------------------

      WRITE(I4UNIT(-1),3000)

C **********************************************************************
C
      IF (MTIED.LT.NDLEV)
     &             STOP ' BXDATA ERROR: MTIED < NDLEV - INCREASE MTIED'
C
C********************* PEB 20/05/93 - MODIFICATION *********************
C
      LENCST = MIN0( LEN(CSTRGA(1)) , 18 )
C
C THE NEXT IF BLOCK IS ONLY USED TO MAKE SURE THAT ANYONE WHO USES  THIS
C SUBROUTINE INDEPENDENTLY IS MADE AWARE OF CHANGES MADE TO ITS ARGUMENT
C LIST ON 20/05/93.
C
C IDENTIFY IF BXDATA IS BEING CALLED USING THE OLD STYLE ARGUMENTS
C IF SO QUIT PROGRAM MESSAGE.  (LENCST=12 => old style).
C
         IF (LENCST.EQ.12) THEN
            WRITE(I4UNIT(-1),1013)
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C IDENTIFY THE RECORD LENGTH OF THE INPUT DATASET (OLD-STYLE = 80 ? )
C (MAXIMUM CURRENTLY ACTIVE USAGE IS 128 BYTES)
C
C
C****************** PEB 20/05/93 - END OF MODIFICATION *****************
C
C***********************************************************************
C INPUT ION SPECIFICATIONS.
C***********************************************************************
C
      READ(IUNIT,1000) TITLED, IZ, IZ0, IZ1, BUFFER(1:55)
C
      J     = 1
      IWORD = 1
      CALL XXWORD( BUFFER(1:55) , CDELIM   , IWORD  ,
     &             J            ,
     &             IFIRST(1)    , ILAST(1) , NWORDS )
C
C********************* PEB 20/07/95 - MODIFICATION *********************
C
      J = 1 + ILAST(1) - IFIRST(1)
      J = MIN0(J,15)
      WRITE(CFORM7(3:4),'(I2.2)') J 
      READ(BUFFER(IFIRST(1):ILAST(1)),CFORM7) BWNO
C
C****************** PEB 20/07/95 - END OF MODIFICATION *****************
C
C
         IF(BWNO.LE.0.0D0) THEN
            WRITE(I4UNIT(-1),1001) 'IONISATION POTENTIAL .LE. 0'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C***********************************************************************
C READ IN ENERGY LEVEL SPECIFICATIONS
C***********************************************************************
C
      LDATA=.TRUE.
C
         DO 1 I=1,NDLEV
C
            LTIED(I) = .FALSE.
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I5)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE=-1.
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
C********************* PEB 30/07/92 - MODIFICATION *********************
C********************* PEB 20/05/93 - MODIFICATION *********************
C
                     READ (CLINE,1003) IA(I)  , C18          ,
     &                                 ISA(I) , ILA(I)       ,
     &                                 C7     , BUFFER(1:44)
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:44) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
C
C********************* PEB 20/07/95 - MODIFICATION *********************
C
                     J = 1 + ILAST(1) - IFIRST(1)
                     J = MIN0(J,15)
                     WRITE(CFORM7(3:4),'(I2.2)') J 
                     READ(BUFFER(IFIRST(1):ILAST(1)),CFORM7) WA(I)
C
C****************** PEB 20/07/95 - END OF MODIFICATION *****************
C
C
C IDENTIFY IF BXDATA IS BEING CALLED USING THE OLD CALLING ARGUMENTS
C REMOVE LEADING BLANKS FROM OLD STYLE INPUT DATASETS.
C
                     IF (C18(1:2).EQ.'  ') C18=C18(3:18)
C
                     CSTRGA(I)=C18(1:LENCST)
C
C****************** PEB 20/05/93 - END OF MODIFICATION *****************
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'COPASE DATA SET LEVEL ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C****************** PEB 30/07/92 - END OF MODIFICATION *****************
C
                  ENDIF
            ENDIF
    1    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'COPASE DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF
C
C***********************************************************************
C READ IN TEMPERATURES (KELVIN) AND DATA FORMAT SELECTOR
C***********************************************************************
C
      BUFFER = ' '
      READ(IUNIT,'(A)') BUFFER
C
      READ(BUFFER,1005) ZF, IQS, (CSCEF(I),CITPOW(I),I=1,NVMAX)
C
C CHECK DATA FORMAT SELECTOR IS VALID
C
         IF(IQS.NE.3) THEN
            WRITE(I4UNIT(-1),1001) '(IQS.NE.3) -'
            WRITE(I4UNIT(-1),1006)
     &      'FILE CONTAINS INVALID DATA FORMAT SELECTOR (MUST EQUAL 3)'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C CHECK 'ZF' EQUALS IZ1
C
         IF(NINT(ZF).NE.IZ1) THEN
            WRITE(I4UNIT(-1),1001) '(ZF.NE.IZ1) -'
            WRITE(I4UNIT(-1),1006)
     &      'FILE CONTAINS INCONSISTANT RECOMBINING ION CHARGE VALUES'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C IDENTIFY THE NUMBER OF TEMPERATURES VALUES INPUT
C
         NV = 0
C
         DO 2 J=1,NVMAX
            IF (CSCEF(J).EQ.' ') THEN
               SCEF(J)  = 0.0D0
               ITPOW(J) = 0.0D0
            ELSE
               READ(CSCEF(J) ,'(F5.2)') SCEF(J)
               READ(CITPOW(J), '(I3)')  ITPOW(J)
               IF (SCEF(J).GT.0.0D0) NV = J
            ENDIF
    2    CONTINUE
C
C CHECK THAT AT LEAST ONE VALID TEMPERATURE EXISTS.
C
    3    IF (NV.LE.0) THEN
            WRITE(I4UNIT(-1),1001) '(NV.LE.0) -'
            WRITE(I4UNIT(-1),1006)
     &      'NO VALID TEMPERATURE VALUES FOUND (NONE > 0.0)'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C-----------------------------------------------------------------------
C COMBINE INPUT MANTISSA AND EXPONENT FOR TEMPERATURES
C-----------------------------------------------------------------------
C
         DO 4 I=1,NV
            SCEF(I)  = SCEF(I)  * ( 10.0**DBLE(ITPOW(I)) )
    4    CONTINUE
C
C-----------------------------------------------------------------------
C CHECK TEMPERATURES ARE IN A STRICTLY INCREASING MONOTONIC SEQUENCE
C (REQUIRED FOR THE LATER USE OF THE MINIMAX AND SPLINE NAG ROUTINES)
C-----------------------------------------------------------------------
C
         DO 5 I=2,NV
            IF (SCEF(I).LE.SCEF(I-1)) THEN
               WRITE(I4UNIT(-1),1007) '(SCEF(',I,').LE.SCEF(',I-1,')) -'
               WRITE(I4UNIT(-1),1006)
     &      'TEMPERATURES ARE NOT IN A STRICT MONOTONIC ASCENDING ORDER'
               WRITE(I4UNIT(-1),1002)
               STOP
             ENDIF
    5    CONTINUE
C
C***********************************************************************
C READ IN TRANSITION SPECIFICATIONS
C***********************************************************************
C
      MAXLEV=0
      LDATA=.TRUE.
C
         DO 6 I=1,NDTRN
            IF (LDATA) THEN
               READ(IUNIT,1008) TCODE(I) , J2 , J1 , AVALM , IAPOW,
     &                          (GAMMA(J),IGPOW(J),J=1,NV)
C
C TRANSITION INPUT INFORMATION IS TERMINATED BY J2=-1
C
                  IF (J2.LE.0) THEN
                     LDATA=.FALSE.
                     ITRAN=I-1
                  ELSE
C
C-----------------------------------------------------------------------
C IDENTIFY THAT SPECIFIED LEVELS ARE TIED.
C-----------------------------------------------------------------------
C
                     LTIED(J1) = .TRUE.
                     LTIED(J2) = .TRUE.
C
C-----------------------------------------------------------------------
C IDENTIFY 'TCODE()' TYPE.
C-----------------------------------------------------------------------
C
                     LTCHR = (TCODE(I).EQ.'H') .OR. (TCODE(I).EQ.'R')
                     LTCPR = (TCODE(I).EQ.'P') .OR. (TCODE(I).EQ.'R')
C
C-----------------------------------------------------------------------
C COMBINE INPUT MANTISSA AND EXPONENT FOR A-VALUE, GAMMAS
C-----------------------------------------------------------------------
C
                     IF (.NOT.LTCPR) THEN
                        AVAL(I)=AVALM*( 10.0**DBLE(IAPOW) )
                        AVAL(I)=DMAX1(AVAL(I),DZERO)
                     ENDIF
                        DO 7 J=1,NV
                           SCOM(J,I)=GAMMA(J)*( 10.0**DBLE(IGPOW(J)) )
                           SCOM(J,I)=DMAX1(SCOM(J,I),DZERO)
    7                   CONTINUE
C
C-----------------------------------------------------------------------
C FIND HIGHEST INDEX LEVEL FOR TRANSITIONS. (TCODE() = ' ' OR 'P' ONLY)
C-----------------------------------------------------------------------
C
                     IF (.NOT.LTCHR) MAXLEV=MAX0(J1,J2,MAXLEV)
C
C-----------------------------------------------------------------------
C CHECK IF ENERGY LEVELS ARE IN CORRECT ORDER. IF NOT REVERSE.
C (REVISION - 05/08/93 - Line I1A(I)=J1 added when LTCHR is true.)
C-----------------------------------------------------------------------
C
                        IF (LTCHR) THEN
                           I1A(I)=J1
                           I2A(I)=J2
                        ELSEIF (WA(J2).LT.WA(J1)) THEN
                           I1A(I)=J2
                           I2A(I)=J1
                           WRITE(I4UNIT(-1),1009)
     &                       ' BXDATA MESSAGE: UPPER AND ',
     &                       'LOWER ENERGY LEVELS HAVE BEEN REVERSED.',
     &                       'CORRECT LEVELS ARE - UPPER = ',J1,
     &                       '  LOWER = ',J2
                        ELSE
                           I1A(I)=J1
                           I2A(I)=J2
                        ENDIF
C-----------------------------------------------------------------------
                  ENDIF
            ENDIF
    6    CONTINUE
C
C-----------------------------------------------------------------------
C
         IF (LDATA) THEN
            READ (IUNIT,1010) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &               'COPASE DATA SET CONTAINS > ',NDTRN,' TRANSITIONS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  ITRAN = NDTRN
               ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C CHECK THAT THERE ARE NO UNTIED LEVELS
C-----------------------------------------------------------------------
C
      LERROR = .FALSE.
C
         DO 8 I=1,MAXLEV
            IF (.NOT.LTIED(I)) THEN
               LERROR = .TRUE.
               WRITE(I4UNIT(-1),1011) I
            ENDIF
    8    CONTINUE
C
         IF (LERROR) THEN
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C***********************************************************************
C
 1000 FORMAT(1A3,I2,2I10,A55)
 1001 FORMAT(1X,32('*'),' BXDATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I4,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I5,1X,1A18,1X,2(I1,1X),A7,A44)
 1004 FORMAT(I5)
 1005 FORMAT(F5.2,4X,I1,6X,14(A5,A3))
 1006 FORMAT(1X,A)
 1007 FORMAT(1X,32('*'),' BXDATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,2(I1,A))
 1008 FORMAT(A1,I3,I4,15(F5.2,I3))
 1009 FORMAT(A,A/17X,2(A,I2)/)
 1010 FORMAT(1X,I3)
 1011 FORMAT(1X,32('*'),' BXDATA ERROR ',32('*')//
     &       1X,'ERROR IN INPUT DATA FILE: LEVEL ',I4,' IS UNTIED')
 1012 FORMAT(1X,A,' - READ VALUE = ',A7)
 1013 FORMAT(1X,32('*'),' BXDATA ERROR ',32('*')//
     &  1X,'SUBROUTINE HAS BEEN CALLED USING THE OLD ARGUMENT LIST.' //
     &  1X,'MAKE THE FOLLOWING CHANGES TO THE VARIABLES PASSED TO'   /
     &  1X,'THE SUBROUTINE & THEN RECOMPILE AND LINK YOUR PROGRAM.'  //
     &  4X,'1) Increase the size of CSTRGA array from C*12 to C*18'  /
     &  4X,'2) Increase the only  dimension of SCEF from 8 to 14'    /
     &  4X,'3) Increase the first dimension of SCOM from 8 to 14'    )

 3000  FORMAT(/1X,30('*'),' BXDATA WARNING',30('*'),/,1x,
     &        '  Use of this subroutine is deprecated,',/,1x,
     &        '  Suggest using xxdata_04.for instead.',/, 1x, 75('*'))

C-----------------------------------------------------------------------
C
      RETURN
      END
