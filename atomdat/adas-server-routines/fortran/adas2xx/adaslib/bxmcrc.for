CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxmcrc.for,v 1.1 2004/07/06 11:44:37 whitefor Exp $ Date $Date: 2004/07/06 11:44:37 $
CX
      SUBROUTINE BXMCRC( NDTEM  , NDTRN  , NDLEV  ,
     &                   IT     , ICNT   , IL     ,
     &                   I1A    , I2A    ,
     &                   RATE   , DRATE  ,
     &                   CRC
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXMCRC *********************
C
C  PURPOSE: TO  CONSTRUCT  EXCITATION/DE-EXCIATATION   RATE  COEFFICIENT
C           MATRIX  'CRC' FOR TRANSITIONS BETWEEN ALL ENERGY LEVELS AT A
C           GIVEN TEMPERATURE 'IT' AND FOR A GIVEN TRANSITION TYPE
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF RECOMBINATIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  IT      = INDEX OF TEMPERATURE VALUE BEING ASSESSED
C  INPUT :  (I*4)  ICNT    = NUMBER OF SELECTED TRANSITIONS
C  INPUT :  (I*4)  IL      = NUMBER OF ENERGY LEVELS
C                                           (SEE: 'ITRN()')
C
C  INPUT :  (I*4)  I1A()   = SELECTED TRANSITION TYPE:
C                            LOWER ENERGY LEVEL INDEX.
C                            DIMENSION: TRANSITION INDEX
C  INPUT :  (I*4)  I2A()   = SELECTED TRANSITION TYPE:
C                            UPPER ENERGY LEVEL INDEX.
C                            DIMENSION: TRANSITION INDEX
C
C  INPUT :  (R*8)  RATE(,) = EXCITATION RATE COEFFS (cm**3/s)
C                            1st DIMENSION: TEMPERATURE INDEX
C                            2nd DIMENSION: TRANSITION INDEX
C  INPUT :  (R*8)  DRATE(,)= DE-EXCIT'N RATE COEFFS (cm**3/s)
C                            1st DIMENSION: TEMPERATURE INDEX
C                            2nd DIMENSION: TRANSITION INDEX
C
C  OUTPUT:  (R*8)  CRC(,)  = EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C
C           (I*4)  IS1     = ENERGY LEVEL ARRAY INDEX
C           (I*4)  IS2     = ENERGY LEVEL ARRAY INDEX
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C
C
C ROUTINES: NONE
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM              , NDTRN              , NDLEV       ,
     &           IT                 , ICNT               , IL
      INTEGER    IS1                , IS2                , IC
C-----------------------------------------------------------------------
      INTEGER    I1A(NDTRN)         , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     RATE(NDTEM,NDTRN)  , DRATE(NDTEM,NDTRN) ,
     &           CRC(NDLEV,NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  ZERO ARRAY CRC(,)
C-----------------------------------------------------------------------
         DO 1 IS1=1,IL
            DO 2 IS2=1,IL
               CRC(IS2,IS1)=0.0D0
    2       CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
         DO 3 IC=1,ICNT
            CRC(I1A(IC),I2A(IC)) = CRC(I1A(IC),I2A(IC)) + DRATE(IT,IC)
            CRC(I2A(IC),I2A(IC)) = CRC(I2A(IC),I2A(IC)) - DRATE(IT,IC)
            CRC(I2A(IC),I1A(IC)) = CRC(I2A(IC),I1A(IC)) + RATE(IT,IC)
            CRC(I1A(IC),I1A(IC)) = CRC(I1A(IC),I1A(IC)) - RATE(IT,IC)
    3    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
