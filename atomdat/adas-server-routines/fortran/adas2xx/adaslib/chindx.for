CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/chindx.for,v 1.3 2004/07/06 12:05:05 whitefor Exp $ Date $Date: 2004/07/06 12:05:05 $
CX
       SUBROUTINE CHINDX(  CNJL  ,  INDJL  ,  NJLEVX  ,  NJLEN  ,
     &                     CNBL  ,  INDBL  ,  BNDLS   ,  NGAP   ,
     &                     INDBLO   
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  FORTRAN 77 SUBROUTINE CHINDX
C
C  PURPOSE: TO RE-INDEX & ALTER TERMS FOR A BUNDLED SET OF LEVELS
C
C  AUTHOR: DAVID H.BROOKS
C
C  DATE: 28.04.95
C
C  MODIFIED: 23.01.96 DAVID H.BROOKS
C            SLIGHT ALTERATION TO METHOD TO TRAP SOME WIDER CASES.
C
C  VERSION:	1.3						DATE: 18.11.98
C  MODIFIED: DAVID H.BROOKS
C            FURTHER ALTERATION TO TRAP SOME WIDER CASES.
C-----------------------------------------------------------------------
       INTEGER INDJL, INDBL, BNDLS, NJLEVX
       INTEGER NGAP , MK   ,INDBLO, NJLEN
       INTEGER I    , J    ,  IMK  
C-----------------------------------------------------------------------
       CHARACTER CNJL(NJLEVX)*18, CNBL(NJLEVX)*18
C-----------------------------------------------------------------------
       DIMENSION INDJL(NJLEVX), INDBL(NJLEVX)
       DIMENSION BNDLS(NJLEVX), NGAP(NJLEVX)
       DIMENSION INDBLO(NJLEVX)
C-----------------------------------------------------------------------
C MAKE ALL THE INDICES/LEVELS/TERMS OF THE SAME BUNDLES BE THE SAME AS 
C THOSE FOR THE LOWEST INDEX/LEVEL/TERM IN THE BUNDLE
C-----------------------------------------------------------------------
       DO 10 I = 1 , NJLEN
         CNBL(I) = CNJL(I)
         INDBL(I) = INDJL(I)
         MK = NJLEN
         IF(BNDLS(I).GT.0.)THEN
           DO 20 J = 1 , NJLEN
             IF(BNDLS(I).EQ.BNDLS(J).AND.I.GT.J)THEN
                 IF(J.LT.MK)THEN
                   MK = J
                 ENDIF
               CNBL(I) = CNBL(MK)
               INDBL(I) = INDBL(MK)
             ENDIF
   20      CONTINUE
         ENDIF
   10  CONTINUE
C-----------------------------------------------------------------------
C INDEXING NOW LEAPS BETWEEN GROUPS SO COMPRESS IT TO MAKE IT ONLY 
C INCREASE ONE AT A TIME.
C-----------------------------------------------------------------------
       NGAP(1) = 1
       INDBLO(1) = 1
       DO 30 I = 2 , NJLEN
         INDBLO(I) = INDBL(I) 
         NGAP(I) = INDBL(I)-INDBL(I-1)
         IF(NGAP(I).GT.1)THEN
           IMK = INDBL(I-1)+1
   33      CONTINUE
             DO 35 J = 1, I
               IF(INDBL(J).EQ.IMK)THEN
                 IMK = IMK+1
                 IF(IMK.EQ.INDBL(I))THEN
                   GOTO 34
                 ELSE
                   GOTO 33
                 ENDIF
               ENDIF     
   35        CONTINUE
   34              CONTINUE
           INDBL(I) = IMK
           DO 40 J = 1 , NJLEN
             IF(INDBL(J).EQ.INDBLO(I))THEN
               INDBL(J) = INDBL(I)
             ENDIF
   40      CONTINUE           
         ENDIF
   30  CONTINUE
C-----------------------------------------------------------------------
       RETURN
       END
