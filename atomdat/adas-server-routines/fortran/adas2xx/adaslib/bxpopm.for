CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxpopm.for,v 1.1 2004/07/06 11:45:50 whitefor Exp $ Date $Date: 2004/07/06 11:45:50 $
CX
      SUBROUTINE BXPOPM( NDTEM  , NDDEN , NDMET , NDLEV ,
     &                   MAXT   , MAXD  , NMET  ,
     &                            DENSA , IMETR ,
     &                            LRSEL , LHSEL ,
     &                            RATIA , RATHA ,
     &                   STCKM  , STVRM , STVHM ,
     &                   POPAR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXPOPM *********************
C
C  PURPOSE: TO CONSTRUCT METASTABLE LEVEL POPULATIONS.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  MAXT    = NUMBER OF INPUT TEMPERATURES ( 1 ->'NDTEM')
C  INPUT :  (I*4)  MAXD    = NUMBER OF INPUT DENSITIES ( 1 ->'NDDEN')
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES LEVELS ( 1 ->'NDMET')
C
C  INPUT :  (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C
C  INPUT :  (L*4)  LRSEL   = .TRUE.  => FREE ELECTRON RECOMBINATION
C                                       REQUESTED.
C                          = .FALSE. => FREE ELECTRON RECOMBINATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN REQUESTED.
C                          = .FALSE. => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN NOT REQUESTED.
C
C  INPUT :  (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C  INPUT :  (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C
C  INPUT :  (R*8)  STCKM(,,)= METASTABLE POPULATIONS STACK:
C                               1st DIMENSION: METASTABLE INDEX
C                               2nd DIMENSION: TEMPERATURE INDEX
C                               3rd DIMENSION: DENSITY INDEX
C  INPUT :  (R*8)  STVRM(,,)= METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                              1st DIMENSION: METASTABLE INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C  INPUT :  (R*8)  STVHM(,,)= METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                              1st DIMENSION: METASTABLE INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C
C  OUTPUT:  (R*8)  POPAR(,,)= LEVEL POPULATIONS
C                              1st DIMENSION: LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              3rd DIMENSION: DENSITY INDEX
C                              (ON OUTPUT CONTAINS POPULATIONS FOR
C                               METASTABLE LEVELS ONLY.)
C
C           (R*8) DCOEF     = DENSITY MULTIPLIED BY RELEVANT RATIOS  FOR
C                             CALCULATING RECOMBINATION CONTRIBUTIONS.
C
C           (I*4) IT        = TEMPERATURE ARRAY INDEX
C           (I*4) IN        = DENSITY ARRAY INDEX
C           (I*4) IM        = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM        , NDDEN        , NDMET   , NDLEV   ,
     &          MAXT         , MAXD         , NMET
      INTEGER   IT           , IN           , IM
C-----------------------------------------------------------------------
      REAL*8    DCOEF
C-----------------------------------------------------------------------
      LOGICAL   LRSEL        , LHSEL
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDDEN)                ,
     &          RATIA(NDDEN)                , RATHA(NDDEN)
      REAL*8    STCKM(NDMET,NDTEM,NDDEN)    ,
     &          STVRM(NDMET,NDTEM,NDDEN)    , STVHM(NDMET,NDTEM,NDDEN)
      REAL*8    POPAR(NDLEV,NDTEM,NDDEN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C METASTABLE LEVEL POPULATIONS.
C-----------------------------------------------------------------------
C
         DO 1 IN=1,MAXD
            DO 2 IT=1,MAXT
               DO 3 IM=1,NMET
                  POPAR(IMETR(IM),IT,IN)=STCKM(IM,IT,IN)
    3          CONTINUE
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD FREE ELECTRON RECOMB. CONTRIBUTION TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            DO 4 IN=1,MAXD
               DCOEF = RATIA(IN)*DENSA(IN)
                  DO 5 IT=1,MAXT
                     DO 6 IM=1,NMET
                        POPAR(IMETR(IM),IT,IN)=POPAR(IMETR(IM),IT,IN) +
     &                                         ( DCOEF*STVRM(IM,IT,IN) )
    6                CONTINUE
    5             CONTINUE
    4       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD CHARGE EXCHANGE RECOMB. CONTRIBUTION TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            DO 7 IN=1,MAXD
               DCOEF = RATHA(IN)*RATIA(IN)*DENSA(IN)
                  DO 8 IT=1,MAXT
                     DO 9 IM=1,NMET
                        POPAR(IMETR(IM),IT,IN)=POPAR(IMETR(IM),IT,IN) +
     &                                         ( DCOEF*STVHM(IM,IT,IN) )
    9                CONTINUE
    8             CONTINUE
    7       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
