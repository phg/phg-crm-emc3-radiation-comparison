CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxsetp.for,v 1.7 2019/04/25 10:38:33 mog Exp $	Date $Date: 2019/04/25 10:38:33 $
CX

       SUBROUTINE BXSETP( IZ0    , IZ     ,
     &                    NDLEV  , IL     , ICNTE ,
     &                    CSTRGA , ISA    , ILA   , XJA  ,
     &                    STRGA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXSETP *********************
C
C  PURPOSE:  TO SET UP PARAMETERS IN THE SHARED POOLED FOR PANEL DISPLAY
C
C  CALLING PROGRAM: ADAS205/ADAS206
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'BXDATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C  OUTPUT: (C*22) STRGA() = LEVEL DESIGNATIONS
C
C          (C*8)  F6      = PARAMETER = 'VREPLACE'
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  J       = VALUE OF QUANTUM NUMBER L + 1
C          (I*4)  LFPOOL  = NO. OF LEVEL STRINGS SENT TO FUNCTION POOL
C
C          (C*2)  SZ0     =         NUCLEAR CHARGE READ
C          (C*2)  SZ      =  RECOMBINED ION CHARGE READ
C          (C*4)  SCNTE   =  NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (C*4)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*1)  CONFIG()= QUANTUM NUMBER (L) LETTERS
C                           DIMENSION: QUANTUM NUMBER L + 1
C          (C*8)  CHA()   = FUNCTION POOL NAMES: CHARGE VALUES
C          (C*8)  CHB()   = FUNCTION POOL NAMES: LEVEL DESIGNATIONS <=99
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  26/06/92 - ADAS91 PEB: PLACED A LIMIT OF 99 ON THE NUMBER OF
C                                 LEVEL STRINGS SENT TO FUNCTION POOL.
C          15/04/93 - AB: CONVERTED TO WRITE TO IDL VIA PIPE INSTEAD
C                         OF ISPF FUNCTION POOL.
C          14/03/95 - L. JALOTA : INCREASED CSTRG TO 18 BYTES IN LINE WITH 
C				  NEW ADF04 FILE FORMATS AND READ ROUTINES.
C          05/04/95 - TIM HAMMOND : ADDED SEVERAL CALLS TO XXFLSH ROUTINE
C                                   TO CLEAR THE PIPE AFTER IT HAS BEEN
C                                   WRITTEN TO.
C V 1.5:   23/11/95 - TIM HAMMOND : ALTERED FORMAT 1001 TO I4 FROM I3 
C                                   AND INCREASED SCNTE AND SIL FROM A3
C                                   TO A4 TO ALLOW FOR LARGER NUMBERS.
C
C VERSION: 1.6                               DATE: 29-04-2003
C MODIFIED: Martin O'Mullane
C		- Remove the restriction of only sending LFPOOL (99)
C                 values of level string identifer to IDL. Send the
C                 complete level list and improve the data handling 
C                 at the IDL level.
C 
C VERSION: 1.7                               DATE: 16-03-2012
C MODIFIED: Martin O'Mullane
C		- Change format statement to '(A)' from '*' to
C                 write strga to IDL.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IZ0           , IZ            , NDLEV    , IL     ,
     &          ICNTE
      INTEGER   ILEV          , ILVAL    , LFPOOL
      INTEGER   ISA(IL)       , ILA(IL)
C-----------------------------------------------------------------------
      REAL*8    XJA(IL)
C-----------------------------------------------------------------------
      CHARACTER F6*8
      CHARACTER SZ*2          , SZ0*2         , SCNTE*4  , SIL*4  ,
     &          CONFIG(20)*1
      CHARACTER CHA(4)*8      , CHB*8
      CHARACTER CSTRGA(IL)*18 , STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      PARAMETER ( F6='VREPLACE' )
C-----------------------------------------------------------------------
      SAVE      CHA            , CHB
C-----------------------------------------------------------------------
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
      DATA CHA   / '(SZ0)   ' , '(SZ)    ' , '(SCNTE) ' , '(SIL)   ' /
      DATA CHB   / '(STR**) ' /
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6 )

C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS.
C-----------------------------------------------------------------------
C
      WRITE(SZ0,1000) IZ0
      WRITE(SZ ,1000) IZ
      WRITE(SCNTE,1001) ICNTE
      WRITE(SIL  ,1001) IL

         DO 1 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1002)
     &                   CSTRGA(ILEV)(1:12),ISA(ILEV),CONFIG(ILVAL), 
     &                      XJA(ILEV)
               ENDIF
    1    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C SEND STRINGS TO ISPF FUNCTION POOL
C ULTIRX PORT - VARIABLES WRITTEN TO IDL VIA PIPE INSTEAD OF TO
C ISPF FUNCTION POOL
C-----------------------------------------------------------------------
C
C     ILEN=2
C     CALL ISPLNK( F6 , CHA(1) , ILEN , SZ0   )
C     ILEN=2
C     CALL ISPLNK( F6 , CHA(2) , ILEN , SZ    )
C     ILEN=3
C     CALL ISPLNK( F6 , CHA(3) , ILEN , SCNTE )
C     ILEN=3
C     CALL ISPLNK( F6 , CHA(4) , ILEN , SIL   )
C
      WRITE(PIPEOU,'(A2)') SZ0
      WRITE(PIPEOU,'(A2)') SZ
      WRITE(PIPEOU,'(A4)') SCNTE
      WRITE(PIPEOU,'(A4)') SIL
      CALL XXFLSH(PIPEOU)
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ONLY SEND LEVEL STRINGS TO FUNCTION POOL FOR FIRST 99 LEVELS
C ULTIRX PORT - VARIABLES WRITTEN TO IDL VIA PIPE INSTEAD OF TO
C ISPF FUNCTION POOL
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C

C Back to the original 
C      LFPOOL = MIN0(NDLEV,99)
      
      lfpool = il
      WRITE(PIPEOU,*) LFPOOL
      CALL XXFLSH(PIPEOU)
C
         DO 2 ILEV=1,LFPOOL
            WRITE(PIPEOU,'(A)') STRGA(ILEV)
    2    CONTINUE

      CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------

 1000 FORMAT(I2)
 1001 FORMAT(I4)
 1002 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
 1003 FORMAT(I2.2)

C-----------------------------------------------------------------------
C
      RETURN
      END
