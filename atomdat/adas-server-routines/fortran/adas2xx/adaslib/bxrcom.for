CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxrcom.for,v 1.1 2004/07/06 11:45:59 whitefor Exp $ Date $Date: 2004/07/06 11:45:59 $
CX
      SUBROUTINE BXRCOM( NDTEM  , NDTRN  , NDLEV  ,
     &                   NTIN   , TIN    , RCIN   ,
     &                   NTOUT  , TOUT   ,
     &                   ICNT   , ITRN   , ICLEV  ,
     &                   RCOUT  , LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXRCOM *********************
C
C  PURPOSE: TO ESTABLISH RECOMBINATION RATE COEFFICIENTS  FOR  A SET  OF
C           TEMPERATURES GIVEN BY THE ARRAY 'TOUT()' USING CUBIC SPLINES
C           ON  A SET OF RATE  COEFFICIENTS  COVERING  THE  TEMPERATURES
C           GIVEN BY THE ARRAY 'TIN()'.
C
C           RECOMBINATION TYPE IS SELECTED VIA 'ICNT' & 'ITRN'
C
C           RATE COEFFICIENTS ARE GIVEN FOR A NUMBER OF CAPTURING LEVELS
C           AND THE ARRAY 'RCOUT(,)' REPRESENTS  COEFFTS.  FOR COMBINAT-
C           IONS OF TEMPERATURE AND CAPTURING LEVEL INDEX.
C
C           SPLINE IS CARRIED OUT USING LOG(RATE COEFFICIENT VALUES)
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF RECOMBINATIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  NTIN    = NUMBER OF TEMPERATURES REPRESENTED IN THE
C                            INPUT DATA SET.
C  INPUT :  (R*8)  TIN()   = TEMPERATURES REPRESENTED IN INPUT DATA SET
C  INPUT :  (R*8)  RCIN(,) = RATE COEFF. REPRESENTED IN INPUT DATA SET
C                            1st DIMENSION: TEMPERATURE INDEX ('TIN')
C                            2nd DIMENSION: RECOMBINATION INDEX
C                                           (SEE: 'ITRN()')
C
C  INPUT :  (I*4)  NTOUT   = NUMBER OF ISPF SELECTED TEMPERATURES FOR
C                            OUTPUT.
C  INPUT :  (R*8)  TOUT()  = ISPF SELECTED TEMPERATURES FOR OUTPUT.
C
C  INPUT :  (I*4)  ICNT    = NUMBER OF SELECTED RECOMBINATIONS
C  INPUT :  (I*4)  ITRN()  = INDEX VALUES IN MAIN TRANSITION ARRAY WHICH
C                            REPRESENT RECOMBINASTION  OF THE  SELECTED
C                            TYPE
C                            USED TO SELECT APPROPRIATE RATE COEFFTS FOR
C                            RECOMBINATION TYPE.
C  INPUT :  (I*4)  ICLEV() = CAPTURING LEVELS INDICES.
C                            DIMENSION: 'TRANSITION'/RECOMBINATION INDEX
C
C  OUTPUT:  (R*8)  RCOUT(,)= SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C
C  OUTPUT:  (L*4)  LTRNG() = .TRUE. => TEMPERATURE VALUES WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                          = .FALSE.=>TEMPERATURE VALUE NOT WITHIN RANGE
C                                     READ FROM INPUT COPASE DATA SET.
C                            1st DIMENSION: TEMPERATURE INDEX.
C
C
C           (I*4)  NTDSN   = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C                                        ALLOWED IN INPUT DATA SET = 8
C           (I*4)  NLTEM   = PARAMETER = MUST BE >= 'NDTEM'
C
C           (I*4)  IOPT    = SPLINE END CONDITIONS/EXTRAPOLATION CONTROL
C                            SWITCH - SEE 'XXSPLE'
C                            I.E. DEFINES THE BOUNDARY DERIVATIVES.
C                            (VALID VALUES = 0, 1, 2, 3, 4)
C           (I*4)  IRECMB  = APPROPRIATE RECOMBINATN INDEX FOR 'RCIN(,)'
C           (I*4)  ICAP    = CAPTURING LEVEL INDEX BEING ASSESSED.
C           (I*4)  IC      = RECOMBINATION ARRAY INDEX
C           (I*4)  IT      = TEMPERATURE ARRAY INDEX
C
C           (R*8)  DYIN()  = INTERPOLATED DERIVATIVES
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C
C           (L*4)  LSETX   = .TRUE.  => X-AXES ('TIN()' VALUES) NEED TO
C                                       SET IN 'XXSPLE'.
C                            .FALSE. => X-AXES ('TIN()' VALUES) HAVE
C                                       BEEN SET IN 'XXSPLE'.
C                            (NOTE: 'LSETX' IS RESET BY 'XXSPLE')
C
C           (R*8)  LRCIN() = LOG ( 'RCIN(,)' ) FOR GIVEN CAPTURING LEVEL
C                            DIMENSION: TEMPERATURE INDEX ('TIN()')
C           (R*8)  LRCOUT()= LOG ( SPLINED RECOMBINATION RATE COEFTS )
C                            DIMENSION: TEMPERATURE INDEX ('TOUT()' )
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  31/01/91 - PE BRIDEN - ADAS91 - INTRODUCED 'LTRNG'
C                                        - REPLACED XXSPLN WITH XXSPLE
C
C UPDATE:  11/12/91 - PE BRIDEN - ADAS91 -NLTEM INCREASED FROM 20 to 101
C
C UPDATE:  10/06/92 - PE BRIDEN - ADAS91 -CORRECT ERROR - CHANGED
C                                         'ICAP=ICLEV(IC)' TO
C                                         'ICAP=ICLEV(IRECMB)'
C
C UPDATE:  20/05/93 - PE BRIDEN - ADAS91 -NTDSN INCREASED FROM 8 to 14
C                                         (REFLECTS CHANGES TO BXDATA)
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NTDSN              , NLTEM
C-----------------------------------------------------------------------
      PARAMETER( NTDSN = 14         , NLTEM = 101        )
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDTEM              , NDLEV      ,
     &           NTIN               , NTOUT              ,
     &           ICNT
      INTEGER    IOPT               , IRECMB             , ICAP       ,
     &           IC                 , IT
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      INTEGER    ICLEV(NDTRN)       , ITRN(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     TIN(NTDSN)         , RCIN(NTDSN,NDTRN)  ,
     &           TOUT(NDTEM)        ,
     &           RCOUT(NDTEM,NDLEV)
      REAL*8     DYIN(NTDSN)        ,
     &           LRCIN(NTDSN)       , LRCOUT(NLTEM)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTEM)
C-----------------------------------------------------------------------
      INTRINSIC  DLOG
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NLTEM.LT.NDTEM) STOP
     &                   ' BXRCOM ERROR: NDTEM > NLTEM. INCREASE NLTEM'
C-----------------------------------------------------------------------
      LSETX = .TRUE.
      IOPT  = 0
C***********************************************************************
         DO 1 IC=1,ICNT
            IRECMB=ITRN(IC)
C-----------------------------------------------------------------------
               DO 2 IT=1,NTIN
                  LRCIN(IT)=DLOG( RCIN(IT,IRECMB) )
    2          CONTINUE
C-----------------------------------------------------------------------
            CALL XXSPLE( LSETX , IOPT  , DLOG   ,
     &                   NTIN  , TIN   , LRCIN  ,
     &                   NTOUT , TOUT  , LRCOUT ,
     &                   DYIN  , LTRNG
     &                 )
C-----------------------------------------------------------------------
            ICAP=ICLEV(IRECMB)
C-----------------------------------------------------------------------
               DO 3 IT=1,NTOUT
                  RCOUT(IT,ICAP) = DEXP( LRCOUT(IT) )
    3          CONTINUE
C-----------------------------------------------------------------------
    1    CONTINUE
C***********************************************************************
      RETURN
      END
