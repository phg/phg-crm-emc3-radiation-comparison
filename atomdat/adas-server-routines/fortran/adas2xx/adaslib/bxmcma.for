CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxmcma.for,v 1.1 2004/07/06 11:44:31 whitefor Exp $ Date $Date: 2004/07/06 11:44:31 $
CX
      SUBROUTINE BXMCMA( NDLEV  ,
     &                   NORD   , IORDR  ,
     &                   CC     ,
     &                   CMAT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXMCMA *********************
C
C  PURPOSE: TO  STACK  UP  NON-METASTABLE/ORDINARY  EXCITED  LEVEL  RATE
C           MATRIX 'CMAT' FROM WHOLE RATE MATRIX 'CC' FOR ALL TRANSIT'NS
C           BETWEEN ALL ENERGY LEVELS AT A FIXED TEMPERATURE AND DENSITY
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  NORD    = NUMBER OF NON-METASTABLE/ORDINARY EXCITED
C                            ENERGY LEVELS.
C  INPUT :  (I*4)  IORDR() = INDEX OF NON-METASTABLE/ORDINARY EXCITED
C                            LEVELS IN COMPLETE LEVEL LIST.
C
C  INPUT :  (R*8)  CC(,)   = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C
C  OUTPUT:  (R*8)  CMAT(,) = RATE MATRIX COVERING ALL NON-METASTABLE/
C                            ORDINARY EXCITED LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C
C           (I*4)  IS1     = ORDINARY EXCITED LEVEL ARRAY INDEX
C           (I*4)  IS2     = ORDINARY EXCITED LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV              , NORD
      INTEGER    IS1                , IS2
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8     CC(NDLEV,NDLEV)    ,
     &           CMAT(NDLEV,NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IS1=1,NORD
               DO 2 IS2=1,NORD
                  CMAT(IS2,IS1) = CC(IORDR(IS2),IORDR(IS1))
    2          CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
