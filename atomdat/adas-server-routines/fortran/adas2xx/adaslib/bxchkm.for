CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxchkm.for,v 1.2 2004/07/06 11:43:32 whitefor Exp $ Date $Date: 2004/07/06 11:43:32 $
CX
      SUBROUTINE BXCHKM( NMET   , IMETR , ICNTE , IE1A , LMETR )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXCHKM *********************
C
C  PURPOSE: TO CHECK IF TRANSITIONS EXIST TO THE METASTABLE LEVELS.
C
C  CALLING PROGRAM: ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NMET    = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  INPUT : (I*4)   IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  INPUT : (I*4)   ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT : (I*4)   IE1A()  = ELECTRON IMPACT TRANSITION: LOWER ENERGY
C                            LEVEL INDEX.
C
C  OUTPUT: (L*4)   LMETR() = .TRUE.  =>ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                       'IMETR()'.
C                            .FALSE. =>ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C
C          (I*4)   I       = GENERAL USE
C          (I*4)   J       = GENERAL USE
C
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NMET        , ICNTE
      INTEGER   I          , J
      INTEGER   IMETR(NMET) , IE1A(ICNTE)
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NMET)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
         DO 1 I = 1,NMET
            LMETR(I)=.FALSE.
               DO 2 J = 1,ICNTE
                  IF ( IE1A(J) .EQ. IMETR(I) ) LMETR(I)=.TRUE.
    2          CONTINUE
            IF ( .NOT.LMETR(I) ) WRITE(0,1000) I,IMETR(I)
    1    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,31('*'),' BXCHKM WARNING ',31('*')/
     &        1X,' METASTABLE INDEX: ',I3,' - LEVEL INDEX: ',I3/
     &        1X,' NO ELECTRON IMPACT TRANSITIONS EXIST TO THIS LEVEL'/
     &        1X,79('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
