CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxoutg.for,v 1.7 2004/07/06 11:45:15 whitefor Exp $ Date $Date: 2004/07/06 11:45:15 $
CX
      SUBROUTINE BXOUTG( LGHOST , DATE  ,
     &                   NDLEV  , NDTEM , NDDEN , NDMET  ,
     &                   TITLED , TITLE , GTIT1 , DSNINC ,
     &                   IZ     , ITSEL , TEV   ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN  , YMAX   ,
     &                   IL     , NMET  , NORD  , MAXD   ,
     &                   LMETR  , IMETR , IORDR , DENSA  ,
     &                   STRGA  , STACK
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXOUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATIONS WITH IDL
C
C  CALLING PROGRAM: ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C
C  INPUT : (C*3)  TITLED  = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
CX INPUT : (C*80) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ      = RECOMBINED ION CHARGE
C  INPUT : (I*4)  ITSEL   = INDEX OF TEMPERATURE SELECTED FROM GRAPH
C  INPUT : (R*8)  TEV     = SELECTED ELECTRON TEMPERATURE (EV) FOR GRAPH
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN    = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX    = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN    = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX    = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  IL      = NUMBER OF ENERGY LEVELS = 'NMET' + 'NORD'
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C  INPUT : (I*4)  NORD    = NUMBER OF ORDINARY LEVELS
C  INPUT : (I*4)  MAXD    = NUMBER OF INPUT ELECTRON DENSITIES
C
C  INPUT : (L*4)  LMETR() = .TRUE.  => ELECTRON IMPACT TRANSITION EXISTS
C                                      TO THE METASTABLE LEVEL  GIVEN BY
C                                      'IMETR()'.
C                           .FALSE. => ELECTRON  IMPACT  TRANSITIONS  DO
C                                      NOT EXIST TO THE METASTABLE LEVEL
C                                      GIVEN BY 'IMETR()'.
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C  INPUT : (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C  INPUT : (R*8)  DENSA() = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*22) STRGA() = LEVEL DESIGNATIONS
C  INPUT : (R*4) STACK(,,,)= POPULATION DEPENDENCE
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C                             4th DIMENSION: DENSITY INDEX
C
C          (I*4)  NDIM1   = PARAMETER = MAXIMUM NUMBER OF DENSITY VALUES
C                           (MUST NOT BE LESS THAN 'NDDEN')
C          (I*4)  NDIM2   = PARAMETER = MAXIMUM NUMBER OF LEVELS (ORD.)
C                           (MUST NOT BE LESS THAN 'NDLEV')
C          (I*4)  NGPIC   = PARAMETER = MAXIMUM NUMBER OF LEVEL POPULAT-
C                           IONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV   = PARAMETER = MAXIMUM NUMBER OF ENERGY LEVELS
C                           WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN  = PARAMETER = IN DEFAULT GRAPH SCALING IS THE
C                           MINIMUM Y-VALUE THAT IS ALLOWED.
C                           (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  ID      = DENSITY INDEX NUMBER FOR ARRAY USE
C          (I*4)  IM      = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  ILEV    = (ORDINARY) LEVEL INDEX NUMBER FOR ARRAY USE
C          (I*4)  IORD1   = INITIAL ORDINARY LEVEL FOR CURRENT GRAPH
C          (I*4)  IORD2   = FINAL ORDINARY LEVEL FOR CURRENT GRAPH
C          (I*4)  IPLOT   = CO-ORDINATE ID AT WHICH LEVEL INDEX VALUE
C                           FOR GRAPH LINE IS TO BE PLOTTED.
C          (I*4)  ILMAX   = MINIMUM OF: NO. OF ENERGY LEVELS OR 'NGLEV'
C
C          (R*4)  XHIGH   = UPPER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XLOW    = LOWER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YHIGH   = UPPER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YLOW    = LOWER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  X()     = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            ELECTRON DENSITIES
C          (R*4)  Y(,)    = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            LEVEL POPULATIONS.
C                            1st DIMENSION = ELECTRON DENSITY INDEX
C                            2nd DIMENSION = ORDINARY  LEVEL  INDEX
C
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3      = BLANK 3 BYTE STRING
C          (C*13) DNAME   = '       DATE: '
C          (C*13) FNAME   = 'INPUT FILE : '
C          (C*13) GNAME   = 'GRAPH TITLE: '
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*30) STRG1   = HEADING FOR LEVEL ASSIGNMENTS
C          (C*30) STRG2   = HEADING FOR LEVEL ASSIGNMENTS
C          (C*30) STRG3   = TEMPORARY STRING FOR LEVEL ASSIGNMENTS
C          (C*80) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLED,IZ,TEV').
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C          (L*4)  LGTXT   = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
C                         = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   ONE      = PARAMETER = THE INTEGER VALUE 1
C          (I*4)   ZERO     = PARAMETER = THE INTEGER VALUE 0
C          (I*4)   I        = LOOP INCREMENT
C          (I*4)   J        = LOOP INCREMENT
C          (I*4)   K        = LOOP INCREMENT
C          (I*4)   L        = LOOP INCREMENT
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
CX          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXFLSH      ADAS      FLUSHES I/O STREAM
C
C
C AUTHOR:  ANDREW BOWEN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    01/04/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 11-05-93
C MODIFIED: ANDREW BOWEN 
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 21-05-93
C MODIFIED: ANDREW BOWEN 
C               - DATASET NAME VARIABLE EXTENDED TO 80 CHARACTERS
C
C VERSION: 1.3                          DATE: 04-04-95
C MODIFIED: TIM HAMMOND
C               - CHANGED STACK FROM REAL*8 TO REAL*4 IN LINE WITH
C                 OTHER ROUTINES
C
C VERSION: 1.4                          DATE: 03-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - TIDIED UP HEADER COMMENTS
C               - CHANGED OUTPUT OF STACK FROM FOUR LEVEL IMPLIED DO
C                 TO SINGLE IMPLIED AND THREE LEVEL NESTED DO.
C
C VERSION: 1.5                          DATE: 15-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - INCREASED PARAMETER NDIM1 20 -> 24
C
C VERSION: 1.6                          DATE: 17-06-96
C MODIFIED: WILLIAM OSBORN
C               - ADDED PIPE FLUSHES AND MADE EXPLICIT THE LOOPS
C
C VERSION: 1.7                          DATE: 14-10-96
C MODIFIED: WILLIAM OSBORN
C               - ADDED WRITE TO I4UNIT FOR HP MACHINES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV    , I4UNIT
C-----------------------------------------------------------------------
      REAL*4    CUTMIN   , GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 24  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
      PARAMETER ( CUTMIN = 1.0E-30 , GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   NDLEV    , NDTEM    , NDDEN    , NDMET
      INTEGER   IL       , NMET     , NORD     , MAXD     ,
     &          IZ       , ITSEL
      INTEGER   ID       , IM       , ILEV     , IORD1    , IORD2    ,
     &          IPLOT    , ILMAX
C-----------------------------------------------------------------------
      REAL*4    XHIGH    , XLOW     ,
     &          YHIGH    , YLOW
C-----------------------------------------------------------------------
      REAL*8    TEV      ,
     &          XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
CX  DSNINC CHANGED TO 80 CHARS
CX
      CHARACTER TITLED*3 , TITLE*40 , GTIT1*40 , DSNINC*80
      CHARACTER GRID*1   , PIC*1    , C3*3     , DATE*8   ,
     &          DNAME*13 , FNAME*13 , GNAME*13 , XTIT*23  , YTIT*23  ,
     &          STRG1*30 , STRG2*30 , STRG3*30 , ISPEC*80 , CADAS*80
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)        , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*4    X(NDIM1)            , Y(NDIM1,NDIM2)
C-----------------------------------------------------------------------
      CHARACTER STRGA(NDLEV)*22
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDDEN)
      REAL*4    STACK(NDLEV,NDMET,NDTEM,NDDEN)
C-----------------------------------------------------------------------
      LOGICAL   LMETR(NDMET)
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
      INTEGER    I       , J        , K        , L
C-----------------------------------------------------------------------
      SAVE      CADAS
C-----------------------------------------------------------------------
      DATA GRID  /' '/   ,
     &     PIC  /' '/    ,
     &     C3   /'   '/  ,
     &     CADAS/' '/
      DATA DNAME/'       DATE: '/,
     &     FNAME/'INPUT FILE : '/,
     &     GNAME/'GRAPH TITLE: '/
      DATA XTIT/'ELECTRON DENSITY (CM-3)'/
      DATA YTIT/'N(I)/(NE*N(**))  (CM+3)'/
      DATA STRG1/'----- LEVEL  ASSIGNMENTS -----'/,
     &     STRG2/'INDEX       DESIGNATION       '/
      DATA ISPEC(1:40)/'POPULATION DEPENDENCE ON METASTABLES:   '/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C     LGTXT = .TRUE.
C     ILMAX = MIN0( NGLEV , IL )
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
C     WRITE(ISPEC(41:80),1000) TITLED,IZ,TEV
C     CALL XXADAS( CADAS )
C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(I4UNIT(-1),*) " "
      WRITE(PIPEOU,*) NDLEV
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDTEM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDDEN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NDMET
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TITLED
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TITLE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) GTIT1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) DSNINC
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) ITSEL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TEV
      CALL XXFLSH(PIPEOU)

CX REDUNDANT          LGRD1
CX SET IN IDL         LDEF1
CX SET IN IDL         XMIN
CX SET IN IDL         XMAX
CX SET IN IDL         YMIN
CX SET IN IDL         YMAX

      WRITE(PIPEOU,*) IL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NMET
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NORD
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) MAXD
      CALL XXFLSH(PIPEOU)

      DO 100 I = 1 , NMET
         IF (LMETR(I)) THEN
            WRITE(PIPEOU,*) ONE
         ELSE
            WRITE(PIPEOU,*) ZERO
         ENDIF
         CALL XXFLSH(PIPEOU)
  100 CONTINUE

      DO 200 I=1,NMET
         WRITE(PIPEOU,*) IMETR(I)
         CALL XXFLSH(PIPEOU)
 200  CONTINUE
      DO 210 I=1,NORD
         WRITE(PIPEOU,*) IORDR(I)
         CALL XXFLSH(PIPEOU)
 210  CONTINUE
      DO 220 I=1,MAXD
         WRITE(PIPEOU,*) DENSA(I)
         CALL XXFLSH(PIPEOU)
 220  CONTINUE
      DO 230 I=1,IL
         WRITE(PIPEOU,'(A22)') STRGA(I)
         CALL XXFLSH(PIPEOU)
 230  CONTINUE
C
      DO 110 L = 1 , MAXD
         DO 120 K = 1 , NDTEM
            DO 130 J = 1 , NMET
               DO 140 I=1,IL
                  WRITE(PIPEOU,*) STACK(I,J,K,L)
                  CALL XXFLSH(PIPEOU)
 140           CONTINUE
  130       CONTINUE
  120    CONTINUE
  110 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
