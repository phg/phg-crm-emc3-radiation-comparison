CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxout0.for,v 1.4 2004/07/06 11:44:53 whitefor Exp $ Date $Date: 2004/07/06 11:44:53 $
CX
      SUBROUTINE BXOUT0( IUNIT  , DATE   , PRGTYP , DSNC80 ,
     &                   TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH  ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA  , WA ,
     &                   ER     ,
     &                   NV     , TSCEF
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXOUT0 *********************
C
C  PURPOSE: TO OUTPUT ION SPECIFICATIONS, INDEXED ENERGY LEVELS AND
C           WAVE NUMBERS RELATIVE  TO GROUND TO STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  IUNIT  = OUTPUT STREAM NUMBER
C  INPUT :  (C*8)  DATE   = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (C*1)  PRGTYP = PROGRAM TYPE
C  INPUT :  (C*80) DSNC80 = INPUT COPASE DATA SET NAME
C  INPUT :  (C*80) DSNP80 = INPUT PROTON DATA SET NAME
C
C  INPUT :  (C*3)  TITLED = ELEMENT SYMBOL.
C  INPUT :  (I*4)  IZ     =  RECOMBINED ION CHARGE
C  INPUT :  (I*4)  IZ0    =         NUCLEAR CHARGE
C  INPUT :  (I*4)  IZ1    = RECOMBINING ION CHARGE
C                            (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT :  (R*8)  BWNO   = IONISATION POTENTIAL (CM-1)
C
C  INPUT :  (I*4)  ICNTE  = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTP  = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTR  = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT :  (I*4)  ICNTH  = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT :  (I*4)  IL     = NUMBER OF ENERGY LEVELS
C
C  INPUT :  (I*4)  IA()   = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT :  (I*4)  ISA()  = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT :  (I*4)  ILA()  = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT :  (R*8)  XJA()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  WA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  INPUT : (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           FOR LEVEL 'IA()'
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURE (NOTE: TE=TP=TH)
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C
C          (R*8)  WN2RYD  = PARAMETER =
C                           WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (R*8)  BRYDO   = IONISATION POTENTIAL (RYDBERGS)
C          (R*8)  BWN     = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           WAVE NUMBERS (CM-1).
C          (R*8)  BRYD    = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           RYDBERGS.
C
C          (I*4)  I       = GENERAL USE
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 5023
C
C DATE:    09/10/90
C
C UPDATE:  17/01/91 - PE BRIDEN: ADDED HEADER INFORMATION TO OUTPUT
C                              - RENAMED SUBROUTINE (ORIGINALLY BXWR7A)
C
C UPDATE:  23/01/91 - PE BRIDEN: REFORMATTED OUTPUT. INTRODUCED 'WN2RYD'
C                                'BRYD' & 'BRYDO'. RENAMED 'BW' -> 'BWN'
C                               - ADDED ARGUMENTS 'TSCEF' AND 'NV'.
C                               - ADDED ARGUMENTS 'ICNTE, ICNTP , ICNTR
C                                 and ICNTH'.
C
C UPDATE:  29/01/91 - PE BRIDEN: SET 'CADAS' TO BLANK AT START (VIA DATA
C                                STATEMENT) AND ADDED 'SAVE CADAS'.
C
C UPDATE:  30/07/92 - PE BRIDEN: 'XJA' VALUES NOW  OUTPUT  USING  FORMAT
C                                F6.1 INSTEAD OF F4.1 - THEREFORE FORMAT
C                                STATEMENT NUMBERED 1005 HAS BEEN EDITED
C
C UPDATE:  20/05/93 - PE BRIDEN - ADAS91: TO REFLECT CHANGES IN BXDATA
C                                         THE FOLLOWING ARRAY DIMENSION/
C                                         SIZE CHANGES WERE MADE:
C                                       1) CHARACTER CSTRGA *12 -> *18
C                                          (CHANGED FORMAT STMT 1005)
C                                       2) TSCEF(8,3)   -> TSCEF(14,3)
C UNIX PORT:
C VERSION: 1.3				DATE: 24/10/95
C MODIFIED:	TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C		- INCREASED FORMAT 1008 FROM I3 TO I4 TO ALLOW FOR MORE
C   		  THAN 1000 TRANSITIONS.
C               - ADDED CHECK FOR A NULL FILENAME OF DSNP80
C               - TIDIED UP SOME OF THE FORMAT STATEMENTS
C
C VERSION: 1.4				DATE: 29/04/2003
C MODIFIED:	Martin O'Mullane
C		- Remove DSNP80 parameter for the (now redundant)
C                 proton impact file.
C		- Increased format 1008 from I4 to I6 to allow for 
c   		  even more transitions.
C
C-----------------------------------------------------------------------
      REAL*8     WN2RYD
C-----------------------------------------------------------------------
      PARAMETER( WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
      INTEGER    I
      INTEGER    IUNIT         ,
     &           IZ            , IZ0            , IZ1        ,
     &           ICNTE         , ICNTP          , ICNTR      , ICNTH  ,
     &           IL            , NV
C-----------------------------------------------------------------------
      REAL*8     BWNO          , BWN            , BRYDO      , BRYD
C-----------------------------------------------------------------------
      CHARACTER  TITLED*3      , DATE*8         , PRGTYP*1   ,
     &           DSNC80*80     , DSNP80*80      , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    IA(IL)        , ISA(IL)        , ILA(IL)
C-----------------------------------------------------------------------
      REAL*8     XJA(IL)       , WA(IL)         , ER(IL)
      REAL*8     TSCEF(14,3)
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*18
C-----------------------------------------------------------------------
      SAVE       CADAS
C-----------------------------------------------------------------------
      DATA       CADAS/' '/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      BRYDO = WN2RYD * BWNO
C
C-----------------------------------------------------------------------
C GATHER ADAS HEADER AND WRITE IT OUT
C-----------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
      WRITE(IUNIT,1000) CADAS(2:80)
C
C-----------------------------------------------------------------------
C OUTPUT ION AND ENERGY LEVEL DETAILS
C-----------------------------------------------------------------------
C
         IF (PRGTYP.EQ.'M') THEN
            WRITE(IUNIT,1001) 'METASTABLE POPULATION ','ADAS205',DATE
         ELSE
            WRITE(IUNIT,1001) 'LINE POWER CALCULATION','ADAS206',DATE
         ENDIF
      WRITE(IUNIT,1002) DSNC80
      WRITE(IUNIT,1003) TITLED , IZ , IZ0 , IZ1 , BWNO , BRYDO
      WRITE(IUNIT,1004)
C
         DO 1 I=1,IL
            BWN  = BWNO  - WA(I)
            BRYD = BRYDO - ER(I)
            WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                        ISA(I) , ILA(I)    , XJA(I) ,
     &                        WA(I)  , ER(I)     ,
     &                        BWN    , BRYD
    1    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT 'INPUT COPASE FILE' TEMPERATURES
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1006)
         DO 2 I=1,NV
            WRITE(IUNIT,1007) I , TSCEF(I,1) , TSCEF(I,2) , TSCEF(I,3)
    2    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT 'INPUT COPASE FILE' INFORMATION
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1008) ICNTE , ICNTP , ICNTH , ICNTR
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(A79)
 1001 FORMAT(27('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,27('*')/)
 1002 FORMAT('INPUT COPASE FILE NAME: ',A80/)
 1009 FORMAT('INPUT PROTON FILE NAME: ',A80/)
 1003 FORMAT(1X,'ION',6X,'NUCLEAR',8X,'RECOMBINING',6X,
     &           7('-'),' IONIZATION POTENTIAL ',7('-')/
     &           9X,'CHARGE (Z0)',4X,'ION CHARGE (Z1)',
     &        4X,'(wave number <cm-1>)',5X,'(rydbergs)'/
     &        1X,78('-')/
     &        1X,1A3,I2,7X,I3,14X,I3,10X,F15.0,5X,F15.7//)
 1004 FORMAT(55('-'),' ENERGY LEVELS ',55('-')/
     &        2X,'INDEX',4X,'CONFIGURATION',3X,'(2S+1)L(J)',7X,
     &           5('-'),' ENERGY RELATIVE TO LEVEL 1 ',6('-'),4X,
     &           'ENERGY RELATIVE TO IONISATION POTENTIAL'/
     &       45X,2( '(wave number <cm-1>)',5X,'(rydbergs)' ,8X )/
     &        1X,125('-'))
 1005 FORMAT( 1X,I4,2X,A18,2X,'(',I1,')',I1,'(',F6.1,')',
     &        2(6X,F15.0,5X,F15.7,2X) )
 1006 FORMAT(/'-- INPUT COPASE FILE TEMPERATURES: (TE=TP=TH) --'/
     &            1X,'INDEX',4X,'(kelvin)',8X,'(eV)',8X,'(reduced)'/
     &            1X,48('-'))
 1007 FORMAT(2X,I2,1P,3(4X,D10.2))
 1008 FORMAT(/'INPUT COPASE FILE INFORMATION:'/,32('-')/
     &       'NUMBER OF ELECTRON IMPACT TRANSITIONS    =',1X,I6/
     &       'NUMBER OF PROTON   IMPACT TRANSITIONS    =',1X,I6/
     &       'NUMBER OF CHARGE EXCHANGE RECOMBINATIONS =',1X,I6/
     &       'NUMBER OF FREE   ELECTRON RECOMBINATIONS =',1X,I6)
C-----------------------------------------------------------------------
      RETURN
      END
