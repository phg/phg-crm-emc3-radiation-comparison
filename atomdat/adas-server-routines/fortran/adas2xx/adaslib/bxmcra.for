CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxmcra.for,v 1.1 2004/07/06 11:44:34 whitefor Exp $ Date $Date: 2004/07/06 11:44:34 $
CX
      SUBROUTINE BXMCRA( NDTRN  , NDLEV  ,
     &                   ICNT   , IL     ,
     &                   I1A    , I2A    ,
     &                   AVAL   ,
     &                   CRA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXMCRA *********************
C
C  PURPOSE: TO CONSTRUCT A-VALUE MATRIX  'CRA'  FOR TRANSITIONS  BETWEEN
C           ALL ENERGY LEVELS.
C
C  CALLING PROGRAM:  ASAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTRN   = MAXIMUM NUMBER OF RECOMBINATIONS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  ICNT    = NUMBER OF SELECTED TRANSITIONS
C  INPUT :  (I*4)  IL      = NUMBER OF ENERGY LEVELS
C                                           (SEE: 'ITRN()')
C
C  INPUT :  (I*4)  I1A()   = SELECTED TRANSITION TYPE:
C                            LOWER ENERGY LEVEL INDEX.
C                            DIMENSION: TRANSITION INDEX
C  INPUT :  (I*4)  I2A()   = SELECTED TRANSITION TYPE:
C                            UPPER ENERGY LEVEL INDEX.
C                            DIMENSION: TRANSITION INDEX
C
C  INPUT :  (R*8)  AVAL()  = A-VALUE (sec-1)
C                            DIMENSION: TRANSITION INDEX
C
C  OUTPUT:  (R*8)  CRA(,)  = A-VALUE  (sec-1)  MATRIX  COVERING  ALL
C                            TRANSITIONS.
C                            1st DIMENSION: LOWER ENERGY LEVEL INDEX
C                            2nd DIMENSION: UPPER ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C
C           (I*4)  IS1     = ENERGY LEVEL ARRAY INDEX
C           (I*4)  IS2     = ENERGY LEVEL ARRAY INDEX
C           (I*4)  IC      = TRANSITION ARRAY INDEX
C
C ROUTINES: NONE
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTRN              , NDLEV       ,
     &           ICNT               , IL
      INTEGER    IS1                , IS2                , IC
C-----------------------------------------------------------------------
      INTEGER    I1A(NDTRN)         , I2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8     AVAL(NDTRN)        ,
     &           CRA(NDLEV,NDLEV)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  ZERO ARRAY CRA(,)
C-----------------------------------------------------------------------
         DO 1 IS1=1,IL
            DO 2 IS2=1,IL
               CRA(IS2,IS1)=0.0D0
    2       CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
         DO 3 IC=1,ICNT
            CRA(I1A(IC),I2A(IC)) = CRA(I1A(IC),I2A(IC)) + AVAL(IC)
            CRA(I2A(IC),I2A(IC)) = CRA(I2A(IC),I2A(IC)) - AVAL(IC)
    3    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
