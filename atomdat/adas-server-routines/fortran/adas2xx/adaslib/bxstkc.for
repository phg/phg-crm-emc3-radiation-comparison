CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas2xx/adaslib/bxstkc.for,v 1.1 2004/07/06 11:46:40 whitefor Exp $ Date $Date: 2004/07/06 11:46:40 $
CX
      SUBROUTINE BXSTKC( NDLEV  , NDMET  ,
     &                   NORD   , NMET   ,
     &                   IORDR  , IMETR  ,
     &                   CC     , STCK   ,
     &                   CRED
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BXSTKC *********************
C
C  PURPOSE: TO STACK UP IN  'CRED'  THE TRANSITION RATE BETWEEN METASTA-
C           BLE LEVELS FOR A GIVEN TEMPERATURE STABLE LEVEL FOR A GIVEN
C           TEMPERATURE AND DENSITY.
C
C  CALLING PROGRAM:  ADAS205/ADAS206
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (R*8)  CC(,)   = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C  INPUT :  (R*4)  STCK(,) = POPULATION MATRIX COVERING ALL NON-METAST-
C                            ABLE/ORDINARY EXCITED LEVELS AS FUNCTION
C                            OF METASTABLE INDEX.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C
C  OUTPUT:  (R*8)  CRED(,) = MATRIX  OF  TRANSITION   RATES   BETWEEN
C                            METASTABLE LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C
C           (I*4)  IM1     = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IM2     = METASTABLE LEVEL ARRAY INDEX
C           (I*4)  IS      = ORDINARY EXCITED LEVEL INDEX
C
C
C ROUTINES: NONE
C
C NOTE:
C           CRED(IM1,IM2) = ( the transition rate from IM2 to IM1 )
C                                              +
C                           SUM( (the transistion rate from ordinary
C                                 level IS to IM1)  x (the population
C                                 in metastable level IM2 that excite
C                                 to oridinary level IS) )
C
C                           ABOVE SUM IS OVER ALL ORDINARY LEVELS.
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  20/05/93 - P BRIDEN: STCK ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV              , NDMET              ,
     &           NORD               , NMET
      INTEGER    IM1                , IM2                , IS
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)       , IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8     CC(NDLEV,NDLEV)    , CRED(NDMET,NDMET)
      REAL*4     STCK(NDLEV,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IM1=1,NMET
            DO 2 IM2=1,NMET
               CRED(IM1,IM2) = CC(IMETR(IM1),IMETR(IM2))
                 DO 3 IS=1,NORD
                    CRED(IM1,IM2) = CRED(IM1,IM2) +
     &                              ( CC(IMETR(IM1),IORDR(IS)) *
     &                                    DBLE(STCK(IS,IM2))     )
    3            CONTINUE
    2       CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
