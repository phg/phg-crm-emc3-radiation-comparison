CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas102/spfman5e.for,v 1.5 2007/05/15 11:18:04 allan Exp $ Date %G
CX
       SUBROUTINE SPFMAN5E( Z0    , Z     , ZEFF  , TITLE , IETYP , 
     &                      IXTYP , IND1  , IND2  , WI, WJ, EI, EJ,  
     &                      IATYP , ACOEFF, IFTYP , IOTYP , IFOUT ,
     &                      IXMAX , ITMAX , EDAT  , XDAT  , TDAT  ,
     &                      IIORD , IIBTS , IIFPT , IIXOP , IIDIF , 
     &                      XTIT1 , IGRD1 , XL1   , XU1   ,
     &                      YL1   , YU1   , XTIT2 , IGRD2 , 
     &                      XL2   , XU2   , YL2   , YU2   , IWRITE,
     &                      FXC2  , FXC3  , XA    , YA    , APOMA ,
     &                      DIFOMA, TOA   , GOA   , APGOA , EXCRA ,
     &                      DEXCRA, GBARFA,  
     &                      ICT   , ITOUT , S     , FIJ   , EIJ   
     &                     )
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: ANALYSE ELECTRON IMPACT RATE DATA AND CONVERT TO 
C  RATE COEFFICIENTS                                                    
C                                                                       
C  VARIOUS FORMS OF DATA ENTRY ARE ALLOWED                              
C                                                                       
C  EXTENDED ARRAY DIMENSION VERSION OF SPFMAIN5                         
C                                                                       
C  DATA IS FITTED WITH APPROXIMATE FORMS TO AID INTERPOLATION DEPENDING 
C  ON THE TRANSITION TYPE. THESE ARE                                    
C                 1. DIPOLE                                             
C                 2. NON-DIPOLE                                         
C                 3. SPIN CHANGE                                        
C                 4. OTHER                                              
C                                                                       
C  DATA ENTRY IS VIA CALL TO PANEL SUBROUTINE  SPFMA4E  AS FOLLOWS:     
C                                                                       
C  INPUT                                                                
C      IPAN = INITIAL PANEL NUMBER AT START                             
C                                                                       
C  OUTPUT                                                               
C      IPAN   = FINAL PANEL NUMBER                                      
C      ANS    = YES  - FINISH UP CALCULATION SINCE NO MORE CASES        
C             = NO   - DATA FOR NEW CASE RETURNED                       
C      Z0     = NUCLEAR CHARGE OF ION                                   
C      Z      = ION CHARGE                                              
C      ZEFF   = ION CHARGE + 1                                          
C      TITLE  = TITLE FOR CASE                                          
C      IETYP  = 1  LEVEL ENERGIES IN CM-1                               
C             = 2  LEVEL ENERGIES IN RYD                                
C      IXTYP  = 1  DIPOLE TRANSITION                                    
C             = 2  NON-DIPOLE TRANSITION                                
C             = 3  SPIN CHANGE TRANSITION                               
C             = 4  OTHER                                                
C      IND1   = LOWER LEVEL INDEX  (USER CHOICE)                        
C      IND2   = UPPER LEVEL INDEX  (USER CHOICE)                        
C      WI     = LOWER LEVEL STATISTICAL WEIGHT                          
C      WJ     = UPPER LEVEL STATISTICAL WEIGHT                          
C      EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS)                  
C      EJ     = UPPER LEVEL ENERGY                                      
C      IATYP  = 1  A-COEFFICIENT RETURNED                               
C             = 2  OSCILLATOR STRENGTH RETURNED                         
C             = 3  LINE STRENGTH RETURNED                               
C      ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM, DIPOLE CASE ONLY)
C      IFTYP  = 1 KELVIN                     FOR SOURCE TEMP. UNITS           
C             = 2  EV                        FOR SOURCE TEMP. UNITS            
C             = 3 SCALED UNITS (TE(K)/Z1**2) FOR SOURCE TEMP. UNITS       
C             = 4 REDUCED UNITS (KTE/EIJ)    FOR SOURCE TEMP. UNITS            
C      IOTYP  = 1 EXCITATION RATE COEFFICIENT (CM3 S-1) RETURNED          
C             = 2 DE-EXCITATION RATE COEFFICIENT (CM3 S-1) RETURNED       
C             = 3 UPSILON RETURNED                           
C      IFOUT  = 1 KELVIN FOR OUTPUT TEMPERATURE UNIT                    
C             = 2   EV   FOR OUTPUT TEMPERATURE UNIT                   
C             = 3 SCALED UNITS  (TE(K)/Z1**2)                  
C             = 4 REDUCED UNITS RETURNED (KTE/EIJ)                      
C      IXMAX  = NUMBER OF TEMP/RATE PAIRS ENTERED                   
C      ITMAX  = NUMBER OF OUTPUT TEMPERATURES ENTERED                   
C      EDAT(I)= INPUT TEMPS.         (SELECTED UNITS)                         
C      XDAT(I)= INPUT RATE COEFFTS.  (SELECTED UNITS)                         
C      TDAT(I)= OUTPUT TEMPS.        (SELECTED UNITS)                         
C      IIORD  = *** UNUSED ***                       
C      IIGPH  = 0  NO COMPARARIVE GRAPH TO BE PRODUCED                       
C             = 1     COMPARATIVE GRAPH TO BE PRODUCED                       
C      IIGPG  = 0  NO GAMMA  GRAPH TO BE PRODUCED                       
C             = 1     GAMMA  GRAPH TO BE PRODUCED                       
C      IIBTS  = 0  BAD POINT OPTION OFF                                 
C             = 1  BAD POINT OPTION ON                                  
C      IIFPT  = 1  SELECT ONE POINT OPTIMISING                          
C             = 2  SELECT TWO POINT OPTIMISING                          
C      IIXOP  = 0  OPTIMISING OFF                                       
C             = 1  OPTIMISING ON  (IF ALLOWED)                          
C      IIDIF  = *** UNUSED ***
C      XTIT1  = SPECIFIC TITLE FOR COMPARATIVE GRAPH                         
C      IGRD1  = 10 DO NOT PUT GRAPH IN A GRIDFILE                       
C             = 11 PUT GRAPH IN A GRIDFILE                              
C      IDEF1  = 11 USE DEFAULT SCALING FOR GRAPH                        
C             = 10 SCALING FOR GRAPH RETURNED                          
C      XL1    = LOWER X FOR COMPARATIVE GRAPH                                
C      XU1    = UPPER X FOR COMPARARIVE GRAPH                                
C      YL1    = LOWER Y FOR COMPARATIVE GRAPH                                
C      YU1    = UPPER Y FOR COMPARATIVE GRAPH                                
C      XTIT2  = SPECIFIC TITLE FOR GAMMA GRAPH                          
C      IGRD2  = 10 DO NOT PUT GRAPH IN A GRIDFILE                       
C             = 11 PUT GRAPH IN A GRIDFILE                              
C      IDEF2  = 11 USE DEFAULT SCALING FOR GRAPH                        
C             = 10 SCALING FOR GRAPH RETURNED                           
C      XL2    = LOWER X FOR GAMMA GRAPH                                 
C      XU2    = UPPER X FOR GAMMA GRAPH                                 
C      YL2    = LOWER Y FOR GAMMA GRAPH                                 
C      YU2    = UPPER Y FOR GAMMA GRAPH                                 
C                                                                       
C                                                                       
C  AUTHOR:  HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           TEL. 0141-553-4196
C
C  DATE:  15/11/96
C                                                                       
C
C  DATE:     15-11-95				VERSION 1.1
C  MODIFIED: HUGH P. SUMMERS         
C               - FIRST EDITION 
C
C  DATE:     25-11-95				VERSION 1.2
C  MODIFIED: WILLIAM OSBORN
C               - CHANGED PARAMS TO PARAMS102
C  DATE:     20-05-99				VERSION 1.3
C  MODIFIED: HUGH SUMMERS
C
C  DATE:     15-05-2007				VERSION 1.4
C  MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
C----------------------------------------------------------------------
       INTEGER I4UNIT
C-----------------------------------------------------------------------
       DIMENSION C1(ISTDIM,ISTDIM-1),C2(ISTDIM,ISTDIM-1),
     &           C3(ISTDIM,ISTDIM-1),C4(ISTDIM,ISTDIM-1)                
       DIMENSION APGOA(ISTDIM),XA(ISTDIM),YA(ISTDIM),
     &          toa(ISTDIM),YOA(ISTDIM)                
       DIMENSION ITITLE(10),XSA(ISTDIM),YSA(ISTDIM),
     &           XOS(ISTDIM),YOS(ISTDIM)             
       DIMENSION WG(12,3),XG(12,3),NQORD(3),GOA(ISTDIM)             
       DIMENSION APOUT(ISTDIM),APOMA(ISTDIM),DIFOMA(ISTDIM)
       DIMENSION EXCRA(ISTDIM),DEXCRA(ISTDIM),GBARFA(ISTDIM)
       DIMENSION EDAT(ISTDIM),XDAT(ISTDIM),TDAT(ISTDIM)
       CHARACTER ANS*3,TITLE*40,XTIT1*40,XTIT2*40                       
       CHARACTER*30 XANNA(2),XANNOT,YANNA(6),YANNOT                     
       CHARACTER GRID*40
       REAL*4 XP(400),YP(400),XPA(ISTDIM),YPA(ISTDIM)                           
       EXTERNAL FORM4                                                   
       COMMON /ESPL3/IEND1,IENDN,G1,GN,A1,B1,AN,BN,P1(3),Q1(3),         
     & PN(3),QN(3),A1RY(ISTDIM),B1RY(ISTDIM),ANRY(ISTDIM),BNRY(ISTDIM)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------        
       DO 1 I =1,ISTDIM
         TOA(I) = 0.0
         GOA(I) = 0.0
 1     CONTINUE
C              
       Z1=Z+1.0D0
       ZEFF=Z1                                                       
C       FXC2=0.0D0                                                       
C       FXC3=0.0D0                                                       
       S=0.0D0                                                          
       FIJ=0.0D0                                                        
C-----------------------------------------------------------------------
C  CONVERSION OF INPUT AND OUTPUT DATA FORMS TO STANDARD                
C-----------------------------------------------------------------------
       IF(IETYP.EQ.1)THEN                                               
           EI=EI/1.09737D5                                              
           EJ=EJ/1.09737D5                                              
       ENDIF                                                            
       EIJ=EJ-EI                                                        
       IF(IXTYP.EQ.1)THEN                                               
           IF(IATYP.EQ.2)THEN                                           
               ACOEFF=8.03232E9*EIJ*EIJ*WI*ACOEFF/WJ                    
           ELSEIF(IATYP.EQ.3)THEN                                       
               ACOEFF=2.67744E9*EIJ*EIJ*EIJ*ACOEFF/WJ                   
           ENDIF 
           S=3.73491E-10*WJ*ACOEFF/(EIJ*EIJ*EIJ)                        
           FIJ=3.333333D-1*EIJ*S/WI                                     
       ENDIF                                                            
C                                                     
       DO 40 ICT=1,IXMAX                                                 
        X=EDAT(ICT)                                                     
        Y=XDAT(ICT)                                                     
        IF(IFTYP.EQ.1)THEN                                              
            XA(ICT)=X                                         
        ELSEIF(IFTYP.EQ.2)THEN                                          
            XA(ICT)=1.16054D4*X                                               
        ELSEIF(IFTYP.EQ.3)THEN                                          
            XA(ICT)=X*Z1*Z1                                  
        ELSEIF(IFTYP.EQ.4)THEN                                          
            XA(ICT)=1.5789D5*X*EIJ                                              
        ENDIF
C                                                           
        ATE=1.57890D5/XA(ICT)
C                                    
        IF(IOTYP.EQ.1)THEN                                           
            YA(ICT)=4.60488D7*WI*DEXP(EIJ*ATE)*Y/DSQRT(ATE)        
        ELSEIF(IOTYP.EQ.2)THEN                               
            YA(ICT)=4.60488D7*WJ*Y/DSQRT(ATE)                    
        ELSEIF(IOTYP.EQ.3)THEN                                   
            YA(ICT)=Y                                           
        ELSEIF(IOTYP.EQ.4)THEN                                   
            YA(ICT)=Y/(Z1*Z1)                                           
        ENDIF                                               
   40  CONTINUE                                                          
       ICT=IXMAX                                                         
C                                                                       
       DO 55 ITOUT=1,ITMAX                                              
        TE=TDAT(ITOUT)                                                  
        IF(IFOUT.EQ.1)THEN                                              
            TOA(ITOUT)=TE                                               
        ELSEIF(IFOUT.EQ.2)THEN                                          
            TOA(ITOUT)=1.16054D4*TE                                     
        ELSEIF(IFOUT.EQ.3)THEN                                          
            TOA(ITOUT)=TE*Z1*Z1                                         
        ELSEIF(IFOUT.EQ.4)THEN                                          
            TOA(ITOUT)=1.5789D5*TE*EIJ                                  
        ENDIF                                                           
   55  CONTINUE                                                         
       ITOUT=ITMAX                                                      
       IQORD=IIORD                                                      
cx       IGPH=IIGPH                                                       
C-----------------------------------------------------------------------
C  CONVERT TO BEST VARIABLES FOR SPLINING DEPENDING UPON TRANSITION TYPE
C-----------------------------------------------------------------------
       IF(IXTYP.EQ.1)THEN                                               
           TE1=XA(1)                                              
           GAM1=YA(1)                            
           IFPTS=IIFPT                        
   72      IF(ICT.LE.1.OR.YA(1).GE.YA(ICT).OR.IFPTS.EQ.1)THEN           
               IFPTS=1                                                  
               CALL ESOLV1(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,IFAIL)     
               IF(IFAIL.LE.0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IXOPT=0                                              
               ENDIF                                                    
           ELSEIF(IFPTS.EQ.2)THEN                                       
               TE2=XA(ICT)                                              
               GAM2=YA(ICT)                                             
               CALL ESOLV2(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,TE2,GAM2,IFAIL
     &         )                                                        
               IF(IFAIL.LE.0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IFPTS=1                                              
                   GO TO 72                                             
               ENDIF                                                    
           ENDIF                                                        
           IF(IXOPT.GT.0)THEN                                           
               IDIFF=IIDIF                                              
           ELSE                                                         
               IDIFF=1                                                  
               FXC2=0.0D0                                               
               FXC3=1.333333D0                                          
           ENDIF                                                        
       ELSEIF(IXTYP.EQ.2.AND.IIXOP.EQ.1)THEN                            
           IFPTS=IIFPT                                                  
           TE1=XA(1)                                                   
           GAM1=YA(1)                                                
   74      IF(ICT.LE.1.OR.IFPTS.EQ.1)THEN
               IFPTS = 1                               
               CALL ESOLV1(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,IFAIL)
               IXOPT=IIXOP
           ELSEIF(IFPTS.EQ.2)THEN                                       
               TE2=XA(ICT)                                            
               GAM2=YA(ICT)                                            
               CALL ESOLV2(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,TE2,GAM2,IFAIL
     &         )                                                        
               IF(IFAIL.LE.0)THEN              
                   IXOPT=IIXOP                                          
               ELSE                                                    
                   IFPTS=1                                              
                   GO TO 74                                             
               ENDIF                                                    
           ENDIF                                                        
           IDIFF=0                                                      
       ELSEIF(IXTYP.EQ.3.AND.IIXOP.EQ.1)THEN                            
           IFPTS=IIFPT                                                  
           TE1=XA(1)                                                   
           GAM1=YA(1)                                                
   76      IF(ICT.LE.1.OR.YA(1).LE.YA(ICT).OR.IFPTS.EQ.1)THEN           
               IFPTS=1                                                  
               CALL ESOLV1(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,IFAIL)
               IXOPT=IIXOP                                              
           ELSEIF(IFPTS.EQ.2)THEN                                       
               TE2=XA(ICT)                                              
               GAM2=YA(ICT)                                             
               CALL ESOLV2(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,TE2,GAM2,IFAIL
     &                    )
               IF(IFAIL.LE.0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IFPTS=1                                              
                   GO TO 76                                            
               ENDIF                                                    
           ENDIF                                                        
           IDIFF=0                                                      
       ELSEIF(IIXOP.EQ.0)THEN                                           
           FCX2=0.0D0                                                   
           FXC3=0.0D0                                                   
           IXOPT=IIXOP                                                  
           IFPTS=0                                                      
           IDIFF=1                                                      
       ENDIF
C
       DO 75 K=1,ICT                                                   
        X=1.57890D5*EIJ/XA(K)                    
        XI=X/(X+1.0D0)    
        IF(IXTYP.EQ.1)THEN                                              
            APOMA(K)=FXC3*S*(DLOG(1.0D0+FXC2)+EEI((1.0D0+    
     &      FXC2)*1.5789D5*EIJ/XA(K)))                  
            IF(IXOPT.EQ.1.AND.IDIFF.EQ.0)THEN                           
                YSA(K)=YA(K)/APOMA(K)                                   
                XSA(K)=XI              
            ELSE                                                        
                YSA(K)=YA(K)-APOMA(K)                                   
                XSA(K)=XI              
            ENDIF                                                       
        ELSEIF(IXTYP.EQ.2.AND.IXOPT.EQ.1)THEN                
            APOMA(K)=FXC3+FXC2*X*EEI(X)            
            YSA(K)=YA(K)/APOMA(K)        
            XSA(K)=XI                       
        ELSEIF(IXTYP.EQ.3.AND.IXOPT.EQ.1)THEN              
           APOMA(K)=FXC3*1.5789D5*EIJ*EE2((1.0D0+FXC2)*1.5789D5*    
     &     EIJ/XA(K))/((1.0D0+FXC2)*XA(K))         
           YSA(K)=YA(K)/APOMA(K)                  
           XSA(K)=XI                            
        ELSEIF(IXTYP.EQ.4)THEN                            
           APOMA(K)=0.0D0                      
           YSA(K)=DLOG10(YA(K))                        
           XSA(K)=XI                     
        ELSEIF(IXOPT.EQ.0)THEN                
           APOMA(K)=0.0D0                     
           YSA(K)=YA(K)                       
           XSA(K)=XI                                       
        ENDIF                                             
        DIFOMA(K)=YA(K)-APOMA(K)
   75  CONTINUE                                           
       CALL ESORT(XSA,YSA,ICT)                                          
       IBPTS=IIBTS                                                      
       IF(IBPTS.LE.0)THEN                                               
           IF(IXOPT.EQ.1.OR.IXTYP.EQ.1)THEN                   
               IEND1=1                                    
               G1=0.0D0                              
               IENDN=1                                 
               GN=0.0D0                   
           ELSE                             
               IEND1=3                            
               IENDN=3                           
           ENDIF                                 
           IFORMS=IXTYP                                                 
           IENDS=1                                                      
           X1=XSA(1)                                                    
           DX1=0.01*X1                         
           CALL EGASYM(X1,DX1,FORM4,IFORMS,IENDS)                       
           IENDS=2                                                      
           XN=XSA(ICT)                                                  
           DXN=0.01*XN                                                  
           CALL EGASYM(XN,DXN,FORM4,IFORMS,IENDS)                    
           CALL EGSPC(XSA,ICT,C1,C2,C3,C4)                              
       ENDIF
C                                                            
        DO 85 K=1,ITOUT                                                 
         X=1.57890D5*EIJ/toa(K)         
         XI=X/(X+1.0D0)               
         XOS(K)=XI              
         IF(IBPTS.GT.0)THEN                                             
             CALL ELNFIT(XI,XSA,Y,YSA,ICT)                               
         ELSE                                                           
             CALL EFASYM(XI,XSA,ICT,YSA,Y,DY,
     &                   C1,C2,C3,C4,FORM5,IFORMS)  
         ENDIF                                                        
         YOS(K)=Y
         IF(IXTYP.EQ.1)THEN                              
            APGOA(K)=FXC3*S*(DLOG(1.0D0+FXC2)+EEI((1.0D0+FXC2)*    
     &      1.5789D5*EIJ/toa(K)))                                  
            IF(IDIFF.EQ.0)THEN                                    
                YOA(K)=APGOA(K)*Y                             
            ELSE                                               
                YOA(K)=APGOA(K)+Y                                  
            ENDIF                                              
        ELSEIF(IXTYP.EQ.2.AND.IXOPT.EQ.1)THEN                
            APGOA(K)=FXC3+FXC2*X*EEI(X)                         
            YOA(K)=APGOA(K)*Y                                         
        ELSEIF(IXTYP.EQ.3.AND.IXOPT.EQ.1)THEN                       
            APGOA(K)=FXC3*1.5789D5*EIJ*EE2((1.0D0+FXC2)*             
     &      1.5789D5*EIJ/toa(K))/((1.0D0+FXC2)*toa(K))           
            YOA(K)=APGOA(K)*Y                                     
        ELSEIF(IXTYP.EQ.4)THEN                                       
            APGOA(K)=0.0D0                                    
            YOA(K)=10.0D0**Y                                     
        ELSEIF(IXOPT.EQ.0)THEN                                   
            APGOA(K)=0.0D0                                        
            YOA(K)=Y                                        
        ENDIF
        GOA(K) = YOA(K)                               
        DEXCRA(K)=2.17161D-8*DSQRT(X/EIJ)*YOA(K)/WJ     
        EXCRA(K)=WJ*DEXP(-X)*DEXCRA(K)/WI           
        GBARFA(K)=6.8916D-2*EIJ*YOA(K)/WI
   85   CONTINUE   
        DO 87 K=1,ITOUT                                                 
         XP(K)=1.0D0-XOS(K)                              
         YP(K)=YOS(K)                                    
   87   CONTINUE                                                        
      RETURN
      END                                                               
