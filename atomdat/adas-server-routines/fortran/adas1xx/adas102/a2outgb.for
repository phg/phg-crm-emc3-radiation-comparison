CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas102/a2outgb.for,v 1.1 2004/07/06 10:01:07 whitefor Exp $ Date $Date: 2004/07/06 10:01:07 $
CX
       SUBROUTINE A2OUTGB( ICT   , DGEX  , DGEY  , B    , LPEND, RMS ,
     &                     ISTDIM, IXTYP , EIJ   , BCVAL , EDAT, XDAT,
     &                     TDAT  , GOAN  , INDIM , ITOUT , DSARCH,
     &                     IASEL , LARCH , ICHC
     &        )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A2OUTGB ******************
C
C  PURPOSE: TO PIPE DATA TO IDL FOR GRAPHING & READ ALTERATIONS
C
C  CALLING PROGRAM:
C            ADAS102.FOR
C
C  INPUT:
C            (I*4)  ICT    = NUMBER OF X-SECTIONS
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (I*4)  IXTYP  = 1  DIPOLE TRANSITION            
C                          = 2  NON-DIPOLE TRANSITION   
C                          = 3  SPIN CHANGE TRANSITION      
C                          = 4  OTHER     
C            (R*8)  EIJ    = TRANSITION ENERGY
C            (R*8)  BCVAL  = BURGESS SCALABLE PARAMETER
C            (I*4)  INDIM  = ARRAY DIMENSION
C            (I*4)  ISTDIM = MAXIMUM ARRAY DIMENSION
C            (I*4)  IASEL  = THE ORIGINAL ARCHIVING OPTION
C            (R*8)  TDAT   = TEMPERATURE SET
C            (R*8)  GOAN   = GAMMA (EFFECTIVE COLLISION STRENGTH)
C            (R*8)  B      = THE FIVE BURGESS KNOTS
C            (R*8)  RMS    = THE R.M.S OF THE BURGESS FIT
C  I/O:
C            (R*8)  EDAT   = USER EDITED ENERGY (PARAMETER X) OR
C                            ENERGY IF NO ALTERATIONS
C            (R*8)  XDAT   = USER EDITED OMEGA (COLLISION) OR
C                            OMEGA IF NO ALTERATIONS
C            (R*8)  DGEX   = THE REDUCED ENERGIES
C            (R*8)  DGEY   = THE REDUCED OMEGAS
C            (I*4)  ICHC   = FLAGS ALTERATION TO C VALUE
C
C  OUTPUT:
C            (C*80) DSARCH = THE ARCHIVE FILE NAME
C            (L)    LPEND  = .F. DONE
C                            .T. CANCEL/RE-PROCESSC
C            (L)    LARCH  = ARCHIVING SELECTION OPTIONC
C           
C
C  ROUTINES:
C           NAME       BRIEF DESCRIPTION
C           -----------------------------------------------------
C           AXETRD    - CALCULATES THE REDUCED ENERGY FOR GRAPHING
C           AXOURD    - CALCULATES THE REDUCED OMEGA FOR GRAPHING
C           AXETRDV - INVERTS THE REDUCTION PROCESS TO RECOVER 
C                      THE ORIGINAL ENERGIES, AFTER EDITING
C           AXOURDV - INVERTS THE REDUCTION PROCESS TO RECOVER 
C                      THE ORIGINAL OMEGAS, AFTER EDITING
C           XXSLEN   - REMOVES GAPS IN A STRING
C           XXFLSH   - CLEARS PIPE BUFFER
C
C  AUTHOR:  HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           TE. 0141-553-4196
C
C  DATE:     15/11/96				VERSION 1.1
C  MODIFIED: HUGH P. SUMMERS
C		- FIRST RELEASE
C-----------------------------------------------------------------------
       INTEGER ICT   , ISTDIM , PIPEIN , PIPEOU , I     , LOGIC
       INTEGER IXTYP , I4UNIT , ITOUT  , IASEL  , IARCH , INDIM
       INTEGER IFIRST, ILAST  , ICHC
C-----------------------------------------------------------------------
       CHARACTER CSTRING*80 , DSARCH*80
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
       REAL*8 BCVAL , EIJ , AXETRD , AXOURD , AXETRDV , AXOURDV, RMS   
C-----------------------------------------------------------------------
       REAL*8 DGEX(ISTDIM), DGEY(ISTDIM), B(5) , EDAT(ISTDIM) 
       REAL*8 XDAT(ISTDIM), TDAT(ISTDIM), GOAN(INDIM)
C-----------------------------------------------------------------------
       LOGICAL LPEND , LARCH
C-----------------------------------------------------------------------
C CALCULATE REDUCED QUANTITIES FOR WRITING TO IDL
C-----------------------------------------------------------------------
C
       DO 5 I = 1, ICT 
           DGEX(I) = AXETRD( IXTYP , EIJ   , EDAT(I), BCVAL )
           DGEY(I) = AXOURD( IXTYP , EIJ   , EDAT(I), XDAT(I), BCVAL ) 
 5     CONTINUE 
       WRITE(PIPEOU,1000) ICT
       WRITE(PIPEOU,1000) ITOUT
       WRITE(PIPEOU,1002) RMS
       DO 10 I = 1, ICT
           WRITE(PIPEOU,1001) DGEX(I), DGEY(I)
 10    CONTINUE 
       DO 20 I = 1, 5
           WRITE(PIPEOU,1002) B(I)
 20    CONTINUE
       DO 25 I = 1, ITOUT
           WRITE(PIPEOU,1003) TDAT(I), GOAN(I)
 25    CONTINUE
       CALL XXFLSH(PIPEOU)     
C
 26    READ(PIPEIN,*)ICHC
         IF(ICHC.EQ.1)THEN
           READ(PIPEIN,*)BCVAL
           GOTO 999
         ELSE IF(ICHC.EQ.0)THEN
           GOTO 26
         ENDIF
C
       READ(PIPEIN,*) ICT
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.0) THEN
           LPEND = .FALSE.
       ELSE
           LPEND = .TRUE.
           IF (LOGIC.EQ.2) THEN
               DO 30 I = 1, ICT
                   READ(PIPEIN,1001) DGEX(I), DGEY(I)
 30            CONTINUE
           ENDIF
       ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE NEW ENERGIES & X-SECTIONS IF ANY ALTERATIONS
C-----------------------------------------------------------------------
C
       IF (LOGIC.EQ.2) THEN
           DO 35 I = 1, ICT
               EDAT(I) = AXETRDV( IXTYP , EIJ   , DGEX(I)  , BCVAL )
               XDAT(I) = AXOURDV( IXTYP , EIJ   , EDAT(I)  , DGEY(I), 
     &                             BCVAL
     &                            )
 35        CONTINUE
       ENDIF
       IF (LPEND) GOTO 999
C
C  RESET LPEND
C
       LPEND = .FALSE.
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.1) THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
           READ(PIPEIN,*) IARCH
       ENDIF
       IF (IARCH.EQ.1) THEN
           LARCH = .TRUE.
       ELSE
           LARCH = .FALSE.
       ENDIF
       IF (LPEND) GO TO 999
C
C-----------------------------------------------------------------------
C IF ARCHIVING IS SELECTED AFTER INITIALLY SPECIFYING NO ARCHIVE
C THEN THE DEFAULT IS TO CREATE A FILE CALLED ARCHIVE.DAT IN THE 
C ARCH101 DIRECTORY
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
           IF (IASEL.EQ.0) THEN
               READ(PIPEIN,1004) CSTRING
           ENDIF
       ENDIF
       CALL XXSLEN( CSTRING, IFIRST, ILAST)
       READ(CSTRING(IFIRST:ILAST),1004) DSARCH
C
C------------------------------------------------------------------------
C
 1000  FORMAT(1X,I3)
 1001  FORMAT(1X,E12.4,1X,E12.4)
 1002  FORMAT(1X,E12.4)
 1003  FORMAT(1X,E12.4,1X,E12.4)
 1004  FORMAT(1A80)
 999   RETURN
       END
