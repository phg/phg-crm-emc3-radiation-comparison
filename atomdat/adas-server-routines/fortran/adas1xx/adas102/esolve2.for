       SUBROUTINE ESOLV2(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,TE2,GAM2,IFAIL) 
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C  PURPOSE: TO FIT TWO POINT APPROXIMATE FORM                         
C
C  VERSION: 1.2
C  DATE: 26-03-08
C  MODIFIED: Allan Whiteford
C               - Added purpose to documentation
C               - Removed 0s from columns 72-80.
C
C-----------------------------------------------------------------------
       P1(X)=GAM1/GAM2-(DLOG(1.0D0+X)+EEI((1.0D0+X)*1.5789E5*EIJ/TE1))/ 
     & (DLOG(1.0D0+X)+EEI((1.0D0+X)*1.5789E5*EIJ/TE2))                  
       P3(X)=GAM1/GAM2-EE2((1.0D0+X)*1.5789E5*EIJ/TE1)*TE2/             
     & (EE2((1.0D0+X)*1.5789E5*EIJ/TE2)*TE1)                            
C                                                                       
       IF(IXTYP.EQ.2)THEN                                               
           ATE1=1.57890D5*EIJ/TE1                                       
           ATE2=1.57890D5*EIJ/TE2                                       
           FXC2=(GAM1-GAM2)/(ATE1*EEI(ATE1)-ATE2*EEI(ATE2))             
           FXC3=GAM1-FXC2*ATE1*EEI(ATE1)                                
           IF(FXC2+FXC3.GT.0.0D0.AND.FXC3.GT.0.0D0)THEN                 
               IFAIL=0                                                  
           ELSE                                                         
               IFAIL=1                                                  
           ENDIF                                                        
       ELSE                                                             
           ICOUNT=1                                                     
           IF(IXTYP.EQ.1)THEN                                           
               A=0.0D0                                                  
               B=1.0D0                                                  
               VA=P1(A)                                                 
           ELSEIF(IXTYP.EQ.3)THEN                                       
               A=-0.95D0                                                
               B=0.25D0                                                 
               VA=P3(A)                                                 
           ENDIF                                                        
    4      B=B+B                                                        
           ICOUNT=ICOUNT+1                                              
           IF(ICOUNT.GT.12)THEN                                         
               WRITE(6,101)                                             
               IFAIL=1                                                  
               RETURN                                                   
           ENDIF                                                        
           IF(IXTYP.EQ.1)THEN                                           
               VB=P1(B)                                                 
           ELSEIF(IXTYP.EQ.3)THEN                                       
               VB=P3(B)                                                 
           ENDIF                                                        
           IF(VB*VA.GT.0.0D0)THEN                                       
               A=B                                                      
               VA=VB                                                    
               GO TO 4                                                  
           ENDIF                                                        
           IF(VA.GT.VB)THEN                                             
               V=VA                                                     
               VA=VB                                                    
               VB=V                                                     
               X=A                                                      
               A=B                                                      
               B=X                                                      
           ENDIF                                                        
           X=(A*VB-B*VA)/(VB-VA)                                        
    9      X1=X                                                         
           IF(IXTYP.EQ.1)THEN                                           
               V=P1(X)                                                  
           ELSEIF(IXTYP.EQ.3)THEN                                       
               V=P3(X)                                                  
           ENDIF                                                        
           IF(V.GT.0.0D0)THEN                                           
               B=X                                                      
               VB=V                                                     
           ELSEIF(V.LT.0.0D0)THEN                                       
               A=X                                                      
               VA=V                                                     
           ELSE                                                         
               GO TO 16                                                 
           ENDIF                                                        
           X=0.5D0*(A+B)                                                
           IF(IXTYP.EQ.1)THEN                                           
               V=P1(X)                                                  
           ELSEIF(IXTYP.EQ.3)THEN                                       
               V=P3(X)                                                  
           ENDIF                                                        
           IF(V.GT.0.0D0)THEN                                           
               B=X                                                      
               VB=V                                                     
           ELSEIF(V.LT.0.0D0)THEN                                       
               A=X                                                      
               VA=V                                                     
           ELSE                                                         
                GO TO 16                                                
           ENDIF                                                        
           X=(A*VB-B*VA)/(VB-VA)                                        
           IF(DABS(X1-X)/X.GT.0.01D0)GO TO 9                            
   16      CONTINUE                                                     
           IF(X.GT.-1.0D0)THEN                                          
               FXC2=X                                                   
               IFAIL=0                                                  
               IF(IXTYP.EQ.1)THEN                                       
                   FXC3=GAM1/(S*(DLOG(1.0D0+X)+                         
     &             EEI((1.0D0+X)*1.5789E5*EIJ/TE1)))                    
               ELSEIF(IXTYP.EQ.3)THEN                                   
                   FXC3=GAM1*(1.0D0+X)*TE1/(1.5789E5*EIJ                
     &             *EE2((1.0D0+X)*1.5789E5*EIJ/TE1))                    
               ENDIF                                                    
           ELSE                                                         
               FXC2=0.0D0                                               
               FXC3=0.0D0                                               
               IFAIL=1                                                  
           ENDIF                                                        
       ENDIF                                                            
       RETURN                                                           
  101  FORMAT(1H0,'** WARNING: FAILURE TO FIX 2 POINT APPROX.')         
      END                                                               
