       SUBROUTINE ESOLV1(FXC2,FXC3,S,EIJ,IXTYP,TE1,GAM1,IFAIL)          
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C  PURPOSE: TO FIT ONE POINT APPROXIMATE FORM                         
C
C  VERSION: 1.2
C  DATE: 26-03-08
C  MODIFIED: Allan Whiteford
C               - Added purpose to documentation
C               - Removed 0s from columns 72-80.
C
C-----------------------------------------------------------------------
       P1(X)=GAM1-1.333333D0*S*(DLOG(1.0D0+X)+EEI((1.0D0+X)*            
     & 1.5789D5*EIJ/TE1))                                               
C
       IF(IXTYP.EQ.2)THEN
           FXC2=0.0D0                                                   
           FXC3=GAM1                                                    
           IFAIL=0                                                      
       ELSEIF(IXTYP.EQ.3)THEN
           FXC2=100.0                                                   
           FXC3=GAM1/(1.5789D5*EIJ*EE2((1.0D0+FXC2)*1.5789D5*EIJ/TE1)   
     &     /((1.0D0+FXC2)*TE1))                                         
           IFAIL=0
       ELSEIF(IXTYP.EQ.1)THEN
           ICOUNT=1                                                     
           A=0.0D0                                                      
           B=1.0D0                                                      
           FXC3=1.333333D0                                              
           VA=P1(A)                                                     
    4      B=B+B                                                        
           ICOUNT=ICOUNT+1                                              
           IF(ICOUNT.GT.12)THEN                                         
               WRITE(6,101)                                             
               FXC2=0.0D0
               IFAIL=1                                                  
               RETURN
           ENDIF
           VB=P1(B)                                                     
           IF(VB*VA.GT.0.0D0)THEN                                       
               A=B                                                      
               VA=VB                                                    
               GO TO 4
           ENDIF                                                        
           IF(VA.GT.VB)THEN                                             
               V=VA                                                     
               VA=VB                                                    
               VB=V                                                     
               X=A                                                      
               A=B                                                      
               B=X                                                      
           ENDIF
           X=(A*VB-B*VA)/(VB-VA)                                        
    9      X1=X                                                         
           V=P1(X)                                                      
           IF(V.GT.0.0D0)THEN
               B=X                                                      
               VB=V                                                     
           ELSEIF(V.LT.0.0D0)THEN                                       
               A=X                                                      
               VA=V                                                     
           ELSE
               GO TO 16
           ENDIF
           X=0.5D0*(A+B)                                                
           V=P1(X)                                                      
           IF(V.GT.0.0D0)THEN
               B=X                                                      
               VB=V                                                     
           ELSEIF(V.LT.0.0D0)THEN                                       
               A=X                                                      
               VA=V                                                     
           ELSE
               GO TO 16
           ENDIF
           X=(A*VB-B*VA)/(VB-VA)                                        
           IF(DABS(X1-X)/X.GT.0.01D0)GO TO 9                            
   16      CONTINUE
           IF(X.GT.0.0D0)THEN                                           
               FXC2=X                                                   
               IFAIL=0
           ELSE
               FXC2=0.0D0
               IFAIL=1
           ENDIF
       ENDIF
       RETURN                                                           
  101  FORMAT(1H0,'** WARNING :  FAILURE TO FIX 1 POINT APPROX.')       
      END                                                               
