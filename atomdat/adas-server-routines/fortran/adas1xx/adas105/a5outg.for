CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas105/a5outg.for,v 1.2 2004/07/06 10:02:25 whitefor Exp $ Date $Date: 2004/07/06 10:02:25 $
CX
       SUBROUTINE A5OUTG(  XA    , YA    , APA    , XP    , YP    , TOA,
     &     YOA  , YOAP,    ICT   , ITOUT , INDIM  , NITHR , NRTHR ,
     &     LPEND , LARCH , DSARCH, IASEL , ASCL   ,
     &     EDAT  , XDAT  , ISTDIM, CIA   , CRA    , Z ,
     &     NIGRP , NRGRP , EIONA , IZETAA, NSHELA ,
     &     NRESOA, Z1    , ENERA , WGHTA , IFTYP  , IOTYP , EMIN
     &     )
       IMPLICIT NONE
C-------------------------------------------------------------------------
C
C  ******************* FORTRAN 77 SUBROUTINE A5OUTG **********************
C
C
C  PURPOSE: TO SEND OUTPUT FROM SPFMAN11 ADAS ANALYSIS SUBROUTINE TO IDL
C           GRAPH EDITOR VIA THE UNIX PIPE. IF THERE IS ANY GRAPH EDITING
C           THEN THE USERS' ALTERATIONS ARE READ BACK AND THE DATA IS
C           RECALCULATED.
C
C  CALLING PROGRAM:
C            ADAS105.FOR
C
C  INPUT:
C            (R*8)  XA     = ENERGY (PARAMETER X) - ADAS OPTION
C            (R*8)  YA     = OMEGA (COLLISION STRENGTH) -ADAS OPTION
C            (R*8)  APA    = APPROXIMATE OMEGA
C            (R*8)  XP     = ENERGY (PARAMETER X) - ADAS OPTION
C            (R*8)  YP     = OMEGA (COLLISION STRENGTH) -ADAS OPTION
C            (R*8)  TOA    = TEMPERATURE SET
C            (R*8)  YOA    = RATE COEFFICIENT
C            (R*8)  YOAP   = APPROX. RATE COEFFICIENT
C            (I*4)  ICT    = NUMBER OF X-SECTIONS
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (I*4)  INDIM  = ARRAY DIMENSIONS
C            (I*4)  NITHR  = NUMBER OF RESONANCES
C            (I*4)  NRTHR  = NUMBER OF RESONANCES
C            (I*4)  IASEL  = THE ORIGINAL ARCHIVING OPTION (SEE A5SPF0)
C            (R*8)  ASCL   = GRAPHIC SCALING PARAMETER
C            (R*8)  CIA()  = SCALING PARAMS
C            (R*8)  CRA()  = SCALING PARAMS
C            (R*8)  Z      = INITIAL ION CHARGE
C            (I*4)  NIGRP  = NUMBER OF SHELL GROUPS
C            (I*4)  NRGRP  = NUMBER OF RESONANCE GROUPS
C            (I*4)  NSHELA = NUMBER OF ENTRIES FOR EACH SHELL GROUP
C            (I*4)  NRESOA = NUMBER OF ENTRIES FOR EACH RESONANCE GROUP
C            (R*8)  EIONA  =
C            (R*8)  IZETAA =
C            (R*8)  ENERA  =
C            (R*8)  WGHTA  =
C            (R*8)  Z1     =
C            (R*8)  EMIN   =
C  OUTPUT:
C            (R*8)  EDAT   = USER EDITED ENERGY (PARAMETER X) 
C            (R*8)  XDAT   = USER EDITED OMEGA (COLLISION STRENGTH) 
C            (L)    LARCH  = ARCHIVING SELECTION OPTION
C            (L)    LPEND  = .F. DONE
C                            .T. CANCEL/RE-PROCESSC
C            (C*80) DSARCH = THE ARCHIVE FILE NAME
C            (I*4)  IFTYP  = ENERGY UNITS - SEE ADAS105
C            (I*4)  IOTYP  = X-SECTION UNITS - SEE ADAS105
C
C  ROUTINES:
C          XXSLEN - REMOVES THE SPACES IN A STRING
C          XXFLSH - CLEARS THE PIPE BUFFER
C
C  DATE:   23/09/96				VERSION 1.1
C  AUTHOR: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C 		- FIRST WRITTEN
C
C  DATE:   14/02/97				VERSION 1.2
C  MODIFIED: RICHARD MARTIN
C	   - ADDED STATEMENTS '136 CONTINUE' AND '146 CONTINUE'
C	     IMMEDIATELY AFTER '135 CONTINUE' AND '145 CONTINUE'
C	   - CHANGED 'GOTO 135' TO 'GOTO 136' ETC..
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       INTEGER ICT  , ITOUT , INDIM , PIPEIN , PIPEOU , I , J
       INTEGER LOGIC , IARCH  , IASEL , IFIRST , ILAST  , ISTDIM 
       INTEGER NITHR , NRTHR , NIGRP , NRGRP, IZETAA(6,2), NSHELA(2)
       INTEGER NRESOA(2)
       INTEGER IIGRP, ISHEL, IRGRP, IRESO, IOTYP, IFTYP
C-----------------------------------------------------------------------
       LOGICAL LPEND , LARCH
C-----------------------------------------------------------------------
       CHARACTER DSARCH*80 , CSTRING*80
C-----------------------------------------------------------------------
       REAL*8 XA(INDIM) , YA(INDIM) , APA(INDIM)
       REAL*8 TOA(INDIM), YOA(INDIM), YOAP(INDIM), EDAT(ISTDIM)
       REAL*8 XDAT(ISTDIM)
       REAL*4 XP(12), YP(12)
       REAL*8 ASCL, CIA(2), CRA(2), Z, EIONA(6,2), ENERA(6,2),WGHTA(6,2)
       REAL*8 QBCHID
C-----------------------------------------------------------------------
       REAL*8 ZETA, XI, E, OMBCH, SUM, OMRES, EIJ, EMIN, Z1
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------

       WRITE(PIPEOU,*) ASCL
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*) ICT
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*) ITOUT
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*) NITHR
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*) NRTHR
       CALL XXFLSH(PIPEOU)
       DO 10 I = 1, ICT
           WRITE(PIPEOU,*) XA(I)
           CALL XXFLSH(PIPEOU)
           WRITE(PIPEOU,*) YA(I)
           CALL XXFLSH(PIPEOU)
           WRITE(PIPEOU,*) APA(I)
           CALL XXFLSH(PIPEOU)
 10    CONTINUE
       DO 20 I = 1, ITOUT
           WRITE(PIPEOU,*) TOA(I)
           CALL XXFLSH(PIPEOU)
           WRITE(PIPEOU,*) YOA(I)
           CALL XXFLSH(PIPEOU)
           WRITE(PIPEOU,*) YOAP(I)
           CALL XXFLSH(PIPEOU)
 20    CONTINUE
       DO 30 I = 1, NITHR+NRTHR
          WRITE(PIPEOU,*) XP(I)
          CALL XXFLSH(PIPEOU)
 30    CONTINUE
       WRITE(PIPEOU,*)NIGRP
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)NRGRP
       CALL XXFLSH(PIPEOU)

C WRITE OUT INFO FOR MESSAGES
       WRITE(PIPEOU,*)EMIN
       CALL XXFLSH(PIPEOU)
       DO 50 I=1,NIGRP
          WRITE(PIPEOU,*)CIA(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*)NSHELA(I)
          CALL XXFLSH(PIPEOU)
          DO 55 J=1,NSHELA(I)
             WRITE(PIPEOU,*)EIONA(J,I)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)IZETAA(J,I)
             CALL XXFLSH(PIPEOU)
 55       CONTINUE
 50    CONTINUE
       DO 60 I=1,NRGRP
          WRITE(PIPEOU,*)CRA(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*)NRESOA(I)
          CALL XXFLSH(PIPEOU)
          DO 65 J=1,NRESOA(I)
             WRITE(PIPEOU,*)ENERA(J,I)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)WGHTA(J,I)
             CALL XXFLSH(PIPEOU)
 65       CONTINUE
 60    CONTINUE
       WRITE(PIPEOU,*)Z1
       CALL XXFLSH(PIPEOU)

C
C-------------------------------------------------------------------------
C READ ALTERATIONS FROM IDL
C-------------------------------------------------------------------------
C
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.0) THEN
           LPEND = .FALSE.
       ELSE
           LPEND = .TRUE. 
           IF (LOGIC.EQ.2) THEN
              READ(PIPEIN,*) ICT
              DO 40 I = 1, ICT
                  APA(I)=0.0
                  READ(PIPEIN,*) EDAT(I)
                  READ(PIPEIN,*) YA(I)
C-------------------------------------------------------------------------
C     CALCULATE XDAT
C-------------------------------------------------------------------------
                  IF(NIGRP.LE.0)GO TO 136
                  DO 135 IIGRP=1,NIGRP
                     SUM=0.0D0
                     DO 132 ISHEL=1,NSHELA(IIGRP)
                        ZETA=IZETAA(ISHEL,IIGRP)
                        XI=EIONA(ISHEL,IIGRP)
                        E=EDAT(I)*EMIN
                        OMBCH=QBCHID(Z,XI,ZETA,E)/(1.0D0-EMIN/E)
                        SUM=SUM+OMBCH
 132                 CONTINUE
                     APA(I)=APA(I)+CIA(IIGRP)*SUM
 135              CONTINUE
 136              CONTINUE
                  IF(NRGRP.LE.0)GO TO 146
                  DO 145 IRGRP=1,NRGRP
                     SUM=0.0D0
                     DO 142 IRESO=1,NRESOA(IRGRP)
                        OMRES=0.0D0
                        E=EDAT(I)*EMIN
                        EIJ=ENERA(IRESO,IRGRP)
                        IF(E.LT.EIJ)GO TO 142
                 OMRES=1.45D0*WGHTA(IRESO,IRGRP)/(EIJ*E*(1.0D0-EMIN/E))
                        SUM=SUM+OMRES
 142                 CONTINUE
                     APA(I)=APA(I)+CRA(IRGRP)*SUM
 145              CONTINUE
 146              CONTINUE
                  XDAT(I)=YA(I)*APA(I)
                  XA(I)=EDAT(I)
                  YA(I)=XDAT(I)
 40            CONTINUE
               IFTYP = 3
               IOTYP = 3
            ENDIF      
         ENDIF
         
         IF (LPEND) GO TO 999
C
C RESET LPEND
C
       LPEND = .FALSE.
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.1) THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
           READ(PIPEIN,*) IARCH
       ENDIF
       IF (IARCH.EQ.1) THEN
           LARCH = .TRUE.
       ELSE
           LARCH = .FALSE.
       ENDIF
       IF (LPEND) GO TO 999
C
C-----------------------------------------------------------------------
C IF ARCHIVING IS SELECTED AFTER INITIALLY SPECIFYING NO ARCHIVE
C THEN THE DEFAULT IS TO CREATE A FILE CALLED ARCHIVE.DAT IN THE 
C ARCH105 DIRECTORY
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
           IF (IASEL.EQ.0) THEN
               READ(PIPEIN,'(A)') CSTRING
           ENDIF
       ENDIF
       CALL XXSLEN( CSTRING , IFIRST , ILAST )
       READ(CSTRING(IFIRST:ILAST),1004) DSARCH
C
C-------------------------------------------------------------------------
C
 1004  FORMAT(1A80)
 1005  FORMAT('CIA = ',2E15.5)
 1006  FORMAT('CRA = ',2E15.5)
 1007  FORMAT('A = ',1E15.5)
 999   RETURN
       END
