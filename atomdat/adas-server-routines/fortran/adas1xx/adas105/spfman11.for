CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas105/spfman11.for,v 1.6 2007/05/15 11:17:52 allan Exp $ Date $Date: 2007/05/15 11:17:52 $
CX
      SUBROUTINE SPFMAN11(Z0,Z,Z1,IIGRP0,IRGRP0,TITLE,                
     &     ACIA,BCIA,ACRA,BCRA,                                 
     &     ISHE,IRES,INS1,ILS1,ES1,IZS1,INS2,
     &     ILS2,ES2,IZS2,ER1,ER2,WR1,WR2,
     &     IIFTYP,IIOTYP,IIFOUT,IXMAX,ITMAX,XDAT,EDAT,TDAT,     
     &     IOP,ASCL,
     &     XA, YA, APA, XP, YP, TOA, YOA, YOAP,
     &     NITHR, NRTHR , CIA , CRA, NA , LA ,
     &     EIONA , IZETAA , ENERA , WGHTA, EMIN)
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C                                                                       
C  ************  FORTRAN 77 PROGRAM:  SPFMAN11 *************************
C                                                                       
C                                                                       
C  PURPOSE: FIT IONISATION CROSS-SECTION DATA WITH SIMPLE APPROXIMATE 
C  FORMS AND TO CALCULATE IONISATION RATE COEFFICIENTS                  
C
C  THE APPROXIMATE FORM ADOPTED IS A SUM OF TERMS ARISING FROM EACH     
C  SHELL OF THE BCHID TYPE + A SUM OF TERMS OF EXCITATION CROSS-SECTION 
C  FORM. THE LATTER SEEK TO REPRESENT SHARP ABOVE THRESHOLD AUTOIONISING
C  FEATURES. SCALING FACTORS ARE ASSIGNED TO AT MOST TWO SHELL GROUPS   
C  (A GROUP FOR EXAMPLE BEING L-SHELLS OF THE SAME N) AND TWO RESONANCE 
C  GROUPS. THE SCALING FACTORS ARE OBTAINED BY LEAST SQUARE FITTING TO  
C  THE OBSERVED DATA.                                                   
C
C  DATA:
C         THIS PROGRAM IS NOT YET PROPERLY ANNOTATED
C
C  INPUT:
C     (R*8)  Z0     = NUCLEAR CHARGE OF ION
C     (R*8)  Z      = INITIAL ION CHARGE
C     (R*8)  Z1     = FINAL ION CHARGE
C     (I*4)  IIGRP0 = NO. OF SHELL GROUPS
C     (I*4)  IRGRP0 = NO. OF RESONANCE GROUPS
C     (C*40) TITLE  = TITLE FOR THIS RUN
C     (R*8)  ACIA   = SCALING PARAMETER FOR SHELL GROUP 1
C     (R*8)  BCIA   = SCALING PARAMETER FOR SHELL GROUP 2
C     (R*8)  ACRA   = SCALING PARAMETER FOR RESONANCE GROUP 1
C     (R*8)  BCRA   = SCALING PARAMETER FOR RESONANCE GROUP 2
C     (I*4)  ISHE() = NO. OF ENTRIES FOR EACH SHELL GROUP (1-6)
C     (I*4)  IRES() = NO. OF ENTRIES FOR EACH RESONANCE GROUP (1-6)
C     (I*4)  INS1() = SHELL GROUP 1 DATA: N
C     (I*4)  ILS1() = SHELL GROUP 1 DATA: L
C     (R*8)  ES1()  = SHELL GROUP 1 DATA: EION(RYD)
C     (I*4)  IZS1() = SHELL GROUP 1 DATA: IZETA
C     (I*4)  INS2() = SHELL GROUP 2 DATA: N
C     (I*4)  ILS2() = SHELL GROUP 2 DATA: L
C     (R*8)  ES2()  = SHELL GROUP 2 DATA: EION(RYD)
C     (I*4)  IZS2() = SHELL GROUP 2 DATA: IZETA
C     (R*8)  ER1()  = RESONANCE GROUP 1 DATA: ENERGY(RYD)
C     (R*8)  ER2()  = RESONANCE GROUP 2 DATA: ENERGY(RYD)
C     (R*8)  WR1()  = RESONANCE GROUP 1 DATA: WEIGHT
C     (R*8)  WR2()  = RESONANCE GROUP 2 DATA: WEIGHT
C     (I*4)  IIFTYP = ENERGY PARAMETER FORM
C                     1 : INCIDENT ENERGY (RYD)
C                     2 : INCIDENT ENERGY (EV)
C                     3 : X THRESHOLD PARAMETER
C     (I*4)  IIOTYP = CROSS-SECTIONAL FORM
C                     1 : X-SECT. (PI*A0**2)
C                     2 : X-SECT. (CM**2)
C                     3 : COLLISION STRENGTH (OMEGA)
C                     4 : SCALED COLLISION STRENGTH ((Z**2)*OMEGA)
C     (I*4)  IIFOUT = OUTPUT TEMPERATURE FORM
C                     1 : KELVIN
C                     2 : EV
C                     3 : SCALED UNITS (TE(K)/(Z1**2))
C     (I*4)  IXMAX  = NUMBER OF X-SECT./ENERGY PAIRS
C     (I*4)  ITMAX  = NUMBER OF TEMPS.
C     (R*8)  XDAT() = X-SECTION DATA
C     (R*8)  EDAT() = ENERGY DATA
C     (R*8)  TDAT() = TEMPERATURE DATA
C     (I*4)  IOP    = USE DEFAULT SCALING PARAMS? (1 = YES, 0 = NO)
C     (R*8)  ASCL  = GRAPHIC SCALING PARAMETER
C
C  OUTPUT:
C     (R*8)  XA()   = SCALED ENERGY
C     (R*8)  YA()   = OMEGA
C     (R*8)  APA()  = APPROXIMATE OMEGA
C     (R*4)  XP()   = SCALED ENERGY RESONANCE POINTS
C     (R*4)  YP()   = OMEGA OF RESONANCE POINTS
C     (R*8)  TOA()  = TEMP (KELVIN)
C     (R*8)  YOA()  = S, MAXWELL AVERAGED IONISATION RATE COEFF.(CM^3 S^-1)
C     (R*8)  YOAP() = SEM, APPROXIMATE RATE COEFF.
C     (I*4)  NITHR  = NUMBER OF RESONANCES
C     (I*4)  NRTHR  = NUMBER OF EXTRA (?) RESONANCES
C     (R*8)  CIA()  = OUTPUT SCALING PARAMS
C     (R*8)  CRA()  = OUTPUT SCALING PARAMS
C     (I*4)  NA(,)  = SHELL GROUP DATA : N
C     (I*4)  LA(,)  = SHELL GROUP DATA : L
C     (R*8)  EIONA(,) = SHELL GROUP DATA : EION(RYD)
C     (I*4)  IZETAA(,)= SHELL GROUP DATA : IZETA
C     (R*8)  ENERA(,) = RESONANCE GROUP DATA : ENERGY(RYD)
C     (R*8)  WGHTA(,) = RESONANCE GROUP DATA : WEIGHT
C
C  AUTHOR:
C
C  ************ H.P.SUMMERS, JET         1 JULY 1987  ***************** 
C  ************ J.SPENCE, JET              JULY 1987  ***************** 
C  *** COR                              30 OCT  1989                ***
C
C  UPDATE :  30/4/92 W DICKSON  -  ADD PARAMETER NDTEM AND SET TO 30
C                                  ALTER OUTPUT TEMP PANEL ACCORDINGLY
C
C  UPDATE :   7/5/92 W DICKSON  -  ADJUST OUTPUT TO INCLUDE TEMPERATURE
C                                  IN EV, AND FORMAT AS SZD FILE
C
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 23-08-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C               - MADE INTO A SUBROUTINE. COMMENTED-OUT PANEL INPUT
C                 AND GRAPHICAL ROUTINES.
C
C VERSION: 1.2                          DATE: 03-10-96
C MODIFIED: WILLIAM OSBORN
C               - ALLOWED ISTOP = 5 FROM XXSIM TO GIVE NO ERROR
C
C VERSION: 1.3                          DATE: 08-10-96
C MODIFIED: WILLIAM OSBORN
C               - LET YAT BE YA WEIGHTED BY X1 TO AGREE WITH NAG VERSION,
C                 IT WAS UNWEIGHTED
C
C VERSION: 1.4                          DATE: 08-10-96
C MODIFIED: WILLIAM OSBORN
C               - ADDED NA AND LA TO PARAMETERS
C
C VERSION 1.5				DATE: 14/02/97
C MODIFIED: RICHARD MARTIN
C	   	- ADDED STATEMENTS '136 CONTINUE' AND '146 CONTINUE'
C	     	  IMMEDIATELY AFTER '135 CONTINUE' AND '145 CONTINUE'
C	   	- CHANGED 'GOTO 135' TO 'GOTO 136' ETC..
C
C VERSION: 1.6				DATE: 15/05/07
C MODIFIED:  Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       PARAMETER ( NDTEM = 40)
C
       DIMENSION APA(40),XA(40),YA(40),YAT(40)
       DIMENSION NSHELA(2),NA(6,2),LA(6,2),EIONA(6,2),IZETAA(6,2)       
       DIMENSION NRESOA(2),ENERA(6,2),WGHTA(6,2),CIA(2),CRA(2)          
       DIMENSION XCOR(40,5)
       DIMENSION TOA(NDTEM),YOA(NDTEM)
       DIMENSION XSTORE(40,5)
       DIMENSION PA(40),QA(40),IIPT(6,2),IRPT(6,2),YOAP(NDTEM)          
       DIMENSION CIADEF(2),CRADEF(2)
C                                                                       
       DIMENSION ISHE(2), IRES(2)                                       
       DIMENSION INS1(6), ILS1(6), ES1(6), IZS1(6)                      
       DIMENSION INS2(6), ILS2(6), ES2(6), IZS2(6)                      
       DIMENSION ER1(6), WR1(6)                                         
       DIMENSION ER2(6), WR2(6)                                         
       DIMENSION XDAT(40), EDAT(40), TDAT(NDTEM)                        
C                                                                       
       REAL*4 XP(12),YP(12),XPA(40),YPA(40),CIA4(2),CRA4(2)             
       REAL*4 EIONA4(6,2),ENERA4(6,2),WGHTA4(6,2)                       
C                                                                       
       CHARACTER TITLE*40                                        

C VARIABLES USED IN CALL TO XXSIM
       REAL*8 RES(5), WKS(210), ERR(5)
       INTEGER ISTOP
C
       COMMON /SPL3/ IEND1,IENDN,G1,GN,A1,B1,AN,BN,P1(3),Q1(3),         
     &               PN(3),QN(3),A1RY(10),B1RY(10),ANRY(10),BNRY(10)    
C      CALL ERRSET(208,256,-1)                                          
C      CALL PAPER(1)                                                    
C      GRID=' '
C      CALL FILNAM(GRID)                                                
C                                                                       

 8900  CONTINUE

C8900  IPAN=0                                                           
C      CALL SPMA11(IPAN,ANS,Z0,Z,Z1,IIGRP0,IRGRP0,TITLE,                
C    &             ACIA,BCIA,ACRA,BCRA,                                 
C    &             ISHE,IRES,INS1,ILS1,ES1,IZS1,INS2,
C    &             ILS2,ES2,IZS2,ER1,ER2,WR1,WR2,
C    &             IIFTYP,IIOTYP,IIFOUT,IXMAX,ITMAX,XDAT,EDAT,TDAT,     
C    &             IOP,ASCL)                                            
C      IF(ANS.EQ.'YES')GOTO 9999                                        
C      WRITE(6,*)IPAN,ANS,Z0,Z,Z1,IIGRP0,IRGRP0,TITLE                   
C      WRITE(6,*)ACIA,BCIA,ACRA,BCRA,IOP,ASCL                           
C      WRITE(6,*)(ISHE(I),I=1,2)                                        
C      WRITE(6,*)(INS1(I),I=1,6),(INS2(I),I=1,6)                        
C      WRITE(6,*)(ILS1(I),I=1,6),(ILS2(I),I=1,6)                        
C      WRITE(6,*)(ES1(I),I=1,6),(ES2(I),I=1,6)                          
C      WRITE(6,*)(IZS1(I),I=1,6),(IZS2(I),I=1,6)                        
C      WRITE(6,*)(IRES(I),I=1,2)                                        
C      WRITE(6,*)(ER1(I),I=1,6),(ER2(I),I=1,6)                          
C      WRITE(6,*)(WR1(I),I=1,6),(WR2(I),I=1,6)                          
C      WRITE(6,*)IIFTYP,IIOTYP,IIFOUT,IXMAX,ITMAX                       
C      DO 4 I=1,IXMAX                                                   
C      WRITE(6,*)I,EDAT(I),XDAT(I)                                      
C   4  CONTINUE                                                         
C      DO 7 I=1,ITMAX                                                   
C      WRITE(6,*)I,TDAT(I)                                              
C   7  CONTINUE                                                         
C                                                                       
       NIGRP=IIGRP0                                                     
       NRGRP=IRGRP0                                                     
C                                                                       
       EMIN=1.0D10                                                      
       IF(NIGRP.LE.0)GO TO 102                                          
       DO 101 IIGRP=1,NIGRP                                             
C                                                                       
       NSHELA(IIGRP)=ISHE(IIGRP)                                        
       IF(IIGRP.EQ.1)CIADEF(IIGRP)=ACIA                                 
       IF(IIGRP.EQ.2)CIADEF(IIGRP)=BCIA                                 
C                                                                       
       DO 100 ISHEL=1,NSHELA(IIGRP)                                     
C                                                                       
       IF(IIGRP.EQ.1)THEN                                               
                         NA(ISHEL,IIGRP)=INS1(ISHEL)                    
                         LA(ISHEL,IIGRP)=ILS1(ISHEL)                    
                         EIONA(ISHEL,IIGRP)=ES1(ISHEL)                  
                         IZETAA(ISHEL,IIGRP)=IZS1(ISHEL)                
                     ELSE                                               
                         NA(ISHEL,IIGRP)=INS2(ISHEL)                    
                         LA(ISHEL,IIGRP)=ILS2(ISHEL)                    
                         EIONA(ISHEL,IIGRP)=ES2(ISHEL)                  
                         IZETAA(ISHEL,IIGRP)=IZS2(ISHEL)                
                     END IF                                             
C                                                                       
       EMIN=DMIN1(EMIN,EIONA(ISHEL,IIGRP))                              
  100  CONTINUE                                                         
  101  CONTINUE                                                         
  102  IF(NRGRP.LE.0)GO TO 105                                          
       DO 104 IRGRP=1,NRGRP                                             
C                                                                       
       NRESOA(IRGRP)=IRES(IRGRP)                                        
       IF(IRGRP.EQ.1)CRADEF(IRGRP)=ACRA                                 
       IF(IRGRP.EQ.2)CRADEF(IRGRP)=BCRA                                 
C                                                                       
       DO 103 IRESO=1,NRESOA(IRGRP)                                     
C                                                                       
       IF(IRGRP.EQ.1) THEN                                              
                          ENERA(IRESO,IRGRP)=ER1(IRESO)                 
                          WGHTA(IRESO,IRGRP)=WR1(IRESO)                 
                      ELSE                                              
                          ENERA(IRESO,IRGRP)=ER2(IRESO)                 
                          WGHTA(IRESO,IRGRP)=WR2(IRESO)                 
                      END IF                                            
C                                                                       
  103  CONTINUE                                                         
  104  CONTINUE                                                         
C-----------------------------------------------------------------------
C       X IS DEFINED AS    X=E/EMIN                                     
C-----------------------------------------------------------------------
C                                                                       
  105  IFTYP=IIFTYP                                                     
C                                                                       
C-----------------------------------------------------------------------
C       OMEGA IS DEFINED AS   OMEGA = XSECT(PI*A0**2)/(1-1/X)           
C-----------------------------------------------------------------------
C                                                                       
       IOTYP=IIOTYP                                                     
C                                                                       
  106  DO 40 ICT=1,IXMAX                                                
       X=EDAT(ICT)                                                      
       Y=XDAT(ICT)                                                      
       IF(X.LT.0.0D0)GO TO 45
C SWITCH ON UNIT TYPE                                         
       GO TO (26,27,28),IFTYP                                           
   26  XA(ICT)=X/EMIN                                                   
       GO TO 30                                                         
   27  XA(ICT)=X/(13.6048D0*EMIN)                                       
       GO TO 30                                                         
   28  XA(ICT)=X                                                        
C SWITCH ON UNIT TYPE                                         
   30  GO TO (32,33,34,35),IOTYP                                        
   32  YA(ICT)=Y/(1.0D0-1.0D0/XA(ICT))                                  
       GO TO 40                                                         
   33  YA(ICT)=Y/(8.7972D-17*(1.0D0-1.0D0/XA(ICT)))                     
       GO TO 40                                                         
   34  YA(ICT)=Y                                                        
       GO TO 40                                                         
   35  YA(ICT)=Y/(Z1*Z1)                                                
C
   40  CONTINUE                                                         
C                                                                       
   45  ICT=ICT-1                                            
C                                                                       
       IFOUT=IIFOUT                                                     
C                                                                       
   48  DO 55 ITOUT=1,ITMAX                                              
         TE=TDAT(ITOUT)                                                 
         IF(TE.LT.0.0D0)GO TO 60                                        
         GO TO (50,51,52),IFOUT                                         
   50    TOA(ITOUT)=TE                                                  
         GO TO 55                                                       
   51    TOA(ITOUT)=1.16054D4*TE                                        
         GO TO 55                                                       
   52    TOA(ITOUT)=TE*Z1*Z1                                            
   55  CONTINUE                                                         
C                                                                       
   60  ITOUT=ITOUT-1                                                    
  240  CONTINUE                                                         
C-----------------------------------------------------------------------
C  GENERATE THE APPROXIMATE VALUES READY FOR THE REGRESSION ANALYSIS    
C-----------------------------------------------------------------------
       IXCOR=40
       NOBS=ICT                                                         
       ISSPZ=5                                                          
       IRZ=5                                                            
       IFAIL=0                                                          
       NVAR=NIGRP+NRGRP+1                                               
       DO 150 IC=1,ICT                                                  
          XSTORE(IC,NVAR)=YA(IC)                                           
          IF(NIGRP.LE.0)GO TO 136 
          DO 135 IIGRP=1,NIGRP                                             
             SUM=0.0D0                                                        
             DO 132 ISHEL=1,NSHELA(IIGRP)                                     
                ZETA=IZETAA(ISHEL,IIGRP)
                XI=EIONA(ISHEL,IIGRP)
                E=XA(IC)*EMIN
                OMBCH=QBCHID(Z,XI,ZETA,E)/(1.0D0-EMIN/E)
                SUM=SUM+OMBCH                                          
 132         CONTINUE
             XSTORE(IC,IIGRP)=SUM
 135      CONTINUE
 136      CONTINUE 
          IF(NRGRP.LE.0)GO TO 146                                          
          DO 145 IRGRP=1,NRGRP                                             
             SUM=0.0D0                                                        
             DO 142 IRESO=1,NRESOA(IRGRP)                                     
                OMRES=0.0D0
                E=XA(IC)*EMIN
                EIJ=ENERA(IRESO,IRGRP)
                IF(E.LT.EIJ)GO TO 142
                OMRES=1.45D0*WGHTA(IRESO,IRGRP)/(EIJ*E*(1.0D0-EMIN/E))
                SUM=SUM+OMRES                                           
 142         CONTINUE
             XSTORE(IC,IRGRP+NIGRP)=SUM                               
 145      CONTINUE
 146      CONTINUE 
C-----------------------------------------------------------------------
C     ALTER REGRESSION WEIGHTING BY DIVIDING BY 1ST FUNCTION            
C-----------------------------------------------------------------------
          X1=XSTORE(IC,1)                                                  
          DO 147 I=1,NVAR                                                  
             XCOR(IC,I)=XSTORE(IC,I)/X1
 147      CONTINUE
 150   CONTINUE                                                         
C       DO 151 IC=1,ICT                                                  
C          WRITE(I4UNIT(-1),1022)(XCOR(IC,J),J=1,NVAR)
C 151   CONTINUE
 1022  FORMAT(1H ,1P,3D12.4)                                            
C-----------------------------------------------------------------------
C UNIX-IDL CONVERSION:
C     REGRESSION NOW PERFORMED BY XXSIM RATHER THAN NAG ROUTINES
C     RESULT IS IN RES
C-----------------------------------------------------------------------
       DO 298 I=1,ICT
          YAT(I) = XCOR(I,NVAR)
 298   CONTINUE
       CALL XXSIM(XCOR, IXCOR, NOBS, YAT, NVAR-1, RES, WKS, ERR, ISTOP)
C       CALL G02BDF(NOBS,NVAR,XCOR,IXCOR,XBAR,STD,SSPZ,ISSPZ,RZ,IRZ,     
C     &             IFAIL)                                               
CC      DO 152 I=1,NVAR                                                  
CC 152  WRITE(6,1025)(SSPZ(I,J),J=1,NVAR)                                
C 1025  FORMAT(1H ,1P,5D12.4)                                            
CC      DO 153 I=1,NVAR                                                  
CC 153  WRITE(6,1025)(RZ(I,J),J=1,NVAR)                                  
C       NIND=NVAR-1                                                      
C       ICOEFF=5                                                         
C       IRZINV=5                                                         
C       ICZ=5                                                            
C       IWKZ=5                                                           
C       IFAIL=0                                                          
C       CALL G02CHF(NOBS,NVAR,NIND,SSPZ,ISSPZ,RZ,IRZ,RESULT,COEFF,       
C     &             ICOEFF,RZINV,IRZINV,CZ,ICZ,WKZ,IWKZ,IFAIL)           
C       IF(IFAIL.LE.0)GO TO 155                                          
C       WRITE(6,1017)IFAIL                                               
C 1017  FORMAT('REGRESSION FAILURE, IFAIL=',I2)                      
       IF(ISTOP.EQ.0.OR.ISTOP.EQ.4.OR.ISTOP.EQ.5)GO TO 155
       WRITE(I4UNIT(-1),1017)ISTOP
 1017  FORMAT('REGRESSION FAILURE, ISTOP=',I2)                      
  155  IDEF=IOP                                                         
       IF(NIGRP.LE.0)GO TO 158                                          
       DO 157 IIGRP=1,NIGRP                                             
       CIA(IIGRP)=RES(IIGRP)                                        
       IF(IDEF.NE.0)CIA(IIGRP)=CIADEF(IIGRP)                            
       CIA4(IIGRP)=CIA(IIGRP)                                           
  157  CONTINUE                                                         
 1023  FORMAT(1H ,F10.5)                                                
  158  IF(NRGRP.LE.0)GO TO 160                                          
       DO 159 IRGRP=1,NRGRP                                             
       CRA(IRGRP)=RES(IRGRP+NIGRP)                                
       IF(IDEF.NE.0)CRA(IRGRP)=CRADEF(IRGRP)                            
       CRA4(IRGRP)=CRA(IRGRP)                                           
  159  CONTINUE                                                         
  160  CONTINUE                                                         
C-----------------------------------------------------------------------
C  GENERATE BEST APPROXIMATE VALUES AND PREPARE GRAPH                   
C-----------------------------------------------------------------------
  161  DO 170 IC=1,ICT                                                  
       APA(IC)=0.0D0                                                    
       IF(NIGRP.LE.0)GO TO 164                                          
       DO 162 IIGRP=1,NIGRP                                             
  162  APA(IC)=APA(IC)+CIA(IIGRP)*XSTORE(IC,IIGRP)
  164  IF(NRGRP.LE.0)GO TO 168                                          
       DO 166 IRGRP=1,NRGRP                                             
  166  APA(IC)=APA(IC)+CRA(IRGRP)*XSTORE(IC,IRGRP+NIGRP)
  168  XPA(IC)=XA(IC)                                                   
       YPA(IC)=YA(IC)/APA(IC)                                           
  170  CONTINUE                                                         
C-----------------------------------------------------------------------
C  SET POSITIONS OF THRESHOLDS FOR PLOTTING AND ESTABLISH POINTING      
C  ARRAYS                                                               
C-----------------------------------------------------------------------
       NITHR=0                                                          
       IF(NIGRP.LE.0)GO TO 175                                          
       DO 173 IIGRP=1,NIGRP                                             
       DO 172 ISHEL=1,NSHELA(IIGRP)                                     
       EIONA4(ISHEL,IIGRP)=EIONA(ISHEL,IIGRP)                           
       IIPT(ISHEL,IIGRP)=0                                              
       DO 171 IC=1,ICT                                                  
       IF(EIONA(ISHEL,IIGRP).LT.XA(IC)*EMIN)GO TO 174                   
  171  IIPT(ISHEL,IIGRP)=IIPT(ISHEL,IIGRP)+1                            
  174  CONTINUE                                                         
       NITHR=NITHR+1                                                    
  172  XP(NITHR)=EIONA(ISHEL,IIGRP)/EMIN                                
  173  CONTINUE                                                         
  175  NRTHR=0                                                          
       IF(NRGRP.LE.0)GO TO 180                                          
       DO 179 IRGRP=1,NRGRP                                             
       DO 178 IRESO=1,NRESOA(IRGRP)                                     
       ENERA4(IRESO,IRGRP)=ENERA(IRESO,IRGRP)                           
       WGHTA4(IRESO,IRGRP)=WGHTA(IRESO,IRGRP)                           
       IRPT(IRESO,IRGRP)=0                                              
       DO 176 IC=1,ICT                                                  
       IF(ENERA(IRESO,IRGRP).LT.XA(IC)*EMIN)GO TO 177                   
  176  IRPT(IRESO,IRGRP)=IRPT(IRESO,IRGRP)+1                            
  177  CONTINUE                                                         
       NRTHR=NRTHR+1                                                    
  178  XP(NRTHR+NITHR)=ENERA(IRESO,IRGRP)/EMIN                          
  179  CONTINUE                                                         
  180  CONTINUE                                                         
C       Z14=Z1                                                           
C       EMIN4=EMIN                                                       
C       CALL SPF11A(TITLE,XPA,YPA,ICT,XP,YP,NITHR,NRTHR,CIA4,CRA4,NIGRP  
C     &             ,NRGRP,ASCL)                                         
C       IF(NIGRP.LE.0)GO TO 185                                          
CC      WRITE(6,1019)(CIA(IIGRP),IIGRP=1,NIGRP)                          
CC1019  FORMAT(1H0,'CIA VALUES = ',2F10.5)                               
C  185  IF(NRGRP.LE.0)GO TO 188                                          
CC      WRITE(6,1021)(CRA(IRGRP),IRGRP=1,NRGRP)                          
CC1021  FORMAT(1H0,'CRA VALUES = ',2F10.5)                               
C  188  CONTINUE                                                         
C  190  CONTINUE                                                         
C-----------------------------------------------------------------------
C  PREPARE INTERPOLATION ARRAYS PA AND QA                               
C-----------------------------------------------------------------------
       PA0=0.0D0                                                        
       QA0=YPA(1)                                                       
       DO 192 IC=1,ICT-1                                                
       PA(IC)=(YPA(IC)-YPA(IC+1))/((XPA(IC)-XPA(IC+1))*EMIN)            
  192  QA(IC)=(XPA(IC)*YPA(IC+1)-XPA(IC+1)*YPA(IC))/(XPA(IC)-XPA(IC+1)) 
       PA(ICT)=0.0D0                                                    
       QA(ICT)=YPA(ICT)                                                 
C-----------------------------------------------------------------------
C  FORM RATE COEFFICIENTS                                               
C-----------------------------------------------------------------------
       DO 220 IT=1,ITOUT                                                
         ATE=1.57890D5/TOA(IT)                                          
         FACT=2.46853D8/DSQRT(ATE)                                      
         SUM=0.0D0                                                      
         SUMA=0.0D0                                                     
         IF(NIGRP.LE.0)GO TO 210                                        
         DO 209 IIGRP=1,NIGRP                                           
         DO 208 ISHEL=1,NSHELA(IIGRP)                                   
         IIK=IIPT(ISHEL,IIGRP)                                          
         XI=EIONA(ISHEL,IIGRP)                                          
         XIATE=XI*ATE                                                   
         FACI=2.3D0*8.7972D-17*IZETAA(ISHEL,IIGRP)*ATE/XI               
         SUMA=SUMA+CIA(IIGRP)*FACT*FACI*DEXP(-XIATE)*EEI(XIATE)         
         X1ATE=XIATE                                                    
         IF(IIK.EQ.ICT)GO TO 202                                        
         X2ATE=XA(IIK+1)*ATE*EMIN                                       
         S =FACI*(DEXP(-X1ATE)*(1.0D0+EEI(X1ATE))-DEXP(-X2ATE)*((X2ATE+ 
     &         1.0D0)*DLOG(X2ATE/XIATE)+1.0D0+EEI(X2ATE)))              
       T=FACI*(DEXP(-X1ATE)*EEI(X1ATE)-DEXP(-X2ATE)*(DLOG(X2ATE/XIATE)+ 
     &       EEI(X2ATE)))                                               
         GO TO 203                                                      
  202    S=FACI*DEXP(-X1ATE)*(1.0D0+EEI(X1ATE))                         
         T=FACI*DEXP(-X1ATE)*EEI(X1ATE)                                 
         GO TO 206                                                      
  203    IF(IIK.LE.0)GO TO 360                                          
         SUM=SUM+FACT*CIA(IIGRP)*(PA(IIK)*S/ATE+QA(IIK)*T)              
         GO TO 361                                                      
  360    SUM=SUM+FACT*CIA(IIGRP)*(PA0*S/ATE+QA0*T)                      
  361    CONTINUE                                                       
         IF(IIK.EQ.ICT-1)GO TO 205                                      
         DO 204 IC=IIK+1,ICT-1                                          
         X1ATE=XA(IC)*ATE*EMIN                                          
         X2ATE=XA(IC+1)*ATE*EMIN                                        
         S = FACI*(DEXP(-X1ATE)*((X1ATE+1.0D0)*DLOG(X1ATE/XIATE)+1.0D0+ 
     & EEI(X1ATE))-DEXP(-X2ATE)*((X2ATE+1.0D0)*DLOG(X2ATE/XIATE)+1.0D0+ 
     &       EEI(X2ATE)))                                               
       T=FACI*(DEXP(-X1ATE)*(DLOG(X1ATE/XIATE)+EEI(X1ATE))-DEXP(-X2ATE)*
     &   (DLOG(X2ATE/XIATE)+EEI(X2ATE)))                                
  204    SUM=SUM+FACT*CIA(IIGRP)*(PA(IC)*S/ATE+QA(IC)*T)                
  205    X1ATE=XA(ICT)*ATE*EMIN                                         
         S = FACI*(DEXP(-X1ATE)*((X1ATE+1.0D0)*DLOG(X1ATE/XIATE)+1.0D0+ 
     &       EEI(X1ATE)))                                               
         T = FACI*(DEXP(-X1ATE)*(DLOG(X1ATE/XIATE)+EEI(X1ATE)))         
  206    SUM = SUM+FACT*CIA(IIGRP)*(PA(ICT)*S/ATE+QA(ICT)*T)            
  208    CONTINUE                                                       
  209    CONTINUE                                                       
  210    IF(NRGRP.LE.0)GO TO 221                                        
         DO 219 IRGRP=1,NRGRP                                           
         DO 218 IRESO=1,NRESOA(IRGRP)                                   
         IRK=IRPT(IRESO,IRGRP)                                          
         XI=ENERA(IRESO,IRGRP)                                          
         XIATE=XI*ATE                                                   
         FACR=1.45D0*8.7972D-17*WGHTA(IRESO,IRGRP)*ATE/XI               
         SUMA=SUMA+CRA(IRGRP)*FACT*FACR*DEXP(-XIATE)                    
         X1ATE=XIATE                                                    
         IF(IRK.EQ.ICT)GO TO 212                                        
         X2ATE=XA(IRK+1)*ATE*EMIN                                       
         U=FACR*(DEXP(-X1ATE)*(X1ATE+1.0D0)-DEXP(-X2ATE)*(X2ATE+1.0D0)) 
         V=FACR*(DEXP(-X1ATE)-DEXP(-X2ATE))                             
         GO TO 213                                                      
  212    U=FACR*(DEXP(-X1ATE)*(X1ATE+1.0D0))                            
         V=FACR*DEXP(-X1ATE)                                            
         GO TO 216                                                      
  213    IF(IRK.LE.0)GO TO 270                                          
         SUM=SUM+FACT*CRA(IRGRP)*(PA(IRK)*U/ATE+QA(IRK)*V)              
         GO TO 271                                                      
  270    SUM=SUM+FACT*CRA(IRGRP)*(PA0*U/ATE+QA0*V)                      
  271    CONTINUE                                                       
         IF(IRK.EQ.ICT-1)GO TO 215                                      
         DO 214 IC=IRK+1,ICT-1                                          
         X1ATE=XA(IC)*ATE*EMIN                                          
         X2ATE=XA(IC+1)*ATE*EMIN                                        
         U=FACR*(DEXP(-X1ATE)*(X1ATE+1.0D0)-DEXP(-X2ATE)*(X2ATE+1.0D0)) 
         V=FACR*(DEXP(-X1ATE)-DEXP(-X2ATE))                             
  214    SUM=SUM+FACT*CRA(IRGRP)*(PA(IC)*U/ATE+QA(IC)*V)                
  215    X1ATE=XA(ICT)*ATE*EMIN                                         
         U=FACR*(DEXP(-X1ATE)*(X1ATE+1.0D0))                            
         V=FACR*DEXP(-X1ATE)                                            
  216    SUM=SUM+FACT*CRA(IRGRP)*(PA(ICT)*U/ATE+QA(ICT)*V)              
  218    CONTINUE                                                       
  219    CONTINUE                                                       
  221    YOA(IT)=SUM                                                    
  220    YOAP(IT)=SUMA                                                  
CC-----------------------------------------------------------------------
CC  PREPARE SECOND GRAPH                                                 
CC-----------------------------------------------------------------------
C       DO 225 IT=1,ITOUT                                                
C         TEA4(IT)=TOA(IT)                                               
C         RAT4(IT)=YOA(IT)                                               
C  225  RATAP4(IT)=YOAP(IT)                                              
C       CALL SPF11B(TITLE,Z14,ITOUT,TEA4,RAT4,RATAP4,NIGRP,NRGRP,        
C     &       NSHELA,NRESOA,EIONA4,ENERA4,IZETAA,WGHTA4,EMIN4,CIA4,CRA4) 

C
C-----------------------------------------------------------------------
C  PREPARE OUTPUT FOR STREAM 7                                          
C  ** MOVED TO A5OUT0 **
C-----------------------------------------------------------------------
C
C 245      WRITE(7,1029) TITLE, Z0, Z, Z1                                   
C          IF(NIGRP.LE.0)GO TO 250                                          
C          WRITE(7,1030) NIGRP,EMIN                                         
C          DO 247 IIGRP=1,NIGRP                                             
C             WRITE(7,1031)IIGRP,CIA(IIGRP)                                 
C             DO 246 ISHEL=1,NSHELA(IIGRP)                                  
C                WRITE(7,1032)EIONA(ISHEL,IIGRP),IZETAA(ISHEL,IIGRP)        
C 246         CONTINUE                                                      
C 247      CONTINUE                                                         
C 250      IF(NRGRP.LE.0)GO TO 255                                          
C          WRITE(7,1033)NRGRP                                               
C          DO 252 IRGRP=1,NRGRP                                             
C             WRITE(7,1034)IRGRP,CRA(IRGRP)                                 
C             DO 251 IRESO=1,NRESOA(IRGRP)                                  
C                WRITE(7,1035)ENERA(IRESO,IRGRP),WGHTA(IRESO,IRGRP)         
C 251         CONTINUE                                                      
C 252      CONTINUE                                                         
C 255      WRITE(7,1036)                                                    
C          DO 256 IC=1,ICT                                                  
C             Y=(1.0D0-1.0D0/XA(IC))*YA(IC)                                 
C             WRITE(7,1037) XA(IC), Y, YA(IC), APA(IC), YPA(IC)
C 256      CONTINUE
C          WRITE(7,1038)                                                    
C          TEV = 1.16054D+04
C          DO 257 IT=1,ITOUT                                                
C             WRITE(7,1039) TOA(IT), TOA(IT)/TEV, YOA(IT),YOAP(IT)           
C 257      CONTINUE
C          
C          IZ  = NINT(Z)
C          IZ1 = NINT(Z1)
C          WRITE(7,1050) IZ, IZ1, ITOUT, EMIN*109737.3
C          WRITE(7,1051) ( TOA(IT)/TEV , IT =1,ITOUT )
C          WRITE(7,1051) ( YOA(IT)     , IT =1,ITOUT )
C                                                                       
C  260  GOTO 8900                                                        
C 9999  WRITE(7,7777)                                                    
C       CALL GREND                                                       

 1029  FORMAT(1H1,A40/1H0,'NUCLEAR CHARGE = ',F5.1,3X,'INITIAL ION CHARG
     &E = ',F5.1,3X,'FINAL ION CHARGE = ',F5.1)                         
 1030  FORMAT(1H0,'DIRECT IONISATION   NIGRP =',I2,3X,'EMIN =',F10.5)   
 1031  FORMAT(1H0,5X,'GROUP',I2,3X,'CIA =',F8.3,8X,'EION(RYD)   ZETA')  
 1032  FORMAT(1H ,35X,F10.5,I5)                                         
 1033  FORMAT(1H0,'EXCIT/AUTOIONISATION  NRGRP =',I2)                   
 1034  FORMAT(1H0,5X,'GROUP',I2,3X,'CRA =',F8.3,8X,'EIJ (RYD)   WGHT')  
 1035  FORMAT(1H ,35X,F10.5,F7.3)                                       
 1036  FORMAT(1H0,'     X        Q(PI*A0**2)     Q/(1-1/X)     QEM/(1-1/
     &X)       Q/QEM')                                                  
 1037  FORMAT(1H ,F10.5,1P,4D15.5)                                      
 1038  FORMAT(1H0,'    TE(K)       TE(EV)        S          SEM')       
 1039  FORMAT(1H ,1P,4D12.4)                                            
 1050  FORMAT(1H0,'  +',I2,'/  +',I2,'/',I5,'/I.P. =',7X,F12.1,
     &            '/ICODE =  1/FCODE =  1/ISEL=   1')
 1051  FORMAT(1H ,1P,6D10.3)
C
 7777  FORMAT('  ')                                                     
C
C
C
      END                                                               
