CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas105/a5spf1.for,v 1.2 2004/07/06 10:02:45 whitefor Exp $ Date $Date: 2004/07/06 10:02:45 $
CX
       SUBROUTINE A5SPF1( LPEND   , IGRPOUT   ,  
     &                    IGRPSCAL1, IGRPSCAL2, ITEXOUT   ,
     &                    CGTIT   , CHEADER   , TEXDSN, ASCL
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN 77 SUBROUTINE: A1SPF1  *******************
C
C  CALLING PROGRAM: 
C          ADAS105.FOR
C
C  PURPOSE:
C          TO COMMUNICATE WITH IDL OUTPUT SELECTION INTERFACE 
C          A5SPF1.PRO VIA THE UNIX PIPE.
C
C  INPUT:
C          NONE
C  OUTPUT:
C          (L)    LPEND     = CANCEL OR DONE.
C          (I*4)  IGRPOUT   = GRAPH DISPLAY SELECTION SWITCH
C          (I*4)  IGRPSCAL1 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             IST GRAPH.
C          (I*4)  IGRPSCAL2 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             2NT GRAPH.
C          (I*4)  ITEXOUT   = HARD COPY OF TEXT OUTPUT REQUEST SWITCH.
C                             0 - NO, 1 - YES
C          (C*40) CGTIT     = THE GRAPH TITLE (INFORMATION FOR ANY
C                             NEW ARCHIVE).
C          (C*80) CHEADER   = THE ADAS HEADER FOR THE DEFAULT FILE.
C          (C*80) TEXDSN    = THE DEFAULT ARCHIVE FILE NAME.
C          (R*8)  ASCL      = GRAPHICAL SCALING PARAMETER
C
C ROUTINES:
C----------
C          XXSLEN - REMOVES SPACES FROM A STRING
C
C  AUTHOR: WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC, 13TH SEPTEMBER 1996
C  DATE:   13/09/96
C
C  VERSION 1.1                                          DATE:   13/09/96
C  MODIFIED: WILLIAM OSBORN
C            - FIRST RELEASE
C
C  VERSION 1.2                                          DATE:   08/10/96
C  MODIFIED: WILLIAM OSBORN
C            - ADDED ASCL PARAMETER
C
C-----------------------------------------------------------------------
       INTEGER  IGRPOUT  , IFIRST  , ILAST 
       INTEGER  IGRPSCAL1, IGRPSCAL2, ITEXOUT , PIPEIN  , PIPEOU
       INTEGER  IPEND
C-----------------------------------------------------------------------
       CHARACTER CGTIT*40 , CHEADER*80 , TEXDSN*80
       CHARACTER CTEXDSN*80
C-----------------------------------------------------------------------
       REAL*8 ASCL
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------

       READ(PIPEIN,*)IPEND
       IF (IPEND.EQ.1)THEN
          LPEND = .TRUE.
       ELSE
          LPEND = .FALSE.
C
C
          READ(PIPEIN,*)IGRPOUT
          IF (IGRPOUT.EQ.1)THEN
             READ(PIPEIN,'(A)')CGTIT
             READ(PIPEIN,*)IGRPSCAL1
C     
             READ(PIPEIN,*)IGRPSCAL2

          ENDIF
C-----------------------------------------------------------------------
          READ(PIPEIN,*)ITEXOUT
          IF (ITEXOUT.EQ.1)THEN
             READ(PIPEIN,'(1A80)')CTEXDSN
             CALL XXSLEN( CTEXDSN , IFIRST , ILAST )
             READ(CTEXDSN(IFIRST:ILAST),'(A)') TEXDSN
C     TEXDSN = CTEXDSN
          ENDIF

          READ(PIPEIN,*)ASCL

       ENDIF
C     
       IF (IPEND.EQ.0.AND.ITEXOUT.EQ.1)THEN
         READ(PIPEIN,'(1A80)')CHEADER
       ENDIF

C-----------------------------------------------------------------------
 1000  FORMAT(1X,I3)
 1001  FORMAT(1X,E8.2)
 1002  FORMAT(1X,1A40)
 1003  FORMAT(1X,1A80)
 1004  FORMAT(1A45)
C-----------------------------------------------------------------------
       RETURN
       END
