CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas105/a5out0.for,v 1.3 2004/07/06 10:02:20 whitefor Exp $ Date $Date: 2004/07/06 10:02:20 $
CX
      SUBROUTINE A5OUT0( DSFULL , INDXREF , IXTIT , Z0     , Z     ,
     &     Z1     , NIGRP   , EMIN  , CIA    , NSHELA, NA  , LA  ,
     &     EIONA  , IZETAA  , NRGRP , CRA    , NRESOA,
     &     ENERA  , WGHTA   , ICT   , XA     , YA    , APA    ,
     &     ITOUT  , TOA     , YOA   , YOAP   ,
     &     NDTEM  , LARCH   , DATE  , IASEL  , TEXDSN, ITEXOUT,
     &     CHEADER
     &     )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A5OUT0*******************
C
C  PURPOSE: 
C        TO WRITE DATA TO ANC OLD/NEW ARCHIVE IN THE FORMAT DECIDED
C        BY THE ANALYSIS OPTION.
C
C  CALLING PROGRAM:
C        ADAS105.FOR
C
C  DATA:  
C     IANOPT SET TO 1, THE ADAS ANALYSIS OPTION, AT PRESENT
C
C  INPUT:
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*40) IXTIT    - TITLE FOR THIS ARCHIVE ENTRY
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION         
C            (R*8)  Z      = INITIAL ION CHARGE                  
C            (R*8)  Z1     = FINAL ION CHARGE
C            (I*4)  NIGRP  = NO. OF SHELL GROUPS
C            (R*8)  EMIN   = MINIMUM ENERGY (?)
C            (R*8)  CIA()  = SCALING PARAMETERS FOR SHELL GROUPS
C            (I*4)  NSHELA()=NO. OF ENTRIES FOR EACH SHELL GROUP
C            (I*4)  NA(,)  = SHELL GROUP DATA : N
C            (I*4)  LA(,)  = SHELL GROUP DATA : L
C            (R*8)  EIONA(,)=SHELL GROUP DATA : EION(RYD)
C            (I*4)  IZETAA(,)=SHELL GROUP DATA : IZETA
C            (I*4)  NRGRP  = NO. OF RESONANCE GROUPS
C            (R*8)  CRA()  = SCALING PARAMETERS FOR RESONANCE GROUPS
C            (I*4)  NRESOA()=NO. OF ENTRIES FOR EACH RESONANCE GROUP
C            (R*8)  ENERA(,)=RESONANCE GROUP DATA : ENERGY(RYD)
C            (R*8)  WGHTA(,)=RESONANCE GROUP DATA : WEIGHT
C            (I*4)  ICT    = NO. OF ENERGY / X-SECTION PAIRS
C            (R*8)  XA()   = X, THRESHOLD PARAMETER RELATIVE TO FIRST IONIS. POT.
C            (R*8)  YA()   = Q, CROSS-SECTION
C            (R*8)  APA()  = QEM, APPROX. X-SECTION
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (R*8)  TOA()  = TEMPERATURE (KELVIN)
C            (R*8)  YOA()  = S, MAXWELL AVERAGED IONISATION RATE COEFF.(CM^3 S^-1)
C            (R*8)  YOAP() = SEM, APPROXIMATE RATE COEFF.
C            (I*4)  NDTEM  = MAXIMUM NUMBER OF INPUT TEMPERATURES
C            (L)    LARCH  = ARCHIVING SELECTION OPTION
C            (I*4)  IASEL  = THE ARCHIVING CHOICE (SEE A5SPF0.FOR)
C            (C*80) TEXDSN = THE DEFAULT HARD COPY FILE NAME
C            (I*4)  ITEXOUT= THE HARD COPY OF FILE OUTPUT FLAG
C                            0 - NO, 1 - YES
C            (C*80) CHEADER= ADAS HEADER FOR PAPER.TXT
C
C  ROUTINES:
C
C  AUTHOR: 
C        WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC., 28TH AUG 1996
C  DATE: 28/08/96
C
C  VERSION 1.1                                       DATE 28/08/96
C  MODIFIED: WILLIAM OSBORN
C		- FIRST RELEASE
C
C  VERSION 1.2                                       DATE 08/10/96
C  MODIFIED: WILLIAM OSBORN
C		- ADDED NA AND LA TO OUTPUT
C
C  VERSION 1.3                                       DATE 08/10/96
C  MODIFIED: RICHARD MARTIN
C		CHANGED SCCS VARIABLE %H% TO @(#)$Header: /home/adascvs/fortran/adas1xx/adas105/a5out0.for,v 1.3 2004/07/06 10:02:20 whitefor Exp $ IN HEADER
C
C-----------------------------------------------------------------------
      CHARACTER IXTIT*40, DSFULL*80, DATE*8, CSTRNG*32, CODE*4
      CHARACTER TEXDSN*80, DCODE(2)*4, CIARR(500)*80, CINDX*3
      CHARACTER CHEADER*80, CSTRING*80
C-----------------------------------------------------------------------
      INTEGER INDXREF, NIGRP, NDTEM, NSHELA(2), IZETAA(6,2)
      INTEGER NRESOA(2), ITOUT, NRGRP, INDX, IASEL, ITEXOUT
      INTEGER IIGRP, ISHEL, IRGRP, IRESO, IC, IT, IHFLAG
      INTEGER IASELCOPY, IANOPT, IWRITE, ICNT, I , ICT
      INTEGER NA(6,2), LA(6,2)
      PARAMETER( IWRITE=17)
C-----------------------------------------------------------------------
      REAL*8 Z0, Z, Z1, EMIN, CIA(2), EIONA(6,2), CRA(2), ENERA(6,2)
      REAL*8 WGHTA(6,2), XA(40), YA(40), APA(40), TOA(NDTEM)
      REAL*8 YOA(NDTEM), YOAP(NDTEM)
      REAL*8 Y, TEV
C-----------------------------------------------------------------------
      LOGICAL LARCH
C-----------------------------------------------------------------------
      REAL*4 YPA
C-----------------------------------------------------------------------
      DATA DCODE/'   A','   B'/, IANOPT/1/
C-----------------------------------------------------------------------
C DEPENDING ON THE OPTIONS, CHECK THE FILE
C-----------------------------------------------------------------------

      IF (.NOT.LARCH) THEN
         IHFLAG = 1
      ELSE
         IHFLAG = 0
      ENDIF
      IF (IHFLAG.EQ.0) THEN
         OPEN( UNIT=17 , FILE = DSFULL , STATUS = 'UNKNOWN' )
         IF (IASEL.NE.3) THEN
C     
C-----------------------------------------------------------------------
C     CHECK FOR HIGHEST INDEX
C-----------------------------------------------------------------------
C     
 1          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:1).NE.' '.AND.CSTRING(1:1).NE.'-') THEN
               IF (CSTRING(1:1).NE.'C') THEN
                  CINDX = CSTRING(2:4)
                  GO TO 1
               ELSE
                  CINDX = CINDX
               ENDIF
            ELSE
               GO TO 1
            ENDIF
C     
C-----------------------------------------------------------------------
C     COPY INDEX COMMENTS TO TEMPORARY ARRAY
C-----------------------------------------------------------------------
C     
            CIARR(1) = CSTRING
            ICNT = 2
 2          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:2).EQ.'C ') THEN
               CIARR(ICNT) = CSTRING
               ICNT = ICNT+1
               GO TO 2
            ELSE
               CIARR(ICNT) = CSTRING
               ICNT = ICNT+1
            ENDIF
C     
C-----------------------------------------------------------------------
C     CLOSE FILE OFF TO RE-OPEN FOR POSITION FINDING
C-----------------------------------------------------------------------
C     
            CLOSE( UNIT=IWRITE , STATUS = 'KEEP')
            OPEN( UNIT=IWRITE , FILE = DSFULL , STATUS = 'UNKNOWN' )
 3          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:1).EQ.'I') THEN
               IF (CSTRING(2:4).NE.CINDX) THEN
                  GO TO 3
               ENDIF
            ELSE
               GO TO 3
            ENDIF
C     
 4          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:1).NE.'-') THEN
               GO TO 4
            ENDIF
C     
C     CLOCK INDEX UP ONE
C     
            READ(CINDX,1016) INDX
            INDX = INDX+1
         ELSE
            INDX = 1
        ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C     NOW WRITE OUT DATA 
C-----------------------------------------------------------------------
C     
      IF (IANOPT.EQ.1) THEN
         CODE = DCODE(1)
      ELSE
         CODE = DCODE(2)
      ENDIF 
 100  IF (IANOPT.EQ.2) THEN
          
C-----------------------------------------------------------------------
C NO BURGESS OPTION PRESENT
C-----------------------------------------------------------------------
C         IF (IHFLAG.EQ.1) THEN
C            OPEN(UNIT = IWRITE,FILE = TEXDSN, STATUS = 'UNKNOWN')
C            WRITE(IWRITE,*) CHEADER
C         ENDIF
C         WRITE(CSTRING,1000) TITLE     
C         WRITE(IWRITE,1001) INDX, CSTRING, DATE, CODE
C     CALL WUPSILON( EI    , EJ    , EIJ   , IXTYP , GF    , 
C     &                      BCVAL , B     , INDL  , INDU  , Z0    ,
C     &                      Z     , ITOUT , TDAT  , GOAN  , DSFULL,
C     &                      ISTDIM, ICT   , EDAT  , XDAT  , IWRITE,
C     &                      INDIM , WI    , WJ
C     &                     ) 
C-----------------------------------------------------------------------
      ELSE
         IF (IHFLAG.EQ.1) THEN
            OPEN(UNIT = IWRITE,FILE = TEXDSN, STATUS = 'UNKNOWN')
            WRITE(IWRITE,*) CHEADER
         ENDIF
         WRITE(CSTRNG,'(1A32)')IXTIT
         WRITE(IWRITE,1028) INDX, CSTRNG, DATE, CODE
         
         WRITE(IWRITE,1029) Z0, Z, Z1                                   
         IF(NIGRP.LE.0)GO TO 250                                          
         WRITE(IWRITE,1030) NIGRP,EMIN             
         DO 247 IIGRP=1,NIGRP                                             
            WRITE(IWRITE,1031)IIGRP,CIA(IIGRP)
            DO 246 ISHEL=1,NSHELA(IIGRP)                                  
               WRITE(IWRITE,1032)NA(ISHEL,IIGRP),LA(ISHEL,IIGRP),
     &              EIONA(ISHEL,IIGRP), IZETAA(ISHEL,IIGRP)
 246        CONTINUE                                                      
 247     CONTINUE                                                         
 250     IF(NRGRP.LE.0)GO TO 255                                          
         WRITE(IWRITE,1033)NRGRP
         DO 252 IRGRP=1,NRGRP
            WRITE(IWRITE,1034)IRGRP,CRA(IRGRP)
            DO 251 IRESO=1,NRESOA(IRGRP)
               WRITE(IWRITE,1035)ENERA(IRESO,IRGRP),
     &              WGHTA(IRESO,IRGRP)
 251        CONTINUE                                    
 252     CONTINUE                                                         
 255     WRITE(IWRITE,1036)
         DO 256 IC=1,ICT                                                  
            Y=(1.0D0-1.0D0/XA(IC))*YA(IC)                                 
            YPA = YA(IC)/APA(IC)
            WRITE(IWRITE,1037) XA(IC), Y, YA(IC), APA(IC), YPA
 256     CONTINUE
         WRITE(IWRITE,1038)
         TEV = 1.16054D+04
         DO 257 IT=1,ITOUT                                                
            WRITE(IWRITE,1039) TOA(IT), TOA(IT)/TEV, YOA(IT),YOAP(IT)
 257     CONTINUE
      ENDIF
C-----------------------------------------------------------------------
C     WRITE STUFF BACK AT THE END
C-----------------------------------------------------------------------
      IF (IHFLAG.EQ.0) THEN
         IF (IASEL.EQ.3) THEN
            WRITE(IWRITE,1013)
         ELSE
            WRITE(IWRITE,1017)
            DO 30 I = 1, ICNT-2
               WRITE(IWRITE,1015) CIARR(I)
 30         CONTINUE
         ENDIF
C     
         WRITE(IWRITE,1014) INDX, CODE, CSTRNG(1:6), 
     &        CSTRNG(7:22), CSTRNG(23:32),
     &        DATE
      ENDIF
      CLOSE( UNIT=IWRITE , STATUS = 'KEEP')
C     
C     GO BACK TO WRITE HARD COPY IF REQUESTED & SET UP OPTION FLAG
C     
      IF (IHFLAG.EQ.0) THEN
         IF (ITEXOUT.EQ.1) THEN
            IHFLAG = 1
            IASELCOPY = IASEL
            IASEL = 3
            GOTO 100 
         ENDIF
      ENDIF
      IASEL = IASELCOPY
      
C     IZ  = NINT(Z)
C     IZ1 = NINT(Z1)
C     WRITE(IWRITE,1050) IZ, IZ1, ITOUT, EMIN*109737.3
C     WRITE(IWRITE,1051) ( TOA(IT)/TEV , IT =1,ITOUT )
C     WRITE(IWRITE,1051) ( YOA(IT)     , IT =1,ITOUT )
      
      RETURN

C-----------------------------------------------------------------------
 1013 FORMAT('-1  '/
     & 'C-----------------------------------------------------------',
     & '------------'/
     & 'C INDEX  CODE  INFORMATION                                  ',
     & 'DATE       ')
 1014  FORMAT('C ',I3,2X,1A4,3X,1A6,1X,1A16,1X,1A10,10X,1A8,6X/
     & 'C-----------------------------------------------------------',
     & '------------')
 1015 FORMAT(1A80)
 1016 FORMAT(I3)
 1017 FORMAT('-1        ')
 1028 FORMAT('I',I3,2X, 1A32, 1A8, 24X, 1A4)
 1029 FORMAT(' NUCLEAR CHARGE = ',F5.1,3X,'INITIAL ION CHARGE = ',
     &     F5.1,3X,'FINAL ION CHARGE = ',F5.1)                         
 1030 FORMAT(' DIRECT IONISATION   NIGRP =',I2,3X,'EMIN =',F10.5)   
 1031 FORMAT(5X,'GROUP',I2,3X,'CIA =',F8.3,8X,'EION(RYD)   ZETA')  
 1032 FORMAT(1H ,35X,2I3,F10.5,I5)                                         
 1033 FORMAT(' EXCIT/AUTOIONISATION  NRGRP =',I2)                   
 1034 FORMAT(5X,'GROUP',I2,3X,'CRA =',F8.3,8X,'EIJ (RYD)   WGHT')  
 1035 FORMAT(1H ,35X,F10.5,F7.3)                                       
 1036 FORMAT('     X        Q(PI*A0**2)     Q/(1-1/X)     QEM/(1-1/X)  
     &    Q/QEM')                                                  
 1037 FORMAT(1H ,F10.5,1P,4D15.5)                                      
 1038 FORMAT('    TE(K)       TE(EV)        S          SEM')       
 1039 FORMAT(1H ,1P,4D12.4)                                            
 1050 FORMAT('  +',I2,'/  +',I2,'/',I5,'/I.P. =',7X,F12.1,
     &            '/ICODE =  1/FCODE =  1/ISEL=   1')
 1051 FORMAT(1H ,1P,6D10.3)
C

      END
