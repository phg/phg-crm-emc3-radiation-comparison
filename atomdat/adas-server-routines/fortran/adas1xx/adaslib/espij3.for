CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/espij3.for,v 1.1 2004/07/06 13:49:45 whitefor Exp $ Date $Date: 2004/07/06 13:49:45 $
CX
       SUBROUTINE ESPIJ3(N,H,W)
       IMPLICIT REAL*8(A-H,O-Z)                                         0000000
C-----------------------------------------------------------------------
C  SUBROUTINE TO CALCULATE SPLINES WITH VARIOUS END CONDITIONS.
C
C  EXTENDED ARRAY DIMENSION VERSION OF NSPIJ3
C
C  USES LABELLED COMMON  /ESPL3/
C
C  CONDITIONS AT 1ST NODE AND NTH NODE CONTROLLED BY IEND1 AND IENDN    0000000
C      IEND=1 : SPECIFIED D LOG(Y) IE. DY/Y AT NODE STORED IN APPROPRIAT5070000
C               APPROPRIATE VECTOR
C          =2 : ZERO CURVATURE                                          0000000
C          =3 : CONSTANT CURVATURE                                      0000000
C          =4 : MATCHED TO SPECIFIED FUNCTIONAL FORM IN TERMS OF        0000000
C               TWO PARAMETERS A AND B SUCH THAT                        0000000
C                        FUNCT = P(1)*A+Q(1)*B                          0000000
C                   1ST DERIV. = P(2)*A+Q(2)*B                          0000000
C                   2ND DERIV. = P(3)*A+Q(3)*B                          0000000
C               WHERE A1,B1,P1,Q1 ARE USED FOR 1ST NODE AND             0000000
C               AN,BN,PN,QN FOR NTH NODE                                0000000
C
C  INPUT
C      N=NUMBER OF KNOTS
C      H(I)=INTERVALS BETWEEN KNOTS
C  OUTPUT
C      W=SPLINE MATRIX
C
C  DATE:     24/10/89			VERSION 1.1
C  AUTHOR:   H.P. SUMMERS, JET          
C
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
       DIMENSION A(ISTDIM),B(ISTDIM),C(ISTDIM),H(ISTDIM),
     &           W(ISTDIM,ISTDIM)                                       0000000
       COMMON /ESPL3/IEND1,IENDN,G1,GN,A1,B1,AN,BN,P1(3),Q1(3),         0000000
     & PN(3),QN(3),A1RY(ISTDIM),B1RY(ISTDIM),ANRY(ISTDIM),
     & BNRY(ISTDIM)                                                     0000000
       X=0.0                                                            0000000
       X3=0.0                                                           0000000
       H(N)=1.0D72                                                      0000000
       DO 10 I=1,N                                                      0000000
       DO 12 J=1,N                                                      0000000
   12  W(I,J)=0.0                                                       0000000
       Y=1.0/H(I)                                                       0000000
       Y3=3.0*Y*Y                                                       0000000
       A(I)=X                                                           0000000
       B(I)=2.0*(X+Y)                                                   0000000
       C(I)=Y                                                           0000000
       IF(I.EQ.1)GO TO 13                                               0000000
       W(I,I-1)=-X3                                                     0000000
   13  W(I,I)=X3-Y3                                                     0000000
       IF(I.EQ.N)GO TO 14                                               0000000
       W(I,I+1)=Y3                                                      0000000
   14  X=Y                                                              0000000
   10  X3=Y3                                                            0000000
       GO TO (50,65,55,60),IEND1                                        0000000
   50  C(1)=0.0D0                                                       0000000
       W(1,1)=B(1)*G1                                                   0000000
       W(1,2)=0.0D0                                                     0000000
       GO TO 65                                                         0000000
   55  C(1)=B(1)                                                        0000000
       W(1,1)=-B(1)*B(1)                                                0000000
       W(1,2)=-W(1,1)                                                   0000000
       GO TO 65                                                         0000000
   60  DT1=1.0D0/(P1(1)*Q1(2)-P1(2)*Q1(1))                              0000000
       T1=0.5D0*DT1*(P1(1)*Q1(3)-P1(3)*Q1(1))                           0000000
       T2=0.5D0*DT1*(P1(3)*Q1(2)-P1(2)*Q1(3))                           0000000
       B(1)=B(1)+T1                                                     0000000
       W(1,1)=W(1,1)-T2                                                 0000000
   65  GO TO (70,85,75,80),IENDN                                        0000000
   70  A(N)=0.0D0                                                       0000000
       W(N,N)=B(N)*GN                                                   0000000
       W(N,N-1)=0.0D0                                                   0000000
       GO TO 85                                                         0000000
   75  A(N)=B(N)                                                        0000000
       W(N,N)=B(N)*B(N)                                                 0000000
       W(N,N-1)=-W(N,N)                                                 0000000
       GO TO 85                                                         0000000
   80  DTN=1.0D0/(PN(1)*QN(2)-PN(2)*QN(1))                              0000000
       T1=0.5D0*DTN*(PN(1)*QN(3)-PN(3)*QN(1))                           0000000
       T2=0.5D0*DTN*(PN(3)*QN(2)-PN(2)*QN(3))                           0000000
       B(N)=B(N)-T1                                                     0000000
       W(N,N)=W(N,N)+T2                                                 0000000
   85  H(N)=0.0                                                         0000000
       DO 20 I=2,N                                                      0000000
       X=A(I)/B(I-1)                                                    0000000
       B(I)=B(I)-X*C(I-1)                                               0000000
       DO 15 J=1,I                                                      0000000
   15  W(I,J)=W(I,J)-X*W(I-1,J)                                         0000000
   20  CONTINUE                                                         0000000
       X=1.0/B(N)                                                       0000000
       DO 23 I=1,N                                                      0000000
   23  W(N,I)=X*W(N,I)                                                  0000000
       DO 30 I=2,N                                                      0000000
       K=N-I+1                                                          0000000
       X=1.0/B(K)                                                       0000000
       DO 25 J=1,N                                                      0000000
   25  W(K,J)=X*(W(K,J)-C(K)*W(K+1,J))                                  0000000
   30  CONTINUE                                                         0000000
       IF(IEND1.NE.4)GO TO 45                                           0000000
       DO 43 I=1,N                                                      0000000
       A1RY(I)=-DT1*Q1(1)*W(1,I)                                        0000000
   43  B1RY(I)=DT1*P1(1)*W(1,I)                                         0000000
       A1RY(1)=A1RY(1)+DT1*Q1(2)                                        0000000
       B1RY(1)=B1RY(1)-DT1*P1(2)                                        0000000
   45  IF(IENDN.NE.4)RETURN                                             0000000
       DO 48 I=1,N                                                      0000000
       ANRY(I)=-DTN*QN(1)*W(N,I)                                        0000000
   48  BNRY(I)=DTN*PN(1)*W(N,I)                                         0000000
       ANRY(N)=ANRY(N)+DTN*QN(2)                                        0000000
       BNRY(N)=BNRY(N)-DTN*PN(2)                                        0000000
       RETURN                                                           0000000
       END                                                              0000000
