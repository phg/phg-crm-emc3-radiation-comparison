CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axetrdv.for,v 1.1 2004/07/06 11:17:18 whitefor Exp $ Date $Date: 2004/07/06 11:17:18 $
CX
       FUNCTION AXETRDV( IXTYP , EIJ   , DGEX  , C )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: AXETRDV ******************
C
C  PURPOSE:  TO CALCULATE THE ELECTRON ENERGY FROM THE REDUCED ENERGY
C            FOR FOUR TYPES OF TRANSITION 
C
C  CALLING PROGRAM:
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (Eij)
C            (R*8)    DGEX   =   REDUCED ENERGY
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    TL     =   Ej/Eij
C            (I)      IXTYPE =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C
C  OUTPUT:   (R*8)    AXETRDV=   ELECTRON ENERGY
C
C 
C  ROUTINES: NONE
C
C  WRITTEN:   CONVERSION OF ETREDINV BY A.LANZAFAME & D.H.BROOKS BY
C             HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C             TEL. 0141-553-4196
C
C  DATE:     24/11/96				VERSION 1.1
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   HUGH SUMMERS  24/11/96
C  MODIFIED:      FIRST RELEASE
C
C-----------------------------------------------------------------------
       INTEGER IXTYP
C-----------------------------------------------------------------------
       REAL*8 EIJ   , DGEX   , C  , TL   , AXETRDV
C-----------------------------------------------------------------------
       IF(IXTYP.EQ.1.OR.IXTYP.EQ.4)THEN
         TL = DEXP(-DLOG(C)/(DGEX-1))-C
       ELSE IF (IXTYP.EQ.2.OR.IXTYP.EQ.3)THEN
         TL = DGEX*C/(1-DGEX)
       ENDIF
       AXETRDV = DABS(TL*EIJ)
C-----------------------------------------------------------------------
       RETURN
       END
