CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axomup.for,v 1.2 2004/07/06 11:17:30 whitefor Exp $ Date $Date: 2004/07/06 11:17:30 $
CX
      FUNCTION AXOMUP(IT,E,T,U,C)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: AXOMUP *********************
C
C  PURPOSE:
C        TO INTERPOLATE OMEGAS OR UPSILONS FOR DIFFERENT TRANSITIONS
C
C  CALLING PROGRAM:
C        OMEUPS
C
C  INPUT:
C        (I*4)    IT - TRANSITION TYPE 
C        (R*8)    E - EXCITATION ENRGY 
C        (R*8)    T - SCALED ENERGY VALUE OF QUADRATURE FIXED POINTS
C        (R*8)    U - SPLINE FIT TO THE KNOT POINTS AND 
C                     REDUCED ENERGIES OR TEMPERATURES 
C        (R*8)    C - SCALABLE PARAMETER
C
C  COMMON:  
C     /BURG/
C        (L*4)    LUPSIL = .TRUE.  (UPSILON FITTING)
C                          .FALSE. (OMEGA FITTING  )
C 
C  OUTPUT:
C        (R*8) AXOMUP - THE UPSILONS
C
C
C  WRITTEN:   CONVERSION OF OMUP BY A.LANZAFAME & D.H.BROOKS BY
C             HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C             TEL. 0141-553-4196
C
C  DATE:     24/11/96				VERSION 1.1
C  MODIFIED:  FIRST RELEASE
C
C  DATE:   18/05/99				VERSION 1.2
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C          PREVENTED CHANGE OF INPUT SUBROUTINE PARAMETER T ON RETURN
C
C-----------------------------------------------------------------------
      INTEGER IT
C-----------------------------------------------------------------------
      REAL*8 E,T,U,C,AXOMUP,TL
C-----------------------------------------------------------------------
      LOGICAL LUPSIL
C-----------------------------------------------------------------------
      COMMON /BURG/LUPSIL
C-----------------------------------------------------------------------
      TL=T/E
      IF (IT.EQ.1) AXOMUP=U*DLOG(TL+2.71828)
      IF (IT.EQ.2) AXOMUP=U
      IF (IT.EQ.3) THEN
          IF(LUPSIL) THEN
              AXOMUP = U/(TL+1)
          ELSE
              AXOMUP=U/(TL+1)**2
          ENDIF
      ENDIF
      IF (IT.EQ.4) AXOMUP=U*DLOG(TL+C)
C----------------------------------------------------------------------
      RETURN
      END

