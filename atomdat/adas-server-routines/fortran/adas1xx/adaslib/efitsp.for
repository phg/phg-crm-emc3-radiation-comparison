CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/efitsp.for,v 1.1 2004/07/06 13:48:26 whitefor Exp $ Date $Date: 2004/07/06 13:48:26 $
CX
       SUBROUTINE EFITSP(X,XA,N,YAA,Y,DY,I0,C1,C2,C3,C4,ISW)            0000000
       IMPLICIT REAL*8(A-H,O-Z)                                         0000000
C-----------------------------------------------------------------------
C  SUBROUTINE TO PERFORM SPLINE INTERPOLATION                           0000000
C
C  EXTENDED ARRAY DIMENSION VERSION OF NFITSP
C
C  INPUT
C      X       = REQUIRED X-VALUE
C      XA(I)   = X-VALUES
C      N       = NUMBER OF VALUES
C      YAA(I)  = Y-VALUES (POSSIBLY STORED AS MULTIPLE SETS)
C      I0      = STARTING INDEX(-1) IN YAA ARRAY OF REQUIRED INPUT SET
C      C1(I,J) = 1ST SPLINE COEFFICIENT PRECURSOR
C      C2(I,J) = 2ND SPLINE COEFFICIENT PRECURSOR
C      C3(I,J) = 3RD SPLINE COEFFICIENT PRECURSOR
C      C4(I,J) = 4TH SPLINE COEFFICIENT PRECURSOR
C      ISW     = .LE.0  ORDINARY     SPLINE INTERPOLATION
C              = .GT.0  LOGARITHMIC  SPLINE INTERPOLATION
C  OUTPUT
C      Y       = RETURNED Y-VALUE
C      DY      = RETURNED DERIVATIVE
C
C
C  DATE:     06/10/89				VERSION 1.1
C  AUTHOR:   H.P. SUMMERS, JET       
C
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
       DIMENSION YAA(ISTDIM),TA(ISTDIM),XA(ISTDIM)                      0000000
       DIMENSION C1(ISTDIM,ISTDIM-1),C2(ISTDIM,ISTDIM-1),
     &           C3(ISTDIM,ISTDIM-1),C4(ISTDIM,ISTDIM-1)                0000000
       DIMENSION CT1(ISTDIM-1),CT2(ISTDIM-1),CT3(ISTDIM-1),
     &           CT4(ISTDIM-1)                                          0000000
       DO 3 I=1,N                                                       0000000
       T=YAA(I+I0)                                                      0000000
       IF(ISW.GT.0)T=DLOG(T)                                            0000000
    3  TA(I)=T                                                          0000000
       DO 24 J=2,N                                                      0000000
       J1=J-1                                                           0000000
       CT1(J1)=0.0                                                      0000000
       CT2(J1)=0.0                                                      0000000
       CT3(J1)=0.0                                                      0000000
       CT4(J1)=0.0                                                      0000000
       DO 23 I=1,N                                                      0000000
       CT1(J1)=CT1(J1)+C1(I,J1)*TA(I)                                   0000000
       CT2(J1)=CT2(J1)+C2(I,J1)*TA(I)                                   0000000
       CT3(J1)=CT3(J1)+C3(I,J1)*TA(I)                                   0000000
   23  CT4(J1)=CT4(J1)+C4(I,J1)*TA(I)                                   0000000
   24  CONTINUE                                                         0000000
       DO 27 J=2,N                                                      0000000
       IF(X.GT.XA(J))GO TO 27                                           0000000
       XB=0.5D0*(XA(J-1)+XA(J))                                         0000000
       J1=J-1                                                           0000000
       GO TO 25                                                         0000000
   27  CONTINUE                                                         0000000
       XB=0.5*(XA(N-1)+XA(N))                                           0000000
       J1=N-1                                                           0000000
   25  XB=X-XB                                                          0000000
       Y=CT1(J1)+XB*(CT2(J1)+XB*(CT3(J1)+XB*CT4(J1)))                   0000000
       DY=CT2(J1)+XB*(2.0D0*CT3(J1)+XB*3.0D0*CT4(J1))                   0000000
       IF(ISW.LE.0)GO TO 26                                             0000000
       Y=DEXP(Y)                                                        0000000
       DY=DY*Y                                                          0000000
   26  RETURN                                                           0000000
       END                                                              0000000
