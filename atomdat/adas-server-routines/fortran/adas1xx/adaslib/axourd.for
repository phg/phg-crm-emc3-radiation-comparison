CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axourd.for,v 1.1 2004/07/06 11:17:36 whitefor Exp $ Date $Date: 2004/07/06 11:17:36 $
CX
       FUNCTION AXOURD( KTYPE, EIJ  , EJ   , OMUP , C    )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: AXOURD *********************
C
C  PURPOSE:  TO CALCULATE THE REDUCED COLLISION STRENGTH OR REDUCED 
C            UPSILON AS A FUNCTION OF Ej/Eij  OR kTE/EIJ FOR FOUR
C            TYPES OF TRANSITION
C
C  CALLING PROGRAM: VARIOUS ADAS101  AND ADAS102 ROUTINES
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (Eij)
C            (R*8)    EJ     =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    ETR     =  Ej/Eij OR kTe/Eij
C            (R*8)    OMUP   =   COLLISION STRENGTH OR UPSILON
C                                AS A FUNCTION 
C                                OF ETR
C            (I)      KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C
C  COMMON:  
C        /BURG/
C            (L*4)    LUPSIL = .TRUE.  (UPSILON FITTING)
C                              .FALSE. (OMEGA FITTING  ) 
C
C  OUTPUT:   (R*8)    AXOURD =   REDUCED COLLISION STRENGTH OR UPSILON
C
C 
C  ROUTINES: NONE
C
C  WRITTEN:   CONVERSION OF OURED BY A.LANZAFAME & D.H.BROOKS BY
C             HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C             TEL. 0141-553-4196
C
C  DATE:     24/11/96				VERSION 1.1
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   HUGH SUMMERS  24/11/96
C  MODIFIED:      FIRST RELEASE.
C
C-----------------------------------------------------------------------
       INTEGER KTYPE
C-----------------------------------------------------------------------
       REAL*8 EIJ  , EJ   , C    , ETR  ,OMUP , AXOURD 
C-----------------------------------------------------------------------
       LOGICAL LUPSIL
C-----------------------------------------------------------------------
       COMMON /BURG/LUPSIL
C-----------------------------------------------------------------------
       ETR = DABS(EJ/EIJ)
C
       IF (KTYPE.EQ.1) AXOURD = OMUP/DLOG(ETR+2.718281828)
       IF (KTYPE.EQ.2) AXOURD = OMUP
       IF (KTYPE.EQ.3) THEN
           IF(LUPSIL) THEN 
              AXOURD = OMUP*(ETR+1)
           ELSE
              AXOURD = OMUP*(ETR+1)*(ETR+1)
           ENDIF
       ENDIF 
       IF (KTYPE.EQ.4) AXOURD = OMUP/DLOG(ETR+C)
C-----------------------------------------------------------------------
       RETURN
       END
