CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axetrd.for,v 1.1 2004/07/06 11:17:15 whitefor Exp $ Date $Date: 2004/07/06 11:17:15 $
CX
       FUNCTION AXETRD( KTYPE , E , T ,  C)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: AXETRD *********************
C
C  PURPOSE:  TO CALCULATE THE REDUCED ENERGY FOR FOUR TYPES OF 
C            TRANSITION
C
C  CALLING PROGRAM: VARIOUS ADAS101 CODES
C
C  FUNCTION:
C
C  INPUT:    (R*8)    E      =   TRANSITION ENERGY (Eij)
C            (R*8)    T      =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    TL     =   Ej/Eij OR kTe/Eij
C            (I)      KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C
C  COMMON:  
C        /BURG/
C            (L*4)    LUPSIL = .TRUE.  (UPSILON FITTING)
C                              .FALSE. (OMEGA FITTING  ) 
C
C
C  OUTPUT:   (R*8)    AXETRD  =   REDUCED ENERGY OR TEMPERATURE
C
C 
C  ROUTINES: NONE
C
C  WRITTEN:   CONVERSION OF ETRED BY A.LANZAFAME & D.H.BROOKS BY
C             HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C             TEL. 0141-553-4196
C
C  DATE:     24/11/96				VERSION 1.1
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   HUGH SUMMERS  24/11/96
C  MODIFIED:      FIRST RELEASE
C
C-----------------------------------------------------------------------
       INTEGER KTYPE
C-----------------------------------------------------------------------
       REAL*8 E , T , C , TL , AXETRD
C----------------------------------------------------------------------- 
      LOGICAL LUPSIL
C-----------------------------------------------------------------------
      COMMON /BURG/LUPSIL
C-----------------------------------------------------------------------
       TL = DABS(T/E)
C
       IF (KTYPE.EQ.1.OR.KTYPE.EQ.4)THEN
          AXETRD = DLOG((TL+C)/C)/DLOG(TL+C)
       ELSE IF (KTYPE.EQ.2.OR.KTYPE.EQ.3)THEN
          AXETRD=TL/(TL+C)
       ENDIF
C-----------------------------------------------------------------------
       RETURN
       END
