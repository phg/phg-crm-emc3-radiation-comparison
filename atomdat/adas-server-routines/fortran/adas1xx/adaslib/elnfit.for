CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/elnfit.for,v 1.1 2004/07/06 13:49:02 whitefor Exp $ Date $Date: 2004/07/06 13:49:02 $
CX
       SUBROUTINE ELNFIT(X,XSA,Y,YSA,ICT)
       IMPLICIT REAL*8(A-H,O-Z)                                         0000000
C-----------------------------------------------------------------------
C  SUBROUTINE TO PERFORM LINEAR INTERPOLATION                           0000000
C
C  EXTENDED ARRAY DIMENSION VERSION OF LINFIT
C
C  INPUT
C      X     = REQUIRED X-VALUE
C      XSA(I)= X-VALUES
C      YSA(I)= Y-VALUES
C      ICT   = NUMBER OF VALUES
C  OUTPUT
C      Y     = RETURNED Y-VALUE
C
C  DATE: 06/10/89				VERSION 1.1
C  AUTHOR:   H.P. SUMMERS, JET        
C
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
       DIMENSION XSA(ISTDIM),YSA(ISTDIM)
       IF(ICT.GT.1)GO TO 10                                             0000000
       Y=YSA(1)                                                         0000000
       RETURN                                                           0000000
   10  IF(X.GT.XSA(1))GO TO 20                                          0000000
       Y=YSA(1)                                                         0000000
       RETURN                                                           0000000
   20  IF(X.LT.XSA(ICT))GO TO 30                                        0000000
       Y=YSA(ICT)                                                       0000000
       RETURN                                                           0000000
   30  I=0                                                              0000000
   40  I=I+1                                                            0000000
       IF(X.GT.XSA(I))GO TO 40                                          0000000
       Y=((X-XSA(I))*YSA(I-1)-(X-XSA(I-1))*YSA(I))/(XSA(I-1)            0000000
     &-XSA(I))                                                          0000000
       RETURN                                                           0000000
      END                                                               0000000
