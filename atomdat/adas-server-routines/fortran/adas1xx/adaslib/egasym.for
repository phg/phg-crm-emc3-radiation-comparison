CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/egasym.for,v 1.1 2004/07/06 13:48:29 whitefor Exp $ Date $Date: 2004/07/06 13:48:29 $
CX
       SUBROUTINE EGASYM(X,DX,FORM,IFORMS,IENDS)                        0000000
       IMPLICIT REAL*8(A-H,O-Z)                                         0000000
C-----------------------------------------------------------------------
C  SUBROUTINE INITIALISES COMMON ARRAYS REQUIRED FOR SPLINING WITH
C  SMOOTH FITTING TO AN ASYMPTOTIC FORM                                 0000000
C
C  EXTENDED ARRAY DIMENSION VERSION OF NGASYM
C
C  USES LABELLED COMMON /ESPL3/
C
C  IF IENDS=1,MATCHING IS AT FIRST KNOT(GIVEN BY X)                     0000000
C          =2,MATCHING IS AT  LAST KNOT(GIVEN BY X)                     0000000
C  ASYMPTOTIC FORMS ARE GIVEN IN THE EXTERNAL FUNCTION FORM(I,X)        0000000
C  WHERE I=4*IFORMS-5+2*IENDS POINTS TO FIRST PART OF ASYMP. FORM       0000000
C         =4*IFORMS-4+2*IENDS POINTS TO SECOND PART OF ASYMP. FORM      0000000
C  THUS A SERIES OF ASYMPTOTIC FORMS MAY BE PRESENT IN FORM             0000000
C
C  INPUT
C      COMMON /ESPL3/ PROVIDES INPUT IN VECTOR IEND WHICH SPECIFIES
C                     CHOICE OF END CONDITION AT FIRST IEND(1) OR LAST
C                     IEND(2) KNOT OF SPLINE
C      X=X-VALUE OF END POINT
C      DX=DISPLACEMENT FROM X-VALUE FOR DERIVATIVE EVALUATION
C      FORM=EXTERNAL FUNCTION SPECIFYING ASYMPTOTIC FORMS
C      IFORMS=SELECTED FORM
C      IENDS=1,MATCHING IS AT FIRST KNOT(GIVEN BY X)                    0000000
C           =2,MATCHING IS AT  LAST KNOT(GIVEN BY X)                    0000000
C  OUTPUT
C      COMMON /ESPL3/ IS SET BY THIS ROUTINE
C
C
C DATE:     06/10/89				VERSION 1.1
C AUTHOR :  **** H.P. SUMMERS, JET             6 OCT 1989  *************
C
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
       COMMON /ESPL3/IEND(2),G(2),AB(4),PQ(12),ABRY(4*ISTDIM)           0000000
       IF(IENDS.EQ.1.AND.IEND(1).EQ.4)GO TO 5                           0000000
       IF(IENDS.EQ.2.AND.IEND(2).EQ.4)GO TO 5                           0000000
    3  RETURN                                                           0000000
    5  I=4*IFORMS-5+2*IENDS                                             0000000
       J=6*IENDS-5                                                      0000000
       IC=0                                                             0000000
       DX1=1.0D0/DX                                                     0000000
   10  PQ(J)=FORM(I,X)                                                  0000000
       T1=FORM(I,X+DX)                                                  0000000
       T2=FORM(I,X-DX)                                                  0000000
       PQ(J+1)=0.5D0*DX1*(T1-T2)                                        0000000
       PQ(J+2)=DX1*DX1*(T1-2.0D0*PQ(J)+T2)                              0000000
       IC=IC+1                                                          0000000
       IF(IC.GT.1)RETURN                                                0000000
       I=I+1                                                            0000000
       J=J+3                                                            0000000
       GO TO 10                                                         0000000

       END                                                              0000000
