CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/egspc.for,v 1.1 2004/07/06 13:48:31 whitefor Exp $ Date $Date: 2004/07/06 13:48:31 $
CX
       SUBROUTINE EGSPC(XA,N,C1,C2,C3,C4)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  SUBROUTINE TO GENERATE PRECURSORS OF SPLINE COEFFICIENTS SUITABLE
C  FOR BOTH FORWARD AND BACKWARD INTERPOLATION
C
C  EXTENDED ARRAY DIMENSION VERSION OF NGSPC
C
C  INPUT
C      XA(I)=SET OF KNOTS
C      N=NUMBER OF KNOTS  (N.LE.20)
C  OUTPUT
C      C1(I,J)=1ST SPLINE COEFFICIENT PRECURSOR
C      C2(I,J)=2ND SPLINE COEFFICIENT PRECURSOR
C      C3(I,J)=3RD SPLINE COEFFICIENT PRECURSOR
C      C4(I,J)=4TH SPLINE COEFFICIENT PRECURSOR
C
C
C DATE:    24/10/89			VERSION 1.1
C AUTHOR:  H.P. SUMMERS, JET          
C
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
       DIMENSION HA(ISTDIM),XA(ISTDIM),W(ISTDIM,ISTDIM)
       DIMENSION C1(ISTDIM,ISTDIM-1),C2(ISTDIM,ISTDIM-1),
     &           C3(ISTDIM,ISTDIM-1),C4(ISTDIM,ISTDIM-1)
       DO 3 J=2,N                                                       0000000
       J1=J-1                                                           0000000
    3  HA(J1)=XA(J)-XA(J1)                                              0000000
       IF(N-2)4,5,5                                                     0000000
    4  C1(1,1)=1.0                                                      0000000
       C2(1,1)=0.0                                                      0000000
       C3(1,1)=0.0                                                      0000000
       C4(1,1)=0.0                                                      0000000
       GO TO 20                                                         0000000
    5  CALL ESPIJ3(N,HA,W)
       DO 10 J=2,N                                                      0000000
       J1=J-1                                                           0000000
       H=HA(J1)                                                         0000000
       H1=1.0/H                                                         0000000
       H2=H1*H1                                                         0000000
       H3=H1*H2                                                         0000000
       DO 8 I=1,N                                                       0000000
       U1=H*(W(J1,I)+W(J,I))                                            0000000
       U2=H*(W(J1,I)-W(J,I))                                            0000000
       X=0.0                                                            0000000
       IF(J1.EQ.I)X=1.0                                                 0000000
       IF(J.EQ.I)X=-1.0                                                 0000000
       C1(I,J1)=0.5*X*X+0.125*U2                                        0000000
       C2(I,J1)=-H1*(1.5*X+0.25*U1)                                     0000000
       C3(I,J1)=-0.5*H2*U2                                              0000000
    8  C4(I,J1)=H3*(2.0*X+U1)                                           0000000
   10  CONTINUE                                                         0000000
   20  CONTINUE                                                         0000000
       RETURN                                                           0000000
       END                                                              0000000
