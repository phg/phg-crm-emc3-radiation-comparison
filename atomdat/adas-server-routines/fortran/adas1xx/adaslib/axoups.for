CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axoups.for,v 1.1 2004/07/06 11:17:33 whitefor Exp $ Date $Date: 2004/07/06 11:17:33 $
CX
      FUNCTION AXOUPS(IT,E,C,P,X)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE:AXOUPS*********************
C
C  PURPOSE:
C        TO CALCULATE UPSILONS 
C
C  CALLING PROGRAM:
C        MAXWELL
C
C  INPUT:
C        (I)     IT - TRANSITION TYPE
C        (R*8)    E - EXCITATION ENRGY
C        (R*8)    C - SCALABLE PARAMETER
C        (R*8)    P - KNOT POINTS
C        (R*8)    X - COLLIDING ELECTRON ENERGY AFTER
C                     EXCITATION
C  OUTPUT:
C        (R*8) AXOUPS - THE UPSILONS
C
C  ROUTINES:
C
C  WRITTEN:   CONVERSION OF OMEUPS BY A.LANZAFAME & D.H.BROOKS BY
C             HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C             TEL. 0141-553-4196
C
C  DATE:     24/11/96				VERSION 1.1
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   HUGH SUMMERS  24/11/96
C  MODIFIED:      FIRST RELEASE
C
C
C-----------------------------------------------------------------------  
      INTEGER IT
C-----------------------------------------------------------------------
      REAL*8 E,C,P(5),X,AXOUPS,AXOMUP,AXETRD,SP5,ETR,SP
C----------------------------------------------------------------------- 
      ETR = AXETRD(IT,E,X,C)
      SP = SP5(P,ETR)
      AXOUPS=AXOMUP(IT,E,X,SP,C)
      RETURN
      END

