CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axourdv.for,v 1.1 2004/07/06 11:17:38 whitefor Exp $ Date $Date: 2004/07/06 11:17:38 $
CX
       FUNCTION AXOURDV( IXTYP , EIJ   , EJ  , DGEY  , C )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: AXOURDV ******************
C
C  PURPOSE:  TO CALCULATE THE COLLISION STRENGTH OR UPSILON FROM THE 
C            REDUCED COLLISION STRENGTH OR REDUCED UPSILON
C            FOR FOUR TYPES OF TRANSITION 
C
C  CALLING PROGRAM:
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (Eij)
C            (R*8)    EJ     =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    ETR    =   Ej/Eij
C            (I)      IXTYPE =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C            (R*8)    DGEY   =   REDUCED COLLISION STRENGTH
C
C  COMMON:  
C        /BURG/
C            (L*4)    LUPSIL = .TRUE.  (UPSILON FITTING)
C                              .FALSE. (OMEGA FITTING  ) 
C
C  OUTPUT:   (R*8)    AXOURD =   REDUCED COLLISION STRENGTH OR UPSILON
C
C 
C  ROUTINES: NONE
C
C  WRITTEN:   CONVERSION OF OUREDINV BY A.LANZAFAME & D.H.BROOKS BY
C             HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C             TEL. 0141-553-4196
C
C  DATE:     24/11/96				VERSION 1.1
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   HUGH SUMMERS  24/11/96
C  MODIFIED:      FIRST RELEASE.
C
C-----------------------------------------------------------------------
       INTEGER IXTYP
C-----------------------------------------------------------------------
       REAL*8 EIJ  , EJ   , C    , ETR  , DGEY , AXOURDV
C-----------------------------------------------------------------------
       LOGICAL LUPSIL
C-----------------------------------------------------------------------
       COMMON /BURG/LUPSIL
C-----------------------------------------------------------------------
       ETR = DABS(EJ/EIJ)
C
       IF (IXTYP.EQ.1) AXOURDV = DGEY*DLOG(ETR+2.718281828)
       IF (IXTYP.EQ.2) AXOURDV = DGEY
       IF (IXTYP.EQ.3) THEN
           IF(LUPSIL) THEN 
             AXOURDV = DGEY/(ETR+1)
           ELSE
              AXOURDV = DGEY/((ETR+1)*(ETR+1))
           ENDIF
       ENDIF 
       IF (IXTYP.EQ.4) AXOURDV = DGEY*DLOG(ETR+C)
C-----------------------------------------------------------------------
       RETURN
       END
