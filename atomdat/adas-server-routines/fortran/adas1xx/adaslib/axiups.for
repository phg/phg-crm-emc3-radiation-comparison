CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/axiups.for,v 1.1 2004/07/06 11:17:21 whitefor Exp $ Date $Date: 2004/07/06 11:17:21 $
CX
      SUBROUTINE AXIUPS(IT,C,E,P,NT,T,U)
C-----------------------------------------------------------------------
C  ******************  FORTRAN77 SUBROUTINE: AXIUPS  *******************
C
C  PURPOSE: TO CALCULATE A SET OF USPILONS BY INTERPOLATION OF THE
C           BURGESS FIVE-POINT SPLINE
C
C  INPUT:
C    IT  TRANSITION TYPE
C    C   UPSILON SCALING PARAMETER
C    E   EXCITATION ENERGY
C    P   KNOTS
C    NT  NUMBER OF TEMPERATURE POINTS
C    T   TEMPERATURE
C
C  OUTPUT:
C    U   UPSILON
C
C  ROUTINES:
C    AXOUPS - TO CALCULATE THE MAXWELL AVERAGED UPSILONS
C
C  AUTHOR:  HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           TEL. 0141-553-4196
C
C  DATE:    24 NOVEMBER 1996
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   HUGH SUMMERS  24/11/96
C  MODIFIED:      FIRST RELEASE.
C
C------------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER IT,I,NT,I4UNIT
      REAL*8 T(NT),U(NT),UPS,XX,WW,AXOUPS
C------------------------------------------------------------------------
      REAL*8 E,C,P(5),C1
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C  FREE PARAMETER C1 FIXED TO SUGGESTED ONE
C------------------------------------------------------------------------
      DATA C1/3.0/
C------------------------------------------------------------------------
C
      DO I=1,NT
         XX=T(I)/157888.0
         WW=1
           IF (IT .EQ. 3) THEN
              XX=XX/(1.+XX/(E*C1))
              WW=WW*(E*C/(XX+E*C))*DEXP(XX/(E*C1))
           ENDIF         
         U(I)=WW*AXOUPS(IT,E,C,P,XX)
      ENDDO
      RETURN
      END






