CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/esort.for,v 1.1 2004/07/06 13:49:42 whitefor Exp $ Date $Date: 2004/07/06 13:49:42 $
CX
       SUBROUTINE ESORT(XA,YA,N)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  SUBROUTINE TO SORT AN ARRAY SO THAT XA IS IN INCREASING ORDER
C
C  EXTENDED ARRAY DIMENSION VERSION OF NSORT
C
C  N.B.  INPUT VALUES ARE ALTERED BY THIS ROUTINE !!!!
C
C  INPUT
C      XA(I)=X-VALUES
C      YA(I)=Y-VALUES
C      N=NUMBER OF VALUES
C  OUTPUT
C      XA(I)=SORTED X-VALUES
C      YA(I)=SORTED Y-VALUES
C
C  AUTHOR:
C  ********* H.P. SUMMERS, JET              24 OCT 1989  **************
C
C  UNIX PORT: 11/07/95				VERSION 1.1
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
       DIMENSION XA(ISTDIM),YA(ISTDIM)
       N1=N-1                                                           0000000
       DO 10 I=1,N1                                                     0000000
       I1=I+1                                                           0000000
       DO 5 J=I1,N                                                      0000000
       IF(XA(I).LE.XA(J))GO TO 5                                        0000000
       SWAP=XA(I)                                                       0000000
       XA(I)=XA(J)                                                      0000000
       XA(J)=SWAP                                                       0000000
       SWAP=YA(I)                                                       0000000
       YA(I)=YA(J)                                                      0000000
       YA(J)=SWAP                                                       0000000
    5  CONTINUE                                                         0000000
   10  CONTINUE                                                         0000000
       RETURN                                                           0000000
      END                                                               0000000
