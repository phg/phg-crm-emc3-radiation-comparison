CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adaslib/efasym.for,v 1.1 2004/07/06 13:48:15 whitefor Exp $ Date $Date: 2004/07/06 13:48:15 $
CX
      SUBROUTINE EFASYM(X,XA,N,YA,Y,DY,
     &                  C1,C2,C3,C4,FORM,IFORMS)
      IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  SUBROUTINE TO PROVIDE A SPLINE INTERPOLATE MAKING USE OF SPECIFIED
C  ASYMPTOTIC BEHAVIOUR
C
C  EXTENDED ARRAY DIMENSION VERSION OF NFASYM
C
C  USES LABELLED COMMON /ESPL3/
C
C  INPUT
C      X=REQUIRED X-VALUE
C      X(I)=KNOTS
C      N=NUMBER OF KNOTS
C      C1(I,J)=1ST SPINE COEFFICIENT PRECURSOR
C      C2(I,J)=2ND SPINE COEFFICIENT PRECURSOR
C      C3(I,J)=3RD SPINE COEFFICIENT PRECURSOR
C      C4(I,J)=4TH SPINE COEFFICIENT PRECURSOR
C      FORM=EXTERNAL FUNCTION SPECIFYING ASYMPTOTIC FORMS
C      IFORMS=INDEX OF REQUIRED FORM
C  OUTPUT
C      Y=RETURNED Y-VALUE
C      DY=RETURNED DERIVATIVE
C
C
C  DATE: 24/10/89				VERSION 1.1
C  AUTHOR:**** H.P. SUMMERS, JET               24 OCT 1989  ***********
C-----------------------------------------------------------------------
      INCLUDE 'PARAMS'
      DIMENSION XA(ISTDIM),YA(ISTDIM),
     &          C1(ISTDIM,ISTDIM-1),C2(ISTDIM,ISTDIM-1),
     &          C3(ISTDIM,ISTDIM-1),C4(ISTDIM,ISTDIM-1)
       COMMON /ESPL3/IEND(2),G(2),AB(4),PQ(12),ABRY(4*ISTDIM)           0000000
       DY=0.0D0                                                         0000000
       IF(X.GE.XA(1))GO TO 5                                            0000000
       IENDS=1                                                          0000000
       IN=1                                                             0000000
       GO TO 15                                                         0000000
    5  IF(X.LE.XA(N))GO TO 10                                           0000000
       IENDS=2                                                          0000000
       IN=N                                                             0000000
       GO TO 15                                                         0000000
   10  CALL EFITSP(X,XA,N,YA,Y,DY,0,C1,C2,C3,C4,0)                      0000000
       RETURN                                                           0000000
   15  K=IEND(IENDS)                                                    0000000
       GOTO(20,30,30,40),K                                              0000000
   20  DY=G(IENDS)*YA(IN)                                               0000000
   25  Y=YA(IN)+(X-XA(IN))*DY                                           0000000
       RETURN                                                           0000000
   30  XX=XA(IN)                                                        0000000
       CALL EFITSP(XX,XA,N,YA,Y,DY,0,C1,C2,C3,C4,0)                     0000000
       IF(IEND(IENDS).EQ.2)GO TO 25                                     0000000
       IF(IENDS.EQ.2)GO TO 35                                           0000000
       H=XA(2)-XA(1)                                                    0000000
       DDY=2.0D0*(-YA(1)+YA(2)-H*DY)/(H*H)                              0000000
       GO TO 38                                                         0000000
   35  H=XA(N)-XA(N-1)                                                  0000000
       DDY=2.0D0*(YA(N-1)-YA(N)+H*DY)/(H*H)                             0000000
   38  DX=X-XA(IN)                                                      0000000
       Y=YA(IN)+DX*DY+0.5D0*DX*DX*DDY                                   0000000
       RETURN                                                           0000000
   40  A=0.0D0                                                          0000000
       B=0.0D0                                                          0000000
       DO 42 I=1,N                                                      0000000
       A=A+ABRY(2*ISTDIM*IENDS-2*ISTDIM+I)*YA(I)                        0000000
   42  B=B+ABRY(2*ISTDIM*IENDS-ISTDIM+I)*YA(I)                          0000000
       J=4*IFORMS+2*IENDS-5                                             0000000
       Y=A*FORM(J,X)+B*FORM(J+1,X)                                      0000000
       RETURN                                                           0000000
      END                                                               0000000
