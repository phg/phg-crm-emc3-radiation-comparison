CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/a6ispf.for,v 1.1 2004/07/06 10:02:55 whitefor Exp $ Date $Date: 2004/07/06 10:02:55 $
CX
       SUBROUTINE A6ISPF( DSFULL , INDXREF , TITLE , CAMETH , Z0    ,
     &             Z      , Z1     , NIGRP , NRGRP  ,
     &             ACIA   , BCIA   , ACRA  , BCRA   ,
     &             NSHELA , NRESOA , INS1  , ILS1   , ES1   , IZS1 ,
     &             INS2   , ILS2   , ES2   , IZS2   , ER1   , ER2  ,
     &             WR1    , WR2    , IIFTYP, IIOTYP , IIFOUT, ICT  ,
     &             ITOUT  , XDAT   , EDAT  , TDAT   ,     
     &             IOP    ,
     &             XA     , YA     , CIA   , CRA    , NA    , LA   ,
     &             EIONA  , IZETAA , ENERA , WGHTA  , TOA   , IASEL,
     &             ISTDIM , LPEND
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A6ISPF *******************
C
C  PURPOSE: TO READ USER ALTERATIONS/SPECIFICATIONS FROM IDL AND TO
C           COMMUNICATE OLD ARCHIVE DATA TO IDL IF REQUESTED.
C
C  CALLING PROGRAM:
C            ADAS106.FOR
C
C  INPUT:
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                              FILE.
C            (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                              TWO TYPES OF ANALYSIS.
C                              A - ADAS, B- BURGESS (ONLY 'A' WORKS)
C            (R*8)  XA()     = ENERGY (PARAMETER X)
C            (R*8)  YA()     = OMEGA (COLLISION STRENGTH)C         
C            (R*8)  CIA()    = SCALING PARAMETERS FOR SHELL GROUPS
C            (R*8)  CRA()    = SCALING PARAMETERS FOR RESONANCE GROUPS
C            (I*4)  NA(,)    = SHELL GROUP DATA : N
C            (I*4)  LA(,)    = SHELL GROUP DATA : L
C            (R*8)  EIONA(,) = SHELL GROUP DATA : EION(RYD)
C            (I*4)  IZETAA(,)= SHELL GROUP DATA : IZETA
C            (R*8)  ENERA(,) = RESONANCE GROUP DATA : ENERGY(RYD)
C            (R*8)  WGHTA(,) = RESONANCE GROUP DATA : WEIGHT
C            (R*8)  TOA()    = TEMP (KELVIN)
C            (I*4)  ISTDIM   = ARRAY DIMENSIONS
C            (I*4)  IASEL  - THE ARCHIVING CHOICE.
C                           0 - NO ARCHIVE
C                           1 - EXAMINE OLD ARCHIVE
C                           2 - REFRESH FROM ARCHIVE
C                           3 - NEW ARCHIVE
C          
C  I/O:
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION
C            (R*8)  Z      = ION CHARGE
C            (R*8)  Z1     = ION CHARGE + 1
C            (I*4)  NIGRP  = NO. OF SHELL GROUPS
C            (I*4)  NRGRP  = NO. OF RESONANCE GROUPS
C            (I*4)  NSHELA() = NO. OF ENTRIES FOR EACH SHELL GROUP
C            (I*4)  NRESOA() = NO. OF ENTRIES FOR EACH RESONANCE GROUP
C  OUTPUT:
C            (R*8)  ACIA   = SCALING PARAMETER FOR SHELL GROUP 1
C            (R*8)  BCIA   = SCALING PARAMETER FOR SHELL GROUP 2
C            (R*8)  ACRA   = SCALING PARAMETER FOR RESONANCE GROUP 1
C            (R*8)  BCRA   = SCALING PARAMETER FOR RESONANCE GROUP 2
C            (I*4)  INS1() = SHELL GROUP 1 DATA: N
C            (I*4)  ILS1() = SHELL GROUP 1 DATA: L
C            (R*8)  ES1()  = SHELL GROUP 1 DATA: EION(RYD)
C            (I*4)  IZS1() = SHELL GROUP 1 DATA: IZETA
C            (I*4)  INS2() = SHELL GROUP 2 DATA: N
C            (I*4)  ILS2() = SHELL GROUP 2 DATA: L
C            (R*8)  ES2()  = SHELL GROUP 2 DATA: EION(RYD)
C            (I*4)  IZS2() = SHELL GROUP 2 DATA: IZETA
C            (R*8)  ER1()  = RESONANCE GROUP 1 DATA: ENERGY(RYD)
C            (R*8)  ER2()  = RESONANCE GROUP 2 DATA: ENERGY(RYD)
C            (R*8)  WR1()  = RESONANCE GROUP 1 DATA: WEIGHT
C            (R*8)  WR2()  = RESONANCE GROUP 2 DATA: WEIGHT
C            (I*4)  IIFTYP = ENERGY PARAMETER FORM
C                            1 : INCIDENT ENERGY (RYD)
C                            2 : INCIDENT ENERGY (EV)
C                            3 : X THRESHOLD PARAMETER
C            (I*4)  IIOTYP = CROSS-SECTIONAL FORM
C                            1 : X-SECT. (PI*A0**2)
C                            2 : X-SECT. (CM**2)
C                            3 : COLLISION STRENGTH (OMEGA)
C                            4 : SCALED COLLISION STRENGTH ((Z**2)*OMEGA)
C            (I*4)  IIFOUT = OUTPUT TEMPERATURE FORM
C                            1 : KELVIN
C                            2 : EV
C                            3 : SCALED UNITS (TE(K)/(Z1**2))
C            (I*4)  ICT    = NUMBER OF X-SECT./ENERGY PAIRS
C            (I*4)  ITOUT  = NUMBER OF TEMPS.
C            (R*8)  XDAT() = X-SECTION DATA
C            (R*8)  EDAT() = ENERGY DATA
C            (R*8)  TDAT() = TEMPERATURE DATA
C            (I*4)  IOP    = USE DEFAULT SCALING PARAMS? (1 = YES, 0 = NO)
C            (L)    LPEND    - 'CANCEL' OR 'DONE' DECISION.
C
C  ROUTINES:
C         XXFLSH - CLEARS PIPE BUFFER
C
C  AUTHOR:
C         WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC., 7TH OCTOBER 1996
C
C  VERSION 1.1                                           DATE:  07/10/96
C  MODIFIED: WILLIAM OSBORN
C               - FIRST RELEASE. MODIFIED A5ISPF.FOR V1.2
C
C-----------------------------------------------------------------------
       INTEGER INDXREF, IZETAA(6,2), ISTDIM, NIGRP, NRGRP
       INTEGER NSHELA(2), NRESOA(2)
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80
       CHARACTER TITLE*40  , CAMETH*4
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       INTEGER I,J, PIPEIN, PIPEOU, IWHC, ILOGIC, ICTTEMP, ITOUTTEMP
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
       REAL*8 Z, Z0, Z1, XA(ISTDIM), YA(ISTDIM), CIA(2), CRA(2)
       REAL*8 EIONA(6,2), ENERA(6,2), WGHTA(6,2), ACIA, BCIA, ACRA, BCRA
       REAL*8 ES1(6), ES2(6), ER1(6), WR1(6), ER2(6), WR2(6)
       REAL*8 EDAT(ISTDIM), XDAT(ISTDIM), TDAT(ISTDIM), TOA(ISTDIM)
C-----------------------------------------------------------------------
       INTEGER IIFTYP, IIOTYP, IIFOUT, ICT, ITOUT, IOP, IASEL
       INTEGER INS1(6), ILS1(6), IZS1(6), INS2(6), ILS2(6), IZS2(6)
       INTEGER NA(6,2), LA(6,2)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

       WRITE(PIPEOU,*)ISTDIM
       CALL XXFLSH(PIPEOU)
       IF(CAMETH.EQ.'   A')THEN
           IWHC = 1
       ELSE
           IWHC = 2
       ENDIF
       WRITE(PIPEOU,*)IWHC
       CALL XXFLSH(PIPEOU)
       IF(IASEL.EQ.2)THEN
          IF(CAMETH.EQ.'   A')THEN
             WRITE(PIPEOU,*)Z
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)Z0
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)Z1
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)NIGRP
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)NRGRP
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)ICT
             CALL XXFLSH(PIPEOU)
             DO 10 I = 1,ICT
                WRITE(PIPEOU,*)XA(I)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)YA(I)
                CALL XXFLSH(PIPEOU)
C     
C     CONVERSION TO ALL UNITS NOW DONE IN IDL TO AVOID DUPLICATION WITH SPFMAN11
C     
 10          CONTINUE
             WRITE(PIPEOU,*)ITOUT
             CALL XXFLSH(PIPEOU)
             DO 20 J = 1,ITOUT
                WRITE(PIPEOU,*)TOA(J)
                CALL XXFLSH(PIPEOU)
 20          CONTINUE
             DO 30 J=1,NIGRP
                WRITE(PIPEOU,*)CIA(J)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)NSHELA(J)
                CALL XXFLSH(PIPEOU)                  
                DO 40 I=1,NSHELA(J)
                   WRITE(PIPEOU,*)NA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)LA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)EIONA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)IZETAA(I,J)
                   CALL XXFLSH(PIPEOU)
 40             CONTINUE
 30          CONTINUE
             DO 35 J=1,NRGRP
                WRITE(PIPEOU,*)CRA(J)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)NRESOA(J)
                CALL XXFLSH(PIPEOU)
                DO 50 I=1,NRESOA(J)
                   WRITE(PIPEOU,*)ENERA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)WGHTA(I,J)
                   CALL XXFLSH(PIPEOU)
 50             CONTINUE
 35          CONTINUE
          ELSE IF(CAMETH.EQ.'   B')THEN
C-----------------------------------------------------------------------
C     NO BURGESS OPTION WRITTEN YET
C-----------------------------------------------------------------------
          ENDIF
       ENDIF
C
C NOW READ ALTERATIONS FROM IDL PIPE FOR ALL POSSIBLE SELECTIONS
C
       READ(PIPEIN,*)ILOGIC
       IF(ILOGIC.EQ.1)THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
       ENDIF
C
       READ(PIPEIN,*)Z       
       READ(PIPEIN,*)Z0
       READ(PIPEIN,*)Z1
       READ(PIPEIN,*)ACIA
       READ(PIPEIN,*)BCIA
       READ(PIPEIN,*)ACRA
       READ(PIPEIN,*)BCRA
       READ(PIPEIN,*)NIGRP
       READ(PIPEIN,*)NRGRP
       DO 60 J=1,NIGRP
          READ(PIPEIN,*)NSHELA(J)
 60    CONTINUE
       DO 65 J=1,NRGRP
          READ(PIPEIN,*)NRESOA(J)
 65    CONTINUE
       IF(NIGRP.GT.0)THEN
          DO 70 I=1,NSHELA(1)
             READ(PIPEIN,*)INS1(I)
             READ(PIPEIN,*)ILS1(I)
             READ(PIPEIN,*)ES1(I)
             READ(PIPEIN,*)IZS1(I)
 70       CONTINUE
          IF(NIGRP.GT.1)THEN
             DO 80 I=1,NSHELA(2)
                READ(PIPEIN,*)INS2(I)
                READ(PIPEIN,*)ILS2(I)
                READ(PIPEIN,*)ES2(I)
                READ(PIPEIN,*)IZS2(I)
 80          CONTINUE
          ENDIF
       ENDIF
       IF(NRGRP.GT.0)THEN
          DO 90 I=1,NRESOA(1)
             READ(PIPEIN,*)ER1(I)
             READ(PIPEIN,*)WR1(I)
 90       CONTINUE
          IF(NRGRP.GT.1)THEN
             DO 100 I=1,NRESOA(2)
                READ(PIPEIN,*)ER2(I)
                READ(PIPEIN,*)WR2(I)
 100         CONTINUE
          ENDIF
       ENDIF
       READ(PIPEIN,*)IIFTYP
       READ(PIPEIN,*)IIOTYP
       READ(PIPEIN,*)IIFOUT
       READ(PIPEIN,*)ICTTEMP
       READ(PIPEIN,*)ITOUTTEMP
C     
       DO 110 I = 1,ICTTEMP
          READ(PIPEIN,*)EDAT(I)
          READ(PIPEIN,*)XDAT(I)
 110   CONTINUE
C
C FILL OUT OVERLAPS WITH ZEROS
C
       IF(ICTTEMP.LT.ISTDIM)THEN
          DO 120 I=ICTTEMP+1,ISTDIM
             EDAT(I) = 0.0
             XDAT(I) = 0.0
 120      CONTINUE
       ENDIF
C READ IN TEMPS
       DO 130 I = 1,ITOUTTEMP
          READ(PIPEIN,*)TDAT(I)
 130   CONTINUE
       IF(ITOUTTEMP.LT.ICT)THEN
          DO 140 I = ITOUTTEMP+1,ICT
             TDAT(I) = 0.0
 140      CONTINUE
       ENDIF
C
C RESTORE ICT,ITOUT
C
       ICT   = ICTTEMP
       ITOUT = ITOUTTEMP

C
C UNITS ALTERED TO FORTRAN INDEXING
C
       IIFTYP = IIFTYP+1
       IIOTYP = IIOTYP+1
       IIFOUT = IIFOUT+1

C READ IN DEFAULT SCALING OPTION

       READ(PIPEIN,*)IOP

       RETURN
       END
