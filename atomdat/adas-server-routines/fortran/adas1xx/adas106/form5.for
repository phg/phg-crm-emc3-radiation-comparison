CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/form5.for,v 1.1 2004/07/06 13:55:49 whitefor Exp $ Date $Date: 2004/07/06 13:55:49 $
CX
       FUNCTION FORM5(I,XI)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C                                                                       
C  ************  FORTRAN 77 PROGRAM:  FORM5    *************************
C                                                                       
C                                                                       
C  PURPOSE:
C
C	THIS PROGRAM IS NOT YET PROPERLY DOCUMENTED
C
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 07-10-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C
C-----------------------------------------------------------------------
       X=XI/(1.0D0-XI)
       GO TO (1,2,9,10,1,2,9,10,5,6,9,10),I
    1  FORM5=1.0D0
       GO TO 15
    2  FORM5=X*EEI(X)
       GO TO 15
    5  FORM5=X*X*EE2(X)
       GO TO 15
    6  FORM5=X*X*X*EE3(X)
       GO TO 15
    9  FORM5=1.0D0
       GO TO 15
   10  FORM5=0.0D0
   15  RETURN
      END
