CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/lspij3.for,v 1.2 2007/05/15 11:17:40 allan Exp $ Date $Date: 2007/05/15 11:17:40 $
CX
       SUBROUTINE LSPIJ3(N,H,W)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C                                                                       
C  ************  FORTRAN 77 SUBROUTINE: LSPIJ3 *************************
C                                                                       
C  PURPOSE: CALCULATE SPLINES WITH VARIOUS END CONDITIONS.
C
C  EXTENDED ARRAY DIMENSION VERSION OF NSPIJ3
C
C  USES LABELLED COMMON  /LSPL3/
C
C  CONDITIONS AT 1ST NODE AND NTH NODE CONTROLLED BY IEND1 AND IENDN
C      IEND=1 : SPECIFIED D LOG(Y) IE. DY/Y AT NODE STORED IN APPROPRIAT5070000
C               APPROPRIATE VECTOR
C          =2 : ZERO CURVATURE
C          =3 : CONSTANT CURVATURE
C          =4 : MATCHED TO SPECIFIED FUNCTIONAL FORM IN TERMS OF
C               TWO PARAMETERS A AND B SUCH THAT
C                        FUNCT = P(1)*A+Q(1)*B
C                   1ST DERIV. = P(2)*A+Q(2)*B
C                   2ND DERIV. = P(3)*A+Q(3)*B
C               WHERE A1,B1,P1,Q1 ARE USED FOR 1ST NODE AND
C               AN,BN,PN,QN FOR NTH NODE
C
C  INPUT
C      N=NUMBER OF KNOTS
C      H(I)=INTERVALS BETWEEN KNOTS
C  OUTPUT
C      W=SPLINE MATRIX
C
C AUTHOR:
C
C  ******* H.P. SUMMERS, JET           7 FEB 1989    ***************
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 07-10-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C
C VERSION: 1.2                          DATE: 15-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C
C----------------------------------------------------------------------
       DIMENSION A(40),B(40),C(40),H(40),W(40,40)
       COMMON /LSPL3/IEND1,IENDN,G1,GN,A1,B1,AN,BN,P1(3),Q1(3),
     &PN(3),QN(3),A1RY(40),B1RY(40),ANRY(40),BNRY(40)
       X=0.0
       X3=0.0
       H(N)=1.0D72
       DO 10 I=1,N
       DO 12 J=1,N
   12  W(I,J)=0.0
       Y=1.0/H(I)
       Y3=3.0*Y*Y
       A(I)=X
       B(I)=2.0*(X+Y)
       C(I)=Y
       IF(I.EQ.1)GO TO 13
       W(I,I-1)=-X3
   13  W(I,I)=X3-Y3
       IF(I.EQ.N)GO TO 14
       W(I,I+1)=Y3
   14  X=Y
   10  X3=Y3
       GO TO (50,65,55,60),IEND1
   50  C(1)=0.0D0
       W(1,1)=B(1)*G1
       W(1,2)=0.0D0
       GO TO 65
   55  C(1)=B(1)
       W(1,1)=-B(1)*B(1)
       W(1,2)=-W(1,1)
       GO TO 65
   60  DT1=1.0D0/(P1(1)*Q1(2)-P1(2)*Q1(1))
       T1=0.5D0*DT1*(P1(1)*Q1(3)-P1(3)*Q1(1))
       T2=0.5D0*DT1*(P1(3)*Q1(2)-P1(2)*Q1(3))
       B(1)=B(1)+T1
       W(1,1)=W(1,1)-T2
   65  GO TO (70,85,75,80),IENDN
   70  A(N)=0.0D0
       W(N,N)=B(N)*GN
       W(N,N-1)=0.0D0
       GO TO 85
   75  A(N)=B(N)
       W(N,N)=B(N)*B(N)
       W(N,N-1)=-W(N,N)
       GO TO 85
   80  DTN=1.0D0/(PN(1)*QN(2)-PN(2)*QN(1))
       T1=0.5D0*DTN*(PN(1)*QN(3)-PN(3)*QN(1))
       T2=0.5D0*DTN*(PN(3)*QN(2)-PN(2)*QN(3))
       B(N)=B(N)-T1
       W(N,N)=W(N,N)+T2
   85  H(N)=0.0
       DO 20 I=2,N
       X=A(I)/B(I-1)
       B(I)=B(I)-X*C(I-1)
       DO 15 J=1,I
   15  W(I,J)=W(I,J)-X*W(I-1,J)
   20  CONTINUE
       X=1.0/B(N)
       DO 23 I=1,N
   23  W(N,I)=X*W(N,I)
       DO 30 I=2,N
       K=N-I+1
       X=1.0/B(K)
       DO 25 J=1,N
   25  W(K,J)=X*(W(K,J)-C(K)*W(K+1,J))
   30  CONTINUE
       IF(IEND1.NE.4)GO TO 45
       DO 43 I=1,N
       A1RY(I)=-DT1*Q1(1)*W(1,I)
   43  B1RY(I)=DT1*P1(1)*W(1,I)
       A1RY(1)=A1RY(1)+DT1*Q1(2)
       B1RY(1)=B1RY(1)-DT1*P1(2)
   45  IF(IENDN.NE.4)RETURN
       DO 48 I=1,N
       ANRY(I)=-DTN*QN(1)*W(N,I)
   48  BNRY(I)=DTN*PN(1)*W(N,I)
       ANRY(N)=ANRY(N)+DTN*QN(2)
       BNRY(N)=BNRY(N)-DTN*PN(2)
       RETURN
       END
