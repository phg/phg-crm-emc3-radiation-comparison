CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/lsort.for,v 1.2 2007/05/15 11:17:43 allan Exp $ Date $Date: 2007/05/15 11:17:43 $
CX
       SUBROUTINE LSORT(XA,YA,N)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C                                                                       
C  ************  FORTRAN 77 PROGRAM:  LSORT *************************
C                                                                       
C                                                                       
C  PURPOSE: SORT AN ARRAY SO THAT FIRST INPUT IS IN INCREASING ORDER
C
C  LARGER ARRAY DIMENSION VERSION OF NSORT
C
C  N.B.  INPUT VALUES ARE ALTERED BY THIS ROUTINE !!!!
C
C  INPUT
C      XA(I)=X-VALUES
C      YA(I)=Y-VALUES
C      N=NUMBER OF VALUES
C  OUTPUT
C      XA(I)=SORTED X-VALUES
C      YA(I)=SORTED Y-VALUES
C
C
C AUTHOR:
C
C  ******** H.P. SUMMERS, JET               7 FEB 1989   ***********
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 07-10-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C
C VERSION: 1.2                          DATE: 15-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C
C-----------------------------------------------------------------------
       DIMENSION XA(40),YA(40)
       N1=N-1
       DO 10 I=1,N1
       I1=I+1
       DO 5 J=I1,N
       IF(XA(I).LE.XA(J))GO TO 5
       SWAP=XA(I)
       XA(I)=XA(J)
       XA(J)=SWAP
       SWAP=YA(I)
       YA(I)=YA(J)
       YA(J)=SWAP
    5  CONTINUE
   10  CONTINUE
       RETURN
      END
