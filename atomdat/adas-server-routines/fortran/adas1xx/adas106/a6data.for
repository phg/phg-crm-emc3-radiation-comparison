CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/a6data.for,v 1.1 2004/07/06 10:02:52 whitefor Exp $ Date $Date: 2004/07/06 10:02:52 $
CX
      SUBROUTINE A6DATA( DSFULL , INDXREF , TITLE , CAMETH , Z0    , Z ,
     &                   Z1     , NIGRP   , EMIN  , CIA    , NSHELA,
     &                   EIONA  , IZETAA  , NRGRP , CRA    , NRESOA,
     &                   ENERA  , WGHTA   , ICT   , XA     , YA    ,APA,
     &                   ITOUT  , TOA     , YOA   , YOAP   ,
     &                   ISTDIM , IREAD   , NA    , LA
     &     )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A6DATA *******************
C
C  PURPOSE: TO REFRESH A DATA INDEX FROM AN ADAS106 ARCHIVE. READS
C           IN THE INDEX CODE A-ADAS, B-BURGESS AND THE THE REST OF
C           THE DATA AS APPROPRIATE.
C
C  CALLING PROGRAM: 
C            ADAS106.FOR
C
C  INPUT:
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                              TWO TYPES OF ANALYSIS.
C                              A - ADAS, B- BURGESS
C            (I*4)  ISTDIM = ARRAY DIMENSIONS : MAX. NO OF VALUES THAT
C                            CAN BE READ IN
C            (I*4)  IREAD  = THE INPUT UNIT
C 
C  OUTPUTS: 
C            (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                              FILE.
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION         
C            (R*8)  Z      = INITIAL ION CHARGE                  
C            (R*8)  Z1     = FINAL ION CHARGE
C            (I*4)  NIGRP  = NO. OF SHELL GROUPS
C            (R*8)  EMIN   = MINIMUM ENERGY (?)
C            (R*8)  CIA()  = SCALING PARAMETERS FOR SHELL GROUPS
C            (I*4)  NSHELA()=NO. OF ENTRIES FOR EACH SHELL GROUP
C            (I*4)  NA(,)  = SHELL GROUP DATA : N
C            (I*4)  LA(,)  = SHELL GROUP DATA : L
C            (R*8)  EIONA(,)=SHELL GROUP DATA : EION(RYD)
C            (I*4)  IZETAA(,)=SHELL GROUP DATA : IZETA
C            (I*4)  NRGRP  = NO. OF RESONANCE GROUPS
C            (R*8)  CRA()  = SCALING PARAMETERS FOR RESONANCE GROUPS
C            (I*4)  NRESOA()=NO. OF ENTRIES FOR EACH RESONANCE GROUP
C            (R*8)  ENERA(,)=RESONANCE GROUP DATA : ENERGY(RYD)
C            (R*8)  WGHTA(,)=RESONANCE GROUP DATA : WEIGHT
C            (R*8)  ICT    = NO. OF ENERGY / X-SECTION PAIRS
C            (R*8)  XA()   = X, THRESHOLD PARAMETER RELATIVE TO FIRST IONIS. POT.
C            (R*8)  YA()   = Q/(1-1/X), Q=CROSS-SECTION ?
C            (R*8)  APA()  = QEM/(1-1/X), QEM=APPROX. X-SECTION )
C            (R*4)  YPA()  = Q/QEM
C            (I*4)  ITOUT  = NO. OF TEMPS.
C            (R*8)  TOA()  = TEMP (KELVIN)
C            (R*8)  YOA()  = S, MAXWELL AVERAGED IONISATION RATE COEFF.(CM^3 S^-1)
C            (R*8)  YOAP() = SEM, APPROXIMATE RATE COEFF.
C
C  ROUTINES: NONE
C
C  AUTHOR:   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC., 28TH AUG 1996
C
C  VERSION 1.1                                           DATE: 28-08-96
C  MODIFIED: WILLIAM OSBORN
C		- FIRST RELEASE
C
C-----------------------------------------------------------------------
      INTEGER INDXREF, NIGRP, NSHELA(2), IZETAA(6,2), NRGRP, NRESOA(2)
      INTEGER ICT, ISTDIM, ITOUT, I , I4UNIT, NA(6,2), LA(6,2)
      INTEGER ICHK1, IIGRP, ISHEL, IRGRP, IC, IREAD, IRESOA, IT
C-----------------------------------------------------------------------
      CHARACTER DSFULL*80 , CSTRING*80
      CHARACTER TITLE*40  , CAMETH*4
C-----------------------------------------------------------------------
      REAL*8 Z0, Z1, Z, CIA(2), EIONA(6,2), CRA(2), ENERA(6,2)
      REAL*8 XA(ISTDIM), YA(ISTDIM), APA(ISTDIM), TOA(ISTDIM)
      REAL*8 WGHTA(6,2), YOA(ISTDIM), YOAP(ISTDIM), EMIN
      REAL*8 Y
C-----------------------------------------------------------------------
      REAL*4 YPA
C-----------------------------------------------------------------------
      
      OPEN( UNIT=IREAD , FILE = DSFULL , STATUS = 'UNKNOWN' )
      
C-----------------------------------------------------------------------
C     READ THE INFORMATION STRING WITH INDEX NUMBER AT THE TOP OF EACH RECORD
C-----------------------------------------------------------------------
      READ(IREAD,1000)CSTRING
 1    IF(CSTRING(1:1).EQ.'I')THEN
         READ(CSTRING,1001)ICHK1
         IF(ICHK1.NE.INDXREF) THEN
            READ(IREAD,1000)CSTRING
            GOTO 1
         ENDIF
      ELSE
         IF(CSTRING(1:1).EQ.'C')THEN
        WRITE(I4UNIT(-1),*)'**************** A6DATA MESSAGE **********'
        WRITE(I4UNIT(-1),'(A,I2)')' NOT FOUND ARCHIVE NUMBER ',INDXREF
        WRITE(I4UNIT(-1),'(A,I2)')' USING LAST ARCHIVE, NUMBER',ICHK1
        WRITE(I4UNIT(-1),*)'**************** A6DATA MESSAGE **********'
            INDXREF = ICHK1
C CLOSE FILE AND START AGAIN
            CLOSE(UNIT=IREAD)
            OPEN( UNIT=IREAD , FILE = DSFULL , STATUS = 'UNKNOWN' )
         ENDIF
         READ(IREAD,1000)CSTRING
         GO TO 1
      ENDIF
C     
      READ(CSTRING,1002)ICHK1,TITLE,CAMETH 
      
      IF(CAMETH.EQ.'   A')THEN
C-----------------------------------------------------------------------
C     READ IN ADAS FORMAT DATA - NO OTHER FORMAT ACCEPTED YET
C-----------------------------------------------------------------------
         READ(IREAD,1003) Z0, Z, Z1                                   
         READ(IREAD,1000) CSTRING
         IF(CSTRING(2:2).EQ.'D')THEN
            READ(CSTRING,1004) NIGRP,EMIN
            READ(IREAD,1000) CSTRING
            DO 2 IIGRP=1,NIGRP                                    
               READ(CSTRING,1005)I,CIA(IIGRP)
               ISHEL=0
 3             READ(IREAD,1000)CSTRING
               IF(CSTRING(6:6).EQ.' ')THEN
                  ISHEL=ISHEL+1
                  READ(CSTRING,1006)NA(ISHEL,IIGRP),LA(ISHEL,IIGRP),
     &                 EIONA(ISHEL,IIGRP),IZETAA(ISHEL,IIGRP)
                  GOTO 3
               ENDIF
               NSHELA(IIGRP)=ISHEL
 2          CONTINUE
         ELSE
            NIGRP=0
            READ(IREAD,1000) CSTRING
         ENDIF
         IF(CSTRING(2:2).EQ.'E')THEN
            READ(CSTRING,1007)NRGRP
            READ(IREAD,1000)CSTRING
            DO 4 IRGRP=1,NRGRP
               READ(CSTRING,1008)I,CRA(IRGRP)
               IRESOA=0
 5             READ(IREAD,1000)CSTRING
               IF(CSTRING(6:6).EQ.' ')THEN
                  IRESOA=IRESOA+1
                  READ(CSTRING,1009)ENERA(IRESOA,IRGRP)
     &                 ,WGHTA(IRESOA,IRGRP)         
                  GOTO 5
               ENDIF
               NRESOA(IRGRP)=IRESOA
 4          CONTINUE                                                      
         ELSE
            NRGRP=0
         ENDIF

C-----------------------------------------------------------------------
C     READ IN X, Q(PI*A0**2), Q/(1-1/X), ETC.
C-----------------------------------------------------------------------
         IC=0
 6       READ(IREAD,1000)CSTRING
         IF(CSTRING(5:5).NE.'T')THEN
            IC=IC+1
            READ(CSTRING,1010)XA(IC), Y, YA(IC), APA(IC), YPA
            GOTO 6
         ENDIF
         ICT=IC
C         READ(IREAD,1000)CSTRING
         
C-----------------------------------------------------------------------
C     READ IN TE(K), TE(EV), S, ETC.
C-----------------------------------------------------------------------
         IT=0
 7       READ(IREAD,1000)CSTRING
         IF(CSTRING(1:2).NE.'-1')THEN
            IT=IT+1
            READ(CSTRING,1011)TOA(IT), YOA(IT),YOAP(IT)
            GOTO 7
         ENDIF
         ITOUT = IT
      ENDIF
      CLOSE( UNIT=IREAD , STATUS = 'KEEP')

      RETURN

C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1001 FORMAT(1X,I3,76X)
 1002 FORMAT(1X,1I3,2X,1A40,24X,1A4)
 1003 FORMAT(18X,1F5.1,24X,1F5.1,22X,1F5.1)
 1004 FORMAT(28X,I2,9X,1F10.5)
 1005 FORMAT(10X,I2,8X,1F8.3,24X)
 1006 FORMAT(36X,2I3,F10.5,I5)
 1007 FORMAT(30X,1I2)
 1008 FORMAT(10X,I2,8X,F8.3,24X)
 1009 FORMAT(36X,F10.5,F7.3)
 1010 FORMAT(1X,F10.5,3D15.5,F10.5)
 1011 FORMAT(1X,3D12.4)
C-----------------------------------------------------------------------

      END
