CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/adas106.for,v 1.1 2004/07/06 10:09:47 whitefor Exp $ Date $Date: 2004/07/06 10:09:47 $
CX
       PROGRAM ADAS106
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN 77 PROGRAM: ADAS106 *********************
C
C  PURPOSE: COLLECTION FROM ARCHIVE OR ENTRY OF IONISATION RATE COEFFTS.
C           FOR EXAMINATION, INTERPOLATION, 
C           COMPARISON WITH APPROXIMATE FORMS, AND GRAPH EDITING VIA
C           COMMUNICATION WITH IDL INTERFACE.
C
C  DATA:
C           DATA IS ENTERED VIA THE IDL INTERFACE, BUT CAN ALSO BE 
C           RECOVERED FROM THE USERS' OWN ARCHIVE FILES. THESE 
C           TYPICALLY RESIDE IN '/DISK2/YOURNAME/ADAS/ARCH106'.
C           PARTICULAR TRANSITION DATASETS HAVE THEIR OWN INDEX
C           NUMBER AND THIS IS ENTERED ON THE FIRST PANEL AFTER
C           SELECTING 'REFRESH FROM OLD ARCHIVE'. THERE IS NO
C           CENTRAL ADAS ARCHIVE.
C  
C           ARCHIVE FILES FOLLOW THE FOLLOWING NAMING CONVENTION;
C           <PRODUCER ID><YEAR><MEMBER>_<FILE NUMBER>.DAT
C
C           THE MEMBER NAME IS CONSTRUCTED AS;
C           <ELEMENT><SPECTROSCOPIC EFFECTIVE ION CHARGE>
C
C           E.G.        'DHB95FE19_001.DAT'
C           DHB = DAVID H.BROOKS
C           95  = 1995
C           FE  = IRON
C           19  = Z+1      I.E. FE+18 IS THE ION IN THIS CASE
C           001 = FILE NUMBER
C  PROGRAM:
C          (I*4)  INDXREF= THE INDEX NUMBER TO REFRESH DATA FROM.
C          (I*4)  IGRPSCAL1 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             IST GRAPH.
C          (I*4)  IGRPSCAL2 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             2NT GRAPH.
C          (I*4)  ITEXOUT= HARD COPY OF TEXT OUTPUT REQUEST SWITCH.
C                          0 - NO
C                          1 - YES
C          (C*80) DSFULL = THE SELECTED ARCHIVE NAME.
C          (C*3)  REP    = 'YES' - FINISH PROGRAM RUN
C                          'NO ' - CONTINUE
C          (C*40) TITLE  = THE INFORMATION LINE IN THE ARCHIVE
C                          FILE.
C          (C*4)  CAMETH = THE TAG TO DISTINGUISH BETWEEN THE
C                          TWO TYPES OF ANALYSIS.
C                          A - ADAS, B- BURGESS
C          (C*8)  DATE   = THE CURRENT DATA 
C          (C*80) CHEADER= THE ADAS HEADER FOR THE PAPER.TXT FILE
C          (C*80) TEXDSN = THE DEFAULT HARD COPY FILE NAME
C          (C*80) DSARCH = THE ARCHIVE FILE NAME
C          (C*40) CGTIT  = THE GRAPH TITLE (INFORMATION FOR ANY
C                          NEW ARCHIVE)
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION         
C            (R*8)  Z      = INITIAL ION CHARGE                  
C            (R*8)  Z1     = FINAL ION CHARGE
C            (I*4)  NIGRP  = NO. OF SHELL GROUPS
C            (R*8)  EMIN   = MINIMUM ENERGY (?)
C            (R*8)  CIA()  = SCALING PARAMETERS FOR SHELL GROUPS
C            (I*4)  NSHELA()=NO. OF ENTRIES FOR EACH SHELL GROUP
C            (R*8)  EIONA(,)=SHELL GROUP DATA : EION(RYD)
C            (I*4)  IZETAA(,)=SHELL GROUP DATA : IZETA
C            (I*4)  NRGRP  = NO. OF RESONANCE GROUPS
C            (R*8)  CRA()  = SCALING PARAMETERS FOR RESONANCE GROUPS
C            (I*4)  NRESOA()=NO. OF ENTRIES FOR EACH RESONANCE GROUP
C            (R*8)  ENERA(,)=RESONANCE GROUP DATA : ENERGY(RYD)
C            (R*8)  WGHTA(,)=RESONANCE GROUP DATA : WEIGHT
C            (R*8)  ICT    = NO. OF ENERGY / X-SECTION PAIRS
C            (R*8)  XA()   = X, THRESHOLD PARAMETER RELATIVE TO FIRST IONIS. POT.
C            (R*8)  YA()   = Q/(1-1/X), Q=CROSS-SECTION ?
C            (R*8)  APA()  = QEM/(1-1/X), QEM=APPROX. X-SECTION )
C            (I*4)  ITOUT  = NO. OF TEMPS.
C            (R*8)  TOA()  = TEMP (KELVIN)
C            (R*8)  YOA()  = S, MAXWELL AVERAGED IONISATION RATE COEFF.(CM^3 S^-1)
C            (R*8)  YOAP() = SEM, APPROXIMATE RATE COEFF.
C          (L)    LPEND  = CANCEL OR DONE FLAG
C          (L)    LARCH  = ARCHIVING SELECTION OPTION
C          (R*8)  EDAT   = USER SELECTED INPUT ENRGIES
C          (R*8)  XDAT   = USER SELECTED INPUT OMEGAS
C          (R*8)  TDAT   = USER SELECTED OUTPUT TEMPERATURES
C          (I*4)  ISTOP  = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END TO
C                          THE PROGRAM
C          (I*4)  IASEL  = ARCHIVING OPTION:
C                          0 = IGNORE ARCHIVING.
C                          1 = EXAMINE OLD ARCHIVE AND POSSIBLY
C                              ADD NEW DATA.
C                          2 = REFRESH TRANSITION DATA FROM 
C                              OLD ARCHIVE (REQUIRES INDEX NO.)
C                          3 = CREATE A NEW ARCHIVE.
C  ROUTINES: (COMPLETE LIST FOR ADAS106 - NOT NECESSARILY 
C             CALLED BY THIS PROGRAM)
C           NAME             BRIEF DESCRIPTION
C           -------------------------------------------------------
C           A6SPF0    GATHERS INPUT ARCHIVE NAME VIA IDL PIPE
C           A6DATA    READS SELECTED DATA FROM ARCHIVE
C           A6ISPF    PIPES DATA TO IDL READS USER PROCESSING EDITS 
C           A6SPF1    GATHERS USER SELECTED OUTPUT OPTIONS
C           SPFMAN12  PERFORMS ADAS PROCESSING (GAMMA EVALUATION)
C           A6OUTG   SENDS DATA TO IDL GRAPH EDITOR
C           A6OUT0    WRITES DATA TO ARCHIVE/TEXT OUTPUT FILES
C           XX0000    SETS MACHINE DEPENDENT ADAS CONFIGURATION
C           XXDATE    PROVIDES CURRENT DATE
C           XXSLEN    REMOVES SPACES FROM A STRING
C           I4UNIT    FETCHES MESSAGE OUTPUT UNIT NUMBER
C           XXFLSH    CLEARS IDL PIPE BUFFER
C           FORM5     INTERNAL TO SPFMAN12
C
C  AUTHOR:  WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C  DATE:    7TH OCTOBER 1996
C     
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1                                        DATE: 28/08/96
C  MODIFIED:  WILLIAM OSBORN
C             FIRST WRITTEN USING ADAS105.FOR V1.2 AS A TEMPLATE
C
C-----------------------------------------------------------------------
       INTEGER INDXREF  , ICT      , ITOUT
       INTEGER IASEL    , IUNT07
       INTEGER IGRPOUT
       INTEGER IGRPSCAL1, IGRPSCAL2, ITEXOUT
       INTEGER PIPEIN   , ISTOP
       INTEGER ISTDIM   , INDIM
       INTEGER NIGRP    , NRGRP    , NSHELA(2) , NRESOA(2) , IZETAA(6,2)
       INTEGER IREAD    , INS1(6)  , ILS1(6)   , IZS1(6)   , INS2(6)
       INTEGER ILS2(6)  , IZS2(6)
       INTEGER IIFTYP   , IIOTYP   , IIFOUT    , IOP
       INTEGER NITHR    , NRTHR    , NA(6,2)   , LA(6,2)
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80 , CAMETH*4
       CHARACTER TITLE*40  , DATE*8     , REP*3
       CHARACTER CGTIT*40  , CHEADER*80 , TEXDSN*80
       CHARACTER IXTIT*40  , DSARCH*80
C-----------------------------------------------------------------------
       PARAMETER( IUNT07 = 7 , PIPEIN = 5, IREAD = 9)
       PARAMETER( ISTDIM = 40, INDIM = 40)
C-----------------------------------------------------------------------
       REAL*8  Z , Z0 , Z1 , EMIN, CIA(2), CRA(2), EIONA(6,2)
       REAL*8  ENERA(6,2) , WGHTA(6,2) , XA(ISTDIM) , YA(ISTDIM)
       REAL*8  APA(ISTDIM), TOA(ISTDIM), YOA(ISTDIM)
       REAL*8  YOAP(ISTDIM)
       REAL*8  ACIA, BCIA, ACRA, BCRA, ES1(6), ES2(6), ER1(6), ER2(6)
       REAL*8  WR1(6), WR2(6) , XDAT(ISTDIM), EDAT(ISTDIM), TDAT(ISTDIM)
       REAL*8  ASCL
C-----------------------------------------------------------------------
       REAL*4  XP(12), YP(12)
C-----------------------------------------------------------------------
       LOGICAL OPEN7 , LARCH , LPEND
C-----------------------------------------------------------------------
       DATA OPEN7/.FALSE./
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
       CGTIT = ' '
       TITLE = ' '
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
       CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
       CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
  100  CONTINUE
C-----------------------------------------------------------------------
C  IF FILES ARE ACTIVE ON UNIT 7 - CLOSE IT
C-----------------------------------------------------------------------
       IF (OPEN7) CLOSE(7)
       OPEN7=.FALSE.
C
C-----------------------------------------------------------------------
C GET ARCHIVE NAME FROM IDL
C-----------------------------------------------------------------------
C
       CALL A6SPF0( REP     , DSFULL  ,
     &              IASEL   , INDXREF )
C       WRITE(0,*)'REP,DSFULL,IASEL,INDXREF',REP, DSFULL, IASEL, INDXREF
C
C-----------------------------------------------------------------------
C  IF PROGRAM RUN COMPLETED END  
C-----------------------------------------------------------------------
C
       IF (REP.EQ.'YES') THEN
           GOTO 999
       ENDIF
C
C-----------------------------------------------------------------------
C OPEN ARCHIVE AND READ IF REFRESHING
C-----------------------------------------------------------------------
C
       IF (IASEL.EQ.2) THEN
          CALL A6DATA(  DSFULL , INDXREF , TITLE , CAMETH , Z0     , Z ,
     &                  Z1     , NIGRP   , EMIN  , CIA    , NSHELA ,
     &                  EIONA  , IZETAA  , NRGRP , CRA    , NRESOA ,
     &                  ENERA  , WGHTA   , ICT   , XA     , YA     ,APA,
     &                  ITOUT  , TOA     , YOA   , YOAP   ,
     &                  ISTDIM , IREAD   , NA    , LA
     &         )

       ENDIF
C
C-----------------------------------------------------------------------
C
  200  CONTINUE
C
C-----------------------------------------------------------------------
C READ USER PROCESSING OPTIONS FROM IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
       CALL A6ISPF( DSFULL , INDXREF, TITLE , CAMETH , Z0    ,
     &              Z      , Z1     , NIGRP , NRGRP  ,
     &              ACIA   , BCIA   , ACRA  , BCRA   ,
     &              NSHELA , NRESOA , INS1  , ILS1   , ES1   , IZS1 ,
     &              INS2   , ILS2   , ES2   , IZS2   , ER1   , ER2  ,
     &              WR1    , WR2    , IIFTYP, IIOTYP , IIFOUT, ICT  ,
     &              ITOUT  , XDAT   , EDAT  , TDAT   ,     
     &              IOP    ,
     &              XA     , YA     , CIA   , CRA    , NA    , LA   ,
     &              EIONA  , IZETAA , ENERA , WGHTA  , TOA   , IASEL,
     &              ISTDIM , LPEND
     &            )
C       WRITE(0,*)'Z0,Z,Z1,NIGRP,NRGRP',Z0,Z,Z1,NIGRP,NRGRP
C       WRITE(0,*)'ACIA,BCIA,ACRA,BCRA',ACIA,BCIA,ACRA,BCRA
C       WRITE(0,*)'NSHELA,NRESOA',NSHELA,NRESOA
C       WRITE(0,*)'INS1,ILS1,ES1,IZS1',(INS1(I),ILS1(I),ES1(I),IZS1(I),
C     &      I=1,6)
C       WRITE(0,*)'INS2,ILS2,ES2,IZS2',(INS2(I),ILS2(I),ES2(I),IZS2(I),
C     &      I=1,6)
C      WRITE(0,*)'ER1,ER2,WR1,WR2',(ER1(I),ER2(I),WR1(I),WR2(I),I=1,6)
C       WRITE(0,*)'IIFTYP, IIOTYP, IIFOUT, ICT, ITOUT, IOP',IIFTYP, 
C     &      IIOTYP, IIFOUT, ICT, ITOUT, IOP
C       WRITE(0,*)'XDAT',XDAT
C       WRITE(0,*)'EDAT',EDAT
C       WRITE(0,*)'TDAT',TDAT
C     
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 999
C
C-----------------------------------------------------------------------
C
       IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C READ USER OUTPUT OPTIONS FROM IDL VIA UNIX PIPE  
C-----------------------------------------------------------------------
C
 300   CONTINUE
C
C-----------------------------------------------------------------------
C
       CALL A6SPF1( LPEND   , IGRPOUT   ,
     &              IGRPSCAL1, IGRPSCAL2, ITEXOUT   ,
     &              CGTIT   , CHEADER   , TEXDSN  , ASCL
     &             )

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 999
C
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 200
C
C-----------------------------------------------------------------------
C PROCESS ANALYSIS OPTION 
C-----------------------------------------------------------------------

      CALL SPFMAN12 ( Z0    , Z     , Z1    , NIGRP , NRGRP , TITLE,
     &     ACIA  , BCIA  , ACRA  , BCRA  ,
     &     NSHELA, NRESOA, INS1  , ILS1  , ES1   , IZS1 ,
     &     INS2  , ILS2  , ES2   , IZS2  , ER1   , ER2  ,
     &     WR1   , WR2   , IIFTYP, IIOTYP, IIFOUT, ICT  ,
     &     ITOUT , XDAT  , EDAT  , TDAT  ,     
     &     IOP   , ASCL  , 
     &     XA    , YA    , APA   , XP    , YP    ,
     &     TOA   , YOA   , YOAP  , NITHR , NRTHR , CIA  , CRA,
     &     NA    , LA    , EIONA , IZETAA, ENERA , WGHTA , EMIN
     &     )

C-----------------------------------------------------------------------
C SEND GRAPHICAL INFO TO IDL AND RECALCULATE VALUES IF GRAPH HAS BEEN
C EDITED
C-----------------------------------------------------------------------

      IF (IGRPOUT.EQ.1)THEN
         CALL A6OUTG( XA    , YA    , APA   , XP    , YP   , TOA  ,
     &        YOA   , YOAP  , ICT   , ITOUT , INDIM , NITHR , NRTHR ,
     &        LPEND , LARCH , DSARCH, IASEL , ASCL  ,
     &        EDAT  , XDAT  , ISTDIM, CIA   , CRA   , Z ,
     &        NIGRP , NRGRP , EIONA , IZETAA, NSHELA,
     &        NRESOA, Z1    , ENERA , WGHTA , IIFTYP , IIOTYP, EMIN
     &        )
C         ICT = ICTN
C         ITOUT = ITOUTN
C     
C-----------------------------------------------------------------------
C     READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C     
         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 999
      ENDIF 
      
C     
C-----------------------------------------------------------------------
C     
      IF (LPEND) GOTO 300
C     
C-----------------------------------------------------------------------
C     IF NO ARCHIVING AND COMPLETE, GO BACK TO OUTPUT OPTIONS
C-----------------------------------------------------------------------
C     
      IF (.NOT.LARCH.AND.ITEXOUT.NE.1)THEN
         GOTO 300
      ENDIF
C
C-----------------------------------------------------------------------
C COPY DSARCH TO DSFULL IF ARCHIVING SELECTED AFTER PICKING NO ARCHIVE
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
         IF (IASEL.EQ.0)THEN
           DSFULL = DSARCH
           IASEL = 3
         ENDIF
       ENDIF
C
C-----------------------------------------------------------------------
C PUT DATA IN ARCHIVE FILE
C-----------------------------------------------------------------------
C
       IF (IASEL.NE.0)THEN
           IF (IASEL.EQ.2)THEN
              IXTIT = TITLE
           ELSE
              IXTIT = CGTIT
           ENDIF
           CALL A6OUT0( DSFULL , INDXREF , IXTIT , Z0     , Z     ,
     &     Z1     , NIGRP   , EMIN  , CIA    , NSHELA, NA  , LA   ,
     &     EIONA  , IZETAA  , NRGRP , CRA    , NRESOA,
     &     ENERA  , WGHTA   , ICT   , XA     , YA    , APA    ,
     &     ITOUT  , TOA     , YOA   , YOAP   ,
     &     ISTDIM , LARCH   , DATE  , IASEL  , TEXDSN, ITEXOUT,
     &     CHEADER
     &                  )
       ENDIF
C
C-----------------------------------------------------------------------
C

C-----------------------------------------------------------------------
C GO BACK TO OUTPUT OPTIONS SCREEN
C-----------------------------------------------------------------------

      GOTO 300

 999   STOP
       END
