CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/lfasym.for,v 1.2 2007/05/15 11:17:50 allan Exp $ Date $Date: 2007/05/15 11:17:50 $
CX
       SUBROUTINE LFASYM(X,XA,N,YA,Y,DY,C1,C2,C3,C4,FORM,IFORMS)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C                                                                       
C  ************  FORTRAN 77 SUBROUTINE: LFASYM *************************
C                                                                       
C  PURPOSE: TO PROVIDE A SPLINE INTERPOLATE MAKING USE OF SPECIFIED
C  ASYMPTOTIC BEHAVIOUR
C
C  LARGER ARRAY DIMENSION VERSION OF NFASYM
C
C  USES LABELLED COMMON /LSPL3/
C
C  INPUT
C      X=REQUIRED X-VALUE
C      X(I)=KNOTS
C      N=NUMBER OF KNOTS
C      C1(I,J)=1ST SPINE COEFFICIENT PRECURSOR
C      C2(I,J)=2ND SPINE COEFFICIENT PRECURSOR
C      C3(I,J)=3RD SPINE COEFFICIENT PRECURSOR
C      C4(I,J)=4TH SPINE COEFFICIENT PRECURSOR
C      FORM=EXTERNAL FUNCTION SPECIFYING ASYMPTOTIC FORMS
C      IFORMS=INDEX OF REQUIRED FORM
C  OUTPUT
C      Y=RETURNED Y-VALUE
C      DY=RETURNED DERIVATIVE
C
C AUTHOR:
C
C  ********** H.P. SUMMERS, JET             7 FEB 1989   ***********
C-----------------------------------------------------------------------
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 07-10-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C
C VERSION: 1.2                          DATE: 15-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C
C----------------------------------------------------------------------
       DIMENSION XA(40),YA(40),C1(40,39),C2(40,39),C3(40,39),C4(40,39)
       COMMON /LSPL3/IEND(2),G(2),AB(4),PQ(12),ABRY(160)
       DY=0.0D0
       IF(X.GE.XA(1))GO TO 5
       IENDS=1
       IN=1
       GO TO 15
    5  IF(X.LE.XA(N))GO TO 10
       IENDS=2
       IN=N
       GO TO 15
   10  CALL LFITSP(X,XA,N,YA,Y,DY,0,C1,C2,C3,C4,0)
       RETURN
   15  K=IEND(IENDS)
       GOTO(20,30,30,40),K
   20  DY=G(IENDS)*YA(IN)
   25  Y=YA(IN)+(X-XA(IN))*DY
       RETURN
   30  XX=XA(IN)
       CALL LFITSP(XX,XA,N,YA,Y,DY,0,C1,C2,C3,C4,0)
       IF(IEND(IENDS).EQ.2)GO TO 25
       IF(IENDS.EQ.2)GO TO 35
       H=XA(2)-XA(1)
       DDY=2.0D0*(-YA(1)+YA(2)-H*DY)/(H*H)
       GO TO 38
   35  H=XA(N)-XA(N-1)
       DDY=2.0D0*(YA(N-1)-YA(N)+H*DY)/(H*H)
   38  DX=X-XA(IN)
       Y=YA(IN)+DX*DY+0.5D0*DX*DX*DDY
       RETURN
   40  A=0.0D0
       B=0.0D0
       DO 42 I=1,N
       A=A+ABRY(80*IENDS-80+I)*YA(I)
   42  B=B+ABRY(80*IENDS-40+I)*YA(I)
       J=4*IFORMS+2*IENDS-5
       Y=A*FORM(J,X)+B*FORM(J+1,X)
       RETURN
      END
