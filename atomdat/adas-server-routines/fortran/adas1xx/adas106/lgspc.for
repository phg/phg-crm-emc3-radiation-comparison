CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas106/lgspc.for,v 1.2 2007/05/15 11:17:44 allan Exp $ Date $Date: 2007/05/15 11:17:44 $
CX
       SUBROUTINE LGSPC(XA,N,C1,C2,C3,C4)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C                                                                       
C  ************  FORTRAN 77 SUBROUTINE: LGSPC  *************************
C                                                                       
C  PURPOSE: GENERATE PRECURSORS OF SPLINE COEFFICIENTS SUITABLE FOR BOTH 
C  FORWARD AND BACKWARD INTERPOLATION
C
C  LARGER ARRAY DIMENSION VERSION OF NGSPC
C
C  INPUT
C      XA(I)=SET OF KNOTS
C      N=NUMBER OF KNOTS  (N.LE.20)
C  OUTPUT
C      C1(I,J)=1ST SPLINE COEFFICIENT PRECURSOR
C      C2(I,J)=2ND SPLINE COEFFICIENT PRECURSOR
C      C3(I,J)=3RD SPLINE COEFFICIENT PRECURSOR
C      C4(I,J)=4TH SPLINE COEFFICIENT PRECURSOR
C
C AUTHOR:
C
C  ******* H.P.SUMMERS, JET         7 FEB 1989   **********
C
C UNIX-IDL CONVERSION:
C
C VERSION: 1.1                          DATE: 07-10-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST CONVERTED.
C
C VERSION: 1.2                          DATE: 15-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C
C----------------------------------------------------------------------
       DIMENSION HA(40),XA(40),W(40,40),C1(40,39)
       DIMENSION C2(40,39),C3(40,39),C4(40,39)
       DO 3 J=2,N
       J1=J-1
    3  HA(J1)=XA(J)-XA(J1)
       IF(N-2)4,5,5
    4  C1(1,1)=1.0
       C2(1,1)=0.0
       C3(1,1)=0.0
       C4(1,1)=0.0
       GO TO 20
    5  CALL LSPIJ3(N,HA,W)
       DO 10 J=2,N
       J1=J-1
       H=HA(J1)
       H1=1.0/H
       H2=H1*H1
       H3=H1*H2
       DO 8 I=1,N
       U1=H*(W(J1,I)+W(J,I))
       U2=H*(W(J1,I)-W(J,I))
       X=0.0
       IF(J1.EQ.I)X=1.0
       IF(J.EQ.I)X=-1.0
       C1(I,J1)=0.5*X*X+0.125*U2
       C2(I,J1)=-H1*(1.5*X+0.25*U1)
       C3(I,J1)=-0.5*H2*U2
    8  C4(I,J1)=H3*(2.0*X+U1)
   10  CONTINUE
   20  CONTINUE
       RETURN
       END
