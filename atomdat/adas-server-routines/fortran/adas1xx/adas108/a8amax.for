CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8amax.for,v 1.2 2004/07/09 15:57:51 allan Exp $ Date $Date: 2004/07/09 15:57:51 $
CX
       subroutine a8amax( ixtyp , ibpts , idiff ,
     &                    s     , eij   , wi    , wj    ,   
     &                    bxc   , bpxc  , fxc1  , 
     &                    fxc2  , fxc3  , xkc   , 
     &                    ict   , xsa   , ysa   ,
     &                    itout , toa   , goa   , apgoa , excra ,
     &                    dexcra, gbarfa
     &                   )  
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran77 subroutine a8amax *******************
c
c  purpose: to perform Maxwellian averages of collision strengths for 
c           adas interpolative fit to neutrals  
c
c  calling program:  adas108.for
c
c  input:
c            (i*4)  ixtyp    = type of transition (1=dipole,2=non-dipole
c                              non-spin change, 3=spin change, 4=null)
c            (i*4)  ibpts    = bad point switch (0=normal, 1=bad.pt)
c            (i*4)  idiff    = difference switch (0=ratio, 1=diff)
c            (r*8)  s        = line strength for dipole case 
c            (r*8)  eij      = transition energy (rydberg) 
c            (r*8)  wi       = lower level statistical weight 
c            (r*8)  wj       = upper level statistical weight 
c            (r*8)  bxc      = threshold form parameter
c            (r*8)  bpxc     = matching parameter
c            (r*8)  fxc1     = threshold form parameter
c            (r*8)  fxc2     = asymptotic form parameter
c            (r*8)  fxc3     = asymptotic form parameter
c            (i*4)  ict      = length of xsa and ysa value set
c            (r*8)  xsa()    = independent (energy) coord. for spline
c            (r*8)  ysa()    = dependent (coll. str.) coord. for spline
c  output: 
c            (i*4)  itout    = length of toa, goa value set 
c            (r*8)  toa()    = output temperatures (K)
c            (r*8)  goa()    = output upsilons
c            (r*8)  apgoa()  = output approximate form upsilons
c            (r*8)  excra()  = output excitation rate coefficients
c            (r*8)  dexcra() = output de-excitation rate coefficients
c            (r*8)  gbarfa() = output gbar*f coefficients
c 
c
c  routines: 
c             egasym	adas	generates asyptotic spline conditions
c	      egspc	adas    generates spline coefficients
c             elnfit    adas    obtains linearly interpolated value
c             efasym    adas    obtains spline interpolated value
c             a8gamg    adas    calculates incomplete gamma function
c             eei       adas    exponential integral exp(x)*e1(x)
c             ee2       adas    exponential integral exp(x)*e2(x)  
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     25/06/99
c  modified: Hugh Summers
c		- first release
c
C  DATE:   07/07/2004			VERSION: 1.2
C  MODIFIED: ALLAN WHITEFORD
C		- CHANGED PARAMS108 TO PARAMS
C
c-----------------------------------------------------------------------
       include  'PARAMS'
c------------------------------------------------------------------------
       integer  ixtyp , ibpts , idiff
       integer  ict   , itout 
       integer  nt    , ng
       integer  iends , iend1 , iendn
       integer  ithrsc, iasymc, it    , k
       integer  i4unit 
c-----------------------------------------------------------------------
       real*8   s     , eij   , wi    , wj    ,   
     &          bxc   , bpxc  ,fxc1   , 
     &          fxc2  , fxc3  , xkc    
       real*8   form4 , te    , ate   ,
     &          g1    , gn    , x1    , dx1   , xn    , dxn 
       real*8   x     , xs    , y     , ys    , dys   , apy    ,
     &          apgt  , apga  , xe    , sumt  , suma  ,
     &          sumtp , sumap , apg   , ee2v
       real*8   eei   , ee2   , a8gamg
       real*8   a1    , b1    , an    , bn   
c-----------------------------------------------------------------------
       real*8   xsa(istdim) , ysa(istdim)  
       real*8   c1(istdim,istdim-1)       , c2(istdim,istdim-1)      ,
     &          c3(istdim,istdim-1)       , c4(istdim,istdim-1) 
       real*8   toa(istdim)   , goa(istdim)   , apgoa(istdim)        ,
     &          excra(istdim) , dexcra(istdim), gbarfa(istdim)  
       real*8   xt(20)      , wt(20)      , xg(20)      , wg(20)  
       real*8   p1(3)       , q1(3)       , pn(3)       , qn(3)      ,
     &          a1ry(istdim)     , b1ry(istdim)   ,
     &          anry(istdim)     , bnry(istdim)
c-----------------------------------------------------------------------
       common /espl3/iend1,iendn,g1,gn,a1,b1,an,bn,p1,q1,pn,qn,
     &               a1ry,b1ry,anry,bnry
c-----------------------------------------------------------------------
       external form4                                                   
c-----------------------------------------------------------------------
c  20 point gauss sqrt(x) weight quadrature for threshold region 
c-----------------------------------------------------------------------
      data nt/20/
      data xt/0.0000000e+00,6.3113181e-03,2.5085916e-02,5.5849748e-02,
     &        9.7826048e-02,1.4995494e-01,2.1092022e-01,2.7918254e-01,
     &        3.5301833e-01,4.3056329e-01,5.0985946e-01,5.8890467e-01,
     &        6.6570309e-01,7.3831561e-01,8.0490884e-01,8.6380137e-01,
     &        9.1350626e-01,9.5276869e-01,9.8059811e-01,9.9629820e-01/
      data wt/0.0000000e+00,1.0006749e-03,3.9396735e-03,8.6315285e-03,
     &        1.4778960e-02,2.1989919e-02,2.9800076e-02,3.7699487e-02,
     &        4.5161909e-02,5.1675117e-02,5.6770495e-02,6.0050215e-02,
     &        6.1210468e-02,6.0059431e-02,5.6528927e-02,5.0679150e-02,
     &        4.2696159e-02,3.2882390e-02,2.1641386e-02,9.4707005e-03/
c-----------------------------------------------------------------------
c  20 point gauss laguerre quadrature for asymptotic region 
c-----------------------------------------------------------------------
      data ng/20/
      data xg/7.053989e-2,3.721268e-1,9.165821e-1,1.707307e0,
     &        2.749199e0,4.048925e0,5.615175e0,7.459017e0,
     &        9.594393e0,1.203880e1,1.481429e1,1.794890e1,
     &        2.147879e1,2.545170e1,2.993255e1,3.501343e1,
     &        4.083306e1,4.761999e1,5.581080e1,6.652442e1/
      data wg/1.687468e-1,2.912544e-1,2.666861e-1,1.660024e-1,
     &        7.482607e-2,2.496442e-2,6.202551e-3,1.144962e-3,
     &        1.557418e-4,1.540144e-5,1.086486e-6,5.330121e-8,
     &        1.757981e-9,3.725503e-11,4.767529e-13,3.372844e-15,
     &        1.155014e-17,1.539522e-20,5.286443e-24,1.656457e-28/
c------------------------------------------------------------------------
c  determine threshold and asymptotic types
c------------------------------------------------------------------------
       if (ixtyp.le.4) then
           iasymc = ixtyp
           ithrsc = 1
       else
           iasymc = ixtyp-4
           ithrsc = 2
       endif
c------------------------------------------------------------------------
c  set up spline coefficient matrices if not bad-point case
c------------------------------------------------------------------------
       if(ibpts.le.0)then                                               
           iend1=1                                                      
           g1=0.0d0                                                     
           iendn=1                                                     
           gn=0.0d0                                                     
           iends=1                                                      
           x1=xsa(1)                                                    
           dx1=0.01*x1                         
           call egasym(x1,dx1,form4,ixtyp,iends)                       
           iends=2                                                      
           xn=xsa(ict)                                                  
           dxn=0.01*xn                                                  
           call egasym(xn,dxn,form4,ixtyp,iends)                    
           call egspc(xsa,ict,c1,c2,c3,c4)                              
       endif
c
c       write(i4unit(-1),*)'itout,nt=',itout,nt                                                            
       do 200 it=1,itout                                                
        te=toa(it)                                                      
        ate=1.57890d5*eij/te                                           
c------------------------------------------------------------------------
c  quadratures over threshold part
c------------------------------------------------------------------------
         sumt = 0.0d0
         sumtp=0.0d0
         do 50 k = 2, nt
           x=xt(k)*(xkc-1.0d0)+1.0                                   
           xs=1.0d0/x                                                 
         if(ibpts.gt.0)then                                             
             call elnfit(xs,xsa,ys,ysa,ict)                               
         else                                                           
             call efasym(xs,xsa,ict,ysa,ys,dys,
     &                   c1,c2,c3,c4,form4,ixtyp)  
         endif
         if(ithrsc.eq.1)then                                       
             apy=fxc1*((x-1.0)/(xkc-1.0))**bxc*
     &           dexp(-(bxc-bpxc)*((x-xkc)/(xkc-1.0d0)))                          
             if(idiff.eq.0)then                                         
                 y=ys*apy                               
             else                                                      
                 y=ys+apy                                              
             endif                                                      
         elseif(ithrsc.eq.2)then                          
             apy=fxc1*((x-1.0)/(xkc-1.0))**bxc*
     &           dexp(-(bxc-bpxc)*((x-xkc)/(xkc-1.0d0)))                        
             y=ys*apy                                  
         else                                         
             apy=0.0d0                                             
             ys=y                                                   
         endif                                                          
c         write(i4unit(-1),*)'k,xt(k),x,xs,ys,apy,=',k,xt(k),x,xs,ys,apy        
c         write(i4unit(-1),*)'a8amax.for: x,y,=',x,y        
         sumtp=sumtp+wt(k)*apy*dexp(-(xkc-1.0d0)*ate*xt(k))/
     &         dsqrt(xt(k))
   50    sumt=sumt+wt(k)*y*dexp(-(xkc-1.0d0)*ate*xt(k))/dsqrt(xt(k))
         sumtp = (xkc-1.0d0)*ate*sumtp           
         sumt = (xkc-1.0d0)*ate*sumt           
c------------------------------------------------------------------------
c  quadratures over asymptotic part
c------------------------------------------------------------------------
        suma=0.0d0
        sumap=0.0d0                                                      
        do 100 k=1,ng                                                 
         x=xg(k)/ate+xkc                                   
         xs=1.0d0/x                                                 
         if(ibpts.gt.0)then                                             
             call elnfit(xs,xsa,ys,ysa,ict)                               
         else                                                           
             call efasym(xs,xsa,ict,ysa,ys,dys,
     &                   c1,c2,c3,c4,form4,ixtyp)  
         endif                                                        
         if(iasymc.eq.1)then                                       
             apy=fxc3*s*dlog(x-xkc+fxc2)                          
             if(idiff.eq.0)then                                         
                 y=ys*apy                                
             else                                                      
                 y=ys+apy                                              
             endif                                                      
         elseif(iasymc.eq.2)then                          
             apy=fxc2+fxc3/(x-xkc+1.0)                                  
             y=ys*apy                                  
         elseif(iasymc.eq.3)then                          
             apy=fxc3/(x-xkc+fxc2)**2                             
             y=ys*apy                                  
         elseif(iasymc.eq.4)then                                         
             apy=0.0d0                                             
             y=10.0d0**ys                                   
         else                                         
             apy=0.0d0                                             
             y=ys                                                   
         endif                                                          
c        write(i4unit(-1),*)'a8amax.for: x,y,=',x,y        
        sumap=sumap+wg(k)*apy
  100   suma=suma+wg(k)*y
        sumap = dexp(-(xkc-1.0d0)*ate)*sumap
        suma = dexp(-(xkc-1.0d0)*ate)*suma
c                                      
        goa(it)=sumt + suma
        apg = sumtp+sumap
c------------------------------------------------------------------------
c  evaluate maxwell averages of approximate forms
c------------------------------------------------------------------------
        if(ithrsc.eq.1)then                                       
            xe=(bxc-bpxc)                                         
            apgt=fxc1*(xkc-1.0d0)*ate*dexp(xe)*
     &           a8gamg(bxc+1.0d0,xe+(xkc-1.0d0)*ate)/
     &           (dabs(xe+(xkc-1.0d0)*ate))**(bxc+1.0d0)                 
        elseif(ithrsc.eq.2)then
            xe=(bxc-bpxc)                                         
            apgt=fxc1*(xkc-1.0d0)*ate*dexp(xe)*
     &           a8gamg(bxc+1.0d0,xe+(xkc-1.0d0)*ate)/
     &           (dabs(xe+(xkc-1.0d0)*ate))**(bxc+1.0d0)                 
        else
            apgt=0.0d0
        endif                 
                                   
        if(iasymc.eq.1)then                                        
            xe=fxc2*ate                                         
            apga=fxc3*s*(dlog(fxc2)+eei(xe))                 
        elseif(iasymc.eq.2)then                           
            xe=ate                                                      
            apga=fxc2+fxc3*ate*eei(ate)                            
        elseif(iasymc.eq.3)then                   
            xe=fxc2*ate                                 
            apga=fxc3*ate*ate*ee2(xe)/xe
        elseif(iasymc.eq.4)then                                  
            apga=0.0d0                                             
        else                                          
            apga=0.0d0                                             
        endif
        apga = dexp(-(xkc-1.0d0)*ate)*apga

        apgoa(it)=apgt+apga
c                
        dexcra(it)=2.1720d-8*dsqrt(ate/eij)*goa(it)/wj                  
        excra(it)=wj*dexp(-ate)*dexcra(it)/wi                           
        gbarfa(it)=6.8916d-2*eij*goa(it)/wi                             
  200  continue
       return
c-----------------------------------------------------------------------
       end
         

             
                 
                 
                
         
       
       
               
         
       
            
  
       
