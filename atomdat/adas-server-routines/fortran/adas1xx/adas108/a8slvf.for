CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8slvf.for,v 1.1 2004/07/06 10:04:04 whitefor Exp $ Date $Date: 2004/07/06 10:04:04 $
CX
       subroutine a8slvf(itype,xk,sigk,xn,sign,s,f2,f3,ifail)
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran77 subroutine a8slvf *******************
c
c  purpose: to find the approximate form parameters f2 and f3 for 
c           neutrals
c
c  calling program:  adas108.for
c
c  input:
c            (i*4)  itype    = type of transition (1=dipole,2=non-dipole
c                              non-spin change, 3=spin change, 4=null)
c            (r*8)  xk       = x-parameter at matching point k
c            (r*8)  sigk     = collisions strength at matching point k
c            (r*8)  xn       = x-parameter at last energy point n
c            (r*8)  sign     = collisions strength at last energy pt. n 
c            (r*8)  s        = line strength for type 1 case 
c            (i*4)  ifail    = failure code  on entry (ifail=0 two point
c                              fit, ifail=-1 one point fit)
c  output: 
c            (r*8)  f2       = asymptotic form parameter
c            (r*8)  f3       = asymptotic form parameter
c            (i*4)  ifail    = failure code  on exit 
c                             (ifail=0 successful two point fit
c                              ifail=1 converted to one point fit)
c 
c
c  routines: none
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     15/06/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       real*8   d1
c-----------------------------------------------------------------------
       parameter ( d1 = 1.0d-10 )
c-----------------------------------------------------------------------
       integer  i4unit,ifail  , ifail0, itype
c-----------------------------------------------------------------------
       real*8   f2    , f3    , xk    , xn    , sigk  , sign  , s    
       real*8   swap     
       real*8   f2l   , f2u   , f2int , fl    , fu    , f
c-----------------------------------------------------------------------

       ifail0=ifail
              
       if (itype.eq.4) then
           f2=1.0
           f3=0.0
       elseif (itype.eq.3) then
           if(ifail0.eq.0) then
               if(sigk.gt.sign) then
                   f2=(xn-xk)*dsqrt(sign)/
     &                (dsqrt(sigk)-dsqrt(sign))
                   f3=sigk*f2**2
                   ifail=0
               else
                   f2=1.0
                   f3=sigk*f2**2
                   ifail=1
               endif
           elseif(ifail0.eq.-1) then
               f2=1.0
               f3=sigk*f2**2
               ifail=1
           endif
       elseif (itype.eq.2) then
           if(ifail0.eq.0) then
               f2=((xn-xk+1)*sign-sigk)/(xn-xk)
               f3=sigk-f2
               ifail=0
           elseif(ifail0.eq.-1) then
               f2=sigk
               f3=0.0
               ifail=1
           endif
       elseif (itype.eq.1)then
           if(ifail0.eq.-1)then
               f3=1.333333d0
               f2=dexp(sigk/(f3*s))
               ifail=1
           elseif(ifail0.eq.0) then
               if(sign.le.sigk) then
                   f3=1.333333d0
                   f2=dexp(sigk/(f3*s))
                   ifail=1
               else                
c-----------------------------------------------------------------------
c  bracket zero for f2 equation 
c-----------------------------------------------------------------------
         f2u = 20.0
         f2int=0.2
         fu  = dlog(xn-xk+f2u)-(sign/sigk)*dlog(f2u)
    5    f2l = f2u-f2int
         fl  = dlog(xn-xk+f2l)-(sign/sigk)*dlog(f2l)
         if (fl*fu .gt. 0.0) then
             if (fl .gt. 0.0 .and. fl .lt. fu) then
                 f2u = f2l
                 fu=fl
                 go to 5
             elseif (fl .gt. 0.0 .and. fl .gt. fu) then
                 f2int=-f2int
                 go to 5
             elseif (fl .lt. 0.0 .and. fu .lt. fl) then
                 f2u = f2l
                 fu=fl
                 go to 5
             elseif (fl .lt. 0.0 .and. fu .gt. fl) then
                 f2int=-f2int
                 go to 5
             endif
         endif
         if(fu.lt.fl) then
             swap=fl
             fl=fu
             fu=swap
             swap=f2l
             f2l=f2u
             f2u=swap
         endif
c-----------------------------------------------------------------------
c  now solve by alternated bisection and linear interpolation
c-----------------------------------------------------------------------
    9    f2 = (f2l*fu-f2u*fl)/(fu-fl)                    
         f  = dlog(xn-xk+f2)-(sign/sigk)*dlog(f2)
c
         if(f)11,16,10                                 
   10       f2u=f2                                         
            fu=f                                            
            go to 12                                         
   11       f2l=f2                                            
            fl=f                                               
   12       f2=0.5*(f2l+f2u)
c                                      
         f  = dlog(xn-xk+f2)-(sign/sigk)*dlog(f2)
c
         if(f)14,16,13                                           
   13       f2u=f2                                                   
            fu=f                                                      
            go to 15                                                   
   14       f2l=f2                                                      
            fl=f                                                         
   15       f2=(f2l*fu-f2u*fl)/(fu-fl)
            if(dabs(f2l-f2)/f2 .gt. d1)go to 9                                  
              
   16    continue
c-----------------------------------------------------------------------
c  obtain remaining parameter
c-----------------------------------------------------------------------
               f3=sigk/(s*dlog(f2))
               ifail = 0
               endif
           endif
       endif
       
       return
c-----------------------------------------------------------------------
       end
         

             
                 
                 
                
         
       
       
               
         
       
            
  
       
