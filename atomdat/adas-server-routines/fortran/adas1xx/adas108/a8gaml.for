CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8gaml.for,v 1.1 2004/07/06 10:03:32 whitefor Exp $ Date $Date: 2004/07/06 10:03:32 $
CX
       function a8gaml( xx )
       implicit none
c---------------------------------------------------------------------
c
c  ********************* fortran77 function a8gaml *******************
c
c  purpose: to evaluate log (gamma (xx)) for xx >0  - based on 
c           numerical recipes  
c
c  calling program:  a8gser.for , a8gcf
c
c  input:
c            (r*8)  xx      = parameter of gamma
c  output:
c            (r*8)  a8gaml = log gamma (xx)
c
c
c  routines: 
c             none
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     25/06/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       integer j
c-----------------------------------------------------------------------
       real*8  xx    , a8gaml , stp   , half   , one    , fpf   ,
     &         x     , tmp    , ser
c-----------------------------------------------------------------------
       real*8  cof(6)
c-----------------------------------------------------------------------
       data   cof  / 76.18009173d0   , -86.50532033d0  ,
     &               24.01409822d0   , -1.231739516d0 ,
     &               0.120858003d-2 , -0.536382d-5    /
       data   stp  / 2.50662827465d0 /
       data   half, one, fpf / 0.5d0   , 1.0d0   , 5.5d0   /
c-----------------------------------------------------------------------  
       x  = xx-one
       tmp = x+fpf
       tmp = (x+half)*dlog(tmp) - tmp
       ser = one
       do j=1,6
         x = x+one
         ser = ser+cof(j)/x
       enddo
       a8gaml = tmp+dlog(stp*ser)
c
       return
       end
       
