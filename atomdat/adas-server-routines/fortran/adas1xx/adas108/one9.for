CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/one9.for,v 1.1 2004/07/06 14:25:47 whitefor Exp $ Date $Date: 2004/07/06 14:25:47 $
CX
      SUBROUTINE ONE9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 SUBROUTINE: ONE9  *******************
C
C  PURPOSE:
C        TO CALCULATE THE NINE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE NINE BURGESS KNOT POINTS
C
C  ROUTINES:
C        NONE
C
C  DATE:    25/05/99				VERSION 1.1
C
C  AUTHOR:  HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C           FIRST RELEASE
C
C-----------------------------------------------------------------------
      INTEGER I      , J
C-----------------------------------------------------------------------
      REAL*8  T1     , T2
C-----------------------------------------------------------------------
      REAL*8  A(9,9) , P(9)
C-----------------------------------------------------------------------
      T1=0
      T2=0
      DO I=1,9
         DO J=1,9
            T1=T1+A(I,J)
         ENDDO
         T2=T2+P(I)
      ENDDO
      T1=T2/T1
      DO I=1,9
        P(I)=T1
      ENDDO
C-----------------------------------------------------------------------
      RETURN
      END
