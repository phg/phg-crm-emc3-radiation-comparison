CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8gamg.for,v 1.1 2004/07/06 10:03:29 whitefor Exp $ Date $Date: 2004/07/06 10:03:29 $
CX
       function a8gamg( a    , x )
       implicit none
c---------------------------------------------------------------------
c
c  ********************* fortran77 function a8gamg *******************
c
c  purpose: to evaluate the incomplete gamma function gamma(a,x)
c           based on numerical recipes  
c
c  calling program:  a8amax.for
c
c  input:
c            (r*8)  a      = parameter of p(a,x)
c            (r*8)  x      = parameter of p(a,x)
c  output:
c            (r*8)  a8gamg = incomplete gamma function gamma(a,x)
c                            (n.b. for x<0 principal value of 
c                             logarithm is taken)
c
c
c  routines: 
c             a8gser	adas	generates series expansion of gamma
c	      a8gcf	adas    generates continued fraction for gamma
c             a8gaml    adas    obtains log(gamma(a))
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     27/06/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       integer i4unit
c-----------------------------------------------------------------------
       real*8  a    , x    , gln  ,a8gamg , gamser , gammcf
c-----------------------------------------------------------------------
       if (x.lt.a+1.0d0) then
           call a8gser(gamser,a,x,gln)
           a8gamg = gamser
       else
           call a8gcf(gammcf,a,x,gln)
           a8gamg = dexp(gln)-gammcf
       endif
c
       return
       end
       
