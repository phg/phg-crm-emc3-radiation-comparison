CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/two9.for,v 1.1 2004/07/06 15:26:57 whitefor Exp $ Date $Date: 2004/07/06 15:26:57 $
CX
      SUBROUTINE TWO9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN 77 SUBROUTINE: TWO9  ********************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   26/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1 , T2 , T3 , T4 , T5 , T6 , T7
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,2)/8.
         T2=A(I,3)/8.
         T3=A(I,4)/8.
         T4=A(I,5)/8.
         T5=A(I,6)/8.
         T6=A(I,7)/8.
         T7=A(I,8)/8.
         A(I,1)=A(I,1)+7.*T1+6.*T2+5.*T3+4.+T4+3.*T5+2.*T6+1.*T7
         A(I,2)=A(I,5)+1.*T1+2.*T2+3.*T3+4.*T4+5.*T5+6.*T6+7.*T7
      ENDDO
      DO J=1,2
         A(1,J)=A(1,J)+.5*A(2,J)+.5*A(3,J)+.5*A(4,J)+.5*A(5,J)
     &          +.5*A(6,J)+.5*A(7,J)+.5*A(8,J)
         A(2,J)=A(5,J)+.5*A(2,J)+.5*A(3,J)+.5*A(4,J)+.5*A(5,J)
     &          +.5*A(6,J)+.5*A(7,J)+.5*A(8,J)
      ENDDO
      P(1)=P(1)+.5*P(2)+.5*P(3)+.5*P(4)+.5*P(5)
     &         +.5*P(6)+.5*P(7)+.5*P(8)
      P(2)=P(5)+.5*P(2)+.5*P(3)+.5*P(4)+.5*P(5)
     &         +.5*P(6)+.5*P(7)+.5*P(8)
      CALL MATIN1(2,A,P)
      P(9)=P(2)
      P(8)=(1.*P(1)+7.*P(9))/8.
      P(7)=(2.*P(1)+6.*P(9))/8.
      P(6)=(3.*P(1)+5.*P(9))/8.
      P(5)=(4.*P(1)+4.*P(9))/8.
      P(4)=(5.*P(1)+3.*P(9))/8.
      P(3)=(6.*P(1)+2.*P(9))/8.
      P(2)=(7.*P(1)+1.*P(9))/8.
C----------------------------------------------------------------------
      RETURN
      END
