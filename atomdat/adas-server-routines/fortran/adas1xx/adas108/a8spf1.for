CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8spf1.for,v 1.1 2004/07/06 10:04:12 whitefor Exp $ Date $Date: 2004/07/06 10:04:12 $
CX
       SUBROUTINE A8SPF1( LPEND   , IGRPOUT   , IANOPT    , 
     &                    IGRPSCAL1, IGRPSCAL2, ITEXOUT   ,
     &                    CGTIT   , CHEADER   , TEXDSN    , 
     &                    BBVAL   , BCVAL     ,
     &                    IBXVAL  , BYVAL     , XMIN1     , XMAX1   ,
     &                    YMIN1   , YMAX1     , XMIN2     , XMAX2   ,
     &                    YMIN2   , YMAX2     , IIBTS     , IIFPT   ,
     &                    IIXOP   , IIDIF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN 77 SUBROUTINE: A8SPF1  *******************
C
C  CALLING PROGRAM: 
C          ADAS108.FOR
C
C  PURPOSE:
C          TO COMMUNICATE WITH IDL OUTPUT SELECTION INTERFACE 
C          A8SPF1.PRO VIA THE UNIX PIPE.
C
C  INPUT:
C          NONE
C  OUTPUT:
C          (L)    LPEND     = CANCEL OR DONE.
C          (I*4)  IGRPOUT   = GRAPH DISPLAY SELECTION SWITCH
C          (I*4)  IANOPT    = ANALYSIS OPTION
C                             1 - ADAS
C                             2 - BURGESS
C          (I*4)  IGRPSCAL1 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             IST GRAPH.
C          (I*4)  IGRPSCAL2 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             2NT GRAPH.
C          (I*4)  ITEXOUT   = HARD COPY OF TEXT OUTPUT REQUEST SWITCH.
C                             0 - NO, 1 - YES
C          (C*40) CGTIT     = THE GRAPH TITLE (INFORMATION FOR ANY
C                             NEW ARCHIVE).
C          (C*80) CHEADER   = THE ADAS HEADER FOR THE DEFAULT FILE.
C          (C*80) TEXDSN    = THE DEFAULT ARCHIVE FILE NAME.
C          (R*8)  BBVAL     = SCALABLE BURGESS PARAMETER-B.
C          (R*8)  BCVAL     = SCALABLE BURGESS PARAMETER-C.
C          (R*8)  IBXVAL    = NON-DIPOLE POINT AT INFINITY SWITCH.
C                             0 - OFF, 1 - ON
C          (R*8)  BYVAL     = THE Y VALUE OF THE POINT AT INFINITY.
C          (R*8)  XMIN1     = USER SELECTED 1ST GRAPH X-AXIS MINIMUM.
C          (R*8)  XMAX1     = USER SELECTED 1ST GRAPH X-AXIS MAXIMUM.
C          (R*8)  YMIN1     = USER SELECTED 1ST GRAPH Y-AXIS MINIMUM.
C          (R*8)  YMAX1     = USER SELECTED 1ST GRAPH Y-AXIS MAXIMUM.
C          (R*8)  XMIN2     = USER SELECTED 2ND GRAPH X-AXIS MINIMUM.
C          (R*8)  XMAX2     = USER SELECTED 2ND GRAPH X-AXIS MAXIMUM.
C          (R*8)  YMIN2     = USER SELECTED 2ND GRAPH Y-AXIS MINIMUM.
C          (R*8)  YMAX2     = USER SELECTED 2ND GRAPH Y-AXIS MAXIMUM.
C          (I*4)  IIBTS     = 0  BAD POINT OPTION OFF
C                           = 1  BAD POINT OPTION ON      
C          (I*4)  IIFPT     = 1  SELECT ONE POINT OPTIMISING    
C                           = 2  SELECT TWO POINT OPTIMISING   
C          (I*4)  IIXOP     = 0  OPTIMISING OFF        
C                           = 1  OPTIMISING ON  (IF ALLOWED)  
C          (I*4)  IIDIF     = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY
C                                WITH OPTIMISING)
C                           = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT
C
C ROUTINES:
C----------
C          XXSLEN - REMOVES SPACES FROM A STRING
C
C
C  AUTHOR: HUGH SUMMERS (UNIV.OF STRATHCLYDE) EXT.4196
C
C  DATE:   25/05/99			VERSION 1.1
C  MODIFIED: HUGH SUMMERS
C		- FIRST RELEASE
C
C-----------------------------------------------------------------------
       INTEGER  IGRPOUT  , IANOPT   , IFIRST  , ILAST 
       INTEGER  IGRPSCAL1, IGRPSCAL2, ITEXOUT , PIPEIN  , PIPEOU
       INTEGER  IIDIF    , IPEND
       INTEGER  IIBTS    , IIFPT    , IIXOP   , I4UNIT  , IBXVAL         
C-----------------------------------------------------------------------
       CHARACTER CGTIT*40 , CHEADER*80 , TEXDSN*80 , CGTEMP*80
       CHARACTER CTEXDSN*80
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
       REAL*8  BBVAL   , BCVAL   , BYVAL
       REAL*8  XMIN1   , XMAX1   , YMIN1   , YMAX1
       REAL*8  XMIN2   , XMAX2   , YMIN2   , YMAX2
C-----------------------------------------------------------------------
       READ(PIPEIN,1000)IPEND
       IF (IPEND.EQ.1)THEN
          LPEND = .TRUE.
       ELSE
          LPEND = .FALSE.
       ENDIF
C
       IF (IPEND.EQ.0) THEN
           READ(PIPEIN,1000)IANOPT
             IF (IANOPT.EQ.1)THEN
               READ(PIPEIN,1000)IIFPT
               READ(PIPEIN,1000)IIBTS
               READ(PIPEIN,1000)IIXOP
               READ(PIPEIN,1000)IIDIF
             ELSE
               READ(PIPEIN,1001)BBVAL
               READ(PIPEIN,1001)BCVAL
               READ(PIPEIN,1000)IBXVAL
               READ(PIPEIN,1001)BYVAL
             ENDIF
C
         READ(PIPEIN,1000)IGRPOUT
         IF (IGRPOUT.EQ.1)THEN
          READ(PIPEIN,1003)CGTEMP
            CALL XXSLEN( CGTEMP , IFIRST , ILAST )
               READ(CGTEMP(IFIRST:IFIRST+39),1004)CGTIT
           READ(PIPEIN,1000)IGRPSCAL1
             IF (IGRPSCAL1.EQ.1)THEN
               READ(PIPEIN,1001)XMIN1
               READ(PIPEIN,1001)XMAX1
               READ(PIPEIN,1001)YMIN1
               READ(PIPEIN,1001)YMAX1
             ENDIF
C
           READ(PIPEIN,1000)IGRPSCAL2
             IF (IGRPSCAL2.EQ.1)THEN
               READ(PIPEIN,1001)XMIN2
               READ(PIPEIN,1001)XMAX2
               READ(PIPEIN,1001)YMIN2
               READ(PIPEIN,1001)YMAX2
             ENDIF
         ENDIF
C-----------------------------------------------------------------------
         READ(PIPEIN,1000)ITEXOUT
           IF (ITEXOUT.EQ.1)THEN
             READ(PIPEIN,1003)CTEXDSN
               CALL XXSLEN( CTEXDSN , IFIRST , ILAST )
               READ(CTEXDSN(IFIRST:ILAST),'(A)') TEXDSN
C              TEXDSN = CTEXDSN
           ENDIF
       ENDIF
C
       IF (IPEND.EQ.0.AND.ITEXOUT.EQ.1)THEN
         READ(PIPEIN,1003)CHEADER
       ENDIF
C-----------------------------------------------------------------------
 1000  FORMAT(1X,I3)
 1001  FORMAT(1X,E8.2)
 1002  FORMAT(1X,1A40)
 1003  FORMAT(1X,1A80)
 1004  FORMAT(1A45)
C-----------------------------------------------------------------------
       RETURN
       END
