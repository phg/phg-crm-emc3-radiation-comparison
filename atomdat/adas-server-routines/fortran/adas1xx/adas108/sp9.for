CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/sp9.for,v 1.1 2004/07/06 15:20:46 whitefor Exp $ Date $Date: 2004/07/06 15:20:46 $
CX
      FUNCTION SP9(P,X)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: SP9 ***********************
C
C  PURPOSE:  TO CALCULATE A SPLINE THROUGH THE NINE KNOT POINTS
C
C  CALLING PROGRAM:
C        OMEUPS
C
C
C  INPUT:   
C        (R*8) P    - THE NINE KNOT POINTS
C        (R*8) X    - THE POINT AT WHICH TO EVALUATE THE SPLINE
C  OUTPUT:   
C        (R*8) SP9  - THE EVALUATED SPLINE
C
C  ROUTINES: NONE
C
C  DATE:     24-05-99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C            FIRST RELEASE
C
C
C------------------------------------------------------------------------
       INTEGER  ISHFT , JSHFT  , KSHFT  , NSPLINE , J0  
C------------------------------------------------------------------------
       PARAMETER ( NSPLINE = 9 , ISHFT = (NSPLINE-1)/2 , 
     &             JSHFT = ISHFT , KSHFT = ISHFT+1 ,J0 = ISHFT-1 )
C------------------------------------------------------------------------
       INTEGER  JX
C------------------------------------------------------------------------
       REAL*8   XI    , AA     , BB     , CC      , DD
       REAL*8   X     , SP9
C------------------------------------------------------------------------
       REAL*8   P(NSPLINE)   , S(NSPLINE-2)
C------------------------------------------------------------------------
       S(1)=20.2871865*P(1)-46.0103057*P(2)+32.615596*P(3)-
     &      8.73926486*P(4)+2.34146341*P(5)-0.626588801*P(6)+
     &      0.16489179*P(7)-0.0384747509*P(8)+0.00549639299*P(9)
       S(2)=-5.43593267*P(1)+38.0515287*P(2)-67.0779801*P(3)+
     &      43.6963243*P(4)-11.7073171*P(5)+3.13294401*P(6)-
     &      0.824458949*P(7)+0.192373755*P(8)-0.027481965*P(9)
       S(3)=1.45654414*P(1)-10.195809*P(2)+43.6963243*P(3)-
     &      70.0460323*P(4)+44.4878049*P(5)-11.9051872*P(6)+
     &      3.13294401*P(7)-0.731020268*P(8)+0.104431467*P(9)
       S(4)=-0.390243902*P(1)+2.73170732*P(2)-11.7073171*P(3)+
     &      44.4878049*P(4)-70.2439024*P(5)+44.4878049*P(6)-
     &      11.7073171*P(7)+2.73170732*P(8)-0.390243902*P(9)
       S(5)=0.104431467*P(1)-0.731020268*P(2)+3.13294401*P(3)-
     &      11.9051872*P(4)+44.4878049*P(5)-70.0460323*P(6)+
     &      43.6963243*P(7)-10.195809*P(8)+1.45654414*P(9)
       S(6)=-0.027481965*P(1)+0.192373755*P(2)-0.824458949*P(3)+
     &      3.13294401*P(4)-11.7073171*P(5)+43.6963243*P(6)-
     &      67.0779801*P(7)+38.0515287*P(8)-5.43593267*P(9)
       S(7)=0.00549639299*P(1)-0.0384747509*P(2)+0.16489179*P(3)-
     &      0.626588801*P(4)+2.34146341*P(5)-8.73926486*P(6)+
     &      32.615596*P(7)-46.0103057*P(8)+20.2871865*P(9)
C
         JX=INT(X*(J0+1))
         XI=X*(J0+1)-JX
         IF(XI.LT.0.0)THEN
           XI=XI+1.0
           JX=JX-1
         ENDIF
         IF(JX.EQ.J0+1)THEN
           JX=JX-1
           XI=XI+1.0
         ENDIF
C         
         AA=1.0-XI
         BB=XI
         CC=(AA**3-AA)/96.0
         DD=(BB**3-BB)/96.0
C
         IF(JX.EQ.-J0-1) THEN
            SP9=AA*P(-J0-1+KSHFT)+BB*P(-J0+KSHFT)
            SP9=SP9+(CC+DD)*S(JX+1+JSHFT)
         ELSEIF(JX.EQ.J0) THEN
            SP9=AA*P(J0+KSHFT)+BB*P(J0+1+KSHFT)
            SP9=SP9+(CC+DD)*S(JX+JSHFT)
         ELSE
            SP9=AA*P(JX+KSHFT)+BB*P(JX+1+KSHFT)
            SP9=SP9+CC*S(JX+JSHFT)+DD*S(JX+1+JSHFT)
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
