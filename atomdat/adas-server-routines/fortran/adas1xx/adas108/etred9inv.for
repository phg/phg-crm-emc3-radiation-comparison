CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/etred9inv.for,v 1.1 2004/07/06 13:49:52 whitefor Exp $ Date $Date: 2004/07/06 13:49:52 $
CX
       FUNCTION ETRED9INV( KTYPE , EIJ   , TR    , B    , C )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: ETRED9INV ******************
C
C  PURPOSE:  TO CALCULATE THE ELECTRON ENERGY FROM THE REDUCED ENERGY
C            FOR EIGHT TYPES OF TRANSITION 
C
C  CALLING PROGRAM: ADAS108
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (Eij)
C            (R*8)    TR     =   REDUCED ENERGY
C            (R*8)    B      =   BURGESS SCALING PARAMETER - B
C            (R*8)    C      =   BURGESS SCALING PARAMETER - C
C            (R*8)    TL     =   Ej/Eij
C            (I*4)    KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE	- EXP THRESHOLD
C                                2 NON ELECTRIC DIPOLE	- EXP THRESHOLD
C                                3 SPIN CHANGE		- EXP THRESHOLD
C                                4 OTHER		- EXP THRESHOLD
C                                5 ELECTRIC DIPOLE	- POWER THRESHOLD
C                                6 NON ELECTRIC DIPOLE	- POWER THRESHOLD
C                                7 SPIN CHANGE		- POWER THRESHOLD
C                                8 OTHER		- POWER THRESHOLD
C
C  OUTPUT:   (R*8)    ETRED9INV=   ELECTRON ENERGY
C
C            (I*4)    IASYMC =   ASYMPTOTIC CLASSIFICATION TYPE
C            (I*4)    ITHRSC =   THRESHOLD CLASSIFICATION TYPE
C            (R*8)    E0     =   SWITCHING ENERGY
C
C 
C  ROUTINES: NONE
C
C  DATE:     25/05/99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C-----------------------------------------------------------------------
       INTEGER KTYPE , IASYMC , ITHRSC
C-----------------------------------------------------------------------
       REAL*8  EIJ   , E0     , TR     
       REAL*8  B     , C      , TL    , TL0   , ETRED9INV
C-----------------------------------------------------------------------
       IF (KTYPE.LE.4) THEN
           IASYMC = KTYPE
           ITHRSC = 1
C           
           IF (IASYMC.EQ.1) THEN
               E0=EIJ*B*C*DLOG(C)/2
           ELSEIF (IASYMC.EQ.2) THEN
               E0=EIJ*B*C/2
           ELSEIF (IASYMC.EQ.3) THEN
               E0=EIJ*B*C/2
           ELSE
               E0=EIJ*B*C*DLOG(C)/2
           ENDIF
C                       
       ELSE
           IASYMC = KTYPE-4
           ITHRSC = 2
C           
           IF (IASYMC.EQ.1) THEN
               E0=EIJ*(B*C*DLOG(C)/2)**0.666667
           ELSEIF (IASYMC.EQ.2) THEN
               E0=EIJ*(B*C/2)**0.666667
           ELSEIF (IASYMC.EQ.3) THEN
               E0=EIJ*(B*C/2)**0.666667
           ELSE
               E0=EIJ*(B*C*DLOG(C)/2)**0.666667
           ENDIF
C                       
       ENDIF
C
       TL0 = DABS(E0/EIJ)
C
       IF (TR.GE.0.0D0) THEN
           IF (IASYMC.EQ.1.OR.IASYMC.EQ.4)THEN
               TL = DEXP(DLOG(C)/(1.0-TR))+TL0-C
           ELSEIF (IASYMC.EQ.2.OR.IASYMC.EQ.3)THEN
               TL = TR*C/(1-TR)+TL0
           ENDIF
       ELSE
           IF (ITHRSC.EQ.1)THEN
               TL = TL0*((TR+1.0)/(1.0-TR))**(1.0/B)
           ELSEIF (ITHRSC.EQ.2)THEN
               TL = 1.0/(-DLOG(TR)/B +1.0/DSQRT(TL0))**2
           ENDIF
       ENDIF    
C
       ETRED9INV = DABS(TL*EIJ)
C-----------------------------------------------------------------------
       RETURN
       END
