CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/lstsq9.for,v 1.1 2004/07/06 14:13:47 whitefor Exp $ Date $Date: 2004/07/06 14:13:47 $
CX
      SUBROUTINE LSTSQ9(IT,B,C,EIJ,GF,N,T,U,P,RMS)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  ******************** FORTRAN77 SUBROUTINE: LSTSQ9 *******************
C
C  PURPOSE: TO PERFORM NINE POINT SPLINE FIT TO REDUCED OMEGAS
C
C  INPUT:
C        (R*8)   EIJ    = TRANSITION ENERGY  (RYD)
C        (R*8)   GF     = GF-VALUE
C        (I*4)   IT     = TRANSITION TYPE
C        (R*8)   T()    = ENERGIES (RYD) OF DATA POINTS
C        (R*8)   U()    = OMEGAS OF DATA POINTS
C        (I*4)   N      = NUMBER OF DATA POINTS
C        (R*8)   B      = BURGESS SCALING PARAMETER - B
C        (R*8)   C      = BURGESS SCALING PARAMETER - C
C
C  OUTPUT:
C        (R*8)   P()    = SPLINE VALUES AT BURGESS/SUMMERS KNOTS 
C
C  LOCAL VARIABLES/CONSTANTS:
C
C        (R*8)   A(,)   = NORMAL EQUATION COEFFICIENT MATRIX
C        (R*8)   V      = GENERAL VARIABLE
C        (R*8)   W      = REAL CONSTANT (=3)
C        (R*8)   Y()    = TEMPORARY KNOTS
C        (R*8)   XX     = GENERAL VARIABLE 
C        (R*8)   YY     = GENERAL VARIABLE 
C
C  ROUTINES: 
C          SPLS9    - CALCULATE CUBIC SPLINE FIT COEFFICIENTS
C          ETRED9   - CALCULATE REDUCED ENERGIES
C          OURED9   - CALCULATE REDUCED OMEGAS
C          MATIN1   - INVERT MATRIX TO GET KNOT POINTS
C          ONE9     - GET KNOT POINTS IF ONLY ONE DATA POINT
C          TWO9     - GET KNOT POINTS IF ONLY TWO DATA POINTS
C          THREE9   - GET KNOT POINTS IF ONLY THREE DATA POINTS
C          FOUR9    - GET KNOT POINTS IF ONLY FOUR DATA POINTS
C          FIVE9    - GET KNOT POINTS IF ONLY FIVE DATA POINT
C          SIX9     - GET KNOT POINTS IF ONLY SIX DATA POINTS
C          SEVEN9   - GET KNOT POINTS IF ONLY SEVEN DATA POINTS
C          EIGHT9   - GET KNOT POINTS IF ONLY EIGHT DATA POINTS
C
C  DATE:     25-05-99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	     FIRST RELEASE    
C           
C-----------------------------------------------------------------------
      INTEGER IT      , N
      INTEGER I       , J       , L       , K     , I4UNIT
C-----------------------------------------------------------------------
      REAL*8  EIJ     , GF      , B       , C
      REAL*8  W       , E       , U1      , V     ,
     &        XX      , YY      , T1      , T2    ,
     &        R       , RMS
      REAL*8  ETRED9  , OURED9
C-----------------------------------------------------------------------
      REAL*8  T(N)    , U(N)
      REAL*8  A(9,9)  , P(9)    , Y(9)    , P1(9)
C-----------------------------------------------------------------------
      W=3.0
      RMS = 0.0
      E=DABS(EIJ)
C-----------------------------------------------------------------------
C  INITIALISE
C-----------------------------------------------------------------------
      IF (GF .EQ. 0) THEN
         U1=-1
         GOTO 3310
      ENDIF
      IF (IT .EQ. 1 .OR. IT.EQ.4 .OR. IT.EQ.5 .OR. IT.EQ.8) THEN
         U1=4*GF/E
      ELSE IF (IT .EQ. 2 .OR. IT .EQ. 6) THEN
         U1=GF
      ELSE
         U1=GF/E**2
      ENDIF
      V=1./U1
      XX = 1.
      CALL SPLS9(XX,Y)
 3310 CONTINUE
      DO I=1,9
         IF (U1.GE.0) THEN
            P(I)=W*V*Y(I) 
         ELSE 
            P(I)=0
         ENDIF
         DO J=1,9
            IF (U1.GE.0) THEN
               A(I,J)=W*V*Y(I)*V*Y(J)
            ELSE
               A(I,J)=0
            ENDIF
         ENDDO
      ENDDO
      IF (N.EQ.0) GOTO 3470
      DO I=1,N
         XX = ETRED9( IT   , E    , T(I)  , B    , C )
         YY = OURED9( IT   , E    , T(I)  , U(I) , B    , C )
         CALL SPLS9(XX,Y)
         V=1./YY
         DO K=1,9
            P(K)=P(K)+V*Y(K)
            DO J=1,9
               A(K,J)=A(K,J)+V*Y(K)*V*Y(J)
            ENDDO
         ENDDO
      ENDDO
 3470 CONTINUE
      DO I=1,9
         P1(I)=P(I)
      ENDDO
      IF (U1.LT.0) THEN 
         L=N
      ELSE
         L=N+1
      ENDIF
      IF (L .GT. 8)THEN
         CALL MATIN1(9,A,P)
      ELSE IF (L .EQ. 1) THEN
         CALL ONE9(A,P)
      ELSE IF (L .EQ. 2) THEN
         CALL TWO9(A,P)
      ELSE IF (L .EQ. 3) THEN
         CALL THREE9(A,P)
      ELSE IF (L .EQ. 4) THEN
         CALL FOUR9(A,P)
      ELSE IF (L .EQ. 5) THEN
         CALL FIVE9(A,P)
      ELSE IF (L .EQ. 6) THEN
         CALL SIX9(A,P)
      ELSE IF (L .EQ. 7) THEN
         CALL SEVEN9(A,P)
      ELSE
         CALL EIGHT9(A,P)
      ENDIF
      T1=DBLE(N)
      T2=T1
      DO I=1,9
         T1=T1-P1(I)*P(I)
      ENDDO
      R=DSQRT(DABS(T1)/T2)
      RMS = 100*R
C      WRITE(*,*)'B=',B,'  C = ',C
C      WRITE(I4UNIT(-1),*)'R.M.S.= ',100*R
C--------------------------------------------------------------------
      RETURN
      END
