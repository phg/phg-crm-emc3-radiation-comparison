CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/oured9inv.for,v 1.1 2004/07/06 14:26:45 whitefor Exp $ Date $Date: 2004/07/06 14:26:45 $
CX
       FUNCTION OURED9INV( KTYPE , EIJ   , EJ  , OURED  , B    , C )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: OURED9INV ******************
C
C  PURPOSE:  TO CALCULATE THE COLLISION STRENGTH FROM THE REDUCED 
C            COLLISION STRENGTH FOR EIGHT TYPES OF TRANSITION 
C
C  CALLING PROGRAM:
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (RYD)
C            (R*8)    EJ     =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (RYD)
C            (R*8)    B      =   BURGESS SCALING PARAMETER - B
C            (R*8)    C      =   BURGESS SCALING PARAMETER - C
C            (R*8)    TL    =   Ej/Eij
C            (I)      KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C            (R*8)    OURED   =   REDUCED COLLISION STRENGTH
C
C  OUTPUT:   (R*8)    OURED9INV =   UPSILON
C
C            (I*4)    IASYMC =   ASYMPTOTIC CLASSIFICATION TYPE
C            (I*4)    ITHRSC =   THRESHOLD CLASSIFICATION TYPE
C            (R*8)    E0     =   SWITCHING ENERGY
C
C 
C  ROUTINES: NONE
C
C  DATE:     25/05/99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C
C-----------------------------------------------------------------------
       INTEGER KTYPE , IASYMC  , ITHRSC
C-----------------------------------------------------------------------
       REAL*8  EIJ   , EJ   , E0    , B    ,C    , TL   , TL0  
       REAL*8  OURED , OURED9INV
C-----------------------------------------------------------------------
       IF (KTYPE.LE.4) THEN
           IASYMC = KTYPE
           ITHRSC = 1
C           
           IF (IASYMC.EQ.1) THEN
               E0=EIJ*B*C*DLOG(C)/2
           ELSEIF (IASYMC.EQ.2) THEN
               E0=EIJ*B*C/2
           ELSEIF (IASYMC.EQ.3) THEN
               E0=EIJ*B*C/2
           ELSE
               E0=EIJ*B*C*DLOG(C)/2
           ENDIF
C                       
       ELSE
           IASYMC = KTYPE-4
           ITHRSC = 2
C           
           IF (IASYMC.EQ.1) THEN
               E0=EIJ*(B*C*DLOG(C)/2)**0.666667
           ELSEIF (IASYMC.EQ.2) THEN
               E0=EIJ*(B*C/2)**0.666667
           ELSEIF (IASYMC.EQ.3) THEN
               E0=EIJ*(B*C/2)**0.666667
           ELSE
               E0=EIJ*(B*C*DLOG(C)/2)**0.666667
           ENDIF
C                       
       ENDIF
C
       TL  = DABS(EJ/EIJ)
       TL0 = DABS(E0/EIJ)
C
       IF (EJ.GE.E0) THEN
           IF (IASYMC.EQ.1.)THEN
               OURED9INV = OURED*DLOG(TL-TL0+2.718281828)
           ELSEIF (IASYMC.EQ.2)THEN
               OURED9INV = OURED
           ELSEIF (IASYMC.EQ.3)THEN
               OURED9INV = OURED/(TL-TL0+1.0)**2
           ELSEIF (IASYMC.EQ.4)THEN
               OURED9INV = OURED*DLOG(TL-TL0+C)
           ENDIF
       ELSE
           IF (ITHRSC.EQ.1)THEN
               IF (IASYMC.EQ.1)THEN
                  OURED9INV = OURED*(TL/TL0)**B*DEXP(
     &                        -(B-(E0/(2.718281828*EIJ)))*((TL/TL0)-1)) 
               ELSEIF (IASYMC.EQ.2)THEN
                  OURED9INV = OURED*(TL/TL0)**B*DEXP(
     &                        -B*((TL/TL0)-1)) 
               ELSEIF (IASYMC.EQ.3)THEN
                  OURED9INV = OURED*(TL/TL0)**B*DEXP(
     &                        -(B+(2.0*E0/EIJ))*((TL/TL0)-1)) 
               ELSEIF (IASYMC.EQ.4)THEN
                  OURED9INV = OURED*(TL/TL0)**B*DEXP(
     &                        -(B-(E0/(C*DLOG(C)*EIJ)))*((TL/TL0)-1)) 
               ENDIF 
           ELSEIF (ITHRSC.EQ.2)THEN
               OURED9INV = OURED*DEXP(-B*(1.0/DSQRT(TL)-1.0/DSQRT(TL0)))
           ENDIF
       ENDIF    
C-----------------------------------------------------------------------
       RETURN
       END
