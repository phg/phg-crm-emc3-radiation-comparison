CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/eight9.for,v 1.1 2004/07/06 13:48:34 whitefor Exp $ Date $Date: 2004/07/06 13:48:34 $
CX
      SUBROUTINE EIGHT9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77SUBROUTINE: EIGHT9 *******************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   25/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,5)/70.
         A(I,1)=A(I,1)-1.*T1
         A(I,2)=A(I,2)+8.*T1
         A(I,3)=A(I,3)-28.*T1
         A(I,4)=A(I,4)+56.*T1
         A(I,5)=A(I,6)+56.*T1
         A(I,6)=A(I,7)-28.*T1
         A(I,7)=A(I,8)+8.*T1
         A(I,8)=A(I,9)-1.*T1
      ENDDO
      DO J=1,8
         A(4,J)=A(4,J)+.5*A(5,J)
         A(5,J)=A(6,J)+.5*A(5,J)
         A(6,J)=A(7,J)
         A(7,J)=A(8,J)
         A(8,J)=A(9,J)
      ENDDO
      P(4)=P(4)+.5*P(5)
      P(5)=P(4)+.5*P(5)
      P(6)=P(7)
      P(7)=P(8)
      P(8)=P(9)
      CALL MATIN1(8,A,P)
      P(9)=P(8)
      P(8)=P(7)
      P(7)=P(6)
      P(6)=P(5)
      P(5)=(-1.*P(1)+8.*(P(2)+P(8))-28.*(P(3)+P(7))+
     &      56.*(P(4)+P(6))-1.*P(9))/70.
C----------------------------------------------------------------------
      RETURN
      END
