CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8optm.for,v 1.1 2004/07/06 10:03:45 whitefor Exp $ Date $Date: 2004/07/06 10:03:45 $
CX
       subroutine a8optm(itype,xa,oa,n,s,b0,bp0,f10,f20,f30,xk0,ifail)
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran77 subroutine a8optm *******************
c
c  purpose: to find the best approximate form parameters for neutral
c           atoms by varing the matching position. 
c
c  calling program:  adas108.for
c
c  input:
c            (i*4)  itype    = type of transition (1=dipole,2=non-dipole
c                              non-spin change, 3=spin change, 4=null)
c            (r*8)  xa()     = x-parameters for cross-section
c            (r*8)  oa       = collisions strengths for transition
c            (i*4)  n        = no of collision strengths
c            (r*8)  s        = line strength if dipole transition
c            (i*4)  ifail    = failure code  on entry (ifail=0 two point
c                              fit, ifail=-1 one point fit)
c  output: 
c            (r*8)  b0       = threshold form parameter
c            (r*8)  bp0      = matching parameter
c            (r*8)  f10      = threshold form parameter
c            (r*8)  f20      = asymptotic form parameter
c            (r*8)  f30      = asymptotic form parameter
c            (r*8)  xk0      = optimum matching x-value
c            (i*4)  ifail    = failure code  on exit 
c                             (ifail=0 successful two point fit
c                              ifail=1 converted to one point fit)
c 
c
c  routines: 
c             a8slvf	adas	solves for asymptotic parms f2 and f3
c	      a8slv2	adas    solves for the parms f1,f2,f3,b
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     19/07/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       integer  ndim
c-----------------------------------------------------------------------
       real*8   d1
c-----------------------------------------------------------------------
       parameter ( ndim = 100 , d1 = 1.0d-10 )
c-----------------------------------------------------------------------
       integer  n     , i     , j     , k     , i4unit
       integer  itype , ifail , ifail0
c-----------------------------------------------------------------------
       real*8   b0    , bp0   , f10   , f20   , f30   , xk0
       real*8   b     , bp    , f1    , f2    , f3    , s  
       real*8   x0    , sig0  , xk    , sigk  , xn    , sign   
       real*8   suml 
c-----------------------------------------------------------------------
       real*8   xa(n) , oa(n) , sum(ndim)
c-----------------------------------------------------------------------

        ifail0=ifail
        suml = 1.0d60
        
        if (n.eq.2) then
            x0=xa(1)
            sig0=oa(1)
            xk=xa(2)
            sigk=oa(2)
            call a8slv2( itype, x0  , sig0 , xk  , sigk , xn  , sign ,
     &                   s    , f1  , f2   , f3  , b    , bp  , ifail
     &                  )
            xk0=xk
            b0=b
            bp0=bp
            f10=f1
            f20=f2
            f30=f3
            return
        elseif (n.eq.1) then
            sigk=oa(1)
            xk=xa(1)
            f1=sigk
            b=0.5
            if (itype.eq.1) then
                f3=1.3333333
                f2=dexp(f1/(f3*s))
                bp=(xk-1.0)*f3*s/(f1*f2)
            elseif (itype.eq.2) then
                f2=sigk
                f3=0.0
                bp=-(xk-1.0)*f3/f1
            elseif (itype.eq.3) then
                f2=1.0
                f3=sigk*f2**2
                bp=-2.0*(xk-1.0)*f3*s/(f1*f2**3)
            endif
            xk0=xk
            b0=b
            bp0=bp
            f10=f1
            f20=f2
            f30=f3
            return
         endif

                         
c-----------------------------------------------------------------------
c  cycle through possible xk values 
c-----------------------------------------------------------------------
       x0=xa(1)
       sig0=oa(1)
       xn=xa(n)
       sign=oa(n)
       
       do k=2,n-1
         xk = xa(k)
         sigk=oa(k)
         
         call a8slv2( itype, x0  , sig0 , xk  , sigk , xn  , sign ,
     &                s    , f1  , f2   , f3  , b    , bp  , ifail
     &                  )

c-----------------------------------------------------------------------
c  evaluate sum of residuals
c-----------------------------------------------------------------------
         sum(k)=0.0
         do i=1,n
           if(i.le.k) then
               sum(k)=sum(k)+(1.0-oa(i)/(f1*((xa(i)-1.0)/(xk-1.0))**b*
     &         exp(-(b-bp)*((xa(i)-xk)/(xk-1.0)))))**2
           else
               if (itype.eq.1) then
                   sum(k)=sum(k)+(1.0-oa(i)/(f3*s*dlog(xa(i)-xk+f2)))**2
               elseif (itype.eq.2) then
                   sum(k)=sum(k)+(1.0-oa(i)/(f2+f3/(xa(i)-xk+1.0)))**2
               elseif (itype.eq.3) then
                   sum(k)=sum(k)+(1.0-oa(i)*(xa(i)-xk+f2)**2/f3)**2
               endif              
           endif
         enddo
c       
         if(suml.gt.sum(k)) then
             suml=sum(k)
             xk0=xa(k)
             b0=b
             bp0=bp
             f10=f1
             f20=f2
             f30=f3
         endif
       enddo
c       write(6,*)'b0,bp0,f10,f20,f30,xk0=',
c     &            b0,bp0,f10,f20,f30,xk0
c       
       return
       end
         

             
                 
                 
                
         
       
