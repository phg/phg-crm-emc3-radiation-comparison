CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8gser.for,v 1.1 2004/07/06 10:03:37 whitefor Exp $ Date $Date: 2004/07/06 10:03:37 $
CX
       subroutine a8gser( gamser, a    , x    , gln )
       implicit none
c---------------------------------------------------------------------
c
c  ********************* fortran77 function a8gser *******************
c
c  purpose: to evaluate the series expansion for the incomplete gamma 
c           function p(a,x) - based on numerical recipes  
c
c  calling program:  a8gamp.for
c
c  input:
c            (r*8)  a      = parameter of p(a,x)
c            (r*8)  x      = paramete of p(a,x)
c  output:
c            (r*8)  gamser = incomplete gamma function gamma(a,x)
c                            (n.b. for x<0 takes principal value of 
c                             logarithm)
c            (r*8)  gln    = ln(gamma(a))
c
c
c  routines: 
c             none
c             a8gaml    adas    obtains log(gamma(a))
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     25/06/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       integer itmax
c-----------------------------------------------------------------------
       real*8  eps
c-----------------------------------------------------------------------
       parameter ( itmax = 100 , eps = 3.0d-7 )
c-----------------------------------------------------------------------
       integer n
c-----------------------------------------------------------------------
       real*8  a    , x    , gln  , gamser , ap    , del   , sum
       real*8  a8gaml
c-----------------------------------------------------------------------
       gln = a8gaml(a)
c
       ap = a
       sum = 1.0d0/a
       del = sum
       do n=1,itmax
         ap = ap + 1.0d0
         del = del*x/ap
         sum = sum+del
         if(dabs(del).lt.dabs(sum)*eps) go to 1
       enddo
       
    1  gamser = sum*dexp(-x+a*dlog(dabs(x)))
       
       return
       end
       
