CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/etred9.for,v 1.1 2004/07/06 13:49:50 whitefor Exp $ Date $Date: 2004/07/06 13:49:50 $
CX
       FUNCTION ETRED9( KTYPE , E    , T    , B    , C )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: ETRED9 *********************
C
C  PURPOSE:  TO CALCULATE THE REDUCED ENERGY FOR EIGHT TYPES OF 
C            TRANSITION
C
C  CALLING PROGRAM: ADAS108 
C
C  FUNCTION:
C
C  INPUT:    (R*8)    E      =   TRANSITION ENERGY (Eij)
C            (R*8)    T      =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    B      =   BURGESS SCALING PARAMETER - B
C            (R*8)    C      =   BURGESS SCALING PARAMETER - C
C            (R*8)    TL     =   Ej/Eij
C            (I*4)    KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE	- EXP THRESHOLD
C                                2 NON ELECTRIC DIPOLE	- EXP THRESHOLD
C                                3 SPIN CHANGE		- EXP THRESHOLD
C                                4 OTHER		- EXP THRESHOLD
C                                5 ELECTRIC DIPOLE	- POWER THRESHOLD
C                                6 NON ELECTRIC DIPOLE	- POWER THRESHOLD
C                                7 SPIN CHANGE		- POWER THRESHOLD
C                                8 OTHER		- POWER THRESHOLD
C
C  OUTPUT:   (R*8)    ETRED9 =   REDUCED ENERGY
C
C            (I*4)    IASYMC =   ASYMPTOTIC CLASSIFICATION TYPE
C            (I*4)    ITHRSC =   THRESHOLD CLASSIFICATION TYPE
C            (R*8)    E0     =   SWITCHING ENERGY
C
C 
C  ROUTINES: NONE
C
C  DATE:     17/06/99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C-----------------------------------------------------------------------
       INTEGER KTYPE , IASYMC , ITHRSC , icount
C-----------------------------------------------------------------------
       REAL*8  E     , E0     , T       
       REAL*8  B     , C      , TL     , TL0   , ETRED9
C-----------------------------------------------------------------------
       IF (KTYPE.LE.4) THEN
           IASYMC = KTYPE
           ITHRSC = 1
C           
           IF (IASYMC.EQ.1) THEN
               E0=E*B*C*DLOG(C)/2
           ELSEIF (IASYMC.EQ.2) THEN
               E0=E*B*C/2
           ELSEIF (IASYMC.EQ.3) THEN
               E0=E*B*C/2
           ELSE
               E0=E*B*C*DLOG(C)/2
           ENDIF
C                       
       ELSE
           IASYMC = KTYPE-4
           ITHRSC = 2
C           
           IF (IASYMC.EQ.1) THEN
               E0=E*(B*C*DLOG(C)/2)**0.666667
           ELSEIF (IASYMC.EQ.2) THEN
               E0=E*(B*C/2)**0.666667
           ELSEIF (IASYMC.EQ.3) THEN
               E0=E*(B*C/2)**0.666667
           ELSE
               E0=E*(B*C*DLOG(C)/2)**0.666667
           ENDIF
C                       
       ENDIF
C
       TL  = DABS(T/E)
       TL0 = DABS(E0/E)
C
       IF (T.GE.E0) THEN
           IF (IASYMC.EQ.1.OR.IASYMC.EQ.4)THEN
               ETRED9 = DLOG((TL-TL0+C)/C)/DLOG(TL-TL0+C)
           ELSEIF (IASYMC.EQ.2.OR.IASYMC.EQ.3)THEN
               ETRED9 = (TL-TL0)/(TL-TL0+C)
           ENDIF
       ELSE
           IF (ITHRSC.EQ.1)THEN
               ETRED9 = 2.0*(TL/TL0)**B/((TL/TL0)**B+1.0) - 1.0
           ELSEIF (ITHRSC.EQ.2)THEN
               ETRED9 = DEXP(-B*(1.0/DSQRT(TL)-1.0/DSQRT(TL0)))-1.0
           ENDIF
       ENDIF    
C-----------------------------------------------------------------------
       RETURN
       END
