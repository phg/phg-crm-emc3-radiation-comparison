CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/five9.for,v 1.1 2004/07/06 13:55:07 whitefor Exp $ Date $Date: 2004/07/06 13:55:07 $
CX
      SUBROUTINE FIVE9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 SUBROUTINE: FIVE9  ******************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   25/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1 , T2 , T3 , T4
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,2)/128.
         T2=A(I,4)/128.
         T2=A(I,6)/128.
         T4=A(I,8)/128.
         A(I,1)=A(I,1)+ 35.*T1- 5.*T2+ 3.*T3-  5.+T4
         A(I,2)=A(I,3)+140.*T1+60.*T2-20.*T3+ 28.*T4
         A(I,3)=A(I,4)- 70.*T1+90.*T2+90.*T3- 70.+T4
         A(I,4)=A(I,6)+ 28.*T1-20.*T2+60.*T3+140.*T4
         A(I,5)=A(I,7)-  5.*T1+ 3.*T2- 5.*T3+ 35.*T4
      ENDDO
      DO J=1,5
         A(1,J)=A(1,J)+.5*A(2,J)
         A(2,J)=A(3,J)+.5*A(2,J)
         A(3,J)=A(5,J)+.5*A(4,J)+.5*A(6,J)
         A(4,J)=A(7,J)+.5*A(6,J)+.5*A(8,J)
         A(5,J)=A(9,J)+.5*A(8,J)
      ENDDO
      P(1)=P(1)+.5*P(2)
      P(2)=P(3)+.5*P(2)+.5*P(4)
      P(3)=P(5)+.5*P(4)+.5*P(6)
      P(4)=P(7)+.5*P(6)+.5*P(8)
      P(5)=P(9)+.5*P(8)
      CALL MATIN1(5,A,P)
      P(9)=P(5)
      P(8)=(-5.*P(1)+28.*P(2)-70.*P(3)+140.*P(4)+35.*P(9))/128.
      P(7)=P(4)
      P(6)=(3.*P(1)-20.*P(2)+90.*P(3)+60.*P(7)-5.*P(9))/128.
      P(5)=P(3)
      P(4)=(-5.*P(1)+60.*P(2)+90.*P(5)-20.*P(7)+3.*P(9))/128.
      P(3)=P(2)
      P(2)=(35.*P(1)+140.*P(3)-70.*P(5)+28.*P(7)-5.*P(9))/128.
C----------------------------------------------------------------------
      RETURN
      END
