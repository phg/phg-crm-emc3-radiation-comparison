CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/omeups9.for,v 1.1 2004/07/06 14:25:22 whitefor Exp $ Date $Date: 2004/07/06 14:25:22 $
CX
      FUNCTION OMEUPS9( IT   , E    , B    , C    , P    , X )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE:OMEUPS9********************
C
C  PURPOSE:
C        TO CALCULATE UPSILONS 
C
C  CALLING PROGRAM:
C        MAXW9
C
C  INPUT:
C        (I*4)  IT      = TRANSITION TYPE
C        (R*8)  E       = EXCITATION ENERGY (RYD)
C        (R*8)  B       = BURGESS SCALABLE PARAMETER - B
C        (R*8)  C       = BURGESS SCALABLE PARAMETER - C
C        (R*8)  P()     = SPLINE VALUES AT KNOTS
C        (R*8)  X       = COLLIDING ELECTRON ENERGY AFTER EXCITATION
C  OUTPUT:
C        (R*8)  OMEUPS9 = UPSILON 
C
C  ROUTINES:
C         SP9    - BURGESS SPLINE FIT
C         OMUP9  - 
C
C  DATE:    21/06/95			VERSION 1.1
C  AUTHOR:  A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C           CONVERTED FROM BURGESS BBC BASIC
C
C-----------------------------------------------------------------------  
      INTEGER IT
C-----------------------------------------------------------------------
      REAL*8  E     , B     , C     , X     , OMEUPS9 , ETR   ,  SP
      REAL*8  OMUP9 , ETRED9, SP9 
C-----------------------------------------------------------------------
      REAL*8  P(9)
C----------------------------------------------------------------------- 
      ETR = ETRED9( IT   , E    , X    , B    , C )
      SP  = SP9( P , ETR )
      OMEUPS9 = OMUP9( IT   , E    , X    , SP   , B    , C )
      RETURN
      END

