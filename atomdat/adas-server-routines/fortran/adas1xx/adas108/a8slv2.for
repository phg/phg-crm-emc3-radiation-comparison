CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8slv2.for,v 1.1 2004/07/06 10:04:01 whitefor Exp $ Date $Date: 2004/07/06 10:04:01 $
CX
       subroutine a8slv2( itype, x0  , sig0 , xk  , sigk , xn  , sign ,
     &                    s    , f1  , f2   , f3  , b    , bp  , ifail
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran77 subroutine a8slv2 *******************
c
c  purpose: to find the approximate form parameters for a neutral atoms
c
c  calling program:  adas108.for
c
c  input:
c            (i*4)  itype    = type of transition (1=dipole,2=non-dipole
c                              non-spin change, 3=spin change, 4=null)
c            (r*8)  x0       = x-parameter at first energy point n
c            (r*8)  sig0     = collisions strength at first energy pt. n 
c            (r*8)  xk       = x-parameter at matching point k
c            (r*8)  sigk     = collisions strength at matching point k
c            (r*8)  xn       = x-parameter at last energy point n
c            (r*8)  sign     = collisions strength at last energy pt. n 
c            (r*8)  s        = line strength for type 1 case 
c            (i*4)  ifail    = failure code  on entry (ifail=0 two point
c                              fit, ifail=-1 one point fit)
c  output: 
c            (r*8)  f1       = threshold form parameter
c            (r*8)  f2       = asymptotic form parameter
c            (r*8)  f3       = asymptotic form parameter
c            (r*8)  b        = threshold form parameter
c            (r*8)  bp       = matching parameter
c            (i*4)  ifail    = failure code  on exit 
c                             (ifail=0 successful two point fit
c                              ifail=1 converted to one point fit)
c 
c
c  routines: 
c             a8slvf	adas	solves for asymptotic parms f2 and f3
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     16/06/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       real*8   d1
c-----------------------------------------------------------------------
       parameter ( d1 = 1.0d-10 )
c-----------------------------------------------------------------------
       integer itype  , ifail , ifail0
c-----------------------------------------------------------------------
       real*8   b     , f1    , f2    , f3    , bp    , s
       real*8   x0    , sig0  , xk    , sigk  , xn    , sign    
       real*8   bl    , bu    , bint  , fl    , fu    , f
       real*8   swap
c-----------------------------------------------------------------------
c    find f2 and f3 parameters
c-----------------------------------------------------------------------

       ifail0=ifail

       call a8slvf(itype,xk,sigk,xn,sign,s,f2,f3,ifail)
       
c-----------------------------------------------------------------------
c   find f1 and bp
c-----------------------------------------------------------------------
      
       f1=sigk
       if (itype.eq.1) bp=(xk-1.0)*f3*s/(f1*f2)
       if (itype.eq.2) bp=-(xk-1.0)*f3/f1
       if (itype.eq.3) bp=-2.0*(xk-1.0)*f3/(f1*f2**3)
       
c       if(ifail.eq.1) then 
c           b=0.5
c           return
c       endif
       
       ifail=0
       
           
c-----------------------------------------------------------------------
c   solve implicit equation for b
c-----------------------------------------------------------------------
       
c-----------------------------------------------------------------------
c  bracket zero for b equation 
c-----------------------------------------------------------------------
         bu = 2.0
         bint=0.1
         fu  = sig0-f1*((x0-1.0)/(xk-1.0))**bu*
     &         exp(-(bu-bp)*((x0-xk)/(xk-1.0)))
    5    bl = bu-bint
         fl  = sig0-f1*((x0-1.0)/(xk-1.0))**bl*
     &         exp(-(bl-bp)*((x0-xk)/(xk-1.0)))
         if (fl*fu .gt. 0.0) then
             if (fl .gt. 0.0 .and. fl .lt. fu) then
                 bu = bl
                 fu=fl
                 go to 5
             elseif (fl .gt. 0.0 .and. fl .gt. fu) then
                 bint=-bint
                 go to 5
             elseif (fl .lt. 0.0 .and. fu .lt. fl) then
                 bu = bl
                 fu=fl
                 go to 5
             elseif (fl .lt. 0.0 .and. fu .gt. fl) then
                 bint=-bint
                 go to 5
             endif
         endif
         if(fu.lt.fl) then
             swap=fl
             fl=fu
             fu=swap
             swap=bl
             bl=bu
             bu=swap
         endif
c-----------------------------------------------------------------------
c  now solve by alternated bisection and linear interpolation
c-----------------------------------------------------------------------
    9    b  = (bl*fu-bu*fl)/(fu-fl)                    
         f  = sig0-f1*((x0-1.0)/(xk-1.0))**b*
     &         exp(-(b-bp)*((x0-xk)/(xk-1.0)))
c
         if(f)11,16,10                                 
   10       bu=b                                         
            fu=f                                            
            go to 12                                         
   11       bl=b                                            
            fl=f                                               
   12       b=0.5*(bl+bu)
c                                      
         f  = sig0-f1*((x0-1.0)/(xk-1.0))**b*
     &         exp(-(b-bp)*((x0-xk)/(xk-1.0)))
c
         if(f)14,16,13                                           
   13       bu=b                                                   
            fu=f                                                      
            go to 15                                                   
   14       bl=b                                                      
            fl=f                                                         
   15       b=(bl*fu-bu*fl)/(fu-fl)
            if(dabs(bl-b)/b .gt. d1)go to 9                                  
              
   16    continue 
c                                                      
       return
c---------------------------------------------------------------------
       end
