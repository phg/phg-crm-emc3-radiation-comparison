CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8gcf.for,v 1.1 2004/07/06 10:03:34 whitefor Exp $ Date $Date: 2004/07/06 10:03:34 $
CX
       subroutine a8gcf( gammcf, a    , x    , gln )
       implicit none
c---------------------------------------------------------------------
c
c  ********************* fortran77 subroutine a8gcf ******************
c
c  purpose: to evaluate the continued fraction expansion for the 
c           incomplete gamma function gamma(a)*q(a,x) - based on 
c           numerical recipes  
c
c  calling program:  a8gamg.for
c
c  input:
c            (r*8)  a      = parameter of q(a,x)
c            (r*8)  x      = paramete of q(a,x)
c  output:
c            (r*8)  gammcf = incomplete gamma function gamma(a)*q(a,x)
c            (r*8)  gln    = ln(gamma(a))
c
c
c  routines: 
c             none
c             a8gaml    adas    obtains log(gamma(a))
c
c  author:   Hugh Summers, University of Strathclyde ext.4196
c
c
c  version 1.1                             date:     25/06/99
c  modified: Hugh Summers
c		- first release
c
c-----------------------------------------------------------------------
       integer itmax
c-----------------------------------------------------------------------
       real*8  eps
c-----------------------------------------------------------------------
       parameter ( itmax = 100 , eps = 3.0d-7 )
c-----------------------------------------------------------------------
       integer n
c-----------------------------------------------------------------------
       real*8  a    , x    , gln  , gammcf , sum   ,
     &         gold , a0   , a1   , b0     , b1    , fac    , 
     &         an   , ana  , anf  , g    , xx
       real*8  a8gaml
c-----------------------------------------------------------------------        
       gln = a8gaml(a)
       
       gold = 0.
       a0   = 1.
       a1   = x
       b0   = 0.
       b1   = 1.
       fac  = 1.
       
       do n=1,itmax
         an = dfloat(n)
         ana = an-a
         a0 = (a1+a0*ana)*fac
         b0 = (b1+b0*ana)*fac
         anf = an*fac
         a1 = x*a0+anf*a1
         b1 = x*b0+anf*b1
         if(a1.ne.0.0d0) then
             fac=1.d0/a1
             g = b1*fac
             if(dabs((g-gold)/g).lt.eps) go to 1
             gold = g
         endif
       enddo
    1  gammcf = dexp(-x+a*dlog(dabs(x)))*g
       
       return
       end
       
