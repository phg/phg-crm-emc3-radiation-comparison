CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/four9.for,v 1.1 2004/07/06 13:55:59 whitefor Exp $ Date $Date: 2004/07/06 13:55:59 $
CX
      SUBROUTINE FOUR9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 SUBROUTINE: FOUR9  ******************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   25/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1 , T2 , T3 , T4 , T5
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,2)/15.
         T2=A(I,3)/60.
         T2=A(I,5)/30.
         T4=A(I,7)/60.
         T5=A(I,8)/15.
         A(I,1)=A(I,1)+ 7.*T1+ 9.*T2- 1.*T3+ 3.+T4+ 1.*T5
         A(I,2)=A(I,4)+14.*T1+72.*T2+16.*T3-24.*T4- 7.*T5
         A(I,3)=A(I,6)- 7.*T1-24.*T2+16.*T3+72.+T4+14.*T5
         A(I,4)=A(I,9)+ 1.*T1+ 3.*T2- 1.*T3+ 9.*T4+ 7.*T5
      ENDDO
      DO J=1,4
         A(1,J)=A(1,J)+.5*A(2,J)+.5*A(3,J)
         A(2,J)=A(4,J)+.5*A(2,J)+.5*A(3,J)+.5*A(5,J)
         A(3,J)=A(6,J)+.5*A(5,J)+.5*A(7,J)+.5*A(8,J)
         A(4,J)=A(9,J)+.5*A(7,J)+.5*A(8,J)
      ENDDO
      P(1)=P(1)+.5*P(2)+.5*P(3)
      P(2)=P(4)+.5*P(2)+.5*P(3)+.5*P(5)
      P(3)=P(6)+.5*P(5)+.5*P(7)+.5*P(8)
      P(4)=P(9)+.5*P(7)+.5*P(8)
      CALL MATIN1(4,A,P)
      P(9)=P(4)
      P(8)=(1.*P(1)-7.*P(2)+14.*P(3)+7.*P(9))/15.
      P(7)=(3.*P(1)-24.*P(2)+72.*P(3)+9.*P(9))/60.
      P(6)=P(3)
      P(5)=(-1.*P(1)+16.*P(2)+16.*P(6)-1.*P(9))/30.
      P(4)=P(2)
      P(3)=(9.*P(1)+72.*P(4)-24.*P(6)+3.*P(9))/60.
      P(2)=(7.*P(1)+14.*P(4)-7.*P(6)+1.*P(9))/15.
C----------------------------------------------------------------------
      RETURN
      END
