CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/maxw9.for,v 1.1 2004/07/06 14:20:35 whitefor Exp $ Date $Date: 2004/07/06 14:20:35 $
CX
      SUBROUTINE MAXW9( IT    , B     , C    , E    ,
     &                  P     , NT    , T     , U
     &                )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  ******************  FORTRAN77 SUBROUTINE: MAXW9   *******************
C
C  PURPOSE: GAUSS-LAGUERRE QUADRATURE FROM BURGESS' PROGRAM
C           OMEUPS
C
C  INPUT:
C       (I*4)  IT       = TRANSITION TYPE
C       (R*8)  B        = BURGESS/SUMMERS SCALING PARAMETER - B
C       (R*8)  C        = BURGESS/SUMMERS SCALING PARAMETER - C
C       (R*8)  E        = EXCITATION ENERGY
C       (R*8)  P()      = KNOTS
C       (I*4)  NT       = NUMBER OF TEMPERATURE POINTS
C       (R*8)  T()      = TEMPERATURES (K)
C
C  OUTPUT:
C       (R*8)  U()      = UPSILONS
C
C  ROUTINES:
C    OMEUPS9 - TO CALCULATE THE MAXWELL AVERAGED UPSILONS
C    UPS9    - TO CALCULATE THE MAXWELL AVERAGED UPSILONS FOR ALL
C              TRANSITIONS EXCEPT 'SPIN CHANGE'
C
C  DATE:     24-06-99				VERSION: 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C		- FIRST RELEASE
C
C------------------------------------------------------------------------
      INTEGER I    , N      , NT     , I4UNIT
      INTEGER IT   , IASYMC , ITHRSC , K      , KT
C------------------------------------------------------------------------
      REAL*8  E      , E0     , B      , C      , C1     , C2
      REAL*8  T1     , T2     , T3     , T4     , T5     , TR    ,
     &        XX     , WW     , RI     , YY     , RI1    
      REAL*8  OMEUPS9, UPS9
C------------------------------------------------------------------------
      REAL*8  T(NT)  , U(NT)
      REAL*8  XT(20) , WT(20) , X(20)  , W(20)  , P(9)
C------------------------------------------------------------------------
C  FREE PARAMETER C1 FIXED TO SUGGESTED ONE
C------------------------------------------------------------------------
      DATA C1/3.0/
C------------------------------------------------------------------------
C  20 POINT GAUSS SQRT(X) WEIGHT QUADRATURE FOR THRESHOLD REGION 
C------------------------------------------------------------------------
      DATA KT/20/
      DATA XT/0.0000000E+00,6.3113181E-03,2.5085916E-02,5.5849748E-02,
     &        9.7826048E-02,1.4995494E-01,2.1092022E-01,2.7918254E-01,
     &        3.5301833E-01,4.3056329E-01,5.0985946E-01,5.8890467E-01,
     &        6.6570309E-01,7.3831561E-01,8.0490884E-01,8.6380137E-01,
     &        9.1350626E-01,9.5276869E-01,9.8059811E-01,9.9629820E-01/
      DATA WT/0.0000000E+00,1.0006749E-03,3.9396735E-03,8.6315285E-03,
     &        1.4778960E-02,2.1989919E-02,2.9800076E-02,3.7699487E-02,
     &        4.5161909E-02,5.1675117E-02,5.6770495E-02,6.0050215E-02,
     &        6.1210468E-02,6.0059431E-02,5.6528927E-02,5.0679150E-02,
     &        4.2696159E-02,3.2882390E-02,2.1641386E-02,9.4707005E-03/
C------------------------------------------------------------------------
C  20 POINT GAUSS LAGUERRE QUADRATURE FOR ASYMPTOTIC REGION 
C------------------------------------------------------------------------
      DATA K/20/
      DATA X/7.053989E-2,3.721268E-1,9.165821E-1,1.707307E0,
     &       2.749199E0,4.048925E0,5.615175E0,7.459017E0,
     &       9.594393E0,1.203880E1,1.481429E1,1.794890E1,
     &       2.147879E1,2.545170E1,2.993255E1,3.501343E1,
     &       4.083306E1,4.761999E1,5.581080E1,6.652442E1/
      DATA W/1.687468E-1,2.912544E-1,2.666861E-1,1.660024E-1,
     &       7.482607E-2,2.496442E-2,6.202551E-3,1.144962E-3,
     &       1.557418E-4,1.540144E-5,1.086486E-6,5.330121E-8,
     &       1.757981E-9,3.725503E-11,4.767529E-13,3.372844E-15,
     &       1.155014E-17,1.539522E-20,5.286443E-24,1.656457E-28/
C------------------------------------------------------------------------
       IF (IT.LE.4) THEN
           IASYMC = IT
           ITHRSC = 1
C           
           IF (IASYMC.EQ.1) THEN
               E0=E*B*C*DLOG(C)/2
           ELSEIF (IASYMC.EQ.2) THEN
               E0=E*B*C/2
           ELSEIF (IASYMC.EQ.3) THEN
               E0=E*B*C/2
           ELSE
               E0=E*B*C*DLOG(C)/2
           ENDIF
C                       
       ELSE
           IASYMC = IT-4
           ITHRSC = 2
C           
           IF (IASYMC.EQ.1) THEN
               E0=E*(B*C*DLOG(C)/2)**0.666667
           ELSEIF (IASYMC.EQ.2) THEN
               E0=E*(B*C/2)**0.666667
           ELSEIF (IASYMC.EQ.3) THEN
               E0=E*(B*C/2)**0.666667
           ELSE
               E0=E*(B*C*DLOG(C)/2)**0.666667
           ENDIF
       ENDIF
C
      IF (IT.EQ.3.OR.IT.EQ.7) THEN
         XX=C-1
         T1=P(5)
         T2=P(6)/(1+XX*.25)**2
         T3=P(7)/(1+XX*.5)**2
         T4=P(8)/(1+XX*.75)**2
         T5=P(9)/C**2
         RI1=C*(T1+4*T2+2*T3+4*T4+T5)/12

         RI=0
         DO I=1,K
            XX=X(I)
            WW=W(I)*DEXP(XX)
c            XX=(XX*E+E0)*C1
            XX=XX*E*C1+E0
            YY=OMEUPS9(IT,E,B,C,P,XX)
            RI=RI+WW*YY
         ENDDO
         C2=RI1/RI
c         write(i4unit(-1),*)'maxw9.for: c1,c2=',c1,c2
      ENDIF
C
      DO I=1,NT
         TR=T(I)/157888.0D0
         U(I)=UPS9( TR    , IT    , E     , E0   , B     , C     ,
     &              P     , C1    , C2    ,
     &              KT    , XT    , WT    , K    , X     , W    
     &             )
c         write(i4unit(-1),*)'maxw9.for: i,tr,ups9,=',i,tr,u(i)
      ENDDO
      RETURN
      END






