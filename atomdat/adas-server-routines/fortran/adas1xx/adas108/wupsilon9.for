CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/wupsilon9.for,v 1.1 2004/07/06 15:29:46 whitefor Exp $ Date $Date: 2004/07/06 15:29:46 $
CX
        SUBROUTINE WUPSILON9( ELO   , EHI   , DELTAE, KTYPE , GF    , 
     &                       BPAR  , CPAR  , YKN   , I1    , I2    ,
     &                       NELEC , NCHAR , NTEMP , T     , UPS   ,
     &                       DSFULL, ISTDIM, NET   , ENTE  , OMUP  ,
     &                       IWRITE, INDIM , WI    , WJ
     &                     )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE:WUPSILON ******************
C
C  PURPOSE: TO WRITE DATA TO AN OLD/NEW ARCHIVE IN BURGESS FORMAT
C
C  INPUT:
C        (R*8) ELO    - LOWER LEVEL ENERGY
C        (R*8) EHI    - UPPER LEVEL ENERGY
C        (R*8) DELTAE - TRANSITION ENERGY
C        (I)   KTYPE  - TRANSITION TYPE
C        (R*8) GF     - WEIGHTED OSCILLATOR STRENGTH
C        (R*8) BPAR   - BURGESS SCALING PARAMETER - B 
C        (R*8) CPAR   - BURGESS SCALING PARAMETER - C 
C        (R*8) YKN    - KNOT POINTS
C        (I)   I1     - LOWER LEVEL INDEX
C        (I)   I2     - UPPER LEVEL INDEX
C        (R*8) NELEC  - NUMBER OF ELECTRONS
C        (R*8) NCHAR  - NUCLEAR CHARGE
C        (I)   NTEMP  - NUMBER OF TEMPERATURE POINTS
C        (R*8) T      - TEMPERATURES
C        (R*8) UPS    - UPSILONS
C        (C*80)DSFULL - ARCHIVE FILE NAME
C        (I)   ISTDIM - MAXIMUM INPUT ARRAY DIMENSIONS
C        (I)   NET    - NUMBER OF ENERGY POINTS
C        (R*8) ENTE   - ENRGIES
C        (R*8) OMUP   - OMEGAS
C        (I)   IWRITE - OUTPUT UNIT NUMBER
C        (I)   INDIM  - MAXIMUM OUTPUT ARRAY DIMENSION
C        (R*8) WI     - LOWER LEVEL STATISTICAL WEIGHT
C        (R*8) WJ     - UPPER LEVEL STATISTICAL WEIGHT
C
C  OUTPUT:
C
C  DATA:  
C         CIARR(500)*80- 500 IS THE CURRENT LIMIT ON INDEXES
C  ROUTINES:
C         NONE
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C  DATE: 18/06/99				VERSION 1.1
C
C-----------------------------------------------------------------------
       INTEGER I1  , I2 , NET 
       INTEGER ITOUT , ISTDIM, INDIM , IWRITE, NTEMP 
       INTEGER KTYPE , I
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80, CSTRING*6
C-----------------------------------------------------------------------
       REAL*8  ELO , EHI, EL, EH, DELTA, DELTAE , GF , YTEST 
       REAL*8  BPAR , CPAR, YY1, YY2, NELEC, NCHAR , WI, WJ
C-----------------------------------------------------------------------
       REAL*8 YKN(9)  , ENTE(ISTDIM) , OMUP(ISTDIM) , T(ISTDIM)
       REAL*8 UPS(INDIM)
C-----------------------------------------------------------------------
       CHARACTER LABEL*60
C-----------------------------------------------------------------------
        YY1 = 0.0
        YY2 = 0.0
          DO 100 I = 1,NTEMP
            YTEST = UPS(I)
            YY1 = MIN(YY1,YTEST)
            YY2 = MAX(YY2,YTEST)
 100      CONTINUE
        WRITE(IWRITE,12) 'OMEGA'
C NEXT 2 LINES SHOULD BE FORMAT(,11) 
        WRITE(IWRITE,17) NELEC
        WRITE(IWRITE,17) NCHAR
        EL=ELO*109737.150
        WRITE(IWRITE,18) EL,WI
        EH=EHI*109737.150
        WRITE(IWRITE,18) EH,WJ
        DELTA=ABS(DELTAE*109737.150)
        WRITE(IWRITE,2) DELTA
        WRITE(IWRITE,9) KTYPE
        WRITE(IWRITE,3) GF
        WRITE(IWRITE,13) BPAR,CPAR
        WRITE(IWRITE,5) (YKN(I),I=1,9)
        WRITE(IWRITE,6) YY1,YY2
        WRITE(IWRITE,7) I1,I2
        LABEL = '  ADAS108 '
        WRITE(IWRITE,12) LABEL
        WRITE(IWRITE,15) NET
        DO I = 1,NET
          WRITE(IWRITE,16)ENTE(I),OMUP(I)
        END DO
        WRITE(IWRITE,12)'UPSILON'
        WRITE(IWRITE,11) NTEMP
        DO I=1,NTEMP
           WRITE(IWRITE,10) T(I),UPS(I)
        END DO
C-----------------------------------------------------------------------
  2     FORMAT(5X,F10.3)
  3     FORMAT(2X,1P,E10.4)
  5     FORMAT(1P,9E12.3)
  6     FORMAT(1P,2E12.2)
  7     FORMAT(2I3)
  9     FORMAT(1X,I4)
 10     FORMAT(1P,2E12.4)
 11     FORMAT(I5)
 12     FORMAT(1X,A)
 13     FORMAT(1X,F8.5,1X,F8.5)
 14     FORMAT(1P,10E12.4)
 15     FORMAT(I5)
 16     FORMAT(1P,2E12.3)
 17     FORMAT(1X,F5.2)
 18     FORMAT(5X,F10.3,5X,F5.1)
        RETURN
        END
