CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/a8afit.for,v 1.3 2007/05/15 11:17:38 allan Exp $ Date $Date: 2007/05/15 11:17:38 $
CX
       SUBROUTINE A8AFIT  ( Z0    , Z     , ZEFF  , IETYP , 
     &                      IXTYP , IND1  , IND2  , WI, WJ, EI, EJ,  
     &                      IATYP , ACOEFF, IFTYP , IOTYP , IFOUT ,
     &                      IXMAX , ITMAX , EDAT  , XDAT  , TDAT  ,
     &                      IIORD , IIBTS , IIFPT , IIXOP , IIDIF , 
     &                      BXC   , BPXC  ,FXC1   , 
     &                      FXC2  , FXC3  , XKC   , 
     &                      XA    , YA    , APOMA ,
     &                      DIFOMA, TOA   , GOA   , APGOA , EXCRA ,
     &                      DEXCRA, GBARFA,  
     &                      ICT   , ITOUT , S     , FIJ   , EIJ   
     &                     )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  PURPOSE: TO ANALYSE ELECTRON IMPACT CROSS-SECTION DATA AND CONVERT TO 
C  RATE COEFFICIENTS                                                    
C                                                                       
C  VARIOUS FORMS OF DATA ENTRY ARE ALLOWED                              
C                                                                       
C  DATA IS FITTED WITH APPROXIMATE FORMS TO AID INTERPOLATION DEPENDING 
C  ON THE TRANSITION TYPE. THESE ARE                                    
C                 1. DIPOLE                                             
C                 2. NON-DIPOLE                                         
C                 3. SPIN CHANGE                                        
C                 4. OTHER                                              
C                                                                       
C  DATA ENTRY IS VIA CALL TO PANEL SUBROUTINE  SPFMA4E  AS FOLLOWS:     
C                                                                       
C  INPUT                                                                
C                                                                       
C  OUTPUT                                                               
C      Z0     = NUCLEAR CHARGE OF ION                                   
C      Z      = ION CHARGE                                              
C      ZEFF   = ION CHARGE + 1                                          
C      IETYP  = 1  LEVEL ENERGIES IN CM-1                               
C             = 2  LEVEL ENERGIES IN RYD                                
C      IXTYP  = 1  DIPOLE TRANSITION                                    
C             = 2  NON-DIPOLE TRANSITION                                
C             = 3  SPIN CHANGE TRANSITION                               
C             = 4  OTHER                                                
C      IND1   = LOWER LEVEL INDEX  (USER CHOICE)                        
C      IND2   = UPPER LEVEL INDEX  (USER CHOICE)                        
C      WI     = LOWER LEVEL STATISTICAL WEIGHT                          
C      WJ     = UPPER LEVEL STATISTICAL WEIGHT                          
C      EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS)                  
C      EJ     = UPPER LEVEL ENERGY                                      
C      IATYP  = 1  A-COEFFICIENT RETURNED                               
C             = 2  OSCILLATOR STRENGTH RETURNED                         
C             = 3  LINE STRENGTH RETURNED                               
C      ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM, DIPOLE CASE ONLY)
C      IFTYP  = 1 UPPER K**2 (RYD) FOR COLLISION ENERGY UNITS           
C             = 2 LOWER K**2 (RYD)                                      
C             = 3 UPPER (K/Z0)**2 (RYD)                                
C             = 4 X PARAMETER                                           
C             = 5 UPPER (K/ZEFF)**2 (RYD)                               
C      IOTYP  = 1 EXCITATION CROSS-SECTION (PI*A0**2) RETURNED          
C             = 2 DE-EXCITATION CROSS-SECTION (PI*A0**2) RETURNED       
C             = 3 COLLISION STRENGTH RETURNED                           
C             = 4 SCALED COLLISION STRENGTH (Z**2*OMEGA) RETURNED       
C      IFOUT  = 1 KELVIN FOR OUTPUT TEMPERATURE UNIT                    
C             = 2   EV   FOR OUTPUT TEMPERATURE UNIT                   
C             = 3 SCALED UNITS RETURNED  (TE(K)/Z1**2)                  
C             = 4 REDUCED UNITS RETURNED (KTE/EIJ)                      
C      IXMAX  = NUMBER OF ENERGY/X-SECT PAIRS ENTERED                   
C      ITMAX  = NUMBER OF OUTPUT TEMPERATURES ENTERED                   
C      EDAT(I)= INPUT ENERGIES (SELECTED UNITS)                         
C      XDAT(I)= INPUT X-SECTS  (SELECTED UNITS)                         
C      TDAT(I)= OUTPUT TEMPS.  (SELECTED UNITS)                         
C      IIORD  = 1  4-PT GAUSS-LAGUERRE QUADRATURE                       
C             = 2  8-PT GAUSS-LAGUERRE QUADRATURE                       
C             = 3 12-PT GAUSS-LAGUERRE QUADRATURE                       
C      IIGPH  = 0  NO X-SECT GRAPH TO BE PRODUCED                       
C             = 1     X-SECT GRAPH TO BE PRODUCED                       
C      IIGPG  = 0  NO GAMMA  GRAPH TO BE PRODUCED                       
C             = 1     GAMMA  GRAPH TO BE PRODUCED                       
C      IIBTS  = 0  BAD POINT OPTION OFF                                 
C             = 1  BAD POINT OPTION ON                                  
C      IIFPT  = 1  SELECT ONE POINT OPTIMISING                          
C             = 2  SELECT TWO POINT OPTIMISING                          
C      IIXOP  = 0  OPTIMISING OFF                                       
C             = 1  OPTIMISING ON  (IF ALLOWED)                          
C      IIDIF  = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY WITH OPTIMISING)
C             = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT                 
C                                                                       
C                                                                       
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE                                                                       
C 
C  DATE:     16-06-99				VERSION 1.1
C  MODIFIED: HUGH SUMMERS
C           	 -FIRST RELEASE
C
C  DATE:   07/07/2004			VERSION: 1.2
C  MODIFIED: ALLAN WHITEFORD
C		- CHANGED PARAMS108 TO PARAMS
C
C  DATE:   15/05/2007			VERSION: 1.3
C  MODIFIED: ALLAN WHITEFORD
C		- UPDATED COMMENTS AS PART OF SUBROUTINE
C                 DOCUMENTATION PRODUCTION.
C
C-----------------------------------------------------------------------
       INCLUDE   'PARAMS'
C-----------------------------------------------------------------------
       INTEGER   I4UNIT, IFAIL
       INTEGER   IETYP , IXTYP  , IND1  , IND2  ,  
     &           IATYP , IFTYP  , IOTYP , IFOUT ,
     &           IXMAX , ITMAX  , 
     &           IIORD , IIBTS  , IIFPT , IIXOP , IIDIF , 
     &           ICT   , ITOUT 
       INTEGER   I     , J      , K     , IT    , IQORD ,
     &           IFPTS , IXOPT  , ITER  , IDIFF ,IBPTS  ,
     &           IFORMS, IENDS  , ICOUT  
       INTEGER   IEND1 , IENDN 
C-----------------------------------------------------------------------
       REAL*8    Z0      , Z       , Z1    , ZEFF  , 
     &           WI      , WJ      , EI    , EJ    ,  
     &           ACOEFF  , S       , FIJ   , EIJ   ,   
     &           BXC     , BPXC    , FXC1  ,
     &           FXC2    , FXC3    , XKC
       REAL*8    SUM     , TE      , ATE   , X     , Y     , DY    ,
     &           X1      , DX1     , XN    , DXN   , XE
       REAL*8    EEI     , EE2  
       REAL*8    G1      , GN      ,
     &           A1      , B1      , AN    , BN    
C-----------------------------------------------------------------------  
       INTEGER   NQORD(3)
C-----------------------------------------------------------------------
       REAL*8    C1(ISTDIM,ISTDIM-1),C2(ISTDIM,ISTDIM-1),
     &           C3(ISTDIM,ISTDIM-1),C4(ISTDIM,ISTDIM-1)                
       REAL*8    APGOA(ISTDIM),XA(ISTDIM),YA(ISTDIM),
     &           XOA(ISTDIM),YOA(ISTDIM)                
       REAL*8    XSA(ISTDIM),YSA(ISTDIM),
     &           XOS(ISTDIM),YOS(ISTDIM)
C-----------------------------------------------------------------------             
       REAL*8    WG(12,3)   ,XG(12,3)
       REAL*8    TOA(ISTDIM),GOA(ISTDIM)             
       REAL*8    APOUT(ISTDIM),APOMA(ISTDIM),DIFOMA(ISTDIM)
       REAL*8    EXCRA(ISTDIM),DEXCRA(ISTDIM),GBARFA(ISTDIM)
       REAL*8    EDAT(ISTDIM),XDAT(ISTDIM),TDAT(ISTDIM)
C----------------------------------------------------------------------- 
       DO 1 I =1,ISTDIM
         TOA(I) = 0.0
         GOA(I) = 0.0
 1     CONTINUE
C              
       Z1=Z+1.0D0
       ZEFF=Z1                                                       
C       FXC2=0.0D0                                                       
C       FXC3=0.0D0                                                       
       S=0.0D0                                                          
       FIJ=0.0D0                                                        
C-----------------------------------------------------------------------
C  CONVERSION OF INPUT AND OUTPUT DATA FORMS TO STANDARD                
C-----------------------------------------------------------------------
       IF(IETYP.EQ.1)THEN                                               
           EI=EI/1.09737D5                                              
           EJ=EJ/1.09737D5                                              
       ENDIF                                                            
       EIJ=EJ-EI                                                        
       IF(IXTYP.EQ.1.or.ixtyp.eq.5)THEN                                               
           IF(IATYP.EQ.2)THEN                                           
               ACOEFF=8.03232E9*EIJ*EIJ*WI*ACOEFF/WJ                    
           ELSEIF(IATYP.EQ.3)THEN                                       
               ACOEFF=2.67744E9*EIJ*EIJ*EIJ*ACOEFF/WJ                   
           ENDIF 
           S=3.73491E-10*WJ*ACOEFF/(EIJ*EIJ*EIJ)                        
           FIJ=3.333333D-1*EIJ*S/WI                                     
       ENDIF                                                            
C                                                     
       DO 40 ICT=1,IXMAX                                                 
        X=EDAT(ICT)                                                     
        Y=XDAT(ICT)                                                     
        IF(IFTYP.EQ.1)THEN                                              
            XA(ICT)=X/EIJ+1.0D0                                         
        ELSEIF(IFTYP.EQ.2)THEN                                          
            XA(ICT)=X/EIJ                                               
        ELSEIF(IFTYP.EQ.3)THEN                                          
            XA(ICT)=Z0*Z0*X/EIJ+1.0D0                                   
        ELSEIF(IFTYP.EQ.4)THEN                                          
            XA(ICT)=X                                                   
        ELSEIF(IFTYP.EQ.5)THEN                                          
            XA(ICT)=ZEFF*ZEFF*X/EIJ+1.0D0                               
        ENDIF                                                           
        IF(IOTYP.EQ.1)THEN                                              
            YA(ICT)=Y*WI*EIJ*XA(ICT)                                    
        ELSEIF(IOTYP.EQ.2)THEN                                          
            YA(ICT)=Y*WJ*EIJ*(XA(ICT)-1.0D0)                            
        ELSEIF(IOTYP.EQ.3)THEN                                          
            YA(ICT)=Y                                                   
        ELSEIF(IOTYP.EQ.4)THEN                                          
            YA(ICT)=Y/(ZEFF*ZEFF)                                       
        ENDIF                                                              
   40  CONTINUE                                                          
       ICT=IXMAX                                                         
C                                                                       
       DO 55 ITOUT=1,ITMAX                                              
        TE=TDAT(ITOUT)                                                  
        IF(IFOUT.EQ.1)THEN                                              
            TOA(ITOUT)=TE                                               
        ELSEIF(IFOUT.EQ.2)THEN                                          
            TOA(ITOUT)=1.16054D4*TE                                     
        ELSEIF(IFOUT.EQ.3)THEN                                          
            TOA(ITOUT)=TE*Z1*Z1                                         
        ELSEIF(IFOUT.EQ.4)THEN                                          
            TOA(ITOUT)=1.5789D5*TE*EIJ                                  
        ENDIF                                                           
   55  CONTINUE                                                         
       ITOUT=ITMAX                                                      
       IQORD=IIORD
C-----------------------------------------------------------------------
C  CONVERT TO BEST VARIABLES FOR SPLINING DEPENDING UPON TRANSITION TYPE
C-----------------------------------------------------------------------
       IF(IXTYP.EQ.1.or.ixtyp.eq.5)THEN                                               
           IFPTS=IIFPT                        
c   72      IF(ICT.LE.1.OR.YA(1).GE.YA(ICT).OR.IFPTS.EQ.1)THEN           
c               IFPTS=1                                                  
c               FXC3=1.333333D0                                          
c               FXC2=DEXP(YA(1)/(FXC3*S))-XA(1)                         
   72      IF(IFPTS.EQ.1)THEN           
               fxc2 = 0.0
               ifail=-1
               call a8optm(ixtyp,xa,ya,ict,s,bxc,bpxc,fxc1,
     &                     fxc2,fxc3,xkc,ifail)
               IF(FXC2.GT.0.0D0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IXOPT=0                                              
               ENDIF                                                    
           ELSEIF(IFPTS.EQ.2)THEN
               fxc2 = 0.0
               ifail=0
               call a8optm(ixtyp,xa,ya,ict,s,bxc,bpxc,fxc1,
     &                     fxc2,fxc3,xkc,ifail)
               IF(FXC2.GT.0.0D0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IFPTS=1                                              
                   GO TO 72                                             
               ENDIF                                                    
           ENDIF                                                        
           IF(IXOPT.GT.0)THEN                                           
               IDIFF=IIDIF                                              
           ELSE                                                         
               IDIFF=1                                                  
               FXC2=0.0D0                                               
               FXC3=1.333333D0                                          
           ENDIF                                                        
       ELSEIF(IXTYP.EQ.2.AND.IIXOP.EQ.1)THEN                            
           IFPTS=IIFPT                                                  
c   74      IF(ICT.LE.1.OR.IFPTS.EQ.1)THEN                               
c               FXC2=0.0D0                                               
c               FXC3=YA(1)                                               
   74      IF(IFPTS.EQ.1)THEN                               
               ifail=-1
               call a8optm(ixtyp,xa,ya,ict,s,bxc,bpxc,fxc1,
     &                     fxc2,fxc3,xkc,ifail)
               IXOPT=IIXOP                                              
           ELSEIF(IFPTS.EQ.2)THEN                                       
               ifail=0
               call a8optm(ixtyp,xa,ya,ict,s,bxc,bpxc,fxc1,
     &                     fxc2,fxc3,xkc,ifail)
               IF(FXC2+FXC3.GT.0.0D0.AND.FXC3.GT.0.0D0)THEN              
                   IXOPT=IIXOP                                          
               ELSE                                                    
                   IFPTS=1                                              
                   GO TO 74                                             
               ENDIF                                                    
           ENDIF                                                        
           IDIFF=0                                                      
       ELSEIF(IXTYP.EQ.3.AND.IIXOP.EQ.1)THEN                            
           IFPTS=IIFPT                                                  
c   76      IF(ICT.LE.1.OR.YA(1).LE.YA(ICT).OR.IFPTS.EQ.1)THEN           
c               IFPTS=1                                                  
c               FXC2=100.0                                               
c               FXC3=YA(1)*(XA(1)+FXC2)*(XA(1)+FXC2)                     
   76      IF(IFPTS.EQ.1)THEN           
               ifail=-1                                       
               call a8optm(ixtyp,xa,ya,ict,s,bxc,bpxc,fxc1,
     &                     fxc2,fxc3,xkc,ifail)
               IXOPT=IIXOP                                              
           ELSEIF(IFPTS.EQ.2)THEN
               ifail=0                                       
               call a8optm(ixtyp,xa,ya,ict,s,bxc,bpxc,fxc1,
     &                     fxc2,fxc3,xkc,ifail)
               IF(FXC2.GT.0.0D0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IFPTS=1                                              
                   GO TO 76                                            
               ENDIF                                                    
           ENDIF                                                        
           IDIFF=0                                                      
       ELSEIF(IXTYP.EQ.4)THEN                                           
           IDIFF=1                                                      
       ELSEIF(IIXOP.EQ.0)THEN                                           
           FXC2=0.0D0                                                   
           FXC3=0.0D0                                                   
           IXOPT=IIXOP                                                  
           IFPTS=0                                                      
           IDIFF=1                                                      
       ENDIF                                                            
C                                                                       
       DO 75 K=1,ICT                                                   
        IF(IXTYP.EQ.1.or.ixtyp.eq.5)THEN                                              
            if(xa(k).le.xkc) then
               apoma(k)=fxc1*((xa(k)-1.0)/(xkc-1.0))**bxc*
     &         dexp(-(bxc-bpxc)*((xa(k)-xkc)/(xkc-1.0)))
            else
                apoma(k)=fxc3*s*dlog(xa(k)-xkc+fxc2)
            endif
            IF(IXOPT.EQ.1.AND.IDIFF.EQ.0)THEN                           
                YSA(K)=YA(K)/APOMA(K)                                   
            ELSE                                                        
                YSA(K)=YA(K)-APOMA(K)                                   
            ENDIF                                                       
            XSA(K)=1.0D0/XA(K)
        ELSEIF(IXTYP.EQ.2.AND.IXOPT.EQ.1)THEN                           
            if(xa(k).le.xkc) then
               apoma(k)=fxc1*((xa(k)-1.0)/(xkc-1.0))**bxc*
     &         dexp(-(bxc-bpxc)*((xa(k)-xkc)/(xkc-1.0)))
            else
                apoma(k)=fxc2+fxc3/(xa(k)-xkc+1.0)
            endif
            xsa(k)=1.0d0/xa(k)
            ysa(k)=ya(k)/apoma(k)                                   
        ELSEIF(IXTYP.EQ.3.AND.IXOPT.EQ.1)THEN                           
            if(xa(k).le.xkc) then
               apoma(k)=fxc1*((xa(k)-1.0)/(xkc-1.0))**bxc*
     &         dexp(-(bxc-bpxc)*((xa(k)-xkc)/(xkc-1.0)))
            else
                apoma(k)=fxc3/(xa(k)-xkc+fxc2)**2
            endif
            xsa(k)=1.0d0/xa(k)
            ysa(k)=ya(k)/apoma(k)                                   
        ELSEIF(IXTYP.EQ.4)THEN                                          
            APOMA(K)=0.0D0                                              
            YSA(K)=DLOG10(YA(K))                                        
            XSA(K)=1.0/XA(K)                                            
        ELSEIF(IXOPT.EQ.0)THEN                                          
            APOMA(K)=0.0D0                                              
            YSA(K)=YA(K)                                                
            XSA(K)=1.0D0/XA(K)                                          
        ENDIF                                                           
        DIFOMA(K)=YA(K)-APOMA(K)                                        
   75  CONTINUE                                           
       CALL ESORT(XSA,YSA,ICT)                                          
       IBPTS=IIBTS 
c
       call a8amax( ixtyp , ibpts , idiff ,
     &              s     , eij   , wi    , wj    ,   
     &              bxc   , bpxc  , fxc1  , 
     &              fxc2  , fxc3  , xkc   , 
     &              ict   , xsa   , ysa   ,
     &              itout , toa   , goa   , apgoa , excra ,
     &              dexcra, gbarfa
     &             )  
                                                     
c
       RETURN
C------------------------------------------------------------------------
  251  FORMAT(1H0,'** WARNING:-TYPE 3, FXC2 TOO SMALL-LIMITED TO 0.1 **'
     & )                                                                
      END                                                               
