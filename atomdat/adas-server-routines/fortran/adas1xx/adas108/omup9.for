CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/omup9.for,v 1.1 2004/07/06 14:25:41 whitefor Exp $ Date $Date: 2004/07/06 14:25:41 $
CX
      FUNCTION OMUP9( KTYPE , E    , T    , U    , B     , C )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: OMUP9 ********************
C
C  PURPOSE:
C        TO CALCULATE UPSILONS FOR DIFFERENT TRANSITIONS
C
C  CALLING PROGRAM:
C        OMEUPS
C
C  INPUT:
C        (I*4)   KTYPE  = TRANSITION TYPE 
C        (R*8)   E      = EXCITATION ENERGY (RYD)
C        (R*8)   T      = SCALED ENERGY VALUE OF QUADRATURE FIXED POINTS
C        (R*8)   U      = SPLINE FIT TO THE KNOT POINTS AT RED. ENERGIES 
C        (R*8)   B      = BURGESS SCALABLE PARAMETER - B
C        (R*8)   C      = BURGESS SCALABLE PARAMETER - C
C  OUTPUT:
C        (R*8)  OMUP9   = UPSILON
C
C
C  DATE:   25/05/99				VERSION 1.1
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C-----------------------------------------------------------------------
      INTEGER KTYPE , IASYMC , ITHRSC
C-----------------------------------------------------------------------
      REAL*8  E    , E0   , T    , U    , B    , C    , TL   , TL0
      REAL*8  OMUP9
C-----------------------------------------------------------------------
       IF (KTYPE.LE.4) THEN
           IASYMC = KTYPE
           ITHRSC = 1
C           
           IF (IASYMC.EQ.1) THEN
               E0=E*B*C*DLOG(C)/2
           ELSEIF (IASYMC.EQ.2) THEN
               E0=E*B*C/2
           ELSEIF (IASYMC.EQ.3) THEN
               E0=E*B*C/2
           ELSE
               E0=E*B*C*DLOG(C)/2
           ENDIF
C                       
       ELSE
           IASYMC = KTYPE-4
           ITHRSC = 2
C           
           IF (IASYMC.EQ.1) THEN
               E0=E*(B*C*DLOG(C)/2)**0.666667
           ELSEIF (IASYMC.EQ.2) THEN
               E0=E*(B*C/2)**0.666667
           ELSEIF (IASYMC.EQ.3) THEN
               E0=E*(B*C/2)**0.666667
           ELSE
               E0=E*(B*C*DLOG(C)/2)**0.666667
           ENDIF
C                       
       ENDIF
C
       TL  = DABS(T/E)
       TL0 = DABS(E0/E)
C
       IF (E.GE.E0) THEN
           IF (IASYMC.EQ.1)THEN
               OMUP9 = U*DLOG(TL-TL0+2.718281828)
           ELSEIF (IASYMC.EQ.2)THEN
               OMUP9 = U
           ELSEIF (IASYMC.EQ.3)THEN
               OMUP9 = U/((TL-TL0+1)*(TL-TL0+1))
           ELSEIF (IASYMC.EQ.4)THEN
               OMUP9 = U*DLOG(TL-TL0+C)
           ENDIF
       ELSE
           IF (ITHRSC.EQ.1)THEN
               OMUP9 = U*(TL/TL0)**B
               IF (IASYMC.EQ.1)THEN
                  OMUP9 = U*(TL/TL0)**B*DEXP(
     &                       -(B-(E0/(2.718281828*E)))*((TL/TL0)-1)) 
               ELSEIF (IASYMC.EQ.2)THEN
                  OMUP9 = U*(TL/TL0)**B*DEXP(
     &                       -B*((TL/TL0)-1)) 
               ELSEIF (IASYMC.EQ.3)THEN
                  OMUP9 = U*(TL/TL0)**B*DEXP(
     &                       -(B+(2.0*E0/E))*((TL/TL0)-1)) 
               ELSEIF (IASYMC.EQ.4)THEN
                  OMUP9 = U*(TL/TL0)**B*DEXP(
     &                       -(B-(E0/(C*DLOG(C)*E)))*((TL/TL0)-1)) 
               ENDIF 
           ELSEIF (ITHRSC.EQ.2)THEN
               OMUP9 = U*DEXP(-B*(1.0/DSQRT(TL)-1.0/DSQRT(TL0)))
           ENDIF
       ENDIF    
C----------------------------------------------------------------------
      RETURN
      END

