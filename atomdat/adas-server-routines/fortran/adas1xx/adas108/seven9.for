CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/seven9.for,v 1.1 2004/07/06 15:19:38 whitefor Exp $ Date $Date: 2004/07/06 15:19:38 $ 
CX
      SUBROUTINE SEVEN9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 SUBROUTINE: SEVEN9 *******************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   25/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1 , T2
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,3)/56.
         T2=A(I,7)/56.
         A(I,1)=A(I,1)- 3.*T1+ 1.*T2
         A(I,2)=A(I,2)+20.*T1- 4.*T2
         A(I,3)=A(I,4)+84.*T1+28.*T2
         A(I,4)=A(I,5)-70.*T1-70.*T2
         A(I,5)=A(I,6)+28.*T1+84.*T2
         A(I,6)=A(I,8)- 4.*T1+20.*T2
         A(I,7)=A(I,9)+ 1.*T1- 3.*T2
      ENDDO
      DO J=1,7
         A(2,J)=A(2,J)+.5*A(3,J)
         A(3,J)=A(4,J)+.5*A(3,J)
         A(4,J)=A(5,J)
         A(5,J)=A(6,J)+.5*A(7,J)
         A(6,J)=A(8,J)+.5*A(7,J)
         A(7,J)=A(9,J)
      ENDDO
      P(2)=P(2)+.5*P(3)
      P(3)=P(4)+.5*P(3)
      P(4)=P(5)
      P(5)=P(6)+.5*P(7)
      P(6)=P(8)+.5*P(7)
      P(7)=P(9)
      CALL MATIN1(7,A,P)
      P(9)=P(7)
      P(8)=P(6)
      P(7)=(1.*P(1)-4.*P(2)+28.*P(3)-70.*P(4)+84.*P(6)
     &      +20.*P(8)-3.*P(9))/56.
      P(6)=P(5)
      P(5)=P(4)
      P(4)=P(3)
      P(3)=(-3.*P(1)+20.*P(2)+84.*P(3)-70.*P(4)+28.*P(6)
     &      -4.*P(8)+1.*P(9))/56.
C----------------------------------------------------------------------
      RETURN
      END
