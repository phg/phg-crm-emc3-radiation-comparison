CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/six9.for,v 1.1 2004/07/06 15:19:57 whitefor Exp $ Date $Date: 2004/07/06 15:19:57 $
CX
      SUBROUTINE SIX9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 SUBROUTINE: SIX9  *******************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   25/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1 , T2 , T3
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,2)/36.
         T2=A(I,5)/90.
         T2=A(I,8)/36.
         A(I,1)=A(I,1)+ 7.*T1+ 1.*T2+ 1.*T3
         A(I,2)=A(I,3)+70.*T1-20.*T2-14.*T3
         A(I,3)=A(I,4)-56.*T1+64.*T2+28.*T3
         A(I,4)=A(I,6)+28.*T1+64.*T2-56.*T3
         A(I,5)=A(I,7)-14.*T1-20.*T2+70.*T3
         A(I,6)=A(I,9)+ 1.*T1+ 1.*T2+ 7.*T3
      ENDDO
      DO J=1,6
         A(1,J)=A(1,J)+.5*A(2,J)
         A(2,J)=A(3,J)+.5*A(2,J)
         A(3,J)=A(4,J)+.5*A(5,J)
         A(4,J)=A(6,J)+.5*A(5,J)
         A(5,J)=A(7,J)+.5*A(8,J)
         A(6,J)=A(9,J)+.5*A(8,J)
      ENDDO
      P(1)=P(1)+.5*P(2)
      P(2)=P(3)+.5*P(2)
      P(3)=P(4)+.5*P(5)
      P(4)=P(6)+.5*P(5)
      P(5)=P(7)+.5*P(8)
      P(6)=P(9)+.5*P(9)
      CALL MATIN1(6,A,P)
      P(9)=P(6)
      P(8)=(1.*P(1)-14.*P(2)+28.*P(3)-56.*P(4)+70.*P(5)
     &      +7.*P(9))/36.
      P(7)=P(6)
      P(6)=P(5)
      P(5)=(1.*P(1)-20.*P(2)+64.*P(3)+64.*P(6)-20.*P(7)
     &      +1.*P(9))/90.
      P(4)=P(3)
      P(3)=P(2)
      P(2)=(7.*P(1)+70.*P(3)-56.*P(4)+28.*P(6)-14.*P(7)
     &      +1.*P(9))/36.
C----------------------------------------------------------------------
      RETURN
      END
