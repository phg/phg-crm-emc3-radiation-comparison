CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/oured9.for,v 1.1 2004/07/06 14:26:42 whitefor Exp $ Date $Date: 2004/07/06 14:26:42 $
CX
       FUNCTION OURED9( KTYPE, EIJ  , EJ   , OMUP , B    , C    )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: OURED9 ********************
C
C  PURPOSE:  TO CALCULATE THE REDUCED COLLISION STRENGTH AS A FUNCTION
C            OF Ej/Eij FOR EIGHT TYPES OF TRANSITION
C
C  CALLING PROGRAM: VARIOUS ADAS108 ROUTINES
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (RYD)
C            (R*8)    EJ     =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (RYD)
C            (R*8)    C      =   BURGESS SCALING PARAMETER -C 
C            (R*8)    C      =   BURGESS SCALING PARAMETER -C 
C            (R*8)    ETR    =   Ej/Eij
C            (R*8)    OMUP   =   COLLISION STRENGTH AS A FUNCTION 
C                                OF ETR
C            (I*4)    KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE	- EXP THRESHOLD
C                                2 NON ELECTRIC DIPOLE	- EXP THRESHOLD
C                                3 SPIN CHANGE		- EXP THRESHOLD
C                                4 OTHER		- EXP THRESHOLD
C                                5 ELECTRIC DIPOLE	- POWER THRESHOLD
C                                6 NON ELECTRIC DIPOLE	- POWER THRESHOLD
C                                7 SPIN CHANGE		- POWER THRESHOLD
C                                8 OTHER		- POWER THRESHOLD
C
C  OUTPUT:   (R*8)    OURED9 =   REDUCED COLLISION STRENGTH
C
C            (I*4)    IASYMC =   ASYMPTOTIC CLASSIFICATION TYPE
C            (I*4)    ITHRSC =   THRESHOLD CLASSIFICATION TYPE
C            (R*8)    E0     =   SWITCHING ENERGY
C
C 
C  ROUTINES: NONE
C
C  DATE:     17/06/99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C-----------------------------------------------------------------------
       INTEGER KTYPE , IASYMC , ITHRSC , icount
C-----------------------------------------------------------------------
       REAL*8  EIJ   , EJ     , E0    , B      , C     , TL    , TL0 
       REAL*8  OMUP  , OURED9 
C-----------------------------------------------------------------------
       IF (KTYPE.LE.4) THEN
           IASYMC = KTYPE
           ITHRSC = 1
C           
           IF (IASYMC.EQ.1) THEN
               E0=EIJ*B*C*DLOG(C)/2
           ELSEIF (IASYMC.EQ.2) THEN
               E0=EIJ*B*C/2
           ELSEIF (IASYMC.EQ.3) THEN
               E0=EIJ*B*C/2
           ELSE
               E0=EIJ*B*C*DLOG(C)/2
           ENDIF
C                       
       ELSE
           IASYMC = KTYPE-4
           ITHRSC = 2
C           
           IF (IASYMC.EQ.1) THEN
               E0=EIJ*(B*C*DLOG(C)/2)**0.666667
           ELSEIF (IASYMC.EQ.2) THEN
               E0=EIJ*(B*C/2)**0.666667
           ELSEIF (IASYMC.EQ.3) THEN
               E0=EIJ*(B*C/2)**0.666667
           ELSE
               E0=EIJ*(B*C*DLOG(C)/2)**0.666667
           ENDIF
C                       
       ENDIF
C
       TL  = DABS(EJ/EIJ)
       TL0 = DABS(E0/EIJ)
C
       IF (EJ.GE.E0) THEN
           IF (IASYMC.EQ.1)THEN
               OURED9 = OMUP/DLOG(TL-TL0+2.718281828)
           ELSEIF (IASYMC.EQ.2)THEN
               OURED9 = OMUP
           ELSEIF (IASYMC.EQ.3)THEN
               OURED9 = OMUP*(TL-TL0+1.0)*(TL-TL0+1.0)
           ELSEIF (IASYMC.EQ.4)THEN
               OURED9 = OMUP/DLOG(TL-TL0+C)
           ENDIF
       ELSE
           IF (ITHRSC.EQ.1)THEN
               IF (IASYMC.EQ.1)THEN
                  OURED9 = OMUP/((TL/TL0)**B*DEXP(
     &                     -(B-(E0/(2.718281828*EIJ)))*((TL/TL0)-1))) 
               ELSEIF (IASYMC.EQ.2)THEN
                  OURED9 = OMUP/((TL/TL0)**B*DEXP(
     &                     -B*((TL/TL0)-1))) 
               ELSEIF (IASYMC.EQ.3)THEN
                  OURED9 = OMUP/((TL/TL0)**B*DEXP(
     &                     -(B+(2.0*E0/EIJ))*((TL/TL0)-1))) 
               ELSEIF (IASYMC.EQ.4)THEN
                  OURED9 = OMUP/((TL/TL0)**B*DEXP(
     &                     -(B-(E0/(C*DLOG(C)*EIJ)))*((TL/TL0)-1))) 
               ENDIF 
           ELSEIF (ITHRSC.EQ.2)THEN
               OURED9 = OMUP/DEXP(-B*(1.0/DSQRT(TL)-1.0/DSQRT(TL0)))
           ENDIF
       ENDIF    
C-----------------------------------------------------------------------
       RETURN
       END
