CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/ups9.for,v 1.1 2004/07/06 15:27:16 whitefor Exp $ Date $Date: 2004/07/06 15:27:16 $
CX
       FUNCTION UPS9( T   , IT  , E   , E0  , B   , C   ,
     &                P   , C1  , C2  ,
     &                KT  , XT  , WT  , K   , X   , W   
     &              )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: UPS9  *********************
C
C  PURPOSE:  
C        TO CALCULATE UPSILONS.
C
C  CALLING PROGRAM:
C        MAXW9
C
C  INPUT:  
C        (R*8)    T    = TEMPERATURE (RYD. ENERGY UNITS)
C        (I*4)    IT   = TRANSITION TYPE
C        (R*8)    E    = EXCITATION ENERGY
C        (R*8)    E0   = SWITCHING ENERGY BETWEEN THRES. AND ASYMP.
C        (R*8)    B    = BURGESS/SUMMERS SCALING PARAMETE - B
C        (R*8)    C    = BURGESS/SUMMERS SCALING PARAMETE - C
C        (R*8)    P(9) = VALUES AT SPLINE KNOTS
C        (R*8)    C1   = SCALING PARAMETERS
C        (R*8)    C2   = SCALING PARAMETERS
C        (I*4)    KT   = NUMBER OF QUADRATURE POINTS - THRES. TYPE
C        (R*8)    XT() = FIXED QUADRATURE POINTS - THRES. TYPE
C        (R*8)    WT() = FIXED QUADRATURE POINTS - THRES. TYPE
C        (I*4)    K    = NUMBER OF QUADRATURE POINTS - ASYMP. TYPE
C        (R*8)    X()  = FIXED QUADRATURE POINTS - ASYMP. TYPE
C        (R*8)    W()  = FIXED QUADRATURE POINTS - ASYMP. TYPE
C
C  OUTPUT:
C        (R*8)    UPS9 - UPSILON
C
C
C  ROUTINES: NONE
C
C  DATE:     24/06/99				VERSION 1.1
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C		- FIRST RELEASE
C
C-----------------------------------------------------------------------
       INTEGER  K      , KT
       INTEGER  I      , IT
       INTEGER  I4UNIT
C----------------------------------------------------------------------- 
       REAL*8   XT(20) , WT(20) , X(20)  , W(20)  , P(9)
C----------------------------------------------------------------------- 
       REAL*8   T      , E      , E0     , B      , C      , 
     &          C1     , C2    
       REAL*8   UPS9   , OMEUPS9, RT     , RA     , XX     , YY     , 
     &          WW     , ft     , fa     ,omeg   
C-----------------------------------------------------------------------
       RT=0
c       DO I=1,KT
c          XX=XT(I)*T/E0
c          WW=WT(I)*DEXP(-XX)
c          RT=RT+WW*OMEUPS9(IT,E,B,C,P,XX)
c        ENDDO
       DO I=2,KT
          XX=XT(I)*E0
          WW=WT(I)*DEXP(-XX/T)/DSQRT(XT(I))
          omeg=omeups9(it,e,b,c,p,xx)
          RT=RT+WW*OMEUPS9(IT,E,B,C,P,XX)
c         write(i4unit(-1),*)'ups9.for: i,xx,omeg=', i,xx,omeg
       ENDDO
C        
       RA=0
       DO I=1,K
          XX=X(I)*T+E0
          WW=W(I)
           IF (IT .EQ. 3 .OR. IT .EQ.7) THEN
              XX=XX/(1.+T/(E*C1))
C             WW=WW*(E*C2/(T+E*C2))*DEXP(XX/(E*C1))
              YY=X(I)*T
              YY=YY/(1.+T/(E*C1))
              WW=WW*(E*C2/(T+E*C2))*DEXP(YY/(E*C1))
           ENDIF
           omeg=omeups9(it,e,b,c,p,xx)
           RA=RA+WW*OMEUPS9(IT,E,B,C,P,XX)
c           write(i4unit(-1),*)'ups9.for: i,xx,omeg=', i,xx,omeg
         ENDDO
        UPS9=(E0/T)*RT+DEXP(-E0/T)*RA
        ft = (e0/t)*rt
        fa = dexp(-e0/t)*ra
c        write(i4unit(-1),*)'ups9.for: it,ft,fa,ups9=',it,ft,fa,ups9
C-----------------------------------------------------------------------
      RETURN
      END

