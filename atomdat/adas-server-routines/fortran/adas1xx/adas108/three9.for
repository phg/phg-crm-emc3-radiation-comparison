CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas108/three9.for,v 1.1 2004/07/06 15:25:44 whitefor Exp $ Date $Date: 2004/07/06 15:25:44 $
CX
      SUBROUTINE THREE9(A,P)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN 77 SUBROUTINE: THREE9  ******************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ9
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) P - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   26/05/99				VERSION 1.1
C
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C
C----------------------------------------------------------------------
      REAL*8 A(9,9),P(9)
      REAL*8 T1 , T2 , T3 , T4 , T5 , T6
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,9
         T1=A(I,2)/32.
         T2=A(I,3)/32.
         T3=A(I,4)/32.
         T4=A(I,6)/32.
         T5=A(I,7)/32.
         T6=A(I,8)/32.
         A(I,1)=A(I,1)+21.*T1+12.*T2+ 5.*T3- 3.+T4- 4.*T5- 3.*T6
         A(I,2)=A(I,5)+14.*T1+24.*T2+30.*T3+30.*T4+24.*T5+14.*T6
         A(I,3)=A(I,9)- 3.*T1- 4.*T2- 3.*T3+ 5.+T4+12.*T5+21.*T6
      ENDDO
      DO J=1,3
         A(1,J)=A(1,J)+.5*A(2,J)+.5*A(3,J)+.5*A(4,J)
         A(2,J)=A(5,J)+.5*A(2,J)+.5*A(3,J)+.5*A(4,J)+
     &          .5*A(6,J)+.5*A(7,J)+.5*A(8,J)
         A(3,J)=A(9,J)+.5*A(6,J)+.5*A(7,J)+.5*A(8,J)
      ENDDO
      P(1)=P(1)+.5*P(2)+.5*P(3)+.5*P(4)
      P(2)=P(5)+.5*P(2)+.5*P(3)+.5*P(4)+.5*P(6)+.5*P(7)+.5*P(8)
      P(3)=P(9)+.5*P(6)+.5*P(7)+.5*P(8)
      CALL MATIN1(3,A,P)
      P(9)=P(3)
      P(8)=(-3.*P(1)+14.*P(2)+21.*P(9))/32.
      P(7)=(-4.*P(1)+24.*P(2)+12.*P(9))/32.
      P(6)=(-3.*P(1)+30.*P(2)+5.*P(9))/32.
      P(5)=P(2)
      P(4)=(5.*P(1)+30.*P(5)-3.*P(9))/32.
      P(3)=(12.*P(1)+24.*P(5)-4.*P(9))/32.
      P(2)=(21.*P(1)+14.*P(5)-3.*P(9))/32.
C----------------------------------------------------------------------
      RETURN
      END
