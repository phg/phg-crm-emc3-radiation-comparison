CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/lstsq.for,v 1.1 2004/07/06 14:13:44 whitefor Exp $ Date $Date: 2004/07/06 14:13:44 $
CX
      SUBROUTINE LSTSQ(IT,C,E12,GF,N,T,U,B,RMS)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  ******************** FORTRAN77 SUBROUTINE: LSTSQ ********************
C
C  PURPOSE: TO PERFORM FIVE POINT SPLINE FIT TO REDUCED OMEGAS
C
C  INPUT:
C           E12  TRANSITION ENERGY  (RYD)
C           GF   GF
C           IT   TRANSITION TYPE
C           T    ENERGY
C           U    OMEGA
C           N    NUMBER OF DATA POINTS
C           C    C PARAMETER
C
C  OUTPUT:
C           B    KNOTS VALUES
C
C  LOCAL VARIABLES/CONSTANTS:
C
C           A(,)
C           V
C           W
C           Y()
C           XX
C           YY
C
C  CALLS:
C          SPLS     - CALCULATE CUBIC SPLINE FIT COEFFICIENTS
C          ETRED    - CALCULATE REDUCED ENERGIES
C          OURED    - CALCULATE REDUCED OMEGAS
C          MATIN1   - INVERT MATRIX TO GET KNOT POINTS
C          ONE      - GET KNOT POINTS IF ONLY ONE DATA POINT
C          TWO      - GET KNOT POINTS IF ONLY TWO DATA POINTS
C          THREE    - GET KNOT POINTS IF ONLY THREE DATA POINTS
C          FOUR     - GET KNOT POINTS IF ONLY FOUR DATA POINTS
C
C  DATE: 02-07-95				VERSION 1.1
C  WRITTEN: A.LANZAFAME & D.H.BROOKS
C           UNIV.OF STRATHCLYDE, 
C           CONVERTED FROM BBC BASIC
C-----------------------------------------------------------------------
      INTEGER IT,N
      INTEGER I,J,L,K,I4UNIT
C-----------------------------------------------------------------------
      REAL*8 E12,GF,T(N),U(N),C
      REAL*8 A(5,5),B(5),Y(5),B1(5)
      REAL*8 W,E,U1,V,XX,YY,T1,T2,R,RMS
      REAL*8 ETRED,OURED
C-----------------------------------------------------------------------
      W=3.0
      RMS = 0.0
      E=DABS(E12)
C-----------------------------------------------------------------------
C  INITIALISE
C-----------------------------------------------------------------------
      IF (GF .EQ. 0) THEN
         U1=-1
         GOTO 3310
      ENDIF
      IF (IT .EQ. 1 .OR. IT.EQ.4) THEN
         U1=4*GF/E
      ELSE IF (IT .EQ. 2) THEN
         U1=GF
      ELSE
         U1=GF/E**2
      ENDIF
      V=1./U1
      XX = 1.
      CALL SPLS(XX,Y)
 3310 CONTINUE
      DO I=1,5
         IF (U1.GE.0) THEN
            B(I)=W*V*Y(I) 
         ELSE 
            B(I)=0
         ENDIF
         DO J=1,5
            IF (U1.GE.0) THEN
               A(I,J)=W*V*Y(I)*V*Y(J)
            ELSE
               A(I,J)=0
            ENDIF
         ENDDO
      ENDDO
      IF (N.EQ.0) GOTO 3470
      DO I=1,N
         XX=ETRED(IT,E,T(I),C)
         YY=OURED(IT,E,T(I),U(I),C)
         CALL SPLS(XX,Y)
         V=1./YY
         DO K=1,5
            B(K)=B(K)+V*Y(K)
            DO J=1,5
               A(K,J)=A(K,J)+V*Y(K)*V*Y(J)
            ENDDO
         ENDDO
      ENDDO
 3470 CONTINUE
      DO I=1,5
         B1(I)=B(I)
      ENDDO
      IF (U1.LT.0) THEN 
         L=N
      ELSE
         L=N+1
      ENDIF
      IF (L .GT. 4)THEN
         CALL MATIN1(5,A,B)
      ELSE IF (L .EQ. 1) THEN
         CALL ONE(A,B)
      ELSE IF (L .EQ. 2) THEN
         CALL TWO(A,B)
      ELSE IF (L .EQ. 3) THEN
         CALL THREE(A,B)
      ELSE
         CALL FOUR(A,B)
      ENDIF
      T1=DBLE(N)
      T2=T1
      DO I=1,5
         T1=T1-B1(I)*B(I)
      ENDDO
      R=DSQRT(DABS(T1)/T2)
      RMS = 100*R
C      WRITE(*,*)'C = ',C
C      WRITE(I4UNIT(-1),*)'R.M.S.= ',100*R

      RETURN
      END
