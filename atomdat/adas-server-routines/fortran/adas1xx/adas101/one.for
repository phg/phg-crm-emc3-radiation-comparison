CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/one.for,v 1.1 2004/07/06 14:25:44 whitefor Exp $ Date $Date: 2004/07/06 14:25:44 $
CX
      SUBROUTINE ONE(A,B)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77SUBROUTINE: ONE  *********************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) B - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   21/06/95			VERSION 1.1
C
C  AUTHOR: A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C          CONVERTED FROM BURGESS BBC BASIC
C
C-----------------------------------------------------------------------
      INTEGER I,J
C-----------------------------------------------------------------------
      REAL*8 A(5,5),B(5)
      REAL*8 T1,T2
C-----------------------------------------------------------------------
      T1=0
      T2=0
      DO I=1,5
         DO J=1,5
            T1=T1+A(I,J)
         ENDDO
         T2=T2+B(I)
      ENDDO
      T1=T2/T1
      B(1)=T1
      B(2)=T1
      B(3)=T1
      B(4)=T1
      B(5)=T1
C-----------------------------------------------------------------------
      RETURN
      END
