CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/spls.for,v 1.2 2004/07/06 15:22:17 whitefor Exp $ Date $Date: 2004/07/06 15:22:17 $
CX
      SUBROUTINE SPLS(XIN,Y)
      IMPLICIT NONE
C---------------------------------------------------------------------
C  ******************* FORTRAN 77 SUBROUTINE SPLS ********************
C
C  PURPOSE:
C         COMPUTE 5-POINTS CUBIC SPLINE COEFFICIENTS
C  INPUT:
C         (R*8) XIN - THE POINT WHERE THE SPLINE IS TO BE EVALUATED
C
C  OUTPUT:
C         (R*8) Y - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C         NONE
C
C  DATE:    02-07-95				VERSION 1.1
C
C  WRITTEN: A.LANZAFAME & D.H. BROOKS, UNIV.OF STRAYTHCLYDE, 
C           CONVERTED FROM BURGESS BBC BASIC
C
C  DATE:    13-05-99				VERSION 1.2
C  AUTHOR:  HUGH SUMMERS, UNIV.OF STRAYTHCLYDE, 
C           CORRECTED MODIFICATION OF INPUT PARAMETER X ON RETURN
C
C---------------------------------------------------------------------
      REAL*8 X ,   XIN
      REAL*8 Y(5)
      REAL*8 A,B,C,D,E
C---------------------------------------------------------------------
      INTEGER I,  I4UNIT
C---------------------------------------------------------------------
      X=XIN
C---------------------------------------------------------------------   
      I=IDINT(4.0*X)+1
      IF (I.GT.4) I=4
      IF (I.LT.1) I=1
      IF (I.EQ.1) THEN
         X=X-.125
         a=(1216*X-480)*X+41
         b=(-2752*X+480)*X+103
         c=1920*X*X-30
         d=-448*X*X+7
         e=64*X*X-1
      ELSE IF (I.EQ.2) THEN
         X=X-.375
         a=((-2048*X+448)*X+32)*X-7
         b=((6656*X-256)*X-584)*X+64
         c=((-7680*X-960)*X+600)*X+75
         d=((3584*X+896)*X-56)*X-14
         e=((-512*X-128)*X+8)*X+2
      ELSE IF (I.EQ.3) THEN 
         X=X-.625
         a=((512*X-128)*X-8)*X+2
         b=((-3584*X+896)*X+56)*X-14
         c=((7680*X-960)*X-600)*X+75
         d=((-6656*X-256)*X+584)*X+64
         e=((2048*X+448)*X-32)*X-7
      ELSE IF (I.EQ.4) THEN
         X=X-.875
         a=64*X*X-1
         b=-448*X*X+7
         c=1920*X*X-30
         d=(-2752*X-480)*X+103
         e=(1216*X+480)*X+41
      ELSE
         WRITE(I4UNIT(-1),*)'ERROR IN SPLS: I OUT OF RANGE'
      ENDIF
      X=1./120.
      Y(1)=a*X
      Y(2)=b*X
      Y(3)=c*X
      Y(4)=d*X
      Y(5)=e*X
C---------------------------------------------------------------------
      RETURN
      END
