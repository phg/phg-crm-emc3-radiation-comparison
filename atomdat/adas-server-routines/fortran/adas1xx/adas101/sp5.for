CX UNIX0 PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/sp5.for,v 1.2 2004/07/06 15:20:37 whitefor Exp $ Date $Date: 2004/07/06 15:20:37 $
CX
      FUNCTION SP5(P,X)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: SP5 ***********************
C
C  PURPOSE:  TO CALCULATE A SPLINE THROUGH THE FIVE KNOT POINTS
C
C  CALLING PROGRAM:
C        OMEUPS
C
C
C  INPUT:   
C        (R*8) P    - THE FIVE KNOT POINTS
C        (R*8) X    - THE POINT AT WHICH TO EVALUATE THE SPLINE
C  OUTPUT:   
C        (R*8) SP5  - THE EVALUATED SPLINE
C
C  ROUTINES: NONE
C
C  DATE:     21/06/95				VERSION 1.1
C  AUTHOR:   A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C
C  DATE:     13/05/99				VERSION 1.2
C  AUTHOR:   HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C            CORRECTED SERIOUS ERROR IN IF JUMPS AND MODIFICATION OF 
C            INPUT VARIABLE X ON RETURN.  MADE CONSTANTS DOUBLE PREC.
C
C
C------------------------------------------------------------------------
      REAL*8 A,B,C,D,E,P(5),X0,X,SP5
      REAL*8 F,G,H
C------------------------------------------------------------------------
      X0=X
C

      A=P(1)
      B=P(2)
      C=P(3)
      D=P(4)
      E=P(5)
      F=16.0D0*(19.0D0*A-43.0D0*B+30.0D0*C-7.0D0*D+E)/15.0D0
      G=16.0D0*(-A+7.0D0*B-12.0D0*C+7.0D0*D-E)/3.0D0
      H=16.0D0*(A-7.0D0*B+30.0D0*C-43.0D0*D+19.0D0*E)/15.0D0
      IF (X0.GT.0.25D0) GOTO 4820
      X0=X0-0.125D0
      D=0.5D0*(A+B)-F/128.0D0
      C=4.0D0*(B-A)
      B=0.5D0*F
      A=0.0D0
      GOTO 4870
 4820 CONTINUE
      IF (X0.GT.0.5D0) GOTO 4840
      X0=X0-0.375D0
      D=0.5D0*(B+C)-(F+G)/256.0D0
      C=4.0D0*(C-B)-(G-F)/96.0D0
      B=0.25D0*(F+G)
      A=2.0D0*(G-F)/3.0D0
      GOTO 4870
 4840 CONTINUE
      IF (X0.GT.0.75D0) GOTO 4860
      X0=X0-0.625D0
      A=2.0D0*(H-G)/3.0D0
      B=0.25D0*(H+G)
      E=C
      C=4.0D0*(D-C)-A/64.0D0
      D=0.5D0*(E+D)-B/64.0D0
      GOTO 4870
 4860 CONTINUE
      X0=X0-0.875D0
      A=0.0D0
      B=0.5D0*H
      C=4.0D0*(E-D)
      D=0.5D0*(E+D)-B/64.0D0
 4870 CONTINUE
      SP5=D+X0*(C+X0*(B+X0*A))
C-----------------------------------------------------------------------
      RETURN
      END
