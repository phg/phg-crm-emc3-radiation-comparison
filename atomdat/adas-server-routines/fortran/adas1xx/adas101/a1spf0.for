CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/a1spf0.for,v 1.1 2004/07/06 09:59:58 whitefor Exp $ Date $Date: 2004/07/06 09:59:58 $
CX
       SUBROUTINE A1SPF0( REP     , DSFULL  ,
     &                    IASEL   , INDXREF
     &                  )

       IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: A1SPF0 ********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL. READS ARCHIVING OPTION
C           AND INDEX/ARCHIVE NAME IF REQUESTED.
C
C  CALLING PROGRAM: ADAS101
C
C  INPUT:
C          NONE
C  OUTPUT:
C          (C*3)  REP    = 'YES' - FINISH PROGRAM RUN
C                          'NO ' - CONTINUE
C          (C*80) DSFULL = THE SELECTED ARCHIVE NAME.
C          (I*4)  IASEL  = THE ARCHIVING OPTION
C                          0 = IGNORE ARCHIVING.
C                          1 = EXAMINE OLD ARCHIVE AND POSSIBLY
C                              ADD NEW DATA.
C                          2 = REFRESH TRANSITION DATA FROM 
C                              OLD ARCHIVE (REQUIRES INDEX NO.)
C                          3 = CREATE A NEW ARCHIVE.
C          (I*4)  INDXREF= THE INDEX NUMBER TO REFRESH DATA FROM.
C
C  ROUTINES:
C
C  AUTHOR: DAVID H.BROOKS (UNIV.OF STRATHCLYDE) EXT.4213/4205
C
C  DATE: 26/05/95 			VERSION 1.1
C  MODIFIED: DAVID H.BROOKS
C		- FIRST RELEASE
C
C-----------------------------------------------------------------------
       CHARACTER REP*3   , DSFULL*80
C-----------------------------------------------------------------------
       INTEGER IASEL   , INDXREF
       INTEGER PIPEIN  , PIPEOU
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN=5, PIPEOU=6 )
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
       READ(PIPEIN,'(1A3)') REP
       READ(PIPEIN,'(I5)') IASEL
       IF (IASEL.NE.0)THEN
           READ(PIPEIN,'(A)') DSFULL
           IF (IASEL.EQ.2)THEN
               READ(PIPEIN,'(I5)') INDXREF
           ENDIF
       ENDIF
C
C-----------------------------------------------------------------------
C
       RETURN
C
       END
