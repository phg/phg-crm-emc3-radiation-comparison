CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/two.for,v 1.1 2004/07/06 15:26:55 whitefor Exp $ Date $Date: 2004/07/06 15:26:55 $
CX
      SUBROUTINE TWO(A,B)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77SUBROUTINE: TWO  *********************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS 
C
C  CALLING PROGRAM: 
C        LSTSQ
C
C  INPUT:  
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) B - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   21/06/95			VERSION 1.1
C
C  AUTHOR: A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C          CONVERTED FROM BURGESS BBC BASIC
C
C-------------------------------------------------------------------
      INTEGER I,J
      REAL*8 A(5,5),B(5)
C-----------------------------------------------------------------------       
      DO I=1,5
         A(I,1)=A(I,1)+.75*A(I,2)+.5*A(I,3)+.25*A(I,4)
         A(I,2)=.25*A(I,2)+.5*A(I,3)+.75*A(I,4)+A(I,5)
      ENDDO
      DO J=1,2
         A(1,J)=A(1,J)+A(2,J)+.5*A(3,J)
         A(2,J)=A(5,J)+A(4,J)+.5*A(3,J)
      ENDDO
      B(1)=B(1)+B(2)+.5*B(3)
      B(2)=B(5)+B(4)+.5*B(3)
      CALL MATIN1(2,A,B)
      B(5)=B(2)
      B(2)=.75*B(1)+.25*B(5)
      B(3)=.5*(B(1)+B(5))
      B(4)=.25*B(1)+.75*B(5)
C-----------------------------------------------------------------------  
      RETURN
      END

