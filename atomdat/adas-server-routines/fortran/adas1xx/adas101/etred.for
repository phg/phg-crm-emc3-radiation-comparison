CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/etred.for,v 1.1 2004/07/06 13:49:48 whitefor Exp $ Date $Date: 2004/07/06 13:49:48 $
CX
       FUNCTION ETRED( KTYPE , E , T ,  C)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: ETRED *********************
C
C  PURPOSE:  TO CALCULATE THE REDUCED ENERGY FOR FOUR TYPES OF 
C            TRANSITION
C
C  CALLING PROGRAM: VARIOUS ADAS101 CODES
C
C  FUNCTION:
C
C  INPUT:    (R*8)    E      =   TRANSITION ENERGY (Eij)
C            (R*8)    T      =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    TL     =   Ej/Eij
C            (I)      KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C
C  OUTPUT:   (R*8)    ETRED  =   REDUCED ENERGY
C
C 
C  ROUTINES: NONE
C
C  DATE:     21/06/95				VERSION 1.1
C  AUTHOR:   A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C
C-----------------------------------------------------------------------
       INTEGER KTYPE
C-----------------------------------------------------------------------
       REAL*8 E , T , C , TL , ETRED
C----------------------------------------------------------------------- 
       TL = DABS(T/E)
C
       IF (KTYPE.EQ.1.OR.KTYPE.EQ.4)THEN
          ETRED = DLOG((TL+C)/C)/DLOG(TL+C)
       ELSE IF (KTYPE.EQ.2.OR.KTYPE.EQ.3)THEN
          ETRED=TL/(TL+C)
       ENDIF
C-----------------------------------------------------------------------
       RETURN
       END
