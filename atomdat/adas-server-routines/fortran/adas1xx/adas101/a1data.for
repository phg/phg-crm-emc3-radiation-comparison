CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/a1data.for,v 1.3 2004/07/06 09:58:33 whitefor Exp $ Date $Date: 2004/07/06 09:58:33 $
CX
       SUBROUTINE A1DATA( DSFULL , INDXREF , TITLE , CAMETH , Z0    ,
     &                    Z      , ZEFF    , INDL  , INDU   , EI    , 
     &                    EJ     , WI      , WJ    , ACOEFF , S     ,
     &                    FIJ    , EIJ     , IXTYP , FXC2   , FXC3  ,
     &                    IXOPS  , IBPTS   , IFPTS , IDIFF  , ICT   , 
     &                    ITOUT  , XA , YA , APOMA , DIFOMA , TOA   ,
     &                    GOA    , APGOA   , EXCRA , DEXCRA , GBARFA,
     &                    ISTDIM , IREAD   , IZ    , IZ0    , GF    ,
     &                    BCVAL  
     &                )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A1DATA *******************
C
C  PURPOSE: TO REFRESH A DATA INDEX FROM AN ADAS101 ARCHIVE. READS
C           IN THE INDEX CODE A-ADAS, B-BURGESS AND THE THE REST OF
C           THE DATA AS APPROPRIATE.
C
C  CALLING PROGRAM: 
C            ADAS101.FOR
C
C  INPUT:
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                              FILE.
C            (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                              TWO TYPES OF ANALYSIS.
C                              A - ADAS, B- BURGESS
C            (R*8)  GF       - THE WEIGHTED OSCILLATOR STRENGTH
C            (R*8)  BCVAL    - THE BURGESS SCALABLE PARAMETER C.
C            (I*4)  ISTDIM = THE MAXIMUM ARRAY DIMENSION
C            (I*4)  IREAD  = THE INPUT UNIT
C 
C  OUTPUTS: 
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION  
C            (R*8)  Z      = ION CHARGE    
C            (R*8)  ZEFF   = ION CHARGE + 1 
C            (I*4)  INDL   = LOWER LEVEL INDEX  (USER CHOICE)    
C            (I*4)  INDU   = UPPER LEVEL INDEX  (USER CHOICE)  
C            (R*8)  WI     = LOWER LEVEL STATISTICAL WEIGHT          
C            (R*8)  WJ     = UPPER LEVEL STATISTICAL WEIGHT     
C            (R*8)  EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS)      
C            (R*8)  EJ     = UPPER LEVEL ENERGY  
C            (R*8)  ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM,
C                            DIPOLE CASE ONLY)
C            (I*4)  IXTYP  = 1  DIPOLE TRANSITION                
C                          = 2  NON-DIPOLE TRANSITION   
C                          = 3  SPIN CHANGE TRANSITION                
C                          = 4  OTHER     
C            (I*4)  IBPTS  = 0  BAD POINT OPTION OFF                    
C                          = 1  BAD POINT OPTION ON                      
C            (I*4)  IFPTS  = 1  SELECT ONE POINT OPTIMISING            
C                          = 2  SELECT TWO POINT OPTIMISING                 
C            (I*4)  IXOPS  = 0  OPTIMISING OFF                             
C                          = 1  OPTIMISING ON  (IF ALLOWED)                 
C            (I*4)  IDIFF  = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY
C                               WITH OPTIMISING)
C                          = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT
C            (R*8)  S      = LINE STRENGTH
C            (R*8)  FIJ    = OSCILLATOR STRENGTH
C            (R*8)  EIJ    = TRANSITION ENERGY
C            (R*8)  FXC2   = SPLINING VARIABLE
C            (R*8)  FXC3   = SPLINING VARIABLE
C            (I*4)  ICT    = NUMBER OF X-SECTIONS
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (R*8)  XA     = ENERGY (PARAMETER X)
C            (R*8)  YA     = OMEGA (COLLISION STRENGTH)
C            (R*8)  APOMA  = APPROXIMATE OMEGA
C            (R*8)  DIFOMA = DIFFERENCE BETWEEN YA & APOMA
C            (R*8)  TOA    = TEMPERATURE SET
C            (R*8)  GOA    = GAMMA (EFFECTIVE COLLISION STRENGTHS)
C            (R*8)  APGOA  = APPROXIMATE GAMMA
C            (R*8)  EXCRA  = EXCITATION RATE COEFFICIENT
C            (R*8)  DEXCRA = DEEXCITATION RATE COEFFICIENT
C            (R*8)  GBARFA = G BAR FUNCTION
C            (I*4)  ISTDIM = THE MAXIMUM ARRAY DIMENSION
C            (I*4)  IREAD  = THE INPUT UNIT
C
C  ROUTINES: NONE
C
C  AUTHOR:   DAVID H.BROOKS (UNIV.OF STRATHCLYDE) EXT.4213/4205
C
C  VERSION 1.1                             DATE:     26/05/95
C  MODIFIED: DAVID H. BROOKS
C	- FIRST RELEASE
C  VERSION 1.2                             DATE:     03/10/96
C  MODIFIED: WILLIAM OSBORN
C       - ADDED TRAP FOR WHEN THE REQUESTED ARCHIVE NUMBER IS NOT IN
C         THE FILE
C
C  DATE: 07/05/99				VERSION 1.3
C  MODIFIED: HUGH SUMMERS
C		- CORRECTED CONFUSION ABOUT NCHAR AND NELEC
C
C-----------------------------------------------------------------------
       INTEGER INDXREF , ICHK1 , INDL  , INDU , ICT   , ITOUT  
       INTEGER IXTYP   , IBPTS , IFPTS , IDIFF, ISTDIM, IREAD
       INTEGER IXOPS   , IZ    , IZ0   , I    , I4UNIT
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80 , CSTRING*80 , CSTRNG*122
       CHARACTER TITLE*40  , CAMETH*4   , CSS*10
C-----------------------------------------------------------------------
       REAL*8 Z , Z0 , ZEFF , EI  , EJ   , WI   , WJ
       REAL*8 ACOEFF ,  FIJ , EIJ , FXC2 , FXC3 , S
       REAL*8 GF, YY1, YY2  , BCVAL      , NELEC
C-----------------------------------------------------------------------
       REAL*8 XA(ISTDIM)  , YA(ISTDIM) , APOMA(ISTDIM) , DIFOMA(ISTDIM)
       REAL*8 TOA(ISTDIM) , GOA(ISTDIM), APGOA(ISTDIM) , EXCRA(ISTDIM)
       REAL*8 DEXCRA(ISTDIM) , GBARFA(ISTDIM)  , B(5)
C-----------------------------------------------------------------------
       OPEN( UNIT=IREAD , FILE = DSFULL , STATUS = 'UNKNOWN' )
       READ(IREAD,1000)CSTRING
 1     IF(CSTRING(1:1).EQ.'I')THEN
         READ(CSTRING,1001)ICHK1
           IF(ICHK1.NE.INDXREF) THEN
             READ(IREAD,1000)CSTRING
             GOTO 1
           ENDIF
       ELSE
          IF(CSTRING(1:1).EQ.'C')THEN
        WRITE(I4UNIT(-1),*)'**************** A1DATA MESSAGE **********'
        WRITE(I4UNIT(-1),'(A,I2)')' NOT FOUND ARCHIVE NUMBER ',INDXREF
        WRITE(I4UNIT(-1),'(A,I2)')' USING LAST ARCHIVE, NUMBER',ICHK1
        WRITE(I4UNIT(-1),*)'**************** A1DATA MESSAGE **********'
             INDXREF = ICHK1
C CLOSE FILE AND START AGAIN
             CLOSE(UNIT=IREAD)
             OPEN( UNIT=IREAD , FILE = DSFULL , STATUS = 'UNKNOWN' )
          ENDIF
          READ(IREAD,1000)CSTRING
          GO TO 1
       ENDIF
C
       READ(CSTRING,1002)ICHK1,TITLE,CAMETH 
       IF(CAMETH.EQ.'   A')THEN
         READ(IREAD,1003)Z0,Z,ZEFF
         READ(IREAD,1004)INDL,INDU
         READ(IREAD,1005)EI,EJ
         READ(IREAD,1006)WI,WJ 
         READ(IREAD,1007)ACOEFF,S,FIJ,EIJ
         READ(IREAD,1008)IXTYP,FXC2,FXC3,IXOPS,IBPTS,IFPTS,IDIFF 
         READ(IREAD,1000)CSTRING
C
         ICT = 0
 2       READ(IREAD,1000)CSTRING
           IF(CSTRING(1:2).NE.' T')THEN
             ICT=ICT+1
             READ(CSTRING,1009)XA(ICT),YA(ICT),APOMA(ICT),DIFOMA(ICT)
             GOTO 2
           ENDIF 
         ITOUT = 0
 3       READ(IREAD,1010)CSTRNG 
           IF(CSTRNG(1:2).NE.'-1')THEN
             ITOUT = ITOUT+1
             READ(CSTRNG,1011)TOA(ITOUT),GOA(ITOUT),APGOA(ITOUT),
     &                        EXCRA(ITOUT),DEXCRA(ITOUT),GBARFA(ITOUT)
           GOTO 3
         ENDIF
       ELSE IF(CAMETH.EQ.'   B')THEN
         READ(IREAD,1013)CSS
         READ(IREAD,1014)NELEC
         READ(IREAD,1014)Z0
         Z=Z0-NELEC
         ZEFF=Z+1.0
         READ(IREAD,1015)EI,WI
         READ(IREAD,1015)EJ,WJ
         EI = EI/109737.150
         EJ = EJ/109737.150
         READ(IREAD,1015)EIJ
         EIJ = EIJ/109737.150
         READ(IREAD,1016)IXTYP
         READ(IREAD,1017)GF
         ACOEFF = GF
         READ(IREAD,1018)BCVAL
         READ(IREAD,1019)(B(I),I=1,5)
         READ(IREAD,1020)YY1,YY2
         READ(IREAD,1021)INDL,INDU
         READ(IREAD,1013)CSS
         READ(IREAD,1022)ICT
         DO 10 I = 1,ICT
           READ(IREAD,1023)XA(I),YA(I)
 10      CONTINUE
         READ(IREAD,1013)CSS
         READ(IREAD,1022)ITOUT
         DO 20 I = 1,ITOUT
           READ(IREAD,1024)TOA(I),GOA(I)
 20      CONTINUE
       FIJ = GF/WI
       ENDIF
       CLOSE( UNIT=IREAD , STATUS = 'KEEP')
C-----------------------------------------------------------------------
 1000  FORMAT(1A80)
 1001  FORMAT(1X,I3,76X)
 1002  FORMAT(1X,1I3,2X,1A40,24X,1A4)
 1003  FORMAT(17X,F6.2,15X,F6.2,25X,F6.2)
 1004  FORMAT(16X,I5,36X,I5)
 1005  FORMAT(16X,F12.6,28X,F12.6)
 1006  FORMAT(16X,F5.1,35X,F5.1)
 1007  FORMAT(10X,1PE12.4,9X,1PE12.4,12X,1PE12.4,25X,0PE12.6)
 1008  FORMAT(13X,I3,29X,1PE11.4,10X,1PE11.4,10X,I2,10X,I2,
     &        11X,I2,10X,I2)
 1009  FORMAT(1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4)
 1010  FORMAT(1A122)
 1011  FORMAT(1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,
     &        1PE12.4,10X,1PE12.4)
 1012  FORMAT(1A2,120X)
 1013  FORMAT(1A10)
 1014  FORMAT(1X,F5.2)
 1015  FORMAT(5X,F10.3,5X,F5.1)
 1016  FORMAT(1X,I4)
 1017  FORMAT(2X,E10.4)
 1018  FORMAT(1X,F8.5)
 1019  FORMAT(5(E12.3))
 1020  FORMAT(2(E12.2))
 1021  FORMAT(2(I3))
 1022  FORMAT(I5)
 1023  FORMAT(2(E12.3))
 1024  FORMAT(2(E12.4))
C-----------------------------------------------------------------------
       RETURN
       END
