CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/maxwell.for,v 1.1 2004/07/06 14:20:38 whitefor Exp $ Date $Date: 2004/07/06 14:20:38 $
CX
      SUBROUTINE MAXWELL(IT,C,E,P,NT,T,U)
C-----------------------------------------------------------------------
C  ******************  FORTRAN77 SUBROUTINE: MAXWELL *******************
C
C  PURPOSE: GAUSS-LAGUERRE QUADRATURE FROM BURGESS' PROGRAM
C           OMEUPS
C
C  INPUT:
C    IT  TRANSITION TYPE
C    C   OMEGA SCALING PARAMETER
C    E   EXCITATION ENERGY
C    P   KNOTS
C    NT  NUMBER OF TEMPERATURE POINTS
C    T   TEMPERATURE
C
C  OUTPUT:
C    U   UPSILON
C
C  ROUTINES:
C    OMEUPS - TO CALCULATE THE MAXWELL AVERAGED UPSILONS
C    UPS    - TO CALCULATE THE MAXWELL AVERAGED UPSILONS FOR ALL
C             TRANSITIONS EXCEPT 'SPIN CHANGE'
C
C  WRITTEN:
C        A.LANZAFAME & D.H.BROOKS, UNIV.OF STRATHCLYDE, 02-JULY-1995
C        CONVERTED FROM BURGESS BBC BASIC
C  DATE: 02-07-95				VERSION: 1.1
C  MODIFIED: A.LANZAFAME & D.H.BROOKS
C		- FIRST RELEASE
C
C------------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER I,N,NT,I4UNIT
      REAL*8 T(NT),U(NT),UPS,T1,T2,T3,T4,T5,XX,WW,RI,YY,RI1,OMEUPS
C------------------------------------------------------------------------
      INTEGER IT,K
      PARAMETER(K=20)
      REAL*8 X(K),W(K),E,C,P(5),C1,C2
C------------------------------------------------------------------------
C  FREE PARAMETER C1 FIXED TO SUGGESTED ONE
C------------------------------------------------------------------------
      DATA C1/3.0/
C------------------------------------------------------------------------
C  QUADRATURE FIXED TO 20 POINTS (CHOICHE OF 5, 10, 15 OR 20 IN OMEUPS)
C------------------------------------------------------------------------
      DATA X/7.053989E-2,3.721268E-1,9.165821E-1,1.707307E0,
     &       2.749199E0,4.048925E0,5.615175E0,7.459017E0,
     &       9.594393E0,1.203880E1,1.481429E1,1.794890E1,
     &       2.147879E1,2.545170E1,2.993255E1,3.501343E1,
     &       4.083306E1,4.761999E1,5.581080E1,6.652442E1/
      DATA W/1.687468E-1,2.912544E-1,2.666861E-1,1.660024E-1,
     &       7.482607E-2,2.496442E-2,6.202551E-3,1.144962E-3,
     &       1.557418E-4,1.540144E-5,1.086486E-6,5.330121E-8,
     &       1.757981E-9,3.725503E-11,4.767529E-13,3.372844E-15,
     &       1.155014E-17,1.539522E-20,5.286443E-24,1.656457E-28/
C------------------------------------------------------------------------
      IF (IT.EQ.3) THEN
         XX=C-1
         T1=P(1)
         T2=P(2)/(1+XX*.25)**2
         T3=P(3)/(1+XX*.5)**2
         T4=P(4)/(1+XX*.75)**2
         T5=P(5)/C**2
         RI1=C*(T1+4*T2+2*T3+4*T4+T5)/12
C        READ(*,*)'C1 = 'C1
         RI=0
         DO I=1,K
            XX=X(I)
            WW=W(I)*DEXP(XX)
            XX=XX*E*C1
            YY=OMEUPS(IT,E,C,P,XX)
            RI=RI+WW*YY
         ENDDO
         C2=RI1/RI
      ENDIF
C
      DO I=1,NT
C
C------------------------------------------------------------------------
C  BURGESS'  GRID 
C------------------------------------------------------------------------
C        IF (IT.EQ.1 .OR. IT.EQ.4) THEN
C           T(I)=E*(C^(NT/(NT-I))-C)
C        ELSE
C           T(I)=c*E*I/(NT-I)
C        ENDIF
C        IF (T(I) .GT. 1E30) THEN
C           T(I)=1E30
C           WRITE(I4UNIT(-1) 'Warning: T too big, try a smaller value of c.'
C        ENDIF
C------------------------------------------------------------------------
C
C        T(I)=157888*T(I)
         U(I)=UPS(T(I)/157888,X,W,IT,E,C,P,C1,C2)
C------------------------------------------------------------------------
C  PRINTOUT IN OMEUPS
C------------------------------------------------------------------------
C        WRITE(I4UNIT(-1),*) T(I),U(I)
C------------------------------------------------------------------------
      ENDDO
C------------------------------------------------------------------------
C  ADDITIONAL PRINTOUTS IN OMEUPS
C------------------------------------------------------------------------
C      IF (IT.EQ.3) THEN
C         GF=1E4*T(NT)/157888
C         GF=GF*UPS(GF)
C         WRITE(I4UNIT(-1),*) 'Integral Omega d(E/Ryd) = ',gf 
C         GF=E*RI1
C         WRITE(I4UNIT(-1),*) 'gf'
C         gf=E*C*(7*T1+32*T2+12*T3+32*T4+7*T5)/90
C         WRITE(I4UNIT(-1),*) gf,'  (1E4*Tmax, Simp, Boole)'
C      ENDIF
C------------------------------------------------------------------------
      RETURN
      END






