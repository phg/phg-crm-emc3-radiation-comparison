CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/ups.for,v 1.1 2004/07/06 15:27:13 whitefor Exp $ Date $Date: 2004/07/06 15:27:13 $
CX
       FUNCTION UPS(T,X,W,IT,E,C,P,C1,C2)
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: UPS   *********************
C
C  PURPOSE:  
C        TO CALCULATE UPSILONS.
C
C  CALLING PROGRAM:
C        MAXWELL
C
C  INPUT:  
C        (R*8)    T - TEMPERATURES
C        (R*8)    X - FIXED QUADRATURE POINTS
C        (R*8)    W - FIXED QUADRATURE POINTS
C        (I)     IT - TRANSITION TYPE
C        (R*8)    E - EXCITATION ENERGY
C        (R*8)    C - OMEGA SCALING PARAMETER
C        (R*8)    P - BURGESS KNOTS
C        (R*8)   C1 - SCALING PARAMETERS
C        (R*8)   C2 - SCALING PARAMTERES
C
C  OUTPUT:
C        (R*8)  UPS - UPSILONS
C
C
C  ROUTINES: NONE
C
C  DATE:     21/06/95			VERSION 1.1
C  AUTHOR:   A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C            CONVERTED FROM BURGESS BBC BASIC
C
C-----------------------------------------------------------------------
       INTEGER IT,K
C-----------------------------------------------------------------------
       PARAMETER(K=20)
C----------------------------------------------------------------------- 
       REAL*8 X(K),W(K),E,C,P(5),C1,C2,T,OMEUPS
C-----------------------------------------------------------------------
       INTEGER I
C----------------------------------------------------------------------- 
       REAL*8 UPS,RI,XX,WW
C-----------------------------------------------------------------------
       RI=0
       DO I=1,K
          XX=X(I)*T
          WW=W(I)
           IF (IT .EQ. 3) THEN
              XX=XX/(1.+T/(E*C1))
              WW=WW*(E*C2/(T+E*C2))*DEXP(XX/(E*C1))
           ENDIF
           RI=RI+WW*OMEUPS(IT,E,C,P,XX)
        ENDDO
        UPS=RI
C-----------------------------------------------------------------------
      RETURN
      END

