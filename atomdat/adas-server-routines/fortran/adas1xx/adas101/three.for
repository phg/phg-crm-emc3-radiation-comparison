CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/three.for,v 1.1 2004/07/06 15:25:41 whitefor Exp $ Date $Date: 2004/07/06 15:25:41 $
CX
       SUBROUTINE THREE( A , B )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77SUBROUTINE: THREE*********************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) B - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   21/06/95				VERSION 1.1
C
C  AUTHOR: A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C          CONVERTED FROM BURGESS BBC BASIC
C
C-----------------------------------------------------------------------
       INTEGER I   , J
C-----------------------------------------------------------------------
       REAL*8 A(5,5) , B(5)  , T1  , T2
C-----------------------------------------------------------------------
       DO 10 I = 1,5
         T1=A(I,2)/8
         T2=A(I,4)/8
         A(I,1)=A(I,1)+3*T1-T2
         A(I,2)=A(I,3)+6*(T1+T2)
         A(I,3)=A(I,5)-T1+3*T2
 10    CONTINUE
C
       DO 20 J = 1,3 
         A(1,J)=A(1,J)+.5*A(2,J)
         A(2,J)=A(3,J)+.5*(A(2,J)+A(4,J))
         A(3,J)=A(5,J)+.5*A(4,J)
 20    CONTINUE
       B(1)=B(1)+.5*B(2)
       B(2)=B(3)+.5*(B(2)+B(4))
       B(3)=B(5)+.5*B(4)
C-----------------------------------------------------------------------
       CALL MATIN1( 3, A, B )
C-----------------------------------------------------------------------
         B(5)=B(3)
         B(3)=B(2)
         B(2)=(3*B(1)+6*B(3)-B(5))/8
         B(4)=(-B(1)+6*B(3)+3*B(5))/8
C-----------------------------------------------------------------------
       RETURN
       END
