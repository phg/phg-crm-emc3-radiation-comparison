CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/omeups.for,v 1.1 2004/07/06 14:25:19 whitefor Exp $ Date $Date: 2004/07/06 14:25:19 $
CX
      FUNCTION OMEUPS(IT,E,C,P,X)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE:OMEUPS*********************
C
C  PURPOSE:
C        TO CALCULATE UPSILONS 
C
C  CALLING PROGRAM:
C        MAXWELL
C
C  INPUT:
C        (I)     IT - TRANSITION TYPE
C        (R*8)    E - EXCITATION ENRGY
C        (R*8)    C - SCALABLE PARAMETER
C        (R*8)    P - KNOT POINTS
C        (R*8)    X - COLLIDING ELECTRON ENERGY AFTER
C                     EXCITATION
C  OUTPUT:
C        (R*8) OMEUPS - THE UPSILONS
C
C  ROUTINES:
C
C  DATE:    21/06/95			VERSION 1.1
C  AUTHOR:  A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C           CONVERTED FROM BURGESS BBC BASIC
C
C-----------------------------------------------------------------------  
      INTEGER IT
C-----------------------------------------------------------------------
      REAL*8 E,C,P(5),X,OMEUPS,OMUP,ETRED,SP5,ETR,SP
C----------------------------------------------------------------------- 
      ETR = ETRED(IT,E,X,C)
      SP = SP5(P,ETR)
      OMEUPS=OMUP(IT,E,X,SP,C)
      RETURN
      END

