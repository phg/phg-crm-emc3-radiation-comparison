CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/omup.for,v 1.3 2004/07/06 14:25:37 whitefor Exp $ Date $Date: 2004/07/06 14:25:37 $
CX
      FUNCTION OMUP(IT,E,T,U,C)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: OMUP *********************
C
C  PURPOSE:
C        TO CALCULATE UPSILONS FOR DIFFERENT TRANSITIONS
C
C  CALLING PROGRAM:
C        OMEUPS
C
C  INPUT:
C        (I)     IT - TRANSITION TYPE 
C        (R*8)    E - EXCITATION ENRGY
C        (R*8)    T - SCALED ENERGY VALUE OF QUADRATURE FIXED POINTS
C        (R*8)    U - SPLINE FIT TO THE KNOT POINTS AND 
C                     REDUCED ENERGIES 
C        (R*8)    C - SCALABLE PARAMETER
C  OUTPUT:
C        (R*8) OMUP - THE UPSILONS
C
C
C  DATE:   21/06/95				VERSION 1.1
C  AUTHOR: A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C          CONVERTED FROM BURGESS BBC BASIC
C
C  DATE:   25/11/96				VERSION 1.2
C  AUTHOR: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C          CORRECTED IT.EQ.3 OPTION
C
C  DATE:   13/05/99				VERSION 1.3
C  AUTHOR: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C          PREVENTED CHANGE OF INPUT SUBROUTINE PARAMETER T ON RETURN
C
C-----------------------------------------------------------------------
      INTEGER IT
C-----------------------------------------------------------------------
      REAL*8 E,T,U,C,OMUP,TL
C-----------------------------------------------------------------------
      TL=T/E
      IF (IT.EQ.1) OMUP=U*DLOG(TL+2.71828)
      IF (IT.EQ.2) OMUP=U
      IF (IT.EQ.3) OMUP=U/((TL+1)*(TL+1))
      IF (IT.EQ.4) OMUP=U*DLOG(TL+C)
C----------------------------------------------------------------------
      RETURN
      END

