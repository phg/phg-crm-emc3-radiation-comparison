CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/ouredinv.for,v 1.1 2004/07/06 14:26:49 whitefor Exp $ Date $Date: 2004/07/06 14:26:49 $
CX
       FUNCTION OUREDINV( IXTYP , EIJ   , EJ  , DGEY  , C )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: ETREDINV ******************
C
C  PURPOSE:  TO CALCULATE THE COLLISION STRENGTH FROM THE REDUCED 
C            COLLISION STRENGTH FOR FOUR TYPES OF TRANSITION 
C
C  CALLING PROGRAM:
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (Eij)
C            (R*8)    EJ     =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    ETR    =   Ej/Eij
C            (I)      IXTYPE =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C            (R*8)    DGEY   =   REDUCED COLLISION STRENGTH
C
C  OUTPUT:   (R*8)    OUREDINV=   ELECTRON ENERGY
C
C 
C  ROUTINES: NONE
C
C  DATE:     22/06/95			VERSION 1.1
C  AUTHOR:   DAVID.H.BROOKS (UNIV. OF STRATHCLYDE)
C
C
C-----------------------------------------------------------------------
       INTEGER IXTYP
C-----------------------------------------------------------------------
       REAL*8 EIJ  , EJ   , C    , ETR  , DGEY , OUREDINV
C-----------------------------------------------------------------------
       ETR = DABS(EJ/EIJ)
C
       IF (IXTYP.EQ.1) OUREDINV = DGEY*DLOG(ETR+2.718281828)
       IF (IXTYP.EQ.2) OUREDINV = DGEY
       IF (IXTYP.EQ.3) OUREDINV = DGEY/((ETR+1)*(ETR+1))
       IF (IXTYP.EQ.4) OUREDINV = DGEY*DLOG(ETR+C)
C-----------------------------------------------------------------------
       RETURN
       END
