CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/adas101.for,v 1.8 2004/07/06 10:07:16 whitefor Exp $ Date $Date: 2004/07/06 10:07:16 $
CX
       PROGRAM ADAS101
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN 77 PROGRAM: ADAS101 *********************
C
C  PURPOSE: COLLECTION FROM ARCHIVE OR ENTRY OF ELECTRON COLLISIONAL 
C           EXCITATION CROSS-SECTIONS FOR EXAMINATION, INTERPOLATION, 
C           COMPARISON WITH APPROXIMATE FORMS, AND GRAPH EDITING VIA
C           COMMUNICATION WITH IDL INTERFACE.
C
C  DATA:
C           DATA IS ENTERED VIA THE IDL INTERFACE, BUT CAN ALSO BE 
C           RECOVERED FROM THE USERS' OWN ARCHIVE FILES. THESE 
C           TYPICALLY RESIDE IN '/DISK2/YOURNAME/ADAS/ARCH101'.
C           PARTICULAR TRANSITION DATASETS HAVE THEIR OWN INDEX
C           NUMBER AND THIS IS ENTERED ON THE FIRST PANEL AFTER
C           SELECTING 'REFRESH FROM OLD ARCHIVE'. THERE IS NO
C           CENTRAL ADAS ARCHIVE.
C  
C           ARCHIVE FILES FOLLOW THE FOLLOWING NAMING CONVENTION;
C           <PRODUCER ID><YEAR><MEMBER>_<FILE NUMBER>.DAT
C
C           THE MEMBER NAME IS CONSTRUCTED AS;
C           <ELEMENT><SPECTROSCOPIC EFFECTIVE ION CHARGE>
C
C           E.G.        'DHB95FE19_001.DAT'
C           DHB = DAVID H.BROOKS
C           95  = 1995
C           FE  = IRON
C           19  = Z+1      I.E. FE+18 IS THE ION IN THIS CASE
C           001 = FILE NUMBER
C  PROGRAM:
C          (I*4)  INDXREF= THE INDEX NUMBER TO REFRESH DATA FROM.
C          (I*4)  INDL   = LOWER LEVEL INDEX  (USER CHOICE)                        
C          (I*4)  INDU   = UPPER LEVEL INDEX  (USER CHOICE)           
C          (I*4)  ICT    = NUMBER OF X-SECTIONS
C          (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C          (I*4)  ICHK1  = CHECKS FOR THE RIGHT INDEX NUMBER
C          (I*4)  IXTYP  = 1  DIPOLE TRANSITION                                    
C                        = 2  NON-DIPOLE TRANSITION                                
C                        = 3  SPIN CHANGE TRANSITION                               
C                        = 4  OTHER     
C          (I*4)  IASEL  = THE ARCHIVING CHOICE.
C                          0 - NO ARCHIVE
C                          1 - EXAMINE OLD ARCHIVE
C                          2 - REFRESH FROM ARCHIVE
C                          3 - NEW ARCHIVE
C          (I*4)  IUNT07 = A FREE UNIT NUMBER
C          (I*4)  NTABCOL= THE NUMBER OF COLUMNS IN THE EDITABLE 
C                          IDL TABLE
C          (I*4)  IGRPOUT= GRAPH DISPLAY SELECTION SWITCH
C          (I*4)  IANOPT = ANALYSIS OPTION
C                          1 - ADAS
C                          2 - BURGESS
C          (R*8)  IBXVAL = NON-DIPOLE POINT AT INFINITY SWITCH.
C                          0 - OFF
C                          1 - ON
C          (I*4)  IGRPSCAL1 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             IST GRAPH.
C          (I*4)  IGRPSCAL2 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             2NT GRAPH.
C          (I*4)  ITEXOUT= HARD COPY OF TEXT OUTPUT REQUEST SWITCH.
C                          0 - NO
C                          1 - YES
C          (I*4)  IATYP  = 1  A-COEFFICIENT RETURNED     
C                        = 2  OSCILLATOR STRENGTH RETURNED         
C                        = 3  LINE STRENGTH RETURNED                         
C          (I*4)  IIBTS  = 0  BAD POINT OPTION OFF       
C                        = 1  BAD POINT OPTION ON        
C          (I*4)  IIFPT  = 1  SELECT ONE POINT OPTIMISING      
C                        = 2  SELECT TWO POINT OPTIMISING    
C          (I*4)  IIXOP  = 0  OPTIMISING OFF                       
C                        = 1  OPTIMISING ON  (IF ALLOWED)               
C          (I*4)  IIDIF  = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY 
C                          WITH OPTIMISING)
C                        = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT                 
C          (I*4)  IIORD  = 1  4-PT GAUSS-LAGUERRE QUADRATURE                       
C                        = 2  8-PT GAUSS-LAGUERRE QUADRATURE                       
C                        = 3 12-PT GAUSS-LAGUERRE QUADRATURE                       
C          (I*4)  IFTYP  = 1 UPPER K**2 (RYD) COLLISION ENERGY UNITS           
C                        = 2 LOWER K**2 (RYD)                                      
C                        = 3 UPPER (K/Z0)**2 (RYD)                                
C                        = 4 X PARAMETER                                           
C                        = 5 UPPER (K/ZEFF)**2 (RYD)                               
C          (I*4)  IOTYP  = 1 EXCITATION CROSS-SECTION (PI*A0**2)          
C                        = 2 DE-EXCITATION CROSS-SECTION (PI*A0**2)       
C                        = 3 COLLISION STRENGTH RETURNED                           
C                        = 4 SCALED COLLISION STRENGTH (Z**2*OMEGA)       
C          (I*4)  IOUTU  = 1 KELVIN FOR OUTPUT TEMPERATURE UNIT                    
C                        = 2   EV   FOR OUTPUT TEMPERATURE UNIT                   
C                        = 3 SCALED UNITS RETURNED  (TE(K)/Z1**2)                  
C                        = 4 REDUCED UNITS RETURNED (KTE/EIJ)                      
C          (I*4)  IETYP  = 1  LEVEL ENERGIES IN CM-1                               
C                        = 2  LEVEL ENERGIES IN RYD                    
C          (C*80) DSFULL = THE SELECTED ARCHIVE NAME.
C          (C*3)  REP    = 'YES' - FINISH PROGRAM RUN
C                          'NO ' - CONTINUE
C          (C*40) TITLE  = THE INFORMATION LINE IN THE ARCHIVE
C                          FILE.
C          (C*4)  CAMETH = THE TAG TO DISTINGUISH BETWEEN THE
C                          TWO TYPES OF ANALYSIS.
C                          A - ADAS, B- BURGESS
C          (C*8)  DATE   = THE CURRENT DATA 
C          (C*80) CHEADER= THE ADAS HEADER FOR THE PAPER.TXT FILE
C          (C*80) TEXDSN = THE DEFAULT HARD COPY FILE NAME
C          (C*80) DSARCH = THE ARCHIVE FILE NAME
C          (C*40) CGTIT  = THE GRAPH TITLE (INFORMATION FOR ANY
C                          NEW ARCHIVE)
C          (R*8)  Z0     = NUCLEAR CHARGE OF ION                 
C          (R*8)  Z      = ION CHARGE                             
C          (R*8)  ZEFF   = ION CHARGE + 1 
C          (R*8)  WI     = LOWER LEVEL STATISTICAL WEIGHT               
C          (R*8)  WJ     = UPPER LEVEL STATISTICAL WEIGHT                  
C          (R*8)  EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS)          
C          (R*8)  EJ     = UPPER LEVEL ENERGY  
C          (R*8)  ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM,
C                           DIPOLE CASE ONLY)
C          (R*8)  S      = LINE STRENGTH
C          (R*8)  FIJ    = OSCILLATOR STRENGTH
C          (R*8)  EIJ    = TRANSITION ENERGY
C          (R*8)  FXC2   = SPLINING VARIABLE
C          (R*8)  FXC3   = SPLINING VARIABLE
C          (R*8)  BCVAL  = BURGESS SCALABLE PARAMETER
C          (R*8)  RMS    = THE R.M.S OF THE BURGESS FIT
C          (R*8)  GF     = THE WEIGHTED OSCILLATOR STRENGTH
C          (R*8)  BYVAL  = THE Y VALUE OF THE POINT AT INFINITY.
C          (R*8)  XMIN1  = USER SELECTED 1ST GRAPH X-AXIS MINIMUM.
C          (R*8)  XMAX1  = USER SELECTED 1ST GRAPH X-AXIS MAXIMUM.
C          (R*8)  YMIN1  = USER SELECTED 1ST GRAPH Y-AXIS MINIMUM.
C          (R*8)  YMAX1  = USER SELECTED 1ST GRAPH Y-AXIS MAXIMUM.
C          (R*8)  XMIN2  = USER SELECTED 2ND GRAPH X-AXIS MINIMUM.
C          (R*8)  XMAX2  = USER SELECTED 2ND GRAPH X-AXIS MAXIMUM.
C          (R*8)  YMIN2  = USER SELECTED 2ND GRAPH Y-AXIS MINIMUM.
C          (R*8)  YMAX2  = USER SELECTED 2ND GRAPH Y-AXIS MAXIMUM.
C          (L)    LPEND  = CANCEL OR DONE FLAG
C          (L)    LARCH  = ARCHIVING SELECTION OPTION
C          (R*8)  XA     = ENERGY (PARAMETER X)
C          (R*8)  YA     = OMEGA (COLLISION STRENGTH)
C          (R*8)  APOMA  = APPROXIMATE OMEGA
C          (R*8)  DIFOMA = DIFFERENCE BETWEEN YA & APOMA
C          (R*8)  TOA    = TEMPERATURE SET
C          (R*8)  GOA    = GAMMA (EFFECTIVE COLLISION STRENGTHS)
C          (R*8)  APGOA  = APPROXIMATE GAMMA
C          (R*8)  EXCRA  = EXCITATION RATE COEFFICIENT
C          (R*8)  DEXCRA = DEEXCITATION RATE COEFFICIENT
C          (R*8)  GBARFA = G BAR FUNCTION
C          (R*8)  EDAT   = USER SELECTED INPUT ENRGIES (AS X PARAM)
C          (R*8)  EBUR   = USER SELECTED INPUT ENRGIES (AS EJ)
C          (R*8)  XDAT   = USER SELECTED INPUT OMEGAS
C          (R*8)  TDAT   = USER SELECTED OUTPUT TEMPERATURES
C          (R*8)  B      = THE FIVE BURGESS KNOTS
C          (R*8)  DGEX   = THE REDUCED ENERGIES
C          (R*8)  DGEY   = THE REDUCED OMEGAS
C          (R*8)  Z1     = Z+1                   
C          (R*8)  X      = TEMP. VALUE (ENERGY) 
C          (R*8)  Y      = TEMP. VALUE (XSECT) 
C          (R*8)  TE     = TEMP. VALUE (TEMPERATURE)a
C          (I*4)  ISTOP  = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END TO
C                          THE PROGRAM
C  ROUTINES: (COMPLETE LIST FOR ADAS101 - NOT NECESSARILY 
C             CALLED BY THIS PROGRAM)
C           NAME             BRIEF DESCRIPTION
C           -------------------------------------------------------
C           A1SPF0    GATHERS INPUT ARCHIVE NAME VIA IDL PIPE
C           A1DATA    READS SELECTED DATA FROM ARCHIVE
C           A1ISPF    PIPES DATA TO IDL READS USER PROCESSING EDITS 
C           A1SPF1    GATHERS USER SELECTED OUTPUT OPTIONS
C           SPFMAN4E  PERFORMS ADAS PROCESSING (GAMMA EVALUATION)
C           A1OUTGA   SENDS DATA TO IDL GRAPH EDITOR - ADAS OPTION
C           A1OUTGB   SENDS DATA TO IDL GRAPH EDITOR-BURGESS OPTION
C           A1OUT0    WRITES DATA TO ARCHIVE/TEXT OUTPUT FILES
C           LSTSQ     PERFORMS BURGESS LEAST SQUARES FIT
C           MATIN1    PERFORMS BURGESS MATRIX INVERSION TO GET 5 KNOTS
C           SPLS      SETS UP BURGESS SPLINE COEFFICIENTS
C           ETRED     CALCULATES BURGESS REDUCED ENERGIES
C           OURED     CALCULATES BURGESS REDUCED OMEGAS
C           ETREDINV  INVERTS ETRED
C           OUREDINV  INVERTS OURED
C           ONE       CALCULATES 5 KNOTS FOR 1 DATA POINT
C           TWO       CALCULATES 5 KNOTS FOR 2 DATA POINTS
C           THREE     CALCULATES 5 KNOTS FOR 3 DATA POINTS
C           FOUR      CALCULATES 5 KNOTS FOR 4 DATA POINTS
C           XX0000    SETS MACHINE DEPENDENT ADAS CONFIGURATION
C           XXDATE    PROVIDES CURRENT DATE
C           XXSLEN    REMOVES SPACES FROM A STRING
C           I4UNIT    FETCHES MESSAGE OUTPUT UNIT NUMBER
C           XXFLSH    CLEARS IDL PIPE BUFFER
C           EFASYM    PROVIDES SPLINE INTERPOLATION
C           ELNFIT    PERFORMS LINEAR INTERPOLATION
C           EEI       INTERNAL TO SPFMAN4E
C           ESORT     SORTS AN ARRAY INTO INCREASING ORDER
C           EGSPC     GENERATES SPLINE COEFFICIENT PRECURSORS - ADAS
C                     OPTION
C           EGASYM    INITIALISES COMMON ARRAYS FOR SMOOTH SPLINE
C           EE2       INTERNAL TO SPFMAN4E
C           FORM4     INTERNAL TO SPFMAN4E
C           ESPIJ3    CALCULATES SPLINES WITH VARIOUS END CONDITIONS
C           EFITSP    PERFORMS SPLINE INTERPOLATION
C           MAXWELL   PERFORMS GAUSS-LAGUERRE QUADRATURE-BURGESS OPT.
C           OMEUPS    SETS UP ARRAYS FOR OMUP DEPENDING ON TRANSITION
C                     TYPE
C           OMUP      CALCULATES UPSILONS
C           SP5       CALCULATES BURGESS SPLINE
C           UPS       SETS UP ARRAYS FOR OMUP DEPENDING ON TRANSITION
C                     TYPE
C           WUPSILON  WRITES OUT DATA IN BURGESS FROMAT
C           PARAMS    SETS DIMENSION COMMON BLOCK
C
C  AUTHOR:  DAVID H.BROOKS (UNIV. OF STRATHCLYDE) EXT.4213/4205
C
C  DATE:    04 JULY 1995
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1   DAVID H.BROOKS  04/07/95
C  MODIFIED:      FIRST RELEASE
C
C  VERSION: 1.2   TIM HAMMOND     10/07/95
C  MODIFIED:      TIDIED CODE AND COMMENTS UP
C
C  VERSION: 1.3	  TIM HAMMOND     17/07/95
C  MODIFIED:      INCREASED LENGTH OF TEXDSN TO 80 FROM 45
C
C  VERSION: 1.4	  WILLIAM OSBORN  30/05/96
C  MODIFIED:      ADDED MENU BUTTON READS FROM PIPE
C
C  VERSION: 1.5	  DAVID H.BROOKS  26/02/96
C  MODIFIED:      ADDED ICHC TO FLAG C VALUE ALTERATIONS & REWORKED
C                 BACKWARD LOOP TO 401 FOR THIS CASE.
C
C  VERSION: 1.6	  DAVID H.BROOKS  20/11/96
C  MODIFIED:      ALTERATIONS TO MAKE SURE ARCHIVE ALWAYS IN CORRECT 
C                 UNITS (BURGESS INCLUDED).
C             
C  VERSION: 1.7	  WILLIAM OSBORN  20/11/96
C  MODIFIED:      REMOVED DIAGNOSTIC IF STATEMENT
C             
C  VERSION: 1.8	  HUGH SUMMERS    10/05/99
C  MODIFIED:      CORRECTED WRITE BACK OF NEW APPROX. FORMS ETC.
C
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
C-----------------------------------------------------------------------
       INTEGER INDXREF  , ICHK1    , INDL    , INDU    , ICT  
       INTEGER ITOUT    , ICHK2    , IXTYP   , IBPTS   , IFPTS   ,IDIFF   
       INTEGER IASEL    , IUNT07   , NTABCOL , IZ      , IZ0 
       INTEGER IGRPOUT  , IANOPT   , I       , IBXVAL
       INTEGER IGRPSCAL1, IGRPSCAL2, ITEXOUT , IATYP
       INTEGER IGRD1    , IGRD2    
       INTEGER IIBTS    , IIFPT    , IIXOP   , IIORD   , IIDIF
       INTEGER IFTYP    , IOTYP    , IOUTU   , IETYP   , IXOPS
       INTEGER ICTN     , ITOUTN   , I4UNIT  , PIPEIN  , ISTOP   , ICHC
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80 , CSTRING*80 , CSTRNG*122 , CAMETH*4
       CHARACTER TITLE*40  , DATE*8     , REP*3
       CHARACTER CGTIT*40  , CHEADER*80 , TEXDSN*80  , DSTEMP*10
       CHARACTER IXTIT*40  , DSARCH*80
C-----------------------------------------------------------------------
       PARAMETER( IUNT07 = 7 , NTABCOL = 3 , PIPEIN = 5)
C-----------------------------------------------------------------------
       REAL*8  Z  , Z0 , ZEFF    , EI  , EJ   , WI   , WJ , S , Z1
       REAL*8  ACOEFF  , FIJ     , EIJ , FXC2 , FXC3 
       REAL*8  BCVAL   , BYVAL   , GF  , RMS  , X   , Y  , TE
       REAL*8  XMIN1   , XMAX1   , YMIN1   , YMAX1
       REAL*8  XMIN2   , XMAX2   , YMIN2   , YMAX2  , ETRED , OURED
C-----------------------------------------------------------------------
       INTEGER IFOUT(NTABCOL)
C-----------------------------------------------------------------------
       REAL*8 XA(ISTDIM)  , YA(ISTDIM) , APOMA(ISTDIM) , DIFOMA(ISTDIM)
       REAL*8 TOA(ISTDIM) , GOA(ISTDIM), APGOA(ISTDIM) , EXCRA(ISTDIM)
       REAL*8 DEXCRA(ISTDIM) , GBARFA(ISTDIM) 
       REAL*8 EDAT(ISTDIM), XDAT(ISTDIM), TDAT(ISTDIM)
       REAL*8 XAN(INDIM)  , YAN(INDIM) , APOMAN(INDIM) , TOAN(INDIM)
       REAL*8 GOAN(INDIM) , B(5)    , DGEX(ISTDIM)  , DGEY(ISTDIM)
       REAL*8 EBUR(ISTDIM)
       REAL*8 DIFOMN(ISTDIM) , APGON(ISTDIM) , EXCRN(ISTDIM)
       REAL*8 DEXCRN(ISTDIM) , GBARFN(ISTDIM)
C-----------------------------------------------------------------------
       LOGICAL OPEN7 , LARCH , LPEND
C-----------------------------------------------------------------------
       DATA OPEN7/.FALSE./
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
       CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
       CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
  100  CONTINUE
C-----------------------------------------------------------------------
C  IF FILES ARE ACTIVE ON UNIT 7 - CLOSE IT
C-----------------------------------------------------------------------
       IF (OPEN7) CLOSE(7)
       OPEN7=.FALSE.
C
C-----------------------------------------------------------------------
C GET ARCHIVE NAME FROM IDL
C-----------------------------------------------------------------------
C
       CALL A1SPF0( REP     , DSFULL  ,
     &              IASEL   , INDXREF )
C
C-----------------------------------------------------------------------
C  IF PROGRAM RUN COMPLETED END  
C-----------------------------------------------------------------------
C
       IF (REP.EQ.'YES') THEN
           GOTO 999
       ENDIF
C
C-----------------------------------------------------------------------
C OPEN ARCHIVE AND READ IF REFRESHING
C-----------------------------------------------------------------------
C
        IF (IASEL.EQ.2) THEN
           CALL A1DATA( DSFULL , INDXREF , TITLE , CAMETH , Z0    ,
     &                  Z      , ZEFF    , INDL  , INDU   , EI    , 
     &                  EJ     , WI      , WJ    , ACOEFF , S     ,
     &                  FIJ    , EIJ     , IXTYP , FXC2   , FXC3  ,
     &                  IXOPS  , IBPTS   , IFPTS , IDIFF  , ICT   , 
     &                  ITOUT  , XA , YA , APOMA , DIFOMA , TOA   ,
     &                  GOA    , APGOA   , EXCRA , DEXCRA , GBARFA,
     &                  ISTDIM , IUNT07  , IZ    , IZ0    , GF    ,
     &                  BCVAL     
     &                )        
       ENDIF
C
C-----------------------------------------------------------------------
C
  200  CONTINUE
C
C-----------------------------------------------------------------------
C READ USER PROCESSING OPTIONS FROM IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
       CALL A1ISPF( DSFULL , INDXREF , TITLE , CAMETH , Z0    ,
     &              Z      , ZEFF    , INDL  , INDU   , EI    , 
     &              EJ     , WI      , WJ    , ACOEFF , IATYP ,
     &              FIJ    , EIJ     , IXTYP , FXC2   , FXC3  ,
     &              IXOPS  , IBPTS   , IFPTS , IDIFF  , ICT   ,
     &              ITOUT  , XA , YA , APOMA , DIFOMA , TOA   ,
     &              GOA    , APGOA   , EXCRA , DEXCRA , GBARFA,
     &              ISTDIM , IASEL   , IFOUT , NTABCOL, EDAT  ,
     &              XDAT   , TDAT    , IFTYP , IOTYP  , IOUTU ,
     &              IETYP  , LPEND   , BCVAL
     &             )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 999
C
C-----------------------------------------------------------------------
C
       IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C READ USER OUTPUT OPTIONS FROM IDL VIA UNIX PIPE  
C-----------------------------------------------------------------------
C
 300   CONTINUE
C
C-----------------------------------------------------------------------
C
       CALL A1SPF1( LPEND   , IGRPOUT   , IANOPT    ,
     &              IGRPSCAL1, IGRPSCAL2, ITEXOUT   ,
     &              CGTIT   , CHEADER   , TEXDSN    , BCVAL   ,
     &              IBXVAL  , BYVAL     , XMIN1     , XMAX1   ,
     &              YMIN1   , YMAX1     , XMIN2     , XMAX2   ,
     &              YMIN2   , YMAX2     , IIBTS     , IIFPT   , 
     &              IIXOP     , IIDIF
     &             )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 999
C
C-----------------------------------------------------------------------
C
       IF (LPEND) GOTO 200
C
C-----------------------------------------------------------------------
C PROCESS ANALYSIS OPTION 
C-----------------------------------------------------------------------
       IF (IANOPT.EQ.1) THEN
         CALL SPFMAN4E( Z0    , Z     , ZEFF  , TITLE , IETYP , 
     &                  IXTYP , INDL  , INDU  , WI, WJ, EI, EJ,  
     &                  IATYP , ACOEFF, IFTYP , IOTYP , IOUTU ,
     &                  ICT   , ITOUT , EDAT  , XDAT  , TDAT  ,
     &                  IIORD , IIBTS , IIFPT , IIXOP , IIDIF , 
     &                  CGTIT , IGRD1 , XMIN1 , XMAX1 ,
     &                  YMIN1 , YMAX1 , CGTIT , IGRD2 , 
     &                  XMIN2 , XMAX2 , YMIN2 , YMAX2 , IUNT07,
     &                  FXC2  , FXC3  , XAN   , YAN   , APOMAN,
     &                  DIFOMN, TOAN  , GOAN  , APGON , EXCRN,
     &                  DEXCRN, GBARFN, 
     &                  ICTN  , ITOUTN, S     , FIJ   , EIJ  
     &                 )
 
         IF (IGRPOUT.EQ.1)THEN
           CALL A1OUTGA( XAN   , YAN   , APOMAN, TOAN  ,
     &                   GOAN  , ICTN  , ITOUTN, INDIM ,
     &                   LPEND , LARCH , DSARCH, IASEL ,
     &                   FXC2  , FXC3  , S     , EIJ   , 
     &                   EDAT  , XDAT  , ISTDIM
     &                  )
           ICT = ICTN
           ITOUT = ITOUTN
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C     
           READ(PIPEIN,*) ISTOP
           IF(ISTOP.EQ.1) GOTO 999
        ENDIF 
       ELSE
C-----------------------------------------------------------------------
C CALCULATE THE FIJ & S IF NOT REFRESHING
C-----------------------------------------------------------------------
 401     CONTINUE
         Z1 = Z+1.0D0
           IF(IETYP.EQ.1)THEN
             EI=EI/1.09737D5
             EJ=EJ/1.09737D5
           ENDIF     
         EIJ=EJ-EI 
           IF(IXTYP.EQ.1)THEN                                               
             IF(IATYP.EQ.2)THEN                                           
                 ACOEFF=8.03232E9*EIJ*EIJ*WI*ACOEFF/WJ                    
             ELSE IF(IATYP.EQ.3)THEN                                       
                 ACOEFF=2.67744E9*EIJ*EIJ*EIJ*ACOEFF/WJ                   
             ENDIF 
             S=3.73491E-10*WJ*ACOEFF/(EIJ*EIJ*EIJ)                        
             FIJ=3.333333D-1*EIJ*S/WI                                     
           ENDIF
              
         DO 403 I=1,ICT   
          X=EDAT(I)
          Y=XDAT(I)
          IF(IFTYP.EQ.1)THEN
             EDAT(I)=X/EIJ+1.0D0
          ELSEIF(IFTYP.EQ.2)THEN
             EDAT(I)=X/EIJ
          ELSEIF(IFTYP.EQ.3)THEN
             EDAT(I)=Z0*Z0*X/EIJ+1.0D0
          ELSEIF(IFTYP.EQ.4)THEN
             EDAT(I)=X
          ELSEIF(IFTYP.EQ.5)THEN
             EDAT(I)=ZEFF*ZEFF*X/EIJ+1.0D0
          ENDIF
          
          IF(IOTYP.EQ.1)THEN
             XDAT(I)=Y*WI*EIJ*EDAT(I)
          ELSEIF(IOTYP.EQ.2)THEN
             XDAT(I)=Y*WJ*EIJ*(EDAT(I)-1.0D0)
          ELSEIF(IOTYP.EQ.3)THEN
             XDAT(I)=Y
          ELSEIF(IOTYP.EQ.4)THEN
             XDAT(I)=Y/(ZEFF*ZEFF)
          ENDIF
 403     CONTINUE
C 
         DO 404 I=1,ICT
           EBUR(I)=DABS(EIJ)*(EDAT(I)-1.0D0)
  404    CONTINUE           
C
         DO 405 I=1,ITOUT
          TE=TDAT(I)
          IF(IOUTU.EQ.1)THEN
             TDAT(I)=TE
          ELSEIF(IOUTU.EQ.2)THEN
             TDAT(I)=1.16054D4*TE
          ELSEIF(IOUTU.EQ.3)THEN
             TDAT(I)=TE*Z1*Z1
          ELSEIF(IOUTU.EQ.4)THEN
             TDAT(I)=1.5789D5*TE*EIJ
          ENDIF  
 405     CONTINUE
c******* need to redefine ietyp, iatyp, iftyp, iotyp after following ***
         ietyp=2
         iatyp=1
         iftyp=4
         iotyp=3
         ioutu=1
c
         GF = FIJ*WI
         CALL LSTSQ( IXTYP , BCVAL , EIJ   ,  GF   , ICT   , EBUR  ,
     &               XDAT  , B     , RMS    
     &             )
C
         CALL MAXWELL( IXTYP , BCVAL , EIJ   , B     , ITOUT , TDAT  ,
     &                 GOAN  
     &               )
C
         CALL A1OUTGB( ICT   , DGEX  , DGEY  , B    , LPEND , RMS  ,
     &                 ISTDIM, IXTYP , EIJ   , BCVAL , EBUR , XDAT ,
     &                 TDAT  , GOAN  , INDIM , ITOUT , DSARCH,
     &                 IASEL , LARCH , ICHC
     &               )
C 
         DO 406 I=1,ICT
           EDAT(I)=EBUR(I)/DABS(EIJ)+1.0D0
  406    CONTINUE           
C
C-----------------------------------------------------------------------
C RESET RELEVANT VARIABLES FOR LOOP BACK
C-----------------------------------------------------------------------
         IF(ICHC.EQ.1)THEN
           RMS = 0.0
           DO 410  I = 1, 5
             B(I) = 0.0
 410       CONTINUE
           GOTO 401
         ENDIF
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 999
C     
       ENDIF

C
C-----------------------------------------------------------------------
C
       IF (LPEND) GOTO 300
C
C-----------------------------------------------------------------------
C IF NO ARCHIVING END COMPLETE PROGRAM RUN
C-----------------------------------------------------------------------
C
       IF (.NOT.LARCH.AND.ITEXOUT.NE.1)THEN
          GOTO 999
       ENDIF
C
C-----------------------------------------------------------------------
C COPY DSARCH TO DSFULL IF ARCHIVING SELECTED AFTER PICKING NO ARCHIVE
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
         IF (IASEL.EQ.0)THEN
           DSFULL = DSARCH
           IASEL = 3
         ENDIF
       ENDIF
       IF (IASEL.NE.0)THEN
           IF (IASEL.EQ.2)THEN
              IXTIT = TITLE
           ELSE
              IXTIT = CGTIT
           ENDIF
           CALL A1OUT0( DSFULL , INDXREF , IXTIT , CAMETH , Z0    ,
     &                  Z      , ZEFF    , INDL  , INDU   , EI    , 
     &                  EJ     , WI      , WJ    , ACOEFF , IATYP ,
     &                  FIJ    , EIJ     , IXTYP , FXC2   , FXC3  ,
     &                  IXOPS  , IIBTS   , IIFPT , IIDIF  , ICT   , 
     &                  ITOUT  , XAN, YAN, APOMAN, DIFOMN , TOAN  ,
     &                  GOAN   , APGON   , EXCRN , DEXCRN , GBARFN,
     &                  ISTDIM , DATE    , IANOPT, IASEL  ,
     &                  S      , CHEADER , TEXDSN, ITEXOUT, GF    ,
     &                  BCVAL  , B       , TDAT  , EDAT   , XDAT  ,
     &                  INDIM  , LARCH  
     &                  )
       ENDIF
C
C-----------------------------------------------------------------------
C
 999   STOP
       END
