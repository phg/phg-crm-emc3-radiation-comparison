CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/spfman4e.for,v 1.3 2004/07/06 15:21:36 whitefor Exp $ Date $Date: 2004/07/06 15:21:36 $
CX
       SUBROUTINE SPFMAN4E( Z0    , Z     , ZEFF  , TITLE , IETYP , 
     &                      IXTYP , IND1  , IND2  , WI, WJ, EI, EJ,  
     &                      IATYP , ACOEFF, IFTYP , IOTYP , IFOUT ,
     &                      IXMAX , ITMAX , EDAT  , XDAT  , TDAT  ,
     &                      IIORD , IIBTS , IIFPT , IIXOP , IIDIF , 
     &                      XTIT1 , IGRD1 , XL1   , XU1   ,
     &                      YL1   , YU1   , XTIT2 , IGRD2 , 
     &                      XL2   , XU2   , YL2   , YU2   , IWRITE,
     &                      FXC2  , FXC3  , XA    , YA    , APOMA ,
     &                      DIFOMA, TOA   , GOA   , APGOA , EXCRA ,
     &                      DEXCRA, GBARFA,  
     &                      ICT   , ITOUT , S     , FIJ   , EIJ   
     &                     )
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PROGRAM TO ANALYSE ELECTRON IMPACT CROSS-SECTION DATA AND CONVERT TO 
C  RATE COEFFICIENTS                                                    
C                                                                       
C  VARIOUS FORMS OF DATA ENTRY ARE ALLOWED                              
C                                                                       
C  EXTENDED ARRAY DIMENSION VERSION OF SPFMAIN4                         
C                                                                       
C  DATA IS FITTED WITH APPROXIMATE FORMS TO AID INTERPOLATION DEPENDING 
C  ON THE TRANSITION TYPE. THESE ARE                                    
C                 1. DIPOLE                                             
C                 2. NON-DIPOLE                                         
C                 3. SPIN CHANGE                                        
C                 4. OTHER                                              
C                                                                       
C  DATA ENTRY IS VIA CALL TO PANEL SUBROUTINE  SPFMA4E  AS FOLLOWS:     
C                                                                       
C  INPUT                                                                
C      IPAN = INITIAL PANEL NUMBER AT START                             
C                                                                       
C  OUTPUT                                                               
C      IPAN   = FINAL PANEL NUMBER                                      
C      ANS    = YES  - FINISH UP CALCULATION SINCE NO MORE CASES        
C             = NO   - DATA FOR NEW CASE RETURNED                       
C      Z0     = NUCLEAR CHARGE OF ION                                   
C      Z      = ION CHARGE                                              
C      ZEFF   = ION CHARGE + 1                                          
C      TITLE  = TITLE FOR CASE                                          
C      IETYP  = 1  LEVEL ENERGIES IN CM-1                               
C             = 2  LEVEL ENERGIES IN RYD                                
C      IXTYP  = 1  DIPOLE TRANSITION                                    
C             = 2  NON-DIPOLE TRANSITION                                
C             = 3  SPIN CHANGE TRANSITION                               
C             = 4  OTHER                                                
C      IND1   = LOWER LEVEL INDEX  (USER CHOICE)                        
C      IND2   = UPPER LEVEL INDEX  (USER CHOICE)                        
C      WI     = LOWER LEVEL STATISTICAL WEIGHT                          
C      WJ     = UPPER LEVEL STATISTICAL WEIGHT                          
C      EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS)                  
C      EJ     = UPPER LEVEL ENERGY                                      
C      IATYP  = 1  A-COEFFICIENT RETURNED                               
C             = 2  OSCILLATOR STRENGTH RETURNED                         
C             = 3  LINE STRENGTH RETURNED                               
C      ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM, DIPOLE CASE ONLY)
C      IFTYP  = 1 UPPER K**2 (RYD) FOR COLLISION ENERGY UNITS           
C             = 2 LOWER K**2 (RYD)                                      
C             = 3 UPPER (K/Z0)**2 (RYD)                                
C             = 4 X PARAMETER                                           
C             = 5 UPPER (K/ZEFF)**2 (RYD)                               
C      IOTYP  = 1 EXCITATION CROSS-SECTION (PI*A0**2) RETURNED          
C             = 2 DE-EXCITATION CROSS-SECTION (PI*A0**2) RETURNED       
C             = 3 COLLISION STRENGTH RETURNED                           
C             = 4 SCALED COLLISION STRENGTH (Z**2*OMEGA) RETURNED       
C      IFOUT  = 1 KELVIN FOR OUTPUT TEMPERATURE UNIT                    
C             = 2   EV   FOR OUTPUT TEMPERATURE UNIT                   
C             = 3 SCALED UNITS RETURNED  (TE(K)/Z1**2)                  
C             = 4 REDUCED UNITS RETURNED (KTE/EIJ)                      
C      IXMAX  = NUMBER OF ENERGY/X-SECT PAIRS ENTERED                   
C      ITMAX  = NUMBER OF OUTPUT TEMPERATURES ENTERED                   
C      EDAT(I)= INPUT ENERGIES (SELECTED UNITS)                         
C      XDAT(I)= INPUT X-SECTS  (SELECTED UNITS)                         
C      TDAT(I)= OUTPUT TEMPS.  (SELECTED UNITS)                         
C      IIORD  = 1  4-PT GAUSS-LAGUERRE QUADRATURE                       
C             = 2  8-PT GAUSS-LAGUERRE QUADRATURE                       
C             = 3 12-PT GAUSS-LAGUERRE QUADRATURE                       
C      IIGPH  = 0  NO X-SECT GRAPH TO BE PRODUCED                       
C             = 1     X-SECT GRAPH TO BE PRODUCED                       
C      IIGPG  = 0  NO GAMMA  GRAPH TO BE PRODUCED                       
C             = 1     GAMMA  GRAPH TO BE PRODUCED                       
C      IIBTS  = 0  BAD POINT OPTION OFF                                 
C             = 1  BAD POINT OPTION ON                                  
C      IIFPT  = 1  SELECT ONE POINT OPTIMISING                          
C             = 2  SELECT TWO POINT OPTIMISING                          
C      IIXOP  = 0  OPTIMISING OFF                                       
C             = 1  OPTIMISING ON  (IF ALLOWED)                          
C      IIDIF  = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY WITH OPTIMISING)
C             = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT                 
C      XTIT1  = SPECIFIC TITLE FOR X-SECT GRAPH                         
C      IGRD1  = 10 DO NOT PUT GRAPH IN A GRIDFILE                       
C             = 11 PUT GRAPH IN A GRIDFILE                              
C      IDEF1  = 11 USE DEFAULT SCALING FOR GRAPH                        
C             = 10 SCALING FOR GRAPH RETURNED                          
C      XL1    = LOWER X FOR X-SECT GRAPH                                
C      XU1    = UPPER X FOR X-SECT GRAPH                                
C      YL1    = LOWER Y FOR X-SECT GRAPH                                
C      YU1    = UPPER Y FOR X-SECT GRAPH                                
C      XTIT2  = SPECIFIC TITLE FOR GAMMA GRAPH                          
C      IGRD2  = 10 DO NOT PUT GRAPH IN A GRIDFILE                       
C             = 11 PUT GRAPH IN A GRIDFILE                              
C      IDEF2  = 11 USE DEFAULT SCALING FOR GRAPH                        
C             = 10 SCALING FOR GRAPH RETURNED                           
C      XL2    = LOWER X FOR GAMMA GRAPH                                 
C      XU2    = UPPER X FOR GAMMA GRAPH                                 
C      YL2    = LOWER Y FOR GAMMA GRAPH                                 
C      YU2    = UPPER Y FOR GAMMA GRAPH                                 
C                                                                       
C                                                                       
C AUTHOR:                                                                       
C  ********* H.P. SUMMERS, JET              7  NOV 1989  ***************
C  ***                                COR  22  DEC 1989              ***
C  ***                                COR  27  FEB 1990              ***
C
C  DATE:     06-06-95				VERSION 1.1
C  MODIFIED: D.H. BROOKS, UNIV.OF STRATHCLYDE          
C            CHANGED TO SUBROUTINE AND EDITED OUT DATA ENTRY AND 
C            GRAPHIC DISPLAY.
C 
C  DATE:     10-07-96				VERSION 1.2
C  MODIFIED: WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC
C            COMMENTED OUT CALCULATION OF DIFOMA WHICH WAS CAUSING
C 	     ERRORS AND WAS VESTIGIAL ANYWAY.
C 
C  DATE:     11-05-99				VERSION 1.3
C  MODIFIED: HUGH SUMMERS
C            ADDED MISSING OUTPUT VARIABLES TO PARAMETERS OF SUBROUTINE
C            ENSURED SETTING OF Z0,Z1,ZEFF
C-----------------------------------------------------------------------
       INCLUDE 'PARAMS'
C----------------------------------------------------------------------
       INTEGER I4UNIT
C-----------------------------------------------------------------------
       DIMENSION C1(ISTDIM,ISTDIM-1),C2(ISTDIM,ISTDIM-1),
     &           C3(ISTDIM,ISTDIM-1),C4(ISTDIM,ISTDIM-1)                
       DIMENSION APGOA(ISTDIM),XA(ISTDIM),YA(ISTDIM),
     &          XOA(ISTDIM),YOA(ISTDIM)                
       DIMENSION ITITLE(10),XSA(ISTDIM),YSA(ISTDIM),
     &           XOS(ISTDIM),YOS(ISTDIM)             
       DIMENSION WG(12,3),XG(12,3),TOA(ISTDIM),NQORD(3),GOA(ISTDIM)             
       DIMENSION APOUT(ISTDIM),APOMA(ISTDIM),DIFOMA(ISTDIM)
       DIMENSION EXCRA(ISTDIM),DEXCRA(ISTDIM),GBARFA(ISTDIM)
       DIMENSION EDAT(ISTDIM),XDAT(ISTDIM),TDAT(ISTDIM)
       CHARACTER ANS*3,TITLE*40,XTIT1*40,XTIT2*40                       
       CHARACTER*30 XANNA(2),XANNOT,YANNA(6),YANNOT                     
       CHARACTER GRID*40
       REAL*4 XP(400),YP(400),XPA(ISTDIM),YPA(ISTDIM)                           
       EXTERNAL FORM4                                                   
       COMMON /ESPL3/IEND1,IENDN,G1,GN,A1,B1,AN,BN,P1(3),Q1(3),         
     & PN(3),QN(3),A1RY(ISTDIM),B1RY(ISTDIM),ANRY(ISTDIM),BNRY(ISTDIM)
C-----------------------------------------------------------------------
C  GAUSS-LAGUERRE QUADRATURE COEFFICIENTS                               
C-----------------------------------------------------------------------
       DATA NQORD/4,8,12/                                               
       DATA WG/6.03154104342D-1 ,3.57418692438D-1 ,                    
     &         3.88879085150D-2 ,5.39294705561D-4 ,8*0.0D0,             
     &         3.69188589342D-1 ,4.18786780814D-1 ,                     
     &         1.75794986637D-1 ,3.33434922612D-2 ,                     
     &         2.79453623523D-3 ,9.07650877336D-5 ,                     
     &         8.48574671627D-7 ,1.04800117487D-9 ,4*0.0D0,             
     &         2.64731371055D-1 ,3.77759275873D-1 ,                     
     &         2.44082011320D-1 ,9.04492222117D-2 ,                     
     &         2.01023811546D-2 ,2.66397354187D-3 ,                     
     &         2.03231592663D-4 ,8.36505585682D-6 ,                     
     &         1.66849387654D-7 ,1.34239103052D-9 ,                     
     &         3.06160163504D-12,8.14807746743D-16/                     
       DATA XG/0.322547689619D0 ,1.745761101158D0 ,                     
     &         4.536620296921D0 ,9.395070912301D0 ,8*0.0D0,             
     &         0.170279632305D0 ,0.903701776799D0 ,                     
     &         2.251086629866D0 ,4.266700170288D0 ,                    
     &         7.045905402393D0 ,1.0758516010181D1,                     
     &         1.5740678641278D1,2.2863131736889D1,4*0.0D0,             
     &         0.115722117358D0 ,0.611757484515D0 ,                     
     &         1.512610269776D0 ,2.833751337744D0 ,                     
     &         4.599227639418D0 ,6.844525453115D0 ,                     
     &         9.621316842457D0 ,1.3006054993306D1,                     
     &         1.7116855187462D1,2.2151090379397D1,                     
     &         2.8487967250984D1,3.7099121044467D1/                  
C-----------------------------------------------------------------------
C  ANNOTATIONS FOR GRAPH AXES                                           
C-----------------------------------------------------------------------
C       DATA XANNA(1)/'1-1/X  (X=THRESHOLD PARAMETER)'/                  
C       DATA XANNA(2)/'LOG10(TE(K))                  '/                  
C       DATA YANNA(1)/'OMEGA-APPROX.OMEGA            '/                  
C       DATA YANNA(2)/'OMEGA/APPROX.OMEGA            '/                  
C       DATA YANNA(3)/'LOG10(OMEGA)                  '/                  
C       DATA YANNA(4)/'GAMMA                         '/                  
C       DATA YANNA(5)/'OMEGA                         '/                  
C       DATA YANNA(6)/'                              '/                  
C-----------------------------------------------------------------------
C  ERRSET STATEMENTS INTRODUCED 16 AUG 1984 TO COUNTER                  
C  UNEXPLAINED PROBLEM ON INPUT BETWEEN LABELS 45 AND 230               
C  OUTPUT OF OSCILLATOR STRENGTH INTRODUCED   8 NOV 1984                
C-----------------------------------------------------------------------
C      CALL ERRSET(208,256,-1)                                          
C      CALL ERRSET(209,2,-1)                                            
C      CALL ERRSET(262,2,-1)                                            
C-----------------------------------------------------------------------
C  INITIALISE FOR GHOST GRAPHICS, GRIDFILE OUTPUT ON FILE GH80GRID.LIST 
C-----------------------------------------------------------------------
C       CALL PAPER(1)                                                    
C       GRID=' '
C       CALL FILNAM(GRID) 
C       ACOEFF=0.00D+00                                                  
C-----------------------------------------------------------------------
C  PANEL ENTRY BY SUBROUTINE SPFMA4E                                    
C-----------------------------------------------------------------------
C 8900  IPAN=0                                                           
C       CALL SPFMA4E(IPAN,ANS,Z0,Z,ZEFF,TITLE,IETYP,IXTYP,               
C     & IND1,IND2,WI,WJ,EI,EJ,IATYP,ACOEFF,IFTYP,IOTYP,IFOUT,            
C     & IXMAX,ITMAX,EDAT,XDAT,TDAT,                                      
C     & IIORD,IIGPH,IIGPG,IIBTS,IIFPT,IIXOP,IIDIF,XTIT1,IGRD1,IDEF1,XL1, 
C     & XU1,YL1,YU1,XTIT2,IGRD2,IDEF2,XL2,XU2,YL2,YU2)                   
C 8510  IF(ANS.EQ.'YES')GOTO 9999                                        
C        
       DO 1 I =1,ISTDIM
         TOA(I) = 0.0
         GOA(I) = 0.0
 1     CONTINUE
C              
       Z1=Z+1.0D0
       ZEFF=Z1                                                       
C       FXC2=0.0D0                                                       
C       FXC3=0.0D0                                                       
       S=0.0D0                                                          
       FIJ=0.0D0                                                        
C-----------------------------------------------------------------------
C  CONVERSION OF INPUT AND OUTPUT DATA FORMS TO STANDARD                
C-----------------------------------------------------------------------
       IF(IETYP.EQ.1)THEN                                               
           EI=EI/1.09737D5                                              
           EJ=EJ/1.09737D5                                              
       ENDIF                                                            
       EIJ=EJ-EI                                                        
       IF(IXTYP.EQ.1)THEN                                               
           IF(IATYP.EQ.2)THEN                                           
               ACOEFF=8.03232E9*EIJ*EIJ*WI*ACOEFF/WJ                    
           ELSEIF(IATYP.EQ.3)THEN                                       
               ACOEFF=2.67744E9*EIJ*EIJ*EIJ*ACOEFF/WJ                   
           ENDIF 
           S=3.73491E-10*WJ*ACOEFF/(EIJ*EIJ*EIJ)                        
           FIJ=3.333333D-1*EIJ*S/WI                                     
       ENDIF                                                            
C                                                     
       DO 40 ICT=1,IXMAX                                                 
        X=EDAT(ICT)                                                     
        Y=XDAT(ICT)                                                     
        IF(IFTYP.EQ.1)THEN                                              
            XA(ICT)=X/EIJ+1.0D0                                         
        ELSEIF(IFTYP.EQ.2)THEN                                          
            XA(ICT)=X/EIJ                                               
        ELSEIF(IFTYP.EQ.3)THEN                                          
            XA(ICT)=Z0*Z0*X/EIJ+1.0D0                                   
        ELSEIF(IFTYP.EQ.4)THEN                                          
            XA(ICT)=X                                                   
        ELSEIF(IFTYP.EQ.5)THEN                                          
            XA(ICT)=ZEFF*ZEFF*X/EIJ+1.0D0                               
        ENDIF                                                           
        IF(IOTYP.EQ.1)THEN                                              
            YA(ICT)=Y*WI*EIJ*XA(ICT)                                    
        ELSEIF(IOTYP.EQ.2)THEN                                          
            YA(ICT)=Y*WJ*EIJ*(XA(ICT)-1.0D0)                            
        ELSEIF(IOTYP.EQ.3)THEN                                          
            YA(ICT)=Y                                                   
        ELSEIF(IOTYP.EQ.4)THEN                                          
            YA(ICT)=Y/(ZEFF*ZEFF)                                       
        ENDIF                                                              
   40  CONTINUE                                                          
       ICT=IXMAX                                                         
C                                                                       
       DO 55 ITOUT=1,ITMAX                                              
        TE=TDAT(ITOUT)                                                  
        IF(IFOUT.EQ.1)THEN                                              
            TOA(ITOUT)=TE                                               
        ELSEIF(IFOUT.EQ.2)THEN                                          
            TOA(ITOUT)=1.16054D4*TE                                     
        ELSEIF(IFOUT.EQ.3)THEN                                          
            TOA(ITOUT)=TE*Z1*Z1                                         
        ELSEIF(IFOUT.EQ.4)THEN                                          
            TOA(ITOUT)=1.5789D5*TE*EIJ                                  
        ENDIF                                                           
   55  CONTINUE                                                         
       ITOUT=ITMAX                                                      
       IQORD=IIORD                                                      
cx       IGPH=IIGPH                                                       
C-----------------------------------------------------------------------
C  CONVERT TO BEST VARIABLES FOR SPLINING DEPENDING UPON TRANSITION TYPE
C-----------------------------------------------------------------------
       IF(IXTYP.EQ.1)THEN                                               
           IFPTS=IIFPT                        
   72      IF(ICT.LE.1.OR.YA(1).GE.YA(ICT).OR.IFPTS.EQ.1)THEN           
               IFPTS=1                                                  
               FXC3=1.333333D0                                          
               FXC2=DEXP(YA(1)/(FXC3*S))-XA(1)                         
               IF(FXC2.GT.0.0D0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IXOPT=0                                              
               ENDIF                                                    
           ELSEIF(IFPTS.EQ.2)THEN                                       
               FXC2=0.0D0                                               
               DO 73 ITER=1,20                                           
   73          FXC2=-XA(1)+(XA(ICT)+FXC2)**(YA(1)/YA(ICT))              
               FXC3=YA(1)/(S*DLOG(XA(1)+FXC2))                          
               IF(FXC2.GT.0.0D0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IFPTS=1                                              
                   GO TO 72                                             
               ENDIF                                                    
           ENDIF                                                        
           IF(IXOPT.GT.0)THEN                                           
               IDIFF=IIDIF                                              
           ELSE                                                         
               IDIFF=1                                                  
               FXC2=0.0D0                                               
               FXC3=1.333333D0                                          
           ENDIF                                                        
       ELSEIF(IXTYP.EQ.2.AND.IIXOP.EQ.1)THEN                            
           IFPTS=IIFPT                                                  
   74      IF(ICT.LE.1.OR.IFPTS.EQ.1)THEN                               
               FXC2=0.0D0                                               
               FXC3=YA(1)                                               
               IXOPT=IIXOP                                              
           ELSEIF(IFPTS.EQ.2)THEN                                       
               FXC2=XA(1)*XA(ICT)*(YA(1)-YA(ICT))/(XA(ICT)-XA(1))       
               FXC3=YA(1)-FXC2/XA(1)                                    
               IF(FXC2+FXC3.GT.0.0D0.AND.FXC3.GT.0.0D0)THEN              
                   IXOPT=IIXOP                                          
               ELSE                                                    
                   IFPTS=1                                              
                   GO TO 74                                             
               ENDIF                                                    
           ENDIF                                                        
           IDIFF=0                                                      
       ELSEIF(IXTYP.EQ.3.AND.IIXOP.EQ.1)THEN                            
           IFPTS=IIFPT                                                  
   76      IF(ICT.LE.1.OR.YA(1).LE.YA(ICT).OR.IFPTS.EQ.1)THEN           
               IFPTS=1                                                  
               FXC2=100.0                                               
               FXC3=YA(1)*(XA(1)+FXC2)*(XA(1)+FXC2)                     
               IXOPT=IIXOP                                              
           ELSEIF(IFPTS.EQ.2)THEN                                       
               FXC2=(XA(ICT)*DSQRT(YA(ICT)/YA(1))-XA(1))/               
     &         (1.0D0 - DSQRT(YA(ICT)/YA(1)))                           
               IF(FXC2.LE.0.1D0)WRITE(I4UNIT(-1),251)                            
               FXC2=DMAX1(FXC2,0.1D0)                                   
               FXC3=YA(1)*(XA(1)+FXC2)*(XA(1)+FXC2)                     
               IF(FXC2.GT.0.0D0)THEN                                    
                   IXOPT=IIXOP                                          
               ELSE                                                     
                   IFPTS=1                                              
                   GO TO 76                                            
               ENDIF                                                    
           ENDIF                                                        
           IDIFF=0                                                      
       ELSEIF(IXTYP.EQ.4)THEN                                           
           IDIFF=1                                                      
       ELSEIF(IIXOP.EQ.0)THEN                                           
           FCX2=0.0D0                                                   
           FXC3=0.0D0                                                   
           IXOPT=IIXOP                                                  
           IFPTS=0                                                      
           IDIFF=1                                                      
       ENDIF                                                            
C                                                                        
       DO 75 K=1,ICT                                                   
        IF(IXTYP.EQ.1)THEN                                              
            APOMA(K)=FXC3*S*DLOG(XA(K)+FXC2)                            
            IF(IXOPT.EQ.1.AND.IDIFF.EQ.0)THEN                           
                YSA(K)=YA(K)/APOMA(K)                                   
            ELSE                                                        
                YSA(K)=YA(K)-APOMA(K)                                   
            ENDIF                                                       
            XSA(K)=1.0D0/XA(K)
        ELSEIF(IXTYP.EQ.2.AND.IXOPT.EQ.1)THEN                           
            APOMA(K)=FXC3+FXC2/XA(K)                                    
            YSA(K)=YA(K)/APOMA(K)                                       
            XSA(K)=1.0D0/XA(K)                                          
        ELSEIF(IXTYP.EQ.3.AND.IXOPT.EQ.1)THEN                           
            APOMA(K)=FXC3/(XA(K)+FXC2)**2                               
            YSA(K)=YA(K)/APOMA(K)                                       
            XSA(K)=1.0/XA(K)                                            
        ELSEIF(IXTYP.EQ.4)THEN                                          
            APOMA(K)=0.0D0                                              
            YSA(K)=DLOG10(YA(K))                                        
            XSA(K)=1.0/XA(K)                                            
        ELSEIF(IXOPT.EQ.0)THEN                                          
            APOMA(K)=0.0D0                                              
            YSA(K)=YA(K)                                                
            XSA(K)=1.0D0/XA(K)                                          
        ENDIF                                                           
        DIFOMA(K)=YA(K)-APOMA(K)                                        
   75  CONTINUE                              
       CALL ESORT(XSA,YSA,ICT)                                          
       IBPTS=IIBTS                                                      
       IF(IBPTS.LE.0)THEN                                               
           IEND1=1                                                      
           G1=0.0D0                                                     
           IENDN=1                                                     
           GN=0.0D0                                                     
           IFORMS=IXTYP                                                 
           IENDS=1                                                      
           X1=XSA(1)                                                    
           DX1=0.01*X1                         
           CALL EGASYM(X1,DX1,FORM4,IFORMS,IENDS)                       
           IENDS=2                                                      
           XN=XSA(ICT)                                                  
           DXN=0.01*XN                                                  
           CALL EGASYM(XN,DXN,FORM4,IFORMS,IENDS)                    
           CALL EGSPC(XSA,ICT,C1,C2,C3,C4)                              
       ENDIF                                                            
       DO 200 IT=1,ITOUT                                              
        TE=TOA(IT)                                                      
        ATE=1.57890D5*EIJ/TE                                           
        ICOUT=NQORD(IQORD)                                             
        SUM=0.0D0                                                      
        DO 85 K=1,ICOUT                                                 
         APOUT(K)=0.0D0                                                 
         XOA(K)=XG(K,IQORD)/ATE+1.0D0                                   
         X=1.0D0/XOA(K)                                                 
         XOS(K)=X                                                      
         IF(IBPTS.GT.0)THEN                                             
             CALL ELNFIT(X,XSA,Y,YSA,ICT)                               
         ELSE                                                           
             CALL EFASYM(X,XSA,ICT,YSA,Y,DY,
     &                   C1,C2,C3,C4,FORM4,IFORMS)  
         ENDIF                                                        
         YOS(K)=Y                                                      
         IF(IXTYP.EQ.1)THEN                                             
             APOUT(K)=FXC3*S*DLOG(XOA(K)+FXC2)                          
             IF(IDIFF.EQ.0)THEN                                         
                YOA(K)=(Y-1.0D0)*APOUT(K)                               
             ELSE                                                      
                 YOA(K)=Y                                              
             ENDIF                                                      
         ELSEIF(IXTYP.EQ.2.AND.IXOPT.EQ.1)THEN                          
             APOUT(K)=FXC3+FXC2/XOA(K)                                  
             YOA(K)=(Y-1.0D0)*APOUT(K)                                  
         ELSEIF(IXTYP.EQ.3.AND.IXOPT.EQ.1)THEN                          
             APOUT(K)=FXC3/(XOA(K)+FXC2)**2                             
             YOA(K)=(Y-1.0D0)*APOUT(K)                                  
         ELSEIF(IXTYP.EQ.4)THEN                                         
             APOUT(K)=0.0D0                                             
             YOA(K)=10.0D0**Y                                   
         ELSEIF(IXOPT.EQ.0)THEN                                         
             APOUT(K)=0.0D0                                             
             YOA(K)=Y                                                   
         ENDIF                                                          
   85   SUM=SUM+WG(K,IQORD)*YOA(K)                                      
        GOA(IT)=SUM                                                     
        IF(IXTYP.EQ.1)THEN                                              
            XE=(1.0D0+FXC2)*ATE                                         
            APGOA(IT)=FXC3*S*(DLOG(1.0D0+FXC2)+EEI(XE))                 
            GOA(IT)=GOA(IT)+APGOA(IT)                                   
        ELSEIF(IXTYP.EQ.2.AND.IXOPT.EQ.1)THEN                           
            XE=ATE                                                      
            APGOA(IT)=FXC3+FXC2*ATE*EEI(ATE)                            
            GOA(IT)=GOA(IT)+APGOA(IT)                                   
        ELSEIF(IXTYP.EQ.3.AND.IXOPT.EQ.1)THEN                   
            XE=(1.0D0+FXC2)*ATE                                 
            APGOA(IT)=FXC3*ATE*ATE*EE2(XE)/XE                   
            GOA(IT)=GOA(IT)+APGOA(IT)                           
        ELSEIF(IXTYP.EQ.4)THEN                                  
            APGOA(IT)=0.0D0                                             
            GOA(IT)=GOA(IT)                                             
        ELSEIF(IXOPT.EQ.0)THEN                                          
            APGOA(IT)=0.0D0                                             
            GOA(IT)=GOA(IT)                                             
        ENDIF                                                           
        DEXCRA(IT)=2.1720D-8*DSQRT(ATE/EIJ)*GOA(IT)/WJ                  
        EXCRA(IT)=WJ*DEXP(-ATE)*DEXCRA(IT)/WI                           
        GBARFA(IT)=6.8916D-2*EIJ*GOA(IT)/WI                             
        DO 87 K=1,ICOUT                                                 
         XP(IT*ICOUT-ICOUT+K)=1.0D0-XOS(K)                              
         YP(IT*ICOUT-ICOUT+K)=YOS(K)                                    
   87   CONTINUE                                                        
  200  CONTINUE                                                    
  246  FORMAT(1H1,A40//1H0,'NUCLEAR CHARGE =',F6.2,'   ION CHARGE =',
     & F6.2,'   EFFECTIVE ION CHARGE =',F6.2/ 
     & 1H0,'LOWER  INDEX = ',I5,20X,'UPPER  INDEX = ', 
     & I5/1H0,'LOWER ENERGY = ',F12.6,13X,'UPPER ENERGY = ',F12.6/
     & 1H0,'LOWER WEIGHT = ',F5.1,20X,'UPPER WEIGHT = ',F5.1///  
     & 1H0,'ACOEFF = ',1PE12.4,5X,'S = ',1PE12.4,5X,'FABS = ',
     & 1PE12.4,5X,'ENERGY DIFFERENCE = ',0PF12.6/
     & 1H0,'XSECT TYPE= ',I3,3X,'XSECT OPTIMISATION FACTORS , FXC2 = ',
     & 1PE11.4,3X,'FXC3 = ',1PE11.4,3X,'IBPTS =', 
     & I2,3X,'IFPTS = ',I2,3X,'IDIFF =',I2/
     & 1H0,'    X',20X,'  OMEGA',10X,'APPROX. OMEGA',12X,'DIFFERENCE')
  247  FORMAT(1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4)
  248  FORMAT(1H0,'  TE(K)',18X,'   GAMMA',11X,'APPROX. GAM',13X,
     & 'EX. RATE',13X,'DEXC. RATE',14X,'GBARF')
  249  FORMAT(1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4,
     & 10X,1PE12.4) 
  251  FORMAT(1H0,'** WARNING:-TYPE 3, FXC2 TOO SMALL-LIMITED TO 0.1 **'
     & )                                                                

      RETURN
      END                                                               
