CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/a1out0.for,v 1.5 2004/07/06 09:59:19 whitefor Exp $ Date $Date: 2004/07/06 09:59:19 $
CX
       SUBROUTINE A1OUT0( DSFULL , INDXREF , TITLE , CAMETH , Z0    ,
     &                    Z      , ZEFF    , INDL  , INDU   , EI    , 
     &                    EJ     , WI      , WJ    , ACOEFF , IATYP ,
     &                    FIJ    , EIJ     , IXTYP , FXC2   , FXC3  ,
     &                    IXOPS  , IIBTS   , IIFPT , IIDIF  , ICT   , 
     &                    ITOUT  , XAN, YAN, APOMAN, DIFOMA , TOAN  ,
     &                    GOAN   , APGOA   , EXCRA , DEXCRA , GBARFA,
     &                    ISTDIM , DATE    , IANOPT, IASEL  ,
     &                    S      , CHEADER , TEXDSN, ITEXOUT, GF    ,
     &                    BCVAL  , B       , TDAT  , EDAT   , XDAT  ,
     &                    INDIM  , LARCH 
     &                )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A1OUT0*******************
C
C  PURPOSE: 
C        TO WRITE DATA TO AN OLD/NEW ARCHIVE IN THE FORMAT DECIDED
C        BY THE ANALYSIS OPTION.
C
C  CALLING PROGRAM:
C        ADAS101.FOR
C
C  DATA:  
C        CIARR(500)*80- 500 IS THE CURRENT LIMIT ON INDEXES
C
C  INPUT:
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                              FILE.
C            (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                              TWO TYPES OF ANALYSIS.
C                              A - ADAS, B- BURGESS
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION         
C            (R*8)  Z      = ION CHARGE                  
C            (R*8)  ZEFF   = ION CHARGE + 1
C            (I*4)  INDL   = LOWER LEVEL INDEX  (USER CHOICE)      
C            (I*4)  INDU   = UPPER LEVEL INDEX  (USER CHOICE)    
C            (R*8)  WI     = LOWER LEVEL STATISTICAL WEIGHT     
C            (R*8)  WJ     = UPPER LEVEL STATISTICAL WEIGHT      
C            (R*8)  EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS)  
C            (R*8)  EJ     = UPPER LEVEL ENERGY  
C            (R*8)  ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM,
C                            DIPOLE CASE ONLY)
C            (I*4)  IATYP  = FORM OF ACOEFF
C                            1 A-COEFFICIENT RETURNED
C                            2 OSCILLATOR STRENGTH RETURNED
C                            3 LINE STRENGTH RETURNED
C            (I*4)  IXTYP  = 1  DIPOLE TRANSITION             
C                          = 2  NON-DIPOLE TRANSITION        
C                          = 3  SPIN CHANGE TRANSITION      
C                          = 4  OTHER     
C            (I*4)  IIBTS  = 0  BAD POINT OPTION OFF        
C                          = 1  BAD POINT OPTION ON                
C            (I*4)  IIFPT  = 1  SELECT ONE POINT OPTIMISING    
C                          = 2  SELECT TWO POINT OPTIMISING     
C            (I*4)  IXOPS  = 0  OPTIMISING OFF              
C                          = 1  OPTIMISING ON  (IF ALLOWED)    
C            (I*4)  IIDIF  = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY
C                               WITH OPTIMISING)
C                          = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT
C            (R*8)  FIJ    = OSCILLATOR STRENGTH
C            (R*8)  EIJ    = TRANSITION ENERGY
C            (R*8)  FXC2   = SPLINING VARIABLE
C            (R*8)  FXC3   = SPLINING VARIABLE
C            (I*4)  ICT    = NUMBER OF X-SECTIONS
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (R*8)  APOMAN = APPROXIMATE OMEGA
C            (R*8)  DIFOMA = DIFFERENCE BETWEEN YAN & APOMAN
C            (R*8)  GOAN   = GAMMA (EFFECTIVE COLLISION STRENGTH)
C            (R*8)  APGOA  = APPROXIMATE GAMMA
C            (R*8)  EXCRA  = EXCITATION RATE COEFFICIENT
C            (R*8)  DEXCRA = DEEXCITATION RATE COEFFICIENT
C            (R*8)  GBARFA = G BAR FUNCTION
C            (I*4)  ISTDIM = THE MAXIMUM ARRAY DIMENSION
C            (R*8)  BCVAL  = BURGESS SCALABLE PARAMETERC
C            (R*8)  XAN    = ENERGY (PARAMETER X) - ADAS OPTION
C            (R*8)  YAN    = OMEGA (COLLISION STRENGTH) -ADAS OPTION
C            (R*8)  TOAN   = TEMPERATURE SET - ADAS OPTION
C            (C*8)  DATE   = THE CURRENT DATA 
C            (I*4)  IANOPT = ANALYSIS OPTION
C                            1 - ADAS OPTION
C                            2 - BURGESS OPTION  
C            (I*4)  IASEL  = THE ARCHIVING CHOICE (SEE A1SPF0.F)
C            (R*8)  S      = THE LINE STRENGTH
C            (C*80) CHEADER= THE ADAS HEADER FOR THE PAPER.TXT FILE
C            (C*80) TEXDSN = THE DEFAULT HARD COPY FILE NAME
C            (I*4)  ITEXOUT= THE HARD COPY OF FILE OUTPUT FLAG
C                            0 - NO, 1 - YES
C            (R*8)  GF     = THE WEIGHTED OSCILLATOR STRENGTH
C            (R*8)  BCVAL  = THE BURGESS SCALABLE PARAMETER C.
C            (R*8)  B      = THE FIVE BURGESS KNOT POINTS
C            (R*8)  EDAT - ENERGY (PARAMETER X) - BURGESS OPTION
C            (R*8)  XDAT - OMEGA (COLLISION STRENGTH)- BURGESS OPTION
C            (R*8)  TDAT - TEMPERATURE SET- BURGESS OPTION 
C            (I*4)  INDIM  = ARRAY DIMENSION
C            (L)    LARCH  = ARCHIVING SELECTION OPTION
C
C
C  ROUTINES:
C        WUPSILON - WRITES OUT DATA IN BURGESS FORMAT
C
C  AUTHOR: 
C        DAVID H.BROOKS (UNIV.OF STRATHCLYDE) EXT.4213/4205
C
C  DATE: 14/06/95				VERSION 1.1
C  MODIFIED: DAVID H.BROOKS
C		- FIRST RELEASE
C
C  DATE: 11/07/95				VERSION 1.2
C  MODIFIED: TIM HAMMOND
C		- TIDIED UP COMMENTS AND CODE
C
C  DATE: 17/07/95				VERSION 1.3
C  MODIFIED: TIM HAMMOND
C		- INCREASED LENGTH OF TEXDSN TO 80 FROM 45
C
C  DATE: 02/04/96				VERSION 1.4
C  MODIFIED: TIM HAMMOND/PAUL BRIDEN
C		- TIDY IANOPT DEPENDENT GOTO (50 & 100) TO A SINGLE
C                 GOTO (100) - STOPS COMPILER WARNING UNDER AIX.
C
C  DATE: 07/05/99				VERSION 1.5
C  MODIFIED: HUGH SUMMERS
C		- CORRECTED CONFUSION ABOUT NCHAR AND NELEC
C                 CORRECTED DISPLACEMENT IN 'XSECT TYPE' FIELD
C
C-----------------------------------------------------------------------
       INTEGER INDXREF , ICHK1 , INDL  , INDU , ICT   , ITOUT  
       INTEGER IXTYP   , IIBTS , IIFPT , IIDIF, ISTDIM, IWRITE
       INTEGER IATYP   , IXOPS , INDX  , IANOPT, I    , ICNT , INDIM
       INTEGER IASEL   , PIPEIN, PIPEOU, ITEXOUT, IHFLAG 
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80 , CSTRING*32 , CSTRNG*80 
       CHARACTER TITLE*40  , CAMETH*4   , DATE*8     , CODE*4
       CHARACTER DCODE(2)*4, CINDX*3    , CIARR(500)*80, CEIJ*20
       CHARACTER CHEADER*80, TEXDSN*80  
C-----------------------------------------------------------------------
       PARAMETER( IWRITE = 9 , PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
       INTEGER I4UNIT , IASELCOPY
C-----------------------------------------------------------------------
       LOGICAL LARCH
C-----------------------------------------------------------------------
       REAL*8 Z , Z0 , ZEFF , EI  , EJ   , WI   , WJ , S , NELEC
       REAL*8 ACOEFF ,  FIJ , EIJ , FXC2 , FXC3 , BCVAL , GF 
C-----------------------------------------------------------------------
       REAL*8 XAN(ISTDIM) , YAN(ISTDIM), APOMAN(ISTDIM), DIFOMA(ISTDIM)
       REAL*8 TOAN(ISTDIM), GOAN(ISTDIM), APGOA(ISTDIM) , EXCRA(ISTDIM)
       REAL*8 DEXCRA(ISTDIM) , GBARFA(ISTDIM), B(5), TDAT(ISTDIM)
       REAL*8 EDAT(ISTDIM) , XDAT(ISTDIM) 
C-----------------------------------------------------------------------
       DATA DCODE/'   A','   B'/
C-----------------------------------------------------------------------
C
       IF (.NOT.LARCH) THEN
           IHFLAG = 1
       ELSE
           IHFLAG = 0
       ENDIF
       IF (IHFLAG.EQ.0) THEN
           OPEN( UNIT=IWRITE , FILE = DSFULL , STATUS = 'UNKNOWN' )
           IF (IASEL.NE.3) THEN
C
C-----------------------------------------------------------------------
C  CHECK FOR HIGHEST INDEX
C-----------------------------------------------------------------------
C
 1             READ(IWRITE,1015) CSTRNG
               IF (CSTRNG(1:1).NE.' '.AND.CSTRNG(1:1).NE.'-') THEN
                   IF (CSTRNG(1:1).NE.'C') THEN
                       CINDX = CSTRNG(2:4)
                       GO TO 1
                   ELSE
                       CINDX = CINDX
                   ENDIF
               ELSE
                   GO TO 1
               ENDIF
C
C-----------------------------------------------------------------------
C  COPY INDEX COMMENTS TO TEMPORARY ARRAY
C-----------------------------------------------------------------------
C
               CIARR(1) = CSTRNG
               ICNT = 2
 2             READ(IWRITE,1015) CSTRNG
               IF (CSTRNG(1:2).EQ.'C ') THEN
                   CIARR(ICNT) = CSTRNG
                   ICNT = ICNT+1
                   GO TO 2
               ELSE
                   CIARR(ICNT) = CSTRNG
                   ICNT = ICNT+1
               ENDIF
C
C-----------------------------------------------------------------------
C  CLOSE FILE OFF TO RE-OPEN FOR POSITION FINDING
C-----------------------------------------------------------------------
C
               CLOSE( UNIT=IWRITE , STATUS = 'KEEP')
               OPEN( UNIT=IWRITE , FILE = DSFULL , STATUS = 'UNKNOWN' )
 3             READ(IWRITE,1015) CSTRNG
               IF (CSTRNG(1:1).EQ.'I') THEN
                   IF (CSTRNG(2:4).NE.CINDX) THEN
                       GO TO 3
                   ENDIF
               ELSE
                   GO TO 3
               ENDIF
C
 4             READ(IWRITE,1015) CSTRNG
               IF (CSTRNG(1:1).NE.'-') THEN
                   GO TO 4
               ENDIF
C
C  CLOCK INDEX UP ONE
C
               READ(CINDX,1016) INDX
               INDX = INDX+1
           ELSE
               INDX = 1
           ENDIF
       ENDIF
C
C-----------------------------------------------------------------------
C  NOW WRITE OUT DATA 
C-----------------------------------------------------------------------
C
         IF (IANOPT.EQ.1) THEN
             CODE = DCODE(1)
         ELSE
             CODE = DCODE(2)
         ENDIF 
 100     IF (IANOPT.EQ.2) THEN

             IF (IHFLAG.EQ.1) THEN
                 OPEN(UNIT = IWRITE,FILE = TEXDSN, STATUS = 'UNKNOWN')
                 WRITE(IWRITE,*) CHEADER
             ENDIF
             WRITE(CSTRING,1000) TITLE     
             WRITE(IWRITE,1001) INDX, CSTRING, DATE, CODE
             NELEC=Z0-Z
             CALL WUPSILON( EI    , EJ    , EIJ   , IXTYP , GF    , 
     &                      BCVAL , B     , INDL  , INDU  , NELEC ,
     &                      Z0    , ITOUT , TDAT  , GOAN  , DSFULL,
     &                      ISTDIM, ICT   , EDAT  , XDAT  , IWRITE,
     &                      INDIM , WI    , WJ
     &                     ) 
         ELSE
             IF (IHFLAG.EQ.1) THEN
                 OPEN(UNIT = IWRITE,FILE = TEXDSN, STATUS = 'UNKNOWN')
                 WRITE(IWRITE,*) CHEADER
             ENDIF
             WRITE(CSTRING,1000) TITLE
             WRITE(IWRITE,1001) INDX, CSTRING, DATE, CODE
             WRITE(IWRITE,1003) Z0, Z, ZEFF
             WRITE(IWRITE,1004) INDL, INDU
             WRITE(IWRITE,1005) EI, EJ
             WRITE(IWRITE,1006) WI, WJ 
C 
             WRITE(CEIJ,*) EIJ
             WRITE(IWRITE,1007) ACOEFF, S, FIJ, CEIJ
C 
             WRITE(IWRITE,1008) IXTYP, FXC2, FXC3, IXOPS, IIBTS, 
     &                          IIFPT, IIDIF 
             WRITE(IWRITE,1009)
C 
             DO 10 I = 1, ICT 
                 WRITE(IWRITE,1010) XAN(I), YAN(I), APOMAN(I),
     &                              DIFOMA(I)
 10          CONTINUE
             WRITE(IWRITE,1011)
             DO 20 I = 1,ITOUT
                 WRITE(IWRITE,1012) TOAN(I), GOAN(I), APGOA(I),
     &                              EXCRA(I), DEXCRA(I), GBARFA(I)
 20          CONTINUE
         ENDIF
         IF (IHFLAG.EQ.0) THEN
             IF (IASEL.EQ.3) THEN
                 WRITE(IWRITE,1013)
             ELSE
                 WRITE(IWRITE,1017)
                 DO 30 I = 1, ICNT-2
                     WRITE(IWRITE,1015) CIARR(I)
 30              CONTINUE
             ENDIF
C
             WRITE(IWRITE,1014) INDX, CODE, CSTRING(1:6), 
     &                          CSTRING(7:22), CSTRING(23:32),
     &				DATE
         ENDIF
         CLOSE( UNIT=IWRITE , STATUS = 'KEEP')
C
C GO BACK TO WRITE HARD COPY IF REQUESTED & SET UP OPTION FLAG
C
         IF (IHFLAG.EQ.0) THEN
             IF (ITEXOUT.EQ.1) THEN
                 IHFLAG = 1
                 IASELCOPY = IASEL
                 IASEL = 3
                 GOTO 100 
             ENDIF
         ENDIF
         IASEL = IASELCOPY
C
C-----------------------------------------------------------------------
C
 1000  FORMAT(1A32)
 1001  FORMAT('I',I3,2X,1A32,1A8,24X,1A4)
 1003  FORMAT(' NUCLEAR CHARGE =',F6.2,'   ION CHARGE =',F6.2,
     &        '   EFFECTIVE ION CHARGE =',F6.2)
 1004  FORMAT(' LOWER  INDEX = ',I5,19X,' UPPER  INDEX = ',I5)
 1005  FORMAT(' LOWER ENERGY = ',F12.6,12X,' UPPER ENERGY = ',F12.6)
 1006  FORMAT(' LOWER WEIGHT = ',F5.1,19X,' UPPER WEIGHT = ',F5.1)
 1007  FORMAT(' ACOEFF = ',1PE12.4,'     S = ',1PE11.4,
     &        '      FABS = ',1PE12.4,'     ENERGY DIFFERENCE =',
     &        1A13)
 1008  FORMAT(' XSECT TYPE=',I3,'   XSECT OPT. FACTORS, FXC2 = ',
     &        1PE11.4,'   FXC3 = ',1PE11.4,'   IXOPS =',I2,
     &        '   IBPTS =',I2,'   IFPTS = ',I2,'   IDIFF =',I2)
 1009  FORMAT('    X',20X,'  OMEGA',10X,'APPROX. OMEGA ',10X,
     &        ' DIFFERENCE ') 
 1010  FORMAT(1PE11.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4)
 1011  FORMAT(' TE(K)',20X,' GAMMA ',9X,' APPROX. GAM',
     &        12X,' EX. RATE ',11X,' DEXC. RATE',13X,' GBARF ')
 1012  FORMAT(1PE11.4,10X,1PE12.4,10X,1PE12.4,10X,1PE12.4,10X,
     &        1PE12.4,10X,1PE12.4)
 1013  FORMAT('-1  '/
     & 'C-----------------------------------------------------------',
     & '------------'/
     & 'C INDEX  CODE  INFORMATION                                  ',
     & 'DATE       ')
 1014  FORMAT('C ',I3,2X,1A4,3X,1A6,1X,1A16,1X,1A10,10X,1A8,6X/
     & 'C-----------------------------------------------------------',
     & '------------')
 1015  FORMAT(1A80)
 1016  FORMAT(I3)
 1017  FORMAT('-1  ')
C-----------------------------------------------------------------------

       RETURN
       END
