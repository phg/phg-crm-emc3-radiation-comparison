CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/a1ispf.for,v 1.6 2004/07/06 09:58:47 whitefor Exp $ Date $Date: 2004/07/06 09:58:47 $
CX
       SUBROUTINE A1ISPF( DSFULL , INDXREF , TITLE , CAMETH , Z0    ,
     &                    Z      , ZEFF    , INDL  , INDU   , EI    , 
     &                    EJ     , WI      , WJ    , ACOEFF , IATYP ,
     &                    FIJ    , EIJ     , IXTYP , FXC2   , FXC3  ,
     &                    IXOPS  , IBPTS   , IFPTS , IDIFF  , ICT   , 
     &                    ITOUT  , XA , YA , APOMA , DIFOMA , TOA   ,
     &                    GOA    , APGOA   , EXCRA , DEXCRA , GBARFA,
     &                    ISTDIM , IASEL   , IFOUT , NTABCOL, EDAT  ,
     &                    XDAT   , TDAT    , IFTYP , IOTYP  , IOUTU ,
     &                    IETYP  , LPEND   , BCVAL
     &                )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A1ISPF *******************
C
C  PURPOSE: TO READ USER ALTERATIONS/SPECIFICATIONS FROM IDL AND TO
C           COMMUNICATE OLD ARCHIVE DATA TO IDL IF REQUESTED.
C
C  CALLING PROGRAM:
C            ADAS101.FOR
C
C  INPUT:
C            (R*8)  XA     = ENERGY (PARAMETER X)
C            (R*8)  YA     = OMEGA (COLLISION STRENGTH)C         
C            (R*8)  GOA    = GAMMA (EFFECTIVE COLLISION STRENGTHS)
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                              FILE.
C            (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                              TWO TYPES OF ANALYSIS.
C                              A - ADAS, B- BURGESS
C  I/O:
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION 
C            (R*8)  Z      = ION CHARGE                
C            (R*8)  ZEFF   = ION CHARGE + 1 
C            (I*4)  INDL   = LOWER LEVEL INDEX  (USER CHOICE)  
C            (I*4)  INDU   = UPPER LEVEL INDEX  (USER CHOICE)
C            (R*8)  WI     = LOWER LEVEL STATISTICAL WEIGHT 
C            (R*8)  WJ     = UPPER LEVEL STATISTICAL WEIGHT  
C            (R*8)  EI     = LOWER LEVEL ENERGY (IN SELECTED UNITS) 
C            (R*8)  EJ     = UPPER LEVEL ENERGY  
C            (R*8)  ACOEFF = TRANSITION PROBABILITY (IN ABOVE FORM,
C                            DIPOLE CASE ONLY)
C            (I*4)  IXTYP  = 1  DIPOLE TRANSITION      
C                          = 2  NON-DIPOLE TRANSITION      
C                          = 3  SPIN CHANGE TRANSITION  
C                          = 4  OTHER
C            (I*4)  IBPTS  = 0  BAD POINT OPTION OFF    
C                          = 1  BAD POINT OPTION ON 
C            (I*4)  IFPTS  = 1  SELECT ONE POINT OPTIMISING  
C                          = 2  SELECT TWO POINT OPTIMISING   
C            (I*4)  IXOPS  = 0  OPTIMISING OFF          
C                          = 1  OPTIMISING ON  (IF ALLOWED)   
C            (I*4)  IDIFF  = 0  RATIO FITTING FOR DIPOLE X-SECT(ONLY
C                               WITH OPTIMISING)
C                          = 1  DIFFERENCE FITTING FOR DIPOLE X-SECT
C            (R*8)  FIJ    = OSCILLATOR STRENGTH
C            (R*8)  EIJ    = TRANSITION ENERGY
C            (R*8)  FXC2   = SPLINING VARIABLE
C            (R*8)  FXC3   = SPLINING VARIABLE
C            (I*4)  ICT    = NUMBER OF X-SECTIONS
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (R*8)  APOMA  = APPROXIMATE OMEGA
C            (R*8)  DIFOMA = DIFFERENCE BETWEEN YA & APOMA
C            (R*8)  TOA    = TEMPERATURE SET
C            (R*8)  APGOA  = APPROXIMATE GAMMA
C            (R*8)  EXCRA  = EXCITATION RATE COEFFICIENT
C            (R*8)  DEXCRA = DEEXCITATION RATE COEFFICIENT
C            (R*8)  GBARFA = G BAR FUNCTION
C            (I*4)  ISTDIM = THE MAXIMUM ARRAY DIMENSION
C            (R*8)  BCVAL  = BURGESS SCALABLE PARAMETER
C  OUTPUT:
C            (I*4)  IASEL  - THE ARCHIVING CHOICE.
C                           0 - NO ARCHIVE
C                           1 - EXAMINE OLD ARCHIVE
C                           2 - REFRESH FROM ARCHIVE
C                           3 - NEW ARCHIVE
C            (I*4)  IFOUT  - THE UNITS SELECTED FOR THE ENERGIES,X-SECTIONS
C                            AND TEMPERATURES (READ FROM THE PIPE).
C            (I*4)  IFTYP  - THE ENERGY UNITS. (SEE SPFMAN4E.F FOR 
C                            A COMPLETE DESCRIPTION)
C            (I*4)  IOTYP  - THE X-SECTION UNITS.(SEE SPFMAN4E.F FOR 
C                            A COMPLETE DESCRIPTION)
C            (I*4)  IOUTU  - THE TEMPERATURE UNITS.(SEE SPFMAN4E.F FOR 
C			     A COMPLETE DESCRIPTION)
C            (I*4)  IETYP  - THE LEVEL ENERGY UNITS.
C                            1- CM-1
C                            2- RYDBERGS
C            (L)    LPEND    - 'CANCEL' OR 'DONE' DECISION.
C            (I*4)  NTABCOL- THE NUMBER OF COLUMNS IN THE EDITABLE IDL TABLE
C            (R*8)  EDAT - ENERGY (PARAMETER X): USER SPECIFIED
C            (R*8)  XDAT - OMEGA (COLLISION STRENGTH): USER SPECIFIED
C            (R*8)  TDAT - TEMPERATURE SET : USER SPECIFIED
C
C  ROUTINES:
C         XXFLSH - CLEARS PIPE BUFFER
C
C  AUTHOR:
C         DAVID H.BROOKS (UNIV.OF STRATHCLYDE) EXT.4213/4205
C
C  DATE:  26/05/95                   VERSION 1.1
C  MODIFIED: DAVID H. BROOKS
C               - FIRST RELEASE
C
C  DATE:  11/07/95                   VERSION 1.2
C  MODIFIED: TIM HAMMOND
C		- TIDIED UP CODE AND COMMENTS
C
C  DATE: 13/09/95		     VERSION 1.3
C  MODIFIED: TIM HAMMOND
C               - IMPROVED SOME OF WRITING TO FORTRAN TO RUN ON SUN
C		  MACHINES
C
C  DATE: 24/10/95	             VERSION 1.4
C  MODIFIED: DAVID H. BROOKS/TIM HAMMOND
C		- ADDED FURTHER CALLS TO XXFLSH AND TIDIED CODE UP MORE
C
C  DATE: 20/11/96	             VERSION 1.5
C  MODIFIED: DAVID H. BROOKS
C		- INCREASED FORMAT 1009 TO F13.6                        
C
C  DATE: 21/05/99	             VERSION 1.6
C  MODIFIED: HUGH SUMMERS
C		- CORRECTED EDAT,XDAT,TDAT TOP UP FROM ICT TO ISTDIM                        
C
C-----------------------------------------------------------------------
       INTEGER INDXREF , INDL  , INDU  , ICT  , ITOUT 
       INTEGER IXTYP   , IBPTS , IFPTS , IDIFF, ISTDIM
       INTEGER PIPEIN  , PIPEOU, IASEL , I    , J   
       INTEGER ICTTEMP , NTABCOL, ITOUTTEMP, IATYP   , IWHC
       INTEGER IFTYP   , IOTYP , IOUTU , IXOPS, IETYP, ILOGIC
       INTEGER I4UNIT
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80
       CHARACTER TITLE*40  , CAMETH*4 , CFORM*1
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
       REAL*8 Z , Z0 , ZEFF , EI  , EJ  , WI   , WJ
       REAL*8 ACOEFF , FIJ , EIJ , FXC2 , FXC3 , BCVAL
C-----------------------------------------------------------------------
       INTEGER IFOUT(NTABCOL)
C-----------------------------------------------------------------------
       REAL*8 XA(ISTDIM)  , YA(ISTDIM) , APOMA(ISTDIM) , DIFOMA(ISTDIM)
       REAL*8 TOA(ISTDIM) , GOA(ISTDIM), APGOA(ISTDIM) , EXCRA(ISTDIM)
       REAL*8 DEXCRA(ISTDIM) , GBARFA(ISTDIM) 
       REAL*8 EDAT(ISTDIM), XDAT(ISTDIM), TDAT(ISTDIM)
C-----------------------------------------------------------------------
       WRITE(PIPEOU,*)ISTDIM
       CALL XXFLSH(PIPEOU)
       IF(CAMETH.EQ.'   A')THEN
           IWHC = 1
       ELSE
           IWHC = 2
       ENDIF
       WRITE(PIPEOU,*)IWHC
       CALL XXFLSH(PIPEOU)
       IF(IASEL.EQ.2)THEN
           IF(CAMETH.EQ.'   A')THEN
               WRITE(PIPEOU,1000)Z
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1000)Z0
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1000)ZEFF
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)IXTYP
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1002)ACOEFF
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1003)INDL
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1003)INDU
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1004)WI
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1004)WJ
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1005)EI
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1005)EJ
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)ICT
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)ITOUT
               CALL XXFLSH(PIPEOU)
               DO 10 I = 1,ICT
                   WRITE(PIPEOU,1006)XA(I),YA(I)
                   CALL XXFLSH(PIPEOU)
C
C CONVERSION TO ALL UNITS NOW DONE IN IDL TO AVOID DUPLICATION WITH SPFMAN4E
C
 10            CONTINUE
               DO 20 J = 1,ITOUT
                   WRITE(PIPEOU,1007)TOA(J)
                   CALL XXFLSH(PIPEOU)
 20            CONTINUE
               WRITE(PIPEOU,1001)IXOPS
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)IBPTS
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)IFPTS
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)IDIFF
               CALL XXFLSH(PIPEOU)
           ELSE IF(CAMETH.EQ.'   B')THEN
               WRITE(PIPEOU,1011)Z
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1011)Z0
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)IXTYP
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1002)FIJ
               CALL XXFLSH(PIPEOU)
               CFORM = 'f'
               WRITE(PIPEOU,1008)CFORM
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1003)INDL
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1003)INDU   
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1004)WI
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1004)WJ
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1005)EI
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1005)EJ
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)ICT
               CALL XXFLSH(PIPEOU)
               WRITE(PIPEOU,1001)ITOUT
               CALL XXFLSH(PIPEOU)
               DO 25 I = 1,ICT
                   WRITE(PIPEOU,1006)XA(I),YA(I)
               CALL XXFLSH(PIPEOU)
 25            CONTINUE
               DO 30 J = 1,ITOUT
                   WRITE(PIPEOU,1007)TOA(J)
                   CALL XXFLSH(PIPEOU)
 30            CONTINUE 
               WRITE(PIPEOU,1002)BCVAL  
               CALL XXFLSH(PIPEOU)
           ENDIF
           CALL XXFLSH(PIPEOU)
       ENDIF
C
C NOW READ ALTERATIONS FROM IDL PIPE FOR ALL POSSIBLE SELECTIONS
C
       READ(PIPEIN,*)ILOGIC
       IF(ILOGIC.EQ.1)THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
       ENDIF
C
       READ(PIPEIN,1000)Z
       READ(PIPEIN,1000)Z0
       READ(PIPEIN,1001)IXTYP
       READ(PIPEIN,1007)ACOEFF
       READ(PIPEIN,1008)CFORM
       READ(PIPEIN,1003)INDL
       READ(PIPEIN,1003)INDU
       READ(PIPEIN,1004)WI
       READ(PIPEIN,1004)WJ
       READ(PIPEIN,1009)EI
       READ(PIPEIN,1009)EJ
       READ(PIPEIN,1001)ICTTEMP
       READ(PIPEIN,1001)ITOUTTEMP
C
       DO 40 I = 1,ICTTEMP
           READ(PIPEIN,'(1x,1e12.4)')EDAT(I)
           READ(PIPEIN,'(1x,1e12.4)')XDAT(I)
 40    CONTINUE
C
C FILL OUT OVERLAPS WITH ZEROS - ALTERED 14/06 TO EDAT/XDAT/TDAT ETC.
C
       DO 50 I = ICTTEMP+1,ISTDIM
           EDAT(I) = 0.0
           XDAT(I) = 0.0
 50    CONTINUE
       DO 60 I = 1,ITOUTTEMP
           READ(PIPEIN,'(1x,1e12.4)')TDAT(I)
 60    CONTINUE
       DO 70 I = ITOUTTEMP+1,ISTDIM
           TDAT(I) = 0.0
 70    CONTINUE
C
C RESTORE ICT,ITOUT
C
       ICT   = ICTTEMP
       ITOUT = ITOUTTEMP
C
C READ UNITS SELECTIONS
C
       DO 80 I = 1, NTABCOL
           READ(PIPEIN,1010)IFOUT(I)
 80    CONTINUE
C
C READ IN ENERGY UNITS AND CONVERT TO FORTRAN INDEXING
C
       READ(PIPEIN,1001)IETYP
       IETYP = IETYP+1
C
C SET UP OTHER VARIABLES
C
       ZEFF=Z+1
       EIJ=DABS(EI-EJ)
       IF(CFORM.EQ.'S')THEN
           IATYP = 3
       ELSE IF(CFORM.EQ.'f')THEN
           IATYP = 2
       ELSE
           IATYP = 1
       ENDIF
C
C UNITS ALTERED TO FORTRAN INDEXING
C
       IFTYP = IFOUT(1)+1
       IOTYP = IFOUT(2)+1
       IOUTU = IFOUT(3)+1
C-----------------------------------------------------------------------
 1000  FORMAT(1X,F6.2)
 1001  FORMAT(1X,I3)
 1002  FORMAT(1X,1PE8.2)
C REDUCED ALL 12 CHARACTER FORMATS TO  8 TO FIT WIDGETS IN IDL
 1003  FORMAT(1X,I5)
 1004  FORMAT(1X,F5.1)
 1005  FORMAT(1X,F8.6)
 1006  FORMAT(1X,1PE12.4,1X,1PE12.4)
 1007  FORMAT(1X,1PE12.4)
 1008  FORMAT(1X,1A1)
 1009  FORMAT(1X,F13.6)
 1010  FORMAT(1X,I3)
 1011  FORMAT(1X,F5.2)
C-----------------------------------------------------------------------
       RETURN
       END
