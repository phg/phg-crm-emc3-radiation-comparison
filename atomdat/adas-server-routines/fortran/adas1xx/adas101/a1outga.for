CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/a1outga.for,v 1.3 2004/07/06 09:59:27 whitefor Exp $ Date $Date: 2004/07/06 09:59:27 $
CX
       SUBROUTINE A1OUTGA( XAN   , YAN   , APOMAN, TOAN  ,
     &                     GOAN  , ICTN  , ITOUTN, INDIM ,
     &                     LPEND , LARCH , DSARCH, IASEL ,
     &                     FXC2  , FXC3  , S     , EIJ   , 
     &                     EDAT  , XDAT  , ISTDIM
     &                   )
       IMPLICIT NONE
C-------------------------------------------------------------------------
C
C  ******************* FORTRAN 77 SUBROUTINE A1OUTGA *********************
C
C
C  PURPOSE: TO SEND OUTPUT FROM SPFMAN4E ADAS ANALYSIS SUBROUTINE TO IDL
C           GRAPH EDITOR VIA THE UNIX PIPE. IF THERE IS ANY GRAPH EDITING
C           THEN THE USERS' ALTERATIONS ARE READ BACK.
C
C  CALLING PROGRAM:
C            ADAS101.FOR
C
C  INPUT:
C            (R*8)  XAN    = ENERGY (PARAMETER X) - ADAS OPTION
C            (R*8)  YAN    = OMEGA (COLLISION STRENGTH) -ADAS OPTION
C            (R*8)  APOMAN = APPROXIMATE OMEGA
C            (R*8)  TOAN   = TEMPERATURE SET
C            (R*8)  GOAN   = GAMMA (EFFECTIVE COLLISION STRENGTH)
C            (I*4)  ICT    = NUMBER OF X-SECTIONS
C            (I*4)  ITOUT  = NUMBER OF TEMPERATURES
C            (I*4)  IASEL  = THE ORIGINAL ARCHIVING OPTION (SEE A1SPF0)
C            (R*8)  FXC2   = SPLINING VARIABLE
C            (R*8)  FXC3   = SPLINING VARIABLE
C            (R*8)  S      = LINE STRENGTH
C            (R*8)  EIJ    = TRANSITION ENERGY (RYD)
C  OUTPUT:
C            (R*8)  EDAT   = USER EDITED ENERGY (PARAMETER X) 
C            (R*8)  XDAT   = USER EDITED OMEGA (COLLISION STRENGTH) 
C            (L)    LARCH  = ARCHIVING SELECTION OPTION
C            (L)    LPEND  = .F. DONE
C                            .T. CANCEL/RE-PROCESSC
C            (C*80) DSARCH = THE ARCHIVE FILE NAME
C
C  ROUTINES:
C          XXSLEN - REMOVES THE SPACES IN A STRING
C          XXFLSH - CLEARS THE PIPE BUFFER
C
C  DATE:   12/06/95				VERSION 1.1
C  AUTHOR: DAVID H.BROOKS (UNIV. OF STRATHCLYDE) EXT.4213/4205
C
C  DATE:     11/07/95				VERSION 1.2
C  MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C 		- TIDIED UP COMMENTS AND CODE
C
C  DATE:	??/05/99				VERSION: 1.3
C  MODIFIED:	HUGH SUMMERS
C
C-------------------------------------------------------------------------
       INTEGER ICTN  , ITOUTN , INDIM , PIPEIN , PIPEOU , I , I4UNIT
       INTEGER LOGIC , IARCH  , IASEL , IFIRST , ILAST  , ISTDIM 
C-------------------------------------------------------------------------
       LOGICAL LPEND , LARCH
C-------------------------------------------------------------------------
       CHARACTER DSARCH*80 , CSTRING*80
C-------------------------------------------------------------------------
       REAL*8  FXC2  , FXC3   , S   , EIJ
C-------------------------------------------------------------------------
       REAL*8 XAN(INDIM) , YAN(INDIM) , APOMAN(INDIM) 
       REAL*8 TOAN(INDIM), GOAN(INDIM), EDAT(ISTDIM)
       REAL*8 XDAT(ISTDIM)
C-------------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-------------------------------------------------------------------------
       WRITE(PIPEOU,1000) ICTN
       WRITE(PIPEOU,1000) ITOUTN
       WRITE(PIPEOU,1006) FXC2
       WRITE(PIPEOU,1006) FXC3
       WRITE(PIPEOU,1006) S
       WRITE(PIPEOU,1006) EIJ
       DO 10 I = 1, ICTN
           WRITE(PIPEOU,1001) XAN(I), YAN(I), APOMAN(I)
 10    CONTINUE
       DO 20 I = 1, ITOUTN
           WRITE(PIPEOU,1002) TOAN(I), GOAN(I)
 20    CONTINUE
       CALL XXFLSH(PIPEOU)
C
C-------------------------------------------------------------------------
C READ ALTERATIONS FROM IDL
C-------------------------------------------------------------------------
C
       READ(PIPEIN,*) ICTN
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.0) THEN
           LPEND = .FALSE.
       ELSE 
           LPEND = .TRUE. 
           IF (LOGIC.EQ.2) THEN
               DO 40 I = 1, ICTN
                   READ(PIPEIN,1003) EDAT(I), XDAT(I)
 40            CONTINUE
           ENDIF      
       ENDIF
       IF (LPEND) GO TO 999
C
C RESET LPEND
C
       LPEND = .FALSE.
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.1) THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
           READ(PIPEIN,*) IARCH
       ENDIF
       IF (IARCH.EQ.1) THEN
           LARCH = .TRUE.
       ELSE
           LARCH = .FALSE.
       ENDIF
       IF (LPEND) GO TO 999
C
C-----------------------------------------------------------------------
C IF ARCHIVING IS SELECTED AFTER INITIALLY SPECIFYING NO ARCHIVE
C THEN THE DEFAULT IS TO CREATE A FILE CALLED ARCHIVE.DAT IN THE 
C ARCH101 DIRECTORY
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
           IF (IASEL.EQ.0) THEN
               READ(PIPEIN,1004) CSTRING
           ENDIF
       ENDIF
       CALL XXSLEN( CSTRING , IFIRST , ILAST )
       READ(CSTRING(IFIRST:ILAST),1004) DSARCH
C
C-------------------------------------------------------------------------
C
 1000  FORMAT(1X,I3)
 1001  FORMAT(1X,1PE12.4,1X,1PE12.4,1X,1PE12.4)
 1002  FORMAT(1X,1PE12.4,1X,1PE12.4)
 1003  FORMAT(1X,1P,E12.4,1X,1P,E12.4)
 1004  FORMAT(1A80)
 1005  FORMAT(1A44)
 1006  FORMAT(1X,1P,E12.4)
 999   RETURN
       END
