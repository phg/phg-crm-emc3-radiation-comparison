CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/four.for,v 1.1 2004/07/06 13:55:56 whitefor Exp $ Date $Date: 2004/07/06 13:55:56 $
CX
      SUBROUTINE FOUR(A,B)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77SUBROUTINE: FOUR *********************
C
C  PURPOSE:
C        TO CALCULATE THE BURGESS KNOT POINTS
C
C  CALLING PROGRAM:
C        LSTSQ
C
C  INPUT:
C        (R*8) A - MATRIX CONTAINING REDUCED PARAMETERS
C  OUTPUT:
C        (R*8) B - THE BURGESS KNOT POINTS
C
C  ROUTINES:
C        MATIN1
C
C  DATE:   21/06/95				VERSION 1.1
C
C  AUTHOR: A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C          CONVERTED FROM BURGESS BBC BASIC
C
C----------------------------------------------------------------------
      REAL*8 A(5,5),B(5)
      REAL*8 T1
C----------------------------------------------------------------------
      INTEGER I,J
C----------------------------------------------------------------------
      DO I=1,5
         T1=A(I,3)/6
         A(I,1)=A(I,1)-T1
         A(I,2)=A(I,2)+4.*T1
         A(I,3)=A(I,4)+4.*T1
         A(I,4)=A(I,5)-T1
      ENDDO
      DO J=1,4
         A(2,J)=A(2,J)+.5*A(3,J)
         A(3,J)=A(4,J)+.5*A(3,J)
         A(4,J)=A(5,J)
      ENDDO
      B(2)=B(2)+.5*B(3)
      B(3)=B(4)+.5*B(3)
      B(4)=B(5)
      CALL MATIN1(4,A,B)
      B(5)=B(4)
      B(4)=B(3)
      B(3)=(-B(1)+4.*(B(2)+B(4))-B(5))/6.
C----------------------------------------------------------------------
      RETURN
      END
