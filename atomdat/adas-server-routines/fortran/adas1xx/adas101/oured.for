CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas101/oured.for,v 1.1 2004/07/06 14:26:40 whitefor Exp $ Date $Date: 2004/07/06 14:26:40 $
CX
       FUNCTION OURED( KTYPE, EIJ  , EJ   , OMUP , C    )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN 77 FUNCTION: OURED *********************
C
C  PURPOSE:  TO CALCULATE THE REDUCED COLLISION STRENGTH AS A FUNCTION
C            OF Ej/Eij FOR FOUR TYPES OF TRANSITION
C
C  CALLING PROGRAM: VARIOUS ADAS101 ROUTINES
C
C  FUNCTION:
C
C  INPUT:    (R*8)    EIJ    =   TRANSITION ENERGY (Eij)
C            (R*8)    EJ     =   COLLIDING ELECTRON ENERGY AFTER 
C                                EXCITATION (Ej)
C            (R*8)    C      =   ADJUSTABLE SCALING PARAMETER
C            (R*8)    ETR     =  Ej/Eij
C            (R*8)    OMUP   =   COLLISION STRENGTH AS A FUNCTION 
C                                OF ETR
C            (I)      KTYPE  =   TRANSITION TYPE
C                                1 ELECTRIC DIPOLE
C                                2 NON ELECTRIC DIPOLE
C                                3 SPIN CHANGE
C                                4 OTHER
C
C  OUTPUT:   (R*8)    OURED  =   REDUCED COLLISION STRENGTH
C
C 
C  ROUTINES: NONE
C
C  DATE:     21/06/95				VERSION 1.1
C  AUTHOR:   A.LANZAFAME (UNIV. OF STRATHCLYDE) & D.H.BROOKS
C
C-----------------------------------------------------------------------
       INTEGER KTYPE
C-----------------------------------------------------------------------
       REAL*8 EIJ  , EJ   , C    , ETR  ,OMUP , OURED 
C-----------------------------------------------------------------------
       ETR = DABS(EJ/EIJ)
C
       IF (KTYPE.EQ.1) OURED = OMUP/DLOG(ETR+2.718281828)
       IF (KTYPE.EQ.2) OURED = OMUP
       IF (KTYPE.EQ.3) OURED = OMUP*(ETR+1)*(ETR+1) 
       IF (KTYPE.EQ.4) OURED = OMUP/DLOG(ETR+C)
C-----------------------------------------------------------------------
       RETURN
       END
