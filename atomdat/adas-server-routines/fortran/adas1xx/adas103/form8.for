CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/form8.for,v 1.1 2004/07/06 13:55:52 whitefor Exp $ Date $Date: 2004/07/06 13:55:52 $
CX
       FUNCTION FORM8(I,XI)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C IDL-UNIX CONVERSION:
C
C  VERSION: 1.1                                        DATE: 01/10/96
C  MODIFIED:  WILLIAM OSBORN
C           - FIRST WRITTEN. NO CHANGES.
C
C-----------------------------------------------------------------------
       X=XI/(1.0D0-XI)
       GO TO (1,2,9,10,1,2,9,10,5,6,9,10),I
    1  FORM8=1.0D0
       GO TO 15
    2  FORM8=X*EEI(X)
       GO TO 15
    5  FORM8=X*X*EE2(X)
       GO TO 15
    6  FORM8=X*X*X*EE3(X)
       GO TO 15
    9  FORM8=1.0D0
       GO TO 15
   10  FORM8=0.0D0
   15  RETURN
      END
