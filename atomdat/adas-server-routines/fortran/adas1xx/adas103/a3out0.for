CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/a3out0.for,v 1.2 2004/07/06 10:01:36 whitefor Exp $ Date $Date: 2004/07/06 10:01:36 $
CX
      SUBROUTINE A3OUT0(  DSFULL , INDXREF, IXTIT , Z0    , Z    ,
     &     Z1    , N0    , V0    , PHFRAC , NGROUP, IGROUP, NIA  , LIA,
     &     ICT   , NJA   , LJA   , IXOPT  , IBPOPT, IFSEL,IBPTS , ICOUT,
     &     EMEAN , EDISPO, SCALEO, EDISP  , SCALE , WIA   ,
     &     WJA   , EIJA  , FIJA  , CORFIA , CORFA ,
     &     XA    , YA    , XSAOLD, YSAOLD , YSAOLP,
     &     XSANEW, APGA  , APGAP , YSANEW , YSANWP, APGOAP,
     &     SCEXP , XOA   , YOA   , APGOA  , NCUTA , ITYPEA,
     &     LARCH , IASEL , TEXDSN, ITEXOUT, CHEADER, DATE
     &     )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A3OUT0*******************
C
C  PURPOSE: 
C        TO WRITE DATA TO ANC OLD/NEW ARCHIVE IN THE FORMAT DECIDED
C        BY THE ANALYSIS OPTION.
C
C  CALLING PROGRAM:
C        ADAS103.FOR
C
C  DATA:  
C     IANOPT SET TO 2, THE BURGESS ANALYSIS OPTION, AT PRESENT
C
C  INPUT:
C     (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C     (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C     (C*40) IXTIT    - TITLE FOR THIS ARCHIVE ENTRY
C     (R*8)  Z0     = NUCLEAR CHARGE OF ION         
C     (R*8)  Z      = INITIAL ION CHARGE                  
C     (R*8)  Z1     = FINAL ION CHARGE
C     (R*8)  Z0     = NUCLEAR CHARGE
C     (R*8)  Z1     = RECOMBINING ION CHARGE
C     (R*8)  Z      = RECOMBINED ION CHARGE
C     (I*4)  N0     = LOWEST ACCESSIBLE PRINCIPLE QUANTUM NO.
C     (R*8)  V0     = LOWEST ACCESSIBLE EFF. PRINCIPLE QUANTUM NO.
C     (R*8)  PHFRAC = LOWEST ACCESSIBLE PHASE OCCUPATION FACTOR
C     (I*4)  NGROUP = NUMBER OF CORE TRANSITION GROUPS
C     (I*4)  IGROUP()=NUMBER OF ENTRIES FOR EACH GROUP
C     (I*4)  NIA(,) = NI VALUES FOR BOTH GROUPS
C     (I*4)  LIA(,) = LI VALUES FOR BOTH GROUPS
C     (I*4)  NJA(,) = NJ VALUES FOR BOTH GROUPS
C     (I*4)  LJA(,) = LJ VALUES FOR BOTH GROUPS
C     (I*4)  NCUTA(,)= NCUT VALUES FOR BOTH GROUPS
C     (R*8)  EMEAN()= MEAN ENERGY VALUES FOR EACH GROUP
C     (R*8)  EDISPO()=INITIAL ENERGY DISPLACEMENT FOR EACH GROUP
C     (R*8)  SCALEO()=INITIAL SCALE FACTOR FOR EACH GROUP
C     (R*8)  EDISP()= FINAL ENERGY DISPLACEMENT FOR EACH GROUP
C     (R*8)  SCALE()= FINAL SCALE FACTOR FOR EACH GROUP
C     (R*8)  WIA(,) = WI VALUES FOR BOTH GROUPS
C     (R*8)  EIJA(,)= EIJ VALUES FOR BOTH GROUPS
C     (R*8)  FIJA(,)= FIJ VALUES FOR BOTH GROUPS
C     (R*8)  CORFIA(,)=INITIAL CORFAC VALUES FOR BOTH GROUPS
C     (R*8)  CORFA(,)= FINAL CORFAC VALUES FOR BOTH GROUPS
C     (R*8)  XA()   = INPUT TEMPERATURES
C     (R*8)  YA()   = INPUT ALF
C     (R*8)  XSAOLD()=INITIAL X VALUES
C     (R*8)  YSAOLD()=INITIAL ALF/ALFG VALUES
C     (R*8)  YSAOLP()=INITIAL ALF/ALFP VALUES
C     (R*8)  APGA() = INITIAL ALFG
C     (R*8)  APGAP()= INITIAL ALFP
C     (R*8)  YSANEW()=FINAL ALF/ALFG VALUES
C     (R*8)  YSANEWP()=FINAL ALF/ALFP VALUES
C     (R*8)  APGOA()= FINAL ALFG
C     (R*8)  APGOAP()=FINAL ALFP
C     (R*8)  SCEXP  = PARAMETER DETERMINING X-SCALE EXPANSION
C     (R*8)  XOA()  = USER OR ARCHIVE ENTERED OUTPUT TEMPS
C     (R*8)  YOA()  = INTERPOLATED ALF VALUES CALCULATED AT XOA TEMPS.
C     (R*8)  ITYPEA(,)= TYPE OF TRANSITION (USED TO CALCULATE CORRECTION
C                                           FACTORS)
C     (I*4)  IBPOPT = OPTIMISE BURGESS FORMULA FIT? 1=YES 0=NO
C     (I*4)  IXOPT  = OPTIMISE BURGESS PROGRAM FIT? 1=YES 0=NO
C     (I*4)  IFSEL  = 0=FIT TO FORMULA, 1=FIT TO INPUT DATA
C     (I*4)  IBPTS  = BAD POINT OPTION 1=ON 0=OFF
C     (I*4)  ICT    = NUMBER OF TEMP./RATE PAIRS
C     (I*4)  ICOUT  = NUMBER OF OUTPUT TEMPS
C            (L*4)  LARCH  = ARCHIVING SELECTION OPTION
C            (I*4)  IASEL  = THE ARCHIVING CHOICE (SEE A5SPF0.FOR)
C            (C*80) TEXDSN = THE DEFAULT HARD COPY FILE NAME
C            (I*4)  ITEXOUT= THE HARD COPY OF FILE OUTPUT FLAG
C                            0 - NO, 1 - YES
C            (C*80) CHEADER= ADAS HEADER FOR PAPER.TXT
C
C  ROUTINES:
C
C  AUTHOR: 
C        WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C  DATE: 01/11/96
C
C  VERSION 1.1                                       DATE 01/11/96
C  MODIFIED: WILLIAM OSBORN
C		- FIRST RELEASE
C
C  VERSION 1.2					     DATE 08-04-97
C  MODIFIED: RICHARD MARTIN
C		REMOVED STRING 'TITLE' WHICH WAS UNUSED. 
C
C-----------------------------------------------------------------------
      INTEGER IWRITE
      PARAMETER( IWRITE=17)
      INTEGER IASEL , N0, NGROUP, IGROUP(2), NIA(2,6), LIA(2,6), ICT
      INTEGER NJA(2,6), LJA(2,6), IXOPT, IBPOPT, IBPTS, ICOUT, ITEXOUT
      INTEGER INDXREF, IFSEL, NCUTA(2,6), ITYPEA(2,6)
      INTEGER IHFLAG , ICNT , INDX , IANOPT, IG, I, IC, IASELCOPY
C-----------------------------------------------------------------------
      REAL*8  Z0, Z, Z1, V0, PHFRAC, EMEAN(2), EDISPO(2), SCALEO(2)
      REAL*8  EDISP(2), SCALE(2), WIA(2,6), WJA(2,6), EIJA(2,6)
      REAL*8  FIJA(2,6), CORFIA(2,6), CORFA(2,6), XA(10), YA(10)
      REAL*8  XSAOLD(10), YSAOLD(10), YSAOLP(10), XSANEW(10)
      REAL*8  APGA(10), APGAP(10), YSANEW(10), YSANWP(10), SCEXP
      REAL*8  XOA(10), YOA(10), APGOA(10), APGOAP(10)
C-----------------------------------------------------------------------
      CHARACTER DSFULL*80, CSTRING*80, CINDX*3, CIARR(500)*80, CODE*4
      CHARACTER DCODE(2)*4, TEXDSN*80, CHEADER*80, CSTRNG*32
      CHARACTER IXTIT*40, DATE*8
C-----------------------------------------------------------------------
      LOGICAL LARCH
C-----------------------------------------------------------------------
      DATA DCODE/'   A','   B'/, IANOPT/2/
C-----------------------------------------------------------------------
C DEPENDING ON THE OPTIONS, CHECK THE FILE
C-----------------------------------------------------------------------

      IF (.NOT.LARCH) THEN
         IHFLAG = 1
      ELSE
         IHFLAG = 0
      ENDIF
      IF (IHFLAG.EQ.0) THEN
         OPEN( UNIT=17 , FILE = DSFULL , STATUS = 'UNKNOWN' )
         IF (IASEL.NE.3) THEN
C     
C-----------------------------------------------------------------------
C     CHECK FOR HIGHEST INDEX
C-----------------------------------------------------------------------
C     
 1          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:1).NE.' '.AND.CSTRING(1:1).NE.'-') THEN
               IF (CSTRING(1:1).NE.'C') THEN
                  CINDX = CSTRING(2:4)
                  GO TO 1
               ELSE
                  CINDX = CINDX
               ENDIF
            ELSE
               GO TO 1
            ENDIF
C     
C-----------------------------------------------------------------------
C     COPY INDEX COMMENTS TO TEMPORARY ARRAY
C-----------------------------------------------------------------------
C     
            CIARR(1) = CSTRING
            ICNT = 2
 2          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:2).EQ.'C ') THEN
               CIARR(ICNT) = CSTRING
               ICNT = ICNT+1
               GO TO 2
            ELSE
               CIARR(ICNT) = CSTRING
               ICNT = ICNT+1
            ENDIF
C     
C-----------------------------------------------------------------------
C     CLOSE FILE OFF TO RE-OPEN FOR POSITION FINDING
C-----------------------------------------------------------------------
C     
            CLOSE( UNIT=IWRITE , STATUS = 'KEEP')
            OPEN( UNIT=IWRITE , FILE = DSFULL , STATUS = 'UNKNOWN' )
 3          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:1).EQ.'I') THEN
               IF (CSTRING(2:4).NE.CINDX) THEN
                  GO TO 3
               ENDIF
            ELSE
               GO TO 3
            ENDIF
C     
 4          READ(IWRITE,1015) CSTRING
            IF (CSTRING(1:1).NE.'-') THEN
               GO TO 4
            ENDIF
C     
C     CLOCK INDEX UP ONE
C     
            READ(CINDX,1016) INDX
            INDX = INDX+1
         ELSE
            INDX = 1
        ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C     NOW WRITE OUT DATA 
C-----------------------------------------------------------------------
C     
      IF (IANOPT.EQ.1) THEN
         CODE = DCODE(1)
      ELSE
         CODE = DCODE(2)
      ENDIF 
 100  IF (IANOPT.EQ.1) THEN
C
C     NO ADAS OPTION
C
      ELSE
         IF (IHFLAG.EQ.1) THEN
            OPEN(UNIT = IWRITE,FILE = TEXDSN, STATUS = 'UNKNOWN')
            WRITE(IWRITE,*) CHEADER
         ENDIF
         WRITE(CSTRNG,'(1A32)')IXTIT
         WRITE(IWRITE,1028) INDX, CSTRNG, DATE, CODE

 245     WRITE(17,246)Z0,Z1,Z,N0,V0,PHFRAC
  246  FORMAT(/ // / ,' NUCLEAR CHARGE =',F6.2,'   RECOMBINING I
     &ON CHARGE =',F6.2,'  RECOMBINED ION CHARGE =',F6.2/
     & / ,'  N0 =',I5,10X,'  V0 =',F6.3,10X,'PHFRAC =',F6.3)
 268   DO 253 IG=1,NGROUP
          WRITE(17,254)IG,EMEAN(IG),EDISPO(IG),SCALEO(IG),EDISP(IG),
     &SCALE(IG)
          DO 252 I=1,IGROUP(IG)
       WRITE(17,255)NIA(IG,I),LIA(IG,I),NJA(IG,I),LJA(IG,I),WIA(IG,I),
     &WJA(IG,I),EIJA(IG,I),FIJA(IG,I),CORFIA(IG,I),CORFA(IG,I),
     &NCUTA(IG,I),ITYPEA(IG,I)
 252      CONTINUE
 253   CONTINUE
  254  FORMAT( / ,' GROUP ',I2,5X,'EMEAN =',F10.5,5X,'EDISP =',F10.5,
     &5X,'SCALE =',F10.5/1H ,42X,F10.5,12X,F10.5
     &/ / ,' NI',3X,'LI',3X,'NJ',3X,'LJ',6X,'WI',8X,'WJ',10X,
     &'EIJ',7X,'FIJ',5X,'CORFAC(I)',4X,'CORFAC(F)',5X,'NCUT',5X,
     &'ITYPE')
  255  FORMAT(1H ,I2,3X,I2,3X,I2,3X,I2,3X,F5.0,5X,F5.0,5X,F10.5,F10.5,
     &2X,F10.5,3X,F10.5,5X,I5,3X,I5)
       WRITE(17,266)
  266  FORMAT( / ,'                          <--------INITIAL VALUES----
     &------>  <-------------FINAL ADJUSTED VALUES----------------------
     &>')
       WRITE(17,257)
  257  FORMAT(1H ,'   TE(K)        ALF            X       ALF/ALFG    AL
     &F/ALFP        X         ALFG        ALFP      ALF/ALFG    ALF/ALFP
     &')
       DO 258 IC=1,ICT
  258  WRITE(17,259)XA(IC),YA(IC),XSAOLD(IC),YSAOLD(IC),YSAOLP(IC),
     &XSANEW(IC),APGA(IC),APGAP(IC),YSANEW(IC),YSANWP(IC)
  259  FORMAT(1H ,1P,10D12.4)
       WRITE(17,256)IXOPT,IBPOPT,IFSEL,IBPTS,SCEXP
  256  FORMAT( / ,' IXOPT =',I3,5X,'IBPOPT =',I3,5X,'IBFSEL =',I3,5X,
     & 'IBPTS =',I3,5X,'SCEXP =',F6.3)
       WRITE(17,263)
  263  FORMAT( / ,'   TE(K)        ALF          ALFG          ALFGP ')
       DO 264 IC=1,ICOUT
  264  WRITE(17,265)XOA(IC),YOA(IC),APGOA(IC),APGOAP(IC)
  265  FORMAT(1H ,1P,4D12.4)

      ENDIF
C-----------------------------------------------------------------------
C     WRITE STUFF BACK AT THE END
C-----------------------------------------------------------------------
      IF (IHFLAG.EQ.0) THEN
         IF (IASEL.EQ.3) THEN
            WRITE(IWRITE,1013)
         ELSE
            WRITE(IWRITE,1017)
            DO 30 I = 1, ICNT-2
               WRITE(IWRITE,1015) CIARR(I)
 30         CONTINUE
         ENDIF
C     
         WRITE(IWRITE,1014) INDX, CODE, CSTRNG(1:6), 
     &        CSTRNG(7:22), CSTRNG(23:32),
     &        DATE
      ENDIF
      CLOSE( UNIT=IWRITE , STATUS = 'KEEP')
C     
C     GO BACK TO WRITE HARD COPY IF REQUESTED & SET UP OPTION FLAG
C     
      IF (IHFLAG.EQ.0) THEN
         IF (ITEXOUT.EQ.1) THEN
            IHFLAG = 1
            IASELCOPY = IASEL
            IASEL = 3
            GOTO 100 
         ENDIF
      ENDIF
      IASEL = IASELCOPY
      
C     IZ  = NINT(Z)
C     IZ1 = NINT(Z1)
C     WRITE(IWRITE,1050) IZ, IZ1, ITOUT, EMIN*109737.3
C     WRITE(IWRITE,1051) ( TOA(IT)/TEV , IT =1,ITOUT )
C     WRITE(IWRITE,1051) ( YOA(IT)     , IT =1,ITOUT )
      
      RETURN

C-----------------------------------------------------------------------
 1013 FORMAT('-1  '/
     & 'C-----------------------------------------------------------',
     & '------------'/
     & 'C INDEX  CODE  INFORMATION                                  ',
     & 'DATE       ')
 1014  FORMAT('C ',I3,2X,1A4,3X,1A6,1X,1A16,1X,1A10,10X,1A8,6X/
     & 'C-----------------------------------------------------------',
     & '------------')
 1015 FORMAT(1A80)
 1016 FORMAT(I3)
 1017 FORMAT('-1        ')
 1028 FORMAT('I',I3,2X, 1A32, 1A8, 24X, 1A4)
 1029 FORMAT(' NUCLEAR CHARGE = ',F5.1,3X,'INITIAL ION CHARGE = ',
     &     F5.1,3X,'FINAL ION CHARGE = ',F5.1)                         
 1030 FORMAT(' DIRECT IONISATION   NIGRP =',I2,3X,'EMIN =',F10.5)   
 1031 FORMAT(5X,'GROUP',I2,3X,'CIA =',F8.3,8X,'EION(RYD)   ZETA')  
 1032 FORMAT(1H ,35X,2I3,F10.5,I5)                                         
 1033 FORMAT(' EXCIT/AUTOIONISATION  NRGRP =',I2)                   
 1034 FORMAT(5X,'GROUP',I2,3X,'CRA =',F8.3,8X,'EIJ (RYD)   WGHT')  
 1035 FORMAT(1H ,35X,F10.5,F7.3)                                       
 1036 FORMAT('     X        Q(PI*A0**2)     Q/(1-1/X)     QEM/(1-1/X)  
     &    Q/QEM')                                                  
 1037 FORMAT(1H ,F10.5,1P,4D15.5)                                      
 1038 FORMAT('    TE(K)       TE(EV)        S          SEM')       
 1039 FORMAT(1H ,1P,4D12.4)                                            
 1050 FORMAT('  +',I2,'/  +',I2,'/',I5,'/I.P. =',7X,F12.1,
     &            '/ICODE =  1/FCODE =  1/ISEL=   1')
 1051 FORMAT(1H ,1P,6D10.3)
C

      END
