CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/a3outg.for,v 1.1 2004/07/06 10:01:39 whitefor Exp $ Date $Date: 2004/07/06 10:01:39 $
CX
       SUBROUTINE A3OUTG( XPA    , YPA    , XP    , YP   , ICOUT , ICT ,
     &        TEA4  , EXCRA , EMEAN4, SCEXP4, NGROUP, IGROUP, EDISP4,
     &        SCALE4, NIA   , LIA   , NJA   , LJA, EIJA4, FIJA4, CORFA4,
     &        LPEND , LARCH , DSARCH, IASEL , ASCL, APGA, APGAP,
     &        EDAT  , XDAT  , IIFTYP, IIOTYP
     &     )
       IMPLICIT NONE
C-------------------------------------------------------------------------
C
C  ******************* FORTRAN 77 SUBROUTINE A3OUTG **********************
C
C
C  PURPOSE: TO SEND OUTPUT FROM SPFMAN8HX ADAS ANALYSIS SUBROUTINE TO IDL
C           GRAPH EDITOR VIA THE UNIX PIPE. IF THERE IS ANY GRAPH EDITING
C           THEN THE USERS' ALTERATIONS ARE READ BACK AND THE DATA IS
C           RECALCULATED.
C
C  CALLING PROGRAM:
C            ADAS105.FOR
C
C  INPUT:
C     (R*4)  XPA(,) = X VALUES FOR RATIO PLOT
C     (R*4)  YPA(,) = Y VALUES FOR RATIO PLOT (RATIOS OF DIEL. RECOM. COEFFS.)
C     (I*4)  ICT    = NUMBER OF POINTS ON RATIO PLOT
C     (R*4)  XP()   = X VALUES FOR RATIO PLOT SPLINE FIT
C     (R*4)  YP()   = X VALUES FOR RATIO PLOT SPLINE FIT
C     (I*4)  ICOUT  = NUMBER OF POINTS ON COEFFICIENT PLOT
C     (R*4)  TEA4() = X VALUES FOR COEFFICIENT PLOT (TEMP.) 
C     (R*4)  EXCRA(,) = Y VALUES FOR COEFFICIENT PLOT (COEFFS.)
C     (R*4)  EMEAN4 = MEAN EIJ USED IN GRAPH ANNOTATION
C     (R*4)  SCEXP4 = SCALE FACTOR USED IN GRAPH ANNOTATION
C     (R*4)  EIJA4  = VALUE OF EIJ USED IN GRAPH ANNOTATION
C     (R*4)  FIJA4  = VALUE OF FIJ USED IN GRAPH ANNOTATION
C     (R*4)  CORFA4 = VALUE OF CORFA USED IN GRAPH ANNOTATION
C     (R*4)  EDISP4 = VALUE OF DISPLACEMENT USED IN GRAPH ANNOTATION
C     (R*4)  SCALE4 = VALUE OF SCALE USED IN GRAPH ANNOTATION
C     (I*4)  NGROUP = NUMBER OF CORE TRANSITION GROUPS
C     (I*4)  IGROUP()=NUMBER OF ENTRIES FOR EACH GROUP
C     (I*4)  NIA(,) = NI VALUES FOR BOTH GROUPS
C     (I*4)  LIA(,) = LI VALUES FOR BOTH GROUPS
C     (I*4)  NJA(,) = NJ VALUES FOR BOTH GROUPS
C     (I*4)  LJA(,) = LJ VALUES FOR BOTH GROUPS
C     (R*8)  APGA() = INITIAL ALFG
C     (R*8)  APGAP()= INITIAL ALFP
C     (R*8)  ASCL   = GRAPH SCALING PARAMETER
C  OUTPUT:
C            (L)    LARCH  = ARCHIVING SELECTION OPTION
C            (L)    LPEND  = .F. DONE
C                            .T. CANCEL/RE-PROCESSC
C            (C*80) DSARCH = THE ARCHIVE FILE NAME
C            (I*4)  IFTYP  = ENERGY UNITS - SEE ADAS103
C            (I*4)  IOTYP  = X-SECTION UNITS - SEE ADAS103
C            (R*8)  EDAT() = INPUT TEMPERATURE (CHANGED IN GRAPH EDITOR)
C            (R*8)  XDAT() = RATE (CHANGED IN GRAPH EDITOR)
C            (I*4)  IIOTYP = INPUT RATE FORM, 1=DIELECTRONIC
C                                             2=DIEL. + RADIATIVE
C            (I*4)  IIFTYP = INPUT TEMP. FORM, 1= KELVIN
C                                              2= EV
C                                              3= SCALED (K/Z1**2)
C
C  ROUTINES:
C          XXSLEN - REMOVES THE SPACES IN A STRING
C          XXFLSH - CLEARS THE PIPE BUFFER
C
C  DATE:   23/09/96				VERSION 1.1
C  AUTHOR: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C 		- FIRST WRITTEN
C
C-----------------------------------------------------------------------
       REAL*8 ASCL, EDAT(10), XDAT(10), APGA(10), APGAP(10), YPAIN
       REAL*8 XSAIN
C-----------------------------------------------------------------------
       REAL*4 XP(10),YP(10),XPA(4,10),YPA(4,10)
       REAL*4 TEA4(10), EXCRA(10,15), EMEAN4, SCEXP4
       REAL*4 EDISP4(2), SCALE4(2), EIJA4(2,6),FIJA4(2,6),CORFA4(2,6)
C-----------------------------------------------------------------------
       LOGICAL LPEND , LARCH
C-----------------------------------------------------------------------
       CHARACTER DSARCH*80 , CSTRING*80
C-----------------------------------------------------------------------
       INTEGER PIPEIN, PIPEOU, ICT, ICOUT, IASEL, LOGIC, IARCH
       INTEGER NGROUP, IGROUP(2), NIA(2,6),LIA(2,6),NJA(2,6),LJA(2,6)
       INTEGER IFIRST, ILAST , I , J , SEL, IIFTYP, IIOTYP
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------

       WRITE(PIPEOU,*) ASCL
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*) ICT
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*) ICOUT
       CALL XXFLSH(PIPEOU)
       DO 10 I = 1, ICT
          DO 11 J = 1, 4
             WRITE(PIPEOU,*) XPA(J,I)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*) YPA(J,I)
             CALL XXFLSH(PIPEOU)
 11       CONTINUE
 10    CONTINUE
       DO 20 I = 1, ICOUT
           WRITE(PIPEOU,*) XP(I)
           CALL XXFLSH(PIPEOU)
           WRITE(PIPEOU,*) YP(I)
           CALL XXFLSH(PIPEOU)
 20    CONTINUE
       DO 30 I = 1, ICOUT
           WRITE(PIPEOU,*) TEA4(I)
           CALL XXFLSH(PIPEOU)
           DO 31 J=1,3
              WRITE(PIPEOU,*) EXCRA(I,J)
              CALL XXFLSH(PIPEOU)
 31        CONTINUE
 30    CONTINUE

C WRITE OUT INFO FOR MESSAGES

       WRITE(PIPEOU,*)EMEAN4
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)SCEXP4
       CALL XXFLSH(PIPEOU)

       WRITE(PIPEOU,*)NGROUP
       CALL XXFLSH(PIPEOU)
       DO 40 I=1,NGROUP
          WRITE(PIPEOU,*)IGROUP(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*)EDISP4(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*)SCALE4(I)
          CALL XXFLSH(PIPEOU)
          DO 41 J=1,IGROUP(I)
             WRITE(PIPEOU,*)NIA(I,J)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)LIA(I,J)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)NJA(I,J)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)LJA(I,J)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)EIJA4(I,J)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)FIJA4(I,J)
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)CORFA4(I,J)
             CALL XXFLSH(PIPEOU)
 41       CONTINUE
 40    CONTINUE
C
C-------------------------------------------------------------------------
C READ ALTERATIONS FROM IDL
C-------------------------------------------------------------------------
C
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.0) THEN
           LPEND = .FALSE.
       ELSE
          LPEND = .TRUE. 
          IF (LOGIC.EQ.2) THEN
             READ(PIPEIN,*)SEL
             READ(PIPEIN,*)ICT
             DO 50 I = 1, ICT
                READ(PIPEIN,*) XSAIN
                READ(PIPEIN,*) YPAIN
C-------------------------------------------------------------------------
C     CALCULATE EDAT AND XDAT
C-------------------------------------------------------------------------
                EDAT(I)=1.57890D5*SCEXP4*EMEAN4/XSAIN
                IF(SEL.EQ.2)THEN
                   XDAT(I)=(10**YPAIN)*APGA(I)
                ELSE
                   XDAT(I)=(10**YPAIN)*APGAP(I)
                ENDIF      
 50          CONTINUE
             IIFTYP=1
             IIOTYP=1
          ENDIF
       ENDIF
         
       IF (LPEND) GO TO 999
C
C RESET LPEND
C
       LPEND = .FALSE.
       READ(PIPEIN,*) LOGIC
       IF (LOGIC.EQ.1) THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
           READ(PIPEIN,*) IARCH
       ENDIF
       IF (IARCH.EQ.1) THEN
           LARCH = .TRUE.
       ELSE
           LARCH = .FALSE.
       ENDIF
       IF (LPEND) GO TO 999
C
C-----------------------------------------------------------------------
C IF ARCHIVING IS SELECTED AFTER INITIALLY SPECIFYING NO ARCHIVE
C THEN THE DEFAULT IS TO CREATE A FILE CALLED ARCHIVE.DAT IN THE 
C ARCH105 DIRECTORY
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
           IF (IASEL.EQ.0) THEN
               READ(PIPEIN,'(A)') CSTRING
           ENDIF
       ENDIF
       CALL XXSLEN( CSTRING , IFIRST , ILAST )
       READ(CSTRING(IFIRST:ILAST),1004) DSARCH
C
C-------------------------------------------------------------------------
C
 1004  FORMAT(1A80)
 1005  FORMAT('CIA = ',2E15.5)
 1006  FORMAT('CRA = ',2E15.5)
 1007  FORMAT('A = ',1E15.5)
 999   RETURN
       END
