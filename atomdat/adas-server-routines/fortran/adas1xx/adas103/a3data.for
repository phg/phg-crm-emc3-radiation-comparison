CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/a3data.for,v 1.1 2004/07/06 10:01:23 whitefor Exp $ Date $Date: 2004/07/06 10:01:23 $
CX
      SUBROUTINE A3DATA( DSFULL , INDXREF , TITLE , CAMETH , Z0    , Z ,
     &                   Z1     , N0, V0, PHFRAC,
     &                   IXOPT, IBPOPT, IFSEL, IBPTS,
     &                   EDISPO, SCALEO,
     &                   NIA, LIA, NJA, LJA, NCUTA, WIA, WJA, EIJA,FIJA,
     &                   CORFIA,
     &                   XA, YA, XOA,
     &                   NGROUP, IGROUP, ICT, ICOUT,
     &                   IREAD
     &     )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A3DATA *******************
C
C  PURPOSE: TO REFRESH A DATA INDEX FROM AN ADAS105 ARCHIVE. READS
C           IN THE INDEX CODE A-ADAS, B-BURGESS AND THE THE REST OF
C           THE DATA AS APPROPRIATE.
C
C  CALLING PROGRAM: 
C            ADAS105.FOR
C
C  INPUT:
C            (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C            (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C            (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                              TWO TYPES OF ANALYSIS.
C                              A - ADAS, B- BURGESS
C            (I*4)  IREAD  = THE INPUT UNIT
C 
C  OUTPUTS: 
C            (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                              FILE.
C     (R*8)  Z0    NUCLEAR CHARGE
C     (R*8)  Z1    RECOMBINING ION CHARGE
C     (R*8)  Z     RECOMBINED ION CHARGE
C     (I*4)  N0     = LOWEST ACCESSIBLE PRINCIPLE QUANTUM NO.
C     (R*8)  V0     = LOWEST ACCESSIBLE EFF. PRINCIPLE QUANTUM NO.
C     (R*8)  PHFRAC = LOWEST ACCESSIBLE PHASE OCCUPATION FACTOR
C     (C*40) TITLE  = TITLE FOR RUN
C     (I*4)  ICT    = NUMBER OF TEMP./RATE PAIRS
C     (I*4)  ICOUT  = NUMBER OF OUTPUT TEMPS
C     (I*4)  IBPOPT = OPTIMISE BURGESS FORMULA FIT? 1=YES 0=NO
C     (I*4)  IXOPT  = OPTIMISE BURGESS PROGRAM FIT? 1=YES 0=NO
C     (I*4)  IFSEL  = 0=FIT TO FORMULA, 1=FIT TO INPUT DATA
C     (I*4)  IBPTS  = BAD POINT OPTION 1=ON 0=OFF
C     (I*4)  NIA(,) = NI VALUES FOR BOTH GROUPS
C     (I*4)  LIA(,) = LI VALUES FOR BOTH GROUPS
C     (I*4)  NJA(,) = NJ VALUES FOR BOTH GROUPS
C     (I*4)  LJA(,) = LJ VALUES FOR BOTH GROUPS
C     (I*4)  NCUTA(,)= NCUT VALUES FOR BOTH GROUPS
C     (R*8)  WIA(,) = WI VALUES FOR BOTH GROUPS
C     (R*8)  EIJA(,)= EIJ VALUES FOR BOTH GROUPS
C     (R*8)  FIJA(,)= FIJ VALUES FOR BOTH GROUPS
C     (R*8)  CORFIA(,)=INITIAL CORFAC VALUES FOR BOTH GROUPS
C     (R*8)  XA()   = INPUT TEMPERATURE FROM ARCHIVE
C     (R*8)  YA()   = RATE FROM ARCHIVE
C     (R*8)  XOA()  = OUTPUT TEMPERATURE FROM ARCHIVE
C     (I*4)  NGROUP = NUMBER OF CORE TRANSITION GROUPS
C     (I*4)  IGROUP()=NUMBER OF ENTRIES FOR EACH GROUP
C
C  ROUTINES: NONE
C
C  AUTHOR:   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC., 6TH NOV 1996
C
C  VERSION 1.1                                           DATE: 06-11-96
C  MODIFIED: WILLIAM OSBORN
C		- FIRST RELEASE
C
C-----------------------------------------------------------------------
      INTEGER INDXREF
      INTEGER ICT, ICOUT, IG, IC , I4UNIT, N0
      INTEGER IREAD, ICHK1, I, IFIRST, ILAST
      INTEGER NIA(2,6),LIA(2,6),NJA(2,6),LJA(2,6),NCUTA(2,6),ITYPEA(2,6)
      INTEGER IXOPT,IBPOPT,IFSEL,IBPTS, IGROUP(2), NGROUP
C-----------------------------------------------------------------------
      CHARACTER DSFULL*80 , CSTRING*120
      CHARACTER TITLE*40  , CAMETH*4
C-----------------------------------------------------------------------
      REAL*8 Z0, Z1, Z, V0, PHFRAC, EMEAN(2),EDISPO(2),SCALEO(2)
      REAL*8 EDISP(2),SCALE(2), WIA(2,6),WJA(2,6),EIJA(2,6),FIJA(2,6)
      REAL*8 CORFIA(2,6),CORFA(2,6),SCEXP
      REAL*8 XA(10),YA(10),XSAOLD(10),YSAOLD(10),YSAOLP(10),XSANEW(10)
      REAL*8 APGA(10),APGAP(10),YSANEW(10),YSANWP(10)
      REAL*8 XOA(10),YOA(10),APGOA(10),APGOAP(10)
C-----------------------------------------------------------------------
      
      OPEN( UNIT=IREAD , FILE = DSFULL , STATUS = 'UNKNOWN' )
      
C-----------------------------------------------------------------------
C     READ THE INFORMATION STRING WITH INDEX NUMBER AT THE TOP OF EACH RECORD
C-----------------------------------------------------------------------
      READ(IREAD,1000)CSTRING
 1    IF(CSTRING(1:1).EQ.'I')THEN
         READ(CSTRING,1001)ICHK1
         IF(ICHK1.NE.INDXREF) THEN
            READ(IREAD,1000)CSTRING
            GOTO 1
         ENDIF
      ELSE
         IF(CSTRING(1:1).EQ.'C')THEN
        WRITE(I4UNIT(-1),*)'**************** A3DATA MESSAGE **********'
        WRITE(I4UNIT(-1),'(A,I2)')' NOT FOUND ARCHIVE NUMBER ',INDXREF
        WRITE(I4UNIT(-1),'(A,I2)')' USING LAST ARCHIVE, NUMBER',ICHK1
        WRITE(I4UNIT(-1),*)'**************** A3DATA MESSAGE **********'
            INDXREF = ICHK1
C CLOSE FILE AND START AGAIN
            CLOSE(UNIT=IREAD)
            OPEN( UNIT=IREAD , FILE = DSFULL , STATUS = 'UNKNOWN' )
         ENDIF
         READ(IREAD,1000)CSTRING
         GO TO 1
      ENDIF
C     
      READ(CSTRING,1002)ICHK1,TITLE,CAMETH 
      
      IF(CAMETH.EQ.'   B')THEN
C-----------------------------------------------------------------------
C     READ IN BURGESS FORMAT DATA - NO OTHER FORMAT ACCEPTED YET
C-----------------------------------------------------------------------
         READ(IREAD,1000)CSTRING
         READ(IREAD,1000)CSTRING
         READ(IREAD,'(1X,A40)')TITLE
         READ(IREAD,1000)CSTRING
         READ(IREAD,1000)CSTRING
 245     READ(IREAD,246)Z0,Z1,Z,N0,V0,PHFRAC
 246     FORMAT(17X,F6.2,27X,F6.2,25X,F6.2,//,6X,I5,16X,F6.3,18X,F6.3)
         IG=0
         READ(IREAD,1000)CSTRING
 400     CONTINUE
         READ(IREAD,1000)CSTRING
         IF(CSTRING(2:6).EQ.'GROUP')THEN
            IG=IG+1
            READ(CSTRING,254)IG,EMEAN(IG),EDISPO(IG),SCALEO(IG)
            READ(IREAD,1000)CSTRING
            READ(CSTRING,270)EDISP(IG),SCALE(IG)
            READ(IREAD,1000)CSTRING
            READ(IREAD,1000)CSTRING
            I=0
 299        CONTINUE
            READ(IREAD,1000)CSTRING
            CALL XXSLEN(CSTRING,IFIRST,ILAST)
            IF(ILAST.NE.IFIRST)THEN
               I=I+1
              READ(CSTRING,255)NIA(IG,I),LIA(IG,I),NJA(IG,I),LJA(IG,I),
     &           WIA(IG,I),WJA(IG,I),EIJA(IG,I),FIJA(IG,I),CORFIA(IG,I)
     &           ,CORFA(IG,I),NCUTA(IG,I),ITYPEA(IG,I)
              GOTO 299
            ENDIF
            IGROUP(IG)=I
  254  FORMAT(7X,I2,12X,F10.5,12X,F10.5,12X,F10.5)
 270   FORMAT(43X,F10.5, 12X,F10.5)
  255  FORMAT(1X,I2,3X,I2,3X,I2,3X,I2,3X,F5.0,5X,F5.0,5X,F10.5,F10.5,
     &2X,F10.5,3X,F10.5,5X,I5,3X,I5)
            GOTO 400
         ENDIF
         NGROUP=IG
         READ(IREAD,1000)CSTRING
         IC=0
 300     CONTINUE
         READ(IREAD,1000)CSTRING
         CALL XXSLEN(CSTRING,IFIRST,ILAST)
         IF(ILAST.NE.IFIRST)THEN
            IC=IC+1
 258        READ(CSTRING,259)XA(IC),YA(IC),XSAOLD(IC),YSAOLD(IC),
     &           YSAOLP(IC),XSANEW(IC),APGA(IC),APGAP(IC),
     &              YSANEW(IC),YSANWP(IC)
 259        FORMAT(2X,10D12.4)
            GOTO 300
         ENDIF
         ICT = IC
         READ(IREAD,256)IXOPT,IBPOPT,IFSEL,IBPTS,SCEXP
 256     FORMAT(8X,I3,5X,8X,I3,13X,I3,12X,I3,12X,F6.3)
         READ(IREAD,1000)CSTRING
         READ(IREAD,1000)CSTRING
         IC=0
 301     CONTINUE
         READ(IREAD,1000)CSTRING
         IF(CSTRING(1:2).NE.'-1')THEN
            IC=IC+1
 264        READ(CSTRING,265)XOA(IC),YOA(IC),APGOA(IC),APGOAP(IC)
 265        FORMAT(2X,4D12.4)
            GOTO 301
         ENDIF
         ICOUT = IC
      ENDIF

      CLOSE( UNIT=IREAD , STATUS = 'KEEP')
      
      RETURN

C-----------------------------------------------------------------------
 1000 FORMAT(1A120)
 1001 FORMAT(1X,I3,76X)
 1002 FORMAT(1X,1I3,2X,1A40,24X,1A4)
 1003 FORMAT(//1A40//)
C-----------------------------------------------------------------------

      END
