CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/adas103.for,v 1.1 2004/07/06 10:09:05 whitefor Exp $ Date $Date: 2004/07/06 10:09:05 $
CX
       PROGRAM ADAS103
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN 77 PROGRAM: ADAS103 *********************
C
C  PURPOSE: COLLECTION FROM ARCHIVE OR ENTRY OF DIELECTRONIC
C           RECOMBINATION COEFFICIENTS FOR EXAMINATION, INTERPOLATION, 
C           COMPARISON WITH APPROXIMATE FORMS, AND GRAPH EDITING VIA
C           COMMUNICATION WITH IDL INTERFACE.
C
C  DATA:
C           DATA IS ENTERED VIA THE IDL INTERFACE, BUT CAN ALSO BE 
C           RECOVERED FROM THE USERS' OWN ARCHIVE FILES. THESE 
C           TYPICALLY RESIDE IN '/DISK2/YOURNAME/ADAS/ARCH103'.
C           PARTICULAR TRANSITION DATASETS HAVE THEIR OWN INDEX
C           NUMBER AND THIS IS ENTERED ON THE FIRST PANEL AFTER
C           SELECTING 'REFRESH FROM OLD ARCHIVE'. THERE IS NO
C           CENTRAL ADAS ARCHIVE.
C  
C           ARCHIVE FILES FOLLOW THE FOLLOWING NAMING CONVENTION;
C           <PRODUCER ID><YEAR><MEMBER>_<FILE NUMBER>.DAT
C
C           THE MEMBER NAME IS CONSTRUCTED AS;
C           <ELEMENT><SPECTROSCOPIC EFFECTIVE ION CHARGE>
C
C           E.G.        'DHB95FE19_001.DAT'
C           DHB = DAVID H.BROOKS
C           95  = 1995
C           FE  = IRON
C           19  = Z+1      I.E. FE+18 IS THE ION IN THIS CASE
C           001 = FILE NUMBER
C  PROGRAM:
C          (I*4)  INDXREF= THE INDEX NUMBER TO REFRESH DATA FROM.
C          (I*4)  IGRPSCAL1 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             IST GRAPH.
C          (I*4)  IGRPSCAL2 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             2NT GRAPH.
C          (I*4)  ITEXOUT= HARD COPY OF TEXT OUTPUT REQUEST SWITCH.
C                          0 - NO
C                          1 - YES
C          (C*80) DSFULL = THE SELECTED ARCHIVE NAME.
C          (C*3)  REP    = 'YES' - FINISH PROGRAM RUN
C                          'NO ' - CONTINUE
C          (C*40) TITLE  = THE INFORMATION LINE IN THE ARCHIVE
C                          FILE.
C          (C*4)  CAMETH = THE TAG TO DISTINGUISH BETWEEN THE
C                          TWO TYPES OF ANALYSIS.
C                          A - ADAS, B- BURGESS
C          (C*8)  DATE   = THE CURRENT DATA 
C          (C*80) CHEADER= THE ADAS HEADER FOR THE PAPER.TXT FILE
C          (C*80) TEXDSN = THE DEFAULT HARD COPY FILE NAME
C          (C*80) DSARCH = THE ARCHIVE FILE NAME
C          (C*40) CGTIT  = THE GRAPH TITLE (INFORMATION FOR ANY
C                          NEW ARCHIVE)
C            (R*8)  Z0     = NUCLEAR CHARGE OF ION         
C            (R*8)  Z      = INITIAL ION CHARGE                  
C            (R*8)  Z1     = FINAL ION CHARGE
C          (L)    LPEND  = CANCEL OR DONE FLAG
C          (L)    LARCH  = ARCHIVING SELECTION OPTION
C          (R*8)  EDAT   = USER SELECTED INPUT ENRGIES
C          (R*8)  XDAT   = USER SELECTED INPUT OMEGAS
C          (R*8)  TDAT   = USER SELECTED OUTPUT TEMPERATURES
C          (I*4)  ISTOP  = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END TO
C                          THE PROGRAM
C          (I*4)  IASEL  = ARCHIVING OPTION:
C                          0 = IGNORE ARCHIVING.
C                          1 = EXAMINE OLD ARCHIVE AND POSSIBLY
C                              ADD NEW DATA.
C                          2 = REFRESH TRANSITION DATA FROM 
C                              OLD ARCHIVE (REQUIRES INDEX NO.)
C                          3 = CREATE A NEW ARCHIVE.
C  ROUTINES: (COMPLETE LIST FOR ADAS103 - NOT NECESSARILY 
C             CALLED BY THIS PROGRAM)
C           NAME             BRIEF DESCRIPTION
C           -------------------------------------------------------
C           A3SPF0    GATHERS INPUT ARCHIVE NAME VIA IDL PIPE
C           A3DATA    READS SELECTED DATA FROM ARCHIVE
C           A3ISPF    PIPES DATA TO IDL READS USER PROCESSING EDITS 
C           A3SPF1    GATHERS USER SELECTED OUTPUT OPTIONS
C           SPFMAN8HX PERFORMS ADAS PROCESSING (GAMMA EVALUATION)
C           A3OUTG    SENDS DATA TO IDL GRAPH EDITOR
C           A3OUT0    WRITES DATA TO ARCHIVE/TEXT OUTPUT FILES
C           XX0000    SETS MACHINE DEPENDENT ADAS CONFIGURATION
C           XXDATE    PROVIDES CURRENT DATE
C           XXSLEN    REMOVES SPACES FROM A STRING
C           I4UNIT    FETCHES MESSAGE OUTPUT UNIT NUMBER
C           XXFLSH    CLEARS IDL PIPE BUFFER
C
C  AUTHOR:  WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C  DATE:    1ST NOVEMBER 1996
C
C  MODIFICATION HISTORY:
C
C  VERSION: 1.1                                        DATE: 01/10/96
C  MODIFIED:  WILLIAM OSBORN
C           - FIRST WRITTEN USING ADAS105.FOR AS A TEMPLATE
C
C-----------------------------------------------------------------------
       INTEGER ICT      , ICOUT
       INTEGER IASEL    , INDXREF   , IUNT17
       INTEGER IGRPSCAL1, IGRPSCAL2, ITEXOUT
       INTEGER PIPEIN   , ISTOP , IREAD
       INTEGER N0, IIOTYP, IIFTYP, IIFOUT, IXMAX, ITMAX, INGRUP, IIATYP
       INTEGER IGRP(2), NIGRP1(6), LIGRP1(6), NJGRP1(6), LJGRP1(6)
       INTEGER NIGRP2(6), LIGRP2(6), NJGRP2(6), LJGRP2(6)
       INTEGER NCGRP1(6), NCGRP2(6)
       INTEGER IGF, IBU, IFSEL, IBT, IGH
       INTEGER NGROUP, IGROUP(2), NIA(2,6), LIA(2,6), NJA(2,6)
       INTEGER LJA(2,6),NCUTA(2,6),ITYPEA(2,6)
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80 , CAMETH*4
       CHARACTER TITLE*40  , DATE*8     , REP*3
       CHARACTER CGTIT*40  , CHEADER*80 , TEXDSN*80
       CHARACTER IXTIT*40  , DSARCH*80
C-----------------------------------------------------------------------
       PARAMETER( IUNT17 = 17 , PIPEIN = 5, IREAD = 9)
C       PARAMETER( ISTDIM = 40, INDIM = 40)
C-----------------------------------------------------------------------
       REAL*8  Z , Z0 , Z1 , V0 , PHFRAC
       REAL*8  EDAT(10), XDAT(10), TDAT(10), EDGRP(2), SCGRP(2)
       REAL*8  WIGRP1(6), WJGRP1(6), EGRP1(6), AGRP1(6), CRGRP1(6)
       REAL*8  WIGRP2(6), WJGRP2(6), EGRP2(6), AGRP2(6), CRGRP2(6)
       REAL*8  ASCL
       REAL*8  EMEAN(2),EDISPO(2),SCALEO(2),EDISP(2), SCALE(2),WIA(2,6)
       REAL*8  WJA(2,6), EIJA(2,6), FIJA(2,6), CORFIA(2,6), CORFA(2,6)
       REAL*8  XA(10), YA(10), XSAOLD(10), YSAOLD(10) , YSAOLP(10)
       REAL*8  XSANEW(10), APGA(10), APGAP(10), YSANEW(10), YSANWP(10)
       REAL*8  SCEXP , XOA(10), YOA(10), APGOA(10), APGOAP(10)
C-----------------------------------------------------------------------
       REAL*4  XP(10), YP(10), XPA(4,10), YPA(4,10), TEA4(10)
       REAL*4  EXCRA(10,15), EMEAN4, SCEXP4
       REAL*4  EDISP4(2), SCALE4(2), EIJA4(2,6),FIJA4(2,6),CORFA4(2,6)
C-----------------------------------------------------------------------
       LOGICAL OPEN17 , LARCH , LPEND
C-----------------------------------------------------------------------
C       DATA OPEN17/.FALSE./
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
       CGTIT = ' '
       TITLE = ' '
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
       CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
       CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
  100  CONTINUE
C-----------------------------------------------------------------------
C  IF FILES ARE ACTIVE ON UNIT 17 - CLOSE IT
C-----------------------------------------------------------------------
       IF (OPEN17) CLOSE(17)
       OPEN17=.FALSE.
C
C-----------------------------------------------------------------------
C GET ARCHIVE NAME FROM IDL
C-----------------------------------------------------------------------
C
       CALL A3SPF0( REP     , DSFULL  ,
     &              IASEL   , INDXREF )
C
C-----------------------------------------------------------------------
C  IF PROGRAM RUN COMPLETED END  
C-----------------------------------------------------------------------
C
       IF (REP.EQ.'YES') THEN
           GOTO 999
       ENDIF

C       CALL CHECK_PIPE
C
C-----------------------------------------------------------------------
C OPEN ARCHIVE AND READ IF REFRESHING
C-----------------------------------------------------------------------
C
       IF (IASEL.EQ.2) THEN
          CALL A3DATA( DSFULL , INDXREF , TITLE , CAMETH , Z0    , Z ,
     &         Z1     , N0, V0, PHFRAC,
     &         IBU, IGF, IFSEL, IBT,
     &         EDGRP, SCGRP,
     &         NIA, LIA, NJA, LJA, NCUTA, WIA, WJA, EIJA, FIJA,
     &         CORFIA,
     &         EDAT, XDAT, TDAT,
     &         INGRUP, IGRP, IXMAX, ITMAX,
     &         IREAD
     &         )
       ENDIF
C
C-----------------------------------------------------------------------
C
  200  CONTINUE
C
C-----------------------------------------------------------------------
C READ USER PROCESSING OPTIONS FROM IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
       CALL A3ISPF( DSFULL , INDXREF, CAMETH , Z0    ,
     &              Z      , Z1     , N0    , V0     , PHFRAC, TITLE,
     &              IIOTYP , IIFTYP , IIFOUT, IXMAX  , ITMAX , EDAT ,
     &              XDAT   , TDAT   , INGRUP, IIATYP , IGRP  , EDGRP,
     &              SCGRP  , NIGRP1 , LIGRP1, WIGRP1 , NJGRP1, LJGRP1,
     &              WJGRP1 , EGRP1  , AGRP1 , CRGRP1 , NIGRP2, LIGRP2,
     &              WIGRP2 , NJGRP2 , LJGRP2, WJGRP2 , EGRP2 , AGRP2,
     &              CRGRP2 , NCGRP1 , NCGRP2,
     &              NIA, LIA, NJA, LJA, NCUTA, WIA, WJA, EIJA, FIJA,
     &              CORFIA,
     &              IASEL  , LPEND
     &            )
C       CALL CHECK_PIPE
C     
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 999
C
C-----------------------------------------------------------------------
C
       IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C READ USER OUTPUT OPTIONS FROM IDL VIA UNIX PIPE  
C-----------------------------------------------------------------------
C
 300   CONTINUE
C
C-----------------------------------------------------------------------
C
       CALL A3SPF1( LPEND   , IBU , IGF , IFSEL , IBT , IGH , ASCL ,
     &              IGRPSCAL1, IGRPSCAL2, ITEXOUT   ,
     &              CGTIT   , CHEADER   , TEXDSN
     &             )
C       CALL CHECK_PIPE

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 999
C
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 200
C
C-----------------------------------------------------------------------
C PROCESS ANALYSIS OPTION 
C-----------------------------------------------------------------------

      CALL SPFMAN8HX(Z0,Z1,Z,N0,V0,PHFRAC,TITLE,
     &     IIOTYP,IIFTYP,IIFOUT,IXMAX,ITMAX,EDAT,XDAT,TDAT,
     &     INGRUP,IIATYP,IGRP,EDGRP,SCGRP,
     &     NIGRP1,LIGRP1,WIGRP1,NJGRP1,LJGRP1,WJGRP1,EGRP1,AGRP1,CRGRP1,
     &     NIGRP2,LIGRP2,WIGRP2,NJGRP2,LJGRP2,WJGRP2,EGRP2,AGRP2,CRGRP2,
     &     IGF,IBU,IFSEL,IBT,IGH,ASCL,NCGRP1,NCGRP2,
     &     XPA,YPA,ICT,XP,YP,ICOUT,
     &     TEA4,EXCRA,
     &     NGROUP, IGROUP,NIA,LIA,
     &     NJA   , LJA   , NCUTA ,
     &     EMEAN , EDISPO, SCALEO, EDISP  , SCALE , WIA   ,
     &     WJA   , EIJA  , FIJA  , CORFIA , CORFA ,
     &     XA    , YA    , XSAOLD, YSAOLD , YSAOLP,
     &     XSANEW, APGA  , APGAP , YSANEW , YSANWP, APGOAP,
     &     SCEXP , XOA   , YOA   , APGOA  , ITYPEA,
     &     EMEAN4, SCEXP4,
     &     EIJA4 , FIJA4 , CORFA4, EDISP4 , SCALE4)

C-----------------------------------------------------------------------
C SEND GRAPHICAL INFO TO IDL AND RECALCULATE VALUES IF GRAPH HAS BEEN
C EDITED
C-----------------------------------------------------------------------

      IF (IGH.EQ.1)THEN
         CALL A3OUTG( XPA    , YPA    , XP    , YP   , ICOUT , ICT ,
     &        TEA4 , EXCRA , EMEAN4, SCEXP4, NGROUP, IGROUP, EDISP4,
     &        SCALE4, NIA   , LIA   , NJA   , LJA, EIJA4, FIJA4, CORFA4,
     &        LPEND , LARCH , DSARCH, IASEL , ASCL, APGA, APGAP,
     &        EDAT  , XDAT  , IIFTYP, IIOTYP
     &        )
C         CALL CHECK_PIPE
     
C-----------------------------------------------------------------------
C     READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C     
         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 999
      ENDIF 
      
C     
C-----------------------------------------------------------------------
C     
      IF (LPEND) GOTO 300
C     
C-----------------------------------------------------------------------
C     IF NO ARCHIVING AND COMPLETE, GO BACK TO OUTPUT OPTIONS
C-----------------------------------------------------------------------
C     
      IF (.NOT.LARCH.AND.ITEXOUT.NE.1)THEN
         GOTO 300
      ENDIF
C
C-----------------------------------------------------------------------
C COPY DSARCH TO DSFULL IF ARCHIVING SELECTED AFTER PICKING NO ARCHIVE
C-----------------------------------------------------------------------
C
       IF (LARCH) THEN
         IF (IASEL.EQ.0)THEN
           DSFULL = DSARCH
           IASEL = 3
         ENDIF
       ENDIF
C
C-----------------------------------------------------------------------
C PUT DATA IN ARCHIVE FILE
C-----------------------------------------------------------------------
C
       TITLE = CGTIT
       IF (IASEL.NE.0)THEN
           IF (IASEL.EQ.2)THEN
              IXTIT = TITLE
           ELSE
              IXTIT = CGTIT
           ENDIF
           CALL A3OUT0( DSFULL, INDXREF, IXTIT , Z0    , Z   ,
     &          Z1    , N0    , V0    , PHFRAC , NGROUP, IGROUP,NIA,LIA,
     &          ICT   , NJA   , LJA   , IBU    , IGF,IFSEL,IBT , ICOUT,
     &          EMEAN , EDISPO, SCALEO, EDISP  , SCALE , WIA   ,
     &          WJA   , EIJA  , FIJA  , CORFIA , CORFA ,
     &          XA    , YA    , XSAOLD, YSAOLD , YSAOLP,
     &          XSANEW, APGA  , APGAP , YSANEW , YSANWP, APGOAP,
     &          SCEXP , XOA   , YOA   , APGOA  , NCUTA , ITYPEA,
     &          LARCH , IASEL , TEXDSN, ITEXOUT, CHEADER, DATE
     &     )
       ENDIF
C
C-----------------------------------------------------------------------
C

C-----------------------------------------------------------------------
C GO BACK TO OUTPUT OPTIONS SCREEN
C-----------------------------------------------------------------------

      GOTO 300

 999   STOP
       END
