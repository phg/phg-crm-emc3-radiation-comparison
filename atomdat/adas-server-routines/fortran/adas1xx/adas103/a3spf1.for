CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/a3spf1.for,v 1.1 2004/07/06 10:01:48 whitefor Exp $ Date $Date: 2004/07/06 10:01:48 $
CX
       SUBROUTINE A3SPF1( LPEND   , IBU, IGF, IFSEL, IBT, IGH, ASCL,
     &                    IGRPSCAL1, IGRPSCAL2, ITEXOUT   ,
     &                    CGTIT   , CHEADER   , TEXDSN
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN 77 SUBROUTINE: A3SPF1  *******************
C
C  CALLING PROGRAM: 
C          ADAS103.FOR
C
C  PURPOSE:
C          TO COMMUNICATE WITH IDL OUTPUT SELECTION INTERFACE 
C          A3SPF1.PRO VIA THE UNIX PIPE.
C
C  INPUT:
C          NONE
C  OUTPUT:
C          (L)    LPEND     = CANCEL OR DONE.
C          (I*4)  IGRPSCAL1 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             IST GRAPH.
C          (I*4)  IGRPSCAL2 = DEFAULT(0) OR USER SCALING(1) SWITCH FOR  
C                             2NT GRAPH.
C          (I*4)  ITEXOUT   = HARD COPY OF TEXT OUTPUT REQUEST SWITCH.
C                             0 - NO, 1 - YES
C          (C*40) CGTIT     = THE GRAPH TITLE (INFORMATION FOR ANY
C                             NEW ARCHIVE).
C          (C*80) CHEADER   = THE ADAS HEADER FOR THE DEFAULT FILE.
C          (C*80) TEXDSN    = THE DEFAULT ARCHIVE FILE NAME.
C          (I*4)  IGF    = OPTIMISE BURGESS FORMULA FIT? 1=YES 0=NO
C          (I*4)  IBU    = OPTIMISE BURGESS PROGRAM FIT? 1=YES 0=NO
C          (I*4)  IFSEL  = 0=FIT TO FORMULA, 1=FIT TO INPUT DATA
C          (I*4)  IBT    = BAD POINT OPTION 1=ON 0=OFF
C          (I*4)  IGH    = GRAPHICAL DISPLAY? 1=YES 0=NO
C          (R*8)  ASCL   = GRAPH SCALING PARAMETER
C
C ROUTINES:
C----------
C          XXSLEN - REMOVES SPACES FROM A STRING
C
C  AUTHOR: WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC, 13TH SEPTEMBER 1996
C  DATE:   1/10/96
C
C  VERSION 1.1                                          DATE:   01/10/96
C  MODIFIED: WILLIAM OSBORN
C            - FIRST RELEASE
C
C-----------------------------------------------------------------------
       INTEGER  IFIRST  , ILAST , IBU, IGF, IFSEL, IBT, IGH
       INTEGER  IGRPSCAL1, IGRPSCAL2, ITEXOUT , PIPEIN  , PIPEOU
       INTEGER  IPEND
C-----------------------------------------------------------------------
       CHARACTER CGTIT*40 , CHEADER*80 , TEXDSN*80
       CHARACTER CTEXDSN*80
C-----------------------------------------------------------------------
       REAL*8 ASCL
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------

       READ(PIPEIN,*)IPEND
       IF (IPEND.EQ.1)THEN
          LPEND = .TRUE.
       ELSE
          LPEND = .FALSE.
C
C
          READ(PIPEIN,*)IBU
          READ(PIPEIN,*)IGF
          READ(PIPEIN,*)IFSEL
          READ(PIPEIN,*)IBT
          READ(PIPEIN,*)IGH
          IF (IGH.EQ.1)THEN
             READ(PIPEIN,'(A)')CGTIT
             READ(PIPEIN,*)IGRPSCAL1
C     
             READ(PIPEIN,*)IGRPSCAL2

          ENDIF
C-----------------------------------------------------------------------
          READ(PIPEIN,*)ITEXOUT
          IF (ITEXOUT.EQ.1)THEN
             READ(PIPEIN,'(1A80)')CTEXDSN
             CALL XXSLEN( CTEXDSN , IFIRST , ILAST )
             READ(CTEXDSN(IFIRST:ILAST),'(A)') TEXDSN
C     TEXDSN = CTEXDSN
          ENDIF

          READ(PIPEIN,*)ASCL

       ENDIF
C     
       IF (IPEND.EQ.0.AND.ITEXOUT.EQ.1)THEN
         READ(PIPEIN,'(1A80)')CHEADER
       ENDIF

C-----------------------------------------------------------------------
 1000  FORMAT(1X,I3)
 1001  FORMAT(1X,E8.2)
 1002  FORMAT(1X,1A40)
 1003  FORMAT(1X,1A80)
 1004  FORMAT(1A45)
C-----------------------------------------------------------------------
       RETURN
       END
