CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/a3ispf.for,v 1.2 2004/07/06 10:01:29 whitefor Exp $ Date $Date: 2004/07/06 10:01:29 $
CX
       SUBROUTINE A3ISPF( DSFULL , INDXREF , CAMETH , Z0    ,
     &              Z      , Z1     , N0    , V0     , PHFRAC, TITLE,
     &              IIOTYP , IIFTYP , IIFOUT, IXMAX  , ITMAX , EDAT ,
     &              XDAT   , TDAT   , INGRUP, IIATYP , IGRP  , EDGRP,
     &              SCGRP  , NIGRP1 , LIGRP1, WIGRP1 , NJGRP1, LJGRP1,
     &              WJGRP1 , EGRP1  , AGRP1 , CRGRP1 , NIGRP2, LIGRP2,
     &              WIGRP2 , NJGRP2 , LJGRP2, WJGRP2 , EGRP2 , AGRP2,
     &              CRGRP2 , NCGRP1 , NCGRP2,
     &              NIA, LIA, NJA, LJA, NCUTA, WIA, WJA, EIJA, FIJA,
     &              CORFIA,
     &              IASEL  , LPEND
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 SUBROUTINE A3ISPF *******************
C
C  PURPOSE: TO READ USER ALTERATIONS/SPECIFICATIONS FROM IDL AND TO
C           COMMUNICATE OLD ARCHIVE DATA TO IDL IF REQUESTED.
C
C  CALLING PROGRAM:
C            ADAS103.FOR
C
C  INPUT:
C     (C*80) DSFULL   - THE USERS' CHOSEN ARCHIVE FILE NAME.
C     (I*4)  INDXREF  - THE INDEX NUMBER TO REFRESH FROM.
C     (C*40) TITLE    - THE INFORMATION LINE IN THE ARCHIVE
C                       FILE.
C     (C*4)  CAMETH   - THE TAG TO DISTINGUISH BETWEEN THE
C                       TWO TYPES OF ANALYSIS.
C                       A - ADAS, B- BURGESS (ONLY 'A' WORKS)
C     (I*4)  IASEL  - THE ARCHIVING CHOICE.
C                       0 - NO ARCHIVE
C                       1 - EXAMINE OLD ARCHIVE
C                       2 - REFRESH FROM ARCHIVE
C                       3 - NEW ARCHIVE
C     (I*4)  NIA(,) = NI VALUES FOR BOTH GROUPS
C     (I*4)  LIA(,) = LI VALUES FOR BOTH GROUPS
C     (I*4)  NJA(,) = NJ VALUES FOR BOTH GROUPS
C     (I*4)  LJA(,) = LJ VALUES FOR BOTH GROUPS
C     (I*4)  NCUTA(,)= NCUT VALUES FOR BOTH GROUPS
C     (R*8)  WIA(,) = WI VALUES FOR BOTH GROUPS
C     (R*8)  EIJA(,)= EIJ VALUES FOR BOTH GROUPS
C     (R*8)  FIJA(,)= FIJ VALUES FOR BOTH GROUPS
C     (R*8)  CORFIA(,)=INITIAL CORFAC VALUES FOR BOTH GROUPS
C
C  I/O:
C     (R*8)  Z0     = NUCLEAR CHARGE OF ION
C     (R*8)  Z      = ION CHARGE
C     (R*8)  Z1     = ION CHARGE + 1
C     (I*4)  N0     = LOWEST ACCESSIBLE PRINCIPLE QUANTUM NO.
C     (R*8)  V0     = LOWEST ACCESSIBLE EFF. PRINCIPLE QUANTUM NO.
C     (R*8)  PHFRAC = LOWEST ACCESSIBLE PHASE OCCUPATION FACTOR
C     (C*40) TITLE  = TITLE FOR RUN
C     (I*4)  IIOTYP = INPUT RATE FORM, 1=DIELECTRONIC
C                                      2=DIEL. + RADIATIVE
C     (I*4)  IIFTYP = INPUT TEMP. FORM, 1= KELVIN
C                                       2= EV
C                                       3= SCALED (K/Z1**2)
C     (I*4)  IIFOUT = INPUT TEMP. FORM, 1= KELVIN
C                                       2= EV
C                                       3= SCALED (K/Z1**2)
C     (I*4)  IXMAX  = NUMBER OF TEMP./RATE PAIRS
C     (I*4)  ITMAX  = NUMBER OF OUTPUT TEMPS
C     (R*8)  EDAT() = USER OR ARCHIVE ENTERED INPUT TEMPERATURE
C     (R*8)  XDAT() = USER OR ARCHIVE ENTERED RATE
C     (R*8)  TDAT() = USER OR ARCHIVE ENTERED OUTPUT TEMPERATURE
C     (I*4)  INGRUP = NO OF CORE TRANSITION GROUPS
C     (I*4)  IIATYP = TRANSITION PROB. FORM, 1= A-COEFFICIENT
C                                            2= OSCILLATOR STRENGTH
C                                            3= LINE STRENGTH
C     (I*4)  IGRP() = NO OF ENTRIES FOR EACH GROUP
C     (R*4)  EDGRP()= INITIAL MEAN ENERGY DISP. FOR EACH GROUP
C     (R*4)  SCGRP()= MEAN SCALE FACTOR FOR EACH GROUP
C
C  OUTPUT:
C     (L)    LPEND    - 'CANCEL' OR 'DONE' DECISION.
C     (I*4)  NIGRP1()= TRANSITION GROUP 1 INFO
C     (I*4)  LIGRP1()= "
C     (R*8)  WIGRP1()= "
C     (I*4)  NJGRP1()= "
C     (I*4)  LJGRP1()= "
C     (R*8)  WJGRP1()= "
C     (R*8)  EGRP1()=  "
C     (R*8)  AGRP1()=  "
C     (R*8)  CRGRP1()= "
C     (I*4)  NCGRP1()= "
C     (I*4)  NIGRP2()= TRANSITION GROUP 2 INFO
C     (I*4)  LIGRP2()= "
C     (R*8)  WIGRP2()= "
C     (I*4)  NJGRP2()= "
C     (I*4)  LJGRP2()= "
C     (R*8)  WJGRP2()= "
C     (R*8)  EGRP2()=  "
C     (R*8)  AGRP2()=  "
C     (R*8)  CRGRP2()= "
C     (I*4)  NCGRP2()= "
C
C  ROUTINES:
C         XXFLSH - CLEARS PIPE BUFFER
C
C  AUTHOR:
C         WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC., 1ST NOVEMBER 1996
C
C  VERSION 1.1                                           DATE:  01/11/96
C  MODIFIED: WILLIAM OSBORN
C               - FIRST RELEASE. MODIFIED A5ISPF.FOR
C
C  VERSION 1.2                                           DATE:  08/11/96
C  MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
       INTEGER INDXREF
C-----------------------------------------------------------------------
       CHARACTER DSFULL*80
       CHARACTER TITLE*40  , CAMETH*4
C-----------------------------------------------------------------------
       LOGICAL LPEND
C-----------------------------------------------------------------------
       INTEGER I,J, PIPEIN, PIPEOU, ILOGIC
       PARAMETER( PIPEIN = 5 , PIPEOU = 6 )
C-----------------------------------------------------------------------
       INTEGER N0, IIOTYP, IIFTYP, IIFOUT, IXMAX, ITMAX, INGRUP, IIATYP
       INTEGER IGRP(2), NIGRP1(6), LIGRP1(6), NJGRP1(6), LJGRP1(6)
       INTEGER NIGRP2(6), LIGRP2(6), NJGRP2(6), LJGRP2(6)
       INTEGER NIA(2,6), LIA(2,6), NJA(2,6), LJA(2,6), NCUTA(2,6)
       INTEGER IASEL , IWHC, ISTDIM , NCGRP1(6), NCGRP2(6)
       PARAMETER (ISTDIM=10)
C-----------------------------------------------------------------------
       REAL*8  Z, Z0, Z1 , V0 , PHFRAC
       REAL*8  EDAT(10), XDAT(10), TDAT(10), EDGRP(2), SCGRP(2)
       REAL*8  WIGRP1(6), WJGRP1(6), EGRP1(6), AGRP1(6), CRGRP1(6)
       REAL*8  WIGRP2(6), WJGRP2(6), EGRP2(6), AGRP2(6), CRGRP2(6)
       REAL*8  WIA(2,6) , WJA(2,6) , EIJA(2,6), FIJA(2,6), CORFIA(2,6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

       WRITE(PIPEOU,*)ISTDIM
       CALL XXFLSH(PIPEOU)
       IF(CAMETH.EQ.'   A')THEN
           IWHC = 1
       ELSE
           IWHC = 2
       ENDIF
       WRITE(PIPEOU,*)IWHC
       CALL XXFLSH(PIPEOU)
       IF(IASEL.EQ.2)THEN
          IF(CAMETH.EQ.'   B')THEN
             WRITE(PIPEOU,*)Z
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)Z0
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)Z1
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)N0
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)V0
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)PHFRAC
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)INGRUP
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)IXMAX
             CALL XXFLSH(PIPEOU)
             DO 20 I=1,IXMAX
                WRITE(PIPEOU,*)EDAT(I)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)XDAT(I)
                CALL XXFLSH(PIPEOU)
 20          CONTINUE
             WRITE(PIPEOU,*)ITMAX
             CALL XXFLSH(PIPEOU)
             DO 21 I=1,ITMAX
                WRITE(PIPEOU,*)TDAT(I)
                CALL XXFLSH(PIPEOU)
 21          CONTINUE
             DO 22 I=1,INGRUP
                WRITE(PIPEOU,*)EDGRP(I)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)SCGRP(I)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)IGRP(I)
                DO 23 J=1,IGRP(I)
                   WRITE(PIPEOU,*)NIA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)LIA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)WIA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)NJA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)LJA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)WJA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)EIJA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)FIJA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)CORFIA(I,J)
                   CALL XXFLSH(PIPEOU)
                   WRITE(PIPEOU,*)NCUTA(I,J)
                   CALL XXFLSH(PIPEOU)
 23             CONTINUE
 22          CONTINUE
          ELSE IF(CAMETH.EQ.'   A')THEN
C-----------------------------------------------------------------------
C     NO ADAS OPTION
C-----------------------------------------------------------------------
          ENDIF
       ENDIF
C
C NOW READ ALTERATIONS FROM IDL PIPE FOR ALL POSSIBLE SELECTIONS
C
       READ(PIPEIN,*)ILOGIC
       IF(ILOGIC.EQ.1)THEN
           LPEND = .TRUE.
       ELSE
           LPEND = .FALSE.
       ENDIF
C
       READ(PIPEIN,*)Z       
       READ(PIPEIN,*)Z0
       READ(PIPEIN,*)Z1
       READ(PIPEIN,*)N0
       READ(PIPEIN,*)V0
       READ(PIPEIN,*)PHFRAC
       READ(PIPEIN,*)INGRUP
       DO 30 I=1,INGRUP
          READ(PIPEIN,*)EDGRP(I)
          READ(PIPEIN,*)SCGRP(I)
          READ(PIPEIN,*)IGRP(I)
 30    CONTINUE
       DO 31 I=1,INGRUP
          DO 32 J=1,IGRP(I)
             IF(I.EQ.1)THEN
                READ(PIPEIN,*)NIGRP1(J)
                READ(PIPEIN,*)LIGRP1(J)
                READ(PIPEIN,*)WIGRP1(J)
                READ(PIPEIN,*)NJGRP1(J)
                READ(PIPEIN,*)LJGRP1(J)
                READ(PIPEIN,*)WJGRP1(J)
                READ(PIPEIN,*)EGRP1(J)
                READ(PIPEIN,*)AGRP1(J)
                READ(PIPEIN,*)CRGRP1(J)
                READ(PIPEIN,*)NCGRP1(J)
             ELSE
                READ(PIPEIN,*)NIGRP2(J)
                READ(PIPEIN,*)LIGRP2(J)
                READ(PIPEIN,*)WIGRP2(J)
                READ(PIPEIN,*)NJGRP2(J)
                READ(PIPEIN,*)LJGRP2(J)
                READ(PIPEIN,*)WJGRP2(J)
                READ(PIPEIN,*)EGRP2(J)
                READ(PIPEIN,*)AGRP2(J)
                READ(PIPEIN,*)CRGRP2(J)
                READ(PIPEIN,*)NCGRP2(J)
             ENDIF
 32       CONTINUE
 31    CONTINUE
       READ(PIPEIN,*)IIFTYP
       READ(PIPEIN,*)IIOTYP
       READ(PIPEIN,*)IIFOUT
       READ(PIPEIN,*)IXMAX
       READ(PIPEIN,*)ITMAX
       DO 33 I=1,IXMAX
          READ(PIPEIN,*)EDAT(I)
          READ(PIPEIN,*)XDAT(I)
 33    CONTINUE
       DO 34 I=1,ITMAX
          READ(PIPEIN,*)TDAT(I)
 34    CONTINUE

       RETURN
       END
