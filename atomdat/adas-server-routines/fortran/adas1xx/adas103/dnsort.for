CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/dnsort.for,v 1.2 2007/05/15 11:18:07 allan Exp $ Date $Date: 2007/05/15 11:18:07 $
CX
       SUBROUTINE DNSORT(XA,YA,N)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: TO SORT AN ARRAY SO THAT FIRST INPUT IS IN INCREASING ORDER
C
C  N.B.  INPUT VALUES ARE ALTERED BY THIS ROUTINE !!!!
C
C  INPUT
C      XA(I)=X-VALUES
C      YA(I)=Y-VALUES
C      N=NUMBER OF VALUES
C  OUTPUT
C      XA(I)=SORTED X-VALUES
C      YA(I)=SORTED Y-VALUES
C
C
C  *********                                             **************
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 07-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 15-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine
C                 documentation production.
C
C-----------------------------------------------------------------------
       DIMENSION XA(10),YA(10)
       N1=N-1
       DO 10 I=1,N1
       I1=I+1
       DO 5 J=I1,N
       IF(XA(I).LE.XA(J))GO TO 5
       SWAP=XA(I)
       XA(I)=XA(J)
       XA(J)=SWAP
       SWAP=YA(I)
       YA(I)=YA(J)
       YA(J)=SWAP
    5  CONTINUE
   10  CONTINUE
       RETURN
      END
