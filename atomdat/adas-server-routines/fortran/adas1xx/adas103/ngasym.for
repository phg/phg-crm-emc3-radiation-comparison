CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas1xx/adas103/ngasym.for,v 1.2 2007/05/15 11:18:00 allan Exp $ Date $Date: 2007/05/15 11:18:00 $
CX
       SUBROUTINE NGASYM(X,DX,FORM,IFORMS,IENDS)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: INITIALISES COMMON ARRAYS REQUIRED FOR SPLINING WITH
C  SMOOTH FITTING TO AN ASYMPTOTIC FORM
C
C
C
C  USES LABELLED COMMON /SPL3/
C
C  IF IENDS=1,MATCHING IS AT FIRST KNOT(GIVEN BY X)
C          =2,MATCHING IS AT  LAST KNOT(GIVEN BY X)
C  ASYMPTOTIC FORMS ARE GIVEN IN THE EXTERNAL FUNCTION FORM(I,X)
C  WHERE I=4*IFORMS-5+2*IENDS POINTS TO FIRST PART OF ASYMP. FORM
C         =4*IFORMS-4+2*IENDS POINTS TO SECOND PART OF ASYMP. FORM
C  THUS A SERIES OF ASYMPTOTIC FORMS MAY BE PRESENT IN FORM
C
C  INPUT
C      COMMON /SPL3/ PROVIDES INPUT IN VECTOR IEND WHICH SPECIFIES
C                     CHOICE OF END CONDITION AT FIRST IEND(1) OR LAST
C                     IEND(2) KNOT OF SPLINE
C      X=X-VALUE OF END POINT
C      DX=DISPLACEMENT FROM X-VALUE FOR DERIVATIVE EVALUATION
C      FORM=EXTERNAL FUNCTION SPECIFYING ASYMPTOTIC FORMS
C      IFORMS=SELECTED FORM
C      IENDS=1,MATCHING IS AT FIRST KNOT(GIVEN BY X)
C           =2,MATCHING IS AT  LAST KNOT(GIVEN BY X)
C  OUTPUT
C      COMMON /SPL3/ IS SET BY THIS ROUTINE
C
C
C  *************                                           *************
C-----------------------------------------------------------------------
C IDL-UNIX CONVERSION:
C
C  VERSION: 1.1                                        DATE: 01/10/96
C  MODIFIED:  WILLIAM OSBORN
C           - FIRST WRITTEN. NO CHANGES.
C
C  VERSION: 1.2                                        DATE: 15/05/07
C  MODIFIED:  Allan Whiteford
C           - Updated comments as part of subroutine
C             documentation production.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       COMMON /SPL3/IEND(2),G(2),AB(4),PQ(12),ABRY(40)
       IF(IENDS.EQ.1.AND.IEND(1).EQ.4)GO TO 5
       IF(IENDS.EQ.2.AND.IEND(2).EQ.4)GO TO 5
    3  RETURN
    5  I=4*IFORMS-5+2*IENDS
       J=6*IENDS-5
       IC=0
       DX1=1.0D0/DX
   10  PQ(J)=FORM(I,X)
       T1=FORM(I,X+DX)
       T2=FORM(I,X-DX)
       PQ(J+1)=0.5D0*DX1*(T1-T2)
       PQ(J+2)=DX1*DX1*(T1-2.0D0*PQ(J)+T2)
       IC=IC+1
       IF(IC.GT.1)RETURN
       I=I+1
       J=J+3
       GO TO 10
      END
