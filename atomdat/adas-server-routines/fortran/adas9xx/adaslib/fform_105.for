      SUBROUTINE FFORM_105( IOUTPUT, IPAR, IENER, DE,
     &                      W      , PAR , ELE  , FFORM
     &                    )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: FFORM_105**************
C
C  PURPOSE: Calculate fitting formula, 105 for an array of energies
C
C  INPUT: (I*4) IOUTPUT  = TYPE OF OUTPUT: 
C                             0 : OMEGA
C                             1 : SIGMAS (cm2)
C  INPUT: (I*4) IPAR     = NUMBER OF PARAMETERS
C  INPUT: (I*4) IENER    = NUMBER OF ENERGIES (X)
C  INPUT: (R*8) DE       = ENERGY DIFFERENCE OR TRANSITION ENERGY
C  INPUT: (R*8) W        = STATISTICAL WEIGTH OF INITIAL STATE
C  INPUT: (R*8) PAR()    = ARRAY OF FITTING PARAMETERS
C                            DIM : FITTING PARAMETERS (MAX 50)
C  INPUT: (R*8) ELE()    = ARRAY OF IMPACT ENERGIES (X)
C                            DIM : ENERGIES (MAX 200)
C
C  OUTPUT:(R*8) FFORM()  = ARRAY OF OMGS OR SIGMAS
C
C   -Formula for ionization from ground state
C   -Formula 5.23 in my Janev version
C   - omg definition of Shemansky et al. ApJ. 296, 765 (1985) there are others 
C     definitions with a factor of PI in the denominator
C
C  THE ENERGY IMPUT IN THIS FORMULA IS IN KEV/AMU --. OUTPUT TO EV/AMU
C
C  AUTHOR: F. GUZMAN
C  DATE  : 5/07/2010
C
C  VERSION : 1.1
C  DATE    : 05-07-2010
C  MODIFIED: F Guzman
C              - First version
C
C-----------------------------------------------------------------------
      INTEGER NENER, NPAR
      REAL*8  A0   , A02  , RY , PI
C-----------------------------------------------------------------------
      PARAMETER(NENER=200,NPAR=20)
      PARAMETER(A0=0.529d-8)
      PARAMETER(A02=A0*A0)
      PARAMETER(Ry=13.606d0)
      PARAMETER(PI=3.141592d0)
C-----------------------------------------------------------------------
      INTEGER IOUTPUT,   IPAR,    IENER
      INTEGER I
C-----------------------------------------------------------------------
      REAL*8        DE,            W
      REAL*8    PAR(NPAR),   ELE(NENER)
      REAL*8 SIGMA(NENER),   OMG(NENER),    VECX(NENER)
      REAL*8 FFORM(NENER)
C-----------------------------------------------------------------------

      DO I=1, IENER
         VECX(I)=1.D-3*ELE(I)
         SIGMA(I)=(56.34D0*DLOG(DEXP(1.D0)+1.115D0*VECX(I))/
     1        (VECX(I)*(1+(21.2D0*(VECX(I)**(-2.41D0))))))*
     2        DEXP(-9.136D0/(1+(8.37D-2*(VECX(I)**1.2733D0))))*1.D-16
         OMG(I)=W*SIGMA(I)*ELE(I)/(A02*Ry)
         IF (IOUTPUT.EQ.1) THEN
            FFORM(I)=SIGMA(I)
         ELSE IF (IOUTPUT.EQ.0) THEN
            FFORM(I)=OMG(I)
         ENDIF
      ENDDO
      RETURN
      END
