      SUBROUTINE FFORM_201( IOUTPUT, IPAR, IENER, DE,
     &                      W      , PAR , TE  , FFORM
     &                    )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: FFORM_201**************
C
C  PURPOSE: Calculate fitting formula, 201 for an array of energies
C
C  INPUT: (I*4) IOUTPUT  = TYPE OF OUTPUT:
C                             0 : upsilon
C                             1 : rate (cm3/s)
C  INPUT: (I*4) IPAR     = NUMBER OF PARAMETERS
C  INPUT: (I*4) IENER    = NUMBER OF ENERGIES (X)
C  INPUT: (R*8) DE       = ENERGY DIFFERENCE OR TRANSITION ENERGY
C  INPUT: (R*8) W        = STATISTICAL WEIGTH OF INITIAL STATE
C  INPUT: (R*8) PAR()    = ARRAY OF FITTING PARAMETERS
C                            DIM : FITTING PARAMETERS (MAX 50)
C  INPUT: (R*8) te()     = ARRAY OF TEMPERATURES (eV)
C                            DIM : TRMPERATURES (MAX 200)
C
C  OUTPUT:(R*8) FFORM()  = ARRAY OF UPSS OR RATES
C
C   - Formula for vibrational excitation on ground state
C   - Formula 4.4 in my Janev version
C   - UPS definition of Shemansky et al. ApJ. 296, 765 (1985) there are other
C     definitions with a factor of PI in the denominator
C
C   THE ENERGY IMPUT IN THIS FORMULA IS IN KEV/AMU --. OUTPUT TO EV/AMU
C
C  AUTHOR: F. GUZMAN
C  DATE  : 5/07/2010
C
C  VERSION : 1.1
C  DATE    : 05-07-2010
C  MODIFIED: F Guzman
C              - First version
C
C-----------------------------------------------------------------------
      INTEGER NENER, NPAR
      REAL*8  RY   , A0  , PI, BOLKEVK, ME, ELCH, RYJ, A02
C-----------------------------------------------------------------------
      PARAMETER(NENER=200,NPAR=20)
      PARAMETER(RY=13.60569193D0, A0=0.52917720859D-8)
      PARAMETER(PI=3.141592d0)
      PARAMETER(BOLKEVK=8.617343D-5)
      PARAMETER(ME=9.10938215D-28, ELCH=1.602176487D-19)
      parameter(RYJ=RY*ELCH, A02=A0*A0)
C-----------------------------------------------------------------------
      INTEGER IOUTPUT,   IPAR,    IENER
      INTEGER I
C-----------------------------------------------------------------------
      REAL*8        DE,            W
      REAL*8    PAR(NPAR),   te(NENER)
      REAL*8 RATE(NENER),   UPS(NENER),    VECX(NENER)
      REAL*8 FFORM(NENER)
C-----------------------------------------------------------------------

      DO I=1, IENER
         VECX(I)=te(I)/BOLKEVK
         RATE(I)=(PAR(1)/((1.D-3*VECX(I))**PAR(2)))+
     1        (PAR(3)/((1.D-3*VECX(I))**PAR(4)))+
     2        (PAR(5)/((1.D-3*VECX(I))**(2.D0*PAR(6))))
         RATE(I)=DEXP(RATE(I))
         UPS(I)=(DSQRT(PI*ME)*DSQRT(te(I)*ELCH*1.D7)*W)*RATE(I)
     1           /(DSQRT(8.D0)*RYJ*1.D7*A02)
         UPS(I)=W*RATE(I)*te(I)/(A02*Ry)
         IF (IOUTPUT.EQ.1) THEN
            FFORM(I)=RATE(I)
         ELSE IF (IOUTPUT.EQ.0) THEN
            FFORM(I)=UPS(I)
         ENDIF
      ENDDO
      RETURN
      END
