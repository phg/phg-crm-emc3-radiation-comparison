      SUBROUTINE FFORM_102( IOUTPUT, IPAR, IENER, DE,
     &                      W      , PAR , ELE  , FFORM
     &                    )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: FFORM_102**************
C
C  PURPOSE: Calculate fitting formula, 102 for an array of energies
C
C  INPUT: (I*4) IOUTPUT  = TYPE OF OUTPUT: 
C                             0 : OMEGA
C                             1 : SIGMAS (cm2)
C  INPUT: (I*4) IPAR     = NUMBER OF PARAMETERS
C  INPUT: (I*4) IENER    = NUMBER OF ENERGIES (X)
C  INPUT: (R*8) DE       = ENERGY DIFFERENCE OR TRANSITION ENERGY
C  INPUT: (R*8) W        = STATISTICAL WEIGTH OF INITIAL STATE
C  INPUT: (R*8) PAR()    = ARRAY OF FITTING PARAMETERS
C                            DIM : FITTING PARAMETERS (MAX 50)
C  INPUT: (R*8) ELE()    = ARRAY OF IMPACT ENERGIES (X)
C                            DIM : ENERGIES (MAX 200)
C
C  OUTPUT:(R*8) FFORM()  = ARRAY OF OMGS OR SIGMAS
C
C   -Formula for charge transfer 
C   -Formula 5.8 in my Janev version
C   - omg definition of Shemansky et al. ApJ. 296, 765 (1985) there are others 
C     definitions with a factor of PI in the denominator
C
C   IMPORTANT: In this formula DE must be passed to e_th in laboratory energy
C   of the projectile what means that e_th=DE=1.5*de_cm. In the mdf files 
C   DE is always so multiply before the calcuations and reset when finished. 
C
C  AUTHOR: F. GUZMAN
C  DATE  : 5/07/2010
C
C  VERSION : 1.1
C  DATE    : 05-07-2010
C  MODIFIED: F Guzman
C              - First version
C
C-----------------------------------------------------------------------
      INTEGER NENER, NPAR
      REAL*8  A0   , A02  , RY , PI
C-----------------------------------------------------------------------
      PARAMETER(NENER=200,NPAR=20)
      PARAMETER(A0=0.529d-8)
      PARAMETER(A02=A0*A0)
      PARAMETER(Ry=13.606d0)
      PARAMETER(PI=3.141592d0)
C-----------------------------------------------------------------------
      INTEGER IOUTPUT,   IPAR,    IENER
      INTEGER I
C-----------------------------------------------------------------------
      REAL*8        DE,            W
      REAL*8    PAR(NPAR),   ELE(NENER)
      REAL*8 SIGMA(NENER),   OMG(NENER),    VECX(NENER)
      REAL*8 FFORM(NENER)
      REAL*8       SLESS,    SBIG
C-----------------------------------------------------------------------
      DE=1.5D0*DE 
C-----------------------------------------------------------------------
      DO I=1, IENER
         VECX(I)=1.5D0*ELE(I)
         SLESS=PAR(1)*(VECX(I)**(PAR(2)))*((1.D0-(DE/VECX(I))**(PAR(3)))
     1        **(PAR(4)))*DEXP(-PAR(5)*(VECX(I)**PAR(6)))
         SBIG=PAR(7)*DEXP(-PAR(8)/(VECX(I)**PAR(9)))/
     1        ((PAR(10)*VECX(I)**(PAR(11)))+
     2        (PAR(12)*(VECX(I)**(PAR(13))))+
     3        (PAR(14)*(VECX(I)**(PAR(15))))+
     4        (PAR(16)*(VECX(I)**(PAR(17)))))
         SIGMA(I)=(SLESS+SBIG)*1.D-16
         OMG(I)=W*SIGMA(I)*ELE(I)/(A02*Ry)
         IF (IOUTPUT.EQ.1) THEN
            FFORM(I)=SIGMA(I)
         ELSE IF (IOUTPUT.EQ.0) THEN
            FFORM(I)=OMG(I)
         ENDIF
      ENDDO
      DE=DE/1.5D0
      RETURN
      END
