      SUBROUTINE FFORM_1( IOUTPUT, IPAR, IENER, DE,
     &                    W      , PAR , ELE  , FFORM
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ***************** FORTRAN77 SUBROUTINE: FFORM_1**************
C
C  PURPOSE: Calculate fitting formula, 1 for an array of energies
C
C  INPUT: (I*4) IOUTPUT  = TYPE OF OUTPUT: 
C                             0 : OMEGA
C                             1 : SIGMAS (cm2)
C  INPUT: (I*4) IPAR     = NUMBER OF PARAMETERS
C  INPUT: (I*4) IENER    = NUMBER OF ENERGIES (X)
C  INPUT: (R*8) DE       = ENERGY DIFFERENCE OR TRANSITION ENERGY
C  INPUT: (R*8) W        = STATISTICAL WEIGTH OF INITIAL STATE
C  INPUT: (R*8) PAR()    = ARRAY OF FITTING PARAMETERS
C                            DIM : FITTING PARAMETERS (MAX 50)
C  INPUT: (R*8) ELE()    = ARRAY OF IMPACT ENERGIES (X)
C                            DIM : ENERGIES (MAX 200)
C
C  OUTPUT:(R*8) FFORM()  = ARRAY OF OMGS OR SIGMAS
C
C   -Formula for vibrational excitation via H2- resonant states
C   -Formula 4.3 in my Janev version
C   - omg definition of Shemansky et al. ApJ. 296, 765 (1985) there are others 
C     definitions with a factor of PI in the denominator
C
C  AUTHOR: F. GUZMAN
C  DATE  : 5/07/2010
C
C  VERSION : 1.1
C  DATE    : 05-07-2010
C  MODIFIED: F Guzman
C              - First version
C
C-----------------------------------------------------------------------
      INTEGER NENER, NPAR
      REAL*8  A0   , A02  , RY , PI
C-----------------------------------------------------------------------
      PARAMETER(NENER=200,NPAR=20)
      PARAMETER(A0=0.529d-8)
      PARAMETER(A02=A0*A0)
      PARAMETER(Ry=13.606d0)
      PARAMETER(PI=3.141592d0)
C-----------------------------------------------------------------------
      INTEGER IOUTPUT,   IPAR,    IENER
      INTEGER I
C-----------------------------------------------------------------------
      REAL*8        DE,            W
      REAL*8    PAR(NPAR),   ELE(NENER)
      REAL*8 SIGMA(NENER),   OMG(NENER),    VECX(NENER)
      REAL*8 FFORM(NENER)
C-----------------------------------------------------------------------

      DO I=1, IENER
         VECX(I)=ELE(I)/DE
         SIGMA(I)=(5.78d0*PAR(1)*((1-(1/VECX(I)))**(6.11/DSQRT(DE))))
     &           /((DE**4.d0)*VECX(I)*VECX(I))*1.d-16
         OMG(I)=W*SIGMA(I)*ELE(I)/(A02*Ry)
         IF (IOUTPUT.EQ.1) THEN
            FFORM(I)=SIGMA(I)
         ELSE IF (IOUTPUT.EQ.0) THEN
            FFORM(I)=OMG(I)
         ENDIF
      ENDDO
      RETURN
      END
