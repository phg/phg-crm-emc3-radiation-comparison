C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas602/adas602.for,v 1.3 2004/07/06 11:01:23 whitefor Exp $ Date $Date: 2004/07/06 11:01:23 $
C
       implicit none            
C-----------------------------------------------------------------------
C
C ******************** FORTRAN77 PROGRAM: ADAS602 **********************
C
C NAME: ADAS602
C
C VERSION:  1.0
C
C PURPOSE:
C         FORTRAN interface routine to UNIX pipe.  
C
C PROGRAM:
C	  (I*4) getfit  = array containing fitting parameters (dim. 6)
C                         getfit(1) - Allow variable line widths (1-y,0-n).
C                         getfit(2) - Fit different widths (1-y,0-n).
C                         getfit(3) - Linear component to background (1-y,0-n).
C                         getfit(4) - Quadratic component to background
C				     (1-y,0-n).
C                         getfit(5) - Weigthed fit (1-y,0-n).
C                         getfit(6) - No background.
C	  (I*4) ifail   = error flag for fitting routine (see routine xxder1).
C	  (I*4) ipvt    = see xxder1.
C	  (I*4) jdo     = Options choices vector.
C	  (I*4) jf      = No. of free parameters.
C	  (I*4) mlwa	= Size of wa.
C	  (I*4) mnp	= Size of jdo, b, cj etc.
C	  (I*4) ndline	= Parameter, max. no. of lines.
C	  (I*4) ndtot	= no. of grating positions
C	  (I*4) nfxl	= no. of lines with fixed positions.
C
C	  (R*8) b	= work vector (see xxycf2).
C	  (R*8) bch	= Vertical separation of initial background guesses.
C	  (R*8) c       = variance co-variance matrix (see xxycf2).
C	  (R*8) cj      = copy of c.
C	  (R*8) fjac	= see xxder1.
C	  (R*8) fvec 	= see xxder1.
C	  (R*8) xg	= see xxder1.
C	  (R*8) wa	= see xxder1.
C
C	  For the other parameters, see ml_gauss.for
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   ML_GAUSS   ADAS	THEORETICAL SPECTRUM FITTING ROUTINE.
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION.
C	   XXFLSH     ADAS	FLUSHES UNIX PIPE.
C
C AUTHOR: D.BROOKS UNIVERSITY OF STRATHCLYDE, JUNE 1997
C
C VERSION:  1.1					DATE: 02-07-97
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION: 1.2					DATE: 20-07-01
C MODIFIED: RICHARD MARTIN
C		- ADDED SUPPORT FOR NIS PROFILES
C
C VERSION: 1.3					DATE: 23-05-02
C MODIFIED: RICHARD MARTIN
C		- Modified to write ya & profiles to pipe as real*4
C
C-----------------------------------------------------------------------
       integer pipein, pipeou, ndtot, ndline, mnp, mlwa, nfxl
       integer maxparams       
       parameter(pipein =5, pipeou=6, ndtot=2048, ndline=10)
       parameter( mnp = 3+2+(4*ndline) )
       parameter (mlwa = 5*mnp+ndtot)
       parameter (maxparams=11)
C-----------------------------------------------------------------------
C  MLWA estimated as for document XXDER1.  2048 is max no of grating
C  positions or pixels or wavelength steps
C-----------------------------------------------------------------------
       integer nsize,numline,getfit(6),i,k,ipvt(mnp),jdo(mnp)
       integer kindep, kwfree, kb0, kb1, kb2, kwt, jf, ifail,Ibtype
       real*8 bch, x(ndtot), y(ndtot), rscs(ndtot), ya(ndtot)
	 real*8 profiles(ndtot,ndline)
       real*8 xo(ndline),w(ndline),flux(ndline),esef(ndline)
       real*8 eclf(ndline), h(ndline), eseh(ndline)
       real*8 eclh(ndline),esexo(ndline),eclxo(ndline)
       real*8 esewv(ndline), eclwv(ndline)
       real*8 b0,b1,b2,eseb0,eseb1,eseb2,esewl
       real*8 eclb0,eclb1,eclb2,eclwl
       real*8 fvec(ndtot),fjac(ndtot,mnp)      
       real*8 xg(mnp),cj(mnp),b(mnp),c(mnp,mnp),wa(mlwa)
       real*8 paramval(maxparams)

C-----------------------------------------------------------------------
C   Set up for i4unit to go to screen, avoiding the pipe 
C-----------------------------------------------------------------------
       call xx0000
C-----------------------------------------------------------------------
C read the data from the pipe
C-----------------------------------------------------------------------
       read(pipein,1000)nsize
       do 10 i = 1,nsize
         read(pipein,1001)x(i),y(i),rscs(i)
 10    continue
       read(pipein,1002)numline
       read(pipein,1002)nfxl
       do 20 i = 1,numline
         read(pipein,1003)xo(i),w(i)
 20    continue
       read(pipein,1004)bch
       do 30 i = 1,6
         read(pipein,1005)getfit(i)
 30    continue
       do 35 i = 1,maxparams
         read(pipein,1004)paramval(i)
 35    continue
 	 read(pipein,1002)Ibtype

       kwfree = getfit(1)
C-----------------------------------------------------------------------
C altered order of getfit for consistent printout with IDL /DHB 10/05/96
C Added kb0 for no background DHB 28/10/96
C-----------------------------------------------------------------------
       kb1 = getfit(3)
       kb2 = getfit(4)
       kindep = getfit(2)
       kwt = getfit(5)
       kb0 = getfit(6)
       call ml_gauss( X,Y,PROFILES,NDLINE,NUMLINE,NFXL,MNP,MLWA,
     & XO,W,BCH,MAXPARAMS,PARAMVAL,IBTYPE,NDTOT,NSIZE,RSCS,KWFREE,
     & KINDEP,KWT,KB0,KB1,KB2,YA,FLUX,ESEF,ECLF,B0,B1,B2,H,JF,
     & ESEB0,ESEB1,ESEB2,ESEWL,ECLB0,ECLB1,ECLB2,ECLWL,ESEH,
     & ECLH,ESEXO,ECLXO,ESEWV,ECLWV,IFAIL,FVEC,FJAC,
     & XG,CJ,JDO,B,C,IPVT,WA)
C-----------------------------------------------------------------------
C write results to the pipe
C-----------------------------------------------------------------------
       do 40 i = 1,nsize
         write(pipeou,1004)real(ya(i))
         call xxflsh(pipeou)
	   do k=1,numline
	   	write(pipeou,1004)real(profiles(i,k))
		call xxflsh(pipeou)
	   enddo
 40    continue
       write(pipeou,1006)jf,ifail
       call xxflsh(pipeou)
       write(pipeou,1001)b0,b1,b2
       call xxflsh(pipeou)
       write(pipeou,1007)eseb0,eseb1,eseb2,esewl
       call xxflsh(pipeou)
       write(pipeou,1007)eclb0,eclb1,eclb2,eclwl
       call xxflsh(pipeou)
       do 50 i = 1, numline
         write(pipeou,1007)xo(i),h(i),w(i),flux(i)
         call xxflsh(pipeou)
         write(pipeou,1001)esexo(i),eseh(i),esef(i)
         call xxflsh(pipeou)
         write(pipeou,1001)eclxo(i),eclh(i),eclf(i)
         call xxflsh(pipeou)
         write(pipeou,1003)esewv(i),eclwv(i)
         call xxflsh(pipeou)
 50    continue
 1000  format(1x,i4)
 1001  format(1x,e13.6,1x,e13.6,1x,e13.6)
 1002  format(1x,i2)
 1003  format(1x,e13.6,1x,e13.6)
 1004  format(1x,e13.6)
 1005  format(1x,i1)
 1006  format(1x,i4,1x,i4)
 1007  format(1x,e13.6,1x,e13.6,1x,e13.6,1x,e13.6)
       stop
       end
