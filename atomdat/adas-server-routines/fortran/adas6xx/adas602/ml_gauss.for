C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas602/ml_gauss.for,v 1.6 2019/09/15 16:33:52 mog Exp $ Date $Date: 2019/09/15 16:33:52 $
C
      SUBROUTINE ML_GAUSS(X,Y,PROFILES,NDLINE,NUMLINE,NFXL,MNP,MLWA,
     & XO,W,BCH,MAXPARAMS,PARAMVAL,IBTYPE,NDTOT,NSIZE,RSCS,KWFREE,
     & KINDEP,KWT,KB0,KB1,KB2,YA,FLUX,ESEF,ECLF,B0,B1,B2,H,JF,
     & ESEB0,ESEB1,ESEB2,ESEWL,ECLB0,ECLB1,ECLB2,ECLWL,ESEH,
     & ECLH,ESEXO,ECLXO,ESEWV,ECLWV,IFAIL,FVEC,FJAC, 
     & XG,CJ,JDO,B,C,IPVT,WA)
C
C-----------------------------------------------------------------------
C  ******************** FORTRAN77 PROGRAM: ML_GAUSS *********************
C
C    ORIGINAL NAME: S/R ML_GAUSS
C
C    PURPOSE: 
C
C    ( November 1996
C      Added no background option
C
C      June-August 1996
C      Added non-weighted option, removed dependence on NAG routines 
C
C      April 1996    
C      Modified program to have variable line widths.
C      Program also now works via IDL pipe.
C
C      18 March 1987
C      The data passed include counts/sec versus grating position,
C      the exposure (qsecs) at each position.
C      Also passed are the number of lines (maximum 10 ), estimates of 
C      the line peak positions and their fwhm along with the change of 
C      background (min to max) and control flags determining aspects of
C      the fitting.  
C    )
C    
C    This routine then estimates initial values for the background base
C    level, its slope and curvature as necessary.
C    The input estimates of line widths are averaged to get the initial
C    estimate of the width if it a common width is to be fitted or 
C    else the input fixed width is used.
C
C    Using the input line positions the heights above the background are
C    estimated for initial guesses.
C
C    The library routine XXDER1 is used to find the maximum 
C    likelihood estimate of the background, the positions and width of
C    the  lines. (See write up by J Lang).
C
C    The library routine XXYCF2 is used to estimate the variance-
C    covariance matrix. This then allows estimates of the standard 
C    errors and 95% confidence limits on the fitted parameters.
C
C    Estimates of the line fluxes ,their standard errors and 95% 
C    confidence limits are then calculated.
C  
C    The spectrum of the result is also calculated.
C
C
C    INPUT
C      y(nsize)    real*8  The input spectrum
C      x(nsize)    real*8  The X values at each data point
C      rscs(nsize) real*8  The number of qsecs at each data point
C      w(10)       real*8  Estimates of the individual line FWHM,
C                          or the fixed line width. 
C      nsize       int*4   Size of the x,y,ya arrays ie no. of grat psns. 
C      numline     int*4   the number of lines fitted
C      kwfree      int*4   line widths to vary  = 1, =0 for fixed input 
C                          line width
C      kindep      int*4   0 for same line width to be fitted to all lines, 
C                          otherwise =1 for variable line widths to be 
C                          fitted
C      kwt         int*4   =1 for weighted fit, =0 for unweighted  fit 
C      kb0         int*4   =1 for  fit to background, =0 no fit to
C                          background and forces kb1 and kb2 to 0
C      kb1         int*4   The base allowed to have a linear slope(=1,
C                          =0 no)
C      kb2         int*4   The base allowed to have a parabolic shape 
C                          (= 1, = 0 for not).
C    OUTPUT
C      b0          real*8  fitted base level - see the equation
C      b1          real*8  fitted slope      -  "   "     "
C      b2          real*8  fitted curve      -  "   "     "
C      h(10)       real*8  fitted heights    -  "   "     "
C      xo(10)      real*8  fitted positions  -  "   "     "
C      w(10)       real*8  fitted widths (FWHM)
C      eseb0       real*8  estimated standard error in b0  
C      eseb1       real*8  estimated standard error in b1
C      eseb2       real*8  estimated standard error in b2
C      esewl       real*8  estimated standard error in the width if the 
C                          same width for all lines
C      eseh(10)    real*8  estimated standard error in the heights
C      esexo(10)   real*8  estimated standard error in the positions
C      esewv(10)   real*8  estimated standard error in the widths
C      eclbo       real*8  estimated 95% confidence limit in b0
C      eclb1       real*8  estimated 95% confidence limit in b1 
C      eclb2       real*8  estimated 95% confidence limit in b2 
C      eclwl       real*8  estimated 95% confidence limit in the width,
C                          if the same width  for all lines
C      eclh(10)    real*8  estimated 95% confidence limit in heights
C      eclxo(10)   real*8  estimated 95% confidence limit in positions
C      eclwv(10)   real*8  estimated 95% confidence limit in the widths
C      flux(10)    real*8  the flux of each line
C      esef(10)    real*8  estimated standard error in fluxes
C      eclf(10)    real*8  estimated 95% confidence limit in fluxes
C      ya(nsize)   real*8  the calculated spectrum
C
C
C      The equation fitted is:-
C
C                                        numline
C                                        ___
C                                        \
C       I(k)  = b0 + b1*x  + b2*x *x  +   >  h *f (x ,xo ,wl ,f )
C                        k       k  k    /    i  i  k   i   i  i
C                                        ---
C                                        i=1
C
C
C
C
C              A J Penny              RAL       Jan 86
C              J Lang, J Payne        RAL       Mar 87 
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXDER1     ADAS      LEAST SQUARES FITTING ROUTINE.
C	   XXYCF2     ADAS	ESIMATES VARIANCE-COVARIANCE MATRIX FOR
C				XXDER1.
C
C  HISTORY
C
C  Jeff Payne RAL  4 July 1986
C  Removed GETFIT and passed values from IDL
C
C  Jeff Payne RAL 11 Sept 1986
C  Passed jf - number of free parameters back to IDL from 
C  subroutine GAUSS.
C
C  J Lang, J Payne (Mar 87) 
C  Renamed this s/r ml_gauss from dogauss and brought some s/r from 
C  do_trapeze as well as extensive mods to allow maximum likelihood 
C  analysis to find fitted parameters, their estimated standard errors 
C  and 95% confidence limits.
C
C  18 March 1987
C  The data passed include counts/sec versus grating position,
C  the exposure (qsecs) at each position.
C  Also passed are the number of lines (maximum 10 ), estimates of 
C  the line peak positions and their fwhm along with the change of 
C  background (min to max) and control flags determining aspects of
C  the fitting.
C
C  J Lang, D Brooks (April 96)
C  Now used via IDL pipe and ADAS 602 as calling routine
C  Added option of using variable widths for lines (implemented 
C  KINDEP=1).
C
C  W Osborn, J Lang, D Brooks (June-August 96)
C  Added no weight option and put weights into common in mlfit to 
C  speed up routine called by mlfit.
C  Replaced E04GCF NAG library routine with routine XXDER1 which
C  is based on Minpack routine LMDER1. NAG routine E04YCF replaced
C  by XXYCF2 written by William Osborn as part of ADAS.
C
C  DH Brooks (October 28)+ J Lang (Nov 96)
C  Added no background option kb0.
C
C-----------------------------------------------------------------------
C
C VERSION:  1.0 D.BROOKS 			DATE:
C		- PORTED TO ADAS		
C
C VERSION:  1.1					DATE: 02-07-97
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:  1.2					DATE: 23-11-98
C MODIFIED: RICHARD MARTIN 
C		- CORRECTED ERROR IN CALCULATION OF INDICES FOR JACOBIAN
C		  IN CASE INDEPENDENT VARYING WIDTHS + FIXED LINE POSITIONS.
C
C VERSION:  1.3					DATE: 24-01-2001
C MODIFIED: Martin O'Mullane
C		- In getavwdth and getheights the size of the input
C                 adjustable array was defined after the array
C                 declaration. Swap such that integers are defined
C                 before real*8.
C
C VERSION: 1.4					DATE:	20-07-2001
C MODIFIED: RICHARD MARTIN
C		- Added support for NIS profiles
C
C VERSION: 1.5					DATE:	12-12-2001
C MODIFIED: Martin O'Mullane
C		- In mlfit and lsfun2 FPCOUNT and FPCOUNT2 should
C                 be defined as integers.
C
C VERSION : 1.6					
C DATE    : 12-09-2019
C MODIFIED: Martin O'Mullane
C		- Make sure code does not extend beyond column 72.
C               - Change some constants to real*8.
C
C-----------------------------------------------------------------------
C
      INTEGER NSIZE,NUMLINE,KWFREE,KINDEP,KWT,KB0,KB1,KB2,JF,LWA
      INTEGER MNP, MLWA, NFXL
      INTEGER MAXPARAMS, IBTYPE
	INTEGER NDTOT,NDLINE
C
C
      REAL*8 Y(NSIZE),X(NSIZE),RSCS(NSIZE),YA(NSIZE)
      real*8 PROFILES(NDTOT,NDLINE)
      REAL*8 H(NUMLINE),XO(NUMLINE),W(NUMLINE),FLUX(NUMLINE)
      REAL*8 ESEH(NUMLINE),ECLH(NUMLINE),ESEXO(NUMLINE)
      REAL*8 ECLXO(NUMLINE),ESEWV(NUMLINE),ECLWV(NUMLINE)
      REAL*8 ESEF(NUMLINE),ECLF(NUMLINE) 
      REAL*8 XG(MNP),CJ(MNP),WA(MLWA),B(MNP),C(MNP,MNP)
      REAL*8 FVEC(NSIZE),FJAC(NSIZE,MNP)
      REAL*8 B0,B1,B2,ESEB0,ESEB1,ESEB2,ECLB0,ECLB1,ECLB2
      REAL*8 ESEWL,ECLWL,BCH,WL
      REAL*8 PARAMVAL(MAXPARAMS)
      INTEGER IFAIL, IPVT(MNP)
C
C
C  Estimate the input values for the base (b0,b1,b2)
C
      IF (KB0 .EQ. 1) THEN
        CALL getbase(nsize,y,x,kb1,kb2,bch,b0,b1,b2)
      ELSE
         B0=0.0D0
      ENDIF
C
C
C
C  Estimate the input value for the width w(h). 
C  If the line widths are allowed to vary individually
C  still must call this subroutine as the input wdiths need halved.
C
      CALL GETAVWDTH(KINDEP,NUMLINE,W,WL)
C
C
C
C  Estimate the input values for the heights (h)
C
      CALL getheights(nsize,y,x,b0,xo,numline,h)
C
C
C
C  Find the number of free parameters (JF) and LWA for Minpack routine
C  Modified on implementing all lines to have different widths
C
      CALL NFREEPAR (NSIZE,KB0,KB1,KB2,KWFREE,KINDEP,NUMLINE,NFXL,JF,
     +               LWA,MNP,JDO)
C
C  If the number of free parameters is greater than the number
C  of grating positions, no fit is possible.
C
      IF (NSIZE .LT.JF) RETURN
C
C
C
C  Do the fit (get b0,b1,b2,wl,h,xo and estimates of their standard 
C  errors annd 95% confidence limits)
C  This subroutine also calls the routine XXDER1 
C  and XXYCF2, the subroutine LSFUN2 necessary for 
C  XXDER1 as well as the subroutine TDIST.
C  If XX routines have error exit return to calling routine.
C
c	write(0,*)'PARAMVAL=',PARAMVAL
      CALL MLFIT(Y,X,RSCS,NSIZE,JF,NUMLINE,NFXL,KB0,KB1,KB2,KWFREE,
     +           KINDEP,KWT,LWA,B0,B1,B2,WL,W,H,XO,MAXPARAMS,PARAMVAL,
     +		 IBTYPE,ESEB0,ESEB1,ESEB2,ESEWL,ESEH,
     +           ESEXO,ECLB0,ECLB1,ECLB2,ECLWL,ECLH,ECLXO,ESEWV,ECLWV,
     +           XG,CJ,WA,B,C,FVEC,FJAC,IPVT,IFAIL )
C
C
C  
C
C  The vector W was used to pass initial guesses of the widths and
C  these values were converted to a scalar WL.
C  Now re-fill W with the estimated or fixed width. 
C
      IF (KINDEP .EQ. 0) THEN
         DO 10 J=1,NUMLINE
            W(J)=WL
  10     CONTINUE
      ENDIF       
C
C
C
C  Calculate the fitted spectrum (calculate YA)
C
      CALL CALGSPEC(NDTOT,NDLINE,NSIZE,NUMLINE,KB0,X,PROFILES,PARAMVAL,
     +		MAXPARAMS,IBTYPE,W,H,XO,B0,B1,B2,YA)
C
C
C
C  Calculate the lines fluxes, their estimated standard errors and their
C  estimated 95% confidence limits.
C
      CALL CALGFL(W,XO,H,NUMLINE,IBTYPE,PARAMVAL,MAXPARAMS,ESEWL,ECLWL,
     +       ESEH,ECLH,ESEXO,ECLXO,ESEWV,ECLWV,FLUX,ESEF,ECLF,KWFREE,
     +	 KINDEP)
C
C
      RETURN
      END
C
C 
C
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      S/R GETBASE
C
C  This subroutine estimates the input guesses for the base
C  level, the base slope, the estimate of the parabolic shape
C
C                              2
C      for y = b0 + b1.x + b2.x
C
C
C  INPUT
C    nsize   int*4         Size of data arrays (no of grat. psns)
C    y       real*8(nsize) The data values
C    x       real*8(nsize) The x values at the data points
C    kb1     int*4         Flag for slope allowed (=1,=0for not)
C    kb2     int*4         Flag for curve allowed (=1,=0for not)
C    bch     real*8        Change in background over length
C  OUTPUT
C    b0      real*8        The base level guess
C    b1      real*8        The slope guess (=0 for no slope)
C    b2      real*8        The parabolic base guess (=0 for no para)
C
C
C   a j penny                      ral                 feb 86
C ------------------------------------------------------------
C
C
C
      subroutine getbase(nsize,y,x,kb1,kb2,bch,b0,b1,b2)
C
C
      integer nsize,kb1,kb2,k
      REAL*8 x(nsize),y(nsize),bch,b0,b1,b2
C
C
      b0 = 1000000.0D0
      do k = 1,nsize
         if (y(k).ge.0.0D0) b0 = DMIN1(b0,y(k))
      enddo
C
      b1 = 0.0D0
      if (kb1.eq.1) then
         b1 = bch/(x(nsize)-x(1))
      endif
C
      b2 = 0.0D0
      if (kb2.eq.1) then
         b2 = 4.0D0*bch/((x(nsize)-x(1))*(x(nsize)-x(1)))
      endif
C
C
      return
      end
C
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      S/R GETAVWDTH
C
C  For KINDEP =0 this subroutine gets the average of all the input 
C  widths (both fixed and non-fixed position lines) when 
C  the width is an unknown otherwise it gets the fixed input
C  width. Half the width is returned because of the definition 
C  of the gaussian profile.
C  For KINDEP=1 this subroutine finds half of each of the input widths
C  because of definition of the gaussian profile, including the fixed
C  line widths.
C
C  INPUT
C    w(numline) real*8   The widths of the lines as input.
C    numline    int*4    The actual number of lines.
C    kindep     int*4    Line width control flag 
C  OUTPUT
C    wl         real*8   The average input width or fixed width.
C    w(numline) real*8   The individual line widths/2.0
C
C   J Lang           RAL     March 1987
C --------------------------------------------------------------
C
      SUBROUTINE GETAVWDTH(KINDEP,NUMLINE,W,WL)
C
C
      INTEGER KINDEP,NUMLINE
      REAL*8 W(NUMLINE)
      REAL*8 WL,SUM
C
c	WRITE(0,*)'getavwidth: kindep=',kindep
      IF (KINDEP .EQ. 0) THEN
        SUM=0.0D0
        DO  K = 1,NUMLINE
          SUM = SUM + W(K)
        ENDDO
        WL = SUM/DFLOAT(NUMLINE)
        WL = WL/2.0D0
      ELSE
        DO K = 1,NUMLINE
           W(K)=W(K)/2.0D0
        ENDDO
      ENDIF
      RETURN
      END
C     
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      S/R GETHEIGHTS
C
C  This subroutine estimates the input guesses for the heights of
C  the lines above the base.
C  It does this by taking the height of the data point above the
C  flat base level input at the input position. Thus no allowance
C  is made for blending or mis-positioning or base errors. (but
C  these are not too important as inputs to the least-squares)
C
C
C  INPUT
C    nsize   int*4          Size of the arrays (no of grat. psns.)
C    y       real*8(nsize)  The data values
C    x       real*8(nsize)  The x values at the data points
C    b0      real*8         The input base level
C    xo      real*8(numline)The input line positions
C    numline int*4          The actual no of lines
C  OUTPUT
C    h       real*8(numline)The input guesses as to the line 
C                           heights
C
C
C   a j penny                      ral                 feb 86
C ------------------------------------------------------------
C
C
      subroutine getheights(nsize,y,x,b0,xo,numline,h)
C
C
      integer nsize,numline,k,kk
      REAL*8 y(nsize),x(nsize),xo(numline),h(numline),b0,pos
      INTEGER I4UNIT
      logical again
C
C  Check that the grating posns are smoothly increasing
C
      do k = 1,numline-1
         if (x(k).gt.x(k+1)) then
            WRITE(I4UNIT(-1),100)'X coords must be ascending order' 
            return
         endif
      enddo
C
C  Get the heights
C
      do k = 1,numline
         pos = xo(k)
         again = .true.
         kk = 0
         do while (again)
            kk = kk + 1
            if (x(kk).ge.pos) again = .false.
            if (kk.eq.nsize) again = .false.
         enddo
         h(k) = y(kk) - b0
      enddo
C
 100  FORMAT(//'X coords must be ascending order')
C
      return
      end
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C
C      S/R NFREEPAR
C
C   This subroutine finds the number of free parameters from
C   the used flags and the parameter LWA needed by Minpack routine
C
C   Derived by J Lang  Mar 87 from A J Penny equivalent
C   routines (dated Feb/Mar 86).
C
C
C
      SUBROUTINE NFREEPAR (NSIZE,KB0,KB1,KB2,KWFREE,KINDEP,
     +                     NUMLINE,NFXL,JF,LWA,MNP,JDO)
C
C
      IMPLICIT REAL*8  (A-H , O-Z)
      DIMENSION JDO(MNP)
C      NOUT=6
C
C
C
C  Load the used flags (JDO)
C
      DO K = 1,MNP
         JDO(K) = -1
      ENDDO
      IF (KB0.EQ.1) JDO(1) = 1
      IF (KB1.EQ.1) JDO(2) = 1
      IF (KB2.EQ.1) JDO(3) = 1
      IF (KINDEP .EQ. 0) THEN
        IF (KWFREE.EQ.1) JDO(4) = 1
        DO K = 1,(NUMLINE-NFXL)
           JDO(5+2*(K-1)) = 1
           JDO(6+2*(K-1)) = 1
        ENDDO
        DO K = 1, NFXL
          JDO(6+2*(NUMLINE-NFXL-1)+K) = 1
        ENDDO
      ELSE
        DO K = 1,(NUMLINE-NFXL)
          JDO(4+3*(K-1)) = 1
          JDO(5+3*(K-1)) = 1
          JDO(6+3*(K-1)) = 1
        ENDDO
        DO K = 1, NFXL
          JDO(6+3*(NUMLINE-NFXL-1)+(2*(K-1)+1)) = 1
          JDO(7+3*(NUMLINE-NFXL-1)+(2*(K-1)+1)) = 1
        ENDDO
      ENDIF    
C
C  Calc no of free parameters (JF)
C  ie  N of NAG library routines used.
C
      JF = 0
      DO K = 1,MNP
         IF (JDO(K).EQ.1) JF = JF + 1
      ENDDO
C
C
C  Calculate LWA parameter needed by Minpack routines.
C
      LWA = 5*JF+NSIZE
C
C  Number of free paramters must not exceed the number of grating
C  positions.
C
      IF (JF.GT.NSIZE) GO TO 10
      RETURN
  10  WRITE (I4UNIT(-1),20)JF,NSIZE
  20  FORMAT(//' Number of free parameters ',I3,' greater than number',
     +        ' of x positions',I4 ,/,'Fit not possible')
      RETURN    
      END
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C
C      S/R MLFIT
C
C   This subroutine fits the spectrum from the initial parameter
C   estimates by means maximum likilihood theory. A Minpack library 
C   routines is used to maximise the log likelihood
C   as described in note by J Lang. The variance-covariance matrix 
C   is evaluated using a routine written by W.Osborn. The diagonal 
C   elements of this matrix are then used to find the standard errors 
C   and 95% confidence limits of the estimated parameters.  
C
C   J Lang et al.                   RAL                 Aug 96
C
C ------------------------------------------------------------
C
C
C
      SUBROUTINE MLFIT(Y,X,RSCS,NSIZE,JF,NUMLINE,NFXL,KB0,KB1,KB2,
     +                  KWFREE,KINDEP,KWT,LWA,B0,B1,B2,WL,W,H,XO,
     +			MAXPARAMS,PARAMVAL,IBTYPE,ESEB0,ESEB1,ESEB2,
     +                  ESEWL,ESEH,ESEXO,ECLB0,ECLB1,ECLB2,ECLWL,ECLH,
     +                  ECLXO,ESEWV,ECLWV,XG,CJ,WA,B,C,FVEC,FJAC,
     +                  IPVT,IFAIL )
C
C
C
      IMPLICIT REAL*8  (A-H , O-Z)
      PARAMETER (NNSIZE = 2048, MNFXL=10,MMAXPARAMS=11)
      DIMENSION Y(NSIZE),X(NSIZE),RSCS(NSIZE)
      DIMENSION H(NUMLINE),XO(NUMLINE), W(NUMLINE)
      DIMENSION WA(LWA),B(JF),C(JF,JF)
      DIMENSION FVEC(NSIZE),FJAC(NSIZE,JF),IPVT(JF)
      DIMENSION XG(JF),CJ(JF)
      DIMENSION ESEH(NUMLINE),ESEXO(NUMLINE)
      DIMENSION ECLH(NUMLINE),ECLXO(NUMLINE)
      DIMENSION ESEWV(NUMLINE),ECLWV(NUMLINE)
      DIMENSION YY(NNSIZE),XX(NNSIZE),RRSCS(NNSIZE),SIGM(NNSIZE)
      DIMENSION TTEST(2048),XXFO(MNFXL),XFO(MNFXL)
      DIMENSION PARAMVAL(MMAXPARAMS),PPARAMVAL(MMAXPARAMS)
      
      integer fpcount
      
      EXTERNAL LSFUN2
C
      COMMON YY,XX,RRSCS,XXFO,SIGM,FFWL,PPARAMVAL,NNUMLINE,
     +       NNKB0,NNFXL,KKB0,KKB1,KKB2,KKWFREE,KKINDEP,IIBTYPE
C      NOUT=6
C
C
C  First set up the common variables to pass to S/R LSFUN2
C  Set the array elements to zero
C
      DO 5 K=1,NNSIZE
       XX(K) = 0.0D0
       YY(K) = 0.0D0 
       RRSCS(K) =0.0D0
       SIGM(K) = 0.0D0
 5    CONTINUE 
C
C  Now fill up the variables
      DO 6 K=1,NSIZE
       YY(K) = Y(K)
       XX(K) = X(K)
C   Put qsecs into secs
       RRSCS(K) = RSCS(K)*0.25
  6   CONTINUE
C
C  Sort out whether weighted or unweighted
      IF (KWT .EQ. 1) THEN
C   Set up sigma of the 'weight'
C   Note sigma is inverse of write up but rhs also inverted for ease.
        DO 7 K = 1,NSIZE
         SIGM(K) = 1.0/DSQRT(YY(K)/(RRSCS(K)*2.0))
  7     CONTINUE
      ELSE
        DO 8 K = 1,NSIZE
         SIGM(K) = 1.0
  8     CONTINUE
      ENDIF
	
	DO k=1,MMAXPARAMS
		PPARAMVAL(k)=PARAMVAL(k)
	ENDDO
C
	IIBTYPE=IBTYPE
      NNUMLINE = NUMLINE
      KKB0 = KB0
      KKB1 = KB1
      KKB2 = KB2
      KKWFREE =  KWFREE
      KKINDEP =  KINDEP
      NNFXL = NFXL
C
C  Set up FFWL the fixed line width needed in COMMON to get
C  the fixed line width into s/r LSFUN2. If not fixed line
C  width the width goes in as part of XG.
C
      IF (KWFREE.EQ.0) THEN
       FFWL=WL
      ELSE
       FFWL=0.0
      ENDIF
C
C
C  Evaluate the vector XG(MNP) 
C   ie the initial guess vector for EO4GCF
C  Elements of the vector depend on chosen background and whether
C  the width is a free parameter or not.
C
C For the case of fitted background: 
C  For KINDEP = 0 ie a fixed line width or the same fitted line width 
C  for all lines the following table is used to assign elements of 
C  the vector.
C
C    For KINDEP = 0
C       KB1   KB2   KWFREE 
C        1     1      1
C        1     1      0
C        1     0      1
C        1     0      0
C        0     1      1
C        0     1      0
C        0     0      1
C        0     0      0
C
C  
C  For KINDEP = 1  ie all lines allowed to have different fitted line
C  widths the following table is used.
C
C   FOR KINDEP = 1, KWFREE must = 1
C       KB1   KB2
C        1     1
C        1     0
C        0     1
C        0     0
C
C  The code below works out elements of XG(MNP) depending on above
C  ie if KB1=0 and KB2=1 then XG(2)=B2.
C
C  For the case of no background, as well as kb0=0 we have kb1=kb2=0
C  and we just modify the cases in the tables above where kb1=kb2=0.
C  This avoids adding yet more loops.
C
C  First the vector elements are set to zero
C  Note that the routine is coded for clarity not efficiency
C  since this part is only called once.
C
      DO 10  K=1,JF
      XG(K) = 0.0D0
 10   CONTINUE
C

	FPCOUNT=1				
	IF (KB0.EQ.1) THEN 
		XG(FPCOUNT)= B0
		FPCOUNT=FPCOUNT+1
	ENDIF
	
	IF (KB1.EQ.1) THEN 
	    XG(FPCOUNT)= B1
	    FPCOUNT=FPCOUNT+1
	ENDIF     
	IF (KB2.EQ.1) THEN 
	    XG(FPCOUNT)= B2
	    FPCOUNT=FPCOUNT+1
	ENDIF 
	IF (KWFREE.EQ.1.AND.KINDEP.EQ.0) THEN
	    XG(FPCOUNT)= WL
	    FPCOUNT=FPCOUNT+1
	ENDIF
	
	DO K=1,(NUMLINE-NFXL)
	    XG(FPCOUNT)  = H(K)
	    XG(FPCOUNT+1)= XO(K)
	    FPCOUNT=FPCOUNT+2
	    IF (KINDEP.EQ.1) THEN
	          XG(FPCOUNT)=W(K)
	          FPCOUNT=FPCOUNT+1
	    ENDIF
	ENDDO
	
	DO K=1,NFXL
	    XG(FPCOUNT)=H(NUMLINE-NFXL+K)
	    FPCOUNT=FPCOUNT+1
	    IF (KINDEP.EQ.1) THEN
	          XG(FPCOUNT)=W(NUMLINE-NFXL+K)
	          FPCOUNT=FPCOUNT+1	
	    ENDIF
	ENDDO

      DO 47 I = 1, NFXL
        XFO(I) = XO(NUMLINE-NFXL+I)
        XXFO(I) = XO(NUMLINE-NFXL+I)
 47   CONTINUE
C   
C
C  Now find the ML estimates, 
C  first getting the remaining input data for XXDER1.
C
      IFAIL=0
C
      CALL XXDER1(LSFUN2,NSIZE,JF,XG,FVEC,FJAC,NSIZE,
     +            IFAIL,IPVT,WA,LWA)
C
C  Since IFAIL was set to 0 on entry to XXDER1, must test value
C  of IFAIL on exit.
C  If IFAIL = 1,2,3 OK
C           = 5 then call XXDER1 again,if IFAIL then = 1,2,3,OK else quit.
C           = 0,4,6,7 then quit.
C
      IF (IFAIL.EQ.1) GO TO 50
      IF (IFAIL.EQ.2) GO TO 50
      IF (IFAIL.EQ.3) GO TO 50
      IF (IFAIL.EQ.5) THEN
          DO K=1,5
           IFAIL=0
          CALL XXDER1(LSFUN2,NSIZE,JF,XG,FVEC,FJAC,NSIZE,IFAIL,
     +                 IPVT,WA,LWA)
          IF (IFAIL .NE. 5) GO TO 49
          ENDDO
  49    IF (IFAIL.EQ.1) GO TO 50
        IF (IFAIL.EQ.2) GO TO 50
        IF (IFAIL.EQ.3) GO TO 50
        WRITE(I4UNIT(-1),102) 
        WRITE(I4UNIT(-1),103) IFAIL
        RETURN
      ELSE
        WRITE (I4UNIT(-1),101) IFAIL
        RETURN
      ENDIF
C
  50  CONTINUE
C
C
C  Compute estimates of the variances of the sample regression
C  coefficients at the final point
C 
        CALL XXYCF2(LSFUN2,NSIZE,JF,XG,FVEC,FJAC,B,C)
C
      DO 60 K = 1, JF
        CJ(K) = C(K,K)
  60  CONTINUE
C
C
C 
C  Take vector XG(MNP) and evaluate the parameters
C  This is just the inverse of part where vector was evaluated
C  Use the same decoding to extract variances from CJ(JF).
C  In fact take DSQRT of variances to get estimated standard error.
C  Also get the confidence limits. 
C
C
C  First fill the vector TTEST with the upper 97.5% points of
C  Students t distribution needed for the 95% confidence limits
C  and initialise to zero the ESE and ECL variables.
C
C  Size of TTEST set to 2048. If (NSIZE-JF) is more than 2048 then
C  will need to get out of this routine.
      IF ((NSIZE-JF) .GT. 2048) THEN
         WRITE(I4UNIT(-1),111)
         RETURN
      ENDIF
      CALL TDIST(TTEST)
C
      ESEB0=0.0
      ESEB1=0.0
      ESEB2=0.0
      ESEWL=0.0
      ECLB0=0.0
      ECLB1=0.0 
      ECLB2=0.0 
      ECLWL=0.0 
      DO 70 K=1,NUMLINE
       ESEH(K) =0.0
       ESEXO(K)=0.0
       ESEWV(K)=0.0
       ECLH(K) =0.0
       ECLXO(K)=0.0
       ECLWV(K)=0.0
  70  CONTINUE
C
C
	FPCOUNT=1
	CL=TTEST(NSIZE-JF)
	
	IF (KB0.EQ.1) THEN
		B0 = XG(FPCOUNT)
      	ESEB0=DSQRT(CJ(FPCOUNT))
      	ECLB0=CL*ESEB0
      	FPCOUNT=FPCOUNT+1
      ENDIF
      IF (KB1.EQ.1) THEN
      	B1 = XG(FPCOUNT)
      	ESEB1=DSQRT(CJ(FPCOUNT))
      	ECLB1=CL*ESEB1
      	FPCOUNT=FPCOUNT+1
      ENDIF
      IF (KB2.EQ.1) THEN
      	B2 = XG(FPCOUNT)
      	ESEB2=DSQRT(CJ(FPCOUNT))
      	ECLB2=CL*ESEB2
      	FPCOUNT=FPCOUNT+1
      ENDIF
      IF (KWFREE.EQ.1.AND.KINDEP.EQ.0) THEN
      	WL = XG(FPCOUNT)
      	ESEWL=DSQRT(CJ(FPCOUNT))
      	ECLWL=CL*ESEWL
      	FPCOUNT=FPCOUNT+1
      ENDIF
      
      DO K=1,(NUMLINE-NFXL)
      	H(K)    = XG(FPCOUNT)
      	XO(K)   = XG(FPCOUNT+1)   	
      	ESEH(K) = DSQRT(CJ(FPCOUNT))
      	ESEXO(K)= DSQRT(CJ(FPCOUNT+1))
      	ECLH(K) = CL*ESEH(K)
      	ECLXO(K)= CL*ESEXO(K)
      	FPCOUNT=FPCOUNT+2
      	IF (KINDEP.EQ.1) THEN
      		W(K)    = XG(FPCOUNT)    		      	
			ESEWV(K)= DSQRT(CJ(FPCOUNT))
			ECLWV(K)= CL*ESEWV(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
	ENDDO
      DO K=1,NFXL
      	H(NUMLINE-NFXL+K)    = XG(FPCOUNT)   	
      	ESEH(NUMLINE-NFXL+K) = DSQRT(CJ(FPCOUNT))
      	ECLH(NUMLINE-NFXL+K) = CL*ESEH(NUMLINE-NFXL+K)
      	FPCOUNT=FPCOUNT+1
      	IF (KINDEP.EQ.1) THEN
      		W(NUMLINE-NFXL+K)    = XG(FPCOUNT)    		      	
			ESEWV(NUMLINE-NFXL+K)= DSQRT(CJ(FPCOUNT))
			ECLWV(NUMLINE-NFXL+K)= CL*ESEWV(NUMLINE-NFXL+K)
			FPCOUNT=FPCOUNT+1
		ENDIF
	ENDDO	
	RETURN


C
C
C 
 101  FORMAT(//' Error exit type ',I3,' from XXDER1. See routine '
     +       ,'document')
 102  FORMAT(//' After 1 call of XXDER1 IFAIL was 5. On restart from') 
 103  FORMAT('  final point of first call error exit type ',I3,' See ',
     +       'routine document')
 111  FORMAT(//' No. of x positions less no. of free parameters',
     +        'greater than declared and evaluated elements of TTEST' )    
C
C
      END
C     
C 
C 
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  S/R LSFUN2
C
C  This is the s/r needed by the routines XXDER1, XXYCF2.
C  It has COMMON with s/r ML_FIT to pass parameters not
C  in the argument list.
C  The code for X(G) is also used to get the correct vectors.
C  See write up for discussion of derivatives.
C
C
      SUBROUTINE LSFUN2(NSIZE, JF, XG, FVEC, FJAC, LDFJAC, IFLAG)
C
C
      IMPLICIT REAL*8  (A-H , O-Z)
      PARAMETER (NNSIZE = 2048, MNFXL=10, MMAXPARAMS=11)
      DIMENSION FJAC(NSIZE,JF),FVEC(NSIZE),XG(JF)
      DIMENSION YY(NNSIZE),XX(NNSIZE),RRSCS(NNSIZE)
      DIMENSION SWHD(NNSIZE),X2(NNSIZE),SIGM(NNSIZE)
      DIMENSION SUM2E(NNSIZE),SWHD2(NNSIZE),XXFO(MNFXL),XP(MNFXL*3)
      DIMENSION PPARAMVAL(MMAXPARAMS)
      
      integer fpcount, fpcount2
        
C
C XXFO HAS DIMENSIONS JF, ALTHOUGH IT ONLY NEEDS NNUMLINE-NNFXL.
C THIS IS TO AVOID USING THE COMMON BLOCK INTEGERS AS DIMENSION
C STATEMENTS.  20/08/96
C
      COMMON YY,XX,RRSCS,XXFO,SIGM,FFWL,PPARAMVAL,NNUMLINE,
     +       NNKB0,NNFXL,KKB0,KKB1,KKB2,KKWFREE,KKINDEP,IBTYPE
C
C
      DO 10 K=1,NSIZE
       SUM2E(K) = 0.0
       SWHD(K) = 0.0
       SWHD2(K) = 0.0
       X2(K)   = 0.0
  10  CONTINUE
  
  	ALPHALEFT=PPARAMVAL(1)
	ALPHARIGHT=PPARAMVAL(2)
	SIGMA=PPARAMVAL(3)
C
C
	FPCOUNT=1
      I1=1
      I2=1
      I3=1
      IF (KKB0.EQ.1) THEN
      	I1=FPCOUNT
      	FPCOUNT=FPCOUNT+1
      ENDIF
      IF (KKB1.EQ.1) THEN 
      	I2=FPCOUNT
      	FPCOUNT=FPCOUNT+1
      ENDIF
      IF (KKB2.EQ.1) THEN
      	I3=FPCOUNT
      	FPCOUNT=FPCOUNT+1
      ENDIF
      IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FPCOUNT=FPCOUNT+1
	FPCOUNT2=FPCOUNT	
	DO I=1,(NNUMLINE-NNFXL)
		XP(3*(I-1)+1)=XG(FPCOUNT2)
		XP(3*(I-1)+2)=XG(FPCOUNT2+1)
		FPCOUNT2=FPCOUNT2+2
		IF (KKWFREE.EQ.0) THEN
			XP(3*(I-1)+3)=FFWL
		ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN
			XP(3*(I-1)+3)=XG(FPCOUNT-1)
		ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
			XP(3*(I-1)+3)=XG(FPCOUNT2)
			FPCOUNT2=FPCOUNT2+1
		ENDIF
	ENDDO
	DO I=1,NNFXL
		XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)=XG(FPCOUNT2)
		FPCOUNT2=FPCOUNT2+1
		XP(3*(NNUMLINE-NNFXL)+3*(I-1)+2)=XXFO(I)
		IF (KKWFREE.EQ.0) THEN
			XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3)=FFWL
		ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN
			XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3)=XG(FPCOUNT-1)
		ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
			XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3)=XG(FPCOUNT2)
			FPCOUNT2=FPCOUNT2+1
		ENDIF
	ENDDO

      IF (IFLAG.EQ.1)THEN
	  IF (IBTYPE.EQ.0) THEN
		DO K=1,NSIZE
			SUME=0.D0
			DO I=1,NNUMLINE
			   DIF=XX(K)-XP(3*(I-1)+2)
			   WWL=XP(3*(I-1)+3)*XP(3*(I-1)+3)
			   SUME=SUME+XP(3*(I-1)+1)*DEXP(-(DIF*DIF)/WWL)    
			ENDDO
			X2(K)=XX(K)*XX(K)
			FVEC(K)=(YY(K)-(KKB0*XG(I1)+KKB1*XG(I2)*XX(K)+
     +       		KKB2*XG(I3)*X2(K)+SUME))*SIGM(K)
     		ENDDO			
	  ELSE IF (IBTYPE.EQ.1.OR.IBTYPE.EQ.2) THEN
		DO K=1,NSIZE
			SUME=0.D0
			DO I=1,NNUMLINE
				IF (XX(K).LT.XP(3*(I-1)+2)) THEN
					ALPHA=ALPHALEFT
				ELSE
					ALPHA=ALPHARIGHT
				ENDIF
				DIF=XX(K)-XP(3*(I-1)+2)
				WWL=XP(3*(I-1)+3)*XP(3*(I-1)+3)
		  	      TERMA=DEXP(-DIF*DIF/WWL)		
		  		TERMB=1.0d0/((DIF/(2.0d0*XP(3*(I-1)+3)*
     +				DSQRT(DLOG(2.0d0))))**2+1.0d0)
		  		TERME=(1.0d0-ALPHA)*TERMA+ALPHA*TERMB
				SUME=SUME+XP(3*(I-1)+1)*TERME
			ENDDO
			X2(K)=XX(K)*XX(K)
			FVEC(K)=(YY(K)-(KKB0*XG(I1)+KKB1*XG(I2)*XX(K)+
     +       		KKB2*XG(I3)*X2(K)+SUME))*SIGM(K)
     		ENDDO
	  ELSE IF (IBTYPE.EQ.3) THEN
		DO K=1,NSIZE
			SUME=0.D0
			DO I=1,NNUMLINE
				IF (XX(K).LT.XP(3*(I-1)+2)) THEN
					ALPHA=ALPHALEFT
				ELSE
					ALPHA=ALPHARIGHT
				ENDIF
				DIF=XX(K)-XP(3*(I-1)+2)
				WWL=XP(3*(I-1)+3)*XP(3*(I-1)+3)
		  	      TERMA=DEXP(-DIF*DIF/WWL)		
		  		TERMB=1.0d0/((DIF/SIGMA)**2+1.0d0)
		  		TERME=(1.0d0-ALPHA)*TERMA+ALPHA*TERMB
				SUME=SUME+XP(3*(I-1)+1)*TERME
			ENDDO
			X2(K)=XX(K)*XX(K)
			FVEC(K)=(YY(K)-(KKB0*XG(I1)+KKB1*XG(I2)*XX(K)+
     +       		KKB2*XG(I3)*X2(K)+SUME))*SIGM(K)
     		ENDDO

	  ENDIF
       	RETURN
CC
      ELSE
CC	 
	 IF (IBTYPE.EQ.0) THEN
         DO K=1,NSIZE
	   	FPCOUNT=1
          	SWHD(K)=0.D0 
          	SWHD2(K)=0.D0
         	X2(K)=XX(K)*XX(K)
         	IF (KKB0.EQ.1) THEN
			FJAC(K,I1)=-SIGM(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	IF (KKB1.EQ.1) THEN
			FJAC(K,I2)=-SIGM(K)*XX(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	IF (KKB2.EQ.1) THEN
			FJAC(K,I3)=-SIGM(K)*X2(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	FPCOUNT2=FPCOUNT
	   	IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FPCOUNT2=FPCOUNT2+1
	   	DO I=1,(NNUMLINE-NNFXL)
		  DIF=XX(K)-XP(3*(I-1)+2)		
		  WWL=XP(3*(I-1)+3)*XP(3*(I-1)+3)
		  TERME=DEXP(-DIF*DIF/WWL)		  
		  FJAC(K,FPCOUNT2)=-TERME*SIGM(K)					! w.r.t. h
		  FJAC(K,FPCOUNT2+1)=-2.0*DIF*XP(3*(I-1)+1)*			! w.r.t. xo
     +			TERME*SIGM(K)/WWL
		  FPCOUNT2=FPCOUNT2+2
		  IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN
		  	SWHD(K)=SWHD(K)+2.0*DIF*DIF*XP(3*(I-1)+1)*		! w.r.t. w 
     +			TERME/(WWL*XP(3*(I-1)+3))
     		  ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
     		  	FJAC(K,FPCOUNT2)=-2.0*DIF*DIF*XP(3*(I-1)+1)*		! w.r.t. w 
     +			TERME*SIGM(K)/(WWL*XP(3*(I-1)+3))
     			FPCOUNT2=FPCOUNT2+1
     		  ENDIF
     		ENDDO
	   	DO I=1,NNFXL
		  DIF=XX(K)-XP(3*(NNUMLINE-NNFXL)+3*(I-1)+2)
		  WWL=XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3)**2	
		  TERME=DEXP(-(DIF*DIF)/WWL)
		  FJAC(K,FPCOUNT2)=-TERME*SIGM(K)
		  FPCOUNT2=FPCOUNT2+1
		  IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN
		    SWHD2(K)=SWHD2(K)+2*DIF*DIF*
     +  	  	XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)*TERME
     +		/(WWL*XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3))
		  ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
		    FJAC(K,FPCOUNT2)=-2.0*DIF*DIF*	
     +		XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)*TERME*SIGM(K)/
     +		(WWL*XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3))
     		    FPCOUNT2=FPCOUNT2+1
     		  ENDIF
	   	ENDDO     		 
	   	IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FJAC(K,FPCOUNT)=
     +		-SIGM(K)*(SWHD(K)+SWHD2(K))
     	   ENDDO  		 
	   RETURN
	 ELSE IF (IBTYPE.EQ.1.OR.IBTYPE.EQ.2) THEN
         DO K=1,NSIZE
	   	FPCOUNT=1
          	SWHD(K)=0.D0 
          	SWHD2(K)=0.D0
         	X2(K)=XX(K)*XX(K)
         	IF (KKB0.EQ.1) THEN
			FJAC(K,I1)=-SIGM(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	IF (KKB1.EQ.1) THEN
			FJAC(K,I2)=-SIGM(K)*XX(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	IF (KKB2.EQ.1) THEN
			FJAC(K,I3)=-SIGM(K)*X2(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	FPCOUNT2=FPCOUNT
	   	IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FPCOUNT2=FPCOUNT2+1

	   	DO I=1,(NNUMLINE-NNFXL)
		  IF (XX(K).LT.XP(3*(I-1)+2)) THEN
		  	  ALPHA=ALPHALEFT
		  ELSE
		  	  ALPHA=ALPHARIGHT
		  ENDIF
		  DIF=XX(K)-XP(3*(I-1)+2)		
		  WWL=XP(3*(I-1)+3)*XP(3*(I-1)+3)
		  TERMA=DEXP(-DIF*DIF/WWL)		
		  TERMB=1.0d0/
     +		((DIF/(2.0d0*XP(3*(I-1)+3)*DSQRT(DLOG(2.0d0))))**2+
     +		1.0d0)
		  TERME=(1.0d0-ALPHA)*TERMA+ALPHA*TERMB
		  FJAC(K,FPCOUNT2)=-TERME*SIGM(K)					! w.r.t. h
		  FJAC(K,FPCOUNT2+1)=-DIF*XP(3*(I-1)+1)*				! w.r.t. xo
     +			((1.0d0-ALPHA)*2.0d0*TERMA+ALPHA*TERMB**2/
     +			(2.0d0*DLOG(2.0d0)))*SIGM(K)/WWL
		  FPCOUNT2=FPCOUNT2+2
		  IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN
		  	SWHD(K)=SWHD(K)+XP(3*(I-1)+1)*DIF**2*		! W.R.T. W
     +			((1.0d0-ALPHA)*2.0d0*TERMA+ALPHA*TERMB**2/
     +			(2.0d0*DLOG(2.0d0)))/(WWL*XP(3*(I-1)+3))
     		  ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
     		  	FJAC(K,FPCOUNT2)=-XP(3*(I-1)+1)*DIF**2*	         ! W.R.T. W
     +			((1.0d0-ALPHA)*2.0d0*TERMA+ALPHA*TERMB**2/
     +		     (2.0d0*DLOG(2.0d0)))*SIGM(K)/(WWL*XP(3*(I-1)+3))
     			FPCOUNT2=FPCOUNT2+1
     		  ENDIF
     		ENDDO
	   	DO I=1,NNFXL
		  IF (XX(K).LT.XP(3*(I-1)+2)) THEN
		  	  ALPHA=ALPHALEFT
		  ELSE
		  	  ALPHA=ALPHARIGHT
		  ENDIF		
		  DIF=XX(K)-XP(3*(NNUMLINE-NNFXL)+3*(I-1)+2)
		  WWL=XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3)**2	
		  TERMA=DEXP(-DIF*DIF/WWL)		
		  TERMB=1.0D0/
     +		((DIF/(2.0D0*XP(3*(I-1)+3)*DSQRT(DLOG(2.0D0))))**2 
     +			+1.0D0)
		  TERME=DEXP(-(DIF*DIF)/WWL)
		  FJAC(K,FPCOUNT2)=-TERME*SIGM(K)
		  FPCOUNT2=FPCOUNT2+1
		  IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN

		    SWHD2(K)=SWHD2(K)+XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)*		! W.R.T. W
     +			DIF**2*((1.0d0-ALPHA)*2.0d0*TERMA+ALPHA*
     +			TERMB**2/(2.0d0*DLOG(2.0d0)))/
     +			(WWL*XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3))
		    
		  ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
		  
		    FJAC(K,FPCOUNT2)=-XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)*		! W.R.T. W
     +			DIF**2*((1.0d0-ALPHA)*2.0d0*TERMA+ALPHA*
     +			TERMB**2/(2.0d0*DLOG(2.0d0)))*SIGM(K)/
     +			(WWL*XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3))
     		    FPCOUNT2=FPCOUNT2+1
		    
     		  ENDIF
	   	ENDDO     		 
	   	IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FJAC(K,FPCOUNT)=
     +		-SIGM(K)*(SWHD(K)+SWHD2(K))
     	   ENDDO  
	   RETURN	 
	 ELSE IF (IBTYPE.EQ.3) THEN
         DO K=1,NSIZE
	   	FPCOUNT=1
          	SWHD(K)=0.D0 
          	SWHD2(K)=0.D0
         	X2(K)=XX(K)*XX(K)
         	IF (KKB0.EQ.1) THEN
			FJAC(K,I1)=-SIGM(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	IF (KKB1.EQ.1) THEN
			FJAC(K,I2)=-SIGM(K)*XX(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	IF (KKB2.EQ.1) THEN
			FJAC(K,I3)=-SIGM(K)*X2(K)
			FPCOUNT=FPCOUNT+1
		ENDIF
         	FPCOUNT2=FPCOUNT
	   	IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FPCOUNT2=FPCOUNT2+1

	   	DO I=1,(NNUMLINE-NNFXL)
		  IF (XX(K).LT.XP(3*(I-1)+2)) THEN
		  	  ALPHA=ALPHALEFT
		  ELSE
		  	  ALPHA=ALPHARIGHT
		  ENDIF
		  DIF=XX(K)-XP(3*(I-1)+2)		
		  WWL=XP(3*(I-1)+3)*XP(3*(I-1)+3)
		  TERMA=DEXP(-DIF*DIF/WWL)		
		  TERMB=1.0d0/((DIF/SIGMA)**2+1.0d0)
		  TERME=(1.0d0-ALPHA)*TERMA+ALPHA*TERMB
		  FJAC(K,FPCOUNT2)=-TERME*SIGM(K)						! w.r.t. h
		  FJAC(K,FPCOUNT2+1)=-DIF*XP(3*(I-1)+1)*					! w.r.t. xo
     +			((1.0d0-ALPHA)*2.0d0*TERMA/WWL+ALPHA*TERMB**2/
     +			SIGMA**2)*SIGM(K)
		  FPCOUNT2=FPCOUNT2+2
		  IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN
		  	SWHD(K)=SWHD(K)+XP(3*(I-1)+1)*DIF**2*		! W.R.T. W
     +			(1.0d0-ALPHA)*2.0d0*TERMA/(WWL*XP(3*(I-1)+3))
     		  ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
     		  	FJAC(K,FPCOUNT2)=-XP(3*(I-1)+1)*DIF**2*		! W.R.T. W
     +			(1.0d0-ALPHA)*2.0d0*TERMA*SIGM(K)/
     +                  (WWL*XP(3*(I-1)+3))
     			FPCOUNT2=FPCOUNT2+1
     		  ENDIF
     		ENDDO
	   	DO I=1,NNFXL
		  IF (XX(K).LT.XP(3*(I-1)+2)) THEN
		  	  ALPHA=ALPHALEFT
		  ELSE
		  	  ALPHA=ALPHARIGHT
		  ENDIF		
		  DIF=XX(K)-XP(3*(NNUMLINE-NNFXL)+3*(I-1)+2)
		  WWL=XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3)**2	
		  TERMA=DEXP(-DIF*DIF/WWL)		
		  TERMB=1.0D0/((DIF/SIGMA)**2+1.0D0)
		  TERME=DEXP(-(DIF*DIF)/WWL)
		  FJAC(K,FPCOUNT2)=-TERME*SIGM(K)
		  FPCOUNT2=FPCOUNT2+1
		  IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) THEN

		    SWHD2(K)=SWHD2(K)+XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)*		! W.R.T. W
     +			DIF**2*(1.0d0-ALPHA)*2.0d0*TERMA/
     +			(WWL*XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3))
		    
		  ELSE IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.1) THEN
		  
		    FJAC(K,FPCOUNT2)=-XP(3*(NNUMLINE-NNFXL)+3*(I-1)+1)*		! W.R.T. W
     +			DIF**2*(1.0d0-ALPHA)*2.0d0*TERMA*SIGM(K)/
     +			(WWL*XP(3*(NNUMLINE-NNFXL)+3*(I-1)+3))
     		    FPCOUNT2=FPCOUNT2+1
		    
     		  ENDIF
	   	ENDDO     		 
	   	IF (KKWFREE.EQ.1.AND.KKINDEP.EQ.0) FJAC(K,FPCOUNT)=
     +		-SIGM(K)*(SWHD(K)+SWHD2(K))
     	   ENDDO  	   	 
	   RETURN
	 ENDIF

	ENDIF

C
      END
C
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  S/R   TDIST
C
C  This fills the vector TTEST with the upper 97.5% percentage
C  points of Student's t distribution.
C  The dimension of TTEST is fixed at 180 from considerations 
C  of the number of grating positions and free parameters.
C
      SUBROUTINE TDIST(TTEST)
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION TTEST(2048)
C
      TTEST(1)=12.706d0      
      TTEST(2)=4.303d0      
      TTEST(3)=3.182d0      
      TTEST(4)=2.776d0      
      TTEST(5)=2.571d0      
      TTEST(6)=2.447d0      
      TTEST(7)=2.365d0      
      TTEST(8)=2.306d0      
      TTEST(9)=2.262d0      
      TTEST(10)=2.228d0
      TTEST(11)=2.201d0
      TTEST(12)=2.179d0
      TTEST(13)=2.160d0
      TTEST(14)=2.145d0
      TTEST(15)=2.131d0
      TTEST(16)=2.120d0
      TTEST(17)=2.110d0
      TTEST(18)=2.101d0
      TTEST(19)=2.093d0
      TTEST(20)=2.086d0
      TTEST(21)=2.080d0
      TTEST(22)=2.074d0
      TTEST(23)=2.069d0
      TTEST(24)=2.064d0
      TTEST(25)=2.060d0
      TTEST(26)=2.056d0
      TTEST(27)=2.052d0
      TTEST(28)=2.048d0
      TTEST(29)=2.045d0
      TTEST(30)=2.042d0
C
      DO 1 K=31,40
       RK=DFLOAT(K)
       TTEST(K)=2.042 + ((2.021-2.0420)/10.0)*(RK-30.0)
   1  CONTINUE
C
      DO 2 K=41,60
       RK=DFLOAT(K)
       TTEST(K)= 2.021 + ((2.000-2.021)/20.0)*(RK-40.0)
   2  CONTINUE
C
      DO 3 K=61,120
       RK=DFLOAT(K)
       TTEST(K)=2.000 + ((1.980-2.000)/60.0)*(RK-60.0)
   3  CONTINUE
C
      DO 4 K=121,180
       RK=DFLOAT(K)
       TTEST(K)= 1.980 + ((1.972-1.980)/60.0)*(RK-120.0)
C 25/4/96   The 60.0 in line above was 80.0 before this date. 
   4  CONTINUE
C
      DO 5 K=181,1000
       RK=DFLOAT(K)
       TTEST(K)=1.972+((1.960-1.972)/820.0)*(RK-180.0)
   5  CONTINUE
C
      DO 6 K=1000,2048
       TTEST(K)=1.960
   6  CONTINUE
C
      RETURN
      END
C
C
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  S/R   CALGSPEC
C
C  This calculates the fitted spectrum.
C
C  INPUT
C    nsize   int*4          Size of data arrays (no of grat. psns.)
C    numline int*4           Actual    "     "    "
C    kb0     int*4   
C    x       real*8(nsize)   x values at data points
C    w       real*8(numline) Width of the lines
C    h       real*8(numline) Heights of the lines
C    xo      real*8(numline) Positions of the lines
C    b0      real*8          Base level
C    b1      real*8          Slope in base
C    b2      real*8          Parabolic part of slope
C  OUTPUT
C    ya      real*8(nsize)   Spectrum made from the fit
C
C
C  a j penny                ral                  feb 86
C  and modified slightly by 
C  J Lang                   RAL                  Mar 87
C
C -----------------------------------------------------------------
C
C
C
      SUBROUTINE CALGSPEC(NDTOT,NDLINE,NSIZE,NUMLINE,KB0,X,PROFILES,
     +		PARAMVAL,MAXPARAMS,IBTYPE,W,H,XO,B0,B1,B2,YA)
C
      INTEGER NDTOT,NDLINE,NSIZE,NUMLINE,KB0,MAXPARAMS,IBTYPE
C
      REAL*8 x(nsize),ya(nsize),W(NUMLINE),H(NUMLINE),XO(NUMLINE)
	real*8 PROFILES(NDTOT,NDLINE)
      REAL*8 SUM,DR
      REAL*8 B0,B1,B2
	REAL*8 PARAMVAL(MAXPARAMS),TERMA,TERMB,TERME
	REAL*8 ALPHA,ALPHALEFT,ALPHARIGHT
C
C
  	ALPHALEFT=PARAMVAL(1)
	ALPHARIGHT=PARAMVAL(2)	
C	WRITE(0,*)'FTN SPEC: IBTYPE=',IBTYPE,ALPHALEFT,ALPHARIGHT
      DO K = 1,NSIZE

C  ADD IN CONTRIBUTION FROM EACH LINE
C
	   IF (IBTYPE.EQ.0) THEN
         	 SUM = 0.0
         	 DO J = 1,NUMLINE
                DR = DABS(X(K)-XO(J))/(W(J))
		    TERME=H(J)*DEXP(-1.0*DR*DR)
         	    SUM = SUM + TERME
		    PROFILES(K,J)=TERME
         	 ENDDO
	   ELSE IF (IBTYPE.GT.0) THEN
         	 SUM = 0.0
         	 DO J = 1,NUMLINE
		     IF (X(K).LT.XO(J)) THEN
		          ALPHA=ALPHALEFT
		     ELSE
		          ALPHA=ALPHARIGHT
		     ENDIF
		     TERMA=DEXP(-((X(K)-XO(J))/W(J))**2)
		     TERMB=1.0D0/(((X(K)-XO(J))/(2.0D0*W(J)*
     +	           DSQRT(DLOG(2.0D0))))**2+1.0D0)
		     TERME=H(J)*((1.0D0-ALPHA)*TERMA+ALPHA*TERMB)
		     SUM=SUM+TERME
		     PROFILES(K,J)=TERME
		  ENDDO
	   ENDIF
C
C  ADD IN BASE AS NECESSARY
C
        IF (KB0.EQ.1) THEN 
         YA(K) = SUM + B0 + B1*X(K) + B2*X(K)*X(K)
        ELSE
         YA(K) = SUM
        ENDIF
C

      enddo
C
      return
      end
C
C
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C S/R  CALGFL
C
C  This subroutine calculates the  line fluxes, estimates of
C  their standard errors and estimates of their 95% confidence
C  intervals.
C
C   INPUT
C     nsize    int*4            Size of data arrays (no of grat. psns.)
C     y        real*8(nsize)    The data
C     ya       real*8(nsize)    The fit to the data
C     x        real*8(nsize)    The x values at the data points
C     w        real*8(numline)  The width of the lines
C     xo       real*8(numline)  The positions of the lines
C     h        real*8(numline)  The heights    "  "    "
C     numline  int*4            The actual number of lines
C     esewl    real*8           Estimated standard error in line width
C                                if it is the same for all lines
C     esewv    real*8(numline)  Estimated standard error in line widths
C     eseh     real*8(numline)  Estimated standard errors in heights
C     esexo    real*8(numline)  Estimated standard errors in line psns.
C     eclwl    real*8           Estimated 95% confidence limit in line
C                               width if it is the same for all lines
C     ecwv     real*8(numline)  Estimated 95% confidence limit for 
C                               fitted widths
C     eclh     real*8(numline)  Estimated 95% confidence limits for 
C                               heights 
C     eclxo    real*8(numline)  Estimated 95% confidence limits for
C                               positions 
C   OUTPUT
C     flux     real*8(numline)  The flux of each line
C     esef     real*8(numline)  Estimated stand. errors in line fluxes
C     eclf     real*8(numline)  Estimated 95% conf. limits in fluxes
C
C        a j penny                      ral             feb 86
C        J LANG                         RAL             MAR 87
C ------------------------------------------------------------
C
C
C
      SUBROUTINE CALGFL(W,XO,H,NUMLINE,IBTYPE,PARAMVAL,MAXPARAMS,ESEWL,
     +                  ECLWL,ESEH,ECLH,ESEXO,ECLXO,ESEWV,ECLWV,
     +                  FLUX,ESEF,ECLF,KWFREE,KINDEP)
C
C
      IMPLICIT REAL*8 (A-H,O-Z)
	INTEGER IBTYPE,MAXPARAMS
      DIMENSION XO(NUMLINE),H(NUMLINE),FLUX(NUMLINE),ESEH(NUMLINE)
      DIMENSION ECLH(NUMLINE),ESEXO(NUMLINE),ECLXO(NUMLINE)
      DIMENSION W(NUMLINE),ESEWV(NUMLINE),ECLWV(NUMLINE) 
      DIMENSION ESEF(NUMLINE),ECLF(NUMLINE)
	REAL*8 PARAMVAL(MAXPARAMS)
C
C
C
C  Initialise the vectors (clear the output)

      DO J = 1,NUMLINE
        FLUX(J) = 0.0D0
        ESEF(J) = 0.0D0
        ECLF(J) = 0.0D0
      ENDDO
C
C  Flux for each line
C
      IF (IBTYPE.EQ.0) THEN
         DO J = 1,NUMLINE
           FLUX(J) = DSQRT(3.14159D0)*H(J)*W(J)
          IF (KINDEP .EQ. 0) THEN
         	 IF (KWFREE.EQ.1) THEN
         	   ESEF(J)=FLUX(J)*DSQRT((ESEH(J)/H(J))**2 + 
     &                     (ESEWL/W(J))**2)
         	   ECLF(J)=FLUX(J)*DSQRT((ECLH(J)/H(J))**2 +
     &                    (ECLWL/W(J))**2)
         	 ELSE
         	   ESEF(J)=FLUX(J)*ESEH(J)/H(J)
         	   ECLF(J)=FLUX(J)*ECLH(J)/H(J)
         	 ENDIF
          ELSE
         	 ESEF(J)=FLUX(J)*
     &                   DSQRT((ESEH(J)/H(J))**2+(ESEWV(J)/W(J))**2)
         	 ECLF(J)=FLUX(J)*
     &                   DSQRT((ECLH(J)/H(J))**2+(ECLWV(J)/W(J))**2)
          ENDIF
         ENDDO
	ELSE IF (IBTYPE.GT.0) THEN
	   ALPHALEFT=PARAMVAL(1)
	   ALPHARIGHT=PARAMVAL(2)
c	   	WRITE(0,*)'FTN FLUX: IBTYPE=',IBTYPE,ALPHALEFT,ALPHARIGHT
         DO J = 1,NUMLINE
           FLUX(J) = H(J)*W(J)*((1.0d0-0.5D0*(ALPHARIGHT+ALPHALEFT))*
     >		DSQRT(3.14159D0)+(ALPHARIGHT+ALPHALEFT)*
     >		3.14159D0*DSQRT(DLOG(2.0D0)))
          IF (KINDEP .EQ. 0) THEN
         	 IF (KWFREE.EQ.1) THEN
         	   ESEF(J)=FLUX(J)*DSQRT((ESEH(J)/H(J))**2 + 
     &                     (ESEWL/W(J))**2)
         	   ECLF(J)=FLUX(J)*DSQRT((ECLH(J)/H(J))**2 + 
     &                     (ECLWL/W(J))**2)
         	 ELSE
         	   ESEF(J)=FLUX(J)*ESEH(J)/H(J)
         	   ECLF(J)=FLUX(J)*ECLH(J)/H(J)
         	 ENDIF
          ELSE
         	 ESEF(J)=FLUX(J)*DSQRT((ESEH(J)/H(J))**2+
     &                   (ESEWV(J)/W(J))**2)
         	 ECLF(J)=FLUX(J)*DSQRT((ECLH(J)/H(J))**2+
     &                   (ECLWV(J)/W(J))**2)
          ENDIF
         ENDDO	
	ENDIF
C
      RETURN
      END
C
C
C
C  THE END!

