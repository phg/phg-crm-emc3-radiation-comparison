C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas603/evalderiv603.for,v 1.4 2016/01/08 21:59:10 mog Exp $ Date $Date: 2016/01/08 21:59:10 $
C
C NAME:   EVALDERIV603
C
C PURPOSE: TO EVALUATE THE DERIVATIVE OF THE ENVELOPE OF A ZEEMAN/PASCHENBACK 
C            MULTIPLET FEATURE AT A GIVEN INPUT GRID POINT.
C
C INPUTS:
C
C       (R*8) xx           - Grating position
C       (R*8) B    - Magnetic field
C       (R*8) PH           - Overall peak height
C       (R*8) CN           - Centroid position
C       (R*8) T    - Temperature
C       (I*4) ncomps   - Actual no of zeeman components returned by C modules. 
C       (I*4) derivflag- 1= Calculate derv. wrt B
C                            2= Calculate derv. wrt PH 
C                            3= Calculate derv. wrt CN
C                            4= Calculate derv. wrt T
C       (R*8) lambdatmp- Input wavelength of components (if icompflag=0)
C       (R*8) strengthtmp - Input strengths of components (if icompflag=0)
C       (R*8) ffpval   - Array of parameters required to evaluate feature envelope.
C
C PROGRAM:
C       
C       (I*4) fsel         - 0= zeeman multiplet, 1= hydrogenic line
C       (I*4) ion          - Index of ion for hydrogenic line case
C       (I*4) key          - Polarisation index 
C                            0= all components, no angular dependence.
C                            1= all components, for given observation angle.
C                            2= Pi components, for given observation angle.
C                            3= Sigma+ components, for given observation angle.
C                            4= Sigma- components, for given observation angle.
C       (I*4) linein   - Iindex of selected line.
C       (I*4) lower    - Lower level index for hydrogenic transition.
C       (I*4) mnfp     - Max. no of free parameters in special feature
C       (I*4) mnxpval  - Size of array containing parameters nec. for evaluating feature.
C       (I*4) ncmps    - Max no zeeman components returned by C modules. 
C       (I*4) prof     - 0= Doppler broadening. 1= Lorentzian broadening.
C       (I*4) upper    - Upper level index for hydrogenic transition.
C
C       (R*8) c    - Speed of light.
C       (R*8) dfridr   - Function which returns numerical derivative
C       (R*8) err          - Error in numerical derivative.
C       (R*8) feature  - Calculated value of feature envelope at xx.
C       (R*8) gamma        - Width of lorentzian line profile.
C       (R*8) h            - Intial step size for evaluation of derivative.
C       (R*8) k    - Boltzmann's constant
C       (R*8) lambda   - Array containing wavelengths of zeeman componenets
C                            re-scaled into grating positions.
C       (R*8) M    - Atomic mass. 
C       (R*8) scale        - Scale factor for converting wavelengths to grating positions.
C       (R*8) strength - Strength of zeeman components
C       (R*8) theta        - Observation angle.
C       (R*8) wavemin  - Minimum of the wavelength range spanned by the input spectrum.
C
C ROUTINES:
C          ROUTINE        SOURCE        BRIEF DESCRIPTION
C          ------------------------------------------------------------
C            components2        adas/xpaschen   (C routine) Returns zeeman multiplet components 
C            hdlikecomp2        adas/xpaschen   (C routine) Returns multiplet components 
C                                                                       for hydrogenic transitions.
C            dfridr             Numerical recipes Evaluates numerical derivative (dfridr
C                                                       can be found at the end of this file).          
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, DEC 1998
C
C VERSION:  1.1                                 DATE: 
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C VERSION:  1.2                                 DATE: 13-03-2001
C MODIFIED: RICHARD MARTIN
C               - Removed second declaration of evalderiv603.
C               - Corrected mistyped Boltzmann constant.
C
C VERSION : 1.3
C DATE    : 09-04-2010
C MODIFIED: Martin O'Mullane
C           - Change integer*4 to integer.
C
C VERSION : 1.4
C DATE    : 08-01-2016
C MODIFIED: Martin O'Mullane
C           - Replace (obsolete) PAUSE statement with STOP.
C 
C-----------------------------------------------------------------------
        real*8 function evalderiv603(xx,B,PH,CN,T,ncomps,derivflag,
     >  lambdatmp,strengthtmp,FFPVAL)
        
        implicit none

c       *******************************************************************
c       ** IF CHANGE NCMPS THEN CHANGE C ROUTINE COMPONENTS2 ACCORDINGLY **
c       *******************************************************************  

        integer   i, linein, key, prof, ncomps,MNXPVAL,MNFP,NCMPS
        integer   fsel, ion, lower, upper,derivflag
        PARAMETER(MNXPVAL=16,MNFP=4,NCMPS=3710)
        real*8 FFPVAL(MNXPVAL-MNFP) 
        real*8 feature,evalfeat603,wavemin,scale,arg,err,dfridr,h
        real*8 B,PH,CN,T,THETA,line,gamma,xx
        real*8 M,c,k
        real*8 lambdatmp(0:NCMPS-1),strengthtmp(0:NCMPS-1)
        real*8 lambda(0:NCMPS-1),strength(0:NCMPS-1)    

        external evalfeat603,dfridr,components2,hdlikecomp2
        
        linein=int(FFPVAL(1))
        prof=int(FFPVAL(2))
        key=int(FFPVAL(3))
        gamma=FFPVAL(4)
        theta=FFPVAL(5)
        scale=FFPVAL(6)
        wavemin=FFPVAL(7)
        M=FFPVAL(8)*1.66057d-27
        fsel=FFPVAL(9)
        ion=int(FFPVAL(10))     
        lower=int(FFPVAL(11))
        upper=int(FFPVAL(12))

        c=2.99d+8
        k=1.381d-23
        err=0.0d0
        h=1.0d0
                
        feature=0.0d0
        
C-----------------------------------------------------------------------
C       Sum over Zeeman components to give value of derivative 
C       of feature envelope at point xx
C----------------------------------------------------------------------- 

        do i=0,ncomps-1 
           if (prof.eq.0) then
                if (lambdatmp(i).ne.0.0) then
                        lambda(i)=(lambdatmp(i)-wavemin)*scale
                        strength(i)=strengthtmp(i)
                endif
                arg=M*c*c/(2*k*T)*((xx-CN-lambda(i))/
     >          (lambda(i)+wavemin*scale))**2
                line=dexp(-arg)*strength(i)
                if (derivflag.eq.3) then
                  line=line*arg*2/(xx-CN-lambda(i))
                else if (derivflag.eq.4) then
                  line=line*arg/T
                endif
        
                feature=feature+line       
           
           else
                if (lambdatmp(i).ne.0.0) then
                        lambda(i)=(lambdatmp(i)-wavemin)*scale
                        strength(i)=strengthtmp(i)
                endif
                line=(gamma**2/((xx-CN-lambda(i))**2+gamma**2))
     >                          *strength(i)
                feature=feature+line    
           endif                
        enddo
        
        if (derivflag.eq.1) then
           h=1.0d0
           evalderiv603=dfridr(evalfeat603,xx,h,B,PH,CN,T,FFPVAL,err,
     >          ncomps,lambdatmp,strengthtmp)           
        else if (derivflag.eq.2) then
                evalderiv603=feature
        else
                evalderiv603=feature*PH
        endif
        
        return
        
        end
C
C -------------------------------------------------------------------------
       REAL*8 FUNCTION dfridr(func,x,h,B,PH,CN,T,FFPVAL,err,ncomps,
     >  lambdatmp,strengthtmp)
       
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
       INTEGER  ntab,MNFP,MNXPVAL,ncomps,NCMPS
       PARAMETER(MNXPVAL=16,MNFP=4)
       REAL*8   err,h,x,func,CON,CON2,BIG,SAFE
       PARAMETER(CON=1.4, CON2=CON*CON,BIG=1.E30,NTAB=10,SAFE=1.0)
         PARAMETER(NCMPS=3710)
       EXTERNAL         func,evalfeat603
       INTEGER  i,j
         INTEGER        KEY,LINEIN,FSEL,ION,LOWER,UPPER,icompflag
       REAL*8   errt,fac,hh,a(NTAB,NTAB)
       REAL*8     B,PH,CN,T,THETA,FFPVAL(MNXPVAL-MNFP)
         real*8         lambdatmp(0:NCMPS-1),strengthtmp(0:NCMPS-1)      
         real*8         lambdatmp1(0:NCMPS-1),strengthtmp1(0:NCMPS-1)
         real*8 evalfeat603

         KEY=INT(FFPVAL(3))
         THETA=FFPVAL(5)
         LINEIN=INT(FFPVAL(1))
         FSEL=INT(FFPVAL(9))
         ION=INT(FFPVAL(10))
         LOWER=INT(FFPVAL(11))
         UPPER=INT(FFPVAL(12)) 

       if (h.eq.0) stop 'h must be nonzero in dfridr'

       hh=h

         icompflag=1 

       a(1,1)=(func(x,B+hh,PH,CN,T,ncomps,icompflag,
     >   lambdatmp1,strengthtmp1,FFPVAL)-
     >   func(x,B-hh,PH,CN,T,ncomps,icompflag,
     >   lambdatmp1,strengthtmp1,FFPVAL))/(2.0*hh)
     
       err=BIG

       do i=2,NTAB
         hh=hh/CON
           
         a(1,i)=(func(x,B+hh,PH,CN,T,ncomps,icompflag,
     >    lambdatmp1,strengthtmp1,FFPVAL)-
     >              func(x,B-hh,PH,CN,T,ncomps,icompflag,
     >    lambdatmp1,strengthtmp1,FFPVAL))/(2.0*hh)
                    

         fac=CON2
           
         do j=2,i
                a(j,i)=(a(j-1,i)*fac-a(j-1,i-1))/(fac-1.0)
                fac=CON2*fac
                errt=max(abs(a(j,i)-a(j-1,i)),abs(a(j,i)-a(j-1,i-1)))
                
                if (errt.le.err) then
                        err=errt
                        dfridr=a(j,i)
                endif
          enddo
                
                if (abs(a(i,i)-a(i-1,i-1)).ge.SAFE*err) return
                
         enddo
             
         return
         
         END
