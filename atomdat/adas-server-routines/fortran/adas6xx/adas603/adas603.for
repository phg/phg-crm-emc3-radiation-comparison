C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas603/adas603.for,v 1.3 2004/07/06 11:02:18 whitefor Exp $ Date $Date: 2004/07/06 11:02:18 $
C
       implicit none            
C-----------------------------------------------------------------------
C
C ******************** FORTRAN77 PROGRAM: ADAS603 **********************
C
C NAME: ADAS603
C
C VERSION:  1.0
C
C PURPOSE:
C         FORTRAN interface routine to UNIX pipe.  
C
C PROGRAM:
C
C	  (I*4) fp(mnfp)  = array of flags indicating if Magnetic field, 
C				  overall peak height, centroid position & temperature
C				  are to be varied (1=yes, 0=no)
C	  (I*4) getfit  = array containing fitting parameters (dim. 6)
C                         getfit(1) - Allow variable line widths (1-y,0-n).
C                         getfit(2) - Fit different widths (1-y,0-n).
C                         getfit(3) - Linear component to background (1-y,0-n).
C                         getfit(4) - Quadratic component to background
C				     (1-y,0-n).
C                         getfit(5) - Weigthed fit (1-y,0-n).
C                         getfit(6) - No background.
C	  (I*4) ifail   = error flag for fitting routine (see routine xxder1).
C	  (I*4) ipvt    = see xxder1.
C	  (I*4) jdo     = Options choices vector.
C	  (I*4) jf      = No. of free parameters.
C	  (I*4) KB0	    = 1: fit background. 0: don't fit background (forces
C							    KB1, KB2 to 0)
C	  (I*4) KB1	    = 1: background has linear component. 0: no linear comp.
C	  (I*4) KB2	    = 1: background has quadratic component. 0: no quadratic comp.
C	  (I*4) kindep  = 0: vary linewidths independently 1: all lines have same width
C				2: no lines to be fitted - fit background only.
C	  (I*4) kwfree  = 1: linewidths to vary.  0: fixed linewidths.
C       (I*4) kwt     = 1 for weighted fit, =0 for unweighted  fit
C	  (I*4) mlwa	= Size of wa.
C	  (I*4) mnp	= Size of jdo, b, cj etc.
C	  (I*4) mnfp	= max no of free parameters for fitting special feature
C	  (I*4) mnxpval 	= size o array fpval
C	  (I*4) ndline	= Parameter, max. no. of ordinary lines.
C	  (I*4) ndtot	= max no. of grating positions
C	  (I*4) nfxl	= no. of lines with fixed positions.C
C	  (I*4) nsize	= actual no. of grating positions.
C	  (I*4) numline	= actual number of ordinary lines.
C
C	  (R*8) b	= work vector (see xxycf2).
C	  (R*8) b0	= fitted background base level. 
C	  (R*8) b1	= fitted linear component of background.
C	  (R*8) b2	= fitted quadratic component of background.
C	  (R*8) bch	= Vertical separation of initial background guesses.
C	  (R*8) c       = variance co-variance matrix (see xxycf2).
C	  (R*8) cj      = copy of c.
C	  (R*8) esef  see mlcds603.for
C	  (R*8) eclf  see mlcds603.for
C	  (R*8) eseh  see mlcds603.for
C	  (R*8) eclh  see mlcds603.for
C	  (R*8) esexo see mlcds603.for
C	  (R*8) eclxo see mlcds603.for
C	  (R*8) esewv see mlcds603.for
C	  (R*8) eclwv see mlcds603.for
C	  (R*8) eseb0 see mlcds603.for
C	  (R*8) eclb0 see mlcds603.for
C	  (R*8) eseb1 see mlcds603.for
C	  (R*8) eclb1 see mlcds603.for
C	  (R*8) eseb2 see mlcds603.for
C	  (R*8) eclb2 see mlcds603.for
C	  (R*8) esewl see mlcds603.for
C	  (R*8) eclwl see mlcds603.for
C	  (R*8) esefxp see mlcds603.for
C	  (R*8) eclfxp see mlcds603.for
C	  (R*8) fjac	= see xxder1.
C	  (R*8) flux  = calculated flux of ordinary lines.
C	  (R*8) fpval = array containing parameter values necessary for evaluating
C		  		feature envelope
C	  (R*8) fvec 	= see xxder1.
C	  (R*8) fxp = Final fitted values of Mag. field, overall peak height,
C			  centroid position and Temperature.
C	  (R*8) h   = Height of ordinary spectral lines.
C	  (R*8) rscs= Exposure in quarter secs. at each input data point. 
C	  (R*8) w   = Width of ordinary spectral lines.
C	  (R*8) wa	= see xxder1.
C	  (R*8) x   = x value at each data point
C	  (R*8) xg	= see xxder1.
C	  (R*8) xo  = Position of each ordinary line
C	  (R*8) y   = Input spectrum
C	  (R*8) ya  = Fitted spectrum.
C
C	  For the other parameters, see ml_gauss.for
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   ML_GAUSS603  ADAS	THEORETICAL SPECTRUM FITTING ROUTINE.
C        XX0000       ADAS    SET MACHINE DEPENDANT ADAS CONFIGURATION.
C	   XXFLSH       ADAS	FLUSHES UNIX PIPE.
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, DEC 1998
C		MODIFIED VERSION OF ADAS602.FOR
C
C VERSION:  1.1					DATE: 04-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION:  1.2					DATE: 14-10-99
C MODIFIED: RICHARD MARTIN
C		- ADDED SUPPORT FOR HYDROGENIC LINES.
C
C VERSION:  1.3					DATE: 15-09-2000
C MODIFIED: RICHARD MARTIN
C		- IMPROVED DOCUMENTATION
C
C
C-----------------------------------------------------------------------
       integer pipein, pipeou, ndtot, ndline, mnp, mlwa, nfxl
       integer MNFP,MNXPVAL
       parameter(pipein =5, pipeou=6, ndtot=2048, ndline=19)
       parameter(MNXPVAL=16,MNFP=4)
       parameter( mnp = 3+(3*ndline) )
       parameter (mlwa = 5*mnp+ndtot)
C-----------------------------------------------------------------------
C  MLWA estimated as for document XXDER1.  2048 is max no of grating
C  positions or pixels or wavelength steps
C-----------------------------------------------------------------------
       integer nsize,numline,getfit(6),i,ipvt(mnp),jdo(mnp),fp(MNFP)
       integer kindep, kwfree, kb0, kb1, kb2, kwt, jf, ifail
       real*8 fpval(MNXPVAL)
       real*8 bch, x(ndtot), y(ndtot), rscs(ndtot), ya(ndtot)
       real*8 xo(ndline),w(ndline),flux(ndline),esef(ndline)
       real*8 eclf(ndline), h(ndline), eseh(ndline)
       real*8 eclh(ndline),esexo(ndline),eclxo(ndline)
       real*8 esewv(ndline), eclwv(ndline)
       real*8 b0,b1,b2,eseb0,eseb1,eseb2,esewl
       real*8 eclb0,eclb1,eclb2,eclwl
       real*8 fvec(ndtot),fjac(ndtot,mnp)      
       real*8 xg(mnp),cj(mnp),b(mnp),c(mnp,mnp),wa(mlwa)
       real*8 FXP(MNFP),ESEFXP(MNFP),ECLFXP(MNFP)

C-----------------------------------------------------------------------
C   Set up for i4unit to go to screen, avoiding the pipe 
C-----------------------------------------------------------------------
       call xx0000
C-----------------------------------------------------------------------
C read the data from the pipe
C-----------------------------------------------------------------------
       read(pipein,1000)nsize
       do 10 i = 1,nsize
         read(pipein,1001)x(i),y(i),rscs(i)
 10    continue
       read(pipein,1002)numline
       read(pipein,1002)nfxl
       do 20 i = 1,numline
         read(pipein,1003)xo(i),w(i)
 20    continue
       read(pipein,1004)bch
       do 30 i = 1,6
         read(pipein,1005)getfit(i)
 30    continue
       kwfree = getfit(1)
C-----------------------------------------------------------------------
C altered order of getfit for consistent printout with IDL /DHB 10/05/96
C Added kb0 for no background DHB 28/10/96
C-----------------------------------------------------------------------
       kb1 = getfit(3)
       kb2 = getfit(4)
       kindep = getfit(2)
       kwt = getfit(5)
       kb0 = getfit(6)
       
       do 35 i=1,MNFP
         read(pipein,1005)fp(i)
 35    continue
       do 36 i=1,MNXPVAL
         read(pipein,1004)fpval(i)                	
 36    continue     

       call ml_gauss603( X,Y,NUMLINE,NFXL,MNP,MLWA,
     & XO,W,BCH,NSIZE,RSCS,KWFREE,KINDEP,KWT,KB0,KB1,KB2,
     & FP,FPVAL,YA,FLUX,ESEF,ECLF,B0,B1,B2,H,JF,
     & ESEB0,ESEB1,ESEB2,ESEWL,ECLB0,ECLB1,ECLB2,ECLWL,ESEH,
     & ECLH,ESEXO,ECLXO,ESEWV,ECLWV,FXP,ESEFXP,ECLFXP,IFAIL,FVEC,FJAC,
     & XG,CJ,JDO,B,C,IPVT,WA)
   
C-----------------------------------------------------------------------
C write results to the pipe
C-----------------------------------------------------------------------
	 do 40 i = 1,nsize
	   write(pipeou,1004)ya(i)	   
	   call xxflsh(pipeou)
 40	 continue
	 write(pipeou,1006)jf,ifail
	 call xxflsh(pipeou)
	 write(pipeou,1001)b0,b1,b2
	 call xxflsh(pipeou)
	 write(pipeou,1007)eseb0,eseb1,eseb2,esewl
       call xxflsh(pipeou)
       write(pipeou,1007)eclb0,eclb1,eclb2,eclwl
	call xxflsh(pipeou)
	 do 50 i = 1, numline
	   write(pipeou,1007)xo(i),h(i),w(i),flux(i)
	   call xxflsh(pipeou)
	   write(pipeou,1001)esexo(i),eseh(i),esef(i)
	   call xxflsh(pipeou)
	   write(pipeou,1001)eclxo(i),eclh(i),eclf(i)
	   call xxflsh(pipeou)
	   write(pipeou,1003)esewv(i),eclwv(i)
	   call xxflsh(pipeou)
 50    continue
 	 do 60 i=1,MNFP
 	   write(pipeou,1001)FXP(i),ESEFXP(i),ECLFXP(i)
 	   call xxflsh(pipeou)
 60	 continue
 	   
 1000  format(1x,i4)
 1001  format(1x,e13.6,1x,e13.6,1x,e13.6)
 1002  format(1x,i2)
 1003  format(1x,e13.6,1x,e13.6)
 1004  format(1x,e13.6)
 1005  format(1x,i1)
 1006  format(1x,i4,1x,i4)
 1007  format(1x,e13.6,1x,e13.6,1x,e13.6,1x,e13.6)
       stop
       end
