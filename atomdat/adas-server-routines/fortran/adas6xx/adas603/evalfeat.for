C UNIX-IDL - SCCS info: Module @(#)evalfeat.for	1.3 Date 10/14/99
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, DEC 1998
C
C VERSION:  1.1					DATE: 04-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C VERSION:	1.2					DATE: 19-03-99
C		RICHARD MARTIN
C		- CORRECTED TEMPERATURE CONVERSION ERROR.
C VERSION:	1.3					DATE: 13-10-99
C		RICHARD MARTIN
C		- INCLUDED SUPPORT FOR HYDROGENIC LINES
C
C VERSION : 1.4
C DATE    : 09-04-2010
C MODIFIED: Martin O'Mullane
C           - Change integer*4 to integer.
C
C-----------------------------------------------------------------------
	real*8 function evalfeat(xx,B,PH,CN,T,FFPVAL)
	
	implicit none

c	*******************************************************************
c	** IF CHANGE NCMPS THEN CHANGE C ROUTINE COMPONENTS2 ACCORDINGLY **
c	*******************************************************************  

	integer   i, linein, key, prof, ncomps,MNXPVAL,MNFP,NCMPS
	integer   fsel, ion, lower, upper
	PARAMETER(MNXPVAL=16,MNFP=4,NCMPS=3710)
	real*8 FFPVAL(MNXPVAL-MNFP) 
	real*8 feature,wavemin,scale
	real*8 B,PH,CN,T,THETA,line,gamma,xx,xxtmp
	real*8 M,c,k
	real*8 lambdatmp(0:NCMPS-1),strengthtmp(0:NCMPS-1)
	real*8 lambda(0:NCMPS-1),strength(0:NCMPS-1)	

	external components2,hdlikecomp2
	
	linein=int(FFPVAL(1))
	prof=int(FFPVAL(2))
	key=int(FFPVAL(3))
	gamma=FFPVAL(4)
	theta=FFPVAL(5)
	scale=FFPVAL(6)
	wavemin=FFPVAL(7)
	M=FFPVAL(8)*1.66057d-27
	fsel=FFPVAL(9)
	ion=int(FFPVAL(10))	
	lower=int(FFPVAL(11))
	upper=int(FFPVAL(12))

	c=2.99d+8
	k=1.346d-23

	xxtmp=xx
	xx=xx-CN
	
	ncomps=0
	feature=0.0d0
	
	if (fsel.eq.0) then
		call components2(key,%VAL(theta),%VAL(B),%VAL(linein),
     >		ncomps,lambdatmp,strengthtmp)
	else
		call hdlikecomp2(key,%VAL(theta),%VAL(B),
     >	  	%VAL(ion),%VAL(lower),%VAL(upper),
     >		ncomps,lambdatmp,strengthtmp)
	endif
	
	do i=0,ncomps-1
	
	   if (prof.eq.0) then
		if (lambdatmp(i).ne.0.0) then
			lambda(i)=(lambdatmp(i)-wavemin)*scale
			strength(i)=strengthtmp(i)
		endif
		line=dexp(-(M*c*c/(2*k*T)*((xx-lambda(i))/
     >		(lambda(i)+wavemin*scale))**2))*strength(i)
		feature=feature+line	   
	   
	   else
		if (lambdatmp(i).ne.0.0) then
			lambda(i)=(lambdatmp(i)-wavemin)*scale
			strength(i)=strengthtmp(i)
		endif
		line=(gamma**2/((xx-lambda(i))**2+gamma**2))
     >				*strength(i)
		feature=feature+line	
	   endif		
	enddo
	
	evalfeat=feature*PH
	xx=xxtmp
	
	return
	
	end
