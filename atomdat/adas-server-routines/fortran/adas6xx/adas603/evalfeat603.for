C UNIX-IDL - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas603/evalfeat603.for,v 1.5 2010/04/09 13:48:37 mog Exp $ Date $Date: 2010/04/09 13:48:37 $
C
C NAME:   EVALFEAT603
C
C PURPOSE: TO EVALUATE THE FEATURE ENVELOPE OF A ZEEMAN/PASCHENBACK MULTIPLET
C	     FEATURE AT A GIVEN INPUT GRID POINT.
C
C INPUTS:
C
C	(R*8) xx	   - Grating position
C	(R*8) B	   - Magnetic field
C	(R*8) PH	   - Overall peak height
C	(R*8) CN	   - Centroid position
C	(R*8) T	   - Temperature
C	(I*4) ncomps   - Actual no of zeeman components returned by C modules. 
C	(I*4) icompflag- 0= don't call C routines 1= call C routines 
C	(R*8) lambdatmp - Input wavelength of components (if icompflag=0)
C	(R*8) strengthtmp - Input strengths of components (if icompflag=0)
C	(R*8) ffpval   - Array of parameters required to evaluate feature envelope.
C
C PROGRAM:
C	
C	(I*4) fsel	   - 0= zeeman multiplet, 1= hydrogenic line
C	(I*4) ion	   - Index of ion for hydrogenic line case
C	(I*4) key	   - Polarisation index 
C			     0= all components, no angular dependence.
C			     1= all components, for given observation angle.
C			     2= Pi components, for given observation angle.
C			     3= Sigma+ components, for given observation angle.
C			     4= Sigma- components, for given observation angle.
C	(I*4) linein   - Iindex of selected line.
C	(I*4) lower    - Lower level index for hydrogenic transition.
C	(I*4) mnfp     - Max. no of free parameters in special feature
C	(I*4) mnxpval  - Size of array containing parameters nec. for evaluating feature.
C	(I*4) ncmps    - Max no zeeman components returned by C modules. 
C	(I*4) prof     - 0= Doppler broadening. 1= Lorentzian broadening.
C	(I*4) upper    - Upper level index for hydrogenic transition.
C
C 	(R*8) c	   - Speed of light.
C 	(R*8) feature  - Calculated value of feature envelope at xx.
C 	(R*8) gamma	   - Width of lorentzian line profile.
C 	(R*8) k	   - Boltzmann's constant
C 	(R*8) lambda   - Array containing wavelengths of zeeman componenets
C			     re-scaled into grating positions.
C 	(R*8) M	   - Atomic mass. 
C	(R*8) scale	   - Scale factor for converting wavelengths to grating positions.
C 	(R*8) strength - Strength of zeeman components
C 	(R*8) theta	   - Observation angle.
C	(R*8) wavemin  - Minimum of the wavelength range spanned by the input spectrum.
C
C ROUTINES:
C          ROUTINE        SOURCE    	BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	     components2	adas/xpaschen	(C routine) Returns zeeman multiplet components 
C	     hdlikecomp2	adas/xpaschen	(C routine) Returns multiplet components 
C									for hydrogenic transitions. 	
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, DEC 1998
C
C VERSION:  1.1					DATE: 04-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C VERSION:	1.2					DATE: 19-03-99
C		RICHARD MARTIN
C		- CORRECTED TEMPERATURE CONVERSION ERROR.
C VERSION:	1.3					DATE: 13-10-99
C		RICHARD MARTIN
C		- INCLUDED SUPPORT FOR HYDROGENIC LINES
C VERSION:	1.4					DATE: 15-09-2000
C		RICHARD MARTIN
C		- RENAMED EVALFEAT603 FROM EVALFEAT
C		- IMPROVED DOCUMENTATION
C		- CORRECTED MISTYPED BOLTZMANN CONSTANT
C
C VERSION : 1.5
C DATE    : 09-04-2010
C MODIFIED: Martin O'Mullane
C           - Change integer*4 to integer.
C
C-----------------------------------------------------------------------
	real*8 function evalfeat603(xx,B,PH,CN,T,ncomps,icompflag,
     >			lambdatmp,strengthtmp,FFPVAL)
	
	implicit none

c	*******************************************************************
c	** IF CHANGE NCMPS THEN CHANGE C ROUTINE COMPONENTS2 ACCORDINGLY **
c	*******************************************************************  

	integer   i, linein, key, prof, ncomps,MNXPVAL,MNFP,NCMPS
	integer   fsel, ion, lower, upper,icompflag
	PARAMETER(MNXPVAL=16,MNFP=4,NCMPS=3710)
	real*8 FFPVAL(MNXPVAL-MNFP) 
	real*8 feature,wavemin,scale,arg
	real*8 B,PH,CN,T,THETA,line,gamma,xx
	real*8 M,c,k
	real*8 lambdatmp(0:NCMPS-1),strengthtmp(0:NCMPS-1)
	real*8 lambda(0:NCMPS-1),strength(0:NCMPS-1)	

	external components2,hdlikecomp2
	
	linein=int(FFPVAL(1))
	prof=int(FFPVAL(2))
	key=int(FFPVAL(3))
	gamma=FFPVAL(4)
	theta=FFPVAL(5)
	scale=FFPVAL(6)
	wavemin=FFPVAL(7)
	M=FFPVAL(8)*1.66057d-27
	fsel=FFPVAL(9)
	ion=int(FFPVAL(10))	
	lower=int(FFPVAL(11))
	upper=int(FFPVAL(12))

	c=2.99d+8
	k=1.381d-23

	feature=0.0d0
	
	if (icompflag.eq.1) then
      	if (fsel.eq.0) then
			call components2(key,%VAL(theta),%VAL(B),%VAL(linein),
     >			ncomps,lambdatmp,strengthtmp)
      	else
			call hdlikecomp2(key,%VAL(theta),%VAL(B),
     >			%VAL(ion),%VAL(lower),%VAL(upper),
     >			ncomps,lambdatmp,strengthtmp)
      	endif
	endif

C-----------------------------------------------------------------------
C	Sum over Zeeman components to give value of feature envelope or its
C	derivative at point xx
C----------------------------------------------------------------------- 

	do i=0,ncomps-1	
	   if (prof.eq.0) then
		if (lambdatmp(i).ne.0.0) then
			lambda(i)=(lambdatmp(i)-wavemin)*scale
			strength(i)=strengthtmp(i)
		endif
		arg=M*c*c/(2*k*T)*((xx-CN-lambda(i))/
     >		(lambda(i)+wavemin*scale))**2
		line=dexp(-arg)*strength(i)

		feature=feature+line	   
	   
	   else
		if (lambdatmp(i).ne.0.0) then
			lambda(i)=(lambdatmp(i)-wavemin)*scale
			strength(i)=strengthtmp(i)
		endif
		line=(gamma**2/((xx-CN-lambda(i))**2+gamma**2))
     >				*strength(i)
		feature=feature+line	
	   endif		
	enddo
	
	evalfeat603=feature*PH
	
	return
	
	end
C
