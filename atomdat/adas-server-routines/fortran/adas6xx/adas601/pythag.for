CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/pythag.for,v 1.1 2004/07/06 14:38:23 whitefor Exp $ Date $Date: 2004/07/06 14:38:23 $
CX
      double precision function pythag(a,b)
      double precision a,b
C-----------------------------------------------------------------------
c ROUTINE: ADAS601
c PURPOSE: finds dsqrt(a**2+b**2) without overflow or destructive underflow
C-----------------------------------------------------------------------
c
C AUTHOR: ALESSANDRO LANZAFAME
C         UNIVERSITY OF STRATHCLYDE
C
C DATE: 29TH APRIL 1995
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      double precision p,r,s,t,u
      p = dmax1(dabs(a),dabs(b))
      if (p .eq. 0.0d0) go to 20
      r = (dmin1(dabs(a),dabs(b))/p)**2
   10 continue
         t = 4.0d0 + r
         if (t .eq. 4.0d0) go to 20
         s = r/t
         u = 1.0d0 + 2.0d0*s
         p = u*p
         r = (s/u)**2 * r
      go to 10
   20 pythag = p
      return
      end
