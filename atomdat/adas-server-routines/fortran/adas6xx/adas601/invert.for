CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/invert.for,v 1.4 2004/07/06 14:09:28 whitefor Exp $ Date $Date: 2004/07/06 14:09:28 $
CX
C
      SUBROUTINE INVERT( M      , NG   , T , DLAM, CO, NCAP7, GT,
     &                   H      , HMAX , FHAT,
     &                   NTEMP  , TEMP ,
     &                   G      , SG   , RL , SIG, IR1USS
     &                 )

C------------------------------------------------------------------------
c ROUTINE: adas601
c
c PURPOSE: not documented
c
C------------------------------------------------------------------------
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C           - MTRA -> 200
C
C VERSION: 1.3				DATE: 24-07-96
C MODIFIED: WILLIAM OSBORN
C           - REAL*8 Q -> REAL*8 Q(MTEMP)
C           - MTEMP ADDED TO PARAMETERS OF R1US00 FUNCTION SO THAT C AND
C             V CAN BE PROPERLY DIMENSIONED LATER ON
C
C VERSION: 1.4				DATE: 08-08-96
C MODIFIED: WILLIAM OSBORN
C           - ERROR CORRECTED: R1USS2->R1USS3 IF IR1USS==3
C
C-----------------------------------------------------------------------
      IMPLICIT NONE
C-----------------------------------------------------------------------
      INTEGER   MTRA   , MTEMP
      INTEGER   MABUND ,  MLEV 
      INTEGER   NP     ,  NCMAX
 
      PARAMETER ( MTRA=200  , MTEMP=101 )
      PARAMETER ( MABUND=30 , MLEV=110              )
      PARAMETER ( NP=8      , NCMAX=20              )
C-----------------------------------------------------------------------
      INTEGER   NG(MTRA)
      REAL*8    T(MTEMP,MTRA) , GT(MTEMP,MTRA)
      REAL*8    G(MTRA)       , SG(MTRA)
C-----------------------------------------------------------------------
      INTEGER   M , NTEMP
      REAL*8    TEMP(MTEMP)    , FHAT(MTEMP)
      REAL*8    H(MTRA,MTEMP) , HMAX      , SIG
      REAL*8    RL
C-----------------------------------------------------------------------
      INTEGER   NCAP7     
      REAL*8    DLAM(NCMAX) , CO(NCMAX)  
C-----------------------------------------------------------------------
      INTEGER   I, J , K, K1, K2, L
      REAL*8    C(MTEMP,MTEMP), V(MTEMP,MTEMP),
     &          W2(MTEMP)     , DL(MTEMP)     , X(MTEMP),
     &          W(MTEMP) 
      REAL*8    FV1(MTEMP)    , FV2(MTEMP)
      REAL*8    PR, PRIOR, Q(MTEMP)
C-----------------------------------------------------------------------
      INTEGER   IR1USS
C-----------------------------------------------------------------------

      EXTERNAL  R1SIC2,R1USS1,R1USS2,R1USS3
      
c-----------------------------------------------------------------------
c Multiply gt by the prior (phi_0 in Thompson notes) - the prior is
c extrapolated outside the range of the B-spline representation -
c to obtain the matrix H (eq 2.9) 
c-----------------------------------------------------------------------

      DO J=1,M
         DO I=1,NG(J)
            PR=PRIOR(T(I,J),DLAM,CO,NCAP7)
            GT(I,J)=GT(I,J)*PR
         ENDDO
      ENDDO

C#
      DO K=1,M,10
C         WRITE(40,*)
         K1=K
         K2=K+9
         DO L=1,NG(1)
C            WRITE(40,7000) 
C     &           T(L,1)/DLOG(10.D0), (GT(L,KK),KK=K1,K2)
         ENDDO
      ENDDO
C 7000 FORMAT(1X,F4.2,(10(1X,1PD14.4)))
C#
      DO I=1,M
         DO K=1,MTEMP
            X(K)=GT(K,I)
            W(K)=T(K,I)
            IF(X(K).NE.0.D0) X(K)=DLOG(X(K))
         ENDDO
         CALL HMAT(H,NTEMP,TEMP,W,X,I)
      ENDDO

c----------------------------------------------------------------------
c Divide H(i,j) by the weight (eq. 2.9) and normalise
c----------------------------------------------------------------------

      HMAX=0.D0
      DO I=1,M
         G(I)=G(I)/SG(I)
         DO J=1,NTEMP
            H(I,J)=H(I,J)/SG(I)
            HMAX=DMAX1(HMAX,H(I,J))
         ENDDO
      ENDDO
C
      DO I=1,M
         DO J=1,NTEMP
            H(I,J)=H(I,J)/HMAX 
         ENDDO
      ENDDO

c--------------------------------------------------------------------
c Inverts the data g=Hf+e to produce a reconstruction of the source 
c function fhat using the smoothing matrix provided by R1USC(=r1sic2)
c and the smoothing parameter provided by R1USS(=r1uss3). 
c fhat will contain the final reconstruction.
c--------------------------------------------------------------------

      RL=100.D0  
C     SIG=0.4D-3
      IF (IR1USS.EQ.1) THEN
         CALL R1US00( H     , MTRA  , C      , V      , MTEMP  , DL,
     &                G     , W2    , Q      , FHAT   , SIG    , RL,
     &                NTEMP , M     , R1SIC2 , R1USS1 , FV1 , FV2 )
      ELSE IF (IR1USS.EQ.2) THEN
         CALL R1US00( H     , MTRA  , C      , V      , MTEMP  , DL,
     &                G     , W2    , Q      , FHAT   , SIG    , RL,
     &                NTEMP , M     , R1SIC2 , R1USS2 , FV1 , FV2 )
      ELSE IF (IR1USS.EQ.3) THEN
         CALL R1US00( H     , MTRA  , C      , V      , MTEMP  , DL,
     &                G     , W2    , Q      , FHAT   , SIG    , RL,
     &                NTEMP , M     , R1SIC2 , R1USS3 , FV1 , FV2 )
      ELSE
         STOP '*** INVERT: R1USS UNDEFINED ***'
      ENDIF

      RETURN
      END
