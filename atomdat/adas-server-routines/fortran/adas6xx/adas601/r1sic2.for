CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1sic2.for,v 1.1 2004/07/06 14:39:52 whitefor Exp $ Date $Date: 2004/07/06 14:39:52 $
CX
      SUBROUTINE R1SIC2(C,IC,N)
C
C This subroutine sets the values of the smoothing matrix for the
C renormalised second order regularisation. This routine is compatible
C with R1SI00, R1OI00, R1US00
C
C------------------------------------------------------------------------
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(IC,*)
C
      Do 11 I=1,N
      Do 11 J=1,N
         C(i,j)=0.d0
 11   Continue
C
      Do 22 i=3,N
         C(i,i)=6.d0
         C(i,i-1)=-4.d0
         C(i-1,i)=-4.d0
         C(i-2,i)=1.d0
         C(i,i-2)=1.d0
 22   Continue
         C(1,1)=1.d0
         C(N,N)=1.d0
         C(1,2)=-2.d0
         C(2,1)=-2.d0
         C(N,N-1)=-2.d0
         C(N-1,N)=-2.d0
         C(2,2)=5.d0
         C(N-1,N-1)=5.d0
C
      RETURN
      END
