CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/f01sort.for,v 1.1 2004/07/06 13:50:51 whitefor Exp $ Date $Date: 2004/07/06 13:50:51 $
CX
C
      SUBROUTINE F01SORT(N,A,B,IRANK)
      IMPLICIT NONE
C
C PURPOSE: SORT REAL ARRAY FROM RANK TABLE
C
C INPUT:  (I*4)      N     = DIMENSION OF REAL ARRAY
C
C INPUT:  (R*8)      A()   = REAL ARRAY TO BE SORTED
C
C OUTPUT: (I*4) IRANK()    = RANK TABLE
C
C         (R*8)      B()   = WORK SPACE
C
C AUTHOR: Alessandro Lanzafame, University of Strathclyde
C
C DATE:   24 January 1996
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER  N     , IRANK(N), K, L
      REAL*8   A(N)  , B(N)
C-----------------------------------------------------------------------

      DO K = 1, N
         DO L = 1, N
	    IF (IRANK(L).EQ.K) B(K)=A(L)
         ENDDO
      ENDDO

      DO  K = 1, N
         A(K) = B(K)
      ENDDO

      RETURN
C-----------------------------------------------------------------------
      END
