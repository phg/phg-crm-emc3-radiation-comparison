CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/hmat.for,v 1.2 2004/07/06 14:05:51 whitefor Exp $ Date $Date: 2004/07/06 14:05:51 $
CX
C
      SUBROUTINE HMAT(H,NTEMP,T0,T,DA,K)
C
C------------------------------------------------------------------------
c NAME: hmat.f
c
c ROUTINE: adas601
c
c PURPOSE: NOT DOCUMENTED
c
C------------------------------------------------------------------------
C AUTHOR: ALESSANDRO LANZAFAME
C         UNIVERSITY OF STRATHCLYDE
C
C DATE: 29TH APRIL 1996
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C           - MTRA -> 200
C
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MTRA=200,MTEMP=101)
      DIMENSION H(MTRA,MTEMP),T0(MTEMP),T(MTEMP),DA(MTEMP)
 

c      EQUIVALENCE (TEMP,T0)
      
      DO 11 I=1,MTEMP
         IF(DA(I).NE.0.D0) GOTO 22
 11   CONTINUE
C
 22   NMIN=I
C
      DO 33 I=NMIN,MTEMP
         IF(DA(I).EQ.0.D0) GOTO 44
 33   CONTINUE
 44   NMAX=I-1
C
      DO 55 I=1,MTEMP
         H(K,I)=0.D0
 55   CONTINUE
C
      TMIN=T(NMIN)
      TLOW=TMIN
      HLOW=DA(NMIN)
      IUP=NMIN+1
C     
      DO 66 J=1,NTEMP
C     PRINT *,T0(J)
         IF(T0(J).LE.TLOW) GOTO 66
C
 77      IF(IUP.GT.NMAX) RETURN
C
         IF(T0(J).GT.T(IUP)) THEN
            HUP=DA(IUP)
            TUP=T(IUP)
            ALP=(HUP-HLOW)/(TUP-TLOW)
            IUP=IUP+1
            IF(ALP.NE.0.D0) THEN
               H(K,J)=H(K,J)+(DEXP(HUP)-DEXP(HLOW))/ALP
            ELSE
               H(K,J)=H(K,J)+(DEXP(HUP)*TUP-DEXP(HLOW)*TLOW)
            END IF
            TLOW=TUP
            HLOW=HUP
            GOTO 77
         ELSE IF(T0(J).EQ.T(IUP)) THEN
            HUP=DA(IUP)
            TUP=T(IUP)
            ALP=(HUP-HLOW)/(TUP-TLOW)
            IUP=IUP+1
            IF(ALP.NE.0.D0)THEN
               H(K,J)=H(K,J)+(DEXP(HUP)-DEXP(HLOW))/ALP
            ELSE
               H(K,J)=H(K,J)+(DEXP(HUP)*TUP-DEXP(HLOW)*TLOW)
            END IF
            TLOW=TUP
            HLOW=HUP
         ELSE
            TUP=T0(J)
            HUP=DA(IUP-1)+(DA(IUP)-DA(IUP-1))*(TUP-T(IUP-1))/(T(IUP)-
     1           T(IUP-1))
            ALP=(HUP-HLOW)/(TUP-TLOW)
            IF(ALP.NE.0.D0)THEN
               H(K,J)=H(K,J)+(DEXP(HUP)-DEXP(HLOW))/ALP
            ELSE
               H(K,J)=H(K,J)+(DEXP(HUP)*TUP-DEXP(HLOW)*TLOW)
            END IF
            TLOW=TUP
            HLOW=HUP
         END IF
 66   CONTINUE
C
      RETURN
      END
