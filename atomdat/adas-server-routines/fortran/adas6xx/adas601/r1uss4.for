CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1uss4.for,v 1.1 2004/07/06 14:40:21 whitefor Exp $ Date $Date: 2004/07/06 14:40:21 $
CX
      SUBROUTINE R1USS4(q,w2,g2,l,sig,n,m,lmax,lmin,acc,a)
C
C This is subroutine finds the value of the smoothing parameter l for which
C the residual sum of squares function equals the noise variance. This routine
C is for use with R1US00
C
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
      Implicit Double Precision (A-H,L,O-Z)
      Dimension q(*),W2(*)
C
C Check that the value of lmax is appropriate
C
      llmax=lmax
      llmin=0.d0
      If(lmax.lt.0.d0) then
          Print *,' the largest smoothing parameter is < 0'
          Print *,'program terminated'
          STOP
      Else
  11      CHI=R1USB0(q,w2,g2,llmax,a,n)/M
          if(CHI.lt.sig) then
             llmin=llmax
             llmax=llmax*10.d0
             goto 11
           END IF
      END IF
C
C Check that the value of lmin is appropriate
C
C
C locate the smoothing parameter
C
 22   l=(llmin+llmax)*0.5
      CHI=R1USB0(q,w2,g2,l,a,n)/M
      if(CHI.gt.sig) llmax=l
      if(CHI.lt.sig) llmin=l
      if(abs(1.d0-llmin/llmax).gt.acc) goto 22
C
      return
      end
