CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1ust1.for,v 1.1 2004/07/06 14:40:32 whitefor Exp $ Date $Date: 2004/07/06 14:40:32 $
CX
      Double Precision Function R1UST1(q,l,a,n,m)
C
C This is function calculates the value of the equivalent degrees of freedom
C given the eigenvalues of the Q matrix q and smoothing parameter l for an
C n*n inverse problem. This routine is designed to be used by R1US00
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      Implicit Double Precision (A-H,L,O-Z)
      Dimension q(*)
C
C
      R1UST1=0.d0
C 
      Do 11 i=1,n
          R1UST1=(q(i)/(q(i)+l*(1.d0-a*q(i))))**2+R1UST1
 11   Continue
      R1UST1=M-R1UST1
C
      return
      end
