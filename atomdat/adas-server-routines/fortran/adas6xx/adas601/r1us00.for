CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1us00.for,v 1.3 2004/07/06 14:40:05 whitefor Exp $ Date $Date: 2004/07/06 14:40:05 $
CX
C
      SUBROUTINE R1US00( H     , IH    , A   , V , IA , DL, G , W2 , Q ,
     &                   FHAT  , SIG   , RL  , N , M ,
     &                   R1USC , R1USS , FV1 , FV2
     &                 )
      implicit double precision (a-h,o-z)
C
C-----------------------------------------------------------------------
C
C PURPOSE: Inverts the data g=Hf+e to produce a reconstruction
C          of the source function fhat using the smoothing matrix 
C          provided by R1USC and the smoothing parameter provided 
C          by R1USS.
C 
C          A denotes the smoothing matrix, V is the orthonormal 
C          matrix which diagonalises the L(-1)C L(-T) matrix,
C          Q are the eigenvalues of that matrix, W are the spectral 
C          components of L(-1) H(T) g. M is the number of data points, 
C          N recovery points.
C 
C          IH and IA are the first dimensions of the 
C          H, A, and V matrices and must be > N. All matrices must be 
C          at least N*N and all vectors must at least of length N except 
C          H and g which must be of dimensions M*N and M respectively. 
C
C          This routine is designed for the case in which H transpose
C          H is singular.
C
C CALLING PROGRAM: ADAS5XX (DIFFERENTIAL EMISSION MEASURE ANALYSIS)
C
C
C INPUT:  (R*8)  H(,)     = KERNEL MATRIX
C
C INPUT:  (I*4)  IH       = FIRST DIMENSION OF H (MAX NUMBER OF 
C                           TEMPERATURE DATA POINTS AND RECOVERY POINTS
C
C INPUT:  (I*4)  IA       = FIRST DIMENSION OF A AND V MATRICES
C
C INPUT:  (R*8)  G()      = DATA POINTS
C
C INPUT:  (R*8)  SIG      = ESTIMATE OF THE STANDARD ERROR IN THE DATA
C
C INPUT:  (R*8)  RL       = ESTIMATE OF THE SMOOTHING PARAMETER
C
C INPUT:  (I*4)  N        = NUMBER OF RECOVERY POINTS
C
C INPUT:  (I*4)  M        = NUMBER OF DATA POINTS
C
C INPUT:         R1USC    = NAME OF THE ROUTINE PROVIDING THE SMOOTHING
C                           MATRIX
C
C INPUT:         R1USS    = NAME OF THE ROUTINE PROVIDING THE SMOOTHING
C                           PARAMETER
C
C OUTPUT: (R*8)  SIG      = STANDARD ERROR
C
C OUTPUT: (R*8)  RL       = SMOOTHING PARAMETER
C
C OUTPUT: (R*8)  FHAT()   = RECONSTRUCTED FUNCTION AT RECOVERY POINTS
C
C         (R*8)  A(,)     = HTH = H TRANSPOSE * H
C
C         (R*8)  ALPHA    = WEIGHT TO ADD HTH TO C TO PRODUCE AN
C                           INVERTIBLE MATRIX
C
C         (R*8)  V(,)     = INVERTIBLE MATRIX: IS THE ORHONORMAL
C                           MATRIX WHICH DIAGONALISES L(-1)
C
C         (R*8)  W2()     = V(I+Rl*Q)*V(T)*L(-1)*H(T)*g
C
C
C AUTHOR: Alan Thompson, Glasgow University.
C
C VERSION 1.2                                   19 January 1995 
C MODIFICATION: Alessandro Lanzafame, University of Strathclyde
C               Substituted NAG routines with EISPACK routines:
C               F01AEF -> reduc
C               F02ABF -> rs .
C               Added more comments and uniformed layout with
C               ADAS style.
C
C************************************************************************
C UNDER S.C.C.S:
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER S.C.C.S CONTROL
C
C VERSION: 1.2				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C           - REMOVED INCLUDE STATEMENT
C
C VERSION: 1.3				DATE: 24-07-96
C MODIFIED: WILLIAM OSBORN
C           - ADDED IA PARAMETER FOR CORRECT DIMENSIONING OF ARRAYS
C
C-----------------------------------------------------------------------
      PARAMETER(ACC=1.D-4)
      DIMENSION H(IH,*),A(IA,*),V(IA,*),G(*),W2(*),Q(*),FHAT(*),DL(*)
      DIMENSION FV1(*) ,FV2(*)
C-----------------------------------------------------------------------
      EXTERNAL R1USC,R1USS
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C get the apropriate smoothing matrix
C-----------------------------------------------------------------------

      CALL R1USC(V,IA,N)

C-----------------------------------------------------------------------
C calculate HTH= H transpose * H
C-----------------------------------------------------------------------

      DO I=1,N
         DO J=1,N
            A(I,J)=0.D0
            DO K=1,M
               A(I,J)=A(I,J)+H(K,I)*H(K,J)
            ENDDO
         ENDDO
      ENDDO

C-----------------------------------------------------------------------
C calculate a weight to add HTH to C to produce an invertible matrix
C-----------------------------------------------------------------------

      ALPHA=0.D0
      DO I=1,N
         ALPHA=DMAX1(ALPHA,A(I,I))
      ENDDO
      ALPHA=1.D0/ALPHA*1.D-2

      DO I=1,N
         DO J=1,N
            V(I,J)=V(I,J)+ALPHA*A(I,J)
         ENDDO
      ENDDO

C-----------------------------------------------------------------------
C calculate HTG = H transpose g
C-----------------------------------------------------------------------

      DO I=1,N
         FHAT(I)=0.D0
         DO J=1,M
            FHAT(I)=FHAT(I)+H(J,I)*G(J)
         ENDDO
      ENDDO

C-----------------------------------------------------------------------
C Calculate g**2
C-----------------------------------------------------------------------

      G2=0.D0
      DO J=1,M
         G2=G2+G(J)**2
      ENDDO

C-----------------------------------------------------------------------
C use Choelsky's decomposition to find LL transpose decomposition of
C HTH and calculate L-1 HTH L-transpose
C-----------------------------------------------------------------------

C 19 JANUARY 1995 MODIFICATION BEGIN:

      IFAIL=0

c      CALL F01AEF(N,A,N,V,N,DL,IFAIL)

      CALL REDUC(IA,N,A,V,DL,IFAIL)
      IF (IFAIL .NE. 0) THEN
         WRITE(0,*) '*** ERROR REDUC: IFAIL =',IFAIL
         STOP
      ENDIF

C 19 JANUARY 1995 MODIFICATION END


C-----------------------------------------------------------------------
C calculate 1 / lii the diagonal elements of L stored in DL
C-----------------------------------------------------------------------

      DO 33 I=1,N
         DL(I)=1.D0/DL(I)
 33   CONTINUE

C-----------------------------------------------------------------------
C use back substituion to calculate L-1 HTg
C-----------------------------------------------------------------------

      W2(1)=FHAT(1)*DL(1)
      DO I=2,N
         W2(I)=0.D0
         DO J=1,I-1
            W2(I)=W2(I)+V(I,J)*W2(J)
            A(J,I)=V(I,J)
         ENDDO
         W2(I)=DL(I)*(FHAT(I)-W2(I))
      ENDDO

C-----------------------------------------------------------------------
C calculate the eigenvalues and eigenvectors of L-1CL-T
C-----------------------------------------------------------------------

C 19 JANUARY 1995 MODIFICATION BEGIN:

      IFAIL=0

c      CALL F02ABF(A,N,N,Q,V,N,FHAT,IFAIL)

      CALL RS(IA,N,A,Q,1,V,FV1,FV2,IFAIL)
      IF (IFAIL .NE. 0) THEN
         WRITE(0,*)'*** ERROR RS: IFAIL = ',IFAIL
         STOP
      ENDIF

C-----------------------------------------------------------------------
C calculate the spectral components VT * L-1 * HT * g
C-----------------------------------------------------------------------

      DO I=1,N
         FHAT(I)=0.D0
         DO J=1,N
            FHAT(I)=FHAT(I)+V(J,I)*W2(J)
         ENDDO
      ENDDO

C-----------------------------------------------------------------------
C calculate the square of the spectral weight
C-----------------------------------------------------------------------

      DO I=1,N
         W2(I)=FHAT(I)**2
      ENDDO

C-----------------------------------------------------------------------
C calculate the value of the smoothing parameter Rl
C-----------------------------------------------------------------------

      RLMAX=RL
      IF(RL.LE.0.D0) RLMAX=1.D0
C     RLMAX=Q(N/2)/(1+ALPHA*Q(N/2))
      RLMIN=RLMAX*1.D-10
C

 771  CALL R1USS(Q,W2,G2,RL,SIG,N,M,RLMAX,RLMIN,ACC,ALPHA)
      IF(RL.EQ.-1.D0) THEN
         RLMAX=RLMAX*10.D0
         GOTO 771
      END IF

C-----------------------------------------------------------------------
C reconstruct the source function
C-----------------------------------------------------------------------

      IF(RL.EQ.0.D0) THEN
         WRITE(0,*) '*** R1US00: NO SATISFACTORY SOLUTION OBTAINED. ***'
C# may05-96 modification begin:
C#         RETURN
         stop
C# may05-96 modification end
      END IF

C-----------------------------------------------------------------------
C calculate V(I+Rl*Q)*V(T)*L(-1)*H(T)*g
C-----------------------------------------------------------------------

      DO I=1,N
         W2(I)=0.D0
         DO J=1,N
            W2(I)=W2(I)+V(I,J)/((1.D0-ALPHA*RL)*Q(J)+RL)*FHAT(J)
         ENDDO
      ENDDO

C-----------------------------------------------------------------------
C  back substitute LT to obtain Fhat the final reconstuction
C-----------------------------------------------------------------------

      FHAT(N)=W2(N)*DL(N)
      DO I=N-1,1,-1
         FHAT(I)=0.D0
         DO J=I+1,N
            FHAT(I)=FHAT(I)+A(I,J)*FHAT(J)
         ENDDO
         FHAT(I)=(W2(I)-FHAT(I))*DL(I)
      ENDDO

C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
      END
