CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1sic1.for,v 1.1 2004/07/06 14:39:49 whitefor Exp $ Date $Date: 2004/07/06 14:39:49 $
CX
      SUBROUTINE R1SIC1(C,IC,N)
C
C This subroutine sets the values of the smoothing matrix for the
C renormalised first order regularisation. This subroutine is compatible
C with R1SI00, R1OI0, R1US00
C
C------------------------------------------------------------------------
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(IC,*)
C
      Do 11 I=1,N
      Do 11 J=1,N
         C(i,j)=0
 11   Continue
C
      Do 22 i=2,N
         C(i,i)=2.d0
         C(i,i-1)=-1.d0
         C(i-1,i)=-1.d0
 22   Continue
         C(1,1)=1.d0
         C(N,N)=1.d0
C
      RETURN
      END
