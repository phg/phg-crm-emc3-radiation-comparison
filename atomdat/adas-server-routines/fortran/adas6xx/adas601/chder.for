CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/chder.for,v 1.2 2004/07/06 12:04:27 whitefor Exp $ Date $Date: 2004/07/06 12:04:27 $
CX
C
      SUBROUTINE CHDER(A,B,C,CDER,N)
C
C-----------------------------------------------------------------------
C
C PURPOSE: COMPUTE THE CHEBYSHEV COEFFICIENTS OF THE DERIVATIVE OF
C          A FUNCTION FROM ITS CHEBYSHEV COEFFICIENTS.
C          REFERENCES: Press, H. W. et al "Numerical Recipes", 1992
C
C CALLING PROGRAMS: VARIOUS
C
C
C INPUT:  (R*8)     A   = Lower limit of the abscissa interval
C
C INPUT:  (R*8)     B   = Upper limit of the abscissa interval
C
C INPUT:  (R*8)     C() = Array of Chebyshev coefficients
C
C OUTPUT: (R*8)  CDER() = Array of Chebishev coefficients of the 
C                         derivative   
C
C INPUT:  (I*4)     N   = Order of the series (maximum degree of 
C                         polynomial expansion = M-1)
C
C AUTHOR:  ALESSANDRO LANZAFAME
C          Strathclyde University
C
C DATE:    29-04-96
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 12-02-97
C MODIFIED: Alessandro Lanzafame
C	    - CORRECTION ON AUTHORSHIP
C
C-----------------------------------------------------------------------
      INTEGER N
      REAL*8  A , B , C(N) , CDER(N)
C-----------------------------------------------------------------------
      INTEGER J
      REAL*8  CON
C-----------------------------------------------------------------------

      CDER(N)   = 0.0D0
      CDER(N-1) = 2*(N-1)*C(N)
      IF (N .GE. 3) THEN
         DO J=N-2,1,-1
            CDER(J) = CDER(J+2) + 2*J*C(J+1)
         ENDDO
      ENDIF

      CON = 2. / (B-A)

      DO J = 1, N
         CDER(J) = CDER(J)*CON
      ENDDO

      RETURN
C-----------------------------------------------------------------------
      END
