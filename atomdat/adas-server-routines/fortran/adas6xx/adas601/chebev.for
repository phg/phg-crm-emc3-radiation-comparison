CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/chebev.for,v 1.2 2004/07/06 12:04:37 whitefor Exp $ Date $Date: 2004/07/06 12:04:37 $
CX
C
      FUNCTION CHEBEV( A, B, C, M, X)
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C PURPOSE: Chebyshev evaluation
C          REFERENCES: Press, H. W. et al "Numerical Recipes", 1992
C
C CALLING PROGRAMS: VARIOUS
C
C
C INPUT: (R*8)     A   = Lower limit of the abscissa interval
C
C INPUT: (R*8)     B   = Upper limit of the abscissa interval
C
C INPUT: (R*8)     C() = Array of Chebyshev coefficients
C
C INPUT: (I*4)     M   = Order of the series (maximum degree of 
C                        polynomial expansion = M-1)
C
C INPUT: (R*8)     X   = Abscissa
C
C
C        (I*4)     J   = GENERAL INETGER
C        (R*8)     D   = GENERAL REAL
C        (R*8)    DD   = GENERAL REAL
C        (R*8)    SV   = GENERAL REAL
C        (R*8)     Y   = GENERAL REAL
C        (R*8)    Y2   = GENERAL REAL
C
C AUTHOR: Alessandro C. Lanzafame, University of Strathclyde
C
C DATE: 16 January 1995
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 12 February 1997
C MODIFIED: ALESSANDRO LANZAFAME (UNIVERSITA' DI CATANIA)
C	    - INCLUDED DESCRIPTION OF INPUT VARIABLE X IN HEADER
C
C-----------------------------------------------------------------------
      INTEGER M
      REAL*8 CHEBEV, A, B, X, C(M)
C-----------------------------------------------------------------------
      INTEGER J
      REAL*8 D, DD, SV, Y, Y2
C-----------------------------------------------------------------------
      IF ( (X-A)*(X-B) .GT. 0.0D0 ) THEN
         STOP '*** ERROR CHEBEV: X NOT IN RANGE ***'
      ENDIF
C-----------------------------------------------------------------------
      D  = 0.0D0
      DD = 0.0D0
C-----------------------------------------------------------------------
C Change of variable
C-----------------------------------------------------------------------
      Y  = (2.0D0*X-A-B)/(B-A)
      Y2 = 2.0D0*Y
C-----------------------------------------------------------------------
C Clenshaw's recurrence
C-----------------------------------------------------------------------
      DO J=M,2,-1
         SV = D
         D  = Y2*D-DD+C(J)
         DD = SV
      ENDDO
C-----------------------------------------------------------------------
C Last step: note that the first and last terms of the summation
C must be halved
C-----------------------------------------------------------------------
      CHEBEV = Y*D-DD + 0.5*C(1)
C-----------------------------------------------------------------------
      RETURN
C-----------------------------------------------------------------------
      END
