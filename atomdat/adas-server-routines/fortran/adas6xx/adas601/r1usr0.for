CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1usr0.for,v 1.1 2004/07/06 14:40:11 whitefor Exp $ Date $Date: 2004/07/06 14:40:11 $
CX
      Double Precision Function R1USR0(q,w2,g2,l,a,n)
C
C This is function calculates the value of the residual sum of squares
C given the spectral components squared W2 and the eigenvalues of the
C Q matrix q and smoothing parameter l for an n*n inverse problem.
C this routine is used with R1US00
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      Implicit Double Precision (A-H,L,O-Z)
      Dimension q(*),w2(*)
C
C
      R1USR0=0.d0
C 
      Do 11 i=1,n
          R1USR0=(q(I)+2.d0*l*(1.d0-a*q(i)))/(q(i)*(1.d0-a*l)+l)**2*
     +         W2(i)+R1USR0
 11   Continue
      R1USR0=G2-R1USR0
C
      return
      end
