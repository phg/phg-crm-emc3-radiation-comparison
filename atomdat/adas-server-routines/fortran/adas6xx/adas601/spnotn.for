CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/spnotn.for,v 1.1 2004/07/06 15:22:25 whitefor Exp $ Date $Date: 2004/07/06 15:22:25 $
CX
      SUBROUTINE SPNOTN(ICNTRL,SPEC,ELEM,IZN,IZI,IZS,SEQSY,IZOMIN,
     &       FION,    LFION    )
C---------------------------------------------------------------------
C     This sub-routine may be entered in two ways depending on the
C     value given to ICNTRL thus:
C     1. Given its symbol in spectroscopic notation SPEC this
C     subroutine determines the nuclear and ion charges IZN, IZI as
C     well as the isoelectronic sequence SEQSY of the identified ion.
C     2. Given the ion sequence symbol SEQSY (eg Li-like) and the
C     chemical symbol of the selected element ELEM this sub-routine
C     determines the nuclear charge IZN, the ion charge IZI, the
C     spectroscopic number IZS=IZI-1 and the spectroscopic symbol of
C     the ion SPEC.
C---------------------------------------------------------------------
C     Written by Peter McWhirter, 13 Park Crescent, Abingdon, U.K.
C     Date: original 1992 May - November.
C---------------------------------------------------------------------
C            (C*11)   SPEC     Ion in spectroscopic notation.
C            (C*2)    ELEM     Symbol of the selected element.
C            (C*8)    SPNUM    Roman numeral in spectroscopic notation.
C            (I*4)    IZN      Nuclear charge of selected ion.
C            (I*4)    IZS      Arabic numeral corresponding to the
C                              Roman numeral in spectroscopic symbol.
C            (C*2)    SYMBL()  List of the symbols of the 92 elements.
C            (I*4)    LT       The tens digit of IZS.
C            (I*4)    LU       The units digit of IZS.
C            (C*8)    RNUM     Roman numerals up to a hundred.
C            (C*4)    SPECU()  Roman numerals less than ten.
C            (C*4)    SPECT()  Roman digits between ten and a hundred.
C            (I*4)    ISPEC()  Number of symbols in Roman numerals.
C            (I*4)    IZI      Ion charge of selected ion.
C            (I*4)    ISEQ     Atomic number of the first element of
C                              the isoelectronic sequence.
C            (C*7)    SEQSY    Identification of the isoelectronic
C                              sequence eg `Li-like'.
C            (I*4)    ICNTRL   Index controlling the entry point.
C            (C*2)    SEQS     Character string containing the first
C                              one or two letters of SEQSY.
C            (C*2)    CHR      Character string containing IZS.
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C---------------------------------------------------------------------
      IMPLICIT REAL*8(A-H,O-Z)
C---------------------------------------------------------------------
C
      CHARACTER*11   SPEC
      CHARACTER*2    ELEM,    SYMBL(92), SEQS    , CHR
      CHARACTER*8    SPNUM,   RNUM
      CHARACTER*4    SPEC1,   SPEC2    , SPECU(9), SPECT(9), FION
      CHARACTER*7    SEQSY
C---------------------------------------------------------------------
      DIMENSION      ISPEC(9)
C---------------------------------------------------------------------
      DATA SYMBL/'H ','HE','LI','BE','B ','C ','N ','O ',
     & 'F ','NE','NA','MG','AL','SI','P ','S ','CL','AR',
     & 'K ','CA','SC','TI','V ','CR','MN','FE','CO','NI',
     & 'CU','ZN','GA','GE','AS','SE','BR','KR','RB','SR',
     & 'Y ','ZR','NB','MO','TE','RU','RH','PD','AG','CD',
     & 'IN','SN','SB','TE','I ','XE','CS','BA','LA','CE',
     & 'PR','ND','PM','SM','EU','GD','TB','DY','HO','ER',
     & 'TM','YB','LU','HF','TA','W ','RE','OS','IR','PT',
     & 'AU','HG','TL','PB','BI','PO','AT','RN','FA','RA',
     & 'AC','TH','PA','U'/
      DATA SPECU/'I','II','III','IV','V','VI','VII','VIII','IX'/
      DATA SPECT/'X','XX','XXX','XL','L','LX','LXX','LXXX','XC'/
      DATA ISPEC/1,2,3,2,1,2,3,4,2/
C----------------------------------------------------------------------
      IF(ICNTRL.EQ.1)THEN
C----------------------------------------------------------------------
C       Determine the nuclear charge of the selected element and
C       the charge of the selected ion given its spectroscopic symbol.
C----------------------------------------------------------------------
C
         IF(SPEC(2:2).EQ.' ')THEN
            ELEM=SPEC(1:1)
            SPNUM=SPEC(3:11)
         ELSE
            ELEM=SPEC(1:2)
            SPNUM=SPEC(4:11)
         ENDIF
         IZN=0
         IZS=0
C      WRITE(12,*)'ELEM= ',ELEM
         DO 1 L=1,92
            IF(L.LT.10)THEN
               ILL=ISPEC(L)
               RNUM=SPECU(L)(1:ILL)
C      WRITE(12,*)RNUM
            ELSE
               LT=L/10
               LU=L-LT*10
C      WRITE(12,*)'LT= ',LT,'LU= ',LU
               IF(LU.EQ.0)THEN
                  ITT=ISPEC(LT)
                  SPEC1=SPECT(LT)(1:ITT)
                  RNUM=SPEC1
               ELSE
                  ITT=ISPEC(LT)
                  IUU=ISPEC(LU)
                  READ(SPECT(LT),1000)SPEC1
                  READ(SPECU(LU),1000)SPEC2
                  RNUM=SPEC1(1:ITT)//SPEC2(1:IUU)
C      WRITE(12,*)'ISPEC(LT)= ',ISPEC(LT),'ISPEC(LU)= ',ISPEC(LU)
               ENDIF
            ENDIF
1000  FORMAT(1A4)
C      WRITE(12,*)'L= ',L,'RNUM= ',RNUM
            IF(SPNUM.EQ.RNUM)IZS=L
            IF(SYMBL(L).EQ.ELEM)THEN
               IZN=L
               GOTO 2
            ENDIF
1        CONTINUE
2        IF(L.GT.92)THEN
cx            WRITE(6,1001)ELEM
            ICNTRL=-1
            RETURN
         ENDIF
         IF(IZN.LT.IZOMIN) THEN
cx            WRITE(6,1005)ELEM,IZOMIN
            ICNTRL=-1
            RETURN
         ENDIF
         IZI=IZS-1
C---------------------------------------------------------------------
C     Name the isoelectronic sequence eg Li-like.
C---------------------------------------------------------------------
         ISEQ=IZN-IZI
         SEQSY=SYMBL(ISEQ)//'-like'
C      WRITE(6,*)'IZN= ',IZN,'  IZI= ',IZI,'  SEQSY= ',SEQSY
cx      WRITE(6,*)' IZN  IZI    SEQSY '
cx      WRITE(6,'(2(i5),4x,a7)') IZN, IZI, SEQSY
C---------------------------------------------------------------------
C    To form a character string consisting of the element symbol and
C    the ion charge with no blank spaces.
C---------------------------------------------------------------------
         WRITE(CHR,1003)IZI
         FION=ELEM//CHR
         IF(FION(2:3).EQ.'  ')THEN
            FION(1:2)=FION(1:1)//FION(4:4)
            LFION=2
         ELSEIF(FION(2:2).EQ.' ')THEN
            FION(1:3)=FION(1:1)//FION(3:4)
            LFION=3
         ELSEIF(FION(3:3).EQ.' ')THEN
            FION(1:3)=FION(1:2)//FION(4:4)
            LFION=3
         ELSE
            LFION=4
         ENDIF
C         WRITE(12,*)'FION= ',FION,'FION(1:LFION)= ',FION(1:LFION)
C---------------------------------------------------------------------
C     Alternative entry point to the sub-routine.
C     Given the ion sequence symbol and the chemical symbol of the
C     selected element find the nuclear and ion charges etc.
C---------------------------------------------------------------------
      ELSEIF(ICNTRL.EQ.2)THEN
         LN=LEN(ELEM)
         IF(LN.EQ.1) ELEM(2:2)=' '
         DO 5 IS=1,92
            IF(ELEM.EQ.SYMBL(IS))THEN
                IZN=IS
                GO TO 7
            ENDIF
5        CONTINUE
cx            WRITE(6,1001)ELEM
            ICNTRL=-1
            RETURN
7        IDX=INDEX(SEQSY,'-')
cx         WRITE(6,*)'  IDX=',IDX
         IF(IDX.LE.2)THEN
             SEQS=SEQSY(1:IDX-1)//' '
         ELSE
             SEQS=SEQSY(1:2)
         ENDIF
cx         WRITE(6,*)'  SEQS=',SEQS,'/'
         DO 6 IS=1,92
            IF(SEQS.EQ.SYMBL(IS))ISEQ=IS
6        CONTINUE
cx         WRITE(6,*)'  ISEQ=',ISEQ
         IZI=IZN-ISEQ
         IZS=IZI+1
C---------------------------------------------------------------------
C     Discover the spectroscopic symbol of the selected ion SPEC.
C---------------------------------------------------------------------
         WRITE(CHR,1003)IZS
         IF(IZS.LT.10)THEN
            READ(CHR(2:2),1004)IU
            SPEC1=SPECU(IU)
            IUU=ISPEC(IU)
            SPEC=ELEM//' '//SPEC1(1:IUU)
         ELSEIF(IZS.GT.9)THEN
            READ(CHR(1:1),1004)IT
            READ(CHR(2:2),1004)IU
            SPEC1=SPECT(IT)
            ITT=ISPEC(IT)
            IF(IU.GT.0)THEN
                SPEC2=SPECU(IU)
                IUU=ISPEC(IU)
                SPEC=ELEM//' '//SPEC1(1:ITT)//SPEC2(1:IUU)
            ELSE
                SPEC=ELEM//' '//SPEC1(1:ITT)
            ENDIF
         ENDIF
      ENDIF
      RETURN
1001  FORMAT(1H ,'The symbol ',1A2,' entered for the ion/element has no
     &t been recognised.')
1002  FORMAT(1A2)
1003  FORMAT(1I2)
1004  FORMAT(1I1)
1005  FORMAT(1H ,'The element selected',1A2,' has a nuclear charge less
     &than the lowest ',I4,' for which Sampsons file contains data')
      END
