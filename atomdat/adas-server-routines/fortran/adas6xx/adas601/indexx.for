CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/indexx.for,v 1.2 2016/01/08 21:59:10 mog Exp $ Date $Date: 2016/01/08 21:59:10 $
CX
C
      SUBROUTINE INDEXX(N,ARR,INDX)
C
C------------------------------------------------------------------------
C ROUTINE: ADAS601
C
C PURPOSE: Indexes an array ARR(1:N), i.e. outputs the array INDX(1:N)
C          such that ARRAY(INDX(J)) is in ascending order for J=1,2...N
C          REFERENCES: Press, H. W. et al "Numerical Recipes", 1992
C
C INPUT:  (I*4)    N   : DIMENSION OF ARRAYS ARR AND INDX
C
C INPUT:  (R*8)  ARR() : UNSORTED REAL ARRAY
C
C OUTPUT: (I*4) INDX() : INDEX SORTING TABLE
C
C------------------------------------------------------------------------
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C
C VERSION : 1.2                       
C DATE    : 08-01-2016
C MODIFIED: Martin O'Mullane
C           - Replace (obsolete) PAUSE statement with STOP.
C 
C-----------------------------------------------------------------------
      INTEGER N       ,  INDX(N) ,   M ,  NSTACK
      REAL*8  ARR(N)
C-----------------------------------------------------------------------
      PARAMETER ( M=7,  NSTACK=50 )
      INTEGER     I  ,  INDXT ,  IR  ,  ITEMP ,  J , JSTACK ,
     &            K  ,  L     ,  ISTACK(NSTACK)
      REAL*8 A
C-----------------------------------------------------------------------

      DO 11 J=1,N
         INDX(J)=J
 11   CONTINUE
      JSTACK=0
      L=1
      IR=N
 1    IF(IR-L.LT.M)THEN
         DO 13 J=L+1,IR
            INDXT=INDX(J)
            A=ARR(INDXT)
            DO 12 I=J-1,1,-1
               IF(ARR(INDX(I)).LE.A)GOTO 2
               INDX(I+1)=INDX(I)
 12         CONTINUE
            I=0
 2          INDX(I+1)=INDXT
 13      CONTINUE
         IF(JSTACK.EQ.0)RETURN
         IR=ISTACK(JSTACK)
         L=ISTACK(JSTACK-1)
         JSTACK=JSTACK-2
      ELSE
         K=(L+IR)/2
         ITEMP=INDX(K)
         INDX(K)=INDX(L+1)
         INDX(L+1)=ITEMP
         IF(ARR(INDX(L+1)).GT.ARR(INDX(IR)))THEN
            ITEMP=INDX(L+1)
            INDX(L+1)=INDX(IR)
            INDX(IR)=ITEMP
         ENDIF
         IF(ARR(INDX(L)).GT.ARR(INDX(IR)))THEN
            ITEMP=INDX(L)
            INDX(L)=INDX(IR)
            INDX(IR)=ITEMP
         ENDIF
         IF(ARR(INDX(L+1)).GT.ARR(INDX(L)))THEN
            ITEMP=INDX(L+1)
            INDX(L+1)=INDX(L)
            INDX(L)=ITEMP
         ENDIF
         I=L+1
         J=IR
         INDXT=INDX(L)
         A=ARR(INDXT)
 3       CONTINUE
         I=I+1
         IF(ARR(INDX(I)).LT.A)GOTO 3
 4       CONTINUE
         J=J-1
         IF(ARR(INDX(J)).GT.A)GOTO 4
         IF(J.LT.I)GOTO 5
         ITEMP=INDX(I)
         INDX(I)=INDX(J)
         INDX(J)=ITEMP
         GOTO 3
 5       INDX(L)=INDX(J)
         INDX(J)=INDXT
         JSTACK=JSTACK+2
         IF(JSTACK.GT.NSTACK)stop 'NSTACK TOO SMALL IN INDEXX'
         IF(IR-I+1.GE.J-L)THEN
            ISTACK(JSTACK)=IR
            ISTACK(JSTACK-1)=I
            IR=J-1
         ELSE
            ISTACK(JSTACK)=J-1
            ISTACK(JSTACK-1)=L
            L=I
         ENDIF
      ENDIF
      GOTO 1
      END
