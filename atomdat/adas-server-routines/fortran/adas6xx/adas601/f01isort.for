CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/f01isort.for,v 1.1 2004/07/06 13:50:44 whitefor Exp $ Date $Date: 2004/07/06 13:50:44 $
CX
C
      SUBROUTINE F01ISORT(N,IA,IB,IRANK)
      IMPLICIT NONE
C
C PURPOSE: SORT INTEGER ARRAY FROM RANK TABLE
C
C INPUT:  (I*4)      N     = DIMENSION OF REAL ARRAY
C
C INPUT:  (I*4)     IA()   = INTEGER ARRAY TO BE SORTED
C
C OUTPUT: (I*4) IRANK()    = RANK TABLE
C
C         (I*4)     IB()   = WORK SPACE
C
C AUTHOR: Alessandro Lanzafame, University of Strathclyde
C
C DATE:   24 January 1996
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER  N     , IRANK(N), K, L
      INTEGER  IA(N) , IB(N)
C-----------------------------------------------------------------------

      DO K = 1, N
         DO L = 1, N
	    IF (IRANK(L).EQ.K) IB(K)=IA(L)
         ENDDO
      ENDDO

      DO  K = 1, N
         IA(K) = IB(K)
      ENDDO

      RETURN
C-----------------------------------------------------------------------
      END
