CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1uss5.for,v 1.1 2004/07/06 14:40:25 whitefor Exp $ Date $Date: 2004/07/06 14:40:25 $
CX
      SUBROUTINE R1USS5(q,w2,g2,l,sig,n,m,lmax,lmin,acc,a)
C
C This is subroutine finds the value of the smoothing parameter l for which
C the residual sum of squares function equals the noise variance. This routine
C is for use with R1US00
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      Implicit Double Precision (A-H,L,O-Z)
      Dimension q(*),W2(*)
C
C Check that the value of lmax is appropriate
C
      Print *,'Enter the desired value of the smoothing parameter'
      Read(*,*) l
C
      return
      end
