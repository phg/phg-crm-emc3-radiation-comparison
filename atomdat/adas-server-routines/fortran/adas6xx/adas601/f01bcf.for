CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/f01bcf.for,v 1.2 2012/11/14 19:42:09 mog Exp $ Date $Date: 2012/11/14 19:42:09 $
CX
C
      SUBROUTINE F01BCF(NCAP7, LAMDA, C, X, LEFT, S, IFAIL)
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C 
C  PURPOSE: EVALUATES A CUBIC SPLINE AND ITS FIRST THREE 
C           DERIVATIVES FROM ITS B-SPLINE REPRESENTATION.
C           REPLACEMENT FOR NAG ROUTINE E02BCF 
C
C  CALLING PROGRAM: VARIOUS
C 
C  INPUT:  (I*4)  NCAP7 =  n-bar + 7, where n-bar is the number 
C                          of intervals of the spline (which is one greater 
C                          than the number of interior knots, i.e. the 
C                          knots strictly within the range lambda(4) to 
C                          lambda(n-bar+4) over which the spline is defined). 
C 
C                          CONSTRAINT: NCAP7 >= 8. 
C 
C  INPUT:  (R*8) LAMDA() = LAMDA(j) must be set to the value of the jth 
C                          member of the complete set of knots, lambda(j), 
C                          for j = 1,2,...,n-bar+7. 
C 
C                          CONSTRAINT: the LAMDA(j) must be in 
C                          non-decreasing order with 
C                          LAMDA(NCAP7-3) > LAMDA(4). 
C 
C  INPUT:  (R*8) C()     = The coefficient c(i) of the B-spline N(i)(x), 
C                          for i = 1,2,...,n-bar+3. The remaining elements 
C                          of the array are not used. 
C 
C  INPUT:  (R*8) X       = The argument x at which the cubic spline and its 
C                          derivatives are to be evaluated. 
C 
C                          CONSTRAINT: LAMDA(4) <= X <= LAMDA(NCAP7-3). 
C 
C  INPUT:  (I*4) LEFT    = Specifies whether left- or right-hand values 
C                          of the spline and its derivatives are to be 
C                          computed.  Left- or right-hand values are formed
C                          according to whether LEFT is equal or not equal 
C                          to 1. If x does not coincide with a knot, 
C                          the value of LEFT is immaterial. If x = LAMDA(4), 
C                          right-hand values are computed, and if 
C                          x = LAMDA(NCAP7-3), left-hand values are formed, 
C                          regardless of the value of LEFT. 
C
C  INPUT:  (I*4) IFAIL   = 0 Handling errors
C
C  OUTPUT: (R*8) S(4)    = S(j) contains the value of the (j-1)th derivative
C                          of the spline at the argument x, for j = 1,2,3,4. 
C                          Note that S(1) contains the value of the spline. 
C 
C  OUTPUT: (I*4) IFAIL   = 0 unless the routine detects an error 
C
C                        = 1: NCAP7 < 8, i.e. the number of intervals is
C                             not positive. 
C 
C                        = 2: Either LAMDA(4) >= LAMDA(NCAP7-3), i.e. the 
C                             range over which s(x) is defined is null or
C                             negative in length, or X is an  invalid 
C                             argument, i.e.
C                             X < LAMDA(4) or X > LAMDA(NCAP7-3). 
C
C
C          (I*4) NC7MAX  = PARAMETER: maximum NCAP7 allowed
C
C          (I*4) N       = PARAMETER: spline order (=4)
C                          (spline degree = N-1 = 3)
C
C          (R*8) PI      = PARAMETER
C
C          (R*8) ALFA()  = Chebyshev series coefficients for x.          
C 
C          (R*8) SJ      = Spline at cosine points (used as workspace)
C
C          (I*4) J       = INDEX lamda(j) <= x<= x(j+1)
C
C          (R*8) Y       = GENERAL REAL
C          (I*4) I       = GENERAL INTEGER
C          (I*4) K       = GENERAL INTEGER
C
C                                                        
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          F01BBF     ADAS      EVAL. CUBIC SPLINE FROM B -SPLINE REPR.     
C          CHE2EV               CHEBYSHEV EVALUATION
C
C
C AUTHOR: Alessandro C. Lanzafame, University of Strathclyde
C
C DATE:  16 January 1996
C
C VERSION 1.1 Alessandro C. Lanzafame 16 January 1996
C
C VERSION 1.1 Alessandro C. Lanzafame 29 April 96
C             Initial checks improved
C
C***********************************************************************
C UNDER SCCS 
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C NOTE: The Chebyshev espansion is not necessary an should be changed
C       by the De Boor formulae. Right and left derivatives for
C       non-continuos functions do not work. It is ok for continuous
C       function.
C
C VERSION: 1.1				DATE: 11-08-08
C MODIFIED: ALESSANDRA GIUNTA
C	    - MODIFIED CONSTRAINT LAMDA(4) GE LAMDA(NCAP7-4) TO
C             LAMDA(4) GT LAMDA(NCAP7-4)
C
C
C----------------------------------------------------------------------- 
      INTEGER     I, K, J
      INTEGER     NC7MAX     , N
      PARAMETER ( NC7MAX=200 , N=4 )
C-----------------------------------------------------------------------
      REAL*8      PI
      PARAMETER ( PI = 3.141592653589793D0 )
C-----------------------------------------------------------------------
      REAL*8      ALFA(N) , ADER(N)
C-----------------------------------------------------------------------
      INTEGER     NCAP, NCAP7, LEFT, IFAIL 
      REAL*8      LAMDA(NCAP7), C(NCAP7), X, S(4)
C-----------------------------------------------------------------------
      REAL*8      BMA, BPA, FAC, SUM, Y, YM, SJ, CHEBEV
C----------------------------------------------------------------------- 
      EXTERNAL    CHEBEV
C-----------------------------------------------------------------------
C
C Trap error NCAP7 > NC7MAX
C-----------------------------------------------------------------------

      NCAP = NCAP7 - 7

      IF (NCAP7 .GT. NC7MAX) THEN
         WRITE(0,*)
     &        '*** ERROR - F01BCF: NCAP7 GREATER THAN PARAM NC7MAX ***'
         WRITE(0,*)
     &        '*** INCREASE NC7MAX                                 ***'
         STOP
      ENDIF

C-----------------------------------------------------------------------
C Trap error NCAP7 < 8
C-----------------------------------------------------------------------

      IF (NCAP7 .LT. 8) THEN
         IFAIL=1
         WRITE(0,*) '*** ERROR - F01BCF: IFAIL = 1 ***'
         STOP
      ENDIF

C-----------------------------------------------------------------------
C Trap errors  LAMDA(0) >= LAMDA(NCAP)
C      or     X < LAMDA(0) or X > LAMDA(NCAP)
C-----------------------------------------------------------------------
C
C------ MODIFIED BY ASG - 11-08-2008 -----------------------------------
C------ COMMENTS: IS THE SOLUTION UNIQUE? ------------------------------
C
C      IF ( ( LAMDA(4) .GE. LAMDA(NCAP7-4) ) .OR.
C     &     ( X .LT. LAMDA(4)            ) .OR.
C     &     ( X .GT. LAMDA(NCAP7-3)      ) ) THEN
C	 IFAIL=2
C	 WRITE(0,99000) LAMDA(4),X,LAMDA(NCAP7-4)
C         WRITE(0,*) '*** ERROR - F01BCF: IFAIL = 2 ***'
C         STOP
C      ENDIF

       IF ( ( LAMDA(4) .GT. LAMDA(NCAP7-4) ) .OR.
     &       ( X .LT. LAMDA(4)            ) .OR.
     &       ( X .GT. LAMDA(NCAP7-3)      ) ) THEN

	  IFAIL=2
	  WRITE(0,99000) LAMDA(4),X,LAMDA(NCAP7-4)
          WRITE(0,*) '*** ERROR - F01BCF: IFAIL = 2 ***'
          STOP
        ENDIF
C
C-----------------------------------------------------------------------

99000 FORMAT(1X,' LAMDA(4) = ',F12.4,' X = ',F12.4,
     &            ' LAMDA(NCAP7-4) = ',F12.4)
C-----------------------------------------------------------------------
C Locate X in array LAMDA, LAMDA(J) <= X <= LAMDA(J+1)
C Check that LAMDA(J) < LAMDA(J+1)
C-----------------------------------------------------------------------

      J=4
 30   CONTINUE
      IF ((X.GE.LAMDA(J)).AND.(J.LE.(NCAP7-4))) THEN
         J = J + 1
         GO TO 30
      ENDIF
      J=J-1
 40   CONTINUE
      IF ((LAMDA(J).EQ.LAMDA(J+1)).AND.(X.LE.LAMDA(J+1))) THEN
         J=J+1
         GOTO 40
      ENDIF

C-----------------------------------------------------------------------
C Evaluate Chebyshev series coefficients
C-----------------------------------------------------------------------

      BMA = 0.5D0*(LAMDA(J+1)-LAMDA(J))
      BPA = 0.5D0*(LAMDA(J+1)+LAMDA(J))
      FAC = 2.D0 / N

      DO I = 1, N
         SUM = 0.0D0
         DO K = 1, N
            Y = COS( PI * (K-0.5D0) / N )
            YM = Y*BMA + BPA 
            CALL F01BBF(NCAP7,LAMDA,C,YM,SJ,IFAIL)
            SUM = SUM + SJ * COS( (PI * (I-1)) * ((K-0.5D0) / N) )
         ENDDO
         ALFA(I) = FAC * SUM
      ENDDO

C-----------------------------------------------------------------------
C Evaluate Chebyshev series
C-----------------------------------------------------------------------

      S(1) = CHEBEV( LAMDA(J), LAMDA(J+1), ALFA, N, X )

C-----------------------------------------------------------------------
C Evaluate derivatives of the Chebyshev-approximated function
C-----------------------------------------------------------------------

      DO I=2,N
         CALL CHDER( LAMDA(J), LAMDA(J+1), ALFA, ADER, N)
         S(I) = CHEBEV( LAMDA(J), LAMDA(J+1), ADER, N, X )
         IF (I .LT. N) THEN
            DO K=1,N
               ALFA(K) = ADER(K)
            ENDDO
         ENDIF
      ENDDO

      RETURN
C-----------------------------------------------------------------------
      END
