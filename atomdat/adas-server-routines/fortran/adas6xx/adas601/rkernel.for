C Copyright (c) 1995, Strathclyde University.
CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/rkernel.for,v 1.3 2004/07/06 15:15:30 whitefor Exp $ Date $Date: 2004/07/06 15:15:30 $
CX
C Alessandro Lanzafame	acl@phys.strath.ac.uk	21 November 1995
C-----------------------------------------------------------------------
C
      SUBROUTINE RKERNEL( NTEMP, TEMP, M  , INDX , G , SG , NG,
     &                    TM   , T   , GT, IR1USS, SIGMA )

C-----------------------------------------------------------------------
C------------------------------------------------------------------------
C AUTHOR: ALESSANDRO LANZAFAME
C         UNIVERSITY OF STRATHCLYDE
C
C DATE: 21ST NOVEMBER 1995
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C           - MTRA -> 200
C           - TRAP OF M > MTRA ERROR
C           - WRITE-OUT OF GT TO UNIT 12 REMOVED
C
C VERSION: 1.3                          DATE: 26-01-99
C MODIFIED: ALESSANDRO LANZAFAME
C           - READ IN G(,) TROUGH DUMMY VARIABLE XG
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      IMPLICIT NONE
C-----------------------------------------------------------------------
      INTEGER   MTRA   , MTEMP
      INTEGER   MABUND ,  MLEV 
      INTEGER   NP     ,  NCMAX
 
      PARAMETER ( MTRA=200  , MTEMP=101 )
      PARAMETER ( MABUND=30 , MLEV=110              )
      PARAMETER ( NP=8      , NCMAX=20              )
C-----------------------------------------------------------------------
      INTEGER   NG(MTRA)
      REAL*8    T(MTEMP,MTRA) , TM(MTRA) , GT(MTEMP,MTRA)
      REAL*8    G(MTRA)       , SG(MTRA)
      REAL*8    TEMP(MTEMP)
      INTEGER   INDX(MTRA)
C-----------------------------------------------------------------------
      INTEGER   M , NTEMP
C-----------------------------------------------------------------------
      INTEGER   I, J
      REAL*8    XT, XG
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------
      INTEGER IR1USS
      REAL*8  SIGMA
C-----------------------------------------------------------------------

      READ(PIPEIN,*) IR1USS
      READ(PIPEIN,*) SIGMA
      READ(PIPEIN,*) NTEMP

      DO I=1,NTEMP
         READ(PIPEIN,*)TEMP(I)
         TEMP(I)=DLOG(TEMP(I))
      ENDDO

      READ(PIPEIN,*) M

      IF (M .GT. MTRA) THEN
         WRITE(0,*)'RKERNEL ERROR: M GREATER THAN MTRA '
         STOP
      ENDIF

      DO I=1,M
cx         READ(PIPEIN,*) INDX(I)
         READ(PIPEIN,*) G(I)
         READ(PIPEIN,*) SG(I)
         READ(PIPEIN,*) NG(I)
         READ(PIPEIN,*) TM(I)
         TM(I)=DLOG10(TM(I))

         DO J=1,NG(I)
            READ(PIPEIN,*) XT
            T(J,I)=DLOG(XT)
            READ(PIPEIN,*) XG
            GT(J,I)=XG
         ENDDO

      ENDDO

      RETURN
      END
