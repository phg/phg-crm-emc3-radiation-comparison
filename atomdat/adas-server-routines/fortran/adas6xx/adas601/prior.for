CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/prior.for,v 1.2 2012/11/14 19:41:18 mog Exp $ Date $Date: 2012/11/14 19:41:18 $
CX
C
C
C------------------------------------------------------------------------
c NAME: PRIOR
c
c ROUTINE: adas601
c
c PURPOSE: this subroutine defines the prior
c
C------------------------------------------------------------------------
C AUTHOR: ALESSANDRO LANZAFAME
C         UNIVERSITY OF STRATHCLYDE
C
C DATE: 29TH APRIL 1996
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C VERSION: 1.1				DATE: 11-08-08
C MODIFIED: ALESSANDRA GIUNTA
C	    - CHANGE EXTRAPOLATION AT LOW TEMPERATURE:
C             LINEAR INSTEAD OF SPLINE
C
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      double Precision Function Prior(T,DLAM,CO,NCAP7)
      Implicit double Precision (a-h,o-z)
      Parameter(wid=0.06,wid1=0.01,NS=4)
      Dimension Dlam(*),CO(*),S(NS)
C
C This function calculates the prior from the spline representation
C calculated from the first look subroutine. If the ends of the prior 
C are extrapolated linearly from the ends of the valid spline region
C
C convert T to base 10
C
      X=T/Dlog(10.d0)
c      X=T
C
C calculate log10 of the Prior
C
      IF(X.lt.Dlam(4)) Then
         X0=Dlam(4)
         Left=0
         Ifail=0

c         Call E02BCF(Ncap7,Dlam,Co,X0,Left,S,Ifail)
	 Call F01BCF(Ncap7,Dlam,Co,X0,Left,S,Ifail)

         T0=10.d0**X0
         T1=10.d0**X
         IF(S(2).gt.0.d0) Then
            Prior=S(1)+(X-X0)*S(2)
         Else

C------ MODIFIED BY ASG - 11-08-2008 -----------------------------------
C
C            Prior=S(1)+Dlog10(1.d0+(T1/T0-1.d0)*s(2))
C             
             Prior=S(1)+(X-X0)*S(2)

C-----------------------------------------------------------------------
         End If
      ELSE
         IF(X.LT.Dlam(Ncap7-3)) Then
            Ifail=0

c            Call E02BBF(NCap7,Dlam,CO,X,S(1),Ifail)
            Call F01BBF(NCap7,Dlam,CO,X,S(1),Ifail)

            Prior=S(1)
         Else
            X0=Dlam(Ncap7-3)
            Left=1
            Ifail=0

c            Call E02BCF(Ncap7,Dlam,Co,X0,Left,S,Ifail)
            Call F01BCF(Ncap7,Dlam,Co,X0,Left,S,Ifail)

            T0=10.d0**X0
            T1=10.d0**X
            IF(S(2).LT.0.d0) Then
               Prior=S(1)+(X-X0)*S(2)
            Else


C------ MODIFIED BY ASG - 11-08-2008 -----------------------------------
C
C               Prior=S(1)+Dlog10(1.d0+(T1/T0-1)*s(2))
C
                Prior=S(1)+(X-X0)*S(2)

C-----------------------------------------------------------------------

            End If
         END IF
      END IF
C
C calculate the Prior
C
C
      prior=10.d0**prior
      return
      end
