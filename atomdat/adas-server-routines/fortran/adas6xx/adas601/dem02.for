C Copyright (c) 1995, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/dem02.for,v 1.3 2004/07/06 13:30:31 whitefor Exp $ Date $Date: 2004/07/06 13:30:31 $
CX
C Alessandro Lanzafame	acl@phys.strath.ac.uk	21 November 1995
C-----------------------------------------------------------------------
C
C  PROGRAM: DEM
C
C  VERSION:  2.0  (ADASS95)
C
C  PURPOSE:  Performs integral inversion to detemine the emission
C            measure, differential in temperature.
C
C  DATA:     The input data consist of:
C
C            abundance:
C
C            observed intensities:
C
C            G(T) functions:
C
C  PROGRAM:
C
C  NOTE:
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C
C AUTHOR:  Alessandro Lanzafame, Strathclyde University
C
C DATE:    aug22-95
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER S.C.C.S CONTROL
C
C VERSION: 1.2				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C	    - MTRA -> 200, REMOVED MTRAE
C           - H(MTEMP,MTEMP)->H(MTRA,MTEMP)
C
C VERSION: 1.3				DATE: 17-07-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT IN CALL OF XX0000
C
C-----------------------------------------------------------------------
C      implicit double precision (a-h,o-z)

C-----------------------------------------------------------------------
      IMPLICIT NONE
C-----------------------------------------------------------------------
      INTEGER   MTRA   , MTEMP
      INTEGER   MABUND ,  MLEV 
      INTEGER   NP     ,  NCMAX
 
      PARAMETER ( MTRA=200  , MTEMP=101 )
      PARAMETER ( MABUND=30 , MLEV=110              )
      PARAMETER ( NP=8      , NCMAX=20              )
C-----------------------------------------------------------------------
      INTEGER   NG(MTRA)
      REAL*8    T(MTEMP,MTRA) , TM(MTRA) , GT(MTEMP,MTRA)
      REAL*8    G(MTRA)       , SG(MTRA)
      INTEGER   INDX(MTRA)
C-----------------------------------------------------------------------
      INTEGER   M , NTEMP

      REAL*8    TEMP(MTEMP) , FHAT(MTEMP)    , G0(MTRA)
      REAL*8    GP(MTRA)    , H(MTRA,MTEMP) , HMAX      , SIGMA
      REAL*8    RL
C-----------------------------------------------------------------------
      INTEGER   NCAP7      
      REAL*8    DLAM(NCMAX) , CO(NCMAX)  , XS(MTEMP),YS(MTEMP)
C-----------------------------------------------------------------------
      INTEGER   IR1USS
C-----------------------------------------------------------------------

C      EXTERNAL R1SIC2,R1USS3

c-----------------------------------------------------------------------
c      integer ieeer, ieee_handler
c      external myhandler
c      ieeer = ieee_handler('set','common',myhandler)
c-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------
C READ KERNEL AND TEMPERATURE MESH FOR RESULTS: PIPE FROM IDL
C-----------------------------------------------------------------------

      CALL RKERNEL( NTEMP, TEMP, M  , INDX , G , SG , NG,
     &              TM   , T   , GT , IR1USS, SIGMA)

C-----------------------------------------------------------------------
C DEFINE PRIOR: B-SPLINE REPRESENTATION
C-----------------------------------------------------------------------

      CALL FIRST( M , T, GT  , FHAT , TM , XS, YS, NG, G,
     &            NCAP7, DLAM, CO )

C-----------------------------------------------------------------------
C PERFORM INVERSION
C-----------------------------------------------------------------------

      CALL INVERT(M      , NG   , T  , DLAM, CO, NCAP7, GT,
     &            H      , HMAX , FHAT ,
     &            NTEMP  , TEMP ,
     &            G      , SG   , RL , SIGMA , IR1USS
     &           )

C-----------------------------------------------------------------------
C OUTPUT RESULTS (PIPE TO IDL)
C-----------------------------------------------------------------------

      CALL WOUT(M     , G0   , GP  , H    , FHAT , 
     &          G     , SG   , XS  , YS   , 
     &          NTEMP , TEMP , RL  , SIGMA  ,
     &          NCAP7 , DLAM , CO  , HMAX )

C-----------------------------------------------------------------------
      STOP
      END
