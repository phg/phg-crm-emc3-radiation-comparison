CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/wout.for,v 1.9 2004/07/06 15:29:33 whitefor Exp $ Date $Date: 2004/07/06 15:29:33 $
CX
      SUBROUTINE WOUT(M     , G0   , GP  , H   , FHAT , 
     &                G     , SG   , XS  , YS  , 
     &                NTEMP , TEMP , RL  , SIG ,
     &                NCAP7 , DLAM , CO  , HMAX )

C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C VERSION: 1.2				DATE: 14-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT XXFLSH IN WHERE NEEDED
C
C VERSION: 1.3				DATE: 16-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - REMOVED FROM BEGINNING OF PROGRAM BEFORE ANYTHING OUTPUT
C
C VERSION: 1.4				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C	    - MTRA -> 200
C           - MORE PARAMETERS NOW PASSED TO IDL
C           - FORTRAN NOW WAITS AT THE END FOR THE IDL TO SAY THAT IT IS
C             SAFE TO CONTINUE
C
C VERSION: 1.5				DATE: 14-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - REPLACED WRITE(0,*) WITH WRITE(I4UNIT(-1),*)
C
C VERSION: 1.6				DATE: 14-02-97
C MODIFIED: RICHARD MARTIN
C	    - REPLACED WRITE(I4UNIT(-1),20),RL,SIG
C		WITH WRITE(I4UNIT(-1),20)RL,SIG
C	    - WRITE(I4UNIT(-1),'(A9,1PE10.2)'),'  RSS=   ',RSS
C		WITH WRITE(I4UNIT(-1),'(A9,1PE10.2)')'  RSS=   ',RSS
C
C VERSION: 1.7				DATE: 21-02-97
C MODIFIED: ALESSANDRO LANZAFAME (IOACT, UNIVERSITA' DI CATANIA)
C	    - ADDED PASSING OF B-SPLINE REPRESENTATION OF FUNCTION
C             PRIOR
C	    - REMOVED DIAGNOSTIC OUTPUT
C
C VERSION: 1.8				DATE: 07-04-97
C MODIFIED: RICHARD MARTIN
C	    As a result of multiple copies being edited, the following
C	    corrections (originally r1.6) were 'lost'
C	    - REPLACED WRITE(I4UNIT(-1),20),RL,SIG
C		WITH WRITE(I4UNIT(-1),20)RL,SIG
C	    - WRITE(I4UNIT(-1),'(A9,1PE10.2)'),'  RSS=   ',RSS
C		WITH WRITE(I4UNIT(-1),'(A9,1PE10.2)')'  RSS=   ',RSS
C
C VERSION 1.9                          DATE:  29-09-00
C MODIFIED: ALESSANDRO LANZAFAME
C           PASS MORE INFORMATION TO IDL
C
C-----------------------------------------------------------------------
      IMPLICIT NONE
C-----------------------------------------------------------------------
      INTEGER   MTRA   , MTEMP
      INTEGER   MABUND ,  MLEV 
      INTEGER   NP     ,  NCMAX
 
      PARAMETER ( MTRA=200  , MTEMP=101 )
      PARAMETER ( MABUND=30 , MLEV=110              )
      PARAMETER ( NP=8      , NCMAX=20              )
C-----------------------------------------------------------------------
      REAL*8    G(MTRA)       , SG(MTRA)
C-----------------------------------------------------------------------
      INTEGER   M , NTEMP , CC
      INTEGER   I4UNIT
      REAL*8    TEMP(MTEMP) , FHAT(MTEMP)    , F0(MTEMP) , G0(MTRA)
      REAL*8    GP(MTRA)    , H(MTRA,MTEMP) , HMAX      , SIG
      REAL*8    RL
C-----------------------------------------------------------------------
      INTEGER   NCAP7  
      REAL*8    DLAM(NCMAX) , CO(NCMAX)  , XS(MTEMP),YS(MTEMP)
C-----------------------------------------------------------------------
      INTEGER   I, J
      REAL*8    FBAR, PRIOR
      REAL*8    RSS, GPBAR
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU
      PARAMETER( PIPEIN=5  , PIPEOU=6)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  Pass smoothing paramenter and variance to idl
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)RL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)SIG
      CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------
C  Pass maximum of matrix H to idl
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)HMAX
      CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------
C  Pass B-spline representation of function prior
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)NCAP7
      DO I=1,NCAP7
         WRITE(PIPEOU,*) DLAM(I)
         WRITE(PIPEOU,*) CO(I)
      ENDDO

C-----------------------------------------------------------------------

      DO I=1,M
         G0(I)=0.D0
         GP(I)=0.D0
         DO J=1,NTEMP
            G0(I)=G0(I)+H(I,J)*FHAT(J)
            GP(I)=GP(I)+H(I,J)*SG(I)
         ENDDO
      ENDDO

      RSS=0.D0
      GPBAR=0.D0      
      DO I=1,M
c         WRITE(31,*) XS(I), YS(I)
         WRITE(PIPEOU,*) YS(I)
         CALL XXFLSH(PIPEOU)
c         WRITE(PIPEOU,*) G(I)*SG(I)/12.56
c         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) G0(I)*SG(I)/12.56
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) GP(I)/12.56
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) DSQRT(SIG)*SG(I)/12.56
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) (G(I)-G0(I))**2/SIG
         CALL XXFLSH(PIPEOU)
         GPBAR=GPBAR+GP(I)
         RSS=RSS+(G(I)-G0(I))**2/SIG
      ENDDO

      WRITE(PIPEOU,*) RSS
      CALL XXFLSH(PIPEOU)

      FBAR=0.D0
      DO I=1,NTEMP
         F0(I)=PRIOR(TEMP(I),DLAM,CO,NCAP7)
c         WRITE(32,*)DEXP(TEMP(I)),FHAT(I)/HMAX*F0(I),F0(I)
         WRITE(PIPEOU,*)FHAT(I)/HMAX*F0(I)
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*)F0(I)
         CALL XXFLSH(PIPEOU)
      ENDDO


CXC--------------------------------------------------------------------
CXC PLOT: THE PRIOR F0
CXC       THE FIRST GUESSES FOR F YSRT
CXC       THE FINAL ESTIMATE OF F FHAT
CXC--------------------------------------------------------------------

CXC      CALL PLOT1(XSRT,YSRT,TEMP,FHAT,F0,M,NTEMP)

C-----------------------------------------------------------------------
C DIAGNOSTIC OUTPUT- REMOVED: NOW IDL-ROUTINES TAKES CARE OF IT PROPERLY
C-----------------------------------------------------------------------
C
C      WRITE(I4UNIT(-1),20)RL,SIG
C 20   FORMAT(1X,'SMOOTHING PARAMETER = ',1PE10.2,' SIGMA0 = ',1PE10.2)
C
C      RSS=0.D0
C      GPBAR=0.D0
C      WRITE(I4UNIT(-1),*) '     I         IREC      EPS       SIG     ',
C     &           'DEVIATION'
C      DO I=1,M
C         WRITE(I4UNIT(-1),10) I,
C     &               G(I)*SG(I)/12.56,
C     &               G0(I)*SG(I)/12.56,
C     &               GP(I)/12.56,
C     &               DSQRT(SIG)*SG(I)/12.56,
C     &              (G(I)-G0(I))**2/SIG
C         GPBAR=GPBAR+GP(I)
C         RSS=RSS+(G(I)-G0(I))**2/SIG
C      ENDDO
C 10   FORMAT(I3,1P5E10.3)
C
C      WRITE(I4UNIT(-1),'(A9,1PE10.2)')'  RSS=   ',RSS
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL THAT IT HAS FINISHED READING IN DATA
C-----------------------------------------------------------------------

      READ(PIPEIN,*)CC

      RETURN
      END
