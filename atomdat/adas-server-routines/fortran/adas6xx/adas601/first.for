CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/first.for,v 1.5 2012/11/14 19:42:44 mog Exp $ Date $Date: 2012/11/14 19:42:44 $
CX
C
      SUBROUTINE FIRST( M , T, GT  , FHAT , TM , XS, YS, NG , G,
     &                  NCAP7, DLAM, CO )
C-----------------------------------------------------------------------
C
C PURPOSE: calculates a first approximation to the DEM by
C          calculating the Average DEM for each data point.
C
C Thompson comments:
C define the arrays GT,is the kernel,N is the first dimension of the Kernel
C G is the data Fhat will contain the DEM estimate,X is a work space,T is the
C log Temp Grid for GT, TM are the log Ts for the maximum and M is the 
C number of data pts
C-----------------------------------------------------------------------
C
C INPUT:   (R*8)  gt(,)   Kernel
C                         G(T) * T * abundance(element)
C                            first index: temperature grid
C                            second index: specroscopic line
C                 g()        4*pi * line intensity 
C                                    in [photons/s/cm^2/sterad]
C
C                 t(,)    temperature grid (log) of gt()
C                            first index: temperature grid
C                            second index: specroscopic line
C 
C                 tm()    temperature (log) of maximum in gt()
C
C          (I*4)  m       number of lines
C
C                 ng()    num of temperaure points in gt()
C
C OUTPUT:  (R*8)  fhat()  estimate of DEM
C                 xs()    sorted tm() in ascendig order
C                 ys()    rearranged fhat() according to tm() 
C
C          (R*8)
C                 dlam()  B-spline knots
C                 co()    B-spline coefficients
C          (I*4)  ncap7   number of intervals of the B-spline + 7
C                         8<=ncap7<=mdist+4, where mdist is the 
C                         number of distinct x values in the data
C
C PROGRAM: (R*8)  x()     work space 
C                 w()     work sapce
C          (I*4)  iseq()  rank vector, used to sort Fhat vs. tm in 
C                            ascending tm order
C-------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER S.C.C.S CONTROL
C
C VERSION: 1.2				DATE: 11-07-96
C MODIFIED: ALESSANDRO LANZAFAME / WILLIAM OSBORN
C           - MTRA -> 200, REMOVED MTRAE
C
C VERSION: 1.3				DATE: 01-04-97
C MODIFIED: ALESSANDRO LANZAFAME
C           - COMMENTED OUT real*8 xsp
C
C VERSION: 1.4                          DATE: 11-02-99
C MODIFIED: ALESSANDRO LANZAFAME
C           - DO NOT PERFORM SORTING. THIS IS NOW DONE BY THE IDL
C             ROUTINES
C
C VERSION: 1.4                          DATE: 11-08-08
C MODIFIED: ALESSANDRA GIUNTA
C           - MODIFIED MINIMUN VALUE OF NCAP7 WHEN M LT 7
C
C-------------------------------------------------------------------------
C-----------------------------------------------------------------------
      IMPLICIT NONE
C-----------------------------------------------------------------------
      INTEGER   MTRA   , MTEMP
      INTEGER   MABUND ,  MLEV 
      INTEGER   NP     ,  NCMAX
 
      PARAMETER ( MTRA=200  , MTEMP=101 )
      PARAMETER ( MABUND=30 , MLEV=110              )
      PARAMETER ( NP=8      , NCMAX=20              )
C-----------------------------------------------------------------------
      INTEGER   NG(MTRA)
      REAL*8    T(MTEMP,MTRA) , TM(MTRA) , GT(MTEMP,MTRA)
      REAL*8    G(MTRA)     
      INTEGER   INDX(MTRA), IWORK(MTRA)
C-----------------------------------------------------------------------
      INTEGER   M
      REAL*8    FHAT(MTEMP)
C-----------------------------------------------------------------------
      INTEGER   NCAP7       , ISEQ(MTRA)
      REAL*8    DLAM(NCMAX) , CO(NCMAX)  , XS(MTEMP),YS(MTEMP)
C-----------------------------------------------------------------------
      INTEGER   I , J, K , MLAM, IFAIL
      REAL*8    X(MTRA), W(MTRA), WW(MTEMP) , ER, RWORK
      REAL*8    WRK1(MTRA) , WRK2(NCMAX,4), RSS, ANS, DDL

c      real*8 xsp
C-----------------------------------------------------------------------
C Integrate [A*G(T) dT] over the whole of its sepcified range 
C for every line.
C
C Fhat(line) is
C 
C   Fhat(line) = 4*pi*LineIntensity[photons/s/cm^2/sterad] 
C                / Integrate[ A*G(T)*T d(ln(T))]
C
C D01GAF integrates a function which is specified numerically at
C four or more points over the whole of its specified range,
C using third-order finite-differencing formulae with error
C estimating, according to the method of Gill-Miller
C-----------------------------------------------------------------------
      DO I=1,M
         DO J=1,NG(I)
            X(J)=GT(J,I)
            W(J)=T(J,I)
         ENDDO

         IFAIL=0

C         CALL D01GAF(W,X,NG(I),ANS,ER,IFAIL)
         CALL F01GAF(W,X,NG(I),ANS,ER,IFAIL)

         FHAT(I)=G(I)/ANS
         XS(I)=TM(I)
         YS(I)=FHAT(I)
      ENDDO

C-----------------------------------------------------------------------
C sort the XS and YS arrays so that XS is increasing
C
C M01DAF ranks a vector of real numbers in ascending or 
C descending orders. Iseq will contain the ranks of the
C corresponding elements of XS
C
C M01EAF rearranges a vector of real numbers into the order
C specified by a vector of ranks, i.e. Iseq.
C
C-----------------------------------------------------------------------

C      IFAIL=0
C      CALL M01DAF(XS,1,M,'A',ISEQ,IFAIL)
C      CALL F01RANK(M,XS,INDX,ISEQ)

C      IFAIL=0
C      CALL M01EAF(XS,1,M,ISEQ,IFAIL)
C      CALL F01SORT(M,XS,RWORK,ISEQ)

C      IFAIL=0
C      CALL M01EAF(YS,1,M,ISEQ,IFAIL)
C      CALL F01SORT(M,YS,RWORK,ISEQ)
      

C      IFAIL=0
C      CALL M01EBF(INDX,1,M,ISEQ,IFAIL)
C      CALL F01ISORT(M,INDX,IWORK,ISEQ)


c...debugging

c      do i=1,m
c         write(21,'(i4,f10.2,1pd14.4)')i,xs(i),ys(i)
c      enddo


C-----------------------------------------------------------------------
C Generate the B-spline representation
C-------------------------------------------------------------

      DO I=1,M
         YS(I)=DLOG10(YS(I))
         WW(I)=1.D0
      ENDDO

c=======================================================================
c calculate a cubic spline fit to the data points
c=======================================================================


c *****************************************************************
c at the moment the spline knot points are just 0.75 decades apart
c this may cause the program to fail if there are not enough data
c points between the knots. if this happens the current fix is to
c simply increase the value of devisor on the line immediately below
c this comment
c******************************************************************

c      devis=0.75
c      ncap7=(xs(m)-xs(1))/devis
c      if(ncap7.eq.0) ncap7=1
c      if(ncap7.gt.m+4)ncap7=m+4-7   ! added - not sure - acl

c      ddx=(xs(m)-xs(1))/ncap7
c      ncap7=ncap7+7

cc      if(ncap7.gt.m+4)ncap7=m+4
c      do i=4,ncap7-3
c         dlam(i)=xs(1)+(i-4)*ddx
c      enddo

c------------------------------------------------------------------
c ensure that there are enough data points to evaluate the spline
c------------------------------------------------------------------
c.................................................................
c check to ensure that there are at least 2 points between each
c knot in the initial spline fit
c................................................................. 

c      i=5
c 44   ic=0

c..................................................................
c count the number of elements in the i-4th interval between knots
c..................................................................

c      do j=1,m
c         if(xs(j).gt.dlam(i-1).and.xs(j).lt.dlam(i)) ic=ic+1
c      enddo

c.................................................................
c check that there are 2 knots in the interval
c if not take out one of the values of dlam to widen the interval
c normally this is the value of dlam(i) but for the las interval
c it is dlam(i-1)
c.................................................................

c      if(ic.lt.2) then
c         if(i.lt.ncap7-4) then
c            do j=i,ncap-4
c               dlam(j)=dlam(j+1)
c            enddo
c            ncap7=ncap7-1
c            goto 44
c         else
c            ncap7=ncap7-1
c         end if
c      else
c         if(i.lt.ncap7-4) then
c            i=i+1
c            goto 44       ! see dream: 66 interpreted as 44 - acl
c         end if
c      endif

c..............................................................
c calculate the b spline representation
c..............................................................

c#######################################################################
C
C------ MODIFIED BY ASG - 11-08-2008 -----------------------------------
C
C      NCAP7=10
C
      IF (M.LT.7)THEN
       NCAP7=8
       WRITE(0,*) '*** WARNING - NUMBER OF LINES=',M,' LT 7 ***'
       WRITE(0,*) ''
      ELSE
        NCAP7=10
      ENDIF

C
C-----------------------------------------------------------------------
C
C CALCULATE A CUBIC SPLINE FIT TO THE DATA POINTS
C
      DDL=1.D0*M/(NCAP7-7)
      DO 33 I=5,NCAP7-4
        MLAM=NINT((I-4)*DDL)
        DLAM(I)=XS(MLAM)
 33   CONTINUE
c#######################################################################

c      write(0,*)(dlam(i),i=1,ncap7)
c      write(0,*)(XS(i),i=1,M)

      ifail=0

c      call e02baf(m,ncap7,xs,ys,ww,dlam,wrk1,wrk2,co,rss,ifail)
      call f01baf(m,ncap7,xs,ys,ww,dlam,wrk1,wrk2,co,rss,ifail)

c...debugging...

c      ifail=0
c      do k=1,m
cx         call e02bbf(ncap7,dlam,co,xs(k),xsp,ifail)
c         call f01bbf(ncap7,dlam,co,xs(k),xsp,ifail)
c         write(20,'(i4,3(f10.2))')indx(k),xs(k),xsp,ys(k)
c      enddo

C-----------------------------------------------------------------------
C store the values estimated from the first approximation in
C YS and TM in XS. 
C-----------------------------------------------------------------------
      do k=1,m
         xs(k)=tm(k)
         ys(k)=fhat(k)
      enddo

      return
      end




