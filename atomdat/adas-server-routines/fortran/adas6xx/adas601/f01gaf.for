CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/f01gaf.for,v 1.1 2004/07/06 13:50:41 whitefor Exp $ Date $Date: 2004/07/06 13:50:41 $
CX
C
      SUBROUTINE F01GAF(X, F, N, ANS, ER, IFAIL)
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C 
C  PURPOSE: EVALUATES THE INTEGRAL FROM X(1) TO X(N) OF A FUNCTION
C           WHOSE VALUES AT POINTS X(I), I=1,...,N ARE STORED IN 
C           F(I), I=1,...,N. THE POINTS X(I), WHICH NEED NOT TO BE
C           EQUALLY SPACED, ARE ASSUMED TO BE DISTINCT AND ARRANGED IN
C           ASCENDING OR DESCENDING ORDER. THE INTEGRATION IS PERFORMED
C           USING A 4-POINT RULE OVER EACH INTERVAL. IF N < 3 THE
C           ROUTINE STOPS AND RETURN AN ERROR PARAMETER. IF N = 3,
C           NO INDICATION OF THE ERROR IS OBTAINABLE AND ER=0.
C
C           REFERENCES: GILL P. E. AND MILLER G. F.
C                       AN ALGORITHM FOR THE INTEGRATION OF UNEQUALLY
C                       SPACED DATA.
C                       COMPT. J., 15, PP. 80-83, 1972
C
C           REPLACEMENT FOR NAG ROUTINE D01GAF
C
C  CALLING PROGRAM: VARIOUS
C 
C  INPUT:  (R*8)  X()    =  VALUES OF THE INDEPENDENT VARIABLE 
C 
C  INPUT:  (R*8)  F()    =  VALUES OF THE DEPENDENT VARIABLE   
C 
C  INPUT:  (I*4)  N      =  NUMBER OF POINTS 
C
C  INPUT:  (I*4) IFAIL   =  0 Handling errors
C
C  OUTPUT: (R*8) ANS     =  THE ESTIMATE OF THE INTEGRAL 
C
C  OUTPUT: (R*8) ER      =  AN  ESTIMATE OF THE UNCERTAINTY IN ANS
C 
C  OUTPUT: (I*4) IFAIL   = 0 UNLESS THE ROUTINE DETECTS AN ERROR 
C
C                        = 1: N < 4 
C 
C                        = 2: X ARE NEITHER STRICTLY INCREASING NOR       
C                             STRICLTY DECREASING 
C
C                        = 3: TWO POINTS HAVE THE SAME X-VALUE.
C
C
C AUTHOR: Alessandro C. Lanzafame, University of Strathclyde
C
C DATE:  18 January 1995
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C

C
C-----------------------------------------------------------------------
      INTEGER  N,    IFAIL
      REAL*8   X(N), F(N),  ANS,  ER
C-----------------------------------------------------------------------
      INTEGER  I, J, K
      REAL*8   E
      REAL*8   H1, H2, H3, H4
      REAL*8   R1, R2, R3, R4
      REAL*8   D1, D2, D3
      REAL*8   C, S, INT 
C-----------------------------------------------------------------------
C Trap error N < 4
C-----------------------------------------------------------------------

      IF (N .LT. 4) THEN
         IF (IFAIL .EQ. 0) THEN
            IFAIL = 1
            WRITE(0,*)
     &           '*** ERROR - F01GAF: IFAIL = 1 ***'
            STOP
         ELSE
            IFAIL = 1
            RETURN
         ENDIF
      ENDIF

C-----------------------------------------------------------------------
C Check that X are increasing or decreasing
C-----------------------------------------------------------------------

      DO I=1, N-1
         IF (X(I+1) .LT. X(I)) GOTO 10
      ENDDO
      GOTO 20
 10   CONTINUE
      DO I=1,N-1
         IF (X(I+1) .GT. X(I)) THEN
            IF (IFAIL .EQ. 0) THEN
               IFAIL = 2
               WRITE(0,*)
     &              '*** ERROR - F01GAF: IFAIL = 2 ***'
               STOP
            ELSE
               IFAIL = 2
               RETURN
            ENDIF
         ENDIF
      ENDDO
 20   CONTINUE

C-----------------------------------------------------------------------
C Check that there are no X with same value
C-----------------------------------------------------------------------

      DO I=1,N-1
         IF (X(I+1) .EQ. X(I)) THEN
            IF (IFAIL .EQ. 0) THEN
               IFAIL = 3
               WRITE(0,*)
     &              '*** ERROR - F01GAF: IFAIL = 3 ***'
               STOP
            ELSE
               IFAIL = 3
               RETURN
            ENDIF
         ENDIF
      ENDDO

C-----------------------------------------------------------------------
C No error detected. Set IFAIL = 0
C-----------------------------------------------------------------------

      IFAIL = 0

C-----------------------------------------------------------------------

      INT = 0.0D0
      E   = 0.0D0
      S   = 0.0D0
      C   = 0.0D0
      R4  = 0.0D0

      J = 3
      K = N-1

      DO I =J, K

         IF (I .EQ. J) THEN

            H2 = X(J-1) - X(J-2)
            D3 = (F(J-1) - F(J-2)) / H2
            H3 = X(J) - X(J-1)
            D1 = (F(J) - F(J-1)) / H3
            H1 = H2 + H3
            D2 = (D1 - D3) / H1
            H4 = X(J+1) - X(J)
            R1 = (F(J+1) - F(J)) / H4
            R2 = (R1 - D1) / (H4 + H3)
            H1 = H1 + H4
            R3 = (R2 - D2) / H1

            INT= H2*(F(1)+H2*(D3/2.0-H2*(D2/6.0-(H2+2.0*H3)*R3/12.0)))

            S  = -H2**3*(H2*(3.0*H2+5.0*H4)+10.0*H3*H1)/60.0

         ELSE
            
            H4 = X(I+1) - X(I)
            R1 = (F(I+1) - F(I)) / H4
            R4 = H4 + H3
            R2 = (R1 - D1) / R4
            R4 = R4 + H2
            R3 = (R2 -D2) / R4
            R4 = R4 + H1
            R4 = (R3 - D3) / R4

         ENDIF

         INT = INT + H3 * ((F(I) + F(I-1))/2.0 -H3*H3*
     &         (D2+R2+(H2-H4)*R3) / 12.0)

         C  = H3**3 * (2.0*H3*H3 + 5.0*(H3*(H4+H2)+
     &        2.0 * H4 * H2)) / 120.0

         E = E +(C+S) * R4

         IF (I .EQ. J) THEN
            S = 2.0*C+S
         ELSE
            S = C
         ENDIF

         IF (I .EQ. K) THEN
            INT = INT + H4 *
     &           (F(N)-H4*(R1/2.0+H4*(R2/6.0+(2.0*H3+H4)*R3/12.0)))
            E   = E - H4**3 * R4 *(H4*(3.0*H4+5.0*H2) + 
     &           10.0*H3*(H2+H3+H4))/60.0 
         ELSE
            H1 = H2
            H2 = H3
            H3 = H4
            D1 = R1
            D2 = R2
            D3 = R3
         ENDIF

      ENDDO

      ANS = INT
      ER  = E

      RETURN
C-----------------------------------------------------------------------
      END
