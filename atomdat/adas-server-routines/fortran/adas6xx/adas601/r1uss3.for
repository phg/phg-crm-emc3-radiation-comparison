CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1uss3.for,v 1.1 2004/07/06 14:40:18 whitefor Exp $ Date $Date: 2004/07/06 14:40:18 $
CX
      SUBROUTINE R1USS3(q,w2,g2,l,sig,n,m,lmax,lmin,acc,a)
C
C This is subroutine finds the largest l value for which of the GCV function is 
C a minimum given the eigenvalues of the Q matrix q and smoothing parameter l
C for an n*n inverse problem. lmax and lmin specify the range of l values to be
C to be searched and sig returns an estimate of sigma. This routine returns
C l = -1 if GCV(l) is increasing at lmax and l =0 if no minimum is found
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      Implicit Double Precision (A-H,L,O-Z)
      Parameter(lord=1.d-10,acc1=1.d-2)
      Dimension q(*),W2(*)
C
C check to see if Gcv(lmax) is decreasing
C
      acc2=acc1
      If(lmin.le.0.d0) lmin=lmax*lord
      l=lmax
      gcv0=R1USR0(q,W2,g2,l,a,n)/R1UST0(q,l,a,n,m)**2
      dl=1.d0-acc1
      l=l*dl
      gcv=R1USR0(q,W2,g2,l,a,n)/R1UST0(q,l,a,n,m)**2
      if(gcv.gt.gcv0) then
      l=-1.d0
      sig=0
      return
      end if
C
C find the minimum of GCV
C
 11   gcv0=gcv
      l=l*dl
      gcv=R1USR0(q,W2,g2,l,a,n)/R1UST0(q,l,a,n,m)**2
       if (gcv.gt.gcv0) then
         l=l/dl
         if(acc2.gt.acc) then
           acc2=acc2*acc1
           dl=1.d0-acc2
         else
           sig=R1USR0(q,w2,g2,l,a,n)/R1UST0(q,l,a,n,m)
           return
         end if
      end if
      if(l.gt.lmin) goto 11
      l=0.d0
      sig=0.d0
C
      return
      end
