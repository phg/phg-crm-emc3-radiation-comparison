CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/r1uss6.for,v 1.1 2004/07/06 14:40:28 whitefor Exp $ Date $Date: 2004/07/06 14:40:28 $
CX
      SUBROUTINE R1USS6(q,w2,g2,l,sig,n,m,lmax,lmin,acc,a)
C
C This is subroutine finds the value of the smoothing parameter l for which
C the Craig + Brown function equals the noise variance. This routine is used
C with R1US00
C------------------------------------------------------------------------
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C------------------------------------------------------------------------
C
      Implicit Double Precision (A-H,L,O-Z)
      Dimension q(*),W2(*)
C
C Check that the value of lmax is appropriate
C
      llmax=lmax
      llmin=0.d0
      If(lmax.lt.0.d0) then
          Print *,' the largest smoothing parameter is < 0'
          Print *,'program terminated'
          STOP
      Else
  11      edf=R1USR0(q,w2,g2,llmax,a,n)/R1UST1(q,llmax,a,n,m)
          if(edf.lt.sig) then
             llmin=llmax
             llmax=llmax*10.d0
             goto 11
           END IF
      END IF
C
C locate the smoothing parameter
C
 22   l=(llmin+llmax)*0.5
      edf=R1USR0(q,w2,g2,l,a,n)/R1UST1(q,l,a,n,m)
      if(edf.gt.sig) llmax=l
      if(edf.lt.sig) llmin=l
      if(abs(1.d0-llmin/llmax).gt.acc) goto 22
C
      return
      end
