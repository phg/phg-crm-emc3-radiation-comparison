CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/rank.for,v 1.1 2004/07/06 14:43:17 whitefor Exp $ Date $Date: 2004/07/06 14:43:17 $
CX
C
      SUBROUTINE RANK(N,INDX,IRANK)
C
C PURPOSE: GIVEN INDX(1:N) AS OUTPUT FROM THE ROUTINE INDEXX, RETURNS
C          THE CORRESPONDING TABLE OF RANKS IRANK(1:N)
C          REFERENCES: Press, H. W. et al "Numerical Recipes", 1992
C
C INPUT:  (I*4)     N     = DIMENSION OF INDX AND IRANK
C
C INPUT:  (I*4)  INDX()   = INDEX TABLE
C
C OUTPUT: (I*4) IRANK()   = RANK TABLE
C
C------------------------------------------------------------------------
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C------------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      INTEGER N,INDX(N),IRANK(N)
      INTEGER J
C-----------------------------------------------------------------------

      DO J=1,N
        IRANK(INDX(J))=J
      ENDDO

      RETURN
C-----------------------------------------------------------------------
      END
