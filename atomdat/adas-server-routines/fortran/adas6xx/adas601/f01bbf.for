CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/f01bbf.for,v 1.1 2004/07/06 13:50:27 whitefor Exp $ Date $Date: 2004/07/06 13:50:27 $
CX
      SUBROUTINE F01BBF( NCAP7 , LAMDA , C , X ,S , IFAIL )
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  PURPOSE: Evaluates a cubic spline from its B-spline representation
C           Replacement for NAG routine E02BBF
C
C  CALLING PROGRAM: VARIOUS
C
C  SUBROUTINE:
C
C  INPUT: (I*4) NCAP7        = NBAR+7, where NBAR is the number of
C                              intervals (one greater than the number
C                              of interior knots, i.e. the knots 
C                              strictly in the range LAMDA(4) to 
C                              LAMDA(NCAP+4)) over which the spline
C                              is defined.
C                              CONSTRAINT: NCAP7 >= 8
C
C  INPUT:  (R*8) LAMDA()     = Values of the complete set of knots
C                              LAMDA(J), J=1,NBAR+7.
C                              CONSTRAINT: Must be in non-decreasing
C                              order with LAMDA(NCAP7-3) > LAMDA(4).
C
C  INPUT:  (R*8) C()         = The coefficients of the B-spline N_i(x),
C                              for i=1,2,...,nbar+3. The remaining 
C                              elements (from NBAR+4 to NBAR+7) are not
C                              used.
C
C  INPUT:  (R*8) X           = The argument x at which the cubic spline
C                              is to be evaluated.
C                              CONSTRAINT: 
C                                 LAMDA(4) <= X <= LAMDA(NCAP7-3)
C  INPUT:  (I*8) IFAIL       = 0 : stop if any error
C                            = 1 : continue if non-fatal error.
C
C  OUTPUT: (R*8) S           = The value of the spline, s(x)
C
C  OUTPUT: (I*4) IFAIL       = 0 : no error detected
C                            = 1 : X does not satisfy
C                                   LAMDA(4) <= X <= LAMDA(NCAP7-3)
C                            = 2 : NCAP7 < 8
C
C
C          (I*4)  KL         = GENERAL INDEX
C          (I*4)  KU         = GENERAL INDEX
C          (I*4)  K          = GENERAL INDEX
C          (I*4)  J          = GENERAL INDEX
C          (R*8)  E1         = GENERAL REAL
C          (R*8)  E2         = GENERAL REAL
C
C  ROUTINES:  NONE
C
C  AUTHORS: H. P. Summers and A. C. Lanzafame, University of Strathclyde
C
C  DATE:    11 January 1995
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER NCAP7, IFAIL
C-----------------------------------------------------------------------
      INTEGER KL, KU, KM , K, J , L
C-----------------------------------------------------------------------
      REAL*8  X, S
      REAL*8  LAMDA(NCAP7), C(NCAP7)
C-----------------------------------------------------------------------
      REAL*8  E1, E2 , E3 , V(4)
C-----------------------------------------------------------------------
C  INITIAL CHECKS
C-----------------------------------------------------------------------
C
      IF ((X.LT.LAMDA(4)).OR.(X.GT.LAMDA(NCAP7-3))) THEN
         IF (IFAIL .EQ. 0) THEN
            IFAIL = 1
            STOP '*** F01BBF ERROR: IFAIL = 1 ***'
         ELSE IF (IFAIL .EQ. 1) THEN
            RETURN
         ENDIF
      ENDIF
C
      IF (NCAP7.LT.8) THEN
         IF (IFAIL .EQ. 0) THEN
            IFAIL = 2
            STOP '*** F01BBF ERROR: IFAIL = 2 ***'
         ELSE IF (IFAIL .EQ. 1) THEN
            IFAIL = 2
            RETURN
         ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C  LOCATE X IN LAMDA ARRAY
C-----------------------------------------------------------------------
C
      KL = 4
      KU = NCAP7-3
 10   IF ((KU-KL).GT.1) THEN
         KM = (KU+KL)/2
         IF ((LAMDA(NCAP7).GT.LAMDA(1)).EQV.(X.GT.LAMDA(KM))) THEN
            KL = KM
         ELSE
            KU = KM
         ENDIF
         GO TO 10
      ENDIF
C
      K = KL + 1
C
C-----------------------------------------------------------------------
C  GENERATE AND EVALUATE B-SPLINES
C-----------------------------------------------------------------------
C
      E1 = X - LAMDA(K-1)
      E2 = LAMDA(K) - X
      V(1) = 1.0D0/(LAMDA(K)-LAMDA(K-1))
      DO J = 2, 4
         V(J) = E1*V(J-1)/(LAMDA(K-1+J)-LAMDA(K-1))
      ENDDO
      DO J = 1, 3
         E3 = X - LAMDA(K-1-J)
         V(1) = E2*V(1)/(LAMDA(K)-LAMDA(K-1-J))
         IF (J.LT.3) THEN
            DO L=2, 4-J
               V(L) = (E3*V(L-1)+(LAMDA(K-1+L)-X)*V(L))/
     &                (LAMDA(K-1+L)-LAMDA(K-1-J))
            ENDDO
         ENDIF
      ENDDO
C
      S=0.0D0
      DO L=1, 4
         S = S + C(K+L-5)*V(L)*(LAMDA(K+L-1)-LAMDA(K+L-5))
      ENDDO
C
C-----------------------------------------------------------------------
C
      RETURN
C-----------------------------------------------------------------------
      END
