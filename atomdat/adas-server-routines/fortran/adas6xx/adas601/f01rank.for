CX SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas6xx/adas601/f01rank.for,v 1.1 2004/07/06 13:50:48 whitefor Exp $ Date $Date: 2004/07/06 13:50:48 $
CX
C
      SUBROUTINE F01RANK(N,A,INDX,IRANK)
      IMPLICIT NONE
C
C PURPOSE: GIVEN A REAL ARRAY A, RETURN SORTING INDEX AND RANK TABLE
C          IN ASCENDING ORDER
C
C INPUT:  (I*4)     N     = DIMENSION OF REAL ARRAY
C
C INPUT:  (R*8)     A()   = REAL ARRAY TO BE SORTED
C
C OUTPUT: (I*4)  INDX()   = INDEX TABLE
C
C OUTPUT: (I*4) IRANK()   = RANK TABLE
C
C AUTHOR: Alessandro Lanzafame, University of Strathclyde
C
C DATE:   24 January 1996
C
C VERSION: 1.1				DATE: 03-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC.)
C	    - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER N    , INDX(N),  IRANK(N)
      REAL*8  A(N)
C-----------------------------------------------------------------------

      CALL INDEXX( N , A     ,  INDX  )
      CALL RANK(   N , INDX  ,  IRANK )


      RETURN
C-----------------------------------------------------------------------
      END
