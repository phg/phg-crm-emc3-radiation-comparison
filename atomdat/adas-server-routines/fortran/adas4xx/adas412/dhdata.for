CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dhdata.for,v 1.4 2009/11/01 19:16:58 mog Exp $ Date $Date: 2009/11/01 19:16:58 $
CX
       SUBROUTINE DHDATA( YEAR   , YEARDF , TITLF  , IFAIL
     &                  , IZ0    , IZ1    , ICLASS , ITMAX  , IEVCUT
     &                  , ITDIMD , ITMAXD , IDMAXD , IZMAXD
     &                  , DTEV   , DDENS
     &                  , DTEVD  , DDENSD , DRCOFD , ZDATA
     &                  , DRCOFI
     &                  )
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: DHDATA *********************
C
C PURPOSE : TO EXTRACT 'SANC0' COLLISIONAL DIELECTRONIC DATA
C
C CALLING PROGRAM: IONBAL (ADAS412)
C	
C	NOTE    : THE SOURCE DATA IS CONTAINED AS SEQUENTIAL DATASETS
C           AS FOLLOWS:
C
C                   (1) JETSHP.ACD<YR>#<IEL).DATA
C                   (2) JETSHP.SCD<YR>#<IEL>.DATA
C                   (3) JETSHP.CCD<YR>#<IEL>.DATA
C                   (4) JETSHP.PRB<YR>#<IEL>.EV<CUT>.DATA
C                   (5) JETSHP.PLT<YR>#<IEL>.EV<CUT>.DATA
C                   (6) JETSHP.PRC<YR>#<IEL>.EV<CUT>.DATA
C                   (7) JETSHP.PLS<YR>#<IEL>.DATA
C
C       WHERE, <YR>  = TWO INTEGERS  FOR THE YEAR SELECTED
C              <IEL> = ELEMENT NAME
C              <CUT> = ENERGY CUT-OFF (EV)
C
C              IF <CUT> = 0 THEN .EV<CUT> IS DELETED FROM ABOVE FILES.
C
C#
C# mar20-95 Alessandro Lanzafame
C#          conversion to Unix
C#
C#                  (1) /ADAS/adas/adf11/acd<YR>/acd<YR>_<IEL>.dat
C#                  (2) /ADAS/adas/adf11/scd<YR>/scd<YR>_<IEL>.dat
C#                  (3) /ADAS/adas/adf11/ccd<YR>/ccd<YR>_<IEL>.dat
C#                  (4) /ADAS/adas/adf11/prb<YR>/prb<YR>_<IEL>.dat
C#                  (5) /ADAS/adas/adf11/plt<YR>/plt<YR>_<IEL>.dat
C#                  (6) /ADAS/adas/adf11/prc<YR>/prc<YR>_<IEL>.dat
C#                  (7) /ADAS/adas/adf11/pls<YR>/pls<YR>_<IEL>.dat
C#
C INPUT  : (C*2)  YEAR      = YEAR OF DATA
C          (C*2)  YEARDF    = DEFAULT YEAR OF DATA IF REQUESTED YEAR
C                             DOES NOT EXIST
C          (I*4)  IZ0       = NUCLEAR CHARGE
C          (I*4)  IZ1       = MINIMUM ION CHARGE + 1
C          (I*4)  ICLASS    = CLASS OF DATA (1 - 6)
C          (I*4)  ITMAX     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C          (I*4)  IEVCUT    = ENERGY CUT-OFF (EV)
C          (R*8)  DTEV()    = DLOG10(ELECTRON TEMPERATURES (EV))
C          (R*8)  DDENS()   = DLOG10(ELECTRON DENSITIES (CM-3))
C
C OUTPUT : (C*80) TITLF     = INFORMATION STRING
C          (I*4)  ITDIMD    = MAXIMUM NUMBER OF DATA TEMP & DENS
C          (I*4)  ITMAXD    = NUMBER OF DATA DTEVD()
C          (I*4)  IDMAXD    = NUMBER OF DATA DDENS()
C          (I*4)  IZMAXD    = NUMBER OF DATA ZDATA()
C          (I*4)  ITDIMD    = MAXIMUM NUMBER OF DATA TEMP & DENS
C          (I*4)  ZDATA()   = Z1 CHARGES IN DATASET
C          (I*4)  IFAIL     = 0    IF ROUTINE SUCCESSFUL
C                           = 1    IF ROUTINE OPEN STATEMENT FAILED
C          (R*8)  DTEVD()   = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C          (R*8)  DDENSD()  = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C          (R*8)  DRCOFD()  = DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C          (R*8)  DRCOFI()  = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C
C PROGRAM: (C*2)  SEQUA()   = ION NAMES FOR A PARTICULAR IZ0
C          (C*36) DSNAME    = FILE NAME ( SEE ABOVE TYPES )
C          (C*80) STRING    = GENERAL VARIABLE
C          (C*80) BLANK     = BLANK STRING
C          (C*2)  YEARSV    = LAST YEAR USED IN THIS ROUTINE
C          (I*4)  IREAD     = INPUT STREAM FOR OPEN STATEMENT
C          (I*4)  IZ0SV     = LAST IZ0 USED IN THIS ROUTINE
C          (I*4)  ICLSV     = LAST ICLASS USED IN THIS ROUTINE
C          (I*4)  INDXZ1    = LOCATION OF IZ1 IN ZDATA()
C          (I*4)  LCK       = MUST BE GREATER THAN 'ITMAXD' & 'IDMAXD'
C                             & 'ITMAX' - ARRAY SIZE FOR SPLINE CALCS.
C          (R*8)  A()       = GENERAL ARRAY
C          (R*8)  DRCOF0(,) = INTERPOLATION OF DRCOFD(,,) W.R.T DTEV()
C          (L*8)  LEXIST    = TRUE --- FILE TO OPEN EXISTS ELSE NOT
C
C PE BRIDEN = ADDED VARIABLES (14/01/91)
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                             SPLINE ROUTINE 'XXSPLN', SEE 'XXSPLN'.
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO X-AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO X-AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLN')
C
C
C          (R*8)  DY()    = SPLINE INTERPOLATED DERIVATIVES
C
C          (R*8 ADAS FUNCTION - 'R8FUN1' ( X -> X) )
C
C AUTHOR : JAMES SPENCE (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/80
C          JET  EXT. 4866
C
C DATE   : 22/02/90
C
C DATE   : 21/08/90 PE BRIDEN - REVISION: SEQUA(43) CHANGED ('TE'->'TC')
C
C DATE   : 08/10/90 PE BRIDEN - REVISION: RENAMED SUBROUTINE
C
C DATE   : 12/11/90 PE BRIDEN - CORRECTION: MOVE THE SETTING OF 'INDXZ1'
C                                           TO AFTER THE  '20 CONTINUE'
C                                           STATEMENT.   ALSO SAVE  THE
C                                           VALUE OF 'IZ1MIN'.
C
C DATE   : 14/01/91 PE BRIDEN - ADAS91:     CALLS TO NAG SPLINE ROUTINES
C                                           'E01BAF' & 'E02BBF' REPLACED
C                                           BY  CALLS   TO  ADAS  SPLINE
C                                           ROUTINE 'XXSPLN'.
C
C DATE   : 25/06/91 PE BRIDEN - CORRECTION: CHANGED FOLLOWING DIMENSION:
C                                            'DIMENSION DRCOFI(ITDIMD)'
C                                           TO
C                                            'DIMENSION DRCOFI(ITMAX)'
C
C
C DATE   : 25/06/91 HP SUMMERS - REVISION: RENAMED FROM D2DATA TO DHDATA
C                                          SET IOPT=4 FOR IONBAL
C DATE   : 27/04/92 PE BRIDEN  - ADDED DEFAULT YEAR FOR DATA IF
C                                REQUESTED YEAR DOES NOT EXIST.
C                                (ADDED 'YEARDF'), INTRODUCED IFAIL=-1
C                                IF DEFAULT YEAR WAS USED AND NOT THE
C                                REQUESTED YEAR.
C
C
C DATE   : 20/03/95 AC LANZAFAME - CONVERSION TO UNIX
C                                 CHANGED OPEN STATEMENT AND FILE
C                                 NAMES
C                                 ADIR ADIR1 ADDED
C                                 DSNAME FROM C*30 TO C*40
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLN     ADAS      CUBIC SPLINE INTERPOLATION/EXTRAPOLATION
C	     R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C VERSION 1.1							DATE: 29-10-97
C		RICHARD MARTIN
C		ADAS412 - PUT UNDER SCCS CONTROL.
C 
C VERSION: 1.2							DATE: 01-12-97
C		RICAHRD MARTIN
C		REMOVED DUPLICATE VARIABLE DECLARATION.
C
C VERSION: 1.3							DATE: 23-11-98
C		RICHARD MARTIN & DAVID BROOKS
C		REMOVED 'DSNAME(12:13) = YEARDF' STATEMENT (FOR IBM).		
C
C VERSION : 1.4
C DATE    : 31-10-2009
C MODIFIED: Martin O'Mullane
C               - Use standard xxcase routine in place of c1u2lc.
C
C-----------------------------------------------------------------------
C
       INTEGER   L1, LENSTR, LSTR, IREAD, LCK, I
C
       PARAMETER ( L1    =  1 )
       PARAMETER ( IREAD = 12 , LCK = 101 )
C
       INTEGER   IFAIL, IOPT
       INTEGER   IZ, IZ0, IZ1, ICLASS,  IZ0SV,	ICLSV, IEVSV, IEVCUT 
       INTEGER   ITMAX, ITDIMD , ITMAXD , IDMAXD , IZMAXD
       INTEGER   LEVMIN,  LEN, ID,  IT
       INTEGER   IZMAX,  IZ1MIN,  IZ1MAX, INDXZ1
       INTEGER   ZDATA(ITDIMD)
C
       REAL*8    DTEV(ITMAX)   , DDENS(ITMAX)
       REAL*8    DTEVD(ITDIMD) , DDENSD(ITDIMD) 
       REAL*8    DRCOFD(ITDIMD,ITDIMD,ITDIMD)
       REAL*8    DRCOFI(ITMAX)
C
       DIMENSION SEQUA(50)
C
       REAL*8    A(LCK)
       REAL*8    DY(LCK)
       REAL*8    DRCOF0(LCK,LCK)
C
       CHARACTER YEAR*2  , YEARDF*2  , YEARSV*2  , TITLF*80  , EVCUT*6
       CHARACTER SEQUA*2 , DSNAME*80 , STRING*80 , BLANKS*80
       CHARACTER ADIR1*80, ADIR*80, SQQ*2, C1U2LC*1
       CHARACTER CENTROOT*80
C
       LOGICAL   LEXIST  , LSETX
C
       EXTERNAL  R8FUN1
C
       SAVE      IZ1MIN
C
C-----------------------------------------------------------------------
C
       DATA  SEQUA/'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE',
     &             'NA','MG','AL','SI','P ','S ','CL','AR','K ','CA',
     &             'SC','TI','V ','CR','MN','FE','CO','NI','CU','ZN',
     &             'GA','GE','AS','SE','BR','KR','RB','SR','Y ','ZR',
     &             'NB','MO','TC','RU','RH','PD','AG','CD','IN','SN'/
       DATA BLANKS/'
     &                           '/
C
       DATA YEARSV/'  '/
       DATA IZ0SV /0   /
       DATA ICLSV /0   /
       DATA IEVSV /0   /
C
C-----------------------------------------------------------------------
C
       IF(YEAR.EQ.YEARSV.AND.IZ0.EQ.IZ0SV.AND.ICLASS.EQ.ICLSV.AND.
     &    IEVCUT.EQ.IEVSV) GOTO 20
       YEARSV = YEAR
       IZ0SV  = IZ0
       ICLSV  = ICLASS
       IEVSV  = IEVCUT
C
C------ENERGY CUTOFF----------------------------------------------------
C
      IF( IEVCUT.GT.0 ) THEN
          WRITE(EVCUT,1050) IEVCUT
          DO 50 I = 1 , 6
             IF( EVCUT(I:I).EQ.' ' ) LEVMIN = I
   50     CONTINUE
          LEVMIN = LEVMIN + 1
      END IF
C
C------FILE NAME--------------------------------------------------------
C
   10  IF (SEQUA(IZ0)(2:2).EQ.' ') THEN
            LEN=1
       ELSE
            LEN=2
       ENDIF
C
C# mar20-95 Alessandro Lanzafame
C#          conversion to Unix
C#
       CALL GETENV('ADASCENT',CENTROOT)
       call xxcase(SEQUA(IZ0), sqq, 'LC')
C        IF (LEN.EQ.1) THEN
C           SQQ=C1U2LC(SEQUA(IZ0)(1:LEN))
C        ELSE
C           SQQ=C1U2LC(SEQUA(IZ0)(1:1))//C1U2LC(SEQUA(IZ0)(LEN:LEN))
C        ENDIF
       LSTR=LENSTR(CENTROOT)
       ADIR1=CENTROOT(1:LSTR)//'/adf11/'
       LSTR=LENSTR(ADIR1)
       
       IF     (ICLASS.EQ.1) THEN
           ADIR=ADIR1(1:LSTR)//'acd'//YEAR
           LSTR=LENSTR(ADIR)
           DSNAME=ADIR(1:LSTR)//'/acd'//YEAR//'_'//SQQ(1:LEN)//'.dat'
       ELSEIF (ICLASS.EQ.2) THEN
           ADIR=ADIR1(1:LSTR)//'scd'//YEAR
           LSTR=LENSTR(ADIR)
           DSNAME=ADIR(1:LSTR)//'/scd'//YEAR//'_'//SQQ(1:LEN)//'.dat'
       ELSEIF (ICLASS.EQ.3) THEN
           ADIR=ADIR1(1:LSTR)//'ccd'//YEAR
           LSTR=LENSTR(ADIR)
           DSNAME=ADIR(1:LSTR)//'/ccd'//YEAR//'_'//SQQ(1:LEN)//'.dat'
       ELSEIF (ICLASS.EQ.4) THEN
         IF( IEVCUT.EQ.0 ) THEN
           ADIR=ADIR1(1:LSTR)//'prb'//YEAR
           LSTR=LENSTR(ADIR)
           DSNAME=ADIR(1:LSTR)//'/prb'//YEAR//'_'//SQQ(1:LEN)//'.dat'
         ELSE
           ADIR=ADIR1(1:LSTR)//'prb'//YEAR

           DSNAME=ADIR//'/prb'//YEAR//'_'//SQQ(1:LEN)//
     &             '.EV'//EVCUT(LEVMIN:6)//'.dat'
         END IF
       ELSEIF (ICLASS.EQ.5) THEN
         IF( IEVCUT.EQ.0 ) THEN
           ADIR=ADIR1//'plt'//YEAR
           DSNAME=ADIR//'/plt'//YEAR//'_'//SQQ(1:LEN)//'.dat'
         ELSE
           ADIR=ADIR1//'plt'//YEAR
           DSNAME=ADIR//'/plt'//YEAR//'_'//SQQ(1:LEN)//
     &             '.EV'//EVCUT(LEVMIN:6)//'.dat'
         END IF
       ELSEIF (ICLASS.EQ.6) THEN
         IF( IEVCUT.EQ.0 ) THEN
           ADIR=ADIR1//'prc'//YEAR
           DSNAME=ADIR//'/prc'//YEAR//'_'//SQQ(1:LEN)//'.dat'
         ELSE
           ADIR=ADIR1//'prc'//YEAR
           DSNAME=ADIR//'/prc'//YEAR//'_'//SQQ(1:LEN)//
     &             '.EV'//EVCUT(LEVMIN:6)//'.dat'
         END IF
       ELSEIF (ICLASS.EQ.7) THEN
           ADIR=ADIR1//'pls'//YEAR
           DSNAME=ADIR//'/pls'//YEAR//'_'//SQQ(1:LEN)//'.dat'
       ENDIF

C# mar20-95 end
C
C------PE BRIDEN - MODIFICATION 27/04/92 - INCLUSION OF DEFAULT YEAR ---
C
C
C------DOES FILE TO BE OPEN EXIST OR NOT--------------------------------
C
       INQUIRE(FILE=DSNAME,EXIST=LEXIST)
       IF( (.NOT.LEXIST) .AND. (YEAR.NE.YEARDF) ) THEN
           WRITE(0,1060)DSNAME , YEARDF
C           DSNAME(12:13) = YEARDF			!FOR IBM
           INQUIRE(FILE=DSNAME,EXIST=LEXIST)
           IFAIL=-1
           YEARSV=YEARDF
       ENDIF
       IF( .NOT.LEXIST ) GO TO 9999
C
       TITLF=BLANKS
       WRITE(TITLF,1000) DSNAME
C
C------READ FILE # IREAD------------------------------------------------
C
C# mar20-95 Alessandro Lanzafame
C#          conversion to Unix
C#
C#     OPEN(UNIT=IREAD,FILE=DSNAME,ACTION='READ')
       OPEN(UNIT=IREAD,FILE=DSNAME,STATUS='OLD')
C# mar20-95 end
C
       READ(IREAD,1010) IZMAX , IDMAXD , ITMAXD , IZ1MIN , IZ1MAX
C
       READ(IREAD,1020) STRING
       READ(IREAD,1040) ( DDENSD(ID) , ID = 1 , IDMAXD )
       READ(IREAD,1040) ( DTEVD(IT)  , IT = 1 , ITMAXD )
C
       IZMAXD = 0
       DO 150 IZ = IZ1MIN , IZ1MAX
          IZMAXD = IZMAXD + 1
          ZDATA(IZMAXD) = IZ
C         IF( IZ .EQ. IZ1 ) INDXZ1 = IZ
          READ(IREAD,1020)STRING
          DO 100 IT = 1 , ITMAXD
             READ(IREAD,1040) ( DRCOFD(IZMAXD,IT,ID) , ID = 1 , IDMAXD )
  100     CONTINUE
  150  CONTINUE
C
       CLOSE(12)
C
C-----------------------------------------------------------------------
C
   20  CONTINUE
C
C------PE BRIDEN - CORRECTION 12/11/90 - SET INDXZ1 AFTER '20 CONTINUE'-
C
       INDXZ1 = IZ1 - IZ1MIN + 1
C
C-----------------------------------------------------------------------
C
C
C>>>>>>INTERPOLATE DRCOFD(,,,) W.R.T TEMPERATURE
C
      LSETX = .TRUE.
      IOPT  = 4
C
         DO 200 ID = 1 , IDMAXD
C
               DO 220 IT = 1 , ITMAXD
                  A(IT) = DRCOFD(INDXZ1,IT,ID)
  220          CONTINUE
C
		 CALL XXSPLN( LSETX  , IOPT  , R8FUN1	  ,
     &			  ITMAXD , DTEVD , A		  ,
     &			  ITMAX  , DTEV  , DRCOF0(1,ID) ,
     &			  DY
     &                 )

  200    CONTINUE
C
C>>>>>>INTERPOLATE ABOVE RESULT W.R.T DENSITY
C
      LSETX = .TRUE.
      IOPT  = 4
C
         DO 300 IT = 1 , ITMAX
C
               DO 320 ID = 1 , IDMAXD
                  A(ID) = DRCOF0(IT,ID)
  320          CONTINUE
C
            CALL XXSPLN( LSETX  , IOPT      , R8FUN1     ,
     &                   IDMAXD , DDENSD    , A          ,
     &                   L1     , DDENS(IT) , DRCOFI(IT) ,
     &                   DY
     &                 )
     
C
  300    CONTINUE
C

       RETURN
C
C-----------------------------------------------------------------------
C DATA SET OEPNING/EXISTENCE ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999  IFAIL  = 1
       YEARSV = '   '
       IZ0SV  = 0
       ICLSV  = 0
       RETURN
C
C-----------------------------------------------------------------------
C
 1000  FORMAT('FILE = ',1A39)
 1010  FORMAT(5I5)
 1020  FORMAT(1A80)
 1040  FORMAT(8F10.5)
 1050  FORMAT(I6)
 1060  FORMAT(1X,'NOTE: REQUESTED DATASET - ',A39,' DOES NOT EXIST.'/
     &        7X,      'USING DEFAULT YEAR (',A2,') DATASET INSTEAD')
       END
