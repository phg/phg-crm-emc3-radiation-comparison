CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dclnorm.for,v 1.1 2004/07/06 13:29:00 whitefor Exp $ Date $Date: 2004/07/06 13:29:00 $
CX
      SUBROUTINE DCLNORM( NDLEV  , NDMET  ,
     &                   NORD   ,
     &                   STCK   ,
     &                   COEF
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCLNORM *********************
C
C  PURPOSE: TO NORMALISE LINE EMISSIVITY
C           ADAPTED FROM B6NORM
C
C  CALLING PROGRAM:  XCOEF
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C
C  INPUT :  (R*8)  STCK(,) = POPULATION MATRIX COVERING ALL NON-METAST-
C                            ABLE/ORDINARY EXCITED LEVELS AS FUNCTION
C                            OF METASTABLE INDEX.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C
C  I/O   :  (R*8)  COEF    = INPUT:
C                            LINE EMISSIVITY 
C                               A(J->K) * [N(J)/N(1)]
C                            AT FIXED TEMPERATURE AND DENSITY.
C                            (UNITS: ERGS CM3 SEC-1)    >>>>?<<<<
C                            OUTPUT:
C                            NORMALISED TO TOTAL STAGE POPULATION
C                               [N(1)/SUM(N(I)] * A(J->K) * [N(J)/N(1)]
C
C           (I*4)  IS1     = ORDINARY EXCITED LEVEL INDEX
C
C           (R*8)  STOTX   = VARIABLE USED TO SUM STAGE TOTAL POPULATN.
C                            (INITIAL VALUE = 1 => GROUND)
C
C ROUTINES: NONE
C
C NOTE:
C
C AUTHOR:  A. Lanzafame, University of Strathclyde
C
C DATE:    apr28-95
C
C UPDATE:
C
C VERSION 1.1						DATE: 27-10-97
C		RICHARD MARTIN.
C		PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV              , NDMET              ,
     &           NORD               , IS1
C-----------------------------------------------------------------------
      REAL*8     COEF,  STOTX
C-----------------------------------------------------------------------
      REAL*8     STCK(NDLEV,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      STOTX = 1.0
C
         DO 1 IS1=1,NORD
c#          STOTX = STOTX + DBLE(STCK(IS1,1))
            STOTX = STOTX + STCK(IS1,1)
    1    CONTINUE
C
      STOTX = 1.0 / STOTX
C
      COEF  = STOTX * COEF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
