CX IDL UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dcpopo.for,v 1.1 2004/07/06 13:29:13 whitefor Exp $ Date $Date: 2004/07/06 13:29:13 $
CX
      SUBROUTINE DCPOPO( NDTEM  , NDMET , NDLEV ,
     &                   MAXT   , NMET  , NORD  ,
     &                            DENSA , IMETR , IORDR ,
     &                            LRSEL , LHSEL ,
     &                            RATIA , RATHA ,
     &                   STACK  , STVR  , STVH  ,
     &                   POPAR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCPOPO *********************
C
C  PURPOSE: TO CONSTRUCT ORDINARY/NON-METASTABLE LEVEL POPULATIONS.
C
C  CALLING PROGRAM:  XCOEF
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMP/DENS PAIRS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  MAXT    = NO. OF INPUT TEMP/DENS PAIRS ( 1 ->'NDTEM')
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES LEVELS ( 1 ->'NDMET')
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ( 1 ->'NDLEV')
C
C  INPUT :  (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (L*4)  LRSEL   = .TRUE.  => FREE ELECTRON RECOMBINATION
C                                       REQUESTED.
C                          = .FALSE. => FREE ELECTRON RECOMBINATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN REQUESTED.
C                          = .FALSE. => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN NOT REQUESTED.
C
C  INPUT :  (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C  INPUT :  (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C
C  INPUT : (R*8) STACK(,,) = ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C  INPUT : (R*8) STVR(,)   = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C  INPUT : (R*8) STVH(,)   = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C
C  I/O   :  (R*8)  POPAR(,) = LEVEL POPULATIONS
C                              1st DIMENSION: LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                               ON INPUT : CONTAINS POPULATIONS FOR
C                                          METASTABLE LEVELS ONLY.
C                               ON OUTPUT: CONTAINS POPULATIONS FOR
C                                          ALL LEVELS.
C
C           (R*8) DCOEF     = DENSITY MULTIPLIED BY RELEVANT RATIOS  FOR
C                             CALCULATING RECOMBINATION CONTRIBUTIONS.
C
C           (I*4) IT        = TEMPERATURE ARRAY INDEX
C           (I*4) IN        = DENSITY ARRAY INDEX
C           (I*4) IO        = ORDINARY LEVEL ARRAY INDEX
C           (I*4) IM        = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C AUTHOR:  HP SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    27/06/91
C

C VERSION 1.1  RICHARD MARTIN					DATE: 27-10-97
C			PUT UNDER SCCS CONTROL.
C			NAME CHANGED FROM BHPOPO TO DCPOPO
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM        , NDMET   , NDLEV   ,
     &          MAXT         , NMET    , NORD
      INTEGER   IT           , IM      , IO
C-----------------------------------------------------------------------
      REAL*8    DCOEF
C-----------------------------------------------------------------------
      LOGICAL   LRSEL        , LHSEL
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)                , IORDR(NDLEV)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDTEM)                ,
     &          RATIA(NDTEM)                , RATHA(NDTEM)
      REAL*8    STVR(NDLEV,NDTEM)           , STVH(NDLEV,NDTEM)
      REAL*8    POPAR(NDLEV,NDTEM)
      REAL*8    STACK(NDLEV,NDMET,NDTEM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  ORDINARY/NON-METSTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         DO 1 IT=1,MAXT
            DO 3 IO=1,NORD
               POPAR(IORDR(IO),IT)=0.0D0
                  DO 4 IM=1,NMET
                     POPAR(IORDR(IO),IT)= POPAR(IORDR(IO),IT) +
     &                ( POPAR(IMETR(IM),IT) * STACK(IO,IM,IT) )
    4             CONTINUE
    3       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD FREE ELECTRON RECOMB. CONTRIBUTION TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            DO 5 IT=1,MAXT
               DCOEF = RATIA(IT)*DENSA(IT)
                  DO 7 IO=1,NORD
                     POPAR(IORDR(IO),IT)=POPAR(IORDR(IO),IT) +
     &                                      ( DCOEF*STVR(IO,IT) )
    7             CONTINUE
    5       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD CHARGE EXCHANGE RECOMB. CONTRIBUTION TO ORDINARY LEVEL POPULATIONS
C-----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            DO 8 IT=1,MAXT
               DCOEF = RATHA(IT)*RATIA(IT)*DENSA(IT)
                  DO 10 IO=1,NORD
                     POPAR(IORDR(IO),IT)=POPAR(IORDR(IO),IT) +
     &                                      ( DCOEF*STVH(IO,IT) )
   10             CONTINUE
    8       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
