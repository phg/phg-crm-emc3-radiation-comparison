CX IDL UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dcspf0.for,v 1.1 2004/07/06 13:29:16 whitefor Exp $ Date $Date: 2004/07/06 13:29:16 $
CX

      SUBROUTINE DCSPF0( REP    ,
     &                   DSNINC , ROOTPATH2, FILEIB, ELEM
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCSPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS412
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSNINC  = INPUT COPASE DATA SET NAME.
C
C  OUTPUT: (C*2)   FILEIB  = YEAR OF REQUESTED ADF11 DATA.
C
C  OUTPUT: (C*2)   ELEM    = SYMBOL OF SELECTED ION.
C
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C	     (C*80)	 ROOTPATH2= PATH TO ADF11 DIRECTORY
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, OCTOBER 1997
C
C VERSION: 1.1						DATE: 27-10-97
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3      , DSNINC*80, 	ROOTPATH2*80
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU, NION
      PARAMETER( PIPEIN=5  , PIPEOU=6, NION=20)

      CHARACTER*2  FILEIB,	ELEM
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSNINC
      READ(PIPEIN,'(A)') FILEIB
      READ(PIPEIN,'(A)') ELEM   
      READ(PIPEIN,'(A)') ROOTPATH2             
C
C-----------------------------------------------------------------------
C
      RETURN
      END
