CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dcstka.for,v 1.1 2004/07/06 13:29:25 whitefor Exp $ Date $Date: 2004/07/06 13:29:25 $
CX
      SUBROUTINE DCSTKA( NDLEV  , NDMET  ,
     &                   NORD   , NMET   ,
     &                   IORDR  , IMETR  ,
     &                   CMAT   , CC     ,
     &                   STCK
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCSTKA *********************
C
C  PURPOSE: TO STACK UP IN  'STCK'  THE NON-METASTABLE/ORDINARY EXCITED
C           LEVEL POPULATION DEPENDENCE ON METASTABLE LEVEL FOR A GIVEN
C           TEMPERATURE AND DENSITY.
C
C  CALLING PROGRAM:  XCOEF
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C
C  INPUT :  (I*4)  NORD    = NUMBER OF ORDINARY EXCITED LEVELS
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLE LEVELS
C
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C  INPUT :  (I*4)  IORDR() =INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                            LEVEL LIST.
C                            (ARRAY SIZE = 'NDLEV' )
C
C  INPUT :  (R*8)  CMAT(,) = INVERTED   RATE   MATRIX   COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C
C  INPUT :  (R*8)  CC(,)   = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C
C  OUTPUT:  (R*8)  STCK(,) = POPULATION MATRIX COVERING ALL NON-METAST-
C                            ABLE/ORDINARY EXCITED LEVELS AS FUNCTION
C                            OF METASTABLE INDEX.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C
C           (I*4)  IS1     = ORDINARY EXCITED LEVEL INDEX
C           (I*4)  IS2     = ORDINARY EXCITED LEVEL INDEX
C           (I*4)  IM      = METASTABLE LEVEL ARRAY INDEX
C
C           (R*8)  POP     = VARIABLE USED TO SUM POPULATION VALUES
C
C
C ROUTINES: NONE
C
C NOTE:
C        IF:     n  =  number of ordinary/non-metastable levels
C                m  =  number of metastable levels
C           Ro(nxn) = Rate matrix (sec-1) covering transistions between
C                     all possible pairs of ordinary levels.
C                     row   : final   level
C                     column: initial level
C                     (Inverse Ro-1(nxn) = 'CMAT(,)' )
C           Rm(nxm) = Rate matrix (sec-1) covering transistions between
C                     all combinations of ordinary and metastable level
C                     ( = 'CC(,)' - ordinary level part )
C           P(nxm)  = Population matrix giving the population dependence
C                     of each ordinary level on metastable level.
C                     ( = 'STCK(,)' )
C
C           Therefore:  Ro(nxn).P(nxm) = Rm(nxm)
C
C            =>          P(nxm)  = Ro-1(nxn).Rm(nxm)
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  20/05/93 - P BRIDEN: STCK ARRAY CHANGED FROM REAL*8 -> REAL*4
C
C          Alessandro Lanzafame, STCK back to real*8
C
C VERSION 1.1 							DATE: 27-10-97
C		RICHARD MARTIN
C		PUT UNDER SCCS CONTROL
C	
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDLEV              , NDMET              ,
     &           NORD               , NMET
      INTEGER    IS1                , IS2                , IM
C-----------------------------------------------------------------------
      REAL*8     POP
C-----------------------------------------------------------------------
      INTEGER    IORDR(NDLEV)       , IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8     CMAT(NDLEV,NDLEV)  , CC(NDLEV,NDLEV)
      REAL*8     STCK(NDLEV,NDMET)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
         DO 1 IS1=1,NORD
            DO 2 IM=1,NMET
               POP = 0.0D0
                 DO 3 IS2=1,NORD
                    POP = POP - (CMAT(IS1,IS2)*CC(IORDR(IS2),IMETR(IM)))
    3            CONTINUE
cx               STCK(IS1,IM) = REAL(POP)
               STCK(IS1,IM) = POP
    2       CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
