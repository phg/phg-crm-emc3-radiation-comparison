      SUBROUTINE DCINIT(  ELEM, NION, NIONS, IZ,
     &                    SQI  ,SEQ   ,IZ0, SPEC)
C
	IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCINIT *********************
C
C  PURPOSE: DETRMINATION OF CHARGE OF THE SELECTED ION AND OBTAIN
C	    SPECTROSCOPIC NOTATION.
C
C  CALLING PROGRAM: ADAS412
C
C  INPUT:	
C	
C     (C*2) ELEM    : SELECTED ELEMENT.
C
C     (I*4) NIONS   : NUMBER OF SELECTED IONS.
C 
C     (I*4) IZ(NION): CHARGES OF THE SELECTED IONS.
C
C  OUTPUT:
C
C     (C*2) SQI(NION): ISO-ELECTRONIC SEQUENCE - EG LI.
C
C     (C*7) SEQ(NION): ISO-ELECTRONIC SEQUENCE - EG Li-like.
C
C     (I*4) IZ0      : NUCLEAR CHARGE.
C 
C     (C*11) SPEC(NIONS):   ION IN SPECTROSCOPIC NOTATION.	
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, OCTOBER 1997
C
C VERSION: 1.1						DATE: 27-10-97
C VERSION: 1.2						DATE: 01-12-97
C	MODIFIED: RICHARD MARTIN
C		- RE-ORDERED VARIABLE DECLARATIONS.
C
C VERSION : 1.3                               
C DATE    : 31-10-2009
C MODIFIED: Martin O'Mullane
C		- Rename ad dcinit.
C
C-----------------------------------------------------------------------

      INTEGER   L, NION, NIONS
      INTEGER   ISPEC(9)
      INTEGER   IZ(NION), IZ0, IZS, IZ1, IT, IU, IN1, IN2

      CHARACTER*2   ELEM,        SYMBL(92),  CHR
      CHARACTER*2   SQI(NION)
      CHARACTER*4   SPECU(9),    SPECT(9),    SPEC1,      SPEC2
      CHARACTER*7   SEQ(NION)
      CHARACTER*11  SPEC(NION)

C-------------------------------------------------------------------
      DATA SYMBL/'H','HE','LI','BE','B','C','N','O',
     &'F','NE','NA','MG','AL','SI','P','S','CL','AR','K',
     &'CA','SC','TI','V','CR','MN','FE','CO','NI','CU',
     &'ZN','GA','GE','AS','SE','BR','KR','RB','SR','Y',
     &'ZR','NB','MO','TE','RU','RH','PD','AG','CD','IN',
     &'SN','SB','TE','I','XE','CS','BA','LA','CE','PR',
     &'ND','PM','SM','EU','GD','TB','DY','HO','ER','TM',
     &'YB','LU','HF','TA','W','RE','OS','IR','PT','AU',
     &'HG','TL','PB','BI','PO','AT','RN','FA','RA','AC',
     &'TH','PA','U'/
      DATA SPECU/'I','II','III','IV','V','VI','VII','VIII','IX'/
      DATA SPECT/'X','XX','XXX','XL','L','LX','LXX','LXXX','XC'/
      DATA ISPEC/1,2,3,2,1,2,3,4,2/
C--------------------------------------------------------------------
C   DETERMINE THE NUCLEAR CHARGE OF THE SELECTED ELEMENT AND THE
C   CHARGES OF THE SELECTED IONS
C--------------------------------------------------------------------
      DO L=1,92
         IF(ELEM.EQ.SYMBL(L))IZ0=L
      ENDDO
      IF(NIONS.GT.92)THEN
         NIONS=IZ0
         DO L=1,NIONS
            SQI(L)=SYMBL(L)
         ENDDO
      ENDIF
      DO L=1,NIONS
C         DO LL=1,92
C            IF (SQI(L).EQ.SYMBL(LL))IZS=LL
C            IZ(L)=IZ0-IZS
C         ENDDO
	   IZS=IZ0-IZ(L)
	   SQI(L)=SYMBL(IZS)
         SEQ(L)=SQI(L)//'-LIKE'
      ENDDO

C--------------------------------------------------------------------
C   IDENTIFY THE IONS WITH THEIR ROMANS NUMERAL IN SPECTROSCOPIC
C   NOTATION.
C--------------------------------------------------------------------
      DO L=1,NIONS
         IZ1=IZ(L)+1
         WRITE(CHR,1020)IZ1
         READ(CHR(1:1),1080)IT
         READ(CHR(2:2),1080)IU
         IF(IT.EQ.0)THEN
            IN2=ISPEC(IU)
            SPEC2=SPECU(IU)
            SPEC(L)=ELEM//' '//SPEC2(1:IN2)
         ELSEIF(IU.EQ.0)THEN
            IN1=ISPEC(IT)
            SPEC1=SPECT(IT)
            SPEC(L)=ELEM//' '//SPEC1(1:IN1)
         ELSE
            SPEC1=SPECT(IT)
            SPEC2=SPECU(IU)
            IN1=ISPEC(IT)
            IN2=ISPEC(IU)
            SPEC(L)=ELEM//' '//SPEC1(1:IN1)//SPEC2(1:IN2)
         ENDIF
      ENDDO

C--------------------------------------------------------------------
C  FORMATS
C--------------------------------------------------------------------
 1015 FORMAT(1A2)
 1016 FORMAT(1A80)
 1020 FORMAT(1I2)
 1080 FORMAT(I1)
 2002 FORMAT(F7.1)
C--------------------------------------------------------------------

      RETURN
      END


