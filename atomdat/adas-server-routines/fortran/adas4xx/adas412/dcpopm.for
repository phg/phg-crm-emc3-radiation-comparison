CX IDL UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dcpopm.for,v 1.1 2004/07/06 13:29:10 whitefor Exp $ Date $Date: 2004/07/06 13:29:10 $
CX
      SUBROUTINE DCPOPM( NDTEM  , NDMET , NDLEV ,
     &                   MAXT   , NMET  ,
     &                            DENSA , IMETR ,
     &                            LRSEL , LHSEL ,
     &                            RATIA , RATHA ,
     &                   STCKM  , STVRM , STVHM ,
     &                   POPAR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCPOPM *********************
C
C  PURPOSE: TO CONSTRUCT METASTABLE LEVEL POPULATIONS.
C
C  CALLING PROGRAM:  XCOEF
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDTEM   = MAXIMUM NUMBER OF TEMP/DENS PAIRS ALLOWED
C  INPUT :  (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLE LEVELS ALLOWED
C  INPUT :  (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C
C  INPUT :  (I*4)  MAXT    = NO. INPUT TEMP/DENSITY PAIRS ( 1 ->'NDTEM')
C  INPUT :  (I*4)  NMET    = NUMBER OF METASTABLES LEVELS ( 1 ->'NDMET')
C
C  INPUT :  (R*8)  DENSA() = ELECTRON DENSITIES  (UNITS: CM-3)
C  INPUT :  (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                            (ARRAY SIZE = 'NDMET' )
C
C  INPUT :  (L*4)  LRSEL   = .TRUE.  => FREE ELECTRON RECOMBINATION
C                                       REQUESTED.
C                          = .FALSE. => FREE ELECTRON RECOMBINATION
C                                       NOT REQUESTED.
C  INPUT :  (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN REQUESTED.
C                          = .FALSE. => CHARGE TRANSFER FROM NEUTRAL
C                                       HYDROGREN NOT REQUESTED.
C
C  INPUT :  (R*8)  RATIA() = RATIO ( N(Z+1)/N(Z)  STAGE ABUNDANCIES )
C  INPUT :  (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C
C  INPUT :  (R*8)  STCKM(,) = METASTABLE POPULATIONS STACK:
C                               1st DIMENSION: METASTABLE INDEX
C                               2nd DIMENSION: TEMPERATURE INDEX
C  INPUT :  (R*8)  STVRM(,) = METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                              1st DIMENSION: METASTABLE INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C  INPUT :  (R*8)  STVHM(,) = METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                              1st DIMENSION: METASTABLE INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C
C  OUTPUT:  (R*8)  POPAR(,) = LEVEL POPULATIONS
C                              1st DIMENSION: LEVEL INDEX
C                              2nd DIMENSION: TEMPERATURE INDEX
C                              (ON OUTPUT CONTAINS POPULATIONS FOR
C                               METASTABLE LEVELS ONLY.)
C
C           (R*8) DCOEF     = DENSITY MULTIPLIED BY RELEVANT RATIOS  FOR
C                             CALCULATING RECOMBINATION CONTRIBUTIONS.
C
C           (I*4) IT        = TEMPERATURE ARRAY INDEX
C           (I*4) IM        = METASTABLE LEVEL ARRAY INDEX
C
C
C ROUTINES: NONE
C
C AUTHOR:  HP SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    27/ 6/91
C
C VERSION: 1.1	RICHARD MARTIN			DATE: 27-10-97
C			PUT UNDER SCCS CONTROL.	    
C			NAME CHANGED FROM BHPOPM TO DCPOPM
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDTEM        , NDMET   , NDLEV   ,
     &          MAXT         , NMET
      INTEGER   IT           , IM
C-----------------------------------------------------------------------
      REAL*8    DCOEF
C-----------------------------------------------------------------------
      LOGICAL   LRSEL        , LHSEL
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)
C-----------------------------------------------------------------------
      REAL*8    DENSA(NDTEM)                ,
     &          RATIA(NDTEM)                , RATHA(NDTEM)
      REAL*8    STCKM(NDMET,NDTEM)          ,
     &          STVRM(NDMET,NDTEM)          , STVHM(NDMET,NDTEM)
      REAL*8    POPAR(NDLEV,NDTEM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C METASTABLE LEVEL POPULATIONS.
C-----------------------------------------------------------------------
C
         DO 1 IT=1,MAXT
            DO 3 IM=1,NMET
               POPAR(IMETR(IM),IT)=STCKM(IM,IT)
    3       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ADD FREE ELECTRON RECOMB. CONTRIBUTION TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            DO 4 IT=1,MAXT
               DCOEF = RATIA(IT)*DENSA(IT)
                  DO 6 IM=1,NMET
                     POPAR(IMETR(IM),IT)=POPAR(IMETR(IM),IT) +
     &                                      ( DCOEF*STVRM(IM,IT) )
    6             CONTINUE
    4       CONTINUE
         ENDIF
C
C-----------------------------------------------------------------------
C ADD CHARGE EXCHANGE RECOMB. CONTRIBUTION TO METASTABLE POPULATIONS.
C-----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            DO 7 IT=1,MAXT
               DCOEF = RATHA(IT)*RATIA(IT)*DENSA(IT)
                  DO 9 IM=1,NMET
                     POPAR(IMETR(IM),IT)=POPAR(IMETR(IM),IT) +
     &                                      ( DCOEF*STVHM(IM,IT) )
    9             CONTINUE
    7       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
