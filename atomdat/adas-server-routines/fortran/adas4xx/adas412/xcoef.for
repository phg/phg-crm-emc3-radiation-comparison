CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/xcoef.for,v 1.6 2009/11/01 19:16:41 mog Exp $ Date $Date: 2009/11/01 19:16:41 $
CX
       SUBROUTINE XCOEF ( FILELS , IFAIL  , LFXIST ,
     &                    NION   , MAXT   ,
     &                    INDA   , NIND   , NSPEC  ,
     &                    LPSEL  , LZSEL  , LISEL  , LHSEL  , LRSEL  ,
     &                    TEVA   , TPVA   , THVA   ,
     &                    DENSA  , DENSPA , RATHA  , RATIA  ,
     &                    ZEFF   ,
     &                    COEF   , SPEC
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: IONBAL *********************
C
C  VERSION:  1.1
C
C  CALLING PROGRAM: ADAS412
C
C  PURPOSE:  TO  CALCULATE  COMPLETE SETS OF SPECTRUM LINE EMISSIVITIES
C            FOR THE IONS OF AN ELEMENT
C
C            PROCESSES  CAN  INCLUDE  ELECTRON AND PROTON IMPACT,  SPON-
C            TANEOUS EMISSION,   FREE ELECTRON RECOMBINATION  AND CHARGE
C            EXCHANGE RECOMBINATION DEPENDING ON THE INPUT DATA SET.
C
C            ACCEPTS MULTIPLE INPUT FILES.  DESIGNED FOR USE IN G(T)
C            CALCULATIONS ETC.
C
C
C  DATA:     THE SOURCE DATA ARE SPECIFIC ION EXCITATION FILES STORED AS
C            PARTITIONED DATA SET MEMBERS AS FOLLOWS:-
C
C                    'JETSHP.<SE>LIKE.DATA(<MEMBER>)'
C
C            ACCORDING TO ADAS DATA FORMAT ADF04.
C
C
C  INPUT  :(C*60) FILELS()= INPUT COPASE FILE NAMES
C          (L*4)  LFXIST()= .TRUE.  => COPASE FILE FOR THIS ION
C                           .FALSE. => NO COPASE FILE FOR THIS ION
C          (I*4)  NION    = NUMBER OF IONS TO BE COMPUTED
C          (I*4)  MAXT    = NUMBER OF TEMPERATURE/DENSITY PAIRS
C          (L*4)  LPSEL   = .TRUE.  => PROTON DATA TO BE INCLUDED
C                         = .FALSE. => PROTON DATA TO BE EXCLUDED
C          (L*4)  LZSEL   = .TRUE.  => SCALE PROTON DATA WITH ZEFF
C                         = .FALSE. => DO NOT SCALE PROTON DATA
C          (L*4)  LISEL   = .TRUE.  => IONISATION TO BE INCLUDED
C                         = .FALSE. => IONISATION TO BE EXCLUDED
C          (L*4)  LHSEL   = .TRUE.  => CHARGE TRANSFER TO BE INCLUDED
C                         = .FALSE. => CHARGE TRANSFER TO BE EXCLUDED
C          (L*4)  LRSEL   = .TRUE.  => RECOMBINATION TO BE INCLUDED
C                         = .FALSE. => RECOMBINATION TO BE EXCLUDED
C          (R*8)  TEVA()  = ELECTRON TEMPERATURES (EV)
C          (R*8)  TPVA()  = PROTON TEMPERATURES (EV)
C          (R*8)  THVA()  = NEUTRAL HYDROGEN TEMPERATURES (EV)
C          (R*8)  DENSA() = ELECTRON DENSITIES  (CM-3)
C          (R*8)  DENSPA()= PROTON DENSITIES  (CM-3)
C          (R*8)  RATHA() = RATIO (NEUTRAL H DENSITY/ELECTRON DENSITY)
C          (R*8)  RATIA() = RATIO (N(Z+1)/N(Z) STAGE ABUNDANCES)
C
C  OUTPUT :(I*4)  IFAIL   = 0    SUBROUTINE SUCCESSFUL
C                           1    SUBROUTINE FAILURE OR WARNING
C          (I*4)  INDA(,) = IDENTIFIER FOR SPECTRUM LINE (10000*IL+IU)
C                           1ST DIMENSION - INDEX OF LINES FOR AN ION
C                           2ND DIMENSION - ION COUNT INDEX
C          (I*4)  NIND()  = NUMBER OF LINES FOR AN ION
C                           1ST DIMENSION - ION COUNT INDEX
C          (I*4)  NSPEC() = NUMBER OF LEVELS FOR AN ION
C                           1ST DIMENSION - ION COUNT INDEX
C          (R*8)  ZEFF    = PLASMA Z EFFECTIVE ( IF 'LZSEL' = .TRUE.)
C                             (IF 'LZSEL' = .FALSE. => 'ZEFF=1.0')
C          (R*8)  COEF(,,)= EMISSIVITY FOR SPECTRUM LINE (10000*J+I)
C                           1ST DIMENSION - INDEX OF LINES FOR AN ION
C                           2ND DIMENSION - TEMPERATURE INDEX
C                           3RD DIMENSION - ION COUNT INDEX
C          (C*51) SPEC(,) = INFORMATION STRING FOR LEVEL
C                           1ST DIMENSION - INDEX OF LEVELS FOR AN ION
C                           2ND DIMENSION - ION COUNT INDEX
C
C
C  PROGRAM:
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C          (I*4)  NZDIM   = PARAMETER = MAX. NO. OF IONS ALLOWED
C          (I*4)  NDMET   = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR COPASE DATA SET
C                                       PASSING FILE.
C          (I*4)  L1      = PARAMETER = 1
C
C          (R*8)  D1      = PARAMETER = 1.0D0
C
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C          (I*4)  IZ0     = NUCLEAR CHARGE
C          (I*4)  IZ      =  RECOMBINED ION CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  MAXT    = NO. OF INPUT TEMP/DENS PAIRS ( 1 -> 'NDTEM')
C          (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C          (I*4)  NORD    = NUMBER OF ORDINARY LEVELS ('IL' - 'NMET')
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C          (I*4)  I       = GENERAL INDEX
C          (I*4)  IT      = TEMPERATURE ARRAY INDEX
C          (I*4)  IS      = ENERGY LEVEL ARRAY INDEX
C
C          (R*8)  TEA()   = INPUT ELECTRON TEMPERATURES (K)
C          (R*8)  TPA()   = INPUT PROTON TEMPERATURES (K)
C          (R*8)  THA()   = INPUT NEUTRAL HYDROGEN TEMPERATURES (K)
C          (R*8)  R8FBCH  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C          (R*8)  ZEFFSQ  = 'ZEFF' * 'ZEFF'
C          (R*8)  DMINT   = +1 or -1 DEPENDING ON WHETHER THE  NUMBER OF
C                           ROW   INTERCHANGES   WAS   EVEN   OR    ODD,
C                           RESPECTIVELY, WHEN INVERTING A MATRIX  USING
C                           'XXMINV'.
C
C          (L*4)  LSOLVE  = .TRUE.  => SOLVE LINEAR EQUATION USING
C                                      'XXMINV'.
C                           .FALSE. =>DO NOT SOLVE LINEAR EQUATION USING
C                                     'XXMINV' - INVERT MATRIX ONLY.
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (C*3)  TITLED  = ELEMENT SYMBOL.
C          (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C          (C*60) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C          (C*51) CLINE   = LEVEL SPECIFICATION LINE
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  IORDR() = INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE
C                           LEVEL LIST.
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  PAR(,)  =
C          (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  AA()    = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  RHS()   = USED ONLY IF 'LSOLVE=.TRUE.' WHEN CALLING
C                           THE SUBROUTINE 'XXMINV'. CONTAINS THE SET
C                           OF 'N' LINEAR EQUATIONS TO BE SOLVED.
C                           INPUT  TO   'XXMINV': RIGHT HAND SIDE VECTOR
C                           OUTPUT FROM 'XXMINV': SOLUTION VECTOR
C                           (ACTS ONLY AS A DUMMY IN THIS PROGRAM)
C          (R*8)  CIE()   = IONISATION RATE COEFFICIENT VECTOR FOR
C                           FIXED TEMPERATURE.
C                            DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  VHRED() = CHARGE EXCHANGE RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C          (R*8)  VRRED() = FREE ELECTRON RECOMBINATION:
C                           VECTOR  OF RECOMBINATION RATE CONTRIBUTIONS
C                           FOR EACH METASTABLE LEVEL.
C                           (UNITS: SEC-1)
C                           VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            DIMENSION: METASTABLE LEVEL INDEX
C
C          (R*8)  EXCRE(,) = ELECTRON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRE(,)= ELECTRON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  EXCRP(,) = PROTON IMPACT TRANSITION:
C                            EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  DEXCRP(,)= PROTON IMPACT TRANSITION:
C                            DE-EXCITATION RATE COEFFS (cm**3/s)
C                            PRE  'BXRATE': UNIT GAMMA VALUES
C                            POST 'BXRATE': TRUE VALUES
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: TRANSITION INDEX
C          (R*8)  VECH(,)  = CHARGE-EXCHANGE RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C          (R*8)  VECR(,)  = FREE ELECTRON RECOMBINATION:
C                            SPLINED RECOMBINATION RATE COEFFT. VALUES.
C                            1st DIMENSION: TEMPERATURE INDEX ('TOUT')
C                            2nd DIMENSION: CAPTURING LEVEL INDEX.
C          (R*8)  CRA(,)   = A-VALUE  (sec-1)  MATRIX  COVERING  ALL
C                            TRANSITIONS.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCE(,)  = ELECTRON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CRCP(,)  = PROTON IMPACT TRANSITION:
C                            EXCIT'N/DE-EXCIT'N RATE COEFFT MATRIX
C                            COVERING ALL TRANSITIONS (cm**3/s).
C                            VALUES FOR GIVEN TEMPERATURE & TRANSITION
C                            TYPE.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C                           (NOTE: DIAGONAL   ELEMENTS  REPRESENT   THE
C                                  NEGATIVE  SUM  OF  THEIR  RESPECTIVE
C                                  COLUMNS.)
C          (R*8)  CC(,)    = RATE MATRIX COVERING ALL TRANSITIONS
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: ENERGY LEVEL INDEX
C                            2nd DIMENSION: ENERGY LEVEL INDEX
C          (R*8)  CMAT(,)  = (INVERTED)   RATE  MATRIX  COVERING   ALL
C                            NON-METASTABLE/ORDINARY EXCITED LEVELS.
C                            (UNITS: SEC)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            PRE  'XXMINV' : NOT-INVERTED
C                            POST 'XXMINV' : INVERTED
C                            1st DIMENSION: ORDINARY EXCITED LEVEL INDEX
C                            2nd DIMENSION: ORDINARY EXCITED LEVEL INDEX
C          (R*8)  CRED(,)  = MATRIX  OF  TRANSITION   RATES   BETWEEN
C                            METASTABLE LEVELS.
C                            (UNITS: SEC-1)
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            1st DIMENSION: METASTABLE LEVEL INDEX
C                            2nd DIMENSION: METASTABLE LEVEL INDEX
C          (R*8)  CRMAT(,) = INVERTED  METASTABLE  LEVEL   RATE   MATRIX
C                            COVERING ALL TRANSITIONS BETWEEN METASTABLE
C                            LEVELS EXCEPT THOSE INVOLVING LEVEL 1.
C                            VALUES FOR GIVEN TEMPERATURE AND DENSITY.
C                            BEFORE INPUT  TO   XXMINV: NOT INVERTED
C                            AFTER  OUTPUT FROM XXMINV: AS-ABOVE
C                            1st DIMENSION: METASTABLE LEVEL INDEX - 1
C                            2nd DIMENSION: METASTABLE LEVEL INDEX - 1
C
C
C          (R*8) POPAR(,)  = LEVEL POPULATIONS
C                             1st DIMENSION: LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C          (R*8) STVR(,)   = ORDINARY EXCITED LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C          (R*8) STVH(,)   = ORDINARY EXCITED LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C          (R*8) STACK(,,) = ORDINARY EXCITED LEVEL POPULAT'N DEPENDENCE
C                            ON METASTABLE LEVEL.
C                             1st DIMENSION: ORDINARY LEVEL INDEX
C                             2nd DIMENSION: METASTABLE INDEX
C                             3rd DIMENSION: TEMPERATURE INDEX
C          (R*8) STVRM(,)  = METASTABLE LEVEL:
C                             FREE-ELECTRON RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C          (R*8) STVHM(,)  = METASTABLE LEVEL:
C                             CHARGE-EXCHANGE RECOMBINATION COEFFICIENTS
C                             (UNITS* CM**3/SEC-1)
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C          (R*8) STCKM(,)  = METASTABLE POPULATIONS STACK
C                             1st DIMENSION: METASTABLE INDEX
C                             2nd DIMENSION: TEMPERATURE INDEX
C
C          (C*1)  TCODE() = TRANSITION: DATA TYPE POINTER:
C                           ' ' => Electron Impact   Transition
C                           'P' => Proton   Impact   Transition
C                           'H' => Charge   Exchange Recombination
C                           'R' => Free     Electron Recombination
C          (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C          (C*22) STRGA() = LEVEL DESIGNATIONS
C
C          (L*4)  LTRNG(,)= .TRUE.  => TEMPERATURE VALUE WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                         = .FALSE. =>TEMPERATURE VALUE NOT WITHIN RANGE
C                                      READ FROM INPUT COPASE DATA SET.
C                           1st DIMENSION: TEMPERATURE INDEX.
C                           2nd DIMENSION: TEMPERATURE TYPE -
C                                          1) => ELECTRON
C                                          2) => PROTON
C                                          3) => NEUTRAL HYDROGEN
C
C NOTE:
C
C          INPUT/OUTPUT STREAM ALLOCATIONS:
C          --------------------------------
C
C          STREAM 10: INPUT - SPECIFIC ION RATE DATA INPUT FILE FROM
C          ('IUNT10')         DATABASE (SEE DATA SECTION ABOVE).
C
C
C AUTHOR:  HP SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    27/06/91
C
C UPDATE:  12/04/94 - H. P. SUMMERS - RATIONALISING OF DIMENSIONS WITH
C                                     LATEST ADAS9120 ROUTINES. NOTED
C                                     STACK IS REAL*4
C
C#
C DATE:    mar20-95 - A. C. Lanzafame - conversion to Unix
C          mar21-95 - A. C. Lanzafame - call to XXDATE avoided: redundant
C          mar24-95 -                 - FILELS from C*44 to C*60
C                                     - DSNINC from C*44 to C*60
C          apr27-95 - A. C. Lanzafame - STACK changed to R*8
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	     DCSTKC	     ADAS      STACK UP TRANSITION RATE BETWEEN METS.
C	     DCSTKA	     ADAS      STACK UP ORDINARY POP. DEPENDENCE ON MET
C	     DCPOPM	     ADAS      CALCULATE BASIC MET. LEVEL POPULATIONS.
C	     DCPOPO	     ADAS      CALCULATE ORDINARY LEVEL POPULATIONS.
C	     DCLNORM     ADAS      NORMALISES LINE EMISSIVITY.
C	     BXDATA	     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C	     BXTTYP	     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C	     BXIORD	     ADAS      SETS  UP ORDINARY LEVEL INDEX.
C	     BXRATE	     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C	     BXRCOM	     ADAS      ESTABLISHES RECOMBINATION RATE COEFFTS.
C	     BXMCRA	     ADAS      CONSTRUCTS A-VALUE MATRIX.
C	     BXMCRC	     ADAS      CONSTRUCTS EXC./DE-EXC. RATE COEF MATRIX
C	     BXMCCA	     ADAS      CONSTRUCTS WHOLE RATE MATRIX.
C	     BXMCMA	     ADAS      CONSTRUCTS ORDINARY LEVEL RATE MATRIX.
C	     BXSTKB	     ADAS      STACK UP RECOMB. CONTRIBUTION FOR ORD.
C	     BXSTKD	     ADAS      STACK UP RECOMB RATE FOR EACH MET. LEVEL
C	     BXMPOP	     ADAS      CALCULATE METASTABLE LEVEL POPULATIONS.
C	     BXSTVM	     ADAS      CALCULATE MET. LEVEL RECOMB. COEFFTS.
C	     XXERYD	     ADAS      CONVERTS ENERGIES FROM W.NO. TO RYDBERGS
C	     XXRATE	     ADAS      CALCULATES EXC. & DE-EXC. RATE COEFFTS.
C	     XXMINV	     ADAS      INVERTS MATRIX AND SOLVES EQUATIONS.
C                                FOR UNIT GAMMA VALUE
C	     R8FBCH	     ADAS      REAL*8 FUNCTION:EVALUATES SHELL CONTRIB.
C                                TO IONISATION RATE  COEFFICIENT  IN  THE
C                                BURGESS-CHIDICHIMO APPROX.
C
C VERSION 1.1							DATE: 28-10-97
C		RICHARD MARTIN
C		PUT UNDER SCCS CONTROL (ADAS412).
C
C VERSION: 1.2                               DATE: 02-05-2003
C MODIFIED: Martin O'Mullane
C		- Use xxdata_04 to read adf04 file. This requires
C                 new arrays some of which are not used in the 
C                 population calculation.
C               - bxttyp parameter list extended.
C
C VERSION : 1.3                               
C DATE    : 04-08-2008
C MODIFIED: Martin O'Mullane
C		- Increase number of transitions to 3000.
C
C VERSION : 1.4                               
C DATE    : 29-08-2008
C MODIFIED: Martin O'Mullane
C		- Reduce nzdim from 20 to 1.
C
C VERSION : 1.5                               
C DATE    : 04-02-2009
C MODIFIED: Allan Whiteford
C		- Increased NDMET from 1 to 5.
C
C VERSION : 1.6                               
C DATE    : 31-10-2009
C MODIFIED: Martin O'Mullane
C               - Increase number of levels to 1500 and transitions
C                 to 10000.
C		- Remove forgotten debug write-to-screen message.
C
C-----------------------------------------------------------------------
      INTEGER   NDLEV  , NDTRN  , NDTEM  , NZDIM  , NDMET
      INTEGER   IUNT10 , NVMAX
      INTEGER   NDQDN
      INTEGER   L1
C-----------------------------------------------------------------------
      REAL*8    D1
C-----------------------------------------------------------------------
      PARAMETER(NDLEV =1500, NDTRN =10000, NDTEM=101, 
     &          NZDIM =1 , NDMET=5)
      PARAMETER(IUNT10=10 , NVMAX = 14)
      PARAMETER(NDQDN=6   )
      PARAMETER(L1=1)
C-----------------------------------------------------------------------
      PARAMETER( D1 = 1.0D0 )
C-----------------------------------------------------------------------
      INTEGER   IZ        , IZ0       , IZ1     , NION    , IFAIL  ,
     &          IL        , NV        , ITRAN   ,
     &          MAXLEV    , NMET      , NORD    ,
     &          MAXT      ,
     &          ICNTE     , ICNTP     , ICNTR   , ICNTH   ,
     &          I         , IS        , IT      , IF
C-----------------------------------------------------------------------
      REAL*8    R8FBCH    , EV
      REAL*8    BWNO      , ZEFF      , ZEFFSQ  , DMINT
C-----------------------------------------------------------------------
      CHARACTER TITLED*3    , CLINE*51,
     &          DSNINC*60
C-----------------------------------------------------------------------
      LOGICAL   OPEN10    , LEXIST    ,
     &          LPSEL     , LZSEL     , LISEL   ,
     &          LHSEL     , LRSEL     ,
     &          LSOLVE
C-----------------------------------------------------------------------
      INTEGER   INDA(NDTRN,NZDIM), NIND(NZDIM)      ,
     &          NSPEC(NZDIM)
      INTEGER   IMETR(NDMET)     , IORDR(NDLEV)     ,
     &          IA(NDLEV)        ,
     &          ISA(NDLEV)       , ILA(NDLEV)       ,
     &          I1A(NDTRN)       , I2A(NDTRN)
      INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &          IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &          IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &          IP1A(NDTRN)      , IP2A(NDTRN)
C-----------------------------------------------------------------------
      REAL*8    COEF(NDTRN,NDTEM,NZDIM)
      REAL*8    SCEF(NVMAX)     ,
     &          TEVA(NDTEM)     , TPVA(NDTEM)      , THVA(NDTEM) ,
     &          TEA(NDTEM)      , TPA(NDTEM)       , THA(NDTEM)  ,
     &          DENSA(NDTEM)    , DENSPA(NDTEM)    ,
     &          RATHA(NDTEM)    , RATIA(NDTEM)     ,
     &          AA(NDTRN)       , AVAL(NDTRN)      ,
     &          XJA(NDLEV)      , WA(NDLEV)        ,
     &          XIA(NDLEV)      , ER(NDLEV)        ,
     &          CIE(NDLEV)      , RHS(NDLEV)       ,
     &          VRRED(NDMET)    , VHRED(NDMET)
      REAL*8    SCOM(NVMAX,NDTRN)             ,
     &          EXCRE(NDTEM,NDTRN)            , EXCRP(NDTEM,NDTRN)    ,
     &          DEXCRE(NDTEM,NDTRN)           , DEXCRP(NDTEM,NDTRN)   ,
     &          VECH(NDTEM,NDLEV)             , VECR(NDTEM,NDLEV)     ,
     &          CRA(NDLEV,NDLEV)              ,
     &          CRCE(NDLEV,NDLEV)             , CRCP(NDLEV,NDLEV)     ,
     &          CC(NDLEV,NDLEV)               , CMAT(NDLEV,NDLEV)     ,
     &          CRED(NDMET,NDMET)             , CRMAT(NDMET,NDMET)
      REAL*8    POPAR(NDLEV,NDTEM)
      REAL*8    STVR(NDLEV,NDTEM)             , STVH(NDLEV,NDTEM)
      REAL*8    STCKM(NDMET,NDTEM)            ,
     &          STVRM(NDMET,NDTEM)            , STVHM(NDMET,NDTEM)
      REAL*8    STACK(NDLEV,NDMET,NDTEM)
C-----------------------------------------------------------------------
      CHARACTER TCODE(NDTRN)*1   , CSTRGA(NDLEV)*18,
     &          SPEC(NDLEV,NZDIM)*51 , FILELS(NZDIM)*60
C-----------------------------------------------------------------------
      LOGICAL   LTRNG(NDTEM,3)   ,
     &          LFXIST(NZDIM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      DATA OPEN10 /.FALSE./
C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   npl           , iadftyp       , itieactn , iorb    
      integer   nplr          , npli          , icnti    , icntl   , 
     &          icnts
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      integer   ipla(ndmet,ndlev)             , npla(ndlev)
      integer   iitrn(ndtrn)                  , iltrn(ndtrn)       , 
     &          istrn(ndtrn)                  ,                    
     &          ia1a(ndtrn)                   , ia2a(ndtrn)        , 
     &          il1a(ndlev)                   , il2a(ndlev)        , 
     &          is1a(ndlev)                   , is2a(ndlev)        
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet) , 
     &          zpla(ndmet,ndlev)             , beth(ndtrn)        ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
      real*8    auga(ndtrn)                   , wvla(ndlev)
C----------------------------------------------------------------------
      character cpla(ndlev)*1                 , cprta(ndmet)*9      
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  , lbseta(ndmet)      ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lss04a(ndlev,ndmet)
C----------------------------------------------------------------------

C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES TO (K)
C-----------------------------------------------------------------------
C
      EV=11605.4
C
      DO 12 IT=1,MAXT
       TEA(IT)=EV*TEVA(IT)
       TPA(IT)=EV*TPVA(IT)
       THA(IT)=EV*THVA(IT)
   12 CONTINUE
C
C-----------------------------------------------------------------------
C RESTRICT METASTABLE SET TO GROUND ONLY
C-----------------------------------------------------------------------
C
      NMET=1
      IMETR(1)=1
C
      DO 100 IF = 1 , NION
         IF (FILELS(IF).EQ.' ') THEN
            LFXIST(IF) = .FALSE.
            GO TO 100
         ELSE
C# mar21-95 Alessandro Lanzafame
C#          DSNINC = '/'//FILELS(IF)
            DSNINC = FILELS(IF)
            INQUIRE(FILE=DSNINC,EXIST=LEXIST)
            IF(.NOT.LEXIST)THEN
               LFXIST(IF)=.FALSE.
               GO TO 100
            ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT, THEN OPEN COPASE FILE
C-----------------------------------------------------------------------
C
         IF (OPEN10) CLOSE(10)
         OPEN10=.FALSE.
C# 
C# mar20-95 Alessandro Lanzafame - conversion to Unix
C#       OPEN( UNIT=IUNT10 , FILE=DSNINC , ACTION='READ' )
         OPEN( UNIT=IUNT10 , FILE=DSNINC , STATUS='OLD' )
C#
         OPEN10=.TRUE.

C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED COPASE DATASET AND PREPARE SPEC VECTOR
C-----------------------------------------------------------------------

       itieactn = 0
       
       call xxdata_04( iunt10 , 
     &                 ndlev  , ndtrn , ndmet   , ndqdn , nvmax ,
     &                 titled , iz    , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,     
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )





         NSPEC(IF)=IL

         DO 8 I=1,IL
            WRITE(CLINE,1000) IA(I)  , CSTRGA(I) ,  ISA(I)  , ILA(I)  ,
     &                        XJA(I) , WA(I)
            SPEC(I,IF)=CLINE
 8       CONTINUE

C-----------------------------------------------------------------------
C SORT TRANSITIONS INTO TRANSITION/RECOMBINATION TYPE, STACK INDA & COEF
C-----------------------------------------------------------------------

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )


         NIND(IF)=0
         DO 11 I=1,ICNTE
            IF(AA(I).GT.1.0D-29) THEN
               NIND(IF)=NIND(IF)+1
               INDA(NIND(IF),IF)=10000*IE1A(I) + IE2A(I)
               DO 7 IT=1,MAXT
                  COEF(NIND(IF),IT,IF)=AA(I)
 7             CONTINUE
            ENDIF
 11      CONTINUE 

C-----------------------------------------------------------------------
C CALCULATE LEVEL ENERGIES RELATIVE TO LEVEL 1 & IONISATION POT. IN RYDS
C-----------------------------------------------------------------------
C
         CALL XXERYD( BWNO , IL , WA  ,
     &                ER   , XIA
     &           )
C
C***********************************************************************
C ANALYSE ENTERED DATA - IF NOT END OF ANALYSIS
C***********************************************************************
C
C-----------------------------------------------------------------------
C  INITIALISE VECR, VECH AND CIE TO ZERO
C-----------------------------------------------------------------------
C
         DO 2 IS=1,NDLEV
            CIE(IS)  = 0.0D0
            DO 3 IT=1,NDTEM
               VECR(IT,IS) = 0.0D0
               VECH(IT,IS) = 0.0D0
 3          CONTINUE
 2       CONTINUE
C
C-----------------------------------------------------------------------
C CALCULATE ELECTRON IMPACT TRANSITN EXCITATION & DE-EXCIT'N RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.
C-----------------------------------------------------------------------
C
         CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                ICNTE , MAXT   ,
     &                XJA   , ER     , TEA   ,
     &                IE1A  , IE2A   ,
     &                EXCRE , DEXCRE
     &                )
C
C-----------------------------------------------------------------------
C CALCULATE PROTON IMPACT TRANSITION EXCITATION & DE-EXCIT'N RATE COEFS.
C NOTE: AT THIS STAGE ASSUME GAMMA = 1.             (ONLY IF REQUIRED).
C-----------------------------------------------------------------------
C
         IF (LPSEL) THEN
            CALL XXRATE( NDTRN , NDTEM  , NDLEV ,
     &                   ICNTP , MAXT   ,
     &                   XJA   , ER     , TPA   ,
     &                   IP1A  , IP2A   ,
     &                   EXCRP , DEXCRP
     &                       )
         ENDIF
C
C-----------------------------------------------------------------------
C SETUP INDEX OF ORDINARY EXCITED LEVELS IN COMPLETE LEVEL LIST 'IORDR'
C-----------------------------------------------------------------------
C
         CALL BXIORD( IL   ,
     &                NMET , IMETR ,
     &                NORD , IORDR
     &                 )
C
C-----------------------------------------------------------------------
C ELECTRON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------
C
         CALL BXRATE( NDTEM      , NDTRN  , D1     ,
     &                NV         , SCEF   , SCOM   ,
     &                MAXT       , TEA    ,
     &                ICNTE      , IETRN  ,
     &                EXCRE      , DEXCRE ,
     &                LTRNG(1,1)
     &               )
C
C-----------------------------------------------------------------------
C PROTON EXCITATION DATA OPTION - SPLINE GAMMA'S TO GIVE RATE COEFFTS.
C-----------------------------------------------------------------------
C
         IF (LPSEL) THEN
            IF(LZSEL) THEN
               ZEFFSQ = ZEFF*ZEFF
            ELSE
               ZEFFSQ = 1.0
            ENDIF
            CALL BXRATE( NDTEM      , NDTRN  , ZEFFSQ ,
     &                   NV         , SCEF   , SCOM   ,
     &                   MAXT       , TPA    ,
     &                   ICNTP      , IPTRN  ,
     &                   EXCRP      , DEXCRP ,
     &                   LTRNG(1,2)
     &                 )
         ENDIF
C
C----------------------------------------------------------------------
C FREE ELECTRON RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------
C
         IF (LRSEL) THEN
            CALL BXRCOM( NDTEM , NDTRN      , NDLEV  ,
     &                   NV    , SCEF       , SCOM   ,
     &                   MAXT  , TEA        ,
     &                   ICNTR , IRTRN      , I2A    ,
     &                   VECR  , LTRNG(1,1)
     &                  )
         ENDIF
C
C----------------------------------------------------------------------
C CHARGE EXCHANGE RECOMBINATION DATA OPTION - SPLINE COEFFICIENTS
C----------------------------------------------------------------------
C
         IF (LHSEL) THEN
            CALL BXRCOM( NDTEM , NDTRN      , NDLEV  ,
     &                   NV    , SCEF       , SCOM   ,
     &                   MAXT  , THA        ,
     &                   ICNTH , IHTRN      , I2A    ,
     &                   VECH  , LTRNG(1,3)
     &                  )
         ENDIF
C
C----------------------------------------------------------------------
C SET UP A-VALUE TRANSITION MATRIX 'CRA' (UNITS: SEC-1)
C----------------------------------------------------------------------
C
         CALL BXMCRA( NDTRN , NDLEV  ,
     &                ICNTE , IL     ,
     &                IE1A  , IE2A   ,
     &                AA    ,
     &                CRA
     &               )
C
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C INCREMENT OVER OVER ALL INPUT TEMPERATURE VALUES
C* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
C
         DO 4 IT=1,MAXT
C
C-----------------------------------------------------------------------
C SET UP EXCITATION/DE-EXCITATION RATE COEFFT. MATRICES FOR GIVEN TEMP.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ELECTRON IMPACT TRANSITIONS 'CRCE' - (UNITS: CM**3/SEC-1)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
            CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                   IT    , ICNTE  , IL     ,
     &                   IE1A  , IE2A   ,
     &                   EXCRE , DEXCRE ,
     &                   CRCE
     &                 )
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C PROTON IMPACT TRANSITIONS 'CRCP' - (UNITS: CM**3/SEC-1) - IF SELECTED
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
            IF (LPSEL) THEN
               CALL BXMCRC( NDTEM , NDTRN  , NDLEV  ,
     &                      IT    , ICNTP  , IL     ,
     &                      IP1A  , IP2A   ,
     &                      EXCRP , DEXCRP ,
     &                      CRCP
     &                     )
            ENDIF
C
C-----------------------------------------------------------------------
C  SET UP VECTOR OF IONISATION RATE COEFFICIENTS - IF REQUIRED
C-----------------------------------------------------------------------
C
            IF (LISEL) THEN
               DO 5 IS=1,MAXLEV
                  CIE(IS) = R8FBCH( IZ, XIA(IS), D1, TEA(IT) )
 5             CONTINUE
            ENDIF
C
C-----------------------------------------------------------------------
C SET UP WHOLE RATE MATRIX  - (UNITS: SEC-1)
C-----------------------------------------------------------------------
C
            CALL BXMCCA( NDLEV     , IL         ,
     &                   LPSEL      , LISEL  ,
     &                   DENSA(IT) , DENSPA(IT) ,
     &                   CRA       ,
     &                   CRCE      , CRCP       , CIE    ,
     &                   CC
     &                  )
C
C-----------------------------------------------------------------------
C SET UP NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX - (UNITS: SEC-1)
C-----------------------------------------------------------------------
C
            CALL BXMCMA( NDLEV ,
     &                   NORD  , IORDR ,
     &                   CC    ,
     &                   CMAT
     &                  )
C
C-----------------------------------------------------------------------
C INVERT NON-METASTABLE (ORDINARY EXCITED LEVEL) MATRIX
C-----------------------------------------------------------------------
C
            LSOLVE=.FALSE.
            CALL XXMINV( LSOLVE , NDLEV , NORD   ,
     &                   CMAT   , RHS   , DMINT
     &                 )
C
C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE POPULATION COMPONENTS ASSOCIATED WITH EACH
C METASTABLE LEVEL.
C-----------------------------------------------------------------------
C
            CALL DCSTKA( NDLEV            , NDMET  ,
     &                   NORD             , NMET   ,
     &                   IORDR            , IMETR  ,
     &                   CMAT             , CC     ,
     &                   STACK(1,1,IT)
     &                             )
C
C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE FREE-ELECT. RECOMB. CONTRIBUTIONS -IF SELECTED
C-----------------------------------------------------------------------
C
            IF (LRSEL) THEN
               CALL BXSTKB( NDTEM            , NDLEV  ,
     &                      IT               , NORD   ,
     &                      IORDR  ,
     &                      CMAT             , VECR   ,
     &                      STVR(1,IT)
     &                    )
            ENDIF
C
C-----------------------------------------------------------------------
C STACK UP NON-METASTABLE CHARGE EXCHANGE CONTRIBUTIONS -IF SELECTED
C-----------------------------------------------------------------------
C
            IF (LHSEL) THEN
               CALL BXSTKB( NDTEM            , NDLEV  ,
     &                      IT               , NORD   ,
     &                      IORDR            ,
     &                      CMAT             , VECH   ,
     &                      STVH(1,IT)
     &                                )
            ENDIF
C
C-----------------------------------------------------------------------
C STACK UP METASTABLE LEVEL TRANSITION RATE CONTRIBUTIONS
C-----------------------------------------------------------------------
C
            CALL DCSTKC( NDLEV            , NDMET            ,
     &                   NORD             , NMET             ,
     &                   IORDR            , IMETR            ,
     &                   CC               , STACK(1,1,IT)    ,
     &                   CRED
     &                  )
C
C-----------------------------------------------------------------------
C STACK UP METASTABLE FREE-ELECT. RECOMB. RATE CONTRIBUTIONS-IF SELECTED
C-----------------------------------------------------------------------
C
            IF (LRSEL) THEN
               CALL BXSTKD( NDTEM , NDLEV         , NDMET  ,
     &                      IT    , NORD          , NMET   ,
     &                              IORDR         , IMETR  ,
     &                      CC    , STVR(1,IT)    , VECR   ,
     &                      VRRED
     &                    )
            ENDIF
C
C-----------------------------------------------------------------------
C STACK UP METASTABLE CHARGE EXCHANGE RATE CONTRIBUTIONS - IF SELECTED
C-----------------------------------------------------------------------
C
            IF (LHSEL) THEN
               CALL BXSTKD( NDTEM , NDLEV         , NDMET  ,
     &                      IT    , NORD          , NMET   ,
     &                              IORDR         , IMETR  ,
     &                      CC    , STVH(1,IT)    , VECH   ,
     &                      VHRED
     &                    )
            ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE METASTABLE POPULATIONS
C-----------------------------------------------------------------------
C
            CALL BXMPOP( NDMET             ,
     &                   NMET              ,
     &                   CRED              ,
     &                   RHS               , CRMAT        ,
     &                   STCKM(1,IT)
     &                 )
C
C-----------------------------------------------------------------------
C CALCULATE RECOMBINATIONS AND CHARGE EXCHANGE CONTRIBUTIONS.
C-----------------------------------------------------------------------
C
 
            IF (LRSEL) THEN
               CALL BXSTVM( NDMET            ,
     &                      NMET             ,
     &                      CRMAT            ,
     &                      VRRED            ,
     &                      STVRM(1,IT)
     &                    )
            ENDIF
C-----------------------------------------------------------------------
            IF (LHSEL) THEN
               CALL BXSTVM( NDMET            ,
     &                      NMET             ,
     &                      CRMAT            ,
     &                      VHRED            ,
     &                      STVHM(1,IT)
     &                    )
            ENDIF
 4       CONTINUE
C
C-----------------------------------------------------------------------
C CALCULATE THE POPULATIONS OF EACH LEVEL.
C-----------------------------------------------------------------------
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE METASTABLE LEVELS FIRST.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
         CALL DCPOPM( NDTEM  , NDMET , NDLEV ,
     &                MAXT   , NMET  ,
     &                         DENSA , IMETR ,
     &                         LRSEL , LHSEL ,
     &                         RATIA , RATHA ,
     &                STCKM  , STVRM , STVHM ,
     &                POPAR
     &               )
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WORK OUT THE POPULATIONS OF EACH ORDINARY LEVEL.
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
         CALL DCPOPO( NDTEM  , NDMET , NDLEV ,
     &                MAXT   , NMET  , NORD  ,
     &                         DENSA , IMETR , IORDR ,
     &                         LRSEL , LHSEL ,
     &                         RATIA , RATHA ,
     &                STACK  , STVR  , STVH  ,
     &                POPAR
     &               )
C-----------------------------------------------------------------------
C EVALUATE EMISSIVITIES AND FILL COEF ARRAY
C-----------------------------------------------------------------------
         DO 9 I=1,NIND(IF)
            IS=MOD(INDA(I,IF),10000)
            DO 10 IT=1,MAXT            
               COEF(I,IT,IF)=COEF(I,IT,IF)*POPAR(IS,IT)

               CALL DCLNORM( NDLEV ,NDMET ,
     &                      NORD  ,
     &                      STACK(1,1,IT)  ,
     &                      COEF(I,IT,IF)
     &                     )

 10         CONTINUE
 9       CONTINUE
 100  CONTINUE
      RETURN
C-----------------------------------------------------------------------
 1000 FORMAT(I5,1X,1A15,4X,I1,'(',I1,')',F4.1,3X,F15.0)
      END
