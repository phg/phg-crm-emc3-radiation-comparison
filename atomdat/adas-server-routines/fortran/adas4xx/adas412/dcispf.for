CX IDL UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas412/dcispf.for,v 1.1 2004/07/06 13:28:49 whitefor Exp $ Date $Date: 2004/07/06 13:28:49 $
CX
      SUBROUTINE DCISPF(NTRANS,NTE,NION,NTRA,NLVL,NTSELMAX,ISEL,
     &			I1,J1,IMAXJ,SWL,WLMIN,WLMAX,NTEMP,TE,EDENS,RNH,
     &			PE,IDDEP,LPEND,MENU)
      
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DCISPF *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS412
C
C  SUBROUTINE:
C
C  INPUT: 
C	
C	(I*4) NTRANS():	NUMBER OF TRANSITIONS FOR WHICH DATA ARE
C                       REQUESTED - MAY BE MORE OR LESS THAN ARE
C                       CALCULATED.
C
C	(I*4) NTE	  :   MAXIMUM NUMBER OF TEMPERATURES.
C	
C	(I*4) NION	  :   MAXIMUM NUBER OF SELECTED IONS.
C
C	(I*4) NTRA	  :   MAXIMUM NUMBER OF TRANSITIONS.
C
C	(I*4) NLVL	  :   MAXIMUN NUMBER OF ATOMIC LEVELS.
C
C	(I*4) NTSELMAX:   MAXIMUM NUMBER OF SELECTIONS BY TRANSITION.
C
C
C  OUTPUT:
C
C	(I*4) ISEL    : SWITCH (1) SELECT TRANSITIONS BY INDEX
C				     (3) SELECT TRANSITIONS BY WAVELENGTH RANGE.
C
C	(I*4) I1(NLVL,NION): ARRAY OF LOWER INDICES OF TRANSITIONS.
C
C	(I*4) J1(NLVL,NION): ARRAY OF UPPER INDICES OF TRANSITIONS.
C
C	(I*4) IMAXJ(NION)	 : INDEX OF HIGHEST LEVEL FOR WHICH RESULTS ARE
C				   REQUIRED
C
C	(R*8) SWL(NTRA,NION): ENTERED SPECTROSCOPIC WAVELENGTHS.
C
C	(R*8) WLMIN(NTRA,NION): MINIMUM OF WAVELENGTH RANGE.
C
C	(R*8) WLMAX(NTRA,NION): MAXIMUM OF WAVELENGTH RANGE.
C
C	(I*4) NTEMP	:	NUMBER OF TEMPERATURES.
C	
C	(R*8) TE(NTE):	ARRAY OF VALUES OF LOG10 TEMPERATURE
C                           (TEMPERATURE IN KELVIN).
C
C	(R*8) EDENS(NTE): ELECTRON DENSITY(CM-3).
C	
C	(R*8) RNH(NTE):	AN ARRAY OF VALUES OF THE RATIO OF
C                           HYDROGEN TO ELECTRON DENSITIES.
C
C	(R*8) PE(NTE):	ELECTRON PRESSURE (K.CM-3)
C
C	(I*4) IDDEP:	SWITCH (1) FOR DENSITY DEPENDECE 
C                       	 (2) NO DENSITY DEPENDENCE
C
C	(I*4) LPEND:	0- "DONE" SELECTED 1- "CANCEL"
C
C	(I*4) MENU:		1- EXIT TO MENU.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR: RICHARD MARTIN, UNIVERSITY OF STRATHCLYDE, OCTOBER 1997
C
C VERSION: 1.1						DATE: 27-10-97
C
C-----------------------------------------------------------------------
	INTEGER 	NTE,	NION,	NTRA,	NLVL,	NTSELMAX, NTEMP	
      INTEGER 	ISEL(NION),	NTRANS(NION),	IMAXJ(NION)
      INTEGER 	I1(NLVL,NION), J1(NLVL,NION)	
      INTEGER	I,	LPEND, MENU, IDDEP
C-----------------------------------------------------------------------     
      REAL*8	WLMIN(NTRA,NION),	WLMAX(NTRA,NION), SWL(NTRA,NION)
      REAL*8 	TE(NTE), EDENS(NTE), RNH(NTE), PE(NTE)    
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU  
      PARAMETER( PIPEIN=5  , PIPEOU=6 )
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)NTE
      CALL XXFLSH(PIPEOU)    
      WRITE(PIPEOU,*)NTSELMAX
      CALL XXFLSH(PIPEOU) 

C-----------------------------------------------------------------------       
C	Write default data to idl
C-----------------------------------------------------------------------
         		      
      READ(PIPEIN,*)LPEND
      READ(PIPEIN,*)MENU

      READ(PIPEIN,*)ISEL(1)
      READ(PIPEIN,*)NTRANS(1)

      IF (ISEL(1).EQ.1) THEN
      		READ(PIPEIN,*)(I1(I,1),I=1,NTSELMAX)
      		READ(PIPEIN,*)(J1(I,1),I=1,NTSELMAX)
      		READ(PIPEIN,*)(SWL(I,1),I=1,NTSELMAX)
      ELSE 
			READ(PIPEIN,*)IMAXJ(1)
      		READ(PIPEIN,*)WLMIN(1,1)
      		READ(PIPEIN,*)WLMAX(1,1)
      ENDIF      

C-----------------------------------------------------------------------       
C	Received data from idl (in case its modified)
C-----------------------------------------------------------------------

	READ(PIPEIN,*)IDDEP

	READ(PIPEIN,*)NTEMP
       DO 7 I=1,NTEMP
      	READ(PIPEIN,*)TE(I)
      	READ(PIPEIN,*)EDENS(I)
      	READ(PIPEIN,*)PE(I)
      	READ(PIPEIN,*)RNH(I)  					 
7     CONTINUE      

      RETURN
      END
