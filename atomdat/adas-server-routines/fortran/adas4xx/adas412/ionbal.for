       SUBROUTINE IONBAL( YEAR  , YEARDF, ROOTPATH, ELEM,
     &                    IFAIL ,
     &                    IZ0   , ITVAL ,
     &                    DTEV  , DDENS , DDENSH,
     &                    FABUND
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: IONBAL *********************
C
C VERSION : 1.1
C
C CALLING PROGRAM: ADAS412
C
C PURPOSE : TO EVALUATE EQUILIBRIUM IONIS. BALANCE IN A PLASMA OF
C           FIXED ELECTRON TEMPERATURE, ELECTRON DENSITY AND NEUTRAL
C           HYDROGEN DENSITY.
C
C NOTE    : ATOMIC RATE COEFFICIENT DATA ARE EXTRACTED FROM THE
C           MASTER ELEMENT FILES USING THE SUBROUTINE 'DHDATA'.
C
C INPUT   : (C*2)  YEAR       = YEAR OF DATA
C           (C*2)  YEARDF     = DEFAULT YEAR OF DATA IF REQUESTED YEAR
C                               DOES NOT EXIST - DATA FROM CENTRAL ADAS
C           (C*80) ROOTPATH   = PATH TO ADF11 DIRECTORY(C*2)  
C           (C*2)  ELEM       = ELEMENT
C           (I*4)  IZ0        = NUCLEAR CHARGE
C           (I*4)  ITVAL      = NUMBER OF DTEV(),DDENS(),DDENSH() SETS
C           (R*8)  DTEV()     = DLOG10(ELECTRON TEMPERATURES (EV))
C           (R*8)  DDENS()    = DLOG10(ELECTRON DENSITIES (CM-3))
C           (R*8)  DDENSH()   = DLOG10(NEUTRAL. H  DENSITIES (CM-3))
C
C OUTPUT  : (I*4)  IFAIL      = 0     IF ROUTINE SUCCESSFUL
C                               1     IF ROUTINE UNSUCCESSFUL
C           (R*8)  FABUND()   = FRACTIONAL ABUNDANCES FOR DTEV() ETC.
C
C PROGRAM : (I*4)  NTDIM      = MAXIMUM NUMBER OF TEMP,DENS PAIRS
C           (I*4)  NTDIMD     = MAXIMUM NUMBER OF DATA TEMP & DENS
C           (I*4)  NZDIM      = MAXIMUM NUMBER OF IONISATION STAGES
C           (I*4)  ICLASA()   = CLASSES OF DATA TO BE EXTRACTED
C           (I*4)  NCLASS     = NUMBER OF DATA CLASSES TO BE EXTRACTED
C           (I*4)  ICLASS     = INDEX OF PARTICULAR CLASS
C           (I*4)  IZZ        = ION CHARGE
C           (I*4)  IZ1        = ION CHARGE+1
C           (I*4)  IEVCUT     = ENERGY CUTOFF (EV)
C           (I*4)  ITMAXD     = NUMBER OF DATA DTEVD()
C           (I*4)  IDMAXD     = NUMBER OF DATA DDENS()
C           (I*4)  IZMAXD     = NUMBER OF DATA ZDATA()
C           (I*4)  IT         = INDEX USED WITH TEMPS
C           (I*4)  ICL        = INDEX USED WITH DATA CLASSES
C           (I*4)  N          = NO. OF IONIS. STAGES INCL. BARE NUCLEUS
C                               (EQUALS IZ0+1)
C           (I*4)  IZM        = ION CHARGE -1
C           (R*8)  DENS()     = ELECTRON DENSITIES (CM-3)
C           (R*8)  DENSH()    = NEUTRAL H DENSITIES (CM-3)
C           (R*8)  DTEVD()    = DLOG10(DATA ELECTRON TEMPS (EV))
C           (R*8)  DDENSD()   = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C           (R*8)  ZDATA()    = Z1 CHARGES IN DATA SET
C           (R*8)  DRCOFD(,,) = DLOG10(DATA RATE COEFFICIENTS (CM3/S))
C           (R*8)  DRCOFI()   = INTERPOLATION OF DRCOFD(,,) FOR
C                               DTEV() & DDENS()
C           (R*8)  ACDA(,)    = INTERPOLATED RECOM. COEFFT (CM3/S)
C           (R*8)  SCDA(,)    = INTERPOLATED IONIS. COEFFT (CM3/S)
C           (R*8)  CCDA(,)    = INTERPOLATED CXR COEFFT. (CM3/S)
C           (R*8)  POPF()     = STAGE FRACTIONAL ABUNDANCES
C           (R*8)  EV         = TEMPERATURE (K) EUIVALENT TO 1 EV
C           (R*8)  RH         = RATIO (H DENS)/(ELEC. DENS)
C           (R*8)  U          = TEMPORARY PARAMETER
C           (R*8)  SUM        = TEMPORARY PARAMETER
C
C ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           DXRD11     ADAS      EXTRACT ADF11 DATA FROM FILES
C
C
C AUTHOR  : HP SUMMERS
C           K1/1/57
C           JET EXT. 4941
C
C DATE    : 25/06/91
C
C UPDATE  : 27/04/92 HP SUMMERS  - ADDED DEFAULT YEAR FOR DATA IF
C                                  REQUESTED YEAR DOES NOT EXIST.
C                                  (ADDED 'YEARDF') IN DHDATA
C
C VERSION 1.1                                                   
C DATE: 28-10-97
C               RICHARD MARTIN
C               PUT UNDER SCCS CONTROL (ADAS412).
C
C VERSION : 1.2
C DATE    : 14-03-2013
C MODIFIED: Martin O'Mullane
C               - Allow adf11 files to be specified by user and year,
C                 and not harwired to read from central ADAS.
C               - Replace dhdata with dxrd11 to allow reading latest
C                 adf11 specification.
C               - Argument list changed to pass in directory location
C                 of the adf11 files.
C               - Unused and redundant variables removed.
C
C----------------------------------------------------------------------

       INTEGER   NTDIM,  NTDIMD,   NZDIM, N
       PARAMETER ( NTDIM  = 101 , NTDIMD = 60 , NZDIM = 92 )
C
       INTEGER   IZ0,         IZ1,    IZZ,    IZM 
       INTEGER   IT,  ITMAXD, IDMAXD
       INTEGER   ICL, ICLASS, NCLASS, ICLASA(3)
       INTEGER   IFAIL, IFAIL1, IZ, I
       integer   L1, L2, L3, L4, L5, L6
       integer   iprtd, igrdd, itval, ismax, izmax, nzdimd
C
       LOGICAL   LEXIST, lpart
C
       REAL*8    DTEV(NTDIM)
       REAL*8    DDENS(NTDIM), DDENSH(NTDIM)
       REAL*8    DENS(NTDIM) , DENSH(NTDIM)
       REAL*8    FABUND(NTDIM,NZDIM)
C
       REAL*8    DTEVD(NTDIMD) , DDENSD(NTDIMD) , ZDATA(NTDIMD)
       REAL*8    DRCOFD(NTDIMD,NTDIMD,NTDIMD)
       REAL*8    DRCOFI(NTDIM)
C
       REAL*8    ACDA(NTDIM,NZDIM) , SCDA(NTDIM,NZDIM)
       REAL*8    CCDA(NTDIM,NZDIM)
       REAL*8    POPF(NZDIM)
       REAL*8    RH, U, SUM 
C        
       CHARACTER YEAR*2   , YEARDF*2
       CHARACTER ROOTPATH*80, ELEM*2   , dsnfull*120 ,
     &           centroot*80, dsndef*80, esym*2
       character class(3)*3   

       integer   npart(nzdim), nptn(4)
       
C-----------------------------------------------------------------------

       DATA NCLASS/3/
       DATA ICLASA/    1,     2,    3/
       DATA CLASS /'acd', 'scd', 'ccd'/

C-----------------------------------------------------------------------
C      DENS AND DENSH CONVERSION FROM LOG10 FORMS
C-----------------------------------------------------------------------

       IFAIL=0

       DO 7 IT     = 1 , ITVAL
          DENS(IT)   = 10.0D0**DDENS(IT)
          DENSH(IT)  = 10.0D0**DDENSH(IT)
    7  CONTINUE

C-----------------------------------------------------------------------

       CALL GETENV('ADASCENT',CENTROOT)
       
       call xxcase(elem, esym, 'LC')
       call xxslen(esym, L1, L2)
       call xxslen(rootpath, L3, L4)
       call xxslen(centroot, L5, L6)
       
       DO 200 ICL=1,NCLASS
       
         ICLASS=ICLASA(ICL)
         
         dsnfull = rootpath(L3:L4) // class(icl) // year // '/' //
     &             class(icl) // year // '_' // esym(L1:L2) // '.dat'   
         dsndef  = centroot(L5:L6) // class(icl) // yeardf // '/' //
     &             class(icl) // yeardf // '_' // esym(L1:L2) // '.dat'   
        
         inquire(file=dsnfull,exist=lexist)           
         if (.not.lexist) then 
             write(0,1060)dsnfull          
             inquire(file=dsndef,exist=lexist)           
             if (.not.lexist) then
                 if (iclass.ne.3) then
                    write(0,1070)dsndef
                    ifail = -1
                    return
                 endif
             else
                 write(0,1080)dsndef
                 dsnfull = dsndef             
             endif                               
         endif                                           
        
         if (iclass.eq.3.and.(.not.lexist)) goto 200
         
         DO 190 IZZ=1,IZ0
           IZ1=IZZ

C-----------------------------------------------------------------------
C      FETCH SELECTED DATASETS, EXTRACT ACD SCD CCD VALUES ONLY
C      AND INTERPOLATE
C
C      NOTE: the routine cannot form an ionisation balance with
C            metastable resolved (partial) adf11 datasets.
C-----------------------------------------------------------------------

           IFAIL1 = 0
           lpart  = .FALSE.
           iprtd  = -1
           igrdd  = -1
         
           call dxrd11( dsnfull, lpart  , ifail1 ,
     &                  iz0    , npart  , iprtd  , igrdd , iclass ,
     &                  iz1    , ntdim  ,
     &                  ntdimd , nzdimd , itval ,
     &                  ismax  , izmax  , itmaxd , idmaxd, nptn   ,
     &                  dtev   , ddens  ,
     &                  dtevd  , ddensd , drcofd , zdata  ,
     &                  drcofi
     &                )

C-----------------------------------------------------------------------
C      CONVERT INTERPOLATED VALUES TO LINEAR UNITS
C-----------------------------------------------------------------------

         DO 140 IT = 1 , itval
           IF    (ICL.EQ.1)THEN
               ACDA(IT,IZ1)=10.0D0 ** DRCOFI(IT)
           ELSEIF(ICL.EQ.2)THEN
               SCDA(IT,IZ1)=10.0D0 ** DRCOFI(IT)
           ELSEIF(ICL.EQ.3)THEN
               IF(IFAIL1.LE.0)THEN
                   CCDA(IT,IZ1)=10.0D0 ** DRCOFI(IT)
               ELSE
                   CCDA(IT,IZ1)=0.0D0
               ENDIF
           ENDIF
  140    CONTINUE
  
  190   CONTINUE
  
  200  CONTINUE




C-----------------------------------------------------------------------
C  EVALUATE EQULIBRIUM FRACTIONAL ABUNDANCES OF ELEMENT
C  IONISATION STAGES AT EACH TEMPERATURE/DENSITY
C-----------------------------------------------------------------------
C
       N=IZ0+1

       DO 100 IT=1,itval
          RH=DENSH(IT)/DENS(IT)
          IZM=0
C
          DO 40 IZ=1,IZ0
             U=SCDA(IT,IZ)/(ACDA(IT,IZ)+RH*CCDA(IT,IZ))
             IF(U.GT.1.0D0)IZM=IZM+1
 40       CONTINUE
C
          POPF(IZM+1)=1.0D0
          IF(IZM.GE.1)THEN
             DO 60 IZ=1,IZM
                U=(ACDA(IT,IZ)+RH*CCDA(IT,IZ))/SCDA(IT,IZ)
                POPF(IZ)=1.0D0
                DO 50 I=1,IZ
                   POPF(I)=POPF(I)*U
 50             CONTINUE
 60          CONTINUE
          ENDIF
C     
          IF(IZM.LT.IZ0) THEN
             DO 80 IZ=IZ0,IZM+1,-1
                POPF(IZ+1)=1.0D0
                U=SCDA(IT,IZ)/(ACDA(IT,IZ)+RH*CCDA(IT,IZ))
                DO 70 I=N,IZ+1,-1
                   POPF(I)=POPF(I)*U
 70             CONTINUE
 80          CONTINUE
          ENDIF
C
          SUM=0.0D0
          DO 85 I=1,N
             SUM=SUM+POPF(I)
 85       CONTINUE
          DO 95 I=1,N
             POPF(I)=POPF(I)/SUM
             FABUND(IT,I)=POPF(I)
 95       CONTINUE
C
 100   CONTINUE
C
       RETURN
C
C-----------------------------------------------------------------------
C
 1060  FORMAT(1X,'REQUESTED DATASET DOES NOT EXIST : ',A)
 1070  FORMAT(1X,'DEFAULT DATASET DOES NOT EXIST   : ',A)
 1080  FORMAT(1X,'USING DEFAULT DATASET            : ',A)
 
       END
