      PROGRAM ADAS412
C
      IMPLICIT NONE
C--------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS412 **********************
C
C VERSION:      1.1 (MODIFIED VERSION OF GOFT V2.0)
C
C PURPOSE:  PROGRAM TO FETCH VALUES OF THE THREE FACTORS THAT MAKE UP
C           THE G(T) FUNCTION, TO EVALUATE THESE FUNCTIONS AND THEN TO
C           LIST THEM TO A FILE. THERE ARE THREE SOURCE DATA FILES OR
C           SUB-ROUTINES FROM WHICH THE FACTORS ARE FETCHED VIZ:-
C           (1) THE RATIO OF THE POPULATION DENSITY OF HYDROGEN (ATOMS
C           PLUS IONS) TO THAT OF ELECTRONS CALCULATED AS A FUNCTION
C           OF TEMPERATURE FOR THE SOLAR ATMOSPHERE.
C           (2) THE STEADY-STATE IONIZATION BALANCE RATIO OF THE
C           POPULATION DENSITY OF THE SELECTED ION TO THE POPULATION
C           DENSITY OF ALL IONS OF THAT ELEMENT - A FUNCTION OF
C           TEMPERATURE AND POSSIBLY DENSITY.
C           (3) THE EFFECTIVE EXCITATION RATE COEFFICIENT FOR THE
C           UPPER LEVEL OF THE SELECTED TRANSITION - A FUNCTION OF
C           TEMPERATURE AND ELECTRON DENSITY.
C
C NOTES:        FORTRAN CODE ALLOWS CALCULATIONS FOR MULTIPLE IONS.
C               CURRENTLY SETUP FOR SINGLE ION (V1.1)
C
C DESCRIPTION:
C           THE PROGRAM CONSISTS OF A NUMBER OF SECTIONS OF WHICH THE
C           FIRST GROUP (A) IS CONCERNED WITH CALLING THE REQUIRED
C           DATA FROM THE DATA BANK. THE MIDDLE SECTIONS (B) ARE
C           CONCERNED WITH THE SELECTION OF THE TRANSITIONS,
C           CALCULATING THEIR WAVELENGTHS ETC. FINALLY SECTIONS (C)
C           EVALUATE THE G(T) FUNCTIONS AND LIST THE RESULTS TO A
C           FILE.
C
C INPUT:    (C*43) FILENH   STRING HAVING NAME OF FILE CONTAINING
C                           VALUES OF n(H)/n(e)
C           (I*4)  MSEL     SWITCH TO CHOOSE BETWEEN UNIFORM DENSITY
C                           AND UNIFORM PRESSURE.
C           (C*44) DSNIN    STRING COMPOSED OF '/' + FILENH
C           (I*4)  NTEMP    THE NUMBER OF TEMPERATURE VALUES.
C           (R*8)  TE()     AN ARRAY OF VALUES OF LOG10 TEMPERATURE
C                           (TEMPERATURE IN KELVIN).
C                           DIMENSION: TEMPERATURE INDEX.
C           (R*8)  RNH()    AN ARRAY OF VALUES OF THE RATIO OF
C                           HYDROGEN TO ELECTRON DENSITIES.
C                           DIMENSION: TEMPERATURE INDEX.
C           (R*8)  ELD      (UNIFORM) ELECTRON DENSITY.
C           (R*8)  EDENS()  ELECTRON DENSITY(CM-3).
C                           DIMENSION:  TEMPERATURE INDEX.
C           (R*8)  ELP      (UNIFORM) ELECTRON PRESSURE (K.CM-3)
C           (C*44) FILEPM   STRING HAVING NAME OF FILE WITH DETAILS
C                           OF LABORATORY PLASMA MODEL.
C           (C*2)  FILEIB   STRING HAVING NAME OF SUB-R'TINE GIVING
C                           VALUES OF IONIZATION BALANCE RATIO
C           (C*2)  ELEM     CHEMICAL SYMBOL OF THE SELECTED ELEMENT
C           (I*4)  NIONS    NUMBER OF IONS SELECTED.
C           (C*2)  SQI()    ISO-ELECTRONIC SEQUENCE - EG LI.
C                           DIMENSION: ION INDEX.
C           (C*7)  SEQ()    ISO-ELECTRONIC SEQUENCE - EG Li-like.
C                           DIMENSION: ION INDEX.
C           (C*2)  SYMBL()  LIST OF 92 CHEMICAL SYMBOLS - H to U.
C                           DIMENSION: NUCLEAR CHARGE.
C           (I*4)  IZ0      NUCLEAR CHARGE.
C           (I*4)  IZS      DIFFERENCE BETWEEN THE NUCLEAR CHARGE AND
C                           THE ION CHARGE.
C           (I*4)  IZ()     CHARGES OF THE SELECTED IONS
C                           DIMENSION: ION INDEX.
C           (I*4)  IZ1      = IZ+1
C           (I*4)  IT IU    THE TENS AND UNITS DIGITS OF IZ1
C           (C*4)  SPECU(), SPECT() CHARACTER STRINGS
C                           ASSOCIATED WITH SETTING THE SPECTROSCOPIC
C                           NOTATION FOR THE IONS
C                           DIMENSION: UNITS OR TENS DIGIT OF IZS.
C           (I*4)  ISPEC()  INTEGER ASSOCIATED WITH THE ABOVE.
C                           DIMENSION :UNITS OR TENS DIGIT OF IZS.
C           (C*11) SPEC()   ION IN SPECTROSCOPIC NOTATION.
C                           DIMENSION: ION INDEX.
C           (R*8)  TEV()    LOG10 OF TEMPERATURE IN EV
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  DDENS    LOG10 ELECTRON DENSITY IN CM-3
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  DDENSH   LOG10 HYDROGEN DENSITY IN CM-3
C                           DIMENSION :TEMPERATURE INDEX.
C           (I*4)  IFAIL    = 0 IF DATA FRANSFER IS OK.
C                           = 1 IF DATA TRANSFER IS NOT OK.
C           (R*8)  RIB(,)   RATIO OF THE POPULATIONS OF THE INDIVIDUAL
C                           SELECTED IONS TO THE TOTAL ION POPULATION
C                           AS FUNCTIONS OF TEMPERATURE.
C                           1ST DIMENSION :TEMPERATURE INDEX.
C                           2ND DIMENSION :ION INDEX.
C           (C*44) FILEEX() STRING HAVING NAME OF SUB-R'TINE GIVING
C                           VALUES OF EFFECTIVE EXCITATION COEFFS.
C                           DIMENSION :ION INDEX.
C           (L*4)  LFXIST() =.TRUE.   :COPASE FILE EXISTS.
C                           =.FALSE.  :NO COPASE FILE.
C                           DIMENSION :ION INDEX.
C           (I*4)  INDA(,)  TRANSITION IDENTIFIER EQUAL TO 10000 TIMES
C                           INDEX OF LOWER LEVEL PLUS INDEX OF UPPER
C                           LEVEL.
C                           1ST DIMENSION   :TRANSITION INDEX.
C                           2ND DIMENSION   :ION INDEX.
C           (I*4)  NINDA()  NUMBER OF TRANSITIONS THUS IDENIFIED
C                           DIMENSION :ION INDEX.
C           (I*4)  NLVLS()  THE NUMBER OF LEVELS IN FILEEX FOR WHICH
C                           DATA ARE AVAILABLE.
C                           DIMENSION :ION INDEX.
C           (C*51) SPLVL(,) A CHARACTER STRING CONTAINING THE INDEX
C                           NUMBER, THE IDENTIFICATION OF THE LEVEL
C                           AND ITS ENERGY WRT THE GROUND LEVEL.
C                           1ST DIMENSION   :LEVEL INDEX.
C                           2ND DIMENSION   :ION INDEX.
C           (L*4)  LPSEL    PROTON COLLISIONS INCLUDED/EXCLUDED.
C           (L*4)  LZSEL    SCALE/DONOT SCALE PROTON COLS. WITH ZEFF.
C           (L*4)  LISEL    IONIZATION INCLUDED/EXCLUDED
C           (L*4)  LHSEL    CHARGE TRANSFER INCLUDED/EXCLUDED.
C           (L*4)  LRSEL    RECOMBINATION INCLUDED/EXCLUDED.
C           (R*8)  TEA()    ELECTRON TEMPERATURE IN EV.
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  TPA()    PROTON TEMPERATURE IN EV.
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  THA()    NEUTRAL HYDROGEN TEMPERATURE IN EV.
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  DENSPA() PROTON DENSITY IN CM-3.
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  RATHA()  RATIO NEUTRAL H TO ELECTRON DENSITIES.
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  RATIA()  RATIO n(z+1) TO n(z).
C                           DIMENSION :TEMPERATURE INDEX.
C           (R*8)  ZEFF     =1 UNLESS LZSEL=.TRUE.
C           (R*8)  COEF(,,) AN ARRAY OF VALUES OF THE EFFECTIVE
C                           EXCITATION RATE COEFFICIENTS TAKEN FROM
C                           FILEEX
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :TEMPERATURE INDEX.
C                           3RD DIMENSION  :ION INDEX.
C           (I*4)  ISEL()   A SWITCH HAVING A VALUE 1, 2 OR 3
C                           DEPENDING ON HOW THE TRANSITIONS
C                           ARE SELECTED BY THE USER.
C                           DIMENSION :ION INDEX.
C           (I*4)  NTRANS() NUMBER OF TRANSITIONS FOR WHICH DATA ARE
C                           REQUESTED - MAY BE MORE OR LESS THAN ARE
C                           CALCULATED
C                           DIMENSION :ION INDEX
C           (I*4)  I1(,)    INDEX OF LOWER LEVEL OF SELECTED
C                           TRANSITION
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (I*4)  J1(,)    INDEX OF UPPER LEVEL OF SELECTED
C                           TRANSITION
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (R*8)  AWL      THE APPROXIMATE WAVELENGTH OF THE CURRENT
C                           TRANSITION AS ENTERED BY THE USER.
C           (R*8)  WLMIN()  MINIMUM OF WAVELENGTH RANGE.
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (R*8)  WLMAX()  MAXIMUM OF WAVELENGTH RANGE.
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (I*4)  IMAXJ()  THE INDEX OF THE HIGHEST LEVEL FOR WHICH
C                           RESULTS ARE REQUIRED.
C                           DIMENSION :ION INDEX.
C           (C*51) SPECTR   STRING CONTAINING CURRENT SPLVL()
C           (R*8)  ENG()    THE ENERGY OF THE IDENTIFIED LEVEL IN CM-1
C           (R*8)  WL(,,)   THE WAVELENGTH OF THE IDENTIFIED
C                           TRANSITION IN ANGSTROMS
C                           1ST DIMENSION  :LOWER LEVEL INDEX.
C                           2ND DIMENSION  :UPPER LEVEL INDEX.
C                           3RD DIMENSION  :ION INDEX.
C           (I*4) IFLAG(,,) FLAGS TRANSITIONS FOR WHICH RESULTS ARE
C                           REQUESTED.
C                           1ST DIMENSION  :LOWER LEVEL INDEX.
C                           2ND DIMENSION  :UPPER LEVEL INDEX.
C                           3RD DIMENSION  :ION INDEX.
C           (C*1)  ANS      CONTROLS ENTRY OF SPECTROSCOPIC
C                           WAVELENGHTS.
C OUTPUT:   (I*4)  NRES()   NUMBER OF TRANSITIONS FOR THE IDENTIFIED
C                           ION FOR WHICH RESULTS HAVE BEEN CALCULATED
C                           DIMENSION :ION INDEX.
C           (I*4)  II()     THE INDEX OF THE LOWER LEVEL OF THE
C                           TRANSITION INDA()
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (I*4)  JJ()     THE SAME FOR THE UPPER LEVEL
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (R*8)  APWL()   APPROX WAVELENGTH OF THE IDENTIFIED
C                           TRANSITION CALCULATED FROM LEVEL ENERGIES.
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (R*8)  GOFT(,,) ARRAY OF VALUES OF THE G(T) FUNCTIONS.
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :TEMPERATURE INDEX.
C                           3RD DIMENSION  :ION INDEX.
C           (R*8)  GMAX(,)  THE MAX VALUE OF G(T) OF THE IDENTIFIED
C                           TRANSITION.
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (I*4)  MAXT(,)  INDEX OF THE TEMPERATURE FOR THE ABOVE.
C                           1ST DIMENSION  :TRANSITION INDEX.
C                           2ND DIMENSION  :ION INDEX.
C           (I*4)  KMIN()   INDEX OF TEMPERATURE WHERE THE IDENTIFIED
C                           G(T) BECOMES NEGLIGIBLE - LOW LIMIT.
C                           DIMENSION  :ION INDEX.
C           (I*4)  KMAX()   AS ABOVE BUT HIGH LIMIT.
C                           DIMENSION  :ION INDEX.
C           (R*8)  VALLIM   PARAMETER  :LIMITING VALUE BELOW WHICH
C                           G(T) IS NOT LISTED.
C           (R*8)  SWL(,)   SPECTROSCOPIC WAVELENGTH - ENTERED BY
C                           USER.
C                           1ST DIMENSION   :TRANSITION INDEX.
C                           2ND DIMENSION   :ION INDEX.
C           (I*4)  IDDEP    SWITCH FOR DENSITY DEPENDECE =1
C                           OR NO DENSITY DEPENDENCE     =0
C           (C*3)  REP      = 'YES' => TERMINATE PROGRAM EXECUTION.
C                           = 'NO ' => CONTINUE PROGRAM EXECUTION.
C           (C*80) DSNINC   = INPUT COPASE DATA SET NAME (FULL MVS DSN)
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C           (C*80) DSNINC   = INPUT COPASE DATA SET NAME (FULL MVS DSN)
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C           (R*8)  BWNO     = IONISATION POTENTIAL (CM-1)
C
C           (I*4)  IA()     = ENERGY LEVEL INDEX NUMBER
C
C           (I*4)  IL       = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C           (C*18) CSTRGA() = NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C
C           (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                            NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C           (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C
C           (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                            NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C
C           (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C
C           (C*22) STRGA() = LEVEL DESIGNATIONS
C
C           (I*4)  NTTRANS = NUMBER OF CALCULATED G(T)'S
C
C           (I*4)  LPEND     0 = CONTINUE 1= CANCEL
C
C           (I*4)  MENU      1 = EXIT TO MENU.
C
C           (I*4)  TEXTFLAG  1= TEXT OUTPUT REQUESTED
C
C           (I*4)  GRAPHFLAG 1= GRAPH OUTPUT REQUESTED
C
C  ROUTINES:
C          ROUTINE      SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C               XX0000  ADAS    SET MACHINE DEPENDANT ADAS CONFIGURATION
C               XXDATE  ADAS    GATHERS CURRENT DATE AS 8 BYTE STRING
C               DCSPF0  ADAS    READS INPUT FILE INFO. FROM IDL
C               D7DATA  ADAS    GATHERS RELEVANT DATA FROM INPUT FILE
C                               TO BE PIPED TO IDL
C               BXSETP  ADAS    WRITES LEVEL DATA FROM INPUT FILE TO IDL.
C               INIT    ADAS    DETRMINATION OF CHARGE OF THE SELECTED ION
C                               AND SPECTROSCOPIC NOTATION.
C               DCISPF  ADAS    GATHERS DATA FROM IDL FOR PROCESSING
C               IONBAL  ADAS    EVALUATES EQUILIBRIUM IONISATION BALANCE
C               XCOEF   ADAS    CALCULATES  COMPLETE SETS OF SPECTRUM LINE
C                               EMISSIVITIES FOR THE IONS OF AN ELEMENT
C               DCOUTG  ADAS    WRITES DATA TO IDL FOR OUTPUT GRAPH.
C
C
C-----------------------------------------------------------------------
C  DECLARATIONS
C    THE VARIOUS ARRAYS ARE GIVEN DIMENSIONS AS FOLLOWS WHERE THE
C    PARAMETERS HAVE THE FOLLOWING MEANINGS:
C        NTEMP    NUMBER OF TEMPERATURE VALUES
C        NTRA     NUMBER OF TRANSITIONS
C        NION     NUMBER OF IONS
C        NLVL     NUMBER OF LEVELS WITHIN EACH ION
C          NTSELMAX MAXIMUM NUMBER OF TRANSITIONS SELECTED BY INDICES
C                               (PASSED TO IDL)
C        VALLIM   LIMITING VALUE BELOW WHICH G(T)'S ARE NOT LISTED
C-----------------------------------------------------------------------
C  USE OF THE INDICES       I - LOWER LEVEL INDEX OF TRANSITION
C                           J - UPPER LEVEL INDEX OF TRANSITION
C                           K - TEMPERATURE INDEX
C                           L - ION INDEX
C                           M - TRANSITION INDEX
C                           N - OTHERS
C-----------------------------------------------------------------------
C  AUTHOR       PETER MCWHIRTER
C               13 PARK CRESCENT
C               ABINGDON OX14 1DF.
C  TELEPHONE    0235-520232
C  DATE         1991 JUNE 27
C-----------------------------------------------------------------------
C  MODIFICATIONS
C  mar21-95 Alessandro Lanzafame, University of Strathclyde
C           Conversion to Unix
C           call XXDATE changed to call DATE - FORTRAN library routine
C           variable (C*8) DATE changed to (C*9) DATEC
C  mar24-95 Alessandro Lanzafame
C           FILEEX from C*44 to C*60
C
C  nov22-95 Alessandro Lanzafame
C             FILEEX from C*60 to C*80 and read from input file
C
C  ADAS PORT:
C
C  VERSION: 1.1                                                 DATE: 28-10-97
C               RICHARD MARTIN.
C               PUT UNDER SCCS CONTROL.
C
C  VERSION:     1.2                                                     DATE: 01-12-97
C               RICHARD MARTIN.
C               REMOVED DUPLICATE DECLARATION FOR NTTRANS.
C
C VERSION : 1.3
C DATE    : 04-08-2008
C MODIFIED: Martin O'Mullane
C               - Increase number of transitions to 3000.
C
C VERSION : 1.4
C DATE    : 29-08-2008
C MODIFIED: Martin O'Mullane
C               - Initialise kmi and kma to 1 rather than letting them
C                 take any random value.
C
C VERSION : 1.5
C DATE    : 03-02-2009
C MODIFIED: Allan Whiteford
C               - Do not print warning messages in the middle of
C                 the ADF20 file
C               - Write a final '1' back to IDL just before
C                 exiting to allow files etc. to be flushed
C                 before IDL frees up the pipe.
C
C VERSION : 1.6
C DATE    : 31-10-2009
C MODIFIED: Martin O'Mullane
C               - Use xxdata_04 rather than d7data.
C               - Rename init to dcinit.
C               - Add real name of user via XXNAME.
C               - Increase number of levels to 1500 and transitions
C                 to 10000.
C
C VERSION : 1.7
C DATE    : 31-10-2009
C MODIFIED: Martin O'Mullane
C               - Change argument list to ionbal subroutine which
C                 enables the user choice of adf11 on input screen
C                 rather than forcing files from central ADAS.
C  
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
        INTEGER NTE, NTRA, NION, NLVL, NTSELMAX
        INTEGER PIPEIN, PIPEOU, IUNIT11
        REAL*8  VALLIM

        PARAMETER( NTE  = 101,    NTRA =10000,    NION = 1,
     &             NLVL = 1500,   NTSELMAX=20,    VALLIM=1.0D-99)
        PARAMETER( PIPEIN=5, PIPEOU=6, IUNIT11=11)

        INTEGER   NDLEV  , NDTRN  , NDTEM , NZDIM  , NDMET, NVMAX, NDQDN
        PARAMETER(NDLEV =1500, NDTRN =10000, NDTEM=101, 
     &            NZDIM =1 , NDMET=5)
        PARAMETER(NDQDN=6 , NVMAX = 14)
      
C--------------------------------------------------------------------
C     INTRINSIC( DLOG10)
C--------------------------------------------------------------------
        INTEGER NIONS, NTEMP, IDDEP, IFAIL, NTTRANS
        INTEGER I,J, IJ, K, KK, L, LL, M, MM, N, KMI, KMA, N1, N2

        INTEGER IZ0, IZ1, IL, ICNTE
        INTEGER IA(NLVL),  ISA(NLVL), ILA(NLVL)

        REAL*8  BWNO, XJA(NLVL), WA(NLVL)

        REAL*8  TE(NTE),      RNH(NTE),          TEV(NTE), PE(NTE)
        REAL*8  DDENS(NTE),   DDENSH(NTE),   TEA(NTE)
        REAL*8  TPA(NTE),     THA(NTE),          EDENS(NTE)
        REAL*8  DENSPA(NTE),  RATHA(NTE),    RATIA(NTE)
        REAL*8  ENG(NLVL,NION)
        REAL*8  ABUND(NTE,92),      RIB(NTE,NION)
        REAL*8  WLMIN(NTRA,NION), WLMAX(NTRA,NION),   GMAX(NTRA,NION)
        REAL*8  APWL(NTRA,NION)
        REAL*8  SWL(NTRA,NION)
        REAL*8  COEF(NTRA,NTE,NION),          GOFT(NTRA,NTE,NION)
        REAL*8  WL(NLVL,NLVL,NION)
        REAL*8  ZEFF, DDEN, DPEN, TTEN

       INTEGER   ISPEC(9)
       INTEGER   IZ(NION)
       INTEGER   NINDA(NION),   ISEL(NION),    NTRANS(NION)
       INTEGER   IMAXJ(NION),   KMIN(NION),    KMAX(NION)
       INTEGER   NLVLS(NION),   NRES(NION)
       INTEGER   II(NLVL,NION)
       INTEGER   JJ(NLVL,NION)
       INTEGER   I1(NLVL,NION),      J1(NLVL,NION)
       INTEGER   INDA(NTRA,NION)
       INTEGER   MAXT(NTRA,NION)
       INTEGER   IFLAG(NLVL,NLVL,NION)
       INTEGER   LPEND, MENU, GRAPHFLAG, TEXTFLAG
       INTEGER   IFIRST, ILAST
       INTEGER   LSTR, LENSTR

C--------------------------------------------------------------------
       CHARACTER*80  FILEEX(NION)
       CHARACTER*2   FILEIB,      ELEM,     SYMBL(92)
       CHARACTER*2   SQI(NION),   YEARDF
       CHARACTER*4   SPECU(9),    SPECT(9)
       CHARACTER*7   SEQ(NION)
       CHARACTER*11  SPEC(NION)
       CHARACTER*51  SPLVL(NLVL,NION),   SPECTR
       CHARACTER     DATEC*9   , BTXT*3
       CHARACTER     USERID*20 , user*30

       CHARACTER   GOFTFILE*80

       CHARACTER TITLED*3,       CSTRGA(NLVL)*18, STRGA(NLVL)*22
       CHARACTER  REP*3, DSNINC*80, DSNINI*80, ROOTPATH2*80, DATE*8

C--------------------------------------------------------------------
       LOGICAL     LFXIST(NION),     LPSEL,       LZSEL,      LISEL
       LOGICAL     LHSEL,            LRSEL,       LEXIST
C--------------------------------------------------------------------
       DATA SYMBL/'H','HE','LI','BE','B','C','N','O',
     & 'F','NE','NA','MG','AL','SI','P','S','CL','AR','K',
     & 'CA','SC','TI','V','CR','MN','FE','CO','NI','CU',
     & 'ZN','GA','GE','AS','SE','BR','KR','RB','SR','Y',
     & 'ZR','NB','MO','TE','RU','RH','PD','AG','CD','IN',
     & 'SN','SB','TE','I','XE','CS','BA','LA','CE','PR',
     & 'ND','PM','SM','EU','GD','TB','DY','HO','ER','TM',
     & 'YB','LU','HF','TA','W','RE','OS','IR','PT','AU',
     & 'HG','TL','PB','BI','PO','AT','RN','FA','RA','AC',
     & 'TH','PA','U'/
       DATA SPECU/'I','II','III','IV','V','VI','VII','VIII','IX'/
       DATA SPECT/'X','XX','XXX','XL','L','LX','LXX','LXXX','XC'/
       DATA ISPEC/1,2,3,2,1,2,3,4,2/
       DATA YEARDF/'89'/
C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   npl           , iadftyp       , itieactn , iorb    
      integer   nplr          , npli          , icnti    , icntl   , 
     &          icnts         , itran         , maxlev   , nv
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
C----------------------------------------------------------------------
      integer   ipla(ndmet,ndlev)             , npla(ndlev)
      integer   iitrn(ndtrn)                  , iltrn(ndtrn)       , 
     &          istrn(ndtrn)                  ,                    
     &          ia1a(ndtrn)                   , ia2a(ndtrn)        , 
     &          il1a(ndlev)                   , il2a(ndlev)        , 
     &          is1a(ndlev)                   , is2a(ndlev)        ,
     &          I1A(NDTRN)                    , I2A(NDTRN)
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)  , prtwta(ndmet) , 
     &          zpla(ndmet,ndlev)             , beth(ndtrn)        ,
     &          qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
      real*8    auga(ndtrn)                   , wvla(ndlev)        ,
     &          AVAL(NDTRN)                   , SCEF(NVMAX)        ,
     &          SCOM(NVMAX,NDTRN)
C----------------------------------------------------------------------
      character cpla(ndlev)*1                 , cprta(ndmet)*9     ,
     &          TCODE(NDTRN)*1
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  , lbseta(ndmet)      ,
     &          lqdorb((ndqdn*(ndqdn+1))/2)
      logical   lss04a(ndlev,ndmet)
C----------------------------------------------------------------------


C--------------------------------------------------------------------
C                           PROGRAM STARTS
C---------------------------AAAAAAAAAAAAAA---------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE and name
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
      DATEC=DATE
      CALL XXNAME(USER)
CX
1     CONTINUE

C-----------------------------------------------------------------------
C GET PROTON, COPASE INPUT DATA SET NAMES (FROM ISPF)
C-----------------------------------------------------------------------

      CALL DCSPF0( REP    ,
     &             DSNINC  ,ROOTPATH2, FILEIB, ELEM
     &           )
        IF (REP.EQ.'YES') GOTO 9999
C---------------------------------------------------------
C INITIALIZE SOME VARIABLES
C-------------------------------------------------------
        IDDEP=0
        NIONS=1

        FILEEX(1)=DSNINC

      OPEN( UNIT=IUNIT11 , FILE=FILEEX(1) , STATUS='OLD' )

C       CALL D7DATA(IUNIT11, NLVL, NTRA, TITLED, IZ(1), IZ0, IZ1, BWNO,
C      &                  IL, IA, CSTRGA, ISA, ILA, XJA, WA)
       itieactn = 0
       
       call xxdata_04( iunit11, 
     &                 ndlev  , ndtrn , ndmet   , ndqdn , nvmax ,
     &                 titled , iz    , iz0     , iz1   , bwno  ,
     &                 npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                 il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                 ia     , cstrga, isa     , ila   , xja   ,
     &                 wa     ,
     &                 cpla   , npla  , ipla    , zpla  ,
     &                 nv     , scef  ,
     &                 itran  , maxlev,
     &                 tcode  , i1a   , i2a     , aval  , scom  ,
     &                 beth   ,     
     &                 iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                 letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                 lstyp  , lltyp , itieactn, ltied
     &               )

        CLOSE(IUNIT11)

C-----------------------------------------------------------------------
C ADDED FOR ADAS412 CONVERSION - BXSETP WRITES TO IDL
C-----------------------------------------------------------------------
        ICNTE=0
         CALL BXSETP( IZ0    , IZ(1)     ,
     &                NLVL  , IL     , ICNTE ,
     &                CSTRGA , ISA    , ILA   , XJA  ,
     &                STRGA
     &                  )

      CALL DCINIT(  ELEM, NION, NIONS, IZ,
     &              SQI  , SEQ   , IZ0 , SPEC )

2       CONTINUE

      CALL DCISPF(NTRANS,NTE,NION,NTRA,NLVL,NTSELMAX,ISEL,I1,J1,
     &                  IMAXJ,SWL,WLMIN,WLMAX,NTEMP,TE,EDENS,RNH,PE,
     &                  IDDEP,LPEND,MENU)

        IF (MENU.EQ.1) GOTO 9999
        IF (LPEND.EQ.1) THEN
                GOTO 1
                LPEND=0

        ENDIF

C--------------------------------------------------------------------
C SECTION FETCHING VALUES OF n(z)/n(Z)
C--------------------------------------------------------------------
C--------------------------------------------------------------------
C   SET THE VALUES OF THE TEMPERATURE AND OF DENSITIES OF THE
C   ELECTRONS AND HYDROGEN ATOMS FOR THE ION BAL SUB-ROUTINE - ALL
C   EXPRESSED AS LOGS.
C--------------------------------------------------------------------
      DO 25 K=1,NTEMP
         TEV(K)=TE(K)-4.06466
         DDENS(K)=0.0
         IF (IDDEP.EQ.1) THEN
             DDENS(K)=DLOG10(EDENS(K))
         ENDIF
         DDENSH(K)=-30.0
25    CONTINUE

C--------------------------------------------------------------------
C   CALL THE ION BAL SUB-ROUTINE AND RE-ARRANGE INDICES OF RIB(,,).
C--------------------------------------------------------------------
      IFAIL=0
      CALL IONBAL( FILEIB,   YEARDF, ROOTPATH2, ELEM,     
     &             IFAIL,
     &             IZ0,      NTEMP ,
     &             TEV,      DDENS ,  DDENSH,
     &             ABUND
     &            )
      DO 26 L=1,NIONS
         DO 27 LL=1,IZ0+1
            IF(LL.EQ.(IZ(L)+1))THEN
               DO 28 K=1,NTEMP
                  RIB(K,L)=ABUND(K,LL)
28             CONTINUE
            ENDIF
27       CONTINUE
26    CONTINUE
C--------------------------------------------------------------------
C SECTION FETCHING VALUES OF THE EFFECTIVE EXCITATION COEFFICIENTS.
C--------------------------------------------------------------------
C   ENTER NAMES OF SUB-ROUTINES PROVIDING EXCITATION COEFFS.
C--------------------------------------------------------------------
      DO 30 L=1,NIONS
         INQUIRE(FILE=FILEEX(L),EXIST=LEXIST)
         IF(.NOT.LEXIST) THEN
            WRITE(0,*)'GOFT ERROR: ADF04 FILE ',FILEEX(L),
     &                ' DOES NOT EXISTS '
            GOTO 9999
         ENDIF
C# mar27-95 end
30    CONTINUE
C--------------------------------------------------------------------
C   SET THE VALUES OF THE TEMPERATURE, DENSITIES OF ELECTRONS AND
C   HYDROGEN ATOMS FOR THE EXCITATION COEFFICIENT SUB-ROUTINE - NOT
C   LOGS THIS TIME.
C--------------------------------------------------------------------
      DO 31 K=1,NTEMP
         TEA(K)=10.0**TEV(K)
         TPA(K)=TEA(K)
         THA(K)=TEA(K)
         DENSPA(K)=0.0
         RATHA(K)=0.0
         RATIA(K)=0.0
31    CONTINUE
      DO 32 L=1,NIONS
         LFXIST(L)=.TRUE.
32    CONTINUE
      IFAIL=0
      LPSEL=.FALSE.
      LZSEL=.FALSE.
      LISEL=.FALSE.
      LHSEL=.FALSE.
      LRSEL=.FALSE.
      ZEFF=1.0
C--------------------------------------------------------------------
C   CALL SUB-ROUTINES PROVIDING EXCITATION COEFFS.
C--------------------------------------------------------------------
         CALL XCOEF(   FILEEX,   IFAIL,    LFXIST,
     &                 NIONS,    NTEMP,
     &                 INDA,     NINDA,    NLVLS,
     &                 LPSEL,    LZSEL,    LISEL,    LHSEL,    LRSEL,
     &                 TEA,      TPA,      THA,
     &                 EDENS,    DENSPA,   RATHA,    RATIA,
     &                 ZEFF,
     &                 COEF,     SPLVL     )


C---------------------------BBBBBBBBBBBBBB---------------------------
C SECTION IDENTIFYING TRANSITIONS FOR WHICH G(T) FUNCTIONS ARE
C REQUIRED
C--------------------------------------------------------------------

C--------------------------------------------------------------------
C   PRESENT TABLES OF THE EXCITED LEVELS FOR WHICH DATA ARE AVAILABLE
C   FOR EACH ION. THESE MAY BE USED TO SELECT THE TRANSITIONS FOR
C   WHICH G(T) FUNCTIONS ARE TO BE CALCULATED.
C--------------------------------------------------------------------
C      DO 40 L=1,NIONS
C         WRITE(6,1030)SEQ(L)
C         READ(5,1031)ISEL(L)
C         WRITE(6,*)'  '
C         DO 41 N=1,NLVLS(L)
C            WRITE(6,1032)SPLVL(N,L)(1:51)
C           WRITE(7,*)SPLVL(N,L)(1:51)
C41       CONTINUE
C--------------------------------------------------------------------
C   SELECT THE REQUIRED TRANSITIONS BY EITHER (1) SPECIFYING THE INDEX
C   NUMBERS OF THE LEVELS, (2) SPECIFYING THE INDIVIDUAL WAVELENGTHS
C   OR (3) SPECIFYING THE RANGE OF WAVELENGTHS THAT INCLUDES ALL THE
C   TRANSITIONS.
C--------------------------------------------------------------------
C         IF(ISEL(L).EQ.1)THEN
C            WRITE(6,1033)
C            READ(5,*)NTRANS(L)
C            DO 42 N=1,NTRANS(L)
c               WRITE(6,1035)
C               READ(5,*)I1(N,L),J1(N,L)
C              WRITE(6,*)ISEL(L),NTRANS(L),I1(N,L),J1(N,L)
C42          CONTINUE
c         ELSEIF(ISEL(L).EQ.2)THEN
C            WRITE(6,1037)
C            READ(5,*)NTRANS(L)
C            WRITE(6,*)L,NTRANS(L)
C            DO 43 N=1,NTRANS(L)
C               WRITE(6,1039)
C               READ(5,*)AWL
C               WLMIN(N,L)=AWL-0.1*AWL
C               WLMAX(N,L)=AWL+0.1*AWL
C               WRITE(6,*)N,WLMIN(N,L),WLMAX(N,L)
C43          CONTINUE
C         ELSEIF(ISEL(L).EQ.3)THEN
C            WRITE(6,1041)
C            READ(5,*)IMAXJ(L)
C            WRITE(6,1043)
C            READ(5,*)WLMIN(1,L),WLMAX(1,L)
C         ENDIF
C40    CONTINUE
C--------------------------------------------------------------------
C   CALCULATE THE WAVELENGTHS OF ALL TRANSITIONS (ALLOWED AND
C   DISALLOWED) AMONG THE LEVELS IN THE TABLE OF INDICES.
C--------------------------------------------------------------------
      DO 44 L=1,NIONS
         DO 45 N=1,NLVLS(L)
            SPECTR=SPLVL(N,L)
            READ (SPECTR(37:51),*)ENG(N,L)
45       CONTINUE
         DO 46 J=2,NLVLS(L)
            DO 47 I=1,J-1
               IF ((ENG(J,L)-ENG(I,L)).NE.0.0) THEN
                  WL(I,J,L)=1.0D08/(ENG(J,L)-ENG(I,L))
               ELSE
                  WL(I,J,L)=0.0
               ENDIF
47          CONTINUE
46       CONTINUE
44    CONTINUE
C--------------------------------------------------------------------
C   IDENTIFY THE TRANSITIONS WITHIN THE WAVELENGTH RANGES.
C--------------------------------------------------------------------
        NTTRANS=0
      DO 48 L=1,NIONS
         DO 57 J=2,NLVLS(L)
            DO 58 I=1,J-1
               IFLAG(I,J,L)=0
58          CONTINUE
57       CONTINUE
         IF(ISEL(L).EQ.1)THEN
            DO 49 N=1,NTRANS(L)
               DO 50 J=2,NLVLS(L)
                  DO 51 I=1,J-1
                     IF(I.EQ.I1(N,L).AND.J.EQ.J1(N,L)) THEN
                         IFLAG(I,J,L)=1
                     ENDIF
51                CONTINUE
50             CONTINUE
49          CONTINUE
         ELSEIF(ISEL(L).EQ.2)THEN
            DO 52 N=1,NTRANS(L)
               DO 53 J=2,NLVLS(L)
                  DO 54 I=1,J-1
                     IF(WL(I,J,L).GE.WLMIN(N,L).AND.
     &               WL(I,J,L).LE.WLMAX(N,L))THEN
                         IFLAG(I,J,L)=1
                     ENDIF
54                CONTINUE
53             CONTINUE
52          CONTINUE
         ELSEIF(ISEL(L).EQ.3)THEN
            DO 55 J=2,IMAXJ(L)
               DO 56 I=1,J-1
                  IF(WL(I,J,L).GE.WLMIN(1,L).AND.
     &            WL(I,J,L).LE.WLMAX(1,L))THEN
                         IFLAG(I,J,L)=1
                         NTTRANS=NTTRANS+1
                     ENDIF
56             CONTINUE
55          CONTINUE
         ENDIF
48    CONTINUE
C---------------------------CCCCCCCCCCCCCC---------------------------
C EVALUATE THE G(T) FUNCTIONS FOR THE SELECTED TRANSITIONS
C--------------------------------------------------------------------
      DO 70 L=1,NIONS
         NRES(L)=0
         DO 71 J=2,NLVLS(L)
            DO 72 I=1,J-1
               IJ=10000*I+J
               DO 73 M=1,NINDA(L)
                  IF(IFLAG(I,J,L).EQ.1.AND.IJ.EQ.INDA(M,L))THEN
                     IFLAG(I,J,L)=2
                     NRES(L)=NRES(L)+1
                     MM=NRES(L)
                     II(MM,L)=I
                     JJ(MM,L)=J
                     APWL(MM,L)=WL(I,J,L)
                     GMAX(MM,L)=1.0D-30
                     DO 74 K=1,NTEMP
                        GOFT(MM,K,L)=COEF(M,K,L)*RIB(K,L)*RNH(K)/
     &                  EDENS(K)
C#
C# sep11-95 ACL modification
C#          store VALLIM instead of values less than VALLIM
C#
                       IF (GOFT(MM,K,L) .LT. VALLIM) GOFT(MM,K,L)=VALLIM
C# sep11-95 modification end
                        IF(GMAX(MM,L).LE.GOFT(MM,K,L))THEN
                           GMAX(MM,L)=GOFT(MM,K,L)
                           MAXT(MM,L)=K
                        ENDIF
74                   CONTINUE
                  ENDIF
73             CONTINUE
72          CONTINUE
71       CONTINUE
70    CONTINUE
C--------------------------------------------------------------------
C   CHECK THAT THE G(T) FUNCTIONS HAVE BEEN EVALUATED FOR ALL THE
C   SELECTED TRANSITIONS.
C--------------------------------------------------------------------
C      DO 75 L=1,NIONS
C         DO 76 J=2,NLVLS(L)
C            DO 77 I=1,J
C               IF(IFLAG(I,J,L).EQ.1)WRITE(6,1050)SEQ(L),I,J
C77          CONTINUE
C76       CONTINUE
C75    CONTINUE
C--------------------------------------------------------------------
C   DETERMINE THE TEMPERATURE RANGES OVER WHICH THE G(T) FUNCTIONS
C   EXCEED THE LIMITING VALUE SET AS 'VALLIM' IN THE PARAMETER LIST.
C--------------------------------------------------------------------
      kmi = 1
      kma = 1

      DO 78 L=1,NIONS
         DO 79 M=1,NINDA(L)
            DO 80 K=1,NTEMP
               IF(GOFT(M,K,L).LE.VALLIM.AND.K.LE.MAXT(M,L))KMI=K
               IF(GOFT(M,K,L).GE.VALLIM.AND.K.GE.MAXT(M,L))KMA=K
               IF(M.EQ.1)KMIN(L)=KMI
               IF(M.EQ.1)KMAX(L)=KMA
80          CONTINUE
C# mar24-95 Alessandro Lanzafame
C#          if kmi=0 condition added
C#
            if (KMI.ne.0) then
               IF(KMI.LT.KMIN(L))KMIN(L)=KMI
            else
               KMIN(L)=1
            endif
            IF(KMA.GT.KMAX(L))KMAX(L)=KMA
79       CONTINUE
78    CONTINUE
C--------------------------------------------------------------------
C ENTER SPECTROSCOPIC WAVELENGTHS.
C--------------------------------------------------------------------
C      WRITE(6,1051)
C      READ(5,1052)ANS          23/5/97
C       ANS='N'
C      IF(ANS.EQ.'Y')THEN
CC      DO 90 L=1,NIONS
C         DO 91 M=1,NRES(L)
C            WRITE(6,1053)SEQ(L),SPEC(L),II(M,L),JJ(M,L),APWL(M,L)
C            WRITE(6,1073)II(M,L),SPLVL(II(M,L),L)(1:33)
C            WRITE(6,1073)JJ(M,L),SPLVL(JJ(M,L),L)(1:33)
C            READ(5,1055)SWL(M,L)
C91       CONTINUE
C90    CONTINUE
C      ENDIF
C--------------------------------------------------------------------
C       Signal to tell IDL data collection complete
C--------------------------------------------------------------------

        WRITE(PIPEOU,*)1
      CALL XXFLSH(PIPEOU)

4       CONTINUE

        READ(PIPEIN,*)LPEND

        IF (LPEND.EQ.0) THEN
                READ(PIPEIN,*)GRAPHFLAG
                READ(PIPEIN,*)TEXTFLAG
                IF (TEXTFLAG.EQ.1) THEN
                        READ(PIPEIN,'(A)')GOFTFILE
                ENDIF
        ENDIF
        READ(PIPEIN,*)MENU

        IF (MENU.EQ.1) GOTO 9999
        IF (LPEND.EQ.1) THEN
                GOTO 2
                LPEND=0
        ENDIF

        IF (GRAPHFLAG.EQ.1) THEN
           IF (ISEL(1).EQ.1)  NTTRANS=NTRANS(1)
           CALL DCOUTG(NTE,NTRA,NLVL,NION,NTEMP,NTTRANS,ISEL,II,JJ,GOFT)
           READ(PIPEIN,*)MENU
           IF (MENU.EQ.1) GOTO 9999
        ENDIF

        IF (TEXTFLAG.EQ.0) GOTO 4

C--------------------------------------------------------------------
C PREPARE AND PRINT THE OUT-PUT FILE.
C--------------------------------------------------------------------
C   OPEN FILE
C--------------------------------------------------------------------
C# mar20-95 Alessandro Lanzafame
C#    CALL FILEINF(IRCODE,'TRK',5,'SECOND',10,'RECFM','FBA',
C#   &             'LRECL',133,'BLKSIZE',5320)
      OPEN(UNIT=10,FILE=GOFTFILE,STATUS='UNKNOWN')
C# mar20_95 end
C--------------------------------------------------------------------
C   WRITE THE HEADER.
C--------------------------------------------------------------------
      DO 100 L=1,NIONS
         WRITE(10,1056)SYMBL(IZ0),IZ(L),NLVLS(L),
     &                 KMAX(L)-KMIN(L)+1,NRES(L)
         WRITE(10,1057)
 1056 FORMAT(1A2,'+',I2,2X,'/NLEVELS=',I4,'/NKNOTS=',I4,'/NLINES=',
     &       I4,'/')
 1057 FORMAT(/,2X,'SOURCE FILES:',/,2X,13('-'))
 1058 FORMAT(2X,'NH/NE DATA',16X,'- ',1A43)
 1077 FORMAT(2X,'PLASMA MODEL',14X,'- ',1A44)
         CALL XXSLEN(SYMBL(IZ0),IFIRST,ILAST)
         LSTR=LENSTR(ROOTPATH2)
         DSNINI=' IONISATION BALANCE DATA   - '//ROOTPATH2(1:LSTR)//
     &    '.<>cd'//FILEIB//'#'//SYMBL(IZ0)(IFIRST:ILAST)//'.dat'
         WRITE(10,*)DSNINI
         WRITE(10,1060)FILEEX(L)
 1060 FORMAT(2X,'SPECIFIC ION FILE DATA',4X,'- ',1A60,/)
C# mar21-95 Alessandro Lanzafame
         CALL GETENV('USER',USERID)
C# mar21-95 end
         WRITE(10,1061)DATEC,user
 1061 FORMAT(/,2X,'PROCESSING CODE',21X,'DATE',11X,' USER IDENTIFIER',
     &       /,2X,30('-'),6X,9('-'),7X,15('-'),
     &       /,2X,'ADAS 412        ',20X,1A9,7X,1A,
     &       //,2X,'ENERGY LEVEL INDEXING',/,2X,21('-'),
     &       /,2X,'INDX',5X,'CODE',15X,'S L  IJ',
     &       /,2X,4('-'),5X,15('-'),4X,'- -  --')
C--------------------------------------------------------------------
C   TABLE RELATING THE ENERGY LEVELS WITH THEIR INDICES.
C--------------------------------------------------------------------
         DO 101 I=1,NLVLS(L)
            WRITE(10,1063)I,SPLVL(I,L)(6:33)
 1063 FORMAT(I5,5X,1A28)
101      CONTINUE
C--------------------------------------------------------------------
C   TABLE OF PLASMA MODEL
C--------------------------------------------------------------------
         IF (IDDEP.EQ.0) THEN
            BTXT='NO '
         ELSE
            BTXT='YES'
         endif
            WRITE(10,1081)BTXT
            DO 104 KK=KMIN(L),KMAX(L)
               DDEN=DLOG10(EDENS(KK))
               DPEN=DLOG10(PE(KK))
               TTEN=10.0**TE(KK)
               WRITE(10,1082)TTEN,EDENS(KK),PE(KK),RNH(KK),
     &                   TE(KK),DDEN,DPEN
 104        CONTINUE
cccccccc ENDIF ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
 1081 FORMAT(/,2X,'PLASMA MODEL   (FINITE DENSITY IONISATION BALANCE =',
     &         1A3,')',/,2X,12('-'),
     &       /,2X,'TE(K)',6X,'NE(CM-3)',3X,'P(KCM-3)',3X,'NH/NE',
     &        6X,'TIME(S)',3X,'LOG(TE)',3X,'LOG(NE)',3X,'LOG(P)',
     &       /,2X,8('-'),3X,8('-'),3X,8('-'),3X,8('-'),3X,7('-'),
     &         3X,7('-'),3X,7('-'),3X,7('-'))
 1082 FORMAT(2X,1PD8.2,3(3X,1PD8.2),3X,8X,3(3X,0PF5.2,2X))
C--------------------------------------------------------------------
C   TABLE OF G(T) VALUES
C--------------------------------------------------------------------
         WRITE(10,1064)SPEC(L)
         DO 102 N=1,NRES(L),10
            N1=N
            N2=N+9
            write(10,2001)(m,m=n1,n2)
            WRITE(10,1065)(APWL(M,L),M=N1,N2)
            WRITE(10,1066)(SWL(M,L),M=N1,N2)
            WRITE(10,1067)(II(M,L),JJ(M,L),M=N1,N2)
            WRITE(10,1068)
            DO 103 KK=KMIN(L),KMAX(L)
              WRITE(10,1069)TE(KK),(GOFT(M,KK,L),
     &         M=N1,N2)
103         CONTINUE
            WRITE(10,1083)
102      CONTINUE
         WRITE(10,1084)
         WRITE(10,1085)SPEC(L)
         DO 110 N=1,NRES(L)
           WRITE(10,1086)N,II(N,L),JJ(N,L),APWL(N,L),SWL(N,L),
     &           SPLVL(II(N,L),L)(25:34),SPLVL(JJ(N,L),L)(25:34)
  110    CONTINUE

         WRITE(10,2014)'ADAS412', USER, DATEC

 1085 FORMAT('C',/,'C  TRANSITION LIST FOR ',1A11,/,'C',33('-'),
     &           /,'C',2X,'IND',3X,'TRANSITION', 4X,'APP. WVL.(A)',
     &                 4X,'EXACT WVL.(A)',
     &                 5X,'LOWER ST. ' , 4X,'UPPER ST.',
     &           /,'C',2X,3('-'),3X,10('-'),4X,12('-'),4X,13('-'),
     &                 5X,10('-'),4X,10('-'))
 1086 FORMAT('C ',I3,4X,I3,' - ',I3, 7X,F7.1,10X,F7.1,7X,1A10,4X,1A10)
 1087 FORMAT('C',/,'C',/,'C',60X,1A20,5X,1A9,/,'C')
100   CONTINUE

        CLOSE(10)
        GOTO 4

 9999  continue
        WRITE(PIPEOU,*)1
        CALL XXFLSH(PIPEOU)
C--------------------------------------------------------------------
C FORMATS FOR THE SECTIONS IDENTIFYING TRANSITIONS
C--------------------------------------------------------------------
C 1030 FORMAT(/,'SELECT TRANSITIONS FOR THE ',1A8,' ION.'/'THIS',
C     &' MAY BE DONE IN ONE OF THREE WAYS AS FOLLOWS:'//' 1. BY ENTERING',
C     &' PAIRS OF INDICES FROM THE FOLLOWING TABLE OR'/' 2. BY ENTERING '
C     &,'SPECIFIC WAVELENGTHS FOR THE REQUIRED TRANSITIONS OR'/' 3. BY'
C     &,'  ENTERING A RANGE OF WAVELENGTHS COVERING ALL THE REQUIRED'
C     &,' TRANSITIONS.'//' SELECT ONE OF THESE BY ENTERING 1, 2, OR 3.')
C 1031 FORMAT(1I1)
C 1032 FORMAT(1X,1A52)
C 1033 FORMAT(1H ,'ENTER REQUIRED NUMBER OF TRANSITIONS FOR THIS ION.')
C 1035 FORMAT(1H ,'ENTER INDICES OF LOWER THEN UPPER LEVELS OF',
C     &'THE TRANSITION SEPARATED BY COMMAS.')
C 1037 FORMAT(1H ,'ENTER THE NUMBER OF TRANSITIONS REQUIRED FOR THIS',
C     &' ION.')
C 1039 FORMAT(1H ,'ENTER WAVELENGTH IN ANGSTROM UNITS OF TRANSITION'/
C     &'(MUST BE WITHIN 10% OF VALUE GIVEN BY THE DIFFERENCE BETWEEN'/
C     &'ENERGIES IN THE TABLE ABOVE.)')
C 1041 FORMAT(/,1H ,'ENTER THE INDEX OF THE HIGHEST LEVEL'
C     &     ' TO BE INCLUDED')
C 1043 FORMAT(/,1H ,'ENTER MIN AND MAX OF WAVELENGTH RANGE ',
C     & 'OF TRANSITIONS TO BE INCLUDED'/' - ANGSTROM UNITS.')
C--------------------------------------------------------------------
C FORMATS FOR THE OUTPUT SECTIONS.
C--------------------------------------------------------------------
 1050 FORMAT(1H ,'THE G(T) FUNCTION FOR THE ',1A7,' ',1I2,'-',1I2,
     &' TRANSITION HAS NOT BEEN EVALUATED')
 1051 FORMAT(/,1H ,'DO YOU WISH TO ENTER THE SPECTROSCOPIC',
     & ' WAVELENGTHS -ANSWER Y/N')
 1052 FORMAT(1A1)
 1053 FORMAT(1H ,'ENTER THE SPECTROSCOPIC WAVELENGTH OF THE FOLLOWING',
     &' TRANSITION '/1X,1A7,1X,1A11,1X,1I2,'-',1I2,' APPROX WLENGTH: ',
     &F10.3,' ANGSTROMS')
 1054 FORMAT(1H ,1A32)
 1055 FORMAT(F10.3)
 1064 FORMAT(/,2X,'TABLE OF G(T) VALUES (CM3 S-1) FOR ',1A11,
     &       /,2X,46('-'))
 2001 format(1x,'TRANS INDEX    ',10(4X,I3,4X))
 1065 FORMAT(1X,'APPROX WLENGTH ',10(1X,F7.1,3X))
 1066 FORMAT(1X,'SPECTR WLENGTH ',10(1X,F9.3,1X))
 1067 FORMAT(1X,'TRANSITION',5X,10(3X,1I2,'-',1I2,3X))
 1068 FORMAT(1X,'LOG(TE(K))',/,1X,10('-'))
 1069 FORMAT(1H ,5X,F4.2,6X,10(1PD10.3,1X))
 1070 FORMAT(6I10)
 1071 FORMAT(/,1H ,' VALUES HAVE BEEN CALCULATED FOR ',1I2,
     &' TRANSITIONS IN ',1A11,/)
 1072 FORMAT(1H1)
 1073 FORMAT(1H ,1I2,1A33)
 1074 FORMAT(1H ,' UNIFORM ELECTRON DENSITY =',1P,D8.2,' CM-3.')
 1075 FORMAT(1H ,' UNIFORM ELECTRON PRESSURE =',1P,D8.2,' K.CM-3.')
 1076 FORMAT(1H ,4X,' PLASMA MODEL.')
 1078 FORMAT(1H ,2X,1I2,1X,F4.2,1X,F8.2,1X,10(1P,D10.3,1X))
 1079 FORMAT(1H ,' DO YOU WANT TO INCLUDE THE EFFECT OF DENSITY',
     &' ON THE IONIZATION BALANCE. ANSWER Y/N.')
 1080 FORMAT(1H ,' YOU HAVE CALLED FILENAME ',1A60,' FOR THE'/
     &' ELEMENT ',1A2,' IS THIS OK? ANSWER Y/N.')
 1083 FORMAT(1X)
 1084 FORMAT('C', 124('-'))

 2014   format('C',/,
     &         'C  CODE     : ',1a7/
     &         'C  PRODUCER : ',a30/
     &         'C  DATE     : ',1a8,/,'C',/,'C',124('-'))

      END

