       program adas416

       implicit none
c-----------------------------------------------------------------------
c
c  ******************* fortran77 program: adas416 **********************
c
c  purpose:   To implement a partition of a set of isonuclear master
c             files.
c
c             Data may be read from standard or partial isonuclear
c             master files
c             e.g.  /../adf11/acd89#c.dat or /../adf11/acd96r#be.dat
c
c             Charge exchange is incorporated if available
c
c             Twelve classes of data may be opened for input with the
c             acd and scd classes obligatory.
c
c               iclass    type      indi     indj
c               ------    ----      ----     ----
c                 1       acd       iprt     igrd
c                 2       scd       iprt     igrd
c                 3       ccd       iprt     igrd
c                 4       prb       iprt
c                 5       prc       iprt
c                 6       qcd       igrd     jgrd
c                 7       xcd       iprt     jprt
c                 8       plt       igrd
c                 9       pls       igrd
c                10       zcd       igrd
c                11       ycd       igrd
c                12       ecd       igrd
c
c
c
c  program:
c
c          (i*4) iunt416 = parameter = unit for script data
c          (i*4) iunt13  = parameter = unit for output gcr data
c          (i*4) itdimd  = parameter = max. number of temperature values
c                                      in isonuclear master files
c          (i*4) iddimd  = parameter = max. number of density values
c                                      in isonuclear master files
c          (i*4) izdimd  = parameter = max. number of charge states in
c                                      isonuclear master files
c          (i*4) ifdimd  = parameter = no. of pec/gtn file types
c                                      presented in script file for
c                                      parent and daughter partition
c                                      (normally 4:pec,gtn,f-pec,f-gtn)
c          (i*4) ipdimd  = parameter = max. number of metastables for
c                                      each ionisation stage
c          (i*4) isdimd  = parameter = max. number of (charge, parent,
c                                      ground) blocks in isonuclear
c                                      master files
c          (i*4) imdimd  = parameter = max. number of populations
c                                      (=isdimd+1)
c          (i*4) idptnl  = parameter = maximum level of partitions
c          (i*4) idptn   = parameter = maximum no. of partitions in
c                                      one level
c          (i*4) idptnc  = parameter = maximum no. of components in
c                                      a partition
c          (i*4) i          = general index
c          (i*4) iz0        = element nuclear charge
c          (i*4) nclass     = number of master file classes =12
c          (i*4) npsum      = total number of populations summed over
c                             partitions of every superstage
c          (r*8) fabun_p(,,) = resolved metastable equilibrium
c                             fractional abundances
c                             1st dim: - temperature index
c                             2nd dim: - density index
c                             3rd dim: - metastable index
c          (c*6) date        = current date
c
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          dgscrp     adas      fetches specum line info. from script
c          dgprt_11   adas      partition set of adf11 datasets
c          dgprt_em   adas      partition set of emissivity (adf13/adf40 etc)
c                               datasets
c          dgwgcr     adas      output partitioned gcr daatsets
c          xx0000     adas      set machine dependant adas configuration
c          xxdate     adas      provides current date
c          xxname     adas      fetches real name of user
c          xxopen     adas      open named dataset
c          xxflsh     adas      flushes unix pipe with idl
c          i4unit     adas      fetch unit number for output of messages
c          xelz0      adas      returns nuclear charge from element name
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    04/10/06
c
c version: 1.1                          date: 04/10/2006
c modified: Hugh Summers
c               - first edition.
c
c version: 1.2                          date: 15-02-2007
c modified: Hugh Summers   - Various adjustments and corrections
c                            connected with scaling and zeroing.
c                            Added in ycd and ecd processing.
c
c version: 1.3                          date: 14/03/2007
c modified: Hugh Summers
c               - adjustments for revised ecd formats.
c                 charge exchange donor/donor mass checks and
c                 dnr_ele, dnr_ams added to parameter return.
c
C-----------------------------------------------------------------------
C
C  Author   : Hugh Summers
C  Date     : 14-03-2007
C
C
C  Version : 1.1
C  Date    : 23-07-2007
C  Modified: Martin O'Mullane
C            - Interactive version.
C
C  Version : 1.1
C  Date    : 11-10-2009
C  Modified: Martin O'Mullane
C            - Send final '1' back to IDL before stopping
C
C-----------------------------------------------------------------------
       integer   ndclas
       integer   iunt13  , iunt416
       integer   itdimd  , iddimd  , izdimd ,
     &           ifdimd  , isdimd  , imdimd
       integer   idptnl  , idptn   , idptnc , idcnct
       integer   idstack
       integer   pipein  , pipeou
c-----------------------------------------------------------------------
       parameter ( ndclas = 12 )
       parameter ( iunt13 = 13 , iunt416 = 46)
       parameter ( itdimd = 50 , iddimd = 40 , izdimd = 92 )
       parameter ( ifdimd =  4 , isdimd = 200 )
       parameter ( imdimd = isdimd+1 )
       parameter ( idptnl = 4  , idptn = 128     , idptnc = 256 )
       parameter ( idcnct = 100 , idstack = 40 )
       parameter ( pipein =   5 , pipeou = 6 )
c-----------------------------------------------------------------------
       integer   ilogic  , i4unit   , nclass
       integer   iz0
       integer   i       , it       , id     , iclass
       integer   npsum
       integer   iplp    , ipld
       integer   nptnl   , ncptn_stack
       integer   ncnct_p ,  ncnct_c
       integer   iz0_p
       integer   iz0_c   , is1min_c , is1max_c
       integer   idmax_p , itmax_p
       integer   ircode
       integer   npsum_p , npsum_c
       integer   nsubs
c-----------------------------------------------------------------------
       real*8    fscal
c-----------------------------------------------------------------------
       character rep*8        , date*8
       character dsnscr*120   , dsngcr_p*120 , dsngcr_c*120 ,
     &           elem*12      , dsntmp*120
       character year*2
       character name*30
c-----------------------------------------------------------------------
       logical   lpart  , lexist  , ltick
       logical   lres   , lres_p  , lres_c   , lptn_c
c-----------------------------------------------------------------------
       integer   issa_min(idcnct)      , issa_max(idcnct)
       integer   nptn(idptnl)          , nptnc(idptnl,idptn)    ,
     &           iptnla(idptnl)        , iptna(idptnl,idptn)    ,
     &           iptnca(idptnl,idptn,idptnc)
       integer   icnctv_p(idcnct)      , icnctv_c(idcnct)
       integer   iblmxa_p(ndclas)
       integer   ispbra_p(ndclas,isdimd)  , isppra_p(ndclas,isdimd)
       integer   iblmxa_c(ndclas)
       integer   ispbra_c(ndclas,isdimd)  , isppra_c(ndclas,isdimd) ,
     &           isstgra_c(ndclas,isdimd)
       integer   jfabun_p(itdimd,iddimd,izdimd)                     ,
     &           jfabun_c(itdimd,iddimd,izdimd)
c-----------------------------------------------------------------------
       real*8    fabun_p(itdimd,iddimd,imdimd)  ,
     &           fabun_c(itdimd,iddimd,imdimd)
       real*8    dtev_p(itdimd), ddens_p(iddimd)
       real*8    dgcra_p(ndclas,isdimd,itdimd,iddimd)
       real*8    dgcra_c(ndclas,isdimd,itdimd,iddimd)
       real*8    dnr_amsa_p(ndclas)
c-----------------------------------------------------------------------
       character dsna_p(ifdimd)*120   , dsna_c(ifdimd)*120
       character cptn_stack(idstack)*80
       character dnr_elea_p(ndclas)*12
       character cclass(ndclas)*3
c-----------------------------------------------------------------------
       logical   lgcra_p(ndclas)   , lgcra_c(ndclas)
       logical   lfilea_p(ifdimd)  , lfilea_c(ifdimd)
c------------------------------------------------------------------------
       data nclass/   12/
       data cclass/'acd' ,'scd' ,'ccd' ,'prb' ,'prc' ,
     &             'qcd' ,'xcd' ,'plt' ,'pls' ,'zcd',
     &             'ycd' ,'ecd' /
c-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C Set machine dependant ADAS configuration values and get date from IDL.
C-----------------------------------------------------------------------

       call xx0000
       call xxdate(date)
       call xxname(name)

       read(pipein,*)ilogic
       if (ilogic.EQ.1) then
          lpart = .TRUE.
       else
          lpart = .FALSE.
       endif

       read(pipein,*)ilogic
       if (ilogic.EQ.1) then
          ltick = .TRUE.
       else
          ltick = .FALSE.
       endif

C-----------------------------------------------------------------------
C initialise
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C Screen 1 of ADAS416 - input 416 script file.
C-----------------------------------------------------------------------

 100   continue

       read(pipein, '(A)')rep
       if (rep(1:6).EQ.'CANCEL') goto 999
       read(pipein, '(A)')dsnscr
       call xxopen( iunt416 , dsnscr , lexist )

       call dgscrp( ifdimd   , idstack , iunt416  ,
     &              dsngcr_p , dsna_p  , lfilea_p  , iplp    ,
     &              dsngcr_c , dsna_c  , lfilea_c  , ipld    ,
     &              idptnl   , idptn   , idptnc    ,
     &              nptnl    , nptn    , nptnc     ,
     &              iptnla   , iptna   , iptnca    ,
     &              ncptn_stack        , cptn_stack,
     &              lres     ,  elem   , year      , ircode
     &            )

       close(iunt416)

       if (ircode.EQ.1) then
          write(i4unit(-1),2001)
          write(i4unit(-1),2010)
          stop
       endif

       call xxelz0(elem, iz0)

C-----------------------------------------------------------------------
C Screen 2 (progress bar)
C   - partition adf11 GCR data.
C   - send ionisation balance to IDL for display before continuing.
C-----------------------------------------------------------------------

 200   continue

       call dgprt_11( ltick       , lpart      ,
     &                dsngcr_p    , iz0        ,
     &                nptnl       , nptn       , nptnc   ,
     &                iptnla      , icnctv_c   ,
     &                iblmxa_c    , ispbra_c   ,
     &                isppra_c    , isstgra_c  ,
     &                ispbra_p    , isppra_p   ,
     &                iz0_p       , lres_p     ,
     &                idmax_p     , itmax_p    ,
     &                ddens_p     , dtev_p     ,
     &                dnr_elea_p  , dnr_amsa_p ,
     &                dgcra_c     , lgcra_c    ,
     &                npsum_p     , ncnct_p    , icnctv_p ,
     &                fabun_p     , jfabun_p   ,
     &                npsum_c     , ncnct_c    ,
     &                fabun_c     , jfabun_c   ,
     &                iblmxa_p    , dgcra_p    , lgcra_p ,
     &                issa_min    , issa_max   , npsum   ,
     &                is1min_c    , is1max_c
     &              )

       write(pipeou,*)itmax_p
       call xxflsh(pipeou)
       write(pipeou,*)idmax_p
       call xxflsh(pipeou)
       write(pipeou,*)npsum_p
       call xxflsh(pipeou)
       write(pipeou,*)npsum_c
       call xxflsh(pipeou)

       do it = 1, itmax_p
         write(pipeou,*)10.0D0**dtev_p(it)
         call xxflsh(pipeou)
       end do
       do id = 1, idmax_p
         write(pipeou,*)10.0D0**ddens_p(id)
         call xxflsh(pipeou)
       end do

       do it = 1, itmax_p
         do id = 1, idmax_p
            do i = 1, npsum_p
               fscal = 10.0D0**jfabun_p(it,id,i)
               write(pipeou,*)fscal * fabun_p(it,id,i)
               call xxflsh(pipeou)
            enddo
            do i = 1, npsum_c
               fscal = 10.0D0**jfabun_c(it,id,i)
               write(pipeou,*)fscal * fabun_c(it,id,i)
               call xxflsh(pipeou)
            enddo
         end do
       end do

        write(pipeou,*)ncptn_stack
        call xxflsh(pipeou)
        do i = 1, ncptn_stack
           write(pipeou,*)cptn_stack(i)
           call xxflsh(pipeou)
        end do

       read(pipein, '(A)')rep
       if (rep(1:6).EQ.'CANCEL') goto 100
       if (rep(1:4).EQ.'MENU') goto 999


C-----------------------------------------------------------------------
C Screen 3 of ADAS416
C   - display parent and child partitioned ionisation balance.
C-----------------------------------------------------------------------

 300   continue


C-----------------------------------------------------------------------
C Screen 4 of ADAS416 (progress bar)
C   - calculate the partitioned emissivities.
C   - De-activate this until properly tested
C-----------------------------------------------------------------------

 400   continue

C        call dgprt_em( ltick   ,
C      &               iblmxa_p , dgcra_p  , lgcra_p ,
C      &               dsna_p   , lfilea_p ,
C      &               idmax_p  , itmax_p  ,
C      &               ddens_p  , dtev_p   ,
C      &               ncnct_p  , icnctv_p ,
C      &               ispbra_p , isppra_p , isstgra_p,
C      &               iblmxa_c ,
C      &               issa_min , issa_max , npsum  ,
C      &               is1min_c , is1max_c ,
C      &               isppra_c , isstgra_c  ,
C      &               dgcra_c
C      &              )


C-----------------------------------------------------------------------
C Screen 4 of ADAS416 (confirm output files)
C   - write the partitioned datasets.
C-----------------------------------------------------------------------

C send adf11 names to IDL

      write(pipeou,*)ndclas
      call xxflsh(pipeou)
      do iclass = 1, nclass
        if(lgcra_c(iclass)) then
           call xxrepl(dsngcr_c, '***', cclass(iclass), dsntmp, nsubs)
           write(pipeou,*)dsntmp
           call xxflsh(pipeou)
        else
           write(pipeou,*)'NULL'
           call xxflsh(pipeou)
        endif
      end do

C and adf15/adf40 etc. names to IDL


C Check if happy to write them

       read(pipein, '(A)')rep
       if (rep(1:6).EQ.'CANCEL') goto 100
       if (rep(1:4).EQ.'MENU') goto 999


C write adf11 if ok

      lres_c = lres_p
      lptn_c = .true.
      iz0_c  = iz0_p
      is1min_c=1
      is1max_c=ncnct_c-1

      call dgwgcr( iunt13      , date       , name     ,
     &             dsngcr_p    , dsngcr_c   ,
     &             ndclas      , idstack    ,
     &             isdimd      , iddimd     , itdimd   ,
     &             idcnct      ,
     &             iz0_c       , is1min_c   , is1max_c ,
     &             ncnct_c     , icnctv_c   ,
     &             ncptn_stack , cptn_stack ,
     &             iblmxa_c    , ispbra_c   , isppra_c , isstgra_c ,
     &             idmax_p     , itmax_p    ,
     &             ddens_p     , dtev_p     ,
     &             dnr_elea_p  , dnr_amsa_p ,
     &             dgcra_c     , lgcra_c    , lres_c   , lptn_c
     &           )


C write emissivity set - later


C-----------------------------------------------------------------------
C Return to the start.
C-----------------------------------------------------------------------

      goto 100

C----------------------------------------------------------------------
 999  continue
      write(pipeou,*) 1

C-----------------------------------------------------------------------

C-----------------------------------------------------------------------

 2001 format(1x,32('*'),' adas416 error ',32('*')//
     &       1x,'i/o error reading from script file ')
 2010 format(/1x,28('*'),'  program terminated   ',27('*'))

C-----------------------------------------------------------------------
       end
