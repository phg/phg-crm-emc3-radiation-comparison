       subroutine dgprt_11( ltick       , lpart      ,
     &                      dsngcr_p    , iz0        ,
     &                      nptnl       , nptn       , nptnc   ,
     &                      iptnla      , icnctv_c   ,
     &                      iblmxa_c    , ispbra_c   ,
     &                      isppra_c    , isstgra_c  ,
     &                      ispbra_p    , isppra_p   ,
     &                      iz0_p       , lres_p     ,
     &                      idmax_p     , itmax_p    ,
     &                      ddens_p     , dtev_p     ,
     &                      dnr_elea_p  , dnr_amsa_p ,
     &                      dgcra_c     , lgcra_c    ,
     &                      npsum_p     , ncnct_p    , icnctv_p ,
     &                      fabun_p     , jfabun_p   ,
     &                      npsum_c     , ncnct_c    ,
     &                      fabun_c     , jfabun_c   ,
     &                      iblmxa_p    , dgcra_p    , lgcra_p ,
     &                      issa_min    , issa_max   , npsum   ,
     &                      is1min_c    , is1max_c
     &                    )

       implicit none
C-----------------------------------------------------------------------
C
C  ******************* fortran77 program: dgprt_11 *********************
C
C  purpose:   Partition an adf11 set of datasets.
C
C               iclass    type      indi     indj
C               ------    ----      ----     ----
C                 1       acd       iprt     igrd
C                 2       scd       iprt     igrd
C                 3       ccd       iprt     igrd
C                 4       prb       iprt
C                 5       prc       iprt
C                 6       qcd       igrd     jgrd
C                 7       xcd       iprt     jprt
C                 8       plt       igrd
C                 9       pls       igrd
C                10       zcd       igrd
C                11       ycd       igrd
C                12       ecd       igrd
C
C
C
C
C routines:
C          routine    source    brief description
C          ------------------------------------------------------------
C          dxdata_11  adas      fetches data from a set of master files
C          dgcnmk     adas      make a child connection vector
C          dgispf     adas      fetches user data from ispf panels
C          dgmpop_sc  adas      partitioned tri-diag matrix inversion
C          dgspow     adas      computes radiated power functions
C          dgunpk     adas      assemble child gcr tables for output
C          dgwr11     adas      output gcf passing file
C          i4unit     adas      fetch unit number for output of messages
C          xfelem     adas      fetches element name
C          xfesym     adas      fetches element chemical symbol
C          xxdata_11  adas      fetches data from a selected master file
C          xxuid      adas      sets a user identifier
C          xxflsh     idl_adas  flushes unix pipe with idl
C
C
C
C  Author   : Hugh Summers
C  Date     : 14-03-2007
C
C
C  Version : 1.1
C  Date    : 23-07-2007
C  Modified: Martin O'Mullane
C            - Interactive version.
C
C  Version : 1.2
C  Date    : 03-06-2009
C  Modified: Martin O'Mullane
C            - Initialise variables before passing to subroutines.
C
C  Version : 1.3
C  Date    : 09-04-2010
C  Modified: Martin O'Mullane
C            - Redefine iexp_min to be 2 less than -2**31 to fix a
C              gfortran overflow problem at compilation.
C
C  Version : 1.4
C  Date    : 23-10-2014
C  Modified: Martin O'Mullane
C            - Check for numeric overflow before row and column compress
C              of crmat and crccd.
C
C-----------------------------------------------------------------------
       integer   ndclas
       integer   iunt11
       integer   itdimd  , iddimd  , izdimd ,
     &           ipdimd  , isdimd , imdimd
       integer   idptnl  , idptn   , idptnc , idcnct
       integer   pipeou
       integer   iexp_min, one
C-----------------------------------------------------------------------
       real*8    dzero
C-----------------------------------------------------------------------
       parameter ( ndclas = 12 )
       parameter ( iunt11 = 11 )
       parameter ( itdimd = 50 , iddimd = 40 , izdimd = 92 )
       parameter ( ipdimd = 10 , isdimd = 200 )
       parameter ( imdimd = isdimd+1 )
       parameter ( idptnl = 4   , idptn = 128, idptnc = 256 )
       parameter ( idcnct = 100)
       parameter ( iexp_min=-2*(2**30-1) , one = 1)
       parameter ( pipeou=6 )
       parameter ( dzero=1.0d-74)
C-----------------------------------------------------------------------
       integer   i4unit  , iz0
       integer   is      , is1      , ip
       integer   ic      , ir
       integer   i       , it       , id     ,iclass , ix
       integer   j       , jmax     , jr     , js1   , jsum   , jsumc  ,
     &           jsprb   , jsprc    , jsplt  , jspls
       integer   npsum    , ip_max
       integer   nptnl
       integer   nptnl_p , iptnl_p  , ncnct_p   , iptnl_c   , ncnct_c
       integer   iz0_p   , is1min_p , is1max_p
       integer   is1min_c , is1max_c
       integer   iblmx_p , ismax_p  , idmax_p   , itmax_p
       integer   npsum_p , npsum_c
       integer   ispb    , nsubs
C-----------------------------------------------------------------------
       real*8    sum        , sccd        , sprb        , sprc      ,
     &           splt       , spls        , szcd        , sycd
       real*8    popf_max
       real*8    dnr_ams
C-----------------------------------------------------------------------
       character dsnacd*120 , dsngcr_p*120 , dsntmp*120
       character elemt*2 , xfesym*2 , chs*2, chp*2
       character dnr_ele*12
C-----------------------------------------------------------------------
       logical lexist , ltick    , lpart
       logical lres_p , lstan_p  , lptn_p
C-----------------------------------------------------------------------
       integer   isip(imdimd)   , ipip(imdimd)   , ipisp(izdimd,ipdimd),
     &           isip_c(imdimd) , ipip_c(imdimd) ,
     &           ipisp_c(izdimd,ipdimd)
       integer   issa_min(idcnct)      , issa_max(idcnct)
       integer   mappc_s(izdimd,ipdimd), mappc_m(izdimd,ipdimd)
       integer   nptn(idptnl)          , nptnc(idptnl,idptn)    ,
     &           iptnla(idptnl)
       integer   nptn_p(idptnl)        , nptnc_p(idptnl,idptn)  ,
     &           iptnla_p(idptnl)      , iptna_p(idptnl,idptn)  ,
     &           iptnca_p(idptnl,idptn,idptnc)
       integer   icnctv_p(idcnct)      , icnctv_c(idcnct)       ,
     &           icnaccum_p(idcnct)
       integer   isppr_p(isdimd)       , ispbr_p(isdimd)   ,
     &           isstgr_p(isdimd)
       integer   iblmxa_p(ndclas)
       integer   ispbra_p(ndclas,isdimd)  , isppra_p(ndclas,isdimd) ,
     &           isstgra_p(ndclas,isdimd)
       integer   iblmxa_c(ndclas)
       integer   ispbra_c(ndclas,isdimd)  , isppra_c(ndclas,isdimd) ,
     &           isstgra_c(ndclas,isdimd)
       integer   jpopf(izdimd)            ,
     &           jcrmat(izdimd,izdimd)    , jcrccd(izdimd,izdimd)   ,
     &           jcrprb(izdimd)           , jcrprc(izdimd)          ,
     &           jcrplt(izdimd)           , jcrpls(izdimd)
       integer   jfabun_p(itdimd,iddimd,izdimd)                     ,
     &           jfabun_c(itdimd,iddimd,izdimd)
       integer   jcrmat_c(imdimd,imdimd)  , jcrccd_c(imdimd,imdimd)
       integer   jcrprb_c(imdimd)         , jcrprc_c(imdimd)        ,
     &           jcrplt_c(imdimd)         , jcrpls_c(imdimd)
       integer   jcrmat_t(imdimd,imdimd)  , jcrccd_t(imdimd,imdimd)
C-----------------------------------------------------------------------
       real*8    fabun_p(itdimd,iddimd,imdimd)  ,
     &           fabun_c(itdimd,iddimd,imdimd)
       real*8    dtev_p(itdimd), ddens_p(iddimd),
     &           drcof_p(isdimd,itdimd,iddimd)
       real*8    dgcra_p(ndclas,isdimd,itdimd,iddimd)
       real*8    dgcr_p(ndclas,isdimd)
       real*8    dgcra_c(ndclas,isdimd,itdimd,iddimd)
       real*8    dgcr_c(ndclas,isdimd)
       real*8    popf(imdimd)
       real*8    crmat(imdimd,imdimd)   , crccd(imdimd,imdimd)
       real*8    crprb(imdimd)          , crprc(imdimd)                ,
     &           crplt(imdimd)          , crpls(imdimd)                ,
     &           crzcd(imdimd)          , crycd(imdimd)                ,
     &           crecd(imdimd,imdimd)
       real*8    crmat_c(imdimd,imdimd) , crccd_c(imdimd,imdimd)
       real*8    crprb_c(imdimd)        , crprc_c(imdimd)              ,
     &           crplt_c(imdimd)        , crpls_c(imdimd)              ,
     &           crzcd_c(imdimd)        , crycd_c(imdimd)              ,
     &           crecd_c(imdimd,imdimd)
       real*8    dnr_amsa_p(ndclas)
C-----------------------------------------------------------------------
       character dsflla(ndclas)*120
       character poptit(imdimd )*16
       character chpop(imdimd)*20
       character poptit_c(imdimd )  , chpop_c(imdimd)*20
       character dnr_elea_p(ndclas)*12
       character cclass(ndclas)*3
C-----------------------------------------------------------------------
       logical lsela(ndclas)     , lexsa(ndclas)
       logical lgcra_p(ndclas)   , lgcra_c(ndclas)
C------------------------------------------------------------------------
       data cclass/'acd' ,'scd' ,'ccd' ,'prb' ,'prc' ,
     &             'qcd' ,'xcd' ,'plt' ,'pls' ,'zcd',
     &             'ycd' ,'ecd' /
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************

C-----------------------------------------------------------------------
C Extract partition, metastable resolution and charge range of
C parent partition level from acd file.
C-----------------------------------------------------------------------

       call xxrepl(dsngcr_p, '***', 'acd', dsnacd, nsubs)

       call xxopen( iunt11 , dsnacd , lexist)
       if (.not.lexist) then
          write(i4unit(-1),2002)dsnacd
          write(i4unit(-1),2010)
          stop
       endif

       iclass = 1
       call  xxdata_11( iunt11    , iclass   ,
     &                  isdimd    , iddimd   , itdimd ,
     &                  idptnl    , idptn    , idptnc , idcnct ,
     &                  iz0_p     , is1min_p , is1max_p ,
     &                  nptnl_p   , nptn_p   , nptnc_p  ,
     &                  iptnla_p  , iptna_p  , iptnca_p ,
     &                  ncnct_p   , icnctv_p ,
     &                  iblmx_p   , ismax_p  , dnr_ele, dnr_ams,
     &                  isppr_p   , ispbr_p  , isstgr_p ,
     &                  idmax_p   , itmax_p  ,
     &                  ddens_p   , dtev_p   , drcof_p ,
     &                  lres_p    , lstan_p  , lptn_p
     &                )

       close(iunt11)


C Check consistency between script file and parent master (acd) file

       if (iz0.ne.iz0_p) then
           write(i4unit(-1),2003)'iz0',iz0,'iz0_p',iz0_p
           write(i4unit(-1),2010)
           stop
       endif

       if(iptnla(2).ne.iptnla_p(1)) then
         write(i4unit(-1),2003)'iptnla(2)',iptnla(2),
     &                         'iptnla_p(1)',iptnla_p(1)
         write(i4unit(-1),2010)
         stop
       endif

       if(nptn(2).ne.nptn_p(1)) then
         write(i4unit(-1),2003)'nptn(2)',nptn(2),
     &                         'nptn_p(1)',nptn_p(1)
         write(i4unit(-1),2010)
         stop
       endif

C-----------------------------------------------------------------------
C  Define number of superstage blocks (stages in original parlance and
C  total number of partitions (metastables in original parlance)
C-----------------------------------------------------------------------

       elemt  = xfesym(iz0)

       npsum = 0
       do is1 = is1min_p, is1max_p + 1

         is = is1 - 1
         ispb = icnctv_p( is1-is1min_p + 1)
         if (is1-is1min_p+1.eq.1) then
             icnaccum_p(is1)=ispb
         else
             icnaccum_p(is1)=icnaccum_p(is1-1)+ispb
         endif
         do ip = 1,ispb
           npsum = npsum + 1
           write(chs , 1022) is
           write(chp , 1022) ip
           poptit(npsum) = elemt//'[ss:'//chs//'](pt:'//chp//')'
           write( chpop(npsum) , 1112) npsum , poptit(npsum)
           isip(npsum)   = is1
           ipip(npsum)   = ip
           ipisp(is1-is1min_p+1,ip) = npsum
         end do
       end do
       npsum_p = npsum

C-----------------------------------------------------------------------
C Fetch complete set of isonuclear master file data
C-----------------------------------------------------------------------

       do iclass = 1, ndclas
          dsntmp = ' '
          call xxrepl(dsngcr_p, '***', cclass(iclass), dsntmp, nsubs)
          inquire (file=dsntmp, exist=lexist)
          if (lexist) then
             lsela(iclass)  = .TRUE.
             lexsa(iclass)  = .TRUE.
             dsflla(iclass) = dsntmp
          else
             lsela(iclass)  = .FALSE.
             lexsa(iclass)  = .FALSE.
             dsflla(iclass) = 'NULL'
          endif
       end do


       call dxdata_11(  iunt11   , ndclas   ,
     &                  dsflla   , lsela    , lexsa    ,
     &                  isdimd   , iddimd   , itdimd   ,
     &                  idptnl   , idptn    , idptnc   , idcnct   ,
     &                  iz0_p    , is1min_p , is1max_p ,
     &                  nptnl_p  , nptn_p   , nptnc_p  ,
     &                  ncnct_p  , icnctv_p ,
     &                  iblmx_p  ,
     &                  idmax_p  , itmax_p  ,
     &                  ddens_p  , dtev_p   ,
     &                  drcof_p  ,
     &                  iblmxa_p , ispbra_p , isppra_p , isstgra_p,
     &                  dnr_elea_p          , dnr_amsa_p          ,
     &                  dgcra_p  , lgcra_p
     &             )


C-----------------------------------------------------------------------
C Create the connection vector for the child partition level. Make
C sure parent and child partition levels are identified correctly.
C-----------------------------------------------------------------------

       iptnl_c = iptnla(1)
       ic=1
       do i=1,nptnl
         if(iptnl_c.lt.iptnla(i)) then
             iptnl_c=iptnla(i)
             ic=i
         endif
       enddo
       iptnl_p = iptnl_c-1

       call  dgcnmk( elemt,
     &               imdimd  , izdimd  , ipdimd   ,
     &               idptnl  , idptn   , idcnct   ,
     &               nptnl   , nptn    , nptnc    ,
     &               iptnla  ,
     &               iptnl_p , ncnct_p , icnctv_p , icnaccum_p ,
     &               is1min_p, is1max_p, npsum_p  ,
     &               isip    , ipip    , ipisp    ,
     &               iptnl_c , ncnct_c , icnctv_c ,
     &               is1min_c, is1max_c, npsum_c  ,
     &               isip_c  , ipip_c  , ipisp_c  ,
     &               poptit_c          , chpop_c  ,
     &               mappc_s , mappc_m ,
     &               issa_min, issa_max
     &             )


C-----------------------------------------------------------------------
C  Evaluate equilibrium populations of partitions and/or superstages
C  at each parent temperature-density point.
C  Use reverse order in temperature to aid decision on which pairs of
C  child partition/superstage couplings  have zero coefficients and so
C  can be omitted from the adf11 tabulations.
C
C  Pass update tick to IDL to monitor progress.
C-----------------------------------------------------------------------

       if (ltick) then
          if (lpart) then
             write(pipeou,*)idmax_p*itmax_p*2
          else
             write(pipeou,*)idmax_p*itmax_p
          endif
          call xxflsh(pipeou)
       endif


       do 10 id=1,idmax_p

         do 20 it=itmax_p,1,-1

           if (ltick) then
              write(pipeou,*)one
              call xxflsh(pipeou)
           endif

           do iclass=1,ndclas
             do ip=1,isdimd
               dgcr_p(iclass,ip)= dgcra_p(iclass,ip,it,id)
             enddo
           enddo

           call dgmpop_sc( ndclas   , isdimd   , izdimd   , ipdimd ,
     &                     idcnct   ,
     &                     ncnct_p  , icnctv_p ,
     &                     iblmxa_p , ispbra_p , isppra_p , isstgra_p ,
     &                     dgcr_p   ,
     &                     npsum    , popf     , jpopf    ,
     &                     crmat    , jcrmat   , crccd    , jcrccd    ,
     &                     crprb    , jcrprb   , crprc    , jcrprc    ,
     &                     crplt    , jcrplt   , crpls    , jcrpls    ,
     &                     crzcd    , crycd    , crecd
     &                   )

           do i=1,npsum_p
              fabun_p(it,id,i) = popf(i)
           enddo

           do is1=1,ncnct_p
              jfabun_p(it,id,is1)= jpopf(is1)
           enddo

C-----------------------------------------------------------------------
C      Compress cr matrices and vectors to that of child partition at
C      each Te and Ne.  Note that ic marks the child partition level
C      index in nptnl.
C      First find where the largest population is and then normalise the
C      popf populations within each child  partition.  With underflow
C      force populations appropriately in comparison with the largest.
C-----------------------------------------------------------------------

           jmax=iexp_min
           do is1=1,ncnct_p
             jmax=max0(jmax,jpopf(is1))
           enddo

           ip_max =1
           popf_max=dzero
           do i=1,npsum
             if(popf(i).gt.dzero) then
                 ip_max=i
                 popf_max=popf(i)*10.0d0**(jpopf(isip(i))-jmax)
             endif
           enddo

           i=0
           do ir=1,nptn(ic)

             jsum=iexp_min
             do ip=1,nptnc(ic,ir)
               jsum=max0(jsum,jpopf(isip(i+ip)))
             enddo

             sum=0.0d0
             do ip=1,nptnc(ic,ir)
               sum=sum+popf(i+ip)*10.0d0**(jpopf(isip(i+ip))-jsum)
             enddo
             if(sum.le.dzero) then
                 do ip=1,nptnc(ic,ir)
                   popf(i+ip)=0.0d0
                 enddo
                   if((i+1).gt.ip_max) then
                       popf(i+1)=1.0d0
                   elseif ((i+nptnc(ic,ir)).lt.ip_max) then
                       popf(i+nptnc(ic,ir))=1.0d0
                   endif
             else
                 do ip=1,nptnc(ic,ir)
                   popf(i+ip)=popf(i+ip)*
     &                        10.0d0**(jpopf(isip(i+ip))-jsum)/sum
                 enddo
             endif
             i=i+nptnc(ic,ir)
           enddo

C-------------------------
C      compress matrices
C-------------------------
C------ determine scaling
           i=0
           do ir=1,nptn(ic)
             do js1=1,ncnct_p
               jsum  = iexp_min
               jsumc  = iexp_min
               do ip=1,nptnc(ic,ir)
                 if(crmat(i+ip,js1).ne.0.0d0) then
                     jsum=max0(jsum,jcrmat(isip(i+ip),js1))
                 endif
                 if(crccd(i+ip,js1).ne.0.0d0) then
                     jsumc=max0(jsumc,jcrccd(isip(i+ip),js1))
                 endif
               enddo
               if(jsum.eq.iexp_min) then
                  jcrmat_t(ir,js1)=0
               else
                  jcrmat_t(ir,js1)=jsum
               endif
               if(jsumc.eq.iexp_min) then
                  jcrccd_t(ir,js1)=0
               else
                  jcrccd_t(ir,js1)=jsumc
               endif
             enddo
             i=i+nptnc(ic,ir)
           enddo
C------ row compress crmat and crccd
           i=0
           do ir=1,nptn(ic)
             do j=1,npsum
               sum  = 0.0d0
               sccd = 0.0d0
               do ip=1,nptnc(ic,ir)
                 sum=sum+crmat(i+ip,j)*
     &                   10.0d0**(jcrmat(isip(i+ip),isip(j))-
     &                            jcrmat_t(ir,isip(j)))
                 if (jcrccd(isip(i+ip),isip(j))-
     &               jcrccd_t(ir,isip(j)) . GT. 0.0D0) then
                        sccd=sccd+crccd(i+ip,j)*
     &                          10.0d0**(jcrccd(isip(i+ip),isip(j))-
     &                                   jcrccd_t(ir,isip(j)))
                 endif
               enddo
               crmat_c(ir,j)=sum
               crccd_c(ir,j)=sccd
             enddo
             i=i+nptnc(ic,ir)
           enddo

C------ row compress crecd noting extra row/column at beginning

           i=0
           do ir=1,nptn(ic)
             sum=0.0d0
             do ip=1,nptnc(ic,ir)
               sum=sum+crecd(i+ip+1,1)*popf(i+ip)
             enddo
             crecd_c(ir+1,1)=sum
             i=i+nptnc(ic,ir)
           enddo

           do j=1,npsum
             crecd_c(1,j+1)=crecd(1,j+1)
           enddo

           i=0
           do ir=1,nptn(ic)
             do j=1,npsum
               sum  = 0.0d0
               do ip=1,nptnc(ic,ir)
                 sum=sum+crecd(i+ip+1,j+1)*popf(i+ip)
               enddo
               crecd_c(ir+1,j+1)=sum
             enddo
             i=i+nptnc(ic,ir)
           enddo

C------ determine scaling
           do ir=1,nptn(ic)
             j=0
             do jr=1,nptn(ic)
               jsum  = iexp_min
               jsumc = iexp_min
               do ip=1,nptnc(ic,jr)
                 if(crmat_c(ir,j+ip).ne.0.0d0) then
                     jsum=max0(jsum,jcrmat_t(ir,isip(j+ip)))
                 endif
                 if(crccd_c(ir,j+ip).ne.0.0d0) then
                     jsumc=max0(jsumc,jcrccd_t(ir,isip(j+ip)))
                 endif
               enddo
               if(jsum.eq.iexp_min) then
                   jcrmat_c(ir,jr)=0
               else
                   jcrmat_c(ir,jr)=jsum
               endif
               if(jsumc.eq.iexp_min) then
                   jcrccd_c(ir,jr)=0
               else
                   jcrccd_c(ir,jr)=jsumc
               endif
               j=j+nptnc(ic,jr)
             enddo
           enddo

C------ column compress crmat and crccd
           do ir=1,nptn(ic)
             j=0
             do jr=1,nptn(ic)
               sum  = 0.0d0
               sccd = 0.0d0
               do ip=1,nptnc(ic,jr)
                 sum=sum+crmat_c(ir,j+ip)*popf(j+ip)*
     &                   10.0d0**(jcrmat_t(ir,isip(j+ip))-
     &                            jcrmat_c(ir,jr))
                 if (jcrccd_t(ir,isip(j+ip))-
     &               jcrccd_c(ir,jr) .GT. 0.0d0) then
                     sccd=sccd+crccd_c(ir,j+ip)*popf(j+ip)*
     &                       10.0d0**(jcrccd_t(ir,isip(j+ip))-
     &                                jcrccd_c(ir,jr))
                 endif
                 
               enddo
               crmat_c(ir,jr)=sum
               crccd_c(ir,jr)=sccd
               j=j+nptnc(ic,jr)
             enddo
           enddo

C------ column compress crecd noting extra row/column at beginning

           do ir=1,nptn(ic)
             crecd_c(ir+1,1)=crecd(ir+1,1)
           enddo

           j=0
           do jr=1,nptn(ic)
             sum  = 0.0d0
             do ip=1,nptnc(ic,jr)
               sum=sum+crecd_c(1,j+ip+1)*popf(j+ip)
             enddo
             crecd_c(1,jr+1)=sum
             j=j+nptnc(ic,jr)
           enddo

           do ir=1,nptn(ic)
             j=0
             do jr=1,nptn(ic)
               sum  = 0.0d0
               do ip=1,nptnc(ic,jr)
                 sum=sum+crecd_c(ir+1,j+ip+1)*popf(j+ip)
               enddo
               crecd_c(ir+1,jr+1)=sum
               j=j+nptnc(ic,jr)
             enddo
           enddo
C-------------------------
C      compress vectors
C-------------------------
C------ determine scaling
           j=0
           do jr=1,nptn(ic)
             jsplt = iexp_min
             jspls = iexp_min
             do ip=1,nptnc(ic,jr)
               if(crplt(j+ip).ne.0.0d0) then
                   jsplt=max0(jsplt,jcrplt(isip(j+ip)))
               endif
               if(crpls(j+ip).ne.0.0d0) then
                   jspls=max0(jspls,jcrpls(isip(j+ip)))
               endif
             enddo
             if(jsplt.eq.iexp_min) then
                 jcrplt_c(jr)=0
             else
                 jcrplt_c(jr)=jsplt
             endif
             if(jspls.eq.iexp_min) then
                 jcrpls_c(jr)=0
             else
                 jcrpls_c(jr)=jspls
             endif
             j=j+nptnc(ic,jr)
           enddo
C------ compress
           j=0
           do jr=1,nptn(ic)
             splt = 0.0d0
             spls = 0.0d0
             szcd = 0.0d0
             sycd = 0.0d0
             do ip=1,nptnc(ic,jr)
               splt=splt+crplt(j+ip)*popf(j+ip)*
     &                   10.0d0**(jcrplt(isip(j+ip))-jcrplt_c(jr))
               spls=spls+crpls(j+ip)*popf(j+ip)*
     &                   10.0d0**(jcrpls(isip(j+ip))-jcrpls_c(jr))

               szcd=szcd+crzcd(j+ip)*popf(j+ip)
               sycd=sycd+crycd(j+ip)*popf(j+ip)
             enddo
             crplt_c(jr)=splt
             crpls_c(jr)=spls
             crzcd_c(jr)=szcd
             crycd_c(jr)=sycd
             j=j+nptnc(ic,jr)
           enddo

C------ determine scaling
           j=nptnc(ic,1)
           do jr=1,nptn(ic)-1
             jsprb = iexp_min
             jsprc = iexp_min
             do ip=1,nptnc(ic,jr+1)
               if(crprb(j+ip).ne.0.0d0) then
                   jsprb=max0(jsprb,jcrprb(isip(j+ip)))
               endif
               if(crprc(j+ip).ne.0.0d0) then
                   jsprc=max0(jsprc,jcrprc(isip(j+ip)))
               endif
             enddo
             if(jsprb.eq.iexp_min) then
                 jcrprb_c(jr)=0
             else
                 jcrprb_c(jr+1)=jsprb
             endif
             if(jsprc.eq.iexp_min) then
                 jcrprc_c(jr)=0
             else
                 jcrprc_c(jr+1)=jsprc
             endif
             j=j+nptnc(ic,jr+1)
           enddo
C------ compress
           j=nptnc(ic,1)
           do jr=1,nptn(ic)-1
             sprb = 0.0d0
             sprc = 0.0d0
             do ip=1,nptnc(ic,jr+1)
               sprb=sprb+crprb(j+ip)*popf(j+ip)*
     &                   10.0d0**(jcrprb(isip(j+ip))-jcrprb_c(jr+1))
               sprc=sprc+crprc(j+ip)*popf(j+ip)*
     &                   10.0d0**(jcrprc(isip(j+ip))-jcrprc_c(jr+1))

             enddo
             crprb_c(jr+1)=sprb
             crprc_c(jr+1)=sprc
             j=j+nptnc(ic,jr+1)
           enddo

C-----------------------------------------------------------------------
C  unpick child generalised collisional-radiative coeffts. back into
C  tabulations ready for output as child adf11 files
C-----------------------------------------------------------------------

           do ix=1,isdimd
              dgcr_c(1,ix)=0.0
              dgcr_c(2,ix)=0.0
           enddo

           call dgunpk_sc( ndclas   , isdimd   ,
     &                     idcnct   ,
     &                     ncnct_c  , icnctv_c ,
     &                     crmat_c  , jcrmat_c , crccd_c  , jcrccd_c ,
     &                     crprb_c  , jcrprb_c , crprc_c  , jcrprc_c ,
     &                     crplt_c  , jcrplt_c , crpls_c  , jcrpls_c ,
     &                     crzcd_c  , crycd_c  , crecd_c  ,
     &                     iblmxa_c , ispbra_c , isppra_c , isstgra_c,
     &                     dgcr_c   , lgcra_c
     &                   )
           do iclass=1,ndclas
             if(lgcra_c(iclass))then
                 do i=1,iblmxa_c(iclass)
                   if(dgcr_c(iclass,i).le.-74.0d0) then
                       dgcra_c(iclass,i,it,id)=-74.0d0
                   else
                       dgcra_c(iclass,i,it,id)=dgcr_c(iclass,i)
                   endif
                 enddo
             endif
           enddo

 20      continue   ! Te
 10    continue     ! dens


C-----------------------------------------------------------------------
C  Calculate partitioned ionisation balance - but only if lpart is .TRUE.
C-----------------------------------------------------------------------

       if (lpart) then

          do id=1,idmax_p

            do it=itmax_p,1,-1

              if (ltick) then
                 write(pipeou,*)one
                 call xxflsh(pipeou)
              endif

              do iclass=1,ndclas
                do ip=1,npsum_c
                  dgcr_c(iclass,ip) = dgcra_c(iclass,ip,it,id)
                enddo
                do ip=npsum_c+1,isdimd
                   dgcr_c(iclass,ip) = 0.0
                enddo
              enddo

              call dgmpop_sc( ndclas   , isdimd  , izdimd  , ipdimd   ,
     &                        idcnct   ,
     &                        ncnct_c  , icnctv_c,
     &                        iblmxa_c , ispbra_c, isppra_c, isstgra_c,
     &                        dgcr_c   ,
     &                        npsum_c  , popf    , jpopf   ,
     &                        crmat    , jcrmat  , crccd   , jcrccd   ,
     &                        crprb    , jcrprb  , crprc   , jcrprc   ,
     &                        crplt    , jcrplt  , crpls   , jcrpls   ,
     &                        crzcd    , crycd   , crecd
     &                      )

              do i=1,npsum_c
                 fabun_c(it,id,i) = popf(i)
              enddo

              do is1=1,ncnct_c
                 jfabun_c(it,id,is1)= jpopf(is1)
              enddo

            end do

          end do
        endif

C-----------------------------------------------------------------------
 2002 format(1x,32('*'),' dgprt_11 error ',32('*')//
     &       1x,'acd file does not exist : ', a)
 2003 format(1x,32('*'),' dgprt_11 error ',32('*')//
     &       1x,'script/gcr mismatch: ', a,'=',i3,' ne ',a,'=',i3)
 2010 format(/1x,28('*'),'  program terminated   ',27('*'))

 1022 format(i2)
 1112 format(i2,'. ',1a10)
C-----------------------------------------------------------------------
       end
