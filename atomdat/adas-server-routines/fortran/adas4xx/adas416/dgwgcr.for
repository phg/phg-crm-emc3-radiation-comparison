      subroutine dgwgcr( iunit      , date      , realname ,
     &                   dsngcr_p   , dsngcr_c  ,
     &                   ndclas     , ndstack   ,
     &                   isdimd     , iddimd   , itdimd    ,
     &                   ndcnct     ,
     &                   iz0_c      , is1min_c  , is1max_c ,
     &                   ncnct_c    , icnctv_c  ,
     &                   ncptn_stack, cptn_stack,
     &                   iblmxa_c   , ispbra_c  , isppra_c , isstgra_c ,
     &                   idmax_p    , itmax_p   ,
     &                   ddens_p    , dtev_p    ,
     &                   dnr_elea_p , dnr_amsa_p,
     &                   dgcra_c    , lgcra_c   , lres_c   , lptn_c
     &                 )
      implicit none
c
c-----------------------------------------------------------------------
c
c ****************** fortran 77 subroutine: dgwgcr ********************
c
c  purpose: To write out a complete set  of adf11 files for a child
c           partition.
c
c
c  calling program: adas416
c
c  notes:    (1) A `standard' adf11 file contains gcr data between one
c                whole ionisation stage and another whole ionisation
c                stage.
c                A `resolved' (or partial) adf11 file contains gcr data
c                between a set of metastables of one ionisation stage
c                and a set of metastables of another ionisation stage.
c                A resolved file is distinguished from a standard file
c                by the presence of a `connection vector' in the adf11
c                data file header lines.
c                The connection vector specifies the number of meta-
c                stables in each ionisation stage which are coupled
c                together by gcr data.
c            (2) A `partitioned' adf11 file contains gcr data between
c                clumps of ionisation stages or metastables or comb-
c                inations of the two called `partitions'.
c                A `partition level' is a specification of the
c                partitions which span all the ionisation stages (and
c                metastables) of an element.  Successive partition
c                levels give a heirarchy corresponding to larger
c                partitions and greater clumping.
c                A `superstage' is a set of partitions which are close-
c                coupled.
c                There are thus equivalences :
c                        ionisation stage   -   superstage
c                        metastable         -   partition
c                        ion charge         -   superstage index
c                A partitioned adf11 file may be standard (with each
c                superstage comprising only one partition) or resolved.
c                A partitioned file is distinguished by the presence of
c                `partition specification block' in the adf11 data
c                file header lines.
c            (3) When  a partition specification block is present, it
c                should be ordered from the highest partition level
c                index to lowest partition level index.  Thus the first
c                partition in the partition block has the least number
c                of partitions and the last has the greatest number.
c            (4) Twelve classes of adf11 data file may be read by the
c                subroutine as follow:
c
c                class index    type      GCR data content
c                -----------    ----      ----------------
c                    1          acd       recombination coeffts
c                    2          scd       ionisation coeffts
c                    3          ccd       CX recombination coeffts
c                    4          prb       recomb/brems power coeffts
c                    5          prc       CX power coeffts
c                    6          qcd       base meta. coupl. coeffts
c                    7          xcd       parent meta. coupl. coeffts
c                    8          plt       low level line power coeffts
c                    9          pls       represent. line power coefft
c                   10          zcd       effective charge
c                   11          ycd       effective squared charge
c                   12          ecd       effective ionisation potential
c
c            (5) A resolved adf11 file, with a connection vector, has a set
c                of names and pointers at precise positions in the data file
c                which are recognised.
c                The names are different for partitioned and unpartitioned
c                data files as follow:
c
c                       file      unpartitioned         partitioned
c                       class        names                 names
c
c                       (all)         z1                   s1
c
c                                (indices 1 and 2)     (indices 1 and 2)
c                       ----       ----     ----         ----     ----
c                       acd        iprt     igrd         ispp     ispb
c                       scd        iprt     igrd         ispp     ispb
c                       ccd        iprt     igrd         ispp     ispb
c                       prb        iprt                  ispp
c                       prc        iprt                  ispp
c                       qcd        igrd     jgrd         ispb     jspb
c                       xcd        iprt     jprt         ispp     jspp
c                       plt        igrd                  ispb
c                       pls        igrd                  ispb
c                       zcd        igrd                  ispb
c                       ycd        igrd                  ispb
c                       ecd        igrd                  ispb
c
c             (6) In partitioned nomenclature: s=superstage; p=partition;
c                 b=base (current superstage), p=parent (next up super-
c                 stage), c=child (next down superstage). Thus arrays
c                 `iprtr' and `igrd' in old notation are now substituted
c                 by `isppr' and `ispbr' respectively internally and in
c                 external naming.
c
c
c subroutine:
c
c
c  input : (i*4)  iunit     = unit to which input file is allocated
c  input : (c*8)  date      = current date
c  input   (c*30) realname  = user identifier passed from idl
c  input : (c*120)dsngcr_p  = parent gcr template file name from script
c  input : (c*120)dsngcr_c  = child gcr template file name from script
c  input : (i*4)  ndclas    = number of adf11 master file classes
c  input : (i*4)  ndstack   = maximum number of partition text lines
c  input : (i*4)  isdimd    = maximum number of (sstage, parent, base)
c                            blocks in isonuclear master files
c  input : (i*4)  iddimd    = maximum number of dens values in
c                             isonuclear master files
c  input : (i*4)  itdimd    = maximum number of temp values in
c                             isonuclear master files
c  input : (i*4)  ndcnct    = maximum number of elements in connection
c                             vector
c
c  input : (i*4)  iz0_c     = nuclear charge
c  input : (i*4)  is1min_c  = minimum ion charge + 1
c                             (generalised to connection vector index)
c  input : (i*4)  is1max_c  = maximum ion charge + 1
c                             (note excludes the bare nucleus)
c                             (generalised to connection vector index
c                              and excludes last one which always remains
c                              the bare nucleus)
c
c  input : (i*4)  nptnl_c   = number of partition levels in block
c  input : (i*4)  nptn_c()  = number of partitions in partition level
c  input : (i*4)  nptn_c()  = number of partitions in partition level
c                             1st dim: partition level
c  input : (i*4)  nptnc_c(, = number of components in partition
c                             1st dim: partition level
c                             2nd dim: member partition in partition level
c  input : (i*4)  iptnla_c()= partition level label (0=resolved root,1=
c                                                    unresolved root)
c                             1st dim: partition level index
c  input : (i*4)  iptna_c(,)= partition member label (labelling starts at 0)
c                             1st dim: partition level index
c                             2nd dim: member partition index in partition
c                             level
c  input : (i*4)  iptnca_c(,,)= component label (labelling starts at 0)
c                               1st dim: partition level index
c                               2nd dim: member partition index in partition
c                                       level
c                               3rd dim: component index of member partition
c  input : (i*4)  ncnct_c   = number of elements in connection vector
c  input : (i*4)  icnctv_c()= connection vector of number of partitions
c                             of each superstage in resolved case
c                             including the bare nucleus
c                             1st dim: connection vector index
c
c  input:  (i*4)  ncptn_stack=  number of text lines in partition stack
c  input:  (c*80) cptn_stack()= stack of partition text lines

c  input : (i*4)  iblmxa_c() = number of (sstage, parent, base)
c                              blocks in isonuclear master file class
c                                1st dim: adf11 class index
c  input : (i*4)  isppra_c(,)= 1st (parent) index for each partition block
c                                1st dim: adf11 class index
c                                2nd dim: index of (sstage, parent, base)
c                                     block in isonuclear master file
c  input : (i*4)  ispbra_c(,)= 2nd (base) index for each partition block
c                                1st dim: adf11 class index
c                                2nd dim: index of (sstage, parent, base)
c                                     block in isonuclear master file
c  input : (i*4)  isstgra_c()= s1 for each resolved data block
c                            (generalises to connection vector index)
c                                1st dim: adf11 class index
c                                2nd dim: index of (sstage, parent, base)
c                                     block in isonuclear master file
c  input:  (c*12) dnr_elea_p()  = donor element (set to ' ' for classes
c                                 other than 3 and 5)
c                                 1st dim: adf11 class index
c  input:  (r*8)  dnr_amsa_p()  = donor mass (set to 0 for classes other
c                                 than 3 and 5)
c                                 1st dim: adf11 class index
c  input : (r*8)  dgcra_c(,,,)= interpolation of prc coefficient (w cm3 )
c                                1st dim: adf11 class index
c                                2nd dim: index of (sstage, parent, base)
c                                         block in isonuclear master file
c  input : (l*4)  lgcra_c()   = .true.  => data present for class
c                               .false. => data not present for class
c                                1st dim: adf11 class index
c
c
c
c routines:
c          routine    source    brief description
c          ----------------------------------------------------------
c         i4unit     adas      fetch unit number for output of messages
c         xxslen     adas      find string less front and tail blanks
c         xxrepl     adas      substitute sub-string in string
c         xfelem     adas      return element name given nuclear charge
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    04/10/06
c
c
c  Version : 1.1
c  Date    : 15-03-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 08-02-2010
c  Modified: Martin O'Mullane
c            - Remove blanks from output file names.
c
c  Version : 1.3
c  Date    : 04-04-2019
c  Modified: Martin O'Mullane
c            - Units of the power coefficients are W cm3 not W cm3 s-1.
c
c------------------------------------------------------------------------
      integer   i4unit    , iunit    , ndclas   , ndstack
      integer   iddimd    , itdimd   , isdimd
      integer   ndcnct
      integer   ncptn_stack
      integer   idmax_p   , itmax_p
      integer   iz0_c     , is1min_c , is1max_c
      integer   ncnct_c
      integer   iclass    , nsubs    , ifirst   , ilast
      integer   i         , it       , id
c------------------------------------------------------------------------
      integer   icnctv_c(ndcnct)         , iblmxa_c(ndclas)
      integer   ispbra_c(ndclas,isdimd)  , isppra_c(ndclas,isdimd) ,
     &          isstgra_c(ndclas,isdimd)
c------------------------------------------------------------------------
      real*8     ddens_p(iddimd)         , dtev_p(itdimd)
      real*8     dgcra_c(ndclas,isdimd,itdimd,iddimd)
      real*8     dnr_amsa_p(ndclas)
c------------------------------------------------------------------------
      character  dsngcr_p*120  , dsngcr_c*120  ,
     &           c2*2          , c8*8          , c12*12        ,
     &           c22*22        , cdnr_tit*22   ,
     &           c80*80        , c120*120      , c2j*2         , c2k*2
      character  cstrg*120     , cstrg1*120    , cterm*80  , cblank*80
      character  xfelem*12     , date*8        , realname*30
c------------------------------------------------------------------------
      character  cclass(12)*3   , cptrn0(12)*4
      character  cptrn3(12)*4   , cptrn4(12)*4
      character  unitsa(12)*12
      character  cptn_stack(ndstack)*80
      character  dnr_elea_p(ndclas)*12
c-----------------------------------------------------------------------
      logical    lres_c         , lptn_c
c-----------------------------------------------------------------------
      logical    lgcra_c(ndclas)
c-----------------------------------------------------------------------
      data       cterm /'-----------------------------------------------
     &---------------------------------'/
      data       cblank/'
     &                                 '/
      data       cclass/'acd' ,'scd' ,'ccd' ,'prb' ,'prc' ,
     &                  'qcd' ,'xcd' ,'plt' ,'pls' ,'zcd',
     &                  'ycd' ,'ecd' /
      data       cptrn0/'/ACD','/SCD','/CCD','/PRB','/PRC',
     &                  '/QCD','/XCD','/PLT','/PLS','/ZCD',
     &                  '/YCD','/ECD'/
      data       cptrn3/'ISPP','ISPP','ISPP','ISPP','ISPP',
     &                  'ISPB','ISPP','ISPB','ISPB','ISPB',
     &                  'ISPB','ISPB'/
      data       cptrn4/'ISPB','ISPB','ISPB','----','----',
     &                  'JSPB','JSPP','----','----','----',
     &                  '----','ISPP'/
      data       unitsa/'cm3 s-1     ','cm3 s-1     ','cm3 s-1     ',
     &                  'W cm3       ','W cm3       ','cm3 s-1     ',
     &                  'cm3 s-1     ','W cm3       ','W cm3       ',
     &                  '            ','            ','eV          '/
c------------------------------------------------------------------------
c
c------------------------------------------------------------------------
c  cycle through each adf11 class in turn
c------------------------------------------------------------------------

       do iclass = 1, ndclas
         if(lgcra_c(iclass)) then

             cstrg=dsngcr_c
             call xxrepl(cstrg,'***',cclass(iclass),cstrg1,nsubs)
             call xxrmve(cstrg1,c120,' ')
             open( unit=iunit , file=c120 , status='unknown')

             c80=cblank
             write(c80(4:5),'(i2)') iz0_c
             write(c80(9:10),'(i2)')idmax_p
             write(c80(14:15),'(i2)')itmax_p
             write(c80(19:20),'(i2)')is1min_c
             write(c80(24:25),'(i2)')is1max_c
             c12 = xfelem(iz0_c)
             if((iclass.eq.3).or.(iclass.eq.5)) then
                 call xxrmve(c12//':'//dnr_elea_p(iclass),c22,' ')
                 call xxcase(c22,cdnr_tit,'uc')
                 write(c80(31:53),'(a1,a22)')'/',cdnr_tit
             else
                 write(c80(31:43),'(a1,a12)')'/',c12
             endif
             write(c80(54:57),'(a4)')cptrn0(iclass)
             write(c80(59:70),'(a1,a11)')'/','GCR PROJECT'
             write(c80(76:80),'(a5)')'ADF11'

             write(iunit,'(1a80)')c80
             write(iunit,'(1a80)')cterm
             if(lres_c) then
                 write(iunit,'(20i5)')(icnctv_c(i),i=1,ncnct_c)
                 write(iunit,'(1a80)')cterm
             endif

             if(lptn_c) then
                 do i=1,ncptn_stack
                   write(iunit,'(1a80)')cptn_stack(i)
                 enddo
                 write(iunit,'(1a80)')cterm
             endif

             write(iunit,'(8f10.5)')(ddens_p(i),i=1,idmax_p)
             write(iunit,'(8f10.5)')(dtev_p(i),i=1,itmax_p)

             do i=1,iblmxa_c(iclass)
               write(c2,'(i2)')isstgra_c(iclass,i)
               c80=cterm
               write(c80(44:44),'(1a1)')'/'
               write(c80(53:62),'(a5,a2,a3)')'/ S1=',c2,'   '
               call xxrepl(date,'/',':',c8,nsubs)
               write(c80(63:80),'(a8,a8,a2)')'/ DATE= ',c8,'  '
c---------------------------------------------------
c------------------
c      process acd
c------------------
               if(iclass.eq.1) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c2k,'(i2)')ispbra_c(iclass,i)
                       write(c80(22:32),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  '
                       write(c80(33:43),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn4(iclass),'=',c2k,'  '
                   endif
c------------------
c      process scd
c------------------
               elseif(iclass.eq.2) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c2k,'(i2)')ispbra_c(iclass,i)
                       write(c80(22:32),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  '
                       write(c80(33:43),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn4(iclass),'=',c2k,'  '
                   endif
c------------------
c      process qcd
c------------------
               elseif(iclass.eq.6) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c2k,'(i2)')ispbra_c(iclass,i)
                       write(c80(22:32),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  '
                       write(c80(33:43),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn4(iclass),'=',c2k,'  '
                   endif
c------------------
c      process xcd
c------------------
               elseif(iclass.eq.7) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c2k,'(i2)')ispbra_c(iclass,i)
                       write(c80(22:32),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  '
                       write(c80(33:43),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn4(iclass),'=',c2k,'  '
                   endif
c------------------
c      process ccd
c------------------
               elseif(iclass.eq.3) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c2k,'(i2)')ispbra_c(iclass,i)
                       write(c80(22:32),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  '
                       write(c80(33:43),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn4(iclass),'=',c2k,'  '
                       write(c80(45:52),'(a4,f4.2)')' MD=',
     &                  dnr_amsa_p(iclass)
                   endif
c------------------
c      process prb
c------------------
               elseif(iclass.eq.4) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c80(22:33),'(a2,a4,a,a2,a3)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  /'
                   endif
c------------------
c      process prc
c------------------
               elseif(iclass.eq.5) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c80(22:33),'(a2,a4,a,a2,a3)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  /'
                       write(c80(45:52),'(a4,f4.2)')' MD=',
     &                  dnr_amsa_p(iclass)
                   endif
c------------------
c      process plt
c------------------
               elseif(iclass.eq.8) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c80(22:33),'(a2,a4,a,a2,a3)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  /'
                   endif
c------------------
c      process pls
c------------------
               elseif(iclass.eq.9) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c80(22:33),'(a2,a4,a,a2,a3)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  /'
                   endif
c------------------
c      process zcd
c------------------
               elseif(iclass.eq.10) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c80(22:33),'(a2,a4,a,a2,a3)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  /'
                   endif
c------------------
c      process ycd
c------------------
               elseif(iclass.eq.11) then
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c80(22:33),'(a2,a4,a,a2,a3)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  /'
                   endif
c------------------
c      process ecd
c------------------
               elseif(iclass.eq.12) then
c                  if(lptn_c.or.lres_c) then
c                      write(c2j,'(i2)')isppra_c(iclass,i)
c                      write(c80(22:33),'(a2,a4,a,a2,a3)')
c     &                '/ ',cptrn3(iclass),'=',c2j,'  /'
c                  endif
                   if(lptn_c.or.lres_c) then
                       write(c2j,'(i2)')isppra_c(iclass,i)
                       write(c2k,'(i2)')ispbra_c(iclass,i)
                       write(c80(22:32),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn3(iclass),'=',c2j,'  '
                       write(c80(33:43),'(a2,a4,a,a2,a2)')
     &                 '/ ',cptrn4(iclass),'=',c2k,'  '
                   endif
c---------------------------------------------------
               endif

               write(iunit,'(1a80)')c80
               do it = 1 , itmax_p
                 if(iclass.lt.10) then
                     write(iunit,'(8f10.5)')
     &                  ( dgcra_c(iclass,i,it,id) , id = 1 , idmax_p)
                 else
                     write(iunit,'(1p8e10.3)')
     &                  ( dgcra_c(iclass,i,it,id) , id = 1 , idmax_p)
                 endif
               enddo
             enddo

c------------------------------------------------------------------------
c  complete  data set comments section
c------------------------------------------------------------------------
             c80=cterm
             write(c80(1:1),'(a1)')'C'

             write(iunit,'(1a80)')c80
             call xxrepl(dsngcr_p,'***',cclass(iclass),c120,nsubs)
             call xxslen(c120,ifirst,ilast)
             write(iunit,2007)cclass(iclass),unitsa(iclass),
     &                        c120(ifirst:ilast)
             call xxslen(realname,ifirst,ilast)
             write(iunit,2008)'adas416',realname(ifirst:ilast),date
             write(iunit,'(1a80)')c80

             close(iunit)

         endif
       enddo

      return
c
c-----------------------------------------------------------------------
c
 2007 format('C',/,
     &       'C    Rate coefficients: adf11 class ',1a3,/,
     &       'C    Units            : ',1a12,/,
     &       'C    Status           : partition applied',/,
     &       'C    Parent dataset   :  ',a
     &      )
 2008 format('C',/,
     &       'C    Code             : ',1a7,/,
     &       'C    Producer         : ',a,/,
     &       'C    Date             : ',1a8,/,
     &       'C'
     &      )
c
c-----------------------------------------------------------------------
c
      end
