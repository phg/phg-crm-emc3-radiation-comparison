       subroutine dgdiag_sc( nzdim  , npdim  , ndcnct ,
     &                       ncnct  , icnctv ,
     &                       cfrec  , jcfrec ,
     &                       cfion  , jcfion ,
     &                       cfmet  , jcfmet
     &                  )
       implicit none
c
c-----------------------------------------------------------------------
c  **************** fortran77 subroutine : dgdiag_sc *******************
c
c purpose: calculation of prime diagonal of superstage/partition
c         rate coefficient matrix including scaling
c
c calling program: dgmpop_sc
c
c input: (i*4) nzdim     = maximum number of superstages
c input: (i*4) npdim      = maximum number of partitions in a superstage
c input: (i*4) ncnct     = number of superstages
c input  (i*4) icnctv()     = partitions in each superstage
c                           1st dim: nzdim
c input: (r*8) cfrec(,,)  = recom. rate coefft. matrix between adjacent
c                           superstage partitions
c                           1st dim: index of recombined sstage part.
c                           2nd dim: index of recombining sstage part.
c                           3rd dim: index of sstage (2-> ncnct)
c input: (i*4) jcfrec()   = scaling for recom. rate coefft. matrix
c                           between adjacent superstages
c                           1st dim: index of sstage (2-> ncnct)
c input: (r*8) cfion(,,)  = ionis. rate coefft. matrix between adjacent
c                           superstage partitions
c                           1st dim: index of ionising sstage part.
c                           2nd dim: index of ionised sstage part.
c                           3rd dim: index of sstage (1->ncnct-1)
c input: (i*4) jcfion()   = scaling for ionis. rate coefft. matrix
c                           between adjacent superstages
c                           1st dim: index of sstage (1-> ncnct-1)
c input: (r*8) cfmet(,,)  = x-coupl. rate coefft. matrix within
c                           partitions of a superstage
c                           1st dim: index of lower sstage part.
c                           2nd dim: index of upper sstage part.
c                           3rd dim: index of sstage (1->ncnct)
c input: (i*4) jcfmet()   = scaling for x-coupl. rate coefft. matrix
c                           within a superstage
c                           1st dim: index of sstage (1-> ncnct)
c
c output:(r*8) cfmet(,,)  = diagonal (negatively signed) filled
c                           x-coupl. rate coefft. matrix within
c                           partitions of a superstage
c                           1st dim: index of lower sstage part.
c                           2nd dim: index of upper sstage part.
c                           3rd dim: index of sstage (1->ncnct)
c output:(i*4) jcfmet()   = scaling for x-coupl. rate coefft. matrix
c                           within a superstage
c                           1st dim: index of sstage (1-> ncnct)
c
c
c routines : none
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    20/04/06
c
c
c  Version : 1.1
c  Date    : 20-04-2006
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 09-04-2010
c  Modified: Martin O'Mullane
c            - Redefine iexp_min to be 2 less than -2**31 to fix a
c              gfortran overflow problem at compilation.
c
c-----------------------------------------------------------------------
       integer  iexp_min
c-----------------------------------------------------------------------
       parameter ( iexp_min=-2*(2**30-1) )
c-----------------------------------------------------------------------
       integer nzdim  , npdim  , ndcnct
       integer ncnct  , jsum
       integer i      , j      , k
c-----------------------------------------------------------------------
       integer icnctv(ndcnct)
       integer jcfrec(nzdim) , jcfion(nzdim) , jcfmet(nzdim)
c-----------------------------------------------------------------------
       real*8  sum
c-----------------------------------------------------------------------
       real*8  cfrec(npdim,npdim,nzdim),
     &         cfion(npdim,npdim,nzdim),
     &         cfmet(npdim,npdim,nzdim)
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c  form the elements on the main diagonal of the matrix
c-----------------------------------------------------------------------
c
       do 1 i = 1,ncnct

         jsum= iexp_min
	 jsum=max0(jsum,jcfmet(i))
	 if(i.gt.1) then
	     jsum=max0(jsum,jcfrec(i))
	 endif
	 if(i.lt.ncnct) then
	     jsum=max0(jsum,jcfion(i))
	 endif

         do 2 j = 1,icnctv(i)
           sum = 0.0d0
             do 3 k = 1,icnctv(i)
	       if(j.ne.k) then
		   cfmet(k,j,i)=cfmet(k,j,i)*10.0d0**(jcfmet(i)-jsum)
                   sum = sum-cfmet(k,j,i)
	       endif
 3           continue
c-------------------------------------------------------------------------------
             if(i  .gt.  1)then
               do 4 k = 1,icnctv(i-1)
                  sum = sum-cfrec(k,j,i)*10.0d0**(jcfrec(i)-jsum)
 4             continue
             endif
c-------------------------------------------------------------------------------
             if (i  .lt.  ncnct)then
               do 5 k = 1,icnctv(i+1)
                  sum = sum-cfion(k,j,i)*10.0d0**(jcfion(i)-jsum)
 5             continue
             endif
c-------------------------------------------------------------------------------
             cfmet(j,j,i) = sum
 2         continue
           jcfmet(i)=jsum
 1     continue
c-------------------------------------------------------------------------------
       return
c-------------------------------------------------------------------------------
       end
