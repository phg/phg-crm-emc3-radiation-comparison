      subroutine  dgcnmk( elemt,
     &                    imdimd  , izdimd  , ipdimd   ,
     &                    ndptnl  , ndptn   , ndcnct   ,
     &                    nptnl   , nptn    , nptnc    ,
     &                    iptnla  ,
     &                    iptnl_p , ncnct   , icnctv   , icnctvs   ,
     &                    is1min_p, is1max_p, npsum_p  ,
     &                    isip    , ipip    , ipisp    ,
     &                    iptnl_c , ncnct_c , icnctv_c ,
     &                    is1min_c, is1max_c, npsum_c  ,
     &                    isip_c  , ipip_c  , ipisp_c  ,
     &                    poptit_c          , chpop_c  ,
     &                    mappc_s , mappc_m ,
     &                    issa_min, issa_max
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: dgcnmk    ******************
c
c  purpose: To make a child connection vector from the parent
c           connection vector and the child partition specification.
c           Make poiter vectors for referencing to child superstages
c           and partitions within a superstage and to bak reference
c           to parent partitions.
c
c  notes:   The child partition is assumed to have partition level
c           label iptnl_c. The usual pattern of ordering partition
c           levels in adf11 and scripts416 files is that this is the
c           top partition in the partition block.
c
c
c  calling program: adas416
c
c
c  subroutine:
c
c  input : (c*2)  elemt     = element chemical symbol
c  input : (i*4)  imdimd    = max. number of populations
c                             (=isdimd+1)
c  input : (i*4)  izdimd    = max. number of charge states in
c                             isonuclear master files
c  input : (i*4)  ipdimd    = max. number of metastables for
c                             each ionisation stage
c  input : (i*4)  ndptnl    = maximum level of partitions
c  input : (i*4)  ndptn     = maximum no. of partitions in one level
c  input : (i*4)  ndcnct    = maximum number of elements in connection
c                             vector
c
c  input : (i*4)  nptnl     = number of partition levels in block
c  input : (i*4)  nptn()    = number of partitions in partition level
c                             1st dim: partition level
c  input : (i*4)  nptnc(,)  = number of components in partition
c                             1st dim: partition level
c                             2nd dim: member partition in partition level
c  input : (i*4)  iptnla()  = partition level label (0=resolved root,1=
c                                                      unresolved root)
c                             1st dim: partition level index
c  input : (i*4)  iptnl_p   = parent partition level label.
c  input : (i*4)  ncnct     = number of elements in connection vector
c  input : (i*4)  icnctv()  = connection vector of number of partitions
c                             in each superstage in resolved case
c                             including the bare nucleus
c                             1st dim: connection vector index
c  input : (i*4)  is1min_p  = lowest superstage of parent
c  input : (i*4)  is1max_p  = highest superstage of parent (excludes bare
c                             nucleus
c  input : (i*4)  npsum_p   = number of partitions at parent level
c  input : (i*4)  isip()    = superstage index +1  for parent partition
c                             1st ind: parent partition index
c  input : (i*4)  ipip()    = parent partition index within a
c                             superstage for parent partition
c                             1st ind: population index
c  input : (i*4)  ipisp(,)  = pointer to parent population index
c                             1st ind: superstage index
c                             2nd ind: partition index in superstage
c  input : (i*4)  iptnl_c   = child partition level label.
c
c
c  output: (i*4)  icnctvs()= connection vector running count of parent
c                              partitions of each superstage
c                             1st dim: connection vector index
c  output: (i*4)  ncnct_c   = no. of elements in child connection vector
c  output: (i*4)  icnctv_c()= connection vector of no. of partitions
c                             in each child superstage in resolved case
c                             including the bare nucleus
c                             1st dim: child connection vector index
c  output: (i*4)  is1min_c  = lowest superstage of child
c  output: (i*4)  is1max_c  = highest superstage of child (excludes bare
c                             nucleus
c  output: (i*4)  npsum_c   = number of partitions at child level
c  output: (i*4)  isip_c()  = superstage index +1  for child population
c                             1st ind: parent partition index
c  output: (i*4)  ipip_c()  = partition index within a superstage for
c                             child partition
c                             1st ind: parent partition index
c  output: (i*4)  ipisp_c(,)= pointer to child partition
c                             1st ind: child superstage index
c                             2nd ind: child partition index in
c                                      superstage
c  output: (c*16) poptit_c()= name for child population on file header
c                             1st. dim: child population index
c  output: (c*20) chpop_c() = string of population index and name,
c                             1st. dim: child population index
c  output: (i*4)  mappc_s(,)= associated child superstage index
c                             1st. dim: parent superstage index
c                             2nd. dim: parent partition (metastable)
c                                        within superstage
c  output: (i*4)  mappc_m(,)= associated child partition (metastable)
c                             index within child superstage
c                             1st. dim: parent superstage index
c                             2nd. dim: parent partition (metastable)
c                                        within superstage
c  output: (i*4)  issa_min()= lowest parent superstage in child
c                             superstage
c                             1st dim: child connection vector index
c  output: (i*4)  issa_max()= highest parent superstage in child
c                             superstage
c                             1st dim: child connection vector index
c
c
c  routines:
c          routine    source    brief description
c          -------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c  date:    13/09/05
c
c
c  Version : 1.1
c  Date    : 03-01-2007
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
      integer   idcnct    , idptn
c-----------------------------------------------------------------------
      parameter ( idcnct = 100 , idptn  = 128 )
c-----------------------------------------------------------------------
      integer   i4unit
      integer   ndptnl    , ndptn    , ndcnct
      integer   izdimd    , ipdimd   , imdimd
      integer   nptnl     , iptnl_p  , ncnct    , iptnl_c  , ncnct_c
      integer   i         , j        , ic
      integer   icount
      integer   is1min_p  , is1max_p , npsum_p
      integer   is1min_c  , is1max_c , npsum_c  ,
     &          is        , is1      , ip       , ispb     ,
     &          ic0       , ic1      , index
c-----------------------------------------------------------------------
      character chs*2     , chp*2    , elemt*2
c-----------------------------------------------------------------------
      integer   nptn(ndptnl)          , nptnc(ndptnl,ndptn)
      integer   iptnla(ndptnl)
      integer   icnctv(ndcnct)        , icnctv_c(ndcnct)
      integer   icnctvs(ndcnct)       , nptncs(idptn)
      integer   issa_min(ndcnct)      , issa_max(ndcnct)
      integer   isip(imdimd)   , ipip(imdimd)   , ipisp(izdimd,ipdimd),
     &          isip_c(imdimd) , ipip_c(imdimd) ,
     &          ipisp_c(izdimd,ipdimd)
      integer   mappc_s(izdimd,ipdimd), mappc_m(izdimd,ipdimd)
c-----------------------------------------------------------------------
      character poptit_c(imdimd )*16  , chpop_c(imdimd)*20
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c   check internal dimensions
c-----------------------------------------------------------------------
       if(idcnct.ne.ndcnct) then
         write(i4unit(-1),2002)'mismatch of internal dimensions: ',
     &                         'idcnct = ',idcnct,' .ne. ndcnct = ',
     &                          ndcnct
         write(i4unit(-1),2003)
         stop
       endif

       if(idptn.ne.ndptn) then
         write(i4unit(-1),2002)'mismatch of internal dimensions: ',
     &                         'idptn = ',idptn,' .ne. ndptn = ',
     &                          ndptn
         write(i4unit(-1),2003)
         stop
       endif

c-----------------------------------------------------------------------
c   find the child partition level in the partition level set
c-----------------------------------------------------------------------

      do i = 1, nptnl
        if (iptnl_c.eq.iptnla(i)) ic=i
      enddo

c-----------------------------------------------------------------------
c   find the parent partition level in the partition level set
c-----------------------------------------------------------------------

      do i = 1, nptnl
        if (iptnl_p.eq.iptnla(i)) ip=i
      enddo

c-----------------------------------------------------------------------
c   form cumulative totals for connection vector and partition level
c-----------------------------------------------------------------------

      icount = 0

      do i=1,ncnct
        icount=icount + icnctv(i)
        icnctvs(i)=icount
      enddo

      icount=0
      do i=1,nptn(ic)
        icount=icount + nptnc(ic,i)
        nptncs(i)=icount
      enddo

c-----------------------------------------------------------------------
c   execute matching to establish child connection vector
c-----------------------------------------------------------------------

      ncnct_c = 0
      icount = 0
      issa_min(1)=1

      do i=1,nptn(ic)
        do j=1,ncnct
          if(nptncs(i).eq.icnctvs(j))then
              ncnct_c  =ncnct_c + 1
              icnctv_c(ncnct_c) = icount+1
              issa_max(ncnct_c)=j
              issa_min(ncnct_c+1)=j+1
              icount = 0
              go to 10
          endif
        enddo
        icount = icount + 1
   10   continue

      enddo

c------------------------------------------------------------------------
c   now make the pointer vectors for the child superstages and partitions
c------------------------------------------------------------------------
       is1min_c = 1
       is1max_c = ncnct_c-1

       npsum_c = 0
       do is1 = is1min_c, is1max_c + 1
         is = is1 -1
         ispb = icnctv_c( is1-is1min_c + 1)
         do ip = 1,ispb
           npsum_c = npsum_c + 1
           write(chs , 1022) is
           write(chp , 1022) ip
           poptit_c(npsum_c) = elemt//'[ss:'//chs//'](pt:'//chp//')'
           write( chpop_c(npsum_c) , 1112) npsum_c ,
     &                                     poptit_c(npsum_c)
           isip_c(npsum_c)   = is1
           ipip_c(npsum_c)   = ip
           ipisp_c(is1-is1min_c+1,ip) = npsum_c
         end do
       end do

c------------------------------------------------------------------------
c   finally make parent-child mappings mappc_s and mappc_m
c------------------------------------------------------------------------

       ic0=0
       ic1=nptncs(1)
       index=1
       do ip=1,npsum_p
           if((ip.gt.ic0).and.(ip.le.ic1))then
               do ic=1,npsum_c
                 if(ipisp_c(isip_c(ic),ipip_c(ic)).eq.index)then
                     mappc_s(isip(ip),ipip(ip))=isip_c(ic)
                     mappc_m(isip(ip),ipip(ip))=ipip_c(ic)
                     go to 16
                  endif
                enddo
           endif
   16      if(ip.eq.ic1) then
               index=index+1
               ic0=ic1
               ic1=nptncs(ic+1)
           endif
       enddo

       return

c
c-----------------------------------------------------------------------
c
 1112 format(i2,'. ',1a16)
 1022 format(i2)
 2002 format(1x,30('*'),'    dgcnmk error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
