        subroutine dgmfsp_sc( ndstat , ndmet  , ndcnct ,
     &                        nstate , nmet   , id     , nmsum  ,
     &                        cfrec  , jcfrec , cfion  , jcfion ,
     &                        cfmet  , jcfmet ,
     &                        popf   , jpopf  ,
     &                        lagain
     &                      )
        implicit none
c
c-----------------------------------------------------------------------
c
c******************* fortran77 subroutine: dgmfsp_sc *******************
c
c purpose: evaluates the populations of partitions in a superstage
c          context using a partitioned matrix double-pass algorithm
c          around a dominant superstage pivot with full matrix and
c          population scaling.
c
c notes:  (1) On exit, the cfrec, cfion and cfmet matrices are in
c             general rescaled with the new scale factors in jcfrec,
c             jcfion and jcfmet respectively.
c         (2) A call to dgdiag is required to establish the complete
c             on diagonal matrix elements (-ve signed) for cfmet.
c         (3) An initial estimate of the dominant superstage index, id,
c             is required.
c
c calling program: dgmpop_sc
c
c input: (i*4) ndstat     = maximum number of superstages
c input: (i*4) ndmet      = maximum number of partitions in a superstage
c input :(i*4) ndcnct     = maximum number of elements in connection
c                              vector
c input: (i*4) nstate     = number of superstages
c input: (i*4) nmet()     = partitions in each superstage
c                           1st dim: ndstat
c input: (i*4) id         = index of dominant superstage
c input: (i*4) nmsum      = total number of partitions
c
c input: (r*8) cfrec(,,)  = recom. rate coefft. matrix between adjacent
c                           superstage partitions
c                           1st dim: index of recombined sstage part.
c                           2nd dim: index of recombining sstage part.
c                           3rd dim: index of sstage (2-> nstate)
c input: (i*4) jcfrec()   = scaling for recom. rate coefft. matrix
c                           between adjacent superstages
c                           1st dim: index of sstage (2-> nstate)
c input: (r*8) cfion(,,)  = ionis. rate coefft. matrix between adjacent
c                           superstage partitions
c                           1st dim: index of ionising sstage part.
c                           2nd dim: index of ionised sstage part.
c                           3rd dim: index of sstage (1->nstate-1)
c input: (i*4) jcfion()   = scaling for ionis. rate coefft. matrix
c                           between adjacent superstages
c                           1st dim: index of sstage (1-> nstate-1)
c input: (r*8) cfmet(,,)  = x-coupl. rate coefft. matrix within
c                           partitions of a superstage
c                           1st dim: index of lower sstage part.
c                           2nd dim: index of upper sstage part.
c                           3rd dim: index of sstage (1->nstate)
c input: (i*4) jcfmet()   = scaling for x-coupl. rate coefft. matrix
c                           within a superstage
c                           1st dim: index of sstage (1-> nstate)
c
c output:(r*8) popf()     = normalised populations of partitions
c                           1st dim: index of partition (1->nmsum)
c output:(i*4) jpopf()    = power of 10 scale factor for populations
c                           (common for each partition of superstage)
c                           1st dim: index of superstage (1->nstate)
c output:(l*8) lagain     = .true. => id on input incorrect - rerun
c                         = .false.=> id on input ok - no rerun
c
c routines :
c           routine     source  brief description
c           ___________________________________________________________
c           dxmadd_sc   adas    matrix add/subtract with scaling
c           dxmmul_sc   adas    matrix multiply with scaling
c           dxmatr_sc   adas    matrix scale
c           i4unit      adas    fetch unit for output of messages
c           xxminv      adas    matrix invert/linear equation solve
c
c
c  author:  H. P. Summers, University of Strathclyde
c           JA7.08
c           Extn. 4196
c
c  date:    19/04/2006
c
c
c  Version : 1.1
c  Date    : 03-01-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 03-06-2009
c  Modified: Martin O'Mullane
c            - Trap for 1x1 matrices which are zero being passed to
c              xxminv. Set subsequent value to 1.0.
c
c-----------------------------------------------------------------------
       integer idstat , idmet , ndone
       real*8  acc
c-----------------------------------------------------------------------
       parameter ( idstat = 92 , idmet = 10 , acc = 1.0d-06 )
       parameter ( ndone = 1)
c-----------------------------------------------------------------------
       integer i4unit
       integer ndstat  , ndmet  , ndcnct , id     , iagain
       integer nmetz   , nposx
       integer nstate  , nmsum
       integer i       , j      , k      , im
       integer nrc     , ncc
       integer jxtemp  , jytemp , jrhs   , jsum
c-----------------------------------------------------------------------
       real*8  sum    , dint    , ratio
c-----------------------------------------------------------------------
       logical lsolve , lagain
c-----------------------------------------------------------------------
       integer nmet(ndcnct)
       integer jcfrec(ndstat),jcfion(ndstat),jcfmet(ndstat)
       integer jpopf(nstate)
       integer jcpopn(idstat+1),jpopn(idstat+1)
       integer jcpopnd(idstat+1),
     &         jcpopnz(idstat+1)
       integer jpopnmo(idstat+1),
     &         jpopnpo(idstat+1)
       integer jcfmet_s(idstat)
c-----------------------------------------------------------------------
       real*8  cfrec(ndmet,ndmet,ndstat)
       real*8  cfion(ndmet,ndmet,ndstat),cfmet(ndmet,ndmet,ndstat)
       real*8  popf(nmsum)
       real*8  cpopn(idmet,idmet,idstat+1),popn(idmet,ndone,idstat+1)
       real*8  cpopnd(idmet,idmet,idstat+1),
     &         cpopnz(idmet,idmet,idstat+1)
       real*8  popnmo(idmet,ndone,idstat+1),
     &         popnpo(idmet,ndone,idstat+1)
       real*8  solve(2*idmet-1,2*idmet-1)
       real*8  xtemp(idmet,idmet),ytemp(idmet,idmet),rdum(idmet)
       real*8  rhs(2*idmet-1)
       real*8  cfmet_s(idmet,idmet,idstat)
c------------------------------------------------------------------------

c-----------------------------------------------------------------------
c   check internal dimensions
c-----------------------------------------------------------------------
       if(idstat.ne.ndstat) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idstat = ',idstat,' .ne. ndstat = ',
     &                          ndstat
         write(i4unit(-1),2001)
         stop
       endif

       if(idmet.ne.ndmet) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idmet = ',idmet,' .ne. ndmet = ',
     &                          ndmet
         write(i4unit(-1),2001)
         stop
       endif
c-----------------------------------------------------------------------
c calculation of coefficients of metastable population state equations
c zero all arrays to avoid carry forward
c-----------------------------------------------------------------------

       do i=1,nmsum
         popf(i)=0.0d0
       enddo
       do i=1,nstate
	 jpopf(i)=0
       enddo

       do i = 1,ndstat+1
         do j = 1,ndmet
           do k = 1,ndmet
               cpopnd(j,k,i) = 0.0d0
               cpopn(j,k,i) = 0.0d0
               cpopnz(j,k,i) = 0.0d0
           end do
         end do
         jcpopnd(i) = 0
         jcpopn(i) = 0
         jcpopnz(i) = 0
       end do

       do k = 1,ndstat+1
         do j = 1,ndmet
           popnmo(j,ndone,k) = 0.0d0
           popnpo(j,ndone,k) = 0.0d0
         end do
         jpopnmo(k) = 0
         jpopnpo(k) = 0
       end do

       do j = 1,ndmet
         do k = 1,ndmet
           xtemp(j,k) = 0.0d0
           ytemp(j,k) = 0.0d0
         end do
       end do

       jxtemp = 0
       jytemp = 0

       call dxmatr_sc( ndmet,
     &                 cfmet(1,1,1), jcfmet(1) , nmet(1)  ,nmet(1)
     &                )

c-------------------------------------------------
c  save scaled initial values of cfmet and jcfmet
c-------------------------------------------------

       do j=1,nmet(1)
         do k=1,nmet(1)
	   cfmet_s(j,k,1)=cfmet(j,k,1)
	 enddo
       enddo

       jcfmet_s(1)=jcfmet(1)

       do i=1,nmet(1)
	 do j=1,nmet(1)
	   xtemp(i,j)=cfmet(i,j,1)
	 enddo
       enddo

       jxtemp=jcfmet(1)

       lsolve = .false.
       call xxminv(lsolve,ndmet,nmet(1),cfmet(1,1,1),rdum,dint)
       
       jcfmet(1)=-jcfmet(1)

       call dxmmul_sc( ndmet,
     &                 xtemp(1,1)   , jxtemp    , nmet(1) , nmet(1) ,
     &                 cfmet(1,1,1) , jcfmet(1) , nmet(1) , nmet(1) ,
     &                 ytemp(1,1)   , jytemp    , nrc     , ncc
     &                 )

       call dxmatr_sc( ndmet,
     &                 cfrec(1,1,2), jcfrec(2) , nmet(1)  ,nmet(2)
     &                )

       call dxmmul_sc( ndmet,
     &                 cfmet(1,1,1) , jcfmet(1) , nmet(1) , nmet(1) ,
     &                 cfrec(1,1,2) , jcfrec(2) , nmet(1) , nmet(2) ,
     &                 cpopn(1,1,2) , jcpopn(2) , nrc     , ncc
     &                 )

       do j = 1,nmet(1)
         do k = 1,nmet(2)
           cpopnd(j,k,2) = cpopn(j,k,2)
         end do
       end do

       jcpopnd(2) = jcpopn(2)

c-----------------------------------------------------------------------

       if(id .ge. 2)then

          do 20 i = 2,id

            call dxmatr_sc( ndmet,
     &                      cfion(1,1,i-1), jcfion(i-1) , nmet(i)  ,
     &                      nmet(i-1)
     &                    )

            call dxmmul_sc(ndmet,
     &                     cfion(1,1,i-1)    , jcfion(i-1) ,
     &                     nmet(i)           , nmet(i-1)   ,
     &                     cpopnd(1,1,i)     , jcpopnd(i)  ,
     &                     nmet(i-1)         , nmet(i)     ,
     &                     xtemp             , jxtemp      ,
     &                     nrc               , ncc
     &                    )

            call dxmatr_sc( ndmet,
     &                      cfmet(1,1,i), jcfmet(i) , nmet(i)  ,nmet(i)
     &                     )
c-------------------------------------------------
c  save scaled initial values of cfmet and jcfmet
c-------------------------------------------------

            do j=1,nmet(i)
              do k=1,nmet(i)
	        cfmet_s(j,k,i)=cfmet(j,k,i)
	      enddo
            enddo

            jcfmet_s(i)=jcfmet(i)

            call dxmadd_sc(ndmet,
     &                     cfmet(1,1,i) , jcfmet(i)  ,
     &                     nmet(i)      , nmet(i)    ,
     &                     xtemp        , jxtemp     ,
     &                     nmet(i)      , nmet(i)    ,
     &                     ytemp        , jytemp     ,
     &                     nrc          , ncc        ,
     &                     1.0d0        , 0          ,
     &                    -1.0d0        , 0
     &                    )

            if (nmet(i).eq.1.and.ytemp(1,1).eq.0.0D0) then
               ytemp(1,1) = 1.0
            else
               lsolve = .false.
               call xxminv(lsolve,ndmet,nmet(i),ytemp,rdum,dint)
            endif

	    jytemp=-jytemp

            call dxmatr_sc( ndmet,
     &                      cfrec(1,1,i+1), jcfrec(i+1) , nmet(i)  ,
     &                      nmet(i+1)
     &                     )

            call dxmmul_sc(ndmet,
     &                     ytemp            , jytemp       ,
     &                     nmet(i),nmet(i)  ,
     &                     cfrec(1,1,i+1)   , jcfrec(i+1)  ,
     &                     nmet(i),nmet(i+1),
     &                     cpopn(1,1,i+1   ), jcpopn(i+1)  ,
     &                     nrc              , ncc
     &                    )
c-----------------------------------------------------------------------
            do j = 1,nmet(i)
              do k = 1,nmet(i+1)
                cpopnd(j,k,i+1) = cpopn(j,k,i+1)
              end do
            end do

            jcpopnd(i+1)=jcpopn(i+1)

 20      continue
       endif
c-----------------------------------------------------------------------

       call dxmatr_sc( ndmet,
     &                 cfmet(1,1,nstate), jcfmet(nstate) ,
     &                 nmet(nstate)     , nmet(nstate)
     &                )

c-------------------------------------------------
c  save scaled initial values of cfmet and jcfmet
c-------------------------------------------------

       do j=1,nmet(nstate)
         do k=1,nmet(nstate)
	   cfmet_s(j,k,nstate)=cfmet(j,k,nstate)
	 enddo
       enddo

       jcfmet_s(nstate)=jcfmet(nstate)

       do i=1,nmet(nstate)
	 do j=1,nmet(nstate)
	   xtemp(i,j)=cfmet(i,j,nstate)
	 enddo
       enddo

       jxtemp=jcfmet(nstate)

       lsolve = .false.
       call xxminv(lsolve,ndmet,nmet(nstate),cfmet(1,1,nstate),
     &             rdum,dint)

       jcfmet(nstate)=-jcfmet(nstate)

       call dxmmul_sc( ndmet,
     &                 xtemp(1,1)        , jxtemp        ,
     &                 nmet(nstate)      , nmet(nstate)  ,
     &                 cfmet(1,1,nstate) , jcfmet(nstate),
     &                 nmet(nstate)      , nmet(nstate)  ,
     &                 ytemp(1,1)        , jytemp        ,
     &                 nrc               , ncc
     &                 )

       call dxmatr_sc( ndmet,
     &                 cfion(1,1,nstate-1), jcfion(nstate-1) ,
     &                 nmet(nstate)       , nmet(nstate-1)
     &               )

       call dxmmul_sc(ndmet,
     &                cfmet(1,1,nstate)   , jcfmet(nstate)   ,
     &                nmet(nstate)        , nmet(nstate)     ,
     &                cfion(1,1,nstate-1) , jcfion(nstate-1) ,
     &                nmet(nstate)        , nmet(nstate-1)   ,
     &                cpopn(1,1,nstate+1) , jcpopn(nstate+1) ,
     &                nrc                 , ncc
     &               )

       do j = 1,nmet(nstate)
         do k = 1,nmet(nstate-1)
           cpopnz(j,k,nstate+1) = cpopn(j,k,nstate+1)
         end do
       end do

       jcpopnz(nstate+1) = jcpopn(nstate+1)

c-----------------------------------------------------------------------
       if (id+1 .le.  nstate-1)then

         do 30 i = nstate-1,id+1,-1

           call dxmatr_sc( ndmet,
     &                     cfrec(1,1,i+1), jcfrec(i+1) , nmet(i)  ,
     &                     nmet(i+1)
     &                   )

           call dxmmul_sc(ndmet,
     &                    cfrec(1,1,i+1)  , jcfrec(i+1)  ,
     &                    nmet(i)         , nmet(i+1)    ,
     &                    cpopnz(1,1,i+2) , jcpopnz(i+2) ,
     &                    nmet(i+1)       , nmet(i)      ,
     &                    xtemp           , jxtemp       ,
     &                    nrc             , ncc
     &                   )

           call dxmatr_sc( ndmet,
     &                     cfmet(1,1,i), jcfmet(i) , nmet(i)  ,nmet(i)
     &                   )

c-------------------------------------------------
c  save scaled initial values of cfmet and jcfmet
c-------------------------------------------------
           do j=1,nmet(i)
             do k=1,nmet(i)
	       cfmet_s(j,k,i)=cfmet(j,k,i)
	     enddo
           enddo

           jcfmet_s(i)=jcfmet(i)

           call dxmadd_sc(ndmet,
     &                    cfmet(1,1,i),jcfmet(i), nmet(i) ,nmet(i),
     &                    xtemp       ,jxtemp   , nmet(i) ,nmet(i),
     &                    ytemp       ,jytemp   , nrc     ,ncc    ,
     &                    1.0d0       , 0       ,-1.0d0   , 0
     &                   )

           lsolve = .false.
           if (nmet(i).eq.1.and.ytemp(1,1).eq.0.0D0) then
              ytemp(1,1) = 1.0
           else
              call xxminv(lsolve,ndmet,nmet(i),ytemp,rdum,dint)
           endif

	   jytemp=-jytemp

           call dxmatr_sc( ndmet,
     &                     cfion(1,1,i-1), jcfion(i-1) ,
     &                     nmet(i)       , nmet(i-1)
     &                   )

           call dxmmul_sc(ndmet,
     &                    ytemp          , jytemp      ,
     &                    nmet(i)        , nmet(i)     ,
     &                    cfion(1,1,i-1) , jcfion(i-1) ,
     &                    nmet(i)        , nmet(i-1)   ,
     &                    cpopn(1,1,i+1) , jcpopn(i+1) ,
     &                    nrc            , ncc
     &                   )

c-----------------------------------------------------------------------
           do j = 1,nmet(i)
             do k = 1,nmet(i-1)
               cpopnz(j,k,i+1) = cpopn(j,k,i+1)
             end do
           end do

           jcpopnz(i+1) = jcpopn(i+1)

 30      continue

       endif

c-----------------------------------------------------------------------
c fill solve matrix
c-----------------------------------------------------------------------

       nmetz = nmet(id)+nmet(id+1)-1
       nposx = nmet(id)
       jrhs=0

       do j = 1,2*idmet-1
          rhs(j) = 0.0d0
          do k = 1,2*idmet-1
            solve(j,k) = 0.0d0
          end do
          solve(j,j) = 1.0d0
	  rhs(j)=0.0d0
       end do

       do j = nposx,nmetz
         rhs(j) = -cpopn(j-nposx+1,1,id+2)*10.0d0**jcpopn(id+2)
         if (nposx .gt. 1)then
           do k = 1,nposx-1
             solve(j,k) = cpopn(j-nposx+1,k+1,id+2)*
     &                    10.0d0**jcpopn(id+2)
           end do
         endif
       end do

       if (nposx .gt. 1)then
         do j = 1,nposx-1
           do k = nposx,nmetz
             solve(j,k) = cpopn(j+1,k-nposx+1,id+1)*
     &                    10.0d0**jcpopn(id+1)
           end do
         end do
       endif

c-----------------------------------------------------------------------
c normalization of popn(id+1,j,ndone)
c-----------------------------------------------------------------------

       lsolve = .true.
       call xxminv(lsolve,2*ndmet-1,nmetz,solve,rhs,dint)

       popn(1,ndone,id+1) = 1.0d0
       jpopn(id+1)=0

       if(nposx.ge.2)then
         do j = 2,nposx
           popn(j,ndone,id+1) =  rhs(j-1)
         end do
         jpopn(id+1)=jrhs
       endif

c-----------------------------------------------------------------------
c solving backwards then forwards through values of popn
c-----------------------------------------------------------------------
       do j = 1,nmet(id)
         popnpo(j,ndone,id+1) = popn(j,ndone,id+1)
       end do
       jpopnpo(id+1) = jpopn(id+1)

c-----------------------------------------------------------------------
       do i = id,2,-1

         call dxmadd_sc(ndmet,
     &                  cpopn(1,1,i),jcpopn(i), nmet(i-1),nmet(i),
     &                  cpopn(1,1,i),jcpopn(i), nmet(i-1),nmet(i),
     &                  cpopn(1,1,i),jcpopn(i), nrc      ,ncc    ,
     &                  0.0d0       , 0       ,-1.0d0   , 0
     &                 )

         call dxmmul_sc( ndmet,
     &                   cpopn(1,1,i)    , jcpopn(i)    ,
     &                   nmet(i-1)       , nmet(i)      ,
     &                   popnpo(1,1,i+1) , jpopnpo(i+1) ,
     &                   nmet(i)         , ndone        ,
     &                   xtemp           , jxtemp       ,
     &                   nrc             , ncc
     &                 )

         do j = 1,nmet(i-1)
           popnpo(j,ndone,i) = xtemp(j,ndone)
         end do
         jpopnpo(i)=jxtemp

       end do
c-----------------------------------------------------------------------

       do j = 1,nmet(id)
         popnmo(j,ndone,id+1) = popn(j,ndone,id+1)
       end do

       jpopnmo(id+1)=jpopn(id+1)
c-----------------------------------------------------------------------

       if (id+1 .le. nstate)then

         do i = id+1,nstate

           call dxmadd_sc(ndmet,
     &                    cpopn(1,1,i+1),jcpopn(i+1),nmet(i),nmet(i-1),
     &                    cpopn(1,1,i+1),jcpopn(i+1),nmet(i),nmet(i-1),
     &                    cpopn(1,1,i+1),jcpopn(i+1), nrc   ,ncc      ,
     &                    0.0d0         , 0         ,-1.0d0 ,0
     &                   )

           call dxmmul_sc(ndmet,
     &                    cpopn(1,1,i+1),jcpopn(i+1),nmet(i),nmet(i-1),
     &                    popnmo(1,1,i) ,jpopnmo(i) ,nmet(i-1),ndone,
     &                    ytemp, jytemp ,nrc        ,ncc
     &                   )

           do j = 1,nmet(i)
             popnmo(j,ndone,i+1) = ytemp(j,ndone)
           end do

           jpopnmo(i+1)=jytemp

         end do
       endif

c-----------------------------------------------------------------------
c loop to sum all values of popn
c-----------------------------------------------------------------------
       do i= 1,id+1
         do j = 1,ndmet
           popn(j,ndone,i) = popnpo(j,ndone,i)
         end do
         jpopn(i)=jpopnpo(i)
       end do

       do i = id+2,nstate+1
         do j = 1,ndmet
           popn(j,ndone,i) = popnmo(j,ndone,i)
         end do
         jpopn(i)=jpopnmo(i)
       end do

       iagain = id

c-----------------------------------------------------------------------
c mark dominant stage-1 as iagain to compare with id
c ---------- modify this loop from i=1,nstate+1 to i=1,nstate  06/07/95
c-----------------------------------------------------------------------

       do i = 1,nstate

         if(popn(1,ndone,i).gt.popn(1,ndone,iagain+1)*
     &      10.0d0**(jpopn(iagain+1)-jpopn(i)))then
             iagain = i-1
         endif

       end do

c-----------------------------------------------------------------------
c flag to recalculate populations if initial id was not the
c best possible choice of dominant stage.
c-----------------------------------------------------------------------

       if(.not.lagain)then
         if(iagain.ne.id)then
 80        ratio = popn(1,ndone,iagain+2)/popn(1,ndone,iagain+1)*
     &            10.0d0**(jpopn(iagain+2)-jpopn(iagain+1))
           if(ratio.lt.acc)then
              iagain = iagain-1
              goto 80
           endif
           if(iagain.ne.id)then
             lagain = .true.
             id = iagain
           endif
         else
           lagain = .false.
         endif
       else
         lagain = .false.
       endif

       if(.not.lagain)then

         sum = 0.0d0
         jsum=jpopn(id)
         do i = 2,nstate+1
           do j = 1,ndmet
             sum = sum+popn(j,ndone,i)*10.0d0**(jpopn(i)-jsum)
           end do
          end do
c-----------------------------------------------------------------------
c output of results
c-----------------------------------------------------------------------
          im = 1

          do i = 2,nstate+1
            do j =1,nmet(i-1)
              popf(im) = popn(j,ndone,i)/sum
              im = im+1
            end do
            jpopf(i-1)= jpopn(i)-jsum
          end do

          popn(ndmet,ndone,nstate+1) = 0.0d0
          jpopn(nstate+1)=0

       endif

c-----------------------------------------------------------------------
c  restore initial values of cfmet and jcfmet
c-----------------------------------------------------------------------
       do i=1,ndstat
         do j=1,ndmet
  	   do k=1,ndmet
	     cfmet(j,k,i)=cfmet_s(j,k,i)
	   enddo
	 enddo
	 jcfmet(i)=jcfmet_s(i)
       enddo
       return
c
c-----------------------------------------------------------------------
c
 2000 format(1x,30('*'),' dgmfsp_sc error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
