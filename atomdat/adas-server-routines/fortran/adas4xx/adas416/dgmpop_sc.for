      subroutine dgmpop_sc( ndclas   , isdimd   , izdimd   , ipdimd    ,
     &                      ndcnct   ,
     &                      ncnct_p  , icnctv_p ,
     &                      iblmxa_p , ispbra_p , isppra_p , isstgra_p ,
     &                      dgcr_p   , npsum    ,
     &                      popf     , jpopf    ,
     &                      crmat    , jcrmat   , crccd    , jcrccd    ,
     &                      crprb    , jcrprb   , crprc    , jcrprc    ,
     &                      crplt    , jcrplt   , crpls    , jcrpls    ,
     &                      crzcd    , crycd    , crecd
     &                    )
      implicit none

c-----------------------------------------------------------------------
c
c  ****************** fortran 77 subroutine: dgmpop ********************
c
c  purpose: calculation of partition resolved superstage populations
c           of a particular element for a given temperature and density.
c
c  notes:   populations are normalised to unity within a superstage and
c           used to form GCR coefficients for a child partition
c
c  calling program: adas416
c
c  subroutine:
c
c
c  input : (i*4)  ndclas     = number of adf11 master file classes
c  input : (i*4)  isdimd     = maximum number of (sstage, parent, base)
c                              blocks in isonuclear master files
c  input : (i*4)  izdimd     = maximum number of superstages
c                              in isonuclear master files
c  input : (i*4)  ipdimd     = maximum number of partition
c                              in a superstage
c  input : (i*4)  ndcnct     = maximum number of elements in connection
c                              vector
c
c  input : (i*4)  ncnct_p    = number of elements in connection vector
c  input : (i*4)  icnctv_p() = connection vector of number of partitions
c                              of each superstage in resolved case
c                              including the bare nucleus
c                              1st dim: connection vector index
c
c  input : (i*4)  iblmxa_p() = number of (sstage, parent, base)
c                              blocks in isonuclear master file class
c                              1st dim: adf11 class index
c  input : (i*4)  isppra_p(,)= 1st (parent) index for each partition block
c                              1st dim: adf11 class index
c                              2nd dim: index of (sstage, parent, base)
c                                   block in isonuclear master file
c  input : (i*4)  ispbra_p(,)= 2nd (base) index for each partition block
c                              1st dim: adf11 class index
c                              2nd dim: index of (sstage, parent, base)
c                                   block in isonuclear master file
c  input : (i*4)  isstgra_p()= s1 for each resolved data block
c                              (generalises to connection vector index)
c                              1st dim: adf11 class index
c                              2nd dim: index of (sstage, parent, base)
c                                    block in isonuclear master file
c  input : (r*8)  dgcr_p(,)  = interpolation of prc coefficient (w cm3 )
c                              1st dim: adf11 class index
c                              2nd dim: index of (sstage, parent, base)
c                                       block in isonuclear master file
c  input : (i*4)  npsum      = number of superstage/partitions
c
c  output: (r*8)  popf()     = scaled populations for a specified
c                              temp./density
c                              1st dim: superstage/partition list index
c  output:(i*4)   jpopf()    = power of 10 scale factor for populations
c                              (common for each partition of superstage)
c                              1st dim: index of superstage (1->nstate)
c  output: (r*8)  crmat(,)   = collisional-radiative matrix
c                              1st dim: superstage/partition list index
c                              2nd dim: superstage/partition list index
c  output:(i*4)   jcrmat(,)  = power of 10 scale factor for sub-matrices
c                              (common for each superstage sub-block)
c                              1st dim: index of superstage (1->nstate)
c                              2nd dim: index of superstage (1->nstate)
c  output: (r*8)  crccd(,)   = cx recombination rate coefficients matrix
c                              1st dim: superstage/partition list index
c                              2nd dim: superstage/partition list index
c  output:(i*4)   jcrccd(,)  = power of 10 scale factor for sub-matrices
c                              (common for each superstage sub-block)
c                              1st dim: index of superstage (1->nstate)
c                              2nd dim: index of superstage (1->nstate)
c  output: (r*8)  crprb()    = recombination/bremsstrahlung power vector
c                              1st dim: superstage/partition list index
c  output:(i*4)   jcrprb()  = power of 10 scale factor for sub-vectors
c                              (common for each superstage sub-block)
c                              1st dim: index of superstage (1->nstate)
c  output: (r*8)  crprc()    = cx recombination power vector
c                              1st dim: superstage/partition list index
c  output:(i*4)   jcrprc()  = power of 10 scale factor for sub-vectors
c                              (common for each superstage sub-block)
c                              1st dim: index of superstage (1->nstate)
c  output: (r*8)  crplt()    = low level line power vector
c                              1st dim: superstage/partition list index
c  output:(i*4)   jcrplt()  = power of 10 scale factor for sub-vectors
c                              (common for each superstage sub-block)
c                              1st dim: index of superstage (1->nstate)
c  output: (r*8)  crpls()    = special select line power vector
c                              1st dim: superstage/partition list index
c  output:(i*4)   jcrpls()  = power of 10 scale factor for sub-vectors
c                              (common for each superstage sub-block)
c                              1st dim: index of superstage (1->nstate)
c  output: (r*8)  crzcd()    = effective charge vector
c                              1st dim: superstage/partition list index
c  output: (r*8)  crycd()    = effective squared charge vector
c                              1st dim: superstage/partition list index
c  output: (r*8)  crecd(,)   = effective ionis/excit potential matrix
c                              1st dim: superstage/partition list index
c                              2nd dim: superstage/partition list index
c
c        (i*4) id            = position of dominant term
c        (i*4) isstg         = superstage index
c        (i*4) item          = general index
c        (i*4) i             = general index
c        (i*4) j             = general index
c        (i*4) k             = general index
c        (r*8) ymin          = value of difference between
c                              recombination and ionisation coefficients
c                              of ground ipdimd
c
c  routines:
c           routine    source    brief description
c           -------------------------------------------------------------
c           dgdiag_sc  adas      sets on-diag. element of scaled matrix
c           dgmfsp_sc  adas      executes sstage partition matrix soln.
c           i4unit     adas      fetch unit for output of messages
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    19/04/06
c
c
c  Version : 1.1
c  Date    : 09-05-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 09-04-2010
c  Modified: Martin O'Mullane
c            - Redefine iexp_min to be 2 less than -2**31 to fix a
c              gfortran overflow problem at compilation.
c
c------------------------------------------------------------------------
       integer   nsdim   , nzdim    , npdim
       integer   iexp_min
c------------------------------------------------------------------------
       parameter ( nsdim = 200 , nzdim = 92 , npdim = 10 )
       parameter ( iexp_min=-2*(2**30-1) )
c------------------------------------------------------------------------
       integer   i4unit
       integer   ndclas
       integer   izdimd  , isdimd   , ipdimd
       integer   ndcnct
       integer   ncnct_p
       integer   npsum
       integer   iclass  , isstg    , jsstg    , nsstg
       integer   i       , j        , k
       integer   ir      , jr       , id
c------------------------------------------------------------------------
       logical   lagain
c------------------------------------------------------------------------
       real*8    ymin
c------------------------------------------------------------------------
       integer   icnctv_p(ndcnct)
       integer   iblmxa_p(ndclas)
       integer   ispbra_p(ndclas,isdimd)  , isppra_p(ndclas,isdimd) ,
     &           isstgra_p(ndclas,isdimd)
       integer   jpopf(izdimd)
       integer   jcfrec(nzdim) , jcfion(nzdim) ,jcfmet(nzdim)       ,
     &           jcfccd(nzdim) , jcfprb(nzdim) ,jcfprc(nzdim)       ,
     &           jcfplt(nzdim) , jcfpls(nzdim)
       integer   jcrmat(izdimd,izdimd) , jcrccd(izdimd,izdimd)      ,
     &           jcrprb(izdimd)        , jcrprc(izdimd)             ,
     &           jcrplt(izdimd)        , jcrpls(izdimd)
c------------------------------------------------------------------------
      real*8     dgcr_p(ndclas,isdimd)

      real*8     cfrec(npdim,npdim,nzdim) , cfion(npdim,npdim,nzdim) ,
     &           cfmet(npdim,npdim,nzdim) , cfccd(npdim,npdim,nzdim)
      real*8     cfprb(npdim,nzdim)       , cfprc(npdim,nzdim)       ,
     &           cfplt(npdim,nzdim)       , cfpls(npdim,nzdim),
     &           cfzcd(npdim,nzdim)       , cfycd(npdim,nzdim)
      real*8     ytem(nzdim)
      real*8     popf(isdimd+1)
      real*8     crmat(isdimd+1,isdimd+1) , crccd(isdimd+1,isdimd+1)
      real*8     crprb(isdimd+1)          , crprc(isdimd+1)          ,
     &           crplt(isdimd+1)          , crpls(isdimd+1)          ,
     &           crzcd(isdimd+1)          , crycd(isdimd+1)          ,
     &           crecd(isdimd+1,isdimd+1)
      real*8     cfien(npdim,npdim,nzdim)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c   check internal dimensions
c-----------------------------------------------------------------------
       if(isdimd.ne.nsdim) then
         write(i4unit(-1),2002)'mismatch of internal dimensions: ',
     &                         'isdimd = ',isdimd,' .ne. nsdim = ',
     &                          nsdim
         write(i4unit(-1),2003)
         stop
       endif

       if(izdimd.ne.nzdim) then
         write(i4unit(-1),2002)'mismatch of internal dimensions: ',
     &                         'izdimd = ',izdimd,' .ne. nzdim = ',
     &                          nzdim
         write(i4unit(-1),2003)
         stop
       endif

       if(ipdimd.ne.npdim) then
         write(i4unit(-1),2002)'mismatch of internal dimensions: ',
     &                         'ipdimd = ',ipdimd,' .ne. npdim = ',
     &                          npdim
         write(i4unit(-1),2003)
         stop
       endif

c
c------------------------------------------------------------------------
c initialisation
c------------------------------------------------------------------------
c
      lagain = .false.

 1    continue

      do i=1,isdimd+1
        popf(i)=0.0d0
      enddo
      do i=1,izdimd
        jpopf(i)=0
      enddo

      do i=1,izdimd
        jcfion(i)=0
        jcfrec(i)=0
        jcfmet(i)=0
        jcfccd(i)=0
        do j=1,ipdimd
          cfprb(j,i)=0.0d0
          cfprc(j,i)=0.0d0
          cfplt(j,i)=0.0d0
          cfpls(j,i)=0.0d0
          cfzcd(j,i)=0.0d0
          cfycd(j,i)=0.0d0
          do k=1,ipdimd
               cfion(j,k,i)=0.0d0
               cfrec(j,k,i)=0.0d0
               cfmet(j,k,i)=0.0d0
               cfccd(j,k,i)=0.0d0
               cfien(j,k,i)=0.0d0
          end do
        end do
      end do

c------------------------------------------------------------------------
c  fill successively recombination, ionisation and excitation arrays
c  note that charge exchange recombination is not included
c------------------------------------------------------------------------
       nsstg = ncnct_p

       do isstg = 1,nsstg

         iclass = 1
         jcfrec(isstg+1) = iexp_min
         do ir= 1,iblmxa_p(iclass)
           if(isstgra_p(iclass,ir).eq.isstg) then
               jcfrec(isstg+1) = max0(int(dgcr_p(iclass,ir)),
     &                                       jcfrec(isstg+1))
           endif
         enddo

         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 10
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=ispbra_p(iclass,ir)
               k=isppra_p(iclass,ir)
               cfrec(j,k,isstg+1) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                       jcfrec(isstg+1))
           endif

         enddo


   10    iclass = 2
         jcfion(isstg) = iexp_min
         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).eq.isstg) then
               jcfion(isstg) = max0(int(dgcr_p(iclass,ir)),
     &                                       jcfion(isstg))
           endif

         enddo

         do ir= 1,iblmxa_p(iclass)

            if(isstgra_p(iclass,ir).gt.isstg) then
                go to 20
            elseif(isstgra_p(iclass,ir).eq.isstg) then
                j=ispbra_p(iclass,ir)
                k=isppra_p(iclass,ir)
                cfion(j,k,isstg) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                        jcfion(isstg))
            endif

         enddo

   20    iclass = 6
         jcfmet(isstg) = iexp_min

         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).eq.isstg) then
               jcfmet(isstg) = max0(int(dgcr_p(iclass,ir)),
     &                                       jcfmet(isstg))
           endif

         enddo

         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 30
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=ispbra_p(iclass,ir)
               k=isppra_p(iclass,ir)
               cfmet(j,k,isstg) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                       jcfmet(isstg))
           endif

         enddo

   30    iclass = 7
         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 40
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=ispbra_p(iclass,ir)
               k=isppra_p(iclass,ir)
               cfmet(j,k,isstg-1) = cfmet(j,k,isstg-1) +
     &                              10.0d0**(dgcr_p(iclass,ir)-
     &                              jcfmet(isstg-1))
           endif
         enddo

   40    continue

       enddo


       do isstg = 0,nsstg

         iclass = 12

         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 45
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=ispbra_p(iclass,ir)
               k=isppra_p(iclass,ir)
               cfien(j,k,isstg+1) = dgcr_p(iclass,ir)
           endif

         enddo

   45    continue

       enddo

c------------------------------------------------------------------------
c finding then marking dominant term
c------------------------------------------------------------------------

      if(.not.lagain) then

        ymin = 1.00d+10
        id = 1
        if (nsstg  .gt.  2)then

          do i=2,nsstg-1
             if(cfrec(1,1,i+1).eq.0.0d0.and.
     &          cfion(1,1,i-1).ne.0.0d0)then
                ytem(i)=abs(dlog(cfion(1,1,i-1))+jcfion(i-1))
             elseif(cfion(1,1,i-1).eq.0.0d0.and.
     &              cfrec(1,1,i+1).ne.0.0d0)then
                ytem(i)=abs(dlog(cfrec(1,1,i+1))+jcfrec(i+1))
             elseif(cfion(1,1,i-1).eq.0.0d0.and.
     &              cfrec(1,1,i+1).eq.0.0d0)then
                ytem(i)=ymin
             else
                ytem(i)=abs(dlog(cfrec(1,1,i+1))-dlog(cfion(1,1,i-1))
     &                           +jcfrec(i+1)-jcfion(i-1))
             endif
          end do


          do i = 2,nsstg-1
             if (ytem(i)  .le.  ymin)then
                ymin = ytem(i)
                id = max(1,i-3)
             endif
          end do

        endif

      endif

c------------------------------------------------------------------------
c  calculate  prime diagonal
c------------------------------------------------------------------------

      call dgdiag_sc( nzdim  , npdim     , ndcnct ,
     &                ncnct_p, icnctv_p  ,
     &                cfrec  , jcfrec    ,
     &                cfion  , jcfion    ,
     &                cfmet  , jcfmet
     &              )

c------------------------------------------------------------------------
c  create the collisional-radiative matrix crmat for return to adas416
c------------------------------------------------------------------------

      do ir=1,npsum
        do jr=1,npsum
          crmat(ir,jr)=0.0d0
        enddo
      enddo

      do isstg=1,nsstg
        do jsstg = 1,nsstg
          jcrmat(isstg,jsstg)=0
        enddo
      enddo

      ir = 0
      jr = 0

      do isstg=1,nsstg
        do i=1,icnctv_p(isstg)
          do j=1,icnctv_p(isstg)
            crmat(ir+i,jr+j)=cfmet(i,j,isstg)
          enddo
          if(isstg.lt.nsstg) then
              do j=1,icnctv_p(isstg+1)
                crmat(ir+i,jr+icnctv_p(isstg)+j)=cfrec(i,j,isstg+1)
c               jcrmat(isstg,isstg)=jcfmet(isstg)
              enddo
          endif
          if(isstg.gt.1) then
              do j=1,icnctv_p(isstg-1)
                crmat(ir+i,jr-icnctv_p(isstg-1)+j)=cfion(i,j,isstg-1)
              enddo
          endif
        enddo
        ir=ir+icnctv_p(isstg)
        jr=jr+icnctv_p(isstg)

        jcrmat(isstg,isstg)=jcfmet(isstg)
        if(isstg.lt.nsstg) then
           jcrmat(isstg,isstg+1)=jcfrec(isstg+1)
        endif
        if(isstg.gt.1) then
           jcrmat(isstg,isstg-1)=jcfion(isstg-1)
        endif

      enddo

c------------------------------------------------------------------------
c  create the ionis/excit potential matrix crecd for return to adas416
c------------------------------------------------------------------------

      do ir=1,npsum+1
        do jr=1,npsum+1
          crecd(ir,jr)=0.0d0
        enddo
      enddo

      ir = 0
      jr = 1

      isstg = 0
      i=1
      do j=1,icnctv_p(isstg+1)
        crecd(ir+i,jr+j)=cfien(i,j,isstg+1)
      enddo

      ir=ir+i
      jr=jr+icnctv_p(isstg+1)

      do isstg=1,nsstg
        do i=1,icnctv_p(isstg)
          if(isstg.lt.nsstg) then
c--- make the supradiagonal rectangular sub-matrix
              do j=1,icnctv_p(isstg+1)
                crecd(ir+i,jr+j)=cfien(i,j,isstg+1)
              enddo
c--- deduce the on-diagnonal square sub-matrix
              do j=i,icnctv_p(isstg)
                crecd(ir+i,ir+j)=crecd(ir+i-1,jr-icnctv_p(isstg)+j)-
     &                           crecd(ir+i-1,jr-icnctv_p(isstg)+1)
              enddo
              if(i.gt.1) then
                  do j=1,i-1
                     crecd(ir+i,ir+j)=-crecd(ir+j,ir+i)
                  enddo
              endif
          endif
        enddo

        ir=ir+icnctv_p(isstg)
        jr=jr+icnctv_p(isstg)

      enddo


c--- now fill the whole matrix outwards from the supradiagonal
       do ir=2,npsum-1
         do jr=1,npsum+1-ir
           crecd(jr,ir+jr)=crecd(jr,jr+1)+crecd(jr+1,ir+jr)
         enddo
       enddo

       do ir=1,npsum
         do jr=ir+1,npsum
           crecd(jr,ir)=-crecd(ir,jr)
         enddo
       enddo


c-----------------------------------------------------------------------
c  perform the main program algebra
c-----------------------------------------------------------------------

      call dgmfsp_sc( izdimd , ipdimd   , ndcnct   ,
     &                nsstg  , icnctv_p , id       , npsum  ,
     &                cfrec  , jcfrec   , cfion    , jcfion ,
     &                cfmet  , jcfmet   ,
     &                popf   , jpopf    ,
     &                lagain
     &           )

      if(lagain)then
         go to 1
      endif

c------------------------------------------------------------------------
c  prepare the other matrices cfccd
c------------------------------------------------------------------------

      do isstg = 1,nsstg

        iclass = 3
        jcfccd(isstg+1) = iexp_min

        do ir= 1,iblmxa_p(iclass)

          if(isstgra_p(iclass,ir).eq.isstg) then
              jcfccd(isstg+1) = max0(int(dgcr_p(iclass,ir)),
     &                                      jcfccd(isstg+1))
          endif

        enddo

        do ir= 1,iblmxa_p(iclass)
          if(isstgra_p(iclass,ir).gt.isstg) then
              go to 50
          elseif(isstgra_p(iclass,ir).eq.isstg) then
              j=ispbra_p(iclass,ir)
              k=isppra_p(iclass,ir)
              cfccd(j,k,isstg+1) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                      jcfccd(isstg+1))
          endif

        enddo

   50   continue

      enddo

c------------------------------------------------------------------------
c  prepare the vectors cfprb, cfprc, cfplt, cfpls, cfzcd
c------------------------------------------------------------------------

      do isstg = 1,nsstg

        iclass = 4
        jcfprb(isstg) = iexp_min

        do ir= 1,iblmxa_p(iclass)

          if(isstgra_p(iclass,ir).eq.isstg) then
              jcfprb(isstg) = max0(int(dgcr_p(iclass,ir)),
     &                                      jcfprb(isstg))
          endif
        enddo

        do ir= 1,iblmxa_p(iclass)
          if(isstgra_p(iclass,ir).gt.isstg) then
              go to 60
          elseif(isstgra_p(iclass,ir).eq.isstg) then
              j=isppra_p(iclass,ir)
              cfprb(j,isstg) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                      jcfprb(isstg))

          endif
        enddo

   60   iclass = 5
        jcfprc(isstg) = iexp_min

        do ir= 1,iblmxa_p(iclass)

          if(isstgra_p(iclass,ir).eq.isstg) then
              jcfprc(isstg) = max0(int(dgcr_p(iclass,ir)),
     &                                      jcfprc(isstg))
          endif
        enddo

        do ir= 1,iblmxa_p(iclass)
          if(isstgra_p(iclass,ir).gt.isstg) then
              go to 70
          elseif(isstgra_p(iclass,ir).eq.isstg) then
              j=isppra_p(iclass,ir)
              cfprc(j,isstg) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                      jcfprc(isstg))

           endif
         enddo

   70    iclass = 8
         jcfplt(isstg) = iexp_min

         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).eq.isstg) then
               jcfplt(isstg) = max0(int(dgcr_p(iclass,ir)),
     &                                       jcfplt(isstg))
           endif
         enddo

         do ir= 1,iblmxa_p(iclass)
           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 80
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=isppra_p(iclass,ir)
               cfplt(j,isstg) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                       jcfplt(isstg))

           endif
         enddo

   80    iclass = 9
         jcfpls(isstg) = iexp_min

         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).eq.isstg) then
               jcfpls(isstg) = max0(int(dgcr_p(iclass,ir)),
     &                                       jcfpls(isstg))
           endif
         enddo
         do ir= 1,iblmxa_p(iclass)
           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 90
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=isppra_p(iclass,ir)
               cfpls(j,isstg) = 10.0d0**(dgcr_p(iclass,ir)-
     &                                       jcfpls(isstg))

           endif
         enddo

   90    iclass = 10
         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 100
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=isppra_p(iclass,ir)
               cfzcd(j,isstg) = dgcr_p(iclass,ir)
           endif
         enddo

  100    iclass = 11
         do ir= 1,iblmxa_p(iclass)

           if(isstgra_p(iclass,ir).gt.isstg) then
               go to 110
           elseif(isstgra_p(iclass,ir).eq.isstg) then
               j=isppra_p(iclass,ir)
               cfycd(j,isstg) = dgcr_p(iclass,ir)
           endif
         enddo

  110    continue

       enddo

c------------------------------------------------------------------------
c  create the collisional-radiative matrices crccd, cdprb, crprc and
c  the collisional-radiative (row) vectors crplt,crpls,crzcd,crycd for
c  return to adas416
c------------------------------------------------------------------------

      do ir=1,npsum
        crprb(ir)=0.0d0
        crprc(ir)=0.0d0
        crplt(ir)=0.0d0
        crpls(ir)=0.0d0
        crzcd(ir)=0.0d0
        crycd(ir)=0.0d0
        do jr=1,npsum
          crccd(ir,jr)=0.0d0
        enddo
      enddo

      ir = 0
      jr = 0

      do isstg=1,nsstg
        do i=1,icnctv_p(isstg)

          crplt(ir+i)=cfplt(i,isstg)
          crpls(ir+i)=cfpls(i,isstg)
          crzcd(ir+i)=cfzcd(i,isstg)
          crycd(ir+i)=cfycd(i,isstg)

          if(isstg.lt.nsstg) then
              do j=1,icnctv_p(isstg+1)
                crccd(ir+i,jr+icnctv_p(isstg)+j)=cfccd(i,j,isstg+1)
              enddo
          endif
        enddo

        jcrplt(isstg)=jcfplt(isstg)
        jcrpls(isstg)=jcfpls(isstg)
        jcrccd(isstg,isstg+1)=jcfccd(isstg+1)

        ir=ir+icnctv_p(isstg)
        jr=jr+icnctv_p(isstg)
      enddo

      jr = 0

      do isstg=1,nsstg-1
         do j=1,icnctv_p(isstg+1)
            crprb(jr+icnctv_p(isstg)+j)=cfprb(j,isstg)
            crprc(jr+icnctv_p(isstg)+j)=cfprc(j,isstg)
         enddo

        jcrprb(isstg+1)=jcfprb(isstg)
        jcrprc(isstg+1)=jcfprc(isstg)

        jr=jr+icnctv_p(isstg)
      enddo


      return
c
c-----------------------------------------------------------------------
c
 2002 format(1x,29('*'),'    dgmpop_sc error   ',28('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,29('*'),'  program terminated  ',28('*'))
c
c-----------------------------------------------------------------------
c
      end
