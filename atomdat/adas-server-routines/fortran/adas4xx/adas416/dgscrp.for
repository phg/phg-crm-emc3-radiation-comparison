       subroutine dgscrp( ifdimd   , ndstack , iunit     ,
     &                    dsngcr_p , dsna_p  , lfilea_p  , ipl_p   ,
     &                    dsngcr_c , dsna_c  , lfilea_c  , ipl_c   ,
     &                    ndptnl   , ndptn   , ndptnc    ,
     &                    nptnl    , nptn    , nptnc     ,
     &                    iptnla   , iptna   , iptnca    ,
     &                    ncptn_stack        , cptn_stack,
     &                    lres     , elem    , year      , ircode
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: dgscrp  ********************
c
c  purpose: to read script file and access parent emissivity files,
c           partition specification data lines and child output
c           emissivity file names
c
c  calling program: adas416
c
c  subroutine:
c
c  input : (i*4)   ifdimd      = maximum number of emissivity file types
c                                (usually 4 - pec,gtn,f-pec,f-gtn)
c  input : (i*4)   ndstack     = maximum number of partition text lines
c  input : (i*4)   iunit       = unit number of opened file
c  input : (i*4)   ndptnl      = maximum level of partitions
c  input : (i*4)   ndptn       = maximum no. of partitions in one level
c  input : (i*4)   ndptnc      = maximum no. of components in a partition
c
c
c  output: (c*120) dsngcr_p    = parent gcr template file name from script
c  output: (c*120) dsna_p()    = parent emissivity source file template
c                                1st dim: emissivity type
c  output: (l*4)   lfilea_p()  = .true.  => parent emissivity type exists
c                                .false. => file does not exist
c                                1st dim: emissivity type
c  output: (i*4)   ipl_p       = position of parent partition in iptnla
c
c  output: (c*120) dsngcr_c    = child gcr template file name from script
c  output: (c*120) dsna_c()    = child emissivity source file template
c                                1st dim: emissivity type
c  output: (l*4)   lfilea_c()  = .true.  => child emissivity type required
c                                .false. => file not required
c                                 1st dim: emissivity type
c  output: (i*4)   ipl_c       = position of child partition in iptnla
c
c  output: (i*4)   nptnl       = number of partition levels in block
c  output: (i*4)   nptn()      = number of partitions in partition level
c  output: (i*4)   nptnc(,)    = number of components in partition
c  output: (i*4)   iptnla()    = partition level index
c  output: (i*4)   iptna(,)    = partition index
c  output: (i*4)   iptnca(,,)  = component index
c  output: (i*4)   ncptn_stack =  number of text lines in partition stack
c  output: (c*80)  cptn_stack()= stack of partition text lines
c  output: (l*4)   lres        = .true.  => script file resolved
c                                .false. => script file unresolved
c  output: (i*4)   ircode      = error flag:
c                                0 => script file was read okay
c                                1 => eof: formatting fault in script file
c
c          (i*4)   iunit      = parameter = input unit for data
c          (l*4)   open10      = .true.  => file allocated to unit 10.
c                                .false. => no file allocated to unit 10.
c
c routines:
c          routine    source   brief description
c          -----------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          xxcase     adas      convert a string to upper or lower case
c          xxelem     adas      return element name given nuclear charge
c          xxrmve     adas      remove selected character from string
c          xxrptn     adas      analyse an adf11 file partition block
c          xxslen     adas      find non-blank characters in string
c
c
c
c  author   : Hugh Summers
c  date     : 03-01-2007
c
c  Version : 1.1
c  Date    : 23-07-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 08-02-2010
c  Modified: Martin O'Mullane
c            - Do not force lowercase on output (child) GCR file names.
c
c  Version : 1.3
c  Date    : 10-04-2019
c  Modified: Martin O'Mullane
c            - Extra check for emissivity file existence since blank
c              is an acceptable value (set to false in this case).
c
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
       integer    ifdimd  , iunit   ,
     &            i       , j
       integer    ndptnl  , ndptn   , ndptnc  , ndstack
       integer    nptnl   , ncptn_stack
       integer    np      , ip      , ip1     ,
     &            ipl_p   , ipl_c   , maxpl
       integer    i4unit  , ircode
c-----------------------------------------------------------------------
       character  year*2      , elem*12      , c12*12    ,
     &            c2*2        , cstrg*80     , cline*120 ,
     &            dsngcr_p*120, dsngcr_c*120 , c120*120  , elem_in*12
       character  dsna_p_temp*120
c-----------------------------------------------------------------------
       logical    lexist
       logical    lres        , lptn
c-----------------------------------------------------------------------
       integer    nptn(ndptnl)          , nptnc(ndptnl,ndptn)
       integer    iptnla(ndptnl)        , iptna(ndptnl,ndptn)
       integer    iptnca(ndptnl,ndptn,ndptnc)
c-----------------------------------------------------------------------
       character  dsna_p(ifdimd)*120   , dsna_c(ifdimd)*120
       character  cptn_stack(ndstack)*80
c-----------------------------------------------------------------------
       logical    lfilea_p(ifdimd)  , lfilea_c(ifdimd)
c-----------------------------------------------------------------------

       ircode = 0

c-----------------------------------------------------------------------
c      read in element and year number
c-----------------------------------------------------------------------

       cline   = ' '
       read(iunit,'(a)',err=9999) cline
       call xxrmve(cline,c12,' ')
       call xxcase(c12,elem,'lc')

       read(iunit,'(a)',err=9999) cline

       cline=' '
       read(iunit,'(a)',err=9999) cline
       call xxrmve(cline,c2,' ')
       year = c2

       cline=' '
       read(iunit,'(a)',err=9999) cline
       lres = .true.
       if (index(cline,'unresolved').gt.0) lres=.false.

c-----------------------------------------------------------------------
c      read in parent emissivity file names
c-----------------------------------------------------------------------

   10  cline=' '
       read(iunit,'(a)',err=9999) cline
       if((index(cline,'parent').le.0).and.
     &    (index(cline,'PARENT').le.0)) then
           go to 10
       endif
       cline=' '
       read(iunit,'(a)',err=9999) cline
       cline=' '
       read(iunit,'(a)',err=9999) cline

c------------------------------------------------
c      check gcr library pathway is present first
c------------------------------------------------

       if((cline(1:6).ne.'gcr  :').and.
     &    (cline(1:6).ne.'GCR  :')) then
           write(i4unit(-1),1001)'parent pathway to '//
     &                           'gcr library required first'
           write(i4unit(-1),1002)
           stop
       endif
       cline(1:6)='      '
       call xxrmve(cline,c120,' ')
       dsngcr_p = c120

       do i = 1 , 4

         dsna_p(i)=' '
         cline=' '
         read(iunit,'(a)',err=9999) cline
         if(cline(1:6).eq.'      ') then
             go to 20
         elseif((cline(1:6).eq.'pec  :').or.
     &          (cline(1:6).eq.'PEC  :')) then
             j=1
         elseif((cline(1:6).eq.'gtn  :').or.
     &          (cline(1:6).eq.'GTN  :')) then
             j=2
         elseif((cline(1:6).eq.'f-pec:').or.
     &          (cline(1:6).eq.'F-PEC:')) then
             j=3
         elseif((cline(1:6).eq.'f-gtn:').or.
     &          (cline(1:6).eq.'F-GTN:')) then
             j=4
         else
             j=0
         endif
         if(j.ne.0)then
             cline(1:6)='      '
             call xxrmve(cline,c120,' ')
             dsna_p(j) = c120
         endif

       enddo

c-----------------------------------------------------------------------
c      read in partition specification
c-----------------------------------------------------------------------

   20  read(iunit,'(a)',err=9999) cline
       if((index(cline,'partition').le.0).and.
     &    (index(cline,'PARTITION').le.0)) then
           go to 20
       endif
       read(iunit,'(a)',err=9999) cline

       call  xxrptn( iunit , ndstack,
     &               ndptnl , ndptn  , ndptnc ,
     &               nptnl  , nptn   , nptnc  ,
     &               iptnla , iptna  , iptnca ,
     &               lres   , lptn   ,
     &               cstrg  ,
     &               ncptn_stack     , cptn_stack
     &              )
       if(.not.lptn) then
           write(i4unit(-1),1001)'no partition specification '//
     &                           'in script'
           write(i4unit(-1),1002)
           stop
       endif

c-----------------------------------------------------------------------
c      read in child emissivity file names
c-----------------------------------------------------------------------

   30  read(iunit,'(a)',err=9999) cline
       if((index(cline,'child').le.0).and.
     &   (index(cline,'child').le.0)) then
          go to 30
       endif
       read(iunit,'(a)',err=9999) cline

       cline=' '
       read(iunit,'(a)',err=9999) cline
       cline(1:6)='      '
       call xxrmve(cline,c120,' ')
       dsngcr_c = cline

       do i = 1 , 4
         dsna_c(i)=' '
         cline=' '
         read(iunit,'(a)',err=9999) cline
         if(cline(1:6).eq.'      ') then
             go to 40
         elseif((cline(1:6).eq.'pec  :').or.
     &          (cline(1:6).eq.'PEC  :')) then
             j=1
         elseif((cline(1:6).eq.'gtn  :').or.
     &          (cline(1:6).eq.'GTN  :')) then
             j=2
         elseif((cline(1:6).eq.'f-pec:').or.
     &          (cline(1:6).eq.'F-PEC:')) then
             j=3
         elseif((cline(1:6).eq.'f-gtn:').or.
     &          (cline(1:6).eq.'F-GTN:')) then
             j=4
         else
             j=0
         endif
         if(j.ne.0)then
            cline(1:6)='      '
            call xxrmve(cline,c120,' ')
            dsna_c(j)=c120
         endif
       enddo

c-----------------------------------------------------------------------
c      test for parent file availability in the different categories
c      search on partitions although should really be superstages
c      jump out as soon as one is found.
c-----------------------------------------------------------------------

   40  maxpl=0
       do i=1,nptnl
         if(iptnla(i).gt.maxpl) maxpl=iptnla(i)
       enddo
       do i=1,nptnl
         if(iptnla(i).eq.maxpl-1) ipl_p=i
         if(iptnla(i).eq.maxpl) ipl_c=i
       enddo

       np= nptn(ipl_p)-1

       do i = 1,4
         lfilea_p(i)=.false.
         dsna_p_temp=dsna_p(i)
         j=index(dsna_p_temp,'**')
         if (j.gt.0) then
            do ip1 = 1,np
              ip=ip1-1
              write(c2,'(i2)')ip
              if(c2(1:1).eq.' ') c2(1:1)='0'
              dsna_p_temp(j:j+1) = c2(1:2)
              inquire (file=dsna_p_temp, exist=lexist)
              if (.not.lexist) then
                  lfilea_p(i)=.false.
              else
                  lfilea_p(i)=.true.
                  go to 50
              endif
            enddo
         else
            lfilea_p(i)=.false.
         endif
   50    continue
       enddo

c-----------------------------------------------------------------------
c      set up  child file outputs to match parent availability
c-----------------------------------------------------------------------

       do i=1,4
         if((dsna_c(i).ne.' ').and.lfilea_p(i))then
             lfilea_c(i)=.true.
         else
             lfilea_c(i)=.false.
         endif
       enddo

       return

c-----------------------------------------------------------------------

9999   continue

       ircode=1
       return

c-----------------------------------------------------------------------
 1001 format(1x,32('*'),' dgscrp error ',32('*')//
     &       1x,'fault in input data file: ',a,i3,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
c-----------------------------------------------------------------------
       end
