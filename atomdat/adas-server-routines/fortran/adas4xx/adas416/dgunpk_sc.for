      subroutine dgunpk_sc( ndclas   , isdimd   ,
     &                      ndcnct   ,
     &                      ncnct_c  , icnctv_c ,
     &                      crmat_c  , jcrmat_c , crccd_c  , jcrccd_c ,
     &                      crprb_c  , jcrprb_c , crprc_c  , jcrprc_c ,
     &                      crplt_c  , jcrplt_c , crpls_c  , jcrpls_c ,
     &                      crzcd_c  , crycd_c  , crecd_c  ,
     &                      iblmxa_c , ispbra_c , isppra_c , isstgra_c,
     &                      dgcr_c   , lgcra_c
     &                    )
      implicit none

c-----------------------------------------------------------------------
c
c ****************** fortran 77 subroutine: dgunpk *********************
c
c purpose: To assemble child generalised collisional-radiative coeffts.
c          in tabulations ready for output as adf11 files from child
c          partitioned collisional-radiative matrices and vectors.
c
c
c calling program: adas416
c
c subroutine:
c
c
c input : (i*4)  ndclas     = number of adf11 master file classes
c input : (i*4)  isdimd     = maximum number of (sstage, parent, base)
c                             blocks in isonuclear master files
c input : (i*4)  ndcnct     = maximum number of elements in connection
c                             vector
c
c input : (i*4)  ncnct_c    = number of elements in connection vector
c input : (i*4)  icnctv_c() = connection vector of number of partitions
c                            of each superstage in resolved case
c                            including the bare nucleus
c                            1st dim: connection vector index
c input : (r*8)  crmat_c(,) = collisional-radiative matrix for child
c                             1st dim: superstage/partition list index
c                             2nd dim: superstage/partition list index
c input : (i*4)  jcrmat_c(,)= power of 10 scale factor for sub-matrices
c                             (common for each child superstage sub-block)
c                              1st dim: index of superstage (1->ncnct_c)
c                              2nd dim: index of superstage (1->ncnct_c)
c input : (r*8)  crccd_c(,) = cx recom rate coeff matrix for child
c                              1st dim: superstage/partition list index
c                              2nd dim: superstage/partition list index
c input : (i*4)  jcrccd_c(,)= power of 10 scale factor for sub-matrices
c                             (common for each child superstage sub-block)
c                              1st dim: index of superstage (1->ncnct_c)
c                              2nd dim: index of superstage (1->ncnct_c)
c input : (r*8)  crprb_c()  = recom/brems power vector for child
c                              1st dim: superstage/partition list index
c input : (i*4)  jcrprb_c(,)= power of 10 scale factor for sub-matrices
c                             (common for each child superstage sub-block)
c                              1st dim: index of superstage (1->ncnct_c)
c input : (r*8)  crprc_c()  = cx recom power vector for child
c                              1st dim: superstage/partition list index
c input : (i*4)  jcrprc_c(,)= power of 10 scale factor for sub-matrices
c                             (common for each child superstage sub-block)
c                              1st dim: index of superstage (1->ncnct_c)
c input : (r*8)  crplt_c()  = low level line power vector for child
c                              1st dim: superstage/partition list index
c input : (i*4)  jcrplt_c(,)= power of 10 scale factor for sub-matrices
c                             (common for each child superstage sub-block)
c                              1st dim: index of superstage (1->ncnct_c)
c input : (r*8)  crpls_c()  = special select line power vector for child
c                              1st dim: superstage/partition list index
c input : (i*4)  jcrpls_c(,)= power of 10 scale factor for sub-matrices
c                             (common for each child superstage sub-block)
c                              1st dim: index of superstage (1->ncnct_c)
c input : (r*8)  crzcd_c()  = effective ion charge vector
c                              1st dim: superstage/partition list index
c input : (r*8)  crycd_c()  = effective squared ion charge vector
c                              1st dim: superstage/partition list index
c input : (r*8)  crecd_c(,) = ionis/excit potential matrix for child
c                             1st dim: superstage/partition list index
c                             2nd dim: superstage/partition list index
c
c output: (i*4)  iblmxa_c() = number of (sstage, parent, base)
c                             blocks in isonuclear master file class
c                             1st dim: adf11 class index
c output: (i*4)  isppra_c(,)= 1st (parent) index for each partition block
c                             1st dim: adf11 class index
c                             2nd dim: index of (sstage, parent, base)
c                                  block in isonuclear master file
c output: (i*4)  ispbra_c(,)= 2nd (base) index for each partition block
c                             1st dim: adf11 class index
c                             2nd dim: index of (sstage, parent, base)
c                                  block in isonuclear master file
c output: (i*4)  isstgra_c()= s1 for each resolved data block
c                             (generalises to connection vector index)
c                             1st dim: adf11 class index
c                             2nd dim: index of (sstage, parent, base)
c                                  block in isonuclear master file
c output: (r*8)  dgcr_p(,)  = interpolation of prc coefficient (w cm3 )
c                             1st dim: adf11 class index
c                             2nd dim: index of (sstage, parent, base)
c                                      block in isonuclear master file
c output: (l*4)  lgcra_p()  = .true.  => data present for class
c                             .false. => data not present for class
c                             1st dim: adf11 class index
c
c
c
c routines:
c          routine    source    brief description
c          ----------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    02/09/05
c
c  Version : 1.1
c  Date    : 09-05-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 03-06-2009
c  Modified: Martin O'Mullane
c            - Check validity of numbers to dlog10 calls - if negative
c              set value to dzero.
c            - Align value of dzero with rest of code. 
c
c  Version : 1.3
c  Date    : 23-06-2009
c  Modified: Martin O'Mullane
c            - When filling isstgra_c for ccd check for j1= and k=1
c              in similar fashion to acd and scd.
c
c  Version : 1.4
c  Date    : 08-04-2019
c  Modified: Martin O'Mullane
c            - Check for scd below dzero.
c
c-----------------------------------------------------------------------
      integer   idcnct
c-----------------------------------------------------------------------
      real*8    dzero
c-----------------------------------------------------------------------
      parameter ( idcnct = 100 , dzero=1.0d-74 )
c-----------------------------------------------------------------------
      integer   i4unit
      integer   ndclas  , isdimd   , ndcnct   , ncnct_c
      integer   iclass  , isstg    , nsstg
      integer   j       , k        , j0       , k0       , ir
c-----------------------------------------------------------------------
      integer   icnctv_c(ndcnct)         , iblmxa_c(ndclas)            ,
     &          ipointa_sstg(idcnct)
      integer   ispbra_c(ndclas,isdimd)  , isppra_c(ndclas,isdimd)     ,
     &          isstgra_c(ndclas,isdimd)
      integer   jcrmat_c(isdimd+1,isdimd+1),jcrccd_c(isdimd+1,isdimd+1),
     &          jcrprb_c(isdimd+1)         , jcrprc_c(isdimd+1)        ,
     &          jcrplt_c(isdimd+1)         , jcrpls_c(isdimd+1)
c-----------------------------------------------------------------------
      real*8     dgcr_c(ndclas,isdimd)

      real*8     crmat_c(isdimd+1,isdimd+1) ,
     &           crccd_c(isdimd+1,isdimd+1)
      real*8     crprb_c(isdimd+1), crprc_c(isdimd+1),
     &           crplt_c(isdimd+1), crpls_c(isdimd+1),
     &           crzcd_c(isdimd+1), crycd_c(isdimd+1),
     &           crecd_c(isdimd+1,isdimd+1)
c-----------------------------------------------------------------------
      logical    lfirst
c-----------------------------------------------------------------------
      logical    lgcra_c(ndclas)
c-----------------------------------------------------------------------
      data       lfirst/.true./
c-----------------------------------------------------------------------
      save       nsstg, ipointa_sstg , lfirst
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c   check internal dimensions
c-----------------------------------------------------------------------
       if(idcnct.ne.ndcnct) then
         write(i4unit(-1),2002)'mismatch of internal dimensions: ',
     &                         'idcnct = ',idcnct,' .ne. ndcnct = ',
     &                          ndcnct
         write(i4unit(-1),2003)
         stop
       endif

c-----------------------------------------------------------------------
c  fill successively recombination, ionisation and excitation arrays
c  note that charge exchange recombination is not included
c-----------------------------------------------------------------------
       if(lfirst) then
           nsstg = ncnct_c
           ipointa_sstg(1)=0
           do isstg=2,nsstg
             ipointa_sstg(isstg)=ipointa_sstg(isstg-1)+icnctv_c(isstg-1)
           enddo
       endif
c------------------
c      process acd
c------------------
       iclass = 1
       if(lfirst) then
           j0=0
           k0=icnctv_c(1)
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg)
               do k=1,icnctv_c(isstg+1)
                 if((crmat_c(j0+j,k0+k).gt.dzero).or.
     &               (j.eq.1.and.k.eq.1)) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     ispbra_c(iclass,iblmxa_c(iclass))=j
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     if (crmat_c(j0+j,k0+k).le.dzero) then
                         dgcr_c(iclass,iblmxa_c(iclass))=
     &                          dlog10(dzero)
                     else
                         dgcr_c(iclass,iblmxa_c(iclass))=
     &                          dlog10(crmat_c(j0+j,k0+k))+
     &                                 jcrmat_c(j0+j,k0+k)
                     endif
                 endif
               enddo
             enddo
             j0=j0+icnctv_c(isstg)
             k0=k0+icnctv_c(isstg+1)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              k0 = ipointa_sstg(isstg+1)
              j = ispbra_c(iclass,ir)
              k = isppra_c(iclass,ir)
              if((crmat_c(j0+j,k0+k).gt.dzero)) then
                 dgcr_c(iclass,ir) = dlog10(crmat_c(j0+j,k0+k))+
     &                                      jcrmat_c(j0+j,k0+k)
              else
                 dgcr_c(iclass,ir) = dlog10(dzero)
              endif
           enddo
       endif

c------------------
c      process scd
c------------------
       iclass = 2
       if(lfirst) then
           j0=icnctv_c(1)
           k0=0
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg+1)
               do k=1,icnctv_c(isstg)
                 if((crmat_c(j0+j,k0+k).gt.dzero).or.
     &               ((j.eq.1).and.(k.eq.1))) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     ispbra_c(iclass,iblmxa_c(iclass))=j
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     if(crmat_c(j0+j,k0+k).gt.dzero)then
                         dgcr_c(iclass,iblmxa_c(iclass))=
     &                          dlog10(crmat_c(j0+j,k0+k))+
     &                                 jcrmat_c(j0+j,k0+k)

                     else
                         dgcr_c(iclass,iblmxa_c(iclass))=
     &                          dlog10(dzero)
                     endif
                 endif
               enddo
             enddo
             j0=j0+icnctv_c(isstg)
             k0=k0+icnctv_c(isstg+1)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg+1)
              k0 = ipointa_sstg(isstg)
              j = ispbra_c(iclass,ir)
              k = isppra_c(iclass,ir)
              if (crmat_c(j0+j,k0+k).gt.dzero) then
                 dgcr_c(iclass,ir) = dlog10(crmat_c(j0+j,k0+k))+
     &                                      jcrmat_c(j0+j,k0+k)
              else
                 dgcr_c(iclass,ir) = dlog10(dzero)
              endif
           enddo
       endif

c------------------
c      process qcd
c------------------
       iclass = 6
       if(lfirst) then
           j0=0
           k0=0
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg)
               do k=1,icnctv_c(isstg)
c##  more care required for the zero problem also for scd  -revisit ##
                 if(((j0+j).ne.(k0+k)).and.
     &                crmat_c(j0+j,k0+k).gt.dzero) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     ispbra_c(iclass,iblmxa_c(iclass))=j
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     dgcr_c(iclass,iblmxa_c(iclass))=
     &                      dlog10(crmat_c(j0+j,k0+k))+
     &                                 jcrmat_c(j0+j,k0+k)

                 endif
               enddo
             enddo
             j0=j0+icnctv_c(isstg)
             k0=k0+icnctv_c(isstg)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              k0 = ipointa_sstg(isstg)
              j = ispbra_c(iclass,ir)
              k = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = dlog10(crmat_c(j0+j,k0+k))+
     &                                 jcrmat_c(j0+j,k0+k)

           enddo
       endif

c------------------
c      process xcd
c------------------
       iclass = 7
       if(lfirst) then
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
       endif

c------------------
c      process ccd
c------------------
       iclass = 3
       if(lfirst) then
           j0=0
           k0=icnctv_c(1)
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg)
               do k=1,icnctv_c(isstg+1)
                 if((crccd_c(j0+j,k0+k).gt.dzero).or.
     &               (j.eq.1.and.k.eq.1))  then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     ispbra_c(iclass,iblmxa_c(iclass))=j
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     dgcr_c(iclass,iblmxa_c(iclass))=
     &                      dlog10(crccd_c(j0+j,k0+k))+
     &                                 jcrccd_c(j0+j,k0+k)

                 else
                    dgcr_c(iclass,iblmxa_c(iclass))=dlog10(dzero)
                 endif
               enddo
             enddo
             j0=j0+icnctv_c(isstg)
             k0=k0+icnctv_c(isstg+1)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              k0 = ipointa_sstg(isstg+1)
              j = ispbra_c(iclass,ir)
              k = isppra_c(iclass,ir)
              if((crccd_c(j0+j,k0+k).gt.dzero)) then
                 dgcr_c(iclass,ir) = dlog10(crccd_c(j0+j,k0+k))+
     &                                      jcrccd_c(j0+j,k0+k)
              else
                 dgcr_c(iclass,ir) = dlog10(dzero)
              endif

           enddo
       endif

c------------------
c      process prb
c------------------
       iclass = 4
       if(lfirst) then
           k0=icnctv_c(1)
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
               do k=1,icnctv_c(isstg+1)
                 if(crprb_c(k0+k).gt.dzero) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     dgcr_c(iclass,iblmxa_c(iclass))=
     &                      dlog10(crprb_c(k0+k))+
     &                             jcrprb_c(k0+k)

                 endif
               enddo
               k0=k0+icnctv_c(isstg+1)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              k0 = ipointa_sstg(isstg+1)
              k = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = dlog10(crprb_c(k0+k))+
     &                                   jcrprb_c(k0+k)

           enddo
       endif

c------------------
c      process prc
c------------------
       iclass = 5
       if(lfirst) then
           k0=icnctv_c(1)
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
               do k=1,icnctv_c(isstg+1)
                 if(crprc_c(k0+k).gt.dzero) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     dgcr_c(iclass,iblmxa_c(iclass))=
     &                      dlog10(crprc_c(k0+k))+
     &                             jcrprc_c(k0+k)

                 endif
               enddo
               k0=k0+icnctv_c(isstg+1)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              k0 = ipointa_sstg(isstg+1)
              k = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = dlog10(crprc_c(k0+k))+
     &                                  jcrprc_c(k0+k)

           enddo
       endif

c------------------
c      process plt
c------------------
       iclass = 8
       if(lfirst) then
           j0=0
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg)
               if(crplt_c(j0+j).gt.dzero) then
                   iblmxa_c(iclass)=iblmxa_c(iclass)+1
                   isstgra_c(iclass,iblmxa_c(iclass))=isstg
                   isppra_c(iclass,iblmxa_c(iclass))=j
                   dgcr_c(iclass,iblmxa_c(iclass))=
     &                    dlog10(crplt_c(j0+j))+
     &                           jcrplt_c(j0+j)

               endif
             enddo
             j0=j0+icnctv_c(isstg)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              j = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = dlog10(crplt_c(j0+j))+
     &                                  jcrplt_c(j0+j)

           enddo
       endif
c------------------
c      process pls
c------------------
       iclass = 9
       if(lfirst) then
           j0=0
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg)
               if(crpls_c(j0+j).gt.dzero) then
                   iblmxa_c(iclass)=iblmxa_c(iclass)+1
                   isstgra_c(iclass,iblmxa_c(iclass))=isstg
                   isppra_c(iclass,iblmxa_c(iclass))=j
                   dgcr_c(iclass,iblmxa_c(iclass))=
     &                    dlog10(crpls_c(j0+j))+
     &                                 jcrpls_c(j0+j)

               endif
             enddo
             j0=j0+icnctv_c(isstg)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              j = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = dlog10(crpls_c(j0+j))+
     &                                 jcrpls_c(j0+j)

           enddo
       endif
c------------------
c      process zcd
c------------------
       iclass = 10
       if(lfirst) then
           j0=0
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg
             do j=1,icnctv_c(isstg)
               iblmxa_c(iclass)=iblmxa_c(iclass)+1
               isstgra_c(iclass,iblmxa_c(iclass))=isstg
               isppra_c(iclass,iblmxa_c(iclass))=j
               dgcr_c(iclass,iblmxa_c(iclass))=crzcd_c(j0+j)
             enddo
             j0=j0+icnctv_c(isstg)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              j = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = crzcd_c(j0+j)
           enddo
       endif
c------------------
c      process ycd
c------------------
       iclass = 11
       if(lfirst) then
           j0=0
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.
           do isstg = 1,nsstg
             do j=1,icnctv_c(isstg)
               iblmxa_c(iclass)=iblmxa_c(iclass)+1
               isstgra_c(iclass,iblmxa_c(iclass))=isstg
               isppra_c(iclass,iblmxa_c(iclass))=j
               dgcr_c(iclass,iblmxa_c(iclass))=crycd_c(j0+j)
             enddo
             j0=j0+icnctv_c(isstg)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              j0 = ipointa_sstg(isstg)
              j = isppra_c(iclass,ir)
              dgcr_c(iclass,ir) = crycd_c(j0+j)
           enddo
       endif

c------------------
c      process ecd
c------------------
       iclass = 12
       if(lfirst) then
           j0=0
           k0=1
           iblmxa_c(iclass)=0
           lgcra_c(iclass) = .false.

           isstg=0
           j=1

           do k=1,icnctv_c(isstg+1)
                 if((crecd_c(j0+j,k0+k).gt.dzero).or.
     &               (j.eq.1.and.k.eq.1)) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     ispbra_c(iclass,iblmxa_c(iclass))=j
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     if (crecd_c(j0+j,k0+k).le.dzero) then
                         dgcr_c(iclass,iblmxa_c(iclass))=0.0d0
                     else
                         dgcr_c(iclass,iblmxa_c(iclass))=
     &                          crecd_c(j0+j,k0+k)
                     endif
                 endif
               enddo

           j0=j0+1
           k0=k0+icnctv_c(1)


           do isstg = 1,nsstg-1
             do j=1,icnctv_c(isstg)
               do k=1,icnctv_c(isstg+1)
                 if((crecd_c(j0+j,k0+k).gt.dzero).or.
     &               (j.eq.1.and.k.eq.1)) then
                     iblmxa_c(iclass)=iblmxa_c(iclass)+1

                     isstgra_c(iclass,iblmxa_c(iclass))=isstg
                     ispbra_c(iclass,iblmxa_c(iclass))=j
                     isppra_c(iclass,iblmxa_c(iclass))=k
                     if (crecd_c(j0+j,k0+k).le.dzero) then
                         dgcr_c(iclass,iblmxa_c(iclass))=0.0d0
                     else
                         dgcr_c(iclass,iblmxa_c(iclass))=
     &                          crecd_c(j0+j,k0+k)
                     endif
                 endif
               enddo
             enddo
             j0=j0+icnctv_c(isstg)
             k0=k0+icnctv_c(isstg+1)
           enddo
           if(iblmxa_c(iclass).gt.0) then
               lgcra_c(iclass) = .true.
           endif
       elseif((.not.lfirst).and.lgcra_c(iclass)) then
           do ir=1,iblmxa_c(iclass)
              isstg = isstgra_c(iclass,ir)
              if(isstg.eq.0)then
                  j0=1
              else
                  j0 = ipointa_sstg(isstg)+1
              endif
              k0 = ipointa_sstg(isstg+1)+1
              j = ispbra_c(iclass,ir)
              k = isppra_c(iclass,ir)
              if (crecd_c(j0+j,k0+k).le.dzero) then
c                 dgcr_c(iclass,iblmxa_c(iclass))=0.0d0
                  dgcr_c(iclass,ir)=0.0d0
              else
c                  dgcr_c(iclass,iblmxa_c(iclass))=
c     &                  crecd_c(j0+j,k0+k)
                  dgcr_c(iclass,ir)=
     &                   crecd_c(j0+j,k0+k)
              endif
           enddo
       endif

      lfirst = .false.

      return
c
c-----------------------------------------------------------------------
c
 2002 format(1x,30('*'),'    dgunpk error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
