C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9spow.for,v 1.1 2004/07/06 13:27:05 whitefor Exp $ Date $Date: 2004/07/06 13:27:05 $
CX
      SUBROUTINE D9SPOW( LSELA  , LEXSA  , LDEFA  , LPART  , LEXSS  ,
     &                   IZ0    , IZL    , IZH    , NPART  ,
     &                   ISDIMD , IZDIMD , ITDIMD , IPDIMD , IMDIMD ,
     &                   ACDA   , SCDA   , CCDA   , PRBA   ,
     &                   PRCA   , QCDA   , XCDA   , PLTA   ,
     &                   NMSUM  , IZIP   , IMIP   , IPIZM  ,
     &                   NTDIM  , NDDIM  , ITMAX  , IDMAX  ,
     &                   DENS   , DENSH  ,
     &                   FPABUN , FSABUN ,
     &                   PLTPEQ ,
     &                   ACDSEQ , SCDSEQ , CCDSEQ , PRBSEQ ,
     &                   PRCSEQ , PLTSEQ ,
     &                   PRBEQ  , PRCEQ  , PLTEQ  , PRADA
     &                  )
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9SPOW *********************
C
C PURPOSE : TO ASSEMBLE RADIATED POWER FUNCTIONS USING FRACTIONAL
C           METASTABLE ABUNDANCES.
C           GENERATE STANDARD ISONUCLEAR MASTER DATA FROM PARTIAL DATA.
C           2D (TEMPERATURE, DENSITY) VERSION.
C
C NOTE    : THE SOURCE ISONUCLEAR MASTER FILE DATA ARE OBTAINED BY A
C           PRIOR CALL TO SUBROUTINE D9DATA FROM SEQUENTIAL FILES
C           WITH THE FOLLOWING NAMING CONVENTIONS:
C
C                   (1) JETSHP.ACD<YR>#<EL).<CODE>DATA
C                   (2) JETSHP.SCD<YR>#<EL>.<CODE>DATA
C                   (3) JETSHP.CCD<YR>#<EL>.<CODE>DATA
C                   (4) JETSHP.PRB<YR>#<EL>.<FILT>.<CODE>DATA
C                   (5) JETSHP.PRC<YR>#<EL>.<FILT>.<CODE>DATA
C                   (6) JETSHP.QCD<YR>#<EL>.<CODE>DATA
C                   (7) JETSHP.XCD<YR>#<EL>.<CODE>DATA
C                   (8) JETSHP.PLT<YR>#<EL>.<CODE>DATA
C
C       WHERE, <YR>   = TWO DIGIT YEAR NUMBER
C              <EL>   = ONE OR TWO CHARACTER ELEMENT SYMBOL
C              <CODE> = R       => PARTIAL DATA
C                       U       => PARTIAL DATA
C                       OMITTED => STANDARD DATA
C              <FILT> = SIX CHARACTER POWER FILTER CODE
C
C       AND DATA OF CLASSES 6 AND 7 DO NOT EXIST FOR THE PARTIAL CASE.
C
C
C INPUT  : (L*4)  LSELA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        INDEX SELECTED
C                           = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                        NOT SELECTED
C INPUT  : (L*4)  LEXSA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        SELECTED INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS SELECTED INDEX
C INPUT  : (L*4)  LDEFA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        DEFAULT YEAR INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS DEFAULT YEAR INDEX
C INPUT  : (L*4)  LPART     = .TRUE.  => PARTIAL DATA SELECTED
C                           = .FALSE. => STANDARD DATA SELECTED
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  IZL       = MINIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  IZH       = MAXIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZL-1 TO
C                             IZH ON INPUT
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH
C                             IONISATION STAGE
C INPUT  : (I*4)  IMDIMD    = MAXIMUM NUMBER OF METASTABLES
C
C INPUT  : (R*8)  ACDA(,,,,)= INTERPOLATION OF ACD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C                             5TH DIM: RECOMBINED METASTABLE INDEX
C INPUT  : (R*8)  SCDA(,,,,)= INTERPOLATION OF SCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C                             5TH DIM: RECOMBINED METASTABLE INDEX
C INPUT  : (R*8)  CCDA(,,,,)= INTERPOLATION OF CCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C                             5TH DIM: RECOMBINED METASTABLE INDEX
C INPUT  : (R*8)  PRBA(,,,) = INTERPOLATION OF PRB COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C INPUT  : (R*8)  PRCA(,,,) = INTERPOLATION OF PRC COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C INPUTT : (R*8)  QCDA(,,,) = INTERPOLATION OF QCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: FIRST METASTABLE INDEX
C                             5TH DIM: SECOND METASTABLE INDEX
C INPUT  : (R*8)  XCDA(,,,) = INTERPOLATION OF XCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: FIRST PARENT METASTABLE INDEX
C                             5TH DIM: SECOND PARENT METASTABLE INDEX
C INPUT  : (R*8)  PLTA(,,,) = INTERPOLATION OF PLT COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: METASTABLE INDEX
C INPUT  : (I*4)  NMSUM     = TOTAL NUMBER OF POPULATIONS
C
C INPUT  :        IZIP()    = ION CHARGE +1 (IZ1) OF METASTABLE IN LIST
C INPUT  :        IMIP()    = METASTABLE INDEX WITHIN CHARGE STATE IZ1
C                             OF METASTABLE INDEX FROM COMPLETE LIST
C INPUT  :        IPIZM(,)  = METASTABLE INDEX IN COMPLETE LIST
C                             1ST DIM: INDEX IZ1-IZL+1
C                             2ND DIM: METASTABLE COUNT FOR STAGE (IGRD)
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV/DDENS PAIRS
C INPUT  : (I*4)  ITMAX     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C INPUT  : (R*8)  DENS()    = ELECTRON DENSITIES (CM-3))
C INPUT  : (R*8)  DENSH()   = HYDROGEN DENSITIES (CM-3))
C INPUT  : (R*8)  FPABUN(,,)= RESOLVED METASTABLE EQUILIBRIUM
C                             FRACTIONAL ABUNDANCES
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - METASTABLE INDEX
C OUTPUT : (L*4)  LEXSS()   = .TRUE.  => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX GENERATED
C                           = .FALSE. => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX NOT GENERATED
C OUTPUT : (R*8)  FSABUN(,,)= STAGE EQUILIBRIUM FRACTIONAL ABUNDANCES
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  PLTPEQ(,,)= METASTABLE PARTIAL EQUILIBRIUM RADIATED
C                             LINE POWER FUNCTIONS
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - METASTABLE INDEX
C OUTPUT : (R*8)  ACDSEQ(,,)= STANDARD (UNRESOLVED) ACD COEFFICIENT
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY  INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  SCDSEQ(,,)= STANDARD (UNRESOLVED) SCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  CCDSEQ(,,)= STANDARD (UNRESOLVED) CCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  PRBSEQ(,,)= STANDARD (UNRESOLVED) SCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  PRCSEQ(,,)= STANDARD (UNRESOLVED) CCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  PLTSEQ(,,)= STANDARD (UNRESOLVED) CCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  PRBEQ(,)  = TOTAL EQUILIBRIUM RADIATED RECOM-BREMS
C                             POWER FUNCTION
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C OUTPUT : (R*8)  PRCEQ(,)  = TOTAL EQUILIBRIUM CX RADIATED RECOM POWER
C                             FUNCTION NORMALISED TO ELECTRON
C                             DENSITY
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C OUTPUT : (R*8)  PLTEQ(,)  = TOTAL EQUILIBRIUM RADIATED LINE POWER
C                             FUNCTION
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C OUTPUT : (R*8)  PRADA(,)  = TOTAL EQUILIBRIUM RADIATED POWER FUNCTION
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C
C PROGRAM: (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  ID        = GENERAL INDEX FOR DENSITY
C          (I*4)  IZ        = GENERAL INDEX FOR CHARGE
C          (I*4)  IP        = GENERAL INDEX FOR CHARGE
C          (I*4)  IZ1       = GENERAL INDEX FOR CHARGE+1
C          (I*4)  ICL       = GENERAL INDEX FOR CLASS
C          (I*4)  IPP       = GENERAL PARENT INDEX
C          (I*4)  IPG       = GENERAL GROUND INDEX
C          (I*4)  IZREF     = GENERAL CHARGE STAE POINTER INDEX
C          (I*4)  IPRT      = GENERAL INDEX FOR PARENT METASTABLE
C          (I*4)  IGRD      = GENERAL INDEX FOR METASTABLE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR : Alessandro Lanzafame, University of Strathclyde
C
C DATE   : 11 December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER   IZ0
      INTEGER   IZ1     , IZL     , IZH
      INTEGER   NTDIM   , NDDIM   , ITMAX   , IDMAX   , NMSUM
      INTEGER   ISDIMD  , IZDIMD  , ITDIMD  , IPDIMD  , IMDIMD
      INTEGER   IT      , ID      , IPRT    , IGRD    
      INTEGER   ICL     , IP      , IZREF   , IPP     , IPG
C-----------------------------------------------------------------------
      INTEGER   NPART(IZDIMD)
      INTEGER   IZIP(IMDIMD)   , IMIP(IMDIMD) ,
     &          IPIZM(IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
      REAL*8    DNS           , DNSH
C-----------------------------------------------------------------------
      REAL*8    DENS(NDDIM)   , DENSH(NDDIM)   ,
     &          PRBEQ(NTDIM,NDDIM), PRCEQ(NTDIM,NDDIM),
     &          PLTEQ(NTDIM,NDDIM), PRADA(NTDIM,NDDIM)
      REAL*8    FPABUN(NTDIM,NDDIM,IMDIMD) 
      REAL*8    FSABUN(NTDIM,NDDIM,IZDIMD)
      REAL*8    PLTPEQ(NTDIM,NDDIM,IMDIMD)
      REAL*8    ACDSEQ(NTDIM,NDDIM,IZDIMD) , SCDSEQ(NTDIM,NDDIM,IZDIMD),
     &          CCDSEQ(NTDIM,NDDIM,IZDIMD) , PRBSEQ(NTDIM,NDDIM,IZDIMD),
     &          PRCSEQ(NTDIM,NDDIM,IZDIMD) , PLTSEQ(NTDIM,NDDIM,IZDIMD)
      REAL*8    ACDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &          SCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &          CCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &          PRBA(NTDIM,NDDIM,IZDIMD,IPDIMD)        ,
     &          PRCA(NTDIM,NDDIM,IZDIMD,IPDIMD)        ,
     &          QCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &          XCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &          PLTA(NTDIM,NDDIM,IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
      LOGICAL   LPART
C-----------------------------------------------------------------------
      LOGICAL     LSELA(8)      , LEXSA(8)       , LDEFA(8)  ,
     &            LEXSS(8)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  CHECK DATA AVAILABILITY AND SET LOGICAL VARIABLES
C-----------------------------------------------------------------------
C
      DO ICL = 1,8
         IF((LEXSA(ICL).OR.LDEFA(ICL)).AND.LSELA(ICL)) THEN
            LEXSS(ICL) = .TRUE.
         ELSE
            LEXSS(ICL) = .FALSE.
         ENDIF
      ENDDO
      LEXSS(6) = .FALSE.
      LEXSS(7) = .FALSE.
C
C-----------------------------------------------------------------------
C  CALCULATE POWER FUNCTIONS AND PREPARE STANDARD MASTER FILE DATA
C-----------------------------------------------------------------------
C     
      DO ID=1,IDMAX
         DO IT=1,ITMAX
C     
            DNS  = DENS(ID)
            DNSH = DENSH(ID)
C
            PRBEQ(IT,ID) = 0.0D0
            PRCEQ(IT,ID) = 0.0D0
            PLTEQ(IT,ID) = 0.0D0
C
            DO IP = 1, IMDIMD
               PLTPEQ(IT,ID,IP) = 0.0D+0
            ENDDO
C
            DO IZ1 = IZL , IZH + 1
C
               IZREF = IZ1-IZL+1
               FSABUN(IT,ID,IZREF) = 0.0D0
C     
               IF(IZ1.LE.IZH) THEN
                  ACDSEQ(IT,ID,IZREF) = 0.0D0
                  SCDSEQ(IT,ID,IZREF) = 0.0D0
                  CCDSEQ(IT,ID,IZREF) = 0.0D0
                  PRBSEQ(IT,ID,IZREF) = 0.0D0
                  PRCSEQ(IT,ID,IZREF) = 0.0D0
                  PLTSEQ(IT,ID,IZREF) = 0.0D0
               ENDIF
C
               DO IP = 1,NMSUM
                  IF( IZIP(IP) .EQ. IZ1 )
     &                 FSABUN(IT,ID,IZREF) = FSABUN(IT,ID,IZREF) +
     &                 FPABUN(IT,ID,IP)
               ENDDO
C
               IF( IZ1 .LE. IZH ) THEN
C
                  DO IPRT = 1, NPART(IZREF+1)
                     IPP = IPIZM( IZREF+1, IPRT)
                     PRBSEQ(IT,ID,IZREF) = 
     &                    PRBSEQ(IT,ID,IZREF) + FPABUN(IT,ID,IPP)*
     &                    PRBA(IT,ID,IZREF,IPRT)
                     PRCSEQ(IT,ID,IZREF) = 
     &                    PRCSEQ(IT,ID,IZREF) + FPABUN(IT,ID,IPP)*
     &                    PRCA(IT,ID,IZREF,IPRT)
                     DO IGRD = 1, NPART(IZREF)
                        IPG = IPIZM( IZREF, IGRD)
                        ACDSEQ(IT,ID,IZREF) = ACDSEQ(IT,ID,IZREF) +
     &                       FPABUN(IT,ID,IPP)*
     &                       ACDA(IT,ID,IZREF,IPRT,IGRD)
                        SCDSEQ(IT,ID,IZREF) = SCDSEQ(IT,ID,IZREF) +
     &                       FPABUN(IT,ID,IPG)*
     &                       SCDA(IT,ID,IZREF,IPRT,IGRD)
                        CCDSEQ(IT,ID,IZREF) = CCDSEQ(IT,ID,IZREF) +
     &                       FPABUN(IT,ID,IPP)*
     &                       CCDA(IT,ID,IZREF,IPRT,IGRD)
                     ENDDO
                  ENDDO
C     
                  DO IGRD = 1, NPART(IZREF)
                     IPG = IPIZM( IZREF, IGRD)
                     PLTPEQ(IT,ID,IPG)   = FPABUN(IT,ID,IPG)*
     &                    PLTA(IT,ID,IZREF,IGRD)
                     PLTSEQ(IT,ID,IZREF) = 
     &                    PLTSEQ(IT,ID,IZREF) + FPABUN(IT,ID,IPG)*
     &                    PLTA(IT,ID,IZREF,IGRD)
                  ENDDO
C     
               ENDIF
C     
            ENDDO
C
            DO IZ1 = IZL , IZH
C
               IZREF = IZ1-IZL+1
C
               PRBEQ(IT,ID) = PRBEQ(IT,ID)+PRBSEQ(IT,ID,IZREF)
               PRCEQ(IT,ID) = PRCEQ(IT,ID)+PRCSEQ(IT,ID,IZREF)
               PLTEQ(IT,ID) = PLTEQ(IT,ID)+PLTSEQ(IT,ID,IZREF)
C     
               IF(FSABUN(IT,ID,IZREF+1).GT.0.0D0)THEN
                  ACDSEQ(IT,ID,IZREF) = 
     &                 ACDSEQ(IT,ID,IZREF)/FSABUN(IT,ID,IZREF+1)
                  CCDSEQ(IT,ID,IZREF) = 
     &                 CCDSEQ(IT,ID,IZREF)/FSABUN(IT,ID,IZREF+1)
                  PRBSEQ(IT,ID,IZREF) = 
     &                 PRBSEQ(IT,ID,IZREF)/FSABUN(IT,ID,IZREF+1)
                  PRCSEQ(IT,ID,IZREF) = 
     &                 PRCSEQ(IT,ID,IZREF)/FSABUN(IT,ID,IZREF+1)
               ELSE
                  ACDSEQ(IT,ID,IZREF) = 0.0D0
                  CCDSEQ(IT,ID,IZREF) = 0.0D0
                  PRBSEQ(IT,ID,IZREF) = 0.0D0
                  PRCSEQ(IT,ID,IZREF) = 0.0D0
               ENDIF
C     
               IF(FSABUN(IT,ID,IZREF).GT.0.0D0)THEN
                  SCDSEQ(IT,ID,IZREF) = 
     &                 SCDSEQ(IT,ID,IZREF)/FSABUN(IT,ID,IZREF)
                  PLTSEQ(IT,ID,IZREF) = 
     &                 PLTSEQ(IT,ID,IZREF)/FSABUN(IT,ID,IZREF)
               ELSE
                  SCDSEQ(IT,ID,IZREF) = 0.0D0
                  PLTSEQ(IT,ID,IZREF) = 0.0D0
               ENDIF
C     
            ENDDO
C     
            PRCEQ(IT,ID) = DNSH*PRCEQ(IT,ID)/DNS
            PRADA(IT,ID) = PRBEQ(IT,ID) + PRCEQ(IT,ID) + PLTEQ(IT,ID)
C     
         ENDDO
      ENDDO
C     
      RETURN
C     
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      END
      

