C Copyright (c) 1997, Strathclyde University. 
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9spc2.for,v 1.2 2004/07/06 13:26:44 whitefor Exp $ Date $Date: 2004/07/06 13:26:44 $
CX
      SUBROUTINE D9SPC2( DSNAME, IBSEL  , IZIN   , IZ0IN  ,
     &                   ITVAL  , IDVAL , TVAL   , DVAL   ,
     &                   WLNGTH ,  
     &                   PECA   , LTRNG  , LDRNG  ,
     &                   TITLX  , IRCODE
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: D9PSC2 ********************
C
C  PURPOSE: TO EXTRACT AND INTERPOLATE  PHOTON EMISSIVITIES FOR
C           EMITTING IONS.
C
C           DERIVED FROM D5SPC2
C
C           THIS ROUTINE TAKES AS INPUT THE NAMES OF THE PHOTON 
C           EMISSIVITY FILES AND CHECKS THEY ARE THERE BEFORE 
C           OPENING THEM AND EXTRACTING ALL REQUIRED INFORMATION.
C
C  CALLING PROGRAM: D9SPEC
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)   IZIN    = ION CHARGE OF EMITTING ION
C  INPUT : (I*4)   IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C  INPUT : (I*4)   ITVAL   = NO. OF ELECTRON TEMPERATURE VALUES
C  INPUT : (I*4)   IDVAL   = NO. OF ELECTRON DENSITY VALUES
C  INPUT : (R*8)   TVAL()  = ELECTRON TEMPERATURES (UNITS: EV)
C                            DIMENSION: TEMPERATURE INDEX
C  INPUT : (R*8)   DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                            DIMENSION: DENSITY INDEX
C
C  OUTPUT: (R*8)   WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C  OUTPUT: (R*8)   PECA(,) = PHOTON EMISSIVITIES.
C                            1ST DIM: TEMPERATURE INDEX
C                            2ND DIM: DENSITY INDEX
C  OUTPUT: (L*4)   LTRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE INDEX
C  OUTPUT: (L*4)   LDRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: DENSITY PAIR INDEX
C
C  OUTPUT: (C*120) TITLX   = INFORMATION STRING (DSN ETC.)
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => NORMAL COMPLETION - NO ERROR DETECTED
C                            1 => DATA SET MEMBER FOR EMITTING ION WITH
C                                 CHARGE 'IZIN' & ION CHARGE 'IZ0IN' CAN
C                                 NOT BE FOUND/DOES NOT EXIST.
C                            2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                 AND THOSE IN INPUT FILE.
C                            3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                 OF RANGE OR DOES NOT EXIST.
C                            4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                 ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                            5 => INVALID VALUE FOR 'IZIN' ENTERED.
C                                 ( 0  <= 'IZIN' <= 99 )
C                            9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                 INPUT DATA-SET.
C
C          (I*4)   NSTORE   = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)   NTDIM    = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                       ERATURES THAT CAN BE READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   NDDIM    = PARAMETER= MAXIMUM NUMBER OF ELECTRON DENS-
C                                       ITIES  THAT  CAN  BE  READ  FROM
C                                       AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   IZ0MIN   = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)   IZ0MAX   = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)   IUNIT    = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)   NBSEL    = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                             DATA SET.
C          (I*4)   IZ0      = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C          (I*4)   IZ       = INPUT FILE - EMITTING ION - CHARGE
C          (I*4)   IZ1      = INPUT FILE - EMITTING ION - CHARGE + 1
C
C          (L*4)   LOPEN    = .TRUE.  => INPUT DATA SET OPEN.
C                             .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)   ESYM     = INPUT FILE - EMITTING ION - ELEMENT SYMBOL
C          (C*120) DSNAME   = NAME OF DATA SET INTERROGATED
C
C          (I*4)   ISELA()  = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                             DIMENSION: DATA-BLOCK INDEX
C          (I*4)   ITA()    = INPUT DATA SET-NUMBER OF ELECTRON 
C                             TEMPERATURES.
C                             DIMENSION: DATA-BLOCK INDEX
C          (I*4)   IDA()    = INPUT DATA SET-NUMBER OF ELECTRON DENSITIES
C                             DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)   TETA(,)  = INPUT DATA SET -
C                             ELECTRON TEMPERATURES (UNITS: eV)
C                             1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                             2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   TEDA(,)  = INPUT DATA SET -
C                             ELECTRON DENSITIES    (UNITS: cm-3)
C                             1st DIMENSION: ELECTRON DENSITY     INDEX
C                             2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   PEC(,,)  = INPUT DATA SET -
C                             FULL SET OF IONIZATIONS PER PHOTON
C                             1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                             2nd DIMENSION: ELECTRON DENSITY     INDEX
C                             3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*10)  CWAVEL() = INPUT FILE - WAVELENGTH (ANGSTROMS)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CFILE()  = INPUT FILE - SPECIFIC ION FILE SOURCE
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CTYPE()  = INPUT FILE - TYPE OF DATA (IE EXCIT., ETC)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*2)   CINDM()  = INPUT FILE - METASTABLE INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E3DATA     ADAS      FETCH INPUT DATA FROM SELECTED DATA SET
C          E3CHKB     ADAS      CHECK VALIDITY OF ION AND 'IBSEL'
C          E3SPLN     ADAS      INTERPOLATE DATA WITH TWO WAY SPLINES
C          E3TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C
C AUTHOR:  Alessandro Lanzafame, University of Strathclyde.
C
C DATE:    7th December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C VERSION:  1.2                                         DATE: 29-05-2002
C MODIFIED: Martin O'Mullane
C               - Change dimension of arrays from e3data as the 96 pecs
C                 can have 24 entries. It is not necessary to tie
C                 these dimensions to the global NTDIM/NDDIM used
C                 in the rest of the program.
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDMAX            , NDDMAX
      INTEGER     IZ0MIN         , IZ0MAX, ID,IT
      INTEGER     NTDIM             , NDDIM
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 350   , NTDMAX  = 24      , NDDMAX =  24  )
      PARAMETER(  NTDIM  = 20    , NDDIM =  20       )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 50       )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , IZIN              ,
     &            ITVAL          , IDVAL             , IRCODE
      INTEGER     IUNIT          , NBSEL             ,
     &            IZ0            , IZ                , IZ1
C-----------------------------------------------------------------------
      REAL*8      WLNGTH
C-----------------------------------------------------------------------
      LOGICAL     LOPEN,     LEXIST
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , 
     &            DSNAME*120         , TITLX*120
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             ,
     &            ITA(NSTORE)               , IDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8      TVAL(NTDIM)               , DVAL(NDDIM)          ,
     &            PECA(NTDIM,NDDIM)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(IDVAL)
C-----------------------------------------------------------------------
      CHARACTER   CINDM(NSTORE)*2           , CFILE(NSTORE)*8      ,
     &            CTYPE(NSTORE)*8           , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDMAX,NSTORE)       , TEDA(NDDMAX,NSTORE)
      REAL*8      PEC(NTDMAX,NDDMAX,NSTORE)
c-----------------------------------------------------------------------
      DATA        IUNIT  /15/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IRCODE = 0
      LOPEN  = .FALSE.
      IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN
C
C-----------------------------------------------------------------------
C IF NEW EMITTING ION ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------
C
         INQUIRE (FILE=DSNAME, EXIST=LEXIST)
         IF(LEXIST) THEN
            IRCODE = 0
            OPEN(UNIT=IUNIT, FILE=DSNAME, STATUS='OLD')
         ELSE
            IRCODE = 1
         ENDIF
         IF (IRCODE.EQ.0) THEN
            LOPEN  = .TRUE.
            CALL E3DATA( IUNIT  , DSNAME ,
     &                   NSTORE , NTDMAX , NDDMAX ,
     &                   IZ0    , IZ     , IZ1    , ESYM  ,
     &                   NBSEL  , ISELA  ,
     &                   CWAVEL , CFILE  , CTYPE  , CINDM ,
     &                   ITA    , IDA    ,
     &                   TETA   , TEDA   ,
     &                   PEC
     &                 )
         ENDIF
C
C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED EMITTING ION CHARGE VALUE.
C-----------------------------------------------------------------------
C
         IF ( IRCODE.EQ.0 ) THEN
            CALL E3CHKB( IUNIT , NBSEL  , IBSEL ,
     &                   IZ0IN , IZIN   ,
     &                   IZ0   , IZ     ,
     &                   LOPEN , IRCODE
     &                 )
         ENDIF
C
C-----------------------------------------------------------------------
C 1) INTERPOLATE WITH TWO WAY SPLINES.
C 2) CREATE TITLE FOR OUTPUT.
C-----------------------------------------------------------------------
C
         IF ( IRCODE.EQ.0 ) THEN
            CALL D9SPLN( NTDMAX            , NDDMAX        ,
     &                   ITA(IBSEL)        , IDA(IBSEL)    ,
     &                   ITVAL             , IDVAL         ,
     &                   TETA(1,IBSEL)     , TEDA(1,IBSEL) ,
     &                   TVAL              , DVAL          ,
     &                   PEC(1,1,IBSEL)    , PECA          ,
     &                   LTRNG             , LDRNG
     &                 )
            CALL E3TITL( IBSEL         , DSNAME       ,
     &                   ESYM          , IZ           ,
     &                   CWAVEL(IBSEL) , CINDM(IBSEL) ,
     &                   TITLX
     &                 )
            READ(CWAVEL(IBSEL),*) WLNGTH
         ENDIF
C
      ELSE
         IRCODE = 4
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
