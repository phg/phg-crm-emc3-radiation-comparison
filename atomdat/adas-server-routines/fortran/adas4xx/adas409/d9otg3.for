C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9otg3.for,v 1.1 2004/07/06 13:26:25 whitefor Exp $ Date $Date: 2004/07/06 13:26:25 $
CX
      SUBROUTINE D9OTG3( LGHOST , DATE  ,
     &                   IMDIMD , NTDIM , NDDIM , 
     &                   NDLINE , NDCOMP,
     &                   ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN   , YMAX   ,
     &                   NMSUM  , ITMAX , IDMAX  ,
     &                   TEV    , DENS  , POPTIT ,
     &                   IBSEL  ,
     &                   NLINE  , NCOMP ,
     &                   TITL   , SPECL , IPLINE ,
     &                   GCFPEQ , GCFEQ , DENSEL
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9OTG3 *********************
C
C  PURPOSE:  PIPE COMMS WITH IDL
C
C            PROVIDES DATA FOR  GRAPH OF SELECTED GCF FUNCTION AND 
C            ITS COMPONENTS
C
C            PLOT IS LOG10(GCF FUNCTION ( CM3 S-1) ) VERSUS
C                   (LOG10(ELECTRON TEMPERATURE (EV) ), 
C                    LOG10(ELECTRON DENSITY (CM-3) ) )
C  CALLING PROGRAM: ADAS409
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST   = .TRUE.  => GHOST80 INITIALISED
C                            .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE     = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM    = MAXIMUM NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDIM    = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)  NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C
C  INPUT : (C*2)  ELEMT    = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*44) DSNINC   = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR     = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF   = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                          = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                          = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM    = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX    = NUMBER OF INPUT ELECTRON TEMPERATURES
C  INPUT : (I*4)  IDMAX    = NUMBER OF INPUT ELECTRON DENSITIES
C
C  INPUT : (R*8)  TEV()    = ELECTRON TEMPERATURES (UNITS: EV)
C  INPUT : (R*8)  DENS()   = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*10) POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (I*4)  NLINE    = NUMBER OF LINES IDENTIFIED IN SCRIPT
C  INPUT : (I*4)  NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C  INPUT : (C*12) TITL(,)  = TITLE FOR LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (C*16) SPECL(,) = SPECIFICATION OF POINTERS OF LINE CPTS.
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  IPLINE(,)= METASTABLE POINTER OF LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (R*8) GCFPEQ(,,,)= GCF FUNC. COMPONENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: LINE INDEX
C                             4TH DIM: LINE COMPONENT INDEX
C  INPUT : (R*8)  GCFEQ(,,) = GCF FUNCTION  (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: LINE INDEX
C
C  OUTPUT : (I*4)  DENSEL  = INDEX OF DENSITY SELECTED FOR OUTPUT
C
C          (I*4)  NDIM1    = PARAMETER = MAXIMUM NUMBER OF TEMP. VALUES
C                            (MUST NOT BE LESS THAN 'NTDIM')
C          (I*4)  NDIM2    = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                            (MUST NOT BE LESS THAN 'IMDIMD')
C          (I*4)  NGPIC    = PARAMETER = MAXIMUM NUMBER OF LEVEL POPUL-
C                            ATIONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV    = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                            WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN   = PARAMETER = IN DEFAULT GRAPH SCALING IS
C                            THE  MINIMUM Y-VALUE THAT IS ALLOWED.
C                            (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO   = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                            NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT       = TEMPERATURE INDEX NUMBER FOR ARRAY USE
C          (I*4)  ID       = DENSITY INDEX NUMBER FOR ARRAY USE
C          (I*4)  IC       = LINE COMPONENT INDEX
C          (I*4)  IM       = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX    = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
C          (R*4)  X()      = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                             ELECTRON DENSITIES
C          (R*4)  Y(,)     = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                             LEVEL POPULATIONS.
C                             1ST DIMENSION = ELECTRON TEMP. INDEX
C                             2ND DIMENSION = METASTABLE  INDEX
C
C          (L*4)  LPLINE() = .TRUE.  => META. REFERENCED BY A LINE CPT
C                          = .FALSE. => META. NOT REFERENCED BY LINE CPT
C
C          (C*80) ISPEC    = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*80) CADAS    = ADAS HEADER: INCLUDES RELEASE,PROGRAM,TIME
C          (C*13) DNAME    = '       DATE: '
C          (C*13) FNAME    = 'INPUT FILE : '
C          (C*13) GNAME    = 'GRAPH TITLE: '
C          (C*23) XTIT     = X-AXIS UNITS/TITLE
C          (C*23) YTIT     = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C          (C*1)  GRID     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC      = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK   = BLANK 3 BYTE STRING
C          (C*3)  CNAM()   = 3 BYTE STRING FOR POWER FUNCT. TOTAL NAMES
c
C          (C*30) HEAD1    = HEADING FOR METASTABLE ASSIGMENTS
C          (C*30) HEAD2    = HEADING FOR SPECTRUM LINE SPECIFICATIONS
C          (C*30) STRG1    = INDX/DESIGNATION TITLE
C          (C*30) STRG3    = COMPONENT TITLE
C          (C*30) STRG4    = COMPONENT PARAMETER TITLE
C          (C*13) STRG5    = TITLE
C          (C*13) STRG6    = SELECT NO.
C          (C*13) STRG7    = COMPONENTS
C
C          (L*4)  LGTXT    = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
C                          = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C	   XXFLSH      IDL_ADAS  FLUSHES OUT UNIX PIPE
C
C
C AUTHOR:  Alessandro Lanzafame, University of Strathclyde
C
C DATE:    13 December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV
C-----------------------------------------------------------------------
CX      REAL*4    CUTMIN   , GHZERO
      REAL*8    GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 20  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
cx      PARAMETER ( CUTMIN = 1.0E-30 , GHZERO = 1.0E-36 )
      PARAMETER ( GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   PIPEIN   , PIPEOU
      INTEGER   IMDIMD   , NTDIM    , NDDIM    , NDLINE   , NDCOMP
      INTEGER   NMSUM    , ITMAX    , IDMAX    , NLINE    , IBSEL    ,
     &          IZ0	 , DENSEL
      INTEGER   IT       , ID       , IM       ,
     &          IMMAX    , IC        
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      INTEGER   NCOMP(NDLINE)       , IPLINE(NDLINE,NDCOMP)
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      LOGICAL   LPLINE(NDIM2)
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*44
      CHARACTER YEAR*2   , YEARDF*2

      CHARACTER DATE*8

C-----------------------------------------------------------------------
      CHARACTER TITL(NDLINE,NDCOMP)*12 , SPECL(NDLINE,NDCOMP)*16     ,
     &          CNAM(1)*3
C-----------------------------------------------------------------------
      CHARACTER POPTIT(IMDIMD)*10
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM) , DENS(NDDIM)
      REAL*8    GCFPEQ(NTDIM,NDDIM,NDLINE,NDCOMP) ,
     &          GCFEQ(NTDIM,NDDIM,NDLINE)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
      DATA CNAM(1) / 'TOT' /
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
      LGTXT = .TRUE.
      IMMAX = MIN0( NGLEV , NMSUM )
C
C-----------------------------------------------------------------------
C ZERO LOGIC VECTOR FOR REFERENCED METASTABLES
C-----------------------------------------------------------------------
C     
      DO IM = 1, NMSUM
         LPLINE(IM) = .FALSE.
      ENDDO

C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) IDMAX
      CALL XXFLSH(PIPEOU)

      DO IT=1, ITMAX
         WRITE(PIPEOU, *) REAL(TEV(IT))
         CALL XXFLSH(PIPEOU)
      ENDDO

      DO IT=1, IDMAX
         WRITE(PIPEOU, *) REAL(DENS(IT))
         CALL XXFLSH(PIPEOU)
      ENDDO

      WRITE(PIPEOU, *) NCOMP(IBSEL)
      CALL XXFLSH(PIPEOU)

      DO IC=1, NCOMP(IBSEL)
         DO IT=1, ITMAX
            DO ID=1, IDMAX
               IF (GCFPEQ(IT,ID,IBSEL,IC).LT.GHZERO) THEN
                  WRITE(PIPEOU,*) REAL(GHZERO)
                  CALL XXFLSH(PIPEOU)
               ELSE
                  WRITE(PIPEOU,*) REAL(GCFPEQ(IT,ID,IBSEL,IC))
                  CALL XXFLSH(PIPEOU)
               ENDIF
            ENDDO
         ENDDO
      ENDDO

      WRITE(PIPEOU,*) NMSUM
      CALL XXFLSH(PIPEOU)

      DO IC=1, NMSUM
         WRITE(PIPEOU,*) POPTIT(IC)
         CALL XXFLSH(PIPEOU)
      ENDDO

      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)

      DO IC=1, NCOMP(IBSEL)
         WRITE(PIPEOU,*) SPECL(IBSEL,IC)
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) IPLINE(IBSEL,IC)
         CALL XXFLSH(PIPEOU)
      ENDDO

      DO IT=1, ITMAX
         DO ID=1,IDMAX
            IF (GCFEQ(IT,ID,IBSEL).LT.GHZERO) THEN
               WRITE(PIPEOU,*) REAL(GHZERO)
               CALL XXFLSH(PIPEOU)
            ELSE
               WRITE(PIPEOU,*) REAL(GCFEQ(IT,ID,IBSEL))
               CALL XXFLSH(PIPEOU)
            ENDIF
         ENDDO
      ENDDO

      READ(PIPEIN,*)DENSEL
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A12,2X,'YEAR: ',1A2,2X,'DEFAULT YEAR: ',1A2)
 1001 FORMAT(I2)
 1002 FORMAT(/1X,31('*'),' D9OTG3 MESSAGE ',31('*')/
     &       1X,'METASTABLE: ',I3,
     &       4X,'NO GRAPH WILL BE OUTPUT BECAUSE:')
 1004 FORMAT(1X,'ALL VALUES ARE BELOW THE CUTOFF OF ',1PE10.3)
 1005 FORMAT(1X,'A SERIOUS ERROR EXISTS IN THE DATA OR D9OTG3')
 1006 FORMAT(1X,31('*'),' END OF MESSAGE ',31('*'))
 1007 FORMAT(I3,9X,A10)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
