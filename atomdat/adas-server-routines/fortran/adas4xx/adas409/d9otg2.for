C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9otg2.for,v 1.1 2004/07/06 13:26:19 whitefor Exp $ Date $Date: 2004/07/06 13:26:19 $
CX
      SUBROUTINE D9OTG2( LGHOST , LEXSS , DATE  ,
     &                   IMDIMD , NTDIM , NDDIM ,
     &                   ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN  , YMAX   ,
     &                   NMSUM  , ITMAX , IDMAX ,
     &                   TEV    , DENS  ,
     &                   POPTIT , PLTPEQ ,
     &                   PRBEQ  , PRCEQ  , PLTEQ, PRADA , DENSEL
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9OTG2 *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR TEMP/DENSITY MODEL GHOST80.
C
C            PROVIDES  GRAPH OF METASTABLE FRACTIONAL LINE POWER
C            FUNCTIONS AND THE PRB, PRC AND PLT POWER FUNCTIONS. A
C            SINGLE GRAPH WILL CONTAIN UP TO SEVEN METASTABLES. (IF MORE
C            THAN SEVEN METASTABLES ARE PRESENT EXTRA GRAPHS WILL  BE
C            OUTPUT AS REQUIRED).
C
C            PLOT IS LOG10(POWER FUNCTION (W CM3 ) ) VERSUS
C            (LOG10(ELECTRON TEMPERATURE (EV) ), 
C              LOG10 (ELECTRON DENSITY (CM-3)) )
C
C  CALLING PROGRAM: ADAS409
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (L*4)  LEXSS()   = .TRUE.  => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX GENERATED
C                           = .FALSE. => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX NOT GENERATED
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD  = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDDIM   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C
C  INPUT : (C*2)  ELEMT   = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*44) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR    = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF  = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN    = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX    = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN    = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX    = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM   = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX   = NUMBER OF INPUT ELECTRON DENSITIES
C  INPUT : (I*4)  IDMAX   = NUMBER OF INPUT DENSITY VALUES
C
C  INPUT : (R*8)  TEV()   = ELECTRON DENSITIES (UNITS: CM-3)
C  INPUT : (R*8)  DENS()  = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*10)POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (R*8) PLTPEQ(,,)= METASTABLE FRAACTIONAL LINE POWER FUNCTION
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: DENSITY INDEX
C                            3RD DIMENSION: METASTABLE INDEX
C  INPUT : (R*8)  PRBEQ(,) = TOTAL EQUILIBRIUM RADIATED RECOM-BREMS
C                            POWER FUNCTION
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: DENSITY INDEX
C  INPUT : (R*8)  PRCEQ(,) = TOTAL EQUILIBRIUM CX RADIATED RECOM POWER
C                            FUNCTION NORMALISED TO ELECTRON DENSITY
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: DENSITY INDEX
C  INPUT : (R*8)  PLTEQ(,) = TOTAL EQUILIBRIUM RADIATED LINE POWER
C                            FUNCTION
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: DENSITY INDEX
C  INPUT : (R*8)  PRADA(,) = TOTAL EQUILIBRIUM RADIATED POWER FUNCTION
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: DENSITY INDEX
C
C	OUTPUT: (I*4)  DENSEL  = INDEX OF DENSITY SELECTED FOR OUTPUT
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT      = TEMPERATURE INDEX NUMBER FOR ARRAY USE
C          (I*4)  ID      = DENSITY INDEX NUMBER FOR ARRAY USE
C          (I*4)  IM      = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX   = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C AUTHOR:  Alessandro Lanzafame, University of Strathclyde
C
C DATE:    12 December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      REAL*8  GHZERO
C-----------------------------------------------------------------------

      PARAMETER ( GHZERO = 1.0E-36 )

C-----------------------------------------------------------------------
      INTEGER   PIPEIN   , PIPEOU    
      INTEGER   IMDIMD   , NTDIM    , NDDIM
      INTEGER   NMSUM    , ITMAX    , IDMAX ,
     &          IZ0	 , DENSEL
      INTEGER   IT       , ID       , IM 
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      LOGICAL   LEXSS(8)
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*44
      CHARACTER YEAR*2   , YEARDF*2

      CHARACTER DATE*8

C-----------------------------------------------------------------------
      CHARACTER POPTIT(IMDIMD)*10
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM), DENS(IDMAX)
      REAL*8    PLTPEQ(NTDIM,NDDIM,IMDIMD) ,
     &          PRBEQ(NTDIM,NDDIM), PRCEQ(NTDIM,NDDIM),  
     &          PLTEQ(NTDIM,NDDIM), PRADA(NTDIM,NDDIM)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
      LGTXT = .TRUE.
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) IDMAX
      CALL XXFLSH(PIPEOU)

      DO IT=1, ITMAX
         WRITE(PIPEOU, *) REAL(TEV(IT))
         CALL XXFLSH(PIPEOU)
      ENDDO

      DO ID=1, IDMAX
         WRITE(PIPEOU, *) REAL(DENS(ID))
         CALL XXFLSH(PIPEOU)
      ENDDO

      WRITE(PIPEOU, *) NMSUM
      CALL XXFLSH(PIPEOU)

      DO IT=1, ITMAX
         DO ID=1, IDMAX
            DO IM=1, NMSUM
               IF (PLTPEQ(IT,ID,IM) .LT. GHZERO) THEN
                  WRITE(PIPEOU, *) REAL(GHZERO)
                  CALL XXFLSH(PIPEOU)
               ELSE
                  WRITE(PIPEOU, *) REAL(PLTPEQ(IT,ID,IM))
                  CALL XXFLSH(PIPEOU)
               ENDIF
            ENDDO
         ENDDO
      ENDDO

      DO IM=1, NMSUM
         WRITE(PIPEOU,'(A10)') POPTIT(IM)
         CALL XXFLSH(PIPEOU)
      ENDDO

      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)

      DO IT=1, ITMAX
         DO ID=1, IDMAX
            IF (PRBEQ(IT,ID) .LT. GHZERO) THEN
               WRITE(PIPEOU, *) REAL(GHZERO)
               CALL XXFLSH(PIPEOU)
            ELSE
               WRITE(PIPEOU, *) REAL(PRBEQ(IT,ID))
               CALL XXFLSH(PIPEOU)
            ENDIF
         ENDDO
      ENDDO

      DO IT=1, ITMAX
         DO ID=1, IDMAX
            IF (PRCEQ(IT,ID) .LT. GHZERO) THEN
               WRITE(PIPEOU, *) REAL(GHZERO)
               CALL XXFLSH(PIPEOU)
            ELSE
               WRITE(PIPEOU, *) REAL(PRCEQ(IT,ID))
               CALL XXFLSH(PIPEOU)
            ENDIF
         ENDDO
      ENDDO

      DO IT=1, ITMAX
         DO ID=1, IDMAX
            IF (PLTEQ(IT,ID) .LT. GHZERO) THEN
               WRITE(PIPEOU, *) REAL(GHZERO)
               CALL XXFLSH(PIPEOU)
            ELSE
               WRITE(PIPEOU, *) REAL(PLTEQ(IT,ID))
               CALL XXFLSH(PIPEOU)
            ENDIF
         ENDDO
      ENDDO
      DO IT=1, ITMAX
         DO ID=1, IDMAX
            IF (PRADA(IT,ID) .LT. GHZERO) THEN
               WRITE(PIPEOU, *) REAL(GHZERO)
               CALL XXFLSH(PIPEOU)
            ELSE
               WRITE(PIPEOU, *) REAL(PRADA(IT,ID))
               CALL XXFLSH(PIPEOU)
            ENDIF
         ENDDO
      ENDDO
      
      READ(PIPEIN,*)DENSEL     
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A12,2X,'YEAR: ',1A2,2X,'DEFAULT YEAR: ',1A2)
 1001 FORMAT(I2)
 1002 FORMAT(/1X,31('*'),' D9OTG2 MESSAGE ',31('*')/
     &       1X,'METASTABLE: ',I3,
     &       4X,'NO GRAPH WILL BE OUTPUT BECAUSE:')
 1004 FORMAT(1X,'ALL VALUES ARE BELOW THE CUTOFF OF ',1PE10.3)
 1005 FORMAT(1X,'A SERIOUS ERROR EXISTS IN THE DATA OR D9OTG2')
 1006 FORMAT(1X,31('*'),' END OF MESSAGE ',31('*'))
 1007 FORMAT(I3,9X,A10)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
