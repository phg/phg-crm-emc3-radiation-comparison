C Copyright (c) 1997, Strathclyde University.  
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9spln.for,v 1.1 2004/07/06 13:27:02 whitefor Exp $ Date $Date: 2004/07/06 13:27:02 $
CX
      SUBROUTINE D9SPLN( NTDIM  , NDDIM  ,
     &                   ITA    , IDA    , ITVAL   , IDVAL  ,
     &                   TETA   , TEDA   , TEVA    , DIN    ,
     &                   PEC    ,          PECA    ,
     &                                     LTRNG   , LDRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9SPLN *********************
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE OF LOG(PHOTON EMISSIVITY COEFFICIENTS) 
C          ON 2D GRID (LOG(TEMPERATURE), LOG(DENSITY))
C          INPUT DATA FOR A GIVEN WAVELENGTH DATA-BLOCK.
C
C          USING  TWO-WAY SPLINES IT CALCULATES  THE  PHOTON EMISSIVITY
C          FOR  'ITVAL' AND 'IDVAL'  INDEX OF  ELECTRON TEMPERATURES  
C          AND  DENSITIES RESPECTIVELY.
C          FROM THE TWO-DIMENSIONAL TABLE OF TEMPERATURES/DENSITIES READ
C          IN FROM THE INPUT FILE. IF A  VALUE  CANNOT  BE  INTERPOLATED
C          USING SPLINES IT IS EXTRAPOLATED VIA 'XXSPLE'.
C
C  CALLING PROGRAM: ADAS409
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NTDIM   = MAX NUMBER OF ELECTRON TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDDIM   = MAX NUMBER OF ELECTRON DENSITIES    ALLOWED
C
C  INPUT : (I*4)  ITA     = INPUT DATA FILE: NUMBER OF ELECTRON TEMPERA-
C                           TURES READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  IDA     = INPUT DATA FILE: NUMBER OF ELECTRON DENSIT-
C                           IES   READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  ITVAL   = NUMBER OF ISPF ENTERED TEMPERATURE/DENSITY
C                           PAIRS  FOR  WHICH  IOINIZATIONS PER PHOTON
C                           ARE REQUIRED FOR TABULAR/GRAPHICAL OUTPUT.
C
C  INPUT : (R*8)  TETA()  = INPUT DATA FILE: ELECTRON TEMPERATURES (EV)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  TEDA()  = INPUT DATA FILE: ELECTRON DENSITIES (CM-3)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: ELECTRON DENSITY INDEX
C  INPUT : (R*8)  TEVA()  = USER ENTERED: ELECTRON TEMPERATURES (EV)
C                           DIMENSION: TEMPERATURE INDEX
C  INPUT : (R*8)  DIN()   = USER ENTERED: ELECTRON DENSITIES (CM-3)
C                           DIMENSION: DENSITY INDEX
C
C  INPUT : (R*8)  PEC(,)   =INPUT DATA FILE: FULL SET OF IONIZATIONS PER
C                           PHOTON VALUES FOR THE DATA-BLOCK BEING
C                           ANALYSED.
C                           1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2ND DIMENSION: ELECTRON DENSITY     INDEX
C  OUTPUT: (R*8)  PECA(,) = SPLINE INTERPOLATED OR  EXTRAPOLATED PHOTON
C                           EMISSIVITY COEFFICIENT AT THE USER ENTERED 
C                           ELECTRON TEMPERATURE AND DENSITY POINTS.
C                           1ST DIM: TEMPERATURE INDEX
C                           2ND DIM: DENSITY INDEX
C
C  OUTPUT: (L*4)  LTRNG()=  .TRUE.  => OUTPUT 'PECA(,)' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'PECA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TEVA()'.
C                           DIMENSION: TEMPERATURE INDEX
C
C  OUTPUT: (L*4)  LDRNG()=  .TRUE.  => OUTPUT 'PECA(,)' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           .FALSE. => OUTPUT 'PECA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DIN()'.
C                           DIMENSION: DENSITY INDEX
C
C          (I*4)  NTIN    = PARAMETER = MAX. NO. OF INPUT  TEMPERATURE
C                                       VALUES. MUST BE >= 'ITA'
C          (I*4)  NDIN    = PARAMETER = MAX. NO. OF INPUT  DENSITY
C                                       VALUES. MUST BE >= 'IDA'
C          (I*4)  NTOUT   = PARAMETER = MAX. NO. OF OUTPUT TEMPERATURE
C                                       VALUES.  MUST BE >= 'ITVAL'
C          (I*4)  NDOUT   = PARAMETER = MAX. NO. OF OUTPUT DENSITY
C                                       PAIRS.  MUST BE >= 'IDVAL'
C          (I*4)  L1      = PARAMETER = 1
C
C          (I*4)  IED     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           DENSITIES.
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  ELECTRON
C                           TEMPERATURES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           TEMPERATURE INDEX .
C          (I*4)  ID      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           DENSITY INDEX .
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (R*8)  XIN()   = 1) LOG( DATA FILE ELECTRON DENSITIES    )
C                           2) LOG( DATA FILE ELECTRON TEMPERATURES )
C          (R*8)  YIN()   = LOG( DATA FILE IONIZATIONS/PHOTON )
C          (R*8)  XOUT()  = 1) LOG( SCALED USER ENTERED ELECTRON DENS. )
C                           2) LOG( SCALED USER ENTERED ELECTRON TEMPS.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED IONIZATIONS/PHOTON )
C          (R*8)  YPASS(,)= LOG( IONIZATIONS/PHOTON) INTERMEDIATE ARRAY
C                           WHICH   STORES   INTERPOLATED/EXTRAPOLATED
C                           VALUES  BETWEEN  THE  TWO  SPLINE SECTIONS.
C                           SECTIONS.
C          (R*8)  DFT()   = SPLINE INTERPOLATED DERIVATIVES (TEMPERATURE)
C          (R*8)  DFD()   = SPLINE INTERPOLATED DERIVATIVES (DENSITY)
C
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  Alessandro Lanzafame, University od Strathclyde
C
C DATE:    7th December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NTIN, NDIN, NTOUT, NDOUT, L1
C-----------------------------------------------------------------------
      PARAMETER( NTIN=24, NDIN=24, NTOUT = 20, NDOUT=24, L1=1 )
C-----------------------------------------------------------------------
      INTEGER    NTDIM, NDDIM, ITA, IDA, ITVAL, IDVAL
      INTEGER    IET, IED, IT, ID, IOPT
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TETA(ITA)             , TEDA(IDA)       ,
     &           TEVA(NTDIM)           , DIN(NTDIM)      ,
     &           PECA(NTDIM,NTDIM)           ,
     &           PEC(NTDIM,NDDIM)
      REAL*8     DFT(NTIN)    , DFD(NDIN)    ,
     &           XTIN(NTIN)   , XDIN(NDIN)   , 
     &           YTIN(NTIN)   , YDIN(NDIN)   ,
     &           XTOUT(NTOUT) , XDOUT        , 
     &           YTOUT(NTOUT) , YDOUT        ,
     &           YPASS(NTOUT,NDIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)          , LDRNG(IDVAL)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NDIN.LT.IDA)
     &             STOP ' D9SPLN ERROR: NDIN < IDA - INCREASE NDIN'
      IF (NTIN.LT.ITA)
     &             STOP ' D9SPLN ERROR: NTIN < ITA - INCREASE NTIN'
      IF (NDOUT.LT.IDVAL)
     &             STOP ' D9SPLN ERROR: NDOUT < IDVAL - INCREASE NDOUT'
      IF (NTOUT.LT.ITVAL)
     &             STOP ' D9SPLN ERROR: NTOUT < ITVAL - INCREASE NTOUT'
C
C-----------------------------------------------------------------------
C SET UP FIRST SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = 0
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON TEMPS.
C-----------------------------------------------------------------------
C
      DO IET=1,ITA
         XTIN(IET) = DLOG( TETA(IET) )
      ENDDO
      DO  IT=1,ITVAL
         XTOUT(IT) = DLOG( TEVA(IT)  )
      ENDDO
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL DATASET ELECTRON DENSITIES FOR ALL USER ELECTRON TEMP
C-----------------------------------------------------------------------
C
      DO IED=1,IDA
         DO  IET=1,ITA
            YTIN(IET) = DLOG( PEC(IET,IED) )
         ENDDO
         CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &                ITA   , XTIN    , YTIN   ,
     &                ITVAL , XTOUT   , YTOUT  ,
     &                DFT   , LTRNG
     &               )
         DO IT=1,ITVAL
            YPASS(IT,IED) = YTOUT(IT)
         ENDDO
      ENDDO
C
C-----------------------------------------------------------------------
C SET UP SECOND SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = 0
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT ELECTRON DENSITIES
C-----------------------------------------------------------------------
C
      DO IED=1,IDA
         XDIN(IED) = DLOG( TEDA(IED) )
      ENDDO
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL USER ELECTRON TEMPERATURES FOR EACH USER ELECTRON DENS
C-----------------------------------------------------------------------
C
      DO IT=1,ITVAL
         DO IED=1,IDA
            YDIN(IED) = YPASS(IT,IED)
         ENDDO
         DO ID=1,IDVAL
            XDOUT = DLOG( DIN(ID)   )
            CALL XXSPLE( LSETX , IOPT       , R8FUN1   ,
     &                   IDA   , XDIN       , YDIN     ,
     &                   L1    , XDOUT      , YDOUT    ,
     &                   DFD   , LDRNG(ID)
     &                  )
            PECA(IT,ID) = DEXP( YDOUT )
         ENDDO
      ENDDO
C
C-----------------------------------------------------------------------
C
      RETURN
      END
