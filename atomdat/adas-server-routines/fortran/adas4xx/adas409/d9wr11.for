C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9wr11.for,v 1.2 2004/07/06 13:27:11 whitefor Exp $ Date $Date: 2004/07/06 13:27:11 $
CX
      SUBROUTINE D9WR11( IUNIT  , DSNINC , DSNMTR , ELEMT ,
     &                   UID    , DATE   , IZ0    ,
     &                   NDLINE , NLINE  ,
     &                   TITL   , IZION  , CIMET  ,
     &                   NTDIM  , NDDIM  , ITMAX  , IDMAX ,
     &                   TEV    , DENS   ,
     &                   GCF
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D9WR11 *******************
C
C  PURPOSE:  TO OUTPUT DATA TO GCF PASSING FILE.
C
C  CALLING PROGRAM: ADAS409
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT NUMBER FOR RESULTS
C  INPUT : (C*80) DSNINC  = INPUT SCRIPT DATA SET NAME
C  INPUT : (C*80) DSNMTR  = INPUT ACD MASTER FILE NAME
C  INPUT : (C*2)  ELEMT   = ELEMENT SYMBOL.
C
C  INPUT : (C*6)  UID     = USER IDENIFIER
C  INPUT : (C*8)  DATE    = CURRENT DATE
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C
C  INPUT : (I*4)  NDLINE  = MAXIMUM NUMBER OF SPECTRUM LINES
C  INPUT : (I*4)  NLINE   = NUMBER OF SPECTRUM LINES
C  INPUT : (C*12) TITL()  = TITLE FOR 1ST COMPONENT OF LINE
C  INPUT : (I*4)  IZION() = ION CHARGE FOR 1ST COMPONENT OF LINE
C  INPUT : (C*1)  CIMET() = +/- SHIFT OF ION CHARGE - 1ST COMPONENT
C
C  INPUT : (I*4)  NTDIM   = MAMXIMUM NUMBER OF INPUT TEMPERATURES
C  INPUT : (I*4)  NDDIM   = MAMXIMUM NUMBER OF INPUT DENSITIES
C  INPUT : (I*4)  ITMAX   = NUMBER OF INPUT TEMPERATURES
C  INPUT : (I*4)  IDMAX   = NUMBER OF INPUT DENSITIES
C  INPUT : (R*8)  TEV()   = ELECTRON TEMPERATURES (UNITS: EV)
C  INPUT : (R*8)  DENS()  = ELECTRON DENSITIES  (UNITS: CM-3)
C
C  INPUT : (R*4)  GCF(,,) = G(TE) FUNCTION (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: LINE INDEX
C
C          (I*4) I         = GENERAL USE
C          (I*4) J         = GENERAL USE
C          (I*4) K         = GENERAL USE
C          (I*4) L         = GENERAL USE
C          (I*4) I1        = GENERAL USE
C          (I*4) I2        = GENERAL USE
C          (I*4) IT        = GENERAL USE
C          (C*80)CLINE     = GENERAL USE
C
C
C ROUTINES: NONE
C
C AUTHOR:  Alessandro Lanzafame
C
C DATE:    13th December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          			  DATE: 20-11-98
C MODIFIED: RICHARD MARTIN & MARTIN O'MULLANE
C                   - REMOVED SEARCH FOR BRACKETS IN DSNINC AND INITIAL STRING 
C                       AS STRING = '        '
C
C-----------------------------------------------------------------------
      INTEGER   NDLINE    , NLINE  , NTDIM , NDDIM
      INTEGER   IUNIT
      INTEGER   ITMAX     , IDMAX  ,IZ0
      INTEGER   I        
      INTEGER   I1        , I2     , IT,  ID
C-----------------------------------------------------------------------
      INTEGER   IZION(NLINE)
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2          , DSNINC*80   , DSNMTR*80
      CHARACTER TITL(NLINE)*12   , CLINE*80
      CHARACTER CODE*8     , SCRIPT*8   , TYPE*4
      CHARACTER UID*80      , DATE*8     , CHZ*5
C-----------------------------------------------------------------------
      CHARACTER CIMET(NLINE)*1
C-----------------------------------------------------------------------
      REAL*8   TEV(NTDIM)  , DENS(NDDIM)
      REAL*8   GCF(NTDIM,NDDIM,NDLINE)
C-----------------------------------------------------------------------
      DATA     CODE/' ADAS409'/ , TYPE/'LINE'/
C-----------------------------------------------------------------------
C
C      I1=INDEX(DSNINC,'(')
C      I2=INDEX(DSNINC,')')
C      SCRIPT = DSNINC(I1+1:I2-1)
       SCRIPT = '        ' 
C
      WRITE(IUNIT,1000) NLINE , ELEMT
C
      DO I = 1 , NLINE
         IF(CIMET(I).EQ.'+')WRITE(CHZ,1005)ELEMT,IZION(I)-1
         IF(CIMET(I).EQ.'-')WRITE(CHZ,1005)ELEMT,IZION(I)+1
         IF(CIMET(I).EQ.' ')WRITE(CHZ,1005)ELEMT,IZION(I)
 
         CLINE = ' '
         WRITE(CLINE(3:16),'(1A6,1A2,I5,1A1)') TITL(I)(7:12),
     &                     ' A',ITMAX,'/'
         CLINE(17:21)=CHZ
         CLINE(22:44)='/CODE='//CODE//'/SCRIPT= '
         WRITE(CLINE(45:52),'(1A8)')SCRIPT
         CLINE(53:70)='/TYPE= '//TYPE//'/ISEL ='
         WRITE(CLINE(71:75),'(I5)') I
         WRITE(IUNIT,'(1A80)')CLINE

CX       WRITE(IUNIT,1001)(DFLOAT(IT),IT=1,ITMAX)
CX       WRITE(IUNIT,1001)(TEV(IT),IT=1,ITMAX)
CX       WRITE(IUNIT,1001)(DENS(IT),IT=1,IDMAX)
CX       WRITE(IUNIT,1001)(GCF(IT,I),IT=1,ITMAX)

         WRITE(IUNIT,1001)(DENS(ID),ID=1,IDMAX)
         DO IT=1, ITMAX
            WRITE(IUNIT,1001) TEV(IT), (GCF(IT,ID,I),ID=1,IDMAX)
         ENDDO
      ENDDO

      WRITE(IUNIT,1002) DSNINC,DSNMTR
      DO I = 1 , NLINE
         IF(CIMET(I).EQ.'+')WRITE(CHZ,1005)ELEMT,IZION(I)-1
         IF(CIMET(I).EQ.'-')WRITE(CHZ,1005)ELEMT,IZION(I)+1
         IF(CIMET(I).EQ.' ')WRITE(CHZ,1005)ELEMT,IZION(I)
         WRITE(IUNIT,1003)I,TITL(I)(7:12),CHZ,TYPE
      ENDDO
      WRITE(IUNIT,1004)DATE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I5,4X,'/',A2,'  GENERALISED CONTRIBUTION FUNCTIONS/')
 1001 FORMAT(1P,8D9.2)
 1002 FORMAT('C',79('-'),/,'C',/
     &       'C  GENERALISED CONTRIBUTION FUNCTIONS',/,'C',/,
     &       'C  SOURCE SPECIFIC ION   FILE:',1A80,/,
     &       'C  SOURCE RECOM. COEFFT. FILE:',1A80,/,
     &       'C  ISEL  WAVELENGTH    ION    TYPE     TRANSITION',/,
     &       'C  ----  ----------   -----   ----     -----------------')
 1003 FORMAT('C ',I3,4X,1A6,'A',6X,1A5,3X,1A4)
 1004 FORMAT('C',/,'C',/,
     &       'C',50X,1A8,/,
     &       'C',79('-'))
 1005 FORMAT(1A2,'+',I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
