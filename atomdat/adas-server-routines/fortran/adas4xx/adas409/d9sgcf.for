C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9sgcf.for,v 1.1 2004/07/06 13:26:39 whitefor Exp $ Date $Date: 2004/07/06 13:26:39 $
CX
      SUBROUTINE D9SGCF( IZ0    , IZL    , IZH    ,
     &                   ISDIMD , IZDIMD , ITDIMD , IPDIMD , IMDIMD ,
     &                   NMSUM  , IZIP   , IMIP   , IPIZM  ,
     &                   NDLINE , NDCOMP ,
     &                   NLINE  , NCOMP  , SPECL  , IPLINE ,
     &                   IZION  , IMET   , CIMET  , INDPH  , CINDPH ,
     &                   IFILE  ,
     &                   NTDIM  , NDDIM  , ITMAX  , IDMAX  ,
     &                   DENS   , DENSH  ,
     &                   PECA   , LPEC   ,
     &                   FPABUN ,
     &                   GCFPEQ , GCFEQ  ,
     &                   NDRAT  , NRAT   ,
     &                   ILINE  , JLINE  ,
     &                   RATA
     &                  )
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9SGCF *********************
C
C PURPOSE : TO ASSEMBLE GCF FUNCTIONS AND THEIR COMPONENTS USING
C           FRACTIONAL METASTABLE ABUNDANCES. 2D (TEMPERATURE, DENSITY)
C           VERSION.
C
C
C
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  IZL       = MINIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  IZH       = MAXIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH
C                             IONISATION STAGE
C INPUT  : (I*4)  IMDIMD    = MAXIMUM NUMBER OF METASTABLES
C
C INPUT  : (I*4)  NMSUM     = TOTAL NUMBER OF POPULATIONS
C
C INPUT  :        IZIP()    = ION CHARGE +1 (IZ1) OF METASTABLE IN LIST
C INPUT  :        IMIP()    = METASTABLE INDEX WITHIN CHARGE STATE IZ1
C                             OF METASTABLE INDEX FROM COMPLETE LIST
C INPUT  :        IPIZM(,)  = METASTABLE INDEX IN COMPLETE LIST
C                             1ST DIM: INDEX IZ1-IZL+1
C                             2ND DIM: METASTABLE COUNT FOR STAGE (IGRD)
C INPUT  : (I*4)  NDLINE    = MAXIMUM NUMBER OF LINES ALLOWED
C INPUT  : (I*4)  NDCOMP    = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C INPUT  : (I*4)  NLINE     = NUMBER OF LINES IDENTIFIED IN SCRIPT
C INPUT  : (I*4)  NCOMP()   = NUMBER OF COMPONENTS OF SCRIPT LINE
C INPUT  : (I*4)  IZION(,)  = CHARGE STATE OF COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  IMET(,)   = METASTABLE INDEX OF COMPONENT OF
C                             SCRIPT LINE WITHIN CHARGE STATE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (C*1)  CIMET(,)  = SIGN (+, BLANK OR -) OF METASTABLE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  INDPH(,)  = PEC FILE INDEX OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (C*1   CINDPH(,) = DRIVER (E OR BLANK => ELECTRONS)
C                                    (H          => HYDROGEN )
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  IFILE(,)  = INDEX OF PEC FILE IN FILE LIST
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV VALUES
C INPUT  : (I*4)  NDDIM     = MAXIMUM NUMBER OF DDENS VALUES
C INPUT  : (I*4)  ITMAX     = NUMBER OF DTEV() VALUES
C INPUT  : (I*4)  IDMAX     = NUMBER OF DDENS() VALUES
C INPUT  : (R*8)  DENS()    = ELECTRON DENSITIES (CM-3))
C INPUT  : (R*8)  DENSH()   = HYDROGEN DENSITIES (CM-3))
C INPUT  : (R*8)  PECA(,,,) = PHOTON EMISSIVITY COEFFICIENTS (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: LINE INDEX
C                             4TH DIM: COMPONENT INDEX
C INPUT  : (L*4)  LPEC(,)   = .TRUE.  => PHOTON EMISSIVITY OBTAINED
C                             .FALSE. => PHOTON EMISSIVITY NOT OBTAINED
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C INPUT  : (R*8)  FPABUN(,,)= RESOLVED METASTABLE EQUILIBRIUM
C                             FRACTIONAL ABUNDANCES
C                             1ST DIM: - TEMPERATURE INDEX
C                             2ND DIM: - DENSITY INDEX
C                             3RD DIM: - METASTABLE INDEX
C INPUT  : (I*4)  NDRAT     = MAXIMUM NUMBER OF LINE RATIOS ALLOWED
C INPUT  : (I*4)  NRAT      = NUMBER OF RATIOS IDENTIFIED IN SCRIPT
C INPUT  : (I*4)  ILINE()   = INDEX OF NUMERATOR LINE FOR LINE RATIO
C INPUT  : (I*4)  JLINE()   = INDEX OF DENOMINATOR LINE FOR LINE RATIO
C
C OUTPUT : (C*16) SPECL(,)  = SPEC. OF POINTERS OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C OUTPUT : (I*4)  IPLINE(,) = METASTABLE POINTER OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C OUTPUT : (R*8)  GCFPEQ(,,,)=GCF FUNC. COMPONENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: LINE INDEX
C                             4TH DIM: LINE COMPONENT INDEX
C OUTPUT : (R*8)  GCFEQ(,,) = GCF FUNCTION  (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: LINE INDEX
C OUTPUT : (R*8)  RATA(,,)  = LINE GCF RATIOS
C                             1ST IND: TEMPERATURE INDEX
C                             2ND IND: DENSITY INDEX
C                             3RD IND: RATIO INDEX
C
C
C PROGRAM: (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  ID        = GENERAL INDEX FOR DENSITY
C          (I*4)  IZ        = GENERAL INDEX FOR CHARGE
C          (I*4)  IP        = GENERAL INDEX FOR CHARGE
C          (I*4)  IZ1       = GENERAL INDEX FOR CHARGE+1
C          (I*4)  IL        = GENERAL INDEX FOR LINE
C          (I*4)  IR        = GENERAL INDEX FOR RATIO
C          (I*4)  ICPT      = GENERAL INDEX FOR LINE COMPONENT
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR : Alessandro Lanzafame, University of Strathclyde
C
C DATE   : 11 December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER   IZ0     
      INTEGER   IZ1     , IZL     , IZH
      INTEGER   NTDIM   , NDDIM   , ITMAX   , IDMAX
      INTEGER   NMSUM
      INTEGER   ISDIMD  , IZDIMD  , ITDIMD  , IPDIMD  , IMDIMD
      INTEGER   NDLINE  , NDCOMP  , NLINE   , NDRAT   , NRAT
      INTEGER   IT      , ID
      INTEGER   IL      , IR      , ICPT    
C-----------------------------------------------------------------------
      INTEGER   IZIP(IMDIMD)   , IMIP(IMDIMD) ,
     &          IPIZM(IZDIMD,IPDIMD)
      INTEGER   NCOMP(NDLINE)        ,
     &          IZION(NDLINE,NDCOMP) , IMET(NDLINE,NDCOMP)  ,
     &          INDPH(NDLINE,NDCOMP) , IFILE(NDLINE,NDCOMP) ,
     &          IPLINE(NDLINE,NDCOMP)
      INTEGER   ILINE(NDRAT)         , JLINE(NDRAT)
C-----------------------------------------------------------------------
      REAL*8    DNS           , DNSH
C-----------------------------------------------------------------------
      REAL*8    DENS(NDDIM)   , DENSH(NDDIM)
      REAL*8    GCFEQ(NTDIM,NDDIM,NDLINE)
C
      REAL*8    FPABUN(NTDIM,NDDIM,IMDIMD)
C
      REAL*8    PECA(NTDIM,NDDIM,NDLINE,NDCOMP)
      REAL*8    GCFPEQ(NTDIM,NDDIM,NDLINE,NDCOMP)
C
      REAL*8    RATA(NTDIM,NDDIM,NDRAT)
C
C-----------------------------------------------------------------------
      CHARACTER SPECL(NDLINE,NDCOMP)*16
      CHARACTER CIMET(NDLINE,NDCOMP)*1 , CINDPH(NDLINE,NDCOMP)*1
C-----------------------------------------------------------------------
      LOGICAL   LPEC(NDLINE,NDCOMP)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  SET LINE TO METASTABLE CROSS-REFERENCING POINTER IPLINE AND POINTER
C  INFORMATION STRING SPECL
C-----------------------------------------------------------------------
C
      DO IL = 1,NLINE
         DO ICPT = 1,NCOMP(IL)
C
            IZ1 = IZION(IL,ICPT) + 1
            IPLINE(IL,ICPT) = IPIZM(IZ1-IZL+1,IMET(IL,ICPT))
C
            WRITE(SPECL(IL,ICPT),1000) ICPT , IZION(IL,ICPT),
     &                                 IMET(IL,ICPT),CIMET(IL,ICPT),
     &                                 INDPH(IL,ICPT),CINDPH(IL,ICPT),
     &                                 IFILE(IL,ICPT)
         ENDDO
      ENDDO
C
C-----------------------------------------------------------------------
C  CALCULATE GCF FUNCTIONS
C-----------------------------------------------------------------------
C
      DO ID=1,IDMAX
         DO IT=1,ITMAX
C
            DNS  = DENS(ID)
            DNSH = DENSH(ID)
C
            DO IL = 1, NLINE
               GCFEQ(IT,ID,IL) = 0.0D+0
               DO ICPT = 1 , NCOMP(IL)
                  IF(CINDPH(IL,ICPT).EQ.'E')THEN
                     GCFPEQ(IT,ID,IL,ICPT) = PECA(IT,ID,IL,ICPT)*
     &                    FPABUN(IT,ID,IPLINE(IL,ICPT))
                  ELSEIF(CINDPH(IL,ICPT).EQ.'H')THEN
                     GCFPEQ(IT,ID,IL,ICPT) = DNSH*PECA(IT,ID,IL,ICPT)*
     &                    FPABUN(IT,ID,IPLINE(IL,ICPT))/DNS
                  ENDIF
                  GCFEQ(IT,ID,IL)=GCFEQ(IT,ID,IL)+GCFPEQ(IT,ID,IL,ICPT)
               ENDDO
            ENDDO    
C
C-----------------------------------------------------------------------
C  CALCULATE LINE RATIOS
C-----------------------------------------------------------------------
C
            DO IR = 1,NRAT
               IF(GCFEQ(IT,ID,JLINE(IR)).GT. 0.0D0)THEN
                  RATA(IT,ID,IR) = 
     &                 GCFEQ(IT,ID,ILINE(IR))/GCFEQ(IT,ID,JLINE(IR))
               ELSE
                  RATA(IT,ID,IR) = 0.0D0
               ENDIF
            ENDDO
C
         ENDDO
      ENDDO
C     
      RETURN
C
C-----------------------------------------------------------------------
 1000 FORMAT(I2,2I3,1A1,I3,1A1,I3)
C-----------------------------------------------------------------------
C
      END
