C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9otg1.for,v 1.1 2004/07/06 13:26:13 whitefor Exp $ Date $Date: 2004/07/06 13:26:13 $
CX
      SUBROUTINE D9OTG1( LGHOST , DATE  ,
     &                   IMDIMD , NTDIM , NDDIM  ,
     &                   ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN  , YMAX   ,
     &                   NMSUM  , ITMAX , IDMAX ,
     &                   TEV    , DENS  ,
     &                   POPTIT , FPABUN, DENSEL
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9OTG1 *********************
C
C  PURPOSE:  COMMUNICATES GRAPHICS DATA TO IDL                
C
C            PROVIDES  GRAPH OF METASTABLE FRACTIONAL ABUNDANCES. A
C            SINGLE GRAPH WILL CONTAIN UP TO SEVEN METASTABLES. (IF MORE
C            THAN SEVEN METASTABLES ARE PRESENT EXTRA GRAPHS WILL  BE
C            OUTPUT AS REQUIRED). 2D (TEMPERATURE, DENSITY) VERSION.
C
C            PLOT IS LOG10(N(META.)/N(TOTAL FOR ELEMENT)) VERSUS
C                    (LOG10(ELECTRON TEMPERATURE (EV) ), DENSITY)
C
C  CALLING PROGRAM: ADAS409
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD  = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM   = MAXIMUM NUMBER OF TEMPERATURE ALLOWED
C  INPUT : (I*4)  NDDIM   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C
C  INPUT : (C*2)  ELEMT   = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*44) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR    = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF  = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN    = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX    = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN    = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX    = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM   = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX   = NUMBER OF INPUT TEMPERATURE VALUES
C  INPUT : (I*4)  IDMAX   = NUMBER OF INPUT DENSITY VALUES
C
C  INPUT : (R*8)  TEV()   = ELECTRON TEMPERATURE (UNITS: EV)
C  INPUT : (R*8)  DENS()  = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*10)POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (R*8) FPABUN(,,)= METASTABLE DEPENDENCE
C                            1ST DIMENSION: TEMPERATURE INDEX
C                            2ND DIMENSION: DENSITY INDEX
C                            3RD DIMENSION: METASTABLE INDEX
C  OUTPUT : (I*4)  DENSEL  = INDEX OF DENSITY SELECTED FOR OUTPUT
C
C          (I*4)  NDIM1   = PARAMETER = MAXIMUM NUMBER OF TEMP.   VALUES
C                           (MUST NOT BE LESS THAN 'NTDIM')
C          (I*4)  NDIM2   = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                           (MUST NOT BE LESS THAN 'IMDIMD')
C          (I*4)  NGPIC   = PARAMETER = MAXIMUM NUMBER OF LEVEL POPULAT-
C                           IONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV   = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                           WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN  = PARAMETER = IN DEFAULT GRAPH SCALING IS THE
C                           MINIMUM Y-VALUE THAT IS ALLOWED.
C                           (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT      = TEMP. INDEX NUMBER FOR ARRAY USE
C          (I*4)  ID      = DENS. INDEX NUMBER FOR ARRAY USE
C          (I*4)  IM      = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX   = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
CX          (R*4)  X()     = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
CX                            ELECTRON DENSITIES
CX          (R*4)  Y(,,)   = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
CX                            LEVEL POPULATIONS.
CX                            1ST DIMENSION = TEMPERATURE   INDEX
CX                            2ND DIMENSION = DENSITY   INDEX
CX                            3RD DIMENSION = ORDINARY  LEVEL  INDEX
C
CX          (C*80) ISPEC   = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
CX          (C*23) XTIT    = X-AXIS UNITS/TITLE
CX          (C*23) YTIT    = Y-AXIS UNITS/TITLE
CX          (C*9)  KEY0    = '    KEY: '
CX          (C*9)  MNMX0   = 'MINIMAX: '
CX          (C*9)  FILE0   = 'FILE   : '
CX          (C*8)  ADAS0   = 'ADAS   :'
CX          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C
CX          (L*4)  LGTXT   = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
CX                         = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C AUTHOR:  Alessandro Lanzafame, University of Strathclyde
C
C DATE:    11 December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV
C-----------------------------------------------------------------------
      REAL*8    GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 20  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
      PARAMETER ( GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   PIPEIN   , PIPEOU    
      INTEGER   IMDIMD   , NTDIM    , NDDIM
      INTEGER   NMSUM    , ITMAX    , IDMAX    ,
     &          IZ0
      INTEGER   IT       , ID       , IM       , 
     &          IMMAX    , DENSEL
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*44
      CHARACTER YEAR*2   , YEARDF*2
      CHARACTER DATE*8   
      CHARACTER POPTIT(IMDIMD)*10
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM) , DENS(NDDIM)
      REAL*8    FPABUN(NTDIM,NDDIM,IMDIMD)
C-----------------------------------------------------------------------

      LGTXT = .TRUE.
      IMMAX = MIN0( NGLEV , NMSUM )    
C-----------------------------------------------------------------------
C  WRITE OUT DATA TO IDL VIA UNIX PIPE 
C  SET THRESHOLD FOR METASTABLE FRACION
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) IDMAX
      CALL XXFLSH(PIPEOU)
      DO IT=1, ITMAX
         WRITE(PIPEOU, *) REAL(TEV(IT))
         CALL XXFLSH(PIPEOU)
      ENDDO
      DO ID=1, IDMAX
         WRITE(PIPEOU, *) REAL(DENS(ID))
         CALL XXFLSH(PIPEOU)
      ENDDO
      WRITE(PIPEOU, *) NMSUM
      CALL XXFLSH(PIPEOU)
      DO IT=1, ITMAX
         DO ID=1, IDMAX
            DO IM=1, NMSUM
               IF (FPABUN(IT,ID,IM).LT.GHZERO) THEN
                  WRITE(PIPEOU, *) REAL(GHZERO)
                  CALL XXFLSH(PIPEOU)
               ELSE
                  WRITE(PIPEOU, *) REAL(FPABUN(IT,ID,IM))
                  CALL XXFLSH(PIPEOU)
               ENDIF
            ENDDO
         ENDDO
      ENDDO
      DO IM=1, NMSUM
         WRITE(PIPEOU,'(A10)') POPTIT(IM)
         CALL XXFLSH(PIPEOU)
      ENDDO
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      
      READ(PIPEIN,*)DENSEL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
