C Copyright (c) 1997, Strathclyde University. 
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9spec.for,v 1.1 2004/07/06 13:26:47 whitefor Exp $ Date $Date: 2004/07/06 13:26:47 $
CX
      SUBROUTINE D9SPEC( LRSPEC ,
     &                   NDLINE , NDCOMP  , NDRAT  , NDFILE ,
     &                   NFILE  , LFILE   ,
     &                   UID    , GROUP   , TYPE   , EXT    ,
     &                   IZ0    , DSPECA  ,
     &                   NLINE  , NCOMP   ,
     &                   IZION  , IMET    , CIMET  , INDPH  ,
     &                   IFILE  ,
     &                   NTDIM  , NDDIM   , ITMAX  , IDMAX  ,
     &                   TEIN   , DEIN    , THIN   , DHIN   ,
     &                   PECA   ,
     &                   LPEC   , LTRNG   , LDRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9SPEC  ********************
C
C  PURPOSE: TO CALCULATE PHOTON EMISSIVITY COEFFICIENTS FOR
C           SPECTRAL LINES IDENTIFIED IN SCRIPT FILE
C
C  CALLING PROGRAM: ADAS409
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)   NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C  INPUT : (I*4)   NDRAT    = MAXIMUM NUMBER OF LINE RATIOS ALLOWED
C  INPUT : (I*4)   NDFILE   = MAXIMUM NUMBER OF EMISSIVITY FILES WHICH
C                             CAN BE SEARCHED
C  INPUT : (I*4)   NFILE    = NUMBER OF PEC FILES TO BE SCANNED
C  INPUT : (L*4)   LFILE()  = .TRUE.  => PEC FILE EXISTS AND MATCHES
C                             .FALSE. => PEC FILE DOES NOT EXIST/MATCH
C  INPUT : (C*6)   UID()    = USER IDENTIFIER OF PEC FILE
C  INPUT : (C*8)   GROUP()  = GROUP IDENTIFIER OF PEC FILE
C  INPUT : (C*5)   TYPE()   = TYPE IDENTIFIER OF PEC FILE
C  INPUT : (C*3)   EXT()    = EXTENSION OF PEC FILE MEMBER NAME
C  INPUT : (I*4)   IZ0      = NUCLEAR CHARGE OF IMPURITY
C  INPUT : (C*120)DSPECA()  = PHOTON EMISSIVITY SOURCE FILES
C  INPUT : (I*4)   NLINE    = NUMBER OF LINES IDENTIFIED IN SCRIPT
C  INPUT : (I*4)   NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C  INPUT : (I*4)   IZION(,) = CHARGE STATE OF COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   IMET(,)  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (C*1)   CIMET(,) = SIGN (+, BLANK OR -) OF METASTABLE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   INDPH(,) = PEC FILE INDEX OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   IFILE(,) = INDEX OF PEC FILE IN FILE LIST
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   NTDIM    = MAXIMUM NUMBER OF TEMPERATURE SETS
C  INPUT : (I*4)   NDDIM    = MAXIMUM NUMBER OF DENSITY SETS
C  INPUT : (I*4)   ITMAX    = NUMBER OF TEMPERATURE SETS
C  INPUT : (I*4)   IDMAX    = NUMBER OF DENSITY SETS
C  INPUT : (R*8)   TEIN()   = ELECTRON TEMPERATURES (EV)
C  INPUT : (R*8)   DEIN()   = ELECTRON DENSITIES (CM-3)
C  INPUT : (R*8)   THIN()   = HYDROGEN TEMPERATURES (EV)
C  INPUT : (R*8)   DHIN()   = HYDROGEN DENSITIES (CM-3)
C
C  OUTPUT: (L*4)   LRSPEC   = .TRUE.  => PEC PROCESSING DONE
C                             .FALSE. => PEC PROCESSING NOT DONE
C  OUTPUT: (R*8)   PECA(,,,)= PHOTON EMISSIVITY COEFFICIENTS (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM  DENSITY INDEX
C                             3RD DIM: LINE INDEX
C                             4RD DIM: COMPONENT INDEX
C  OUTPUT: (L*4)   LPEC(,)  = .TRUE.  => PHOTON EMISSIVITY OBTAINED
C                             .FALSE. => PHOTON EMISSIVITY NOT OBTAINED
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C
C          (I*4)   IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C          (L*4)   OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                            .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C
C ROUTINES:
C          ROUTINE   SOURCE   BRIEF DESCRIPTION
C          -----------------------------------------------------------
C	   D9SPC2   IDL-ADAS  OBTAIN PHOTON EMISSIVITY COEFFICIENT
C
C AUTHOR:  A. C. Lanzafame, University of Strathclyde
C
C DATE:    7th December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IRCODE, IT,ID
      INTEGER    NDLINE  , NDCOMP  , NDRAT   , NDFILE ,
     &           NLINE   , NFILE   , NTDIM   , NDDIM  ,
     &           INDL    , INDC    , 
     &           IZ      , IZ0     , ITMAX   , IDMAX
C-----------------------------------------------------------------------
      INTEGER    NCOMP(NDLINE)        ,
     &           IZION(NDLINE,NDCOMP) , IMET(NDLINE,NDCOMP)  ,
     &           INDPH(NDLINE,NDCOMP) , IFILE(NDLINE,NDCOMP)
C-----------------------------------------------------------------------
      REAL*8     WLNGTH
C-----------------------------------------------------------------------
      REAL*8     TEIN(NTDIM) , DEIN(NDDIM) , THIN(NTDIM) , DHIN(NDDIM) ,
     &           PECA(NTDIM,NDDIM,NDLINE,NDCOMP)
C-----------------------------------------------------------------------
      CHARACTER  TITLX*120
      CHARACTER  DSPECA(NDFILE)*120
C-----------------------------------------------------------------------
      CHARACTER  CIMET(NDLINE,NDCOMP)*1 ,
     &           UID(NDFILE)*6  , GROUP(NDFILE)*8, TYPE(NDFILE)*5 ,
     &           EXT(NDFILE)*3
C-----------------------------------------------------------------------
      LOGICAL    OPEN10      , LRSPEC    
C-----------------------------------------------------------------------
      LOGICAL    LFILE(NDFILE) , LPEC(NDLINE,NDCOMP)
      LOGICAL    LTRNG(NTDIM)  , LDRNG(NTDIM)
C-----------------------------------------------------------------------
      DATA OPEN10 /.FALSE./
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      IF(LRSPEC) RETURN
C
C-----------------------------------------------------------------------
C  CHECK FILE FOR EACH COMPONENT
C-----------------------------------------------------------------------
C
      DO INDL=1,NLINE
         DO INDC=1,NCOMP(INDL)
            IF(CIMET(INDL,INDC).EQ.'+') THEN
               IZ=IZION(INDL,INDC)-1
            ELSEIF(CIMET(INDL,INDC).EQ.'-')THEN
               IZ=IZION(INDL,INDC)+1
            ELSE
               IZ=IZION(INDL,INDC)
            ENDIF
            CALL D9SPC2( DSPECA(IFILE(INDL,INDC)), INDPH(INDL,INDC) ,
     &                   IZ , IZ0 , ITMAX , IDMAX , TEIN , DEIN,
     &                   WLNGTH, PECA(1,1,INDL,INDC), LTRNG, LDRNG,
     &                   TITLX , IRCODE
     &                  )
            IF(IRCODE.GT.0) THEN
               LPEC(INDL,INDC) = .FALSE.
            ELSE
               LPEC(INDL,INDC) = .TRUE.
            ENDIF
         ENDDO
      ENDDO
      LRSPEC = .TRUE.
C-----------------------------------------------------------------------
      RETURN
      END
