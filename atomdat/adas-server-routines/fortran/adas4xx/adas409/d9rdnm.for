C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9rdnm.for,v 1.1 2004/07/06 13:26:33 whitefor Exp $ Date $Date: 2004/07/06 13:26:33 $
CX
      SUBROUTINE D9RDNM( DSNINC , LPART  , IFAIL  ,
     &                   IZ0    , NPART  , IPRTD  , IGRDD , ICLASS ,
     &                   IZ1    , ITMAX  , IDMAX  ,
     &                   ISDIMD , IZDIMD , ITDIMD ,
     &                   ISMAXD , IZMAXD , ITMAXD , IDMAXD , NPARTR,
     &                   DTEV   , DDENS  ,
     &                   DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                   DRCOFI
     &                 )
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9RDNM *********************
C
C PURPOSE : TO EXTRACT COLLISIONAL DIELECTRONIC DATA FOR A
C           (TEMPERATURE, DENSITY) GRID FROM
C           EITHER PARTIAL (METASTABLE/PARENT RESOLVED) OR STANDARD
C           (UNRESOLVED) ISONUCLEAR MASTER FILES
C
C NOTE    : THE SOURCE DATA IS CONTAINED AS SEQUENTIAL DATASETS
C           WITH THE FOLLOWING NAMING CONVENTIONS:
C
C                   (1) JETSHP.ACD<YR>#<EL).<CODE>DATA
C                   (2) JETSHP.SCD<YR>#<EL>.<CODE>DATA
C                   (3) JETSHP.CCD<YR>#<EL>.<CODE>DATA
C                   (4) JETSHP.PRB<YR>#<EL>.<FILT>.<CODE>DATA
C                   (5) JETSHP.PRC<YR>#<EL>.<FILT>.<CODE>DATA
C                   (6) JETSHP.QCD<YR>#<EL>.<CODE>DATA
C                   (7) JETSHP.XCD<YR>#<EL>.<CODE>DATA
C                   (8) JETSHP.PLT<YR>#<EL>.<CODE>DATA
C                   (9) JETSHP.PLS<YR>#<EL>.<CODE>DATA
C
C       WHERE, <YR>   = TWO DIGIT YEAR NUMBER
C              <EL>   = ONE OR TWO CHARACTER ELEMENT SYMBOL
C              <CODE> = R       => PARTIAL DATA
C                       U       => PARTIAL DATA
C                       OMITTED => STANDARD DATA
C              <FILT> = SIX CHARACTER POWER FILTER CODE
C
C       AND DATA OF CLASSES 6 AND 7 DO NOT EXIST FOR THE PARTIAL CASE.
C
C
C INPUT  : (C*120) DSNINC   = ISONUCLEAR MASTER FILE NAME - VERIFIED
C                             AND READY FOR DYNAMIC ALLOCATION.
C INPUT  : (L*4)  LPART     = .TRUE.  => PARTIAL (RESOLVED) MASTER DATA
C                            . FALSE. => UNSRESOLVED MASTER DATA
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX ON INPUT
C INPUT  : (I*4)  IPRTD     = REQUIRED PARENT INDEX
C INPUT  : (I*4)  IGRDD     = REQUIRED GROUND INDEX
C INPUT  : (I*4)  ICLASS    = CLASS OF DATA (1 - 9 )
C INPUT  : (I*4)  IZ1       = REQUIRED ION CHARGE + 1
C INPUT  : (I*4)  ITMAX     = NUMBER OF DTEV() VALUES 
C INPUT  : (I*4)  IDMAX     = NUMBER OF DDENS() VALUES
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISONUCLEAR MASTER FILES
C INPUT  : (R*8)  DTEV()    = DLOG10(ELECTRON TEMPERATURES (EV))
C INPUT  : (R*8)  DDENS()   = DLOG10(ELECTRON DENSITIES (CM-3))
C
C OUTPUT : (I*4)  IFAIL     = 0    IF ROUTINE SUCCESSFUL - DATA FOR THE
C                                  REQUESTED YEAR USED.
C                           = 1    IF ROUTINE OPEN STATEMENT FAILED
C                           = 2    IF FILE EXISTS BUT REQUIRED DATA
C                                  BLOCK DOES NOT
C OUTPUT : (I*4)  ISMAXD    = NUMBER OF (CHARGE, PARENT, METASTABLE)
C                             BLOCKS IN SELECTED MASTER FILE
C OUTPUT : (I*4)  IZMAXD    = NUMBER OF ZDATA() VALUES IN SELECTED
C                             MASTER FILE
C OUTPUT : (I*4)  ITMAXD    = NUMBER OF DTEVD() VALUES IN SELECTED
C                             MASTER FILE
C OUTPUT : (I*4)  IDMAXD    = NUMBER OF DDENSD() VALUES IN SELECTED
C                             MASTER FILE
C OUTPUT : (I*4)  NPARTR()  = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX FOUND IN MASTER FILE
C OUTPUT : (R*8)  DTEVD()   = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DDENSD()  = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DRCOFD(,,)= DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C                             IN SELECTED MASTER FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C OUTPUT : (R*8)  ZDATA()   = CHARGE + 1 FOR IONS IN SELECTED MASTER
C                             FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C OUTPUT : (R*8)  DRCOFI(,) = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C
C PROGRAM: (C*80) DSNOLD    = FILE NAME USED IN PREVIOUS CALL
C          (C*80) CLINE     = GENERAL CHARACTER VARIABLE
C          (C*80) CTERM     = TERMINATOR LINE - '-' FILLED VARIABLE
C          (C*4)) CPATRN()  = PATTERN USED TO DETECT DATA CLASS
C          (I*4)  IZ0D      = NUCLEAR CHARGE READ FROM MASTER FILE
C          (I*4)  IZ1MIN    = MINIMUM CHARGE+1 READ FROM MASTER FILE
C          (I*4)  IZ1MAX    = MAXIMUM CHARGE+1 READ FROM MASTER FILE
C          (I*4)  IABT      = ABORT CODE
C          (I*4)  INDSEL    = LOCATION OF (CHARGE,PRNT,GRND)
C                             DATA BLOCK IN FILE
C          (I*4)  IZDAT     = CURRENT DATA BLOCK ION CHARGE +1
C          (I*4)  ISEL      = GENERAL INDEX
C          (I*4)  I         = GENERAL INDEX
C          (I*4)  IT        = GENERAL INDEX
C          (I*4)  ID        = GENERAL INDEX
C          (I*4)  IZCHK     = INDEX TO VERIFY DATA Z1 SET COMPLETE
C          (I*4)  IPRTR()   = PARENT INDICES IN DATA SET
C          (I*4)  IGRDR()   = GROUND INDICES IN DATA SET
C          (I*4)  LCK       = MUST BE GREATER THAN 'ITMAXD' & 'IDMAXD'
C                             & 'ITMAX' - ARRAY SIZE FOR SPLINE CALCS.
C          (R*8)  A()       = GENERAL ARRAY
C          (R*8)  DRCOF0(,) = INTERPOLATION OF DRCOFD(,,) W.R.T DTEV()
C          (L*8)  LEXIST    = TRUE --- FILE TO OPEN EXISTS ELSE NOT
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                             SPLINE ROUTINE 'XXSPLN', SEE 'XXSPLN'.
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO X-AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO X-AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLN')
C          (R*8)  DY()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4FCTN     ADAS      CONVERT STRING TO INTEGER FORM
C
C          (R*8 ADAS FUNCTION - 'R8FUN1' ( X -> X) )
C
C AUTHOR : Alessandro Lanzafame
C
C DATE   : 8th December 1995
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      INTEGER   IUNT12   , LCK
      INTEGER   L1
C-----------------------------------------------------------------------
      PARAMETER ( L1      =  1 )
      PARAMETER ( IUNT12  = 12 , LCK = 100 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT  , I4FCTN  , IOPT
      INTEGER   IFAIL   , IZ0     , IPRTD   , IGRDD   , ICLASS
      INTEGER   IZ1     , ITMAX   , IDMAX
      INTEGER   ISDIMD  , IZDIMD  , ITDIMD
      INTEGER   ISMAXD  , IZMAXD  , ITMAXD  , IDMAXD
      INTEGER   IZ0D    , IZ1MIN  , IZ1MAX  , IABT    , IZDAT
      INTEGER   INDSEL  , I       , IT      , ID      , ISEL
      INTEGER   IZCHK
C-----------------------------------------------------------------------
      INTEGER   NPART(IZDIMD) , NPARTR(IZDIMD)
      INTEGER   IGRDR(LCK) , IPRTR(LCK)
C-----------------------------------------------------------------------
      REAL*8    R8FUN1
C-----------------------------------------------------------------------
      REAL*8    DTEV(ITMAX)   , DDENS(IDMAX)
      REAL*8    DTEVD(ITDIMD) , DDENSD(ITDIMD) , ZDATA(ISDIMD)
      REAL*8    DRCOFD(ISDIMD,ITDIMD,ITDIMD)
      REAL*8    DRCOFI(ITMAX,IDMAX)
      REAL*8    A(LCK)  ,   DY(LCK)  , DRCOF0(LCK,LCK)
C-----------------------------------------------------------------------
      CHARACTER DSNINC*120 , DSNOLD*80
      CHARACTER CLINE*80  , CTERM*80   ,  CHINDI*4
C-----------------------------------------------------------------------
      CHARACTER CPATRN(9)*4
C-----------------------------------------------------------------------
      LOGICAL   LEXIST  , LSETX , LPART , LREAD
C-----------------------------------------------------------------------
      EXTERNAL  R8FUN1
C-----------------------------------------------------------------------
      DATA      LREAD  /.FALSE./
      DATA      DSNOLD /' '/
       DATA      CTERM /'-----------------------------------------------
     &---------------------------------'/
      DATA      CPATRN/'IPRT','IPRT','IPRT','IPRT','IPRT',
     &                 'IGRD','IPRT','IGRD','IGRD'/
C-----------------------------------------------------------------------
      SAVE      LREAD , DSNOLD , IZ1MIN
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ON FIRST ENTRY: MAKE SURE SET ARRAY BOUNDS ARE VALID
C-----------------------------------------------------------------------
C
      IF (LCK.LT.ITDIMD) STOP
     &     ' D9RDNM ERROR: ARRAY DIMENSION LCK < ITDIMD'
      IF (LCK.LT.ISDIMD) STOP
     &     ' D9RDNM ERROR: ARRAY DIMENSION LCK < ISDIMD'
      IF (LCK.LT.ITMAX ) STOP
     &     ' D9RDNM ERROR: ARRAY DIMENSION LCK < ITMAX'
      IF (LCK.LT.IDMAX ) STOP
     &     ' D9RDNM ERROR: ARRAY DIMENSION LCK < IDMAX'
C
C-----------------------------------------------------------------------
C BYPASS MASTER FILE READ IF PREVIOUSLY DONE
C-----------------------------------------------------------------------
C
      IFAIL  = 0
      IF(DSNINC.EQ.DSNOLD.AND.LREAD) GO TO 20
C
C-----------------------------------------------------------------------
C  CONFIRMATORY CHECK THAT ISONUCLEAR MASTER FILE FILE
C-----------------------------------------------------------------------
C
      INQUIRE(FILE=DSNINC,EXIST=LEXIST)
C
      IF( .NOT.LEXIST ) GOTO 9999
C
C-----------------------------------------------------------------------
C  READ FILE.  REVERIFY THAT TYPE IS CORRECLY IDENTIFIED
C-----------------------------------------------------------------------
C
      OPEN(UNIT=IUNT12,FILE=DSNINC,STATUS='UNKNOWN',ERR=9999)
C
      READ(IUNT12,'(A80)')CLINE
      IZ0D = I4FCTN(CLINE(1:5),IABT)
      IF(IABT.GT.0.OR.IZ0D.NE.IZ0) THEN
         WRITE(I4UNIT(-1),2001)'INCORRECT NUCLEAR CHARGE'
         WRITE(I4UNIT(-1),2002)
         STOP
      ENDIF
      IDMAXD = I4FCTN(CLINE(6:10),IABT)
      IF(IABT.GT.0.OR.IDMAXD.LE.0.OR.IDMAXD.GT.ITDIMD) THEN
         WRITE(I4UNIT(-1),2001)'INVALID NUMBER OF DENSITIES'
         WRITE(I4UNIT(-1),2002)
         STOP
      ENDIF
      ITMAXD = I4FCTN(CLINE(11:15),IABT)
      IF(IABT.GT.0.OR.ITMAXD.LE.0.OR.ITMAXD.GT.ITDIMD) THEN
         WRITE(I4UNIT(-1),2001)'INVALID NUMBER OF TEMPERATURES'
         WRITE(I4UNIT(-1),2002)
         STOP
      ENDIF
      IZ1MIN = I4FCTN(CLINE(16:20),IABT)
      IZ1MAX = I4FCTN(CLINE(21:25),IABT)
      IF(IABT.GT.0.OR.IZ1MIN.GT.IZ1MAX.OR.IZ1MIN.LT.1
     &     .OR.IZ1MAX.GT.IZ0) THEN
         WRITE(I4UNIT(-1),2001)'INCORRECT ION LIMITS'
         WRITE(I4UNIT(-1),2002)
         STOP
      ENDIF
      READ(IUNT12,'(A80)') CLINE
      IF(LPART) THEN
         READ(IUNT12,'(A80)') CLINE
         IF (INDEX(CLINE,'.').GT.0)THEN
            WRITE(I4UNIT(-1),2001)'INCORRECT STRUCTURE'
            WRITE(I4UNIT(-1),2002)
            STOP
         ELSE
            READ(CLINE,'(16I5)') (NPARTR(I),I=1,
     &           MIN0(IZ1MAX-IZ1MIN+2,16))
         ENDIF
         IF(IZ1MAX+IZ1MAX-2.GT.16) THEN
            READ(IUNT12,'(16I5)') (NPARTR(I),I=17,IZ1MAX-IZ1MIN+2)
         ENDIF
         DO I=1,IZ1MAX-IZ1MIN+2
            IF(NPART(I).NE.NPARTR(I))THEN
               WRITE(I4UNIT(-1),2001)'PARTITION MISMATCH'
               WRITE(I4UNIT(-1),2002)
               STOP
            ENDIF
         ENDDO
         READ(IUNT12,'(A80)') CLINE
         IF(CLINE(1:5).NE.'-----') THEN
            WRITE(I4UNIT(-1),2001)'INCORRECT STRUCTURE'
            WRITE(I4UNIT(-1),2002)
            STOP
         ENDIF
      ENDIF
C     
      READ(IUNT12,1040) ( DDENSD(ID) , ID = 1 , IDMAXD )
      READ(IUNT12,1040) ( DTEVD(IT) , IT = 1 , ITMAXD )
C     
      CHINDI = CPATRN(ICLASS)
C
      ISMAXD = 0
 10   READ(IUNT12,'(A80)',END=17)CLINE
      IF(CLINE(2:80).NE.CTERM(2:80)) THEN
         IF(LPART) THEN
            IF( CLINE(24:27) .EQ. CHINDI) THEN
               ISMAXD = ISMAXD + 1
               READ(CLINE,1070) IPRTR(ISMAXD), IGRDR(ISMAXD), IZDAT
               ZDATA(ISMAXD) = DFLOAT( IZDAT )
            ELSE
               WRITE(0,*)CLINE
               WRITE(I4UNIT(-1),2001)'INCORRECT CLASS IPRT CODE',
     &              ICLASS,CHINDI
               WRITE(I4UNIT(-1),2002)
               STOP
            ENDIF
         ELSE
            ISMAXD = ISMAXD + 1
            READ(CLINE,1071) IZDAT
            ZDATA(ISMAXD) = DFLOAT( IZDAT )
         ENDIF
         DO IT = 1 , ITMAXD
            READ(IUNT12,1040) (DRCOFD(ISMAXD,IT,ID), ID=1,IDMAXD)
         ENDDO
         GO TO 10
      ENDIF
C
 17   LREAD = .TRUE.
      DSNOLD = DSNINC
      CLOSE(IUNT12)
C
C-----------------------------------------------------------------------
C      VERIFY Z1 SET IN MASTER FILE CONSISTENT WITH IZ1MIN AND IZ1MAX
C      EXCEPT FOR XCD AND QCD CASES
C-----------------------------------------------------------------------
C
      IF(ICLASS.EQ.6.OR.ICLASS.EQ.7) THEN
         IZMAXD = IZ1MAX-IZ1MIN+1
      ELSE
         IZCHK = IZ1MIN
         DO I=1,ISMAXD
            IZDAT = DINT(ZDATA(I))
            IF(IZDAT.NE.IZCHK) THEN
               IZCHK = IZCHK+1
            ENDIF
         ENDDO
         IF(IZCHK.EQ.IZ1MAX) THEN
            IZMAXD = IZ1MAX-IZ1MIN+1
         ELSE
            WRITE(I4UNIT(-1),2001)'INCONSISTENT Z1 SET IN FILE'
            WRITE(I4UNIT(-1),2002)
            STOP
         ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C      SELECT CORRECT DATA BLOCK REQUESTED
C-----------------------------------------------------------------------
C
 20   CONTINUE
C
      INDSEL = 0
      DO ISEL = 1,ISMAXD
         IZDAT = DINT( ZDATA(ISEL) )
         IF(LPART.AND.(ICLASS.NE.8).AND.(ICLASS.NE.9)) THEN
            IF( IZDAT .EQ. IZ1  .AND. IPRTR(ISEL) .EQ. IPRTD .AND.
     &           IGRDR(ISEL) .EQ. IGRDD) THEN
               INDSEL = ISEL
            ENDIF
         ELSEIF(LPART.AND.((ICLASS.EQ.8).OR.(ICLASS.EQ.9))) THEN
            IF( IZDAT .EQ. IZ1  .AND. IPRTR(ISEL) .EQ. IPRTD ) THEN
               INDSEL = ISEL
            ENDIF
         ELSE
            IF( IZDAT .EQ. IZ1 ) THEN
               INDSEL = ISEL
            ENDIF
         ENDIF
      ENDDO
C
      IF( INDSEL .EQ. 0 ) THEN
         IFAIL = 2
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C      INTERPOLATE USING SPLINES
C-----------------------------------------------------------------------
C
C
C>>>>>>INTERPOLATE DRCOFD(,,,) W.R.T TEMPERATURE
C
      LSETX = .TRUE.
      IOPT  = 0
      DO ID = 1 , IDMAXD
         DO IT = 1 , ITMAXD
            A(IT) = DRCOFD(INDSEL,IT,ID)
         ENDDO
         CALL XXSPLN( LSETX  , IOPT  , R8FUN1       ,
     &                ITMAXD , DTEVD , A            ,
     &                ITMAX  , DTEV  , DRCOF0(1,ID) ,
     &                DY
     &               )
      ENDDO
C
C>>>>>>INTERPOLATE ABOVE RESULT W.R.T DENSITY
C     
      LSETX = .TRUE.
      IOPT  = 0
      DO IT = 1 , ITMAX
         DO ID = 1 , IDMAXD
            A(ID) = DRCOF0(IT,ID)
         ENDDO
         DO ID = 1, IDMAX
            CALL XXSPLN( LSETX  , IOPT      , R8FUN1        ,
     &                   IDMAXD , DDENSD    , A             ,
     &                   L1     , DDENS(ID) , DRCOFI(IT,ID) ,
     &                   DY
     &                  )
         ENDDO
      ENDDO
      RETURN
C
C-----------------------------------------------------------------------
C DATA SET OPENING/EXISTENCE ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999  IFAIL  = 1
       RETURN
C
C-----------------------------------------------------------------------
C
 1000  FORMAT('FILE = ',1A30)
 1010  FORMAT(5I5)
 1040  FORMAT(8F10.5)
 1050  FORMAT(I6)
 1060  FORMAT(1X,'NOTE: REQUESTED DATASET - ',A30,' DOES NOT EXIST.'/
     1        7X,      'USING DEFAULT YEAR (',A2,') DATASET INSTEAD'/)
 1070  FORMAT(28X,I2,9X,I2,16X,I2)
 1071  FORMAT(57X,I2)
C
 2001 FORMAT(1X,32('*'),' D9RDNM ERROR ',32('*')//
     &       1X,'FAULT IN MASTER DATA FILE: ',A,I3,A)
 2002 FORMAT(/1X,28('*'),'  PROGRAM TERMINATED   ',27('*'))
C
 2010  FORMAT(1H ,'INDSEL=',I2,3X,'IZDAT =',I2,3X,
     &            'IPRTD =',I2,3X,'IGRDD =',I2,/
     &        /7F10.5/7F10.4/7F10.5)
C-----------------------------------------------------------------------
C
       END
