C Copyright (c) 1997, Strathclyde University.
CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas409/d9data.for,v 1.1 2004/07/06 13:26:01 whitefor Exp $ Date $Date: 2004/07/06 13:26:01 $
CX
       SUBROUTINE D9DATA( DSFLLA , LSELA  , LEXSA  , LDEFA , LPART  ,
     &                    IZ0    , IZ1MIN , IZ1MAX , NPART  ,
     &                    NTDIM  , NDDIM  , ITMAX  , IDMAX  ,
     &                    ISDIMD , IZDIMD , ITDIMD , IPDIMD , NPARTR,
     &                    DTEV   , DDENS  ,
     &                    DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                    DRCOFI ,
     &                    ACDA   , LACDA  ,
     &                    SCDA   , LSCDA  ,
     &                    CCDA   , LCCDA  ,
     &                    PRBA   , LPRBA  ,
     &                    PRCA   , LPRCA  ,
     &                    QCDA   , LQCDA  ,
     &                    XCDA   , LXCDA  ,
     &                    PLTA   , LPLTA
     &                  )
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D9DATA *********************
C
C PURPOSE : TO EXTRACT A COMPLETE SET OF COLLISIONAL DIELECTRONIC DATA
C           FOR A (TEMPERATURE, DENSITY) GRID
C           FROM EITHER PARTIAL (METASTABLE/PARENT RESOLVED) OR STANDARD
C           (UNRESOLVED) ISONUCLEAR MASTER FILES
C
C           DERIVED FROM D5DATA
C
C NOTE    : THE SOURCE DATA IS CONTAINED AS SEQUENTIAL DATASETS
C           WITH THE FOLLOWING NAMING CONVENTIONS:
C
C                   (1) JETSHP.ACD<YR>#<EL).<CODE>DATA
C                   (2) JETSHP.SCD<YR>#<EL>.<CODE>DATA
C                   (3) JETSHP.CCD<YR>#<EL>.<CODE>DATA
C                   (4) JETSHP.PRB<YR>#<EL>.<FILT>.<CODE>DATA
C                   (5) JETSHP.PRC<YR>#<EL>.<FILT>.<CODE>DATA
C                   (6) JETSHP.QCD<YR>#<EL>.<CODE>DATA
C                   (7) JETSHP.XCD<YR>#<EL>.<CODE>DATA
C                   (8) JETSHP.PLT<YR>#<EL>.<CODE>DATA
C
C       WHERE, <YR>   = TWO DIGIT YEAR NUMBER
C              <EL>   = ONE OR TWO CHARACTER ELEMENT SYMBOL
C              <CODE> = R       => PARTIAL DATA
C                       U       => PARTIAL DATA
C                       OMITTED => STANDARD DATA
C              <FILT> = SIX CHARACTER POWER FILTER CODE
C
C       AND DATA OF CLASSES 6 AND 7 DO NOT EXIST FOR THE STANDARD CASE.
C
C
C INPUT  : (C*120)DSFLLA()  = MASTER FILE DATA SET NAMES (FULL MVS DSN)
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C INPUT  : (L*4)  LSELA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        INDEX SELECTED
C                           = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                        NOT SELECTED
C INPUT  : (L*4)  LEXSA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        SELECTED INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS SELECTED INDEX
C INPUT  : (L*4)  LDEFA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        DEFAULT YEAR INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS DEFAULT YEAR INDEX
C INPUT  : (L*4)  LPART     = .TRUE.  => PARTIAL DATA SELECTED
C                           = .FALSE. => STANDARD DATA SELECTED
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  IZ1MIN    = MINIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  IZ1MAX    = MAXIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX ON INPUT
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV VALUES
C INPUT  : (I*4)  NDDIM     = MAXIMUM NUMBER OF DDENS VALUES
C INPUT  : (I*4)  ITMAX     = NUMBER OF DTEV() VALUES
C INPUT  : (I*4)  IDMAX     = NUMBER OF DDENS() VALUES
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH
C                             IONISATION STAGE
C INPUT  : (R*8)  DTEV()    = DLOG10(ELECTRON TEMPERATURES (EV))
C INPUT  : (R*8)  DDENS()   = DLOG10(ELECTRON DENSITIES (CM-3))
C
C OUTPUT : (I*4)  NPARTR()  = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX FOUND IN MASTER FILE
C OUTPUT : (R*8)  DTEVD()   = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DDENSD()  = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DRCOFD(,,)= DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C                             IN SELECTED MASTER FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C OUTPUT : (R*8)  ZDATA()   = CHARGE + 1 FOR IONS IN SELECTED MASTER
C                             FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C OUTPUT : (R*8)  DRCOFI(,) = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C OUTPUT : (R*8)  ACDA(,,,,)= INTERPOLATION OF ACD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4RD DIM: RECOMBINING METASTABLE INDEX
C                             5TH DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (L*4)  LACDA(,,) = .TRUE.  => ACD COEFFICIENT AVAILABLE
C                             .FALSE. => ACD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (R*8)  SCDA(,,,,)= INTERPOLATION OF SCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4RD DIM: RECOMBINING METASTABLE INDEX
C                             5TH DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (L*4)  LSCDA(,,) = .TRUE.  => SCD COEFFICIENT AVAILABLE
C                             .FALSE. => SCD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (R*8)  CCDA(,,,,)= INTERPOLATION OF CCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4RD DIM: RECOMBINING METASTABLE INDEX
C                             5TH DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (L*4)  LCCDA(,,) = .TRUE.  => CCD COEFFICIENT AVAILABLE
C                             .FALSE. => CCD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (R*8)  PRBA(,,,) = INTERPOLATION OF PRB COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (L*4)  LCCDA(,)  = .TRUE.  => PRB COEFFICIENT AVAILABLE
C                             .FALSE. => PRB COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (R*8)  PRCA(,,,) = INTERPOLATION OF PRC COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (L*4)  LPRCA(,)  = .TRUE.  => PRC COEFFICIENT AVAILABLE
C                             .FALSE. => PRC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (R*8)  QCDA(,,,,)= INTERPOLATION OF QCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: FIRST METASTABLE INDEX
C                             5TH DIM: SECOND METASTABLE INDEX
C OUTPUT : (L*4)  LQCDA(,,) = .TRUE.  => QCD COEFFICIENT AVAILABLE
C                             .FALSE. => QDC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: FIRST METASTABLE INDEX
C                             3RD DIM: SECOND METASTABLE INDEX
C OUTPUT : (R*8)  XCDA(,,,,)= INTERPOLATION OF XCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: FIRST PARENT METASTABLE INDEX
C                             5TH DIM: SECOND PARENT METASTABLE INDEX
C OUTPUT : (L*4)  LXCDA(,,) = .TRUE.  => XCD COEFFICIENT AVAILABLE
C                             .FALSE. => XDC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: FIRST PARENT METASTABLE INDEX
C                             3RD DIM: SECOND PARENT METASTABLE INDEX
C OUTPUT : (R*8)  PLTA(,,,) = INTERPOLATION OF PLT COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: DENSITY INDEX
C                             3RD DIM: CHARGE STATE INDEX
C                             4TH DIM: METASTABLE INDEX
C OUTPUT : (L*4)  LPLTA(,)  = .TRUE.  => PLT COEFFICIENT AVAILABLE
C                             .FALSE. => PLT COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM:  METASTABLE INDEX
C
C PROGRAM: (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  ID        = GENERAL INDEX FOR DENSITY
C          (I*4)  IZ        = GENERAL INDEX FOR CHARGE
C          (I*4)  IZ1       = GENERAL INDEX FOR CHARGE+1
C          (I*4)  IPRT      = GENERAL INDEX FOR PARENT METASTABLE
C          (I*4)  JPRT      = GENERAL INDEX FOR PARENT METASTABLE
C          (I*4)  IGRD      = GENERAL INDEX FOR METASTABLE
C          (I*4)  JGRD      = GENERAL INDEX FOR METASTABLE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR: Alessandro Lanzafame, University of Strathclyde
C
C DATE:   21 October 1996
C
C-----------------------------------------------------------------------
C
C VERSION: 1.1                                          DATE: 12-03-98
C MODIFIED: RICHARD MARTIN
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
       INTEGER   IFAIL   , IZ0     , ICLASS
       INTEGER   IZ1     , IZ1MIN  , IZ1MAX  , NTDIM   , NDDIM
       INTEGER   ITMAX   , IDMAX
       INTEGER   ISDIMD  , IZDIMD  , ITDIMD  , IPDIMD
       INTEGER   ISMAXD  , IZMAXD  , ITMAXD  , IDMAXD
       INTEGER   IT      , ID
       INTEGER   IZ      , IPRT    , JPRT    , IGRD    , JGRD
C-----------------------------------------------------------------------
       INTEGER   NPART(IZDIMD)     , NPARTR(IZDIMD)
C-----------------------------------------------------------------------
       REAL*8    DTEV(ITMAX)   , DDENS(IDMAX)
       REAL*8    DTEVD(ITDIMD) , DDENSD(ITDIMD) , ZDATA(ISDIMD)
       REAL*8    DRCOFD(ISDIMD,ITDIMD,ITDIMD)
       REAL*8    DRCOFI(ITMAX,IDMAX)
       REAL*8    ACDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    SCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    CCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    PRBA(NTDIM,NDDIM,IZDIMD,IPDIMD)
       REAL*8    PRCA(NTDIM,NDDIM,IZDIMD,IPDIMD)
       REAL*8    QCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    XCDA(NTDIM,NDDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    PLTA(NTDIM,NDDIM,IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
       CHARACTER DSFLLA(8)*120 , DSNINC*120
C-----------------------------------------------------------------------
       LOGICAL   LPART
C-----------------------------------------------------------------------
       LOGICAL   LSELA(8)      , LEXSA(8)       , LDEFA(8)
       LOGICAL   LACDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LSCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LCCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LPRBA(IZDIMD,IPDIMD)
       LOGICAL   LPRCA(IZDIMD,IPDIMD)
       LOGICAL   LQCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LXCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LPLTA(IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C      ZERO ACD, SCD, CCD, PRB, PRC, QCD, XCD, PLT MATRICES
C      AND SET ASSOCIATED LOGICAL VARIABLES TO FALSE
C-----------------------------------------------------------------------
C
C       DO ID=1,NDDIM
C          DO IT = 1,NTDIM
C             DO IZ = 1,IZDIMD
C                DO IPRT = 1,IPDIMD
C                   DO IGRD = 1,IPDIMD
C                      ACDA(IT,ID,IZ,IPRT,IGRD) = 0.0D+0
C                      LACDA(IZ,IPRT,IGRD)   = .FALSE.
C                      SCDA(IT,ID,IZ,IPRT,IGRD) = 0.0D+0
C                      LSCDA(IZ,IPRT,IGRD)   = .FALSE.
C                      CCDA(IT,ID,IZ,IPRT,IGRD) = 0.0D+0
C                      LCCDA(IZ,IPRT,IGRD)   = .FALSE.
C                      QCDA(IT,ID,IZ,IPRT,IGRD) = 0.0D+0
C                      LQCDA(IZ,IPRT,IGRD)   = .FALSE.
C                      XCDA(IT,ID,IZ,IPRT,IGRD) = 0.0D+0
C                      LXCDA(IZ,IPRT,IGRD)   = .FALSE.
C                   ENDDO
C                   PRBA(IT,ID,IZ,IPRT) = 0.0D+0
C                   LPRBA(IZ,IPRT)   = .FALSE.
C                   PRCA(IT,ID,IZ,IPRT) = 0.0D+0
C                   LPRCA(IZ,IPRT)   = .FALSE.
C                   PLTA(IT,ID,IZ,IPRT) = 0.0D+0
C                   LPLTA(IZ,IPRT)   = .FALSE.
C                ENDDO
C             ENDDO
C          ENDDO
C       ENDDO
C
C-----------------------------------------------------------------------
C      FETCH ACD, SCD, CCD, PRB, PRC DATA
C-----------------------------------------------------------------------
C
       DO ICLASS = 1,5
          IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
             DSNINC = DSFLLA(ICLASS)
             DO IZ1=IZ1MIN,IZ1MAX
                DO IPRT = 1, NPART(IZ1-IZ1MIN+2)
                   DO IGRD = 1, NPART(IZ1-IZ1MIN+1)
                      IFAIL = 0
                      CALL D9RDNM(DSNINC,LPART ,IFAIL ,
     &                            IZ0   ,NPART ,IPRT  ,IGRD ,ICLASS ,
     &                            IZ1   ,ITMAX ,IDMAX ,
     &                            ISDIMD,IZDIMD,ITDIMD,
     &                            ISMAXD,IZMAXD,ITMAXD,IDMAXD,NPARTR,
     &                            DTEV  ,DDENS ,
     &                            DTEVD ,DDENSD,DRCOFD,ZDATA ,
     &                            DRCOFI
     &                            )
C
                      IF( IFAIL.EQ.0 ) THEN
                         IF(ICLASS.EQ.1)
     &                        LACDA(IZ1-IZ1MIN+1,IPRT,IGRD) = .TRUE.
                         IF(ICLASS.EQ.2) 
     &                        LSCDA(IZ1-IZ1MIN+1,IPRT,IGRD) = .TRUE.
                         IF(ICLASS.EQ.3) 
     &                        LCCDA(IZ1-IZ1MIN+1,IPRT,IGRD) = .TRUE.
                         IF(ICLASS.EQ.4)
     &                        LPRBA(IZ1-IZ1MIN+1,IPRT) = .TRUE.
                         IF(ICLASS.EQ.5) 
     &                        LPRCA(IZ1-IZ1MIN+1,IPRT) = .TRUE.
C
                         DO ID=1,IDMAX
                            DO IT = 1 , ITMAX
                               IF    (ICLASS .EQ. 1)THEN
                                  ACDA(IT,ID,IZ1-IZ1MIN+1,IPRT,IGRD) =
     &                                 10.0 ** DRCOFI(IT,ID)
                               ELSEIF(ICLASS .EQ. 2)THEN
                                  SCDA(IT,ID,IZ1-IZ1MIN+1,IPRT,IGRD) =
     &                                 10.0 ** DRCOFI(IT,ID)
                               ELSEIF(ICLASS .EQ. 3)THEN
                                  CCDA(IT,ID,IZ1-IZ1MIN+1,IPRT,IGRD) =
     &                                 10.0 ** DRCOFI(IT,ID)
                               ELSEIF(ICLASS .EQ. 4)THEN
                                  PRBA(IT,ID,IZ1-IZ1MIN+1,IPRT)=
     &                                 PRBA(IT,ID,IZ1-IZ1MIN+1,IPRT)+
     &                                 10.0**DRCOFI(IT,ID)
                               ELSEIF(ICLASS .EQ. 5)THEN
                                  PRCA(IT,ID,IZ1-IZ1MIN+1,IPRT)=
     &                                 PRCA(IT,ID,IZ1-IZ1MIN+1,IPRT)+
     &                                 10.0**DRCOFI(IT,ID)
                               ENDIF
                            ENDDO
                         ENDDO
                      ENDIF
                   ENDDO
                ENDDO
             ENDDO
          ENDIF
       ENDDO
C
C-----------------------------------------------------------------------
C      FETCH METASTABLE CROSS COUPLING
C-----------------------------------------------------------------------
C
       ICLASS = 6
       IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
          DSNINC = DSFLLA(ICLASS)
          DO IZ1=IZ1MIN,IZ1MAX
             DO IGRD = 1, NPART(IZ1-IZ1MIN+1)
                DO JGRD = 1, NPART(IZ1-IZ1MIN+1)
                   IFAIL = 0
                   CALL D9RDNM(DSNINC,LPART ,IFAIL ,
     &                         IZ0   ,NPART ,IGRD  ,JGRD ,ICLASS ,
     &                         IZ1   ,ITMAX ,IDMAX ,
     &                         ISDIMD,IZDIMD,ITDIMD,
     &                         ISMAXD,IZMAXD,ITMAXD,IDMAXD,NPARTR,
     &                         DTEV  ,DDENS ,
     &                         DTEVD ,DDENSD,DRCOFD,ZDATA  ,
     &                         DRCOFI
     &                        )
                   IF( IFAIL.EQ.0 ) THEN
                      LQCDA(IZ1-IZ1MIN+1,IGRD,JGRD) = .TRUE.
                      DO ID = 1 , IDMAX
                         DO IT = 1 , ITMAX
                            QCDA(IT,ID,IZ1-IZ1MIN+1,IGRD,JGRD) = 
     &                           10.0 ** DRCOFI(IT,ID)
                         ENDDO
                      ENDDO
                   ENDIF
                ENDDO
             ENDDO
          ENDDO
       ENDIF
C     
C-----------------------------------------------------------------------
C     FETCH PARENT CROSS COUPLING DATA
C-----------------------------------------------------------------------
C
       ICLASS = 7
       IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
          DSNINC = DSFLLA(ICLASS)
          DO IZ1=IZ1MIN,IZ1MAX
             DO IPRT = 1, NPART(IZ1-IZ1MIN+2)
                DO JPRT = 1, IPRT-1
                   IFAIL = 0
                   CALL D9RDNM(DSNINC,LPART ,IFAIL ,
     &                         IZ0   ,NPART ,IPRT  ,JPRT ,ICLASS ,
     &                         IZ1   ,ITMAX ,IDMAX ,
     &                         ISDIMD,IZDIMD,ITDIMD,
     &                         ISMAXD,IZMAXD,ITMAXD,IDMAXD,NPARTR,
     &                         DTEV  ,DDENS ,
     &                         DTEVD ,DDENSD,DRCOFD,ZDATA ,
     &                         DRCOFI
     &                        )
                   IF( IFAIL.EQ.0 ) THEN
                      LXCDA(IZ1-IZ1MIN+1,IPRT,JPRT) = .TRUE.
                      DO ID = 1 , IDMAX
                         DO IT = 1 , ITMAX
                            XCDA(IT,ID,IZ1-IZ1MIN+1,IPRT,JPRT) = 
     &                           10.0 ** DRCOFI(IT,ID)
                         ENDDO
                      ENDDO
                   ENDIF
                ENDDO
             ENDDO
          ENDDO
       ENDIF
C
C-----------------------------------------------------------------------
C      FETCH LINE POWER
C-----------------------------------------------------------------------
C
       ICLASS = 8
       IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
          DSNINC = DSFLLA(ICLASS)
          DO IZ1=IZ1MIN,IZ1MAX
             DO IGRD = 1, NPART(IZ1-IZ1MIN+1)
                IPRT  = 0
                IFAIL = 0
                CALL D9RDNM(DSNINC,LPART ,IFAIL ,
     &                      IZ0   ,NPART ,IGRD  ,IPRT  ,ICLASS ,
     &                      IZ1   ,ITMAX ,IDMAX ,
     &                      ISDIMD,IZDIMD,ITDIMD,
     &                      ISMAXD,IZMAXD,ITMAXD,IDMAXD,NPARTR,
     &                      DTEV  ,DDENS ,
     &                      DTEVD ,DDENSD,DRCOFD,ZDATA ,
     &                      DRCOFI
     &                  )
                IF( IFAIL.EQ.0 ) THEN
                   LPLTA(IZ1-IZ1MIN+1,IGRD) = .TRUE.
                   DO ID = 1 , IDMAX
                      DO IT = 1 , ITMAX
                         PLTA(IT,ID,IZ1-IZ1MIN+1,IGRD) = 
     &                        10.0**DRCOFI(IT,ID)
                      ENDDO
                   ENDDO
                ENDIF
             ENDDO
          ENDDO
       ENDIF
C
C-----------------------------------------------------------------------
C
       RETURN
C
C-----------------------------------------------------------------------
C
 1000  FORMAT('FILE = ',1A30)
C-----------------------------------------------------------------------
C
       END
