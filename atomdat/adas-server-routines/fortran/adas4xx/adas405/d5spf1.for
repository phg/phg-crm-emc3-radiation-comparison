CX - UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5spf1.for,v 1.3 2004/07/06 13:17:47 whitefor Exp $ Date $Date: 2004/07/06 13:17:47 $    
CX
      SUBROUTINE D5SPF1( LPLT1, LPASS, LPEND, DSN44, L2FILE, SAVFIL,
     &                   IGRAPH, CADAS )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D5SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS405
C
C  SUBROUTINE:
C
C  OUTPUT:   (L*4)   LPLT1    = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (I*4)   IGRAPH   = INDEX OF GRAPH TO BE PLOTTED:
C                               1=ION FRACTION
C                               2=POWER FUNCTION
C                               3=CONTRIBUTION FUNCTION
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  Tim Hammond  (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    06-09-95
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.3                          DATE: 13-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED CALL TO XXADAS TO RETRIEVE HEADER FOR
C                 TEXT OUTPUT
C
C-----------------------------------------------------------------------
      CHARACTER    SAVFIL*80   , DSN44*80    , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LPLT1       , L2FILE      ,
     &             LPASS      
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      INTEGER      IGRAPH      
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C
C
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF

C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC .EQ. ONE) THEN 
               LPLT1 = .TRUE.
               READ(PIPEIN,*) IGRAPH
          ELSE
               LPLT1 = .FALSE.
          ENDIF
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC .EQ. ONE) THEN 
              L2FILE = .TRUE.
              READ(PIPEIN, '(A)') SAVFIL
          ELSE
              L2FILE = .FALSE.
          ENDIF
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC .EQ. ONE) THEN 
              LPASS = .TRUE.
              READ(PIPEIN, '(A)') DSN44
          ELSE
              LPASS = .FALSE.
          ENDIF
          IF (L2FILE) CALL XXADAS(CADAS)
      ENDIF
c
C-----------------------------------------------------------------------
C
      RETURN
      END
