CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5diag.for,v 1.1 2004/07/06 13:15:12 whitefor Exp $ Date $Date: 2004/07/06 13:15:12 $
CX
       SUBROUTINE D5DIAG( NDSTAT , NDMET ,
     &                    NSTATE , NMET  ,
     &                    CFREC  , CFION , CFMET
     &                  )
       IMPLICIT NONE
C
C-------------------------------------------------------------------------------
C  **************** FORTRAN77 SUBROUTINE : D5DIAG ****************      ********
C
C PURPOSE: CALCULATION OF PRIME DIAGONAL OF METASTABLE RATE COEFFICIENT
C   MATRIX
C
C CALLING PROGRAM: D5MPOP
C
C INPUT: (R*8) CFREC( , , )  = RECOMBINATION RATE COEFFICIENTS TO ALL
C                              METASTABLE NDMET;STARTING FROM FIRST TO
C                              GROUND LEVEL WITH CFREC(1,NDMET,NDMET)
C                              SET TO ZERO
C                              DIMENSIONS = (NSTATE,NDMET,NDMET)
C
C INPUT: (R*8) CFION( , , )  = IONISATION RATE COEFFICIENTS TO ALL
C                              METASTABLE NDMET;STARTING FROM GROUND
C                              TO FIRST LEVEL,WITH
C                              CFION(NSTATE,NDMET,NDMET) SET TO ZERO
C                              DIMENSIONS = (NSTATE,NDMET,NDMET)
C INPUT: (I*4) NDSTAT        = MAXIMUM NUMBER OF NDMET
C INPUT: (I*4) NSTATE        = PARAMETER = NO OF NDMET
C INPUT: (I*4) NDMET         = PARAMETER = MAXIMUM SIZE OF MATRICES
C                              HOLDING METASTABLE TRANSITIONS
C INPUT  (I*4) NMET( )       = NO  OF METASTABLES IN EACH ENERGY LEVEL
C                              DETERMINES ACTUAL SIZE OF MINI MATRICES
C                              DIMENSION = NSTATE
C
C OUTPUT:(R*8) CFMET( , , )  = CROSS COUPLING COEFFICIENTS BETWEEN
C                              METASTABLE NDMET WITH LEADING DIAGONAL
C                              CALCULATED
C                              DIMENSIONS = (NSTATE,NDMET,NDMET)
C
C ROUTINES : NONE
C
C AUTHOR : D. BROOKS,  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C
C DATE : 07/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C-------------------------------------------------------------------------------
       INTEGER NDMET
       INTEGER NSTATE,NDSTAT
       INTEGER I,J,K
C-------------------------------------------------------------------------------
       INTEGER NMET(NDSTAT)
C-------------------------------------------------------------------------------
       REAL*8  SUM
C-------------------------------------------------------------------------------
       REAL*8  CFREC(NDMET,NDMET,NDSTAT),
     &         CFION(NDMET,NDMET,NDSTAT),
     &         CFMET(NDMET,NDMET,NDSTAT)
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
C
C-------------------------------------------------------------------------------
C
C*******************************************************************************
C  FORM THE ELEMENTS ON THE MAIN DIAGONAL OF THE MATRIX
C-------------------------------------------------------------------------------
C
       DO 1 I = 1,NSTATE
         DO 2 J = 1,NDMET
           SUM = 0.0
             DO 3 K = 1,NMET(I)
               SUM = SUM-CFMET(K,J,I)
 3           CONTINUE
C-------------------------------------------------------------------------------
             IF(I  .GT.  1)THEN
               DO 4 K = 1,NMET(I-1)
                  SUM = SUM-CFREC(K,J,I)
 4             CONTINUE
             ENDIF
C-------------------------------------------------------------------------------
             IF (I  .LT.  NSTATE)THEN
               DO 5 K = 1,NMET(I+1)
                  SUM = SUM-CFION(K,J,I)
 5             CONTINUE
             ENDIF
C-------------------------------------------------------------------------------
             CFMET(J,J,I) = SUM
 2         CONTINUE
 1     CONTINUE
C-------------------------------------------------------------------------------
       RETURN
C-------------------------------------------------------------------------------
       END
