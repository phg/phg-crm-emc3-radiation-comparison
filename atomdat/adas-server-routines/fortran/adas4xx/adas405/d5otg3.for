CX - UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5otg3.for,v 1.4 2004/07/06 13:16:24 whitefor Exp $ Date $Date: 2004/07/06 13:16:24 $
CX
      SUBROUTINE D5OTG3( LGHOST , DATE  ,
     &                   IMDIMD , NTDIM , NDLINE , NDCOMP ,
     &                   ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN   , YMAX   ,
     &                   NMSUM  , ITMAX ,
     &                   TEV    , POPTIT,
     &                   IBSEL  ,
     &                   NLINE  , NCOMP ,
     &                   TITL   , SPECL , IPLINE ,
     &                   GCFPEQ , GCFEQ
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D5OTG3 *********************
C
C  PURPOSE:  PIPE COMMS WITH IDL
C
C            PROVIDES DATA FOR  GRAPH OF SELECTED GCF FUNCTION AND 
C            ITS COMPONENTS
C
C            PLOT IS LOG10(GCF FUNCTION ( CM3 S-1) ) VERSUS
C                    LOG10(ELECTRON TEMPERATURE (EV) )
C
C  CALLING PROGRAM: ADAS405
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST   = .TRUE.  => GHOST80 INITIALISED
C                            .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE     = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM    = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)  NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C
C  INPUT : (C*2)  ELEMT    = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*44) DSNINC   = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR     = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF   = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                          = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                          = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM    = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX    = NUMBER OF INPUT ELECTRON DENSITIES
C
C  INPUT : (R*8)  TEV()    = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*10) POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (I*4)  NLINE    = NUMBER OF LINES IDENTIFIED IN SCRIPT
C  INPUT : (I*4)  NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C  INPUT : (C*12) TITL(,)  = TITLE FOR LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (C*16) SPECL(,) = SPECIFICATION OF POINTERS OF LINE CPTS.
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  IPLINE(,)= METASTABLE POINTER OF LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (R*8)  GCFPEQ(,,)=GCF FUNC. COMPONENT (CM3 S-1)
C                           1ST DIM: TEMPERATURE INDEX
C                           2ND DIM: LINE INDEX
C                           3ND DIM: LINE COMPONENT INDEX
C  INPUT : (R*8)  GCFEQ()  = GCF FUNCTION  (CM3 S-1)
C
C          (I*4)  NDIM1    = PARAMETER = MAXIMUM NUMBER OF TEMP. VALUES
C                            (MUST NOT BE LESS THAN 'NTDIM')
C          (I*4)  NDIM2    = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                            (MUST NOT BE LESS THAN 'IMDIMD')
C          (I*4)  NGPIC    = PARAMETER = MAXIMUM NUMBER OF LEVEL POPUL-
C                            ATIONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV    = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                            WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN   = PARAMETER = IN DEFAULT GRAPH SCALING IS
C                            THE  MINIMUM Y-VALUE THAT IS ALLOWED.
C                            (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO   = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                            NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT       = TEMP. INDEX NUMBER FOR ARRAY USE
C          (I*4)  IC       = LINE COMPONENT INDEX
C          (I*4)  IM       = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX    = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
C          (R*4)  X()      = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                             ELECTRON DENSITIES
C          (R*4)  Y(,)     = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                             LEVEL POPULATIONS.
C                             1ST DIMENSION = ELECTRON TEMP. INDEX
C                             2ND DIMENSION = METASTABLE  INDEX
C
C          (L*4)  LPLINE() = .TRUE.  => META. REFERENCED BY A LINE CPT
C                          = .FALSE. => META. NOT REFERENCED BY LINE CPT
C
C          (C*80) ISPEC    = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*80) CADAS    = ADAS HEADER: INCLUDES RELEASE,PROGRAM,TIME
C          (C*13) DNAME    = '       DATE: '
C          (C*13) FNAME    = 'INPUT FILE : '
C          (C*13) GNAME    = 'GRAPH TITLE: '
C          (C*23) XTIT     = X-AXIS UNITS/TITLE
C          (C*23) YTIT     = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C          (C*1)  GRID     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC      = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK   = BLANK 3 BYTE STRING
C          (C*3)  CNAM()   = 3 BYTE STRING FOR POWER FUNCT. TOTAL NAMES
c
C          (C*30) HEAD1    = HEADING FOR METASTABLE ASSIGMENTS
C          (C*30) HEAD2    = HEADING FOR SPECTRUM LINE SPECIFICATIONS
C          (C*30) STRG1    = INDX/DESIGNATION TITLE
C          (C*30) STRG3    = COMPONENT TITLE
C          (C*30) STRG4    = COMPONENT PARAMETER TITLE
C          (C*13) STRG5    = TITLE
C          (C*13) STRG6    = SELECT NO.
C          (C*13) STRG7    = COMPONENTS
C
C          (L*4)  LGTXT    = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
C                          = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C	   XXFLSH      IDL_ADAS  FLUSHES OUT UNIX PIPE
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    28/04/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED MINOR SYNTAX ERRORS
C
C VERSION: 1.3                          DATE: 14-10-95
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED I4UNIT WRITES FOR HP MACHINES
C
C VERSION: 1.4                          DATE: 08-06-98
C MODIFIED: RICHARD MARTIN
C		    - INCREASED NDIM1 to 30.
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV
C-----------------------------------------------------------------------
      REAL*4    CUTMIN   , GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 30  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
      PARAMETER ( CUTMIN = 1.0E-30 , GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   PIPEIN   , PIPEOU
      INTEGER   IMDIMD   , NTDIM    , NDLINE   , NDCOMP
      INTEGER   NMSUM    , ITMAX    , NLINE    , IBSEL    ,
     &          IZ0
      INTEGER   IT       , IM       , I4UNIT   ,
     &          IMMAX    , IC        
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      INTEGER   NCOMP(NDLINE)       , IPLINE(NDLINE,NDCOMP)
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      LOGICAL   LPLINE(NDIM2)
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*44
      CHARACTER YEAR*2   , YEARDF*2
      CHARACTER GRID*1   , PIC*1    , C3BLNK*3 , DATE*8   ,
     &          FILE0*9  , MNMX0*9  , KEY0*9   , ADAS0*8  ,
     &          DNAME*13 , GNAME*10 ,
     &          XTIT*25  , YTIT*24  ,
     &          HEAD1*30 , HEAD2*30 ,
     &          STRG1*30 , STRG3*30 , STRG4*30 ,
     &          STRG5*13 , STRG6*13 , STRG7*13 ,
     &          ISPEC*80 , CADAS*80 
C-----------------------------------------------------------------------
      CHARACTER TITL(NDLINE,NDCOMP)*12 , SPECL(NDLINE,NDCOMP)*16     ,
     &          CNAM(1)*3
C-----------------------------------------------------------------------
      REAL*4    X(NDIM1)            , Y(NDIM1,NDIM2)      , Z(NDIM1,1)
C-----------------------------------------------------------------------
      CHARACTER POPTIT(IMDIMD)*10
      CHARACTER KEY(3)*22
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM)
      REAL*8    GCFPEQ(NTDIM,NDLINE,NDCOMP) ,
     &          GCFEQ(NTDIM,NDLINE)
C-----------------------------------------------------------------------
      SAVE      CADAS
C-----------------------------------------------------------------------
      DATA ISPEC(1:40)
     &           /'CONTRIB. FUNCT. VS ELECTRON TEMPERATURE:'/
      DATA XTIT  /'ELECTRON TEMPERATURE (eV)'/
      DATA YTIT  /'CONTRIB. FUNC. (cm3 s-1)'/
      DATA ADAS0 /'ADAS   :'/                ,
     &     FILE0 /'FILE   : '/               ,
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(FULL LINE - TOTAL   )'/  ,
     &     KEY(2)/' (DASH LINE - PARTIAL)'/  ,
     &     KEY(3)/'                    ) '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/    ,
     &     C3BLNK/'   '/ ,
     &     CADAS /' '/
      DATA DNAME /'       DATE: '/,
     &     GNAME /'SPECIES : '/
      DATA HEAD1 /'---- METASTABLE ASSIGMENTS ---'/,
     &     HEAD2 /'--SPECTRUM LINE SPECIFICATION-'/
      DATA STRG1 /'INDX        DESIGNATION       '/,
     &     STRG3 /'COMPONENT PARAMETERS          '/,
     &     STRG4 /'IC IZ IM  IP  IF  INDX        '/,
     &     STRG5 /'TITLE      = '/,
     &     STRG6 /'SELECT NO. = '/,
     &     STRG7 /'COMPONENTS = '/
C-----------------------------------------------------------------------
      DATA CNAM(1) / 'TOT' /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LGTXT = .TRUE.
      IMMAX = MIN0( NGLEV , NMSUM )
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
      ISPEC(41:80) = TITLE
C
C-----------------------------------------------------------------------
C ZERO LOGIC VECTOR FOR REFERENCED METASTABLES
C-----------------------------------------------------------------------
C
        DO 1 IM = 1, NMSUM
         LPLINE(IM) = .FALSE.
    1   CONTINUE
c
c-----------------------------------------------------------------------
c CONVERT TEMPERATURE VALUES TO REAL*4 ARRAY
c-----------------------------------------------------------------------
c
         DO 2 IT=1,ITMAX
            X(IT)=REAL(TEV(IT))
    2    CONTINUE
c
c-----------------------------------------------------------------------
c CONVERT SELECTED GCFEQ FUNCTION TO REAL*4
c-----------------------------------------------------------------------
c
         DO 3 IT=1,ITMAX
            Z(IT,1)=GCFEQ(IT,IBSEL)
            IF (Z(IT,1).LT.GHZERO) Z(IT,1)=GHZERO
    3    CONTINUE
c
c-----------------------------------------------------------------------
c CONVERT COMPONENTS OF SELECTED GCF FUNCTION TO REAL*4
c-----------------------------------------------------------------------
c
         DO 5 IC=1,NCOMP(IBSEL)
            LPLINE(IPLINE(IBSEL,IC)) = .TRUE.
            DO 4 IT=1,ITMAX
               Y(IT,IC)=GCFPEQ(IT,IBSEL,IC)
               IF (Y(IT,IC).LT.GHZERO) Y(IT,IC)=GHZERO
    4       CONTINUE
    5    CONTINUE
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE (I4UNIT(-1),*) "  "
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      DO 200, IT=1, ITMAX
          WRITE(PIPEOU, *) X(IT)
          CALL XXFLSH(PIPEOU)
200   CONTINUE
      WRITE (I4UNIT(-1),*) "  "
      WRITE(PIPEOU, *) NCOMP(IBSEL)
      CALL XXFLSH(PIPEOU)
      DO 201, IC=1, NCOMP(IBSEL)
          DO 202, IT=1, ITMAX
              WRITE(PIPEOU,*) Y(IT,IC)
              CALL XXFLSH(PIPEOU)
              WRITE (I4UNIT(-1),*) "  "
202       CONTINUE
201   CONTINUE
      WRITE (I4UNIT(-1),*) "  "
      WRITE(PIPEOU,*) NMSUM
      CALL XXFLSH(PIPEOU)
      DO 203, IC=1, NMSUM
          WRITE(PIPEOU,*) POPTIT(IC)
          CALL XXFLSH(PIPEOU)
203   CONTINUE
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      DO 204, IC=1, NCOMP(IBSEL)
          WRITE(PIPEOU,*) SPECL(IBSEL,IC)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*) IPLINE(IBSEL,IC)
          CALL XXFLSH(PIPEOU)
204   CONTINUE
      DO 205, IT=1, ITMAX
          WRITE(PIPEOU,*) Z(IT,1)
          CALL XXFLSH(PIPEOU)
205   CONTINUE

C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1A12,2X,'YEAR: ',1A2,2X,'DEFAULT YEAR: ',1A2)
 1001 FORMAT(I2)
 1002 FORMAT(/1X,31('*'),' D5OTG3 MESSAGE ',31('*')/
     &       1X,'METASTABLE: ',I3,
     &       4X,'NO GRAPH WILL BE OUTPUT BECAUSE:')
 1004 FORMAT(1X,'ALL VALUES ARE BELOW THE CUTOFF OF ',1PE10.3)
 1005 FORMAT(1X,'A SERIOUS ERROR EXISTS IN THE DATA OR D5OTG1')
 1006 FORMAT(1X,31('*'),' END OF MESSAGE ',31('*'))
 1007 FORMAT(I3,9X,A10)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
