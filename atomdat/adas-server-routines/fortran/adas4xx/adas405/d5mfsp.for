CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5mfsp.for,v 1.3 2004/07/06 13:15:31 whitefor Exp $ Date $Date: 2004/07/06 13:15:31 $
CX
        SUBROUTINE D5MFSP( NDSTAT , NDMET  , NDONE  ,
     &                     NSTATE , NMET   , ID     , NMSUM  ,
     &                     CFREC  , CFION  , CFMET  , CPOPN  ,
     &                     POPN   , POPNMO , POPNPO ,
     &                     CPOPND , CPOPNZ ,
     &                     POPF   ,
     &                     XTEMP  , YTEMP  ,
     &                     RHS    , RDUM   , SOLVE  , LSOLVE , LAGAIN
     &                   )
        IMPLICIT NONE
C-------------------------------------------------------------------------------
C******************* FORTRAN77 SUBROUTINE: D5MFSP ********************* ********
C
C PURPOSE: TO PERFORM THE MAIN MATRIX ALGEBRA WHICH CALCULATES THE
C          LEVEL POPULATIONS-INCLUDING METASTABLE STATES
C
C CALLING PROGRAM: D5MPOP
C
C INPUT: (R*8) CFREC( , , )  = RECOMBINATION RATE COEFFICIENTS TO ALL
C         METASTABLE NDMET;STARTING FROM FIRST TO
C         GROUND LEVEL,WITH CFREC(NDMET,NDMET,1)
C         SET TO ZERO
C         DIMENSIONS = (NDMET,NDMET,NDSTAT)
C
C        (R*8) CFION( , , )  = IONISATION RATE COEFFICIENTS TO ALL
C         METASTABLE NDMET;STARTING FROM GROUND TO
C         FIRST LEVEL,WITH
C                CFION(NDMET,NDMET,NSTATE) SET TO ZERO
C         DIMENSIONS = (NDMET,NDMET,NDSTAT)
C
C        (I*4) NSTATE        = PARAMETER = NO OF NDMET
C
C        (I*4) NDMET         = PARAMETER = MAXIMUM SIZE OF MATRICES
C         HOLDING METASTABLE TRANSITIONS
C
C        (R*8) NMET( )       = NO  OF METASTABLES IN EACH ENERGY LEVEL
C         DETERMINES ACTUAL SIZE OF MINI MATRICES
C         DIMENSION = NDSTAT
C
C        (I*4) NDONE         = 1 0;MODIFYING MATRICES IN ORDER TO USE
C         SUBROUTINES
C
C        (I*4) ID            = POSITION OF DOMINANT TERM
C
C        (R*8) CFMET( , , )  = CROSS COUPLING COEFFICIENTS BETWEEN
C         METASTABLE NDMET WITH LEADING DIAGONAL
C         CALCULATED
C         DIMENSIONS = (NDMET,NDMET,NDSTAT)
C
C        (L*4) LSOLVE        =  TRUE  => XXMINV SOLVES SET OF 'N' LINEAR
C          EQUATIONS A X = B WHERE A,X,B ARE
C          MATRICES/VECTORS AND:
C          A = 'A(,)' ON INPUT
C          X = 'B()' ON OUTPUT
C          B = 'B()' ON INPUT
C          FALSE  => ONLY MATRIX INVERSION,
C          A INVERSE REPLACES A
C
C        (R*8) DINT          = + OR - 1 DEPENDING ON THE NUMBER OF ROW
C         INTERCHANGES IN THE MATRIX INVERSION
C
C
C        (R*8) NMETZ    = ACTUAL DIMENSION OF NORMALIZATION MATRIX
C         ONCE FIRST ROW & COLUMN IS ELIMINATED
C       = NMET(ID)+NMET(ID+1)-1
C
C        (I*4) NDSTAT    = PARAMETER = MAXIMUM NUMBER OF NDMET
C
C        (I*4) NPOSX    = NMET(ID)
C
C        (I*4) NPOSY    = NMET(ID+1)
C
C
C OUTPUT:(R*8) CPOPN( , , )  = ARRAY HOLDING COEFFICIENTS OF POPULATION
C         STATE EQUATIONS
C         DIMENSIONS = (NDMET,NDMET,NDSTAT+1)
C
C        (R*8) POPN( , , )   = ARRAY HOLDING POPULATION STATE VALUES WITH
C         THIRD DIMENSION SET TO 1
C         DIMENSIONS = (NDMET,NDONE,NDSTAT+1)
C
C        (R*8) CPOPND( , , ) = TEMPORARY NAME OF MATRIX TO BE SUBSTITUTED
C         INTO NEXT EQUATION IN UPWARD LOOP
C         DIMENSIONS = (NDMET,NDMET,NDSTAT+1)
C
C        (R*8) CPOPNZ( , , ) = TEMPORARY NAME OF MATRIX TO BE SUBSTITUTED
C         INTO NEXT EQUATION  IN DOWNWARD LOOP
C         DIMENSIONS = (NDMET,NDMET,NDSTAT+1)
C
C        (R*8) POPNPO( , , ) = TEMPORARY NAME OF MATRIX HOLDING POPULATION
C         STATE VALUES AFTER NORMALIZATION,TO BE
C         SUBSTITUTED INTO NEXT EQUATION IN UPWARD
C         LOOP
C         DIMENSIONS = (NDMET,NDONE,NDSTAT+1)
C
C        (R*8) POPNMO( , , ) = TEMPORARY NAME OF MATRIX HOLDING POPULATION
C         STATE VALUES AFTER NORMALIZATION,TO BE
C         SUBSTITUTED INTO NEXT EQUATION IN DOWNWARD
C         LOOP
C         DIMENSIONS = (NDMET,NDONE,NDSTAT+1)
C
C        (R*8) SUM           = SUM OF ALL LEVEL POPULATION VALUES
C         INCLUDING METASTABLES
C
C        (R*8) XTEMP( , )    = TEMPORARY MATRIX FOR DURING SUBROUTINE
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDMET)
C
C        (R*8) YTEMP( , )    = TEMPORARY MATRIX FOR DURING SUBROUTINE
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDMET)
C
C        (R*8) PTEMP( , , )  = TEMPORARY MATRIX FOR DURING ERROR CHECK
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDONE,NDSTAT)
C
C        (R*8) QTEMP( , , )  = TEMPORARY MATRIX FOR DURING ERROR CHECK
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDONE,NDSTAT)
C
C        (R*8) RTEMP( , , )  = TEMPORARY MATRIX FOR DURING ERROR CHECK
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDONE,NDSTAT)
C
C        (R*8) STEMP( , , )  = TEMPORARY MATRIX FOR DURING ERROR CHECK
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDONE,NDSTAT)
C
C        (R*8) TEMP( , , )   = MATRIX HOLDING RESULTS OF ERROR CHECK
C         ALL OF WHICH SHOULD BE ZERO
C         DIMENSIONS = (NDMET,NDONE,NDSTAT)
C
C        (R*8) SOLVE( , )    = NORMALIZATION MATRIX AT CRITICAL STAGE
C         DIMENSIONS = (2*NDMET-1,2*NDMET-1)
C
C        (R*8) CTEMP( , , )  = HOLDS VALUES OF CFMET FOR ERROR CHECK,IS
C         NECCESSARY SINCE CFMET IS ALTERED DURING
C         CALCULATIONS
C         DIMENSIONS = (NDMET,NDMET,NDSTAT)
C
C        (R*8) RHS( )    = SIPHONED OFF COLUMN OF NORMALIZATION
C         MATRIX,USED TO CALCULATE METASTABLE
C         NDMET OF DOMINANT STAGE THROUGH XXMINV
C         DIMENSIONS = (2*NDMET-1)
C
C        (R*8) RDUM( )    = DUMMY ARRAY USED IN XXMINV AS RHS WHEN
C         LSOLVE =  FALSE
C
C
C ROUTINES :
C           ROUTINE   SOURCE    BRIEF DESCRIPTION
C           ___________________________________________________________
C           DXMADD    ADAS      MATRIX ADDITION/SUBTRACTION
C           DXMMUL    ADAS      MATRIX MULTIPLICATION
C           XXMINV    ADAS      MATRIX INVERSION
C
C
C AUTHOR:  D. BROOKS, H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    02/06/94
C
C UPDATE:  14/02/95  HPS - INTRODUCED IAGAIN TO IMPROVE DOMINANT STAGE
C                          IDENTIFICATION.
C UPDATE:  06/07/95  HPS - MODIFIED LOOP TO PREVENT IAGAIN AND HENCE ID
C                          BEING SET GREATER THAN NSTATE-1.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 01-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - COPIED FOLLOWING UPDATES MADE BY DAVID BROOKS:
C
C UPDATE:  29/11/95  DHB - INTRODUCED A CHECK TO MAKE SURE THAT THE
C                          POPULATION EQUATIONS ARE SOLVED FOR THE BEST
C                          POSSIBLE CHOICE OF DOMINANT STAGE. NB: THIS
C                          IS NOT THE DOMINANT STAGE ITSELF BUT THE
C                          NEAREST STAGE TO IT THAT CAN SUPPORT THE
C                          CALCULATION I.E. THERE IS A CHECK TO MAKE
C                          SURE THE DOMINANT STAGE IDENTIFICATION DOES
C                          NOT PUSH THE SOLUTION LOOP TOO CLOSE TO ANY
C                          REGION OF RAPID POPULATION DROP OFF. THE
C                          PARAMETER ACC HAS BEEN INTRODUCED TO MEASURE
C                          THIS DROP OFF AND CAN BE ADJUSTED IF IT IS TOO
C                          STRINGENT.
C UPDATE:  29/11/95  DHB - ADDED CHECK TO AVOID UNNECCESARY LOOPING IN
C                          UNRESOLVED CASE.
C
C VERSION: 1.3                          DATE: 01-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - SWAPPED ORDER OF DECLARATION OF PARAMETER ACC.
C
C-------------------------------------------------------------------------------
       INTEGER NDSTAT,NDMET,NDONE,ID,IAGAIN
       INTEGER NMETZ,NPOSX,NPOSY
       INTEGER NSTATE,NMSUM
       INTEGER I,J,K,IM
C-------------------------------------------------------------------------------
       INTEGER NMET(NDSTAT)
C-------------------------------------------------------------------------------
       LOGICAL LSOLVE , LAGAIN
C-------------------------------------------------------------------------------
       REAL*8  SUM,DINT,ACC,RATIO
       PARAMETER ( ACC = 1.0D-06 )
C-------------------------------------------------------------------------------
       REAL*8  CFREC(NDMET,NDMET,NDSTAT)
       REAL*8  CFION(NDMET,NDMET,NDSTAT),CFMET(NDMET,NDMET,NDSTAT)
       REAL*8  CPOPN(NDMET,NDMET,NDSTAT+1),POPN(NDMET,NDONE,NDSTAT+1)
       REAL*8  CPOPND(NDMET,NDMET,NDSTAT+1),
     &         CPOPNZ(NDMET,NDMET,NDSTAT+1)
       REAL*8  POPNMO(NDMET,NDONE,NDSTAT+1),
     &         POPNPO(NDMET,NDONE,NDSTAT+1)
       REAL*8  SOLVE(2*NDMET-1,2*NDMET-1)
       REAL*8  XTEMP(NDMET,NDMET),YTEMP(NDMET,NDMET),RDUM(NDMET)
       REAL*8  RHS(2*NDMET-1)
       REAL*8  POPF(NMSUM)
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
C
C-------------------------------------------------------------------------------
C CALCULATION OF COEFFICIENTS OF METASTABLE POPULATION STATE EQUATIONS
C ZERO ALL ARRAYS TO AVOID CARRY FORWARD
C-------------------------------------------------------------------------------
C
       DO 1 J = 1,NDMET
         DO 2 K = 1,NDMET
           DO 3 I = 1,NDSTAT+1
             CPOPND(J,K,I) = 0.0
             CPOPN(J,K,I) = 0.0
             CPOPNZ(J,K,I) = 0.0
 3         CONTINUE
 2       CONTINUE
 1     CONTINUE
       DO 600 J = 1,NDMET
         DO 601 K = 1,NDSTAT+1
           POPNMO(J,NDONE,K) = 0.0
           POPNPO(J,NDONE,K) = 0.0
 601     CONTINUE
 600   CONTINUE
       DO 602 J = 1,NDMET
         DO 603 K = 1,NDMET
           XTEMP(J,K) = 0.0
           YTEMP(J,K) = 0.0
 603     CONTINUE
 602   CONTINUE
C-------------------------------------------------------------------------------
       CALL XXMINV(LSOLVE,NDMET,NMET(1),CFMET(1,1,1),RDUM,DINT)
       CALL DXMMUL(NDMET,CFMET(1,1,1),NMET(1),NMET(1),CFREC(1,1,2),
     & NMET(1),NMET(2),CPOPN(1,1,2))
C-------------------------------------------------------------------------------
       DO 16 J = 1,NMET(1)
         DO 17 K = 1,NMET(2)
           CPOPND(J,K,2) = CPOPN(J,K,2)
 17      CONTINUE
 16    CONTINUE
C-------------------------------------------------------------------------------
       IF(ID .GE. 2)THEN
       DO 20 I = 2,ID
         CALL DXMMUL(NDMET,CFION(1,1,I-1),NMET(I),NMET(I-1),
     &               CPOPND(1,1,I),NMET(I-1),NMET(I),XTEMP)
         CALL DXMADD(NDMET,CFMET(1,1,I),NMET(I),NMET(I),XTEMP,NMET(I),
     &               NMET(I),YTEMP,NMET(I),NMET(I),1,-1)
         CALL XXMINV(LSOLVE,NDMET,NMET(I),YTEMP,RDUM,DINT)
         CALL DXMMUL(NDMET,YTEMP,NMET(I),NMET(I),CFREC(1,1,I+1),
     &               NMET(I),NMET(I+1),CPOPN(1,1,I+1))
C-------------------------------------------------------------------------------
       DO 18 J = 1,NMET(I)
         DO 19 K = 1,NMET(I+1)
           CPOPND(J,K,I+1) = CPOPN(J,K,I+1)
 19      CONTINUE
 18    CONTINUE
 20    CONTINUE
       ENDIF
C-------------------------------------------------------------------------------
       CALL XXMINV(LSOLVE,NDMET,NMET(NSTATE),CFMET(1,1,NSTATE),
     & RDUM,DINT)
       CALL DXMMUL(NDMET,CFMET(1,1,NSTATE),NMET(NSTATE),NMET(NSTATE),
     &             CFION(1,1,NSTATE-1),NMET(NSTATE),NMET(NSTATE-1),
     &             CPOPN(1,1,NSTATE+1))
C-------------------------------------------------------------------------------
       DO 21 J = 1,NMET(NSTATE)
         DO 22 K = 1,NMET(NSTATE-1)
           CPOPNZ(J,K,NSTATE+1) = CPOPN(J,K,NSTATE+1)
 22      CONTINUE
 21    CONTINUE
C-------------------------------------------------------------------------------
       IF (ID+1 .LE.  NSTATE-1)THEN
         DO 23 I = NSTATE-1,ID+1,-1
           CALL DXMMUL(NDMET,CFREC(1,1,I+1),NMET(I),NMET(I+1),
     &                 CPOPNZ(1,1,I+2),NMET(I+1),NMET(I),XTEMP)
           CALL DXMADD(NDMET,CFMET(1,1,I),NMET(I),NMET(I),XTEMP,
     &                 NMET(I),NMET(I),YTEMP,NMET(I),NMET(I),1,-1)
           CALL XXMINV(LSOLVE,NDMET,NMET(I),YTEMP,RDUM,DINT)
           CALL DXMMUL(NDMET,YTEMP,NMET(I),NMET(I),CFION(1,1,I-1),
     &                 NMET(I),NMET(I-1),CPOPN(1,1,I+1))
C-------------------------------------------------------------------------------
           DO 24 J = 1,NMET(I)
             DO 25 K = 1,NMET(I-1)
               CPOPNZ(J,K,I+1) = CPOPN(J,K,I+1)
 25          CONTINUE
 24        CONTINUE
 23      CONTINUE
       ENDIF
C*******************************************************************************
C MAIN BODY OF SUBROUTINE
C*******************************************************************************
C FILL SOLVE MATRIX
C-------------------------------------------------------------------------------
       NMETZ = NMET(ID)+NMET(ID+1)-1
       NPOSX = NMET(ID)
       NPOSY = NMET(ID+1)
C-------------------------------------------------------------------------------
       DO 30 J = 1,NMETZ
         RHS(J) = 0.0
          DO 31 K = 1,NMETZ
            SOLVE(J,K) = 0.0
 31       CONTINUE
         SOLVE(J,J) = 1.0
 30    CONTINUE
C
       DO 32 J = NPOSX,NMETZ
         RHS(J) = -CPOPN(J-NPOSX+1,1,ID+2)
          IF (NPOSX .GT. 1)THEN
            DO 33 K = 1,NPOSX-1
              SOLVE(J,K) = CPOPN(J-NPOSX+1,K+1,ID+2)
 33         CONTINUE
          ENDIF
 32    CONTINUE
C
       IF (NPOSX .GT. 1)THEN
         DO 34 J = 1,NPOSX-1
           DO 35 K = NPOSX,NMETZ
             SOLVE(J,K) = CPOPN(J+1,K-NPOSX+1,ID+1)
 35        CONTINUE
 34      CONTINUE
       ENDIF
C-------------------------------------------------------------------------------
C NORMALIZATION OF POPN(ID+1,J,NDONE)
C-------------------------------------------------------------------------------
       LSOLVE = .TRUE.
       CALL XXMINV(LSOLVE,2*NDMET-1,NMETZ,SOLVE,RHS,DINT)
       LSOLVE = .FALSE.
       POPN(1,NDONE,ID+1) = 1.0
C-------------------------------------------------------------------------------
       IF(NPOSX.GE.2)THEN
         DO 40 J = 2,NPOSX
           POPN(J,NDONE,ID+1) =  RHS(J-1)
 40      CONTINUE
       ENDIF
C-------------------------------------------------------------------------------
C SOLVING BACKWARDS THEN FORWARDS THROUGH VALUES OF POPN
C-------------------------------------------------------------------------------
         DO 42 J = 1,NMET(ID)
         POPNPO(J,NDONE,ID+1) = POPN(J,NDONE,ID+1)
 42    CONTINUE
C-------------------------------------------------------------------------------
       DO 50 I = ID,2,-1
       CALL DXMADD(NDMET,CPOPN(1,1,I),NMET(I-1),NMET(I),CPOPN(1,1,I),
     &             NMET(I-1),NMET(I),CPOPN(1,1,I),NMET(I-1),NMET(I),
     &             0,-1)
       CALL DXMMUL(NDMET,CPOPN(1,1,I),NMET(I-1),NMET(I),
     &             POPNPO(1,1,I+1),NMET(I),NDONE,XTEMP)
         DO 55 J = 1,NMET(I-1)
           POPNPO(J,NDONE,I) = XTEMP(J,NDONE)
 55      CONTINUE
 50    CONTINUE
C-------------------------------------------------------------------------------
       DO 57 J = 1,NMET(ID)
         POPNMO(J,NDONE,ID+1) = POPN(J,NDONE,ID+1)
 57    CONTINUE
C-------------------------------------------------------------------------------
       IF (ID+1 .LE. NSTATE)THEN
         DO 60 I = ID+1,NSTATE
           CALL DXMADD(NDMET,CPOPN(1,1,I+1),NMET(I),NMET(I-1),
     &                 CPOPN(1,1,I+1),NMET(I),NMET(I-1),CPOPN(1,1,I+1),
     &                 NMET(I),NMET(I-1),0,-1)
           CALL DXMMUL(NDMET,CPOPN(1,1,I+1),NMET(I),NMET(I-1),
     &                 POPNMO(1,1,I),NMET(I-1),NDONE,YTEMP)
           DO 65 J = 1,NMET(I)
             POPNMO(J,NDONE,I+1) = YTEMP(J,NDONE)
 65        CONTINUE
 60      CONTINUE
       ENDIF
C-------------------------------------------------------------------------------
C LOOP TO SUM ALL VALUES OF POPN
C-------------------------------------------------------------------------------
       DO 66 I= 1,ID+1
         DO 67 J = 1,NDMET
           POPN(J,NDONE,I) = POPNPO(J,NDONE,I)
 67      CONTINUE
 66    CONTINUE
C
       DO 68 I = ID+2,NSTATE+1
         DO 69 J = 1,NDMET
           POPN(J,NDONE,I) = POPNMO(J,NDONE,I)
 69      CONTINUE
 68    CONTINUE
C
       IAGAIN = ID
C
C MARK DOMINANT STAGE-1 AS IAGAIN TO COMPARE WITH ID
C ---------- MODIFY THIS LOOP FROM I=1,NSTATE+1 TO I=1,NSTATE  06/07/95
C
       DO 81 I = 1,NSTATE
         IF(POPN(1,NDONE,I).GT.POPN(1,NDONE,IAGAIN+1))THEN
             IAGAIN = I-1
         ENDIF
 81    CONTINUE
C
C FLAG TO RECALCULATE POPULATIONS IF INITIAL ID WAS NOT THE
C BEST POSSIBLE CHOICE OF DOMINANT STAGE.
C
       IF(.NOT.LAGAIN)THEN
         IF(IAGAIN.NE.ID)THEN
82        RATIO = POPN(1,NDONE,IAGAIN+2)/POPN(1,NDONE,IAGAIN+1)
             IF(RATIO.LT.ACC)THEN
                IAGAIN = IAGAIN-1
                GOTO 82
             ENDIF
             IF(IAGAIN.NE.ID)THEN
               LAGAIN = .TRUE.
               ID = IAGAIN
             ENDIF
         ELSE
           LAGAIN = .FALSE.
         ENDIF
       ELSE
         LAGAIN = .FALSE.
       ENDIF
C
       IF(.NOT.LAGAIN)THEN
C
       SUM = 0.0
         DO 70 I = 2,NSTATE+1
           DO 80 J = 1,NDMET
             SUM = SUM+POPN(J,NDONE,I)
 80        CONTINUE
 70      CONTINUE
C-------------------------------------------------------------------------------
C OUTPUT OF RESULTS
C-------------------------------------------------------------------------------
       IM = 1
       DO 85 I = 2,NSTATE+1
         DO 90 J =1,NMET(I-1)
           POPF(IM) = POPN(J,NDONE,I)/SUM
           IM = IM+1
 90      CONTINUE
 85    CONTINUE
       POPN(NDMET,NDONE,NSTATE+1) = 0.0
C
       ENDIF
C
       RETURN
C-------------------------------------------------------------------------------
       END
