CX - UNIX PORT SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5otg1.for,v 1.4 2004/07/06 13:15:47 whitefor Exp $ Date $Date: 2004/07/06 13:15:47 $
CX
      SUBROUTINE D5OTG1( LGHOST , DATE  ,
     &                   IMDIMD , NTDIM ,
     &                   ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN  , YMAX   ,
     &                   NMSUM  , ITMAX  ,
     &                   TEV    ,
     &                   POPTIT , FPABUN
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D5OTG1 *********************
C
C  PURPOSE:  COMMUNICATES GRAPHICS DATA TO IDL                
C
C            PROVIDES  GRAPH OF METASTABLE FRACTIONAL ABUNDANCES. A
C            SINGLE GRAPH WILL CONTAIN UP TO SEVEN METASTABLES. (IF MORE
C            THAN SEVEN METASTABLES ARE PRESENT EXTRA GRAPHS WILL  BE
C            OUTPUT AS REQUIRED).
C
C            PLOT IS LOG10(N(META.)/N(TOTAL FOR ELEMENT)) VERSUS
C                    LOG10(ELECTRON TEMPERATURE (EV) )
C
C  CALLING PROGRAM: ADAS405
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD  = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C
C  INPUT : (C*2)  ELEMT   = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*44) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR    = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF  = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN    = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX    = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN    = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX    = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM   = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX   = NUMBER OF INPUT ELECTRON DENSITIES
C
C  INPUT : (R*8)  TEV()   = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*10)POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (R*8) FPABUN(,)= METASTABLE DEPENDENCE
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: METASTABLE INDEX
C
C          (I*4)  NDIM1   = PARAMETER = MAXIMUM NUMBER OF TEMP.   VALUES
C                           (MUST NOT BE LESS THAN 'NTDIM')
C          (I*4)  NDIM2   = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                           (MUST NOT BE LESS THAN 'IMDIMD')
C          (I*4)  NGPIC   = PARAMETER = MAXIMUM NUMBER OF LEVEL POPULAT-
C                           IONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV   = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                           WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN  = PARAMETER = IN DEFAULT GRAPH SCALING IS THE
C                           MINIMUM Y-VALUE THAT IS ALLOWED.
C                           (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT      = TEMP. INDEX NUMBER FOR ARRAY USE
C          (I*4)  IM      = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX   = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
C          (R*4)  X()     = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            ELECTRON DENSITIES
C          (R*4)  Y(,)    = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            LEVEL POPULATIONS.
C                            1ST DIMENSION = ELECTRON TEMP.   INDEX
C                            2nd DIMENSION = ORDINARY  LEVEL  INDEX
C
C          (C*80) ISPEC   = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C
C          (L*4)  LGTXT   = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
C                         = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    28/04/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES AND CORRECTED ERROR
C                 IN DEFN OF XXFLSH
C
C VERSION: 1.3                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES AND CORRECTED ERROR
C                 IN DEFN OF XXFLSH
C
C VERSION: 1.4                          DATE: 08-06-98
C MODIFIED: RICHARD MARTIN
C		    - INCREASED NDIM1 TO 30:
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV
C-----------------------------------------------------------------------
      REAL*4    CUTMIN   , GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 30  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
      PARAMETER ( CUTMIN = 1.0E-30 , GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   PIPEIN   , PIPEOU    
      INTEGER   IMDIMD   , NTDIM
      INTEGER   NMSUM    , ITMAX    ,
     &          IZ0
      INTEGER   IT       , IM       , 
     &          IMMAX    
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*44
      CHARACTER YEAR*2   , YEARDF*2
      CHARACTER DATE*8   ,
     &          FILE0*9  , MNMX0*9  , KEY0*9   , ADAS0*8  ,
     &          XTIT*25  , YTIT*25  ,
     &          ISPEC*80 
C-----------------------------------------------------------------------
      REAL*4    X(NDIM1)            , Y(NDIM1,NDIM2)
C-----------------------------------------------------------------------
      CHARACTER POPTIT(IMDIMD)*10
      CHARACTER KEY(3)*22
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM)
      REAL*8    FPABUN(NTDIM,IMDIMD)
C-----------------------------------------------------------------------
      DATA ISPEC(1:40)
     &           /'ION FRACTION  VS  ELECTRON TEMPERATURE: '/
      DATA XTIT  /'ELECTRON TEMPERATURE (eV)'/
      DATA YTIT  /'N(INDX)/N(TOTAL)         '/
      DATA ADAS0 /'ADAS   :'/                ,
     &     FILE0 /'FILE   : '/               ,
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(FULL LINE - TOTAL   )'/  ,
     &     KEY(2)/' (DASH LINE - PARTIAL)'/  ,
     &     KEY(3)/'                    ) '/
C
C-----------------------------------------------------------------------
      LGTXT = .TRUE.
      IMMAX = MIN0( NGLEV , NMSUM )
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
      ISPEC(41:80) = TITLE
 
C
C-----------------------------------------------------------------------
C CONVERT TEMPERATURE VALUES TO REAL*4 ARRAY
C-----------------------------------------------------------------------
C
         DO 1 IT=1,ITMAX
            X(IT)=REAL(TEV(IT))
    1    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT META. FRACTIONS TO REAL*4
C-----------------------------------------------------------------------
C
         DO 3 IM=1,NMSUM
            DO 4 IT=1,ITMAX
               Y(IT,IM)=FPABUN(IT,IM)
               IF (Y(IT,IM).LT.GHZERO) Y(IT,IM)=GHZERO
    4       CONTINUE
    3    CONTINUE
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      DO 200, IT=1, ITMAX
          WRITE(PIPEOU, *) X(IT)
          CALL XXFLSH(PIPEOU)
200   CONTINUE
      WRITE(PIPEOU, *) NMSUM
      CALL XXFLSH(PIPEOU)
      DO 201, IT=1, ITMAX
          DO 202, IM=1, NMSUM
              WRITE(PIPEOU, *) Y(IT,IM)
              CALL XXFLSH(PIPEOU)
202       CONTINUE
201   CONTINUE
      DO 203, IM=1, NMSUM
          WRITE(PIPEOU,'(A10)') POPTIT(IM)
          CALL XXFLSH(PIPEOU)
203   CONTINUE
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C
      RETURN
      END
