CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5ispf.for,v 1.2 2004/07/06 13:15:16 whitefor Exp $ Date $Date: 2004/07/06 13:15:16 $
CX
      SUBROUTINE D5ISPF( IPAN   , LPEND  , LSNULL ,
     &                   NDTIN  ,  
     &                   NLINE  , NGFPLN ,
     &                   CIION  , CMPTS  , CTITLE ,
     &                   TITLE  ,
     &                   IBSEL  ,
     &                   AMSR   , AMSD   ,
     &                   ITTYP  , ITVAL  ,
     &                   TEIN   , DEIN   , THIN   , DHIN  ,
     &                   LOSEL  , LFSEL  ,
     &                   LPLT1  , LGRD1  , LDEF1  ,
     &                   LGRD3  , LDEF3  ,
     &                   XLG    , XUG    , YLG    , YUG   ,
     &                   XL3    , XU3    , YL3    , YU3
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D5ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL
C
C  CALLING PROGRAM: ADAS405
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C  INPUT : (L*4)   LSNULL   = .TRUE.  => SCRIPT FILE SET TO NULL
C                             .FALSE. => SCRIPT FILE VALID
C
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURE/DENSITY
C                             SETS ALLOWED.
C
C  INPUT : (I*4)   NLINE    = NUMBER OF EMITTING LINES
C  INPUT : (I*4)   NGFPLN   = NUMBER OF GRAPHS IN IONIS/POWER SET (=2).
C
C  INPUT : (C*2)   CIION()  = SCRIPT FILE: EMITTING ION FOR LINE
C                             DIMENSION: LINE INDEX
C  INPUT : (C*2)   CMPTS()  = SCRIPT FIL : NUMBE OF COMPONENTS FOR LINE
C                             DIMENSION: LINE INDEX
C  INPUT : (C*12)  CTITLE() = SCRIPT FILE: INFORMATION ON LINE
C                             DIMENSION: LINE INDEX
C
C  OUTPUT: (C*40)   TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)    IBSEL   = SELECTED LINE INDEX FOR ANALYSIS
C  OUTPUT: (R*8)    AMSR    = RECEIVER ATOMIC MASS NUMBER
C  OUTPUT: (R*8)    AMSD    = RECEIVER ATOMIC MASS NUMBER
C
C  OUTPUT: (I*4)    ITTYP   = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)    ITVAL   = NUMBER OF INPUT TEMP/DENSITY SETS (1->20)
C  OUTPUT: (R*8)    TEIN()  = USER ENTERED ISPF VALUES -
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C  OUTPUT: (R*8)    DEIN()  = USER ENTERED ISPF VALUES -
C                             ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C  OUTPUT: (R*8)    THIN()  = USER ENTERED ISPF VALUES -
C                             HYDROGEN TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C  OUTPUT: (R*8)    DHIN()  = USER ENTERED ISPF VALUES -
C                             HYDROGEN DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C
C  OUTPUT: (L*4)    LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)    LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)    LPLT1   = .TRUE.  => DISPLAY GRAPH
C                           = .FALSE. => DO NOT DISPLAY GRAPH
C  OUTPUT: (L*4)    LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)    LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C  OUTPUT: (L*4)    LGRD3   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)    LDEF3   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)    XLG()    = LOWER LIMIT FOR X-AXIS OF GRAPHS.
C                              DIMENSION: NGFPLN
C  OUTPUT: (R*8)    XUG()    = UPPER LIMIT FOR X-AXIS OF GRAPHS.
C                              DIMENSION: NGFPLN
C  OUTPUT: (R*8)    YLG()    = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
C                              DIMENSION: NGFPLN
C  OUTPUT: (R*8)    YUG()    = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
C  OUTPUT: (R*8)    XL3     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    XU3     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    YL3     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)    YU3     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  NOTE: - ALL GRAPHICS RELATED VARIABLES MENTIONED ABOVE ARE NOW
C          HANDLED BY THE ROUTINE D5SPF1.FOR
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  FLUSH OUT UNIX PIPE
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/10/91
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IPAN          , I             , 
     &           NDTIN  ,
     &           NLINE         , IBSEL         , ITTYP        , ITVAL  ,
     &           NGFPLN
C-----------------------------------------------------------------------
      REAL*8     XL3           , XU3           , YL3          , YU3
      REAL*8     AMSR          , AMSD
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , LSNULL        ,
     &           LOSEL         , LFSEL         ,
     &           LPLT1         , LGRD1         , LDEF1         ,
     &           LGRD3         , LDEF3
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      REAL*8     TEIN(NDTIN)              , DEIN(NDTIN)
      REAL*8     THIN(NDTIN)              , DHIN(NDTIN)
      REAL*8     XLG(NGFPLN), XUG(NGFPLN) , YLG(NGFPLN) , YUG(NGFPLN)
C-----------------------------------------------------------------------
      CHARACTER  CIION(NLINE)*2           , CMPTS(NLINE)*2           ,
     &           CTITLE(NLINE)*12
C-----------------------------------------------------------------------
      INTEGER    LOGIC
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER (PIPEOU=6       , PIPEIN=5)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) NDTIN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NLINE
      CALL XXFLSH(PIPEOU)
      DO 1, I=1, NLINE
          WRITE(PIPEOU, '(A)') CIION(I)
          WRITE(PIPEOU, '(A)') CMPTS(I)
          WRITE(PIPEOU, '(A)') CTITLE(I)
          CALL XXFLSH(PIPEOU)
1     CONTINUE
C
C-----------------------------------------------------------------------
C     READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
          READ(PIPEIN, '(A)') TITLE
          READ(PIPEIN, *) IBSEL
          READ(PIPEIN, *) ITTYP
          READ(PIPEIN, *) ITVAL
          READ(PIPEIN, *) AMSR
          READ(PIPEIN, *) AMSD
          DO 2, I=1, ITVAL
              READ(PIPEIN, *) TEIN(I)
              READ(PIPEIN, *) DEIN(I)
              READ(PIPEIN, *) THIN(I)
              READ(PIPEIN, *) DHIN(I)
2         CONTINUE
      ENDIF
 
C-----------------------------------------------------------------------

      RETURN
      END
