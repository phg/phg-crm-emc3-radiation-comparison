CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5spf0.for,v 1.4 2004/07/06 13:17:23 whitefor Exp $ Date $Date: 2004/07/06 13:17:23 $
CX
      SUBROUTINE D5SPF0( RP1    , DSFLLA   , DSNINC ,
     &                   LSELA    , LEXSA  , LDEFA  , FILTR  ,
     &                   LPART  , LSNULL ,
     &                   YR     , YRD      ,
     &                   IZ0
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D5SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL - GETS NAME OF DATA FILE
C           (INPUT DATA SET SPECIFICATIONS).
C
C  CALLING PROGRAM: ADAS405
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   RP1     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*120) DSFLLA()= MASTER FILE DATA SET NAMES (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C  OUTPUT: (C*120) DSNINC  = SCRIPT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C  OUTPUT: (L*4)   LSELA() = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       INDEX SELECTED
C                          = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                       NOT SELECTED
C  OUTPUT: (L*4)   LEXSA() = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       SELECTED INDEX EXISTS
C                          = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                       FOR THIS SELECTED INDEX
C  OUTPUT: (L*4)   LDEFA() = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       DEFAULT YEAR INDEX EXISTS
C                          = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                       FOR THIS DEFAULT YEAR INDEX
C  OUTPUT: (L*4)   LPART   = .TRUE.  => PARTIAL DATA SELECTED
C                          = .FALSE. => STANDARD DATA SELECTED
C
C  OUTPUT: (L*4)   LSNULL  = .TRUE.  => SCRIPT FILE SET TO NULL
C                            .FALSE. => SCRIPT FILE VALID
C  OUTPUT: (C*2)   YR      = TWO DIGIT SELECTED YEAR NUMBER
C  OUTPUT: (C*2)   YRD     = TWO DIGIT DEFAULT YEAR NUMBER
C  OUTPUT: (I*4)   IZ0     = NUCLEAR CHARGE
C
C
C          (C*7)  FILTR    = POWER FILTER CHARACTERS FOR FILE NAMES
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -----------------------------------------------------------
C          I4EIZ0     AAAS     GET NUCLEAR CHARGE FROM ELEMENT SYMBOL
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    24/03/94
C
C UPDATE   04/08/94- H.P.SUMMERS- FETCH AND PASS NUCLEAR CHARGE
C
C VERSION: 1.1			DATE:    08/11/95
C MODIFIED: TIM HAMMOND        
C               - UNIX PORT. ROUTINE IS NOW PURELY AND SIMPLY
C     		  A COMMUNICATION WITH IDL ROUTINE.
C
C VERSION: 1.2			DATE:    08/11/95
C MODIFIED: TIM HAMMOND        
C               - REMOVED SUPERFLUOUS VARIABLES
C     		  
C VERSION: 1.3			DATE:    08/11/95
C MODIFIED: TIM HAMMOND        
C               - CORRECTED READING IN OF LOGICAL VARIABLES - PREVIOUSLY
C                 READ IN AS INTEGERS FROM IDL, BUT THIS ONLY WORKS
C                 WELL ON DEC MACHINES.
C 
C Version: 1.4				Date: 25/3/97
C Modified: Richard Martin
C		FILTR changed from CHAR*6 to CHAR*7 to accomodate longer
C		filter names in accordance with ADAS408.
C		    		  
C-----------------------------------------------------------------------
      INTEGER     IZ0           , I4EIZ0
      INTEGER     I
      INTEGER     ILOGIC
C-----------------------------------------------------------------------
      CHARACTER   DSNINC*120     , DSFLLA(8)*120 ,
     &            YR*2           , EL*2      
      CHARACTER   YRD*2
      CHARACTER   FILTR*7   
      CHARACTER   RP1*3       
C-----------------------------------------------------------------------
      LOGICAL     LSNULL  ,
     &            LPART      
      LOGICAL     LSELA(8)      , LEXSA(8)       , LDEFA(8)
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU        , ONE      , ZERO
      PARAMETER(  PIPEIN=5      , PIPEOU=6      , ONE=1    , ZERO=0)
C-----------------------------------------------------------------------
C  READ BASIC OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') RP1
      IF (RP1.EQ.'NO') THEN
          READ(PIPEIN,'(A)') YR
          READ(PIPEIN,'(A)') YRD
          READ(PIPEIN,'(A)') EL
          READ(PIPEIN,'(A)') DSNINC
          READ(PIPEIN,'(A)') FILTR
          DO 1, I=1, 8
              READ(PIPEIN,'(A)') DSFLLA(I)
1         CONTINUE
          DO 2, I=1, 8
              READ(PIPEIN,*) ILOGIC
              IF (ILOGIC.EQ.1) THEN
                  LEXSA(I) = .TRUE.
              ELSE
                  LEXSA(I) = .FALSE.
              ENDIF
2         CONTINUE
          DO 3, I=1, 8
              READ(PIPEIN,*) ILOGIC
              IF (ILOGIC.EQ.1) THEN
                  LDEFA(I) = .TRUE.
              ELSE
                  LDEFA(I) = .FALSE.
              ENDIF
3         CONTINUE
          DO 4, I=1, 8
              READ(PIPEIN,*) ILOGIC
              IF (ILOGIC.EQ.1) THEN
                  LSELA(I) = .TRUE.
              ELSE
                  LSELA(I) = .FALSE.
              ENDIF
4         CONTINUE
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC.EQ.1) THEN 
              LPART = .TRUE.
          ELSE
              LPART = .FALSE.
          ENDIF
          IZ0 = I4EIZ0(EL)
          IF(INDEX(DSNINC,'NULL').GT.0) LSNULL=.TRUE.
C
C-----------------------------------------------------------------------
C NOW WRITE OUT VALUE OF LSNULL TO IDL
C-----------------------------------------------------------------------
          IF(LSNULL) THEN
              WRITE(PIPEOU,*) ONE
          ELSE
              WRITE(PIPEOU,*) ZERO
          ENDIF
          CALL XXFLSH(PIPEOU)
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
