CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas405/d5spc2.for,v 1.3 2004/07/06 13:17:03 whitefor Exp $ Date $Date: 2004/07/06 13:17:03 $
CX
      SUBROUTINE D5SPC2( DSNAME, IBSEL  , IZIN   , IZ0IN  ,
     &                   ITVAL  , TVAL   , DVAL   ,
     &                   WLNGTH ,  
     &                   PECA   , LTRNG  , LDRNG  ,
     &                   TITLX  , IRCODE
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: D5PSC2 ********************
C
C  PURPOSE: TO EXTRACT AND INTERPOLATE  PHOTON EMISSIVITIES FOR
C           EMITTING IONS.
C
C	    THIS IS A NEW ROUTINE, WRITTEN BECAUSE OF THE VERY 
C           NAMING CONVENTIONS ON THE IBM AND UNIX MACHINES.
C           IT REPLACES THE OLD SPEC FORTRAN ROUTINE AND A LOT
C           OF THE OBSOLETE FUNCTIONALITY THEREIN. THIS ROUTINE
C           TAKES AS INPUT THE NAMES OF THE PHOTON EMISSIVITY FILES
C           AND CHECKS THEY ARE THERE BEFORE OPENING THEM AND
C           EXTRACTING ALL REQUIRED INFORMATION.
C
C  CALLING PROGRAM: D5SPEC
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)   IZIN    = ION CHARGE OF EMITTING ION
C  INPUT : (I*4)   IZ0IN   = NUCLEAR CHARGE OF EMITTING ION
C
C  INPUT : (I*4)   ITVAL   = NO. OF ELECTRON TEMPERATURE/DENSITY PAIRS
C  INPUT : (R*8)   TVAL()  = ELECTRON TEMPERATURES (UNITS: EV)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)   DVAL()  = ELECTRON DENSITIES (UNITS: CM-3)
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (R*8)   WLNGTH  = SELECTED BLOCK WAVELENGTH (ANGSTROMS)
C
C  OUTPUT: (R*8)   PECA()  = PHOTON EMISSIVITIES.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LTRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (L*4)   LDRNG() =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                           .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (C*120) TITLX   = INFORMATION STRING (DSN ETC.)
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => NORMAL COMPLETION - NO ERROR DETECTED
C                            1 => DATA SET MEMBER FOR EMITTING ION WITH
C                                 CHARGE 'IZIN' & ION CHARGE 'IZ0IN' CAN
C                                 NOT BE FOUND/DOES NOT EXIST.
C                            2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                 AND THOSE IN INPUT FILE.
C                            3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                 OF RANGE OR DOES NOT EXIST.
C                            4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                 ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                            5 => INVALID VALUE FOR 'IZIN' ENTERED.
C                                 ( 0  <= 'IZIN' <= 99 )
C                            9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                 INPUT DATA-SET.
C
C          (I*4)   NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)   NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   NDDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON DENS-
C                                      ITIES  THAT  CAN  BE  READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)   IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)   IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)   IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)   NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                            DATA SET.
C          (I*4)   IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C          (I*4)   IZ      = INPUT FILE - EMITTING ION - CHARGE
C          (I*4)   IZ1     = INPUT FILE - EMITTING ION - CHARGE + 1
C
C          (L*4)   LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                            .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)   ESYM    = INPUT FILE - EMITTING ION - ELEMENT SYMBOL
CA         (C*120)  DSNAME  = NAME OF DATA SET INTERROGATED
C
C          (I*4)   ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                            TURES.
C                            DIMENSION: DATA-BLOCK INDEX
C          (I*4)   IDA()   = INPUT DATA SET-NUMBER OF ELECTRON DENSITIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)   TETA(,) = INPUT DATA SET -
C                            ELECTRON TEMPERATURES (UNITS: eV)
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   TEDA(,) = INPUT DATA SET -
C                            ELECTRON DENSITIES    (UNITS: cm-3)
C                            1st DIMENSION: ELECTRON DENSITY     INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)   PEC(,,)   =INPUT DATA SET -
C                             FULL SET OF IONIZATIONS PER PHOTON
C                             1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                             2nd DIMENSION: ELECTRON DENSITY     INDEX
C                             3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*10)  CWAVEL() = INPUT FILE - WAVELENGTH (ANGSTROMS)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CFILE()  = INPUT FILE - SPECIFIC ION FILE SOURCE
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*8)   CTYPE()  = INPUT FILE - TYPE OF DATA (IE EXCIT., ETC)
C                             DIMENSION: DATA-BLOCK INDEX
C          (C*2)   CINDM()  = INPUT FILE - METASTABLE INDEX
C                             DIMENSION: DATA-BLOCK INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          E3DATA     ADAS      FETCH INPUT DATA FROM SELECTED DATA SET
C          E3CHKB     ADAS      CHECK VALIDITY OF ION AND 'IBSEL'
C          E3SPLN     ADAS      INTERPOLATE DATA WITH TWO WAY SPLINES
C          E3TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C
C AUTHOR:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C DATE:    08/11/95
C
C VERSION: 1.1				DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2				DATE: 10-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - INCREASED LENGTH OF TITLX FROM 80 TO 120
C
C VERSION: 1.3				DATE: 08-06-98
C MODIFIED: RICHARD MARTIN
C		    - INCREASED NTDIM AND NDDIM TO 30.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDIM             , NDDIM
      INTEGER     IZ0MIN         , IZ0MAX
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 350   , NTDIM  = 30       , NDDIM =  30  )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 50       )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          ,
     &            IZ0IN          , IZIN              ,
     &            ITVAL          , IRCODE
      INTEGER     IUNIT          , NBSEL             ,
     &            IZ0            , IZ                , IZ1
C-----------------------------------------------------------------------
      REAL*8      WLNGTH
C-----------------------------------------------------------------------
      LOGICAL     LOPEN,     LEXIST
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , 
     &            DSNAME*120         , TITLX*120
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             ,
     &            ITA(NSTORE)               , IDA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , DVAL(ITVAL)          ,
     &            PECA(ITVAL)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)              , LDRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CINDM(NSTORE)*2           , CFILE(NSTORE)*8      ,
     &            CTYPE(NSTORE)*8           , CWAVEL(NSTORE)*10
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDIM,NSTORE)        , TEDA(NDDIM,NSTORE)
      REAL*8      PEC(NTDIM,NDDIM,NSTORE)
c-----------------------------------------------------------------------
      DATA        IUNIT  /15/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IRCODE = 0
      LOPEN  = .FALSE.
      IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN
C
C-----------------------------------------------------------------------
C IF NEW EMITTING ION ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------
C
          INQUIRE (FILE=DSNAME, EXIST=LEXIST)
          IF(LEXIST) THEN
              IRCODE = 0
              OPEN(UNIT=IUNIT, FILE=DSNAME, STATUS='OLD')
          ELSE
              IRCODE = 1
          ENDIF
          IF (IRCODE.EQ.0) THEN
              LOPEN  = .TRUE.
              CALL E3DATA( IUNIT  , DSNAME ,
     &                     NSTORE , NTDIM  , NDDIM  ,
     &                     IZ0    , IZ     , IZ1    , ESYM  ,
     &                     NBSEL  , ISELA  ,
     &                     CWAVEL , CFILE  , CTYPE  , CINDM ,
     &                     ITA    , IDA    ,
     &                     TETA   , TEDA   ,
     &                     PEC
     &                   )
          ENDIF
C
C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED EMITTING ION CHARGE VALUE.
C-----------------------------------------------------------------------
C
          IF ( IRCODE.EQ.0 ) THEN
              CALL E3CHKB( IUNIT , NBSEL  , IBSEL ,
     &                     IZ0IN , IZIN   ,
     &                     IZ0   , IZ     ,
     &                     LOPEN , IRCODE
     &                   )
          ENDIF
C
C-----------------------------------------------------------------------
C 1) INTERPOLATE WITH TWO WAY SPLINES.
C 2) CREATE TITLE FOR OUTPUT.
C-----------------------------------------------------------------------
C
          IF ( IRCODE.EQ.0 ) THEN
              CALL E3SPLN( NTDIM             , NDDIM         ,
     &                     ITA(IBSEL)        , IDA(IBSEL)    ,
     &                     ITVAL             ,
     &                     TETA(1,IBSEL)     , TEDA(1,IBSEL) ,
     &                     TVAL              , DVAL          ,
     &                     PEC(1,1,IBSEL)    , PECA          ,
     &                     LTRNG             , LDRNG
     &                   )
              CALL E3TITL( IBSEL         , DSNAME       ,
     &                     ESYM          , IZ           ,
     &                     CWAVEL(IBSEL) , CINDM(IBSEL) ,
     &                     TITLX
     &                   )
              READ(CWAVEL(IBSEL),*) WLNGTH
          ENDIF
C
      ELSE
          IRCODE = 4
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
