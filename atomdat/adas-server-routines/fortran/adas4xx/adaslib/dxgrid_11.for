       subroutine dxgrid_11( ndclas , isdimd , iddimd     , itdimd     ,
     &                       idmax_p, itmax_p, nde_pec_c  , nte_pec_c  ,
     &                       ddens_p, dtev_p , ddea_pec_c , dtea_pec_c ,
     &                       dgcra_p, lgcra_p, dgcra_pec_c, lgcra_pec_c,
     &                       iblmxa_p
     &                     )
       implicit none
c
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: dxgrid_11 ******************
c
c purpose : convert generalised collisional_radiative data blocks from
c           adf11 files to a new temperature and density grid.
c
c note    : the source data is contained as sequential datasets
c           with the following naming conventions:
c
c
c           Ten classes of adf11 data file may be converted as follow:
c
c                class index    type      GCR data content
c                -----------    ----      ----------------
c                    1          acd       recombination coeffts
c                    2          scd       ionisation coeffts
c                    3          ccd       CX recombination coeffts
c                    4          prb       recomb/brems power coeffts
c                    5          prc       CX power coeffts
c                    6          qcd       base meta. coupl. coeffts
c                    7          xcd       parent meta. coupl. coeffts
c                    8          plt       low level line power coeffts
c                    9          pls       represent. line power coefft
c                   10          zcd       effective charge
c                   11          ycd       effective squared charge
c                   12          ecd       effective ionisation potential
c
c
c input  : (i*4)  ndclas      = number of adf11 master file classes
c input  : (i*4)  isdimd      = maximum number of (charge, parent, ground)
c                               blocks in isonuclear master files
c input  : (i*4)  iddimd      = maximum number of dens values in
c                               isoelectronic master files
c input  : (i*4)  itdimd      = maximum number of temp values in
c                               isoelectronic master files
c
c input  : (i*4)  idmax_p     = number of dens values in parent
c                               isonuclear master files
c input  : (i*4)  itmax_p     = number of temp values in parent
c                               isonuclear master files
c input  : (i*4)  nde_pec_c   = number of dens values in child
c                               isonuclear master files
c input  : (i*4)  nte_pec_c   = number of temp values in child
c                               isonuclear master files
c
c input  : (r*8)  ddens_p()   = log10(elec. dens.(cm-3)) from parent adf11
c input  : (r*8)  dtev_p()    = log10(elec. temp. (eV) from parent adf11
c input  : (r*8)  ddea_pec_c()= log10(elec. dens.(cm-3)) from parent adf11
c input  : (r*8)  dtea_pec_c()= log10(elec. temp. (eV) from parent adf11
c
c input  : (r*8)  dgcra_p(,,,)= log10(coll.-rad. coefft.) from
c                               isonuclear master file
c                               1st dim: adf11 class index
c                               2nd dim: index of (sstage, parent, base)
c                                        block in isonuclear master file
c                               3nd dim: electron temperature index
c                               4th dim: electron density index
c input  : (l*4)  lgcra_p()   = .true.  => data present for class
c                               .false. => data not present for class
c                               1st dim: adf11 class index
c input  : (r*8)  dgcra_p(,,,)= log10(coll.-rad. coefft.) from
c                               isonuclear master file
c                               1st dim: adf11 class index
c                               2nd dim: index of (sstage, parent, base)
c                                        block in isonuclear master file
c                               3nd dim: electron temperature index
c                               4th dim: electron density index
c input  : (l*4)  lgcra_p()   = .true.  => data present for class
c                               .false. => data not present for class
c                               1st dim: adf11 class index
c input  : (i*4)  iblmxa_p()  = number of (sstage, parent, base)
c                               blocks in isonuclear master file class
c                               1st dim: adf11 class index
c
c
c output : (r*8)  dgcra_c(,,,)= log10(coll.-rad. coefft.) from
c                               isonuclear master file
c                               1st dim: adf11 class index
c                               2nd dim: index of (sstage, parent, base)
c                                        block in isonuclear master file
c                               3nd dim: electron temperature index
c                               4th dim: electron density index
c
c output : (r*8)  drcofd(,,)= dlog10(data rate coefficients (cm-3/s))
c                             in selected master file
c                             1st dim: (charge,meta,grd) block index
c                             2nd dim: temperature index
c                             3rd dim: density index
c output : (r*8)  zdata()   = charge + 1 for ions in selected master
c                             file
c                             1st dim: (charge,meta,grd) block index
c output : (r*8)  drcofi()  = interpolation of drcofd(,,) for
c                             dtev() & ddens()
c
c program: (c*80) dsnold    = file name used in previous call
c          (c*80) cline     = general character variable
c          (c*80) cterm     = terminator line - '-' filled variable
c          (c*4)) cpatrn()  = pattern used to detect data class
c          (i*4)  iz0d      = nuclear charge read from master file
c          (i*4)  iz1min    = minimum charge+1 read from master file
c          (i*4)  iz1max    = maximum charge+1 read from master file
c          (i*4)  iabt      = abort code
c          (i*4)  indsel    = location of (charge,prnt,grnd)
c                             data block in file
c          (i*4)  izdat     = current data block ion charge +1
c          (i*4)  isel      = general index
c          (i*4)  i         = general index
c          (i*4)  it        = general index
c          (i*4)  id        = general index
c          (i*4)  izchk     = index to verify data z1 set complete
c          (i*4)  iprtr()   = parent indices in data set
c          (i*4)  igrdr()   = ground indices in data set
c          (i*4)  lck       = must be greater than 'itmaxd' & 'idmaxd'
c                             & 'itmax' - array size for spline calcs.
c          (r*8)  a()       = general array
c          (r*8)  drcof0(,) = interpolation of drcofd(,,) w.r.t dtev()
c          (l*8)  lexist    = true --- file to open exists else not
c          (i*4)  l1      = parameter = 1
c          (i*4)  iopt    = defines the boundary derivatives for the
c                             spline routine 'xxspln', see 'xxspln'.
c          (l*4)  lsetx   = .true.  => set up spline parameters relating
c                                      to x-axis.
c                           .false. => do not set up spline parameters
c                                      relating to x-axis.
c                                      (i.e. they were set in a previous
c                                            call )
c                           (value set to .false. by 'xxspln')
c          (r*8)  dy()    = spline interpolated derivatives
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          i4fctn     adas      convert string to integer form
c          xxspln     adas      splinig routine
c
c          (r*8 adas function - 'r8fun1' ( x -> x) )
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    04/10/06
c
c
c  Version : 1.1
c  Date    : 04-10-2006
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       integer   nddim   , ntdim
c-----------------------------------------------------------------------
       parameter ( nddim = 100  , ntdim = 100 )
c-----------------------------------------------------------------------
       integer   i4unit
       integer   ndclas   , iddimd  , itdimd  , isdimd
       integer   iopt
       integer   idmax_p  , itmax_p
       integer   nte_pec_c          , nde_pec_c
       integer   i       , it       , id      ,  iclass
c-----------------------------------------------------------------------
       integer   iblmxa_p(ndclas)
c-----------------------------------------------------------------------
       real*8    r8fun1
c-----------------------------------------------------------------------
       real*8    dtev_p(itdimd)              , ddens_p(iddimd)
       real*8    dtea_pec_c(itdimd)          , ddea_pec_c(iddimd)
       real*8    dgcra_p(ndclas,isdimd,itdimd,iddimd)
       real*8    dgcra_pec_c(ndclas,isdimd,itdimd,iddimd)
       real*8    drcof0(ntdim,nddim)
       real*8    ain(ntdim )  , aout(ntdim)  , daout(ntdim)
       real*8    bin(nddim)   , bout(nddim)  , dbout(nddim)
c-----------------------------------------------------------------------
       logical   lsetx
c-----------------------------------------------------------------------
       logical   lgcra_p(ndclas)  , lgcra_pec_c(ndclas)
c-----------------------------------------------------------------------
       external  r8fun1
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c  check internal dimensions are sufficient
c-----------------------------------------------------------------------

       if(nddim.lt.iddimd) then
         write(i4unit(-1),2000)'internal dimension error: ',' nddim =',
     &                         nddim,' < iddimd =',iddimd
         write(i4unit(-1),2003)
         stop
       endif

       if(ntdim.lt.itdimd) then
         write(i4unit(-1),2000)'internal dimension error: ',' ntdim =',
     &                         ntdim,' < itdimd =',itdimd
         write(i4unit(-1),2003)
         stop
       endif

c-----------------------------------------------------------------------
c  interpolate using splines - work over class and each block
c-----------------------------------------------------------------------

       do iclass=1,ndclas
         if(lgcra_pec_c(iclass).and.lgcra_p(iclass))then

             do i=1,iblmxa_p(iclass)

c--------------------------------------------
c  interpolate dgcra_p(,,,) w.r.t temperature
c--------------------------------------------
               lsetx = .true.
               iopt  = 0

               do id = 1 , idmax_p

                 do it = 1 , itmax_p
                    ain(it) = dgcra_p(iclass,i,it,id)
                 enddo

                 call xxspln( lsetx       , iopt      , r8fun1       ,
     &                        itmax_p     , dtev_p    , ain          ,
     &                        nte_pec_c   , dtea_pec_c, aout         ,
     &                        daout
     &                      )
                 do it = 1, nte_pec_c
                   drcof0(it,id)=aout(it)
                 enddo

               enddo
c--------------------------------------------
c  interpolate above w.r.t density
c--------------------------------------------
               lsetx = .true.
               iopt  = 0

               do  it = 1 , nte_pec_c

                 do id = 1 , idmax_p
                    bin(id) = drcof0(it,id)
                 enddo

                 call xxspln( lsetx      , iopt       , r8fun1     ,
     &                        idmax_p    , ddens_p    , bin        ,
     &                        nde_pec_c  , ddea_pec_c , bout       ,
     &                        dbout
     &                      )

                 do id = 1, nde_pec_c
                   dgcra_pec_c(iclass,i,it,id)=bout(id)
                 enddo

               enddo

             enddo

         elseif(lgcra_pec_c(ndclas).and.(.not.lgcra_p(ndclas)))then
             write(i4unit(-1),2000)'child class required not present ',
     &                              'in parent data'
             write(i4unit(-1),2003)
             stop
         endif

       enddo


       return
c
c-----------------------------------------------------------------------
c
 2000 format(1x,30('*'),' dxgrid_11 error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
