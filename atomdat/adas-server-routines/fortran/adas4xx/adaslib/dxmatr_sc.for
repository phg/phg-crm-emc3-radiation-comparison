       subroutine dxmatr_sc( ndim ,
     &                       a    , ja   , nra  , nca
     &                     )
       implicit none
c
c-----------------------------------------------------------------------
c
c  ****************** fortran 77 subroutine: dxmatr_sc *****************
c
c  purpose: scales/rescales a matrix and returns the normalising scaling
c
c  calling program: dgmfsp
c
c  subroutine:
c
c  input :(i*4) ndim          = maximum dimension of matrices
c  input :(r*8) a(,)          = first matrix (which may be scaled)
c  input :(i*4) ja            = power of 10 norm scaling (initially)
c                               of matrix a
c  input :(i*4) nra           = number of rows in matrix a
c  input :(i*4) nca           = number of columns in matrix a
c
c  output:(r*8) a(,)          = scaled/rescaled first matrix
c  output:(i*4) ja            = refreshed power of 10 norm scaling
c                               of matrix a
c
c  routines: none
c
c
c  author:  H. P. Summers, University of Strathclyde
c           JA7.08
c           Extn. 4196
c
c  date:    20/03/2006
c
c
c  Version : 1.1
c  Date    : 03-01-2007
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       integer ndim
       integer i4unit
       integer nra  , nca
       integer j    , k
       integer ja   , jnorm
c-----------------------------------------------------------------------
       real*8  cnorm
c-----------------------------------------------------------------------
       real*8  a(ndim,ndim)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
       cnorm = 0.0d0
       do j = 1,nra
         do k = 1,nca
            cnorm=dmax1(cnorm,dabs(a(j,k)))
         end do
       end do

       jnorm=0
       if (cnorm.ne.0.0D0) then
    4     if (cnorm.lt.1.0d-1) then
            jnorm=jnorm-1
            cnorm=cnorm*10.0d0
            go to 4
          elseif( cnorm.ge.1.0d0) then
            jnorm=jnorm+1
            cnorm=cnorm*0.1d0
            go to 4
          endif
       endif

       ja=ja+jnorm

       do j=1,nra
         do k=1,nca
           a(j,k)=a(j,k)*10.0d0**(-jnorm)
         enddo
       enddo

       return

       end
