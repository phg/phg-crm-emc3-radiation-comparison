CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adaslib/dxmadd.for,v 1.1 2004/07/06 13:33:26 whitefor Exp $ Date $Date: 2004/07/06 13:33:26 $
CX
       SUBROUTINE DXMADD( NDMET,
     &                   A    , NRA  , NCA  , B   , NRB  , NCB  ,
     &                   C    , NRC  , NCC  ,
     &                   NX   , NY
     &                 )
       IMPLICIT NONE
C
C-------------------------------------------------------------------------------
C                                                                       ********
C  ****************** FORTRAN 77 SUBROUTINE: DXMADD ****************************
C
C  PURPOSE: CALCULATES THE SUM OF TWO RECTANGULAR MATRICES WITH
C           ARBITRARY INTEGER MULTIPLIERS
C
C  CALLING PROGRAM: D5MFSP
C
C  SUBROUTINE:
C
C  INPUT :(I*4) NDMET         = MAXIMUM DIMENSION OF MATRICES
C  INPUT :(R*8) A(,)          = FIRST MATRIX
C  INPUT :(I*4) NRA           = NUMBER OF ROWS IN MATRIX A
C  INPUT :(I*4) NCA           = NUMBER OF COLUMNS IN MATRIX A
C  INPUT :(R*8) B(,)          = SECOND MATRIX
C  INPUT :(I*4) NRB           = NUMBER OF ROWS IN MATRIX B
C  INPUT :(I*4) NCB           = NUMBER OF COLUMNS IN MATRIX B
C  INPUT :(I*4) NX            = MULTIPLIER OF FIRST MATRIX
C  INPUT :(I*4) NY            = MULTIPLIER OF SECOND MATRIX
C  INPUT :(I*4) NRC           = NUMBER OF ROWS IN MATRIX C
C  INPUT :(I*4) NCC           = NUMBER OF COLUMNS IN MATRIX C
C
C  OUTPUT:(R*8) C(,)          = RESULTANT MATRIX C
C
C
C  ROUTINES: NONE
C
C
C  AUTHOR:  D. BROOKS, H. P. SUMMERS, JET
C           K1/1/57
C           JET EXT. 4941
C
C  DATE:    02/06/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C-------------------------------------------------------------------------------
       INTEGER NX,NY,NRA,NCA,NRB,NCB,NRC,NCC,NDMET
       INTEGER J,K
C-------------------------------------------------------------------------------
       REAL*8  A(NDMET,NDMET),B(NDMET,NDMET),C(NDMET,NDMET)
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
C
C-------------------------------------------------------------------------------
       IF (NRA .NE. NRB) STOP
     &   'DIMENSION MISMATCH'
       IF (NCA .NE. NCB) STOP
     &   'DIMENSION MISMATCH'
C-------------------------------------------------------------------------------
       DO 1 J = 1,NRA
         DO 2 K = 1,NCA
           C(J,K) = (NX*A(J,K))+(NY*B(J,K))
 2       CONTINUE
 1     CONTINUE
C-------------------------------------------------------------------------------
       RETURN
C-------------------------------------------------------------------------------
       END
