       subroutine dxmadd_sc( ndim ,
     &                       a    , ja   , nra  , nca  ,
     &                       b    , jb   , nrb  , ncb  ,
     &                       c    , jc   , nrc  , ncc  ,
     &                       x    , jx   , y    , jy
     &                     )
       implicit none
c
c-----------------------------------------------------------------------
c
c  ****************** fortran 77 subroutine: dxmadd_sc *****************
c
c  purpose: Calculates the sum of two rectangular matrices with
c           arbitrary real multipliers with normalising scaling.
c           Normalising scaling version of dxmadd.for
c
c  calling program: dgmfsp
c
c  subroutine:
c
c  input :(i*4) ndim          = maximum dimension of matrices
c  input :(r*8) a(,)          = first matrix
c  input :(i*4) ja            = power of 10 norm scaling of matrix a
c  input :(i*4) nra           = number of rows in matrix a
c  input :(i*4) nca           = number of columns in matrix a
c  input :(r*8) b(,)          = second matrix
c  input :(i*4) jb            = power of 10 norm scaling of matrix b
c  input :(i*4) nrb           = number of rows in matrix b
c  input :(i*4) ncb           = number of columns in matrix b
c  input :(r*8) x             = multiplier of first matrix
c  input :(i*4) jx            = power of 10 norm scaling of first multiplier
c  input :(r*8) y             = multiplier of second matrix
c  input :(i*4) jy            = power of 10 norm scaling of second multiplier
c
c  output:(r*8) c(,)          = resultant matrix c
c  output:(i*4) jc            = power of 10 nrrm scaling of matrix c
c  output:(i*4) nrc           = number of rows in matrix c
c  output:(i*4) ncc           = number of columns in matrix c
c
c
c  routines: none
c
c
c  author:  H. P. Summers, University of Strathclyde
c           JA7.08
c           Extn. 4196
c
c  date:    20/03/2006
c
c  Version : 1.1
c  Date    : 20-03-2006
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 04-06-2009
c  Modified: Martin O'Mullane
c            - Deal with possibility of resulting matrix being zero.
c
c-----------------------------------------------------------------------
       integer ndim
       integer i4unit
       integer nra  , nca  , nrb  , ncb  , nrc  , ncc
       integer j    , k
       integer ja   , jb   , jc   , jx   , jy   , jnorm
c-----------------------------------------------------------------------
       real*8  cnorm
       real*8  x    , y
c-----------------------------------------------------------------------
       real*8  a(ndim,ndim), b(ndim,ndim), c(ndim,ndim)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
       if ((nra .ne. nrb).and.(nca.ne.ncb)) then
          write(i4unit(-1),1000)
          write(i4unit(-1),1001)
          stop
      endif

c-----------------------------------------------------------------------

       nrc = nra
       ncc = nca

       cnorm = 0.0d0
       do j = 1,nra
         do k = 1,nca
           c(j,k) = (x*a(j,k))+(y*b(j,k)*10.0d0**(jb+jy-ja-jx))
           cnorm=dmax1(cnorm,dabs(c(j,k)))
         end do
       end do
       jc=ja+jx

       jnorm=0
    4  continue
       if (cnorm.eq.0.0D0) then
         jnorm = 0
       elseif (cnorm.lt.1.0d-1) then
         jnorm=jnorm-1
         cnorm=cnorm*10.0d0
         go to 4
       elseif( cnorm.ge.1.0d0) then
         jnorm=jnorm+1
         cnorm=cnorm*0.1d0
         go to 4
       endif

       jc=jc+jnorm

       do j=1,nra
         do k=1,nca
           c(j,k)=c(j,k)*10.0d0**(-jnorm)
         enddo
       enddo

       return

c-----------------------------------------------------------------------
 1000 format(1x,30('*'),' dxmadd_sc error ',31('*')//
     &       1x,'Mismatch of matrix dimensions: ',a)
 1001 format(/1x,27('*'),' program terminated ',28('*'))
c-----------------------------------------------------------------------
       end
