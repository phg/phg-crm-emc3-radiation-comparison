      SUBROUTINE DXEXCF( CONFG , NOCCUP ,LTYPE)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C                                                                       
C  ****************** FORTRAN 77 SUBROUTINE: DXEXCF ********************
C
C  PURPOSE: RETURNS VECTOR OF OCCUPANCIES FOR STANDARD SHELL INDICES
C           1-15 FROM AN EISSNER HEXADECIMAL CHARACTER CONFIGURATION
C           SPECIFICATION
C
C  CALLING PROGRAM: VARIOUS
C
C  SUBROUTINE:
C
C  INPUT  :  (C*18)  CONFIG   = EISSNER CONFIGURATION
C  OUTPUT :  (I*4)   NOCCUP() = OCCUPANCY FOR EACH DECIMAL ORBITAL
C                               INDEX 1-15.
C  OUTPUT :  (L*4)   LTYPE    = .TRUE.  => CONFIG. EISSNER FORM
C                               .FALSE. => CONFIG. NOT EISSNER FORM
C
C            (I*4)   I      = GENERAL INDEX
C            (I*4)   IFIRST = GENERAL STRING POSITION INDEX
C            (I*4)   ILAST  = GENERAL STRING POSITION INDEX
C            (C*1)   CHRA() = EISSNER HEXADECIMAL ORBITAL LIST
C            (L*4)   LSTAN  = .TRUE.  => CONFIG. STANDARD FORM
C                             .FALSE. => CONFIG. NOT STANDARD FORM
C            (I*4)   NVLCE  = VALENCE SHELL
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4EISS     ADAS      EXPAND EISSNER CONFIG. INTO SHELL OCCUP.
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXDTES     ADAS      DETECT CONFIGURATION FORM
C          XXSLEN     ADAS      FIND NON-BLANK LENGTH OF A STRING
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    04/06/96
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    20TH AUGUST 1996
C
C VERSION: 1.1				DATE: 20-08-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2
C MODIFIED: H. P. SUMMERS		DATE: 24-06-97
C  	    - INSERTED TRAP FOR BARE NUCLEUS CONFIG.
C
C VERSION: 1.3
C MODIFIED: R. MARTIN          		DATE: 30-06-97
C	    - ADDED SCCS KEYWORDS OMITTED IT PREVIOUS VERSION.
C
C VERSION: 1.4				DATE: 22-11-2003
C MODIFIED: Martin O'Mullane
C	    - Pass configurations through ceprep before acting on them.
C
C  Version  : 1.5
C  Date     : 23-08-2010
C  Modified : Martin O'Mullane
C               - extra arguments for xxdtes.
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I          , ISHEL   , NSHEL
      INTEGER    IFIRST  , ILAST      , INDEX   , I4EISS     , NVLCE  ,
     &           lvlce 
C-----------------------------------------------------------------------
      LOGICAL    LTYPE   , LSTAN      , lbndl   , lprnt
C-----------------------------------------------------------------------
      INTEGER    NELA(6) , NOCCUP(61)
C-----------------------------------------------------------------------
      CHARACTER  CONFG*18             , CONFGL*19, ceprep*19
      character  cstr_top*19          , cstr_tail*99
C-----------------------------------------------------------------------
      CHARACTER  CHEISA(6)*1
C-----------------------------------------------------------------------
      external ceprep
C-----------------------------------------------------------------------
        DO 5 I = 1,61
          NOCCUP(I)=0
    5   CONTINUE
C-----------------------------------------------------------------------
C  DETECT BARE NUCLEUS AS BLANK CONFIGURATION OF '01' OR '00'
C-----------------------------------------------------------------------
        CALL XXSLEN(CONFG,IFIRST,ILAST)
        IF(IFIRST.EQ.0.AND.ILAST.EQ.0) THEN
            LTYPE =.TRUE.
            WRITE(I4UNIT(-1),1000)
            WRITE(I4UNIT(-1),1001)
            RETURN
        ELSEIF((ILAST-IFIRST).EQ.1) THEN
            IF((CONFG(IFIRST:ILAST).EQ.'01').OR.
     &         (CONFG(IFIRST:ILAST).EQ.'00')) THEN
                LTYPE=.TRUE.
                WRITE(I4UNIT(-1),1000)
                WRITE(I4UNIT(-1),1001)
                RETURN
            ENDIF
        ENDIF
C-----------------------------------------------------------------------
C  EXPAND CONFIGURATION WITH A LEADING BLANK
C-----------------------------------------------------------------------
        
        confgl = ceprep(CONFG)

        CALL XXDTES(CONFGL    , LTYPE     , LSTAN, 
     &              lbndl     , lprnt     ,
     &              cstr_top  , cstr_tail ,
     &              nvlce     , lvlce     )
        
        IF(.NOT.LTYPE) THEN
            RETURN
        ENDIF

        READ(CONFGL,1008)(NELA(ISHEL),CHEISA(ISHEL),ISHEL=1,6)
        NSHEL=0
   10  CONTINUE
       if (nshel.LT.6) then
          IF(NELA(NSHEL+1).GT.0) THEN
             NSHEL=NSHEL+1
             NELA(NSHEL)=MOD(NELA(NSHEL),50)
             GO TO 10
           ENDIF
       endif

       DO 20 ISHEL=1,NSHEL
         INDEX = I4EISS(CHEISA(ISHEL))
         NOCCUP(INDEX) = NELA(ISHEL)
   20  CONTINUE

      RETURN
C----------------------------------------------------------------------
 1000 FORMAT(1X,31('*'),' DXEXCF WARNING',32('*')//
     &       1X,'BLANK OR NO-ELECTRON CONFIG. DETECTED')
 1001 FORMAT(/1X,23('*'),' BARE NUCLEUS CONFIG. ASSUMED   ',22('*'))
 1008  FORMAT(6(I2,1A1))
C----------------------------------------------------------------------
      END
