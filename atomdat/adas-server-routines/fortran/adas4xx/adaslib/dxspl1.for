CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adaslib/dxspl1.for,v 1.1 2004/07/06 13:33:56 whitefor Exp $ Date $Date: 2004/07/06 13:33:56 $
CX
      SUBROUTINE DXSPL1( ISWIT  , LSWIT  , IZ1   ,
     &                   NDOUT  , NTOUT ,
     &                   NDIN   , NTIN  , NZIN   ,
     &                   IDE    , ITE   , IZE    ,
     &                            TIN   , ZIPT   , EIA    ,
     &                   AIPT   ,
     &                   ZINTRP ,
     &                   ATTY
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DXSPL1 *********************
C
C  PURPOSE: PERFORMS THE FIRST PART OF A THREE WAY SPLINE ON INPUT DATA.
C           GENERATES A TABLE OF LOG10(SCALED COEF/PWRS) COVERING 'ITE'
C           TEMPERATURES AND 'IDE' DENSITIES FOR THE ELEMENT RECOMBINING
C           ION CHARGE GIVEN BY 'IZ1'.
C
C  CALLING PROGRAM: D1SPLN/D4DATA
C
C  DATA:
C
C           THE  SOURCE  DATA IS CONTAINED AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             6. JETUID.PRB<YR>.DATA
C             7. JETUID.PRC<YR>.DATA
C             8. JETUID.PLT<YR>.DATA
C             9. JETUID.PLS<YR>.DATA
C            10. JETUID.MET<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C
C           THE PARTICULAR TYPE OPENED (1-10) IS SELECTED BY 'ISWIT'
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ISWIT   = DATA TYPE SELECTOR (SEE ABOVE) (1 -> 10)
C  INPUT : (L*4)  LSWIT   = .TRUE.  => IONISATION POTENTIALS PRESENT
C                           .FALSE. => IONS. POTENTIALS NOT PRESENT
C  INPUT : (I*4)  IZ1     = OUTPUT - ELEMENT RECOMBINING ION CHARGE
C
C  INPUT : (I*4)  NDOUT   = OUTPUT - MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  NTOUT   = OUTPUT - MAXIMUM NUMBER OF TEMPERATURES
C
C  INPUT : (I*4)  NDIN    = INPUT  - MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  NTIN    = INPUT  - MAXIMUM NUMBER OF TEMPERATURES
C  INPUT : (I*4)  NZIN    = INPUT  - MAXIMUM NUMBER OF CHARGE STATES
C
C  INPUT : (I*4)  ITE     = INPUT  - NUMBER OF REDUCED TEMPERATURES
C  INPUT : (I*4)  IDE     = INPUT  - NUMBER OF REDUCED DENSITIES
C  INPUT : (I*4)  IZE     = INPUT  - NUMBER OF CHARGE STATES/RECOMBINING
C                                    ION CHARGE
C
C  INPUT : (R*8)  TIN()   = INPUT - SET OF 'ITE' REDUCED ELECTRON TEMPS.
C  INPUT : (R*8)  ZIPT()  = INPUT - SET OF 'IZE' RECOMBINING ION CHARGES
C  INPUT : (R*8)  EIA()   = IONISATION POTENTIALS: ()=ION CHARGE
C                           (UNITS: RYDBERGS)
C
C  INPUT : (R*8)  AIPT(,,)= INPUT - COEFFICIENT/POWER ARRAY.
C                           1ST DIMENSION: REDUCED DENSITY ('DENSR()')
C                           2ND DIMENSION: REDUCED TEMPERATURE ('TR()')
C                           3RD DIMENSION: CHARGE STATE ('ZIPT()')
C
C  OUTPUT: (L*4)  ZINTRP(1)= .TRUE.  => 'ATTY(,)' VALUES INTERPOLATED
C                          = .FALSE. => 'ATTY(,)' VALUES EXTRAPOLATED
C
C  OUTPUT: (R*8)  ATTY(,) = WORKING SPACE FOR 3-WAY SPLINE ITERPOLATION
C                           (STORES VALUES OF 'ANS(1)' )
C                           1ST DIMENSION: TEMPERATURE
C                           2ND DIMENSION: DENSITY
C
C          (I*4)  NZDIM1  = PARAMETER = MUST BE EQUAL TO OR GREATER THAN
C                           THE MAXIMUM NUMBER OF INPUT CHARGE STATES
C          (I*4)  L1      = PARAMETER = 1
C
C          (R*8)  TK2ATE  = PARAMETER = EQUATION CONSTANT = 157890
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR CHARGE STATE VALUES
C          (I*4)  JZ      = RECOMBINING ION CHARGE FOR CHARGE STATE 'IZ'
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = 0, 1, 2, 3)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  ATE     = 'TK2ATE' / INPUT REDUCED TEMPERATURE
C          (R*8)  Z2      = 'JZ' SQUARED
C          (R*8)  Z3      = 'JZ' CUBED
C          (R*8)  ZL      = LOG10 OF CURRENT CHARGE STATE
C          (R*8)  Y       = SCALED 'AIPT(,,)' VALUE
C          (R*8)  Z1(1)   = IZ1
C          (R*8)  ANS(1)  = SPLINE INTERPOLATED LOG10(SCALED 'AIPT(,,)')
C                           VALUE FOR A RECOMBINING ION CHARGE EQUAL TO
C                           'IZ1' AT FIXED TEMPERATURE AND DENSITY.
C          (R*8)  F()     = LOG10 ( 'Y' ) - DIMENSION => CHARGE STATE
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C NOTE:
C                         SPLINE IS CARRIED OUT ON:
C            LOG10(SCALED 'AIPT(,,)' VALUES AT FIXED TEMP AND DENSITY)
C                                  VERSUS
C                           RECOMBINING ION CHARGE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FUN2     ADAS      REAL*8 FUNCTION: ( X -> 1/(1+X) )
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE  :  13/06/91 - PE BRIDEN: ADAS91 VERSION OF 'D4SPL1'
C DATE  :  15/12/92 - PE BRIDEN: IF IZE<=1 THEN ANS(1)=F(1) INSTEAD
C                                OF ANS(1)=F(IZ) (WHICH IN ERROR GIVES
C                                ANS(1)=F(2) IF IZE=1.)
C
C UPDATE:  12/08/93 - HP SUMMERS: INCLUDE ISWIT IN PARAMETER AND ALLOW
C                                 SEPARATE TREATMENT ACCORDING TO ISWIT
C UPDATE:  18/08/93 - HP SUMMERS: ALTER ORDER TO TAKE LOG10 FIRST BEFORE
C                                 ADJUSTING FOR DATA CLASS
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NZDIM1               , L1
C-----------------------------------------------------------------------
      REAL*8     TK2ATE
C-----------------------------------------------------------------------
      PARAMETER( NZDIM1 = 24          , L1 = 1         )
C-----------------------------------------------------------------------
      PARAMETER( TK2ATE = 1.5789D+05  )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    ISWIT                , IZ1            ,
     &           NDOUT                , NTOUT          ,
     &           NDIN                 , NTIN           , NZIN        ,
     &           IDE                  , ITE            , IZE
      INTEGER    IOPT                 , JZ             ,
     &           ID                   , IT             , IZ
C-----------------------------------------------------------------------
      REAL*8     R8FUN2               , ATE            ,
     &           Z2                   , Z3             , Y           ,
     &           ZL                   , Z1(1)                , ANS(1)
C-----------------------------------------------------------------------
      LOGICAL    LSWIT                , LSETX          , ZINTRP(1)
C-----------------------------------------------------------------------
      REAL*8     TIN(NTIN)            , ZIPT(NZIN)     ,
     &           EIA(50)
      REAL*8     F(NZDIM1)            , DF(NZDIM1)
      REAL*8     ATTY(NTOUT,NDOUT)
      REAL*8     AIPT(NDIN,NTIN,NZIN)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN2
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IF (NZDIM1.LT.NZIN)
     &   STOP ' DXSPL1 ERROR: NZDIM1 < NZIN - INCREASE NZDIM1'
C-----------------------------------------------------------------------
      Z1(1) = DBLE(IZ1)
      IOPT  = 0
      LSETX = .TRUE.
C-----------------------------------------------------------------------
         DO 1 IT=1,ITE
C
            ATE = TK2ATE / TIN(IT)
C
               DO 2 ID=1,IDE
C
C-----------------------------------------------------------------------
C  SCALING OF CONDENSED MASTER DATA ACCORDING TO DATA CLASS
C---------------------------------------------------------------------
C
                     DO 3 IZ=1,IZE
C
                        IF (AIPT(ID,IT,IZ).LE.0.0) THEN
                            WRITE(I4UNIT(-1),*)
     &                   ' DXSPL1 WARNING: AN INPUT COEFFT. VALUE <= 0',
     &                   ' SET TO 1.0D-74'
                            AIPT(ID,IT,IZ) = 1.0D-74
                        ENDIF
C
                        JZ  = NINT( ZIPT(IZ) )
                        Z2  = DBLE( JZ * JZ  )
                        Z3  = DBLE( JZ * JZ  * JZ )
                        ZL  = DLOG10(ZIPT(IZ))
C
                        Y   = DLOG10(AIPT(ID,IT,IZ))
C
                        IF (ISWIT.EQ.1) THEN
                            Y = Y - ZL
                        ELSEIF (ISWIT.EQ.2) THEN
                            IF (LSWIT) THEN
                               Y=Y + 0.434294D0*EIA(JZ)*ATE/Z2 + 3.0*ZL
                            ELSE
                               Y = Y + 3.0*ZL
                            ENDIF
                        ELSEIF (ISWIT.EQ.3) THEN
                            Y = Y
                        ELSEIF (ISWIT.EQ.4) THEN
                            Y = Y
                        ELSEIF (ISWIT.EQ.5) THEN
                            Y = Y
                        ELSEIF (ISWIT.EQ.6) THEN
                            Y = Y
                        ELSEIF (ISWIT.EQ.7) THEN
                            Y = Y
                        ELSEIF (ISWIT.EQ.8) THEN
                            IF (LSWIT) THEN
                               Y=Y + 0.434294D0*EIA(JZ)*ATE/Z2 + 3.0*ZL
                            ELSE
                               Y = Y + 3.0*ZL
                            ENDIF
                        ELSEIF (ISWIT.EQ.9) THEN
                            IF (LSWIT) THEN
                               Y=Y + 0.434294D0*EIA(JZ)*ATE/Z2 + 3.0*ZL
                            ELSE
                               Y = Y + 3.0*ZL
                            ENDIF
                        ELSEIF (ISWIT.EQ.10) THEN
                            Y = Y
                        ELSE
                            Y = Y
                        ENDIF
C
                        F(IZ) = Y
C
    3                CONTINUE
C
C-----------------------------------------------------------------------
C  INTERPOLATE SCALED VALUE (AT FIXED TEMP. AND DENS.) FOR A RECOMBINING
C  ION EQUAL TO 'Z1(1)'.
C-----------------------------------------------------------------------
C
                     IF (IZE.LE.1) THEN
                        ANS(1) = F(1)
                     ELSE
                        CALL XXSPLE( LSETX , IOPT      , R8FUN2 ,
     &                               IZE   , ZIPT      , F      ,
     &                               L1    , Z1(1)     , ANS(1) ,
     &                               DF    , ZINTRP(1)
     &                             )
                     ENDIF
C
                  ATTY(IT,ID) = ANS(1)
C
    2          CONTINUE
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
