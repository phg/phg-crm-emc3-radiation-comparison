       subroutine dxdata_15( iunit    , idsets   ,
     &                       idstore  , iddimd   , itdimd   ,
     &                       idstack  , idcmt    ,
     &                       idptnl   , idptn    , idptnc   , idcnct   ,
     &                       iz0_p    ,
     &                       dsn_p    , lfile_p  , iss_min  , iss_max  ,
     &                       dsflla_p , lexsa_p    ,
     &                       nptnl_p  , nptn_p   , nptnc_p  ,
     &                       ncnct_p  , icnctv_p ,
     &                       idmaxa_p , itmaxa_p , ddensa_p , dteva_p  ,
     &                       lresa_p  , lptna_p  ,
     &                       nbsela_p ,
     &                       cwavela_p, cfilea_p , ctypea_p , cindma_p ,
     &                       ispbra_pec_p        , isppra_pec_p        ,
     &                       isstgra_pec_p       , iszra_pec_p         ,
     &                       peca_p   , lpeca_p
     &                  )
       implicit none
c
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: dxdata_15 ******************
c
c purpose : to extract photon emissivity coefft. data for a set or
c           superstages of a parent partition level and verify
c           their consistency.
c
c note    : (1) The source data is contained as sequential datasets
c               with naming convention following a template.  The
c               standard template is
c
c           /.../adf15/pec<yr>#<el>_<nn>/pec<yr>#<el>_<nn>#<el>**.dat
c
c               where
c                   <yr> is a two digit year number
c                   <nn> is a two digit partition level, which may be
c                        omitted for a base partition
c                   <el> is a one or two character element symbol
c                    **  is the superstage index
c
c               The superstage indexing starts at 00 . The two
c               characters including the redundant initial zero
c               should be present for superstages and partition
c               levels.
c
c
c
c
c  input : (i*4)  iunit     = unit to which input file is allocated
c  input : (i*4)  idsets    = maximum number of adf15 superstage files
c                             which can be read in.
c  input : (i*4)  idstore   = maximum number of blocks in a
c                             superstage adf15 pec file
c  input : (i*4)  idstack   = maximum number of partition text lines in a
c                             superstage adf15 pec file
c  input : (i*4)  idcmt     = maximum number of comment text lines in a
c                             superstage adf15 pec file
c  input : (i*4)  iddimd    = maximum number of dens values in an
c                             adf15 pec file
c  input : (i*4)  itdimd    = maximum number of temp values in an
c                             adf15 pec file
c  input : (i*4)  idptnl    = maximum level of partitionsidcnct
c  input : (i*4)  idptn     = maximum no. of partitions in one level
c  input : (i*4)  idptnc    = maximum no. of components in a partition
c  input : (i*4)  idcnct    = maximum number of elements in connection
c                             vector
c
c  input : (i*4)  iz0_p     = nuclear charge
c  input : (c*120)dsn_p     = adf15 parent superstage file template
c  input : (l*4)  lfile_p   = .true.  => input files exist for this
c                                        adf15 emissivity template
c                           = .false. => input fils do not exist
c                                        for this template
c  input : (i*4)  iss_min   = lowest parent superstage in child
c                             superstage (n.b. counter starts at 1 not 0,
c                             equivalent to iz1=iz+1 with ionisation stages)
c  input : (i*4)  iss_max   = highest parent superstage in child
c                             superstage(n.b. counter starts at 1 not 0,
c                             equivalent to iz1=iz+1 with ionisation stages)
c
c  input : (i*4)  nptnl_p   = number of partition levels in block
c  input : (i*4)  nptn_p()  = number of partitions in partition level
c  input : (i*4)  nptn_p()  = number of partitions in partition level
c                             1st dim: partition level
c  input : (i*4)  nptnc_p(,)= number of components in partition
c                             1st dim: partition level
c                             2nd dim: member partition in partition level
c  input : (i*4)  ncnct_p   = number of elements in connection vector
c  input : (i*4)  icnctv_p()= connection vector of number of partitions
c                             of each superstage in resolved case
c                             including the bare nucleus
c                             1st dim: connection vector index
c
c  output: (c*120)dsflla_p()= adf15 files accessed from superstage set
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c  output: (l*4)  lexsa_p() = .true.  => input data set type for this
c                                        superstage index exists
c                             .false. => output data set for this
c                                        superstage index not generated
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  output: (i*4)  idmaxa_p()= number of densities in pec superstage
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c  output: (i*4)  itmaxa_p()= number of temperatures in pec superstage
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  output: (r*8)  ddensa_p(,)= densities in pec superstage
c                              1st dim: density index
c                              2nd dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  output: (r*8)  dteva_p(,)= temperatures in pec superstage
c                              1st dim: temperature index
c                              2nd dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  output: (l*4)  lresa_p() = .true.  => partial parent superstage file
c                             .false. => not partial file
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c  output: (l*4)  lptna_p() = .true.  => partition block present
c                             .false. => partition block not present
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  output: (i*4)  nbsela_p()= number of spectrum line emmsivity blocks
c                             in parent superstages of child
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c  output: (i*4)  isppra_pec_p(,) = 1st (parent) index for each line block
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                             2nd dim: index of line block
c                                      adf15 superstage file
c  output: (i*4)  ispbra_pec_p(,)   = 2nd (base) index for each line block
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                             2nd dim: index of line block
c                                      adf15 superstage file
c  output: (i*4)  isstgra_pec_p(,)  = s1 for each line block
c                               (generalises to connection vector index)
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                             2nd dim: index of line block
c                                      adf15 superstage file
c  output: (i*4)  iszra_pec_p(,)= z  for each line block
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                             2nd dim: index of line block
c                                      adf15 superstage file
c  output: (r*8)  peca_p(,,,)=read - photon emissivity values
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                             2nd dim: electron temperature index
c                             3rd dim: electron density index
c                             4th dim: data-block index
c  output: (l*4)  lpeca_p()     = .true.  => data present for class
c                                 .false. => data not present for class
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4eiz0     adas      returns z0 for given element symbol
c          i4fctn     adas      convert character string to integer
c          i4unit     adas      fetch unit number for output of messages
c          xxcase     adas      convert a string to upper or lower case
c          xxhkey     adas      obtain key/response strings from text
c          xxrptn     adas      analyse an adf11 file partition block
c          xxword     adas      extract position of number in buffer
c          xxdata_15  adas      fetch complete adf11 data from file
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:  16/09/05
c
c
c  Version : 1.1
c  Date    : 03-01-2007
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       integer   ntdim       , nddim      , ndsets     , ndstore
       integer   ndptnl      , ndptn      , ndptnc     , ndcnct
       integer   ndstack     , ndcmt
c-----------------------------------------------------------------------
       parameter ( ntdim = 50 , nddim = 40 , ndsets = 75 )
       parameter ( ndstore = 500 )
       parameter ( ndptnl = 4  , ndptn = 128     , ndptnc = 256 )
       parameter ( ndcnct = 100 )
       parameter ( ndstack = 40 , ndcmt = 2000 )
c-----------------------------------------------------------------------
       integer   i4unit
       integer   iunit       , idsets     , idstore
       integer   idstack     , idcmt
       integer   iddimd      , itdimd
       integer   idptnl      , idptn      , idptnc     , idcnct
       integer   iz0_p
       integer   nbsel
       integer   nptnl_p     , ncnct_p
       integer   nptnl       , ncnct      , ncptn_stack   , ncmt_stack
       integer   iss_min     , iss_max
       integer   is          , is1        , ib
       integer   it          , id         , i       , j
       integer   idx
c-----------------------------------------------------------------------
       character dsn_p*120   , dsninc*120 , dsn_p_temp*120
       character esym*2
       character c2*2
c-----------------------------------------------------------------------
       logical   lfile_p     , lexist
       logical   lres        , lptn       , lcmt      , lsup
c-----------------------------------------------------------------------
       integer   isela(ndstore)           ,
     &           ita(ndstore)             , ida(ndstore)
       integer   itmaxa_p(idsets)         , idmaxa_p(idsets)
       integer   nptn_p(idptnl)           , nptnc_p(idptnl,idptn)
       integer   icnctv_p(idcnct)
       integer   nptn(ndptnl)             , nptnc(ndptnl,ndptn)
       integer   iptnla(ndptnl)           , iptna(ndptnl,ndptn)
       integer   iptnca(ndptnl,ndptn,ndptnc)
       integer   icnctv(ndcnct)
       integer   isstgr(ndstore)          , iszr(ndstore)
       integer   ispbr(ndstore)           , isppr(ndstore)

       integer   nbsela_p(idsets)
       integer   ispbra_pec_p(idsets,idstore) ,
     &           isppra_pec_p(idsets,idstore) ,
     &           isstgra_pec_p(idsets,idstore),
     &           iszra_pec_p(idsets,idstore)
c-----------------------------------------------------------------------
       real*8    ddensa_p(iddimd,idsets)  , dteva_p(itdimd,idsets)
       real*8    teta(ntdim,ndstore)      , teda(nddim,ndstore)
       real*8    wavel(ndstore)
       real*8    pec(ntdim,nddim,ndstore) , pec_max(ndstore)
       real*8    peca_p(idsets,itdimd,iddimd,idstore)
c-----------------------------------------------------------------------
       character dsflla_p(idsets)*120
       character cindm(ndstore)*2            , cfile(ndstore)*8      ,
     &           ctype(ndstore)*8            , cwavel(ndstore)*10
       character cptn_stack(ndstack)*80      , cmt_stack(ndcmt)*80
       character cindma_p(idsets,idstore)*2  ,
     &           cfilea_p(idsets,idstore)*8  ,
     &           ctypea_p(idsets,idstore)*8  ,
     &           cwavela_p(idsets,idstore)*10
c-----------------------------------------------------------------------
       logical   lexsa_p(idsets)
       logical   lpeca_p(idsets)
       logical   lresa_p(idsets)             , lptna_p(idsets)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c   check internal dimensions
c-----------------------------------------------------------------------
       if(itdimd.ne.ntdim) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'itdimd = ',itdimd,' .ne. ntdim = ',
     &                          ntdim
         write(i4unit(-1),2003)
         stop
       endif

       if(iddimd.ne.nddim) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'iddimd = ',iddimd,' .ne. nddim = ',
     &                          nddim
         write(i4unit(-1),2003)
         stop
       endif

       if(idsets.ne.ndsets) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idsets = ',idsets,' .ne. ndsets = ',
     &                          ndsets
         write(i4unit(-1),2003)
         stop
       endif

       if(idstore.ne.ndstore) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idstore = ',idstore,' .ne. ndstore = ',
     &                          ndstore
         write(i4unit(-1),2003)
         stop
       endif

       if(idstack.ne.ndstack) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idstack = ',idstack,' .ne. ndstack = ',
     &                          ndstack
         write(i4unit(-1),2003)
         stop
       endif

       if(idcmt.ne.ndcmt) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idcmt = ',idcmt,' .ne. ndcmt = ',
     &                          ndcmt
         write(i4unit(-1),2003)
         stop
       endif

       if(idptnl.ne.ndptnl) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idptnl = ',idptnl,' .ne. ndptnl = ',
     &                          ndptnl
         write(i4unit(-1),2003)
         stop
       endif

       if(idptn.ne.ndptn) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idptn = ',idptn,' .ne. ndptn = ',
     &                          ndptn
         write(i4unit(-1),2003)
         stop
       endif

       if(idptnc.ne.ndptnc) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idptnc = ',idptnc,' .ne. ndptnc = ',
     &                          ndptnc
         write(i4unit(-1),2003)
         stop
       endif

       if(idcnct.ne.ndcnct) then
         write(i4unit(-1),2000)'mismatch of internal dimensions: ',
     &                         'idcnct = ',idcnct,' .ne. ndcnct = ',
     &                          ndcnct
         write(i4unit(-1),2003)
         stop
       endif
c-----------------------------------------------------------------------
c search for availability of parent adf15 files
c-----------------------------------------------------------------------

       if(.not.lfile_p) then
           write(i4unit(-1),2002)'No parent adf15 files',
     &                           ' for script template'

           write(i4unit(-1),2004)
           return
       endif

       do idx=1,iss_max-iss_min+1
         is1=idx+iss_min-1
         is=is1-1
         lexsa_p(idx)=.false.
         dsn_p_temp=dsn_p
         j=index(dsn_p_temp,'**')
         write(c2,'(i2)')is
         if(c2(1:1).eq.' ') c2(1:1)='0'
         dsn_p_temp(j:j+1) = c2(1:2)
          inquire (file=dsn_p_temp, exist=lexist)
          if (.not.lexist) then
              lexsa_p(idx)=.false.
              dsflla_p(idx)=' '
          else
              lexsa_p(idx)=.true.
              dsflla_p(idx)=dsn_p_temp
          endif
        enddo

c-----------------------------------------------------------------------
c Open and read each parent adf15 file in turn
c-----------------------------------------------------------------------

       do idx=1,iss_max-iss_min+1
         is1=idx+iss_min-1
         is=is1-1
         if(lexsa_p(idx)) then
            lpeca_p(idx)=.true.
            dsninc = dsflla_p(idx)

            open(unit=iunit , file = dsninc )

            call  xxdata_15( iunit  , dsninc ,
     &                       ndstore, ntdim  , nddim  ,
     &                       ndptnl , ndptn  , ndptnc , ndcnct ,
     &                       ndstack, ndcmt  ,
     &                       iz0_p   , is    , is1    , esym   ,
     &                       nptnl  , nptn   , nptnc  ,
     &                       iptnla , iptna  , iptnca ,
     &                       ncnct  , icnctv ,
     &                       ncptn_stack     , cptn_stack      ,
     &                       lres   , lptn   , lcmt   , lsup   ,
     &                       nbsel  , isela  ,
     &                       cwavel , cfile  , ctype  , cindm ,
     &                       wavel  , ispbr  , isppr  , isstgr , iszr  ,
     &                       ita    , ida    ,
     &                       teta   , teda   ,
     &                       pec    , pec_max,
     &                       ncmt_stack      , cmt_stack
     &                     )

            close(iunit)

            lresa_p(idx)=lres
            lptna_p(idx)=lptn


c----------------------------------------------
c  check consistent temperatures and densities
c----------------------------------------------
            if(nbsel.gt.1) then
                do i=2,nbsel
                  if(ita(i).ne.ita(1))then
                      write(i4unit(-1),2001)'temperature ranges vary',
     &                               ': is1 = ',is1
                      write(i4unit(-1),2003)
                     stop
                  else
                     do it=1,ita(i)
                       if(teta(it,i).ne.teta(it,1)) then
                           write(i4unit(-1),2000)'temperatures vary',
     &                              ': is1 = ',is1
                           write(i4unit(-1),2003)
                           stop
                       endif
                     enddo
                  endif
                enddo
                do i=2,nbsel
                  if(ida(i).ne.ida(1))then
                      write(i4unit(-1),2001)'density ranges vary',
     &                               ': is1 = ',is1
                      write(i4unit(-1),2003)
                     stop
                  else
                     do id=1,ida(i)
                       if(teda(id,i).ne.teda(id,1)) then
                           write(i4unit(-1),2000)'densities vary',
     &                              ': is1 = ',is1
                           write(i4unit(-1),2003)
                           stop
                       endif
                     enddo
                  endif
                enddo
            endif


c-----------------------------------------------------------------------
c stack output arrays - note indexing shift to start at 1 for economy
c-----------------------------------------------------------------------
            itmaxa_p(idx) = ita(1)
            idmaxa_p(idx) = ida(1)
            do it=1,itmaxa_p(idx)
              dteva_p(it,idx)= teta(it,1)
            enddo
            do id=1,idmaxa_p(idx)
              ddensa_p(id,idx)= teda(id,1)
            enddo


            nbsela_p(idx)=nbsel
            do ib=1,nbsel
              isppra_pec_p(idx,ib)=isppr(ib)
              ispbra_pec_p(idx,ib)=ispbr(ib)
              isstgra_pec_p(idx,ib)=isstgr(ib)
              iszra_pec_p(idx,ib)=iszr(ib)
              cwavela_p(idx,ib)=cwavel(ib)
              cfilea_p(idx,ib)=cfile(ib)
              ctypea_p(idx,ib)=ctype(ib)
              cindma_p(idx,ib)=cindm(ib)
              do it=1,itmaxa_p(idx)
                do id=1,idmaxa_p(idx)
                  peca_p(idx,it,id,ib)=pec(it,id,ib)
                enddo
              enddo
            enddo

         else
             lpeca_p(idx)=.false.
             nbsela_p(idx)=0
             isppra_pec_p(idx,1)=0
             ispbra_pec_p(idx,1)=0
             isstgra_pec_p(idx,1)=0
             iszra_pec_p(idx,1)=0
             cwavela_p(idx,1)=' '
             cfilea_p(idx,1)=' '
             ctypea_p(idx,1)=' '
             cindma_p(idx,1)=' '
         endif

       enddo

       return
c
c-----------------------------------------------------------------------
c
 2000 format(1x,30('*'),' dxdata_15 error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(1x,30('*'),' dxdata_15 error   ',30('*')//
     &       2x,a,a,2i3)
 2002 format(1x,30('*'),' dxdata_15 warning ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,30('*'),' program terminated ',29('*'))
 2004 format(/1x,30('*'),' program continues  ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
