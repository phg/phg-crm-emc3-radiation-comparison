CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adaslib/dxspl2.for,v 1.2 2004/07/06 13:34:00 whitefor Exp $ Date $Date: 2004/07/06 13:34:00 $
CX
      SUBROUTINE DXSPL2( ISWIT  , LSWIT , IZ1   ,
     &                   NDOUT  , NTOUT ,
     &                   NDIN   ,
     &                   IDE    , ITE   ,
     &                   MAXD   , DIN   , DOUT  ,
     &                   DINTRP ,
     &                   ATTY
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DXSPL2 *********************
C
C  PURPOSE: PERFORMS THE SECOND PART OF A 3 WAY SPLINE ON INPUT DATA.
C           GENERATES A TABLE OF LOG10( COEFFTS./POWERS )    COVERING
C           'ITE' TEMPERATURES AND 'MAXD' DENSITIES FOR THE ELEMENT
C           RECOMBINING ION CHARGE GIVEN BY 'IZ1'.
C
C  CALLING PROGRAM: D1SPLN/D4DATA
C
C  DATA:
C
C           THE  SOURCE  DATA ORIGINATES AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             6. JETUID.PRB<YR>.DATA
C             7. JETUID.PRC<YR>.DATA
C             8. JETUID.PLT<YR>.DATA
C             9. JETUID.PLS<YR>.DATA
C            10. JETUID.MET<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C
C           THE PARTICULAR TYPE OPENED (1-10) IS SELECTED BY 'ISWIT'
C           IT IS PASSED IN A MODIFIED FORM AFTER PROCESSING BY
C           DXSPL1.
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ISWIT   = DATA TYPE SELECTOR (1 -> 10)
C  INPUT : (L*4)  LSWIT   = .TRUE.  => IONISATION POTENTIALS PRESENT
C                           .FALSE. => IONS. POTENTIALS NOT PRESENT
C  INPUT : (I*4)  IZ1     = OUTPUT - ELEMENT RECOMBINING ION CHARGE
C
C  INPUT : (I*4)  NDOUT   = OUTPUT - MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  NTOUT   = OUTPUT - MAXIMUM NUMBER OF TEMPERATURES
C
C  INPUT : (I*4)  NDIN    = INPUT  - MAXIMUM NUMBER OF DENSITIES
C
C  INPUT : (I*4)  IDE     = INPUT  - NUMBER OF REDUCED DENSITIES
C  INPUT : (I*4)  ITE     = INPUT  - NUMBER OF REDUCED TEMPERATURES
C
C  INPUT : (I*4)  MAXD    = OUTPUT - NUMBER OF REDUCED DENSITIES
C                                                           ( <= NDOUT )
C  INPUT : (R*8)  DIN()   = INPUT  - SET OF 'IDE' REDUCED ELECTRON DENS-
C                                    ITIES ).
C  INPUT : (R*8)  DOUT()  = OUTPUT - SET OF 'MAXD' ELECTRON  DENSITIES
C                                    (UNITS: CM-3).
C
C  OUTPUT: (L*4)  DINTRP() = .TRUE.  => 'ATTY(,)' VALUE FOR DENSITY
C                                       INDEX INTERPOLATED.
C                          = .FALSE. => 'ATTY(,)' VALUE FOR DENSITY
C                                       INDEX EXTRAPOLATED.
C                            1ST DIMENSION: DENSITY INDEX
C
C  IN/OUT: (R*8)  ATTY(,) = WORKING SPACE FOR 3-WAY SPLINE ITERPOLATION
C                           (STORES LOG10 (INTERPOLATED VALUES))
C                           INPUT 'ATTY' VALUES ARE ASSIGNED TO 'YIN' &
C                           THEN 'YOUT' VALUES ARE ASSIGNED TO 'ATTY'.
C                           1ST DIMENSION: TEMPERATURE
C                           2ND DIMENSION: DENSITY
C
C          (I*4)  NDDIM1  = PARAMETER = MUST BE EQUAL TO OR GREATER THAN
C                           THE MAXIMUM NUMBER OF INPUT DENSITIES.
C          (I*4)  NDDIM2  = PARAMETER = MUST BE EQUAL TO OR GREATER THAN
C                           THE MAXIMUM NUMBER OF OUTPUT DENSITIES.
C
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = 0, 1, 2, 3)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  Z1R7    = 1 / (IZ1**7)
C          (R*8)  YIN()   ='ATTY(,)' AT FIXED TEMPERATURE -
C                                                  DIMENSION => DENSITY
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C          (R*8)  XOUT()  = 'DOUT()' VALUES CONVERTED TO REDUCED DENSITY
C          (R*8)  YOUT()  = SPLINE INTERPOLATED 'ATTY(,,)' VALUES  FOR
C                           REDUCED DENSITY EQUAL  TO  'XOUT()'  AT  A
C                           FIXED TEMPERATURE.
C
C NOTE:
C                         SPLINE IS CARRIED OUT ON:
C                 'ATTY(,,)' VALUES AT FIXED TEMPERATURE
C                                  VERSUS
C                          LOG10( REDUCED DENSITY )
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE  :  13/06/91 - PE BRIDEN: ADAS91 VERSION OF 'D4SPL2'
C
C UPDATE:  07/08/91 - PE BRIDEN: CHANGED THE LINE -
C                                Z1R7 = 1.0 / DBLE(IZ1**7)
C                                TO -
C                                Z1R7 = 1.0 / DBLE(IZ1)**7
C                                TO AVOID INTEGER OVERFLOW WHEN IZ1>21
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C VERSION: 1.2                          DATE: 25-10-97
C MODIFIED: LORNE HORTON (JET)
C               - FORCED ZERO LOW DENSITY DEPENDENCE
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NDDIM1               , NDDIM2
C-----------------------------------------------------------------------
      PARAMETER( NDDIM1 = 24          , NDDIM2 = 40    )
C-----------------------------------------------------------------------
      INTEGER    ISWIT                , IZ1            ,
     &           NDOUT                , NTOUT          ,
     &           NDIN                 ,
     &           IDE                  , ITE            ,
     &           MAXD
      INTEGER    IOPT                 , ID             , IT
C-----------------------------------------------------------------------
      REAL*8     Z1R7
C-----------------------------------------------------------------------
      LOGICAL    LSWIT                , LSETX
C-----------------------------------------------------------------------
      REAL*8     DOUT(NDOUT)          , DIN(NDIN)
      REAL*8     YIN(NDDIM1)          , DF(NDDIM1)     ,
     &           XOUT(NDDIM2)         , YOUT(NDDIM2)
      REAL*8     ATTY(NTOUT,NDOUT)
C-----------------------------------------------------------------------
      LOGICAL    DINTRP(NDOUT)
C-----------------------------------------------------------------------
      REAL*8     DLOG10
      INTRINSIC  DLOG10
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NDDIM1.LT.NDIN)
     &   STOP ' DXSPL2 ERROR: NDDIM1 < NDIN  - INCREASE NDDIM1'
      IF (NDDIM2.LT.NDOUT)
     &   STOP ' DXSPL2 ERROR: NDDIM2 < NDOUT - INCREASE NDDIM2'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS ACCORDING TO DATA TYPE.
C-----------------------------------------------------------------------
C
         IF (ISWIT.EQ.1) THEN
            IOPT = 2
         ELSEIF (ISWIT.EQ.2) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.3) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.4) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.5) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.6) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.7) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.8) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.9) THEN
            IOPT = 4
         ELSEIF (ISWIT.EQ.10) THEN
            IOPT = 4
         ELSE
            IOPT = 4
         ENDIF
C-----------------------------------------------------------------------
      LSETX=.TRUE.
C
C-----------------------------------------------------------------------
C CONVERT DENSITIES 'DOUT()' INTO REDUCED DENSITIES 'XOUT()'.
C-----------------------------------------------------------------------
C
      Z1R7 = 1.0 / DBLE(IZ1)**7
C
         DO 1 ID=1,MAXD
            XOUT(ID) = DOUT(ID) * Z1R7
    1    CONTINUE
C
C-----------------------------------------------------------------------
C PERFORM SPLINE INTERPOLATION/EXTRAPOLATION FOR EACH TEMPERATURE VALUE.
C-----------------------------------------------------------------------
C
         DO 2 IT=1,ITE
C
               DO 3 ID=1,IDE
                  YIN(ID) = ATTY(IT,ID)
    3          CONTINUE
C
            CALL XXSPLE( LSETX , IOPT   , DLOG10 ,
     &                   IDE   , DIN    , YIN    ,
     &                   MAXD  , XOUT   , YOUT   ,
     &                   DF    , DINTRP
     &                 )
C
               DO 4 ID=1,MAXD
                  ATTY(IT,ID) = YOUT(ID)
    4          CONTINUE
C
    2    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
