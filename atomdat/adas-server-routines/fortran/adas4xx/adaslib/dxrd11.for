       subroutine dxrd11( dsnfull, lpart  , ifail  ,
     &                    iz0    , npart  , iprtd  , igrdd , iclass ,
     &                    iz1    , itval  ,
     &                    isdimd , izdimd , itdimd ,
     &                    ismax  , izmax  , itmax  , idmax , nptn   ,
     &                    dtev   , ddens  ,
     &                    dtevd  , ddensd , drcof , zdata  ,
     &                    drcofi
     &                  )
       implicit none

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DXRD11 *********************
C
C PURPOSE : To extract collisional dielectronic data  from
C           either partial (metastable/parent resolved) or standard
C           (unresolved) isonuclear master files
C
C                CLASS INDEX    TYPE      GCR DATA CONTENT
C                -----------    ----      ----------------
C                    1          acd       recombination coeffts
C                    2          scd       ionisation coeffts
C                    3          ccd       CX recombination coeffts
C                    4          prb       recomb/brems power coeffts
C                    5          prc       CX power coeffts
C                    6          qcd       base meta. coupl. coeffts
C                    7          xcd       parent meta. coupl. coeffts
C                    8          plt       low level line power coeffts
C                    9          pls       represent. line power coefft
C                   10          zcd       effective charge
C                   11          ycd       effective squared charge
C                   12          ecd       effective ionisation potential
C
C
C INPUT  : (C*120) DSNINC   = ISONUCLEAR MASTER FILE NAME - VERIFIED
C                             AND READY FOR DYNAMIC ALLOCATION.
C INPUT  : (L*4)  LPART     = .TRUE.  => PARTIAL (RESOLVED) MASTER DATA
C                            . FALSE. => UNSRESOLVED MASTER DATA
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX ON INPUT
C INPUT  : (I*4)  IPRTD     = REQUIRED PARENT INDEX
C INPUT  : (I*4)  IGRDD     = REQUIRED GROUND INDEX
C INPUT  : (I*4)  ICLASS    = CLASS OF DATA (1 - 9 )
C INPUT  : (I*4)  IZ1       = REQUIRED ION CHARGE + 1
C INPUT  : (I*4)  ITVAL     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (R*8)  DTEV()    = DLOG10(ELECTRON TEMPERATURES (EV))
C INPUT  : (R*8)  DDENS()   = DLOG10(ELECTRON DENSITIES (CM-3))
C
C OUTPUT : (I*4)  IFAIL     = 0    IF ROUTINE SUCCESSFUL - DATA FOR THE
C                                  REQUESTED YEAR USED.
C                           = 1    IF ROUTINE OPEN STATEMENT FAILED
C                           = 2    IF FILE EXISTS BUT REQUIRED DATA
C                                  BLOCK DOES NOT
C OUTPUT : (I*4)  ISMAXD    = NUMBER OF (CHARGE, PARENT, METASTABLE)
C                             BLOCKS IN SELECTED MASTER FILE
C OUTPUT : (I*4)  IZMAXD    = NUMBER OF ZDATA() VALUES IN SELECTED
C                             MASTER FILE
C OUTPUT : (I*4)  ITMAXD    = NUMBER OF DTEVD() VALUES IN SELECTED
C                             MASTER FILE
C OUTPUT : (I*4)  IDMAXD    = NUMBER OF DDENSD() VALUES IN SELECTED
C                             MASTER FILE
C OUTPUT : (I*4)  NPARTR()  = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX FOUND IN MASTER FILE
C OUTPUT : (R*8)  DTEVD()   = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DDENSD()  = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DRCOFD(,,)= DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C                             IN SELECTED MASTER FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C OUTPUT : (R*8)  ZDATA()   = CHARGE + 1 FOR IONS IN SELECTED MASTER
C                             FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C OUTPUT : (R*8)  DRCOFI()  = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C
C PROGRAM: (C*80) DSNOLD    = FILE NAME USED IN PREVIOUS CALL
C          (C*80) CLINE     = GENERAL CHARACTER VARIABLE
C          (C*80) CTERM     = TERMINATOR LINE - '-' FILLED VARIABLE
C          (C*4)) CPATRN()  = PATTERN USED TO DETECT DATA CLASS
C          (I*4)  IZ0D      = NUCLEAR CHARGE READ FROM MASTER FILE
C          (I*4)  IZ1MIN    = MINIMUM CHARGE+1 READ FROM MASTER FILE
C          (I*4)  IZ1MAX    = MAXIMUM CHARGE+1 READ FROM MASTER FILE
C          (I*4)  IABT      = ABORT CODE
C          (I*4)  INDSEL    = LOCATION OF (CHARGE,PRNT,GRND)
C                             DATA BLOCK IN FILE
C          (I*4)  IZDAT     = CURRENT DATA BLOCK ION CHARGE +1
C          (I*4)  ISEL      = GENERAL INDEX
C          (I*4)  I         = GENERAL INDEX
C          (I*4)  IT        = GENERAL INDEX
C          (I*4)  ID        = GENERAL INDEX
C          (I*4)  IZCHK     = INDEX TO VERIFY DATA Z1 SET COMPLETE
C          (I*4)  IPRTR()   = PARENT INDICES IN DATA SET
C          (I*4)  IGRDR()   = GROUND INDICES IN DATA SET
C          (I*4)  LCK       = MUST BE GREATER THAN 'ITMAXD' & 'IDMAXD'
C                             & 'ITMAX' - ARRAY SIZE FOR SPLINE CALCS.
C          (R*8)  A()       = GENERAL ARRAY
C          (R*8)  DRCOF0(,) = INTERPOLATION OF DRCOFD(,,) W.R.T DTEV()
C          (L*8)  LEXIST    = TRUE --- FILE TO OPEN EXISTS ELSE NOT
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                             SPLINE ROUTINE 'XXSPLN', SEE 'XXSPLN'.
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO X-AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO X-AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLN')
C          (R*8)  DY()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4FCTN     ADAS      CONVERT STRING TO INTEGER FORM
C          R8FUN1
C
C NOTES: Based on dxrdnm.for
C
C AUTHOR  : Martin O'Mullane
C DATE    : 12-10-2009
C
C VERSION : 1.1
C DATE    : 12-10-2009
C MODIFIED: Martin O'Mullane
C               - First version.
C
C VERSION : 1.2
C DATE    : 14-03-2013
C MODIFIED: Martin O'Mullane
C               - Increase LCK to 101 (from 100).
C
C-----------------------------------------------------------------------
       integer   iunt12   , lck
       integer   l1
C-----------------------------------------------------------------------
       parameter ( l1     =  1 )
       parameter ( iunt12 = 12 , lck = 101 )
C-----------------------------------------------------------------------
       integer   isdimd    , iddimd   , itdimd    , izdimd
       integer   ndptnl    , ndptn     , ndptnc   , ndcnct
       parameter (ndptnl = 4, ndptn =128,  ndptnc =256  , ndcnct=100)
C-----------------------------------------------------------------------
       integer   iclass    , iz1
       integer   iz0       , is1min    , is1max
       integer   iblmx     , ismax     , itmax     , idmax
       integer   nptnl     , ncnct
       integer   it        , id        , isel      , ifail     ,
     &           iopt      , izdat     , itval     , i
       integer   igrdd     , igrdr     , indsel    , iprtd     ,
     &           izmax     , nblock
C-----------------------------------------------------------------------
       logical   lpart     , lres      , lstan     , lptn      ,
     &           lread     , lsetx
C-----------------------------------------------------------------------
       real*8    dnr_ams
C-----------------------------------------------------------------------
       character dsnfull*120   , dsnold*120
       character dnr_ele*12
C-----------------------------------------------------------------------
       integer   npart(izdimd)
       integer   nptn(ndptnl)          , nptnc(ndptnl,ndptn)
       integer   iptnla(ndptnl)        , iptna(ndptnl,ndptn)
       integer   iptnca(ndptnl,ndptn,ndptnc)
       integer   icnctv(ndcnct)
       integer   isppr(isdimd)   , ispbr(isdimd)   , isstgr(isdimd)
       integer   isppr_s(lck)    , ispbr_s(lck)    , isstgr_s(lck)
C-----------------------------------------------------------------------
       real*8    zdata(isdimd)
       real*8    dtev(itval)            , ddens(itval)
       real*8    ddensd(itdimd)         , dtevd(itdimd)
       real*8    drcof(isdimd,itdimd,itdimd)
       real*8    drcofi(itval)
       real*8    a(lck)  ,   dy(lck)  , drcof0(lck,lck)
C-----------------------------------------------------------------------
       external  r8fun1
C-----------------------------------------------------------------------
       data      lread  /.false./
C-----------------------------------------------------------------------
       save      lread , dsnold , nblock , isstgr_s , isppr_s , ispbr_s
C-----------------------------------------------------------------------

       iddimd = itdimd
C-----------------------------------------------------------------------
C Get data from adf11 dataset but check if it was read already
C-----------------------------------------------------------------------

       if (dsnfull.eq.dsnold.and.lread) goto 20

       open(unit=iunt12, file=dsnfull, err=9999)

       call xxdata_11( iunt12  ,
     &                 iclass ,
     &                 isdimd , iddimd , itdimd ,
     &                 ndptnl , ndptn  , ndptnc , ndcnct ,
     &                 iz0    , is1min , is1max ,
     &                 nptnl  , nptn   , nptnc  ,
     &                 iptnla , iptna  , iptnca ,
     &                 ncnct  , icnctv ,
     &                 iblmx  , ismax  , dnr_ele, dnr_ams,
     &                 isppr  , ispbr  , isstgr ,
     &                 idmax  , itmax  ,
     &                 ddensd , dtevd  , drcof  ,
     &                 lres   , lstan  , lptn
     &               )

       lread = .true.
       dsnold = dsnfull

       nblock = iblmx
       do i = 1, nblock
          isstgr_s(i) = isstgr(i)
          isppr_s(i)  = isppr(i)
          ispbr_s(i)  = ispbr(i)
       end do

       close(iunt12)


   20  continue

C-----------------------------------------------------------------------
C Make sure set array bounds are valid
C-----------------------------------------------------------------------

       IF (LCK.LT.ITDIMD) STOP
     &              ' DXRD11 ERROR: ARRAY DIMENSION LCK < ITDIMD'
       IF (LCK.LT.ISDIMD) STOP
     &              ' DXRD11 ERROR: ARRAY DIMENSION LCK < ISDIMD'
       IF (LCK.LT.ITMAX ) STOP
     &              ' DXRD11 ERROR: ARRAY DIMENSION LCK < ITMAX'


C-----------------------------------------------------------------------
C      SELECT CORRECT DATA BLOCK REQUESTED
C-----------------------------------------------------------------------

       indsel = 0
       do isel = 1, nblock
          izdat = isstgr_s(isel)
          zdata(isel) = izdat
          if (lpart) then
            if (iclass.eq.4.or.iclass.eq.5.or.
     &          iclass.eq.8.or.iclass.eq.9) then
              if( izdat .eq. iz1  .and. isppr_s(isel) .eq. iprtd ) then
                  indsel = isel
              endif
            else
              if( izdat .eq. iz1  .and. isppr_s(isel) .eq. iprtd .and.
     &            ispbr_s(isel) .eq. igrdd) then
                  indsel = isel
              endif
            endif
          else
              if( izdat .eq. iz1 ) then
                  indsel = isel
              endif
         endif
       end do
       izmax = is1max - is1min + 1


       if( indsel .eq. 0 ) then
           ifail = 2
           return
       endif

C-----------------------------------------------------------------------
C Interpolate using splines
C-----------------------------------------------------------------------
C
C
C Interpolate DRCOF(,,,) wrt temperature

       lsetx = .TRUE.
       iopt  = 0

       do id = 1 , idmax

          do it = 1 , itmax
             a(it) = drcof(indsel,it,id)
          end do

          call xxspln( lsetx  , iopt  , r8fun1       ,
     &                 itmax  , dtevd , a            ,
     &                 itval  , dtev  , drcof0(1,id) ,
     &                 dy
     &               )

       end do

C Next interpolate above result wrt density

       lsetx = .TRUE.
       iopt  = 0

       do it = 1 , itval

          do id = 1 , idmax
             a(id) = drcof0(it,id)
          end do

          call xxspln( lsetx  , iopt      , r8fun1     ,
     &                 idmax  , ddensd    , a          ,
     &                 l1     , ddens(it) , drcofi(it) ,
     &                 dy
     &               )

       end do

       return

C-----------------------------------------------------------------------
C Data set opening/existence error handling
C-----------------------------------------------------------------------

 9999  ifail = 1
       return

C-----------------------------------------------------------------------

       end
