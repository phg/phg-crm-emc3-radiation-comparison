C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adaslib/dxcomp.for,v 1.2 2004/07/06 13:33:12 whitefor Exp $ Date $Date: 2004/07/06 13:33:12 $
C UNIX-IDL PORT - SCCS INFO: MODULE @(#)dxcomp.for	1.1 DATE 08/20/96

      SUBROUTINE DXCOMP( NOCCUM , NOCCUP , LMATCH , IORBIT )
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN 77 SUBROUTINE: DXCOMP ******************
C
C  PURPOSE: COMPARES PARENT AND RECOMBINE ION METASTABLE CONFIGURATION
C           EXPANDED ORBITAL VECTORS.  RETURNS .TRUE. IF ONE DIFFERENCE
C           AND GIVES DIFFERING ORBITAL.
C
C  CALLING PROGRAM: D7LINK
C
C  SUBROUTINE:
C
C  INPUT  :  (I*4)   NOCCUM() = OCCUPANCY FOR EACH DECIMAL ORBITAL
C                               INDEX 1-15 OF RECOMBINED ION METASTABLE
C  INPUT  :  (I*4)   NOCCUP() = OCCUPANCY FOR EACH DECIMAL ORBITAL
C                               INDEX 1-15 OF PARENT METASTABLE
C
C  OUTPUT :  (L*4)   LMATCH   = .TRUE.  => ONE ORBITAL DIFFERENCE
C                               .FALSE. => 0 OR >1 MISMATCH
C  OUTPUT :  (L*4)   IORBIT   = DECIMAL ORBITAL INDEX FOR MISMATCH
C
C            (I*4)   I      = GENERAL INDEX
C
C  ROUTINES:  NONE
C
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    05/06/96
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    20TH AUGUST 1996
C
C VERSION: 1.1				DATE: 20-08-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 22-11-2003
C MODIFIED: Martin O'Mullane
C	    - Extend NOCCUM and NOCCUP to 61 (from 15).
C
C----------------------------------------------------------------------
      INTEGER    I       , IORBIT    , ICOUNT
C----------------------------------------------------------------------
      INTEGER    NOCCUM(61)          , NOCCUP(61)
C----------------------------------------------------------------------
      LOGICAL    LMATCH
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C                                                                      
C----------------------------------------------------------------------

      ICOUNT = 0
      DO 20 I=1,61
        IF(NOCCUM(I).EQ.NOCCUP(I))THEN
            GO TO 20
        ELSEIF(NOCCUM(I).EQ.NOCCUP(I)+1)THEN
            ICOUNT = ICOUNT+1
            IORBIT = I
        ELSE
            ICOUNT = ICOUNT+2
        ENDIF
   20 CONTINUE
      IF(ICOUNT.EQ.1) THEN
          LMATCH = .TRUE.
      ELSE
          LMATCH = .FALSE.
          IORBIT = 0
      ENDIF

      RETURN
C----------------------------------------------------------------------
      END
