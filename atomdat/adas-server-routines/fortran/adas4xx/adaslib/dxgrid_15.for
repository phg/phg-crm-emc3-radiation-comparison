       subroutine dxgrid_15( idsets     ,
     &                       idstore    , iddimd     , itdimd     ,
     &                       idmaxa_p   , itmaxa_p   ,
     &                       ddensa_p   , dteva_p    ,
     &                       nde_pec_c  , nte_pec_c  ,
     &                       ddea_pec_c , dtea_pec_c ,
     &                       iss_min    , iss_max    ,
     &                       nbsela_p   ,
     &                       peca_p     , lpeca_p    ,
     &                       peca_c     , lpeca_c
     &                     )
       implicit none
c
c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: dxgrid_15 ******************
c
c purpose : convert pec adf15 data blocks from a set of parent
c          superstages to a new temperature and density grid.
c
c
c  input : (i*4)  idsets    = maximum number of adf15 superstage files
c                             which can be read in.
c  input : (i*4)  idstore   = maximum number of blocks in a
c                             superstage adf15 pec file
c  input : (i*4)  iddimd    = maximum number of dens values in an
c                             adf15 pec file
c  input : (i*4)  itdimd    = maximum number of temp values in an
c                             adf15 pec file
c
c  input : (i*4)  idmaxa_p()= number of densities in pec superstage
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c  input : (i*4)  itmaxa_p()= number of temperatures in pec superstage
c                             1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  input : (r*8)  ddensa_p(,)= densities in pec superstage
c                              1st dim: density index
c                              2nd dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c  input : (r*8)  dteva_p(,)= temperatures in pec superstage
c                              1st dim: temperature index
c                              2nd dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c  input  : (i*4)  nde_pec_c   = number of dens values in child
c                               isonuclear master files
c  input  : (i*4)  nte_pec_c   = number of temp values in child
c                               isonuclear master files
c  input  : (r*8)  ddea_pec_c()= log10(elec. dens.(cm-3)) from parent adf11
c  input  : (r*8)  dtea_pec_c()= log10(elec. temp. (eV) from parent adf11
c
c  input  : (i*4)  iss_min   = lowest parent superstage index in child
c                             superstage (n.b. indexing starts at 1 not 0)
c  input  : (i*4)  iss_max   = highest parent superstage index in child
c                             superstage(n.b. indexing starts at 1 not 0)
c  input  : (i*4)  nbsela_p()    = number of (sstage, parent, base)
c                                 blocks in isonuclear master file class
c                                 1st dim: adf11 class index
c  input  : (r*8)  peca_p(,,,)= pec coefficients from parent superstages
c                               1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                               2nd dim: electron temperature index
c                               3rd dim: electron density index
c                               4th dim: data-block index
c input  : (l*4)  lpeca_p()   = .true.  => data present for class
c                               .false. => data not present for class
c                               1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c output: (r*8)  peca_c(,,,)  = regridded pec coefficients for parent
c                               superstages
c                               1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c                               2nd dim: electron temperature index
c                               3rd dim: electron density index
c                               4th dim: data-block index
c output: (l*4)  lpeca_c()   = .true.  => data present for superstage
c                               .false. => data not present for class
c                               1st dim: count over parent superstages
c                                      (from 1 to iss_max-iss_min+1)
c
c program: (c*80) dsnold    = file name used in previous call
c          (c*80) cline     = general character variable
c          (c*80) cterm     = terminator line - '-' filled variable
c          (c*4)) cpatrn()  = pattern used to detect data class
c          (i*4)  iz0d      = nuclear charge read from master file
c          (i*4)  iz1min    = minimum charge+1 read from master file
c          (i*4)  iz1max    = maximum charge+1 read from master file
c          (i*4)  iabt      = abort code
c          (i*4)  indsel    = location of (charge,prnt,grnd)
c                             data block in file
c          (i*4)  izdat     = current data block ion charge +1
c          (i*4)  isel      = general index
c          (i*4)  i         = general index
c          (i*4)  it        = general index
c          (i*4)  id        = general index
c          (i*4)  izchk     = index to verify data z1 set complete
c          (i*4)  iprtr()   = parent indices in data set
c          (i*4)  igrdr()   = ground indices in data set
c          (i*4)  lck       = must be greater than 'itmaxd' & 'idmaxd'
c                             & 'itmax' - array size for spline calcs.
c          (r*8)  a()       = general array
c          (r*8)  drcof0(,) = interpolation of drcofd(,,) w.r.t dtev()
c          (l*8)  lexist    = true --- file to open exists else not
c          (i*4)  l1      = parameter = 1
c          (i*4)  iopt    = defines the boundary derivatives for the
c                             spline routine 'xxspln', see 'xxspln'.
c          (l*4)  lsetx   = .true.  => set up spline parameters relating
c                                      to x-axis.
c                           .false. => do not set up spline parameters
c                                      relating to x-axis.
c                                      (i.e. they were set in a previous
c                                            call )
c                           (value set to .false. by 'xxspln')
c          (r*8)  dy()    = spline interpolated derivatives
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          i4fctn     adas      convert string to integer form
c          xxspln     adas      splinig routine
c
c          (r*8 adas function - 'r8fun1' ( x -> x) )
c
c  author:  h. p. summers, university of strathclyde
c           ja7.08
c           tel. 0141-548-4196
c
c date   : 14/09/05
c
c version: 1.1                          date: 14/09/2005
c modified: Hugh Summers
c               - first edition.
c
c  Version : 1.1
c  Date    : 03-01-2007
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       integer   ntdim       , nddim
c-----------------------------------------------------------------------
       parameter ( ntdim = 50 , nddim = 40 )
c-----------------------------------------------------------------------
       integer   i4unit
       integer   idsets   , idstore , iddimd  , itdimd
       integer   iopt
       integer   nte_pec_c          , nde_pec_c
       integer   iss_min            , iss_max
       integer   ib       , it      , id      , is1
       integer   idmax_p            , itmax_p
c-----------------------------------------------------------------------
       integer   nbsela_p(idsets)
       integer   idmaxa_p(idsets)   , itmaxa_p (idsets)
c-----------------------------------------------------------------------
       real*8    r8fun1
c-----------------------------------------------------------------------
       real*8    dtev_p(ntdim)             , ddens_p(nddim)
       real*8    dteva_p(itdimd,idsets)    , ddensa_p(iddimd,idsets)
       real*8    dtea_pec_c(itdimd)          , ddea_pec_c(iddimd)
       real*8    peca_p(idsets,itdimd,iddimd,idstore)
       real*8    peca_c(idsets,itdimd,iddimd,idstore)
       real*8    drcof0(ntdim,nddim)
       real*8    ain(ntdim )  , aout(ntdim)  , daout(ntdim)
       real*8    bin(nddim)   , bout(nddim)  , dbout(nddim)
c-----------------------------------------------------------------------
       logical   lsetx
c-----------------------------------------------------------------------
       logical   lpeca_p(idsets)  , lpeca_c(idsets)
c-----------------------------------------------------------------------
       external  r8fun1
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c  check internal dimensions are sufficient
c-----------------------------------------------------------------------

       if(nddim.lt.iddimd) then
         write(i4unit(-1),2000)'internal dimension error: ',' nddim =',
     &                         nddim,' < iddimd =',iddimd
         write(i4unit(-1),2003)
         stop
       endif

       if(ntdim.lt.itdimd) then
         write(i4unit(-1),2000)'internal dimension error: ',' ntdim =',
     &                         ntdim,' < itdimd =',itdimd
         write(i4unit(-1),2003)
         stop
       endif

c-----------------------------------------------------------------------
c  interpolate using splines - work over class and each block
c-----------------------------------------------------------------------
c
       do is1=1,iss_max-iss_min+1
         lpeca_c(is1) = lpeca_p(is1)
         if(lpeca_p(is1))then
             idmax_p=idmaxa_p(is1)
             itmax_p=itmaxa_p(is1)

             do ib=1,nbsela_p(is1)

c--------------------------------------------
c  interpolate peca_p(,,,) w.r.t temperature
c--------------------------------------------
               lsetx = .true.
               iopt  = 0

               do id = 1 , idmax_p

                 do it = 1 , itmax_p
                    dtev_p(it)=dteva_p(it,is1)
                    ain(it) = peca_p(is1,it,id, ib)
                 enddo

                 call xxspln( lsetx       , iopt      , r8fun1       ,
     &                        itmax_p     , dtev_p    , ain          ,
     &                        nte_pec_c   , dtea_pec_c, aout         ,
     &                        daout
     &                      )
                 do it = 1, nte_pec_c
                   drcof0(it,id)=aout(it)
                 enddo

               enddo
c--------------------------------------------
c  interpolate above w.r.t density
c--------------------------------------------
               lsetx = .true.
               iopt  = 0

               do  it = 1 , nte_pec_c

                 do id = 1 , idmax_p
                    ddens_p(id)=ddensa_p(id,is1)
                    bin(id) = drcof0(it,id)
                 enddo

                 call xxspln( lsetx      , iopt       , r8fun1     ,
     &                        idmax_p    , ddens_p    , bin        ,
     &                        nde_pec_c  , ddea_pec_c , bout       ,
     &                        dbout
     &                      )

                 do id = 1, nde_pec_c
                   peca_c(is1,it,id,ib)=bout(id)
                 enddo

               enddo

             enddo

         endif

       enddo


       return
c
c-----------------------------------------------------------------------
c
 2000 format(1x,30('*'),' dxgrid_15 error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2003 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
