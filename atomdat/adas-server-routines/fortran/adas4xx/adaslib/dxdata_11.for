       subroutine dxdata_11( iunit    , ndclas   ,
     &                       dsflla   , lsela    , lexsa    ,
     &                       isdimd   , iddimd   , itdimd   ,
     &                       ndptnl   , ndptn    , ndptnc   , ndcnct   ,
     &                       iz0_p    , is1min_p , is1max_p ,
     &                       nptnl_p  , nptn_p   , nptnc_p  ,
     &                       ncnct_p  , icnctv_p ,
     &                       iblmx_p  ,
     &                       idmax_p  , itmax_p  ,
     &                       ddens_p  , dtev_p   ,
     &                       drcof_p  ,
     &                       iblmxa_p , ispbra_p , isppra_p , isstgra_p,
     &                       dnr_elea_p          , dnr_amsa_p          ,
     &                       dgcra_p  , lgcra_p
     &                  )
       implicit none

c-----------------------------------------------------------------------
c
c  ****************** fortran77 subroutine: dxdata_11 ******************
c
c purpose : to extract a complete set of collisional dielectronic data
c           for a parent partition level from either partial (resolved)
c           or standard (unresolved) isonuclear master files and verify
c           their consistency.
c
c note    : (1) The source data is contained as sequential datasets
c               with the naming convention:
c                        /.../***<yr.>/***<yr><r>_<nn>_<el>.dat
c               where
c                   <yr> is a two digit year number
c                   <nn> is a two digit partition level, which may be
c                        omitted for a base partition
c                   <el> is a one or two character element symbol
c                   <r>  is the letter `r' for resolved adf11 data,
c                        otherwise omitted
c                   ***  represents the adf11 class type as:
c
c                class index    type      GCR data content
c                -----------    ----      ----------------
c                    1          acd       recombination coeffts
c                    2          scd       ionisation coeffts
c                    3          ccd       CX recombination coeffts
c                    4          prb       recomb/brems power coeffts
c                    5          prc       CX power coeffts
c                    6          qcd       base meta. coupl. coeffts
c                    7          xcd       parent meta. coupl. coeffts
c                    8          plt       low level line power coeffts
c                    9          pls       represent. line power coefft
c                   10          zcd       effective charge
c                   11          ycd       effective squared charge
c                   12          ecd       effective ionisation potential
c
c               and data of classes 6 and 7 do not exist for the
c               standard unresolved case.
c
c           (2) Consistency is checked with the analysed parameters of
c               a reference adf11 file provided as input to the sub-
c               routine.
c
c
c
c  input : (i*4)  iunit     = unit to which input file is allocated
c  input : (i*4)  ndclas    = number of adf11 master file classes
c  input : (c*120)dsflla()  = master file names
c                             1st dim: adf11 class index
c  input : (l*4)  lsela()   = .true.  => input data set type for this
c                                        index selected
c                           = .false. => input data set for this index
c                                        not selected
c                             1st dim: adf11 class index
c  input : (l*4)  lexsa()   = .true.  => input data set type for this
c                                        selected index exists
c                           = .false. => input data set does not exist
c                                        for this selected index
c                             1st dim: adf11 class index
c
c
c  input : (i*4)  isdimd    = maximum number of (sstage, parent, base)
c                             blocks in isonuclear master files
c  input : (i*4)  iddimd    = maximum number of dens values in
c                             isonuclear master files
c  input : (i*4)  itdimd    = maximum number of temp values in
c                             isonuclear master files
c  input : (i*4)  ndptnl    = maximum level of partitions
c  input : (i*4)  ndptn     = maximum no. of partitions in one level
c  input : (i*4)  ndptnc    = maximum no. of components in a partition
c  input : (i*4)  ndcnct    = maximum number of elements in connection
c                             vector
c
c  input : (i*4)  iz0_p     = nuclear charge
c  input : (i*4)  is1min_p  = minimum ion charge + 1
c                             (generalised to connection vector index)
c  input : (i*4)  is1max_p  = maximum ion charge + 1
c                             (note excludes the bare nucleus)
c                             (generalised to connection vector index
c                              and excludes last one which always remains
c                              the bare nucleus)
c  input : (i*4)  nptnl_p   = number of partition levels in block
c  input : (i*4)  nptn_p()  = number of partitions in partition level
c  input : (i*4)  nptn_p()  = number of partitions in partition level
c                             1st dim: partition level
c  input : (i*4)  nptnc_p(,)= number of components in partition
c                             1st dim: partition level
c                             2nd dim: member partition in partition level
c  input : (i*4)  ncnct_p   = number of elements in connection vector
c  input : (i*4)  icnctv_p()= connection vector of number of partitions
c                             of each superstage in resolved case
c                             including the bare nucleus
c                             1st dim: connection vector index
c
c  input : (i*4)  iblmx_p   = number of (charge, parent, ground)
c                             blocks in isonuclear master file

c  input : (i*4)  idmax_p   = number of dens values in
c                             isonuclear master files
c  input : (i*4)  itmax_p   = number of temp values in
c                             isonuclear master files
c  input : (r*8)  ddens_p() = log10(electron density(cm-3)) from adf11
c  input : (r*8)  dtev_p()  = log10(electron temperature (eV) from adf11
c
c
c  output: (r*8)  drcof_p(,,)= log10(coll.-rad. coefft.(cm^3s^-1)) from
c                             reference isonuclear master file
c                             1st dim: index of (sstage, parent, base)
c                                      block in isonuclear master file
c                             2nd dim: electron temperature index
c                             3rd dim: electron density index
c  output: (i*4)  iblmxa_p()    = number of (sstage, parent, base)
c                                 blocks in isonuclear master file class
c                                 1st dim: adf11 class index
c  output: (i*4)  isppra_p(,)   = 1st (parent) index for each partition block
c                                 1st dim: adf11 class index
c                                 2nd dim: index of (sstage, parent, base)
c                                      block in isonuclear master file
c  output: (i*4)  ispbra_p(,)   = 2nd (base) index for each partition block
c                                 1st dim: adf11 class index
c                                 2nd dim: index of (sstage, parent, base)
c                                      block in isonuclear master file
c  input : (i*4)  isstgra_p(,)  = s1 for each resolved data block
c                               (generalises to connection vector index)
c                                 1st dim: adf11 class index
c                                 2nd dim: index of (sstage, parent, base)
c                                      block in isonuclear master file
c  output: (c*12) dnr_elea_p()  = donor element (set to ' ' for classes
c                                 other than 3 and 5)
c                                 1st dim: adf11 class index
c  output: (r*8)  dnr_amsa_p()  = donor mass (set to 0 for classes other
c                                 than 3 and 5)
c                                 1st dim: adf11 class index
c  output: (r*8)  dgcra_p(,,,)  = log10(coll.-rad. coefft.) from
c                                 isonuclear master file
c                                 1st dim: adf11 class index
c                                 2nd dim: index of (sstage, parent, base)
c                                          block in isonuclear master file
c                                 3nd dim: electron temperature index
c                                 4th dim: electron density index
c  output: (l*4)  lgcra_p()     = .true.  => data present for class
c                                 .false. => data not present for class
c                                 1st dim: adf11 class index
c
c
c
c routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          xxdata_11  adas      fetch complete adf11 data from file
c
c
c author:  h. p. summers, university of strathclyde
c          ja7.08
c          tel. 0141-548-4196
c
c date:    04/10/06
c
c  Version : 1.1
c  Date    : 04-10-2006
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       integer   i4unit
       integer   iunit       , ndclas
       integer   iddimd      , itdimd     , isdimd
       integer   ndptnl      , ndptn      , ndptnc     , ndcnct
       integer   iz0_p       , is1min_p   , is1max_p
       integer   iz0         , is1min     , is1max
       integer   iblmx_p     , itmax_p    , idmax_p
       integer   ismax       , itmax      , idmax
       integer   nptnl_p     , ncnct_p
       integer   nptnl       , ncnct
       integer   iclass      , ib         , ic         , ip
       integer   it          , id
c-----------------------------------------------------------------------
       real*8    dnr_ams
c-----------------------------------------------------------------------
       logical   lres        , lstan      , lptn
c-----------------------------------------------------------------------
       integer   nptn_p(ndptnl)           , nptnc_p(ndptnl,ndptn)
       integer   icnctv_p(ndcnct)
c
       integer   nptn(ndptnl)             , nptnc(ndptnl,ndptn)
       integer   iptnla(ndptnl)           , iptna(ndptnl,ndptn)
       integer   iptnca(ndptnl,ndptn,ndptnc)
       integer   icnctv(ndcnct)
       integer   isstgr(isdimd)
       integer   ispbr(isdimd)            , isppr(isdimd)

       integer   iblmxa_p(ndclas)
       integer   ispbra_p(ndclas,isdimd)  , isppra_p(ndclas,isdimd)
       integer   isstgra_p(ndclas,isdimd)
c-----------------------------------------------------------------------
       real*8    ddens_p(iddimd)          , dtev_p(itdimd)
       real*8    ddens(iddimd)            , dtev(itdimd)
       real*8    drcof_p(isdimd,itdimd,iddimd)
       real*8    dgcra_p(ndclas,isdimd,itdimd,iddimd)
       real*8    dnr_amsa_p(ndclas)
c-----------------------------------------------------------------------
       character dsflla(ndclas)*120 , dsninc*120
       character dnr_ele*12         , dnr_elea_p(ndclas)*12
c-----------------------------------------------------------------------
       logical   lsela(ndclas), lexsa(ndclas)
       logical   lgcra_p(ndclas)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c  fetch each adf11 class in turn
c-----------------------------------------------------------------------

       do iclass = 1,ndclas
        if (lexsa(iclass).and.lsela(iclass)) then
            lgcra_p(iclass)=.true.
            dsninc = dsflla(iclass)

            open(unit=iunit , file = dsninc )

            call  xxdata_11( iunit   , iclass  ,
     &                       isdimd  , iddimd  , itdimd ,
     &                       ndptnl  , ndptn   , ndptnc , ndcnct ,
     &                       iz0     , is1min  , is1max ,
     &                       nptnl   , nptn    , nptnc  ,
     &                       iptnla  , iptna   , iptnca ,
     &                       ncnct   , icnctv  ,
     &                       iblmx_p , ismax   , dnr_ele, dnr_ams,
     &                       isppr   , ispbr   , isstgr ,
     &                       idmax   , itmax   ,
     &                       ddens   , dtev    , drcof_p  ,
     &                       lres    , lstan   , lptn
     &                     )

            close(iunit)

c-----------------------------------------------------------------------
c  execute checks against the reference file input
c-----------------------------------------------------------------------
            if(iz0_p.ne.iz0)then
              write(i4unit(-1),2000)'nuclear charge inconsistent:',
     &                              'iclass = ',iclass
              write(i4unit(-1),2003)
              stop
            endif

            if((is1min_p.ne.is1min).or.
     &         (is1max_p.ne.is1max))then
              write(i4unit(-1),2001)'superstage range inconsistent:',
     &                              ' is1min,is1max, = ',is1min,ismax
              write(i4unit(-1),2003)
              stop
            endif

            if(ncnct_p.ne.ncnct)then
              write(i4unit(-1),2000)'connection vector inconsistent:',
     &                              ' iclass = ',iclass
              write(i4unit(-1),2003)
              stop
            else
              do ic=1,ncnct
                if(icnctv_p(ic).ne.icnctv(ic))then
                    write(i4unit(-1),2000)'connection vector inconsis',
     &                                    'tent: iclass = ',iclass
                    write(i4unit(-1),2003)
                    stop
                endif

              enddo

            endif

            if(nptnl_p.ne.nptnl)then
              write(i4unit(-1),2000)'partition level inconsistent:',
     &                              ' iclass = ',iclass
              write(i4unit(-1),2003)
              stop
            endif
            if(nptn_p(1).ne.nptn(1))then
              write(i4unit(-1),2000)'partition inconsistent:',
     &                              ' iclass = ',iclass
              write(i4unit(-1),2003)
              stop
            else
              do ip=1,nptn(1)
                if(nptnc_p(1,ip).ne.nptnc(1,ip))then
                    write(i4unit(-1),2000)'partition inconsistent:',
     &                              ' iclass = ',iclass
                    write(i4unit(-1),2003)
                    stop
                endif

              enddo

            endif
c-----------------------------------------------------------------------
c  check consistent temperatures and densities
c-----------------------------------------------------------------------
            if(itmax_p.ne.itmax)then
               write(i4unit(-1),2001)'temperature range inconsistent:',
     &                               'iclass = ',iclass
               write(i4unit(-1),2003)
               stop
            else
               do it=1,itmax
                 if(dtev_p(it).ne.dtev(it)) then
                    write(i4unit(-1),2000)'temperatures incon',
     &                              'sistent: iclass = ',iclass
                    write(i4unit(-1),2003)
                    stop
                  endif
                enddo
            endif

            if(idmax_p.ne.idmax)then
               write(i4unit(-1),2001)'density range inconsistent:',
     &                               'iclass = ',iclass
               write(i4unit(-1),2003)
               stop
            else
               do id=1,idmax
                 if(ddens_p(id).ne.ddens(id)) then
                    write(i4unit(-1),2000)'densities incon',
     &                              'sistent: iclass = ',iclass
                    write(i4unit(-1),2003)
                    stop
                  endif
                enddo
            endif
c-----------------------------------------------------------------------
c  stack cx donor information
c-----------------------------------------------------------------------
           if((iclass.eq.3).or.(iclass.eq.5))then
               dnr_amsa_p(iclass) = dnr_ams
               dnr_elea_p(iclass) = dnr_ele
           else
               dnr_amsa_p(iclass) = 0.0d0
               dnr_elea_p(iclass) = ' '
           endif

c-----------------------------------------------------------------------
c  stack the output arrays
c-----------------------------------------------------------------------
            iblmxa_p(iclass)=iblmx_p
            do ib=1,iblmx_p
              isppra_p(iclass,ib)=isppr(ib)
              ispbra_p(iclass,ib)=ispbr(ib)
              isstgra_p(iclass,ib)=isstgr(ib)
              do it=1,itmax
                do id=1,idmax
                  dgcra_p(iclass,ib,it,id)=drcof_p(ib,it,id)
                enddo
              enddo
            enddo

         else
             lgcra_p(iclass)=.false.
             iblmxa_p(iclass)=0
             isppra_p(iclass,1)=0
             ispbra_p(iclass,1)=0
             isstgra_p(iclass,1)=0
         endif

       enddo

       return
c
c-----------------------------------------------------------------------
c
 2000 format(1x,30('*'),' dxdata_11 error   ',30('*')//
     &       2x,a,a,i3,a,i3 )
 2001 format(1x,30('*'),' dxdata_11 error   ',30('*')//
     &       2x,a,a,2i3)
 2003 format(/1x,30('*'),' program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
