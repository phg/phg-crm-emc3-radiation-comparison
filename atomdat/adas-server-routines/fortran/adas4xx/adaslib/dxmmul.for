CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adaslib/dxmmul.for,v 1.1 2004/07/06 13:33:28 whitefor Exp $ Date $Date: 2004/07/06 13:33:28 $
CX
       SUBROUTINE DXMMUL( NDMET ,
     &                    A     , NRA  , NCA  , B    , NRB  , NCB  ,
     &                    C
     &                  )
       IMPLICIT NONE
C
C-------------------------------------------------------------------------------
C                                                                       ********
C  ****************** FORTRAN 77 SUBROUTINE: DXMMUL ****************************
C
C  PURPOSE: CALCULATES THE PRODUCT OF TWO RECTANGULAR MATRICES
C
C  CALLING PROGRAM: D5MFSP
C
C  SUBROUTINE:
C
C  INPUT :(I*4) NDMET         = MAXIMUM DIMENSION OF MATRICES
C  INPUT :(R*8) A(,)          = FIRST MATRIX
C  INPUT :(I*4) NRA           = NUMBER OF ROWS IN MATRIX A
C  INPUT :(I*4) NCA           = NUMBER OF COLUMNS IN MATRIX A
C  INPUT :(R*8) B(,)          = SECOND MATRIX
C  INPUT :(I*4) NRB           = NUMBER OF ROWS IN MATRIX B
C  INPUT :(I*4) NCB           = NUMBER OF COLUMNS IN MATRIX B
C
C  OUTPUT:(R*8) C(,)          = RESULTANT MATRIX C
C
C         (I*4) L             = GENERAL INDEX
C         (I*4) M             = GENERAL INDEX
C         (I*4) N             = GENERAL INDEX
C
C
C  ROUTINES: NONE
C
C
C  AUTHOR:  D. BROOKS, H. P. SUMMERS, JET
C           K1/1/57
C           JET EXT. 4941
C
C  DATE:    02/06/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C-------------------------------------------------------------------------------
       INTEGER NRA,NCA,NRB,NCB,NDMET
       INTEGER L,M,N
C-------------------------------------------------------------------------------
       REAL*8  A(NDMET,NDMET),B(NDMET,NDMET),C(NDMET,NDMET)
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
C
C-------------------------------------------------------------------------------
       IF(NCA .NE. NRB)STOP
     &   'DIMENSION MISMATCH'
C-------------------------------------------------------------------------------
       DO 1 L =1,NRA
         DO 2 N =1,NCB
           C(L,N) = 0.0
            DO 3 M = 1,NCA
              C(L,N) = C(L,N)+A(L,M)*B(M,N)
 3          CONTINUE
 2       CONTINUE
 1     CONTINUE
C-------------------------------------------------------------------------------
       RETURN
C-------------------------------------------------------------------------------
       END
