      subroutine xxtotl_23(ndlev    , ndmet     , ndtem     , ndtext   ,
     &                     seq      , iz0       , iz        , iz1      ,
     &                     ctype    ,
     &                     bwno_f   , nlvl_f    , lmet_f    , lcstrg_f ,
     &                     ia_f     , code_f    , cstrga_f  ,
     &                     isa_f    , ila_f     , xja_f     , wa_f     ,
     &                     nmet_f   , imeta_f   ,
     &                     bwno_i   , nlvl_i    , lmet_i    , lcstrg_i ,
     &                     ia_i     , code_i    , cstrga_i  ,
     &                     isa_i    , ila_i     , xja_i     , wa_i     ,
     &                     nmet_i   , imeta_i   ,
     &                     nte_ion  , tea_ion   , lqred_ion , qred_ion ,
     &                     nf_a     , indf_a    , lyld_a    , yld_a    ,
     &                     nte_exc  , tea_exc   , lqred_exc , qred_exc ,
     &                     l_ion    , l_aug     , l_exc     ,
     &                     ntext    , ctext     ,
     &                     q_ion    , is_q_ion  , q_exc     , is_q_exc ,
     &                     qtot     , is_qtot
     &                    )

      implicit none
c-----------------------------------------------------------------------
c  ****************** fortran77 subroutine: xxtotl_23 ******************
c
c  purpose:  to evaluate absolute ionisation rate coefficients,
c            between initial and final metastables, using data from
c            and adf23 file.
c
c  input : (i*4)  ndlev       = maximum number of energy levels in
c                               either ion stage
c          (i*4)  ndmet       = maximum number of metastables
c          (i*4)  ndtem       = maximum number of temperatures
c          (i*4)  ndtext      = maximum number of comment text lines
c
c          (c*2)  seq         = iso-electronic sequence symbol
c          (i*4)  iz0         = nuclear charge
c          (i*4)  iz          = ionising ion charge
c          (i*4)  iz1         = ioniswd ion charge (=iz+1)
c          (c*2)  ctype       = adf23 file resol. ('ca', 'ls' or 'ic')
c          (r*8)  bwno_f        = ionis. poten. of ionised ion (cm-1)
c          (i*4)  nlvl_f        = number of levels of ionised ion (cm-1)
c          (l*4)  lmet_f      = .true.  => ionised metastables marked
c                               .false. => ionised metastables unmarked
c                                          (default action - mark ground)
c          (l*4)  lcstrg_f    = .true.  => standard config strings for
c                                          ionised ion states
c                               .false. => unreadable config string for
c                                          at least one ionised ion state
c          (i*4)  ia_f()      = index of ionised ion levels
c                               1st dim: ionised ion level index
c          (c*1)  code_f()    = met. or excit. DR parent marker (* or #)
c                               1st dim: ionised ion level index
c          (i*(*))cstrga_f()  = ionised ion configuration strings
c                               1st dim: ionised ion level index
c          (i*4)  isa_f()     = ionised ion level multiplicity
c                               1st dim: ionised ion level index
c          (i*4)  ila_f()     = ionised ion total orb. ang. mom.
c                               1st dim: ionised ion level index
c          (r*8)  xja_f()     = ionised ion level (stat wt-1)/2
c                               1st dim: ionised ion level index
c          (r*8)  wa_f()      = ionised ion level wave number (cm-1)
c                               1st dim: ionised ion level index
c          (i*4)  nmet_f      = number of ionised ion metastables
c          (i*4)  imeta_f()   = pointers to ionised metastables in full
c                               ionised ion state list
c                               1st dim: ionised metastable index
c          (r*8)  bwno_i        = ionis. poten. of ionising ion (cm-1)
c          (i*4)  nlvl_i        = number of levels of ionising ion (cm-1)
c          (l*4)  lmet_i      = .true.  => ionising metastable marked
c                               .false. => ionising metastables unmarked
c                                          (default action - mark ground)
c          (l*4)  lcstrg_i    = .true.  => standard config strings for
c                                          ionising ion states
c                               .false. => unreadable config string for
c                                          at least one ionising ion state
c          (i*4)  ia_i()      = index of ionising ion levels
c                               1st dim: ionising ion level index
c          (c*1)  code_i()    = met. or excit. DR parent marker (* or #)
c                               1st dim: ionising ion level index
c          (i*(*))cstrga_i()  = ionising ion configuration strings
c                               1st dim: ionising ion level index
c          (i*4)  isa_i()     = ionising ion level multiplicity
c                               1st dim: ionising ion level index
c          (i*4)  ila_i()     = ionising ion total orb. ang. mom.
c                               1st dim: ionising ion level index
c          (r*8)  xja_i()     = ionising ion level (stat wt-1)/2
c                               1st dim: ionising ion level index
c          (r*8)  wa_i()      = ionising ion level wave number (cm-1)
c                               1st dim: ionising ion level index
c          (i*4)  nmet_i      = number of ionising ion metastables
c          (i*4)  imeta_i()   = pointers to ionising metastables in full
c                               ionising ion state list
c                               1st dim: ionising metastable index
c          (i*4)  nte_ion()   = number of temperatures for direct ionis-
c                               ation data for initial metastable block
c                               1st dim: ionising ion metastable index
c          (r*8)  tea_ion(,)  = temperatures (K) for direct ionis-
c                               ation data for initial metastable block
c                               1st dim: ionising ion metastable index
c                               2nd dim: temperature index
c          (l*4)  lqred_ion(,)= .true. => direct ionisation data line
c                                         present for ionised ion state
c                               .false.=> data line not present for
c                                         ionised ion state.
c                               1st dim: ionising ion metastable index
c                               2nd dim: ionised ion state index
c          (r*8)  qred_ion(,,)= reduced direct ionisation rate coeffts.
c                               1st dim: ionising ion metastable index
c                               2nd dim: ionised ion state index
c                               3rd dim: temperature index
c          (i*4)  nf_a()      = number of Auger ionised ion final states
c                               1st dim: ionising ion metastable index
c          (i*4)  indf_a(,)   = Auger ionised ion final state
c                               1st dim: ionising ion metastable index
c                               2nd dim: final state index
c          (l*4)  lyld_a(,)   = .true. => Auger data for ionising ion excited state
c                               .false.=> no Auger data
c                               1st dim: ionising ion metastable index
c                               2nd dim: initial state index
c          (r*8)  yld_a(,,)   = Auger yields
c                               1st dim: ionising ion metastable index
c                               2nd dim: ionising ion excited state index
c                               3rd dim: ionised ion state index
c          (i*4)  nte_exc()   = number of temperatures for excitation
c                               data for initial metastable block
c                               1st dim: ionising ion metastable index
c          (r*8)  tea_exc(,)  = temperatures (K) for direct excit-
c                               ation data for initial metastable block
c                               1st dim: ionising ion metastable index
c                               2nd dim: temperature index
c          (l*4)  lqred_exc(,)= .true. => direct excitation data line
c                                         present for excited ion state
c                               .false.=> data line not present for
c                                         excited ion state.
c                               1st dim: ionising ion metastable index
c                               2nd dim: excited ionising ion state index
c          (r*8)  qred_exc(,,)= reduced excitation rate coeffts.
c                               1st dim: ionising ion metastable index
c                               2nd dim: excited ionising ion state index
c                               3rd dim: temperature index
c          (l*4)  l_ion()     = .true. => ionisation data present for metastable
c                               .false.=> ionisation data not present
c                               1st dim: ionising ion metastable index
c          (l*4)  l_aug()     = .true. => Auger data present for metastable
c                               .false.=> Auger data not present
c                               1st dim: ionising ion metastable index
c          (l*4)  l_exc()     = .true. => excitation data present for metastable
c                               .false.=> excitation data not present
c                               1st dim: ionising ion metastable index
c          (i*4)  ntext       = number of comment text lines
c          (c*80) ctext()     = comment text lines
c                               1st dim: text line index
c
c  output: (r*8)  q_ion(,,)      = direct ionisation rate coeffts.
c                                   1st dim: ionising ion metastable index
c                                   2nd dim: ionised ion state index
c                                   3rd dim: temperature index
c          (i*4)  is_q_ion(,,)   = scaling exponent for direct ionisation
c                                  rate coeffts.
c                                   1st dim: ionising ion metastable index
c                                   2nd dim: ionised ion state index
c                                   3rd dim: temperature index
c          (r*8)  q_exc(,,)      = excitation rate coeffts.
c                                   1st dim: ionising ion metastable index
c                                   2nd dim: excited ionising ion state index
c                                   3rd dim: temperature index
c          (i*4)  is_q_exc(,,)   = scaling exponent for excitation
c                                  rate coeffts.
c                                   1st dim: ionising ion metastable index
c                                   2nd dim: excited ionising ion state index
c                                   3rd dim: temperature index
c          (r*8)  qtot(,,)      = total ionisation rate coeffts.
c                                   1st dim: ionising ion metastable index
c                                   2nd dim: ionised ion metastable state index
c                                   3rd dim: temperature index
c          (i*4)  is_qtot(,,)   = scaling exponent for total ionisation
c                                  rate coeffts.
c                                   1st dim: ionising ion metastable index
c                                   2nd dim: ionised ion metastable state index
c                                   3rd dim: temperature index
c
c  routines:
c          routine    source    brief description
c          ------------------------------------------------------------
c          i4unit     adas      fetch unit number for output of messages
c          xxbass     adas      scale a number to a base of form a*base**ia
c          xxbasr     adas      re-base a scale number pair to a new base
c          xxbasa     adas      add two scaled number pairs of the same base
c
c  author:  Hugh Summers
c  date  :  21-05-2008
c
c
c  version  : 1.1
c  date     : 21-05-2008
c  modified : Hugh Summers
c               - first version
c
c
c-----------------------------------------------------------------------
      integer   nsym
c-----------------------------------------------------------------------
      real*8    const1     , const2    , ryd_ev
      real*8    ev_k       , elog      , base_10    , base_e
c-----------------------------------------------------------------------
      parameter( nsym = 92 )
      parameter( const1 = 8065.541d0 , const2 = 2.1716d-8 )
      parameter( ryd_ev = 13.6048d0 )
      parameter( ev_k = 11604.0d0 , elog = 4.34294945d-1 ,
     &           base_10 = 10.0d0 , base_e = 2.718281828d0)
c-----------------------------------------------------------------------
      integer   i4unit     , i4eiz0
      integer   ndlev      , ndmet      , ndtem    , ndtext
      integer   iz0        , iz         , iz1      , izf
      integer   nlvl_f     , nlvl_i
      integer   imet_i     , imet_count , indi     , ilvl
      integer   nmet_i     , nmet_f
      integer   ntext
      integer   ia         , ib          , ic
      integer   im_i       , im_f        , it
      integer   i          , j           , k       , im
c-----------------------------------------------------------------------
      real*8    bwno_f     , bwno_i
      real*8    a          , b           , c
      real*8    xia        , xib         , xic
      real*8    te         , wt_i
c-----------------------------------------------------------------------
      character seq*2      , esym*2     , ctype*2    , xfesym*2
c-----------------------------------------------------------------------
      logical   lmet_f     , lmet_i     , lcstrg_f   , lcstrg_i
      logical   lgrid
c-----------------------------------------------------------------------
      integer   ia_f(ndlev)
      integer   isa_f(ndlev)    , ila_f(ndlev)
      integer   ia_i(ndlev)
      integer   isa_i(ndlev)    , ila_i(ndlev)
      integer   nte_ion(ndmet)  , nte_exc(ndmet)     , nf_a(ndmet)
      integer   indf_a(ndmet,ndlev)
      integer   imeta_i(ndmet)  , imeta_f(ndmet)
      integer   is_q_ion(ndmet,ndlev,ndtem)
      integer   is_q_exc(ndmet,ndlev,ndtem)
      integer   is_qtot(ndmet,ndmet,ndtem)
c-----------------------------------------------------------------------
      real*8    xja_f(ndlev)    , wa_f(ndlev)
      real*8    xja_i(ndlev)    , wa_i(ndlev)
      real*8    tea_ion(ndmet,ndtem)  , qred_ion(ndmet,ndlev,ndtem)
      real*8    tea_exc(ndmet,ndtem)  , qred_exc(ndmet,ndlev,ndtem)
      real*8    yld_a(ndmet,ndlev,ndlev)
      real*8    q_ion(ndmet,ndlev,ndtem)
      real*8    q_exc(ndmet,ndlev,ndtem)
      real*8    qtot(ndmet,ndmet,ndtem)
c-----------------------------------------------------------------------
      character code_f(ndlev)*1 , cstrga_f(ndlev)*(*)
      character code_i(ndlev)*1 , cstrga_i(ndlev)*(*)
      character ctext(ndtext)*80
c----------------------------------------------------------------------
      logical   l_ion(ndmet)     , l_aug(ndmet)      , l_exc(ndmet)
      logical   lqred_ion(ndmet,ndlev)
      logical   lqred_exc(ndmet,ndlev)
      logical   lyld_a(ndmet,ndlev)
c----------------------------------------------------------------------
c----------------------------------------------------------------------
c  verify common temperature grid otherwise fault
c----------------------------------------------------------------------

      if (.not.lmet_i) then
          nmet_i = 1
          imeta_i(1)=1
      endif

      if (.not.lmet_f) then
          nmet_f = 1
          imeta_f(1)=1
      endif

      lgrid = .true.
      do im=1,nmet_i
        if (nte_ion(1).eq.nte_ion(im)) then
            do it=1,nte_ion(1)
              if(tea_ion(1,it).ne.tea_ion(im,it))then
                  lgrid=.false.
              endif
            enddo
        else
            lgrid=.false.
        endif

        if(l_exc(im))then
            if (nte_ion(1).eq.nte_exc(im)) then
                do it=1,nte_ion(1)
                  if(tea_ion(1,it).ne.tea_exc(im,it))then
                      lgrid=.false.
                  endif
                enddo
            else
                lgrid=.false.
            endif
        endif
      enddo

      if(.not.lgrid) then
          write(i4unit(-1),1002)'no qtot returned as no common Te grid'
          write(i4unit(-1),1003)
      endif

c----------------------------------------------------------------------
c  convert direct reduced rates to standard (scaled) rate coefficients
c----------------------------------------------------------------------
      do im_i =1,nmet_i
        do im_f=1,nmet_f
          do it=1,nte_ion(im_i)
            qtot(im_i,im_f,it)=0.0d0
            is_qtot(im_i,im_f,it)=0
          enddo
        enddo
      enddo

      do im_i =1,nmet_i
        do it=1,nte_ion(im_i)
          te=tea_ion(im_i,it)/ev_k
          i=imeta_i(im_i)
          do j=1,nlvl_f
            xia= -(bwno_i -wa_i(i)+ wa_f(j))/(const1*te)
            ia=int(xia)
            a=base_e**(xia-ia)

            ib=0
            b=qred_ion(im_i,j,it)
            call xxbass( b , ib , c , ic , base_e )

            a=a*c
            ia=ia+ic
            call xxbasr(a,ia,base_e,q_ion(im_i,j,it),
     &                  is_q_ion(im_i,j,it), base_10)

          enddo
        enddo
      enddo

c----------------------------------------------------------------------
c  If only one final metastable, accumulate all ionisations to
c  excited final states in the ground state total.
c  If more than one final metastable, put only the direct ionisation
c  to that metastable in the total.
c----------------------------------------------------------------------
      if(.not.lgrid) go to 20

      do im_i =1,nmet_i
        do it=1,nte_ion(im_i)
          if(nmet_f.eq.1) then
              qtot(im_i,1,it)=1.0d0
              is_qtot(im_i,1,it) = -74
              do j=1,nlvl_f
                a=qtot(im_i,1,it)
                ia=is_qtot(im_i,1,it)
                call xxbasa( a , ia ,
     &                       q_ion(im_i,j,it) , is_q_ion(im_i,j,it) ,
     &                       base_10          ,
     &                       qtot(im_i,1,it)  , is_qtot(im_i,1,it)
     &                     )
              enddo
          elseif(nmet_f.gt.1) then
              do j=1,nlvl_f
                do im_f = 1,nmet_f
                  if(imeta_f(im_i).eq.j) then
                       qtot(im_i,im_f,it)=q_ion(im_i,j,it)
                       is_qtot(im_i,im_f,it)=is_q_ion(im_i,j,it)
                  endif
                enddo
              enddo
          endif
        enddo
      enddo

c----------------------------------------------------------------------
c  convert excit. reduced rates to standard (scaled) rate coefficients
c----------------------------------------------------------------------
   20 do im_i =1,nmet_i
        do it=1,nte_ion(im_i)
          te=tea_exc(im_i,it)/ev_k
          i=imeta_i(im_i)
          wt_i= 2.d0*xja_i(i)+1.d0
          do j=2,nlvl_i
            xia= -(wa_i(j)-wa_i(i))/(const1*te)
            ia=int(xia)
            a=base_e**(xia-ia)
            a=a*const2*sqrt(ryd_ev/te)/wt_i
            ib=0
            b=qred_exc(im_i,j,it)
            call xxbass( b , ib , c, ic , base_e )
            a=a*c
            ia=ia+ic
            call xxbasr(a,ia,base_e,q_exc(im_i,j,it),
     &                  is_q_exc(im_i,j,it), base_10)
          enddo
        enddo
      enddo

c----------------------------------------------------------------------
c  Accumulate in the totals for the different metastables, using the
c  Auger branching ratio.
c----------------------------------------------------------------------
      if(.not.lgrid) go to 40

      do im_i =1,nmet_i
        do it=1,nte_ion(im_i)
          do j=2,nlvl_i
            if(lyld_a(im_i,j)) then
                 do k = 1,nf_a(im_i)
                   do im_f=1,nmet_f
                       if(indf_a(im_i,k).eq.imeta_f(im_f))then
                           a=yld_a(im_i,j,k)*q_exc(im_i,j,it)
                           ia=is_q_exc(im_i,j,it)
                           b=qtot(im_i,im_f,it)
                           ib=is_qtot(im_i,im_f,it)
                           call xxbasa( a , ia , b, ib, base_10 ,
     &                                  qtot(im_i,im_f,it)      ,
     &                                  is_qtot(im_i,im_f,it)
     &                                )
                       endif
                   enddo
                 enddo
            endif
          enddo
        enddo
      enddo

   40 return

c
c-----------------------------------------------------------------------
c
 1002 format(1x,30('*'),' xxtotl_23 warning ',29('*')//
     &       1x,'fault in input data file: ',a,i4,a)
 1003 format(/1x,29('*'),' program continues  ',29('*'))
c
c-----------------------------------------------------------------------
c
      end
