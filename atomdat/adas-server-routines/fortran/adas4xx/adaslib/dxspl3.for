CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adaslib/dxspl3.for,v 1.2 2004/07/06 13:34:05 whitefor Exp $ Date $Date: 2004/07/06 13:34:05 $
CX
      SUBROUTINE DXSPL3( ISWIT  , LSWIT , IZ1   ,
     &                   NDOUT  , NTOUT ,
     &                            NTIN  ,
     &                   MAXD   , ITE   ,
     &                   MAXT   , TIN   , TOUT  , EIAVAL ,
     &                   TINTRP ,
     &                   ATTY
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DXSPL3 *********************
C
C  PURPOSE: PERFORMS THE THIRD PART OF A THREE WAY SPLINE ON INPUT DATA.
C           GENERATES  A  TABLE  OF  LOG10( COEFFTS./POWERS )  COVERING
C           'MAXT' TEMPERATURES AND 'MAXD' DENSITIES FOR THE ELEMENT
C           RECOMBINING ION CHARGE GIVEN BY 'IZ1'.
C
C  CALLING PROGRAM: D1SPLN/D4DATA
C  DATA:
C
C           THE  SOURCE  DATA ORIGINATES AS MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             6. JETUID.PRB<YR>.DATA
C             7. JETUID.PRC<YR>.DATA
C             8. JETUID.PLT<YR>.DATA
C             9. JETUID.PLS<YR>.DATA
C            10. JETUID.MET<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C
C           THE PARTICULAR TYPE OPENED (1-10) IS SELECTED BY 'ISWIT'
C           IT IS PASSED IN A MODIFIED FORM AFTER PROCESSING BY
C           DXSPL1 AND DXSPL2.
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ISWIT   = DATA TYPE SELECTOR (1 -> 8)
C  INPUT : (L*4)  LSWIT   = .TRUE.  => IONISATION POTENTIALS PRESENT
C                           .FALSE. => IONS. POTENTIALS NOT PRESENT
C  INPUT : (I*4)  IZ1     = OUTPUT - ELEMENT RECOMBINING ION CHARGE
C
C  INPUT : (I*4)  NDOUT   = OUTPUT - MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  NTOUT   = OUTPUT - MAXIMUM NUMBER OF TEMPERATURES
C
C  INPUT : (I*4)  NTIN    = INPUT  - MAXIMUM NUMBER OF TEMPERATURES
C
C  INPUT : (I*4)  MAXD    = INPUT  - NUMBER OF REDUCED DENSITIES
C  INPUT : (I*4)  ITE     = INPUT  - NUMBER OF REDUCED TEMPERATURES
C
C  INPUT : (I*4)  MAXT    = OUTPUT - NUMBER OF REDUCED TEMPERATURES
C                                                           ( <= NTOUT )
C  INPUT : (R*8)  TIN()   = INPUT  - SET OF 'ITE' REDUCED ELECTRON TEMPS
C  INPUT : (R*8)  TOUT()  = OUTPUT - SET OF 'MAXT' ELECTRON TEMPERATURES
C                                    (UNITS: KELVIN).
C  INPUT : (R*8)  EIAVAL  = IONISATION POTENTIAL (RYDBERGS) FOR
C                           THE ION CHARGE GIVEN BY 'IZ1'.
C
C  OUTPUT: (L*4)  TINTRP() = .TRUE.  => 'ATTY(,)' VALUE FOR TEMPERATURE
C                                       INDEX INTERPOLATED.
C                          = .FALSE. => 'ATTY(,)' VALUE FOR TEMPERATURE
C                                       INDEX EXTRAPOLATED.
C                            1ST DIMENSION: TEMPERATURE INDEX
C
C  IN/OUT: (R*8)  ATTY(,) = WORKING SPACE FOR 3-WAY SPLINE ITERPOLATION
C                           (STORES LOG10 (INTERPOLATED VALUES))
C                           INPUT 'ATTY' VALUES ARE ASSIGNED TO 'YIN' &
C                           THEN 'YOUT' VALUES ARE ASSIGNED TO 'ATTY'.
C                           1ST DIMENSION: TEMPERATURE
C                           2ND DIMENSION: DENSITY
C
C          (I*4)  NTDIM1  = PARAMETER = MUST BE EQUAL TO OR GREATER THAN
C                           THE MAXIMUM NUMBER OF INPUT TEMPERATURES.
C          (I*4)  NTDIM2  = PARAMETER = MUST BE EQUAL TO OR GREATER THAN
C                           THE MAXIMUM NUMBER OF OUTPUT TEMPERATURES.
C
C          (R*8)  EIACON  = PARAMETER = -68570.7
C
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = 0, 1, 2, 3)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  Z1R2    = 1.0 / (IZ1**2)
C          (R*8)  SCON1   = SCALING CONSTANT
C          (R*8)  SCON2   = SCALING CONSTANT
C          (R*8)  XOUT()  ='TOUT()' CONVERTED TO REDUCED TEMPERATURE
C          (R*8)  YIN()   ='ATTY(,)' AT FIXED DENSITY -
C                                               DIMENSION => TEMPERATURE
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C          (R*8)  YOUT()  = SPLINE INTERPOLATED 'ATTY(,,)' VALUES  FOR
C                           REDUCED TEMPERATURE  EQUAL TO 'XOUT()' AT A
C                           FIXED DENSITY.
C
C NOTE:
C                         SPLINE IS CARRIED OUT ON:
C                   'ATTY(,,)' VALUES AT FIXED DENSITY
C                                  VERSUS
C                          LOG10( REDUCED DENSITY )
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (WITH EXTRAP. INFO)
C          I4UNIT     ADAS      UNIT NUMBER FOR WARNING MESSAGES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE  :  13/06/91 - PE BRIDEN: ADAS91 VERSION OF 'D4SPL3'
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C VERSION: 1.2                          DATE: 25-10-97
C MODIFIED: LORNE HORTON (JET)
C               - ADDED FURTHER CHECKS ON LOW T EXTRAPOLATIONS
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NTDIM1               , NTDIM2
C-----------------------------------------------------------------------
      REAL*8     EIACON
C-----------------------------------------------------------------------
      PARAMETER( NTDIM1 = 24          , NTDIM2 = 55    )
C-----------------------------------------------------------------------
      PARAMETER( EIACON =-6.85707D+04 )
C-----------------------------------------------------------------------
      INTEGER    ISWIT                , IZ1            ,
     &           NDOUT                , NTOUT          ,
     &                                  NTIN           ,
     &           MAXD                 , ITE            ,
     &           MAXT
      INTEGER    IOPT                 , ID             , IT
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      REAL*8     EIAVAL
      REAL*8     Z1R2                 ,
     &           SCON1                , SCON2
C-----------------------------------------------------------------------
      LOGICAL    LSWIT                , LSETX
C-----------------------------------------------------------------------
      REAL*8     TOUT(NTOUT)          , TIN(NTIN)
      REAL*8     YIN(NTDIM1)          , DF(NTDIM1)     ,
     &           XOUT(NTDIM2)         , YOUT(NTDIM2)
      REAL*8     ATTY(NTOUT,NDOUT)
C-----------------------------------------------------------------------
      LOGICAL    TINTRP(NTOUT)
C-----------------------------------------------------------------------
      REAL*8     DLOG10
      INTRINSIC  DLOG10
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NTDIM1.LT.NTIN )
     &   STOP ' DXSPL3 ERROR: NTDIM1 < NTIN  - INCREASE NTDIM1'
      IF (NTDIM2.LT.NTOUT)
     &   STOP ' DXSPL3 ERROR: NTDIM2 < NTOUT - INCREASE NTDIM2'
C
C-----------------------------------------------------------------------
C SET UP SCALING CONSTANTS & SPLINE BOUNDARY CONDITIONS: DEPEND ON ISWIT
C-----------------------------------------------------------------------
C
         IF (ISWIT.EQ.1) THEN
            IOPT  = 1
            SCON1 = DLOG10( DBLE(IZ1) )
            SCON2 = 0.0
         ELSEIF (ISWIT.EQ.2) THEN
            IF(LSWIT)THEN
                IOPT  = 0
                SCON1 = -3.0 * DLOG10( DBLE(IZ1) )
                SCON2 = EIACON * EIAVAL
            ELSE
                IOPT  = 0
                SCON1 = -3.0 * DLOG10( DBLE(IZ1) )
                SCON2 = 0.0
            ENDIF
         ELSEIF (ISWIT.EQ.3) THEN
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ELSEIF (ISWIT.EQ.4) THEN
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ELSEIF (ISWIT.EQ.5) THEN
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ELSEIF (ISWIT.EQ.6) THEN
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ELSEIF (ISWIT.EQ.7) THEN
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ELSEIF (ISWIT.EQ.8) THEN
            IF(LSWIT)THEN
                IOPT  = 0
                SCON1 = -3.0 * DLOG10( DBLE(IZ1) )
                SCON2 = EIACON * EIAVAL
            ELSE
                IOPT  = 0
                SCON1 = -3.0 * DLOG10( DBLE(IZ1) )
                SCON2 = 0.0
            ENDIF
         ELSEIF (ISWIT.EQ.9) THEN
            IF(LSWIT)THEN
                IOPT  = 0
                SCON1 = -3.0 * DLOG10( DBLE(IZ1) )
                SCON2 = EIACON * EIAVAL
            ELSE
                IOPT  = 0
                SCON1 = -3.0 * DLOG10( DBLE(IZ1) )
                SCON2 = 0.0
            ENDIF
         ELSEIF (ISWIT.EQ.10) THEN
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ELSE
            IOPT  = 0
            SCON1 = 0.0
            SCON2 = 0.0
         ENDIF
C-----------------------------------------------------------------------
      LSETX=.TRUE.
C
C-----------------------------------------------------------------------
C CONVERT TEMPERATURES 'TOUT()' INTO REDUCED TEMPERATURES 'XOUT()'
C-----------------------------------------------------------------------
C
      Z1R2 = 1.0 / DBLE(IZ1**2)
C
         DO 1 IT=1,MAXT
            XOUT(IT) = TOUT(IT) * Z1R2
    1    CONTINUE
C
C-----------------------------------------------------------------------
C PERFORM SPLINE INTERPOLATION/EXTRAPOLATION FOR EACH DENSITY VALUE.
C-----------------------------------------------------------------------
C
         DO 2 ID=1,MAXD
C
               DO 3 IT=1,ITE
                  YIN(IT) = ATTY(IT,ID)
    3          CONTINUE
C
    4       CALL XXSPLE( LSETX , IOPT   , DLOG10 ,
     &                   ITE   , TIN    , YIN    ,
     &                   MAXT  , XOUT   , YOUT   ,
     &                   DF    , TINTRP
     &                 )
C
C-----------------------------------------------------------------------
C CHECK THAT LOW TEMPERATURE EXTRAPOLATIONS ARE IN ACCEPTABLE
C RANGE:
C-----------------------------------------------------------------------
C  ACD:
               IF ( ISWIT.EQ.1 ) THEN
                  IF ( DF(1).GT.-0.5 ) THEN
                    IOPT = 3
                    WRITE(I4UNIT(-1),1000)
                    WRITE(I4UNIT(-1),1040) IZ1
                    GOTO 4
                  ELSE IF ( DF(1).LT.-4.5) THEN
                    IOPT = 5
                    WRITE(I4UNIT(-1),1010)
                    WRITE(I4UNIT(-1),1040) IZ1
                    GOTO 4
                  ENDIF
               ENDIF
C  PRB:
               IF ( ISWIT.EQ.4 ) THEN
                  IF ( DF(1).GT.+0.5 ) THEN
                    IOPT = 6
                    WRITE(I4UNIT(-1),1020)
                    WRITE(I4UNIT(-1),1040) IZ1
                    GOTO 4
                  ELSE IF ( DF(1).LT.-3.5) THEN
                    IOPT = 7
                    WRITE(I4UNIT(-1),1030)
                    WRITE(I4UNIT(-1),1040) IZ1
                    GOTO 4
                  ENDIF
               ENDIF
C
C-----------------------------------------------------------------------
C SCALE INTERPOLATED VALUE ACCORDING TO DATA TYPE
C-----------------------------------------------------------------------
C
               IF (ISWIT.EQ.1) THEN
C
                  DO 5 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT) + SCON1
    5             CONTINUE
C
               ELSEIF (ISWIT.EQ.2) THEN
C
                  DO 6 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT) + SCON1 + ( SCON2/TOUT(IT) )
    6             CONTINUE
C
               ELSEIF (ISWIT.EQ.3) THEN
C
                  DO 7 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
    7             CONTINUE
C
               ELSEIF (ISWIT.EQ.4) THEN
C
                  DO 8 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
    8             CONTINUE
C
               ELSEIF (ISWIT.EQ.5) THEN
C
                  DO 9 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
    9             CONTINUE
C
               ELSEIF (ISWIT.EQ.6) THEN
C
                  DO 10 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
   10             CONTINUE
C
               ELSEIF (ISWIT.EQ.7) THEN
C
                  DO 11 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
   11             CONTINUE
C
               ELSEIF (ISWIT.EQ.8) THEN
C
                  DO 12 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT) + SCON1 + ( SCON2/TOUT(IT) )
   12             CONTINUE
C
               ELSEIF (ISWIT.EQ.9) THEN
C
                  DO 13 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT) + SCON1 + ( SCON2/TOUT(IT) )
   13             CONTINUE
C
               ELSEIF (ISWIT.EQ.10) THEN
C
                  DO 14 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
   14             CONTINUE
C
               ELSE
C
                  DO 15 IT=1,MAXT
                     ATTY(IT,ID) = YOUT(IT)
   15             CONTINUE
C
               ENDIF
C-----------------------------------------------------------------------
    2    CONTINUE
C-----------------------------------------------------------------------
 1000 FORMAT(' *** WARNING:  FORCING LOW TE SLOPE OF ACD TO -1/2 ***')
 1010 FORMAT(' *** WARNING:  FORCING LOW TE SLOPE OF ACD TO -9/2 ***')
 1020 FORMAT(' *** WARNING:  FORCING LOW TE SLOPE OF PRB TO +1/2 ***')
 1030 FORMAT(' *** WARNING:  FORCING LOW TE SLOPE OF PRB TO -7/2 ***')
 1040 FORMAT('               IZ1 = ', I2)
C
      RETURN
      END
