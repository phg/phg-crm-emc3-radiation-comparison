CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/d1setp.for,v 1.2 2004/07/06 13:12:13 whitefor Exp $ Date $Date: 2004/07/06 13:12:13 $
CX
      SUBROUTINE D1SETP( LPARTL , LPOWER , NEL    ,
     &                   NDDEN  , IDE    , DENSR  ,
     &                   NDMET  , IME    , IMETR  , CSTRGA ,
     &                   NPRNT  , IPRNT  , IPSYS, IZE,  ZIPT, NDZ1V 
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D1SETP *********************
C
C  PURPOSE:  PIPE COMMS WITH IDL d1setp.pro    
C
C  CALLING PROGRAM: ADAS401
C
C  DATA:
C           DATA, EXCEPT 'NEL', IS OBTAINED VIA SUBROUTINE 'XXIN68'
C           AND USED ONLY IF ('LPOWER' .AND. 'LPARTL')
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LPARTL  = .TRUE.  => PARTIAL  CONDENSED FILE READ IN
C                           .FALSE. => STANDARD CONDENSED FILE READ IN
C  INPUT : (L*4)  LPOWER  = .TRUE.  => LINE POWER  FILE READ IN
C                           .FALSE. => COEFFICIENT FILE READ IN
C  INPUT : (I*4)  NEL     = NO. OF ELECTRONS IN SELECTED ISO-ELECTRONIC
C                           SEQUENCE.
C
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF INPUT FILE DENSITIES
C                           ALLOWED (=8).
C  INPUT : (I*4)  IDE     = NO. OF DENSITY  VALUES  CONTAINED IN THE
C                           INPUT MASTER CONDENSED/METASTABLE FILE.
C  INPUT : (R*8)  DENSR() = SET OF 'IDE' ELECTRON TEMPERATURES READ FROM
C                           INPUT FILE.
C
C  ** THE FOLLOWING VARIABLES EXIST ONLY FOR PARTIAL LINE POWER FILES **
C
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  IME     = NO. OF METASTABLE LEVELS CONTAINED IN THE
C                           INPUT MASTER CONDENSED/METASTABLE FILE.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (I*4)  IMETR() =THE ORIGINAL COPDAT INDEX FOR EACH METASTABLE
C                           LEVEL. DIMENSION: METASTABLE LEVEL INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (C*12) CSTRGA()=THE DESIGNATION OF EACH METASTABLE LEVEL.
C                           DIMENSION: METASTABLE LEVEL INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  INPUT : (I*4)  NPRNT   = NUMBER OF PARENTS CONTAINED IN THE INPUT
C                           MASTER CONDENSED/METASTABLE FILE.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C                           (NOTE: THE NUMBER OF PARENTS CANNOT EXCEED
C                                  THE NUMBER OF METASTABLE LEVELS)
C  INPUT : (I*4)  IPRNT() = THE PARENT INDEX FOR INPUT PARENT.
C                           DIMENSION: PARENT/(METASTABLE LEVEL) INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (I*4)  IPSYS() = THE SPIN SYSTEM REFERENCE FOR EACH INPUT
C                           PARENT.
C                           DIMENSION: PARENT/(METASTABLE LEVEL) INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  ** END OF VARIABLES WHICH  EXIST ONLY FOR PARTIAL LINE POWER FILES **
C
C
CX         (C*45) STRGA   = STORES METASTABLE DESIGNATION
CX         (C*10) STRGB   = STORES DENSITY VALUE
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    17/06/91
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2				DATE: 24-10-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C		- REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   NEL            , NDDEN         , IDE         ,
     &                           NDMET         , IME         , NPRNT
      INTEGER   IZE            , I
C-----------------------------------------------------------------------
      LOGICAL   LPARTL         , LPOWER
C-----------------------------------------------------------------------
      INTEGER    IMETR(IME)     , IPRNT(NPRNT)  , IPSYS(NPRNT)
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER  (PIPEOU=6       , PIPEIN=5)
      INTEGER    NDZ1V
C-----------------------------------------------------------------------
      REAL*8    DENSR(IDE)
      REAL*8    ZIPT(NDZ1V)
C-----------------------------------------------------------------------
      CHARACTER CSTRGA(IME)*12
C-----------------------------------------------------------------------
C- - - -- - - - -  - - - - - - - - - -  - - - - - - - - - - - - - - - - -
C  WRITE ANY REQUIRED INFORMATION TO IDL
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      WRITE(PIPEOU,*) IZE
      CALL XXFLSH(PIPEOU)
      DO 12, I=1, IZE
	   WRITE(PIPEOU,*) ZIPT(I)
12    CONTINUE
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(I3,8X,1A12,2(10X,'*'))
 1002 FORMAT(I3,8X,1A12,2( 8X,I3 ))
 1003 FORMAT(I2.2)
 1004 FORMAT(D10.3)
 1005 FORMAT(I1)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
