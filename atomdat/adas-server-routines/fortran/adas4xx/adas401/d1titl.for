CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/d1titl.for,v 1.1 2004/07/06 13:12:53 whitefor Exp $ Date $Date: 2004/07/06 13:12:53 $
CX
      SUBROUTINE D1TITL( IMSEL  , DSFULL ,
     &                   IPRNT  , IPSYS  ,
     &                   CELEM  , CYEAR  , CLASS  ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D1TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA.
C
C  CALLING PROGRAM: ADAS401
C
C  SUBROUTINE:
C
C
C  INPUT : (I*4)  IMSEL    = INDEX OF SELECTED METASTABLE STATE
C                            (ONLY USED IF (LPARTL.AND.LPOWER) -
C                             EQUALS ONE OTHERWISE).
C  INPUT : (C*80) DSFULL   = FULL MVS INPUT DATA SET NAME
C
C  INPUT : (I*4)  IPRNT   = PARENT INDEX FOR SELECTED 'METASTABLE' INDEX
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (I*4)  IPSYS   = SPIN SYSTEM  FOR SELECTED 'METASTABLE' INDEX
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  INPUT : (C*2)  CELEM   = ISO-ELECTRONIC SEQUENCE ELEMENT NAME
C  INPUT : (C*2)  CYEAR   = YEAR OF INPUT DATA (2 CHARACTER ABBREV.)
C
C  INPUT : (C*3)  CLASS   = CLASS OF DATA SET BEING ANALYSED.
C                           ('ACD','SCD','CCD','PRB','PRC','PLT','PLS')
C
C  OUTPUT: (C*80) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*1)  CPRNT    = 'IPRNT' AS CHARACTER
C          (C*1)  CPSYS    = 'IPSYS' AS CHARACTER
C          (C*2)  CMSEL    = 'IMSEL' AS CHARACTER
C
C ROUTINES: NONE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    17/06/91
C
C UNIX PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IMSEL           , IPRNT          , IPSYS
C-----------------------------------------------------------------------
      CHARACTER  DSFULL*80       ,
     &           CELEM*2         , CYEAR*2        , CLASS*3    ,
     &           TITLX*80
      CHARACTER  CMSEL*2         , CPRNT*1        , CPSYS*1
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      WRITE(CMSEL,1000) IMSEL
      WRITE(CPRNT,1001) IPRNT
      WRITE(CPSYS,1001) IPSYS
C
      TITLX(1:80) = 'FILE: ' // DSFULL(2:30) // ' BLK=' // CMSEL //
     &              '; <' // CELEM // '> CLASS=' // CLASS //
     &              ' YEAR=' // CYEAR // ' PRT=' // CPRNT //
     &              ' SYS=' // CPSYS // ' '
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(I1)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
