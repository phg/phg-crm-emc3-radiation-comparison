CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/d1ispf.for,v 1.3 2004/07/06 13:11:34 whitefor Exp $ Date $Date: 2004/07/06 13:11:34 $
CX
      SUBROUTINE D1ISPF( IPAN   , LPEND  ,
     &                   LPARTL , LPOWER , NEL   , NDDEN ,
     &                   NDTMAX , NDTIN  , TVALS , DENSR ,
     &                   TITLE  , GTIT1  ,
     &                   IZ1    , IMSEL  , IDSEL ,
     &                   ITTYP  , ITDVAL , TIN   , DIN   ,
     &                   LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1   , YU1   ,
     &			 TR     , ITE    , L3    , IDE    )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D1ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL              
C
C  CALLING PROGRAM: ADAS401
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (L*4)   LPARTL   = .TRUE.  => PARTIAL  CONDENSED FILE READ IN
C                             .FALSE. => STANDARD CONDENSED FILE READ IN
C  INPUT : (L*4)   LPOWER   = .TRUE.  => LINE POWER  FILE READ IN
C                             .FALSE. => COEFFICIENT FILE READ IN
C  INPUT : (I*4)   NEL      = NUMBER  OF  ELECTRONS  IN  SELECTED  ISO-
C                             ELECTRONIC SEQUENCE.
C
C  INPUT : (I*4)   NDTMAX   = MAX. NUMBER OF TEMP./DENSITY PAIRS ALLOWED
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (C*40)  GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C
C  OUTPUT: (I*4)   IZ1      = RECOMBINING  ION  CHARGE   SELECTED   FOR
C                             ANALYSIS = ION CHARGE + 1.
C                            ( RECOMBINING  ION CHARGE Z1 = 'IZ1'
C                              (RECOMBINED) ION CHARGE Z  = 'IZ1' - 1
C                              NUCLEAR CHARGE Z0  = 'IZ1' + 'NEL' - 1 )
C  OUTPUT: (I*4)   IMSEL    = SELECTED METASTABLE INDEX FOR ANALYSIS
C                             (ONLY REQUIRED IF (LPARTL.AND.LPOWER) -
C                              SET EQUAL TO ONE OTHERWISE).
C  OUTPUT: (I*4)   IDSEL    = INDEX OF DATA-SET READ DENSITY SELECTED
C                             FOR GRAPHING.
C
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURE UNITS = KELVIN
C                           = 2 => INPUT TEMPERATURE UNITS = EV
C                           = 3 => INPUT TEMPERATURE UNITS = REDUCED
C  OUTPUT: (I*4)   ITDVAL   = NUMBER OF INPUT TEMP/DENSITY PAIRS (1->40)
C  OUTPUT: (R*8)   TIN()    = ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  OUTPUT: (R*8)   DIN()    = ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT.
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2516
C
C DATE:    17/06/91
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - MINOR SYNTAX ERROR CORRECTION
C
C VERSION: 1.3                          DATE: 24-10-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED SUBROUTINE STATEMENT SYNTAX AND
C   		  REMOVED SOME SUPERFLUOUS VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IPAN           , NEL           , NDTMAX      , IZ1    ,
     &           IMSEL          , IDSEL         , ITTYP       , ITDVAL
      INTEGER    NDTIN          , ITE           , NDDEN
      INTEGER    J    		, L3            , IDE
C-----------------------------------------------------------------------
      REAL*8     TOLVAL         ,
     &           XL1            , XU1           , YL1         , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND          , LPARTL        , LPOWER      ,
     &           LFSEL          , LOSEL         , LGRD1       , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       , GTIT1*40
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTMAX)                    , DIN(NDTMAX)
      REAL*8     TVALS(NDTIN,3)  
      REAL*8     DENSR(NDDEN)
      REAL*8     TR(NDTIN)
C-----------------------------------------------------------------------
      INTEGER    LOGIC
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER (PIPEOU=6       , PIPEIN=5)
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) NDTMAX
      WRITE(PIPEOU,*) NDTIN
      WRITE(PIPEOU,*) NDDEN
      WRITE(PIPEOU,*) ITE
      WRITE(PIPEOU,*) IDE
      CALL XXFLSH(PIPEOU)
      DO 3, J=1, NDTIN
          TVALS(J,3) = TR(J)
          WRITE(PIPEOU,*) TVALS(J,3)
          CALL XXFLSH(PIPEOU)
3     CONTINUE

      DO 4, J=1, NDDEN
          WRITE(PIPEOU,*) DENSR(J)
4     CONTINUE
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C     READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C

      READ(PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPEND = .TRUE.
      ELSE
          READ(PIPEIN, *) ITTYP
          READ(PIPEIN, *) IZ1
          READ(PIPEIN, *) ITDVAL
          READ(PIPEIN, *) LOGIC
          IF (LOGIC.EQ.1) THEN
              LOSEL = .TRUE.
              DO 5, J=1, ITDVAL
                  READ(PIPEIN, *) TIN(J)
5             CONTINUE
              DO 6, J=1, ITDVAL
                  READ(PIPEIN, *) DIN(J)
6             CONTINUE
              READ(PIPEIN, *) LOGIC
              IF (LOGIC.EQ.1) THEN
                  LFSEL = .TRUE.
                  READ(PIPEIN,*) TOLVAL
                  TOLVAL = TOLVAL /100.0
              ELSE
                  LFSEL = .FALSE.
              ENDIF
          ELSE
              LOSEL = .FALSE.
          ENDIF
          LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
