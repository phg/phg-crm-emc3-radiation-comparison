CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/d1spf0.for,v 1.1 2004/07/06 13:12:18 whitefor Exp $ Date $Date: 2004/07/06 13:12:18 $
CX
      SUBROUTINE D1SPF0( REP    , DSFULL,   ISWIT ,   IZ1,   CYEAR,
     &			 CELEM  , CLASS, CHEAD)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D1SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL - GETS NAME OF DATA FILE
C
C  CALLING PROGRAM: ADAS401
C
C  PREVIOUSLY THE FORTRAN HANDLED ALL THE FILE SELECTION BUT THIS
C  IS NOW HANDLED PURELY BY THE IDL AND ALL RELEVANT DATA IS
C  SIMPLY PIPED INTO THIS FILE TO BE FED BACK TO THE MAIN ADAS401
C  PROGRAM
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C          (I*4)   ISWIT   = OPTION NUMBER FOR SELECTED DATA CLASS
C                            (SEE ADAS401 COMMENTS FOR MORE DETAILS)
C          (C*2)   CYEAR   = YEAR OF INPUT DATA (2 CHARACTER ABBREV.)
C          (C*2)   CELEM   = ISO-ELECTRONIC SEQUENCE ELEMENT NAME
C          (C*3)   CLASS   = CLASS OF DATA SET BEING ANALYSED.
C                           ('ACD','SCD','CCD','PRB','PRC','PLT','PLS')
C          (C*80)  CHEAD   = HEADER FOR CLASS BEING ANALYSED.
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)   IVALID   = FLAG FOR WHETHER FILE NAME IS VALID OR NOT
C                             0 = INVALID NAME
C                             1 = VALID NAME
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  TIM HAMMOND    (TESSELLA SUPPORT SERVICES PLC)
C
C VERSION: 1.1			
C MODIFIED: TIM HAMMOND		DATE:    10/08/95
C		- UNIX PORT
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSFULL*80
      CHARACTER   CYEAR*2	, CHEAD*80
      CHARACTER   CLASS*3       , CELEM*2
      INTEGER     ISWIT		, IZ1
      INTEGER     IVALID        , I4UNIT
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOUT
      PARAMETER(  PIPEIN=5      , PIPEOUT=6)
C-----------------------------------------------------------------------
C  READ BASIC OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      IF (REP.EQ.'NO') THEN
	  READ(PIPEIN, *) IVALID
	  IF (IVALID.EQ.1) THEN
              READ(PIPEIN,'(A)') DSFULL
              READ(PIPEIN,'(A)') CLASS
              READ(PIPEIN,'(A)') CHEAD
              READ(PIPEIN,'(A)') CYEAR
              READ(PIPEIN,'(A)') CELEM
          ELSE
	      WRITE(I4UNIT(-1),2000)
	      WRITE(I4UNIT(-1),2001)
          ENDIF
C
C-----------------------------------------------------------------------
C  READ FURTHER INFORMATION ON THE SELECTED DATA FILE FROM THE PIPE
C-----------------------------------------------------------------------
C
          READ(PIPEIN, *) ISWIT
      ENDIF

C
C-----------------------------------------------------------------------
2000  FORMAT(/1X,32('*'),' D1SPF0 ERROR ',32('*')//
     &        1X,24(' '),' INPUT FILE NAME IS INVALID')     
2001  FORMAT(/1X,28('*'),' END OF ERROR MESSAGE ',28('*'))
C-----------------------------------------------------------------------
C
      RETURN
      END
