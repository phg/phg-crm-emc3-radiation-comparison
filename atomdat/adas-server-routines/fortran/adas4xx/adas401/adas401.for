CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/adas401.for,v 1.9 2004/07/06 10:37:37 whitefor Exp $ Date $Date: 2004/07/06 10:37:37 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS401 **********************
C
C  ORIGINAL NAME: SPRTGRF6
C
C  PURPOSE:
C           TO GRAPH AND INTERPOLATE SELECTED DATA FROM MASTER CONDENSED
C           COLLISIONAL DIELECTRONIC FILES FOR EXAMINATION AND DISPLAY.
C
C  NOTE FOR UNIX-IDL PORT:
C
C           THE FOLLOWING DISCUSSION OF DATASET NAMES NO LONGER APPLIES
C           BUT IS LEFT IN FOR COMPLETENESS. THE ONLY ACCEPTABLE FORMS
C           FOR DATASET NAMES ARE NOW:
C
C                       <CLASS><YR>_<EL><IND>.dat
C           OR   <PREF>#<CLASS><YR>_<EL><IND>.dat
C
C	    HERE <CLASS> IS A 3 LETTER CODE CORRESPONDING TO ONE OF
C           THE OPTIONS GIVEN BELOW (EG. ACD, SCD ETC.)
C           <YR> IS A TWO DIGIT FORM OF THE YEAR
C           <EL> IS A ONE OR TWO CHARACTER ELEMENT SYMBOL
C           <IND> IS ABSENT FOR STANDARD DATA FILES BUT FOR PARTIAL
C           FILES IS THE PARENT AND SPIN SYSTEM INDICES
C           <PREF> IS AN OPTIONAL TWO CHARACTER PREFIX WHICH MUST BE
C           FOLLOWED BY A #
C           NOTE THAT THE _ AND .dat EXTENSION MUST BE THERE OR THE
C           DATA FILE WILL BE REJECTED.
C
C
CX DATA:    THE SOURCE DATA IS CONTAINED AS MEMBERS OF PARTITIONED DATA-
CX          SETS AS FOLLOWS ('ISWIT' NUMBER SHOWN IN BRACKETS):-
CX
CX                      (1) JETUID.ACD<YR>.DATA
CX                      (2) JETUID.SCD<YR>.DATA
CX                      (3) JETUID.CCD<YR>.DATA
CX                      (4) JETUID.PRB<YR>.DATA
CX                      (5) JETUID.PRC<YR>.DATA
CX                      (6) JETUID.QCD<YR>.DATA
CX                      (7) JETUID.XCD<YR>.DATA
CX                      (8) JETUID.PLT<YR>.DATA
CX                      (9) JETUID.PLS<YR>.DATA
CX
CX          WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
CX          (IF <YR> IS BLANK THEN THE CURRENT RECOMMENDED DATA SETS ARE
CX          USED ? - NOT IMPLEMENTED 25/02//91).
CX
CX          'JETUID' IS GIVEN BY 'UIDIN'
CX
CX          THE PARTICULAR TYPE OPENED IS SELECTED BY '??????'
CX
CX          MEMBERS ARE SELECTED THROUGH THE USE OF  THE ISO-ELECTRONIC
CX          SEQUENCE AND INDICES OF PARENTAGE AND SPIN SYSTEM.
CX
CX          MEMBERS OF THE PARTITIONED DATA SETS 1-5 ARE NAMED AS
CX
CX                           <SE><I><J>  - PARTIAL  FILES
CX                           <SE>        - STANDARD FILES
CX
CX          MEMBERS OF THE PARTITIONED DATA SETS 6-7 ARE NAMED AS
CX
CX                           <SE><I><J>  - PARTIAL  FILES
CX                           (THERE ARE NO STANDARD FILES)
CX
CX          MEMBERS OF THE PARTITIONED DATA SETS 8-9 ARE NAMED AS
CX
CX                           <SE><#><#>  - PARTIAL  FILES
CX                           <SE>        - STANDARD FILES
CX
CX          WHERE <SE> IS THE ONE OR TWO LETTER ION SEQUENCE CODE, <I>
CX          IS THE PARENT OR METASTABLE INDEX AND <J> IS THE SPIN SYSTEM 
CX          PARENT OR METASTABLE INDEX DEPENDING ON THE DATA SET CLASS.
CX          ## DENOTES THE BARE NUCLEUS STAGE.
CX
C           EACH MEMBER HAS A SET OF 8 REDUCED DENSITY DATA AS FOLLOWS:-
C
C                       (1) 1.00D-03 CM-3
C                       (2) 1.00D+00 CM-3
C                       (3) 1.00D+03 CM-3
C                       (4) 1.00D+06 CM-3
C                       (5) 1.00D+09 CM-3
C                       (6) 1.00D+12 CM-3
C                       (7) 1.00D+15 CM-3
C                       (8) 1.00D+18 CM-3
C
C
C  PROGRAM:
C
C          (I*4)  NDTIN   = PARAMETER = MAXIMUM NUMBER OF INPUT FILE
C                                       ELECTRON TEMPERATURES (=24)
C          (I*4)  NDDEN   = PARAMETER = MAXIMUM NUMBER OF INPUT FILE
C                                       ELECTRON DENSITIES (=24)
C          (I*4)  NDZ1V   = PARAMETER = MAXIMUM NUMBER OF INPUT FILE
C                                       CHARGE-STATES.
C          (I*4)  NDMET   = PARAMETER = MAXIMUM NUMBER OF INPUT FILE
C                                       METASTABLE INDEX BLOCKS.
C          (I*4)  NTDMAX  = PARAMETER = MAXIMUM NUMBER OF USER ENTERED
C                                       TEMPERATURES/DENSITY PAIRS.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX GENERATED POWERS/COEFFTS.
C                           /TEMPERATUREOR DENSITY PAIRS  FOR GRAPHICAL
C                           OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IPAN    = ISPF PANEL NUMBER (DUMMY IN 'D4CNTL')
C          (I*4)  ISWIT   = OPTION NUMBER FOR SELECTED DATA SET CLASS
C                           (SEE DATA SECTION ABOVE RELATING 'ISWIT'
C                            TO DATA SET CLASS).
C          (I*4)  NEL     = NUMBER OF ELECTRONS IN ISO-ELECTRONIC SEQ.
C          (I*4)  IDE     = INPUT FILE: NUMBER OF ELECTRON DENSITIES
C          (I*4)  ITE     = INPUT FILE: NUMBER OF ELECTRON TEMPERATURES
C          (I*4)  IZE     = INPUT FILE: NUMBER OF CHARGE-STATES
C          (I*4)  IME     = INPUT FILE: NUMBER OF METASTABLE LEVELS
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
CA                          OR IF 1 <= 'ISWIT' <= 7.
C          (I*4)  IMSEL   = SELECTED METASTABLE INDEX FOR ANALYSIS
C                            (ONLY REQUIRED IF (LPARTL.AND.LPOWER) -
C                             SET EQUAL TO ONE OTHERWISE).
C          (I*4)  IDSEL   = INDEX OF DATA-SET READ DENSITY SELECTED
C                            FOR GRAPHING.
C          (I*4)  IZ0     = NUCLEAR CHARGE OF ELEMENT UNDER ANALYSIS
C          (I*4)  IZ1     = RECOMBINING  ION  CHARGE   SELECTED   FOR
C                           ANALYSIS = ION CHARGE + 1.
C                           ( RECOMBINING  ION CHARGE Z1 = 'IZ1'
C                             (RECOMBINED) ION CHARGE Z  = 'IZ1' - 1
C                             NUCLEAR CHARGE Z0  = 'IZ1' + 'NEL' - 1 )
C          (I*4)  ITDVAL  = USER ENTERED: NUMBER OF TEMPERATURE/DENSITY
C                           PAIRS FOR SPLINING (1->40)
C          (I*4)  NPRNT   = INPUT FILE: NUMBER OF PARENTS
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
CA                          OR 1 <= 'ISWIT' <= 7.
C                           (NOTE: THE NUMBER OF PARENTS CANNOT EXCEED
C                                  THE NUMBER OF METASTABLE LEVELS)
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITTYP   = 1 => 'TIN()' TEMPERATURE UNITS = KELVIN
C                         = 2 => 'TIN()' TEMPERATURE UNITS = EV
C                         = 3 => 'TIN()' TEMPERATURE UNITS = REDUCED
CA         (I*4)  IM      = METASTABLE INDEX FOR LOOPS
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR CHARGE-STATE VALUES
C
C          (R*8)  R8TCON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  TOLVAL  = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                            (=0 IF MINIMAX FIT NOT SELECTED)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMINR   = GRAPH: LOWER LIMIT FOR TEMPERATURE (reduced)
C          (R*8)  XMAXR   = GRAPH: UPPER LIMIT FOR TEMPERATURE (reduced)
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR POWER/COEFFT.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR POWER/COEFFT.
C
C          (L*4)  LPEND   = .TRUE.  => END      PROGRAM EXECUTION
C                         = .FALSE. => CONTINUE PROGRAM EXECUTION
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT GENERATED
C                           .FALSE. => NO GHOST80 OUTPUT GENERATED
C          (L*4)  LPARTL  = .TRUE.  => INPUT DATA SET PARTIAL  TYPE
C                           .FALSE. => INPUT DATA SET STANDARD TYPE
C          (L*4)  LPOWER  = .TRUE.  => INPUT DATA SET - LINE POWERS
C                           .FALSE. => INPUT DATA SET - COEFFICIENTS
C          (L*4)  LERROR  = .TRUE.  => ERROR DETECTED IN READING FILE
C                           .FALSE. => NO ERROR DETECTED IN FILE
CX         (L*4)  LDSEL   = .TRUE.  => INPUT  DATA SET INFORMATION
CX                                     TO BE DISPLAYED BEFORE RUN.
CX                        = .FALSE. => INPUT  DATA SET INFORMATION
CX                                     NOT TO BE DISPLAYED BEFORE RUN.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                      FOR OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                         = .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
CA         (L*4)  LSWIT   = .TRUE.  => IONISATION POTENTIALS        
C                                      INCLUDED IN INPUT MASTER FILE.
CA                          .FALSE. => IONISATION POTENTIALS        
C                                      NOT INCLUDED IN INPUT MASTER FILE
C          (L*4)  LZRNG(1)= .TRUE.  => 'AOUT()' VALUES FOR CHARGE-
C                                      STATE 'IZ1' INTERPOLATED.
C                         = .FALSE. => 'AOUT()' VALUE  FOR CHARGE-
C                                       STATE 'IZ1' EXTRAPOLATED.
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C
C          (C*2)  XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)  ESYMB   = SYMBOL OF ELEMENT UNDER ANALYSIS
C          (C*2)  CELEM   = ISO-ELECTRONIC SEQUENCE ELEMENT NAME
C          (C*2)  CYEAR   = YEAR OF INPUT DATA (2 CHARACTER ABBREV.)
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                           'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*3)  CLASS   = CLASS OF DATA SET BEING ANALYSED.
C                           ('ACD','SCD','CCD','PRB','PRC','PLT','PLS')
C          (C*8)  CPROJ   = PROJECT UNDER WHICH INPUT DATA IS STORED
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) ENAME   = NAME   OF ELEMENT UNDER ANALYSIS
C          (C*40) TITLE   = IDENTIFYING TITLE FOR PROGRAM RUN (VIA ISPF)
C          (C*40) GTIT1   = IDENTIFYING TITLE FOR GRAPH PLOT  (VIA ISPF)
C          (C*80) DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                           (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C          (C*80) CHEAD   = HEADER FOR CLASS BEING ANALYSED.
C          (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED DENSITY and MET. INDEX
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C
C          (I*4)  IMETR() = INPUT FILE: ORIGINAL COPDAT INDEX FOR EACH
C                           EACH METASTABLE LEVEL.
C                           DIMENSION: METASTABLE LEVEL INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
CA                          OR 1 <= 'ISWIT' <= 7.
C          (I*4)  IPRNT() = INPUT FILE: PARENT INDEX FOR INPUT PARENT.
C                           DIMENSION: PARENT/(METASTABLE LEVEL) INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
CA                          * FOR 1 <= 'ISWIT' <= 7 ONLY 'IPRNT(1)' IS
C                           USED WHICH REPRESENTS PARENT INDEX FOR FILE
C          (I*4)  IPSYS() = INPUT FILE: SPIN SYSTEM REFERENCE FOR EACH
C                           INPUT PARENT.
C                           DIMENSION: PARENT/(METASTABLE LEVEL) INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
CA                          * FOR 1 <= 'ISWIT' <= 7 ONLY 'IPSYS(1)' IS
C                           USED WHICH REPRESENTS FILE SPIN-SYSTEM INDEX
C
CA         (R*8)  EIA()   = IONISATION POTENTIALS: ()=ION CHARGE
C                           UNITS: WAVE NUMBERS (CM-1)
C                           (= 0.0 IF NOT SET)
C          (R*8)  TR()    = INPUT FILE: SET OF 'ITE' ELECTRN TEMPERATURE
C                                       VALUES (UNITS: KELVIN/Z1**2).
C          (R*8)  DENSR() = INPUT FILE: SET OF 'IDE' ELECTRON DENSITIES
C                                       (UNITS: CM-3/Z1**7).
C          (R*8)  ZIPT()  = INPUT FILE: SET OF 'IZE' CHARGE-STATES.
C                                       (UNITS: ION CHARGE + 1 =
C                                               RECOMBINING ION CHARGE).
C          (R*8)  TUSER() = USER ENTERED: ELECTRON TEMPERATURES
C                           (UNITS: SEE 'ITTYP')
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  DUSER() = USER ENTERED: ELECTRON DENSITIES
C                           (UNITS: CM-3)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TKEL()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' TEMPERATURES. (KELVIN)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TEV()   = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' TEMPERATURES. (EV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TRED()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' TEMPERATURES. (K/Z1**2)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  DRED()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' ELECTRON DENSITIES (/Z1**7)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (R*8)  TREDM() = MINIMAX:
C                           SET OF 'NMX' TEMPERATURES.(K/Z1**2)
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C          (R*8)  AOUT()  = EXTRAPOLATED/INTERPOLATED  DATA  FOR  EACH
C                           USER ENTERED TEMPERATURE/DENSITY PAIR.
C                           (SAME DATA TYPE AS 'AIPT(,,,)').
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX.
C          (R*8)  AOUTM() = MINIMAX: POWER/COEFFTS. AT 'TREDM()'.
C                           (SAME DATA TYPE AS 'AIPT(,,,)').
C
C          (L*4)  LDRNG() = .TRUE.  => 'AOUT()' VALUE FOR DENSITY
C                                      INDEX INTERPOLATED.
C                         = .FALSE. => 'AOUT()' VALUE FOR DENSITY
C                                      INDEX EXTRAPOLATED.
C                          DIMENSION: DENSITY INDEX
C          (L*4)  LTRNG() = .TRUE.  => 'AOUT()' VALUE FOR TEMPERATURE
C                                      INDEX INTERPOLATED.
C                         = .FALSE. => 'AOUT()' VALUE FOR TEMPERATURE
C                                      INDEX EXTRAPOLATED.
C                          DIMENSION: TEMPERATURE INDEX
C
C          (C*12) CSTRGA()= INPUT FILE: METASTABLE LEVEL DESIGNATIONS
C                           DIMENSION: METASTABLE LEVEL INDEX.
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
CA                          OR 1 <= 'ISWIT' <= 7.
C
C          (R*8)  APLOT(,)= INPUT MASTER CONDENSED FILE -
C                           RELEVANT COEFFICIENT/POWER DATA FOR 'ISWIT'
C                           FOR FIXED DENSITY 'DENSR' AND META. INDEX
C                           1st DIMENSION: TEMPERATURE  INDEX ('TR()')
C                           2nd DIMENSION: CHARGE STATE INDEX ('ZIPT()')
C          (R*8)  TVALS(,)= INPUT DATA SET - ELECTRON TEMPERATURES
C                           1ST DIMENSION: ELECTRON TEMPERATURE INDEX
C                                          ( SEE 'TR(,)' )
C                           2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                          2 => EV      (ITTYP=2)
C                                          3 => REDUCED (ITTYP=3)
C
C          (R*8)  AIPT(,,,)= OPTION 1: RECOMBINATION       COEFFICIENTS
C                            OPTION 2: IONIZATION          COEFFICIENTS
C                            OPTION 3: CHARGE-EXCHANGE RECOMB. COEFFTS.
C                            OPTION 4: RECOMB.-BREM. POWER COEFFICIENTS
C                            OPTION 5: CHARGE-EXCHANGE RECOMB. PWR COEFF
CA                           OPTION 6: METASTABLE CROSS-COUPLING COEFF.
CA                           OPTION 7: PARENT META. CROSS-COUPLING COEFF
CA                           OPTION 8: TOTAL    LINE POWER COEFFICIENTS
CA                           OPTION 9: SPECIFIC LINE POWER COEFFICIENTS
CA                           OPTION10: METASTABLE STATE POPULATIONS
C                            1ST DIMENSION: ELECTRON DENSITY INDEX
C                                           ('DENSR()')
C                            2ND DIMENSION: ELECTRON TEMPERATURE INDEX
C                                           ('TR()')
C                            3RD DIMENSION: CHARGE STATE INDEX
C                                           ('ZIPT()')
C                            4TH DIMENSION: METASTABLE STATE INDEX
C                            (FOR OPTIONS 1 -> 5 AND ALL STANDARD FILES
C                             THIS DIMENSION ALWAYS EQUALS 1)
C
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   LOGIC  - USED TO PIPE LOGICAL VALUES
C
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D1SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          D1INFO     ADAS      FETCHES DATA SET INFO FROM FUNCTION POOL
C          D1SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          D1ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          D1SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION
C          D1TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          D1OUTG     ADAS      GRAPHICAL OUTPUT ROUTINR USING GHOST80
C          D1OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
CA         XXIN17     ADAS      READ IN PARTIAL MASTER CONDENSED FILE
CA                              ( 1 <= 'ISWIT' <= 7)
CA         XXIN80     ADAS      READ IN PARTIAL MASTER CONDENSED FILE
CA                              ( 8 <= 'ISWIT' <= 10)
C          XXINST     ADAS      READ IN STANDARD MASTER CONDENSED FILE
CA         XXCEIA     ADAS      CONVERT ION. POTENTIAL FROM WAVE NUMBERS
C                               TO RYDBERGS & EXTRAPOLATE MISSING VALUES
C          XXTCON     ADAS      CONVERSION OF TEMPERATURE ARRAY UNITS
C          XXDCON     ADAS      CONVERSION OF DENSITY     ARRAY UNITS
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          XFESYM     ADAS      CHARACTER*2  FUNCTION -
C                               GATHERS ELEMENT SYMBOL FOR INPUT Z0
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               GATHERS ELEMENT NAME FOR INPUT Z0
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION -
C                               CONVERSION OF TEMPERATURE ARRAY UNITS
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    17/06/91
C
C UPDATE:  22/11/91 - PE BRIDEN: 1) ONLY SCALE USER ENTERED TEMPERATURE/
C                                   DENSITY VALUES AND CARRY OUT SPLINE
C                                   ROUTINE IF 'LOSEL'=.TRUE.
C                                2) ADDED THE ARGUMENT 'LOSEL' TO D1OUT0
C                                2) ADDED THE ARGUMENT 'LOSEL' TO D1OUTG
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  06/07/95 - PE BRIDEN: -VERSION NUMBER INCREASED FROM 1.0->1.1
C                                 TO REFLECT CHANGES TO D1OUTG, D1SPF0,
C                                                       D1SPLN, DXSPL1
C                                                       DXSPL2, DXSPL3
C                                                       P40107, P40108C
C                                                       P40108L,P40110
C                                                       P40111, P40112L
C                                                       P40114
C
C UPDATE:  16/08/95 - PE BRIDEN: -VERSION NUMBER INCREASED FROM 1.1->1.2
C                                 EXTENDED DATA SET CLASSES AND
C                                 ALLOWED ALTERNATIVE NAMES BESIDES
C                                 IPRT AND ISYS IN THE DATA SETS.
C                                 (CHANGES IMPLEMENTED BY HUGH SUMMERS)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 18-07-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C		- PUT INTO SCCS READY FOR IDL-ADAS CONVERSION
C
C VERSION: 1.2				DATE: 04-09-95
C MODIFIED: TIM HAMMOND
C               - FINAL SKELETON VERSION OF THE CODE BEFORE
C		  UPDATES TO ACCOUNT FOR EXTRA DATA TYPES ARE
C		  INCORPORATED.
C
C VERSION: 1.3				DATE: 06-09-95
C MODIFIED: TIM HAMMOND
C               - SIGNIFICANT CHANGES DUE TO INCORPORATION OF CODE
C		  TO DEAL WITH EXTRA DATA TYPES.
C
C VERSION: 1.4                          DATE: 08-09-95
C MODIFIED: TIM HAMMOND
C               - ADDED CHECK FOR WHETHER GRAPHICAL OUTPUT IS REQ'D
C                 OR NOT BEFORE CALLING ROUTINE D1OUTG
C
C VERSION: 1.5				DATE: 24-10-95
C MODIFIED: TIM HAMMOND
C		- REMOVED SEVERAL UNUSED VARIABLES
C
C VERSION: 1.6				DATE: 03-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - REMOVED SPURIOUS FINAL COMMA BEFORE CLOSING BRACKET
C                 ON CALL TO D1ISPF.
C VERSION: 1.7				DATE: 09-07-96
C MODIFIED: WILLIAM OSBORN
C               - ADDED MENU BUTTONS AND MOVED TEXT OUTPUT TO BEFORE
C                 GRAPHICAL OUTPUT.
C VERSION: 1.8				DATE: 20-08-96
C MODIFIED: WILLIAM OSBORN + PAUL BRIDEN
C               - MINOR MODS TO REMOVE COMPILATION
C                 WARNING MESSAGES:
C                 1) INITIALISE IME AS 0.
C                 2) CHANGE EIA -> EIA(1) IN CALLS TO
C                    XXXINST and XXCEIA.
C
C VERSION: 1.9				DATE: 29-08-97
C MODIFIED: MARTIN O'MULLANE
C               - MINOR CHANGE TO DIMENSIONS NDTIN AND NDDEN
C                 INCREASE BOTH TO 24 TO ACCOMMODATE LARGER
C		  ADF10 FILES.
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NDTIN       , NDDEN        , NDZ1V       , NDMET    ,
     &           NTDMAX      , NMX          , MAXDEG
      INTEGER    L1          , L2           , L3
      INTEGER    IUNT07      , IUNT10
      INTEGER    LOGIC       , ISTOP
      INTEGER    ONE            , ZERO
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER (PIPEOU=6       , PIPEIN=5)
      PARAMETER (ONE=1          , ZERO=0)
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NDTIN  = 24 , NDDEN  =  24 , NDZ1V  = 10 , NDMET = 5 ,
     &           NTDMAX = 40 , NMX    = 100 , MAXDEG = 19 )
      PARAMETER( L1     =  1 , L2     =   2 , L3     =  3 )
      PARAMETER( IUNT07 =  7 , IUNT10 =  10 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT      
      INTEGER    IPAN        , ISWIT        , NEL         ,
     &           IDE         , ITE          , IZE         , IME       ,
     &           IMSEL       , IDSEL        , IZ0         , IZ1       ,
     &           ITDVAL      , NPRNT        , KPLUS1      , 
     &           ITTYP       , IM           , IT           , IZ
C-----------------------------------------------------------------------
CX    REAL*8     R8TCON
      REAL*8     TOLVAL      ,
     &           XMIN        , XMAX         , XMINR       , XMAXR     ,
     &           YMIN        , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LPEND       , LGHOST       , LPARTL      , LPOWER    ,
     &           LERROR      , LOSEL       , LFSEL     ,
     &           LGRD1       , LDEF1        , LSWIT       , OPEN10    ,
     &           LZRNG(1)
C-----------------------------------------------------------------------
CX    UNIX PORT - Following variables added for output selection options
      LOGICAL    LGRAPH       , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  XFESYM*2    , XFELEM*12
      CHARACTER  ESYMB*2     , CELEM*2      , CYEAR*2     , REP*3     ,
     &           CLASS*3     , CPROJ*8      , DATE*8      , ENAME*12  ,
     &           TITLE*40    , GTIT1*40     , DSFULL*80   , CHEAD*80  ,
     &           TITLX*80    , TITLM*80
CX    UNIX PORT - Following is name of output text file
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER    IMETR(NDMET)     , IPRNT(NDMET)    , IPSYS(NDMET)
C-----------------------------------------------------------------------
      REAL*8     EIA(250)         ,
     &           TR(NDTIN)        , DENSR(NDDEN)    , ZIPT(NDZ1V)     ,
     &           TUSER(NTDMAX)    , DUSER(NTDMAX)   ,
     &           TKEL(NTDMAX)     , TEV(NTDMAX)     , TRED(NTDMAX)    ,
     &           DRED(NTDMAX)     , AOUT(NTDMAX)    ,
     &           TREDM(NMX)       , AOUTM(NMX)      , COEF(MAXDEG+1)
C-----------------------------------------------------------------------
      LOGICAL    LDRNG(NTDMAX)    , LTRNG(NTDMAX)
C-----------------------------------------------------------------------
      CHARACTER  CLASSA(9)*3
      CHARACTER  CSTRGA(NDMET)*12
C-----------------------------------------------------------------------
      REAL*8     TVALS(NDTIN,3)   , APLOT(NDTIN,NDZ1V)
      REAL*8     AIPT(NDDEN,NDTIN,NDZ1V,NDMET)
C-----------------------------------------------------------------------
      DATA       IPAN    /0/      , ITDVAL /0/       , IME /0/
      DATA       OPEN10 /.FALSE./ , LGHOST /.FALSE./
C-----------------------------------------------------------------------
      DATA       CLASSA/ 'ACD' , 'SCD' , 'CCD' , 'PRB' , 'PRC' ,
     &                   'QCD' , 'XCD' , 'PLT' , 'PLS' /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM *****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
  100 IPAN  = 0
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
          CLOSE(10)
          OPEN10 = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL PANEL)
C-----------------------------------------------------------------------
C
      CALL D1SPF0( REP , DSFULL , ISWIT, IZ1,  CYEAR , CELEM, CLASS,
     &             CHEAD)
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL(ONLY IF IT IS VALID - ISWIT.GE.0)
C-----------------------------------------------------------------------
C
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      IF (ISWIT.GE.0) THEN
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
          IF (REP.EQ.'NO') THEN
              READ(PIPEIN, *) LOGIC
              OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='UNKNOWN' )
              OPEN10=.TRUE.
              LPOWER = (ISWIT.GE.8)
              IF (LOGIC.EQ.1) THEN
                  LPARTL = .TRUE.
              ELSE
                  LPARTL = .FALSE.
              ENDIF
              READ(PIPEIN, *) IPRNT(1)
              READ(PIPEIN, *) IPSYS(1)
              READ(PIPEIN, *) NEL
          ENDIF
      ELSE
          GOTO 100
      ENDIF
C
C-----------------------------------------------------------------------
C NOTE THAT THE VALUE OF IMSEL CAN BE CHANGED FOR A CASE WHERE
C LPARTL AND LPOWER ARE BOTH TRUE
C-----------------------------------------------------------------------
C
      IMSEL = 1
C
C-----------------------------------------------------------------------
C IF END OF RUN REQUESTED (I.E. 'CANCEL' ON INPUT SCREEN) THEN STOP
C-----------------------------------------------------------------------
C
      IF (REP.EQ.'YES') THEN
          GOTO 9999
      ENDIF
            
      IF (LPARTL) THEN
C
          IF (LPOWER) THEN
              CALL XXIN80( IUNT10   , DSFULL   , LERROR ,
     &                     NDDEN    , NDTIN    , NDZ1V  , NDMET ,
     &                     IDE      , ITE      , IZE    ,
     &                     DENSR    , TR       , ZIPT   ,
     &                     IME      , IMETR    , CSTRGA ,
     &                     NPRNT    , IPRNT    , IPSYS  ,
     &                     LSWIT    , EIA      ,
     &                     AIPT
     &                    )
           ELSE
              CALL XXIN17( IUNT10   , ISWIT, DSFULL   , LERROR ,
     &                     NDDEN    , NDTIN    , NDZ1V  ,
     &                     IPRNT(1) , IPSYS(1) ,
     &                     IDE      , ITE      , IZE    ,
     &                     DENSR    , TR       , ZIPT   ,
     &                     LSWIT    , EIA      ,
     &                     AIPT
     &                    )
           ENDIF
C
      ELSE
           CALL XXINST( IUNT10 , DSFULL , LERROR ,
     &                  NDDEN  , NDTIN  , NDZ1V  ,
     &                  IDE    , ITE    , IZE    ,
     &                  DENSR  , TR     , ZIPT   ,
     &                  LSWIT  , EIA(1) ,
     &                  AIPT
     &                 )
      ENDIF
C
C-----------------------------------------------------------------------
C IF ERROR ENCOUNTERED READING DATA RETURN TO DATASET SELECTION PANEL
C-----------------------------------------------------------------------
C
      IF (LERROR) THEN 
          WRITE (PIPEOU, *) ONE
          GOTO 100
      ELSE
          WRITE (PIPEOU, *) ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C CONVERT IONS. RATE COEFFS TO RYDBERGS AND FILL IN ANY MISSING VALUES
C-----------------------------------------------------------------------
C
      IF (LSWIT) THEN               
C
          IF     ( (ISWIT.GE.1) .AND. (ISWIT.LE. 7) ) THEN
             CALL XXCEIA( EIA(1) )
          ELSEIF ( (ISWIT.GE.8) .AND. (ISWIT.LE.10) )THEN
             DO 20 IM=1,IME
                CALL XXCEIA(EIA(50*(IM-1)+1))
   20        CONTINUE
          ENDIF
C
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
      CALL D1SETP( LPARTL , LPOWER , NEL    ,
     &             NDDEN  , IDE    , DENSR  ,
     &             NDMET  , IME    , IMETR  , CSTRGA ,
     &             NPRNT  , IPRNT  , IPSYS, IZE, ZIPT, NDZ1V
     &           )

C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
200   CONTINUE
      CALL D1ISPF( IPAN   , LPEND  ,
     &             LPARTL , LPOWER , NEL   , NDDEN ,
     &             NTDMAX , NDTIN  , TVALS , DENSR ,
     &             TITLE  , GTIT1  ,
     &             IZ1    , IMSEL  , IDSEL ,
     &             ITTYP  , ITDVAL , TUSER , DUSER ,
     &             LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN  , YMAX  ,
     &		   TR     , ITE    , L3    , IDE  
     &           )
      IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT USER ENTERED TEMPERATURES/DENSITY VALUES IF THEY EXIST
C-----------------------------------------------------------------------
C
      IF (LOSEL) THEN
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C CONVERT USER ENTERED TEMPERATURES INTO KELVIN (TUSER -> TKEL)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
          CALL XXTCON( ITTYP , L1 , IZ1 , ITDVAL , TUSER , TKEL )
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C CONVERT USER ENTERED TEMPERATURES INTO EV     (TUSER -> TEV)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
          CALL XXTCON( ITTYP , L2 , IZ1 , ITDVAL , TUSER , TEV  )
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C CONVERT USER ENTERED TEMPERATURES INTO REDUCED (TUSER -> TRED)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
          CALL XXTCON( ITTYP , L3 , IZ1 , ITDVAL , TUSER , TRED )
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C CONVERT USER ENTERED DENSITIES INTO REDUCED (DUSER -> DRED)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
          CALL XXDCON( L1    , L2 , IZ1 , ITDVAL , DUSER , DRED )
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
      ENDIF
C
C-----------------------------------------------------------------------
C  CONVERT XMIN & XMAX TEMPERATURE PLOTTING UNITS INTO REDUCED.
CA NOW HANDLED BY IDL
C-----------------------------------------------------------------------
C
C     XMINR  = R8TCON( ITTYP , L3 , IZ1 , XMIN )
C     XMAXR  = R8TCON( ITTYP , L3 , IZ1 , XMAX )
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING THREE-WAY SPLINE
C IF REQUIRED.
C-----------------------------------------------------------------------
C
      IF (LOSEL) THEN
          CALL D1SPLN( ISWIT    , LSWIT             ,
     &                 NTDMAX   ,
     &                 NDDEN    , NDTIN             , NDZ1V     ,
     &                 ITDVAL   ,
     &                 IDE      , ITE               , IZE       ,
     &                 DUSER    , TKEL              , IZ1       ,
     &                 DENSR    , TR                , ZIPT      ,
     &                 EIA(50*(IMSEL-1)+1)  , AIPT(1,1,1,IMSEL) ,
     &                 LZRNG(1) , LDRNG             , LTRNG     ,
     &                 AOUT
     &                )
      ENDIF
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITVAL > 2).
C-----------------------------------------------------------------------
C
      IF (ITDVAL.LE.2) LFSEL = .FALSE.
C
      IF (LFSEL) THEN
          CALL XXMNMX( LMLOG    , MAXDEG   , TOLVAL  ,
     &                 ITDVAL   , TRED     , AOUT    ,
     &                 NMX      , TREDM    , AOUTM   ,
     &                 KPLUS1   , COEF     ,
     &                 TITLM
     &                )
          WRITE(I4UNIT(-1),1000) TITLM(1:79)
      ENDIF
C
C *********************************************************************
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR DATA UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      IZ0   = NEL + IZ1 - 1
      ESYMB = XFESYM( IZ0 )
      ENAME = XFELEM( IZ0 )
C
      CALL D1TITL( IMSEL         , DSFULL        ,
     &             IPRNT(IMSEL)  , IPSYS(IMSEL)  ,
     &             CELEM         , CYEAR         , CLASS       ,
     &             TITLX
     &           )
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
CX UNIX PORT - Added new call to d1spf1 routine which communicates with idl
CX             via a pipe to get user selected output options. Note that if
CX             user selects graph x-axis ranges these are assumed to be
CX             in ev so no further conversion is carried out.
C-----------------------------------------------------------------------
C
300   CONTINUE
      CALL D1SPF1( DSFULL         , LFSEL        , LDEF1       ,
     &             LGRAPH         , L2FILE       , SAVFIL      ,
     &             XMIN           , XMAX         , YMIN        ,
     &             YMAX           , LPEND 	 , IDSEL )

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C     IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 200

C
C----------------------------------------------------------------------
C OUTPUT RESULTS
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
         CALL D1OUT0(IUNT07     , ISWIT      , LOSEL      , LFSEL  ,
     &               TITLE      , TITLX      , TITLM      , DATE   ,
     &               IMSEL      , ITDVAL     , LPARTL     , LPOWER ,
     &               ENAME      , ESYMB      ,
     &               IZ0        , IZ1        , NEL        ,
     &               CHEAD      ,
     &               CLASS      , CPROJ      , CYEAR      , CELEM ,
     &               IPRNT(IMSEL), IPSYS(IMSEL), CSTRGA(IMSEL)    ,
     &               LZRNG(1)   , LTRNG      , LDRNG      ,
     &               TKEL       , TEV        , TRED       ,
     &               DUSER                   , DRED       ,
     &               AOUT       , DSFULL     ,
     &               KPLUS1     , COEF
     &           )
        CLOSE(UNIT=IUNT07)
      ENDIF

C
C-----------------------------------------------------------------------
C SET UP ARRAY CONTAINING SELECTED DENSITY INPUT DATA FOR PLOTTING.
C-----------------------------------------------------------------------
C
      DO 2 IZ = 1,IZE
          DO 3 IT = 1,ITE
              APLOT(IT,IZ) = AIPT(IDSEL,IT,IZ,IMSEL)
3         CONTINUE
2     CONTINUE

      IF (LGRAPH) THEN
          CALL D1OUTG( ISWIT        , LGHOST       ,
     &                 NTDMAX       , NDTIN        , NDZ1V        ,
     &                 TITLE        , TITLX        , TITLM        , 
     &                 DATE         , GTIT1        ,
     &                 ESYMB        , IZ0          , IZ1          , 
     &                 NEL          , CHEAD        ,
     &                 CLASS        , CPROJ        , CYEAR        , 
     &                 CELEM        , LPARTL       , LPOWER       ,
     &                 IPRNT(IMSEL) , IPSYS(IMSEL) , CSTRGA(IMSEL),
     &                 DENSR(IDSEL) ,
     &                 ZIPT         , IZE          ,
     &                 TR           , APLOT        , ITE    ,
     &                 TRED         , DRED         , AOUT         , 
     &                 ITDVAL       ,
     &                 TREDM        , AOUTM        , NMX    ,
     &                 LGRD1        , LDEF1        , LOSEL        , 
     &                 LFSEL        ,
     &                 XMINR        , XMAXR        , YMIN         , 
     &                 YMAX )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF(ISTOP.EQ.1) GOTO 9999
C     
       ENDIF
 
C
C-----------------------------------------------------------------------
C  RETURN TO IDL PANEL. 
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C
 9999 END
