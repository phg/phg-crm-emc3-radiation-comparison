CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/d1outg.for,v 1.1 2004/07/06 13:11:56 whitefor Exp $ Date $Date: 2004/07/06 13:11:56 $
CX
      SUBROUTINE D1OUTG( ISWIT  , LGHOST ,
     &                   NTDMAX , NDTIN  , NDZ1V  ,
     &                   TITLE  , TITLX  , TITLM  , DATE   ,
     &                   GTIT1  ,
     &                   ESYMB  , IZ0    , IZ1    , NEL    ,
     &                   CHEAD  ,
     &                   CLASS  , CPROJ  , CYEAR  , CELEM  ,
     &                   LPARTL , LPOWER ,
     &                   IPRNT  , IPSYS  , CSTRGA ,
     &                   DENSR  ,
     &                   ZIPT   , IZE    ,
     &                   TR     ,          APLOT  , ITE    ,
     &                   TRED   , DRED   , AOUT   , ITDVAL ,
     &                   TREDM  ,          AOUTM  , NMX    ,
     &                   LGRD1  , LDEF1  , LOSEL  , LFSEL  ,
     &                   XMIN   , XMAX   , YMIN   , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D1OUTG *********************
C
C  PURPOSE:  COMMUNICATES GRAPHICS DATA TO IDL                       
C            (SELECTED BY 'ISWIT'):
C
C            1) INPUT DATA AT SELECTED DENSITY
C            2) MODEL DATA AT SPECIFIED TEMPERATURE/DENSITY PAIR VALUES
C
C  CALLING PROGRAM: ADAS401
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ISWIT   = DATA TYPE SELECTOR (1 -> 10)
C                           (SEE 'ADAS401' FOR CASES 1 -> 9,
C                            CASE 10 IMPLIES METSTABLE POPULATIONS)
C  I/O   : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                         = .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (I*4)  NTDMAX  = USER ENTERED VALUES -
C                            MAXIMUM NUMBER OF TEMPERATURE DENSITY PAIRS
C  INPUT : (I*4)  NDTIN   = INPUT MASTER CONDENSED FILE -
C                            MAXIMUM NUMBER OF REDUCED TEMPERATURES
C  INPUT : (I*4)  NDZ1V   = INPUT MASTER CONDENSED FILE -
C                            MAXIMUM NUMBER OF CHARGE STATES
C
C  INPUT : (C*40) TITLE   = IDENTIFYING TITLE FOR PROGRAM RUN (VIA ISPF)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED DENSITY and MET. INDEX
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (C*40) GTIT1   = ISPF ENTERED TITLE FOR GRAPH
C
C  INPUT : (C*2)  ESYMB   = SYMBOL OF ELEMENT UNDER ANALYSIS
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE OF ELEMENT UNDER ANALYSIS
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE UNDER ANALYSIS
C  INPUT : (I*4)  NEL     = NUMBER OF ELECTRONS IN SELECTED SEQUENCE
C
C  INPUT : (C*80) CHEAD   = HEADER FOR CLASS BEING ANALYSED.
C
C  INPUT : (C*3)  CLASS   = CLASS OF DATA SET BEING ANALYSED.
C                           ('ACD','SCD','CCD','PRB','PRC','PLT','PLS')
C  INPUT : (C*8)  CPROJ   = PROJECT UNDER WHICH INPUT DATA IS STORED
C  INPUT : (C*2)  CYEAR   = YEAR OF INPUT DATA (2 CHARACTER ABBREV.)
C  INPUT : (C*2)  CELEM   = ISO-ELECTRONIC SEQUENCE ELEMENT NAME
C
C  INPUT : (L*4)  LPARTL  = .TRUE.  => INPUT DATA SET PARTIAL  TYPE
C                           .FALSE. => INPUT DATA SET STANDARD TYPE
C  INPUT : (L*4)  LPOWER  = .TRUE.  => INPUT DATA SET POWER    TYPE
C                           .FALSE. => INPUT DATA SET COEFFT.  TYPE
C
C  INPUT : (I*4)  IPRNT   = PARENT INDEX FOR SELECTED 'METASTABLE' INDEX
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (I*4)  IPSYS   = SPIN SYSTEM  FOR SELECTED 'METASTABLE' INDEX
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (C*12) CSTRGA  = SELECTED METASTABLE LEVEL DESIGNATION
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  INPUT : (R*8)  DENSR   = SELECTED REDUCED DENSITY UNDER ANALYSIS
C
C  INPUT : (R*8)  ZIPT()  = INPUT MASTER CONDENSED FILE -
C                           SET OF 'IZE' INPUT RECOMBINING ION CHARGES
C                           DIMENSION: CHARGE STATE INDEX
C  INPUT : (I*4)  IZE     = INPUT MASTER CONDENSED FILE -
C                            NUMBER OF CHARGE STATES READ
C
C  INPUT : (R*8)  TR()    = INPUT MASTER CONDENSED FILE -
C                           SET OF 'ITE' REDUCED TEMPERATURES (K/Z1**2)
C                           DIMENSION: TEMPERATURE  INDEX
C  INPUT : (R*8)  APLOT(,)= INPUT MASTER CONDENSED FILE -
C                           RELEVANT COEFFICIENT/POWER DATA FOR 'ISWIT'
C                           FOR FIXED DENSITY 'DENSR' AND META. INDEX
C                           1st DIMENSION: TEMPERATURE  INDEX ('TR()')
C                           2nd DIMENSION: CHARGE STATE INDEX ('ZIPT()')
C  INPUT : (I*4)  ITE     = INPUT MASTER CONDENSED FILE -
C                            NUMBER OF REDUCED TEMPERATURES READ
C
C  INPUT : (R*8)  TRED()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' REDUCED TEMPS. (K/Z1**2)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  DRED()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' REDUCED DENSITIES (/Z1**7)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  AOUT()  = USER ENTERED VALUES -
C                           RELEVANT COEFFICIENT/POWER DATA FOR 'ISWIT'
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (I*4)  ITDVAL  = USER ENTERED VALUES -
C                            NUMBER OF TEMPERATURE/DENSITY PAIRS READ
C
C  INPUT : (R*8)  TREDM() = MINIMAX: 'NMX' SELECTED REDUCED TEMPERATURES
C                           DIMENSION: TEMPERATURE INDEX
C  INPUT : (R*8)  AOUTM() = MINIMAX: RELEVANT RATE COEFFT/POWER DATA FOR
C                           FOR 'ISWIT' AT TEMPERATURE 'TREDM()'
C                           DIMENSION: TEMPERATURE INDEX
C  INPUT : (I*4)  NMX     = MINIMAX: NUMBER OF MINIMAXGENERATED COEFFTS
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LOSEL   = .TRUE.  => INTERPOLATED VALUES FITTED
C                           .FALSE. => INTERPOLATED VALUES NOT FITTED
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED.
C                           (IF LOSEL=.FALSE. -> LFSEL=.FALSE.)
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (REDUCED)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (REDUCED)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RELEVANT COEFFTS.
C                                  ((erg) cm**3/s)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RELEVANT COEFFTS.
C                                  ((erg) cm**3/s)
C
C          (I*4)  L2      = PARAMETER = 2
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80
C                           TAKES NUMBERS AS BEING ZERO = 1.0E-36
C          (R*4)  YDMIN   = PARAMETER = MINIMUM ALLOWED Y-VALUE FOR
C                           PLOTTING. (USED FOR DEFAULT GRAPH SCALING)
C                           (SET TO 'GHZERO'/ZERO TO REMOVE)
C
CX         (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
CX         (I*4)  IKEY    = NUMBER OF 'KEY()' VALUES TO BE OUTPUT
CX         (I*4)  ICOUNT  = NUMBER OF POINTS PLOTTED FOR GRAPH CURVE
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR CHARGE STATES
C          (I*4)  I       = GENERAL USE ARRAY SUBSCRIPT
C
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*9)  DTYPE   = 'DATA   :'
C          (C*8)  CMODEL  = ' (MODEL)'
C          (C*27) KEY()   = DESCRIPTIVE KEY FOR GRAPH (3 TYPES)
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
CX         (C*7)  C7      = GENERAL USE 7 BYTE STRING
CX         (C*9)  C9      = GENERAL USE 9 BYTE STRING
CX         (C*6)  ZCHARS  = PLOTTED CURVE KEY: " Z1=??"
CX         (C*14) C14     = GENERAL USE 14 BYTE STRING
CX         (C*24) C24     = GENERAL USE 24 BYTE STRING
C          (C*34) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT()  = Y-AXIS UNITS/TITLE, DEPENDENT ON 'ISWIT'.
C                           DIMENSION: 'ISWIT'
C          (C*31) STRG01  = DESCRIPTIVE STRING FOR ISO-ELEC. SEQUENCE
C          (C*31) STRG02  = DESCRIPTIVE STRING FOR DATA SET PROJECT
C          (C*31) STRG03  = DESCRIPTIVE STRING FOR CLASS OF DATA
C          (C*31) STRG04  = DESCRIPTIVE STRING FOR YEAR OF DATA
C          (C*31) STRG05  = DESCRIPTIVE STRING FOR TYPE OF DATA
C          (C*31) STRG06  = DESCRIPTIVE STRING FOR PARENT REF.
C          (C*31) STRG07  = DESCRIPTIVE STRING FOR SPIN SYSTEM REF.
C          (C*31) STRG08  = DESCRIPTIVE STRING FOR DESIGNATION
C          (C*31) STRG09  = DESCRIPTIVE STRING FOR DENSITY PLOTTED
C          (C*31) STRG10  = DESCRIPTIVE STRING FOR ELEMENT
C          (C*31) STRG11  = DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*31) STRG12  = DESCRIPTIVE STRING FOR RECOMBINING ION CHGE
C          (C*31) STRG13  = DESCRIPTIVE STRING FOR NO. OF ELECTRONS
C          (C*36) HEAD1   = HEADING FOR INPUT FILE INFORMATION
C          (C*36) HEAD2   = HEADING FOR MODEL INFORMATION
C          (C*44) HEAD3A  = FIRST  HEADER FOR TEMPERATURE/DENSITY INFO.
C          (C*44) HEAD3B  = SECOND HEADER FOR TEMPERATURE/DENSITY INFO.
C
C
C ROUTINES:
C           ROUTINE     SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
CX          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C NOTE:     MINIMAX  CURVE  IS  FITTED  THROUGH  USER  ENTERED  SPLINED
C           TEMPERATURE/DENSITY PAIR VALUES.
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/37
C           JET EXT. 2520
C
C DATE:     17/06/91
C
C UPDATE:   07/08/91 - PE BRIDEN: INTRODUCED SUBROUTINE 'XXGSEL'
C
C UPDATE:   22/11/91 - PE BRIDEN: 1) ADDED ARGUMENT 'LOSEL' TO D1OUTG
C                                 2) WHEN FINDING DEFAULT GRAPH LIMITS
C                                    IGNORE MODEL DATA IF NOT USED.
C                                 3) ONLY SEND MODEL INFO TO GRAPH IF
C                                    IT EXISTS.
C                                 4) ADDED VARIABLE C24 + OTHER MINOR
C                                    CHANGES.
C
C UPDATE:  25/11/91 - PE BRIDEN: MADE FILNAM/PICSAV ARGUMENT LIST
C                                COMPATIBLE WITH GHOST VERSION 8.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  12/08/93 - HP SUMMERS  - EXTENDED ISWIT RANGE AND ADJUSTED
C
C UNIX PORT:
C
C VERSION: 1.1                          DATE: 24-10-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    L2
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( L2     = 2       )
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-32 )
C-----------------------------------------------------------------------
      INTEGER    ISWIT            ,
     &           NTDMAX           , NDTIN           , NDZ1V        ,
     &           IZ0              , IZ1             , NEL          ,
     &           IPRNT            , IPSYS           ,
     &           ITE              , IZE             , ITDVAL       ,
     &           NMX
      INTEGER    IZ               , I   
      INTEGER    ITEMP
      INTEGER    PIPEOU         , PIPEIN,    ONE              , ZERO
      PARAMETER (PIPEOU=6       , PIPEIN=5,   ONE=1    , ZERO=0)
C-----------------------------------------------------------------------
      REAL*8     DENSR            ,
     &           XMIN             , XMAX            ,
     &           YMIN             , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST           , LPARTL          , LPOWER       ,
     &           LGRD1            , LDEF1           ,
     &           LOSEL            , LFSEL
C-----------------------------------------------------------------------
      CHARACTER  ESYMB*2          , CYEAR*2         , CELEM*2      ,
     &           CLASS*3          , CPROJ*8         , DATE*8       ,
     &           CSTRGA*12        , TITLE*40        , GTIT1*40     ,
     &           TITLX*80         , TITLM*80        , CHEAD*80
      CHARACTER  GRID*1           , PIC*1           , C3BLNK*3     ,
     &           ADAS0*8      ,
     &           CMODEL*8         , KEY0*9          , MNMX0*9      ,
     &           DTYPE*9         ,
     &           DNAME*12         ,  
     &           XTIT*34          , CADAS*80        , ISPEC*88
      CHARACTER  STRG01*31        , STRG02*31       , STRG03*31    ,
     &           STRG04*31        , STRG05*31       , STRG06*31    ,
     &           STRG07*31        , STRG08*31       , STRG09*31    ,
     &           STRG10*31        , STRG11*31       , STRG12*31    ,
     &           STRG13*31        , HEAD1*36        , HEAD2*36     ,
     &                              HEAD3A*44       , HEAD3B*44
C-----------------------------------------------------------------------
      REAL*8     TR(NDTIN)        , ZIPT(NDZ1V)     ,
     &           TRED(NTDMAX)     , DRED(NTDMAX)    , AOUT(NTDMAX) ,
     &           TREDM(NMX)                         , AOUTM(NMX)
      REAL*8     APLOT(NDTIN,NDZ1V)
C-----------------------------------------------------------------------
      CHARACTER  YTIT(10)*23      , KEY(3)*27
C-----------------------------------------------------------------------
      SAVE       ISPEC            , CADAS
C
C-----------------------------------------------------------------------
C
      DATA ISPEC(1:48)
     &           /'COLLISIONAL-DIELECTRONIC COEFFT VS TEMPERATURE: '/
      DATA DNAME/'      DATE: '/
      DATA XTIT/'REDUCED TEMPERATURE (kelvin/Z1**2)'/
      DATA YTIT(1)/' ACD (CM**3 SEC-1)     '/
      DATA YTIT(2)/' SCD (CM**3 SEC-1)     '/
      DATA YTIT(3)/' CCD (CM**3 SEC-1)     '/
      DATA YTIT(4)/' PRB (ERG CM**3 SEC-1) '/
      DATA YTIT(5)/' PRC (ERG CM**3 SEC-1) '/
      DATA YTIT(6)/' QCD (CM**3 SEC-1)     '/
      DATA YTIT(7)/' XCD (CM**3 SEC-1)     '/
      DATA YTIT(8)/' PLT (ERG CM**3 SEC-1) '/
      DATA YTIT(9)/' PLS (ERG CM**3 SEC-1) '/
      DATA YTIT(10)/' METASTABLE POPULATION '/
      DATA ADAS0  /'ADAS   :'/       ,
     &     DTYPE  /'DATA   : '/
     &     MNMX0  /'MINIMAX: '/      ,
     &     KEY0   /'KEY    : '/      ,
     &     KEY(1) /'(LONG DASH - INPUT DATA)  ('/ ,
     &     KEY(2) /'CROSSES/FULL LINE - SPLINE)'/ ,
     &     KEY(3) /'  (SHORT DASH - MINIMAX)   '/
      DATA GRID   /' '/   ,
     &     PIC    /' '/   ,
     &     C3BLNK /'   '/ ,
     &     CADAS  /' '/
      DATA CMODEL /' (MODEL)'/
      DATA HEAD1 /'------ INPUT FILE INFORMATION ------'/            ,
     &     HEAD2 /'--------- MODEL INFORMATION --------'/             ,
     &     HEAD3A/'-- MODEL TEMPERATURE/DENSITY RELATIONSHIP --' / ,
     &     HEAD3B/'INDEX     REDUCED TEMP.     REDUCED DENSITY ' /
      DATA STRG01/' ISO-ELECTRONIC SEQUENCE     = '/ ,
     &     STRG02/' PROJECT                     = '/ ,
     &     STRG03/' CLASS                       = '/ ,
     &     STRG04/' YEAR                        = '/ ,
     &     STRG05/' TYPE                        = '/ ,
     &     STRG06/' PARENT REFERENCE            = '/ ,
     &     STRG07/' SPIN SYSTEM REFERENCE       = '/ ,
     &     STRG08/' DESIGNATION                 = '/ ,
     &     STRG09/' REDUCED DENSITY PLOTTED     = '/ ,
     &     STRG10/' ELEMENT SYMBOL              = '/ ,
     &     STRG11/' Z0: NUCLEAR CHARGE          = '/ ,
     &     STRG12/' Z1: RECOMBINING ION CHARGE  = '/ ,
     &     STRG13/' NUMBER OF ELECTRONS         = '/
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF ( (ISWIT.LT.1).OR.(ISWIT.GT.10) )
     &   STOP ' D1OUTG ERROR: ISWIT VALUE INVALID'
C
C-----------------------------------------------------------------------
C SET UP GRAPH MAIN TITLE & GATHER ADAS HEADER
C-----------------------------------------------------------------------
C
      ISPEC(49:88)=TITLE
C-----------------------------------------------------------------------
C PIPE COMMUNICATIONS WITH IDL
C-----------------------------------------------------------------------

      WRITE(PIPEOU, *) ISWIT
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) ITE
      CALL XXFLSH(PIPEOU)
      DO 1, ITEMP=1, ITE
          WRITE(PIPEOU, *) TR(ITEMP)
1     CONTINUE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) IZE
      CALL XXFLSH(PIPEOU)
      DO 2, ITEMP = 1, ITE
          DO 3, IZ=1, IZE
              WRITE(PIPEOU, *) APLOT(ITEMP, IZ)
3         CONTINUE
2     CONTINUE
      CALL XXFLSH(PIPEOU)

      IF (LOSEL) THEN
          WRITE(PIPEOU, *) ONE
          WRITE(PIPEOU, *) ITDVAL
	  CALL XXFLSH(PIPEOU)
	  DO 4, I=1, ITDVAL
	      WRITE(PIPEOU, *) TRED(I)
              WRITE(PIPEOU, *) DRED(I)
4	  CONTINUE
	  CALL XXFLSH(PIPEOU)
          DO 5, I=1, ITDVAL
              WRITE(PIPEOU, *) AOUT(I)
5         CONTINUE
          CALL XXFLSH(PIPEOU)
      ELSE
	  WRITE(PIPEOU, *) ZERO
          CALL XXFLSH(PIPEOU)
      ENDIF

      IF (LFSEL) THEN
          WRITE(PIPEOU, *) ONE
          WRITE(PIPEOU, *) NMX
          CALL XXFLSH(PIPEOU)
          DO 6, I=1, NMX
	      WRITE(PIPEOU, *) TREDM(I)
	      WRITE(PIPEOU, *) AOUTM(I)
6	  CONTINUE
	  CALL XXFLSH(PIPEOU)
      ELSE
          WRITE(PIPEOU, *) ZERO
          CALL XXFLSH(PIPEOU)
      ENDIF

      WRITE(PIPEOU, '(A)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A)') ESYMB
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1X,79('*')/1X,'D1OUTG ERROR: GRAPH SCALING - XMIN >= XMAX'/
     &       15X,'XMIN = ',1PE12.5,'   XMAX = ',1PE12.5/1X,79('*'))
 1001 FORMAT(1X,79('*')/1X,'D1OUTG ERROR: GRAPH SCALING - YMIN >= YMAX'/
     &       15X,'YMIN = ',1PE12.5,'   YMAX = ',1PE12.5/1X,79('*'))
 1002 FORMAT(' Z1=',I2)
 1003 FORMAT(1P,D9.2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
