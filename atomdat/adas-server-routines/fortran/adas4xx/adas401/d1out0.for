CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas401/d1out0.for,v 1.3 2004/07/06 13:11:53 whitefor Exp $ Date $Date: 2004/07/06 13:11:53 $
CX
       SUBROUTINE D1OUT0( IWRITE   , ISWIT  , LOSEL  , LFSEL  ,
     &                    TITLE    , TITLX  , TITLM  , DATE   ,
     &                    IMSEL    , ITDVAL , LPARTL , LPOWER ,
     &                    ENAME    , ESYMB  ,
     &                    IZ0      , IZ1    , NEL    ,
     &                    CHEAD    ,
     &                    CLASS    , CPROJ  , CYEAR  , CELEM  ,
     &                    IPRNT    , IPSYS  , CSTRGA ,
     &                    LZRNG    , LTRNG  , LDRNG  ,
     &                    TKEL     , TEV    , TRED   ,
     &                    DUSER             , DRED   ,
     &                    AOUT     , DSFULL , 
     &                    KPLUS1   , COEF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D1OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED COLL-DIEL COEFFTS
C
C  CALLING PROGRAM: ADAS401
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (I*4)  ISWIT   = DATA TYPE SELECTOR (1 -> 8)
C                           (SEE 'ADAS401' FOR CASES 1 -> 7,
C                            CASE 8 IMPLIES METSTABLE POPULATIONS)
C  OUTPUT: (L*4)  LOSEL   = .TRUE.  => INTERPOLATED VALUES FITTED
C                         = .FALSE. => INTERPOLATED VALUES NOT FITTED
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C                           (IF LOSEL=.FALSE. => LFSEL=.FALSE.)
C
C  INPUT : (C*40) TITLE   = IDENTIFYING TITLE FOR PROGRAM RUN (VIA ISPF)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED DENSITY and MET. INDEX
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IMSEL   = INDEX OF SELECTED METASTABLE STATE
C                           (ONLY USED IF (LPARTL.AND.LPOWER) -
C                            EQUALS ONE OTHERWISE).
C  INPUT : (I*4)  ITDVAL  = USER ENTERED VALUES -
C                            NUMBER OF TEMPERATURE/DENSITY PAIRS READ
C  INPUT : (L*4)  LPARTL  = .TRUE.  => PARTIAL  CONDENSED FILE READ IN
C                           .FALSE. => STANDARD CONDENSED FILE READ IN
C  INPUT : (L*4)  LPOWER  = .TRUE.  => LINE POWER  FILE READ IN
C                           .FALSE. => COEFFICIENT FILE READ IN
C
C  INPUT : (C*12) ENAME   = NAME   OF ELEMENT UNDER ANALYSIS
C  INPUT : (C*2)  ESYMB   = SYMBOL OF ELEMENT UNDER ANALYSIS
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE OF ELEMENT UNDER ANALYSIS
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE UNDER ANALYSIS
C  INPUT : (I*4)  NEL     = NUMBER OF ELECTRONS IN SELECTED SEQUENCE
C
C  INPUT : (C*80) CHEAD   = HEADER FOR CLASS BEING ANALYSED.
C
C  INPUT : (C*3)  CLASS   = CLASS OF DATA SET BEING ANALYSED.
C                           ('ACD','SCD','CCD','PRB','PRC','PLT','PLS')
C  INPUT : (C*8)  CPROJ   = PROJECT UNDER WHICH INPUT DATA IS STORED
C  INPUT : (C*2)  CYEAR   = YEAR OF INPUT DATA (2 CHARACTER ABBREV.)
C  INPUT : (C*2)  CELEM   = ISO-ELECTRONIC SEQUENCE ELEMENT NAME
C          (C*80) DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C
C  INPUT : (I*4)  IPRNT   = PARENT INDEX FOR SELECTED 'METASTABLE' INDEX
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (I*4)  IPSYS   = SPIN SYSTEM  FOR SELECTED 'METASTABLE' INDEX
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C  INPUT : (C*12) CSTRGA  = SELECTED METASTABLE LEVEL DESIGNATION
C                           NOT USED FOR STANDARD MASTER CONDENSED FILES
C
C  INPUT : (L*4)  LZRNG   =  .TRUE.  => OUTPUT 'AOUT()' VALUES    INTER-
C                                       POLATED FOR THE USER REQUESTED
C                                       CHARGE-STATE 'IZ1'.
C                            .FALSE. => OUTPUT 'AOUT()' VALUES    EXTRA-
C                                       POLATED FOR THE USER REQUESTED
C                                       CHARGE-STATE 'IZ1'.
C  INPUT : (L*4)  LTRNG() =  .TRUE.  => OUTPUT 'AOUT()' VALUE WAS INTER-
C                                       POLATED  FOR  THE  USER  ENTERED
C                                       ELECTRON TEMPERATURE 'TRED()'.
C                            .FALSE. => OUTPUT 'AOUT()' VALUE WAS EXTRA-
C                                       POLATED  FOR  THE  USER  ENTERED
C                                       ELECTRON TEMPERATURE 'TRED()'.
C                            DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C  INPUT : (L*4)  LDRNG() = .TRUE.  => OUTPUT 'AOUT()' VALUE WAS INTER-
C                                       POLATED  FOR  THE  USER  ENTERED
C                                       ELECTRON DENSITY 'DRED()'.
C                            .FALSE. => OUTPUT 'AOUT()' VALUE WAS EXTRA-
C                                       POLATED  FOR  THE  USER  ENTERED
C                                       ELECTRON DENSITY 'DRED()'.
C                            DIMENSION: TEMPERATURE DENSITY PAIR INDEX
C
C  INPUT : (R*8)  TKEL()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' TEMPERATURES. (KELVIN)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  TEV()   = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' TEMPERATURES. (EV)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  TRED()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' TEMPERATURES. (K/Z1**2)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  INPUT : (R*8)  DUSER() = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' ELECTRON DENSITIES (CM-3)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  DRED()  = USER ENTERED VALUES -
C                           SET OF 'ITDVAL' ELECTRON DENSITIES (/Z1**7)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  INPUT : (R*8)  AOUT()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  COEF-
C                           FICIENTS/POWERS FOR THE USER ENTERED TEMP./
C                           DENSITY PAIRS. (DATA TYPE GIVEN BY 'ISWIT')
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  IZ      = RECOMBINED ION CHARGE (= 'IZ1' - 1).
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (L*4)  LRATE   = .TRUE.  => 'AOUT' = RATE-COEFFT. (ISWIT<4).
C                           .FALSE. => 'AOUT' = POWER        (ISWIT>3).
C
C          (C*1)  C1T     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT ELECTRON TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*1)  C1D     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT ELECTRON DENSITY.     (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR  : PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/37
C           JET EXT. 2520
C
C DATE:    17/06/91
C
C UPDATE:  22/11/91 - PE BRIDEN: 1) 'LOSEL' ADDED TO ARGUMENT LIST.
C                                2) IF 'LOSEL'=.FALSE. OUTPUT NOTE
C                                   STATING NO INTERPOLATED DATA.
C
C UNIX PORT:
C
C VERSION: 1.1                          DATE: 06-09-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST RELEASE
C
C VERSION: 1.2				DATE: 24-10-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C		- REMOVED UNUSED VARIABLES
C
C VERSION: 1.3                          DATE: 13-05-96
C MODIFIED: TIM HAMMOND 
C               - REMOVED HOLLERITH CONSTANTS FROM OUTPUT
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , ISWIT          , IMSEL        ,
     &           ITDVAL        , IZ0            , IZ1          ,
     &           NEL           , IPRNT          , IPSYS        ,
     &           KPLUS1
      INTEGER    IZ            , I
C----------------------------------------------------------------------
      LOGICAL    LOSEL         , LFSEL          , LPARTL       ,
     &           LPOWER        , LZRNG
      LOGICAL    LRATE
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)      , TITLM*(*)    ,
     &           ESYMB*2       , CYEAR*2        , CELEM*2      ,
     &           CLASS*3       , CPROJ*8        , DATE*8       ,
     &           CSTRGA*12     , ENAME*12       , CHEAD*80
      CHARACTER  DSFULL*80
      CHARACTER  C1T*1         , C1D*1          , CADAS*80
C----------------------------------------------------------------------
      REAL*8     TKEL(ITDVAL)  , TEV(ITDVAL)    , TRED(ITDVAL) ,
     &           DUSER(ITDVAL) , DRED(ITDVAL)   , AOUT(ITDVAL) ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LTRNG(ITDVAL) , LDRNG(ITDVAL)
C----------------------------------------------------------------------
      SAVE       CADAS
C----------------------------------------------------------------------
      DATA       CADAS /' '/
C----------------------------------------------------------------------
C
C**********************************************************************
C
      LRATE = ISWIT.LE.3
      IZ    = IZ1 - 1
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &                 ' ISO-ELECTRONIC SEQUENCE INTERROGATION',
     &                 'ADAS401' , DATE
C
         IF (LRATE) THEN
            WRITE(IWRITE,1002) TITLE
         ELSE
            WRITE(IWRITE,1003) TITLE
         ENDIF
C
         IF (LPARTL.AND.LPOWER) THEN
            WRITE(IWRITE,1004) DSFULL(1:80) , IMSEL , CHEAD
         ELSE
            WRITE(IWRITE,1005) DSFULL(1:80) , CHEAD
         ENDIF
C
      WRITE(IWRITE,1006) CELEM  ,
     &                   CLASS  , CYEAR
C
         IF (.NOT.LPARTL) THEN
            WRITE(IWRITE,1007)
         ELSE
            WRITE(IWRITE,1008) IPRNT , IPSYS
            IF (LPOWER) WRITE(IWRITE,1009) CSTRGA
         ENDIF
C
      WRITE(IWRITE,1010) ENAME  , ESYMB  ,
     &                   IZ0    , IZ1    ,
     &                   IZ     , NEL
C
         IF (.NOT.LZRNG) THEN
               IF (LRATE) THEN
                  WRITE(IWRITE,1011) 'RATE-COEFFICIENTS'
               ELSE
                  WRITE(IWRITE,1011) 'POWERS'
               ENDIF
         ENDIF
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES, DENSITIES AND IONIZATIONS PER PHOTON
C---------------------------------------------------------------------
C
         IF (LRATE) THEN
            WRITE (IWRITE,1012)
         ELSE
            WRITE (IWRITE,1013)
         ENDIF
C
         IF (LOSEL) THEN
C
               DO 1 I=1,ITDVAL
                  C1T = '*'
                  C1D = '*'
                  IF (LTRNG(I)) C1T = ' '
                  IF (LDRNG(I)) C1D = ' '
                  WRITE(IWRITE,1014) TKEL(I)  , C1T , TEV(I)  ,
     &                                       C1T , TRED(I) ,
     &                            DUSER(I) , C1D , DRED(I) ,
     &                            AOUT(I)
    1          CONTINUE
C
            WRITE(IWRITE,1015)
C
               IF (LRATE) THEN
                  WRITE(IWRITE,1017) 'RATE-COEFFICIENT'
               ELSE
                  WRITE(IWRITE,1017) 'POWER'
               ENDIF
C
            WRITE(IWRITE,1015)
C
         ELSE
               WRITE(IWRITE,1015)
               WRITE(IWRITE,1016)
               WRITE(IWRITE,1015)
         ENDIF
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
               IF (LRATE) THEN
                  WRITE(IWRITE,1018)
               ELSE
                  WRITE(IWRITE,1019)
               ENDIF
C
            WRITE(IWRITE,1015)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1020) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1021) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1022) TITLM
            WRITE(IWRITE,1015)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,18('*'),' TABULAR OUTPUT FROM ',A38,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,18('*')/)
 1002 FORMAT(1H ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'RATE COEFFICIENT AS A FUNCTION OF ELECTRON ',
     & 'TEMPERATURE AND DENSITY '/
     & 2X,69('-')/
     & 18X,'DATA GENERATED USING PROGRAM: ADAS401'/18X,37('-'))
 1003 FORMAT(1H ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'POWER AS A FUNCTION OF ELECTRON ',
     & 'TEMPERATURE AND DENSITY '/
     & 2X,56('-')/
     & 12X,'DATA GENERATED USING PROGRAM: ADAS401'/12X,37('-'))
 1004 FORMAT(1H ,'FILE: ',A80,' - METASTABLE INDEX: ',I2/
     &       1H ,'DATA: ',A80)
 1005 FORMAT(1H ,'FILE: ',A80/
     &       1H ,'DATA: ',A80)
 1006 FORMAT(1H ,'INPUT FILE INFORMATION:'/
     &       1H ,23('-')/
     &       1H ,'ISO-ELECTRONIC SEQUENCE     =', 2X,A2   /
cx   &       1H ,'PROJECT                     =', 2X,A8   /
     &       1H ,'CLASS                       =', 2X,A3   /
     &       1H ,'YEAR                        =', 2X,A2   )
 1007 FORMAT(1H ,'TYPE                        =  STANDARD')
 1008 FORMAT(1H ,'TYPE                        =  PARTIAL' /
     &       1H ,'PARENT REFERENCE            =', 2X,I2   /
     &       1H ,'SPIN SYSTEM REFERENCE       =', 2X,I2   )
 1009 FORMAT(1H ,'DESIGNATION                 =', 2X,A12  )
 1010 FORMAT(1H ,'MODEL INFORMATION:'/
     &       1H ,18('-')/
     &       1H ,'ELEMENT NAME                =', 2X,A12 /
     &       1H ,'ELEMENT SYMBOL              =', 2X,A2  /
     &       1H ,'NUCLEAR CHARGE         (Z0) =', 2X,I3  /
     &       1H ,'RECOMBINING ION CHARGE (Z1) =', 2X,I3  /
     &       1H ,'RECOMBINED  ION CHARGE (Z)  =', 2X,I3  /
     &       1H ,'NUMBER OF ELECTRONS         =', 2X,I3  )
 1011 FORMAT(1H ,'*** WARNING: ',A,
     &           'WERE EXTRAPOLATED FOR THE ABOVE CHARGE-STATE (Z1)')
 1012 FORMAT(1H /1H ,1X,'------ ELECTRON TEMPERATURE ------',
     &               3X,      '-- ELECTRON DENSITY --',
     &               3X,         'RATE-COEFFICIENT'/
     &           1H ,4X,'kelvin',8X,'eV',7X,'K/Z1**2',
     &               7X,'cm-3',5X,'cm-3/Z1**7',
     &               6X,'cm**3/sec')
 1013 FORMAT(1H /1H ,1X,'------ ELECTRON TEMPERATURE ------',
     &               3X,      '-- ELECTRON DENSITY --',
     &               3X,         '---- POWER ----'/
     &           1H ,4X,'kelvin',8X,'eV',7X,'K/Z1**2',
     &               7X,'cm-3',5X,'cm-3/Z1**7',
     &               4X,'ergs cm**3/sec')
 1014 FORMAT(1H ,1P,1X,D10.3,2(1X,A,D10.3),3X,D10.3,1X,A,D10.3,5X,D10.3)
 1015 FORMAT(1H ,79('-'))
 1016 FORMAT(1H ,7('*'),' NO OUTPUT TEMPERATURE/DENSITY PAIRS WERE ',
     &                  'SELECTED FOR ANALYSIS ',8('*'))
 1017 FORMAT(1H ,'NOTE: * => ',A,' EXTRAPOLATED FOR ',
     &                     'TEMPERATURE/DENSITY VALUE')
C
 1018 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL FIT - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(RATE COEFFICIENT<cm**3/sec>) vs. ',
     &                'LOG10(ELECTRON TEMPERATURE<K/Z1**2>) -')
 1019 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL FIT - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(POWER<ergs cm**3/sec>) versus ',
     &                'LOG10(ELECTRON TEMPERATURE<K/Z1**2>) -')
 1020 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1021 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1022 FORMAT (1H ,A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
