CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas413/adas413.for,v 1.1 2004/07/06 10:49:02 whitefor Exp $ Date $Date: 2004/07/06 10:49:02 $
CX
C
C
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS413 **********************
C
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF STATE SELECTIVE IONISATION.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
C  DATA:     THE SOURCE DATA IS adf23
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            TEMPERATURES        : Kelvin
C            COLL. RATE COEFFTS. : cm**3 s-1
C            AUGER RATES         : s-1
C            ENERGY LEVELS       : cm-1
C
C  PROGRAM:
C
C          (I*4)  NDT   = PARAMETER = MAXIMUM NUMBER TEMPERATURES
C                                       THAT CAN BE READ FROM
C                                       AN INPUT DATA-SET.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF IDL ENTRED RECEIVER
C                                       /DONOR TEMPERATURE PAIRS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED RATE-COEFFT./
C                           TEMPERATURE PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE PAIRS
C          (I*4)  ITTYP   = 1 => 'TRIN/TDIN(array)' UNITS: KELVIN
C                         = 2 => 'TRIN/TDIN(array)' UNITS: EV
C                         = 3 => 'TRIN/TDIN(array)' UNITS: REDUCED
C
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMINEV  = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: EV
C          (R*8)  XMAXEV  = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: EV
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEF (cm**3/sec)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEF (cm**3/sec)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*120)DSFULL  = FULL INPUT DATA SET NAME (READ FROM PIPE)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*120) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C          (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END TO
C                           THE PROGRAM
C
C********* From  DDDATA ***************************************************
C
C          (I*4)  NDPRT   = MAXIMUM NUMBER OF PARENT STATES
C          (I*4)  NDREP   = MAX. NUMBER OF REPRESENTATIVE N-SHELLS
C          (I*4)  NDLEV   = MAXIMUM NUMBER OF RESOLVED LEVELS
C          (I*4)  NDAUG   = MAXIMUM NUMBER OF AUGER RATE INITIAL AND
C                           FINAL PARENT PAIRS
C          (I*4)  NDT   = MAX. NUMBER OF ELECTRON TEMPERATURES
C
C          (C*2)  SEQSYM  = RECOMBINED ION SEQ
C          (I*4)  IZ      = RECOMBINED ION CHARGE
C          (I*4)  IZ0     = NUCLEAR CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C          (I*4)  NPRF    = NUMBER OF FINAL PARENTS
C          (R*8)  BWNF    = BINDING WAVE NO. OF GROUND PARENT (CM-1)
C          (I*4)  NPRFM   = NUMBER OF FINAL PARENTS WHICH ARE METASTABLES
C          (I*4)  IPRFM() = CROSS-REFERENCING OF FINAL METASTABLE
C                           PARENTS TO FINAL PARENT LIST.
C          (I*4)  NPRI    = NUMBER OF FINAL PARENTS WHICH ARE INTERMEDIATE
C                           PARENTS FOR REPR. N-SHELL DOUBLY EXCITED STATES
C          (I*4)  IPRI()  = CROSS-REFERENCING OF INTERMEDIATE
C                           PARENTS TO FINAL PARENT LIST.
C          (I*4)  IPA()   = INDEX OF FINAL PARENT ENERGY LEVELS
C          (C*18) CSTRPA()= NOMENCL./CONFIG. FOR PARENT LEVEL 'IPA()'
C          (I*4)  ISPA()  = MULTIPLICITY FOR PARENT LEVEL 'IPA()'
C                           NOTE: (ISPA-1)/2 = QUANTUM NUMBER (SP)
C          (I*4)  ILPA()  = QUANTUM NUMBER (LP) FOR PARENT LEVEL 'IPA()'
C          (R*8)  XJPA()  = QUANTUM NUMBER (JP) FOR PARENT LEVEL 'IPA()'
C                           NOTE: (2*XJPA)+1 = STATISTICAL WEIGHT
C          (R*8)  WPA()   = ENERGY RELATIVE TO PARENT LEVEL 1 (CM-1)
C                           FOR PARENT LEVEL 'IPA()'
C
C          (I*4)  NLEV    = NUMBER OF ENERGY LEVELS (TERMS) OF THE
C                           IONISING ION
C          (R*8)  BWNI    = IONISATION POTENTIAL (CM-1) OF LOWEST LEVEL
C                           OF IONISING ION
C          (I*4)  NLEVM   = NUMBER OF IONISING ION LEVELS WHICH ARE METASTABLES
C          (I*4)  ILEVM() = CROSS-REFERENCING OF IONISNG ION METASTABLES
C                           TO IONISING ION LEVEL LIST.
C          (I*4)  IA()    = IONISING ION ENERGY LEVEL INDEX NUMBER
C          (C*18) CSTRGA()= NOMENCL./CONFIG. FOR RECOMBINED ION LEVEL
C                           'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR RECOMBINED LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR RECOMBINED LEVEL
C                           'IA()'
C          (R*8)  XJA()   = QUANTUM NUMBER (J) FOR RECOMBINED LEVEL
C                           'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO RECOMBINED LEVEL 1 (CM-1)
C                           FOR RECOMBINED LEVEL 'IA()'
C          (L*4)  LRAUG   = .TRUE.  => RESOLVED AUGER DATA PRESENT
C                           .FALSE. => RESOLVED AUGER DATA NOT PRESENT
C          (I*4)  NPF     = NUMBER OF FINAL PARENTS WITH RESOLVED AUGER DATA
C          (I*4)  INDF()  = INDICES OF FINAL PARENTS WITH RESOLVED AUGER DATA
C          (R*8)  RAUG(,) = RESOLVED AUGER RATES
C                           1ST.DIM: IONISING ION LEVEL INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C          (I*4)  IMETI() = INDEX OF METASTABLES IN IONISING ION LEVEL LIST
C          (L*4)  LSYSM() = .TRUE.  => SPIN SYSTEMS LINKED TO METASTABLE
C                           .FALSE. => NO SPIN SYSTEMS LINKED TO METASTABLE
C          (I*4)  NSYSM()  = NUMBER OF SPIN SYSTEMS LINKED TO METASTABLE
C          (L*4)  LREPM() = .TRUE.  => REPR. LEVELS LINKED TO METASTABLE
C                           .FALSE. => NO REP. LEVELS LINKED TO METASTABLE
C          (I*4)  NREPM()  = NUMBER OF REPR. LEVELS LINKED TO METASTABLE
C          (L*4)  LIONIS()= .TRUE.  => STATE SELECTIVE IONIS. DATA PRESENT
C                           .FALSE. => STATE SELECTIVE IONIS. DATA NOT PRESENT
C                           1ST.DIM: INITIAL METASTABLE INDEX
C          (L*4)  LRION(,)= .TRUE.  => DATA PRESENT FOR FINAL STATE
C                           .FALSE. => DATA NOT PRESENT FOR FINAL STATE
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C          (R*8)  RION(,,)= STATE SELECTIVE DIRECT IONISATION COEFFICIENTS
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C                           3RD.DIM: ELECTRON TEMPERATURE INDEX 
C          (L*4)  LEXCIT()= .TRUE.  => STATE SELECTIVE EXCIT. DATA PRESENT
C                           .FALSE. => STATE SELECTIVE EXCIT. DATA NOT PRESENT
C                           1ST.DIM: INITIAL METASTABLE INDEX
C          (L*4)  LREXC(,)= .TRUE.  => DATA PRESENT FOR FINAL STATE
C                           .FALSE. => DATA NOT PRESENT FOR FINAL STATE
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL IONISING ION LEVEL INDEX
C          (R*8)  REXC(,,)= STATE SELECTIVE DIRECT EXCITATION COEFFICIENTS
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL IONISING ION LEVEL INDEX
C                           3RD.DIM: ELECTRON TEMPERATURE INDEX 
C          (I*4)  NPIS()  = INDEX OVER INTERMEDIATE PARENT & SPIN SYSTEM
C                           FOR BUNDLE-N DOUBLY EXCITED STATES
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C          (I*4)  IPRTI(,)= INTERMEDIATE PARENT INDEX IN PARENT LIST
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (I*4)  ISYSI(,)= SPIN SYSTEM INDEX FOR INTERMEDIATE PARENT 
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (I*4)  ISPSYI(,)= SPIN OF SYSTEM FOR INTERMEDIATE PARENT
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (I*4)  IMETF(,)= FINAL PARENT INDICES TO WHICH INTERM. PARENT/
C                           SPIN SYSTEM/REPR. N-SHELL AUGERS 
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C                           3RD.DIM: FINAL IONISED ION METASTABLE INDEX 
C          (I*4)  NVALS(,)= NUMBER OF FINAL PARENT S TO WHICH INTERM. 
C                           PARENT/SPIN SYSTEM/REPR. N-SHELL AUGERS
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (I*4)  NREPI(,)= NUMBER OF REPR. N-SHELLS FOR  INTERM. 
C                           PARENT/SPIN SYSTEM
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (I*4)  IREP(,,)= REPR. N-SHELLS FOR INTERM. PARENT/SPIN SYSTEM/
C                           N-SHELL AUTOIONISING LEVELS
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (R*8)  AUGN(,,,)=AUGER RATES FOR INTERM. PARENT/SPIN SYSTEM/
C                           REPRESENTATIVE. N-SHELL
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (L*4)  LEXCN(,,)=.TRUE. => DATA PRESENT FOR REPR. N-SHELL
C                           .FALSE.=> DATA NOT PRESENT FOR REPR. N-SHELL
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C          (R*8)  EXCN(,,,)=EXCITATION RATES TO AUTOIONISING PARENT/SPIN
C                           SYSTEM REPRESENTATIVE N-SHELL
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C                           4TH DIM: TEMPERATURE INDEX
C          (I*4)  NTE     = NUMBER OF ELECTRON TEMPERATURES
C          (R*8)  TEA()   = ELECTRON TEMPERATURES (K)
C
C
C****************************************************************************

C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  IRZ0()  = NUCLEAR CHARGE OF RECEIVING IMPURITY ION -
C                           READ FROM SELECTED DATA-BLOCK.
C                           DIMENSION: DATA-BLOCK INDEX.
C          (I*4)  IRZ1()  = INITIAL CHARGE OF RECEIVER -
C                           READ FROM SELECTED DATA-BLOCK.
C                           DIMENSION: DATA-BLOCK INDEX.
C          (I*4)  IDZ0()  = NUCLEAR CHARGE OF NEUTRAL DONOR -
C                           READ FROM SELECTED DATA-BLOCK.
C                           DIMENSION: DATA-BLOCK INDEX.
C          (I*4)  ITRA()  = INPUT DATA SET - NUMBER OF RECEIVER TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITDA()  = INPUT DATA SET- NUMBER OF DONOR TEMPERATURES
C                           DIMENSION: DATA-BLOCK INDEX
C
C 	   (C*9)  CDONOR()= DONOR IDENTIFICATION STRING
C	   (C*9)  CRECVR()= RECEIVER IDENTIFICATION STRING
C  	   (C*10) CFSTAT()= FINAL STATE SPECIFICATION STRING
C
C          (R*8)  AMSRA() = INPUT DATA-SET - RECEIVER ATOMIC MASS
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  AMSDA() = INPUT DATA SET  - DONOR    ATOMIC MASS
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TRIN()  = USER ENTERED RECEIVER TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TDIN()  = USER ENTERED DONOR    TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TREVA() = USER ENTERED RECEIVER TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TDEVA() = USER ENTERED DONOR    TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TRKEL() = USER ENTERED RECEIVER TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TDKEL() = USER ENTERED DONOR    TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  TFEVM() = MINIMAX: SELECTED TEMPERATURES (eV)
C          (R*8)  QTCXA() = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE USER ENTERED RECEIVER/
C                           DONOR TEMPERATURE PAIRS (UNITS: CM**3/SEC)
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (R*8)  QTCXM() = RATE-COEFFTS. (cm**3/sec) AT 'TFEVM()'
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (R*8)  TFRA(,) = INPUT DATA SET -
C                           RECEIVER TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: RECEIVER TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TFDA(,) = INPUT DATA SET -
C                           DONOR    TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: DONOR    TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  QFTEQA(,)= INPUT DATA SET -
C                            EQUAL TEMPERATURE RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1st DIMENSION: RECEIVER TEMPERATURE INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (R*8) TRVALS(,,)= INPUT DATA SET - RECEIVER TEMPERATURES
C                            1ST DIMENSION: RECEIVER TEMPERATURE INDEX
C                                           ( SEE 'TFRA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8) TDVALS(,,)= INPUT DATA SET - DONOR    TEMPERATURES
C                            1ST DIMENSION: DONOR    TEMPERATURE INDEX
C                                           ( SEE 'TFDA(,)' )
C                            2ND DIMENSION: 1 => KELVIN  (ITTYP=1)
C                                           2 => EV      (ITTYP=2)
C                                           3 => REDUCED (ITTYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C          (R*8)  QFTCXA(,,)=INPUT DATA SET -
C                            FULL SET OF RATE-COEFFICIENTS
C                                   (UNITS: cm**3 sec-1)
C                            1st DIMENSION: DONOR    TEMPERATURE INDEX
C                            2nd DIMENSION: RECEIVER TEMPERATURE INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LEQUA() = INPUT DATA SET - DATA SET ENTRY FORMAT
C                                  .TRUE.  => DATA  SET CONTAINS  EQUAL
C                                             TEMPERATURE COEFFICIENT.
C                                  .FALSE. => DATA SET DOES NOT CONTAIN
C                                             EQUAL TEMPERATURE COEFFT.
C                           DIMENSION: DATA-BLOCK INDEX
C          (L*4)  LTRRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (L*4)  LTDRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TDEVA()'.
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
CC	SOURCA NO LONGER IN ADF14 FILE
CX          (C*26) SOURCA()= INPUT DATA SET - INFORMATION STRING
CX                           DIMENSION: DATA-BLOCK INDEX
C
C****************************************************************************


C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          DDSPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          DDDATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          DDSETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          DDISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          DDSPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          DDTITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          DDOUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          DDOUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
C
C AUTHOR:  H. P. Summers, University of Strathclyde
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    21/03/98
C
C UPDATE:  
C 
C VERSION:	1.1						DATE: 11-06-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
      INTEGER    IUNT07 , IUNT10   , PIPEIN  , ISTOP  
      INTEGER    MAXDEG , NMX
      INTEGER    NDT    , NDTIN  
      INTEGER    L1     , L2       , IFORM
C-----------------------------------------------------------------------
      REAL*8     DZERO
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( IUNT07 =   7 , IUNT10 = 10  , PIPEIN = 5 )
      PARAMETER( NDT    =  12 , NDTIN  = 20  )
      PARAMETER( L1     =   1 , L2     =  2  )
      PARAMETER( MAXDEG =  19 , NMX    = 100 )
C-----------------------------------------------------------------------
      PARAMETER( DZERO  = 1.0D-60 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IFIRST       , ILAST        , LEN_NAME
      INTEGER    KPLUS1
      INTEGER    ITVAL        , ITTYP        , IOPT       
      INTEGER    IP_AUG       , IM_AUG 
      INTEGER    IP_RES       , IL_RES
      INTEGER    IN_REP       , IM_REP       , IPIS_REP   , IPI_REP    ,
     &           ISS_REP
C-----------------------------------------------------------------------
      REAL*8     XMIN         , XMAX         , XMINEV     , XMAXEV     ,
     &           YMIN         , YMAX         ,
     &           TOLVAL
      real*8     tsmin        , tsmax        , tsstep
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , OPEN07       
      LOGICAL    LPEND        , 
     &           LOSEL        , LFSEL      , LGRAPH     , L2FILE       ,
     &           LDSEL        , LGRD1      , LDEF1      ,       
     &           LREP
C-----------------------------------------------------------------------
      CHARACTER  REP*3        , DATE*8       , DSFULL*120  , SAVFIL*80 ,
     &           TITLE*40     , TITLM*80     , TITLX*120   , CADAS*80
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)    , TEVA(NDTIN)   , TKEL(NDTIN)
      REAL*8     QDRIN(NDT)    , QDROUT(NDTIN)             
      REAL*8     TFEVM(NMX)    , QDRM(NMX)     , COEF(MAXDEG+1)
      REAL*8     TFEVS(NMX)    , QDRS(NMX)     
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN)  , lfit(nmx)
C-----------------------------------------------------------------------
      character cpif*21        , clim*21
      character cpi*21         , cpf*21     , css*4
      character clem*21        , clef*21  
      character cstrl*1
C-----------------------------------------------------------------------
C

C *********** DATA definitions ****************
C-----------------------------------------------------------------------
      INTEGER   NDPRT       , NDPRTI         , NDREP          ,
     &          NDLEV       , NDMET          , NDAUG
C-----------------------------------------------------------------------
      PARAMETER ( NDPRT = 40 , NDPRTI=10  , NDLEV = 20 , NDREP = 100 )
      PARAMETER ( NDMET = 5  , NDAUG = 8  )
C-----------------------------------------------------------------------
      INTEGER   IZ          , IZ0            , IZ1        ,
     &          NPRF        , NPRFM          , NPRI       , NLEV       ,
     &          NLEVM       , NPF
      INTEGER   NTE         , I
      INTEGER   NREP  
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRT)  , ISPA(NDPRT)    , ILPA(NDPRT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IPRFM(NDPRT), IPRI(NDPRTI)   , ILEVM(NDLEV)
      INTEGER   INDF(NDPRT)
      INTEGER   IMETI(NDMET), NSYSM(NDMET)   , NREPM(NDMET)
      INTEGER   IPRTI(NDMET,NDPRTI)          , ISYSI(NDMET,NDPRTI)     , 
     &          ISPSYI(NDMET,NDPRTI)
      INTEGER   IMETF(NDMET,NDPRTI,NDPRT)    , NVALS(NDMET,NDPRTI)
      INTEGER   NPIS(NDMET)                  , NREPI(NDMET,NDPRTI)     ,
     &          IREP(NDREP,NDMET,NDPRTI)
      INTEGER   NREPA(NDREP)

C-----------------------------------------------------------------------
      REAL*8    BWNP        , BWNR
      REAL*8    R8TCON
C-----------------------------------------------------------------------
      REAL*8    XJPA(NDPRT) , WPA(NDPRT)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)
      REAL*8    TEA(NDT)    , RAUG(NDPRT,NDLEV)
      REAL*8    RION(NDMET,NDPRT,NDT) , REXC(NDMET,NDLEV,NDT)
      REAL*8    AUGN(NDREP,NDMET,NDPRTI,NDPRT)                         ,
     &          EXCN(NDREP,NDMET,NDPRTI,NDT)
      REAL*8    AUGA(NDREP,NDAUG)
C-----------------------------------------------------------------------
      CHARACTER SEQSYM*2    
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRT)*18 , CSTRGA(NDLEV)*18
C-----------------------------------------------------------------------
      LOGICAL   LRAUG
C-----------------------------------------------------------------------
      LOGICAL   LSYSM(NDMET)       , LREPM(NDMET)
      LOGICAL   LIONIS(NDMET)      , LEXCIT(NDMET)
      LOGICAL   LRION(NDMET,NDPRT) , LREXC(NDMET,NDLEV)
      LOGICAL   LEXCN(NDREP,NDMET,NDPRTI)
C-----------------------------------------------------------------------
      REAL*8     TVALS(NDT,3)
C-----------------------------------------------------------------------

      DATA OPEN10 /.FALSE./ , OPEN07/.FALSE./
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )

C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
 100  CONTINUE

         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (VIA IDL PIPE)
C-----------------------------------------------------------------------
C
      CALL DDSPF0( REP , DSFULL , LDSEL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS = 'OLD' )
      OPEN10=.TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
       CALL DDDATA( IUNT10 , NDPRT  , NDPRTI, NDREP , NDLEV , 
     &              NDMET  , NDAUG  , NDT   ,
     &              SEQSYM , IZ     , IZ0   , IZ1   ,
     &              NPRF   , BWNP   , NPRFM , IPRFM ,
     &              NPRI   , IPRI   ,
     &              IPA    , CSTRPA , ISPA  , ILPA  , XJPA  ,
     &              WPA    ,
     &              NLEV   , BWNR   , NLEVM , ILEVM ,
     &              IA     , CSTRGA , ISA   , ILA   , XJA   ,
     &              WA     ,
     &              LRAUG  , NPF    , INDF  , RAUG  ,
     &              IMETI  , LSYSM  , NSYSM , LREPM , NREPM , 
     &              LIONIS , LRION  , RION  ,
     &              LEXCIT , LREXC  , REXC  ,
     &              NPIS   , IPRTI  , ISYSI , ISPSYI,
     &              IMETF  , NVALS  , NREPI , IREP  ,
     &              AUGN   , LEXCN  , EXCN  ,
     &              NTE    , TEA
     &             )
C
C-----------------------------------------------------------------------
C now do stuff
C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS ELECTRON TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TEA()' ARE IN KELVIN.
C-----------------------------------------------------------------------
C
      DO IFORM=1,3
         CALL XXTCON( L1    , IFORM ,
     &                IZ1   ,
     &                NTE   , TEA   , TVALS(1,IFORM)  )
      END DO

      CLOSE(10)
   
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL PIPE)
C-----------------------------------------------------------------------

  200 CALL DDISPF( LPEND  , IZ     , IZ0     , IZ1     ,
     &             NDT    , NDTIN   , NTE    , TVALS   ,
     &             NDPRT  , NDPRTI  , NDREP   , NDLEV , NDMET   , 
     &             NDAUG  , 
     &             NPRF   , NPRFM   , IPRFM   , NPRI  , IPRI    ,
     &             IPA    , CSTRPA  , ISPA    , ILPA  ,
     &             NLEV   , NLEVM   , ILEVM   ,
     &             IA     , CSTRGA  , ISA     , ILA   ,
     &             LRAUG  , NPF     , INDF    ,
     &             IMETI  , LSYSM   , NSYSM   , LREPM , NREPM   , 
     &             LIONIS , LRION   , LEXCIT  , LREXC ,
     &             NPIS   , IPRTI   , ISYSI   , ISPSYI,
     &             IMETF  , NVALS   , NREPI   , IREP  ,
     &             LEXCN  , TITLE   , IOPT    ,
     &             IP_AUG , IM_AUG  , IP_RES , IL_RES  ,
     &             IN_REP , IM_REP  , IPIS_REP, IPI_REP, ISS_REP,
     &             ITTYP  , ITVAL   , TIN     ,
     &             LOSEL  , LFSEL   , LGRD1   , LDEF1  ,
     &             TOLVAL ,
     &             XMIN   , XMAX    , YMIN    , YMAX    )

C
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100   
C
C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN, EV  
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L1 , IZ1 , ITVAL , TIN , TKEL )
      CALL XXTCON( ITTYP , L2 , IZ1 , ITVAL , TIN , TEVA )

C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV.
C-----------------------------------------------------------------------
C
      XMINEV = R8TCON( ITTYP , L2 , IZ1 , XMIN )
      XMAXEV = R8TCON( ITTYP , L2 , IZ1 , XMAX )
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C
C Depending on option qdr is the direct ionis.,  excitation to
C autionising levels or excitation to autoionising n-shells 
C nb: if the data is 0.0E+0 the spline fit will fail
C-----------------------------------------------------------------------
C
      IF (IOPT.EQ.2) THEN
            DO I=1, NTE
              QDRIN(I) = MAX(DZERO,EXCN(IN_REP,IM_REP,IPIS_REP,I))
            END DO
      ELSEIF (IOPT.EQ.0) THEN
            DO I=1, NTE
              QDRIN(I) = MAX(DZERO,RION(IL_RES,IP_RES,I))
            END DO
      ELSE
            DO I=1, NTE
              QDRIN(I) = MAX(DZERO,REXC(IP_AUG,IM_AUG,I))
            END DO 
      ENDIF
C
         CALL DDSPLN( NDT        , NDTIN   ,
     &                NTE          , ITVAL   ,
     &                TVALS(1,L2)  , TEVA    ,
     &                QDRIN        , QDROUT  ,
     &                LTRNG        )
C
C--------------------------------------               
C SPLINE THE OUTPUT CURVE TO NMX POINTS
C--------------------------------------
C    
         TSMIN  = LOG10(TVALS(1,L2))
         TSMAX  = LOG10(TVALS(ITVAL,L2))
         TSSTEP = (TSMAX - TSMIN )/ (NMX - 1)
C
         DO I=1,NMX
           TFEVS(I) = 10.0**( TSMIN + TSSTEP*(I-1) )
         END DO
C
         CALL DDSPLN( NDT          , NMX     ,
     &                NTE          , NMX     ,
     &                TVALS(1,L2)  , TFEVS   ,
     &                QDRIN        , QDRS    ,
     &                LFIT         
     &              )
C         
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITVAL > 2).
C-----------------------------------------------------------------------
C
      IF (ITVAL.LE.2) LFSEL = .FALSE.
C 
      IF (LFSEL) THEN
         CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                ITVAL   , TEVA     , QDROUT  ,
     &                NMX     , TFEVM    , QDRM    ,
     &                KPLUS1  , COEF     ,
     &                TITLM   )
      ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT FILE UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      CALL XXSLEN(DSFULL,IFIRST,ILAST)
      LEN_NAME=ILAST-IFIRST+1
      TITLX = ' '
      TITLX(1:LEN_NAME) = DSFULL(IFIRST:ILAST)

C
C-----------------------------------------------------------------------
C  DDSPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
C  A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------   
C
 300  CONTINUE 
  
       CALL DDSPF1( DSFULL   , 
     &              LFSEL    , LDEF1    ,
     &              LGRAPH   , L2FILE   , SAVFIL   ,
     &              XMIN     , XMAX     , YMIN     , YMAX  ,
     &              LPEND    , CADAS    , LREP     )
     
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
C IF DESIRED - SAVE RESULTS TO FILE AND GRAPH
C         
C make up parent and spin strings for output
C----------------------------------------------------------------------
C         
         write(clem,1100) cstrga(imeti(ip_aug)),isa(imeti(ip_aug)),
     &                    cstrl(ila(imeti(ip_aug)))
         write(clef,1100) cstrga(im_aug),isa(im_aug),
     &                    cstrl(ila(im_aug))
         write(cpif,1100) cstrpa(ip_res),ispa(ip_res),
     &                    cstrl(ilpa(ip_res))
         write(clim,1100) cstrga(imeti(il_res)),isa(imeti(il_res)),
     &                    cstrl(ila(imeti(il_res)))  
         write(cpi,1100)  cstrga(im_rep),isa(im_rep),
     &                    cstrl(ila(im_rep))
         write(cpf,1100)  cstrpa(ipi_rep),ispa(ipi_rep),
     &                    cstrl(ilpa(ipi_rep))
 	 write(css,1102)  iss_rep
     
 1100    format(a18,i2,a1)        
 1102    format('(',i2,')')

C
      IF (L2FILE) THEN
         IF (LREP.AND.OPEN07)THEN
            OPEN07 = .FALSE.
            CLOSE(IUNT07)
         ENDIF
         IF(.NOT.OPEN07)THEN
            OPEN(UNIT=IUNT07,FILE=SAVFIL,STATUS='UNKNOWN')
            OPEN07 = .TRUE.
         ENDIF
C                        
         CALL DDOUT0( IUNT07   , LFSEL ,
     &                TITLE    , TITLX  , TITLM , DATE   ,
     &                iopt     ,
     &                clem     , clef   , 
     &                nrep     , nrepa  , auga(1,ip_aug) ,
     &                cpif     , clim   ,
     &                cpi      , cpf    , css   , 
     &                irep(in_rep,im_rep,ipis_rep)  ,
     &                ITVAL    , TEVA   , TKEL  ,
     &                IZ0      , IZ1    , 
     &                LTRNG    , 
     &                QDROUT   ,
     &                KPLUS1   , COEF   , CADAS  )
C
      ENDIF
C
C-----------------------------
C  If selected, generate graph
C-----------------------------
C
      IF(LGRAPH) THEN
         CALL DDOUTG( TITLE  , TITLX  , TITLM , DATE  ,
     &                IZ0    , IZ1    ,
     &                iopt   ,
     &                clem   , clef   , 
     &                nrep   , nrepa  , auga(1,ip_aug) ,
     &                cpif   , clim   ,
     &                cpi    , cpf    , css   , 
     &                irep(in_rep,im_rep,ipis_rep)  ,
     &                TEVA   , QDROUT , ITVAL ,
     &                TFEVM  , QDRM   , NMX   ,
     &                TFEVS  , QDRS   , 
     &                LGRD1  , LDEF1  , LFSEL ,
     &                XMIN   , XMAX   , YMIN  , YMAX  )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
         READ(PIPEIN,*) ISTOP
         IF (ISTOP.EQ.1) GOTO 9999
         
      ENDIF
C
C-----------------------------------------------------------------------
C RETURN TO OUTPUT PANELS. (ON RETURN UNIT 10 IS CLOSED).
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE

      IF(OPEN07) CLOSE(IUNT07)

      END
