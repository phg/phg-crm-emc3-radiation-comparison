CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas413/ddtitl.for,v 1.1 2004/07/06 13:30:03 whitefor Exp $ Date $Date: 2004/07/06 13:30:03 $
CX
      SUBROUTINE DDTITL( IOPT     , 
     &           	 IP_RES   , IL_RES , 
     & 			 DSFULL   ,
     &                   TITLX 
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DDTITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS413
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IOPT       = Type of plot
C  INPUT : (I*4)  IL_RES     = resolved DR parent
C  INPUT : (I*4)  IL_RES     = resolved DR level
C  I
C  INPUT : (C*80) DSFULL   = FULL INPUT DATA SET NAME
C
CX  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: RECEIVER -
CX                                                 NUCLEAR CHARGE
CX  INPUT : (I*4)  IRZ1     = SELECTED DATA-BLOCK: RECEIVER -
CX                                                 RECOMBINING ION CHARGE
CX  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: DONOR    -
CX                                                 NUCLEAR CHARGE
C
C  INPUT : (C*9)  CDONOR   = SELECTED DATA-BLOCK: DONOR IDENTITY
C
C  INPUT : (C*9)  CRECVR   = SELECTED DATA-BLOCK: RECEIVER IDENTITY
C
C  INPUT : (C*10) CFSTAT  = SELECTED DATA-BLOCK: FINAL STATE SPEC.
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C
C
C AUTHOR   : H. P. Summers, University of Strathclyde
C            JA8.08
C            Tel. 0141-553-4196
C
C DATE     : 30/01/98
C
C UPDATE: 
C
C VERSION:	1.1						DATE: 11-06-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IOPT        , IP_RES  , IL_RES  ,         
     &           LEN_NAME    ,
     &		 IFIRST      , ILAST	  
C-----------------------------------------------------------------------
      CHARACTER  C2A*2           , C2B*2
      CHARACTER  DSFULL*120      , TITLX*120
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      CALL XXSLEN(DSFULL,IFIRST,ILAST)
      LEN_NAME=ILAST-IFIRST+1
      WRITE(C2A,1000) IP_RES
      WRITE(C2B,1000) IL_RES
      TITLX(1:LEN_NAME) = DSFULL(IFIRST:ILAST)
      TITLX(LEN_NAME+1:LEN_NAME+21) = ' Parent: '//C2A//' Level: '//C2B
C	titlx ='A temporary title'
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
