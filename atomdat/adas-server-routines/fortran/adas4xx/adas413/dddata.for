CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas413/dddata.for,v 1.1 2004/07/06 13:29:31 whitefor Exp $ Date $Date: 2004/07/06 13:29:31 $
CX
      SUBROUTINE  DDDATA( IUNIT  , NDPRT  , NDPRTI, NDREP , NDLEV ,
     &                    NDMET  , NDAUG  , NDT   ,
     &                    SEQSYM , IZ     , IZ0   , IZ1   ,
     &                    NPRF   , BWNF   , NPRFM , IPRFM ,
     &                    NPRI   , IPRI   ,
     &                    IPA    , CSTRPA , ISPA  , ILPA  , XJPA  ,
     &                    WPA    ,
     &                    NLEV   , BWNI   , NLEVM , ILEVM ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA   ,
     &                    WA     ,
     &                    LRAUG  , NPF    , INDF  , RAUG  ,
     &                    IMETI  , LSYSM  , NSYSM , LREPM , NREPM , 
     &                    LIONIS , LRION  , RION  ,
     &                    LEXCIT , LREXC  , REXC  ,
     &                    NPIS   , IPRTI  , ISYSI , ISPSYI,
     &                    IMETF  , NVALS  , NREPI , IREP  ,
     &                    AUGN   , LEXCN  , EXCN   ,
     &                    NTE    , TEA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DDDATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT ADF23 DATA SET.
C
C  CALLING PROGRAM: ADAS213/ADAS413
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C
C  INPUT : (I*4)  NDPRT   = MAXIMUM NUMBER OF PARENT STATES
C  INPUT : (I*4)  NDPRTI  = MAXIMUM NUMBER OF INTERMEDIATE PARENT STATES
C  INPUT : (I*4)  NDREP   = MAX. NUMBER OF REPRESENTATIVE N-SHELLS
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF RESOLVED LEVELS
C  INPUT : (I*4)  NDMET   = MAXIMUM NUMBER OF IONISING ION METASTABLES
C  INPUT : (I*4)  NDAUG   = MAXIMUM NUMBER OF AUGER RATE INITIAL AND
C                           FINAL PARENT PAIRS
C  INPUT : (I*4)  NDT     = MAX. NUMBER OF ELECTRON TEMPERATURES
C
C  OUTPUT: (C*2)  SEQSYM  = RECOMBINED ION SEQ
C  OUTPUT: (I*4)  IZ      = RECOMBINED ION CHARGE
C  OUTPUT: (I*4)  IZ0     = NUCLEAR CHARGE
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE
C  OUTPUT: (I*4)  NPRF    = NUMBER OF FINAL PARENTS
C  OUTPUT: (R*8)  BWNF    = BINDING WAVE NO. OF GROUND PARENT (CM-1)
C  OUTPUT: (I*4)  NPRFM   = NUMBER OF FINAL PARENTS WHICH ARE METASTABLES
C  OUTPUT: (I*4)  IPRFM() = CROSS-REFERENCING OF FINAL METASTABLE
C                           PARENTS TO FINAL PARENT LIST.
C  OUTPUT: (I*4)  NPRI    = NUMBER OF FINAL PARENTS WHICH ARE INTERMEDIATE
C                           PARENTS FOR REPR. N-SHELL DOUBLY EXCITED STATES
C  OUTPUT: (I*4)  IPRI()  = CROSS-REFERENCING OF INTERMEDIATE
C                           PARENTS TO FINAL PARENT LIST.
C  OUTPUT: (I*4)  IPA()   = INDEX OF FINAL PARENT ENERGY LEVELS
C  OUTPUT: (C*18) CSTRPA()= NOMENCL./CONFIG. FOR PARENT LEVEL 'IPA()'
C  OUTPUT: (I*4)  ISPA()  = MULTIPLICITY FOR PARENT LEVEL 'IPA()'
C                           NOTE: (ISPA-1)/2 = QUANTUM NUMBER (SP)
C  OUTPUT: (I*4)  ILPA()  = QUANTUM NUMBER (LP) FOR PARENT LEVEL 'IPA()'
C  OUTPUT: (R*8)  XJPA()  = QUANTUM NUMBER (JP) FOR PARENT LEVEL 'IPA()'
C                           NOTE: (2*XJPA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WPA()   = ENERGY RELATIVE TO PARENT LEVEL 1 (CM-1)
C                           FOR PARENT LEVEL 'IPA()'
C
C  OUTPUT: (I*4)  NLEV    = NUMBER OF ENERGY LEVELS (TERMS) OF THE
C                           IONISING ION
C  OUTPUT: (R*8)  BWNI    = IONISATION POTENTIAL (CM-1) OF LOWEST LEVEL
C                           OF IONISING ION
C  OUTPUT: (I*4)  NLEVM   = NUMBER OF IONISING ION LEVELS WHICH ARE METASTABLES
C  OUTPUT: (I*4)  ILEVM() = CROSS-REFERENCING OF IONISNG ION METASTABLES
C                           TO IONISING ION LEVEL LIST.
C  OUTPUT: (I*4)  IA()    = IONISING ION ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCL./CONFIG. FOR RECOMBINED ION LEVEL
C                           'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR RECOMBINED LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR RECOMBINED LEVEL
C                           'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J) FOR RECOMBINED LEVEL
C                           'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WA()    = ENERGY RELATIVE TO RECOMBINED LEVEL 1 (CM-1)
C                           FOR RECOMBINED LEVEL 'IA()'
C  OUTPUT: (L*4)  LRAUG   = .TRUE.  => RESOLVED AUGER DATA PRESENT
C                           .FALSE. => RESOLVED AUGER DATA NOT PRESENT
C  OUTPUT: (I*4)  NPF     = NUMBER OF FINAL PARENTS WITH RESOLVED AUGER DATA
C  OUTPUT: (I*4)  INDF()  = INDICES OF FINAL PARENTS WITH RESOLVED AUGER DATA
C  OUTPUT: (R*8)  RAUG(,) = RESOLVED AUGER RATES
C                           1ST.DIM: IONISING ION LEVEL INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C  OUTPUT: (I*4)  IMETI() = INDEX OF METASTABLES IN IONISING ION LEVEL LIST
C  OUTPUT: (L*4)  LSYSM() = .TRUE.  => SPIN SYSTEMS LINKED TO METASTABLE
C                           .FALSE. => NO SPIN SYSTEMS LINKED TO METASTABLE
C  OUTPUT: (I*4)  NSYSM()  = NUMBER OF SPIN SYSTEMS LINKED TO METASTABLE
C  OUTPUT: (L*4)  LREPM() = .TRUE.  => REPR. LEVELS LINKED TO METASTABLE
C                           .FALSE. => NO REP. LEVELS LINKED TO METASTABLE
C  OUTPUT: (I*4)  NREPM()  = NUMBER OF REPR. LEVELS LINKED TO METASTABLE
C  OUTPUT: (L*4)  LIONIS()= .TRUE.  => STATE SELECTIVE IONIS. DATA PRESENT
C                           .FALSE. => STATE SELECTIVE IONIS. DATA NOT PRESENT
C                           1ST.DIM: INITIAL METASTABLE INDEX
C  OUTPUT: (L*4)  LRION(,)= .TRUE.  => DATA PRESENT FOR FINAL STATE
C                           .FALSE. => DATA NOT PRESENT FOR FINAL STATE
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C  OUTPUT: (R*8)  RION(,,)= STATE SELECTIVE DIRECT IONISATION COEFFICIENTS
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C                           3RD.DIM: ELECTRON TEMPERATURE INDEX 
C  OUTPUT: (L*4)  LEXCIT()= .TRUE.  => STATE SELECTIVE EXCIT. DATA PRESENT
C                           .FALSE. => STATE SELECTIVE EXCIT. DATA NOT PRESENT
C                           1ST.DIM: INITIAL METASTABLE INDEX
C  OUTPUT: (L*4)  LREXC(,)= .TRUE.  => DATA PRESENT FOR FINAL STATE
C                           .FALSE. => DATA NOT PRESENT FOR FINAL STATE
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL IONISING ION LEVEL INDEX
C  OUTPUT: (R*8)  REXC(,,)= STATE SELECTIVE DIRECT EXCITATION COEFFICIENTS
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: FINAL IONISING ION LEVEL INDEX
C                           3RD.DIM: ELECTRON TEMPERATURE INDEX 
C  OUTPUT: (I*4)  NPIS()  = INDEX OVER INTERMEDIATE PARENT & SPIN SYSTEM
C                           FOR BUNDLE-N DOUBLY EXCITED STATES
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C  OUTPUT: (I*4)  IPRTI(,)= INTERMEDIATE PARENT INDEX IN PARENT LIST
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (I*4)  ISYSI(,)= SPIN SYSTEM INDEX FOR INTERMEDIATE PARENT 
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (I*4)  ISPSYI(,)= SPIN OF SYSTEM FOR INTERMEDIATE PARENT
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (I*4)  IMETF(,)= FINAL PARENT INDICES TO WHICH INTERM. PARENT/
C                           SPIN SYSTEM/REPR. N-SHELL AUGERS 
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C                           3RD.DIM: FINAL IONISED ION METASTABLE INDEX 
C  OUTPUT: (I*4)  NVALS(,)= NUMBER OF FINAL PARENT S TO WHICH INTERM. 
C                           PARENT/SPIN SYSTEM/REPR. N-SHELL AUGERS
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (I*4)  NREPI(,)= NUMBER OF REPR. N-SHELLS FOR  INTERM. 
C                           PARENT/SPIN SYSTEM
C                           1ST.DIM: IONISING ION METASTABLE INDEX
C                           2ND.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (I*4)  IREP(,,)= REPR. N-SHELLS FOR INTERM. PARENT/SPIN SYSTEM/
C                           N-SHELL AUTOIONISING LEVELS
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (R*8)  AUGN(,,,)=AUGER RATES FOR INTERM. PARENT/SPIN SYSTEM/
C                           REPRESENTATIVE. N-SHELL
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (L*4)  LEXCN(,,)=.TRUE. => DATA PRESENT FOR REPR. N-SHELL
C                           .FALSE.=> DATA NOT PRESENT FOR REPR. N-SHELL
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C  OUTPUT: (R*8)  EXCN(,,,)=EXCITATION RATES TO AUTOIONISING PARENT/SPIN
C                           SYSTEM REPRESENTATIVE N-SHELL
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: IONISING ION METASTABLE INDEX
C                           3RD.DIM: INTERMEDIATE PARENT & SPIN SYSTEM
C                                    INDEX
C                           4TH DIM: TEMPERATURE INDEX
C  OUTPUT: (I*4)  NTE     = NUMBER OF ELECTRON TEMPERATURES
C  OUTPUT: (R*8)  TEA()   = ELECTRON TEMPERATURES (K)
C
C          (I*4)  IND     = GENERAL INDEX
C          (I*4)  INDX    = GENERAL INDEX
C          (I*4)  INDX1   = GENERAL INDEX
C          (I*4)  INDX2   = GENERAL INDEX
C          (I*4)  II      = GENERAL INDEX
C          (I*4)  I       = GENERAL INDEX
C          (I*4)  IPI     = GENERAL INDEX FOR INTERM. PARENT
C          (I*4)  IPF     = GENERAL INDEX FOR FINAL PARENT
C          (I*4)  IPIS    = GENERAL INDEX FOR INTERM. PARENT/SPIN SYSTEM
C          (I*4)  IR      = GENERAL INDEX FOR REPRESENTATIVE N-SHELLS
C          (I*4)  IT      = GENERAL INDEX
C          (I*4)  J       = GENERAL INDEX
C          (I*4)  K       = GENERAL INDEX
C
C          (L*4)  LDATA   = GENERAL READ/DO NOT READ FLAG
C
C          (C*18) C18     = GENERAL CHARACTER STRING
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4EIZ0     ADAS      RETURNS NUCL. CHARGE FROM ELEMENT SYMBOL
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C  DATE:    21/03/98
C
C  UPDATE:
C
C VERSION:	1.1						DATE: 11-06-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.         
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8    DZERO
C-----------------------------------------------------------------------
      PARAMETER( DZERO = 1.0D-60 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT      , I4EIZ0
      INTEGER   IUNIT       , NDPRT          , NDPRTI     , NDREP      ,
     &          NDLEV       , NDMET          , NDAUG      , NDT
      INTEGER   IZ          , IZ0            , IZ1        ,
     &          NPRNT       , NPRF           , NPRFM      , NPRI       ,
     &          NLEV        , NLEVM          , NPF        , NPFOLD     ,
     &          IL 
      INTEGER   INDX        , NTE
      INTEGER   ILINE
      INTEGER   I           , IABT           , IND        ,
     &          IPI         , IPIS           , IPF        , IP         ,
     &          IFIRST(1)   , ILAST(1)       , IWORD      , IM         ,
     &          IR          , J              ,
     &          NWORDS      , INDX1          , INDX2      ,
     &          IT 
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRT)  , ISPA(NDPRT)    , ILPA(NDPRT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IPRFM(NDPRT), ILEVM(NDLEV)   , IPRI(NDPRTI)
      INTEGER   INDF(NDPRT) 
      INTEGER   IMETI(NDMET), NSYSM(NDMET)   , NREPM(NDMET)
      INTEGER   IPRTI(NDMET,NDPRTI)          , ISYSI(NDMET,NDPRTI)     , 
     &          ISPSYI(NDMET,NDPRTI)
      INTEGER   IMETF(NDMET,NDPRTI,NDPRT)    , NVALS(NDMET,NDPRTI)
      INTEGER   NPIS(NDMET)                  , NREPI(NDMET,NDPRTI)     ,
     &          IREP(NDREP,NDMET,NDPRTI)   
C-----------------------------------------------------------------------
      REAL*8    R8FCTN
      REAL*8    BWNF        , BWNI
C-----------------------------------------------------------------------
      REAL*8    XJPA(NDPRT) , WPA(NDPRT)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)
      REAL*8    TEA(NDT)
      REAL*8    RAUG(NDLEV,NDPRT)  
      REAL*8    RION(NDMET,NDPRT,NDT) , REXC(NDMET,NDLEV,NDT)
      REAL*8    AUGN(NDREP,NDMET,NDPRTI,NDPRT)                         ,
     &          EXCN(NDREP,NDMET,NDPRTI,NDT)
C-----------------------------------------------------------------------
      CHARACTER SEQSYM*2
      CHARACTER C7*7        , CDELIM*7       , C18*18
      CHARACTER CLINE*80    , BUFFER*132
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRT)*(*) , CSTRGA(NDLEV)*(*)
C-----------------------------------------------------------------------
      LOGICAL   LDATA       , LRAUG       
C-----------------------------------------------------------------------
      LOGICAL   LSYSM(NDMET)       , LREPM(NDMET)
      LOGICAL   LIONIS(NDMET)      , LEXCIT(NDMET)
      LOGICAL   LRION(NDMET,NDPRT) , LREXC(NDMET,NDLEV)
      LOGICAL   LEXCN(NDREP,NDMET,NDPRTI)
C-----------------------------------------------------------------------
      DATA      CDELIM / ' ()<>{}' /
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C ZERO ARRAYS
C-----------------------------------------------------------------------
C
      DO 2 IPI=1,NDMET
        LSYSM(IPI)=.FALSE.
        LREPM(IPI)=.FALSE.
        LIONIS(IPI)=.FALSE.
        LEXCIT(IPI)=.FALSE.
        DO 1 J=1,NDPRT
           LRION(IPI,J)=.FALSE.
    1   CONTINUE
    2 CONTINUE
C
      DO 5 I=1,NDREP
        DO 4 IPI=1,NDMET
          DO 3 J=1,NDPRTI
            LEXCN(I,IPI,J)=.FALSE.
    3     CONTINUE
    4   CONTINUE
    5 CONTINUE
C
      DO 7 IPI=1,NDMET
        DO 6 J=1,NDLEV
          LREXC(IPI,J)=.FALSE.
    6   CONTINUE
    7 CONTINUE
C
C-----------------------------------------------------------------------
C INPUT ION SPECIFICATIONS.
C-----------------------------------------------------------------------
C
      READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'seq =')
      READ(BUFFER(INDX+7:INDX+8),'(A2)')SEQSYM
      INDX=INDEX(BUFFER,'nucchg =')
      READ(BUFFER(INDX+8:INDX+9),'(I2)')IZ0
C
      IZ=IZ0-I4EIZ0(SEQSYM)
      IZ1=IZ+1
C
C-----------------------------------------------------------------------
C LOCATE AND READ IONISED ION (PARENT) LEVEL DATA
C-----------------------------------------------------------------------
C
   10 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'nprf')
      IF(INDX.LE.0) THEN
          GO TO 10
      ENDIF
C
      INDX=INDEX(BUFFER,'bwnf =')
      READ(BUFFER(INDX+6:INDX+17),'(F12.1)') BWNF
      INDX=INDEX(BUFFER,'nprf =')
      READ(BUFFER(INDX+6:INDX+8),'(I3)') NPRF
      READ(IUNIT,'(A)')CLINE,CLINE,CLINE
C
      LDATA = .TRUE.
      NPRFM = 0
C
         DO 20 I=1,NDPRT
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I5)')  ILINE
C
C  PARENT LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     NPRNT=I-1
C
C  PARENT LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'PARENT LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (CLINE,1003) IPA(I)   , C18          ,
     &                                 ISPA(I) , ILPA(I)       ,
     &                                 C7      , BUFFER(1:44)
                     IF (INDEX(C18,'*').GT.0) THEN
                         NPRFM=NPRFM+1
                         IPRFM(NPRFM)=I
                     ELSEIF (INDEX(C18,'#').GT.0) THEN
                         NPRI=NPRI+1
                         IPRI(NPRI)=I
                     ENDIF
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:44) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
                     CSTRPA(I)=C18(1:18)
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJPA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'ADF23 DATA SET PARENT ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C
                     IF(BUFFER(ILAST(1):ILAST(1)).EQ.'*') THEN
                         ILAST(1)=ILAST(1)-1
                     ENDIF
                     READ(BUFFER(IFIRST(1):ILAST(1)),*) WPA(I)
C
                  ENDIF
            ENDIF
   20    CONTINUE
C         write(i4unit(-1),*)'nprnt=',nprnt
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF23 DATA SET CONTAINS > ',NDPRT,
     &                          ' PARENT LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  NPRNT = NDPRT
               ENDIF
         ENDIF
C
         IF((NPRNT.NE.NPRF).OR.(NPRFM.LE.0)) THEN
             WRITE(I4UNIT(-1),1001) 'FINAL PARENT ERROR'
             WRITE(I4UNIT(-1),1002)
         ENDIF
C
C-----------------------------------------------------------------------
C LOCATE AND READ IONISING ION  LEVEL DATA
C-----------------------------------------------------------------------
C
   30 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'nlev =')
      IF(INDX.LE.0) THEN
          GO TO 30
      ENDIF
C
      INDX=INDEX(BUFFER,'bwni =')
      READ(BUFFER(INDX+6:INDX+17),'(F12.1)') BWNI
      INDX=INDEX(BUFFER,'nlev =')
      READ(BUFFER(INDX+6:INDX+8),'(I3)') NLEV
      READ(IUNIT,'(A)')CLINE,CLINE,CLINE
C
      LDATA = .TRUE.
      NLEVM = 0
C
         DO 40 I=1,NDLEV
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I5)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (CLINE,1003) IA(I)   , C18          ,
     &                                 ISA(I)  , ILA(I)       ,
     &                                 C7      , BUFFER(1:44)
                     IF(INDEX(C18,'*').GT.0) THEN
                         NLEVM=NLEVM+1
                         ILEVM(NLEVM)=I
                     ENDIF
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:44) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
                     CSTRGA(I)=C18(1:18)
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'ADF23 DATA SET LEVEL ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C
                    READ(BUFFER(IFIRST(1):ILAST(1)),*) WA(I)
C
                  ENDIF
            ENDIF
   40    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF23 DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF
C
         IF((IL.NE.NLEV).OR.(NLEVM.LE.0))THEN
             WRITE(I4UNIT(-1),1001)'INITIAL LEVEL ERROR'
             WRITE(I4UNIT(-1),1002)
         ENDIF
C        write(i4unit(-1),*)'il=',il
C
C-----------------------------------------------------------------------
C LOCATE AND READ FINAL PARENT / INITIAL LEVEL RESOLVED AUGER RATE DATA
C-----------------------------------------------------------------------
C
      LRAUG = .FALSE.
      NPF = 0
   50 NPFOLD = NPF
      READ(IUNIT,'(A)') BUFFER
      IF((BUFFER(1:20).EQ.'--------------------').AND.
     &   (.NOT.LRAUG)) THEN
         WRITE(I4UNIT(-1),1007)'RESOLVED AUGER DATA'
         WRITE(I4UNIT(-1),1008)
         GO TO 60
      ELSEIF(BUFFER(1:20).EQ.'--------------------') THEN
C        write(i4unit(-1),*)'dddata: Exiting resolved  auger search'
         GO TO 60
      ENDIF
C
C     write(i4unit(-1),*)buffer
      INDX=INDEX(BUFFER,'indi')
C      write(i4unit(-1),*)'indx(indi)=',indx
C
      IF(INDX.LE.0) THEN
          GO TO 50
      ELSE
          READ(BUFFER(12:130),'(I8,11I10)')(INDF(NPFOLD+I),I=1,12)
          DO 53 IP=1,12
              IF(INDF(NPFOLD+12-IP+1).GT.0)THEN
                  NPF=NPFOLD+12-IP+1
                  LRAUG = .TRUE.
                  READ(IUNIT,'(A)')BUFFER
                  GO TO 54
              ENDIF
   53     CONTINUE
   54     READ(IUNIT,'(A)')BUFFER
   55     READ(IUNIT,'(A)')BUFFER
          READ(BUFFER(1:5),'(I5)')INDX1
          IF(INDX1.LE.0) THEN
              GO TO 50
          ELSE
              READ(BUFFER(10:130),'(12D10.2)')(RAUG(INDX1,IP),
     &             IP=NPFOLD+1,NPF)
              GO TO 55
          ENDIF
       ENDIF
   60  CONTINUE
C
C----------------------
C CHECKS ON ACQUISITION
C----------------------
C
C
C-----------------------------------------------------------------------
C LOCATE DATA BLOCK FOR EACH IONISING ION METASTABLE LEVEL
C-----------------------------------------------------------------------
C
   74   DO 150 IM=1,NLEVM
C
   75   READ(IUNIT,'(A)') BUFFER
        INDX=INDEX(BUFFER,'meti*=')
        IF(INDX.LE.0) THEN
            GO TO 75
        ENDIF
C
        READ(BUFFER(INDX+6:INDX+7),'(I2)') IMETI(IM)
C       write(i4unit(-1),*)'dddata: im,imeti=',im,imeti(im)
C
        LSYSM(IM) = .TRUE.
        INDX=INDEX(BUFFER,'nsys =')
        IF (INDX.LE.0) THEN
            LSYSM(IM)=.FALSE.
        ELSE
            READ(BUFFER(INDX+6:INDX+7),'(I2)') NSYSM(IM)
            IF(NSYSM(IM).LE.0) LSYSM(IM)=.FALSE.
        ENDIF
        IF(.NOT.LSYSM(IM)) THEN
           WRITE(I4UNIT(-1),1007)'SPIN SYSTEM DATA'
           WRITE(I4UNIT(-1),1008)
        ENDIF
C
        LREPM(IM) = .TRUE.
        INDX=INDEX(BUFFER,'nrep =')
        IF (INDX.LE.0) THEN
            LREPM(IM)=.FALSE.
        ELSE
            READ(BUFFER(INDX+6:INDX+7),'(i2)') NREPM(IM)
            IF(NREPM(IM).LE.0) LREPM(IM)=.FALSE.
        ENDIF
        IF(.NOT.LREPM(IM)) THEN
           WRITE(I4UNIT(-1),1007)'BUNDLE-N DATA'
           WRITE(I4UNIT(-1),1008)
        ENDIF
C
C       write(i4unit(-1),*)'dddata: im,imeti nsysm,nrepm=',
C    &                      im,imeti(im),nsysm(im),nrepm(im)
        LIONIS(IM) = .FALSE.
        LEXCIT(IM) = .FALSE.
  76    READ(IUNIT,'(A)')BUFFER
        IF(BUFFER(50:59).EQ.'----------') THEN
            GO TO 79
        ELSEIF(INDEX(BUFFER,'ionis rates').GT.0) THEN
            READ(IUNIT,'(A)')BUFFER,BUFFER
            J     = 1
            IWORD = 1
            CALL XXWORD( BUFFER(11:132), CDELIM    , IWORD  ,
     &                   J             ,
     &                   IFIRST(1)     , ILAST(1)  , NTE    )
            READ(BUFFER(11:132),'(E9.2,11E10.2)')(TEA(J),J=1,NTE)
            READ(IUNIT,'(A)')BUFFER
  77        READ(IUNIT,'(A)')BUFFER
            READ(BUFFER(1:5),'(I5)')INDX1
            IF(INDX1.LE.0) THEN
                GO TO 76
            ELSE
                READ(BUFFER(10:130),'(12D10.2)')(RION(IM,INDX1,IT),
     &               IT=1,NTE)
                DO 177 IT=1,NTE
                  IF(RION(IM,INDX1,IT).GT.0.0D0)THEN
                      LRION(IM,INDX1)=.TRUE.
                      LIONIS(IM) = .TRUE.
                      GO TO 77
                   ENDIF
  177           CONTINUE
                GO TO 77
            ENDIF
        ELSEIF(INDEX(BUFFER,'excit rates').GT.0) THEN
            READ(IUNIT,'(A)')BUFFER,BUFFER,BUFFER
  78        READ(IUNIT,'(A)')BUFFER
            READ(BUFFER(1:5),'(I5)')INDX1
            IF(INDX1.LE.0) THEN
                GO TO 76
            ELSE
                READ(BUFFER(10:130),'(12D10.2)')(REXC(IM,INDX1,IT),
     &               IT=1,NTE)
                DO 178 IT=1,NTE
                  IF(REXC(IM,INDX1,IT).GT.0.0D0)THEN
                      LREXC(IM,INDX1)=.TRUE.
                      LEXCIT(IM) = .TRUE.
                      GO TO 78
                   ENDIF
  178           CONTINUE
                GO TO 78
            ENDIF
        ELSE
            GO TO 76
        ENDIF
C
   79   CONTINUE
C           write(i4unit(-1),*)'dddata: resol. ionis, nte, lionis=',
C    &                          nte,lionis(im)
C           write(i4unit(-1),*)'dddata: resol. excit, nte, lexcit=',
C    &                          nte, lexcit(im)
C
C-----------------------------------------------------------------------
C LOCATE REPRESENTATIVE N-SHELL DATA BLOCK FOR EACH INTERMEDIATE PARENT
C-----------------------------------------------------------------------
C
        IPIS=0
   80   READ(IUNIT,'(A)',END=999)BUFFER
        IF(BUFFER(1:10).EQ.'C---------') GO TO 999
        INDX1=INDEX(BUFFER,'ipri#=')
        INDX2=INDEX(BUFFER,'meti*=')
        IF (INDX2.GT.0) THEN
            BACKSPACE(IUNIT)
             GOTO 150
          ENDIF 
        IF(INDX1.LE.0) THEN
            GO TO 80
        ENDIF
C
        IPIS=IPIS+1
        NPIS(IM)=IPIS
        READ(BUFFER(INDX1+6:INDX1+7),'(I2)') IPRTI(IM,IPIS)
        INDX1=INDEX(BUFFER,'sys  =')
        READ(BUFFER(INDX1+6:INDX1+7),'(I2)') ISYSI(IM,IPIS)
        INDX1=INDEX(BUFFER,'spnsy=')
        READ(BUFFER(INDX1+6:INDX1+7),'(I2)') ISPSYI(IM,IPIS)
C        write(i4unit(-1),*)'dddata: im,ipis,npis(im)=',
C     &                      im,ipis,npis(im)
C
C------------------------------------
C  GET REPR. N-SHELLS AND AUGER RATES
C------------------------------------
C
   90       READ(IUNIT,'(A)') BUFFER
            INDX=INDEX(BUFFER,'Auger rates')
            IF(INDX.LE.0) THEN
                GO TO 90
            ENDIF
C
            READ(IUNIT,'(A)')BUFFER,BUFFER
            READ(BUFFER(21:132),'(12I10)')
     &                   (IMETF(IM,IPIS,IPF),IPF=1,12)
C
            DO 110 IPF=1,12
                 IF (IMETF(IM,IPIS,IPF).LE.0) THEN
                    NVALS(IM,IPIS)=IPF-1
                    GO TO 120
                 ENDIF
  110       CONTINUE
  120       READ(IUNIT,'(A)')BUFFER
  125       READ(IUNIT,'(A)')BUFFER
            READ(BUFFER(1:6),'(I6)')IR
            IF(IR.GT.0) THEN
                 READ(BUFFER,'(I5,I6,8X,11D10.2)')IND,
     &           IREP(IND,IM,IPIS),
     &           (AUGN(IND,IM,IPIS,IPF),IPF=1,NVALS(IM,IPIS))
                 GO TO 125
            ENDIF
            NREPI(IM,IPIS)=IND
C           write(i4unit(-1),*)'dddata: im,ipis,nvals,nrepi=',
C    &                          im,ipis,nvals(im,ipis),nrepi(im,ipis)
C          
  130       READ(IUNIT,'(A)') BUFFER
            INDX=INDEX(BUFFER,'excit rates')
            IF(INDX.LE.0) THEN
                GO TO 130
            ENDIF
C
            READ(IUNIT,'(A)')BUFFER,BUFFER
            READ(BUFFER(11:132),'(E9.2,11E10.2)')(TEA(J),J=1,NTE)
            READ(IUNIT,'(A)')BUFFER
C 
  135       READ(IUNIT,'(A)')BUFFER
            READ(BUFFER(1:6),'(I6)')IR
            IF(IR.GT.0) THEN
                 READ(BUFFER,'(I5,4X,12D10.2)')IND,
     &           (EXCN(IND,IM,IPIS,IT),IT=1,NTE)
                 DO 136 IT=1,NTE
                    IF(EXCN(IND,IM,IPIS,IT).GT.0.0D0)THEN
                       LEXCN(IND,IM,IPIS)=.TRUE.
                       GO TO 135
                    ENDIF
  136            CONTINUE
                 GO TO 135
            ENDIF
C           write(i4unit(-1),*)'dddata: im,ipis,ind,excn=',
C    &                          im,ipis,ind,excn(ind,im,ipis,1)
C
            GO TO 80
C
  150  CONTINUE
C
C-----------------------------------------------------------------------
C
 1001 FORMAT(1X,30('*'),' DDDATA ERROR ',30('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I5,1X,1A18,1X,2(I1,1X),A7,A44)
 1004 FORMAT(I5)
 1005 FORMAT(I6,I6,9X,10E10.2)
 1006 FORMAT(I6,5X,10E10.2)
 1007 FORMAT(1X,30('*'),' DDDATA WARNING ',30('*')//
     &       1X,'INPUT DATAFILE LACKS: ',A)
 1008 FORMAT(/1X,29('*'),' PROGRAM CONTINUES ',28('*'))
 1012 FORMAT(1X,A,' - READ VALUE = ',A7)
C
C-----------------------------------------------------------------------
C
  999 CONTINUE
  
      RETURN
      END
