CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas413/ddispf.for,v 1.1 2004/07/06 13:29:34 whitefor Exp $ Date $Date: 2004/07/06 13:29:34 $
CX
      SUBROUTINE DDISPF( LPEND  , IZ     , IZ0     , IZ1     ,
     &                   NDT    , NDTIN   , NTE    , TVALS   ,
     &                   NDPRT  , NDPRTI  , NDREP   , NDLEV , NDMET  , 
     &                   NDAUG  , 
     &                   NPRF   , NPRFM   , IPRFM   , NPRI  , IPRI   ,
     &                   IPA    , CSTRPA  , ISPA    , ILPA  ,
     &                   NLEV   , NLEVM   , ILEVM   ,
     &                   IA     , CSTRGA  , ISA     , ILA   ,
     &                   LRAUG  , NPF     , INDF    , 
     &                   IMETI  , LSYSM   , NSYSM   , LREPM , NREPM  , 
     &                   LIONIS , LRION   , LEXCIT  , LREXC ,
     &                   NPIS   , IPRTI   , ISYSI   , ISPSYI,
     &                   IMETF  , NVALS   , NREPI   , IREP  ,
     &                   LEXCN  , TITLE   , IOPT    , 
     &                   IP_AUG , IM_AUG  , IP_RES , IL_RES  ,
     &                   IN_REP , IM_REP  , IPIS_REP, IPI_REP, ISS_REP,
     &                   ITTYP  , ITVAL   , TIN     ,
     &                   LOSEL  , LFSEL   , LGRD1   , LDEF1  ,
     &                   TOLVAL ,  XL1    , XU1     , YL1     , YU1 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DDISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS413
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C  INPUT : (I*4)   NDT      = MAX. NO. OF TEMPERATURES ALLOWED IN
C                             DATA SET
C  INPUT : (I*4)   NDTIN    = MAXIMUM NUMBER OF INPUT TEMPERATURES  
C                             ALLOWED.
C
C
C  INPUT : (I*4)   NTE      = INPUT FILE - NO. TEMPERATURES
C
C  INPUT : (R*8)   TVALS(,) = INPUT DATA FILE: TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   IOPT     = 1 => AUGER RATES PLOT
C                           = 2 => RESOLVED STATE SELECTIVE DR PLOT
C                           = 3 => REPRESENTATIVE BUNDLE-N DR PLOT
C
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)   ITVAL    = NUMBER OF INPUT TEMPERATURE PAIRS (1->20)
C  OUTPUT: (R*8)   TIN()    = USER ENTERED VALUES -
C                             TEMPERATURES (UNITS: SEE 'ITTYP')
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  CALLS XXFLSH COMMAND TO CLEAR PIPE
C
C AUTHOR:  H. P. Summers, University of Strathclyde
C          JA8.08
C          Tel.  0141-553-4196
C
C DATE:    25/03/98
C
C UPDATE:  
C
C VERSION:	1.1						DATE: 11-06-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
      integer   itprt      , itprti     , itrep      , itlev
      integer   itmet      , itaug
C-----------------------------------------------------------------------
      parameter ( itprt=40 , itprti=10 , itlev=20 , itrep=100 )
      parameter ( itmet=5  , itaug=8 )
C-----------------------------------------------------------------------
      INTEGER    NDPRT       , NDPRTI         , NDREP          ,
     &           NDLEV       , NDMET          , NDAUG          , NDT   ,
     &           NDTIN   
      INTEGER    IZ          , IZ0            , IZ1            ,
     &           NPRF        , NPRFM          , NPRI           , NLEV  ,
     &           NLEVM       , NPF            , ICOUNT
C-----------------------------------------------------------------------
      INTEGER    ITTYP   , ITVAL  , IOPT    ,
     &           IP_AUG  , IM_AUG , 
     &           IP_RES  , IL_RES ,
     &           IPS_REP , IN_REP , IM_REP  , IPIS_REP  , IPI_REP , 
     &           ISS_REP   
      INTEGER    krep
C-----------------------------------------------------------------------
      REAL*8     TOLVAL        ,
     &           XL1           , XU1           , YL1          , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND         ,
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    NTE  
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)
      REAL*8     TVALS(NDT,3)
C-----------------------------------------------------------------------
      character cprf(itprt)*21        , clev(itlev)*21
      character crep(itmet*itprti)*56 
      character cnrep(itrep,itmet*itprti)*6
      character cstrl*1
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRT)  , ISPA(NDPRT)    , ILPA(NDPRT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IPRFM(NDPRT), IPRI(NDPRTI)   , ILEVM(NDLEV)
      INTEGER   INDF(NDPRT)
      INTEGER   IMETI(NDMET), NSYSM(NDMET)   , NREPM(NDMET)
      INTEGER   IPRTI(NDMET,NDPRTI)          , ISYSI(NDMET,NDPRTI)     , 
     &          ISPSYI(NDMET,NDPRTI)
      INTEGER   IMETF(NDMET,NDPRTI,NDPRT)    , NVALS(NDMET,NDPRTI)
      INTEGER   NPIS(NDMET)                  , NREPI(NDMET,NDPRTI)     ,
     &          IREP(NDREP,NDMET,NDPRTI)
C 
      INTEGER   IRION(ITMET,ITPRT) , IREXC(ITMET,ITLEV)
      INTEGER   IIONIS(ITMET)      , IEXCIT(ITMET)
      INTEGER   NRCNT(ITMET*ITPRTI)
      INTEGER   IEXCN(ITREP,ITMET*ITPRTI)
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRT)*(*) , CSTRGA(NDLEV)*(*)
C-----------------------------------------------------------------------
      LOGICAL   LRAUG
C-----------------------------------------------------------------------
      LOGICAL   LSYSM(NDMET)       , LREPM(NDMET)
      LOGICAL   LIONIS(NDMET)      , LEXCIT(NDMET)
      LOGICAL   LRION(NDMET,NDPRT) , LREXC(NDMET,NDLEV)
      LOGICAL   LEXCN(NDREP,NDMET,NDPRTI)
C-----------------------------------------------------------------------
      INTEGER    PIPEIN , PIPEOU   ,   
     &           I      , J        , K ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
C CHECK INTERNAL DIMENSION CONSISTENCY
C-----------------------------------------------------------------------
      IF (ITPRT.LT.NDPRT)
     &         STOP ' DDISPF ERROR: ITPRT < NDPRT - INCREASE NDPRT'
      IF (ITPRTI.LT.NDPRTI)
     &         STOP ' DDISPF ERROR: ITPRTI < NDPRTI - INCREASE NDPRTI'
      IF (ITREP.LT.NDREP)
     &             STOP ' DDISPF ERROR: ITREP < NDREP - INCREASE NDREP'
      IF (ITLEV.LT.NDLEV)
     &             STOP ' DDISPF ERROR: ITLEV < NDLEV - INCREASE NDLEV'
      IF (ITMET.LT.NDMET)
     &             STOP ' DDISPF ERROR: ITMET < NDMET - INCREASE NDMET'
      IF (ITAUG.LT.NDAUG)
     &             STOP ' DDISPF ERROR: ITAUG < NDAUG - INCREASE NDAUG'

C-----------------------------------------------------------------------
C CONSTRUCT RESOLVED IONIS/EXCIT STRINGS OF INFORMATION FOR SELECTION
C-----------------------------------------------------------------------

	do i = 1,nprf
	  write(cprf(ipa(i)),100)cstrpa(ipa(i)),ispa(ipa(i)),
     &	  cstrl(ilpa(ipa(i)))
       end do
       
       do i = 1,nlev
         write(clev(ia(i)),100)cstrga(ia(i)),isa(ia(i)),
     &	 cstrl(ila(ia(i)))
       end do
       
 100   format(a18,i2,a1)
     
C make an integer array of lionis for IDL

       do i = 1,nlevm
         if (lionis(i)) then
            iionis(i) = 1
         else
            iionis(i) = 0
         endif
       end do

C make an integer array of lrion for IDL

       do i = 1,nprf
         do j = 1,nlevm
           if (lrion(j,i)) then
              irion(j,i) = 1
           else
              irion(j,i) = 0
           endif
         end do
       end do
      
C make an integer array of lexcit for IDL

       do i = 1,nlevm
         if (lexcit(i)) then
            iexcit(i) = 1
         else
            iexcit(i) = 0
         endif
       end do

C make an integer array of lrexc for IDL

       do i = 1,nlev
         do j = 1,nlevm
           if (lrexc(j,i)) then
              irexc(j,i) = 1
           else
              irexc(j,i) = 0
           endif
         end do
       end do
      
C now representative n-shell

	krep=1
	do i=1,nlevm               ! initial ionising ion metastable index
	  do j= 1,npis(i)          ! interm. oarent & spin system index
               nrcnt(krep)=nrepi(i,j)
C	    
 	       write(crep(krep),101)clev(imeti(i))     ,
     &	                            cprf(iprti(i,j))   ,
     & 	                            isysi(i,j)         ,
     &                              ispsyi(i,j)
C 	       
 	       do k=1,nrepi(i,j)
C
         	 write(cnrep(k,krep),102)irep(k,i,j)
C
 	         if (lexcn(k,i,j)) then
                    iexcn(k,krep) = 1
                 else
                    iexcn(k,krep) = 0
                 endif
C
	       end do
C 	        	  
 	       krep=krep+1
C 	       
	  end do
	end do
	krep = krep-1
C	
  101   format(a21,5x,a21,3x,i2,'(',i2,')')
  102   format('n= ',i3)
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C
C start with ion charges
C----------------------------------------------------------------------- 
	WRITE(PIPEOU,*) iz
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) iz0
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) iz1
	CALL XXFLSH(PIPEOU)

C now temperature information

	WRITE(PIPEOU,*) NDT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NTE
	CALL XXFLSH(PIPEOU)

C resolved ionis/excit

	WRITE(PIPEOU,*) NDPRT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDPRTI
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDMET
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDLEV
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDREP
	CALL XXFLSH(PIPEOU)

	WRITE(PIPEOU,*) NPRF
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NPRFM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NPRI
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NLEV
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NLEVM
	CALL XXFLSH(PIPEOU)

        DO I=1,NPRFM
          WRITE(PIPEOU,*) IPRFM(I)
          CALL XXFLSH(PIPEOU)
        END DO

        DO I=1,NPRI
          WRITE(PIPEOU,*) IPRI(I)
          CALL XXFLSH(PIPEOU)
        END DO

        DO I=1,NLEVM
          WRITE(PIPEOU,*) IMETI(I)
          CALL XXFLSH(PIPEOU)
        END DO


C representative n-shell excitation

	WRITE(PIPEOU,*) NDMET*NDPRTI
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) KREP
	CALL XXFLSH(PIPEOU)


        DO  J=1,3
          DO I=1, NTE
            WRITE(PIPEOU,'(E12.4)') TVALS(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO

C cprf and clev will be output in increasing order

	DO I=1, NPRF
          WRITE(PIPEOU,'(A21)') CPRF(I)
          CALL XXFLSH(PIPEOU)
        END DO

	DO I=1, NLEV
          WRITE(PIPEOU,'(A21)') CLEV(I)
          CALL XXFLSH(PIPEOU)
        END DO

        DO  J=1,NPRF
          DO I=1, NLEVM
            WRITE(PIPEOU,'(I1)') IRION(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO

        DO  J=1,NLEV
          DO I=1, NLEVM
            WRITE(PIPEOU,'(I1)') IREXC(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO

	DO I=1, KREP
          WRITE(PIPEOU,'(A56)') CREP(I)
          CALL XXFLSH(PIPEOU)
        END DO

	DO I=1, KREP
          WRITE(PIPEOU,*) NRCNT(I)
          CALL XXFLSH(PIPEOU)
        END DO
C
        DO  J=1,KREP
	  DO I=1, NRCNT(J)
            WRITE(PIPEOU,'(A6)') CNREP(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO
C
        DO  J=1,KREP
          DO I=1, NRCNT(J)
            WRITE(PIPEOU,'(I1)') IEXCN(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE FROM IDL
C-----------------------------------------------------------------------
C 
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
 
	READ(PIPEIN,'(A40)') TITLE
 
	READ(PIPEIN,*) ITTYP
	READ(PIPEIN,*) IOPT
	READ(PIPEIN,*) IP_AUG 
	READ(PIPEIN,*) IM_AUG 
	READ(PIPEIN,*) IP_RES 
	READ(PIPEIN,*) IL_RES 
	READ(PIPEIN,*) IPS_REP 
	READ(PIPEIN,*) IN_REP 
	

	READ(PIPEIN,*) ITVAL
	READ(PIPEIN,*) (TIN(I), I=1,itval)

	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 1) THEN
	   LFSEL = .TRUE.
	ELSE
	   LFSEL = .FALSE.
	END IF

	READ(PIPEIN,*) TOLVAL
	TOLVAL = TOLVAL / 100.0


C convert from IDL 0 index to fortran

	IP_AUG  = IP_AUG  + 1     		
	IM_AUG  = IM_AUG  + 1     		
	IP_RES  = IP_RES  + 1     	
	IL_RES  = IL_RES  + 1
	IPS_REP = IPS_REP + 1    
	IN_REP  = IN_REP  + 1

C locate initial metastable and interm parent for repr. level case

        im_rep=0
        ipis_rep=0
        ipi_rep=0
        iss_rep=0
C
	if (krep.gt.0) then
            icount=1
	    do i=1,nlevm         ! initial ionising ion metastable index
	      do j= 1,npis(i)    ! interm. oarent & spin system index
                if(icount.eq.ips_rep) then
                    im_rep=imeti(i)
                    ipis_rep=j
                    ipi_rep=iprti(i,j)
                    iss_rep=ispsyi(i,j)
                endif 
 	        icount=icount+1
	      end do
	    end do
	endif
C
C-----------------------------------------------------------------------
C


      RETURN
      END
