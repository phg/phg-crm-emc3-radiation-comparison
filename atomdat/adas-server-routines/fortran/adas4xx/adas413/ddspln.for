CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas413/ddspln.for,v 1.1 2004/07/06 13:30:00 whitefor Exp $ Date $Date: 2004/07/06 13:30:00 $
CX
      SUBROUTINE DDSPLN( NTDIM  , NDTIN  ,
     &                   ITA    , ITVAL  ,
     &                   TFILE  , TEVA   ,
     &                   QDRIN  , QDROUT ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTFILEE: DDSPLN *********************
C
C  PURPOSE:
C         1) PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE) VERSUS LOG(QDRIN)
C            INPUT DATA. ('TFILE' VERSUS 'QDRIN' , ITA DATA PAIRS)
C
C         2) INTERPOLATES 'ITVAL'  QDRIN VALUES USING ABOVE SPLINES AT
C            TEMPERATURES READ IN FROM ISPF PANELS FOR TABULAR OUTPUT.
C            (ANY TEMPERATURE VALUES WHICH REQUIRED EXTRAPOLATION TO
C             TAKE PLACE ARE SET TO ZERO).
C
C         3) INTERPOLATES 'ITVAL' QDRIN VALUES USING ABOVE SPLINES AT
C            TEMPERATURES EQUI-DISTANCE ON RANGE OF LOG(TEMPERATURES)
C            STORED IN INPUT 'TFILE' ARRAY.
C
C  CALLING PROGRAM: ADAS413
C
C
C  SUBROUTFILEE:
C
C  INPUT : (I*4)  NTDIM   = MAX. NO. OF TEMPERATURES ALLOWED IN
C                           DATA SET
C  INPUT : (I*4)  NDTIN   = MAX. NO. OF USER TEMPERATURES ALLOWED 
C  INPUT : (I*4)  ITA     = INPUT DATA FILE: NUMBER OF DR/TEMPERATURE
C                           PAIRS READ FOR THE TRANSITION BEING ASSESSED
C  INPUT : (I*4)  ITVAL   = NUMBER OF  SPLINE  INTERPOLATED  QDRIN/TEMP.
C                           REQUIRED FOR GRAPHICAL DISPLAY.
C
C  INPUT : (I*4)  TFILE() = INPUT DATA FILE: TEMPERATURES 
C  INPUT : (I*4)  TEVA()  = ISPF PANEL ENTERED TEMPERATURES 
C
C  INPUT : (R*8)  QDRIN() = INPUT DATA FILE: SELECTED TRANSITION -
C                           QDRIN VALUES AT 'TFILE()'.
C  OUTPUT: (I*4)  QDROUT()= SPLINE INTERPOLATED QDRIN VALUES AT 'TEVA()'
C                           (EXTRAPOLATED VALUES = 0.0).
C
C  OUTPUT: (L*4)  LTRNG() = .TRUE.  => OUTPUT   SPLINE    VALUE    WAS
C                                      INTERPOLATED FOR 'DLOG(TEVA()'.
C                           .FALSE. => OUTPUT   SPLINE    VALUE    WAS
C                                      EXTRAPOLATED FOR 'DLOG(TEVA()'.
C                                      (NOTE: 'YOUT()=0' AS 'IOPT < 0').
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF  INPUT  TEMP/QDRIN
C                                      PAIRS MUST BE >= 'ITA'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF 'OUTPUT TEMP/QDRIN
C                                      PAIRS MUST BE >= 'ITVAL' & 'ITVAL'
C
C          (I*4)  IARR    = ARRAY SUBSCRIPT USED FOR TEMP/QDRIN PAIRS
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTFILEE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATFILEG
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATFILEG TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  XIN()   = LOG( 'TFILE()' )
C          (R*8)  YIN()   = LOG( 'QDRIN()' )
C          (R*8)  XOUT()  = LOG(TEMPERATURES AT WHICH SPLINES REQUIRED)
C          (R*8)  YOUT()  = LOG(OUTPUT SPLINE INTERPOLATED QDRIN VALUES)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C ROUTFILEES:
C          ROUTFILEE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTFILEE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H. P. Summers, University of Strathclyde
C          JA8.08
C          Tel. 0141-553-4196
C
C
C DATE:    30/01/98
C 
C UPDATE:  
C
C VERSION:	1.1						DATE: 11-06-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C	
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN          , NOUT     , NTDIM     , NDTIN
C-----------------------------------------------------------------------
      PARAMETER( NIN = 14     , NOUT = 100    )
C-----------------------------------------------------------------------
      INTEGER    ITA          , ITVAL
      INTEGER    IOPT         , IARR
C-----------------------------------------------------------------------
      REAL*8     R8FUN1       
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     TFILE(NTDIM) , QDRIN(NTDIM)    ,
     &           TEVA(NDTIN)  , QDROUT(NDTIN)   
      REAL*8     XIN(NIN)     , YIN(NIN)      ,
     &           XOUT(NOUT)   , YOUT(NOUT)
      REAL*8     DF(NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN) 
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.ITA)
     &             STOP ' DDSPLN ERROR: NIN < ITA - INCREASE NIN'
      IF (NOUT.LT.ITVAL)
     &             STOP ' DDSPLN ERROR: NOUT < ITVAL - INCREASE NGOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH OFF EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = 0
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT QDRIN/TEMP PAIRS
C-----------------------------------------------------------------------
C
         DO 1 IARR=1,ITA
            XIN(IARR) = DLOG( TFILE(IARR) )
            YIN(IARR) = DLOG( QDRIN(IARR) )
    1    CONTINUE
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED TEMP/QDRIN PAIRS FOR TABULAR OUTPUT - IF REQUESTED
C-----------------------------------------------------------------------
C
C
         DO 2 IARR=1,ITVAL
           XOUT(IARR) = DLOG(TEVA(IARR))
    2    CONTINUE
C
         CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &                ITA   , XIN    , YIN    ,
     &                ITVAL , XOUT   , YOUT   ,
     &                DF    , LTRNG
     &              )
C
	 DO IARR=1,ITVAL
           QDROUT(IARR) = DEXP( YOUT(IARR) )
	 END DO

c         DO 3 IARR=1,ITVAL
c            IF (LTRNG(IARR)) THEN
c               QDROUT(IARR) = DEXP( YOUT(IARR) )
c            ELSE
c               QDROUT(IARR) = 0.0
c            ENDIF
c    3    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
