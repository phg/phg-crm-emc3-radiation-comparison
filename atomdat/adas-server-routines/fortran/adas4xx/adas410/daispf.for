CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/daispf.for,v 1.2 2004/07/06 13:27:22 whitefor Exp $ Date $Date: 2004/07/06 13:27:22 $
CX
      SUBROUTINE DAISPF( LPEND  ,
     &                   IZ     , IZ0     , IZ1     ,
     &                   NTDIM  , NDTIN   , ITA     , TVALS   ,
     &                   ndaug  , iaprs   , ipaug   , lauga   ,
     &			 NDPRNT , NDLEV   ,
     &                   NPRNT  , IPA     , CSTRPA  , ISPA    , ILPA ,
     &                   IL     , IA      , CSTRGA  , ISA     , ILA  ,
     &			 LDIELR ,
     &                   nprnti , nprntf  , nsysf   ,
     &                   iprti  , iprtf   , isys    , ispsys  , 
     &                   ndrep  , nrep    , irepa   , nrepa   ,
     &                   ldieln , 
     &                   TITLE  , IOPT    , 
     &                   IP_AUG , IP_RES  , IL_RES  ,
     &                   IN_REP , IPI_REP , IPF_REP , ISP_REP ,
     &                   ITTYP  , ITVAL   , TIN     ,
     &                   LOSEL  , LFSEL   , LGRD1   , LDEF1   ,
     &                   TOLVAL , 
     &                   XL1    , XU1     , YL1     , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DAISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS410
C
C
C Rather than passing all information such as parent spin, L-value, spin
C system etc. to IDL this subroutine constructs arrays of strings for
C the 3 classes of data in adf09 files
C   i. Auger rates - 
C      all possible initial and final parents (config + term for both)
C  ii. Resolved DR rates
C      all possible initial parents (configuration and term)
C      the resolved levels in the adf09 dataset (config + term) corresponding
C      to the parents.
C iii. Representative DR
C      all possible parent/spin systems (initial parent, final parent, spin sys)
C      representative n-shell data 
C      total data extrapolated from the representative n-shell data
C
C Integer arrays for the existance of the data are also passed to IDL. These
C are used to ensure that non-existant cases are not selected. The index of 
C these string arrays, that the user has chosen, is passed back from IDL.
C This subroutine then passes back to the calling routine the indices in the
C appropriate data arrays corresponding to these string array indices. 
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C  INPUT : (I*4)   NTDIM    = MAX. NO. OF TEMPERATURES ALLOWED IN
C                             DATA SET
C  INPUT : (I*4)   NDTIN    = MAXIMUM NUMBER OF INPUT TEMPERATURES  
C                             ALLOWED.
C
C
C  INPUT : (I*4)   ITA      = INPUT FILE - NO. TEMPERATURES
C
C  INPUT : (R*8)   TVALS(,) = INPUT DATA FILE: TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   IOPT     = 1 => AUGER RATES PLOT
C                           = 2 => RESOLVED STATE SELECTIVE DR PLOT
C                           = 3 => REPRESENTATIVE BUNDLE-N DR PLOT
C
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)   ITVAL    = NUMBER OF INPUT TEMPERATURE PAIRS (1->20)
C  OUTPUT: (R*8)   TIN()    = USER ENTERED VALUES -
C                             TEMPERATURES (UNITS: SEE 'ITTYP')
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  CALLS XXFLSH COMMAND TO CLEAR PIPE
C
C AUTHOR:  Martin O'Mullane
C          K1/1/43
C          JET EXT. 5313
C
C DATE:    11/09/97
C
C UPDATE:  27/10/97  HP Summers - corrected internal array dimensioning
C                                 by putting limits as parameters
C
C 
C VERSION: 1.1
C
C VERSION: 1.2						DATE: 20/02/98
C MODIFIED: MARTIN O'MULLANE AND RICHARD MARTIN
C               - MODIFIED DAISPF CALL SUCH THAT THE CALL SPANS LESS
C                 THAN 20 LINES (ALLOWS COMPILATION AT GARCHING).
C               - ADDED TOTAL DR RATE TO REPRESENTATIVE LEVEL SET IN THE
C                 FIRST POSITION.
C
C-----------------------------------------------------------------------
      integer   itprnt      , itlev     , itrep      , itaug
C-----------------------------------------------------------------------
      parameter ( itprnt=10 , itrep=100 , itlev=1000, itaug=8)
C-----------------------------------------------------------------------
      INTEGER    NDTIN   , NTDIM  , NDREP   ,
     &           ITTYP   , ITVAL  , IOPT    ,
     &           IP_AUG  , 
     &           IP_RES  , IL_RES ,
     &           IPS_REP , IN_REP , IPI_REP , IPF_REP, ISP_REP , 
     &           i4unit
      INTEGER    nrep   , nprnti , nprntf ,
     &           krep   , ipi    , ipf    , ipfs , ktest
      integer    IZ     , IZ0    , IZ1    
C-----------------------------------------------------------------------
      REAL*8     TOLVAL       ,
     &           XL1           , XU1           , YL1          , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , LDFIT         ,
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    ITA           
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)
      REAL*8     TVALS(NTDIM,3)
C-----------------------------------------------------------------------
      character cprnt(itprnt)*21        , clev(itlev)*21
      character crep(itprnt*itprnt)*56  , cnrep(itrep+1)*6
      character cstrl*1
      character caug(itaug)*47
C-----------------------------------------------------------------------
      INTEGER   NDAUG       , IAPRS     
      INTEGER   NDPRNT      , NDLEV     
      INTEGER   NPRNT       , IL         
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRNT) , ISPA(NDPRNT)   , ILPA(NDPRNT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IPAUG(NDAUG,2)
      
      integer   iaug(itrep,itaug)
      integer   idielr(itlev,itprnt)
      integer   idieln(itrep+1,itprnt*itprnt)
      integer   isum(itprnt*itprnt)

      INTEGER   IREPA(NDREP)  , NREPA(NDREP)   
      INTEGER   IPRTI(NDPRNT) ,IPRTF(NDPRNT,NDPRNT) 
      INTEGER   NSYSF(NDPRNT,NDPRNT) , ISYS(NDPRNT,NDPRNT,2)
      INTEGER   ISPSYS(NDPRNT,NDPRNT,2)
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRNT)*(*) , CSTRGA(NDLEV)*(*)
C-----------------------------------------------------------------------
      LOGICAL   LDIELR(NDLEV,NDPRNT) , ldieln(ndrep,ndprnt,ndprnt,2)
      LOGICAL   LAUGA(NDREP,NDAUG)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
      INTEGER    PIPEIN , PIPEOU   ,
     &           I      , J        , K ,
     &           ILOGIC , izero
      PARAMETER  (PIPEIN = 5, PIPEOU = 6, izero = 0)
C-----------------------------------------------------------------------
C Construct strings of info for selection
C 
C resolved DR

	do i = 1,nprnt
	  write(cprnt(ipa(i)),100)cstrpa(ipa(i)),ispa(ipa(i)),
     &	  cstrl(ilpa(ipa(i)))
       end do
       
       do i = 1,il
         write(clev(ia(i)),100)cstrga(ia(i)),isa(ia(i)),
     &	 cstrl(ila(ia(i)))
       end do
       
 100   format(a18,i2,a1)     

C make an integer array of ldielr for IDL

       do i = 1,nprnt
         do j = 1,il
           if (ldielr(j,i)) then
              idielr(j,i) = 1
           else
              idielr(j,i) = 0
           endif
         end do
       end do
      
C now representative n-shell

	do i=1,ndprnt*ndprnt
 	   isum(i) = 0                      ! total DR logical 
        end do
        krep = 1	 
	do ipi=1,nprnti               ! initial parent
	  do ipf= 1,nprntf            ! final parent
	    do ipfs= 1,nsysf(ipi,ipf) ! no. spin systems built on final  parent
	    
 	       write(crep(krep),101)cprnt(iprti(ipi))     ,
     &	                            cprnt(iprtf(ipi,ipf)) ,
     & 	                            isys(ipi,ipf,ipfs)    ,
     &                              ispsys(ipi,ipf,ipfs)
 	       
 	       do i=1,nrep
 	         if (ldieln(i,ipi,ipf,ipfs)) then
                    idieln(i+1,krep) = 1
                 else
                    idieln(i+1,krep) = 0
                 endif
                 isum(krep) = isum(krep) + idieln(i+1,krep)
 	       end do

 	       krep=krep+1
 	       
	    end do
	  end do
	end do
	krep = krep-1

	do i=1,krep
          if (isum(i).ge.1) then
             idieln(1,i)=1
          else
             idieln(1,i)=0
          endif
       end do
        
	
 101    format(a21,5x,a21,3x,i2,'(',i2,')')

C Use position 1 to store total DR rate

	write(cnrep(irepa(1)),'(A)')'Total '
	do i=1,nrep
	  write(cnrep(irepa(i)+1),102)nrepa(i)
	end do

 102    format('n= ',i3)


C Auger rates

	do k=1,iaprs  
	        
 	   write(caug(k),103)cprnt(ipaug(k,1)),cprnt(ipaug(k,2))
 	   
 	   do i=1,nrep
 	     if (lauga(i,k)) then
                iaug(i,k) = 1
             else
                iaug(i,k) = 0
             endif

 	   end do
 	   
	end do
	 
 103    format(a21,5x,a21)
    
      
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C
C start with ion charges
 
	WRITE(PIPEOU,*) iz
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) iz0
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) iz1
	CALL XXFLSH(PIPEOU)

C now temperature information

	WRITE(PIPEOU,*) NTDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) ITA
	CALL XXFLSH(PIPEOU)

C Auger rates

	WRITE(PIPEOU,*) NDAUG
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) IAPRS
	CALL XXFLSH(PIPEOU)


C resolved DR

	WRITE(PIPEOU,*) NDPRNT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDLEV
	CALL XXFLSH(PIPEOU)

	WRITE(PIPEOU,*) NPRNT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) IL
	CALL XXFLSH(PIPEOU)

C representative DR

	WRITE(PIPEOU,*) NDPRNT*NDPRNT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) KREP
	CALL XXFLSH(PIPEOU)

	WRITE(PIPEOU,*) NDREP
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NREP
	CALL XXFLSH(PIPEOU)


        DO  J=1,3
          DO I=1, ITA
            WRITE(PIPEOU,'(E12.4)') TVALS(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO

C cprnt and clev will be output in increasing order

	if (iaprs.ne.0) then 
	   DO I=1, IAPRS
             WRITE(PIPEOU,'(A47)') CAUG(I)
             CALL XXFLSH(PIPEOU)
           END DO

           DO  J=1,IAPRS
             DO I=1, NREP
               WRITE(PIPEOU,'(i1)') iaug(I,J)
               CALL XXFLSH(PIPEOU)
             END DO
           END DO
        else
           WRITE(PIPEOU,'(A47)') 'No Auger data'  
           CALL XXFLSH(PIPEOU)
           DO I=1, NREP
             WRITE(PIPEOU,'(i1)')izero
             CALL XXFLSH(PIPEOU)
           END DO
        endif



	DO I=1, NPRNT
          WRITE(PIPEOU,'(A21)') CPRNT(I)
          CALL XXFLSH(PIPEOU)
        END DO

	DO I=1, IL
          WRITE(PIPEOU,'(A21)') CLEV(I)
          CALL XXFLSH(PIPEOU)
        END DO

        DO  J=1,nprnt
          DO I=1, il
            WRITE(PIPEOU,'(i1)') idielr(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO


	DO I=1, KREP
          WRITE(PIPEOU,'(A56)') CREP(I)
          CALL XXFLSH(PIPEOU)
        END DO

	DO I=1, NREP + 1
          WRITE(PIPEOU,'(A6)') CNREP(I)
          CALL XXFLSH(PIPEOU)
        END DO

        DO  J=1,KREP
          DO I=1, NREP + 1
            WRITE(PIPEOU,'(i1)') idieln(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO

C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE FROM IDL
 
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
 
	READ(PIPEIN,'(A40)') TITLE
 
	READ(PIPEIN,*) ITTYP
	READ(PIPEIN,*) IOPT
	READ(PIPEIN,*) IP_AUG 
	READ(PIPEIN,*) IP_RES 
	READ(PIPEIN,*) IL_RES 
	READ(PIPEIN,*) IPS_REP 
	READ(PIPEIN,*) IN_REP 
	

	if (iopt.ne.0) then
	
	   READ(PIPEIN,*) ITVAL
	   READ(PIPEIN,*) (TIN(I), I=1,itval)

	   READ(PIPEIN,*) ILOGIC
	   IF (ILOGIC .EQ. 1) THEN
	      LFSEL = .TRUE.
	   ELSE
	      LFSEL = .FALSE.
	   END IF

	   READ(PIPEIN,*) TOLVAL
	   TOLVAL = TOLVAL / 100.0

	endif

C convert from IDL 0 index to fortran

	IP_AUG  = IP_AUG  + 1     		
	IP_RES  = IP_RES  + 1     	
	IL_RES  = IL_RES  + 1
	IPS_REP = IPS_REP + 1    
	IN_REP  = IN_REP  + 1

C convert ips_rep into initial and final parent and spin system indices

	ktest = 1
	do ipi=1,nprnti               ! initial parent
	  do ipf= 1,nprntf            ! final parent
	    do ipfs= 1,nsysf(ipi,ipf) ! no. spin systems built on final  parent
	    
	     if (ktest.eq.ips_rep) then
	        ipi_rep = ipi
	        ipf_rep = ipf
	        isp_rep = ipfs
	        goto 200
	     endif
 	     
 	     ktest = ktest + 1
 	       
	    end do
	  end do
	end do

 200    continue
C
C-----------------------------------------------------------------------
C


      RETURN
      END
