CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/daspf0.for,v 1.1 2004/07/06 13:27:49 whitefor Exp $ Date $Date: 2004/07/06 13:27:49 $
CX
      SUBROUTINE DASPF0( REP    , DSFULL , LDSEL )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: E5SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: adas410
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME
C
C  OUTPUT: (L*4)   LDSEL   = .TRUE.  => IONATOM DATA SET INFORMATION
C                                       TO BE DISPLAYED BEFORE RUN.
C                          = .FALSE. => IONATOM DATA SET INFORMATION
C                                       NOT TO BE DISPLAYED BEFORE RUN.
C
C          (I*4)   PIPEIN  = UNIT NO. FOR OUTPUT TO PIPE
C	   (I*4)   PIPEOU  = UNIT NO. FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  Martin O'Mullane
C          K1/1/45
C          JET EXT. 5313
C
C DATE:    09/09/97
C
C UPDATE:  27/10/97  HP Summers - corrected length of DSFULL to *120
C VERSION:
C MODIFIED: 
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSFULL*120
C-----------------------------------------------------------------------
      LOGICAL     LDSEL
C-----------------------------------------------------------------------
      INTEGER PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------

C READ FROM IDL PIPE

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL


C
C-----------------------------------------------------------------------
C
      RETURN
      END
