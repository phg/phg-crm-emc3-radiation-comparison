CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/daoutg.for,v 1.2 2004/07/06 13:27:43 whitefor Exp $ Date $Date: 2004/07/06 13:27:43 $
CX
      SUBROUTINE DAOUTG( TITLE  , TITLX  , TITLM , DATE  ,
     &                   IZ0    , IZ1    , 
     &                   iopt   ,
     &                   cpai   , cpaf   , 
     &                   irep   , nrepa  , auga  ,
     &                   cprnt  , clev   ,
     &                   cpi    , cpf    , css   , nrep  ,
     &                   TEVA   , QDR    , ITVAL ,
     &                   TFEVM  , QDRM   , NMX   ,
     &                   TFEVS  , QDRS   , 
     &                   LGRD1  , LDEF1  , LFSEL , 
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DAOUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED DATA.
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE IINTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(RATE-COEFF.(cm**3/s)) VERSUS LOG10(TEMP.(eV))
C
C  CALLING PROGRAM: ADAS505
C
C  SUBROUTINE:
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK,TYPE and SOURCE
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE
C
C  INPUT : (R*8)  TEVA()  = TEMPERATURES (eV)
C  INPUT : (R*8)  QDR()   = SPLINE INTERPOLATED OR EXTRAPOLATED RATE-
C                           COEFFICIENTS FOR THE USER ENTERED TEMPERATURES
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURES.
C
C  INPUT : (R*8)  TFEVM() = MINIMAX: SELECTED TEMPERATURES (eV)
C                                    (SEE 'LDFIT' FOR TYPE)
C  INPUT : (R*8)  QDRM()  = RATE-COEFFTS. (cm**3/sec) AT 'TFEVM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX  GENERATED RATE-COEFFT./
C                           TEMPERATURE PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (eV)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (eV)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEFFT.(cm**3/s)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEFFT.(cm**3/s)
C
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
C
C          (C*26) STRG1   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*26) STRG2   =  DESCRIPTIVE STRING FOR RECOMBINING ION CHGE
C          (C*25) STRG3   =  DESCRIPTIVE STRING ATOMIC MASS NUMBER
C          (C*25) STRG4   =  DESCRIPTIVE STRING ISOTOPIC MASS NUMBER
C          (C*32) HEAD1   =  HEADING FOR RECEIVER INFORMATION
C          (C*44) HEAD2   =  FIRST  HEADER FOR RECEIVER/DONOR TEMP. INFO
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXLIM8      ADAS      SETTING UP OF DEFAULT GRAPH AXES
C          XXGHDI      ADAS      GHOST80 DESCRIPTION LINE + INTEGER
C          XXGHD8      ADAS      GHOST80 DESCRIPTION LINE + REAL*8
C          XXGH88      ADAS      GHOST80 TABLE LISTING WITH 2 x REAL*8
C          XXGSEL      ADAS      SELECTS POINTS WHICH WILL FIT ON GRAPH
C          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  Martin O'Mullane
C          K1/1/43
C          JET EXT. 5313
C
C DATE:    25/9/97
C
C VERSION: 1.1
C MODIFIED: 
C
C VERSION: 1.2						DATE: 20-02-98
C MODIFIED: MARTIN O'MULLANE
C		- MINOR CHANGES TO DESCRIPTIVE STRINGS.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    ITVAL           , NMX              ,
     &           IZ0             , IZ1              ,
     &           iopt            , nrep
      INTEGER    I               
      integer    irep            , nrepa(irep)  
C-----------------------------------------------------------------------
      REAL*8     XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGRD1           , LDEF1            , LFSEL     
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       ,  TITLX*120        , TITLM*80  ,
     &           DATE*8
      CHARACTER  GRID*1         , PIC*1             , C3BLNK*3  ,
     &           MNMX0*9        , KEY0*9            , ADAS0*8   ,
     &           DNAME*12       ,
     &           CADAS*80       , ISPEC*88	   
      CHARACTER  STRG(4)*49     ,
     &           HEAD1*32       , HEAD2*44 
      character  cprnt*21       , clev*21
      character  cpi*21         , cpf*21        , css*6 , c3*3
      character  cpai*21        , cpaf*21
C-----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)    , QDR(ITVAL)     ,
     &           TFEVM(NMX)     , QDRM(NMX)      , auga(irep)   ,
     &           TFEVS(NMX)     , QDRS(NMX) 
C-----------------------------------------------------------------------
      CHARACTER  KEY(2)*28
C-----------------------------------------------------------------------
      SAVE       ISPEC          , CADAS
C-----------------------------------------------------------------------
      DATA ISPEC(1:59)
     & /'DIELECTRONIC RECOMBINATION RATE COEFFT VERSUS TEMPERATURE: '/
      DATA DNAME /'      DATE: '/
      DATA ADAS0 /'ADAS   :'/
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(CROSSES/FULL LINE - SPLINE)'/  ,
     &     KEY(2)/'  (DASH LINE - MINIMAX)     '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/   ,
     &     C3BLNK/'   '/ ,
     &     CADAS /' '/
C-----------------------------------------------------------------------
C
      INTEGER PIPEIN, PIPEOU, ZERO, ONE
      PARAMETER (PIPEIN=5,PIPEOU=6, ONE=1, ZERO=0)
C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C

      head2 = '--- TEMPERATURE ---- RATE COEFF. ---'

      if (iopt.eq.0) then
      
         head1   = '---- Auger Breakup ----'
         strg(1) = ' Initial parent         : '//cpai
         strg(2) = ' Final Parent           : '//cpaf
         strg(3) = ' '
         strg(4) = ' '
         
      elseif(iopt.eq.1) then
      
         head1   = '----- Resolved DR -----'
         strg(1) = ' Initial parent         : '//cprnt
         strg(2) = ' Resolved level         : '//clev
         strg(3) = ' '
         strg(4) = ' '
         
      elseif(iopt.eq.2) then
      
         head1   = '----- Bundle-n DR -----'
         strg(1) = ' Initial parent         : '//cpi
         strg(2) = ' Final parent           : '//cpf
         strg(3) = ' Recombined system/spin : '//css
         if (nrep.eq.1) then 
            strg(4) = ' Total rate ' 
         else
           write(c3,'(i3)')nrep-1
           strg(4) = ' Bundle-n level         : '//c3
         endif
      
      endif
        
        
      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ1
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,*) IOPT
      CALL XXFLSH(PIPEOU)
      
C
      if (iopt.eq.0) then
      
         WRITE(PIPEOU,*) irep
         CALL XXFLSH(PIPEOU)

         DO I = 1,irep
            WRITE(PIPEOU,*) nrepa(I)
            CALL XXFLSH(PIPEOU)
         END DO


         DO I = 1,irep
            WRITE(PIPEOU,*) auga(I)
	    CALL XXFLSH(PIPEOU)
         END DO
      
      
      else
      	
         WRITE(PIPEOU,*) ITVAL
         CALL XXFLSH(PIPEOU)

         DO I = 1,ITVAL
            WRITE(PIPEOU,*) TEVA(I)
            CALL XXFLSH(PIPEOU)
         END DO


         DO I = 1,ITVAL
            WRITE(PIPEOU,*) QDR(I)
	    CALL XXFLSH(PIPEOU)
         END DO

         WRITE(PIPEOU,*) NMX
         CALL XXFLSH(PIPEOU)
         DO I = 1, NMX
	    WRITE(PIPEOU,*) QDRS(I)
	    CALL XXFLSH(PIPEOU)
         END DO
         DO I = 1, NMX
            WRITE(PIPEOU,*) TFEVS(I)
	    CALL XXFLSH(PIPEOU)
         END DO
         
      endif
C
      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMAX
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMAX
	 CALL XXFLSH(PIPEOU)
      ENDIF
C
      IF (LFSEL) THEN 
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) NMX
	 CALL XXFLSH(PIPEOU)
         DO I = 1, NMX
	    WRITE(PIPEOU,*) QDRM(I)
	    CALL XXFLSH(PIPEOU)
         END DO
         DO I = 1, NMX
            WRITE(PIPEOU,*) TFEVM(I)
	    CALL XXFLSH(PIPEOU)
         END DO
      ELSE 
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ENDIF

C
 	
      WRITE(PIPEOU,'(A49)') (STRG(I), I=1,4)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A44)') HEAD2
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C
      RETURN
C-----------------------------------------------------------------------
      END
