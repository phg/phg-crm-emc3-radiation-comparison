CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/dadata.for,v 1.2 2004/07/06 13:27:17 whitefor Exp $ Date $Date: 2004/07/06 13:27:17 $
CX
      SUBROUTINE  DADATA( IUNIT  , NDPRT  , NDREP , NDLEV ,
     &                    NDAUG  , NDT   ,
     &                    SEQSYM , IZ     , IZ0   , IZ1   ,
     &                    NPRNT  , NPRNTI , NPRNTF, BWNP  ,
     &                    IPA    , CSTRPA , ISPA  , ILPA  , XJPA  ,
     &                    WPA    ,
     &                    IL     , BWNR   ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA   ,
     &                    WA     ,
     &                    NREP   , IAPRS  , CAPRS , IPAUG ,
     &                    IREPA  , NREPA  , AUGA  , LAUGA ,
     &                    IPRTI  , TPRTI  , ISPRTI, DIELR , LDIELR,
     &                    IPRTF  , TPRTF  , ISPRTF,
     &                    NSYSF  , ISYS   , ISPSYS, DIELN , LDIELN,
     &                    DIELT  ,
     &                    NTE    , TEA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DADATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT ADF09 DATA SET.
C
C  CALLING PROGRAM: ADAS204/ADAS212/ADAS410
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C
C  INPUT : (I*4)  NDPRT   = MAXIMUM NUMBER OF PARENT STATES
C  INPUT : (I*4)  NDREP   = MAX. NUMBER OF REPRESENTATIVE N-SHELLS
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF RESOLVED LEVELS
C  INPUT : (I*4)  NDAUG   = MAXIMUM NUMBER OF AUGER RATE INITIAL AND
C                           FINAL PARENT PAIRS
C  INPUT : (I*4)  NDT     = MAX. NUMBER OF ELECTRON TEMPERATURES
C
C  OUTPUT: (C*2)  SEQSYM  = RECOMBINED ION SEQ
C  OUTPUT: (I*4)  IZ      = RECOMBINED ION CHARGE
C  OUTPUT: (I*4)  IZ0     = NUCLEAR CHARGE
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE
C  OUTPUT: (I*4)  NPRNT   = TOTAL NUMBER OF PARENTS
C  OUTPUT: (I*4)  NPRNTI  = NUMBER OF PARENTS WHICH ARE INITIAL PARENTS
C  OUTPUT: (I*4)  NPRNTF  = NUMBER OF PARENTS WHICH ARE FINAL PARENTS
C  OUTPUT: (R*8)  BWNP    = BINDING WAVE NO. OF GROUND PARENT (CM-1)
C  OUTPUT: (I*4)  IPA()   = NUMBER OF PARENT ENERGY LEVELS
C  OUTPUT: (C*18) CSTRPA()= NOMENCL./CONFIG. FOR PARENT LEVEL 'IPA()'
C  OUTPUT: (I*4)  ISPA()  = MULTIPLICITY FOR PARENT LEVEL 'IPA()'
C                           NOTE: (ISPA-1)/2 = QUANTUM NUMBER (SP)
C  OUTPUT: (I*4)  ILPA()  = QUANTUM NUMBER (LP) FOR PARENT LEVEL 'IPA()'
C  OUTPUT: (R*8)  XJPA()  = QUANTUM NUMBER (JP) FOR PARENT LEVEL 'IPA()'
C                           NOTE: (2*XJPA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WPA()   = ENERGY RELATIVE TO PARENT LEVEL 1 (CM-1)
C                           FOR PARENT LEVEL 'IPA()'
C
C  OUTPUT: (I*4)  IL      = NUMBER OF ENERGY LEVELS (TERMS) OF
C                           RECOMBINED ION
C  OUTPUT: (R*8)  BWNR    = IONISATION POTENTIAL (CM-1) OF LOWEST LEVEL
C                           OF RECOMBINED ION
C  OUTPUT: (I*4)  IA()    = RECOMBINED ION ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCL./CONFIG. FOR RECOMBINED ION LEVEL
C                           'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR RECOMBINED LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR RECOMBINED LEVEL
C                           'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J) FOR RECOMBINED LEVEL
C                           'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WA()    = ENERGY RELATIVE TO RECOMBINED LEVEL 1 (CM-1)
C                           FOR RECOMBINED LEVEL 'IA()'
C  OUTPUT: (I*4)  NREP    = NUMBER OF REPRESENTATIVE N-SHELLS
C  OUTPUT: (I*4)  IREPA() = REPRESENTATIVE N-SHELL INDEX NUMBER
C  OUTPUT: (I*4)  NREPA() = REPRESENTATIVE N-SHELLS
C  OUTPUT: (I*4)  IAPRS   = NUMBER OF AUGER RATE INITIAL AND FINAL
C                           PARENT PAIRS
C  OUTPUT: (C*10) CAPRS() = AUGER RATE PARENT PAIR STRING
C                           1ST.DIM: PARENT PAIR INDEX
C  OUTPUT: (I*40) IPAUG(,)= INITIAL AND FINAL PARENTS FOR AUGER BREAKUPS
C                           1ST.DIM: PARENT PAIR INDEX
C                           2ND.DIM: INITIAL AND FINAL PARENT INDICES
C  OUTPUT: (R*8)  AUGA(,) = AUGER RATES (SEC-1)
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: PARENT PAIR INDEX
C  OUTPUT: (L*4)  LAUGA(,) = .TRUE. => AUGER RATE PRESENT FOR N-SHELL
C                            .FALSE.=> AUGER RATE NOT PRESENT
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: PARENT PAIR INDEX
C  OUTPUT: (I*4)  IPRTI() = INITIAL PARENT BLOCK INDEX
C  OUTPUT: (C*5)  TPRTI() = INITIAL PARENT BLOCK TERM
C  OUTPUT: (I*4)  ISPRTI()= INITIAL PARENT BLOCK SPIN MULTIPLICITY
C  OUTPUT: (R*8)  TEA()   = ELECTRON TEMPERATURES (K)
C  OUTPUT: (R*8)  DIELR(,,)= TERM SELECTIVE DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: LEVEL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: TEMPERATURE INDEX
C  OUTPUT: (L*4)  LDIELR(,)= .TRUE. => DIEL. PRESENT FOR LEVEL INDEX
C                            .FALSE.=> DIEL. NOT PRESENT FOR LEVEL INDEX
C                           1ST.DIM: LEVEL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C  OUTPUT: (I*4)  IPRTF(,) = FINAL PARENT BLOCK INDEX
C  OUTPUT: (C*5)  TPRTF(,) = FINAL PARENT BLOCK TERM
C  OUTPUT: (I*4)  ISPRTF(,)= FINAL PARENT BLOCK SPIN MULTIPLICITY
C  OUTPUT: (I*4)  NSYSF(,) = NO,. OF SPIN SYSTEMS BUILT ON FINAL PARENT
C  OUTPUT: (I*4)  ISYS(,,) = N-SHELL SPIN SYSTEM INDEX FOR FINAL PARENT
C  OUTPUT: (I*4)  ISPSYS(,,)=N-SHELL SPIN SYSTEM FOR FINAL PARENT
C  OUTPUT: (R*8)  DIELN(,,,,) =N-SHELL DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: REPR. N-SHELL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: FINAL PARENT INDEX
C                           4TH.DIM: SPIN SYSTEM INDEX
C                           5TH.DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)  LDIELN(,)= .TRUE. => DIEL. PRESENT FOR REPR. N-SHELL
C                            .FALSE.=> DIEL. NOT PRESENT FOR  N-SHELL
C                           1ST.DIM: REPR. N-SHELL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: FINAL PARENT INDEX
C                           4TH.DIM: SPIN SYSTEM INDEX
C  OUTPUT: (R*8)  DIELT(,,,) =N-SHELL DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: INITIAL PARENT INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C                           3RD.DIM: SPIN SYSTEM INDEX
C                           4TH.DIM: TEMPERATURE INDEX
C
C          (I*4)  INDX    = GENERAL INDEX
C          (I*4)  INDX1   = GENERAL INDEX
C          (I*4)  II      = GENERAL INDEX
C          (I*4)  I       = GENERAL INDEX
C          (I*4)  IPI     = GENERAL INDEX
C          (I*4)  IPF     = GENERAL INDEX
C          (I*4)  IPFS    = GENERAL INDEX
C          (I*4)  J       = GENERAL INDEX
C          (I*4)  K       = GENERAL INDEX
C
C          (L)    LDATA   = GENERAL READ/DO NOT READ FLAG
C          (L)    LNOPI   = FLAG TO DETERMINE WHETHER HAVE PASSED
C			    INTO A NEW INITIAL PARENT BLOCK
C
C          (C*20) C20     = GENERAL CHARACTER STRING
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4EIZ0     ADAS      RETURNS NUCL. CHARGE FROM ELEMENT SYMBOL
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C          XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C  DATE:    05/08/97
C
C  UPDATE: Modified final parent reading block to acount for missing 
C          final parents. (do-while to 130 statements).
C          Also added END=999 to read statement to avoid EOF error 
C          when there is no data in SYS/SPNSYS block.
C          Martin O'Mullane, 3-10-97
C  
C  VERSION: 1.1
C
C  VERSION: 1.2						DATE: 20-02-98
C  MODIFIED: MARTIN O'MULLANE
C    		- ERROR IN ASSIGNING NUMBER OF PARENTS IF >= NDPRT.          
C 		- ADDED TOTAL DR RATE FOR EACH (INITIAL PARENT, FINAL PARENT, 
C             SPIN SYSTEM) BLOCK BY SUMMING UP AND INTERPOLATING THE 
C             REPRESENTATIVE LEVEL SET.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8    DZERO
C-----------------------------------------------------------------------
      PARAMETER( DZERO = 1.0D-60 )
C-----------------------------------------------------------------------
      INTEGER   I4UNIT      , I4EIZ0
      INTEGER   IUNIT       , NDPRT          , NDREP      ,
     &          NDLEV       , NDAUG          , NDT
      INTEGER   IZ          , IZ0            , IZ1        ,
     &          NPRNT       , NPRNTI         , NPRNTF     ,
     &          IL          , NREP           , IAPRS      , NVALS
      INTEGER   INDX        , NTE
      INTEGER   ILINE       , IRECL
      INTEGER   IQS         , I              , II         , IABT       ,
     &          IPI         , IPF            , IPFS       ,
     &          IFIRST(1)   , ILAST(1)       , IWORD      ,
     &          J           , J1             , J2         , K          ,
     &          LENCST      , NWORDS         , IAPOW      , INDX1
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRT)  , ISPA(NDPRT)    , ILPA(NDPRT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IREPA(NDREP), NREPA(NDREP)   , IPAUG(NDAUG,2)
      INTEGER   IPRTI(NDPRT), ISPRTI(NDPRT)
      INTEGER   IPRTF(NDPRT,NDPRT), ISPRTF(NDPRT,NDPRT)
      INTEGER   NSYSF(NDPRT,NDPRT)
      INTEGER   ISYS(NDPRT,NDPRT,2), ISPSYS(NDPRT,NDPRT,2)
C-----------------------------------------------------------------------
      REAL*8    R8FCTN
      REAL*8    BWNP        , BWNR
C-----------------------------------------------------------------------
      REAL*8    XJPA(NDPRT) , WPA(NDPRT)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)
      REAL*8    TEA(NDT)
      REAL*8    AUGA(NDREP,NDAUG) , DIELR(NDLEV,NDPRT,NDT)
      REAL*8    DIELN(NDREP,NDPRT,NDPRT,2,NDT)
      REAL*8    DIELT(NDPRT,NDPRT,2,NDT)
C-----------------------------------------------------------------------
      CHARACTER SEQSYM*2
      CHARACTER C7*7        , CDELIM*7       , C20*20
      CHARACTER CJ1*4       , CAVLM*8        ,
     &          CLINE*80    , BUFFER*128
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRT)*(*) , CSTRGA(NDLEV)*(*)
      CHARACTER CAPRS(NDAUG)*10   , TPRTI(NDPRT)*5
      CHARACTER TPRTF(NDPRT,NDPRT)*5
C-----------------------------------------------------------------------
      LOGICAL   LDATA       , LTCHR          , LTCPR      ,
     &          LTCHS       , LERROR         , LNOPI
C-----------------------------------------------------------------------
      LOGICAL   LAUGA(NDREP,NDAUG)
      LOGICAL   LDIELR(NDLEV,NDPRT)
      LOGICAL   LDIELN(NDREP,NDPRT,NDPRT,2)
C-----------------------------------------------------------------------
      DATA      CDELIM / ' ()<>{}' /
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C ZERO ARRAYS
C-----------------------------------------------------------------------
C
      DO 5 I=1,NDLEV
        DO 4 IPI=1,NDPRT
          DO 3 J=1,NDT
            DIELR(I,IPI,J)=0.0D0
            LDIELR(I,IPI)=.FALSE.
    3     CONTINUE
    4   CONTINUE
    5 CONTINUE
C
C-----------------------------------------------------------------------
C INPUT ION SPECIFICATIONS.
C-----------------------------------------------------------------------
C
      READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'SEQ=')
      READ(BUFFER(INDX+5:INDX+6),'(A2)')SEQSYM
      INDX=INDEX(BUFFER,'NUCCHG=')
      READ(BUFFER(INDX+7:INDX+8),'(I2)')IZ0
C
      IZ=IZ0-I4EIZ0(SEQSYM)
      IZ1=IZ+1
C
C-----------------------------------------------------------------------
C LOCATE AND READ RECOMBINING ION (PARENT) LEVEL DATA
C-----------------------------------------------------------------------
C
   10 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'NPRNTI')
      IF(INDX.LE.0) THEN
          GO TO 10
      ENDIF
C
      INDX=INDEX(BUFFER,'BWNP=')
      READ(BUFFER(INDX+5:INDX+16),'(F12.1)') BWNP
      INDX=INDEX(BUFFER,'NPRNTI=')
      READ(BUFFER(INDX+7:INDX+8),'(I2)') NPRNTI
      INDX=INDEX(BUFFER,'NPRNTF=')
      READ(BUFFER(INDX+7:INDX+8),'(I2)') NPRNTF
      READ(IUNIT,'(A)')CLINE,CLINE,CLINE
C
      LDATA=.TRUE.
C
         DO 20 I=1,NDPRT
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I6)')  ILINE
C
C  PARENT LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     NPRNT=I-1
C
C  PARENT LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'PARENT LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (CLINE,1003) IPA(I)   , C20          ,
     &                                 ISPA(I) , ILPA(I)       ,
     &                                 C7      , BUFFER(1:37)
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:37) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
                     CSTRPA(I)=C20(1:18)
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJPA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'ADF09 DATA SET PARENT ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C
                     IF(BUFFER(ILAST(1):ILAST(1)).EQ.'*') THEN
                         ILAST(1)=ILAST(1)-1
                     ENDIF
                     READ(BUFFER(IFIRST(1):ILAST(1)),*) WPA(I)
C
                  ENDIF
            ENDIF
   20    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF09 DATA SET CONTAINS > ',NDPRT,
     &                          ' PARENT LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  NPRNT = NDPRT
               ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C LOCATE AND READ RECOMBINED ION  LEVEL DATA
C-----------------------------------------------------------------------
C
   30 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'NTRM=')
      IF(INDX.LE.0) THEN
          GO TO 30
      ENDIF
C
      INDX=INDEX(BUFFER,'BWNR=')
      READ(BUFFER(INDX+5:INDX+16),'(F12.1)') BWNR
      INDX=INDEX(BUFFER,'NTRM=')
      READ(BUFFER(INDX+5:INDX+7),'(I3)') IL
      READ(IUNIT,'(A)')CLINE,CLINE,CLINE
C
      LDATA=.TRUE.
C
         DO 40 I=1,NDLEV
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I6)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (CLINE,1003) IA(I)   , C20          ,
     &                                 ISA(I)  , ILA(I)       ,
     &                                 C7      , BUFFER(1:37)
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:37) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C
                     CSTRGA(I)=C20(1:18)
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'ADF09 DATA SET LEVEL ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C
                    READ(BUFFER(IFIRST(1):ILAST(1)),*) WA(I)
C
                  ENDIF
            ENDIF
   40    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF09 DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C LOCATE AND READ REPRESENTATIVE LEVEL AND AUGER RATE DATA
C-----------------------------------------------------------------------
C
   50 READ(IUNIT,'(A)') BUFFER
      INDX=INDEX(BUFFER,'NREP=')
      IF(INDX.LE.0) THEN
          GO TO 50
      ENDIF
C
      INDX=INDEX(BUFFER,'NREP=')
      READ(BUFFER(INDX+5:INDX+16),'(I3)') NREP
      READ(IUNIT,'(A)')BUFFER
C      WRITE(6,*)'NREP=',NREP
C
      READ(IUNIT,'(21X,A100)')BUFFER(1:100)
C      WRITE(6,*)BUFFER
      J     = 1
      IWORD = 1
      CALL XXWORD( BUFFER(1:100), CDELIM    , IWORD  ,
     &             J            ,
     &             IFIRST(1)    , ILAST(1)  , IAPRS  )
      READ(BUFFER(1:100),'(10A10)')(CAPRS(J),J=1,IAPRS)
C
      DO 55 J=1,IAPRS
        INDX=INDEX(CAPRS(J),'-')
        READ(CAPRS(J)(INDX-1:INDX-1),'(I1)')IPAUG(J,1)
        READ(CAPRS(J)(INDX+1:INDX+1),'(I1)')IPAUG(J,2)
   55 CONTINUE
C
      READ(IUNIT,'(A)')BUFFER
C
      LDATA=.TRUE.
C
         DO 70 I=1,NDREP
C
            IF (LDATA) THEN
               READ (IUNIT,'(A128)') BUFFER
               READ (BUFFER,'(I6)')  ILINE
C
C  REPR. LEVEL INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     NREP=I-1
C
C  REPR. LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'REPR. N-SHELL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
                     READ (BUFFER,1005) IREPA(I) , NREPA(I)     ,
     &                                 (AUGA(I,K),K=1,IAPRS)
                     DO 65 K=1,IAPRS
                       IF(AUGA(I,K).GT.0.0D0) THEN
                           LAUGA(I,K)=.TRUE.
                       ELSE
                           LAUGA(I,K)=.FALSE.
                       ENDIF
   65                CONTINUE
                  ENDIF
            ENDIF
   70    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'ADF09 DATA SET CONTAINS > ',NDREP,
     &                          ' REPR. N-SHELLS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  NREP = NDREP
               ENDIF
         ENDIF
C
C-----------------------------------------------------------------------
C LOCATE DATA BLOCK FOR EACH INITIAL PARENT
C-----------------------------------------------------------------------
C
      DO 140 IPI=1,NPRNTI
C
   75   READ(IUNIT,'(A)') BUFFER
        INDX=INDEX(BUFFER,'PRTI=')
        IF(INDX.LE.0) THEN
            GO TO 75
        ENDIF
C
        INDX=INDEX(BUFFER,'PRTI=')
        READ(BUFFER(INDX+5:INDX+6),'(I2)') IPRTI(IPI)
        INDX=INDEX(BUFFER,'TRMPRT=')
        READ(BUFFER(INDX+7:INDX+11),'(A5)') TPRTI(IPI)
        INDX=INDEX(BUFFER,'SPNPRT=')
        READ(BUFFER(INDX+7:INDX+8),'(I2)') ISPRTI(IPI)
C
        READ(IUNIT,'(11X,1A100)')BUFFER(1:100),BUFFER(1:100)
        J     = 1
        IWORD = 1
        CALL XXWORD( BUFFER(1:100), CDELIM    , IWORD  ,
     &               J            ,
     &               IFIRST(1)    , ILAST(1)  , NTE    )
        READ(BUFFER(1:100),'(10E10.2)')(TEA(J),J=1,NTE)
C
        READ(IUNIT,'(A)')BUFFER
        LDATA=.TRUE.
C
        DO 80 I=1,NDLEV
          IF (LDATA) THEN
              READ (IUNIT,'(A128)') BUFFER
              READ (BUFFER,'(I6)')  ILINE
C
C  DIEL. RECOM. INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
              IF (ILINE.LE.0) THEN
                  LDATA=.FALSE.
                  NVALS=I-1
              ELSE
                  READ (BUFFER,1006) INDX,
     &                               (DIELR(INDX,IPI,J),J=1,NTE)
                  LDIELR(INDX,IPI)=.TRUE.
              ENDIF
          ENDIF
   80   CONTINUE
C
C-----------------------------------------------------------------------
C LOCATE DATA BLOCK FOR EACH FINAL PARENT
C-----------------------------------------------------------------------
C
        IPF   = 1
        LNOPI = .TRUE.
        DO WHILE (IPF.LE.NPRNTF.AND.LNOPI)
C
C we will eventually run out of dataset - in this case end reading
C
   85     READ(IUNIT,'(A)',end=999) BUFFER
          INDX  = INDEX(BUFFER,'PRTF=')
          INDX1 = INDEX(BUFFER,'PRTI=')
C
C not all final parents are present in the adf09 dataset. If we read
C PRTI indicating a new block rewind one record and pass control back
C to the initial parent loop.
C
          IF (INDX1.GT.0) THEN
             BACKSPACE(IUNIT)
             LNOPI = .FALSE.
             GOTO 130
          ENDIF 
          IF(INDX.LE.0) THEN
              GO TO 85
          ENDIF
C
          INDX=INDEX(BUFFER,'PRTF=')
          READ(BUFFER(INDX+5:INDX+6),'(I2)') IPRTF(IPI,IPF)
          INDX=INDEX(BUFFER,'TRMPRT=')
          READ(BUFFER(INDX+7:INDX+11),'(A5)') TPRTF(IPI,IPF)
          INDX=INDEX(BUFFER,'SPNPRT=')
          READ(BUFFER(INDX+7:INDX+8),'(I2)') ISPRTF(IPI,IPF)
          INDX=INDEX(BUFFER,'NSYS=')
          READ(BUFFER(INDX+5:INDX+6),'(I2)') NSYSF(IPI,IPF)
C
C-----------------------
C AND EACH SPIN SYSTEM
C-----------------------
C
          DO 120 IPFS=1,NSYSF(IPI,IPF)
C
   90       READ(IUNIT,'(A)') BUFFER
            INDX=INDEX(BUFFER,'SPNSYS')
            IF(INDX.LE.0) THEN
                GO TO 90
            ENDIF
C
            INDX=INDEX(BUFFER,'SYS=')
            READ(BUFFER(INDX+4:INDX+5),'(I2)') ISYS(IPI,IPF,IPFS)
            INDX=INDEX(BUFFER,'SPNSYS=')
            READ(BUFFER(INDX+7:INDX+11),'(I2)') ISPSYS(IPI,IPF,IPFS)
C
C
            READ(IUNIT,'(A)')BUFFER,BUFFER
            LDATA=.TRUE.
C
            DO 110 I=1,NDREP
              IF (LDATA) THEN
                 READ (IUNIT,'(A128)') BUFFER
                 READ (BUFFER,'(I6)')  ILINE
C
C  DIEL. RECOM. INPUT INFORMATION IS TERMINATED BY ILINE= 0 OR -1
C
                 IF (ILINE.LE.0) THEN
                    LDATA=.FALSE.
                    NVALS=I-1
                 ELSE
                    READ (BUFFER,1006) INDX,
     &                      (DIELN(INDX,IPI,IPF,IPFS,J),J=1,NTE)
                    LDIELN(INDX,IPI,IPF,IPFS)=.TRUE.
                 ENDIF
              ENDIF
  110       CONTINUE

C Calculate total DR rate now for each initial parent, final parent and 
C spin system

            call dasumd ( ndrep  , ndprt   , ndt    ,
     &                    ipi    , ipf     , ipfs   ,
     &                    nte    , nvals+1 , nrepa  , dieln  ,
     &                    dielt  )
        
  120   CONTINUE
  
C
  130   CONTINUE
  
          IPF = IPF+1
        END DO
        
C
  140 CONTINUE
C
C-----------------------------------------------------------------------
C
 1001 FORMAT(1X,32('*'),' DADATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I6,5X,1A20,1X,2(I1,1X),A7,A37)
 1004 FORMAT(I5)
 1005 FORMAT(I6,I6,9X,10E10.2)
 1006 FORMAT(I6,5X,10E10.2)
 1012 FORMAT(1X,A,' - READ VALUE = ',A7)
C
C-----------------------------------------------------------------------
C
  999 CONTINUE
  
      RETURN
      END
