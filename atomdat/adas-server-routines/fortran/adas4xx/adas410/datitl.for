CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/datitl.for,v 1.1 2004/07/06 13:28:07 whitefor Exp $ Date $Date: 2004/07/06 13:28:07 $
CX
      SUBROUTINE DATITL( IOPT     , 
     &           	 IP_RES   , IL_RES , 
     & 			 DSFULL   ,
     &                   TITLX 
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DATITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS410
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IOPT       = Type of plot
C  INPUT : (I*4)  IL_RES     = resolved DR parent
C  INPUT : (I*4)  IL_RES     = resolved DR level
C  I
C  INPUT : (C*80) DSFULL   = FULL INPUT DATA SET NAME
C
CX  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: RECEIVER -
CX                                                 NUCLEAR CHARGE
CX  INPUT : (I*4)  IRZ1     = SELECTED DATA-BLOCK: RECEIVER -
CX                                                 RECOMBINING ION CHARGE
CX  INPUT : (I*4)  IRZ0     = SELECTED DATA-BLOCK: DONOR    -
CX                                                 NUCLEAR CHARGE
C
C  INPUT : (C*9)  CDONOR   = SELECTED DATA-BLOCK: DONOR IDENTITY
C
C  INPUT : (C*9)  CRECVR   = SELECTED DATA-BLOCK: RECEIVER IDENTITY
C
C  INPUT : (C*10) CFSTAT  = SELECTED DATA-BLOCK: FINAL STATE SPEC.
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C
C
C AUTHOR   : Martin O'Mullane
C            K1/1/43
C            JET EXT. 5313
C
C DATE     : 25/9/97
C
C VERSION  : 
C MODIFIED : 
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IOPT        , IP_RES  , IL_RES  ,         
     &           LEN_NAME    ,
     &		 IFIRST      , ILAST	  
C-----------------------------------------------------------------------
      CHARACTER  C2A*2           , C2B*2
      CHARACTER  DSFULL*120      , TITLX*120
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      CALL XXSLEN(DSFULL,IFIRST,ILAST)
      LEN_NAME=ILAST-IFIRST+1
      WRITE(C2A,1000) IP_RES
      WRITE(C2B,1000) IL_RES
      TITLX(1:LEN_NAME) = DSFULL(IFIRST:ILAST)
      TITLX(LEN_NAME+1:LEN_NAME+21) = ' Parent: '//C2A//' Level: '//C2B
C	titlx ='A temporary title'
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
