CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/cstrl.for,v 1.1 2004/07/06 12:24:04 whitefor Exp $ Date $Date: 2004/07/06 12:24:04 $
CX
      FUNCTION CSTRL(L)
C
      IMPLICIT NONE
C---------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: CSTRL  ********************
C
C  PURPOSE: RETURNS CHARACTER CODE FOR L VALUE	
C
C
C  INPUT :  INTEGER  L       = Angular Momentumn (eg.  1 )
C
C  OUTPUT:  (C*1)    CSTRL   = Angular Momentumn (eg. 'P')
C
C  Returns a blank string if L > 15 
C
C  SUBROUTINES:  NONE
C
C  AUTHOR:  Martin O'Mullane, JET
C
C  DATE:    2/10/97
C
C VERSION: 
C MODIFIED: 
C
C-----------------------------------------------------------------------
      INTEGER      L
C-----------------------------------------------------------------------
      CHARACTER*1  CSTRL  , CHL(16)
C-----------------------------------------------------------------------
      DATA CHL/'S','P','D','F','G','H','I','K','L','M','N',
     &         'O','Q','R','T','U'/
C-----------------------------------------------------------------------
      
      IF (L.GT.15) THEN
         CSTRL = ' '
      ELSE
         CSTRL = CHL(L+1)      
      ENDIF

      RETURN
      END
