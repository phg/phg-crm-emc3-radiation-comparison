CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/adas410.for,v 1.2 2004/07/06 10:47:42 whitefor Exp $ Date $Date: 2004/07/06 10:47:42 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS410 **********************
C
C
C  VERSION:  1.0 
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF BADNELL DIELECTRONIC DATA.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
C  DATA:     THE SOURCE DATA IS adf09
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            TEMPERATURES        : KELVIN
C            RATE-COEFFICIENTS   : CM**3 S**-1
C
C  PROGRAM:
C
C          (I*4)  NTDIM   = PARAMETER = MAXIMUM NUMBER TEMPERATURES
C                                       THAT CAN BE READ FROM
C                                       AN INPUT DATA-SET.
C          (I*4)  NDTIN   = PARAMETER = MAX. NO. OF IDL ENTRED RECEIVER
C                                       /DONOR TEMPERATURE PAIRS.
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED RATE-COEFFT./
C                           TEMPERATURE PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  ITVAL   = NUMBER OF USER ENTERED TEMPERATURE PAIRS
C          (I*4)  ITTYP   = 1 => 'TRIN/TDIN(array)' UNITS: KELVIN
C                         = 2 => 'TRIN/TDIN(array)' UNITS: EV
C                         = 3 => 'TRIN/TDIN(array)' UNITS: REDUCED
C
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: SEE 'ITTYP'
C          (R*8)  XMINEV  = GRAPH: LOWER LIMIT FOR TEMPERATURE
C                                  UNITS: EV
C          (R*8)  XMAXEV  = GRAPH: UPPER LIMIT FOR TEMPERATURE
C                                  UNITS: EV
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR RATE-COEF (cm**3/sec)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR RATE-COEF (cm**3/sec)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM PIPE)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*120) TITLX  = INFORMATION STRING CONTAINING: INPUT DATA
C                           SET-NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C          (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END TO
C                           THE PROGRAM
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  IP_AUG  = USER SELECTED INDEX OF AUGER RATES
C          (I*4)  IP_RES  = USER SELECTED INDEX OF RES. PARENT
C          (I*4)  IL_RES  = USER SELECTED INDEX OF RES. LEVEL
C          (I*4)  IN_REP  = USER SELECTED INDEX OF REP. N-SHELL
C          (I*4)  IPI_REP = USER SELECTED INDEX OF REP. INITIAL PARENT
C          (I*4)  IPF_REP = USER SELECTED INDEX OF REP. FINAL PARENT
C          (I*4)  ISP_REP = USER SELECTED INDEX OF REP. SPIN SYSTEM
C
C          (R*8)  TIN()   = USER ENTERED RECEIVER TEMPERATURES
C                           (NOTE: UNITS ARE GIVEN BY 'ITTYP')
C                           DIMENSION: TEMP. INDEX
C          (R*8)  TEVA()  = USER ENTERED RECEIVER TEMPERATURES
C                           (UNITS: EV)
C                           DIMENSION: TEMP. INDEX
C          (R*8)  TKEL()  = USER ENTERED RECEIVER TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: TEMP. INDEX
C          (R*8)  TFEVM() = MINIMAX: SELECTED TEMPERATURES (eV)
C          (R*8)  QDRIN() = DEPENDING ON OPTION EITHER THE RESOLVED
C                           OR REPRESENTATIVE DR COEFFS FROM INPUT FILE
C                           (UNITS: CM**3/SEC)
C                           DIMENSION: TEMP INDEX
C          (R*8)  QDROUT()= DEPENDING ON OPTION EITHER THE RESOLVED
C                           OR REPRESENTATIVE DR COEFFS SPLINED TO 
C                           THE USER SELECTED TEMPERATURE
C                           (UNITS: CM**3/SEC)
C                           DIMENSION: TEMP INDEX
C          (R*8)  QDRS()  = DEPENDING ON OPTION EITHER THE RESOLVED
C                           OR REPRESENTATIVE DR COEFFS SPLINED FOR
C                           PLOTTING. NMX POINTS BETWEEN THE USER 
C                           SELECTED TEMPERATURE RANGE
C                           (UNITS: CM**3/SEC)
C                           DIMENSION: TEMP INDEX
C          (R*8)  QDRM()  = RATE-COEFFTS. (cm**3/sec) AT 'TFEVM()'
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (L*4)  LTRRNG()= .TRUE.  => OUTPUT 'QTCXA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           .FALSE. => OUTPUT 'QTCXA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TREVA()'.
C                           DIMENSION: RECEIVER/DONOR TEMP. PAIR INDEX
C          (L*4)  LTRNG() = .TRUE.  => OUTPUT 'QDROUT()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'QDROUT()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TEVA()'.
C          (L*4)  LFIT()  = .TRUE.  => OUTPUT 'QDRS()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'QDRS()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      DONOR TEMPERATURE 'TEVA()'.
C          (L*4)  OPEN07  = .TRUE.  => UNIT 7 IS OPEN
C                           .FALSE.  => UNIT 7 IS CLOSED
C          (L*4)  LREP    = .TRUE.  => PAPER.TXT TO BE REPLACED
C                           .FALSE.  => PAPER.TXT NOT TO BE REPLACED
C
C
C********* From  DADATA *************************************************************
C
C          (I*4)  NDPRT   = MAXIMUM NUMBER OF PARENT STATES
C          (I*4)  NDREP   = MAX. NUMBER OF REPRESENTATIVE N-SHELLS
C          (I*4)  NDLEV   = MAXIMUM NUMBER OF RESOLVED LEVELS
C          (I*4)  NDAUG   = MAXIMUM NUMBER OF AUGER RATE INITIAL AND
C                           FINAL PARENT PAIRS
C          (I*4)  NTDIM   = MAX. NUMBER OF ELECTRON TEMPERATURES
C
C          (C*2)  SEQSYM  = RECOMBINED ION SEQ
C          (I*4)  IZ      = RECOMBINED ION CHARGE
C          (I*4)  IZ0     = NUCLEAR CHARGE
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C          (I*4)  NPRNT   = TOTAL NUMBER OF PARENTS
C          (I*4)  NPRNTI  = NUMBER OF PARENTS WHICH ARE INITIAL PARENTS
C          (I*4)  NPRNTF  = NUMBER OF PARENTS WHICH ARE FINAL PARENTS
C          (R*8)  BWNP    = BINDING WAVE NO. OF GROUND PARENT (CM-1)
C          (I*4)  IPA()   = NUMBER OF PARENT ENERGY LEVELS
C          (C*18) CSTRPA()= NOMENCL./CONFIG. FOR PARENT LEVEL 'IPA()'
C          (I*4)  ISPA()  = MULTIPLICITY FOR PARENT LEVEL 'IPA()'
C                           NOTE: (ISPA-1)/2 = QUANTUM NUMBER (SP)
C          (I*4)  ILPA()  = QUANTUM NUMBER (LP) FOR PARENT LEVEL 'IPA()'
C          (R*8)  XJPA()  = QUANTUM NUMBER (JP) FOR PARENT LEVEL 'IPA()'
C                           NOTE: (2*XJPA)+1 = STATISTICAL WEIGHT
C          (R*8)  WPA()   = ENERGY RELATIVE TO PARENT LEVEL 1 (CM-1)
C                           FOR PARENT LEVEL 'IPA()'
C
C          (I*4)  IL      = NUMBER OF ENERGY LEVELS (TERMS) OF
C                           RECOMBINED ION
C          (R*8)  BWNR    = IONISATION POTENTIAL (CM-1) OF LOWEST LEVEL
C                           OF RECOMBINED ION
C          (I*4)  IA()    = RECOMBINED ION ENERGY LEVEL INDEX NUMBER
C          (C*18) CSTRGA()= NOMENCL./CONFIG. FOR RECOMBINED ION LEVEL
C                           'IA()'
C          (I*4)  ISA()   = MULTIPLICITY FOR RECOMBINED LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR RECOMBINED LEVEL
C                           'IA()'
C          (R*8)  XJA()   = QUANTUM NUMBER (J) FOR RECOMBINED LEVEL
C                           'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  WA()    = ENERGY RELATIVE TO RECOMBINED LEVEL 1 (CM-1)
C                           FOR RECOMBINED LEVEL 'IA()'
C          (I*4)  NREP    = NUMBER OF REPRESENTATIVE N-SHELLS
C          (I*4)  IREPA() = REPRESENTATIVE N-SHELL INDEX NUMBER
C          (I*4)  NREPA() = REPRESENTATIVE N-SHELLS
C          (I*4)  IAPRS   = NUMBER OF AUGER RATE INITIAL AND FINAL
C                           PARENT PAIRS
C          (C*10) CAPRS() = AUGER RATE PARENT PAIR STRING
C                           1ST.DIM: PARENT PAIR INDEX
C          (I*40) IPAUG(,)= INITIAL AND FINAL PARENTS FOR AUGER BREAKUPS
C                           1ST.DIM: PARENT PAIR INDEX
C                           2ND.DIM: INITIAL AND FINAL PARENT INDICES
C          (R*8)  AUGA(,) = AUGER RATES (SEC-1)
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: PARENT PAIR INDEX
C          (L*4)  LAUGA(,) = .TRUE. => AUGER RATE PRESENT FOR N-SHELL
C                            .FALSE.=> AUGER RATE NOT PRESENT
C                           1ST.DIM: REPRESENTATIVE N-SHELL INDEX
C                           2ND.DIM: PARENT PAIR INDEX
C          (I*4)  IPRTI() = INITIAL PARENT BLOCK INDEX
C          (C*5)  TPRTI() = INITIAL PARENT BLOCK TERM
C          (I*4)  ISPRTI()= INITIAL PARENT BLOCK SPIN MULTIPLICITY
C          (R*8)  TEA()   = ELECTRON TEMPERATURES (K)
C          (I*4)  NTE     = NUMBER OF TEMPERATURES IN SET
C          (R*8)  DIELR(,,)= TERM SELECTIVE DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: LEVEL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: TEMPERATURE INDEX
C          (L*4)  LDIELR(,)= .TRUE. => DIEL. PRESENT FOR LEVEL INDEX
C                            .FALSE.=> DIEL. NOT PRESENT FOR LEVEL INDEX
C                           1ST.DIM: LEVEL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C          (I*4)  IPRTF(,) = FINAL PARENT BLOCK INDEX
C          (C*5)  TPRTF(,) = FINAL PARENT BLOCK TERM
C          (I*4)  ISPRTF(,)= FINAL PARENT BLOCK SPIN MULTIPLICITY
C          (I*4)  NSYSF(,) = NO,. OF SPIN SYSTEMS BUILT ON FINAL PARENT
C          (I*4)  ISYS(,,) = N-SHELL SPIN SYSTEM INDEX FOR FINAL PARENT
C          (I*4)  ISPSYS(,,)=N-SHELL SPIN SYSTEM FOR FINAL PARENT
C          (R*8)  DIELN(,,,,) =N-SHELL DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: REPR. N-SHELL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: FINAL PARENT INDEX
C                           4TH.DIM: SPIN SYSTEM INDEX
C                           5TH.DIM: TEMPERATURE INDEX
C          (R*8)  LDIELN(,)= .TRUE. => DIEL. PRESENT FOR REPR. N-SHELL
C                            .FALSE.=> DIEL. NOT PRESENT FOR  N-SHELL
C                           1ST.DIM: REPR. N-SHELL INDEX
C                           2ND.DIM: INITIAL PARENT INDEX
C                           3RD.DIM: FINAL PARENT INDEX
C                           4TH.DIM: SPIN SYSTEM INDEX
C          (R*8)  DIELT(,,,) =N-SHELL DIELEC. COEFFTS.(CM3 S-1)
C                           1ST.DIM: INITIAL PARENT INDEX
C                           2ND.DIM: FINAL PARENT INDEX
C                           3RD.DIM: SPIN SYSTEM INDEX
C                           4TH.DIM: TEMPERATURE INDEX
C
C****************************************************************************
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          DASPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          DADATA     ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C          DASETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          DAISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          DASPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          DATITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          DAOUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          DAOUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPERATURE FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8TCON     ADAS      REAL*8 FUNCTION:CONVERT TEMPERATURE FORM
C
C AUTHOR:  Martin O'Mullane
C          K1/1/43
C          JET EXT. 5313
C
C DATE:    08/08/97
C
C VERSION: 1.1
C
C VERSION: 1.2							DATE 20-2-98
C MODIFIED: MARTIN O'MULLANE & RICHARD MARTIN
C           - IMPROVED ERROR HANDLING FOR INVALID SELECTIONS
C           - LOGIC OF CHECKING INPUT CHANGED SUCH THAT ONLY THE SELECTED
C             PLOT VALUES ARE CHECKED
C           - ADDED CODE TO HANDLE TOTALS IN REPRESENTATIVE N-SHELL
C           - MISSING AUGER DATA IS GRAYED OUT RATHER THAN CRASHING
C           - RETAINED DEFAULT SELECTIONS ARE CHECKED FOR COMPATIBLILITY
C		- MODIFIED CALL TO DAISPF.FOR SUCH THAT THE CALL SPANS LESS
C		  THAN 20 LINES (ALLOWS COMPILATION AT GARCHING).
C
C-----------------------------------------------------------------------
      INTEGER    IUNT07 , IUNT10   , PIPEIN  , ISTOP  
      INTEGER    MAXDEG , NMX
      INTEGER    NTDIM  , NDTIN  
      INTEGER    L1     , L2       , IFORM
C-----------------------------------------------------------------------
      REAL*8     DZERO
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( IUNT07 =   7 , IUNT10 = 10  , PIPEIN = 5 )
      PARAMETER( NTDIM  =  10 , NDTIN  = 20  )
      PARAMETER( L1     =   1 , L2     =  2  )
      PARAMETER( MAXDEG =  19 , NMX    = 100 )
C-----------------------------------------------------------------------
      PARAMETER( DZERO  = 1.0D-40 )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      integer    IFIRST       , ILAST        , LEN_NAME
      INTEGER    KPLUS1
      INTEGER    ITVAL        , ITTYP        , IOPT       
      integer    IP_AUG
      integer    IP_RES       , IL_RES
      integer    IN_REP       , IPI_REP      , IPF_REP    , ISP_REP  
      integer    NREP_SEL       
C-----------------------------------------------------------------------
      REAL*8     XMIN         , XMAX         , XMINEV     , XMAXEV     ,
     &           YMIN         , YMAX         ,
     &           TOLVAL
      real*8     tsmin        , tsmax        , tsstep
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , OPEN07       
      LOGICAL    LPEND        , 
     &           LOSEL        , LFSEL      , LGRAPH     , L2FILE       ,
     &           LDSEL        , LGRD1      , LDEF1      ,       
     &           LREP
C-----------------------------------------------------------------------
      CHARACTER  REP*3        , DATE*8       , DSFULL*120  , SAVFIL*80 ,
     &           TITLE*40     , TITLM*80     , TITLX*120   , CADAS*80
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)    , TEVA(NDTIN)   , TKEL(NDTIN)
      REAL*8     QDRIN(NTDIM)  , QDROUT(NDTIN)             
      REAL*8     TFEVM(NMX)    , QDRM(NMX)     , COEF(MAXDEG+1)
      REAL*8     TFEVS(NMX)    , QDRS(NMX)     
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(NDTIN)  , lfit(nmx)
C-----------------------------------------------------------------------
      character cprnt*21       , clev*21
      character cpi*21         , cpf*21     , css*6
      character cpai*21        , cpaf*21  
      character cstrl*1
C-----------------------------------------------------------------------
C

C *********** DATATA definitions ****************
C-----------------------------------------------------------------------
      INTEGER   NDPRT       , NDREP          ,
     &          NDLEV       , NDAUG         
C-----------------------------------------------------------------------
      PARAMETER ( NDPRT = 10 , NDLEV = 1000 , NDREP = 100 )
      PARAMETER ( NDAUG = 8 )
C-----------------------------------------------------------------------
      INTEGER   IZ          , IZ0            , IZ1        ,
     &          NPRNT       , NPRNTI         , NPRNTF     ,
     &          IL          , NREP           , IAPRS
      INTEGER   NTE         , I              
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRT)  , ISPA(NDPRT)    , ILPA(NDPRT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      INTEGER   IREPA(NDREP), NREPA(NDREP)   , IPAUG(NDAUG,2)
      INTEGER   IPRTI(NDPRT), ISPRTI(NDPRT)
      INTEGER   IPRTF(NDPRT,NDPRT) , ISPRTF(NDPRT,NDPRT)
      INTEGER   NSYSF(NDPRT,NDPRT) , ISYS(NDPRT,NDPRT,2)
      INTEGER   ISPSYS(NDPRT,NDPRT,2)
C-----------------------------------------------------------------------
      REAL*8    BWNP        , BWNR
      REAL*8    R8TCON
C-----------------------------------------------------------------------
      REAL*8    XJPA(NDPRT) , WPA(NDPRT)
      REAL*8    XJA(NDLEV)  , WA(NDLEV)
      REAL*8    TEA(NTDIM)
      REAL*8    AUGA(NDREP,NDAUG) , DIELR(NDLEV,NDPRT,NTDIM)
      REAL*8    DIELN(NDREP,NDPRT,NDPRT,2,NTDIM)
      REAL*8    DIELT(NDPRT,NDPRT,2,NTDIM)
C-----------------------------------------------------------------------
      CHARACTER SEQSYM*2    
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRT)*18 , CSTRGA(NDLEV)*18
      CHARACTER CAPRS(NDAUG)*10  , TPRTI(NDPRT)*5
      CHARACTER TPRTF(NDPRT,NDPRT)*5
C-----------------------------------------------------------------------
      LOGICAL   LAUGA(NDREP,NDAUG)
      LOGICAL   LDIELR(NDLEV,NDPRT)
      LOGICAL   LDIELN(NDREP,NDPRT,NDPRT,2)
C-----------------------------------------------------------------------
      REAL*8     TVALS(NTDIM,3)
C-----------------------------------------------------------------------

      DATA OPEN10 /.FALSE./ , OPEN07/.FALSE./
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )

C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------

 100  CONTINUE

         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (VIA IDL PIPE)
C-----------------------------------------------------------------------

      CALL DASPF0( REP , DSFULL , LDSEL )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN
C-----------------------------------------------------------------------

         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF

C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS = 'OLD' )
      OPEN10=.TRUE.

C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------

       CALL DADATA( IUNT10 , NDPRT  , NDREP , NDLEV ,
     &              NDAUG  , NTDIM  ,
     &              SEQSYM , IZ     , IZ0   , IZ1   ,
     &              NPRNT  , NPRNTI , NPRNTF, BWNP  ,
     &              IPA    , CSTRPA , ISPA  , ILPA  , XJPA  ,
     &              WPA    ,
     &              IL     , BWNR   ,
     &              IA     , CSTRGA , ISA   , ILA   , XJA   ,
     &              WA     ,
     &              NREP   , IAPRS  , CAPRS , IPAUG ,
     &              IREPA  , NREPA  , AUGA  , LAUGA ,
     &              IPRTI  , TPRTI  , ISPRTI, DIELR , LDIELR,
     &              IPRTF  , TPRTF  , ISPRTF,
     &              NSYSF  , ISYS   , ISPSYS, DIELN , LDIELN,
     &              DIELT  ,
     &              NTE    , TEA
     &             )

C-----------------------------------------------------------------------
C SET UP ARRAY 'TVALS(,)' - CONTAINS INPUT RECEIVER TEMPS. IN 3 FORMS.
C    NOTE: INPUT DATA SET TEMPERATURES 'TEA()' ARE IN KELVIN.
C-----------------------------------------------------------------------

      DO IFORM=1,3
         CALL XXTCON( L1    , IFORM ,
     &                IZ1   ,
     &                NTE   , TEA   , TVALS(1,IFORM)  )
      END DO

      CLOSE(10)
   

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL PIPE)
C-----------------------------------------------------------------------

  200 CALL DAISPF( LPEND  ,
     &             IZ     , IZ0     , IZ1     ,
     &             NTDIM  , NDTIN   , NTE     , TVALS   ,
     &             ndaug  , iaprs   , ipaug   , lauga  ,
     &		 NDPRT  , NDLEV   ,
     &             NPRNT  , IPA     , CSTRPA  , ISPA    , ILPA  ,
     &             IL     , IA      , CSTRGA  , ISA     , ILA   ,
     &		 LDIELR ,
     &             nprnti , nprntf  , nsysf   ,
     &             iprti  , iprtf   , isys    , ispsys  ,
     &             ndrep  , nrep    , irepa   , nrepa   ,
     &             ldieln ,
     &             TITLE  , IOPT    ,
     &             IP_AUG , IP_RES   , IL_RES  ,
     &             IN_REP , IPI_REP , IPF_REP , ISP_REP ,
     &             ITTYP  , ITVAL   , TIN     ,
     &             LOSEL  , LFSEL   , LGRD1   , LDEF1   ,
     &             TOLVAL ,
     &             XMIN   , XMAX    , YMIN    , YMAX    )


C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 100   

C-----------------------------------------------------------------------
C CONVERT INPUT TEMPERATURES INTO KELVIN, EV  
C-----------------------------------------------------------------------

      CALL XXTCON( ITTYP , L1 , IZ1 , ITVAL , TIN , TKEL )
      CALL XXTCON( ITTYP , L2 , IZ1 , ITVAL , TIN , TEVA )

C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV.
C-----------------------------------------------------------------------

      XMINEV = R8TCON( ITTYP , L2 , IZ1 , XMIN )
      XMAXEV = R8TCON( ITTYP , L2 , IZ1 , XMAX )

C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING TWO-WAY SPLINE
C-----------------------------------------------------------------------

C Depending on option qdr is the resolved DR or bundle-n DR
C nb: if the data is 0.0E+0 the spline fit will fail

      if (iopt.eq.0) then
          
         LFSEL = .FALSE. 
          
      else
      
         if (iopt.eq.1) then

            do i=1, nte
              qdrin(i) = max(dzero,DIELR(IL_RES,IP_RES,i))
            end do 
            
         else
         
           do i=1, nte
             if (in_rep.eq.1) then
                qdrin(i) = max(dzero,
     &                         DIELT(IPI_REP,IPF_REP,ISP_REP,i))
             else
                qdrin(i) = max(dzero,
     &                     DIELN(IN_REP-1,IPI_REP,IPF_REP,ISP_REP,i))
             endif
           end do 
           
         endif

         CALL DASPLN( NTDIM        , NDTIN   ,
     &                NTE          , ITVAL   ,
     &                TVALS(1,L2)  , TEVA    ,
     &                QDRIN        , QDROUT  ,
     &                LTRNG        )
               
C spline the output curve to nmx points
    
         tsmin  = log10(tvals(1,L2))
         tsmax  = log10(tvals(ITVAL,L2))
         tsstep = (tsmax - tsmin )/ (nmx - 1)

         do i=1,nmx
           tfevs(i) = 10.0**( tsmin + tsstep*(i-1) )
         end do

         CALL DASPLN( NTDIM        , NMX     ,
     &                NTE          , NMX     ,
     &                TVALS(1,L2)  , TFEVS   ,
     &                QDRIN        , QDRS    ,
     &                LFIT         )
         
           
        
      endif
       

C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (ITVAL > 2).
C-----------------------------------------------------------------------

      IF (ITVAL.LE.2) LFSEL = .FALSE.
 
      IF (LFSEL) THEN
         CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                ITVAL   , TEVA     , QDROUT  ,
     &                NMX     , TFEVM    , QDRM    ,
     &                KPLUS1  , COEF     ,
     &                TITLM   )
      ENDIF

C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT FILE UNDER ANALYSIS.
C-----------------------------------------------------------------------

      CALL XXSLEN(DSFULL,IFIRST,ILAST)
      LEN_NAME=ILAST-IFIRST+1
      TITLX = ' '
      TITLX(1:LEN_NAME) = DSFULL(IFIRST:ILAST)

C-----------------------------------------------------------------------
C DASPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
C A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------   

 300  CONTINUE 
  
       CALL DASPF1( DSFULL   , 
     &              LFSEL    , LDEF1    ,
     &              LGRAPH   , L2FILE   , SAVFIL   ,
     &              XMIN     , XMAX     , YMIN     , YMAX  ,
     &              LPEND    , CADAS    , LREP     )
     
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------

      IF (LPEND) GOTO 200
C
C----------------------------------------------------------------------
C IF DESIRED - SAVE RESULTS TO FILE AND GRAPH
C----------------------------------------------------------------------

C Make up parent and spin strings for output.
C Also give an arbitrary value, use index 1, to AUGA in the cases where it is
C not used. This is to avoid undefined values of index = -1 which is 
C set in the IDL widget, cw_single_sel. 
         
         if (iopt.eq.0) then
            write(cpai,1100) cstrpa(ipaug(ip_aug,1)),
     &                       ispa(ipaug(ip_aug,1)),
     &                       cstrl(ilpa(ipaug(ip_aug,1)))
            write(cpaf,1100) cstrpa(ipaug(ip_aug,2)),
     &                       ispa(ipaug(ip_aug,2)),
     &                       cstrl(ilpa(ipaug(ip_aug,2)))
            nrep_sel = -99
         elseif (iopt.eq.1) then
            write(cprnt,1100)cstrpa(ip_res),ispa(ip_res),
     &                       cstrl(ilpa(ip_res))
            write(clev,1100) cstrga(il_res),isa(il_res),
     &                       cstrl(ila(il_res))
            ip_aug   = 1
            nrep_sel = -99              
         elseif (iopt.eq.2) then
            write(cpi,1100)  cstrpa(ipi_rep),ispa(ipi_rep),
     &                       cstrl(ilpa(ipi_rep))
            write(cpf,1100)  cstrpa(ipf_rep),ispa(ipf_rep),
     &                       cstrl(ilpa(ipf_rep))
 	    write(css,1102)  isys(ipi_rep,ipf_rep,isp_rep),
     &                       ispsys(ipi_rep,ipf_rep,isp_rep)
            ip_aug   = 1
            nrep_sel = nrepa(in_rep)             
         endif
     
 1100    format(a18,i2,a1)        
 1102    format(i2,'(',i2,')')


      IF (L2FILE) THEN
         IF (LREP.AND.OPEN07)THEN
            OPEN07 = .FALSE.
            CLOSE(IUNT07)
         ENDIF
         IF(.NOT.OPEN07)THEN
            OPEN(UNIT=IUNT07,FILE=SAVFIL,STATUS='UNKNOWN')
            OPEN07 = .TRUE.
         ENDIF
         

               
         CALL DAOUT0( IUNT07   , LFSEL ,
     &                TITLE    , TITLX  , TITLM , DATE      ,
     &                iopt     ,
     &                cpai     , cpaf   , 
     &                nrep     , nrepa  , auga(1,ip_aug)    ,
     &                cprnt    , clev   ,
     &                cpi      , cpf    , css   , nrep_sel  ,
     &                ITVAL    , TEVA   , TKEL  ,
     &                IZ0      , IZ1    , 
     &                LTRNG    , 
     &                QDROUT   ,
     &                KPLUS1   , COEF   , CADAS  )

      ENDIF

C  If selected, generate graph

      IF(LGRAPH) THEN
         CALL DAOUTG( TITLE  , TITLX  , TITLM , DATE  ,
     &                IZ0    , IZ1    ,
     &                iopt   ,
     &                cpai   , cpaf   , 
     &                nrep   , nrepa  , auga(1,ip_aug)   ,
     &                cprnt  , clev   ,
     &                cpi    , cpf    , css   , nrep_sel ,
     &                TEVA   , QDROUT , ITVAL ,
     &                TFEVM  , QDRM   , NMX   ,
     &                TFEVS  , QDRS   , 
     &                LGRD1  , LDEF1  , LFSEL ,
     &                XMIN   , XMAX   , YMIN  , YMAX  )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

         READ(PIPEIN,*) ISTOP
         IF (ISTOP.EQ.1) GOTO 9999
         
      ENDIF

C-----------------------------------------------------------------------
C RETURN TO OUTPUT PANELS. (ON RETURN UNIT 10 IS CLOSED).
C-----------------------------------------------------------------------

      GOTO 300
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE

      IF(OPEN07) CLOSE(IUNT07)

      END
