CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas410/dasumd.for,v 1.1 2004/07/06 13:28:04 whitefor Exp $ Date $Date: 2004/07/06 13:28:04 $
CX
       SUBROUTINE DASUMD ( NDREP  , NDPRT   , NDT    ,
     &                     IPI    , IPF     , IPFS   ,
     &                     MAXTM  , IREPMAX , IREP   , DRMF   , 
     &                     DRMS   
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C **************** FORTRAN 77 SUBROUTINE: DASUMD  **********************
C
C  VERSION: 1.0
C
C  PURPOSE: TO SUM BADNELL DIELECTRONIC RATE COEFFICIENT DATA OVER THE
C           REPRESENTATIVE SET TO GIVE ZERO DENSITY TOTAL RATE
C           FROM SATELLITE LINES.
C
C           BASED ON B4SUMD
C       
C  CALLING PROGRAM: DADATA
C
C
C  INPUT:
C  INPUT :  (I*4) NDREP    = MAXIMUM NUMBER OF REPRESENTATIVE LEVELS
C  INPUT :  (I*4) NDPRT    = MAXIMUM NUMBER OF PARENT STATES
C  INPUT :  (I*4) NDT      = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT :  (I*4) IPI      = INITIAL PARENT
C  INPUT :  (I*4) IPF      = FINAL PARENT
C  INPUT :  (I*4) IPFS     = SOIN SYSTEM
C  INPUT :  (I*4) DRMF(,,) = BADNELL DIELECTRONIC DATA (CM3 S-1)
C                            1ST.DIM: REPR. N-SHELL INDEX
C                            2ND.DIM: INITIAL PARENT INDEX
C                            3RD.DIM: FINAL PARENT INDEX
C                            4TH.DIM: SPIN SYSTEM INDEX
C                            5TH.DIM: TEMPERATURE INDEX
C  INPUT :  (I*4) NBT      = NO. OF TEMPERATURES
C  INPUT :  (I*4) IREPMAX  = NO OF REPRESENTATIVE LEVELS
C  INPUT :  (I*4) IREP()   = SET OF REPRESENTATIVE LEVELS
C
C  OUTPUT:  (R*8) DRMS()   = SUMMED DR RATE COEFFICIENTS (CM3 S-1)
C                            1ST.DIM: INITIAL PARENT INDEX
C                            2ND.DIM: FINAL PARENT INDEX
C                            3RD.DIM: SPIN SYSTEM INDEX
C                            4TH.DIM: TEMPERATURE INDEX
C
C           (I*4) NREP     = GENERAL LEVEL INDEX
C           (I*4) IN       = GENERAL INDEX
C           (I*4) IT       = GENERAL INDEX
C           (R*8) V        = GENERAL VARIABLE FOR N-SHELL
C           (R*8) V1       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) Y        = GENERAL VARIABLE FOR N-SHELL
C           (R*8) Y0       = GENERAL VARIABLE FOR N-SHELL
C           (R*8) Y1       = GENERAL VARIABLE FOR N-SHELL
C
C
C  AUTHOR:   Martin O'Mullane
C            JET
C           
C  DATE:     19-02-98
C
C  VERSION:  1.1						DATE: 03-03-98
C  MODIFIED: RICHARD MARTIN
C		- PUT THROUGH SCCS.
C                 
C-------------------------------------------------------------------
       REAL*8      DMIN
C-------------------------------------------------------------------
       PARAMETER ( DMIN = 1.00D-70 )
C-------------------------------------------------------------------
       INTEGER     NDREP  , NDPRT   , NDT       ,
     &             IPI    , IPF     , IPFS
       INTEGER     MAXTM  , IREPMAX 
       INTEGER     IN     , IT      ,  
     &             NREP   , N0      , N1   
C-------------------------------------------------------------------
       REAL*8      V      , V1      , Y         , Y0       , Y1      
C-------------------------------------------------------------------
       INTEGER     IREP(NDREP)
C-------------------------------------------------------------------
       REAL*8      DRMS(NDPRT,NDPRT,2,NDT)
       REAL*8      DRMF(NDREP,NDPRT,NDPRT,2,NDT)
C-------------------------------------------------------------------


       DO IT = 1,MAXTM
         DRMS(IPI,IPF,IPFS,IT)  = 0.0D+0
       END DO

       DO 100 IT = 1,MAXTM

         DRMS(IPI,IPF,IPFS,IT) = DRMF(1,IPI,IPF,IPFS,IT)

         DO 120 IN = 2,IREPMAX
            N1 =  IREP(IN)
            N0 =  IREP(IN-1)
            V  =  DFLOAT(N0)
            V1 =  DFLOAT(N1)
            Y1 =  DLOG10( DMAX1( DRMF(IN  ,IPI,IPF,IPFS,IT) , DMIN ) )
            Y0 =  DLOG10( DMAX1( DRMF(IN-1,IPI,IPF,IPFS,IT) , DMIN ) )
 
            DO 130 NREP = N0+1, N1

              Y =  Y1 - (N1 - NREP)/(N1-N0) * (Y1 - Y0)
              DRMS(IPI,IPF,IPFS,IT)  =  DRMS(IPI,IPF,IPFS,IT) + 10.0**Y

  130      CONTINUE

  120    CONTINUE
  100  CONTINUE

C       WRITE(6,1104) ( DRMS(IPI,IPF,IPFS,IT) , IT = 1,MAXTM )

       RETURN
C-------------------------------------------------------------------
 1100  FORMAT(1H ,'NDREP=',I3,3X,'NDT=',I3,3X,'MAXTM=',I3,3X,
     &            'IREPMAX=',I3)
 1102  FORMAT(1H , I5, 1P, 12D10.2)
 1104  FORMAT(1H ,'SUM  ', 1P, 12D10.2)
 1110  FORMAT(1H ,3I5,1P,6D10.2)
C-------------------------------------------------------------------
       END
