CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8out0.for,v 1.5 2006/10/06 16:55:59 mog Exp $ Date $Date: 2006/10/06 16:55:59 $
CX
      SUBROUTINE D8OUT0( iunt   , DATE   , dsn03  , dsn35  ,
     &                   opclas , dsnout ,
     &                   TITLE  , IZ0    , IZL    , IZU    ,
     &                   AMIMP  , AMH    ,
     &                   NUMT   , NUMD   ,
     &                   TEVMIN , TEVMAX , DNSMIN , DNSMAX ,
     &                   CADAS
     &                 )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8OUT0 *********************
C
C  PURPOSE: WRITES TEXT OUTPUT TO FILE FOR ADAS408.
C
C  CALLING PROGRAM: ADAS408
C
C  INPUT : (I*4)  iunt      = UNIT NUMBER FOR OUTPUT.
C  INPUT : (C*8)  DATE      = DATE STRING.
C  INPUT : (C*80) DSNIN     = FULL INPUT DATA SET NAME.
C  INPUT : (C*80) DSNPAS    = FULL OUTPUT PASSING DATA SET NAME.
C  INPUT : (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C  INPUT : (I*4)  IZ0       = IMPURITY NUCLEAR CHARGE.
C  INPUT : (I*4)  IZL       = IMPURITY LOWEST ION CHARGE.
C  INPUT : (I*4)  IZU       = IMPURITY HIGHEST ION CHARGE.
C  INPUT : (R*8)  AMIMP     = IMPURITY ATOMIC MASS.
C  INPUT : (R*8)  AMH       = IMPURITY ATOMIC MASS.
C  INPUT : (I*4)  NUMT      = NUMBER OF ELECTRON TEMPERATURE VALUES
C  INPUT : (I*4)  NUMD      = NUMBER OF ELECTRON DENSITY VALUES
C  INPUT : (R*8)  TEVMIN    = MINIMUM ELECTRON TEMPERATURE (EV)
C  INPUT : (R*8)  TEVMAX    = MAXIMUM ELECTRON TEMPERATURE (EV)
C  INPUT : (R*8)  DNSMIN    = MINIMUM ELECTRON DENSITY (CM-3)
C  INPUT : (R*8)  DNSMAX    = MAXIMUM ELECTRON DENSITY (CM-3)
C
C  INPUT : (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C
C          (I*4)  I         = ARRAY INDEX.
C          (C*2)  ELEM      = IMPURITY ELEMENT SYMBOL.
C
C          (L*4)  LFIRST    = FLAGS IF FIRST CALL OF SUBROUTINE.
C                             .TRUE.  => FIRST CALL.
C                             .FALSE. => NOT FIRST CALL.
C          (C*44) DSNCHK    =  PASSING DATA SET FORM FOR PRINTOUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XFESYM     ADAS      READS INPUT DATA SET NAME WHEN RUNNING
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    12/05/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C VERSION: 1.2                          DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C               - INCREASED SIGNIFICANT FIGURES IN FILTER WIDTH
C		  FORMATTING (LINE 1010)
C
C VERSION: 1.3				DATE: 24-03-97
C MODIFIED: MARTIN O'MULLANE		
C		CHANGED FNAME FROM CHAR*6 TO CHAR*7
C
C VERSION: 1.4				DATE: 15-08-03
C MODIFIED: MARTIN O'MULLANE		
C		Updated with adas408 restructure.
C
C VERSION : 1.5				
C DATE    : 04-10-06
C MODIFIED: Hugh Summers		
C		Updated to include zcd, ycd and ecd classses.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    iunt    , IZ0     , IZL     , IZU     ,
     &           NUMT    , NUMD    , I
C-----------------------------------------------------------------------
      REAL*8     AMIMP   , AMH     , 
     &           TEVMIN  , TEVMAX  , DNSMIN  , DNSMAX
C-----------------------------------------------------------------------
      LOGICAL    LFIRST 
C-----------------------------------------------------------------------
      CHARACTER  XFESYM*2
      CHARACTER  DATE*8     , dsn03*80   , dsn35*80  , TITLE*40   ,
     &           ELEM*2     , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL    OPCLAS(13)
C-----------------------------------------------------------------------
      CHARACTER  dsnout(13)*80
C-----------------------------------------------------------------------
      SAVE       LFIRST
C-----------------------------------------------------------------------
      DATA       LFIRST / .TRUE. /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C IF FIRST CALL OF ROUTINE WRITE HEADER.
C-----------------------------------------------------------------------

      IF (LFIRST) THEN
         WRITE(iunt,1000) CADAS(2:80)
         WRITE(iunt,1001) DATE
         WRITE(iunt,1002) DSN03
      ENDIF

      write(iunt, 1020)
      do i = 1, 13
        if (opclas(i)) write(iunt, 1030)dsnout(i)
      end do

C-----------------------------------------------------------------------
C WRITE SUMMARY OF USER INPUT.
C-----------------------------------------------------------------------

      ELEM = XFESYM(IZ0)

      WRITE(iunt,1005) TITLE
      WRITE(iunt,1006) ELEM  , IZ0 , IZL , IZU ,
     &                 AMIMP
      WRITE(iunt,1007) AMH

      WRITE(iunt,1008)
      IF (dsn35.EQ.'NULL') THEN
         WRITE(iunt,1009)
      ELSE
         WRITE(iunt,1010)dsn35
      ENDIF
      
      WRITE(iunt,1011) NUMT,NUMD,TEVMIN,DNSMIN,TEVMAX,DNSMAX
      
C-----------------------------------------------------------------------

 1000 FORMAT(  A79 )
 1001 FORMAT( / , 41( '*' ) ,
     &        '    RUN SUMMARY FOR PROGRAM GENERATING STANDARD   ' ,
     &        41( '*' ) / 1H , 41( '*' ) ,
     &        '   ISONUCLEAR MASTER FILES FROM PARAMETRIC FORMS  ' ,
     &        41( '*' ) / 1H , 53( '*' ) ,
     &        ' ADAS408 - DATE: ' , A8 , 1X , 53( '*' ) / )
 1002 FORMAT( /  /  , 'INPUT PARAMETER FILE     : ', A / )
 1005 FORMAT(  /  , 19( '-' ) , 1X , A40 , 1X , 19( '-' ) // )
 1006 FORMAT( /  /  , 'IMPURITY INFORMATION:' /
     &        1H  , '---------------------' /
     &        1H , ' ELEMENT SYMBOL         =' , 6X ,A2   /
     &        1H , ' NUCLEAR CHARGE         =' , 2X ,I6   /
     &        1H , ' LOWEST ION CHARGE      =' , 2X ,I6   /
     &        1H , ' HIGHEST ION CHARGE     =' , 2X ,I6   /
     &        1H , ' ATOMIC MASS NUMBER     =' , 2X ,F6.2  )
 1007 FORMAT( /  /  , 'NEUTRAL DONOR INFORMATION:' /
     &        1H  , '--------------------------' /
     &        1H , ' ELEMENT SYMBOL         =' , 6X ,' H'    /
     &        1H , ' NUCLEAR CHARGE         =' , 2X ,'     1'/
     &        1H , ' ATOMIC MASS NUMBER     =' , 2X ,F6.2  )
 1008 FORMAT( /  /  , 'FILTER INFORMATION:' /
     &          1H  , '-------------------' /)
 1009 FORMAT('  No filter used')
 1010 FORMAT( 1H , 'Filter from : ', A)
 1011 FORMAT( /  /  , 'ELECTRON TEMPERATURE/DENSITY INFORMATION:' /
     &        1H  , '-----------------------------------------' //
     &        1H , 26X , 'TEMPERATURE (EV)' , 6X , 'DENSITY (CM-3)' /
     &        1H , 26X , 16( '-' ) , 6X , 14 ('-') /
     &        1H , ' NUMBER OF VALUES       =' , 8X ,I2   , 19X , I2  /
     &        1H , ' MINIMUM VALUE          =' , 2X ,1P,D12.4,9X,D12.4/
     &        1H , ' MAXIMUM VALUE          =' , 2X ,1P,D12.4,9X,D12.4//
     &        1H , ' (NOTE: EQUAL INTERVALS IN THE LOGARITHM ARE SET)')

 1020 FORMAT( /  /  , 'Output files:' /)
 1030 FORMAT( 1H , A)
 
C-----------------------------------------------------------------------

      RETURN
      END
