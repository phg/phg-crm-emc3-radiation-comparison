CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8ispf.for,v 1.2 2004/07/06 13:25:03 whitefor Exp $ Date $Date: 2004/07/06 13:25:03 $
CX
      SUBROUTINE D8ISPF( LPEND  , 
     &                   TITLE  , AMSI   , AMSH   ,
     &                   LEVCUT , IEVCUT , THBE   , THSI   , FNAME   ,
     &                   NUMT   , NUMD   , ITTYP  ,
     &                   TINMIN , TINMAX , DINMIN , DINMAX ,
     &                   ITDIMD , IDDIMD , IZ0
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS408
C
C  INPUT : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT : (I*4)  IDDIMD    = MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  IZ0       = NUCLEAR CHARGE
C  OUTPUT: (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (R*8)   AMSI     = IMPURITY ATOMIC MASS.
C  OUTPUT: (R*8)   AMSH     = HYDROGEN ATOMIC MASS.
C  OUTPUT: (L*4)   LEVCUT   = .TRUE.  => USE SIMPLE CUT-OFF ENERGY.
C                             .FALSE. => USE BE/SI FILTER CHARACTERISTIC
C  OUTPUT: (I*4)   IEVCUT   = ENERGY FOR SIMPLE CUT-OFF (EV)
C  OUTPUT: (R*8)   THBE     = BERYLLIUM FILTER THICKNESS (MICRONS)
C  OUTPUT: (R*8)   THSI     = SILICON FILTER THICKNESS (CM)
C  OUTPUT: (C*7)   FNAME    = FILTER NAME TO BE USED AS FIELD IN MASTER
C
C  OUTPUT: (I*4)   NUMT     = NUMBER OF TEMPERATURES TO BE USED IN SCAN
C  OUTPUT: (I*4)   NUMD     = NUMBER OF DENSITIES
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C  OUTPUT: (R*8)   TINMIN   = MINIMUM ELECTRON TEMPERATURE FOR SCAN
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C  OUTPUT: (R*8)   TINMAX   = MAXIMUM ELECTRON TEMPERATURE FOR SCAN
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C  OUTPUT: (R*8)   DINMIN   = MINIMUM ELECTRON DENSITY FOR SCAN (CM-3)
C  OUTPUT: (R*8)   DINMAX   = MAXIMUM ELECTRON DENSITY FOR SCAN (CM-3)
C                             FILE NAMES.
C
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          XXFLSH    IDL-ADAS   CALLS XXFLSH COMMAND TO CLEAR PIPE.
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    10/05/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 04-04-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2				DATE: 24-03-97
C MODIFIED: MARTIN O'MULLANE		
C		CHANGED FNAME FROM CHAR*6 TO CHAR*7
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IDDIMD        , ITDIMD   , IZ0
      INTEGER    I4UNIT
      INTEGER    PIPEIN        , PIPEOU   ,
     &           I             , J        , K                        ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
      INTEGER    IEVCUT  ,
     &           NUMT    , NUMD    , ITTYP
C-----------------------------------------------------------------------
      REAL*8     AMSI    , AMSH    ,
     &           THBE    , THSI    ,
     &           TINMIN  , TINMAX  , DINMIN  , DINMAX
C-----------------------------------------------------------------------
      LOGICAL    LPEND   , LEVCUT
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40, FNAME*7
C
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) ITDIMD
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IDDIMD
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C  READ INPUTS FROM PIPE FROM IDL
C-----------------------------------------------------------------------
C
      READ(PIPEIN, *) ILOGIC
      IF (ILOGIC .EQ. 1 ) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
          READ(PIPEIN, '(A40)') TITLE
	  READ(PIPEIN,*)AMSI
	  READ(PIPEIN,*)AMSH
	  READ(PIPEIN,*)ILOGIC
	  IF(ILOGIC .EQ. 0) THEN
             LEVCUT=.TRUE.
          ELSE
             LEVCUT=.FALSE.
          ENDIF
	  READ(PIPEIN,*)IEVCUT
	  READ(PIPEIN,*)THBE
	  READ(PIPEIN,*)THSI
	  READ(PIPEIN,'(A7)')FNAME
	  READ(PIPEIN,*)NUMT
	  READ(PIPEIN,*)NUMD
	  READ(PIPEIN,*)ITTYP
	  READ(PIPEIN,*)TINMIN
	  READ(PIPEIN,*)TINMAX
	  READ(PIPEIN,*)DINMIN
	  READ(PIPEIN,*)DINMAX
      ENDIF

C
C-----------------------------------------------------------------------
C
 1000 FORMAT( / 1X , 31('*') , ' D8ISPF MESSAGE ' , 31('*') //
     &        ' OUTPUT MASTER PASSING FILE FORM: ',A22           )
 1001 FORMAT( 1X , 'SUCH FILES ALREADY EXIST BUT WILL BE REPLACED' )
 1002 FORMAT( / 1X , 31('*') , ' END OF MESSAGE ' , 31('*') / )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
