CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/ioadas2.for,v 1.4 2015/12/21 14:33:11 mog Exp $ Date $Date: 2015/12/21 14:33:11 $
CX
       SUBROUTINE IOADAS2( iunt   , DATE    ,
     &                     NTDIM  , NNEDIM  , NZDIM  , 
     &                     DATA0  , DATA    ,
     &                     ITMAX  , IDMAX   , IZMAX  ,
     &                     DENSE  , TEMPE   ,
     &                     IZ0    , NAME    , METHOD ,
     &                     CSTRG1 , CSTRG2  , LINFO  ,
     &                     user   , type    , dsn03  , dsn35 , lfilter
     &                   )
       implicit none
 
C-----------------------------------------------------------------------
C
C PURPOSE : TO WRITE ADAS DATA WHICH IS DEPENDENT ON DENSITY
C           AND HAS SEPARATE 'NEUTRAL' STAGE.
C
C           THE DATA IS IN THE FORM :-
C                        DATA(IT,ID,IZ)
C                        DATA0(IT,ID)
C           WHERE,
C                  IT     :  TEMPERATURE INDEX ( 1 - ITMAX )
C                  ID     :  DENSITY     INDEX ( 1 - IDMAX )
C                  IZ     :  STAGE       INDEX ( 1 - ITMAX )
C
C           WITH ELECTRON TEMPERATUES ---- TEMPE(1 - ITMAX)
C                ELECTRON DENSITIES   ---- DENSE(1 - IDMAX)
C
C           (I*4)  iunt    =  STREAM NUMBER (PREVIOUSLY ALLOCATED)
C
C
C           INPUT
C           ~~~~~
C           (R*4)  DATA   :  PROFILE ARRAY (SEE ABOVE)
C           (R*4)  DATA0  :  PROFILE ARRAY (SEE ABOVE)
C           (I*4)  ITMAX  :  NUMBER OF TEMPERATURE INDICIES
C           (I*4)  IDMAX  :  NUMBER OF DENSITY     INDICIES
C           (I*4)  IZMAX  :  NUMBER OF STAGE       INDICIES
C           (R*4)  DENSE  :  ELECTRON DENSITIES
C           (R*4)  TEMPE  :  ELECTRON TEMPERATURES
C           (I*4)  IZ0    :  NUCLEAR CHARGE OF SEPCIES
C           (C*13) NAME   :  NAME OF ELEMENT
C           (C*25) METHOD :  METHOD USED IN THE CALCULATIONS
C           (C*10) CSTRG1 :  FIRST PARENT/GROUND INFORMATION STRING
C           (C*10) CSTRG2 :  SECOND PARENT/GROUND INFORMATION STRING
C           (C*8)  LINFO  :  WAVELENGTH IDENTIFIER FOR SPECIFIC LINE
C
C
C BASED ON IOADAS2 BY
C          JAMES SPENCE
C          JET/TESSELLA SUPPORT SERVICES PLC  23/3/90
C
C MODIFIED TO WRITE ONLY
C          M. O'MULLANE    11/8/92
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C
C VERSION: 1.2				DATE: 21-07-2003
C MODIFIED: Martin O'Mullane
C            - Add a comment section at the end which requires user, date
C              and adf03/adf35 filenames.
C            - Remove redundant variables.
C
C VERSION: 1.3				DATE: 17-05-2007
C MODIFIED: Allan Whiteford
C           - Updated comments as part of subroutine documentation
C             procedure.
C
C VERSION : 1.4				
C DATE    : 21-12-2015
C MODIFIED: Martin O'Mullane
C            - The adf03 file was used to find the length of the adf35 
C              file when writing the comments.
C
C-----------------------------------------------------------------------
       integer    ntdim     , nnedim    , nzdim     ,
     &            iunt      , iz0       , idmax     , itmax     ,
     &            izmax     , id        , it        , iz        ,
     &            iz1       ,
     &            l1        , l2        , l3        , l4
C-----------------------------------------------------------------------
       character  adfcode*5 , date*8    , cstrg1*10 , cstrg2*10 ,
     &            name*13   , method*24 , user*30   , dsn03*80  , 
     &            dsn35*80  , type*40
C-----------------------------------------------------------------------
       logical    lfilter
C-----------------------------------------------------------------------
       character  linfo(nzdim)*8
C-----------------------------------------------------------------------
       real*8     data0(ntdim,nnedim)   , data(ntdim,nnedim,nzdim) , 
     &            tempe(ntdim),dense(nnedim)
C-----------------------------------------------------------------------
       data       adfcode/'ADF11'/
C-----------------------------------------------------------------------

 
       DATE(3:3) = '.'
       DATE(6:6) = '.'
 
       WRITE(iunt,1000)IZ0,IDMAX,ITMAX,1,IZMAX,NAME,METHOD,ADFCODE
       WRITE(iunt,1100)
       WRITE(iunt,1200)(DENSE(ID),ID = 1,IDMAX)
       WRITE(iunt,1200)(TEMPE(IT),IT = 1,ITMAX)
 
       DO IZ1 = 1,IZMAX
         IZ = IZ1 - 1
         WRITE(iunt,1300)CSTRG1,CSTRG2,LINFO(IZ1),IZ1,DATE
         DO IT = 1,ITMAX
           IF (IZ1.EQ.1) THEN
              WRITE(iunt,1400)(DATA0(IT,ID),ID = 1,IDMAX)
           ELSE
              WRITE(iunt,1400)(DATA(IT,ID,IZ),ID = 1,IDMAX)
           ENDIF
         END DO
       END DO
 

C Write producer and other information
 
       write(iunt, 3000)
       
       call xxslen(type, L1, L2)
       call xxslen(dsn03, L3, L4)
       
       write(iunt, 3010)type(l1:l2), dsn03(l3:l4)
       
       if (lfilter) then 
          call xxslen(dsn35, L3, L4)
          write(iunt, 3020)dsn35(L3:L4)
       endif
       
       write(iunt, 3030)'ADAS408', user, date
       

       
C-----------------------------------------------------------------------
 1000  FORMAT(5I5,5X,'/',A13,6X,'/',A24,A5)
 1100  FORMAT(' -----------------------------------------------------',
     &        '--------------------------')
 1200  FORMAT(8(1X,F9.5))
 1300  FORMAT(' --------------------/', 1A10 ,'/', 1A10 ,'/',1A8,
     &        '/ Z1=',I2,'   / DATE= ',A8)
 1400  FORMAT(8F10.5)
 

 3000 FORMAT('C',79('-'), /, 'C')
 3010 FORMAT('C  Generate ', A, ' from : ',/,'C',5x,A)
 3020 FORMAT('C  with filter : ', A,/,'C')
 3030 FORMAT('C',/,
     &       'C  CODE     : ',1A7/
     &       'C  PRODUCER : ',A30/
     &       'C  DATE     : ',1A8,/,'C',/,'C',79('-'))
C-----------------------------------------------------------------------
 
       END
