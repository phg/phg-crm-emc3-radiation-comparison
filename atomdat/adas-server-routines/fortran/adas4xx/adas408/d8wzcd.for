         subroutine d8wzcd( iuntz     , iunty     , iunte   ,
     &                      lzcd      , lycd      , lecd    ,
     &                      itdimd    , iddimd    ,
     &                      izdimd    , iodimd    , imdimd  ,
     &                      itmax     , idmax     ,
     &                      dtev      , ddens     ,
     &                      iz0       , izl       , izu     ,
     &                      user      , date
     &                    )

      implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: d8wzcd *********************
C
C  purpose:  To create zcd, ycd and ecd files for unresolved baseline.
C
C  calling program: adas408
C
C
C  subroutine:
C
C  input : (i*4)  iuntz     = unit for zcd file output
C  input : (i*4)  iunty     = unit for ycd file output
C  input : (i*4)  iunte     = unit for ecd file output
C  input : (i*4)  iunte     = unit for ecd file output
C  input : (l*4)  lzcd      = .true.  => output zcd file
C                             .false. => do not output zcd file
C  input : (l*4)  lycd      = .true.  => output ycd file
C                             .false. => do not output ycd file
C  input : (l*4)  lecd      = .true.  => output ecd file
C                             .false. => do not output ecd file
C  input : (i*4)  itdimd    = maximum number of temperatures
C  input : (i*4)  iddimd    = maximum number of densities
C  input : (i*4)  izdimd    = maximum number of charge states
C  input : (i*4)  itmax     = number of temperatures
C  input : (i*4)  idmax     = number of densities
C  input : (r*8)  dtev()    = temperature set of tables (ev) - log mesh
C  input : (r*8)  ddens()   = density set of tables (cm-3) - log mesh
C  input : (i*4)  iz0       = nuclear charge
C  input : (i*4)  izl       = first included ion (=0 for neutral)
C  input : (i*4)  izu       = last included ion (=iz0 for bare nucleus)
C  input : (c*30) user      = producer
C  input : (c*8)  date      = date string.
C
C  routines:
C          routine    source    brief description
C          -------------------------------------------------------------
C          i4unit     adas      fetch unit number for output of messages
C          xxword     adas      parses a string into separate words
C          xxopen     adas      check existence and open a file
C          xxrmve     adas      removes occurrences of a char. in string
C          xxmkrp     adas      create the root partition text lines
C          xxslen     adas      finds the length of a string excluding
C                               leading and trailing blanks
C          xfesym     adas      fetch the chemical symbol of an element
C          xfelem     adas      fetch the name of an element
C          xxdata_00  adas      read an adf00 dataset
C
C
C author:  H. P. Summers, university of strathclyde
C          ja7.08
C          tel. 0141-548-4196
C
C date:    06/10/06
C
C
C version  : 1.1
C date     : 06-10-2006
C modified : Hugh Summers
C              - first version
C
C version  : 1.2
C date     : 16-01-2007
C modified : Hugh Summers
C              - adjustment to ecd part to include z1=0 quasi-state
C                for the neutral creation energy. Use new version of
C                xxdata_00.for to handle metastable resolved cases.
C
C version  : 1.3
C date     : 08-03-2007
C modified : Hugh Summers
C              - adjustment of first output file line to include adf no
C                and remove class from ion header lines.
C
C VERSION : 1.4
C DATE    : 09-04-2010
C MODIFIED: Martin O'Mullane
C           - Change integer*4 to integer.
C
C VERSION : 1.5
C DATE    : 18-09-2012
C MODIFIED: Martin O'Mullane
C           - New variables to support latest version of xxdata_00.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      integer   ndclas   , ndshel   , ndstack
      integer   iunt15
      integer   izdim_    , iodim_    , imdim_
C-----------------------------------------------------------------------
      logical   lptn
C-----------------------------------------------------------------------
      parameter ( ndclas = 3 , ndstack = 40 )
      parameter ( lptn = .false. )
      parameter ( iunt15 = 15 )
      parameter ( izdim_ = 80, iodim_=128, imdim_=4)
C-----------------------------------------------------------------------
      integer   iuntz     , iunty     , iunte
      integer   iddimd    , itdimd
      integer   izdimd    , iodimd    , imdimd
      integer   i4unit    , iclass
      integer   iz0       , izl       , izu       , is1min    , is1max
      integer   itmax     , idmax
      integer   ifirst    , ilast
      integer   i         , it        , id
      integer   iptnl     , ncptn_stack
C-----------------------------------------------------------------------
      real*8    z         , wf
C-----------------------------------------------------------------------
      logical   lzcd      , lycd          , lecd
      logical   lexist    , lresol        , lquno
C-----------------------------------------------------------------------
      character  dsfull*120    , coupling*2
      character  c2*2          , c80*80        , c2b*2         , c2p*2
      character  cterm*80      , cblank*80
      character  xfelem*12     , elem_symb*2   , elem_name*12  , date*8
      character  xfesym*2      , user*30       , adascent*120
C-----------------------------------------------------------------------
      integer    iorba(izdim_)     , na(izdim_,iodim_)         ,
     &           la(izdim_,iodim_) , iqa(izdim_,iodim_)
      integer    imeta(iodim_)     , imeta_actual(iodim_)
      integer    iorbma(izdim_,imdim_) ,
     &           nma(izdim_,iodim_,imdim_)    ,
     &           lma(izdim_,iodim_,imdim_)    ,
     &           iqma(izdim_,iodim_,imdim_)   ,
     &           isa(izdim_)                  , ila(izdimd)    ,
     &           isma(izdim_,imdim_)          , ilma(izdim_,imdim_)
C-----------------------------------------------------------------------
      real*8     ddens(iddimd)   , dtev(itdimd)
      real*8     bwnoa(izdim_)   , eeva(izdim_)  , eevma(izdimd,iodim_)
      real*8     xja(izdim_)     , xjma(izdim_,imdim_)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      character  cclass(ndclas)*3   , cptrn0(ndclas)*4
      character  cptrn3(ndclas)*4   , cptrn4(ndclas)*4
      character  cptrn5(ndclas)*4   , cptrn6(ndclas)*4
      character  cptrn7(ndclas)*4
      character  unitsa(ndclas)*12
      character  cz(2)*2
      character  cptn_stack(ndstack)*80
      character  cstr_std(izdim_)*100
      character  cstrm_std(izdim_,imdim_)*100
C-----------------------------------------------------------------------
      data       cterm /'-----------------------------------------------
     &---------------------------------'/
      data       cblank/'
     &                                 '/
      data       cclass/'zcd' , 'ycd' ,'ecd' /
      data       cptrn0/'/ZCD', '/YCD','/ECD'/
      data       cptrn3/'IGRD', 'IGRD','IGRD'/
      data       cptrn4/'IPRT', 'IPRT','IPRT'/
      data       cptrn5/'ZCD ', 'YCD ','ECD '/
      data       cptrn6/'ISPB', 'ISPB','ISPB'/
      data       cptrn7/'ISPP', 'ISPP','ISPP'/
      data       unitsa/'            ','            ','eV          '/
      data       cz    /'Z1'  , 'S1'         /
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      is1min=izl+1
      is1max=izu+1
C------------------------------------------------------------------------
C  fetch the ionisation potential data for the ecd file
C-----------------------------------------------------------------------

      elem_symb = xfesym(iz0)
      elem_name = xfelem(iz0)

      call getenv("ADASCENT",adascent)
      call xxrmve(adascent//'/adf00/'//
     &            elem_symb//'.dat',
     &            dsfull,' ')

      call xxslen(dsfull,ifirst,ilast)
      call xxopen(iunt15, dsfull(ifirst:ilast),  lexist)

      if (.not.lexist) then
          write(i4unit(-1),1001)dsfull(ifirst:ilast)
          write(i4unit(-1),1002)
          stop
      endif

      call xxdata_00( iunt15    , dsfull ,
     &                izdim_    , iodim_ , imdim_ ,
     &                elem_symb , iz0    , wf     ,
     &                bwnoa     , eeva   ,
     &                iorba     , na     , la     , iqa    ,
     &                cstr_std  ,
     &                isa       , ila    , xja    ,
     &                imeta     , imeta_actual    , eevma  ,
     &                iorbma    , nma    , lma    , iqma   ,
     &                cstrm_std ,
     &                isma      , ilma   , xjma   ,
     &                lexist    , lresol , lquno  , coupling
     &              )

C------------------------------------------------------------------------
C  make the root partition if required
C-----------------------------------------------------------------------

       if (lptn) then
          iptnl = 1
          call xxmkrp( ndstack       ,
     &                 iz0           , iptnl       ,
     &                 ncptn_stack   , cptn_stack
     &               )
      endif


C-----------------------------------------------------------------------
C write zcd output on unit iuntz
C-----------------------------------------------------------------------
      if(lzcd) then

            iclass = 1

             c80=cblank
             write(c80(4:5),'(i2)') iz0
             write(c80(9:10),'(i2)')idmax
             write(c80(14:15),'(i2)')itmax
             write(c80(19:20),'(i2)')is1min
             write(c80(24:25),'(i2)')is1max
             write(c80(31:43),'(a1,a12)')'/',elem_name
             write(c80(54:57),'(a4)')cptrn0(iclass)
             write(c80(59:80),'(a22)')'/GCR-PROJECT     ADF11'

             write(iuntz,'(1a80)')c80
             write(iuntz,'(1a80)')cterm

             if(lptn) then
                 do i=1,ncptn_stack
                   write(iuntz,'(1a80)')cptn_stack(i)
                 enddo
                 write(iuntz,'(1a80)')cterm
             endif

             write(iuntz,'(8f10.5)')(ddens(i),i=1,idmax)
             write(iuntz,'(8f10.5)')(dtev(i),i=1,itmax)

             do i=is1min,is1max+1
               z=dfloat(i)-1.0d0
               write(c2,'(i2)')i
               c80=cterm
               write(c80(44:52),'(a9)')'/--------'
               if(lptn) then
                   write(c80(53:62),'(a2,a2,a1,a2,a3)')
     &                              '/ ',cz(2),'=',c2,'   '
               else
                   write(c80(53:62),'(a2,a2,a1,a2,a3)')
     &                              '/ ',cz(1),'=',c2,'   '
               endif
               write(c80(63:80),'(a8,a8,a2)')'/ DATE= ',date,'  '
               write(c2b,'(i2)')1
               write(c2p,'(i2)')0
               if(lptn) then
                   write(c80(22:33),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn6(iclass),'=',c2b,'  /'
                   write(c80(33:44),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn7(iclass),'=',c2p,'  /'
               else
                   write(c80(22:33),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn3(iclass),'=',c2b,'  /'
                   write(c80(33:44),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn4(iclass),'=',c2p,'  /'
               endif
C---------------------------------------------------

               write(iuntz,'(1a80)')c80
               do it = 1 , itmax
                 write(iuntz,'(1p8d10.3)')( z , id = 1 , idmax)
               enddo
             enddo

             c80=cterm
             write(c80(1:1),'(a1)')'C'

             write(iuntz,'(1a80)')c80
             write(iuntz,2007)cclass(iclass),unitsa(iclass),
     &                        'none',' '
             call xxslen(user,ifirst,ilast)
             write(iuntz,2008)'adas408   ',user(ifirst:ilast),date
             write(iuntz,'(1a80)')c80

      endif

C-----------------------------------------------------------------------
C write ycd output on unit iuntz
C-----------------------------------------------------------------------
      if(lycd) then

            iclass = 2

             c80=cblank
             write(c80(4:5),'(i2)') iz0
             write(c80(9:10),'(i2)')idmax
             write(c80(14:15),'(i2)')itmax
             write(c80(19:20),'(i2)')is1min
             write(c80(24:25),'(i2)')is1max
             write(c80(31:43),'(a1,a12)')'/',elem_name
             write(c80(54:57),'(a4)')cptrn0(iclass)
             write(c80(59:80),'(a22)')'/GCR-PROJECT     ADF11'

             write(iunty,'(1a80)')c80
             write(iunty,'(1a80)')cterm

             if(lptn) then
                 do i=1,ncptn_stack
                   write(iunty,'(1a80)')cptn_stack(i)
                 enddo
                 write(iunty,'(1a80)')cterm
             endif

             write(iunty,'(8f10.5)')(ddens(i),i=1,idmax)
             write(iunty,'(8f10.5)')(dtev(i),i=1,itmax)

             do i=is1min,is1max+1
               z=dfloat(i)-1.0d0
               write(c2,'(i2)')i
               c80=cterm
               write(c80(44:52),'(a9)')'/--------'
               if(lptn) then
                   write(c80(53:62),'(a2,a2,a1,a2,a3)')
     &                              '/ ',cz(2),'=',c2,'   '
               else
                   write(c80(53:62),'(a2,a2,a1,a2,a3)')
     &                              '/ ',cz(1),'=',c2,'   '
               endif
               write(c80(63:80),'(a8,a8,a2)')'/ DATE= ',date,'  '
               write(c2b,'(i2)')1
               write(c2p,'(i2)')0
               if(lptn) then
                   write(c80(22:33),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn6(iclass),'=',c2b,'  /'
                   write(c80(33:44),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn7(iclass),'=',c2p,'  /'
               else
                   write(c80(22:33),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn3(iclass),'=',c2b,'  /'
                   write(c80(33:44),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn4(iclass),'=',c2p,'  /'
               endif
C---------------------------------------------------

               write(iunty,'(1a80)')c80
               do it = 1 , itmax
                 write(iunty,'(1p8d10.3)')( z**2 , id = 1 , idmax)
               enddo
             enddo

             c80=cterm
             write(c80(1:1),'(a1)')'C'

             write(iunty,'(1a80)')c80
             write(iunty,2007)cclass(iclass),unitsa(iclass),
     &                        'none',' '
             call xxslen(user,ifirst,ilast)
             write(iunty,2008)'adas408   ',user(ifirst:ilast),date
             write(iunty,'(1a80)')c80

      endif

C-----------------------------------------------------------------------
C write ecd output on unit iuntz
C-----------------------------------------------------------------------
      if(lecd) then

             iclass = 3

             c80=cblank
             write(c80(4:5),'(i2)') iz0
             write(c80(9:10),'(i2)')idmax
             write(c80(14:15),'(i2)')itmax
             write(c80(19:20),'(i2)')is1min
             write(c80(24:25),'(i2)')is1max
             write(c80(31:43),'(a1,a12)')'/',elem_name
             write(c80(54:57),'(a4)')cptrn0(iclass)
             write(c80(59:80),'(a22)')'/GCR-PROJECT     ADF11'

             write(iunte,'(1a80)')c80
             write(iunte,'(1a80)')cterm

             if(lptn) then
                 do i=1,ncptn_stack
                   write(iunte,'(1a80)')cptn_stack(i)
                 enddo
                 write(iunte,'(1a80)')cterm
             endif

             write(iunte,'(8f10.5)')(ddens(i),i=1,idmax)
             write(iunte,'(8f10.5)')(dtev(i),i=1,itmax)

             do i=is1min,is1max+1
               z=dfloat(i-1)-1.0d0
               write(c2,'(i2)')i-1
               c80=cterm
               write(c80(44:52),'(a9)')'/--------'
               if(lptn) then
                   write(c80(53:62),'(a2,a2,a1,a2,a3)')
     &                              '/ ',cz(2),'=',c2,'   '
               else
                   write(c80(53:62),'(a2,a2,a1,a2,a3)')
     &                              '/ ',cz(1),'=',c2,'   '
               endif
               write(c80(63:80),'(a8,a8,a2)')'/ DATE= ',date,'  '
               write(c2b,'(i2)')1
               write(c2p,'(i2)')1
               if(lptn) then
                   write(c80(22:33),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn6(iclass),'=',c2b,'  /'
                   write(c80(33:44),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn7(iclass),'=',c2p,'  /'
               else
                   write(c80(22:33),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn3(iclass),'=',c2b,'  /'
                   write(c80(33:44),'(a2,a4,a,a2,a3)')
     &             '/ ',cptrn4(iclass),'=',c2p,'  /'
               endif
C---------------------------------------------------

               write(iunte,'(1a80)')c80
               if(i.eq.is1min) then
                   do it = 1 , itmax
                     write(iunte,'(1p8d10.3)')
     &                     ( 0.0d0 , id = 1 , idmax)
                   enddo
               else
                   do it = 1 , itmax
                     write(iunte,'(1p8d10.3)')
     &                     ( eeva(i-1) , id = 1 , idmax)
                   enddo
               endif
             enddo


             c80=cterm
             write(c80(1:1),'(a1)')'C'

             write(iunte,'(1a80)')c80
             call xxslen(dsfull,ifirst,ilast)
             write(iunte,2007)cclass(iclass),unitsa(iclass),
     &                        'none',dsfull(ifirst:ilast)
             call xxslen(user,ifirst,ilast)
             write(iunte,2008)'adas408   ',user(ifirst:ilast),date
             write(iunte,'(1a80)')c80


      endif

      return
C
C-----------------------------------------------------------------------
C
 1001 format(1x,30('*'), '    d8wzcd error    ',30('*')//
     &       2x,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,30('*') ,'   d8wzcd warning   ',29('*')//
     &       2x,a,a,a)
 1004 format(/1x,29('*'),' program continues  ',29('*'))

 2007 format('C',/,
     &       'C    Rate coefficients : adf11 class ',1a3,/,
     &       'C    Units             : ',1a12,/,
     &       'C    Status            : special generation',/,
     &       'C    Parent dataset    : ',a,/,
     &       'C    Reference dataset : ',a
     &      )
 2008 format('C',/,
     &       'C    Code              : ',1a10,/,
     &       'C    Producer          : ',a,/,
     &       'C    Date              : ',1a8,/,
     &       'C',/
     &      )
C
C-----------------------------------------------------------------------
C

      end
