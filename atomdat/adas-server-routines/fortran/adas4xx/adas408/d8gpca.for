C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8gpca.for,v 1.4 2007/05/18 08:52:58 allan Exp $ Date $Date: 2007/05/18 08:52:58 $
       SUBROUTINE D8GPCA  (TEA    , IZ1    ,
     &                     ITYPE  , N0     , V0     ,
     &                     EIJ_in , FIJ    , EDISPG , SCALEG ,
     &                     PHFRAC , CORFAC , NCUT_in, NG ,
     &                     ALF )
 
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8GPCA *********************
C
C PURPOSE :  ROUTINE TO PROVIDE BURGESS GENERAL PROGRAM RESULTS
C            AT A GIVEN TEMPERATURES AND AT ZERO DENSITY.
C
C            EQUAL THE GENERAL FORMULA RESULTS AS FAR AS POSSIBLE
C            BY MODIFICATION OF BETHE CORRECTIONS VIA A SINGLE
C            SCALING PARAMETER CORFAC.
C            THE CORRECTION FACTORS USED IN THE GENERAL PROGRAM
C            ARE OBTAINED BY ADJUSTMENT OF STANDARD SETS FOR SPECIFIC
C            TYPES OF TRANSITION.  THE ADJUSTMENT IS
C            (NEW COR(J))=EXP(-CORFAC/(L**DF+0.5))*(STANDARD COR(J)
C            THE STANDARD COR'S ARE AS FOLLOWS:
C
C  TYPE    TRANSITION                     COR'S                     DF
C    1   NI=1,NJ>=2,LJ=LI+1:       0.05,0.30,0.50,0.90              2.0
C    2   NI=2,NJ=3,LJ=LI+1:        0.01,0.02,0.20,0.40,0.70,0.90    1.0
C    3   NI=2,NJ=3,LJ=LI-1:        0.01,0.01,0.01,0.08,0.30,0.70    1.0
C    4   NJ-NI=0, LJ=LI+1 :        0.30,0.35,0.40,0.45,0.70,0.90    0.5
C    5   NJ-NI=0, LJ=LI-1 :        0.30,0.35,0.40,0.45,0.70,0.90    0.5
C    6   NJ-NI>0, LJ=LI+1 :        0.01,0.02,0.20,0.40,0.70,0.90    1.0
C    7   NJ-NI>0, LJ=LI-1 :        0.01,0.01,0.01,0.08,0.30,0.70    1.0
C
C  CALLING PROGRAM: ADAS408
C
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZ1       = RECOMBINING ION CHARGE
C
C          (I*4)  ITYPE     = TYPE OF DIELECTRONIC TRANSITION
C          (I*4)  N0        = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR DIEL. RE
C          (I*4)  NCUT      = CUT-OFF PRINC. QUANTUM SHELL IN
C                             GENERAL PROGRAM
C          (I*4)  NG        = CUT-OFF PRINC. QUANTUM SHELL FROM
C                             COLLISINAL IONISATION
C          (R*8)  V0        = EFFECTIVE PRINC. QUANTUM NUMBER
C                             FOR LOWEST ACCESS
C          (R*8)  PHFRAC    = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR LOWEST SHELL
C          (R*8)  CFAC      = ADJUSTMENT FOR BETHE CORRECTIONS
C                             IN GENERAL PROGRAM
C          (R*8)  EIJ       = Z SCALED PARENT TRANSITION ENERGY (RYD)
C          (R*8)  FIJ       = OSCILLATOR STRENGTH FOR TRANSITION
C          (R*8)  EDSP      = ENERGY ADJUSTMENT IN BURGESS GENERAL
C                             FORMULA (RYD)
C          (R*8)  SCALE     = MULTIPLIER ON BURGESS GENERAL FORMULA
C
C          (R*8)  TEA       = TEMPERATURE OF CALCULATION (K)
C
C
C  OUTPUT: (R*8)  ALFO      = GENERAL PROGRAM DIELECTRONIC COEFFICIENTS
C          (R*8)  PHFRAC    = REVISED PHASE SPACE FACTOR
C          (R*8)  CORFAC    = REVISED BETHE CORRECTION SCALER
C
C
C  PROGRAM:
C
C
C  ROUTINES:
C          ROUTINE   SOURCE   DESCRIPTION
C          -------------------------------------------------------
C          GPDIEL    ADAS     ?
C          BF        ADAS     ?
C
C
C  HISTORY : BASED ON GPCALC
C            H P SUMMERS   11-5-87
C
C  AUTHOR: M O'MULLANE, UCC
C
C  DATE:    28/07/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C VERSION: 1.2                          DATE: 23-05-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               REPLACED CFAC WITH CORFAC: ERROR. REMOVED ALF0 AND ALFDAT
C
C
C VERSION: 1.3                          DATE: 16-01-2004
C MODIFIED: Martin O'Mullane
C             - Added ncut_in and eij_in as input arguments because
C               ncut and eij are altered in this subroutine.
C             - Trap for AD1.eq.0 for shells above n=10.
C             - X was defined the same way for the for both parts.
C               Re-diefine it for n>10 using v1 rather than v.
C
C VERSION: 1.4                          DATE: 17-05-2007
C MODIFIED: Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
      INTEGER     I0     , I1    , INREP   ,ITYPE, 
     &            IZ1    , J     , JCOR    , L      ,   N    ,        
     &            N0     , N1    , NCUT    , NG     , n_in 
      integer     ncut_in
C-----------------------------------------------------------------------
      REAL*8      A      , AD    , AD1     , ADU    , ADU1   ,  
     &            ATE    , B     , CORFAC  , 
     &            CPT    , DEFN  , DEFN1   , DF     ,          
     &            EDISPG , EIJ   , EPSIJ   , 
     &            F      , FAC   , FIJ     , PHFRAC , SCALEG ,  
     &            T      , V     , V0      , V1     , X      ,   
     &            XL     , Z1    , Z2      , ZZ1    ,  ZZ2
      REAL*8      TEA    , ALFN1 , ALFN    , ALF
      REAL*8      eij_in
C-----------------------------------------------------------------------
      INTEGER     JCORA(7)       , INREPA(7)        , NREP(30)
C-----------------------------------------------------------------------
      REAL*8      CORA(20,7)     , DFA(7)  , COR(20)
C-----------------------------------------------------------------------
 
 
 
       DATA JCORA/20,20,20,20,20,20,20/
       DATA DFA/2.0D0,1.0D0,1.0D0,0.5D0,0.5D0,1.0D0,1.0D0/
       DATA (CORA(J,1),J=1,20)/0.05D0,0.3D0,0.5D0,0.9D0,16*1.0D0/
       DATA (CORA(J,2),J=1,20)/0.01D0,0.02D0,0.20D0,0.40D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,3),J=1,20)/0.01D0,0.01D0,0.01D0,0.08D0,0.30D0,0.70D0
     &,14*1.0D0/
       DATA (CORA(J,4),J=1,20)/0.30D0,0.35D0,0.40D0,0.45D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,5),J=1,20)/0.30D0,0.35D0,0.40D0,0.45D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,6),J=1,20)/0.01D0,0.02D0,0.20D0,0.40D0,0.70D0,0.90D0
     &,14*1.0D0/
       DATA (CORA(J,7),J=1,20)/0.01D0,0.01D0,0.01D0,0.08D0,0.30D0,0.70D0
     &,14*1.0D0/
       DATA INREPA/21,21,21,25,25,21,21/
       DATA NREP/1,2,3,4,5,6,7,8,9,10,12,15,20,30,40,50,60,70,100,150,
     &200,250,300,400,500,600,700,800,900,1000/
 
 
C-----------------------------------------------------------------------
C  SET CUTOFF N TO BE MINIMIUM OF NCUT AND NG
C-----------------------------------------------------------------------
       
       NCUT=MIN(NCUT_in,NG)
 
       Z1=FLOAT(IZ1)
       F=FIJ
 
       EIJ=EIJ_in*((IZ1+1)**2)
 
 
       ALF   = 0.0D+0
       ALFN  = 0.0D+0
       ALFN1 = 0.0D+0
 
       ZZ1=Z1*Z1
       Z2=Z1+1.0D0
       ZZ2=Z2*Z2
       EPSIJ=EIJ/ZZ2
 
       DF=DFA(ITYPE)
 
       INREP=INREPA(ITYPE)
       JCOR=JCORA(ITYPE)
       DO J=1,JCOR
         XL=J-1
         COR(J)=CORA(J,ITYPE)*DEXP(-CORFAC/(XL**DF+0.5D0))
       END DO
       L=0
 
C-----------------------------------------------------------------------
C      CALCULATION FOR FIRST TEN QUANTUM SHELLS
C-----------------------------------------------------------------------
       I0=0
   41  I0=I0+1
       IF(N0.GE.NREP(I0))GO TO 41
       IF(I0.LT.10)I0=10
       DO 100 N=N0,NREP(I0)
         V=N
         IF(N.EQ.N0) V=V0
         DEFN=N-V
         T=0.0D0
         AD=0.0D0
         IF(N.LE.NCUT) THEN
           n_in  = n
           CALL GPDIEL(Z1,EPSIJ,F,T,COR,JCOR,N_in,DEFN,AD,ADU,L,CPT)
         ENDIF
 
         ATE=1.5789D5/TEA
         X=ATE*(ZZ1/(V*V)-ZZ2*EPSIJ)
         FAC=8.0D0*(8.7972D-17*ATE)**1.5D0*1.57456D10*DEXP(X)*ZZ1*ZZ1
         ALFN=FAC*AD
         IF(N.EQ.N0) THEN
             ALFN = PHFRAC*ALFN
         ENDIF
         ALF=ALF+ALFN
  100  CONTINUE
 
C-----------------------------------------------------------------------
C      CALCULATION FOR REMAINING QUANTUM SHELLS
C-----------------------------------------------------------------------
 
       IF (NCUT.LT.11) THEN
          ALFN=0.0D0
       ELSE
          DO 109 I1=I0+1,INREP
            N1=NREP(I1)
            V1=N1
            DEFN1=N1-V1
            AD1=0.0D0
            IF (N1.LE.NCUT) THEN
               CALL GPDIEL(Z1,EPSIJ,F,T,COR,JCOR,
     &                     N1,DEFN1,AD1,ADU1,L,CPT)
               ATE = 1.5789D5/TEA
C re-define X using v1 rather than using the old X (with v)
               X   = ATE*(ZZ1/(V1*V1)-ZZ2*EPSIJ)
               FAC = 8.0D0*(8.7972D-17*ATE)**1.5D0*
     &               1.57456D10*DEXP(X)*ZZ1*ZZ1
               IF (FAC.GT.1D-40.and.ad1.GT.0.0D0) THEN
                 ALFN1=FAC*AD1
                 B=-DLOG(ALFN/ALFN1)/DLOG(V/V1)
                 A=ALFN*V**B
                 ALF=ALF+((2.0D0*V1+1.0D0-B)*ALFN1-(2.0D0*V+1.0D0-B)
     &                     *ALFN)/(2.0D0-2.0D0*B)
               ELSE
                 ALFN1=0.0D0
                 ALF=ALF
               ENDIF
               N=N1
               V=V1
               ALFN=ALFN1
            ELSE
               ALFN=0.00D+00
            ENDIF
  109     CONTINUE
       ENDIF
 
C-----------------------------------------------------------------------
C      CALCULATION OF REMAINDER
C-----------------------------------------------------------------------
 
       ALF=ALF+0.5D0*(NREP(INREP)-1)*ALFN
 
       END
