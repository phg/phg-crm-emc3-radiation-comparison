CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8tran.for,v 1.3 2006/05/19 16:59:56 mog Exp $ Date $Date: 2006/05/19 16:59:56 $
CX
       subroutine d8tran(ndeng  , ndedge  , 
     &                   ieng   , iedge   , 
     &                   edge   , energy  , fraction , 
     &                   ein    , fout
     &                  )
 
       implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D8TRAN *******************
C
C  PURPOSE:  To determine transmission fraction at energy ein.   
C
C  CALLING PROGRAM: adas408
C
C  FUNCTION:
C
C  input : (i*4)  ndeng    = maximum number of energies in adf35 file.
C  input : (i*4)  ndedge   = maximum number of energy edges in adf35 file.
C  input : (i*4)  ieng     = actual number of energies.
C  input : (i*4)  iedge    = actual number of edges.
C  input : (r*8)  edge     = tabulated edge energies (eV).
C  input : (r*8)  energy   = tabulated energies (eV).
C  input : (r*8)  fraction = tabulated transmission fractions.
C  input : (r*8)  ein      = user supplied energy (eV).
C
C  output: (r*8)  fout     = transmission fraction at ein.
C
C  NOTES: No extrapolation is allowed and energies outside the range
C         and set to the limit values.
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          i4indfvs   ADAS      Finds nearest index for a non-monotonic
C                               array
C          xxpint     ADAS      Order 3 polynomial interpolation.
C
C
C  VERSION  : 1.1                          
C  DATE     : 15-04-96
C  MODIFIED : Martin O'Mullane
C              - First version.
C
C  VERSION  : 1.2                          
C  DATE     : 23-07-2003
C  MODIFIED : Martin O'Mullane  
C              - Interpolates adf35 filter file data
C                rather than calculating the fraction from formulae.
C                This allows a wider range of filters.
C
C  VERSION  : 1.3
C  DATE     : 16-02-2005
C  MODIFIED : Martin O'Mullane
C              - Drop warnings to screen.
C
C-----------------------------------------------------------------------
       integer   morder
C-----------------------------------------------------------------------
       parameter (morder = 3 )
C-----------------------------------------------------------------------
       integer   ndeng         , ieng            , ndedge      , iedge
       integer   i             , j               , ind
       integer   i4indfvs      , i4unit
C-----------------------------------------------------------------------
       real*8    ein           , fout
C-----------------------------------------------------------------------
       real*8    energy(ndeng) , fraction(ndeng) , edge(ndedge)
       real*8    xin(morder)   , yin(morder)
C-----------------------------------------------------------------------

C If no filter then exit

       if (ieng.EQ.0) then
          fout = 1.0D0
          return
       endif
       
C Check bounds

       if (ein.LE.energy(1)) then
          fout = 10.0D0**fraction(1)
          return
       endif
       
       if (ein.GE.energy(ieng)) then
          fout = 10.0D0**fraction(ieng)
          return
       endif


C Find index in energy array closest to ein

       ind = i4indfvs(ieng, energy, ein)

C Use 3 point interpolation. If the last enegy point is at an edge then
C set the transmission fraction to the middle point - some loss of
C precision but acceptable.
       
       if (ind.EQ.1) then
       
          fout =  10.0D0**fraction(ind)
       
       else
       
          j = 1
          do i = ind-1, ind+1
            xin(j) = energy(i)
            yin(j) = 10.0D0**fraction(i)
            j = j+1
          end do
       
          do i = 1, iedge
            if (xin(3).EQ.edge(i)) yin(3) = yin(2)
            if (xin(1).EQ.edge(i)) yin(1) = yin(2)
          end do

c catch end of grid point

          if (xin(3).EQ.0.0) then
             xin(3) = 1.1*xin(2)
             yin(3) = yin(2)
          endif
                 
          call xxpint(morder, xin, yin, ein, fout)
                 
       endif
       
C Warn if we go above 1.0 or below 0.0 for transmission fraction
C but only correct if we go below zero.
C       if (fout.GT.1.0.OR.fout.LT.0.0) write(i4unit(-1),1001)ein, fout

C Too many meaningless warnings so correct below and above

       if (fout.LT.0.0) fout = 1.0D-74
       if (fout.GT.1.0) fout = 1.0D0
              
       
       
C-----------------------------------------------------------------------
 1001 FORMAT(1X,31('*'),' D8TRAN WARNING ',30('*')//
     &       1X,'Error in interpolation at energy: ',1pe12.5, ' eV',
     &       ' fraction = ', e12.5)
C-----------------------------------------------------------------------

       end
