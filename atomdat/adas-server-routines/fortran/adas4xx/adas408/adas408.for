      PROGRAM ADAS408

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS408 **********************
C
C  ORIGINAL NAME: NCRAT0
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  PROGRAM FOR PRODUCTION OF STANDARD ISONUCLEAR MASTER FILES
C            FOR AN ELEMENT FROM SETS OF PARAMETERS
C
C  NOTE: THE FOLLOWING INFORMATION ABOUT DATA SETS IS NOW OUT OF DATE,
C        BUT IS KEPT FOR POSSIBLE FUTURE REFERENCE.
C        ALSO THE PREVIOUS, BATCH FUNCTIONALITY FOR THIS PROGRAM HAS NOT
C        BEEN IMPLEMENTED ON THE UNIX CONVERSION.
C
C  DATA:     THE SOURCE PARAMETER DATA ARE CONTAINED IN MEMBERS OF
C            A PARTITIONED DATA SET AS FOLLOWS:-
C
C                    '<UID>.ATOMPARS.DATA(<CODE>#<EL>{#<LABEL>}
C
C            WHERE
C              <UID>      = SOURCE USER IDENTIFIER
C              <CODE>     = TWO CHARACTER PREFIX IDENTIFYING THE SOURCE
C                           OF THE DATA
C              <EL>       = ONE OR TWO  CHARACTER ELEMENT SYMBOL
C              <LABEL>    = OPTIONAL ONE CHARACTER MEMBER SUBSET LABEL
C
C
C              E.G. 'JETSHP.ATOMPARS.DATA(VM#NI)'
C
C
C  PROGRAM:
C  PARAM : (I*4)  IUNT07    = OUTPUT UNIT FOR RUN SUMMARY.
C  PARAM : (I*4)  IUNT10    = INPUT UNIT FOR ATOMPARS DATA SET.
C  PARAM : (I*4)  IUNT11    = INPUT UNIT FOR FILTER DATA SET.
C  PARAM : (I*4)  IUNTBS    = BASE UNIT NUMBER FOR PASSING FILES
C
C  PARAM : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMPERATURES
C  PARAM : (I*4)  IDDIMD    = MAXIMUM NUMBER OF DENSITIES
C  PARAM : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C  PARAM : (I*4)  IGDIMD    = MAXIMUM NUMBER OF GROUPS
C  PARAM : (R*8)  EV        = CONVERSION OF K TO EV
C
C          (I*4)  IPSET     = INPUT PARAMETER SET NUMBER FOR CURRENT.
C          (I*4)  I         = LOOP INDEX.
C
C          (R*8)  AMSI      = IMPURITY ATOMIC MASS.
C          (R*8)  AMSH      = HYDROGEN ATOMIC MASS.
C          (L*4)  LEVCUT    = .TRUE.  => USE SIMPLE CUT-OFF ENERGY.
C                             .FALSE. => USE BE/SI FILTER CHARACTERISTIC
C          (I*4)  IEVCUT    = ENERGY FOR SIMPLE CUT-OFF (EV)
C          (R*8)  THBE      = BERYLLIUM FILTER THICKNESS (MICRONS)
C          (R*8)  THBECM    = BERYLLIUM FILTER THICKNESS (CM)
C          (R*8)  THSI      = SILICON FILTER THICKNESS (MM)
C          (R*8)  THSICM    = SILICON FILTER THICKNESS (CM)
C          (C*7)  FNAME     = FILTER NAME TO BE USED AS FIELD IN MASTER
C                             FILE NAME
C          (I*4)  NUMT      = NUMBER OF TEMPERATURES TO BE USED IN SCAN
C          (I*4)  NUMD      = NUMBER OF DENSITIES
C          (I*4)  IFOUT     = 1 => INPUT TEMPERATURES IN KELVIN
C                             2 => INPUT TEMPERATURES IN EV
C          (L*4)  OPEN10    = FLAGS IF INPUT ATOMPARS DATA SET OPEN.
C                             .TRUE.  => INPUT ATOMPARS DATA SET OPEN.
C                             .FALSE. => INPUT ATOMPARS DATA SET CLOSED.
C          (L*4)  OPEN11    = FLAGS IF PASSING FILE OPEN.
C                             .TRUE.  => PASSING FILE OPEN.
C                             .FALSE. => PASSING FILE CLOSED.
C          (L*4)  OPEN07    = FLAGS IF TEXT OUTPUT FILE OPEN.
C                             .TRUE.  => PASSING FILE OPEN.
C                             .FALSE. => PASSING FILE CLOSED.
C          (L*4)  OPCLAS()  = FLAGS IF CLASS PASSING FILE OPEN.
C                             .TRUE.  => CLASS PASSING FILE OPEN.
C                             .FALSE. => CLASS PASSING FILE CLOSED.
C          (C*8)  DATE      = DATE.
C          (C*3)  REP       = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*80) DSNIN     = FILE NAME OF INPUT DATA SET.
C          (C*80) DSNRUN    = RUN SUMMARY FILE DATA SET NAME.
C          (C*80) dsnpap    = PASSING FILE DATA SET NAME.
C          (C*80) DSNCLS    = FILE NAME FOR CURRENT PASS CLASS.
C          (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C          (L*4)  L2FILE    = FLAG WHETHER OUTPUT FILE REQUIRED
C          (C*80) SAVFIL    = NAME OF FILE FOR TEXT OUTPUT
C          (I*4)  NCLASS    = NUMBER OF DATA CLASSES TO PASSING FILES
C
C          (L*4)  LFCLAS()  = .FALSE. => NO FILTER IN FILE NAME
C                             .TRUE.  => INCLUDE FILTER IN FILE NAME
C
C          (I*4)  IZ0       =  NUCLEAR CHARGE
C          (I*4)  IZL       =  LOWEST INCLUDED ION
C          (I*4)  IZU       =  HIGHEST INCLUDED ION
C
C          (I*4)  IZRA()    =  RECOMBINING ION (RAD.  RECOM.)
C          (I*4)  IZDA()    =  RECOMBINING ION (DIEL. RECOM.)
C          (I*4)  IZIA()    =  IONISING ION    (COLL. IONIS.)
C          (I*4)  IZTA()    =  RADIATING ION   (TOTAL LINE POWER)
C          (I*4)  IZSA()    =  RADIATING ION   (SPECIFIC LINE POWER)
C
C          (C*5)  CRRCA()   =  RADIATIVE RECOM. CODE
C          (I*4)  NRRCA()   =    - NOT USED -
C          (I*4)  ISRRCA()  =    - NOT USED -
C
C          (I*4)  NZA()     = LOWEST ACCESSIBLE SHELL FOR RAD. RECOM.
C          (I*4)  KSIA()    = NUMBER OF ELECTRONS IN SHELL
C
C          (I*4)  N0RA()    = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR RAD. RECOM.
C          (I*4)  V0RA()    = EFFECTIVE PRINCIPAL QUANTUM NUMBER
C                             FOR SHELL
C          (R*8)  PHFCRA()  = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR SHELL
C          (R*8)  EDSPRA()  = ENERGY ADJUSTMENT IN LOWEST SHELL
C                             RATE COEFFICIENT
C          (R*8)  SCLERA()  = MULTIPLIER FOR LOWEST SHELL
C                             RATE COEFFICIENT
C
C          (C*5)  CDRCA()   =  DIELECTRONIC RECOM. CODE
C          (I*4)  NDRCA()   =  NUMBER OF TRANSITIONS FOLLOWING
C          (I*4)  ISDRCA()  =    - NOT USED -
C
C          (R*8)  DEDA(,)   = TRANSITION ENERGY (EV)
C          (R*8)  FDA(,)    = OSCILLATOR STRENGTH
C          (R*8)  GDA(,)    = GAUNT FACTOR
C          (I*4)  NNDA(,)   = DELTA N FOR TRANSITION
C          (I*4)  MSDA(,)   = MERTZ SWITCH (0=OFF, 1=ON)
C
C          (I*4)  ITYPDA(,) = TYPE OF DIELECTRONIC TRANSITION
C          (I*4)  N0DA(,)   = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR DIEL. RE
C          (I*4)  NCUTA(,)  = CUT-OFF PRINC. QUANTUM SHELL IN
C                             GENERAL PROGRAM
C          (I*4)  V0DA(,)   = EFFECTIVE PRINC. QUANTUM NUMBER
C                             FOR LOWEST ACCESS
C          (R*8)  PHFCDA(,) = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR LOWEST SHELL
C          (R*8)  CRFCDA(,) = ADJUSTMENT FOR BETHE CORRECTIONS
C                             IN GENERAL PROGRAM
C          (R*8)  EPSIJA(,) = Z-SCALED PARENT TRANSITION ENERGY (RYD)
C          (R*8)  FIJA(,)   = OSCILLATOR STRENGTH FOR TRANSITION
C          (R*8)  EDSPDA(,) = ENERGY ADJUSTMENT IN BURGESS GENERAL
C                             FORMULA (RYD)
C          (R*8)  SCLEDA(,) = MULTIPLIER ON BURGESS GENERAL FORMULA
C
C          (C*5)  CCIOA()   =  COLLISIONAL IONIS. CODE
C          (I*4)  NCIOSA()  =  NUMBER OF SHELL  VALUES FOLLOWING
C          (I*4)  NCIORA()  =  NUMBER OF RESON. VALUES FOLLOWING
C          (I*4)  ISCIOA()  =    - NOT USED -
C
C          (R*8)  PIOA(,)   = SHELL IONISATION POTENTIAL (EV)
C          (R*8)  AIOA(,)   = LOTZ PARAMETER
C          (R*8)  BIOA(,)   = LOTZ PARAMETER
C          (R*8)  CIOA(,)   = LOTZ PARAMETER
C          (I*4)  NQIOA(,)  = EQUIVALENT ELECTRONS IN SHELL
C
C          (R*8)  ZETAA(,)  = NUMBER OF EQUIVALENT ELECTRONS FOR SHELL
C          (R*8)  EIONA(,)  = IONISATION ENERGY FOR SHELL (RYD)
C          (R*8)  CIA(,)    = MULTIPLIER FOR BURGESS-CHIDICHIMO RATE
C                             FOR SHELL
C          (R*8)  WGHTA(,)  = WEIGHTING FACTOR FOR EXCITATION TO
C                             RESONANCE
C          (R*8)  ENERA(,)  = EXCITATION ENERGY FOR TRANSITION
C                             TO RESONANCE (RYD)
C          (R*8)  CRA(,)    = MULTIPLIER ON EXCITATION RATE EXPRESSSION
C
C          (C*5)  CPLTA()   =  TOTAL LINE POWER CODE
C          (I*4)  NPLTA()   =  NUMBER OF TRANSITIONS FOLLOWING
C          (I*4)  ISPLTA()  =    - NOT USED -
C
C          (R*8)  DEPTA(,)  = TRANSITION ENERGY (EV)
C          (R*8)  FPTA(,)   = OSCILLATOR STRENGTH
C          (R*8)  GPTA(,)   = GAUNT FACTOR
C          (I*4)  NNPTA(,)  = DELTA N FOR TRANSITION
C          (R*8)  SPYLTA(,) = MULTIPLIER OF VAN REGEMORTER P
C                             FACTOR IN TOTAL POWER
C
C          (C*5)  CPLSA()   = SPECIFIC LINE POWER CODE
C          (I*4)  NPLSA()   =   - NOT USED -
C          (I*4)  ISPLSA()  =   - NOT USED -
C          (C*8)  INFO()    = WAVELENGTH OF SPECIFIC LINE FOR
C                             NAMING PURPOSES
C
C          (R*8)  DEPSA(,)  = TRANSITION ENERGY (EV)
C          (R*8)  FPSA(,)   = OSCILLATOR STRENGTH
C          (R*8)  GPSA(,)   = GAUNT FACTOR
C          (I*4)  NNPSA(,)  = DELTA N FOR TRANSITION
C          (R*8)  SPYLSA(,) = MULTIPLIER OF VAN REGEMORTER P FACTOR
C                             IN SPECIFIC LINE POWER
C
C          (L*4)  LVALID  = .TRUE. DATA SET READ AND APPEARS VALID
C                         = .FALSE. ERROR DETECTED IN READING DATA SET
C
C          (I*4)  PIPEIN  = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END
C                           TO THE ROUTINE FROM ANY POINT RATHER THAN
C                           A RETURN TO THE PREVIOUS SCREEN. IF 1 THEN
C                           PROGRAM ENDS, IF 0 THEN IT CONTINUES.
C
C  	   (R*8)  TEL()       = TEMPERATURE SET OF TABLES - LOG MESH
C          (R*8)  FNEL()      = DENSITY SET OF TABLES - LOG MESH
C          (C*13) NAME        = ELEMENT NAME
C
C          (R*8)  RAL(,,)     = RADIATIVE AND DIELECTRONIC RECOMB.
C          (R*8)  CAL(,,)     = CHARGE EXCHANGE RECOMBINATION
C
C          (R*8)  SAL0(,)     = IONISATION (NEUTRAL)
C          (R*8)  SAL(,,)     = IONISATION (NON-NEUTRAL)
C
C          (R*8)  PLTL0(,)    = TOTAL LINE RADIATED POWER (NEUTRAL)
C          (R*8)  PLTL(,,)    = TOTAL LINE RADIATED POWER (NON-NEUTRAL)
C
C          (R*8)  PRBL(,,)    = RAD. + DIEL RECOM. + BREMS. POWER
C          (R*8)  PRCL(,,)    = CX. RECOM. POWER
C
C          (R*8)  PLSL0(,)    = SPECIFIC LINE POWER   (NEUTRAL)
C          (R*8)  PLSL(,,)    = SPECIFIC LINE POWER   (NON-NEUTRAL)
C
C          (R*8)  PRBLNFL(,,) = RECOM+BREMM POWER (NO FILTER)
C          (R*8)  PRCLNFL(,,) = CX POWER (NO FILTER)
C          (R*8)  PLTLNFL(,,) = LINE POWER (NO FILTER)
C          (R*8)  PLTL0NFL(,,)= NEUTRAL LINE POWER (NO FILTER)
C
C          (C*8)  INFRAL()    = RECOMBINATION INFO STRING
C          (C*8)  INFSAL()    = IONISATION INFO STRING
C          (C*8)  INFCAL()    = CX INFO STRING
C          (C*8)  INFPRB()    = RECOM+BREMM POWER INFO STRING
C          (C*8)  INFPRC()    = CX POWER INFO STRING
C          (C*8)  INFPLT()    = TOTAL LINE POWER INFO STRING
C          (C*8)  INFPLS()    = SPECIFIC LINE POWER INFO STRING
C          (C*8)  INFPRBN()   = RECOMM+BREM POWER (NO FILTER) INFO
C          (C*8)  INFPRCN()   = CX POWER (NO FILTER) INFO STRING
C          (C*8)  INFPLTN()   = TOTAL LINE POWER (NO FILTER) INFO
C
C          (C*24) METRAL      = RECOMBINATION METHOD STRING
C          (C*24) METSAL      = IONISATION METHOD STRING
C          (C*24) METCAL      = CX METHOD STRING
C          (C*24) METPRB      = RECOM+BREMM POWER METHOD STRING
C          (C*24) METPRC      = CX POWER METHOD STRING
C          (C*24) METPLT      = TOTAL LINE POWER METHOD STRING
C          (C*24) METPLS      = SPECIFIC LINE POWER METHOD STRING
C          (C*24) METPRBN     = RECOMM+BREM POWER (NO FILTER) METHOD
C          (C*24) METPRCN     = CX POWER (NO FILTER) METHOD STRING
C          (C*24) METPLTN     = TOTAL LINE POWER (NO FILTER) METHOD
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          D8DATA     ADAS      READS INPUT DATA SET IN ADF03 FORMAT.
C          D8ISPF     IDL-ADAS  GATHERS PROCESSING DATA FROM IDL
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/05/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 04-04-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CREATED SKELETON
C
C VERSION: 1.2				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS TO D8EVAL TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.3				DATE: 09-08-96
C MODIFIED: WILLIAM OSBORN
C	    CHANGED REMAINING IF(OPCLAS(I).EQ.TRUE) STATMENT TO
C           IF(OPCLAS(I)).
C
C UPDATE: 25/02/97 M O'Mullane 
C         Changed thickness of silicon to mm and converted before
C         entering D8EVAL.
C
C VERSION: 1.5				DATE: 21-07-2003
C MODIFIED: Martin O'Mullane
C            - Major restructuring.
C            - Read adf03 files with xxdata_03.
C	     - Read transmission data from adf35 filter.
C            - Remove Be and Si thicknesses.
C            - Remove all unecessary variables.
C
C VERSION : 1.6
C DATE    : 05-03-2004
C MODIFIED: Martin O'Mullane
C            - Change dimensions : 55 temperatures, 50 densities 
C              and 80 ion stages.
C            - The type of output to the acd (RR/DR/both) and 
C              prb (RR/DR/BR/all) files is passed from IDL.
C
C VERSION : 1.7
C DATE    : 21-02-2005
C MODIFIED: Martin O'Mullane
C            - Write a string to IDL at end in order to force it
C              to wait until output files are written. 
C
C VERSION : 1.8
C DATE    : 24-06-2005
C MODIFIED: Martin O'Mullane
C            - Add arrays for filtered rr/dr/br power.
C
C VERSION : 1.9
C DATE    : 04-10-2006
C MODIFIED: Hugh Summers
C            - Added handling of zcd, ycd and ecd classes.
C            - Adjusted details to preferred superstage nomenclatures. 
C
C VERSION : 1.10
C DATE    : 16-01-2007
C MODIFIED: Hugh Summers
C            - Added dimension parameters iodimd and imdimd to call to
C              d8out1.for to enable use of revised d8wzcd.for. 
C
C VERSION : 1.11
C DATE    : 07-02-2010
C MODIFIED: Martin O'Mullane
C            - Acquire Te and dens from IDL as vectors, not min/max pairs.
C
C-----------------------------------------------------------------------
      integer    pipein       , pipeou
      integer    iunt07       , iuntbs
      integer    itdimd       , iddimd    , izdimd
      integer    iodimd       , imdimd
C-----------------------------------------------------------------------
      parameter( pipein = 5   , pipeou = 6)
      parameter( iunt07 = 07  , iuntbs = 50  ) 
      parameter( itdimd = 55  , iddimd = 50  , izdimd = 80  )
      parameter( iodimd = 60  , imdimd = 4   )
C-----------------------------------------------------------------------
      real*8     ev
C-----------------------------------------------------------------------
      parameter( ev = 11605.4D0 )
C-----------------------------------------------------------------------
      INTEGER    IZ0     , IZL     , IZU     ,
     &           NUMT    , NUMD    , IFOUT
      INTEGER    NCLASS
      INTEGER    ilogic  , I       , l1      , l2      , J   , K
      INTEGER    itypacd , itypprb
C-----------------------------------------------------------------------
      REAL*8     amimp   , amh
C-----------------------------------------------------------------------
      LOGICAL    OPEN07  , L2FILE  , ltick
C-----------------------------------------------------------------------
      character  dsn03*80   , dsn35*80  , dsncls*80    , dsnpap*80     , 
     &           passdir*80 , title*80  , date*8       , user*30       , 
     &           reply*8    , cadas*80  , file_ext*40  , filter_ext*40
C-----------------------------------------------------------------------
      INTEGER    list(13)
C-----------------------------------------------------------------------
      LOGICAL    OPCLAS(13) , LFCLAS(13)
C-----------------------------------------------------------------------
      CHARACTER  CCLASS(13)*3 , dsnout(13)*80
C-----------------------------------------------------------------------
      DATA   NCLASS / 13 /
      DATA   CCLASS / 'acd' , 'scd' , 'ccd' , 'prb' , 'plt' ,
     &                'pls' , 'prc' , 'prb' , 'plt' , 'prc' ,
     &                'zcd' , 'ycd' , 'ecd' /
      DATA   LFCLAS/.FALSE. , .FALSE. , .FALSE. , .TRUE.  , .TRUE.  ,
     &              .FALSE. , .TRUE.  , .FALSE. , .FALSE. , .FALSE. ,
     &              .FALSE. , .FALSE. , .FALSE. /
C-----------------------------------------------------------------------
C     D8EVAL OUTPUT VARIABLES
C-----------------------------------------------------------------------

      REAL*8    CAL      (ITDIMD,IDDIMD,IZDIMD),
     &          PRBL     (ITDIMD,IDDIMD,IZDIMD),
     &          SAL      (ITDIMD,IDDIMD,IZDIMD),
     &          RAL      (ITDIMD,IDDIMD,IZDIMD),
     &          PRCL     (ITDIMD,IDDIMD,IZDIMD),
     &          PLTL     (ITDIMD,IDDIMD,IZDIMD),
     &          PLSL     (ITDIMD,IDDIMD,IZDIMD),
     &          SAL0     (ITDIMD,IDDIMD),
     &          PLTL0    (ITDIMD,IDDIMD),
     &          PLSL0    (ITDIMD,IDDIMD)
      REAL*8    PRBLNFL  (ITDIMD,IDDIMD,IZDIMD),
     &          PRCLNFL  (ITDIMD,IDDIMD,IZDIMD),
     &          PLTLNFL  (ITDIMD,IDDIMD,IZDIMD),
     &          PLTL0NFL (ITDIMD,IDDIMD)
      REAL*8    ralout   (ITDIMD,IDDIMD,IZDIMD),
     &          ralrr    (ITDIMD,IDDIMD,IZDIMD),
     &          raldr    (ITDIMD,IDDIMD,IZDIMD),
     &          prbout   (ITDIMD,IDDIMD,IZDIMD),
     &          prboutnfl(ITDIMD,IDDIMD,IZDIMD),
     &          prbrr    (ITDIMD,IDDIMD,IZDIMD),
     &          prbdr    (ITDIMD,IDDIMD,IZDIMD),
     &          prbbr    (ITDIMD,IDDIMD,IZDIMD),
     &          prbrrnfl (ITDIMD,IDDIMD,IZDIMD),
     &          prbdrnfl (ITDIMD,IDDIMD,IZDIMD),
     &          prbbrnfl (ITDIMD,IDDIMD,IZDIMD)

      CHARACTER*24 METRAL,METSAL,METCAL,METPRB,METPRC,METPLT,METPLS
      CHARACTER*24 METPRBN,METPRCN,METPLTN

      CHARACTER*8 INFRAL(IZDIMD),INFSAL(IZDIMD),INFCAL(IZDIMD),
     &            INFPRB(IZDIMD),INFPRC(IZDIMD),INFPLT(IZDIMD),
     &            INFPLS(IZDIMD),INFPRBN(IZDIMD),INFPRCN(IZDIMD),
     &            INFPLTN(IZDIMD)
 
      REAL*8    TEL(ITDIMD),FNEL(ITDIMD)
      CHARACTER NAME*13
      
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C Set machine dependant adas configuration values.
C-----------------------------------------------------------------------

      call xx0000
      call xxname(user)
      call xxdate(date)
      call xxadas(cadas)

      ltick = .TRUE.
      
C-----------------------------------------------------------------------
C Initialize values.
C-----------------------------------------------------------------------

      open07 = .FALSE.

      do i= 1,nclass
        opclas(i) = .FALSE.
      end do

      reply  = 'NO'
      dsnpap = ' '
      cadas  = ' '

C-----------------------------------------------------------------------
C  Input screen
C
C  If files are active then close the units before proceeding.
C-----------------------------------------------------------------------

 100  continue


      if (open07) then
         close( iunt07 )
         open07 = .false.
      endif


      read(pipein,'(A)')reply
      if (reply(1:6).EQ. 'CANCEL') then
         goto 999
      endif

      read(pipein,'(a)') dsn03
      read(pipein,'(a)') dsn35
      


C-----------------------------------------------------------------------
C  Processing screen.
C-----------------------------------------------------------------------

 200  continue

      
      read(pipein,'(A)')reply
      
      if (reply(1:4).EQ.'MENU') goto 999
      if (reply(1:6).EQ.'CANCEL') goto 100
      
      if (reply(1:8).EQ.'CONTINUE') then

         read(pipein,'(A)') title

         read(pipein,*) ifout
         read(pipein,*) numt
         do i = 1, numt
            read(pipein, *)tel(i)
            if (ifout.eq.1) tel(i) = tel(i) / ev
         end do
         read(pipein,*) numd
         do i = 1, numd
            read(pipein, *)fnel(i)
         end do

         read(pipein,*) amimp
         read(pipein,*) amh

      endif
         
         
C-----------------------------------------------------------------------
C  Do the calculations.
C-----------------------------------------------------------------------
     

      call d8eval ( dsn03    , dsn35    ,
     &              numt     , numd     , amh      , 
     &              tel      , fnel     , name     , 
     &              iz0      , izl      , izu      ,
     &              ral      , sal0     , sal      , cal     , 
     &              prbl     , prcl     , pltl0    , pltl    , 
     &              plsl0    , plsl     , 
     &              prblnfl  , prclnfl  , pltl0nfl , pltlnfl ,
     &              ralrr    , raldr    ,
     &              prbrr    , prbdr    , prbbr    ,
     &              prbrrnfl , prbdrnfl , prbbrnfl ,
     &              infral   , infsal   , infcal   ,
     &              infprb   , infprc   , infplt   , infpls  ,
     &              infprbn  , infprcn  , infpltn  ,
     &              metral   , metsal   , metcal   ,
     &              metprb   , metprc   , metplt   , metpls  ,
     &              metprbn  , metprcn  , metpltn  ,
     &              ltick           
     &            )


C-----------------------------------------------------------------------
C Output screen
C-----------------------------------------------------------------------

  300  continue
  
      read(pipein,'(A)')reply
      
      if (reply(1:4).EQ.'MENU') goto 999
      if (reply(1:6).EQ.'CANCEL') goto 200
      
      if (reply(1:8).EQ.'CONTINUE') then
         
         read(pipein,'(a)')file_ext
         read(pipein,'(a)')filter_ext
         read(pipein,'(a)')passdir
         
	 read(pipein,*)itypacd
	 read(pipein,*)itypprb
         
         read(pipein,*)ilogic
         if (ilogic.eq.1) then
            read(pipein,'(a)')dsnpap
            l2file = .TRUE.
            open(iunt07, file = dsnpap)
         else
            l2file = .FALSE.
         endif
         
         call xxslen(passdir, L1, L2)
         do i = 1, nclass
            
            read(pipein,*)list(i)

            if (list(i).EQ.1) then
               
               if (lfclas(i)) then
                  dsncls = cclass(i)//filter_ext
               else
                  dsncls = cclass(i)//file_ext
               endif
               if (L2.NE.0) dsncls = passdir(l1:l2)//dsncls
            
               open( unit=iuntbs+i, file=dsncls)
               opclas(i) = .TRUE.
               dsnout(i) = dsncls
            endif
            
         end do
	 
C Fill out arrays depending on ACD and PRB output options

	 if (itypacd.EQ.0) then
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            ralout(k,j,i) = ral(k,j,i)
		  end do
		end do
            end do 
	 elseif (itypacd.EQ.1) then
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            ralout(k,j,i) = ralrr(k,j,i)
		  end do
		end do
            end do 
	 else
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            ralout(k,j,i) = raldr(k,j,i)
		  end do
		end do
            end do 
	 endif         

           
	 if (itypprb.EQ.0) then
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            prboutnfl(k,j,i) = prblnfl(k,j,i)
	            prbout(k,j,i)    = prbl(k,j,i)
		  end do
		end do
            end do 
	 elseif (itypprb.EQ.1) then
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            prboutnfl(k,j,i) = prbrrnfl(k,j,i)
	            prbout(k,j,i)    = prbrr(k,j,i)
		  end do
		end do
            end do 
	 elseif (itypprb.EQ.2) then
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            prboutnfl(k,j,i) = prbdrnfl(k,j,i)
	            prbout(k,j,i)    = prbdr(k,j,i)
		  end do
		end do
            end do 
	 else
	    do i = 1, izdimd
	       do j = 1, iddimd
	          do k = 1, itdimd
	            prboutnfl(k,j,i) = prbbrnfl(k,j,i)
	            prbout(k,j,i)    = prbbr(k,j,i)
		  end do
		end do
            end do 
	 endif         

         call d8out1( iuntbs   , opclas  , date    , 
     &                itdimd   , iddimd  , 
     &                izdimd   , iodimd  , imdimd  ,
     &                numt     , numd    ,
     &                iz0      , izl     , izu     ,
     &                tel      , fnel    , name    ,
     &                ralout   , sal0    , sal     , cal    ,
     &                prbout   , prcl    ,
     &                pltl0    , pltl    , plsl0   , plsl   ,
     &                prboutnfl, prclnfl ,
     &                pltl0nfl , pltlnfl ,
     &                infral   , infsal  , infcal  ,
     &                infprb   , infprc  , infplt  , infpls ,
     &                infprbn  , infprcn , infpltn ,
     &                metral   , metsal  , metcal  ,
     &                metprb   , metprc  , metplt  , metpls ,
     &                metprbn  , metprcn , metpltn , 
     &                user     , dsn03   , dsn35
     &              )
      
         if (l2file) then 
            call d8out0( iunt07  , date      , dsn03  , dsn35  ,
     &                   opclas  , dsnout    ,
     &                   title   , iz0       , izl    , izu    ,
     &                   amimp   , amh       ,
     &                   numt    , numd      ,
     &                   10.0**tel(1)  , 10.0**tel(numt) , 
     &                   10.0**fnel(1) ,10.0** fnel(numd) ,
     &                   cadas
     &                  )
         endif
         
         do i = 1, nclass
            
            if (opclas(i)) close(iuntbs+i)
            opclas(i) = .FALSE.

         enddo         

      endif
      
      
      

C Close the I/O files

      if (open07)  close(iunt07)


C Go back to output screen when finished

      goto 300


C-----------------------------------------------------------------------
C  Exit ADAS408 but force IDL to wait
C-----------------------------------------------------------------------

 999  continue
      reply = 'EXIT NOW'
      write(pipeou, '(A)')reply

C-----------------------------------------------------------------------

      END
