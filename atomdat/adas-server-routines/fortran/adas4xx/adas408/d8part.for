CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8part.for,v 1.2 2006/05/19 17:00:29 mog Exp $ Date $Date: 2006/05/19 17:00:29 $
CX
       subroutine d8part(x, y, num, te, flimit, result)

       implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D8TRAN *******************
C
C  PURPOSE: To integrate between x(1) and x(num) the integrand
C                          f(x)exp(-x) * exp(+a)
C           where f(x) is the filter function.
C
C
C  CALLING PROGRAM: adas408
C
C  FUNCTION:
C
C  input : (r*8)  x        = tabulated edge energies (eV).
C  input : (r*8)  y        = tabulated energies (eV).
C  input : (i*4)  num      = actual number of edges.
C  input : (r*8)  te       = user supplied temperature (eV).
C  input : (r*8)  flimit   = lower limit of integration (eV/Te)
C
C  output: (r*8)  result   = value of integral.
C
C  NOTES: 
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C  VERSION  : 1.1                          
C  DATE     : 05-08-2003
C  MODIFIED : Martin O'Mullane
C              - First version in SCCS.
C
C  VERSION  : 1.2
C  DATE     : 16-02-2005
C  MODIFIED : Martin O'Mullane
C              - Do not re-use x1() and x2() in parts integration.
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       integer  num   , i , ione
C-----------------------------------------------------------------------
       real*8   te    , flimit  , result  , 
     &          xi    , xip1    , xdiff   , ei   , eip1   , anyl 
C-----------------------------------------------------------------------
       real*8   x(*)  , y(*) 
C-----------------------------------------------------------------------

       result = 0.0D0
       
       do i = 1, num-2
         xi     = x(i) / te
         xip1   = x(i+1) / te
         xdiff  = xip1 - xi
         ei     = exp(flimit-xi)
         eip1   = exp(flimit-xip1)
         
C         if (y(i).gt.0.0) y(i)=1.0
C         if (y(i+1).gt.0.0) y(i+1)=1.0
         
         if ((flimit-xi).le.0.0.and.(flimit-xip1).le.0.0) then
           anyl   = (y(i+1)-y(i))*((1.0D0+xi)*ei-(1.0D0+xip1)*eip1) +
     &              (xip1*y(i)-xi*y(i+1))*(ei-eip1)
           anyl   = anyl / xdiff
         else
           anyl=0.0
         endif
         result = result + anyl
       end do

       end


