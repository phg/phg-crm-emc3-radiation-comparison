CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8velh.for,v 1.1 2004/07/06 13:25:53 whitefor Exp $ Date $Date: 2004/07/06 13:25:53 $
CX
      function d8velh(th,hm,v1,v2)
 
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
      implicit real*8(a-h,o-z)
 
C Velocity constant in units of 1.0D4 m/s := sqrt(2e/1.66043D-27)/1.0D4
 
      data vcon / 1.3892 /
 
C Determine velocity
 
      d8velh = vcon * sqrt(th / hm)
 
C Check and limit
 
      d8velh = max(d8velh,v1)
      d8velh = min(d8velh,v2)
 
      return
 
      end
