CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8intg.for,v 1.3 2006/05/19 17:00:29 mog Exp $ Date $Date: 2006/05/19 17:00:29 $
CX
       subroutine d8intg( ndedge  ,  ndeng   ,
     &                    iedge   ,  ieng    , 
     &                    edge    ,  energy  ,  fraction ,
     &                    te      ,  flimit  ,  result   
     &                   )

       implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D8TRAN *******************
C
C  PURPOSE: To integrate between a and b with an interval of step
C           the integrand
C                          f(x)exp(-x) * exp(+a)
C           where f(x) is the filter function.
C
C
C  CALLING PROGRAM: adas408
C
C  FUNCTION:
C
C  input : (i*4)  ndeng    = maximum number of energies in adf35 file.
C  input : (i*4)  ndedge   = maximum number of energy edges in adf35 file.
C  input : (i*4)  ieng     = actual number of energies.
C  input : (i*4)  iedge    = actual number of edges.
C  input : (r*8)  edge     = tabulated edge energies (eV).
C  input : (r*8)  energy   = tabulated energies (eV).
C  input : (r*8)  fraction = tabulated transmission fractions.
C  input : (r*8)  te       = user supplied temperature (eV).
C  input : (r*8)  flimit   = lower limit of integration (eV/Te)
C
C  output: (r*8)  result   = value of integral.
C
C  NOTES: 
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          i4indfvs   ADAS      Finds nearest index for a non-monotonic
C                               array
C          xxmerg     ADAS      Merge two grids.
C          d8tran     ADAS      Returns transmission of a filter.
C          d8part     ADAS      Trapezoidal integration routine.
C
C  Author   : Martin O'Mullane UCC 26/8/92
C
C  VERSION  : 1.1                          
C  DATE     : 15-04-96
C  MODIFIED : Martin O'Mullane
C              - First version in SCCS.
C
C  VERSION  : 1.2                          
C  DATE     : 05-0-2003
C  MODIFIED : Martin O'Mullane  
C              - Uses adf35 filter file data.
C                
C  VERSION  : 1.3
C  DATE     : 16-02-2005
C  MODIFIED : Martin O'Mullane
C              - Do not re-use x1() and x2() in parts integration.
C
C
C-----------------------------------------------------------------------
       integer   nstep
       real*8    floor
C-----------------------------------------------------------------------
       parameter (nstep = 20000)
       parameter (floor = 1.0D-06)
C-----------------------------------------------------------------------
       integer   ndeng          , ieng            , ndedge      , iedge
       integer   i              , j               , k           , 
     &           ind            , num             , n1          , n2
       integer   ifirst, ilast
       integer   i4indfvs
C-----------------------------------------------------------------------
       real*8    flimit           , te              , result           , 
     &           part             , fout            , yval
C-----------------------------------------------------------------------
       real*8    energy(ndeng)    , fraction(ndeng) , edge(ndedge)
       real*8    x1(nstep)        , x2(nstep)       , grid(nstep+1000) , 
     &           x(nstep+1000)    , y(nstep+1000)
       real*8    xint(nstep+1000) , yint(nstep+1000)
C-----------------------------------------------------------------------
C If no filter then exit

      if (ieng.EQ.0) then
         result = 0.0D0
         return
      endif
      
       

C Set up computation grids
C
C  1. Make an exp(-x) grid where (exp(-x(i)/te).LT.1.0d-10).
C     Vary the step size keeping the number of steps the same. 
C  2. Make filter grid for values above the lower limit
C  3. Merge the filter and exp(-x) grids
C  4. Restrict the the integral to values above parameter 'floor'

      
      do i = 1, nstep
        x1(i) = flimit + (i-1)*(23.0D0*te-flimit)/(nstep-1)
        x2(i) = 0.0D0
      end do
      n1 = nstep
      
      
      ind = i4indfvs(ieng, energy, flimit)
      
      do i = ind, ieng
         x2(i-ind+1) = energy(i)
      end do
      n2 = ieng-ind+1
      
      call xxmerg( x1   , n1   ,
     &             x2   , n2   , 
     &             grid , num
     &           )
              
              
      j = 1
      do i=1,num
        
        call d8tran( ndeng   , ndedge  , 
     &               ieng    , iedge   , 
     &               edge    , energy  , fraction , 
     &               grid(i) , fout
     &             )
        yval = fout * exp(-grid(i)/te)
        if (fout.GE.floor.AND.yval.GE.1.0D-36) then
           x(j) = grid(i)
           y(j) = fout
           j = j + 1
        endif
        
        
      end do
      
      num = j-1
      


C Now integrate - sum the parts up to each edge
      

      result = 0.0D0
      ifirst = 1
      ilast  = 0
      
      do i = 1, iedge
        
        do j = 1, nstep+1000
          xint(j) = 0.0D0
          yint(j) = 0.0D0
        end do
        
        if (num.LE.2) then
           ind = 1
        else
           ind = i4indfvs(num, x, edge(i))
        endif
        
        if (ind.GT.1.AND.ind.LT.num) then
           
           ilast = ind
           k     = 1
           do j = ifirst, ilast
             xint(k) = x(j)
             yint(k) = y(j)
             k = k + 1
           end do
           ifirst = ilast + 1
           
           call d8part(xint, yint, k-1, te, flimit, part)
                      
           result = result + part
              
        endif
      
      end do
      
C grid beyond last point above      
      
      if (ifirst.LT.num) then

        do j = 1, nstep+1000
          xint(j) = 0.0D0
          yint(j) = 0.0D0
        end do
        
        k = 1
        do j = ifirst, num
           xint(k) = x(j)
           yint(k) = y(j)
           k = k + 1
        end do
          
        call d8part(xint, yint, k-1, te, flimit, part)
        result = result + part
                       
      endif

C If no edges evaluate the integral
            
       if (result.EQ.0.0D0) call d8part(x, y, num, te, flimit, result)
       
 10   format(a4,2x,1p,7e12.5)

      end
