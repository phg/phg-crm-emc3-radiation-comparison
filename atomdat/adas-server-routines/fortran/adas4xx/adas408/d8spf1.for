C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8spf1.for,v 1.2 2004/07/06 13:25:40 whitefor Exp $ Date $Date: 2004/07/06 13:25:40 $
C
      SUBROUTINE D8SPF1( FNAME , IZ0   , LPEND ,
     &                   DSNPAS, SAVFIL, L2FILE, CADAS)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS408
C
C  SUBROUTINE:
C
C  INPUT :   (C*7)   FNAME    = FILE FILTER NAME
C  INPUT :   (I*4)   IZ0      = INPUT NUCLEAR CHARGE
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (C*80)  DSNPAS   = FILENAME FOR PASSING FILE
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C  OUTPUT:   (C*80)  CADAS    = HEADER FOR TEXT OUTPUT
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     IDL-ADAS  CALLS FLUSH TO CLEAR PIPES.
C
C AUTHOR:  William Osborn (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    15TH APRIL 1996
C
C VERSION: 1.1			DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C 
C VERSION: 1.2				DATE: 24-03-97
C MODIFIED: MARTIN O'MULLANE		
C		CHANGED FNAME FROM CHAR*6 TO CHAR*7
C
C-----------------------------------------------------------------------
      CHARACTER    FNAME*7   , SAVFIL*80   , CADAS*80   , DSNPAS*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , L2FILE 
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      , I4UNIT	     , IZ0	   ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)

C-----------------------------------------------------------------------
C  WRITE VALUES TO IDL
C-----------------------------------------------------------------------
      WRITE(PIPEOU,'(A7)')FNAME
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)IZ0
      CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN

         READ(PIPEIN,*) ILOGIC

         IF (ILOGIC .EQ. ONE) THEN 
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
            CALL XXADAS(CADAS)
         ELSE
            L2FILE = .FALSE.
         ENDIF

	 READ(PIPEIN,'(A)') DSNPAS
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
