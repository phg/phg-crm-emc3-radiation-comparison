CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8out1.for,v 1.5 2009/03/04 13:23:45 afoster Exp $ Date $Date: 2009/03/04 13:23:45 $
CX
      SUBROUTINE D8OUT1( IUNTBS   , OPCLAS   , DATE    ,
     &                   ITDIMD   , IDDIMD   , 
     &                   IZDIMD   , IODIMD   , IMDIMD  ,
     &                   NUMT     , NUMD     ,
     &                   IZ0      , IZL      , IZU     ,
     &                   TEL      , FNEL     , NAME    ,
     &                   RAL      , SAL0     , SAL     , CAL     ,
     &                   PRBL     , PRCL     ,
     &                   PLTL0    , PLTL     , PLSL0   , PLSL    ,
     &                   PRBLNFL  , PRCLNFL  ,
     &                   PLTL0NFL , PLTLNFL  ,
     &                   INFRAL   , INFSAL   , INFCAL  ,
     &                   INFPRB   , INFPRC   , INFPLT  , INFPLS  ,
     &                   INFPRBN  , INFPRCN  , INFPLTN ,
     &                   METRAL   , METSAL   , METCAL  ,
     &                   METPRB   , METPRC   , METPLT  , METPLS  ,
     &                   METPRBN  , METPRCN  , METPLTN ,  
     &                   user     , dsn03    , dsn35
     &                 )

      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8OUT0 *********************
C
C  PURPOSE: WRITES DATA OUTPUT TO FILES FOR ADAS408.
C
C  CALLING PROGRAM: ADAS408
C
C  INPUT : (I*4)  IUNTBS    = BASE UNIT FOR PASSING FILES
C  INPUT : (C*8)  DATE      = DATE STRING.
C  INPUT : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT : (I*4)  IDDIMD    = MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C  INPUT : (I*4)  NUMT      = NUMBER OF TEMPERATURES TO BE USED IN SCAN
C  INPUT : (I*4)  NUMD      = NUMBER OF DENSITIES
C  INPUT : (I*4)  IZ0       =  NUCLEAR CHARGE
C  INPUT : (I*4)  IZL       =  LOWEST INCLUDED ION
C  INPUT : (I*4)  IZU       =  HIGHEST INCLUDED ION
C  INPUT : (R*8)  TEL()       = TEMPERATURE SET OF TABLES - LOG MESH
C  INPUT : (R*8)  FNEL()      = DENSITY SET OF TABLES - LOG MESH
C  INPUT : (C*13) NAME        = ELEMENT NAME
C
C  INPUT : (R*8)  RAL(,,)     = RADIATIVE AND DIELECTRONIC RECOMB.
C  INPUT : (R*8)  CAL(,,)     = CHARGE EXCHANGE RECOMBINATION
C
C  INPUT : (R*8)  SAL0(,)     = IONISATION (NEUTRAL)
C  INPUT : (R*8)  SAL(,,)     = IONISATION (NON-NEUTRAL)
C
C  INPUT : (R*8)  PLTL0(,)    = TOTAL LINE RADIATED POWER (NEUTRAL)
C  INPUT : (R*8)  PLTL(,,)    = TOTAL LINE RADIATED POWER (NON-NEUTRAL)
C
C  INPUT : (R*8)  PRBL(,,)    = RAD. + DIEL RECOM. + BREMS. POWER
C  INPUT : (R*8)  PRCL(,,)    = CX. RECOM. POWER
C
C  INPUT : (R*8)  PLSL0(,)    = SPECIFIC LINE POWER   (NEUTRAL)
C  INPUT : (R*8)  PLSL(,,)    = SPECIFIC LINE POWER   (NON-NEUTRAL)
C
C  INPUT : (R*8)  PRBLNFL(,,) = RECOM+BREMM POWER (NO FILTER)
C  INPUT : (R*8)  PRCLNFL(,,) = CX POWER (NO FILTER)
C  INPUT : (R*8)  PLTLNFL(,,) = LINE POWER (NO FILTER)
C  INPUT : (R*8)  PLTL0NFL(,,)= NEUTRAL LINE POWER (NO FILTER)
C
C  INPUT : (C*8)  INFRAL()    = RECOMBINATION INFO STRING
C  INPUT : (C*8)  INFSAL()    = IONISATION INFO STRING
C  INPUT : (C*8)  INFCAL()    = CX INFO STRING
C  INPUT : (C*8)  INFPRB()    = RECOM+BREMM POWER INFO STRING
C  INPUT : (C*8)  INFPRC()    = CX POWER INFO STRING
C  INPUT : (C*8)  INFPLT()    = TOTAL LINE POWER INFO STRING
C  INPUT : (C*8)  INFPLS()    = SPECIFIC LINE POWER INFO STRING
C  INPUT : (C*8)  INFPRBN()   = RECOMM+BREM POWER (NO FILTER) INFO
C  INPUT : (C*8)  INFPRCN()   = CX POWER (NO FILTER) INFO STRING
C  INPUT : (C*8)  INFPLTN()   = TOTAL LINE POWER (NO FILTER) INFO
C
C  INPUT : (C*24) METRAL      = RECOMBINATION METHOD STRING
C  INPUT : (C*24) METSAL      = IONISATION METHOD STRING
C  INPUT : (C*24) METCAL      = CX METHOD STRING
C  INPUT : (C*24) METPRB      = RECOM+BREMM POWER METHOD STRING
C  INPUT : (C*24) METPRC      = CX POWER METHOD STRING
C  INPUT : (C*24) METPLT      = TOTAL LINE POWER METHOD STRING
C  INPUT : (C*24) METPLS      = SPECIFIC LINE POWER METHOD STRING
C  INPUT : (C*24) METPRBN     = RECOMM+BREM POWER (NO FILTER) METHOD
C  INPUT : (C*24) METPRCN     = CX POWER (NO FILTER) METHOD STRING
C  INPUT : (C*24) METPLTN     = TOTAL LINE POWER (NO FILTER) METHOD
C
C          (I*4)  I         = ARRAY INDEX.
C          (I*4)  I1        = ARRAY INDEX.
C          (I*4)  I2        = ARRAY INDEX.
C          (C*10) CSTRG1()  = FIRST PARENT/GROUND INFORMATION STRING
C          (C*10) CSTRG2()  = SECOND PARENT/GROUND INFORMATION STRING
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          IOADAS1    ADAS      OUTPUT STANDARD MASTER DATA EXCL NEUTRAL
C          IOADAS2    ADAS      OUTPUT STANDARD MASTER DATA INCL NEUTRAL
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    13/05/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C VERSION : 1.2				
C DATE    : 21-07-2003
C MODIFIED: Martin O'Mullane
C            - Add a comment section at the end which requires user, date
C              and adf03/adf35 filenames.
C            - Remove redundant variables.
C
C VERSION : 1.3                          
C DATE    : 04-10-2006
C MODIFIED: Hugh Summers
C            - Added handling of zcd, ycd and ecd classes.
C            - Adjusted details to preferred superstage nomenclatures. 
C
C VERSION : 1.4                          
C DATE    : 16-01-2007
C MODIFIED: Hugh Summers
C            - Added call to new version of d8wzcd with added dimension
C              parameter iodimd and imdimd. 
C
C VERSION : 1.5                          
C DATE    : 04-03-2009
C MODIFIED: Adam Foster
C            - Corrected order of electron temperature and density 
C              in call to d8wzcd
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    ITDIMD     , IDDIMD     , IZDIMD    , IODIMD  , IMDIMD
      INTEGER    IUNTBS
      INTEGER    NUMT       , NUMD       , NUMZ
      INTEGER    IZ0        , IZL        , IZU       , i4unit
      INTEGER    iuntz      , iunty      , iunte
C-----------------------------------------------------------------------
      LOGICAL    lfilter
      LOGICAL    lzcd       , lycd       , lecd
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , NAME*13    ,
     &           user*30    , dsn03*80   , dsn35*80  , type*40
      CHARACTER  METRAL*24  , METSAL*24  , METCAL*24 , METPRB*24  , 
     &           METPRC*24  , METPLT*24  , METPLS*24 , METPRBN*24 ,
     &           METPRCN*24 , METPLTN*24
C-----------------------------------------------------------------------
      REAL*8    CAL      (ITDIMD,IDDIMD,IZDIMD),
     &          PRBL     (ITDIMD,IDDIMD,IZDIMD),
     &          SAL      (ITDIMD,IDDIMD,IZDIMD),
     &          RAL      (ITDIMD,IDDIMD,IZDIMD),
     &          PRCL     (ITDIMD,IDDIMD,IZDIMD),
     &          PLTL     (ITDIMD,IDDIMD,IZDIMD),
     &          PLSL     (ITDIMD,IDDIMD,IZDIMD),
     &          SAL0     (ITDIMD,IDDIMD),
     &          PLTL0    (ITDIMD,IDDIMD),
     &          PLSL0    (ITDIMD,IDDIMD)
      REAL*8    PRBLNFL  (ITDIMD,IDDIMD,IZDIMD),
     &          PRCLNFL  (ITDIMD,IDDIMD,IZDIMD),
     &          PLTLNFL  (ITDIMD,IDDIMD,IZDIMD),
     &          PLTL0NFL (ITDIMD,IDDIMD)
      REAL*8    TEL(ITDIMD),FNEL(IDDIMD)
C-----------------------------------------------------------------------
      LOGICAL    OPCLAS(13)
C-----------------------------------------------------------------------
      CHARACTER*8 INFRAL(IZDIMD),INFSAL(IZDIMD),INFCAL(IZDIMD),
     &            INFPRB(IZDIMD),INFPRC(IZDIMD),INFPLT(IZDIMD),
     &            INFPLS(IZDIMD),INFPRBN(IZDIMD),INFPRCN(IZDIMD),
     &            INFPLTN(IZDIMD)
      CHARACTER   CSTRG1(13)*10  , CSTRG2(13)*10
C-----------------------------------------------------------------------
      DATA  CSTRG1 /' IPRT= 1  ' , ' IPRT= 1  ' , ' IPRT= 1  ' ,
     &              ' IPRT= 1  ' , ' IGRD= 1  ' , ' IGRD= 1  ' ,
     &              ' IPRT= 1  ' , ' IPRT= 1  ' , ' IGRD= 1  ' ,
     &              ' IPRT= 1  ' , ' IGRD= 1  ' , ' IGRD= 1  ' ,
     &              ' IGRD= 1  '/
      DATA  CSTRG2 /' IGRD= 1  ' , ' IGRD= 1  ' , ' IGRD= 1  ' ,
     &              ' IGRD= 1  ' , ' IPRT= 0  ' , ' IPRT= 0  ' ,
     &              ' IGRD= 1  ' , ' IGRD= 1  ' , ' IPRT= 0  ' ,
     &              ' IGRD= 1  ' , '----------' , '----------',
     &              '----------'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------

      NUMZ = IZU-IZL+1

C-----------------------------------------------------------------------
C   COLLISIONAL DIELECTRONIC - RADIATIVE RECOMBINATION RATE COEFFICIENTS
C-----------------------------------------------------------------------

 
      if (opclas(1)) then
         type    = 'radiative recombination'
         lfilter = .FALSE.
         CALL IOADAS1( IUNTBS+1  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  , RAL ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METRAL  ,
     &                 CSTRG1(1) , CSTRG2(1) , INFRAL  ,
     &                 user      , type      , dsn03   , dsn35 , lfilter 
     &               )
         write(i4unit(-1),1000)type
      endif
 

C-----------------------------------------------------------------------
C   IONISATION RATE COEFFICIENTS
C-----------------------------------------------------------------------

 
      if (opclas(2)) then
         type    = 'ionisation'
         lfilter = .FALSE.
         CALL IOADAS2( IUNTBS+2  , DATE      ,                            
     &                 ITDIMD    , IDDIMD    , IZDIMD  ,                  
     &                 SAL0      , SAL       ,                            
     &                 NUMT      , NUMD      , NUMZ    ,                  
     &                 FNEL      , TEL       ,                            
     &                 IZ0       , NAME      , METSAL  ,                  
     &                 CSTRG1(2) , CSTRG2(2) , INFSAL  ,                  
     &                 user      , type      , dsn03   , dsn35 , lfilter  
     &               )                                                    
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   CHARGE EXCHANGE RECOMBINATION
C-----------------------------------------------------------------------

 
      if (opclas(3)) then
         type    = 'charge exchange recombination'
         lfilter = .FALSE.
         CALL IOADAS1( IUNTBS+3  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  , CAL   ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METCAL  ,
     &                 CSTRG1(3) , CSTRG2(3) , INFCAL  ,
     &                 user      , type      , dsn03   , dsn35 , lfilter
     &               )
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   RADIATIVE AND DIELECTRONIC RECOMBINATION WITH BREMSSTRAHLUNG POWER
C-----------------------------------------------------------------------

 
 
      if (opclas(4)) then
         type    = 'continuum power'
         lfilter = .TRUE.
         CALL IOADAS1( IUNTBS+4  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  , PRBL  ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,  
     &                 IZ0       , NAME      , METPRB  ,
     &                 CSTRG1(4) , CSTRG2(4) , INFPRB  ,
     &                 user      , type      , dsn03   , dsn35 , lfilter 
     &               )
         write(i4unit(-1),1000)type
      endif
 

C-----------------------------------------------------------------------
C   TOTAL LINE RADIATED POWER
C-----------------------------------------------------------------------

 
      if (opclas(5)) then
         type    = 'total line power'
         lfilter = .TRUE.
         CALL IOADAS2( IUNTBS+5  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  ,
     &                 PLTL0     , PLTL      ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METPLT  ,
     &                 CSTRG1(5) , CSTRG2(5) , INFPLT  ,
     &                 user      , type      , dsn03   , dsn35 , lfilter
     &               )
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   SPECIFIC LINE POWER
C-----------------------------------------------------------------------

 
      if (opclas(6)) then
          type   = 'specific line power'
         lfilter = .FALSE.
          CALL IOADAS2( IUNTBS+6  , DATE      ,
     &                  ITDIMD    , IDDIMD    , IZDIMD ,
     &                  PLSL0     , PLSL      ,
     &                  NUMT      , NUMD      , NUMZ   ,
     &                  FNEL      , TEL       ,
     &                  IZ0       , NAME      , METPLS ,
     &                  CSTRG1(6) , CSTRG2(6) , INFPLS ,
     &                  user      , type      , dsn03  , dsn35 , lfilter
     &               )
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   CHARGE EXCHANGE RECOMBINATION POWER
C-----------------------------------------------------------------------

 
      if (opclas(7)) then
         type    = 'charge exchange recombination power'
         lfilter = .TRUE.
         CALL IOADAS1( IUNTBS+7  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  , PRCL    ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METPRC  ,
     &                 CSTRG1(7) , CSTRG2(7) , INFPRC  ,
     &                 user      , type      , dsn03   , dsn35   , 
     &                 lfilter
     &                )
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   RADIATIVE AND DIELECTRONIC RECOMBINATION WITH BREMSSTRAHLUNG POWER
C   NO FILTER APPLIED
C-----------------------------------------------------------------------

 
      if (opclas(8)) then
         type    = 'continuum power'
         lfilter = .FALSE.
         CALL IOADAS1( IUNTBS+8  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  , PRBLNFL ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METPRBN ,
     &                 CSTRG1(8) , CSTRG2(8) , INFPRBN ,
     &                 user      , type      , dsn03   , dsn35   , 
     &                lfilter 
     &               )
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   TOTAL LINE RADIATED POWER
C   NO FILTER APPLIED
C-----------------------------------------------------------------------


      if (opclas(9)) then
         type    = 'total line power'
         lfilter = .FALSE.
         CALL IOADAS2( IUNTBS+9  , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  ,
     &                 PLTL0NFL  , PLTLNFL   ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METPLTN ,
     &                 CSTRG1(9) , CSTRG2(9) , INFPLTN ,
     &                 user      , type      , dsn03   , dsn35 , lfilter
     &               )
         write(i4unit(-1),1000)type
      endif

C-----------------------------------------------------------------------
C   CHARGE EXCHANGE RECOMBINATION POWER
C   NO FILTER APPLIED
C-----------------------------------------------------------------------


      if (opclas(10)) then
         type    = 'charge exchange recombination power'
         lfilter = .FALSE.
         CALL IOADAS1( IUNTBS+10 , DATE      ,
     &                 ITDIMD    , IDDIMD    , IZDIMD  , PRCLNFL ,
     &                 NUMT      , NUMD      , NUMZ    ,
     &                 FNEL      , TEL       ,
     &                 IZ0       , NAME      , METPRCN ,
     &                 CSTRG1(10), CSTRG2(10), INFPRCN ,
     &                 user      , type      , dsn03   , dsn35   , 
     &                 lfilter
     &               )
         write(i4unit(-1),1000)type
      endif
 
C-----------------------------------------------------------------------
C   FINAL ZCD, YCD AND ECD FILES
C-----------------------------------------------------------------------

      lzcd  = opclas(11)
      lycd  = opclas(12)
      lecd  = opclas(13)
      iuntz = iuntbs+11
      iunty = iuntbs+12
      iunte = iuntbs+13 
      
      if(lzcd.or.lycd.or.lecd) then
      
         call d8wzcd( iuntz     , iunty     , iunte   ,
     &                lzcd      , lycd      , lecd    ,
     &                itdimd    , iddimd    , 
     &                izdimd    , iodimd    , imdimd  ,
     &                numt      , numd      ,
     &                tel       , fnel      ,
     &                iz0       , izl       , izu     ,
     &                user      , date   
     &              )  
         
      endif 

C-----------------------------------------------------------------------

 1000 format('Write adf11 file for : ', a)

C-----------------------------------------------------------------------

      END
