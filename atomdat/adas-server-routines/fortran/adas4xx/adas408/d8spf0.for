CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8spf0.for,v 1.1 2004/07/06 13:25:32 whitefor Exp $ Date $Date: 2004/07/06 13:25:32 $
CX
      SUBROUTINE D8SPF0( REP    , DSFULL)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS408
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL UNIX DSN)
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  TIM HAMMOND    (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    04-04-96
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 04-04-96
C MODIFIED: TIM HAMMOND 
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
      CHARACTER    REP*3        , DSFULL*80
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOUT
      PARAMETER(  PIPEIN=5      , PIPEOUT=6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
