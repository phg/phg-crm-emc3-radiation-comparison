CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8cros.for,v 1.1 2004/07/06 13:24:13 whitefor Exp $ Date $Date: 2004/07/06 13:24:13 $
CX
      SUBROUTINE D8CROS(NZDIM,CXC,TH,HM,NZ)
 
      IMPLICIT REAL*8 (A-H,O-Z)
 
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
      LOGICAL LFIT
 
      DIMENSION CXC(NZDIM),CXCT(10)
C
C DIMENSION CXCT TO LARGEST SPECIFIC FITTED ELEMENT AVAILABLE
C
 
      LFIT = .FALSE.
 
      IF (NZ.EQ.6) THEN
 
         LFIT = .TRUE.
 
C CARBON C II (Q=1)
 
         V      = D8VELH(TH,HM,11.0D0,32.0D0)
         CXCT(1) = 0.061D-16 * (1.0D0 + V / 61.5) * V
 
C CARBON C III (Q=2)
 
         V      = D8VELH(TH,HM,8.0D0,28.0D0)
         CXCT(2) = 9.16D-16 * EXP( -18.68 / V)
 
C CARBON C IV (Q=3)
 
         V      = D8VELH(TH,HM,3.0D0,35.0D0)
         CXCT(3) = (21.93 - 1.93 * V + 0.07 * V * V
     +            -0.000753 * V * V * V) * 1.0D-16
 
C CARBON C V (Q=4)
 
         V      = D8VELH(TH,HM,1.0D0,30.0D0)
         CXCT(4) = (2.66-1.76 * LOG(V) / V + 17.14 / V +
     +             37.87 * EXP(-8.51 / V)) * 1.0D-16
 
C CARBON C VI (Q=5)
 
         V      = D8VELH(TH,HM,6.0D0,31.0D0)
         CXCT(5) = (3.24 - 377.3 * LOG(V) / (V * V) + 276.6 / V +
     +             0.320 * V * EXP(-6.52 / V)) * 1.0D-16
 
C CARBON C VII (Q=6)
 
         V      = D8VELH(TH,HM,2.0D0,35.0D0)
         CXCT(6) = V * 1.0D-16 * EXP(0.676 - 15.05 / V)
 
 
 
      ENDIF
 
 
 
      IF (NZ .EQ. 8) THEN
 
         LFIT = .TRUE.
 
C OXYGEN O II (Q=1)
 
         V      = D8VELH(TH,HM,6.0D0,110.0D0)
         CXCT(1) = (15.83 - 0.374 * V + 0.003913 * V * V
     +            - 0.0000138 * V * V * V) * 1.0D-16
 
C OXYGEN O III (Q=2)
 
         V      = D8VELH(TH,HM,1.0D0,80.0D0)
         CXCT(2) = (8.84 - 0.29 * V + 0.00283 * V * V) * 1.0D-16
 
C OXYGEN O IV (Q=3)
 
         V      = D8VELH(TH,HM,5.0D0,44.0D0)
         CXCT(3) = (27.22 + 1.013 * V - 0.0203 * V * V) * 1.0D-16
 
C OXYGEN O V (Q=4)
 
         V      = D8VELH(TH,HM,10.0D0,50.0D0)
         CXCT(4) = (41.53 - 0.994 * V + 0.0147 * V * V) * 1.0D-16
 
C OXYGEN O VI (Q=5)
 
         V      = D8VELH(TH,HM,10.0D0,100.0D0)
         CXCT(5) = (58.75 - 0.685 * V + 0.00427 * V * V) * 1.0D-16
 
C OXYGEN O VII (Q=6)
 
         V      = D8VELH(TH,HM,4.0D0,100.0D0)
         CXCT(6) = (25.772 + 0.755 * V - 0.0143 * V * V
     +            + 0.0000794 * V * V * V) * 1.0D-16
 
C OXYGEN O VIII (Q=7)
 
         V      = D8VELH(TH,HM,30.0D0,100.0D0)
         CXCT(7) = (31.25 - 0.0553 * V ) * 1.0D-16
 
C OXYGEN O IX (Q=8)
 
         V      = D8VELH(TH,HM,4.0D0,100.0D0)
         CXCT(8) = (1.493 + 1.809 * V - 0.0116 * V * V) * 1.0D-16
 
 
      ENDIF
 
 
 
      IF (NZ .EQ. 7) THEN
 
         LFIT = .TRUE.
 
C NITROGEN N II (Q=1)
 
         V      = D8VELH(TH,HM,7.0D0,38.0D0)
         CXCT(1) = (1.73825 + 0.1023 * V) * 1.0D-16
 
C NITROGEN N III (Q=2)
 
         V      = D8VELH(TH,HM,0.0D0,100.0D0)
         CXCT(2) = (5.8971 - 0.07059 * V + 0.00064 * V * V) * 1.0D-16
 
C NITROGEN N IV (Q=3)
 
         V      = D8VELH(TH,HM,0.0D0,100.0D0)
         IF (V.LT.10.0D0) THEN
            CXCT(3) = (3.659 + 12.191 * V - 1.0593 * V * V) * 1.0D-16
         ELSE
            CXCT(3) = (31.6 - 0.323 * V + 0.00177 * V * V) * 1.0D-16
         ENDIF
 
C NITROGEN N V (Q=4)
 
         V      = D8VELH(TH,HM,0.0D0,100.0D0)
         CXCT(4) = (31.237 + 0.00335 * V - 0.000723 * V * V) * 1.0D-16
 
C NITROGEN N VI (Q=5)
 
         V      = D8VELH(TH,HM,5.0D0,100.0D0)
         CXCT(5) = (50.7561 - 1.8545 * V + 0.03217 * V * V
     +            -0.0001562 * V * V * V) * 1.0D-16
 
C NITROGEN N VII (Q=6)
 
         V      = D8VELH(TH,HM,1.0D0,100.0D0)
         CXCT(6) = (27.0D0 + 0.037 * V) * 1.0D-16
 
C NITROGEN N VIII (Q=7)
 
         V      = D8VELH(TH,HM,25.0D0,110.0D0)
         CXCT(7) = (29.36 - 0.162 * V) * 1.0D-16
 
 
      ENDIF
 
 
 
C FOR ANY CALL WITH NZ <> 6, 7, OR 8, ASSUME CXC = 6.0D-16 * Q**1.07
 
      DO I = 1,NZ
         CXC(I) = 6.0D-16 * FLOAT(I)**1.07
      END DO
 
      IF (LFIT) THEN
         DO I = 1,NZ
            CXC(I) = CXCT(I)
         END DO
      ENDIF
 
      RETURN
      END
