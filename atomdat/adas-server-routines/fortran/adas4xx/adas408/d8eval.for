       SUBROUTINE D8EVAL ( dsn03    , dsn35    ,
     &                     numte    , numne    , fuelmas  , 
     &                     TEL      , FNEL     , NAME     , 
     &                     iz0      , izl      , izu      ,
     &                     RAL      , SAL0     , SAL      , CAL     ,
     &                     PRBL     , PRCL     , PLTL0    , PLTL    ,
     &                     PLSL0    , PLSL     , 
     &                     PRBLNFL  , PRCLNFL  , PLTL0NFL , PLTLNFL ,
     &                     ralrr    , raldr    ,
     &                     prbrr    , prbdr    , prbbr    ,
     &                     prbrrnfl , prbdrnfl , prbbrnfl ,
     &                     INFRAL   , INFSAL   , INFCAL   ,
     &                     INFPRB   , INFPRC   , INFPLT   , INFPLS  ,
     &                     INFPRBN  , INFPRCN  , INFPLTN  ,
     &                     METRAL   , METSAL   , METCAL   ,
     &                     METPRB   , METPRC   , METPLT   , METPLS  ,
     &                     METPRBN  , METPRCN  , METPLTN  ,
     &                     ltick            
     &                   )
 
 
      implicit none
 
 
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8EVAL *********************
C
C
C  PURPOSE:  To calculate tables of values of ionisation, recombination
C            and radiated power rates for non-coronal impurity studies
C            over a given temperature and density range with atomic
C            data parameters from files of format adf03
C
C  CALLING PROGRAM: ADAS408
C
C
C
C  SUBROUTINE:
C
C  INPUT:  (C*80) dsn03       = adf03 atompars file
C  INPUT:  (C*80) dsn35       = adf35 filter data file
C
C  
C
C  THE OUTPUT ARRAYS ARE INDEXED
C          XXX(,) = XXX(ITDIMD,IDDIMD,IZDIMD) 1ST DIMENSION TEMPERATURE
C                                              2ND DIMENSION DENSITY
C                                              3RD DIMENSION ION STAGE
C  ALL TABLES ARE LOG10 IN CGS UNITS
C
C
C  OUTPUT: (R*8)  TEL()       = TEMPERATURE SET OF TABLES - LOG MESH
C          (R*8)  FNEL()      = DENSITY SET OF TABLES - LOG MESH
C          (C*13) NAME        = ELEMENT NAME
C
C          (R*8)  RAL(,,)     = RADIATIVE AND DIELECTRONIC RECOMB.
C          (R*8)  CAL(,,)     = CHARGE EXCHANGE RECOMBINATION
C
C          (R*8)  SAL0(,)     = IONISATION (NEUTRAL)
C          (R*8)  SAL(,,)     = IONISATION (NON-NEUTRAL)
C
C          (R*8)  PLTL0(,)    = TOTAL LINE RADIATED POWER (NEUTRAL)
C          (R*8)  PLTL(,,)    = TOTAL LINE RADIATED POWER (NON-NEUTRAL)
C
C          (R*8)  PRBL(,,)    = RAD. + DIEL RECOM. + BREMS. POWER
C          (R*8)  PRCL(,,)    = CX. RECOM. POWER
C
C          (R*8)  PLSL0(,)    = SPECIFIC LINE POWER   (NEUTRAL)
C          (R*8)  PLSL(,,)    = SPECIFIC LINE POWER   (NON-NEUTRAL)
C
C          (R*8)  PRBLNFL(,,) = RECOM+BREMM POWER (NO FILTER)
C          (R*8)  PRCLNFL(,,) = CX POWER (NO FILTER)
C          (R*8)  PLTLNFL(,,) = LINE POWER (NO FILTER)
C          (R*8)  PLTL0NFL(,,)= NEUTRAL LINE POWER (NO FILTER)
C
C          (C*8)  INFRAL()    = RECOMBINATION INFO STRING
C          (C*8)  INFSAL()    = IONISATION INFO STRING
C          (C*8)  INFCAL()    = CX INFO STRING
C          (C*8)  INFPRB()    = RECOM+BREMM POWER INFO STRING
C          (C*8)  INFPRC()    = CX POWER INFO STRING
C          (C*8)  INFPLT()    = TOTAL LINE POWER INFO STRING
C          (C*8)  INFPLS()    = SPECIFIC LINE POWER INFO STRING
C          (C*8)  INFPRBN()   = RECOMM+BREM POWER (NO FILTER) INFO
C          (C*8)  INFPRCN()   = CX POWER (NO FILTER) INFO STRING
C          (C*8)  INFPLTN()   = TOTAL LINE POWER (NO FILTER) INFO
C
C          (C*24) METRAL      = RECOMBINATION METHOD STRING
C          (C*24) METSAL      = IONISATION METHOD STRING
C          (C*24) METCAL      = CX METHOD STRING
C          (C*24) METPRB      = RECOM+BREMM POWER METHOD STRING
C          (C*24) METPRC      = CX POWER METHOD STRING
C          (C*24) METPLT      = TOTAL LINE POWER METHOD STRING
C          (C*24) METPLS      = SPECIFIC LINE POWER METHOD STRING
C          (C*24) METPRBN     = RECOMM+BREM POWER (NO FILTER) METHOD
C          (C*24) METPRCN     = CX POWER (NO FILTER) METHOD STRING
C          (C*24) METPLTN     = TOTAL LINE POWER (NO FILTER) METHOD
C
C
C  PROGRAM:
C
C  THE PARAMETER ARRAYS ARE INDEXED
C            XXX()  = XXX(IZDIMD)         1ST DIMENSION ION STAGE
C
C            XXX(,) = XXX(IZDIMD,IGDIMD)  1ST DIMENSION ION STAGE
C                                         2ND DIMENSION GROUP
C
C
C  INPUT:  (I*4)  IZDIMD    = MAXIMUM NUMBER OF IONISATION STAGES
C          (I*4)  IGDIMD    = MAXIMUM NUMBER OF GROUPS
C
C          (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMPERATURES
C          (I*4)  IDDIMD    = MAXIMUM NUMBER OF DENSITIES
C
C          (R*8)  TEMIN     = MINIMIUM TEMPERATURE OF TABLE
C          (R*8)  TEMAX     = MAXIMUM  TEMPERATURE
C          (I*4)  NUMTE     = NUMBER OF TEMPERATURE
C
C          (R*8)  FNEMIN    = MINIMIUM DENSITY OF TABLE
C          (R*8)  FNEMAX    = MAXIMUM  DENSITY
C          (I*4)  NUMNE     = NUMBER OF DENSITIES
C
C
C          (R*8)  FUELMAS   = MASS OF FUEL     (AMU)
C
C          (I*4)  IZ0       =  NUCLEAR CHARGE
C          (I*4)  IZL       =  LOWEST INCLUDED ION
C          (I*4)  IZU       =  HIGHEST INCLUDED ION
C
C          (I*4)  IZRA()    =  RECOMBINING ION (RAD.  RECOM.)
C          (I*4)  IZDA()    =  RECOMBINING ION (DIEL. RECOM.)
C          (I*4)  IZIA()    =  IONISING ION    (COLL. IONIS.)
C          (I*4)  IZTA()    =  RADIATING ION   (TOTAL LINE POWER)
C          (I*4)  IZSA()    =  RADIATING ION   (SPECIFIC LINE POWER)
C
C
C
C          (C*5)  CRRCA()   =  RADIATIVE RECOM. CODE
C          (I*4)  NRRCA()   =    - NOT USED -
C          (I*4)  ISRRCA()  =    - NOT USED -
C
C          (I*4)  NZA()     = LOWEST ACCESSIBLE SHELL FOR RAD. RECOM.
C          (I*4)  KSIA()    = NUMBER OF ELECTRONS IN SHELL
C
C          (I*4)  N0RA()    = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR RAD. RECOM.
C          (R*8)  V0RA()    = EFFECTIVE PRINCIPAL QUANTUM NUMBER
C                             FOR SHELL
C          (R*8)  PHFCRA()  = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR SHELL
C          (R*8)  EDSPRA()  = ENERGY ADJUSTMENT IN LOWEST SHELL
C                             RATE COEFFICIENT
C          (R*8)  SCLERA()  = MULTIPLIER FOR LOWEST SHELL
C                             RATE COEFFICIENT
C
C
C
C          (C*5)  CDRCA()   =  DIELECTRONIC RECOM. CODE
C          (I*4)  NDRCA()   =  NUMBER OF TRANSITIONS FOLLOWING
C          (I*4)  ISDRCA()  =    - NOT USED -
C
C          (R*8)  DEDA(,)   = TRANSITION ENERGY (EV)
C          (R*8)  FDA(,)    = OSCILLATOR STRENGTH
C          (R*8)  GDA(,)    = GAUNT FACTOR
C          (I*4)  NNDA(,)   = DELTA N FOR TRANSITION
C          (I*4)  MSDA(,)   = MERTZ SWITCH (0=OFF, 1=ON)
C
C          (I*4)  ITYPDA(,) = TYPE OF DIELECTRONIC TRANSITION
C          (I*4)  N0DA(,)   = LOWEST ACCESSIBLE PRINC. QUANTUM SHELL
C                             FOR DIEL. RE
C          (I*4)  NCUTA(,)  = CUT-OFF PRINC. QUANTUM SHELL IN
C                             GENERAL PROGRAM
C          (I*4)  V0DA(,)   = EFFECTIVE PRINC. QUANTUM NUMBER
C                             FOR LOWEST ACCESS
C          (R*8)  PHFCDA(,) = PHASE SPACE OCCUPANCY AVAILABILITY
C                             FOR LOWEST SHELL
C          (R*8)  CRFCDA(,) = ADJUSTMENT FOR BETHE CORRECTIONS
C                             IN GENERAL PROGRAM
C          (R*8)  EPSIJA(,) = Z-SCALED PARENT TRANSITION ENERGY (RYD)
C          (R*8)  FIJA(,)   = OSCILLATOR STRENGTH FOR TRANSITION
C          (R*8)  EDSPDA(,) = ENERGY ADJUSTMENT IN BURGESS GENERAL
C                             FORMULA (RYD)
C          (R*8)  SCLEDA(,) = MULTIPLIER ON BURGESS GENERAL FORMULA
C
C
C
C          (C*5)  CCIOA()   =  COLLISIONAL IONIS. CODE
C          (I*4)  NCIOSA()  =  NUMBER OF SHELL  VALUES FOLLOWING
C          (I*4)  NCIORA()  =  NUMBER OF RESON. VALUES FOLLOWING
C          (I*4)  ISCIOA()  =    - NOT USED -
C
C          (R*8)  PIOA(,)   = SHELL IONISATION POTENTIAL (EV)
C          (R*8)  AIOA(,)   = LOTZ PARAMETER
C          (R*8)  BIOA(,)   = LOTZ PARAMETER
C          (R*8)  CIOA(,)   = LOTZ PARAMETER
C          (I*4)  NQIOA(,)  = EQUIVALENT ELECTRONS IN SHELL
C
C          (R*8)  ZETAA(,)  = NUMBER OF EQUIVALENT ELECTRONS FOR SHELL
C          (R*8)  EIONA(,)  = IONISATION ENERGY FOR SHELL (RYD)
C          (R*8)  CIA(,)    = MULTIPLIER FOR BURGESS-CHIDICHIMO RATE
C                             FOR SHELL
C          (R*8)  WGHTA(,)  = WEIGHTING FACTOR FOR EXCITATION TO
C                             RESONANCE
C          (R*8)  ENERA(,)  = EXCITATION ENERGY FOR TRANSITION
C                             TO RESONANCE (RYD)
C          (R*8)  CRA(,)    = MULTIPLIER ON EXCITATION RATE EXPRESSSION
C
C
C
C          (C*5)  CPLTA()   =  TOTAL LINE POWER CODE
C          (I*4)  NPLTA()   =  NUMBER OF TRANSITIONS FOLLOWING
C          (I*4)  ISPLTA()  =    - NOT USED -
C
C          (R*8)  DEPTA(,)  = TRANSITION ENERGY (EV)
C          (R*8)  FPTA(,)   = OSCILLATOR STRENGTH
C          (R*8)  GPTA(,)   = GAUNT FACTOR
C          (I*4)  NNPTA(,)  = DELTA N FOR TRANSITION
C
C          (R*8)  SPYLTA(,) = MULTIPLIER OF VAN REGEMORTER P
C                             FACTOR IN TOTAL POWER
C
C
C
C          (C*5)  CPLSA()   = SPECIFIC LINE POWER CODE
C          (I*4)  NPLSA()   =   - NOT USED -
C          (I*4)  ISPLSA()  =   - NOT USED -
C          (C*8)  INFO()    = WAVELENGTH OF SPECIFIC LINE FOR
C                             NAMING PURPOSES
C
C          (R*8)  DEPSA(,)  = TRANSITION ENERGY (EV)
C          (R*8)  FPSA(,)   = OSCILLATOR STRENGTH
C          (R*8)  GPSA(,)   = GAUNT FACTOR
C          (I*4)  NNPSA(,)  = DELTA N FOR TRANSITION
C
C          (R*8)  SPYLSA(,) = MULTIPLIER OF VAN REGEMORTER P FACTOR
C                             IN SPECIFIC LINE POWER
C
C
C
C  ROUTINES:
C          ROUTINE   SOURCE     DESCRIPTION
C          ----------------------------------------------------------
C          I4UNIT    ADAS       FETCH UNIT NUMBER FOR MESSAGE OUTPUT
C          D8FLIN    ADAS408    INITIALISE ENERGY MESH FOR FILTER
C                               INTEGRATION
C          D8TRAN    ADAS408    FILTER TRANSMISSION AT PARTICULAR ENERGY
C          D8INTG    ADAS408    INTEGRATE WITH FILTER
C          D8CXSC    ADAS408    RETURNS CROSS-SECTIONS FROM CX COLLISION
C          D8VGOL    ADAS408    CALCULATES VON GOELER RECOMB. COEFFS
C          NGFFMH    ADAS       FREE-FREE GAUNT FACTOR                        
C
C
C
C  HISTORY:  DERIVED FROM NCRAT0  --- J. SPENSE,     TESSELLA
C                                     H. P. SUMMERS, JET
C                                     27/3/1990
C
C  CHANGES : 13/12/90  H.P.SUMMERS - CHANGE CONSTANTS FOR LINE POWER AND
C                                    BREMSSTRAHLUNG POWER TO MATCH
C                                    ABELS-VAN MAANEN (1985).
C                                    NB. NO MERTZ SWITCH ON H-LIKE
C                                    AND HE-LIKE DIELECTRONIC RECOMB.
C          :  5/ 2/91  H.P.SUMMERS - ALTER COMMON /RATCOM/ TO REMOVE
C                                    UNNECESSARY STORAGE AND ORGANISE
C                                    FOR NEW (ADF03) DATA INPUT.
C          :  1/ 8/91  H.P.SUMMERS - ALTER LINFO DIMENSION TO ALLOW
C                                    IT AS AN INFORMATION STRING FOR ALL
C                                    DATA SETS.  SET THE HYDROGEN
C                                    ISOTOPE MASS IN LINFO AS
C                                    ' MH=*.**' FOR CCD AND PRC.
C                                    ADD HMADAS TO /RATCOM/
C          :  6/8/92   M O'MULLANE - STAND-ALONE VERSION
C
C
C
C  ADAS408 IMPLEMENTATION
C
C  AUTHOR: M O'MULLANE, UCC
C
C  DATE:    10/05/94
C
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C VERSION: 1.2				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.3				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C           S.C.C.S. ERROR
C
C VERSION: 1.4				DATE: 30-01-98
C MODIFIED: Martin O'Mullane
C           Added free-free Gaunt factor to the eveluation of bremsstrahlung
C           power. Used NGFFMH to calculate gff.
C
C VERSION: 1.5				DATE: 05-08-2003
C MODIFIED: Martin O'Mullane
C            - ADAS408 restructured.
C            - Pass in adf03 and adf35 data files rather than the
C              data in them.
C
C VERSION : 1.6
C DATE    : 05-03-2004
C MODIFIED: Martin O'Mullane
C             - Alter dimensions for 55 temperatures, 50 densities, 
C               80 ion stages, and 15 groups.
C             - Type B ionisation was wrong. Calculate it with a call 
C               to rbchid.for.
C             - Warn when EA contribution to type B ionisation is
C               attempted as it is not coded.
C             - Warn when type B radiative recombination is
C               requested as the prb is not yet calculated.
C             - Pass out separated RR and DR recombination rates.
C             - Pass out separated RR, DR and BR power coefficients
C               but only for the non-filtered case.
C
C VERSION : 1.7
C DATE    : 06-10-2004
C MODIFIED: Martin O'Mullane
C             - Increase number of groups in the adf03 file to 20
C               from 15 to cope with high Z DR.  
C
C VERSION : 1.8
C DATE    : 24-06-2005
C MODIFIED: Martin O'Mullane
C            - Add arrays for filtered rr/dr/br power.
C
C VERSION : 1.9
C DATE    : 07-04-2008
C MODIFIED: Adam Foster
C            - No longer using wrong shell energy in case B
C              Dielectronic Recombination Power (POWDRC)
C
C VERSION : 1.10
C DATE    : 07-02-2010
C MODIFIED: Martin O'Mullane
C            - Te and dens are passed in as vectors, not min/max pairs.
C
C VERSION : 1.11
C DATE    : 08-12-2011
C MODIFIED: Martin O'Mullane
C            - Increase the number of filter edges to 10 (from 6).
C
C VERSION : 1.12
C DATE    : 27-05-2014
C MODIFIED: Martin O'Mullane
C            - Incorrect parent energy used for type B DR contribution
C              to PRB.
C
C-----------------------------------------------------------------------
      INTEGER    IUNT10      , PIPEOU        , ONE          ,
     &           ITDIMD      , IDDIMD        , IZDIMD       , IGDIMD
      INTEGER    ndedge      , ndeng         , KJVAL        , KNVAL
      INTEGER    KTEDIM      , KNEDIM        , KZDIM        , KGDIM
C-----------------------------------------------------------------------
      PARAMETER (IUNT10 = 10 , PIPEOU = 6    , ONE    = 1   ,
     &           ITDIMD = 55 , IDDIMD = 50   , IZDIMD = 80  , 
     &           IGDIMD = 20 )
      PARAMETER (KTEDIM = 55 , KNEDIM = 50   , KZDIM  = 80  ,
     &           KGDIM  = 20 )
      PARAMETER (ndedge = 10 , ndeng  = 2500 , KJVAL  = 50  ,
     &           KNVAL  = 21 )
C-----------------------------------------------------------------------
      REAL*8     EXP1        , EXP2          , EXP3         , IH
C-----------------------------------------------------------------------
      PARAMETER ( EXP1 = 1.0D0/17.0D0        , EXP2 = 2.0D0/17.0D0     , 
     &            EXP3=12.0D0/17.0D0         , IH   = 13.6048D0)
C-----------------------------------------------------------------------
      INTEGER    NUMTE       , NUMNE
      INTEGER    IZ0         , IZL           , IZU          , iedge    , 
     &           ieng
      INTEGER    I4UNIT
      integer    i           , j             , k            , KJJ      ,    
     &           JJU         , JJ            , JL           , IK       ,
     &           NUMZ        , NZ            , IRPOW        , IDPOW    ,
     &           ICPOW       , ICIOS         , ICIOR        , NG       ,    
     &           N0          , IDRC          , IREF         , IPLT     ,   
     &           IPLS        , ng0
C-----------------------------------------------------------------------
      REAL*8     TEMIN       , TEMAX         , FNEMIN       , FNEMAX   ,
     &           FUELMAS
      REAL*8     TE          , FNE
      REAL*8     TEK         , FZ            , TEMINLG      , TEMAXLG  , 
     &           DTLG        , FNMINLG       , FNMAXLG      , DNLG     ,
     &           PY          , Z1            , PS6
      REAL*8     ALM         , FSA           , Y            , S1       ,
     &           S2          , S5            , PARTR        , ATE      ,    
     &           FACT        , XI            , XIATE        , FACI     ,
     &           DXIATE      , FACR          , S3           , S4       ,     
     &           S6          , Y1            , RTEMP        , Y2       ,
     &           PS1B        , PS1BNFL       , S1B          , PS1      ,     
     &           PS1NFL      , DPTEA         , DPV0         , DPPHF    ,  
     &           DPEDSP      , DPSCAL        , FAC          , ZZ       ,  
     &           DPZ1        , DPGA          , DPGA0        , DPGRST   ,
     &           ADIELP      , PT            , PTNFL        , TMENERGY , 
     &           S1NFL       , FRAC          , PS           , PS5      ,    
     &           PS4         , SUM1          , SUM1NFL      , S8       ,     
     &           S8NFL       , PB            , PBNFL        , PC       ,
     &           PCNFL       , FRAC1         , FRAC2        , VAV      ,    
     &           FCA
      REAL*8     EEI         , NGFFMH        , rbchid
C-----------------------------------------------------------------------
      character  dsn03*80    , dsn35*80
      CHARACTER  XFELEM*12   , NAME*13       , STRG3*8
      character  METRAL*24   , METSAL*24     , METCAL*24    , METPRB*24,
     &           METPRC*24   , METPLT*24     , METPLS*24    , 
     &           METPRBN*24  , METPRCN*24    , METPLTN*24
C-----------------------------------------------------------------------
      logical    ltick       , lvalid
      LOGICAL    LRPOW       , LDPOW         , LCPOW
C-----------------------------------------------------------------------
      INTEGER   IZRA(IZDIMD) , IZDA(IZDIMD)  , IZIA(IZDIMD) ,
     &          IZTA(IZDIMD) , IZSA(IZDIMD)  ,
     &          NRRCA(IZDIMD), ISRRCA(IZDIMD),
     &          NZA(IZDIMD)  , KSIA(IZDIMD)  ,
     &          N0RA(IZDIMD) , 
     &          NDRCA(IZDIMD), ISDRCA(IZDIMD),
     &          NNDA(IZDIMD,IGDIMD)          , MSDA(IZDIMD,IGDIMD)     ,
     &          ITYPDA(IZDIMD,IGDIMD)        , N0DA(IZDIMD,IGDIMD)     ,
     &          NCUTA(IZDIMD,IGDIMD)         , NCIOSA(IZDIMD)          , 
     &          NCIORA(IZDIMD)               , ISCIOA(IZDIMD)          ,
     &          NQIOA(IZDIMD,IGDIMD)         , NPLTA(IZDIMD)           , 
     &          ISPLTA(IZDIMD)               , NNPTA(IZDIMD,IGDIMD)    ,
     &          NNPSA(IZDIMD,IGDIMD)         , NPLSA(IZDIMD)           , 
     &          ISPLSA(IZDIMD)               , JUP(KJVAL)
C-----------------------------------------------------------------------
      real*8    edge(ndedge) , energy(ndeng) , fraction(ndeng) 
      REAL*8    PHFCRA(IZDIMD)               , EDSPRA(IZDIMD)          ,
     &          SCLERA(IZDIMD)               , DEDA(IZDIMD,IGDIMD)     , 
     &          FDA(IZDIMD,IGDIMD)           , GDA(IZDIMD,IGDIMD)      ,
     &          V0DA(IZDIMD,IGDIMD)          ,
     &          PHFCDA(IZDIMD,IGDIMD)        , CRFCDA(IZDIMD,IGDIMD)   ,
     &          EPSIJA(IZDIMD,IGDIMD)        , FIJA(IZDIMD,IGDIMD)     ,
     &          EDSPDA(IZDIMD,IGDIMD)        , SCLEDA(IZDIMD,IGDIMD)   ,
     &          PIOA(IZDIMD,IGDIMD)          , AIOA(IZDIMD,IGDIMD)     ,
     &          BIOA(IZDIMD,IGDIMD)          , CIOA(IZDIMD,IGDIMD)     ,
     &          ZETAA(IZDIMD,IGDIMD)         , EIONA(IZDIMD,IGDIMD)    ,
     &          CIA(IZDIMD,IGDIMD)           , WGHTA(IZDIMD,IGDIMD)    ,
     &          ENERA(IZDIMD,IGDIMD)         , CRA(IZDIMD,IGDIMD)      ,
     &          DEPTA(IZDIMD,IGDIMD)         , FPTA(IZDIMD,IGDIMD)     ,
     &          GPTA(IZDIMD,IGDIMD)          , SPYLTA(IZDIMD,IGDIMD)   ,
     &          DEPSA(IZDIMD,IGDIMD)         , FPSA(IZDIMD,IGDIMD)     ,
     &          GPSA(IZDIMD,IGDIMD)          , SPYLSA(IZDIMD,IGDIMD)   ,
     &          V0RA(IZDIMD)
      REAL*8    CAL(ITDIMD,IDDIMD,IZDIMD)    ,
     &          PRBL(ITDIMD,IDDIMD,IZDIMD)   ,
     &          SAL(ITDIMD,IDDIMD,IZDIMD)    ,
     &          RAL(ITDIMD,IDDIMD,IZDIMD)    ,
     &          PRCL(ITDIMD,IDDIMD,IZDIMD)   ,
     &          PLTL(ITDIMD,IDDIMD,IZDIMD)   ,
     &          PLSL(ITDIMD,IDDIMD,IZDIMD)   ,
     &          SAL0(ITDIMD,IDDIMD)          ,
     &          PLTL0(ITDIMD,IDDIMD)         ,
     &          PLSL0(ITDIMD,IDDIMD)
      REAL*8    PRBLNFL(ITDIMD,IDDIMD,IZDIMD),
     &          PRCLNFL(ITDIMD,IDDIMD,IZDIMD),
     &          PLTLNFL(ITDIMD,IDDIMD,IZDIMD),
     &          PLTL0NFL(ITDIMD,IDDIMD)
      REAL*8    TEL(ITDIMD)                  , FNEL(ITDIMD)           ,
     &          S5P(KNVAL)                   , PS5P(KNVAL)            ,
     &          PS5PNFL(KNVAL)               ,
     &          RADI6(KTEDIM, KZDIM, KGDIM)  ,
     &          POWRRCNFL(KTEDIM,KNEDIM,KZDIM),
     &          POWDRCNFL(KTEDIM,KNEDIM,KZDIM),
     &          POWBRSNFL(KTEDIM,KNEDIM,KZDIM),
     &          POWRRC(KTEDIM,KNEDIM,KZDIM)   ,
     &          POWDRC(KTEDIM,KNEDIM,KZDIM)   ,
     &          POWBRS(KTEDIM,KNEDIM,KZDIM)   , CXC(KZDIM)
      REAL*8    ralrr(ITDIMD,IDDIMD,IZDIMD)   ,
     &          raldr(ITDIMD,IDDIMD,IZDIMD)   ,
     &          prbrr(ITDIMD,IDDIMD,IZDIMD)   ,
     &          prbdr(ITDIMD,IDDIMD,IZDIMD)   ,
     &          prbbr(ITDIMD,IDDIMD,IZDIMD)   ,
     &          prbrrnfl(ITDIMD,IDDIMD,IZDIMD),
     &          prbdrnfl(ITDIMD,IDDIMD,IZDIMD),
     &          prbbrnfl(ITDIMD,IDDIMD,IZDIMD)
C-----------------------------------------------------------------------
      CHARACTER CRRCA(IZDIMD)*5              , CDRCA(IZDIMD)*5         , 
     &          CCIOA(IZDIMD)*5              , CPLTA(IZDIMD)*5         , 
     &          CPLSA(IZDIMD)*5
      CHARACTER INFO(IZDIMD)*8               , INFRAL(IZDIMD)*8        , 
     &          INFSAL(IZDIMD)*8             , INFCAL(IZDIMD)*8        ,
     &          INFPRB(IZDIMD)*8             , INFPRC(IZDIMD)*8        ,
     &          INFPLT(IZDIMD)*8             , INFPLS(IZDIMD)*8        ,
     &          INFPRBN(IZDIMD)*8            , INFPRCN(IZDIMD)*8       ,
     &          INFPLTN(IZDIMD)*8
      CHARACTER METHOD(2)*24
C-----------------------------------------------------------------------
      DATA METHOD /'ABELS-VAN MAANEN 1985   ',
     &             'SUMMERS & DICKSON 1992  '/
C-----------------------------------------------------------------------
      external rbchid
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C  Open adf files to fetch in atomic parameters and filter data.
C-----------------------------------------------------------------------

      open(unit=iunt10 , file=dsn03 , status='UNKNOWN')

      call xxdata_03( iunt10,  izdimd,  igdimd,
     &                iz0,     izl,     izu,
     &                izra,    izda,    izia,    izta,    izsa,
     &                crrca,   nrrca,   isrrca,
     &                nza,     ksia,
     &                n0ra,    v0ra,    phfcra,  edspra,  sclera,
     &                cdrca,   ndrca,   isdrca,
     &                deda,    fda,     gda,     nnda,    msda,
     &                itypda,  n0da,    ncuta,   v0da,    phfcda,
     &                crfcda,  epsija,  fija,    edspda,  scleda,
     &                ccioa,   nciosa,  nciora,  iscioa,
     &                pioa,    aioa,    bioa,    cioa,    nqioa,
     &                zetaa,   eiona,   cia,
     &                wghta,   enera,   cra,
     &                cplta,   nplta,   isplta,
     &                depta,   fpta,    gpta,    nnpta,   spylta,
     &                cplsa,   nplsa,   isplsa,  info,
     &                depsa,   fpsa,    gpsa,    nnpsa,   spylsa,
     &                lvalid
     &              )


      name=xfelem(iz0)//' '
      close( iunt10 )

      if (dsn35.NE.'NULL') then
      
         open(unit=iunt10 , file=dsn35 , status='UNKNOWN')
         call xxdata_35( iunt10  , 
     &                   ndedge  , ndeng  ,
     &                   iedge   , ieng   , 
     &                   edge    , energy , fraction
     &                 )
         close( iunt10 )
         
      else
      
         iedge = 0
         ieng  = 0
         
      endif

 
C------------------
C  Te/dens in log10
C------------------

      do i=1,numte
        tel(i) = dlog10(tel(i))
      end do
 
      do i = 1,numne
        fnel(i) = dlog10(fnel(i))
      end do

 
C-----------------------------------------------------------------------
C  COMPUTE RATE COEFFT. DATA USING SEMI-EMPIRICAL APPROXIMATE FORMULAE
C
C    *  USES EITHER  (A) OLD  PARAMETRISATIONS USED BY ABELS-VAN MAANEN
C               OR   (B) NEW  PARAMETRISATIONS RECOMMENDED IN ADAS
C-----------------------------------------------------------------------
 
      WRITE(STRG3,'('' MH='',F4.2)')FUELMAS
 
 
C  SET ALM=1.0 TO ENSURE DIELECTRONIC RECOMBINATION IS ON

      ALM = 1.0D0
 
C  NUMBER OF IONISATION STAGES CONSIDERED

      NUMZ = IZU - IZL + 1
 
 
 
C-----------------------------------------------------------------------
C
C #######################  BEGIN CALCULATIONS  ########################
C
C-----------------------------------------------------------------------
 
 
 
      DO I=1,NUMTE
 
        TE = 10.0D0**TEL(I)
 
 
C Send a progress event if requested

        if (ltick) then
           write(pipeou,*)one
           call xxflsh(pipeou)
        endif
                
C------------------------------------------------------
C  CALCULATION OF IONISATION RATES
C
C      INDEX "1" MEANS IONISATION FROM 1. TO 2. STAGE
C------------------------------------------------------
 
        DO IK=1,NUMZ
          FSA = 0.0D0
 
          IREF = IZIA(IK) - IZL
 
C-----------------
C  VM 1985
C-----------------
 
          IF (CCIOA(IK).EQ.'CIO#A') THEN
            METSAL=METHOD(1)
            INFSAL(IK)='IONIS   '
 
            DO ICIOS = 1,NCIOSA(IK)
              Y = PIOA(IK,ICIOS) / TE
 
              IF (Y.LE.100.0D0) THEN
                S1 = EXP(-Y) * EEI(Y)
                S2 = EXP(-Y - CIOA(IK,ICIOS)) *
     &               EEI(Y + CIOA(IK,ICIOS))
                S5 = AIOA(IK,ICIOS) * NQIOA(IK,ICIOS) *
     &              (S1 / Y -   BIOA(IK,ICIOS) *
     &               EXP(CIOA(IK,ICIOS)) * S2 /
     &               (Y + CIOA(IK,ICIOS)))
                PARTR = 6.7D-07 * S5 / (TE * SQRT(TE))
                FSA = FSA + 6.7D-07 * S5 / (TE * SQRT(TE))
              ENDIF
            END DO
 
C-----------------
C  S&D 1992
C-----------------
 
          ELSE
            METSAL=METHOD(2)
            INFSAL(IK)='IONIS   '
 
            ATE = 11605.0 * te
            DO ICIOS = 1,NCIOSA(IK)
              y = izia(ik)
              PARTR = CIA(IK,ICIOS) * 
     &                rbchid(y, eiona(ik,icios), 
     &                       zetaa(ik,icios), ate )      
              FSA = FSA + PARTR
            END DO
            IF (NCIORA(IK).GE.1) THEN
               write(i4unit(-1),1010)'EA corrections not enabled'
C                DO ICIOR = 1,NCIORA(IK)
C                  XI = ENERA(IK,ICIOR)
C                  XIATE = XI * ATE
C                  FACR = 1.45D0 * 8.7972D-17 * WGHTA(IK,ICIOR) * ATE/XI
C                  PARTR=CRA(IK,ICIOR) * FACT * FACR * EXP(-XIATE)
C                  FSA = FSA + PARTR
C                END DO
            ENDIF
          ENDIF
 
          FSA = MAX(FSA,1.0D-74)
          FSA = LOG10(FSA)
          DO J = 1,NUMNE
            IF (IREF.EQ.0) THEN
               SAL0(I,J) = FSA
            ELSE
               SAL(I,J,IREF) = FSA
            ENDIF
          END DO
 
        END DO

 
C-----------------------------------------------------------------------
C  CALCULATION OF RADIATIVE RECOMBINATION RATES
C  CALCULATION RADIATIVE RECOMBINATION POWER AT SAME TIME
C
C     INDEX "1" MEANS RECOMBINATION FROM  1. TO  0. STAGE
C-----------------------------------------------------------------------
 
        DO IK=1,NUMZ
 
          IREF = IZRA(IK) - IZL
          FZ   = dfloat(IZRA(IK))
          NZ   = INT(FZ)
 
 
C------------------
C  VM 1985
C------------------
 
          IF (CRRCA(IK).EQ.'RRC#A') THEN
             METRAL=METHOD(1)
             INFRAL(IK)='RECOM   '
 
             S2 = FZ**2 * (2.0D0 * NZA(IK)**2
     &            - KSIA(IK)) / NZA(IK)**3
             S3 = 126.0D0 * FZ**EXP3 / IH**EXP1
 
 
             LRPOW=.FALSE.
             DO K =1,NUMZ
               IF ( IZIA(K).EQ.(IZRA(IK)-1) ) THEN
                  IRPOW=K
                  LRPOW=.TRUE.
               ENDIF
             END DO
 
             IF (LRPOW) THEN
                IF (CCIOA(IRPOW).EQ.'CIO#A') THEN
                   Y1 = PIOA(IRPOW,1) / TE
                ELSE
                   Y1 = EIONA(IRPOW,1) / TE
                ENDIF
             ELSE
                WRITE(I4UNIT(-1),1000) IZRA(IK)-1
                Y1=0.0D0
             ENDIF
 
C----------------------------------
C  SET LIMITS OF N FOR N-SHELL AND
C  EVALUATE THE PARTIAL SUMS
C  TAKE A MAXIMIUM OF (KNVAL-1) SHELLS
C----------------------------------
 
             JL = NZA(IK) + 1.01D0
 
             DO J = 1,NUMNE
               FNE = 10.0D0**FNEL(J)
               RTEMP  = S3 * TE**EXP1 / FNE**EXP2 + 1.000001D0
               JUP(J) = INT(RTEMP)
             END DO
 
             S4 = IH * FZ**2 / TE
             S5 = 2.0D0 * FZ**4 * (IH / TE)**1.5D0
 
             KJJ = 1
             DO JJ = JL,(JL + KNVAL-1)
               Y2 = S4 / JJ**2
               S5P(KJJ) = S5 / JJ**3 * EEI(Y2)
               call d8intg( ndedge  ,  ndeng   ,
     &                      iedge   ,  ieng    , 
     &                      edge    ,  energy  ,  fraction ,
     &                      te      ,  y2      ,  ps5   
     &                    )
               PS5P(KJJ) = S5 / JJ**3 * PS5
               PS5PNFL(KJJ) = S5 / JJ**3
 
               KJJ = KJJ + 1
             END DO
 
             call d8intg( ndedge  ,  ndeng   ,
     &                    iedge   ,  ieng    , 
     &                    edge    ,  energy  ,  fraction ,
     &                    te      ,  y1      ,  ps1   
     &                  )
     
             PS1B = SQRT(IH / TE) * S2 * Y1 * PS1
             PS1BNFL = SQRT(IH / TE) * S2 * Y1
 
             S1B = SQRT(IH / TE) * S2 * Y1 * EEI(Y1)
 
             DO J = 1,NUMNE
               S1 = S1B
               PS1 = PS1B
               PS1NFL = PS1BNFL
               JJU = JUP(J) - JL + 1
               IF (JJU.GT.KNVAL) JJU = KNVAL
               DO JJ = 1,JJU
                 S1  = S1 + S5P(JJ)
                 PS1 = PS1 + PS5P(JJ)
                 PS1NFL = PS1NFL + PS5PNFL(JJ)
               END DO
               RAL(I,J,IREF) = 2.6D-14 * S1
               POWRRC(I,J,IREF) = 2.6D-14 * TE * PS1
               POWRRCNFL(I,J,IREF) = 2.6D-14 * TE * PS1NFL
	       
	       ralrr(I,J,IREF) = RAL(I,J,IREF)
	       
             END DO
 
 
C-----------------
C  S&D 1992
C-----------------
 
          ELSE
             METRAL=METHOD(1)
             INFRAL(IK)='RECOM   '
 
             DPTEA  = 11605.4D0 * TE
C CHECK THIS
             DPZ1   = dfloat(IZL) + FZ
             N0     = N0RA(IK)
             DPV0   = V0RA(IK)
             DPPHF  = PHFCRA(IK)
             DPEDSP = EDSPRA(IK)
             DPSCAL = SCLERA(IK)
             CALL D8VGOL(DPTEA,DPGA,DPGA0,DPGRST,DPZ1,
     &                  N0,DPV0,DPPHF)
             FAC    = IH * (DPZ1 / DPV0)**2 / TE
             PARTR  = DPSCAL * FAC**DPEDSP * DPGA0 + DPGRST
 
             DO J = 1,NUMNE
               RAL(I,J,IREF)   = PARTR
	       ralrr(I,J,IREF) = RAL(I,J,IREF)
             END DO
 
          ENDIF
 
        END DO

 
C--------------------------------------------------
C  CALCULATION OF DIELECTRONIC RECOMBINATION RATE
C--------------------------------------------------
 
        IF (ALM.EQ.0.0D0) GO TO 160
 
        DO IK = 1,NUMZ
 
          IREF = IZDA(IK) - IZL
          FZ   = dfloat(IZDA(IK))
          NZ   = IZDA(IK)
 
          IF (IREF.EQ.0.OR.IREF.GT.(IZU+1) ) GOTO 151
 
C-----------------
C  VM 1985
C-----------------
 
          IF (CDRCA(IK).EQ.'DRC#A') THEN
             DO IDRC = 1,NDRCA(IK)
 
               DO J = 1,NUMNE
                 RADI6(J,IREF,IDRC) = 0.0D0
               END DO
               Y = DEDA(IK,IDRC) / (IH * (FZ+1.0D0))
 
               IF (MSDA(IK,IDRC).EQ.0) THEN
                  S4 = 1.0D0 + 0.105D0 * Y + 0.015D0 * Y**2
               ELSE
                  S4 = 2.0D0 + 0.42D0 * Y + 0.06D0 * Y**2
               ENDIF
 
               S1 = FDA(IK,IDRC) * SQRT(DEDA(IK,IDRC)) / S4
               S3 = 6.5D-10 * (FZ+1.0D0)**2 * 
     &              SQRT(FZ / (FZ**2 + 13.4D0))
               ZZ = 1.0D0 + 0.015D0 * FZ**3 / (FZ+1.0D0)**2
               Y = DEDA(IK,IDRC) / (TE * ZZ)
               S4 = 0.0D0
               IF (Y.LT.100.0D0) S4 = EXP(-Y)
 
               DO J = 1,NUMNE
                 FNE = 10.0D0**FNEL(J)
                 ZZ = 1.51D+17 / FNE * FZ**6 * SQRT(TE)
                 ZZ = ZZ**(1.0D0 / 7.0D0)
                 IF (NNDA(IK,IDRC).EQ.0) THEN
                    S6 = 1.0D0 / (1.0D0 + 200.0D0 / ZZ)
                 ELSE
                    S6 = 1.0D0 / 
     &                   (1.0D0 + 666.7D0 / ((FZ+1.0D0) * ZZ)**2)
                 ENDIF
                 RADI6(J,IREF,IDRC) = RADI6(J,IREF,IDRC) + ALM
     &                              * S3 / SQRT(TE**3) * S1 * S6 * S4
                 RAL(I,J,IREF)   = RAL(I,J,IREF) + RADI6(J,IREF,IDRC)
                 raldr(I,J,IREF) = raldr(I,J,IREF) + RADI6(J,IREF,IDRC)
               END DO
 
             END DO
 
C-----------------
C  S&D 1992
C-----------------
 
          ELSE
             DO IDRC = 1,NDRCA(IK)
 
               DO J = 1,NUMNE
                 RADI6(J,IREF,IDRC) = 0.0D0
               END DO
 
               DO J = 1,NUMNE
                 FNE = 10.0D0**FNEL(J)
                 TEK = 11605.0D0 * TE
                 
C Wilson's cutoff - do not use for now.
                 ng0 = (4.3D17*FZ**6* SQRT(TE)/ FNE)**(0.1666666D0)
                 
                 ZZ = 1.51D+17 / FNE * FZ**6 * SQRT(TE)
                 ZZ = ZZ**(1.0D0 / 7.0D0)
                 NG = INT(ZZ)
                 
                 CALL D8GPCA( TEK                , NZ                 ,
     &                        ITYPDA(IK,IDRC)    , N0DA(IK,IDRC)      ,
     &                        V0DA(IK,IDRC)      , EPSIJA(IK,IDRC)    ,
     &                        FIJA(IK,IDRC)      , EDSPDA(IK,IDRC)    ,
     &                        SCLEDA(IK,IDRC)    , PHFCDA(IK,IDRC)    ,
     &                        CRFCDA(IK,IDRC)    , NCUTA(IK,IDRC)     ,
     &                        NG                 , ADIELP
     &                      )
 
                 RADI6(J,IREF,IDRC) = RADI6(J,IREF,IDRC) + ADIELP
                 RAL(I,J,IREF) = RAL(I,J,IREF) + RADI6(J,IREF,IDRC)
                 raldr(I,J,IREF) = raldr(I,J,IREF) + RADI6(J,IREF,IDRC)

               END DO
 
             END DO
 
          ENDIF
 
  151   CONTINUE
 
        END DO
 
  160   CONTINUE

 
C----------------------------------------------------
C  COMPLETE RAD. + DIEL. RECOM. CALC. BY TAKING LOGS
C----------------------------------------------------
 
        DO J = 1,NUMNE
          DO IK = 1,NUMZ
            
	    FSA = MAX(RAL(I,J,IK),1.0D-74)
            FSA = LOG10(FSA)
            RAL(I,J,IK) = FSA
          
	    FSA = MAX(ralrr(I,J,IK),1.0D-74)
            FSA = LOG10(FSA)
            ralrr(I,J,IK) = FSA
	    
	    FSA = MAX(raldr(I,J,IK),1.0D-74)
            FSA = LOG10(FSA)
            raldr(I,J,IK) = FSA
	  
	  END DO
        END DO
 
 
C--------------------------------------
C  CALCULATION OF CHARGE EXCHANGE RATE
C--------------------------------------
 
C------------------------------------------------------
C  EVALUATE ON ELECTRON TEMPERATURE GRID
C  USE MEAN HYDROGEN MASS TO OBTAIN RATE COEFFICIENT
C  GREENLAND (AERE (R)-1128, 1984) FOR C, O, AND N,
C  ELSE CAROLAN ET. AL., PLASMA PHYSICS 25, P.1065 (1983)
C
C  * D8CXSC RETURNS CROSS-SECTION IN CM**2
C------------------------------------------------------
 
        METCAL=METHOD(1)
 
        CALL D8CROS(KZDIM,CXC,TE,FUELMAS,IZ0)
        VAV = 1.3892D6 * SQRT(TE / FUELMAS)
 
        DO IK = 1,NUMZ
 
          NZ = IZRA(IK)
 
          INFCAL(IK) = STRG3
 
          FCA = VAV * CXC(NZ)
          FCA = MAX(FCA,1.0D-74)
          FCA = LOG10(FCA)
 
          DO J = 1,NUMNE
            CAL(I,J,IK) = FCA
          END DO
 
        END DO
 
 
C--------------------------------------------------------------------
C  BEGIN CALCULATION OF RADIATED POWER
C      *   LOW ENERGY CUTOFF AT EGR OPERATES ON TOTAL LINE POWER AND
C          RECOMBINATION/BREMSSTRAHLUNG POWER
C      *   CGS UNITS
C--------------------------------------------------------------------
 
 
C----------------------------------
C  CALCULATION OF TOTAL LINE POWER
C----------------------------------
 
        S3 = 2.53D-24 / SQRT(TE)
        DO IK = 1,NUMZ
 
          IREF = IZTA(IK) - IZL
          Z1   = dfloat(IZTA(IK)) + 1.0D0
 
          PT    = 0.0D0
          PTNFL = 0.0D0
 
C------------------
C  VM 1985
C------------------
 
          IF (CPLTA(IK).EQ.'PLT#A') THEN
             METPLT      = METHOD(1)
             METPLTN     = METHOD(1)
             INFPLT(IK)  = 'PLT     '
             INFPLTN(IK) = 'PLT     '
 
             DO IPLT = 1,NPLTA(IK)
               S1 = 0.0D0
               IF (IK.LE.NUMZ) THEN
                  TMENERGY = ABS(DEPTA(IK,IPLT))
                  call d8tran(ndeng    , ndedge  , 
     &                        ieng     , iedge   , 
     &                        edge     , energy  , fraction , 
     &                        tmenergy , frac
     &                        )
                  S1 = FPTA(IK,IPLT) * GPTA(IK,IPLT) * FRAC *
     &                 EXP(-DEPTA(IK,IPLT) / TE)
                  S1NFL = S1 / FRAC
               ENDIF
               PT = PT + S1 * S3
               PTNFL = PTNFL + S1NFL * S3
             END DO
C-----------------
C  S&D 1992
C-----------------
 
          ELSE
             METPLT      = METHOD(2)
             METPLTN     = METHOD(2)
             INFPLT(IK)  = 'PLT     '
             INFPLTN(IK) = 'PLT     '

             DO IPLT = 1,NPLTA(IK)
               S1 = 0.0D0
               IF (IK.LE.NUMZ) THEN
                  TMENERGY = ABS(DEPTA(IK,IPLT))
                  call d8tran(ndeng    , ndedge  , 
     &                        ieng     , iedge   , 
     &                        edge     , energy  , fraction , 
     &                        tmenergy , frac
     &                        )
                  ATE = DEPTA(IK,IPLT) / TE
                  CALL D7PYVR(ATE,Z1,PY)
                  S1 = FPTA(IK,IPLT) * PY * SPYLTA(IK,IPLT) * FRAC *
     &                 EXP(-DEPTA(IK,IPLT) / TE)
                  S1NFL = S1 / FRAC
               ENDIF
               PT    = PT + S1 * S3
               PTNFL = PTNFL + S1NFL * S3
             END DO
 
          ENDIF
 
          PT = MAX(PT,1.0D-74)
          PT = LOG10(PT)
          PTNFL = MAX(PTNFL,1.0D-74)
          PTNFL = LOG10(PTNFL)
 
          DO J = 1,NUMNE
            IF (IREF.EQ.0) THEN
               PLTL0(I,J) = PT
               PLTL0NFL(I,J) = PTNFL
            ELSE
               PLTL(I,J,IREF) = PT
               PLTLNFL(I,J,IREF) = PTNFL
            ENDIF
          END DO
 
        END DO

 
C-------------------------------------
C  CALCULATION OF SPECIFIC LINE POWER
C-------------------------------------
 
        S3 = 2.53D-24 / SQRT(TE)
        DO IK = 1,NUMZ
 
          IREF = IZSA(IK) - IZL
          Z1   = dfloat(IZSA(IK)) + 1.0D0
 
 
          PS = 0.0D0
 
C------------------
C  VM 1985
C------------------
 
          IF (CPLSA(IK).EQ.'PLS#A') THEN
             METPLS     = METHOD(1)
             INFPLS(IK) = INFO(IK)
 
             DO IPLS = 1,NPLSA(IK)
               S1 = 0.0D0
               IF (IK.LE.NUMZ) THEN
                  S1 = FPSA(IK,IPLS) * GPSA(IK,IPLS ) *
     &                 EXP(-DEPSA(IK,IPLS) / TE)
               ENDIF
               PS = PS + S3 * S1
             END DO
C------------------
C  S&D 1992
C------------------
 
          ELSE
             METPLS     = METHOD(2)
             INFPLS(IK) = INFO(IK)
 
             DO IPLS = 1,NPLSA(IK)
               S1 = 0.0D0
               IF (IK.LE.NUMZ) THEN
                  ATE = DEPSA(IK,IPLS) / TE
                  CALL D7PYVR(ATE,Z1,PY)
                  S1 = FPSA(IK,IPLS) * PY * SPYLSA(IK,IPLS) *
     &                 EXP(-DEPSA(IK,IPLS) / TE)
               ENDIF
               PS = PS + S3 * S1
             END DO
 
 
          ENDIF
 
          PS = MAX(PS,1.0D-74)
          PS = LOG10(PS)
 
          DO J = 1,NUMNE
            IF (IREF.EQ.0) THEN
               PLSL0(I,J) = PS
            ELSE
               PLSL(I,J,IREF) = PS
            ENDIF
          END DO
 
        END DO

 
C----------------------------------------------------------------
C  CALCULATION OF RADIATIVE AND DIELECTRONIC RECOMBINATION POWER
C  NOTE BREMMSTRAHLUNG DEPENDENCE ON THE FILTER IS ON TE ONLY
C----------------------------------------------------------------
 
        call d8intg( ndedge  ,  ndeng   ,
     &               iedge   ,  ieng    , 
     &               edge    ,  energy  ,  fraction ,
     &               te      ,  0.0D0   ,  ps4  
     &             )
 
        DO J = 1,NUMNE
 
 
C RAD RECOM - IREF INDEXING DONE IN RAD REC COEFF CALCULATION
 
          DO IK = 1,NUMZ
 
 
C------------------
C  VM 1985
C------------------
 
            IF (CRRCA(IK).EQ.'RRC#A') THEN
               
               METPRB      = METHOD(1)
               METPRBN     = METHOD(1)
               INFPRB(IK)  = 'PRB CALC'
               INFPRBN(IK) = 'PRB CALC'
 
               POWRRC(I,J,IK)    = 1.6021D-19 * POWRRC(I,J,IK)
               POWRRCNFL(I,J,IK) = 1.6021D-19 * POWRRCNFL(I,J,IK)
  
C-----------------
C  S&D 1992
C-----------------
 
            ELSE
            
               METPRB      = METHOD(2)
               METPRBN     = METHOD(2)
               INFPRB(IK)  = 'PRB CALC'
               INFPRBN(IK) = 'PRB CALC'
 
               write(i4unit(-1),1010)'Type B prb is not available'
 
C **************** INSERT CASE (B) OPTION HERE **************
 
            ENDIF
 
          END DO
 
 
 
C DIELECTRONIC RECOM
 
          DO IK = 1,NUMZ
 
            IREF = IZDA(IK) - IZL
            IF (IREF.GT.0.AND.IREF.LE.(IZU+1) ) THEN
 
C-----------------------------------
C  NO SEPERATE FORMS FOR DIEL POWER
C-----------------------------------
 
                LDPOW=.FALSE.
                K=0
                DO K =1,NUMZ
                  IF ( IZIA(K).EQ.(IZDA(IK)-1) ) THEN
                     IDPOW=K
                     LDPOW=.TRUE.
                  ENDIF
                END DO
 
                IF (LDPOW) THEN
                   IF (CCIOA(IDPOW).EQ.'CIO#A') THEN
                      Y1 = PIOA(IDPOW,1)
                   ELSE
                      Y1 = EIONA(IDPOW,NCIOSA(IK)) * IH
                   ENDIF
                ELSE
                   WRITE(I4UNIT(-1),1000) IZDA(IK)-1
                   Y1=0.0D0
                ENDIF
 
                SUM1    = 0.0D0
                SUM1NFL = 0.0D0
                DO IDRC = 1,NDRCA(IK)
                  
                  if (CDRCA(IK).EQ.'DRC#A') then
                     TMENERGY = ABS(DEDA(IK,IDRC))
                  else
                     TMENERGY = 13.60569D0 * ABS(EPSIJA(IK,IDRC) * 
     &                          (IZDA(IK)+1)**2)
                  endif
                  
                  call d8tran(ndeng    , ndedge  , 
     &                        ieng     , iedge   , 
     &                        edge     , energy  , fraction , 
     &                        tmenergy , frac1
     &                        )
                  SUM1 = SUM1 + ABS(tmenergy) * FRAC1 *
     &                   RADI6(J,IREF,IDRC)
                  SUM1NFL = SUM1NFL + ABS(tmenergy) *
     &                      RADI6(J,IREF,IDRC)
                  
                  TMENERGY = ABS(Y1)
                  call d8tran(ndeng    , ndedge  , 
     &                        ieng     , iedge   , 
     &                        edge     , energy  , fraction , 
     &                        tmenergy , frac2
     &                        )
                  SUM1    = SUM1 + ABS(Y1) * FRAC2 *
     &                      RADI6(J,IREF,IDRC)
                  SUM1NFL = SUM1NFL + ABS(Y1) * RADI6(J,IREF,IDRC)
 
                END DO
 
                POWDRC(I,J,IREF)    = 1.6021D-19 * SUM1
                POWDRCNFL(I,J,IREF) = 1.6021D-19 * SUM1NFL
 
            ENDIF
 
 
          END DO
 
 
C-----------------------------------------------------
C  CALCULATION OF CHARGE EXCHANGE RECOMBINATION POWER
C-----------------------------------------------------
 
          DO IK = 1,NUMZ
 
            S8=0.0D0
            S8NFL=0.0D0
 
C-------------------
C  NO SEPERATE FORM
C-------------------
 
 
            LCPOW=.FALSE.
            DO K =1,NUMZ
              IF ( IZIA(K).EQ.(IZRA(IK)-1) ) THEN
                 ICPOW=K
                 LCPOW=.TRUE.
              ENDIF
            END DO
 
            IF (LCPOW) THEN
               IF (CCIOA(ICPOW).EQ.'CIO#A') THEN
                  Y1 = PIOA(ICPOW,1)
               ELSE
                  Y1 = EIONA(ICPOW,NCIOSA(IK)) * IH
               ENDIF
            ELSE
               WRITE(I4UNIT(-1),1000) IZRA(IK)-1
               Y1=0.0D0
            ENDIF
 
            METPRC      = METHOD(1)
            METPRCN     = METHOD(1)
            INFPRC(IK)  = STRG3
            INFPRCN(IK) = STRG3
 
            TMENERGY = ABS(Y1)
            call d8tran(ndeng    , ndedge  , 
     &                  ieng     , iedge   , 
     &                  edge     , energy  , fraction , 
     &                  tmenergy , frac
     &                  )
            S8 = 1.6021D-19 * ABS(Y1) * FRAC * 10.0D0**CAL(I,J,IK)
            S8NFL=S8 / FRAC
 
 
            PC = S8
            PC = MAX(PC,1.0D-74)
            PC = LOG10(PC)
            PRCL(I,J,IK) = PC
 
            PCNFL = S8NFL
            PCNFL = MAX(PCNFL,1.0D-74)
            PCNFL = LOG10(PCNFL)
            PRCLNFL(I,J,IK) = PCNFL
 
          END DO
 
C--------------------------------------
C  CALCULATION OF BREMSSTRAHLUNG POWER
C--------------------------------------
 
          DO IK=1,NUMZ
 
            FZ = dfloat(IZRA(IK))
            
            PS6 = NGFFMH(FZ*FZ*IH/TE)
 
            POWBRS(I,J,IK)    = 1.54D-32 * (FZ**2) * SQRT(TE) * PS4 *PS6
            POWBRSNFL(I,J,IK) = 1.54D-32 * (FZ**2) * SQRT(TE) * PS6
 
          END DO
 
C--------------------------------------------------------------------
C  TOTAL POWER DUE TO BREMSSTRAHLUNG ,RECOMBINATION AND DIELECTRONIC
C  RECOMBINATION  PB = (S4 + S5)
C--------------------------------------------------------------------
          DO IK=1,NUMZ
 
            PB    = POWRRC(I,J,IK) + POWDRC(I,J,IK) + POWBRS(I,J,IK)
            PBNFL = POWRRCNFL(I,J,IK) + POWDRCNFL(I,J,IK) +
     &              POWBRSNFL(I,J,IK)
 
            PB = MAX(PB,1.0D-74)
            PB = LOG10(PB)
            PRBL(I,J,IK) = PB
 
            PBNFL = MAX(PBNFL,1.0D-74)
            PBNFL = LOG10(PBNFL)
            PRBLNFL(I,J,IK) = PBNFL
          
            fsa = max(powrrc(i,j,ik),1.0D-74)
            fsa = log10(fsa)
            prbrr(i,j,ik) = fsa
	  
            fsa = max(powdrc(i,j,ik),1.0D-74)
            fsa = log10(fsa)
            prbdr(i,j,ik) = fsa

            fsa = max(powbrs(i,j,ik),1.0D-74)
            fsa = log10(fsa)
            prbbr(i,j,ik) = fsa


            fsa = max(powrrcnfl(i,j,ik),1.0D-74)
            fsa = log10(fsa)
            prbrrnfl(i,j,ik) = fsa
	  
            fsa = max(powdrcnfl(i,j,ik),1.0D-74)
            fsa = log10(fsa)
            prbdrnfl(i,j,ik) = fsa

            fsa = max(powbrsnfl(i,j,ik),1.0D-74)
            fsa = log10(fsa)
            prbbrnfl(i,j,ik) = fsa

	  END DO
 
 
        END DO

 
C-----------------------------------------------------------------------
C     END OF TEMPERATURE LOOP
C-----------------------------------------------------------------------

      END DO

C-----------------------------------------------------------------------
 1000 FORMAT(/1X,30('*'),' D8EVAL  WARNING ',30('*')/
     &        2X,'RECOMBINED ION IONISATION POTENTIAL, Z = ',I2/
     &        2X,'NOT AVAILABLE - SET TO ZERO'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 1010 FORMAT(/1X,30('*'),' D8EVAL  WARNING ',30('*')/,A,
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C-----------------------------------------------------------------------

      END
