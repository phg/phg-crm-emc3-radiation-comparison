C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas408/d8vgol.for,v 1.3 2007/05/18 08:52:58 allan Exp $ Date $Date: 2007/05/18 08:52:58 $
      subroutine d8vgol(tea, ga, ga0, garest, z1, n0, v0, phfrac)
 
      implicit none
 
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D8VGOL *********************
C
C  PURPOSE:    Routine to evaluate total radiative recombination rate 
C              coefficients at zero density using the Von Goeler type
C              formula with modified capture to the lowest accessible
C              principal quantum shell.
C
C              PHFRAC gives the proportion of the lowest level capture
C              allowed based on the available phase space of occupied
C              shells arguments.
C
C  CALLING PROGRAM: ADAS408
C
C
C  INPUT:  (R*8)  TEA         = Electron temperatures (k)
C          (R*8)  z1          = Recombining ion charge
C          (I*4)  n0          = Lowest accessible n-shell by recombination                        
C          (R*8)  v0          = Effective principal quantum number of                             
C                               lowest accessible shell                                           
C          (R*8)  phfrac      = Phase space occupation factor for lowest 
C                               accessible shell         
C               
C  
C  OUTPUT: (R*8)  ga          = Total radiative recombination 
C                               coefficient (cm**3 sec-1)
C          (R*8)  ga0         = Ground shell  recombination coefficient
C          (R*8)  garest      = Recombination coefficient to all shells 
C                               excluding the ground shell.
C
C
C  ROUTINES:
C          ROUTINE   SOURCE     DESCRIPTION
C          ----------------------------------------------------------
C
C
C  HISTORY:
C 
C  H.P. Summers, JET       24 June 1987
C  M. O'Mullane            10 Aug  1992 - modified for one temperature
C
C
C-----------------------------------------------------------------------
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-04-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C VERSION: 1.2				DATE: 16-02-2004
C MODIFIED: Martin O'Mullane
C            - Convert to implicit none.
C
C VERSION: 1.3				DATE: 17-05-2007
C MODIFIED: Allan Whiteford
C            - Updated comments as part of subroutine documentation
C              procedure.
C
C-----------------------------------------------------------------------
      INTEGER    inrep
C-----------------------------------------------------------------------
      PARAMETER (inrep = 21 )
C-----------------------------------------------------------------------
      INTEGER   i0   , i1    , n    , n0     , n1
C-----------------------------------------------------------------------
      REAL*8    alfn , alfn1 , ate  , ate0   , aten   , aten1 , b ,   
     &          eei  , ga    , ga0  , garest , phfrac , tea   , v ,   
     &          v0   ,  v1   , z1
C-----------------------------------------------------------------------
      INTEGER   nrep(30)  
C-----------------------------------------------------------------------
      data nrep /1,2,3,4,5,6,7,8,9,10,12,15,20,30,40,50,60,70,100,150,
     &           200,250,300,400,500,600,700,800,900,1000/
C-----------------------------------------------------------------------
 
      ga     = 0.0D0
      ga0    = 0.0D0
      garest = 0.0D0
      n1     = n0+1
 
      i0 = 0
   41 i0 = i0+1
      if (n1.GE.nrep(i0)) go to 41
      if (i0.LT.10) i0 = 10
 
      do n = n1,nrep(i0)
        v      = n
        ate    = 1.5789D5 * z1 * z1 / tea
        aten   = ate / (v * v)
        alfn   = 2.6D-14 * z1 * sqrt(ate) * 2.0D0 *
     &               aten * eei(aten) / v
        garest = garest + alfn
      end do
 
      do i1 = i0+1, inrep
        n1     = nrep(i1)
        v1     = n1
        ate    = 1.5789D5 * z1 * z1 / tea
        aten1  = ate / (v1 * v1)
        alfn1  = 2.6D-14 * z1 * sqrt(ate) * 2.0D0 *
     &           aten1 * eei(aten1) / v1
        b      = -log(alfn / alfn1)/log(v / v1)
        garest = garest + ((2.0D0 *v1 + 1.0D0 - b) *
     &           alfn1-(2.0D0 * v + 1.0D0 - b) * alfn) /
     &           (2.0D0 - 2.0D0 * b)
        n      = n1
        v      = v1
        alfn   = alfn1
      end do
 
      garest = garest + 0.5D0 * (nrep(inrep) - 1) * alfn
      ate    = 1.5789D5 * z1 * z1 / tea
      ate0   = ate / (v0 * v0)
      ga0    = 2.6D-14 * z1 * sqrt(ate) * 2.0D0 *
     &         phfrac * ate0 * eei(ate0) / v0
      ga     = ga0 + garest
 
C-----------------------------------------------------------------------
      return
      
      end
