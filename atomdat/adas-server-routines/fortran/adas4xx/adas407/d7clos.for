C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7clos.for,v 1.2 2004/07/06 13:21:28 whitefor Exp $ Date $Date: 2004/07/06 13:21:28 $

      SUBROUTINE D7CLOS( IUNT11 , IUNT12 ,
     &                   OPEN11 , OPEN12 ,
     &                   IFCNT  , NDFILE , FILESL , FILESU ,
     &	  		 DATE
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7CLOS *********************
C
C  PURPOSE: TO WRITE TERMINATOR SEQUENCES AND CLOSE FILES ON
C           UNIT11 (MAINCL FILE) AND UNIT12 (ATOMPARS FILE)
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNT11   = UNIT NUMBER FOR MAINCL OUTPUT FILE
C  INPUT : (I*4)   IUNT12   = UNIT NUMBER FOR ATOMPARS OUTPUT FILE
C  INPUT : (L*4)   OPEN11   = .TRUE.  => MAINCL PASSING FILE OPENED
C                             .FALSE. => MAINCL PASSING FILE NOT OPENED
C  INPUT : (L*4)   OPEN12   = .TRUE.  => ATMPRS  PASSING FILE OPENED
C                             .FALSE. => ATMPRS  PASSING FILE NOT OPENED
C  INPUT : (I*4)   IFCNT    = NUMBER OF RECOMBINED/RECOMBINING FILE
C                             PAIRS USED
C  INPUT : (I*4)   NDFILE   = MAX. NUMBER OF FILES FOR DIMENSIONING FILESL
C			      AND FILESU. THIS IS NECESSARY ON A DEC, BUT
C			      NOT ON THE IBM AT JET (???)
C  INPUT : (C*80)  FILESL() = RECOMBINED FILE
C  INPUT : (C*80)  FILESU() = RECOMBINING FILE
C
C          (I*4)   I        = GENERAL INTEGER
C          (C*6)   USERID   = USER IDENTIFIER
C          (C*8)   DATE     = CURRENT DATE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    12/07/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    INCREASED FILE NAME LENGTHS TO 80
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER      IUNT11      , IUNT12
      INTEGER      IFCNT       , I      , NDFILE , I4UNIT
C-----------------------------------------------------------------------
      CHARACTER    FILESL(NDFILE)*80 , FILESU(NDFILE)*80
      CHARACTER    USERID*10    , DATE*8
C-----------------------------------------------------------------------
      LOGICAL      OPEN11      , OPEN12
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------

      CALL XXGUID(USERID)

      IF(OPEN11) THEN
          WRITE(IUNT11,1000) -1 , 0
          CLOSE(11)
      ENDIF
C
      IF(OPEN12) THEN
          WRITE(IUNT12,1001)USERID,DATE
          DO 10 I = 1,IFCNT
            WRITE(IUNT12,1002)I,FILESL(I)
   10     CONTINUE
C
          DO 20 I = 1,IFCNT
            WRITE(IUNT12,1003)I,FILESU(I)
   20     CONTINUE
C
          WRITE(IUNT12,1004)
          CLOSE(12)
C
      ENDIF
C
      RETURN
C
C-----------------------------------------------------------------------
C
 1000  FORMAT(' &FILINFO  NUCCHG=',I2,', NPARNT=',I1,
     &         ', SEQ=','''??''',', XRMEMB=','''????????''',',  &END')
 1001  FORMAT('C',79('-')/'C'/
     &        'C  INFORMATION'/
     &        'C  -----------'/
     &        'C'/
     &        'C  PROCESSING CODE:  ADAS407'/
     &        'C  USER IDENTIFIER:  ',1A10/
     &        'C  DATE           :  ',1A8/
     &        'C'
     &       )
 1002  FORMAT('C  RECOMBINED ION DATA SETS : ',
     &       (/'C',30X,I2,2X,1A80)
     &       )
 1003  FORMAT('C  RECOMBINING ION DATA SETS: ',
     &       (/'C',30X,I2,2X,1A80)
     &       )
 1004  FORMAT('C'/'C',79('-'))
C
C-----------------------------------------------------------------------
C
      END
