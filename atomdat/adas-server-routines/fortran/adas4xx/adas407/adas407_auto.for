       PROGRAM ADAS407_AUTO


       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS407_AUTO ******************
C
C  VERSION:  1.0
C
C  PURPOSE:  To calculate approximate form parameters for ionisation
C            recombination, total line power and specific line power
C            from specific ion files and prepare an atomic parameters
C            file of type adf03. This branch works automatically 
C            without user intervention.
C
C
C  DATA:     The source data is from specific ion files prepared with
C            eissner configuration specification. These are stored
C            datasets by element nuclear charge as follows:-
C
C              /home/adas/adas/adf04/copmm#<NUCCHG>/ls#<ELEM>#<ION>.dat
C
C            where, <NUCCHG> = one or two digit element nuclear charge
C                   <ELEM>   = element string
C                   <ION>    = ion charge
C
C  COMMENT:  To avoid diverging from the routines used in the standard
C            ADAS407 program some quantities are calculated which are
C            not strictly necessary (see D7EXPS).
C
C  PROGRAM:
C          (I*4)  NDLEV   = PARAMETER = MAX. NUMBER OF LEVELS ALLOWED
C          (I*4)  NDTRN   = PARAMETER = MAX. NO. OF TRANSITIONS ALLOWED
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF TEMPERATURES ALLOWED
C          (I*4)  NDMET   = PARAMETER = MAX. NO. OF METASTABLES ALLOWED
C          (I*4)  NDOSC   = PARAMETER = MAX. NO. OF ALLOWED TRANSITION
C                                       FOR EACH METASTABLE
C          (I*4)  NDBNDL  = PARAMETER = MAX. NO. OF TRANSITION
C                                       BUNDLING GROUPS
C          (I*4)  NDCONF  = PARAMETER = MAXIMUM NUMBER OF CONFIGURATIONS
C          (I*4)  NDTHET  = PARAMETER = MAXIMUM NUMBER OF TEMPERATURES
C                                       FOR MAINCL FILE
C          (I*4)  NDRHO   = PARAMETER = MAXIMUM NUMBER OF DENSITIES
C                                       FOR MAINCL FILE
C          (I*4)  NDREP   = PARAMETER = MAXIMUM NUMBER OF REPRESENTATIVE
C                                       LEVELS FOR MAINCL FILE
C          (I*4)  NDORB   = PARAMETER = MAXIMUM NUMBER OF ORBITALS
C          (I*4)  NDQDN   = MAX. NUMBER OF N-SHELLS FOR QUANTUM DEFECTS
C
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT09  = PARAMETER = INPUT UNIT FOR FIRST ADF04 DATA SET
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR SECOND ADF04 DATA SET
C          (I*4)  IUNT12  = PARAMETER = OUTPUT UNIT FOR ATOMPARS PASSING FILE.
C
C          (I*4)  L1      = PARAMETER = 1
C
C          (C*1)  PRGTYP  = PARAMETER = PROGRAM TYPE = 'M' (METPOP)
C
C          (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTP   = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C          (I*4)  ICNTR   = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C          (I*4)  ICNTH   = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C          (I*4)  IFOUT   = 1 => INPUT TEMPERATURES IN KELVIN
C                         = 2 => INPUT TEMPERATURES IN EV
C                         = 3 => INPUT TEMPERATURES IN REDUCED FORM
C          (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C                              (1ST COPASE FILE)
C          (I*4)  IPL     = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C                              (2ND COPASE FILE)
C          (I*4)  ITRAN   = INPUT DATA FILE: NUMBER OF TRANSITIONS
C          (I*4)  ITSEL   = INDEX OF TEMPERATURE SELECTED FOR POWER
C                           MATCHING
C          (I*4)  IZ      = RECOMBINED ION CHARGE
C                              (1ST COPASE FILE)
C          (I*4)  IZ0     = NUCLEAR CHARGE
C                              (1ST COPASE FILE)
C          (I*4)  IZ1     = RECOMBINING ION CHARGE
C                              (2ND COPASE FILE)
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C          (I*4)  IZ2     = RECOMBINED ION CHARGE
C                              (2ND COPASE FILE)
C          (I*4)  IZ02    = NUCLEAR CHARGE
C                              (2ND COPASE FILE)
C          (I*4)  IZ12    = RECOMBINING ION CHARGE
C                              (2ND COPASE FILE)
C                           (NOTE: IZ12 SHOULD EQUAL IZ2+1)
C          (I*4)  ISG     = MULTIPLICITY OF GROUND STATE OF RECOMBINED ION
C          (I*4)  I4UNIT  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  MAXLEV  = HIGHEST INDEX LEVEL IN READ TRANSITIONS
C          (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C          (I*4)  NPMET    = NUMBER OF PARENT METASTABLES (1<=NMET<=5)
C          (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C
C          (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C                              (1ST COPASE FILE)
C          (R*8)  BWNO2   = IONISATION POTENTIAL (CM-1)
C                              (2ND COPASE FILE)
C	   (R*8)  Z1	  = RECOMBINING ION CHARGE
C
C          (L*4)  LWR12   = .TRUE.  => UNIT 12 HAS BEEN WRITTEN TO
C                         = .FALSE. => UNIT 12 HAS NOT BEEN WRITTEN TO.
C
C          (C*3)  TITLED  = ELEMENT SYMBOL
C                              (1ST COPASE FILE)
C          (C*3)  TITLED2 = ELEMENT SYMBOL (1ST COPASE FILE).
C                              (2ND COPASE FILE)
C          (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C          (C*80) DSNMBN  = OUTPUT MAINBN DATA SET NAME (SEQUENTIAL)
C          (C*80) DSNPRS  = OUTPUT ATOMPARS FILE NAME (SEQUENTIAL)
C          (C*80) DSNIC1  = FIRST INPUT COPASE DATA SET NAME (MVS DSN)
C          (C*80) DSNIC2  = SECOND INPUT COPASE DATA SET NAME (MVS DSN)
C
C          (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C                              (1ST COPASE FILE)
C          (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C                              (1ST COPASE FILE)
C          (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                              (1ST COPASE FILE)
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IA2()   = ENERGY LEVEL INDEX NUMBER
C                              (2ND COPASE FILE)
C          (I*4)  IPLA()  = QUANTUM NUMBER (L) FOR LEVEL 'IA2()'
C                              (2ND COPASE FILE)
C          (I*4)  IPSA()  = MULTIPLICITY FOR LEVEL 'IA2()'
C                              (2ND COPASE FILE)
C                           NOTE: (IPSA-1)/2 = QUANTUM NUMBER (S)
C          (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  IPMETR()= INDEX OF PARENT METASTABLES IN LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C          (I*4)  I1A()   = TRANSITION:
C                            LOWER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            NOT USED                 (CASE 'H' & 'R')
C          (I*4)  I2A()   = TRANSITION:
C                            UPPER ENERGY LEVEL INDEX (CASE ' ' & 'P')
C                            CAPTURING    LEVEL INDEX (CASE 'H' & 'R')
C          (I*4)  IETRN() = ELECTRON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT ELECTRON IMPACT TRANSITIONS.
C          (I*4)  IPTRN() = PROTON IMPACT TRANSITION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT PROTON IMPACT TRANSITIONS.
C          (I*4)  IRTRN() = FREE ELECTRON RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT FREE ELECTRON RECOMBINATIONS.
C          (I*4)  IHTRN() = CHARGE EXCHANGE RECOMBINATION:
C                           INDEX VALUES IN MAIN TRANSITION ARRAYS WHICH
C                           REPRESENT CHARGE EXCHANGE RECOMBINATIONS.
C          (I*4)  IE1A()  = ELECTRON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IE2A()  = ELECTRON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C          (I*4)  IP1A()  = PROTON IMPACT TRANSITION:
C                            LOWER ENERGY LEVEL INDEX
C          (I*4)  IP2A()  = PROTON IMPACT TRANSITION:
C                            UPPER ENERGY LEVEL INDEX
C
C          (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XIA()   = ENERGY RELATIVE TO ION. POT. (RYDBERGS)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  AA()    = ELECTRON IMPACT TRANSITION: A-VALUE (SEC-1)
C          (R*8)  AVAL()  = TRANSITION:
C                            A-VALUE (SEC-1)          (CASE ' ')
C                            NEUTRAL BEAM ENERGY      (CASE 'H')
C                            NOT USED                 (CASE 'P' & 'R')
C          (R*8)  SCOM(,) = TRANSITION:
C                            GAMMA VALUES             (CASE ' ' & 'P')
C                            RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                           1ST DIMENSION - TEMPERATURE 'SCEF()'
C                           2ND DIMENSION - TRANSITION NUMBER
C          (R*8)  SCEF()  = INPUT DATA FILE: ELECTRON TEMPERATURES (K)
C          (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                           DIMENSION: LEVEL INDEX
C          (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C          (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURES (SEE 'SCEF()')
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C          (R*8)  PTOT(,)  = TOTAL ZERO-DENS. RAD. POWER FOR EACH META.
C                            1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                            2ND DIM: METASTABLE INDEX
C          (R*8)  FFMA(,,) = OSC. STRENGTHS FOR ALLLOWED TRANSITIONS
C                            1ST DIM: INCLUDED ALLOWED TRANSITION INDEX
C                            2ND DIM: METASTABLE INDEX
C          (R*8)  WVMA(,,) = WAVELENGTH (A) FOR ALLLOWED TRANSITIONS
C                            1ST DIM: INCLUDED ALLOWED TRANSITION INDEX
C                            2ND DIM: METASTABLE INDEX
C          (R*8)  GBMA(,,) = GBAR FOR ALLLOWED TRANSITIONS
C                            1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                            2ND DIM: INCLUDED ALLOWED TRANSITION INDEX
C                            3RD DIM: METASTABLE INDEX
C          (R*8)  PIFIT(,)  = RADIATED POWER INITIAL FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PFIT(,)   = RADIATED POWER SIMPLE SCALE FIT AT ITSEL
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PAPPRX(,) = RADIATED POWER OPTIMISED FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PNLIA(,)  = INITIAL SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PNLSA(,)  = SIMPLE  SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PNLFA(,)  = SIMPLE  SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  QDORB()   = QUANTUM DEFECTS FOR ORBITALS
C                             1ST DIM: INDEX FOR NL ORBITAL (CF INDX)
C          (L*4)  LQDORB()  = .TRUE.  => SOURCE DATA AVAILABLE FOR QD.
C                           = .FALSE. => SOURCE DATA NOT AVAILABE QD.=0.0
C          (R*8)  QDN()     = QUANTUM DEFECT FOR N-SHELLS.  NON-ZERO ONLY
C                             FOR ADF04 FILES WITH ORBITAL ENERGY DATA
C                             1ST. DIM: N-SHELL (1<=N<=NDQDN)
C
C          (C*1)  TCODE()   = TRANSITION: DATA TYPE POINTER:
C                             ' ' => ELECTRON IMPACT   TRANSITION
C                             'P' => PROTON   IMPACT   TRANSITION
C                             'H' => CHARGE   EXCHANGE RECOMBINATION
C                             'R' => FREE     ELECTRON RECOMBINATION
C          (C*18) CSTRGA()  = NOMENCLATURE/CONFIGURATION FOR LEVEL'IA()'
C          (C*18) CSTRGB()  = AS CSTRGA() BUT ONLY TAKING THE LAST
C                             'ICSTMX' NON-BLANK BYTES.
C          (C*22) STRGA()   = LEVEL DESIGNATIONS
C
C          (L*4)  LSETP()   = .TRUE.  => POWER ANALYSIS COMPLETED FOR
C                                        METASTABLE IMET
C                             .FALSE. => POWER ANALYSIS NOT COMPLETED
C                                        FOR METASTABLE IMET
C          (I*4)  IBNDL     = NUMBER OF TRANSITION BUNDLES
C          (R*8)  DEBNDL(,) = MEAN TRANSITION ENERGY FOR BUNDLE (RYD)
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  FBNDL(,)  = SUMMED OSCILLATOR STRENGTH FOR BUNDLE
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  GBNDL(,)  = MEAN GAUNT (OR P) FACTOR FOR BUNDLE
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C
C          (I*4)  ISPRT()   = RECOMBINING ION (PARENT) SPIN
C                             1ST DIM: PARENT INDEX
C          (I*4)  ISPSYS(,) = RECOMBINED ION SPIN
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C          (I*4)  NSYS()    = NUMBER OF SPIN SYSTEMS FOR RECOMBINED ION
C                             1ST DIM: PARENT INDEX
C          (I*4)  INPAR()   = N QUANTUM NO. SUM FOR ELECTRONS OF PARENT
C                             1ST DIM: PARENT INDEX
C          (I*4)  ILPAR()   = L QUANTUM NO. SUM FOR ELECTRONS OF PARENT
C                             1ST DIM: PARENT INDEX
C          (R*8)  ENPAR()   = RECOMBINING ION (PARENT) ENERGY
C                             1ST DIM: PARENT INDEX
C          (C*2)  TRMPRT()  = RECOMBINING ION METASTABLE (PARENT) TERM
C                             1ST DIM: PARENT INDEX
C          (R*8)  SPNFAC(,) = SPIN WEIGHT FRACT. FOR PARENT/SPIN SYSTEM
C                             1ST DIM: PARENT INDEX
C                             2ND DIM: RECOMBINED ION SPIN SYSTEM INDEX
C          (I*4)  NTHETA    = NUMBER OF TEMPERATURES FOR MAINCL FILE
C          (I*4)  NRHO      = NUMBER OF DENSITIES FOR MAINCL FILE
C          (R*8)  THETA()   = Z-SCALED ELECTRON TEMPS. FOR MAINCL FILE
C          (R*8)  RHO()     = Z-SCALED ELECT. DENSITIES FOR MAINCL FILE
C          (R*8)  RHOP()    = Z-SCALED PROTON DENSITIES FOR MAINCL FILE
C          (I*4)  INREP     = NUMBER OF REPRESENTATIVE N-SHELLS FOR THE
C                             MAINCL FILE
C          (I*4)  NREP()    = REPRESENTATIVE N-SHELLS
C                             1ST DIM: REPRESENTATIVE LEVEL INDEX
C          (I*4)  WNREP()   = LYMAN SERIES DILUTS. FOR REPRESENTATIVE
C                             N-SHELLS
C                            1ST DIM: REPRESENTATIVE LEVEL INDEX
C          (I*4)  NREP()    = REPRESENTAIVE N-SHELLS FOR MAINCL FILE
C          (R*8)  THETA()   = Z-SCALED ELECTRON TEMPS. FOR MAINCL FILE
C          (I*4)  NCUT()    = N-SHELL AUTOIONISATION CUT-OFF FOR PARENT
C                             1ST DIM: PARENT INDEX
C          (I*4)  N0A(,)    = LOWEST ALLOWED N-SHELL
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C          (R*4)  PARMR(,,) = PARAMETERS OF RADIATIVE RECOMBINATION
C                             APPROXIMATE FORMS
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C                             3RD IND: PARMS. 1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C          (I*4)  NCONFG    = NUMBER OF CONFIGURATIONS
C
C          (L*4)  LTADJ     = .TRUE. => ADJUST PARMS FROM SPECIAL TABLES
C                             .FALSE.=> DO NOT ADJUST PARMS FROM TABLES
C          (I*4)  ISPECA()  = SPECIFIC LINE INDEX IN ALLOWED TRANSITIONS
C                             2ND DIM: METASTABLE INDEX
C          (I*4)  NORB      = NUMBER OF ELECTRON ORBITALS IN USE
C          (R*8)  VORB()    = EFFECT. PRINC. QUANT. NO. FOR ORBITAL
C                             1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C          (L*4)  LEICHR()  = .TRUE. => EISSNER ORBITAL USED
C                             .FALSE => EISSNER ORBITAL NOT USED
C          (C*80) CADAS     = LONG TITLE
C          (C*80) SAVFIL    = FILENAME FOR FILE INFO OUTPUT
C          (L*4)  LEISS     = .TRUE. => PARENTS AND METASTABLES FOUND
C                                       TO HAVE EISSNER CONFIG. FORMS
C                             .FALSE => NOT EISSNER CONFIG. FORMS
C          (L*4)  LLINK(,,) = .TRUE.  => LINK EXISTS
C                             .FALSE. => NO LINK EXISTS
C                              1ST DIM: METASTABLE INDEX
C                              2ND DIM: PARENT METASTABLE INDEX
C                              3RD DIM: SPEN SYSTEM INDEX
C          (L*4)  ILINK(,,) = DECIMAL ORBITAL INDEX FOR RECOMBINED
C                             ION ORBITAL DIFFERENCE WITH PARENT
C                              1ST DIM: METASTABLE INDEX
C                              2ND DIM: PARENT METASTABLE INDEX
C                              3RD DIM: SPEN SYSTEM INDEX
C
C          (I*4)   NALCM    = NUMBER OF SPIN DISTINGUISED 
C                             METASTABLES
C          (I*4)   IALCM()  = INDEX OF ENERGY ORDERED SPIN 
C                             DISTINQUISHED METASTABLE
C                             1ST. DIM: METASTABLE INDEX  
C          (I*4)   ISALCM() = SPIN OF ENERGY ORDERED SPIN 
C                             DISTINGUISHED METASTABLE
C                             1ST. DIM: DISTINGUISHED METASTABLE INDEX  
C          (I*4)   NALCP      NUMBER OF SPIN DISTINGUISHED
C                             PARENTS 
C          (I*4)   IALCP()  = INDEX FOR ENERGY ORDERED SPIN 
C                             DISTINGUISHED PARENT
C                             1ST. DIM: PARENT INDEX  
C          (I*4)   ISALCP() = SPIN OF ENERGY ORDERED SPIN 
C                             DISTINGUISHED PARENT
C                             1ST. DIM: DISTINGUISHED PARENT INDEX  
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D7DATA     ADAS      GATHERS RELEVANT DATA FROM FIRST FILE
C          D7DATR     ADAS      GATHERS RELEVANT DATA FROM SECOND FILE
C          D7WR12     ADAS      OUTPUT DATA TO METPOP PASSING FILE
C          D7OUT0     ADAS      OUTPUT ION SPEC. ETC. TO OUTPUT STREAM
C          D7OUT1     ADAS      OUTPUT MAIN RESULTS TO OUTPUT STREAM
C          BXTTYP     ADAS      SORT TRANSITIONS INTO TRAN/RECOMB TYPES
C          XXERYD     ADAS      CONVERTS ENERGIES FROM W.NO. TO RYDBERGS
C          XXTCON     ADAS      CONVERTS FILE ENTERED TEMPS. TO EV.
C          D7PWRF     ADAS      RETURNS EFFECTIVE POERE PARAMETERS
C          D7EXPS     ADAS      CALCULATES IONIS/RECOM PARAMETERS
C          D7GRPS     ADAS      GROUPS ALL WAVELENGTHS INTO (MAX OF 5) 
C                               EFFECTIVE LINES
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C AUTHOR   : Martin O'Mullane
C
C VERSION  : 1.1                          
C DATE     : 15-01-2002
C
C MODIFIED : Martin O'Mullane  
C              - First version. Obviously based on adas407.for.
C
C
C VERSION : 1.2
C DATE    : 23-05-2003
C MODIFIED: Martin O'Mullane
C               - Use xxdata_04 to read adf04 files. This requires
C                 new varaibles a lot of which are not used. Note
C                 that in the second adf04 file a 2 in the variable
C                 name signifies that it not used in 407 whereas a
C                 'p' indicates that it's a parent value.  
C               - Remove d7data and d7datr.
C               - Increase NDLEV to 1000 and NDTRN to 15000
C               - Do not attempt to read ionised file if ionising one
C                 is H-like.
C               - Information on parameterisation type comes from IDL.
C
C VERSION : 1.3
C DATE    : 04-11-2003
C MODIFIED: Hugh Summers
C               - Increased ndorb for consistency with iodimd in d7exps.
C               - increased ndmet for parent consistency in d7alfs.
C
C VERSION : 1.4
C DATE    : 03-03-2004
C MODIFIED: Martin O'Mullane
C               - Do not open DSNINC2 if its identified as 'NULL'.
C
C VERSION : 1.5
C DATE    : 15-11-2004
C MODIFIED: Martin O'Mullane
C               - Increase the number of permitted configurations
C                 in adf04 files to 30.
C               - For PLT match at middle temperature (nv/2) rather
C                 than index 4.
C               - Increase to 3500 levels and 500000 transitions.
C               - Send name of adf04 files to screen.
C
C VERSION : 1.5
C DATE    : 04-11-2009
C MODIFIED: Martin O'Mullane
C               - Add dsnbns and metastable selection to allow the
C                 creation of an adf25 file (but only when called
C                 from run_adas407.pro, not interactive ADAS).
C
C VERSION : 1.5
C DATE    : 19-03-2010
C MODIFIED: Martin O'Mullane
C               - Do not change nuclear charge for fully stripped
C                 ion.
C
C VERSION : 1.6
C DATE    : 18-07-2012
C MODIFIED: Martin O'Mullane
C               - Increase length of configuration strings to 
C                 40 characters.
C
C VERSION : 1.7
C DATE    : 17-06-2019
C MODIFIED: Martin O'Mullane
C               - Pass lqdorb to d7exps.
C
C-----------------------------------------------------------------------
      INTEGER   NDLEV  , NDTRN  , NDTEM  , NDMET  , NDOSC  , NDBNDL
      INTEGER   NDCONF
      INTEGER   NDTHET , NDRHO  , NDREP  , NDORB  , NDQDN
      INTEGER   IUNT07 , IUNT09 , IUNT10 , IUNT11 , IUNT12
      INTEGER   L1
      INTEGER   PIPEIN , PIPEOU
C-----------------------------------------------------------------------
      REAL*8    FMIN
C-----------------------------------------------------------------------
      CHARACTER PRGTYP*1
C-----------------------------------------------------------------------
      PARAMETER( NDLEV  = 3500, NDTRN=500000, NDTEM=14, NDMET = 10, 
     &           NDOSC  = 500 )
      PARAMETER( NDBNDL = 5  )
      PARAMETER( NDCONF = 30 , NDQDN = 8  )
      PARAMETER( NDTHET = 24 , NDRHO = 24 , NDREP  = 30 , NDORB  = 61)
      PARAMETER( IUNT07 = 7  , IUNT09 = 9 , IUNT10 = 10 , iunt11 = 11, 
     &           IUNT12 = 12 )
      PARAMETER( L1 = 1      )
      PARAMETER( PIPEIN = 5  , PIPEOU = 6)
C-----------------------------------------------------------------------
      PARAMETER( FMIN = 0.003D0)
C-----------------------------------------------------------------------
      PARAMETER( PRGTYP = 'M' )
C-----------------------------------------------------------------------
      INTEGER   IZ        , IZ0       , IZ1     , ISG     ,
     &          IL        , NV        , ITRAN   , IBNDL   ,
     &          MAXLEV    , NMET      , NPMET   , 
     &          NORB      , ITSEL     , IMET    ,
     &          ICNTE     , ICNTP     , ICNTR   , ICNTH   ,
     &          IFORM     ,
     &          IZ2       , IZ02      , IZ12    , IPL
      INTEGER   NCONFG    , NTHETA
      INTEGER   NALCM     , NALCP
      INTEGER   NRHO      , INREP
      INTEGER   IM        , I         , J       , IT      , ILOW   ,
     &          ilogic 
      integer   icnte2    , icnth2    , icntp2  , icntr2 
      integer   i4unit
C-----------------------------------------------------------------------
      REAL*8    CI4       , EDISGP    , SCALGP
      REAL*8    BWNO      , BWNOP
      REAL*8    Z1
      REAL*8    EIJ       , WI        , WJ      , S       , FIJ    , 
     &          ATE       , GBARF
C-----------------------------------------------------------------------
      CHARACTER CSELR*1   , CSELD*1   , CSELS*1 , CSELP*1 , CSELL*1
      CHARACTER TITLED*3  , DATE*8    ,
     &          DSNPRS*120, DSNIC1*120, DSNIC2*120 ,  TITLED2*3      ,
     &          dsnbms*120
C-----------------------------------------------------------------------
      LOGICAL   LTADJ     , LEISS     , LWR12
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)     , IPMETR(NDMET)    ,
     &          IA(NDLEV)        ,
     &          ISA(NDLEV)       , ILA(NDLEV)       ,
     &          I1A(NDTRN)       , I2A(NDTRN)       ,
     &          IPA(NDLEV)       ,
     &          IPSA(NDLEV)      , IPLA(NDLEV)
      INTEGER   IETRN(NDTRN)     , IPTRN(NDTRN)     ,
     &          IRTRN(NDTRN)     , IHTRN(NDTRN)     ,
     &          IE1A(NDTRN)      , IE2A(NDTRN)      ,
     &          IP1A(NDTRN)      , IP2A(NDTRN)
      INTEGER   ICTM(NDMET), IUMA(NDOSC,NDMET), IMALOCA(NDOSC,NDMET)  ,
     &          ISPECA(NDMET)    , IFSPEC(NDMET)  , IEFTA(NDOSC,NDMET)
      INTEGER   ITSELA(NDMET)    , IBNDLA(NDMET)
      INTEGER   ISPRT(NDMET)     , ISPSYS(NDMET,2)  , NSYS(NDMET)   ,
     &          INPAR(NDMET)     , ILPAR(NDMET)
      INTEGER   NREP(NDREP)
      INTEGER   NCUT(NDMET)      , N0A(NDMET,2)
      INTEGER   IONLEV(NDMET,2)
      INTEGER   NCF(NDMET,NDCONF)   , LCF(NDMET,NDCONF)   ,
     &          NDCF(NDMET,NDCONF)  , LDCF(NDMET,NDCONF)  ,
     &          IINAA(NDMET,NDCONF) , IIPNAA(NDMET,NDCONF),
     &          NCTAA(NDMET,NDCONF) , NCTAAC(NDMET,NDCONF)
      INTEGER   NDMIN(NDMET)
      INTEGER   NTRANS(NDMET), ITYPEA(NDMET,NDCONF) , N1A(NDMET,NDCONF),
     &          NCUTT(NDMET,NDCONF)
      INTEGER   IZETA4(NDMET,2,NDORB) , NZETA(NDMET,2)
      INTEGER   ILINK(NDMET,NDMET,2)
      INTEGER   IALCM(NDMET)    , IALCP(NDMET)     ,
     &          ISALCM(NDMET)   , ISALCP(NDMET)
      integer   index(ndosc)
      integer   ie1a2(ndtrn)    , ie2a2(ndtrn)     ,
     &          ip1a2(ndtrn)    , ip2a2(ndtrn)     ,
     &          ietrn2(ndtrn)   ,
     &          ihtrn2(ndtrn)   , iptrn2(ndtrn)    , irtrn2(ndtrn) 
C-----------------------------------------------------------------------
      REAL*8    SCEF(NDTEM)     ,
     &          AA(NDTRN)       , AVAL(NDTRN)      ,
     &          XJA(NDLEV)      , WA(NDLEV)        ,
     &          XIA(NDLEV)      , ER(NDLEV)        ,
     &          XPJA(NDLEV)     , WPA(NDLEV)
      REAL*8    TSCEF(NDTEM,3)     , SCOM(NDTEM,NDTRN)        ,
     &          PTOT(NDTEM,NDMET)
      REAL*8    SPNFAC(NDMET,2)
      REAL*8    FFMA(NDOSC,NDMET)    , WVMA(NDOSC,NDMET)
      REAL*8    GBMA(NDTEM,NDOSC,NDMET)
      REAL*8    DEBNDL(NDBNDL,NDMET) , FBNDL(NDBNDL,NDMET)    ,
     &          GBNDL(NDBNDL,NDMET)
      REAL*8    PNLIA(NDBNDL,NDMET)  , PNLSA(NDBNDL,NDMET)    ,
     &          PNLFA(NDBNDL,NDMET)
      REAL*8    PIFIT(NDTEM,NDMET)   ,
     &          PFIT(NDTEM,NDMET)    , PAPPRX(NDTEM,NDMET)
      REAL*8    THETA(NDTHET)
      REAL*8    ENPAR(NDMET)  , PARMR(NDMET,2,4)
      REAL*8    XITRUE(NDMET,2)
      REAL*8    SAO(NDMET,2,NDTHET)
      REAL*8    WVMIN(NDMET,NDCONF) , WVMAX(NDMET,NDCONF) ,
     &          ECF(NDMET,NDCONF)   , FCF(NDMET,NDCONF)   ,
     &          PCF(NDMET,NDCONF)   , WCF(NDMET,NDCONF)   ,
     &          ECTAA(NDMET,NDCONF)
      REAL*8    W(NDMET)     , E(NDMET)   , FM0(NDMET)   , FM(NDMET)  ,
     &          DE0(NDMET)   , DE(NDMET)
      REAL*8    ALFRA(NDMET,2,NDTHET)     , ALFRA0(NDMET,2,NDTHET)     ,
     &          ALFRAR(NDMET,2,NDTHET)
      REAL*8    ALFDA(NDMET,NDTHET)      , ALFPART(NDMET, NDCONF,NDTHET)
      REAL*8    ADIELO(NDMET,2,NDTHET)
      REAL*8    PARMD(NDMET,10,NDCONF)
      REAL*8    VORB(NDORB)   , EIONA(NDMET,2,NDORB)
      REAL*8    DESPEC(NDMET) , FSPEC(NDMET)
      REAL*8    GSPEC(NDMET)  , WVSPEC(NDMET)
      REAL*8    STOT(NDTEM,NDMET)
      REAL*8    SNLIA(NDMET)  , SNLSA(NDMET)
      REAL*8    SIFIT(NDTEM,NDMET)   , SFIT(NDTEM,NDMET)
      real*8    qdorb((ndqdn*(ndqdn+1))/2)    , qdn(ndqdn)
      real*8    aa2(ndtrn)    
      REAL*8    RHO(NDRHO)    , RHOP(NDRHO)   , WNREP(NDREP)
C-----------------------------------------------------------------------
      CHARACTER TCODE(NDTRN)*1  , CSTRGA(NDLEV)*40  , 
     &          CSTRGPA(NDLEV)*40 
      CHARACTER CSELPA(NDMET)*1   , CSELLA(NDMET)*1
      CHARACTER TRMPRT(NDMET)*2
C-----------------------------------------------------------------------
      CHARACTER SAVFIL*120         , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL   LSETP(NDMET)
      LOGICAL   LEICHR(NDORB)
      LOGICAL   LLINK(NDMET,NDMET,2)
      logical   lqdorb((ndqdn*(ndqdn+1))/2)
C-----------------------------------------------------------------------
      DATA NTHETA / 24 /
      DATA THETA / 5.0D+02 , 7.0D+02 , 1.0D+03 , 1.5D+03 , 2.0D+03 ,
     &             3.0D+03 , 5.0D+03 , 7.0D+03 , 1.0D+04 , 1.5D+04 ,
     &             2.0D+04 , 3.0D+04 , 5.0D+04 , 7.0D+04 , 1.0D+05 ,
     &             1.5D+05 , 2.0D+05 , 3.0D+05 , 5.0D+05 , 7.0D+05 ,
     &             1.0D+06 , 1.5D+06 , 2.0D+06 , 3.0D+06 /
      DATA NRHO / 24 /
      DATA RHO   / 1.0D+01 , 1.0D+02 , 1.0D+03 , 1.0D+04 , 1.0D+05 ,
     &             1.0D+06 , 3.0D+06 , 1.0D+07 , 3.0D+07 , 1.0D+08 ,
     &             3.0D+08 , 1.0D+09 , 3.0D+09 , 1.0D+10 , 3.0D+10 ,
     &             1.0D+11 , 3.0D+11 , 1.0D+12 , 3.0D+12 , 1.0D+13 ,
     &             3.0D+13 , 1.0D+14 , 3.0D+14 , 1.0D+15 /
      DATA RHOP  / 0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 ,
     &             0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 ,
     &             0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 ,
     &             0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 ,
     &             0.0D+00 , 0.0D+00 , 0.0D+00 , 0.0D+00 /
       DATA INREP /24/
       DATA NREP  / 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 ,10 ,
     &             12 ,15 ,20 ,30 ,40 ,50 ,70 ,100,150,200,
     &             250,300,400,500, 0 , 0 , 0 , 0 , 0 , 0 /
       DATA WNREP / 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
     &              0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
     &              0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 /
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Extra parameters for xxdata_04 and bxttyp - unused for calculations
C-----------------------------------------------------------------------
      integer   npl           , iadftyp       , itieactn , iorb    
      integer   nplr          , npli          , icnti    , icntl   , 
     &          icnts
      integer   npl2          , iadftyp2      , iorb2   
      integer   nplr2         , npli2         , icnti2   , icntl2  , 
     &          icnts2        , itran2        , nv2      , maxlev2  
C----------------------------------------------------------------------
      logical   lprn          , lcpl          , lorb     , lbeth   ,
     &          letyp         , lptyp         , lrtyp    , lhtyp   ,
     &          lityp         , lstyp         , lltyp
      logical   lprn2         , lcpl2         , lorb2    , lbeth2  ,
     &          letyp2        , lptyp2        , lrtyp2   , lhtyp2  ,
     &          lityp2        , lstyp2        , lltyp2
C----------------------------------------------------------------------
      integer   iptla(ndmet,ndlev)            , npla(ndlev)
      integer   iitrn(ndtrn)                  , iltrn(ndtrn)       , 
     &          istrn(ndtrn)                  ,                    
     &          ia1a(ndtrn)                   , ia2a(ndtrn)        , 
     &          il1a(ndlev)                   , il2a(ndlev)        , 
     &          is1a(ndlev)                   , is2a(ndlev)        
      integer   iptla2(ndmet,ndlev)           , npla2(ndlev)       ,
     &          i1a2(ndtrn)                   , i2a2(ndtrn)
      integer   iitrn2(ndtrn)                 , iltrn2(ndtrn)      , 
     &          istrn2(ndtrn)                 ,                    
     &          ia1a2(ndtrn)                  , ia2a2(ndtrn)       , 
     &          il1a2(ndlev)                  , il2a2(ndlev)       , 
     &          is1a2(ndlev)                  , is2a2(ndlev)        
C----------------------------------------------------------------------
      real*8    bwnoa(ndmet)                  , prtwta(ndmet)      , 
     &          zpla(ndmet,ndlev)             , beth(ndtrn) 
      real*8    auga(ndtrn)                   , wvla(ndlev)
      real*8    bwnoa2(ndmet)                 , prtwta2(ndmet)     , 
     &          zpla2(ndmet,ndlev)            , beth2(ndtrn)
      real*8    auga2(ndtrn)                  , wvla2(ndlev)
      real*8    aval2(ndtrn), scef2(ndtem)    , scom2(ndtem,ndtrn) , 
     &          qdorb2((ndqdn*(ndqdn+1))/2)   , qdn2(ndqdn)
C----------------------------------------------------------------------
      character cpla(ndlev)*1                 , cprta(ndmet)*9      
      character cpla2(ndlev)*1                , cprta2(ndmet)*9    ,     
     &          tcode2(ndtrn)*1
C----------------------------------------------------------------------
      logical   ltied(ndlev)                  , lbseta(ndmet)      ,
     &          lss04a(ndlev,ndmet)
      logical   ltied2(ndlev)                 , lbseta2(ndmet)      ,
     &          lss04a2(ndlev,ndmet)          , 
     &          lqdorb2((ndqdn*(ndqdn+1))/2)
C----------------------------------------------------------------------


C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------

C Read in adf04 specific ion files

      read(pipein,'(A)') date
      read(pipein,'(A)') dsnic1
      read(pipein,'(A)') dsnic2
      read(pipein,'(A)') dsnprs
      read(pipein,'(A)') dsnbms
      read(pipein,'(A)') savfil

      read(pipein, *)ilogic
      if (ilogic.eq.0) then
         CSELS = 'A'
      else
         CSELS = 'B'
      endif
      
      read(pipein, *)ilogic
      if (ilogic.eq.0) then
         CSELR = 'A'
      else
         CSELR = 'B'
      endif
      
      read(pipein, *)ilogic
      if (ilogic.eq.0) then
         CSELD = 'A'
      else
         CSELD = 'B'
      endif
      
      read(pipein, *)ilogic
      if (ilogic.eq.0) then
         CSELP = 'A'
         CSELL = 'A'
      else
         CSELP = 'B'
         CSELL = 'B'
      endif
      
C Output file names to console to track activity
      
      write(i4unit(-1), '(A)')dsnic1
      write(i4unit(-1), '(A)')dsnic2

C Extract data from adf04 files

      OPEN( UNIT=IUNT09 , FILE=DSNIC1, STATUS='UNKNOWN')
      if (dsnic2(1:4).NE.'NULL') then
         OPEN( UNIT=IUNT10 , FILE=DSNIC2, STATUS='UNKNOWN')
      endif
       
      itieactn = 0
      
      call xxdata_04( iunt09 , 
     &                ndlev  , ndtrn , ndmet   , ndqdn , ndtem ,
     &                titled , iz    , iz0     , iz1   , bwno  ,
     &                npl    , bwnoa , lbseta  , prtwta, cprta ,
     &                il     , qdorb , lqdorb  , qdn   , iorb  ,
     &                ia     , cstrga, isa     , ila   , xja   ,
     &                wa     ,
     &                cpla   , npla  , iptla   , zpla  ,
     &                nv     , scef  ,
     &                itran  , maxlev,
     &                tcode  , i1a   , i2a     , aval  , scom  ,
     &                beth   ,     
     &                iadftyp, lprn  , lcpl    , lorb  , lbeth ,
     &                letyp  , lptyp , lrtyp   , lhtyp , lityp ,
     &                lstyp  , lltyp , itieactn, ltied
     &              )

      ISG = ISA(1)

C Fetch restricted data from second adf04 dataset

      if (iz1.EQ.iz0) then
      
         ipl        = 1
         cstrgpa(1) = '                  '
         ipa(1)     = 1
         ipsa(1)    = 1
         ipla(1)    = 0
         xpja(1)    = 0.0D0
         wpa(1)     = 0.0D0
         
         iz2        = iz + 1
         iz02       = iz0
         iz12       = iz1 + 1
         
         icnte2     = 0
      
      else

         open( unit=iunt10 , file=dsnic2, status='unknown')

         itieactn = 0
      
         call xxdata_04( iunt10  , 
     &                   ndlev   , ndtrn  , ndmet   , ndqdn  , ndtem  ,
     &                   titled2 , iz2    , iz02    , iz12   , bwnop  ,
     &                   npl2    , bwnoa2 , lbseta2 , prtwta2, cprta2 ,
     &                   ipl     , qdorb2 , lqdorb2 , qdn2   , iorb2  ,
     &                   ipa     , cstrgpa, ipsa    , ipla   , xpja   ,
     &                   wpa     ,
     &                   cpla2   , npla2  , iptla2  , zpla2  ,
     &                   nv2     , scef2  ,
     &                   itran2  , maxlev2,
     &                   tcode2  , i1a2   , i2a2    , aval2  , scom2  ,
     &                   beth2   ,     
     &                   iadftyp2, lprn2  , lcpl2   , lorb2  , lbeth2 ,
     &                   letyp2  , lptyp2 , lrtyp2  , lhtyp2 , lityp2 ,
     &                   lstyp2  , lltyp2 , itieactn, ltied2
     &                 )
     
         call bxttyp( ndlev  , ndmet  , ndtrn  , nplr2  , npli2  ,
     &                itran2 , tcode2 , i1a2   , i2a2   , aval2  ,
     &                icnte2 , icntp2 , icntr2 , icnth2 , icnti2 ,
     &                icntl2 , icnts2 ,
     &                ietrn2 , iptrn2 , irtrn2 , ihtrn2 , iitrn2 ,
     &                iltrn2 , istrn2 ,
     &                                  ie1a2  , ie2a2  , aa2    ,
     &                                  ip1a2  , ip2a2  ,
     &                                  ia1a2  , ia2a2  , auga2  ,
     &                                  il1a2  , il2a2  , wvla2  ,
     &                                  is1a2  , is2a2  , lss04a2
     &              )
     
      endif



C Sort first adf04 file transitions into transition/recombination types

       call bxttyp( ndlev  , ndmet  , ndtrn  , nplr  , npli  ,
     &              itran  , tcode  , i1a    , i2a   , aval  ,
     &              icnte  , icntp  , icntr  , icnth , icnti , 
     &              icntl  , icnts  ,
     &              ietrn  , iptrn  , irtrn  , ihtrn , iitrn ,
     &              iltrn  , istrn  ,
     &                                ie1a   , ie2a  , aa    ,
     &                                ip1a   , ip2a  ,
     &                                ia1a   , ia2a  , auga  ,
     &                                il1a   , il2a  , wvla  ,
     &                                is1a   , is2a  , lss04a 
     &            )



C Calculate level energies relative to level 1 & ionisation pot. in ryds

      CALL XXERYD( BWNO , IL , WA  , ER , XIA )

      DO IFORM = 1, 3
         CALL XXTCON( L1 , IFORM , IZ1 , NV , SCEF , TSCEF(1,IFORM) )
      END DO
      
      Z1       = IZ1
 
C--- Send ion charges to IDL
C--- Read metastable selections from IDL
      
      write(pipeou, *)iz
      call xxflsh(pipeou)
      write(pipeou, *)iz0
      call xxflsh(pipeou)
      write(pipeou, *)iz2
      call xxflsh(pipeou)
      write(pipeou, *)iz02
      call xxflsh(pipeou)
      
      read(pipein, *)nmet
      do i = 1, nmet
         read(pipein, *)imetr(i)
      end do
      
      read(pipein, *)npmet
      do i = 1, npmet
         read(pipein, *)ipmetr(i)
      end do

C Evaluate contributing lines to PLT - will be superseeded by alternative
C calculation but include for adf03 completeness. Assume one metastable
C as we are not concerned with maincl files.

      ltadj = .FALSE.
      
      DO IM = 1, NMET
        ICTM(IM) = 0
        DO IT = 1, NV
          PTOT(IT,IM) = 0.0D0
        END DO
      END DO


      DO 30 I = 1,ICNTE
        DO 25 IM = 1,NMET
 
          IF (IE1A(I).EQ.IMETR(IM)) THEN
 
             EIJ = (WA(IE2A(I))-WA(IE1A(I)))/1.097373D5
             WI  =  2.0D0*XJA(IE1A(I))+1.0D0
             WJ  =  2.0D0*XJA(IE2A(I))+1.0D0
             S   =  3.73491D-10*WJ*AA(I)/(EIJ*EIJ*EIJ)
             FIJ = 3.333333D-1*EIJ*S/WI

             DO IT = 1,NV
                ATE   = 157890.0D0*EIJ/TSCEF(IT,1)
                GBARF = 6.8916D-2*EIJ*SCOM(IT,IETRN(I))/WI
                IF (ATE.LE.165.0D0) THEN
                    PTOT(IT,IM) = PTOT(IT,IM)+1.25D-4*DEXP(-ATE)*
     &                            GBARF/DSQRT(TSCEF(IT,1))
                ENDIF
             END DO

             IF ((FIJ.GT.FMIN).AND.(ICTM(IM).LT.NDOSC)) THEN

                ICTM(IM)           = ICTM(IM)+1
                IUMA(ICTM(IM),IM)  = IE2A(I)
                FFMA(ICTM(IM),IM)  = FIJ
                WVMA(ICTM(IM),IM)  = 9.112672D2/EIJ

                DO IT = 1, NV
                   GBMA(IT,ICTM(IM),IM) = 6.8916D-2*EIJ*
     &                                    SCOM(IT,IETRN(I))/(WI*FIJ)
                END DO

             ELSEIF ((FIJ.GT.FMIN).AND.(ICTM(IM).GE.NDOSC)) THEN

                ILOW  = 0
                DO J = 1,ICTM(IM)
                   IF(FFMA(J,IM).LT.FIJ)ILOW=J
                END DO
                IF (ILOW.GT.0) THEN
                    DO J=ILOW+1,ICTM(IM)
                       IUMA(J-1,IM) = IUMA(J,IM)
                       FFMA(J-1,IM) = FFMA(J,IM)
                       WVMA(J-1,IM) = WVMA(J,IM)
                    END DO
                    IUMA(ICTM(IM),IM) = IE2A(I)
                    FFMA(ICTM(IM),IM) = FIJ
                    WVMA(ICTM(IM),IM) = 9.112672D2/EIJ

                    DO IT = 1, NV
                       GBMA(IT,ICTM(IM),IM) = 6.8916D-2*EIJ*
     &                                        SCOM(IT,IETRN(I))/(WI*FIJ)
                    END DO

                ENDIF
             ENDIF
 
          ENDIF
   25   CONTINUE
   30 CONTINUE


C Group the line powers and return options 
 
      call d7grps(ICTM(1), ndosc, wvma(1,1), index)
      
      do i = 1, ndosc
        IMALOCA(I,1) = index(i)
      end do
      
      ispeca(1) = 1

C Analyse data
C
C 1. power

      itsel = nv/2
      imet  = 1

      CALL D7PWRF( NDTEM   , NDMET   , NDOSC  , NDBNDL ,
     &             NV      , TSCEF(1,1)       ,
     &             IMET    , ITSEL   , CSELP  , CSELL  ,
     &             PTOT    , Z1      ,
     &             ICTM    , IUMA    , FFMA   , WVMA   , GBMA ,
     &             IMALOCA , IEFTA   , ISPECA ,
     &             IBNDL   ,
     &             DEBNDL  , FBNDL   , GBNDL  ,
     &             PIFIT   , PFIT    , PAPPRX ,
     &             PNLIA   , PNLSA   , PNLFA  ,
     &             WVSPEC  , IFSPEC  ,
     &             DESPEC  , FSPEC   , GSPEC  ,
     &             STOT    ,
     &             SIFIT   , SFIT    ,
     &             SNLIA   , SNLSA
     &             )

      LSETP(IMET)  = .TRUE.
      IBNDLA(IMET) = IBNDL
      ITSELA(IMET) = ITSEL
      CSELPA(IMET) = CSELP
      CSELLA(IMET) = CSELL


C 2. ionisation and recombination

      CALL D7LINK( NDLEV   , NDMET  ,
     &             NMET    , IMETR  , NPMET  , IPMETR ,
     &             CSTRGA  , ISA    , ILA    , NALCM  , IALCM ,
     &             ISALCM  ,
     &             CSTRGPA , IPSA   , IPLA   , NALCP  , IALCP ,
     &             ISALCP  ,
     &             LLINK   , ILINK  , LEISS
     &           )


      CALL D7EXPS( NDMET   , NDCONF , NDTHET , NDORB  , NDLEV   ,
     &             ndtrn   , ndqdn  , LTADJ  , 
     &             IZ      , IZ0    , IZ1    ,
     &             il      , ia     , isa    , ila    , xja     ,
     &             cstrga  , wa     , bwno   ,
     &             iorb    , lqdorb , qdorb  ,
     &             ipl     , ipa    , ipsa   , ipla   , xpja    ,
     &             cstrgpa , wpa    , bwnop  ,
     &             icnte2  , ie1a2  , ie2a2  , aval2  ,
     &             NTHETA  , THETA  , ITSELA , NPMET  , IPMETR  ,
     &             ISPRT   , ISPSYS , NSYS   , INPAR  , ILPAR   ,
     &             ENPAR   , TRMPRT , SPNFAC ,
     &             NORB    , VORB   , LEICHR ,
     &             CI4     , EIONA  , IZETA4 , NZETA  ,
     &             SAO     , IONLEV , XITRUE ,
     &             NCUT    , N0A    , PARMR  ,
     &             ALFRA   , ALFRA0 , ALFRAR , NCONFG ,
     &             WVMIN   , WVMAX  ,
     &             ECF     , FCF    , PCF    , WCF    , W       ,
     &             NCF     , LCF    , NDCF   , LDCF   , NDMIN   ,
     &             E       , DE0    , DE     , FM0    , FM      ,
     &             IINAA   , IIPNAA , NCTAA  , NCTAAC , ECTAA   ,
     &             NTRANS  , ITYPEA , N1A    , NCUTT  , PARMD   ,
     &             EDISGP  , SCALGP , ADIELO , ALFDA  , ALFPART ,
     &             LLINK   , ILINK  , LEISS  , NMET   , IMETR
     &     )


C Write the adf03, adf25 and paper.txt files

      if (dsnprs(1:4).NE.'NULL') then
      
        lwr12 = .TRUE.
        OPEN(UNIT=IUNT12,FILE=DSNPRS)
        
        CALL D7WR12( IUNT12 , LWR12   ,
     &                NDMET  , NDCONF  , NDORB   , NDBNDL  ,
     &                CSELR  , CSELD   , CSELS   , CSELP   , CSELL  ,
     &                IZ0    , IZ      , ISG     ,
     &                ISPSYS , ITYPEA  , IZETA4  ,
     &                NSYS   , NTRANS  , N0A     ,
     &                NCTAA  , NZETA   , N1A     ,
     &                Z1     , CI4     , EIONA   ,
     &                PARMR  , PARMD   ,
     &                IBNDL  ,
     &                DEBNDL , FBNDL   , GBNDL   , PNLSA ,
     &                WVSPEC , IFSPEC  ,
     &                DESPEC , FSPEC   , GSPEC   , SNLSA
     &              )

      endif
      
      if (savfil(1:4).NE.'NULL') then
      
         OPEN(UNIT=IUNT07,FILE=SAVFIL)
         CALL D7OUT0( IUNT07 , DATE   , PRGTYP , DSNIC1 , DSNIC2 ,
     &                TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &                ICNTE  , ICNTP  , ICNTR  , ICNTH  ,
     &                IL     ,
     &                IA     , CSTRGA , ISA    , ILA    , XJA    , WA ,
     &                ER     ,
     &                NV     , TSCEF  , CADAS
     &              )

         CALL D7OUT1( IUNT07  ,
     &                NDTEM   , NDMET   , NDOSC  , NDBNDL , NDORB ,
     &                NDCONF  , NDTHET  ,
     &                TITLED  , IZ0     , IZ     , IZ1    ,
     &                NV      , TSCEF   , NMET   , IMETR  ,
     &                NPMET   , IPMETR  ,
     &                ISPRT   , ISPSYS  , NSYS   , TRMPRT , SPNFAC,
     &                NORB    , VORB    , LEICHR ,
     &                CSELPA  , CSELLA  ,  PTOT  , LSETP  ,
     &                ICTM    , IUMA    , FFMA   , WVMA   , GBMA  ,
     &                IBNDLA  , ITSELA  , IEFTA  ,
     &                DEBNDL  , FBNDL   , GBNDL  ,
     &                PIFIT   , PFIT    , PAPPRX ,
     &                PNLIA   , PNLSA   , PNLFA  , NTHETA , THETA ,
     &                CI4     , IZETA4  , EIONA  , NZETA  , SAO   ,
     &                NCUT    , N0A     , PARMR  ,
     &                ALFRA   , ALFRA0  , ALFRAR ,
     &                NTRANS  , ITYPEA  , N1A    , NCUTT  , PARMD ,
     &                ADIELO  , ALFDA   , ALFPART
     &              ) 

      endif

      if (dsnbms(1:4).NE.'NULL') then
      
         OPEN( UNIT=IUNT11 , FILE=DSNBMS, STATUS='UNKNOWN')
         
         CALL D7WR11( IUNT11 ,
     &                NDMET  , NDRHO   , NDTHET  , NDREP   , NDCONF ,
     &                NDORB  , NDQDN   ,
     &                IZ0    , IZ      ,
     &                NPMET  , NRHO    , NTHETA  , INREP   ,
     &                ISPRT  , ISPSYS  , ITYPEA  , IZETA4  ,
     &                NCUT   , NSYS    , NTRANS  , N0A     , NREP   ,
     &                NCTAA  , NZETA   , N1A     ,
     &                Z1     , EDISGP  , SCALGP  , CI4     ,
     &                ALFRA  , ADIELO  , EIONA   , SAO     ,
     &                THETA  , RHO     , RHOP    ,
     &                PARMR  , PARMD   ,
     &                WNREP  ,
     &                TRMPRT ,
     &                NALCM  , ISALCM  , NALCP   , ISALCP  ,
     &                QDN
     &              )
     
      endif


C Close all the files and write terminating 1 to IDL

      close(IUNT07)            
      close(IUNT09)            
      close(IUNT10)           
      close(IUNT11)            
      close(IUNT12)            

      write(pipeou, *)1
      call xxflsh(pipeou)

C-----------------------------------------------------------------------

      END
