C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7spln.for,v 1.1 2004/07/06 13:23:50 whitefor Exp $ Date $Date: 2004/07/06 13:23:50 $

      SUBROUTINE D7SPLN(         LOSEL ,
     &                   NV    , MAXT  , NPSPL  ,
     &                   SCEF  , TOA   , TOSA   ,
     &                   PTOT  , POWOA , POWOSA ,
     &                   LTRNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7SPLN *********************
C
C  (IDENTICAL TO: E6SPLN (EXCEPT SOME VARIABLE NAMES ARE CHANGED))
C
C  PURPOSE:
C         1) PERFORMS CUBIC SPLINE ON LOG(TEMPERATURE) VERSUS LOG(POW)
C            INPUT DATA. ('SCEF' VERSUS 'PTOT' , NV DATA PAIRS)
C
C         2) INTERPOLATES 'MAXT'  POW VALUES USING ABOVE SPLINES AT
C            TEMPERATURES READ IN FROM ISPF PANELS FOR TABULAR OUTPUT.
C            (ANY TEMPERATURE VALUES WHICH REQUIRED EXTRAPOLATION TO
C             TAKE PLACE ARE SET TO ZERO).
C                 - THIS STEP ONLY TAKES PLACE IF 'LOSEL=.TRUE.' -
C
C         3) INTERPOLATES 'NPSPL' POW VALUES USING ABOVE SPLINES AT
C            TEMPERATURES EQUI-DISTANCE ON RANGE OF LOG(TEMPERATURES)
C            STORED IN INPUT 'SCEF' ARRAY.
C
C  CALLING PROGRAM: ADAS407
C
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LOSEL   = .TRUE.  => CALCULATE POWS FOR INPUT TEMPS.
C                                      READ FROM ISPF PANEL.
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF POW/TEMPERATURE
C                           PAIRS READ FOR THE TRANSITION BEING ASSESSED
C  INPUT : (I*4)  MAXT    = NUMBER OF ISPF ENTERED TEMPERATURE VALUES AT
C                           WHICH INTERPOLATED POW VALUES ARE REQUIRED
C                           FOR TABULAR OUTPUT.
C  INPUT : (I*4)  NPSPL   = NUMBER OF  SPLINE  INTERPOLATED  POW/TEMP.
C                           REQUIRED FOR GRAPHICAL DISPLAY.
C
C  INPUT : (I*4)  SCEF()  = INPUT DATA FILE: TEMPERATURES (KELVIN)
C  INPUT : (I*4)  TOA()   = ISPF PANEL ENTERED TEMPERATURES (KELVIN)
C  OUTPUT: (I*4)  TOSA()  = 'NPSPL' TEMPERATURES FOR GRAPHICAL OUTPUT
C                           (KELVIN).
C
C  INPUT : (R*8)  PTOT()  = INPUT DATA FILE: SELECTED TRANSITION -
C                           POW VALUES AT 'SCEF()'.
C  OUTPUT: (I*4)  POWOA() = SPLINE INTERPOLATED POW VALUES AT 'TOA()'
C                           (EXTRAPOLATED VALUES = 0.0).
C  OUTPUT: (R*8)  POWOSA()= SPLINE INTERPOLATED POW VALUES AT 'TOSA()'
C
C  OUTPUT: (L*4)  LTRNG() = .TRUE.  => OUTPUT   SPLINE    VALUE    WAS
C                                      INTERPOLATED FOR 'DLOG(TOA()'.
C                           .FALSE. => OUTPUT   SPLINE    VALUE    WAS
C                                      EXTRAPOLATED FOR 'DLOG(TOA()'.
C                                      (NOTE: 'YOUT()=0' AS 'IOPT < 0').
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF  INPUT  TEMP/POW
C                                      PAIRS MUST BE >= 'NV'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF 'OUTPUT TEMP/POW
C                                      PAIRS MUST BE >= 'MAXT' & 'NPSPL'
C
C          (I*4)  IARR    = ARRAY SUBSCRIPT USED FOR TEMP/POW PAIRS
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (R*8)  TSTEP   = THE SIZE OF STEP BETWEEN 'XOUT()' VALUES FOR
C                           GRAPHICAL  OUTPUT  TEMP/POW  PAIRS  TO  BE
C                           CALCULATED USING SPLINES.
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  XIN()   = LOG( 'SCEF()' )
C          (R*8)  YIN()   = LOG( 'PTOT()' )
C          (R*8)  XOUT()  = LOG(TEMPERATURES AT WHICH SPLINES REQUIRED)
C          (R*8)  YOUT()  = LOG(OUTPUT SPLINE INTERPOLATED POW VALUES)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C          (L*4)  LDUMP() = .TRUE.  => OUTPUT SPLINE VALUE INTRPOLATED
C                                      FOR 'YOUT()'.
C                           .FALSE. => OUTPUT SPLINE VALUE EXTRAPOLATED
C                                      FOR 'YOUT()'.
C                                      (NOTE: USED AS A DUMMY ARGUMENT.
C                                             ALL VALUES WILL BE TRUE.)
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    10/06/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN          , NOUT
C-----------------------------------------------------------------------
      PARAMETER( NIN = 100    , NOUT = 100    )
C-----------------------------------------------------------------------
      INTEGER    NV           , MAXT          , NPSPL
      INTEGER    IOPT         , IARR
C-----------------------------------------------------------------------
      REAL*8     R8FUN1       , TSTEP
C-----------------------------------------------------------------------
      LOGICAL    LOSEL        , LSETX
C-----------------------------------------------------------------------
      REAL*8     SCEF(NV)     , PTOT(NV)      ,
     &           TOA(MAXT)    , POWOA(MAXT)   ,
     &           TOSA(NPSPL)  , POWOSA(NPSPL)
      REAL*8     XIN(NIN)     , YIN(NIN)      ,
     &           XOUT(NOUT)   , YOUT(NOUT)
      REAL*8     DF(NIN)
C-----------------------------------------------------------------------
      LOGICAL    LTRNG(MAXT)  , LDUMP(NOUT)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.NV)
     &             STOP ' D7SPLN ERROR: NIN < NV - INCREASE NIN'
      IF (NOUT.LT.MAXT)
     &             STOP ' D7SPLN ERROR: NOUT < MAXT - INCREASE NTOUT'
      IF (NOUT.LT.NPSPL)
     &             STOP ' D7SPLN ERROR: NOUT < NPSPL - INCREASE NGOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH OFF EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = -1
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT POW/TEMP PAIRS
C-----------------------------------------------------------------------
C
         DO 1 IARR=1,NV
            XIN(IARR) = DLOG( SCEF(IARR)  )
            YIN(IARR) = DLOG( PTOT(IARR) )
    1    CONTINUE
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED TEMP/POW PAIRS FOR TABULAR OUTPUT - IF REQUESTED
C-----------------------------------------------------------------------
C
         IF (LOSEL) THEN
C
               DO 2 IARR=1,MAXT
                  XOUT(IARR) = DLOG(TOA(IARR))
    2          CONTINUE
C
            CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &                   NV    , XIN    , YIN    ,
     &                   MAXT  , XOUT   , YOUT   ,
     &                   DF    , LTRNG
     &                 )
C
               DO 3 IARR=1,MAXT
                  IF (LTRNG(IARR)) THEN
                     POWOA(IARR) = DEXP( YOUT(IARR) )
                  ELSE
                     POWOA(IARR) = 0.0
                  ENDIF
    3          CONTINUE
C
         ENDIF
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED TEMP/POW PAIRS FOR GRAPH OUTPUT- 'NPSPL' VALUES
C-----------------------------------------------------------------------
C
      TSTEP = ( XIN(NV)-XIN(1) ) / DBLE( NPSPL-1 )
C
         DO 4 IARR=1,NPSPL
            XOUT(IARR) = ( DBLE(IARR-1)*TSTEP ) + XIN(1)
    4    CONTINUE
C
      CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &             NV    , XIN    , YIN    ,
     &             NPSPL , XOUT   , YOUT   ,
     &             DF    , LDUMP
     &           )
C
         DO 5 IARR=1,NPSPL
            TOSA(IARR)   = DEXP( XOUT(IARR) )
            POWOSA(IARR) = DEXP( YOUT(IARR) )
    5    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
