C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7pwrf.for,v 1.3 2004/07/06 13:23:23 whitefor Exp $ Date $Date: 2004/07/06 13:23:23 $

      SUBROUTINE D7PWRF( NDTEM   , NDMET   , NDOSC  , NDBNDL ,
     &                   NV      , TSCEF   ,
     &                   IMET    , ITSEL   , CSELP  , CSELL  ,
     &                   PTOT    , Z1      ,
     &                   ICTM    , IUMA    , FFMA   , WVMA   , GBMA ,
     &                   IMALOCA , IEFTA   , ISPECA ,
     &                   IBNDL   ,
     &                   DEBNDL  , FBNDL   , GBNDL  ,
     &                   PIFIT   , PFIT    , PAPPRX ,
     &                   PNLIA   , PNLSA   , PNLFA  ,
     &                   WVSPEC  , IFSPEC  ,
     &                   DESPEC  , FSPEC   , GSPEC  ,
     &                   STOT    ,
     &                   SIFIT   , SFIT    ,
     &                   SNLIA   , SNLSA
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7ISPF *********************
C
C
C  SUBROUTINE:
C
C
C  INPUT : (I*4)  NDTEM     = MAX. NUMBER OF TEMPERATURES ALLOWED
C        : (I*4)  NDMET     = MAX. NUMBER OF METASTABLES ALLOWED
C        : (I*4)  NDOSC     = MAX. NUMBER OF OSCILLATOR STRENGTHS
C        : (I*4)  NDBNDL    = MAX. NUMBER OF TRANSITION BUNDLING GROUPS
C
C        : (I*4)  NV        = NUMBER OF TEMPERATURES
C        : (R*8)  TSCEF()   = ELECTRON TEMPERATURES (EV)
C        : (I*4)  IMET      = INDEX OF METASTABLE
C        : (I*4)  ITSEL     = TEMPERATURE SELECTED FOR MATCHING
C
C        : (C*1)  CSELP     = 'A' => A FORM FOR TOTAL LINE POWER
C                             'B' => B FORM FOR TOTAL LINE POWER
C        : (C*1)  CSELL     = 'A' => A FORM FOR SPECIFIC LINE POWER
C                             'B' => B FORM FOR SPECIFIC LINE POWER
C        : (R*8)  PTOT(,)   = TOTAL ZERO-DENS. RAD. POWER FOR EACH META.
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: METASTABLE INDEX
C        : (R*8)  Z1        = RECOMBINING ION CHARGE
C
C        : (I*4)  ICTM()    = NUMBER OF INCLUDED TRANSITIONS FOR EACH
C                             METASTABLE
C                             1ST DIM: METASTABLE INDEX
C        : (I*4)  IUMA(,)   = INDEX OF METASTABLE ASSIGNED TRANSITION
C                             IN FULL ELECTRON COLL. TRANSITION LIST.
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C        : (I*4)  FFMA(,)   = OSCILLATOR STRENGTH OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C        : (I*4)  WVMA(,)   = WAVELENGTH (A)  OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C        : (R*8)  GBMA(,,)  = GBAR FOR ALLLOWED TRANSITIONS
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: INCLUDED ALLOWED TRANS. INDEX
C                             3RD DIM: METASTABLE INDEX
C        : (I*4)  IMALOCA(,)= BUNDLE ALLOCATION OF ALLOWED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C        : (I*4)  ISPECA()  = SPECIFIC LINE INDEX IN ALLOWED TRANSITIONS
C                             2ND DIM: METASTABLE INDEX
C
C
C OUTPUT : (I*4)  IBNDL     = NUMBER OF TRANSITION BUNDLES
C          (I*4)  IEFTA(,)  = INCREASING BUNDLE TRANSITION ORDERING
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  DEBNDL(,) = MEAN TRANSITION ENERGY FOR BUNDLE (RYD)
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  FBNDL(,)  = SUMMED OSCILLATOR STRENGTH FOR BUNDLE
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  GBNDL(,)  = MEAN GAUNT (OR P) FACTOR FOR BUNDLE
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PIFIT(,)  = RADIATED POWER INITIAL FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PFIT(,)   = RADIATED POWER SIMPLE SCALE FIT AT ITSEL
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PAPPRX(,) = RADIATED POWER OPTIMISED FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PNLIA(,)  = INITIAL SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PNLSA(,)  = SIMPLE  SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C          (R*8)  PNLFA(,)  = OPTIMUM  SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C
C PROGRAM: (I*4)  NDBUND    = MAX. NUMBER OF BUNDLES ALLOWED
C        : (I*4)  NDTEMP    = MAX. NUMBER OF TEMPERATURES FOR
C                             INTERNAL ARRAYS
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D7PYVR     ADAS      CALCULATE VAN REGEMORTER P FACTOR
C          LMDIF1     MINPACK   MINIMISING FITTING
C          LSFUN1     ADAS      NECESSARY FOR LMDIF1
C
C
C HISTORY: BASED ON POWERFIT
C          H.P. SUMMERS, JET
C          25-2-91
C
C
C
C AUTHOR:  M O'MULLANE, UCC
C
C DATE:     3/06/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 25-04-96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED NAG ROUTINE E04FDF AND REPLACED WITH LMDIF1 FROM
C	      MINIMAX LIBRARY
C VERSION: 1.3				DATE: 21-05-96
C MODIFIED: WILLIAM OSBORN
C	    - CHANGED CUTOFF FOR PAPPRX FROM 1E-20 TO DPMPAR(2)
C
C-----------------------------------------------------------------------
 
C-----------------------------------------------------------------------
      INTEGER   NDTEM , NDMET , NDOSC , NDBNDL , NV ,
     +          IMET  , ITSEL
 
      INTEGER   NDTEMP, NDBUND
 
      INTEGER   IB,  IE,  IC, IT,  IN, IMA, IEF, IBNDL
      INTEGER   NPT, NLF, INFO, LWA
C-----------------------------------------------------------------------
      PARAMETER (NDTEMP = 14,  NDBUND = 5)
C-----------------------------------------------------------------------
      REAL*8    WVNO, WVLN, DEIH, DEEV, ATE,  GFAC, PY
      REAL*8    TE1,  TE2,  PER1, PER2, Z1, FVEC(NDTEMP), TOL
C-----------------------------------------------------------------------
      CHARACTER  CSELP*1 , CSELL*1
C-----------------------------------------------------------------------
      INTEGER    ICTM(NDMET) , IUMA(NDOSC,NDMET), IMALOCA(NDOSC,NDMET)
      INTEGER    IEFTA(NDOSC,IMET) , ISPECA(NDMET) , IFSPEC(NDMET)
      INTEGER	 IWA(NDBUND)
C-----------------------------------------------------------------------
      REAL*8     TSCEF(NDTEM)
      REAL*8     PTOT(NDTEM,NDMET)
      REAL*8     FFMA(NDOSC,NDMET)    , WVMA(NDOSC,NDMET)
      REAL*8     GBMA(NDTEM,NDOSC,NDMET)
      REAL*8     DEBNDL(NDBNDL,NDMET) , FBNDL(NDBNDL,NDMET)
      REAL*8     GBNDL(NDBNDL,NDMET)
      REAL*8     DESPEC(NDMET) , FSPEC(NDMET)
      REAL*8     GSPEC(NDMET)  , WVSPEC(NDMET)
      REAL*8     ITA(NDBUND)          , ITB(NDBUND)
      REAL*8     PNLIA(NDBNDL,NDMET)  , PNLSA(NDBNDL,NDMET)
      REAL*8     PNLFA(NDBNDL,NDMET)
      REAL*8     PIFIT(NDTEM,NDMET)   , PFIT(NDTEM,NDMET)
      REAL*8     PAPPRX(NDTEM,NDMET)
      REAL*8     STOT(NDTEM,NDMET)
      REAL*8     SNLIA(NDMET)  , SNLSA(NDMET)
      REAL*8     SIFIT(NDTEM,NDMET)   , SFIT(NDTEM,NDMET)
      REAL*8     WA(490), DPMPAR
C-----------------------------------------------------------------------
      EXTERNAL LSFUN1 
      REAL*8 PSUMA, PFITA
      COMMON /POWFIT/PSUMA(NDTEMP),PFITA(NDTEMP,NDBUND)
 
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      DO 10 IB=1,NDBNDL
        DEBNDL(IB,IMET) = 0.0
        FBNDL(IB,IMET) = 0.0
        GBNDL(IB,IMET) = 0.0
        PNLIA(IB,IMET) = 1.0
        PNLFA(IB,IMET) = 1.0
        PNLSA(IB,IMET) = 1.0
        ITA(IB)=0
        ITB(IB)=0
   10 CONTINUE
        SNLIA(IMET) = 1.0
        SNLSA(IMET) = 1.0
C-----------------------------------------------------------------------
C  FIND NUMBER OF BUNDLES AND RELABEL INTO NUMERICAL ORDER
C-----------------------------------------------------------------------
      IE=0
      DO 20 IC=1,ICTM(IMET)
        IMA=IMALOCA(IC,IMET)
        IF (IMA.NE.0) THEN
           IF (ITB(IMA).EQ.0) THEN
              IE=IE+1
              ITA(IE)=IMA
              ITB(IMA)=IE
           ENDIF
           IEFTA(IC,IMET)=ITB(IMA)
        ELSE
           IEFTA(IC,IMET)=0
        ENDIF
   20 CONTINUE
      IBNDL=IE
C
      DO 30 IC=1,ICTM(IMET)
        IEF=IEFTA(IC,IMET)
        IF (IEF.NE.0) THEN
           DEBNDL(IEF,IMET) = DEBNDL(IEF,IMET) +
     +                        FFMA(IC,IMET) * (1.0/WVMA(IC,IMET)) *
     +                        1.0D8/1.09737D5
           FBNDL(IEF,IMET) = FBNDL(IEF,IMET) + FFMA(IC,IMET)
           GBNDL(IEF,IMET) = GBNDL(IEF,IMET) + FFMA(IC,IMET) *
     +                       GBMA(ITSEL,IC,IMET)
        ENDIF
C
   30 CONTINUE
C-----------------------------------------------------------------------
C  SET UP DETAILS FOR TOTAL LINE POWER
C-----------------------------------------------------------------------
      DO 40 IB=1,IBNDL
        DEBNDL(IB,IMET) = DEBNDL(IB,IMET) / FBNDL(IB,IMET)
        GBNDL(IB,IMET)  = GBNDL(IB,IMET) / FBNDL(IB,IMET)
   40 CONTINUE
C
      DO 60 IT=1,NV
         PIFIT(IT,IMET)=0.0
 
         DO 50 IB=1,IBNDL
 
           ATE=1.57890D5*DEBNDL(IB,IMET)/TSCEF(IT)
           IF (CSELP.EQ.'A') THEN
              GFAC= GBNDL(IB,IMET)
           ELSE
              CALL D7PYVR(ATE,Z1,PY)
              GFAC=PY
              IF(IT.EQ.ITSEL) GBNDL(IB,IMET)=GFAC
           ENDIF
           IF (ATE.LE.165.0) THEN
              PFITA(IT,IB)=1.25D-4*EXP(-ATE)*
     +                             FBNDL(IB,IMET)*GFAC/SQRT(TSCEF(IT))
              PIFIT(IT,IMET)=PIFIT(IT,IMET)+PFITA(IT,IB)*PNLIA(IB,IMET)
           ENDIF
   50    CONTINUE
         NLF=IBNDL
         PSUMA(IT)=PTOT(IT,IMET)
   60 CONTINUE
C-----------------------------------------------------------------------
C  SET UP DETAILS FOR SPECIFIC LINE
C-----------------------------------------------------------------------
           WVSPEC(IMET) = WVMA(ISPECA(IMET),IMET)
           IFSPEC(IMET) = IEFTA(ISPECA(IMET),IMET)
           DESPEC(IMET) = (1.0/WVSPEC(IMET)) *
     +                        1.0D8/1.09737D5
           FSPEC(IMET) = FFMA(ISPECA(IMET),IMET)
           GSPEC(IMET) = GBMA(ITSEL,ISPECA(IMET),IMET)
      DO 70 IT=1,NV
C
         ATE=1.57890D5*DESPEC(IMET)/TSCEF(IT)
         STOT(IT,IMET)=1.25D-4*EXP(-ATE)*
     +            FSPEC(IMET)*GBMA(IT,ISPECA(IMET),IMET)/SQRT(TSCEF(IT))
C
         IF (CSELL.EQ.'A') THEN
            GFAC= GSPEC(IMET)
         ELSE
            CALL D7PYVR(ATE,Z1,PY)
            GFAC=PY
            IF(IT.EQ.ITSEL) GSPEC(IMET)=GFAC
         ENDIF
         IF (ATE.LE.165.0) THEN
            SIFIT(IT,IMET)=1.25D-4*EXP(-ATE)*SNLIA(IMET)*
     +                        FSPEC(IMET)*GFAC/SQRT(TSCEF(IT))
         ENDIF
   70 CONTINUE
C-----------------------------------------------------------------------
C     FIND SCALING FOR TOTAL POWER AT SELECTED TEMPERATURE
C-----------------------------------------------------------------------
      DO 90 IB=1,IBNDL
        PNLSA(IB,IMET) = PTOT(ITSEL,IMET)/PIFIT(ITSEL,IMET)
   90 CONTINUE
C
      DO 110 IT=1,NV
         PFIT(IT,IMET)=0.0
         DO 100 IB=1,IBNDL
           PFIT(IT,IMET)=PFIT(IT,IMET)+PFITA(IT,IB)*PNLSA(IB,IMET)
  100    CONTINUE
         NLF=IBNDL
         PSUMA(IT)=PTOT(IT,IMET)
  110 CONTINUE
C
      NPT=NV
      LWA=490
      TOL = DSQRT(DPMPAR(1))

      CALL LMDIF1(LSFUN1,NPT,NLF,PNLFA(1,IMET),FVEC,TOL,INFO,IWA,WA,
     &		  LWA)
C      WRITE(I4UNIT(-1),*)'LMDIF1 OUTPUT STATE: ',INFO
      DO 130 IT=1,NV
        PAPPRX(IT,IMET) = 0.0D0
        DO 120 IB=1,IBNDL
          PAPPRX(IT,IMET)=PAPPRX(IT,IMET)+PNLFA(IB,IMET)*PFITA(IT,IB)
  120   CONTINUE
C CHECK IF PAPPRX IS ZERO OR NEGATIVE SINCE WE PLOT ITS LOGARITHM LATER ON
        IF(PAPPRX(IT,IMET).LE.DPMPAR(2)) PAPPRX(IT,IMET)=DPMPAR(2)
  130 CONTINUE
C-----------------------------------------------------------------------
C     FIND SCALING FOR SPECIFIC LINE POWER AT SELECTED TEMPERATURE
C-----------------------------------------------------------------------
        SNLSA(IMET) = STOT(ITSEL,IMET)/SIFIT(ITSEL,IMET)
C
      DO 140 IT=1,NV
           SFIT(IT,IMET)=SIFIT(IT,IMET)*SNLSA(IMET)
  140 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
 
      END
