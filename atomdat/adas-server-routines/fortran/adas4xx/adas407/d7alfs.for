C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7alfs.for,v 1.9 2019/06/17 13:35:34 mog Exp $ Date $Date: 2019/06/17 13:35:34 $
      SUBROUTINE D7ALFS( NDMET  , NDCONF , NDTHET , ndlev  , ndtrn  ,
     &                   IODIMD , IPMET  , IPMETR , IZ     , IZ0    ,
     &                   NTHETA , THETA  , ITREF  , IGRPA  ,
     &                   NORB   , VORB   , iz1    ,
     &                   il     , ia     , isa    , xja    , wa     ,
     &                   cstrga , icnte  , aval   , ie1a   , ie2a   ,
     &                   NCUT   , N0A    , PARMR  ,
     &                   ALFRA  , ALFRA0 , ALFRAR , ALRAPX ,
     &                   NCONFG ,
     &                   WVMIN  , WVMAX  ,
     &                   ECF    , FCF    , PCF    , WCF    , W      ,
     &                   NCF    , LCF    , NDCF   , LDCF   , NDMIN  ,
     &                   E      , DE0    , DE     , FM0    , FM     ,
     &                   IINAA  , IIPNAA , NCTAA  , NCTAAC , ECTAA  ,
     &                   NTRANS , ITYPE  , N1A    , NCUTT  , PARMD  ,
     &                   ALFDA  , ALFPART, AGNGPX ,
     &                   KGRPA  , NSYS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7ALFS *********************
C
C
C  PURPOSE:
C           (1) CACLULATES RADIATIVE AND DIELECTRONIC VALUES AND
C               PARAMETERS FROM SPECIFIC ION FILES WHICH HAVE EISSNER
C               CONFIGURATION NOTATION
C           (2) CONSIDERS METASTABLE LEVEL INDICES AND EVALUATES NCUT
C               IDENTIFIES DIPOLE TRANSITION OF TYPE DN=0 AND DN>0
C               EVALUATES OSCILLATOR STRENGTHS AND AVERAGE ENERGY
C               OF TRANSITION
C           (3) SEPARATES TRANSITION ARRAYS FOR EACH METASTABLE AND
C               EVALUATES WAVELENGTH RANGES OF TRANSITION ARRAYS
C           (4) EVALUATES POWER IN EACH TRANSTION ARRAY
C           (5) CALCULATES IONISATION, RADIATIVE & DIELECTRONIC
C               RECOMBINATION VALUES AND PARAMETERS.
C
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT NUMBER FOR SPECIFIC ION FILE FOR
C  INPUT : (I*4)  NDMET    = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NDTHET   = MAXIMUM NUMBER OF TEMPS. FOR MAINCL FILE
C  INPUT : (I*4)  NDCONF   = MAXIMUM NUMBER OF CONFIGURATIONS OR
C                            DIPOLE TRANSITIONS PER PARENT ALLOWED
C  INPUT : (I*4)  IODIMD   = MAXIMUM NUMBER OF ORBITALS
C
C  INPUT : (I*4)  NPMET    = NO. OF RECOMBINING ION (PARENT) METASTABLES
C  INPUT : (I*4)  IPMETR() = INDICES OF RECOMBINING ION (PARENT)
C                            METASTABLES IN LEVEL LIST
C
C  INPUT : (I*4)  IZ       = RECOMBINING ION CHARGE
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE
C
C  INPUT : (I*4)  NTHETA   = NUMBER OF TEMPERATURES FOR MAINCL FILE
C  INPUT : (R*8)  THETA()  = Z-SCALED TEMPERATURES FOR MAINCL FILE
C  INPUT : (I*4)  ITREF    = MAINCL TEMPERATURE INDEX FOR MATCHING
C
C  INPUT : (I*4)  IGRPA()  = NUMBER OF ELECTRONS ALLOWED IN EACH SHELL
C                            1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C
C  INPUT : (R*8)  NORB     = NUMBER OF ELECTRON ORBITALS REQUIRED
C  INPUT : (R*8)  VORB()   = EFFECT. PRINC. QUANT. NO. FOR ORBITAL
C                            1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C  OUTPUT: (I*4)  N0A(,)   = LOWEST ALLOWED N-SHELL
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C  I/O   : (R*4)  PARMR(,,)= PARAMETERS OF RADIATIVE RECOMBINATION
C                            APPROXIMATE FORMS
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: PARMS.  1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C  OUTPUT: (R*8)  ALFRA(,,)= TOTAL RADIATIVE RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)  ALFRA0(,,)=GROUND RADIATIVE RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)  ALFRAR(,,)=EXCIT. RADIATIVE RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)  ALFRAPX() =
C  OUTPUT: (I*4)  NCONFG   = NUMBER OF CONFIGURATIONS
C
C  OUTPUT: (R*8)  WVMIN(,) = MINIMUM WAVELENGTH FOR TRANSITION ARRAY (A)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  WVMAX(,) = MAXIMUM WAVELENGTH FOR TRANSITION ARRAY (A)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  ECF(,)   = AVERAGE ENERGY FOR TRANSITION ARRAY (RYD)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  FCF(,)   = SUMMED OSCIL. STRENGTH FOR TRANSITION ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  PCF(,)   = RADIATED POWER FOR TRANSITION ARRAY AT
C                            SELELECTED TEMPERATURE (CF. ITSEL)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  WCF(,)   = STATISTICAL WEIGHT FOR UPPER SHELL OF ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  W()      = STATISTICAL WEIGHT FOR PARENT
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  NCF(,)   = N-SHELL OF ACTIVE ELEC. IN PARENT FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  LCF(,)   = L-SHELL OF ACTIVE ELEC. IN PARENT FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  NDCF(,)  = N-SHELL CHANGE OF ACTIVE ELECTRON FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  LDCF(,)  = L-SHELL CHANGE OF ACTIVE ELECTRON FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  NDMIN()  = ?
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (R*8)  E()      = AVERAGE ENERGY FOR TRANSITION ARRAY (RYD)
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (R*8)  DE0()    = MEAN DELTA N = 0 TRANSITION ENERGY (CM-1)
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (R*8)  DE()     = MEAN DELTA N > 0 TRANSITION ENERGY (CM-1)
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (R*8)  FM0()    = DELTA N = 0 OSCILLATOR STRENGTH
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (R*8)  FM()     = DELTA N > 0 OSCILLATOR STRENGTH
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  IINAA(,) = UPP. LEVEL INDEX OF DIPOLE TRANS FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  IIPNAA(,)= PAR. LEVEL INDEX OF DIPOLE TRANS FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  NCTAA(,) = ?
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  NCTAAC(,)= SECONDARY AUGER N-SHELL CUT-OFF FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  ECTAA (,)= IONIS. ENERGY CUT-OFF (CM-1) FOR ARRAY
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C
C  OUTPUT: (I*4)  NTRANS() = NUMBER OF DIPOLE TRANSITIONS FOR PARENT
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  ITYPE(,) = TYPE OF DIELECTRONIC CORE TRANSITION
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  N1A(,)   = LOWEST ALLOWED N-SHELL VIA DIELECTRONIC
C                            TRANSITION
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (I*4)  NCUTT()  = N-SHELL ALT. AUG. CUT-OFF FOR DIELECTRONIC
C                            TRANSITION
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  OUTPUT: (R*4)  PARMD(,,)= PARAMETERS OF DIELECTRONIC RECOMBINATION
C                            APPROXIMATE FORMS
C                            1ST DIM: PARENT INDEX
C                            3RD DIM: PARMS.  1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C                                             5: EFF. N FOR LOWEST LEVEL
C                                             6: PHASE SPACE FACTOR
C                                             7: ENERGY DISPLACEMENT
C                                             8: SCALING MULTIPLIER
C                                             9: ENERGY DISPLACEMENT
C                                            10: SCALING MULTIPLIER
C                            3RD DIM: CONFIGURATION INDEX
C  OUTPUT: (R*8)  ALFDA(,) = TOTAL DIELECTRONIC RECOMB. COEFFTS. WITHOUT
C                            SPIN SYSTEM DIVISION  (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)  ALFPART(,)=PARTIAL DIELECT. RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)  AGNGPX() = ?
C
C  OUTPUT: (I*4)  NIA()    = ?
C  OUTPUT: (I*4)  LIA()    = ?
C  OUTPUT: (R*8)  WIA()    = ?
C  OUTPUT: (I*4)  NIA()    = ?
C  OUTPUT: (I*4)  LIA()    = ?
C  OUTPUT: (R*8)  WIA()    = ?
C  OUTPUT: (R*8)  WIA()    = ?
C
C  OUTPUT: (I*4)  KGRPA()  = INDEX POINTER TO ELECTRON ORBITALS.
C  OUTPUT: (I*4)  NSYS()   = NUMBER OF SPIN SYSTEMS (1 OR 2)
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  NCUT()   = N-SHELL CUT-OFF
C                            1ST DIM: PARENT INDEX
C
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          NVGOEL     ADAS      CALC. RAD. RECOM. COEFFTS. TO N-SHELLS
C          GPCALL     ADAS      CALC. DIELECTRONIC COEFFTS. TO N-SHELLS
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    29/06/94
C
C UPDATES: 09/08/94 - HPS - CORRECT PARAMETER LIST FOR GPCALL TO INCLUDE
C                           IFSEL.  SET IFSEL =1, BUT IT IS NOT USED
C UPDATES: 07/03/96 - HPS - PUT DIMENSIONALITY OF A NUMBER OF VECTORS OF
C                           LENGTH 15 TO INTERNAL PARAMETER ITDIMD
C
C UPDATES: 15/03/96 - PEB - CORRECT ERROR: WIA(,) AND WJA(,) CHANGED TO
C                           REAL*8 FROM INTEGER.
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM VERSION
C
C VERSION: 1.2				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.3				DATE: 20-08-96
C MODIFIED: HUGH SUMMERS + WILLIAM OSBORN
C 	    ADDED TRAP FOR ZERO POWER FOR A PARENT. THIS
C           IS NOT A FULL SOLUTION (CF. D7AUTS COMMENTS)
C VERSION: 1.4				DATE: 24-06-97
C MODIFIED: HUGH SUMMERS
C           CHANGED PARAMETER KTERM FROM 100 TO 300
C                             KTRAN FROM 1000 TO 3200
C
C
C VERSION : 1.5
C DATE    : 23-05-2003
C MODIFIED: Martin O'Mullane
C               - Pass in adf04 data rather than rewinding and
C                 reading it again.
C               - Remove all unused variables and reduced length of
C                 parameter list.
C               - Remove redundant code and format statements.
C
C
C VERSION : 1.6
C DATE    : 04-11-2003
C MODIFIED: Hugh Summers
C               - Increased extended electron list strings to 93
C               - Changed minimum A-value, aminsc to 1.0D3
C               - match iodimd to ndorb for internal dimensions
C
C VERSION : 1.6
C DATE    : 06-01-2004
C MODIFIED: Martin O'Mullane
C               - Remove redundant nia, lia, wia, nja, lja and wja
C                 arguments.
C               - Pre-process configuration string with a new 
C                 routine (ceprep) to account for leading d10 and
C                 f10-f14 terms.
C               - Add error trapping code to check for overruns
C                 and index=0 errors.
C
C VERSION : 1.7
C DATE    : 15-11-2004
C MODIFIED: Martin O'Mullane
C               - Increase to 3500 levels and 500000 transitions.
C
C VERSION : 1.8
C DATE    : 18-07-2012
C MODIFIED: Martin O'Mullane
C               - Increase length of configuration strings to 
C                 40 characters.
C
C VERSION : 1.9
C DATE    : 17-06-2019
C MODIFIED: Martin O'Mullane
C               - For DR core transitions which are not evaluated by
C                 a call to gpcall, determine the type using code
C                 copied from gpcalcx. 
C
C-----------------------------------------------------------------------
      INTEGER   KTERM  , KTRAN  , ITDIMD , ndorb , imdimd
C-----------------------------------------------------------------------
      REAL*8    AMINSC
C-----------------------------------------------------------------------
      PARAMETER( KTERM  = 3500 , KTRAN = 500000 , ITDIMD = 100 )
      PARAMETER( ndorb  = 61   , imdimd = 10  , AMINSC = 1.0D3)
C-----------------------------------------------------------------------
      INTEGER   NDMET   , NDCONF  , NDTHET
      INTEGER   IODIMD  
      INTEGER   NTHETA  , ITREF
      INTEGER   I       , IC      , ICONFG  , ICOUNT  , IELEC   ,
     &          IFLAG   , IOPT    , IFSEL   ,
     &          IORB    , IPAR    , IPMET   ,
     &          ISHEL   , ISYS    , IT      , ITERM   , ITRAN   ,
     &          ITRANS  , ITYP    , IUTERM  , IZ      ,
     &          IZ0     , IZ1     , I4JGRP  , I4LGRP  , I4NGRP  ,
     &          J       , LAAL    , LAAU    , LI      ,
     &          LJ      , LL      , LSUML   , LSUMU   , LU      ,
     &          N       , NAAL    , NAAU    , NCONFG  ,
     &          NCT     , NDELPR  , NELEC   ,
     &          NI      , NJ      , NL      , NMARK   , NORB    ,
     &          NPHEL   , NSHEL   , NSQR    , NSUML   , NSUMU   ,
     &          NT      , NTENT   , NTERM   , NTRAN   , NU      ,
     &          N0      , N1
      integer   ndtrn   , ndlev   , icnte   , il      , i4unit
C-----------------------------------------------------------------------
      REAL*8    AA      , ARSCAL  , CORFAC  , DDE     ,
     &          DDE0    , DEIS    , EDISGF  , EDISP   ,
     &          EDISPG  , EIJ     , FF      , FF0     , F0      ,
     &          PHFR    , PHFR1A  , POW     , PY      , SCALEG  ,
     &          SCALGF  , TE      , VCUT    ,
     &          VINIT   , VTENT   , V0      , V1      , WI      ,
     &          WJ      , WVL     , W1      , X       ,
     &          Y       , Z       , Z1
C-----------------------------------------------------------------------
      CHARACTER BSTRG*93     , STEXP1*93    ,
     &          STRAAU*93    , STRAAL*93    , CFAAU*93   , CFAAL*93 ,
     &          ceprep*41 
C-----------------------------------------------------------------------
      INTEGER   IPMETR(NDMET)
      INTEGER   IGRPA(IODIMD), KGRPA(IODIMD)
      INTEGER   NSYS(NDMET)
      INTEGER   NCUT(NDMET)  , N0A(NDMET,2)
      INTEGER   ICFPT(KTERM)
      INTEGER   IIP(KTRAN)   , II(KTRAN)
      INTEGER   NCF(NDMET,NDCONF)   , LCF(NDMET,NDCONF)   ,
     &          NDCF(NDMET,NDCONF)  , LDCF(NDMET,NDCONF)  ,
     &          IINAA(NDMET,NDCONF) , IIPNAA(NDMET,NDCONF),
     &          NCTAA(NDMET,NDCONF) , NCTAAC(NDMET,NDCONF)
      INTEGER   NDMIN(NDMET)
      INTEGER   NTRANS(NDMET), ITYPE(NDMET,NDCONF) , N1A(NDMET,NDCONF) ,
     &          NCUTT(NDMET,NDCONF)
      INTEGER   NELA(6)      , JHEISA(6)
      INTEGER   JGRPG(imdimd)
      INTEGER   NHEISA(ndorb)
      integer   ie1a(ndtrn)  , ie2a(ndtrn) 
      integer   ia(ndlev)    , isa(ndlev)
C-----------------------------------------------------------------------
      REAL*8    VORB(IODIMD)
      REAL*8    THETA(NDTHET)
      REAL*8    PARMR(NDMET,2,4)
      REAL*8    SUMN(ITDIMD) , SUMT(ITDIMD)
      REAL*8    A(KTRAN)
      REAL*8    WVMIN(NDMET,NDCONF) , WVMAX(NDMET,NDCONF) ,
     &          ECF(NDMET,NDCONF)   , FCF(NDMET,NDCONF)   ,
     &          PCF(NDMET,NDCONF)   , WCF(NDMET,NDCONF)   ,
     &          ECTAA(NDMET,NDCONF)
      REAL*8    W(NDMET)     , E(NDMET)   , FM0(NDMET)   , FM(NDMET)   ,
     &          DE0(NDMET)   , DE(NDMET)
      REAL*8    ALFRA(NDMET,2,NDTHET), ALFRA0(NDMET,2,NDTHET) ,
     &          ALFRAR(NDMET,2,NDTHET)
      REAL*8    ALFDA(NDMET,NDTHET)  , ALFPART(NDMET,NDCONF,NDTHET)
      REAL*8    PARMD(NDMET,10,NDCONF)
      REAL*8    SUM(10,ITDIMD)      , RATN(10,ITDIMD)
      REAL*8    TELGA(ITDIMD)
      REAL*8    GA(ITDIMD),GA0(ITDIMD),GAREST(ITDIMD)
      REAL*8    ALRAPX(ITDIMD)
      REAL*8    ALFO(ITDIMD),ALFGF(ITDIMD)
      REAL*8    ALFDAT(ITDIMD)
      REAL*8    AGRNGP(ITDIMD)  , AGRNGF(ITDIMD),
     &          AGNGPX(ITDIMD)
      REAL*8    xja(ndlev)     , wa(ndlev)        , aval(ndtrn)
C-----------------------------------------------------------------------
      CHARACTER STRG6(KTERM)*41  ,   CONFIG(KTERM)*41
      CHARACTER CHEISA(6)*1
      character STREXP(KTERM)*93 
      character cstrga(ndlev)*40 
C-----------------------------------------------------------------------
      DATA NHEISA/ 1 , 2 , 2 , 3 , 3 , 3 , 4 , 4 , 4 , 4 ,
     &             5 , 5 , 5 , 5 , 5 , 6 , 6 , 6 , 6 , 6 ,
     &             6 , 7 , 7 , 7 , 7 , 7 , 7 , 7 , 8 , 8 ,
     &             8 , 8 , 8 , 8 , 8 , 8 , 9 , 9 , 9 , 9 ,
     &             9 , 9 , 9 , 9 , 9 ,10 ,10 ,10 ,10 ,10 ,
     &            10 ,10 ,10 ,10 ,10 ,11 ,11 ,11 ,11 ,11 ,
     &            11 /
      DATA BSTRG/' '/
C-----------------------------------------------------------------------
      external ceprep
C-----------------------------------------------------------------------
       if (iodimd.ne.ndorb) then
           write(i4unit(-1),1003)'iodimd',iodimd, 'ndorb',ndorb
           write(i4unit(-1),1004)
           stop
       endif
      
       if (kterm.ne.ndlev) then
           write(i4unit(-1),1003)'kterm',kterm, 'ndlev',ndlev
           write(i4unit(-1),1004)
           stop
       endif
      
       if (imdimd.ne.ndmet) then
           write(i4unit(-1),1003)'imdimd',imdimd,'ndmet',ndmet
           write(i4unit(-1),1004)
           stop
       endif

      Z1 = IZ1
      Z  = IZ

C-----------------------------------------------------------------------
C  IDENTIFY NUMBER OF TERMS AND CONFIGURATIONS
C-----------------------------------------------------------------------
      
      nterm  = il
      NCONFG = 0
      
      do iterm = 1, il
          
          strg6(iterm) = ceprep(cstrga(iterm))

          IF(ITERM.EQ.1) THEN
              NCONFG=NCONFG+1
              CONFIG(NCONFG)=STRG6(ITERM)
              ICFPT(ITERM)=NCONFG
          ELSE
              DO IT=1,ITERM-1
                IF(STRG6(ITERM).EQ.STRG6(IT)) THEN
                    ICFPT(ITERM)=ICFPT(IT)
                    GOTO 40
                 ENDIF
              END DO
              NCONFG=NCONFG+1
              CONFIG(NCONFG)=STRG6(ITERM)
              ICFPT(ITERM)=NCONFG
  40          CONTINUE
          ENDIF

      end do
      
C-----------------------------------------------------------------------
C  CONVERT MAINCL Z-SCALED TEMPERATURES TO ABSOLUTE VALUES
C  INPUT TEMPERATURES FROM SPECIFIC ION FILE
C-----------------------------------------------------------------------

      DO IT=1,NTHETA
        TELGA(IT)=Z*Z*THETA(IT)
      END DO

C-----------------------------------------------------------------------
C  IDENTIFY NUMBER OF TRANSITIONS
C-----------------------------------------------------------------------
      
      itran=1
      do i=1, icnte
         if (aval(i).gt.z1**4*aminsc) then
            II(ITRAN)  = ie1a(i)
            IIP(ITRAN) = ie2a(i)
            A(ITRAN)   = aval(i)
            itran      = itran+1
            if (itran.GT.ktran) then
               write(i4unit(-1), 3000)'Increase KTRAN'
               stop
            endif
         endif
      end do

      ntran=itran-1

C-----------------------------------------------------------------------
C  IDENTIFY CONFIGURATION ORBITALS FOR TERMS BY STRING HANDLING
C  STREXP(ITERM) HOLDS ORBITAL CHARACTER FOR EACH ELECTRON  OUTSIDE
C  CLOSED SHELLS
C-----------------------------------------------------------------------
      DO 450 ITERM=1,NTERM
        READ(STRG6(ITERM),1008)(NELA(ISHEL),CHEISA(ISHEL),
     &                          ISHEL=1,6)
        NSHEL=0
  5     CONTINUE
  
C only proceed if nshel is less than 6 
  
        if(nshel.LT.6) then
           IF(NELA(NSHEL+1).GT.0) THEN
                NSHEL=NSHEL+1
                NELA(NSHEL)=MOD(NELA(NSHEL),50)
                GO TO 5
           ENDIF
        endif
        
        DO 465 IPAR=1,IPMET
          IF(ITERM.EQ.IPMETR(IPAR)) THEN
              IF(NSHEL.GT.0)THEN
                  JGRPG(IPAR)=I4JGRP(CHEISA(NSHEL))
              ELSE
                  JGRPG(IPAR) = 0
              ENDIF
          ENDIF
  465   CONTINUE
        STREXP(ITERM)=BSTRG
        IC=1
        DO 460 ISHEL=1,NSHEL
          DO 470 I=1,NELA(ISHEL)
            IF(ISHEL.EQ.1.AND.I.EQ.1)THEN
                STREXP(ITERM)(1:1)=CHEISA(1)
                IC=IC+1
            ELSE
                STREXP(ITERM)(1:IC)=STREXP(ITERM)(1:IC-1)//CHEISA(ISHEL)
                IC=IC+1
            ENDIF
 470      CONTINUE
 460    CONTINUE
 450  CONTINUE

      NI=0
      J=1
 12   IF(STREXP(1)(J:J).NE.' ') THEN
          NI=NI+1
          J=J+1
          GO TO 12
      ENDIF
      NELEC=NI
      
C-----------------------------------------------------------------------
C  CALCULATES NCUT(IPAR) ,DUE TO AUGER EFFECTS OF PARENT CONFIGURATION
C                                                        INTERACTION
C-----------------------------------------------------------------------
      DO 250 IPAR=1,IPMET
        DO 260 ITERM=1,NTERM
          IF(IPMETR(IPAR).EQ.IA(ITERM)) THEN
              W(IPAR)=2.0D0*xja(ITERM)+1.0D0
              E(IPAR)=wa(ITERM)
              IF(IPMETR(IPAR).EQ.1) THEN
                  NCUT(IPAR)=1000
              ELSE
                  IORB=1
                  VCUT=IZ*DSQRT(109737.3D0/E(IPAR))
                  DO 255 N=1,NORB
                    IF(VCUT.GE.VORB(N))THEN
                        IORB=N
                    ENDIF
  255             CONTINUE
                  IORB=IORB+1
                  IF(IORB.GT.NORB) THEN
                      NTENT=int(VCUT)+1
  257                 CONTINUE
                      IF(NTENT.LE.NHEISA(NORB)) THEN
                          NTENT=NTENT+1
                          GOTO 257
                      ENDIF
                      NCUT(IPAR)=NTENT
                  ELSE
                    if (KGRPA(IORB).ne.0) NCUT(IPAR)=NHEISA(KGRPA(IORB))
                  ENDIF
              ENDIF
          ENDIF
 260    CONTINUE
 250  CONTINUE

      NTENT=0
C-----------------------------------------------------------------------
C  SCANS TRANSITIONS TO ISOLATE ALLOWED DELTAN=0 AND DELTAN>0 CASES
C  FOR EACH METASTABLE INDEX BY EXPLOITING STRINGS.
C  FOR EACH TRANSITION ARRAY FOR EACH METASTABLE IDENTIFIES WAVE-
C  LENGTH MINIMUM & MAXIMUM, MEAN ENERGY, OSCILLATOR STRENGTH, POWER
C  LOWER N & L AND CHANGE IN N & L.
C-----------------------------------------------------------------------

      DO 600 IPAR=1,IPMET
        NDMIN(IPAR) = 100
        FF0=0.0D0
        DDE0=0.0D0
        FF=0.0D0
        DDE=0.0D0
        DO 280 ICONFG=1,NCONFG
          WVMIN(IPAR,ICONFG)=1.0D8
          WVMAX(IPAR,ICONFG)=1.0D-8
          WCF(IPAR,ICONFG)=0.0D0
          ECF(IPAR,ICONFG)=0.0D0
          FCF(IPAR,ICONFG)=0.0D0
          PCF(IPAR,ICONFG)=0.0D0
  280   CONTINUE
        DO 700 NT=1,NTRAN
	
          IF(IPMETR(IPAR).EQ.II(NT)) THEN
              NSUML=0
              LSUML=0
              STEXP1=STREXP(IPMETR(IPAR))
              DO 290 IELEC=1,NELEC
                NSUML=NSUML+I4NGRP(STEXP1(IELEC:IELEC))
                LSUML=LSUML+I4LGRP(STEXP1(IELEC:IELEC))
  290         CONTINUE
  
              DO 800 J=1,NTERM
                IF(IIP(NT).EQ.IA(J)) THEN
                    IUTERM=J
                    AA=A(NT)
                    Y=wa(J)
                    WI=2.0D0*xja(J)+1.0D0
                    DO 900 IT=1,NTERM
                      IF(II(NT).EQ.IA(IT)) THEN
                          X=wa(IT)
                          STEXP1=STREXP(IT)
                      ENDIF
 900                CONTINUE
                    DO 850 N=1,NELEC
                      IF(I4NGRP(STREXP(J)(N:N)).EQ.
     &                    I4NGRP(STEXP1(N:N)))THEN
                          ICOUNT=0
                      ELSE
                          ICOUNT=1
                          GO TO 860
                      ENDIF
 850                CONTINUE
 860                DEIS=ABS(Y-X)
                    WVL=1.0D8/DEIS
                    ICONFG=ICFPT(IUTERM)
                    NSUMU=0
                    LSUMU=0
                    NMARK=0
                    DO 870 IELEC=1,NELEC
                      NU=I4NGRP(STREXP(IUTERM)(IELEC:IELEC))
                      LU=I4LGRP(STREXP(IUTERM)(IELEC:IELEC))
                      NL=I4NGRP(STEXP1(IELEC:IELEC))
                      LL=I4LGRP(STEXP1(IELEC:IELEC))
                      IF(NMARK.EQ.0)THEN
                          IF(NU.NE.NL .OR. LU.NE.LL) THEN
                              NCF(IPAR,ICONFG)=NL
                              LCF(IPAR,ICONFG)=LL
                              NMARK=1
                          ENDIF
                      ENDIF
                      NSUMU=NSUMU+NU
                      LSUMU=LSUMU+LU
  870               CONTINUE
                    NDCF(IPAR,ICONFG)=NSUMU-NSUML
                    LDCF(IPAR,ICONFG)=LSUMU-LSUML

                    IF( NDCF(IPAR,ICONFG) .LE. NDMIN(IPAR) ) THEN
                        NDMIN(IPAR) = NDCF(IPAR,ICONFG)
                    ENDIF

                    WVMIN(IPAR,ICONFG)=DMIN1(WVMIN(IPAR,ICONFG),
     &              WVL)
                    WVMAX(IPAR,ICONFG)=DMAX1(WVMAX(IPAR,ICONFG),
     &              WVL)
                    F0 = (AA*WI/W(IPAR))/(8.03232D9*
     &                   (DEIS/109737.3D0)**2)
                    FCF(IPAR,ICONFG)=FCF(IPAR,ICONFG)+F0
                    ECF(IPAR,ICONFG)=ECF(IPAR,ICONFG)+
     &                               (DEIS/109737.3D0)*F0
                    WCF(IPAR,ICONFG)=WCF(IPAR,ICONFG)+WI
                    IF(ICOUNT.EQ.0) THEN
                        FF0=FF0+F0
                        DDE0=DDE0+(DEIS*F0)
                    ELSE
                        FF=FF+F0
                        DDE=DDE+(DEIS*F0)
                    ENDIF
                ENDIF
 800          CONTINUE
          ENDIF
 700    CONTINUE
 
C-----------------------------------------------------------------------
C  EVALUATES RADIATED POWER FOR EACH METASTABLE AND TRANSITION ARRAY
C-----------------------------------------------------------------------
        DO 702 IT=1,NTHETA
          SUM(IPAR,IT)=0.0D0
 702    CONTINUE
        DO 710 ICONFG=1,NCONFG
          IF(WVMIN(IPAR,ICONFG).LE.WVMAX(IPAR,ICONFG)) THEN
              ECF(IPAR,ICONFG)=ECF(IPAR,ICONFG)/FCF(IPAR,ICONFG)
              DO 705 IT=1,NTHETA
                TE=TELGA(IT)
                Y=157890.0D0*ECF(IPAR,ICONFG)/TE
                CALL D7PYVR(Y,Z1,PY)
                POW=2.72D-28*FCF(IPAR,ICONFG)*PY*DEXP(
     &              -1.57890D5*ECF(IPAR,ICONFG)/TE)/DSQRT(TE)
                IF(IT.EQ.ITREF)THEN
                    PCF(IPAR,ICONFG)=POW
                ENDIF
                SUM(IPAR,IT)=SUM(IPAR,IT)+POW
705           CONTINUE
           ENDIF
710      CONTINUE
         DO 720 ICONFG=1,NCONFG
           IF(WVMIN(IPAR,ICONFG).LE.WVMAX(IPAR,ICONFG)) THEN
C------------------ CORRECTION 20/08/96 --------------------------------
               IF(SUM(IPAR,ITREF).GT.0.0D0)
     &           PCF(IPAR,ICONFG)=PCF(IPAR,ICONFG)/SUM(IPAR,ITREF)
C-----------------------------------------------------------------------
           ENDIF
 720     CONTINUE
C-----------------------------------------------------------------------
C  EVALUATES OSCILLATOR STRENGTHS AND TRANSITION ENERGY FOR DELETAN=O
C  AND DELTAN>0 TRANSITIONS FOR EACH METASTABLE LEVEL
C-----------------------------------------------------------------------
         FM0(IPAR)=FF0
         IF(FM0(IPAR).GT.1.0D-10) THEN
             DE0(IPAR)=DDE0/FM0(IPAR)
         ELSE
             DE0(IPAR)=0.0D0
         ENDIF
         FM(IPAR)=FF
         IF(FM(IPAR).GT.1.0D-10) THEN
             DE(IPAR)=DDE/FM(IPAR)
         ELSE
             DE(IPAR)=0.0D0
         ENDIF
  600  CONTINUE

C-----------------------------------------------------------------------
C  CALCULATES NCTAAC(IPAR,ICONFG) DUE TO ALTERNATIVE AUGER CHANNELS
C-----------------------------------------------------------------------
C      WRITE(7,1331)
C      WRITE(7,1332)
       DO 750 IPAR=1,IPMET
         DO 755 ICONFG=1,NCONFG
           NCTAAC(IPAR,ICONFG)=1000
           IF(WVMIN(IPAR,ICONFG).LE.WVMAX(IPAR,ICONFG)) THEN
               IFLAG=0
               DO 760 ITRANS=NTRAN,1,-1
                 IF(ICFPT(IIP(ITRANS)).EQ.ICONFG.AND.IFLAG.EQ.0) THEN
                     STRAAU=STREXP(IIP(ITRANS))
                     STRAAL=STREXP(II(ITRANS))
                     NAAL=I4NGRP(STRAAL(NELEC:NELEC))
                     NAAU=I4NGRP(STRAAU(NELEC:NELEC))
                     LAAL=I4LGRP(STRAAL(NELEC:NELEC))
                     LAAU=I4LGRP(STRAAU(NELEC:NELEC))
                     CFAAU=STRAAU(1:NELEC-1)
                     CFAAL=STRAAL(1:NELEC-1)
                     IINAA(IPAR,ICONFG)=0
                     IIPNAA(IPAR,ICONFG)=IIP(ITRANS)
                     IF(NAAL.EQ.NAAU .AND. LAAL.GE.LAAU) GOTO 759
                     IF(II(ITRANS) .GT. IPMETR(IPAR)
     &                       .AND. ISA(IIP(ITRANS)).EQ.
     &                       ISA(IPMETR(IPAR)) .AND. CFAAL.EQ.CFAAU
     &                       .AND. NAAL.LE.NAAU ) THEN
                         ECTAA(IPAR,ICONFG)=wa(IIP(ITRANS))-
     &                                      wa(II(ITRANS))
                         IFLAG=1
                         IINAA(IPAR,ICONFG)=II(ITRANS)
                         IIPNAA(IPAR,ICONFG)=IIP(ITRANS)
                     ENDIF
  759                CONTINUE
                  ENDIF
  760           CONTINUE
             ENDIF
             IF(ECTAA(IPAR,ICONFG).GT.0) THEN
                 VCUT=IZ*DSQRT(109737.3D0/ECTAA(IPAR,ICONFG))
                 IORB=1
                 DO 763 N=1,NORB
                   IF(VCUT.GE.VORB(N))THEN
                       IORB=N
                   ENDIF
  763            CONTINUE
                 IORB=IORB+1
                 IF(IORB.GT.NORB) THEN
                     NTENT=int(VCUT)+1
  767                CONTINUE
                     IF(NTENT.LE.NHEISA(NORB)) THEN
                         NTENT=NTENT+1
                         GOTO 767
                     ENDIF
                     NCTAAC(IPAR,ICONFG)=NTENT
                 ELSE
                     if (KGRPA(IORB).ne.0) then
                        NCTAAC(IPAR,ICONFG)=NHEISA(KGRPA(IORB))
                     endif
                 ENDIF
             ELSE
                 NCTAAC(IPAR,ICONFG)=1000
             ENDIF
             NTENT=0
  755      CONTINUE
  750    CONTINUE

C-----------------------------------------------------------------------
C  EVALUATES BOLTZMANN POPULATION FOR METASTABLES AT EACH
C  TEMPERATURE
C-----------------------------------------------------------------------
         W1=2*xja(1)+1
         DO 965 IT=1,NTHETA
           TE=TELGA(IT)
           SUMT(IT)=0.0D0
           SUMN(IT)=0.0D0
           DO 960 IPAR=1,IPMET
             ALFDA(IPAR,IT)=0.0D0
             RATN(IPAR,IT)=(W(IPAR)/W1)*DEXP(-1.4388D0*E(IPAR)/TE)
             SUMT(IT)=SUMT(IT)+RATN(IPAR,IT)*SUM(IPAR,IT)
             SUMN(IT)=SUMN(IT)+RATN(IPAR,IT)
  960      CONTINUE
           SUMT(IT)=SUMT(IT)/SUMN(IT)
           DO 962 IPAR=1,IPMET
             RATN(IPAR,IT)=RATN(IPAR,IT)/SUMN(IT)
 962       CONTINUE
           SUMN(IT)=1.0D0
  965    CONTINUE

C-----------------------------------------------------------------------
C     SET ARRAY'S TO ZERO
C-----------------------------------------------------------------------
         DO 327 IT=1,NTHETA
           GA(IT)=0.0D0
           GA0(IT)=0.0D0
           GAREST(IT)=0.0D0
           IF(IT.LE.NTHETA) THEN
           AGRNGP(IT)=0.0D0
           AGRNGF(IT)=0.0D0
           ALRAPX(IT)=0.0D0
           AGNGPX(IT)=0.0D0
           ENDIF
  327    CONTINUE
C-----------------------------------------------------------------------
C  EVALUATE RADIATIVE RECOMBINATION COEFFICIENTS
C-----------------------------------------------------------------------
         DO 340 IPAR=1,IPMET
           DO 345 ISYS=1,NSYS(IPAR)
             N0=N0A(IPAR,ISYS)
             V0=PARMR(IPAR,ISYS,1)
             PHFR=PARMR(IPAR,ISYS,2)
             NCT=NCUT(IPAR)
             IF(N0.GT.NCT) THEN
                 DO 328 IT=1,NTHETA
                   GA(IT)=0.0D0
                   GA0(IT)=0.0D0
                   GAREST(IT)=0.0D0
  328            CONTINUE
             ELSE
                CALL NVGOEL(NTHETA,TELGA,GA,GA0,GAREST,
     &                      Z,N0,V0,PHFR,NCT)
             ENDIF
             EDISP=0.0D0
             ARSCAL=1.0D0
             PARMR(IPAR,ISYS,3)=EDISP
             PARMR(IPAR,ISYS,4)=ARSCAL
             DO 330 IT=1,NTHETA
               ALFRA(IPAR,ISYS,IT)  = GA(IT)
               ALFRA0(IPAR,ISYS,IT) = GA0(IT)
               ALFRAR(IPAR,ISYS,IT) = GAREST(IT)
  330        CONTINUE
  345      CONTINUE
  340    CONTINUE

C-----------------------------------------------------------------------
C  EVALUATE DIELECTRONIC RECOMBINATION COEFFICIENTS
C-----------------------------------------------------------------------
         Z=IZ
         IOPT=2
         DO 370 IPAR=1,IPMET
           ITRANS=0
           NDELPR = NDMIN(IPAR) + 1
           DO 360 ICONFG=1,NCONFG
            IF( WVMIN(IPAR,ICONFG) .LE. WVMAX(IPAR,ICONFG) .AND.
     &                           NDCF(IPAR,ICONFG) .LE. NDELPR ) THEN
                 NCT=MIN0(NCUT(IPAR),NCTAAC(IPAR,ICONFG))
                 EDISPG=0.0D0
                 SCALEG=1.0D0
                 CORFAC=0.0D0
                 NI=NCF(IPAR,ICONFG)
                 LI=LCF(IPAR,ICONFG)
                 WI=W(IPAR)
                 NJ=NI+NDCF(IPAR,ICONFG)
                 LJ=LI+LDCF(IPAR,ICONFG)
                 WJ=WCF(IPAR,ICONFG)
                 EIJ=ECF(IPAR,ICONFG)
                 FF=FCF(IPAR,ICONFG)
C-----------------------------------------------------------------------
C  CALCULATION OF DIELECTRONIC RECOMBINATION PARAMETERS
C-----------------------------------------------------------------------
                 VINIT=Z*DSQRT(1.0D0/EIJ)
                 IORB=0
                 DO 510 N=1,NORB
                   IF(VINIT.GE.VORB(N))THEN
                       IORB=N
                   ENDIF
  510            CONTINUE
  511            CONTINUE
                 IORB=IORB+1

C trap overruns and 0 results
                 if (iorb.gt.ndorb) iorb = norb
		 if (KGRPA(IORB).eq.0) goto 360
                 
                 IF(KGRPA(IORB).LE.JGRPG(IPAR)) GOTO 511
                 READ(CONFIG(ICONFG),1008)(NELA(ISHEL),
     &                                      CHEISA(ISHEL),ISHEL=1,6)
                 NSHEL=0
  504            CONTINUE
                 if (nshel.LT.6) then
                    IF(NELA(NSHEL+1).GT.0) THEN
                        NSHEL=NSHEL+1
                        NELA(NSHEL)=MOD(NELA(NSHEL),50)
                        JHEISA(NSHEL)=I4JGRP(CHEISA(NSHEL))
                        GO TO 504
                    ENDIF
                 endif
                 
  530            CONTINUE
                 IF(KGRPA(IORB).LT.JHEISA(1).OR.
     &                  KGRPA(IORB).GT.JHEISA(NSHEL))THEN
                     VTENT=VORB(IORB)
                 ELSE
                     DO 515 N=1,NSHEL
                       IF(NELA(N).GE.IGRPA(JHEISA(N)) .AND.
     &                         KGRPA(IORB).EQ.JHEISA(N)) THEN
                           IORB=IORB+1
                           GOTO 530
                       ENDIF
  515                CONTINUE
                     VTENT=VORB(IORB)
                 ENDIF
                 
                 NTENT=NHEISA(KGRPA(IORB))
                 IF(IORB.GT.NORB) THEN
                     NTENT=int(VINIT)+1
                     VTENT=DFLOAT(NTENT)
  540                CONTINUE
                     IF(NTENT.LE.NHEISA(NORB)) THEN
                         NTENT=NTENT+1
                         GOTO 540
                     ENDIF
                 ENDIF
                 NPHEL=0
                 DO 550 ISHEL=1,NSHEL
                   IF(NHEISA(JHEISA(ISHEL)).EQ.NTENT) THEN
                       NPHEL=NPHEL+NELA(ISHEL)
                   ENDIF
  550            CONTINUE
                 NSQR=2*NTENT*NTENT
                 PHFR=DFLOAT(NSQR-NPHEL)/DFLOAT(NSQR)
C-----------------------------------------------------------------------
                 N1=NTENT
                 V1=VTENT
                 PHFR1A=PHFR
C-----------------------------------------------------------------------
C      SKIP CALL OF NBRGFS IF NCUT.LE.N1
C      SET ALFO,ALFGF EQUAL TO ZERO
C-----------------------------------------------------------------------
                 IF (N1.GT.NCT) THEN
                 DO 551 IT=1,NTHETA
                   ALFO(IT)=0.0D0
                   ALFGF(IT)=0.0D0
  551            CONTINUE

C Determine type of core transition

                 ityp=1                                                 
                 if(ni.eq.1.and.nj.ge.2.and.lj.eq.li+1)go to 552          
                 ityp=2                                                 
                 if(ni.eq.2.and.nj.eq.3.and.lj.eq.li+1)go to 552          
                 ityp=3                                                 
                 if(ni.eq.2.and.nj.eq.3.and.lj.eq.li-1)go to 552          
                 ityp=4                                                 
                 if(ni.eq.nj.and.lj.eq.li+1)go to 552                     
                 ityp=5                                                 
                 if(ni.eq.nj.and.lj.eq.li-1)go to 552                     
                 ityp=6                                                 
                 if(ni.lt.nj.and.lj.eq.li+1)go to 552                     
                 ityp=7                                                 
  552            continue                          

             ELSE
                 IFSEL = 1
                 CALL GPCALL(NTHETA,TELGA,ALFDAT,ALFO,
     &                       ALFGF,Z,N1,V1,NI,LI,WI,
     &                       NJ,LJ,WJ,EIJ,FF,EDISPG,SCALEG,PHFR1A,
     &                       CORFAC,NCT,ITYP,IOPT,IFSEL,IZ0)
             ENDIF
             ITRANS=ITRANS+1
             NCTAA(IPAR,ITRANS)=NCTAAC(IPAR,ICONFG)
             ITYPE(IPAR,ITRANS)=ITYP
             N1A(IPAR,ITRANS)=N1
             PARMD(IPAR,1,ITRANS)=V1
             PARMD(IPAR,2,ITRANS)=PHFR1A
             NCUTT(IPAR,ITRANS)=NCT
             PARMD(IPAR,3,ITRANS)=EIJ
             PARMD(IPAR,4,ITRANS)=FF
             PARMD(IPAR,7,ITRANS)=CORFAC
             DO 346 IT=1,NTHETA
               ALFPART(IPAR,ITRANS,IT)=ALFO(IT)
               IF(IPAR.EQ.1) THEN
                   AGRNGP(IT)=AGRNGP(IT)+ALFO(IT)
                   AGRNGF(IT)=AGRNGF(IT)+ALFGF(IT)
               ENDIF
  346        CONTINUE
             NTRANS(IPAR)=ITRANS
             DO 350 IT=1,NTHETA
               ALFDA(IPAR,IT)=ALFDA(IPAR,IT)+ALFO(IT)
  350        CONTINUE
         ENDIF
  360    CONTINUE
  370  CONTINUE
       EDISGF=0.0D0
       SCALGF=1.0D0
       DO 373 IPAR=1,IPMET
         DO 372 ITRANS=1,NTRANS(IPAR)
           PARMD(IPAR,5,ITRANS)=EDISGF
           PARMD(IPAR,6,ITRANS)=SCALGF
  372    CONTINUE
  373  CONTINUE



       RETURN

C-----------------------------------------------------------------------
 1003 format(1x,32('*'),' d7alfs error ',32('*')//
     &       1x,'internal parameter ',1a6, ' = ',i3,' ne ',1a6,' = ',i3)
 1004 format(/1x,27('*'),' program terminated   ',28('*'))

 1008  FORMAT(6(I2,1A1))
 3000  FORMAT(/1X,30('*'),' D7ALFS ERROR  ',30('*'),/,1x, A, 
     &        /,1x,75('*'))

C-----------------------------------------------------------------------
       END
