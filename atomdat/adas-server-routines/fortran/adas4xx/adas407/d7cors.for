C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7cors.for,v 1.2 2007/05/16 15:34:37 allan Exp $ Date $Date: 2007/05/16 15:34:37 $

       SUBROUTINE D7CORS(INZO,INZ,CORRS,CORRAD,CORRAR,CORRGA,LABEL)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C      PURPOSE: CORRECT EX90 OUTPUT TO BETTER DATA. FOR A GIVEN
C      ISO-ELECTRONIC SEQUENCE, USES INTERPOLATION BETWEEN VALUES OF
C      Z0.
C
C      INPUT PARAMETERS:
C         INZO    -  ATOMIC MASS OF ION
C         INZ     -  CHARGE OF RECOMBINED ION
C      OUTPUT PARAMETERS:
C         CORRS   -  MULTIPLICITIVE CORRECTION FACTOR FOR IONISATION
C         CORRAD  -  MULTIPLICITIVE CORRECTION FACTOR FOR DI-EL. RECOM.
C         CORRAR  -  MULTIPLICITIVE CORRECTION FACTOR FOR RAD. RECOM.
C         CORRGA  -  MULTIPLICITIVE CORRECTION FACTOR FOR GA0 OF RAD.
C                    RECOM. APPROXIMATE FORM
C         LABEL   -  DETAILS OF CORRECTION DATA SOURCE
C
C      NOTES:
C        (1) SEQREP(ISEQ) RETURNS NO. OF ELECTRONS IN SEQUENCE ISEQ
C        (2) NZO IS MAXIMUM NO. OF ZO'S REPRESENTING A SEQUENCE
C        (3) NZOREP(ISEQ) RETURNS NO. OF ZO'S REPRESENTING A PARTICULAR
C            SEQUENCE
C        (4) ZOREP(ISEQ,IZO) CONTAINS SET OF REPRESENTATIVE ZO'S
C        (5) DATA FOR ZOREP = 1,100 IS DUMMY. SET EQUAL TO VALUES AT
C            THE PROPER ENDS OF THE DATA.  (E.G. AT ZO = 4,27)
C
C*****************  W.J.DICKSON JET 9/1/90 *****************************
C** PE BRIDEN 19AUG94 - MADE ALL REAL NUMBERS 8 BYTE INSTEAD OF 4 BYTE
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE.
C
C VERSION: 1.2				DATE: 16-05-07
C MODIFIED: ALLAN WHITEFORD
C	    - UPDATED COMMENTS AS PART OF SUBROUTINE DOCUMENTATION
C	      PROCEDURE
C-----------------------------------------------------------------------
       PARAMETER (NSEQ=10,NZO=10)
       CHARACTER*40 LABEL,SOURCE
       DIMENSION SOURCE(NSEQ)
       DIMENSION ZOREP(NSEQ,NZO),NZOREP(NSEQ),SEQREP(NSEQ)
       DIMENSION SCORR(NSEQ,NZO),ADCORR(NSEQ,NZO)
       DIMENSION ARCORR(NSEQ,NZO),GACORR(NSEQ,NZO)
C-----------------------------------------------------------------------
C      NUCLEUS TO H -LIKE SEQUENCE
       ISEQ=1
       SEQREP(ISEQ)=1
       NZOREP(ISEQ)=9
       SOURCE(1) = 'HLIKE.DATA(R#HPS88)'
       DATA( ZOREP(1,I),I=1,9) /1.00,4.00,5.00,7.00,10.0,14.0,19.0,27.0,
     &                          100/
       DATA( SCORR(1,I),I=1,9) /1.07,1.07,1.06,1.06,1.07,1.03,1.00,1.00,
     &                          1.00/
       DATA(ADCORR(1,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(ARCORR(1,I),I=1,9) /0.95,0.95,0.95,0.94,0.95,0.94,0.94,0.95,
     &                          0.95/
       DATA(GACORR(1,I),I=1,9) /0.90,0.90,0.90,0.90,0.89,0.89,0.91,0.90,
     &                          0.90/
C-----------------------------------------------------------------------
C      H -LIKE TO HE-LIKE SEQUENCE
       ISEQ=2
       SEQREP(ISEQ)=2
       NZOREP(ISEQ)=8
       SOURCE(2) = 'HELIKE.DATA(R#HPS88)'
       DATA( ZOREP(2,I),I=1,8) /1.00,4.00,6.00,9.00,13.0,18.0,26.0,
     &                          100.0/
       DATA( SCORR(2,I),I=1,8) /1.17,1.17,1.18,1.20,1.18,1.05,1.00,
     &                          1.00/
       DATA(ADCORR(2,I),I=1,8) /0.38,0.38,0.41,0.41,0.47,0.55,0.72,
     &                          0.72/
       DATA(ARCORR(2,I),I=1,8) /1.37,1.37,1.20,1.13,1.08,1.06,1.01,
     &                          1.01/
       DATA(GACORR(2,I),I=1,8) /1.51,1.51,1.31,1.21,1.13,1.10,1.02,
     &                          1.02/
C-----------------------------------------------------------------------
C      HE-LIKE TO LI-LIKE SEQUENCE
       ISEQ=3
       SEQREP(ISEQ)=3
       NZOREP(ISEQ)=9
       SOURCE(3) = 'LILIKE.DATA(R#HPS88)'
       DATA( ZOREP(3,I),I=1,9) /1.00,4.00,5.00,7.00,10.0,14.0,19.0,27.0,
     &                          100/
       DATA( SCORR(3,I),I=1,9) /0.83,0.83,0.90,1.03,1.02,0.97,1.00,1.00,
     &                          1.00/
       DATA(ADCORR(3,I),I=1,9) /0.38,0.38,0.38,0.42,0.43,0.43,0.48,0.61,
     &                          0.61/
       DATA(ARCORR(3,I),I=1,9) /1.34,1.34,1.30,1.19,1.16,1.12,1.11,1.11,
     &                          1.11/
       DATA(GACORR(3,I),I=1,9) /1.63,1.63,1.58,1.38,1.32,1.25,1.23,1.22,
     &                          1.22/
C-----------------------------------------------------------------------
C      LI-LIKE TO BE-LIKE SEQUENCE
       ISEQ=4
       SEQREP(ISEQ)=4
       NZOREP(ISEQ)=9
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(4,I),I=1,9) /1.00,4.00,6.00,8.00,11.0,15.0,21.0,28.0,
     &                          100/
       DATA( SCORR(4,I),I=1,9) /0.97,0.97,0.98,0.95,0.93,0.93,1.00,1.00,
     &                          1.00/
       DATA(ADCORR(4,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(ARCORR(4,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(GACORR(4,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
C-----------------------------------------------------------------------
C      BE-LIKE TO B -LIKE SEQUENCE
       ISEQ=5
       SEQREP(ISEQ)=5
       NZOREP(ISEQ)=9
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(5,I),I=1,9) /1.00,5.00,6.00,8.00,9.00,12.0,16.0,21.0,
     &                          100/
       DATA( SCORR(5,I),I=1,9) /0.73,0.73,0.89,1.06,1.19,1.15,1.16,1.00,
     &                          1.00/
       DATA(ADCORR(5,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(ARCORR(5,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(GACORR(5,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
C-----------------------------------------------------------------------
C      B -LIKE TO C -LIKE SEQUENCE
       ISEQ=6
       SEQREP(ISEQ)=6
       NZOREP(ISEQ)=10
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(6,I),I=1,10) /1.00,6.00,7.00,8.00,10.0,13.0,17.0,
     &                           21.0,28.0,100/
       DATA( SCORR(6,I),I=1,10) /1.00,1.00,1.00,1.14,1.11,1.00,1.09,
     &                           0.84,0.90,1.00/
       DATA(ADCORR(6,I),I=1,10) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00,1.00,1.00/
       DATA(ARCORR(6,I),I=1,10) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00,1.00,1.00/
       DATA(GACORR(6,I),I=1,10) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00,1.00,1.00/
C-----------------------------------------------------------------------
C      C -LIKE TO N -LIKE SEQUENCE
       ISEQ=7
       SEQREP(ISEQ)=7
       NZOREP(ISEQ)=9
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(7,I),I=1,9) /1.00,7.00,8.00,9.00,11.0,14.0,18.0,28.0,
     &                          100/
       DATA( SCORR(7,I),I=1,9) /0.96,0.96,1.02,1.21,1.08,1.06,1.06,1.00,
     &                          1.00/
       DATA(ADCORR(7,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(ARCORR(7,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(GACORR(7,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
C-----------------------------------------------------------------------
C      N -LIKE TO O -LIKE SEQUENCE
       ISEQ=8
       SEQREP(ISEQ)=8
       NZOREP(ISEQ)=9
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(8,I),I=1,9) /1.00,8.00,9.00,10.0,12.0,15.0,19.0,28.0,
     &                          100/
       DATA( SCORR(8,I),I=1,9) /1.00,1.06,1.06,1.15,1.05,0.91,0.92,1.00,
     &                          1.00/
       DATA(ADCORR(8,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(ARCORR(8,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(GACORR(8,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
C-----------------------------------------------------------------------
C      O -LIKE TO F -LIKE SEQUENCE
       ISEQ=9
       SEQREP(ISEQ)=9
       NZOREP(ISEQ)=9
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(9,I),I=1,9) /1.00,9.00,10.0,11.0,13.0,16.0,20.0,25.0,
     &                          100/
       DATA( SCORR(9,I),I=1,9) /1.37,1.37,0.89,1.14,1.12,1.09,1.06,1.04,
     &                          1.04/
       DATA(ADCORR(9,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(ARCORR(9,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
       DATA(GACORR(9,I),I=1,9) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00/
C-----------------------------------------------------------------------
C      F -LIKE TO NE-LIKE SEQUENCE
       ISEQ=10
       SEQREP(ISEQ)=10
       NZOREP(ISEQ)=10
       SOURCE(ISEQ) = '                    '
       DATA( ZOREP(10,I),I=1,10) /1.00,10.0,11.0,12.0,14.0,17.0,21.0,
     &                          25.0,34.0,100/
       DATA( SCORR(10,I),I=1,10) /0.54,0.54,0.87,1.01,1.01,1.04,1.12,
     &                          1.32,1.27,1.27/
       DATA(ADCORR(10,I),I=1,10) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00,1.00,1.00/
       DATA(ARCORR(10,I),I=1,10) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00,1.00,1.00/
       DATA(GACORR(10,I),I=1,10) /1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     &                          1.00,1.00,1.00/
C-----------------------------------------------------------------------
       ISEQ=0
       NELEC=INZO-INZ
       XNELEC=NELEC
       XINZO=INZO
       DO 100, I=1,NSEQ
       IF(XNELEC.EQ.SEQREP(I)) THEN
         ISEQ=I
       ENDIF
  100  CONTINUE
       IF(ISEQ.EQ.0) THEN
         CORRS =1.00D0
         CORRAD=1.00D0
         CORRAR=1.00D0
         CORRGA=1.00D0
         LABEL='NO MATCHING DATA AVAILABLE'
       ELSE
         NIZO=NZOREP(ISEQ)
         DO 110 I=2,NIZO
         X1=ZOREP(ISEQ,I-1)
         X2=ZOREP(ISEQ,I)
         IF(X1.LE.XINZO .AND. X2.GT.XINZO)THEN
          R=(XINZO-X1)/(X2-X1)
          CORRS = SCORR(ISEQ,I-1)+R*( SCORR(ISEQ,I)- SCORR(ISEQ,I-1))
          CORRAD=ADCORR(ISEQ,I-1)+R*(ADCORR(ISEQ,I)-ADCORR(ISEQ,I-1))
          CORRAR=ARCORR(ISEQ,I-1)+R*(ARCORR(ISEQ,I)-ARCORR(ISEQ,I-1))
          CORRGA=GACORR(ISEQ,I-1)+R*(GACORR(ISEQ,I)-GACORR(ISEQ,I-1))
          LABEL=SOURCE(ISEQ)
         ENDIF
  110    CONTINUE
       ENDIF
       RETURN
       END
