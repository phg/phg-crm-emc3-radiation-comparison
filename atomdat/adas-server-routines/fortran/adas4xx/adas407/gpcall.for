C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/gpcall.for,v 1.4 2007/05/16 15:36:28 allan Exp $ Date $Date: 2007/05/16 15:36:28 $

       SUBROUTINE GPCALL(MAXT,TEA,ALFDAT,ALFO,ALFGF,Z1,N0,V0,NI,LI,WI,  
     &NJ,LJ,WJ,EIJ,F,EDISPG,SCALEG,PHFRAC,CORFAC,NCUT,ITYPE,IOPT,
     &IFSEL,IZ0)
       IMPLICIT REAL*8(A-H,O-Z)                                         
C-----------------------------------------------------------------------
C  PURPOSE: PROVIDE BURGESS GENERAL FORMULA RESULTS AT A SERIES OF 
C  TEMPERATURES, AND ALSO TO PRODUCE BURGESS GENERAL PROGRAM RESULTS
C  AT  ZERO DENSITY AT THE SAME TEMPERATURES.
C
C  THE LATTER ARE ADJUSTED TO EQUAL INPUT DATA AS FAR AS POSSIBLE BY
C  MODIFICATION OF BETHE CORRECTIONS VIA A SINGLE SCALING PARAMETER
C  CORFAC. THE ROUTINE FMIN   IS USED TO OPTIMISE CORFAC.
C  
C  ********** H.P. SUMMERS, JET             11 JUNE 1987  ************* 
C  ********** H.P. SUMMERS, JET     MOD.(1) 24 AUG  1989  ************* 
C  ********** W.J. DICKSON, JET     MOD.(2) 14 DEC  1989  *************
C  ********** W.J. DICKSON, JET     MOD.(3)  7 AUG  1990  *************
C  INPUT                                                                
C      MAXT=NUMBER OF TEMPERATURES                                      
C      TEA(I)=ELECTRON TEMPERATURES (K)                                 
C      Z1=RECOMBINING ION CHARGE                                        
C      N0=LOWEST ACCESSIBLE N-SHELL BY RECOMBINATION                    
C      V0=EFFECTIVE PRINCIPAL QUANTUM NUMBER OF LOWEST ACCESSIBLE SHELL 
C      NI=LOWER PRINCIPAL QUANTUM NUMBER OF PARENT TRANSITION           
C      LI=LOWER ANGULAR QUANTUM NUMBER OF PARENT TRNASITION.            
C      WI=LOWER PARENT STATE STATISTICAL WEIGHT.                        
C      NJ=UPPER PRINCIPAL QUANTUM NUMBER OF PARENT TRANSITION           
C      LJ=UPPER ANGULAR QUANTUM NUMBER OF PARENT TRNASITION.            
C      WJ=UPPER PARENT STATE STATISTICAL WEIGHT.                        
C      EIJ=PARENT TRANSITION ENERGY (RYD)                               
C      FIJ=ABSORPTION OSCILLATOR STRENGTH OF PARENT TRANSITION          
C      EDISPG=UNIFORM ENERGY DISPLACEMENT FOR GENERAL FORMULA           
C      SCALEG=UNIFORM SCALING OF GENERAL FORMULA                        
C      PHFRAC=INITIAL ESTIMATE OF PHASE SPACE FACTOR                    
C      CORFAC=INITIAL ESTIMATE OF BETHE CORRECTION SCALER               
C      NCUT  =HIGH N CUT-OFF (APPLICABLE TO METASTABLE INITIAL STATES)  
C  OUTPUT                                                               
C      ALFO(I)=GENERAL PROGRAM DIELECTRONIC COEFFICIENTS (CM+3 SEC-1)   
C      ALFGF(I)=GENERAL FORMULA DIELECTRONIC COEFFICIENTS               
C      PHFRAC=REVISED PHASE SPACE FACTOR                                
C      CORFAC=REVISED BETHE CORRECTION SCALER                           
C
C  UPDATE:  07/03/96  HP SUMMERS - INCREASED NRAT FROM 15 TO 100

C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    19TH APRIL 1996
C
C VERSION: 1.1				DATE: 19-04-96
C MODIFIED: WILLIAM OSBORN
C	    -NO CHANGES
C
C VERSION: 1.2				DATE: 25-04-96
C MODIFIED: WILLIAM OSBORN
C	    -REPLACED CALLS TO E04ABF WITH FUNCTION FMIN FROM WWW.NETLIB
C
C VERSION: 1.3				DATE: 09-08-96
C MODIFIED: WILLIAM OSBORN
C	    -REMOVED COMMA FROM WRITE STATEMENT (WRITING OF XV0)
C
C VERSION: 1.4				DATE: 16-05-07
C MODIFIED: Allan Whiteford
C	    - Old IBM statement labels in columns 73-80 removed. 
C	    - Updated comments as part of subroutine documentation
C             procedure.
C
C-----------------------------------------------------------------------
       PARAMETER(NRAT=100)
       DIMENSION TEA(NRAT),TEAC(NRAT),ALFOC(NRAT),ALFDTC(NRAT)
       DIMENSION CFAC(3),ALFO(NRAT),ALFDAT(NRAT),ALFGF(NRAT)            
C
       COMMON /GPCOMM/ TEAC,ALFDTC,ALFOC,Z1C,V0C,WIC,WJC,EIJC,FC,PHFRC, 
     &               CORC,MAXTC,N0C,NIC,LIC,NJC,LJC,NCUTC,ITYPEC,IOPTC  
C
       EXTERNAL GPCALC,GPCALCX
C
C
C-----------------------------------------------------------------------
C   CALCULATE GENERAL FORMULA
C-----------------------------------------------------------------------
       ZZ1=Z1*Z1                                                        
       Z2=Z1+1.0D0                                                      
       ZZ2=Z2*Z2                                                        
       EPSIJ=EIJ/ZZ2                                                    
       X=Z2*EPSIJ                                                       
       A=DSQRT(X)/(1.0D0+X*(0.105D0+0.015D0*X))                         
       B=DSQRT(Z1*Z2*ZZ2*ZZ2/(ZZ1+13.4D0))                              
       SA=1.0D0+0.015D0*Z1*ZZ1/ZZ2                                      
       DO 110 IT=1,MAXT                                                 
         ALFGF(IT)=0.0D+00
         Y=1.5789D5*(ZZ2*EPSIJ+EDISPG)/(SA*TEA(IT))                     
         ALFGF(IT)=3.0D-3*SCALEG*B*F*A*DEXP(-Y)/(TEA(IT)**1.5D0)        
C        WRITE(6,*)IT,TEA(IT),X,A,B,SA,F,Y,ALFGF(IT)
C        WRITE(7,*)IT,TEA(IT),X,A,B,SA,F,Y,ALFGF(IT)
  110  CONTINUE
C-----------------------------------------------------------------------
C   SET UP VARIABLES FOR COMMON BLOCK
C-----------------------------------------------------------------------
       MAXTC=MAXT
       DO IRAT=1,NRAT
           TEAC(IRAT)=TEA(IRAT)
          ALFOC(IRAT)=ALFO(IRAT)
          IF(IFSEL.EQ.1) THEN
              ALFDTC(IRAT)=ALFDAT(IRAT)
          ELSEIF(IFSEL.EQ.0) THEN
              ALFDTC(IRAT)=ALFGF(IRAT)
          ENDIF
       ENDDO
       Z1C=Z1
       N0C=N0
       V0C=V0
       NIC=NI
       LIC=LI
       WIC=WI
       NJC=NJ
       LJC=LJ
       WJC=WJ
       EIJC=EIJ
       CORC=CORFAC
       FC=F
       PHFRC=PHFRAC
       NCUTC=NCUT
       ITYPEC=ITYPE
       IOPTC=IOPT
       TOL=DSQRT(DPMPAR(1))
 
C      WRITE(6,1200) N0,V0,Z1,NCUT,IOPT,ITYPE
C      WRITE(6,1201) NI,LI,WI,NJ,LJ,WJ
C      WRITE(6,1202) EIJ,F,EDISPG,SCALEG,CORFAC
C      WRITE(7,1200) N0,V0,Z1,NCUT,IOPT,ITYPE
C      WRITE(7,1201) NI,LI,WI,NJ,LJ,WJ
C      WRITE(7,1202) EIJ,F,EDISPG,SCALEG,CORFAC
C
C-----------------------------------------------------------------------
C      IOPT<0   RETURNS GENERAL PROGRAM RESULTS WITH THE VALUE OF       
C               CORFAC READ FORM DATA FILE 'ALFCDAT'                    
C          =1   RETURNS GENERAL FORMULA RESULTS ONLY.                   
C          =0,2 RETURNS GENERAL FORMULA AND PROGRAM RESULTS WITH THE    
C           >3  THE INITIAL VALUE OF CORFAC USED.                       
C          =3   RETURNS GENERAL FORMULA AND PROGRAM RESULTS WITH        
C               CORFAC ADJUSTED TO GIVE AGREEMENT BETWEEN GP AND        
C               THE INPUT DATA ALFDAT.
C-----------------------------------------------------------------------
    1  IF(IOPT.LT.0) THEN
          CFAC(1)=0.0D0
          RDV = GPCALC(CFAC(1))
       ELSEIF(IOPT.EQ.1) THEN
          GOTO 250
       ELSEIF(IOPT.EQ.0 .OR. IOPT.EQ.2 .OR. IOPT.GT.3) THEN
          CFAC(1)=CORFAC
          RDV = GPCALC(CFAC(1))
       ELSEIF(IOPT.EQ.3) THEN
C MINIMISE WRT CORFAC FIRST
C          E1=0.001
C          E2=0.001
          A=-10
          B=10
C          MAXCAL=30
C          XCOR=0.0D+00
C          FRDV=0.0D+00
C          IFAIL=1
C         WRITE(6,*) ' CALLING ROUTINE TO MINIMISE CORFAC'
C         WRITE(7,*) ' CALLING ROUTINE TO MINIMISE CORFAC'
C          CALL E04ABF(GPCALC,E1,E2,A,B,MAXCAL,XCOR,FRDV,IFAIL)
          XCOR=FMIN(A,B,GPCALC,TOL)
          CORFAC=XCOR
C
          IBEFIT=0
          IF(IBEFIT.GT.0) THEN
C MINIMISE WRT V0
C            E1= 0.001
C            E2= 0.01
            A= EIJ*0.75
            B= EIJ*1.25
C            MAXCAL=30
C            XV0=V0
C            FRDV=0.0D+00
C            IFAIL=1
C           WRITE(6,*) ' CALLING ROUTINE TO MINIMISE V0'
C           WRITE(7,*) ' CALLING ROUTINE TO MINIMISE V0'
C            CALL E04ABF(GPCALCX,E1,E2,A,B,MAXCAL,XV0,FRDV,IFAIL)
            XV0 = FMIN(A,B,GPCALCX,TOL)
            V0=XV0
            WRITE(I4UNIT(-1),1300) XV0
C            WRITE(6,1300) XV0
C            WRITE(7,1300) XV0
          ENDIF
C END OF MINIMISATION
       ENDIF
C       IF(IFAIL.NE.0) THEN
C         WRITE(6,1210) IFAIL
C         WRITE(7,1210) IFAIL
C       ENDIF
       ITYPE=ITYPEC
       DO IT=1,MAXT
         ALFO(IT)=ALFOC(IT)
       ENDDO
C
  250  RETURN                                                           
C
 1200  FORMAT(1H0,' N0= ',I4,' V0= ',F9.4,' Z1= ',F7.2,' NCT= ',I5,
     &    ' IOPT= ',I3,' ITYPE =  ',I3)
 1201  FORMAT(' NI= ',I3,' LI= ',I3,' WI= ',F3.0,' NJ= ',I3,' LJ= ',I3,
     &        ' WJ= ',F3.0)
 1202  FORMAT(' EIJ= ',F9.5,' FIJ= ',F9.5,' EDISPG= ',F9.5,' SCALEG= ',
     &                 F9.5,' CORFAC= ',F9.5)
 1210  FORMAT(1H0,' NAG ROUTINE TO OPTIMISE CORFAC RETURNS IFAIL = ',I3)
 1300  FORMAT(1H0,' ADJUSTED EFF. QUANTUM NUMBER = ',F9.5)
C
      END                                                               
