C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7auts.for,v 1.9 2019/06/17 13:34:56 mog Exp $ Date $Date: 2019/06/17 13:34:56 $

      SUBROUTINE D7AUTS( NDMET  , NDTHET , NDORB  , ndlev  , ndqdn  ,
     &                   IODIMD ,
     &                   IZ     , IZ0    , iz1    ,
     &                   il     , ia     , isa    , ila    , xja    , 
     &                   wa     , cstrga , bwno   , 
     &                   iorb   , lqdorb , qdorb  ,
     &                   NTHETA , THETA  ,
     &                   NPMET  ,
     &                   IGRPA  , EICHR  , LEICHR ,
     &                   NSYS   , ISPSYS , INPAR  , ILPAR  , ENPAR  ,
     &                   SBCHA  ,
     &                   KGRPA  , IZETA4 , EIONA  , NZETA  ,
     &                   IONLEV , XITRUE ,
     &                   NORB   , VORB   ,
     &                   N0A    , PARMR  ,
     &                   LLINK  , ILINK  , LEISS  ,
     &                   NMET   , IMETR
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7AUTS *********************
C
C
C  PURPOSE:     CALCULATES IONISATION RATES FROM GIVEN SPECIFIC ION FILE
C               USING BURGESS/CHIDICHIMO FORMULA RESOLVED INTO
C               PARENT AND SPIN SYSTEM COMPONENTS.
C
C               AUTOIONISATION EFFECTS ARE INCLUDED BY REDUCING ORBITAL
C               IONISATION ENERGY TO EXCITATION ENERGY OF LOWEST AUTO-
C               IONISING STATE, WITH A LINEAR SWITCHOFF
C               BETWEEN 20<Z1<25.
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT NUMBER FOR SPECIFIC ION FILE FOR
C                            RECOMBINED ION
C  INPUT : (I*4)  NDMET    = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NDTHET   = MAXIMUM NUMBER OF TEMPS. FOR MAINCL FILE
C  INPUT : (I*4)  NDORB    = MAXIMUM NUMBER OF ELECTRON ORBITALS
C  INPUT : (I*4)  IODIMD   = MAXIMUM NUMBER OF ELECTRON ORBITALS
C
C  INPUT : (I*4)  NTHETA   = NUMBER OF TEMPERATURES FOR MAINCL FILE
C  INPUT : (R*8)  THETA()  = Z-SCALED TEMPERATURES FOR MAINCL FILE
C
C  INPUT : (I*4)  NPMET    = NO. OF RECOMBINING ION (PARENT) METASTABLES
C
C  INPUT : (I*4)  NSYS()   = NUMBER OF SPIN SYSTEMS FOR RECOMBINED ION
C                            1ST DIM: PARENT INDEX
C  INPUT : (I*4)  ISPSYS(,)= RECOMBINED ION SPIN
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C  INPUT : (I*4)  INPAR()  = N QUANTUM NO. SUM FOR ELECTRONS OF PARENT
C                            1ST DIM: PARENT INDEX
C  INPUT : (I*4)  ILPAR()  = L QUANTUM NO. SUM FOR ELECTRONS OF PARENT
C                            1ST DIM: PARENT INDEX
C  INPUT : (R*8)  ENPAR()  = RECOMBINING ION (PARENT) ENERGY
C                            1ST DIM: PARENT INDEX
C  INPUT : (L*4)  LLINK(,,)= .TRUE.  => LINK EXISTS
C                            .FALSE. => NO LINK EXISTS
C                             1ST DIM: METASTABLE INDEX
C                             2ND DIM: PARENT METASTABLE INDEX
C                             3RD DIM: SPEN SYSTEM INDEX
C  INPUT : (L*4)  ILINK(,,)= DECIMAL ORBITAL INDEX FOR RECOMBINED
C                            ION ORBITAL DIFFERENCE WITH PARENT
C                             1ST DIM: METASTABLE INDEX
C                             2ND DIM: PARENT METASTABLE INDEX
C                             3RD DIM: SPEN SYSTEM INDEX
C  INPUT : (L*4)  LEISS    = .TRUE. => PARENTS AND METASTABLES FOUND
C                                      TO HAVE EISSNER CONFIG. FORMS
C                            .FALSE => NOT EISSNER CONFIG. FORMS
C  INPUT : (I*4)  NMET    = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C  INPUT : (I*4)  IMETR() = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C
C
C  OUTPUT: (I*4)  IZ       = CHARGE ON IONISING ION
C  OUTPUT: (I*4)  IZ0      = NUCLEAR CHARGE
C
C  OUTPUT: (I*4)  NORB     = NUMBER OF DISTINCT ELECTRON ORBITALS FROM
C                            CONFIGURATIONS IN TERM LIST
C  OUTPUT: (R*8)  VORB()   = EFFECTIVE PRINCIPAL QUANTUM NUMBERS OF
C                            ORBITALS (ORDERED)
C  OUTPUT: (R*8)  EPSIL()  = ENERGIES OF ORBITALS (RYDBERG) (NATURAL
C                            ORDER)
C  OUTPUT: (I*4)  KGRPA()  = INDEXING OF SORTED ORBITALS TO NATURAL
C                            ORDER
C  OUTPUT: (I*4)  N0A(,)   = LOWEST ALLOWED N-SHELL
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C  OUTPUT: (R*8)  PARMR(,,)= PARAMETERS OF RADIATIVE RECOMBINATION
C                            APPROXIMATE FORMS
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C                            3RD IND: PARMS.  1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C
C  OUTPUT: (R*8)  SBCHA(,,)= BEST EXTIMATE OF METASTABLE AVERAGED
C                            IONISATION RATE
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C                            3RD IND: TEMPERATURE INDEX
C  OUTPUT  (R*8)  C1       = SCALING FACTOR
C  OUTPUT  (I*4)  IZETA4(,,)= NO. OF ELECTRONS IN ORBITALS
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C                            3RD IND: SHELL INDEX
C  OUTPUT  (R*8)  EIONA(,,)= SET OF SCALED ORBITAL IONISATION POTENTIALS
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C                            3RD IND: SHELL INDEX
C  OUTPUT  (I*4)  NZETA(,) = NO. OF OCCUPIED GROUND STATE ORBITALS
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C  OUTPUT  (I*4)  IONLEV(,)= TERM INDEX OF GROUND FOR SPIN SYSTEM/PARENT
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C  OUTPUT  (I*4)  XITRUE(,)= EXACT IONISATION ENERGY OF GROUND FOR SPIN
C                            SYSTEM/PARENT
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C
C          (I*4)  KTERM    = PARAMETER = MAXIMUM NUMBER OF TERMS
C                                        ALLOWED IN SUBROUTINE
C          (I*4)  NTRUE    = NUMBER OF ORBITALS USED IN LEVEL LIST
C                            INCLUDING NON-DISPLAYED CLOSE SHELLS
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          SBCHID     ADAS      CALCULATES IONISATION RATES
C          I4JGRP     ADAS      DECIMAL VALUE OF EISSNER HEX CHARACTER
C          I4LGRP     ADAS      L-VALUE FROM EISSNER HEX ORBITAL
C          I4NGRP     ADAS      N-VALUE FROM EISSNER HEX ORBITAL
C          I4NDEC     ADAS      N-VALUE FROM DECIMAL ORBITAL
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR MESSAGE OUTPUT
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    29/06/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE.
C
C VERSION: 1.2				DATE: 20-08-96
C MODIFIED: HUGH SUMMERS + WILLIAM OSBORN
C	    - TRAP IONLEV(IPAR,ISYS)=0. NOTE NO
C             FULL SOLUTION YET TO THIS PROBLEM OF
C             PARENTS AND RECOMBINED METASTABLES WITH
C             INNER SHELL DIFFERENCES
C           - ADDED FOLLOWING TO CALL PARAMETERS
C             LLINK,ILINK,LEISS,NMET AND IMETR
C           - ADDED DETECTION OF SPECIAL PARENT
C             METASTABLE LINKS
C
C
C VERSION : 1.3
C DATE    : 10-02-97
C MODIFIED: HUGH SUMMERS 
C	    - INCLUDE UNSPECIFIED LOW N-SHELLS OF OUTER N-SHELL
C             IN PHFR ON ASSUMPTION THAT THEY ARE FILLED.
C
C VERSION : 1.4
C DATE    : 23-05-2003
C MODIFIED: Martin O'Mullane
C               - Pass in adf04 data rather than rewinding and
C                 reading it again.
C               - Remove all unused variables and reduced length of
C                 parameter list.
C               - Remove redundant code and format statements.
C
C VERSION : 1.5
C DATE    : 06-01-2004
C MODIFIED: Martin O'Mullane
C               - Pre-process configuration string with a new 
C                 routine (ceprep) to account for leading d10 and
C                 f10-f14 terms.
C               - Add error trapping code to check for overruns
C                 and index=0 errors.
C
C VERSION : 1.6
C DATE    : 15-11-2004
C MODIFIED: Martin O'Mullane
C               - Increase to 3500 levels.
C
C VERSION : 1.7
C DATE    : 17-05-2007
C MODIFIED: Martin O'Mullane
C               - Updated comments as part of subroutine documentation
C                 procedure.	
C
C VERSION : 1.8
C DATE    : 17-07-2012
C MODIFIED: Martin O'Mullane
C               - Change inconsistent orbital check from a stop 
C                 to a warning.	
C               - Increase length of configuration strings to 
C                 40 characters.
C
C VERSION : 1.9
C DATE    : 17-06-2019
C MODIFIED: Martin O'Mullane
C               - Add a new argument, lqdorb, to the routine.
C               - Order orbital energies in nl but only for orbitals
C                 which are contained in the adf04 file. 
C
C-----------------------------------------------------------------------
      INTEGER   KTERM
C-----------------------------------------------------------------------
      PARAMETER(KTERM=3500)
C-----------------------------------------------------------------------
      INTEGER   NDMET   , NDTHET  , NDORB   , IODIMD
      INTEGER   I       , ILSUM   , IMIN    , IN      ,
     &          INMAX   , INMIN   , INPHEL  , INSUM   , IN0A    ,
     &          IPAR    , ISHEL   , ISYS    , IT      , ITERM   ,
     &          IX      , IZ      , IZETA   , IZHIGH  , IZLOW   ,
     &          IZ0     , IZ1     , I4JGRP  , I4LGRP  , I4NGRP  ,
     &          J       , JS0     , KMIN    , LS      ,
     &          LS0     , N       , NEL     , NORB    ,
     &          NPMET   , NSHEL   , NTERM   , NTHETA  ,
     &          NSHSUM  , NEOUT   , NEIN    ,
     &          NTRUE   , NMET    , IMET    , I4NDEC
      INTEGER   I4UNIT  , k
      integer   ndlev   , ndqdn   , il      , iorb    , L
C-----------------------------------------------------------------------
      REAL*8    BENER   , BWNO    , DIFF    , ENERSM  ,
     &          FLAG    , PHFR    , PHSUM   , RBCHID  , SBCH    ,
     &          TE      , TOT     , VMIN    , V0      ,
     &          WTSUM   , XI      , XIAUT   , ZETA    , ZFAC    ,
     &          Z       , Z1
C-----------------------------------------------------------------------
      CHARACTER STRGM*41, ceprep*41
C-----------------------------------------------------------------------
      LOGICAL   LEISS
C-----------------------------------------------------------------------
      INTEGER   NSYS(NDMET)    , ISPSYS(NDMET,2)   ,
     &          INPAR(NDMET)   , ILPAR(NDMET)
      INTEGER   IGRPA(IODIMD)  , KGRPA(IODIMD)
      INTEGER   N0A(NDMET,2)
      INTEGER   NZETA(NDMET,2) , IZETA4(NDMET,2,NDORB)
      INTEGER   IONLEV(NDMET,2)
      INTEGER   NELA(7)
      INTEGER   JS(15)
      INTEGER   IMETR(NDMET)
      INTEGER   ILINK(NDMET,NDMET,2)
      integer   ia(ndlev)      , isa(ndlev)         , ila(ndlev)    
C-----------------------------------------------------------------------
      REAL*8    ENPAR(NDMET)
      REAL*8    VORB(IODIMD) , EPSIL(IODIMD)
      REAL*8    THETA(NDTHET)
      REAL*8    PARMR(NDMET,2,4)
      REAL*8    SBCHA(NDMET,2,NDTHET)      , EIONA(NDMET,2,NDORB)
      REAL*8    XITRUE(NDMET,2)
      REAL*8    ENERSMN(10) , WTSUMN(10)
      real*8    qdorb((ndqdn*(ndqdn+1))/2) , xja(ndlev)     , 
     &          wa(ndlev)      
C-----------------------------------------------------------------------
      CHARACTER EICHR(IODIMD)*1
      CHARACTER STRG6(KTERM)*41
      CHARACTER CHEISA(7)*1
      character cstrga(ndlev)*40 
C-----------------------------------------------------------------------
      LOGICAL   LEICHR(IODIMD)
      LOGICAL   LLINK(NDMET,NDMET,2)
      logical   lqdorb((ndqdn*(ndqdn+1))/2)
C-----------------------------------------------------------------------
      external ceprep
C-------------------------------------------------------------------------------
C
C
C
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C  READ DATA FROM FILE SCANNING LEVEL  LIST
C-----------------------------------------------------------------------

      Z1=IZ1

      iterm=1
      do iterm = 1, il
         
         strg6(iterm) = ceprep(cstrga(iterm))
         read(strg6(iterm),1008)(nela(ishel),cheisa(ishel),ishel=1,7)
         if (iterm.eq.1) neout = 0
         nshel=0
 110     continue
         if(nshel.LT.7) then
            if(nela(nshel+1).gt.0) then
               nshel=nshel+1
               if(iterm.eq.1) neout= neout+mod(nela(nshel),50)
               do  i = 1,iodimd
                  if(cheisa(nshel).eq.eichr(i)) leichr(i)=.TRUE.
               end do
               go to 110
            endif
         endif
      end do
      nterm=il

C------------------------------------
C  IDENTIFY CLOSED ORBITALS IN USE
C------------------------------------
      NEIN = IZ0-IZ-NEOUT
      NSHSUM = 0
      DO I = 1,IODIMD
         NSHSUM=NSHSUM+IGRPA(I)
         IF(NEIN.GE.NSHSUM) LEICHR(I) = .TRUE.
      END DO
      
C------------------------------------
C  CHECK TRUE SUM TO COMPARE WITH NORB
C------------------------------------
      NTRUE = 0
      DO I = 1,IODIMD
         IF(LEICHR(I))NTRUE=NTRUE+1
      END DO

C----------------------------------------------------------
C  READ ORBITAL ENERGIES AND CONVERT
C  The adf04 list will have lqdorb set to false where no 
C  data is present. Skip these when filling vorb and epsil.
C----------------------------------------------------------

       k = 1
       do i = 1, iorb
          call xxidtl(i, n, l)
          if (lqdorb(i).and.qdorb(i).GT.0.0) then
             vorb(k)  = dfloat(n)-qdorb(i)
             epsil(k) = (dfloat(iz1) / (dfloat(n)-qdorb(i)))**2 
             k = k + 1
          endif
       end do
       norb = k-1

C------------------------------------
C  SORTING VORB IN INCREASING ORDER
C  KGRPA RETAINS ORBITAL CHARACTER
C------------------------------------
      DO I=1,15
         KGRPA(I)=I
      END DO

      DO I=1,NORB-1
         
         IMIN=I
         VMIN=VORB(I)
         
         DO J=I+1,NORB
            IF (VORB(J).LT.VMIN) THEN
               IMIN=J
               VMIN=VORB(J)
               KMIN=J
            ENDIF
         END DO
         
         IF (IMIN.NE.I) THEN
            VORB(IMIN)=VORB(I)
            VORB(I)=VMIN
            KGRPA(IMIN)=KGRPA(I)
            KGRPA(I)=KMIN
         ENDIF
      
      END DO

C------------------------------------
C  CHECK NTRUE AND NORB AGREE
C------------------------------------
      IF (NTRUE.NE.NORB) THEN
         WRITE(I4UNIT(-1),1005) NTRUE,NORB
c         STOP
      ENDIF

C-----------------------------------------------------------------------
C    CALCULATION OF RADIATIVE RECOMBINATION PARAMETERS FOR STAGE BELOW
C-----------------------------------------------------------------------

      DO 430 IPAR=1,NPMET
         DO 425 ISYS=1,NSYS(IPAR)
            ENERSM=0.0D0
            WTSUM=0.0D0
            PHSUM=0.0D0
            FLAG=0.0D0
            INMIN = 5
            INMAX = 1
            DO IN = 1,5
               ENERSMN(IN) = 0.0D0
               WTSUMN(IN)  = 0.0D0
            ENDDO
            DO 410 ITERM=1,NTERM
               READ(STRG6(ITERM),1008)(NELA(ISHEL),CHEISA(ISHEL),
     &              ISHEL=1,7)
               NSHEL=0
 404           CONTINUE
               if (nshel.LT.7) then
                  IF(NELA(NSHEL+1).GT.0) THEN
                     NSHEL=NSHEL+1
                     NELA(NSHEL)=MOD(NELA(NSHEL),50)
                     GO TO 404
                  ENDIF
               endif
               INSUM=0
               ILSUM=0
               LS0=I4JGRP(CHEISA(1))
               IF(LS0.GT.1) THEN
                  DO 418 LS=1,LS0-1
                     NEL=IGRPA(LS)
                     INSUM=INSUM+NEL*I4NGRP(EICHR(LS))
                     ILSUM=ILSUM+NEL*I4LGRP(EICHR(LS))
 418              CONTINUE
               ENDIF
               DO 420 ISHEL=1,NSHEL
                  NEL=0
                  IF(ISHEL.LT.NSHEL) THEN
                     NEL=NELA(ISHEL)
                  ELSE
                     NEL=NELA(ISHEL)-1
                  ENDIF
                  INSUM=INSUM+NEL*I4NGRP(CHEISA(ISHEL))
                  ILSUM=ILSUM+NEL*I4LGRP(CHEISA(ISHEL))
 420           CONTINUE
               IN0A=I4NGRP(CHEISA(NSHEL))
               IF(IN0A .GT. INMAX) INMAX = IN0A
               IF(IN0A .LT. INMIN) INMIN = IN0A
               INPHEL=0
               DO 440 ISHEL=1,NSHEL
                  IF(I4NGRP(CHEISA(ISHEL)).EQ.IN0A) THEN
                     IF((ISHEL.EQ.1).AND.
     &                  (I4LGRP(CHEISA(1)).GT.0))THEN
                        INPHEL =2*I4LGRP(CHEISA(1))**2
                     ENDIF
                     INPHEL=INPHEL+NELA(ISHEL)
                  ENDIF
 440           CONTINUE
               IF(INSUM.EQ.INPAR(IPAR) .AND. ILSUM.EQ.ILPAR(IPAR)
     &              .AND. ISA(ITERM).EQ.ISPSYS(IPAR,ISYS)) THEN
                  IF(FLAG.LT.1) THEN
                     N0A(IPAR,ISYS)=IN0A
                     IONLEV(IPAR,ISYS)=IA(ITERM)
                     PHFR=1-((DFLOAT(INPHEL)-1)/
     &                    DFLOAT(2*N0A(IPAR,ISYS)**2))
                     FLAG=FLAG+1.0D0
                  ENDIF
                  IF(IN0A.EQ.N0A(IPAR,ISYS)) THEN
                     ENERSM=ENERSM+(2*xja(ITERM)+1)*wa(ITERM)
                     WTSUM=WTSUM+(2*xja(ITERM)+1)
                     PHSUM=PHSUM+(2*ILA(ITERM)+1)
                  ELSE
                     ENERSMN(IN0A) = ENERSMN(IN0A)+ (2*xja(ITERM)+1)
     &                               *wa(ITERM)
                     WTSUMN(IN0A)  = WTSUMN(IN0A) + (2*xja(ITERM)+1)
                  ENDIF
               ENDIF
C---------------------- ADDITION 20/08/96 -----------------------------
        IF((IONLEV(IPAR,ISYS).EQ.0).AND.LEISS)THEN
            DO 445 IMET=1,NMET
              IF(LLINK(IMET,IPAR,ISYS))THEN
                  IONLEV(IPAR,ISYS)=IA(IMETR(IMET))
                  PHFR = 1.0D0 - 
     &                   1.0D0/(2.0D0*I4NDEC(ILINK(IMET,IPAR,ISYS))**2)
                  ENERSM =wa(IONLEV(IPAR,ISYS))
                  WTSUM = 0.0D0
                  GO TO 446
              ENDIF
  445       CONTINUE
        ENDIF
  446   CONTINUE
C----------------------------------------------------------------------
 410        CONTINUE
C---------------------- ADDITION 20/08/96 -----------------------------
        IF(IONLEV(IPAR,ISYS).EQ.0) THEN
            XITRUE(IPAR,ISYS)= BWNO + ENPAR(IPAR)
        ELSE
            XITRUE(IPAR,ISYS)= BWNO + ENPAR(IPAR) -
     &                         wa(IONLEV(IPAR,ISYS))
        ENDIF
C----------------------------------------------------------------------
C
            IF(WTSUM.LE.0.0D0) THEN
               WRITE(I4UNIT(-1),1004)IPAR
               N0A(IPAR,ISYS) = INMIN
               V0 = INMIN
               PHFR = 1.0D0
            ELSE
               V0=Z1*DSQRT(109760/(BWNO + ENPAR(IPAR) - (ENERSM/WTSUM)))
            ENDIF
C     
            PARMR(IPAR,ISYS,1)=V0
            PARMR(IPAR,ISYS,2)=PHFR

 425     CONTINUE
 430  CONTINUE
C-----------------------------------------------------------------------
C     EVALUATE IONISATION RATE COEFFICIENTS
C-----------------------------------------------------------------------
      IZLOW  = 0
      IZHIGH = 1
      IF(IZ1.LE.IZLOW) THEN
         ZFAC = 1.0D0
      ELSEIF(IZ1.GE.IZHIGH) THEN
         ZFAC = 0.0D0
      ELSE
         ZFAC = DFLOAT(IZHIGH-IZ1)/DFLOAT(IZHIGH-IZLOW)
      ENDIF
C-----------------------------------------------------------------------

      Z=IZ
     
      DO 320 IT=1,NTHETA
         TE=Z1*Z1*THETA(IT)
     
         DO 310 IPAR=1,NPMET
            DO 309 ISYS=1,NSYS(IPAR)
               SBCHA(IPAR,ISYS,IT)=0.0D0
               IF(IT.LE.1) THEN
                  NZETA(IPAR,ISYS)=0
               ENDIF
               TOT=0.0D0
               ZETA=1.0D0
C------------------- CORRECTION 20/08/96 ---------------------------
               IF(IONLEV(IPAR,ISYS).LE.0) GO TO 309
C-------------------------------------------------------------------
               STRGM=STRG6(IONLEV(IPAR,ISYS))
               READ(STRGM,1008)(NELA(ISHEL),CHEISA(ISHEL),ISHEL=1,7)
               NSHEL=0
 304           CONTINUE
               if (nshel.LT.7) then
                  IF(NELA(NSHEL+1).GT.0) THEN
                     NSHEL=NSHEL+1
                     NELA(NSHEL)=MOD(NELA(NSHEL),50)
                     JS(NSHEL)=I4JGRP(CHEISA(NSHEL))
                     GO TO 304
                  ENDIF
               endif
               JS0=JS(1)
               IZETA=0
C     
               XIAUT = (XITRUE(IPAR,ISYS)-ENPAR(IPAR)) / 109737.3D0
               BENER=XIAUT+( XITRUE(IPAR,ISYS)/109737.3D0 -XIAUT )*
     &               (1.0D0-ZFAC)
C     
C     IF(STRGM.EQ.STRG6(1)) THEN
C     BENER=(BWNO-wa(IONLEV(IPAR,ISYS)))/109737.3D0
C     ELSE
C     BENER=EPSIL(JS(NSHEL))
C     ENDIF
C     
               IF(JS0.GT.1)THEN
                  DO 307 N=1,JS0-1
                     ZETA=IGRPA(N)
                     XI=EPSIL(N)
                     IF(XI.LT.BENER) THEN
                        XI=BENER*((VORB(JS(NSHEL))/VORB(N))**2)
                     ENDIF
                     DIFF=XI-EPSIL(NORB)
                     IF(DIFF.LT.XIAUT)THEN
                        XI=XI-(XI-XIAUT)*ZFAC
                     ELSE
                        ix = 1
                        DO 303 I=NORB,(JS(NSHEL)+1),-1
                           IF((XI-EPSIL(I)).GT.XIAUT) THEN
                              IX=I
                           ENDIF
 303                    CONTINUE
                        XI=XI-EPSIL(IX)*ZFAC
                     ENDIF
                     SBCH=RBCHID(Z,XI,ZETA,TE)
                     IF(IT.EQ.5) THEN
                        IZETA=IZETA+1
                        IZETA4(IPAR,ISYS,IZETA)=ZETA
                        EIONA(IPAR,ISYS,IZETA)=XI
                     ENDIF
                     TOT=TOT+SBCH
 307              CONTINUE
               ENDIF
               DO 305 N=1,NSHEL
                  ZETA=NELA(N)
                  IF(N.EQ.NSHEL) THEN
                     XI=BENER
                  ELSE
                     XI=EPSIL(JS(N))
                     IF(XI.LT.BENER) THEN
                        XI=BENER*((VORB(JS(NSHEL))/VORB(JS(N)))**2)
                     ENDIF
                  ENDIF
                  DIFF=XI-EPSIL(NORB)
                  IF(DIFF.LT.XIAUT)THEN
                     XI=XI-(XI-XIAUT)*ZFAC
                  ELSE
                     DO 308 I=NORB,(JS(NSHEL)+1),-1
                        IF((XI-EPSIL(I)).GT.XIAUT) THEN
                           IX=I
                        ENDIF
 308                 CONTINUE
                     XI=XI-EPSIL(IX)*ZFAC
                  ENDIF
                  if (xi.GT.0.0D0) SBCH=RBCHID(Z,XI,ZETA,TE)
                  IF(IT.EQ.5) THEN
                     IZETA=IZETA+1
                     IZETA4(IPAR,ISYS,IZETA)=ZETA
                     EIONA(IPAR,ISYS,IZETA)=XI
                     NZETA(IPAR,ISYS)=IZETA
                  ENDIF
                  TOT=TOT+SBCH
 305         CONTINUE
             SBCHA(IPAR,ISYS,IT)=TOT
 309      CONTINUE
 310   CONTINUE
 320  CONTINUE
C     

      DO 327 IPAR=1,NPMET
         DO 326 ISYS=1,NSYS(IPAR)
            DO 325 IZETA=1,NZETA(IPAR,ISYS)
               EIONA(IPAR,ISYS,IZETA)=(EIONA(IPAR,ISYS,IZETA)/(Z1*Z1))
 325        CONTINUE
 326     CONTINUE
 327  CONTINUE
C     
      RETURN

C-----------------------------------------------------------------------

 1004 FORMAT(/1X,30('*'),' D7AUTS WARNING  ',30('*')/
     &        2X,'PARENT ',I2,' DOES NOT LINK TO RECOMBINED TERMS '/
     &        2X,'CHECK PARENT VALIDITY - RAD. RECOM. PARMS. UNSOUND'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 1005 FORMAT(/1X,30('*'),' D7AUTS WARNING  ',30('*')/
     &        2X,'INCONSISTENT ORBITALS, LEVEL SET=',I2,'  TABULATED='
     &        ,I2/
     &        1X,29('*'),' PROGRAM CONTINUES ',29('*'))
 1008  FORMAT(7(I2,1A1))

C-----------------------------------------------------------------------

       END
