C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7spf0.for,v 1.2 2004/07/06 13:23:36 whitefor Exp $ Date $Date: 2004/07/06 13:23:36 $

      SUBROUTINE D7SPF0( REP    , DSNIC1 , DSNIC2 , LDSEL )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7SPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM PANEL 'P40708','P40709'.
C           (INPUT DATA SET SPECIFICATIONS).
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSNIC1  = FIRST INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C  OUTPUT: (C*80)  DSNIC2  = SECOND INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C  OUTPUT: (L*4)   LDSEL   = .TRUE.  => FIRST DATA SET INFORMATION
C                                       TO BE DISPLAYED BEFORE RUN.
C                          = .FALSE. => FIRST DATA SET INFORMATION
C                                       NOT TO BE DISPLAYED BEFORE RUN.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    19/05/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    25TH MARCH 1996
C
C VERSION: 1.1				DATE: 25-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    INCREASED FILE LENGTHS FROM 44 TO 80
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSNIC1*80      , DSNIC2*80
C-----------------------------------------------------------------------
      LOGICAL     LDSEL
C-----------------------------------------------------------------------
      INTEGER PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSNIC1
      READ(PIPEIN,'(A)') DSNIC2


      RETURN
      END
