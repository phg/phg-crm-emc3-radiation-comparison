C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7ispf.for,v 1.4 2004/07/06 13:22:09 whitefor Exp $ Date $Date: 2004/07/06 13:22:09 $

      SUBROUTINE D7ISPF( IPAN  , LPEND  ,
     &                   NDLEV , NDTRN  , NDTEM , NDMET , NDOSC ,
     &			 NDBNDL,
     &                   IL    , IPL    , NV    , TSCEF ,
     &                   WA    , XJA    ,
     &                   ICNTE , IETRN  , IE1A  , IE2A  , AA    , SCOM ,
     &                   TITLE , NMET   , NPMET , IMETR , IPMETR,
     &                   IFOUT , ITSEL  ,
     &                   CSELR , CSELD  , CSELS , CSELP , CSELL ,
     &                   LTADJ ,
     &                   IMET  ,
     &                   PTOT  , FMIN   , Z1    ,
     &                   ICTM  , IUMA   , FFMA  , WVMA  , GBMA  , PYMA ,
     &                   CSTGMA, IMALOCA, ISPECA
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7ISPF *********************
C
C  PURPOSE: ISPF PANEL INPUT SUBROUTINE FOR 'ADAS407'
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDLEV    = MAX. NUMBER OF LEVELS ALLOWED
C  INPUT : (I*4)   NDTRN    = MAX. NO. OF TRANSITIONS ALLOWED
C  INPUT : (I*4)   NDTEM    = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)   NDMET    = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)   NDOSC    = MAX. NUMBER OF OSCILLATOR STRENGTHS
C  INPUT : (I*4)   NDBNDL   = MAX. NUMBER OF TRANSITION BUNDLES
C  INPUT : (I*4)   IL       = NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)   IPL      = NUMBER OF PARENT ENERGY LEVELS
C
C  INPUT : (I*4)   NV       = INPUT DATA FILE: NUMBER OF TEMPERATURES
C  INPUT : (R*8)   TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C  INPUT : (R*8)  WA()      = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                             1ST DIM: LEVEL INDEX
C  INPUT : (R*8)  XJA()     = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                             (NOTE: (2*XJA)+1 = STATISTICAL WEIGHT)
C                             1ST DIM: LEVEL INDEX
C  INPUT : (I*4)  ICNTE     = NUMBER OF ELECTRON COLL. TRANSITIONS
C  INPUT : (I*4)  IETRN()   = ELECTRON IMPACT TRANSITION:
C                             INDEX VALUES IN MAIN TRANS. ARRAYS WHICH
C                             REPRESENT ELECTRON IMPACT TRANSITIONS.
C  INPUT : (I*4)  IE1A()    = TRANSITION: LOWER ENERGY LEVEL INDEX
C                             1ST DIM: ELECTRON COLL. TRANSITION INDEX
C  INPUT : (I*4)  IE2A()    = TRANSITION: UPPER ENERGY LEVEL INDEX
C                             1ST DIM: ELECTRON COLL. TRANSITION INDEX
C  INPUT : (R*8)  AA()      = TRANSITION: A-VALUE (SEC-1)
C                             1ST DIM: ELECTRON COLL. TRANSITION INDEX
C  INPUT : (R*8)  SCOM(,)   = TRANSITION:
C                             GAMMA VALUES             (CASE ' ' & 'P')
C                             RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                             1ST DIMENSION - TEMPERATURE 'SCEF()'
C                             2ND DIMENSION - TRANSITION NUMBER
C  INPUT : (R*8)   Z1	    = RECOMBINING ION CHARGE
C
C  OUTPUT: (C*40)  TITLE    = IDL ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (I*4)   NMET     = NUMBER OF METASTABLES ( 1 <= NMET <= 5 )
C  OUTPUT: (I*4)   NPMET    = NUMBER OF PARENT METASTABLES (1<=NMET<=5)
C  OUTPUT: (I*4)   IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C  OUTPUT: (I*4)   IPMETR() = INDEX OF PARENT METASTABLES IN LEVEL LIST
C  OUTPUT: (I*4)   IMET     = SELECTED METASTABLE FOR ANALYSIS
C
C  OUTPUT: (I*4)   IFOUT    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED FORM
C  OUTPUT: (I*4)   ITSEL    = TEMPERATURE SELECTED MATCHING  (FROM INPUT
C                             LIST).
C
C  OUTPUT: (C*1)   CSELR    = 'A' => A FORM FOR RADIATIVE RECOMB.
C                             'B' => B FORM FOR RADIATIVE RECOMB.
C  OUTPUT: (C*1)   CSELD    = 'A' => A FORM FOR DIELECTRONIC RECOMB.
C                             'B' => B FORM FOR DIELECTRONIC RECOMB.
C  OUTPUT: (C*1)   CSELS    = 'A' => A FORM FOR COLLISIONAL IONIS.
C                             'B' => B FORM FOR COLLISIONAL IONIS.
C  OUTPUT: (C*1)   CSELP    = 'A' => A FORM FOR TOTAL LINE POWER
C                             'B' => B FORM FOR TOTAL LINE POWER
C  OUTPUT: (C*1)   CSELL    = 'A' => A FORM FOR SPECIFIC LINE POWER
C                             'B' => B FORM FOR SPECIFIC LINE POWER
C  OUTPUT: (L*4)   LTADJ    = .TRUE. => ADJUST PARMS FROM SPECIAL TABLES
C                             .FALSE.=> DO NOT ADJUST PARMS FROM TABLES
C  OUTPUT: (R*8)  PTOT(,)   = TOTAL ZERO-DENS. RAD. POWER FOR EACH META.
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  FMIN      =  MINIMIUM PERMITTED OSCILLATOR STRENGTH
C  INPUT : (R*8)    Z0      = NUCLEAR CHARGE, COPASE FILE 1
C  INPUT : (R*8)    Z1      = RECOMBINING ION CHARGE, COPASE FILE 1
C  INPUT : (R*8)    Z02     = NUCLEAR CHARGE, COPASE FILE 2
C  INPUT : (R*8)    Z12     = RECOMBINING ION CHARGE, COPASE FILE 2
C
C  OUTPUT: (I*4)  ICTM()    = NUMBER OF INCLUDED TRANSITIONS FOR EACH
C                             METASTABLE
C                             1ST DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  IUMA(,)   = INDEX OF METASTABLE ASSIGNED TRANSITION
C                             IN FULL ELECTRON COLL. TRANSITION LIST.
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (C*22) CSTGMA(,) = STRING IDENTIFIER FOR INCLUDED TRANSITION
C                             CONTAINING J,I,FIJ,WVLN
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  FFMA(,)   = OSCILLATOR STRENGTH OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  WVMA(,)   = WAVELENGTH (A)  OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (R*8)  GBMA(,,)  = GBAR FOR ALLLOWED TRANSITIONS
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: INCLUDED ALLOWED TRANS. INDEX
C                             3RD DIM: METASTABLE INDEX
C  OUTPUT: (R*8)  PYMA(,,)  = VAN REGEMORTER P FOR ALLOWED TRANSITIONS
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: INCLUDED ALLOWED TRANS. INDEX
C                             3RD DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  IMALOCA(,)= BUNDLE ALLOCATION OF ALLOWED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  ISPECA()  = SPECIFIC LINE INDEX IN ALLOWED TRANSITIONS
C                             2ND DIM: METASTABLE INDEX
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D7BNDL     ADAS      GETS ALLOWED TRANSITIONS FOR METASTABLES
C          XXFLSH     ADAS      FLUSHES BUFFER
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    19/05/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED COMMA FROM FUNCTION PARAMETERS. STRATHCLYDE WAS
C	      SENSITIVE TO THIS, BUT RAL WAS NOT
C
C VERSION: 1.3				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    TOOK OUT R*8 Z0,Z1... VARIABLES
C
C VERSION: 1.4				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    PUT Z1 BACK
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IPAN       , NMET       , NPMET   ,
     &           NDLEV      , NDTRN      , NDTEM   , NDMET   ,
     &           NDOSC      , NDBNDL     , ICNTE      ,
     &           IL         , IPL        , NV      , IMET    ,
     &           IFOUT      , MAXT       ,
     &           ITSEL	    , I4UNIT
C-----------------------------------------------------------------------
      CHARACTER  CSELR*1 , CSELD*1 , CSELS*1 , CSELP*1 , CSELL*1
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      LOGICAL    LPEND      , LTADJ
C-----------------------------------------------------------------------
      INTEGER    ICTM(NDMET) , IUMA(NDOSC,NDMET), IMALOCA(NDOSC,NDMET)
      INTEGER    ISPECA(NDMET)
      INTEGER    IMETR(NDMET), IPMETR(NDMET)
      INTEGER    IETRN(NDTRN)
      INTEGER    IE1A(NDTRN) , IE2A(NDTRN)
      INTEGER    ILOGIC      , I      , J       , K      , IBUNDLE
C-----------------------------------------------------------------------
C REPLACED ALL DIMENSIONS OF SIZE 14 WITH NDTEM
      REAL*8     TSCEF(NDTEM,3)          , SCOM(NDTEM,NDTRN)
      REAL*8     PTOT(NDTEM,NDMET)
      REAL*8     XJA(NDLEV)           , WA(NDLEV)
      REAL*8     FMIN      , Z1
      REAL*8     AA(NDTRN)
      REAL*8     FFMA(NDOSC,NDMET)    , WVMA(NDOSC,NDMET)
      REAL*8     GBMA(NDTEM,NDOSC,NDMET) , PYMA(NDTEM,NDOSC,NDMET)
C-----------------------------------------------------------------------
      CHARACTER  CSTGMA(NDOSC,NDMET)*22
C-----------------------------------------------------------------------
      INTEGER PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C

C
C-----------------------------------------------------------------------
C WRITE THE INPUT PARAMETERS TO THE IDL PIPE
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)NDLEV
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDTRN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDTEM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDMET
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDOSC
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDBNDL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)IL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)IPL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NV
      CALL XXFLSH(PIPEOU)

      DO 1,J=1,3
         DO 2,I=1,NV
            WRITE(PIPEOU,*)TSCEF(I,J)
            CALL XXFLSH(PIPEOU)
 2       CONTINUE
 1    CONTINUE

      WRITE(PIPEOU,*)ICNTE
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C READ PIPE TO SEE IF BUNDLE BUTTON HAS BEEN PRESSED
C-----------------------------------------------------------------------
C

 100  READ(PIPEIN,*)IBUNDLE
      IF(IBUNDLE.NE.1) GOTO 200
C     
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C IF SO, IDENTIFY AND SELECT PRINCIPAL ALLOWED TRANSITIONS FOR EACH METASTABLE
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C

      CALL D7BNDL( NDLEV , NDTRN , NDMET , NDOSC ,
     &     IL    , WA    , XJA   , NV    , TSCEF ,
     &     ICNTE , IETRN , IE1A  , IE2A  , AA    , SCOM ,
     &     NMET  , IMETR , PTOT  , FMIN  , Z1    ,
     &     ICTM  , IUMA  , CSTGMA,
     &     FFMA  , WVMA  , GBMA  , PYMA
     &     )
      
      
      GOTO 100
C-----------------------------------------------------------------------
C READ FROM THE IDL PIPE AFTER THE USER HAS FINISHED
C-----------------------------------------------------------------------

 200  READ(PIPEIN,*)ILOGIC

      IF(ILOGIC .EQ. 0) THEN
         LPEND = .FALSE.
      ELSE
         LPEND = .TRUE.
      ENDIF
      READ(PIPEIN,'(A40)')TITLE
      READ(PIPEIN,*)IFOUT
      READ(PIPEIN,*)ITSEL
      READ(PIPEIN,'(A1)')CSELR
      READ(PIPEIN,'(A1)')CSELD
      READ(PIPEIN,'(A1)')CSELS
      READ(PIPEIN,'(A1)')CSELP
      READ(PIPEIN,'(A1)')CSELL
      READ(PIPEIN,*)ILOGIC
      IF (ILOGIC .EQ. 0) THEN 
         LTADJ = .FALSE.
      ELSE 
         LTADJ = .TRUE.
      ENDIF
      DO 50 J=1,NDMET
	DO 51 I=1,NDOSC
           READ(PIPEIN,*)IMALOCA(I,J)
 51     CONTINUE
 50   CONTINUE
      READ(PIPEIN,*)(ISPECA(I),I=1,NDMET)
      READ(PIPEIN,*)NMET
      READ(PIPEIN,*)NPMET
      READ(PIPEIN,*)(IMETR(I),I=1,NDMET)
      READ(PIPEIN,*)(IPMETR(I),I=1,NDMET)
      READ(PIPEIN,*)IMET
      RETURN
      END
