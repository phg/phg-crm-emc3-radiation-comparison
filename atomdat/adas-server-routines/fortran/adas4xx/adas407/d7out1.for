C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7out1.for,v 1.7 2019/05/09 17:04:07 mog Exp $ Date $Date: 2019/05/09 17:04:07 $

      SUBROUTINE D7OUT1( IUNIT   ,
     &                   NDTEM   , NDMET   , NDOSC  , NDBNDL , NDORB ,
     &                   NDCONF  , NDTHET  ,
     &                   TITLED  , IZ0     , IZ     , IZ1    ,
     &                   NV      , TSCEF   ,
     &                   NMET    , IMETR   , NPMET   , IPMETR  ,
     &                   ISPRT   , ISPSYS  , NSYS   , TRMPRT , SPNFAC,
     &                   NORB    , VORB    , LEICHR ,
     &                   CSELPA  , CSELLA  , PTOT    , LSETP   ,
     &                   ICTM    , IUMA    , FFMA   , WVMA   , GBMA  ,
     &                   IBNDLA  , ITSELA  , IEFTA  ,
     &                   DEBNDL  , FBNDL   , GBNDL  ,
     &                   PIFIT   , PFIT    , PAPPRX ,
     &                   PNLIA   , PNLSA   , PNLFA  , NTHETA  , THETA ,
     &                   CI4     , IZETA4  , EIONA  , NZETA  , SAO   ,
     &                   NCUT    , N0A     , PARMR  ,
     &                   ALFRA   , ALFRA0  , ALFRAR ,
     &                   NTRANS  , ITYPEA  , N1A    , NCUTT  , PARMD ,
     &                   ADIELO  , ALFDA  , ALFPART
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D7OUT1 *******************
C
C  PURPOSE: OUTPUT OF MAIN RESULTS (PAREMETER GENERATION ANALYSIS)
C
C  CALLING PROGRAM: ADAS407
C
C  DATA:
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           WAVELENGTHS         : ANGSTROM
C           POWER COEFFICIENTS  : RYD CM3 S-1
C           TEMPERATURES        : KELVIN
C           RATE COEFFICIENTS   : CM3 SEC-1
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT FOR RESULTS
C
C  INPUT : (I*4)  NDTEM     = MAX. NUMBER OF TEMPERATURES ALLOWED
C  INPUT : (I*4)  NDMET     = MAX. NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NDOSC     = MAX. NUMBER OF OSCILLATOR STRENGTHS
C  INPUT : (I*4)  NDBNDL    = MAX. NUMBER OF TRANSITION BUNDLING GROUPS
C  INPUT : (I*4)  NDORB     = MAX. NUMBER OF ORBITALS
C  INPUT : (I*4)  NDCONF    = MAX. NUMBER OF CONFIGURATIONS
C  INPUT : (I*4)  NDTHET    = MAX. NUMBER OF TEMPS. FOR MAINCL FILE
C
C  INPUT : (C*3)  TITLED    = ELEMENT SYMBOL.
C  INPUT : (I*4)  IZ0       = NUCLEAR CHARGE
C  INPUT : (I*4)  IZ        = RECOMBINED ION CHARGE
C  INPUT : (I*4)  IZ1       = RECOMBINING ION CHARGE
C
C  INPUT : (I*4)  NV        = NUMBER OF TEMPERATURES
C  INPUT : (R*8)  TSCEF     = ELECTRON TEMPERATURES (KELVIN,EV,REDUCED)
C  INPUT : (I*4)  NMET      = NUMBER OF METASTABLES
C  INPUT : (I*4)  IMETR()   = INDEX OF METASTABLES IN LEVEL LIST
C
C  INPUT : (I*4)  NPMET     = NUMBER OF PARENT METASTABLES (1<=NMET<=5)
C  INPUT : (I*4)  IPMETR()  = INDEX OF PARENT METASTABLES IN LEVEL LIST
C                             (ARRAY SIZE = 'NDMET' )
C
C  INPUT : (I*4)  ISPRT()   = RECOMBINING ION (PARENT) SPIN
C                             1ST DIM: PARENT INDEX
C  INPUT : (I*4)  ISPSYS(,) = RECOMBINED ION SPIN
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C  INPUT : (I*4)  NSYS()    = NUMBER OF SPIN SYSTEMS FOR RECOMBINED ION
C                             1ST DIM: PARENT INDEX
C  INPUT : (C*2)  TRMPRT()  = RECOMBINING ION METASTABLE (PARENT) TERM
C                             1ST DIM: PARENT INDEX
C  INPUT : (R*8)  SPNFAC(,)= SPIN WEIGHT FRACTION FOR PARENT/SPIN SYSTEM
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: RECOMBINED ION SPIN SYSTEM INDEX
C
C  INPUT : (I*4)  NORB      = NUMBER OF ELECTRON ORBITALS IN USE
C  INPUT : (R*8)  VORB()    = EFFECT. PRINC. QUANT. NO. FOR ORBITAL
C                             1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C  INPUT : (L*4)  LEICHR()  = .TRUE. => EISSNER ORBITAL USED
C                             .FALSE => EISSNER ORBITAL NOT USED
C
C  INPUT : (C*1)  CSELPA()  = 'A' => A FORM FOR TOTAL LINE POWER
C                             'B' => B FORM FOR TOTAL LINE POWER
C                             1ST DIM: METASTABLE INDEX
C  INPUT : (C*1)  CSELLA()  = 'A' => A FORM FOR SPECIFIC LINE POWER
C                             'B' => B FORM FOR SPECIFIC LINE POWER
C                             1ST DIM: METASTABLE INDEX
C  INPUT : (R*8)  PTOT(,)   = TOTAL ZERO-DENS. RAD. POWER FOR EACH META.
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (L*4)  LSETP()   = .TRUE.  => POWER ANALYSIS COMPLETED FOR
C                                        METASTABLE IMET
C                             .FALSE. => POWER ANALYSIS NOT COMPLETED
C                                        FOR METASTABLE IMET
C                             1ST DIM: METASTABLE INDEX
C
C  INPUT : (I*4)  ICTM()    = NUMBER OF INCLUDED TRANSITIONS FOR EACH
C                             METASTABLE
C                             1ST DIM: METASTABLE INDEX
C  INPUT : (I*4)  IUMA(,)   = INDEX OF METASTABLE ASSIGNED TRANSITION
C                             IN FULL ELECTRON COLL. TRANSITION LIST.
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (I*4)  FFMA(,)   = OSCILLATOR STRENGTH OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (I*4)  WVMA(,)   = WAVELENGTH (A)  OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  GBMA(,,)  = GBAR FOR ALLLOWED TRANSITIONS
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: INCLUDED ALLOWED TRANS. INDEX
C                             3RD DIM: METASTABLE INDEX
C  INPUT : (I*4)  IBNDLA()  = NUMBER OF TRANSITION BUNDLES
C                             1ST DIM: METASTABLE INDEX
C  INPUT : (I*4)  ITSELA()  = TEMPERATURE SELECTED FOR MATCHING
C                             1ST DIM: METASTABLE INDEX
C  INPUT : (I*4)  IEFTA(,)  = INCREASING BUNDLE TRANSITION ORDERING
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C
C  INPUT : (R*8)  DEBNDL(,) = MEAN TRANSITION ENERGY FOR BUNDLE (RYD)
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  FBNDL(,)  = SUMMED OSCILLATOR STRENGTH FOR BUNDLE
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  GBNDL(,)  = MEAN GAUNT (OR P) FACTOR FOR BUNDLE
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  PIFIT(,)  = RADIATED POWER INITIAL FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  PFIT(,)   = RADIATED POWER SIMPLE SCALE FIT AT ITSEL
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  PAPPRX(,) = RADIATED POWER OPTIMISED FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  PNLIA(,)  = INITIAL SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  PNLSA(,)  = SIMPLE  SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  PNLFA(,)  = SIMPLE  SCALING FACTOR FOR POWER FIT
C                             1ST DIM: BUNDLE INDEX
C                             2ND DIM: METASTABLE INDEX
C
C  INPUT : (I*4)  NTHETA    = NUMBER OF TEMPERATURES FOR MAINCL FILE
C  INPUT : (R*8)  THETA()   = Z-SCALED ELECTRON TEMPS. FOR MAINCL FILE
C
C  INPUT : (R*8)  CI4       = SCALE FACTOR FOR IONISATION FORMULA
C  INPUT : (I*4)  IZETA4(,,)= NO. OF ELECTRONS IN ORBITALS
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C                             3RD IND: SHELL INDEX
C  INPUT : (R*8)  EIONA(,,) = SET OF SCALED ORBITAL IONIS. POTENTIALS
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C                             3RD IND: SHELL INDEX
C  INPUT : (I*4)  NZETA(,)  = NO. OF OCCUPIED GROUND STATE ORBITALS
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C  INPUT : (R*8)  SAO(,,)   = BEST EXTIMATE OF METASTABLE AVERAGED
C                             IONISATION RATE
C                             1ST DIM: PARENT INDEX
C                             2ND IND: SPIN SYSTEM INDEX
C                             3RD IND: TEMPERATURE INDEX
C
C  INPUT : (I*4)  NCUT()    = N-SHELL AUTOIONISATION CUT-OFF FOR PARENT
C                             1ST DIM: PARENT INDEX
C  INPUT : (I*4)  N0A(,)    = LOWEST ALLOWED N-SHELL
C                             1ST DIM: PARENT INDEX
C                             2ND DIM: SPIN SYSTEM INDEX
C  INPUT : (R*4)  PARMR(,, )= PARAMETERS OF RADIATIVE RECOMBINATION
C                             APPROXIMATE FORMS
C                             1ST DIM: PARENT INDEX
C                             2ND DIM: SPIN SYSTEM INDEX
C                             3RD DIM: PARMS. 1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C
C  INPUT : (R*8)  ALFRA(,,)= TOTAL RADIATIVE RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  INPUT : (R*8)  ALFRA0(,,)=GROUND RADIATIVE RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: TEMPERATURE INDEX
C  INPUT : (R*8)  ALFRAR(,,)=EXCIT. RADIATIVE RECOMB. COEFFTS. (CM3 S-1)
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: TEMPERATURE INDEX
C
C  INPUT : (I*4)  NTRANS() = NUMBER OF DIPOLE TRANSITIONS FOR PARENT
C                            1ST DIM: PARENT INDEX
C  INPUT : (I*4)  ITYPEA(,)= TYPE OF DIELECTRONIC CORE TRANSITION
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  INPUT : (I*4)  N1A(,)   = LOWEST ALLOWED N-SHELL VIA DIELECTRONIC
C                            TRANSITION
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  INPUT : (I*4)  NCUTT()  = N-SHELL ALT. AUG. CUT-OFF FOR DIELECTRONIC
C                            TRANSITION
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: CONFIGURATION INDEX
C  INPUT : (R*4)  PARMD(,,)= PARAMETERS OF DIELECTRONIC RECOMBINATION
C                            APPROXIMATE FORMS
C                            1ST DIM: PARENT INDEX
C                            3RD DIM: PARMS.  1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C                                             5: EFF. N FOR LOWEST LEVEL
C                                             6: PHASE SPACE FACTOR
C                                             7: ENERGY DISPLACEMENT
C                                             8: SCALING MULTIPLIER
C                                             9: ENERGY DISPLACEMENT
C                                            10: SCALING MULTIPLIER
C
C
C          (I*4)  PGLEN   = PARAMETER = NUMBER OF LINES PER OUTPUT PAGE
C
C          (I*4)  NBLOCK  = NUMBER OF LINES IN CURRENT OUTPUT BLOCK.
C          (I*4)  NLINES  = LAST PAGE LINE WRITTEN.
C                           IF 'NLINES+NBLOCK' > 'PGLEN' START NEW PAGE.
C          (I*4)  MINT    = MINIMUM OF 10 AND 'NV'
C          (I*4)  I       = GENERAL USE
C          (I*4)  J       = GENERAL USE
C          (I*4)  K       = GENERAL USE
C          (I*4)  IT      = TEMPERATURE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IC      = GENERAL USE
C          (I*4)  IB      = GENERAL USE
C          (I*4)  IMET    = CURRENT METASTABLE INDEX
C          (I*4)  IPAR    = CURRENT PARENT INDEX
C          (I*4)  ISYS    = CURRENT SYSTEM INDEX
C          (I*4)  ITRANS  = CURRENT SYSTEM INDEX
C
C          (R*8)  Z1      = IZ1
C          (R*8)  Z2      = IZ1+1
C
C          (C*32) C32     = GENERAL USE 32 BYTE CHARACTER STRING
C
C
C NOTE:
C          ONLY THE FIRST TEN TEMPERATURES ARE OUTPUT.
C
C          AN 'OUTPUT BLOCK' IS A SINGLE CONTAINED OUTPUT TABLE
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSTNP     ADAS      STARTS NEW PAGE IF CURRENT PAGE FULL
C          I4NGRP     ADAS      GIVES PRINC. QUANTUM NUMBER OF ORBITAL
C          I4NGRP     ADAS      GIVES L QUANTUM NUMBER OF ORBITAL
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    15/06/94
C
C UPDATE: 23/08/94 - PAUL BRIDEN - CORRECT BUG: CI4 IS REAL*8 NOT INT*4
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C
C VERSION: 1.3				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    MINOR SYNTAX ERROR
C
C VERSION: 1.4                          DATE: 24-09-96
C MODIFIED: HUGH SUMMERS AND WILLIAM OSBORN
C           - REMOVED Z2*Z2 FACTOR ON PRINT OUT OF DIEL.
C             CORE TRANSITION ENERGIES
C
C VERSION: 1.5                          DATE: 22-11-2003
C MODIFIED: HUGH SUMMERS
C           - Increased EICHR Eissner strings to 61 (from 15).
C
C VERSION: 1.6                          DATE: 16-01-2004
C MODIFIED: Martin O'Mullane
C           - Trap NAN error in power percentage error. This caused
C             a failure with certain compiler options. It is not a
C             necessay output and the error prevented the adf03 
C             file being written.
C
C VERSION : 1.7
C DATE    : 08-05-2019
C MODIFIED: Martin O'Mullane
C           - Simplify the code to write the orbital energies.
C           - Work with orbital lists where the adf04 specification
C             now has hydrogenic ordering with 0.0 for orbitals
C             missing in the configurations.
C  
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   PGLEN
C-----------------------------------------------------------------------
      PARAMETER ( PGLEN = 63 )
C-----------------------------------------------------------------------
      INTEGER   IUNIT , i4unit
      INTEGER   I4NGRP, I4LGRP
      INTEGER   NDTEM , NDMET , NDOSC , NDBNDL, NDORB  , NDCONF  ,
     &          NDTHET,
     &          NORB  , NV    , NMET  , NPMET , IZ0    , IZ      , IZ1
      INTEGER   NBLOCK    , NLINES    , MINT   ,
     &          I         , J         , K      , IT    , IMET  , IPAR  ,
     &          ISYS      , IC        , IB     , ITRANS, NTHETA
C-----------------------------------------------------------------------
      REAL*8    WVNO      , WVLN      , DEIH   , DEEV
      REAL*8    TE1       , PER1      , TE2    , PER2
      REAL*8    Z1        , Z2
      REAL*8    CI4
C-----------------------------------------------------------------------
      CHARACTER C32*32    , TITLED*3
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)  , IPMETR(NDMET)
      INTEGER   ICTM(NDMET)   , IUMA(NDOSC,NDMET), IEFTA(NDOSC,NDMET)
      INTEGER   IBNDLA(NDMET) , ITSELA(NDMET)
      INTEGER   ISPRT(NDMET)  , ISPSYS(NDMET,2)  , NSYS(NDMET)
      INTEGER   IZETA4(NDMET,2,NDORB) , NZETA(NDMET,2)
      INTEGER   NCUT(NDMET)   , N0A(NDMET,2)
      INTEGER   NTRANS(NDMET), ITYPEA(NDMET,NDCONF) , N1A(NDMET,NDCONF),
     &          NCUTT(NDMET,NDCONF)
C-----------------------------------------------------------------------
      REAL*8    TSCEF(NDTEM,3)
      REAL*8    SPNFAC(NDMET,2)
      REAL*8    PTOT(NDTEM,NDMET)
      REAL*8    FFMA(NDOSC,NDMET)    , WVMA(NDOSC,NDMET)   ,
     &          GBMA(NDTEM,NDOSC,NDMET)
      REAL*8    DEBNDL(NDBNDL,NDMET) , FBNDL(NDBNDL,NDMET) ,
     &          GBNDL(NDBNDL,NDMET)
      REAL*8    PNLIA(NDBNDL,NDMET)  , PNLSA(NDBNDL,NDMET) ,
     &          PNLFA(NDBNDL,NDMET)
      REAL*8    PIFIT(NDTEM,NDMET)   , PFIT(NDTEM,NDMET)   ,
     &          PAPPRX(NDTEM,NDMET)
      REAL*8    VORB(NDORB)
      REAL*8    EIONA(NDMET,2,NDORB)
      REAL*8    THETA(NDTHET) , SAO(NDMET,2,NDTHET)
      REAL*8    PARMR(NDMET,2,4)
      REAL*8    ALFRA(NDMET,2,NDTHET)     , ALFRA0(NDMET,2,NDTHET)    ,
     &          ALFRAR(NDMET,2,NDTHET)
      REAL*8    PARMD(NDMET,10,NDCONF)
      REAL*8    ALFDA(NDMET,NDTHET)    , ALFPART(NDMET,NDCONF,NDTHET)
      REAL*8    ADIELO(NDMET,2,NDTHET)
C-----------------------------------------------------------------------
      LOGICAL   LSETP(NDMET)         , LEICHR(NDORB)
C-----------------------------------------------------------------------
      CHARACTER TRMPRT(NDMET)*2
      CHARACTER CSELPA(NDMET)*1      , CSELLA(NDMET)*1
C
      CHARACTER EICHR(61)
C-----------------------------------------------------------------------
      DATA EICHR /'1','2','3','4','5','6','7','8','9','A','B',
     &           'C','D','E','F','G','H','I','J','K','L','M',
     &           'N','O','P','Q','R','S','T','U','V','W','X',
     &           'Y','Z','a','b','c','d','e','f','g','h','i',
     &           'j','k','l','m','n','o','p','q','r','s','t',
     &           'u','v','w','x','y','z'/
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      Z1=IZ1
      Z2=IZ1+1
C-----------------------------------------------------------------------
C  OUTPUT PARENT/SPIN SYSTEM ASSIGNMENTS
C-----------------------------------------------------------------------
      WRITE(IUNIT,1012) TITLED , IZ

      DO 6 IPAR = 1,NPMET
        DO 4 ISYS = 1,NSYS(IPAR)
          WRITE(IUNIT,1013)IPAR,IPMETR(IPAR),TRMPRT(IPAR),ISPRT(IPAR),
     &                     ISYS,ISPSYS(IPAR,ISYS),SPNFAC(IPAR,ISYS)
    4   CONTINUE
    6 CONTINUE
C-----------------------------------------------------------------------
C  OUTPUT ORBITAL ASSIGNMENTS AND ENERGIES
C-----------------------------------------------------------------------
      WRITE(IUNIT,1014) TITLED , IZ
      do i = 1, norb
         if (leichr(i)) then
             write(iunit,1015)i, eichr(i), i4ngrp(eichr(i)), 
     &            i4lgrp(eichr(i)),vorb(i)
         endif
      end do
      
C       IC = 1
C       DO 8 I = 1,NORB
C     7   IF(.NOT.LEICHR(IC))THEN
C             IC=IC+1
C             GO TO 7
C         ENDIF
C         WRITE(IUNIT,1015)I,EICHR(IC),I4NGRP(EICHR(IC)),
C      &     I4LGRP(EICHR(IC)), VORB(I)
C         IC=IC+1
C     8 CONTINUE
C-----------------------------------------------------------------------
      MINT=MIN0(10,NV)
C
      DO 100 IMET = 1 , NMET
C
       IF (LSETP(IMET)) THEN
          WRITE(IUNIT,1002)IMET, IMETR(IMET)
C-----------------------------------------------------------------------
C  OUTPUT GBAR LISTINGS FOR METASTABLE
C-----------------------------------------------------------------------
          WRITE(IUNIT,1003)(TSCEF(IT,1),IT=1,MINT)
          WRITE(IUNIT,1004)
          DO 10 IC=1,ICTM(IMET)
           WVLN=WVMA(IC,IMET)
           WRITE(IUNIT,1005)IMET,IUMA(IC,IMET),FFMA(IC,IMET),
     &                   WVLN,IEFTA(IC,IMET),
     &                   (GBMA(IT,IC,IMET),IT=1,MINT)
   10     CONTINUE
C
C-----------------------------------------------------------------------
C  OUTPUT POWER FITTINGS FOR METASTABLE
C-----------------------------------------------------------------------
C
          WRITE(IUNIT,1006)IBNDLA(IMET),CSELPA(IMET),
     &                     ITSELA(IMET),(TSCEF(IT,1),IT=1,MINT)
          WRITE(IUNIT,1007)
C
          WRITE(IUNIT,1008)'EXACT VALUE ',(PTOT(IT,IMET),IT=1,MINT)
          WRITE(IUNIT,1008)'INITIAL FIT ',(PIFIT(IT,IMET),IT=1,MINT)
          WRITE(IUNIT,1008)'SIMPLE FIT  ',(PFIT(IT,IMET),IT=1,MINT)
          WRITE(IUNIT,1008)'OPTIMUM FIT ',(PAPPRX(IT,IMET),IT=1,MINT)
C
          do it=mint-1, 1, -1
            if (ptot(it,imet).NE.0.0D0) then
               TE1=TSCEF(it,1)/11605.0
               PER1=(PFIT(it,IMET)-PTOT(it,IMET))/PTOT(it,IMET)
               PER1=100.0*PER1
            endif
          end do
          TE2=TSCEF(MINT,1)/11605.0
          PER2=(PFIT(MINT,IMET)-PTOT(MINT,IMET))/PTOT(MINT,IMET)
          PER2=100.0*PER2
C
          IF (CSELPA(IMET).EQ.'A') THEN
             WRITE(IUNIT,1011)TE1,PER1,TE2,PER2
             DO 15 IB=1,IBNDLA(IMET)
               WRITE(IUNIT,1010)IB,DEBNDL(IB,IMET)*13.6048,
     &                      FBNDL(IB,IMET),
     &                      GBNDL(IB,IMET),PNLIA(IB,IMET),
     &                      PNLSA(IB,IMET),PNLFA(IB,IMET),
     &                      GBNDL(IB,IMET)*PNLSA(IB,IMET)
   15        CONTINUE
          ELSE
             WRITE(IUNIT,1009)TE1,PER1,TE2,PER2
             DO 20 IB=1,IBNDLA(IMET)
               WRITE(IUNIT,1010)IB,DEBNDL(IB,IMET)*13.6048,
     &                      FBNDL(IB,IMET),
     &                      GBNDL(IB,IMET),PNLIA(IB,IMET),
     &                      PNLSA(IB,IMET),PNLFA(IB,IMET),
     &                      GBNDL(IB,IMET)*PNLSA(IB,IMET)
   20        CONTINUE
          ENDIF
C
       ENDIF
C
  100 CONTINUE
C
C-----------------------------------------------------------------------
C  OUTPUT COLLISIONAL IONISATION PARAMETERS
C-----------------------------------------------------------------------
C
      MINT = MIN0(10,NTHETA)
C
      WRITE(IUNIT,1016) TITLED , IZ , TITLED , IZ+1
C
      DO 130 IPAR = 1,NPMET
        DO 120 ISYS = 1,NSYS(IPAR)
          DO 110 I = 1,NZETA(IPAR,ISYS)
            IF(I.EQ.1) THEN
               WRITE(IUNIT,1017)IPAR,ISYS,CI4,
     &                          I,Z1*Z1*EIONA(IPAR,ISYS,I),
     &                          IZETA4(IPAR,ISYS,I)
            ELSE
               WRITE(IUNIT,1018)I,Z1*Z1*EIONA(IPAR,ISYS,I),
     &                          IZETA4(IPAR,ISYS,I)
            ENDIF
  110     CONTINUE
  120   CONTINUE
  130 CONTINUE
C
      WRITE(IUNIT,1024)(Z1*Z1*THETA(IT),IT=1,MINT)
      WRITE(IUNIT,1025)
C
      DO 134 IPAR = 1,NPMET
        DO 132 ISYS = 1,NSYS(IPAR)
          WRITE(IUNIT,1026)IPAR,ISYS,(SAO(IPAR,ISYS,IT),IT=1,MINT)
  132   CONTINUE
  134 CONTINUE
C
C-----------------------------------------------------------------------
C  OUTPUT RADIATIVE RECOMBINATION PARAMETERS
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1019) TITLED , IZ+1 , TITLED , IZ
C
      DO 150 IPAR = 1,NPMET
        DO 140 ISYS = 1,NSYS(IPAR)
          WRITE(IUNIT,1020)IPAR,ISYS,N0A(IPAR,ISYS),
     &                     (PARMR(IPAR,ISYS,I),I=1,4),NCUT(IPAR)
  140   CONTINUE
  150 CONTINUE
C
      WRITE(IUNIT,1027)(Z1*Z1*THETA(IT),IT=1,MINT)
      WRITE(IUNIT,1028)
C
      DO 154 IPAR = 1,NPMET
        DO 152 ISYS = 1,NSYS(IPAR)
          WRITE(IUNIT,1029)IPAR,ISYS,'N=NMIN *',
     &                     (ALFRA0(IPAR,ISYS,IT),IT=1,MINT)
          WRITE(IUNIT,1029)IPAR,ISYS,'N>NMIN *',
     &                     (ALFRAR(IPAR,ISYS,IT),IT=1,MINT)
          WRITE(IUNIT,1029)IPAR,ISYS,'TOTAL   ',
     &                     (ALFRA(IPAR,ISYS,IT),IT=1,MINT)
  152   CONTINUE
  154 CONTINUE
C
C-----------------------------------------------------------------------
C  OUTPUT DIELECTRONIC RECOMBINATION PARAMETERS
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1021) TITLED , IZ+1 , TITLED , IZ
C
       DO 170 IPAR=1,NPMET
         DO 160 ITRANS=1,NTRANS(IPAR)
           IF(ITRANS.EQ.1) THEN
               WRITE(IUNIT,1022) IPAR,NTRANS(IPAR),
     &                           ITRANS,ITYPEA(IPAR,ITRANS),
     &                   N1A(IPAR,ITRANS),(PARMD(IPAR,K,ITRANS),K=1,2),
     &                   NCUTT(IPAR,ITRANS),PARMD(IPAR,3,ITRANS),
     &                   (PARMD(IPAR,K,ITRANS),K=4,7)
           ELSE
               WRITE(IUNIT,1023) ITRANS,ITYPEA(IPAR,ITRANS),
     &                   N1A(IPAR,ITRANS),(PARMD(IPAR,K,ITRANS),K=1,2),
     &                   NCUTT(IPAR,ITRANS),PARMD(IPAR,3,ITRANS),
     &                   (PARMD(IPAR,K,ITRANS),K=4,7)
           ENDIF
  160    CONTINUE
  170    CONTINUE
C
      WRITE(IUNIT,1030)(Z1*Z1*THETA(IT),IT=1,MINT)
      WRITE(IUNIT,1031)
C
      DO 176 IPAR = 1,NPMET
        DO 172 ITRANS = 1,NTRANS(IPAR)
          WRITE(IUNIT,1032)IPAR,ITRANS,'       *',
     &                     (ALFPART(IPAR,ITRANS,IT),IT=1,MINT)
  172   CONTINUE
        WRITE(IUNIT,1033)IPAR,'TOTAL  *',
     &                   (ALFDA(IPAR,IT),IT=1,MINT)
        DO 174 ISYS = 1,NSYS(IPAR)
          WRITE(IUNIT,1034)IPAR,ISYS,'        ',
     &                     (ADIELO(IPAR,ISYS,IT),IT=1,MINT)
  174   CONTINUE
  176 CONTINUE
C
      WRITE(IUNIT,1035)
C
      RETURN
C
C-----------------------------------------------------------------------
C
 1002 FORMAT( / / / ,' TRANSITION AND POWER ANALYSIS FOR METASTABLE  ',
     &       I2,5X,'LEVEL INDEX = ',I3//)
 1003 FORMAT( / ,54('-'),' TRANSITION ANALYSIS ',55('-')/
     &        2X,'I1',2x,'I2',4x,'OSCIL',5x,'WVLN',3x,'BNDL',2X,
     &        'TE(K)=',1P,10D9.2)
 1004 FORMAT( 2X,'  ',2X,'  ',4X,'STREN',5X,' (A)',3X,'INDX',10X,
     &           40('-'),' G-BAR ',41('-')/
     &        1X,130('-'))
 1005 FORMAT(1H ,I3,I4,F9.3,F10.1,3X,I2,7X,10F9.3)
 1006 FORMAT( / /
     &       1H ,'NUMBER OF TRANSITION BUNDLES  = ',I2/
     &       1H ,'TYPE OF FITTING APPROXIMATION =  ',A1/
     &       1H ,'TEMPERATURE INDEX FOR MATCH   = ',I2/
     &        / ,51('-'),' TOTAL LINE POWER ANALYSIS ',52('-')/
     &       1H ,2X,'APPROXIMATION',19X,'TE(K)=',1P,10D9.2)
 1007 FORMAT(43X, 27('-'),
     &       ' POWER COEFFICIENT (RYD CM3 S-1) ',
     &       28('-')/
     &       1X,130('-'))
 1008  FORMAT(1H ,2X,1A12,26X,1P,10D9.2)
 1009 FORMAT( / /
     &       1H ,'SCALING FOR ATOMPARS FILE     = ','SIMPLE FIT'/
     &       1H ,'ERROR AT',1P,D10.3,'            = ',0P,F7.2,'%'/
     &       1H ,'ERROR AT',1P,D10.3,'            = ',0P,F7.2,'%'/
     &        / ,56('-'),' SCALING SUMMARY ',57('-')/
     &       1H ,10X,'BNDL        DE-POW      F-POW      P-POW      ',
     &               'SCALE      SCALE      SCALE      P-SCLD     ',
     &               '    '/
     &       1H ,10X,'INDX        (eV)                             ',
     &               'INITIAL     SIMPLE     OPTIM.                ',
     &               '    '/
     &       1X,130('-'))
 1010 FORMAT(1H ,5X,I7,5X,7F11.3)
 1011 FORMAT( / /
     &       1H ,'SCALING FOR ATOMPARS FILE     = ','SIMPLE FIT'/
     &       1H ,'ERROR AT',1P,D10.3,' eV         = ',0P,F7.2,'%'/
     &       1H ,'ERROR AT',1P,D10.3,' eV         = ',0P,F7.2,'%'/
     &        / ,56('-'),' SCALING SUMMARY ',57('-')/
     &       1H ,10X,'BNDL        DE-POW      F-POW      G-POW      ',
     &               'SCALE      SCALE      SCALE      G-SCLD     ',
     &               '    '/
     &       1H ,10X,'INDX        (eV)                             ',
     &               'INITIAL     SIMPLE     OPTIM.                ',
     &               '    '/
     &       1X,130('-'))
 1012 FORMAT( / /
     &        / ,'SELECTED PARENT/SPIN SYSTEM COMBINATIONS FOR ',1A3,I2/
     &        / ,9('-'),' RECOMBINING ION ',25('-'),' RECOMBINED ION ',
     &       11('-')/
     &       2X,'PARENT',2X,'LEVEL ',2X,'PARENT',2X,'   PARENT   ',
     &                   9X,'SYSTEM',2X,'   SYSTEM   ',2X,'SPIN WT.' /
     &       2X,'INDEX ',2X,'INDEX ',2X,' TERM ',2X,'MULTIPLICITY',
     &                   9X,'INDEX ',2X,'MULTIPLICITY',2X,'FRACTION' /
     &       1X,78('-'))
 1013 FORMAT(1H ,2X,I2,6X,I2,7X,1A2,9X,I2,15X,I2,9X,I2,6X,F10.3)
 1014 FORMAT( / /
     &        / ,'ORBITAL ASSIGMENTS AND ENERGIES FOR ',1A3,I2/
     &        / ,53('-')/
     &       2X,'ORBITAL',2X,'EISSNER',2X,'  N   ',2X,'  L   ',
     &                               2X,' EFFECTIVE  ' /
     &       2X,' INDEX ',2X,'SYMBOL ',2X,'      ',2X,'      ',
     &                               2X,'PRINC.QU.NO.' /
     &       1X,53('-'))
 1015 FORMAT(1H ,3X,I2,7X,1A1,7X,I2,6X,I2,4X,F12.3)
 1016 FORMAT( / / / ,'COLLISIONAL IONISATION PARAMETERS FOR ',1A3,I2,
     &       ' TO ',1A3,I2/
     &        / ,64('-')/
     &       2X,'PARENT',2X,'SYSTEM',2X,'IONIS. GRP.',2X,'ORBITAL',2X,
     &          'ION. ENER.',2X,'  EQUIV. ' /
     &       2X,'INDEX ',2X,'INDEX ',2X,'MULTIPLIER ',2X,' INDEX ',2X,
     &          '  (RYD)   ',2X,'ELECTRONS' /
     &       1X,64('-'))
 1017 FORMAT(1H , 3X,I2,6X,I2,4X,F10.5,4X,I2,4X,F12.3,5X,I2)
 1018 FORMAT(1H , 31X,I2,4X,F12.3,5X,I2)
 1019 FORMAT( / /
     &        / ,'RADIATIVE RECOMBINATION PARAMETERS FOR ',1A3,I2,
     &       ' TO ',1A3,I2/
     &        / ,87('-')/
     &       2X,'PARENT',2X,'SYSTEM',2X,'LOWEST',2X,'EFFECTIVE',
     &           2X,'PHASE SPACE',2X,'ENER. DISP.',6X,'SCALE ',4X,
     &              'CUT-OFF'/
     &       2X,'INDEX ',2X,'INDEX ',2X,'   N  ',2X,'    N    ',
     &           2X,'  FACTOR   ',2X,'   (RYD)   ',6X,'FACTOR',4X,
     &              '   N   '/
     &       1X,87('-'))
 1020 FORMAT(1H ,3X,I2,6X,I2,6X,I2,3X,F10.5,3X,F10.3,3X,F10.3,
     &           2X,F10.3,5X,I4)
 1021 FORMAT( / /
     &        / ,'DIELECTRONIC RECOMBINATION PARAMETERS FOR ',1A3,I2,
     &       ' TO ',1A3,I2/
     &        / ,131('-')/
     &       2X,'PARENT',2X,'NO. OF',2X,'TRANS.',2X,'TRANS.',2X,
     &          'LOWEST',2X,'EFFECTIVE',2X,'PHASE SP.',2X,'CUT-OFF',
     &           2X,'  EIJ   ',4X,'  FIJ   ',2X,'ENER. DISP.',4X,
     &           'SCALE ',2X,'BETHE COR.'/
     &       2X,'INDEX ',2X,'TRANS.',2X,'INDEX ',2X,' TYPE ',2X,
     &          '   N  ',2X,'    N    ',2X,' FACTOR  ',2X,'   N   ',
     &           2X,' (RYD)  ',4X,'        ',2X,'   (RYD)   ',4X,
     &              'FACTOR',2X,'  FACTOR  '/
     &       1X,131('-'))
 1022 FORMAT(1H ,3X,I2,6X,I2,6X,I2,6X,I2,5X,I2,3X,F10.5,1X,F10.3,3X,I4,
     &           2X,F10.5,2X,F10.5,2X,F10.5,2X,F10.5,2X,F10.5)
 1023 FORMAT(1H ,           19X,I2,6X,I2,5X,I2,3X,F10.5,1X,F10.3,3X,I4,
     &           2X,F10.5,2X,F10.5,2X,F10.5,2X,F10.5,2X,F10.5)
 1024 FORMAT( / ,130('-')/
     &       1H ,2X,'PARENT',2X,'SYSTEM',18X,'TE(K)=',1P,10D9.2)
 1025 FORMAT(1H ,2X,'INDEX ',2X,'INDEX ',26X, 27('-'),
     &       ' IONISATION COEFFICIENT (CM3 S-1) ',
     &       27('-')/
     &       1X,130('-'))
 1026 FORMAT(1H ,3X,I2,6X,I2,27X,1P,10D9.2)
 1027 FORMAT( / ,130('-')/
     &       1H ,2X,'PARENT',2X,'SYSTEM',2X,'NOTES   ',8X,
     &       'TE(K)=',1P,10D9.2)
 1028 FORMAT(1H ,2X,'INDEX ',2X,'INDEX ',2X,'        ',16X, 26('-'),
     &       ' RECOMBINATION COEFFICIENT (CM3 S-1) ',
     &       25('-')/
     &       1X,130('-'))
 1029 FORMAT(1H ,3X,I2,6X,I2,4X,1A8,15X,1P,10D9.2)
 1030 FORMAT( / ,130('-')/
     &       1H ,2X,'PARENT',2X,'SYSTEM',2X,'TRANS.',2X,'NOTES   ',
     &       'TE(K)=',1P,10D9.2)
 1031 FORMAT(1H ,2X,'INDEX ',2X,'INDEX ',2X,'INDEX ',2X,'        ',8X,
     &       26('-'),' RECOMBINATION COEFFICIENT (CM3 S-1) ',
     &       25('-')/
     &       1X,130('-'))
 1032 FORMAT(1H ,3X,I2,6X,2X,6X,I2,4X,1A8,7X,1P,10D9.2)
 1033 FORMAT(1H ,3X,I2,6X,2X,6X,2X,4X,1A8,7X,1P,10D9.2)
 1034 FORMAT(1H ,3X,I2,6X,I2,6X,2X,4X,1A8,7X,1P,10D9.2)
 1035 FORMAT( / ,'NOTES:  * ==> EXCLUDES SPIN WT. FACTOR')
C
C-----------------------------------------------------------------------
C
      END
