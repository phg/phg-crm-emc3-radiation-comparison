C UNIX-IDL PORT - SCCS INFO: MODULE $Date: 2007/05/16 15:36:28 $ DATE @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/gpcalcx.for,v 1.3 2007/05/16 15:36:28 allan Exp $

       REAL*8 FUNCTION GPCALCX(XV0)                                     
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: ROUTINE TO PROVIDE BURGESS GENERAL PROGRAM RESULTS AT A
C  SERIES OF TEMPERATURES AND AT ZERO DENSITY.                           
C
C  EQUAL THE GENERAL FORMULA RESULTS AS FAR AS POSSIBLE BY MODIFICATION 
C  OF BETHE CORRECTIONS VIA A SINGLE SCALING PARAMETER CORFAC.          
C   THE CORRECTION FACTORS USED IN THE GENERAL PROGRAM                  
C  ARE OBTAINED BY ADJUSTMENT OF STANDARD SETS FOR SPECIFIC TYPES OF    
C  TRANSITION.  THE ADJUSTMENT IS                                       
C    (NEW COR(J))=EXP(-CORFAC/(L**DF+0.5))*(STANDARD COR(J)             
C  THE STANDARD COR'S ARE AS FOLLOWS:                                   
C   TYPE    TRANSITION                     COR'S                     DF 
C     1   NI=1,NJ>=2,LJ=LI+1:       0.05,0.30,0.50,0.90              2.0
C     2   NI=2,NJ=3,LJ=LI+1:        0.01,0.02,0.20,0.40,0.70,0.90    1.0
C     3   NI=2,NJ=3,LJ=LI-1:        0.01,0.01,0.01,0.08,0.30,0.70    1.0
C     4   NJ-NI=0, LJ=LI+1 :        0.30,0.35,0.40,0.45,0.70,0.90    0.5
C     5   NJ-NI=0, LJ=LI-1 :        0.30,0.35,0.40,0.45,0.70,0.90    0.5
C     6   NJ-NI>0, LJ=LI+1 :        0.01,0.02,0.20,0.40,0.70,0.90    1.0
C     7   NJ-NI>0, LJ=LI-1 :        0.01,0.01,0.01,0.08,0.30,0.70    1.0
C
C     (1)  INCLUDE NCUT AND EXTEND ARRAY SIZES
C     (2)  IMPLIMENTATION OF NCUT,LOW TEMPERATURE CHECK, CORRECTION
C          INVOLVING V1
C     (3)  NRAT INCREASED FROM 10 TO 15
C
C  ********** H.P. SUMMERS, JET             11 JUNE 1987  ************* 
C  ********** H.P. SUMMERS, JET     MOD.(1) 24 AUG  1989  ************* 
C  ********** W.J. DICKSON, JET     MOD.(2) 14 DEC  1989  *************
C  ********** P.E. BRIDEN ,TESSELLA MOD.(3) 23 AUG  1994  *************
C  INPUT                                                                
C      MAXT=NUMBER OF TEMPERATURES                                      
C      TEA(I)=ELECTRON TEMPERATURES (K)                                 
C      Z1=RECOMBINING ION CHARGE                                        
C      N0=LOWEST ACCESSIBLE N-SHELL BY RECOMBINATION                    
C      V0=EFFECTIVE PRINCIPAL QUANTUM NUMBER OF LOWEST ACCESSIBLE SHELL 
C      NI=LOWER PRINCIPAL QUANTUM NUMBER OF PARENT TRANSITION           
C      LI=LOWER ANGULAR QUANTUM NUMBER OF PARENT TRNASITION.            
C      WI=LOWER PARENT STATE STATISTICAL WEIGHT.                        
C      NJ=UPPER PRINCIPAL QUANTUM NUMBER OF PARENT TRANSITION           
C      LJ=UPPER ANGULAR QUANTUM NUMBER OF PARENT TRNASITION.            
C      WJ=UPPER PARENT STATE STATISTICAL WEIGHT.                        
C      EIJ=PARENT TRANSITION ENERGY (RYD)                               
C      FIJ=ABSORPTION OSCILLATOR STRENGTH OF PARENT TRANSITION          
C      EDISPG=UNIFORM ENERGY DISPLACEMENT FOR GENERAL FORMULA           
C      SCALEG=UNIFORM SCALING OF GENERAL FORMULA                        
C      PHFRAC=INITIAL ESTIMATE OF PHASE SPACE FACTOR                    
C      CORFAC=INITIAL ESTIMATE OF BETHE CORRECTION SCALER               
C      NCUT  =HIGH N CUT-OFF (APPLICABLE TO METASTABLE INITIAL STATES)  
C  OUTPUT                                                               
C      ALFO(I)=GENERAL PROGRAM DIELECTRONIC COEFFICIENTS (CM+3 SEC-1)   
C      PHFRAC=REVISED PHASE SPACE FACTOR                                
C      CORFAC=REVISED BETHE CORRECTION SCALER                           
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    19TH APRIL 1996
C
C VERSION: 1.1				DATE: 19-04-96
C MODIFIED: WILLIAM OSBORN
C	    - NRAT INCREASED FROM 15 TO 100 IN LINE WITH GPCALL
C
C VERSION: 1.2				DATE: 25-04-96
C MODIFIED: WILLIAM OSBORN
C	    - CONVERTED TO A FUNCTION FOR FMIN TO USE
C
C VERSION: 1.3				DATE: 16-05-07
C MODIFIED: Allan Whiteford
C	    - Old IBM statement labels in columns 73-80 removed. 
C	    - Updated comments as part of subroutine documentation
C             procedure.
C
C-----------------------------------------------------------------------
       PARAMETER(NRAT=100)
       DIMENSION TEA(NRAT),CORA(20,7),JCORA(7)
       DIMENSION DFA(7),INREPA(7),NREP(30)
       DIMENSION COR(20),CFAC(3),ALFO(NRAT),ALFDAT(NRAT)                
       DIMENSION ALFN1(NRAT),ALFN(NRAT),ALF(NRAT)                       
C
       COMMON /GPCOMM/ TEA,ALFDAT,ALFO,Z1,V0C,WI,WJ,EIJ,F,PHFRAC,       
     &                  CFAC1,MAXT,N0,NI,LI,NJ,LJ,NCUT,ITYPE,IOPT       
C
       DATA JCORA/20,20,20,20,20,20,20/                                 
       DATA DFA/2.0D0,1.0D0,1.0D0,0.5D0,0.5D0,1.0D0,1.0D0/              
       DATA (CORA(J,1),J=1,20)/0.05D0,0.3D0,0.5D0,0.9D0,16*1.0D0/       
       DATA (CORA(J,2),J=1,20)/0.01D0,0.02D0,0.20D0,0.40D0,0.70D0,0.90D0
     &,14*1.0D0/                                                        
       DATA (CORA(J,3),J=1,20)/0.01D0,0.01D0,0.01D0,0.08D0,0.30D0,0.70D0
     &,14*1.0D0/                                                        
       DATA (CORA(J,4),J=1,20)/0.30D0,0.35D0,0.40D0,0.45D0,0.70D0,0.90D0
     &,14*1.0D0/                                                        
       DATA (CORA(J,5),J=1,20)/0.30D0,0.35D0,0.40D0,0.45D0,0.70D0,0.90D0
     &,14*1.0D0/                                                        
       DATA (CORA(J,6),J=1,20)/0.01D0,0.02D0,0.20D0,0.40D0,0.70D0,0.90D0
     &,14*1.0D0/                                                        
       DATA (CORA(J,7),J=1,20)/0.01D0,0.01D0,0.01D0,0.08D0,0.30D0,0.70D0
     &,14*1.0D0/                                                        
       DATA INREPA/21,21,21,25,25,21,21/                                
       DATA NREP/1,2,3,4,5,6,7,8,9,10,12,15,20,30,40,50,60,70,100,150,  
     &200,250,300,400,500,600,700,800,900,1000/                         
C
       DO 105 IT=1,MAXT
            ALF(IT)=0.00D+00
           ALFO(IT)=0.00D+00
           ALFN(IT)=0.00D+00
          ALFN1(IT)=0.00D+00
  105  CONTINUE
       ZZ1=Z1*Z1                                                        
       Z2=Z1+1.0D0                                                      
       ZZ2=Z2*Z2                                                        
       EPSIJ=EIJ/ZZ2                                                    
       V0=XV0
C-----------------------------------------------------------------------
C  IDENTIFY TYPE OF PARENT TRANSITION                                   
C-----------------------------------------------------------------------
       ITYPE=1                                                          
       IF(NI.EQ.1.AND.NJ.GE.2.AND.LJ.EQ.LI+1)GO TO 50                   
       ITYPE=2                                                          
       IF(NI.EQ.2.AND.NJ.EQ.3.AND.LJ.EQ.LI+1)GO TO 50                   
       ITYPE=3                                                          
       IF(NI.EQ.2.AND.NJ.EQ.3.AND.LJ.EQ.LI-1)GO TO 50                   
       ITYPE=4                                                          
       IF(NI.EQ.NJ.AND.LJ.EQ.LI+1)GO TO 50                              
       ITYPE=5                                                          
       IF(NI.EQ.NJ.AND.LJ.EQ.LI-1)GO TO 50                              
       ITYPE=6                                                          
       IF(NI.LT.NJ.AND.LJ.EQ.LI+1)GO TO 50                              
       ITYPE=7                                                          
   50  DF=DFA(ITYPE)                                                    
C
C      WRITE(6,1200) N0,V0,Z1,NCUT,IOPT,ITYPE
C      WRITE(6,1201) NI,LI,WI,NJ,LJ,WJ
C      WRITE(6,1202) EIJ,F,PHFRAC,CFAC1
C      WRITE(7,1200) N0,V0,Z1,NCUT,IOPT,ITYPE
C      WRITE(7,1201) NI,LI,WI,NJ,LJ,WJ
C      WRITE(7,1202) EIJ,F,PHFRAC,CFAC1
C
       INREP=INREPA(ITYPE)
       JCOR=JCORA(ITYPE)
       DO 10 J=1,JCOR                                                   
       XL=J-1                                                           
   10  COR(J)=CORA(J,ITYPE)*DEXP(-CFAC1/(XL**DF+0.5D0))                 
       L=0                                                              
       DO 40 IT=1,MAXT                                                  
   40  ALF(IT)=0.0D0                                                    
C-----------------------------------------------------------------------
C      CALCULATION FOR FIRST TEN QUANTUM SHELLS
C-----------------------------------------------------------------------
       I0=0                                                             
   41  I0=I0+1                                                          
       IF(N0.GE.NREP(I0))GO TO 41                                       
       IF(I0.LT.10)I0=10                                                
       DO 100 N=N0,NREP(I0)                                             
       V=N                                                              
       IF(N.EQ.N0)V=V0                                                  
       DEFN=N-V                                                         
       T=0.0D0                                                          
       AD=0.0D0                                                         
       IF(N.LE.NCUT) THEN
        CALL GPDIEL(Z1,EPSIJ,F,T,COR,JCOR,N,DEFN,AD,ADU,L,CPT)          
C       WRITE(6,1000)N,I0,NREP(I0),AD                                   
C       WRITE(7,1000)N,I0,NREP(I0),AD                                   
       ENDIF
       DO 60 IT=1,MAXT                                                  
       ATE=1.5789D5/TEA(IT)                                             
       X=ATE*(ZZ1/(V*V)-ZZ2*EPSIJ)                                      
       FAC=8.0D0*(8.7972D-17*ATE)**1.5D0*1.57456D10*DEXP(X)*ZZ1*ZZ1     
       ALFN(IT)=FAC*AD                                                  
       IF(N.EQ.N0) THEN
           ALFN(IT) = PHFRAC*ALFN(IT)
       ENDIF
       ALF(IT)=ALF(IT)+ALFN(IT)                                         
C      WRITE(6,1006) ATE,X,N,V,ALFN(IT),ALF(IT)
C      WRITE(7,1006) ATE,X,N,V,ALFN(IT),ALF(IT)
   60  CONTINUE
  100  CONTINUE                                                         
C-----------------------------------------------------------------------
C      CALCULATION FOR REMAINING QUANTUM SHELLS
C-----------------------------------------------------------------------
       IF(NCUT.LT.11) THEN
         DO 107 IT=1,MAXT
         ALFN(IT)=0.0D0
  107    CONTINUE
       ELSE
       DO 109 I1=I0+1,INREP                                             
       N1=NREP(I1)                                                      
       V1=N1                                                            
       DEFN1=N1-V1                                                      
       AD1=0.0D0                                                        
       IF(N1.LE.NCUT) THEN
        CALL GPDIEL(Z1,EPSIJ,F,T,COR,JCOR,N1,DEFN1,AD1,ADU1,L,CPT)      
C       WRITE(6,1000)N1,I1,NREP(I1),AD1                                 
C       WRITE(7,1000)N1,I1,NREP(I1),AD1                                 
       DO 106 IT=1,MAXT                                                 
       ATE=1.5789D5/TEA(IT)                                             
C----CORRECTION: UNTIL 13/12/89  V USED INCORRECTLY INSTEAD OF V1
       X=ATE*(ZZ1/(V1*V1)-ZZ2*EPSIJ)                                    
C---------------------------------------------------------------------
       FAC=8.0D0*(8.7972D-17*ATE)**1.5D0*1.57456D10*DEXP(X)*ZZ1*ZZ1     
       IF(FAC.GT.1D-70) THEN
        ALFN1(IT)=FAC*AD1                                               
        B=-DLOG(ALFN(IT)/ALFN1(IT))/DLOG(V/V1)                          
        A=ALFN(IT)*V**B                                                 
        ALF(IT)=ALF(IT)+((2.0D0*V1+1.0D0-B)*ALFN1(IT)-(2.0D0*V+1.0D0-B) 
     &  *ALFN(IT))/(2.0D0-2.0D0*B)                                      
       ELSE
        ALFN1(IT)=0.0D0
        ALF(IT)=ALF(IT)
       ENDIF
C       WRITE(6,1001)ATE,X,FAC,V,V1,ALFN1(IT),ALFN(IT),ALF(IT),A,B      
C       WRITE(7,1001)ATE,X,FAC,V,V1,ALFN1(IT),ALFN(IT),ALF(IT),A,B      
  106  CONTINUE                                                         
       N=N1                                                             
       V=V1                                                             
       DO 108 IT=1,MAXT                                                 
  108  ALFN(IT)=ALFN1(IT)                                               
       ELSE
        DO 111 IT=1,MAXT                                                
  111   ALFN(IT)=0.00D+00                                               
       ENDIF
  109  CONTINUE                                                         
       ENDIF
C-----------------------------------------------------------------------
C      CALCULATION OF RESIDUAL
C-----------------------------------------------------------------------
       RDV1=0.0D0                                                       
       RDV2=0.0D0
       DO 150 IT=1,MAXT                                                 
       ALF(IT)=ALF(IT)+0.5D0*(NREP(INREP)-1)*ALFN(IT)                   
       ALFO(IT)=ALF(IT)                                                 
       IF(ALF(IT).LT.1.0D-50) THEN
         RDV2=0.0D0
       ELSE
         RDV2=(ALFDAT(IT)/ALF(IT)-1.0D0)**2
       ENDIF
C      WRITE(6,1205) TEA(IT),ALFDAT(IT),ALF(IT),RDV2
C      WRITE(7,1205) TEA(IT),ALFDAT(IT),ALF(IT),RDV2
  150  RDV1=RDV1+RDV2                                                   
       RDV=RDV1                                                         
C      WRITE(6,1206) RDV
C      WRITE(7,1206) RDV
C
       GPCALCX = RDV

       RETURN
C
 1000  FORMAT(1H ,'A ',3I5,1P,D12.4)                                    
 1001   FORMAT(1H ,'B ',1P,10D10.2)                                     
 1002  FORMAT(1H ,'C ',1P,3D12.4)                                       
 1003  FORMAT(1H ,'D ',1P,4D12.4)                                       
 1006  FORMAT(1H ,' C : ',1P,2D10.2,0P,I6,1P,3D10.2)
 1200  FORMAT(1H0,' N0= ',I4,' V0= ',F9.4,' Z1= ',F7.2,' NCT= ',I5,
     &    ' IOPT= ',I3,' ITYPE =  ',I3)
 1201  FORMAT(' NI= ',I3,' LI= ',I3,' WI= ',F3.0,' NJ= ',I3,' LJ= ',I3,
     &        ' WJ= ',F3.0)
 1202  FORMAT(' EIJ= ',F9.5,' FIJ= ',F9.5,' PHFRAC= ',
     &                F9.5,' CORFAC= ',F9.5)
 1205  FORMAT(1H ,' TE,ALFDAT,ALFGP,RDV2 = ',1P,4D10.2)
 1206 FORMAT(1H ,' RESIDUAL= ',F9.4)
 1300  FORMAT(1H ,'IT= ',1P,D12.3,' ALFO- ',D12.3)
C
      END                                                               
