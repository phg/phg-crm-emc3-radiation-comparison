C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7exps.for,v 1.8 2019/06/17 13:33:19 mog Exp $ Date $Date: 2019/06/17 13:33:19 $

      SUBROUTINE D7EXPS( NDMET   , NDCONF , NDTHET , NDORB  , NDLEV   ,
     &                   ndtrn   , ndqdn  , LTADJ  ,
     &                   IZ      , IZ0    , IZ1    ,
     &                   il      , ia     , isa    , ila    , xja     ,
     &                   cstrga  , wa     , bwno   ,
     &                   iorb    , lqdorb , qdorb  ,
     &                   ipl     , ipa    , ipsa   , ipla   , xpja    ,
     &                   cstrgpa , wpa    , bwnop  ,
     &                   icnte2  , ie1a2  , ie2a2  , aval2  ,
     &                   NTHETA  , THETA  , ITSELA , NPMET  , IPMETR  ,
     &                   ISPRT   , ISPSYS , NSYS   , INPAR  , ILPAR   ,
     &                   ENPAR   , TRMPRT , SPNFAC ,
     &                   NORB    , VORB   , LEICHR ,
     &                   CI4     , EIONA  , IZETA4 , NZETA  , SAO     ,
     &                   IONLEV  , XITRUE , NCUT   , N0A    , PARMR   ,
     &                   ALFRA   , ALFRA0 , ALFRAR , NCONFG , 
     &                   WVMIN   , WVMAX  ,
     &                   ECF     , FCF    , PCF    , WCF    , W       ,
     &                   NCF     , LCF    , NDCF   , LDCF   , NDMIN   ,
     &                   E       , DE0    , DE     , FM0    , FM      ,
     &                   IINAA   , IIPNAA , NCTAA  , NCTAAC , ECTAA   ,
     &                   NTRANS  , ITYPEA , N1A    , NCUTT  , PARMD   ,
     &                   EDISGP  , SCALGP , ADIELO , ALFDA  , ALFPART ,
     &                   LLINK   , ILINK  , LEISS  , NMET   , IMETR
     &                 )
      
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7EXPS *********************
C
C
C  PURPOSE:
C           (1) GENERATES APPROXIMATE FORM PARAMETERS AND NUMERICAL
C               VALUES FOR IONISATION AND RECOMBINATION RATES FROM
C               SPECIFIC ION FILES
C           (2) RETURNS DATA REQUIRED FOR A MAINCL INPUT FILE RESOLVED
C               INTO PARENT/SPIN SYSTEM COMPONENTS.
C           (3) RETURNS PARAMETERS REQUIRED FOR AN ATOMPARS FILE
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT NUMBER FOR SPECIFIC ION FILE FOR
C                            RECOMBINED ION
C  INPUT : (I*4)  IUNIT1   = UNIT NUMBER FOR SPECIFIC ION FILE FOR
C                            RECOMBINING ION
C  INPUT : (I*4)  NDMET    = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NDTHET   = MAXIMUM NUMBER OF TEMPS. FOR MAINCL FILE
C  INPUT : (I*4)  NDCONF   = MAXIMUM NUMBER OF CONFIGURATIONS ALLOWED
C  INPUT : (I*4)  NDORB    = MAXIMUM NUMBER OF ELECTRON ORBITALS
C
C  INPUT : (L*4)   LTADJ   = .TRUE. => ADJUST PARMS FROM SPECIAL TABLES
C                            .FALSE.=> DO NOT ADJUST PARMS FROM TABLES
C
C  INPUT : (I*4)  IZ       = RECOMBINED ION CHARGE
C  INPUT : (I*4)  IZ1      = RECOMBINING ION CHARGE
C
C  INPUT : (I*4)  NTHETA   = NUMBER OF TEMPERATURES FOR MAINCL FILE
C  INPUT : (R*8)  THETA()  = Z-SCALED TEMPERATURES FOR MAINCL FILE
C  INPUT : (I*4)  ITSELA() = TEMPERATURE INDEX FOR POWER MATCHING
C                            1ST IND: RECOMBINED ION METASTABLE INDEX
C
C  INPUT : (I*4)  NPMET    = NO. OF RECOMBINING ION (PARENT) METASTABLES
C  INPUT : (I*4)  IPMETR() = INDICES OF RECOMBINING ION (PARENT)
C                            METASTABLES IN LEVEL LIST
C  INPUT : (L*4)  LLINK(,,) = .TRUE.  => LINK EXISTS
C                             .FALSE. => NO LINK EXISTS
C                              1ST DIM: METASTABLE INDEX
C                              2ND DIM: PARENT METASTABLE INDEX
C                              3RD DIM: SPEN SYSTEM INDEX
C  INPUT : (L*4)  ILINK(,,) = DECIMAL ORBITAL INDEX FOR RECOMBINED
C                             ION ORBITAL DIFFERENCE WITH PARENT
C                              1ST DIM: METASTABLE INDEX
C                              2ND DIM: PARENT METASTABLE INDEX
C                              3RD DIM: SPEN SYSTEM INDEX
C  INPUT : (L*4)  LEISS    = .TRUE. => PARENTS AND METASTABLES FOUND
C                                       TO HAVE EISSNER CONFIG. FORMS
C                             .FALSE => NOT EISSNER CONFIG. FORMS
C  INPUT : (I*4)  NMET     = NUMBER OF METASTABLES (1 <= NMET <= 'NDMET')
C  INPUT : (I*4)  IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL LIST
C                           (ARRAY SIZE = 'NDMET' )
C
C  OUTPUT: (I*4)  ISPRT()  = RECOMBINING ION (PARENT) SPIN
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  ISPSYS(,)= RECOMBINED ION SPIN
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C  OUTPUT: (I*4)  NSYS()   = NUMBER OF SPIN SYSTEMS FOR RECOMBINED ION
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  INPAR()  = N QUANTUM NO. SUM FOR ELECTRONS OF PARENT
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  ILPAR()  = L QUANTUM NO. SUM FOR ELECTRONS OF PARENT
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (R*8)  ENPAR()  = RECOMBINING ION (PARENT) ENERGY
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (C*2)  TRMPRT() = RECOMBINING ION METASTABLE (PARENT) TERM
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  NCUT()   = N-SHELL AUTOIONISATION CUT-OFF FOR PARENT
C                            1ST DIM: PARENT INDEX
C  OUTPUT: (I*4)  N0A(,)   = LOWEST ALLOWED N-SHELL
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C  OUTPUT: (R*4)  PARMR(,,)= PARAMETERS OF RADIATIVE RECOMBINATION
C                            APPROXIMATE FORMS
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: SPIN SYSTEM INDEX
C                            3RD DIM: PARMS.  1: EFF. N FOR LOWEST LEVEL
C                                             2: PHASE SPACE FACTOR
C                                             3: ENERGY DISPLACEMENT
C                                             4: SCALING MULTIPLIER
C
C          (I*4)  ITDIMD   = PARAMETER = LIMIT NUMBER OF TEMPERATURES
C                                        INTRINSIC TO ROUTINE
C          (I*4)  IMDIMD   = PARAMETER = LIMIT NUMBER OF METASTABLES
C                                        INTRINSIC TO ROUTINE
C          (I*4)  IODIMD   = PARAMETER = LIMIT NUMBER OF ELEC. ORBITALS
C          (R*8)  SPNFAC(,)= SPIN WEIGHT FRACTION FOR PARENT/SPIN SYSTEM
C                            1ST DIM: PARENT INDEX
C                            2ND DIM: RECOMBINED ION SPIN SYSTEM INDEX
C          (R*8)  SPNSUM() = SPIN SYSTEM WEIGHT SUM BASED ON PARENT
C                            1ST DIM: PARENT INDEX
C          (I*4)  IGRPA()  = NUMBER OF ELECTRONS ALLOWED IN EACH SHELL
C                            1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C          (C*1)  EICHR()  = EISSNER NOTATION CHARACTER FOR ORBITAL
C                            1ST DIM: ORBITAL INDEX
C          (L*4)  LEICHR() = .TRUE. => EISSNER ORBITAL USED
C                            .FALSE => EISSNER ORBITAL NOT USED
C          (I*4)  KGRPA()  = NUMBER OF ELECTRONS IN EACH SHELL
C                            1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C          (R*8)  VORB()   = EFFECT. PRINC. QUANT. NO. FOR ORBITAL
C                            1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C          (R*8)  EPSIL()  = ENERGY OF ORBITAL (RYDBERG)
C                            1ST DIM: SHELL INDEX (1=1S, 2=2S ETC)
C          (R*8)  SAO(,,)  = BEST EXTIMATE OF METASTABLE AVERAGED
C                            IONISATION RATE
C                            1ST DIM: PARENT INDEX
C                            2ND IND: SPIN SYSTEM INDEX
C                            3RD IND: TEMPERATURE INDEX
C          (C*1)  CHLA()   = CONVERTS NUMERICAL VALUE FOR L QUANTUM
C                            TO CHARACTER VALUE (CAPITAL).  NOTE
C                            THAT L+1 (<11) IS THE CALL PARAMETER.
C          (I*4)  ILPRT()  = RECOMBINING ION (PARENT) TOTAL ORBITAL
C                            ANGULAR MOMENTUM
C                            1ST DIM: PARENT INDEX
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D7AUTS     ADAS      ANALYSES FOR IONISATION RATE PARAMETERS
C          D7ALFS     ADAS      ANALYSES FOR RECOMBINATION PARAMETERS
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    27/06/94
C
C UPDATE:  04/07/95 - HPS  CORRECTED ERROR BARE NUCLEUS CASE TO ENSURE
C                          NSHEL=1, NEL=1 CHEISA(1)='1' SET.
C                          NOTE:  INSUM, ILSUM ARE NOT SATISFACTORY IN
C                          THE BARE NUCLEUS CASE.  STILL TO RECONSIDER
C                          THE ALGORITM FOR DECIDING INNER CLOSED SHELLS
C                          BARE NUCLEUS MAKES A FALSE ASSUMPTION BUT
C                          WITHOUT SERIOUS CONSEQUENCE.
C UPDATE:  07/03/96 - HPS  REMOVED VALUE ASSIGNMENT OF NTHETA
C                          REMOVED VALUE ASSIGNMENT OF MAXDTA
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 14-05-96
C MODIFIED: WILLIAM OSBORN
C	    REARRANGED ARGUMENTS TO STAY UNDER
C	    LIMIT OF 20 CONTINUATION CHARACTERS AT ARCETRI AND GARCHING
C VERSION: 1.3				DATE: 20-08-96
C MODIFIED: HUGH SUMMERS + WILLIAM OSBORN
C	    - CORRECTED ASSIGNMENT OF 'TRMPRT'
C           - ADDED FOLLOWING TO CALL PARAMETERS
C             LLINK,ILINK,LEISS,NMET AND IMETR
C VERSION: 1.4				DATE: 24-09-96
C MODIFIED: HUGH SUMMERS + WILLIAM OSBORN
C	    - INTRODUCED ILPRT AND CORRECT OUTPUT TRMPRT
C
C
C VERSION : 1.5
C DATE    : 23-05-2003
C MODIFIED: Martin O'Mullane
C               - Pass through adf04 data for d7alfs and d7auts.
C               - Do not rewind files to get parent data; use 
C                 the new arguments.
C               - Make implicit none.
C               - Remove all unused variables and reduced length of
C                 parameter list.
C               - Remove redundant code and format statements.
C
C VERSION : 1.6
C DATE    : 04-11-2003
C MODIFIED: Hugh Summers
C               - checked iodimd consistency with passed ndorb.
C               - Extended igrpa eichr,chla
C               - corrected array indexing error in applying corrad
C                 to parmd(ipar,6,j)
C 
C VERSION : 1.7
C DATE    : 06-01-2004
C MODIFIED: Martin O'Mullane
C               - Remove redundant nia, lia, wia, nja, lja and wja
C                 variables.
C               - Pre-process configuration string with a new 
C                 routine (ceprep) to account for leading d10 and
C                 f10-f14 terms.
C               - Add error trapping code to check for overruns
C                 and index=0 errors.
C
C VERSION : 1.8
C DATE    : 18-07-2012
C MODIFIED: Martin O'Mullane
C               - Increase length of configuration strings to 
C                 40 characters.
C               - Read 7 rather than 6 shells from configurations.
C
C VERSION : 1.9
C DATE    : 17-06-2019
C MODIFIED: Martin O'Mullane
C               - Add a new argument, lqdorb, to the routine so 
C                 that it can be passed to d7auts.
C
C-----------------------------------------------------------------------
       INTEGER   ITDIMD , IMDIMD , IODIMD
C-----------------------------------------------------------------------
       PARAMETER (ITDIMD = 100 , IMDIMD = 10 , IODIMD = 61 )
C-----------------------------------------------------------------------
       INTEGER   NDMET    , NDCONF   , NDTHET    , NDORB   ,
     &           IZ       , IZ1      , NMET      , NTHETA
       integer   I        , I4JGRP   , I4LGRP    , I4NGRP  , 
     &           ILSUM    , INSUM    , IPAR      , IPRINT  , 
     &           ISHEL    , ISYS     , IT        , ITERM   , 
     &           J        , K        , LS        , LS0     , 
     &           NCONFG   , NEL      , NORB      , NPMET   ,
     &           NSHEL
       integer   il       , ipl      , ndlev     , ndtrn   ,
     &           iorb     , ndqdn    , iz0       , icnte2
       integer   i4unit
C-----------------------------------------------------------------------
       real*8    CI4      , CORRAD   , CORRAR    , CORRGA   , CORRS  , 
     &           EDISGP   , SCALGP
       real*8    bwno     , bwnop
C-----------------------------------------------------------------------
       LOGICAL   LTADJ    , LEISS
C-----------------------------------------------------------------------
       CHARACTER CSTGRP*2 , STRG6*41  , LABEL*40 , ceprep*41
C-----------------------------------------------------------------------
       INTEGER IMETR(NDMET)  , IPMETR(NDMET)   , ITSELA(NDMET)
       INTEGER ISPRT(NDMET)  , ISPSYS(NDMET,2) , NSYS(NDMET) ,
     &         INPAR(NDMET)  , ILPAR(NDMET)
       INTEGER IGRPA(IODIMD) , KGRPA(IODIMD)
       INTEGER NCUT(NDMET)   , N0A(NDMET,2)
       INTEGER IONLEV(NDMET,2)
       INTEGER NCF(NDMET,NDCONF)   , LCF(NDMET,NDCONF)   ,
     &         NDCF(NDMET,NDCONF)  , LDCF(NDMET,NDCONF)  ,
     &         IINAA(NDMET,NDCONF) , IIPNAA(NDMET,NDCONF),
     &         NCTAA(NDMET,NDCONF) , NCTAAC(NDMET,NDCONF)
       INTEGER NDMIN(NDMET)
       INTEGER NTRANS(NDMET), ITYPEA(NDMET,NDCONF) , N1A(NDMET,NDCONF) ,
     &         NCUTT(NDMET,NDCONF)
       INTEGER IZETA4(NDMET,2,NDORB) , NZETA(NDMET,2)
       INTEGER ILINK(NDMET,NDMET,2)
       INTEGER ILPRT(IMDIMD)
       integer NELA(7)      
       integer ia(ndlev)    , isa(ndlev)   , ila(ndlev)  ,
     &         ipa(ndlev)   , ipsa(ndlev)  , ipla(ndlev)
       integer ie1a2(ndtrn) , ie2a2(ndtrn)
C-----------------------------------------------------------------------
       REAL*8  SPNFAC(NDMET,2)  , SPNSUM(IMDIMD)
       REAL*8  VORB(NDORB)
       REAL*8  THETA(NDTHET)
       REAL*8  ENPAR(NDMET)      , PARMR(NDMET,2,4)
       REAL*8  SAO(NDMET,2,NDTHET)
       REAL*8  XITRUE(NDMET,2)
       REAL*8  WVMIN(NDMET,NDCONF) , WVMAX(NDMET,NDCONF) ,
     &         ECF(NDMET,NDCONF)   , FCF(NDMET,NDCONF)   ,
     &         PCF(NDMET,NDCONF)   , WCF(NDMET,NDCONF)   ,
     &         ECTAA(NDMET,NDCONF)
       REAL*8  W(NDMET)     , E(NDMET)   , FM0(NDMET)   , FM(NDMET)   ,
     &         DE0(NDMET)   , DE(NDMET)
       REAL*8  ALFRA(NDMET,2,NDTHET)     , ALFRA0(NDMET,2,NDTHET)     ,
     &         ALFRAR(NDMET,2,NDTHET)
       REAL*8  ALFDA(NDMET,NDTHET)       , ALFPART(NDMET,NDCONF,NDTHET)
       REAL*8  ADIELO(NDMET,2,NDTHET)
       REAL*8  PARMD(NDMET,10,NDCONF)
       REAL*8  EIONA(NDMET,2,NDORB)
       REAL*8  APGOA(ITDIMD) ,  APDOA(ITDIMD)
       real*8  wa(ndlev)     , wpa(ndlev), 
     &         xja(ndlev)    , xpja(ndlev)
       real*8  qdorb((ndqdn*(ndqdn+1))/2)
       real*8  aval2(ndtrn)    
C-----------------------------------------------------------------------
       LOGICAL LEICHR(NDORB)     , LLINK(NDMET,NDMET,2)
       logical lqdorb((ndqdn*(ndqdn+1))/2)
C-----------------------------------------------------------------------
       CHARACTER TRMPRT(NDMET)*2 , EICHR(IODIMD)*1,  CHLA(20)*1  , 
     &           CHEISA(7)*1
       character cstrga(ndlev)*40, cstrgpa(ndlev)*40
C-----------------------------------------------------------------------
       DATA IGRPA / 2 , 2 , 6 , 2 , 6 ,10 , 2 , 6 ,10 ,14 , 2 ,
     &              6 ,10 ,14 ,18 , 2 , 6 ,10 ,14 ,18 ,22 , 2 ,
     &              6 ,10 ,14 ,18 ,22 ,26 , 2 , 6 ,10 ,14 ,18 ,
     &             22 ,26 ,30 , 2 , 6 ,10 ,14 ,18 ,22 ,26 ,30 ,
     &             34 , 2 , 6 ,10 ,14 ,18 ,22 ,26 ,30 ,34 ,38 ,
     &              2 , 6 ,10 ,14 ,18 ,22 /
       DATA EICHR /'1','2','3','4','5','6','7','8','9','A','B',
     &             'C','D','E','F','G','H','I','J','K','L','M',
     &             'N','O','P','Q','R','S','T','U','V','W','X',
     &             'Y','Z','a','b','c','d','e','f','g','h','i',
     &             'j','k','l','m','n','o','p','q','r','s','t',
     &             'u','v','w','x','y','z'/
       DATA CHLA  /'S','P','D','F','G','H','I','J','K','L','M',
     &             'N','O','Q','R','T','U','V','W','Z'/
C-----------------------------------------------------------------------
      external ceprep
C-----------------------------------------------------------------------

       if (iodimd.ne.ndorb) then
           write(i4unit(-1),1003)'iodimd',iodimd,'ndorb',ndorb
           write(i4unit(-1),1004)
           stop
       endif

       if (imdimd.ne.ndmet) then
           write(i4unit(-1),1003)'imdimd',imdimd,'ndmet',ndmet
           write(i4unit(-1),1004)
           stop
       endif


C-----------------------------------------------------------------------
C  INITIALISE TEMPERATURES AND CONSTANTS
C-----------------------------------------------------------------------

       CI4    = 1.0D0
       NORB   = 0
       EDISGP = 0.0D0
       SCALGP = 1.0D0

       DO I=1,IODIMD
         KGRPA(I)  = 0
         VORB(I)   = 0.0D0
         LEICHR(I) = .FALSE.
       END DO

       DO I=1,NDCONF
         DO J=1,NDMET
           DO K=1,10
             PARMD(J,K,I)=0.0D0
           END DO
           NTRANS(J) = 0
         END DO
       END DO

       DO I=1,4
         DO J=1,NDMET
           DO K=1,2
             PARMR(J,K,I)=0.0D0
           END DO
         END DO
       END DO


C-----------------------------------------------------------------------
C REWIND RECOMBINING ION FILE AND GET DATA ON PARENTS AND SPIN SYSTEMS
C-----------------------------------------------------------------------


        do iterm = 1, ipl
        DO 111 IPAR=1,NPMET
          SPNSUM(IPAR)=0
          IF(IPA(ITERM).EQ.IPMETR(IPAR)) THEN
            ISPRT(IPAR)=IPSA(ITERM)
            ILPRT(IPAR)=IPLA(ITERM)
            ENPAR(IPAR)=WPA(ITERM)
            IF(IPSA(ITERM).EQ.1) THEN
              NSYS(IPAR)=1
              ISPSYS(IPAR,1)=2
            ELSEIF(IPSA(ITERM).GT.1) THEN
              NSYS(IPAR)=2
              ISPSYS(IPAR,1)=IPSA(ITERM)-1
              ISPSYS(IPAR,2)=IPSA(ITERM)+1
            ENDIF
           strg6 = ceprep(cstrgpa(iterm))
           READ(strg6,'(7(i2,a1))')(NELA(ISHEL),CHEISA(ISHEL),ISHEL=1,7)
           NSHEL=0
  114      CONTINUE
          if (nshel.LT.7) then
             IF(NELA(NSHEL+1).GT.0) THEN
               NSHEL=NSHEL+1
               NELA(NSHEL)=MOD(NELA(NSHEL),50)
               GO TO 114
             ENDIF
          endif
C------------------ADDITION   03/07/95 ---------------------------------
           IF(NSHEL.EQ.0) THEN
             NSHEL=1
             NELA(1)=0
             CHEISA(1)='1'
           ENDIF
C-----------------------------------------------------------------------
           INSUM=0
           ILSUM=0
           LS0=I4JGRP(CHEISA(1))
           IF(LS0.GT.1) THEN
             DO 113 LS=1,LS0-1
             NEL=IGRPA(LS)
             INSUM=INSUM+NEL*I4NGRP(EICHR(LS))
             ILSUM=ILSUM+NEL*I4LGRP(EICHR(LS))
  113        CONTINUE
           ENDIF
           DO 112 ISHEL=1,NSHEL
            INSUM=INSUM+NELA(ISHEL)*I4NGRP(CHEISA(ISHEL))
            ILSUM=ILSUM+NELA(ISHEL)*I4LGRP(CHEISA(ISHEL))
  112      CONTINUE
           INPAR(IPAR)=INSUM
           ILPAR(IPAR)=ILSUM

           IF(NSHEL.GT.0)THEN
C-------------------- CORRECTION 20/08/96 ----------------------------
               WRITE(TRMPRT(IPAR),'(I1,A1)')ISPRT(IPAR),
     &                                      CHLA(ILPRT(IPAR)+1)
C---------------------------------------------------------------------
C              TRMPRT(IPAR)=CSTGRP(CHEISA(NSHEL))
           ELSE
               TRMPRT(IPAR)='  '
           ENDIF
C
          ENDIF
  111  CONTINUE
       IF (IPA(ITERM).GE.IPMETR(NPMET)) goto 120
       end do
  120  continue


       do ipar=1,npmet
          do isys=1,nsys(ipar)
             spnsum(ipar)=spnsum(ipar)+ispsys(ipar,isys)
          end do
       end do

C-----------------------------------------------------------------------
C CALL SUBROUTINE D7AUTS FOR IONISATION RATES Z TO Z1
C-----------------------------------------------------------------------

       CALL D7AUTS( NDMET  , NDTHET , NDORB  , ndlev  , ndqdn  ,
     &              IODIMD ,
     &              IZ     , IZ0    , iz1    ,
     &              il     , ia     , isa    , ila    , xja    , 
     &              wa     , cstrga , bwno   , 
     &              iorb   , lqdorb , qdorb  ,
     &              NTHETA , THETA  ,
     &              NPMET  ,
     &              IGRPA  , EICHR  , LEICHR ,
     &              NSYS   , ISPSYS , INPAR  , ILPAR  , ENPAR  ,
     &              SAO    ,
     &              KGRPA  , IZETA4 , EIONA  , NZETA  ,
     &              IONLEV , XITRUE ,
     &              NORB   , VORB   ,
     &              N0A    , PARMR  , LLINK  , ILINK  , LEISS  ,
     &              NMET   , IMETR
     &            )

C-----------------------------------------------------------------------
C   CALL SUBROUTINE D7ALFS FOR RECOMBINATION RATES Z1 TO Z.
C-----------------------------------------------------------------------

       CALL D7ALFS( NDMET   , NDCONF , NDTHET , ndlev  , ndtrn  ,
     &              IODIMD  , NPMET  , IPMETR , IZ1    , IZ0    ,
     &              NTHETA  , THETA  , ITSELA(1)       ,
     &              IGRPA   , NORB   , VORB   , iz1+1  ,
     &              ipl     , ipa    , ipsa   , xpja   , wpa    ,
     &              cstrgpa , icnte2 , aval2  , ie1a2  , ie2a2  ,
     &              NCUT    , N0A    , PARMR  ,
     &              ALFRA   , ALFRA0 , ALFRAR , APGOA  ,
     &              NCONFG  , WVMIN  , WVMAX  ,
     &              ECF     , FCF    , PCF    , WCF    , W      ,
     &              NCF     , LCF    , NDCF   , LDCF   , NDMIN  ,
     &              E       , DE0    , DE     , FM0    , FM     ,
     &              IINAA   , IIPNAA , NCTAA  , NCTAAC , ECTAA  ,
     &              NTRANS  , ITYPEA , N1A    , NCUTT  , PARMD  ,
     &              ALFDA   , ALFPART, APDOA  ,
     &              KGRPA  , NSYS
     &            )
C-----------------------------------------------------------------------
       DO IPAR=1,NPMET
         DO ISYS=1,NSYS(IPAR)
          DO IT=1,NTHETA
            SPNFAC(IPAR,ISYS)   = DFLOAT(ISPSYS(IPAR,ISYS))/SPNSUM(IPAR)
            ALFRA(IPAR,ISYS,IT) = SPNFAC(IPAR,ISYS)*ALFRA(IPAR,ISYS,IT)
            ADIELO(IPAR,ISYS,IT)= SPNFAC(IPAR,ISYS)*ALFDA(IPAR,IT)
          END DO
         END DO
       END DO
C-----------------------------------------------------------------------
       IPRINT=-1
       IF(IPRINT.GT.0) GOTO 5000
C-----------------------------------------------------------------------
C   CALL SUBROUTINE D7CORS TO CORRECT OUTPUT TO BETTER DATA
C-----------------------------------------------------------------------
       IF(LTADJ) THEN
           CORRS=1.00D0
           CORRAD=1.00D0
           CORRAR=1.00D0
           CORRGA=1.00D0
           CALL D7CORS(IZ0,IZ,CORRS,CORRAD,CORRAR,CORRGA,LABEL)
           DO 97 IPAR=1,NPMET
             DO 96 ISYS=1,NSYS(IPAR)
               CI4=CORRS*CI4
               SCALGP=CORRAD*SCALGP
               DO 85 J=1,NTRANS(IPAR)
                 PARMD(IPAR,6,J)=CORRAD*PARMD(IPAR,6,J)
   85          CONTINUE
               PARMR(IPAR,ISYS,4)=CORRGA*PARMR(IPAR,ISYS,4)
               DO 95 IT=1,NTHETA
                 SAO(IPAR,ISYS,IT) = CORRS*SAO(IPAR,ISYS,IT)
                 ADIELO(IPAR,ISYS,IT) = CORRAD*ADIELO(IPAR,ISYS,IT)
                 ALFRA(IPAR,ISYS,IT) = CORRAR*ALFRA(IPAR,ISYS,IT)
   95          CONTINUE
   96        CONTINUE
   97      CONTINUE
       ENDIF

 5000  CONTINUE

       RETURN

C-----------------------------------------------------------------------
 1003 format(1x,32('*'),' d7exps error ',32('*')//
     &       1x,'internal parameter ',1a6, ' = ',i3,' ne ',1a6,' = ',i3)
 1004 format(/1x,27('*'),' program terminated   ',28('*'))
C-----------------------------------------------------------------------
       END
