C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7link.for,v 1.5 2017/09/28 09:15:13 mog Exp $ Date $Date: 2017/09/28 09:15:13 $
C UNIX-IDL PORT - SCCS INFO: MODULE @(#)d7link.for	1.2 DATE 02/27/98

      SUBROUTINE D7LINK( NDLEV   , NDMET  ,
     &                   NMET    , IMETR  , NPMET  , IPMETR ,
     &                   CSTRGA  , ISA    , ILA    , NALCM  , IALCM ,
     &                   ISALCM  ,
     &                   CSTRGPA , IPSA   , IPLA   , NALCP  , IALCP ,
     &                   ISALCP  ,
     &                   LLINK   , ILINK  , LEISS
     &                 )
      IMPLICIT NONE
C---------------------------------------------------------------------
C                                                             ********
C  ****************** FORTRAN 77 SUBROUTINE: D7LINK ******************
C
C  PURPOSE: RETURNS A TRUTH TABLE OF LINKS BETWEEN PARENTS AND
C           RECOMBINED ION METASTABLES FOR RADIATIVE RECOMBINATION
C           AND IONISATION.  ALSO SUPPLIES THE DECIMAL ORBITAL NUMBER
C           FOR THE POSITION OF THE SHELL OF THE RECOMBINED ELECTRON.
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT  :  (I*4)   NDLEV    = MAX. NUMBER OF LEVELS ALLOWED
C  INPUT  :  (I*4)   NDMET    = MAX. NO. OF METASTABLES ALLOWED
C  INPUT  :  (I*4)   NMET     = NUMBER OF METASTABLES (1<=NMET<=NDMET)
C  INPUT  :  (I*4)   IMETR()  = INDEX OF METASTABLE IN COMPLETE LEVEL
C                               LIST (ARRAY SIZE = 'NDMET' )
C  INPUT  :  (I*4)   NPMET    = NUMBER OF PARENT METASTABLES
C                               (1<=NPMET<=NDMET)
C  INPUT  :  (I*4)   IPMETR() = INDEX OF PARENT METASTABLES IN LEVEL
C                               LIST (ARRAY SIZE = 'NDMET' )
C  INPUT  :  (C*18)  CSTRGA() = CONFIGURATION (EISSNER FORM) FOR
C                               RECOMBINED ION LEVELS
C  INPUT  :  (I*4)   ILA()    = QUANTUM NUMBER (L) FOR LEVELS
C                               (RECOMNBINED  ION COPASE FILE)
C  INPUT  :  (I*4)   ISA()    = MULTIPLICITY FOR LEVELS
C                               (RECOMBINED ION COPASE FILE)
C                               NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT  :  (C*18)  CSTRGPA()= CONFIGURATION (EISSNER FORM) FOR
C                               RECOMBINING ION LEVELS
C  INPUT  :  (I*4)   IPLA()   = QUANTUM NUMBER (L) FOR LEVELS
C                               (RECOMBINING ION COPASE FILE)
C  INPUT  :  (I*4)   IPSA()   = MULTIPLICITY FOR LEVEL 'IA2()'
C                               (RECOMBINING ION COPASE FILE)
C                               NOTE: (IPSA-1)/2 = QUANTUM NUMBER (S)
C
C  OUTPUT :  (I*4)   NALCM    = NUMBER OF SPIN DISTINGUISED 
C                               METASTABLES
C  OUTPUT :  (I*4)   IALCM()  = INDEX OF ENERGY ORDERED SPIN 
C                               DISTINQUISHED METASTABLE
C                               1ST. DIM: METASTABLE INDEX  
C  OUTPUT :  (I*4)   ISALCM() = SPIN OF ENERGY ORDERED SPIN 
C                               DISTINQUISHED METASTABLE
C                               1ST. DIM: DISTINQUISHED METASTABLE INDEX  
C  OUTPUT :  (I*4)   NALCP      NUMBER OF SPIN DISTINQUISHED
C                               PARENTS 
C  OUTPUT :  (I*4)   IALCP()  = INDEX FOR ENERGY ORDERED SPIN 
C                               DISTINQUISHED PARENT
C                               1ST. DIM: PARENT INDEX  
C  OUTPUT :  (I*4)   ISALCP() = SPIN OF ENERGY ORDERED SPIN 
C                               DISTINQUISHED PARENT
C                               1ST. DIM: DISTINQUISHED PARENT INDEX  
C  OUTPUT :  (L*4)   LLINK(,,)= .TRUE.  => LINK EXISTS
C                               .FALSE. => NO LINK EXISTS
C                                1ST DIM: METASTABLE INDEX
C                                2ND DIM: PARENT METASTABLE INDEX
C                                3RD DIM: SPEN SYSTEM INDEX
C  OUTPUT :  (L*4)   ILINK(,,)= DECIMAL ORBITAL INDEX FOR RECOMBINED
C                               ION ORBITAL DIFFERENCE WITH PARENT
C                                1ST DIM: METASTABLE INDEX
C                                2ND DIM: PARENT METASTABLE INDEX
C                                3RD DIM: SPEN SYSTEM INDEX
C  OUTPUT :  (L*4)   LEISS    = .TRUE.  => ALL CONFIGS. EISSNER FORM
C                               .FALSE. => NOT ALL CONFIGS. EISSNER
C
C            (I*4)   NOCCUM() = OCCUPANCY FOR EACH DECIMAL ORBITAL
C                               INDEX 1-15 OF METASTABLE
C            (I*4)   NOCCUP() = OCCUPANCY FOR EACH DECIMAL ORBITAL
C                               INDEX 1-15 OF PARENT
C
C            (I*4)   I      = GENERAL INDEX
C            (I*4)   J      = GENERAL INDEX
C            (I*4)   IM     = GENERAL INDEX
C            (I*4)   IPAR   = GENERAL INDEX
C            (I*4)   IORBIT = CURRENT ORBITAL INDEX
C            (L*4)   LMATCH = GENERAL LOGICAL VARIABLE
C            (L*4)   LTYPE    = .TRUE.  => CONFIG. EISSNER FORM
C                               .FALSE. => CONFIG. NOT EISSNER FORM
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          DXEXCF     ADAS      EXPAND EISSNER CONFIG. INTO SHELL OCCUP.
C          DXCOMP     ADAS      COMPARE TWO OCCUPANCY VECTORS
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    05/06/96
C
C  UPDATE:  24/07/96 - PEB - ADDED THIRD 'LTYPE' ARGUMENT TO 3RD AND 4TH
C                            CALLS TO ROUTINE DXEXCF. (IT HAD BEEN LEFT
C                            OFF.)
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    20TH AUGUST 1996
C
C VERSION: 1.1				DATE: 20-08-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 14-08-97
C MODIFIED: HUGH SUMMERS
C	    - ADDED SPIN DISTINQUISHED PARENT AND METASTABLE 
C             IDENTIFICATION, COUNTERS AND POINTERS 
C
C VERSION: 1.3				DATE: 22-11-2003
C MODIFIED: Martin O'Mullane
C	    - Pass configurations through ceprep before acting on them.
C	    - Extend dimensions of orbital arrays.
C
C VERSION : 1.4
C DATE    : 18-07-2012
C MODIFIED: Martin O'Mullane
C               - Increase length of configuration strings to 
C                 40 characters.
C
C VERSION : 1.5
C DATE    : 21-09-2017
C MODIFIED: Martin O'Mullane
C               - In the warning message, correct the name of routine
C                 and also output the offending configuration string.
C
C-------------------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   NDLEV   , NDMET   ,
     &          NMET    , NPMET
      INTEGER   I       , J       , IM      , IPAR
      INTEGER   IORBIT
      INTEGER   NALCM   , NALCP
C-------------------------------------------------------------------------------
      LOGICAL   LMATCH  , LEISS  , LTYPE
C-------------------------------------------------------------------------------
      INTEGER   IMETR(NDMET)     , IPMETR(NDMET)    ,
     &          ISA(NDLEV)       , ILA(NDLEV)       ,
     &          IPSA(NDLEV)      , IPLA(NDLEV)
      INTEGER   ILINK(NDMET,NDMET,2)
      INTEGER   NOCCUM(61)       , NOCCUP(61)
      INTEGER   IALCM(NDMET)     , IALCP(NDMET)     ,
     &          ISALCM(NDMET)    , ISALCP(NDMET)
C-------------------------------------------------------------------------------
      LOGICAL   LLINK(NDMET,NDMET,2)
C-------------------------------------------------------------------------------
      CHARACTER CSTRGA(NDLEV)*40 , CSTRGPA(NDLEV)*40, ceprep*41  , 
     &          cfg*41
C-------------------------------------------------------------------------------
      external ceprep
C-------------------------------------------------------------------------------
C
C-------------------------------------------------------------------------------
C
      LEISS = .TRUE.
C
      DO 40 IPAR = 1,NPMET
C
        cfg = ceprep(CSTRGPA(IPAR))
        CALL DXEXCF(cfg,NOCCUP,LTYPE)
        IF (.NOT.LTYPE) THEN
            LEISS = .FALSE.
            WRITE(I4UNIT(-1),1000)
            WRITE(I4UNIT(-1),'(20x,a)')cfg
            WRITE(I4UNIT(-1),1001)
            GO TO 42
        ENDIF
C
        IF(IPSA(IPAR).LE.1) THEN
            DO 10 IM = 1,NMET
              IF(ISA(IM).EQ.2) THEN
C
                  cfg = ceprep(CSTRGPA(IM))
                  CALL DXEXCF(cfg,NOCCUM,LTYPE)
                  IF (.NOT.LTYPE) THEN
                      LEISS = .FALSE.
                      WRITE(I4UNIT(-1),1000)
                      WRITE(I4UNIT(-1),'(5x,a)')cfg
                      WRITE(I4UNIT(-1),1001)
                      RETURN
                  ENDIF
C
                  CALL DXCOMP(NOCCUM,NOCCUP,LMATCH, IORBIT)
                  LLINK(IM,IPAR,1)=LMATCH
                  ILINK(IM,IPAR,1)=IORBIT
              ELSE
                  LLINK(IM,IPAR,1)=.FALSE.
                  ILINK(IM,IPAR,1)=0
              ENDIF
   10       CONTINUE
        ELSE
            DO 20 IM = 1,NMET
              IF(ISA(IM).EQ.IPSA(IPAR)-1) THEN
                  cfg = ceprep(CSTRGA(IM))
                  CALL DXEXCF(cfg,NOCCUM,LTYPE)
                  CALL DXCOMP(NOCCUM,NOCCUP,LMATCH, IORBIT)
                  LLINK(IM,IPAR,1)=LMATCH
                  ILINK(IM,IPAR,1)=IORBIT
                  LLINK(IM,IPAR,2)=.FALSE.
                  ILINK(IM,IPAR,2)=0
              ELSEIF(ISA(IM).EQ.IPSA(IPAR)+1) THEN
                  cfg = ceprep(CSTRGA(IM))
                  CALL DXEXCF(cfg,NOCCUM,LTYPE)
                  CALL DXCOMP(NOCCUM,NOCCUP,LMATCH, IORBIT)
                  LLINK(IM,IPAR,2)=LMATCH
                  ILINK(IM,IPAR,2)=IORBIT
                  LLINK(IM,IPAR,1)=.FALSE.
                  ILINK(IM,IPAR,1)=0
              ELSE
                  LLINK(IM,IPAR,1)=.FALSE.
                  LLINK(IM,IPAR,2)=.FALSE.
                  ILINK(IM,IPAR,1)=0
                  ILINK(IM,IPAR,2)=0
              ENDIF
   20       CONTINUE
        ENDIF
   40 CONTINUE
C
C----------------------------------------------------------------------
C  INDEX LOWEST METASTABLE AND PARENT OF EACH SPIN SYSTEM AS SUITABLE
C  FOR ALLOCATION 
C----------------------------------------------------------------------
   42 NALCM = 1
      IALCM(1)=1
      ISALCM(1)=ISA(IMETR(1))
      IF(NMET.GT.1) THEN
          DO 50 I=2,NMET
            DO 45 J=1,NALCM
              IF(ISA(IMETR(I)).EQ.ISALCM(J)) THEN
                  IALCM(I)=IALCM(J)
                  ISALCM(I)=ISALCM(J)
                  GO TO 50
              ENDIF
   45       CONTINUE
            NALCM=NALCM+1
            IALCM(I)=NALCM
            ISALCM(I)=ISA(IMETR(I))
   50     CONTINUE
      ENDIF
C
      NALCP = 1
      IALCP(1)=1
      ISALCP(1)=IPSA(IPMETR(1))
      IF(NPMET.GT.1) THEN
          DO 60 I=2,NPMET
            DO 55 J=1,NALCP
              IF(IPSA(IPMETR(I)).EQ.ISALCP(J)) THEN
                  IALCP(I)=IALCP(J)
                  ISALCP(I)=ISALCP(J)
                  GO TO 60
              ENDIF
   55       CONTINUE
            NALCP=NALCP+1
            IALCP(I)=NALCP
            ISALCP(I)=IPSA(IPMETR(I))
   60     CONTINUE
      ENDIF
C
      RETURN
C----------------------------------------------------------------------
 1000 FORMAT(1X,31('*'),' D7LINK WARNING',32('*')//
     &       1X,'CONFIGURATIONS NOT ALL EISSNER FORM')
 1001 FORMAT(/1X,23('*'),' PARENT/METASTABLE LINK ABORTED ',22('*'))
C----------------------------------------------------------------------
      END
