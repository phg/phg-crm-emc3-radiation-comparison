C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7bndl.for,v 1.2 2004/07/06 13:21:15 whitefor Exp $ Date $Date: 2004/07/06 13:21:15 $

      SUBROUTINE D7BNDL(NDLEV , NDTRN , NDMET , NDOSC ,
     +                  IL    , WA    , XJA   , NV    , TSCEF ,
     +                  ICNTE , IETRN , IE1A  , IE2A  , AA    , SCOM  ,
     +                  NMET  , IMETR , PTOT  , FMIN  , Z1    ,
     +                  ICTM  , IUMA  , CSTGMA,
     +                  FFMA  , WVMA  , GBMA  , PYMA
     +                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7BNDL *********************
C
C
C  PURPOSE:  SUBROUTINE TO DISCARD TRANSITIONS WITH AN OSCILLATOR
C            STRENGTH BELOW A CERTAIN INPUT VALUE. GETS SOME VALUES
C	     VIA A PIPE FROM IDL AND THEN RETURNS SOME VALUES TO IDL
C	     FOR DISPLAY USE IN CW_ADAS407_PROC
C
C
C  CALLING PROGRAM: D7ISPF
C
C
C  SUBROUTINE:
C
C
C
C  INPUT : (I*4)  NDLEV     = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTRN     = MAXIMUM NUMBER OF TRANSITIONS
C  INPUT : (I*4)  NDLEV     = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTRN     = MAXIMUM NUMBER OF TRANSITIONS
C  INPUT : (I*4)  NDMET     = MAXIMUM NUMBER OF METASTABLES
C  INPUT : (I*4)  NDOSC     = MAXIMUM NUMBER OF RADIATIVE TRANSITIONS
C                             ALLOWED FOR ASSEMBLING POWER FOR EACH
C                             METASTABLE
C
C  INPUT : (I*4)  IL        = NUMBER OF ENERGY LEVELS
C  INPUT : (R*8)  WA()      = ENERGY RELATIVE TO LEVEL 1 (CM-1)
C                             1ST DIM: LEVEL INDEX
C  INPUT : (R*8)  XJA()     = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                             (NOTE: (2*XJA)+1 = STATISTICAL WEIGHT)
C                             1ST DIM: LEVEL INDEX
C  INPUT : (I*4)  NV        = INPUT DATA FILE: NUMBER OF TEMPERATURES
C  INPUT : (R*8)  TSCEF(,)  = INPUT DATA FILE: ELECTRON TEMPERATURES
C                             2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                            2 => EV      (IFOUT=2)
C                                            3 => REDUCED (IFOUT=3)
C  INPUT : (I*4)  ICNTE     = NUMBER OF ELECTRON COLL. TRANSITIONS
C  INPUT : (I*4)  IETRN()   = ELECTRON IMPACT TRANSITION:
C                             INDEX VALUES IN MAIN TRANS. ARRAYS WHICH
C                             REPRESENT ELECTRON IMPACT TRANSITIONS.
C  INPUT : (I*4)  IE1A()    = TRANSITION: LOWER ENERGY LEVEL INDEX
C                             1ST DIM: ELECTRON COLL. TRANSITION INDEX
C  INPUT : (I*4)  IE2A()    = TRANSITION: UPPER ENERGY LEVEL INDEX
C                             1ST DIM: ELECTRON COLL. TRANSITION INDEX
C  INPUT : (R*8)  AA()      = TRANSITION: A-VALUE (SEC-1)
C                             1ST DIM: ELECTRON COLL. TRANSITION INDEX
C  INPUT : (R*8)  SCOM(,)   = TRANSITION:
C                             GAMMA VALUES             (CASE ' ' & 'P')
C                             RATE COEFFT. (CM3 SEC-1) (CASE 'H' & 'R')
C                             1ST DIMENSION - TEMPERATURE 'SCEF()'
C                             2ND DIMENSION - TRANSITION NUMBER
C
C  INPUT : (I*4)  NMET      = NUMBER OF METASTABLES SELECTED
C  INPUT : (I*4)  IMETR()   = INDEX OF METASTABLES IN LEVELE LIST
C                             1ST DIM: METASTABLE INDEX
C  OUTPUT: (R*8)  PTOT(,)   = TOTAL ZERO-DENS. RAD. POWER FOR EACH META.
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: METASTABLE INDEX
C  INPUT : (R*8)  FMIN      = MINIMIUM PERMITTED OSCILLATOR STRENGTH
C  INPUT : (R*8)  Z1        = RECOMBINING ION CHARGE
C
C
C  OUTPUT: (I*4)  ICTM()    = NUMBER OF INCLUDED TRANSITIONS FOR EACH
C                             METASTABLE
C                             1ST DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  IUMA(,)   = INDEX OF METASTABLE ASSIGNED TRANSITION
C                             IN FULL ELECTRON COLL. TRANSITION LIST.
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (C*22) CSTGMA(,) = STRING IDENTIFIER FOR INCLUDED TRANSITION
C                             CONTAINING J,I,FIJ,WVLN
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  FFMA(,)   = OSCILLATOR STRENGTH OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (I*4)  WVMA(,)   = WAVELENGTH (A)  OF INCLUDED TRANSITION
C                             1ST DIM: METASTABLE TRANSITION SET INDEX
C                             2ND DIM: METASTABLE INDEX
C  OUTPUT: (R*8)  GBMA(,,)  = GBAR FOR ALLLOWED TRANSITIONS
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: INCLUDED ALLOWED TRANS. INDEX
C                             3RD DIM: METASTABLE INDEX
C  OUTPUT: (R*8)  PYMA(,,)  = VAN REGEMORTER P FOR ALLOWED TRANSITIONS
C                             1ST DIM: SPECIFIC ION FILE TEMP. INDEX
C                             2ND DIM: INCLUDED ALLOWED TRANS. INDEX
C                             3RD DIM: METASTABLE INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D7PYVR     ADAS      EVALUATES VAN REGEMORTER P FACTOR
C
C
C  AUTHOR: M O'MULLANE, UCC
C
C  DATE:    18/05/94
C
C  UPDATE:
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    REPLACED NDMET BY NMET IN LOOP TO READ IMETR

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   NDLEV  , NDTRN  , NDMET  , NDOSC
      INTEGER   IL     , ICNTE  , NMET   , NV
C-----------------------------------------------------------------------
      INTEGER   IMETR(NDMET)
      INTEGER   IETRN(NDTRN)
      INTEGER   IE1A(NDTRN)    , IE2A(NDTRN)
      INTEGER   ICTM(NDMET)    , IUMA(NDOSC,NDMET)
C-----------------------------------------------------------------------
      REAL*8    FMIN           , Z1
C-----------------------------------------------------------------------
      REAL*8    XJA(NDLEV)           , WA(NDLEV)
      REAL*8    AA(NDTRN)
      REAL*8    TSCEF(14,3)          , SCOM(14,NDTRN)
      REAL*8    PTOT(14,NDMET)
      REAL*8    FFMA(NDOSC,NDMET)    , WVMA(NDOSC,NDMET)
      REAL*8    GBMA(14,NDOSC,NDMET) , PYMA(14,NDOSC,NDMET)
C-----------------------------------------------------------------------
      CHARACTER  CSTGMA(NDOSC,NDMET)*22
C-----------------------------------------------------------------------
 
      INTEGER   IM,  I , J , IT , ILOW
      REAL*8    EIJ, WI, WJ, S, FIJ, Y, PY, ATE, GBARF
 
C-----------------------------------------------------------------------
      INTEGER   PIPEIN,	PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C-----------------------------------------------------------------------

C READ VALUES FROM IDL PIPE

      READ(PIPEIN,*)NMET

      READ(PIPEIN,*)(IMETR(J),J=1,NMET)

      DO 5 IM=1,NMET
        ICTM(IM)=0
        DO 3 IT=1,NV
          PTOT(IT,IM)=0.0D0
    3   CONTINUE
    5 CONTINUE
 
      DO 30 I=1,ICNTE
        DO 25 IM=1,NMET
 
          IF (IE1A(I).EQ.IMETR(IM)) THEN
 
             EIJ=(WA(IE2A(I))-WA(IE1A(I)))/1.097373D5
             WI = 2.0*XJA(IE1A(I))+1.0
             WJ = 2.0*XJA(IE2A(I))+1.0
             S = 3.73491D-10*WJ*AA(I)/(EIJ*EIJ*EIJ)
             FIJ=3.333333D-1*EIJ*S/WI
C---------------------------------------------------------------------
C EVALUATE CONTRIBUTIONS TO TOTAL POWER FROM EACH TRANSITION
C---------------------------------------------------------------------
             DO 6 IT = 1,NV
              ATE=157890.0D0*EIJ/TSCEF(IT,1)
              GBARF= 6.8916D-2*EIJ*SCOM(IT,IETRN(I))/WI
              IF(ATE.LE.165.0)THEN
                  PTOT(IT,IM)=PTOT(IT,IM)+1.25D-4*DEXP(-ATE)*
     &                        GBARF/DSQRT(TSCEF(IT,1))
              ENDIF
    6        CONTINUE
C
             IF ((FIJ.GT.FMIN).AND.(ICTM(IM).LT.NDOSC)) THEN
C
                ICTM(IM)=ICTM(IM)+1
                IUMA(ICTM(IM),IM)=IE2A(I)
                FFMA(ICTM(IM),IM)=FIJ
                WVMA(ICTM(IM),IM)=9.112672D2/EIJ
C---------------------------------------------------------------------
C EVALUATE GBAR AND VAN REGEMORTER P
C---------------------------------------------------------------------
                DO 8 IT = 1, NV
                 Y = 157890.0D0*EIJ/TSCEF(IT,1)
                 CALL D7PYVR(Y,Z1,PY)
                 PYMA(IT,ICTM(IM),IM) = PY
                 GBMA(IT,ICTM(IM),IM) = 6.8916D-2*EIJ*SCOM(IT,IETRN(I))/
     &                                  (WI*FIJ)
    8           CONTINUE
C
                WRITE(CSTGMA(ICTM(IM),IM),1000)IE2A(I),IE1A(I),FIJ,
     &             WVMA(ICTM(IM),IM)
C
             ELSEIF ((FIJ.GT.FMIN).AND.(ICTM(IM).GE.NDOSC)) THEN
C---------------------------------------------------------------------
C CHECK FOR SMALLER OSCILLATOR STRENGTH IN LIST - IF SO PUSH DOWN
C EVALUATE GBAR AND VAN REGEMORTER P
C---------------------------------------------------------------------
                ILOW  = 0
                DO 10 J=1,ICTM(IM)
                    IF(FFMA(J,IM).LT.FIJ)ILOW=J
   10           CONTINUE
                IF(ILOW.GT.0) THEN
                    DO 15 J=ILOW+1,ICTM(IM)
                     IUMA(J-1,IM)=IUMA(J,IM)
                     FFMA(J-1,IM)=FFMA(J,IM)
                     WVMA(J-1,IM)=WVMA(J,IM)
   15               CONTINUE
                    IUMA(ICTM(IM),IM)=IE2A(I)
                    FFMA(ICTM(IM),IM)=FIJ
                    WVMA(ICTM(IM),IM)=9.112672D2/EIJ
C
                    DO 18 IT = 1, NV
                     Y = 157890.0D0*EIJ/TSCEF(IT,1)
                     CALL D7PYVR(Y,Z1,PY)
                     PYMA(IT,ICTM(IM),IM) = PY
                     GBMA(IT,ICTM(IM),IM) = 6.8916D-2*EIJ*
     &                                      SCOM(IT,IETRN(I))/(WI*FIJ)
   18               CONTINUE
C
                    WRITE(CSTGMA(ICTM(IM),IM),1000)IE2A(I),IE1A(I),FIJ,
     &                 WVMA(ICTM(IM),IM)
                ENDIF
             ENDIF
 
          ENDIF
   25   CONTINUE
   30 CONTINUE

C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C WRITE THE RESULTS TO THE IDL PIPE FOR DISPLAY USE
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      WRITE(PIPEOU,*)NDMET
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDOSC
      CALL XXFLSH(PIPEOU)

      DO 13,I=1,NDMET
         WRITE(PIPEOU,*)ICTM(I)
         CALL XXFLSH(PIPEOU)
 13   CONTINUE
      
      DO 14,J=1,NDMET
         DO 21,I=1,NDOSC
            WRITE(PIPEOU,*)IUMA(I,J)
            CALL XXFLSH(PIPEOU)
 21      CONTINUE
 14   CONTINUE
      
      DO 16,J=1,NDMET
         DO 17,I=1,NDOSC
            WRITE(PIPEOU,'(A22)')CSTGMA(I,J)
            CALL XXFLSH(PIPEOU)
 17      CONTINUE
 16   CONTINUE

C
C-----------------------------------------------------------------------
 1000 FORMAT(I4,I3,F7.3,F8.1)
C-----------------------------------------------------------------------
C
      RETURN
      END
