C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7pyvr.for,v 1.1 2004/07/06 13:23:26 whitefor Exp $ Date $Date: 2004/07/06 13:23:26 $

      SUBROUTINE D7PYVR(Y,Z1,PY)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7PYVR *********************
C
C  PURPOSE : CALCULATES VAN REGEMORTER'S P FACTOR FOR ELECTRON
C            COLLISIONS WITH ATOMS AND IONS.
C
C  INPUT   : Y=ATE*(1/V1**2+1/V2**2) WITH
C            ATE - 1.5789D5*Z1**2/TE
C            TE  - ELECTRON TEMPERATURE (K)
C            V1  - INITIAL EFFECTIVE PRINCIPAL QUANTUM NUMBER
C            V2  - FINAL EFFECTIVE PRINCIPAL QUANTUM NUMBER
C            Z1  - TARGET ION CHARGE +1
C
C  OUTPUT  : PY  - P-FACTOR
C
C
C  AUTHOR  : H.P. SUMMERS, JET
C            K1/1/57
C            JET EXT. 4941
C
C  DATE:     03/06/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE.
C
C-----------------------------------------------------------------------
      REAL*8   Y   , Z1   , PY   , Y1
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (Y.LE.0.04) THEN
         PY=-0.2756644*(LOG(Y)+0.41)
         RETURN
      ENDIF
C
      IF (Z1.LE.1.0) THEN
         IF (Y.LE.1.0) THEN
            PY=(0.0196*Y+0.0882)/(Y+0.075)
            RETURN
         ELSE
            Y1=0.066/SQRT(Y)
            PY=Y1*(1.0+8.0*Y1)
            RETURN
         ENDIF
      ELSE
         Y1=1.0/Y
         PY=0.2+Y1*(0.04-0.00068*Y1)
         RETURN
      ENDIF
C-----------------------------------------------------------------------
      END
