C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/lsfun1.for,v 1.2 2004/07/06 14:13:34 whitefor Exp $ Date $Date: 2004/07/06 14:13:34 $

      subroutine lsfun1(npt,nlf,pnlfa,fvecc,iflag)
      implicit real*8(a-h,o-z)
 
C-----------------------------------------------------------------------
C  PURPOSE : Program to evaluate functionals for least squares to line
C            power in Gaunt factor approximation.
C
C  COMMENT : Specific for application to program 'ADAS.FORT(POWERFIT)'
C            Necessary for input to NAG routine E04FDF
C
C
C  AUTHOR  : H.P. Summers, JET
C  DATE    : 25-2-91
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    17TH APRIL 1996
C
C VERSION: 1.1				DATE: 17-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 25-04-96
C MODIFIED: WILLIAM OSBORN
C	    - ADDED IFLAG FOR CALLS BY LMDIF1 RATHER THAN E04FDF
C
C-----------------------------------------------------------------------
 
      parameter (ntemp=14,ngroup=5)
 
      dimension fvecc(npt),pnlfa(nlf)
      dimension psuma(ntemp),pfita(ntemp,ngroup)
 
      common /powfit/psuma,pfita
 
      do it=1,npt
 
        sum=0.0
        do i=1,nlf
          sum=sum+pnlfa(i)*pfita(it,i)
        end do
        fvecc(it)=(psuma(it)-sum)/psuma(npt)
 
      end do
 
      return
      end
