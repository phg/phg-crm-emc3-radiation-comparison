C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/nvgoel.for,v 1.3 2019/06/17 13:33:58 mog Exp $ Date $Date: 2019/06/17 13:33:58 $

       SUBROUTINE NVGOEL(MAXT,TEA,GA,GA0,GAREST,Z1,N0,V0,PHFRAC,NCUT)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: EVALUATE TOTAL RADIATIVE RECOMBINATION RATE COEFFICIENTS
C  AT ZERO DENSITY USING THE VON GOELER TYPE FORMULA WITH MODIFIED
C  CAPTURE TO THE LOWEST ACCESSIBLE PRINCIPAL QUANTUM SHELL.
C
C  PHFRAC GIVES THE PROPORTION OF THE LOWEST LEVEL CAPTURE ALLOWED
C  BASED ON THE AVAILABLE PHASE SPACE OF OCCUPIED SHELLS ARGUMENTS.
C
C  MODIFICATION OF VGOEL TO EXTEND ARRAYS AND INCLUDE NCUT
C
C  ********** H.P. SUMMERS, JET          24 JUNE 1987  ****************
C  ***********                     MOD.  24 AUG  1989  ****************
C  INPUT
C      MAXT=NUMBER OF TEMPERATURES
C      TEA(I)=ELECTRON TEMPERATURES (K)
C      Z1=RECOMBINING ION CHARGE
C      N0=LOWEST ACCESSIBLE N-SHELL BY RECOMBINATION
C      V0=EFFECTIVE PRINCIPAL QUANTUM NUMBER OF LOWEST ACCESSIBLE SHELL
C      PHFRAC=PHASE SPACE OCCUPATION FACTOR FOR LOWEST ACCESSIBLE SHELL
C      NCUT=CUT-OFF OF MAXIMUM NUMBER OF N-SHELLS
C  OUTPUT
C      GA(I)=TOTAL RADIATIVE RECOMBINATION COEFFICIENT (CM+3 SEC-1)
C      GA0(I)=GROUND SHELL  RECOMBINATION COEFFICIENT
C      GAREST(I)=RECOMBINATION COEFFICIENT TO ALL SHELLS EXCLUDING
C      THE GROUND SHELL.
C
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    26TH MARCH 1996
C
C VERSION: 1.1				DATE: 26-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. ERRSET COMMENTED OUT
C
C VERSION: 1.2				DATE: 16-05-07
C MODIFIED: Allan Whiteford
C	    - Updated comments as part of subroutine documentation
C             procedure.
C VERSION: 1.3				DATE: 17-06-2019
C MODIFIED: Martin O'Mullane
C	    - Remove mainframe numbers beyond column 72.
C-----------------------------------------------------------------------
       DIMENSION TEA(100),NREP(30)
       DIMENSION ALFN1(100),ALFN(100),GA(100),GA0(100),GAREST(100)
       DATA INREP/21/
       DATA NREP/1,2,3,4,5,6,7,8,9,10,12,15,20,30,40,50,60,70,100,150,
     &200,250,300,400,500,600,700,800,900,1000/
C      CALL ERRSET(208,256,-1)
C
C----------------------------------------------------------------------
C      INITIALISE OUTPUT ARRAYS TO ZERO
C----------------------------------------------------------------------
C
       DO 40 IT=1,MAXT
          GA(IT)=0.0D0
          GA0(IT)=0.0D0
          GAREST(IT)=0.0D0
   40  CONTINUE
C
C----------------------------------------------------------------------
C    CALCULATE CONTRIBUTION FROM SHELLS    N0 < N <= 10
C----------------------------------------------------------------------
C
       N1=N0+1
       I0=0
   41  I0=I0+1
       IF(N1.GE.NREP(I0)) GO TO 41
       IF(I0.LT.10) I0 = 10
       DO 100 N=N1,NREP(I0)
          V=N
          DO 60 IT=1,MAXT
             ATE=1.5789D5*Z1*Z1/TEA(IT)
             ATEN=ATE/(V*V)
             IF(N.LE.NCUT) THEN
               ALFN(IT)=2.6D-14*Z1*DSQRT(ATE)*2.0D0*ATEN*EEI(ATEN)/V
             ELSE
               ALFN(IT)=0.0D0
             ENDIF
             GAREST(IT) = GAREST(IT) + ALFN(IT)
   60     CONTINUE
  100  CONTINUE
C
C----------------------------------------------------------------------
C      CALCULATE CONTRIBUTION FOR N-SHELLS GREATER THAN 11
C----------------------------------------------------------------------
C
       IF(NCUT.GE.11) THEN
         DO 109 I1=I0+1,INREP
         N1=NREP(I1)
         V1=N1
         IF(N1.LE.NCUT) THEN
           NMAX=N1
           DO 106 IT=1,MAXT
           ATE=1.5789D5*Z1*Z1/TEA(IT)
           ATEN1=ATE/(V1*V1)
           ALFN1(IT)=2.6D-14*Z1*DSQRT(ATE)*2.0D0*ATEN1*EEI(ATEN1)/V1
           B=-DLOG(ALFN(IT)/ALFN1(IT))/DLOG(V/V1)
           A=ALFN(IT)*V**B
           GAREST(IT)=GAREST(IT)+((2.0D0*V1+1.0D0-B)*ALFN1(IT)-(2.0D0*V
     &     +1.0D0-B)*ALFN(IT))/(2.0D0-2.0D0*B)
  106      CONTINUE
           N=N1
           V=V1
           DO 108 IT=1,MAXT
  108      ALFN(IT)=ALFN1(IT)
         ENDIF
  109    CONTINUE
         DO 150 IT=1,MAXT
  150    GAREST(IT)=GAREST(IT)+0.5D0*(NMAX-1)*ALFN(IT)
       ENDIF
C
C----------------------------------------------------------------------
C      CALCULATE CONTRIBUTION FOR GROUND STATE N-SHELL
C----------------------------------------------------------------------
C
       DO 200 IT=1,MAXT
          ATE=1.5789D5*Z1*Z1/TEA(IT)
          ATE0=ATE/(V0*V0)
          GA0(IT)=2.6D-14*Z1*DSQRT(ATE)*2.0D0*PHFRAC*ATE0*EEI(ATE0)/V0
          GA(IT)=GA0(IT)+GAREST(IT)
  200  CONTINUE
C
C----------------------------------------------------------------------
C      END OF SUMMATION - RETURN
C----------------------------------------------------------------------
C
       RETURN
      END
