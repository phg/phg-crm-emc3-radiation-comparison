C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7spf1.for,v 1.2 2004/07/06 13:23:45 whitefor Exp $ Date $Date: 2004/07/06 13:23:45 $

      SUBROUTINE D7SPF1( OPEN11 , OPEN12 ,
     &                   LMBN   , LPRS   ,
     &                   DSNMBN , DSNPRS ,
     &                   IPEND  ,
     &                   L2FILE , SAVFIL , CADAS  ,
     &                   LGRD1  , LDEF1  ,
     &                   XL1    , XU1    , YL1    , YU1   ,
     &                   LPLT   , LGRD   , LDEF   ,
     &                   XLG    , XUG    , YLG    , YUG   ,
     &                   LSETP)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7SPF1 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH IDL OUTPUT FOR OUTPUT OPTIONS,
C           BOTH GRAPHICAL AND PASSING FILE.
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   OPEN11   = .TRUE.  => CONTOUR PASSING FILE OPENED
C                             .FALSE. => CONTOUR PASSING FILE NOT OPENED
C  I/O   : (L*4)   OPEN12   = .TRUE.  => METPOP  PASSING FILE OPENED
C                             .FALSE. => METPOP  PASSING FILE NOT OPENED
C
C  INPUT : (L*4)   LSETP    = .TRUE.  => METASTABLE ONE HAS BEEN ANALYSED
C			      .FALSE. => METASTABLE ONE HAS NOT BEEN ANALYSED
C  OUTPUT: (L*4)   LMBN     = .TRUE.  => OUTPUT DATA TO MAINBN PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        MAINBN PASSING FILE.
C  OUTPUT: (L*4)   LPRS     = .TRUE.  => OUTPUT DATA TO ATOMPARS PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        ATOMPARS PASSING FILE.
C
C  OUTPUT: (C*80)  DSNMBN   = OUTPUT MAINBN DATA SET NAME (SEQUENTIAL)
C  OUTPUT: (C*80)  DSNPRS   = OUTPUT ATOMPARS FILE NAME (SEQUENTIAL)
C
C  OUTPUT: (I*4)   IPEND    =    0    => CANCEL SELECTED
C                                1    => MENU SELECTED
C                                2    => GRAPH AND RETURN TO PROCESSING
C                                3    => GRAPH AND EXIT
C
C  OUTPUT: (L*4)   L2FILE   = .TRUE.  => INFO FILE OUTPUT SELECTED
C                             .FALSE. => INFO FILE OUTPUT NOT SELECTED
C
C  OUTPUT: (L*4)   LPLT     = .TRUE.  => GRAPH SPECIFIC LINE POWER PLOT
C                             .FALSE. => DON'T GRAPH SPECIFIC LINE POWER PLOT
C
C  OUTPUT: (C*80)  SAVFIL   = FILE TO WHICH TO OUTPUT INFO
C  
C  OUTPUT: (C*80)  CADAS    = LONG TITLE
C  
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE POWER GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF POWER GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF POWER GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF POWER GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF POWER GRAPH
C
C  OUTPUT: (L*4)   LDEF     = .TRUE.  => USE SP.LINE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C  OUTPUT: (R*8)   XLG      = LOWER LIMIT FOR X-AXIS OF SP. LINE GRAPH
C  OUTPUT: (R*8)   XUG      = UPPER LIMIT FOR X-AXIS OF SP. LINE GRAPH
C  OUTPUT: (R*8)   YLG      = LOWER LIMIT FOR Y-AXIS OF SP. LINE GRAPH
C  OUTPUT: (R*8)   YUG      = UPPER LIMIT FOR Y-AXIS OF SP. LINE GRAPH
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH     ADAS	FLUSHES BUFFER
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    09/10/90
C
C UPDATE:  26/11/90 - ADAS91 - PE BRIDEN - AMENDED   'XXDISP'   ARGUMENT
C                                          LIST. IT NOW INCLUDES DISPLAY
C                                          RETURN CODES.
C                                          IF 'RETURN' OR 'END' ENTERED
C                                          ON A PANEL, EXCEPT VIA PFKEY,
C                                          PROGRAM TERMINATES.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    26TH MARCH 1996
C
C VERSION: 1.1				DATE: 26-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    INCREASED FILE LENGTHS TO 80 CHARACTERS
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      CHARACTER    DSNMBN*80   , DSNPRS*80
      CHARACTER    DSNIC1*80   , DSNIC2*80
      CHARACTER    SAVFIL*80   , CADAS*80
C-----------------------------------------------------------------------
      LOGICAL      OPEN11      , OPEN12    ,
     &             LMBN        , LPRS      ,
     &             L2FILE      ,
     &             LPLT      ,
     &             LDEF1       , LDEF      ,
     &             LGRD        , LGRD1	   , LSETP
C-----------------------------------------------------------------------
      REAL*8       XL1    , XU1    , YL1    , YU1   ,
     &             XLG    , XUG    , YLG    , YUG
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      , IPEND       ,
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER (PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  WRITE LSETP TO IDL SO THAT IT KNOWS WHETHER MET. 1 HAS BEEN ANALYSED
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      IF (LSETP) THEN
          WRITE(PIPEOU,*)ONE
      ELSE
	  WRITE(PIPEOU,*)ZERO
      ENDIF
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) IPEND
      IF (IPEND .EQ. 0) THEN
         RETURN
      ELSEIF (IPEND .EQ. 1) THEN
         RETURN
      ELSEIF (IPEND .EQ. 2) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
            LDEF1  = .TRUE.
            READ(PIPEIN,*) XL1
            READ(PIPEIN,*) XU1
            READ(PIPEIN,*) YL1
            READ(PIPEIN,*) YU1
         ELSE
            LDEF1  = .FALSE.
         ENDIF
C
	 READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LPLT = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               LDEF  = .TRUE.
	       READ(PIPEIN,*) XLG
	       READ(PIPEIN,*) XUG
	       READ(PIPEIN,*) YLG
	       READ(PIPEIN,*) YUG
	    ELSE
	       LDEF  = .FALSE.
            ENDIF
         ELSE
            LPLT = .FALSE.
         ENDIF
C
      ELSEIF (IPEND .EQ. 3) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
            L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
            CALL XXADAS(CADAS)
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LMBN = .TRUE.
            READ(PIPEIN, '(A)') DSNMBN
         ELSE
            LMBN = .FALSE.
         ENDIF
         
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LPRS = .TRUE.
            READ(PIPEIN, '(A)') DSNPRS
         ELSE
            LPRS = .FALSE.
         ENDIF
         
      ENDIF

      RETURN
      END
