C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/gpdiel.for,v 1.2 2004/07/06 13:59:41 whitefor Exp $ Date $Date: 2004/07/06 13:59:41 $
C UNIX-IDL PORT - SCCS INFO: MODULE @(#)gpdiel.for	1.1 DATE 04/22/96

       SUBROUTINE GPDIEL(Z,EIJ,F,T,COR,JCOR,N,DEF,AD,ADU,L,CPT)

       IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: GPDIEL *********************
C
C
C  ROUTINES:
C          ROUTINE   SOURCE   DESCRIPTION
C          -------------------------------------------------------
C          BF        ADAS
C
C  HISTORY : H P SUMMERS 
C
C  UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C  DATE:    19TH APRIL 1996
C
C  VERSION: 1.1				DATE: 19-04-96
C  MODIFIED: WILLIAM OSBORN
C	      -NO CHANGES TO IBM VERSION
C
C
C
C VERSION: 1.2				DATE: 04-12-2003
C MODIFIED: Martin O'Mullane
C            - Major restructuring.
C
C-----------------------------------------------------------------------
       REAL*8  THETA(1000), COR(20), CPTS(1000)
C-----------------------------------------------------------------------

       AD=0.0D0
       ZZ=Z*Z
       Z1=(Z+1.0D0)*(Z+1.0D0)
       EN=N
       EN=EN-DEF
       T1=Z1*EIJ/ZZ-1.0D0/(EN*EN)
       Z2=Z1*EIJ/ZZ
       
C  X:SECT REQUIREMENT:T4=4.68145D-33*Z2*Z2*F/T1

       T4=0.51013D0*Z2*Z2*F
       E=T1*ZZ
       IF(T1)4,4,1
    1  A=DSQRT(T1)
       CALL BF(THETA,N,A)
       B=0.777135D-6*ZZ*ZZ
       J1=JCOR
       IF(N-JCOR)5,6,6
    5  J1=N
    6  DO 2 J=1,J1
    2  THETA(J)=COR(J)*THETA(J)
       C3=0.0D0
       C3U=0.0D0
       DO 3 J=1,N
       TJ=J
       TH=THETA(J)*TJ*EN*EN
       TJ=TJ+TJ-1.0D0
        T3=TJ*TH/(B*TJ*(1.0D0+T)+TH)
       T3U=TJ
       THETA(J)=TH/(B*TJ*(1.0D0+T))

C  X-SECT REQUIREMENT:CPTS(J)=T3*T4

       CPTS(J)=T3*T4
       C3U=C3U+T3U
    3   C3=C3+T3

C  X-SECT REQUIREMENT:WRITE(6,100)(CPTS(J),J=1,N)

       Z2=Z1*EIJ/ZZ
        AD=0.51013D0*Z2*Z2*F*C3
       ADU=0.51013D0*Z2*Z2*F*C3U
C      WRITE(6,102)C3,AD
       CPT=CPTS(L+1)

C  X-XECT REQUIREMENT:ADC=C3*T4
C  X-SECT REQUIREMENT:WRITE(6,101)ADC

       IF(N-10)7,8,8
    7  NP=N
       GO TO 9
    8  NP=10
    9  CONTINUE


    4  RETURN

C-----------------------------------------------------------------------
  100  FORMAT(1P,10E12.2)
  101  FORMAT(1P,E12.2//)
C-----------------------------------------------------------------------
      
      END
