C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7grps.for,v 1.1 2004/07/06 13:21:59 whitefor Exp $ DATE $Date: 2004/07/06 13:21:59 $
C
       subroutine d7grps(nwave, ndwave, wave, index) 
        
       implicit none
       
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 FUNCTION: D7GRPS *********************
C
C  PURPOSE  : Puts wavelengths into a 5 bin histogram for automating
C             effective line selection in ADAS407. 
C
C  COMMENTS : Note that the number of bins is fixed and no attempt is 
C             made at clever adaptive methods of optimising case where
C             there is a large empty wavelength space between groups.
C
C  INPUT:  (I*4)   NWAVE   = number of wavelengths
C  INPUT:  (I*4)   NDWAVE  = max size of wavelength array
C  INPUT:  (R*8)   WAVE    = wavelengths
C
C  OUTPUT: (I*4)   INDEX   = indices (1->nbin) of wavelengths
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4INDF     ADAS      Finds closet index in an array
C
C
C  AUTHOR   : Martin O'Mullane
C
C  VERSION  : 1.1                          
C  DATE     : 17-01-2002
C
C  MODIFIED : Martin O'Mullane  
C              - First version.
C
C-----------------------------------------------------------------------
       integer nbin
C-----------------------------------------------------------------------
       parameter (nbin = 5)
C-----------------------------------------------------------------------
       integer   i            , ih          , i4indf   , nwave  , ndwave
C-----------------------------------------------------------------------
       real*8    xmin         , xmax
C-----------------------------------------------------------------------
       integer   index(ndwave)
C-----------------------------------------------------------------------
       real*8    xhist(nbin)  , wave(ndwave)
C-----------------------------------------------------------------------
       
       xmin = wave(1)
       xmax = wave(1)
       do i = 1, nwave
         if (wave(i).GE.xmax) xmax = wave(i)
         if (wave(i).LE.xmin) xmin = wave(i)
       end do
       
        
       do i = 1, nbin
         xhist(i) = xmin + dfloat(i-1) * (xmax-xmin) / (nbin-1)
       end do
       
       do i = 1, nwave
          ih = i4indf(nbin, xhist, wave(i))
          if (ih.GT.0) index(i) = ih
       end do
       
       end       
       
