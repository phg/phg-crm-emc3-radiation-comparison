C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7setp.for,v 1.2 2012/07/18 18:41:20 mog Exp $ DATE $Date: 2012/07/18 18:41:20 $

       SUBROUTINE D7SETP( IZ0    , IZ     ,
     &                    NDLEV  , IL     , ICNTE ,
     &                    CSTRGA , ISA    , ILA   , XJA  ,
     &                    STRGA  ,
     &                    IL2    ,
     &                    CSTRGA2, ISA2   , ILA2  , XJA2 ,
     &                    STRGA2
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7SETP *********************
C
C  PURPOSE:  TO SET UP STRGA AND STRGA2 ARRAYS FOR USE IN ADAS407.PRO AND
C            OTHER IDL ROUTINES. THEN PIPE THEM AND OTHER PARAMETERS TO IDL.
C
C  CALLING PROGRAM: ADAS407
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'BXDATA', 'D7DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  INPUT : (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF ENERGY LEVELS ALLOWED
C  INPUT : (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C  INPUT : (I*4)  ICNTE   = NUMBER OF ELECTRON IMPACT TRANSITIONS
C
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT : (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT : (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT : (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT : (I*4)  IL2     = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C                              (2ND INPUT COPASE FILE)
C  INPUT : (C*18) CSTRGA2()= NOMENCLATURE/CONFIGURATION
C                           FOR LEVEL 'IA()'
C                              (2ND INPUT COPASE FILE)
C  INPUT : (I*4)  ISA2()  = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C                              (2ND INPUT COPASE FILE)
C  INPUT : (I*4)  ILA2()  = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C                              (2ND INPUT COPASE FILE)
C  INPUT : (R*8)  XJA2()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C                              (2ND INPUT COPASE FILE)
C
C  OUTPUT: (C*22) STRGA() = LEVEL DESIGNATIONS
C  OUTPUT: (C*22) STRGA2()= LEVEL DESIGNATIONS
C                              (2ND INPUT COPASE FILE)
C
C          (C*8)  F6      = PARAMETER = 'VREPLACE'
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)  ILEV    = ARRAY COUNTER FOR LEVEL INDEX
C          (I*4)  J       = VALUE OF QUANTUM NUMBER L + 1
C          (I*4)  LFPOOL  = NO. OF LEVEL STRINGS SENT TO FUNCTION POOL
C
C          (C*2)  SZ0     =         NUCLEAR CHARGE READ
C          (C*2)  SZ      =  RECOMBINED ION CHARGE READ
C          (C*3)  SCNTE   =  NUMBER OF ELECTRON IMPACT TRANSITIONS
C          (C*3)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*3)  SIL     =  NUMBER OF ENERGY LEVELS
C          (C*22) BLANKS  =  BLANK CHARACTER STRING
C                           DIMENSION: QUANTUM NUMBER L + 1
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           XXFLSH      ADAS        FLUSHES PIPE
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    19/05/94
C
C UNIX-IDL PORT:
C          WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    2ND APRIL 1996
C
C VERSION:  1.1                   DATE: 02-04-96
C MODIFIED: WILLIAM OSBORN
C           - FIRST VERSION
C
C VERSION : 1.2
C DATE    : 18-07-2012
C MODIFIED: Martin O'Mullane
C               - Increase length of configuration strings to 
C                 40 characters but not the lenght of strings
C                 sent to IDL for 407 display.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   IZ0           , IZ            , NDLEV    , IL     ,
     &          ICNTE         , IL2
      INTEGER   ILEN          , ILEV          , ILVAL    , LFPOOL
      INTEGER   ISA(IL)       , ILA(IL)
      INTEGER   ISA2(IL2)     , ILA2(IL2)
C-----------------------------------------------------------------------
      REAL*8    XJA(IL)
      REAL*8    XJA2(IL2)
C-----------------------------------------------------------------------
      CHARACTER F6*8
      CHARACTER SZ*2          , SZ0*2         , SCNTE*4  , SIL*4  ,
     &          CONFIG(20)*1
      CHARACTER SZ1*2         , SIL2*4        , BLANKS*22
      CHARACTER CSTRGA(IL)*40   , STRGA(NDLEV)*22
      CHARACTER CSTRGA2(IL2)*40 , STRGA2(NDLEV)*22
C-----------------------------------------------------------------------
      INTEGER PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5,PIPEOU=6)
C-----------------------------------------------------------------------
      DATA BLANKS/' '/
      DATA CONFIG/ 'S','P','D','F','G','H','I','J','K',11*' '/
C **********************************************************************
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS FOR FIRST COPASE FILE LEVEL LIST PANEL.
C-----------------------------------------------------------------------
C
      WRITE(SZ0,1000) IZ0
      WRITE(SZ ,1000) IZ
      WRITE(SCNTE,1001) ICNTE
      WRITE(SIL  ,1001) IL
         DO 1 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL ) THEN
                  ILVAL=ILA(ILEV)+1
                  WRITE(STRGA(ILEV)(1:22),1002) CSTRGA(ILEV)(1:12),
     &                  ISA(ILEV),CONFIG(ILVAL),XJA(ILEV)
               ELSE
                  STRGA(ILEV)(1:22)=BLANKS
               ENDIF
    1    CONTINUE
      IF (IL.LT.NDLEV) STRGA(IL+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C SEND STRINGS TO D7SETP.PRO VIA PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A2)') SZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A2)') SZ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A4)') SCNTE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A4)') SIL
      CALL XXFLSH(PIPEOU)
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ONLY SEND LEVEL STRINGS TO IDL FOR FIRST 99 LEVELS
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
      LFPOOL = MIN0(NDLEV,99)
C
      WRITE(PIPEOU,*) LFPOOL
      CALL XXFLSH(PIPEOU)

         DO 2 ILEV=1,LFPOOL
            WRITE(PIPEOU,'(A22)')STRGA(ILEV)
            CALL XXFLSH(PIPEOU)
    2    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP CHARACTER STRINGS FOR SECOND COPASE FILE LEVEL LIST PANEL.
C-----------------------------------------------------------------------
C
      WRITE(SZ1,1000) IZ+1
      WRITE(SIL2 ,1001) IL2
         DO 3 ILEV=1,NDLEV
            STRGA(ILEV)=' '
               IF     ( ILEV.LE.IL2) THEN
                  ILVAL=ILA2(ILEV)+1
                  WRITE(STRGA2(ILEV)(1:22),1002) CSTRGA2(ILEV)(1:12),
     &                  ISA2(ILEV),CONFIG(ILVAL),XJA2(ILEV)
               ELSE
                  STRGA2(ILEV)(1:22)=BLANKS
               ENDIF
    3    CONTINUE
      IF (IL2.LT.NDLEV) STRGA2(IL2+1) = '*** END OF LEVELS *** '
C
C-----------------------------------------------------------------------
C SEND STRINGS TO IDL
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,'(A2)') SZ1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A4)') SIL2
      CALL XXFLSH(PIPEOU)
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C ONLY SEND LEVEL STRINGS TO IDL FOR FIRST 99 LEVELS
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
      LFPOOL = MIN0(NDLEV,99)
C
      WRITE(PIPEOU,*) LFPOOL
      CALL XXFLSH(PIPEOU)

         DO 4 ILEV=1,LFPOOL
            WRITE(PIPEOU,'(A22)')STRGA2(ILEV)
            CALL XXFLSH(PIPEOU)
    4    CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
 1001 FORMAT(I4)
 1002 FORMAT(1A12,'(',I1,')',A1,'(',F4.1,')')
 1003 FORMAT(I2.2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
