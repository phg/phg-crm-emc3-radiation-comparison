C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7data.for,v 1.2 2004/07/06 13:21:37 whitefor Exp $ Date $Date: 2004/07/06 13:21:37 $

       SUBROUTINE D7DATA( IUNIT  , NDLEV  , NDTRN ,
     &                    TITLED , IZ     , IZ0   , IZ1   , BWNO  ,
     &                    IL     ,
     &                    IA     , CSTRGA , ISA   , ILA   , XJA   , WA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7DATA *********************
C
C  PURPOSE:  TO FETCH LEVEL DATA FROM INPUT COPASE DATA SET.
C
C  CALLING PROGRAM: ADAS407
C
C  DATA:
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           IONISATION POTENTIAL: WAVE NUMBER (CM-1)
C           INDEX LEVEL ENERGIES: WAVE NUMBER (CM-1)
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT : (I*4)  NDLEV   = MAXIMUM NUMBER OF LEVELS THAT CAN BE READ
C  INPUT : (I*4)  NDTRN   = MAX. NUMBER OF TRANSITIONS THAT CAN BE READ
C
C  OUTPUT: (C*3)  TITLED  = ELEMENT SYMBOL.
C  OUTPUT: (I*4)  IZ      =  RECOMBINED ION CHARGE READ
C  OUTPUT: (I*4)  IZ0     =         NUCLEAR CHARGE READ
C  OUTPUT: (I*4)  IZ1     = RECOMBINING ION CHARGE READ
C                           (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  OUTPUT: (R*8)  BWNO    = IONISATION POTENTIAL (CM-1)
C
C  OUTPUT: (I*4)  IL      = INPUT DATA FILE: NUMBER OF ENERGY LEVELS
C
C  OUTPUT: (I*4)  IA()    = ENERGY LEVEL INDEX NUMBER
C  OUTPUT: (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  OUTPUT: (I*4)  ISA()   = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  OUTPUT: (I*4)  ILA()   = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  OUTPUT: (R*8)  XJA()   = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  OUTPUT: (R*8)  WA()    = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE.
C          (I*4)  IABT    = RETURN CODE FROM 'R8FCTN' (0 => NO ERROR)
C                           OR FROM INTERROGATION OF 'C7'
C          (I*4)  IFIRST  = BYTE POSITION OF START OF NUMBER IN BUFFER
C          (I*4)  ILAST   = BYTE POSITION OF END   OF NUMBER IN BUFFER
C          (I*4)  IWORD   = THE WORD POSITION OF THE REQUIRED DATA IN
C                           A STRING TO BE INTERROGATED BY XXWORD.
C          (I*4)  J       = GENERAL USE.
C          (I*4)  LENCST  = BYTE LENGTH OF STRING CSTRGA()
C          (I*4)  NWORDS  = NUMBER OF NUMBERS STORED IN BUFFER
C          (I*4)  ILINE   = ENERGY LEVEL INDEX FOR CURRENT LINE
C          (I*4)  IRECL   = RECORD LENGTH OF INPUT DATASET (<=128)
C
C          (C*7)  C7      = USED TO PARSE VALUE FOR XJA()
C          (C*7)  CDELIM  = DELIMITERS FOR INPUT OF DATA FROM HEADERS
C          (C*18) C18     = USED TO PARSE VALUE TO CSTRGA()
C          (C*80) CLINE   = CURRENT ENERGY LEVEL INDEX PARAMETER LINE
C          (C*128)BUFFER  = GENERAL STRING BUFFER STORAGE
C
C          (L*4)  LDATA   = IDENTIFIES  WHETHER  THE END OF AN  INPUT
C                           SECTION IN THE DATA SET HAS BEEN LOCATED.
C                           (.TRUE. => END OF SECTION REACHED)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXWORD     ADAS      EXTRACT POSITION OF NUMBER IN BUFFER
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      CONVERTS FROM CHARACTER TO REAL VARIABLE
C
C
C ROUTINES: NONE
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/47
C          JET EXT. 4941
C
C DATE:    19/05/94
C
C UPDATE:  12/07/94 - H. P. SUMMERS - ALLOWED DUMMY BARE NUCLEUS FILE
C                                     TO BE READ BY DETECTING IZ0=IZ
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    25TH MARCH 1996
C
C VERSION: 1.1				DATE: 25-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 09-09-96
C MODIFIED: WILLIAM OSBORN / PAUL BRIDEN
C           - INSTEAD OF USING FORMAT SPECIFIER F15.0 WHEN
C             INTERNALLY READING A FLOATING POINT NUMBER,
C             CREATE THE APPROPRIATE SPECIFIER WITHIN CFORM7 
C             AND USE THIS.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   IUNIT       , NDLEV          , NDTRN      ,
     &          IZ          , IZ0            , IZ1        ,
     &          IL          ,
     &          MAXLEV
      INTEGER   ILINE       , IRECL
      INTEGER   I              , IABT       ,
     &          IFIRST(1)   , ILAST(1)       , IWORD      ,
     &          J           ,
     &          LENCST      , NWORDS
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
C-----------------------------------------------------------------------
      REAL*4    ZF
C-----------------------------------------------------------------------
      REAL*8    R8FCTN
      REAL*8    BWNO
      REAL*8    XJA(NDLEV)  , WA(NDLEV)
C-----------------------------------------------------------------------
      CHARACTER TITLED*3    , CSTRGA(NDLEV)*(*)
      CHARACTER C7*7        , CDELIM*7       , C18*18     ,
     &          CLINE*80    , BUFFER*128
      CHARACTER CFORM7*7
C-----------------------------------------------------------------------
      LOGICAL   LDATA
C-----------------------------------------------------------------------
      DATA      CDELIM / ' ()<>{}' /
      DATA      CFORM7 / '(F??.0)' /
C-----------------------------------------------------------------------
      SAVE      CFORM7
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      LENCST = MIN0( LEN(CSTRGA(1)) , 18 )
C
C THE NEXT IF BLOCK IS ONLY USED TO MAKE SURE THAT ANYONE WHO USES  THIS
C SUBROUTINE INDEPENDENTLY IS MADE AWARE OF CHANGES MADE TO ITS ARGUMENT
C LIST ON 20/05/93.
C
C IDENTIFY IF BXDATA IS BEING CALLED USING THE OLD STYLE ARGUMENTS
C IF SO QUIT PROGRAM MESSAGE.  (LENCST=12 => old style).
C
         IF (LENCST.EQ.12) THEN
            WRITE(I4UNIT(-1),1013)
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C IDENTIFY THE RECORD LENGTH OF THE INPUT DATASET (OLD-STYLE = 80 ? )
C (MAXIMUM CURRENTLY ACTIVE USAGE IS 128 BYTES)
C
      INQUIRE(IUNIT,RECL=IRECL)
      IRECL = MIN0( IRECL , 128 )
C
C****************** PEB 20/05/93 - END OF MODIFICATION *****************
C
C***********************************************************************
C INPUT ION SPECIFICATIONS.
C***********************************************************************
C
      READ(IUNIT,1000) TITLED, IZ, IZ0, IZ1, BUFFER(1:55)
C
      J     = 1
      IWORD = 1
      CALL XXWORD( BUFFER(1:55) , CDELIM   , IWORD  ,
     &             J            ,
     &             IFIRST(1)    , ILAST(1) , NWORDS )
C

C****************** WRO/PEB 09/09/96 - BEGINNING OF MODIFICATION *******
C** REPLACED:   READ(BUFFER(IFIRST(1):ILAST(1)),'(F15.0)') BWNO       **
      J = 1 + ILAST(1) - IFIRST(1)
      J = MIN0(J,15)
      WRITE(CFORM7(3:4),'(I2.2)') J
      READ(BUFFER(IFIRST(1):ILAST(1)),CFORM7) BWNO
C****************** WRO/PEB 09/09/96 - END OF MODIFICATION *******
C
         IF(BWNO.LE.0.0D0.AND.IZ0.NE.IZ) THEN
            WRITE(I4UNIT(-1),1001) 'IONISATION POTENTIAL .LE. 0'
            WRITE(I4UNIT(-1),1002)
            STOP
         ENDIF
C
C***********************************************************************
C READ IN ENERGY LEVEL SPECIFICATIONS
C***********************************************************************
C
      LDATA=.TRUE.
C
         DO 1 I=1,NDLEV
C
            IF (LDATA) THEN
               READ (IUNIT,'(A80)') CLINE
               READ (CLINE,'(I5)')  ILINE
C
C  ENERGY LEVEL INPUT INFORMATION IS TERMINATED BY ILINE=-1.
C
                  IF (ILINE.LE.0) THEN
                     LDATA=.FALSE.
                     IL=I-1
C
C  ENERGY LEVEL INDEX 'ILINE' SHOULD EQUAL 'I'
C
                  ELSEIF (ILINE.NE.I) THEN
                     WRITE(I4UNIT(-1),1001) 'ENERGY LEVEL INDEX',ILINE,
     &                             ' OUT OF ORDER'
                     WRITE(I4UNIT(-1),1002)
                     STOP
                  ELSE
C
C********************* PEB 30/07/92 - MODIFICATION *********************
C********************* PEB 20/05/93 - MODIFICATION *********************
C
                     READ (CLINE,1003) IA(I)  , C18          ,
     &                                 ISA(I) , ILA(I)       ,
     &                                 C7     , BUFFER(1:44)
                     J     = 1
                     IWORD = 1
                     CALL XXWORD( BUFFER(1:44) , CDELIM    , IWORD  ,
     &                            J            ,
     &                            IFIRST(1)    , ILAST(1)  , NWORDS )
C

C****************** WRO/PEB 09/09/96 - BEGINNING OF MODIFICATION *******
C** REPLACED:   READ(BUFFER(IFIRST(1):ILAST(1)),'(F15.0)') WA(I)      **
                     J = 1 + ILAST(1) - IFIRST(1)
                     J = MIN0(J,15)
                     WRITE(CFORM7(3:4),'(I2.2)') J
                     READ(BUFFER(IFIRST(1):ILAST(1)),CFORM7) WA(I)
C****************** WRO/PEB 09/09/96 - END OF MODIFICATION *******
C     
C IDENTIFY IF D7DATA IS BEING CALLED USING THE OLD CALLING ARGUMENTS
C REMOVE LEADING BLANKS FROM OLD STYLE INPUT DATASETS.
C
                     IF (C18(1:2).EQ.'  ') C18=C18(3:18)
C
                     CSTRGA(I)=C18(1:LENCST)
C
C****************** PEB 20/05/93 - END OF MODIFICATION *****************
C
                     J = INDEX(C7,')') - 1
C
                        IF (C7(1:J).EQ.' ') THEN
                           IABT   = -1
                        ELSE IF (J.GT.0) THEN
                           XJA(I) = R8FCTN( C7(1:J) , IABT )
                        ELSE
                           IABT   = J - 1
                        ENDIF
C
                        IF (IABT.NE.0) THEN
                           WRITE(I4UNIT(-1),1001)
     &                                   'COPASE DATA SET LEVEL ',I,
     &                                   ' HAS INVALID WEIGHT'
                              IF (IABT.EQ.-1) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO NUMBER FOUND',C7
                              ELSE IF (IABT.EQ.-2) THEN
                                 WRITE(I4UNIT(-1),1012)
     &                                    'NO RIGHT BRACKET FOUND',C7
                              ELSE
                                 WRITE(I4UNIT(-1),1012)
     &                                    'INVALID NUMBER FOUND' , C7
                              ENDIF
                           WRITE(I4UNIT(-1),1002)
                           STOP
                        ENDIF
C****************** PEB 30/07/92 - END OF MODIFICATION *****************
C
                  ENDIF
            ENDIF
    1    CONTINUE
C
         IF (LDATA) THEN
            READ (IUNIT,1004) I
               IF (I.GT.0) THEN
                  WRITE(I4UNIT(-1),1001)
     &                          'COPASE DATA SET CONTAINS > ',NDLEV,
     &                          ' ENERGY LEVELS'
                  WRITE(I4UNIT(-1),1002)
                  STOP
               ELSE
                  IL = NDLEV
               ENDIF
         ENDIF
C
C***********************************************************************
C
 1000 FORMAT(1A3,I2,2I10,A55)
 1001 FORMAT(1X,32('*'),' D7DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 1002 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 1003 FORMAT(I5,1X,1A18,1X,2(I1,1X),A7,A44)
 1004 FORMAT(I5)
 1005 FORMAT(F5.2,4X,I1,6X,14(A5,A3))
 1006 FORMAT(1X,A)
 1007 FORMAT(1X,32('*'),' D7DATA ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,2(I1,A))
 1008 FORMAT(A1,I3,I4,15(F5.2,I3))
 1009 FORMAT(A,A/17X,2(A,I2)/)
 1010 FORMAT(1X,I3)
 1011 FORMAT(1X,32('*'),' D7DATA ERROR ',32('*')//
     &       1X,'ERROR IN INPUT DATA FILE: LEVEL ',I2,' IS UNTIED')
 1012 FORMAT(1X,A,' - READ VALUE = ',A7)
 1013 FORMAT(1X,32('*'),' D7DATA ERROR ',32('*')//
     &  1X,'SUBROUTINE HAS BEEN CALLED USING THE OLD ARGUMENT LIST.' //
     &  1X,'MAKE THE FOLLOWING CHANGES TO THE VARIABLES PASSED TO'   /
     &  1X,'THE SUBROUTINE & THEN RECOMPILE AND LINK YOUR PROGRAM.'  //
     &  4X,'1) Increase the size of CSTRGA array from C*12 to C*18'  /
     &  4X,'2) Increase the only  dimension of SCEF from 8 to 14'    /
     &  4X,'3) Increase the first dimension of SCOM from 8 to 14'    )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
