C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7wr12.for,v 1.5 2019/06/21 14:21:29 mog Exp $ Date $Date: 2019/06/21 14:21:29 $

      SUBROUTINE D7WR12( IUNIT  , LWR12   ,
     &                   NDMET  , NDCONF  , NDORB   , NDBNDL  ,
     &                   CSELR  , CSELD   , CSELS   , CSELP   , CSELL  ,
     &                   IZ0    , IZ      , ISG     ,
     &                   ISPSYS , ITYPEA  , IZETA4  ,
     &                   NSYS   , NTRANS  , N0A     ,
     &                   NCTAA  , NZETA   , N1A     ,
     &                   Z1     , CI4     , EIONA   ,
     &                   PARMR  , PARMD   ,
     &                   IBNDL  ,
     &                   DEBNDL , FBNDL   , GBNDL   , PNLSA   ,
     &                   WVSPEC , IFSPEC  ,
     &                   DESPEC , FSPEC   , GSPEC   , SNLSA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D7WR12 *******************
C
C  PURPOSE:  TO OUTPUT DATA TO ATOMPARS PASSING FILE.
C            DATA  FOR INITIATING AN ADAS408 DATA PREPARATION RUN
C
C  CALLING PROGRAM: ADAS407
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT NUMBER FOR RESULTS
C
C
C          (I*4) I         = GENERAL USE
C          (I*4) ISG       = MULTIPLICITY OF GROUND STATE OF RECOMBINED
C                            ION
C          (I*4) ITYPDN()  = SPECIFIES DELTA N FOR TRANSITION TYPE (1-7)
C          (I*4) ITYPMZ()  = SPECIFIES MERTZ CORRECTION ON(1) OR OFF(0)
C                            FOR TRANSITION TYPE (1-7)
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D7LOTZ     ADAS      RETURNS THE LOTZ IONISATION PARAMETERS
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    01/07/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2                          DATE: 13-05-96
C MODIFIED: TIM HAMMOND, TESSELLA SUPPORT SERVICES PLC.
C           - COMMENTED OUT REFERENCES TO VARIABLE IZO ('oh', not 'zero')
C
C VERSION: 1.3                                 DATE: 23-05-2003
C MODIFIED: Martin O'Mullane
C		- Remove all unused variables and reduced length of
C                 parameter list.
C               - In cases where there is no spin system connection
C                 between adjacent ions force it to be ground. The
C                 proper solution is to use IC and not LS input files.
C
C VERSION : 1.5                                 
C DATE    : 19-03-2010
C MODIFIED: Martin O'Mullane
C		- Use nearest integer (NINT) rather than integer part
C                 (INT) when calculating V0 in radiative recombination.
C
C-----------------------------------------------------------------------
      INTEGER   IPDIMD
C-----------------------------------------------------------------------
      PARAMETER ( IPDIMD = 3 )
C-----------------------------------------------------------------------
      INTEGER   IUNIT
      INTEGER   NDMET   , NDCONF  ,
     &          NDORB   , NDBNDL
      INTEGER   I       ,
     &          IBNDL   , ISYS    , IZ0     , ISG
      INTEGER   IZ      , IZ1     , IQ      , J       , K 
      INTEGER   NRRC    , ISRRC   , NDRC    , ISDRC   ,
     &          NCIOS   , NCIOR   , ISCIO   , NPLT    , ISPLT   ,
     &          NPLS    , ISPLS   , i4unit
C-----------------------------------------------------------------------
      REAL*8    CI4     , Z1      , ZETA
C-----------------------------------------------------------------------
      CHARACTER CSELR*1 , CSELD*1 , CSELS*1 , CSELP*1 , CSELL*1
C-----------------------------------------------------------------------
      LOGICAL   LWR12
C-----------------------------------------------------------------------
      INTEGER   NSYS(NDMET)
      INTEGER   NTRANS(NDMET)
      INTEGER   ISPSYS(NDMET,2)  , N0A(NDMET,2) , NZETA(NDMET,2)
      INTEGER   ITYPEA(NDMET,NDCONF)  , N1A(NDMET,NDCONF)  ,
     &          NCTAA(NDMET,NDCONF)
      INTEGER   IZETA4(NDMET,2,NDORB)
      INTEGER   IFSPEC(NDMET)
      INTEGER   ITYPDN(7)      , ITYPMZ(7)
C-----------------------------------------------------------------------
      REAL*8    PARMR(NDMET,2,4)
      REAL*8    PARMD(NDMET,10,NDCONF)
      REAL*8    EIONA(NDMET,2,NDORB)
      REAL*8    DEBNDL(NDBNDL,NDMET) , FBNDL(NDBNDL,NDMET) ,
     &          GBNDL(NDBNDL,NDMET)  , PNLSA(NDBNDL,NDMET)
      REAL*8    DESPEC(NDMET) , FSPEC(NDMET)   ,
     &          GSPEC(NDMET)  , WVSPEC(NDMET)  , SNLSA(NDMET)
      REAL*8    A(IPDIMD)     , B(IPDIMD)      , C(IPDIMD)
C-----------------------------------------------------------------------
      DATA  ITYPDN / 1 , 1 , 1 , 0 , 0 , 1 , 1 /
      DATA  ITYPMZ / 0 , 1 , 1 , 0 , 0 , 1 , 1 /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  FIND CORRECT SPIN SYSTEM FOR RECOMBINED ION GROUND
C-----------------------------------------------------------------------
      
      isys = 0
      DO I = 1, NSYS(1)
        IF(ISPSYS(1,I).EQ.ISG) THEN
            ISYS = I
        ENDIF
      END DO

      if (isys.EQ.0) then
         isys=1
         write(i4unit(-1),3000)'Spin systems mismatch for stage ', iz,
     &                         'Force use of ground spin system'
      endif
      

C-----------------------------------------------------------------------
C  OUTPUT DATA
C-----------------------------------------------------------------------
        IF( .NOT. LWR12 ) THEN
            WRITE(IUNIT,1000) IZ0
        ENDIF

        WRITE(IUNIT,1010)
        IZ1 = IZ+1
        WRITE(IUNIT,1020)IZ1,IZ1,IZ,IZ,IZ
C-----------------------------------------------------------------------
C  RADIATIVE RECOMBINATION DATA
C-----------------------------------------------------------------------
        NRRC  = 0
        ISRRC = 0
        IF(CSELR.EQ.'A') THEN
            IQ=nint(2.0D0*N0A(1,ISYS)**2*(1.0D0-PARMR(1,ISYS,2)))
            WRITE(IUNIT,1030)'A',NRRC,ISRRC
            WRITE(IUNIT,1040)N0A(1,ISYS),IQ
        ELSE
            WRITE(IUNIT,1030)'B',NRRC,ISRRC
            WRITE(IUNIT,1042)N0A(1,ISYS),(PARMR(1,ISYS,I),I=1,4)
        ENDIF
C-----------------------------------------------------------------------
C  DIELECTRONIC RECOMBINATION DATA
C-----------------------------------------------------------------------
        ISDRC = 0
        NDRC  = NTRANS(1)
        IF(CSELD.EQ.'A') THEN
             WRITE(IUNIT,1050)'A',NDRC,ISDRC
             DO 10 I = 1,NDRC
               WRITE(IUNIT,1052)13.6048D0*PARMD(1,3,I),
     &                        PARMD(1,4,I),0.0D0,ITYPDN(ITYPEA(1,I)),
     &                       ITYPMZ(ITYPEA(1,I))
   10        CONTINUE
        ELSE
             WRITE(IUNIT,1050)'B',NDRC,ISDRC
             DO 15 I = 1,NDRC
               WRITE(IUNIT,1054)ITYPEA(1,I),N1A(1,I),NCTAA(1,I),
     &                          PARMD(1,1,I),PARMD(1,2,I),PARMD(1,7,I),
     &                          PARMD(1,3,I)/(IZ1+1.0D0)**2.0D0,
     &                          (PARMD(1,K,I),K=4,6)
   15        CONTINUE
        ENDIF
C-----------------------------------------------------------------------
C  IONISATION DATA
C-----------------------------------------------------------------------
        ISCIO = 0
        IF(CSELS.EQ.'A') THEN
            CALL D7LOTZ( IZ0   , IZ1  ,
     &                   NCIOS ,
     &                   A     , B    ,  C
     &                 )
 
          NCIOR = 0
          WRITE(IUNIT,1070)'A',NCIOS,NCIOR,ISCIO
          DO 20 I = 1,NCIOS
            J = NZETA(1,ISYS)-I+1
               WRITE(IUNIT,1072)13.6048D0*Z1*Z1*EIONA(1,ISYS,J),
     &                          A(I),B(I),C(I),IZETA4(1,ISYS,J)
   20     CONTINUE
        ELSE
          NCIOS = NZETA(1,ISYS)
          NCIOR = 0
          WRITE(IUNIT,1070)'B',NCIOS,NCIOR,ISCIO
          DO 25 I = 1,NCIOS
            ZETA = IZETA4(1,ISYS,I)
            WRITE(IUNIT,1074)ZETA,Z1*Z1*EIONA(1,ISYS,I),CI4
   25     CONTINUE
        ENDIF
C-----------------------------------------------------------------------
C  TOTAL LINE POWER DATA
C-----------------------------------------------------------------------
        ISPLT = 0
        NPLT = IBNDL
        IF(CSELP.EQ.'A') THEN
          WRITE(IUNIT,1090)'A',NPLT,ISPLT
          DO 30 I = 1,NPLT
            WRITE(IUNIT,1092)13.6048D0*DEBNDL(I,1),FBNDL(I,1),
     &                       GBNDL(I,1)*PNLSA(I,1),I-1
   30     CONTINUE
        ELSE
          WRITE(IUNIT,1090)'B',NPLT,ISPLT
          DO 35 I = 1,NPLT
            WRITE(IUNIT,1092)13.6048D0*DEBNDL(I,1),FBNDL(I,1),
     &                       PNLSA(I,1)
   35     CONTINUE
        ENDIF
C-----------------------------------------------------------------------
C  SPECIFIC LINE POWER DATA
C-----------------------------------------------------------------------
        NPLS  = 1
        ISPLS = 0
        IF(CSELL.EQ.'A') THEN
          WRITE(IUNIT,1100)'A',NPLS,ISPLS,WVSPEC(1)
          WRITE(IUNIT,1110)13.6048D0*DESPEC(1),FSPEC(1),GSPEC(1),
     &                     IFSPEC(1)-1
        ELSE
          WRITE(IUNIT,1100)'B',NPLS,ISPLS,WVSPEC(1)
          WRITE(IUNIT,1110)13.6048D0*DESPEC(1),FSPEC(1),SNLSA(1)
        ENDIF

      RETURN

C-----------------------------------------------------------------------

 1000  FORMAT(I5,3X,'??',3X,'??',10X,'    1    0    0',35X,'ADF03')
 1010  FORMAT('----------')
 1020  FORMAT(5I5)
 1030  FORMAT(5X,'RRC#',A1,2I5)
 1040  FORMAT(10X,2I5)
 1042  FORMAT(10X,I5,4F10.3)
 1050  FORMAT(5X,'DRC#',A1,2I5)
 1052  FORMAT(10X,3F10.3,2I5)
 1054  FORMAT(10X,3I5,3F10.3/15X,4F10.3)
 1070  FORMAT(5X,'CIO#',A1,3I5)
 1072  FORMAT(10X,4F10.3,I5)
 1074  FORMAT(10X,3F10.3)
 1090  FORMAT(5X,'PLT#',A1,2I5)
 1092  FORMAT(10X,3F10.3,I5)
 1100  FORMAT(5X,'PLS#',A1,2I5,F10.2)
 1110  FORMAT(10X,3F10.3,I5)
 3000  FORMAT(/1X,30('*'),' D7WR12 WARNING',30('*'),/,1x, A, i3,
     &        /,1x,a,/,1x,75('*'))

C-----------------------------------------------------------------------

      END
