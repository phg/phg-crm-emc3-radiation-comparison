C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7out0.for,v 1.4 2012/07/18 18:41:26 mog Exp $ Date $Date: 2012/07/18 18:41:26 $

      SUBROUTINE D7OUT0( IUNIT  , DATE   , PRGTYP , DSNC44 , DSNP44 ,
     &                   TITLED , IZ     , IZ0    , IZ1    , BWNO   ,
     &                   ICNTE  , ICNTP  , ICNTR  , ICNTH  ,
     &                   IL     ,
     &                   IA     , CSTRGA , ISA    , ILA    , XJA  , WA ,
     &                   ER     ,
     &                   NV     , TSCEF  , CADAS
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7OUT0 *********************
C
C  PURPOSE: TO OUTPUT ION SPECIFICATIONS, INDEXED ENERGY LEVELS AND
C           WAVE NUMBERS RELATIVE  TO GROUND TO STREAM 'IUNIT'.
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  IUNIT  = OUTPUT STREAM NUMBER
C  INPUT :  (C*8)  DATE   = CURRENT DATE AS 'DD/MM/YY'
C  INPUT :  (C*1)  PRGTYP = PROGRAM TYPE
C  INPUT :  (C*80) DSNC44 = INPUT COPASE DATA SET NAME
C  INPUT :  (C*80) DSNP44 = INPUT PARENT COPASE DATA SET NAME
C
C  INPUT :  (C*3)  TITLED = ELEMENT SYMBOL.
C  INPUT :  (I*4)  IZ     =  RECOMBINED ION CHARGE
C  INPUT :  (I*4)  IZ0    =         NUCLEAR CHARGE
C  INPUT :  (I*4)  IZ1    = RECOMBINING ION CHARGE
C                            (NOTE: IZ1 SHOULD EQUAL IZ+1)
C  INPUT :  (R*8)  BWNO   = IONISATION POTENTIAL (CM-1)
C
C  INPUT :  (I*4)  ICNTE  = NUMBER OF ELECTRON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTP  = NUMBER OF PROTON IMPACT TRANSITIONS INPUT
C  INPUT :  (I*4)  ICNTR  = NUMBER OF FREE ELECTRON RECOMBINATIONS INPUT
C  INPUT :  (I*4)  ICNTH  = NO. OF CHARGE EXCHANGE RECOMBINATIONS INPUT
C
C  INPUT :  (I*4)  IL     = NUMBER OF ENERGY LEVELS
C
C  INPUT :  (I*4)  IA()   = ENERGY LEVEL INDEX NUMBER
C  INPUT : (C*18) CSTRGA()= NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
C  INPUT :  (I*4)  ISA()  = MULTIPLICITY FOR LEVEL 'IA()'
C                           NOTE: (ISA-1)/2 = QUANTUM NUMBER (S)
C  INPUT :  (I*4)  ILA()  = QUANTUM NUMBER (L) FOR LEVEL 'IA()'
C  INPUT :  (R*8)  XJA()  = QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
C                           NOTE: (2*XJA)+1 = STATISTICAL WEIGHT
C  INPUT :  (R*8)  WA()   = ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL
C                           'IA()'
C
C  INPUT : (R*8)  ER()    = ENERGY RELATIVE TO LEVEL 1 (RYDBERGS)
C                           FOR LEVEL 'IA()'
C
C  INPUT : (I*4)  NV      = INPUT DATA FILE: NUMBER OF GAMMA/TEMPERATURE
C                           PAIRS FOR A GIVEN TRANSITION.
C  INPUT : (R*8)  TSCEF(,) = INPUT DATA FILE: ELECTRON TEMPERATURES
C                            1ST DIMENSION: TEMPERATURE (NOTE: TE=TP=TH)
C                            2ND DIMENSION: 1 => KELVIN  (IFOUT=1)
C                                           2 => EV      (IFOUT=2)
C                                           3 => REDUCED (IFOUT=3)
C
C          (R*8)  WN2RYD  = PARAMETER =
C                           WAVE NUMBER (CM-1) TO RYDBERG CONVERSION
C
C          (R*8)  BRYDO   = IONISATION POTENTIAL (RYDBERGS)
C          (R*8)  BWN     = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           WAVE NUMBERS (CM-1).
C          (R*8)  BRYD    = ENERGY RELATIVE TO IONISATION POTENTIAL IN
C                           RYDBERGS.
C
C          (I*4)  I       = GENERAL USE
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR:  H. P. Summers, jet
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    14/06/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    INCREASED FILE NAME LENGTH TO 80 CHARACTERS
C
C VERSION : 1.3
C DATE    : 06-01-2004
C MODIFIED: Martin O'Mullane
C            - Extend format statements to accept large numbers
C              of transitions. 
C
C VERSION : 1.4
C DATE    : 18-07-2012
C MODIFIED: Martin O'Mullane
C               - Increase length of configuration strings to 
C                 40 characters.
C
C-----------------------------------------------------------------------
      REAL*8     WN2RYD
C-----------------------------------------------------------------------
      PARAMETER( WN2RYD = 9.11269D-06 )
C-----------------------------------------------------------------------
      INTEGER    I
      INTEGER    IUNIT         ,
     &           IZ            , IZ0            , IZ1        ,
     &           ICNTE         , ICNTP          , ICNTR      , ICNTH  ,
     &           IL            , NV
C-----------------------------------------------------------------------
      REAL*8     BWNO          , BWN            , BRYDO      , BRYD
C-----------------------------------------------------------------------
      CHARACTER  TITLED*3      , DATE*8         , PRGTYP*1   ,
     &           DSNC44*80     , DSNP44*80      , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    IA(IL)        , ISA(IL)        , ILA(IL)
C-----------------------------------------------------------------------
      REAL*8     XJA(IL)       , WA(IL)         , ER(IL)
      REAL*8     TSCEF(14,3)
C-----------------------------------------------------------------------
      CHARACTER  CSTRGA(IL)*40
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------

      BRYDO = WN2RYD * BWNO

C-----------------------------------------------------------------------
C OUTPUT ION AND ENERGY LEVEL DETAILS
C-----------------------------------------------------------------------
C
         IF (PRGTYP.EQ.'M') THEN
            WRITE(IUNIT,1001) ' PARAMETER GENERATION ','ADAS407',DATE
         ELSE
            WRITE(IUNIT,1001) 'LINE POWER CALCULATION','ADAS206',DATE
         ENDIF
      WRITE(IUNIT,1002) DSNC44,DSNP44
      WRITE(IUNIT,1003) TITLED , IZ , IZ0 , IZ1 , BWNO , BRYDO
      WRITE(IUNIT,1004)
C
         DO 1 I=1,IL
            BWN  = BWNO  - WA(I)
            BRYD = BRYDO - ER(I)
            WRITE(IUNIT,1005) IA(I)  , CSTRGA(I) ,
     &                        ISA(I) , ILA(I)    , XJA(I) ,
     &                        WA(I)  , ER(I)     ,
     &                        BWN    , BRYD
    1    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT 'INPUT COPASE FILE' TEMPERATURES
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1006)
         DO 2 I=1,NV
            WRITE(IUNIT,1007) I , TSCEF(I,1) , TSCEF(I,2) , TSCEF(I,3)
    2    CONTINUE
C
C-----------------------------------------------------------------------
C OUTPUT 'INPUT COPASE FILE' INFORMATION
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1008) ICNTE , ICNTP , ICNTH , ICNTR
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(A79)
 1001 FORMAT( / ,27('*'),' TABULAR OUTPUT FROM ',A22,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,27('*')/)
 1002 FORMAT( / ,'INPUT INITIAL ION COPASE FILE NAME: ',A80/
     &        1X,'INPUT PARENT  ION COPASE FILE NAME: ',A80/)
 1003 FORMAT( / ,1X,'ION',6X,'NUCLEAR',8X,'RECOMBINING',6X,
     &           7('-'),' IONIZATION POTENTIAL ',7('-')/
     &           9X,'CHARGE (Z0)',4X,'ION CHARGE (Z1)',
     &        4X,'(wave number <cm-1>)',5X,'(rydbergs)'/
     &        1X,78('-')/
     &        1X,1A3,I2,7X,I3,14X,I3,10X,F15.0,5X,F15.7//)
 1004 FORMAT( / ,55('-'),' ENERGY LEVELS ',55('-')/
     &        2X,'INDEX',4X,'CONFIGURATION',3X,'(2S+1)L(J)',7X,
     &           5('-'),' ENERGY RELATIVE TO LEVEL 1 ',6('-'),4X,
     &           'ENERGY RELATIVE TO IONISATION POTENTIAL'/
     &       45X,2( '(wave number <cm-1>)',5X,'(rydbergs)' ,8X )/
     &        1X,125('-'))
 1005 FORMAT( 1X,I4,2X,A,2X,'(',I1,')',I1,'(',F6.1,')',
     &        2(6X,F15.0,5X,F15.7,2X) )
 1006 FORMAT( / / / ,'-- INPUT COPASE FILE TEMPERATURES: (TE=TP=TH) --'/
     &            1X,'INDEX',4X,'(kelvin)',8X,'(eV)',8X,'(reduced)'/
     &            1X,48('-'))
 1007 FORMAT(2X,I2,1P,3(4X,D10.2))
 1008 FORMAT( / / / ,'INPUT COPASE FILE INFORMATION:'/1H ,30('-')/
     &        / ,' NUMBER OF ELECTRON IMPACT TRANSITIONS    =',1X,I5/
     &       1H ,'NUMBER OF PROTON   IMPACT TRANSITIONS    =',1X,I5/
     &       1H ,'NUMBER OF CHARGE EXCHANGE RECOMBINATIONS =',1X,I5/
     &       1H ,'NUMBER OF FREE   ELECTRON RECOMBINATIONS =',1X,I5)
C-----------------------------------------------------------------------
      RETURN
      END
