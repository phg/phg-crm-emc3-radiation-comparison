C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7wr11.for,v 1.4 2004/07/06 13:24:02 whitefor Exp $ Date $Date: 2004/07/06 13:24:02 $
      SUBROUTINE D7WR11( IUNIT  ,
     &                   NDMET  , NDRHO   , NDTHET  , NDREP   , NDCONF ,
     &                   NDORB  , NDQDN   ,
     &                   IZ0    , IZ      ,
     &                   NPMET  , NRHO    , NTHETA  , INREP   ,
     &                   ISPRT  , ISPSYS  , ITYPEA  , IZETA4  ,
     &                   NCUT   , NSYS    , NTRANS  , N0A     , NREP   ,
     &                   NCTAA  , NZETA   , N1A     ,
     &                   Z1     , EDISGP  , SCALGP  , CI4     ,
     &                   ALFRA  , ADIELO  , EIONA   , SAO     ,
     &                   THETA  , RHO     , RHOP    ,
     &                   PARMR  , PARMD   ,
     &                   WNREP  ,
     &                   TRMPRT ,
     &                   NALCM  , ISALCM  , NALCP   , ISALCP  ,
     &                   QDN
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D7WR11 *******************
C
C  PURPOSE:  TO OUTPUT DATA TO MAINBN PASSING FILE.
C            DATA  FOR INITIATING A MAINBNS BUNDLE-NS CALCULATION
C
C  CALLING PROGRAM: ADAS407
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT UNIT NUMBER FOR RESULTS
C  INPUT : (I*4)  IZ      = RECOMBINED ION CHARGE
C                              (1ST COPASE FILE)
C  INPUT : (I*4)  NDQDN   = MAX. NUMBER OF N-SHELLS FOR QUANTUM DEFECTS
C  INPUT : (I*4)  NALCM   = NUMBER OF SPIN DISTINGUISED 
C                            METASTABLES
C  INPUT : (I*4)  ISALCM()= SPIN OF ENERGY ORDERED SPIN 
C                           DISTINQUISHED METASTABLE
C                           1ST. DIM: DISTINGUISHED METASTABLE INDEX  
C  INPUT : (I*4)  NALCP     NUMBER OF SPIN DISTINGUISHED
C                           PARENTS 
C  INPUT : (I*4)  ISALCP()= SPIN OF ENERGY ORDERED SPIN 
C                           DISTINGUISHED PARENT
C                           1ST. DIM: DISTINGUISHED PARENT INDEX  
C  INPUT : (R*8)  QDN()   = QUANTUM DEFECT FOR N-SHELLS.  NON-ZERO ONLY
C                           FOR ADF04 FILES WITH ORBITAL ENERGY DATA
C                           1ST. DIM: N-SHELL (1<=N<=NDQDN)
C
C
C          (I*4) I         = GENERAL USE
C          (I*4) IFIRST    = GENERAL USE
C          (I*4) ILAST     = GENERAL USE
C
C NOTE:
C          THIS OUTPUT DATA IS FOR SUBSEQUENT INPUT TO A BACKGROUND
C          EXECUTION OF THE POPULATION  PROGRAM 'MAINBNS'.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XFESYM     ADAS      OBTAIN ELEMENT SYMBOL FROM NUCL. CHARGE
c          XXSLEN     ADAS      FIRST AND LAST NON-BLANK CHRS. OF STRNG 
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    01/07/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    22ND APRIL 1996
C
C VERSION: 1.1				DATE: 22-04-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION.
C
C VERSION: 1.2				DATE: 20-08-96
C MODIFIED: HUGH SUMMERS + WILLIAM OSBORN
C	    - MADE THE REPRESENTATIVE LEVEL LIST
C             TO THE MAINBN FILE BEGIN AT 1.
C
C
C VERSION: 1.3				DATE: 14-08-97
C MODIFIED: HUGH SUMMERS
C	    - ADDED SPIN DISTINQUISHED PARENT AND METASTABLE 
C             IDENTIFICATION, COUNTERS AND POINTERS.  USING
C             IGRD TO MARK THE SPIN SYSTEM NOW.  PUT OUT
C             TRUE QUANTUM DEFECTS.  IZ INCLUDED IN CALL
C             VARAIBLES. 
C
C VERSION: 1.4				DATE: 07-12-98
C MODIFIED: Martin O'Mullane
C	    - Extended MAINBNS namelist to give the ionisation (adf07)
C             and DR cross reference file in the adf25 driver.
C
C-----------------------------------------------------------------------
      INTEGER   IUNIT
      INTEGER   NDMET   , NDRHO   , NDTHET  , NDREP   , NDCONF  ,
     &          NDORB   , NDQDN   , NDEF
      INTEGER   I       , IMAX    , IN      , INREP   , I0      ,
     &          IPAR    , ISHEL   , ISYS    , IT      ,
     &          ITRANS  , IZ0     , IZ      ,
     &          JPRT    , K       , IFIRST  , ILAST   ,
     &          NIGRP   , NRGRP   ,
     &          NLOW    , NPMET   , NRHO    , NTHETA  , NTRY
      INTEGER   NALCM   , NALCP   , IGRD
C-----------------------------------------------------------------------
      REAL*8    CION    , CI4     , CPY     ,
     &          EDISGP  , PRTWHT  , SCALGP  ,
     &          TR      , T3      ,
     &          W       , W1      , Z1      , Z12     , Z17
C-----------------------------------------------------------------------
      CHARACTER ESYM*2  , SEQSYM*2, XRMEMB*8, XFESYM*2
C-----------------------------------------------------------------------
      INTEGER   NREP(NDREP)    , NSYS(NDMET)
      INTEGER   ISPRT(NDMET)   , NCUT(NDMET)    , NTRANS(NDMET)
      INTEGER   ISPSYS(NDMET,2)  , N0A(NDMET,2) , NZETA(NDMET,2)
      INTEGER   ITYPEA(NDMET,NDCONF)  , N1A(NDMET,NDCONF)  ,
     &          NCTAA(NDMET,NDCONF)
      INTEGER   IZETA4(NDMET,2,NDORB)
      INTEGER   ISALCM(NDMET)  , ISALCP(NDMET)
C-----------------------------------------------------------------------
      REAL*8    THETA(NDTHET)  , RHO(NDRHO)      , RHOP(NDRHO)
      REAL*8    WNREP(NDREP)
      REAL*8    PARMR(NDMET,2,4)
      REAL*8    PARMD(NDMET,10,NDCONF)
      REAL*8    EIONA(NDMET,2,NDORB)
      REAL*8    ALFRA(NDMET,2,NDTHET)
      REAL*8    ADIELO(NDMET,2,NDTHET)
      REAL*8    SAO(NDMET,2,NDTHET)
      REAL*8    QDN(NDQDN)
C
C-----------------------------------------------------------------------
      CHARACTER TRMPRT(NDMET)*2
C-----------------------------------------------------------------------
      DATA   W/0.0D0/ , W1/1.0D0/ , CPY/1.0D0/ , CION/1.0D0/
      DATA   NIGRP/1/ , NRGRP/0/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       Z17  = Z1**7
       Z12  = Z1*Z1
       TR   = 1.0D6*Z1*Z1
       SEQSYM = XFESYM(IZ0-IZ)
       ESYM   = XFESYM(IZ0)
       XRMEMB = ESYM//'      '
       CALL XXSLEN(XRMEMB,IFIRST,ILAST)
       IF(IZ.LT.10) THEN
           WRITE(XRMEMB(ILAST+1:ILAST+1),'(I1)') IZ
           XRMEMB(ILAST+2:ILAST+2) = 'n'
       ELSE
           WRITE(XRMEMB(ILAST+1:ILAST+2),'(I2)') IZ
           XRMEMB(ILAST+3:ILAST+3) = 'n'
       ENDIF    
C
       WRITE(IUNIT,3050) IZ0, NPMET, SEQSYM, XRMEMB
       WRITE(IUNIT,3035)NRHO,NTHETA,TR,W,Z1,CPY,CION,W1
       WRITE(IUNIT,3036)
       WRITE(IUNIT,3037)(Z17*RHO(IN),IN=1,NRHO)
       WRITE(IUNIT,3037)(Z12*THETA(IT),IT=1,NTHETA)
       WRITE(IUNIT,3037)(Z17*RHOP(IN),IN=1,NRHO)
       WRITE(IUNIT,3037)(Z12*THETA(IT),IT=1,NTHETA)
       WRITE(IUNIT,3038)
C
       NLOW=1
C
       DO 923 IPAR=1,NPMET
         DO 922 ISYS=1,NSYS(IPAR)
           NTRY=N0A(IPAR,ISYS)
           IF(IPAR.EQ.1 .AND. ISYS.EQ.1) THEN
               NLOW=NTRY
           ENDIF
           IF(NTRY.LT.NLOW) THEN
               NLOW=NTRY
           ENDIF
  922    CONTINUE
  923  CONTINUE
C
       I0=0
C
  925  I0=I0+1
C------------------------- ADJUSTMENT 06/06/96 ---------------------
C      IF(NLOW.GT.NREP(I0))GO TO 925
C-------------------------------------------------------------------
       IMAX=INREP-I0+1
       WRITE(IUNIT,3039)NREP(I0),NREP(INREP)+20,IMAX
       WRITE(IUNIT,3039)(NREP(I),I=I0,INREP)
       WRITE(IUNIT,3037)(WNREP(I),I=I0,INREP)
       WRITE(IUNIT,3032)
C
       DO 942 IPAR=1,NPMET
         DO 940 ISYS=1,NSYS(IPAR)
           DO 927 K=1,NALCM
             IF(ISPSYS(IPAR,ISYS).EQ.ISALCM(K)) THEN
                 IGRD = K
                 GO TO 928
             ENDIF
  927      CONTINUE
           NALCM=NALCM+1
           ISALCM(NALCM)=ISPSYS(IPAR,ISYS)
           IGRD = NALCM             
  928      WRITE(IUNIT,3033) IPAR,TRMPRT(IPAR),ISPRT(IPAR),
     &                       NCUT(IPAR),IGRD,ISPSYS(IPAR,ISYS)
           NDEF = MIN0(NDQDN-N0A(IPAR,ISYS)+1,5)
           WRITE(IUNIT,3055) N0A(IPAR,ISYS) ,
     &                       NDEF,(QDN(K),K=N0A(IPAR,ISYS),
     &                                      N0A(IPAR,ISYS)+NDEF-1)
           WRITE(IUNIT,3040)N0A(IPAR,ISYS),(PARMR(IPAR,ISYS,K),K=1,4)
           WRITE(IUNIT,3039) NTRANS(IPAR)
C
           DO 930 ITRANS=1,NTRANS(IPAR)
             T3=PARMD(IPAR,3,ITRANS)/(Z1+1.0D0)**2
             WRITE(IUNIT,3041) ITYPEA(IPAR,ITRANS),N1A(IPAR,ITRANS),
     &                       PARMD(IPAR,1,ITRANS),PARMD(IPAR,2,ITRANS),
     &                       NCTAA(IPAR,ITRANS),T3,
     &                       PARMD(IPAR,4,ITRANS),PARMD(IPAR,7,ITRANS),W
  930      CONTINUE
C
           WRITE(IUNIT,3044) EDISGP,SCALGP
           WRITE(IUNIT,3051) NIGRP,NRGRP
C
           DO JPRT = 1,NPMET
             PRTWHT = 0.0
             IF(JPRT .EQ. IPAR) THEN
                 PRTWHT = 1.000
             ENDIF
             WRITE(IUNIT,3052) JPRT, PRTWHT
           ENDDO
C
           WRITE(IUNIT,3039) NZETA(IPAR,ISYS)
           WRITE(IUNIT,3042) CI4, (IZETA4(IPAR,ISYS,ISHEL),
     &                       EIONA(IPAR,ISYS,ISHEL),
     &                       ISHEL=1,NZETA(IPAR,ISYS))
           WRITE(IUNIT,3037) (ALFRA(IPAR,ISYS,IT)+
     &                       ADIELO(IPAR,ISYS,IT),IT=1,NTHETA)
           WRITE(IUNIT,3037) (SAO(IPAR,ISYS,IT),IT=1,NTHETA)
           IF(IPAR.EQ.NPMET .AND. ISYS.EQ.NSYS(NPMET)) THEN
               WRITE(IUNIT,3032)
           ELSE
               WRITE(IUNIT,3034)
           ENDIF
  940    CONTINUE
  942  CONTINUE
C
C-----------------------------------------------------------------------
C
 3032  FORMAT('---------------------------------------------------------
     &---------------')
 3033  FORMAT('PRT=',I2,'  TRMPRT=  (',1A2,')   SPNPRT=',I2,'  NCTPRT=',
     &I4,' SYSG=',I2,'  SPNSYS=',I2)
 3034  FORMAT('----------')
 3035  FORMAT(2I5,1P,6D10.2)
 3036  FORMAT('    0    4    1    0')
 3037  FORMAT(1P,7D10.2)
 3038  FORMAT('  2.50D 04  0.00D 00')
 3039  FORMAT(14I5)
 3040  FORMAT(I5,4F10.5)
 3041  FORMAT(I2,I5,2F9.4,I5,3F10.5,1P,D10.2)
C3041  FORMAT(2I5,5F10.5,1P,D10.2)
 3042  FORMAT(F5.3,5(I5,F10.5)/5X,5(I5,F10.5)/5X,5(I5,F10.5))
 3044  FORMAT(1H ,14X,'FIT PARAMETERS FOR GP',14X,2F10.5)
 3050  FORMAT(' &FILINFO  NUCCHG=',I2,', NPARNT=',I1,
     &         ', SEQ=','''',1A2,'''',', XRMEMB=','''',
     &            1A8,'''',',  ',/,11x,'IONIZ = ''',' '',',/,
     &            11x,'DRSUP = ''',' ''',
     &          ' &END')
 3051  FORMAT(2I5,'    IBSEL1= 0')
 3052  FORMAT(4X,'JPRT=',I2,3X,'PRTWHT=',F6.3,3X,'IBSEL=  0')
 3055  FORMAT('NMIN=', I2, 3X, 'JDEF=', I2, 3X, 5(F6.3) )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
