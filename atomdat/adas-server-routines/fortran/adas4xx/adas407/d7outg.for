C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas407/d7outg.for,v 1.2 2004/07/06 13:22:56 whitefor Exp $ Date $Date: 2004/07/06 13:22:56 $

       SUBROUTINE D7OUTG( LGHOST ,
     &                    TITLE  , TITLX , DATE ,
     &                    TEMP   , PTOTA , NTEMP ,
     &                    TOSA   , POWESA, NPSPL ,
     &                    POWISA , POWSSA, POWOSA,
     &                    LGRD1  , LDEF1 ,
     &                    XMIN   , XMAX  , YMIN  , YMAX
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D7OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED TRANSITION - PASSES
C	     RELEVANT PARAMETERS TO IDL VIA A PIPE
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                ORIGINAL DATA    (CROSSES    )
C                                SPLINE INTERPOLATED DATA  (FULL CURVE)
C
C            PLOT IS LOG10(POW(W CM**3))  VERSUS LOG10(TEMP(EV))
C
C  CALLING PROGRAM: ADAS407
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = NOT USED.
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, ELEMENT, CHARGE AND  TRANSITION
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  TEMP()  = INPUT DATA FILE: TEMPERATURES (EV)
C  INPUT : (R*8)  PTOTA() = INPUT DATA FILE: SELECTED TRANSITION -
C                           POW FUNCTION (W CM**3) AT 'TEMP()'
C  INPUT : (I*4)  NTEMP   = INPUT DATA FILE: NUMBER OF POW/TEMPERATURE
C                           PAIRS FOR THE SELECTED TRANSITION.
C
C  INPUT : (R*8)  TOSA()  = SPLINE: SELECTED TEMPERATURES (KELVIN)
C  INPUT : (R*8)  POWESA()= EXACT POWER COEFFT. (W CM**3) AT 'TOSA()'
C  INPUT : (R*8)  POWISA()= INIT. FIT POWER COEF. (W CM**3) AT 'TOSA()'
C  INPUT : (R*8)  POWSSA()= SIMPLE FIT POWER COEF.(W CM**3) AT 'TOSA()'
C  INPUT : (R*8)  POWOSA()= OPT. FIT POWER COEF.(W CM**3) AT 'TOSA()'
C  INPUT : (I*4)  NPSPL   = NUMBER  OF SPLINE INTERPOLATED POW/TEMP.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR TEMPERATURE (EV)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR TEMPERATURE (EV)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR POW FUNCT. (W CM**3)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR POW FUNCT. (W CM**3)
C
C
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    08/04/94
C
C UNIX-IDL PORT:
C	   WILLIAM OSBORN, TESSELLA SUPPORT SERVICES PLC.
C
C DATE:    26ND MARCH 1996
C
C VERSION: 1.1				DATE: 26-03-96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. NO CHANGES TO IBM CODE.
C
C VERSION: 1.2				DATE: 23-04-96
C MODIFIED: WILLIAM OSBORN
C	    INCREASED SIZE OF TITLX TO 120
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       INTEGER NTEMP      , NPSPL
       INTEGER I         
C-----------------------------------------------------------------------
       REAL*8 TEMP(NTEMP)  , PTOTA(NTEMP)  ,
     &        TOSA(NPSPL)  , POWESA(NPSPL) , POWISA(NPSPL) ,
     &        POWSSA(NPSPL), POWOSA(NPSPL) ,
     &        XMIN         , XMAX          ,
     &        YMIN         , YMAX
C-----------------------------------------------------------------------
       CHARACTER TITLE*40 , TITLX*120    , DATE*8
C-----------------------------------------------------------------------
       LOGICAL LDEF1      , LGRD1        , LGHOST
C-----------------------------------------------------------------------
      INTEGER PIPEIN, PIPEOU, ZERO, ONE, I4UNIT
      PARAMETER (PIPEIN=5,PIPEOU=6, ONE=1, ZERO=0)
C-----------------------------------------------------------------------

C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C

      WRITE(PIPEOU,'(A40)') TITLE 
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,*)NTEMP
      CALL XXFLSH(PIPEOU)

      DO 1,I=1,NTEMP
         WRITE(PIPEOU,*)TEMP(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE

      DO 2,I=1,NTEMP
         WRITE(PIPEOU,*)PTOTA(I)
         CALL XXFLSH(PIPEOU)
 2    CONTINUE

      WRITE(PIPEOU,*)NPSPL
      CALL XXFLSH(PIPEOU)

      DO 3,I=1,NPSPL
         WRITE(PIPEOU,*)TOSA(I)
         CALL XXFLSH(PIPEOU)
 3       CONTINUE

      DO 4,I=1,NPSPL
         WRITE(PIPEOU,*)POWESA(I)
         CALL XXFLSH(PIPEOU)
 4    CONTINUE

      DO 5,I=1,NPSPL
         WRITE(PIPEOU,*)POWISA(I)
         CALL XXFLSH(PIPEOU)
 5    CONTINUE

      DO 6,I=1,NPSPL
         WRITE(PIPEOU,*)POWSSA(I)
         CALL XXFLSH(PIPEOU)
 6    CONTINUE

      DO 7,I=1,NPSPL
         WRITE(PIPEOU,*)POWOSA(I)
         CALL XXFLSH(PIPEOU)
 7    CONTINUE

      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
	 CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) XMAX
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMIN
	 CALL XXFLSH(PIPEOU)
	 WRITE(PIPEOU,*) YMAX
	 CALL XXFLSH(PIPEOU)
      ENDIF

C
C
C-----------------------------------------------------------------------
C
      RETURN
      END
