CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas411/dbout0.for,v 1.1 2004/07/06 13:28:18 whitefor Exp $ Date $Date: 2004/07/06 13:28:18 $
CX
       SUBROUTINE DBOUT0( IWRITE   , LFSEL  ,
     &                    TITLE    , TITLX  , TITLM , DATE   ,
     &                    iopt     ,
     &                    cprnt    , clev   ,
     &                    ITVAL    , TEVA   , TKEL  ,
     &                    IZ0      , IZ1    , 
     &                    LTRNG    , 
     &                    QDR      ,
     &                    KPLUS1   , COEF   , CADAS
     &                  )
       IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DBOUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING RR DATA UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS411
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM IDL PIPE)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
C                           SET NAME + OTHER INFO
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IOPT    = TYPE OF ANALYSIS
C  INPUT : (I*4)  IP_RES  = resolved parent
C  INPUT : (I*4)  IP_RES  = resolved level
C  INPUT : (I*4)  ITVAL   = NUMBER OF USER ENTERED RECEIVER/DONOR TEMP-
C                           ERATURE PAIRS.
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (I*4)  IZ1     = RECOMBINING ION CHARGE
C
C  INPUT : (L*4)  LTRNG() = .TRUE.  => OUTPUT 'QDR()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TEVA()'.
C                           .FALSE. => OUTPUT 'QDR()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      RECEIVER TEMPERATURE 'TEVA()'.
C                           DIMENSION: TEMP. INDEX
C
C  INPUT : (R*8)  TEVA()  = USER ENTERED: TEMPERATURES (eV)
C  INPUT : (R*8)  TKEL()  = USER ENTERED: TEMPERATURES (kelvin)
C
C  INPUT : (R*8)  QDR()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  RATE-
C                           COEFFICIENTS FOR THE USER ENTERED TEMPERATURE 
C			    (UNITS: cm**3/sec)
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  IZ      = RECOMBINED ION CHARGE
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1      = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT TEMPERATURE. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*2)  XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)  CESYM   = RECEIVER ELEMENT SYMBOL
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CELEM   = RECEIVER ELEMENT NAME
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          XFESYM     ADAS      CHARACTER*2  FUNCTION -
C                               RETURNS ELEMENT SYMBOL FOR GIVEN Z0
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C
C AUTHOR  : H. P. Summers, University of Strathclyde
C           JA8.08
C           TEL.  0141-553-4196
C
C DATE:    10/11/97
C
C UPDATE: 
C
C VERSION: 1.1						DATE: 10-03-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , ITVAL         ,
     &           iopt          ,
     &           IZ0           , IZ1           ,
     &           KPLUS1
      INTEGER    IZ            , I
      integer    len1          , len2
C----------------------------------------------------------------------
      LOGICAL    LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)     , TITLM*(*) , DATE*8   
      CHARACTER  XFESYM*2      , XFELEM*12     ,
     &           C1*1          , 
     &           CESYM*2       , CELEM*12      , 
     &           CADAS*80
      CHARACTER  CSTRNG*120
      character  cprnt*21      , clev*21
C----------------------------------------------------------------------
      REAL*8     TEVA(ITVAL)  , TKEL(ITVAL)  ,
     &           QDR(ITVAL)   ,
     &           COEF(KPLUS1)  
C----------------------------------------------------------------------
      LOGICAL    LTRNG(ITVAL)
C----------------------------------------------------------------------
      DATA       CSTRNG/'BLK'/

C**********************************************************************
C
      CELEM = XFELEM( IZ0 )
      CESYM = XFESYM( IZ0 )
      IZ    = IZ1 - 1

C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &      'RADIATIVE RECOMBINATION RATE-COEFFICIENT INTERROGATION',
     &      'ADAS411' , DATE
      WRITE(IWRITE,1002) TITLE

      call xxslen(titlx,len1,len2)
      WRITE(IWRITE,1003) TITLX(1:len2)


      WRITE(IWRITE,1005) CELEM , IZ0 ,IZ1 , IZ    

      if (iopt.eq.1) then
         write(iwrite,1100)cprnt,clev
      endif	

C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES AND RATE COEFFICIENTS
C---------------------------------------------------------------------
C
      if (iopt.eq.1) then
            
         WRITE (IWRITE,1006)

         DO I=1,ITVAL
           C1 = '*'
           IF (LTRNG(I)) C1 = ' '
           WRITE(IWRITE,1007) TKEL(I)  , C1 , TEVA(I) , QDR(I)
         END DO	

         WRITE (IWRITE,1008)
         WRITE (IWRITE,1009)
         WRITE (IWRITE,1008)
      
      endif
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
            WRITE (IWRITE,1008)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1012) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1013) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1014) TITLM
            WRITE(IWRITE,1008)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(A79)
 1001 FORMAT(//,15('*'),' TABULAR OUTPUT FROM ',A46,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,15('*')/)
 1002 FORMAT(/ ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'RADIATIVE RECOMBINATION ',
     & ' RATE-COEFFICIENTS AS A FUNCTION OF TEMPERATURE'/
     & 2X,77('-')/
     & 21X,'DATA GENERATED USING PROGRAM: ADAS411'/21X,37('-'))
 1003 FORMAT(1H ,'FILE : ',a)

 1005 FORMAT(/,
     &       1H ,31X,8('-'),/
     &       1H ,'Element                       :', 2X,A12 ,/
     &       1H ,'Nuclear charge         (Z0)   :', 2X,I6  ,/
     &       1H ,'Recombining ion charge (Z1)   :', 2X,I6  ,/
     &       1H ,'Recombined ion charge  (Z)    :', 2X,I6   )
     
 1006 FORMAT(/ /,' -------- TEMPERATURE --------',7X,
     &               'RATE COEFFICIENT'/
     &           1H ,6X,'Kelvin',8X,'eV',9X,5X,'cm**3/sec')
 1007 FORMAT(1H ,1P,3X,D10.3,1X,A,4X,D10.3,5x,D12.4)
 1008 FORMAT(79('-'))
 1009 FORMAT('NOTE: * => RATE-COEFFICIENT EXTRAPOLATED FOR THIS ',
     &                     'TEMPERATURE VALUE')
C
 1010 FORMAT (1H / / ,'MINIMAX POLYNOMIAL FIT TO DONOR TEMPERATURES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(RATE-COEFFICIENT<cm**3/sec>) versus ',
     &                'LOG10(DONOR TEMPERATURE<eV>) -')
 1011 FORMAT (/ / ,'MINIMAX POLYNOMIAL FIT TO RECEIVER TEMPERATURES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(RATE-COEFFICIENT<cm**3/sec>) versus ',
     &                'LOG10(RECEIVER TEMPERATURE<eV>) -')
 1012 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1013 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1014 FORMAT ( / ,A79)

 1100 FORMAT(1H ,31X,8('-'),/
     &       1H ,'Initial parent                :', 2X,A21 ,/
     &       1H ,'Resolved level                :', 2X,A21 )
     
 1107 FORMAT(1H ,3X,i9,13X,1pD10.3)
C
C---------------------------------------------------------------------
C
      RETURN
      END
