CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas411/dbispf.for,v 1.1 2004/07/06 13:28:12 whitefor Exp $ Date $Date: 2004/07/06 13:28:12 $
CX
      SUBROUTINE DBISPF( LPEND  ,
     &                   IZ     , IZ0     , IZ1     ,
     &                   NTDIM  , NDTIN   ,
     &                   ITA    , TVALS   ,
     &			 NDPRNT , NDLEV   ,
     &                   NPRNT  , IPA     , CSTRPA  , ISPA    , ILPA ,
     &                   IL     , IA      , CSTRGA  , ISA     , ILA  ,
     &			 lradr ,
     &                   nprnti ,
     &                   iprti  , ispsys  , 
     &                   TITLE  , IOPT    , 
     &                   IP_RES , IL_RES  ,
     &                   ITTYP  , ITVAL   , TIN     ,
     &                   LOSEL  , LFSEL   , LGRD1   , LDEF1   ,
     &                   TOLVAL , 
     &                   XL1    , XU1     , YL1     , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DBISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS411
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C  INPUT : (I*4)   NTDIM    = MAX. NO. OF TEMPERATURES ALLOWED IN
C                             DATA SET
C  INPUT : (I*4)   NDTIN    = MAXIMUM NUMBER OF INPUT TEMPERATURES  
C                             ALLOWED.
C
C
C  INPUT : (I*4)   ITA      = INPUT FILE - NO. TEMPERATURES
C
C  INPUT : (R*8)   TVALS(,) = INPUT DATA FILE: TEMPERATURES
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: 1 => KELVIN   (ITTYP=1)
C                                            2 => EV       (ITTYP=2)
C                                            3 => REDUCED  (ITTYP=3)
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   IOPT     = 1 => AUGER RATES PLOT
C                           = 2 => RESOLVED STATE SELECTIVE DR PLOT
C                           = 3 => REPRESENTATIVE BUNDLE-N DR PLOT
C
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)   ITVAL    = NUMBER OF INPUT TEMPERATURE PAIRS (1->20)
C  OUTPUT: (R*8)   TIN()    = USER ENTERED VALUES -
C                             TEMPERATURES (UNITS: SEE 'ITTYP')
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  CALLS XXFLSH COMMAND TO CLEAR PIPE
C
C AUTHOR:  H. P. Summers, University of Strathclyde
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    10/11/97
C
C UPDATE:  2
C
C VERSION: 1.1						DATE: 10-03-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      integer   itprnt      , itlev
C-----------------------------------------------------------------------
      parameter ( itprnt=10 , itlev=1000 )
C-----------------------------------------------------------------------
      INTEGER    NDTIN   , NTDIM  ,
     &           ITTYP   , ITVAL  , IOPT    ,
     &           IP_RES  , IL_RES 
      INTEGER    nprnti
      integer    IZ     , IZ0    , IZ1 
C-----------------------------------------------------------------------
      REAL*8     TOLVAL       ,
     &           XL1           , XU1           , YL1          , YU1
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , 
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    ITA  
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)
      REAL*8     TVALS(NTDIM,3)
C-----------------------------------------------------------------------
      character cprnt(itprnt)*21        , clev(itlev)*21
      character cstrl*1
C-----------------------------------------------------------------------
      INTEGER   NDPRNT      , NDLEV     
      INTEGER   NPRNT       , IL         
C-----------------------------------------------------------------------
      INTEGER   IPA(NDPRNT) , ISPA(NDPRNT)   , ILPA(NDPRNT)
      INTEGER   IA(NDLEV)   , ISA(NDLEV)     , ILA(NDLEV)
      
      integer   iradr(itlev,itprnt)

      INTEGER   IPRTI(NDPRNT)  
      INTEGER   ISPSYS(NDPRNT,NDPRNT,2)
C-----------------------------------------------------------------------
      CHARACTER CSTRPA(NDPRNT)*(*) , CSTRGA(NDLEV)*(*)
C-----------------------------------------------------------------------
      LOGICAL   Lradr(NDLEV,NDPRNT) 
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------
      INTEGER    PIPEIN , PIPEOU   ,
     &           I      , J        ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C Construct strings of info for selection
C 
C resolved RR

	do i = 1,nprnt
	  write(cprnt(ipa(i)),100)cstrpa(ipa(i)),ispa(ipa(i)),
     &	  cstrl(ilpa(ipa(i)))
       end do
       
       do i = 1,il
         write(clev(ia(i)),100)cstrga(ia(i)),isa(ia(i)),
     &	 cstrl(ila(ia(i)))
       end do
       
 100   format(a18,i2,a1)     

C make an integer array of lradr for IDL

       do i = 1,nprnt
         do j = 1,il
           if (lradr(j,i)) then
              iradr(j,i) = 1
           else
              iradr(j,i) = 0
           endif
         end do
       end do
          
      
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C
C start with ion charges
 
	WRITE(PIPEOU,*) iz
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) iz0
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) iz1
	CALL XXFLSH(PIPEOU)

C now temperature information

	WRITE(PIPEOU,*) NTDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) ITA
	CALL XXFLSH(PIPEOU)


C resolved DR

	WRITE(PIPEOU,*) NDPRNT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) NDLEV
	CALL XXFLSH(PIPEOU)

	WRITE(PIPEOU,*) NPRNT
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*) IL
	CALL XXFLSH(PIPEOU)


        DO  J=1,3
          DO I=1, ITA
            WRITE(PIPEOU,'(E12.4)') TVALS(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO

C cprnt and clev will be output in increasing order


	DO I=1, NPRNT
          WRITE(PIPEOU,'(A21)') CPRNT(I)
          CALL XXFLSH(PIPEOU)
        END DO

	DO I=1, IL
          WRITE(PIPEOU,'(A21)') CLEV(I)
          CALL XXFLSH(PIPEOU)
        END DO

        DO  J=1,nprnt
          DO I=1, il
            WRITE(PIPEOU,'(i1)') iradr(I,J)
            CALL XXFLSH(PIPEOU)
          END DO
        END DO



C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE FROM IDL
 
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
 
	READ(PIPEIN,'(A40)') TITLE
 
	READ(PIPEIN,*) ITTYP
	READ(PIPEIN,*) IOPT
	READ(PIPEIN,*) IP_RES 
	READ(PIPEIN,*) IL_RES 
	

	if (iopt.ne.0) then
	
	   READ(PIPEIN,*) ITVAL
	   READ(PIPEIN,*) (TIN(I), I=1,itval)

	   READ(PIPEIN,*) ILOGIC
	   IF (ILOGIC .EQ. 1) THEN
	      LFSEL = .TRUE.
	   ELSE
	      LFSEL = .FALSE.
	   END IF

	   READ(PIPEIN,*) TOLVAL
	   TOLVAL = TOLVAL / 100.0

	endif

C convert from IDL 0 index to fortran

	IP_RES  = IP_RES  + 1     	
	IL_RES  = IL_RES  + 1


 200    continue
C
C-----------------------------------------------------------------------
C


      RETURN
      END
