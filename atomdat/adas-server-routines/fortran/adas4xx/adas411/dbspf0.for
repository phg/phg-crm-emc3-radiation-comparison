CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas411/dbspf0.for,v 1.1 2004/07/06 13:28:26 whitefor Exp $ Date $Date: 2004/07/06 13:28:26 $
CX
      SUBROUTINE DBSPF0( REP    , DSFULL , LDSEL )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: DBSPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS411
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME
C
C  OUTPUT: (L*4)   LDSEL   = .TRUE.  => IONATOM DATA SET INFORMATION
C                                       TO BE DISPLAYED BEFORE RUN.
C                          = .FALSE. => IONATOM DATA SET INFORMATION
C                                       NOT TO BE DISPLAYED BEFORE RUN.
C
C          (I*4)   PIPEIN  = UNIT NO. FOR OUTPUT TO PIPE
C	   (I*4)   PIPEOU  = UNIT NO. FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  H. P. Summers
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    10/11/97
C
C UPDATE:  
C
C VERSION: 1.1						DATE: 10-03-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSFULL*120
C-----------------------------------------------------------------------
      LOGICAL     LDSEL
C-----------------------------------------------------------------------
      INTEGER PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------

C READ FROM IDL PIPE

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL


C
C-----------------------------------------------------------------------
C
      RETURN
      END
