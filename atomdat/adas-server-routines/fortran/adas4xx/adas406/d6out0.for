CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6out0.for,v 1.4 2004/07/06 13:19:40 whitefor Exp $ Date $Date: 2004/07/06 13:19:40 $
CX
       SUBROUTINE D6OUT0( IWRITE   , IMDIMD , NDLINE , NDCOMP , NDRAT  ,
     &                    NDFILE   ,
     &                    TITLE    , DATE   ,
     &                    IZ0      , LPART  , YEAR   , YEARDF ,
     &                    LSELA    , LEXSA  , LDEFA  , FILTR  , NFILE  ,
     &                    DSNINC   , DSPECA ,
     &                    NTDIM    , ITMAX    ,
     &                    TEV      , TEVH   , DENS   , DENSH  , TIMEF  ,
     &                    IZL      , IZH    , NSTAGE , NMSUM  ,
     &                    POPTIT   , FABUN0 , FPABUN ,
     &                    ERBEQ    , ERCEQ  , ELTEQ  , ERAD   ,
     &                    NLINE    , NCOMP  ,
     &                    IZION    , IMET   , CIMET  , INDPH  , CINDPH ,
     &                    IFILE    , TITL   ,
     &                    GCFPEQ   , LPEC   , GCF    , NRAT     ,
     &                    ILINE    , JLINE  , TITR   , RATA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6OUT0 *********************
C
C  PURPOSE:  TO  PRINT PRIMARY OUTPUT FROM TRANSIENT IONISATION PROGRAM
C            ADAS406
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE   = OUTPUT UNIT FOR RESULTS
C  INPUT : (I*4)  IMDIMD   = MAXIMUM NUMBER OF CHARGE/MET STATES
C  INPUT : (I*4)  NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)  NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C  INPUT : (I*4)  NDRAT    = MAXIMUM NUMBER OF LINE RATIOS ALLOWED
C  INPUT : (I*4)  NDFILE   = MAXIMUM NUMBER OF EMISSIVITY FILES
C
C  INPUT : (C*40) TITLE    = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*8)  DATE     = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  IZ0      = INPUT FILE: EMITTING ION NUCLEAR CHARGE
C  INPUT : (L*4)  LPART    = .TRUE.  => PARTIAL DATA SELECTED
C                          = .FALSE. => STANDARD DATA SELECTED
C  INPUT : (C*2)  YEAR     = SELECTED YEAR -  TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF   = DEFAULT YEAR - TWO DIGIT YEAR NUMBER
C  INPUT : (L*4)  LSELA()  = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       INDEX SELECTED
C                          = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                       NOT SELECTED
C  INPUT : (L*4)  LEXSA()  = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       SELECTED INDEX EXISTS
C                          = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                       FOR THIS SELECTED INDEX
C  INPUT : (L*4)  LDEFA()  = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       DEFAULT YEAR INDEX EXISTS
C                          = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS DEFAULT YEAR INDEX
C  INPUT : (C*7)  FILTR    = STRING GIVING FILTER NAME PART OF FILES
C  INPUT : (I*4)  NFILE    = NUMBER OF PEC FILES ACCESSED
C  INPUT : (C*80) DSNINC   = NAME OF SCRIPT FILE
C  INPUT : (C*44) DSGCFPEQ() = NAMES OF PHOTON EMIISVITY COEFFICIENT FILES
C  INPUT : (I*4)  NTDIM    = MAXIMUM NUMBER OF TE/NE PAIRS
C  INPUT : (I*4)  ITMAX    = NUMBER OF OUTPUT TEMPERATURES
C
C  INPUT : (R*8)  TEV()    = SELECTED ELECTRON TEMPERATURES (EV)
C  INPUT : (R*8)  TEVH()   = SELECTED HYDROGEN TEMPERATURES (EV)
C  INPUT : (R*8)  DENS()   = SELECTED ELECTRON DENSITIES (CM-3)
C  INPUT : (R*8)  DENSH()  = SELECTED HYDROGEN DENSITIES (CM-3)
C  INPUT : (R*8)  TIMEF   = INTEGRATION TIME (SEC)
C  INPUT : (I*4)  IZL      = LOWEST ION CHARGE+1
C  INPUT : (I*4)  IZH      = HIGHEST ION CHARGE +1 (EXCL BARE NUCL.)
C  INPUT : (I*4)  NSTAGE   = NUMBER OF IONISATION STAGES
C  INPUT : (I*4)  NMSUM    = TOTAL METASTABLE STATE SUM
C  INPUT : (R*8)  POPTIT() = (ION/MET) POPULATION TITLE
C  INPUT : (R*8)  FPABUN(,)= (ION/MET) POPULATION FRACTION AT T=TIMEF
C                            1ST.DIM: TEMPERATURE INDEX
C                            2ND.DIM: (ION/MET) STATE INDEX
C  INPUT : (R*8)  FABUN0() = INITIAL METASTABLE POPULATION FRACTIONS
C                            1ST DIMENSION: METASTABLE INDEX
C  INPUT : (R*8)  ERBEQ()  = TRANS. RECOM/BREMS. EN. EXCESS FUNC.(J CM3)
C  INPUT : (R*8)  ERCEQ()  = EQUIL. CX/RECOM. EN. EXCESS FUNC.(J CM3)
C  INPUT : (R*8)  ELTEQ()  = EQUIL. LOW LINE  EN. EXCESS FUNC.(J CM3)
C  INPUT : (R*8)  ERAD()   = EQUIL. TOTAL RAD. EN. EXCESS FUNC.(J CM3)
C  INPUT : (I*4)  NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                            1ST DIM: LINE INDEX
C  INPUT : (I*4)  IZION(,) = CHARGE STATE OF COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  IMET(,)  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (C*1)  CIMET(,) = SIGN (+, BLANK OR -) OF METASTABLE
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  INDPH(,) = PEC FILE INDEX OF LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (C*1   CINDPH(,)= DRIVER (E OR BLANK => ELECTRONS)
C                                    (H          => HYDROGEN )
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  IFILE(,) = INDEX OF PEC FILE IN FILE LIST
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (R*8)  GCFPEQ(,,)= GCF PHOTON EXCESS FUNC. COMPONENT (CM3)
C                            1ST DIM: TEMPERATURE INDEX
C                            2ND DIM: LINE INDEX
C                            3RD DIM: LINE COMPONENT INDEX
C  INPUT : (L*4)  LPEC(,)  = .TRUE.  => PHOTON EMISSIVITY OBTAINED
C                            .FALSE. => PHOTON EMISSIVITY NOT OBTAINED
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (R*8)  GCF(,)   = LINE PHOTON EXCESS GCF FUNCTIONS (CM3)
C                            1ST IND: TEMPERATURE INDEX
C                            2ND IND: LINE INDEX
C  INPUT : (C*12) TITL(,)  = TITLE FOR LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  NRAT     = NUMBER OF RATIOS IDENTIFIED IN SCRIPT
C  INPUT : (I*4)  ILINE()  = INDEX OF NUMERATOR LINE FOR LINE RATIO
C  INPUT : (I*4)  JLINE()  = INDEX OF DENOMINATOR LINE FOR LINE RATIO
C  INPUT : (C*25) TITR()   = TILE FOR LINE RATIO
C  INPUT : (R*8)  RATA(,)  = LINE GCF RATIOS
C                            1ST IND: TEMPERATURE INDEX
C                            2ND IND: RATIO INDEX
C
C
C          (I*4)  I        = GENERAL USE - ARRAY ELEMENT INDEX
C          (I*4)  J        = GENERAL USE - ARRAY ELEMENT INDEX
C          (I*4)  IP       = GENERAL USE - ARRAY ELEMENT INDEX
C          (I*4)  IF       = GENERAL USE
C          (I*4)  IL       = GENERAL USE
C          (I*4)  IC       = GENERAL USE
C          (I*4)  IR       = GENERAL USE
C          (I*4)  ITL      = GENERAL USE
C          (I*4)  ITU      = GENERAL USE
C          (R*8)  EV       = PARAMETER = EV/KELVIN CONVERSION CONSTANT
C
C          (C*12) XFELEM   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CELEM    = EMITTING ION  ELEMENT NAME
C          (C*80) CADAS    = ADAS HEADER: INCLUDES RELEASE,PROGRAM,TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C
C AUTHOR  : H. P. Summers, JET
C           K1/1/57
C           JET EXT. 4941
C
C DATE:    27/04/44
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				     DATE:17/07/96
C MODIFIED: WILLIAM OSBORN
C	    - JOINED LINES IN SUBROUTINE DEFINITION SO AS TO AVOID >20
C	      CONTINUATION CHARACTERS
C
C VERSION: 1.3				     DATE:17/07/96
C MODIFIED: WILLIAM OSBORN
C	    - SPLIT 1023 FORMAT STATEMENT SO AS TO AVOID >20 CONT LINES
C
C VERSION: 1.4				     DATE:25/03/97
C MODIFIED: RICHARD MARTIN
C	    - CHANGED FILTR FROM C*6 TO C*7 TO ACCOMODATE LONGER FILTER 
C	      NAMES IN ACCORDANCE WITH ADAS405 AND ADAS408
C
C----------------------------------------------------------------------
      REAL*8     EV
C----------------------------------------------------------------------
      PARAMETER ( EV = 11605.4 )
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IMDIMD       , NDLINE      , NDCOMP
      INTEGER    NDRAT         , NDFILE       , NTDIM
      INTEGER    IZ0           , I            , J           , ITMAX
      INTEGER    IZL           , IZH          , NSTAGE      , NMSUM
      INTEGER    ITL           , ITU          , IP          , IF
      INTEGER    IL            , IC           , IR
      INTEGER    NFILE         , NLINE        , NRAT
C----------------------------------------------------------------------
      REAL*8     TIMEF
C----------------------------------------------------------------------
      INTEGER    NCOMP(NDLINE)        ,
     &           IZION(NDLINE,NDCOMP) , IMET(NDLINE,NDCOMP)  ,
     &           INDPH(NDLINE,NDCOMP) , IFILE(NDLINE,NDCOMP)
      INTEGER    ILINE(NDRAT)         , JLINE(NDRAT)
C----------------------------------------------------------------------
      LOGICAL    LPART
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , DATE*8    , CRESOL*12
      CHARACTER  XFELEM*12     ,
     &           CELEM*12      , CADAS*80  ,
     &           YEAR*2        , YEARDF*2  , FILTR*7   ,
     &           CT*1          , CS*1
      CHARACTER  CCLASS(8)*3
      CHARACTER  DASHES*10     , BLANKS*10 , CLINE*100
      CHARACTER  DSNINC*80
C----------------------------------------------------------------------
      CHARACTER  POPTIT(IMDIMD)*10
      CHARACTER  TITL(NDLINE,NDCOMP)*12 , TITR(NDRAT)*25
      CHARACTER  CIMET(NDLINE,NDCOMP)*1 , CINDPH(NDLINE,NDCOMP)*1
      CHARACTER  DSPECA(NDFILE)*120
C----------------------------------------------------------------------
      REAL*8     TEV(NTDIM)  , TEVH(NTDIM) , DENS(NTDIM) , DENSH(NTDIM)
      REAL*8     ERBEQ(NTDIM), ERCEQ(NTDIM), ELTEQ(NTDIM), ERAD(NTDIM)
      REAL*8     FPABUN(NTDIM,IMDIMD)      , FABUN0(IMDIMD)
      REAL*8     GCFPEQ(NTDIM,NDLINE,NDCOMP)
      REAL*8     GCF(NTDIM,NDLINE)
      REAL*8     RATA(NTDIM,NDRAT)
C----------------------------------------------------------------------
      LOGICAL    LSELA(8)    , LEXSA(8)    , LDEFA(8)
      LOGICAL    LPEC(NDLINE,NDCOMP)
C----------------------------------------------------------------------
      SAVE       CADAS
C----------------------------------------------------------------------
      DATA       CADAS /' '/
      DATA       (CCLASS(I),I=1,8)/'ACD','SCD','CCD','PRB',
     &                             'PRC','QCD','XCD','PLT'/
      DATA       DASHES/'----------'/ , BLANKS/'          '/
C----------------------------------------------------------------------
C
C**********************************************************************
C
      CELEM = XFELEM( IZ0 )
C
      IF ( LPART ) THEN
          CRESOL = 'PARTIAL     '
          CT     = 'P'
      ELSE
          CRESOL = 'STANDARD    '
          CT     = 'S'
      ENDIF
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND MASTER FILE SELECTION INFORMATION
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &                 ' TRANSIENT IONISATION AND EMISSION  ',
     &                 'ADAS406' , DATE
      WRITE(IWRITE,1002) TITLE
      WRITE(IWRITE,1004) CELEM , CRESOL
      WRITE(IWRITE,1005) YEAR  , YEARDF
C
      DO 5 I=1,8
       IF(LSELA(I).AND.LEXSA(I))THEN
           CS   = 'S'
       ELSEIF (LSELA(I).AND.LDEFA(I))THEN
           CS   = 'D'
       ELSE
           CS   = ' '
       ENDIF
       IF((I.EQ.4).OR.(I.EQ.5).OR.(I.EQ.8))THEN
           WRITE(IWRITE,1006)CCLASS(I),CT,CS,FILTR
       ELSE
           WRITE(IWRITE,1006)CCLASS(I),CT,CS
       ENDIF
    5 CONTINUE
C
      WRITE(IWRITE,1007)
C
      WRITE(IWRITE,1024) DSNINC(2:80)
C
      WRITE(IWRITE,1025)
C
      DO 7 IF = 1, NFILE
       WRITE(IWRITE,1026)IF,DSPECA(IF)
    7 CONTINUE
C
      WRITE(IWRITE,1010)IZ0,IZL-1,IZH,NSTAGE,NMSUM
C
C---------------------------------------------------------------------
C OUTPUT TEMPERATURES AND DENSITIES
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1008)
C
      DO 10 I=1,ITMAX
       WRITE(IWRITE,1009) I, TEV(I)*EV, TEV(I) , DENS(I), DENSH(I)
   10 CONTINUE
C
C---------------------------------------------------------------------
C OUTPUT INITIAL FRACTIONAL ABUNDANCES AT TIME T=0
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1028)
      WRITE(IWRITE,1029)
      DO 14 IP = 1,NMSUM
        WRITE(IWRITE,1030) IP, POPTIT(IP),FABUN0(IP)
   14 CONTINUE
C
C---------------------------------------------------------------------
C OUTPUT TRANSIENT FRACTIONAL ABUNDANCES AT TIME T=TIMEF
C---------------------------------------------------------------------
C
      ITU = 0
   15 ITL = ITU + 1
      IF(ITL.LE.ITMAX)THEN
          WRITE(IWRITE,1027)TIMEF
          ITU=ITU+10
          ITU=MIN0(ITU,ITMAX)
          WRITE(IWRITE,1011)
          WRITE(IWRITE,1012)'TE (eV)       ',(TEV(I) ,I=ITL,ITU)
          WRITE(IWRITE,1012)'NE (cm-3)     ',(DENS(I),I=ITL,ITU)
          WRITE(IWRITE,1012)'NH (cm-3)     ',(DENSH(I),I=ITL,ITU)
          WRITE(IWRITE,1013)(DASHES,I=ITL,ITU)
          WRITE(IWRITE,1014)
C
           DO 20 IP = 1,NMSUM
             DO 18 I=ITL,ITU
              J=I-ITL+1
              IF(FPABUN(I,IP).GT.1.0D-12)THEN
                  WRITE(CLINE(10*J-9:10*J),'(1P,D10.2)')FPABUN(I,IP)
              ELSE
                  WRITE(CLINE(10*J-9:10*J),'(1A10)')BLANKS
              ENDIF
   18        CONTINUE
             WRITE(IWRITE,1015) IP, POPTIT(IP),CLINE(1:10*(ITU-ITL+1))
   20      CONTINUE
          IF(LEXSA(4).OR.LDEFA(4))
     &        WRITE(IWRITE,1012)'ERB  (J CM3)  ',(ERBEQ(I),I=ITL,ITU)
          IF(LEXSA(5).OR.LDEFA(5))
     &        WRITE(IWRITE,1012)'ERC  (J CM3)  ',(ERCEQ(I),I=ITL,ITU)
          IF(LEXSA(8).OR.LDEFA(8))
     &        WRITE(IWRITE,1012)'ELT  (J CM3)  ',(ELTEQ(I),I=ITL,ITU)
          WRITE(IWRITE,1012)'ERAD (J CM3)  ',(ERAD(I),I=ITL,ITU)
C
C---------------------------------------------------------------------
C OUTPUT SPECTRAL LINE PHOTON EXCESS CONTRIBUTION FUNCTIONS
C---------------------------------------------------------------------
C
          WRITE(IWRITE,1016)
          WRITE(IWRITE,1012)'TE (eV)       ',(TEV(I) ,I=ITL,ITU)
          WRITE(IWRITE,1012)'NE (cm-3)     ',(DENS(I),I=ITL,ITU)
          WRITE(IWRITE,1012)'NH (cm-3)     ',(DENSH(I),I=ITL,ITU)
          WRITE(IWRITE,1013)(DASHES,I=ITL,ITU)
          WRITE(IWRITE,1017)
C
          DO 30 IL = 1 , NLINE
           WRITE(IWRITE,1018)IL,TITL(Il,1),(GCF(I,IL),I=ITL,ITU)
           DO 25 IC = 1,NCOMP(IL)
            WRITE(IWRITE,1019)IC,IZION(IL,IC),IMET(IL,IC),CIMET(IL,IC),
     &                        INDPH(IL,IC),CINDPH(IL,IC),IFILE(IL,IC),
     &                        (GCFPEQ(I,IL,IC),I=ITL,ITU)
   25      CONTINUE
   30     CONTINUE
C
C---------------------------------------------------------------------
C OUTPUT SPECTRAL LINE RATIOS
C---------------------------------------------------------------------
C
          WRITE(IWRITE,1020)
          WRITE(IWRITE,1021)
C
          DO 35 IR = 1 , NRAT
           WRITE(IWRITE,1022)IR,ILINE(IR),JLINE(IR),
     &                        (RATA(I,IR),I=ITL,ITU)
   35     CONTINUE
C
          GO TO 15
      ENDIF
C
      WRITE(IWRITE,1023)
      WRITE(IWRITE,1031)
C
      RETURN
C-----------------------------------------------------------------------
C
 1000 FORMAT(///,A79)
 1001 FORMAT(//,20('*'),' TABULAR OUTPUT FROM ',A36,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,20('*')/)
 1002 FORMAT(//,19('-'),1X,A40,1X,19('-')/)
 1004 FORMAT(//,'ELEMENT NAME  :', 2X,A12,/
     &       1H ,'RESOLUTION    :', 2X,A12,/ )
 1005 FORMAT(//,'MASTER FILE SELECTION:',/
     &       1H ,'----------------------',/
     &       1H ,'SELECTED YEAR :',2X,A,/
     &       1H ,'DEFAULT YEAR  :',2X,A,//
     &       1H ,'CLASS      TYPE      SELECT      FILTER',/
     &       1H ,'-----      ----      ------      ------')
 1006 FORMAT(1H ,1X,A3,10X,A1,10X,A1,10X,1A7)
 1007 FORMAT(//,'KEY:',/,
     &       1H ,'----',/,
     &       1H ,'SELECT: S=SELECTED, D=DEFAULT, blank= not available',/
     &       1H ,'TYPE  : P=PARTIAL , S=STANDARD',/,
     &       1H ,'FILTER: FT=FILTER THICKNESS SPECIFICATION, EV=',
     &           'ENERGY CUT-OFF, blank=no filter')
C
 1008 FORMAT(///,/,
     &       1H ,'OUTPUT PLASMA TEMPERATURE AND DENSITY SETS:',/,
     &       1H ,'-------------------------------------------',/,
     &       1H ,'INDEX',6X,'ELECTRON TEMPERATURE',10X,
     &           'ELECTRON',12X,'HYDROGEN',/,
     &       1H ,12X,'(kelvin)',5x,'(eV)',12x,'DENSITY (cm-3)',
     &           6x,'DENSITY (cm-3)',/,
     &       1H ,79('-'))
 1009 FORMAT(1H ,I3,6X,1PD10.2,2X,1PD10.2,9X,1PD10.2,10X,1PD10.2)
C
 1010 FORMAT (//,/,
     &        1H ,'IONISATION STAGE/METASTABLE SUMMARY:',/,
     &        1H ,'------------------------------------',/,
     &        1H ,'NUCLEAR CHARGE              =',I3,/,
     &        1H ,'LOWEST CHARGE STATE         =',I3,/,
     &        1H ,'HIGHEST CHARGE STATE        =',I3,/,
     &        1H ,'NUMBER OF STAGES            =',I3,/,
     &        1H ,'NUMBER OF METASTABLE STATES =',I3)
 1011 FORMAT (//)
 1012 FORMAT (1H ,1A14,6X,1P,10D10.2)
 1013 FORMAT (1H ,20X,10A10)
 1014 FORMAT (1H ,'IND  ION   MET',/,
     &        1H ,'--------------')
 1015 FORMAT (1H ,I2,2X,1A10,6X,A)
 1016 FORMAT (//,/,
     &        1H ,'SPECTRAL LINE PHOTON EXCESS GCF FUNCTIONS (CM3):',/,
     &        1H ,'------------------------------------------------')
 1017 FORMAT (1H ,'IND  ION   WVLEN.(A)',/,
     &        1H ,'  (IC,IZ,IM ,IP ,IF)',/,
     &        1H ,'-------------------- ')
 1018 FORMAT (1H ,I2,2X,A12,'A',3X,1P,10D10.2)
 1019 FORMAT (1H ,2X,'(',I2,1X,I2,1X,I2,A1,I3,A1,1X,I2,')',
     &            1P,10D10.2)
 1020 FORMAT (//,/,
     &        1H ,'SPECTRAL LINE RATIOS:',/,
     &        1H ,'---------------------')
 1021 FORMAT (1H ,'IR   JL   KL',/,
     &        1H ,'------------')
 1022 FORMAT (1H ,I2,3X,I2,3X,I2,8X,1P,10D10.2)
 1023 FORMAT (//,/,
     &        1H ,'TABLE KEY:',/,
     &        1H ,'----------',/,
     &        1H ,'TE   = ELECTRON TEMPERATURE                      ',
     &            'NE   = ELECTRON DENSITY                          ',/,
     &        1H ,'NE   = HYDROGEN DENSITY                          ',
     &            'IND  = STAGE/METASTABLE COUNT                    ',/,
     &        1H ,'ION  = ION SPECIFICATION                         ',
     &            'MET  = METASTABLE INDEX                          ',/,
     &        1H ,'PRB  = RECOMB.+ BREMS. POWER FUNCTION            ',
     &            'PRC  = CHARGE EXCHANGE RECOMB. POWER FUNCTION    ',/)
 1024 FORMAT (//,/,
     &        1H ,'SCRIPT FILE:',14X,A)
 1025 FORMAT (//,'PHOTON EMISSIVITY COEFFICIENT FILES:',/,
     &        1H ,'------------------------------------',/,
     &        1H ,'             IF           FILE      ',/,
     &        1H ,'             --           ----      ')
 1026 FORMAT (1H ,13X,I2,11X,A)
 1027 FORMAT (///,/,
     &        1H ,'FRACTIONAL ABUNDANCES, ENERGY EXCESS, GCF ',
     &            'FUNCTIONS AND LINE RATIOS AT T = ',
     &             1P,D10.2,' SEC:',/,
     &        1H ,'------------------------------------------',
     &            '---------------------------------',
     &            '---------------'/)
 1028 FORMAT (//,/,
     &        1H ,'INITIAL  FRACTIONAL ABUNDANCES AT T = 0:',/,
     &        1H ,'----------------------------------------',/)
 1029 FORMAT (1H ,'IND  ION   MET    FRACT. ABUND.'/,
     &        1H ,'-------------------------------')
 1030 FORMAT (1H ,I2,2X,1A10,6X,1P,D10.2)
 1031 FORMAT (1H ,'PLT  = LINE RADIATED POWER FUNCTION              ',
     &            'PRAD = TOTAL RADIATED POWER FUNCTION             ',/,
     &        1H ,'IL   = SPECTRUM LINE INDEX                       ',
     &            'IC   = SPECTRUM LINE COMPONENT COUNT             ',/,
     &        1H ,'IZ   = ASSOCIATED ION FOR LINE COMPONENT         ',
     &            'IM   = ASSOCIATED METASTABLE FOR LINE COMPONENT  ',/,
     &        1H ,'IP   = PHOTON EMISSIVITY FILE SELECTION INDEX    ',
     &            'IF   = EMISSIVITY FILE INDEX                     ',/,
     &        1H ,'IR   = SPECTRUM LINE RATIO INDEX                 ',
     &            'JL   = NUMERATOR SPECTRUM LINE INDEX             ',/,
     &        1H ,'KL   = DENOMINATOR SPECTRUM LINE INDEX           ')
C
C----------------------------------------------------------------------
C
      END
