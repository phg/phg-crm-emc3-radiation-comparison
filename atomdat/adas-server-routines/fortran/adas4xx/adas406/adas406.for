       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS406 **********************
C
C  VERSION:  1.0
C
C  PURPOSE:   TO EVALUATE TRANSIENT METASTABLE RESOLVED IONISATION
C             STATES AND RADIATED ENERGY EXCESSES FOR POPULATION
C             EVOLUTION IN CONSTANT TEMP./DENS. CONDITIONS AND
C             COMPUTE PHOTON EXCESS LINE CONTRIBUTION FUNCTIONS
C
C             DATA MAY BE READ FROM STANDARD OR PARTIAL ISONUCLEAR
C             MASTER FILES
C             E.G.  JETSHP.ACD89#C.DATA OR JETSHP.ACD93#BE.RDATA
C
C             CHARGE EXCHANGE IS INCORPORATED IF AVAILABLE
C
C             EIGHT CLASSES OF DATA MAY BE OPENED FOR INPUT WITH THE ACD
C             AND SCD CLASSES OBLIGATORY.
C
C               ICLASS    TYPE      INDI     INDJ
C               ------    ----      ----     ----
C                 1       ACD       IPRT     IGRD
C                 2       SCD       IPRT     IGRD
C                 3       CCD       IPRT     IGRD
C                 4       PRB       IPRT     IGRD
C                 5       PRC       IPRT     IGRD
C                 6       QCD       IGRD     JGRD
C                 7       XCD       IPRT     JPRT
C                 8       PLT       IGRD
C
C
C             RADIATED POWER GRAPHS ARE ONLY DISPLAYED IF APPROPRIATE
C             MASTER FILE DATA CLASSES ARE SELECTED.  CONTRIBUTION
C             FUNCTIONS ARE EVALUATED FOR LINES SPECIFIED IN A SCRIPT
C             FILE.
C             E.G.  '<UID>.ADAS405.DATA(SCR#<EL>)'
C
C            WHERE, <UID> = THE USER IDENTIFIER
C                   <EL>  = THE ELEMENT SYMBOL
C
C
C  PROGRAM:
C
C          (I*4) IZDIMD  = PARAMETER = MAX. NUMBER OF CHARGE STATES IN
C                                      ISONUCLEAR MASTER FILES
C          (I*4) IMDIMD  = PARAMETER = MAX. NUMBER OF POPULATIONS
C          (I*4) IPDIMD  = PARAMETER = MAX. NUMBER OF METASTABLES FOR
C                                      EACH IONISATION STAGE
C          (I*4) ITDIMD  = PARAMETER = MAX. NUMBER OF TEMP OR DENS
C                                      VALUES IN ISOELECTRONIC
C                                      MASTER FILES
C          (I*4) ISDIMD  = PARAMETER = MAX. NUMBER OF (CHARGE, PARENT,
C                                      GROUND) BLOCKS IN ISONUCLEAR
C                                      MASTER FILES
C          (I*4) IUNT07  = PARAMETER = PRINCIPAL TEXT OUTPUT STREAM
C          (I*4) IUNT11  = PARAMETER = UNIT FOR DATA INPUT/OUTPUT
C          (I*4) IUNT13  = PARAMETER = UNIT FOR INPUT MASTER FILE CHECK
C          (I*4) L1      = PARAMETER = INTEGER = 1
C          (I*4) L2      = PARAMETER = INTEGER = 2
C          (I*4) NTDIM   = PARAMETER = MAX. NUMBER OF SELECTED TEMP/
C                                      DENS PAIRS
C          (I*4) NDLINE  = PARAMETER = MAX. NUMBER OF LINES ALLOWED
C          (I*4) NDCOMP  = PARAMETER = MAX. NUMBER OF LINE COMPONENTS
C                                      FOR EACH LINE
C          (I*4) NDRAT   = PARAMETER = MAX. NUMBER OF LINE RATIOS
C                                      ALLOWED
C          (I*4) NDFILE  = PARAMETER = MAX. NUMBER OF EMISSIVITY FILES
C                                      WHICH CAN BE SEARCHED
C          (I*4) NGFPLN  = PARAMETER = NUMBER OF GRAPH SETS USED BY
C                                      POPULATION/POWER PLOT ROUTINES
C          (I*4) NDONE   = PARAMETER = ARRAY DIMENSION = 1
C
C
C          (I*4) I          = GENERAL INDEX
C          (I*4) IG         = GENERAL INDEX
C          (I*4) IGRD       = GENERAL INDEX FOR RECOMBINED METASTABLES
C          (I*4) IRCODE     = RETURN CODE
C          (I*4) IT         = GENERAL TEMPERATURE INDEX
C          (I*4) ITMAX      = NUMBER OF INPUT TEMP/DENSITY SETS (1->20)
C          (I*4) ITMAXS     = STORED PREVIOUS IMAX
C          (I*4) ITTYP      = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C          (I*4) IZ         = GENERAL CHARGE STATE INDEX
C          (I*4) IZ0        = ELEMENT NUCLEAR CHARGE
C          (I*4) IZ1        = GENERAL RECOMBING ION CHARGE
C          (I*4) IZH        = HIGHEST RECOMBINING ION CHARGE IN MASTER
C                             FILE
C          (I*4) IZL        = LOWEST RECOMBINING ION CHARGE IN MASTER
C                             FILE
C          (I*4) IFILE(,)  = INDEX OF PEC FILE IN FILE LIST
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C          (I*4) ILINE()   = INDEX OF NUMERATOR LINE FOR LINE RATIO
C          (I*4) IMET(,)   = NUMBER OF COMPONENTS OF SCRIPT LINE
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C          (I*4) IMIP()     = META. INDEX WITHIN A STAGE FOR POPULATION
C                             1ST IND: POPULATION INDEX
C          (I*4) INDPH(,)  = PEC FILE INDEX OF LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C          (I*4) IPLINE(,)  = METASTABLE POINTER OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C          (I*4) IPIZM(,)   = POINTER TO POPULATION INDEX
C                             1ST IND: IONISATION STAGE INDEX
C                             2ND IND: METASTABLE INDEX WITH STAGE
C          (I*4) IZION(,)  = CHARGE STATE OF COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C          (I*4) IZIP()     = RECOMBINING ION CHARGE  FOR POPULATION
C                             1ST IND: POPULATION INDEX
C          (I*4) JLINE()    = INDEX OF DENOMINATOR LINE FOR LINE RATIO
C          (I*4) NFILE      = NUMBER OF PEC FILES TO BE SCANNED
C          (I*4) NLINE      = NUMBER OF LINES IDENTIFIED IN SCRIPT
C          (I*4) NMSUM      = TOTAL NUMBNER OF POPULATIONS INCLUDING
C          (I*4) NSTAGE     = NUMBER OF STAGES INCL THE EXTRA STAGE =
C                             IZU-IZL+2
C          (I*4) NRAT       = NUMBER OF RATIOS IDENTIFIED IN SCRIPT
C                             THE EXTRA (BARE NUCLEUS) STAGE
C          (I*4) NCOMP()    = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C          (I*4) NPRT()     = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZL-1 TO
C                             IZU ON INPUT
C          (I*4) NPRTR()    = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZL TO
C                             IZU FOUND IN MASTER FILE
CA         (I*4)  ISTOP   = FLAG USED BY IDL TO SIGNAL AN IMMEDIATE END
CA                          TO THE ROUTINE FROM ANY POINT RATHER THAN
CA                          A RETURN TO THE PREVIOUS SCREEN. IF 1 THEN
CA                          PROGRAM ENDS, IF 0 THEN IT CONTINUES.
CA         (I*4)  IRCODE  = RETURN CODE WHEN SCRIPT FILE IS OPENED
CA	    		    0 = SCRIPT FILE READ OKAY
CA			    1 = SCRIPT FILE DOES NOT EXIST
CA			    2 = I/O ERROR READING SCRIPT FILE
CA                          3 = FILENAME ERROR WITHIN SCRIPT FILE
C
C          (R*8)  EV      = PARAMETER = EV TO K CONVER. FACTOR =11605.4
C
C          (R*8)  AMSR      = RECEIVER ATOMIC MASS NUMBER
C          (R*8)  AMSD      = RECEIVER ATOMIC MASS NUMBER
C
C          (R*8)  ACDA(,,,) = INTERPOLATION OF ACD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C          (R*8)  SCDA(,,,) = INTERPOLATION OF SCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C          (R*8)  CCDA(,,,) = INTERPOLATION OF CCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C          (R*8)  PRBA(,,)  = INTERPOLATION OF PRB COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C          (R*8)  PRCA(,,)  = INTERPOLATION OF PRC COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C           R*8)  QCDA(,,,) = INTERPOLATION OF QCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: FIRST METASTABLE INDEX
C                             4TH DIM: SECOND METASTABLE INDEX
C          (R*8)  XCDA(,,,) = INTERPOLATION OF XCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: FIRST PARENT METASTABLE INDEX
C                             4TH DIM: SECOND PARENT METASTABLE INDEX
C          (R*8)  PLTA(,,)  = INTERPOLATION OF PLT COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: METASTABLE INDEX
C          (R*8)  ACDSEQ(,) = STANDARD (UNRESOLVED) ACD COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8)  SCDSEQ(,) = STANDARD (UNRESOLVED) SCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8)  CCDSEQ(,) = STANDARD (UNRESOLVED) CCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8)  ERBSEQ(,) = STANDARD (UNRESOLVED) PRB ENER. EXCESS (J)
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8)  ERCSEQ(,) = STANDARD (UNRESOLVED) PRC ENER. EXCESS (J)
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8)  ELTSEQ(,) = STANDARD (UNRESOLVED) PLT ENER. EXCESS (J)
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8)  ERBEQ()   = TOTAL RADIATED RECOM-BREMS
C                                   ENERGY EXCESS FUNCTION (J) TO TIMEF
C          (R*8)  ERCEQ()   = TOTAL CX RADIATED RECOM ENERGY EXCESS
C                                   FUNCTION (J) TO TIMEF NORMALISED
C                                   TO ELECTRON DENSITY
C          (R*8)  ELTEQ()   = TOTAL RADIATED LINE POWER
C                                   ENERGY EXCESS FUNCTION (J) TO TIMEF
C          (R*8)  ERADA()   = TOTAL RADIATED ENERGY EXCESS FUNCTION (J)
C                                   TO TIMEF
C          (R*8) DTEV()     = DLOG10(ELECTRON TEMPERATURES (EV))
C          (R*8) DTEVH()    = DLOG10(NEUTRAL HYDROGEN TEMPERATURES (EV))
C          (R*8) DDENS()    = DLOG10(ELECTRON DENSITIES (CM-3))
C          (R*8) DDENSH()   = DLOG10(NEUTREAL HYDROGEN DENSITIES(CM-3))
C          (R*8) DTEVD()    = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C                             IN SELECTED MASTER FILE
C          (R*8) DDENSD()   = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C                             IN SELECTED MASTER FILE
C          (R*8) TEIN()     = USER ENTERED ISPF VALUES -
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DEIN()     = USER ENTERED ISPF VALUES -
C                             ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DENS()     = USER ENTERED ISPF VALUES IN STANDARD UNITS
C                             ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) THIN()     = USER ENTERED ISPF VALUES -
C                             HYDROGEN TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DHIN()     = USER ENTERED ISPF VALUES -
C                             HYDROGEN DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DENSH()    = USER ENTERED ISPF VALUES IN STANDARD UNITS
C                             HYDROGEN DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DENSS()    = STORED PREVIOUS USER VALUES (STAND. UNITS)
C                             ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DENSHS()   = STORED PREVIOUS USER VALUES (STAND. UNITS)
C                             HYDROGEN DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C          (R*8) DRCOFD(,,) = DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C                             IN SELECTED MASTER FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C          (R*8) DRCOFI()   = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C          (R*8) DRLT()     = LOW TEMPERATURE DIELECTRONIC DATA
C                             1ST DIM: TEMPERATURE INDEX
C          (R*8) FABUN0()   = FRACT. ABUNDANCES OF METASTABLES AT T=0
C          (R*8) FABUNS()   = STORED PREVIOUS
C                             FRACT. ABUNDANCES OF METASTABLES AT T=0
C          (R*8) FPABUN(,)  = RESOLVED METASTABLE FRACTIONAL
C                             ABUNDANCES AT T=TIMEF
C                             1ST DIM: TEMPERATURE/DENSITY SET
C                             2ND DIM: METASTABLE INDEX
C          (R*8) FPINTG(,)  = RESOLVED METASTABLE FRACTIONAL
C                             ABUNDANCES INTEGRALS TO TIME T=TIMEF
C                             1ST DIM: TEMPERATURE/DENSITY SET
C                             2ND DIM: METASTABEL INDEX
C          (R*8) FSABUN(,)  = STAGE EQUILIBRIUM FRACTIONAL
C                             ABUNDANCES
C                             1ST DIM: TEMPERATURE/DENSITY SET
C                             2ND DIM: CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8) FSINTG(,)  = STAGE TRANSIENT FRACTIONAL
C                             ABUNDANCE EXCESSES
C                             1ST DIM: TEMPERATURE/DENSITY SET
C                             2ND DIM: CHARGE STATE INDEX (IZ1-IZL+1)
C          (R*8) ELTPEQ(,)  = METASTABLE PARTIAL RADIATED
C                             LINE ENERGY EXCESS FUNCTIONS (J) TO TIMEF
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR
C                             2ND DIM: - METASTABLE INDEX
C          (R*8) GCFPEQ(,,) = GCF PHOTON EXCESS FUNC. COMPONENT (CM3)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C                             3ND DIM: LINE COMPONENT INDEX
C          (R*8) GCFEQ(,)   = GCF PHOTON EXCESS FUNCTION  (CM3)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C          (R*8) RATA(,)    = LINE GCF PHOTON EXCESS RATIOS
C                             1ST IND: TEMPERATURE INDEX
C                             2ND IND: RATIO INDEX
C
C          (R*8) PECA(,,)   = PHOTON EMISSIVITY COEFFICIENTS (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C          (R*8) TEV()      = ELECTRON TEMPERATURES (EV)
C          (R*8) TEVH()     = NEUTRAL HYDROGEN TEMPERATURES (EV)
C          (R*8) TEVS()     = STORED PREVIOUS ELECTRON TEMPERATURES (EV)
C          (R*8) TEVHS()    = STORED PREVIOUS NEUTRAL HYDROGEN
C                             TEMPERATURES (EV)
C          (R*8) XLGIN()    = LOWER LIMIT FOR X-AXIS OF GRAPHS:USER UNTS
C                             DIMENSION: NGFPLN
C          (R*8) XUGIN()    = UPPER LIMIT FOR X-AXIS OF GRAPHS:USER UNIT
C                             DIMENSION: NGFPLN
C          (R*8) XLG()      = LOWER LIMIT FOR X-AXIS OF GRAPHS:STAN.UNT.
C                             DIMENSION: NGFPLN
C          (R*8) XUG()      = UPPER LIMIT FOR X-AXIS OF GRAPHS.STAN.UNT.
C                             DIMENSION: NGFPLN
C          (R*8) YLG()      = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8) YUG()      = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
C          (R*8) XL3        = LOWER LIMIT FOR X-AXIS OF GRAPH:STAN.UNITS
C          (R*8) XU3        = UPPER LIMIT FOR X-AXIS OF GRAPH:STAN.UNITS
C          (R*8) YL3        = LOWER LIMIT FOR Y-AXIS OF GRAPH
C          (R*8) YU3        = UPPER LIMIT FOR Y-AXIS OF GRAPH
C          (R*8) YTEMP(,)   = TEMPORARY MATRIX FOR DURING SUBROUTINE
C                             CALCULATIONS
C                             DIMENSIONS = (IPDIMD,IPDIMD)
C          (R*8) YTEM()     = TEMPORARY ARRAY FOR HOLDING VALUES OF
C                             DIFFERENCE BETWEEN RECOMBINATION AND
C                             IONISATION GROUND LEVEL COEFFICIENTS
C                             DIMENSIONS = (NSTAGE)
C          (R*8) ZDATA()    = CHARGE + 1 FOR IONS IN SELECTED MASTER
C                             FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C          (R*8) ZL         = LOWEST RECOMBINING ION CHARGE =IZL
C          (R*8) ZU         = HIGHEST RECOMBINING ION CHARGE =IZL
C
C
C          (C*12) CHELEM    = FULL ELEMENT NAME
C          (C*2)  CHG       = CHARACTER STRING FOR IGRD INDEX
C          (C*2)  CHZ       = CHARACTER STRING FOR ION CHARGE
C          (C*14) CHPOP()   = STRING OF POPULATION INDEX AND NAME
C          (C*2)  CIION()   = SCRIPT FILE: EMITTING ION FOR LINE
C                             DIMENSION: LINE INDEX
C          (C*1)  CIMET(,)  = SIGN (+, BLANK OR -) OF METASTABLE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C          (C*1)  CINDPH(,) = DRIVER (E OR BLANK => ELECTRONS)
C                                    (H          => HYDROGEN )
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C          (C*80) CLINE     = GENERAL PURPOSE CHARACTER STRING VARIABLE
C          (C*2)  CMPTS()   = SCRIPT FILE: NO. OF COMPONENTS FOR LINE
C                             DIMENSION: LINE INDEX
C          (C*12) CTITLE()  = SCRIPT FILE: INFORMATION ON LINE
C                             DIMENSION: LINE INDEX
C          (C*6)  DATE       = CURRENT DATE                             D
C          (C*80) DSNINC    = SCRIPT DATA SET NAME
C          (C*120)DSFLLA()  = MASTER FILE DATA SET NAMES
C          (C*80) DSN80    = FULL MVS NAME FOR GCF PASSING FILE
C          (C*80) DSPECA() = PHOTON EMISSIVITY SOURCE FILES
C          (C*2)  ELEMT     = ELEMENT CHEMICAL SYMBOL
C          (C*3)  EXT()     = EXTENSION OF PEC FILE MEMBER NAME
C          (C*7)  FILTR     = POWER FILTER CHARACTERS FOR FILE NAMES
C          (C*8)  GROUP()   = GROUP IDENTIFIER OF PEC FILE
C          (C*40) GTIT1     = TITLE FOR FIRST AND SECOND GRAPH SETS
C          (C*40) GTIT3     = TITLE FOR FIRST AND SECOND GRAPH SETS
C          (C*4)  ION()     = ION NAME OF PEC FILE MEMBER NAME
C          (C*8)  MEMB()    = MEMBER NAME OF PEC FILE
C          (C*8)  POPTIT()  = NAME FOR POPULATION                       ON
C                             1ST. DIM: POPULATION INDEX
C          (C*6)  PROJ()    = USER IDENTIFIER OF PEC FILE
C                            1ST. DIM: PEC FILE INDEX NUMBER
C          (C*3)  REP        = 'YES' => TERMINATE PROGRAM EXECUTION.
C                            = 'NO ' => CONTINUE PROGRAM EXECUTION.
C          (C*16) SPECL(,)  = SPEC. OF POINTERS OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C          (C*40) TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C          (C*12) TITL(,)   = TITLE FOR LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C          (C*25) TITR()    = TILE FOR LINE RATIO
C          (C*5)  TYPE()    = TYPE IDENTIFIER OF PEC FILE
C          (C*80)  UID()    = USER IDENTIFIER OF PEC FILE
C          (C*2)  YEAR      = TWO DIGIT SELECTED YEAR NUMBER
C          (C*2)  YEARDF    = TWO DIGIT DEFAULT YEAR NUMBER
C          (C*2)  YEARDF    = TWO DIGIT DEFAULT YEAR NUMBER
C
C
C
C          (L*4)  LACDA(,,) = .TRUE.  => ACD COEFFICIENT AVAILABLE
C                             .FALSE. => ACD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C          (L*4)  LSCDA(,,) = .TRUE.  => SCD COEFFICIENT AVAILABLE
C                             .FALSE. => SCD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C          (L*4)  LCCDA(,,) = .TRUE.  => CCD COEFFICIENT AVAILABLE
C                             .FALSE. => CCD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C          (L*4)  LPRBA(,)  = .TRUE.  => PRB COEFFICIENT AVAILABLE
C                             .FALSE. => PRB COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C          (L*4)  LPRCA(,)  = .TRUE.  => PRC COEFFICIENT AVAILABLE
C                             .FALSE. => PRC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C          (L*4)  LQCDA(,,) = .TRUE.  => QCD COEFFICIENT AVAILABLE
C                             .FALSE. => QDC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: FIRST METASTABLE INDEX
C                             3RD DIM: SECOND METASTABLE INDEX
C           L*4)  LXCDA(,,) = .TRUE.  => XCD COEFFICIENT AVAILABLE
C                             .FALSE. => XDC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: FIRST PARENT METASTABLE INDEX
C                             3RD DIM: SECOND PARENT METASTABLE INDEX
C          (L*4)  LPLTA(,)  = .TRUE.  => PLT COEFFICIENT AVAILABLE
C                             .FALSE. => PLT COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM:  METASTABLE INDEX
C          (L*4)  LRSCRP    = .TRUE.  => SCRIPT FILE READ
C                             .FALSE. => SCRIPT FILE NOT READ
C          (L*4)  LFILE()   = .TRUE.  => PEC FILE EXISTS AND MATCHES
C                             .FALSE. => PEC FILE DOES NOT EXIST/MATCH
C          (L*4)  LSELA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        INDEX SELECTED
C                           = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                        NOT SELECTED
C          (L*4)  LEXSA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        SELECTED INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS SELECTED INDEX
C          (L*4)  LDEFA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        DEFAULT YEAR INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS DEFAULT YEAR INDEX
C          (L*4)  LPART     = .TRUE.  => PARTIAL DATA SELECTED
C                           = .FALSE. => STANDARD DATA SELECTED
C          (L*4)  LSNULL    = .TRUE.  => SCRIPT FILE SET TO NULL
C                             .FALSE. => SCRIPT FILE VALID
C          (L*4)  LOSEL     = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C          (L*4)  LPLT1     = .TRUE.  => DISPLAY GRAPH
C                           = .FALSE. => DO NOT DISPLAY GRAPH
C          (L*4)  LGRD1     = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF1     = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LGRD3     = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C          (L*4)  LDEF3     = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
C          (L*4)  LDOUT     = .TRUE.  => ?
C                             .FALSE. => ?
C          (L*4)  LGHOST    = .TRUE.  => GHOST GRAPHICS OUTPUT PRESENT
C                             .FALSE. => NO GHOST GRAPHIC OUTPUT
C                                        PRESENT      (VESTIGIAL)
C          (L*4)  LPASS     = .TRUE.  => OUTPUT DATA TO GCF PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        GCF PASSING FILE.
C          (L*4)  LTRNG()   =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER
C                                      -POLATED FOR THE USER ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                            .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA
C                                      -POLATED FOR THE USER  ENTERED
C                                      ELECTRON TEMPERATURE 'TVAL()'.
C                             DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)  LDRNG()   =.TRUE.  => OUTPUT 'PECA()'  VALUE WAS INTER
C                                      -POLATED FOR THE USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            .FALSE. => OUTPUT 'PECA()'  VALUE WAS EXTRA
C                                      -POLATED FOR THE USER  ENTERED
C                                      ELECTRON DENSITY 'DVAL()'.
C                            DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C          (L*4)  LEXSS()   = .TRUE.  => OUTPUT STANDARD MASTER DATA
C                                        FOR THIS INDEX GENERATED
C                           = .FALSE. => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX NOT GENERATED
C          (L*4)  LPEC(,)   = .TRUE.  => PHOTON EMISSIVITY OBTAINED
C                             .FALSE. => PHOTON EMISSIVITY NOT OBTAINED
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C          (L*4)  LRSPEC    = .TRUE.  => PEC PROCESSING DONE
C                             .FALSE. => PEC PROCESSING NOT DONE
C          (L*4)  LTSET     = .TRUE.  => A CALCULATION HAS TAKEN PLACE
C                                        FOR THE CURRENT TEMPERATURES
C                           = .FALSE. => A CALCULATION HAS NOT TAKEN
C          (L*4)  OPEN11    = .TRUE.  => GCF PASSING FILE OPENED
C                             .FALSE. => GCF PASSING FILE NOT OPENED
C          (L*4)  OPEN13    = .TRUE.  => STREAM 13  OPENED
C                             .FALSE. => STREAM 13 NOT OPENED
C
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D6DATA     ADAS      FETCHES DATA FROM MASTER FILES
C          D6ISPF     ADAS      FETCHES USER DATA FROM ISPF PANELS
C          D6MPOP     ADAS      GENERAL MATRIX DIAGONALISATION ROUTINE
C          D6OTG1     ADAS      GRAPHICAL OUTPUT OF POPULATIONS AT TIMEF
C          D6OTG2     ADAS      GRAPHICAL OUTPUT OF RADIATED ENERGY
C                               EXCESSES
C          D6OTG3     ADAS      GRAPHICAL OUTPUT OF PHOTON EXCESS
C                               CONTRIB. FUNCTIONS
C          D6OUT0     ADAS      TEXT OUTPUT TO PAPER.TEXT FILE
C          D6SCRP     ADAS      FETCHES SPECUM LINE INFO. FROM SCRIPT
C          D6SETL     ADAS      PUTS LINE INFO TO ISPF POOL FOR DISPLAY
C          D6SETM     ADAS      METASTABLE INFO TO ISPF POOL FOR DISPLAY
C          D6SGCF     ADAS      COMPUTES CONTRIBUTION FUNCTIONS
C          D6SPEC     ADAS      OBTAINS LINE EMISSIVITIES FROM PEC FILES
C          D6SPF0     ADAS      FETCHES INPUT FILE NAMES FROM ISPF PANEL
C          D6SPF1     ADAS      OBTAINS INPUT FILE NAMES FROM ISPF PANEL
C          D6SPOW     ADAS      COMPUTES RADIATED POWER FUNCTIONS
C          D6WR11     ADAS      OUTPUT GCF PASSING FILE
C          I4FCTN     ADAS      CONVERT STRING TO INTEGER
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XFELEM     ADAS      FETCHES ELEMET NAME
C          XFESYM     ADAS      FETCHES ELEMENT CHEMICAL SYMBOL
C          XXUID      ADAS      SETS A USER IDENTIFIER
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          XXTCON     ADAS      CONVERTS ISPF ENTERED TEMPS. TO KELVIN
C          XXDCON     ADAS      CONVERTS ISPF ENTERED DENS. TO CM-3
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    21/04/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:26/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C VERSION: 1.3				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - ADDED FORGOTTEN XX0000 PROCEDURE
C VERSION: 1.4				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - MINOR CORRECTION TO ERROR INFORMATION
C VERSION: 1.5				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - INCREASED IZDIMD TO 50 AND ITDIMD TO 51 IN LINE WITH JET
C VERSION: 1.6				     DATE:25/03/97
C MODIFIED: RICHARD MARTIN
C	    - CHANGER FILTR FROM C*6 TO C*7 TO ACCOMODATE LONGER FILTER NAME
C	      IN ACCORDANCE WITH ADAS405 AND ADAS408
C VERSION: 1.7				     DATE:05/03/98
C MODIFIED: RICHARD MARTIN
C	    - INCREASED IZDIDM TO 54
C
C VERSION: 1.8				     DATE:09/03/98
C MODIFIED: RICHARD MARTIN
C	    - INCREASED UID FROM CHAR*6 TO CHAR*80 IN ACCORDANCE WITH XXUID.
C
C VERSION: 1.9					DATE: 08/06/98
C MODIFIED: RICHARD MARTIN
C 	    - INCREASED NDTIM TO 30.
C		
C VERSION: 1.10				DATE: 22/01/2002
C MODIFIED: Martin O'Mullane
C		- Increased IZDIMD to 83 to accommodate lead.
C		- Trap case of taking log of TEVH and DENSH when
C                 they are zero.
C		
C VERSION: 1.11				DATE: 13/05/2004
C MODIFIED: Martin O'Mullane
C		- Increased ISDIMD to 83 to accommodate lead.
C
C VERSION: 1.12				DATE: 03/06/2009
C MODIFIED: Allan Whiteford
C		- Initiliase LRSPEC to FALSE
C
C VERSION: 1.13				DATE: 13/10/2009
C MODIFIED: Martin O'Mullane
C		- Use d5data to get data from adf11 files.
C
C-----------------------------------------------------------------------
       INTEGER   IUNT07  , IUNT11 , IUNT13
       INTEGER   L1      , L2
       INTEGER   NTDIM   , ITDIMD , IZDIMD , IMDIMD ,
     &           NDLINE  , NDCOMP , NDRAT  , NDFILE ,
     &           IPDIMD  , ISDIMD , NGFPLN ,
     &           NDONE   , ISTOP  , PIPEIN , PIPEOU , IGRAPH
C-----------------------------------------------------------------------
       REAL*8    EV
C-----------------------------------------------------------------------
       PARAMETER ( IUNT07 = 7 , IUNT11 = 11  , IUNT13 = 13 )
       PARAMETER ( L1 = 1   , L2 = 2  )
       PARAMETER ( NTDIM  = 30 , ITDIMD = 51 , IZDIMD = 83 )
       PARAMETER ( NDLINE = 10 , NDCOMP = 10 , NDRAT = 10 , NDFILE = 6 )
       PARAMETER ( IPDIMD = 5, ISDIMD = 83 ,IMDIMD = ISDIMD+1 )
       PARAMETER ( NGFPLN = 2 )
       PARAMETER ( EV = 11605.4 )
       PARAMETER ( NDONE = 1 , PIPEIN=5 , PIPEOU=6)
C-----------------------------------------------------------------------
       LOGICAL OPEN11 , OPEN13 , LRSCRP , LSNULL , LPART  ,
     &         LGHOST , LTSET
       LOGICAL LPEND  , LOSEL  ,
     &         LPLT1  , LGRD1  , LDEF1  , LGRD3  , LDEF3
       LOGICAL LRSPEC
       LOGICAL LPASS  , LDOUT   , L2FILE
C-----------------------------------------------------------------------
       LOGICAL LSELA(8)  , LEXSA(8)   , LDEFA(8)       , LEXSS(8)
       LOGICAL LFILE(NDFILE)
       LOGICAL LPEC(NDLINE,NDCOMP)    , LTRNG(NTDIM)   , LDRNG(NTDIM)
       LOGICAL LACDA(IZDIMD,IPDIMD,IPDIMD),
     &         LSCDA(IZDIMD,IPDIMD,IPDIMD),
     &         LCCDA(IZDIMD,IPDIMD,IPDIMD),
     &         LQCDA(IZDIMD,IPDIMD,IPDIMD),
     &         LXCDA(IZDIMD,IPDIMD,IPDIMD),
     &         LPRBA(IZDIMD,IPDIMD),
     &         LPRCA(IZDIMD,IPDIMD),
     &         LPLTA(IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
       CHARACTER REP*3    , DSNINC*80 , DATE*8
       CHARACTER PROJ(NDFILE)*6  , GROUP(NDFILE)*8  , TYPE(NDFILE)*5 ,
     &           EXT(NDFILE)*3   , ION(NDFILE)*4    , MEMB(NDFILE)*8
       CHARACTER CLINE*80
       CHARACTER YEAR*2    , YEARDF*2  , XFELEM*12 , TITLE*40
       CHARACTER CHELEM*12
       CHARACTER UID*80
       CHARACTER ELEMT*2 , XFESYM*2   , FILTR*7
       CHARACTER DSN80*80
       CHARACTER GTIT1*40    , GTIT3*40
       CHARACTER DSFULL*80
C-----------------------------------------------------------------------
       CHARACTER DSFLLA(8)*120
       CHARACTER DSPECA(NDFILE)*120
       CHARACTER CIMET(NDLINE,NDCOMP)*1  , CINDPH(NDLINE,NDCOMP)*1  ,
     &           TITL(NDLINE,NDCOMP)*12  , SPECL(NDLINE,NDCOMP)*16  ,
     &           TITR(NDRAT)*25
       CHARACTER CIION(NDLINE)*2  , CMPTS(NDLINE)*2  , CTITLE(NDLINE)*12
       CHARACTER POPTIT(IMDIMD )*10 , CHZ*2, CHG*2
       CHARACTER CHPOP(IMDIMD)*14
C-----------------------------------------------------------------------
       INTEGER   I4UNIT  , I4FCTN  , IABT   , IRCODE
       INTEGER   NFILE   , IZ0    , NLINE  , NRAT
       INTEGER   I       , IZ
       INTEGER   IBSEL   , ITTYP   , ITMAX  , ITMAXS
       INTEGER   IZL     , IZH     ,
     &           IT      , IGRD    ,
     &           NSTAGE  , NMSUM   , IG     ,
     &           IZ1
C-----------------------------------------------------------------------
       INTEGER   NCOMP(NDLINE)
       INTEGER   IZION(NDLINE,NDCOMP)  , IMET(NDLINE,NDCOMP) ,
     &           INDPH(NDLINE,NDCOMP)  , IFILE(NDLINE,NDCOMP),
     &           ILINE(NDRAT)          , JLINE(NDRAT)
       INTEGER   IPLINE(NDLINE,NDCOMP)
       INTEGER   IZIP(IMDIMD) , IMIP(IMDIMD) ,
     &           IPIZM(IZDIMD,IPDIMD)
       INTEGER   NPRT(IZDIMD) ,  NPRTR(IZDIMD)
C-----------------------------------------------------------------------
       REAL*8    XL3        , XU3         , YL3         , YU3
       REAL*8    ZL         , ZH          ,
     &           AMSR       , AMSD        ,
     &           TIMEF      ,
     &           TIMEFS
C-----------------------------------------------------------------------
       REAL*8    XLGIN(2)   , XUGIN(2)
       REAL*8    YLG(2)      , YUG(2)
       REAL*8    TEIN(NTDIM), DEIN(NTDIM) , THIN(NTDIM) , DHIN(NTDIM)
       REAL*8    PECA(NTDIM,NDLINE,NDCOMP)
       REAL*8    RATA(NTDIM,NDRAT)
       REAL*8    TEV(NTDIM) , DTEV(NTDIM) ,
     &           TEVH(NTDIM), DTEVH(NTDIM),
     &           DENS(NTDIM), DDENS(NTDIM), DENSH(NTDIM), DDENSH(NTDIM),
     &           FPABUN(NTDIM,IMDIMD), FPINTG(NTDIM,IMDIMD) ,
     &           FSABUN(NTDIM,IMDIMD), FSINTG(NTDIM,IMDIMD)
       REAL*8    TEVS(NTDIM), TEVHS(NTDIM), DENSS(NTDIM), DENSHS(NTDIM)
       REAL*8    DTEVD(ITDIMD) , DDENSD(ITDIMD) , ZDATA(ISDIMD),
     &           DRCOFD(ISDIMD,ITDIMD,ITDIMD),
     &           DRCOFI(NTDIM)
       REAL*8    ACDA(NTDIM,IZDIMD,IPDIMD,IPDIMD),
     &           SCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD),
     &           CCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD),
     &           QCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD),
     &           XCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD),
     &           PRBA(NTDIM,IZDIMD,IPDIMD),
     &           PRCA(NTDIM,IZDIMD,IPDIMD),
     &           PLTA(NTDIM,IZDIMD,IPDIMD)
       REAL*8    POPE(IMDIMD)  , POPF(IMDIMD)  ,
     &           PINTE(IMDIMD) , PINTF(IMDIMD)
       REAL*8     ERBEQ(NTDIM) , ERCEQ(NTDIM)   ,  ELTEQ(NTDIM) ,
     &            ERADA(NTDIM) , ELTPEQ(NTDIM,IMDIMD)
       REAL*8    ACDSEQ(NTDIM,IZDIMD) , SCDSEQ(NTDIM,IZDIMD)   ,
     &           CCDSEQ(NTDIM,IZDIMD) , ERBSEQ(NTDIM,IZDIMD)   ,
     &           ERCSEQ(NTDIM,IZDIMD) , ELTSEQ(NTDIM,IZDIMD)
       REAL*8    GCFPEQ(NTDIM,NDLINE,NDCOMP) , GCFEQ(NTDIM,NDLINE)
       REAL*8    FABUN0(IMDIMD)  , FABUNS(IMDIMD)
C-----------------------------------------------------------------------
       DATA LGHOST/.FALSE./ , LOSEL/.TRUE./ , LSNULL/.FALSE./
C
       DATA ITMAXS/0/
       DATA TIMEFS/0.0/
       DATA OPEN11/.FALSE./ , OPEN13/.FALSE./
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ######################### MAIN PROGRAM ###############################
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
       CALL XX0000
C
C-----------------------------------------------------------------------
C      GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
       CALL XXDATE( DATE)
C
C-----------------------------------------------------------------------
C      GET USER IDENTIFIER AND SET FOR FILE OPENING
C-----------------------------------------------------------------------
C
       CALL XXUID(UID)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C      GET USER-DEFINED VALUES
C-----------------------------------------------------------------------
C
  500  CONTINUE
       LDOUT  = .FALSE.
       LRSCRP = .FALSE.
       LSNULL = .FALSE.
       LTSET  = .FALSE.
       LRSPEC = .FALSE.
C
C-----------------------------------------------------------------------
C      IF FILE IS OPEN ON UNIT 13 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
       IF(OPEN13) CLOSE(IUNT13)
       OPEN13=.FALSE.
C
C-----------------------------------------------------------------------
C      GET SELECTION FILE INPUT DATA SET NAME (FROM ISPF)
C-----------------------------------------------------------------------
C
       CALL D6SPF0( REP     ,
     &              DSFLLA  , DSNINC ,
     &              LSELA   , LEXSA  , LDEFA  , FILTR ,
     &              LPART   , LSNULL ,
     &              YEAR    , YEARDF ,
     &              IZ0
     &            )
       IF(REP.EQ.'YES')THEN
           GO TO 9999
       ENDIF
C
C-----------------------------------------------------------------------
C      OBTAIN SPECTRUM LINE DATA FROM SCRIPT FILE
C-----------------------------------------------------------------------
C
 5     IF(.NOT.LSNULL) THEN
          CALL D6SCRP( LRSCRP , LSNULL  ,
     &                  DSNINC , DSPECA  ,
     &                  NDLINE , NDCOMP  , NDRAT  , NDFILE ,
     &                  NFILE  , LFILE   ,
     &                  PROJ   , GROUP   , TYPE   , EXT    , ION    ,
     &                  MEMB   , IZ0     ,
     &                  NLINE  , NCOMP   ,
     &                  IZION  , IMET    , CIMET  , INDPH  , CINDPH ,
     &                  IFILE  , TITL    ,
     &                  NRAT   ,
     &                  ILINE  , JLINE   , TITR   , IRCODE
     &                )
          WRITE(PIPEOU, *) IRCODE
          CALL XXFLSH(PIPEOU)
          IF (IRCODE.EQ.1) THEN
             WRITE(I4UNIT(-1),3001)
             WRITE(I4UNIT(-1),2002)
             STOP
          ELSE IF (IRCODE.EQ.2) THEN
             WRITE(I4UNIT(-1),3002)
             WRITE(I4UNIT(-1),2002)
             STOP
          ELSE IF (IRCODE.EQ.3) THEN
             WRITE(I4UNIT(-1),3003)
             WRITE(I4UNIT(-1),2002)
             STOP
          ENDIF
       ENDIF

C
C-----------------------------------------------------------------------
C      MAP SPECTRUM LINE INFORMATION FOR PANEL DISPLAY
C-----------------------------------------------------------------------
C
       IF(.NOT.LSNULL) THEN
          DO 3 I=1,NLINE
C     
             IF(CIMET(I,1).EQ.'+') THEN
                IZ=IZION(I,1)-1
             ELSEIF(CIMET(I,1).EQ.'-')THEN
                IZ=IZION(I,1)+1
             ELSE
                IZ=IZION(I,1)
             ENDIF
C     
             WRITE(CIION(I),'(I2)')IZ
             WRITE(CMPTS(I),'(I2)')NCOMP(I)
             WRITE(CTITLE(I),'(A12)')TITL(I,1)
C     
 3        CONTINUE
       ELSE
          NLINE = 0
       ENDIF
CX
CX-----------------------------------------------------------------------
CX      SET SPECTRUM LINE NUMBER IN SHARED POOL VARIABLE
CX-----------------------------------------------------------------------
CX
CX       CALL D6SETL(NLINE)
C
C-----------------------------------------------------------------------
C      ESTABLISH METASTABLE PARTITION AND CHARGE RANGE FROM ACD FILE
C-----------------------------------------------------------------------
C
       IF(OPEN13) CLOSE(IUNT13)
       OPEN13=.FALSE.
C
       IF(LEXSA(1).OR.LDEFA(1))THEN
          OPEN( UNIT=IUNT13 , FILE=DSFLLA(1) , STATUS='UNKNOWN')
          OPEN13=.TRUE.
          READ(IUNT13,'(A80)') CLINE
          IZL = I4FCTN(CLINE(16:20),IABT)
          IZH = I4FCTN(CLINE(21:25),IABT)
          IF(IABT.GT.0.OR.IZL.GT.IZH.OR.IZL.LT.1.OR.
     &         IZH.GT.IZ0) THEN
             WRITE(I4UNIT(-1),2001)'INCORRECT ION LIMITS'
             WRITE(I4UNIT(-1),2002)
          ENDIF
          ZL = IZL
          ZH = IZH
          READ(IUNT13,'(A80)') CLINE
          IF(LPART) THEN
             READ(IUNT13,'(A80)') CLINE
             IF (INDEX(CLINE,'.').GT.0)THEN
                WRITE(I4UNIT(-1),2001)'INCORRECT STRUCTURE'
                WRITE(I4UNIT(-1),2002)
                STOP
             ELSE
                READ(CLINE,'(16I5)') (NPRT(I),I=1,
     &               MIN0(IZH-IZL+2,16))
             ENDIF
             IF(IZH+IZL-2.GT.16) THEN
                READ(IUNT13,'(16I5)') (NPRT(I),I=17,IZH-IZL+2)
             ENDIF
             READ(IUNT13,'(A80)') CLINE
             IF(CLINE(1:5).NE.'-----') THEN
                WRITE(I4UNIT(-1),2001)'INCORRECT STRUCTURE'
                WRITE(I4UNIT(-1),2002)
                STOP
             ENDIF
          ELSE
             DO 8 I = 1,IZH-IZL+2
                NPRT(I) = 1
 8           CONTINUE
          ENDIF
       ELSE
          WRITE(I4UNIT(-1),2001)'DOES NOT EXIST'
          WRITE(I4UNIT(-1),2002)
          STOP
       ENDIF
       CLOSE(IUNT13)
C
C-----------------------------------------------------------------------
C  DEFINE NUMBER OF STAGES AND TOTAL NUMBER OF METASTABLES
C-----------------------------------------------------------------------
       ELEMT  = XFESYM(IZ0)
       CHELEM = XFELEM(IZ0)
       NSTAGE = IZH - IZL + 2
C     
       NMSUM = 0
       DO 12 IZ1 = IZL, IZH + 1
          IZ = IZ1 -1
          IGRD = NPRT( IZ1-IZL + 1)
          DO 13 IG = 1,IGRD
             NMSUM = NMSUM + 1
             WRITE(CHZ , 1022) IZ
             WRITE(CHG , 1022) IG
             POPTIT(NMSUM) = ELEMT//'+'//CHZ//' ('//CHG//')'
C     WRITE( CHPOP(NMSUM) , 1112) NMSUM , POPTIT(NMSUM)
             WRITE( CHPOP(NMSUM) , 1112) POPTIT(NMSUM)
             IZIP(NMSUM)   = IZ1
             IMIP(NMSUM)   = IG
             IPIZM(IZ1-IZL+1,IG) = NMSUM
 13       CONTINUE
 12    CONTINUE
CX
CX-----------------------------------------------------------------------
CX      SET METASTABLE LIST IN SHARED POOL VARIABLE
CX-----------------------------------------------------------------------
CX
CX       CALL D6SETM( IMDIMD ,
CX     &              NMSUM  , CHPOP
CX     &            )
C
C-----------------------------------------------------------------------
C      GET USER-DEFINED VALUES
C-----------------------------------------------------------------------
C
       CALL D6ISPF( LPEND  , LSNULL ,
     &              IMDIMD , NTDIM  , CHPOP  ,
     &              NMSUM  , NLINE  , NGFPLN ,
     &              CIION  , CMPTS  , CTITLE ,
     &              TITLE  ,
     &              IBSEL  ,
     &              AMSR   , AMSD   ,
     &              TIMEF  , FABUN0 ,
     &              ITTYP  , ITMAX  ,
     &              TEIN   , DEIN   , THIN   , DHIN  ,
     &              LOSEL
     &            )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
      IF(LPEND ) GOTO 500

C***********************************************************************
C      ANALYSE ENTERED DATA - IF NOT END ANALYSIS
C***********************************************************************
C
C-----------------------------------------------------------------------
C      CONVERT TEMPERATURES TO EV, FORM LOG10 AND CONVERTT GRAPH SCALES
C-----------------------------------------------------------------------
C
C
           CALL XXTCON( ITTYP , L2 , IZ0 , ITMAX , TEIN , TEV  )
           CALL XXTCON( ITTYP , L2 , IZ0 , ITMAX , THIN , TEVH )
           CALL XXDCON( L1    , L1 , IZ0 , ITMAX , DEIN , DENS )
           CALL XXDCON( L1    , L1 , IZ0 , ITMAX , DHIN , DENSH)
C
C           XLG(1)  = R8TCON( ITTYP , L2 , IZ0 , XLGIN(1) )
C           XLG(2)  = R8TCON( ITTYP , L2 , IZ0 , XLGIN(2) )
C           XUG(1)  = R8TCON( ITTYP , L2 , IZ0 , XUGIN(1) )
C           XUG(2)  = R8TCON( ITTYP , L2 , IZ0 , XUGIN(2) )
C           XL3     = R8TCON( ITTYP , L2 , IZ0 , XL3IN    )
C           XU3     = R8TCON( ITTYP , L2 , IZ0 , XU3IN    )
C
C
C-----------------------------------------------------------------------
C  IF TEMPERATURES, DENSITIES, INITIAL FRACTIONS AND INTEGRATION
C  TIME ALREADY USED BYPASS REEVALUATON
C-----------------------------------------------------------------------
C
           IF(LTSET.AND.(ITMAX.EQ.ITMAXS).AND.(TIMEF.EQ.TIMEFS)) THEN
               DO 6 I = 1,ITMAX
                IF((TEV(I).NE.TEVS(I)).OR.(TEVH(I).NE.TEVHS(I)).OR.
     &             (DENS(I).NE.DENSS(I)).OR.(DENSH(I).NE.DENSHS(I)))
     &             GO TO 9
    6          CONTINUE
C
               DO 14 I = 1,NMSUM
                IF(FABUN0(I).NE.FABUNS(I))
     &             GO TO 9
   14          CONTINUE
C
               GO TO 300
           ENDIF
C
C
C
C-----------------------------------------------------------------------
C  SET TEMPERATURES AND DENSITIES AND BACKUP VALUES (INCL. TIMEF)
C-----------------------------------------------------------------------
C
    9      TIMEFS= TIMEF
C
           ITMAXS=ITMAX
C
           DO 7 I = 1 , ITMAX
C
            TEVS(I)   = TEV(I)
            TEVHS(I)  = TEVH(I)
            DENSS(I)  = DENS(I)
            DENSHS(I) = DENSH(I)
C
            DTEV(I)   = DLOG10(TEV(I))
            DDENS(I)  = DLOG10(DENS(I))
            
            IF(TEVH(I).GT.1.0D-70) THEN
               DTEVH(I) = DLOG10(TEVH(I))
            ELSE
               DTEVH(I) = -70.0
            ENDIF
            IF(DENSH(I).GT.1.0D-70) THEN
               DDENSH(I) = DLOG10(DENSH(I))
            ELSE
               DDENSH(I) = -70.0
            ENDIF
C
    7      CONTINUE
C
           DO 15 I=1,NMSUM
             FABUNS(I) = FABUN0(I)
   15      CONTINUE
C
C-----------------------------------------------------------------------
C      COMPUTE SPECTRUM LINE EMISSIVITIES
C-----------------------------------------------------------------------
C
           IF(.NOT.LSNULL) THEN
               CALL D6SPEC( LRSPEC ,
     &                      NDLINE , NDCOMP  , NDRAT  , NDFILE ,
     &                      NFILE  , LFILE   ,
     &                      PROJ   , GROUP   , TYPE   , EXT    ,
     &                      IZ0    , DSPECA  ,
     &                      NLINE  , NCOMP   ,
     &                      IZION  , IMET    , CIMET  , INDPH  ,
     &                      IFILE  ,
     &                      NTDIM  , ITMAX  ,
     &                      TEV    , DENS    , TEVH   , DENSH  ,
     &                      PECA   ,
     &                      LPEC   , LTRNG   , LDRNG
     &                    )
           ENDIF
C
C-----------------------------------------------------------------------
C      FETCH COMPLETE SET OF ISONUCLEAR MASTER FILE DATA
C-----------------------------------------------------------------------
C
           CALL D5DATA( DSFLLA , LSELA  , LEXSA  , LDEFA , LPART  ,
     &                  IZ0    , IZL    , IZH    , NPRT   ,
     &                  NTDIM  , ITMAX  ,
     &                  ISDIMD , IZDIMD , ITDIMD , IPDIMD , NPRTR ,
     &                  DTEV   , DDENS  ,
     &                  DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                  DRCOFI ,
     &                  ACDA   , LACDA  ,
     &                  SCDA   , LSCDA  ,
     &                  CCDA   , LCCDA  ,
     &                  PRBA   , LPRBA  ,
     &                  PRCA   , LPRCA  ,
     &                  QCDA   , LQCDA  ,
     &                  XCDA   , LXCDA  ,
     &                  PLTA   , LPLTA
     &                 )
C
C-----------------------------------------------------------------------
C      FETCH LOW TEMPERATURE DR RATE COEFFICIENTS
C-----------------------------------------------------------------------
C
C            IF ( LDRLT) THEN
C
C              DO IZZ=IZL,IZH
C                DO IT = 1,NTDIM
C                  DRLT(IT) = 0.0D+0
C                ENDDO
C                IZ1=IZZ
C                CALL DRLT2(  IZ0 , IZ1 , NTDIM , ITMAX, TEMP, DRLT)
C                WRITE(6,2405) IZ1, (DRLT(IT), IT=1,6)
C                DO IT = 1,ITMAX
C                  ACDA(IT,IZ1,1,1) = ACDA(IT,IZ1,1,1) + DRLT(IT)
C                ENDDO
C              ENDDO
C
C            ENDIF
C
C
C-----------------------------------------------------------------------
C  EVALUATE TRANSIENT AND EQUILIBIUM POPULATIONS OF ELEMENT METASTABLES
C  AND/OR IONISATION STAGES AT EACH TEMPERATURE/DENSITY SET
C-----------------------------------------------------------------------
C
           DO 100 IT=1,ITMAX
C
             CALL D6MPOP( NTDIM  , IZDIMD , IPDIMD , IMDIMD ,
     &                    NSTAGE , ITMAX  , NPRT   , NMSUM  ,
     &                    ACDA   , SCDA   , CCDA   , QCDA   , XCDA   ,
     &                    DENS   , DENSH  ,
     &                    FABUN0 ,
     &                    IT     , TIMEF  ,
     &                    POPE   , POPF   , PINTE  , PINTF
     &                   )
C
            DO 22 I=1,NMSUM
             FPABUN(IT,I) = POPF(I)
             FPINTG(IT,I) = PINTF(I)
   22       CONTINUE
C            WRITE(7,*)'IT,TEV = ',IT,TEV(IT)
C            WRITE(7,*)'(FPABUN(IT,I),I=1,NMSUM) = ',
C     &                 (FPABUN(IT,I),I=1,NMSUM)
C            WRITE(7,*)'(FPINTG(IT,I),I=1,NMSUM) = ',
C     &                 (FPINTG(IT,I),I=1,NMSUM)
C
  100      CONTINUE
C
C-----------------------------------------------------------------------
C  DETERMINE TRANSIENT RADIATED ENERGY EXCESSES AND
C  SUM METASTABLE POPULATIONS WITHIN CHARGE STATES
C
C  ALSO DETERMINE METASTABLE FRACTIONS
C-----------------------------------------------------------------------
C
             CALL D6SPOW( LSELA  , LEXSA  , LDEFA  , LPART  , LEXSS  ,
     &                    IZ0    , IZL    , IZH    , NPRT   ,
     &                    ISDIMD , IZDIMD , ITDIMD , IPDIMD , IMDIMD ,
     &                    ACDA   , SCDA   , CCDA   , PRBA   ,
     &                    PRCA   , QCDA   , XCDA   , PLTA   ,
     &                    NMSUM  , IZIP   , IMIP   , IPIZM  ,
     &                    NTDIM  , ITMAX  ,
     &                    DENS   , DENSH  ,
     &                    FPABUN , FSABUN , FPINTG , FSINTG ,
     &                    ELTPEQ ,
     &                    ACDSEQ , SCDSEQ , CCDSEQ , ERBSEQ ,
     &                    ERCSEQ , ELTSEQ ,
     &                    ERBEQ  , ERCEQ  , ELTEQ  , ERADA
     &                  )
C
C-----------------------------------------------------------------------
C  DETERMINE GCF FUNCTIONS
C-----------------------------------------------------------------------
C
             IF(.NOT.LSNULL)
     &       CALL D6SGCF( IZ0    , IZL    , IZH    ,
     &                    ISDIMD , IZDIMD , ITDIMD , IPDIMD , IMDIMD ,
     &                    NMSUM  , IZIP   , IMIP   , IPIZM  ,
     &                    NDLINE , NDCOMP ,
     &                    NLINE  , NCOMP  , SPECL  , IPLINE ,
     &                    IZION  , IMET   , CIMET  , INDPH  , CINDPH ,
     &                    IFILE    ,
     &                    NTDIM  , ITMAX  ,
     &                    DENS   , DENSH  ,
     &                    PECA   , LPEC   ,
     &                    FPINTG ,
     &                    GCFPEQ , GCFEQ  ,
     &                    NDRAT  , NRAT   ,
     &                    ILINE  , JLINE  ,
     &                    RATA
     &                  )
C
C-----------------------------------------------------------------------
C MARK EVALUATION FOR THESE TEMPERATURES AND DENSITIES AS SET
C----------------------------------------------------------------------
C
              LTSET = .TRUE.
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
CX UNIX PORT - ADDED NEW CALL TO NEW D6SPF1 ROUTINE WHICH COMMUNICATES  
CX             WITH IDL VIA A PIPE TO GET USER SELECTED OUTPUT OPTIONS. 
C-----------------------------------------------------------------------
C
300   CONTINUE
      CALL D6SPF1( LPLT1, LPASS, LPEND, DSN80, L2FILE, DSFULL,
     &             IGRAPH)
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
      IF(LPEND) GOTO 5
C

C
C***********************************************************************
C END OF CURRENT FILE ANALYSIS - ESTABLISH IF PASSING FILE OUTPUT REQ'D
C***********************************************************************
C
      IF(.NOT.LSNULL) THEN
C
C-----------------------------------------------------------------------
C OPEN PASSING FILE OUTPUT DATA SET - DSN80 - IF REQUESTED
C-----------------------------------------------------------------------
C
         IF (.NOT.OPEN11 .AND. LPASS ) THEN
            OPEN(UNIT=IUNT11,FILE=DSN80,STATUS='UNKNOWN')
            OPEN11=.TRUE.
         ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING GCF FILE - DSN80 - IF REQUESTED
C-----------------------------------------------------------------------
C
         IF (LPASS)
     &        CALL D6WR11( IUNT11 , DSNINC , DSFLLA(1), ELEMT ,
     &                     UID    , DATE   , IZ0      ,
     &                     NDLINE , NLINE  ,
     &                     TITL(1,1),IZION(1,1),CIMET(1,1),
     &                     NTDIM  , ITMAX  ,
     &                     TEV    , DENS   ,
     &                     GCFEQ
     &                   )
      ENDIF
C
C-----------------------------------------------------------------------
C      OUTPUT TO PAPER.TEXT
C-----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT07, FILE=DSFULL, STATUS='UNKNOWN')
         CALL D6OUT0( IUNT07   , IMDIMD , NDLINE , NDCOMP , NDRAT  ,
     &                NDFILE   , TITLE  , DATE   ,
     &                IZ0      , LPART  , YEAR   , YEARDF ,
     &                LSELA    , LEXSA  , LDEFA  , FILTR  ,
     &                NFILE    , DSNINC , DSPECA ,
     &                NTDIM    , ITMAX  ,
     &                TEV      , TEVH   , DENS   , DENSH  , TIMEF    ,
     &                IZL      , IZH    , NSTAGE , NMSUM  ,
     &                POPTIT   , FABUN0 , FPABUN ,
     &                ERBEQ    , ERCEQ  , ELTEQ  , ERADA  ,
     &                NLINE    , NCOMP  ,
     &                IZION    , IMET   , CIMET  , INDPH  , CINDPH ,
     &                IFILE    , TITL   , GCFPEQ , LPEC   , GCFEQ  ,
     &                NRAT     , ILINE  , JLINE  , TITR   ,  RATA
     &              )
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP GRAPHICAL OUTPUT IF REQUIRED - FRACTIONS THEN POWER FUNCTIONS
C-----------------------------------------------------------------------
C
 150  IF(LPLT1) THEN
         IF (IGRAPH.EQ.1) THEN
            CALL D6OTG1( LGHOST   , DATE     ,
     &                   IMDIMD   , NTDIM    ,
     &                   ELEMT    , TITLE    , GTIT1  , DSNINC ,
     &                   IZ0      , YEAR     , YEARDF ,
     &                   LGRD1    , LDEF1    ,
     &                   XLGIN(1) , XUGIN(1) , YLG(1) , YUG(1) ,
     &                   NMSUM    , ITMAX    ,
     &                   TEV      ,
     &                   TIMEF    ,
     &                   POPTIT   , FABUN0   , FPABUN
     &                 )
C
         ELSE IF (IGRAPH.EQ.2) THEN
                CALL D6OTG2( LGHOST , LEXSS , DATE   ,
     &                       IMDIMD , NTDIM ,
     &                       ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                       IZ0    , YEAR  , YEARDF ,
     &                       LGRD1  , LDEF1 ,
     &                       XLGIN(2) , XUGIN(2) , YLG(2) , YUG(2) ,
     &                       NMSUM  , ITMAX  ,
     &                       TEV    ,
     &                       TIMEF  ,
     &                       POPTIT , FABUN0 , ELTPEQ ,
     &                       ERBEQ  , ERCEQ  , ELTEQ  , ERADA
     &                     )
         ELSE IF (IGRAPH.EQ.4)THEN
                CALL D6OTG4( LGHOST , LEXSS , DATE   ,
     &                       IMDIMD , NTDIM ,
     &                       ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                       IZ0    , YEAR  , YEARDF ,
     &                       LGRD1  , LDEF1 ,
     &                       XLGIN(2) , XUGIN(2) , YLG(2) , YUG(2) ,
     &                       NMSUM  , ITMAX  ,
     &                       TEV    ,
     &                       TIMEF  ,
     &                       POPTIT , FABUN0 , ELTPEQ ,
     &                       ERBEQ  , ERCEQ  , ELTEQ  , ERADA
     &                     )
         ELSE
      
C
C-----------------------------------------------------------------------
C GRAPHICAL OUTPUT OF SELECTED LINE GCF FUNCTIONS
C-----------------------------------------------------------------------
C
            IF(.NOT.LSNULL)
     &           CALL D6OTG3( LGHOST , DATE  ,
     &                   IMDIMD , NTDIM , NDLINE , NDCOMP ,
     &                   ELEMT  , TITLE , GTIT3  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD3  , LDEF3 ,
     &                   XL3    , XU3   , YL3    , YU3    ,
     &                   NMSUM  , ITMAX ,
     &                   TEV    , POPTIT, FABUN0 ,
     &                   TIMEF  ,
     &                   IBSEL  ,
     &                   NLINE  , NCOMP ,
     &                   TITL   , SPECL , IPLINE ,
     &                   GCFPEQ , GCFEQ
     &                 )
         ENDIF

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 9999

      ENDIF
C-----------------------------------------------------------------------
      LDOUT = .TRUE.
      GOTO 300
C
 9999  STOP
C-----------------------------------------------------------------------
C
 1022 FORMAT(I2)
C 1112 FORMAT(I2,'. ',1A10)
 1112 FORMAT(1A10,'    ')
 2001 FORMAT(1X,32('*'),' ADAS406 ERROR ',32('*')//
     &       1X,'FAULT IN INPUT DATA FILE: ',A,I3,A)
 2002 FORMAT(/1X,28('*'),'  PROGRAM TERMINATED   ',27('*'))
 2405 FORMAT(1H ,' IZ1=',I3,3X,1P,6D10.2)
 3001 FORMAT(1X,32('*'),' D6SCRP ERROR ',32('*')//
     &       1X,'SCRIPT FILE DOES NOT EXIST ')
 3002 FORMAT(1X,32('*'),' D6SCRP ERROR ',32('*')//
     &       1X,'I/O ERROR READING FROM SCRIPT FILE ')
 3003 FORMAT(1X,32('*'),' D6SCRP ERROR ',32('*')//
     &       1X,'SCRIPT FILE CONTAINS INVALID FILE NAME(S) ')
C-----------------------------------------------------------------------
       END
