CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6spf0.for,v 1.3 2004/07/06 13:20:07 whitefor Exp $ Date $Date: 2004/07/06 13:20:07 $
CX
      SUBROUTINE D6SPF0( RP1    , DSFLLA   , DSNINC ,
     &                   LSELA  , LEXSA    , LDEFA  , FILTR  ,
     &                   LPART  , LSNULL   ,
     &                   YR     , YRD      ,
     &                   IZ0
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL - GETS NAME OF DATA FILE
C           (INPUT DATA SET SPECIFICATIONS).
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   RP1     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFLLA()= MASTER FILE DATA SET NAMES
C
C  OUTPUT: (C*80)  DSNINC  = SCRIPT DATA SET NAME
C
C  OUTPUT: (L*4)   LINFO   = .TRUE.  => SHOW MASTER FILE DATA SET
C                                       AVAILABILITY BEFORE RUN.
C                          = .FALSE. => DO NOT SHOW MASTER FILE DATA SET
C                                       AVAILABILITY BEFORE RUN.
C
C  OUTPUT: (L*4)   LSELA() = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       INDEX SELECTED
C                          = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                       NOT SELECTED
C  OUTPUT: (L*4)   LEXSA() = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       SELECTED INDEX EXISTS
C                          = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                       FOR THIS SELECTED INDEX
C  OUTPUT: (L*4)   LDEFA() = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                       DEFAULT YEAR INDEX EXISTS
C                          = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                       FOR THIS DEFAULT YEAR INDEX
C  OUTPUT: (L*4)   LPART   = .TRUE.  => PARTIAL DATA SELECTED
C                          = .FALSE. => STANDARD DATA SELECTED
C
C  OUTPUT: (L*4)   LSNULL  = .TRUE.  => SCRIPT FILE SET TO NULL
C                            .FALSE. => SCRIPT FILE VALID
C  OUTPUT: (C*2)   YR      = TWO DIGIT SELECTED YEAR NUMBER
C  OUTPUT: (C*2)   YRD     = TWO DIGIT DEFAULT YEAR NUMBER
C  OUTPUT: (I*4)   IZ0     = NUCLEAR CHARGE
C
C          (C*7)  FILTR    = POWER FILTER CHARACTERS FOR FILE NAMES
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -----------------------------------------------------------
C          DXDSNM     ADAS     VERIFY MULTIPLE DATA SET AVAILABILITY
C          I4EIZ0     AAAS     GET NUCLEAR CHARGE FROM ELEMENT SYMBOL
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    24/03/94
C
C UPDATE   04/08/94- H.P.SUMMERS- FETCH AND PASS NUCLEAR CHARGE
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:26/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C VERSION: 1.3				     DATE:25/03/97
C MODIFIED: RICHARD MARTIN
C	    - CHANGED FILTR FROM C*6 TO C*7 TO ACCOMODATE LONGER FILTER 
C	      NAMES IN ACCORDANCE WITH ADAS405 AND ADAS408
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IZ0           , I4EIZ0         , I         , ILOGIC
C-----------------------------------------------------------------------
      CHARACTER   DSNINC*80      , DSFLLA(8)*120  ,
     &            YR*2           , EL*2
      CHARACTER   YRD*2
      CHARACTER   FILTR*7
      CHARACTER   RP1*3
C-----------------------------------------------------------------------
      LOGICAL     LSNULL  ,
     &            LPART
      LOGICAL     LSELA(8)      , LEXSA(8)       , LDEFA(8)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU        , ONE      , ZERO
      PARAMETER(  PIPEIN=5      , PIPEOU=6      , ONE=1    , ZERO=0)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  READ BASIC OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') RP1
      IF (RP1.EQ.'NO') THEN
          READ(PIPEIN,'(A)') YR
          READ(PIPEIN,'(A)') YRD
          READ(PIPEIN,'(A)') EL
          READ(PIPEIN,'(A)') DSNINC
          READ(PIPEIN,'(A)') FILTR
          DO 1, I=1, 8
              READ(PIPEIN,'(A)') DSFLLA(I)
1         CONTINUE
          DO 2, I=1, 8
              READ(PIPEIN,*) ILOGIC
              IF (ILOGIC.EQ.1) THEN
                  LEXSA(I) = .TRUE.
              ELSE
                  LEXSA(I) = .FALSE.
              ENDIF
2         CONTINUE
          DO 3, I=1, 8
              READ(PIPEIN,*) ILOGIC
              IF (ILOGIC.EQ.1) THEN
                  LDEFA(I) = .TRUE.
              ELSE
                  LDEFA(I) = .FALSE.
              ENDIF
3         CONTINUE
          DO 4, I=1, 8
              READ(PIPEIN,*) ILOGIC
              IF (ILOGIC.EQ.1) THEN
                  LSELA(I) = .TRUE.
              ELSE
                  LSELA(I) = .FALSE.
              ENDIF
4         CONTINUE
          READ(PIPEIN,*) ILOGIC
          IF (ILOGIC.EQ.1) THEN 
              LPART = .TRUE.
          ELSE
              LPART = .FALSE.
          ENDIF
          IZ0 = I4EIZ0(EL)
          IF(INDEX(DSNINC,'NULL').GT.0) LSNULL=.TRUE.
C
C-----------------------------------------------------------------------
C NOW WRITE OUT VALUE OF LSNULL TO IDL
C-----------------------------------------------------------------------
          IF(LSNULL) THEN
              WRITE(PIPEOU,*) ONE
          ELSE
              WRITE(PIPEOU,*) ZERO
          ENDIF
          CALL XXFLSH(PIPEOU)
      ENDIF

      RETURN
      END
