CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6scrp.for,v 1.3 2004/07/06 13:19:47 whitefor Exp $ Date $Date: 2004/07/06 13:19:47 $
CX
       SUBROUTINE D6SCRP( LRSCRP , LSNULL  ,
     &                    DSNINC , DSPECA  ,
     &                    NDLINE , NDCOMP  , NDRAT  , NDFILE ,
     &                    NFILE  , LFILE   ,
     &                    UID    , GROUP   , TYPE   , EXT    , ION    ,
     &                    MEMB   , IZ0     ,
     &                    NLINE  , NCOMP   ,
     &                    IZION  , IMET    , CIMET  , INDPH  , CINDPH ,
     &                    IFILE  , TITL    ,
     &                    NRAT   ,
     &                    ILINE  , JLINE   , TITR   , IRCODE
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6SCRP  ********************
C
C  PURPOSE: TO READ SCRIPT FILE AND ACCESS EMISSIVITY DATA
C           ON SPECTRAL LINES REQUESTED FOR FURTHER PROCESSING IN
C           EQUILIBRIUM IONISATION CODES.
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  INPUT : (C*80)  DSNINC   = SCRIPT DATA SET NAME (FULL MVS DSN)
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C  INPUT : (I*4)   NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)   NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C  INPUT : (I*4)   NDRAT    = MAXIMUM NUMBER OF LINE RATIOS ALLOWED
C  INPUT : (I*4)   NDFILE   = MAXIMUM NUMBER OF EMISSIVITY FILES WHICH
C                             CAN BE SEARCHED
C
C  OUTPUT: (L*4)   LRSCRP   = .TRUE.  => SCRIPT FILE READ
C                             .FALSE. => SCRIPT FILE NOT READ
C  OUTPUT: (L*4)   LSNULL   = .TRUE.  => SCRIPT FILE SET TO NULL
C                             .FALSE. => SCRIPT FILE VALID
C  OUTPUT: (C*120) DSPECA() = PHOTON EMISSIVITY SOURCE FILES
C  OUTPUT: (I*4)   NFILE    = NUMBER OF PEC FILES TO BE SCANNED
C  OUTPUT: (L*4)   LFILE()  = .TRUE.  => PEC FILE EXISTS AND MATCHES
C                             .FALSE. => PEC FILE DOES NOT EXIST/MATCH
C  OUTPUT: (C*6)   UID()    = USER IDENTIFIER OF PEC FILE
C  OUTPUT: (C*8)   GROUP()  = GROUP IDENTIFIER OF PEC FILE
C  OUTPUT: (C*5)   TYPE()   = TYPE IDENTIFIER OF PEC FILE
C  OUTPUT: (C*3)   EXT()    = EXTENSION OF PEC FILE MEMBER NAME
C  OUTPUT: (C*4)   ION()    = ION NAME OF PEC FILE MEMBER NAME
C  OUTPUT: (C*8)   MEMB()   = MEMBER NAME OF PEC FILE
C  OUTPUT: (I*4)   NLINE    = NUMBER OF LINES IDENTIFIED IN SCRIPT
C  OUTPUT: (I*4)   NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C  OUTPUT: (I*4)   IZION(,) = CHARGE STATE OF COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (I*4)   IMET(,)  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (C*1)   CIMET(,) = SIGN (+, BLANK OR -) OF METASTABLE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (I*4)   INDPH(,) = PEC FILE INDEX OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (C*1)   CINDPH(,)= DRIVER (E OR BLANK => ELECTRONS)
C                                    (H          => HYDROGEN )
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (I*4)   IFILE(,) = INDEX OF PEC FILE IN FILE LIST
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (C*12)  TITL(,)  = TITLE FOR LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  OUTPUT: (I*4)   NRAT     = NUMBER OF RATIOS IDENTIFIED IN SCRIPT
C  OUTPUT: (I*4)   ILINE()  = INDEX OF NUMERATOR LINE FOR LINE RATIO
C  OUTPUT: (I*4)   JLINE()  = INDEX OF DENOMINATOR LINE FOR LINE RATIO
C  OUTPUT: (C*25)  TITR()   = TILE FOR LINE RATIO
C  OUTPUT: (I*4)   IRCODE   = ERROR FLAG:
C                             0 => SCRIPT FILE WAS READ OKAY
C                             1 => SCRIPT FILE DOES NOT EXIST
C                             2 => I/O ERROR READING THE SCRIPT FILE
C                             3 => 1 OR MORE FILE NAMES IN SCRIPT FILE
C                                  IS/ARE INVALID.
C
C          (I*4)   IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C          (L*4)   OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                            .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -----------------------------------------------------------
C          XXSLEN     ADAS      FIND NON-BLANK CHARACTERS IN STRING
C
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    20/04/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION. USED SOME CODE FROM D5SCRP.FOR V1.3 IN
C             ADDING IRCODE PARAMETER.
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C VERSION: 1.3				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - INCREASED LENGTH OF CLINE AND DSNPEC TO 120
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUNT10  , IRCODE
C-----------------------------------------------------------------------
      PARAMETER( IUNT10 = 10 )
C-----------------------------------------------------------------------
      INTEGER    NDLINE  , NDCOMP  , NDRAT   , NDFILE ,
     &           NLINE   , NRAT    , NFILE   , IFIRST , ILAST ,
     &           INDL    , INDC    , INDR    , I       , J      ,
     &           IZ      , IZ0
C-----------------------------------------------------------------------
      INTEGER    NCOMP(NDLINE)        ,
     &           IZION(NDLINE,NDCOMP) , IMET(NDLINE,NDCOMP)  ,
     &           INDPH(NDLINE,NDCOMP) , IFILE(NDLINE,NDCOMP) ,
     &           ILINE(NDRAT)         , JLINE(NDRAT)
C-----------------------------------------------------------------------
      CHARACTER  DSNINC*80   , CLINE*120 , ELEM*2    , CSTRNG*5  ,
     &           DSNPEC*120   , IONT*4    , CIZ*2
C-----------------------------------------------------------------------
      CHARACTER  CIMET(NDLINE,NDCOMP)*1 , CINDPH(NDLINE,NDCOMP)*1 ,
     &           TITL(NDLINE,NDCOMP)*12 , TITR(NDRAT)*25          ,
     &           UID(NDFILE)*6  , GROUP(NDFILE)*8, TYPE(NDFILE)*5 ,
     &           MEMB(NDFILE)*8 , EXT(NDFILE)*3  , ION(NDFILE)*4
      CHARACTER  DSPECA(NDFILE)*120
C-----------------------------------------------------------------------
      LOGICAL    OPEN10      , LRSCRP    , LEXIST   , LSNULL
C-----------------------------------------------------------------------
      LOGICAL    LFILE(NDFILE)
C-----------------------------------------------------------------------
      DATA OPEN10 /.FALSE./
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      IF(LRSCRP) RETURN
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
      IRCODE = 0
      IF (OPEN10) THEN
         CLOSE(10)
         OPEN10 = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT SCRIPT FILE - DSNINC
C-----------------------------------------------------------------------
C
      INQUIRE (FILE=DSNINC, EXIST=LEXIST)
      IF (LEXIST) THEN
          IRCODE = 0
          OPEN(UNIT=IUNT10,FILE=DSNINC,STATUS='UNKNOWN',ERR=9999)
          OPEN10=.TRUE.
      ELSE
          IRCODE = 1
      ENDIF
C
C-----------------------------------------------------------------------
C      READ IN ELEMENT, NUMBER OF LINES AND NUMBER OF FILES
C-----------------------------------------------------------------------
C
      IF (IRCODE.EQ.0) THEN
         ELEM   = '  '
         READ(IUNT10,'(A)') CLINE
         CSTRNG = CLINE(10:14)
         CALL XXSLEN(CSTRNG,IFIRST,ILAST)
         ELEM   = CSTRNG(IFIRST:ILAST)
         CALL XXEIZ0(ELEM , IZ0)
C     
         READ(IUNT10,'(A)') CLINE
         READ(CLINE(10:14),*) NLINE
         IF(NLINE.LE.0)GO TO 25
C     
         READ(IUNT10,'(A)') CLINE
         READ(CLINE(10:14),*) NRAT
         READ(IUNT10,'(A)') CLINE
         READ(CLINE(10:14),*) NFILE
C     
C-----------------------------------------------------------------------
C      READ IN EMISSIVITY FILE NAMES
C-----------------------------------------------------------------------
C
         DO 1 I = 1 , NFILE
            LFILE(I)= .FALSE.
            READ(IUNT10,'(A)') CLINE
            CALL XXSLEN(CLINE(31:120),IFIRST,ILAST)
            DSNPEC = CLINE(30+IFIRST:30+ILAST)
            DSPECA(I) = DSNPEC
            INQUIRE (FILE=DSNPEC, EXIST=LEXIST)
            IF (.NOT.LEXIST) THEN
               IRCODE = 3 
               RETURN
            ENDIF
 1       CONTINUE
C
 2       READ(IUNT10,'(A)',END=25) CLINE
         IF(INDEX(CLINE,'ILINE').LE.0)GO TO 2
C
C-----------------------------------------------------------------------
C      READ IN POINTERS FOR SPECTRAL LINES
C-----------------------------------------------------------------------
C
         DO 5 I=1,NLINE
            READ(IUNT10,'(A)') CLINE
            READ(IUNT10,'(A)') CLINE
            READ(CLINE,1000)INDL , NCOMP(I) , IZION(I,1)   , INDC,
     &           IMET(I,1)  , CIMET(I,1)  ,
     &           INDPH(I,1) , CINDPH(I,1) ,
     &           IFILE(I,1) , TITL(I,1)
            IF(NCOMP(INDL).GT.1) THEN
               DO 3 J=2,NCOMP(INDL)
                  READ(IUNT10,'(A)') CLINE
                  READ(CLINE,1001)IZION(I,J) , INDC,
     &                 IMET(I,J)  , CIMET(I,J)  ,
     &                 INDPH(I,J) , CINDPH(I,J) ,
     &                 IFILE(I,J) , TITL(I,J)
 3             CONTINUE
            ENDIF
 5       CONTINUE
C     
C-----------------------------------------------------------------------
C     CHECK FILE FOR EACH COMPONENT
C-----------------------------------------------------------------------
C
         DO 10 INDL=1,NLINE
            DO 7 INDC=1,NCOMP(INDL)
C     
               IF(CIMET(INDL,INDC).EQ.'+') THEN
                  IZ=IZION(INDL,INDC)-1
               ELSEIF(CIMET(INDL,INDC).EQ.'-')THEN
                  IZ=IZION(INDL,INDC)+1
               ELSE
                  IZ=IZION(INDL,INDC)
               ENDIF
               WRITE(CIZ,'(I2)')IZ
               IONT='    '
               IONT(1:2)=ELEM
C     
               CALL XXSLEN(CIZ,IFIRST,ILAST)
C     
               IF(IONT(2:2).EQ.' ')THEN
                  IONT(2:2+ILAST-IFIRST) = CIZ(IFIRST:ILAST)
               ELSE
                  IONT(3:3+ILAST-IFIRST) = CIZ(IFIRST:ILAST)
               ENDIF
C     
C     UNIX PORT: NOT NEEDED
C     IF(.NOT.LFILE(IFILE(INDL,INDC)))THEN
C     CALL XXDSN1 ( UID(IFILE(INDL,INDC))  ,
C     &                     GROUP(IFILE(INDL,INDC)),
C     &                     TYPE(IFILE(INDL,INDC)) ,
C     &                     MEMB(IFILE(INDL,INDC)) ,
C     &                     DSNFIL  , LEXIST
C     &               )
C             IF(LEXIST.AND.(IONT.EQ.ION(IFILE(INDL,INDC))))
C     &           LFILE(IFILE(INDL,INDC)) = .TRUE.
C         ENDIF
C
 7          CONTINUE
C     
 10      CONTINUE
C
         LSNULL = .FALSE.
C
C-----------------------------------------------------------------------
C      READ IN DATA ON SPECTRAL LINE RATIOS
C-----------------------------------------------------------------------
C
         IF(NRAT.GT.0)THEN
 15         READ(IUNT10,'(A)',END=30) CLINE
            IF(INDEX(CLINE,'IRATIO').LE.0)GO TO 15
C     
            READ(IUNT10,'(A)') CLINE
            DO 20 I=1,NRAT
               READ(IUNT10,'(A)') CLINE
               READ(CLINE,1002) INDR, ILINE(I), JLINE(I), TITR(I)
 20         CONTINUE
            GO TO 30
         ENDIF
 25      LSNULL = .TRUE.
C     
 30      LRSCRP = .TRUE.
         CLOSE(IUNT10)
      ENDIF
C
      RETURN
 9999 IRCODE=2
      RETURN
C-----------------------------------------------------------------------
 1000 FORMAT(I4,4X,I4,4X,I4,4X,I4,4X,I4,A1,3X,I4,A1,3X,I4,7X,A12)
 1001 FORMAT(16X,I4,4X,I4,4X,I4,A1,3X,I4,A1,3X,I4,7X,A12)
 1002 FORMAT(I4,4X,I4,4X,I4,7X,1A25)
C-----------------------------------------------------------------------
       END
