CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6otg1.for,v 1.3 2004/07/06 13:18:52 whitefor Exp $ Date $Date: 2004/07/06 13:18:52 $
CX
      SUBROUTINE D6OTG1( LGHOST , DATE   ,
     &                   IMDIMD , NTDIM  ,
     &                   ELEMT  , TITLE  , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR   , YEARDF ,
     &                   LGRD1  , LDEF1  ,
     &                   XMIN   , XMAX   , YMIN  , YMAX   ,
     &                   NMSUM  , ITMAX  ,
     &                   TEV    ,
     &                   TIMEF  ,
     &                   POPTIT , FABUN0 , FPABUN
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6OTG1 *********************
C
C  PURPOSE:  PASSES GRAPHICS DATA TO IDL.
C
C            PROVIDES  GRAPH OF METASTABLE FRACTIONAL ABUNDANCES. A
C            SINGLE GRAPH WILL CONTAIN UP TO SEVEN METASTABLES. (IF MORE
C            THAN SEVEN METASTABLES ARE PRESENT EXTRA GRAPHS WILL  BE
C            OUTPUT AS REQUIRED).
C
C            PLOT IS LOG10(N(META.)/N(TOTAL FOR ELEMENT)) VERSUS
C                    LOG10(ELECTRON TEMPERATURE (EV) )
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE    = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD  = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM   = MAXIMUM NUMBER OF DENSITIES ALLOWED
C
C  INPUT : (C*2)  ELEMT   = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE   = IDL ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1   = IDL ENTERED TITLE FOR GRAPH
C  INPUT : (C*80) DSNINC  = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR    = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF  = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                         = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                         = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN    = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX    = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN    = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX    = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM   = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX   = NUMBER OF INPUT ELECTRON DENSITIES
C
C  INPUT : (R*8)  TEV()   = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (R*8)  TIMEF   = INTEGRATION TIME (SEC)
C
C  INPUT : (C*10)POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (R*8) FABUN0() = INITIAL METASTABLE POPULATION FRACTIONS
C                             1ST DIMENSION: METASTABLE INDEX
C  INPUT : (R*8) FPABUN(,)= METASTABLE POPULATION FRACTIONS
C                             1ST DIMENSION: TEMPERATURE INDEX
C                             2ND DIMENSION: METASTABLE INDEX
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                           NUMBERS AS BEING ZERO = 1.0E-36
C          (I*4)  NDIM1   = PARAMETER = MAXIMUM NUMBER OF TEMP.   VALUES
C                           (MUST NOT BE LESS THAN 'NTDIM')
C          (I*4)  NDIM2   = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                           (MUST NOT BE LESS THAN 'IMDIMD')
C          (I*4)  NGPIC   = PARAMETER = MAXIMUM NUMBER OF LEVEL POPULAT-
C                           IONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV   = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                           WHICH CAN BE LISTED ON THE GRAPH.
C
C          (I*4)  IT      = TEMP. INDEX NUMBER FOR ARRAY USE
C          (I*4)  IM      = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX   = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
C          (R*4)  X()     = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            ELECTRON DENSITIES
C          (R*4)  Y(,)    = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                            LEVEL POPULATIONS.
C                            1ST DIMENSION = ELECTRON TEMP.   INDEX
C                            2nd DIMENSION = ORDINARY  LEVEL  INDEX
C
C          (C*80) ISPEC   = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*13) DNAME   = '       DATE: '
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*30) HEAD1   = HEADING FOR LEVEL ASSIGNMENTS
C          (C*30) STRG1   = HEADING FOR LEVEL ASSIGNMENTS
C
C          (L*4)  LGTXT   = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
C                         = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    28/04/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C
C VERSION: 1.3					DATE: 09-06-98
C MODIFIED: RICHARD MARTIN
C	    - INCREASED NTDIM1 TO 30.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 30  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
C-----------------------------------------------------------------------
      INTEGER   IMDIMD   , NTDIM
      INTEGER   NMSUM    , ITMAX    ,
     &          IZ0
      INTEGER   IT       , IM       ,
     &          IMMAX
C-----------------------------------------------------------------------
      REAL*4    TIME4    , GHZERO
C-----------------------------------------------------------------------
      INTEGER   PIPEIN, PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6 , GHZERO = 1.0E-36)
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX
      REAL*8    TIMEF
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*80
      CHARACTER YEAR*2   , YEARDF*2
      CHARACTER GRID*1   , PIC*1    , C3BLNK*3 , DATE*8   ,
     &          FILE0*9  , MNMX0*9  , KEY0*9   , ADAS0*8  ,
     &          DNAME*13 , GNAME*10 ,
     &          XTIT*25  , YTIT*25  ,
     &          HEAD1*40 ,
     &          STRG1*40 ,
     &          ISPEC*80
C-----------------------------------------------------------------------
      REAL*4    X(NDIM1)            , Y(NDIM1,NDIM2)
C-----------------------------------------------------------------------
      CHARACTER POPTIT(IMDIMD)*10   , SY0(NDIM2)*10
      CHARACTER KEY(3)*22
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM)  , FABUN0(IMDIMD)
      REAL*8    FPABUN(NTDIM,IMDIMD)
C-----------------------------------------------------------------------
      DATA ISPEC(1:40)
     &           /'ION FRACTION  VS  ELECTRON TEMPERATURE: '/
      DATA XTIT  /'ELECTRON TEMPERATURE (eV)'/
      DATA YTIT  /'N(INDX)/N(TOTAL)         '/
      DATA ADAS0 /'ADAS   :'/                ,
     &     FILE0 /'FILE   : '/               ,
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(FULL LINE - TOTAL   )'/  ,
     &     KEY(2)/' (DASH LINE - PARTIAL)'/  ,
     &     KEY(3)/'                    ) '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/    ,
     &     C3BLNK/'   '/
      DATA DNAME /'       DATE: '/,
     &     GNAME /'SPECIES : '/
      DATA HEAD1 /'--------- METASTABLE ASSIGMENTS --------'/
      DATA STRG1 /'INDX         DESIGNATION     INIT. FRAC.'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LGTXT = .TRUE.
      IMMAX = MIN0( NGLEV , NMSUM )
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
      ISPEC(41:80) = TITLE
C
C      CALL XXELEM(IZ0,ENAME)
C      WRITE(GTIT1,1000) ENAME,YEAR,YEARDF
C
C-----------------------------------------------------------------------
C CONVERT TIMEF AND TEMPERATURE VALUES TO REAL*4
C-----------------------------------------------------------------------
C
         TIME4 = TIMEF
C
         DO 1 IT=1,ITMAX
            X(IT)=REAL(TEV(IT))
    1    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT META. FRACTIONS TO REAL*4
C-----------------------------------------------------------------------
C
         DO 3 IM=1,NMSUM
            WRITE(SY0(IM),'(1P,E10.2)')FABUN0(IM)
            DO 4 IT=1,ITMAX
               Y(IT,IM)=FPABUN(IT,IM)
C               IF (Y(IT,IM).LT.GHZERO) Y(IT,IM)=GHZERO
    4       CONTINUE
    3    CONTINUE
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      DO 200, IT=1, ITMAX
          WRITE(PIPEOU, *) X(IT)
          CALL XXFLSH(PIPEOU)
200   CONTINUE
      WRITE(PIPEOU, *) NMSUM
      CALL XXFLSH(PIPEOU)
      DO 201, IT=1, ITMAX
          DO 202, IM=1, NMSUM
              WRITE(PIPEOU, *) Y(IT,IM)
              CALL XXFLSH(PIPEOU)
202       CONTINUE
201   CONTINUE
      DO 203, IM=1, NMSUM
          WRITE(PIPEOU,'(A10)') POPTIT(IM)
          CALL XXFLSH(PIPEOU)
203   CONTINUE
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)TIME4
      CALL XXFLSH(PIPEOU)
      DO 206 IM=1,NMSUM
         WRITE(PIPEOU,*)SY0(IM)
         CALL XXFLSH(PIPEOU)
 206  CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
