CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6spow.for,v 1.2 2004/07/06 13:20:26 whitefor Exp $ Date $Date: 2004/07/06 13:20:26 $
CX
       SUBROUTINE D6SPOW( LSELA  , LEXSA  , LDEFA  , LPART  , LEXSS  ,
     &                    IZ0    , IZL    , IZH    , NPART  ,
     &                    ISDIMD , IZDIMD , ITDIMD , IPDIMD , IMDIMD ,
     &                    ACDA   , SCDA   , CCDA   , PRBA   ,
     &                    PRCA   , QCDA   , XCDA   , PLTA   ,
     &                    NMSUM  , IZIP   , IMIP   , IPIZM  ,
     &                    NTDIM  , ITMAX  ,
     &                    DENS   , DENSH  ,
     &                    FPABUN , FSABUN , FPINTG , FSINTG ,
     &                    ELTPEQ ,
     &                    ACDSEQ , SCDSEQ , CCDSEQ , ERBSEQ ,
     &                    ERCSEQ , ELTSEQ ,
     &                    ERBEQ  , ERCEQ  , ELTEQ  , ERADA
     &                  )
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6SPOW *********************
C
C PURPOSE : TO ASSEMBLE RADIATED ENERGY EXCESS FUNCTIONS USING
C           FRACTIONAL METASTABLE ABUNDANCES INTEGRAL EXCESSES
C
C NOTE    : THE SOURCE ISONUCLEAR MASTER FILE DATA ARE OBTAINED BY A
C           PRIOR CALL TO SUBROUTINE D6DATA FROM SEQUENTIAL FILES
C           WITH THE FOLLOWING NAMING CONVENTIONS:
C
C                   (1) JETSHP.ACD<YR>#<EL).<CODE>DATA
C                   (2) JETSHP.SCD<YR>#<EL>.<CODE>DATA
C                   (3) JETSHP.CCD<YR>#<EL>.<CODE>DATA
C                   (4) JETSHP.PRB<YR>#<EL>.<FILT>.<CODE>DATA
C                   (5) JETSHP.PRC<YR>#<EL>.<FILT>.<CODE>DATA
C                   (6) JETSHP.QCD<YR>#<EL>.<CODE>DATA
C                   (7) JETSHP.XCD<YR>#<EL>.<CODE>DATA
C                   (8) JETSHP.PLT<YR>#<EL>.<CODE>DATA
C
C       WHERE, <YR>   = TWO DIGIT YEAR NUMBER
C              <EL>   = ONE OR TWO CHARACTER ELEMENT SYMBOL
C              <CODE> = R       => PARTIAL DATA
C                       U       => PARTIAL DATA
C                       OMITTED => STANDARD DATA
C              <FILT> = SIX CHARACTER POWER FILTER CODE
C
C       AND DATA OF CLASSES 6 AND 7 DO NOT EXIST FOR THE PARTIAL CASE.
C
C
C INPUT  : (L*4)  LSELA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        INDEX SELECTED
C                           = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                        NOT SELECTED
C INPUT  : (L*4)  LEXSA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        SELECTED INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS SELECTED INDEX
C INPUT  : (L*4)  LDEFA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        DEFAULT YEAR INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS DEFAULT YEAR INDEX
C INPUT  : (L*4)  LPART     = .TRUE.  => PARTIAL DATA SELECTED
C                           = .FALSE. => STANDARD DATA SELECTED
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  IZL       = MINIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  IZH       = MAXIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZL-1 TO
C                             IZH ON INPUT
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH
C                             IONISATION STAGE
C INPUT  : (I*4)  IMDIMD    = MAXIMUM NUMBER OF METASTABLES
C
C INPUT  : (R*8)  ACDA(,,,) = INTERPOLATION OF ACD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C INPUT  : (R*8)  SCDA(,,,) = INTERPOLATION OF SCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C INPUT  : (R*8)  CCDA(,,,) = INTERPOLATION OF CCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C INPUT  : (R*8)  PRBA(,,)  = INTERPOLATION OF PRB COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C INPUT  : (R*8)  PRCA(,,)  = INTERPOLATION OF PRC COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C INPUTT : (R*8)  QCDA(,,,) = INTERPOLATION OF QCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: FIRST METASTABLE INDEX
C                             4TH DIM: SECOND METASTABLE INDEX
C INPUT  : (R*8)  XCDA(,,,) = INTERPOLATION OF XCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: FIRST PARENT METASTABLE INDEX
C                             4TH DIM: SECOND PARENT METASTABLE INDEX
C INPUT  : (R*8)  PLTA(,,)  = INTERPOLATION OF PLT COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: METASTABLE INDEX
C INPUT  : (I*4)  NMSUM     = TOTAL NUMBER OF POPULATIONS
C
C INPUT  :        IZIP()    = ION CHARGE +1 (IZ1) OF METASTABLE IN LIST
C INPUT  :        IMIP()    = METASTABLE INDEX WITHIN CHARGE STATE IZ1
C                             OF METASTABLE INDEX FROM COMPLETE LIST
C INPUT  :        IPIZM(,)  = METASTABLE INDEX IN COMPLETE LIST
C                             1ST DIM: INDEX IZ1-IZL+1
C                             2ND DIM: METASTABLE COUNT FOR STAGE (IGRD)
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV/DDENS PAIRS
C INPUT  : (I*4)  ITMAX     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C INPUT  : (R*8)  DENS()    = ELECTRON DENSITIES (CM-3))
C INPUT  : (R*8)  DENSH()   = HYDROGEN DENSITIES (CM-3))
C INPUT  : (R*8)  FPABUN(,) = RESOLVED METASTABLE EQUILIBRIUM
C                             FRACTIONAL ABUNDANCES
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR
C                             2ND DIM: - METASTABLE INDEX
C INPUT  : (R*8)  FPINTG(,) = RESOLVED TRANSIENT METASTABLE POPULATION
C                             EXCESS INTEGRALS
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR
C                             2ND DIM: - METASTABLE INDEX
C OUTPUT : (L*4)  LEXSS()   = .TRUE.  => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX GENERATED
C                           = .FALSE. => OUTPUT STANDARD MASTER DATA FOR
C                                        THIS INDEX NOT GENERATED
C OUTPUT : (R*8)  FSABUN(,) = STAGE EQUILIBRIUM FRACTIONAL ABUNDANCES
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  FSINTG(,) = STAGE TRANSIENT FRACTIONAL ABUNDANCES
C                             EXCESSES
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  ELTPEQ(,) = METASTABLE PARTIAL TRANSIENT RADIATED
C                             LINE ENERGY EXCESS FUNCTIONS
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR
C                             2ND DIM: - METASTABLE INDEX
C OUTPUT : (R*8)  ACDSEQ(,) = STANDARD (UNRESOLVED) ACD COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  SCDSEQ(,) = STANDARD (UNRESOLVED) SCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  CCDSEQ(,) = STANDARD (UNRESOLVED) CCD COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  ERBSEQ(,) = STANDARD (UNRESOLVED) RB ENERGY EXCESS
C                             COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  ERCSEQ(,) = STANDARD (UNRESOLVED) RC ENERGY EXCESS
C                             COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  ELTSEQ(,) = STANDARD (UNRESOLVED) LT ENERGY EXCESS
C                             COEFFICIENT
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR INDEX
C                             2ND DIM: - CHARGE STATE INDEX (IZ1-IZL+1)
C OUTPUT : (R*8)  ERBEQ()   = TOTAL TRANSIENT RADIATED RECOM-BREMS
C                                   ENERGY EXCESS FUNCTION
C OUTPUT : (R*8)  ERCEQ()   = TOTAL TRANSIENT CX RADIATED RECOM ENERGY
C                                   EXCESS FUNCTION NORMALISED TO
C                                   ELECTRON DENSITY
C OUTPUT : (R*8)  ELTEQ()   = TOTAL TRANSIENT RADIATED LINE ENERGY
C                                   EXCESS FUNCTION
C OUTPUT : (R*8)  ERADA()   = TOTAL TRANSIENT RADIATED ENERGY EXCESS
C                                   FUNCTION
C
C PROGRAM: (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  IP        = GENERAL INDEX FOR CHARGE
C          (I*4)  IZ1       = GENERAL INDEX FOR CHARGE+1
C          (I*4)  ICL       = GENERAL INDEX FOR CLASS
C          (I*4)  IPP       = GENERAL PARENT INDEX
C          (I*4)  IPG       = GENERAL GROUND INDEX
C          (I*4)  IZREF     = GENERAL CHARGE STAE POINTER INDEX
C          (I*4)  IPRT      = GENERAL INDEX FOR PARENT METASTABLE
C          (I*4)  IGRD      = GENERAL INDEX FOR METASTABLE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR : H. P. SUMMERS, JET
C          K1/1/57
C          JET  EXT. 4941
C
C DATE   : 28/04/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
       INTEGER   IZ0
       INTEGER   IZ1     , IZL     , IZH     , NTDIM   , ITMAX   , NMSUM
       INTEGER   ISDIMD  , IZDIMD  , ITDIMD  , IPDIMD  , IMDIMD
       INTEGER   IT      , IPRT    , IGRD
       INTEGER   ICL     , IP      , IZREF   , IPP     , IPG
C-----------------------------------------------------------------------
       INTEGER   NPART(IZDIMD)
       INTEGER   IZIP(IMDIMD)   , IMIP(IMDIMD) ,
     &           IPIZM(IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
       REAL*8    DNS           , DNSH
C-----------------------------------------------------------------------
       REAL*8    DENS(NTDIM)   , DENSH(NTDIM)   ,
     &           ERBEQ(NTDIM)  , ERCEQ(NTDIM)   ,  ELTEQ(NTDIM) ,
     &           ERADA(NTDIM)
C
       REAL*8    FPABUN(NTDIM,IMDIMD) , ELTPEQ(NTDIM,IMDIMD)
       REAL*8    FSABUN(NTDIM,IZDIMD)
C
       REAL*8    FSINTG(NTDIM,IZDIMD) , FPINTG(NTDIM, IZDIMD)
C
       REAL*8    ACDSEQ(NTDIM,IZDIMD) , SCDSEQ(NTDIM,IZDIMD)   ,
     &           CCDSEQ(NTDIM,IZDIMD) , ERBSEQ(NTDIM,IZDIMD)   ,
     &           ERCSEQ(NTDIM,IZDIMD) , ELTSEQ(NTDIM,IZDIMD)
C
       REAL*8    ACDA(NTDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &           SCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &           CCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &           PRBA(NTDIM,IZDIMD,IPDIMD)        ,
     &           PRCA(NTDIM,IZDIMD,IPDIMD)        ,
     &           QCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &           XCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD) ,
     &           PLTA(NTDIM,IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
       LOGICAL   LPART
C-----------------------------------------------------------------------
       LOGICAL     LSELA(8)      , LEXSA(8)       , LDEFA(8)  ,
     &             LEXSS(8)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  CHECK DATA AVAILABILITY AND SET LOGICAL VARIABLES
C-----------------------------------------------------------------------
C
           DO 1 ICL = 1,8
            IF(LEXSA(ICL).OR.LDEFA(ICL)) THEN
                LEXSS(ICL) = .TRUE.
            ELSE
                LEXSS(ICL) = .FALSE.
            ENDIF
    1      CONTINUE
           LEXSS(6) = .FALSE.
           LEXSS(7) = .FALSE.
C
C-----------------------------------------------------------------------
C  CALCULATE POWER FUNCTIONS AND PREPARE STANDARD MASTER FILE DATA
C-----------------------------------------------------------------------
C
           DO 100 IT=1,ITMAX
C
            DNS  = DENS(IT)
            DNSH = DENSH(IT)
C
            ERBEQ(IT) = 0.0D0
            ERCEQ(IT) = 0.0D0
            ELTEQ(IT) = 0.0D0
C
            DO 5 IP = 1, IMDIMD
             ELTPEQ(IT,IP) = 0.0D+0
    5       CONTINUE
C
            DO 50 IZ1 = IZL , IZH + 1
C
             IZREF = IZ1-IZL+1
C     WRITE(6,*)'IT,IZ1,IZREF = ',IT,IZ1,IZREF
C
             FSABUN(IT,IZREF) = 0.0D0
             FSINTG(IT,IZREF) = 0.0D0
C
             IF(IZ1.LE.IZH) THEN
                 ACDSEQ(IT,IZREF) = 0.0D0
                 SCDSEQ(IT,IZREF) = 0.0D0
                 CCDSEQ(IT,IZREF) = 0.0D0
                 ERBSEQ(IT,IZREF) = 0.0D0
                 ERCSEQ(IT,IZREF) = 0.0D0
                 ELTSEQ(IT,IZREF) = 0.0D0
             ENDIF
C
             DO 10 IP = 1,NMSUM
               IF( IZIP(IP) .EQ. IZ1 ) THEN
                    FSABUN(IT,IZREF) = FSABUN(IT,IZREF) +
     &                                 FPABUN(IT,IP)
                    FSINTG(IT,IZREF) = FSINTG(IT,IZREF) +
     &                                 FPINTG(IT,IP)
               ENDIF
   10        CONTINUE
C
             IF( IZ1 .LE. IZH ) THEN
C
                 DO 20 IPRT = 1, NPART(IZREF+1)
C
                  IPP = IPIZM( IZREF+1, IPRT)
C
                  ERBSEQ(IT,IZREF) = ERBSEQ(IT,IZREF) + FPINTG(IT,IPP)*
     &                               PRBA(IT,IZREF,IPRT)
                  ERCSEQ(IT,IZREF) = ERCSEQ(IT,IZREF) + FPINTG(IT,IPP)*
     &                               PRCA(IT,IZREF,IPRT)
C
                  DO 15 IGRD = 1, NPART(IZREF)
C
                   IPG = IPIZM( IZREF, IGRD)
C
                   ACDSEQ(IT,IZREF) = ACDSEQ(IT,IZREF) +
     &                                FPABUN(IT,IPP)*
     &                                ACDA(IT,IZREF,IPRT,IGRD)
                   SCDSEQ(IT,IZREF) = SCDSEQ(IT,IZREF) +
     &                                FPABUN(IT,IPG)*
     &                                SCDA(IT,IZREF,IPRT,IGRD)
                   CCDSEQ(IT,IZREF) = CCDSEQ(IT,IZREF) +
     &                                FPABUN(IT,IPP)*
     &                                CCDA(IT,IZREF,IPRT,IGRD)
C
   15             CONTINUE
C
   20            CONTINUE
C
                 DO 25 IGRD = 1, NPART(IZREF)
C
                  IPG = IPIZM( IZREF, IGRD)
C
                  ELTPEQ(IT,IPG)   = FPINTG(IT,IPG)*
     &                               PLTA(IT,IZREF,IGRD)
                  ELTSEQ(IT,IZREF) = ELTSEQ(IT,IZREF) + FPINTG(IT,IPG)*
     &                               PLTA(IT,IZREF,IGRD)
C
   25            CONTINUE
C
             ENDIF
C
   50       CONTINUE
C
            DO 70 IZ1 = IZL , IZH
C
             IZREF = IZ1-IZL+1
C
             ERBEQ(IT) = ERBEQ(IT)+ERBSEQ(IT,IZREF)
             ERCEQ(IT) = ERCEQ(IT)+ERCSEQ(IT,IZREF)
             ELTEQ(IT) = ELTEQ(IT)+ELTSEQ(IT,IZREF)
C
             IF(FSABUN(IT,IZREF+1).GT.0.0D0)THEN
                 ACDSEQ(IT,IZREF) = ACDSEQ(IT,IZREF)/FSABUN(IT,IZREF+1)
                 CCDSEQ(IT,IZREF) = CCDSEQ(IT,IZREF)/FSABUN(IT,IZREF+1)
                 ERBSEQ(IT,IZREF) = ERBSEQ(IT,IZREF)/FSINTG(IT,IZREF+1)
                 ERCSEQ(IT,IZREF) = ERCSEQ(IT,IZREF)/FSINTG(IT,IZREF+1)
             ELSE
                 ACDSEQ(IT,IZREF) = 0.0D0
                 CCDSEQ(IT,IZREF) = 0.0D0
                 ERBSEQ(IT,IZREF) = 0.0D0
                 ERCSEQ(IT,IZREF) = 0.0D0
             ENDIF
C
             IF(FSABUN(IT,IZREF).GT.0.0D0)THEN
                 SCDSEQ(IT,IZREF) = SCDSEQ(IT,IZREF)/FSABUN(IT,IZREF)
                 ELTSEQ(IT,IZREF) = ELTSEQ(IT,IZREF)/FSINTG(IT,IZREF)
             ELSE
                 SCDSEQ(IT,IZREF) = 0.0D0
                 ELTSEQ(IT,IZREF) = 0.0D0
             ENDIF
C
   70       CONTINUE
C
            ERCEQ(IT) = DNSH*ERCEQ(IT)/DNS
            ERADA(IT) = ERBEQ(IT) + ERCEQ(IT) + ELTEQ(IT)
C
  100       CONTINUE
C
       RETURN
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
       END
