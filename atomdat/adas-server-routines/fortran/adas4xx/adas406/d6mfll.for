CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6mfll.for,v 1.2 2007/03/28 14:18:45 allan Exp $ Date $Date: 2007/03/28 14:18:45 $
CX
       SUBROUTINE D6MFLL ( NTDIM  , IZDIMD , IPDIMD , IMDIMD ,          
     &                     NMDIM  ,                                     
     &                     NSTAGE , NPART  ,                            
     &                     ACDA   , SCDA   , CCDA   , QCDA   , XCDA   , 
     &                     DENS   , DENSH  ,                            
     &                     ITEM   ,                                     
     &                     A                                            
     &                   )                                              
       IMPLICIT NONE                                                    
C-----------------------------------------------------------------------
C                                                                       
C  ****************** FORTRAN 77 SUBROUTINE: D6MFLL ********************
C                                                                       
C  PURPOSE: FILLS MATRIX WITH RECOMBINATION, IONISATION  AND METASTABLE 
C           CROSS-COUPLING COEFFICIENTS READY FOR EIGENVECTOR SOLUTION  
C                                                                       
C  CALLING PROGRAM: D6MPOP                                              
C                                                                       
C  SUBROUTINE:                                                          
C                                                                       
C                                                                       
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV/DDENS PAIRS        
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES           
C                             IN ISONUCLEAR MASTER FILES                
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH    
C                             IONISATION STAGE                          
C INPUT  : (I*4)  IMDIMD    = MAXIMUM NUMBER OF POPULATIONS             
C                                                                       
C INPUT  : (I*4)  NMDIM     = MAX. NUMBER OF POPULATIONS (FROM D6MPOP)  
C                                                                       
C INPUT  : (I*4)  NSTAGE    = NUMBER OF IONISATION STATES (EXCL.        
C                             EXTRA ONE (BARE NUCLEUS) ADDED AT END     
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF     
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO 
C                             IZ1MAX ON INPUT                           
C                                                                       
C INPUT  : (R*8)  ACDA(,,,) = INTERPOLATION OF ACD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX                
C                             2ND DIM: CHARGE STATE INDEX               
C                             3RD DIM: RECOMBINING METASTABLE INDEX     
C                             4TH DIM: RECOMBINED METASTABLE INDEX      
C INPUT  : (R*8)  SCDA(,,,) = INTERPOLATION OF SCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX                
C                             2ND DIM: CHARGE STATE INDEX               
C                             3RD DIM: RECOMBINING METASTABLE INDEX     
C                             4TH DIM: RECOMBINED METASTABLE INDEX      
C INPUT  : (R*8)  CCDA(,,,) = INTERPOLATION OF CCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX                
C                             2ND DIM: CHARGE STATE INDEX               
C                             3RD DIM: RECOMBINING METASTABLE INDEX     
C                             4TH DIM: RECOMBINED METASTABLE INDEX      
C INPUT  : (R*8)  QCDA(,,,) = INTERPOLATION OF QCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX                
C                             2ND DIM: CHARGE STATE INDEX               
C                             3RD DIM: FIRST METASTABLE INDEX           
C                             4TH DIM: SECOND METASTABLE INDEX          
C INPUT  : (R*8)  XCDA(,,,) = INTERPOLATION OF XCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX                
C                             2ND DIM: CHARGE STATE INDEX               
C                             3RD DIM: FIRST PARENT METASTABLE INDEX    
C                             4TH DIM: SECOND PARENT METASTABLE INDEX   
C                                                                       
C INPUT  : (R*8)  DENS()    = ELECTRON DENSITIES FOR MODEL              
C INPUT  : (R*8)  DENSH()   = NEUTRAL HYDROGEN DENSITIES FOR MODEL      
C                                                                       
C INPUT  : (I*4)  ITEM      = CURRENT TEMP/DENSITY INDEX                
C                                                                       
C OUTPUT : (R*8)  A(,)      = RECOMB/IONIS COLL. RAD. MATRIX            
C                                                                       
C          (I*4)  I         = GENERAL INDEX                             
C          (I*4)  IGRD      = GENERAL INDEX                             
C          (I*4)  IND       = GENERAL INDEX                             
C          (I*4)  IND1      = GENERAL INDEX                             
C          (I*4)  IPRT      = GENERAL INDEX                             
C          (I*4)  IZ        = IONISATION STAGE COUNTER                  
C          (I*4)  I         = GENERAL INDEX                             
C          (I*4)  JGRD      = GENERAL INDEX                             
C                                                                       
C                                                                       
C ROUTINES:                                                             
C          ROUTINE    SOURCE    BRIEF DESCRIPTION                       
C          ------------------------------------------------------------ 
C                                                                       
C                                                                       
C AUTHOR : H. P. SUMMERS, JET                                           
C          K1/1/57                                                      
C          JET  EXT. 4941                                               
C                                                                       
C DATE   : 27/07/94                                                     
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C
C VERSION: 1.2				     DATE:28/03/07
C MODIFIED: ALLAN WHITEFORD
C	    - REMOVED VERSIONING INFORMATION FROM COLUMNS 73-80
C
C-----------------------------------------------------------------------
       INTEGER   NTDIM   , IZDIMD  , IPDIMD  , IMDIMD                   
       INTEGER   NMDIM   , NSTAGE                                       
       INTEGER   ITEM                                                   
       INTEGER   I       , IGRD    , IND     , IND1    , IPRT    ,      
     &           IZ      , J       , JGRD                               
C-----------------------------------------------------------------------
       INTEGER   NPART(IZDIMD)                                          
C-----------------------------------------------------------------------
       REAL*8    ACDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)                       
       REAL*8    SCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)                       
       REAL*8    CCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)                       
       REAL*8    QCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)                       
       REAL*8    XCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)                       
C                                                                       
       REAL*8    DENS(NTDIM) , DENSH(NTDIM)                             
C                                                                       
       REAL*8    A(NMDIM,NMDIM)                                         
C-----------------------------------------------------------------------
C                                                                       
C-----------------------------------------------------------------------
C      ZERO IONISATION/RECOMBINATION MATRIX                             
C-----------------------------------------------------------------------
       DO 4 I = 1,IMDIMD                                                
         DO 2 J = 1,IMDIMD                                              
           A(I,J) = 0.0D+0                                              
    2    CONTINUE                                                       
    4  CONTINUE                                                         
C-----------------------------------------------------------------------
C      START FILLING MATRIX                                             
C-----------------------------------------------------------------------
       IND = 0                                                          
C                                                                       
       DO 40 IZ = 1,NSTAGE                                              
         IND1 = IND + NPART(IZ)                                         
C                                                                       
         DO 30 IGRD = 1,NPART(IZ)                                       
C                                                                       
           DO 20 IPRT = 1,NPART(IZ + 1)                                 
C------------------------                                               
C   FILL IONISATION RATES                                               
C------------------------                                               
             A(IND+IGRD,IND+IGRD) = A(IND+IGRD,IND+IGRD) -              
     &                              DENS(ITEM)*SCDA(ITEM,IZ,IPRT,IGRD)  
                                                                        
             A(IND1+IPRT,IND+IGRD) = A(IND1+IPRT,IND+IGRD) +            
     &                               DENS(ITEM)*SCDA(ITEM,IZ,IPRT,IGRD) 
C---------------------------                                            
C   FILL RECOMBINATION RATES                                            
C---------------------------                                            
             A(IND1+IPRT,IND1+IPRT) = A(IND1+IPRT,IND1+IPRT) -          
     &                             DENS(ITEM)*ACDA(ITEM,IZ,IPRT,IGRD) - 
     &                             DENSH(ITEM)*CCDA(ITEM,IZ,IPRT,IGRD)  
C                                                                       
             A(IND+IGRD,IND1+IPRT) = A(IND+IGRD,IND1+IPRT) +            
     &                             DENS(ITEM)*ACDA(ITEM,IZ,IPRT,IGRD) + 
     &                             DENSH(ITEM)*CCDA(ITEM,IZ,IPRT,IGRD)  
                                                                        
   20      CONTINUE                                                     
C-----------------------------                                          
C   FILL CROSS COUPLING COEFF.                                          
C-----------------------------                                          
           DO 25 JGRD = 1,NPART(IZ)                                     
             IF ( IGRD .NE. JGRD) THEN                                  
                 A(IND+JGRD,IND+IGRD) =  A(IND+JGRD,IND+IGRD)+          
     &                                DENS(ITEM)*QCDA(ITEM,IZ,IGRD,JGRD)
C                                                                       
                 A(IND+IGRD,IND+IGRD) = A(IND+IGRD,IND+IGRD)-           
     &                                DENS(ITEM)*QCDA(ITEM,IZ,IGRD,JGRD)
                 IF(IZ.GT.1) THEN                                       
                     A(IND+JGRD,IND+IGRD) =  A(IND+JGRD,IND+IGRD)+      
     &                              DENS(ITEM)*XCDA(ITEM,IZ-1,IGRD,JGRD)
C                                                                       
                     A(IND+IGRD,IND+IGRD) = A(IND+IGRD,IND+IGRD)-       
     &                              DENS(ITEM)*XCDA(ITEM,IZ-1,IGRD,JGRD)
                 ENDIF                                                  
             ENDIF                                                      
C                                                                       
   25      CONTINUE                                                     
C                                                                       
   30    CONTINUE                                                       
C                                                                       
         IND = IND1                                                     
   40  CONTINUE                                                         
C                                                                       
       RETURN                                                           
C                                                                       
C-----------------------------------------------------------------------
C                                                                       
      END                                                               
