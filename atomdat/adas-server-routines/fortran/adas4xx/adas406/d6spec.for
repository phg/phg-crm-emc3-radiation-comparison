CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6spec.for,v 1.2 2004/07/06 13:19:59 whitefor Exp $ Date $Date: 2004/07/06 13:19:59 $
CX
       SUBROUTINE D6SPEC( LRSPEC ,
     &                    NDLINE , NDCOMP  , NDRAT  , NDFILE ,
     &                    NFILE  , LFILE   ,
     &                    UID    , GROUP   , TYPE   , EXT    ,
     &                    IZ0    , DSPECA  ,
     &                    NLINE  , NCOMP   ,
     &                    IZION  , IMET    , CIMET  , INDPH  ,
     &                    IFILE  ,
     &                    NTDIM  , ITMAX  ,
     &                    TEIN   , DEIN    , THIN   , DHIN   ,
     &                    PECA   ,
     &                    LPEC   , LTRNG   , LDRNG
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6SPEC  ********************
C
C  PURPOSE: TO CALCULATE PHOTON EMISSIVITY COEFFICIENTS FOR
C           SPECTRAL LINES IDENTIFIED IN SCRIPT FILE
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)   NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C  INPUT : (I*4)   NDRAT    = MAXIMUM NUMBER OF LINE RATIOS ALLOWED
C  INPUT : (I*4)   NDFILE   = MAXIMUM NUMBER OF EMISSIVITY FILES WHICH
C                             CAN BE SEARCHED
C  INPUT : (I*4)   NFILE    = NUMBER OF PEC FILES TO BE SCANNED
C  INPUT : (L*4)   LFILE()  = .TRUE.  => PEC FILE EXISTS AND MATCHES
C                             .FALSE. => PEC FILE DOES NOT EXIST/MATCH
C  INPUT : (C*6)   UID()    = USER IDENTIFIER OF PEC FILE
C  INPUT : (C*8)   GROUP()  = GROUP IDENTIFIER OF PEC FILE
C  INPUT : (C*5)   TYPE()   = TYPE IDENTIFIER OF PEC FILE
C  INPUT : (C*3)   EXT()    = EXTENSION OF PEC FILE MEMBER NAME
C  INPUT : (I*4)   IZ0      = NUCLEAR CHARGE OF IMPURITY
C  INPUT : (C*120) DSPECA() = PHOTON EMISSIVITY SOURCE FILES
C  INPUT : (I*4)   NLINE    = NUMBER OF LINES IDENTIFIED IN SCRIPT
C  INPUT : (I*4)   NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C  INPUT : (I*4)   IZION(,) = CHARGE STATE OF COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   IMET(,)  = NUMBER OF COMPONENTS OF SCRIPT LINE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (C*1)   CIMET(,) = SIGN (+, BLANK OR -) OF METASTABLE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   INDPH(,) = PEC FILE INDEX OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   IFILE(,) = INDEX OF PEC FILE IN FILE LIST
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)   NTDIM    = MAXIMUM NUMBER OF TEMP/DENSITY SETS
C  INPUT : (I*4)   ITMAX    = NUMBER OF TEMP/DENSITY SETS
C  INPUT : (R*8)   TEIN()   = ELECTRON TEMPERATURES (EV)
C  INPUT : (R*8)   DEIN()   = ELECTRON DENSITIES (CM-3)
C  INPUT : (R*8)   THIN()   = HYDROGEN TEMPERATURES (EV)
C  INPUT : (R*8)   DHIN()   = HYDROGEN DENSITIES (CM-3)
C
C  OUTPUT: (L*4)   LRSPEC   = .TRUE.  => PEC PROCESSING DONE
C                             .FALSE. => PEC PROCESSING NOT DONE
C  OUTPUT: (R*8)   PECA(,,) = PHOTON EMISSIVITY COEFFICIENTS (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C  OUTPUT: (L*4)   LPEC(,)  = .TRUE.  => PHOTON EMISSIVITY OBTAINED
C                             .FALSE. => PHOTON EMISSIVITY NOT OBTAINED
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C
C          (I*4)   IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C          (L*4)   OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                            .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C
C ROUTINES:
C          ROUTINE    SOURCE   BRIEF DESCRIPTION
C          -----------------------------------------------------------
C	   D5SPC2   IDL-ADAS  OBTAIN PHOTON EMISSIVITY COEFFICIENT
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    20/04/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IRCODE
      INTEGER    NDLINE  , NDCOMP  , NDRAT   , NDFILE ,
     &           NLINE   , NFILE   , NTDIM   ,
     &           INDL    , INDC    , 
     &           IZ      , IZ0     , ITMAX
C-----------------------------------------------------------------------
      INTEGER    NCOMP(NDLINE)        ,
     &           IZION(NDLINE,NDCOMP) , IMET(NDLINE,NDCOMP)  ,
     &           INDPH(NDLINE,NDCOMP) , IFILE(NDLINE,NDCOMP)
C-----------------------------------------------------------------------
      REAL*8     WLNGTH
C-----------------------------------------------------------------------
      REAL*8     TEIN(NTDIM) , DEIN(NTDIM) , THIN(NTDIM) , DHIN(NTDIM) ,
     &           PECA(NTDIM,NDLINE,NDCOMP)
C-----------------------------------------------------------------------
      CHARACTER  TITLX*120
C-----------------------------------------------------------------------
      CHARACTER  CIMET(NDLINE,NDCOMP)*1 , DSPECA(NDFILE)*120, 
     &           UID(NDFILE)*6  , GROUP(NDFILE)*8, TYPE(NDFILE)*5 ,
     &           EXT(NDFILE)*3
C-----------------------------------------------------------------------
      LOGICAL    OPEN10      , LRSPEC
C-----------------------------------------------------------------------
      LOGICAL    LFILE(NDFILE) , LPEC(NDLINE,NDCOMP)
      LOGICAL    LTRNG(NTDIM)  , LDRNG(NTDIM)
C-----------------------------------------------------------------------
      DATA OPEN10 /.FALSE./
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      IF(LRSPEC) RETURN
C
C-----------------------------------------------------------------------
C      CHECK FILE FOR EACH COMPONENT
C-----------------------------------------------------------------------
C
      DO 10 INDL=1,NLINE
         DO 7 INDC=1,NCOMP(INDL)
C     
            IF(CIMET(INDL,INDC).EQ.'+') THEN
               IZ=IZION(INDL,INDC)-1
            ELSEIF(CIMET(INDL,INDC).EQ.'-')THEN
               IZ=IZION(INDL,INDC)+1
            ELSE
               IZ=IZION(INDL,INDC)
            ENDIF
C
C
C-----------------------------------------------------------------------
C UNIX PORT - HAVE REMOVED CALL TO SPEC AND REPLACED WITH NEW
C             FUNCTION D5SPC2        
C-----------------------------------------------------------------------
C
         CALL D5SPC2( DSPECA(IFILE(INDL,INDC)), INDPH(INDL,INDC) ,
     &                IZ  , IZ0  , ITMAX  , TEIN  , DEIN,
     &                WLNGTH, PECA(1,INDL,INDC), LTRNG, LDRNG,
     &                TITLX , IRCODE
     &              )

         IF(IRCODE.GT.0) THEN
             LPEC(INDL,INDC) = .FALSE.
         ELSE
             LPEC(INDL,INDC) = .TRUE.
         ENDIF
C
    7   CONTINUE
C
   10  CONTINUE
       LRSPEC = .TRUE.
C
       RETURN
C-----------------------------------------------------------------------
       END
