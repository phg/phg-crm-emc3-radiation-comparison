CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6sgcf.for,v 1.2 2004/07/06 13:19:53 whitefor Exp $ Date $Date: 2004/07/06 13:19:53 $
CX
       SUBROUTINE D6SGCF( IZ0    , IZL    , IZH    ,
     &                    ISDIMD , IZDIMD , ITDIMD , IPDIMD , IMDIMD ,
     &                    NMSUM  , IZIP   , IMIP   , IPIZM  ,
     &                    NDLINE , NDCOMP ,
     &                    NLINE  , NCOMP  , SPECL  , IPLINE ,
     &                    IZION  , IMET   , CIMET  , INDPH  , CINDPH ,
     &                    IFILE    ,
     &                    NTDIM  , ITMAX  ,
     &                    DENS   , DENSH  ,
     &                    PECA   , LPEC   ,
     &                    FPABUN ,
     &                    GCFPEQ , GCFEQ  ,
     &                    NDRAT  , NRAT   ,
     &                    ILINE  , JLINE  ,
     &                    RATA
     &                  )
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6SGCF *********************
C
C PURPOSE : TO ASSEMBLE GCF FUNCTIONS AND THEIR COMPONENTS USING
C           FRACTIONAL METASTABLE ABUNDANCES.
C
C
C
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  IZL       = MINIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  IZH       = MAXIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH
C                             IONISATION STAGE
C INPUT  : (I*4)  IMDIMD    = MAXIMUM NUMBER OF METASTABLES
C
C INPUT  : (I*4)  NMSUM     = TOTAL NUMBER OF POPULATIONS
C
C INPUT  :        IZIP()    = ION CHARGE +1 (IZ1) OF METASTABLE IN LIST
C INPUT  :        IMIP()    = METASTABLE INDEX WITHIN CHARGE STATE IZ1
C                             OF METASTABLE INDEX FROM COMPLETE LIST
C INPUT  :        IPIZM(,)  = METASTABLE INDEX IN COMPLETE LIST
C                             1ST DIM: INDEX IZ1-IZL+1
C                             2ND DIM: METASTABLE COUNT FOR STAGE (IGRD)
C INPUT  : (I*4)  NDLINE    = MAXIMUM NUMBER OF LINES ALLOWED
C INPUT  : (I*4)  NDCOMP    = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C INPUT  : (I*4)  NLINE     = NUMBER OF LINES IDENTIFIED IN SCRIPT
C INPUT  : (I*4)  NCOMP()   = NUMBER OF COMPONENTS OF SCRIPT LINE
C INPUT  : (I*4)  IZION(,)  = CHARGE STATE OF COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  IMET(,)   = METASTABLE INDEX OF COMPONENT OF
C                             SCRIPT LINE WITHIN CHARGE STATE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (C*1)  CIMET(,)  = SIGN (+, BLANK OR -) OF METASTABLE
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  INDPH(,)  = PEC FILE INDEX OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (C*1   CINDPH(,) = DRIVER (E OR BLANK => ELECTRONS)
C                                    (H          => HYDROGEN )
C                              1ST DIM: LINE INDEX
C                              2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  IFILE(,)  = INDEX OF PEC FILE IN FILE LIST
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV/DDENS PAIRS
C INPUT  : (I*4)  ITMAX     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C INPUT  : (R*8)  DENS()    = ELECTRON DENSITIES (CM-3))
C INPUT  : (R*8)  DENSH()   = HYDROGEN DENSITIES (CM-3))
C INPUT  : (R*8)  PECA(,,)  = PHOTON EMISSIVITY COEFFICIENTS (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C INPUT  : (L*4)  LPEC(,)   = .TRUE.  => PHOTON EMISSIVITY OBTAINED
C                             .FALSE. => PHOTON EMISSIVITY NOT OBTAINED
C                             2ND DIM: LINE INDEX
C                             3RD DIM: COMPONENT INDEX
C INPUT  : (R*8)  FPABUN(,) = RESOLVED METASTABLE EQUILIBRIUM
C                             FRACTIONAL ABUNDANCES
C                             1ST DIM: - TEMPERATURE/DENSITY PAIR
C                             2ND DIM: - METASTABLE INDEX
C INPUT  : (I*4)  NDRAT     = MAXIMUM NUMBER OF LINE RATIOS ALLOWED
C INPUT  : (I*4)  NRAT      = NUMBER OF RATIOS IDENTIFIED IN SCRIPT
C INPUT  : (I*4)  ILINE()   = INDEX OF NUMERATOR LINE FOR LINE RATIO
C INPUT  : (I*4)  JLINE()   = INDEX OF DENOMINATOR LINE FOR LINE RATIO
C
C OUTPUT : (C*16) SPECL(,)  = SPEC. OF POINTERS OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C OUTPUT : (I*4)  IPLINE(,) = METASTABLE POINTER OF LINE COMPONENT
C                             1ST DIM: LINE INDEX
C                             2ND DIM: COMPONENT INDEX
C OUTPUT : (R*8)  GCFPEQ(,,)= GCF FUNC. COMPONENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C                             3ND DIM: LINE COMPONENT INDEX
C OUTPUT : (R*8)  GCFEQ(,)  = GCF FUNCTION  (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: LINE INDEX
C OUTPUT : (R*8)  RATA(,)   = LINE GCF RATIOS
C                             1ST IND: TEMPERATURE INDEX
C                             2ND IND: RATIO INDEX
C
C
C PROGRAM: (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  IP        = GENERAL INDEX FOR CHARGE
C          (I*4)  IZ1       = GENERAL INDEX FOR CHARGE+1
C          (I*4)  IL        = GENERAL INDEX FOR LINE
C          (I*4)  IR        = GENERAL INDEX FOR RATIO
C          (I*4)  ICPT      = GENERAL INDEX FOR LINE COMPONENT
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR : H. P. SUMMERS, JET
C          K1/1/57
C          JET  EXT. 4941
C
C DATE   : 03/05/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
       INTEGER   IZ0
       INTEGER   IZ1     , IZL     , IZH     , NTDIM   , ITMAX   , NMSUM
       INTEGER   ISDIMD  , IZDIMD  , ITDIMD  , IPDIMD  , IMDIMD
       INTEGER   NDLINE  , NDCOMP  , NLINE   , NDRAT   , NRAT
       INTEGER   IT
       INTEGER   IL      , IR      , ICPT
C-----------------------------------------------------------------------
       INTEGER   IZIP(IMDIMD)   , IMIP(IMDIMD) ,
     &           IPIZM(IZDIMD,IPDIMD)
       INTEGER   NCOMP(NDLINE)        ,
     &           IZION(NDLINE,NDCOMP) , IMET(NDLINE,NDCOMP)  ,
     &           INDPH(NDLINE,NDCOMP) , IFILE(NDLINE,NDCOMP) ,
     &           IPLINE(NDLINE,NDCOMP)
       INTEGER   ILINE(NDRAT)         , JLINE(NDRAT)
C-----------------------------------------------------------------------
       REAL*8    DNS           , DNSH
C-----------------------------------------------------------------------
       REAL*8    DENS(NTDIM)   , DENSH(NTDIM)   ,
     &           GCFEQ(NTDIM,NDLINE)
C
       REAL*8    FPABUN(NTDIM,IMDIMD)
C
       REAL*8    PECA(NTDIM,NDLINE,NDCOMP) , GCFPEQ(NTDIM,NDLINE,NDCOMP)
C
       REAL*8    RATA(NTDIM,NDRAT)
C
C-----------------------------------------------------------------------
       CHARACTER SPECL(NDLINE,NDCOMP)*16
       CHARACTER CIMET(NDLINE,NDCOMP)*1 , CINDPH(NDLINE,NDCOMP)*1
C-----------------------------------------------------------------------
       LOGICAL   LPEC(NDLINE,NDCOMP)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  SET LINE TO METASTABLE CROSS-REFERENCING POINTER IPLINE AND POINTER
C  INFORMATION STRING SPECL
C-----------------------------------------------------------------------
C
           DO 3 IL = 1,NLINE
            DO 2 ICPT = 1,NCOMP(IL)
C
             IZ1 = IZION(IL,ICPT) + 1
             IPLINE(IL,ICPT) = IPIZM(IZ1-IZL+1,IMET(IL,ICPT))
C
             WRITE(SPECL(IL,ICPT),1000) ICPT , IZION(IL,ICPT),
     &                                  IMET(IL,ICPT),CIMET(IL,ICPT),
     &                                  INDPH(IL,ICPT),CINDPH(IL,ICPT),
     &                                  IFILE(IL,ICPT)
    2       CONTINUE
    3      CONTINUE
C
C-----------------------------------------------------------------------
C  CALCULATE GCF FUNCTIONS
C-----------------------------------------------------------------------
C
           DO 100 IT=1,ITMAX
C
            DNS  = DENS(IT)
            DNSH = DENSH(IT)
C
            DO 50 IL = 1, NLINE
C
             GCFEQ(IT,IL) = 0.0D+0
C
             DO 25 ICPT = 1 , NCOMP(IL)
C
              IF(CINDPH(IL,ICPT).EQ.'E')THEN
                  GCFPEQ(IT,IL,ICPT) = PECA(IT,IL,ICPT)*
     &                                 FPABUN(IT,IPLINE(IL,ICPT))
              ELSEIF(CINDPH(IL,ICPT).EQ.'H')THEN
                  GCFPEQ(IT,IL,ICPT) = DNSH*PECA(IT,IL,ICPT)*
     &                                 FPABUN(IT,IPLINE(IL,ICPT))/DNS
              ENDIF
C
             GCFEQ(IT,IL) = GCFEQ(IT,IL)+GCFPEQ(IT,IL,ICPT)
C
   25        CONTINUE
C
   50       CONTINUE
C
C-----------------------------------------------------------------------
C  CALCULATE LINE RATIOS
C-----------------------------------------------------------------------
C
            DO 75 IR = 1,NRAT
             IF(GCFEQ(IT,JLINE(IR)).GT. 0.0D0)THEN
                 RATA(IT,IR) = GCFEQ(IT,ILINE(IR))/GCFEQ(IT,JLINE(IR))
             ELSE
                 RATA(IT,IR) = 0.0D0
             ENDIF
   75       CONTINUE
C
  100      CONTINUE
C
       RETURN
C
C-----------------------------------------------------------------------
 1000 FORMAT(I2,2I3,1A1,I3,1A1,I3)
C-----------------------------------------------------------------------
C
       END
