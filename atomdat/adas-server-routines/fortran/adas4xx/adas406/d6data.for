CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6data.for,v 1.3 2004/07/06 13:18:19 whitefor Exp $ Date $Date: 2004/07/06 13:18:19 $
CX
       SUBROUTINE D6DATA( DSFLLA , LSELA  , LEXSA  , LDEFA , LPART  ,
     &                    IZ0    , IZ1MIN , IZ1MAX , NPART  ,
     &                    NTDIM  , ITMAX  ,
     &                    ISDIMD , IZDIMD , ITDIMD , IPDIMD , NPARTR,
     &                    DTEV   , DDENS  ,
     &                    DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                    DRCOFI ,
     &                    ACDA   , LACDA  ,
     &                    SCDA   , LSCDA  ,
     &                    CCDA   , LCCDA  ,
     &                    PRBA   , LPRBA  ,
     &                    PRCA   , LPRCA  ,
     &                    QCDA   , LQCDA  ,
     &                    XCDA   , LXCDA  ,
     &                    PLTA   , LPLTA
     &                  )
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6DATA *********************
C
C PURPOSE : TO EXTRACT A COMPLETE SET OF COLLISIONAL DIELECTRONIC DATA
C           FOR A TEMP/DENSITY MODEL
C           FROM EITHER PARTIAL (METASTABLE/PARENT RESOLVED) OR STANDARD
C           (UNRESOLVED) ISONUCLEAR MASTER FILES
C
C NOTE    : THE SOURCE DATA IS CONTAINED AS SEQUENTIAL DATASETS
C           WITH THE FOLLOWING NAMING CONVENTIONS:
C
C                   (1) JETSHP.ACD<YR>#<EL).<CODE>DATA
C                   (2) JETSHP.SCD<YR>#<EL>.<CODE>DATA
C                   (3) JETSHP.CCD<YR>#<EL>.<CODE>DATA
C                   (4) JETSHP.PRB<YR>#<EL>.<FILT>.<CODE>DATA
C                   (5) JETSHP.PRC<YR>#<EL>.<FILT>.<CODE>DATA
C                   (6) JETSHP.QCD<YR>#<EL>.<CODE>DATA
C                   (7) JETSHP.XCD<YR>#<EL>.<CODE>DATA
C                   (8) JETSHP.PLT<YR>#<EL>.<CODE>DATA
C
C       WHERE, <YR>   = TWO DIGIT YEAR NUMBER
C              <EL>   = ONE OR TWO CHARACTER ELEMENT SYMBOL
C              <CODE> = R       => PARTIAL DATA
C                       U       => PARTIAL DATA
C                       OMITTED => STANDARD DATA
C              <FILT> = SIX CHARACTER POWER FILTER CODE
C
C       AND DATA OF CLASSES 6 AND 7 DO NOT EXIST FOR THE STANDARD CASE.
C
C
C INPUT  : (C*120)DSFLLA()  = MASTER FILE DATA SET NAMES
C INPUT  : (L*4)  LSELA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        INDEX SELECTED
C                           = .FALSE. => INPUT DATA SET FOR THIS INDEX
C                                        NOT SELECTED
C INPUT  : (L*4)  LEXSA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        SELECTED INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS SELECTED INDEX
C INPUT  : (L*4)  LDEFA()   = .TRUE.  => INPUT DATA SET TYPE FOR THIS
C                                        DEFAULT YEAR INDEX EXISTS
C                           = .FALSE. => INPUT DATA SET DOES NOT EXIST
C                                        FOR THIS DEFAULT YEAR INDEX
C INPUT  : (L*4)  LPART     = .TRUE.  => PARTIAL DATA SELECTED
C                           = .FALSE. => STANDARD DATA SELECTED
C INPUT  : (I*4)  IZ0       = NUCLEAR CHARGE
C INPUT  : (I*4)  IZ1MIN    = MINIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  IZ1MAX    = MAXIMUM ION CHARGE+1 IN MASTER DATA FILES
C INPUT  : (I*4)  NPART()   = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX ON INPUT
C INPUT  : (I*4)  NTDIM     = MAXIMUM NUMBER OF DTEV/DDENS PAIRS
C INPUT  : (I*4)  ITMAX     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C INPUT  : (I*4)  ISDIMD    = MAXIMUM NUMBER OF (CHARGE, PARENT, GROUND)
C                             BLOCKS IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  IZDIMD    = MAXIMUM NUMBER OF CHARGE STATES
C                             IN ISONUCLEAR MASTER FILES
C INPUT  : (I*4)  ITDIMD    = MAXIMUM NUMBER OF TEMP OR DENS VALUES IN
C                             ISOELECTRONIC MASTER FILES
C INPUT  : (I*4)  IPDIMD    = MAXIMUM NUMBER OF METASTABLES FOR EACH
C                             IONISATION STAGE
C INPUT  : (R*8)  DTEV()    = DLOG10(ELECTRON TEMPERATURES (EV))
C INPUT  : (R*8)  DDENS()   = DLOG10(ELECTRON DENSITIES (CM-3))
C
C OUTPUT : (I*4)  NPARTR()  = METASTABLE PARTITION.  I.E. NUMBER OF
C                             METASTABLES FROM CHARGE STATE IZ1MIN-1 TO
C                             IZ1MAX FOUND IN MASTER FILE
C OUTPUT : (R*8)  DTEVD()   = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DDENSD()  = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C                             IN SELECTED MASTER FILE
C OUTPUT : (R*8)  DRCOFD(,,)= DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C                             IN SELECTED MASTER FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C                             2ND DIM: TEMPERATURE INDEX
C                             3RD DIM: DENSITY INDEX
C OUTPUT : (R*8)  ZDATA()   = CHARGE + 1 FOR IONS IN SELECTED MASTER
C                             FILE
C                             1ST DIM: (CHARGE,META,GRD) BLOCK INDEX
C OUTPUT : (R*8)  DRCOFI()  = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C OUTPUT : (R*8)  ACDA(,,,) = INTERPOLATION OF ACD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (L*4)  LACDA(,,) = .TRUE.  => ACD COEFFICIENT AVAILABLE
C                             .FALSE. => ACD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (R*8)  SCDA(,,,) = INTERPOLATION OF SCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (L*4)  LSCDA(,,) = .TRUE.  => SCD COEFFICIENT AVAILABLE
C                             .FALSE. => SCD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (R*8)  CCDA(,,,) = INTERPOLATION OF CCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C                             4TH DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (L*4)  LCCDA(,,) = .TRUE.  => CCD COEFFICIENT AVAILABLE
C                             .FALSE. => CCD COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C                             3RD DIM: RECOMBINED METASTABLE INDEX
C OUTPUT : (R*8)  PRBA(,,)  = INTERPOLATION OF PRB COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (L*4)  LCCDA(,)  = .TRUE.  => PRB COEFFICIENT AVAILABLE
C                             .FALSE. => PRB COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (R*8)  PRCA(,,)  = INTERPOLATION OF PRC COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (L*4)  LPRCA(,)  = .TRUE.  => PRC COEFFICIENT AVAILABLE
C                             .FALSE. => PRC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: RECOMBINING METASTABLE INDEX
C OUTPUT : (R*8)  QCDA(,,,) = INTERPOLATION OF QCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: FIRST METASTABLE INDEX
C                             4TH DIM: SECOND METASTABLE INDEX
C OUTPUT : (L*4)  LQCDA(,,) = .TRUE.  => QCD COEFFICIENT AVAILABLE
C                             .FALSE. => QDC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: FIRST METASTABLE INDEX
C                             3RD DIM: SECOND METASTABLE INDEX
C OUTPUT : (R*8)  XCDA(,,,) = INTERPOLATION OF XCD COEFFICIENT (CM3 S-1)
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: FIRST PARENT METASTABLE INDEX
C                             4TH DIM: SECOND PARENT METASTABLE INDEX
C OUTPUT : (L*4)  LXCDA(,,) = .TRUE.  => XCD COEFFICIENT AVAILABLE
C                             .FALSE. => XDC COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM: FIRST PARENT METASTABLE INDEX
C                             3RD DIM: SECOND PARENT METASTABLE INDEX
C OUTPUT : (R*8)  PLTA(,,)  = INTERPOLATION OF PLT COEFFICIENT (W CM3 )
C                             1ST DIM: TEMPERATURE INDEX
C                             2ND DIM: CHARGE STATE INDEX
C                             3RD DIM: METASTABLE INDEX
C OUTPUT : (L*4)  LPLTA(,)  = .TRUE.  => PLT COEFFICIENT AVAILABLE
C                             .FALSE. => PLT COEFFICIENT NOT AVAILABLE
C                             1ST DIM: CHARGE STATE INDEX
C                             2ND DIM:  METASTABLE INDEX
C
C PROGRAM: (I*4)  IT        = GENERAL INDEX FOR TEMPERATURE
C          (I*4)  IZ        = GENERAL INDEX FOR CHARGE
C          (I*4)  IZ1       = GENERAL INDEX FOR CHARGE+1
C          (I*4)  IPRT      = GENERAL INDEX FOR PARENT METASTABLE
C          (I*4)  JPRT      = GENERAL INDEX FOR PARENT METASTABLE
C          (I*4)  IGRD      = GENERAL INDEX FOR METASTABLE
C          (I*4)  JGRD      = GENERAL INDEX FOR METASTABLE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR : H. P. SUMMERS, JET
C          K1/1/57
C          JET  EXT. 4941
C
C DATE   : 25/04/94
C
C UPDATE :
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C VERSION: 1.3				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - CHANGED 'DO  2 IT = 1,ITDIMD'
C	          TO  'DO  2 IT = 1,NTDIM'
C		SO AS NOT TO GO OUTSIDE ARRAY BOUNDS
C	    - CHANGED DECISIONS TO 'IF ((LEXSA(I).OR.LDEFA(I)).AND.LSELA(I))'
C	      SEE D5DATA FOR MORE INFO.
C
C-----------------------------------------------------------------------
       INTEGER   IFAIL   , IZ0     , ICLASS
       INTEGER   IZ1     , IZ1MIN  , IZ1MAX  , NTDIM   , ITMAX
       INTEGER   ISDIMD  , IZDIMD  , ITDIMD  , IPDIMD
       INTEGER   ISMAXD  , IZMAXD  , ITMAXD  , IDMAXD
       INTEGER   IT      , IZ      , IPRT    , JPRT    , IGRD    , JGRD
C-----------------------------------------------------------------------
       INTEGER   NPART(IZDIMD)     , NPARTR(IZDIMD)
C-----------------------------------------------------------------------
       REAL*8    DTEV(ITMAX)   , DDENS(ITMAX)
       REAL*8    DTEVD(ITDIMD) , DDENSD(ITDIMD) , ZDATA(ISDIMD)
       REAL*8    DRCOFD(ISDIMD,ITDIMD,ITDIMD)
       REAL*8    DRCOFI(ITMAX)
       REAL*8    ACDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    SCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    CCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    PRBA(NTDIM,IZDIMD,IPDIMD)
       REAL*8    PRCA(NTDIM,IZDIMD,IPDIMD)
       REAL*8    QCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    XCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8    PLTA(NTDIM,IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
       CHARACTER DSFLLA(8)*120 , DSNINC*120
C-----------------------------------------------------------------------
       LOGICAL   LPART
C-----------------------------------------------------------------------
      LOGICAL     LSELA(8)      , LEXSA(8)       , LDEFA(8)
       LOGICAL   LACDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LSCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LCCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LPRBA(IZDIMD,IPDIMD)
       LOGICAL   LPRCA(IZDIMD,IPDIMD)
       LOGICAL   LQCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LXCDA(IZDIMD,IPDIMD,IPDIMD)
       LOGICAL   LPLTA(IZDIMD,IPDIMD)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C      ZERO ACD, SCD, CCD, PRB, PRC, QCD, XCD, PLT MATRICES
C      AND SET ASSOCIATED LOGICAL VARIABLES TO FALSE
C-----------------------------------------------------------------------
       DO  2 IT = 1,NTDIM
          DO 4 IZ = 1,IZDIMD
             DO 6 IPRT = 1,IPDIMD
                DO 8 IGRD = 1,IPDIMD
                   ACDA(IT,IZ,IPRT,IGRD) = 0.0D+0
                   LACDA(IZ,IPRT,IGRD)   = .FALSE.
                   SCDA(IT,IZ,IPRT,IGRD) = 0.0D+0
                   LSCDA(IZ,IPRT,IGRD)   = .FALSE.
                   CCDA(IT,IZ,IPRT,IGRD) = 0.0D+0
                   LCCDA(IZ,IPRT,IGRD)   = .FALSE.
                   QCDA(IT,IZ,IPRT,IGRD) = 0.0D+0
                   LQCDA(IZ,IPRT,IGRD)   = .FALSE.
                   XCDA(IT,IZ,IPRT,IGRD) = 0.0D+0
                   LXCDA(IZ,IPRT,IGRD)   = .FALSE.
    8           CONTINUE
                PRBA(IT,IZ,IPRT) = 0.0D+0
                LPRBA(IZ,IPRT)   = .FALSE.
                PRCA(IT,IZ,IPRT) = 0.0D+0
                LPRCA(IZ,IPRT)   = .FALSE.
                PLTA(IT,IZ,IPRT) = 0.0D+0
                LPLTA(IZ,IPRT)   = .FALSE.
    6        CONTINUE
    4     CONTINUE
    2  CONTINUE
C
C-----------------------------------------------------------------------
C      FETCH ACD, SCD, CCD, PRB, PRC DATA
C-----------------------------------------------------------------------
C
       DO 20 ICLASS = 1,5
        IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
            DSNINC = DSFLLA(ICLASS)
            DO 18 IZ1=IZ1MIN,IZ1MAX
             DO 16 IPRT = 1, NPART(IZ1-IZ1MIN+2)
              DO 14 IGRD = 1, NPART(IZ1-IZ1MIN+1)
               IFAIL = 0
               CALL DXRDNM( DSNINC , LPART  , IFAIL  ,
     &                      IZ0    , NPART  , IPRT   , IGRD  , ICLASS ,
     &                      IZ1    , ITMAX  ,
     &                      ISDIMD , IZDIMD , ITDIMD ,
     &                      ISMAXD , IZMAXD , ITMAXD , IDMAXD , NPARTR,
     &                      DTEV   , DDENS  ,
     &                      DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                      DRCOFI
     &                    )
C
              IF( IFAIL.EQ.0 ) THEN
                 IF(ICLASS.EQ.1) LACDA(IZ1-IZ1MIN+1,IPRT,IGRD) = .TRUE.
                 IF(ICLASS.EQ.2) LSCDA(IZ1-IZ1MIN+1,IPRT,IGRD) = .TRUE.
                 IF(ICLASS.EQ.3) LCCDA(IZ1-IZ1MIN+1,IPRT,IGRD) = .TRUE.
                 IF(ICLASS.EQ.4) LPRBA(IZ1-IZ1MIN+1,IPRT) = .TRUE.
                 IF(ICLASS.EQ.5) LPRCA(IZ1-IZ1MIN+1,IPRT) = .TRUE.
C
                 DO 12 IT = 1 , ITMAX
                  IF    (ICLASS .EQ. 1)THEN
                      ACDA(IT,IZ1-IZ1MIN+1,IPRT,IGRD) =
     &                10.0 ** DRCOFI(IT)
                  ELSEIF(ICLASS .EQ. 2)THEN
                      SCDA(IT,IZ1-IZ1MIN+1,IPRT,IGRD) =
     &                10.0 ** DRCOFI(IT)
                  ELSEIF(ICLASS .EQ. 3)THEN
                      CCDA(IT,IZ1-IZ1MIN+1,IPRT,IGRD) =
     &                10.0 ** DRCOFI(IT)
                  ELSEIF(ICLASS .EQ. 4)THEN
                      PRBA(IT,IZ1-IZ1MIN+1,IPRT)
     &                =PRBA(IT,IZ1-IZ1MIN+1,IPRT)+10.0**DRCOFI(IT)
                  ELSEIF(ICLASS .EQ. 5)THEN
                      PRCA(IT,IZ1-IZ1MIN+1,IPRT)
     &                =PRCA(IT,IZ1-IZ1MIN+1,IPRT)+10.0**DRCOFI(IT)
                  ENDIF
   12            CONTINUE
               ENDIF
   14         CONTINUE
   16        CONTINUE
   18       CONTINUE
        ENDIF
   20  CONTINUE
C
C-----------------------------------------------------------------------
C      FETCH METASTABLE CROSS COUPLING
C-----------------------------------------------------------------------
C
       ICLASS = 6
       IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
           DSNINC = DSFLLA(ICLASS)
           DO 30 IZ1=IZ1MIN,IZ1MAX
            DO 28 IGRD = 1, NPART(IZ1-IZ1MIN+1)
             DO 26 JGRD = 1, NPART(IZ1-IZ1MIN+1)
              IFAIL = 0
              CALL DXRDNM( DSNINC , LPART  , IFAIL  ,
     &                     IZ0    , NPART  , IGRD   , JGRD  , ICLASS ,
     &                     IZ1    , ITMAX  ,
     &                     ISDIMD , IZDIMD , ITDIMD ,
     &                     ISMAXD , IZMAXD , ITMAXD , IDMAXD , NPARTR,
     &                     DTEV   , DDENS  ,
     &                     DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                     DRCOFI
     &                   )
              IF( IFAIL.EQ.0 ) THEN
                  LQCDA(IZ1-IZ1MIN+1,IGRD,JGRD) = .TRUE.
                  DO 24 IT = 1 , ITMAX
                   QCDA(IT,IZ1-IZ1MIN+1,IGRD,JGRD) = 10.0 ** DRCOFI(IT)
   24             CONTINUE
              ENDIF
   26       CONTINUE
   28      CONTINUE
   30     CONTINUE
       ENDIF
C
C-----------------------------------------------------------------------
C      FETCH PARENT CROSS COUPLING DATA
C-----------------------------------------------------------------------
C
       ICLASS = 7
       IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
           DSNINC = DSFLLA(ICLASS)
           DO 40 IZ1=IZ1MIN,IZ1MAX
            DO 38 IPRT = 1, NPART(IZ1-IZ1MIN+2)
             DO 36 JPRT = 1, IPRT-1
              IFAIL = 0
              CALL DXRDNM( DSNINC , LPART  , IFAIL  ,
     &                     IZ0    , NPART  , IPRT   , JPRT  , ICLASS ,
     &                     IZ1    , ITMAX  ,
     &                     ISDIMD , IZDIMD , ITDIMD ,
     &                     ISMAXD , IZMAXD , ITMAXD , IDMAXD , NPARTR,
     &                     DTEV   , DDENS  ,
     &                     DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                     DRCOFI
     &                   )
              IF( IFAIL.EQ.0 ) THEN
                  LXCDA(IZ1-IZ1MIN+1,IPRT,JPRT) = .TRUE.
                  DO 34 IT = 1 , ITMAX
                   XCDA(IT,IZ1-IZ1MIN+1,IPRT,JPRT) = 10.0 ** DRCOFI(IT)
   34             CONTINUE
              ENDIF
   36        CONTINUE
   38       CONTINUE
   40      CONTINUE
       ENDIF
C
C-----------------------------------------------------------------------
C      FETCH LINE POWER
C-----------------------------------------------------------------------
C
       ICLASS = 8
       IF((LEXSA(ICLASS).OR.LDEFA(ICLASS)).AND.LSELA(ICLASS)) THEN
           DSNINC = DSFLLA(ICLASS)
           DO 50 IZ1=IZ1MIN,IZ1MAX
            DO 48 IGRD = 1, NPART(IZ1-IZ1MIN+1)
             IPRT  = 0
             IFAIL = 0
             CALL DXRDNM( DSNINC , LPART  , IFAIL  ,
     &                    IZ0    , NPART  , IGRD   , IPRT  , ICLASS ,
     &                    IZ1    , ITMAX  ,
     &                    ISDIMD , IZDIMD , ITDIMD ,
     &                    ISMAXD , IZMAXD , ITMAXD , IDMAXD , NPARTR,
     &                    DTEV   , DDENS  ,
     &                    DTEVD  , DDENSD , DRCOFD , ZDATA  ,
     &                    DRCOFI
     &                  )
             IF( IFAIL.EQ.0 ) THEN
                 LPLTA(IZ1-IZ1MIN+1,IGRD) = .TRUE.
                 DO 46 IT = 1 , ITMAX
                  PLTA(IT,IZ1-IZ1MIN+1,IGRD) = 10.0**DRCOFI(IT)
   46            CONTINUE
             ENDIF
   48       CONTINUE
   50      CONTINUE
       ENDIF
C
C-----------------------------------------------------------------------
C
       RETURN
C
C-----------------------------------------------------------------------
C
 1000  FORMAT('FILE = ',1A30)
C-----------------------------------------------------------------------
C
       END
