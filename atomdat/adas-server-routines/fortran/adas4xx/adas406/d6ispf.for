CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6ispf.for,v 1.2 2004/07/06 13:18:25 whitefor Exp $ Date $Date: 2004/07/06 13:18:25 $
CX
      SUBROUTINE D6ISPF( LPEND  , LSNULL ,
     &                   IMDIMD , NDTIN  , CHPOP  ,
     &                   NMSUM  , NLINE  , NGFPLN ,
     &                   CIION  , CMPTS  , CTITLE ,
     &                   TITLE  ,
     &                   IBSEL  ,
     &                   AMSR   , AMSD   ,
     &                   TIMEF  , FABUN0 ,
     &                   ITTYP  , ITVAL  ,
     &                   TEIN   , DEIN   , THIN   , DHIN  ,
     &                   LOSEL
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6ISPF *********************
C
C  PURPOSE: COMMUNICATION WITH IDL PROCESSING SCREEN FOR 'ADAS406'
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C  INPUT : (L*4)   LSNULL   = .TRUE.  => SCRIPT FILE SET TO NULL
C                             .FALSE. => SCRIPT FILE VALID
C
C  INPUT : (I*4)   IMDIMD   = MAX. NUMBER OF METASTABLES
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURE/DENSITY
C                             SETS ALLOWED.
C
C  INPUT : (C*14)  CHPOP()  = METASTABLE IDENTIFIER STRING + INDEX
C  INPUT : (I*4)   NMSUM    = NUMBER OF METASTABLES
C  INPUT : (I*4)   NLINE    = NUMBER OF EMITTING LINES
C  INPUT : (I*4)   NGFPLN   = NUMBER OF GRAPHS IN IONIS/POWER SET (=2).
C
C  INPUT : (C*2)   CIION()  = SCRIPT FILE: EMITTING ION FOR LINE
C                             DIMENSION: LINE INDEX
C  INPUT : (C*2)   CMPTS()  = SCRIPT FIL : NUMBE OF COMPONENTS FOR LINE
C                             DIMENSION: LINE INDEX
C  INPUT : (C*12)  CTITLE() = SCRIPT FILE: INFORMATION ON LINE
C                             DIMENSION: LINE INDEX
C
C  OUTPUT: (C*40)   TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)    IBSEL   = SELECTED LINE INDEX FOR ANALYSIS
C  OUTPUT: (R*8)    AMSR    = RECEIVER ATOMIC MASS NUMBER
C  OUTPUT: (R*8)    AMSD    = RECEIVER ATOMIC MASS NUMBER
C
C  OUTPUT: (R*8)    TIMEF   = INTEGRATION TIME (SEC)
C  OUTPUT: (R*8)    FABUN0()= FRACT. ABUNDANCES OF METASTABLES AT T=0
C  OUTPUT: (I*4)    ITTYP   = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C                           = 3 => INPUT TEMPERATURES IN REDUCED UNITS
C  OUTPUT: (I*4)    ITVAL   = NUMBER OF INPUT TEMP/DENSITY SETS (1->20)
C  OUTPUT: (R*8)    TEIN()  = USER ENTERED ISPF VALUES -
C                             ELECTRON TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C  OUTPUT: (R*8)    DEIN()  = USER ENTERED ISPF VALUES -
C                             ELECTRON DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C  OUTPUT: (R*8)    THIN()  = USER ENTERED ISPF VALUES -
C                             HYDROGEN TEMPERATURES (UNITS: SEE 'ITTYP')
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C  OUTPUT: (R*8)    DHIN()  = USER ENTERED ISPF VALUES -
C                             HYDROGEN DENSITIES    (UNITS: CM-3)
C                             DIMENSION: TEMPERATURE/DENSITY SET INDEX
C
C  OUTPUT: (L*4)    LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  FLUSHES UNIX PIPE
C
C AUTHOR:  H. P. SUMMERS
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    11/10/91
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I             , LOGIC         ,
     &           IMDIMD        , NDTIN         ,
     &           NMSUM         ,
     &           NLINE         , IBSEL         , ITTYP        , ITVAL  ,
     &           NGFPLN        , PIPEIN        , PIPEOU
      PARAMETER (PIPEOU = 6, PIPEIN = 5)
C-----------------------------------------------------------------------
      REAL*8     AMSR          , AMSD          , TIMEF
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , LSNULL        ,
     &           LOSEL
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      REAL*8     TEIN(NDTIN)              , DEIN(NDTIN)
      REAL*8     THIN(NDTIN)              , DHIN(NDTIN)
      REAL*8     FABUN0(IMDIMD)
C-----------------------------------------------------------------------
      CHARACTER  CIION(NLINE)*2           , CMPTS(NLINE)*2           ,
     &           CTITLE(NLINE)*12         , CHPOP(IMDIMD)*14
C***********************************************************************
C
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) NDTIN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) IMDIMD
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NMSUM
      CALL XXFLSH(PIPEOU)
      DO 4 I=1,NMSUM
         WRITE(PIPEOU,'(A)')CHPOP(I)
         CALL XXFLSH(PIPEOU)
 4    CONTINUE
      WRITE(PIPEOU,*) NLINE
      CALL XXFLSH(PIPEOU)
      DO 1, I=1, NLINE
          WRITE(PIPEOU, '(A)') CIION(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, '(A)') CMPTS(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU, '(A)') CTITLE(I)
          CALL XXFLSH(PIPEOU)
1     CONTINUE
C
C-----------------------------------------------------------------------
C     READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
          READ(PIPEIN, '(A)') TITLE
          READ(PIPEIN, *) IBSEL
          READ(PIPEIN, *) TIMEF
          DO 3 I=1,NMSUM
             READ(PIPEIN, *) FABUN0(I)
 3        CONTINUE
          READ(PIPEIN, *) ITTYP
          READ(PIPEIN, *) ITVAL
          READ(PIPEIN, *) AMSR
          READ(PIPEIN, *) AMSD
          DO 2, I=1, ITVAL
              READ(PIPEIN, *) TEIN(I)
              READ(PIPEIN, *) DEIN(I)
              READ(PIPEIN, *) THIN(I)
              READ(PIPEIN, *) DHIN(I)
2         CONTINUE
      ENDIF
 
C-----------------------------------------------------------------------

      RETURN
      END
