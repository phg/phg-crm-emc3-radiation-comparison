CX ULTRIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas406/d6otg3.for,v 1.3 2004/07/06 13:19:15 whitefor Exp $ Date $Date: 2004/07/06 13:19:15 $
CX
      SUBROUTINE D6OTG3( LGHOST , DATE  ,
     &                   IMDIMD , NTDIM , NDLINE , NDCOMP ,
     &                   ELEMT  , TITLE , GTIT1  , DSNINC ,
     &                   IZ0    , YEAR  , YEARDF ,
     &                   LGRD1  , LDEF1 ,
     &                   XMIN   , XMAX  , YMIN   , YMAX   ,
     &                   NMSUM  , ITMAX ,
     &                   TEV    , POPTIT, FABUN0 ,
     &                   TIMEF  ,
     &                   IBSEL  ,
     &                   NLINE  , NCOMP ,
     &                   TITL   , SPECL , IPLINE ,
     &                   GCFPEQ , GCFEQ
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D6OTG3 *********************
C
C  PURPOSE:  COMMUNICATES GRAPH DATA TO IDL
C
C            PROVIDES  GRAPH OF SELECTED GCF FUNCTION AND ITS COMPONENTS
C
C            PLOT IS LOG10(GCF FUNCTION ( CM3 S-1) ) VERSUS
C                    LOG10(ELECTRON TEMPERATURE (EV) )
C
C  CALLING PROGRAM: ADAS406
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST   = .TRUE.  => GHOST80 INITIALISED
C                            .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*8)  DATE     = CURRENT DATE AS 'DD/MM/YY'
C
C  INPUT : (I*4)  IMDIMD   = MAXIMUM NUMBER OF METASTABLES ALLOWED
C  INPUT : (I*4)  NTDIM    = MAXIMUM NUMBER OF DENSITIES ALLOWED
C  INPUT : (I*4)  NDLINE   = MAXIMUM NUMBER OF LINES ALLOWED
C  INPUT : (I*4)  NDCOMP   = MAXIMUM NUMBER OF COMPONENT FOR EACH LINE
C
C  INPUT : (C*2)  ELEMT    = ELEMENT SYMBOL.
C  INPUT : (C*40) TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*40) GTIT1    = ISPF ENTERED TITLE FOR GRAPH
C  INPUT : (C*80) DSNINC   = INPUT COPASE DATA SET NAME (MVS DSN)
C
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE
C  INPUT : (C*2)  YEAR     = TWO DIGIT YEAR NUMBER
C  INPUT : (C*2)  YEARDF   = TWO DIGIT DEFAULT YEAR NUMBER
C
C  INPUT : (L*4)  LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                          = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                          = .FALSE. => DO NOT USE DEFAULT SCALING
C
C  INPUT : (R*8)  XMIN     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  XMAX     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  INPUT : (R*8)  YMIN     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  INPUT : (R*8)  YMAX     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C  INPUT : (I*4)  NMSUM    = NUMBER OF METASTABLES
C  INPUT : (I*4)  ITMAX    = NUMBER OF INPUT ELECTRON DENSITIES
C
C  INPUT : (R*8)  TEV()    = ELECTRON DENSITIES (UNITS: CM-3)
C
C  INPUT : (C*10) POPTIT() = METASTABLE DESIGNATIONS
C  INPUT : (R*8)  FABUN0() = INITIAL METASTABLE POPULATION FRACTIONS
C                            1ST DIMENSION: METASTABLE INDEX
C  INPUT : (R*8)  TIMEF    = INTEGRATION TIME (SEC)
C  INPUT : (I*4)  NLINE    = NUMBER OF LINES IDENTIFIED IN SCRIPT
C  INPUT : (I*4)  NCOMP()  = NUMBER OF COMPONENTS OF SCRIPT LINE
C  INPUT : (C*12) TITL(,)  = TITLE FOR LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (C*16) SPECL(,) = SPECIFICATION OF POINTERS OF LINE CPTS.
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (I*4)  IPLINE(,)= METASTABLE POINTER OF LINE COMPONENT
C                            1ST DIM: LINE INDEX
C                            2ND DIM: COMPONENT INDEX
C  INPUT : (R*8)  GCFPEQ(,,)=GCF FUNC. COMPONENT (CM3 S-1)
C                           1ST DIM: TEMPERATURE INDEX
C                           2ND DIM: LINE INDEX
C                           3ND DIM: LINE COMPONENT INDEX
C  INPUT : (R*8)  GCFEQ()  = GCF FUNCTION  (CM3 S-1)
C
C          (I*4)  NDIM1    = PARAMETER = MAXIMUM NUMBER OF TEMP. VALUES
C                            (MUST NOT BE LESS THAN 'NTDIM')
C          (I*4)  NDIM2    = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                            (MUST NOT BE LESS THAN 'IMDIMD')
C          (I*4)  NGPIC    = PARAMETER = MAXIMUM NUMBER OF LEVEL POPUL-
C                            ATIONS TO BE DISPLAYED ON A SINGLE GRAPH.
C          (I*4)  NGLEV    = PARAMETER = MAXIMUM NUMBER OF METASTABLES
C                            WHICH CAN BE LISTED ON THE GRAPH.
C
C          (R*4)  CUTMIN   = PARAMETER = IN DEFAULT GRAPH SCALING IS
C                            THE  MINIMUM Y-VALUE THAT IS ALLOWED.
C                            (NOTE: 'CUTMIN' MUST BE > THAN 'GHZERO')
C          (R*4)  GHZERO   = PARAMETER = VALUE BELOW WHICH GHOST80 TAKES
C                            NUMBERS AS BEING ZERO = 1.0E-36
C
C          (I*4)  IT       = TEMP. INDEX NUMBER FOR ARRAY USE
C          (I*4)  IC       = LINE COMPONENT INDEX
C          (I*4)  IM       = METASTABLE INDEX NUMBER FOR ARRAY USE
C          (I*4)  IMMAX    = MINIMUM OF: NO. OF METASTABLES OR NGLEV'
C
C          (R*4)  X()      = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                             ELECTRON DENSITIES
C          (R*4)  Y(,)     = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C                             LEVEL POPULATIONS.
C                             1ST DIMENSION = ELECTRON TEMP. INDEX
C                             2ND DIMENSION = METASTABLE  INDEX
C
C          (L*4)  LPLINE() = .TRUE.  => META. REFERENCED BY A LINE CPT
C                          = .FALSE. => META. NOT REFERENCED BY LINE CPT
C
C          (C*80) ISPEC    = GRAPH TITLE (INCORPORATES 'ELEMT,IZ0').
C          (C*13) DNAME    = '       DATE: '
C          (C*13) FNAME    = 'INPUT FILE : '
C          (C*13) GNAME    = 'GRAPH TITLE: '
C          (C*23) XTIT     = X-AXIS UNITS/TITLE
C          (C*23) YTIT     = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*9)  FILE0   = 'FILE   : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
c
C          (C*1)  GRID     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC      = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK   = BLANK 3 BYTE STRING
C          (C*3)  CNAM()   = 3 BYTE STRING FOR POWER FUNCT. TOTAL NAMES
c
C          (C*40) HEAD1    = HEADING FOR METASTABLE ASSIGMENTS
C          (C*30) HEAD2    = HEADING FOR SPECTRUM LINE SPECIFICATIONS
C          (C*40) STRG1    = INDX/DESIGNATION TITLE
C          (C*30) STRG3    = COMPONENT TITLE
C          (C*30) STRG4    = COMPONENT PARAMETER TITLE
C          (C*13) STRG5    = TITLE
C          (C*13) STRG6    = SELECT NO.
C          (C*13) STRG7    = COMPONENTS
C
C          (L*4)  LGTXT    = .TRUE.  => LAST SCREEN DUMP WAS TEXT.
C                          = .FALSE. => LAST SCREEN DUMP WAS GHOST80.
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
CX          XXLIM4      ADAS      SETS UP DEFAULT X-AXIS (DENS) FOR GRAPH
CX          XXELEM      ADAS      SETS UP ELEMENT NAME AS STRING
CX          BXLIMY      ADAS      SETS UP DEFAULT Y-AXIS (POP.) FOR GRAPH
C
C NOTE: FOR DEFAULT GRAPH SCALING ONLY:
C       IF YLOW=YHIGH => NO OUTPUT DATA FOR PLOTTING
C                        THIS MEANS EITHER NO TRANSITIONS WERE SPECIFIED
C                        TO THE METASTABLE  LEVEL  (IN  WHICH  CASE  ALL
C                        VALUES ARE ZERO) OR ALL VALUES ARE  LESS  THAN
C                        'CUTMIN'.
C
C AUTHOR:  H. P. SUMMERS, JET
C          K1/1/57
C          JET EXT. 4941
C
C DATE:    28/04/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C
C VERSION: 1.3                                  DATE: 09/06/98
C MODIFIED: RICHARD MARTIN
C           - INCREASED NTDIM1 TO 30.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   NDIM1    , NDIM2    , NGPIC    , NGLEV
C-----------------------------------------------------------------------
      REAL*4    CUTMIN   , GHZERO
C-----------------------------------------------------------------------
      PARAMETER ( NDIM1= 30  , NDIM2=200        , NGPIC=7 , NGLEV = 55 )
      PARAMETER ( CUTMIN = 1.0E-20 , GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      INTEGER   IMDIMD   , NTDIM    , NDLINE   , NDCOMP
      INTEGER   NMSUM    , ITMAX    , NLINE    , IBSEL    ,
     &          IZ0
      INTEGER   IT       , IM       ,
     &          IMMAX    , IC
C-----------------------------------------------------------------------
      INTEGER   NCOMP(NDLINE)       , IPLINE(NDLINE,NDCOMP)
      INTEGER   PIPEIN   , PIPEOU
      PARAMETER(PIPEIN=5 , PIPEOU=6)
C-----------------------------------------------------------------------
      REAL*4    TIME4
C-----------------------------------------------------------------------
      REAL*8    XMIN     , XMAX     ,
     &          YMIN     , YMAX     ,
     &          TIMEF
C-----------------------------------------------------------------------
      LOGICAL   LGHOST   , LGRD1    , LDEF1    , LGTXT
C-----------------------------------------------------------------------
      LOGICAL   LPLINE(NDIM2)
C-----------------------------------------------------------------------
      CHARACTER ELEMT*2  , TITLE*40 , GTIT1*40 , DSNINC*80
      CHARACTER YEAR*2   , YEARDF*2
      CHARACTER GRID*1   , PIC*1    , C3BLNK*3 , DATE*8   ,
     &          FILE0*9  , MNMX0*9  , KEY0*9   , ADAS0*8  ,
     &          DNAME*13 , GNAME*10 ,
     &          XTIT*25  , YTIT*24  ,
     &          HEAD1*40 , HEAD2*30 ,
     &          STRG1*40 , STRG3*30 , STRG4*30 ,
     &          STRG5*13 , STRG6*13 , STRG7*13 ,
     &          ISPEC*80
C-----------------------------------------------------------------------
      CHARACTER TITL(NDLINE,NDCOMP)*12 , SPECL(NDLINE,NDCOMP)*16     ,
     &          CNAM(1)*3
C-----------------------------------------------------------------------
      REAL*4    X(NDIM1)            , Y(NDIM1,NDIM2)      , Z(NDIM1,1)
      REAL*4    YMAG(NDIM1,NDIM2)   , ZMAG(NDIM1,1)
C-----------------------------------------------------------------------
      CHARACTER POPTIT(IMDIMD)*10   , SY0(NDIM2)*10
      CHARACTER KEY(3)*22
C-----------------------------------------------------------------------
      REAL*8    TEV(NTDIM)
      REAL*8    GCFPEQ(NTDIM,NDLINE,NDCOMP) ,
     &          GCFEQ(NTDIM,NDLINE)
      REAL*8    FABUN0(IMDIMD)
C-----------------------------------------------------------------------
      DATA ISPEC(1:40)
     &           /'CONTRIB. FUNCT. VS ELECTRON TEMPERATURE:'/
      DATA XTIT  /'ELECTRON TEMPERATURE (eV)'/
      DATA YTIT  /'CONTRIB. FUNC. (cm3)    '/
      DATA ADAS0 /'ADAS   :'/                ,
     &     FILE0 /'FILE   : '/               ,
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(FULL LINE - TOTAL   )'/  ,
     &     KEY(2)/' (DASH LINE - PARTIAL)'/  ,
     &     KEY(3)/'                    ) '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/    ,
     &     C3BLNK/'   '/
      DATA DNAME /'       DATE: '/,
     &     GNAME /'SPECIES : '/
      DATA HEAD1 /'--------- METASTABLE ASSIGMENTS --------'/,
     &     HEAD2 /'--SPECTRUM LINE SPECIFICATION-'/
      DATA STRG1 /'INDX        DESIGNATION      INIT. FRAC.'/,
     &     STRG3 /'COMPONENT PARAMETERS          '/,
     &     STRG4 /'IC IZ IM  IP  IF  INDX        '/,
     &     STRG5 /'TITLE      = '/,
     &     STRG6 /'SELECT NO. = '/,
     &     STRG7 /'COMPONENTS = '/
C-----------------------------------------------------------------------
      DATA CNAM(1) / 'TOT' /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LGTXT = .TRUE.
      IMMAX = MIN0( NGLEV , NMSUM )
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
      ISPEC(41:80) = TITLE
C
C      CALL XXELEM(IZ0,ENAME)
C      WRITE(GTIT1,1000) ENAME,YEAR,YEARDF
C
C-----------------------------------------------------------------------
C ZERO LOGIC VECTOR FOR REFERENCED METASTABLES
C-----------------------------------------------------------------------
C
        DO 1 IM = 1, NMSUM
         LPLINE(IM) = .FALSE.
    1   CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT TIMEF AND TEMPERATURE VALUES TO REAL*4
C-----------------------------------------------------------------------
C
         TIME4 = TIMEF
C
         DO 2 IT=1,ITMAX
            X(IT)=REAL(TEV(IT))
    2    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT SELECTED GCFEQ FUNCTION TO REAL*4
C-----------------------------------------------------------------------
C
         DO 3 IT=1,ITMAX
            Z(IT,1)=GCFEQ(IT,IBSEL)
            IF (ABS(Z(IT,1)).LT.GHZERO)
     &          Z(IT,1)=SIGN(GHZERO,Z(IT,1))
            ZMAG(IT,1)=ABS(Z(IT,1))
    3    CONTINUE
C
C-----------------------------------------------------------------------
C CONVERT COMPONENTS OF SELECTED GCF FUNCTION AND FABUN0 TO REAL*4
C-----------------------------------------------------------------------
C
         DO 5 IC=1,NCOMP(IBSEL)
            LPLINE(IPLINE(IBSEL,IC)) = .TRUE.
            DO 4 IT=1,ITMAX
               Y(IT,IC)=GCFPEQ(IT,IBSEL,IC)
               IF (ABS(Y(IT,IM)).LT.GHZERO)
     &             Y(IT,IM)=SIGN(GHZERO,Y(IT,IM))
               YMAG(IT,IM)=ABS(Y(IT,IM))
    4       CONTINUE
    5    CONTINUE
C
         DO 6 IM = 1, NMSUM
           WRITE(SY0(IM),'(1P,E10.2)')FABUN0(IM)
    6    CONTINUE
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU, *) ITMAX
      CALL XXFLSH(PIPEOU)
      DO 200, IT=1, ITMAX
          WRITE(PIPEOU, *) X(IT)
          CALL XXFLSH(PIPEOU)
200   CONTINUE
      WRITE(PIPEOU, *) NCOMP(IBSEL)
      CALL XXFLSH(PIPEOU)
      DO 201, IC=1, NCOMP(IBSEL)
          DO 202, IT=1, ITMAX
              WRITE(PIPEOU,*) Y(IT,IC)
              CALL XXFLSH(PIPEOU)
202       CONTINUE
201   CONTINUE
      WRITE(PIPEOU,*) NMSUM
      CALL XXFLSH(PIPEOU)
      DO 203, IC=1, NMSUM
          WRITE(PIPEOU,'(A)') POPTIT(IC)
          CALL XXFLSH(PIPEOU)
203   CONTINUE
      WRITE(PIPEOU,*) IZ0
      CALL XXFLSH(PIPEOU)
      DO 204, IC=1, NCOMP(IBSEL)
          WRITE(PIPEOU,*) SPECL(IBSEL,IC)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*) IPLINE(IBSEL,IC)
          CALL XXFLSH(PIPEOU)
204   CONTINUE
      DO 205, IT=1, ITMAX
          WRITE(PIPEOU,*) Z(IT,1)
          CALL XXFLSH(PIPEOU)
205   CONTINUE
      WRITE(PIPEOU,*)TIME4
      CALL XXFLSH(PIPEOU)
      DO 206 IM=1,NMSUM
         WRITE(PIPEOU,*)SY0(IM)
         CALL XXFLSH(PIPEOU)
 206  CONTINUE

C
C-----------------------------------------------------------------------
C
      RETURN
      END
