CX ULTRIX PORT - SCCS info: Module @(#)d6mpop.for	1.3 Date 09/13/96
CX
       SUBROUTINE D6MPOP( NTDIM  , IZDIMD , IPDIMD , IMDIMD ,
     &                    NSTAGE , ITMAX  , NPRT   , NMSUM  ,
     &                    ACDA   , SCDA   , CCDA   , QCDA   , XCDA   ,
     &                    DENS   , DENSH  ,
     &                    FABUN0 ,
     &                    ITEM   , TIMEF  ,
     &                    POPE   , POPF   , PINTE  , PINTF
     &                   )
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN 77 SUBROUTINE: D6MPOP ********************
C
C  PURPOSE: CALCULATION OF METASTABLE RESOLVED IONISATION STAGE
C           POPULATIONS OF A PARTICULAR ELEMENT FOR A GIVEN TEMPERATURE
C           AND DENSITY
C
C  CALLING PROGRAM: ADAS405
C
C  SUBROUTINE:
C
C
C INPUT :(I*4) NTDIM         = MAXIMUM NUMBER OF MODEL TEMPS/DENSITIES
C INPUT :(I*4) IZDIMD        = MAXIMUM NUMBER OF STAGES-1
C INPUT :(I*4) IPDIMD        = MAXIMUM SIZE OF METASTABLES FRO A STAGE
C INPUT :(I*4) IMDIMD        = MAXIMUM NUMBER OF POPULATIONS
C INPUT :(I*4) NSTAGE        = NUMBER OF STAGES-1
C INPUT :(I*4) ITMAX         = NUMBER OF MODEL TEMPS/DENSITIES
C INPUT :(R*8) NPRT( )       = PARTITION OF TOTAL METASTABLES ACCORDING
C                              TO IONISATION STAGES
C                              1ST DIM: STAGE INDEX
C INPUT :(I*4) NMSUM         = TOTAL NUMBER OF POPULATIONS
C INPUT :(R*8) ACDA(,,,)     = GENERALISED CR RECOMBINATION COEFFICIENT
C                              1ST DIM: TEMPERATURE INDEX
C                              2ND DIM: STAGE INDEX (LESS 1)
C                              3RD DIM: METASTABLE INDEX
C                              4TH DIM: METASTABLE INDEX
C INPUT :(R*8) SCDA(,,,)     = GENERALISED CR IONISATION COEFFICIENT
C                              1ST DIM: TEMPERATURE INDEX
C                              2ND DIM: STAGE INDEX (LESS 1)
C                              3RD DIM: METASTABLE INDEX
C                              4TH DIM: METASTABLE INDEX
C INPUT :(R*8) CCDA(,,,)     = GENERALISED CR CHARGE EXCH. COEFFICIENT
C                              1ST DIM: TEMPERATURE INDEX
C                              2ND DIM: STAGE INDEX (LESS 1)
C                              3RD DIM: METASTABLE INDEX
C                              4TH DIM: METASTABLE INDEX
C INPUT :(R*8) QCDA(,,,)     = GENERALISED CR CROSS-COUPL. COEFFICIENT
C                              1ST DIM: TEMPERATURE INDEX
C                              2ND DIM: STAGE INDEX (LESS 1)
C                              3RD DIM: METASTABLE INDEX
C                              4TH DIM: METASTABLE INDEX
C INPUT :(R*8) XCDA(,,,)     = GENERALISED CR PARENT X-CP. COEFFICIENT
C                              1ST DIM: TEMPERATURE INDEX
C                              2ND DIM: STAGE INDEX (LESS 1)
C                              3RD DIM: METASTABLE INDEX
C                              4TH DIM: METASTABLE INDEX
C INPUT :(R*8) DENS()        = ELECTRON DENSITIES FOR MODEL
C                              1ST DIM: TEMPERATURE INDEX
C INPUT :(R*8) DENSH()       = NEUTRAL HYDROGEN DENSITIES FOR MODEL
C                              1ST DIM: TEMPERATURE INDEX
C
C INPUT :(R*8) FABUN0()      = INITIAL POPULATION ABUNDANCES AT TIME = 0
C                              1ST DIM: POPULATION INDEX
C
C INPUT :(I*4) ITEM          = CURRENT TEMP/DENSITY INDEX
C INPUT :(R*8) TIMEF         = INTEGRATION TIME (SEC)
C
C OUTPUT:(R*8) POPE()        = IONISATION BALANCE POPULATIONS
C OUTPUT:(R*8) POPF()        = POPULATIONS AT T = TIMEF
C OUTPUT:(R*8) PINTE()       = POPULATION EXCESS INTEGRAL TO T = INFIN.
C OUTPUT:(R*8) PINTF()       = POPULATION EXCESS INTEGRAL TO T = TIMEF
C
C        (I*4) ISTATE        = STAGE INDEX
C        (I*4) ITEM          = GENERAL INDEX
C        (I*4) I             = GENERAL INDEX
C        (I*4) J             = GENERAL INDEX
C        (I*4) K             = GENERAL INDEX
C        (R*8) FV1()         = WORK ARRAY USED BY XXEIGN
C        (I*4) IV1()         = WORK ARRAY USED BY XXEIGN
C
C  ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ----------------------------------------------------------
C           D6MFLL     ADAS      FILL UP IONIS./RECOM. MATRIX
CX           F02AGF     NAG       GENERAL MATRIX DIAGONALISATION
CX           F02ATF     NAG       SIMULTANEOUS EQUATION SOLUTION
C           XXEIGN     ADAS/NETLIB GENERAL MATRIX DIAGONALISATION
C           XXSIM      ADAS/NETLIB SIMULTANEOUS EQUATION SOLUTION
C
C
C  AUTHOR:  H. P. SUMMERS, JET
C           K1/1/57
C           JET EXT. 4941
C
C  DATE:    27/06/94
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    07/06/96
C
C VERSION: 1.1				     DATE:07/06/96
C MODIFIED: WILLIAM OSBORN
C	    - FIRST VERSION
C VERSION: 1.2				     DATE:27/06/96
C MODIFIED: WILLIAM OSBORN
C	    - REMOVED UNUSED VARIABLES
C VERSION: 1.3				     DATE:13/09/96
C MODIFIED: WILLIAM OSBORN
C	    - CHANGED PARAMETERS OF XXSIM IN LINE WITH CHANGES FOR
C             NON-SQUARE MATRICES.
C		
C VERSION: 1.4				DATE: 13/05/2004
C MODIFIED: Martin O'Mullane
C		- Increased NMDIM to 83 to accommodate lead.
C
C VERSION : 1.5				
C DATE    : 24/03/2011
C MODIFIED: Martin O'Mullane
C		- React to return code from xxsim.
C
C-----------------------------------------------------------------------
       INTEGER NMDIM  , NTIME
C-----------------------------------------------------------------------
       PARAMETER ( NMDIM = 84 , NTIME = 11 )
C-----------------------------------------------------------------------
       INTEGER NTDIM  , IZDIMD , IPDIMD , IMDIMD
       INTEGER ITEM   , NMSUM
       INTEGER NSTAGE , ITMAX
       INTEGER I      , I0     , I1     , IAA    , IFAIL  ,
     &         IP     , IV     , IVI    , IVR    ,
     &         J
C-----------------------------------------------------------------------
       REAL*8 AMAXM   , EIVAL  , HMEAN  , R0     , R1     ,
     &        TIME    , TIMEF  , TINT
C-----------------------------------------------------------------------
       INTEGER NPRT(IZDIMD)
C-----------------------------------------------------------------------
       REAL*8 ACDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8 SCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8 QCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8 XCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
       REAL*8 CCDA(NTDIM,IZDIMD,IPDIMD,IPDIMD)
C
       REAL*8 DENS(NTDIM)      , DENSH(NTDIM)
C
       REAL*8 FABUN0(IMDIMD)   , POPE(IMDIMD)     , POPF(IMDIMD)
       REAL*8 PINTE(IMDIMD)    , PINTF(IMDIMD)
C
       REAL*8 RHS(NMDIM)       , RR(NMDIM)        , RI(NMDIM)
       REAL*8 A(NMDIM,NMDIM)   , WKS(NMDIM*(NMDIM+2))
       REAL*8 VR(NMDIM,NMDIM)  , VI(NMDIM,NMDIM)
       REAL*8 C(NMDIM)         , ERR(NMDIM)
       REAL*8 POP(NTIME,NMDIM)
C-----------------------------------------------------------------------
       REAL*8 FV1(NMDIM)
       INTEGER IV1(NMDIM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ON FIRST ENTRY: MAKE SURE SET ARRAY BOUNDS ARE VALID
C-----------------------------------------------------------------------
C
            IF (NMDIM.LT.IMDIMD) STOP
     &                   ' D6MPOP ERROR: ARRAY DIMENSION NMDIM < IMDIMD'
C-----------------------------------------------------------------------
C  SET INITIAL CONDITION IN RHS
C-----------------------------------------------------------------------
         DO 10 I=1,IMDIMD
           RHS(I)=FABUN0(I)
   10    CONTINUE
C-----------------------------------------------------------------------
C  FILL NON-ZERO ELEMENTS OF MATRIX FROM STACK
C-----------------------------------------------------------------------
C
             CALL D6MFLL ( NTDIM  , IZDIMD , IPDIMD , IMDIMD ,
     &                     NMDIM  ,
     &                     NSTAGE , NPRT   ,
     &                     ACDA   , SCDA   , CCDA   , QCDA   , XCDA   ,
     &                     DENS   , DENSH  ,
     &                     ITEM   ,
     &                     A
     &                   )
C
C-----------------------------------------------------------------------
C  ZERO VERY SMALL ELEMENTS - TO PREVENT PROBLEMS WITH NETLIB ROUTINES.
CX  FIND THE HARMONIC MEAN AS AN APPROPRIATE TIMESCALE.
C-----------------------------------------------------------------------
       AMAXM=0.0D0
C
       DO 16 I=1,NMSUM
         DO 15 J=1,NMSUM
           AMAXM=DMAX1(AMAXM,DABS(A(I,J)))
   15    CONTINUE
   16  CONTINUE
C
CX       HMEAN=0.0D0
C
       DO 18 I=1,NMSUM
CX         HMEAN=HMEAN+1.0D0/DABS(A(I,I))
         DO 17 J=1,NMSUM
           IF(DABS(A(I,J)).LE.1.0D-40*AMAXM) A(I,J)=0.0D0
   17    CONTINUE
   18  CONTINUE
C-----------------------------------------------------------------------
C  FIND EIGENVALUES AND EIGENVECTORS
C-----------------------------------------------------------------------
       IFAIL=1
C
       IVR=NMDIM
       IVI=NMDIM
C
C-----------------------------------------------------------------------
C NAG ROUTINE REPLACED BY ADAS ROUTINE XXEIGN
C-----------------------------------------------------------------------
C       CALL F02AGF( A      , NMDIM  , NMSUM  ,
C     &              RR     , RI     , VR     , IVR   , VI    , IVI   ,
C     &              INTGER , IFAIL
C     &            )
	CALL XXEIGN( A      , NMDIM  , NMSUM  ,
     &               RR     , RI     , VR     , VI    ,
     &               IV1    , FV1    , IFAIL
     &            )
C
C-----------------------------------------------------------------------
C  SOLVE SIMULTANEOUS EQUATIONS WITH STARTING CONDITION
C-----------------------------------------------------------------------
C
       IFAIL=1
C
       IAA=NMDIM
C
C-----------------------------------------------------------------------
C NAG ROUTINE REPLACED BY ADAS ROUTINE XXSIM
C-----------------------------------------------------------------------
C       CALL F04ATF( VR    , IVR    , RHS    , NMSUM  ,
C     &              C     , AA     , IAA    , WKS1   ,WKS2   ,
C     &              IFAIL
C     &            )
       CALL XXSIM(VR,IVR,NMSUM,RHS,NMSUM,C,WKS,ERR,IFAIL)
       
       if (.not.(ifail.EQ.0.OR.ifail.EQ.4)) then
          write(0,*)'Probable error in equation solution'
          write(0,*)'LSQR error code : ', ifail
       endif

C
C-----------------------------------------------------------------------
C  FIND THE EQUILIBRIUM SOLUTION
C-----------------------------------------------------------------------
C
       R0=1.0D70
C
       DO 21 I=1,NMSUM
         IF(DABS(RR(I)).LT.R0)THEN
             I0=I
             R0=DABS(RR(I))
         ENDIF
   21  CONTINUE
C
       R1=1.0D70
C
       DO 22 I=1,NMSUM
         POPE(I)=VR(I,I0)*C(I0)
         IF(DABS(RR(I)).LT.R1.AND.I.NE.I0)THEN
             I1=I
             R1=DABS(RR(I))
         ENDIF
   22  CONTINUE
C
C-----------------------------------------------------------------------
C  EVALUATE TIME DEPENDENT SOLUTION AT NTIME TIME POINTS FROM 0 TO TIMEF
C-----------------------------------------------------------------------
C
       TINT = TIMEF/(NTIME-1)
       TIME = -TINT
C
       DO 40 I=1,NTIME
         TIME=TIME+TINT
         DO 35 IP=1,NMSUM
           POP(I,IP)=0.0D0
           DO 30 IV=1,NMSUM
             POP(I,IP)=POP(I,IP)+C(IV)*VR(IP,IV)*DEXP(RR(IV)*TIME)
   30      CONTINUE
   35   CONTINUE
   40  CONTINUE
C
       DO 45 IP=1,NMSUM
         POPF(IP) = POP(NTIME,IP)
   45  CONTINUE
C
C-----------------------------------------------------------------------
C  EVALUATE TIME INTEGRALS OF POPULATION DIFFERENCE FROM EQUILIBRIUM
C  TO T = TIMEF (PINTF) AND TO T = INFINITY (PINTE)
C-----------------------------------------------------------------------
C
       DO 60 IP=1,NMSUM
         PINTE(IP)=0.0D0
         PINTF(IP)=0.0D0
         DO 50 IV=1,NMSUM
            IF(IV.NE.I0)THEN
               EIVAL  =  C(IV)*VR(IP,IV)/RR(IV)
               PINTE(IP)=PINTE(IP)-EIVAL
               PINTF(IP) = PINTF(IP) +
     &              EIVAL*(DEXP(RR(IV)*TIMEF)-1.0)
            ENDIF
 50      CONTINUE
 60    CONTINUE
C
C-----------------------------------------------------------------------
C
       RETURN
C-----------------------------------------------------------------------
       END
