CX UNIX PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/cnv404a.for,v 1.5 2007/01/05 18:29:15 mog Exp $ DATE $Date: 2007/01/05 18:29:15 $
CX
       SUBROUTINE CNV404A(ITYPE  , ISWIT  ,
     &                    NUTMAX , NUDMAX , NUZMAX , NUMMAX ,
     &                    MAXT   , MAXD   ,
     &                    IZL    , IZH    , IZ0    ,
     &                    TEK    , DENSA  ,
     &                    NGRD   ,
     &                    IST2   , IST5   , IWRITE , DATE,
     &                    DSNIN  , OPEN17)
       IMPLICIT NONE
C
C----------------------------------------------------------------------
C
C ************ FORTRAN 77 SUBROUTINE BND404A ***************************
C
C   VERSION 1.0
C
C   PURPOSE:
C           TO FETCH DATA FROM RESOLVED ADF10 FILES, SPLINE THEM
C           ONTO THE REQUESTED TEMPERATURE/DENSITY GRID, AND
C           WRITE THE RESULT TO ADF11 FILES.
C
C   CALLING ROUTINE / PROGRAM : LH404RR / ADAS404
C
C   DATA:
C
C           THE SOURCE DATA IS CONTAINED AS MEMBERS OF PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             6. JETUID.QCD<YR>.DATA
C             7. JETUID.XCD<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C
C   SUBROUTINE:
C
C   INPUT : (I*4)  ITYPE  - TYPE OF ADF10 DATA BEING READ (SEE ABOVE)
C   INPUT : (I*4)  ISWIT  - SWITCH FOR INTERPOLATION TYPE
C   INPUT : (I*4)  NUTMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF TEMPERATURES
C   INPUT : (I*4)  NUDMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF DENSITIES
C   INPUT : (I*4)  NUZMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF CHARGE STATES
C   INPUT : (I*4)  NUMMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF METASTABLES
C   INPUT : (I*4)  MAXT   - OUTPUT ELEMENT MASTER FILE
C                                 ACTUAL NUMBER OF TEMPERATURES
C   INPUT : (I*4)  MAXD   - OUTPUT ELEMENT MASTER FILE
C                                 ACTUAL NUMBER OF DENSITIES
CX   INPUT : (C*()) DSNAME - ROOT NAME OF MASTER CONDENSED FILE
CX                           TO BE OPENED
C   INPUT : (I*4)  IZL    - LOWEST ION CHARGE TO READ
C   INPUT : (I*4)  IZH    - HIGHEST ION CHARGE TO READ
C   INPUT : (I*4)  IZ0    - NUCLEAR CHARGE TO READ
C   INPUT : (R*8)  DENSA()- OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXD DENSITIES
C   INPUT : (R*8)  TEK()  - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXT TEMPERATURES
C   INPUT : (I*4)  NGRD() - NUMBER OF GROUND STATES OF THE FIRST
C                           50 ISOELECTRONIC SEQUENCES
C   INPUT : (I*4)  IST2   - UNIT NUMBER FOR OUTPUT INFORMATION
C                           AND ERROR MESSAGES
C   INPUT : (I*4)  IST5   - UNIT NUMBER FOR READING MASTER CONDENSED
C                           FILE
C   INPUT : (I*4)  IWRITE - UNIT NUMBER FOR WRITING ADF11 DATA
C   INPUT : (C*8)  DATE   - CURRENT DATE
C
C   PARAMETER : (I*4)  NTDMAX - SIZE OF LOCAL WORKING SPACE
C                                 (MUST BE GREATER THAN NUTMAX & NUDMAX)
C   PARAMETER : (I*4)  NDZ1V  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF CHARGE STATES
C   PARAMETER : (I*4)  NDTIN  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF TEMPERATURES
C   PARAMETER : (I*4)  NDDEN  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF DENSITIES
C
C         : (R*8)  DENSR()    - INPUT MASTER CONDENSED FILE
C                                 SET OF IDE REDUCED DENSITIES
C         : (R*8)  TR()       - INPUT MASTER CONDENSED FILE
C                                 SET OF ITE REDUCED TEMPERATURES
C         : (R*8)  ZIPT()     - INPUT MASTER CONDENSED FILE
C                                 SET OF IZE RECOMBINING ION CHARGES
C         : (R*8)  AIPT(,,)   - INPUT MASTER CONDENSED FILE
C                                 RELEVANT RATE COEFFICIENTS
C                                 1ST DIMENSION - DENSITY INDEX
C                                 2ND DIMENSION - TEMPERATURE INDEX
C                                 3RD DIMENSION - CHARGE STATE INDEX
C         : (R*8)  EIA()      - INPUT MASTER CONDENSED FILE
C                                 SET OF IONISATION POTENTIALS (CM-1)
C
C         : (R*8)  ATTY(,)    - WORK SPACE FOR INTERPOLATION
C                             - STORES LOG10(INTERPOLATED VALUES)
C                                 1ST DIMENSION - TEMPERATURE
C                                 2ND DIMENSION - DENSITY
C         : (R*8)  ARRAY(,)   - STORES LOG10(INTERPOLATED VALUES)
C                                 1ST DIMENSION - TEMPERATURE
C                                 2ND DIMENSION - DENSITY
C
C     ROUTINES:
C     ---------
C         XXOPEN   -
C         XXTERM   -
C         XXIN17   -  FETCH DATA FROM MASTER CONDENSED FILE
C         D4SPLN   -  INTERPOLATE CONDENSED MASTER FILE
C                     UPDATED VERSION OF D1SPLN
C
C---------------------------------------------
C AUTHOR:  LORNE D. HORTON
C          ROOM K1/1/58, JET JOINT UNDERTAKING
C
C   DATE:   5TH AUGUST 1996
C----------------------------------------------------------------------
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C VERSION: 1.2				DATE: 20-10-97
C MODIFIED: LORNE HORTON (JET)
C		- INCREASED SPACE FOR FILE NAME DIAGNOSTICS
C
C VERSION: 1.3				DATE: 13-10-99
C MODIFIED: MARTIN O'MULLANE
C		- PRB DEFINITION HAS BEEN CHANGED AND THEY ARE NOW
C                 SUMMED OVER THE PARENTS. THIS NECESSITATES WRITING
C                 DIFFERENT INFORMATION ON THE HEADER LINE OF THE 
C                 ADF11 FILE.
C
C VERSION: 1.4				DATE: 13-10-99
C MODIFIED: MARTIN O'MULLANE
C		- DO NOT WRITE QCD AND XCD BLOCKS WHICH ARE ALL ZERO.
C 
C VERSION :  1.5                         
C DATE    : 04-01-2007
C MODIFIED: Martin O'Mullane
C           - When converting from ergs to Joules check for zero value
C             before subtracting (it's a log!) 7. In this case set 
C             value of element to -74.0. 
C
C----------------------------------------------------------------------
C
       INTEGER   ITYPE, ISWIT, I4UNIT
       INTEGER   NUTMAX, NUDMAX, NUZMAX, NUMMAX
       INTEGER   MAXT, MAXD
       INTEGER   IZL, IZH, IZ0
       INTEGER   NGRD(50), IST2, IST5, IWRITE
       REAL*8    TEK(NUTMAX), DENSA(NUDMAX)
       CHARACTER DSNAME*80, DATE*8
C
       INTEGER   NTDMAX, NDZ1V, NDTIN, NDDEN
       REAL*8    DMIN, DMINL
       PARAMETER (NTDMAX = 40)
       PARAMETER (NDZ1V  = 20, NDTIN  = 24, NDDEN  = 24 )
       PARAMETER (DMIN = 1.0D-74, DMINL = -74.0)
       LOGICAL   OPEN17
C----------------------------------------------------------------------
C      VARIABLES FOR DSNAME PARSING AND OPENING
C----------------------------------------------------------------------
       LOGICAL   LEXIST
       INTEGER   INDS, NELEC, LS, L1, L2
       INTEGER   IBGN, IEND
       CHARACTER SEQUA*2, XFESYM*2,DSNIN(50,10)*80
       CHARACTER STRING*132
C----------------------------------------------------------------------
C      VARIABLES FOR XXIN17
C----------------------------------------------------------------------
       LOGICAL   LERROR , LSWIT
       INTEGER   IDE, ITE, IZE, IME, NPRNT
       REAL*8    DENSR(NDDEN) ,TR(NDTIN), ZIPT(NDZ1V)
       REAL*8    EIA(50)
       REAL*8    AIPT(NDDEN,NDTIN,NDZ1V)
C----------------------------------------------------------------------
C      VARIABLES FOR D4SPLN
C----------------------------------------------------------------------
       LOGICAL   LSWITX
       LOGICAL   LZRNG(1), LDRNG(NTDMAX), LTRNG(NTDMAX)
       INTEGER   ISWITX, IZ1
       REAL*8    TUSR(NTDMAX) , DUSR(NTDMAX)
       REAL*8    ARRAY(NTDMAX, NTDMAX)
       REAL*8    ATTY(NTDMAX, NTDMAX)
C----------------------------------------------------------------------
C      MISCELLANEOUS COUNTERS, ETC.
C----------------------------------------------------------------------
       INTEGER   I, IT, ID, IZ, IGRD, NGRDI, IPRT, JPRT, NPRTI
       INTEGER   NOUTER, NINNER, IOUT, IINN
       REAL*8    TOTAL
       INTEGER   ZERO, PIPEOU
       PARAMETER (PIPEOU = 6)
       DATA      ZERO/0/
C
C----------------------------------------------------------------------
C
       IF( NUTMAX .GT. NTDMAX .OR. NUDMAX .GT. NTDMAX) THEN
           WRITE(I4UNIT(-1),1020)
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE AN ERROR
C----------------------------------------------------------------------
           WRITE(PIPEOU,*)ZERO
           CALL XXTERM
       ENDIF
       IF( ITYPE .LT. 1 .OR. ITYPE .GT. 7) THEN
           WRITE(I4UNIT(-1),1030)
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE AN ERROR
C----------------------------------------------------------------------
           WRITE(PIPEOU,*)ZERO
           CALL XXTERM
       ENDIF
C----------------------------------------------------------------------
C   REDEFINE OUTPUT ARRAY VARIABLES - THIS IS JUST TO GET
C   DIMENSIONS IN CALLED SUBROUTINES CORRECT!
C----------------------------------------------------------------------
       DO IT = 1,MAXT
           TUSR(IT) = TEK(IT)
       ENDDO
       DO ID = 1,MAXD
           DUSR(ID) = DENSA(ID)
       ENDDO
C----------------------------------------------------------------------
C   CYCLE THROUGH ION CHARGE
C----------------------------------------------------------------------
       INDS = INDEX( DSNAME , ')' )
       INDS = INDS - 1
       DO 310 IZ1 = IZL, IZH
          NELEC = IZ0 - IZ1 + 1
C
          SEQUA = XFESYM( NELEC )
          LS    = 1
          IF( SEQUA(2:2) .NE. ' ') LS = 2
C
CX          DSNAME = DSNAME(1:INDS)//SEQUA(1:LS)//'##)'
          DSNAME = DSNIN(IZ1-IZL+1,ITYPE)
          IBGN = INDEX( DSNAME , '##' )
          IEND = IBGN + 1
C----------------------------------------------------------------------
C   DETERMINE NO. OF METASTABLES FOR IZ AND IZ1
C----------------------------------------------------------------------
          NGRDI = NGRD( NELEC )
          IF (NELEC.GT.1) THEN
             NPRTI = NGRD( NELEC - 1 )
          ELSE
             NPRTI = 1
          ENDIF
C----------------------------------------------------------------------
C   CYCLE THROUGH PARENT AND/OR GROUND STATES DEPENDING ON ITYPE
C----------------------------------------------------------------------
          IF (ITYPE.GE.1 .AND. ITYPE.LE.3) THEN
             NOUTER = NGRDI
             NINNER = NPRTI
          ELSE IF (ITYPE.EQ.4) THEN
             NOUTER = 1
             NINNER = NPRTI
          ELSE IF (ITYPE.EQ.5) THEN
             NOUTER = 1
             NINNER = NPRTI
          ELSE IF (ITYPE.EQ.6) THEN
             NOUTER = NGRDI
             NINNER = NGRDI
          ELSE IF (ITYPE.EQ.7) THEN
             NOUTER = NPRTI
             NINNER = NPRTI
          ENDIF
C----------------------------------------------------------------------
          DO 350 IOUT = 1, NOUTER
             DO 360 IINN = 1, NINNER
C----------------------------------------------------------------------
C   NAMING CONVENTION OF PRC AND PRB FILES IS DIFFERENT!
C   AND THEY ARE NOT GROUND LEVEL RESOLVED
C----------------------------------------------------------------------
                IF (ITYPE.EQ.4.OR.ITYPE.EQ.5) THEN
                   WRITE(DSNAME(IBGN:IEND),'(2I1)') IINN, 0
                ELSE
                   WRITE(DSNAME(IBGN:IEND),'(2I1)') IINN, IOUT
                ENDIF
C----------------------------------------------------------------------
C     ZERO OUTPUT ARRAY FOR USE WHEN INPUT FILE IS MISSING
C----------------------------------------------------------------------
                DO 34 ID=1,MAXD
                   DO 35 IT=1,MAXT
                      ARRAY(IT,ID) = 0.0
   35              CONTINUE
   34           CONTINUE
C----------------------------------------------------------------------
C   FETCH CONTENTS OF ADF10 RESOLVED FILES
C----------------------------------------------------------------------
                CALL D4OPEN(IST5, DSNAME, LEXIST)
C
                IF (LEXIST) THEN
                   IF(OPEN17)THEN
                      CALL XXSLEN(DSNAME,L1,L2)
                      STRING = DSNAME(L1:L2)//' IZ1 REQUIRED: XXX'
                      CALL XXSLEN(STRING,L1,L2)
                      WRITE(STRING(L2-2:L2),'(I3)') IZ1
                      WRITE(IST2,2010)  STRING(L1:L2)
                   ENDIF
C
                   IPRT = IINN
                   IF (ITYPE.EQ.4.OR.ITYPE.EQ.5) THEN
                      JPRT = 0
                   ELSE
                      JPRT = IOUT
                   ENDIF
                   CALL XXIN17( IST5   , ITYPE  , DSNAME , LERROR ,
     &                          NDDEN  , NDTIN  , NDZ1V  ,
     &                          IPRT   , JPRT   ,
     &                          IDE    , ITE    , IZE    ,
     &                          DENSR  , TR     , ZIPT   ,
     &                          LSWIT  , EIA    ,
     &                          AIPT
     &                        )
C-------------------------------------------------------------
C        CLOSE FILE AND TERMINATE IF ERROR
C-------------------------------------------------------------
                   IF(OPEN17)THEN
                      WRITE(IST2,2020) IZE, (ZIPT(I) , I = 1,IZE)
                   ENDIF
                   CLOSE(IST5)
                   IF( LERROR ) THEN
                      WRITE(I4UNIT(-1),1010)  DSNAME
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE AN ERROR
C----------------------------------------------------------------------
                      WRITE(PIPEOU,*)ZERO
                      CALL XXTERM
                   ENDIF
                ELSE
C-------------------------------------------------------------
C        SOME COUPLINGS MAY NOT EXIST BECAUSE THEY'RE ALWAYS
C        ZERO.  SAY SO AND MOVE ONTO THE NEXT FILE.
C-------------------------------------------------------------
                   IF(OPEN17)THEN
                      CALL XXSLEN(DSNAME,L1,L2)
                      STRING = DSNAME(L1:L2)//' NOT FOUND'
                      CALL XXSLEN(STRING,L1,L2)
                      WRITE(IST2,2030) STRING(L1:L2)
                   ENDIF
                   GOTO 340
                ENDIF
C----------------------------------------------------------------------
C     CONVERT IONIS. POTENTIALS TO RYDBERGS AND FILL IN MISSING VALUES
C----------------------------------------------------------------------
C               IF(LSWIT) CALL XXCEIA (EIA)
C
                DO I = 1,50
                   EIA(I) = EIA(I) / 109737.08
                ENDDO
C-----------------------------------------------------------------------
C     SET MINIMUM VALUES OF AIPT
C-----------------------------------------------------------------------
                DO 13 IZ=1,IZE
                   DO 14 ID=1,IDE
                      DO 15 IT=1,ITE
                         AIPT(ID,IT,IZ) =
     &                          DMAX1( AIPT(ID,IT,IZ) , DMIN)
   15                 CONTINUE
   14              CONTINUE
   13           CONTINUE
C----------------------------------------------------------------------
C     PERFORM A THREE WAY SPLINE ON THE INPUT DATA
C     IF THE ENERGY EXPONENTIALS ARE AVAILABLE, USE THEM
C     ONLY FOR THE GROUND STATE
C----------------------------------------------------------------------
                IF (IOUT .EQ. 1 .AND. LSWIT) THEN
                   ISWITX = 2
                   LSWITX = .TRUE.
                ELSE
                   ISWITX = ISWIT
                   LSWITX = .FALSE.
                ENDIF
C
                CALL D4SPLN( ISWITX , LSWITX ,
     &                       NTDMAX , NTDMAX ,
     &                       NDDEN  , NDTIN  , NDZ1V  ,
     &                       MAXT   , MAXD   ,
     &                       IDE    , ITE    , IZE    ,
     &                       DUSR   , TUSR   , IZ1    ,
     &                       DENSR  , TR     , ZIPT   ,
     &                       EIA    , AIPT   ,
     &                       LZRNG  , LDRNG  , LTRNG  ,
     &                       ATTY   , ARRAY
     &                     )
     
                TOTAL = 0.0D0
                DO IT = 1,MAXT
                  DO  ID = 1, MAXD
                    TOTAL = TOTAL + ARRAY(IT,ID)
                  END DO
                END DO
                   
C----------------------------------------------------------------------
C     WRITE TO ISONUCLEAR MASTER FILE
C     CONVERT POWERS FROM ERG/S*CM**3 TO W*CM**3
C----------------------------------------------------------------------
  340           IF (ITYPE.GE.1 .AND. ITYPE.LE.3) THEN
                   WRITE(IWRITE,1201) IINN, IOUT, IZ1, DATE
                ELSE IF (ITYPE.EQ.4) THEN
                   WRITE(IWRITE,1201) IINN,    0, IZ1, DATE
                ELSE IF (ITYPE.EQ.5) THEN
                   WRITE(IWRITE,1201) IINN,    0, IZ1, DATE
                ELSE IF (ITYPE.EQ.6) THEN
                   IF (TOTAL.NE.0.0D0) THEN
                      WRITE(IWRITE,1202) IINN, IOUT, IZ1, DATE
                   ENDIF
                ELSE IF (ITYPE.EQ.7) THEN
                   IF (TOTAL.NE.0.0D0) THEN
                      WRITE(IWRITE,1203) IINN, IOUT, IZ1, DATE
                   ENDIF
                ENDIF
                DO IT = 1,MAXT
                   IF (ITYPE.EQ.4 .OR. ITYPE.EQ.5 ) THEN 
                      do id = 1, maxd
                         if (array(it,id).ne.0.0D0) then
                             array(it,id) = array(it,id) - 7.0d0
                         else
                             array(it,id) = DMINL
                         endif
                      end do
                      WRITE(IWRITE,1210)
     &                 (DMAX1(ARRAY(IT,ID),DMINL) , ID = 1, MAXD)
                   ELSEIF (ITYPE.EQ.6 .OR. ITYPE.EQ. 7) THEN
                      IF (TOTAL.NE.0.0D0) THEN
                         WRITE(IWRITE,1210)
     &                    (DMAX1(ARRAY(IT,ID),DMINL) , ID = 1, MAXD)
                      ENDIF
                   ELSE
                      WRITE(IWRITE,1210)
     &                 (DMAX1(ARRAY(IT,ID),DMINL) , ID = 1, MAXD)
                   ENDIF
                ENDDO
                TOTAL=0.0D0
C----------------------------------------------------------------------
C    END OF PARENT/GROUND LOOP
C----------------------------------------------------------------------
  360        CONTINUE
  350     CONTINUE
C----------------------------------------------------------------------
C    END OF ION CHARGE LOOP
C----------------------------------------------------------------------
  310  CONTINUE
C
       RETURN
C
 1010  FORMAT(/' CNV404A ERROR : ',1A35)
 1020  FORMAT(/' CNV404A ERROR : NUTMAX OR NUDMAX GREATER THAN NTDMAX')
 1030  FORMAT(/' CNV404A ERROR : INVALID DATA TYPE SPECIFIER ')
C----------------------------------------------------------------------
 1201  FORMAT(21('-'),'/ IPRT=',I2,'  / IGRD=',I2,
     &     '  /--------/ Z1=',I2,'   / DATE= ',1A8)
 1202  FORMAT(21('-'),'/ IGRD=',I2,'  / JGRD=',I2,
     &     '  /--------/ Z1=',I2,'   / DATE= ',1A8)
 1203  FORMAT(21('-'),'/ IPRT=',I2,'  / JPRT=',I2,
     &     '  /--------/ Z1=',I2,'   / DATE= ',1A8)
 1210  FORMAT(8F10.5)
C----------------------------------------------------------------------
 2010  FORMAT(11X,A)
 2020  FORMAT(1H ,13X,'NO. OF IZ IN DATASET =',I3,':',10F4.0)
 2030  FORMAT(3X,5('*'),3X,A,/,3X,5('*'),3X,'==> COUPLING ASSUMED ZERO')
C
      END
