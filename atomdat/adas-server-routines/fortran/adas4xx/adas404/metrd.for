CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/metrd.for,v 1.3 2004/07/06 14:20:49 whitefor Exp $ Date $Date: 2004/07/06 14:20:49 $
CX
       SUBROUTINE METRD(NUTMAX , NUDMAX , NUZMAX , NUMMAX ,
     &                  MAXT   , MAXD   , DSNIN  ,
     &                  IZL    , IZH    , IZ0    ,
     &                  TEK    , DENSA  ,
     &                  METFRC ,
     &                  NGRD   ,
     &                  IST2   , IST5 )
       IMPLICIT NONE
C
C----------------------------------------------------------------------
C
C ************ FORTRAN 77 SUBROUTINE METRD *****************************
C
C   VERSION 1.0
C
C   PURPOSE:
C           TO FETCH DATA FROM ADF10 'MET' FILES AND SPLINE ONTO
C           THE REQUESTED TEMPERATURE/DENSITY GRID.
C
C           CALLING PROGRAM LH404RU
C
C   SUBROUTINE:
C
C   INPUT : (I*4)  NUTMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF TEMPERATURES
C   INPUT : (I*4)  NUDMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF DENSITIES
C   INPUT : (I*4)  NUZMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF CHARGE STATES
C   INPUT : (I*4)  NUMMAX - OUTPUT ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF METASTABLES
C   INPUT : (I*4)  MAXT   - OUTPUT ELEMENT MASTER FILE
C                                 ACTUAL NUMBER OF TEMPERATURES
C   INPUT : (I*4)  MAXD   - OUTPUT ELEMENT MASTER FILE
C                                 ACTUAL NUMBER OF DENSITIES
C   INPUT : (C*80) DSNIN(,) - NAMES OF MASTER CONDENSED FILES
C                             TO BE OPENED
C   INPUT : (I*4)  IZL    - LOWEST ION CHARGE TO READ
C   INPUT : (I*4)  IZH    - HIGHEST ION CHARGE TO READ
C                           ACTUALLY READ ONE MORE IF IZH<IZ0
C   INPUT : (I*4)  IZ0    - NUCLEAR CHARGE TO READ
C   INPUT : (R*8)  DENSA()- OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXD DENSITIES
C   INPUT : (R*8)  TEK()  - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXT TEMPERATURES
C   OUTPUT: (R*8)  METFRC(,,,) - METASTABLE POPULATION FRACTIONS,
C                                 SPLINED ONTO THE OUTPUT TEMPERATURES
C                                 AND DENSITIES
C                                 1ST DIMENSION - DENSITY INDEX
C                                 2ND DIMENSION - TEMPERATURE INDEX
C                                 3RD DIMENSION - CHARGE STATE INDEX
C                                 4TH DIMENSION - METASTABLE INDEX
C   INPUT : (I*4)  NGRD() - NUMBER OF GROUND STATES OF THE FIRST
C                           50 ISOELECTRONIC SEQUENCES
C   INPUT : (I*4)  IST2   - UNIT NUMBER FOR OUTPUT INFORMATION
C                           AND ERROR MESSAGES
C   INPUT : (I*4)  IST5   - UNIT NUMBER FOR READING MASTER CONDENSED
C                           FILE
C
C   PARAMETER : (I*4)  NTDMAX - SIZE OF LOCAL WORKING SPACE
C                                 (MUST BE GREATER THAN NUTMAX & NUDMAX)
C   PARAMETER : (I*4)  NDZ1V  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF CHARGE STATES
C   PARAMETER : (I*4)  NDTIN  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF TEMPERATURES
C   PARAMETER : (I*4)  NDDEN  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF DENSITIES
C   PARAMETER : (I*4)  NDMET  - MASTER CONDENSED FILE
C                                 MAXIMUM NUMBER OF METASTABLES
C
C         : (R*8)  DENSR()    - INPUT MASTER CONDENSED FILE
C                                 SET OF IDE REDUCED DENSITIES
C         : (R*8)  TR()       - INPUT MASTER CONDENSED FILE
C                                 SET OF ITE REDUCED TEMPERATURES
C         : (R*8)  ZIPT()     - INPUT MASTER CONDENSED FILE
C                                 SET OF IZE RECOMBINING ION CHARGES
C         : (R*8)  AIPTM(,,)  - INPUT MASTER CONDENSED FILE
C                                 RATIO OF METASTABLE TO GROUND POP.
C                                 1ST DIMENSION - DENSITY INDEX
C                                 2ND DIMENSION - TEMPERATURE INDEX
C                                 3RD DIMENSION - CHARGE STATE INDEX
C                                 4TH DIMENSION - METASTABLE INDEX
C         : (R*8)  EIA()      - INPUT MASTER CONDENSED FILE
C                                 SET OF IONISATION POTENTIALS (CM-1)
C
C         : (R*8)  ATTY(,)    - WORK SPACE FOR INTERPOLATION
C                             - STORES LOG10(INTERPOLATED VALUES)
C                                 1ST DIMENSION - TEMPERATURE
C                                 2ND DIMENSION - DENSITY
C         : (R*8)  ARRAY(,)   - STORES LOG10(INTERPOLATED VALUES)
C                                 1ST DIMENSION - TEMPERATURE
C                                 2ND DIMENSION - DENSITY
C
C     ROUTINES:
C     ---------
C         XXOPEN   -
C         XXTERM   -
C         XXIN80   -  FETCH DATA FROM MASTER CONDENSED FILE
C         D4SPLN   -  INTERPOLATE CONDENSED MASTER FILE
C                     UPDATED VERSION OF D1SPLN
C
C---------------------------------------------
C AUTHOR:  LORNE D. HORTON
C          ROOM K1/1/58, JET JOINT UNDERTAKING
C
C   DATE:  21ST FEBRUARY 1996
C----------------------------------------------------------------------
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C     - FIRST CONVERTED
C
C VERSION: 1.2				DATE: 20-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C     - REMOVED DIAGNOSTIC WRITE STATEMENTS
C
C VERSION: 1.3				DATE: 20-10-97
C MODIFIED: LORNE HORTON
C     - REMOVED WHITE SPACE FROM DATA FILENAME.
C
C----------------------------------------------------------------------
C
       INTEGER   NUTMAX, NUDMAX, NUZMAX, NUMMAX
       INTEGER   MAXT, MAXD
       INTEGER   IZL, IZH, IZ0
       INTEGER   NGRD(50), IST2, IST5
       REAL*8    TEK(NUTMAX), DENSA(NUDMAX)
       REAL*8    METFRC(NUDMAX,NUTMAX,NUZMAX,NUMMAX)
       CHARACTER DSNAME*80
       CHARACTER DSNIN(50,10)*80
C
       INTEGER   NTDMAX, NDZ1V, NDTIN, NDDEN, NDMET
       REAL*8    DMIN
       PARAMETER (NTDMAX = 40)
       PARAMETER (NDZ1V  = 20, NDTIN  = 24, NDDEN  = 24 , NDMET = 4)
       PARAMETER (DMIN = 1.0D-74)
C----------------------------------------------------------------------
C      VARIABLES FOR DSNAME PARSING AND OPENING
C----------------------------------------------------------------------
       LOGICAL   LEXIST
       INTEGER   INDS, NELEC, LS, L1, L2
       CHARACTER SEQUA*2, XFESYM*2
       CHARACTER STRING*132
C----------------------------------------------------------------------
C      VARIABLES FOR XXIN80
C----------------------------------------------------------------------
       LOGICAL   LERROR , LSWIT
       INTEGER   IDE, ITE, IZE, IME, NPRNT
       INTEGER   IMETR(NDMET), IPRNT(NDMET), IPSYS(NDMET)
       REAL*8    DENSR(NDDEN) ,TR(NDTIN), ZIPT(NDZ1V)
       REAL*8    EIA(50)
       REAL*8    AIPTM(NDDEN,NDTIN,NDZ1V,NDMET)
       CHARACTER CSTRGA(NDMET)*12
C----------------------------------------------------------------------
C      VARIABLES FOR D4SPLN
C----------------------------------------------------------------------
       LOGICAL   LZRNG(1), LDRNG(NTDMAX), LTRNG(NTDMAX)
       INTEGER   ISWIT, IZ1
       REAL*8    TUSR(NTDMAX) , DUSR(NTDMAX)
       REAL*8    ARRAY(NTDMAX, NTDMAX)
       REAL*8    ATTY(NTDMAX, NTDMAX)
C----------------------------------------------------------------------
C      MISCELLANEOUS COUNTERS, ETC.
C----------------------------------------------------------------------
       INTEGER   I, IT, ID, IZ, IGRD, NGRDI
       INTEGER   ZERO, PIPEOU
       PARAMETER (PIPEOU = 6)
       DATA      ZERO/0/
C
C----------------------------------------------------------------------
C
       IF( NUTMAX .GT. NTDMAX .OR. NUDMAX .GT. NTDMAX) THEN
           WRITE(IST2,1020)
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE AN ERROR
C----------------------------------------------------------------------
           WRITE(PIPEOU,*)ZERO
           CALL XXTERM
       ENDIF
C----------------------------------------------------------------------
C   REDEFINE OUTPUT ARRAY VARIABLES - THIS IS JUST TO GET
C   DIMENSIONS IN CALLED SUBROUTINES CORRECT!
C----------------------------------------------------------------------
       DO IT = 1,MAXT
           TUSR(IT) = TEK(IT)
       ENDDO
       DO ID = 1,MAXD
           DUSR(ID) = DENSA(ID)
       ENDDO
C----------------------------------------------------------------------
C   CYCLE THROUGH ION CHARGE
C----------------------------------------------------------------------
CX       INDS = INDEX( DSNAME , ')' )
CX       INDS = INDS - 1
       DO 310 IZ1 = IZL, MIN0(IZH+1,IZ0)
          NELEC = IZ0 - IZ1 + 1
C
          SEQUA = XFESYM( NELEC )
          LS    = 1
          IF( SEQUA(2:2) .NE. ' ') LS = 2
C
CX          DSNAME = DSNAME(1:INDS)//SEQUA(1:LS)//'##)'
          DSNAME = DSNIN(IZ1-IZL+1,10)
C----------------------------------------------------------------------
C   FETCH CONTENTS OF PARENT/MET RESOLVED FILES
C----------------------------------------------------------------------
          CALL XXOPEN(IST5, DSNAME, LEXIST)
C
          IF (LEXIST) THEN
             CALL XXSLEN(DSNAME,L1,L2)
             STRING = DSNAME(L1:L2)//' IZ1 REQUIRED: XXX'
             CALL XXSLEN(STRING,L1,L2)
             WRITE(STRING(L2-2:L2),'(I3)') IZ1
             WRITE(IST2,2010)  STRING(L1:L2)
C
             CALL XXIN80( IST5     , DSNAME   , LERROR ,
     &                    NDDEN    , NDTIN    , NDZ1V  , NDMET ,
     &                    IDE      , ITE      , IZE    ,
     &                    DENSR    , TR       , ZIPT   ,
     &                    IME      , IMETR    , CSTRGA ,
     &                    NPRNT    , IPRNT    , IPSYS  ,
     &                    LSWIT    , EIA      ,
     &                    AIPTM
     &                  )
C-------------------------------------------------------------
C        CLOSE FILE AND TERMINATE IF ERROR
C-------------------------------------------------------------
             WRITE(IST2,2020) IZE, (ZIPT(I) , I = 1,IZE)
             CLOSE(IST5)
             IF( LERROR ) THEN
                WRITE(IST2,1010)  DSNAME
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE AN ERROR
C----------------------------------------------------------------------
                WRITE(PIPEOU,*)ZERO
                CALL XXTERM
             ENDIF
          ELSE
             WRITE(IST2,1010)  DSNAME
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE AN ERROR
C----------------------------------------------------------------------
             WRITE(PIPEOU,*)ZERO
             CALL XXTERM
          ENDIF
C----------------------------------------------------------------------
C     CONVERT IONIS. POTENTIALS TO RYDBERGS AND FILL IN MISSING VALUES
C----------------------------------------------------------------------
C         IF(LSWIT) CALL XXCEIA (EIA)
C
          DO I = 1,50
             EIA(I) = EIA(I) / 109737.08
          ENDDO
C----------------------------------------------------------------------
C   DETERMINE NO. OF METASTABLES FOR IZ
C----------------------------------------------------------------------
          NGRDI = NGRD( NELEC )
C----------------------------------------------------------------------
C   CYCLE THROUGH METASTABLES
C----------------------------------------------------------------------
          DO 350 IGRD = 1, NGRDI
C-----------------------------------------------------------------------
C     SET MINIMUM VALUES OF AIPTM
C-----------------------------------------------------------------------
             DO 13 IZ=1,IZE
                DO 14 ID=1,IDE
                   DO 15 IT=1,ITE
                      AIPTM(ID,IT,IZ,IGRD) =
     &                          DMAX1( AIPTM(ID,IT,IZ,IGRD) , DMIN)
   15              CONTINUE
   14           CONTINUE
   13        CONTINUE
C----------------------------------------------------------------------
C     PERFORM A THREE WAY SPLINE ON THE INPUT DATA
C     FORCE ENERGY EXPONENTIALS OFF
C----------------------------------------------------------------------
             ISWIT = 10
             LSWIT = .FALSE.
C
             CALL D4SPLN( ISWIT  , LSWIT  ,
     &                    NTDMAX , NTDMAX ,
     &                    NDDEN  , NDTIN  , NDZ1V  ,
     &                    MAXT   , MAXD   ,
     &                    IDE    , ITE    , IZE    ,
     &                    DUSR   , TUSR   , IZ1    ,
     &                    DENSR  , TR     , ZIPT   ,
     &                    EIA    , AIPTM(1,1,1,IGRD),
     &                    LZRNG  , LDRNG  , LTRNG  ,
     &                    ATTY   , ARRAY
     &                  )
C----------------------------------------------------------------------
C     LOAD ARRAY INTO OUTPUT MATRIX
C----------------------------------------------------------------------
             DO 24 ID=1,MAXD
                DO 25 IT=1,MAXT
                   METFRC(ID,IT,IZ1,IGRD) = 10**ARRAY(IT,ID)
   25           CONTINUE
   24        CONTINUE
C----------------------------------------------------------------------
C    END OF METASTABLE LOOP
C----------------------------------------------------------------------
  350     CONTINUE
C----------------------------------------------------------------------
C    END OF ION CHARGE LOOP
C----------------------------------------------------------------------
  310  CONTINUE
C
       RETURN
C
 1010  FORMAT(/' METRD ERROR : ',1A80)
 1020  FORMAT(/' METRD ERROR : NUTMAX OR NUDMAX GREATER THAN NTDMAX ')
 2010  FORMAT(11X,A)
 2020  FORMAT(1H ,13X,'NO. OF IZ IN DATASET =',I3,':',10F4.0)
C
      END
