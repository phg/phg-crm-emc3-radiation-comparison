      SUBROUTINE LH404RR(DATE  , USER  , 
     &                   IZ0   , IZL   , IZH   , CHPRFIX ,
     &                   MAXT  , MAXD  , TEK   , DENSA   ,
     &                   DSNIN , DSNO  , LDTYP , YEAR    , OPEN17
     &                  )
C----------------------------------------------------------------------
C
C ************ FORTRAN 77 PROGRAM: LH404RR *****************************
C
C   VERSION 1.0
C
C   PURPOSE:
C           TO FETCH DATA FROM MASTER CONDENSED PARENT/METASTABLE
C           RESOLVED COLLISIONAL DIELECTRONIC FILES AND PREPARE
C           RESOLVED ISONUCLEAR (ADF11) MASTER FILES.
C
C           BASED UPON LH404RU
C
C   PROGRAM:
C
C   PARAMETER : (I*4)  NUTMAX - OUTPUT  ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF TEMPERATURES
C   PARAMETER : (I*4)  NUDMAX - OUTPUT  ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF DENSITIES
C   PARAMETER : (I*4)  NUZMAX - OUTPUT  ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF CHARGE STATES
C   PARAMETER : (I*4)  NUMMAX - METASTABLE FRACTIONS
C                                 MAXIMUM NUMBER OF METASTABLES
C
C       (R*8)  DENSA()        - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXD DENSITIES
C       (R*8)  TEK()          - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXT TEMPERATURES
C       (R*8)  DENSL()        - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXD DENSITIES IN LOGARITHM
C       (R*8)  TEVL()         - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXT TEMPERATURES IN LOGARITHM
C
C
C     ROUTINES:
C     ---------
C         XUFLOW   - VS FORTRAN UNDERFLOW EXCEPTION HANDLER
C         CL3270   - JET-SPECIFIC CLEAR SCREEN ROUTINE
C         XXDATE   - ADAS - GATHER CURRENT DATE
C         XXOPEN   - ADAS - OPEN FILE
C         XXSLEN   - ADAS - GET FIRST AND LAST CHAR. POS. IN A STRING
C         DMGUID   - JET-SPECIFIC - GATHER USERS ID
C         FILEINF  - VS FORTRAN FILE INFORMATION ROUTINE
C         CNV404A  - READ ACD,SCD,CCD,PRB,PRC,QCD,XCD RESOLVED
C                    ADF10 FILES AND WRITE RESOLVED ADF11 FILES
C         CNV404B  - READ PLT,PLS RESOLVED ADF10 FILES
C                    AND WRITE RESOLVED ADF11 FILES
C
C----------------------------------------------------------------------
C AUTHOR:  LORNE D. HORTON
C          ROOM K1/1/58, JET JOINT UNDERTAKING
C
C   DATE:   5TH AUGUST 1996
C
C----------------------------------------------------------------------
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C               - MADE A SUBROUTINE OF ADAS404
C
C VERSION:  1.2                         DATE: 4/11/99
C MODIFIED: Martin O'Mullane
C           - Add real name of producer.
C           - Modify comments.
C           - Write the metastable line with new D4WMET subroutine.
C           - Increase size of DSNOUT to 80 from 35.
C
C VERSION :  1.2                         
C DATE    : 23-02-2006
C MODIFIED: Martin O'Mullane
C           - Use the same extrapolation as unresolved case. ie
C             do not bypass options in the dxspl<n>.for routines.
C
C VERSION :  1.3                         
C DATE    : 04-01-2007
C MODIFIED: Martin O'Mullane
C           - F-like and Ne-like stages have 2 (not 1) metastables. 
C             Change NGRD vector.
C           - Use 11605.4 as K/eV conversion (same as xxtcon).
C
C VERSION : 1.4                         
C DATE    : 31-01-201
C MODIFIED: Martin O'Mullane
C           - Use the correct number of metastables (up to Ar-like) 
C             in the NGRD vector.
C
C----------------------------------------------------------------------

       INTEGER   NUTMAX, NUDMAX, NUZMAX, NUMMAX
       INTEGER   IST1, IST2, IST4, IST5
       REAL*8    DLOGMIN, DMIN
       PARAMETER (NUTMAX = 35, NUDMAX = 30, NUZMAX = 50, NUMMAX = 4)
       PARAMETER (IST1 = 5, IST2 = 17, IST4 = 11, IST5 = 12)
       PARAMETER (DLOGMIN = -74.0 , DMIN = 1.0D-74)
C----------------------------------------------------------------------
C      DECLARATIONS FOR INPUT DATA
C----------------------------------------------------------------------
       INTEGER   IZ0, IZL, IZH
       CHARACTER DATE*8 , YEAR*2, USERID*6, CHPRFIX*2, USER*30
       CHARACTER SYSUID*7, PREFIX*7
       LOGICAL   LPRFIX, OPEN17
C----------------------------------------------------------------------
C      VARIABLES LOADED BY DATA STATEMENTS IN THE SOURCE CODE
C----------------------------------------------------------------------
       INTEGER   NGRD(50)
       INTEGER   MAXT, MAXD
       REAL*8    TEK(NUTMAX), DENSA(NUDMAX)
       CHARACTER CDTYP(9)*3
       LOGICAL   LDTYP(9)
       CHARACTER  DSNO(10)*80 , DSNIN(50,10)*80
C----------------------------------------------------------------------
C      VARIABLES FOR BUILDING FILE NAMES
C----------------------------------------------------------------------
       INTEGER   LSE
       CHARACTER DSNOUT*80
       CHARACTER ELEMT*2, ELEMN*12, XFESYM*2, XFELEM*12
       LOGICAL   LEXIST
C----------------------------------------------------------------------
C      VARIABLES FOR WRITING TO PASS FILES
C----------------------------------------------------------------------
       INTEGER   ITYPE, ISWIT, IWRITE
       INTEGER   NELH, NELL, NEL
C----------------------------------------------------------------------
C      LOG TEMPERATURE AND DENSITY VECTORS FOR OUTPUT
C----------------------------------------------------------------------
       REAL*8    TEVL(NUTMAX), DENSL(NUDMAX)
C----------------------------------------------------------------------
C      MISCELLANEOUS COUNTERS, ETC.
C----------------------------------------------------------------------
       INTEGER   IT, ID, IZ, IGRD, IRCODE, LEN1, LEN2
       REAL*8    SUM
C
C----------------------------------------------------------------------
C
       DATA NGRD  /  1 , 2 , 1 , 2 , 2 , 4 , 3 , 4 , 2 , 2 ,
     &               1 , 2 , 2 , 4 , 3 , 4 , 2 , 2 , 1 , 1 ,
     &               1 , 1 , 3 , 2 , 1 , 1 , 1 , 1 , 1 , 1 ,
     &               1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ,
     &               3 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 /
C
       DATA CDTYP / 'ACD' , 'SCD' , 'CCD' , 'PRB' , 'PRC' ,
     &              'QCD' , 'XCD' , 'PLT' , 'PLS' /
C----------------------------------------------------------------------
C   DETERMINE WHICH DATA TYPES SHOULD BE INCLUDED IN THE CONVERSION
C   FROM ADF10 TO ADF11.  MISSING FILES WILL BE ASSUMED ZERO AND
C   WRITTEN TO ADF11 FORMAT IF LDTYP = .TRUE.
C----------------------------------------------------------------------
C   NOW READ IN FROM IDL
C----------------------------------------------------------------------
CX       DATA LDTYP / .TRUE. ,.TRUE. ,.TRUE. ,.TRUE. ,.TRUE. ,
CX     &              .TRUE. ,.TRUE. ,.TRUE. ,.TRUE. /
C----------------------------------------------------------------------
C   DATA FOR 96 FILE GENERATION
C----------------------------------------------------------------------
C      DATA MAXT / 30 /
C      DATA MAXD / 24 /
C      DATA TEK   / 2.3208D+03 , 3.4812D+03 , 5.8020D+03 , 8.1228D+03,
C    &              1.1604D+04 , 1.7406D+04 , 2.3208D+04 , 3.4812D+04,
C    &              5.8020D+04 , 8.1228D+04 , 1.1604D+05 , 1.7406D+05,
C    &              2.3208D+05 , 3.4812D+05 , 5.8020D+05 , 8.1228D+05,
C    &              1.1604D+06 , 1.7406D+06 , 2.3208D+06 , 3.4812D+06,
C    &              5.8020D+06 , 8.1228D+06 , 1.1604D+07 , 1.7406D+07,
C    &              2.3208D+07 , 3.4812D+07 , 5.8020D+07 , 8.1228D+07,
C    &              1.1604D+08 , 1.7406D+08 , 5*0.0D0/
C      DATA DENSA / 5.0000D+07 , 1.0000D+08 , 2.0000D+08 ,
C    &              5.0000D+08 , 1.0000D+09 , 2.0000D+09 ,
C    &              5.0000D+09 , 1.0000D+10 , 2.0000D+10 ,
C    &              5.0000D+10 , 1.0000D+11 , 2.0000D+11 ,
C    &              5.0000D+11 , 1.0000D+12 , 2.0000D+12 ,
C    &              5.0000D+12 , 1.0000D+13 , 2.0000D+13 ,
C    &              5.0000D+13 , 1.0000D+14 , 2.0000D+14 ,
C    &              5.0000D+14 , 1.0000D+15 , 2.0000D+15 ,
C    &              6*0.0D0/
C----------------------------------------------------------------------
C   DATA FOR 96 HELIUM STUDY - REMOVE BOTTOM 3 AND TOP TEMPERATURES
C----------------------------------------------------------------------
CX       DATA MAXT / 26 /
CX       DATA MAXD / 24 /
CX       DATA TEK   / 8.1228D+03,
CX     &              1.1604D+04 , 1.7406D+04 , 2.3208D+04 , 3.4812D+04,
CX     &              5.8020D+04 , 8.1228D+04 , 1.1604D+05 , 1.7406D+05,
CX     &              2.3208D+05 , 3.4812D+05 , 5.8020D+05 , 8.1228D+05,
CX     &              1.1604D+06 , 1.7406D+06 , 2.3208D+06 , 3.4812D+06,
CX     &              5.8020D+06 , 8.1228D+06 , 1.1604D+07 , 1.7406D+07,
CX     &              2.3208D+07 , 3.4812D+07 , 5.8020D+07 , 8.1228D+07,
CX     &              1.1604D+08 , 9*0.0D0/
CX       DATA DENSA / 5.0000D+07 , 1.0000D+08 , 2.0000D+08 ,
CX     &              5.0000D+08 , 1.0000D+09 , 2.0000D+09 ,
CX     &              5.0000D+09 , 1.0000D+10 , 2.0000D+10 ,
CX     &              5.0000D+10 , 1.0000D+11 , 2.0000D+11 ,
CX     &              5.0000D+11 , 1.0000D+12 , 2.0000D+12 ,
CX     &              5.0000D+12 , 1.0000D+13 , 2.0000D+13 ,
CX     &              5.0000D+13 , 1.0000D+14 , 2.0000D+14 ,
CX     &              5.0000D+14 , 1.0000D+15 , 2.0000D+15 ,
CX     &              6*0.0D0/
C----------------------------------------------------------------------
C   DATA FOR SANCO FILE FORMATION
C----------------------------------------------------------------------
C      DATA MAXT / 35 /
C      DATA MAXD / 26 /
C      DATA TEK   / 1.1604D+04 , 1.4609D+04 , 1.8391D+04 , 2.3153D+04,
C    &              2.9148E+04 , 3.6695D+04 , 4.6196D+04 , 5.8158D+04,
C    &              7.3216E+04 , 9.2174E+04 ,
C    &              1.1604D+05 , 1.4609D+05 , 1.8391D+05 , 2.3153D+05,
C    &              2.9148E+05 , 3.6695D+05 , 4.6196D+05 , 5.8158D+05,
C    &              7.3216E+05 , 9.2174E+05 ,
C    &              1.1604D+06 , 1.8391D+06 ,
C    &              2.9148E+06 , 4.6196D+06 ,
C    &              7.3216E+06 ,
C    &              1.1604D+07 , 1.8391D+07 ,
C    &              2.9148E+07 , 4.6196D+07 ,
C    &              7.3216E+07 ,
C    &              1.1604D+08 , 1.8391D+08 ,
C    &              2.9148E+08 , 4.6196D+08 ,
C    &              5.8158E+08 /
C      DATA DENSA / 1.000D+10 , 1.585D+10, 2.512D+10 ,
C    &              3.981D+10 , 6.310D+10,
C    &              1.000D+11 , 1.585D+11, 2.512D+11 ,
C    &              3.981D+11 , 6.310D+11,
C    &              1.000D+12 , 1.585D+12, 2.512D+12 ,
C    &              3.981D+12 , 6.310D+12,
C    &              1.000D+13 , 1.585D+13, 2.512D+13 ,
C    &              3.981D+13 , 6.310D+13,
C    &              1.000D+14 , 1.585D+14, 2.512D+14 ,
C    &              3.981D+14 , 6.310D+14,
C    &              1.00D+15  , 0,0,0,0 /
C----------------------------------------------------------------------
C   DATA FOR NORMAL FILES
C----------------------------------------------------------------------
C      DATA MAXT / 20 /
C      DATA MAXD / 13 /
C      DATA TEK   / 4.62E+3, 7.32E+3
C    &            , 1.16D+4, 1.84D+4, 2.91D+4, 4.62D+4, 7.32D+4
C    &            , 1.16D+5, 1.84D+5, 2.91D+5, 4.62D+5, 7.32D+5
C    &            , 1.16D+6, 1.84D+6, 2.91D+6, 4.62D+6, 7.32D+6
C    &            , 1.16D+7, 1.84D+7, 2.91D+7, 4.62D+7, 7.32D+7
C    &            , 1.16D+8,
C    &              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C      DATA DENSA / 1.000D+04 , 1.000D+05, 1.000D+06 ,
C    &              1.000D+07 , 1.000D+08, 1.000D+09 ,
C    &              1.000D+10 , 1.000D+11, 1.000D+12 ,
C    &              1.000D+13 , 1.000D+14, 1.000D+15 ,
C    &              1.000D+16 , 0, 0, 0, 0, 0, 0, 0,
C    &                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C----------------------------------------------------------------------
C   DATA FOR CHROMIUM AND MOLYBDENUM STUDY
C----------------------------------------------------------------------
C      DATA MAXT / 12 /
C      DATA MAXD / 13 /
C      DATA TEK   /
C    &              1.16D+4, 1.84D+4, 2.91D+4, 4.62D+4, 7.32D+4
C    &            , 1.16D+5, 1.84D+5, 2.91D+5, 4.62D+5, 7.32D+5
C    &            , 1.16D+6, 1.84D+6
C    &            , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
C    &            , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C      DATA DENSA / 1.000D+04 , 1.000D+05, 1.000D+06 ,
C    &              1.000D+07 , 1.000D+08, 1.000D+09 ,
C    &              1.000D+10 , 1.000D+11, 1.000D+12 ,
C    &              1.000D+13 , 1.000D+14, 1.000D+15 ,
C    &              1.000D+16 , 0, 0, 0, 0, 0, 0, 0,
C    &                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C----------------------------------------------------------------------
C      GATHER NUCLEAR CHARGE AND YEAR OF DATA FROM SCREEN
CX UNIX-IDL CONVERSION: READ IN AS PARAMETERS
C----------------------------------------------------------------------
CX       CALL XUFLOW(0)
CX       CALL CL3270
CX       WRITE(IST2,1000)
CX       WRITE(IST2,1002)
CX       READ(IST1,*) IZ0 , IZL , IZH
CX       WRITE(IST2,1010)
CX       READ(IST1,1018) USERID
CX       WRITE(IST2,1012)
CX       READ(IST1,1014) YEAR
CX       WRITE(IST2,1016)
CX       READ(IST2,1014) CHPRFIX

     
       IF( CHPRFIX .NE. 'NO') THEN
         LPRFIX = .TRUE.
       ELSE
         LPRFIX = .FALSE.
       ENDIF
C----------------------------------------------------------------------
C   GATHER CURRENT DATE - NOW A PARAMETER
C----------------------------------------------------------------------
CX       CALL XXDATE(DATE)
C----------------------------------------------------------------------
C   GATHER USER ID FOR OUTPUT FILES
C----------------------------------------------------------------------
CX       CALL DMGUID(SYSUID,PREFIX)
C----------------------------------------------------------------------
C   CALCULATE LOG BASE 10 VALUES OF OUTPUT TEMPERATURES AND DENSITIES
C----------------------------------------------------------------------
       DO 2 IT = 1,MAXT
         TEVL(IT) = DLOG10( TEK(IT) / 1.16054D+04 )
    2  CONTINUE
       DO 3 ID = 1,MAXD
         DENSL(ID) = DLOG10( DENSA(ID) )
    3  CONTINUE
C----------------------------------------------------------------------
C   DEFINE DATA ON ELEMENT  AND RANGE OF CALCULATION
C----------------------------------------------------------------------
       ELEMT = XFESYM( IZ0 )
       ELEMN = XFELEM( IZ0 )
       LSE   = 1
       IF( ELEMT(2:2) .NE. ' ' ) LSE = 2
C----------------------------------------------------------------------
C   CYCLE THROUGH TYPES OF VARIABLE
C----------------------------------------------------------------------
       DO 300 ITYPE  = 1,9
         IF (.NOT. LDTYP(ITYPE)) GOTO 300
C          ISWIT = 1                          ! this bypasses options in the
C          IF (ITYPE.EQ.2) ISWIT = 2          ! spline routines - WHY?
C----------------------------------------------------------------------
C   DYNAMIC OPEN OF ISONUCLEAR MASTER FILE
C
C      OUTPUT IS WRITTEN TO PASSING FILES
C              E.G.   ACD404.PASS
C                     SCD404.PASS
C                     CCD404.PASS
C                     PRB404.PASS
C                     PRC404.PASS
C                     QCD404.PASS
C                     XCD404.PASS
C                     PLT404.PASS
C                     PLS404.PASS
C----------------------------------------------------------------------
CX         DSNOUT = '/'//SYSUID(1:6)//'.'//CDTYP(ITYPE)//'404.PASS'
         DSNOUT = DSNO(ITYPE)
C
         IF(OPEN17)THEN
            WRITE(IST2,1004)  DSNOUT
            WRITE(IST2,1005)
         ENDIF
C
         IWRITE = IST4
         INQUIRE(FILE = DSNOUT , EXIST = LEXIST)
CX         CALL FILEINF(IRCODE,'TRK',10,'SECOND',5,'RECFM','FB',
CX     &                'LRECL',80,'BLKSIZE',4800)
         OPEN( UNIT = IWRITE, FILE = DSNOUT, STATUS = 'UNKNOWN')
C
         WRITE(IWRITE,1200) IZ0, MAXD, MAXT, IZL, IZH, ELEMN
         WRITE(IWRITE,1205)
         call d4wmet(iwrite,iz0)
         WRITE(IWRITE,1205)
         WRITE(IWRITE,1210) ( DENSL(ID), ID = 1,MAXD)
         WRITE(IWRITE,1210) ( TEVL(IT) , IT = 1,MAXT)
C----------------------------------------------------------------------
CX   CONSTRUCT FIRST PART OF INPUT FILE NAME
C CONSTRUCTED IN IDL NOW AND PASSED AS DSNIN
C----------------------------------------------------------------------
CX         DSNAME = '/'//USERID//'.'//CDTYP(ITYPE)//YEAR//'.DATA('
CX         IF ( LPRFIX ) THEN
CX           DSNAME = DSNAME(1:19)//CHPRFIX//'#)'
CX         ELSE
CX           DSNAME = DSNAME(1:19)//')'
CX         ENDIF
C----------------------------------------------------------------------
C   CALL SUBROUTINE TO INTERROGATE MASTER CONDENSED FILES AND
C   WRITE ELEMENT MASTER FILE
C----------------------------------------------------------------------
         IF (ITYPE .GE. 1 .AND. ITYPE. LE. 7) THEN
           CALL CNV404A(ITYPE , ISWIT   ,
     &                  NUTMAX, NUDMAX  , NUZMAX , NUMMAX,
     &                  MAXT  , MAXD    ,
     &                  IZL   , IZH     , IZ0    , TEK   ,  DENSA ,
     &                  NGRD  ,
     &                  IST2  , IST5    , IWRITE , DATE,
     &                  DSNIN , OPEN17)
         ELSEIF (ITYPE .GE. 8 .AND. ITYPE. LE. 9) THEN
           CALL CNV404B(ITYPE , ISWIT   ,
     &                  NUTMAX, NUDMAX  , NUZMAX , NUMMAX,
     &                  MAXT  , MAXD    ,
     &                  IZL   , IZH     , IZ0    , TEK   ,  DENSA ,
     &                  NGRD  ,
     &                  IST2  , IST5    , IWRITE , DATE,
     &                  DSNIN , OPEN17)
         ENDIF
C----------------------------------------------------------------------
C         WRITE INFORMATION SECTION AT BOTTOM OF DATA SET
C         CLOSE ISONUCLEAR MASTER FILE
C----------------------------------------------------------------------
         WRITE(IWRITE,1206)
         WRITE(IWRITE,1220)
         IF(ITYPE .EQ. 4 .OR. ITYPE .EQ. 5 .OR.
     &      ITYPE .EQ. 8 .OR. ITYPE .EQ. 9) THEN
           WRITE(IWRITE,1230)
           WRITE(IWRITE,1232)
         ELSE
           WRITE(IWRITE,1234)
           WRITE(IWRITE,1236)
         ENDIF
         WRITE(IWRITE,1220)
         WRITE(IWRITE,1240)
         IF (LPRFIX) THEN
           WRITE(IWRITE,1244) YEAR, CHPRFIX
         ELSE
           WRITE(IWRITE,1242) YEAR
         ENDIF
         WRITE(IWRITE,1246) ELEMT, IZL, IZH
         WRITE(IWRITE,1220)
         WRITE(IWRITE,1224) USER, DATE
         WRITE(IWRITE,1220)
         WRITE(IWRITE,1206)
         CLOSE(IWRITE)
C----------------------------------------------------------------------
C    END OF VARIABLE TYPE LOOP
C----------------------------------------------------------------------
  300  CONTINUE
C----------------------------------------------------------------------
C   FINISHED!
C----------------------------------------------------------------------
       RETURN
C
 1000  FORMAT(' PROGRAM LH404RR TO FORM ISONUCLEAR MASTER FILES '/
     &        ' ----------------------------------------------- ')
 1002  FORMAT(' INPUT NUCLEAR CHARGE (IZ0)'/
     &        ' IZ1 OF LOWEST CHARGE STATE (IZL) AND '/
     &        ' IZ1 OF HIGHEST CHARGE STATE (IZH):')
 1004  FORMAT(/' DSNOUT : ',1A80)
 1005  FORMAT(/' ACCESSING DATA FROM :' )
 1006  FORMAT(/' DSNAME : ',1A80)
 1010  FORMAT(/' INPUT USERID OF DATA (USUALLY JETSHP) :')
 1012  FORMAT(/' INPUT YEAR OF DATA :')
 1014  FORMAT(1A2)
 1016  FORMAT(/' INPUT DATA PREFIX (EG. <PJ> ) OR <NO> IF NONE:')
 1018  FORMAT(1A6)
C
 1200  FORMAT(5I5,5X,'/',1A12,'       /GCR PROJECT      ')
 1205  FORMAT(80('-'))
 1206  FORMAT('C',79('-'))
 1210  FORMAT(8F10.5)
C
 1220  FORMAT('C')
 1230  FORMAT('C     RADIATED POWERS IN UNITS OF W CM**3 ')
 1232  FORMAT('C     ----------------------------------- ')
 1234  FORMAT('C     RATE COEFFICIENTS IN UNITS OF CM**3 S-1 ')
 1236  FORMAT('C     --------------------------------------- ')
 1240  FORMAT('C     INPUT DATA')
 1242  FORMAT('C         FOR YEAR: ',1A2)
 1244  FORMAT('C         FOR YEAR: ',1A2,' AND WITH PREFIX: ',1A2)
 1246  FORMAT('C         FOR ',1A2,' RECOMBINING ION CHARGE(S) ',
     &                   I2,' TO ',I2)
C
 1224  FORMAT('C     CODE     : ADAS404',/,
     &        'C     PRODUCER : ',A30,/,
     &        'C     DATE     : ',A8)
     
      END
