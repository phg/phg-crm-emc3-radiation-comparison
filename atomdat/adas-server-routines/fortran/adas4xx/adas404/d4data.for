CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4data.for,v 1.3 2004/07/06 13:13:49 whitefor Exp $ Date $Date: 2004/07/06 13:13:49 $
CX
      SUBROUTINE D4DATA( TITLE , DATE  , UIDIN  , USERID ,
     &                   ISWIT , NIND  , YEAR   , SELTAB , REPTAB ,
     &                   NDZ   , NDDEN , NDTIN  ,
     &                   IZ0   , IZ1   , NEL1   , IZ2    , NEL2   ,
     &                   T     , TL    , MAXT   ,
     &                   DENSA , DENSL , MAXD   ,
     &                   ATTY  , ARRAY , ZINTRP , DINTRP , TINTRP ,
     &                   DSNIN , DSNO  , OPEN17
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4DATA *********************
C
C  PURPOSE: TO OPEN/ACQUIRE DATA FROM STD.MASTER CONDENSED COLLISIONAL-
C           DIELECTRONIC   FILES,   OBTAIN  INTERPOLATED   COLLISIONAL-
C           DIELECTRONIC RECOMBINATION AND IONISATION COEFFICIENTS, AND
C           PREPARE ELEMENT MASTER FILE IF REQUIRED.
C
C  CALLING PROGRAM: ADAS404
C
C  DATA:
C
C       INPUT:
C       ------
C           THE  SOURCE  DATA IS CONTAINED AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             6. JETUID.PLT<YR>.DATA
C             7. JETUID.PLS<YR>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C           IF <YR> IS BLANK THEN THE CURRENT RECOMMENDED DATA SETS ARE
C           USED
C
C           'JETUID' IS GIVEN BY 'UIDIN'
C
C           THE PARTICULAR TYPE OPENED (1-7) IS SELECTED BY 'ISWIT'
C
C           THE MEMBERS OF THE PARTITIONED DATA SETS ARE  <SE>
C           WHERE <SE> IS THE ONE OR TWO LETTER ION SEQUENCE CODE
C
C           THIS PROGRAM ASSESSES ONLY STANDARD MASTER CONDENSED FILES.
C           -----------------------------------------------------------
C
C
C      OUTPUT:
C      -------
C           THE OUTPUT ELEMENT MASTER DATA IS IN SEQUENTIAL FILES AS
C           FOLLOWS:
C
C             1. JETUID.ACD<YR>#<EL>.DATA
C             2. JETUID.SCD<YR>#<EL>.DATA
C             3. JETUID.CCD<YR>#<EL>.DATA
C             4. JETUID.PRB<YR>#<EL>.DATA
C             5. JETUID.PRC<YR>#<EL>.DATA
C             6. JETUID.PLT<YR>#<EL>.DATA
C             7. JETUID.PLS<YR>#<EL>.DATA
C
C           WHERE <YR> IS AS ABOVE AND  <EL> IS THE ELEMENT SYMBOL
C
C           'JETUID' IS GIVEN BY 'USERID'
C
C  SUBROUTINE:
C
C  INPUT : (C*32) TITLE    = USER ENTERED PROGRAM RUN TITLE
C  INPUT : (C*8)  DATE     = CURRENT DATE (AS 'DD/MM/YY')
C  INPUT : (C*6)  UIDIN   = PROJECT UID FOR INPUT CONDENSED DATA FILE
C  INPUT : (C*6)  USERID  = USER ID FOR OUTPUT INC. ELEMENT MASTER FILE
C
C  INPUT : (I*4)  ISWIT    = DATA TYPE SELECTOR (SEE ABOVE) (1 -> 7)
C  INPUT : (I*4)  NIND     = NUMBER OF STAGES BEGINNING WITH LOWEST TO
C                            BE SKIPPED.   IT IS ASSUMED THAT DATA FOR
C                            THESE STAGES WILL BE SUPPLIED SEPARATELY.
C                            (DESIGNED FOR NEUTRAL STATE).
C  INPUT : (C*2)  YEAR     = REFERENCE  YEAR  (ABBREVIATED)  OF   INPUT
C                            MASTER CONDENSED COLL.-DIEL. COEFFTS. FILE.
C  INPUT : (L*4)  SELTAB  = .TRUE.  => PREPARE ELEMENT MASTER FILE
C                           .FALSE. => DO NOT PREPARE ELEM. MASTER FILE
C  INPUT : (L*4)  REPTAB  = .TRUE.  => REPLACE EXISTING ELEMENT MASTER
C                                      FILES.
C                         = .FALSE. => DO NOT REPLACE EXISTING ELEMENT
C                                      MASTER FILES.
C                           ('REPTAB' IS IGNORED IF 'SELTAB'=.FALSE.)
C
C  INPUT : (I*4)  NDZ      = NUMBER OF CHARGE STATES
C  INPUT : (I*4)  NDDEN    = MAXIMUM NUMBER OF INPUT DENSITIES
C  INPUT : (I*4)  NDTIN    = MAXIMUM NUMBER OF INPUT TEMPERATURES
C
C  INPUT : (I*4)  IZ0      = ELEMENT NUCLEAR CHARGE
C                                         (DETERMINES OUTPUT FILE NAME)
C  INPUT : (I*4)  IZ1      = MINIMUM ALLOWED IONIC CHARGE + 1
C                            (ACCORDING TO AVAILABLE NO. OF SEQUENCES
C                             STORED IN FILES FOR 'YEAR')
C  INPUT : (I*4)  NEL1     = NUMBER OF ELECTRONS IN STATE 'IZ1'
C  INPUT : (I*4)  IZ2      = MAXIMUM ALLOWED IONIC CHARGE + 1
C  INPUT : (I*4)  NEL2     = NUMBER OF ELECTRONS IN STATE 'IZ2'
C
C  INPUT : (R*8)  T()      = SET OF 'MAXT' ELECTRON TEMPERATURES: KELVIN
C  INPUT : (R*8)  TL()     = LOG10('T()')
C  INPUT : (I*4)  MAXT     = NUMBER OF TEMPERATURES (<= 'NDTIN' )
C
C  INPUT : (R*8)  DENSA()  = SET OF 'MAXD' ELECTRON DENSITIES: CM-3
C  INPUT : (R*8)  DENSL()  = LOG10 ('DENSA()')
C  INPUT : (I*4)  MAXD     = NUMBER OF DENSITIES (<= 'NDDEN' )
C
C  OUTPUT: (R*8)  ATTY(,) = WORKING SPACE FOR 3-WAY SPLINE ITERPOLATION
C                           ( STORES LOG10(INTERPOLATED VALUES) )
C                            1ST DIMENSION: TEMPERATURE
C                            2ND DIMENSION: DENSITY
C  OUTPUT: (R*8)  ARRAY(,,)= LOG10(INTERPOLATED DATA) FOR:
C                             1ST ARRAY DIMENSION - ION CHARGE/STAGE
C                             2ND ARRAY DIMENSION - TEMPERATURE
C                             3RD ARRAY DIMENSION - DENSITY
C  OUTPUT: (L*4)  ZINTRP() = .TRUE.  => 'ARRAY(,,)' VALUE FOR CHARGE-
C                                       STATE INTERPOLATED.
C                          = .FALSE. => 'ARRAY(,,)' VALUE FOR CHARGE-
C                                       STATE EXTRAPOLATED.
C                           1ST DIMENSION: CHARGE-STATE INDEX
C  OUTPUT: (L*4)  DINTRP(,)= .TRUE.  => 'ARRAY(,,)' VALUE FOR DENSITY
C                                       INDEX AND CHARGE INTERPOLATED.
C                          = .FALSE. => 'ARRAY(,,)' VALUE FOR DENSITY
C                                       INDEX AND CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: DENSITY INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C  OUTPUT: (L*4)  TINTRP(,)= .TRUE.  =>'ARRAY(,,)' VALUE FOR TEMPERATURE
C                                      INDEX AND CHARGE INTERPOLATED.
C                          = .FALSE. =>'ARRAY(,,)' VALUE FOR TEMPERATURE
C                                      INDEX AND CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: TEMPERATURE INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C  OUTPUT: (C*80) DSNO()  = OUPUT MASTER FILE NAME FOR EACH DATA TYPE
C
C  OUTPUT: (C*80) DSNIN(,)= INPUT FILE NAME FOR EACH DATA TYPE AND
C                           CHARGE
C
C          (I*4)  NKDIM   = PARAMETER =
C                           MAXIMUM  ARRAY  DIMENSIONS  FOR  CONDENSED
C                           MASTER FILE DATA FOR A GIVEN CHARGE STATE.
C          (I*4)  IUNT12  = PARAMETER = UNIT FOR READING DATA = 12
C
C          (R*8)  LOGMIN  = PARAMETER = MINIMUM LOG VALUE ALLOWED
C
C          (C*2)  XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*30) DSNAME  = INPUT MASTER CONDENSED FILE DATA SET NAME
C          (C*30) DSNOUT  = OUTPUT ELEMENT MASTER FILE DATA SET NAME
C          (C*2)  SEQUA   = ELEMENT SYMBOL FOR GIVEN NUCLEAR CHARGE
C          (C*3)  CDTYP() = INPUT MASTER CONDENSED FILE TYPE  USED  FOR
C                           CONSTRUCTING 'DSNAME'/'DSNOUT'. () = 'ISWIT'
C
C          (L*4)  LEXIST  = .TRUE.  => STANDARD MASTER CONDENSED FILE
C                                      EXISTS.
C                           .FALSE. => STANDARD MASTER CONDENSED FILE
C                                      DOES NOT EXIST.
C          (L*4)  LERROR  = .TRUE.  => ERROR FOUND IN READING STANDARD
C                                      MASTER CONDENSED FILE.
C                           .FALSE  =>NO ERROR FOUND IN READING STANDARD
C                                      MASTER CONDENSED FILE.
C          (L*4)  LSWIT   = .TRUE.  => SET OF 'EIA' VALUES PRESENT IN
C                                      MASTER CONDENSED FILE.
C                           .FALSE  => SET OF 'EIA' VALUES NOT PRESENT
C                                      IN MASTER CONDENSED FILE.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZSTRT  = 'IZ1'
C          (I*4)  IZSTOP  = 'IZ2'
C          (I*4)  NZMAX   = 'NDZ'
C          (I*4)  NDMAX   = 'NDDEN'
C          (I*4)  NTMAX   = 'NDTIN'
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IS      = ARRAY SUBSCRIPT USED FOR SEQUENCE VALUES.
C                           REPRESENTS NUCLEAR CHARGE FOR ISO-ELECTRONIC
C                           SEQUENCE ELEMENT.
C                           (IMPLIES NUCLEAR CHARGE 'IS'-LIKE SEQUENCE)
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IDE     = NUMBER OF REDUCED DENSITIES READ FROM INPUT
C                           MASTER CONDENSED FOR SEQUENCE 'IS'.
C          (I*4)  ITE     = NO. OF REDUCED TEMPERATURES READ FROM INPUT
C                           MASTER CONDENSED FOR SEQUENCE 'IS'.
C          (I*4)  IZE     = NO. OF CHARGE STATES GIVEN IN THE INPUT
C                           MASTER CONDENSED FOR SEQUENCE 'IS'.
C          (I*4)  IZF     = ELEMENT RECOMBINING ION CHARGE (IZ0+1-IS)
C          (I*4)  LS      = NON-BLANK LENGTH OF 'SEQUA'.
C          (I*4)  LD1     = VALUE  FOR  FIRST  DIMENSION OF 'ARRAY(,,)'
C                           (REPRESENTS STAGE/ION CHARGE)
C
C          (R*8)  ZIPT()  = SET OF 'IZE' INPUT RECOMBINING ION CHARGES
C                           READ FROM CONDENSED MASTER FILE.
C          (R*8)  TR()    = SET OF 'ITE'  INPUT  REDUCED  TEMPERATURES
C                           (K/Z1**2) READ FROM CONDENSED MASTER FILE.
C          (R*8)  DENSR() = SET OF 'IDE' INPUT REDUCED DENSITIES (CM-3/
C                           Z1**7) READ FROM CONDENSED MASTER FILE.
C          (R*8)  AIPT(,,)= CONDENSED MASTER FILE DATA. COLL-DIEL COEFF.
C                           1ST DIMENSION: REDUCED DENSITY ('DENSR()')
C                           2ND DIMENSION: REDUCED TEMPERATURE ('TR()')
C                           3RD DIMENSION: CHARGE STATE ('ZIPT()')
C          (R*8)  EIA()   = IONISATION RATE COEFFICIENTS: ()=ION CHARGE
C                           (UNITS: PRIOR TO 'XXCEIA' CALL: WAVE NUMBERS
C                                   AFTER CALL TO 'XXCEIA': RYDBERGS )
C NOTE:
C          STREAM HANDLING:
C                  STREAM 12 IS USED FOR READING CONDENSED MASTER FILES
C                  STREAM 13 IS USED FOR WRITING  ELEMENT  MASTER FILES
C
C          THIS SUBROUTINE IS A STRUCTURED AND AMENDED VERSION OF  THE
C          SUBROUTINE 'EIONST' WRITTEN BY H.P. SUMMERS, JET  (VERSION:
C          2 NOV 1989 / 1FEB 1990).
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D4OUTF     ADAS      OUTPUT OF ELEMENT MASTER FILE
C          DXSPL1     ADAS      1ST PART OF 3-WAY SPLINE OF INPUT DATA
C          DXSPL2     ADAS      2ND PART OF 3-WAY SPLINE OF INPUT DATA
C          DXSPL3     ADAS      3RD PART OF 3-WAY SPLINE OF INPUT DATA
C          XXOPEN     ADAS      INQUIRE AND OPEN A DATA SET
C          XXINST     ADAS      FETCH   DATA   FROM   STANDARD  MASTER
C                               CONDENSED FILE.
C          XXCEIA     ADAS      CONVERT ION. RATE COEF. FROM WAVE NOS TO
C                               RYDBERGS AND EXTRAPOLATE MISSING VALUES
C          XFESYM     ADAS      CHARACTER*2 FUNCTION -
C                               FETCH ELEMENT SYMBOL FOR GIVEN NUCLEAR
C                               CHARGE
C          XXTERM     ADAS      TERMINATES PROGRAM WITH MESSAGE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    08/10/90
C
C DATE:    26/10/90 - PE BRIDEN (JET/TESSELLA) - MINIMUM LOG10 VALUE
C                     ALLOWED WAS SET EQUAL TO 'LOGMIN'.  I.E. GIVES
C                     LOWER LIMIT FOR 'ARRAY(,,)'.
C
C UPDATE:  29/01/91 - PE BRIDEN - ADAS91 -INTRODUCED 'TINTRP' & 'DINTRP'
C                                        - AMENDED ARGUMENT LIST
C                                        - AMENDED ARGUMENT LISTS FOR
C                                          'D4SPL2' AND 'D4SPL3'.
C
C UPDATE:  30/01/91 - PE BRIDEN - ADAS91 -INTRODUCED 'ZINTRP'
C
C UPDATE:  13/02/91 - PE BRIDEN - ADAS91 - REPLACED XXESYM WITH XFESYM
C
C UPDATE:  21/02/91 - PE BRIDEN - ADAS91: INTRODUCED 'IZSTRT' , 'IZSTOP'
C                                 'NZMAX', 'NDMAX' AND 'NTMAX' TO  STOP
C                                 ICA MESSAGES BEING GENERATED.
C
C UPDATE:  05/03/91 - PE BRIDEN - ADAS91: ADDED CALL TO 'XXOPEN' BEFORE
C                                         AMENDED 'XXINST'.
C
C UPDATE:  20/03/91 - PE BRIDEN - ADAS91: MAJOR   CHANGES   TO   SPLINE
C                                         ROUTINES 'D4SPL?' -> 'DXSPL?'.
C                                         DENSA() ADDED TO ARGUMENT LIST
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C VERSION: 1.2				DATE: 20-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- TIDIED OUTPUT
C
C VERSION: 1.3				DATE: 28-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- CORRECTED DEFINITION OF OPEN17 TO LOGICAL TYPE
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NKDIM       , IUNT17           , IUNT12
C-----------------------------------------------------------------------
      REAL*8     LOGMIN
C-----------------------------------------------------------------------
      PARAMETER( NKDIM = 10  , IUNT17 = 17      , IUNT12 = 12 )
C-----------------------------------------------------------------------
      PARAMETER( LOGMIN = -74.0                 )
C-----------------------------------------------------------------------
      CHARACTER  XFESYM*2
      CHARACTER  UIDIN*6     , USERID*6
      CHARACTER  YEAR*2      , DSNAME*80        , DSNOUT*80  , TITLE*32
      CHARACTER  SEQUA*2     , DATE*8           , CDTYP(7)*3
      CHARACTER  DSNO(10)*80 , DSNIN(50,10)*80
C-----------------------------------------------------------------------
      LOGICAL    SELTAB      , REPTAB           , LERROR     ,
     &           LEXIST      , LSWIT            , OPEN17
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    NDZ         , NDDEN            , NDTIN
      INTEGER    IZ0         , IZ1              , IZ2        ,
     &                         NEL1             , NEL2       ,
     &           ISWIT       , MAXT             , MAXD       ,
     &           NIND
      INTEGER    ID          , IS               , IT         ,
     &           IDE         , ITE              , IZE        ,
     &           IZSTRT      , IZSTOP           ,
     &           NZMAX       , NDMAX            , NTMAX
      INTEGER    IZF         , LS               , LD1
C-----------------------------------------------------------------------
      REAL*8     T(NDTIN)    , TL(NDTIN)         ,
     &           DENSA(NDDEN), DENSL(NDDEN)
      REAL*8     ZIPT(NKDIM) , TR(NKDIM)         , DENSR(NKDIM)
      REAL*8     EIA(50)     , ATTY(NDTIN,NDDEN)
      REAL*8     ARRAY(NDZ,NDTIN,NDDEN)
      REAL*8     AIPT(NKDIM,NKDIM,NKDIM)
C-----------------------------------------------------------------------
      LOGICAL    ZINTRP(NDZ) , DINTRP(NDDEN,NDZ) , TINTRP(NDTIN,NDZ)
C-----------------------------------------------------------------------
      DATA  CDTYP/'acd','scd','ccd','prb','prc','plt','pls'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
       NZMAX = NDZ
       NDMAX = NDDEN
       NTMAX = NDTIN
C
C-----------------------------------------------------------------------
C CONSTRUCT OUTPUT ELEMENT MASTER FILE DATA SET NAME (FOR DYNAMIC OPEN)
C IDL-UNIX : DSNO CREATED IN D4SPF1.PRO
C-----------------------------------------------------------------------
C
       SEQUA = XFESYM( IZ0 )
       LS=1
       IF (SEQUA(2:2).NE.' ') LS=2
CX       DSNOUT='/'//USERID//'.'//CDTYP(ISWIT)//YEAR//'#'//
CX     &        SEQUA(1:LS)//'.DATA'
       DSNOUT=DSNO(ISWIT)
C-----------------------------------------------------------------------
       IF(OPEN17)WRITE(IUNT17,1000) CDTYP(ISWIT)
       IF (SELTAB.AND.OPEN17) WRITE(IUNT17,1001) DSNOUT
       IF(OPEN17)WRITE(IUNT17,1002)
C
C***********************************************************************
C READ IN RELEVANT CONDENSED MASTER FILES AND INTERPOLATE
C***********************************************************************
C
       LD1=NIND
       IZF=IZ1-1
C-----------------------------------------------------------------------
          DO 1 IS=NEL1,NEL2,-1
             LD1=LD1+1
             IZF=IZF+1
C
C-----------------------------------------------------------------------
C CONSTRUCT RELEVANT INPUT CONDENSED MASTER FILE DATA SET NAME (DYNAMIC)
C-----------------------------------------------------------------------
C
             SEQUA = XFESYM( IS )
             LS=1
             IF (SEQUA(2:2).NE.' ') LS=2
CX             DSNAME='/'//UIDIN//'.'//CDTYP(ISWIT)//YEAR//
CX     &              '.DATA('//SEQUA(1:LS)//')'
             DSNAME=DSNIN(NEL1-IS+1,ISWIT)
C-----------------------------------------------------------------------
             IF(OPEN17)WRITE(IUNT17,1003) IS,DSNAME
C
C-----------------------------------------------------------------------
C READ IN DATA FROM SELECTED STANDARD MASTER CONDENSED FILE DATA SET
C-----------------------------------------------------------------------
C
             CALL XXOPEN( IUNT12 , DSNAME , LEXIST )
                IF (LEXIST) THEN
                   CALL XXINST( IUNT12 , DSNAME , LERROR ,
     &                          NKDIM  , NKDIM  , NKDIM  ,
     &                          IDE    , ITE    , IZE    ,
     &                          DENSR  , TR     , ZIPT   ,
     &                          LSWIT  , EIA    ,
     &                          AIPT
     &                        )
                   CLOSE( IUNT12 )
                   IF (LERROR) CALL XXTERM
                ELSE
                   CALL XXTERM
                ENDIF
C
C-----------------------------------------------------------------------
C  CONVERT IONS. RATE COEFFS TO RYDBERGS AND FILL IN ANY MISSING VALUES
C-----------------------------------------------------------------------
C
             IF (LSWIT) CALL XXCEIA( EIA )
C
C-----------------------------------------------------------------------
C PERFORM A THREE WAY SPLINE ON THE INPUT DATA
C-----------------------------------------------------------------------
C
             CALL DXSPL1( ISWIT         , LSWIT  , IZF    ,
     &                    NDMAX         , NTMAX  ,
     &                    NKDIM         , NKDIM  , NKDIM  ,
     &                    IDE           , ITE    , IZE    ,
     &                                    TR     , ZIPT   , EIA  ,
     &                    AIPT          ,
     &                    ZINTRP(LD1)   ,
     &                    ATTY
     &                  )
C-----------------------------------------------------------------------
             CALL DXSPL2( ISWIT         , LSWIT  , IZF    ,
     &                    NDMAX         , NTMAX  ,
     &                    NKDIM         ,
     &                    IDE           , ITE    ,
     &                    MAXD          , DENSR  , DENSA  ,
     &                    DINTRP(1,LD1) ,
     &                    ATTY
     &                  )
C-----------------------------------------------------------------------
             CALL DXSPL3( ISWIT         , LSWIT  , IZF    ,
     &                    NDMAX         , NTMAX  ,
     &                                    NKDIM  ,
     &                    MAXD          , ITE    ,
     &                    MAXT          , TR     , T      , EIA(IZF) ,
     &                    TINTRP(1,LD1) ,
     &                    ATTY
     &                  )
C
C-----------------------------------------------------------------------
C ASSIGN SPLINE OUTPUT RESULTS TO MAIN ARRAY.
C-----------------------------------------------------------------------
C
                DO 4 ID=1,MAXD
                   DO 5 IT=1,MAXT
                      ARRAY(LD1,IT,ID) = DMAX1(ATTY(IT,ID),LOGMIN)
    5              CONTINUE
    4           CONTINUE
C-----------------------------------------------------------------------
    1     CONTINUE
C
C***********************************************************************
C
C-----------------------------------------------------------------------
C OUTPUT ELEMENT MASTER FILE IF REQUIRED.
C-----------------------------------------------------------------------
C
             IF (SELTAB) THEN
                IZSTRT = IZ1
                IZSTOP = IZ2
                CALL D4OUTF ( DSNOUT, TITLE  , DATE   , REPTAB ,
     &                        NZMAX , NDMAX  , NTMAX  ,
     &                        IZ0   , IZSTRT , IZSTOP ,
     &                        MAXD  , MAXT   ,
     &                        DENSL , TL     ,
     &                        ARRAY
     &                      )
             ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/ /' ELEMENT MASTER FILE CREATION - TYPE: ',A3/1X,40('-'))
 1001 FORMAT(/' OUTPUT - ELEMENT MASTER FILE DSN: ',1A80)
 1002 FORMAT(/' INPUT  - INCLUDED MASTER CONDENSED FILES'/)
 1003 FORMAT(I5,1A80)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
