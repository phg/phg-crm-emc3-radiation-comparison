      SUBROUTINE D4WMET( IUNIT  , IZ0 )
      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: D4WMET *******************
C
C  PURPOSE:  Write the number of metastables for each ionisation
C            stage to the top of resolved adf11 files.
C
C
C  CALLING PROGRAM: ADAS404 - lh404rr
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = OUTPUT UNIT NUMBER
C  INPUT : (I*4)  IZ0      =         NUCLEAR CHARGE READ
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  Martin O'Mullane
C
C DATE:    4/11/99
C
C VERSION:  1.1                                   DATE: 4/11/99
C MODIFIED: Martin O'Mullane
C           - First version
C
C VERSION : 1.2
C DATE    : 31-01-201
C MODIFIED: Martin O'Mullane
C           - Allow iz0 up to 50.
C
C-----------------------------------------------------------------------
      INTEGER   IUNIT     , IZ0  , I,   J   , nlines
C-----------------------------------------------------------------------
      INTEGER   imet(50)
C-----------------------------------------------------------------------
      character str*80
C-----------------------------------------------------------------------
      DATA imet   /  1 , 2 , 1 , 2 , 2 , 4 , 3 , 4 , 2 , 2 ,
     &               1 , 2 , 2 , 4 , 3 , 4 , 2 , 2 , 1 , 1 ,
     &               1 , 1 , 3 , 2 , 1 , 1 , 1 , 1 , 1 , 1 ,
     &               1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ,
     &               3 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 /
C-----------------------------------------------------------------------

C Don't forget to write the fully stripped case - the 1 at the end.

      write(iunit,100)(imet(j), j = iz0, 1, -1), 1

 100  format(16(i5,:))

      END
