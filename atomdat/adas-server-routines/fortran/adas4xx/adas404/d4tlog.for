CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4tlog.for,v 1.1 2004/07/06 13:14:47 whitefor Exp $ Date $Date: 2004/07/06 13:14:47 $
CX
      SUBROUTINE D4TLOG( INTYP, ITVAL, TEMP )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4TLOG *********************
C
C  PURPOSE:      TO CONVERT AN ARRAY OF LOG10(TEMPERATURES) FROM:
C           (KELVIN TO ELECTRON VOLTS) OR (ELECTRON VOLTS TO KELVIN)
C
C  CALLING PROGRAM:  D4OUTF
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  INTYP   = 1 => INPUT 'TEMP(array)' UNITS: KELVIN
C                            = 2 => INPUT 'TEMP(array)' UNITS: eV
C  INPUT :    (I*4)  ITVAL   = NUMBER OF TEMPERATURES IN 'TIN(array)'
C  I/O   :    (R*8)  TEMP()  = LOG10(INPUT TEMPERATURES (STATED UNITS))
C
C             (R*8)  EV2KEL  = LOG10( ELEC.VOLTS TO KELVIN CONVERSION)
C             (R*8)  TCONV   = TEMPERATURE CONVERSION PARAMETER
C
C ROUTINES:  NONE
C
C NOTE:
C            TEMPERATURE CONVERSION PARAMETERS:
C
C            INTYP = 1 ; TCONV =>             KELVIN  -> eV
C            INTYP = 2 ; TCONV =>                 eV  -> KELVIN
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    08/10/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER I          , INTYP      , ITVAL
C-----------------------------------------------------------------------
      REAL*8  EV2KEL     , TCONV
      REAL*8  TEMP(ITVAL)
C-----------------------------------------------------------------------
      PARAMETER ( EV2KEL=4.06466D+00 )
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  TEMPERATURE CONVERSION:  KELVIN -> EV
C-----------------------------------------------------------------------
C
         IF (INTYP.EQ.1) THEN
            TCONV=-EV2KEL
C
C-----------------------------------------------------------------------
C  TEMPERATURE CONVERSION:  EV -> KELVIN
C-----------------------------------------------------------------------
C
         ELSEIF (INTYP.EQ.2) THEN
            TCONV=EV2KEL
C-----------------------------------------------------------------------
         ELSE
            STOP
     &      ' D4TLOG ERROR: INVALID OUTPUT TEMPERATURE TYPE SPECIFIER'
         ENDIF
C-----------------------------------------------------------------------
         DO 1 I=1,ITVAL
            TEMP(I)=TEMP(I)+TCONV
    1    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
