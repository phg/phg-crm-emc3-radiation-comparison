CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4out0.for,v 1.1 2004/07/06 13:14:15 whitefor Exp $ Date $Date: 2004/07/06 13:14:15 $
CX
      SUBROUTINE D4OUT0( IUNIT , IZ1    , IZ2    ,
     &                   DATE  , ENAME  , TITLE  ,
     &                   T     , TL     , MAXT   ,
     &                   DENSA , DENSL  , MAXD   ,
     &                   CADAS , IRESO
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4OUT0 *********************
C
C  PURPOSE:  TO GENERATE MAIN HEADING AND KEYS FOR TABULAR OUTPUT.
C
C  CALLING PROGRAM: ADAS404
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT   = OUTPUT STREAM FOR COLL.-DIEL. TABLE
C  INPUT : (I*4)  IZ1     = MINIMUM ALLOWED IONIC CHARGE + 1
C  INPUT : (I*4)  IZ2     = MAXIMUM ALLOWED IONIC CHARGE + 1
C
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C  INPUT : (C*12) ENAME   = NAME OF ELEMENT UNDER CONSIDERATION
C  INPUT : (C*32) TITLE   = USER ENTERED PROGRAM RUN TITLE.
C
C  INPUT : (R*8)  T()     = SET OF 'MAXT' ELECTRON TEMPERATURES (USER
C                           ENTERED VALUES) - UNITS KELVIN.
C  INPUT : (R*8)  TL()    = LOG10('T()')
C  INPUT : (I*4)  MAXT    = NUMBER OF TEMPERATURES (< 56)
C
C  INPUT : (R*8)  DENSA() = SET OF 'MAXD' ELECTRON DENSITIES (CM-3)
C                           (USER ENTERED VALUES)
C  INPUT : (R*8)  DENSL() = LOG10('DENSA()')
C  INPUT : (I*4)  MAXD    = NUMBER OF DENSITIES (< 31)
C
C  I/O   : (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C                           (BACKGROUND - REQUIRED AS INPUT. THIS SUB-
C                                         ROUTINE WILL UPDATE THE TIME
C                                         AND DATE IN THIS ADAS HEADER.
C                            FOREGROUND - SET WITHIN SUBROUTINE.)
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  IPLEN   = OUTPUT PAGE LENGTH IN LINES
C          (I*4)  IPLEFT  = PAGE LENGTH IN LINES FOR DENS/TEMP VALUES ON
C                           HEADER PAGE. (IF 'MAXT > PLEFT' PUT DENSITY/
C                           TEMPERATURE VALUES ON NEW PAGE)
C                           ('IPLEFT=IPLEN-26')
C
C          (I*4)  I       = GENERAL ARRAY ELEMENT INDEX
C
C          (R*8)  R8TCON  = REAL*8 FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  TEV     = ELECTRON TEMPERATURE IN eV
C
C ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           -----------------------------------------------------------
C           XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C           R8TCON     ADAS      REAL*8 FUNCTION: CONVERTS TEMP. UNITS
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     08/10/90
C
C UPDATE:   17/01/91 - PE BRIDEN: ADDED HEADER INFORMATION TO OUTPUT
C                               - RENAMED SUBROUTINE (ORIGINALLY D4TAB0)
C
C UPDATE:   28/01/91 - PE BRIDEN: - ADDED 'CADAS' AS AN ARGUMENT
C
C UPDATE:   29/01/91 - PE BRIDEN: - REFORMATTED OUTPUT - ADDED DENSITY
C                                   AND TEMPERATURE SECTION.
C                                 - ADDED EXTRA ARGUMENTS FOR ABOVE.
C                                 - ADDED EXTRA VARIABLES FOR ABOVE.
C                                 - ADDED 'R8TCON' FUNCTION FOR ABOVE.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IPLEN       , L1       , L2          , IPLEFT
C-----------------------------------------------------------------------
      PARAMETER( IPLEN  = 66 , L1 = 1   , L2 = 2      )
      PARAMETER( IPLEFT = IPLEN - 26    )
C-----------------------------------------------------------------------
      INTEGER    IUNIT       , IZ1      , IZ2         , IRESO      ,
     &           MAXT        , MAXD     , I           
C-----------------------------------------------------------------------
      REAL*8     R8TCON      , TEV
C-----------------------------------------------------------------------
      CHARACTER  ENAME*12    , TITLE*32 , DATE*8      , CADAS*80
C-----------------------------------------------------------------------
      REAL*8     T(MAXT)     , TL(MAXT) , DENSA(MAXD) , DENSL(MAXD)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C GATHER ADAS HEADER AND WRITE IT OUT
C-----------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
      WRITE(IUNIT,1000) CADAS(2:80)
C
C-----------------------------------------------------------------------
C OUPUT HEADING AND KEY.
C-----------------------------------------------------------------------
C
      WRITE(IUNIT,1001) 'ELEMENT MASTER FILE CREATION', 'ADAS404', DATE
C      WRITE(IUNIT,1002)
      WRITE(IUNIT,1003) TITLE
      WRITE(IUNIT,1004) ENAME, IZ1, IZ2
C
      IF(IRESO.EQ.0)THEN
         WRITE(IUNIT,1013)
      ELSEIF(IRESO.EQ.1)THEN
         WRITE(IUNIT,1014)
      ELSE
         WRITE(IUNIT,1015)
      ENDIF
C
      WRITE(IUNIT,1005) MAXT , MAXD
      WRITE(IUNIT,1006)
      WRITE(IUNIT,1007)
C
         DO 1 I=1,MAXT
            TEV  = R8TCON( L1 , L2 , L1  , T(I) )
               IF (I.LE.MAXD) THEN
                  WRITE(IUNIT,1008) I , TL(I)    , T(I)     , TEV    ,
     &                              I , DENSL(I) , DENSA(I)
               ELSE
                  WRITE(IUNIT,1009) I , TL(I)    , T(I)     , TEV
               ENDIF
    1    CONTINUE
C
         IF (MAXD.GT.MAXT) THEN
            DO 2 I=MAXT+1,MAXD
               WRITE(IUNIT,1010) I , DENSL(I) , DENSA(I)
    2       CONTINUE
         ENDIF
C
      WRITE(IUNIT,1007)
C
C      WRITE(IUNIT,1011)
C      WRITE(IUNIT,1012)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/ /,A79)
 1001 FORMAT( / ,24('*'),' TABULAR OUTPUT FROM ',A28,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,24('*')/)
 1002 FORMAT( / ,19X,'TABLES OF COLLISIONAL-',
     & 'DIELECTRONIC RECOMBINATION/IONIZATION COEFFICIENTS AND',
     & ' IONIZATION BALANCE'/20X,95('-'))
 1003 FORMAT( / / / ,'PROGRAM RUN TITLE:',1X,A32)
 1004 FORMAT( / ,'ELEMENT NAME',5X,':',1X,A12,21X,
     &           'RECOMBINING ION CHARGE (Z1) RANGE:',1X,I2,' -> ',I2
     &        /)
 1005 FORMAT('OUTPUT TEMPERATURE & DENSITY VALUES:'/
     &        ,36('-')//
     &       ,'NUMBER OF ELECTRON TEMPERATURES = ',I2/
     &       ,'NUMBER OF ELECTRON DENSITIES    = ',I2)
 1006 FORMAT( / ,'INDEX',3X,10('-'),' ELECTRON  TEMPERATURES ',10('-'),
     &           10X,'INDEX',3X,4('-'),' ELECTRON DENSITIES ',4('-')/
     &        8X,'LOG10(kelvin)',5X,'(kelvin)',9X,'(eV)',24X,
     &           'LOG10(cm-3)',6X,'(cm-3)')
 1007 FORMAT(52('-'),10X,36('-'))
 1008 FORMAT( 2X,I2,5X,F8.2,1P,7X,2(D11.3,4X),0P,
     &       10X,I2,5X,F8.2,1P,6X,D11.3)
 1009 FORMAT( 2X,I2,5X,F8.2,1P,7X,2(D11.3,4X))
 1010 FORMAT(64X,I2,5X,F8.2,1P,6X,D11.3)
C 1011 FORMAT(/ // / ,'TABLE KEY:'/1H ,10('-')/
C     &  / ,'LOG(TE)',8X,'= LOG10( ELECTRON TEMPERATURE <kelvin> )'/
C     & 1H ,'LOG(NE)',8X,'= LOG10( ELECTRON DENSITY <electrons cm-3> )'/
C     & 1H ,'Z',14X,'= RECOMBINED  ION CHARGE'/
C     & 1H ,'Z1',13X,'= RECOMBINING ION CHARGE'/
C     &  / ,'LOG(A)',9X,'= LOG10( COLLISIONAL-DIELECTRONIC ',
C     &                 'RECOMBINATION COEFFICIENT <cm3 sec-1> )'/
C     & 1H ,'LOG(S)',9X,'= LOG10( COLLISIONAL-DIELECTRONIC ',
C     &                 'IONIZATION    COEFFICIENT <cm3 sec-1> )'/
C     & 1H ,'LOG(N(Z)/NTOT) = LOG10( IONIZATION BALANCE )')
C 1012 FORMAT( / / / ,13X,'NOTE: ',
C     &        'AN  ASTERISK (*)  IS USED IN THE  FOLLOWING  TABLES  TO'/
C     &1H ,19X,'IDENTIFY THE TEMPERATURES AND DENSITIES FOR WHICH:     '/
C     & / ,19X,'1) THE COLL.-DIEL. COEFFICIENTS HAD TO BE EXTRAPOLATED.'/
C     & / ,19X,'2) THE  IONIZATION  BALANCES  WERE   CALCULATED   USING'/
C     &1H ,19X,'   EXTRAPOLATED COLL.-DIEL. COEFFICIENTS.'/
C     & / ,19X,'IN THE CASE OF THE  COLL.-DIEL. COEFFICIENT TABLES  THE'/
C     &1H ,19X,'RECOMBINED ION CHARGES (Z) FOR WHICH  THE  COEFFICIENTS'/
C     &1H ,19X,'HAD  TO  BE  EXTRAPOLATED ARE  ALSO  IDENTIFIED  BY  AN'/
C     &1H ,19X,'ASTERISK.')
C
 1013 FORMAT('FILE TYPES       : ADF10 - STANDARD, ADF11 - STANDARD'/)
 1014 FORMAT('FILE TYPES       : ADF10 - PARTIAL RESOLVED, ADF11 - PARTI
     &AL UNRESOLVED'/)
 1015 FORMAT('FILE TYPES       : ADF10 - PARTIAL RESOLVED, ADF11 - PARTI
     &AL RESOLVED'/)
C-----------------------------------------------------------------------
C
      RETURN
      END
