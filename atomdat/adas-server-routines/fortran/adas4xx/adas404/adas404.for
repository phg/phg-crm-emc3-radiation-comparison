C UNIX-IDL PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/adas404.for,v 1.4 2004/07/06 10:39:40 whitefor Exp $ Date $Date: 2004/07/06 10:39:40 $
C
      PROGRAM ADAS404
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS404 **********************
C
C  ORIGINAL NAME: EION3
C
C  VERSION: 1.0
C
C  PURPOSE:
C           TO FETCH DATA FROM MASTER CONDENSED COLLISIONAL DIELECTRONIC
C           FILES AND PREPARE ELEMENT MASTER FILES IF REQUIRED.
C
C           TABLES AND GRAPHS ARE GENERATED OF  COLLISIONAL-DIELECTRONIC
C           COEFFICIENTS (RECOMBINATION AND IONISATION)  AND  IONISATION
C           BALANCE AT SPECIFIED DENSITIES.
C           (UP TO EIGHT DENSITIES CAN BE OUTPUT IN TABULAR  FORM,   AND
C           TWO OF THESE CAN ALSO BE OUTPUT AS A SERIES OF 3 GRAPHS)
C
C           THE MAIN BODY OF THIS PROGRAM DEALS WITH CONVERSION OF
C           STANDARD FILE TYPES TO STANDARD FILE TYPES. IF RESOLVED TO
C           UNRESOLVED OR RESOLVED TO RESOLVED ARE REQUESTED THEN THE
C           ROUTINES LH404RU AND LH404RR ARE CALLED RESPECTIVELY.
C
C  DATA:    SEE SUBROUTINE D4DATA FOR DESCRIPTION OF INPUT/OUTPUT FILES.
C
C
C  PROGRAM:
C
C          (I*4)  NDZ     = PARAMETER = NUMBER OF CHARGE STATES FOR
C                           COLL-DIEL COEFF AND IONISATION BALANCE ARRAY
C          (I*4)  NDTIN   = PARAMETER = MAX NO. OF INPUT TEMPERATURES
C          (I*4)  NDDEN   = PARAMETER = MAX NO. OF INPUT DENSITIES
C          (I*4)  L0      = PARAMETER = 0
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L5      = PARAMETER = 5
C          (I*4)  L8      = PARAMETER = 8
C          (I*4)  IUNIT   = PARAMETER = OUTPUT UNIT NUMBER FOR TABLES: 7
C          (I*4)  IUNT20  = PARAMETER = OUTPUT UNIT NO. FOR JCL DATA: 20
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80
C                           TAKES NUMBERS AS BEING ZERO = 1.0E-36
C
C          (C*2)  YEAR    = REFERENCE YEAR (ABBREVIATED) OF INPUT MASTER
C                           CONDENSED COLL.-DIELECTRONIC COEFFTS. FILE.
C          (C*6)  UIDIN   = PROJECT UID FOR INPUT CONDENSED DATA FILE
C          (C*6)  USERID  = USER ID FOR OUTPUT INC. ELEMENT MASTER FILE
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) ELEMT   = NAME OF ELEMENT UNDER CONSIDERATION.
C          (C*32) TITLE   = IDENTIFYING TITLE FOR PROGRAM RUN (VIA ISPF)
C          (C*80) TITLF   = PROGRAM GENERATED TITLE CONTAINING; ELEMENT
C                           NAME, ION-CHARGE RANGE, DENSITY.
C          (C*80) CADAS   = ADAS HEADER (REQUIRED FOR BATCH RUNNING)
C          (C*80) FOUT()  = OUTPUT FORMATS FOR COLL-DIEL COEFF. TABLES.
C          (C*16) STRG1   = DENSITY HEADER FOR ION.-BALANCE TABLE.
C          (C*133)STRG2   = COLUMN HEADER FOR ION.-BALANCE TABLE
C          (C*133)STRG3() = FORMAT FOR ION.-BALANCE TABLE.
C
C          (L*4)  LPEND   = .TRUE.  => END      PROGRAM EXECUTION
C                         = .FALSE. => CONTINUE PROGRAM EXECUTION
C          (L*4)  LBKJOB  = .TRUE.  => FOREGROUND: BATCH JOB REQUESTED
C                         = .FALSE. => FOREGROUND: NO BATCH JOB REQUESTD
C          (L*4)  LTAB    = .TRUE.  => FOREGROUND: BATCH TABLE OUTPUT
C                         = .FALSE. => FOREGROUND: NO BATCH TABLE OUTPUT
C          (L*4)  TABOUT  = .TRUE.  => TABULAR OUTPUT REQUIRED
C                           .FALSE. => NO TABLES REQUIRED
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT GENERATED
C                           .FALSE. => NO GHOST80 OUTPUT GENERATED
C          (L*4)  SELTAB  = .TRUE.  => PREPARE ELEMENT MASTER FILE
C                           .FALSE. => DO NOT PREPARE ELEM. MASTER FILE
C          (L*4)  REPTAB  = .TRUE.  => REPLACE EXISTING ELEMENT MASTER
C                                      FILES.
C                         = .FALSE. => DO NOT REPLACE EXISTING ELEMENT
C                                      MASTER FILES.
C                           ('REPTAB' IS IGNORED IF 'SELTAB'=.FALSE.)
C          (L*4)  BATCH   = .TRUE.  => PROGRAM BEING RUN IN BACKGROUND
C                         = .FALSE. => PROGRAM BEING RUN IN FOREGROUND
C          (L*4)  LGTXT   = .TRUE.  => LAST SCREEN DISPLAY WAS TEXT DUMP
C                         = .FALSE. => LAST SCREEN DISPLAY WAS GHOST80
C                                      (USED IN FOREGROUND MODE ONLY)
C
C          (I*4)  I       = GENERAL USE
C          (I*4)  K       = GENERAL USE
C          (I*4)  IZE1    = LOWEST ION CHARGE
C          (I*4)  IZE2    = HIGHEST ION CHARGE  (NB. EXCLUDING THE  BARE
C                           NUCLEUS - ONE MORE STAGE IS BROUGHT IN AUTO-
C                           MATICALLY IN THE IONISATION BALANCE)
C          (I*4)  IZ0     = ELEMENT NUCLEAR CHARGE
C          (I*4)  IZ1     = MINIMUM ALLOWED IONIC CHARGE + 1
C          (I*4)  IZ2     = MAXIMUM ALLOWED IONIC CHARGE + 1
C          (I*4)  NEL1    = NUMBER OF ELECTRONS IN STATE 'IZ1'
C          (I*4)  NEL2    = NUMBER OF ELECTRONS IN STATE 'IZ2'
C          (I*4)  MAXD    = NUMBER OF DENSITIES (< 31)
C          (I*4)  MAXT    = NUMBER OF TEMPERATURES (< 56)
C          (I*4)  ITTYP   = UNITS OF USER ENTERED TEMPERATURES 'TIN()'
C                           1 => KELVIN
C                           2 => EV
C          (I*4)  MAX     = MAXIMUM NUMBER OF CHARGES FOR COLL-DIEL COEF
C          (I*4)  MAX1    = MAXIMUM NUMBER OF CHARGES FOR ION.-BALANCE
C                           ('MAX1' + 1)
C          (I*4)  IN      = ARRAY SUBSCRIPT USED FOR 'KDTAB()'.
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR ION CHARGE VALUES
C          (I*4)  IZTAB   = MAXIMUM NUMBER OF CHARGES PRINTED ACROSS
C                           THE PAGE FOR THE COLL-DIEL TABLES.
C          (I*4)  IBLKPG  = NUMBER OF OUPUT TABLE BLOCKS PER PAGE
C          (I*4)  NPAGES  = NO. OF PAGES REQUIRED FOR COLL-DIEL TABLES.
C          (I*4)  IULIM   = NO. OF OUTPUT BLOCKS PER DENSITY FOR ION.-
C                           BLANCE TABLES (=1,2 OR 3)
C          (I*4)  IULEN() = NUMBER OF CHARGES IN A GIVEN ION.-BALANCE
C                           OUTPUT BLOCK. (ARRAY SIZE = 3).
C                           (IULEN(IULIM) = NO. CHARGES IN BLOCK IULIM)
C          (R*4)  XMAX4   = REAL('XMAX')
C          (R*4)  XMIN4   = REAL('XMIN')
C
C          (R*8)  XMAX    = UPPER LIMIT FOR GRAPH X-AXIS (TEMPERATURE)
C          (R*8)  XMIN    = LOWER LIMIT FOR GRAPH X-AXIS (TEMPERATURE)
C          (R*8)  YMAX    = UPPER LIMIT FOR GRAPH Y-AXIS (COEFF ETC.)
C          (R*8)  YMIN    = LOWER LIMIT FOR GRAPH Y-AXIS (COEFF ETC.)
C          (R*8)  YLIM    = LOWEST LIMIT ALLOWED FOR GRAPH Y-AXIS
C          (R*8)  DENSA() = SET OF 'MAXD' ELECTRON DENSITIES (CM-3)
C                           (USER ENTERED VALUES)
C          (R*8)  TIN()   = SET OF 'MAXT' ELECTRON TEMPERATURES (USER
C                           ENTERED VALUES) - UNITS GIVEN BY 'ITTYP'
C          (R*8)  T()     = SET OF 'MAXT' ELECTRON TEMPERATURES (USER
C                           ENTERED VALUES) - UNITS KELVIN.
C                           (='TIN()' CONVERTED TO KELVIN)
C          (R*8)  DENSL() = LOG10('DENSA()')
C          (R*8)  TL()    = LOG10('T()')
C          (R*8)  ACDL(,,)= INITIALLY: LOG10(RECOMB. COLL-DIEL COEFF)
C                           THEN     : LOG10(IONISATION-BALANCE)
C                              1ST ARRAY DIMENSION = ION CHARGE/STAGE
C                              2ND ARRAY DIMENSION = TEMPERATURE
C                              3RD ARRAY DIMENSION = DENSITY
C          (R*8)  SCDL(,,)= INITIALLY: LOG10(IONIS. COLL-DIEL COEFF)
C                           THEN     : IONISATION-BALANCE
C                              1ST ARRAY DIMENSION = ION CHARGE/STAGE
C                              2ND ARRAY DIMENSION = TEMPERATURE
C                              3RD ARRAY DIMENSION = DENSITY
C          (R*8)  ATTY(,) = WORKING SPACE FOR 3-WAY SPLINE ITERPOLATION
C                           (STORES LOG10 (INTERPOLATED VALUES))
C                           1ST DIMENSION: TEMPERATURE
C                           2ND DIMENSION: DENSITY
C                           ( USED IN SUBROUTINE 'D4DATA' ONLY )
C
C          (L*4)  LDIBAL() =.TRUE. => IONIZATION BALANCES FOR DENSITY
C                                     INVOLVE NOT EXTRAPOLATION.
C                          =.FALSE.=> IONIZATION BALANCES FOR DENSITY
C                                     INVOLVE EXTRAPOLATION  IN  SOME
C                                     PART OF THEIR CALCULTION.
C                           1ST DIMENSION: DENSITY INDEX
C          (L*4)  LTIBAL() =.TRUE. => IONIZATION BALANCES FOR TEMP'TURE
C                                     INVOLVE NOT EXTRAPOLATION.
C                          =.FALSE.=> IONIZATION BALANCES FOR TEMP'TURE
C                                     INVOLVE EXTRAPOLATION  IN  SOME
C                                     PART OF THEIR CALCULTION.
C                           1ST DIMENSION: TEMPERATURE INDEX
C          (L*4)  LZACDL() = .TRUE.  =>'ACDL(,,,)'  VALUES  FOR  CHARGE-
C                                      STATE INTERPOLATED.
C                          = .FALSE. =>'ACDL(,,,)'  VALUES  FOR  CHARGE-
C                                      STATE EXTRAPOLATED.
C                           1ST DIMENSION: CHARGE-STATE INDEX
C          (L*4)  LZSCDL() = .TRUE.  =>'SCDL(,,,)'  VALUES  FOR  CHARGE-
C                                      STATE INTERPOLATED.
C                          = .FALSE. =>'SCDL(,,,)'  VALUES  FOR  CHARGE-
C                                      STATE EXTRAPOLATED.
C                           1ST DIMENSION: CHARGE-STATE INDEX
C          (L*4)  LTACDL(,)= .TRUE.  =>'ACDL(,,,)' VALUE FOR TEMPERATURE
C                                      INDEX AND CHARGE INTERPOLATED.
C                          = .FALSE. =>'ACDL(,,,)' VALUE FOR TEMPERATURE
C                                      INDEX AND CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: TEMPERATURE INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C          (L*4)  LDACDL(,)= .TRUE.  => 'ACDL(,,,)' VALUE FOR DENSITY
C                                       INDEX AND CHARGE INTERPOLATED.
C                          = .FALSE. => 'ACDL(,,,)' VALUE FOR DENSITY
C                                       INDEX AND CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: DENSITY INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C          (L*4)  LTSCDL(,)= .TRUE.  =>'SCDL(,,,)' VALUE FOR TEMPERATURE
C                                      INDEX AND CHARGE INTERPOLATED.
C                          = .FALSE. =>'SCDL(,,,)' VALUE FOR TEMPERATURE
C                                      INDEX AND CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: TEMPERATURE INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C          (L*4)  LDSCDL(,)= .TRUE.  => 'SCDL(,,,)' VALUE FOR DENSITY
C                                       INDEX AND CHARGE INTERPOLATED.
C                          = .FALSE. => 'SCDL(,,,)' VALUE FOR DENSITY
C                                       INDEX AND CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: DENSITY INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C
C          (I*4)   IRESO    = 0 IF STANDARD -> STANDARD CONVERSION
C                             1 IF RESOLVED -> UNRESOLVED
C                             2 IF RESOLVED -> RESOLVED
C
C          (C*80) DSNO()  = OUPUT MASTER FILE NAME FOR EACH DATA TYPE
C
C          (C*80) DSNIN(,)= INPUT FILE NAME FOR EACH DATA TYPE AND
C                           CHARGE
C NOTE:
C
C          THE PROGRAM INCLUDES ONE FURTHER STAGE IN THE ION.-BALANCE.
C          THUS THE LARGEST 'IZE2' FOR A GIVEN ELEMENT IS THE HYDROGEN-
C          LIKE STAGE.
C
C          GRAPHS CAN ONLY BE PLOTTED FOR DENSITIES WHICH ARE ALSO
C          CHOSEN FOR TABULAR OUTPUT. IF THIS IS NOT THE CASE THEN
C          NO GRAPH WILL BE GENERATED.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D4ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          D4BTCH     ADAS      GATHERS USERS VALUES VIA BATCH JOB JCL
C          D4ZNEL     ADAS      SETS MIN & MAX ION CHARGES/ELECTRON NOS.
C          D4DATA     ADAS      FETCHES MASTER CONDENSED COLL-DIEL FILES
C                               CALCULATES INTERPOLATED COLL-DIEL COEFFS
C                               PREPARES ELEMENT MASTER FILE IF REQUIRED
C          D4OUT0     ADAS      HEADING OUTPUT AND KEY FOR TABLES
C          D4SET1     ADAS      SETS UP COLL-DIEL OUTPUT TABLE FORMAT
C          D4OUT1     ADAS      OUTPUT OF COLL-DIEL TABLE
C          D4LIMY     ADAS      SETS UP Y-AXIS RANGE FOR GRAPH
C          D4OUTG     ADAS      OUTPUTS GRAPH VIA GHOST80.
C          D4IBAL     ADAS      CALCULATES IONISATION BALANCE VALUES
C          D4LBAL     ADAS      IDENTIFY EXTRAPOLATED ION. BALANCES
C          D4SET2     ADAS      SETS UP ION.-BAL. OUTPUT TABLE FORMAT
C          D4OUT2     ADAS      OUTPUT OF ION.-BAL. TABLE
C          D4SPF1     ADAS      SETS UP JCL FOR BATCH JOB USING ISPF
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXTCON     ADAS      CONVERSION OF TEMPERATURE ARRAY UNITS
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               GATHERS ELEMENT NAME FOR INPUT Z0
C          XXLIM8     ADAS      SETS UP X-AXIS (TEMP.) RANGE FOR GRAPH
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C                               AND CLEAR SCREEN (FOREGROUND USE)
C          GREND      GHOST80   CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          ZA06CS     HSL       READ INFO. FROM JCL //EXEC PARM.G FIELD
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    08/10/90
C
C UPDATE:  17/01/91 - PE BRIDEN: ADAS91 - RENAMED SUBROUTINES AS FOLLOWS
C                                         'D4TAB0' -> 'D4OUT0'
C                                         'D4TAB1' -> 'D4OUT1'
C                                         'D4TAB2' -> 'D4OUT2'
C
C UPDATE:  25/01/91 - PE BRIDEN - ADAS91 - INTRODUCED 'TIN()'
C
C UPDATE:  28/01/91 - PE BRIDEN - ADAS91 - ADDED 'CADAS' AS ARGUMENT TO
C                                          'D4BTCH','D4OUT0' & 'D4OUTG'
C
C UPDATE:  29/01/91 - PE BRIDEN - ADAS91 - ADDED   'T()','TL()','MAXT',
C                                          'DENSA()','DENSL()' & 'MAXD'
C                                          AS ARGUMENTS TO 'D4OUT0'.
C                                        - INTRODUCED: 'TLACDL(,)' ,
C                                                      'TDACDL(,)' ,
C                                                      'TLDCDL(,)' &
C                                                      'TDSCDL(,)'
C                                        - REORDERED AND AMENDED D4DATA
C                                          ARGUMENT LIST.
C                                        - REORDERED AND AMENDED D4OUT1
C                                          ARGUMENT LIST.
C                                        - REORDERED AND AMENDED D4OUT2
C                                          ARGUMENT LIST.
C
C UPDATE:  30/01/91 - PE BRIDEN - ADAS91 - INTRODUCED 'TZACDL & TZSCDL'
C                                        - INTRODUCED 'LDIBAL & LTIBAL'
C                                        - INTRODUCED ROUTINE 'D4LBAL'
C
C UPDATE:  13/02/91 - PE BRIDEN - ADAS91 - REPLACED XXELEM WITH XFELEM
C
C UPDATE:  20/03/91 - PE BRIDEN - ADAS91 - 'DENSA()' ADDED TO ARGUMENT
C                                          LIST OF 'D4DATA'.
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C VERSION: 1.2				DATE: 21-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- ADDED PIPE FLUSH AFTER END OF CALCULATION SIGNAL
C
C MODIFIED: KEITH NORMAN (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED IF (LDTYP(J).EQ.1) TO IF(LDTYP(J))
C
C VERSION: 1.3				DATE: 20-10-97
C MODIFIED: LORNE HORTON (JET)
C       - REMOVED YEAR SPECIFIC INFO FROM D4ZNEL (IZ3, NEL3, NIND, NSET)
C       - CHANGED ARGUMENTS TO LH404RU/LH404RR FROM RECOMBINED CHARGE
C         TO RECOMBINING CHARGE
C       - ADDED CODE TO RESTORE CHARACTER PREFIX FOR OUTPUT
C
C VERSION:  1.4                         DATE: 4/11/99
C MODIFIED: Martin O'Mullane
C           - Add real name of user via XXNAME.
C           - Modify comments.
C           - Remove blocks of 0.0 from xcd and qcd output.
C           - Parse name of a selected output class to determine
C             the prefix. If we select resolved then met files do
C             not exist.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER NDZ      , NDDEN    , NDTIN
      INTEGER L0       , L1       , L2      , L5     , L8
      INTEGER IUNIT    , IUNT20   , PIPEOU  , PIPEIN , IUNT17
C-----------------------------------------------------------------------
      REAL*4  GHZERO
C-----------------------------------------------------------------------
      PARAMETER( NDZ   = 50 , NDTIN   = 55 , NDDEN = 30 )
      PARAMETER( L0    =  0 , L1      =  1 , L2     =  2, 
     &           L5    =  5 , L8      = 8  )
      PARAMETER( IUNIT =  17, IUNT20  = 20 , PIPEOU = 6, PIPEIN = 5)
      PARAMETER( IUNT17= 17)
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 )
C-----------------------------------------------------------------------
      CHARACTER    XFELEM*12
      CHARACTER    YEAR*2   , DATE*8   , UIDIN*6  , USERID*6
      CHARACTER    ELEMT*12 , TITLE*32 ,
     &             CADAS*80 , USER*30
      CHARACTER    CHPRFIX*2
      CHARACTER    DSNO(10)*80 , DSNIN(50,10)*80 , SAVFIL*80
      CHARACTER    STRING*80
C-----------------------------------------------------------------------
      LOGICAL      SELTAB   , REPTAB     ,
     &             LGHOST   , LPEND      ,
     &             LBKJOB   , LTAB       ,
     &             OPEN17
C-----------------------------------------------------------------------
      INTEGER I        , J        , K
      INTEGER M1       , M2
      INTEGER IZE1     , IZE2     , IZ0     , IZ1    , IZ2    ,
     &        NEL1     , NEL2
      INTEGER MAXD     , MAXT     , ITTYP   , MAX    , MAX1
      INTEGER ID       , IT       , IZ
      INTEGER IRESO    , ONE
C-----------------------------------------------------------------------
      REAL*8  DENSA(NDDEN)          , TIN(NDTIN)            , T(NDTIN)
      REAL*8  DENSL(NDDEN)          , TL(NDTIN)
      REAL*8  ATTY(NDTIN,NDDEN)
      REAL*8  ACDL(NDZ,NDTIN,NDDEN)
C-----------------------------------------------------------------------
      LOGICAL LDIBAL(NDDEN)         ,
     &        LZACDL(NDZ)           ,
     &        LDACDL(NDDEN,NDZ)     ,
     &        LTACDL(NDTIN,NDZ)     ,
     &        LDTYP(9)
C-----------------------------------------------------------------------
      DATA   LGHOST/.FALSE./ , LBKJOB/.FALSE./,
     &       LTAB  /.FALSE./ , OPEN17/.FALSE./
      DATA   CADAS/' '/, CHPRFIX/'  '/, ONE/1/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM *****************************
C-----------------------------------------------------------------------
CXC
CXC-----------------------------------------------------------------------
CXC IDENTIFY WHETHER PROGRAM IS BEING RUN IN FOREGROUND OR BACKGROUND
CXC-----------------------------------------------------------------------
CXC
CX      CALL ZA06CS (L5,'BATCH',BATCH)
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
      CALL XXNAME(USER)
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE ( DATE )
C
C-----------------------------------------------------------------------
C WRITE ARRAY DIMENSIONS TO IDL
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*)NDTIN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDDEN
      CALL XXFLSH(PIPEOU)

C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL)
C-----------------------------------------------------------------------
C
 1    CONTINUE
      CALL D4ISPF ( LPEND  , LTAB  ,
     &     NDDEN , NDTIN  ,
     &     TITLE , UIDIN  , USERID ,
     &     IZ0   , IZE1   , IZE2   ,
     &     MAXD  , MAXT   , ITTYP  ,
     &     DENSA , TIN    ,
     &     YEAR  , SELTAB , REPTAB ,
     &     IRESO  ,
     &     SAVFIL, DSNIN  , DSNO   ,
     &     LDTYP
     &                  )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 999
C
C-----------------------------------------------------------------------
C OPEN PAPER.TXT IF APPROPRIATE
C-----------------------------------------------------------------------
C
      IF(LTAB.AND.(.NOT.OPEN17))THEN
         OPEN(UNIT=IUNT17, FILE=SAVFIL, STATUS = 'UNKNOWN')
         OPEN17=.TRUE.
      ENDIF
C
C-----------------------------------------------------------------------
C IF INPUT TEMPERATURE UNITS ARE NOT KELVIN THEN CONVERT TO KELVIN
C-----------------------------------------------------------------------
C
      CALL XXTCON( ITTYP , L1 , L0 , MAXT , TIN , T )
C
C-----------------------------------------------------------------------
C FETCH NAME OF ELEMENT WITH NUCLEAR CHARGE IZ0
C-----------------------------------------------------------------------
C
      ELEMT = XFELEM( IZ0 )
C
C-----------------------------------------------------------------------
C SETUP MAXIMUM AND MINIMUM IONIC CHARGE + 1 AND NUMBER OF ELECTRONS
C-----------------------------------------------------------------------
C
      CALL D4ZNEL ( IZ1  , IZ2  ,
     &              NEL1 , NEL2 ,
     &              IZ0  , IZE1 , IZE2
     &            )
C
C-----------------------------------------------------------------------
C CALCULATE LOG BASE 10 VALUES OF INPUT TEMPERATURES AND DENSITIES
C-----------------------------------------------------------------------
C
      DO 2 IT=1,MAXT
        TL(IT)=DLOG10(T(IT))
    2 CONTINUE
      DO 3 ID=1,MAXD
        DENSL(ID)=DLOG10(DENSA(ID))
    3 CONTINUE

C-----------------------------------------------------------------------
C PARSE NAME OF FIRST MET FILE TO FIND IF A PREFIX IS BEING USED
C
C This is stupid if we have chosen resolved as met files are not used.
C Use the first valid type selected instead.
C-----------------------------------------------------------------------

      STRING = DSNIN(1,10)
      do i = 1, 10
        if (ldtyp(i)) then 
           string = DSNIN(1,i)
           goto 9
        endif
      end do
  9   continue
        
      CALL XXSLEN(STRING,M1,M2)
      M1 = INDEX(STRING,'_')
      M2 = INDEX(STRING,'#')
      if (m1.LT.m2) then
         CHPRFIX = '  '
      else
         CALL XXSLEN(STRING,M1,M2)
         M2 = INDEX(STRING,'#')
         CHPRFIX = STRING(M2-2:M2)
      endif 

C-----------------------------------------------------------------------
C OUTPUT TABLE MAIN HEADING IF REQUIRED
C-----------------------------------------------------------------------
C
      IF (LTAB.AND.OPEN17) THEN
         CALL D4OUT0 ( IUNIT , IZ1   , IZ2   ,
     &        DATE  , ELEMT , TITLE ,
     &        T     , TL    , MAXT  ,
     &        DENSA , DENSL , MAXD  ,
     &        CADAS , IRESO
     &        )
      ENDIF
C
C-----------------------------------------------------------------------
C CALL APPROPRIATE ROUTINE IF INPUT DATA IS RESOLVED
C-----------------------------------------------------------------------
C
      IF(IRESO.GT.0)THEN

         IF(IRESO.EQ.1)THEN
            CALL LH404RU(DATE  , USER  ,
     &                   IZ0   , IZ1   , IZ2   , CHPRFIX , 
     &                   MAXT  , MAXD  , T     , DENSA   ,
     &                   DSNIN , DSNO  , LDTYP , YEAR    , OPEN17
     &                  )
         ELSEIF(IRESO.EQ.2)THEN
            CALL LH404RR(DATE  , USER  ,
     &                   IZ0   , IZ1   , IZ2   , CHPRFIX ,
     &                   MAXT  , MAXD  , T     , DENSA   ,
     &                   DSNIN , DSNO  , LDTYP , YEAR    , OPEN17
     &                  )
         ENDIF
C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE FINISHED CALCULATING SUCCESSFULLY
C----------------------------------------------------------------------
         WRITE(PIPEOU,*)ONE
         CALL XXFLSH(PIPEOU)         
         GOTO 1

      ENDIF
C
C-----------------------------------------------------------------------
C FETCH MASTER CONDENSED COLLISIONAL ISOELECTRONIC FILES,
C OBTAIN INTERPOLATED COLL-RAD. RECOMB. AND IONIS. COEFFTS,
C AND PREPARE ELEMENT MASTER FILE IF REQUIRED.
C-----------------------------------------------------------------------
C
C LOOP OVER FIRST FIVE MASTER CLASSES
      DO 57 J=1,5
         IF(LDTYP(J))THEN
            CALL D4DATA ( TITLE , DATE  , UIDIN  , USERID ,
     &                    J     , L0    , YEAR   , SELTAB , REPTAB ,
     &                    NDZ   , NDDEN , NDTIN  ,
     &                    IZ0   , IZ1   , NEL1   , IZ2    , NEL2   ,
     &                    T     , TL    , MAXT   ,
     &                    DENSA , DENSL , MAXD   ,
     &                    ATTY  , ACDL  , LZACDL , LDACDL , LTACDL ,
     &                    DSNIN , DSNO  , OPEN17
     &                  )
         ENDIF
 57   CONTINUE
C AND LAST TWO - NUMBERING IN D4DATA IS DIFFERENT: PLT AND PLS ARE 6 AND 7
C RATHER THAN 8 AND 9 AS THEY ARE IN LORNE'S CODES, LH404RU AND LH404RR,
C HENCE J-2 IN ARGUMENT LIST
      DO 58 J=8,9
         IF(LDTYP(J))THEN
            CALL D4DATA ( TITLE , DATE  , UIDIN  , USERID ,
     &                    J-2    , L0    , YEAR   , SELTAB , REPTAB ,
     &                    NDZ   , NDDEN , NDTIN  ,
     &                    IZ0   , IZ1   , NEL1   , IZ2    , NEL2   ,
     &                    T     , TL    , MAXT   ,
     &                    DENSA , DENSL , MAXD   ,
     &                    ATTY  , ACDL  , LZACDL , LDACDL , LTACDL ,
     &                    DSNIN , DSNO  , OPEN17
     &                  )
         ENDIF
 58   CONTINUE
C----------------------------------------------------------------------
      MAX =IZ2-IZ1+1
      MAX1=MAX+1

C----------------------------------------------------------------------
C TELL IDL THAT WE HAVE FINISHED CALCULATING SUCCESSFULLY
C----------------------------------------------------------------------
      WRITE(PIPEOU,*)ONE
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C  RETURN TO IDL INPUT.
C-----------------------------------------------------------------------
C
      GOTO 1
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(' ADAS404 -  ELEMENT: ',A12,6X,'Z1 = ',I2,' -> ',I2,4X,
     &       'DENSITY = ',1PD9.2,' CM-3')
 1001 FORMAT(/ /,A79)
C
C-----------------------------------------------------------------------
C
  999 END
