CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4outf.for,v 1.1 2004/07/06 13:14:23 whitefor Exp $ Date $Date: 2004/07/06 13:14:23 $
CX
      SUBROUTINE D4OUTF ( DSNOUT, TITLE , DATE   , REPTAB ,
     &                    NDZ   , NDDEN , NDTIN  ,
     &                    IZ0   , IZ1   , IZ2    ,
     &                    MAXD  , MAXT  ,
     &                    DENSL , TL    ,
     &                    ARRAY
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4OUTF *********************
C
C  PURPOSE: TO DYNAMICALLY OPEN AND OUTPUT ELEMENT MASTER FILE
C
C  CALLING PROGRAM: D4DATA
C
C  DATA:
C           THE OUTPUT ELEMENT MASTER DATA IS A SEQUENTIAL FILE WHICH
C           IS NAMED AS FOLLOWS:
C
C             1. JETUID.ACD<YR>#<EL>.DATA
C             2. JETUID.SCD<YR>#<EL>.DATA
C             3. JETUID.CCD<YR>#<EL>.DATA
C             4. JETUID.PRB<YR>#<EL>.DATA
C             5. JETUID.PRC<YR>#<EL>.DATA
C             6. JETUID.PLT<YR>#<EL>.DATA
C             7. JETUID.PLS<YR>#<EL>.DATA
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C           IF <YR> IS BLANK THEN THE CURRENT RECOMMENDED DATA SETS ARE
C           USED
C
C           <EL> IS THE ELEMENT SYMBOL
C
C           THE PARTICULAR TYPE OPENED (1-7) IS SELECTED BY 'ISWIT'
C           (SEE SUBROUTINE 'D4DATA')
C
C  SUBROUTINE:
C
C  INPUT : (C*80) DSNOUT  = OUTPUT ELEMENT MASTER FILE DATA SET NAME
C  INPUT : (C*32) TITLE   = USER ENTERED PROGRAM RUN TITLE.
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C  OUTPUT: (L*4)  REPTAB  = .TRUE.  => REPLACE EXISTING ELEMENT MASTER
C                                      FILES.
C                         = .FALSE. => DO NOT REPLACE EXISTING ELEMENT
C                                      MASTER FILES.
C
C  INPUT : (I*4)  NDZ      = NUMBER OF CHARGE STATES
C  INPUT : (I*4)  NDDEN    = MAXIMUM NUMBER OF INPUT DENSITIES
C  INPUT : (I*4)  NDTIN    = MAXIMUM NUMBER OF INPUT TEMPERATURES
C
C  INPUT : (I*4)  IZ0      = ELEMENT NUCLEAR CHARGE
C                                         (DETERMINES OUTPUT FILE NAME)
C  INPUT : (I*4)  IZ1      = MINIMUM ALLOWED IONIC CHARGE + 1
C                            (ACCORDING TO AVAILABLE NO. OF SEQUENCES
C                             STORED IN FILES FOR 'YEAR')
C  INPUT : (I*4)  IZ2      = MAXIMUM ALLOWED IONIC CHARGE + 1
C
C  INPUT : (I*4)  MAXD     = NUMBER OF USER SUPPLIED DENSITIES (<=NDDEN)
C  INPUT : (I*4)  MAXT     = NUMBER OF USER SUPPLIED TEMPS. (<=NDTIN)
C
C  INPUT : (R*8)  DENSL()  = LOG10 ( SET OF 'MAXD' ELECTRON DENSITIES
C                                                              (CM-3) )
C  INPUT : (R*8)  TL()     = LOG10 ( SET OF 'MAXT' ELECTRON TEMPERATURES
C                                                             (KELVIN) )
C
C  INPUT : (R*8)  ARRAY(,,)= LOG10(ELEMENT COLL.-DIEL COEFF INTERPOLATED
C                                  DATA)
C                             1ST ARRAY DIMENSION - ION CHARGE/STAGE
C                             2ND ARRAY DIMENSION - TEMPERATURE
C                             3RD ARRAY DIMENSION - DENSITY
C
C          (L)    LEXIST  = .TRUE.  => REQUESTED OUTPUT FILE EXISTS
C                         = .FALSE. => REQUESTED FILE DOES NOT EXIST
C
C          (C*12) XFELEM   = FUNCTION (SEE ROUTINES SECTION BELOW)
C          (C*12) ENAME    = ELEMENT NAME
C
C          (I*4)  I4UNIT   = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IZF      = ELEMENT RECOMBINING ION CHARGE
C          (I*4)  ID       = ARRAY SUBSCRIPT USED FOR DENSITY VALUES
C          (I*4)  IT       = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IZ       = ARRAY SUBSCRIPT USED FOR ION CHARGE VALUES
C          (I*4)  IRCODE   = RETURN CODE FROM IBM SUBROUTINE 'FILEINF'
C
C
C NOTE:
C          STREAM HANDLING:
C                  STREAM 13 IS USED FOR WRITING  ELEMENT  MASTER FILES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          D4TLOG     ADAS      CONVERTS LOG10(TEMPERATURE) ARRAY FROM:
C                                     KELVIN <-> ELECTRON VOLTS
C          XFELEM     ADAS      CHATACTER*12 FUNCTION -
C                               ELEMENT NAME FOR GIVEN NUCLEAR CHARGE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          FILEINF    IBM       SETS UP CHARACTERISTICS BEFORE AN OPEN
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    26/10/90
C
C DATE:    13/02/91 - PE BRIDEN - ADAS91 - REPLACED XXELEM WITH XFELEM
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER XFELEM*12
      CHARACTER DSNOUT*80 , TITLE*32   , DATE*8   , ENAME*12
C----------------------------------------------------------------------
      LOGICAL   REPTAB    , LEXIST
C----------------------------------------------------------------------
      INTEGER   I4UNIT
      INTEGER   NDZ       , NDDEN      , NDTIN
      INTEGER   IZ0       , IZ1        , IZ2      ,
     &          MAXD      , MAXT
      INTEGER   IZF       , ID         , IT       , IZ    , IRCODE
C----------------------------------------------------------------------
      REAL*8    DENSL(NDDEN) , TL(NDTIN) , ARRAY(NDZ,NDTIN,NDDEN)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ESTABLISH WHETHER REQUESTED DATA SET EXISTS
C-----------------------------------------------------------------------
C
      INQUIRE(FILE=DSNOUT,EXIST=LEXIST)
C
C***********************************************************************
C IF FILE EXISTS AND IS NOT TO BE REPLACED NOTIFY USER AND SKIP OUTPUT
C***********************************************************************
C
         IF (LEXIST.AND.(.NOT.REPTAB)) THEN
            WRITE(I4UNIT(-1),1000) DSNOUT
            WRITE(I4UNIT(-1),1001)
            WRITE(I4UNIT(-1),1003)
         ELSE
C
C***********************************************************************
C OUTPUT FILE IS TO BE WRITTEN REPLACING EXISTING FILE IF NECESSARY
C***********************************************************************
C
               IF (LEXIST) THEN
                  WRITE(I4UNIT(-1),1000) DSNOUT
                  WRITE(I4UNIT(-1),1002)
                  WRITE(I4UNIT(-1),1003)
               ENDIF
C
C-----------------------------------------------------------------------
C  CONVERT LOG10(TREMPERATURES) FROM KELVIN TO EV FOR OUTPUT
C-----------------------------------------------------------------------
C
            CALL D4TLOG ( 1, MAXT, TL )
C
C-----------------------------------------------------------------------
C  GATHER ELEMENT NAME
C-----------------------------------------------------------------------
C
            ENAME = XFELEM( IZ0 )
C
C
C-----------------------------------------------------------------------
C  DYNAMICALLY OPEN OUTPUT MASTER FILE AND WRITE RESULTS
C-----------------------------------------------------------------------
C
CX            CALL FILEINF(IRCODE,'TRK',10,'SECOND',5,'RECFM','FB',
CX     &                   'LRECL',80,'BLKSIZE',4800)
            OPEN(UNIT=13,FILE=DSNOUT,STATUS='UNKNOWN')
C-----------------------------------------------------------------------
            WRITE(13,1004) IZ0 , MAXD , MAXT , IZ1 , IZ2 , ENAME , TITLE
            WRITE(13,1005)
            WRITE(13,1006) (DENSL(ID), ID=1,MAXD)
            WRITE(13,1006) (TL(IT),    IT=1,MAXT)
            IZ=0
               DO 1 IZF=IZ1,IZ2
                  IZ=IZ+1
                  WRITE(13,1007) IZF , DATE
                     DO 2 IT=1,MAXT
                  WRITE(13,1006) ( ARRAY(IZ,IT,ID), ID=1,MAXD )
    2                CONTINUE
    1          CONTINUE
C
C-----------------------------------------------------------------------
C  CONVERT LOG10(TREMPERATURES) BACK FROM EV TO KELVIN FOR RETURN
C-----------------------------------------------------------------------
C
            CALL D4TLOG ( 2, MAXT, TL )
C
C-----------------------------------------------------------------------
C  CLOSE STREAM 13
C-----------------------------------------------------------------------
C
            CLOSE(13)
C-----------------------------------------------------------------------
         ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,31('*'),' D4OUTF MESSAGE ',31('*')//
     & ' OUTPUT ELEMENT MASTER FILE NAME: ',A80)
 1001 FORMAT(1X,'THIS FILE ALREADY EXISTS AND HAS NOT BEEN REPLACED')
 1002 FORMAT(1X,'THIS FILE ALREADY EXISTED BUT HAS BEEN REPLACED')
 1003 FORMAT(/1X,31('*'),' END OF MESSAGE ',31('*')/)
 1004 FORMAT(5I5,5X,'/',A12,3X,'/',1X,A32)
 1005 FORMAT(1X,79('-'))
 1006 FORMAT(8F10.5)
 1007 FORMAT(1X,51('-'),'/ Z1=',I2,3X,'/ DATE= ',A8)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
