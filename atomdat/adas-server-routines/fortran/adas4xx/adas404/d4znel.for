CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4znel.for,v 1.2 2004/07/06 13:14:55 whitefor Exp $ Date $Date: 2004/07/06 13:14:55 $
CX
      SUBROUTINE D4ZNEL ( IZ1  , IZ2  ,
     &                    NEL1 , NEL2 ,
     &                    IZ0  , IZE1 , IZE2
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4ZNEL *********************
C
C  PURPOSE: TO RETURN THE MAXIMUM AND MINIMUM CHOSEN FOR THE IONIC
C           CHARGE (+1) AND THE NUMBER OF ELECTRONS IN EACH CASE.
C
C  CALLING PROGRAM: ADAS404
C
C  SUBROUTINE:
C
C  OUTPUT: (I*4)  IZ1     = MINIMUM ALLOWED IONIC CHARGE + 1
C  OUTPUT: (I*4)  IZ2     = MAXIMUM ALLOWED IONIC CHARGE + 1
C
C  OUTPUT: (I*4)  NEL1    = NUMBER OF ELECTRONS IN STATE 'IZ1'
C  OUTPUT: (I*4)  NEL2    = NUMBER OF ELECTRONS IN STATE 'IZ2'
C
C  INPUT : (I*4)  IZ0     = ELEMENT NUCLEAR CHARGE
C  INPUT : (I*4)  IZE1    = LOWEST ION CHARGE
C  INPUT : (I*4)  IZE2    = HIGHEST ION CHARGE  (NB. EXCLUDING THE  BARE
C                           NUCLEUS - ONE MORE STAGE IS BROUGHT IN AUTO-
C                           MATICALLY IN THE IONISATION BALANCE)
C
C  ROUTINES: NONE
C
C  AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C  DATE:    08/10/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-20-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C VERSION: 1.2				DATE: 20-10-97
C MODIFIED: LORNE HORTON (JET)
C       - REMOVED NONSENSE WITH NIND, NSET AND YEAR
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER IZ1  , IZ2
      INTEGER NEL1 , NEL2
      INTEGER IZ0  , IZE1 , IZE2
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C CALCULATE MIN AND MAX NUMBER OF ELECTRONS
C-----------------------------------------------------------------------
C
      NEL1=IZ0-IZE1
      NEL2=IZ0-IZE2
C
C-----------------------------------------------------------------------
C CALCULATE MAX AND MIN IONIC CHARGE + 1
C-----------------------------------------------------------------------
C
      IZ1=IZE1+1
      IZ2=IZE2+1
C
C-----------------------------------------------------------------------
C
      RETURN
      END
