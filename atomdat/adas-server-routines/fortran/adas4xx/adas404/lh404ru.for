       SUBROUTINE LH404RU(DATE  , USER , 
     &                    IZ0   , IZL  , IZH   , CHPRFIX ,
     &                    MAXT  , MAXD , TEK   , DENSA   ,
     &                    DSNIN , DSNO , LDTYP , YEAR    , OPEN17
     &                   )
       IMPLICIT NONE
C----------------------------------------------------------------------
C
C ************ FORTRAN 77 PROGRAM: LH404RU *****************************
C
C   VERSION 1.0
C
C   PURPOSE:  TO FETCH DATA FROM MASTER CONDENSED PARENT/METASTABLE
C             RESOLVED COLLISIONAL DIELECTRONIC FILES, BUNDLE THEM,
C             AND PREPARE UNRESOLVED ISONUCLEAR (ADF11) MASTER FILES.
C           
C             ALSO CHECK FOR AN EQUIVALENTLY NAMED ADF15 FILES
C           
C             AND BUNDLE RESOLVED DATA BLOCKS INTO UNRESOLVED ONES.
C           
C             LOOSELY BASED UPON WJD404R
C           
C
C   PROGRAM:
C
C   PARAMETER : (I*4)  NUTMAX - OUTPUT  ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF TEMPERATURES
C   PARAMETER : (I*4)  NUDMAX - OUTPUT  ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF DENSITIES
C   PARAMETER : (I*4)  NUZMAX - OUTPUT  ELEMENT MASTER FILE
C                                 MAXIMUM NUMBER OF CHARGE STATES
C   PARAMETER : (I*4)  NUMMAX - METASTABLE FRACTIONS
C                                 MAXIMUM NUMBER OF METASTABLES
C
C       (R*8)  DENSA()        - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXD DENSITIES
C       (R*8)  TEK()          - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXT TEMPERATURES
C       (R*8)  DENSL()        - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXD DENSITIES IN LOGARITHM
C       (R*8)  TEVL()         - OUTPUT ELEMENT MASTER FILE
C                                 SET OF MAXT TEMPERATURES IN LOGARITHM
C
C
C     ROUTINES:
C     ---------
C         XUFLOW   - VS FORTRAN UNDERFLOW EXCEPTION HANDLER
C         CL3270   - JET-SPECIFIC CLEAR SCREEN ROUTINE
C         XXDATE   - ADAS - GATHER CURRENT DATE
C         XXOPEN   - ADAS - OPEN FILE
C         XXSLEN   - ADAS - GET FIRST AND LAST CHAR. POS. IN A STRING
C         DMGUID   - JET-SPECIFIC - GATHER USERS ID
C         METRD    - READ MET FILES AND SPLINE ONTO TEMP/DENS ARRAY
C         FILEINF  - VS FORTRAN FILE INFORMATION ROUTINE
C         BND404A  - READ ACD,SCD,CCD,PRB,PRC RESOLVED ADF10 FILES
C                    AND BUNDLE THEM INTO UNRESOLVED ADF11 FILES
C         BND404B  - READ PLT,PLS RESOLVED ADF10 FILES
C                    AND BUNDLE THEM INTO UNRESOLVED ADF11 FILES
C         BND404C  - READ ADF10 FILES AND BUNDLE THE RESOLVED
C                    BLOCKS INTO UNRESOLVED BLOCKS TO BE TACKED
C                    ONTO THE BOTTOM OF THE FILE
C
C----------------------------------------------------------------------
C AUTHOR:  LORNE D. HORTON
C          ROOM K1/1/58, JET JOINT UNDERTAKING
C
C   DATE:  21ST FEBRUARY 1996
C
C----------------------------------------------------------------------
C
C VERSION: 1.2							
C DATE: 20-10-97
C MODIFIED: LORNE HORTON
C		- MODIFIED CALL TO BND404A and BND404B.
C
C VERSION:  1.3                         
C DATE: 4/11/99
C MODIFIED: Martin O'Mullane
C           - Add real name of producer.
C           - Modify comments.
C
C VERSION :  1.4                         
C DATE    : 04-01-2007
C MODIFIED: Martin O'Mullane
C           - F-like and Ne-like stages have 2 (not 1) metastables. 
C             Change NGRD vector.
C           - Use 11605.4 as K/eV conversion (same as xxtcon).
C
C VERSION :  1.5                        
C DATE    : 17-05-2007
C MODIFIED: Allan Whiteford
C           - Updated comments as part of subroutine documentation
C             procedure.
C
C VERSION : 1.6                         
C DATE    : 31-01-2011
C MODIFIED: Martin O'Mullane
C           - Use the correct number of metastables (up to Ar-like) 
C             in the NGRD vector.
C
C VERSION : 1.7                         
C DATE    : 29-06-2012
C MODIFIED: Martin O'Mullane
C           - Output correct units for PRB and PRC in adf11 files.
C
C----------------------------------------------------------------------

       INTEGER   NUTMAX, NUDMAX, NUZMAX, NUMMAX
       INTEGER   IST1, IST2, IST4, IST5
       REAL*8    DLOGMIN, DMIN
       PARAMETER (NUTMAX = 35, NUDMAX = 30, NUZMAX = 50, NUMMAX = 4)
       PARAMETER (IST1 = 5, IST2 = 17, IST4 = 11, IST5 = 12)
       PARAMETER (DLOGMIN = -74.0 , DMIN = 1.0D-74)
C----------------------------------------------------------------------
C      DECLARATIONS FOR INPUT DATA
C----------------------------------------------------------------------
       INTEGER   IZ0, IZL, IZH
       CHARACTER DATE*8 , YEAR*2, USERID*6, CHPRFIX*2, USER*30
       CHARACTER SYSUID*7, PREFIX*7
       LOGICAL   LPRFIX, OPEN17
C----------------------------------------------------------------------
C      VARIABLES LOADED BY DATA STATEMENTS IN THE SOURCE CODE
C----------------------------------------------------------------------
       INTEGER   NGRD(50)
       INTEGER   MAXT, MAXD
       REAL*8    TEK(NUTMAX), DENSA(NUDMAX)
       CHARACTER CDTYP(12)*3
       LOGICAL   LDTYP(9)
C----------------------------------------------------------------------
C      VARIABLES FOR BUILDING FILE NAMES
C----------------------------------------------------------------------
       INTEGER   LSE
       CHARACTER DSNAME*80, DSNOUT*80
       CHARACTER ELEMT*2, ELEMN*12, XFESYM*2, XFELEM*12
       LOGICAL   LEXIST
       CHARACTER  DSNO(10)*80 , DSNIN(50,10)*80
C----------------------------------------------------------------------
C      VARIABLES FOR METRD
C----------------------------------------------------------------------
       REAL*8    METFRC(NUDMAX, NUTMAX, NUZMAX, NUMMAX)
C----------------------------------------------------------------------
C      VARIABLES FOR WRITING TO PASS FILES
C----------------------------------------------------------------------
       INTEGER   ITYPE, IWRITE
       INTEGER   NELH, NELL, NEL
C----------------------------------------------------------------------
C      LOG TEMPERATURE AND DENSITY VECTORS FOR OUTPUT
C----------------------------------------------------------------------
       REAL*8    TEVL(NUTMAX), DENSL(NUDMAX)
C----------------------------------------------------------------------
C      MISCELLANEOUS COUNTERS, ETC.
C----------------------------------------------------------------------
       INTEGER   IT, ID, IZ, IGRD, IRCODE, LEN1, LEN2
       REAL*8    SUM
C
C----------------------------------------------------------------------
C
       DATA NGRD  /  1 , 2 , 1 , 2 , 2 , 4 , 3 , 4 , 2 , 2 ,
     &               1 , 2 , 2 , 4 , 3 , 4 , 2 , 2 , 1 , 1 ,
     &               1 , 1 , 3 , 2 , 1 , 1 , 1 , 1 , 1 , 1 ,
     &               1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ,
     &               3 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 /
C
       DATA CDTYP / 'acd' , 'scd' , 'ccd' , 'prb' , 'prc' ,
     &              'qcd' , 'xcd' , 'plt' , 'pls' , 'met' ,
     &              'pec' , 'sxb' /
C----------------------------------------------------------------------
C   DETERMINE WHICH DATA TYPES SHOULD BE INCLUDED IN THE MERGING
C   FROM RESOLVED TO UNRESOLVED.  'QCD' AND 'XCD' SHOULD ALWAYS
C   BE OFF.  'MET' FILES ARE READ IN ALL CASES AND MUST EXIST
C   FOR THE CODE TO WORK.
C----------------------------------------------------------------------
C   NOW READ IN FROM IDL
C----------------------------------------------------------------------
CX       DATA LDTYP / .TRUE. ,.TRUE. ,.TRUE. ,.TRUE. ,.TRUE. ,
CX     &              .FALSE.,.FALSE.,.TRUE. ,.TRUE. /
C----------------------------------------------------------------------
C   DATA FOR 96 FILE GENERATION
C----------------------------------------------------------------------
C      DATA MAXT / 30 /
C      DATA MAXD / 24 /
C      DATA TEK   / 2.3208D+03 , 3.4812D+03 , 5.8020D+03 , 8.1228D+03,
C    &              1.1604D+04 , 1.7406D+04 , 2.3208D+04 , 3.4812D+04,
C    &              5.8020D+04 , 8.1228D+04 , 1.1604D+05 , 1.7406D+05,
C    &              2.3208D+05 , 3.4812D+05 , 5.8020D+05 , 8.1228D+05,
C    &              1.1604D+06 , 1.7406D+06 , 2.3208D+06 , 3.4812D+06,
C    &              5.8020D+06 , 8.1228D+06 , 1.1604D+07 , 1.7406D+07,
C    &              2.3208D+07 , 3.4812D+07 , 5.8020D+07 , 8.1228D+07,
C    &              1.1604D+08 , 1.7406D+08 , 5*0.0D0/
C      DATA DENSA / 5.0000D+07 , 1.0000D+08 , 2.0000D+08 ,
C    &              5.0000D+08 , 1.0000D+09 , 2.0000D+09 ,
C    &              5.0000D+09 , 1.0000D+10 , 2.0000D+10 ,
C    &              5.0000D+10 , 1.0000D+11 , 2.0000D+11 ,
C    &              5.0000D+11 , 1.0000D+12 , 2.0000D+12 ,
C    &              5.0000D+12 , 1.0000D+13 , 2.0000D+13 ,
C    &              5.0000D+13 , 1.0000D+14 , 2.0000D+14 ,
C    &              5.0000D+14 , 1.0000D+15 , 2.0000D+15 ,
C    &              6*0.0D0/
C----------------------------------------------------------------------
C   DATA FOR 96 HELIUM STUDY - REMOVE BOTTOM 3 AND TOP TEMPERATURES
C----------------------------------------------------------------------
CX       DATA MAXT / 26 /
CX       DATA MAXD / 24 /
CX       DATA TEK   / 8.1228D+03,
CX     &              1.1604D+04 , 1.7406D+04 , 2.3208D+04 , 3.4812D+04,
CX     &              5.8020D+04 , 8.1228D+04 , 1.1604D+05 , 1.7406D+05,
CX     &              2.3208D+05 , 3.4812D+05 , 5.8020D+05 , 8.1228D+05,
CX     &              1.1604D+06 , 1.7406D+06 , 2.3208D+06 , 3.4812D+06,
CX     &              5.8020D+06 , 8.1228D+06 , 1.1604D+07 , 1.7406D+07,
CX     &              2.3208D+07 , 3.4812D+07 , 5.8020D+07 , 8.1228D+07,
CX     &              1.1604D+08 , 9*0.0D0/
CX       DATA DENSA / 5.0000D+07 , 1.0000D+08 , 2.0000D+08 ,
CX     &              5.0000D+08 , 1.0000D+09 , 2.0000D+09 ,
CX     &              5.0000D+09 , 1.0000D+10 , 2.0000D+10 ,
CX     &              5.0000D+10 , 1.0000D+11 , 2.0000D+11 ,
CX     &              5.0000D+11 , 1.0000D+12 , 2.0000D+12 ,
CX     &              5.0000D+12 , 1.0000D+13 , 2.0000D+13 ,
CX     &              5.0000D+13 , 1.0000D+14 , 2.0000D+14 ,
CX     &              5.0000D+14 , 1.0000D+15 , 2.0000D+15 ,
CX     &              6*0.0D0/
C----------------------------------------------------------------------
C   DATA FOR SANCO FILE FORMATION
C----------------------------------------------------------------------
C      DATA MAXT / 35 /
C      DATA MAXD / 26 /
C      DATA TEK   / 1.1604D+04 , 1.4609D+04 , 1.8391D+04 , 2.3153D+04,
C    &              2.9148E+04 , 3.6695D+04 , 4.6196D+04 , 5.8158D+04,
C    &              7.3216E+04 , 9.2174E+04 ,
C    &              1.1604D+05 , 1.4609D+05 , 1.8391D+05 , 2.3153D+05,
C    &              2.9148E+05 , 3.6695D+05 , 4.6196D+05 , 5.8158D+05,
C    &              7.3216E+05 , 9.2174E+05 ,
C    &              1.1604D+06 , 1.8391D+06 ,
C    &              2.9148E+06 , 4.6196D+06 ,
C    &              7.3216E+06 ,
C    &              1.1604D+07 , 1.8391D+07 ,
C    &              2.9148E+07 , 4.6196D+07 ,
C    &              7.3216E+07 ,
C    &              1.1604D+08 , 1.8391D+08 ,
C    &              2.9148E+08 , 4.6196D+08 ,
C    &              5.8158E+08 /
C      DATA DENSA / 1.000D+10 , 1.585D+10, 2.512D+10 ,
C    &              3.981D+10 , 6.310D+10,
C    &              1.000D+11 , 1.585D+11, 2.512D+11 ,
C    &              3.981D+11 , 6.310D+11,
C    &              1.000D+12 , 1.585D+12, 2.512D+12 ,
C    &              3.981D+12 , 6.310D+12,
C    &              1.000D+13 , 1.585D+13, 2.512D+13 ,
C    &              3.981D+13 , 6.310D+13,
C    &              1.000D+14 , 1.585D+14, 2.512D+14 ,
C    &              3.981D+14 , 6.310D+14,
C    &              1.00D+15  , 0,0,0,0 /
C----------------------------------------------------------------------
C   DATA FOR NORMAL FILES
C----------------------------------------------------------------------
C      DATA MAXT / 20 /
C      DATA MAXD / 13 /
C      DATA TEK   / 4.62E+3, 7.32E+3
C    &            , 1.16D+4, 1.84D+4, 2.91D+4, 4.62D+4, 7.32D+4
C    &            , 1.16D+5, 1.84D+5, 2.91D+5, 4.62D+5, 7.32D+5
C    &            , 1.16D+6, 1.84D+6, 2.91D+6, 4.62D+6, 7.32D+6
C    &            , 1.16D+7, 1.84D+7, 2.91D+7, 4.62D+7, 7.32D+7
C    &            , 1.16D+8,
C    &              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C      DATA DENSA / 1.000D+04 , 1.000D+05, 1.000D+06 ,
C    &              1.000D+07 , 1.000D+08, 1.000D+09 ,
C    &              1.000D+10 , 1.000D+11, 1.000D+12 ,
C    &              1.000D+13 , 1.000D+14, 1.000D+15 ,
C    &              1.000D+16 , 0, 0, 0, 0, 0, 0, 0,
C    &                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C----------------------------------------------------------------------
C   DATA FOR CHROMIUM AND MOLYBDENUM STUDY
C----------------------------------------------------------------------
C      DATA MAXT / 12 /
C      DATA MAXD / 13 /
C      DATA TEK   /
C    &              1.16D+4, 1.84D+4, 2.91D+4, 4.62D+4, 7.32D+4
C    &            , 1.16D+5, 1.84D+5, 2.91D+5, 4.62D+5, 7.32D+5
C    &            , 1.16D+6, 1.84D+6
C    &            , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
C    &            , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C      DATA DENSA / 1.000D+04 , 1.000D+05, 1.000D+06 ,
C    &              1.000D+07 , 1.000D+08, 1.000D+09 ,
C    &              1.000D+10 , 1.000D+11, 1.000D+12 ,
C    &              1.000D+13 , 1.000D+14, 1.000D+15 ,
C    &              1.000D+16 , 0, 0, 0, 0, 0, 0, 0,
C    &                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /
C
C----------------------------------------------------------------------
C      GATHER NUCLEAR CHARGE AND YEAR OF DATA FROM SCREEN
C----------------------------------------------------------------------
CX       CALL XUFLOW(0)
CX       CALL CL3270
CX       WRITE(IST2,1000)
CX       WRITE(IST2,1002)
CX       READ(IST1,*) IZ0 , IZL , IZH
CX       WRITE(IST2,1010)
CX       READ(IST1,1018) USERID
CX       WRITE(IST2,1012)
CX       READ(IST1,1014) YEAR
CX       WRITE(IST2,1016)
CX       READ(IST2,1014) CHPRFIX
       IF( CHPRFIX .NE. 'NO') THEN
         LPRFIX = .TRUE.
       ELSE
         LPRFIX = .FALSE.
       ENDIF
C----------------------------------------------------------------------
C   GATHER CURRENT DATE
C----------------------------------------------------------------------
CX       CALL XXDATE(DATE)
C----------------------------------------------------------------------
C   GATHER USER ID FOR OUTPUT FILES
C----------------------------------------------------------------------
CX       CALL DMGUID(SYSUID,PREFIX)
C----------------------------------------------------------------------
C   CALCULATE LOG BASE 10 VALUES OF OUTPUT TEMPERATURES AND DENSITIES
C----------------------------------------------------------------------
       DO 2 IT = 1,MAXT
         TEVL(IT) = DLOG10( TEK(IT) / 1.16054D+04 )
    2  CONTINUE
       DO 3 ID = 1,MAXD
         DENSL(ID) = DLOG10( DENSA(ID) )
    3  CONTINUE
C----------------------------------------------------------------------
C   DEFINE DATA ON ELEMENT  AND RANGE OF CALCULATION
C----------------------------------------------------------------------
       ELEMT = XFESYM( IZ0 )
       ELEMN = XFELEM( IZ0 )
       LSE   = 1
       IF( ELEMT(2:2) .NE. ' ' ) LSE = 2
C----------------------------------------------------------------------
C   READ MET FILE AND SPLINE ONTO REQUIRED MESH
C   IDL-UNIX: DSNIN PASSED RATHER THAN THE FILENAME ROOT
C----------------------------------------------------------------------
CX       DSNAME = '/'//USERID//'.'//CDTYP(10)//YEAR//'.DATA('
CTEMP
C     FOR TESTING, LOOK FOR MET FILES IN JETXJS
C
C      DSNAME(2:7) = 'JETXJS'
CTEMP
CX       IF ( LPRFIX ) THEN
CX         DSNAME = DSNAME(1:19)//CHPRFIX//'#)'
CX       ELSE
CX         DSNAME = DSNAME(1:19)//')'
CX       ENDIF
       CALL METRD(NUTMAX, NUDMAX  , NUZMAX , NUMMAX ,
     &            MAXT  , MAXD    , DSNIN  ,
     &            IZL   , IZH     , IZ0    ,
     &            TEK   , DENSA   ,
     &            METFRC,
     &            NGRD  ,
     &            IST2  , IST5    )
CTEMP
C     WRITE METASTABLE RESULTS FOR BE-LIKE IONS
C
C      IF (.TRUE.) THEN
C        WRITE(10,1210) (DENSL(ID), ID = 1,MAXD)
C        WRITE(10,1210) (TEVL(IT) , IT = 1,MAXT)
C        DO IGRD = 1, NGRD(4)
C          WRITE(10,'(1X,''/'',I1,''/'')') IGRD
C          DO IT = 1, MAXT
C            WRITE(10,1210) (METFRC(ID,IT,IZ0-3,IGRD),ID=1,MAXD)
C          ENDDO
C        ENDDO
C      ENDIF
CTEMP
C----------------------------------------------------------------------
C   CHANGE METASTABLE NORMALISATION TO TOTAL RATHER THAN GROUND
C----------------------------------------------------------------------
       DO ID = 1, MAXD
         DO IT = 1, MAXT
           DO IZ = IZL, MIN0(IZ0,IZH+1)
             NEL = IZ0 - IZ + 1
             SUM = 0.0
             DO IGRD = 1, NGRD(NEL)
               SUM = SUM + METFRC(ID,IT,IZ,IGRD)
             ENDDO
             DO IGRD = 1, NGRD(NEL)
               METFRC(ID,IT,IZ,IGRD) = METFRC(ID,IT,IZ,IGRD) / SUM
             ENDDO
           ENDDO
         ENDDO
       ENDDO
C----------------------------------------------------------------------
C   CYCLE THROUGH TYPES OF VARIABLE
C----------------------------------------------------------------------
       DO 300 ITYPE  = 1,9
         IF (.NOT. LDTYP(ITYPE)) GOTO 300
C----------------------------------------------------------------------
C   DYNAMIC OPEN OF ISONUCLEAR MASTER FILE
C
C      OUTPUT IS WRITTEN TO PASSING FILES
C              E.G.   ACD404.PASS
C                     SCD404.PASS
C                     CCD404.PASS
C                     PRB404.PASS
C                     PRC404.PASS
C                     PLT404.PASS
C                     PLS404.PASS
C----------------------------------------------------------------------
CX         DSNOUT = '/'//SYSUID(1:6)//'.'//CDTYP(ITYPE)//'404.PASS'
         DSNOUT = DSNO(ITYPE)
C
         WRITE(IST2,1004)  DSNOUT
         WRITE(IST2,1005)
C
         IWRITE = IST4
         INQUIRE(FILE = DSNOUT , EXIST = LEXIST)
CX         CALL FILEINF(IRCODE,'TRK',10,'SECOND',5,'RECFM','FB',
CX     &                'LRECL',80,'BLKSIZE',4800)
         OPEN( UNIT = IWRITE, FILE = DSNOUT, STATUS = 'UNKNOWN')
C
         WRITE(IWRITE,1200) IZ0, MAXD, MAXT, IZL, IZH, ELEMN
         WRITE(IWRITE,1205)
         WRITE(IWRITE,1210) ( DENSL(ID), ID = 1,MAXD)
         WRITE(IWRITE,1210) ( TEVL(IT) , IT = 1,MAXT)
C----------------------------------------------------------------------
C   CONSTRUCT FIRST PART OF INPUT FILE NAME - DONE IN IDL NOW
C----------------------------------------------------------------------
CX         DSNAME = '/'//USERID//'.'//CDTYP(ITYPE)//YEAR//'.DATA('
CX         IF ( LPRFIX ) THEN
CX           DSNAME = DSNAME(1:19)//CHPRFIX//'#)'
CX         ELSE
CX           DSNAME = DSNAME(1:19)//')'
CX         ENDIF
C----------------------------------------------------------------------
C   CALL SUBROUTINE TO INTERROGATE MASTER CONDENSED FILES AND
C   WRITE ELEMENT MASTER FILE
C----------------------------------------------------------------------
         IF (ITYPE .GE. 1 .AND. ITYPE. LE. 5) THEN
           CALL BND404A(ITYPE ,
     &                  NUTMAX, NUDMAX  , NUZMAX , NUMMAX,
     &                  MAXT  , MAXD    ,
     &                  IZL   , IZH     , IZ0    , TEK   ,  DENSA ,
     &                  METFRC,
     &                  NGRD  ,
     &                  IST2  , IST5    , IWRITE , DATE,
     &                  DSNIN)
         ELSEIF (ITYPE .GE. 8 .AND. ITYPE. LE. 9) THEN
           CALL BND404B(ITYPE ,
     &                  NUTMAX, NUDMAX  , NUZMAX , NUMMAX,
     &                  MAXT  , MAXD    ,
     &                  IZL   , IZH     , IZ0    , TEK   ,  DENSA ,
     &                  METFRC,
     &                  NGRD  ,
     &                  IST2  , IST5    , IWRITE , DATE,
     &                  DSNIN)
         ENDIF
C----------------------------------------------------------------------
C         WRITE INFORMATION SECTION AT BOTTOM OF DATA SET
C         CLOSE ISONUCLEAR MASTER FILE
C----------------------------------------------------------------------
         WRITE(IWRITE,1206)
         WRITE(IWRITE,1220)
         IF(ITYPE .EQ. 4 .OR. ITYPE .EQ. 5 .OR.
     &      ITYPE .EQ. 8 .OR. ITYPE .EQ. 9) THEN
           WRITE(IWRITE,1230)
           WRITE(IWRITE,1232)
         ELSE
           WRITE(IWRITE,1234)
           WRITE(IWRITE,1236)
         ENDIF
         WRITE(IWRITE,1220)
         WRITE(IWRITE,1240)
         IF (LPRFIX) THEN
           WRITE(IWRITE,1244) YEAR, CHPRFIX
         ELSE
           WRITE(IWRITE,1242) YEAR
         ENDIF
         WRITE(IWRITE,1246) ELEMT, IZL, IZH
         WRITE(IWRITE,1220)
         WRITE(IWRITE,1224) USER,DATE
         WRITE(IWRITE,1220)
         WRITE(IWRITE,1206)
         CLOSE(IWRITE)
C----------------------------------------------------------------------
C    END OF VARIABLE TYPE LOOP
C----------------------------------------------------------------------
  300  CONTINUE
C----------------------------------------------------------------------
C ADF15 FILE OUTPUT REMOVED FOR IDL-UNIX CONVERSION
C----------------------------------------------------------------------
CX
CX C----------------------------------------------------------------------
CX C   SAME IDEA FOR ADF15 FILES
CX C
CX C      OUTPUT IS WRITTEN TO PASSING FILES BY ION NAME
CX C              E.G.   PEC#C0.PASS
CX C                     SXB#C0.PASS
CX C----------------------------------------------------------------------
CX        IWRITE = IST4
CX C----------------------------------------------------------------------
CX C   LOOP OVER FILE TYPE
CX C----------------------------------------------------------------------
CX        DO 380 ITYPE = 11, 12
CX C----------------------------------------------------------------------
CX C   CONSTRUCT FIRST PART OF INPUT AND OUTPUT FILE NAMES
CX C----------------------------------------------------------------------
CX CX         DSNAME = '/'//USERID//'.'//CDTYP(ITYPE)//YEAR
CX CX     &            //'#'//ELEMT(1:LSE)//'.DATA('
CX C
CX CX         DSNOUT = '/'//SYSUID(1:6)//'.'//CDTYP(ITYPE)//'#'
CX          DSNOUT = DSNO(ITYPE)
CX C----------------------------------------------------------------------
CX C   LOOP OVER RECOMBINED ION CHARGE
CX C----------------------------------------------------------------------
CX          DO 370 IZ = IZL-1, IZH-1
CX C----------------------------------------------------------------------
CX C   CHECK FOR EXISTENCE OF INPUT FILE
CX C----------------------------------------------------------------------
CX            LEN1 = 1
CX            LEN2 = INDEX(DSNAME,'(')
CX            IF ( LPRFIX ) THEN
CX              DSNAME = DSNAME(LEN1:LEN2)//CHPRFIX//'R#'//ELEMT(1:LSE)
CX            ELSE
CX              DSNAME = DSNAME(LEN1:LEN2)//ELEMT(1:LSE)
CX            ENDIF
CX            CALL XXSLEN(DSNAME,LEN1,LEN2)
CX            IF (IZ.LT.10) THEN
CX              WRITE(DSNAME(LEN2+1:LEN2+1),'(I1)') IZ
CX              LEN2 = LEN2 + 1
CX            ELSE
CX              WRITE(DSNAME(LEN2+1:LEN2+2),'(I2)') IZ
CX              LEN2 = LEN2 + 2
CX            ENDIF
CX            DSNAME = DSNAME(LEN1:LEN2)//')'
CX C
CX            WRITE(IST2,1005)
CX            WRITE(IST2,1006)  DSNAME
CX C
CX            CALL XXOPEN(IST5, DSNAME, LEXIST)
CX            IF (.NOT. LEXIST) GOTO 370
CX C----------------------------------------------------------------------
CX C   OPEN OUTPUT PASSING FILE
CX C----------------------------------------------------------------------
CX            LEN1 = 1
CX            LEN2 = INDEX(DSNOUT,'#')
CX            DSNOUT = DSNOUT(LEN1:LEN2)//ELEMT(1:LSE)
CX            CALL XXSLEN(DSNOUT,LEN1,LEN2)
CX            IF (IZ.LT.10) THEN
CX              WRITE(DSNOUT(LEN2+1:LEN2+1),'(I1)') IZ
CX              LEN2 = LEN2 + 1
CX            ELSE
CX              WRITE(DSNOUT(LEN2+1:LEN2+2),'(I2)') IZ
CX              LEN2 = LEN2 + 2
CX            ENDIF
CX            DSNOUT = DSNOUT(LEN1:LEN2)//'.PASS'
CX C
CX            WRITE(IST2,1004)  DSNOUT
CX C
CX            INQUIRE(FILE = DSNOUT , EXIST = LEXIST)
CX CX           CALL FILEINF(IRCODE,'TRK',10,'SECOND',5,'RECFM','FB',
CX CX     &                  'LRECL',80,'BLKSIZE',4800)
CX            OPEN( UNIT = IWRITE, FILE = DSNOUT, STATUS = 'UNKNOWN')
CX C----------------------------------------------------------------------
CX C   CALL SUBROUTINE TO INTERROGATE INPUT FILE AND APPEND
CX C   UNRESOLVED DATA
CX C----------------------------------------------------------------------
CX            CALL BND404C(ITYPE-10,
CX      &                  NUTMAX, NUDMAX  , NUZMAX , NUMMAX,
CX      &                  MAXT  , MAXD    , DSNAME ,
CX      &                  IZ    , IZ0     , TEK    , DENSA ,
CX      &                  METFRC,
CX      &                  NGRD  ,
CX      &                  IST2  , IST5    , IWRITE)
CX C----------------------------------------------------------------------
CX C   WRITE INFORMATION SECTION AT BOTTOM OF DATA SET AND CLOSE
CX C----------------------------------------------------------------------
CX            WRITE(IWRITE,1220)
CX            WRITE(IWRITE,1224) DATE
CX            WRITE(IWRITE,1220)
CX            WRITE(IWRITE,1206)
CX            CLOSE(IWRITE)
CX C----------------------------------------------------------------------
CX C   END LOOP OVER RECOMBINED ION CHARGE
CX C----------------------------------------------------------------------
CX   370    CONTINUE
CX C----------------------------------------------------------------------
CX C    END OF VARIABLE TYPE LOOP
CX C----------------------------------------------------------------------
CX   380  CONTINUE
C----------------------------------------------------------------------
C   FINISHED!
C----------------------------------------------------------------------
       RETURN
C
 1000  FORMAT(' PROGRAM LH404RU TO FORM ISONUCLEAR MASTER FILES '/
     &        ' ----------------------------------------------- ')
 1002  FORMAT(' INPUT NUCLEAR CHARGE (IZ0)'/
     &        ' IZ1 OF LOWEST CHARGE STATE (IZL) AND '/
     &        ' IZ1 OF HIGHEST CHARGE STATE (IZH):')
 1004  FORMAT(/' DSNOUT : ',1A80)
 1005  FORMAT(/' ACCESSING DATA FROM :' )
 1006  FORMAT(/' DSNAME : ',1A80)
 1010  FORMAT(/' INPUT USERID OF DATA (USUALLY JETSHP) :')
 1012  FORMAT(/' INPUT YEAR OF DATA :')
 1014  FORMAT(1A2)
 1016  FORMAT(/' INPUT DATA PREFIX (EG. <PJ> ) OR <NO> IF NONE:')
 1018  FORMAT(1A6)
C
 1200  FORMAT(5I5,5X,'/',1A12,'       /GCR PROJECT      ')
 1205  FORMAT(80('-'))
 1206  FORMAT('C',79('-'))
 1210  FORMAT(8F10.5)
C
 1220  FORMAT('C')
 1230  FORMAT('C     RADIATED POWERS IN UNITS OF W CM**3 ')
 1232  FORMAT('C     ----------------------------------- ')
 1234  FORMAT('C     RATE COEFFICIENTS IN UNITS OF CM**3 S-1 ')
 1236  FORMAT('C     --------------------------------------- ')
 1240  FORMAT('C     INPUT DATA')
 1242  FORMAT('C         FOR YEAR: ',1A2)
 1244  FORMAT('C         FOR YEAR: ',1A2,' AND WITH PREFIX: ',1A2)
 1246  FORMAT('C         FOR ',1A2,' RECOMBINING ION CHARGE(S) ',
     &                   I2,' TO ',I2)

 1224  FORMAT('C     CODE     : ADAS404',/,
     &        'C     PRODUCER : ',A30,/,
     &        'C     DATE     : ',A8)

      END
