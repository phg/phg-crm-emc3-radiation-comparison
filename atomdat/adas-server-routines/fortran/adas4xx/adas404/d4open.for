CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4open.for,v 1.2 2007/05/18 08:52:58 allan Exp $ Date $Date: 2007/05/18 08:52:58 $
CX
       SUBROUTINE D4OPEN( IUNIT, DSFULL , LEXIST )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4OPEN *********************
C
C  PURPOSE: TO INQUIRE & OPEN INPUT DATA FILE & ALLOCATE TO UNIT 'IUNIT'
C           (READ ONLY)  - IF IT DOES NOT EXISTS A MESSAGE IS  SENT  TO
C           THE SCREEN AND LEXIST IS RETURNED AS FALSE.
C
C           *** THIS VERSION SUPPRESSES ERROR MESSAGE ON NON-EXISTING
C               FILES. IT IS OTHERWISE IDENTICAL TO XXOPEN.
C
C  CALLING PROGRAM:  ADAS404
C
C  SUBROUTINE:
C
C  INPUT :    (I*4)  IUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED
C  INPUT :    (C*(*))DSFULL  = FULL INPUT DATA SET NAME (INCL. USERID)
C                              IN FORM FOR DYNAMIC ALLOCATION.
C  OUTPUT:    (L*4)  LEXIST  = .TRUE.  => DATA SETS EXISTS AND IS OPEN
C                            = .FALSE. => DATA SET DOES NOT EXIST
C
C             (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C             (C*1)  BSLASH  = '/' - MUST BE FIRST 'DSFULL'CHARACTER
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXTERM     ADAS      TERMINATES ADAS PROGRAM WITH MESSAGE
C
C AUTHOR:  LORNE HORTON (JET)
C	     - BASED ON XXOPEN.
C
C DATE:    20-10-97
C-----------------------------------------------------------------------
C VERSION: 1.1						DATE:27-02-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION: 1.2						DATE:17-05-07
C MODIFIED: Allan Whiteford
C		- Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
       INTEGER   I4UNIT
       INTEGER   IUNIT
C-----------------------------------------------------------------------
       LOGICAL   LEXIST
C-----------------------------------------------------------------------
       CHARACTER DSFULL*(*)   , BSLASH*1
C-----------------------------------------------------------------------
       DATA      BSLASH /'/'/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  INQUIRE AND DYNAMICALLY OPEN FILE (CHECK IF '/' IS PRESENT)
C-----------------------------------------------------------------------
C
         IF (DSFULL(1:1).NE.BSLASH) THEN
            WRITE(I4UNIT(-1),1000) DSFULL
            WRITE(I4UNIT(-1),1001)
            WRITE(I4UNIT(-1),1003)
            LEXIST = .FALSE.
         ELSE
C
            INQUIRE( FILE=DSFULL , EXIST=LEXIST )
C
               IF (LEXIST) THEN
                  OPEN( UNIT=IUNIT , FILE=DSFULL , status = 'old' ,
     &                   ERR=9999  )
               ELSE
CX                WRITE(I4UNIT(-1),1000) DSFULL
CX                WRITE(I4UNIT(-1),1002)
CX                WRITE(I4UNIT(-1),1003)
               ENDIF
C
         ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C OPEN ERROR HANDLING - SEND MESSAGE AND TERMINATE PROGRAM
C-----------------------------------------------------------------------
C
 9999 WRITE(I4UNIT(-1),1000) DSFULL
      WRITE(I4UNIT(-1),1004)
      CALL XXTERM
      RETURN
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,32('*'),' XXOPEN ERROR ',32('*')//
     &        1X,'DATA SET NAME: ',A)
 1001 FORMAT( 1X,'THIS DATA SET NAME IS NOT IN A FORM VALID FOR ',
     &           ' DYNAMIC ALLOCATION')
 1002 FORMAT( 1X,'THIS DATA SET DOES NOT EXIST/CANNOT BE LOCATED')
 1003 FORMAT(/1X,28('*'),' END OF ERROR MESSAGE ',28('*'))
 1004 FORMAT( 1X,'AN ERROR WAS ENCOUNTERED WHEN TRYING TO OPEN ',
     &           'THIS DATA-SET')
C
C-----------------------------------------------------------------------
C
      END
