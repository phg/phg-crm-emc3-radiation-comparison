CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4ibal.for,v 1.1 2004/07/06 13:13:52 whitefor Exp $ Date $Date: 2004/07/06 13:13:52 $
CX
      SUBROUTINE D4IBAL ( MAXT  , IZ1   , IZ2  ,
     &                    NDZ   , NDTIN ,
     &                    ACDL  , SCDL
     &                  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4IBAL *********************
C
C  PURPOSE: TO CALCULATE IONISATION BALANCES AT FIXED DENSITY
C
C  CALLING PROGRAM: ADAS404
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  MAXT    = NUMBER OF USER ENTERED TEMPERATURES <= NDTIN
C  INPUT : (I*4)  IZ1     = MINIMUM ALLOWED IONIC CHARGE + 1
C                                       (ACCORDING TO AVAILABLE 'NSET')
C  INPUT : (I*4)  IZ2     = MAXIMUM ALLOWED IONIC CHARGE + 1
C
C  INPUT : (I*4)  NDZ     = NUMBER OF CHARGE STATES
C  INPUT : (I*4)  NDTIN   = MAXIMUM NUMBER OF INPUT TEMPERATURES
C
C  I/O   : (R*8)  ACDL(,) = INPUT : LOG10(RECOMB. COLL-DIEL COEFF)
C                           OUTPUT: LOG10(IONISATION-BALANCE)
C                           NOTE:   THESE VALUES ARE FOR A FIXED DENSITY
C                              1ST ARRAY DIMENSION = ION CHARGE/STAGE
C                              2ND ARRAY DIMENSION = TEMPERATURE
C  I/O   : (R*8)  SCDL(,) = INPUT : LOG10(IONIS. COLL-DIEL COEFF)
C                           OUTPUT: IONISATION-BALANCE
C                           NOTE:   THESE VALUES ARE FOR A FIXED DENSITY
C                              1ST ARRAY DIMENSION = ION CHARGE/STAGE
C                              2ND ARRAY DIMENSION = TEMPERATURE
C
C          (I*4)  MAX     = MAXIMUM NUMBER OF CHARGES FOR COLL-DIEL COEF
C          (I*4)  MAX1    = MAXIMUM NUMBER OF CHARGES FOR ION.-BALANCE
C                           ('MAX' + 1)
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR ION CHARGE VALUES
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE VALUES
C          (I*4)  IREF    = REFERENCE POINT IN COLL-DIEL COEFF ARRAY,
C                           REPRESENTING THE CHARGE BELOW WHICH THE ION.
C                           COLL-DIEL COEFF IS GREATER THAN RECOMB COLL-
C                           DIEL. COEFF..
C
C          (R*8)  S       = USED FOR SUMMING COEFFICIENTS
C          (R*8)  SLOG    = LOG10( 'S' )
C
C
C ROUTINES: NONE
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     08/10/90
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER MAXT  , IZ1   , IZ2
      INTEGER NDZ   , NDTIN
      INTEGER MAX   , MAX1  , IZ  , IT  , IREF
C-----------------------------------------------------------------------
      REAL*8  S     , SLOG
      REAL*8  ACDL(NDZ,NDTIN)  , SCDL(NDZ,NDTIN)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      MAX=IZ2-IZ1+1
      MAX1=MAX+1
C
C-----------------------------------------------------------------------
C CALCULATE DIFFERENCE BEYWEEN RECOMB. AND IONIS. COLL-DIEL COEFFTS.
C-----------------------------------------------------------------------
C
         DO 1 IZ=1,MAX
            DO 2 IT=1,MAXT
               ACDL(IZ,IT)=ACDL(IZ,IT)-SCDL(IZ,IT)
               SCDL(IZ,IT)=0.0
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C FIND CHARGE BELOW WHICH IONIS COLL-DIEL COEFF > RECOMB COLL-DIELCOEFF
C-----------------------------------------------------------------------
C
         DO 3 IT=1,MAXT
            IREF=MAX1
               DO 4 IZ=MAX,1,-1
                  IF (ACDL(IZ,IT).GT.0) IREF=IZ
    4          CONTINUE
C
C-----------------------------------------------------------------------
C SUM COEFFICIENT DIFFERENCES APPROPRIATELY
C-----------------------------------------------------------------------
C
            S=0.0
                DO 5 IZ=IREF-1,1,-1
                   S=S+ACDL(IZ,IT)
                   SCDL(IZ,IT)=S
    5           CONTINUE
            SCDL(IREF,IT)=0.0
            S=0.0
                DO 6 IZ=IREF,MAX
                   S=S-ACDL(IZ,IT)
                   SCDL(IZ+1,IT)=S
    6           CONTINUE
C
C-----------------------------------------------------------------------
C CALCULATE IONISATION BALANCE
C-----------------------------------------------------------------------
C
            S=0.0
               DO 7 IZ=1,MAX1
                  ACDL(IZ,IT)=SCDL(IZ,IT)
                     IF (SCDL(IZ,IT).GT.-70.0D0) THEN
                        SCDL(IZ,IT)=10.0**SCDL(IZ,IT)
                     ELSE
                        SCDL(IZ,IT)=0.0D0
                     ENDIF
                  S=S+SCDL(IZ,IT)
    7          CONTINUE
            S=1.0D0/S
            SLOG=DLOG10(S)
               DO 8 IZ=1,MAX1
                  SCDL(IZ,IT)=SCDL(IZ,IT)*S
                  ACDL(IZ,IT)=ACDL(IZ,IT)+SLOG
    8          CONTINUE
C-----------------------------------------------------------------------
    3    CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
