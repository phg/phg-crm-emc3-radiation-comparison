CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4ispf.for,v 1.4 2004/07/06 13:14:03 whitefor Exp $ Date $Date: 2004/07/06 13:14:03 $
CX
      SUBROUTINE D4ISPF( LPEND  , LTAB  ,
     &                   NDDEN , NDTIN  ,
     &                   TITLE , UIDIN  , USERID ,
     &                   IZ0   , IZE1   , IZE2   ,
     &                   MAXD  , MAXT   , ITTYP  ,
     &                   DENSA , TINE   ,
     &                   YEAR  , SELTAB , REPTAB ,
     &                   IRESO  ,
     &                   SAVFIL, DSNIN  , DSNO   ,
     &                   LDTYP
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4ISPF *********************
C
C  PURPOSE: ISPF PANEL INPUT SUBROUTINE FOR 'ADAS404'
C
C  CALLING PROGRAM: ADAS404
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C  OUTPUT: (L*4)   LTAB     = .TRUE.  => PAPER.TXT OUTPUT REQUESTED
C                           = .FALSE. => NO PAPER.TXT OUTPUT REQUESTED
C
C  INPUT : (I*4)   NDDEN    = MAX. NUMBER OF INPUT DENSITIES ALLOWED
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURES ALLOWED
C
C  OUTPUT: (C*32)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (C*6)   UIDIN    = PROJECT UID FOR INPUT CONDENSED DATA FILE
C  OUTPUT: (C*6)   USERID   = USER ID FOR OUTPUT INC ELEMENT MASTER FILE
C
C  OUTPUT: (I*4)   IZ0      = ELEMENT NUCLEAR CHARGE
C  OUTPUT: (I*4)   IZE1     = LOWEST ION CHARGE
C  OUTPUT: (I*4)   IZE2     = HIGHEST ION CHARGE (NB. EXCLUDING THE BARE
C                             NUCLEUS - ONE MORE STAGE IS BROUGHT IN
C                             AUTOMATICALLY IN THE IONISATION BALANCE).
C
C  OUTPUT: (I*4)   MAXD     = NUMBER OF INPUT DENSITIES ( 1 -> 30)
C  OUTPUT: (I*4)   MAXT     = NUMBER OF INPUT TEMPERATURES ( 1 -> 55)
C  OUTPUT: (I*4)   ITTYP    = 1 => INPUT TEMPERATURES IN KELVIN
C                           = 2 => INPUT TEMPERATURES IN EV
C
C  OUTPUT: (R*8)   DENSA()  = SET OF 'MAXD' ELECTRON DENSITIES (CM-3)
C                             (USER ENTERED VALUES)
C  OUTPUT: (R*8)   TINE()   = SET OF 'MAXT' ELECTRON TEMPERATURES -
C                             UNITS GIVEN BY 'ITTYP'.
C                             (USER ENTERED VALUES)
C
C  OUTPUT: (C*2)   YEAR     = REFERENCE YEAR (ABBREVIATED) OF INPUT
C                             MASTER CONDENSED COLL.-DIEL COEFFT. FILE.
C  OUTPUT: (L*4)   SELTAB   = .TRUE. => PREPARE ELEMENT MASTER FILE
C                             .FALSE.=> DO NOT PREPARE ELEM. MASTER FILE
C  OUTPUT: (L*4)   REPTAB   = .TRUE. => REPLACE EXISTING ELEMENT MASTER
C                                       FILES.
C                           = .FALSE.=> DO NOT REPLACE EXISTING ELEMENT
C                                       MASTER FILES.
C                             ('REPTAB' IS IGNORED IF 'SELTAB'=.FALSE.)
C
C  OUTPUT: (I*4)   IRESO    = 0 IF STANDARD -> STANDARD CONVERSION
C                             1 IF RESOLVED -> UNRESOLVED
C                             2 IF RESOLVED -> RESOLVED
C  OUTPUT: (L*4)   LDTYP()  = .TRUE. IF CLASS SELECTED FOR OUTPUT
C                             .FALSE. OTHERWISE
C
C          (I*4)   NCDEN    = PARAMETER = SIZE OF 'CDENSA()' ARRAY
C                                         MUST BE >= 'NDDEN'
C          (I*4)   NCTIN    = PARAMETER = SIZE OF 'CTINE()' ARRAY
C                                         MUST BE >= 'NDTIN'
C
C          (I*4)   I4UNIT   = FUNCTION (SEE ROUTINE SECTION BELOW)
C
C          (L*4)   BATCH    = .TRUE.  => CURRENT INPUT DATA TO BE RUN IN
C                                        BATCH
C                             .FALSE. => CURRENT INPUT DATA TO BE RUN IN
C                                        FOREGROUND
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          XXFLSH     ADAS      FLUSHES UNIT
C
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    08/10/90
C
C UPDATE:  26/11/90 - ADAS91 - PE BRIDEN - AMENDED 'XXDISP' AND 'D4PAN0'
C                                          ARGUMENT LISTS. THEY NOW IN-
C                                          CLUDE DISPLAY RETURN CODES.
C                                          IF 'RETURN' OR 'END' ENTERED
C                                          ON A PANEL, EXCEPT VIA PFKEY,
C                                          PROGRAM TERMINATES.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C     - FIRST CONVERTED
C
C VERSION: 1.2				DATE: 20-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C     - REMOVED DIAGNOSTIC WRITE STATEMENTS
C
C VERSION: 1.3				DATE: 20-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C     - REMOVED CHECK_PIPE
C
C VERSION: 1.4				DATE: 20-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C     - ADDED SETTING OF REPTAB TO .TRUE.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER PIPEIN, PIPEOU
      INTEGER    NCDEN      , NCTIN
C-----------------------------------------------------------------------
      PARAMETER ( PIPEIN  = 5   , PIPEOU  = 6   )
      PARAMETER ( NCDEN  = 30   , NCTIN  = 55   )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT           , ILOGIC        , I         , J
      INTEGER    NDDEN            , NDTIN         ,
     &           IZ0              , IZE1          , IZE2      ,
     &           MAXD             , MAXT          , ITTYP
      INTEGER    IRESO
      INTEGER    IPANRC           , ILEN          , IABT
C-----------------------------------------------------------------------
      CHARACTER  TITLE*32         , UIDIN*6       , USERID*6  , GRID*20
      CHARACTER  YEAR*2           , P*2           , PLAST*2   , KEY*4
      CHARACTER  CURPOS*8         , MSGTXT*8      , PNAME*8
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      LOGICAL    LPEND            , LTAB
      LOGICAL    SELTAB           , REPTAB        , BATCH , LDTYP(9)
C-----------------------------------------------------------------------
      REAL*8     DENSA(NDDEN)     , TINE(NDTIN)
C-----------------------------------------------------------------------
      CHARACTER  CDENSA(NCDEN)*8  , CTINE(NCTIN)*8
      CHARACTER  DSNIN(50,10)*80  , DSNO(10)*80
C-----------------------------------------------------------------------
C***********************************************************************
C
C-----------------------------------------------------------------------
      IF (NDDEN.GT.NCDEN)
     &   STOP ' D4ISPF ERROR: CDENSA ARRAY TOO SMALL - INCREASE NDDEN'
      IF (NDTIN.GT.NCTIN)
     &   STOP ' D4ISPF ERROR: CTINE ARRAY TOO SMALL - INCREASE NCTIN'
C-----------------------------------------------------------------------
C WRITE ARRAY DIMENSIONS TO IDL - WRITTEN IN MAIN PROG NOW
C-----------------------------------------------------------------------
C      WRITE(PIPEOU,*)NDDEN
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)NDTIN
C      CALL XXFLSH(PIPEOU)

C-----------------------------------------------------------------------
C READ IN USER OPTIONS FROM IDL
C-----------------------------------------------------------------------

      READ(PIPEIN,*)ILOGIC
      IF(ILOGIC.EQ.0)THEN
         LPEND=.FALSE.
         READ(PIPEIN,*)ILOGIC
         IF(ILOGIC.EQ.1)THEN
            LTAB=.TRUE.
            READ(PIPEIN,'(A80)')SAVFIL
         ELSE
            LTAB=.FALSE.
         ENDIF
         READ(PIPEIN,'(A32)')TITLE
         READ(PIPEIN,*)IZ0
         READ(PIPEIN,*)IZE1
         READ(PIPEIN,*)IZE2
         READ(PIPEIN,*)MAXD
         READ(PIPEIN,*)MAXT
         READ(PIPEIN,*)ITTYP
         READ(PIPEIN,*)IRESO
         DO 1 I=1,MAXD
            READ(PIPEIN,*)DENSA(I)
 1       CONTINUE
         DO 2 I=1,MAXT
            READ(PIPEIN,*)TINE(I)
 2       CONTINUE
         READ(PIPEIN,'(A2)')YEAR
         DO 4 J=1,10
            DO 3 I=1,IZE2-IZE1+1
               READ(PIPEIN,'(A80)')DSNIN(I,J)
 3          CONTINUE
 4       CONTINUE
         READ(PIPEIN,'(A80)')DSNIN(I,10)
         DO 5 J=1,9
            READ(PIPEIN,'(A80)')DSNO(J)
            READ(PIPEIN,*)ILOGIC
            IF(ILOGIC.EQ.1)THEN
               LDTYP(J)=.TRUE.
            ELSE
               LDTYP(J)=.FALSE.
            ENDIF
 5       CONTINUE

C**** ALWAYS OUTPUT AND REPLACE REQUESTED FILES ****
         SELTAB = .TRUE.
         REPTAB = .TRUE.
C***************************************************

C         CALL CHECK_PIPE

      ELSE
         LPEND = .TRUE.
      ENDIF

C
C-----------------------------------------------------------------------
C
      RETURN
      END
