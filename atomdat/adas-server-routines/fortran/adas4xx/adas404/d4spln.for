CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4spln.for,v 1.2 2006/02/23 17:27:31 mog Exp $ Date $Date: 2006/02/23 17:27:31 $
CX
      SUBROUTINE D4SPLN( ISWIT  , LSWIT  ,
     &                   NUDMAX , NUTMAX ,
     &                   NDDEN  , NDTIN  , NDZ1V  ,
     &                   MAXT   , MAXD   ,
     &                   IDE    , ITE    , IZE    ,
     &                   DUSR   , TUSR   , IZ1    ,
     &                   DENSR  , TR     , ZIPT   ,
     &                   EIA    , AIPT   ,
     &                   LZRNG  , LDRNG  , LTRNG  ,
     &                   ATTY   , AOUT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4SPLN *********************
C
C  PURPOSE: TO INTERPOLATE/EXTRAPOLATED DATA FROM MASTER CONDENSED  FILE
C           TO THE USER ENTERED TEMPERATURE/DENSITY ARRAY FOR THE SELEC-
C           TED RECOMBINING ION CHARGE.
C           BASED ON ADAS9140.FORT(D1SPLN)
C
C  CALLING PROGRAM: LH404RU
C
C  DATA:
C
C           THE  SOURCE  DATA IS CONTAINED AS  MEMBERS  OF  PARTITIONED
C           DATA SETS AS FOLLOWS:
C
C             1. JETUID.ACD<YR>.DATA
C             2. JETUID.SCD<YR>.DATA
C             3. JETUID.CCD<YR>.DATA
C             4. JETUID.PRB<YR>.DATA
C             5. JETUID.PRC<YR>.DATA
C             6. JETUID.QCD<YR>.DATA
C             7. JETUID.XCD<YR>.DATA
C             8. JETUID.PLT<YR>.DATA
C             9. JETUID.PLS<YR>.DATA
C            10. JETUID.MET<YR>.DATA
C
C
C           WHERE <YR> DENOTES TWO INTEGERS FOR THE YEAR SELECTED.
C
C           THE PARTICULAR TYPE OPENED (1-10) IS SELECTED BY 'ISWIT'
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  ISWIT   = DATA TYPE SELECTOR (SEE ABOVE) (1 -> 10)
C  INPUT : (L*4)  LSWIT   = .TRUE.  => SET OF 'EIA' VALUES PRESENT IN
C                                      MASTER CONDENSED FILE.
C                           .FALSE  => SET OF 'EIA' VALUES NOT PRESENT
C                                      IN MASTER CONDENSED FILE.
C
C  INPUT : (I*4)  NUDMAX  = USER ENTERED VALUES -
C                            MAXIMUM NUMBER OF DENSITY VALUES
C  INPUT : (I*4)  NUTMAX  = USER ENTERED VALUES -
C                            MAXIMUM NUMBER OF TEMPERATURE VALUES
C
C  INPUT : (I*4)  NDDEN   = INPUT MASTER CONDENSED FILE -
C                            MAXIMUM NUMBER OF REDUCED DENSITIES
C  INPUT : (I*4)  NDTIN   = INPUT MASTER CONDENSED FILE -
C                            MAXIMUM NUMBER OF REDUCED TEMPERATURES
C  INPUT : (I*4)  NDZ1V   = INPUT MASTER CONDENSED FILE -
C                            MAXIMUM NUMBER OF CHARGE STATES
C
C  INPUT : (I*4)  MAXT    = USER ENTERED VALUES -
C                            NUMBER OF TEMPERATURE VALUES ENTERED
C  INPUT : (I*4)  MAXD    = USER ENTERED VALUES -
C                            NUMBER OF DENSITY VALUES ENTERED
C
C  INPUT : (I*4)  IDE     = INPUT MASTER CONDENSED FILE -
C                            NUMBER OF REDUCED DENSITIES READ
C  INPUT : (I*4)  ITE     = INPUT MASTER CONDENSED FILE -
C                            NUMBER OF REDUCED TEMPERATURES READ
C  INPUT : (I*4)  IZE     = INPUT MASTER CONDENSED FILE -
C                            NUMBER OF CHARGE STATES READ
C
C  INPUT : (R*8)  DUSR()  = USER ENTERED VALUES -
C                           SET OF 'IUDVAL' ENTERED ELECTRON DENSITIES
C                           (UNITS: CM**-3)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (R*8)  TUSR()  = USER ENTERED VALUES -
C                           SET OF'IUTVAL' ENTERED ELECTRON TEMPERATURES
C                           (UNITS: KELVIN)
C                           DIMENSION: TEMPERATURE/DENSITY PAIR INDEX
C  INPUT : (I*4)  IZ1     = USER ENTERED VALUE -
C                           RECOMBINING ION CHARGE
C
C  INPUT : (R*8)  DENSR() = INPUT MASTER CONDENSED FILE -
C                           SET OF 'IDE' REDUCED DENSITIES (CM-3/Z1**7)
C  INPUT : (R*8)  TR()    = INPUT MASTER CONDENSED FILE -
C                           SET OF 'ITE' REDUCED TEMPERATURES (K/Z1**2)
C  INPUT : (R*8)  ZIPT()  = INPUT MASTER CONDENSED FILE -
C                           SET OF 'IZE' INPUT RECOMBINING ION CHARGES
C
C  INPUT : (R*8)  EIA()   = INPUT MASTER CONDENSED FILE -
C                           IONISATION RATE COEFFTS. - (UNITS: RYDBERGS)
C                           DIMENSION: ION CHARGE
C  INPUT : (R*8)  AIPT(,,)= INPUT MASTER CONDENSED FILE -
C                           RELEVANT COEFFICIENT/POWER DATA FOR 'ISWIT'.
C                           1ST DIMENSION: DENSITY INDEX     ('DENSR()')
C                           2ND DIMENSION: TEMPERATURE INDEX  ('TR()')
C                           3RD DIMENSION: CHARGE STATE INDEX ('ZIPT()')
C
C  OUTPUT: (L*4)  LZRNG(1) = .TRUE.  => 'AOUT()' VALUES FOR CHARGE-
C                                       STATE 'IZ1' INTERPOLATED.
C                          = .FALSE. => 'AOUT()' VALUE  FOR CHARGE-
C                                       STATE 'IZ1' EXTRAPOLATED.
C  OUTPUT: (L*4)  LDRNG()  = .TRUE.  => 'AOUT()' VALUE FOR DENSITY
C                                       INDEX INTERPOLATED.
C                          = .FALSE. => 'AOUT()' VALUE FOR DENSITY
C                                       INDEX EXTRAPOLATED.
C                           DIMENSION: DENSITY INDEX
C  OUTPUT: (L*4)  LTRNG()  = .TRUE.  => 'AOUT()' VALUE FOR TEMPERATURE
C                                       INDEX INTERPOLATED.
C                          = .FALSE. => 'AOUT()' VALUE FOR TEMPERATURE
C                                       INDEX EXTRAPOLATED.
C                           DIMENSION: TEMPERATURE INDEX
C
C  OUTPUT: (R*8)  AOUT(,)  = EXTRAPOLATED/INTERPOLATED  DATA  FOR
C                            USER ENTERED TEMPERATURE/DENSITY ARRAY.
C                           ( STORES LOG10(INTERPOLATED VALUES) )
C                              1ST DIMENSION: TEMPERATURE
C                              2ND DIMENSION: DENSITY
C
C          (I*4)  NUDIM   = PARAMETER = MUST BE GREATER THAN OR EQUAL TO
C                                       'NUDMAX' AND 'NUTMAX'
C
C          (I*4)  NDMAX1  = 'NDDEN'
C          (I*4)  NTMAX1  = 'NDTIN'
C          (I*4)  NZMAX1  = 'NDZ1V'
C          (I*4)  NDMAX2  = 'NUDMAX'
C          (I*4)  NTMAX2  = 'NUTMAX'
C          (I*4)  ITD     = GENERAL USE ARRAY SUBSCRIPT INDEX
C          (I*4)  IDD     = GENERAL USE ARRAY SUBSCRIPT INDEX
C
C          (R*8)  ATTY(,) = WORKING SPACE FOR 3-WAY SPLINE ITERPOLATION
C                           ( STORES LOG10(INTERPOLATED VALUES) )
C                            1ST DIMENSION: TEMPERATURE
C                            2ND DIMENSION: DENSITY
C
C  PARAMETER (I*4)  DLOGMIN = SETS MINIMUM VALUE OF LOG OF COEFFICIENT
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XUFLOW
C          DXSPL1     ADAS      1ST PART OF 3-WAY SPLINE OF INPUT DATA
C          DXSPL2     ADAS      2ND PART OF 3-WAY SPLINE OF INPUT DATA
C          DXSPL3     ADAS      3RD PART OF 3-WAY SPLINE OF INPUT DATA
C
C
C AUTHOR:  WILLIAM J. DICKSON       12/12/92
C          (REFER TO DOCUMENTATION FOR   D1SPLN)
C
C UPDATES FROM D1SPLN:
C
C   12/12/92   ARRAY BOUNDS FOR ATTY SET EQUAL TO THOSE IN DXSPL1 ETC
C              THERFORE INCLUDE ATTY IN CALL LIST
C   22/02/96   HOUSECLEANING AFTER COPY FOR USE WITH LH404RU
C-----------------------------------------------------------------------
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST CONVERTED
C
C
C VERSION : 1.2                        
C DATE    : 23-02-2006
C MODIFIED: Martin O'Mullane
C           - Remove mainframe listing information in columns 72-80.
C
C-----------------------------------------------------------------------
      INTEGER    NUDIM
      REAL*8     DLOGMIN
C-----------------------------------------------------------------------
      PARAMETER( NUDIM = 40    ,  DLOGMIN = -74.0  )
C-----------------------------------------------------------------------
      INTEGER    ISWIT                   ,
     &           NUDMAX                  , NUTMAX        ,
     &           NDDEN                   , NDTIN         , NDZ1V       ,
     &           MAXT                    , MAXD          ,
     &           IDE                     , ITE           , IZE         ,
     &           IZ1
      INTEGER    NDMAX1                  , NTMAX1        , NZMAX1      ,
     &           NDMAX2                  , NTMAX2        , ITD ,
     &           IDD
C-----------------------------------------------------------------------
      LOGICAL    LSWIT                   , LZRNG(1)
C-----------------------------------------------------------------------
      REAL*8     DUSR(NUDMAX)            , TUSR(NUTMAX)  ,
     &           DENSR(NDDEN)            , TR(NDTIN)     , ZIPT(NDZ1V) ,
     &           EIA(50)                 , AOUT(NUTMAX,NUDMAX)
C     REAL*8     ATTY(NUDIM,NUDIM)
      REAL*8     ATTY(NUTMAX,NUDMAX)
      REAL*8     AIPT(NDDEN,NDTIN,NDZ1V)
C-----------------------------------------------------------------------
      LOGICAL    LDRNG(NUDMAX)           , LTRNG(NUTMAX)
C-----------------------------------------------------------------------
C
CX      CALL XUFLOW(0)
C
C-----------------------------------------------------------------------
C
      IF (NUDIM.LT.NUDMAX .OR. NUDIM.LT.NUTMAX)
     &   STOP ' D1SPLN ERROR: NUDIM < NUDMAX OR NUTMAX- INCREASE NUDIM'
C
C-----------------------------------------------------------------------
C
      NDMAX1 = NDDEN
      NTMAX1 = NDTIN
      NZMAX1 = NDZ1V
C
      NDMAX2 = NUDMAX
      NTMAX2 = NUTMAX
C-----------------------------------------------------------------------
C PERFORM THREE WAY SPLINE ON THE INPUT DATA.
C-----------------------------------------------------------------------
             CALL DXSPL1(  ISWIT   , LSWIT  , IZ1      ,
     &                     NDMAX2  , NTMAX2 ,
     &                     NDMAX1  , NTMAX1 , NZMAX1   ,
     &                     IDE     , ITE    , IZE      ,
     &                     TR      , ZIPT   , EIA      ,
     &                     AIPT    ,
     &                     LZRNG(1),
     &                     ATTY
     &                  )
C
C-----------------------------------------------------------------------
C
             CALL DXSPL2( ISWIT    , LSWIT  , IZ1      ,
     &                    NDMAX2   , NTMAX2 ,
     &                    NDMAX1   ,
     &                    IDE      , ITE    ,
     &                    MAXD     , DENSR  , DUSR     ,
     &                    LDRNG    ,
     &                    ATTY
     &                  )
C
C-----------------------------------------------------------------------
C
             CALL DXSPL3( ISWIT    , LSWIT  , IZ1      ,
     &                    NDMAX2   , NTMAX2 ,
     &                               NTMAX1 ,
     &                    MAXD     , ITE    ,
     &                    MAXT     , TR     , TUSR     , EIA(IZ1) ,
     &                    LTRNG    ,
     &                    ATTY
     &                  )
C-----------------------------------------------------------------------
C ASSIGN SPLINE OUTPUT RESULTS TO OUTPUT TEMP/DEN PAIR ARRAY.
C-----------------------------------------------------------------------
          DO 1 ITD=1,MAXT
            DO 2 IDD = 1,MAXD
               AOUT(ITD,IDD) = DMAX1( ATTY(ITD,IDD) , DLOGMIN)
    2       CONTINUE
    1     CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
