CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas404/d4lbal.for,v 1.1 2004/07/06 13:14:09 whitefor Exp $ Date $Date: 2004/07/06 13:14:09 $
CX
      SUBROUTINE D4LBAL( NDZ    ,          NDDEN , NDTIN ,
     &                   IZ1    , IZ2    , MAXD  , MAXT  ,
     &                   LDACDL , LDSCDL ,
     &                   LTACDL , LTSCDL ,
     &                   LDIBAL ,
     &                   LTIBAL
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: D4LBAL *********************
C
C  PURPOSE:  TO IDENTIFY THE TEMPERATURES AND DENSITY FOR WHICH THE
C            CALCULATION OF IONISATION BALANCES INVOLVED THE USE OF
C            EXTRAPOLATED DATA.
C
C  CALLING PROGRAM: ADAS404
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NDZ     = NUMBER OF CHARGE STATES
C  INPUT : (I*4)  NDDEN   = MAXIMUM NUMBER OF INPUT DENSITIES
C  INPUT : (I*4)  NDTIN   = MAXIMUM NUMBER OF INPUT TEMPERATURES
C
C  INPUT : (I*4)  IZ1     = MINIMUM ALLOWED IONIC CHARGE + 1
C  INPUT : (I*4)  IZ2     = MAXIMUM ALLOWED IONIC CHARGE + 1
C  INPUT : (I*4)  MAXD    = NUMBER OF USER ENTERED DENSITIES (<=NDDEN)
C  INPUT : (I*4)  MAXT    = NUMBER OF USER ENTERED TEMPERATURES(<=NDTIN)
C
C  INPUT : (L*4)  LDACDL(,)=.TRUE. => RECOMBINATION COLL.-DIEL COEFFT.
C                                     VALUE FOR DENSITY     INDEX AND
C                                     CHARGE INTERPOLATED.
C                          =.FALSE.=> RECOMBINATION COLL.-DIEL COEFFT.
C                                     VALUE FOR DENSITY     INDEX AND
C                                     CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: DENSITY     INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C  INPUT : (L*4)  LDSCDL(,)=.TRUE. => IONIZATION    COLL.-DIEL COEFFT.
C                                     VALUE FOR DENSITY     INDEX AND
C                                     CHARGE INTERPOLATED.
C                          =.FALSE.=> IONIZATION    COLL.-DIEL COEFFT.
C                                     VALUE FOR DENSITY     INDEX AND
C                                     CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: DENSITY     INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C
C  INPUT : (L*4)  LTACDL(,)=.TRUE. => RECOMBINATION COLL.-DIEL COEFFT.
C                                     VALUE FOR TEMPERATURE INDEX AND
C                                     CHARGE INTERPOLATED.
C                          =.FALSE.=> RECOMBINATION COLL.-DIEL COEFFT.
C                                     VALUE FOR TEMPERATURE INDEX AND
C                                     CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: TEMPERATURE INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C  INPUT : (L*4)  LTSCDL(,)=.TRUE. => IONIZATION    COLL.-DIEL COEFFT.
C                                     VALUE FOR TEMPERATURE INDEX AND
C                                     CHARGE INTERPOLATED.
C                          =.FALSE.=> IONIZATION    COLL.-DIEL COEFFT.
C                                     VALUE FOR TEMPERATURE INDEX AND
C                                     CHARGE EXTRAPOLATED.
C                           1ST DIMENSION: TEMPERATURE INDEX
C                           2ND DIMENSION: CHARGE-STATE INDEX
C
C  OUTPUT: (L*4)  LDIBAL() =.TRUE. => IONIZATION BALANCES FOR DENSITY
C                                     INVOLVE NOT EXTRAPOLATION.
C                          =.FALSE.=> IONIZATION BALANCES FOR DENSITY
C                                     INVOLVE EXTRAPOLATION  IN  SOME
C                                     PART OF THEIR CALCULTION.
C                           1ST DIMENSION: DENSITY INDEX
C  OUTPUT: (L*4)  LTIBAL() =.TRUE. => IONIZATION BALANCES FOR TEMP'TURE
C                                     INVOLVE NOT EXTRAPOLATION.
C                          =.FALSE.=> IONIZATION BALANCES FOR TEMP'TURE
C                                     INVOLVE EXTRAPOLATION  IN  SOME
C                                     PART OF THEIR CALCULTION.
C                           1ST DIMENSION: TEMPERATURE INDEX
C
C          (I*4)  NZ      = NUMBER OF CHARGE STATES TO BE ASSESSED
C          (I*4)  ID      = ARRAY SUBSCRIPT USED FOR DENSITY INDEX
C          (I*4)  IT      = ARRAY SUBSCRIPT USED FOR TEMPERATURE INDEX
C          (I*4)  IZ      = ARRAY SUBSCRIPT USED FOR CHARGE-STATE INDEX
C
C          (L*4)  LS      = .TRUE.  => NO EXTRAPOLATION
C                           .FALSE. => EXTRAPOLATION
C
C NOTE:
C
C ROUTINES: NONE
C
C AUTHOR:   PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:     30/01/91 - ADAS91 ROUTINE
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 11-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDZ     , NDDEN    , NDTIN      ,
     &           IZ1     , IZ2      , MAXD       , MAXT
      INTEGER    NZ      , ID       , IT         , IZ
C-----------------------------------------------------------------------
      LOGICAL    LS
C-----------------------------------------------------------------------
      LOGICAL    LDIBAL(NDDEN)             , LTIBAL(NDTIN)          ,
     &           LDACDL(NDDEN,NDZ)         , LDSCDL(NDDEN,NDZ)      ,
     &           LTACDL(NDTIN,NDZ)         , LTSCDL(NDTIN,NDZ)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      NZ = IZ2 - IZ1 + 1
C
C-----------------------------------------------------------------------
C ANALYSE DENSITIES
C-----------------------------------------------------------------------
C
         DO 1 ID = 1,MAXD
            LS = .TRUE.
               DO 2 IZ = 1,NZ
                  IF ( (.NOT.LDACDL(ID,IZ)) .OR. (.NOT.LDSCDL(ID,IZ)) )
     &               LS = .FALSE.
    2          CONTINUE
            LDIBAL(ID) = LS
    1    CONTINUE
C
C-----------------------------------------------------------------------
C ANALYSE TEMPERATURES
C-----------------------------------------------------------------------
C
         DO 3 IT = 1,MAXT
            LS = .TRUE.
               DO 4 IZ = 1,NZ
                  IF ( (.NOT.LTACDL(IT,IZ)) .OR. (.NOT.LTSCDL(IT,IZ)) )
     &               LS = .FALSE.
    4          CONTINUE
            LTIBAL(IT) = LS
    3    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
