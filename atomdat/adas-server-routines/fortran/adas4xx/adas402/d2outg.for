CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2outg.for,v 1.1 2004/07/06 13:13:20 whitefor Exp $ Date $Date: 2004/07/06 13:13:20 $
CX
       SUBROUTINE D2OUTG(TITLE,TITLF,TITLM,TR,RATA,ITE,ZIPT,IZMAX,
     & Z1,TRMX,RATMX,NMX,TRS,RATS,MAXT,IGOPT,ISWIT,XMIN,XMAX,YMIN,YMAX,
     & ICLASS)
       PARAMETER (NKDIM=60)
       IMPLICIT REAL*4(A-H,O-Z)
       INTEGER IVAL
       REAL*8 TR,RATA,ZIPT,Z1,TRMX,RATMX,TRS,RATS,XMIN,XMAX,YMIN,YMAX
       CHARACTER*40 ISPEC
       CHARACTER*15 GRTIT(2)
       CHARACTER*23 XTIT,YTIT
       CHARACTER TITLE*40, TITLF*120, TITLM*80, ZCHARS*9, CHARS*16
C-----------------------------------------------------------------------
C  GRAPHIC ROUTINE FOR USE BY ADAS402
C
C  PROVIDES COMPARATIVE GRAPH OF CONDENSED MASTER DATA     (SOLID CURVE)
C                                SPLINE INTERPOLATED DATA  (CROSSES)
C                                MINIMAX FIT TO DATA       (DASH CURVE )
C
C  PLOT IS LOG10(RATE COEFFT.(CM**3 SEC-1))  VERSUS LOG10(TE(K)/Z1**2))
C
C  ****** H.P. SUMMERS, JET              12 FEB 1990   **********
C
C UPDATE: P.E. BRIDEN, JET/TESSELLA      08 OCT 1990   **********
C         1) RE-NAMED SUBROUTINE
C
C UPDATE: P.E. BRIDEN, JET/TESSELLA      16 NOV 1990   **********
C         1) RE-POSITIONED SPLINE OUTPUT TITLE.
C         2) SWITCH OFF GHOST80 ERRORS WHEN PLOTTING
C
C UPDATE: P.E. BRIDEN, JET/TESSELLA      25 JAN 1991   **********
C         1) ARRAY DECLARATION: 'TRS(40)' INSTEAD OF 'TRS(60)'
C         2) ARRAY DECLARATION: 'RATS(40)' INSTEAD OF 'RATS(60)'
C
C UPDATE: P.E. BRIDEN, JET/TESSELLA      22 NOV 1991   **********
C         1) DECLARED GRID AND PIC AS CHARACTER*1 (BLANK)
C         2) MADE FILNAM/PICSAV ARGUMENT LIST COMPATIBLE WITH GHOST8
C
C-----------------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 30-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C               - COMMENTED OUT IBM SPECIFIC CODE AND REPLACED WITH PIPE
C                 WRITES
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
       DIMENSION TR(NKDIM),RATA(NKDIM,NKDIM),ZIPT(NKDIM)
       DIMENSION TRMX(100),RATMX(100)
       DIMENSION TRS(40),RATS(40),XA4(100),YA4(100),YTIT(7)
       INTEGER PIPEOU
       PARAMETER ( PIPEOU = 6)
       CHARACTER GRID*1 ,PIC*1
       DATA ISPEC/'MASTER CONDENSED FILE DATA PLOTTING    '/
       DATA GRTIT(1)/'               '/
       DATA XTIT   /'  TE   (EV)            '/
       DATA YTIT(1)/'  ACD (CM**3 SEC-1)    '/
       DATA YTIT(2)/'  SCD (CM**3 SEC-1)    '/
       DATA YTIT(3)/'  CCD (CM**3 SEC-1)    '/
       DATA YTIT(4)/'  PRB (WATT CM**3)     '/
       DATA YTIT(5)/'  PLT (WATT CM**3)     '/
       DATA YTIT(6)/'  PRC (WATT CM**3)     '/
       DATA YTIT(7)/'  PLS (WATT CM**3)     '/
       DATA GRID/' '/,PIC/' '/
C       CALL ERRORS(0)
C       CALL PAPER(1)
C       CALL FILNAM(GRID)
C       CALL DEVON
C       CALL PSPACE(0.1,0.9,0.1,0.9)
       IF(IGOPT.NE.1)THEN
           XLOW=XMIN
           XHIGH=XMAX
           YLOW=YMIN
           YHIGH=YMAX
       ELSE
               XLOW=1.0E0
               XHIGH=1.0E5
           IF(ISWIT.EQ.1)THEN
               YLOW=1.0E-14
               YHIGH=1.0E-10
           ELSEIF(ISWIT.EQ.2)THEN
               YLOW=1.0E-14
               YHIGH=1.0E-7
           ELSEIF(ISWIT.EQ.3)THEN
               YLOW=1.0E-12
               YHIGH=1.0E-5
           ELSEIF(ISWIT.EQ.4)THEN
               YLOW=1.0E-32
               YHIGH=1.0E-25
           ELSEIF(ISWIT.EQ.5)THEN
               YLOW=1.0E-33
               YHIGH=1.0E-24
           ELSEIF(ISWIT.EQ.6)THEN
               YLOW=1.0E-29
               YHIGH=1.0E-21
           ELSEIF(ISWIT.EQ.7)THEN
               YLOW=1.0E-12
               YHIGH=1.0E-05
           ENDIF
       ENDIF
C       CALL MAPXYL(XLOW,XHIGH,YLOW,YHIGH)
C       CALL SCAXYL
C       CALL GRAXYL
C-----------------------------------------------------------------------
C  PLOT CONDENSED MASTER FILE DATA FOR PARTICULAR REDUCED DENSITY
C-----------------------------------------------------------------------
       IZ1 = Z1
       DO 12 IZ=1,IZMAX
          IF( IZ1.EQ.ZIPT(IZ) ) INDXZ1 = IZ
   12  CONTINUE
C
       IZ1MIN = MAX( INDXZ1 - 3 , 1     )
       IZ1MAX = MIN( INDXZ1 + 3 , IZMAX )
C
       ISTAG = 1
       WRITE(PIPEOU,*)100
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)IZ1MIN
       CALL XXFLSH(PIPEOU)
       WRITE(PIPEOU,*)IZ1MAX
       CALL XXFLSH(PIPEOU)
       DO 15 IZ = IZ1MIN , IZ1MAX
          WRITE(ZCHARS,100)ZIPT(IZ)
          ICOUNT = 0
          DO 10 I=1,ITE
             IF( ( RATA(IZ,I).GE.YLOW .AND. RATA(IZ,I).LE.YHIGH )
     &            .AND.
     &            ( TR(I).GE.XLOW .AND. TR(I).LE.XHIGH ) ) THEN
                ICOUNT = ICOUNT + 1
                XA4(ICOUNT)=TR(I)
                YA4(ICOUNT)=RATA(IZ,I)
             END IF
 10       CONTINUE
C     CALL BROKEN(10,5,10,5)
          WRITE(PIPEOU,*)ICOUNT
          CALL XXFLSH(PIPEOU)
          IF( ICOUNT.GT.0 ) THEN
             DO 11 I=1,ICOUNT
                WRITE(PIPEOU,*)XA4(I)
                CALL XXFLSH(PIPEOU)
                WRITE(PIPEOU,*)YA4(I)
                CALL XXFLSH(PIPEOU)
 11          CONTINUE
C     CALL PTJOIN(XA4,YA4,1,ICOUNT,0)
             XOLD=XA4(ICOUNT-ISTAG)
             YOLD=YA4(ICOUNT-ISTAG)
             WRITE(PIPEOU,*)XOLD
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,*)YOLD
             CALL XXFLSH(PIPEOU)
             WRITE(PIPEOU,'(A)')ZCHARS
             CALL XXFLSH(PIPEOU)
C     CALL PLOTCS(XOLD,YOLD,ZCHARS,9)
          ENDIF
          ISTAG = ISTAG + 1
 15    CONTINUE
C-----------------------------------------------------------------------
C  PLOT MINIMAX CURVE
C-----------------------------------------------------------------------
       WRITE(PIPEOU,*)NMX
       CALL XXFLSH(PIPEOU)
       IF(NMX.GE.1)THEN
           DO 20 I=1,NMX
              XA4(I)=TRMX(I)
              YA4(I)=RATMX(I)
              WRITE(PIPEOU,*)XA4(I)
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*)YA4(I)
              CALL XXFLSH(PIPEOU)
 20        CONTINUE
C           CALL BROKEN(5,5,5,5)
C           CALL MOVEPT(XA4(1),YA4(1))
C           CALL PTJOIN(XA4,YA4,1,NMX,0)
       ENDIF
C-----------------------------------------------------------------------
C  PLOT SPLINE INTERPOLATION CURVE
C-----------------------------------------------------------------------
C       CALL FULL
       WRITE(PIPEOU,*)MAXT
       CALL XXFLSH(PIPEOU)
       DO 30 I=1,MAXT
          XA4(I)=TRS(I)
          YA4(I)=RATS(I)
          WRITE(PIPEOU,*)XA4(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*)YA4(I)
          CALL XXFLSH(PIPEOU)
 30    CONTINUE
       WRITE(CHARS,101)Z1
C       CALL PTGRAF(XA4,YA4,1,MAXT,162)
C
C  AMENDMENT - PE BRIDEN - 16/11/90
C
CORIG  XOLD=XA4(1)
CORIG  YOLD=YA4(1)
CORIG  CALL PLOTCS(XOLD,YOLD,CHARS,16)
C
       IVAL=0
C
       DO 31 I=1,MAXT
          IF (IVAL.EQ.0) THEN
             IF ( (XA4(I).GT.XLOW) .AND. (XA4(I).LT.XHIGH)
     &            .AND.
     &            (YA4(I).GT.YLOW) .AND. (YA4(I).LT.YHIGH) ) IVAL=I
          ENDIF
 31    CONTINUE
C     
       WRITE(PIPEOU,*)IVAL
       CALL XXFLSH(PIPEOU)
       IF (IVAL.GT.0) THEN
          XOLD=XA4(IVAL)
          YOLD=YA4(IVAL)
          WRITE(PIPEOU,*)XOLD
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*)YOLD
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,'(A)')CHARS
          CALL XXFLSH(PIPEOU)
C     CALL PLOTCS(XOLD,YOLD,CHARS,16)
       ENDIF
C-----------------------------------------------------------------------
C  PLOT INFORMATION DATA
C-----------------------------------------------------------------------
C       CALL CSPACE(0.0,1.0,0.0,1.0)
C       CALL CTRMAG(10)
C
C       CALL CTRORI(90.0)
C       CALL PLACE(50,3)
C       CALL TYPECS(YTIT(ICLASS),23)
C       CALL CTRORI(0.0)
       WRITE(PIPEOU,'(A)')YTIT(ICLASS)
       CALL XXFLSH(PIPEOU)
C
C       CALL UNDLIN(1)
C       CALL PLACE(3,3)
C       CALL TYPECS(TITLE,40)
C       CALL UNDLIN(0)
       WRITE(PIPEOU,'(A)')TITLE
       CALL XXFLSH(PIPEOU)
C
C       CALL PLACE(20,5)
C       CALL TYPECS(TITLF,80)
       WRITE(PIPEOU,'(A)')TITLF
       CALL XXFLSH(PIPEOU)
C
C       CALL PLACE(20,6)
C       CALL TYPECS(TITLM,80)
       WRITE(PIPEOU,'(A)')TITLM
       CALL XXFLSH(PIPEOU)
C
C       CALL PLACE(60,69)
C       CALL TYPECS(XTIT,23)
       WRITE(PIPEOU,'(A)')XTIT
       CALL XXFLSH(PIPEOU)
C
C       CALL FRAME
C   95  CALL PICSAV(GRID,PIC)
C       CALL FRAME
       RETURN
  100  FORMAT('  Z1=',F4.1)
  101  FORMAT('  Z1=',F4.1,'(MODEL)')
      END
