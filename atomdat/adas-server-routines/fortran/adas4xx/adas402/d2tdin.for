CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2tdin.for,v 1.1 2004/07/06 13:13:41 whitefor Exp $ Date $Date: 2004/07/06 13:13:41 $
CX
      SUBROUTINE D2TDIN( DSFULL
     &                 , ITDIMD , ITMAXD , IDMAXD
     &                 , TEMP   , DENS   , LRESO
     &                 )
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C PURPOSE : TO EXTRACT TEMPERATURE AND DENSITY FROM ADF11 FILE FOR
C           DISPLAY ON ADAS402 PROCESSING SCREEN
C
C INPUT  : (C*80) DSFULL    = INPUT DATASET NAME
C          (I*4)  ITDIMD    = MAXIMUM NUMBER OF DATA TEMP & DENS
C          (L*4)  LRESO     = .TRUE. => RESOLVED DATASET
C                             .FALSE.=> UNRESOLVED DATASET
C
C OUTPUT : (I*4)  ITMAXD    = NUMBER OF DATA TEMPS
C          (I*4)  IDMAXD    = NUMBER OF DATA DENS
C          (R*8)  TEMP()    = DATA ELECTRON TEMPERATURES (EV)
C          (R*8)  DENS()    = DATA ELECTRON DENSITIES (CM-3)
C
C PROGRAM: 
C
C-----------------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 29-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST WRITTEN
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      LOGICAL LRESO
C
      INTEGER   IREAD, ITDIMD
      PARAMETER ( IREAD = 12 )
C
      REAL*8    TEMP(ITDIMD)  , DENS(ITDIMD)
C     
      INTEGER   IZMAX , IDMAXD , ITMAXD , IZ1MIN , IZ1MAX, ID, IT
C     
      CHARACTER DSFULL*80, STRING*80
C-----------------------------------------------------------------------
      
      OPEN(UNIT=IREAD,FILE=DSFULL,STATUS='UNKNOWN',ERR=9999)
C     
      READ(IREAD,1010) IZMAX , IDMAXD , ITMAXD , IZ1MIN , IZ1MAX
C     
      READ(IREAD,1020) STRING
C IF RESOLVED, OMIT TWO EXTRA STRINGS
      IF(LRESO)THEN
         READ(IREAD,1020) STRING
         READ(IREAD,1020) STRING
      ENDIF
      READ(IREAD,1040) ( DENS(ID) , ID = 1 , IDMAXD )
      READ(IREAD,1040) ( TEMP(IT)  , IT = 1 , ITMAXD )
C-----------------------------------------------------------------------
C VALUES IN FILE ARE ACTUALLY DLOG10 OF THE DENSITY AND TEMPS SO CONVERT
C-----------------------------------------------------------------------
      DO 1 ID=1,IDMAXD
         DENS(ID) = 10.0 ** DENS(ID)
 1    CONTINUE
      DO 2 IT=1,ITMAXD
         TEMP(IT) = 10.0 ** TEMP(IT)
 2    CONTINUE
C     
      CLOSE(IREAD)
C     
 9999 CONTINUE
      RETURN
C
C-----------------------------------------------------------------------
C
 1000  FORMAT('FILE = ',1A30)
 1010  FORMAT(5I5)
 1020  FORMAT(1A80)
 1040  FORMAT(8F10.5)
C
C-----------------------------------------------------------------------
C
       END
