CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2data.for,v 1.4 2004/07/06 13:13:03 whitefor Exp $ Date $Date: 2004/07/06 13:13:03 $
CX
       SUBROUTINE D2DATA( DSFULL , TITLF  , IFAIL  , LRESO ,IPRT , IGRND
     &                  , IZ0    , IZ1    , ICLASS , ITMAX , IEVCUT
     &                  , ITDIMD , ITMAXD , IDMAXD , IZMAXD
     &                  , DTEV   , DDENS
     &                  , DTEVD  , DDENSD , DRCOFD , ZDATA
     &                  , DRCOFI , YEAR
     &                  )

       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C PURPOSE : TO EXTRACT adf11 COLLISIONAL DIELECTRONIC DATA 
C
C NOTE    : THE SOURCE DATA IS CONTAINED IN CENTRAL (OR USER) ADAS
C           DATASETS AS:
C           /home/adas/adas/adf11/<typ><yr>/<typ><yr>_<el>.<fl>.dat 
C
C           where, <yr>  = nominal year of data
C                  <el>  = element name
C                  <typ> = type of data
C                  <fl>  = optional filter for power data
C           
C           The classes are
C              iclass = 1 : acd
C              iclass = 2 : scd
C              iclass = 3 : ccd
C              iclass = 4 : prb
C              iclass = 5 : prc
C              iclass = 6 : plt
C              iclass = 7 : pls
C
C
C           This routine was originally used to extract data for the SANCO
C           impurity transport code where filenames were constructed from
C           a userid, the year and a cutoff energy. The routine was 
C           rewritten so that the full adf11 filename was passed. Hence some
C           of the inputs are unecessary but are retained for backwards
C           compatibility in calling codes.    
C  
C INPUT  : (C*2)  YEAR      = YEAR OF DATA (not used)
C          (C*2)  YEARDF    = DEFAULT YEAR OF DATA IF REQUESTED YEAR
C                             DOES NOT EXIST
C          (I*4)  IZ0       = NUCLEAR CHARGE 
C          (I*4)  IZ1       = MINIMUM ION CHARGE + 1 (not used)
C          (I*4)  ICLASS    = CLASS OF DATA (1 - 6)
C          (I*4)  ITMAX     = NUMBER OF ( DTEV() , DDENS() ) PAIRS
C          (I*4)  IEVCUT    = ENERGY CUT-OFF (EV) (not used)
C          (R*8)  DTEV()    = DLOG10(ELECTRON TEMPERATURES (EV))
C          (R*8)  DDENS()   = DLOG10(ELECTRON DENSITIES (CM-3))
C          (I*4)  IPRT      = INDEX OF PARENT STATE
C          (I*4)  IGRND     = INDEX OF GROUND STATE
C          (L*4)  LRESO     = FLAG WHETHER RESOLVED DATA
C
C OUTPUT : (C*120)TITLF     = INFORMATION STRING
C          (I*4)  ITDIMD    = MAXIMUM NUMBER OF DATA TEMP & DENS
C          (I*4)  ITMAXD    = NUMBER OF DATA DTEVD()
C          (I*4)  IDMAXD    = NUMBER OF DATA DDENS()
C          (I*4)  IZMAXD    = NUMBER OF DATA ZDATA()
C          (I*4)  ITDIMD    = MAXIMUM NUMBER OF DATA TEMP & DENS
C          (I*4)  ZDATA()   = Z1 CHARGES IN DATASET
C          (I*4)  IFAIL     = -1   IF ROUTINE SUCCESSFUL BUT THE DEFAULT
C                                  YEAR FOR THE DATA WAS USED.
C                           = 0    IF ROUTINE SUCCESSFUL - DATA FOT THE
C                                  REQUESTED YEAR USED.
C                           = 1    IF ROUTINE OPEN STATEMENT FAILED
C          (R*8)  DTEVD()   = DLOG10(DATA ELECTRON TEMPERATURES (EV))
C          (R*8)  DDENSD()  = DLOG10(DATA ELECTRON DENSITIES (CM-3))
C          (R*8)  DRCOFD()  = DLOG10(DATA RATE COEFFICIENTS (CM-3/S))
C          (R*8)  DRCOFI()  = INTERPOLATION OF DRCOFD(,,) FOR
C                             DTEV() & DDENS()
C
C PROGRAM: (C*80) STRING    = GENERAL VARIABLE
C          (C*80) BLANK     = BLANK STRING
C          (C*2)  YEARSV    = LAST YEAR USED IN THIS ROUTINE
C          (I*4)  IREAD     = INPUT STREAM FOR OPEN STATEMENT
C          (I*4)  IZ0SV     = LAST IZ0 USED IN THIS ROUTINE
C          (I*4)  ICLSV     = LAST ICLASS USED IN THIS ROUTINE
C          (I*4)  INDXZ1    = LOCATION OF IZ1 IN ZDATA()
C          (I*4)  LCK       = MUST BE GREATER THAN 'ITMAXD' & 'IDMAXD'
C                             & 'ITMAX' - ARRAY SIZE FOR SPLINE CALCS.
C          (R*8)  A()       = GENERAL ARRAY
C          (R*8)  DRCOF0(,) = INTERPOLATION OF DRCOFD(,,) W.R.T DTEV()
C          (L*8)  LEXIST    = TRUE --- FILE TO OPEN EXISTS ELSE NOT
C
C PE BRIDEN = ADDED VARIABLES (14/01/91)
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                             SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO X-AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO X-AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C
C          (R*8)  DY()    = SPLINE INTERPOLATED DERIVATIVES
C
C          (R*8 ADAS FUNCTION - 'R8FUN1' ( X -> X) )
C
C PE BRIDEN = ADDED VARIABLES (23/04/93)
C
C          (I*4 ADAS FUNCTION - 'I4UNIT' (OUTPUT STREAM))
C
C AUTHOR : JAMES SPENCE (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/80
C          JET  EXT. 4866
C
C DATE   : 22/02/90
C
C DATE   : 21/08/90 PE BRIDEN - REVISION: SEQUA(43) CHANGED ('TE'->'TC')
C
C DATE   : 08/10/90 PE BRIDEN - REVISION: RENAMED SUBROUTINE
C
C DATE   : 12/11/90 PE BRIDEN - CORRECTION: MOVE THE SETTING OF 'INDXZ1'
C                                           TO AFTER THE  '20 CONTINUE'
C                                           STATEMENT.   ALSO SAVE  THE
C                                           VALUE OF 'IZ1MIN'.
C
C DATE   : 14/01/91 PE BRIDEN - ADAS91:     CALLS TO NAG SPLINE ROUTINES
C                                           'E01BAF' & 'E02BBF' REPLACED
C                                           BY  CALLS   TO  ADAS  SPLINE
C                                           ROUTINE 'XXSPLE'.
C
C DATE   : 25/06/91 PE BRIDEN - CORRECTION: CHANGED FOLLOWING DIMENSION:
C                                            'DIMENSION DRCOFI(ITDIMD)'
C                                           TO
C                                            'DIMENSION DRCOFI(ITMAX)'
C
C DATE   : 07/08/91 PE BRIDEN - ADDED ERROR HANDLING IF THE OPEN STATE-
C                               MENT FAILS. (IFAIL=1 RETURNED)
C
C DATE   : 27/04/92 PE BRIDEN - ADDED DEFAULT YEAR FOR DATA IF REQUESTED
C                               YEAR DOES NOT EXIST. (ADDED 'YEARDF')
C                               INTRODUCED IFAIL = -1 IF DEFAULT YEAR
C                               WAS USED AND NOT THE REQUESTED YEAR.
C
C DATE   : 10/03/93 PE BRIDEN - ALLOWED INPUT DATA SETS TO BE ACCESSED
C                               FROM ANY USERID (DEFAULT = JETSHP)
C                               - INTRODUCED USERID VARIABLE AND CALL
C                                 TO XXUID.
C
C DATE   : 23/04/93 PE BRIDEN - ADDED I4UNIT FUNCTION TO WRITE
C                               STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  14/09/94 - PE BRIDEN - ADAS91: ADDED CHECK TO MAKE SURE THAT
C                                         ITMAX, ITMAXD AND IDMAXD ARE
C                                         IN RANGE (I.E. <= LCK).
C
C UPDATE:  16/08/96 - PE BRIDEN - ADAS91: MINOR MOD - IF DEFAULT DATA
C                                         IS NOT FOUND THEN ASSIGN TITLF
C                                         BEFORE EXITING WITH AN ERROR.
C
C-----------------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 28-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C VERSION: 1.2				DATE: 14-02-97
C MODIFIED: RICHARD MARTIN
C		- CHANGED DEFINITION OF 'BLANKS' TO 80 CHARACTERS ONLY
C
C VERSION: 1.3				DATE: 26-10-97
C MODIFIED: LORNE HORTON (JET)
C		- CHANGED EXTRAPOLATION ALGORITHM
C
C VERSION: 1.4				DATE: 23-07-99
C MODIFIED: Martin O'Mullane (JET)
C		- Changed the test for checking whether the file was opened.
C               - Removed commented out code for constructing filenames
C               - Modified the comments at the top of the file
C               - Converted code to implicit none
C               - Increased LCK to 200
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       INTEGER   L1   , IREAD  , LCK     , IDUMAX 
C-----------------------------------------------------------------------
       PARAMETER ( L1    =  1 )
       PARAMETER ( IREAD = 12 , LCK = 200, IDUMAX = 60 )
C-----------------------------------------------------------------------
       INTEGER   IOPT    , ITDIMD  , ITMAX
       INTEGER   I4UNIT  ,  ICLASS , ID    ,  IDMAXD   ,      
     &           IEVCUT  ,  IFAIL  , IG    ,  IGRND    ,
     &           INDXZ1  ,  IP     , IPRT  ,  IT       ,
     &           ITMAXD  ,  IZ     , IZ0   ,  IZ1      ,
     &           IZ1MAX  ,  IZ1MIN , IZMAX ,  IZMAXD
C-----------------------------------------------------------------------
       LOGICAL   LEXIST  , LSETX   , LRESO
C-----------------------------------------------------------------------
       CHARACTER YEAR*2    , TITLF*120
       CHARACTER STRING*80 , BLANKS*80
       CHARACTER DSFULL*80 , dsfullsv*80
C-----------------------------------------------------------------------
       REAL*8 DTEV(ITMAX)   , DDENS(ITMAX)
       REAL*8 DTEVD(ITDIMD) , DDENSD(ITDIMD) , ZDATA(ITDIMD)
       REAL*8 DRCOFD(ITDIMD,ITDIMD,ITDIMD)
       REAL*8 DRDUM(IDUMAX,IDUMAX)
       REAL*8 DRCOFI(ITMAX)
       REAL*8 A(LCK)
       REAL*8 DY(LCK)
       REAL*8 DRCOF0(LCK,LCK)
C-----------------------------------------------------------------------
       LOGICAL   LINTRP(LCK)
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       EXTERNAL  R8FUN1
C-----------------------------------------------------------------------
       SAVE      IZ1MIN
       SAVE      DSFULLSV
C-----------------------------------------------------------------------
       DATA BLANKS/'
     &                            '/
       DATA dsfullsv/'                              '/

C-----------------------------------------------------------------------
C CHECK DIMENSIONS
C-----------------------------------------------------------------------

       IF (LCK.LT.ITMAX) THEN
          WRITE(I4UNIT(-1),2000)ITMAX,LCK
          STOP
       ENDIF
          

C-----------------------------------------------------------------------
C CHECK IF CALL IS TO THE CURRENTLY OPEN DATASET
C-----------------------------------------------------------------------
       
       if (dsfullsv.eq.dsfull) goto 20
       dsfullsv = dsfull

       IFAIL  = 0

       INQUIRE(FILE=DSFULL,EXIST=LEXIST)

       TITLF=BLANKS
       WRITE(TITLF,1000) DSFULL(1:73)

       IF( .NOT.LEXIST ) GOTO 9999
       OPEN(UNIT=IREAD,FILE=DSFULL,STATUS='UNKNOWN',ERR=9999)

       READ(IREAD,1010) IZMAX , IDMAXD , ITMAXD , IZ1MIN , IZ1MAX

       IF(LRESO)THEN
          READ(IREAD,1020)STRING
          READ(IREAD,1020)STRING
       ENDIF
       READ(IREAD,1020) STRING
       READ(IREAD,1040) ( DDENSD(ID) , ID = 1 , IDMAXD )
       READ(IREAD,1040) ( DTEVD(IT)  , IT = 1 , ITMAXD )

       IZMAXD = 0

C-----------------------------------------------------------------------
C IF RESOLVED DATA THEN CHECK WHETHER THE PARENT AND GROUND STATES MATCH
C IPRT AND IGRND FOR EACH SET OF DATA.
C-----------------------------------------------------------------------
       IF (LRESO) THEN
 90       CONTINUE
          READ(IREAD,1020)STRING
          IF(STRING(22:22).NE.'/') GOTO 160
          READ(STRING,1021)IP,IG,IZ
          IF(IP.EQ.IPRT.AND.IG.EQ.IGRND)THEN
             IZMAXD = IZMAXD + 1
             ZDATA(IZMAXD) = IZ
             DO 100 IT = 1 , ITMAXD
                READ(IREAD,1040) ( DRCOFD(IZMAXD,IT,ID),ID = 1,IDMAXD)
 100         CONTINUE
          ELSE
             DO 102 IT = 1, ITMAXD
                READ(IREAD,1040) (DRDUM(IT,ID),ID=1,IDMAXD)
 102         CONTINUE
          ENDIF
          GOTO 90
       ELSE
          DO 150 IZ = IZ1MIN , IZ1MAX
             IZMAXD = IZMAXD + 1
             ZDATA(IZMAXD) = IZ
C     IF( IZ .EQ. IZ1 ) INDXZ1 = IZ
             READ(IREAD,1020)STRING
             DO 101 IT = 1 , ITMAXD
                READ(IREAD,1040) ( DRCOFD(IZMAXD,IT,ID),ID = 1,IDMAXD)
 101         CONTINUE
 150      CONTINUE
       ENDIF
 160   CONTINUE

       CLOSE(12)


C-----------------------------------------------------------------------
C INTERPOLATE USING SPLINES
C-----------------------------------------------------------------------

       IF (LCK.LT.ITMAXD) THEN
          WRITE(I4UNIT(-1),2010)ITMAXD,LCK
          STOP
       ENDIF
       IF (LCK.LT.IDMAXD) THEN
          WRITE(I4UNIT(-1),2020)IDMAXD,LCK
          STOP
       ENDIF
       
       

C-----------------------------------------------------------------------
C JUMP TO HERE IF THE DATASET IS ALREADY OPEN
C-----------------------------------------------------------------------
   20  CONTINUE

       INDXZ1 = IZ1 - IZ1MIN + 1

C FIRST INTERPOLATE DRCOFD(,,,) W.R.T TEMPERATURE

      LSETX = .TRUE.

      DO 200 ID = 1 , IDMAXD

         IF (ICLASS.EQ.1) THEN
            IOPT = 1
         ELSE
            IOPT = 0
         ENDIF

         DO 220 IT = 1 , ITMAXD
            A(IT) = DRCOFD(INDXZ1,IT,ID)
  220    CONTINUE

  230    CALL XXSPLE( LSETX  , IOPT  , R8FUN1       ,
     &                ITMAXD , DTEVD , A            ,
     &                ITMAX  , DTEV  , DRCOF0(1,ID) ,
     &                DY     , LINTRP
     &              )
     
C  ACD:
         IF ( ICLASS.EQ.1 ) THEN
            IF ( DY(1).GT.-0.5 ) THEN
               IOPT = 3
               GOTO 230
            ELSE IF ( DY(1).LT.-4.5) THEN
               IOPT = 5
               GOTO 230
            ENDIF
         ENDIF
C  PRB:
         IF ( ICLASS.EQ.4 ) THEN
            IF ( DY(1).GT.+0.5 ) THEN
               IOPT = 6
               GOTO 230
            ELSE IF ( DY(1).LT.-3.5) THEN
               IOPT = 7
               GOTO 230
            ENDIF
         ENDIF

  200 CONTINUE



C FOLLOWED BY INTERPOLATING THE ABOVE RESULT W.R.T DENSITY

      LSETX = .TRUE.
      IF (ICLASS.EQ.1) THEN
         IOPT = 2
      ELSE
         IOPT = 4
      ENDIF

         DO 300 IT = 1 , ITMAX

               DO 320 ID = 1 , IDMAXD
                  A(ID) = DRCOF0(IT,ID)
  320          CONTINUE

            CALL XXSPLE( LSETX  , IOPT      , R8FUN1     ,
     &                   IDMAXD , DDENSD    , A          ,
     &                   L1     , DDENS(IT) , DRCOFI(IT) ,
     &                   DY     , LINTRP
     &                 )

  300    CONTINUE

       RETURN


C-----------------------------------------------------------------------
C DATA SET OPENING/EXISTENCE ERROR HANDLING
C-----------------------------------------------------------------------

 9999  IFAIL  = 1
       RETURN

C-----------------------------------------------------------------------

 1000  FORMAT('FILE = ',1A73)
 1010  FORMAT(5I5)
 1020  FORMAT(1A80)
 1021  FORMAT(28X,I4,7X,I4,14X,I4)
 1040  FORMAT(8F10.5)
 1050  FORMAT(I6)
 1060  FORMAT(1X,'NOTE: REQUESTED DATASET - ',A30,' DOES NOT EXIST.'/
     1        7X,      'USING DEFAULT YEAR (',A2,') DATASET INSTEAD'/)

 2000  FORMAT('D2DATA ERROR: ITMAX (',I3,') > LCK (',I3,')')
 2010  FORMAT('D2DATA ERROR: ITMAXD (',I3,') > LCK (',I3,')')    
 2020  FORMAT('D2DATA ERROR: IDMAXD (',I3,') > LCK (',I3,')')    
C-----------------------------------------------------------------------

       END
       
