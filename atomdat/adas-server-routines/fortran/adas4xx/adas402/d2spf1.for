CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2spf1.for,v 1.1 2004/07/06 13:13:35 whitefor Exp $ Date $Date: 2004/07/06 13:13:35 $
CX
      SUBROUTINE D2SPF1( DSFULL         , LFSEL        , IOPT03      ,
     &                   LGRAPH         , L2FILE       , SAVFIL      ,
     &                   XMIN           , XMAX         , YMIN        ,
     &                   YMAX           , LPEND
     &                 )
C
C-----------------------------------------------------------------------
C
C PURPOSE : TO INTERFACE PROGRAM "ADAS402" WITH USER DEFINED INPUT
C           DATA GIVEN INTO IDL OUTPUT SCREEN.
C
C  CALLING PROGRAM: ADAS402
C
C  SUBROUTINE:
C
C  INPUT :   (C*80)  DSFULL   = DATA FILE NAME
C  INPUT :   (L*4)   LFSEL    = .TRUE.  => POLYNOMIAL FIT SELECTED
C				.FALSE. => NO POLYNOMIAL FIT SELECTED
C  OUTPUT:   (I*4)   IOPT03   = 0  => USER SELECTED AXES LIMITS
C				1 => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR TEMP OR DENSITY UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR TEMP OR DENSITY UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS IONIZATIONS/PHOTON
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT IONIZATIONS/PHOTON
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C AUTHOR  : WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE   : 28/10/96
C
C
C VERSION: 1.1				DATE: 28-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST WRITTEN USING D1SPF1.FOR
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8       XMIN        , XMAX        , YMIN        , YMAX
C-----------------------------------------------------------------------
      CHARACTER    DSFULL*80   , SAVFIL*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      ,
     &             LFSEL
C-----------------------------------------------------------------------
      INTEGER      ILOGIC      , IOPT03      ,
     &             PIPEIN      , ONE         , ZERO
      PARAMETER( PIPEIN=5      , ONE=1       , ZERO=0)
C
C-----------------------------------------------------------------------
C READ IN DATA FROM IDL PIPE
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    LGRAPH = .TRUE.
	    READ(PIPEIN,*) ILOGIC
	    IF (ILOGIC .EQ. ONE) THEN 
               IOPT03 = 0
	       READ(PIPEIN,*) XMIN
	       READ(PIPEIN,*) XMAX
	       READ(PIPEIN,*) YMIN
	       READ(PIPEIN,*) YMAX
	    ELSE
               IOPT03 = 1
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF
C
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
      ENDIF
C
 9995  RETURN
       END
