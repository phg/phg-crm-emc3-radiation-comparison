CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2ispf.for,v 1.2 2004/07/06 13:13:12 whitefor Exp $ Date $Date: 2004/07/06 13:13:12 $
CX
      SUBROUTINE D2ISPF( TITLE  , LPEND  , IEVCUT
     &                 , IFORM1 , IFORM2 , IFORM3
     &                 , ITMAX
     &                 , IOPT01 , IOPT02
     &                 , TOLVAL
     &                 , Z0     , Z1     , Z      , DSEL
     &                 , NTDIMD , ITMAXD , IDMAXD , TR     , DR
     &                 , TEMP   , DENS
     &                 )
C
C-----------------------------------------------------------------------
C
C PURPOSE : TO INTERFACE PROGRAM "ADAS402" WITH USER DEFINED
C           DATA FROM IDL PROCESSING SCREEN.
C
C INPUT   : (I*4)  NTDIM    = MAXIMUM NUMBER OF TEMP() & DENS()
C           (I*4)  NTDIMD   = MAXIMUM NUMBER OF TEMP() & DENS() IN DATA
C           (I*4)  ITMAXD   = NUMBER OF TEMPS IN DATA
C           (I*4)  IDMAXD   = NUMBER OF DENSS IN DATA
C           (R*8)  TR       = TEMPS FROM INPUT DATASET (EV)
C           (R*8)  DR       = DENS FROM INPUT DATASET (CM-3)
C
C OUTPUT  : (C*40) TITLE    = TITLE OF RUN
C           (L*4)  LPEND    = .TRUE. => CANCEL SELECTED
C           (I*4)  IEVCUT   = ENERGY CUT-OFF (EV)
C           (I*4)  IFORM1   = UNITS OF OUTPUT TEMPERATURE
C           (I*4)  IFORM2   = UNITS OF OUTPUT DENSITY
C           (I*4)  IFORM3   = UNITS OF OUTPUT RATE COEFFICIENTS
C           (I*4)  ITMAX    = NUMBER OF TEMP/DENS PAIRS
C           (I*4)  IOPT01   = 1 -       DO MINMAX POLYNOMINAL
C                             0 - DON'T DO MINMAX POLYNOMINAL
C           (I*4)  IOPT02   = 1 -       SELECT OUTPUT TEMP/DENS
C                             0 - DON'T SELECT OUTPUT TEMP/DENS
C           (R*8)  TOLVAL   = FRACTIONAL TOLERANCE FOR ACCEPTANCE OF
C                             MINIMAX POLYNOMIAL FIT TO DATA.
C                             (IF SELECTED)
C           (R*8)  Z0       = NUCLEAR         CHARGE
C           (R*8)  Z1       = RECOMBINING ION CHARGE
C           (R*8)  Z        = RECOMBINED  ION CHARGE
C           (R*8)  DSEL     = APPROXIMATE SELECTED DENSITY
C           (R*8)  TEMP()   = ELECTRON TEMPERATURES
C           (R*8)  DENS()   = ELECTRON DENSITIES
C
C PROGRAM : (C*6)  USERID   = USERID UNDER WHICH REQUESTED DATA IS KEPT
C           (I*4)  I4UNIT   = ADAS FUNCTION (OUTPUT STEAM NUMBER)
C
C AUTHOR  : JAMES SPENCE (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/80
C           JET EXT. 4866
C
C DATE   : 12/02/90
C
C DATE   : 08/10/90 PE BRIDEN - REVISION: RENAMED SUBROUTINE
C
C UPDATE : 12/10/90 P.E.BRIDEN - ADDED MINIMAX TOLERANCE VARIABLE TO
C                                PANEL 1 (SEE 'CH1B(3)')
C                                INCREASED 'CH1B()' & 'PAN1B()' ARRAY
C                                SIZES TO 3.
C
C UPDATE : 16/11/90 P.E.BRIDEN - 1) MADE SURE ENTERED TEMPERATURE VALUES
C                                   ARE IN MONOTONIC ASCENDING ORDER.
C                                2) RECODED ERROR MESSAGES USING NEW
C                                   ADAS PANEL MESSAGE CODES.
C
C UPDATE : 22/11/91 P.E.BRIDEN - 1) REPLACED  CALL TO PANEL(CLEAR) WITH
C                                  ISPLNK('CONTROL','DISPLAY','REFRESH')
C
C UPDATE : 27/04/92 P.E.BRIDEN - 1) INCLUDED DEFAULT YEAR 'IDYEAR'.
C                                   (READ FROM PANEL 'P40212'.)
C
C UPDATE : 23/04/93 PE BRIDEN  - 1) ADDED I4UNIT FUNCTION TO WRITE
C                                   STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  16/08/96 - PE BRIDEN - ADDED COLLECTION OF USERID FROM PANEL.
C                                 INCREASE DIMS: CH2(5->6),PAN2A(4->5)
C                                 + MADE CH2(6) THE OLD CH2(5). CH2(5)
C                                   NOW NEW PANEL VARIABLE SUSER.
C                                 ADDED CALL TO XXUID TO SET USERID.
C
C-----------------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 28-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED. EXTENSIVE CHANGES.
C
C VERSION: 1.2				DATE: 27-02-98
C MODIFIED: RICHARD MARTIN
C		- INCREASED NTDIM FROM 40 TO 60.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
      INTEGER PIPEIN, PIPEOU
C
      PARAMETER ( NTDIM=60 , PIPEIN=5 , PIPEOU = 6 )
C
      INTEGER IEVCUT , IFORM1 , IFORM2 , IFORM3 , ITMAX
      INTEGER IOPT01 , IOPT02 , LOGIC
      INTEGER NTDIMD , ITMAXD , IDMAXD
C
      REAL*8  TEMP(NTDIM), DENS(NTDIM) , TOLVAL , Z0 , Z1 , Z , DSEL
      REAL*8  TR(NTDIMD), DR(NTDIMD)
C
      LOGICAL LPEND
C
C-----------------------------------------------------------------------
C
      CHARACTER    TITLE*40
C
C-----------------------------------------------------------------------
C

C-----------------------------------------------------------------------
C WRITE DATA TO IDL PIPE
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)NTDIMD
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)ITMAXD
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)IDMAXD
      CALL XXFLSH(PIPEOU)
      DO 1 I=1,ITMAXD
         WRITE(PIPEOU,*)TR(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
      DO 2 I=1,IDMAXD
         WRITE(PIPEOU,*)DR(I)
         CALL XXFLSH(PIPEOU)
 2    CONTINUE
C-----------------------------------------------------------------------
C READ IN DATA FROM IDL PIPE
C-----------------------------------------------------------------------

      READ(PIPEIN,*)LOGIC
      IF ( LOGIC.EQ.0 ) THEN
         LPEND = .FALSE.
         READ(PIPEIN,'(A)')TITLE
C         READ(PIPEIN,*)IEVCUT
         READ(PIPEIN,*)IFORM1
         READ(PIPEIN,*)IFORM2
         READ(PIPEIN,*)IFORM3
         READ(PIPEIN,*)ITMAX
         READ(PIPEIN,*)IOPT01
         READ(PIPEIN,*)IOPT02
         READ(PIPEIN,*)TOLVAL
         READ(PIPEIN,*)Z0
         READ(PIPEIN,*)Z1
         READ(PIPEIN,*)Z
         READ(PIPEIN,*)DSEL
         IF (IOPT02.NE.0) THEN
            DO 10 I=1,ITMAX
               READ(PIPEIN,*)TEMP(I)
               READ(PIPEIN,*)DENS(I)
 10         CONTINUE
         ENDIF
C     
      ELSE
         LPEND = .TRUE.
      ENDIF          

	
      RETURN
      END
