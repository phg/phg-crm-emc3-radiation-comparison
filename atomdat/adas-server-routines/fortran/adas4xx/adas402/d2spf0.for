CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2spf0.for,v 1.1 2004/07/06 13:13:26 whitefor Exp $ Date $Date: 2004/07/06 13:13:26 $
CX
      SUBROUTINE D2SPF0( ANS    , DSFULL
     &                 , IYEAR  , IDYEAR , ICLASS
     &                 , LPART  , LRESO  , IPRT  , IGRND
     &                  )
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C PURPOSE : TO INTERFACE PROGRAM "ADAS402" WITH USER DEFINED INPUT
C           DATA FROM THE IDL INPUT SCREEN.
C
C INPUT   : 
C
C OUTPUT  : (C*3)  ANS      = 'YES' - END      PROGRAM EXECUTION
C                             'NO ' - CONTINUE PROGRAM EXECUTION
C           (C*80) DSFULL   = FULL INPUT FILE NAME
C           (I*4)  IYEAR    = REQUESTED YEAR OF DATA (E.G 74 OR 90)
C           (I*4)  IDYEAR   = DEFAULT   YEAR OF DATA (E.G 89)
C           (I*4)  ICLASS   = CLASS OF DATA (1 TO 8)
C           (L*4)  LPART    = .TRUE.  => PARTIAL TYPE
C                             .FALSE. => STANDARD TYPE
C           (L*4)  LRESO    = .TRUE. => PARTIAL RESOLVED
C                             .FALSE.=> PARTIAL UNRESOLVED OR STANDARD
C           (I*4)  IPRT     = INDEX OF PARENT STATE
C           (I*4)  IGRND    = INDEX OF GROUND STATE
C
C PROGRAM : 
C
C AUTHOR  : WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE   : 28/10/96
C
C
C VERSION: 1.1				DATE: 28-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST WRITTEN
C
C-----------------------------------------------------------------------
C
      INTEGER   PIPEIN, IYEAR, IDYEAR, ICLASS , ILOGIC , IPRT , IGRND
C
      PARAMETER ( PIPEIN=5 )
C-----------------------------------------------------------------------
      CHARACTER ANS*3 , DSFULL*80
      LOGICAL   LPART , LRESO
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C READ IN DATA FROM IDL PIPE
C-----------------------------------------------------------------------

       READ(PIPEIN,'(A)')ANS

       IF(ANS.EQ.'NO')THEN
          READ(PIPEIN,'(A)')DSFULL
          READ(PIPEIN,*)ICLASS
          READ(PIPEIN,*)IYEAR
          READ(PIPEIN,*)ILOGIC
          IF(ILOGIC.EQ.1)THEN
             LPART = .TRUE.
          ELSE
             LPART = .FALSE.
          ENDIF
          READ(PIPEIN,*)ILOGIC
          IF(ILOGIC.EQ.1.AND.LPART)THEN
             LRESO = .TRUE.
          ELSE
             LRESO = .FALSE.
          ENDIF
          READ(PIPEIN,*)IPRT
          READ(PIPEIN,*)IGRND
       ENDIF
C
       RETURN
       END
