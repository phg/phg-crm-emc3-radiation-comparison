CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/d2out0.for,v 1.1 2004/07/06 13:13:17 whitefor Exp $ Date $Date: 2004/07/06 13:13:17 $
CX
       SUBROUTINE D2OUT0( IWRITE   ,
     I                    TITLE    , FINFO , MINFO  ,
     I                    ICLASS   , Z0    , Z1     , Z     ,
     I                    ITMAX    , TEMP  , TEMPEV , TEMPR ,
     I                    DENS     , AOUT  ,
     I                    KPLUS1   , COEF
     I             )
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C PURPOSE : TO PRINT-OUT RESULTS REGARDING 'ADAS402'.
C
C INPUT   : (C*40) TITLE    = TITLE OF RUN
C           (C*120) FINFO    = INFORMATION STRING CONTAINING NAME OF
C                             OPEN DATASET
C           (C*80) MINFO    = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C           (I*4)  IWRITE   = OUTPUT STREAM FOR RESULTS
C           (I*4)  ITMAX    = NUMBER OF TEMP/DENS PAIRS
C           (I*4)  KPLUS1   = NUMBER OF 'MINIMAX' COEFFICIENTS
C           (I*4)  ICLASS   = CLASS OF DATA
C           (R*8)  Z0       = NUCLEAR         CHARGE
C           (R*8)  Z1       = RECOMBINING ION CHARGE
C           (R*8)  Z        = RECOMBINED  ION CHARGE
C           (R*8)  TEMP()   = OUTPUT ELECTRON TEMPERATURES IN KELVIN
C           (R*8)  TEMPEV() = OUTPUT ELECTRON TEMPERATURES IN EV
C           (R*8)  TEMPR()  = REDUCED TEMPERATURE (TEMP IN KELVIN/Z1**2)
C           (R*8)  DENS()   = OUTPUT ELECTRON DENSITIES    IN CM-3
C           (R*8)  AOUT()   = INTERPOLATED DATA AT OUTPUT TEMPERATURE
C                             AND DENSITY PAIR
C           (R*8)  COEF()   = 'MINIMAX' COEFFICIENTS
C
C NOTES   :
C
C AUTHOR  : JAMES SPENCE (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/80
C           JET  EXT. 4866
C
C DATE   : 16/02/90
C
C DATE   : 08/10/90 PE BRIDEN - REVISION: RENAMED SUBROUTINE
C
C DATE   : 16/11/90 PE BRIDEN - CORRECTION: CHANGED POWER COLUMN TITLE
C                                        FROM 'ERG CM3/S' TO 'WATT CM3'
C
C-----------------------------------------------------------------------
C
       DIMENSION TEMP(ITMAX)   , TEMPEV(ITMAX) , TEMPR(ITMAX)
       DIMENSION DENS(ITMAX)
       DIMENSION AOUT(ITMAX)
C
       DIMENSION COEF(KPLUS1)
C
       CHARACTER TITLE*(*) , FINFO*(*) , MINFO*(*)
C
C-----------------------------------------------------------------------
C
C       WRITE( IWRITE , 1000 )
       WRITE( IWRITE , 1010 ) TITLE
       WRITE( IWRITE , 1020 )
       WRITE( IWRITE , 1030 ) Z0 , Z1 , Z
       WRITE( IWRITE , 1040 ) FINFO
C
       IF( ICLASS.LE.3 ) THEN
           WRITE( IWRITE , 1050 )
       ELSE
           WRITE( IWRITE , 1055 )
       END IF
       DO 100 IT = 1 , ITMAX
          WRITE( IWRITE , 1060 ) TEMP(IT) , TEMPEV(IT) , TEMPR(IT) ,
     &                           DENS(IT) , AOUT(IT)
  100  CONTINUE
C
       IF( KPLUS1.GT.0 ) THEN
           WRITE( IWRITE , 1070 )
           WRITE( IWRITE , 1080 ) ( I , COEF(I) , I=1,KPLUS1 )
           WRITE( IWRITE , 1090 ) MINFO
       END IF
C
C-----------------------------------------------------------------------
C
 1000  FORMAT( // )
 1010  FORMAT( / , 45X , A )
 1020  FORMAT( / , ' DATA GENERATED USING PROGRAM ADAS402'
     &             / '  ------------------------------------' )
 1030  FORMAT( / , ' NUCLEAR CHARGE = ' , F6.2 ,
     &               '       RECOMBINING ION CHARGE = ' , F6.2 ,
     &               '        RECOMBINED ION CHARGE = ' , F6.2  )
 1040  FORMAT( / , 1X , A / )
 1050  FORMAT( / , '        TEMP(K)       TEMP(EV)      TEMP(K)/Z1**2
     &    DENS(CM-3)      RATE COEFF(CM**3/S)'
     &             / '       ---------      ---------      -------------
     &     ----------      -------------------')
 1055  FORMAT( / , '        TEMP(K)       TEMP(EV)      TEMP(K)/Z1**2
     &    DENS(CM-3)         POWER(WATT CM3) '
     &             / '       ---------      ---------      -------------
     &     ----------      -------------------')
 1060  FORMAT( 1P , 1X ,  5X , D10.3
     &                 ,  5X , D10.3
     &                 ,  9X , D10.3
     &                 ,  5X , D10.3
     &                 , 15X , D10.3 )
 1070  FORMAT( /
     &       / / , ' TAYLOR COEFFICIENTS OF MINIMAX POLYNOMINAL  ( LOG
     &10(RATE COEF) VS LOG10(TEMP(EV))'
     &       / '  ------------------------------------------------------
     &--------------------------------'
     &       / '  ' )
 1080  FORMAT( 3( 5X , I3 , ') = ' , 1PD17.9 ) )
 1090  FORMAT( / , 1X , A )
C
C-----------------------------------------------------------------------
C
       RETURN
       END
