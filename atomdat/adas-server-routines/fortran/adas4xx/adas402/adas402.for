CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas4xx/adas402/adas402.for,v 1.2 2004/07/06 10:38:37 whitefor Exp $ Date $Date: 2004/07/06 10:38:37 $
CX
      PROGRAM ADAS402
      IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C ORIGINAL NAME: SPRTGRF7
C
C VERSION : 1.0
C
C PURPOSE : TO GRAPH AND INTERPOLATE SELECTED DATA FROM MASTER
C           CONDENSED 'SANC0' COLLISIONAL DILECTRONIC FILES FOR
C           EXAMINATION AND DISPLAY.
C
C DATA    : THE SOURCE DATA IS CONTAINED AS SEQUENTIAL DATASETS
C           AS FOLLOWS :-
C
C                   (1) JETSHP.ACD<YR>#<IEL>.DATA
C                   (2) JETSHP.SCD<YR>#<IEL>.DATA
C                   (3) JETSHP.CCD<YR>#<IEL>.DATA
C                   (4) JETSHP.PRB<YR>#<IEL>.DATA
C                   (5) JETSHP.PLT<YR>#<IEL>.DATA
C                   (7) JETSHP.PRC<YR>#<IEL>.DATA
C                   (6) JETSHP.PLS<YR>#<IEL>.DATA
C
C           WHERE, <YR>  = TWO INTEGERS FOR THE SELECTED YEAR
C                  <IEL> = ELEMENT NAME
C
C           EACH DATASET HAS A SET OF REDUCED DENSITY DATA WHICH MAY
C           ALTER FROM FILE TO FILE.
C
C PROGRAM : (C*120) FINFO    = INFORMATION STRING CONTAINING NAME OF
C                             OPEN DATASET
C           (C*80) MINFO    = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C           (C*40) TITLE    = TITLE OF RUN
C           (C*28) SUBSTR   = GENERAL CHARACTER STRING
C           (C*3)  ANS      = 'YES' - END      PROGRAM EXECUTION
C                             'NO ' - CONTINUE PROGRAM EXECUTION
C           (C*2)  YEAR     = REQUESTED YEAR OF DATA  (E.G 74 OR 90)
C           (C*2)  YEARDF   = DEFAULT   YEAR OF DATA  (E.G 89)
C           (I*4)  I4UNIT   = FUNCTION (SEE ROUTINE SECTION BELOW)
C           (I*4)  NTDIM    = MAXIMUM VALUE OF ITMAX ALLOWED.
C                             ( ALSO SET IN 'SPCOPS2' )
C           (I*4)  NTDIMD   = MAXIMUM VALUE OF ITMAXD & IDMAX.
C           (I*4)  NTDIMX   = NUMBER OF EVALUATED DATA RETURNED FROM
C                             'MINIMAX'
C           (I*4)  IWRITE   = OUTPUT STREAM FOR RESULTS
C           (I*4)  IPAN     = PANEL NUMBER
C           (I*4)  IYEAR    = REQUESTED YEAR OF DATA (E.G 77 OR 90)
C           (I*4)  IDYEAR   = DEFAULT   YEAR OF DATA (E.G 89)
C           (I*4)  ICLASS   = CLASS OF DATA (1 TO 6)
C           (I*4)  IEVCUT   = ENERGY CUT-OFF (EV)
C           (I*4)  IFORM1   = UNITS OF OUTPUT TEMPERATURE
C           (I*4)  IFORM2   = UNITS OF OUTPUT DENSITY
C           (I*4)  IFORM3   = UNITS OF OUTPUT RATE COEFFICIENTS
C           (I*4)  ITMAX    = NUMBER OF TEMP/DENS PAIRS
C           (I*4)  IOPT01   = 1 -       DO MINMAX POLYNOMINAL
C                             0 - DON'T DO MINMAX POLYNOMINAL
C           (I*4)  IOPT02   = 1 -       SELECT OUTPUT TEMP/DENS
C                             0 - DON'T SELECT OUTPUT TEMP/DENS
C           (I*4)  IOPT03   = 1 - DEFAULT GRAPHIC SCALING
C                             0 - MANUAL  GRAPHIC SCALING
C           (I*4)  IZ0      = NUCLEAR         CHARGE
C           (I*4)  IZ1      = RECOMBINING ION CHARGE
C           (I*4)  IZMAX    = NUMBER OF CHARGED STATES IN DATA
C           (I*4)  ITMAXD   = NUMBER OF TEMPERATURE IN DATA
C           (I*4)  IDMAXD   = NUMBER OF DENSITIES IN DATA
C           (I*4)  IZMAXD   = NUMBER OF Z1        IN DATA
C           (I*4)  KPLUS1   = NUMBER OF 'MINIMAX' COEFFICIENTS
C           (I*4)  IT       = GENERAL PURPOSE VARIABLE
C           (I*4)  IZ       = GENERAL PURPOSE VARIABLE
C           (I*4)  IDSEL    = SELECTED DATA DENSITY
C           (R*8)  EV       = MULTIPLIER TO CONVERT TO KELVIN.
C           (R*8)  Z0       = NUCLEAR         CHARGE
C           (R*8)  Z1       = RECOMBINING ION CHARGE
C           (R*8)  Z        = RECOMBINED  ION CHARGE
C           (R*8)  DSEL     = APPROXIMATE SELECTED DENSITY
C           (R*8)  DDSEL    = DLOG10( DSEL )
C           (R*8)  TEMP0()  = OUTPUT ELECTRON TEMPERATURES IN FORM
C                             DEFINED IN 'SPCOPS2'
C           (R*8)  TEMP()   = OUTPUT ELECTRON TEMPERATURES IN KELVIN
C           (R*8)  TEV()    = OUTPUT ELECTRON TEMPERATURES IN EV
C           (R*8)  TEMPR()  = REDUCED ELECTRON TEMPERATURES IN KELVIN
C           (R*8)  DTEV()   = LOG10( TEV() )
C           (R*8)  TEMPX()  = TEMPERATURES AT WHICH INTERPOLATED DATA
C                             IS EVALUATED IN 'MINIMAX'
C           (R*8)  DENS()   = OUTPUT ELECTRON DENSITIES    IN CM-3
C           (R*8)  DDENS()  = LOG10( DENS() )
C           (R*8)  XMIN     = MIN. X VALUE FOR MANUAL GRAPHIC SCALING
C           (R*8)  XMAX     = MAX. X VALUE FOR MANUAL GRAPHIC SCALING
C           (R*8)  XMINEV   = MIN. X VALUE IN EV.
C           (R*8)  XMAXEV   = MAX. X VALUE IN EV.
C           (R*8)  YMIN     = MIN. Y VALUE FOR MANUAL GRAPHIC SCALING
C           (R*8)  YMAX     = MAX. Y VALUE FOR MANUAL GRAPHIC SCALING
C           (R*8)  ZDATA()  = RECOMBINING ION CHARGE (Z1) DATA
C           (R*8)  TEVD()   = ELECTRON TEMPERATURE IN DATA
C           (R*8)  DTEVD()  = LOG10(ELECTRON TEMPERATURE) IN DATA
C           (R*8)  DENSD()  = ELECTRON DENSITY IN DATA
C           (R*8)  DDENSD() = LOG10(ELECTRON DENSITY) IN DATA
C        (R*8)  DRCOFD(,,,) = LOG10( RATE COEFFICIENTS )
C           (R*8)  DRCOFI() = LOG10( INTERPOLATED RATE COEFFICIENTS )
C                             FOR DTEV() & DDENS()
C           (R*8)  RCOFI()  = ( INTERPOLATED RATE COEFFICIENTS )
C                             FOR DTEV() & DDENS()
C           (R*8)  AOUT()   = INTERPOLATED DATA AT OUTPUT TEMPERATURE
C                             AND DENSITY PAIR
C           (R*8)  DAOUT()  = LOG10( AOUT() )
C           (R*8)  AOUTX()  = INTERPOLATED DATA EVALUATED FOR GRAPH
C                             IN 'MINIMAX'
C           (R*8)  RATA(,)  = (VALUE OF 10**DRCOFD(,,) FOR IDSEL)
C           (R*8)  COEF()   = 'MINIMAX' COEFFICIENTS
C           (R*8)  GEN0     = GENERAL VARIABLE
C           (R*8)  GEN1     = GENERAL VARIABLE
C
C NOTE    :
C
C
C ROUTINE : D2ISPF          = GATHERS USERS VALUES.
C           D2DATA          = GATHERS REQUIRED DATASET FILE AND
C                             INTERPOLATES.
C           XX0000          = SET MACHINE DEPENDANT ADAS CONFIGURATION
C           XXMNMX          = MINMAX POLYNOMINAL FIT ROUTINE.
C           D2OUTG          = GRAPHIC OUTPUT ROUTINE.
C           D2OUT0          = RESULTS OUTPUT ROUTINE.
C           I4UNIT          = FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR  : JAMES SPENCE (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/80
C           JET EXT. 4866
C
C DATE    : 22/02/90
C
C UPDATE  : 08/10/90 - RENAME SUBROUTINES & PROGRAM (P.E.BRIDEN)
C
C UPDATE  : 12/10/90 --- P.E.BRIDEN -
C                        REPLACED CALL TO 'MINIMAX' BY CALL TO
C                        'XXMNMX' & INTRODUCED RELATED VARIABLES
C                        (L*4) LMLOG, (I*4) MAXDEG, (R*8) TOLVAL
C                        (REMOVED 'NCDIM' & REPLACED BY 'MAXDEG+1'.
C                         REMOVED 'DTEMPR()' -  NO LONGER REQUIRED.
C                         REMOVED 'DAOUT()'  -  NO LONGER REQUIRED.)
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C UPDATE  : 12/11/90 --- P.E.BRIDEN -
C                        ADDED CALL TO 'XXENDG' TO CLOSE GHOST80 FILES.
C                        ALSO ADDED 'LGHOST' LOGICAL VARIABLE WHICH
C                        IDENTIFIES IF GHOST80 HAS BEEN CALLED.
C
C           (L*4)  LGHOST   = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                           = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C
C UPDATE  : 27/04/92 --- P.E.BRIDEN -
C                        ADDED 'IDYEAR' AND 'YEARDF' TO CONTAIN DEFAULT
C                        YEAR OF DATA TO USE IF REQUESTED YEAR DOES NOT
C                        EXIST. ADDED AS ARGUMENTS TO D2DATA & D2ISPF.
C                        ADDED IFAIL=-1 => DEFAULT YEAR FOR DATA USED.
C                        CHANGED 'IF( IFAIL.NE.0 ) THEN'
C                        TO      'IF( IFAIL.GT.0 ) THEN'
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C-----------------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C VERSION: 1.1				DATE: 28-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C		- FIRST CONVERTED
C
C VERSION: 1.2				DATE: 28-02-98
C MODIFIED: RICHARD MARTIN
C		- INCREASED NTDIM FROM 40 TO 60 (SEE ALSO D2ISPF.FOR)
C
C-----------------------------------------------------------------------
C
       LOGICAL   LMLOG, LFSEL, LGRAPH, L2FILE, LPEND, OPEN17
       INTEGER   MAXDEG , PIPEIN
       REAL*8    TOLVAL
C
       PARAMETER(LMLOG=.TRUE.)
       PARAMETER(MAXDEG=19)
       PARAMETER(PIPEIN = 5)
C
C-----------------------------------------------------------------------
C
       PARAMETER ( NTDIM  = 60 , NTDIMX = 100 , NTDIMD = 60 )
       PARAMETER ( IWRITE = 17                              )
       PARAMETER ( EV     = 1.160D+04                       )
C-----------------------------------------------------------------------
C
       INTEGER   I4UNIT
       LOGICAL   LPART
C
C-----------------------------------------------------------------------
C
       DIMENSION TEMP0(NTDIM)
       DIMENSION TEMP(NTDIM)  , TEMPR(NTDIM)
       DIMENSION TEV(NTDIM)   , DTEV(NTDIM)
       DIMENSION TEMPX(NTDIMX)
       DIMENSION DENS(NTDIM)  , DDENS(NTDIM)
       DIMENSION AOUTX(NTDIMX)
C
       DIMENSION DTEVD(NTDIMD) , DDENSD(NTDIMD) , ZDATA(NTDIMD)
       DIMENSION TEVD(NTDIMD)  , DENSD(NTDIMD)
       DIMENSION DRCOFD(NTDIMD,NTDIMD,NTDIMD)
       DIMENSION RATA(NTDIMD,NTDIMD), TR(NTDIMD), DR(NTDIMD)
       DIMENSION DRCOFI(NTDIM) , RCOFI(NTDIM)
C
       DIMENSION COEF(MAXDEG+1)
C
       CHARACTER TITLE*40     , ANS*3    , YEAR*2    , YEARDF*2
       CHARACTER FINFO*120     , MINFO*80 , SUBSTR*28 , DATE*8
       CHARACTER DSFULL*80    , SAVFIL*80
C-----------------------------------------------------------------------
       DATA OPEN17/.FALSE./
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
       CALL XX0000
C
C-----------------------------------------------------------------------
C GET DATE FROM IDL
C-----------------------------------------------------------------------
C
       CALL XXDATE(DATE)
C
C-----------------------------------------------------------------------
C      GET USER-DEFINED VALUES FROM IDL INPUT SCREEN
C-----------------------------------------------------------------------
C
 10    CALL D2SPF0( ANS    , DSFULL
     &            , IYEAR  , IDYEAR , ICLASS
     &            , LPART  , LRESO  , IPRT , IGRND
     &            )
       
       IF (ANS.EQ.'YES') THEN
          GOTO 9999
       ENDIF

C
C-----------------------------------------------------------------------
C      GET TEMPERATURE AND DENSITY PAIRS FROM FILE
C-----------------------------------------------------------------------
C
       CALL D2TDIN(DSFULL, NTDIMD, ITMAXD, IDMAXD, TR, DR, LRESO)

C
C-----------------------------------------------------------------------
C      GET USER-DEFINED VALUES FROM IDL PROCESSING SCREEN
C-----------------------------------------------------------------------
C

 11    CALL D2ISPF( TITLE  , LPEND  , IEVCUT
     I            , IFORM1 , IFORM2 , IFORM3
     I            , ITMAX
     I            , IOPT01 , IOPT02
     I            , TOLVAL
     I            , Z0     , Z1     , Z      , DSEL
     I            , NTDIMD , ITMAXD , IDMAXD , TR     , DR
     I            , TEMP0  , DENS
     I            )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999

       IF (LPEND) GOTO 10

C
C-----------------------------------------------------------------------
C      GET USER-DEFINED VALUES FROM IDL OUTPUT SCREEN
C-----------------------------------------------------------------------
C
 12    CALL D2SPF1( DSFULL         , LFSEL        , IOPT03      ,
     &              LGRAPH         , L2FILE       , SAVFIL      ,
     &              XMIN           , XMAX         , YMIN        ,
     &              YMAX           , LPEND)
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
       READ(PIPEIN,*) ISTOP
       IF(ISTOP.EQ.1) GOTO 9999

       IF (LPEND) GOTO 11

C
C-----------------------------------------------------------------------
C      COPY CERTAIN 'D2ISPF' DATA INTO REQUIRED 'ADAS402' FORMS
C-----------------------------------------------------------------------
C
       WRITE(YEAR  ,1000) IYEAR
       WRITE(YEARDF,1000) IDYEAR
C
       IZ0  = INT(Z0)
       IZ1  = INT(Z1)
C
C-----------------------------------------------------------------------
C      TEMP & DENS CONVERSION AND LOG10 CALCULATION
C-----------------------------------------------------------------------
C
       Z1SQR = Z1 * Z1
       DDSEL = DLOG10( DSEL )
C
       DO 100 IT     = 1 , ITMAX
          IF     ( IFORM1.EQ.1 ) THEN
                                 TEMP(IT)   = TEMP0(IT)
                                 TEV(IT)    = TEMP(IT) / EV
                                 TEMPR(IT)  = TEMP(IT) / Z1SQR
          ELSEIF ( IFORM1.EQ.2 ) THEN
                                 TEV(IT)    = TEMP0(IT)
                                 TEMP(IT)   = TEMP0(IT) * EV
                                 TEMPR(IT)  = TEMP(IT) / Z1SQR
          ELSEIF ( IFORM1.EQ.3 ) THEN
                                 TEMPR(IT)  = TEMP0(IT)
                                 TEMP(IT)   = TEMP0(IT) * Z1SQR
                                 TEV(IT)    = TEMP(IT)  / EV
          END IF
          DTEV(IT)   = DLOG10( TEV(IT)  )
          DDENS(IT)  = DLOG10( DENS(IT) )
  100  CONTINUE
C
C-----------------------------------------------------------------------
C      XMIN & XMAX CONVERSION INTO PLOTTING UNITS
C-----------------------------------------------------------------------
C
       IF ( IOPT03.EQ.0 ) THEN
            IF     ( IFORM1.EQ.1 ) THEN
                                   XMINEV = XMIN / EV
                                   XMAXEV = XMAX / EV
            ELSEIF ( IFORM1.EQ.2 ) THEN
                                   XMINEV = XMIN
                                   XMAXEV = XMAX
            ELSEIF ( IFORM1.EQ.3 ) THEN
                                   XMINEV = XMIN * Z1SQR / EV
                                   XMAXEV = XMAX * Z1SQR / EV
            END IF
       END IF
C
C-----------------------------------------------------------------------
C      FETCH SELECTED DATASET AND INTERPOLATE
C-----------------------------------------------------------------------
C
       IFAIL = 0
C
       CALL D2DATA( DSFULL , FINFO  , IFAIL  , LRESO  , IPRT   , IGRND
     &            , IZ0    , IZ1    , ICLASS , ITMAX  , IEVCUT
     &            , NTDIMD , ITMAXD , IDMAXD , IZMAXD
     &            , DTEV   , DDENS
     &            , DTEVD  , DDENSD , DRCOFD , ZDATA
     &            , DRCOFI , YEAR
     &            )
C
       IF( IFAIL.GT.0 ) THEN
           WRITE(I4UNIT(-1),9000) FINFO(1:80)
           GOTO 10
       END IF
C
C-----------------------------------------------------------------------
C      CONVERT DATA & INTERPOLATED VALUES TO LINEAR UNITS
C-----------------------------------------------------------------------
C
       DO 110 IT = 1 , ITMAXD
           TEVD(IT) = 10.0 ** DTEVD(IT)
  110  CONTINUE
C
       DO 120 ID = 1 , IDMAXD
           DENSD(ID) = 10.0 ** DDENSD(ID)
  120  CONTINUE
C
       DO 140 IT = 1 , ITMAX
          RCOFI(IT) = 10.0 ** DRCOFI(IT)
  140  CONTINUE
C
C-----------------------------------------------------------------------
C      FIND NEAREST DATA DENSITY TO REQUIRED DENSITY
C-----------------------------------------------------------------------
C
       IDSEL = 0
       IF ( DDSEL .LT. DDENSD(1)      ) IDSEL = 1
       IF ( DDSEL .GT. DDENSD(IDMAXD) ) IDSEL = IDMAXD
C
       IF ( IDSEL.EQ.0 ) THEN
           GEN0 = 1.0D+30
           DO 150 ID = 1 , IDMAXD
              GEN1 = DABS( DSEL - 10.0**DDENSD(ID) )
              IF( GEN1 .LT. GEN0 ) THEN
                                   GEN0  = GEN1
                                   IDSEL = ID
              END IF
  150      CONTINUE
       END IF
C
       WRITE(SUBSTR,1010) 10.0**DDENSD(IDSEL)
       FINFO(70:97) = SUBSTR
C
C-----------------------------------------------------------------------
C      COPY DRCOFD(,,) INTO DRATA(,) FOR REQUIRED IDSEL
C-----------------------------------------------------------------------
C
       DO 250 IZ         = 1 , IZMAXD
          DO 200 IT      = 1 , ITMAXD
             RATA(IZ,IT) = 10.0 ** DRCOFD(IZ,IT,IDSEL)
  200     CONTINUE
  250  CONTINUE
C
C-----------------------------------------------------------------------
C      FIT MINIMAX POLYNOMINAL IF REQUIRED
C-----------------------------------------------------------------------
C
       KPLUS1 = 0
       NMX    = 0
C
       IF( IOPT01.EQ.1 ) THEN
          CALL XXMNMX( LMLOG  , MAXDEG , TOLVAL ,
     I         ITMAX  , TEV    , RCOFI  ,
     O         NTDIMX , TEMPX  , AOUTX  ,
     O         KPLUS1 , COEF   ,
     O         MINFO
     &         )
          NMX = NTDIMX
       END IF
C     
C-----------------------------------------------------------------------
C      GRAPHICS SECTION
C-----------------------------------------------------------------------
C
       IF(LGRAPH)THEN
          CALL D2OUTG( TITLE  , FINFO  , MINFO  ,
     I              TEVD   , RATA   , ITMAXD ,
     I              ZDATA  , IZMAXD , Z1     ,
     I              TEMPX  , AOUTX  , NMX    ,
     I              TEV    , RCOFI  , ITMAX  ,
     I              IOPT03 , ICLASS ,
     I              XMINEV , XMAXEV , YMIN   , YMAX   , ICLASS )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C     
          READ(PIPEIN,*) ISTOP
          IF(ISTOP.EQ.1) GOTO 9999

       ENDIF
C
C-----------------------------------------------------------------------
C      RESULTS  SECTION
C-----------------------------------------------------------------------
C
       IF(L2FILE)THEN
          IF(.NOT.OPEN17)THEN
             OPEN(UNIT=IWRITE,FILE=SAVFIL,STATUS='UNKNOWN')
             OPEN17 = .TRUE.
          ENDIF
          CALL D2OUT0( IWRITE ,
     I         TITLE  , FINFO , MINFO  ,
     I         ICLASS , Z0    , Z1     , Z     ,
     I         ITMAX  , TEMP  , TEV    , TEMPR , DENS  , RCOFI ,
     I         KPLUS1 , COEF
     I         )
       ENDIF
C
C-----------------------------------------------------------------------
C GO BACK TO OUTPUT SCREEN
C-----------------------------------------------------------------------
C
       GOTO 12
C
C-----------------------------------------------------------------------
C
 1000  FORMAT( I2 )
 1010  FORMAT('DENSITY    = ',1P,D10.2,' CM-3')
C
 9000  FORMAT(' *** ERROR *** THE FOLLOWING DOES NOT EXIST :-'
     &       /'               ' , A )
C
C-----------------------------------------------------------------------
C
 9999  CONTINUE

       IF(OPEN17)CLOSE(IWRITE)

       END
