CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/adas308.for,v 1.7 2015/12/21 14:19:38 mog Exp $ Date $Date: 2015/12/21 14:19:38 $
CX
      PROGRAM ADAS308
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS308 **********************
C
C  ORIGINAL NAME: SPLMS
C
C  VERSION:  1.1 (ADAS91) (See S.C.C.S. info for version number)
C
C  PURPOSE:  CODE FOR INTERPRETATION OF L-RESOLVED COLUMN INTEGRATED
C            SPECTRUM LINE EMISSIVITIES IN TERMS OF MEAN RATE
C            COEFFICIENTS AND EMISSION MEASURE.  APPLICABLE TO
C            IMPURITIES IN PLASMA TRAVERSED BY NEUTRAL BEAMS OF H OR HE.
C
C            THE RECOMBINED TARGET ION IS TREATED AS H-LIKE.
C
C            THE MODEL INCLUDES CAPTURE, N-N' LEVEL CASCADE, AND MIXING
C            AMONG L LEVELS OF SAME N BY COLLISIONS.
C
C            EIKONAL APPROXIMATION IS USED FOR CAPTURE FROM EXCITED H OR
C            HE STATES.
C
C            OUTPUT FROM THE PROGRAM IS GRAPHS AND TABLES OF L-RESOLVED
C            EMISSIVITY FOR THE REQUESTED TRANSITIONS. L-RESOLVED
C            RATE TABLES ARE OPTIONALLY OUTPUT. IF REQUESTED THE GRAPHS
C            AND THE TEXT FILE ARE OUTPUT TO HARDCOPY.
C
CX DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS IN A PARTITIONED
CX           DATA SET AS FOLLOWS:-
CX
CX                   'JETSHP.QCX#<DONOR>.DATA(<MEMBER>)'
CX
CX           WHERE
CX             <DONOR>    = <DONOR ELEMENT SYMBOL><DONOR CHARGE STATE>
CX             <MEMBER>   = <ID>#<RECEIVER>
CX
CX             <ID>       = 3 CHARACTER PREFIX IDENTIFYING THE SOURCE
CX                          OF THE DATA.
CX             <RECEIVER> = <RECVR ELEMENT SYMBOL><RECVR CHARGE STATE>
CX
CX
CX             E.G. 'JETSHP.QCX#H0.DATA(OLD#C6)'
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           ENERGIES            : KEV / AMU
C           CROSS SECTIONS      : CM**2
C
C  PARAM : (I*4)  IUNT7     = OUTPUT UNIT FOR RESULTS.
C  PARAM : (I*4)  IUNT10    = INPUT UNIT FOR DATA.
C  PARAM : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES IN DATA SET.
C  PARAM : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  PARAM : (I*4)  MXBEAM    = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  PARAM : (I*4)  MXOBSL    = MAXIMUM NUMBER OF OBSERVED SPECTRUM
C                             LINES.
C  PARAM : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  PARAM : (I*4)  MXTAB     = MAXIMUM NUMBER OF TABLES FOR OUTPUT.
C  PARAM : (I*4)  MXGRF     = MAXIMUM NUMBER OF GRAPHS FOR OUTPUT.
C  PARAM : (I*4)  NGFPLN    = NUMBER OF GRAPHS PER PREDICTED SPECTRUM
C                             LINE.
C
C  PARAM : (R*8)  EMP       = REDUCED MASS FOR POSITIVE ION.
C                             UNITS: ELECTRON MASSES
C
C          (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C          (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C          (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  IZR       = ION CHARGE OF RECEIVER.
C          (I*4)  IZD       = ION CHARGE OF DONOR.
C          (I*4)  INDD      = DONOR STATE INDEX.
C          (I*4)  NENRGY    = NUMBER OF ENERGIES READ FROM DATA SET.
C          (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C          (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C          (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C          (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
CX         (I*4)  IPAN      = ISPF PANEL NUMBER.
C          (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C          (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C          (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C          (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C          (I*4)  NOLINE    = NUMBER OF OBSERVED SPECTRUM LINES.
C          (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C          (I*4)  NTAB      = NUMBER OF EMISSIVITY TABLES FOR OUTPUT.
C          (I*4)  NGRF      = NUMBER OF EMISSIVITY GRAPHS FOR OUTPUT.
C          (I*4)  NUMIN     = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C          (I*4)  NUMAX     = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C
C          (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C          (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C          (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C          (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C          (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C          (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C          (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C          (R*8)  EM        = EMMISSION MEASURE.
C                             UNITS: CM-5
C
C          (L*4)  OPEN10    = FLAGS IF INPUT DATA SET OPEN.
C                             .TRUE.  => INPUT DATA SET OPEN.
C                             .FALSE. => INPUT DATA SET CLOSED.
C          (L*4)  LGHOST    = INITIALISATION FLAG FOR GHOST80.
C                             .TRUE.  => GHOST80 INITIALISED.
C                             .FALSE. => GHOST80 NOT INITIALISED.
CX         (L*4)  LDSEL     = FLAGS WHETHER TO DISPLAY TEXT IN INPUT
CX                            DATA SET.
CX                            .TRUE.  => DISPLAY TEXT IN INPUT DATA SET.
CX                            .FALSE. => DO NOT DISPLAY TEXT IN INPUT
CX                                       DATA SET.
C          (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C          (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C          (L*4)  LPEND     = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C          (L*4)  LRTTB     = FLAG FOR RATE TABLE PRINTING.
C                             .TRUE.  = PRINT RATE TABLES.
C                             .FALSE. = DO NOT PRINT RATE TABLES.
C          (L*4)  LPLT1     = FLAGS WHETHER TO PLOT GRAPHS ON SCREEN.
C                             .TRUE.  => PLOT GRAPHS ON SCREEN.
C                             .FALSE. => DO NOT PLOT GRAPHS ON SCREEN.
C          (L*4)  LGRD1     = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
C          (L*4)  LDEF1     = FLAGS DEFAULT GRAPH SCALING
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
C
C          (C*8)  DATE      = DATE.
C          (C*3)  REP       = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*80) DSFULL    = FILE NAME OF INPUT DATA SET.
C          (C*80) TITLEF    = NOT SET - TITLE FROM INPUT DATA SET.
C          (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C          (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C          (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C
C          (I*4)  LFORMA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (I*4)  NL()      = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NU()      = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  IDTAB()   = LIST OF INDEXES OF EMISSIVITY TABLES FOR
C                             OUTPUT.
C                             DIMENSION: MXTAB
C          (I*4)  IDGRF()   = LIST OF INDEXES OF EMISSIVITY GRAPHS FOR
C                             OUTPUT.
C                             DIMENSION: MXGRF
C
C          (R*8)  ENRGYA()  = COLLISION ENERGIES READ FROM INPUT DATA
C                             SET.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C          (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA READ FROM
C                             INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XSECTA()  = TOTAL CHARGE EXCHANGE CROSS-SECTIONS READ
C                             FROM INPUT DATA SET.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C          (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C          (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C          (R*8)  EMISA()   = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (R*8)  XLG()     = LOWER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  XUG()     = UPPER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  YLG()     = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  YUG()     = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  TBLF()    = TABLE OF RADIATIVE LIFETIMES.
C                             UNITS: SECS
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQEX()   = MEAN EXCITATION RATE COEFFICIENTS FOR
C                             NL-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  QTHEX()   = MEAN EXCITATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  FTHEX()   = FRACTION OF N-LEVEL MEAN EXCITATION RATE
C                             COEFFICIENTS IN NL-LEVEL.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  QTHCH()   = MEAN CHARGE EXCHANGE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  FTHCH()   = FRACTION OF N-LEVEL MEAN CHARGE EXCHANGE
C                             COEFFICIENTS IN NL-LEVEL.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMEP()  = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NL->NL+1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMEM()  = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NL->NL-1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMIP()  = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NL->NL+1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMIM()  = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NL->NL-1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  QEX()     =
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  TOTPOP()  = TOTAL COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TOTEMI()  = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  AVRGWL()  = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C
C          (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             READ FROM INPUT DATA SET.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C          (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF N-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C          (R*8)  FRACMA(,) = M-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF L-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFM(N,L,M)
C                                            WITH M >= 0 ONLY
C          (R*8)  TBLPOP(,) = TABLE OF COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             1ST DIMENSION: PREDICTED LINE INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C          (R*8)  TBLEMI(,) = TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: PREDICTED LINE INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C          (R*8)  TBLWLN(,) = TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: PREDICTED LINE INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
CX         C8SPF0     ADAS      HANDLES IDL PANEL FOR DATA SET INPUT.
C          C8CHRG     ADAS      SETS UP CHARGES FOR DONOR AND RECEIVER.
CX         C8ISPF     ADAS      HANDLES IDL PANELS FOR USER INPUT.
CX         C8SPF1     ADAS      HANDLES OUTPUT COMMS WITH IDL.
C          C8TBQM     ADAS      FILLS N AND L-RESOLVED COLLISIONAL RATE
C                               TABLES.
C          C8EMIS     ADAS      PREDICTS THE L-RESOLVED EMISSIVITY FOR
C                               REQUESTED TRANSITIONS.
C          C8OUTG     ADAS      CONTROLS GRAPHICAL OUTPUT FOR ADAS306.
C          C8OUT0     ADAS      CONTROLS TEXT OUTPUT FOR ADAS306.
C          CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C          CXFRAC     ADAS      CONVERTS L AND M RESOLVED CROSS-SECTIONS
C                               FROM ABSOLUTE VALUES TO FRACTIONS.
C          CXEXTR     ADAS      EXTRAPOLATES N AND L RESOLVED CROSS-
C                               SECTIONS FROM INPUT DATA SET.
C          CXSETP     ADAS      SETS UP PARAMETERS IN THE SHARED POOL
C                               FOR ISPF PANEL DISPLAY.
C          CXTBLF     ADAS      FILLS L-RESOLVED RADIATIVE LIFETIME
C                               TABLE.
C          CXTBEX     ADAS      FILLS N AND L-RESOLVED ELECTRON IMPACT
C                               EXCITATION RATE TABLES.
C          CXQEIK     ADAS      FILLS N AND L-RESOLVED CHARGE EXCHANGE
C                               RATE TABLES USING EIKONAL APPROXIMATION.
C          CXQXCH     ADAS      FILLS N AND L-RESOLVED CHARGE EXCHANGE
C                               RATE TABLES USING INPUT DATA SET.
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
CX         XXENDG     ADAS      CLOSES GHOST80 GRIDFILE AND OUTPUT
CX                              CHANNEL.
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM INPUT
C                               DATA SET.
C          XXRAMS     ADAS      SETS/RETRIEVES RECEIVER ATOMIC WEIGHT.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    25/11/93
C
C UNIX-IDL PORT:
C
C VERSION:  1.1                          DATE: 20/6/95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C            - FIRST VERSION
C
C VERSION:  1.2                          DATE: 21/06/96
C MODIFIED: TIM HAMMOND 
C            - ADDED PIPE COMMS FOR "RETURN TO MENU" BUTTONS.
C
C VERSION:  1.3                          DATE: 06/08/96
C MODIFIED: TIM HAMMOND
C            - ADDED PIPE COMMS FOR INFORMATION WIDGETS
C
C VERSION:  1.4                          DATE: 08/04/99
C MODIFIED: Martin O'Mullane
C            - Added XXRAMS to set the weight of the receiver for
C              subsequent use in C8PRSL.
C
C VERSION  : 1.5
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C VERSION  : 1.6
C DATE     : 05-01-2012
C MODIFIED : Martin O'Mullane
C              - Increase dimension for number of energies in
C                an input adf01 file to 35.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUNT7        , IUNT10       , MXNENG       ,
     &           MXNSHL       , MXBEAM       , MXOBSL       ,
     &           MXPRSL       , MXTAB        , MXGRF        ,
     &           NGFPLN
      PARAMETER( IUNT7  = 7   , IUNT10 = 11  , MXNENG = 35  ,
     &           MXNSHL = 20  , MXBEAM = 6   , MXOBSL = 10  ,
     &           MXPRSL = 20  , MXTAB  = 5   , MXGRF  = 2   ,
     &           NGFPLN = 2                                   )
C-----------------------------------------------------------------------
      REAL*8     EMP
      PARAMETER( EMP = 1836.0D0 )
C-----------------------------------------------------------------------
      INTEGER    NGRND   , NTOT    , NBOT    , NTOP    , IZR     ,
     &           IZD     , INDD    , NENRGY  , NMINF   , NMAXF   ,
     &           IDZ0    , IRZ0    , IRZ1    , IRZ2    , IPAN    ,
     &           NBEAM   , ITHEOR  , IBSTAT  , IEMMS   , NOLINE  ,
     &           NPLINE  , NTAB    , NGRF    , NUMIN   , NUMAX
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , EM
C-----------------------------------------------------------------------
      LOGICAL    OPEN10  , LGHOST  , LDSEL   , LPARMS  , LSETL   ,
     &           LSETM   , LPEND   , LRTTB   , LPLT1   , LGRD1   ,
     &           LDEF1   
C-----------------------------------------------------------------------
CX   UNIX PORT - Following variables added for output selection options
      LOGICAL    LGRAPH  , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , REP*3      , DSFULL*80  , TITLEF*80  ,
     &           SYMBR*2    , SYMBD*2    , TITLE*40
      CHARACTER  RMASS*7  
CX   UNIX PORT - Following is name of output text file
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER    LFORMA(MXNENG)  , NL(MXOBSL)      , NU(MXOBSL)      ,
     &           NPL(MXPRSL)     , NPU(MXPRSL)     , IDTAB(MXTAB)    ,
     &           IDGRF(MXGRF)
C-----------------------------------------------------------------------
      REAL*8     ENRGYA(MXNENG)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &           PL2A(MXNENG)    , PL3A(MXNENG)    , XSECTA(MXNENG)  ,
     &           BMENA(MXBEAM)   , BMFRA(MXBEAM)   , EMISA(MXOBSL)   ,
     &           XLG(NGFPLN)     , XUG(NGFPLN)     , YLG(NGFPLN)     ,
     &           YUG(NGFPLN)     , QTHEX(MXNSHL)   , QTHCH(MXNSHL)   ,
     &           QEX(MXNSHL)     , TOTPOP(MXPRSL)  , TOTEMI(MXPRSL)  ,
     &           AVRGWL(MXPRSL)  , QEFF(MXPRSL)    ,
     &           TBLF((MXNSHL*(MXNSHL+1))/2)    ,
     &           TBQEX((MXNSHL*(MXNSHL+1))/2)   ,
     &           FTHEX((MXNSHL*(MXNSHL+1))/2)   ,
     &           FTHCH((MXNSHL*(MXNSHL+1))/2)   ,
     &           TBQMEP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM((MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
      REAL*8     XSECNA(MXNENG,MXNSHL)                            ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)             ,
     &           TBLPOP(MXPRSL,2*MXNSHL-3)                        ,
     &           TBLEMI(MXPRSL,2*MXNSHL-3)                        ,
     &           TBLWLN(MXPRSL,2*MXNSHL-3)
C-----------------------------------------------------------------------
      INTEGER    PIPEIN,     PIPEOU,     ISTOP   , ONE
      PARAMETER (PIPEIN = 5, PIPEOU = 6, ONE=1)
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      OPEN10 = .FALSE.
      LGHOST = .FALSE.
      NTOT = MXNSHL
      NTOP = MXNSHL
      NGRND = 1
      NBOT = 2
C
C-----------------------------------------------------------------------
C START OF CONTROL LOOP.
C-----------------------------------------------------------------------
C
    1 CONTINUE
      IPAN = 0
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 THEN CLOSE THE UNIT.
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
         CLOSE( IUNT10 )
         OPEN10 = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
CX GET INPUT DATA SET NAME (FROM IDL).
C-----------------------------------------------------------------------
C
      CALL C8SPF0( REP , DSFULL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN.
C-----------------------------------------------------------------------
C
      IF (REP.EQ.'YES') THEN
CX       IF (LGHOST) CALL XXENDG
         GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD'  )
      OPEN10 = .TRUE.
CX
CX----------------------------------------------------------------------
CX IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CX----------------------------------------------------------------------
CX THIS PART IS NOW HANDLED BY THE IDL SUBROUTINE XXTEXT.PRO
CX----------------------------------------------------------------------
CX
CX    IF (LDSEL) THEN
CX       CALL XXTEXT(IUNT10)
CX       GOTO 1
CX    ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
      call xxdata_01( iunt10 , mxneng , mxnshl ,
     &                symbr  , symbd  , izr    , izd    ,
     &                indd   , nenrgy , nminf  , nmaxf  ,
     &                lparms , lsetl  , enrgya ,
     &                alphaa , lforma , xlcuta , pl2a   ,
     &                pl3a   , xsecta , xsecna , fracla
     &              )
C
C-----------------------------------------------------------------------
C SET UP TARGET CHARGES.
C-----------------------------------------------------------------------
C
      CALL C8CHRG( SYMBD  , IZD    , SYMBR  , IZR    , IDZ0   ,
     &             IRZ0   , IRZ1   , IRZ2                       )
C
C-----------------------------------------------------------------------
C CONVERT L AND M RESOLVED CROSS-SECTIONS TO FRACTIONS.
C-----------------------------------------------------------------------
C
      CALL CXFRAC( MXNENG , MXNSHL , NENRGY , NMINF  , NMAXF  ,
     &             LSETL  , XSECNA , FRACLA
     &           )
C
C-----------------------------------------------------------------------
C EXTRAPOLATE N AND L RESOLVED CROSS-SECTIONS BEYOND NMINF AND NMAXF.
C-----------------------------------------------------------------------
C
      CALL CXEXTR( MXNENG , MXNSHL , NMINF  , NMAXF  , NENRGY ,
     &             LPARMS , ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &             PL3A   , XSECNA , FRACLA                     )
C
C-----------------------------------------------------------------------
CX GET NON-USER-DEFINED DATA FROM THE INPUT FILE.
C-----------------------------------------------------------------------
C
      CALL CXSETP( MXTAB  , MXGRF  , SYMBD  , IDZ0   , SYMBR  ,
     &             IRZ0   , IRZ1   , IRZ2   , NGRND  , NTOT     )

C
C-----------------------------------------------------------------------
CX GET USER-DEFINED VALUES (FROM IDL)
C-----------------------------------------------------------------------
C
C
C IF THE USER CAME IN FROM THE INPUT SCREEN KILL THE INFORMATION
C WIDGET
C
      WRITE(PIPEOU,*) ONE
      CALL XXFLSH(PIPEOU)
    2 CONTINUE
C
      IPAN = 0
      LPEND = .FALSE.
C
      CALL C8ISPF( MXBEAM , MXOBSL , MXPRSL , MXTAB  , MXGRF  ,
     &             NGFPLN , NGRND  , NTOT   , IPAN   , LPEND  ,
     &             TITLE  , RAMSNO , TIEV   , TEV    , DENSZ  ,
     &             DENS   , ZEFF   , BMAG   , NBEAM  , BMFRA  ,
     &             BMENA  , ITHEOR , IBSTAT , IEMMS  , NOLINE ,
     &             NU     , NL     , EMISA  , NPLINE , NPU    ,
     &             NPL    , NTAB   , NGRF   , IDTAB  , IDGRF  ,
     &             LRTTB  , LPLT1  , LGRD1  , LDEF1  , XLG    ,
     &             XUG    , YLG    , YUG                        )

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 1

C-----------------------------------------------------------------------
C SET THE RECEIVER ATOMIC MASS.
C-----------------------------------------------------------------------
      
      WRITE(RMASS,'(F7.3)')RAMSNO
      CALL XXRAMS(RMASS)

C-----------------------------------------------------------------------
C  SET UP TABLES OF ALL NECESSARY ATOMIC DATA FOR SUBSEQUENT LOOKUP.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C FILL RADIATIVE LIFETIME TABLE.
C-----------------------------------------------------------------------
C
      CALL CXTBLF( MXNSHL , IRZ1   , NBOT   , NTOP   , TBLF )
C
C-----------------------------------------------------------------------
C FILL EXCITATION RATE TABLES.
C-----------------------------------------------------------------------
C
      CALL CXTBEX( MXNSHL , IRZ1   , NBOT   , NTOP   , NGRND  ,
     &             TEV    , TBQEX  , QTHEX  , FTHEX             )
C
C-----------------------------------------------------------------------
C CALCULATE CHARGE EXCHANGE RECOMBINATION RATES.
C-----------------------------------------------------------------------
C
      IF (ITHEOR .EQ. 2) THEN
C
C-----------------------------------------------------------------------
C EIKONAL TREATMENT.
C-----------------------------------------------------------------------
C
         CALL CXQEIK( MXNSHL , MXBEAM , IRZ1   , IBSTAT ,
     &                NBOT   , NTOP   , NBEAM  , BMENA  ,
     &                BMFRA  , QTHCH  , FTHCH             )
C
C-----------------------------------------------------------------------
C
      ELSE
C
C-----------------------------------------------------------------------
C NON-EIKONAL TREATMENT.
C-----------------------------------------------------------------------
C
         CALL CXQXCH ( MXNENG , MXNSHL , MXBEAM , NBEAM  ,
     &                 BMENA  , BMFRA  , NBOT   , NTOP   ,
     &                 NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                 ALPHAA , XSECNA , FRACLA , QTHCH  ,
     &                 FTHCH                               )
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C FILL L SHELL MIXING TRANSITION RATE TABLES.
C-----------------------------------------------------------------------
C
      CALL C8TBQM( MXNSHL , IRZ0   , IRZ1   , NBOT   , NTOP   ,
     &             TEV    , DENS   , ZEFF   , TIEV   , EMP    ,
     &             TBLF   , TBQMEP , TBQMEM , TBQMIP , TBQMIM   )
C
C-----------------------------------------------------------------------
C CHOOSE BETWEEN CHARGE EXCHANGE AND EXCITATION
C  EMISSIVITY SOLUTION
C-----------------------------------------------------------------------
C
      IF (IEMMS .EQ. 1) THEN
C
         CALL C8EMIS( MXNSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   ,
     &                DENSZ  , DENS   , NOLINE , NU     ,
     &                NL     , EMISA  , NPLINE , NPU    ,
     &                NPL    , QTHCH  , FTHCH  , TBQMEP ,
     &                TBQMEM , TBQMIP , TBQMIM , NUMIN  ,
     &                NUMAX  , EM     , QEX    , TOTPOP ,
     &                TOTEMI , AVRGWL , QEFF   , TBLPOP ,
     &                TBLEMI , TBLWLN                     )
C
C-----------------------------------------------------------------------
C
C
      ELSE IF (IEMMS .EQ. 2) THEN
C
         CALL C8EMIS( MXNSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   ,
     &                DENSZ  , DENS   , NOLINE , NU     ,
     &                NL     , EMISA  , NPLINE , NPU    ,
     &                NPL    , QTHEX  , FTHEX  , TBQMEP ,
     &                TBQMEM , TBQMIP , TBQMIM , NUMIN  ,
     &                NUMAX  , EM     , QEX    , TOTPOP ,
     &                TOTEMI , AVRGWL , QEFF   , TBLPOP ,
     &                TBLEMI , TBLWLN                     )
C
      ENDIF
C
C-----------------------------------------------------------------------
CX ADDED THE NEW CALL TO C8SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CX A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------
C
C IF THE USER CAME IN FROM THE PROCESSING SCREEN KILL THE INFORMATION
C WIDGET
C
      WRITE(PIPEOU,*) ONE
      CALL XXFLSH(PIPEOU)
3     CONTINUE

      CALL C8SPF1( DSFULL        ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             LPEND
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

CX IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
      IF (LPEND) GOTO 2
C
C  IF SELECTED - GENERATE GRAPH
CX UNIX PORT - C8OUTG COMMUNICATES WITH IDL VIA PIPE.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN

C-----------------------------------------------------------------------
C OUTPUT GRAPHS.
C-----------------------------------------------------------------------
C
          CALL C8OUTG( MXNSHL , MXBEAM , MXOBSL , MXPRSL , MXGRF  ,
     &             NGFPLN , NGRF   , IDGRF  , LPLT1  , LGRD1  ,
     &             LDEF1  , DATE   , TITLE  , DSFULL , SYMBD  ,
     &             IDZ0   , SYMBR  , IRZ0   , IRZ1   , IRZ2   ,
     &             RAMSNO , TEV    , TIEV   , DENS   , DENSZ  ,
     &             ZEFF   , BMAG   , NBEAM  , BMFRA  , BMENA  ,
     &             NOLINE , NU     , NL     , EMISA  , ITHEOR ,
     &             IBSTAT , IEMMS  , NPLINE , NPU    , NPL    ,
     &             XLG    , XUG    , YLG    , YUG    , QEFF   ,
     &             TBLEMI , TBLWLN , LGHOST                     )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF(ISTOP.EQ.1) GOTO 9999

      ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT TEXT RESULTS.
C-----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT7, FILE = SAVFIL, STATUS='UNKNOWN')
         CALL C8OUT0( IUNT7  , MXNSHL , MXBEAM , MXOBSL , MXPRSL ,
     &                MXTAB  , NUMIN  , NUMAX  , NBOT   , NTOP   ,
     &                NTAB   , IDTAB  , LRTTB  , DATE   , TITLE  ,
     &                DSFULL , SYMBD  , IDZ0   , SYMBR  , IRZ0   ,
     &                IRZ1   , IRZ2   , RAMSNO , TEV    , TIEV   ,
     &                DENS   , DENSZ  , ZEFF   , BMAG   , NBEAM  ,
     &                BMFRA  , BMENA  , NOLINE , NU     , NL     ,
     &                EMISA  , ITHEOR , IBSTAT , IEMMS  , EM     ,
     &                QEX    , TBLF   , TBQEX  , QTHEX  , FTHEX  ,
     &                QTHCH  , FTHCH  , TBQMEP , TBQMEM , TBQMIP ,
     &                TBQMIM , NPLINE , NPL    , NPU    , TOTPOP ,
     &                TOTEMI , AVRGWL , QEFF   , TBLPOP , TBLEMI ,
     &                TBLWLN                                       )
         CLOSE(IUNT7)
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO IDL CONTROL
C-----------------------------------------------------------------------
C
      GOTO 3
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE
      STOP
C
C-----------------------------------------------------------------------
C
      END
