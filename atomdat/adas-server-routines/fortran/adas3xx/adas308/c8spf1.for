CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8spf1.for,v 1.1 2004/07/06 11:56:05 whitefor Exp $ Date $Date: 2004/07/06 11:56:05 $
CX
      SUBROUTINE C8SPF1( DSFULL     , 
     &                   LGRAPH     , L2FILE     , SAVFIL   ,
     &                   LPEND
     &                 )

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8SPF1 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS308
C
C  SUBROUTINE:
C
C  INPUT :   (C*80)  DSFULL   = DATA FILE NAME
C  INPUT :   (I*4)   IBSEL    = SELECTED INPUT DATA BLOCK FOR ANALYSIS
C  INPUT :   (L*4)   LFSEL    = .TRUE.  => POLYNOMIAL FIT SELECTED
C				.FALSE. => NO POLYNOMIAL FIT SELECTED
C  OUTPUT:   (L*4)   LDEF1    = .TRUE.  => USER SLECTED AXES LIMITS
C				.FALSE. => NO USER SUPPLIED LIMITS
C  OUTPUT:   (L*4)   LGRAPH   = .TRUE.  => SELECT GRAPHICAL OUTPUT
C                             = .FALSE. => DO NOT SELECT GRAPHICAL OUTPUT
C  OUTPUT:   (L*4)   L2FILE   = .TRUE.  => SAVE DATA TO FILE
C				.FALSE. => DO NOT SAVE DATA TO FILE
C  OUTPUT:   (C*80)  SAVFIL   = FILENAME FOR SAVING DATA
C  OUTPUT:   (R*8)   XMIN     = LOWER LIMIT FOR TEMP OR DENSITY UNITS 
C  OUTPUT:   (R*8)   XMAX     = UPPER LIMIT FOR TEMP OR DENSITY UNITS
C  OUTPUT:   (R*8)   YMIN     = LOWER LIMITS IONIZATIONS/PHOTON
C  OUTPUT:   (R*8)   YMAX     = UPPER LIMIT IONIZATIONS/PHOTON
C  OUTPUT:   (L*4)   LPEND    = .TRUE.  => PROCESS OUTPUT OPTIONS
C  				.FALSE. => CANCEL OUTPUT OPTIONS
C
C	     (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C	     (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C	     (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C	     (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH    IDL-ADAS   CALLS FLUSH TO CLEAR PIPES
C
C AUTHOR:  Tim Hammond (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    16-May-1995
C
C MODIFIED :	
C
C-----------------------------------------------------------------------
      CHARACTER    DSFULL*80   , SAVFIL*80
C-----------------------------------------------------------------------
      LOGICAL      LPEND       , LGRAPH      , L2FILE      ,
     &             LFSEL       , LDEF1
C-----------------------------------------------------------------------
      INTEGER      IBSEL       , ILOGIC      , 
     &             PIPEIN      , PIPEOU      , ONE         , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
C  READ USER OPTIONS FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
	 LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
C
      IF (.NOT.LPEND) THEN
         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN
            LGRAPH = .TRUE.
            READ(PIPEIN,*) ILOGIC
            IF (ILOGIC .EQ. ONE) THEN
               LDEF1  = .TRUE.
            ELSE
               LDEF1  = .FALSE.
            ENDIF
         ELSE
            LGRAPH = .FALSE.
         ENDIF

         READ(PIPEIN,*) ILOGIC
         IF (ILOGIC .EQ. ONE) THEN 
	    L2FILE = .TRUE.
            READ(PIPEIN, '(A)') SAVFIL
         ELSE
            L2FILE = .FALSE.
         ENDIF
C
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
