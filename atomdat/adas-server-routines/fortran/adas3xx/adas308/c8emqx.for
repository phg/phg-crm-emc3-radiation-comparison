CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8emqx.for,v 1.1 2004/07/06 11:55:03 whitefor Exp $ Date $Date: 2004/07/06 11:55:03 $
CX
      SUBROUTINE C8EMQX( MXNSHL , MXOBSL , IZ1    , NBOT   ,
     &                   NUMIN  , NUMAX  , NU     , NL     ,
     &                   EMISA  , NREP   , ICREP  , QTHEOR ,
     &                   WLOW   , EM     , QEX
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8EMQX *********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: C8EMIS
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  MXOBSL   = MAXIMUM NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NUMIN    = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NUMAX    = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS OF
C                            OBSERVED SPECTRUM LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS OF
C                            OBSERVED SPECTRUM LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (R*8)  EMISA()  = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                            LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NREP     =
C  INPUT : (I*4)  ICREP()  =
C                            DIMENSION: MXOBSL.
C  INPUT : (R*8)  QTHEOR() = MEAN CHARGE EXCHANGE OR EXCITATION RATE
C                            COEFFICIENTS FOR N-LEVELS AVERAGED OVER
C                            BEAM FRACTIONS.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: PRINCIPAL QUANTUM NUMBER INDEX.
C  INPUT : (R*8)  WLOW(,)  =
C                            1ST DIMENSION: REFERENCED BY I4IDFL(N,L).
C                            2ND DIMENSION: REFERENCED BY L+1.
C
C  OUTPUT: (R*8)  EM       = EMMISSION MEASURE.
C  OUTPUT: (R*8)  QEX()    =
C                            DIMENSION: MXNSHL.
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C  PARAM : (I*4)  MXOB     = MXOBSL.
C
C          (I*4)  N        = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L        = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  N1       = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L1       = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  IC       = ARRAY INDEX.
C          (I*4)  IC1      = ARRAY INDEX.
C          (I*4)  IC2      = ARRAY INDEX.
C          (I*4)  IFAIL    = RETURN FLAG FROM NAG ROUTINE.
C          (I*4)  M        = LOOP INDEX.
C          (I*4)  I        = LOOP INDEX.
C          (I*4)  J        = ARRAY INDEX.
C
C          (R*8)  XM       = REAL VALUE = M.
C          (R*8)  X1       =
C          (R*8)  X2       =
C          (R*8)  T1       =
C          (R*8)  T2       =
C          (R*8)  SUM      =
C          (R*8)  SUMEX    =
C          (R*8)  SUMTH    =
C
C          (I*4)  JLIST()  =
C                            DIMENSION: MXN.
C
C          (R*8)  REMISA() =
C                            DIMENSION: MXOB.
C          (R*8)  REMQ()   =
C                            DIMENSION: MXOB.
C          (R*8)  WKSPCE() = WORKSPACE FOR NAG ROUTINE.
C                            DIMENSION: MXOB.
C          (R*8)  EMQ()    =
C                            DIMENSION: MXNSHL.
C
C          (R*8)  CNDSA(,) = CONDENSED MATRIX.
C                            1ST DIMENSION: MXN.
C                            2ND DIMENSION: 2.
C          (R*8)  ARED(,)  = LINEAR CONDENSATION TRANSFORMATION ARRAY.
C                            1ST DIMENSION: MXN.
C                            2ND DIMENSION: MXN.
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          R8ATAB     ADAS      RETURNS HYDRONIC L-RESOLVED A-VALUES.
C                               IF INPUT QUANTUM NUMBERS ARE INVALID
C                               THEN RETURNS ZERO.
C          XXFLSH     ADAS      FLUSHES UNIX PIPE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    14/10/93
C
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C           CHANGED ...J.EQ.NREP... TO ...J.NE.NREP... IN THE IF
C           STATEMENT AFTER LINE 2.
C	    GENERAL UNIX PORT.
C
C DATE:    20/6/95
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
      REAL*8     R8ATAB
C-----------------------------------------------------------------------
      INTEGER    MXN       , MXOB
      PARAMETER( MXN = 20  , MXOB = 10 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXOBSL  , IZ1     , NBOT    , NUMIN   ,
     &           NUMAX   , NREP
      INTEGER    N       , L       , N1      , L1      , IC      ,
     &           IC1     , IC2     , IFAIL   , M       , I       ,
     &           J
      INTEGER    PIPEOU  , PIPEIN  
      PARAMETER (PIPEOU=6, PIPEIN=5)
C-----------------------------------------------------------------------
      REAL*8     EM
      REAL*8     XM      , X1      , X2      , T1      , T2      ,
     &           SUM     , SUMEX   , SUMTH
C-----------------------------------------------------------------------
      INTEGER    ICREP(MXOBSL) , NU(MXOBSL)   , NL(MXOBSL)
      INTEGER    JLIST(MXN)
C-----------------------------------------------------------------------
      REAL*8     EMISA(MXOBSL)   , QTHEOR(MXNSHL)  , QEX(MXNSHL)
      REAL*8     REMISA(MXOB)    , REMQ(MXOB)      , WKSPCE(MXOB)    ,
     &           EMQ(MXN)
      REAL*8     WLOW((MXNSHL*(MXNSHL+1))/2,MXNSHL)
      REAL*8     CNDSA(MXN,2)                       ,
     &           ARED(MXOB,MXOB)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETERS 'MXN' AND 'MXOB'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXOB .LT. MXOBSL) THEN
         WRITE(I4UNIT(-1),1001) MXOB, MXOBSL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      IF (NUMIN .EQ. NUMAX) THEN
C
         JLIST(NUMIN) = 1
         CNDSA(NUMIN,1) = 1.0D0
         CNDSA(NUMIN,2) = 0.0D0
C
      ELSE
C
         DO 1 M = NUMIN , NUMAX
C
            J = 1
    2       CONTINUE
            IF ((M .GE. NU(ICREP(J))) .AND. (J .NE. NREP)) THEN
               J = J + 1
               GOTO 2
            ENDIF
            JLIST(M) = J - 1
C
            XM = DFLOAT( M )
            IC1 = ICREP(J-1)
            IC2 = ICREP(J)
            X1 = DFLOAT( NU(IC1) )
            X2 = DFLOAT( NU(IC2) )
C
            IF ((QTHEOR(M) .EQ. 0.0D0)       .OR.
     &          (QTHEOR(NU(IC1)) .EQ. 0.0D0) .OR.
     &          (QTHEOR(NU(IC2)) .EQ. 0.0D0)      ) THEN
               T1 = ( XM - X2 ) / ( X1 - X2 )
               T2 = ( XM - X1 ) / ( X2 - X1 )
            ELSE
               T1 = ( XM - X2 ) * QTHEOR(M)
     &              / ( ( X1 - X2 ) * QTHEOR(NU(IC1)) )
               T2 = ( XM - X1 ) * QTHEOR(M)
     &              / ( ( X2 - X1 ) * QTHEOR(NU(IC2)) )
            ENDIF
C
            CNDSA(M,1) = T1
            CNDSA(M,2) = T2
           
C
    1    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      DO 3 I = 1 , NREP
C
         IC = ICREP(I)
         N  = NU(IC)
         N1 = NL(IC)
C
         DO 4 J = 1 , NREP
            ARED(I,J) = 0.0D0
    4    CONTINUE
C
         DO 5 M = N , NUMAX
C
            SUM = 0.0D0
            DO 6 L = 0 , N-1
               DO 7 L1 = L-1 , L+1 , 2
                  SUM = SUM + R8ATAB( IZ1 , N , L , N1 , L1 )
     &                  * WLOW(I4IDFL( N , L ),M)
    7          CONTINUE
    6       CONTINUE
C
            J = JLIST(M)
            ARED(I,J) = ARED(I,J) + SUM * CNDSA(M,1)
            IF (NUMIN .NE. NUMAX) THEN
               ARED(I,J+1) = ARED(I,J+1) + SUM * CNDSA(M,2)
            ENDIF
C
    5    CONTINUE
C
         REMISA(I) = EMISA(IC)
C
    3 CONTINUE
C
C-----------------------------------------------------------------------
C SOLVE FOR EMISSION MEASURE * CHARGE EXCHANGE PRODUCTS REMQ FOR
C REPRESENTATIVE LEVELS.
C-----------------------------------------------------------------------
C
CX    IFAIL = 0
CX    CALL F04ARF( ARED , MXOB , REMISA , NREP , REMQ , WKSPCE , IFAIL )
CX----------------------------------------------------------------------
CX    THE ABOVE IS A CALL TO THE NAG MATHEMATICAL LIBRARY. THIS HAS
CX    NOW BEEN REPLACED BY THE APPROPRIATE IDL ROUTINE. THE RELEVANT
CX    VARIABLES ARE THEREFORE FED TO THE IDL ROUTINE c8emqx.pro AND
CX    THE RESULT RETURNED INTO THE VARIABLE REMQ 
CX----------------------------------------------------------------------
CX    WRITE OUT DATA TO PIPE
CX----------------------------------------------------------------------

      WRITE (PIPEOU, *) NREP
      CALL XXFLSH (PIPEOU)
      DO 100,I=1,NREP
            DO 200,J=1,NREP
                WRITE( PIPEOU, *) ARED(I,J)
                CALL XXFLSH (PIPEOU)
200         CONTINUE
            WRITE( PIPEOU, *) REMISA(I)
            CALL XXFLSH (PIPEOU)
100   CONTINUE

CX----------------------------------------------------------------------
CX    READ RESULT FROM THE PIPE
CX----------------------------------------------------------------------

      DO 300,I=1,NREP
300       READ(PIPEIN, *) REMQ(I)


      
C-----------------------------------------------------------------------
C EXTEND TO FULL SET OF LEVELS BETWEEN NUMIN AND NUMAX AND COMPARE SUM
C WITH THEORETICAL SUM.
C-----------------------------------------------------------------------
C
      SUMEX = 0.0D0
      SUMTH = 0.0D0
      DO 8 M = NUMIN , NUMAX
         J = JLIST(M)
         EMQ(M) = CNDSA(M,1) * REMQ(J)
         IF (NUMIN .NE. NUMAX) THEN
            EMQ(M) = EMQ(M) + CNDSA(M,2) * REMQ(J+1)
         ENDIF
         SUMEX = SUMEX + EMQ(M)
         SUMTH = SUMTH + QTHEOR(M)
    8 CONTINUE
C
C-----------------------------------------------------------------------
C CALCULATE THE EMISSION MEASURE.
C-----------------------------------------------------------------------
C
      EM = SUMEX / SUMTH
C
C-----------------------------------------------------------------------
C ISOLATE THE EXPERIMENTAL MEAN CHARGE EXCHANGE RATE COEFFTS.
C-----------------------------------------------------------------------
C
      DO 9 N = NUMIN , NUMAX
         QEX(N) = EMQ(N) / EM
    9 CONTINUE
C
C-----------------------------------------------------------------------
C EXTEND EXPERIMENTAL MEAN CHARGE EXCHANGE RATE COEFFICIENTS TO         N
C N < NUMIN USING THE THEORETICAL RATIO.
C-----------------------------------------------------------------------
C
      DO 10 N = NBOT , NUMIN
         IF (QTHEOR(NUMIN) .GT. 0.0D0) THEN
            QEX(N) = QTHEOR(N) * QEX(NUMIN) / QTHEOR(NUMIN)
         ELSE
            QEX(N) = 0.0D0
         ENDIF
   10 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C8EMQX ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C8EMQX.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C8EMQX ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXOB'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXOBSL''.'/
     &        2X, 'MXOB = ', I3 , '   MXOBSL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXOB'' IN SUBROUTINE C8EMQX.'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
