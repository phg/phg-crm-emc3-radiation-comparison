CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8chrg.for,v 1.1 2004/07/06 11:54:56 whitefor Exp $ Date $Date: 2004/07/06 11:54:56 $
CX
      SUBROUTINE C8CHRG( SYMBD , IZD   , SYMBR , IZR   , IDZ0  ,
     &                   IRZ0  , IRZ1  , IRZ2
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8CHRG *********************
C
C  PURPOSE:  SETS UP NUCLEAR CHARGE OF DONOR AND NULEAR, INITIAL AND
C            FINAL CHARGES OF RECEIVER. CHECKS VALIDITY OF RECEIVER
C            CHARGES.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (C*2)  SYMBD   = DONOR ELEMENT SYMBOL.
C  INPUT : (I*4)  IZD     = DONOR ION CHARGE.
C  INPUT : (C*2)  SYMBR   = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IZR     = RECEIVER ION CHARGE.
C
C  OUTPUT: (I*4)  IDZ0    = DONOR NUCLEAR CHARGE.
C  OUTPUT: (I*4)  IRZ0    = RECEIVER NUCLEAR CHARGE.
C  OUTPUT: (I*4)  IRZ1    = RECEIVER ION INITIAL CHARGE.
C  OUTPUT: (I*4)  IRZ2    = RECEIVER ION FINAL CHARGE.
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           I4UNIT      ADAS        RETURNS UNIT NO. FOR OUTPUT MESSAGES
C           CXCHRG      ADAS        RETURNS DONOR NUCLEAR CHARGE AND
C                                   RECEIVER NULEAR, INITIAL AND FINAL
C                                   CHARGES.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    26/11/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    IZR     , IZD     , IDZ0    , IRZ0    , IRZ1    ,
     &           IRZ2
C-----------------------------------------------------------------------
      CHARACTER  SYMBD*2  , SYMBR*2
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET UP CHARGES.
C-----------------------------------------------------------------------
C
      CALL CXCHRG( SYMBD , IZD   , SYMBR , IZR   , IDZ0  ,
     &             IRZ0  , IRZ1  , IRZ2                    )
C
C-----------------------------------------------------------------------
C CHECK CHARGES.
C-----------------------------------------------------------------------
C
      IF (IRZ1 .NE. IRZ0) THEN
         WRITE(I4UNIT(-1),1000) IRZ0 , IRZ1
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C8CHRG ERROR ', 32('*') //
     &        2X, 'NUCLEAR AND ION CHARGES ARE NOT EQUAL.'/
     &        2X, 'NUCLEAR CHARGE Z0 = ', I3 ,
     &            '   ION CHARGE Z1 = ', I3/
     &        2X, 'DIFFERENCE MUST BE 0 (H-LIKE).'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
