CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8emis.for,v 1.1 2004/07/06 11:54:59 whitefor Exp $ Date $Date: 2004/07/06 11:54:59 $
CX
      SUBROUTINE C8EMIS( MXNSHL , MXOBSL , MXPRSL , IZ0    ,
     &                   IZ1    , NGRND  , NTOT   , NBOT   ,
     &                   DENSZ  , DENS   , NOLINE , NU     ,
     &                   NL     , EMISA  , NPLINE , NPU    ,
     &                   NPL    , QTHEOR , FTHEOR , TBQMEP ,
     &                   TBQMEM , TBQMIP , TBQMIM , NUMIN  ,
     &                   NUMAX  , EM     , QEX    , TOTPOP ,
     &                   TOTEMI , AVRGWL , QEFF   , TBLPOP ,
     &                   TBLEMI , TBLWLN
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8EMIS *********************
C
C  PURPOSE:  PREDICTS THE L-RESOLVED EMISSIVITY FOR REQUESTED
C            TRANSITIONS.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  MXOBSL   = MAXIMUM NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  MXPRSL   = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                            PREDICT.
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NGRND    = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  NTOT     = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                            STATE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  DENSZ    = PLASMA ION DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (I*4)  NOLINE   = NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS OF
C                            OBSERVED SPECTRUM LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS OF
C                            OBSERVED SPECTRUM LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (R*8)  EMISA()  = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                            LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NPLINES  = NUMBER OF SPECTRUM LINES TO PREDICT.
C  INPUT : (I*4)  NPU()    = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS OF
C                            SPECTRUM LINES TO PREDICT.
C                            DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NPL()    = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS OF
C                            SPECTRUM LINES TO PREDICT.
C                            DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  QTHEOR() = MEAN CHARGE EXCHANGE OR EXCITATION RATE
C                            COEFFICIENTS FOR N-LEVELS AVERAGED OVER
C                            BEAM FRACTIONS.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  INPUT : (R*8)  FTHEOR() = MEAN CHARGE EXCHANGE OR EXCITATION RATE
C                            FOR NL-LEVELS AS A FRACTION OF
C                            CORRESPONDING N-LEVEL.
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEP() = ELECTRON RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM() = ELECTRON RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP() = POSITIVE ION RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM() = POSITIVE ION RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (I*4)  NUMIN    = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  OUTPUT: (I*4)  NUMAX    = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  OUTPUT: (R*8)  EM       = EMMISSION MEASURE.
C  OUTPUT: (R*8)  QEX()    =
C                            DIMENSION: MXNSHL.
C  OUTPUT: (R*8)  TOTPOP() = TOTAL COLLISION POP. FOR PREDICTED SPECTRUM
C                            LINE.
C                            UNITS: CM-2
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TOTEMI() = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS: PH CM-2 SEC-1
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  AVRGWL() = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS: A
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  QEFF()   = EFF. RATE COEFFICIENT FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS:
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TBLPOP(,)= TABLE OF COLLISION POP. FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS: CM-2
C                            1ST DIMENSION: PREDICTED LINE INDEX.
C                            2ND DIMENSION: REFERENCED BY FUNC I4IDLI().
C  OUTPUT: (R*8)  TBLEMI(,)= TABLE OF COLLISION EMISSIVITIES FOR
C                            PREDICTED SPECTRUM LINE.
C                            UNITS: PH CM-2 SEC-1
C                            1ST DIMENSION: PREDICTED LINE INDEX.
C                            2ND DIMENSION: REFERENCED BY FUNC I4IDLI().
C  OUTPUT: (R*8)  TBLWLN(,)= TABLE OF WAVELENGTHS FOR PREDICTED SPECTRUM
C                            LINE.
C                            UNITS: A
C                            1ST DIMENSION: PREDICTED LINE INDEX.
C                            2ND DIMENSION: REFERENCED BY FUNC I4IDLI().
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C  PARAM : (I*4)  MXOB     = MXOBSL.
C
C          (I*4)  NREP     =
C          (I*4)  IC       = LOOP INDEX.
C
C          (I*4)  ICREP()  =
C                            DIMENSION: MXOB.
C
C          (R*8)  WHIGH()  =
C                            DIMENSION: REFERENCED BY L+1.
C          (R*8)  WLOW(,)  =
C                            1ST DIMENSION: REFERENCED BY I4IDFL(N,L).
C                            2ND DIMENSION: REFERENCED BY L+1.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          CXWFIL     ADAS
C          CXEMQX
C          CXPRSL     ADAS      PREDICTS REQUESTED SPECTRUM LINES.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    15/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    MXN        , MXOB
      PARAMETER( MXN = 20   , MXOB = 10  )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXOBSL  , MXPRSL  , IZ0     , IZ1     ,
     &           NGRND   , NTOT    , NBOT    , NOLINE  , NPLINE
      INTEGER    NREP    , NUMIN   , NUMAX   , IC
C-----------------------------------------------------------------------
      REAL*8     DENSZ   , DENS    , EM
C-----------------------------------------------------------------------
      INTEGER    NU(MXOBSL)   , NL(MXOBSL)   , NPU(MXPRSL)  ,
     &           NPL(MXPRSL)
      INTEGER    ICREP(MXOB)
C-----------------------------------------------------------------------
      REAL*8     EMISA(MXOBSL)                  ,
     &           QTHEOR(MXNSHL)                 ,
     &           QEX(MXNSHL)                    ,
     &           TOTPOP(MXPRSL)                 ,
     &           TOTEMI(MXPRSL)                 ,
     &           AVRGWL(MXPRSL)                 ,
     &           QEFF(MXPRSL)                   ,
     &           FTHEOR((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM((MXNSHL*(MXNSHL+1))/2)
      REAL*8     WHIGH((MXN*(MXN+1))/2)
C-----------------------------------------------------------------------
      REAL*8     TBLPOP(MXPRSL,2*MXNSHL-3)      ,
     &           TBLEMI(MXPRSL,2*MXNSHL-3)      ,
     &           TBLWLN(MXPRSL,2*MXNSHL-3)
      REAL*8     WLOW((MXN*(MXN+1))/2,MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXN' AND 'MXOB'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXOB .LT. MXOBSL) THEN
         WRITE(I4UNIT(-1),1001) MXOB, MXOBSL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      NREP = 1
      ICREP(1) = 1
      NUMIN = NU(1)
      NUMAX = NU(1)
C
      DO 1 IC = 2 , NOLINE
C
         IF (NU(IC) .NE. NU(IC-1)) THEN
            NREP = NREP + 1
            ICREP(NREP) = IC
         ENDIF
C
         NUMIN = MIN0( NUMIN , NU(IC) )
         NUMAX = MAX0( NUMAX , NU(IC) )
C
  1   CONTINUE
C
C-----------------------------------------------------------------------
C
      CALL C8WFIL( MXNSHL , IZ1    , NGRND  , NTOT   , NBOT   ,
     &             NUMAX  , DENSZ  , DENS   , QTHEOR , FTHEOR ,
     &             TBQMEP , TBQMEM , TBQMIP , TBQMIM , WHIGH  ,
     &             WLOW                                         )
C
C-----------------------------------------------------------------------
C
      CALL C8EMQX( MXNSHL , MXOBSL , IZ1    , NBOT   , NUMIN  ,
     &             NUMAX  , NU     , NL     , EMISA  , NREP   ,
     &             ICREP  , QTHEOR , WLOW   , EM     , QEX      )
C
C-----------------------------------------------------------------------
C PREDICT REQUESTED SPECTRUM LINES.
C-----------------------------------------------------------------------
C
      CALL C8PRSL( MXNSHL , MXPRSL , IZ0    , IZ1    , NPLINE ,
     &             NPU    , NPL    , NUMAX  , WHIGH  , WLOW   ,
     &             EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &             QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C8EMIS ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C8EMIS.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C8EMIS ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXOB'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXOBSL''.'/
     &        2X, 'MXOB = ', I3 , '   MXOBSL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXOB'' IN SUBROUTINE C8EMIS.'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
