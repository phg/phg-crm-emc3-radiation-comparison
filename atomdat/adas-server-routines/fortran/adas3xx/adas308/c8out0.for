CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8out0.for,v 1.4 2004/07/06 11:55:35 whitefor Exp $ Date $Date: 2004/07/06 11:55:35 $
CX
      SUBROUTINE C8OUT0( IWRITE , MXNSHL , MXBEAM , MXOBSL ,
     &                   MXPRSL , MXTAB  , NUMIN  , NUMAX  ,
     &                   NBOT   , NTOP   , NTAB   , IDTAB  ,
     &                   LRTTB  , DATE   , TITLE  , DSFULL ,
     &                   SYMBD  , IDZ0   , SYMBR  , IRZ0   ,
     &                   IRZ1   , IRZ2   , RAMSNO , TEV    ,
     &                   TIEV   , DENS   , DENSZ  , ZEFF   ,
     &                   BMAG   , NBEAM  , BMFRA  , BMENA  ,
     &                   NOLINE , NU     , NL     , EMISA  ,
     &                   ITHEOR , IBSTAT , IEMMS  , EM     ,
     &                   QEX    , TBLF   , TBQEX  , QTHEX  ,
     &                   FTHEX  , QTHCH  , FTHCH  , TBQMEP ,
     &                   TBQMEM , TBQMIP , TBQMIM , NPLINE ,
     &                   NPL    , NPU    , TOTPOP , TOTEMI ,
     &                   AVRGWL , QEFF   , TBLPOP , TBLEMI ,
     &                   TBLWLN
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8OUT0 *********************
C
C  PURPOSE: WRITES TEXT OUTPUT TO FILE FOR ADAS308.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)  IWRITE    = UNIT NUMBER FOR OUTPUT.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXBEAM    = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  INPUT : (I*4)  MXOBSL    = MAXIMUM NUMBER OF OBSERVED SPECTRUM
C                             LINES.
C  INPUT : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  INPUT : (I*4)  MXTAB     = MAXIMUM NUMBER OF TABLES FOR OUTPUT.
C  INPUT : (I*4)  NUMIN     = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NUMAX     = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             TABLES.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             TABLES.
C  INPUT : (I*4)  NTAB      = NUMBER OF TABLES FOR OUTPUT.
C  INPUT : (I*4)  IDTAB()   = LIST OF INDEXES OF TABLES FOR OUTPUT.
C                             DIMENSION: MXTAB
C  INPUT : (L*4)  LRTTB     = FLAG FOR RATE TABLE PRINTING.
C                             .TRUE.  = PRINT RATE TABLES.
C                             .FALSE. = DO NOT PRINT RATE TABLES.
C  INPUT : (C*8)  DATE      = DATE.
C  INPUT : (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C  INPUT : (C*80) DSFULL    = FILE NAME OF INPUT DATA SET.
C  INPUT : (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C  INPUT : (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C  INPUT : (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C  INPUT : (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C  INPUT : (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C  INPUT : (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C  INPUT : (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C  INPUT : (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C  INPUT : (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (I*4)  NOLINE    = NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NU()      = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NL()      = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (R*8)  EMISA()   = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  INPUT : (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  INPUT : (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C  INPUT : (R*8)  EM        = EMMISSION MEASURE.
C  INPUT : (R*8)  QEX()     =
C                             DIMENSION: MXNSHL.
C  INPUT : (R*8)  TBLF()    = TABLE OF RADIATIVE LIFETIMES.
C                             UNITS: SECS
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQEX()   = MEAN EXCITATION RATE COEFFICIENTS FOR
C                             NL-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  QTHEX()   = MEAN EXCITATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  FTHEX()   = FRACTION OF N-LEVEL MEAN EXCITATION RATE
C                             COEFFICIENTS IN NL-LEVEL.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  QTHCH()   = MEAN CHARGE EXCHANGE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  FTHCH()   = FRACTION OF N-LEVEL MEAN CHARGE EXCHANGE
C                             COEFFICIENTS IN NL-LEVEL.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEP()  = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NL->NL+1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM()  = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NL->NL-1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP()  = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NL->NL+1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM()  = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NL->NL-1.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C  INPUT : (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  TOTPOP()  = TOTAL COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  TOTEMI()  = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  AVRGWL()  = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS:
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  TBLPOP(,) = TABLE OF COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             1ST DIMENSION: PREDICTED LINE INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C  INPUT : (R*8)  TBLEMI(,) = TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: PREDICTED LINE INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C  INPUT : (R*8)  TBLWLN(,) = TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: PREDICTED LINE INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C
C          (I*4)   N        = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)   L        = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)   N1       = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)   L1       = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)   IDL      = L-RESOLVED TABLE INDEX.
C          (I*4)   ID       = SPECTRUM LINE TABLE INDEX.
C          (I*4)   I        = LOOP INDEX.
C          (I*4)   K        = ARRAY INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          I4IDLI     ADAS      RETURNS INDEX FOR PREDICTED SPECTRUM
C                               LINE TABLES.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    19/11/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 20-06-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 13-05-96
C               - REMOVED HOLLERITH CONSTANTS FROM OUTPUT AND TIDIED UP
C                 HEADER
C VERSION: 1.3                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C              - SWAPPED DENS AND DENSZ IN OUTPUT LINE - THEY WERE THE
C		 WRONG WAY ROUND
C
C VERSION: 1.4                          DATE: 24-11-98
C MODIFIED: RICHARD MARTIN & MARTIN OMULLANE
C		    - CORRECTED WRITING OF ELECTRON & ION TEMPS. FROM
C			BEING WRITTED WRONG WAY ROUND.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4IDFL  , I4IDLI
C-----------------------------------------------------------------------
      INTEGER    IWRITE  , MXNSHL  , MXBEAM  , MXOBSL  , MXPRSL  ,
     &           MXTAB   , NUMIN   , NUMAX   , NBOT    , NTOP    ,
     &           NTAB    , IDZ0    , IRZ0    , IRZ1    , IRZ2    ,
     &           NBEAM   , NOLINE  , ITHEOR  , IBSTAT  , IEMMS   ,
     &           NPLINE
      INTEGER    N       , L       , N1      , L1      , IDL     ,
     &           ID      , I       , K
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , EM
C-----------------------------------------------------------------------
      LOGICAL    LRTTB
C-----------------------------------------------------------------------
      CHARACTER  CADAS*80   , DATE*8     , TITLE*40   , DSFULL*80  ,
     &           SYMBR*2    , SYMBD*2
C-----------------------------------------------------------------------
      INTEGER    IDTAB(MXTAB)    , NU(MXOBSL)      , NL(MXOBSL)      ,
     &           NPU(MXPRSL)     , NPL(MXPRSL)
C-----------------------------------------------------------------------
      REAL*8     BMFRA(MXBEAM)   , BMENA(MXBEAM)   , QEX(MXNSHL)     ,
     &           QTHCH(MXNSHL)   , QTHEX(MXNSHL)   , EMISA(MXOBSL)   ,
     &           TOTPOP(MXPRSL)  , TOTEMI(MXPRSL)  , AVRGWL(MXPRSL)  ,
     &           QEFF(MXPRSL)    ,
     &           TBLF((MXNSHL*(MXNSHL+1))/2)    ,
     &           TBQEX((MXNSHL*(MXNSHL+1))/2)   ,
     &           FTHEX((MXNSHL*(MXNSHL+1))/2)   ,
     &           FTHCH((MXNSHL*(MXNSHL+1))/2)   ,
     &           TBQMEP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM((MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
      REAL*8     TBLPOP(MXPRSL,2*MXNSHL-3)      ,
     &           TBLEMI(MXPRSL,2*MXNSHL-3)      ,
     &           TBLWLN(MXPRSL,2*MXNSHL-3)
C-----------------------------------------------------------------------
      SAVE       CADAS
C-----------------------------------------------------------------------
      DATA       CADAS / ' ' /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C GET ADAS HEADER.
C-----------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C-----------------------------------------------------------------------
C WRITE TITLE INFORMATION.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001) 'L-RESOLVED CHARGE EXCHANGE EMMISIVITY',
     &                   'ADAS308', DATE
      WRITE(IWRITE,1002) TITLE
C
C-----------------------------------------------------------------------
C WRITE SUMMARY OF USER INPUT.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1003) DSFULL
      WRITE(IWRITE,1004) SYMBR , SYMBD , IRZ0 , IDZ0 , IRZ1 , IRZ2 ,
     &                   RAMSNO
      WRITE(IWRITE,1005) TIEV , TEV , DENSZ , DENS , ZEFF , BMAG
C
      WRITE(IWRITE,1006) NBEAM
      DO 1 I = 1 , NBEAM
         WRITE(IWRITE,1007) I , BMFRA(I) , BMENA(I)
    1 CONTINUE
C
      WRITE(IWRITE,1008) NOLINE
      DO 2 I = 1 , NOLINE
         WRITE(IWRITE,1009) I , NU(I) , NL(I) , EMISA(I)
    2 CONTINUE
C
      IF (ITHEOR .EQ. 1) THEN
         WRITE(IWRITE,1010) 'INPUT DATA SET'
      ELSE IF (ITHEOR .EQ. 2) THEN
         WRITE(IWRITE,1010) 'EIKONAL MODEL'
         IF (IBSTAT .EQ. 1) THEN
            WRITE(IWRITE,1011) 'H(1S)'
         ELSE IF (IBSTAT .EQ. 2) THEN
            WRITE(IWRITE,1011) 'H(2S)'
         ELSE IF (IBSTAT .EQ. 3) THEN
            WRITE(IWRITE,1011) 'H(2P)'
         ELSE IF (IBSTAT .EQ. 4) THEN
            WRITE(IWRITE,1011) 'H(1S2)'
         ELSE IF (IBSTAT .EQ. 4) THEN
            WRITE(IWRITE,1011) 'H(1S2S)'
         ENDIF
      ENDIF
C
      IF (IEMMS .EQ. 1) THEN
         WRITE(IWRITE,1012) 'CHARGE EXCHANGE'
         WRITE(IWRITE,1013) EM
         WRITE(IWRITE,1014)
         DO 3 N = NUMIN , NUMAX
            WRITE(IWRITE,1015) N, QEX(N), QTHCH(N)
    3    CONTINUE
      ELSE IF (IEMMS .EQ. 2) THEN
         WRITE(IWRITE,1012) 'ELECTRON IMPACT EXCITATION'
         WRITE(IWRITE,1013) EM
         WRITE(IWRITE,1014)
         DO 4 N = NUMIN , NUMAX
            WRITE(IWRITE,1015) N, QEX(N), QTHEX(N)
    4    CONTINUE
      ENDIF
C
C-----------------------------------------------------------------------
C WRITE OUT PREDICTED EMISSIVITIES.
C-----------------------------------------------------------------------
C
      IF ( NPLINE .EQ. 0 ) THEN
C
         WRITE(IWRITE,1016)
C
      ELSE
C
         WRITE(IWRITE,1017)
         DO 5 I = 1 , NTAB
            WRITE(IWRITE,1018)
            K = IDTAB(I)
            N  = NPU(K)
            N1 = NPL(K)
            DO 6 L = 0 , N1
               DO 7 L1 = L+1 , L-1 , -2
                  IF ((L1 .GE. 0) .AND. (L1 .LT. N1)) THEN
                     ID = I4IDLI( N1 , L , L1 )
                     WRITE(IWRITE,1019) N , L , N1 , L1 ,
     &                                  TBLPOP(K,ID) , TBLEMI(K,ID) ,
     &                                  TBLWLN(K,ID)
                  ENDIF
    7          CONTINUE
    6       CONTINUE
            WRITE(IWRITE,1020) TOTPOP(K), TOTEMI(K), AVRGWL(K), QEFF(K)
    5    CONTINUE
C
         WRITE(IWRITE,1021)
         DO 8 I = 1 , NPLINE
            N  = NPU(I)
            N1 = NPL(I)
            WRITE(IWRITE,1022) N , N1 , TOTPOP(I) , TOTEMI(I) ,
     &                         AVRGWL(I) , QEFF(I)
    8    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C IF REQUESTED, WRITE OUT RATE TABLES.
C-----------------------------------------------------------------------
C
      IF (LRTTB) THEN
C
         WRITE(IWRITE,1023)
         DO 9 N = NBOT , NTOP
            IDL = I4IDFL( N , 0 )
            WRITE(IWRITE,1024) N , IDL , QTHCH(N)
            DO 10 L = 0 , N-1
               IDL = I4IDFL( N , L )
               WRITE(7,1025) L , TBLF(IDL) , TBQEX(IDL) , FTHCH(IDL) ,
     &                       TBQMEP(IDL) , TBQMEM(IDL) , TBQMIP(IDL) ,
     &                       TBQMIM(IDL)
   10       CONTINUE
    9    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1H  , A79 )
 1001 FORMAT( 1H  , 15( '*' ) , ' TABULAR INPUT  FROM ' , A43 ,
     &        ' PROGRAM: ' , A7 , 1X , '- DATE: ' , A8 , 1X ,
     &        16( '*' ) / )
 1002 FORMAT( 1H  , 19( '-' ) , 1X , A40 , 1X , 19( '-' ) // )
 1003 FORMAT( 1H  , 'FILE: ', A80 // )
 1004 FORMAT( 1H  ,25X , 'RECEIVER' , 6X , 'NEUTRAL DONOR' /
     &        1H , 25X , 8( '-' ) , 6X , 13 ('-') /
     &        1H , 'ELEMENT SYMBOL         =' , 6X ,A2   , 12X , A2  /
     &        1H , 'NUCLEAR CHARGE         =' , 2X ,I6   ,  8X , I6  /
     &        1H , 'RECOMBINING ION CHARGE =' , 2X ,I6   , 13X , '-' /
     &        1H , 'RECOMBINED ION CHARGE  =' , 2X ,I6   , 13X , '-' /
     &        1H , 'ATOMIC MASS NUMBER     =' , 2X ,F6.2 , 13X , '-' )
 1005 FORMAT( / 1H  , 'PLASMA PARAMETERS:' //
     &        1H , ' ION TEMPERATURE      (EV)   = ' , 1P , D10.2 , 5X ,
     &        1H , ' ELECTRON TEMPERATURE (EV)   = ' , D10.2 , /
     &        1H , ' ION DENSITY          (CM-3) = ' , D10.2 , 5X ,
     &        1H , ' ELECTRON DENSITY     (CM-3) = ' , D10.2 , /
     &        1H , ' PLASMA EFFECTIVE Z          = ' , 0P , F6.2 , 9X ,
     &        1H , ' MAGNETIC INDUCTION   (T)    = ' , 0P , F6.2 )
 1006 FORMAT( / 1H  , 'BEAM PARAMETERS:' //
     &        1H , ' NUMBER OF BEAM COMPONENTS = ', I2 //
     &        1H , ' INDEX    FRACTION       ENERGY ' /
     &        1H , '                          (EV)  ' /
     &        1H , ' -------------------------------' )
 1007 FORMAT( 1H , 3X , I2 , 6X , F6.3 , 5X , 1P , D10.2 )
 1008 FORMAT( / 1H  , 'OBSERVED SPECTRUM LINES:' //
     &        1H , ' NUMBER OF OBSERVED SPECTRUM LINES = ', I2 //
     &        1H , ' INDEX   NU   NL     COL. EMIS.   ' /
     &        1H , '                   (PH CM-2 SEC-1)' /
     &        1H , ' ---------------------------------' )
 1009 FORMAT( 1H , 3X , I2 , 4X , I2 , 3X, I2 , 5X , 1P , D10.2 )
 1010 FORMAT( / 1H  , 'CHARGE EXCHANGE MODEL : ' , A )
 1011 FORMAT( 1H  , 'DONOR STATE           : ' , A )
 1012 FORMAT( 1H  , 'EMISSION MEASURE MODEL: ' , A )
 1013 FORMAT( / 1H  , ' EMISSION MEASURE (CM-5) = ', 1P , D12.4 )
 1014 FORMAT( / 1H  , '  N      QEX(N)        QTHEOR(N) ' /
     &        1H  , '       (CM3 SEC-1)    (CM3 SEC-1)' /
     &        1H  , ' --------------------------------' )
 1015 FORMAT( 1H  , 1X , I2 , 4X , 1P , D11.4 , 4X , D11.4 )
 1016 FORMAT( / 1H  , 'NO EMISSIVITIES TO BE PREDICTED' )
 1017 FORMAT( / 1H  , 40('-') , ' PREDICTED EMISSIVITES ' , 40('-') )
 1018 FORMAT( / 1H  , '  N  L    N1  L1     COL. POP.' ,
     &        '      COL. EMIS.                  AIR WVLN.' /
     &        1H , '                      (CM-2)  ' ,
     &        '   (PH CM-2 SEC-1)                   (A)   ' /
     &        1H , ' -----------------------------' ,
     &        '-------------------------------------------' )
 1019 FORMAT( 1H , 2I3 , 3X , 2I3 , 1P , D15.4 , 1X ,
     &        D15.4 , 17X , 0P , F10.2 )
 1020 FORMAT( 1H , ' ----------------------------' ,
     &        '--------------------------------------------' /
     &        1H , 9X , 'SUMS =' , 1P , D15.4 , 1X , D15.4 ,
     &        '    MEAN WVL(A) =' , 0P , F10.2 ,
     &        '   EFF. RATE COEFFT. =' , 1P , D12.4 )
 1021 FORMAT( / 1H  ,'SUMMARY OF EMISSIVITIES:' //
     &        1H ,'  N   N1      COL. POP.       COL. EMIS.  ' ,
     &        '     AIR WVLN.      EFF. COEFFT.' /
     &        1H ,'               (CM-2)      (PH CM-2 SEC-1)' ,
     &        '       (A)          (CM3 SEC-1)' /
     &        1H ,' -----------------------------------------' ,
     &        '--------------------------------' )
 1022 FORMAT( 1H , I3 , 2X , I3 , 1P , D15.4 , 2X , D15.4 , 6X ,
     &        0P , F10.2 , 3X , 1P , D15.4 )
 1023 FORMAT( / 1H  , 40('-') , '      RATE TABLES      ' , 40('-') //
     &        1H ,'    N    L  NPT     QTHCH      TBLF     TBQEX' ,
     &        '     FTHCH    TBQMEP    TBQMEM    TBQMIP    TBQMIM' /
     &        1H ,'   ------------------------------------------' ,
     &        '--------------------------------------------------' )
 1024 FORMAT( 1H , I5 , 5X , I5 , 1P , D10.2 )
 1025 FORMAT( 1H , 5X , I5 , 15X , 1P , 7D10.2 )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
