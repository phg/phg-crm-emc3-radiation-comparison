CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8wsol.for,v 1.1 2004/07/06 11:56:24 whitefor Exp $ Date $Date: 2004/07/06 11:56:24 $
CX
      SUBROUTINE C8WSOL( MXNSHL , IZ1    , NGRND  , N      ,
     &                   DENSZ  , DENS   , TBQMEP , TBQMEM ,
     &                   TBQMIP , TBQMIM , RHS
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8WSOL *********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: C8WFIL
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NGRND    = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  N        = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  DENSZ    = PLASMA ION DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  TBQMEP() = ELECTRON RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM() = ELECTRON RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP() = POSITIVE ION RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM() = POSITIVE ION RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  I/O   : (R*8)  RHS()    =
C                            DIMENSION: REFERENCED BY L+1.
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C
C          (I*4)  L       = ORBITAL QUANTUM NUMBER.
C          (I*4)  IDL     = ARRAY INDEX.
C          (I*4)  N1      = PRINCIPAL QUANTUM NUMBER.
C          (I*4)  LP      = ARRAY INDEX = L+1.
C          (I*4)  IFAIL   = RETURN FLAG FROM NAG ROUTINE.
C
C          (R*8)  VDS()   =
C          (R*8)  VDI()   =
C          (R*8)  VD()    =
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          R8ATAB     ADAS      RETURNS HYDRONIC L-RESOLVED A-VALUES.
C                               IF INPUT QUANTUM NUMBERS ARE INVALID
C                               THEN RETURNS ZERO.
C          XXFLSH     ADAS      FLUSHES UNIX PIPE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    12/10/93
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C           UNIX PORT - ROUTINE NOW USES AN IDL ROUTINE TO SOLVE 
C                       EQUATIONS RATHER THAN NAG
C
C DATE:    20/6/95
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
      REAL*8     R8ATAB
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , NGRND   , N       , IZ1
      INTEGER    L       , IDL     , N1      , LP      , IFAIL
      INTEGER    I
      INTEGER    PIPEOU  , PIPEIN
      PARAMETER (PIPEOU=6, PIPEIN=5)
C-----------------------------------------------------------------------
      REAL*8     DENSZ   , DENS
C-----------------------------------------------------------------------
      REAL*8     TBQMEP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM((MXNSHL*(MXNSHL+1))/2)  ,
     &           RHS(MXNSHL)
      REAL*8     VDS(MXN)  , VDI(MXN)  , VD(MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C ESTABLISH DIAGONAL AND OFF-DIAGONAL VECTORS OF MATRIX M FOR LEVEL N.
C-----------------------------------------------------------------------
C
      VDS(1) = 0.0D0
      VDI(1) = 0.0D0
C
      DO 1 L = 1 , N-1
         LP = L + 1
         IDL = I4IDFL( N , L )
         VDS(LP) = -DENSZ * TBQMIM(IDL) - DENS * TBQMEM(IDL)
         IDL = I4IDFL( N , L-1 )
         VDI(LP) = -DENSZ * TBQMIP(IDL) - DENS * TBQMEP(IDL)
    1 CONTINUE
C
      DO 2 L = 0 , N-1
C
         LP = L + 1
C
         IF (LP .LT. N) THEN
            VD(LP) = -VDI(LP+1) - VDS(LP)
         ELSE
            VD(LP) = - VDS(LP)
         ENDIF
C
         DO 3 N1 = NGRND , N-1
            VD(LP) = VD(LP) + R8ATAB( IZ1 , N , L , N1 , L+1 )
     &               + R8ATAB( IZ1 , N , L , N1 , L-1 )
C
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C SOLVE N LEVEL EQUATIONS USING TRI-DIGONAL NAG ROUTINE.
C-----------------------------------------------------------------------
C
CX    IFAIL = 0
CX    CALL F04EAF( N , VD , VDS , VDI , RHS , IFAIL )
CX----------------------------------------------------------------------
CX    THE ABOVE CALL IS TO A ROUTINE IN THE NAG MATHEMATICAL LIBRARY.
CX    THIS HAS NOW BEEN SUPERCEDED BY THE RELEVANT IDL ROUTINE. THE
CX    IMPORTANT VARIABLES ARE THEREFORE PIPED TO THE IDL ROUTINE 
CX    c8wsol.pro AND THE SOLUTION FED BACK INTO RHS
CX----------------------------------------------------------------------
CX    WRITE OUT THE VALUES TO BE SOLVED TO THE IDL
CX----------------------------------------------------------------------

      WRITE (PIPEOU, *) N
      CALL XXFLSH(PIPEOU)
      DO 100, I=1,N
          WRITE (PIPEOU, *) VD(I)
          CALL XXFLSH(PIPEOU)
100   CONTINUE
      DO 200, I=1,N
          WRITE (PIPEOU, *) VDS(I)
          CALL XXFLSH(PIPEOU)
200   CONTINUE
      DO 300, I=1,N
          WRITE (PIPEOU, *) VDI(I)
          CALL XXFLSH(PIPEOU)
300   CONTINUE
      DO 400, I=1,N
          WRITE (PIPEOU, *) RHS(I)
          CALL XXFLSH(PIPEOU)
400   CONTINUE
      CALL XXFLSH(PIPEOU)	

CX----------------------------------------------------------------------
CX    READ THE SOLUTION BACK IN                    
CX----------------------------------------------------------------------

      READ (PIPEIN, *)(RHS(I),I=1,N)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C8WSOL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C8WSOL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
