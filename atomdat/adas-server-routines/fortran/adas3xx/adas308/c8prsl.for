CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8prsl.for,v 1.2 2004/07/06 11:55:55 whitefor Exp $ Date $Date: 2004/07/06 11:55:55 $
CX
      SUBROUTINE C8PRSL( MXNSHL , MXPRSL , IZ0    , IZ1    ,
     &                   NPLINE , NPU    , NPL    , NUMAX  ,
     &                   WHIGH  , WLOW   , EM     , QEX    ,
     &                   TOTPOP , TOTEMI , AVRGWL , QEFF   ,
     &                   TBLPOP , TBLEMI , TBLWLN
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8PRSL *********************
C
C  PURPOSE:  FILLS TABLES FOR REQUESTED PREDICTIONS OF SPECTRUM LINES.
C
C  CALLING PROGRAM: C8EMIS
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  MXPRSL   = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                            PREDICT.
C  INPUT : (R*8)  IZ0      = NUCLEAR CHARGE.
C  INPUT : (R*8)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NPLINE   = NUMBER OF SPECTRUM LINES TO PREDICT.
C  INPUT : (I*4)  NPU()    = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS OF
C                            SPECTRUM LINES TO PREDICT.
C                            DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NPL()    = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS OF
C                            SPECTRUM LINES TO PREDICT.
C                            DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NUMAX    = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (R*8)  WHIGH()  =
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  WLOW(,)  =
C                            1ST DIMENSION: REFERENCED BY I4IDFL(N,L).
C                            2ND DIMENSION: REFERENCED BY L+1.
C  INPUT : (R*8)  EM       = EMMISION MEASURE.
C  INPUT : (R*8)  QEX()    =
C                            DIMENSION: MXNSHL.
C
C  OUTPUT: (R*8)  TOTPOP() = TOTAL COLLISION POP. FOR PREDICTED SPECTRUM
C                            LINE.
C                            UNITS: CM-2
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TOTEMI() = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS: PH CM-2 SEC-1
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  AVRGWL() = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS: A
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  QEFF()   = EFF. RATE COEFFICIENT FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS:
C                            DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TBLPOP(,)= TABLE OF COLLISION POP. FOR PREDICTED
C                            SPECTRUM LINE.
C                            UNITS: CM-2
C                            1ST DIMENSION: PREDICTED LINE INDEX.
C                            2ND DIMENSION: REFERENCED BY FUNC I4IDLI().
C  OUTPUT: (R*8)  TBLEMI(,)= TABLE OF COLLISION EMISSIVITIES FOR
C                            PREDICTED SPECTRUM LINE.
C                            UNITS: PH CM-2 SEC-1
C                            1ST DIMENSION: PREDICTED LINE INDEX.
C                            2ND DIMENSION: REFERENCED BY FUNC I4IDLI().
C  OUTPUT: (R*8)  TBLWLN(,)= TABLE OF WAVELENGTHS FOR PREDICTED SPECTRUM
C                            LINE.
C                            UNITS: A
C                            1ST DIMENSION: PREDICTED LINE INDEX.
C                            2ND DIMENSION: REFERENCED BY FUNC I4IDLI().
C
C  PARAM : (I*4)  C1       = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C2       = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C3       = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C4       = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C5       = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  RZ       = PRECISION AIR WAVELENGTH PARAM.
C
C          (I*4)  IN       = LOOP INDEX FOR SPECTRUM LINES.
C          (I*4)  N        = PRINCIPAL QUANTUM NUMBER OF INITIAL STATE.
C          (I*4)  L        = LOOP INDEX FOR ORBITAL QUANTUM NUMBER OF
C                            INITIAL STATE.
C          (I*4)  N1       = PRINCIPAL QUANTUM NUMBER OF FINAL STATE.
C          (I*4)  L1       = LOOP INDEX FOR ORBITAL QUANTUM NUMBER OF
C                            FINAL STATE.
C          (I*4)  NP       = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  IDL      = ARRAY INDEX.
C          (I*4)  ID       = ARRAY INDEX.
C
C          (R*8)  Z1       = REAL VALUE = IZ1.
C          (R*8)  T1       = COL. POP. FOR PREDICTED SPECTRUM LINE.
C                            UNITS: CM-2
C          (R*8)  T2       = COL. EMIS. FOR PREDICTED SPECTRUM LINE.
C                            UNITS: PH CM-2 SEC-1
C          (R*8)  E0       = BINDING ENERGY
C                            UNITS: RYD
C          (R*8)  E10      = BINDING ENERGY
C                            UNITS: RYD
C          (R*8)  DELTA    =
C          (R*8)  SIG2     =
C          (R*8)  RF       =
C          (R*8)  WAVAIR   = WAVELENGTH FOR PREDICTED SPECTRUM LINE.
C                            UNITS: A
C          (R*8)  SUM1     = SUM OF COL. POP. FOR PREDICTED LINE.
C                            UNITS: CM-2
C          (R*8)  SUM2     = SUM OF COL. EMIS. FOR PREDICTED LINE.
C                            UNITS: PH CM-2 SEC-1
C          (R*8)  SUM3     = SUM OF WAVELENGTHS FOR PREDICTED LINE.
C                            UNITS: A
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          I4IDLI     ADAS      RETURNS INDEX FOR PREDICTED SPECTRUM
C                               LINE TABLES.
C          R8ATAB     ADAS      RETURNS HYDRONIC L-RESOLVED A-VALUES.
C                               IF INPUT QUANTUM NUMBERS ARE INVALID
C                               THEN RETURNS ZERO.
C          R8CONST    ADAS      RETURNS FUNDAMENTAL ATOMIC CONSTANTS.
C          
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    14/10/93
C
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C           ADDED ERROR MESSAGE FOR NUMERICAL ERRORS INTRODUCED
C           BY BAD VALUES FOR THE OBSERVED SPECTRUM LINES
C           ALSO GENERAL UNIX PORT.
C
C DATE:     20/6/95
C
C VERSION: 1.2
C MODIFIED: Martin O'Mullane
C           - Corrected the vacuum to air conversion. Parameter C4 of the
C             expression for the refractive index of air should be 146.0 
C             and not 176.0. (see Astrophysical Quantities section 54)
C           - Introduced R8CONST to return fundamental atomic constants.
C             The mass dependent rydberg constant is returned if set
C             by XXRAMS in the main program. As a consequence RZ has
C             been removed from the parameter statement.
C
C DATE:     08/04/99
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4IDFL  , I4IDLI, I4UNIT
      REAL*8     R8ATAB
C-----------------------------------------------------------------------
      REAL*8     C1                , C2                ,
     &           C3                , C4                ,
     &           C5                
      PARAMETER( C1 = 6432.8D0     , C2 = 2949810.0D0  ,
     &           C3 = 25540.0D0    , C4 = 146.0D0      ,
     &           C5 = 41.0D0       )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXPRSL  , IZ0     , IZ1     , NPLINE  ,
     &           NUMAX
      INTEGER    IN      , N       , L       , N1      , L1      ,
     &           NP      , IDL     , ID
C-----------------------------------------------------------------------
      REAL*8     EM      , RZ      , R8CONST
      REAL*8     Z1      , T1      , T2      , E0      , E10     ,
     &           DELTA   , SIG2    , RF      , WAVAIR  , SUM1    ,
     &           SUM2    , SUM3
C-----------------------------------------------------------------------
      INTEGER    NPU(MXPRSL)  , NPL(MXPRSL)
C-----------------------------------------------------------------------
      REAL*8     WHIGH((MXNSHL*(MXNSHL+1))/2)  ,
     &           QEX(MXNSHL)                   ,
     &           TOTPOP(MXPRSL)                ,
     &           TOTEMI(MXPRSL)                ,
     &           AVRGWL(MXPRSL)                ,
     &           QEFF(MXPRSL)
      REAL*8     WLOW((MXNSHL*(MXNSHL+1))/2,MXNSHL) ,
     &           TBLPOP(MXPRSL,2*MXNSHL-3)          ,
     &           TBLEMI(MXPRSL,2*MXNSHL-3)          ,
     &           TBLWLN(MXPRSL,2*MXNSHL-3)
C-----------------------------------------------------------------------

      Z1 = DFLOAT( IZ1 )
      
C Get the mass corrected Rydberg constant

      RZ = R8CONST('RYR')
             
C-----------------------------------------------------------------------

      DO 1 IN = 1 , NPLINE

         N  = NPU(IN)
         N1 = NPL(IN)

         SUM1 = 0.0D0
         SUM2 = 0.0D0
         SUM3 = 0.0D0

C-----------------------------------------------------------------------

         DO 2 L = 0 , N-1

            IDL = I4IDFL( N , L )

            IF (N .GT. NUMAX) THEN

               T1 = WHIGH(IDL) * EM * QEX(NUMAX)

            ELSE

               T1 = 0.0D0
               DO 3 NP = N , NUMAX
                  T1 = T1 + WLOW(IDL,NP) * EM * QEX(NP)
    3          CONTINUE
 
            ENDIF
 
            SUM1 = SUM1 + T1
 
            DO 4 L1 = L-1 , L+1 , 2
 
               T2 = R8ATAB( IZ1 , N , L , N1 , L1 ) * T1
 
               IF (T2 .GT. 0.0D0) THEN
 
                  CALL CXHYDE( IZ0 , Z1 , N , L , E0 )
                  CALL CXHYDE( IZ0 , Z1 , N1 , L1 , E10 )
                  DELTA = DABS( RZ * ( E0 - E10 ) )
                  SIG2 = DELTA**2 * 1.0D-8
                  RF = 1.0D0 + 1.0D-8 * ( C1 + ( C2 / ( C4 - SIG2 ) )
     &                 + ( C3 / ( C5 - SIG2 ) ) )
                  WAVAIR = 1.0D8 / ( DELTA * RF )
 
                  ID = I4IDLI( N1 , L , L1 )
                  TBLPOP(IN,ID) = T1
                  TBLEMI(IN,ID) = T2
                  TBLWLN(IN,ID) = WAVAIR
 
                  SUM2 = SUM2 + T2
                  SUM3 = SUM3 + T2 * WAVAIR
 
               ENDIF
 
    4       CONTINUE
    2    CONTINUE

C-----------------------------------------------------------------------

         TOTPOP(IN) = SUM1
         TOTEMI(IN) = SUM2
         IF ( (SUM2.EQ.0.0D0).OR.(EM.EQ.0.0D0) ) THEN
             WRITE(I4UNIT(-1),1000)
             STOP
         ENDIF
         AVRGWL(IN) = SUM3 / SUM2
         QEFF(IN)   = SUM2 / EM

C-----------------------------------------------------------------------

    1 CONTINUE


C-----------------------------------------------------------------------
1000  FORMAT( 1X, 32('*'), ' C8PRSL ERROR ', 32('*') //
     &        20X, 'ZERO DENOMINATOR IN CALCULATIONS.'//
     &        8X, 'CHECK OBSERVED SPECTRUM LINES DATA ON ',
     &        'PROCESSING OPTIONS SCREEN.'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C-----------------------------------------------------------------------
      RETURN
      
      END
