CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8ispf.for,v 1.1 2004/07/06 11:55:09 whitefor Exp $ Date $Date: 2004/07/06 11:55:09 $
CX
      SUBROUTINE C8ISPF( MXBEAM , MXOBSL , MXPRSL , MXTAB  ,
     &                   MXGRF  , NGFPLN , NGRND  , NTOT   ,
     &                   IPAN   , LPEND  , TITLE  , RAMSNO ,
     &                   TIEV   , TEV    , DENSZ  , DENS   ,
     &                   ZEFF   , BMAG   , NBEAM  , BMFRA  ,
     &                   BMENA  , ITHEOR , IBSTAT , IEMMS  ,
     &                   NOLINE , NU     , NL     , EMISA  ,
     &                   NPLINE , NPU    , NPL    , NTAB   ,
     &                   NGRF   , IDTAB  , IDGRF  , LRTTB  ,
     &                   LPLT1  , LGRD1  , LDEF1  , XLG    ,
     &                   XUG    , YLG    , YUG
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8ISPF *********************
C
C  PURPOSE: PIPE COMMS WITH IDL FOR THE PROCESSING OPTIONS OF 308.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)   MXBEAM   = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  INPUT : (I*4)   MXOBSL   = MAXIMUM NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)   MXPRSL   = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  INPUT : (I*4)   MXTAB    = MAXIMUM NUMBER OF TABLES FOR OUTPUT.
C  INPUT : (I*4)   MXGRF    = MAXIMUM NUMBER OF GRAPHS FOR OUTPUT.
C  INPUT : (I*4)   NRTABS   = NUMBER OF INDIVIDUAL RATE TABLES.
C  INPUT : (I*4)   NGRND    = MINIMUM ALLOWED VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)   NTOT     = MAXIMUM ALLOWED VALUE OF N QUANTUM NUMBER.
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (R*8)   RAMSNO   = RECEIVER ATOMIC MASS.
C  OUTPUT: (R*8)   TIEV     = ION TEMPERATURE.
C                             UNITS: EV
C  OUTPUT: (R*8)   TEV      = ELECTRON TEMPERATURE.
C                             UNITS: EV
C  OUTPUT: (R*8)   DENSZ    = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  OUTPUT: (R*8)   DENS     = ELECTRON DENSITY.
C                             UNITS: CM-3
C  OUTPUT: (R*8)   ZEFF     = EFFECTIVE ION CHARGE.
C  OUTPUT: (R*8)   BMAG     = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C  OUTPUT: (I*4)   NBEAM    = NUMBER OF BEAM ENERGIES.
C  OUTPUT: (R*8)   BMFRA()  = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  OUTPUT: (R*8)   BMENA()  = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  OUTPUT: (I*4)   ITHEOR   = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  OUTPUT: (I*4)   IBSTAT   = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  OUTPUT: (I*4)   IEMMS    = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C  OUTPUT: (I*4)   NOLINE   = NUMBER OF OBSERVED SPECTRUM LINES.
C  OUTPUT: (I*4)   NU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (R*8)   EMISA()  = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NPLINE   = NUMBER OF SPECTRUM LINES TO PREDICT.
C  OUTPUT: (I*4)   NPU()    = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NPL()    = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NTAB     = NUMBER OF TABLES FOR OUTPUT.
C  OUTPUT: (I*4)   NGRF     = NUMBER OF GRAPHS FOR OUTPUT.
C  OUTPUT: (I*4)   IDTAB()  = LIST OF INDEXES OF TABLES FOR OUTPUT.
C                             DIMENSION: MXTAB
C  OUTPUT: (I*4)   IDGRF()  = LIST OF INDEXES OF GRAPHS FOR OUTPUT.
C                             DIMENSION: MXGRF
C  OUTPUT: (L*4)   LRTTB    = FLAG FOR RATE TABLE PRINTING.
C                             .TRUE.  = PRINT RATE TABLES.
C                             .FALSE. = DO NOT PRINT RATE TABLES.
C  OUTPUT: (L*4)   LPLT1    = FLAGS WHETHER TO PLOT GRAPHS ON SCREEN.
C                             .TRUE.  => PLOT GRAPHS ON SCREEN.
C                             .FALSE. => DO NOT PLOT GRAPHS ON SCREEN.
C  OUTPUT: (L*4)   LGRD1    = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
CX UNIX PORT - LGRD1 ONLY USED TO KEEP ARGUMENT LIST THE SAME
C  OUTPUT: (L*4)   LDEF1    = FLAGS DEFAULT GRAPH SCALING
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
CX UNIX PORT - AXES SCALING CHOSEN LATER UNDER IDL-ADAS
CX OUTPUT: (R*8)   XLG()    = LOWER LIMIT FOR X-AXIS OF GRAPHS.
CX                            DIMENSION: NGFPLN
CX OUTPUT: (R*8)   XUG()    = UPPER LIMIT FOR X-AXIS OF GRAPHS.
CX                            DIMENSION: NGFPLN
CX OUTPUT: (R*8)   YLG()    = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
CX                            DIMENSION: NGFPLN
CX OUTPUT: (R*8)   YUG()    = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
CX                            DIMENSION: NGFPLN
C
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   LOGIC  - USED TO PIPE LOGICAL VALUES
C          (L*4)   LPRED    = FLAGS CHOICE FOR PREDICTION OF SPECTRUM
C                             LINES.
C                             .TRUE.  => PREDICT SPECTRUM LINES.
C                             .FALSE. => DO NOT PREDICT SPECTRUM LINES.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL_ADAS  calls 'flush' to clear the pipe.

C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    18/11/93
C
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C           UNIX PORT
C
C DATE:    20/6/95
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXBEAM  , MXOBSL  , MXPRSL  , MXTAB   , MXGRF   ,
     &           NGRND   , NTOT    , IPAN    , NBEAM   , ITHEOR  ,
     &           IBSTAT  , IEMMS   , NOLINE  , NPLINE  , NTAB    ,
     &           NGRF    , NRTABS  , NGFPLN
      INTEGER    ILEN    , IABT    , IPANRC
      INTEGER    I
      INTEGER    PIPEOU  , PIPEIN  , LOGIC
      PARAMETER (PIPEOU=6, PIPEIN=5)
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TIEV    , TEV     , DENSZ   , DENS    ,
     &           ZEFF    , BMAG
C-----------------------------------------------------------------------
      LOGICAL    LPEND   , LRTTB   , LPLT1   , LGRD1   , LDEF1
      LOGICAL    LPRED
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
      CHARACTER  P*2       , PLAST*2   , KEY*4     , CURPOS*8  ,
     &           MSGTXT*8  , PNAME*8
C-----------------------------------------------------------------------
      INTEGER    NU(MXOBSL)     , NL(MXOBSL)     , NPU(MXPRSL)    ,
     &           NPL(MXPRSL)    , IDTAB(MXTAB)   , IDGRF(MXGRF)
C-----------------------------------------------------------------------
      REAL*8     BMFRA(MXBEAM)  , BMENA(MXBEAM)  , EMISA(MXOBSL)  ,
     &           XLG(NGFPLN)    , XUG(NGFPLN)    , YLG(NGFPLN)    ,
     &           YUG(NGFPLN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C    READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------

      READ( PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
      READ( PIPEIN, '(A)') TITLE
      READ( PIPEIN, *) RAMSNO
      READ( PIPEIN, *) TIEV
      READ( PIPEIN, *) TEV
      READ( PIPEIN, *) DENSZ
      READ( PIPEIN, *) DENS
      READ( PIPEIN, *) ZEFF
      READ( PIPEIN, *) BMAG
      READ( PIPEIN, *) ITHEOR
      READ( PIPEIN, *) IBSTAT
      READ( PIPEIN, *) IEMMS
      READ( PIPEIN, *) NBEAM
      DO 1, I=1,NBEAM
          READ( PIPEIN, *) BMFRA(I)
          READ( PIPEIN, *) BMENA(I)
1     CONTINUE
      READ( PIPEIN, *) NOLINE
      DO 2, I=1,NOLINE
          READ( PIPEIN, *) NU(I)
          READ( PIPEIN, *) NL(I)
          READ( PIPEIN, *) EMISA(I)
2     CONTINUE
      READ( PIPEIN, *) NPLINE
      DO 3, I=1,NPLINE
          READ( PIPEIN, *) NPU(I)
          READ( PIPEIN, *) NPL(I)
3     CONTINUE
      READ(PIPEIN, *) LOGIC
      IF (LOGIC .EQ. 1) THEN
          LRTTB = .TRUE.
      ELSE
          LRTTB = .FALSE.
      ENDIF

      READ( PIPEIN, *) NGRF
      IF (NGRF.GT.0) THEN 
          DO 4, I=1,NGRF
4             READ( PIPEIN, *) IDGRF(I)
      ENDIF

      READ( PIPEIN, *) NTAB
      IF (NTAB.GT.0) THEN
          DO 5, I=1,NTAB
5             READ( PIPEIN, *) IDTAB(I)
      ENDIF

C-----------------------------------------------------------------------
      RETURN
      END
