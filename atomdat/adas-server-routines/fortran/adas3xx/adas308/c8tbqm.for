CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas308/c8tbqm.for,v 1.1 2004/07/06 11:56:18 whitefor Exp $ Date $Date: 2004/07/06 11:56:18 $
CX
      SUBROUTINE C8TBQM( MXNSHL , IZ0    , IZ1    , NBOT   ,
     &                   NTOP   , TEV    , DENS   , ZP     ,
     &                   TPV    , EMP    , TBLF   , TBQMEP ,
     &                   TBQMEM , TBQMIP , TBQMIM
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C8TBQM *********************
C
C  PURPOSE:  SETS UP TABLES OF ELECTRON AND POSITIVE ION IMPACT RATE
C            COEFFICIENTS BETWEEN NEARLY DEGENERATE L STATES OF THE
C            SAME N FOR HYDROGENIC IONS.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE OF TARGET ION.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  TEV      = ELECTRON TEMPERATURE.
C                            UNITS: EV
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  ZP       = CHARGE OF COLLIDING POSITIVE ION.
C  INPUT : (R*8)  TPV      = TEMPERATURE (COLLIDING POSITIVE ION
C                            DISTRIBUTION).
C                            UNITS: EV
C  INPUT : (R*8)  EMP      = REDUCED MASS FOR COLLIDING POSITIVE ION.
C                            UNITS: ELECTRON MASSES
C  INPUT : (R*8)  ZP       = CHARGE OF COLLIDING POSITIVE ION.
C  INPUT : (R*8)  TPV      = POSITIVE ION TEMPERATURE.
C                            UNITS: EV
C  INPUT : (R*8)  EMP      = REDUCED MASS FOR COLLIDING POSITIVE ION.
C                            UNITS: ELECTRON MASSES
C  INPUT : (R*8)  TBLF()   = TABLE OF RADIATIVE LIFETIMES.
C                            UNITS: SECS
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (R*8)  TBQMEP() = ELECTRON RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  OUTPUT: (R*8)  TBQMEM() = ELECTRON RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  OUTPUT: (R*8)  TBQMIP() = POSITIVE ION RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  OUTPUT: (R*8)  TBQMIM() = POSITIVE ION RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  PARAM : (I*4)  MXJ     = MAXIMUM NUMBER OF J SUB-SHELLS.
C  PARAM : (R*8)  P1      =
C  PARAM : (R*8)  P2      =
C
C          (I*4)  NI      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE I.
C          (I*4)  NJ      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE J.
C          (I*4)  LI      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE I.
C          (I*4)  LJ      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE J.
C          (I*4)  IDLI    = TABLE INDEX.
C          (I*4)  IDLJ    = TABLE INDEX.
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  FACE    =
C          (R*8)  FACI    =
C          (R*8)  WI      = STATISTICAL WEIGHT OF STATE I.
C          (R*8)  WJ      = STATISTICAL WEIGHT OF STATE J.
C          (R*8)  GAE     = GAMA RATE PARAMETER FOR ELECTRON COLLISIONS.
C          (R*8)  GAP     = GAMA RATE PARAMETER FOR POSITIVE ION
C                           COLLISIONS.
C          (R*8)  QEP()   = ELECTRON RATE COEFFT. FOR NLJ->NL+1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  QEM()   = ELECTRON RATE COEFFT. FOR NLJ->NL-1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  QIP()   = POSITIVE ION RATE COEFFT. FOR NLJ->NL+1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  QIM()   = POSITIVE ION RATE COEFFT. FOR NLJ->NL-1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C       2) BEFORE CALLING C8TBQM THE LIFETIME TABLE MUST BE FILLED
C          WITH A CALL TO CXTBLF.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXCRDG     ADAS      CALCULATES COLLISIONAL RATE COEFFICIENTS
C                               BETWEEN NEARLY DEGENERATE LEVELS OF
C                               H-, LI- OR NA-LIKE IONS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    12/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXJ
      PARAMETER( MXJ = 2 )
C-----------------------------------------------------------------------
      REAL*8     P1               , P2
      PARAMETER( P1 = 2.17161D-8  , P2 = 13.6048D0  )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , IZ0     , IZ1     , NTOP    , NBOT
      INTEGER    NI      , NJ      , LI      , LJ      , IDLI    ,
     &           IDLJ    , I
C-----------------------------------------------------------------------
      REAL*8     TEV     , DENS    , ZP      , TPV     , EMP
      REAL*8     FACE    , FACI    , WI      , WJ      , GAE     ,
     &           GAP
C-----------------------------------------------------------------------
      REAL*8     TBLF((MXNSHL*(MXNSHL+1))/2)    ,
     &           TBQMEP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM((MXNSHL*(MXNSHL+1))/2)
      REAL*8     QEP(2*MXJ)  , QEM(2*MXJ)  , QIP(2*MXJ)  ,
     &           QIM(2*MXJ)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CLEAR OUTPUT TABLES BELOW 'NBOT'.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , I4IDFL( NBOT , 0 ) - 1
         TBQMEP(I) = 0.0D0
         TBQMEM(I) = 0.0D0
         TBQMIP(I) = 0.0D0
         TBQMIM(I) = 0.0D0
    1 CONTINUE
C
      FACE = P1 * DSQRT( P2 / TEV )
      FACI = P1 * DSQRT( P2 / TPV )
C
C-----------------------------------------------------------------------
C FILL OUTPUT TABLES BETWEEN 'NBOT' AND 'NTOP'.
C-----------------------------------------------------------------------
C
      DO 2 NI = NBOT , NTOP
C
         NJ = NI
C
         IDLI = I4IDFL( NI, NI-1 )
         TBQMEP(IDLI) = 0.0D0
         TBQMIP(IDLI) = 0.0D0
C
         IDLJ = I4IDFL( NI, 0 )
         TBQMEM(IDLJ) = 0.0D0
         TBQMIM(IDLJ) = 0.0D0
C
         DO 3 LI = 0 , NI-2
C
            LJ = LI + 1
            WI = DFLOAT( 4 * LI + 2 )
            WJ = DFLOAT( 4 * LJ + 2 )
C
            CALL CXCRDG( MXNSHL , MXJ    , IZ0    , IZ1    , NI     ,
     &                   LI     , NJ     , LJ     , TEV    , DENS   ,
     &                   ZP     , TPV    , EMP    , TBLF   , GAE    ,
     &                   GAP    , QEP    , QEM    , QIP    , QIM      )
C
            IDLI = I4IDFL( NI , LI )
            IDLJ = I4IDFL( NJ , LJ )
            TBQMEP(IDLI) = FACE * GAE / WI
            TBQMEM(IDLJ) = FACE * GAE / WJ
            TBQMIP(IDLI) = FACI * GAP / WI
            TBQMIM(IDLJ) = FACI * GAP / WJ
C
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
