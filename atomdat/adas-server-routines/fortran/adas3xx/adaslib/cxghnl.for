CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxghnl.for,v 1.2 2007/04/11 11:24:03 allan Exp $ Date $Date: 2007/04/11 11:24:03 $
CX
      SUBROUTINE CXGHNL ( MXNSHL , IZ1    , IZEFF  , N      ,
     &                    L      , N1     , TEV    , GAMA
     &                  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXGHNL *********************
C
C  PURPOSE:  CALCULATES APPROXIMATE EXCITATION RATE PARAMETERS FROM
C            N,L LEVELS OF HYDROGEN-LIKE AND LITHIUM-LIKE IONS TO HIGHER
C            LEVELS N1,L1 USING CLASSICAL OVERLAPS.
C
C            FOR TRANSITIONS FROM 1S,2S AND 2P APPROXIMATE FITTINGS ARE
C            USED BASED ON SAMPSON DATA.
C
C            FOR TRANSITIONS FROM HIGHER NL LEVELS UPWARDS A RESOLVED
C            VARIANT OF PERCIVAL-RICHARDS IS USED, WITH NUMERICAL
C            QUADRATURES.
C
C  CALLING PROGRAM: ADAS308.
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = ION CHARGE.OF ION
C  INPUT : (R*8)  IZEFF    = EFFECTIVE ION CHARGE (CF. SAMPSON ET AL.).
C  INPUT : (I*4)  N        = LOWER VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  L        = L QUANTUM NUMBER FOR NL.
C  INPUT : (I*4)  N1       = UPPER VALUE OF N QUANTUM NUMBER.
C  INPUT : (R*8)  TEV      = ELECTRON TEMPERATURE.
C                            UNITS: EV
C
C  OUTPUT: (R*8)  GAMA()   = TABLE OF RATE PARAMETERS.
C                            UNITS:
C                            DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C
C  PARAM : (I*4)  MXN      = 'MXNSHL'.
C  PARAM : (I*4)  NMAX     =
C  PARAM : (R*8)  P1       =
C  PARAM : (R*8)  P2       =
C  PARAM : (R*8)  C1       = 2/3.
C  PARAM : (R*8)  C2       = 4/3.
C
C          (I*4)  L1       = L QUANTUM NUMBER FOR N1.
C          (I*4)  K        =
C          (I*4)  I        = LOOP INDEX.
C          (I*4)  J        = LOOP INDEX.
C
C          (R*8)  Z1       = REAL VALUE = IZ1.
C          (R*8)  ZEFF     = REAL VALUE = IZEFF.
C          (R*8)  Z12      = Z1**2.
C          (R*8)  XN       = REAL VALUE = N.
C          (R*8)  XN1      = REAL VALUE = N1.
C          (R*8)  XND      = REAL VALUE = N1-N.
C          (R*8)  XNP      = REAL VALUE = N1*N.
C          (R*8)  DE       =
C          (R*8)  ATE      =
C          (R*8)  Z2OMT    =
C          (R*8)  SUM      = SUM OF 'FOMA' TERMS.
C          (R*8)  W1       =
C          (R*8)  OVL      =
C          (R*8)  XLN      =
C          (R*8)  XLG      =
C          (R*8)  Z2S      =
C          (R*8)  GAM      =
C          (R*8)  XL       =
C          (R*8)  XM       =
C          (R*8)  XP       =
C          (R*8)  CM       =
C          (R*8)  CP       =
C          (R*8)  D        =
C          (R*8)  E        =
C          (R*8)  F        =
C          (R*8)  G        =
C          (R*8)  H        =
C          (R*8)  T        =
C          (R*8)  Y        =
C
C          (R*8)  XA()     =
C          (R*8)  WA()     =
C          (R*8)  FOMA()   =
C
C          (R*8)  FA(,)    =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT MESSAGES.
C          CXOVLP     ADAS
C          R8RD2B     ADAS      RETURNS HYDRONIC BOUND-BOUND RADIAL
C                               INTEGRALS.
C          R8FEEI     ADAS
C
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     05/10/93
C
C VERSION  : 1.2                          
C DATE     : 11-04-2007
C MODIFIED : Allan Whiteford
C               - Renamed R8OVLP to CXOVLP.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      REAL*8     CXOVLP  , R8RD2B  , R8FEEI
C-----------------------------------------------------------------------
      INTEGER    MXN       , NMAX
      PARAMETER( MXN = 20  , NMAX = 15 )
C-----------------------------------------------------------------------
      REAL*8     C1                  , C2                  ,
     &           P1
      PARAMETER( C1 = 2.0D0 / 3.0D0  , C2 = 4.0D0 / 3.0D0  ,
     &           P1 = 13.6049D0                              )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , IZ1     , IZEFF   , N       , L       ,
     &           N1
      INTEGER    L1      , K       , I       , J
C-----------------------------------------------------------------------
      REAL*8     TEV
      REAL*8     Z1      , ZEFF    , Z12     , XN      , XN1     ,
     &           XND     , XNP     , DE      , ATE     , Z2OMT   ,
     &           SUM     , W1      , OVL     , XLN     , XLG     ,
     &           Z2S     , GAM     , XL      , XM      , XP      ,
     &           CM      , CP      , D       , E       , F       ,
     &           G       , H       , T       , Y
C-----------------------------------------------------------------------
      REAL*8     GAMA(MXNSHL)
      REAL*8     XA(4)         , WA(4)         , FOMA(MXN)
      REAL*8     FA(4,3)
C-----------------------------------------------------------------------
      DATA       FA / 2.2D-1 , 1.0D0  , 1.1D-1 , 1.0D-1 , 8.3D-1 ,
     &                1.0D0  , 4.6D-1 , 5.0D-1 , 1.0D0  , 8.2D-1 ,
     &                1.0D0  , 1.0D0                               /
       DATA      XA / 3.225477D-1 , 1.745761D0  , 4.536620D0  ,
     &                9.395071D0                                /
       DATA      WA / 6.031541D-1 , 3.574187D-1 , 3.888791D-2 ,
     &                5.392947D-4                               /
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C CHECK INPUT VALUES VALID.
C-----------------------------------------------------------------------
      IF (MXNSHL .GT. MXN) THEN
         WRITE(I4UNIT(-1),1000) MXN
         STOP
      ENDIF
C
      IF ((N .LT. 1)  .OR. (L .LT .0) .OR. (L .GE. N) .OR.
     &    (N1 .LT. 2) .OR. (N .GE .N1)                     ) THEN
         WRITE(I4UNIT(-1),1001) N , L , N1
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      Z1   = DFLOAT( IZ1 )
      ZEFF = DFLOAT( IZEFF )
      Z12 = Z1**2
      XN  = DFLOAT( N )
      XN1 = DFLOAT( N1 )
      XND = XN1 - XN
      XNP = XN * XN1
      XLN = 2.0D0 * ( 2.0D0 * DFLOAT( L ) + 1.0D0 ) * XN**4
      DE  = Z12 * ( 1.0D0 / XN**2 - 1.0D0 / XN1**2 )
      ATE = P1 / TEV
C
C-----------------------------------------------------------------------
C
      IF (N .EQ. 1) THEN
         Z2OMT = 2.186D1 / ( XN1**3 * ( 1.0D0 - 1.0D0 / XN1**2 ) )
         K = 1
      ELSE IF ((N .EQ. 2) .AND. (L .EQ. 0)) THEN
         Z2OMT = 3.846D2
     &           / ( XN1**3 * ( 1.0D0 - 4.0D0 / XN1**2 )**1.5D0 )
         K = 2
      ELSE IF ((N .EQ. 2) .AND. (L .EQ. 1)) THEN
         Z2OMT = 1.112D3
     &           / ( XN1**3 * ( 1.0D0 - 4.0D0 / XN1**2 )**2.0D0 )
         K = 3
      ELSE
         Z2OMT = 0.0D0
         K = 4
      ENDIF
C
C-----------------------------------------------------------------------
C
      SUM = 0.0D0
      DO 1 J = 1 , N1
         L1 = J - 1
         W1 = DFLOAT( 2 * L1 + 1 )
         OVL = CXOVLP( MXNSHL , NMAX , N , L , N1 , L1 )
         IF (L1 .LE. 2) THEN
            FOMA(J) = FA(K,J) * ( W1 * OVL )**0.723D0
         ELSE
            FOMA(J) = ( W1 * OVL )**0.723D0
         ENDIF
         SUM = SUM + FOMA(J)
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      DO 2 J = 1 , N1
C
         L1 = J - 1
         XLG = DFLOAT( MAX0( L , L1 ) )
         FOMA(J) = FOMA(J) / SUM
C
         IF (K .NE. 4) THEN
C
            IF (IABS( L - L1 ) .NE. 1) THEN
C
C-----------------------------------------------------------------------
C NON-DIPOLE TRANSITION.
C-----------------------------------------------------------------------
C
               GAMA(J) = FOMA(J) * Z2OMT / ZEFF**2
C
C-----------------------------------------------------------------------
C
            ELSE
C
C-----------------------------------------------------------------------
C  DIPOLE TRANSITION.
C-----------------------------------------------------------------------
C
               Z2S = 2.0D0 * XLG * R8RD2B( N1 , L1 , N , L )
               F   = DEXP( 0.75D0 * FOMA(J) * Z2OMT / Z2S ) - 1.0D0
               GAMA(J) = C2 * Z2S * ( DLOG( 1.00D0 + F )
     &                   + R8FEEI( ( 1.00D0 + F ) * DE * ATE ) )
     &                   / ZEFF**2
C
C-----------------------------------------------------------------------
C
            ENDIF
C
         ELSE
C
C-----------------------------------------------------------------------
C PERCIVAL-RICHARDS.
C-----------------------------------------------------------------------
C
            GAM = 0.0D0
            DO 3 I = 1 , 4
               E = XA(I) / ATE + DE
C
               D = DEXP( -Z12 / ( XNP * E**2 ) )
               IF ((N .LT. 5) .OR. (XND .GT. 10.0D0)) THEN
                  F = 1.0D0 - D
                  Y = 1.0D0
               ELSE
                  F = (1.0D0 - 0.3D0 * XND * D / XNP )
     &                **( 1.0D0 + 2.0D0 * XND )
                  Y = 1.0D0 / ( 1.0D0 - D * DLOG( 18.0D0 * XND )
     &                / ( 4.0D0 * XND ) )
               ENDIF
C
               XL = DLOG( ( 1.0D0 + 0.53D0 * E**2 * XNP / Z12 )
     &                    / ( 1.0D0 + 0.4D0 * E ) )
               G = 0.5D0 * ( E * XN**2 / ( Z1 * XN1 ) )**3
               T = DSQRT( 2.0D0 - ( XN / XN1 )**2 )
               XP = 2.0D0 * Z1 / ( E * XN**2 * ( T + 1.0D0 ) )
               XM = 2.0D0 * Z1 / ( E * XN**2 * ( T - 1.0D0 ) )
               CP = ( XP**2 / ( 2.0D0 * Y + 1.5D0 * XP ) )
     &              * DLOG( 1.0D0 + C1 * XP )
               CM = ( XM**2 / ( 2.0D0 * Y + 1.5D0 * XM ) )
     &              * DLOG( 1.0D0 + C1 * XM )
               H = CM - CP
C
               IF (IABS(L - L1) .NE. 1) THEN
                  Z2S = 2.0D0 * XLG * R8RD2B( N1 , L1 , N , L )
                  GAM = GAM + WA(I) * ( FOMA(J) * XLN * F * G * H
     &                  + C2 * Z2S * D * XL )
               ELSE
                  GAM = GAM + WA(I) * ( FOMA(J) * XLN * G * H )
               ENDIF
C
    3       CONTINUE
C
            GAMA(J) = GAM / Z12
C
C-----------------------------------------------------------------------
C
         ENDIF
C
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXGHNL ERROR ', 32('*') //
     &        2X, 'VALUE OF ''MXNSHL'' GREATER THAN ', I2, '.' /
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE CXGHNL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' CXGHNL ERROR ', 32('*') //
     &        2X, 'INVALID QUANTUM NUMBERS INPUT.' /
     &        2X, 'N= ', I2, '   L= ', I2 , '   N1= ', I2   //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
