      subroutine cxsqef( iunit  , dsname , ibsel  ,
     &                   nqeff  , epro   , ttar   ,
     &                   em1    , em2    , iord   ,
     &                   ti     , densi  , zeff   , bmag   ,
     &                   nener  , ener   , qener  ,
     &                   csymb  , czion  , cwavel , cdonor , crecvr  ,
     &                   ctrans , cfile  , ctype  , cindm  ,
     &                   qeff   , ircode
     &                 )
      implicit none
C-----------------------------------------------------------------------
C
C ********************* FORTRAN77 SUBROUTINE : CXSQEF ******************
C
C  PURPOSE:  Subroutine to evaluate Maxwell averaged effective rate
C            coefficients for charge exchange/Stark studies.
C
C            The source data is effective coefficients in the
C            collisional/radiative sense or effective emission
C            coefficients for photon emission but before averaging
C            over ion/atom speed distribution functions.
C
C            The function also returns the raw eff. coefft. data for
C            verification and graphing purposes.
C
C            The effective rate coefficient appropriate to one of
C            the particles being in a monoenergetic beam and the other
C            belonging to a Maxwell distribution may be returned.
C            The target and projectile roles may be reversed. Arbitrary
C            relative speeds are allowed.
C
C  SUBROUTINE:
C
C  input : (i*4) iunit   = unit number on which ionatom file is opened
C  input : (c)   dsname  = full name of data set to be opened and read
C  input : (i*4) ibsel   = selector for particular rate coefft.
C
C  input : (i*4) nqeff   = number of rates to be evaluated (when ttar>0)
C                          a 1d array of plasma/beam conditions are
C                          evaluated to give a vector of rates. at
C                          the moment, epro, ttar, ti, densi, zeff &
C                          bmag are allowed to vary along the vector.
C  input : (r*8) epro    = incident particle energy (ev/amu)
C  input : (r*8) ttar    = maxwell temperature of target particles (ev)
C                          if (ttar.le.0) then rates for t=0 are
C                          returned
C  input : (r*8) em1     = atomic mass number of first particle
C  input : (r*8) em2     = atomic mass number of second particle
C  input : (i*4) iord    = 1 for 1st particle incident and monoenergetic
C                        = 2 for 2nd particle incident and monoenergetic
C  input : (r*8) ti      = plasma ion temperature (ev)
C  input : (r*8) densi   = plasma ion density (cm-3)
C  input : (r*8) zeff    = plasma z effective
C  input : (r*8) bmag    = plasma magnetic field (tesla)
C
C  output: (r*8) qeff    = rate coefficient (cm3 sec-1)
C  output: (i*4) nener   = number of source data values
C  output: (r*8) ener(i) = set of energies (ev/amu) for
C                          selected source data
C  output: (r*8) qener(i)= rate coeffts.(cm**3 sec-1) for
C                          selected source data
C  output: (c*2) csymb   = element symbol
C  output: (c*3) czion   = emitting ion charge
C  output: (c*8) cwavel  = wavelength (A)
C  output: (c*6) cdonor  = donor neutral atom
C  output: (c*5) crecvr  = receiver nucleus
C  output: (c*7) ctrans  = transition
C  output: (c*10)cfile   = specific ion file source
C  output: (c*2) ctype   = type of emissivity
C  output: (c*3) cindm   = emissivity index
C  output: (i*4) ircode  = return code from subroutine:
C                          0 => normal completion - no error detected
C                          1 => error opening requested data set
C                               exist - data set not connected
C                          3 => the selected data-block 'ibsel' is out
C                               of range or does not exist.
C
C
C  ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           xxdata_12   ADAS     reads values from 'ionatom' dataset
C           c3corr      ADAS     calculates scaled plasma parameter
C           c3alrs      ADAS     calculates rate coefficient
C
C  AUTHOR:  C. J. WHITEHEAD,  UNIVERSITY OF STRATHCLYDE
C
C  DATE:    25/11/94
C
C  UPDATE:  19/12/94  HP SUMMERS - TIDIED UP FORMATTING
C           03/01/95  HP SUMMERS - CORRECTED THERMAL AVERAGED RATE
C                                  COEFFICIENT BY INTRODUCING OAA ARRAY
C
C  UPDATE:  11/01/95  PE BRIDEN  - CHANGED DSNAME FROM C*30 TO C*44
C                                  TO AGREE WITH THAT IN C2FILE.
C                                - INITIALISE NBSEL AS ZERO.
C
C  UPDATE:  03/05/95  PE BRIDEN  - C3DATA CHANGED TO C3DATAO AS CURRENT
C                                  VERSION OF SQEF NEEDS TTO BE UPDATED
C                                  TO USE THE NEW VERSION OF C3DATA.
C
C  UPDATE:  15/05/95  Tim Hammond - UNIX PORT
C                                   Put under SCCS control
C----------------------------------------------------------------------
C
C  Copied from ...adas3xx/adas303/sqef.for, renamed and relocated as
C  ...adas3xx/adaslib/cxsqef.for.
C
C  VERSION  : 1.1                          
C  DATE     : 15-11-2002
C  MODIFIED : Lorne Horton
C               - First version
C               - Switched to ADAS-standard C3DATA.
C                 This is primarily a change to requiring the full 
C                 input file name as input.
C               - Increased NSTORE to 150 - consistent with ADAS303
C               - Added loop to allow multiple evaluations per call.
C                 This means changing from a function to a
C                 subroutine
C               - Removed IPASS. Routine now re-reads data sets only 
C                 when theinput name has changed.
C               - Added SAVE statement
C
C
C  VERSION  : 1.2                          
C  DATE     : 02-12-2004
C  MODIFIED : Martin O'Mullane
C               - Replace c3data with xxdata_12.
C               - Place into central ADAS.
C
C  VERSION  : 1.3                          
C  DATE     : 17-05-2007
C  MODIFIED : Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C  VERSION  : 1.4                          
C  DATE     : 05-06-2007
C  MODIFIED : Martin O'Mullane
C               - New version of xxdata_12 with extra outputs.
C
C----------------------------------------------------------------------
C----------------------------------------------------------------------
      integer   nstore           , mener            , mtemp           ,
     &          mdens            , mzeff            , mbmag
C----------------------------------------------------------------------
      parameter(nstore = 500     , mener = 24       , mtemp = 12      ,
     &          mdens  = 24      , mzeff =12        , mbmag = 12      )
C----------------------------------------------------------------------
      integer   iunit            , ibsel            , nqeff           , 
     &          iord             , iostat           , ircode          , 
     &          nbsel            , i                , j               , 
     &          nener
C----------------------------------------------------------------------
      real*8    scener           , em1              , em2             ,
     &          sctemp           , scdens           , sczeff          , 
     &          scbmag           , sc               , ethr
C----------------------------------------------------------------------
      character dsname*132       , dsread*132
      character csymb*2          , czion*2          , cwavel*8        , 
     &          cdonor*6         , crecvr*5         , ctrans*7        ,  
     &          cfile*10         , ctype*2          , cindm*3  
C----------------------------------------------------------------------
      character csymba(nstore)*2    , cziona(nstore)*2    , 
     &          cwavela(nstore)*8   , cdonora(nstore)*6   , 
     &          crecvra(nstore)*5   , ctransa(nstore)*7   ,  
     &          cfilea(nstore)*10   , ctypea(nstore)*2    ,
     &          cindma(nstore)*3  
C----------------------------------------------------------------------
      integer   ntempa(nstore)   , ndensa(nstore)   , nenera(nstore)  ,
     &          nzeffa(nstore)   , nbmaga(nstore)
C----------------------------------------------------------------------
      real*8    epro(nqeff)      , ttar(nqeff)      , ti(nqeff)       ,
     &          densi(nqeff)     , zeff(nqeff)      , bmag(nqeff)     ,
     &          qeff(nqeff)      , qefref(nstore)   , enref(nstore)   , 
     &          teref(nstore)    , deref(nstore)    , zeref(nstore)   , 
     &          bmref(nstore)
      real*8    enera(mener,nstore) , qenera(mener,nstore)
      real*8    tempa(mtemp,nstore) , qtempa(mtemp,nstore)
      real*8    densa(mdens,nstore) , qdensa(mdens,nstore)
      real*8    zeffa(mzeff,nstore) , qzeffa(mzeff,nstore)
      real*8    bmaga(mbmag,nstore) , qbmaga(mbmag,nstore)
      real*8    ener(mener)         , qener(mener,nqeff)              , 
     &          oa(mener)           , ya(mener)
C----------------------------------------------------------------------
      save      dsread , nbsel  ,
     &          qefref ,
     &          csymba , cziona , cwavela, cdonora, crecvra , 
     &          ctransa, cfilea , ctypea , cindma ,
     &          enref  , teref  , deref  , zeref  , bmref  ,
     &          nenera , ntempa , ndensa , nzeffa , nbmaga ,
     &          enera  , tempa  , densa  , zeffa  , bmaga  ,
     &          qenera , qtempa , qdensa , qzeffa , qbmaga

C-----------------------------------------------------------------------
C
C  Note effective rate/emission coefficient storage :                     
C
C     ctrana(ibsel) cdonoa(ibsel)  crecva(ibsel)                           
C     cfilea(ibsel) cpcoda(ibsel) cindma(ibsel)                            
C     qefref(ibsel)                                                        
C     enref(ibsel)  teref(ibsel)  deref(ibsel)  zeref(ibsel)  bmref(ibsel) 
C     nenera(ibsel) ntempa(ibsel) ndensa(ibsel) nzeffa(ibsel) nbmaga(ibsel)
C     ( enera(ie,ibsel),ie=1,nenera(ibsel))                                
C     (qenera(ie,ibsel),ie=1,nenera(ibsel))                                
C     ( tempa(it,ibsel),it=1,ntempa(ibsel))                                
C     (qtempa(it,ibsel),it=1,ntempa(ibsel))                                
C     ( densa(in,ibsel),in=1,ndensa(ibsel))                                
C     (qdensa(in,ibsel),in=1,ndensa(ibsel))                                
C     ( zeffa(iz,ibsel),iz=1,nzeffa(ibsel))                                
C     (qzeffa(iz,ibsel),iz=1,nzeffa(ibsel))                                
C     ( bmaga(ib,ibsel),ib=1,nbmaga(ibsel))                                
C     (qbmaga(ib,ibsel),ib=1,nbmaga(ibsel))                                
C
C
C  With limits :
C
C     ibsel     (1 - 150)  : nstore
C     nener     (1 - 24)   : mener
C     ntemp     (1 - 12)   : mtemp
C     ndens     (1 - 24)   : mdens
C     nzeff     (1 - 12)   : mzeff
C     nbmag     (1 - 12)   : mbmag
C
C-----------------------------------------------------------------------
C  Initialise output variables
C-----------------------------------------------------------------------

      csymb  = '  '       
      czion  = '  '    
      cwavel = '        '      
      cdonor = '      '    
      crecvr = '     '    
      ctrans = '       '      
      cfile  = '          '       
      ctype  = '  '    
      cindm  = '   '        
      
      do i = 1, nqeff
         qeff(i) = 0.0D0
      end do
     
C-----------------------------------------------------------------------
C  Open dataset - file name now provided as input
C-----------------------------------------------------------------------
      
      if (dsname.ne.dsread) then

         open( unit=iunit , file=dsname , status='OLD' , iostat=iostat)
         if (iostat.NE.0) then
            dsread = ' '
            ircode = 1
            return
         endif

         call xxdata_12( iunit   , dsname ,
     &                   mtemp   , mdens  , mener  , mzeff   , mbmag  , 
     &                   nstore  ,
     &                   nbsel   ,
     &                   csymba  , cziona , cwavela, cdonora , crecvra,                        
     &                   ctransa , cfilea , ctypea , cindma  ,                         
     &                   qefref  ,
     &                   enref   , teref  , deref  , zeref   , bmref  ,
     &                   nenera  , ntempa , ndensa , nzeffa  , nbmaga ,
     &                   enera   , tempa  , densa  , zeffa   , bmaga  ,
     &                   qenera  , qtempa , qdensa , qzeffa  , qbmaga
     &                 )
         
         close(iunit)
         dsread = dsname
      
      endif


C-----------------------------------------------------------------------
C  Check for invalid index selection
C-----------------------------------------------------------------------
      if (ibsel.le.0.or.ibsel.gt.nbsel) then
         dsread = ' '
         ircode = 3
         return
      endif

C-----------------------------------------------------------------------
C  Collect header information for passing back to user
C-----------------------------------------------------------------------
      csymb  = csymba(ibsel)
      czion  = cziona(ibsel)
      cwavel = cwavela(ibsel)
      cdonor = cdonora(ibsel)
      crecvr = crecvra(ibsel)
      ctrans = ctransa(ibsel)
      cfile  = cfilea(ibsel)
      ctype  = ctypea(ibsel)
      cindm  = cindma(ibsel)     
C-----------------------------------------------------------------------
C  Loop over the vector of plasma/beam conditions for evaluation
C-----------------------------------------------------------------------
      do i=1,nqeff

C-----------------------------------------------------------------------
C  Correction : incident particle energy - used only when ttar<=0.0
C-----------------------------------------------------------------------
         call c3corr(nenera  , ibsel , qenera  , enera ,
     &               qefref  , enref , nstore  , mener ,
     &               epro(i) , scener )
C-----------------------------------------------------------------------
C  Correction : plasma ion density
C-----------------------------------------------------------------------
         call c3corr(ndensa  , ibsel , qdensa  , densa ,
     &               qefref  , deref , nstore  , mdens ,
     &               densi(i), scdens )
C-----------------------------------------------------------------------
C  Correction : plasma ion temperature
C-----------------------------------------------------------------------
         call c3corr(ntempa  , ibsel , qtempa  , tempa ,
     &               qefref  , teref , nstore  , mtemp ,
     &               ti(i)   , sctemp )
C----------------------------------------------------------------------
C  Correction : plasma z effective
C-----------------------------------------------------------------------
         call c3corr(nzeffa  , ibsel , qzeffa  , zeffa ,
     &               qefref  , zeref , nstore  , mzeff ,
     &               zeff(i) , sczeff )
C-----------------------------------------------------------------------
C  Correction : plasma magnetic field
C-----------------------------------------------------------------------
         call c3corr(nbmaga  , ibsel , qbmaga  , bmaga ,
     &               qefref  , bmref , nstore  , mbmag ,
     &               bmag(i) , scbmag )
C-----------------------------------------------------------------------
C  Assemble complete correction.  Convert source data to cross-sections
C  for entry to thermal averaging routine.
C-----------------------------------------------------------------------
         sc    = scdens*sctemp*sczeff*scbmag
         nener = nenera(ibsel)

         do j=1,nener
            ener(j)    = enera(j,ibsel)
            ya(j)      = ener(j)
            qener(j,i) = sc*qenera(j,ibsel)
            oa(j)      = qener(j,i) / (1.38377D6*dsqrt(ener(j)))
         end do     
C-----------------------------------------------------------------------
C  For zero or negative target temperature, return spline onto correct
C  incident particle energy
C-----------------------------------------------------------------------
         qeff(i) = scener*sc*qefref(ibsel)
C-----------------------------------------------------------------------
C  Compute alphas and reduced speeds.  Set zero threshold energy
C-----------------------------------------------------------------------
         if (ttar(i).GT.0.0D0) then
            ethr = 0.0D0
            call c3alrs(iord   , em1    , em2    ,
     &                  epro(i), ttar(i), ethr   ,
     &                  ya     , nener  , nstore ,
     &                  oa     , qeff(i))
         endif
      end do

C-----------------------------------------------------------------------
      return
      end
