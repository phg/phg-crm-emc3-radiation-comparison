CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxqeik.for,v 1.3 2007/05/17 17:01:33 allan Exp $ Date $Date: 2007/05/17 17:01:33 $
CX
      SUBROUTINE CXQEIK( MXNSHL , MXBEAM , IZ1    , IDONOR ,
     &                   NBOT   , NTOP   , NBEAM  , BMENA  ,
     &                   BMFRA  , QTHEOR , FTHEOR
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXQEIK *********************
C
C  PURPOSE:  USES THE EIKONIAL APPROXIMATION TO CALCULATE THE
C            THEORETICAL CHARGE EXCHANGE RATE COEFFICIENTS TO N SHELLS
C            AND THE NL FRACTIONS FROM NEUTRAL HYDROGEN OR HELIUM IN
C            GROUND OR EXCITED STATE TO A BARE NUCLEUS TARGET.
C
C            AN ENERGY DEPENDENT MODIFYING FACTOR CAN BE SWITCHED ON TO
C            MAKE THE TOTAL RATE COEFFT. AGREE BETTER WITH UDWA AT LOW
C            ENERGY. THIS IS ESTABLISHED FROM H+C(+6) AND H+O(+8) DATA.
C            LMOD=.TRUE. SWITCHES ON THE MODIFICATION.
C
C  CALLING PROGRAM: ADAS308 , C6QEIK.
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  MXBEAM   = MAXIMUM NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  IZ1      = CHARGE OF TARGET ION.
C  INPUT : (I*4)  IDONOR   = DONOR STATE FOR EIKONAL MODEL.
C                            1 = H(1S)    DONOR
C                            2 = H(2S)    DONOR
C                            3 = H(2P)    DONOR
C                            4 = H(1S2)   DONOR
C                            5 = HE(1S2S) DONOR
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NBEAM    = NO. OF ENERGY COMPONENTS IN NEUTRAL BEAM.
C  INPUT : (R*8)  BMENA()  = BEAM ENERGY COMPONENTS.
C                            UNITS: EV/AMU
C                            DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMFRA()  = BEAM COMPONENT FRACTIONS.
C                            DIMENSION: COMPONENT INDEX.
C
C  OUTPUT: (R*8)  QTHEOR() = MEAN RATE COEFFICIENTS FOR N-LEVELS
C                            AVERAGED OVER BEAM FRACTIONS.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  OUTPUT: (R*8)  FTHEOR() = MEAN RATE FOR NL-LEVELS AS A FRACTION OF
C                            CORRESPONDING N-LEVEL.
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  PARAM : (I*4)  MXN      = 'MXNSHL'.
C  PARAM : (R*8)  P1       =
C  PARAM : (R*8)  P2       =
C  PARAM : (R*8)  P3       =
C  INPUT : (L*4)  LMOD     = MODIFY FLAG.
C                            .TRUE.  = MODIFY RATE COEFFICIENTS.
C                            .FALSE. = LEAVE COEFFICIENTS UNCHANGED.
C
C          (I*4)  NA       = PRINCIPAL QUANTUM NUMBER OF ELECTRON IN
C                            INITIAL STATE OF INCIDENT NEUTRAL ATOM.
C          (I*4)  LA       = ORBITAL QUANTUM NUMBER OF ELECTRON IN
C                            INITIAL STATE OF INCIDENT NEUTRAL ATOM.
C          (I*4)  N        = PRINCIPAL QUANTUM NUMBER OF FINAL STATE.
C          (I*4)  L        = ORBITAL QUANTUM NUMBER.
C          (I*4)  IB       = ENERGY INDEX.
C          (I*4)  IDL      = INDEX FROM FUNC I4IDFL(N,L).
C
C          (R*8)  ZT       = SCREENING CHARGE FOR THE 1S ELECTRON OF THE
C                            TARGET ATOM IN THE INITIAL STATE.
C          (R*8)  ZT1      = EFFECTIVE CHARGE FOR THE 1S ELECTRON OF THE
C                            TARGET ATOM IN THE FINAL STATE.
C          (R*8)  THETA    = PARAMETER TO GIVE CORRECT BINDING ENERGY
C                            FOR INITIAL TARGET STATE.
C          (R*8)  VEL      = VELOCITY OF INCIDENT ATOM.
C                            UNITS: CM SEC-1
C          (R*8)  VELAU    = VELOCITY OF INCIDENT ATOM.
C                            UNITS: AT. UNITS.
C          (R*8)  XSECNA   = N-RESOLVED CROSS-SECTION FOR CAPTURE.
C                            UNITS: AT. UNITS
C          (R*8)  DIV      = DIVISOR FOR CROSS-SECTIONS.
C
C          (R*8)  FRACLA() = L-RESOLVED CROSS-SECTION AS A FRACTION OF
C                            CORRESPONDING N-RESOLVED CROSS-SECTION.
C                            DIMENSION: REFERENCED BY L QUANTUM NUMBER.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXSGEI     ADAS      CALCULATES EXCHANGE RATE CROSS-SECTIONS.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     07/10/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 20-06-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 02-04-96
C MODIFIED: TIM HAMMOND 
C               - COMMENTED OUT UNREACHABLE LINES WHEN LMOD IS SET
C                 TO .FALSE. WHICH IS CURRENTLY THE CASE (THIS PREVENTS
C                 THE COMPILER GIVING INFO MESSAGES).
C
C VERSION: 1.3                          DATE: 17-05-07
C MODIFIED: Allan Whiteford 
C               - Corrected typo in comments.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      REAL*8     P1                , P2                ,
     &           P3
      PARAMETER( P1 = 1.38377D6    , P2 = 2.1877D8     ,
     &           P3 = 2.80023D-17                        )
C-----------------------------------------------------------------------
      LOGICAL    LMOD
      PARAMETER( LMOD = .FALSE. )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXBEAM  , IZ1     , IDONOR  , NBOT    ,
     &           NTOP    , NBEAM
      INTEGER    NA      , LA      , N       , L       , IB      ,
     &           IDL
C-----------------------------------------------------------------------
      REAL*8     ZT      , ZT1     , THETA   , VEL     , VELAU   ,
     &           XSECNA  , DIV
C-----------------------------------------------------------------------
      REAL*8     BMENA(MXBEAM)  , BMFRA(MXBEAM)  , QTHEOR(MXNSHL)  ,
     &           FTHEOR((MXNSHL*(MXNSHL+1))/2)
      REAL*8     FRACLA(MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXN' LARGE ENOUGH.
C-----------------------------------------------------------------------
C
      IF (MXNSHL .GT. MXN) THEN
         WRITE(I4UNIT(-1),1000) MXN
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C SET PROPERTIES OF DONOR ACCORDING TO DONOR FLAG.
C-----------------------------------------------------------------------
C
      IF (IDONOR .EQ. 1) THEN
         NA = 1
         LA = 0
         ZT = 1.0D0
         ZT1 = 1.0D0
         THETA = 1.0D0
      ELSE IF (IDONOR .EQ. 2) THEN
         NA = 2
         LA = 0
         ZT = 1.0D0
         ZT1 = 1.0D0
         THETA = 1.0D0
      ELSE IF (IDONOR .EQ. 3) THEN
         NA = 2
         LA = 1
         ZT = 1.0D0
         ZT1 = 1.0D0
         THETA = 1.0D0
      ELSE IF (IDONOR .EQ. 4) THEN
         NA = 1
         LA = 0
         ZT = 1.6875D0
         ZT1 = 1.6875D0
         THETA = 0.635D0
      ELSE IF (IDONOR .EQ. 5) THEN
         NA = 2
         LA = 0
         ZT = 1.18D0
         ZT1 = 1.18D0
         THETA = 1.0D0
      ENDIF
C
C-----------------------------------------------------------------------
C
      DO 1 N = NBOT , NTOP
C
         QTHEOR(N) = 0.0D0                              
         DO 2 IDL = I4IDFL( N , 0) , I4IDFL( N , N-1 )    
            FTHEOR(IDL) = 0.0D0                  
    2    CONTINUE
C
C-----------------------------------------------------------------------
C FOR EACH BEAM ENERGY DETERMINE RATE COEFFICIENTS AND ADD TO SUMS.
C-----------------------------------------------------------------------
C
         DO 3 IB = 1 , NBEAM
C
            VEL   = P1 * DSQRT( BMENA(IB) )
            VELAU = VEL / P2
C
C-----------------------------------------------------------------------
C NOTE THAT THE FOLLOWING LINES ARE COMMENTED OUT TO PREVENT 
C COMPILER WARNING MESSAGES THAT LINES CANNOT BE REACHED DUE TO LMOD
C BEING SET IN THE HEADER
C-----------------------------------------------------------------------
C
C           IF (LMOD) THEN
C              DIV = 0.795D0 + 5.52D0 / VELAU**2
C           ELSE
               DIV = 1.0D0
C           ENDIF
C
            CALL CXSGEI( MXNSHL , IZ1    , ZT     , ZT1    , THETA  ,
     &                   VELAU  , NA     , LA     , N      , XSECNA ,
     &                   FRACLA                                       )
C
            QTHEOR(N) = QTHEOR(N) + P3 * VEL * XSECNA * BMFRA(IB) / DIV
C
            DO 4 L = 0 , N-1
               IDL = I4IDFL( N , L )
               FTHEOR(IDL) = FTHEOR(IDL) + P3 * VEL * BMFRA(IB)
     &                       * FRACLA(L+1) * XSECNA / DIV
    4       CONTINUE
C
    3    CONTINUE
C
C-----------------------------------------------------------------------
C CHANGE NL RATES TO FRACTIONS.
C-----------------------------------------------------------------------
C
         DO 5 IDL = I4IDFL( N, 0 ) , I4IDFL( N , N-1 )
            IF (QTHEOR(N) .GT. 0.0D0) THEN
               FTHEOR(IDL) = FTHEOR(IDL) / QTHEOR(N)
            ELSE
               FTHEOR(IDL) = 0.0D0
            ENDIF
    5    CONTINUE
C
C-----------------------------------------------------------------------
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXQEIK ERROR ', 32('*') //
     &        2X, 'VALUE OF ''MXNSHL'' GREATER THAN ', I2, '.' /
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE CXQEIK.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
