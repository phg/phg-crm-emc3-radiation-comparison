CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/z16cd.for,v 1.2 2007/04/23 13:33:41 allan Exp $ Date $Date: 2007/04/23 13:33:41 $
CX
      FUNCTION Z16CD( IB  , IV  , IO1 , IO2 , IO3 ,
     &                IT  , IG  , IS  ,
     &                QN  , PM  , ZT1 , ETA , NA  , LA
     &           )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 COMPLEX*16 FUNCTION: Z16CD  ****************
C
C  PURPOSE: CALCULATES THE COMPLEX FUNCTION D(ALPHA,GAMMA) FOR THE
C           EIKONAL NL SELECTIVE CHARGE TRANSFER CROSS-SECTION 
C           APPROXIMATION OF EICHLER 
C
C  NOTE: EVALUATES FORMULA (22C) OF EICHLER PHYS REV A (1981) 23,498.
C        COLLISION IS A^(+ZP) +B_(ZT)(NL) -> A^(+ZP-1)(N'L') +B^(+)_(ZT)
C        BINDING ENERGY FOR TARGET (DONOR) ATOM DETERMINED BY ZT AS 
C            EPST=-ZT^2/2N^2
C        BINDING ENERGY FOR PROJECTILE (RECEIVER) ION DETERMINED FROM 
C            EPSP=-ZP^2/2(N')^2
C        ALSO EPS=EPSP-EPST 
C            
C        ALPHA REPRESENTS PARAMETERS IB,IV,IO1,IO2,IO3,IT
C        GAMMA REPRESENTS PARAMETER IG                                              
C
C  CALLING PROGRAM: CXSGEI
C
C  INPUT:  (I*4)  IB      = EICHLER PARAMETER BETA
C  INPUT:  (I*4)  IV      = EICHLER PARAMETER NU
C  INPUT:  (I*4)  IO1     = EICHLER PARAMETER SIGMA 1
C  INPUT:  (I*4)  IO2     = EICHLER PARAMETER SIGMA 2
C  INPUT:  (I*4)  IO3     = EICHLER PARAMETER SIGMA 3
C  INPUT:  (I*4)  IT      = EICHLER PARAMETER TAU
C  INPUT:  (I*4)  IG      = EICHLER PARAMETER GAMMA
C  INPUT:  (I*4)  IS      = EICHLER INDEX PARAMETER S
C  INPUT:  (R*8)  QN      = EICHLER PARAMETER = ZT/N 
C  INPUT:  (R*8)  PM      = EICHLER PARAMETER P_(-)=EPS*ETA-V/2 WITH
C                           V THE PROJECTILE (RECEIVER) LAB SPEED
C  INPUT:  (R*8)  ZT1     = EFFECTIVE TARGET (DONOR) CHARGE IN THE FINAL 
C                           STATE.
C  INPUT:  (R*8)  ETA     = EICHLER PARAMETER ETA
C  INPUT:  (I*4)  NA      = N QUANTUM NUMBER OF THE INITIAL STATE.
C  INPUT:  (I*4)  LA      = L QUANTUM NUMBER OF THE INITIAL STATE.
C
C  OUTPUT: (R*8)  Z16CD   = D(ALPHA,GAMMA)
C
C          (I*4)  JB      =
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  XNA     = REAL VALUE = NA.
C          (R*8)  XS      = REAL VALUE = S.
C          (R*8)  T1      =
C          (R*8)  T2      =
C          (R*8)  T3      =
C
C          (Z*16) CT1     =
C          (Z*16) CT2     =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4JGAM     ADAS      RETURNS VALUE FROM 'JGAM' TABLE.
C          R8GAM      ADAS      RETURNS VALUE FROM 'GAM' TABLE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    05/10/93
C
C VERSION: 1.1                          DATE: 05-10-93
C MODIFIED: JONATHAN NASH
C               - FIRST FRELEASE
C
C VERSION: 1.2                          DATE: 18-04-07
C MODIFIED: HUGH SUMMERS
C               - FIRST FULLY COMMENTED RELEASE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      COMPLEX*16  Z16CD
C-----------------------------------------------------------------------
      INTEGER     I4JGAM
      REAL*8      R8GAM
C-----------------------------------------------------------------------
      INTEGER     IB      , IV      , IO1     , IO2     , IO3     ,
     &            IT      , IG      , IS      , NA      , LA
      INTEGER     JB      , I
C-----------------------------------------------------------------------
      REAL*8      QN      , PM      , ZT1     , ETA
      REAL*8      XNA     , XS      , T1      , T2      , T3
C-----------------------------------------------------------------------
      COMPLEX*16  CT1     , CT2
C-----------------------------------------------------------------------
C
      JB  = ( IB + 1 ) / 2
      XNA = DFLOAT( NA )
      CT1 = ( 0.0D0 , -1.0D0 )**IO2 * ( DCMPLX( QN , PM ) )**IS
      XS  = DFLOAT( IS )
      CT2 = DCMPLX( XS , -ETA * ZT1 )
C
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , LA + IB + 1
         CT2 = CT2 - 1.0D0
         CT1 = CT1 * CT2 / DFLOAT( I )
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      T2 = ( -1.0D0 )**( IO1 + IO3 + IS ) * 2.0D0**( IO2 - IS )
     &     * R8GAM( IG + IV + 1 )
     &     / ( R8GAM( IO1 + 1) * R8GAM( IO2 + 1 ) * R8GAM( IO3 + 1 ) )
     &     * 64.0D0**( I4JGAM( IG + IV + 1 ) - I4JGAM( IO1 + 1 )
     &                 - I4JGAM( IO2 + 1 ) - I4JGAM( IO3 + 1 ) )
     &     * QN**( IB - 2 * IV - IT + 2 * IO1 ) * PM**IO2
     &     * R8GAM( IB + 2 - 2 * IV )
     &     / ( R8GAM( IB + 2 - 2 * IV - IT ) * R8GAM( IT + 1 ) )
     &     * 64.0D0**( I4JGAM ( IB + 2 - 2 * IV )
     &                 - I4JGAM( IB + 2 - 2 * IV - IT )
     &                 - I4JGAM( IT + 1 ) )
      T1 = 2.0D0**( 2 * LA + IB + 2 * IV ) * QN**IB
     &     * ( R8GAM( NA - LA ) * R8GAM( NA + LA + 1 ) / XNA)**0.5D0
     &     * R8GAM( LA + 2 + IV ) * ( 2 * LA + 2 + IB )
     &     * R8GAM( JB + 1 )
     &     / ( R8GAM( 2 * LA + 3 + 2 * IV ) * R8GAM( NA - LA - IB )
     &         * R8GAM( JB - IV + 1 ) * R8GAM( IB + 1 )
     &         * R8GAM( IV + 1 ) )
     &     * 8.0D0**( I4JGAM( NA - LA ) + I4JGAM( NA + LA + 1 ) )
     &     * 64.0D0**( I4JGAM( LA + 2 + IV ) + I4JGAM( JB + 1 )
     &                 - I4JGAM( 2 * LA + 3 + 2 * IV )
     &                 - I4JGAM( NA - LA - IB ) - I4JGAM( JB - IV + 1 )
     &                 - I4JGAM( IB + 1 ) - I4JGAM( IV + 1 ) )
      T3  = -DFLOAT( IB / 2 ) - 1.5D0
C
C-----------------------------------------------------------------------
C
      IF (IV .NE. 0) THEN
         DO 2 I = 0 , IV-1
            T3 = T3 + 1.0D0
            T1 = T1 * T3
    2    CONTINUE
      ENDIF
C
C-----------------------------------------------------------------------
C
      Z16CD = T1 * T2 * CT1
C
C-----------------------------------------------------------------------
C
      RETURN
      END
