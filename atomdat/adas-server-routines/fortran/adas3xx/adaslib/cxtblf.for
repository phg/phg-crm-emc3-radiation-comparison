CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxtblf.for,v 1.1 2004/07/06 13:11:20 whitefor Exp $ Date $Date: 2004/07/06 13:11:20 $
CX
      SUBROUTINE CXTBLF( MXNSHL , IZ1 , NBOT , NTOP , TBLF )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXTBLF *********************
C
C  PURPOSE: SETS UP A RADIATIVE LIFETIME TABLE FOR NL LEVELS OF A
C           HYDROGENIC ION.
C
C  CALLING PROGRAM: ADAS306 , ADAS308.
C
C  INPUT : (I*4)  MXNSHLL  = MAXIMUM VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C
C  OUTPUT: (R*8)  TBLF()   = TABLE OF RADIATIVE LIFETIMES.
C                            UNITS: SECS
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C          (I*4)  NU       = UPPER VALUE OF N QUANTUM NUMBER.
C          (I*4)  LU       = L QUANTUM NUMBER FOR NU.
C          (I*4)  NL       = LOWER VALUE OF N QUANTUM NUMBER.
C          (I*4)  LL       = L QUANTUM NUMBER FOR NL.
C          (I*4)  IDL      = TABLE INDEX.
C
C          (R*8)  SUM     = SUM OF A-VALUES FOR GIVEN NU AND LU.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8ATAB     ADAS      RETURNS HYDRONIC L-RESOLVED A-VALUES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     30/09/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER  I4IDFL
      REAL*8   R8ATAB
C-----------------------------------------------------------------------
      INTEGER  MXNSHL  , IZ1     , NBOT    , NTOP
      INTEGER  NU      , LU      , NL      , LL      , IDL
C-----------------------------------------------------------------------
      REAL*8   SUM
C-----------------------------------------------------------------------
      REAL*8   TBLF((MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C FILL LIFETIME TABLE FROM 1 TO 'NBOT'-1 WITH 1.0.
C-----------------------------------------------------------------------
C
      DO 1 IDL  = 1 , I4IDFL( NBOT-1 , NBOT-2 )
         TBLF(IDL) = 1.0D+10
    1 CONTINUE
C
C-----------------------------------------------------------------------
C FILL LIFETIME TABLE FROM 'NBOT' TO 'NTOP'.
C-----------------------------------------------------------------------
C
      DO 2 NU = NBOT , NTOP
         DO 3 LU = 0 , NU-1
C
            SUM=0.0D0
C
            DO 4 NL = NBOT-1 , NU-1
               DO 5 LL = LU-1 , LU+1 , 2
C
                  IF ((LL .GE. 0) .AND. (LL.LT.NL)) THEN
                     SUM = SUM + R8ATAB( IZ1 , NU , LU , NL , LL )
                  ENDIF
C
    5          CONTINUE
    4       CONTINUE
C
            IF (SUM .GT. 1.0D-10) THEN
               TBLF(I4IDFL( NU , LU )) = 1.0D+00 / SUM
            ELSE
               TBLF(I4IDFL( NU , LU )) = 1.0D+10
            ENDIF
C
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
