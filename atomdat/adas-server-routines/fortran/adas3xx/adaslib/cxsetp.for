CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxsetp.for,v 1.1 2004/07/06 13:11:07 whitefor Exp $ Date $Date: 2004/07/06 13:11:07 $
CX
      SUBROUTINE CXSETP( MXTAB  , MXGRF  , SYMBD  , IDZ0   ,
     &                   SYMBR  , IRZ0   , IRZ1   , IRZ2   ,
     &                   NGRND  , NTOT
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXSETP *********************
C
C  PURPOSE:  SETS UP PARAMETERS IN THE SHARED POOL FOR PANEL DISPLAY.
C
C  CALLING PROGRAM: ADAS306 / ADAS308.
C
C  DATA:  DATA IS OBTAINED VIA SUBROUTINE 'CXDATA'.
C
C  INPUT : (I*4)  MXTAB   = MAXIMUM NUMBER OF EMISSIVITY TABLES.
C  INPUT : (I*4)  MXGRF   = MAXIMUM NUMBER OF EMISSIVITY GRAPHS.
C  INPUT : (C*2)  SYMBD   = ELEMENT SYMBOL OF DONOR.
C  INPUT : (I*4)  IDZ0    = DONOR NUCLEAR CHARGE.
C  INPUT : (C*2)  SYMBR   = ELEMENT SYMBOL OF RECEIVER.
C  INPUT : (I*4)  IRZ0    = RECEIVER NUCLEAR CHARGE.
C  INPUT : (I*4)  IRZ1    = RECEIVER ION INITIAL CHARGE.
C  INPUT : (I*4)  IRZ2    = RECEIVER ION FINAL CHARGE.
C  INPUT : (I*4)  NGRND   = MINIMUM ALLOWED N QUANTUM NUMBER.
C  INPUT : (I*4)  NTOT    = MAXIMUM ALLOWED N QUANTUM NUMBER.
CX
CX         (I*4)  PIPEIN  = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
CX         (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
CX	    XXFLSH	IDL-ADAS    FLUSHES UNIX PIPE
C
C AUTHOR:  Tim Hammond (TESSELLA SUPPORT SERVICES PLC)
C          RAL EXT. 6404
C
C DATE:    05/06/95
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER  SYMBD*2  , SYMBR*2
C-----------------------------------------------------------------------
      INTEGER    MXTAB   , MXGRF   , IDZ0    , IRZ0    , IRZ1    ,
     &           IRZ2    , NGRND   , NTOT
      INTEGER    PIPEIN             , PIPEOU
      PARAMETER( PIPEIN=5           , PIPEOU=6)
C-----------------------------------------------------------------------
CX WRITE DATA OUT TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, *) MXTAB
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) MXGRF
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, '(A2)') SYMBD
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) IDZ0
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, '(A2)') SYMBR
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) IRZ0
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) IRZ1
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) IRZ2
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) NGRND
      CALL XXFLSH( PIPEOU)
      WRITE( PIPEOU, *) NTOT
      CALL XXFLSH( PIPEOU)
C-----------------------------------------------------------------------
C
      RETURN

      END
