CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxmrdg.for,v 1.2 2007/05/17 17:07:51 allan Exp $ Date $Date: 2007/05/17 17:07:51 $
CX
      SUBROUTINE CXMRDG ( MXNSHL , MXJSHL , IZ0    , IZ1    ,
     &                    AMSSNO , NI     , LI     , NJ     ,
     &                    LJ     , BMAG   , TIEV   , TBLF   ,
     &                    FMP    , FMM    , FMI    , FMJ
     &                  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXMRDG *********************
C
C  PURPOSE:  CALCULATES MAGNETIC FIELD DEPENDENT MIXING RATE
C            COEFFICIENTS BETWEEN NEARLY DEGENERATE LEVELS FOR
C            HYDROGEN-LIKE, LITHIUM-LIKE AND SODIUM-LIKE IONS.
C
C            RATES ARE CALCULATED FOR THE SEPARATE
C            NLJ->NL+1J', NLJ->NLJ' AND NLJ->NL-1J'
C            TRANSITIONS.
C
C  CALLING PROGRAM: C6TBFM
C
C  INPUT : (I*4)  MXNSHL  = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL  = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ0     = TARGET NUCLEAR CHARGE.
C  INPUT : (I*4)  IZ1     = ION CHARGE.
C  INPUT : (R*8)  AMSSNO  = ATOMIC MASS NO.
C  INPUT : (I*4)  NI      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE I.
C  INPUT : (I*4)  LI      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE I.
C  INPUT : (I*4)  NJ      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE J.
C  INPUT : (I*4)  LJ      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE J.
C  INPUT : (R*8)  BMAG    = MAGNETIC INDUCTION.
C                           UNITS: TESLA
C  INPUT : (R*8)  TIEV    = TEMPERATURE (ION DISTRIBUTION).
C                           UNITS: EV
C  INPUT : (R*8)  TBLF()  = TABLE OF RADIATIVE LIFETIMES.
C                           UNITS: SECS
C                           DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (R*8)  FMP()   = RATE COEFFT. FOR NLJ->NL+1J'.
C                           DIMENSION: J->J' TRANSITION INDEX.
C  OUTPUT: (R*8)  FMM()   = RATE COEFFT. FOR NLJ->NL+1J'.
C                           DIMENSION: J->J' TRANSITION INDEX.
C  OUTPUT: (R*8)  FMI()   = RATE COEFFT. FOR NLJ->NLJ' FOR STATE I.
C                           DIMENSION: J->J' TRANSITION INDEX.
C  OUTPUT: (R*8)  FMJ()   = RATE COEFFT. FOR NLJ->NLJ' FOR STATE J.
C                           DIMENSION: J->J' TRANSITION INDEX.
C
C  PARAM : (I*4)  MXTERM  = 2.
C  PARAM : (R*8)  P1      =
C
C          (I*4)  ICI     =
C          (I*4)  ICJ     =
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C
C          (R*8)  Z1      = REAL VALUE = IZ1.
C          (R*8)  TI      = TEMPERATURE (ION DISTRIBUTION).
C                           UNITS:
C          (R*8)  XNI     = REAL VALUE = NI.
C          (R*8)  XLI     = REAL VALUE = LI.
C          (R*8)  XLJ     = REAL VALUE = LJ.
C          (R*8)  XLG     =
C          (R*8)  FACT1   =
C          (R*8)  FACT2   =
C          (R*8)  EI0     = BINDING ENERGY FOR STATE I.
C                           UNITS: RYD
C          (R*8)  EJ0     = BINDING ENERGY FOR STATE J.
C                           UNITS: RYD
C          (R*8)  ZEFFI   = EFFECTIVE ION CHARGE FOR STATE I.
C          (R*8)  ZEFFJ   = EFFECTIVE ION CHARGE FOR STATE J.
C          (R*8)  TAUI    = RADIATIVE LIFETIME FOR STATE I.
C                           UNITS: SECS
C          (R*8)  TAUJ    = RADIATIVE LIFETIME FOR STATE J.
C                           UNITS: SECS
C          (R*8)  TAU     =
C          (R*8)  XS      =
C          (R*8)  XXJI    =
C          (R*8)  XXJJ    =
C          (R*8)  DE      =
C          (R*8)  W       =
C          (R*8)  T1      =
C          (R*8)  T2      =
C          (R*8)  T3      =
C          (R*8)  F1      =
C          (R*8)  F2      =
C
C          (R*8)  XSI()   =
C                           DIMENSION: 2
C          (R*8)  XJI()   =
C                           DIMENSION: 2
C          (R*8)  XEI()   =
C                           DIMENSION: 2
C          (R*8)  XSJ()   =
C                           DIMENSION: 2
C          (R*8)  XJJ()   =
C                           DIMENSION: 2
C          (R*8)  XEJ()   =
C                           DIMENSION: 2
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          R8ZETA     ADAS
C          R8WIG6     ADAS
C          CXHYDE     ADAS      CALCULATES BINDING ENERGY FOR H-LIKE
C                               ION.
C          CXLTHE     ADAS      CALCULATES BINDING ENERGY FOR LI-LIKE
C                               ION.
C          CXSODE     ADAS      CALCULATES BINDING ENERGY FOR NA-LIKE
C                               ION.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    04/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
      REAL*8     R8ZETA  , R8WIG6
C-----------------------------------------------------------------------
      INTEGER    MXTERM
      PARAMETER( MXTERM = 2 )
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 1.1605D4 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ0     , IZ1     , NI      ,
     &           LI      , NJ      , LJ
      INTEGER    ICI     , ICJ     , I       , J
C-----------------------------------------------------------------------
      REAL*8     AMSSNO  , BMAG    , TIEV
      REAL*8     Z1      , TI      , XNI     , XLI     , XLJ     ,
     &           XLG     , FACT1   , FACT2   , EI0     , EJ0     ,
     &           ZEFFI   , ZEFFJ   , TAUI    , TAUJ    , TAU     ,
     &           XS      , XXJI    , XXJJ    , DE      , W       ,
     &           T1      , T2      , T3      , F1      , F2
C-----------------------------------------------------------------------
      REAL*8     TBLF((MXNSHL*(MXNSHL+1))/2)  ,
     &           FMP(2*MXJSHL)                ,
     &           FMM(2*MXJSHL)                ,
     &           FMI(2*MXJSHL)                ,
     &           FMJ(2*MXJSHL)
      REAL*8     XSI(MXTERM)   , XJI(MXTERM)   , XEI(MXTERM)  ,
     &           XSJ(MXTERM)   , XJJ(MXTERM)   , XEJ(MXTERM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE OUTPUT VALUES.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , 2 * MXJSHL
         FMP(I) = 0.0D0
         FMM(I) = 0.0D0
         FMI(I) = 0.0D0
         FMJ(I) = 0.0D0
    1 CONTINUE
C
C-----------------------------------------------------------------------
C CHECK INPUT VALUES ARE VALID.
C-----------------------------------------------------------------------
C
      IF ((NI .NE. NJ) .OR. (NI .GT. MXNSHL) .OR.
     &    (IABS( LI - LJ ) .NE. 1)                ) RETURN
C
C-----------------------------------------------------------------------
C
      Z1 = DFLOAT( IZ1 )
C
      TI = P1 * TIEV
C
      XNI = DFLOAT( NI )
C
      XLI = DFLOAT( LI )
      XLJ = DFLOAT( LJ )
      XLG = DMAX1( XLI , XLJ )
C
      FACT1 = 5.84189D-15 * BMAG**2 * TIEV / AMSSNO
      FACT2 = 1.81011D-11 * BMAG**2
C
C-----------------------------------------------------------------------
C CALCULATE CENTRE BINDING ENERGY:
C   CXHYDE = HYDROGEN-LIKE.
C   CXLTHE = LITHIUM-LIKE.
C   CXSODE = SODIUM-LIKE.
C-----------------------------------------------------------------------
C
      IF (IZ1 .EQ. IZ0) THEN
         CALL CXHYDE( IZ0 , Z1 , NI , LI , EI0 )
         CALL CXHYDE( IZ0 , Z1 , NJ , LJ , EJ0 )
         ZEFFI = Z1
         ZEFFJ = Z1
      ELSE IF (IZ1 .EQ. (IZ0 - 2)) THEN
         CALL CXLTHE( IZ0 , Z1 , NI , LI , EI0 )
         CALL CXLTHE( IZ0 , Z1 , NJ , LJ , EJ0 )
         ZEFFI = Z1
         ZEFFJ = Z1
      ELSE IF (IZ1 .EQ. (IZ0 - 10)) THEN
         CALL CXSODE( IZ0 , NI , LI , ZEFFI , EI0 )
         CALL CXSODE( IZ0 , NJ , LJ , ZEFFJ , EJ0 )
      ELSE
         WRITE(I4UNIT(-1),1000)
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CHANGE TO TERM ENERGIES (-VE) AND ADD IN TERM SPLITTING
C-----------------------------------------------------------------------
C
      IF (LI .EQ. 0) THEN
         ICI = 1
         XSI(1) = 0.5D0
         XJI(1) = 0.5D0
         XEI(1) = -EI0
      ELSE
         ICI = 2
         XSI(1) = 0.5D0
         XSI(2) = 0.5D0
         XJI(1) = XLI + 0.5D0
         XJI(2) = XLI - 0.5D0
         XEI(1) = -EI0 + 0.5D0 * XLI * R8ZETA( ZEFFI , NI , LI )
         XEI(2) = -EI0 - 0.5D0 * ( XLI + 1.0D0 )
     &            * R8ZETA( ZEFFI , NI , LI )
      ENDIF
C
      ICJ = 2
      XSJ(1) = 0.5D0
      XSJ(2) = 0.5D0
      XJJ(1) = XLJ + 0.5D0
      XJJ(2) = XLJ - 0.5D0
      XEJ(1) = -EJ0 + 0.5D0 * XLJ * R8ZETA( ZEFFJ , NI , LJ )
      XEJ(2) = -EJ0 - 0.5D0 * ( XLJ + 1.0D0 )
     &         * R8ZETA( ZEFFJ , NI , LJ )
      TAUI = TBLF(I4IDFL( NI , LI ))
      TAUJ = TBLF(I4IDFL( NI , LJ ))
      TAU  = DSQRT( TAUI * TAUJ )
      T1 = 2.25D0 * ( XNI / Z1 )**2 * XLG * ( XNI**2 - XLG**2 )
C
C-----------------------------------------------------------------------
C RATE COEFFICIENT FOR H(DYN).
C-----------------------------------------------------------------------
C
      DO 2 I = 1 , ICI
         DO 3 J = 1 , ICJ
C
            IF (XSI(I) .EQ. XSJ(J)) THEN
C
               XS   = XSI(I)
               XXJI = XJI(I)
               XXJJ = XJJ(J)
C
               DE = DABS( XEJ(J) - XEI(I) )
C
               IF (DE .LE. 0.0D0) THEN
                  T3 = 1.70911D33 * TAU
               ELSE
                  F1 = 4.27278D32 * TAU * DE**2
                  F2 = 0.5D0 / TAU
                  T3 = 4.0D0 * DMIN1( F1 , F2 ) / DE**2
               ENDIF
C
               W  = R8WIG6( XLJ , 1.0D0 , XLI , XXJI , XS , XXJJ )
               T2 = ( 2.0D0 * XXJI + 1.0D0 ) * ( 2.0D0 * XXJJ + 1.0D0 )
     &              * W**2 * T1
C
               IF (T2 .GT. 0.0D0) THEN
                  FMP(2*(I-1)+J) = FACT1 * T3 * T2
     &                             / ( 2.0D0 * XXJI + 1.0D0 )
                  FMM(2*(J-1)+I) = FACT1 * T3 * T2
     &                             / ( 2.0D0 * XXJJ + 1.0D0 )
               ENDIF
C
            ENDIF
C
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C  RATE COEFFICIENT FOR H(MAG) FOR STATE I.
C-----------------------------------------------------------------------
C
      DO 4 I = 1 , ICI
         DO 5 J = 1 , ICI
C
            IF(I .NE. J) THEN
C
               XXJI = XJI(I)
               XXJJ = XJI(J)
C
               DE = DABS( XEI(I) - XEI(J) )
C
               IF (DE .LE. 0.0D0) THEN
                  T3 = 1.70911D33 * TAUI
               ELSE
                  F1 = 4.27278D32 * TAUI * DE**2
                  F2 = 0.5D0 / TAUI
                  T3 = 4.0D0 * DMIN1( F1 , F2 ) / DE**2
               ENDIF
C
               W  = R8WIG6( 0.5D0 , 0.5D0 , 1.0D0 ,
     &                      XXJJ  , XXJI  , XLI     )
               T2 = 2.0D0 * ( 2.0D0 * XXJI + 1.0D0 )
     &              * ( 2.0D0 * XXJJ + 1.0D0 ) * W**2
C
               FMI(2*(I-1)+J) = FACT2 * T3 * T2
     &                          / ( 2.0D0 * XXJI + 1.0D0 )
C
            ENDIF
C
    5     CONTINUE
    4 CONTINUE
C
C-----------------------------------------------------------------------
C  RATE COEFFICIENT FOR H(MAG) FOR STATE J.
C-----------------------------------------------------------------------
C
      DO 6 I = 1 , ICJ
         DO 7 J = 1 , ICJ
C
            IF(I .NE. J) THEN
C
               XXJI = XJJ(I)
               XXJJ = XJJ(J)
C
               DE = DABS( XEJ(I) - XEJ(J) )
C
               IF (DE .LE. 0.0D0) THEN
                  T3 = 1.70911D33 * TAUJ
               ELSE
                  F1 = 4.27278D32 * TAUJ * DE**2
                  F2 = 0.5D0 / TAUJ
                  T3 = 4.0D0 * DMIN1( F1 , F2 ) / DE**2
               ENDIF
C
               W  = R8WIG6( 0.5D0 , 0.5D0 , 1.0D0 ,
     &                      XXJJ  , XXJI  , XLJ     )
               T2 = 2.0D0 * ( 2.0D0 * XXJI + 1.0D0 )
     &              * ( 2.0D0 * XXJJ + 1.0D0 ) * W**2
C
               FMJ(2*(I-1)+J) = FACT2 * T3 * T2
     &                          / ( 2.0D0 * XXJI + 1.0D0 )
C
            ENDIF
C
    7     CONTINUE
    6 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXCRDG ERROR ', 32('*') //
     &        2X, 'DIFFERENCE BETWEEN NUCLEAR AND ION CHARGE GIVES ',
     &            'UNKNOWN ION TYPE.'/
     &        2X, 'NUCLEAR CHARGE Z0 = ', I3 ,
     &            '   ION CHARGE Z1 = ', I3/
     &        2X, 'DIFFERENCE MUST BE 0 (H-LIKE), 2 (LI-LIKE), ',
     &            'OR 10 (NA-LIKE).' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
