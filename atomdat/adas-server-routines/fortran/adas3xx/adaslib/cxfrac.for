CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxfrac.for,v 1.2 2007/05/24 15:27:40 mog Exp $ Date $Date: 2007/05/24 15:27:40 $
CX
       SUBROUTINE CXFRAC( MXNENG , MXNSHL ,
     &                    NENRGY , NMIN   , NMAX   ,
     &                    LSETL  ,
     &                    SIGNA  , FRACLA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXFRAC *********************
C
C  PURPOSE:  TO CONVERT L RESOLVED PARTIAL CROSS-SECTIONS FROM
C            ABSOLUTE VALUES TO FRACTIONS OF THE N AND L RESOLVED DATA,
C            RESPECTIVELY.
C
C  CALLING PROGRAM: ADAS301/ADAS306/ADAS307/ADAS308/ADAS309
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES READ.
C  INPUT : (I*4)  NMIN      = LOWEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (I*4)  NMAX      = HIGHEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C  INPUT : (R*8)  SIGNA(,)  = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C
C  I/O   : (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             INPUT : ABSOLUTE VALUES (CM2)
C                             OUTPUT: FRACTION OF N-RESOLVED DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C
C          (I*4)  IE        = ENERGY INDEX.
C          (I*4)  N         = N QUANTUM NUMBER.
C          (I*4)  L         = L QUANTUM NUMBER.
C          (I*4)  IDL       = L-RESOLVED DATA INDEX.
C          (I*4)  IDM       = M-RESOLVED DATA INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C          I4IDFM     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N, L AND M.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    15/09/93
C
C VERSION  : 1.2
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   I4IDFL  , I4IDFM
      INTEGER   MXNSHL  , MXNENG  , NENRGY  , NMIN    , NMAX
      INTEGER   IE      , N       , L       , IDL     , IDM
C-----------------------------------------------------------------------
      LOGICAL   LSETL   , LSETM
C-----------------------------------------------------------------------
      REAL*8    SIGNA(MXNENG,MXNSHL)  ,
     &          FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------

C***********************************************************************
C IF L-RESOLVED CROSS-SECTIONS THEN CONVERT TO FRACTION OF N-RESOLVED.
C***********************************************************************
C
      IF (LSETL) THEN
         DO 5 IE = 1, NENRGY
            DO 6 N = NMIN, NMAX
               DO 7 IDL = I4IDFL( N, 0 ), I4IDFL( N, N-1 )
                  IF (SIGNA(IE,N) .EQ. 0.0D+00) THEN
                     FRACLA(IE,IDL) = 0.0D+00
                  ELSE
                     FRACLA(IE,IDL) = FRACLA(IE,IDL) / SIGNA(IE,N)
                  ENDIF
    7          CONTINUE
    6       CONTINUE
    5    CONTINUE
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
