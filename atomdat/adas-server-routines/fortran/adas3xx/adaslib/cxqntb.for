       subroutine cxqntb( iunit  ,
     &                    ndein  , ndrep  , ndlev   ,
     &                    inrep   , nrep   ,
     &                    nbeam  , bmena  , bmfra   ,
     &                    qcxn   , qthrep , alpha
     &                  )

       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran 77 subroutine: cxqntb *****************
c
c  Purpose: Gathers data from an adf01 charge exchange file and returns
c           rate coeffts. on a requested energy vector and represent-
c           ative n-shell set. Also returns average rate coeffts over
c           fractionally weighted beam energies.
c
c  Notes:   The code is a conversion/modernisation of subroutine bnqctn
c           used in adas310
c
c  Subroutine:
c
c   input : (i*4) iunit    = unit number for input adf01 file .
c   input : (i*4) ndein    = maximum  number of user requested
c                            beam energies
c   input : (i*4) ndlev    = maximum  number of n-shells
c   input : (i*4) ndrep    = maximum  number of representative n-shells
c   input : (i*4) inrep    = number of representative levels
c   input : (i*4) nrep()   = representative n-levels
c                            1st dim: representative n-shell index
c   input : (i*4) nbeam    = number of energy components in neutral
c                            hydrogen beam
c   input : (r*8) bmena()  = beam energy components (ev/amu)
c                            1st dim: beam energy component index
c   input : (r*8) bmfra()  = beam fractions in energy components
c                            1st dim: beam energy component index
c
c   output: (r*8) qcxn(,)  = cx rate coefficient (cm^3 s^-1)
c                            1st dim: component energy index
c                            2nd dim: representative n-shell index
c   output: (r*8) qthrep() = mean qcxn coefficients for representative
c                            n-shells  (averaged over beam
c                            fractions) (cm3 sec-1)
c                            1st dim: representative n-shell index
c   output: (r*8) alpha    = size of 1/n**alpha tail for ch.exch x-sect.
c
c
c
c  Routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     xxdata_01  adas      reads input data set in adf01 format.
c     xxnbaf     adas      nag e01baf spline fit substitute.
c     xxnbbf     adas      nag e01bbf spline fit substitute.
c
c
c  Author   : Hugh Summers
c  Date     : 15-06-2007
c
c
c  Version : 1.1
c  Date    : 15-06-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 15-07-2009
c  Modified: Martin O'Mullane
c            - Do not close adf01 file within routine. It should be
c              opened and closed by calling routine.
c
c-----------------------------------------------------------------------
      integer   idneng           , idnshl
c-----------------------------------------------------------------------
      parameter (idneng = 30     , idnshl = 550 )
c-----------------------------------------------------------------------
      integer    iunit
      integer    i4unit
      integer    ndein   , ndrep   , ndlev
      integer    izr     , izd     , indd     , nbeng2  , nminf   ,
     &           nmaxf   , nbeam   , inrep
      integer    ifail   , iener   , iloop    , n       , nn
      integer    i       , j
c-----------------------------------------------------------------------
      real*8     alpha   , vel     , xval     , sval    , zval    ,
     &           ss
c-----------------------------------------------------------------------
      logical    lparms  , lsetl
c-----------------------------------------------------------------------
      character  symbr*2 , symbd*2
c-----------------------------------------------------------------------
      integer    lforma(idneng)
      integer    nrep(ndrep+1)
c-----------------------------------------------------------------------
      real*8     bengy(idneng)   , alphaa(idneng)  , xlcuta(idneng)  ,
     &           pl2a(idneng)    , pl3a(idneng)    , xtot(idneng)
      real*8     xsa(idneng)     , ysa(idneng)     , zsa(idneng)
      real*8     wts(idneng)     , b(idneng)
      real*8     a(idneng+4,4)   , diag(idneng+4)  , xka(idneng+4)   ,
     &           c(idneng+4)     , zka(idneng+4)   , zc(idneng+4)
      real*8     xsign(idneng,idnshl)                      ,
     &           xsigl(idneng,(idnshl*(idnshl+1))/2)

      real*8     bmena(ndein)    , bmfra(ndein)
      real*8     qthrep(ndrep+1) , qcxn(ndein,ndrep+1)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initial internal dimension check
c-----------------------------------------------------------------------
c
       if (idneng.ne.ndein) then
          write(i4unit(-1),1001) 'idneng=',idneng,' .ne. ndein=',ndein
          write(i4unit(-1),1002)
          stop
       endif

       if (idnshl.ne.ndlev) then
          write(i4unit(-1),1001) 'idnshl=',idnshl,' .ne. ndlev=',ndlev
          write(i4unit(-1),1002)
          stop
       endif

       alpha = 0.00d+00

       do i=1,inrep
         qthrep(i)=0.0d+00
         do j = 1,nbeam
           qcxn(j,i)=0.0d0
         enddo
       enddo
c
c-----------------------------------------------------------------------
c read charge exchange data from adf01 file.
c-----------------------------------------------------------------------
c
       call xxdata_01( iunit  , idneng , idnshl ,
     &                 symbr  , symbd  , izr    , izd    ,
     &                 indd   , nbeng2 , nminf  , nmaxf  ,
     &                 lparms , lsetl  , bengy  ,
     &                 alphaa , lforma , xlcuta , pl2a   ,
     &                 pl3a   , xtot   , xsign  , xsigl
     &                )

c
c-----------------------------------------------------------------------
c  main loops through beam energy fractions and representative levels
c-----------------------------------------------------------------------
       do i=1,inrep
         n=nrep(i)

         if(n.ge.nminf)then
             nn=min0(n,nmaxf)
             do iener=1,nbeng2
               xsa(iener)=dlog(bengy(iener))
               ysa(iener)=dlog(xsign(iener,nn))
               wts(iener)=1.0D0
             enddo
             ifail=0
             do iener=3,nbeng2-2
               xka(iener+2) = xsa(iener)
             enddo

             call xxnbaf(nbeng2,nbeng2+4,xsa,ysa,wts,xka,b,a,diag,
     &                   c,ss,ifail)
         endif

         if(n.gt.nmaxf)then
             do iener=1,nbeng2
               zsa(iener)=dlog(alphaa(iener))
             enddo
             ifail=0
             do iener=3,nbeng2-2
               zka(iener+2) = xsa(iener)
             enddo

             call xxnbaf(nbeng2,nbeng2+4,xsa,zsa,wts,zka,b,a,diag,
     &                   zc,ss,ifail)
         endif

         do iloop=1,nbeam
           vel=1.384d6*dsqrt(bmena(iloop))
           if(bmena(iloop).lt.bengy(1))then
               qcxn(iloop,i)=0.0d0
           elseif(bmena(iloop).gt.bengy(nbeng2))then
               qcxn(iloop,i)=0.0d0
           elseif(n.lt.nminf)then
               qcxn(iloop,i)=0.0d0
           else
               xval=dlog(bmena(iloop))
               ifail=0
               call xxnbbf(nbeng2+4, xka, c, xval, sval, ifail)
               qcxn(iloop,i)=vel*dexp(sval)
               if(n.gt.nmaxf)then
                   zval=dlog(bmena(iloop))
                   ifail=0
                   call xxnbbf(nbeng2+4, zka, zc, zval, sval, ifail)
                   alpha=dexp(sval)
                   qcxn(iloop,i)=qcxn(iloop,i)*
     &             (dfloat(nn)/dfloat(n))**alpha
               endif
           endif

         enddo

       enddo
c-----------------------------------------------------------------------
c  combine set of beam energies fractions
c-----------------------------------------------------------------------
       do i=1,inrep
         qthrep(i)=0.00d+00
         do iloop=1,nbeam
           vel=1.384d6*dsqrt(bmena(iloop))
           qthrep(i)=qthrep(i)+qcxn(iloop,i)*bmfra(iloop)
         enddo
       enddo

       return
c
c-----------------------------------------------------------------------
c
 1001 format(1x,31('*'),' cxqntb error ',30('*')//
     &       1x,'Incompatible internal parameters: ',a,i2,a,i2)
 1002 format(/1x,29('*'),' Program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c
       end
