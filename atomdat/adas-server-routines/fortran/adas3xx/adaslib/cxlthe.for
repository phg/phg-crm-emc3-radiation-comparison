CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxlthe.for,v 1.1 2004/07/06 13:10:28 whitefor Exp $ Date $Date: 2004/07/06 13:10:28 $
CX
      SUBROUTINE CXLTHE( IZ0 , ZEFF , N , L , E0 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: CXLTHE ********************
C
C  PURPOSE:  PROVIDES BINDING ENERGY OF TERM CENTRE FOR OUTER ELECTRON
C            IN LITHIUM LIKE IONS.
C
C            FROM EDLEN (1979) PHYSICA SCRIPTA, 19, 255.
C
C            FINE STRUCTURE FOR L>0 MUST BE ADDED EXTERNALLY.
C
C  CALLING PROGRAM: GENERAL USE
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE.
C  INPUT : (R*8)  ZEFF    = EFFECTIVE NUCLEAR CHARGE.
C  INPUT : (I*4)  N       = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  L       = ORBITAL QUANTUM NUMBER.
C
C  OUTPUT: (R*8)  E0      = BINDING ENERGY.
C                           UNITS: RYD
C
C          (R*8)  Z       = DUMMY ARGUMENT TO STATEMENT FUNCTIONS.
C          (R*8)  F       = DUMMY ARGUMENT TO STATEMENT FUNCTIONS.
C
C          (R*8)  Z0      = REAL VALUE = IZ0.
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XL      = REAL VALUE = L.
C          (R*8)  R       =
C          (R*8)  RC      =
C          (R*8)  T1      =
C          (R*8)  T2      =
C          (R*8)  T3      =
C          (R*8)  T4      =
C          (R*8)  A       =
C          (R*8)  B       =
C          (R*8)  C       =
C          (R*8)  T2S     =
C          (R*8)  D2S     =
C          (R*8)  T2P1    =
C          (R*8)  T2P2    =
C          (R*8)  T2P     =
C          (R*8)  D2P     =
C          (R*8)  U       =
C          (R*8)  U0      =
C          (R*8)  V       =
C          (R*8)  TNL     =
C          (R*8)  S       =
C          (R*8)  AK      =
C          (R*8)  DP      =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          R8P        ADAS
C          R8QP       ADAS
C          EL0        INTERNAL
C          SIG0       INTERNAL
C          DRS        INTERNAL
C          DRP        INTERNAL
C          DLS        INTERNAL
C          DLP        INTERNAL
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    02/11/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8    R8P     , R8QP
      REAL*8    EL0     , SIG0    , DRS     , DRP      , DLS     ,
     &          DLP
C-----------------------------------------------------------------------
      INTEGER   IZ0     , N       , L
C-----------------------------------------------------------------------
      REAL*8    ZEFF    , E0
      REAL*8    Z       , F
      REAL*8    Z0      , XN      , XL      , R       , RC      ,
     &          T1      , T2      , T3      , T4      , A       ,
     &          B       , C       , T2S     , D2S     , T2P1    ,
     &          T2P2    , T2P     , D2P     , U       , U0      ,
     &          V       , TNL     , S       , AK      , DP
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C STATEMENT FUNCTIONS
C-----------------------------------------------------------------------
C
      EL0( Z , F )  = 0.25D0 * ( Z**2 - 3.18244D0 * Z + 2.0038D0 +
     &                0.208015D0 / ( Z - 1.3833D0 ) ) - 6.3789D -
     &                6 * F * ( Z - 2.0D0 )**2
C
      SIG0( Z , F ) = 0.141441D0 * ( Z - 1.7025D0 - 0.768371D0 *
     &                (1.0D0 - 0.090333D0 / ( Z - 2.184D0 ) ) /
     &                ( Z - 0.975D0 ) ) - 1.9137D - 6 * F *
     &                ( Z - 2.0D0 )**2
C
      DRS( Z ) = 0.456536D0 * ( Z - 1.2808D0 )**4 + 1.2763D-5 *
     &           ( Z - 1.2808D0 )**6 + 4.34D-10 * ( Z - 1.2808D0 )**8
C
      DRP( Z ) = 0.21305D0 * ( Z - 2.241D0 )**4 + 0.466D-5 *
     &           ( Z - 2.241D0 )**6 + 1.48D-10 * ( Z - 2.241D0 )**8
C
      DLS( Z ) = 4.5246D-3 * ( Z - 1.6D0 )**4 * ( -2.179D0 - 2.0D0 *
     &           DLOG( 7.29729D-3 * ( Z - 1.6D0 ) ) + 5.26427D-2 *
     &           ( Z - 1.6D0 ) - 5.32504D-5 * ( Z - 1.6D0 )**2 *
     &           ( 3.0D0 * ( DLOG ( 7.29729D-3 * ( Z - 1.6D0 ) ) )**2 +
     &           8.695D0 * DLOG( 7.29729D-3 * ( Z - 1.6D0 ) ) +
     &           19.081D0 ) )
C
      DLP( Z ) = 4.525D-3 * ( Z - 2.0D0 )**4 * (3.0D-2 - 2.6412D-5 *
     &           ( Z - 2.0D0 )**2 * DLOG( 7.29729D-3 * ( Z - 2.0D0 ) ) )
C
C-----------------------------------------------------------------------
C
      Z0 = DFLOAT( IZ0 )
      XN = DFLOAT( N )
      XL = DFLOAT( L )
C
C-----------------------------------------------------------------------
C
      R  = 109737.318D0 - 60.200D0 / ( 2.00D0 * Z0 )
      RC = 109737.3D0 / R
C
      T1 = DRS( Z0 )
      T2 = DRP( Z0 )
      T3 = DLS( Z0 )
      T4 = DLP( Z0 )
C
C-----------------------------------------------------------------------
C
      IF (L .LE. 1) THEN
C
         IF (L .LT. 1) THEN
C
C-----------------------------------------------------------------------
C S STATES.
C-----------------------------------------------------------------------
C
            T2S = R * EL0 ( Z0 , RC ) + DRS( Z0 ) - DLS( Z0 )
            T2S = T2S / ( R * ( Z0 - 2.0D0 )**2 )
            D2S = 2.0D0 - 1.0D0 / DSQRT( T2S )
            C   = 0.0828D0 / Z0 - 0.2283D0 / Z0**2
            B   = -5.5D-4 * Z0 + 5.963D-3 + 0.19404D0 /
     &            ( Z0 - 0.36D0 ) - 0.3368D0 / ( Z0 - 0.36D0 )**2
            A   = D2S - B * T2S - C * T2S**2
C
C-----------------------------------------------------------------------
C
         ELSE
C
C-----------------------------------------------------------------------
C P STATES.
C-----------------------------------------------------------------------
C
            T2P1 = R * EL0( Z0 , RC ) + DRS( Z0 ) - DLS( Z0 )
            T2P2 = R * SIG0( Z0 , RC ) + DRS( Z0 ) - DRP( Z0 ) -
     &             DLS( Z0 ) + DLP( Z0 )
            T2P  = ( T2P1 - T2P2 ) / ( R * ( Z0 - 2.0D0 )**2 )
            D2P  = 2.0D0 - 1.0D0 / DSQRT( T2P )
            C    = -2.603D-2 / ( Z0 - 2.37D0 ) +1.326D-2 /
     &             ( Z0 - 2.37D0 )**2
            B    = -1.2D-3 * Z0 + 2.1237D-2 - 8.905D-2 /
     &             ( Z0 - 1.74D0 ) + 4.803D-2 / ( Z0 - 1.74D0 )**2
            A    = D2P - B * T2P - C * T2P**2
C
C-----------------------------------------------------------------------
C
         ENDIF
C
C-----------------------------------------------------------------------
C
         U  = 0.0D0
         U0 = 1.0D0
C
    1    CONTINUE
         IF ( DABS( U - U0 ) .GT. 1.0D-6) THEN
            U0  = U
            V   = XN - U0
            TNL = 1.0D0 / V**2
            U   = A + TNL * ( B + TNL * C )
            GOTO 1
         ENDIF
C
         E0 = ( ( Z0 - 2.0D0 ) / ( XN - U ) )**2
C
C-----------------------------------------------------------------------
C
      ELSE
C
C-----------------------------------------------------------------------
C L > 2 CASES.
C-----------------------------------------------------------------------
C
         S = 0.3397D0 + 0.102D0 / ( Z0 - 0.4D0 )
         A = 9.0D0 * ( ( Z0 - 2.0D0 ) / ( Z0 - S ) )**4
         AK = 0.2113D0 * Z0 + 0.598D0 - 2.4D0 / Z0
         DP = A * R8P( N , L ) + A * DSQRT( A ) * AK * R8QP( N , L )
         E0 = ( Z0 - 2.0D0 )**2 * ( 1.0D0 + 5.32504D-5 *
     &        ( Z0 - 2.0D0 )**2 * ( XN / ( XL + 0.5D0 ) - 0.75D0 ) /
     &        XN**2 ) / XN**2 + DP
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
