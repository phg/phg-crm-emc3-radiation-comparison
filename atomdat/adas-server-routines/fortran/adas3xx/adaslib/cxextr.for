CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxextr.for,v 1.3 2007/05/24 16:16:25 mog Exp $ Date $Date: 2007/05/24 16:16:25 $
CX
      SUBROUTINE CXEXTR( MXNENG , MXNSHL , NMINF  , NMAXF  ,
     &                   NENRGY , LPARMS , ALPHAA , LFORMA ,
     &                   XLCUTA , PL2A   , PL3A   , XSECNA ,
     &                   FRACLA
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXEXTR *********************
C
C  PURPOSE:  EXTRAPOLATES N AND L RESOLVED CROSS-SECTION BELOW AND ABOVE
C            DATA IN INPUT DATASET.
C
C            N AND L RESOLVED CROSS-SECTIONS BELOW THE INPUT DATA ARE
C            SET TO ZERO.
C
C            N RESOLVED CROSS-SECTIONS ABOVE THE INPUT DATA ARE
C            CALCULATED USING THE EXTRAPOLATION EXPONENT 'ALPHA'.
C
C            L-RESOLVED CROSS-SECTIONS ABOVE THE INPUT DATA ARE
C            CALCULATED USING THE FUNCTION R8FORM IF THE L-SPLITTING
C            PARAMTERS ARE PRESENT, OTHERWISE THE DISTRIBUTION FROM THE
C            INPUT DATA FOR THE MAXIMUM PRINCIPAL QUANTUM NUMBER IS
C            COPIED TO THE HIGHER LEVELS AS FOLLOWS:
C
C            SIGMA(N,L) = SIGMA(NMAXF,L)   FOR L = 0 TO NMAXF-1
C                       = 0                FOR L = NMAXF TO N-1
C
C            WHERE NMAXF IS THE MAXIMUM PRINCIPAL QUANTUM NUMBER IN THE
C            INPUT DATA.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES IN DATASET.
C  INPUT : (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C  INPUT : (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (I*4)  LFORMA()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: ENERGY INDEX
C
C  I/O   : (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C  I/O   : (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             EXPRESSED AS FRACTION OF CORRESPONDING
C                             N-RESOLVED CROSS-SECTION.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C  PARAM : (R*8)  ALPMIN    = MINIMUM VLUE OF EXTRPOLATION EXPONENT
C                             'ALPHA'.
C
C          (I*4)  IE        = ENERGY INDEX.
C          (I*4)  N         = PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L         = ORBITAL QUANTUM NUMBER.
C          (I*4)  IDL       = L-RESOLVED DATA INDEX.
C          (I*4)  IDL1      = L-RESOLVED DATA INDEX.
C          (I*4)  IDL2      = L-RESOLVED DATA INDEX.
C          (I*4)  ITYPE     = TYPE OF APPROX. TO USE IN FUNC R8FORM.
C
C          (R*8)  XNMAXF    = REAL VALUE = NMAXF.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C          R8FORM     ADAS      RETURNS L-RES X-SEC AS FRACTION OF N-RES
C                               X-SEC.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    20/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4IDFL
      REAL*8     R8FORM
C-----------------------------------------------------------------------
      REAL*8     ALPMIN  , CXMIN
      PARAMETER( ALPMIN = 1.0D0    , CXMIN = 1.0D-70   )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXNENG  , NMINF   , NMAXF   , NENRGY
      INTEGER    IE      , N       , L       , IDL     , IDL1    ,
     &           IDL2    , ITYPE
C-----------------------------------------------------------------------
      REAL*8     XNMAXF
C-----------------------------------------------------------------------
      LOGICAL    LPARMS
C-----------------------------------------------------------------------
      INTEGER    LFORMA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8     ALPHAA(MXNENG)  , XLCUTA(MXNENG)  , PL2A(MXNENG)    ,
     &           PL3A(MXNENG)
      REAL*8     XSECNA(MXNENG,MXNSHL)                 ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      XNMAXF = DFLOAT( NMAXF )
C
C-----------------------------------------------------------------------
C ZERO 'XSECNA' AND 'FRACLA' FOR N BELOW 'NMINF'.
C-----------------------------------------------------------------------
C
      DO 1 IE = 1 , NENRGY
C
         DO 2 N = 2 , NMINF-1
            XSECNA(IE,N) = 0.0D0
    2    CONTINUE
C
         DO 3 IDL = 1 , I4IDFL( NMINF-1 , NMINF-2 )
            FRACLA(IE,IDL) = 0.0D0
    3    CONTINUE
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C EXTRAPOLATE N-RESOLVED CROSS-SECTION FROM 'NMAXF' TO 'MXNSHL'.
C-----------------------------------------------------------------------
C
      DO 4 IE = 1 , NENRGY
C
         IF (ALPHAA(IE) .LE. ALPMIN) THEN
C
            DO 5 N = NMAXF+1 , MXNSHL
               XSECNA(IE,N) = 0.0D0
    5       CONTINUE
C
         ELSE
C
            DO 6 N = NMAXF+1 , MXNSHL
               XSECNA(IE,N) = XSECNA(IE,NMAXF) *
     &                        ( XNMAXF / DFLOAT( N ) )**ALPHAA(IE)
    6       CONTINUE
C
         ENDIF
C
    4 CONTINUE
C
C-----------------------------------------------------------------------
C FILL L-RESOLVED CROSS-SECTION FROM 'NMAXF' TO 'MXNSHL'.
C-----------------------------------------------------------------------
C
      IF (LPARMS) THEN
C
         DO 7 IE = 1 , NENRGY
C
            ITYPE = LFORMA(IE)
C
            DO 8 N = NMAXF+1 , MXNSHL
               DO 9 L = 0 , N-1
C
                  IDL = I4IDFL( N , L )
                  FRACLA(IE,IDL) = R8FORM( MXNENG , MXNSHL , N      ,
     &                                     L      , IE     , ITYPE  ,
     &                                     NENRGY , XLCUTA , PL2A   ,
     &                                     PL3A                       )
C
    9          CONTINUE
    8       CONTINUE
    7    CONTINUE
C
C-----------------------------------------------------------------------
C
      ELSE
C
         DO 10 IE = 1 , NENRGY
            DO 11 N = NMAXF+1 , MXNSHL
C
               DO 12 L = 0 , NMAXF-1
                  IDL1 = I4IDFL( N , L )
                  IDL2 = I4IDFL( NMAXF , L )
                  FRACLA(IE,IDL1) = FRACLA(IE,IDL2)
   12          CONTINUE
C
               DO 13 L = NMAXF , N-1
                  IDL = I4IDFL( N , L )
                  FRACLA(IE,IDL) = 0.0D0
   13          CONTINUE
C
   11       CONTINUE
   10    CONTINUE
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
