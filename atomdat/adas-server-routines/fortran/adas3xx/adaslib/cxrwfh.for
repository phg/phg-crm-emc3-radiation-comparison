CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxrwfh.for,v 1.1 2004/07/06 13:11:04 whitefor Exp $ Date $Date: 2004/07/06 13:11:04 $
CX
      SUBROUTINE CXRWFH( MXTERM , N      , L      ,
     &                   A      , NA     , IPA    , UA
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXRWFH *********************
C
C  PURPOSE:
C
C           POWER SERIES GENERATED HAS THE FORM:
C
C           X**IPA * EXP( UA * X ) * ( A(1) + X * A(2) + .... )
C                                     <------ NC TERMS ------>
C
C
C  CALLING PROGRAM: ADAS308.
C
C  INPUT : (I*4)  MXTERM  = MAXIMUM NUMBER OF TERMS.
C  INPUT : (I*4)  N       = N QUANTUM NUMBER.
C  INPUT : (I*4)  L       = L QUANTUM NUMBER.
C
C  OUTPUT: (I*4)  NA      = NUMBER OF TERMS IN POWER SERIES.
C  OUTPUT: (I*4)  IPA     = POWER OF LEADING TERM.
C  OUTPUT: (I*4)  UA      = POWER OF EXPONENT IN LEADING TERM.
C  OUTPUT: (R*8)  A()     = POWER SERIES COEFFICIENTS.
C
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  X       = REAL VALUE = 2/N.
C          (R*8)  T       =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4JGAM     ADAS      RETURNS VALUE FROM 'JGAM' TABLE.
C          R8GAM      ADAS      RETURNS VALUE FROM 'GAM' TABLE.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     01/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4JGAM
      REAL*8     R8GAM
C-----------------------------------------------------------------------
      INTEGER    MXTERM  , N       , L       , NA      , IPA
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     UA
      REAL*8     XN      , X       , T
C-----------------------------------------------------------------------
      REAL*8     A(MXTERM)
C-----------------------------------------------------------------------
C
      XN = DFLOAT( N )
      X  = 2.0D+00 / XN
C
      T  = R8GAM( N + L + 1 ) / ( XN**2 * R8GAM( N - L ) )
      T  = T**0.5D+00 * X**( L + 1 ) / R8GAM( 2 * L + 2 )
      T  = T * 8.0D+00**( I4JGAM( N + L + 1 ) - I4JGAM( N - L )
     &                    - 2 * I4JGAM( 2 * L + 2 ) )
C
      A(1) = T
      DO 1 I = 1 , N - L - 1
         T = DFLOAT( L + I - N ) * T * X
     &       / DFLOAT( I * ( 2 * L + I + 1 ) )
         A(I+1) = T
    1 CONTINUE
C
      UA  = -1.00D+00 / XN
      IPA = L + 1
      NA  = N - L
C
C-----------------------------------------------------------------------
C
      RETURN
      END
