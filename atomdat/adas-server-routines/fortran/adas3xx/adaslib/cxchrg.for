CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxchrg.for,v 1.1 2004/07/06 13:09:55 whitefor Exp $ Date $Date: 2004/07/06 13:09:55 $
CX
      SUBROUTINE CXCHRG( SYMBD , IZD   , SYMBR , IZR  , IDZ0  ,
     &                   IRZ0  , IRZ1  , IRZ2
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXCHRG *********************
C
C  PURPOSE:  SETS UP NUCLEAR CHARGE OF DONOR AND NULEAR, INITIAL AND
C            FINAL CHARGES OF RECEIVER.
C
C  CALLING PROGRAM: C6CHRG , ADAS308
C
C  INPUT : (C*2)  SYMBD   = DONOR ELEMENT SYMBOL.
C  INPUT : (I*4)  IZD     = DONOR ION CHARGE.
C  INPUT : (C*2)  SYMBR   = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IZR     = RECEIVER ION CHARGE.
C
C  OUTPUT: (I*4)  IDZ0    = DONOR NUCLEAR CHARGE.
C  OUTPUT: (I*4)  IRZ0    = RECEIVER NUCLEAR CHARGE.
C  OUTPUT: (I*4)  IRZ1    = RECEIVER ION INITIAL CHARGE.
C  OUTPUT: (I*4)  IRZ2    = RECEIVER ION FINAL CHARGE.
C
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           I4IEZ0      ADAS        RETURNS THE NUCLEAR CHARGE GIVEN THE
C                                   ELEMENT SYMBOL.
C           XXSTUC      ADAS        ENSURES ALL LETTERS IN STRING ARE
C                                   UPPER CASE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    11/11/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4EIZ0
C-----------------------------------------------------------------------
      INTEGER    IZD     , IZR     , IDZ0    , IRZ0    , IRZ1    ,
     &           IRZ2
C-----------------------------------------------------------------------
      CHARACTER  SYMBD*2  , SYMBR*2
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C ENSURE ELEMENT NAMES ARE UPPER CASE.
C-----------------------------------------------------------------------
C
      CALL XXSTUC( SYMBD )
      CALL XXSTUC( SYMBR )
C
C-----------------------------------------------------------------------
C SET UP CHARGES.
C-----------------------------------------------------------------------
C
      IDZ0 = I4EIZ0( SYMBD )
      IRZ0 = I4EIZ0( SYMBR )
      IRZ1 = IZR
      IRZ2 = IZR - 1
C
C-----------------------------------------------------------------------
C
      RETURN
      END
