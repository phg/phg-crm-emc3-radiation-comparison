CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxsode.for,v 1.1 2004/07/06 13:11:16 whitefor Exp $ Date $Date: 2004/07/06 13:11:16 $
CX
      SUBROUTINE CXSODE( IZ0 , N , L , ZEFF , E0 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: CXSODE ********************
C
C  PURPOSE:  PROVIDES BINDING ENERGY OF TERM CENTRE FOR OUTER ELECTRON
C            IN SODIUM LIKE IONS.
C
C            FROM EDLEN (1978) PHYSICA SCRIPTA, 17, 565.
C
C            FINE STRUCTURE FOR L>0 MUST BE ADDED EXTERNALLY.
C
C            THE ROUTINE IS INCOMPLETE - OPERATES FOR SELECTED IONS
C            ONLY.
C
C  CALLING PROGRAM: GENERAL USE
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE.
C  INPUT : (I*4)  N       = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  L       = ORBITAL QUANTUM NUMBER.
C
C  OUTPUT: (R*8)  ZEFF    = EFFECTIVE CHARGE FOR SPIN-ORBIT SEPAR.
C                           (= ION CHARGE + 1 USUALLY).
C  OUTPUT: (R*8)  E0      = BINDING ENERGY (+VE FOR BOUND STATES).
C                           UNITS: RYD
C
C  PARAM : (I*4)  MXQ     = MAXIMUM ION INDEX FOR QUANTUM DEFECTS DATA.
C  PARAM : (I*4)  MXT     =
C
C          (I*4)  IZ1     = IZ0-10.
C          (I*4)  K       = ARRAY INDEX.
C          (I*4)  IZ1     = LOOP INDEX.
C
C          (R*8)  Z0      = REAL VALUE = IZ0.
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XL      = REAL VALUE = L.
C          (R*8)  R       =
C          (R*8)  A       =
C          (R*8)  B       =
C          (R*8)  C       =
C          (R*8)  S       =
C          (R*8)  U       =
C          (R*8)  U0      =
C          (R*8)  V       =
C          (R*8)  TNL     =
C          (R*8)  AK      =
C          (R*8)  DP      =
C
C          (I*4)  ITAB()  = LOOK UP TABLE FOR QUANTUM DEFECTS DATA.
C                           = 0 : NO DATA PRESENT.
C                           = 1 : DATA PRESENT.
C                           DIMENSION: ION CHARGE INDEX ( = Z+1 ).
C
C          (R*8)  STAB(,) = QUANTUM DEFECTS FOR S.
C                           1ST DIMENSION: 3
C                           2ND DIMENSION: ION CHARGE INDEX ( = Z+1 ).
C          (R*8)  PTAB(,) = QUANTUM DEFECTS FOR P.
C                           1ST DIMENSION: 3
C                           2ND DIMENSION: ION CHARGE INDEX ( = Z+1 ).
C          (R*8)  DTAB(,) = QUANTUM DEFECTS FOR D.
C                           1ST DIMENSION: 3
C                           2ND DIMENSION: ION CHARGE INDEX ( = Z+1 ).
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          R8P        ADAS
C          R8QP       ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    03/11/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      REAL*8     R8P     , R8QP
C-----------------------------------------------------------------------
      INTEGER    MXQ       , MXT
      PARAMETER( MXQ = 40  , MXT = 3 )
C-----------------------------------------------------------------------
      INTEGER    IZ0     , N       , L
      INTEGER    IZ1     , K       , I
C-----------------------------------------------------------------------
      REAL*8     ZEFF    , E0
      REAL*8     Z0      , XN      , XL      , R       , A       ,
     &           B       , C       , S       , U       , U0      ,
     &           V       , TNL     , AK      , DP
C-----------------------------------------------------------------------
      INTEGER    ITAB(MXQ)
C-----------------------------------------------------------------------
      REAL*8     STAB(MXT,MXQ)  , PTAB(MXT,MXQ)  , DTAB(MXT,MXQ)
C-----------------------------------------------------------------------
      DATA       ITAB / 0 , 1 , 0 , 1 , 0 , 0 , 0 , 1 , 0 , 0 ,
     &                  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
     &                  0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 ,
     &                  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0   /
C
      DATA     ( STAB(K,2) , K=1,3 ) / 1.0673913D0   , 0.0986200D0  ,
     &                                 0.042497D0                     /
      DATA     ( PTAB(K,2) , K=1,3 ) / 0.696181166D0 , 0.16989453D0 ,
     &                                 0.1290323D0                    /
      DATA     ( DTAB(K,2) , K=1,3 ) / 0.0463705D0   , -0.1345277D0 ,
     &                                 -0.050059D0                    /
C
      DATA     ( STAB(K,4) , K=1,3 ) / 0.77453D0     , 0.12328D0    ,
     &                                 0.09113D0                      /
      DATA     ( PTAB(K,4) , K=1,3 ) / 0.51156D0     , 0.18504D0    ,
     &                                 0.27238D0                      /
      DATA     ( DTAB(K,4) , K=1,3 ) / 0.08976D0     , -0.16760D0   ,
     &                                 -0.45203D0                     /
C
      DATA     ( STAB(K,8) , K=1,3 ) / 0.39672D0     , 2.22848D0    ,
     &                                 -8.38311D0                     /
      DATA     ( PTAB(K,8) , K=1,3 ) / 0.34111D0     , 0.12764D0    ,
     &                                 0.48962D0                      /
      DATA     ( DTAB(K,8) , K=1,3 ) / 0.10462D0     , -0.22851D0   ,
     &                                 0.25215D0                      /
C
      DATA     ( STAB(K,26), K=1,3 ) / 0.23174D0     , -0.01696D0   ,
     &                                 0.47248D0                      /
      DATA     ( PTAB(K,26), K=1,3 ) / 0.12440D0     , 0.61191D0    ,
     &                                 -2.61692D0                     /
      DATA     ( DTAB(K,26), K=1,3 ) / 0.03454D0     , 0.54469D0    ,
     &                                -3.28797D0                      /
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      IZ1 = IZ0 - 10
      Z0  = DFLOAT( IZ0 )
      XN  = DFLOAT( N )
      XL  = DFLOAT( L )
C
C-----------------------------------------------------------------------
C CHECK DATA AVAILABLE FOR THIS ATOM.
C-----------------------------------------------------------------------
C
      IF (ITAB(IZ1) .EQ. 0) THEN
          ZEFF = DFLOAT( IZ1 )
          E0 = ( ( Z0 - 10.0D0 ) / XN )**2
          WRITE(I4UNIT(-1),1000) ZEFF , E0
          RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C
      R = 109737.318D0 - 60.200D0 / ( 2.00D0 * Z0 )
C
C-----------------------------------------------------------------------
C
      IF (L .LE. 2) THEN
C
         IF (L .EQ. 0) THEN
C
C-----------------------------------------------------------------------
C S-STATES
C-----------------------------------------------------------------------
C
            ZEFF = DFLOAT( IZ1 )
            A = STAB(1,IZ1)
            B = STAB(2,IZ1)
            C = STAB(3,IZ1)
C
C-----------------------------------------------------------------------
C
         ELSE IF (L .EQ. 1) THEN
C
C-----------------------------------------------------------------------
C P-STATES.
C-----------------------------------------------------------------------
C
            S = 4.7064D0
            DO 1 I = 1 , 6
               S = 4.7064D0 + 7.966D0 * ( 1.0D0 + 2.0D0 / ( Z0 - S ) ) /
     &             ( Z0 - S )
    1       CONTINUE
C
            ZEFF = Z0 - S + 0.956D - 5 * ( Z0 - S )**3
            A = PTAB(1,IZ1)
            B = PTAB(2,IZ1)
            C = PTAB(3,IZ1)
C
C-----------------------------------------------------------------------
C
         ELSE
C
C-----------------------------------------------------------------------
C D-STATES.
C-----------------------------------------------------------------------
C
            S = 8.133D0 + 16.96D0 / ( Z0 - 7.5D0 )
            ZEFF = Z0 - S + 0.23D-5 * ( Z0 - S )**3
            A = DTAB(1,IZ1)
            B = DTAB(2,IZ1)
            C = DTAB(3,IZ1)
C
C-----------------------------------------------------------------------
C
         ENDIF
C
C-----------------------------------------------------------------------
C
C
         U  = A
         U0 = A + 1.0D0
C
    2    CONTINUE
         IF ( DABS( U - U0 ) .GT. 1.0D-6) THEN
            U0  = U
            V   = XN - U0
            TNL = 1.0D0 / V**2
            U   = A + TNL * ( B + TNL * C )
            GOTO 2
         ENDIF
C
         E0 = ( ( Z0 - 10.0D0 ) / ( XN - U ) )**2
C
C-----------------------------------------------------------------------
C
      ELSE
C
C-----------------------------------------------------------------------
C L>2 CASES.
C-----------------------------------------------------------------------
C
         ZEFF = DFLOAT( IZ1 )
         S = 5.6806D0 + 1.798D0 / ( Z0 - 5.6806D0 )
C
         DO 3 I = 1 , 6
            S = 5.6806D0 + 1.798D0 / ( Z0 - S )
    3    CONTINUE
C
         A  = ( Z0 - 10.0D0 )**4 * ( 9.0D0 / ( Z0 - 0.4D0 )**4 +
     &        324.0D0 / ( Z0 - 2.4D0 )**4 + 566.0D0 / ( Z0 - S )**4 )
         AK = 0.3568D0 * ( Z0 - 10.0D0 ) + 3.8359D0 - 10.5D0 /
     &        ( Z0 - 8.0D0 )
         DP = A * R8P( N , L ) + A * DSQRT( A ) * AK * R8QP( N , L )
         E0 = ( Z0 - 10.0D0 )**2 * ( 1.0D0 + 5.32504D-5 *
     &        ( Z0 - 10.0D0) **2 * ( XN / ( XL + 0.5D0 ) - 0.75D0 ) /
     &        XN**2 ) / XN**2 + DP
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 31('*'), ' CXSODE WARNING ', 31('*') //
     &        2X, 'ENERGY DATA NOT AVAILABLE FOR THIS NA-LIKE ION.'/
     &        2X, 'USING: ZEFF = ', 1P, D8.2, '  E0 = ', D8.2        )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
