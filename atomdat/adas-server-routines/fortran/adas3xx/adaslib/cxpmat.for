CX  SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxpmat.for,v 1.2 2004/07/06 13:10:40 whitefor Exp $ Date $Date: 2004/07/06 13:10:40 $
CX    
	SUBROUTINE  CXPMAT(PRMAT,NRES,MAXTE,MAXNE,MAXEB,EBRAY,TERAY,
     &			   NERAY,NB,RCMAT,TEREF, NEREF,EBREF,ITREF,
     &			   INREF,IEREF,IUNIT,INFILE,INA,ITA,IEA,
     &			   NBENG,NTEMP,NDENS,NLEV)
C----------------------------------------------------------------------
C
C	********** FORTRAN 77 ROUTINE : CXPMAT.FOR **********
C
C   PURPOSE : TO INTERROGATE AND EXTRACT THE  PROJECTION MATRICES
C	      WHICH ARE GENERATED USING ADAS311
C
C   INPUT   : 
C
C
C	    (I*4)       INUNIT		: STREAM NUMBER FOR ACCESSING
C					  INPUT FILE.
C	    (I*4)	NBENG		: MAXIMUM NUMBER OF ENERGIES.
C	    (I*4)	NDENS		: MAXIMUM NUMBER OF DENSITIES.
C	    (I*4)	NTEMP		: MAXIMUM NUMBER OF TEMPS.
C	    (I*4)	NLEV		: MAXIMUM NUMBER OF LEVELS
C					  CONTAINED IN THE PROJECTION
C					  AND RECOMBINATION MATRICES.
C	    (CHR)	INFILE		: INPUT FILENAME TO BE 
C					  INTERROGATED.
C
C   OUTPUT  :  
C
C	    (R*8)	PRMAT()		: PROJECTION MATRIX.
C					  1ST DIM.: LEVEL INDEX.
C					  2ND DIM.: LEVEL INDEX.
C					  3RD DIM.: ENERGY INDEX.
C					  4TH DIM.: DENSITY INDEX.
C					  5TH DIM.: TEMPERATURE INDEX.
C	    (R*8)	RCMAT()		: COLLISIONAL-RADIATIVE
C					  RECOMBINATION COEFFICIENTS.
C					  1ST DIM.: LEVEL INDEX.
C					  2ND DIM.: ENERGY INDEX.
C					  3RD DIM.: DENSITY INDEX.
C					  4TH DIM.: TEMPERATURE INDEX.
C	    (R*8)	EBRAY()		: ARRAY CONTAINING THE NEUTRAL
C				          BEAM ENERGY (eV/amu).
C					  1ST DIM.: ENERGY INDEX.
C	    (R*8)	TERAY()		: ARRAY CONTAINING THE
C					  TEMPERATURE (eV).
C					  1ST DIM.: TEMPERATURE INDEX.
C	    (R*8)	NERAY()		: ARRAY CONTAINING THE ELECTRON
C					  DENSITY.
C					  1ST DIM.: DENSITY INDEX.
C	    (R*8)	TEREF		: REFERENCE TEMPERATURE (eV).
C	    (R*8)	NEREF		: REFERENCE ELECTRON DENSITY (cm-3).
C	    (R*8)	EBREF		: REFERENCE BEAM ENERGY (eV amu-1).
C	    (R*8)	NB		: NEUTRAL BEAM DENSITY (cm-3)
C
C	    (I*4)	NRES		: NUMBER OF RESOLVED LEVELS
C					  CONTAINED IN THE COLLISIONAL
C					  RADIATIVE MATRIX.
C	    (I*4)	MAXTE		: NUMBER OF TEMPERATURES CONTAINED
C					  IN THE INPUT FILE.
C	    (I*4)	MAXNE		: NUMBER OF DENSITIES CONTAINED 
C					  IN THE INPUT FILE.
C	    (I*4)	MAXEB		: NUMBER OF BEAM ENERGIES CONTAINED
C					  IN THE INPUT FILE.
C	    (I*4)	ITREF		: INDEX FOR REFERENCE TEMPERATURE.
C					  TO BE USED INCONJUCTION WITH THE
C					  ARRAY ITA().
C	    (I*4)	INREF		: INDEX FOR REFERENCE DENSITY.
C					  TO BE USED INCONJUCTION WITH THE
C					  ARRAY INA().
C	    (I*4)	IEREF		: INDEX FOR REFERENCE BEAM ENERGY.
C					  TO BE USED INCONJUCTION WITH THE
C					  ARRAY IEA().
C	    (I*4)	INA()		: ARRAY CONTAINING THE DENSITY
C					  INDEX.
C	    (I*4)	IEA()		: ARRAY CONTAINING THE ENERGY
C					  INDEX.
C	    (I*4)	ITA()		: ARRAY CONTAINING THE 
C					  TEMPERATURE INDEX.
C
C  GENERAL  :
C
C	    (R*8)	EB		: NEUTRAL BEAM ENERGY (eV/AMU).
C	    (R*8)	TE		: ELECTRON TEMPERATURE (eV).
C	    (R*8)	TP		: ION TEMPERATURE (K).
C	    (R*8)	NE		: ELECTRON DENSITY (cm-3).
C	    (R*8)	NP		: ION DENSITY (cm-3).
C
C	    (I*4)	NTOTAL		: COUNTER USED TO DETERMINE HOW
C					  MANY TABULATED TABLES TO BE
C					  READ FROM INPUT FILE.
C	    (I*4) 	I		: GENERAL COUNTER.
C	    (I*4)       J		: GENERAL COUNTER.
C	    (I*4)	K		: GENERAL COUNTER.
C	    (I*4)	IN		: GENERAL INDEX.
C	    (I*4)	IT		: GENERAL INDEX.
C	    (I*4)	IE		: GENERAL INDEX.
C	    (I*4)	ITCOUNT		: NUMBER OF TEMPERATURES.
C	    (I*4)	INCOUNT		: NUMBER OF DENSITIES.
C	    (I*4)	IECOUNT	        : NUMBER OF BEAM ENERGIES.
C	    (I*4)	ICOUNT		: GENERAL COUNTER.
C	    (CHR)	LINE		: GENERAL VARIABLE.
C	
C
C
C
C   ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          CCFIND     ADAS    IDENTIFIED REPEATED VALUES IN ARRAY.
C	   CCSORT     ADAS    SORTS ARRAY VALUES INTO INCREASING ORDER.   
C	   CCFILL     ADAS    FILLS ARRAY WITH CORRESPONDING INDEX.
C 
C
C
C   AUTHOR  : HARVEY ANDERSON
C	      UNIVERSITY OF STRATHCLYDE
C	      ANDERSON@PHYS.STRATH.AC.UK
C
C   DATE    : 30/8/99
C
C  VERSION  : 1.2                          
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane  
C             Declaration of variables is not standard. NTEMP is 
C             defined after it is first used to dimension a 
C             variable length input array. Reconfigure variable 
C             definitions to follow ADAS conventions.
C
C----------------------------------------------------------------------
      INTEGER	NRES	, MAXTE   , MAXNE   , MAXEB ,       
     &		I	, J       , NTOTAL  , K     ,       
     &		IUNIT	, IE      , IN              , IT    , 
     &		ITCOUNT	, ITREF   , INREF   , IEREF ,       
     &		INCOUNT , IECOUNT , 
     &		NBENG   , NTEMP   , NDENS   , NLEV
C----------------------------------------------------------------------
      REAL*8    TE      ,   EB    , NE	    , 
     &	        TP	,   NP	  , NB      , TEREF , 
     &		EBREF	,   NEREF 
C----------------------------------------------------------------------
      CHARACTER LINE*132, INFILE*80
C----------------------------------------------------------------------
      INTEGER	IEA(NBENG)        , ITA(NTEMP)      ,
     &		INA(NDENS)
C----------------------------------------------------------------------
      REAL*8  PRMAT(NLEV,NLEV,NBENG,NDENS,NTEMP)    , EBRAY(NBENG) ,
     &	      RCMAT(NLEV,NBENG,NDENS,NTEMP)	    , TERAY(NTEMP) ,
     &	      NERAY(NDENS)
C----------------------------------------------------------------------

   	OPEN(UNIT=IUNIT,FILE=INFILE,STATUS='UNKNOWN')

   	READ(IUNIT,10) NRES , MAXEB, MAXTE, MAXNE

           ICOUNT = NRES / 2
	   IF(MOD(NRES,2) .EQ. 0.0) THEN
	     DO I=1,ICOUNT+3
		READ(IUNIT,'(A)')  LINE
	     ENDDO
	   ELSE
	     DO I=1,ICOUNT+4
		READ(IUNIT,'(A)') LINE
	     ENDDO
	   ENDIF
C
	 NTOTAL=0
 	  DO J=1,MAXEB
	   DO K=1,MAXNE
	     NTOTAL = NTOTAL+1
	   ENDDO
	  ENDDO
	   DO I=1,MAXTE
	     NTOTAL=NTOTAL+1
	   ENDDO
  
	 ITCOUNT=0
         INCOUNT=0
	 IECOUNT=0
	 IT=0
         IN=0
	 IE=0

	 DO K =1, NTOTAL

            READ(IUNIT,'(A)') LINE
	    READ(IUNIT,'(A)') LINE
	    READ(IUNIT,20)    EB , TE , TP
	    READ(IUNIT,20)    NE , NP , NB
	    READ(IUNIT,'(A)') LINE
	    READ(IUNIT,'(A)') LINE

	    TE = TE / (1.16*10**4)
	    CALL CCFIND(TERAY,TE,ITCOUNT,IT)
	    CALL CCFIND(NERAY,NE,INCOUNT,IN)
	    CALL CCFIND(EBRAY,EB,IECOUNT,IE)
             DO I=1,NRES
	        READ(IUNIT,30) (PRMAT(I,J,IE,IN,IT), J=1,NRES)	
             ENDDO

	    READ(IUNIT,'(A)') LINE
	    READ(IUNIT,30) (RCMAT(I,IE,IN,IT),I=1,NRES)


            IF( ITCOUNT .EQ. 1 ) THEN
	      TEREF = TE
	      NEREF = NE
	      EBREF = EB
	    ELSE IF (ITCOUNT .EQ. 2) THEN
	      NEREF = NE
	      EBREF = EB
	    ENDIF
	
	  ENDDO

C
C--------------------------------------------------------------------  
C                                                                    
C        SORTS EBRAY,NERAY AND TERAY IN INCREASING ORDER                
C                                                                      
C-------------------------------------------------------------------
C
          CALL CCFILL(IEA,IECOUNT)                                      
          CALL CCSORT(EBRAY,IEA,IECOUNT)                                  
          CALL CCFILL(INA,INCOUNT)                                      
          CALL CCSORT(NERAY,INA,INCOUNT)                                 
          CALL CCFILL(ITA,ITCOUNT) 
          CALL CCSORT(TERAY,ITA,ITCOUNT) 

C
C--------------------------------------------------------------------  
C                                                                    
C        FINDS REFERENCE VALUE - IT SHOULD BE NOTED THAT THE
C	 REFERENCE INDICES ARE USED IN CONJUCTION WITH THE
C	 ARRAYS INA(),IEA() AND ITA(). THE ARE EMPLOYED WHEN
C	 REFERENCING THE PROJECTION AND RECOMBINATION MATRIX.
C	 THEY DO NOT CORRESPOND TO THE REFERENCE VALUES STORED
C	 IN THE ARRAYS TERAY(),EBRAY() AND NERAYA().               
C                                                                      
C-------------------------------------------------------------------
C
      ITREF = 0
      DO I=1,ITCOUNT
        IF (TERAY(I).EQ.TEREF) ITREF = ITA(I)
      END DO
      IF (ITREF.EQ.0) STOP 'REFERENCE TEMPERATURE NOT IN DATA!'
      IEREF = 0
      DO I=1,IECOUNT
        IF (EBRAY(I).EQ.EBREF) IEREF = IEA(I)
      END DO
      IF (IEREF.EQ.0) STOP 'REFERENCE BEAM ENERGY NOT IN DATA!'
      INREF = 0
      DO I=1,INCOUNT
        IF (NERAY(I).EQ.NEREF) INREF = INA(I)
      END DO
      IF (INREF.EQ.0) STOP 'REFERENCE DENSITY NOT IN DATA!' 
 

C
C-------------------------------------------------------------------
C

  10  FORMAT(12X,I4,8X,I4,8X,I4,8X,I4)
  20  FORMAT(13X,1PE9.3,17X, 1PE9.3,13X,1PE9.3)
  30  FORMAT(5(1PE17.10,1X))

      END
