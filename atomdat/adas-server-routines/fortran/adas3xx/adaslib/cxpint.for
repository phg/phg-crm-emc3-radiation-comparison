CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxpint.for,v 1.1 2004/07/06 13:10:35 whitefor Exp $ Date $Date: 2004/07/06 13:10:35 $
CX
      SUBROUTINE CXPINT( MXTERM ,
     &                   C      , NC     , IPC    , UC     ,
     &                   D      , ND     , IPD    , UD
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXPINT *********************
C
C  PURPOSE: INTEGRATES POWER SERIES OF THE FORM:
C
C           X**IPC * EXP( UC * X ) * ( C(1) + X * C(2) + .... )
C                                     <------ NC TERMS ------>
C
C           TO GIVE AN ANSWER OF THE FORM:
C
C           -X**IPD * EXP( UD * X ) * ( D(1) + X * D(2) + .... )
C                                      <------ ND TERMS ------>
C
C           NOTE SIGN OF OUTPUT.
C
C  CALLING PROGRAM: ADAS308.
C
C  INPUT : (I*4)  MXTERM  = MAXIMUM NUMBER OF TERMS.
C  INPUT : (I*4)  NC      = NUMBER OF TERMS IN POWER SERIES.
C  INPUT : (I*4)  IPC     = POWER OF LEADING TERM.
C  INPUT : (R*8)  UC      = POWER OF EXPONENT.
C  INPUT : (R*8)  C()     = POWER SERIES COEFFICIENTS.
C
C  OUTPUT: (I*4)  ND      = NUMBER OF TERMS IN POWER SERIES.
C  OUTPUT: (I*4)  IPD     = POWER OF LEADING TERM.
C  OUTPUT: (R*8)  UD      = POWER OF EXPONENT.
C  OUTPUT: (R*8)  D()     = POWER SERIES COEFFICIENTS.
C
C  PARAM : (R*8)  P1      = 1.0D+12
C
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  T       =
C          (R*8)  T1      =
C
C ROUTINES: NONE
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     01/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 1.0D+12 )
C-----------------------------------------------------------------------
      INTEGER    MXTERM  , NC      , IPC     , ND      , IPD
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     UC      , UD
      REAL*8     T       , T1
C-----------------------------------------------------------------------
      REAL*8     C(MXTERM)  , D(MXTERM)
C-----------------------------------------------------------------------
C
      UD = UC
      IF (UC .EQ. 0) THEN
C
         IPD = IPC + 1
         ND  = NC
         DO 1 I = 1 , NC
            D(I) = -C(I) / DFLOAT( I )
    1    CONTINUE
C
      ELSE
C
         ND  = NC + IPC
         IPD = 0
         T   = 0.0D+00
C
         DO 2 I = NC , 1 , -1
            T1 = T
            T  = C(I) - T * DFLOAT( I + IPC ) / UC
            IF ((P1 * DABS( T )) .LE. DABS( T1 )) T = 0.0D+00
    2    CONTINUE
C
         IF (IPC .GE. 0) THEN
C
            DO 3 I = IPC , 1 , -1
               T = -T * DFLOAT( I ) / UC
    3       CONTINUE
C
            D(1) = -T / UC
            DO 4 I = 1 , IPC
               D(I+1) = -UC * D(I) / DFLOAT( I )
    4       CONTINUE
C
            IF (NC .GT. 1) THEN
               DO 5 I = IPC + 1 , IPC + NC - 1
                  D(I+1) = ( -UC * D(I) - C(I-IPC) ) / DFLOAT( I )
    5          CONTINUE
            ENDIF
C
         ENDIF
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
