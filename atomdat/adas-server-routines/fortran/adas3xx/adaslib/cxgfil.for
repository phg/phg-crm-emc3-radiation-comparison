CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxgfil.for,v 1.1 2004/07/06 13:10:19 whitefor Exp $ Date $Date: 2004/07/06 13:10:19 $
CX
      SUBROUTINE CXGFIL( MXA    , MXB    , IZ0    , AMSSNO ,
     &                   N      , N1     , TIEV   , NDIV   ,
     &                   ANGDIV , NPTS   , CEMIS  , WAVLN  ,
     &                   CWLN   , XA     , YA     , XB     ,
     &                   YB
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXGFIL *********************
C
C  PURPOSE: FILLS ADAS306 AND 308 GRAPH ARRAYS.
C
C  CALLING PROGRAM: C6OUTG ; C8OUTG.
C
C  INPUT : (I*4)  MXA       = MAXIMUM NUMBER OF POINTS IN GRAPH A.
C  INPUT : (I*4)  MXB       = MAXIMUM NUMBER OF POINTS IN GRAPH B.
C  INPUT : (I*4)  IZ0       = TARGET NUCLEAR CHARGE.
C  INPUT : (R*8)  AMSSNO    = ATOMIC MASS NUMBER OF TARGET.
C  INPUT : (I*4)  N         = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  N1        = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: A
C  INPUT : (I*4)  NDIV      = NUMBER OF DIVISIONS ON X AXIS OF PLOT.
C  INPUT : (R*8)  ANGDIV    = NO. OF ANGSTROMS PER DIVISION.
C  INPUT : (I*4)  NPTS      = NUMBER OF POINTS OF DATA.
C  INPUT : (R*8)  CEMIS()   = COLUMN EMISSIVITIES.
C                             UNITS: PHOT CM-2 SEC-1
C                             DIMENSION: MXA
C  INPUT : (R*8)  WAVLN()   = WAVELENGTHS.
C                             UNITS: A
C                             DIMENSION: MXA
C
C  OUTPUT  (R*8)  CWLN      = CENTRAL WAVELENGTH ON GRAPH.
C                             UNITS: A
C  OUTPUT: (R*8)  XA()      = X DATA POINTS FOR GRAPH A.
C                             DIMENSION: MXA
C  OUTPUT: (R*8)  YA()      = Y DATA POINTS FOR GRAPH A.
C                             DIMENSION: MXA
C  OUTPUT: (R*8)  XB()      = X DATA POINTS FOR GRAPH B.
C                             DIMENSION: MXB
C  OUTPUT: (R*8)  YB()      = Y DATA POINTS FOR GRAPH B.
C                             DIMENSION: MXB
C
C          (I*4)  MIDDIV    = CENTRAL DIVISION ON GRAPH.
C          (I*4)  I         = LOOP INDEX.
C          (I*4)  J         = LOOP INDEX.
C
C          (R*8)  Z0        = REAL VALUE = IZ0.
C          (R*8)  XN        = REAL VALUE = N.
C          (R*8)  XN1       = REAL VALUE = N1.
C          (R*8)  G         =
C
C          (R*8)  MINVAL    = MINIMUM VALUE BELOW WHICH YB IS SET TO ZERO
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    22/11/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXA     , MXB     , IZ0     , N       , N1      ,
     &           NDIV    , NPTS
      INTEGER    MIDDIV  , I       , J
C-----------------------------------------------------------------------
      REAL*8     AMSSNO  , TIEV    , ANGDIV  , CWLN
      REAL*8     Z0      , XN      , XN1     , G
C-----------------------------------------------------------------------
      REAL*8     CEMIS(MXA)  , WAVLN(MXA)  , XA(MXA)     ,
     &           YA(MXA)     , XB(MXB)     , YB(MXB)
C-----------------------------------------------------------------------
      REAL*8     X , Y
      REAL*8     MINVAL
      PARAMETER (MINVAL = 1.0D-37)
C
C-----------------------------------------------------------------------
C
      Z0 = DFLOAT( IZ0 )
      XN  = DFLOAT( N )
      XN1 = DFLOAT( N1 )
C
      MIDDIV = NDIV / 2
      CWLN = 9.1127D2 / ( Z0**2 * ( 1.0D0 / XN1**2 - 1.0D0 / XN**2 ) )
      G = 4.635D-5 * SQRT( TIEV / AMSSNO )
C
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , NPTS
         XA(I) = WAVLN(I)
         YA(I) = CEMIS(I)
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      DO 2 I = 1 , NDIV
         YB(I) = 0.0D0
         XB(I) = CWLN + DFLOAT( I - MIDDIV ) * ANGDIV
         DO 3 J = 1 , NPTS
            YB(I) = YB(I) + 5.642D-1 * CEMIS(J) *
     &              EXP( -( ( XB(I) - XA(J) ) /
     &              ( ( XA(J) + MIDDIV * ANGDIV ) * G ) )**2 ) /
     &              ( G * ( XA(J) + MIDDIV * ANGDIV ) )
    3    CONTINUE
      IF (YB(I).LT.MINVAL) YB(I)=0.0
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C
      RETURN
      END
