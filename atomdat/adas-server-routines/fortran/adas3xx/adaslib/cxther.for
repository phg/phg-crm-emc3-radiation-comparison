C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxther.for,v 1.1 2004/07/06 13:11:23 whitefor Exp $ DATE $Date: 2004/07/06 13:11:23 $
C
       SUBROUTINE CXTHER ( NEDIM  , NTDIM  ,
     &                     LSETX  , LPASS  ,
     &                     AMDON  , AMREC  , 
     &                     ALPH   , ETH    , ILTYP  ,
     &                     NENIN  , ENIN   , NENOUT , ENOUT ,
     &                     SGIN   , RCOUT
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************** FORTRAN77 SUBROUTINE: CXTHER *****************
C
C  VERSION: 1.0 (ADAS91)
C
C  PURPOSE:  OBTAINS RATE COEFFICIENTS FOR DONOR/RECEIVER CHARGE
C            EXCHANGE COLLISIONS FOR CASES OF THERMAL DONOR AND
C            THERMAL RECEIVER FROM CROSS-SECTION TABULATIONS. AN ARRAY
C            OF VALUES IS PRODUCED.
C
C  CALLING PROGRAM:  ADAS302
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NEDIM    = MAX. NUMBER OF ENERGIES IN SOURCE DATA
C                             VECTOR
C  INPUT :  (I*4)  NTDIM    = MAX. NUMBER OF TEMPERATURES IN OUTPUT
C                             VECTOR
C  INPUT :  (L*4)  LSETX    = .TRUE. => SPLINE PRESET FOR THESE KNOTS
C                             .FLSE. => SPLINE NOT SET FOR THESE KNOTS
C  INPUT :  (L*4)  LPASS    = .TRUE. => DO NOT CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C                             .FLSE. => CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C  INPUT :  (R*8)  AMDON    = DONOR MASS NUMBER
C  INPUT :  (R*8)  AMREC    = RECEIVER MASS NUMBER
C  INPUT :  (R*8)  ALPH     = HIGH ENERGY EXTENSION PARAMETER
C  INPUT :  (R*8)  ETH      = THRESHOLD ENERGY (RYD.)
C  INPUT : (I*4)   ILTYP    = TYPE FOR LOW AND HIGH ENERGY CROSS-
C                             SECTION EXTRAPOLATION
C
C  INPUT : (I*4)   NENIN    = NUMBER OF ENERGIES IN INPUT DATA SET
C  INPUT : (R*8)   ENIN()   = ENERGIES (EV/AMU) IN INPUT DATA SET
C  INPUT : (I*4)   NENOUT   = NUMBER OF TEMPERATURES FOR OUTPUT DATA SET
C  INPUT : (R*8)   ENOUT()  = TEMPERATURES (EV) FOR OUTPUT DATA SET
C  INPUT : (R*8)   SGIN()   = INPUT X-SECTIONS (CM2) FROM INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (R*8)   RCOUT(,) = RATE COEFF. (CM3 S-1) IN OUTPUT DATA SET
C                             1ST.DIM: DONOR TEMPERATURE INDEX
C                             2ND.DIM: RECEIVER TEMPERATURE INDEX
C
C          (I*4)   I        = GENERAL INDEX
C          (I*4)   IT       = GENERAL INDEX
C          (I*4)   ITR      = GENERAL INDEX
C          (I*4)   ITD      = GENERAL INDEX
C          (I*4)   ITHETA   = GENERAL INDEX
C          (I*4)   IOPT     = SPLINE END POINT CURVATURE/GRADIENT OPTION
C                             1 => DDY1 = 0, DDYN = 0
C                             4 => DY1 = 0 , DDYN = 0
C          (I*4)   IXD      = DONOR GAUSSIAN QUADRATURE INDEX
C          (I*4)   IXR      = RECEIVER GAUSSIAN QUADRATURE INDEX
C          (I*4)   NGS      = GAUSSIAN QUARATURE DIMENSION
C          (I*4)   NTHETA   = NUMBER OF ANGLE VALUES FOR QUADRATURE
C          (I*4)   LTHETA   = NTHETA+1
C          (I*4)   L1       = PARAMETER = 1
C
C          (R*8)   ETHD     = THERMAL ENERGY OF DONOR       (JOULES)
C          (R*8)   ETHR     = THERMAL ENERGY RECEIVER       (JOULES)
C          (R*8)   HSIMP    = SIMPSON'S RULE STEP INTERVAL
C          (R*8)   THETA    = ANGLE BETWEEN PARTICLE VELOCITIES (RAD)
C          (R*8)   FAC      = GENERAL VARIABLE
C          (R*8)   FLAG     = GENERAL VARIABLE
C          (R*8)   XMDKG    = DONOR MASS    (KG)
C          (R*8)   XMRKG    = RECEIVER MASS (KG)
C          (R*8)   VD       = DONOR SPEED    (M S-1)
C          (R*8)   VR       = RECEIVER SPEED (M S-1)
C          (R*8)   RATE     = EVALUATED RATE COEFFICIENT (CM3 S-1)
C          (R*8)   PART1    = GENERAL VARIABLE
C          (R*8)   PART2    = GENERAL VARIABLE
C          (R*8)   PART3    = GENERAL VARIABLE
C          (R*8)   PART12   = GENERAL VARIABLE
C          (R*8)   PART23   = GENERAL VARIABLE
C          (R*8)   PART123  = GENERAL VARIABLE
C          (R*8)   VREL1    = GENERAL RELATIVE SPEED VARIABLE
C          (R*8)   XSEC1    = GENERAL CROSS-SECTION VARIABLE
C          (R*8)   VAL      = GENERAL VARIABLE
C
C          (R*8)   XGS()    = GAUSSIAN QUADRATURE NODES
C          (R*8)   WGS()    = GAUSSIAN QUADRATURE WEIGHTS
C          (R*8)   VREL()   = RELATIVE SPEED OF PARTICLES FOR DIFFERENT
C                             ANGLES (CM S-1)
C          (R*8)   XSEC()   = CHARGE EXCHANGE CROSS-SECTIONS FOR
C                             RELATIVE SPEEDS AT DIFFERENT ANGLES (CM2)
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          SIGCX      ADAS      INTERPOLATES CX CROSS-SECTION TABLES
C
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    17/11/95
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
      INTEGER     NGS           , NTHETA        , LTHETA
      INTEGER     IOPT          , L1
C-----------------------------------------------------------------------
      REAL*8      CMSAMU   , PI
C-----------------------------------------------------------------------
      PARAMETER ( NGS = 8  , NTHETA = 180  , LTHETA =NTHETA+1  )
      PARAMETER ( IOPT = 4 , L1 = 1                            )
      PARAMETER ( CMSAMU = 1.389209D6 , PI = 3.141592654       )
C-----------------------------------------------------------------------
      INTEGER     NEDIM         , NTDIM
      INTEGER     NENIN         , NENOUT
      INTEGER     IXD           , IXR           , I           ,
     &            ITHETA        , ITR           , ITD
      INTEGER     ILTYP         , I4UNIT
C-----------------------------------------------------------------------
      REAL*8      AMDON         , AMREC         , ALPH        ,
     &            ETH
      REAL*8      ETHD          , ETHR          , HSIMP
      REAL*8      RATE
      REAL*8      THETA         , FAC           , FLAG
      REAL*8      XMDKG         , XMRKG         , VD          , VR
      REAL*8      PART1         , PART2         , PART3
      REAL*8      PART23        , PART123
      REAL*8      VAL
C-----------------------------------------------------------------------
      REAL*8      XGS(NGS)      , WGS(NGS)
      REAL*8      ENIN(NEDIM)   , SGIN(NEDIM)   ,
     &            ENOUT(NTDIM)  , RCOUT(NTDIM,NTDIM)
      REAL*8      VREL(LTHETA)  , XSEC(LTHETA)
C-----------------------------------------------------------------------
      LOGICAL     LSETX         , LPASS
C-----------------------------------------------------------------------
      DATA XGS/  0.170279632305   ,  0.903701776799  ,  2.251086629866,
     &           4.266700170288   ,  7.045905402393  , 10.758516010181,
     &          15.740678641278   , 22.863131736889   /
      DATA WGS/  3.69188589342D-01,  4.18786780814D-01,
     &           1.75794986637D-01,  3.33434922612D-02,
     &           2.79453623523D-03,  9.07650877336D-05,
     &           8.48574671627D-07,  1.04800117487D-09 /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C      CHANGE UNITS OF INPUT VARIABLES
C-----------------------------------------------------------------------
C
       XMDKG  = 1.6606D-27 * AMDON
       XMRKG  = 1.6606D-27 * AMREC
C
       DO 100 ITD = 1,NENOUT
         DO 50 ITR = 1, NENOUT
           RATE    = 0.00D+00
C
C-----------------------------------------------------------------------
C  MAXWELLIAN DONOR  -  MAXWELLIAN RECEIVER
C-----------------------------------------------------------------------
C
           ETHD   = 1.6022D-19 * ENOUT(ITD)
           ETHR   = 1.6022D-19 * ENOUT(ITR)
C
           PART1   = 0.00D+00
           PART123 = 0.00D+00
C
C-----------------------------------------------------------------------
C  USE GAUSSIAN QUADRATURE TO INTEGRATE OVER FIRST DISTRIBUTION FUNCTION
C-----------------------------------------------------------------------
C
           DO 16 IXD = 1,NGS
             VD    = DSQRT(2.0 * ETHD * XGS(IXD) / XMDKG)
             PART1 = 4.0*PI *(1/(2.0*PI))*WGS(IXD)*DSQRT(XGS(IXD) / PI)
C
C-----------------------------------------------------------------------
C  USE GAUSSIAN QUADRATURE TO INTEGRATE OVER SECOND DISTRIBUTION
C  FUNCTION
C-----------------------------------------------------------------------
C
             PART2   =  0.00D+00
             PART23  =  0.00D+00
             DO 14 IXR = 1,NGS
               VR    =  DSQRT(2.0 * ETHR * XGS(IXR) / XMRKG)
               PART2 =  (1.0/(2.0*PI)) * WGS(IXR) * DSQRT(XGS(IXR) / PI)
C
C-----------------------------------------------------------------------
C  USE SIMPSONS RULE TO INTEGRATE VREL AND XSEC(VREL) IN THETA
C-----------------------------------------------------------------------
C
               PART3  = 0.00D+00
               HSIMP  = PI/NTHETA
               DO 10 ITHETA = 0,NTHETA
                 THETA = (PI * ITHETA) / 180.0
                 I = ITHETA + 1
                 VAL = VD**2 + VR**2 - 2.0*VD*VR*DCOS(THETA)
                 IF(VAL.LE.0.0D0)THEN
                     VREL(I)=0.0D0
                 ELSE
                     VREL(I) = 100.0 *DSQRT(VAL)
                 ENDIF
   10          CONTINUE
C
C-----------------------------------------------------------------------
C  OBTAIN  CROSS SECTIONS FROM SPLINE INTERPOLATION OF TABULAR VALUES
C-----------------------------------------------------------------------
C
C               WRITE(I4UNIT(-1),*)'EXTHER.FOR: entering SIGCX'
               CALL SIGIA ( LSETX    , LPASS    , 
     &                      ALPH     , ETH      , ILTYP    , IOPT     ,
     &                      NENIN    , ENIN     , SGIN     ,
     &                      LTHETA   , VREL     , XSEC
     &                    )
C               WRITE(I4UNIT(-1),*)'EXTHER.FOR: leaving SIGIA'
C
               DO 12 ITHETA = 0,NTHETA
                 IF(ITHETA.EQ.0 .OR. ITHETA.EQ.NTHETA) THEN
                     FAC  =  1.00D+00
                 ELSE
                     FLAG  = ( -1.0 ) ** ITHETA
                     IF ( FLAG.LT.0 ) THEN
                         FAC  =  4.00D+00
                     ELSEIF ( FLAG.GT.0 ) THEN
                         FAC  =  2.00D+00
                     ENDIF
                 ENDIF
                 THETA = (PI * ITHETA) / 180.0
                 I     = ITHETA + 1
                 PART3 = PART3 + (2.0*PI*FAC*VREL(I)*XSEC(I)*
     &                   DSIN(THETA))
   12          CONTINUE
               PART3  = PART3 * HSIMP/3.0
               PART23 = PART23 + (PART2 * PART3)
   14        CONTINUE
             PART123 = PART123 + (PART1 * PART23)
   16      CONTINUE
           RATE = PART123
C
           RCOUT(ITD,ITR) = RATE
   50    CONTINUE
  100  CONTINUE
C
C-----------------------------------------------------------------------
C
       RETURN
       END
