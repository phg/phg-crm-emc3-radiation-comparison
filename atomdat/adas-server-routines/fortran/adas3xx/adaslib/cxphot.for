CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxphot.for,v 1.1 2004/07/06 13:10:33 whitefor Exp $ Date $Date: 2004/07/06 13:10:33 $
CX
      SUBROUTINE CXPHOT( IZ1    , TE     , TR     , V      ,
     &                   N      , L      , L1     , LP     ,
     &                   ISP    , LT     , LT1    , IS     ,
     &                   IRES   , PREC   , PION   , PSTIM
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXPHOT *********************
C
C  PURPOSE:  CALCULATES PHOTO INTEGRALS USING GIIH BOUND-FREE
C            GAUNT-FACTORS.
C
C  CALLING PROGRAM: C6TBRC.
C
C  INPUT : (I*4)  IZ1     = ION CHARGE.
C  INPUT : (R*8)  TE      = ELECTRON TEMPERATURE.
C                           UNITS: K
C  INPUT : (R*8)  TR      = RADIATION TEMPERATURE.
C                           UNITS: K
C  INPUT : (R*8)  V       = EFFECTIVE PRINCIPAL QUANTUM NUMBER OF BOUND
C                           ELECTRON.
C  INPUT : (I*4)  N       = PRINCIPAL QUANTUM NUMBER OF BOUND ELECTRON.
C  INPUT : (I*4)  L       = ORBITAL QUANTUM NUMBER OF BOUND ELECTRON.
C  INPUT : (I*4)  L1      = ORBITAL QUANTUM NUMBER OF FREE ELECTRON.
C  INPUT : (I*4)  LP      = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF PARENT STATE.
C  INPUT : (I*4)  ISP     = 2*SP+1 WHERE SP IS TOTAL SPIN OF PARENT
C                           STATE.
C  INPUT : (I*4)  LT      = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF BOUND SYSTEM.
C  INPUT : (I*4)  LT1     = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF FREE SYSTEM.
C  INPUT : (I*4)  IS      = 2*S+1 WHERE S IS TOTAL SPIN OF SYSTEM.
C  INPUT : (I*4)  IRES    = LEVEL OF RESOLUTION.
C                         = 1 :
C                         = 2 : ABOVE LT1 SUM.
C                         = 3 : ABOVE LT SUM.
C                         = 4 : ABOVE S SUM.
C                         = 5 : UNRESOLVED GBF.
C
C  OUTPUT: (R*8)  PREC    = RADIATIVE RECOMBINATION INTEGRAL.
C  OUTPUT: (R*8)  PION    = PHOTOIONISATION INTEGRAL.
C  OUTPUT: (R*8)  PSTIM   = STIMULATED RECOMBINATION INTEGRAL.
C
C
C  PARAM : (I*4)  MXT     = 8.
C  PARAM : (I*4)  MXI     = 4.
C  PARAM : (R*8)  P1      =
C  PARAM : (R*8)  P2      = 0.9
C
C          (I*4)  IB      = FLAGS VALUE OF B.
C                         = 1 : B >= P2.
C                         = 2 : B <  P2.
C          (I*4)  IB1     = FLAGS VALUE OF B1.
C                         = 1 : B1 >= P2.
C                         = 2 : B1 <  P2.
C          (I*4)  I       = LOOP INDEX.
C
C          (R*8)  Z1      = REAL VALUE = IZ1.
C          (R*8)  B       = P1 * Z1**2 / ( V**2 * TE )
C          (R*8)  B1      = P1 * Z1**2 / ( V**2 * TR )
C          (R*8)  VVE     =
C          (R*8)  GI      = R8GIIH().
C          (R*8)  GI1     = R8GIIH().
C          (R*8)  GI2     = R8GIIH().
C          (R*8)  F1      =
C          (R*8)  F2      =
C          (R*8)  F3      =
C          (R*8)  F4      =
C          (R*8)  F5      =
C          (R*8)  F6      =
C          (R*8)  T       =
C          (R*8)  T2      =
C          (R*8)  T4      =
C          (R*8)  T6      =
C          (R*8)  Q1      =
C          (R*8)  Q2      =
C          (R*8)  EX      =
C          (R*8)  EX1     =
C          (R*8)  D1      =
C          (R*8)  D2      =
C          (R*8)  U2      =
C          (R*8)  U6      =
C
C          (R*8)  X1()    =
C                           DIMENSION: MXT.
C          (R*8)  X2()    =
C                           DIMENSION: MXT.
C          (R*8)  W1()    =
C                           DIMENSION: MXT.
C          (R*8)  W2()    =
C                           DIMENSION: MXT.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8GIIH     ADAS      RETURNS BOUND-FREE G-FACTORS.
C          R8FEII     ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    05/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8GIIH  , R8FEEI
C-----------------------------------------------------------------------
      INTEGER    MXT      , MXI
      PARAMETER( MXT = 8  , MXI = 4 )
C-----------------------------------------------------------------------
      REAL       P1             , P2
      PARAMETER( P1 = 1.5789D5  , P2 = 0.9D0 )
C-----------------------------------------------------------------------
      INTEGER    IZ1     , N       , L       , L1      , LP      ,
     &           ISP     , LT      , LT1     , IS      , IRES
      INTEGER    IB      , IB1     , I
C-----------------------------------------------------------------------
      REAL*8     TE      , TR      , V       , PREC    , PION    ,
     &           PSTIM
      REAL*8     Z1      , B       , B1      , VVE     , GI      ,
     &           GI1     , GI2     , F1      , F2      , F3      ,
     &           F4      , F5      , F6      , T       , T2      ,
     &           T4      , T6      , Q1      , Q2      , EX      ,
     &           EX1     , D1      , D2      , U2      , U6
C-----------------------------------------------------------------------
      REAL*8     X1(MXT)  , X2(MXT)  , W1(MXT)  , W2(MXT)
C-----------------------------------------------------------------------
      DATA       X1 / -0.8611363156D0   , -0.3399810436D0   ,
     &                 0.3399810436D0   ,  0.8611363156D0   ,
     &                 4 * 0.0D0                              /
      DATA       X2 /  0.3225476896D0   ,  1.7457611012D0   ,
     &                 4.5366202969D0   ,  9.3950709123D0   ,
     &                 4 * 0.0D0                              /
      DATA       W1 /  0.3478548451D0   ,  0.6521451548D0   ,
     &                 0.6521451548D0   ,  0.3478548451D0   ,
     &                 4 * 0.0D0                              /
      DATA       W2 /  6.03154104340D-1 ,  3.5741869244D-1  ,
     &                 3.8887908515D-2  ,  5.3929470556D-3  ,
     &                 4 * 0.0D0                              /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      Z1 = DFLOAT( IZ1 )
      B  = P1 * Z1**2 / ( V**2 * TE )
      B1 = P1 * Z1**2 / ( V**2 * TR )
C
C-----------------------------------------------------------------------
C CALCULATE RADIATIVE RECOMBINATION INTEGRAL.
C-----------------------------------------------------------------------
C
      IF (B .GE. P2) THEN
C
C INTEGRAL FORM A ( B>=0.9 )
C
         IB = 1
         F2 = 1.0D0
         VVE = 0.0D0
         T2 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                ISP  , LT   , LT1  , IS   , IRES          )
         U2 = R8FEEI( B )
         PREC = T2 * U2
C
         DO 1 I = 1 , MXI
            Q2 = X2(I) + B
            VVE = Q2 / B - 1.0D0
            GI = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                   ISP  , LT   , LT1  , IS   , IRES          )
            PREC = PREC + F2 * W2(I) * ( GI - T2 ) / Q2
    1    CONTINUE
C
C-----------------------------------------------------------------------
C
      ELSE
C
C INTEGRAL FORM B ( B<0.9 )
C
         IB = 2
         EX = DEXP( B )
         F1 = -0.5D0 * DLOG( B ) * EX
         F2 = 3.678794117D-1 * EX
         VVE = 1.0D0 / B - 1.0D0
         T2 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                ISP  , LT   , LT1  , IS   , IRES          )
         PREC = F2 * T2 * 0.596347D0
C
         DO 2 I = 1 , MXI
            Q1 = B**( 0.5D0 * ( X1(I) + 1.0D0 ) )
            Q2 = X2(I) + 1.0D0
            VVE = Q1 / B - 1.0D0
            GI1 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            VVE = Q2 / B - 1.0D0
            GI2 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            PREC = PREC + F1 * W1(I) * DEXP( -Q1 ) * GI1 +
     &             F2 * W2(I) * ( GI2 - T2 ) / Q2
    2    CONTINUE
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE PHOTOIONISATION INTEGRAL.
C-----------------------------------------------------------------------
C
      IF (B1 .GE. P2) THEN
C
C INTEGRAL FORM C ( B1>=0.9 )
C
         IB1 = 1
         F4 = 1.0D0
         VVE = 0.0D0
         T4  = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                 ISP  , LT   , LT1  , IS   , IRES          )
         PION = T4 * R8FEEI( B1 )
C
         DO 3 I = 1 , MXI
            Q2 = X2(I) + B1
            IF (Q2 .LE. 193.0D0) THEN
               T = DEXP( -Q2 )
            ELSE
               T  = 0.0D0
            ENDIF
            VVE = Q2 / B - 1.0D0
            GI = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                   ISP  , LT   , LT1  , IS   , IRES          )
            PION = PION + F4 * W2(I) * ( GI / ( 1.0D0 - T ) - T4 ) / Q2
    3    CONTINUE
C
C-----------------------------------------------------------------------
C
      ELSE
C
C INTEGRAL FORM D ( B1<0.9 )
C
         IB1 = 2
         EX1 = DEXP( B1 )
         F3 = ( 1.0D0 - B1 ) * EX1
         F4 = 3.678794117D-1 * EX1
         VVE = 1.0D0 / B - 1.0D0
         T4  = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                 ISP  , LT   , LT1  , IS   , IRES          )
         PION = F4 * T4 * 0.596347D0
C
         DO 4 I = 1 , MXI
            Q1 = 2.0D0 * B1 / ( ( 1.0D0 - B1 ) * X1(I) +
     &           ( 1.0D0 + B1 ) )
            D1 = 2.0D0 * B1
            IF (Q1 .GT. 1.0D-6) D1 = D1 * ( DEXP( Q1 ) - 1.0D0 ) / Q1
            Q2 = X2(I) + 1.0D0
            VVE = Q1 / B - 1.0D0
            GI1 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            VVE = Q2 / B - 1.0D0
            GI2 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            PION = PION + F3 * W1(I) * GI1 / D1 + F4 * W2(I) *
     &             ( GI2 / ( 1.0D0 - DEXP( -Q2 ) ) - T4 ) / Q2
    4    CONTINUE
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE STIMULATED RECOMBINATION INTEGRAL.
C-----------------------------------------------------------------------
C
      IF (IB1 .EQ. 1) THEN
C
C INTEGRAL FORM E ( B>=0.9, B1>=0.9, TR>=TE )
C INTEGRAL FORM G ( B<0.9 , B1>=0.9, TR<TE  )
C INTEGRAL FORM I ( B>=0.9, B1>=0.9, TR<TE  )
C
         F6 = 1.0D0
         T6 = T4
         U6 = 1.0D0 + B / B1
         PSTIM = T6 * R8FEEI( B + B1 )
C
         DO 5 I = 1 , MXI
            Q2 = ( X2(I) + B + B1 ) / U6
            IF (Q2 .LE. 193.0D0) THEN
               T = DEXP( -Q2 )
            ELSE
               T  = 0.0D0
            ENDIF
            VVE = Q2 / B - 1.0D0
            GI  = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            PSTIM = PSTIM + F6 * W2(I) * ( GI / ( 1.0D0 - T ) - T6 ) /
     &              ( Q2 * U6 )
    5    CONTINUE
C
C-----------------------------------------------------------------------
C
      ELSE IF (IB .EQ. 1) THEN
C
C INTEGRAL FORM J ( B>=0.9, B1<0.9 , TR>=TE )
C
         F6 = EX1
         VVE = 0.0D0
         T6  = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                 ISP  , LT   , LT1  , IS   , IRES          )
         PSTIM = F6 * T6 * ( 1.0D0 - B * U2 ) / B1
C
         DO 6 I = 1 , MXI
            U6 = B1 / B
            Q2 = U6 * ( X2(I) + B )
            IF (Q2 .LE. 1.0D-6) THEN
               D2 = Q2
            ELSE
               D2 = DEXP( Q2 ) - 1.0D0
            ENDIF
            VVE = Q2 / B - 1.0D0
            GI  = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            PSTIM = PSTIM + F6 * W2(I) * U6 * ( GI / D2 - T6 / Q2 ) / Q2
    6    CONTINUE
C
C-----------------------------------------------------------------------
C
      ELSE IF (TR .GE. TE) THEN
C
C INTEGRAL FORM F ( B<0.9 , B1<0.9 , TR>=TE )
C
         F5 = ( 1.0D0 - B ) * EX * EX1
         F6 = F2 * EX1
         VVE = 1.0D0 / B - 1.0D0
         T6 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                ISP  , LT   , LT1  , IS   , IRES          )
         PSTIM = F6 * B * T6 * 0.403653D0 / B1
C
         DO 7 I = 1 , MXI
            Q1 = 2.0D0 * B1 / ( ( 1.0D0 - B ) * X1(I) + ( 1.0D0 + B ) )
            D1 = 2.0D0 * B1
            IF (Q1 .GT. 1.0D-6) D1 = D1 * ( DEXP( Q1 ) - 1.0D0 ) / Q1
            U6 = B1 / B
            Q2 = U6 * ( X2(I) + 1.0D0 )
            IF (Q2 .LE. 1.0D-6) THEN
               D2 = Q2
            ELSE
               D2 = ( DEXP( Q2 ) - 1.0D0 )
            ENDIF
            VVE = Q1 / B - 1.0D0
            GI1 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            VVE = Q2 / B - 1.0D0
            GI2 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            PSTIM = PSTIM + F5 * W1(I) * DEXP( -B * Q1 / B1 ) * GI1 /
     &              D1 + F6 * W2(I) * U6 * ( GI2 / D2 - T6 / Q2 ) / Q2
    7    CONTINUE
C
C-----------------------------------------------------------------------
C
      ELSE
C
C INTEGRAL FORM H ( B<0.9 , B1<0.9 , TR<TE  )
C
         F5 = F3 * EX
         U6 = 1.0D0 + B / B1
         F6 = DEXP( -U6 ) * EX * EX1
         T6 = T2
         PSTIM = F6 * T6 * R8FEEI( U6 )
C
         DO 8 I = 1 , MXI
            Q1 = 2.0D0 * B1 / ( ( 1.0D0 - B1 ) + X1(I) +
     &           ( 1.0D0 + B1 ) )
            D1 = 2.0D0 * B1 * DEXP( B * Q1 / B1 )
            IF (Q1 .GT. 1.0D-6) D1 = D1 * ( DEXP( Q1 ) - 1.0D0 ) / Q1
            Q2 = X2(I) + U6
            VVE = Q1 / B - 1.0D0
            GI1 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            VVE = Q2 / ( U6 * B ) - 1.0D0
            GI2 = R8GIIH( VVE , V     , N    , L    , L1   , LP   ,
     &                    ISP  , LT   , LT1  , IS   , IRES          )
            PSTIM = PSTIM + F5 * W1(I) * GI1 / D1 + F6 + W2(I) *
     &              ( GI2 / ( 1.0D0 - DEXP( -Q2 / U6 ) ) - T6 ) / Q2
    8    CONTINUE
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
