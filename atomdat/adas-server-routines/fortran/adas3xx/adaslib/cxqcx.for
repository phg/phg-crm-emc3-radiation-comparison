      SUBROUTINE CXQCX(DSNIN  , NEIN  , EIN   ,
     &                 INSEL  , ILSEL ,
     &                 CXOUT  , LEXT  )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN 77 SUBROUTINE: CXQCX ******************
C
C  PURPOSE: Gathers data from adf01 charge exchange files and
C           interpolates on the requested energy vector.
C
C
C  INPUT
C
C  (C*80) DSNIN    : adf01 file name.
C  (R*8)  NEIN     : NUMBER OF USER REQUESTED ENERGIES
C                    UNITS: EV/AMU
C  (R*8)  EIN      : USER REQUESTED ENERGIES
C  (I*4)  INSEL    : SELECTED INPUT DATA n QUANTUM SHELL - 0 for total
C  (I*4)  ILSEL    : SELECTED INPUT DATA l QUANTUM SHELL
C
C
C
C  OUTPUT
C
C  (R*8)  CXOUT()  : CROSS SECTION DATA
C                    UNITS: CM**2
C  (L*4)  LEXT()   : .TRUE. IF INTEPOLATED
C
C
C  PROGRAM:
C
C
C  ROUTINES:
C
C     ROUTINE    SOURCE    BRIEF DESCRIPTION
C     -------------------------------------------------------------
C     CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C     C1BSIG     ADAS      SETS UP X-SECTIONS FOR SELECTED N-SHELL
C     XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C     R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C
C  NOTE     : Depending on the choice of output cross section the
C             appropriate n,l or m value must be present.
C
C
C  VERSION  : 1.1
C  DATE     : 18-01-2001
C  AUTHOR   : Martin O'Mullane
C
C  DATE     : 14-03-2007
C  VERSION  : 1.2
C  MODIFIED : Martin O'Mullane
C               - Increase maximum number of shells to 100.
C               - Use xxdata_01 to read in adf01 dataset.
C
C  VERSION  : 1.3
C  DATE     : 22-05-2007
C  MODIFIED : Martin O'Mullane
C               - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
      INTEGER   IUNT10
      INTEGER   MXNENG       , MXNSHL       , MAXIN       , MAXOUT
      REAL*8    ZERO         , ZEROL
C-----------------------------------------------------------------------
      PARAMETER(IUNT10 =  10    )
      PARAMETER(MXNENG =  50    , MXNSHL = 100 )
      PARAMETER(ZERO = 1.0D-72  , ZEROL = -165.7D0 )
      PARAMETER(MAXIN = MXNENG  , MAXOUT = 100    )
C-----------------------------------------------------------------------
      INTEGER   NEIN
      INTEGER   IZR          , IZD          , INDD         , NENER  ,
     &          NMIN         , NMAX
      integer   ilsel        , insel
      integer   iopt         , iarr
C-----------------------------------------------------------------------
      REAL*8    R8FUN1
C-----------------------------------------------------------------------
      LOGICAL   LPARMS       , LSETL        , LSETM        , LSETX
C-----------------------------------------------------------------------
      CHARACTER DSNIN*80     , SYMBR*2      , SYMBD*2
C-----------------------------------------------------------------------
      INTEGER   LFORMA(MXNENG), IEDATA(2)   , NDATA(2)
C-----------------------------------------------------------------------
      REAL*8    EIN(NEIN)     , CXOUT(NEIN)
      REAL*8    ENERA(MXNENG), ALPHAA(MXNENG), XLCUTA(MXNENG) ,
     &          PL2A(MXNENG) , PL3A(MXNENG)  , XTOT(MXNENG)   ,
     &          XSIGN(MXNENG,MXNSHL) ,
     &          XSIGL(MXNENG,(MXNSHL*(MXNSHL+1))/2)           ,
     &          SIGA(MXNENG)
      REAL*8    XIN(MAXIN)     , YIN(MAXIN)      ,
     &          XOUT(MAXOUT)   , YOUT(MAXOUT)
      REAL*8    DF(MAXIN)
C-----------------------------------------------------------------------
      LOGICAL   LEXT(NEIN)
C-----------------------------------------------------------------------
      EXTERNAL  R8FUN1
C-----------------------------------------------------------------------


C-----------------------------------------------------------------------
C Open input data file - DSNIN
C-----------------------------------------------------------------------

      OPEN( UNIT=IUNT10 , FILE=DSNIN , STATUS='OLD' )


C-----------------------------------------------------------------------
C Fetch all the data from selected dataset
C-----------------------------------------------------------------------

      call xxdata_01( IUNT10 , MXNENG , MXNSHL ,
     &                SYMBR  , SYMBD  , IZR    , IZD    ,
     &                INDD   , NENER  , NMIN   , NMAX   ,
     &                LPARMS , LSETL  , ENERA  ,
     &                ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &                PL3A   , XTOT   , XSIGN  , XSIGL
     &              )
      CLOSE (IUNT10)

C-----------------------------------------------------------------------
C Interpolate onto user requested energy range
C-----------------------------------------------------------------------

C First find the appropriate cross section according to the total/n/l/m
C selection criteria of the user.
C This ADAS301 subroutine returns the correct cross section in SIGA

      IEDATA(1) = 1
      IEDATA(2) = NENER
      NDATA(1)  = NMIN
      NDATA(2)  = NMAX

      CALL C1BSIG( MXNENG  , MXNSHL , NENER  ,
     &             INSEL   , ILSEL  ,
     &             IEDATA  , NDATA  ,
     &             XTOT    , XSIGN  ,
     &             XSIGL   ,
     &             ALPHAA  ,
     &             SIGA
     &           )


C Now spline onto the requested energies.

      if (nein.GT.maxout) then
         write(0,*)'Increase max allowed values in XXSPLE'
         stop
      endif

      LSETX = .TRUE.
      IOPT  = -1

      DO IARR=1,NENER
         XIN(IARR) = DLOG( ENERA(IARR)  )
         IF (SIGA(IARR).LE.ZERO) THEN
             YIN(IARR) = DLOG( ZERO )
         ELSE
             YIN(IARR) = DLOG( SIGA(IARR) )
         ENDIF
      END DO

      DO IARR=1,NEIN
         XOUT(IARR) = DLOG(EIN(IARR))
      END DO

      CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &             NENER , XIN     , YIN    ,
     &             NEIN  , XOUT    , YOUT   ,
     &             DF    , LEXT
     &           )


      DO IARR=1,NEIN
         IF (LEXT(IARR)) THEN
            IF(YOUT(IARR).LE.ZEROL) THEN
                CXOUT(IARR) = 0.0D0
            ELSE
                CXOUT(IARR) = DEXP( YOUT(IARR) )
            ENDIF
         ELSE
            CXOUT(IARR) = 0.0D0
         ENDIF
      END DO


      END
