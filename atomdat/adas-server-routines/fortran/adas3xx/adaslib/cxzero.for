CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxzero.for,v 1.1 2004/07/06 13:11:26 whitefor Exp $ Date $Date: 2004/07/06 13:11:26 $
CX
      SUBROUTINE CXZERO( D1  , XI  , Z   , E   , TIF , T2  ,
     &                   A   , B   , VA  , VB  , X
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: CXZERO ********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: CXEIQP
C
C  INPUT : (R*8)  D1      =
C  INPUT : (R*8)  XI      =
C  INPUT : (R*8)  Z       =
C  INPUT : (R*8)  E       =
C  INPUT : (R*8)  TIF     =
C  INPUT : (R*8)  T2      =
C
C  I/O   : (R*8)  A       =
C  I/O   : (R*8)  B       =
C  I/O   : (R*8)  VA      =
C  I/O   : (R*8)  VB      =
C
C  OUTPUT: (R*8)  X       =
C
C          (R*8)  XM      =
C          (R*8)  XP      =
C          (R*8)  X2      =
C          (R*8)  X3      =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8XIP      ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    07/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8   R8XIP
C-----------------------------------------------------------------------
      REAL*8   D1      , XI      , Z       , E      , TIF     ,
     &         T2      , A       , B       , VA     , VB      ,
     &         X
      REAL*8   XP      , T       , D       , V
C-----------------------------------------------------------------------
      LOGICAL  LFIRST
C-----------------------------------------------------------------------
C
      LFIRST = .TRUE.
      X  = ( A * VB - B * VA ) / ( VB - VA )
      XP = 0.0D0
C
C-----------------------------------------------------------------------
C
    1 CONTINUE
C
      IF (LFIRST .OR. ((DABS( XP - X ) / X - D1) .GT. 0.0D0) ) THEN
C
         LFIRST = .FALSE.
C
         XP = X
         T  = Z + E * X
         D  = TIF * X
         V  = 0.5D0 - T2 * R8XIP( XI , D ) / T**2
C
         IF (V .GT. 0.0D0) THEN
            B  = X
            VB = V
         ELSE IF (V .LT. 0.0D0) THEN
            A  = X
            VA = V
         ELSE
            RETURN
         ENDIF
C
         X = 0.5D0 * ( A + B )
         T = Z + E * X
         D = TIF * X
         V = 0.5D0 - T2 * R8XIP( XI , D ) / T**2
C
         IF (V .GT. 0.0D0) THEN
            B  = X
            VB = V
         ELSE IF (V .LT. 0.0D0) THEN
            A  = X
            VA = V
         ELSE
            RETURN
         ENDIF
C
         X = ( A * VB - B * VA ) / ( VB - VA )
C
         GOTO 1
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
