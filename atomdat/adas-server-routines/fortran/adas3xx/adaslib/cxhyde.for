CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxhyde.for,v 1.1 2004/07/06 13:10:25 whitefor Exp $ Date $Date: 2004/07/06 13:10:25 $
CX
      SUBROUTINE CXHYDE( IZ0 , ZEFF , N , L , E0 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: CXHYDE ********************
C
C  PURPOSE:  CALCULATES LOWEST ORDER NON-RELATIVISTIC, RELATIVISTIC AND
C            QUANTUMELECTRODYNAMIC ENERGIES FOR HYDROGENIC IONS.
C
C            BINDING ENERGY FOR CENTRE OF TERM IS PRODUCED.
C
C            FINE STRUCTURE FOR L>0 MUST BE ADDED EXTERNALLY.
C            FORMULAE ARE FROM ERIKSON (1977) J.PHY CHEM.REF.DATA,6,831.
C
C            QED EFFECTS FOR L>0 OMITTED.
C
C  CALLING PROGRAM: GENERAL USE
C
C  INPUT : (I*4)  IZ0     = NUCLEAR CHARGE.
C  INPUT : (R*8)  ZEFF    = EFFECTIVE NUCLEAR CHARGE.
C  INPUT : (I*4)  N       = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  L       = ORBITAL QUANTUM NUMBER.
C
C  OUTPUT: (R*8)  E0      = CENTER BINDING ENERGY.
C                           UNITS: RYD
C
C          (R*8)  Z0      = REAL VALUE = IZ0.
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XL      = REAL VALUE = L.
C          (R*8)  BLN     =
C          (R*8)  RMC     =
C          (R*8)  QED     =
C
C          (R*8)  BLNA()  =
C                           DIMENSION: 4
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER   IZ0     , N       , L
C-----------------------------------------------------------------------
      REAL*8    ZEFF    , E0
      REAL*8    Z0      , XN      , XL      , BLN     , RMC     ,
     &          QED
C-----------------------------------------------------------------------
      REAL*8    BLNA(4)
C-----------------------------------------------------------------------
      DATA      BLNA / -2.984128D0 ,  2.811768D0 ,
     &                 -2.767699D0 , -2.749859D0   /
C-----------------------------------------------------------------------
C
      Z0 = DFLOAT( IZ0 )
      XN = DFLOAT( N )
      XL = DFLOAT( L )
      E0 = ( Z0 / XN )**2
C
      IF (L .LT. 1) THEN
C
         RMC = 5.32504D - 5 * ( ZEFF / XN )**2
     &         * ( XN / ( XL + 1.0D0 ) - 0.75D0 )
C
         IF (N .LT. 5) THEN
            BLN = BLNA(N)
         ELSE
            BLN = -2.71631D0 - 2.68551D-1 / XN**1.5D0
         ENDIF
C
         QED = 3.2984D-7 * ZEFF * ( ZEFF / XN )**3
     &         * ( DLOG( 1.8779D4 / ZEFF**2 ) + BLN + 6.333D-1 )
         E0  = E0 * ( 1.0D0 + ( ZEFF / Z0 )**2 * RMC ) - QED
C
      ELSE
C
         RMC = 5.32504D-5 * ( ZEFF / XN )**2
     &         * ( XN / ( XL + 0.5D0 ) - 0.75D0 )
         E0  = E0 * ( 1.0D0 + ( ZEFF / Z0 )**2 * RMC )
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
