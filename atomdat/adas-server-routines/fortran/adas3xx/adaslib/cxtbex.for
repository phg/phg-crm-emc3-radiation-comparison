CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxtbex.for,v 1.2 2013/01/04 11:10:19 mog Exp $ Date $Date: 2013/01/04 11:10:19 $
CX
      SUBROUTINE CXTBEX( MXNSHL , IZ1    , NBOT   , NTOP   ,
     &                   NGRND  , TEV    , TBQEX  , QTHEX  ,
     &                   FTHEX
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXTBEX *********************
C
C  PURPOSE:  SETS UP A TABLE OF ELECTRON IMPACT EXCITATION RATE
C            COEFFICIENTS FOR A HYDROGENIC ION FROM THE GROUND STATE
C            TO EXCITED NL-LEVELS.
C
C  CALLING PROGRAM: ADAS308 , C6TBEX.
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NGRND    =
C  INPUT : (R*8)  TEV      = ELECTRON TEMPERATURE.
C                            UNITS: EV
C
C  OUTPUT: (R*8)  TBQEX()  = TABLE OF NL-LEVEL EXCITATION RATE
C                            COEFFICIENTS.
C                            UNITS:
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  OUTPUT: (R*8)  QTHEX()  = TABLE OF N-LEVEL EXCITATION RATE
C                            COEFFICIENTS.
C                            UNITS:
C                            DIMENSION: N-SHELL
C  OUTPUT: (R*8)  FTHEX()  = TABLE OF NL-LEVEL EXCITATION RATE
C                            COEFFICIENTS EXPRESSED AS FRACTION OF
C                            CORRESPONDING N-LEVEL RATE.
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  PARAM : (I*4)  MXN      = 'MXNSHL'.
C  PARAM : (R*8)  P1       =
C
C          (I*4)  N        = N QUANTUM NUMBER.
C          (I*4)  L        = L QUANTUM NUMBER.
C          (I*4)  IDL      = L-RESOLVED TABLE INDEX.
C
C          (R*8)  ATE      =
C          (R*8)  RDE      =
C          (R*8)  ETE      =
C          (R*8)  FACT     =
C
C          (R*8)  GAMA()   = TABLE OF EXCITATION RATE PARAMETERS.
C                            UNITS:
C                            DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXGHNL     ADAS      CALCULATES EXCITATION RATE PARAMETERS.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     05/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 13.6048D0 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , IZ1     , NBOT    , NTOP   , NGRND
      INTEGER    N       , L       , IDL
C-----------------------------------------------------------------------
      REAL*8     TEV
      REAL*8     ATE     , RDE     , ETE     , FACT
C-----------------------------------------------------------------------
      REAL*8     TBQEX((MXNSHL*(MXNSHL+1))/2)  ,
     &           QTHEX(MXNSHL)                 ,
     &           FTHEX((MXNSHL*(MXNSHL+1))/2)
      REAL*8     GAMA(MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXN' LARGE ENOUGH.
C-----------------------------------------------------------------------
C
      IF (MXNSHL .GT. MXN) THEN
         WRITE(I4UNIT(-1),1000) MXN
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
       ATE = P1 * DFLOAT( IZ1 )**2 / TEV
       FACT = 2.17161D-8 * DSQRT( P1 / TEV ) / 2.0D0
C
C-----------------------------------------------------------------------
C ZERO EXCITATION RATE TABLES.
C-----------------------------------------------------------------------
C
      DO 1 N = 1 , MXNSHL
         QTHEX(N) = 0.0D0
    1 CONTINUE
C
      DO 2 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         TBQEX(IDL) = 0.0D0
         FTHEX(IDL) = 0.0D0
    2 CONTINUE
C
C-----------------------------------------------------------------------
C FILL EXCITATION RATE TABLE FROM 'NBOT' TO 'NTOP'.
C-----------------------------------------------------------------------
C
      DO 3 N = NBOT , NTOP
C
         CALL CXGHNL( MXNSHL , IZ1    , IZ1    , NGRND  , 0      ,
     &                N      , TEV    , GAMA                       )
C
         RDE = 1.0D0 / ( DFLOAT( NGRND ) )**2 -
     &         1.0D0 / ( DFLOAT( N ) )**2
         ETE = DEXP( -ATE * RDE )
C
         DO 4 L = 0 , N-1
            TBQEX(I4IDFL( N , L )) = FACT * ETE * GAMA(L+1)
    4    CONTINUE
C
    3 CONTINUE
C
C-----------------------------------------------------------------------
C FILL 'QTHEX' AND 'FTHEX' ARRAYS.
C-----------------------------------------------------------------------
C
      DO 5 N = NBOT , NTOP
C
         QTHEX(N) = 0.0D0
         DO 6 L = 0 , N-1
            QTHEX(N) = QTHEX(N) + TBQEX(I4IDFL( N , L ))
    6    CONTINUE
C
         DO 7 L = 0 , N-1
            IDL = I4IDFL( N , L )
            IF (QTHEX(N) .GT. 0.0D0) THEN
               FTHEX(IDL) = TBQEX(IDL) / QTHEX(N)
            ELSE
               FTHEX(IDL) = 0.0D0
            ENDIF
    7    CONTINUE
C
    5 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXTBEX ERROR ', 32('*') //
     &        2X, 'VALUE OF ''MXNSHL'' GREATER THAN ', I2, '.' /
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE CXTBEX.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
