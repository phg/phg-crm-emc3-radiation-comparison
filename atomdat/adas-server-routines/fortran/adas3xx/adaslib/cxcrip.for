      SUBROUTINE CXCRIP( MXCOLL , MXTEMP , IZT    , IZC    , WI     ,
     &                   EI     , WJ     , EJ     , EM     , PHI    ,
     &                   NCOLL  , EPS    , OMEG   , NTEMP  , TVA    ,
     &                   RAT    , QI     , QJ     , GA
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXCRIP *********************
C
C  PURPOSE:  CALCULATES ELECTRON AND POSITIVE ION COLLISION EXCITATION
C            AND DEEXCITATION RATE COEFFICIENTS FOR DIPOLE TRANSITIONS
C            IN THE IMPACT PARAMETER APPROXIMATION.
C
C            (BURGESS AND SUMMERS,1976,MON.NOT.R.AST.SOC.,174,345)
C
C            OPTIONALLY A SET OF INCIDENT PARTICLE ENERGIES AND
C            COLLISION STRENGTHS MAY BE PROVIDED, IN WHICH CASE THE
C            IMPACT PARAMETER THEORY IS USED TO CALCULATE THE COLLISION
C            STRENGTHS AT HIGH ENERGY WITH VALUES SCALED TO THE HIGHEST
C            ENERGY INPUT COLLISION STRENGTH.
C
C  CALLING PROGRAM:  CXCRDG
C
C  INPUT : (I*4)  MXCOLL  = MAXIMUM NUMBER OF COLLISION STRENGTHS.
C  INPUT : (I*4)  MXTEMP  = MAXIMUM NUMBER OF TEMPERATURES.
C  INPUT : (I*4)  IZT     = TARGET ION CHARGE.
C  INPUT : (I*4)  IZC     = CHARGE OF COLLIDING PARTICLE.
C  INPUT : (R*8)  WI      = STATISTICAL WEIGHT OF STATE I.
C  INPUT : (R*8)  EI      = BINDING ENERGY OF STATE I.
C                           UNITS: RYD
C  INPUT : (R*8)  WJ      = STATISTICAL WEIGHT OF STATE J.
C  INPUT : (R*8)  EJ      = BINDING ENERGY OF STATE J.
C                           UNITS: RYD
C  INPUT : (R*8)  EM      = REDUCED MASS FOR COLLIDING PARTICLE.
C                           UNITS: ELECTRON MASSES
C  INPUT : (R*8)  PHI     = FIJ/EIJ WHERE:
C                           FIJ = ABSORPTION OSCILLATOR STRENGTH;
C                           EIJ = EI-EJ = THE TRANSITION ENERGY (RYD).
C  INPUT : (I*4)  NCOLL   = NUMBER OF TABULAR VALUES OF COLLISION
C                           STRENGTH.
C  INPUT : (R*8)  EPS()   = INCIDENT ELECTRON ENERGIES.
C                           UNITS: RYD
C                           DIMENSION: COLLISION INDEX.
C  INPUT : (R*8)  OMEG()  = COLLISION STRENGTHS.
C                           DIMENSION: COLLISION INDEX.
C  INPUT : (I*4)  NTEMP   = NUMBER OF TEMPERATURES.
C  INPUT : (R*8)  TVA()   = TEMPERATURES (INCIDENT PARTICLE
C                           DISTRIBUTION).
C                           UNITS: EV.
C                           DIMENSION: TEMPERATURE INDEX.
C
C  OUTPUT: (R*8)  RAT     = RATIO OF OMEG(NCOLL) TO I.P. OMEGA.
C  OUTPUT: (R*8)  QI()    = COLLISIONAL EXCITATION RATE COEFFICIENTS.
C                           UNITS: CM3 SEC-1
C                           DIMENSION: TEMPERATURE INDEX.
C  OUTPUT: (R*8)  QJ()    = COLLISIONAL DEEXCITATION RATE COEFFICIENTS.
C                           UNITS: CM3 SEC-1
C                           DIMENSION: TEMPERATURE INDEX.
C  OUTPUT: (R*8)  GA()    = GAMMA RATE PARAMETERS.
C                           UNITS:
C                           DIMENSION: TEMPERATURE INDEX.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          CXEIQP     ADAS      Impact parameter cross section calculation
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/10/93
C
C VERSION: 1.1                          DATE: 20-06-95
C MODIFIED: TIM HAMMOND (Probably)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION  : 1.2                        DATE: 17-05-07
C MODIFIED : Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C VERSION  : 1.3                          
C DATE     : 10-05-2011
C MODIFIED : Martin O'Mullane
C              - Further updating of comments.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXTEMP  , MXCOLL  , IZT     , IZC     , NTEMP   ,
     &           NCOLL
      INTEGER    I       , K
C-----------------------------------------------------------------------
      REAL*8     WI      , EI      , WJ      , EJ      , EM      ,
     &           PHI     , RAT
      REAL*8     Z1      , Z2      , EIJ     , ELAM    , ZCOL    ,
     &           Z2PHI   , SC      , T1      , T2      , EPSM    ,
     &           R       , EIQ     , FLAG    , ATE     , S       ,
     &           U1      , G1      , U2      , G2      , B       ,
     &           S1      , E       , P
C-----------------------------------------------------------------------
      REAL*8     EPS(MXCOLL)   , OMEG(MXCOLL)  , TVA(MXTEMP)   ,
     &           GA(MXTEMP)    , QI(MXTEMP)    , QJ(MXTEMP)
      REAL*8     X(4)    , H(4)
C-----------------------------------------------------------------------
      DATA       X / 0.322547689619D0 , 1.745761101158D0 ,
     &               4.536620296921D0 , 9.395070912301D0   /
      DATA       H / 6.03154104342D-1 , 3.57418692438D-1 ,
     &               3.88879085150D-2 , 5.39294705561D-4   /
C-----------------------------------------------------------------------
C
      Z1  = DFLOAT( IZT + 1 )
      Z2  = DABS( DFLOAT( IZC ) )
      EIJ = EI - EJ
C
C-----------------------------------------------------------------------
C
      IF (EIJ .LE. 1.0D-10) THEN
         DO 1 I = 1 , NTEMP
            QI(I) = 0.0D0
            QJ(I) = 0.0D0
            GA(I) = 0.0D0
    1    CONTINUE
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C
      ELAM  = 9.11763D2 / EIJ
      ZCOL  = Z2 * ( Z1 - 1.0D0 )
      Z2PHI = Z2**2 * PHI
      SC    = 1.0D0
      T2    = WI
      EPSM  = EIJ
      R     = 1.25D0 * Z1 / EI + 0.25D0 / Z1
C
      IF (NCOLL .GT. 0) THEN
         EPSM = EPS(NCOLL)
         CALL CXEIQP( EPSM  , EIJ   , EM    , ZCOL  , Z2PHI , SC    ,
     &                WI    , WJ    , R     , EIQ   , FLAG            )
         T2 = WI * EIQ
         IF (NCOLL .EQ. 1) EPSM = EIJ
         RAT = OMEG(NCOLL) / T2
         T2  = WI * OMEG(NCOLL) / T2
      ENDIF
C
C-----------------------------------------------------------------------
C
      DO 2 I = 1 , NTEMP
C
         ATE = 13.6049D0 / TVA(I)
         T1 = T2 * DEXP( -ATE * ( EPSM - EIJ ) )
         S = 0.0D0
C
         IF (NCOLL .GE. 2) THEN
C
            S  = OMEG(1)
            U1 = ATE * ( EPS(1) - EIJ )
            G1 = 1.0D0 - U1
            U2 = ATE * ( EPS(2) - EIJ )
            G2 = DEXP( -U2 )
            B  = ( OMEG(2) - OMEG(1) ) / ( U2 - U1 )
C
            DO 3 K = 3 , NCOLL
               S  = S + B * ( G1 - G2 )
               U1 = U2
               G1 = G2
               U2 = ATE * ( EPS(K) - EIJ )
               G2 = DEXP( -U2 )
               B  = ( OMEG(K) - OMEG(K-1) ) / ( U2 - U1 )
    3       CONTINUE
C
            S = S + B * ( G1 - G2 ) - OMEG(NCOLL) * G2
C
         ENDIF
C
         S1 = 0.0D0
         DO 4 K = 1 , 4
            E = X(K) / ATE + EPSM
            CALL CXEIQP( E     , EIJ   , EM    , ZCOL  , Z2PHI ,
     &                   SC    , WI    , WJ    , R     , EIQ   ,
     &                   FLAG                                    )
            S1 = S1 + H(K) * EIQ
    4    CONTINUE
C
         S = S + T1 * S1
         P = 6.8916111D-2 * S / ( WI * PHI )
         QJ(I) = 2.171609D-8 * DSQRT( ATE / EM ) * S / WJ
         GA(I) = 4.604D7 * WJ * QJ(I) / DSQRT( ATE )
         IF ((ATE * EIJ) .GT. -70.0D0) THEN
            QI(I) = QJ(I) * WJ * DEXP( -ATE * EIJ ) / WI
         ELSE
            QI(I) = 0.0D0
         ENDIF
C
   2  CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
