      SUBROUTINE CXEIQP( EI   , EIJ  , EM   , Z    , PHI  , SC   ,
     &                   WI   , WJ   , R    , EIQ  , FLAG
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: CXEIQP ********************
C
C  PURPOSE: Calculates ECIP (exchange classical impact-parameter) cross
C           sections.
C
C  CALLING PROGRAM: CXRIPV
C
C  I/O   : (R*8)  EI      = BINDING ENERGY OF STATE I.
C                           UNITS: RYD
C  INPUT : (R*8)  EIJ     = TRANSITION ENERGY (RYD).
C                           UNITS: RYD
C  INPUT : (R*8)  EM      = REDUCED MASS FOR COLLIDING PARTICLE.
C                           UNITS: ELECTRON MASSES
C  INPUT : (R*8)  Z       = Target ion charge * charge colliding particle
C  INPUT : (R*8)  PHI     = FIJ/EIJ WHERE:
C                           FIJ = ABSORPTION OSCILLATOR STRENGTH;
C  INPUT : (R*8)  SC      = External global scaling factor
C  INPUT : (R*8)  WI      = STATISTICAL WEIGHT OF STATE I.
C  INPUT : (R*8)  WJ      = STATISTICAL WEIGHT OF STATE J.
C  INPUT : (R*8)  R       = Mean atomic radius (atomic units) (5n**2+1)/4z
C
C  OUTPUT: (R*8)  EIQ     = EI * Cross section (units: pi a0**2)
C  OUTPUT: (R*8)  FLAG    = 1.0  Weak coupling region
C                           0.0  Strong coupling region.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8XIP    ADAS        Impact parameter 1st Bessel Integral
C          R8YIP    ADAS        Impact parameter 2nd Bessel Integral
C          CXZERO   ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    07/10/93
C
C VERSION  : 1.2                          
C DATE     : 10-05-2011
C MODIFIED : Martin O'Mullane
C              - Improve comments and specify arguments.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8XIP   , R8YIP
C-----------------------------------------------------------------------
      REAL*8     PD1
      PARAMETER( PD1 = 1.0D-02 )
C-----------------------------------------------------------------------
      REAL*8     EIJ     , EM      , Z       , PHI     , SC      ,
     &           WI      , WJ      , R       , EI      , EIQ     ,
     &           FLAG
      REAL*8     EF      , T       , C       , XIJ     , TI      ,
     &           TF      , TIF     , XI      , D       , E       ,
     &           EM2     , T1      , T2      , P       , EIQW    ,
     &           R1      , A       , VA      , B       , VB
C-----------------------------------------------------------------------
C
      FLAG = 1.0D0
      EF = EI - EIJ
      T  = EF / EI
C
C-----------------------------------------------------------------------
C
      C = 1.0D0
C
      IF ((T .LT. 0.2D0) .OR. (T .GT. 5.0D0)) THEN
C
         IF (T .LT. 0.2D0) THEN
            XIJ = EIJ
         ELSE
            XIJ = -EIJ
         ENDIF
C
         IF (Z .LT. 1.0E-6) THEN
            C = DSQRT( 4.0D0 * EF / XIJ )
         ELSE IF (EM .GT. 1.5D0) THEN
            C = DABS( XIJ ) / ( EI * DSQRT( EF ) + EF * DSQRT( EI ) )
            C = Z * DSQRT( EM ) * (1.1056D0 / DSQRT( XIJ ) - C )
            IF ((3.142D0 * C) .GT. -150.0D0) THEN
               C = DEXP( 3.142D0 * C )
            ELSE
               EIQ = 0.0D0
               RETURN
            ENDIF
         ENDIF
C
         EI = 1.25D0 * XIJ
         EF = 0.25D0 * XIJ
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      TI  = DSQRT( EI )
      TF  = DSQRT( EF )
      TIF = DABS( EIJ ) / ( TI + TF )
      XI  = Z * TIF / ( TI * TF )
      D   = TIF * R
      E   = TI * TF
C
      IF (EM .GE. 1.5D0) THEN
         EM2 = DSQRT( EM )
         TIF = EM2 * TIF
         XI  = -EM2 * XI
         D   = EM2 * D
      ENDIF
C
      T  = Z + E * R
      T1 = R8XIP( XI , D ) / T**2
      T2 = 4.0D0 * PHI * E * SC * EM
C
      IF (WI .GT. WJ) T2 = T2 * WI / WJ
C
      P = T2 * T1
      EIQW = 8.0D0 * C * PHI * EM * R8YIP( XI , D )
C
C-----------------------------------------------------------------------
C
      IF (P .GT. 0.5) THEN
C
         R1 = R
C
   12    CONTINUE
         IF (P .GT. 0.5D0) THEN
            FLAG = 0.0D0
            A = R1
            VA = 0.5D0 - P
            R1 = 2 * R1
            T  = Z + E * R1
            D  = TIF * R1
            P  = T2 * R8XIP( XI , D ) / T**2
            GOTO 12
         ENDIF
C
         IF (P .LT. 0.5D0) THEN
            B  = R1
            VB = 0.5D0 - P
            CALL  CXZERO( PD1 , XI  , Z   , E   , TIF , T2  ,
     &                    A   , B   , VA  , VB  , R1          )
         ENDIF
C
         T  = Z + E * R1
         D  = TIF * R1
         T1 = R8XIP( XI , D ) / T**2
C
      ENDIF
C
      EIQ = 8.0D0 * C * PHI * EM
     &      * ( R8YIP( XI , D ) + 0.5D0 * T1 * ( T**2 - Z**2 ) )
C
C-----------------------------------------------------------------------
C
      EIQ = DMIN1( EIQ , EIQW )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
