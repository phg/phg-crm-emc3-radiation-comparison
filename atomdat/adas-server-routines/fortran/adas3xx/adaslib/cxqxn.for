CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxqxn.for,v 1.1 2004/07/06 13:11:01 whitefor Exp $ Date $Date: 2004/07/06 13:11:01 $
CX
      SUBROUTINE CXQXN ( MXNENG , MXNSHL , MXBEAM , NBEAM  ,
     &                   BMENA  , BMFRA  , NBOT   , NTOP   ,
     &                   NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                   ALPHAA , XSECNA , QTHEOR , RATE
     &                 )
C
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXQXN  *********************
C
C  PURPOSE:  USES THE INPUT DATASET TO CALCULATE THE CHARGE EXCHANGE
C            RATE COEFFICIENTS FOR N-LEVELS AVERAGED OVER THE BEAM
C            FRACTIONS.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIEL.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  MXBEAM    = MAXIMUM NO. OF BEAM ENERGIES.
C  INPUT : (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NMINF     = MINIMUM PRINCIPAL QUANTUM NUMBER OF INPUT
C                             DATASET.
C  INPUT : (I*4)  NMAXF     = MAXIMUM PRINCIPAL QUANTUM NUMBER OF INPUT
C                             DATASET.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES IN DATASET.
C  INPUT : (R*8)  ENRGYA()  = COLLISION ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C
C  OUTPUT: (R*8)  QTHEOR()  = MEAN RATE COEFFICIENTS FOR N-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  OUTPUT: (R*8)  RATE(,)   = RATE COEFFICIENTS FOR EACH COMPONENT OF
C                             THE BEAM AS A FUNCTION OF N-LEVEL.
C                             UNITS: CM3 SEC-1
C                             1ST DIMENSION: BEAM INDEX
C                             2ND DIMENSION: N-SHELL
C
C  PARAM : (R*8)  ALPMIN    = MINIMUM VALUE OF EXTRAPOLATION EXPONENT
C                             'ALPHA'.
C  PARAM : (R*8)  C1        =
C
C          (I*4)  IB        = BEAM INDEX.
C          (I*4)  N         = PRINCIPAL QUANTUM NUMBER.
C          (I*4)  IE        = ENERGY INDEX.
C          (I*4)  IEU       = ENERGY INDEX ABOVE BEAM ENERGY.
C          (I*4)  IEL       = ENERGY INDEX BELOW BEAM ENERGY.
C
C          (R*8)  XNMAXF    = REAL VALUE = NMAXF.
C          (R*8)  ALPHA     = EXTRAPOLATION EXPONENT.
C          (R*8)  VEL       = VELOCITY OF INCIDENT ATOM.
C                             UNITS: CM SEC-1
C          (R*8)  GRAD      = GRADIENT OF STRAIGHT LINE INTERPOLATION.
C          (R*8)  XSEC      = CROSS-SECTION AT INTERMEDIATE ENERGY.
C
C          (L*4)  LMATCH    = FLAG IF BEAM ENERGY MATCHES EXACTLY A DATA
C                             SET ENERGY.
C                           = .TRUE.  => BEAM ENERGY MATCHES.
C                           = .FALSE. => BEAM ENERGY DOES NOT MATCH.
C
C ROUTINES:  NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    18/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     ALPMIN          , C1
      PARAMETER( ALPMIN = 1.0D0  , C1 = 1.3840D4 )
C-----------------------------------------------------------------------
      INTEGER    MXNENG  , MXNSHL  , MXBEAM  , NBEAM   , NBOT    ,
     &           NTOP    , NMINF   , NMAXF   , NENRGY
      INTEGER    IB      , N       , IE      , IEU     , IEL
C-----------------------------------------------------------------------
      REAL*8     XNMAXF  , ALPHA   , VEL     , GRAD    , XSEC
C-----------------------------------------------------------------------
      LOGICAL    LMATCH
C-----------------------------------------------------------------------
      REAL*8     BMENA(MXBEAM)   , BMFRA(MXBEAM)   , ENRGYA(MXNENG)  ,
     &           ALPHAA(MXNENG)  , QTHEOR(MXNSHL)
      REAL*8     XSECNA(MXNENG,MXNSHL)  , RATE(MXBEAM,MXNSHL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      XNMAXF = DFLOAT( NMAXF )
      IEU = 0
      IEL = 0
      LMATCH = .FALSE.
C
      DO 1 N = 1 , MXNSHL
         QTHEOR(N) = 0.0D0
    1 CONTINUE
C
C-----------------------------------------------------------------------
C FILL 'RATE' ARRAY.
C-----------------------------------------------------------------------
C
      DO 2 IB = 1 , NBEAM
C
C-----------------------------------------------------------------------
C
         IF ((BMENA(IB) .LT. ENRGYA(1)) .OR.
     &       (BMENA(IB) .GT. ENRGYA(NENRGY))) THEN
C
            DO 3 N = 1 , MXNSHL
               RATE(IB,N) = 0.0D0
    3       CONTINUE
C
C-----------------------------------------------------------------------
C
         ELSE
C
C-----------------------------------------------------------------------
C DETERMINE VALUE OF EXTRPOLATION EXPONENT 'ALPHA'.
C-----------------------------------------------------------------------
C
            IEU = 0
            IEL = 0
            LMATCH = .FALSE.
C
            DO 4 IE = 1 , NENRGY
               IF (BMENA(IB) .GE. ENRGYA(IE)) THEN
                  IEL = IE
                  IEU = IE + 1
                  IF (BMENA(IB) .EQ. ENRGYA(IE)) LMATCH = .TRUE.
               ENDIF
    4       CONTINUE
C
            IF (LMATCH) THEN
               ALPHA = ALPHAA(IEL)
            ELSE
               ALPHA = ( ( BMENA(IB) - ENRGYA(IEU) ) * ALPHAA(IEL)
     &                 - ( BMENA(IB) - ENRGYA(IEL) ) * ALPHAA(IEU) )
     &                 / ( ENRGYA(IEL) - ENRGYA(IEU) )
            ENDIF
C
C-----------------------------------------------------------------------
C
            VEL = C1 * DSQRT( BMENA(IB) ) * 1.0D2
C
            DO 5 N = NBOT , NTOP
C
               IF (N .LT. NMINF) THEN
C
                  RATE(IB,N) = 0.00D+00
C
               ELSE IF ((ALPHA .LE. ALPMIN) .AND. (N .GT. NMAXF)) THEN
C
                  RATE(IB,N) = 0.00D+00
C
               ELSE IF (LMATCH) THEN
C
                  IF (N .LE. NMAXF) THEN
                     RATE(IB,N) = VEL * XSECNA(IEL,N)
                  ELSE
                     RATE(IB,N) = VEL * XSECNA(IEL,NMAXF)
     &                            * ( XNMAXF / DFLOAT( N ) )**ALPHA
                  ENDIF
C
               ELSE
C
                  IF (N .LE. NMAXF) THEN
                     GRAD = ( XSECNA(IEU,N) - XSECNA(IEL,N) )
     &                      / ( ENRGYA(IEU) - ENRGYA(IEL) )
                     XSEC = GRAD * (BMENA(IB)-ENRGYA(IEL))
     &                      + XSECNA(IEL,N)
                     RATE(IB,N) = VEL * XSEC
                  ELSE
                     GRAD = ( XSECNA(IEU,NMAXF) - XSECNA(IEL,NMAXF) )
     &                      / ( ENRGYA(IEU) - ENRGYA(IEL) )
                     XSEC = GRAD * (BMENA(IB)-ENRGYA(IEL))
     &                      + XSECNA(IEL,NMAXF)
                     RATE(IB,N) = VEL * XSEC
     &                            * ( XNMAXF / DFLOAT( N ) )**ALPHA
                  ENDIF
C
               ENDIF
C
    5       CONTINUE
C
C-----------------------------------------------------------------------
C
         ENDIF
C
    2 CONTINUE
C
C-----------------------------------------------------------------------
C FILL 'QTHEOR' ARRAY.
C-----------------------------------------------------------------------
C
      DO 6 N = NBOT , NTOP
         DO 7 IB = 1 , NBEAM
C
            IF (RATE(IB,N) .LE. 1.0D-30) RATE(IB,N) = 1.0D-30
            QTHEOR(N) = QTHEOR(N) + RATE(IB,N) * BMFRA(IB)
C
    7    CONTINUE
    6 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
