CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxovlp.for,v 1.2 2007/04/11 11:24:03 allan Exp $ Date $Date: 2007/04/11 11:24:03 $
CX
      FUNCTION CXOVLP ( MXNSHL , NMAX , N1 , L1 , N2 , L2 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 FUNCTION: CXOVLP **********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: CXGHNL (ADAS306/307/308/309).
C
C  FUNC  : (R*8)  CXOVLP  =
C
C  INPUT : (I*4)  MXNSHL  = MAXIMUM VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  NMAX    = N LEVEL AT WHICH TO CHANGE APPROXIMATION.
C  INPUT : (I*4)  N1      = FIRST N QUANTUM NUMBER.
C  INPUT : (I*4)  L1      = FIRST L QUANTUM NUMBER.
C  INPUT : (I*4)  N2      = SECOND N QUANTUM NUMBER.
C  INPUT : (I*4)  L2      = SECOND L QUANTUM NUMBER.
C
C  PARAM : (I*4)  MXN     = 20.
C  PARAM : (I*4)  MXTERM  = 2*MXN.
C
C          (I*4)  N1SAVE  = N1 ON LAST CALL OF FUNCTION.
C          (I*4)  L1SAVE  = N1 ON LAST CALL OF FUNCTION.
C          (I*4)  N2SAVE  = N2 ON LAST CALL OF FUNCTION.
C          (I*4)  NA      = NUMBER OF TERMS IN POWER SERIES.
C          (I*4)  IPA     = POWER OF LEADING TERM.
C          (I*4)  NB      = NUMBER OF TERMS IN POWER SERIES.
C          (I*4)  IPB     = POWER OF LEADING TERM.
C          (I*4)  NC      = NUMBER OF TERMS IN POWER SERIES.
C          (I*4)  IPC     = POWER OF LEADING TERM.
C          (I*4)  IL2     = LOOP INDEX.
C
C          (R*8)  SOVL    =
C          (R*8)  UA      = POWER OF EXPONENT IN LEADING TERM.
C          (R*8)  UB      = POWER OF EXPONENT IN LEADING TERM.
C          (R*8)  UC      = POWER OF EXPONENT IN LEADING TERM.
C          (R*8)  S       =
C
C          (R*8)  A()     = POWER SERIES COEFFICIENTS.
C          (R*8)  B()     = POWER SERIES COEFFICIENTS.
C          (R*8)  C()     = POWER SERIES COEFFICIENTS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUPUT OF MESSAGES.
C          R8PROV     ADAS
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     04/10/93
C
C VERSION  : 1.2                          
C DATE     : 11-04-2007
C MODIFIED : Allan Whiteford
C               - Renamed from R8OVLP.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     CXOVLP
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      REAL*8     R8PROV
C-----------------------------------------------------------------------
      INTEGER    MXN         , MXTERM
      PARAMETER( MXN = 20    , MXTERM = 2 * MXN )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , NMAX    , N1      , L1      , N2      ,
     &           L2      , IL2
      INTEGER    N1SAVE  , L1SAVE  , N2SAVE  , NA      , IPA     ,
     &           NB      , IPB     , NC      , IPC
C-----------------------------------------------------------------------
      REAL*8     SOVL    , UA      , UB      , UC      , S
C-----------------------------------------------------------------------
      REAL*8     A(MXTERM)  , B(MXTERM)  , C(MXTERM)
C-----------------------------------------------------------------------
      SAVE       N1SAVE  , L1SAVE  , N2SAVE
      SAVE       SOVL
C-----------------------------------------------------------------------
      DATA       N1SAVE / 0 /  , L1SAVE / 0 /  , N2SAVE / 0 /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK 'MXNSHL' NOT TOO LARGE.
C-----------------------------------------------------------------------
C
      IF (MXNSHL .GT. MXN) THEN
         WRITE(I4UNIT(-1),1000) MXN
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      IF (MAX0( N1 , N2 ) .LE. MIN0( NMAX , MXNSHL )) THEN
C
C-----------------------------------------------------------------------
C
         IF ((N1 .NE. N1SAVE) .OR. (L1 .NE. L1SAVE) .OR.
     &       (N2 .NE. N2SAVE)                            ) THEN
C
C
            N1SAVE = N1
            L1SAVE = L1
            N2SAVE = N2
C
            CALL CXRWFH( MXTERM , N1 , L1 , A , NA , IPA , UA )
            CALL CXPPRD( MXTERM , A , NA , IPA , A , NA , IPA ,
     &                   B , NB , IPB )
            S = 2.0D0 * UA
            SOVL = 0.0D0
C
            DO 1 IL2 = 0 , N2-1
               CALL CXRWFH( MXTERM , N2 , IL2 , A , NA , IPA , UA )
               CALL CXPPRD( MXTERM , A , NA , IPA , A , NA , IPA ,
     &                      C , NC , IPC )
               CALL CXPPRD( MXTERM , B , NB , IPB , C , NC , IPC ,
     &                      A , NA , IPA )
               UA  = 2.0D0 * UA + S
               IPA = IPA - 2
               CALL CXPINT( MXTERM , A , NA , IPA , UA ,
     &                      C , NC , IPC , UC )
               SOVL = SOVL + C(1) * DFLOAT( 2 * IL2 + 1 )
    1       CONTINUE
C
         ENDIF
C
C-----------------------------------------------------------------------
C
C
         CALL CXRWFH( MXTERM , N1 , L1 , A , NA , IPA , UA )
         CALL CXPPRD( MXTERM , A , NA , IPA , A , NA , IPA ,
     &                B , NB , IPB )
         S = 2.0D0 * UA
C
         CALL CXRWFH( MXTERM , N2 , L2 , A , NA , IPA , UA )
         CALL CXPPRD( MXTERM , A , NA , IPA , A , NA , IPA ,
     &                C , NC , IPC )
         CALL CXPPRD( MXTERM , B , NB , IPB , C , NC , IPC ,
     &                A , NA , IPA )
         UA  = 2.0D0 * UA + S
         IPA = IPA - 2
         CALL CXPINT( MXTERM , A , NA , IPA , UA ,
     &                C , NC , IPC , UC )
C
         CXOVLP = C(1) / SOVL
C
C-----------------------------------------------------------------------
C
      ELSE
C
         CXOVLP = R8PROV( N1 , L1 , N2 , L2 )
C
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXOVLP ERROR ', 32('*') //
     &        2X, 'VALUE OF ''MXNSHL'' GREATER THAN ', I2, '.' /
     &        2X, 'INCREASE PARAMETER ''MXN'' IN FUNCTION CXOVLP.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
