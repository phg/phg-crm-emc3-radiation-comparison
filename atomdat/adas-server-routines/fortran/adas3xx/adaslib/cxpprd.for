CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxpprd.for,v 1.1 2004/07/06 13:10:43 whitefor Exp $ Date $Date: 2004/07/06 13:10:43 $
CX
      SUBROUTINE CXPPRD( MXTERM ,
     &                   A      , NA     , IPA    ,
     &                   B      , NB     , IPB    ,
     &                   C      , NC     , IPC
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXPPRD *********************
C
C  PURPOSE: CALCULATES PRODUCT OF TWO POWER SERIES OF THE FORM:
C
C           X**IPA * ( A(1) + X * A(2) + .... )
C                     <------ NA TERMS ------>
C
C  CALLING PROGRAM: ADAS308.
C
C  INPUT : (I*4)  MXTERM  = MAXIMUM NUMBER OF TERMS.
C  INPUT : (I*4)  NA      = NUMBER OF TERMS IN FIRST POWER SERIES.
C  INPUT : (I*4)  IPA     = POWER OF LEADING TERM IN FIRST SERIES.
C  INPUT : (R*8)  A()     = POWER SERIES COEFFICIENTS IN FIRST SERIES.
C  INPUT : (I*4)  NB      = NUMBER OF TERMS IN SECOND POWER SERIES.
C  INPUT : (I*4)  IPB     = POWER OF LEADING TERM IN SECOND SERIES.
C  INPUT : (R*8)  B()     = POWER SERIES COEFFICIENTS IN SECOND SERIES.
C
C  OUTPUT: (I*4)  NC      = NUMBER OF TERMS IN POWER SERIES.
C  OUTPUT: (I*4)  IPC     = POWER OF LEADING TERM.
C  OUTPUT: (R*8)  C()     = POWER SERIES COEFFICIENTS.
C
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C          (I*4)  JU      = LOOP LIMIT.
C          (I*4)  JL      = LOOP LIMIT.
C
C          (R*8)  X       = STORE FOR SUM WHEN CALCULATING C(I).
C
C ROUTINES: NONE
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     01/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXTERM  , NA      , IPA     , NB      , IPB     ,
     &           NC      , IPC
      INTEGER    I       , J       , JU      , JL
C-----------------------------------------------------------------------
      REAL*8     X
C-----------------------------------------------------------------------
      REAL*8     A(MXTERM)  , B(MXTERM)  , C(MXTERM)
C-----------------------------------------------------------------------
C
      IPC = IPA + IPB                                                   0000000
      NC  = MIN0( NA+NB-1 , MXTERM )                                    0000000
C
      DO 6 I = 1 , NC                                                   0000000
C
         JU = MIN0( NA , I )                                            0000000
         JL = MAX0( 1 , I-NB+1 )                                        0000000
         X  = 0.0D+00                                                   0000000
C
         DO 5 J = JL , JU                                               0000000
            X = X + A(J) * B(I-J+1)                                     0000000
    5    CONTINUE
C
         C(I) = X                                                       0000000
C
    6 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN                                                            0000000
      END                                                               0000000
