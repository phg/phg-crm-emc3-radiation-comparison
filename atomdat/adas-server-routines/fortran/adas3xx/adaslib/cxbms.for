      SUBROUTINE CXBMS(DSNIN,NSITYP,IOUNIT,SIFRAC,UBMENG,UTDENS,
     &  	       UTTEMP,NREQ,MXREQS,BSTOT)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C       ********** FORTRAN 77 SUBROUTINE: CXBMS **********
C
C	PURPOSE: TO ASSEMBLE COMPOSITE BEAM STOPPING OF EMISSION
C		 COEFFICIENTS USING THE LINEAR INTERPOLATION
C		 AND COMBINATION METHOD.
C
C
C	INPUT
C
C	(R*8)  UBMENG	: USER REQUESTED NEUTRAL BEAM ENERGIES
C			  UNITS: EV/AMU
C			  DIMENSION: NREQ
C	(R*8)  UTDENS	: USER REQUESTED TARGET DENSITIES
C			  UNITS: CM-3
C			  DIMENSION: NREQ
C	(R*8)  UTTEMP	: USER REQUESTED TARGET TEMPERATURES
C 			  UNITS: EV
C			  DIMENSION: NREQ
C	(R*8)  SIFRAC   : FRACTIONAL IMPURITY CONTENT.
C			  1ST DIMENSION: MXREQS
C                         2ND DIMENSION: NSITYP
C	(I*4)  NSITYP	: NUMBER OF PLASMA IMPURITY IONS.
C	(I*4)  NREQ	: NUMBER OF REQUESTED ENERGIES,
C		          DENSITIES AND TEMPERATURES. 
C	(I*4)  MXREQS   : SIZE OF FIRST DIMENSION OF SIFRAC
C
C	(I*4)  IOUNIT	: UNIT NUMBER EMPLOYED TO READ ADF21
C			  AND ADF22 TYPE FILES.
C
C	(CHR)  DSNIN()	: ARRAY CONTAINING NAME OF EACH FILE TO BE READ.
C                         DIMENSION: NSITYP
C
C
C       OUTPUT
C
C       (R*8)  BSTOT()  : TOTAL BEAM STOPPING COEFFICIENTS.
C                         DIMENSION: NREQ
C
C	GENERAL
C
C       (R*8)  BEREF()  : REFERENCE BEAM ENERGIES.
C                         UNITS: EV/AMU
C                         DIMENSION: MXIT
C       (R*8)  TDREF()  : REFERENCE TARGET DENSITIES.
C                         UNITS: CM-3
C                         DIMENSION: MXIT
C       (R*8)  TTREF()  : REFERENCE TARGET TEMPERATURES.
C                         UNITS: EV
C                         DIMENSION: MXIT
C       (R*8)  SVREF()  : STOPPING COEFFT. AT REFERENCE BEAM ENERGY,
C                         TARGET DENSITY AND TEMPERATURE.
C                         UNITS: CM3 S-1
C                         DIMENSION: MXIT
C       (R*8)  BE(,)    : BEAM ENERGIES.
C                         UNITS: EV/AMU
C                         1ST DIMENSION: MXBE
C                         2ND DIMENSION: MXIT
C       (R*8)  TDENS(,) : TARGET DENSITIES.
C                         UNITS: CM-3
C                         1ST DIMENSION: MXTD
C                         2ND DIMENSION: MXIT
C       (R*8)  TTEMP(,) : TARGET TEMPERATURES.
C                         UNITS: EV
C                         1ST DIMENSION: MXTT
C                         2ND DIMENSION: MXIT
C       (R*8)  SVT(,)   : STOPPING COEFFT. AT REFERENCE BEAM ENERGY
C                         AND TARGET DENSITY.
C                         UNITS: CM3 S-1
C                         1ST DIMENSION: MXTT
C                         2ND DIMENSION: MXIT
C       (R*8)  SVED(,,) : STOPPING COEFFT. AT REFERENCE TARGET
C                         TEMPERATURE.
C                         UNITS: CM3 S-1
C                         1ST DIMENSION: MXBE
C                         2ND DIMENSION: MXTD
C                         3RD DIMENSION: MXIT
C       (R*8)  BSION(,) : BEAM STOPPING COEFFICIENTS FOR INDIVIDUAL
C                         IONS.
C                         1ST DIMENSION: MXREQ
C                         2ND DIMENSION: MXIT
C
C	(R*8)  FACT1    : FACTOR USED IN CALCULATING ZEFF
C	(R*8)  FACT2    : SIMILAR TO FACT1.
C	(R*8)  ZEFF()	: USED SO THAT THE BEAM STOPPING
C                         COEFFICIENTS FOR INDIVUDUAL
C			  IMPURITY IONS ARE EVALUATED AT THE
C                         CORRECT EFFECTIVE ELECTRON DENSITY.
C	(R*8)  WT	: WEIGHTING FACTOR.
C	(R*8)  TFRAC	: GENERAL VARIABLE.
C
C	(I*4)  MXREQ    : MAXIMUM NUMBER OF REQUESTED ENERGIES
C			  DENSITIES AND TEMPERATURES 
C	(I*4)  MXIT     : MAXIMUM POSSIBLE NUMBER OF DIFFERENT
C			  PLASMA IONS.
C	(I*4)  MXBE	: MAXIMUM NUMBER OF BEAM ENERGIES WHICH CAN
C			  BE READ FROM THE ADF21/22 TYPE FILES.
C	(I*4)  MXTD	: MAXIMUM NUMBER OF TARGET DENSITIES WHICH CAN
C			  BE READ FROM THE ADF21/22 TYPE FILES.
C	(I*4)  MXTT	: MAXIMUM NUMBER OF TARGET TEMPERATURES WHICH
C			  CAN BE READ FROM THE ADF21/22 TYPE FILES.
C       (I*4)  ITZ()    : TARGET ION CHARGE.
C                         DIMENSION: MXIT
C       (I*4)  NBE()    : NUMBER OF BEAM ENERGIES.
C                         DIMENSION: MXIT
C       (I*4)  NTDENS() : NUMBER OF TARGET DENSITIES.
C                         DIMENSION: MXIT
C       (I*4)  NTTEMP() : NUMBER OF TARGET TEMPERATURES.
C                         DIMENSION: MXIT
C
C       (CHR)  TSYM()   : TARGET ION ELEMENT SYMBOL.
C                         DIMENSION: MXIT
C
C       (L*4)  LIBMA(,) : FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                         USED FOR REQUESTED BEAM ENERGIES.
C                         .TRUE.  => INTERPOLATION USED.
C                         .FALSE. => EXTRAPOLATION USED.
C                         1ST DIMENSION: MXREQ
C                         2ND DIMENSION: MXIT
C			  NOTE: USE OF FLAGS NOT IMPLEMENTED
C       (L*4)  LIDNA(,) : FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                         USED FOR REQUESTED ION DENSITIES.
C                         .TRUE.  => INTERPOLATION USED.
C                         .FALSE. => EXTRAPOLATION USED.
C                         1ST DIMENSION: MXREQ
C                         2ND DIMENSION: MXIT
C			  NOTE: USE OF FLAGS NOT IMPLEMENTED
C       (L*4)  LITIA(,) : FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                         USED FOR REQUESTED ION TEMPERATURES.
C                         .TRUE.  => INTERPOLATION USED.
C                         .FALSE. => EXTRAPOLATION USED.
C                         1ST DIMENSION: MXREQ
C                         2ND DIMENSION: MXIT
C			  NOTE: USE OF FLAGS NOT IMPLEMENTED
C
C 	ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          C4DATA     ADAS      READS INPUT DATA SET IN ADF21/22 FORMAT.
C          C4SPLN     ADAS      PERFORMS SPLINE ON INPUT DATA.
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C
C
C 	NOTE  : THE IMPURITY FRACTIONS ARE RENORMNALISED TO
C		ENSURE THAT THE TOTAL SUM OF EACH IMPURITY 
C	        FRACTION DOES NOT EXCEED THE VALUE OF ONE. 
C	
C      
C  	AUTHOR: HARVEY ANDERSON
C	        UNIVERSITY OF STRATHCLYDE
C		ANDERSON@PHYS.STRATH.AC.UK
C
C	DATE  : 30/09/99
C
C	VERSION : 1.1
C       DATE    : 3-6-2000
C	MODIFIED: Martin O'Mullane
C		  Repositioned declaration of variables into standard
C                 ADAS convention and to satisfy g77. 	
C
C	VERSION : 1.2
C       DATE    : 19-3-2003
C	MODIFIED: Lorne Horton
C		  Increased MXREQ to 1024.  Added check on internal
C                 matrix sizes. Added LSET flag to allow faster
C                 splines on repeated calls to C4SPLN.
C
C	VERSION : 1.3
C       DATE    : 03-12-2004
C	MODIFIED: Martin O'Mullane
C		   - Replace c4data with xxdata_21. 	
C		   - Increase dsnin length to 132 characters. 	
C		   - Merge L Horton's changes into central version. 	
C 
C	VERSION : 1.4
C       DATE    : 08-12-2004
C	MODIFIED: Martin O'Mullane/Allan Whiteford
C		   - Increase size of dsnsav to 132 and set initial
C                    values to ' ' rather than ''. 	
C 
C-----------------------------------------------------------------------
	INTEGER  MXREQ , MXIT , MXBE , MXTD , MXTT
C-----------------------------------------------------------------------
	PARAMETER(MXREQ=1024, MXIT=10, MXBE=25, MXTD=25, MXTT=25)
C-----------------------------------------------------------------------
	INTEGER  NSITYP		, IOUNIT	, I 		, 
     &		 NREQ		, MXREQS        , J
C-----------------------------------------------------------------------
        INTEGER  I4UNIT
C-----------------------------------------------------------------------
	INTEGER  ITZ(mxit)	, NBE(mxit)	,NTDENS(mxit)	,
     &	         NTTEMP(mxit) 
C-----------------------------------------------------------------------
       LOGICAL   LIBMA(mxreq,mxit)      , LIDNA(mxreq,mxit)      ,
     &           LITIA(mxreq,mxit)      , LSET
C-----------------------------------------------------------------------
	REAL*8   BEREF(mxit)	, TDREF(mxit)	, TTREF(mxit)	, 
     &		 SVREF(mxit)	, BE(MXBE,mxit)	, TDENS(MXTD,mxit), 
     &	         TTEMP(MXTT,mxit)     , SVT(MXTT,mxit),
     &           SIFRAC(mxreqs,nsityp), BSTOT(nreq)
        REAL*8   SVED(MXBE,MXTD,mxit) ,  FACT1	, FACT2         ,
     &           ZEFF(mxreq)    ,
     &		 UBMENG(NREQ)   , UTDENS(NREQ)  , UTTEMP(NREQ)	,
     &		 BSION(mxreq,mxit)    ,  WT	, TFRAC
C-----------------------------------------------------------------------
	CHARACTER  DSNIN(nsityp)*132 , TSYM(mxit)*2 , DSNSAV(mxit)*132
C-----------------------------------------------------------------------
        DATA     LSET /.FALSE./
        DATA     DSNSAV /MXIT*' '/
        SAVE     DSNSAV
        SAVE     ITZ      , TSYM,
     &           BEREF    , TDREF    , TTREF    , SVREF   ,
     &           NBE      , BE       , NTDENS   , TDENS   ,
     &           NTTEMP   , TTEMP    , SVT      , SVED
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXREQ' AND 'MXIT'.
C-----------------------------------------------------------------------

      IF ( MXREQ .LT. NREQ ) THEN
         WRITE(I4UNIT(-1),1000) 'MXREQ' , 'NREQ' ,
     &                           MXREQ  ,  NREQ  , 'MXREQ'
         STOP
      ENDIF

      IF ( MXIT .LT. NSITYP ) THEN
         WRITE(I4UNIT(-1),1000) 'MXIT' , 'NSITYP' ,
     &                           MXIT  ,  NSITYP  , 'MXIT'
         STOP
      ENDIF

C-----------------------------------------------------------------------
C       RENORMALISE IMPURITY ION FRACTIONS TO ELIMINATE ERROR
C-----------------------------------------------------------------------

      DO J=1, NREQ

        TFRAC = 0.0
        DO I=1, NSITYP
          TFRAC = TFRAC + SIFRAC(J,I)
        ENDDO

        DO I=1, NSITYP
          SIFRAC(J,I) = ( SIFRAC(J,I) / TFRAC )
        ENDDO
 
      ENDDO

C-----------------------------------------------------------------------
C CHECK TO SEE IF ANY OF THE INPUT FILES HAVE CHANGED. IF SO, FORCE
C A COMPLETE RE-READ AND RE-SPLINE
C-----------------------------------------------------------------------

      DO I = 1, NSITYP
         IF (DSNIN(I) .NE. DSNSAV(I)) LSET = .TRUE.
         DSNSAV(I) = DSNIN(I)
      END DO

C-----------------------------------------------------------------------
C
C    OPEN AND EXTRACT EFFECTIVE COEFFICIENTS FOR EACH IMPURITY
C    OF INTEREST FROM THE ADF21/ADF22 TYPE FILES.
C
C-----------------------------------------------------------------------

      IF (LSET) THEN

         DO  I = 1 , NSITYP

            CALL xxdata_21( IOUNIT      , MXBE        , MXTD        ,
     &                      MXTT        , ITZ(I)      , TSYM(I)     ,
     &                      BEREF(I)    , TDREF(I)    , TTREF(I)    ,
     &                      SVREF(I)    , NBE(I)      , BE(1,I)     ,
     &                      NTDENS(I)   , TDENS(1,I)  , NTTEMP(I)   ,
     &                      TTEMP(1,I)  , SVT(1,I)    , SVED(1,1,I) ,
     &                      DSNIN(I)   
     &                    )

            CLOSE( IOUNIT )

          ENDDO

      ENDIF

C-----------------------------------------------------------------------
C
C	PRODUCED CORRECT WEIGHT (ZEFF) TO ENABLE THE BEAM STOPPING
C	COEFFICIENT TO BE EVALUATED AT THE EFFECTIVE DENSITY.
C
C-----------------------------------------------------------------------

      DO J = 1, NREQ
        FACT1 = 0.0
        FACT2 = 0.0
        DO I = 1, NSITYP		
	  FACT1= ( ITZ(I)**2 ) * SIFRAC(J,I) + FACT1
          FACT2=   ITZ(I)*SIFRAC(J,I) + FACT2
        END DO
        ZEFF(J) = FACT1/FACT2
      END DO

C-----------------------------------------------------------------------
C 		PERFORM SPLINE ON USER-DEFINED VLAUES.
C-----------------------------------------------------------------------

      CALL C4SPLN( MXBE   , MXTD   , MXTT   , MXREQ  ,
     &             NREQ   , UBMENG , UTDENS , UTTEMP ,
     &             NSITYP , SVREF  , NBE    , BE     ,
     &             NTDENS , TDENS  , NTTEMP , TTEMP  ,
     &             SVT    , SVED   , BSION  , LIBMA  ,
     &             LIDNA  , LITIA  , ZEFF   , ITZ    ,
     &             LSET   )

C-----------------------------------------------------------------------
C
C    WEIGHT STOPPING ION COEFFICIENTS. THE TOTAL STOPPING COEFFICIENT 
C    IS EVALUATED IN TERMS OF THE ELECTRON DENSITY SINCE ADAS310 
C    GENERATES STOPPING COEFFICIENTS IN TERMS OF THE ELECTRON DENSITY
C
C-----------------------------------------------------------------------

       DO 6 J=1,NREQ
		WT=0.0
       DO 5 I=1,NSITYP
		BSION(J,I)=BSION(J,I)*SIFRAC(J,I)*ITZ(I)
 		WT=WT+( ITZ(I)*SIFRAC(J,I) )
    5 CONTINUE
       DO 22 I=1,NSITYP
	     BSION(J,I)=BSION(J,I)/WT
   22 CONTINUE
    6 CONTINUE	

C-----------------------------------------------------------------------
C          CALCULATE TOTAL STOPPING ION COEFFICIENT AT 
C	   REQUIRED SET OF PLASMA CONDITIONS.
C-----------------------------------------------------------------------

      DO 7 I = 1 , NREQ
         BSTOT(I) = 0.0D0
         DO 8 J = 1 , NSITYP
            BSTOT(I) = BSTOT(I) + BSION(I,J)
    8    CONTINUE
    7 CONTINUE

C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 33('*'), ' CXBMS ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''', A,
     &            ''' IS LESS THAN INPUT ARGUMENT ''', A, '''.'/
     &        2X, 'INTERNAL PARAM = ', I3 , '   INPUT ARGUMENT = ', I3/
     &        2X, 'INCREASE PARAMETER ''', A,
     &            ''' IN SUBROUTINE CXBMS.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C-----------------------------------------------------------------------

      RETURN 
      END
