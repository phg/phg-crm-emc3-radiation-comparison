CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxsgei.for,v 1.1 2004/07/06 13:11:14 whitefor Exp $ Date $Date: 2004/07/06 13:11:14 $
CX
      SUBROUTINE CXSGEI( MXNSHL , IZ1    , ZT     , ZT1    , THETA  ,
     &                   VEL    , NA     , LA     , NB     ,
     &                   XSECNA , FRACLA
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXSGEI *********************
C
C  PURPOSE:  USES THE EIKONAL APPROXIMATION TO CALCULATE CHARGE EXCHANGE
C            CROSS-SECTIONS INTO N'L' RESOLVED EXCITED STATES OF
C            HYDROGENIC IONS IN CAPTURE FROM HYDROGEN NL STATES.
C
C            THE SUBROUTINE CAN BE USED FOR CAPTURE FROM HELIUM BY
C            APPROPRIATE CHOICE OF ZT AND ZT1.
C
C            FOR HYDROGEN, ZT = ZT1 = 1.0
C            FOR HELIUM  , ZT = ZT1 = 1.6875
C
C            EICHLER (1981) PHYS.REV.A,23,498.
C
C  CALLING PROGRAM: ADAS308.
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = HYDROGENIC ION CHARGE (PROJECTILE).
C  INPUT : (R*8)  ZT       = SCREENING CHARGE FOR THE 1S ELECTRON OF THE
C                            TARGET ATOM IN THE INITIAL STATE.
C  INPUT : (R*8)  ZT1      = EFFECTIVE CHARGE FOR THE 1S ELECTRON OF THE
C                            TARGET ATOM IN THE FINAL STATE.
C  INPUT : (R*8)  THETA    = PARAMETER TO GIVE CORRECT BINDING ENERGY
C                            FOR INITIAL TARGET STATE.
C  INPUT : (R*8)  VEL      = COLLISION SPEED.
C                            UNITS: AT. UNITS
C  INPUT : (I*4)  NA       = PRINCIPAL QUANTUM NUMBER OF INTIAL STATE OF
C                            TARGET FROM WHICH CAPTURE IS MADE.
C  INPUT : (I*4)  LA       = ORBITAL QUANTUM NUMBER OF INTIAL STATE OF
C                            TARGET FROM WHICH CAPTURE IS MADE.
C  INPUT : (I*4)  NB       = PRINCIPAL QUANTUM NUMBER OF FINAL STATES OF
C                            PROJECTILE TO WHICH CAPTURE IS MADE.
C
C  OUTPUT: (R*8)  XSECNA   = N-RESOLVED CROSS-SECTION FOR CAPTURE.
C                            UNITS: AT. UNITS
C  OUTPUT: (R*8)  FRACLA() = L-RESOLVED CROSS-SECTION AS A FRACTION OF
C                            CORRESPONDING N-RESOLVED CROSS-SECTION.
C                            DIMENSION: REFERENCED BY L QUANTUM NUMBER.
C
C  PARAM : (I*4)  MXN      = 'MXNSHL'.
C  PARAM : (R*8)  PI       = PI.
C
C          (I*4)  LB       = ORBITAL QUANTUM NUMBER OF FINAL STATE OF
C                            PROJECTILE.
C          (I*4)  I        = LOOP INDEX.
C          (I*4)  K        = LOOP INDEX.
C          (I*4)  IU       = LOOP INDEX.
C          (I*4)  MB       = LOOP INDEX.
C          (I*4)  MB1      = LOOP INDEX.
C          (I*4)  IND      =
C          (I*4)  IL       = LOOP INDEX.
C          (I*4)  IG       = LOOP INDEX.
C          (I*4)  IWM      = LOOP INDEX.
C          (I*4)  IW1      = LOOP INDEX.
C          (I*4)  IW2      = LOOP INDEX.
C          (I*4)  IW3      = LOOP INDEX.
C          (I*4)  IW4      = LOOP INDEX.
C          (I*4)  IBA      = LOOP INDEX.
C          (I*4)  IVA      = LOOP INDEX.
C          (I*4)  ITA      = LOOP INDEX.
C          (I*4)  IAM      =
C          (I*4)  IA1      = LOOP INDEX.
C          (I*4)  IA2      = LOOP INDEX.
C          (I*4)  IA3      =
C          (I*4)  ISA      =
C          (I*4)  IBB      = LOOP INDEX.
C          (I*4)  IVB      = LOOP INDEX.
C          (I*4)  ITB      = LOOP INDEX.
C          (I*4)  IAM      =
C          (I*4)  IB1      = LOOP INDEX.
C          (I*4)  IB2      = LOOP INDEX.
C          (I*4)  IB3      =
C          (I*4)  ISB      =
C          (I*4)  IP       = LOOP INDEX.
C
C          (R*8)  Z1       = REAL VALUE = IZ1.
C          (R*8)  XNA      = REAL VALUE = N.
C          (R*8)  XNB      = REAL VALUE = N.
C          (R*8)  ETA      =
C          (R*8)  EPS      =
C          (R*8)  PM       =
C          (R*8)  PP       =
C          (R*8)  QNA      =
C          (R*8)  QNB      =
C          (R*8)  QP       =
C          (R*8)  ZT12     = ZT12**2.
C          (R*8)  T1       =
C          (R*8)  T2       =
C          (R*8)  T3       =
C          (R*8)  T4       =
C          (R*8)  T5       =
C          (R*8)  T6       =
C          (R*8)  T7       =
C          (R*8)  T8       =
C          (R*8)  SUM      =
C          (R*8)  SUM1     =
C          (R*8)  BB       =
C          (R*8)  BB1      =
C
C          (Z*16) CTA      =
C          (Z*16) CTB      =
C          (Z*16) CT1      =
C          (Z*16) CSUM     =
C
C          (R*8)  B()      =
C          (R*8)  F()      =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4JGAM     ADAS      RETURNS VALUE FROM 'JGAM' TABLE.
C          R8GAM      ADAS      RETURNS VALUE FROM 'GAM' TABLE.
C          Z16CD      ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    06/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4JGAM
      REAL*8     R8GAM
      COMPLEX*16 Z16CD
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      REAL*8     PI
      PARAMETER( PI = 3.141593D0 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , IZ1     , NA      , LA      , NB
      INTEGER    LB      , I       , K       , IU      , MB      ,
     &           MB1     , IND     , IL      , IG      , IWM     ,
     &           IW1     , IW2     , IW3     , IW4     , IBA     ,
     &           IVA     , ITA     , IAM     , IA1     , IA2     ,
     &           IA3     , ISA     , IBB     , IVB     , ITB     ,
     &           IBM     , IB1     , IB2     , IB3     , ISB     ,
     &           IP
C-----------------------------------------------------------------------
      REAL*8     ZT      , ZT1     , THETA   , VEL     , XSECNA
      REAL*8     Z1      , XNA     , XNB     , ETA     , EPS     ,
     &           PM      , PP      , QNA     , QNB     , QP      ,
     &           ZT12    , T1      , T2      , T3      , T4      ,
     &           T5      , T6      , T7      , T8      , SUM     ,
     &           SUM1    , BB      , BB1
C-----------------------------------------------------------------------
      COMPLEX*16 CTA     , CTB     , CT1     , CSUM
C-----------------------------------------------------------------------
      REAL*8     FRACLA(MXNSHL)
      REAL*8     B(MXN)         , F(2*MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXN' LARGE ENOUGH.
C-----------------------------------------------------------------------
C
      IF (MXNSHL .GT. MXN) THEN
         WRITE(I4UNIT(-1),1000) MXN
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      Z1  = DFLOAT( IZ1 )
      XNA = DFLOAT( NA )
      XNB = DFLOAT( NB )
      ETA = 1.0D0 / VEL
      EPS = 0.5D0 * ( THETA * ZT**2 / XNA**2 - Z1**2 / XNB**2 )
      PM  = EPS * ETA - 0.5D0 * VEL
      PP  = EPS * ETA + 0.5D0 * VEL
      QNA = ZT / XNA
      QNB = Z1 / XNB
      QP  = QNA**2 + PM**2
      ZT12 = ZT1**2
C
      T5 = 2.0D0**( 8 - LA ) * PI * Z1**5
     &     * ( ZT / ( XNA * QP ) )**( 5 + 2 * LA ) / ( XNB**3 * VEL**2 )
C
      IF (ZT1 .GT. 0.0D0) THEN
       T6 = PI * ETA * ZT1
     &      * DEXP( -2.0D0 * ETA * ZT1 * DATAN( -PM / QNA ) )
     &      / DSINH( PI * ETA * ZT1 )
      ELSE
       T6 = 1.0D0
      ENDIF
C
C-----------------------------------------------------------------------
C
      XSECNA = 0.0D0
      DO 1 LB = 0 , NB-1
C
       T8 = 16.0D0**LB * ( 2.0D0 * DFLOAT( LB ) + 1.0D0 )
     &      * R8GAM( NB + LB + 1 ) * R8GAM( LB + 1 ) * R8GAM( LB + 1 )
     &      / ( XNB * R8GAM( NB - LB ) * R8GAM( 2 * LB + 2 )
     &        * R8GAM( 2 * LB + 2 ) )
     &      * 64.0D0**( I4JGAM( NB + LB + 1 ) + 2 * I4JGAM( LB + 1 )
     &                - I4JGAM( NB - LB ) - 2 * I4JGAM( 2 * LB + 2 ) )
C
       B(1) = 1.0D0
       DO 2 I = 1 , NB-LB
        B(I+1) = DFLOAT( -NB**2 + (LB + I)**2 ) * B(I)
     &           / ( ( 0.5D0 + DFLOAT( LB + I ) ) * DFLOAT( I ) )
    2  CONTINUE
C
       DO 3 K = 1 , 2*NB-1
C
        SUM = 0.0D0
        DO 4 IU = 0 , LB
         T7 = ( -1.0D0 )**IU
     &        * 64.0D0**( I4JGAM( LB + 1 ) - I4JGAM( LB - IU + 1 )
     &                  - I4JGAM( IU + 1 ) )
     &        * R8GAM( LB + 1 )
     &        / ( R8GAM( LB - IU + 1 ) * R8GAM( IU + 1 ) )
C
         SUM1 = 0.0D0
         DO 5 MB = 0 , NB-LB-1
          BB = B(MB+1)
          DO 6 MB1 = 0 , NB-LB-1
           BB1 = B(MB1+1)
           IND = LB + IU + MB + MB1
           IF (IND .EQ. (K - 1)) SUM1 = SUM1 + BB * BB1
    6     CONTINUE
    5    CONTINUE
C
         SUM = SUM + T7 * SUM1
C
    4   CONTINUE
C
        SUM  = SUM * T8
        F(K) = SUM
C
    3  CONTINUE
C
       CSUM = ( 0.0D0 , 0.0D0 )
       DO 7 IL = LB , ( 2 * NB - 2 )
C
        T4 = F(IL+1) * QNB**( 2 * IL )
C
        DO 8 IG = 0 , LA / 2
C
         T1 = (-1.0D0)**IG * R8GAM( 2 * LA - 2 * IG + 1 )
     &        / ( R8GAM( LA - IG + 1 ) * R8GAM( LA - 2 * IG + 1 )
     &        * R8GAM( IG + 1 ) )
     &        * 64.0D0**( I4JGAM( 2 * LA - 2 * IG + 1 )
     &                    - I4JGAM( LA - IG + 1 )
     &                    - I4JGAM( LA - 2 * IG + 1 )
     &                    - I4JGAM( IG + 1 ) )
C
         IWM = LA - 2 * IG
         DO 11 IW1 = 0 , IWM
          DO 12 IW2 = 0 , IWM-IW1
           DO 13 IW3 = 0 , IWM-IW1-IW2
            IW4 = IWM - IW1 - IW2 - IW3
C
            T2 = R8GAM( IWM + 1 )
     &           / ( R8GAM( IW1 + 1 ) * R8GAM( IW2 + 1 )
     &               * R8GAM( IW3 + 1 ) * R8GAM( IW4 + 1 ) )
     &           * 64.0D0**( I4JGAM( IWM + 1 ) - I4JGAM( IW1 + 1 )
     &                       - I4JGAM( IW2 + 1 ) - I4JGAM( IW3 + 1 )
     &                       - I4JGAM( IW4 + 1 ) )
C
            CT1 = ( 0.0D0 , 1.0D0 )**( IW3 - IW2 )
C
            DO 21 IBA = 0 , NA-LA-1
             DO 22 IVA = 0 , ( IBA + 1 ) / 2
              DO 23 ITA = 0 , ( IBA + 1 - 2 * IVA )
               IAM = IG + IVA
               DO 24 IA1 = 0 , IAM
                DO 25 IA2 = 0 , IAM-IA1
C
                 IA3 = IAM - IA1 - IA2
                 ISA = ITA + IW2 + IW4 + IA2 + 2 * IA3
                 CTA = Z16CD( IBA , IVA , IA1 , IA2 , IA3 ,
     &                        ITA , IG  , ISA , QNA , PM  ,
     &                        ZT1 , ETA , NA  , LA          )
C
                 DO 31 IBB = 0 , NA-LA-1
                  DO 32 IVB = 0 , ( IBB + 1 ) / 2
                   DO 33 ITB = 0 , IBB + 1 - 2 * IVB
                    IBM = IG + IVB
                    DO 34 IB1 = 0 , IBM
                     DO 35 IB2 = 0 , IBM-IB1
C
                      IB3 = IBM - IB1 - IB2
                      ISB = ITB + IW3 + IW4 + IB2 + 2 * IB3
                      CTB = Z16CD( IBB  , IVB  , IB1 , IB2 , IB3 ,
     &                             ITB  , IG   , ISB  , QNA  , PM   ,
     &                             ZT1  , ETA  , NA   , LA            )
C
                      DO  41 IP = 0 , IW1+IA1+IB1
C
                       T3 = ( -1.0D0 )**( IW1 - IP )
     &                      * R8GAM( IW1 + IA1 + IB1 + 1 )
     &                      / ( R8GAM( IW1 + IA1 + IB1 - IP + 1 )
     &                      * R8GAM( IP + 1 )
     &                      * ( 5 + 2 * LA + IL + IBA + IBB - ISA - ISB
     &                          - IP ) )
     &                      * 64.0D0**( I4JGAM( IW1 + IA1 + IB1 + 1 )
     &                      - I4JGAM( IW1 + IA1 + IB1 - IP + 1 )
     &                      - I4JGAM( IP + 1 ) )
     &                      * QNA**( 2 * IW1 - 2 * IP )
     &                      * PM**( IW2 + IW3 )
     &                      * QP**( IP - IBA - IBB - IL )
C
                            CSUM = CSUM + T4 * T1 * CT1 * T2 * T3 * CTA
     &                             * DCONJG( CTB )
C
   41                 CONTINUE
C
   35                CONTINUE
   34               CONTINUE
   33              CONTINUE
   32             CONTINUE
   31            CONTINUE
C
   25           CONTINUE
   24          CONTINUE
   23         CONTINUE
   22        CONTINUE
   21       CONTINUE
C
   13      CONTINUE
   12     CONTINUE
   11    CONTINUE
C
    8   CONTINUE
    7  CONTINUE
C
       FRACLA(LB+1) = T5 * T6 *DREAL( CSUM )
       XSECNA = XSECNA + FRACLA(LB+1)
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      DO 9  LB = 0 , NB-1
       FRACLA(LB+1) = FRACLA(LB+1) / XSECNA
    9 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXSGEI ERROR ', 32('*') //
     &        2X, 'VALUE OF ''MXNSHL'' GREATER THAN ', I2, '.' /
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE CXSGEI.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
