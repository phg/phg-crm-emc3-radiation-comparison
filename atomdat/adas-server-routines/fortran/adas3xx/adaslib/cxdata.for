CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxdata.for,v 1.2 2004/07/06 13:10:09 whitefor Exp $ Data $Date: 2004/07/06 13:10:09 $
CX
       SUBROUTINE CXDATA( IUNIT  , MXNENG , MXNSHL , TITLED ,
     &                    SYMBR  , SYMBD  , IZR    , IZD    ,
     &                    INDD   , NENRGY , NMIN   , NMAX   ,
     &                    LPARMS , LSETL  , LSETM  , ENRGYA ,
     &                    ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &                    PL3A   , SIGTA  , SIGNA  , SIGLA  ,
     &                    SIGMA
     &                  )
C
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXDATA *********************
C
C  PURPOSE:  TO FETCH DATA FROM INPUT DATA SET OF TYPE ADF01.
C
C  CALLING PROGRAM: ADAS301/ADAS306/ADAS307/ADAS308/ADAS309
C
C  DATA:
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           COLLISION ENERGIES  : KEV/AMU
C           ALPHA               :
C           TOTAL XSECTS.       : CM2
C           N-SHELL XSECTS.     : CM2
C           NL-SHELL DATA       : CM2
C           NLM-SHELL DATA      : CM2
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C
C  OUTPUT: (C*80) TITLED    = NOT SET - TITLE FOR DATA SOURCE.
C  OUTPUT: (C*2)  SYMBR     = READ - RECEIVER ION ELEMENT SYMBOL.
C  OUTPUT: (C*2)  SYMBD     = READ - DONOR ION ELMENT SYMBOL.
C  OUTPUT: (I*4)  IZR       = READ - ION CHARGE OF RECEIVER.
C  OUTPUT: (I*4)  IZD       = READ - ION CHARGE OF DONOR.
C  OUTPUT: (I*4)  INDD      = READ - DONOR STATE INDEX.
C  OUTPUT: (I*4)  NENRGY    = NUMBER OF ENERGIES READ.
C  OUTPUT: (I*4)  NMIN      = LOWEST N-SHELL FOR WHICH DATA READ.
C  OUTPUT: (I*4)  NMAX      = HIGHEST N-SHELL FOR WHICH DATA READ.
C  OUTPUT: (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C  OUTPUT: (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C  OUTPUT: (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C
C  OUTPUT: (R*8)  ENRGYA()  = READ - COLLISION ENERGIES.
C                             UNITS: EV/AMU (READ AS KEV/AMU)
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  ALPHAA()  = READ - EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (I*4)  LFORMA()  = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  XLCUTA()  = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  PL2A()    = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  PL3A()    = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  SIGTA()   = READ - TOTAL CHARGE EXCHANGE
C                                    CROSS-SECTION.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C  OUTPUT: (R*8)  SIGNA(,)  = READ - N-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C  OUTPUT: (R*8)  SIGLA(,)  = READ - L-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C  OUTPUT: (R*8)  SIGMA(,)  = READ - M-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFM(N,L,M)
C                                            WITH M >= 0 ONLY
C
C	   (R*8)  ZEROST   = PARAMETER = EFFECTIVE SHIFT APPLIED TO
C					 CROSS-SECTION VALUES TO AVOID
C					 ZERO VALUES (WILL NOT AFFECT
C					 ANY VALUES WHICH ARE GREATER
C					 THAN AROUND 1.0E+15*ZEROSHFT -
C					 i.e. 1.0E-25.)
C
C          (I*4)  OLDMIN   = PREVIOUS VALUE READ FOR NMIN.
C          (I*4)  OLDMAX   = PREVIOUS VALUE READ FOR NMAX.
C          (I*4)  IBLK     = CURRENT DATA BLOCK.
C          (I*4)  IVALUE   = USED TO PARSE FOR END OF DATA FLAG (-1).
C          (I*4)  N        = N QUANTUM NUMBER.
C          (I*4)  L        = L QUANTUM NUMBER.
C          (I*4)  M        = M QUANTUM NUMBER.
C          (I*4)  I        = LOOP COUNTER.
C          (I*4)  J        = LOOP COUNTER.
C          (I*4)  IERR     = ERROR RETURN CODE.
C          (C*2)  CIZR     = ION CHARGE OF RECEIVER.
C          (C*2)  CIZD     = ION CHARGE OF DONOR.
C          (C*1)  INDD     = DONOR STATE INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4FCTN     ADAS      RETURNS CHARACTER STRING AS AN INTEGER.
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C          I4IDFM     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N, L AND M.
C          XXIDTL     ADAS      INVERSE OF I4IDFL. RETURNS QUANTUM
C                               NUMBERS N AND L FROM INDEX.
C          XXIDTM     ADAS      INVERSE OF I4IDFM. RETURNS QUANTUM
C                               NUMBERS N, L AND M FROM INDEX.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    21/09/93
C
C UPDATE:  18/10/93 - J NASH    - ADAS91:
C          UPDATED TO READ L-SPLITTING PARAMETERS IF PRESENT IN DATASET.
C
C UPDATE:  01/05/95 - Tim Hammond - IDLADAS:
C	   UNIX port.
C
C UPDATE:  16/05/95 - Tim Hammond - IDLADAS:
C	   ADDED AND APPLIED ZEROST PARAMETER => EFFECTIVE ZERO FOR
C          CROSS-SECTIONS (CODING DONE BY PAUL BRIDEN).
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL *8   ZEROST
C-----------------------------------------------------------------------
      PARAMETER (ZEROST = 1.0D-40)
C-----------------------------------------------------------------------
      INTEGER   I4FCTN  , I4UNIT  , I4IDFL  , I4IDFM
C-----------------------------------------------------------------------
      INTEGER   IUNIT   , MXNSHL  , MXNENG   ,
     &          IZR     , IZD     , INDD     ,
     &          NENRGY  , NMIN    , NMAX
      INTEGER   NEMORE  , OLDMIN  , OLDMAX  , IBLK    , IVALUE  ,
     &          N       , L       , M       , I       , J       ,
     &          IERR
C-----------------------------------------------------------------------
      LOGICAL   LPARMS  , LSETL   , LSETM
C-----------------------------------------------------------------------
      CHARACTER TITLED*80  , SYMBR*2    , SYMBD*2
      CHARACTER CLINE*133  , CIZR*2     , CIZD*2     , CINDD*1
C-----------------------------------------------------------------------
      INTEGER   LFORMA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8    ENRGYA(MXNENG)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &          PL2A(MXNENG)    , PL3A(MXNENG)    , SIGTA(MXNENG)
      REAL*8    SIGNA(MXNENG,MXNSHL)                           ,
     &          SIGLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)            ,
     &          SIGMA(MXNENG,(MXNSHL*(MXNSHL+1)*(MXNSHL+2))/6)
C-----------------------------------------------------------------------
C
C***********************************************************************
C INITIALIZE VALUES.
C***********************************************************************
C
      IVALUE = -1
      NENRGY = 0
      NEMORE = 0
      OLDMIN = 0
      OLDMAX = 0
      IBLK = 0
      LSETL = .FALSE.
      LSETM = .FALSE.
C
C***********************************************************************
C READ IN DATA.
C***********************************************************************
C-----------------------------------------------------------------------
C READ TITLE LINE OF DATA FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) CLINE
      READ(CLINE,1001) SYMBR, CIZR, SYMBD, CIZD, CINDD
      IZR = I4FCTN( CIZR , IERR )
      IF (IERR .EQ. 0) IZD = I4FCTN( CIZD , IERR )
      IF (IERR .EQ. 0) INDD = I4FCTN( CINDD , IERR )
      IF (IERR .NE. 0) THEN
         WRITE(I4UNIT(-1),2000)
         WRITE(I4UNIT(-1),2006)
         STOP
      ENDIF
C
C CHECK FOR L-SPLITTING PARAMETERS FLAG.
C
      IF ( INDEX( CLINE , 'LPARMS' ) .NE. 0 ) THEN
         LPARMS = .TRUE.
      ELSE
         LPARMS = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C START OF READ LOOP.
C-----------------------------------------------------------------------
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C READ NEXT LINE OF DATA FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000,END=6) CLINE
C
C-----------------------------------------------------------------------
C IGNORE LINE IF COMMENT.
C-----------------------------------------------------------------------
C
      IF ((CLINE(1:1) .EQ. 'C') .OR. (CLINE(1:1) .EQ. 'c')) GOTO 1
C
C-----------------------------------------------------------------------
C CHECK FOR END OF DATA FLAG.
C-----------------------------------------------------------------------
C
      READ(CLINE,1002) IVALUE
      IF (IVALUE .GT. 0) THEN
C
C-----------------------------------------------------------------------
C EITHER HEADER FOR DATA BLOCK;
C-----------------------------------------------------------------------
C
         IF (INDEX( CLINE, '/' ) .NE. 0) THEN
C
C INCREMENT BLOCK COUNTER AND UPDATE NUMBER OF ENERGIES READ.
C
            IBLK = IBLK + 1
            NENRGY = NENRGY + NEMORE
C
C READ NUMBER OF ENERGIES IN NEXT BLOCK. REDUCE IF TOO LARGE.
C
            READ(CLINE,1002) NEMORE
            IF ((NENRGY + NEMORE) .GT. MXNENG) THEN
               WRITE(I4UNIT(-1),2001) MXNENG
               NEMORE = MXNENG - NENRGY
            ENDIF
C
C READ MAXIMUM AND MINIMUM N VALUES AND CHECK AGAINST PREVIOUS VALUES.
C
            READ(IUNIT,1002) NMIN
            IF (OLDMIN .EQ. 0) THEN
               OLDMIN = NMIN
            ELSE IF (NMIN .NE. OLDMIN) THEN
               WRITE(I4UNIT(-1),2002) IBLK, 'MINIMUM', NMIN, OLDMIN
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF
C
            READ(IUNIT,1002) NMAX
            IF (NMAX .GT. MXNSHL) THEN
               WRITE(I4UNIT(-1),2003) IBLK, NMAX, MXNSHL
               WRITE(I4UNIT(-1),2006)
               STOP
            ELSE IF (OLDMAX .EQ. 0) THEN
               OLDMAX = NMAX
            ELSE IF (NMAX .NE. OLDMAX) THEN
               WRITE(I4UNIT(-1),2002) IBLK, 'MAXIMUM', NMAX, OLDMAX
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF
C
C READ REMAINDER OF HEADER DATA.
C
            READ(IUNIT,1003) (ENRGYA(NENRGY+I), I=1,NEMORE)
            READ(IUNIT,1003) (ALPHAA(NENRGY+I), I=1,NEMORE)
C
            IF (LPARMS) THEN
               READ(IUNIT,1004) (LFORMA(NENRGY+I), I=1,NEMORE)
               READ(IUNIT,1005) (XLCUTA(NENRGY+I), I=1,NEMORE)
               READ(IUNIT,1005) (PL2A(NENRGY+I), I=1,NEMORE)
               READ(IUNIT,1005) (PL3A(NENRGY+I), I=1,NEMORE)
            ENDIF
C
            READ(IUNIT,1005) (SIGTA(NENRGY+I), I=1,NEMORE)
            READ(IUNIT,1000) CLINE
C
C INITIALIZE PARTIAL CROSS-SECTION ARRAYS FOR NEXT BLOCK TO -1.
C
            DO 2 I = NENRGY+1, NENRGY+NEMORE
C
               DO 3 J = NMIN, NMAX
                  SIGNA(I,J) = -1.0D+00
    3           CONTINUE
C
               DO 4 J = I4IDFL(NMIN,0), I4IDFL(NMAX,NMAX-1)
                  SIGLA(I,J) = -1.0D+00
    4           CONTINUE
C
               DO 5 J = I4IDFM(NMIN,0,0), I4IDFM(NMAX,NMAX-1,NMAX-1)
                  SIGMA(I,J) = -1.0D+00
    5          CONTINUE
C
    2       CONTINUE
C
C-----------------------------------------------------------------------
C OR M-RESOLVED PARTIAL CROSS SECTIONS;
C-----------------------------------------------------------------------
C
         ELSE IF (CLINE(7:9).NE.'   ') THEN
C
C READ DATA.
C
            READ(CLINE,1006) N, L, M
            IF ((N.GE.NMIN) .AND. (N.LE.NMAX)  .AND.
     &          (L.GE.0)    .AND. (L.LE.(N-1)) .AND.
     &          (M.GE.0)    .AND. (M.LE.L)          ) THEN
               READ (CLINE,1005) (SIGMA(NENRGY+I,I4IDFM(N,L,M)),
     &                            I=1,NEMORE)
            ELSE
               WRITE(I4UNIT(-1),2004) IBLK, NMIN, NMAX,
     &                                'N,L,M', N, L, M
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF
C
C SET M-RESOLVED DATA FLAG.
C
            LSETM = .TRUE.
C
C-----------------------------------------------------------------------
C OR L-RESOLVED PARTIAL CROSS SECTIONS;
C-----------------------------------------------------------------------
C
         ELSE IF(CLINE(4:6).NE.'   ') THEN
C
C READ DATA.
C
            READ(CLINE,1007) N, L
            IF ((N.GE.NMIN) .AND. (N.LE.NMAX)  .AND.
     &          (L.GE.0)    .AND. (L.LE.(N-1))      ) THEN
               READ (CLINE,1005) (SIGLA(NENRGY+I,I4IDFL(N,L)),
     &                            I=1,NEMORE)
            ELSE
               WRITE(I4UNIT(-1),2004) IBLK, NMIN, NMAX, 'N,L', N, L
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF
C
C SET L-RESOLVED DATA FLAG.
C
            LSETL = .TRUE.
C
C IF L = 0 THEN ALSO FILL CORRESPONDING M-RESOLVED STATE.
C
            IF (L.EQ.0) THEN
               READ (CLINE,1005) (SIGMA(NENRGY+I,I4IDFM(N,0,0)),
     &                            I=1,NEMORE)
            ENDIF
C
C-----------------------------------------------------------------------
C OR N-RESOLVED PARTIAL CROSS SECTIONS.
C-----------------------------------------------------------------------
C
         ELSE
C
C READ DATA.
C
            READ(CLINE,1008) N
            IF ((N.GE.NMIN) .AND. (N.LE.NMAX)) THEN
               READ (CLINE,1005) (SIGNA(NENRGY+I,N), I=1,NEMORE)
            ELSE
               WRITE(I4UNIT(-1),2004) IBLK, NMIN, NMAX, 'N', N
               WRITE(I4UNIT(-1),2006)
               STOP
            ENDIF
C
         ENDIF
C
C-----------------------------------------------------------------------
C GOTO START OF READ LOOP.
C-----------------------------------------------------------------------
C
         GOTO 1
C
      ENDIF
C
C***********************************************************************
C POST READ PROCESSING.
C***********************************************************************
C
    6 CONTINUE
C
C-----------------------------------------------------------------------
C UPDATE NUMBER OF ENERGIES.
C-----------------------------------------------------------------------
C
      NENRGY = NENRGY + NEMORE
C
C-----------------------------------------------------------------------
C CONVERT ENERGIES FROM KEV/AMU TO EV/AMU.
C-----------------------------------------------------------------------
C
      DO 7 I = 1, NENRGY
         ENRGYA(I) = ENRGYA(I) * 1.0D+03
    7 CONTINUE
C
C***********************************************************************
C CHECK DATA + ADD IN ZERO SHIFT.
C***********************************************************************
C
      DO 8 I = 1, NENRGY
C
         DO 9 J = NMIN, NMAX
            IF (SIGNA(I,J) .EQ. -1.0D+00) THEN
               WRITE(I4UNIT(-1),2005) 'N', ENRGYA(I), 'N', J
               WRITE(I4UNIT(-1),2006)
               STOP
            ELSE
               SIGNA(I,J) = SIGNA(I,J) + ZEROST
            ENDIF
    9    CONTINUE
C
         IF (LSETL) THEN
            DO 10 J = I4IDFL(NMIN,0), I4IDFL(NMAX,NMAX-1)
               IF (SIGLA(I,J) .EQ. -1.0D+00) THEN
                  CALL XXIDTL( J, N, L )
                  WRITE(I4UNIT(-1),2005) 'L', ENRGYA(I), 'N,L', N, L
                  WRITE(I4UNIT(-1),2006)
                  STOP
               ELSE
                  SIGLA(I,J) = SIGLA(I,J) + ZEROST
               ENDIF
   10       CONTINUE
         ENDIF
C
         IF (LSETM) THEN
            DO 11 J = I4IDFM(NMIN,0,0), I4IDFM(NMAX,NMAX-1,NMAX-1)
               IF (SIGMA(I,J) .EQ. -1.0D+00) THEN
                  CALL XXIDTM( J, N, L, M )
                  WRITE(I4UNIT(-1),2005) 'M', ENRGYA(I), 'N,L,M',
     &                                   N, L, M
                  WRITE(I4UNIT(-1),2006)
                  STOP
               ELSE
                  SIGMA(I,J) = SIGMA(I,J) + ZEROST
               ENDIF
   11       CONTINUE
         ENDIF
C
    8 CONTINUE
C
C***********************************************************************
C
 1000 FORMAT(1A133)
 1001 FORMAT(1A2,1X,1A2,5X,1A2,1X,1A2,2X,1A1)
 1002 FORMAT(1X,I4)
 1003 FORMAT(10X,9F9.2)
 1004 FORMAT(10X,9I9)
 1005 FORMAT(10X,1P,9D9.2)
 1006 FORMAT(3I3)
 1007 FORMAT(2I3)
 1008 FORMAT(I3)
C
 2000 FORMAT(1X,32('*'),' CXDATA ERROR ',32('*')//
     &       2X,'DATA FILE HAS INCORRECT FORMAT.'/
     &       2X,'EXPECTING NEW ADF01 FORMAT.')
 2001 FORMAT(1X,31('*'),' CXDATA MESSAGE ',31('*')//
     &       2X,'TOTAL NUMBER OF ENERGIES IN INPUT DATA SET TOO GREAT.'/
     &       2X,'ONLY THE FIRST ',I2,'HAVE BEEN ACCEPTED.'//
     &       1X,31('*'),' END OF MESSAGE ',31('*'))
 2002 FORMAT(1X,32('*'),' CXDATA ERROR ',32('*')//
     &       2X,'ERROR IN DATA SET BLOCK ',I3,' :'/
     &       2X,A,' READ FOR N DOES NOT MATCH PREVIOUS VALUE.'/
     &       2X,'VALUE READ = ',I3/
     &       2X,'PREVIOUS VALUE = ',I3)
 2003 FORMAT(1X,32('*'),' CXDATA ERROR ',32('*')//
     &       2X,'ERROR IN DATA SET BLOCK ',I3,' :'/
     &       2X,'MAXIMUM VALUE OF N TOO GREAT.'/
     &       2X,'VALUE READ  = ',I3/
     &       2X,'MAX ALLOWED = ',I3/
     &       2X,'INCREASE PARAMETER ''MXNSHL''.')
 2004 FORMAT(1X,32('*'),' CXDATA ERROR ',32('*')//
     &       2X,'ERROR IN DATA SET BLOCK ',I3,' :'/
     &       2X,'INVALID QUANTUM NUMBERS READ.'/
     &       2X,'NMIN, NMAX = ',I3,1X,I3/
     &       2X,A,' = ',3(I3,1X))
 2005 FORMAT(1X,32('*'),' CXDATA ERROR ',32('*')//
     &       2X,'MISSING ',A,'-RESOLVED DATA.'/
     &       2X,'ENERGY = ',F9.2, ' KEV/AMU'/
     &       2X,A,' = ',3(I3,1X))
 2006 FORMAT(/1X,29('*'),' PROGRAM TERMINATED ',29('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
