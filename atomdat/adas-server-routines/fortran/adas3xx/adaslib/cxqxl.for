CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxqxl.for,v 1.1 2004/07/06 13:10:58 whitefor Exp $ Date $Date: 2004/07/06 13:10:58 $
CX
      SUBROUTINE CXQXL ( MXNENG , MXNSHL , MXBEAM , NBEAM  ,
     &                   BMENA  , BMFRA  , NBOT   , NTOP   ,
     &                   NENRGY , ENRGYA , FRACLA , QTHEOR ,
     &                   RATE   , FTHEOR
     &                 )
C
       IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXQXL  *********************
C
C  PURPOSE:  USES THE INPUT DATASET TO CALCULATE THE CHARGE EXCHANGE
C            RATE COEFFICIENTS FOR NL-LEVELS AVERAGED OVER THE BEAM
C            FRACTIONS. RATES ARE EXPRESSED AS A FRACTION OF
C            CORRESPONDING N-LEVEL.
C
C  CALLING PROGRAM: ADAS308
C
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  MXBEAM    = MAXIMUM NO. OF BEAM ENERGIES.
C  INPUT : (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES IN DATASET.
C  INPUT : (R*8)  ENRGYA()  = COLLISION ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             EXPRESSED AS FRACTION OF CORRESPONDING
C                             N-RESOLVED CROSS-SECTION.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L)
C  INPUT : (R*8)  QTHEOR()  = MEAN RATE COEFFICIENTS FOR N-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  INPUT : (R*8)  RATE(,)   = RATE COEFFICIENTS FOR EACH COMPONENT OF
C                             THE BEAM AS A FUNCTION OF N-LEVEL.
C                             UNITS: CM3 SEC-1
C                             1ST DIMENSION: BEAM INDEX
C                             2ND DIMENSION: N-SHELL
C
C  INPUT : (R*8)  FTHEOR()  = MEAN RATE COEFFICIENTS FOR NL-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS. EXPRESSED AS
C                             FRACTIONS OF CORRESPONDING N-LEVELS.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C  PARAM : (I*4)  MXN       = 'MXNSHL'.
C
C          (I*4)  IB        = BEAM INDEX.
C          (I*4)  N         = PRINCIPAL QUANTUM NUMBER.
C          (I*4)  IE        = ENERGY INDEX.
C          (I*4)  IEU       = ENERGY INDEX ABOVE BEAM ENERGY.
C          (I*4)  IEL       = ENERGY INDEX BELOW BEAM ENERGY.
C          (I*4)  IDL       = LOOP INDEX.
C
C          (R*8)  GRAD      = GRADIENT OF STRAIGHT LINE INTERPOLATION.
C
C          (L*4)  LMATCH    = FLAG IF BEAM ENERGY MATCHES EXACTLY A DATA
C                             SET ENERGY.
C                           = .TRUE.  => BEAM ENERGY MATCHES.
C                           = .FALSE. => BEAM ENERGY DOES NOT MATCH.
C
C          (R*8)  PTH()     = RATE COEFFICIENTS FOR NL-LEVELS FOR A
C                             PATICULAR BEAM COMPONENT.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN THE QUANTUM
C                               NUMBERS N AND L.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    19/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    MXNENG  , MXNSHL  , MXBEAM  , NBEAM   , NBOT    ,
     &           NTOP    , NENRGY
      INTEGER    IB      , N       , IE      , IEU     , IEL     ,
     &           IDL
C-----------------------------------------------------------------------
      REAL*8     GRAD
C-----------------------------------------------------------------------
      LOGICAL    LMATCH
C-----------------------------------------------------------------------
      REAL*8     BMENA(MXBEAM)                 ,
     &           BMFRA(MXBEAM)                 ,
     &           ENRGYA(MXNENG)                ,
     &           QTHEOR(MXNSHL)                ,
     &           FTHEOR((MXNSHL*(MXNSHL+1))/2)
      REAL*8     FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)  ,
     &           RATE(MXBEAM,MXNSHL)
      REAL*8     PTH((MXN*(MXN+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETER 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CLEAR 'FTHEOR' ARRAY.
C-----------------------------------------------------------------------
C
      DO 1 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         FTHEOR(IDL) = 0.00D+00
    1 CONTINUE
C
C-----------------------------------------------------------------------
C
      DO 2 IB = 1 , NBEAM
C
C-----------------------------------------------------------------------
C FILL 'PTH' ARRAY.
C-----------------------------------------------------------------------
C
C
         IF ((BMENA(IB) .LT. ENRGYA(1)) .OR.
     &       (BMENA(IB) .GT. ENRGYA(NENRGY))) THEN
C
            DO 3 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
               PTH(IDL) = 0.00D+00
    3       CONTINUE
C
C-----------------------------------------------------------------------
C
         ELSE
C
            IEU = 0
            IEL = 0
            LMATCH = .FALSE.
C
            DO 4 IE = 1 , NENRGY
               IF (BMENA(IB) .GE. ENRGYA(IE)) THEN
                  IEL = IE
                  IEU = IE + 1
                  IF (BMENA(IB) .EQ. ENRGYA(IE)) LMATCH = .TRUE.
               ENDIF
    4       CONTINUE
C
            IF (LMATCH) THEN
C
               DO 5 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
                  PTH(IDL) = FRACLA(IEL,IDL)
    5          CONTINUE
C
            ELSE
C
               DO 6 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
                  GRAD = ( FRACLA(IEL,IDL) - FRACLA(IEU,IDL) )
     &                   / ( ENRGYA(IEL) - ENRGYA(IEU) )
                  PTH(IDL) = GRAD * ( BMENA(IB) - ENRGYA(IEU) )
     &                       + FRACLA(IEU,IDL)
    6          CONTINUE
C
            ENDIF
C
C-----------------------------------------------------------------------
C FILL 'FTHEOR' ARRAY.
C-----------------------------------------------------------------------
C
            DO 7 N = NBOT , NTOP
               DO 8 IDL = I4IDFL( N , 0 ) , I4IDFL( N , N-1 )
                  FTHEOR(IDL) = FTHEOR(IDL) + ( PTH(IDL) * BMFRA(IB)
     &                          * RATE(IB,N) )
    8          CONTINUE
    7       CONTINUE
C
C-----------------------------------------------------------------------
C
         ENDIF
C
    2 CONTINUE
C
C-----------------------------------------------------------------------
C NORMALIZE 'FTHEOR' ARRAY.
C-----------------------------------------------------------------------
C
      DO 9 N = 1 , MXNSHL
         IF (QTHEOR(N) .GT. 0.0D+00) THEN
            DO 10 IDL = I4IDFL( N , 0 ) , I4IDFL( N , N-1 )
               FTHEOR(IDL) = FTHEOR(IDL) / QTHEOR(N)
   10       CONTINUE
         ENDIF
    9 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXQXL ERROR  ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE CXQXL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
