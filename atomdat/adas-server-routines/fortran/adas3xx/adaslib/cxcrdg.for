CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxcrdg.for,v 1.3 2007/05/17 17:07:51 allan Exp $ Date $Date: 2007/05/17 17:07:51 $
CX
      SUBROUTINE CXCRDG ( MXNSHL , MXJSHL , IZ0    , IZ1    ,
     &                    NI     , LI     ,  NJ    , LJ     ,
     &                    TEV    , DENS   , ZP     , TPV    ,
     &                    EMP    , TBLF   , GAE    , GAP    ,
     &                    QEP    , QEM    , QIP    , QIM
     &                  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXCRDG *********************
C
C  PURPOSE:  CALCULATES ELECTRON AND +VE ION COLLISIONAL RATE
C            COEFFICIENTS BETWEEN NEARLY DEGENERATE LEVELS FOR
C            HYDROGEN-LIKE, LITHIUM-LIKE AND SODIUM-LIKE IONS.
C
C            RATES ARE CALCULATED FOR BOTH TERM AVERAGED AND
C            SEPARATE NLJ->NL+1J' AND NLJ->NL-1J' STATES.
C            
C
C  CALLING PROGRAM: C6TBQM , C8TBQM
C
C  INPUT : (I*4)  MXNSHL  = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL  = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ0     = TARGET NUCLEAR CHARGE.
C  INPUT : (I*4)  IZ1     = ION CHARGE.
C  INPUT : (I*4)  NI      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE I.
C  INPUT : (I*4)  LI      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE I.
C  INPUT : (I*4)  NJ      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE J.
C  INPUT : (I*4)  LJ      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE J.
C  INPUT : (R*8)  TEV     = TEMPERATURE (ELECTRON DISTRIBUTION).
C                           UNITS: EV
C  INPUT : (R*8)  DENS    = ELECTRON DENSITY.
C                           UNITS: CM-3
C  INPUT : (R*8)  ZP      = CHARGE OF COLLIDING POSITIVE ION.
C  INPUT : (R*8)  TPV     = TEMPERATURE (COLLIDING POSITIVE ION
C                           DISTRIBUTION).
C                           UNITS: EV
C  INPUT : (R*8)  EMP     = REDUCED MASS FOR COLLIDING POSITIVE ION.
C                           UNITS: ELECTRON MASSES
C  INPUT : (R*8)  TBLF()  = TABLE OF RADIATIVE LIFETIMES.
C                           UNITS: SECS
C                           DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (R*8)  GAE     = TERM AVERAGED GAMA RATE PARAMETER FOR
C                           ELECTRON COLLISIONS.
C  OUTPUT: (R*8)  GAP     = TERM AVERAGED GAMA RATE PARAMETER FOR
C                           POSITIVE ION COLLISIONS.
C  OUTPUT: (R*8)  QEP()   = ELECTRON RATE COEFFT. FOR NLJ->NL+1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C  OUTPUT: (R*8)  QEM()   = ELECTRON RATE COEFFT. FOR NLJ->NL-1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C  OUTPUT: (R*8)  QIP()   = POSITIVE ION RATE COEFFT. FOR NLJ->NL+1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C  OUTPUT: (R*8)  QIM()   = POSITIVE ION RATE COEFFT. FOR NLJ->NL-1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C
C  PARAM : (I*4)  MXTEMP  = MAXIMUM NUMBER OF TEMPERATURES.
C  PARAM : (I*4)  MXCOLL  = MAXIMUM NUMBER OF COLLISION STRENGTHS.
C  PARAM : (I*4)  MXTERM  = 2.
C  PARAM : (R*8)  P1      =
C
C          (I*4)  IZ      = IZ1-1.
C          (I*4)  NTEMP   = NUMBER OF TABULATED TEMPERATURES.
C          (I*4)  NCOLL   = NUMBER OF TABULATED COLLISION STRENGTHS.
C          (I*4)  ICI     =
C          (I*4)  ICJ     =
C          (I*4)  IZC     = INTEGER BELOW CHARGE OF POSITIVE ION.
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C
C          (R*8)  WI      = STATISTICAL WEIGHT OF STATE I.
C          (R*8)  WJ      = STATISTICAL WEIGHT OF STATE J.
C          (R*8)  EI      = BINDING ENERGY OF STATE I.
C                           UNITS: RYD
C          (R*8)  EJ      = BINDING ENERGY OF STATE I.
C                           UNITS: RYD
C          (R*8)  TE      = TEMPERATURE (ELECTRON DISTRIBUTION).
C                           UNITS:
C          (R*8)  TP      = TEMPERATURE (COLLIDING POSITIVE ION
C                           DISTRIBUTION).
C                           UNITS:
C          (R*8)  Z1      = REAL VALUE = IZ1.
C          (R*8)  XNI     = REAL VALUE = NI.
C          (R*8)  XLI     = REAL VALUE = LI.
C          (R*8)  XLJ     = REAL VALUE = LJ.
C          (R*8)  XLG     =
C          (R*8)  EI0     = BINDING ENERGY FOR STATE I.
C          (R*8)  EJ0     = BINDING ENERGY FOR STATE J.
C          (R*8)  ZEFFI   = EFFECTIVE ION CHARGE FOR STATE I.
C          (R*8)  ZEFFJ   = EFFECTIVE ION CHARGE FOR STATE J.
C          (R*8)  TAU     =
C          (R*8)  W       =
C          (R*8)  T1      =
C          (R*8)  T2      =
C          (R*8)  XXLI    =
C          (R*8)  XXLJ    =
C          (R*8)  XS      =
C          (R*8)  XXJI    =
C          (R*8)  XXJJ    =
C          (R*8)  DE      =
C          (R*8)  PHI     =
C          (R*8)  ZC      =
C          (R*8)  EMM     =
C          (R*8)  QI1     = COLLISIONAL EXCITATION RATE COEFFICIENT FROM
C                           PENGELLY AND SEATON.
C                           UNITS: CM3 SEC-1
C          (R*8)  QIJ     = COLLISIONAL DEEXCITATION RATE COEFFICIENT
C                           FROM PENGELLY AND SEATON.
C                           UNITS: CM3 SEC-1
C          (R*8)  G1      = GAMMA RATE PARAMETER FROM PENGELLY AND
C                           SEATON.
C                           UNITS:
C          (R*8)  RAT     =
C          (R*8)  SCF     = SCALING FACTOR.
C
C          (R*8)  EPS()   = INCIDENT ELECTRON ENERGIES.
C                           UNITS: RYD
C                           DIMENSION: COLLISION INDEX.
C          (R*8)  OMEG()  = COLLISION STRENGTHS.
C                           DIMENSION: COLLISION INDEX.
C          (R*8)  TA()    = TEMPERATURES (INCIDENT POSITIVE ION
C                           DISTRIBUTION).
C                           UNITS: EV
C                           DIMENSION: TEMPERATURE INDEX.
C          (R*8)  QI()    = COLLISIONAL EXCITATION RATE COEFFICIENTS
C                           FROM IMPACT PARAMETER APPROXIMATION.
C                           UNITS: CM3 SEC-1
C                           DIMENSION: TEMPERATURE INDEX.
C          (R*8)  QJ()    = COLLISIONAL DEEXCITATION RATE COEFFICIENTS
C                           FROM IMPACT PARAMETER APPROXIMATION.
C                           UNITS: CM3 SEC-1
C                           DIMENSION: TEMPERATURE INDEX.
C          (R*8)  GA()    = GAMMA RATE PARAMETERS FROM IMPACT PARAMETER
C                           APPROXIMATION.
C                           UNITS:
C                           DIMENSION: TEMPERATURE INDEX.
C          (R*8)  XSJ()   =
C                           DIMENSION: 2
C          (R*8)  XJJ()   =
C                           DIMENSION: 2
C          (R*8)  XEJ()   =
C                           DIMENSION: 2
C          (R*8)  XSI()   =
C                           DIMENSION: 2
C          (R*8)  XJI()   =
C                           DIMENSION: 2
C          (R*8)  XEI()   =
C                           DIMENSION: 2
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          R8ZETA     ADAS
C          R8WIG6     ADAS
C          CXHYDE     ADAS      CALCULATES BINDING ENERGY FOR H-LIKE
C                               ION.
C          CXLTHE     ADAS      CALCULATES BINDING ENERGY FOR LI-LIKE
C                               ION.
C          CXSODE     ADAS      CALCULATES BINDING ENERGY FOR NA-LIKE
C                               ION.
C          CXCRPS     ADAS      CALCULATES COLLISON RATE COEFFICIENTS
C                               FROM PENGELLY AND SEATON.
C          CXCRIP     ADAS      CALCULATES COLLISON RATE COEFFICIENTS
C                               FROM IMPACT PARAMETER APPROXIMATION.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C       2) THE SCALING FACTOR 'SCF' USED IN CALCULATING THE POSITIVE ION
C          RATES IS NEEDED BECAUSE THE CROSS-SECTION ROUTINES REQUIRE AN
C          INTEGER VALUE FOR THE ION CHARGE. THE ADJACENT LOWER INTEGER
C          VALUE IS USED AND THE RESULTS SCALED BY 'SCF'.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/10/93
C
C UPDATE:  01/11/93 - J NASH    - ADAS91:
C          UPDATED TO MORE GENERAL FORM TO ALLOW USE BY EITHER ADAS306
C          OR ADAS308. NOW HANDLES H-, LI-, OR NA-LIKE IONS, AND
C          RETURNS EXPLICIT RATES FOR J-RESOLVED TRANSITIONS.
C
C
C VERSION : 1.2  
C DATE    : 28-09-2005
C MODIFIED: Martin O'Mullane
C               - Scale the gamma (GAP) in addition to the rates
C                 since 308 and 309 use this and ignore QIP and QIM.
C
C VERSION : 1.3 
C DATE    : 17-05-2007
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
      REAL*8     R8ZETA  , R8WIG6
C-----------------------------------------------------------------------
      INTEGER    MXTEMP       , MXCOLL       , MXTERM
      PARAMETER( MXTEMP = 40  , MXCOLL = 20  , MXTERM = 2 )
      REAL*8     P1
      PARAMETER( P1 = 1.1605D4 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ0     , IZ1     , NI      ,
     &           LI      , NJ      , LJ
      INTEGER    IZ      , NTEMP   , NCOLL   , ICI     , ICJ     ,
     &           IZC     , I       , J
C-----------------------------------------------------------------------
      REAL*8     TEV     , DENS    , ZP      , TPV     , EMP     ,
     &           GAE     , GAP
      REAL*8     WI      , WJ      , EI      , EJ      , TE      ,
     &           TP      , Z1      , XNI     , XLI     , XLJ     ,
     &           XLG     , EI0     , EJ0     , ZEFFI   , ZEFFJ   ,
     &           TAU     , W       , T1      , T2      , XXLI    ,
     &           XXLJ    , XS      , XXJI    , XXJJ    , DE      ,
     &           PHI     , ZC      , EMM     , Q1I     , Q1J     ,
     &           G1      , RAT     , SCF
C-----------------------------------------------------------------------
      REAL*8     TBLF((MXNSHL*(MXNSHL+1))/2)  ,
     &           QEP(2*MXJSHL)                ,
     &           QEM(2*MXJSHL)                ,
     &           QIP(2*MXJSHL)                ,
     &           QIM(2*MXJSHL)
      REAL*8     EPS(MXCOLL)   , OMEG(MXCOLL)  , TA(MXTEMP)    ,
     &           QI(MXTEMP)    , QJ(MXTEMP)    , GA(MXTEMP)    ,
     &           XSJ(MXTERM)   , XJJ(MXTERM)   , XEJ(MXTERM)  ,
     &           XSI(MXTERM)   , XJI(MXTERM)   , XEI(MXTERM)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C INITIALIZE OUTPUT VALUES.
C-----------------------------------------------------------------------
C
      GAE = 0.0D0
      GAP = 0.0D0
C
      DO 1 I = 1 , 2 * MXJSHL
         QEP(I) = 0.0D0
         QEM(I) = 0.0D0
         QIP(I) = 0.0D0
         QIM(I) = 0.0D0
    1 CONTINUE
C
C-----------------------------------------------------------------------
C CHECK INPUT VALUES ARE VALID.
C-----------------------------------------------------------------------
C
      IF ((NI .NE. NJ) .OR. (NI .GT. MXNSHL) .OR.
     &    (IABS( LI - LJ ) .NE. 1)                ) RETURN
C
C-----------------------------------------------------------------------
C
      TE  = P1 * TEV
      TP  = P1 * TPV
C
      Z1 = DFLOAT( IZ1 )
      IZ = IZ1 - 1
C
      XNI = DFLOAT( NI )
C
      XLI = DFLOAT( LI )
      XLJ = DFLOAT( LJ )
      XLG = DMAX1( XLI , XLJ )
C
C-----------------------------------------------------------------------
C CALCULATE CENTRE BINDING ENERGY:
C   CXHYDE = HYDROGEN-LIKE.
C   CXLTHE = LITHIUM-LIKE.
C   CXSODE = SODIUM-LIKE.
C-----------------------------------------------------------------------
C
      IF (IZ1 .EQ. IZ0) THEN
         CALL CXHYDE( IZ0 , Z1 , NI , LI , EI0 )
         CALL CXHYDE( IZ0 , Z1 , NJ , LJ , EJ0 )
         ZEFFI = Z1
         ZEFFJ = Z1
      ELSE IF (IZ1 .EQ. (IZ0 - 2)) THEN
         CALL CXLTHE( IZ0 , Z1 , NI , LI , EI0 )
         CALL CXLTHE( IZ0 , Z1 , NJ , LJ , EJ0 )
         ZEFFI = Z1
         ZEFFJ = Z1
      ELSE IF (IZ1 .EQ. (IZ0 - 10)) THEN
         CALL CXSODE( IZ0 , NI , LI , ZEFFI , EI0 )
         CALL CXSODE( IZ0 , NJ , LJ , ZEFFJ , EJ0 )
      ELSE
         WRITE(I4UNIT(-1),1000) IZ0 , IZ1
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CHANGE TO TERM ENERGIES (-VE) AND ADD IN TERM SPLITTING
C-----------------------------------------------------------------------
C
      IF (LI .EQ. 0) THEN
         ICI = 1
         XSI(1) = 0.5D0
         XJI(1) = 0.5D0
         XEI(1) = -EI0
      ELSE
         ICI = 2
         XSI(1) = 0.5D0
         XSI(2) = 0.5D0
         XJI(1) = XLI + 0.5D0
         XJI(2) = XLI - 0.5D0
         XEI(1) = -EI0 + 0.5D0 * XLI * R8ZETA( ZEFFI , NI , LI )
         XEI(2) = -EI0 - 0.5D0 * ( XLI + 1.0D0 )
     &            * R8ZETA( ZEFFI , NI , LI )
      ENDIF
C
      ICJ = 2
      XSJ(1) = 0.5D0
      XSJ(2) = 0.5D0
      XJJ(1) = XLJ + 0.5D0
      XJJ(2) = XLJ - 0.5D0
      XEJ(1) = -EJ0 + 0.5D0 * XLJ * R8ZETA( ZEFFJ , NI , LJ )
      XEJ(2) = -EJ0 - 0.5D0 * ( XLJ + 1.0D0 )
     &         * R8ZETA( ZEFFJ , NI , LJ )
      TAU = DSQRT( TBLF(I4IDFL( NI , LI )) * TBLF(I4IDFL( NI , LJ )) )
      XXLI = XLI
      XXLJ = XLJ
      W  = R8WIG6( XLJ , 1.0D0 , XLI , XXLI , 0.0D0 , XXLJ )
      T1 = 2.25D0 * ( XNI / Z1 )**2 * XLG * ( XNI**2 - XLG**2 )
     &     * W**2 * ( 2.0D0 * XLI + 1.0D0 ) * ( 2.0D0 * XLJ + 1.0D0 )
C
C-----------------------------------------------------------------------
C
      DO 2 I = 1 , ICI
         DO 3 J = 1 , ICJ
C
            IF (XSI(I) .EQ. XSJ(J)) THEN
C
               XS   = XSI(I)
               XXJI = XJI(I)
               XXJJ = XJJ(J)
               W  = R8WIG6( XLJ , 1.0D0 , XLI , XXJI , XS , XXJJ )
               T2 = ( 2.0D0 * XXJI + 1.0D0 ) * ( 2.0D0 * XXJJ + 1.0D0 )
     &              * W**2 * T1
C
               IF (T2 .GT. 0.0D0) THEN
C
                  DE = XEJ(J) - XEI(I)
C
                  IF (DE .LE. 0.0D0) THEN
                     WI = 2.0D0 * XXJJ + 1.0D0
                     WJ = 2.0D0 * XXJI + 1.0D0
                     EI = -XEJ(J)
                     EJ = -XEI(I)
                     DE = -DE
                  ELSE
                     WI = 2.0D0 * XXJI + 1.0D0
                     WJ = 2.0D0 * XXJJ + 1.0D0
                     EI = -XEI(I)
                     EJ = -XEJ(J)
                  ENDIF
C
                  PHI = T2 / ( 3.0D0 * WI )
C
C-----------------------------------------------------------------------
C
                  NCOLL = 0
                  NTEMP = 1
                  TA(1) = TEV
                  IZC = 1
                  EMM = 1.0D0
C
                  CALL CXCRPS( IZ   , IZC  , NI   , LI   , LJ   ,
     &                         WI   , EI   , WJ   , EJ   , EMM  ,
     &                         PHI  , TEV  , TEV  , DENS , TAU ,
     &                         Q1I  , Q1J  , G1                   )
C
                  CALL CXCRIP( MXCOLL , MXTEMP , IZ     , IZC    ,
     &                         WI     , EI     , WJ     , EJ     ,
     &                         EMM    , PHI    , NCOLL  , EPS    ,
     &                         OMEG   , NTEMP  , TA     , RAT    ,
     &                         QI     , QJ     , GA                )
C
                  IF (G1 .EQ. 0.0D0) THEN
                     GAE = GAE + GA(1)
                  ELSE
                     GAE = GAE + G1
                  ENDIF
C
                  IF (DE .GT. 0.0D0) THEN
                     IF (G1 .EQ. 0.0D0) THEN
                        QEP(2*(I-1)+J) = QI(1)
                        QEM(2*(J-1)+I) = QJ(1)
                     ELSE
                        QEP(2*(I-1)+J) = Q1I
                        QEM(2*(J-1)+I) = Q1J
                     ENDIF
                  ELSE
                     IF (G1 .EQ. 0.0D0) THEN
                        QEP(2*(I-1)+J) = QJ(1)
                        QEM(2*(J-1)+I) = QI(1)
                     ELSE
                        QEP(2*(I-1)+J) = Q1J
                        QEM(2*(J-1)+I) = Q1I
                     ENDIF
                  ENDIF
C
C-----------------------------------------------------------------------
C
                  IZC = IDINT( ZP + 0.001D0 )
                  SCF = ( ZP / DFLOAT( IZC ) )**2
                  TA(1) = TPV
                  EMM = EMP
C
                  CALL CXCRPS( IZ   , IZC  , NI   , LI   , LJ   ,
     &                         WI   , EI   , WJ   , EJ   , EMM  ,
     &                         PHI  , TPV  , TEV  , DENS , TAU ,
     &                         Q1I  , Q1J  , G1                   )
C
                  CALL CXCRIP( MXCOLL , MXTEMP , IZ     , IZC    ,
     &                         WI     , EI     , WJ     , EJ     ,
     &                         EMM    , PHI    , NCOLL  , EPS    ,
     &                         OMEG   , NTEMP  , TA     , RAT    ,
     &                         QI     , QJ     , GA                )
C
                  IF (G1 .EQ. 0.0D0) THEN
                     GAP = GAP + SCF * GA(1)
                  ELSE
                     GAP = GAP + SCF * G1
                  ENDIF
C
                  IF (DE .GT. 0.0D0) THEN
                     IF (G1 .EQ. 0.0D0) THEN
                        QIP(2*(I-1)+J) = SCF * QI(1)
                        QIM(2*(J-1)+I) = SCF * QJ(1)
                     ELSE
                        QIP(2*(I-1)+J) = SCF * Q1I
                        QIM(2*(J-1)+I) = SCF * Q1J
                     ENDIF
                  ELSE
                     IF (G1 .EQ. 0.0D0) THEN
                        QIP(2*(I-1)+J) = SCF * QJ(1)
                        QIM(2*(J-1)+I) = SCF * QI(1)
                     ELSE
                        QIP(2*(I-1)+J) = SCF * Q1J
                        QIM(2*(J-1)+I) = SCF * Q1I
                     ENDIF
                  ENDIF
C
C-----------------------------------------------------------------------
C
               ENDIF
C
            ENDIF
C
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXCRDG ERROR ', 32('*') //
     &        2X, 'DIFFERENCE BETWEEN NUCLEAR AND ION CHARGE GIVES ',
     &            'UNKNOWN ION TYPE.'/
     &        2X, 'NUCLEAR CHARGE Z0 = ', I3 ,
     &            '   ION CHARGE Z1 = ', I3/
     &        2X, 'DIFFERENCE MUST BE 0 (H-LIKE), 2 (LI-LIKE), ',
     &            'OR 10 (NA-LIKE).' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
