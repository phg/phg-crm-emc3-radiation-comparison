CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxqxch.for,v 1.2 2007/05/17 17:07:51 allan Exp $ Date $Date: 2007/05/17 17:07:51 $
CX
      SUBROUTINE CXQXCH ( MXNENG , MXNSHL , MXBEAM , NBEAM  ,
     &                    BMENA  , BMFRA  , NBOT   , NTOP   ,
     &                    NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                    ALPHAA , XSECNA , FRACLA , QTHEOR ,
     &                    FTHEOR
     &                  )
C
      IMPLICIT NONE     
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXQXCH *********************
C
C  PURPOSE:  USES THE INPUT DATASET TO CALCULATE THE CHARGE EXCHANGE
C            RATE COEFFICIENTS FOR BOTH N-LEVELS AND NL-LEVELS AVERAGED
C            OVER THE BEAM FRACTIONS.
C            
C            NL-LEVEL RATES ARE EXPRESSED AS A FRACTION OF
C            CORRESPONDING N-LEVEL.
C
C  CALLING PROGRAM: ADAS308 , C6QXCH
C
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  MXBEAM    = MAXIMUM NO. OF BEAM ENERGIES.
C  INPUT : (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NMINF     = MINIMUM PRINCIPAL QUANTUM NUMBER OF INPUT
C                             DATASET.
C  INPUT : (I*4)  NMAXF     = MAXIMUM PRINCIPAL QUANTUM NUMBER OF INPUT
C                             DATASET.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES IN DATASET.
C  INPUT : (R*8)  ENRGYA()  = COLLISION ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C  INPUT : (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             EXPRESSED AS FRACTION OF CORRESPONDING
C                             N-RESOLVED CROSS-SECTION.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C  OUTPUT: (R*8)  QTHEOR()  = MEAN RATE COEFFICIENTS FOR N-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  OUTPUT: (R*8)  FTHEOR()  = MEAN RATE COEFFICIENTS FOR NL-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS. EXPRESSED AS
C                             FRACTIONS OF CORRESPONDING N-LEVELS.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C  PARAM : (I*4)  MXB       = 'MXBEAM'.
C  PARAM : (I*4)  MXN       = 'MXNSHL'.
C
C          (I*4)  IB        = BEAM INDEX.
C
C          (R*8)  RATE(,)   = RATE COEFFICIENTS FOR EACH COMPONENT OF
C                             THE BEAM AS A FUNCTION OF N-LEVEL.
C                             UNITS: CM3 SEC-1
C                             1ST DIMENSION: BEAM INDEX
C                             2ND DIMENSION: N-SHELL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURN UNIT NO. FOR OUTPUT OF MESSAGES.
C          CXQXN      ADAS      CALCULATES N-LEVEL RATE COEFFICIENTS.
C          CXQXL      ADAS      CALCULATES NL-LEVEL RATE COEFFICIENTS AS
C                               FRACTION OF CORRESPONDING N-LEVEL.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    19/10/93
C
C
C VERSION: 1.1                          DATE: 20-06-95
C MODIFIED: TIM HAMMOND (Probably)
C               - PUT UNDER S.C.C.S. CONTROL
C
C VERSION  : 1.2                        DATE: 17-05-07
C MODIFIED : Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    MXB       , MXN
      PARAMETER( MXB = 6   , MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    MXNENG  , MXNSHL  , MXBEAM  , NBEAM   , NBOT    ,
     &           NTOP    , NMINF   , NMAXF   , NENRGY
      INTEGER    IB
C-----------------------------------------------------------------------
      REAL*8     BMENA(MXBEAM)                 ,
     &           BMFRA(MXBEAM)                 ,
     &           ENRGYA(MXNENG)                ,
     &           ALPHAA(MXNENG)                ,
     &           QTHEOR(MXNSHL)                ,
     &           FTHEOR((MXNSHL*(MXNSHL+1))/2)
      REAL*8     XSECNA(MXNENG,MXNSHL)                 ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     RATE(MXB,MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXB' AND 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXB .LT. MXBEAM) THEN
         WRITE(I4UNIT(-1),1000) MXB, MXBEAM
         STOP
      ENDIF
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1001) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CHECK BEAM ENERGIES AGAINST INPUT DATA.
C-----------------------------------------------------------------------
C
      DO 1 IB = 1, NBEAM
         IF (BMENA(IB) .LT. ENRGYA(1)) THEN
            WRITE(I4UNIT(-1),1002) IB, BMENA(IB), ENRGYA(1)
            STOP
         ELSE IF (BMENA(IB) .GT. ENRGYA(NENRGY)) THEN
            WRITE(I4UNIT(-1),1003) IB, BMENA(IB), ENRGYA(NENRGY)
            STOP
         ENDIF
    1 CONTINUE
C
C-----------------------------------------------------------------------
C CALCULATE N-LEVEL RATE COEFFICIENTS.
C-----------------------------------------------------------------------
C
      CALL CXQXN ( MXNENG , MXNSHL , MXBEAM , NBEAM  ,
     &             BMENA  , BMFRA  , NBOT   , NTOP   ,
     &             NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &             ALPHAA , XSECNA , QTHEOR , RATE     )
C
C-----------------------------------------------------------------------
C CALCULATE NL-LEVEL RATE COEFFICIENTS.
C-----------------------------------------------------------------------
C
      CALL CXQXL ( MXNENG , MXNSHL , MXBEAM , NBEAM  ,
     &             BMENA  , BMFRA  , NBOT   , NTOP   ,
     &             NENRGY , ENRGYA , FRACLA , QTHEOR ,
     &             RATE   , FTHEOR                     )
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' CXQXCH ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXB'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXBEAM''.'/
     &        2X, 'MXB = ', I3 , '   MXBEAM = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXB'' IN SUBROUTINE CXQXCH.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' CXQXCH ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE CXQXCH.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1002 FORMAT( 1X, 32('*'), ' CXQXCH ERROR ', 32('*') //
     &        2X, 'ENERGY OF BEAM COMPONENT LESS THAN MINIMUM ',
     &            'ENERGY IN INPUT DATASET.'/
     &        2X, 'COMPONENT = ', I1 , '  ENERGY = ', 1P, D8.2 ,
     &            '  MIN. IN DATA = ', 1P, D8.2//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1003 FORMAT( 1X, 32('*'), ' CXQXCH ERROR ', 32('*') //
     &        2X, 'ENERGY OF BEAM COMPONENT GREATER THAN MAXIMUM ',
     &            'ENERGY IN INPUT DATASET.'/
     &        2X, 'COMPONENT = ', I1 , '  ENERGY = ', 1P, D8.2 ,
     &            '  MAX. IN DATA = ', 1P, D8.2//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN                                                            00007300
      END                                                               00007500
