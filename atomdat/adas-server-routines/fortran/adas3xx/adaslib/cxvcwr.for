       subroutine cxvcwr( iunit   , ndvec  ,
     &                    nvec    , vec    , cvtit
     &                   )
       implicit none
c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: cxvcwr *******************
c
c  purpose:  To write a vector of values in a multi-line specified
c            format with a title field .
c
c  Notes:
c
c  Subroutine:
c
c   input : (i*4)   iunit    = unit number for output.
c   input : (i*4)   ndvec    = maximum number of vector elements
c   input : (i*4)   nevc     = number of elements in vector
c   input : (r*8)   vec()    = vector for output
c                              1st dim: index of vector
c   input : (c*(*)) cvtit    = vector title string (appears on first
c                              line of output)
c
c  Routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     xxslen     adas      locate first and last char. of string
c
c
c  author   : Hugh Summers
c  date     : 30-05-2007
c
c  Version : 1.1
c  Date    : 30-05-2007
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       integer    iunit
       integer    ndvec
       integer    nvec
       integer    i       , i0     , i1
       integer    j       , j0     , j1     , nblin  , nbtail
c-----------------------------------------------------------------------
       character  c80*80     , cvtit*(*)
c-----------------------------------------------------------------------
       real*8     vec(ndvec)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------

       call xxslen(cvtit,i0,i1)
       
       nblin = nvec/6
       nbtail=nvec+6*nblin
       if(nbtail.gt.0) nblin=nblin+1
       
       do i=1,nblin
         
         j0=6*(i-1)+1
         j1=min(6*i,nvec)
         c80=' '
         if (i.eq.1)c80(64:64+i1-i0+1)=cvtit
       
         write(c80(1:60),'(1p6d10.2)')(vec(j),j=j0,j1)
         write(iunit,'(1a80)')c80
       
       enddo
       
       return

       end
