CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adaslib/cxcrps.for,v 1.1 2004/07/06 13:10:03 whitefor Exp $ Date $Date: 2004/07/06 13:10:03 $
CX
      SUBROUTINE CXCRPS( IZT  , IZC  , N    , LI   , LJ   ,
     &                   WI   , EI   , WJ   , EJ   , EM   ,
     &                   PHI  , TV   , TEV  , DENS , TAU  ,
     &                   QI   , QJ   , GA
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CXCRIP *********************
C
C  PURPOSE:  CALCULATES PENGELLY & SEATON (1964) COLLISION RATES BETWEEN
C            NEARLY DEGENERATE LEVELS. A VARIATION OF IMPACT PARAMETER
C            THEORY FOR DIPOLE TRANSITIONS IS USED.
C
C  CALLING PROGRAM:  CXCRDG
C
C  INPUT : (I*4)  IZT     = TARGET ION CHARGE.
C  INPUT : (I*4)  IZC     = CHARGE OF COLLIDING PARTICLE.
C  INPUT : (I*4)  N       = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  LI      = ORBITAL QUANTUM NUMBER.
C  INPUT : (I*4)  LJ      = ORBITAL QUANTUM NUMBER.
C  INPUT : (R*8)  WI      = STATISTICAL WEIGHT OF STATE I.
C  INPUT : (R*8)  EI      = BINDING ENERGY OF STATE I.
C                           UNITS: RYD
C  INPUT : (R*8)  WJ      = STATISTICAL WEIGHT OF STATE J.
C  INPUT : (R*8)  EJ      = BINDING ENERGY OF STATE J.
C                           UNITS: RYD
C  INPUT : (R*8)  EM      = REDUCED MASS FOR COLLIDING PARTICLE.
C                           UNITS: ELECTRON MASSES
C  INPUT : (R*8)  PHI     = FIJ/EIJ WHERE:
C                           FIJ = ABSORPTION OSCILLATOR STRENGTH;
C                           EIJ = EI-EJ = THE TRANSITION ENERGY (RYD).
C  INPUT : (R*8)  TV      = TEMPERATURE (COLLIDING PARTICLE
C                           DISTRIBUTION).
C                           UNITS: EV
C  INPUT : (R*8)  TEV     = TEMPERATURE (ELECTRON DISTRIBUTION).
C                           UNITS: EV
C  INPUT : (R*8)  DENS    = ELECTRON DENSITY.
C                           UNITS: CM-3
C  INPUT : (R*8)  TAU     = MEAN RADIATIVE LIFETIME OF INTIAL AND FINAL
C                           LEVELS.
C                           UNITS: SEC
C
C  OUTPUT: (R*8)  QI      = EXCITATION RATE COEFFICIENT.
C                           UNITS: CM3 SEC-1
C  OUTPUT: (R*8)  QJ      = DEEXCITATION RATE COEFFICIENTS.
C                           UNITS: CM3 SEC-1
C  OUTPUT: (R*8)  GA      = GAMMA RATE PARAMETER.
C                           UNITS:
C
C  PARAM : (R*8)  P1      =
C  PARAM : (R*8)  P2      =
C
C          (I*4)  IND1    =
C                           : 0 = FINITE RADIATIVE LIFETIME CUT-OFF.
C                           : 1 = BETHE CUT-OFF.
C          (I*4)  IND2    =
C                           : 0 = LIFETIME OR BETHE CUT-OFF.
C                           : 1 = DEBYE CUT-OFF.
C
C          (R*8)  T       = TEMPERATURE (COLLIDING PARTICLE
C                           DISTRIBUTION).
C                           UNITS:
C          (R*8)  TE      = TEMPERATURE (ELECTRON DISTRIBUTION).
C                           UNITS:
C          (R*8)  ATP     =
C          (R*8)  Z1      = ZT+1.
C          (R*8)  XN      = REAL VALUE = N.
C          (R*8)  XLI     = REAL VALUE = LI.
C          (R*8)  XLJ     = REAL VALUE = LJ.
C          (R*8)  XL      =
C          (R*8)  DNL     =
C          (R*8)  EIJ     =
C          (R*8)  TAU1    =
C          (R*8)  F1      =
C          (R*8)  F       =
C          (R*8)  B       =
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    11/10/93
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     P1              , P2
      PARAMETER( P1 = 1.16054D4  , P2 = 1.5789D5 )
C-----------------------------------------------------------------------
      INTEGER    IZT     , IZC     , N       , LI      , LJ
      INTEGER    IND1    , IND2
C-----------------------------------------------------------------------
      REAL*8     WI      , EI      , WJ      , EJ      , EM      ,
     &           PHI     , TV      , TEV     , DENS    , TAU     ,
     &           QI      , QJ      , GA
      REAL*8     T       , TE      , ATP     , Z1      , ZC      ,
     &           XN      , XLI     , XLJ     , XL      , DNL     ,
     &           EIJ     , TAU1    , F1      , F       , B
C-----------------------------------------------------------------------
C
      T   = P1 * TV
      TE  = P1 * TEV
      ATP = P2 / T
C
      Z1  = DFLOAT( IZT + 1 )
      ZC  = DFLOAT( IZC )
C
      XN  = DFLOAT( N )
      XLI = DFLOAT( LI )
      XLJ = DFLOAT( LJ )
      XL  = 0.5D0 * ( XLI + XLJ )
C
      DNL = 6.0D0 * ( ZC * XN / Z1 )**2 * (XN**2 - XL**2 - XL - 1.0D0 )
C
C-----------------------------------------------------------------------
C
      EIJ = DABS( EI - EJ )
      IF (EIJ .GT. 0.0D0) THEN
         TAU1 = 7.53D-17 / EIJ
      ELSE
         TAU1 = 1.0D0
      ENDIF
C
      IF (TAU1 .GT. TAU) THEN
         TAU1 = TAU
         IND1 = 0
      ELSE
         IND1=1
      ENDIF
C
C-----------------------------------------------------------------------
C
      F1 = 1.68D0 + DLOG10( TE / DENS )
      F  = 10.95D0 + DLOG10( T * TAU1**2 / EM )
C
      IF (F .LE. F1) THEN
         IND2 = 0
      ELSE
         F = F1
         IND2 = 1
      ENDIF
C
C-----------------------------------------------------------------------
C
      B = 11.54D0 + DLOG10( T / ( DNL * EM ) ) + F
C
      IF ( (B .GT. 1.0D0) .OR.
     &    ((B .GT. 0.0D0) .AND. (IND1 .EQ. 0)) .OR.
     &    ((B .GT. 0.0D0) .AND. (IND2 .EQ. 1))      ) THEN
C
         QI = 7.94D-5 * DSQRT( EM / T ) * ZC**2 * PHI * B
C
      ELSE
C
         QI = 0.0D0
C
      ENDIF
C
      QJ = QI * WI / WJ
      GA = 4.604D7 * QJ * WJ / DSQRT( ATP )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
