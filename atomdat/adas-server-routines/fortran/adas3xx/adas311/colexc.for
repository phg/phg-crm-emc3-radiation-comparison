CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/colexc.for,v 1.2 2004/07/06 12:07:48 whitefor Exp $ Date $Date: 2004/07/06 12:07:48 $
CX
       SUBROUTINE COLEXC(ATE,EN,EN1,AI)
       IMPLICIT REAL*8(A-H,O-Z)
       U=1.0/(EN*EN)
       EM=U-1.0/(EN1*EN1)
       X=ATE*U
       EXM=1.0
       IF(EM.GT.0.0)EXM=DEXP(ATE*EM)
       EX=EEI(X)
       E1=U-1.0/((EN1-0.5)*(EN1-0.5))
       F1=1.0
       IF(E1)2,4,6
    2  E1=-E1
       GO TO 7
    4  G1=(2.0+X)*EX-(1.0+1.0/X)
       GO TO 8
    6  F1=DEXP(ATE*(-E1))
    7  X1=X+ATE*E1
       G1=(U/E1+1.0)*EEI(X1)-(U/E1-1.0)*EX-1.0/X
    8  E2=U-1.0/((EN1+0.5)*(EN1+0.5))
       F2=1.0
       IF(E2)10,12,14
   10  E2=-E2
       GO TO 15
   12  G2=(2.0+X)*EX-(1.0+1.0/X)
       GO TO 16
   14  F2=DEXP(ATE*(-E2))
   15  X2=X+ATE*E2
       G2=(U/E2+1.0)*EEI(X2)-(U/E2-1.0)*EX-1.0/X
   16  AI=0.4166667D0*ATE*EXM*(F2*G2-F1*G1)
C       Commented out the last line
C	HARVEY ANDERSON
C	10/3/98
C
C      AI=0.0
       RETURN
      END
